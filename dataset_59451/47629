{{Infobox automobile
| name         = DTU Dynamo 7.0
| image        = DTU Dynamo 7.0 at EuroSpeedway Lausitz 1.jpg
| caption      = 
| manufacturer = 
| aka          = 
| production   = 
| model_years  = 
| assembly     = [[Kongens Lyngby]], Denmark
| predecessor  = Dynamo 6.0
| successor    = [[DTU Dynamo 8.0|Dynamo 8.0]]
| class        = 
| body_style   = 
| layout       = 
| platform     = 
| engine       = 49 [[Cubic centimetre|cm<sup>3</sup>]] [[Single-cylinder engine|single-cylinder]] 3 valve [[Overhead camshaft#Single overhead camshaft|SOHC]]
| transmission = 2-speed [[manual transmission|manual]]
| wheelbase    = 
| length       = 
| width        = 
| height       = 
| weight       = approx. 120 kg
| related      = 
| designer     = 
| sp           = uk
}}
The '''Dynamo 7.0''' is the seventh generation car run by [[DTU Roadrunners]] to compete in the Urban Concept class in the [[Eco-marathon|Shell Eco-marathon Europe]]. The car is developed by students at the [[Technical University of Denmark]] with the single purpose of achieving the best [[Fuel economy in automobiles|fuel economy]] as possible.

The Dynamo 7.0 features a 49 [[Cubic centimetre|cm<sup>3</sup>]] moped engine run on second generation [[Second generation biofuels|second generation]] bio[[Ethanol fuel|ethanol]].

In the 2011 [[Eco-marathon|Shell Eco-marathon Europe]] the Dynamo 7.0 achieved a record of 509.4&nbsp;km/L (gasoline equivalent) at the [[EuroSpeedway Lausitz]] racetrack, placing it first in the category of Urban Concept cars with [[internal combustion engine]]s.

==Design and technology==

===Engine and drive train===
A 49 cc [[Four-stroke engine|four-stroke]] [[Spark-ignition engine|spark ignited]] engine originating from a high-end moped is powering the Dynamo 7.0. The engine was in its originally application running on gasoline but has been modified to run on ethanol as a green alternative.<ref name="DTU Roadrunners webpage Engine">{{cite web
 | url=http://www.ecocar.mek.dtu.dk/Dynamo/Engine.aspx
 | title=Ecocar.dk: Engine description
 | publisher=[[Technical University of Denmark]]
 | accessdate=2012-04-28
}}</ref><ref name="Inbicon web page">{{cite web
 | url=http://www.inbicon.com/About_inbicon/News/Data/Pages/Worlds_top_eco-car_wins_using_ethanol_Inbicon.aspx
 | title=Worlds top eco-car wins using ethanol
 | publisher= Inbicon
 | accessdate=2012-04-28
}}</ref>
For this purpose the [[engine control unit|engine control unit (ECU)]] had to be replaced by a custom programmable ECU developed by students participating in the project. The programmable ECU enables user control of the [[air–fuel ratio]]. During a race the engine is run with a lean mixture to optimize fuel efficiency resulting in a [[thermal efficiency]] of approximately 30 percent.<ref name="Ingeniøren">{{cite web
 | url=http://ing.dk/artikel/119235-dtu-rykker-paa-bioethanol-til-dette-aars-shell-eco-marathon
 | title= DTU rykker på bioethanol til dette års Shell Eco-marathon (English: ''DTU goes for bioethanol for this year's version of the Shell Eco-marathon'')
 | publisher= [[Ingeniøren]] (English: ''The News Magazine "The Engineer"'')
 | accessdate=2012-04-28
}}</ref>

{| class="wikitable"
|+ Engine performance
|-
! scope="col" |  
! scope="col" | Gasoline
! scope="col" | Ethanol
|-
! scope="row" | Power [kW]
| 1.8 @ 5500 rpm || 2.0 @ 6000 rpm
|-
! scope="row" | Torque [Nm]
| 3.4 @ 4000 rpm || 3.5 @ 4000 rpm
|}

The car has only one wheel drive, meaning the engine transfers the torque to the rear right wheel. Torque is transferred to the wheel through a 2-speed [[manual transmission]] with an automatic dry clutch.<ref name="DTU Broadcast video">{{cite web
 | url=https://www.youtube.com/watch?v=ChZkcKBmTmw&hd=1
 | title=DTU Roadrunners' økobil kørte længst på literen (English: ''DTU Roadrunners eco-car drove farthest to the litre'')
 | publisher= DTUbroadcast
 | accessdate=2012-04-28
}}</ref>

===Body and chassis===
The car is made with separate chassis and bodywork. The aluminium chassis features double wishbone-style suspension. The suspension has no dampers and only act as linkage between the vehicle and the wheel enabling adjustments of ride height, camber and toe.<ref name="Ecocar.dk Steering and suspension">{{cite web
 | url=http://www.ecocar.mek.dtu.dk/Innovator/Steering%20and%20suspension.aspx
 | title= Ecocar.dk: Steering and suspension
 | publisher= [[Technical University of Denmark]]
 | accessdate=2012-04-28
}}</ref>

The body is made from [[carbon-fiber-reinforced polymer]]. It carries no structural loads and acts only as a housing guiding the air around the car. The aerodynamics of an [[eco-marathon]] challenger is a key parameter to optimize fuel efficiency. Therefore a spoiler was installed subsequently to improve the initial design which suffered from a somewhat high [[drag coefficient]]. The body is equipped with [[Light-emitting diode|LED]] indicator, head- and rear light.

The car is capable of carrying only the driver together with a suitcase-like object stored in the engine compartment.

==Race results==
During the Shell Eco-marathon 2011 at [[EuroSpeedway Lausitz]] a total of five valid attempts were made.<ref name="Shell Eco-Marathon 2011 results for Urban Concept, Internal Combustion">{{cite web
 |url=http://www-static.shell.com/static/ecomarathon/downloads/2011/europe/results/Final_results_Europe_2011_UrbanConcept_Internal_Combustion_Engine.pdf 
 |title=Shell Eco-marathon Europe 2011 Final results : UrbanConcept Internal Combustion Engine 
 |publisher=Royal Dutch Shell 
 |accessdate=2012-04-24 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120927113814/http://www-static.shell.com/static/ecomarathon/downloads/2011/europe/results/Final_results_Europe_2011_UrbanConcept_Internal_Combustion_Engine.pdf 
 |archivedate=2012-09-27 
 |df= 
}}</ref>
In each attempts improvement in fuel economy was made. This was achieved by optimizing several settings in the car including gear-ratio, fuel amount regulation, clutch engagement and aerodynamics.<ref name="Ecocar.dk 2011 achivements">{{cite web
 | url=http://www.ecocar.mek.dtu.dk/Achievements/2011.aspx
 | title=Ecocar.dk: Achievements 2011
 | publisher=[[Technical University of Denmark]]
 | accessdate=2012-04-28
}}</ref>

{| class="wikitable"
|+ '''2011 Race results <ref name="Shell Eco-Marathon 2011 results for Urban Concept, Internal Combustion" />
'''
|-
! Attempt number !! Result [km/L]
|-
| 1 || 338.3
|-
| 2 || 428.3
|-
| 3 || 457.5
|-
| 4 || 473.5
|-
| 5 || 509.4 
|}

==References==
{{reflist}}

== External links ==
# http://ecocar.dk/
# http://www.shell.com/home/content/ecomarathon/europe/
# http://www.dtu.dk/Nyheder/Nyt_fra_DTU/Arkiv.aspx?guid={C7C67F81-A33E-4DE6-AC93-53E4404167CE}
# http://www.dtu.dk/Nyheder/Nyt_fra_DTU/Arkiv.aspx?guid={0E739C57-8E34-435E-9ABF-4C0EEF31F859}

[[Category:Shell Eco-marathon challengers]]
[[Category:Concept cars]]