'''William Maks de May''' (1917&ndash;1993), known as Willy de Majo, was a graphic designer, born in Vienna.

De Majo is widely known as the founder of the [[International Council of Graphic Design Associations]] (ICOGRADA), and as a vocal supporter of the professional status of designers, playing a significant role advocating for the design profession internationally.

His archive is located at the [[University of Brighton Design Archives]].<ref>[http://arts.brighton.ac.uk/collections/design-archives/archives/willy-de-majo-archive Willy de Majo Archive:] [[University of Brighton Design Archives]]</ref>

== Education and early career==
After undertaking his training at the Vienna Commercial Academy (Wiener Handelsakademie), de Majo founded a design business in [[Belgrade]] in 1935.<ref>Willy de Majo on [http://archiveshub.ac.uk/data/gb1837-des/wdm The Archives Hub]</ref> In 1939, de Majo moved to Britain and joined the [[BBC]] as a broadcaster for their overseas service.

== World War Two==
From 1941-1943, de Majo served with the [[Royal Yugoslav Air Force]] attached to the [[Royal Air Force]]. In 1944 he was with the War Ministry in London, and between 1945-1946, he was with the [[RAF]] at [[Supreme Headquarters, Allied Expeditionary Force]]. De Majo was awarded a military [[MBE]] for his service.<ref>[http://www.independent.co.uk/news/people/obituary-willy-de-majo-1512513.html Obituary], The Independent, Saturday 23 October 1993</ref>

== Post-War==
Following the end the war, de Majo re-established his design practice - W. M. de Majo Associates - in London, offering graphic and exhibition design services, alongside corporate identity and product development. Clients at this time included [[British Overseas Airways Corporation]] and [[British South American Airways]] for whom he designed posters,<ref>[http://vintageposterblog.com/2011/04/13/modern-selling/#.VR07rTmHzZd Quad Royal: British post war posters and graphics]</ref> and Charles Letts & Co Ltd for whom he designed address books and diaries.<ref>''Design Journal'' No. 247, 1969, p27</ref>

De Majo designed the museum exhibition at [[Baden-Powell House]], London, which was opened by [[Queen Elizabeth II]] in 1961.<ref>[http://media.bufvc.ac.uk/newsonscreen2/Pathe/109774/NoS_109774_other.pdf Baden-Powell House] - British Universities Film and Video Council</ref>

== Festival of Britain==
In 1951, de Majo was the co-ordinating designer of the ‘Ulster Farm and Factory’ exhibition which was part of the [[Festival of Britain]]. The exhibition, held at [[Castlereagh (borough)|Castlereagh]], [[Northern Ireland]] told the story of how Ulster earned its living through agriculture and industry, and had as its central theme, the continuing tradition of craftsmanship and skill in farm and factory.<ref>Atkinson, H, ''The Festival of Britain: A Land and Its People'', I.B.Tauris, 2012, ISBN 978-1848857926</ref>

== ICOGRADA==
In 1963 de Majo became the first president of the International Council of Graphic Design Associations (ICOGRADA), an organization he founded with Peter Kneebone, recognizing “the need to create meaningful international dialogue around the future trajectory of graphic design.”<ref>[http://www.ico-d.org/about/history#legacy The history of ICOGRADA]</ref>

De Majo chaired the ICOGRADA Congresses in [[Zurich]] (1964) and in [[Bled]] (1966). A film was made of the Congress in Bled - ''ICOGRADA 66'' (1966) - for which de Majo wrote the commentary.<ref>Willy de Majo - [http://explore.bfi.org.uk/51dc8c0292dc0 Filmography] BFI</ref>

== Awards==
In 1969 de Majo was awarded the SIAD Design Medal for International Services to Design and the Profession.<ref>Chartered Society of Designers - [http://www.csd.org.uk/index.aspx?id=68 Medal Winners]</ref>
He also received various awards from international design associations including the commemorative medal of [[ZPAP]], the Association of Polish Designers and  honorary membership of the Dutch GVN Graphic Designers Association.

He was also made an Honorary Fellow of the [[Society of Typographic Designers]].

==External links and further reading==
*Breakell, Sue and Whitworth, Lesley, [http://jdh.oxfordjournals.org/content/early/2014/03/20/jdh.ept006.full ''Émigré Designers in the University of Brighton Design Archives''], Journal of Design History, Vol. 28, No.1, March 2013
*Woodham, Jonathan, ''[http://jdh.oxfordjournals.org/content/18/3/257.full.pdf+html Local, National and Global: Redrawing the Design Historical Map]'', Journal of Design History, Vol. 18, No. 3, Autumn 2005

==References==
{{reflist}}
*
*
*
*

==Categories==

{{DEFAULTSORT:de Majo, Willy}}
[[Category:1917 births]]
[[Category:Graphic designers]]
[[Category:1993 deaths]]
[[Category:Exhibition designers]]