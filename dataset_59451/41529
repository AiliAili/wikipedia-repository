{{Use mdy dates|date=January 2014}}
{{Infobox artist
| name          = Alma Thomas
| image         = Alma Thomas.jpg
| caption       = Alma Thomas in her studio, ca. 1968
| birth_name     = Alma Woodsey Thomas
| birth_date     = {{birth date |1891|9|22|mf-y}}
| birth_place    = [[Columbus, Georgia|Columbus]], Georgia, U.S.
| death_date     = {{death date and age |1978|2|24|1891|9|22|mf=y}}
| death_place    = Washington, D.C.
| field         = Painting
| training      = [[Howard University]]<br />[[Columbia University]]
| movement      = [[Expressionism]]<br />[[Realism (visual arts)|Realism]]
| works         = ''Sky Light''; ''Iris, Tulips, Jonquils and Crocuses''; ''Watusi (Hard Edge)''; ''Wind and Crepe Myrtle Concerto''; ''Air View of a Spring Nursery''; ''Milky Way''; ''Flowers at Jefferson Memorial''; ''Untitled (Music Series)''; ''Red Rose Sonata''; ''Breeze Rustling Through Fall Flowers''; ''The Eclipse''
| patrons       = 
| awards        = 
}}

'''Alma Woodsey Thomas''' (September 22, 1891 – February 24, 1978) was an African-American [[Expressionism|Expressionist]] painter and art educator.<ref name=nytimes>{{cite news | url=https://www.nytimes.com/2009/10/07/arts/design/07borrow.html?_r=1 | title=A Bold and Modern White House | last=Vogel | first=Carol | date=October 6, 2009 |work=The New York Times | pages=A14 | accessdate=October 7, 2009 | archiveurl= https://web.archive.org/web/20130616215631/http://www.nytimes.com/2009/10/07/arts/design/07borrow.html?_r=1| archivedate=June 16, 2013<!--DASHBot-->| deadurl= no}}</ref> She lived and worked primarily in Washington, D.C. and the ''[[Washington Post]]'' described her as a force in the [[Washington Color School]].<ref name=wapo>{{cite news | url=http://www.washingtonpost.com/wp-dyn/articles/A32839-2005Apr6.html | title=An Alumni Reunion On the Hilltop | last=Dawson | first=Jessica | date=April 7, 2005 |work=The Washington Post | pages=C05 | accessdate=October 8, 2009 }}</ref> [[The Wall Street Journal]] describes her as a previously "underappreciated artist" who is more recently recognized for her "exuberant" works, noteworthy for their pattern, rhythm and color.<ref>[https://www.wsj.com/articles/alma-thomas-review-1456869006 Alma Thomas Review] The Wall Street Journal, March 1, 2016</ref>

==Personal life and education==
Alma Thomas was born the eldest of four children to John Harris Thomas, a businessman,<ref name="GA">{{cite web | author=Charles T. Butler | year=2004 | title=Alma Thomas (1891–1978) | work=Individual Artists | publisher=The New Georgia Encyclopedia | url=http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-1040 | accessdate=July 6, 2011| archiveurl= https://web.archive.org/web/20110606080115/http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-1040| archivedate= June 6, 2011 <!--DASHBot-->| deadurl= no}}</ref> and Amelia Cantey Thomas, a [[fashion designer|dress designer]],<ref name="GA"/> in [[Columbus, Georgia]], 1891. In 1907 the family moved to the [[Logan Circle, Washington, D.C.|Logan Circle]] neighborhood of Washington, D.C., relocating due to [[racism|racial violence]] in [[Georgia (U.S. state)|Georgia]] and the [[state school|public school]] system of Washington.<ref name="Cotter1">{{cite news | author=Holland Cotter | title=White House Art: Colors From a World of Black and White | series=Critic's Notebook|work=The New York Times | url=https://www.nytimes.com/2009/10/11/weekinreview/11cotter.html | accessdate=July 6, 2011 | date=October 11, 2009}}</ref> As a child, she displayed artistic interest, making [[puppet]]s and sculptures at home.<ref name="GA"/> Thomas attended Armstrong Technical High School, where she took her first art classes. After graduating from high school in 1911, she studied [[kindergarten]] education at [[Miner Normal School]] until 1913. She served as a substitute teacher in Washington until 1914 when she obtained a permanent teaching position on the [[Eastern Shore of Maryland]]. Two years later, in 1916, she started teaching kindergarten at the [[Thomas Garrett Settlement House]] in [[Wilmington, Delaware]], staying there until 1923.<ref name="AAA">{{cite web | title=Alma Thomas papers, 1894–2000 | work=Finding Aid | publisher=Archives of American Art | url=http://www.aaa.si.edu/collections/alma-thomas-papers-9241/more#biohist | accessdate=July 6, 2011}}</ref>

[[File:Alma Thomas House.jpg|thumb|left|[[Alma Thomas House]] in Washington, D.C. Where Alma lived until her death.]]
Thomas entered [[Howard University]] in 1921, as a [[home economics]] student, only to switch to [[fine art]] after studying under art department founder [[James V. Herring]]. She earned her BS in Fine Arts in 1924<ref name="AAA"/> from Howard University; becoming the first graduate from the university Fine Arts program.<ref name=wapo/> That year, Thomas began teaching at Shaw Junior High School, where she taught until her retirement in 1960. She taught alongside [[Malkia Roberts]].<ref name="ThomasArt1998">{{cite book|author1=Alma Thomas|author2=Fort Wayne Museum of Art|title=Alma W. Thomas: A Retrospective of the Paintings|url=https://books.google.com/books?id=eb5222HXmzoC&pg=PA43|year=1998|publisher=Pomegranate|isbn=978-0-7649-0686-2|pages=43–}}</ref> While at Shaw Junior High, she started a community arts program that encouraged student appreciation of fine art. The program supported [[marionette]] performances and the distribution of student designed [[holiday card]]s which were given to soldiers at the [[Tuskegee Veterans Administration Medical Center]]. In 1934, she earned her [[Master of Arts|Masters]] in [[Art Education]] from [[Columbia University]] and studied painting at [[American University]] under [[Jacob Kainen]] from 1950 to 1960. In 1958, she visited art centers in Western Europe on behalf of the [[Tler School of Art]]. 
She retired in 1960 from teaching and dedicated herself to painting. 
In 1963, she walked in the [[March on Washington]] with her friend [[Lillian Evans]].<ref>{{cite web|url=http://blog.aaa.si.edu/2013/08/alma-thomass-march-on-washington-with-250000-others.html|title=Alma Thomas’s March on Washington …with 250,000 Others|date=August 9, 2013|publisher=Archives of American Art}}</ref>

Alma Thomas died on February 28, 1978 still living in the same house that her family moved into upon their arrival in Washington in 1906,<ref name="Cotter1"/>.<ref name="AAA"/>

==Artistic career==
<blockquote>
"''Creative art is for all time and is therefore independent of time. It is of all ages, of every land, and if by this we mean the creative spirit in man which produces a picture or a statue is common to the whole civilized world, independent of age, race and nationality; the statement may stand unchallenged.''"<br/>-Alma Thomas, 1970<ref name="D220">Patton, 220.</ref></blockquote>

Alma Thomas' early work was [[representational art|representational]] in manner.<ref name="D220"/>  Upon further education at Howard and training under James V. Herring and [[Lois Mailou Jones]] her work became more [[abstract art|abstract]].<ref name="NMWA2">{{cite web | year=2011 | title=Alma Woodsey Thomas | work=Artist Profile | publisher=National Museum of Women in the Arts | url=http://www.nmwa.org/collection/Profile.asp?LinkID=753 | accessdate=July 6, 2011| archiveurl= https://web.archive.org/web/20110624191431/http://nmwa.org/collection/Profile.asp?LinkID=753| archivedate= June 24, 2011 <!--DASHBot-->| deadurl= no}}</ref> Thomas would not be recognized as a professional artist until her retirement from teaching in 1960, when she enrolled in classes at American University. There she learned about the [[Color Field]] movement and theory from [[Ben L. Summerford]] and [[Jacob Kainen]]. She then became interested in the use of color and [[composition (visual arts)|composition]]. Within twelve years after her first class at American, she began creating Color Field paintings, inspired by the work of the [[New York School (art)|New York School]] and [[Abstract Expressionism]].<ref name="D220"/> She worked out of the kitchen in her house, creating works like ''Watusi (Hard Edge)'' (1963), a manipulation of the [[Matisse]] cutout ''The Snail'',<ref name="Gopnik"/> in which Thomas shifted shapes around and changed the colors that Matisse used, and named it after a [[Chubby Checker]] song.<ref name="Cotter1"/>

Her first retrospective exhibit was in 1966 at the Gallery of Art at Howard University, curated by art historian [[James A. Porter]]. For this exhibition, she created ''Earth Paintings'', a series of nature inspired abstract works, including ''Wind and Crepe Myrtle Concerto'' (1973) which art historian [[Sharon Patton]] considers "one of the most [[Minimalist]] Color-Field paintings ever produced by an African-American artist."<ref name="D220"/> These paintings have been compared to [[Byzantine art|Byzantine]] [[mosaic]]s and the [[Pointillism|pointillist]] paintings of [[Georges-Pierre Seurat]].<ref name="NMWA2"/> A friend of [[Delilah Pierce]], Thomas and Pierce would drive into the [[countryside]] where Thomas would seek inspiration, pulling ideas from the effects of light and atmosphere on rural environments. Thomas was, in 1972, the first African-American woman to have a solo exhibition at the [[Whitney Museum of American Art]], and within the same year an exhibition was also held at the [[Corcoran Gallery of Art]].<ref name="D220"/>

===Legacy===
[[File:Alma Thomas at Whitney.jpg|thumb|right|Thomas at opening in the [[Whitney Museum]], 1972]]

In 2009, two paintings, including ''Watusi (Hard Edge)'',<ref name="Cotter1"/>  by Alma Thomas were chosen by [[Michelle Obama|First Lady Michelle Obama]], [[White House]] interior designer Michael Smith and White House [[curator]] William Allman to be exhibited during the [[Obama presidency]].<ref name="NYT1">{{cite news | title=A Bold and Modern White House | work=Art & Design | publisher=The New York Time | url=https://www.nytimes.com/2009/10/07/arts/design/07borrow.html?_r=1 | accessdate=July 6, 2011 | first=Carol | last=Vogel | date=October 7, 2009}}</ref> ''Watusi (Hard Edge)'' was eventually removed from the White House due to concerns with the piece fitting into the space in Michelle Obama's [[East Wing]] office.<ref>http://flavorwire.com/48319/alma-thomas-watusi-gets-the-white-house-kibosh</ref> ''Sky Light'', on loan from the [[Hirshhorn Museum and Sculpture Garden]], hung in the Obama family private quarters.<ref name="Gopnik">{{cite news | author=Blake Gopnik | title=Alma Thomas's "Watusi (Hard Edge)" Won't Hang in White House | series=Arts & Living |work=Washington Post | url=http://www.washingtonpost.com/wp-dyn/content/article/2009/11/04/AR2009110405053.html | accessdate=July 6, 2011 | date=November 5, 2009}}</ref> In 2015, the Obamas hung Thomas's work ''Resurrection'' in the Old Family Dining Room.<ref>{{Cite news|url=http://www.economist.com/news/books-and-arts/21707510-how-forgotten-african-american-artists-are-coming-back-mainstream-rediscovery|title=Rediscovery|newspaper=The Economist|issn=0013-0613|access-date=2016-09-23}}</ref><ref>{{Cite web|url=http://www.culturetype.com/2015/04/17/alma-thomas-is-given-pride-of-place-at-the-white-house/|title=Alma Thomas is Given Pride of Place at the White House {{!}} Culture Type|access-date=2016-09-23}}</ref> The painting is the first work by an African-American woman to hang in the public spaces of the White House as part of the permanent collection.<ref>{{Cite web|url=http://www.culturetype.com/2015/04/17/alma-thomas-is-given-pride-of-place-at-the-white-house/|title=Alma Thomas is Given Pride of Place at the White House {{!}} Culture Type|access-date=2016-09-23}}</ref> The choice of Thomas for the White House collection was described as an ideal [[symbol]] for the Obama administration by ''[[New York Times]]'' [[art critic]] Holland Cotter. Cotter described Thomas' work as "forward-looking without being radical; post-racial but also race-conscious."<ref name="Artnews1">{{cite web | author=Robin Cembalest  | year=2009 | title=Critics Nix Obamas' Pix Mix   | work=Past Issues| publisher=ARTnews| url=http://artnews.com/issues/article.asp?art_id=2789 | accessdate=July 6, 2011| archiveurl= https://web.archive.org/web/20110707170726/http://artnews.com/issues/article.asp?art_id=2789| archivedate= July 7, 2011 <!--DASHBot-->| deadurl= no}}</ref> Thomas' papers were donated in several periods between 1979 and 2004 to the [[Archives of American Art]] by J. Maurice Thomas, Alma Thomas' sister.<ref name="AAA"/>

==Notable exhibitions==

*''A Proud Continuum: Eight Decades of Art at Howard University'', 2005, Howard University<ref name=wapo/>
*''Color Balance: Paintings by Felrath Hines and Alma Thomas'', 2010, [[Nasher Museum of Art]]<ref name="Nasher">{{cite web | year=2011 | title=Color Balance: Paintings by Felrath Hines and Alma Thomas | work=Exhibitions | publisher=Nasher Museum of Art | url=http://www.nasher.duke.edu/exhibitions_colorbalance.php | accessdate=July 6, 2011}}</ref>
* "Alma Thomas", 2016, [[Studio Museum in Harlem]]<ref name="Studio Museum">{{cite web|title=Past Exhibits - Alma Thomas|url=https://www.studiomuseum.org/exhibition/alma-thomas|website=Studio Museum Harlem|accessdate=11 March 2017}}</ref>

==Notable collections==

*''Air View of a Spring Nursery'', 1966; [[Columbus Museum]]<ref name="GA"/>
*''Breeze Rustling Through Fall Flowers'', 1968; [[Phillips Collection]]<ref name="Phillips">{{cite web | title=Breeze Rustling Through Fall Flowers | work=American Art | publisher=Phillips Museum | url=http://www.phillipscollection.org/research/american_art/artwork/Thomas-Breeze_Rustling.htm | accessdate=July 6, 2011}}</ref>
*''Iris, Tulips, Jonquils and Crocuses'', 1969; [[National Museum of Women in the Arts]]<ref name="NMWA1">{{cite web|url=https://nmwa.org/works/iris-tulips-jonquils-and-crocuses|title=Iris, Tulips, Jonquils and Crocuses|last=|first=|date=|year=2011|publisher=National Museum of Women in the Arts|work=Permanent Collection|accessdate=January 10, 2017|archiveurl=https://web.archive.org/web/20110624204926/http://nmwa.org/collection/detail.asp?WorkID=2178|archivedate=June 24, 2011 <!--DASHBot-->|deadurl=no}}</ref>
* ''Evening Glow'', 1972; [[Baltimore Museum of Art]]<ref>{{Cite web|url=http://collection.artbma.org/emuseum/view/objects/asitem/search$0040/0/title-asc?t:state:flow=cb452905-77fb-40d9-ab41-8a056e19f26a|title=The Baltimore Museum of Art|last=|first=|date=|website=collection.artbma.org|publisher=|access-date=2016-02-13}}</ref>
*''Red Roses Sonata'', 1972; [[The Metropolitan Museum of Art]]<ref>{{cite web|url=http://www.metmuseum.org/art/collection/search/632918?sortBy=Relevance&amp;ft=Alma+Thomas&amp;offset=0&amp;rpp=20&amp;pos=1|title=The Metropolitan Museum of Art|last=|first=|date=|website=metmuseum.org/art/collection|publisher=|access-date=2017-01-29}}</ref>

==Notes==
{{reflist|2}}

==References==

*Patton, Sharon F. ''African-American Art.'' Oxford: Oxford University Press (1998). ISBN 978-01-92842-13-8

==Further reading==

*''Alma W. Thomas: A Retrospective of the Paintings''. Fort Wayne: Fort Wayne Museum of Art (1998). ISBN 0-7649-0686-0
* ''Alma W. Thomas: A Retrospective of the Paintings''. Fort Wayne: Fort Wayne Museum of Art (1998). ISBN 0-7649-0686-0
* Merry A. Foresta, ''A Life in Art: Alma Thomas, 1891-1978''. Washington, D.C.: National Museum of American Art (1981). [http://www.worldcat.org/title/life-in-art-alma-thomas-1891-1978-national-museum-of-american-art-smithsonian-institution-washington-26111981-2821982/oclc/927776976?ht=edition&referer=di OCLC 927776976]
* ''Alma Thomas''. New York: Whitney Museum of American Art (1972). [http://www.worldcat.org/title/alma-w-thomas/oclc/53302446&referer=brief_results OCLC 53302446]
* {{cite book|last1=Berry|first1=Ian|last2=Haynes|first2=Lauren|title=Alma Thomas|date=2016|publisher=Prestel|isbn=3791355716}}
*[https://www.nytimes.com/2016/08/05/arts/design/alma-thomas-an-incandescent-pioneer.html Retrospective article from ''The New York Times'']
*{{cite news|last1=Dobryzinski|first1=Judith H.|title='Alma Thomas' Review; Alma Thomas was an Underappreciated Artist Who Immersed Herself in a Lifetime of Learning and Beauty.|url=http://search.proquest.com/docview/1769027434?accountid=108|publisher=ProQuest|date=2016}}

{{Authority control}}

{{DEFAULTSORT:Thomas, Alma}}
[[Category:1891 births]]
[[Category:1978 deaths]]
[[Category:Abstract expressionist artists]]
[[Category:African-American artists]]
[[Category:People from Columbus, Georgia]]
[[Category:Artists from Washington, D.C.]]
[[Category:Howard University alumni]]
[[Category:Columbia University alumni]]
[[Category:Artists from Georgia (U.S. state)]]
[[Category:20th-century women artists]]