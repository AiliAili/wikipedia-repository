{{good article}}
{{Infobox single
| Name = Addicted to You
| Cover = Utada Hikaru - Addicted To You.jpg
| Artist = [[Utada Hikaru]]
| from Album = [[Distance (Hikaru Utada album)|Distance]]
| Released = September 6, 1999
| Format = {{flat list|
* [[CD single]]
* [[Music download|digital download]]}}
| Recorded = 1999
| Genre=  {{flat list|
* [[Rock music|Rock]]
* [[Contemporary R&B|R&B]]}}
| Length = 
| Label = [[EMI Music Japan]]
| Writer = Hikaru Utada
| Producer = {{flat list|
* [[Jimmy Jam & Terry Lewis]]}}
| Reviews = 
| Last single = "[[First Love (Hikaru Utada song)|First Love]]" <br>(1999)
| This single = "Addicted to You" <br>(1999)
| Next single = "[[Wait & See (Risk)]]" <br>(2000)
}}

"'''Addicted to You'''" is a song by Japanese recording artist [[Utada Hikaru]] from her second studio album ''[[Distance (Hikaru Utada album)|Distance]]'' (2001). It was released as the album's [[Single (music)|lead single]] on September 6, 1999 by [[EMI Music Japan]]. "Addicted to You" was written by Utada and produced by [[Jimmy Jam & Terry Lewis]]; this is her first collaboration with American producers and composers. The single artwork was shot by American photographer [[Richard Avedon]] and features two black-and-white figures of Utada. Musically, "Addicted to You" is a [[R&B]] and [[rock music|rock]] song.

"Addicted to You" received positive reviews from music critics, many who highlighted it from the parent album; one music critic viewed the single as "[[nostalgia|nostalgic]]".<ref name="yeah"/> It achieved commercial success in Japan, with a peak position of number one on the [[Oricon Singles Chart]] and a million certification by the [[Recording Industry Association of Japan]] (RIAJ). The single remains the fourth highest selling single in first week sales, and the thirty-ninth best selling single in Japan. A music video was shot in [[Hong Kong]], and featured Utada inside a club.

==Background and release==
{{double image|left|JimmyJamHWoFJan2012.jpg|126|TerryLewisHWoFJan2012.jpg|107|[[Jimmy Jam & Terry Lewis]] produced several tracks for Utada's album ''Distance''}}
"Addicted to You" was written by Utada and produced by [[Jimmy Jam & Terry Lewis]]; this is her first collaboration with American producers and composers.<ref name="note">{{cite AV media notes|title="Addicted to You"|others=Utada Hikaru|publisher=[[EMI Music Japan]]|year=September 16, 1999|type=CD Single: liner notes}}</ref> The song was recorded in mid-1999 at Flyte Tyme Studios, [[Minneapolis, Minnesota]] and mixed at Flyte Tyme, [[Edina, Minnesota]].<ref name="note"/> Darnell Davis played the keyboard and Alex Richbourg played the drums.<ref name="note"/> There are two versions of the track: the Up-in-Heaven mix and the Underwater mix. EMI did not originally select "Addicted to You" as the lead single as they feared it would not make impact in Japan.<ref name="note"/> The song was then re-composed in order to appeal more to the Japanese audience.<ref name="note"/> "Addicted to You" is a [[rock music|rock]] and [[R&B]] song.<ref name="db">{{cite journal|work=OngakuDB.com, provided by [[Yahoo! Music]] JP|accessdate=28 April 2015|url=http://music.yahoo.co.jp/jpop/cd_reviews/ongakudb/20010418/odbrvw003.html|title=Yahoo! Music - J-Pop Reviews [Utada Hikaru - Distance]|author=Unknown|archiveurl=https://web.archive.org/web/20040813195945/http://music.yahoo.co.jp/jpop/cd_reviews/ongakudb/20010418/odbrvw003.html|archivedate=13 August 2004}}</ref>

"Addicted to You" was released on September 16, 1999 in [[CD]] format by EMI, as the lead single for the singer's second studio album ''[[Distance (Hikaru Utada album)|Distance]]'' (2001).<ref>{{cite web|url=http://www.amazon.com/Addicted-You-Hikaru-Utada/dp/B00003Q4A9|title=Utada Hikaru: Addicted to You|publisher=[[Amazon.com]]|accessdate=28 April 2015}}</ref> The cover sleeve features two shots of Utada, one being a close-up of Utada's face and the second being a long shot of her, and was photographed by American artist [[Richard Avedon]], his only collaboration with Utada up until his death in October 2004 as a result of a [[cerebral hemorrhage]].<ref>[https://www.nytimes.com/2004/10/01/arts/01CND-AVED.html "Richard Avedon, the Eye of Fashion, Dies at 81"], Andy Grundberg, ''[[The New York Times]]'', 1 October 2004.</ref><ref>{{cite news|last=Rourke|first=Mary|title=Photographer Richard Avedon Dies|url=http://www.latimes.com/la-100104avedon_lat,0,2855512.story|accessdate=22 March 2013|newspaper=Los Angeles Times}}</ref> The CD featured the instrumental and radio edits of both versions.<ref name="note"/>

==Critical reception==
"Addicted to You" received positive reviews from music critics. Editor in chief for ''[[Rockin'On Japan]]'' magazine Kano said that the song was "exceptional" and commended her collaboration with Jam and Lewis.<ref>{{cite web|url=http://www.utadahikaru.jp/sc/review.htm |title=Utada Hikaru Single Collection review |author=Kano |work=[[Rockin'On Japan]] |accessdate=4 July 2015}}</ref> Yonemoto Hiromi from ''Yeah!! J-Pop!'' was positive in his review, feeling that the composition sounded "nostalgic"-like.<ref name="yeah">{{cite journal|work=Yeah!! J-Pop!, provided by [[Yahoo! Music]] JP|accessdate=28 April 2015|url=http://music.yahoo.co.jp/jpop/cd_reviews/planet/20010328/plarvw002.html|title=Yahoo! Music - J-Pop Reviews [Utada Hikaru - Distance]|author=Yonemoto Hiromi|archiveurl=https://web.archive.org/web/20040813195739/http://music.yahoo.co.jp/jpop/cd_reviews/planet/20010328/plarvw002.html|archivedate=13 August 2004}}</ref> A staff review from ''CDJournal'' discussed Utada's first [[greatest hits]] compilation ''[[Utada Hikaru Single Collection Vol. 1]]'' and commended Utada's "fine" vocal delivery and composition."<ref name="addicted">{{cite web|url=http://artist.cdjournal.com/d/utada-hikaru-single-collection-vol1/3204020152 |title=Ayumi Hamasaki / Utada Single Collection Vol. 1 review | script-title=ja:宇多田 ヒカル / Utada Single Collection Vol. 1 [CCCD] |last=|first=|work=CdJournal.com |date= 18 February 2015 |accessdate=14 March 2015}}</ref> Ian Martin from [[Allmusic]] also commended Jam and Lewis' collaboration, saying that "providing stark contrast to the cheap, tinny sound that characterized much Japanese pop of the previous decade, with "Wait & See" and "Addicted to You" both featuring the production talents of Jimmy Jam & Terry Lewis."<ref>{{cite web| first=Ian | last=Martin | url=http://www.allmusic.com/album/distance-mw0000359515 |title=AllMusic – Hikaru Utada – Distance – Songs, Highlights, Credits and Awards |work=Allmusic. | accessdate=5 July 2015}}</ref> Miko Amaranthine from ''[[Yahoo! Music]]'' listed the song at number four on his Top Ten Hikaru Utada songs, stating ""Addicted to you" is one of my favorites for a slightly greedy purpose [...] When I listen to this song, I am reminded how much I love my marriage and am thankful I do not have to play the "dating game"."<ref name="yahoo">{{cite web| first=Miko | last=Amaranthine | url=http://voices.yahoo.com/top-10-songs-favorite-artist-utada-hikaru-1033591.html?cat=9 |title=Yahoo! Music Best Utada Songs |work=[[Yahoo! Music]] |archiveurl=https://web.archive.org/web/20140509005518/http://voices.yahoo.com/top-10-songs-favorite-artist-utada-hikaru-1033591.html?cat=9 |archivedate=9 May 2014}}</ref> At the 15th Annual Japan Gold Disc Awards, "Addicted to You" and Utada's previous singles "[[Automatic/Time Will Tell|Automatic]]" and "[[Movin' on Without You]]" received the award for "Song of the Year".<ref>{{cite web| url=https://www.riaj.or.jp/e/data/gdisc/2000.html |title=Recording Industry Association of Japan - THE 15th JAPAN GOLD DISC AWARD 2002
 |work=[[Recording Industry Association of Japan]] | date=2000 | accessdate=5 July 2015}}</ref>

==Commercial response==
"Addicted to You" debuted at number one on the Japanese [[Oricon Singles Chart]], her second number one single since her "[[Movin' on Without You]]" (1999), and stayed in the chart for fifteen weeks.<ref name="chart">{{cite web|work=[[Oricon]]|accessdate=28 April 2015|url=http://www.oricon.co.jp/music/release/d/267401/1/|title=Release - ORICON MUSIC STYLE|archiveurl=https://web.archive.org/web/20060113030343/http://www.oricon.co.jp/music/release/d/267401/1/|archivedate=13 January 2006}}</ref> First week sales sold over one million units, making it the second highest selling single in the first charting week behind [[Mr. Children]]'s "Nameless Poetry", but both positions were replaced by [[AKB48]]'s "Everyday, Katyusha" and "Flying Get" in 2011.<ref>{{cite web | url=http://www.musictvprogram.com/corner-ranking-single-syodo.html | title=Corner Ranking Single - RIAJ |publisher=Music TV Program | accessdate=18 April 2015}}</ref> The song was certified million by RIAJ for shipments of one million units, selling 1.7 million units in total. "Addicted to You" is the thirty-ninth best selling single in Japan music history and Utada's second best selling single behind "[[Automatic (Hikaru Utada song)|Automatic/Time Will Tell]]".<ref name="cert">{{cite web | url=http://www.musictvprogram.com/corner-ranking-single.html | title=Corner Ranking Single - RIAJ |publisher=Music TV Program | accessdate=18 April 2015}}</ref> According to Oricon, the song was Utada's fourth million selling single.{{efn|group=upper-alpha|These singles are also her million-selling certified singles:
* {{cite web|url=http://www.riaj.or.jp/issue/record/1999/199905.pdf |title=Automatic/Time Will Tell certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140122064612/http://www.riaj.or.jp/issue/record/1999/199905.pdf |archivedate=January 22, 2014 }}
* {{cite web|url=http://www.riaj.or.jp/issue/record/2000/200001.pdf |title=Moving on Without You certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140117114148/http://www.riaj.or.jp/issue/record/2000/200001.pdf |archivedate=January 17, 2014 }}
* {{cite web|url=http://www.riaj.or.jp/issue/record/2000/200007.pdf |title=Wait & See (Risk) certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20131229020125/http://www.riaj.or.jp/issue/record/2000/200007.pdf |archivedate=December 29, 2013 }}
* {{cite web|url=http://www.riaj.or.jp/issue/record/2000/200009.pdf |title=For You/Time Limit certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140122032609/http://www.riaj.or.jp/issue/record/2000/200009.pdf |archivedate=January 22, 2014 }}
* {{cite web|url=http://www.riaj.or.jp/issue/record/2001/2001_04.pdf |title=Can You Keep a Secret? certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140117063934/http://www.riaj.or.jp/issue/record/2001/2001_04.pdf |archivedate=January 17, 2014 }}
* {{cite web|url=http://www.riaj.or.jp/issue/record/2002/200203.pdf |title=Traveling certification |work=Oricon |accessdate=July 2, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140116130742/http://www.riaj.or.jp/issue/record/2002/200203.pdf |archivedate=January 16, 2014 }}
* {{cite web|url=https://www.riaj.or.jp/data/others/gold/200312.html|title=Colors certification|work=Oricon|accessdate=July 2, 2015}}}}{{efn|group=upper-alpha|According to [[Oricon]], the songs "For You", "Time Limit", "Travelling" and "Colors" did not sell over one million units in Japan but was certified Million by [[Recording Industry Association of Japan]] (RIAJ)}}

"Addicted to You" reached number one on the Japanese [[Count Down TV]] Chart for two weeks, and remained on the chart for thirteen weeks.<ref name="tbs">{{cite web|work=[[Count Down TV]] |publisher=[[TBS (Japan)|TBS]] |accessdate=May 12, 2015 |url=http://www.tbs.co.jp/cdtv/songdb/song4298-j.html |title=CDTV PowerWeb:! Utada Hikaru - Addicted to You |archiveurl=https://web.archive.org/web/20060620141248/http://www.tbs.co.jp/cdtv/songdb/song4298-j.html |archivedate=June 20, 2006 |deadurl=unfit }}</ref> This was her fourth consecutive number one on the chart, following "[[Automatic/Time Will Tell]]",<ref>{{cite web|work=[[Count Down TV]]|publisher=[[TBS (Japan)|TBS]]|accessdate=May 12, 2015|url=http://www.tbs.co.jp/cdtv/songdb/song3399-j.html|title=CDTV PowerWeb:! Utada Hikaru - Automatic / Time Will Tell|archiveurl=https://web.archive.org/web/20060620053426/http://www.tbs.co.jp/cdtv/songdb/song3399-j.html|archivedate=June 20, 2006 }}</ref> "[[Movin' on Without You]]",<ref>{{cite web|work=[[Count Down TV]]|publisher=[[TBS (Japan)|TBS]]|accessdate=May 12, 2015|url=http://www.tbs.co.jp/cdtv/songdb/song3558-j.html|title=CDTV PowerWeb:! Utada Hikaru - Movin' on Without You|archiveurl=https://web.archive.org/web/20060621044124/http://www.tbs.co.jp/cdtv/songdb/song3558-j.html|archivedate=June 21, 2006}}</ref> and "[[First Love (Hikaru Utada song)|First Love]]".<ref>{{cite web|work=[[Count Down TV]]|publisher=[[TBS (Japan)|TBS]]|accessdate=May 12, 2015|url=http://www.tbs.co.jp/cdtv/songdb/song3745-j.html|title=CDTV PowerWeb:! Utada Hikaru - First Love|archiveurl=https://web.archive.org/web/20060620053456/http://www.tbs.co.jp/cdtv/songdb/song3745-j.html|archivedate=June 20, 2006}}</ref> In the Annual 1999 Count Down TV chart, "Addicted to You" was placed at number five.<ref name="annual">{{cite web|work=[[Count Down TV]]|publisher=[[TBS (Japan)|TBS]]|url=http://www.tbs.co.jp/cdtv/cddb/countdown1999total-j.html|title=CDTV PowerWeb:! 1999 Annual List|archiveurl=https://web.archive.org/web/20061009100833/http://www.tbs.co.jp/cdtv/cddb/countdown1999total-j.html|archivedate=12 April 2006}}</ref>

==Promotion and other appearances==
[[Wataru Takeishi]] directed the accompanying [[music video]], which was filmed in [[Hong Kong]] and featured Utada inside a nightclub.<ref name="video">{{cite web | url=https://www.youtube.com/watch?v=V_In-3UlkEk | title=宇多田ヒカル - Addicted to You (UP-IN-HEAVEN MIX) |publisher=[[YouTube]] | language=Japanese | accessdate=28 April 2015}}</ref> The music video was included on her ''Single Clip Collection Vol. 1'' (1999).<ref>{{cite AV media notes|title=Single Clip Collection Vol. 1|others=Utada Hikaru|publisher=EMI Music Japan|type=DVD compilation}}</ref> "Addicted to You" has been included in four of Utada's live Japanese tours concert: ''Bohemian Summer 2000'', ''Utada Unplugged'', ''Utada in Bukodan 2004-2005'', and ''[[Utada United 2006]]''. The live versions were then released on a live DVD for each tour.<ref>{{cite AV media notes|title=Bohemian Summer 2000|others=Utada Hikaru|publisher=[[EMI Music Japan]]|year=2001|type=Live DVD|id=TOBF-5060}}</ref><ref>{{cite AV media notes|title=Utada Unplugged|others=Utada Hikaru|publisher=EMI Music Japan|year=28 November 2001|type=Live DVD|id=TOBF-5508}}</ref><ref>{{cite AV media notes|title=Utada United 2006|others=Utada Hikaru|publisher=EMI Music Japan|year=20 December 2006|type=Live DVD|id=TOBF-5506}}</ref><ref>{{cite AV media notes|title=Utada in Bukodan 2004-2005|others=Utada Hikaru|publisher=EMI Music Japan|type=Live DVD|id=TOBF-5509}}</ref>

==Formats and track listings==
These are the formats and track listings of major single releases of "Addicted to You".

*"'''CD Single'''"
#"Addicted to You" (Up-in-Heaven remix) &mdash; 5:19
#"Addicted to You" (Underwater remix) &mdash; 6:17
#"Addicted to You" (Up-in-Heaven remix) (Instrumental) &mdash; 4:08
#"Addicted to You" (Underwater remix) (Instrumental) &mdash; 5:21

==Personnel==
* Hikaru Utada&nbsp;– songwriting, composition
* Jerry Jam and Terry Lewis&nbsp;– arrangement, instruments, production, composition
* Teruzane Utada&nbsp;– arrangement, production, composition
* Akira Miyake&nbsp;– arrangement, production, composition
* Steve Hodge&nbsp;– guitar, recording
* Alex Richbourg&nbsp;– drums
* Darnell Davis&nbsp;– keyboards
* Indoh Mitsuhiro&nbsp;– recording
* Ugajin Masaaki&nbsp;– recording
* Richard Avedon&nbsp;– recording

Credits adapted from the promotional CD single.<ref name="note"/>

==Charts, peaks and positions==
{{col-begin}}
{{col-2}}

===Charts===
{| class="wikitable plainrowheaders"
!Chart (1999)
!Peak<br>position
|-
!scope="row"|Japan Weekly Chart ([[Oricon]])<ref name="chart"/>
| style="text-align:center;"|1
|-
!scope="row"|Japan [[Count Down TV]] Chart ([[Tokyo Broadcasting System|TBS]])<ref name="tbs"/>
| style="text-align:center;"|1
|-
!scope="row"|Japan Annual [[Count Down TV]] Yearly Chart ([[Tokyo Broadcasting System|TBS]])<ref name="annual"/>
| style="text-align:center;"|5
|-
|}
{{col-2}}

===Certification===
{{Certification Table Top}}
{{Certification Table Entry |region=Japan |artist=Utada Hikaru |title=Addicted to You |type=single |award=Million |certref=<ref name="cert"/> |relyear=1999 |autocat=yes}}
{{Certification Table Bottom}}
{{col-end}}

==Notes==
{{notelist-ua}}

== References ==
{{reflist|2}}

{{Utada}}
{{Utada singles}}

[[Category:Hikaru Utada songs]]
[[Category:1999 singles]]
[[Category:Oricon Weekly number-one singles]]
[[Category:Songs written by Hikaru Utada]]
[[Category:Song recordings produced by Jimmy Jam and Terry Lewis]]
[[Category:EMI Music Japan singles]]
[[Category:1999 songs]]