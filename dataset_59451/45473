{{EngvarB|date=May 2015}}
{{Use dmy dates|date=May 2015}}
{{Infobox military person
| honorific_prefix  = The Honourable
| name              = John Eric Loverseed
| honorific_suffix  = {{post-nominals|post-noms=MP AFC}}
| native_name       = 
| native_name_lang  = 
| image         = 
| caption       = 
| birth_date    = {{Birth date|df=yes|1910|12|04}}
| death_date    = 1962
| birth_place   = [[Downham]], Norfolk
| death_place   = [[Biggleswade]], Bedfordshire
| placeofburial = 
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname      = 
| birth_name    = 
| allegiance= {{flag|United Kingdom}}
| branch= {{air force|United Kingdom}}<br>[[File:Flag of Spain 1931 1939.svg|24px]] [[Republican Spanish Air Force]]
| serviceyears  = RAF 1929–1935<br>RSAF 1937–1938<br> RAF 1939–1943
| rank          = [[Flying Officer]]<br>[[Warrant Officer]]
| servicenumber = 907964
| unit          =
*[[Republican Spanish Air Force]]
*[[No. 1 Anti-Aircraft Co-operation Unit]]
*[[No. 501 Squadron RAF|501 Squadron]]
| commands      = 
| battles       = [[Spanish Civil War]]<br>[[World War II]]
*[[Battle of France]]
*[[Battle of Britain]]
| battles_label = 
| awards        = [[File:UK AFC ribbon.svg|30px]] [[Air Force Cross (United Kingdom)|Air Force Cross]]
| relations     =
*Father – [[John Frederick Loverseed]]
*Son – [[Bill Loverseed|Raymond Eric William Loverseed]]
| laterwork     = 
| signature     = 
| website       = <!-- {{URL|example.com}} -->
}}
'''John Eric Loverseed''' {{post-nominals|size=100%|country=GBR-cats|AFC}} (4 December 1910 – 1962) was a pilot who flew with the [[Royal Air Force]] in 1930s, with Republican forces in the [[Spanish Civil War]] in 1937/38, and with the RAF again during the [[Battle of Britain]]. In 1943 he was elected as a wartime [[Member of Parliament|MP]] for the [[Common Wealth Party]]. He was later a co-founder of the pacifist [[Fellowship Party]].<ref>[http://www.ww2awards.com/person/49704 WW2 Awards John Eric Loverseed]</ref>

==Early and private life==
Loverseed was born in [[Downham]] in Norfolk, the son of [[Liberal Party (UK)|Liberal]] politician and former MP for [[Sudbury (UK Parliament constituency)|Sudbury]], [[John Frederick Loverseed]] (1881–1928), and his wife Catherine Annie (Kitty) (née Thurman).

Loverseed was educated at [[Sudbury Grammar School]]. He joined the RAF on a [[short service commission]] in 1929, and was commissioned as a probationary [[Pilot Officer]],<ref>http://www.london-gazette.co.uk/issues/33489/pages/2765</ref> was confirmed in the rank of Pilot Officer in April 1930,<ref>http://www.london-gazette.co.uk/issues/33605/pages/2967</ref> and promoted to [[Flying Officer]] in October 1930.<ref>http://www.london-gazette.co.uk/issues/33662/pages/7339</ref>  He served in the Middle East, and was posted to [[Heliopolis (Cairo suburb)|Heliopolis]] in March 1931. His son, Bill, was born in Egypt in 1932. He left the RAF and was transferred to the RAF officer reserve in July 1935.<ref>http://www.london-gazette.co.uk/issues/34302/pages/4317</ref>

He served as a pilot in the [[Spanish Republican Air Force]] during the [[Spanish Civil War]] from December 1936 to February 1937, and was injured in January 1937. He was removed from the RAF reserve in December 1937.<ref>http://www.london-gazette.co.uk/issues/34470/pages/38</ref>

He married five times, first to Pamela; then to Ethel Maud Hill, having a son and a daughter; then to Gladys Male, having two daughters and a son; then to Georgina June Mary Matthews having one son, John Frederick, born 10 January 1956; and finally, on 9 September 1962, to Joan Buckland.

==Second World War==
He rejoined the RAF in November 1939, as a [[non-commissioned officer]]. After a refresher flying course, he was posted to [[No. 1 Anti-Aircraft Co-operation Unit]] (AACU) on 20 May 1940. The AACU towed targets for anti-aircraft practice.

The following day, he was posted [[No. 501 Squadron RAF|501 Squadron]], which had been deployed to airfields in France as part of the [[RAF Advanced Air Striking Force|Advanced Air Striking Force]] (AASF) providing air support for the [[British Expeditionary Force (World War II)|British Expeditionary Force]]. He was injured when his [[Hawker Hurricane]] crash-landed on 31 May 1940, and was evacuated to a hospital in England. By the time he recovered and returned to his unit on 19 July 1940, 501 Squadron had also returned to England following the [[Fall of France]].

He took part in six operational sorties in the [[Battle of Britain]] before being posted back to 1 AACU on 19 August 1940. He had been promoted to the rank of [[Warrant Officer]] by the time he was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]] on 1 January 1943.<ref>http://www.london-gazette.co.uk/issues/35841/pages/34</ref>

==Political career==
He stood as the [[Common Wealth Party]] candidate for [[Eddisbury (UK Parliament constituency)|Eddisbury]] in Cheshire in the by-election in April 1943 caused by the death of sitting [[National Liberal Party (UK, 1931)|National Liberal]] MP [[Richard John Russell]]. The Common Wealth Party had been founded in July 1942 by the alliance of two left-wing groups, the [[1941 Committee]] and the neo-Christian [[Forward March movement]], standing on a radical "libertarian socialist" platform. It opposed the electoral pact established by the [[Conservative Party (UK)|Conservative]], [[Labour Party (UK)|Labour]], and [[Liberal Party (UK)|Liberal Parties]] in the national wartime coalition government, who agreed that MPs filling casual parliamentary vacancies should be returned unopposed, and was one of several small parties who put up opposing candidates in wartime by-elections.

Russell had won the Eddisbury seat as a Liberal in 1929, and retained it as a National Liberal since 1931. Taking advantage of the fact that the National Liberal candidate, Thomas Peacock, was thought to have Conservative leanings,<ref>"6 Candidates at Eddisbury, Liberal Dissensions, Official Nominee To Be Opposed", ''The Times'', 11 March 1943, p.2</ref> Loverseed downplayed his party's commitment to common ownership, and emphasised its liberal policies.

Peacock campaigned against Loverseed using the slogan "Hitler is watching Eddisbury".<ref>[https://books.google.com/books?id=MQGAXGB4GLkC&pg=PA132 ''By-elections in British politics''] Chris Cook, John Ramsden, Routledge, 1997, ISBN 1-85728-535-2, p.132.</ref> Other potential candidates withdrew leaving one other candidate – H Heathcote Williams, an Independent Liberal. Loverseed unexpectedly won the by-election with 8,023 votes, a majority of 486,<ref>"Common Wealth Gain Eddisbury, Election Result, Poll Unexpectedly High", ''The Times'', 9 April 1943, p.2</ref> and was discharged from the RAF at his own request to serve in Parliament. He was the first Common Wealth Party candidate to win an election, and joined its only other MP, Sir [[Richard Acland]]. Two others followed at by-elections in 1944 – [[Hugh Lawson (British politician)|Hugh Lawson]] in [[Skipton (UK Parliament constituency)|Skipton]] and [[Ernest Millington]] in [[Chelmsford (UK Parliament constituency)|Chelmsford]].  [[Olaf Stapledon]] described Loverseed's victory as offering "a fresh inspiration in political life at a time when it was greatly needed."<ref>Quoted in ''[https://books.google.com/books?id=KYJt7s6SI-QC&pg=PA305 Olaf Stapledon: speaking for the future]'', Robert Crossley, Robert Crossley, Syracuse University Press, 1994, ISBN 0-8156-0281-2,p.305.</ref>

Loverseed left the Common Wealth Party in November 1944, becoming an independent, and then joining the [[Labour Party (UK)|Labour Party]] in May 1945.<ref name="craig">[[F. W. S. Craig]], ''Minor Parties at British Parliamentary Elections''</ref> He stood in Eddisbury again at the [[United Kingdom general election, 1945|1945 general election]], this time polling less than half the votes of [[National Liberal Party (UK, 1931)|National Liberal]] candidate, [[Sir John Barlow, 2nd Baronet]], who took the seat with 15,294 votes and a majority of 7,902.<ref>Dod's Parliamentary Companion, 1945 p419</ref> He was expelled from the Labour Party in July 1945.<ref>Expelled From Labour Party, ''The Times'', 30 July 1945</ref>

In [[United Kingdom general election, 1955|May 1955]] he stood against [[Herbert Morrison]] unsuccessfully for [[Lewisham South (UK Parliament constituency)|South Lewisham]] as an Independent [[Pacifist]].<ref>[http://www.bbm.org.uk/Loverseed.htm Battle of Britain London Monument – Sgt. J E LOVERSEED]</ref>

In June 1955, he was a co-founder of the pacifist [[Fellowship Party]], which claimed to be the oldest environmentalist party in Britain; it dissolved in 2007.<ref>[http://www.independent.co.uk/news/obituaries/ron-mallone-pacifist-campaigner-who-founded-the-fellowship-party-1690490.html Ron Mallone: Pacifist campaigner who founded the Fellowship Party], The Independent, 25 May 2009</ref>

==Son==
His son, [[Bill Loverseed|Raymond Eric William Loverseed]] was born on an RAF base in Egypt in 1932. Bill joined the RAF in 1952,<ref name="london-gazette.co.uk">http://www.london-gazette.co.uk/issues/45700/supplements/7086</ref> and flew with the [[Red Arrows]] in their first year, 1965, and also in 1970.<ref>[http://www.raf.mod.uk/reds/displayinfo/rollofhonour.cfm Red Arrows Roll of Honour]</ref> He took command of the Red Arrows in 1971 after the previous leader, [[Dennis Hazell]], broke his leg after ejecting due to an engine failure in practice in November 1970. Four Red Arrows' pilots were killed in an accident at [[RAF Kemble]] in January 1971, when two planes carrying two men each collided in mid-air. Bill Loverseed was promoted to Squadron Leader in July 1971,<ref>http://www.london-gazette.co.uk/issues/45417/supplements/7208</ref> but resigned his commission in May 1972.<ref name="london-gazette.co.uk"/> He married four times. He flew a [[de Havilland Canada DHC-5 Buffalo|Buffalo]] transport plane that crash-landed at the [[Farnborough Air Show]] in 1984, and a [[Piper Cherokee]] aircraft that suffered severe icing and crashed in Newfoundland in 1987.  He died in 1998, on a [[Dash 7]] that he was piloting on a test flight over Devon.<ref>[http://news.bbc.co.uk/1/hi/uk/224181.stm Red Arrows ace killed in crash], BBC News, 29 November 1998</ref>

==References==
{{div col}}
<references/>
{{div col end}}

== External links ==
* {{Hansard-contribs | mr-john-loverseed | John Loverseed }}

{{s-start}}
{{s-par|uk}}
{{succession box
 | title  = Member of Parliament for [[Eddisbury (UK Parliament constituency)|Eddisbury]]
 | years  = [[Eddisbury by-election, 1943|1943]] – [[United Kingdom general election, 1945|1945]]
 | before = [[Richard John Russell]]
 | after  = [[Sir John Barlow, 2nd Baronet|Sir John Barlow]]
}}
{{s-end}}

{{DEFAULTSORT:Loverseed, John Eric}}
[[Category:1910 births]]
[[Category:1962 deaths]]
[[Category:Labour Party (UK) MPs for English constituencies]]
[[Category:UK MPs 1935–45]]
[[Category:Common Wealth Party MPs]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:British people of the Spanish Civil War]]
[[Category:The Few]]
[[Category:People educated at Sudbury Grammar School]]
[[Category:Royal Air Force airmen]]