{{Infobox journal
| title         = Painted Bride Quarterly
| cover         = 
| editor        = Kathleen Volk Miller and Marion Wrenn
| discipline    = [[literary magazine]] 
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = l
| publisher     =
| country       = 
| frequency     = Quarterly
| history       = 1973 to present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://pbqmag.org
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0362-7969
| eISSN         = 
| boxwidth      = 
}}
{{redirect|PBQ|Petabecquerel|Becquerel}}
The '''''Painted Bride Quarterly''''', also known informally as '''''PBQ''''', is a Philadelphia-based [[literary magazine]]. It was established in 1973 by Louise Simons and R. Daniel Evans in connection with the [[Painted Bride Art Center]], an art gallery founded in 1969 in an old bridal shop on [[South Street (Philadelphia)|South Street]] in [[Philadelphia, Pennsylvania]].  The journal is supported by [[Drexel University]] in Philadelphia.  It is staffed by a mix of volunteer editors and changing student staff.  The magazine is published quarterly online and yearly in print.  The magazine, which sees itself as "literary forum for poetry, fiction, prose, essays, interviews and photography", has a dual-city editorial staff in Philadelphia and New York. ''PBQ'' has featured works by such poets as [[Charles Bukowski]], [[Etheridge Knight]], [[Sonia Sanchez]], [[Amiri Baraka]], [[Simon Perchik]], [[Yusef Komunyakaa]], [[Gregory Pardlo]], and [[Major Jackson]], among others. 

''PBQ'' is the only magazine of its longevity to have a complete [http://pbqmag.org/issues/past-issues/1999-2016/ archive] of its history available online.

==History==
In 1969, Gerry Givnish and a group of his artist friends founded a gallery space on Philadelphia's [[South Street (Philadelphia)|South Street]] in an old bridal shop.  The cooperative gallery began a cultural revolution in the area.  Soon, dozens of art galleries blossomed, but none as provocative, daring or enduring as theirs. 

The group of artists staked their claim on their patch of street with a window display. Among the remains of the bridal shop was a mannequin, made up for some hellish ‘60s wedding she would never attend. The artists took her straight to the storefront to cause a scene, but not in a typical virginal, white wedding gown. Soon, the "[[Painted Bride]]" became a [[South Street (Philadelphia)|South Street]] attraction. People would stop by just to see what provocative outfit or lewd position she’d be in that day. She was the icon and namesake of the art center that, in 1973, gave birth to ''Painted Bride Quarterly''.

== Events ==
''PBQ'' holds interactive prose and poetry events to make its presence known in the Philadelphia area.  On the last Thursday of the month, the journal hosts a [[poetry slam]] at the [[Pen & Pencil Club]] called "Slam, Bam, Thank You, Ma’am."  Monthly poetry readings are held in both Philadelphia and New York, featuring local poets and musicians.

On May 13, 2009, ''PBQ'' held its first annual Bookfair for Literacy, raising funds for Philadelphia Reads and donating the remaining books to [[Books Through Bars]].   More than twenty regional presses and literary magazines attended and sold their wares at below costs.  For a few months prior to the event, books were donated by members of the Drexel community, and then sold with all proceeds going to Philadelphia Reads. The Second Annual Bookfair for Literacy was held on Friday, May 21, 2010. 

== Awards and honors ==
''PBQ'' has had many of its first-published poems chosen for [[Pushcart Prize]]s, on ''Poetry Daily'', and other accolades, such as inclusion in ''Online Writing: The Best of the First Ten Years''.

==See also==
*[[Painted Bride Art Center]]

==References==
{{Reflist}}
{{unreferenced|date=July 2010}}

==External links==
*Finding aid for the [http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_MsColl517 Painted Bride Quarterly records] from the [http://www.library.upenn.edu University of Pennsylvania Libraries]

{{Drexel University|state=collapsed}}

[[Category:American literary magazines]]
[[Category:American quarterly magazines]]
[[Category:Magazines established in 1973]]
[[Category:Media in Philadelphia]]
[[Category:Magazines published in Pennsylvania]]