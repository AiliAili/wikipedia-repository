<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox aircraft begin
 | name=V-Liner
 | image=
 | caption=
}}{{Infobox aircraft type
 | type=Advertising display aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=[[Slingsby Aviation|Slingsby Aircraft]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=Prototype destroyed before completion
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=0
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}
The '''Slingsby CAMCO V-Liner''' was a proposed advertising aircraft of unusual design of the 1960s. It was intended to display advertising using electric lights to be viewed at a long distance, and as such consisted of a long triangular frame carried between two sets of wings and fuselages. The prototype V-Liner was destroyed in a factory fire before completion and the type was abandoned.

==Design and development==

In the late 1960s, the American company [[Central Aircraft Manufacturing Company Inc.]] (CAMCO) was interested in means of [[aerial advertising]]. A requirement was identified for an 18-letter message to be displayed by means of electric lights, which would be easily readable at a range of 2–3 miles (3–5&nbsp;km).<ref name="JAWA69 p226"/> The use of [[blimp]]s (non-rigid [[airship]]s) was not considered economical,<ref name="flightcamco">''Flight International'' 12 September 1968, p. 420.</ref> while a fixed wing aircraft carrying such a message would be much longer than any ever built.<ref name="JAWA69 p226"/>

CAMCO decided that a long rigid triangular framework of aluminium tubing would be used to carry the display lighting, and that it would be carried between separate sets of wings and fuselages at the front and rear of the framework. The aircraft's crew would consist of two; one in each of the fuselages, with the aircraft normally flown from the forward fuselage and the display electronics controlled from the rear. Each fuselage would have a set of [[monoplane]] wings, which were all-moving so that they could be used as control surfaces, while the aircraft was to be powered by two engines carried on pylons above the front fuselage and its wings. Production aircraft were planned to be [[amphibious aircraft|amphibious]], although the prototype would not be.<ref name="JAWA69 p226"/><ref name="flightcamco"/><ref name="flightfarn">''Flight International'' 19 September 1968, p. 447.</ref>

CAMCO chose Slingsby Aircraft of [[Kirbymoorside]], England to build the new aircraft, called the V-Liner, in 1968. A prototype was expected to fly in late 1969,<ref name="JAWA69 p226"/> while it was hoped to build 42 in four years, with several hundred built over a longer period. V-Liners would be available only for lease from CAMCO, not for sale. Variants would be the "CV2 Video Liner", "CV3 Vector" and "CV4 Victory Liner".<ref name="flightfarn"/>

Work began on construction of the first prototype, but the Slingsby factory was wrecked by a fire on 18 November 1968, and the partly built prototype was badly damaged.<ref name="Jackv1 p386-7">Jackson 1974, pp. 386–387.</ref> The fire forced Slingsby into [[receivership]], and although the company was rescued by [[Vickers Limited]] and resumed glider production, work on the V-Liner was abandoned.<ref name="Flightsling">''Flight International'' 13 November 1969, p. 735.</ref>

==Specifications==
{{Aircraft specs
|ref=Jane's All The World's Aircraft 1969–70<ref name="JAWA69 p226">Taylor 1969, p. 226.</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=two
|capacity=
|length m=
|length ft=378
|length in=0
|length note=
|span m=
|span ft=69
|span in=6
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=6.95:1<!-- give where relevant eg sailplanes  -->
|airfoil=NACA 0018
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=12000
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Pratt & Whitney Canada PT6]]A-20
|eng1 type=[[turboprop]]s
|eng1 kw=<!-- prop engines -->
|eng1 hp=579<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=<ref>Other sources (e.g.Jackson 1974, pp. 385–386) specify two [[Continental O-580|Rolls-Royce Continental GTSIO-580D]] piston engines with a 100 hp (75 kW) APU powering the display equipment.</ref>
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=69
|max speed kts=
|max speed note=(ferrying speed)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=53
|cruise speed kts=
|cruise speed note=(operating speed)
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=40<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns=
|bombs=
|rockets=
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*[http://www.flightglobal.com/pdfarchive/view/1968/1968%20-%202035.html "Twenty-Sixth SBAC Show: Slingsby CAMCO V-Liner"]. ''Flight International'', 19 September 1968, pp.&nbsp;446.
*[http://www.flightglobal.com/pdfarchive/view/1968/1968%20-%201960.html "The CAMCO V-Liner"]. ''[[Flight International]]'', 12 September 1968, p.&nbsp;420.
* Jackson, A.J. ''British Civil Aircraft since 1919''. London: Putnam, 1974. ISBN 0-370-10006-9.
*[http://www.flightglobal.com/pdfarchive/view/1969/1969%20-%203187.html "Private Flying: Slingsby in Business Again"]. ''Flight International'', 13 November 1969, p.&nbsp;735.
*Taylor, John W. R.. ''Jane's All The World's Aircraft 1969–70''. London: Sampson Low, Marston & Company, 1969. ISBN 0-354-00051-9.

<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->

==External links==
*[http://www.flightglobal.com/pdfarchive/view/1968/1968%20-%201849.html Picture of model with model Boeing 707 for scale]
*[http://www.britishpathe.com/record.php?id=45450 British Pathe film of the V-liner model displayed at SBAC ] <!-- about halfway through-->
*[http://www.freepatentsonline.com/3614033.html CAMCOs patent application for the aircraft design]
<!-- Navboxes go here -->

{{Slingsby aircraft}}

[[Category:Slingsby aircraft|CAMCO V-Liner]]
[[Category:Tandem-wing aircraft]]