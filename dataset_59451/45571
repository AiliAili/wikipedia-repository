{{Multiple issues|
{{notability|Biographies|date=November 2016}}
{{unreliable sources|date=November 2016}}
}}

{{Infobox military person
|name=Wilhelm Schilling
|birth_date={{birth date|1915|1|30|df=y}}
|death_date={{death date and age|2000|3|14|1915|1|30|df=y}}
|image=
|birth_place= [[Kamieniec Ząbkowicki|Kamenz]], [[Lower Silesia]], [[Germany]] (now [[Poland]])
|death_place=[[Germany]]
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1939–1945
|rank=[[Oberleutnant]]
|unit=[[JG 54]]
|battles=[[World War II]]
*[[Eastern Front (World War II)|Eastern Front]]
*[[Western Front (World War II)|Western Front]]
*[[Battle of France]]
*[[Battle of Britain]]
*[[Operation Barbarossa]]
*[[Defence of the Reich]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Wilhelm Schilling''' (30 January 1915 – 14 March 2000) was a [[Luftwaffe]] [[fighter ace]] and recipient of the [[Knight's Cross of the Iron Cross]] during [[World War II]]. The Knight's Cross of the Iron Cross was awarded to recognise extreme battlefield bravery or successful military leadership - for the fighter pilots, it was a measure of skill and combat success. He was credited with at least 50 victories in 538 missions.

==Career==

Upon finishing flight training, Wilhelm Schilling joined 3./JG 21 as an ''[[Unteroffizier]]''. He gained his first victory during the [[Battle of France]], shooting down a [[Hawker Hurricane|Hurricane]] fighter on 12 May 1940 over [[Brussels]]. In the ensuing [[Battle of Britain]] (by which time his unit had been renamed 9./JG 54) he added three more victories: a [[Spitfire]] on 12 August 1940, another on 5 September near [[Ashford, Kent|Ashford]], and a [[Bristol Blenheim]] bomber over the North Sea on 8 November.

The unit was then transferred to the Balkans and Bucharest,<ref>Luftwaffe Air Units: Single–Engined Fighters website.</ref> before going to the [[Eastern Front (World War II)|Eastern Front]] to prepare for [[Operation Barbarossa]]. His first victory of the campaign was on the second day, 23 June 1941, when he shot down a Russian [[Tupolev SB|SB-2]] bomber. III./JG 54 was based in the north covering the advance to, and siege of, Leningrad. By the end of the year, he had 17 victories. He hadn't advanced his score by 14 February 1942, when he was seriously wounded by Russian ground fire. Returning to the Leningrad Front in March 1942, he was awarded the ''Ehrenpokal'' (Goblet of Honour) on 1 July and the German Cross in Gold on 4 August. But he was wounded again by anti-aircraft fire, on September 16, 1942 over Dubrovka in [[Bf 109|Bf 109 G-2]] "Yellow 3" after shooting down an Il-2 bomber. While recovering in hospital at the front he was awarded the ''Ritterkreuz'' on 10 October for his 46 victories at the time.<ref>Luftwaffe Officer Career Summaries website.</ref><ref>Weal 2001, p. 61</ref>

In February 1943, III./JG 54 was withdrawn from the Eastern Front to [[Defence of the Reich]] duties, in a misguided attempt to rotate the fighter units between west and east. But for the pilots of III./JG 54, used to low-level combats, being thrown in against the [[United States Army Air Forces]] (USAAF) four-engined bombers at high altitude it was a disaster. In its first two missions the unit lost 15 planes and some of its best pilots. However, they persisted and things gradually improved.<ref>Sundin 1997, p. 74</ref> Now commissioned as an officer, on 1 August Schilling was promoted to ''Oberleutnant'',<ref>Luftwaffe Officer Career Summaries website.</ref> and a month later he was promoted to ''[[Staffelkapitän]]'' (Squadron Leader) of 9./JG 54 in September 1943.

On 20 February 1944, the [[8th Air Force]] started its [[Big Week]] against the Reich's industry. By now Schilling had added a further four victories, including three four-engined bombers. During the week III./JG 54 was heavily engaged, suffering considerable losses but inflicting only light damage on the bomber formations. On the first day, February 20, he was seriously injured over Dehnsen/Alfeld while flying [[Bf 109|Bf 109 G-6]] "Yellow 1" (''Werknummer'' 440141 — factory number) but made a successful forced-landing.

After his recovery, he was transferred back to the Eastern Front, as ''Staffelkapitän'' of 5./JG 54, on 4 May 1944. With the siege of [[Leningrad]] finally lifted, they were now based in Estonia. In June II./JG 54 was transferred across to Finland as combat support over the Karelian isthmus as the Soviets launched an attack to try and knock Finland out of the war. By July, they were back in Estonia with intense air activity, as another Soviet offensive aimed to turn the flanks of Army Group North either end of [[Lake Peipus]]. Then, on 22 August, he was finally transferred back to the Reich, as a fighter-instructor in 1./[[Ergänzungs-Jagdgruppe Ost]] (renamed 1./ErgJG Nord on 1 September<ref>Luftwaffe Air Units: Single–Engined Fighters website, EJGr Ost.</ref>). On 15 January 1945 he was made ''Staffelkapitän'' of the training unit 4./EJG 1,(renamed 10./EJG 1 in April 1945<ref>Luftwaffe Air Units: Single–Engined Fighters website, EJG 1.</ref>) where he served till the war's end.<ref>Weal 2006, p. 93</ref><ref>Luftwaffe Officer Career Summaries website.</ref>

Wilhelm Schilling was credited with 50 victories (plus 13 more unconfirmed) in 538 missions, 41 of them on the [[Eastern Front (World War II)|Eastern Front]].

==Awards==
* [[Iron Cross]] (1939)
** 2nd Class
** 1st Class
* [[Wound Badge]] in Black or Silver
* [[Front Flying Clasp of the Luftwaffe]] in Gold with Pennant
* [[Ehrenpokal der Luftwaffe]] (1 July 1942)
* [[German Cross]] in Gold on 4 August 1942 as ''[[Oberfeldwebel]]'' in the III./Jagdgeschwader 54<ref>Patzwall & Scherzer 2001, p. 406.</ref>
* [[Knight's Cross of the Iron Cross]] on 10 October 1942 as ''Oberfeldwebel'' and pilot in the 9./[[Jagdgeschwader 54]]<ref>Fellgiebel 2000, p. 377.</ref>{{refn|According to Scherzer on 3 November 1942 as pilot in the III./[[Jagdgeschwader 54]].<ref>Scherzer 2007, p. 662.</ref>|group="Note"}}

==Notes==
{{Reflist|group="Note"}}

==References==
; Notes
{{Reflist|30em}}
; Bibliography
{{Refbegin}}
* Barbas, Bernd (1985). ''Planes of the Luftwaffe Fighter Aces Vol II''. Kookaburra Technical Publishing. 	ISBN 0-85880-050-0, with picture p.&nbsp;163
* Bergström. Christer; Dikov, Andrey; Antipov, Vlad (2006). ''Black Cross, Red Star Vol 3''. Eagle Editions Ltd ISBN 0-9761034-4-3, with picture p.&nbsp;235
* Fellgiebel W.P., Elite of the Third Reich, The recipients of the Knight's Cross of the Iron Cross 1939-1945: A Reference, Helion & Company Limited, Solihull, 2003, ISBN 1-874622-46-9
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |last2=Scherzer
  |first2=Veit 
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* Patzwall K.D., Der Ehrenpokal für besondere Leistung im Luftkrieg, Studien zur Geschichte der Auszeichnungen, Band 6, Verlag Klaus D. Patzwall, Norderstedt, 2008, ISBN 978-3-931533-08-3
* Sundin, Claes & Bergström. Christer (1997). ''Luftwaffe Fighter Aircraft in Profile''. Atglen, PA: Schiffer Military History. ISBN 0-7643-0291-4 including colour profile of aircraft (#64)
* Weal, John (2001). ''Aviation Elite Units #6: Jagdgeschwader 54 ‘Grünherz’''. Oxford: Osprey Publishing Limited. ISBN 1-84176-286-5 including colour profile of aircraft (#29)
* Weal, John (1999). ''Bf109F/G/K Aces of the Western Front''. Oxford: Osprey Publishing Limited. ISBN 1-85532-905-0 including colour profile of aircraft (#39)
* Weal, John (2006). ''Bf109 Defence of the Reich Aces''. Oxford: Osprey Publishing Limited. ISBN 1-84176-879-0 including colour profile of aircraft (#31)
{{Refend}}

{{Knight's Cross recipients of JG 54}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Schilling, Wilhekm}}
[[Category:1915 births]]
[[Category:2000 deaths]]
[[Category:People from Ząbkowice Śląskie County]]
[[Category:People from the Province of Silesia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]