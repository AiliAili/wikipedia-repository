{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Alaler Gharer Dulal
| title_orig    = 
| image         = Alaler Ghore Dulal.jpg 
| caption = 
| author        = [[Peary Chand Mitra]]
| illustrator   = 
| cover_artist  = 
| country       = [[India]]
| language      = [[Bengali language|Bengali]]
| series        = 
| genre         = [[Novel]]
| publisher     = 
| release_date  = 1857
| english_release_date =
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
98| preceded_by   = 
Anisuzzaman| followed_by   = 
}}
'''''Alaler Gharer Dulal''''' (Bengali: ''আলালের ঘরে দুলাল'',  published in 1857) is a [[Bengali language|Bengali]] novel by [[Peary Chand Mitra]] (1814-1883). The writer used the [[pseudonym]] ''Tekchand Thakur'' for this novel.

The novel describes the society of the nineteenth century Calcutta (now [[Kolkata]]), and the bohemian lifestyle of the protagonist named Matilal. The novel is a landmark in the history of Bengali language and [[Bengali literature]], as it used ''[[Bengali language#Spoken and literary variants|Cholitobhasa]]'' (colloquial form of the Bengali language) for the first time in print. The novel also happens to be one of the earliest Bengali novels.<ref>[[Hana Catherine Mullens]] wrote ''Phoolmani O Karunar Bibaran'' in 1852. This is regarded as the first novel in Bengali; ''Alaler Gharer Dulal'' was published in 1858, as per ''Sansad Bangali Charitabhidhan'' page 423. Harinath Mazumdar wrote a novel ''Bijay Basanta'' at the same time as per ''Ramtanu Lahiri O Tatkalin Banga Samaj'', page 88.</ref> The simple prose style introduced in the novel came to be known as "Alali language". The novel was first published serially in a monthly magazine, ''Masik Patrika''. Later, a dramatised version was staged at the [[Bengal Theatre]] (January 1875).

==Footnotes==
{{Reflist}}

==References==
* {{cite book |last=Huq |first=Mohammad Daniul |year=2012 |chapter=Alaler Gharer Dulal |chapter-url=http://en.banglapedia.org/index.php?title=Alaler_Gharer_Dulal |editor1-last=Islam |editor1-first=Sirajul |editor1-link=Sirajul Islam |editor2-last=Jamal |editor2-first=Ahmed A. |title=Banglapedia: National Encyclopedia of Bangladesh |edition=Second |publisher=[[Asiatic Society of Bangladesh]]}}

==External links==
* https://archive.org/details/TheSpoiltChild
{{Wikisourcelang|bn|আলালের ঘরের দুলাল|আলালের ঘরের দুলাল|}}

[[Category:1857 novels]]
[[Category:19th-century Indian novels]]
[[Category:Bengali literature]]
[[Category:Bengali-language novels]]
[[Category:Novels set in Kolkata]]


{{1850s-novel-stub}}