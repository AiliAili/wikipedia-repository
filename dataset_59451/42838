<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=MD-II
 | image=Frontier MD-II.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Frontier Aircraft Inc]]
 | designer=
 | first flight=
 | introduced=1990s
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=2
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]19,500 (kit, 1998)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Frontier MD-II''' was an [[United States|American]] [[homebuilt aircraft]] that was designed and supplied as a kit by [[Frontier Aircraft Inc]] of [[Vail, Colorado]], introduced in the 1990s.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 167. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The MD-II featured a [[strut-braced]] [[high-wing]], a two-seats-in-[[side-by-side configuration]] enclosed cockpit accessed via doors, fixed [[conventional landing gear]] with [[wheel pants]] and a single [[tractor configuration|tractor]] engine.<ref name="Aerocrafter" />

The aircraft was fabricated from [[2024-T3 aluminum]] sheet. Its {{convert|29.50|ft|m|1|abbr=on}} span wing employed a USA 35B [[airfoil]], mounted [[Flap (aircraft)|flaps]] and had an area of {{convert|177.0|sqft|m2|abbr=on}}. The cabin width was {{convert|41|in|cm|abbr=on}}. The acceptable power range was {{convert|65|to|200|hp|kW|0|abbr=on}} and the standard powerplant used was the {{convert|140|hp|kW|0|abbr=on}} [[Avia M 332]] (LOM) four cylinder, inverted, air-cooled, [[supercharged]], inline [[aircraft engine]].<ref name="Aerocrafter" />

With the Avia engine the MD-II had a typical empty weight of {{convert|1150|lb|kg|abbr=on}} and a gross weight of {{convert|2000|lb|kg|abbr=on}}, giving a useful load of {{convert|850|lb|kg|abbr=on}}. With full fuel of {{convert|43|u.s.gal}} the payload for pilot, passenger and baggage was {{convert|592|lb|kg|abbr=on}}.<ref name="Aerocrafter" />

The manufacturer estimated the construction time from the supplied kit at 1500 hours.<ref name="Aerocrafter" />

==Operational history==
By 1998, the company reported two kits had been sold with two aircraft complete and flying.<ref name="Aerocrafter" />

In December 2013, no examples were [[Aircraft registration|registered]] with the US [[Federal Aviation Administration]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=MD-II&PageNo=1|title = Make / Model Inquiry Results|accessdate = 28 December 2013|last = [[Federal Aviation Administration]]|date = 28 December 2013}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (MD-II) ==
{{Aircraft specs
|ref=AeroCrafter<ref name="Aerocrafter" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=27
|length in=6
|length note=
|span m=
|span ft=29
|span in=6
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=177.0
|wing area note=
|aspect ratio=
|airfoil=USA 35B
|empty weight kg=
|empty weight lb=1150
|empty weight note=
|gross weight kg=
|gross weight lb=2000
|gross weight note=
|fuel capacity={{convert|43|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Avia M 332]]
|eng1 type=four cylinder, inverted, air-cooled, [[supercharged]], inline [[aircraft engine]]
|eng1 kw=
|eng1 hp=140

|prop blade number=1
|prop name=metal
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=184
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=140
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=30
|stall speed kts=
|stall speed note=flaps down
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=690
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=16000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1200
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=11.3
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

<!-- ==External links== -->

[[Category:Frontier aircraft|MD-II]]
[[Category:United States sport aircraft 1990–1999]]
[[Category:United States civil utility aircraft 1990–1999]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Homebuilt aircraft]]