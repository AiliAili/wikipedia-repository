{{Infobox album
| Name = The Age of Adz
| Type = studio
| Artist = [[Sufjan Stevens]]
| Cover = sufjanstevensageofadz.jpg
| Alt = A painting of a samurai warrior in black, red, and white with the name of the album and artist written around it.
| Released = {{Start date|2010|10|12|mf=yes}}
| Recorded = 2009–2010
| Genre = {{flatlist|
* [[Electronica]]
* [[experimental music|experimental]]
* {{nowrap|[[art rock]]}}
* [[electropop]]
* [[futurepop]]
* [[electroacoustic music|electroacoustic]]
* [[glitch music|glitch]]
* [[synthpop]]
}}
| Length = {{Duration|m=74|s=49}}
| Label = [[Asthmatic Kitty]]
| Producer = [[Sufjan Stevens]]
| Last album = ''[[All Delighted People]]''<br/>(2010)
| This album = '''''The Age of Adz'''''<br/>(2010)
| Next album = ''[[Silver & Gold (Sufjan Stevens album)|Silver & Gold]]''<br/>(2012)
| Misc       = {{Singles
  | Name          = The Age of Adz
  | Type          = studio
  | single 1      = I Walked
  | single 1 date = August 27, 2010
  | single 2      = Too Much
  | single 2 date = September 9, 2010
 }}
}}
'''''The Age of Adz''''' (pronounced {{IPAc-en|ɒ|d|z}})<ref name=ak/> is a 2010 album by American singer/songwriter [[Sufjan Stevens]], released on October 12, 2010 by [[Asthmatic Kitty]]. It is Stevens' sixth studio album and his first song-based full-length album in five years, since the release of ''[[Illinois (Sufjan Stevens album)|Illinois]]'' in 2005.

The album features a heavy use of electronics augmented by orchestration, and takes inspiration from the apocalyptic artwork of schizophrenic artist [[Royal Robertson]]. Stevens' use of electronics marked a radical departure from much of his previous work—most notably from ''[[Seven Swans]]'' and ''[[Michigan (album)|Michigan]]''. Unlike ''Illinois'', the lyrics do not explore events, characters or setting, but deal instead with themes and emotions on a personal level.

Critics praised the intimacy of the album, but many were divided over the change in style that Stevens had taken. Nonetheless, it appeared on several "best of 2010" lists—including those of ''[[Paste (magazine)|Paste]]'', ''[[The New York Times]]'' and [[MTV]]. Commercially, the album gave Stevens his career's best first week sales to date and was his highest charting album to date, peaking in the top ten on the [[Billboard 200|''Billboard'' 200]].

==Background and recording==
[[File:Sufjan Stevens, August 2011.jpg|left|alt=Stevens on-stage wearing a colorful suit and a string of lights while playing guitar and singing into a microphone|thumb|Stevens' musical interests have shifted dramatically—he is seen here in 2011 wearing a colorful light-up outfit that fits the obscure electronic themes of ''The Age of Adz'']]

In 2006, Sufjan Stevens released an album of extra material left over from ''Illinois'' (originally conceived as a double album), titled ''[[The Avalanche: Outtakes and Extras from the Illinois Album|The Avalanche]]'',<ref name="ak2"/> as well as an album of [[Christmas music]] titled ''[[Songs for Christmas (Sufjan Stevens album)|Songs for Christmas]]'' (produced in parts between 2001 and 2006).<ref name="ak3"/> Following the release of ''The Avalanche'', Stevens expressed a dissatisfaction with his music, stating in an interview with [[Pitchfork Media]] in 2006: "I'm getting tired of my voice. I'm getting tired of ... the banjo. I'm getting tired of ... the trumpet".<ref name= "sufinterview">{{cite news |last=Crock |first=Jason |url= http://pitchfork.com/features/interviews/6335-sufjan-stevens/ |title=Sufjan Stevens |publisher=[[Pitchfork Media]] |date=May 15, 2006 |accessdate=June 2, 2011}}</ref> In 2009 Stevens admitted that his [[Sufjan Stevens#The Fifty States Project|Fifty States Project]]—an attempt to write an album for each of the 50 [[U.S. State]]s—had been a "promotional gimmick" and not something that he had seriously intended to complete.<ref name=guardian2009 /> In the same year he released ''[[The BQE (album)|The BQE]]'', an orchestral suite accompanying a home-made film dedicated to the [[Interstate 278|Brooklyn-Queens Expressway]].<ref name="ak4"/> In an interview with [[BeatRoute (newspaper)|BeatRoute Magazine]] in 2010, Stevens stated "[''The BQE''] kinda sabotaged the mechanical way of approaching my music, which was basically narrative long-form. It really opened things up for me. It also confused things as well. I don’t think I ever really fully recovered from that process".<ref name="beatroute"/> On August 20, 2010, without prior announcement, Stevens released the [[Extended play|EP]] ''[[All Delighted People]]'',<ref name="ak5"/> and less than one week later, announced ''The Age of Adz'' to be released on October 12.<ref name="ak"/><ref name="npr"/>

In interviews, Stevens stated that during 2009–2010 he suffered from a mysterious debilitating [[virus|viral infection]] that affected his [[nervous system]]. He experienced chronic pain, and was forced to stop working on music for several months.<ref name=stereogum/> He said: "''The Age of Adz'', is, in some ways, a result of that process of working through health issues and getting much more in touch with my physical self. That's why I think the record's really obsessed with sensation and has a hysterical [[melodrama]] to it."<ref name="exclaim"/>

[[My Brightest Diamond]] frontwoman [[Shara Nova]]—who previously collaborated with Stevens as a backing vocalist on the albums ''The Avalanche'' and ''Illinois''—has a solo performance on the track "Impossible Soul", and provides backing vocals throughout the album.<ref name=mbdcoll>{{cite web|title=Collaborations|url=http://www.mybrightestdiamond.com/collaborations/|publisher=[[My Brightest Diamond]]|accessdate=21 January 2013|quote=Sufjan Stevens: Age of Adz background vocals throughout album & solo on “Impossible Soul”}}</ref><ref name=newmusicbox>{{cite web|last=Kushner|first=Daniel J.|title=Shara Worden: Conspiring in Song|url=http://www.newmusicbox.org/articles/shara-worden/|publisher=[[NewMusicBox]]|accessdate=21 January 2013}}</ref>

===Artwork===
The artwork of [[Royal Robertson]], a self-proclaimed prophet from Louisiana,<ref name=akrmerch>{{cite web|title=Sufjan Stevens ''The Age of Adz''|url=http://asthmatickitty.com/merch/the-age-of-adz/|publisher=[[Asthmatic Kitty Records]]|accessdate=21 January 2013}}</ref> was used for the album's cover and interior.<ref name=ak/> Will Hermes of ''[[Rolling Stone (magazine)|Rolling Stone]]'' said that Sufjan Stevens uses the artwork "as a springboard for music that evokes a visionary psyche."<ref name=rs/> Stevens became interested in the work of Robertson after recording music for a friend's documentary on the artist, and said that "[the more I studied him and his work], the more I felt a weird affinity to this guy and the story of his life."<ref name="dissufjan" /> He began to transcribe some of the text that appears in Robertson's artwork, and says this process stayed with him a "long time" and "that some of it started to come up in the lyrics, in the songs I was writing.<ref name="dissufjan">{{cite web|last=Skinner|first=James|title="I feel much more optimistic right now." DiS meets Sufjan Stevens|url=http://drownedinsound.com/in_depth/4141197-%E2%80%9Ci-feel-much-more-optimistic-right-now-%E2%80%9D-dis-meets-sufjan-stevens|work=[[Drowned in Sound]]|accessdate=July 22, 2011}}</ref>

==Musical style and themes==

===Musical style===
{{Listen
|filename=Age Of Adz - Sufjan Stevens.ogg
|title="Age of Adz" (2010)
|description=30 second sample from the song "Age of Adz", demonstrating the use of heavy electronic and orchestral sounds.
|format=[[Ogg]]}}
''The Age of Adz'' marked a radical departure in musical style from that of Stevens' previous album ''Illinois''. Robin Hilton summarised the changes as Stevens "[replacing] delicately plucked banjo lines, wispy vocals and sentimental melodies with glitchy soundscapes, [[Hip hop music|hip-hop]] [''sic''] beats and heavily filtered vocals."<ref name=firstlisten>{{cite web|last=Hilton|first=Robin|title=First Listen: Sufjan Stevens, 'The Age Of Adz'|url=http://www.npr.org/templates/story/story.php?storyId=130049247|publisher=[[NPR Music]]|accessdate=21 January 2013}}</ref> Amongst his other work, the 2001 [[Electronic music|electronic]] album ''[[Enjoy Your Rabbit]]'' was regarded as being the most close in style to this album.<ref name=akrmerch /> Nevertheless, critics highlighted the first track "Futile Devices" as being stylistically consistent with his earlier [[Acoustic music|acoustic]] work.<ref name=relevant>{{cite web|last=Taylor|first=John|title=Sufjan Stevens, The Age of Adz|url=http://www.relevantmagazine.com/culture/music/reviews/23120-sufjan-stevens-the-age-of-adz|publisher=Relevant Magazine|accessdate=21 January 2013|date=October 2010}}</ref>

Sufjan Stevens himself has said that the album's tracks "are [[Pop music|pop]] songs, but they’re based on sound experimentation and noise".<ref name=kushner2>{{cite web|last=Kushner|first=Daniel J.|title=Adz and Ends: An Interview with Sufjan Stevens (Part 2 of 3)|url=http://postpostrock.com/2011/07/22/music-for-the-5-an-interview-with-sufjan-stevens-part-2-of-3/|publisher=Post-Post-Rock|accessdate=21 January 2013}}</ref> He contrasted the way in which the album was made for listeners who understood his interest in "electronic music and noise and in sound sculpting and [[Minimal music|minimalism]]", and with ''Illinois'' which he described as a "[[Popular music|populist]] record".<ref name=kushner2 />
Professional music critics dubbed the album [[experimental music|experimental]],<ref>{{cite web|url=http://www.nme.com/reviews/sufjan-stevens/16002 |title=Sufjan Stevens - 'Carrie & Lowell' |website=[[NME]] |accessdate=6 August 2016}}</ref><ref>{{cite web|url=http://www.spin.com/2015/04/sufjan-stevens-carrie-and-lowell-review/ |title=Review: Sufjan Stevens Sheds the Frills on the Deeply Personal ‘Carrie & Lowell’ |website=[[Spin (magazine)|Spin]] |accessdate=6 August 2016}}</ref> [[electronic pop]],<ref>{{cite web|url=http://www.allmusic.com/artist/sufjan-stevens-mn0000580300/biography |title=Sufjan Stevens Biography by Mario Mesquita Borges |website=[[Allmusic]] |accessdate=5 August 2016}}</ref><ref>{{cite web|url=http://www.loudandquiet.com/2010/12/sufjan-stevens-the-age-of-adz/ |title=Sufjan Stevens The Age of Adz (Asthmatic Kitty) |website=[[Loud and Quiet]] |accessdate=6 August 2016}}</ref> [[electronica]],<ref>{{cite web|url=http://www.slantmagazine.com/music/review/sufjan-stevens-carrie-lowell |title=Sufjan Stevens Carrie & Lowell |website=[[Slant Magazine]] |accessdate=6 August 2016}}</ref> [[art rock]],<ref>{{cite web|url=http://americansongwriter.com/2010/10/sufjan-stevens-the-age-of-adz/ |title=Sufjan Stevens: The Age of Adz |website=[[American Songwriter]] |accessdate=5 August 2016}}</ref> [[electropop]],<ref>{{cite web|url=http://consequenceofsound.net/2015/03/album-review-sufjan-stevens-carrie-lowell/ |title=Sufjan Stevens – Carrie & Lowell |website=[[Consequence of Sound]] |accessdate=6 August 2016}}</ref> [[futurepop]],<ref>{{cite web|url=http://www.nme.com/reviews/sufjan-stevens/13936 |title=Sufjan Stevens - 'Silver & Gold' |website=[[NME]] | accessdate=5 August 2016}}</ref> [[electroacoustic music|electroacoustic]],<ref>{{cite web|url=http://pitchfork.com/reviews/albums/20218-carrie-lowell/ |title=Sufjan Stevens Carrie & Lowell |website=[[Pitchfork (website)|Pitchfork]] |accessdate=5 August 2016}}</ref> [[glitch music|glitch]],<ref>{{cite web|url=https://www.pastemagazine.com/articles/2015/03/sufjan-stevens-carrie-lowell-review.html |title=Sufjan Stevens: Carrie & Lowell Review |website=[[Paste (magazine)|Paste]] |accessdate=6 August 2016}}</ref>
and [[synthpop]].<ref>{{cite web|url=http://www.rollingstone.com/music/albumreviews/sufjan-stevens-carrie-lowell-20150331 |title=Sufjan Stevens: Carrie & Lowell |website=[[Rolling Stone]] |accessdate=6 August 2016}}</ref>

===Themes===

''The Age of Adz'' departs from the geography-based concepts of Stevens' previous albums; until his announcement in 2009 that he had never seriously intended to complete the project, it had been expected that his next work might continue the [[Sufjan Stevens#The Fifty States Project|Fifty States Project]] that he had begun with ''Michigan'' and continued with ''Illinois''.<ref name="guardian2009">{{cite news | url=https://www.theguardian.com/music/2009/oct/27/sufjan-stevens-the-bqe | title= Sufjan Stevens's symphony for New York |work=[[The Guardian]] |date=October 27, 2009 |accessdate=October 28, 2009 | location=London | first=Andrew | last=Purcell}}</ref> The album contains no "conceptual underpinnings",<ref name=akrmerch /> instead focusing on themes that are personal and intimate to Stevens himself;<ref name="allmusic" /> the label Asthmatic Kitty describes the themes as "personal and primal … love, sex, death, disease, illness, anxiety, and suicide".<ref name=akrmerch />

A recurrent focus of the album is love,<ref name="ind" /> a theme that sometimes overlaps with spirituality as Stevens seems to address both a lover and a divine power.<ref name="frontierpsych">{{cite web |url=http://frontpsych.com/2010/12/20/the-church-of-sufjan-stevens-the-age-of-adz/ |title=The Church of Sufjan Stevens&nbsp;– The Age of Adz |work=Frontier Psychiatrist |date=December 20, 2010 |accessdate=December 20, 2010}}</ref> Another important theme is that of mortality — the song "Now That I'm Older" has been noted as "repeatedly [acknowledging] mortality and the importance of making the most of life."<ref name="frontierpsych" /> Acknowledging the interplay of these two themes, the album has been described as having songs "in which love and death reign darkly over an imaginative landscape peopled with apparitions, ghosts, orators and space travellers".<ref name="ind" />

==Release and reception==
''The Age of Adz'' debuted at number seven on the [[Billboard 200|''Billboard'' 200]], with 36,000 copies sold, giving Stevens his career's best first week sales to date until ''[[Carrie & Lowell]]'' opened with 51,000 copies in 2015. It was his highest charting album to date, peaking in the top ten on the Billboard 200.<ref name="stereogum"/><ref name="billboard"/> It also placed number one on ''Billboard's'' "Rock Albums", "Independent Albums", "Alternative Albums" and "Folk Albums" lists, and placed number two on the "Digital Albums" and "Tastemaker Albums" lists.<ref name="billboard"/> The album also placed within the "Top 100 Albums" lists in several other countries.<ref name=acharts>{{cite web|title=Sufjan Stevens&nbsp;– The Age of Adz|url=http://acharts.us/album/57917|publisher=acharts.us|accessdate=3 October 2012}}</ref>

===Critical reception===
{{Album ratings
| MC = 80/100<ref name="metacritic"/>
|rev1 = [[AllMusic]]
|rev1Score = {{rating|4|5}}<ref name="allmusic"/>
|rev2 = ''[[The A.V. Club]]''
|rev2Score = B<ref name="avclub"/>
|rev3 = ''[[Entertainment Weekly]]''
|rev3score = A−<ref name="ew"/>
|rev4 = ''[[The Guardian]]''
|rev4Score = {{rating|4|5}}<ref name="theguardian"/>
|rev5 = ''[[The Independent]]''
|rev5Score = {{Rating|4|5}}<ref name="ind"/>
|rev6 = ''[[Los Angeles Times]]''
|rev6Score = {{rating|3.5|4}}<ref name="lat"/>
|rev7 = ''[[NME]]''
|rev7score = 7/10<ref name="nme"/>
|rev8 = [[Pitchfork Media]]
|rev8score = 8.4/10<ref name="pitchfork"/>
|rev9 = ''[[Rolling Stone]]''
|rev9Score = {{rating|3.5|5}}<ref name="rs"/>
|rev10 = ''[[Spin (magazine)|Spin]]''
|rev10Score = 7/10<ref name="spin"/>
}}
''The Age of Adz'' received widespread critical acclaim upon its release. At [[Metacritic]], which assigns a normalised rating out of 100 to reviews from mainstream critics, the album received an average score of 80, based on 33 reviews, indicating "generally favorable reviews".<ref name="metacritic"/> Keith Meatto of the ''Frontier Psychiatrist'' described the album as "a musical masterpiece that blends analog and digital sounds as it reflects on love and loss, life and death, humanity and divinity".<ref name="frontierpsych" /> [[Entertainment.ie]]'s Jenny Mulligan described Stevens as "a strange one, that's for sure, but he may just be a genius".<ref name="entertainment.ie"/> ''[[Uncut (magazine)|Uncut]]'' commented that the album provides plenty of evidence to argue that he is either "one of the most important songwriters of his generation" or "just an infuriating, neurotic show-off".<ref name="uncut"/> [[Alexis Petridis]] of ''The Guardian'' said that although the album "goes a bit barmy and over-the-top" there are some "incredible tune[s]" that are "not only genuinely remarkable, but genuinely enjoyable".<ref name="theguardian" /> Alex Denney of ''[[NME]]'' similarly commented that the album "conjures just enough moments of heart-stopping gorgeousness to foot the bill for its dizzying excesses".<ref name="nme" /> Sam Lewis of the [[BBC]] remarked on the same point: that the album is "suffused with individual moments of brilliance", but overall it is "let down by its self-conscious incoherence".<ref name="BBC" />

The most discussed track of the album among reviewers was "Impossible Soul", which at 25 minutes in length comprises a third of the overall album. [[Pitchfork Media|Pitchfork]]'s reviewer Ryan Dombal described the track as having "more engaging ideas than most artists could muster in a career",<ref name="pitchfork" /> although No Ripcord reviewer Alan Shulman criticized the middle sections as being an "epic train wreck", saying that the closing minutes come as "a breath of fresh air".<ref name="no ripcord">{{cite web |url=http://www.noripcord.com/reviews/music/sufjan-stevens/age-adz |title=The Age of Adz |work=No Ripcord |author=Alan Shulman |date=October 12, 2010 |accessdate=October 22, 2010}}</ref> [[One Thirty BPM]] reviewer Rob Hakimian was mixed in his reception of the track, and commented that it would "make or break" the album for listeners, describing it as a successful "proclamation of love", but also "bloated" and "way over the top".<ref name="onethirtybpm" />

===Accolades===

Many reviewing sites included ''The Age of Adz'' in their best of 2010 lists.

{| class="wikitable"
|+Best of the year (2010) lists
! Publisher !! Accolade !! Rank
|-
| ''[[Time (magazine)|Time]]'' || Top 10 Albums of 2010 || #6<ref name="Auto8C-1"/>
|-
| ''[[Exclaim!]]'' || Best Pop & Rock Album of 2010 || #8<ref name= Exclaim!>{{cite web|title=Pop & Rock Year in Review|work=[[Exclaim!]]|url=http://exclaim.ca/Features/YearInReview/pop_rock_year_in_review_2010/Page/8}}</ref>
|-
| [[Pitchfork Media|''Pitchfork'']] || 50 top albums of 2010 || #25<ref>{{cite web |url=http://pitchfork.com/features/staff-lists/7893-the-top-50-albums-of-2010/3/ |title=The Top 50 Albums of 2010 |work=Pitchfork |date=December 16, 2010 |accessdate=December 18, 2010}}</ref>

|-
| ''[[The New York Times]]'' || Top Pop 2010 Anthems || #5<ref name=NYTimes>{{cite news|last=Pareles|first=Jon|title=Top Pop 2010|url=https://www.nytimes.com/2010/12/19/arts/music/19pareles.html|work=The New York Times|accessdate=July 17, 2011|date=December 16, 2010}}</ref>
|-
| [[MTV]] || 20 Best Albums Of 2010 || #10<ref name=MTV>{{cite web|last=Montgomery|first=James|title=Kanye West, Robyn And More: 20 Best Albums Of 2010|url=http://www.mtv.com/news/articles/1654286/kanye-west-robyn-more-20-best-albums-2010.jhtml|publisher=MTV|accessdate=July 17, 2011}}</ref>
|-
| ''[[Paste (magazine)|Paste]]'' || The 50 Best Albums of 2010 || #9<ref name=Paste>{{cite web|last=Jackson|first=Josh|title=The 50 Best Albums of 2010|url=http://www.pastemagazine.com/blogs/lists/2010/12/the-50-best-albums-of-2010.html?p=5|publisher=Paste|accessdate=July 17, 2011}}</ref>
|}

==Track listing==
{{Track listing
| total_length = 74:49
| all_writing = Sufjan Stevens
| all_music = Sufjan Stevens
| title1 = Futile Devices
| length1 = 2:11
| title2 = Too Much
| length2 = 6:44
| title3 = Age of Adz
| length3 = 8:00
| title4 = I Walked
| length4 = 5:01
| title5 = Now That I'm Older
| length5 = 4:56
| title6 = Get Real Get Right
| length6 = 5:10
| title7 = Bad Communication
| length7 = 2:24
| title8 = Vesuvius
| length8 = 5:26
| title9 = All for Myself
| length9 = 2:55
| title10 = I Want to Be Well
| length10 = 6:27
| title11 = Impossible Soul
| length11 = 25:35
}}

*''Note: On the vinyl release of the album, the last movement (about three minutes) of "Impossible Soul" is moved to the end of side C, just after "I Want to be Well" for reasons of space and time restrictions.''

==Personnel==
The album's liner notes provides limited information regarding the personnel. [[Royal Robertson|Prophet Royal Robertson]] is credited with all artwork except the pieces "Bar-Koom!", "Katora", and "Space Autos" by Scott Ogden.<ref name=liner>''The Age of Adz'' (CD liner notes). [[Sufjan Stevens]]. [[Asthmatic Kitty]]. 2010. AKR077.</ref><ref name=allmusiccredits>{{cite web|title=The Age of Adz - Credits|url=http://www.allmusic.com/album/the-age-of-adz-mw0002043119/credits|publisher=[[Allmusic]]|accessdate=9 February 2013}}</ref> [[Shara Nova]] performs a solo on "Impossible Soul", and provides backing vocals throughout.<ref name=mbdcoll />  [[St. Vincent (musician)|Annie Clark]] sang backing vocals on "Now That I'm Older".<ref name=recordcheck>{{cite web|title=Record Check with St. Vincent|url=https://www.youtube.com/watch?v=DfEvSu4ppKM|accessdate=26 January 2015}}</ref>

==Remixes==
The song "Vesuvius" was sampled in "Fade Away" by [[Social Club (band)|Social Club]] and [[Khleo Thomas]] and "All For Myself" was sampled in [[Asaiah Ziv]] and Kiya Lacey's "Babylon[ER]"{{fact|date=September 2016}} and [[Kendrick Lamar]]'s "[[Hood Politics (Kendrick Lamar song)|Hood Politics]]".<ref name="NME 2015">{{cite web|last1=Cooper|first1=Leonie|title=Kendrick Lamar samples Sufjan Stevens on 'To Pimp A Butterfly' track Read more at http://www.nme.com/news/kendrick-lamar/83590|url=http://www.nme.com/news/kendrick-lamar/83590|website=NME|accessdate=26 September 2016}}</ref>

==Chart positions==
{| class="wikitable"
|-
! Region
! Sales charts (2010)
! Peak position
|-
|Australia
| [[ARIA Charts]]
|align="center"| 61<ref>{{cite web|url=http://www.noise11.com/news/aria-albums-spirit-of-the-anzacs-in-no-1-20150411|title=ARIA Albums: Spirit Of The Anzacs Is No 1|publisher=Noise11|last=Ryan|first=Gavin|date=April 11, 2015|accessdate=April 11, 2015}}</ref>
|-
|Ireland
| [[Irish Albums Chart]]
|align="center"| 23<ref name=acharts />
|-
|Norway
| [[VG-lista]]
|align="center"| 19<ref name=acharts />
|-
|United Kingdom
| [[UK Albums Chart]]
|align="center"| 30<ref name=acharts />
|-
|rowspan="2"|United States
| [[Billboard 200|US ''Billboard'' 200]]
|align="center"| 7<ref name=allmusiccharts>{{cite web|title=The Age of Adz|url=http://www.allmusic.com/album/the-age-of-adz-r2051179/charts-awards|publisher=allmusic|accessdate=July 24, 2011}}</ref>
|-
| [[Independent Albums]]
|align="center"| 1<ref name=allmusiccharts/>
|-
|}

==References==
{{reflist |colwidth=30em
|refs=
<ref name="ak">{{cite web |url=http://asthmatickitty.com/news.php?newsID=615 |title=The Age of Adz, a New Album from Sufjan Stevens |publisher=[[Asthmatic Kitty]] |date=August 26, 2010 |accessdate=August 26, 2010}}</ref>

<ref name="ak2">{{cite web |url=http://asthmatickitty.com/the-avalanche |title=The Avalanche |publisher=[[Asthmatic Kitty]] |accessdate=October 21, 2010}}</ref>

<ref name="ak3">{{cite web |url=http://asthmatickitty.com/songs-for-christmas |title=Songs For Christmas |publisher=[[Asthmatic Kitty]] |accessdate=October 21, 2010}}</ref>

<ref name="pitchfork">{{cite web |url=http://pitchfork.com/reviews/albums/14738-the-age-of-adz/ |title=Sufjan Stevens: The Age of Adz |publisher=[[Pitchfork Media]] |last=Dombal |first=Ryan |date=October 12, 2010 |accessdate=January 12, 2014}}</ref>

<ref name="ak4">{{cite web |url=http://asthmatickitty.com/the-bqe |title=The BQE |publisher=[[Asthmatic Kitty]] |accessdate=October 21, 2010}}</ref>

<ref name="beatroute">{{cite web |url=http://www.beatroute.ca/view_archived_article.php?id=70&sectionID=15&articleID=3605 |title=Sufjan Stevens |work=BeatRoute Magazine |last=Hallahan |first=Clinton |accessdate=October 22, 2010}}</ref>

<ref name="ak5">{{cite web |url=http://asthmatickitty.com/news.php?newsID=613 |title=A New EP from Sufjan Stevens |publisher=[[Asthmatic Kitty]] |date=August 20, 2010 |accessdate=October 21, 2010}}</ref>

<ref name="npr">{{cite web |url=http://www.npr.org/templates/story/story.php?storyId=130049247&sc=fb&cc=fp |title=First Listen: Sufjan Stevens, 'The Age Of Adz' |publisher=[[NPR]] |last=Hilton |first=Robin |date=September 26, 2010 |accessdate=September 27, 2010}}</ref>

<ref name="stereogum">{{cite web |url=http://stereogum.com/553642/sufjan-reveals-health-issues-has-career-best-sales-week/news/ |title=Sufjan Reveals Health Issues, Has Career-Best Sales Week |publisher=[[Stereogum]] |last=Suarez |first=Jessica |date=October 22, 2010 |accessdate=October 25, 2010}}</ref>

<ref name="exclaim">{{cite web |url=http://exclaim.ca/News/sufjan_stevens_discusses_his_serious_issues |title=Sufjan Stevens Discusses His "Mysterious and Debilitating" Health Issues |work=[[Exclaim!]] |date=October 22, 2010 |accessdate=October 27, 2010}}</ref>

<ref name="billboard">{{cite web |url={{BillboardURLbyName|artist=sufjan stevens|chart=all}} |title=The Age of Adz&nbsp;– Sufjan Stevens |work=Billboard |accessdate=October 25, 2010}}</ref>

<ref name="metacritic">{{cite web |url=http://www.metacritic.com/music/the-age-of-adz/sufjan-stevens |title=Reviews for The Age of Adz by Sufjan Stevens |publisher=[[Metacritic]] |accessdate=October 19, 2010}}</ref>

<ref name="allmusic">{{cite web |url=http://www.allmusic.com/album/the-age-of-adz-mw0002043119 |title=The Age of Adz&nbsp;– Sufjan Stevens |publisher=[[AllMusic]] |last=Monger |first=James Christopher |accessdate=November 6, 2010}}</ref>

<ref name="avclub">{{cite web |url=http://www.avclub.com/review/sufjan-stevens-ithe-age-of-adzi-46256 |title=Sufjan Stevens: The Age Of Adz |work=[[The A.V. Club]] |last=Murray |first=Noel |date=October 12, 2010 |accessdate=November 7, 2010}}</ref>

<ref name="BBC">{{cite web |url=http://www.bbc.co.uk/music/reviews/8d5c |title=Review of Sufjan Stevens&nbsp;— The Age of Adz |publisher=[[BBC Music]] |last=Lewis |first=Sam |date=October 8, 2010 |accessdate=October 13, 2010}}</ref>

<ref name="entertainment.ie">{{cite web |url=http://entertainment.ie/album-review/Sufjan-Stevens-The-Age-of-Adz/7279.htm |title=Sufjan Stevens&nbsp;– The Age of Adz |publisher=[[Entertainment.ie]] |last=Mulligan |first=Jenny |date=October 13, 2010 |accessdate=January 12, 2011}}</ref>

<ref name="ew">{{cite news |url=http://www.ew.com/article/2010/10/06/age-adz |title=The Age of Adz |work=[[Entertainment Weekly]] |last=Vozick-Levinson |first=Simon |date=October 6, 2010 |accessdate=October 12, 2010}}</ref>

<ref name="theguardian">{{cite news |url=https://www.theguardian.com/music/2010/oct/07/sufjan-stevens-age-adz-review |title=Sufjan Stevens: The Age of Adz&nbsp;— review |work=[[The Guardian]] |last=Petridis |first=Alexis |authorlink=Alexis Petridis |date=October 7, 2010 |accessdate=October 13, 2010 |location=London}}</ref>

<ref name="ind">{{cite news |url=http://www.independent.co.uk/arts-entertainment/music/reviews/album-sufjan-stevens-the-age-of-adz-asthmatic-kitty-2100589.html |title=Album: Sufjan Stevens, The Age of Adz (Asthmatic Kitty) |work=[[The Independent]] |last=Gill |first=Andy |date=October 8, 2010 |accessdate=October 8, 2010 |location=London |archiveurl=https://web.archive.org/web/20101012073509/http://www.independent.co.uk/arts-entertainment/music/reviews/album-sufjan-stevens-the-age-of-adz-asthmatic-kitty-2100589.html |archivedate=October 12, 2010}}</ref>

<ref name="lat">{{cite news |url=http://latimesblogs.latimes.com/music_blog/2010/10/album-review-sufjan-stevens-age-of-adz.html |title=Album review: Sufjan Stevens' 'Age of Adz' |work=[[Los Angeles Times]] |last=Powers |first=Ann |authorlink=Ann Powers |date=October 12, 2010 |accessdate=October 13, 2010}}</ref>

<ref name="nme">{{cite web |url=http://www.nme.com/reviews/sufjan-stevens/11629 |title=Album Review: Sufjan Stevens&nbsp;– The Age Of Adz (Asthmatic Kitty) |work=[[NME]] |last=Denney |first=Alex |date=October 8, 2008 |accessdate=January 21, 2013}}</ref>

<ref name="onethirtybpm">{{cite web |url=http://onethirtybpm.com/2010/10/11/album-review-sufjan-stevens-the-age-of-adz/ |title=Album Review: Sufjan Stevens&nbsp;— The Age of Adz |work=[[Beats Per Minute (website)|One Thirty BPM]] |last=Hakimian |first=Rob |date=October 12, 2010 |accessdate=October 12, 2010}}</ref>

<ref name="rs">{{cite web |url=http://www.rollingstone.com/music/albumreviews/the-age-of-adz-20101012 |title=The Age of Adz |work=[[Rolling Stone]] |last=Hermes |first=Will |authorlink=Will Hermes |date=October 12, 2010 |accessdate=November 7, 2010}}</ref>

<ref name="spin">{{cite web |url=http://www.spin.com/reviews/sufjan-stevens-age-adz-asthmatic-kitty |title=Sufjan Stevens, 'The Age of Adz' (Asthmatic Kitty) |work=[[Spin (magazine)|Spin]] |last=Wood |first=Mikael |date=October 2, 2010 |accessdate=October 8, 2010}}</ref>

<ref name="Auto8C-1">{{cite news |url=http://www.time.com/time/specials/packages/article/0,28804,2035319_2034644_2034625,00.html |title=Sufjan Stevens, 'Age of Adz'&nbsp;– The Top 10 Everything of 2010 |work=[[Time (magazine)|Time]] |accessdate=December 26, 2012 |date=December 9, 2010}}</ref>

<ref name="uncut">{{cite journal |title=Sufjan Stevens: The Age of Adz |work=[[Uncut (magazine)|Uncut]] |issue=162 |date=November 2010}}</ref>
}}

==External links==
*''[http://sufjanstevens.bandcamp.com/album/the-age-of-adz The Age of Adz]'' at [[Bandcamp]]
*''[http://www.discogs.com/master/281160 The Age of Adz]'' at [[Discogs]] (list of releases)
*''[http://www.metacritic.com/music/the-age-of-adz/sufjan-stevens The Age of Adz]'' at [[Metacritic]] 
*''[https://rateyourmusic.com/release/album/sufjan_stevens/the_age_of_adz/ The Age of Adz]'' at rateyourmusic.com

{{Sufjan Stevens}}

{{Good article}}

{{Use mdy dates|date=April 2012}}

{{DEFAULTSORT:Age Of Adz, The}}
[[Category:2010 albums]]
[[Category:Sufjan Stevens albums]]
[[Category:Albums produced by Sufjan Stevens]]
[[Category:Asthmatic Kitty albums]]
[[Category:English-language albums]]