{{Cleanup-rewrite|date=May 2009}}

'''Accounting reform''' is an expansion of [[accounting]] rules that goes beyond the realm of financial measures for both individual economic entities and national economies.  It is advocated by those who consider the focus of the present standards and practices wholly inadequate to the task of measuring and reporting the activity, success, and failure of modern enterprise, including government.

Real debate concerns concepts such as whether to report transactions, such as asset acquisitions, at their cost or at their current market values. The former, traditional approach, appeals for its reliability, but can quickly lose its relevance due to inflation and other factors; the latter, increasingly common approach, is appealing for its relevance, but is less reliable due to the need to use subjective measures. Accounting standards setters such as the [[International Accounting Standards Board]] attempt to strike a balance between relevance and reliability.

== Business ==

Limited reforms within professional [[management]] circles have led in the past to activity-based costing, [[economic value added]], and [[risk]] measures. Current accounting practices have also been criticised for being too complex.

Heads of the [[U.S. Securities and Exchange Commission]] since the 1980s have consistently complained that this lobbying makes it impossible for them to apply meaningful reform, even in the wake of [[accounting scandals]], e.g. that which felled [[Arthur Andersen]] in 2002.

== National economies ==
Any comprehensive scheme of accounting reform is a major professional and academic enterprise. Typically it requires examination of the role of each of the fundamental [[factors of production]], an analysis of [[capital (economics)|capital]] indicating how many types there are and how each supports each factor of a production process.

A comprehensive scheme that would affect, for instance, the United Nations standards for [[national accounts]], the rules of the [[Bank for International Settlements]], or [[Listing (finance)|listing]] requirements on the major [[stock exchange]]s, would have to defend any change against critics that advocated lesser reforms.

[[Marilyn Waring]], who deeply criticized the UN account system for systematically under-valuing the social and economic contributions of women, stated also that she had to read literally an entire room full of books in order even to understand the standards applied today.

The critique from [[ecological economics]] claims that most means of [[measuring well-being]] indicated that the [[developed nation]]s were in a state of "[[uneconomic growth]]" through the 1980s and 1990s, due mostly to failures of measurement, most or all of which could be tracked back to the practice of using [[Gross National Product]] or other narrow indicators as a means of making [[money supply]] (and other economic) decisions.

Robert Costanza, [[Paul Hawken]], [[Amory Lovins]] and others who advocate a consistent global system for valuing [[natural capital]], note that failures in this area are particularly grim:  promoting [[extinction]], loss of [[biodiversity]], [[climate change]] and destructive weather for the sake of such "growth".  [[John McMurtry]] characterized this as "the [[cancer]] stage of [[capitalism]]".
What makes "economic sense" under current standards, they argue, is in fact leading to ecological catastrophe, social conflict, and economic chaos.

==Notable advocates==
Notable advocates of accounting reform:
*Baruch Lev
* [http://www.bc.edu/schools/law/fac-staff/deans-faculty/cunninghaml/ Lawrence A. Cunningham]
*[[Marilyn Waring]]
*Robert Costanza
*[[Amory Lovins]]

==See also==
*[[Philosophy of Accounting]]
*[[standard accounting practices]]
*[[economic value added]]
*[[risk]]
*[[Inflation accounting]]

==References==
{{reflist}}

[[Category:National accounts]]
[[Category:Accounting]]
[[Category:Environmental economics]]