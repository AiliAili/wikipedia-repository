{{Use mdy dates|date=November 2013}}
{{good article}}
{{Infobox NFL season
| team = Dayton Triangles
| logo = [[File:1920DaytonTriangles.jpg|200px|alt=A group of men standing in rows of three in their football uniforms. One executive is all the way to the left, and the other executive is on the right. The words "Triangle Football Team, 1920" is written on the photograph.]]
| year = 1920
| record = 5–2–2 Overall<br> 4–2–2 [[NFL|APFA]]
| league_place = 6th APFA
| coach = [[Bud Talbott]]
| owner = Carl Storck
| stadium = [[Triangle Park (Dayton)|Triangle Park]]
| playoffs =<!--No playoffs until 1932-->
}}

The '''[[1920 APFA season|1920]] [[Dayton Triangles]] season''' was the franchise's inaugural season in the [[National Football League|American Professional Football Association]] (AFPA)—later named the National Football League. The Triangles entered the season coming off a 5–2–1 record in 1919 in the [[Ohio League]]. After the 1919 season, several representatives from the Ohio League wanted to form a new professional league; thus, the APFA was created. A majority of the team stayed from the 1919 team, including the coaching staff, while two players left the team.

The Triangles opened the season with a win against the [[Columbus Panhandles]]. This game is considered the first league game where two APFA teams played against each other. After a six-game winning streak, the Triangles faced their first loss of the season to the future champions, the [[Akron Pros]]. This team would give the Triangles their only two losses of the year. The Triangles finished the season with a 5–2–2 record, which earned them sixth place in the APFA standings. No players were awarded with the first team [[All-Pro]] award, Norb Sacksteder made the second team, and Frank Bacon made the third team.

{{TOC limit|2}}

== Offseason ==
The Dayton Triangles finished 5–2–1 in their [[1919 Dayton Triangles season|1919 season]] in the [[Ohio League]].<ref name="1919triangles">{{cite web | url=http://www.profootballarchives.com/1919dayt.html | title=1919 Dayton Triangles | publisher=Maher Sports Media | work=The Pro Football Archives | accessdate=February 28, 2012}}</ref> The Triangles had several players added to their team for the 1920 season: [[Max Broadhurst]], Doc Davis, Guy Early, [[Russ Hathaway]], Chuck Helvie, Pesty Lentz, [[Norb Sacksteder]], Ed Sauer, [[Fritz Slackford]], and Tiny Turner. Two players—one with a last name Albers, and one with a last name Yerges—did not play for the Triangles in 1920, and the coaching staff stayed the same.<ref>{{Cite web | url=http://www.daytontriangles.com/1919team.htm | title=1919 Dayton Triangles & Season Record | publisher=DaytonTriangles.com | accessdate=September 12, 2012}}</ref><ref name="roster" />

After the 1919 season, representatives of four Ohio League teams—the [[Canton Bulldogs]], the [[Cleveland Tigers (NFL)|Cleveland Tigers]], the [[Dayton Triangles]], and the [[Akron Pros]]—called a meeting on August 20, 1920, to discuss the formation of a new league. At the meeting, they tentatively agreed on a [[salary cap]] and pledged not to sign college players or players already under contract with other teams. They also agreed on a name for the circuit: the American Professional Football Conference.<ref>PFRA Research (1980), pp. 3–4</ref><ref>Siwoff, Zimmber & Marini (2010), pp. 352–353</ref> They then invited other professional teams to a second meeting on September 17.

At that meeting, held at Bulldogs owner [[Ralph Hay]]'s [[Hupmobile]] showroom in Canton, representatives of the [[Rock Island Independents]], the [[Muncie Flyers]], the [[Decatur Staleys]], the [[Racine Cardinals]], the [[Massillon Tigers]], the [[Chicago Cardinals (NFL, 1920–59)|Chicago Cardinals]], and the [[Hammond Pros]] agreed to join the league. Representatives of the [[Buffalo All-Americans]] and [[Rochester Jeffersons]] could not attend the meeting, but sent letters to Hay asking to be included in the league.<ref name="nflbday4">PFRA Research (1980), p. 4</ref> Team representatives changed the league's name slightly to the American Professional Football Association and elected officers, installing Jim Thorpe as president.<ref name="nflbday4" /><ref>{{Cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9503E5DC173AE532A2575AC1A96F9C946195D6CF | title=Thorpe Made President | newspaper=The New York Times| date=September 19, 1920 | format=PDF}}</ref><ref>{{Cite news | url=https://news.google.com/newspapers?id=f8MWAAAAIBAJ&sjid=OiEEAAAAIBAJ&pg=3568,5105560&dq=gridders&hl=en | title=Organize Pro Gridders; Choose Thorpe, Prexy | newspaper=[[The Milwaukee Journal]] | date=September 19, 1920 | page=24}}</ref> Under the new league structure, teams created their schedules dynamically as the season progressed, so there were no minimum or maximum number of games needed to be played.<ref>Peterson (1997), p. 74</ref><ref>Davis (2005), p. 59</ref> Also, representatives of each team voted to determine the winner of the APFA trophy.<ref>{{Cite news | last=Price | first=Mark | date=April 25, 2011 | url=http://www.ohio.com/news/searching-for-lost-trophy-1.204246 | title=Searching for Lost Trophy | newspaper=[[Akron Beacon-Journal]] | accessdate=June 23, 2012}}</ref>

== Schedule ==

{| class="wikitable" style="text-align:center"
|-
! Week !! Date !! Opponent !! Result !! Venue !! Attendance !! Record
|-
! scope="row" |  1
| colspan="8" | ''No game scheduled''
|-style="background: #ddffdd;"
! scope="row" | 2
| October 3, 1920
| vs. Columbus Panhandles
| 14–0 W
| Triangle Park
| {{NA}}
| 1–0
|-style="background: #ffeeaa;"
! scope="row" | 3
| October 10, 1920
| vs. Cleveland Tigers
| 0–0 T
| Triangle Park
| {{NA}}
| 1–0–1
|-style="background: #ddffdd;"
! scope="row" | 4
| October 17, 1920
| vs. Hammond Pros
| 44–0 W
| Triangle Park
| 2,000
| 2–0–1
|-style="background: #ffeeaa;"
! scope="row" | 5
| October 24, 1920
| vs. Canton Bulldogs
| 20–20 T
| Triangle Park
| 5,000
| 2–0–2
|-style="background: #ddffdd;"
! scope="row" | 6
| October 31, 1920
| vs. Cincinnati Celts{{Dagger}}
| 23–7 W
| Triangle Park
| {{NA}}
| 3–0–2
|-
! scope="row" |  7
| colspan="8" | ''No game scheduled''
|-style="background: #ddffdd;"
! scope="row" | 8
| November 14, 1920
| at Rock Island Independents
| 21–0 W
| Douglas Park
| 4,991
| 4–0–2
|-style="background: #ffdddd;"
! scope="row" | 9
| November 21, 1920
| at Akron Pros
| 13–0 L
| League Park
| 3,700
| 4–1–2
|-style="background: #ddffdd;"
! scope="row" | 10
| November 25, 1920
| vs. Detroit Heralds
| 28–0 W
| Triangle Park
| {{NA}}
| 5–1–2
|-style="background: #ffdddd;"
! scope="row" | 10
| November 28, 1920
| vs. Akron Pros
| 14–0 L
| Triangle Park
| 5,000
| 5–2–2
|-
! scope="row" |  11
| colspan="8" | ''No game scheduled''
|-
! scope="row" |  12
| colspan="8" | ''No game scheduled''
|-
! scope="row" |  13
| colspan="8" | ''No game scheduled''
|-
| colspan="7" | A dagger ({{Dagger}}) indicates teams not affiliated with the APFA.<ref name="nflhist">NFL History (2003), pp. 1–7</ref><ref name="archives">{{Cite web | url=http://www.profootballarchives.com/1920apfaday.html | title=1920 Dayton Triangles (APFA) | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=September 18, 2012}}</ref>
|}

== Game summaries ==

=== Week 2: at Columbus Panhandles ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Triangles'''
|H1=0|H2=0|H3=7|H4=7
}}
''October 3, 1920, at Triangle Park''

The Triangles' opening game against the Columbus Panhandles is considered by football historians to be the first football game between two APFA teams. Since kickoff times were not standardized in 1920, it is unknown if this game or the Muncie–Rock Island game is the first game played. The Triangles won 13–0.<ref name="B&C p. 1">Braunwart & Carroll (1981), p. 1</ref> The Triangles' defense made a goal-line stand in the second quarter while the Panhandles had the ball on the 3-yard line.<ref name="B&C p. 2" /> Before halftime, the Triangles' [[Quarterback|back]] [[Al Mahrt]] completed a 30-yard pass to [[End (American football)|end]] [[Dutch Thiele]] to give the Triangles possession on the 5-yard line.<ref name="B&C p. 2" /> The Triangles failed to convert, however, as time ran out.<ref name="B&C p. 2">Braunwart & Carroll (1981), p. 2</ref> Early in the third quarter, the Triangles started a possession on their own 35-yard line.<ref name="B&C p. 2" /> Four consecutive run plays carried them to midfield.<ref name="B&C p. 2" /> Soon after, back [[Lou Partlow]] had a succession of runs which resulted in a touchdown.<ref name="B&C p. 2" /> The other Triangle score came in the middle of the fourth quarter when end Frank Bacon returned a punt for a 60-yard touchdown.<ref name="B&C p. 2" /> After both touchdowns, [[George Kinderdine]] was responsible for the extra points.<ref name="B&C p. 2" />

=== Week 3: vs. Cleveland Tigers ===
{{Linescore Amfootball
|Road=Tigers
|R1=0|R2=0|R3=0|R4=0
|Home=Triangles
|H1=0|H2=0|H3=0|H4=0
}}
''October 10, 1920, at Triangle Park''

After the historic game, the Triangles played against the Cleveland Tigers. The owner of the Tigers, [[Jimmy O'Donnell]], helped with the foundation of the APFA.<ref>Grabowski (1992), p. 51</ref> No team scored, and the game ended in a 0–0 tie.
{{Clear}}

=== Week 4: vs. Hammond Pros ===
{{Linescore Amfootball
|Road=Pros
|R1=0|R2=0|R3=0|R4=0
|Home='''Triangles'''
|H1=7|H2=20|H3=3|H4=14
}}
''October 17, 1920, at Triangle Park''

The [[Hammond Pros]] were the Triangles' next opponent. In the first quarter, Mahrt had a one-yard rushing touchdown. The Triangles scored three touchdowns in the second quarter: a 50-yard receiving touchdown from Mahrt, a 35-yard receiving touchdown from Reese, and a rushing touchdown from Partlow. The extra point was missed after the first touchdown. In the next quarter, Roudebush kicked a 35-yard field goal. The last score of the game was a receiving touchdown from Sacksteder. The final score of the game was 44–0 before a crowd of 2,000.<ref name="nflhist-2">NFL History (2003), p. 2</ref><!--this is a citation for the whole paragraph-->

=== Week 5: vs. Canton Bulldogs ===
{{Linescore Amfootball
|Road=Bulldogs
|R1=7|R2=7|R3=3|R4=3
|Home=Triangles
|H1=0|H2=14|H3=6|H4=0
}}
''October 24, 1920, at Triangle Park''

In week five, the Triangles battled the Canton Bulldogs, who had the [[Pro Football Hall of Fame|Hall-of-Fame]] back [[Jim Thorpe]].<ref>{{Cite web | title=Jim Thorpe | url=http://www.profootballhof.com/hof/member.aspx?player_id=213 | publisher=[[Pro Football Hall of Fame]] | accessdate=March 4, 2012|archiveurl=http://www.webcitation.org/65ydjUVkB|archivedate=March 6, 2012|deadurl=no}}</ref> The Bulldogs opened the scoring in the first quarter on a two-yard rushing touchdown by [[Pete Calac]].<ref name="hist3" /> But the Triangles came back in the second quarter,<ref name="5news">{{Cite news | url=https://news.google.com/newspapers?id=Ki1KAAAAIBAJ&sjid=_YUMAAAAIBAJ&pg=1232%2C110248 | title=Thorpe Saves Canton Eleven with Two Goals | newspaper=Youngstown Vindicator| date=October 25, 1920 | page=10}}</ref> scoring twice: Bacon had a four-yard rushing touchdown, and end [[Dave Reese]] had a 50-yard receiving touchdown.<ref name="hist3">NFL History (2003), p. 3</ref> Guyon scored a 22-yard rushing touchdown during the second quarter, but the extra point was missed.<ref name="hist3" /> In the third quarter, the Triangles responded with a 3-yard rushing touchdown by Partlow, but Dayton missed the extra point to make the score 20–14.<ref name="hist3" /> Thorpe then came into the game, and kicked a 45-yard field goal to bring his team within three points.<ref name="hist3" /><ref name="5news" /> In the final minutes, Thorpe kicked another 35-yard field goal to tie it.<ref name="hist3" /> The Triangles were the first team to score on the Bulldogs since the opening game of the previous year.<ref name="5news" />

=== Week 6: vs. Cincinnati Celts ===
{{Linescore Amfootball
|Road=Celts
|R1=0|R2=7|R3=0|R4=0
|Home='''Triangles'''
|H1=7|H2=9|H3=7|H4=0
}}
''October 31, 1920, at Triangle Park''

The [[Cincinnati Celts]] were the next opponent for the Triangles. The Celts were not directly affiliated with the APFA and did not join the league until the following season.<ref>{{Cite web | url=http://www.ohiohistorycentral.org/entry.php?rec=2240 | title=Cincinnati Celts | work=Ohio History Center | publisher=[[Ohio Historical Society]] | date=July 1, 2005 | accessdate=March 11, 2012}}</ref> The lone score in the first quarter came from a fumble recovery by [[George Roudebush]], who returned it for a touchdown.<ref name="hist3" /> In the next quarter, Wehringer for the Celts ran an interception back for a touchdown.<ref name="hist3" /> The Triangles score twice in the second quarter: a rushing touchdown from Abrell and a 35-yard field goal from Roudebush.<ref name="hist3" /> Frank Bacon had a 30-yard interception return for a touchdown in the third quarter, en route to the 23–7 victory for the Triangles.<ref name="hist3" />

=== Week 8: at Rock Island Independents ===
{{Linescore Amfootball
|Road='''Triangles'''
|R1=7|R2=0|R3=0|R4=14
|Home=Independents
|H1=0|H2=0|H3=0|H4=0
}}
''November 14, 1920, at Douglas Park''

In week eight, the Triangles played against the Rock Island Independents. The Independents had six players returning from injuries this game.<ref name="8news">{{Cite news | title=Triangles Use Arial Attack to Upset Independents | newspaper=[[Rock Island Argus]] | date=November 15, 1920}}</ref> In the first quarter, [[Rube Ursella]] for the Independents fumbled a punt on the 40-yard line, and the Triangles gained possession.<ref name="8news" /> On that possession, Bacon scored a rushing touchdown.<ref name="nflhist-4">NFL History (2003), p. 4</ref> The Independents controlled the football for a majority of the second quarter. On their final possession of the half, they traveled to the Triangles' four-inch line, but the referee signaled to end the first half.<ref name="8news" /> In the fourth quarter, [[Ed Novack]] and [[Arnold Wyman]] for the Independents left the game due to injury.<ref name="8news" /> The Triangles scored two passing touchdowns in the final 10 minutes of the game; the first was caught by [[Dave Reese]], and the second was caught by Roudebush.<ref name="nflhist-4" />

=== Week 9: vs Akron Pros ===
{{Linescore Amfootball
|Road=Triangles
|R1=0|R2=0|R3=0|R4=0
|Home='''Pros'''
|H1=0|H2=0|H3=0|H4=13
}}
''November 21, 1920, at League Park''

In week nine, the Triangles played against the Akron Pros. The Pros came into this game as one of the few teams left in the APFA who were undefeated.<ref name="Carroll1982-2">Carroll (1982), p. 2</ref> The game started out with three scoreless quarters until [[Rip King]] threw a 15-yard passing touchdown in the fourth quarter to [[Frank McCormick (American football)|Frank McCormick]].<ref name="nflhist-5">NFL History (2003), p. 5</ref> [[Fritz Pollard]], a future Hall-of-Famer, rushed for a 17-yard touchdown, and Charlie Copley made one extra point and missed another one to beat the Triangles 13–0 and give the Triangles their first loss of the season.<ref name="nflhist-5" />

=== Week 10: vs. Detroit Heralds ===
{{Linescore Amfootball
|Road=Heralds
|R1=0|R2=0|R3=0|R4=0
|Home='''Triangles'''
|H1=14|H2=14|H3=0|H4=0
}}
''November 25, 1920, at Triangle Park''

Coming off their first loss of the season, the Triangles played against the [[Detroit Heralds]]. Bacon contributed to every point in the first quarter; he first a 3-yard rushing touchdown, and followed up by a catching a receiving touchdown.<ref name="nflhist-5" /> [[Dick Abrell]] contributed in the second quarter, as he scored a rushing touchdown.<ref name="nflhist-5" /> Sacksteder caught a receiving touchdown from Roudebush to put the Triangles up 28–0 at halftime.<ref name="nflhist-5" /> The second half was scoreless, and the Triangles moved on to a 5–1–2 record with one game left in their season.

=== Week 10: vs. Akron Pros ===
{{Linescore Amfootball
|Road='''Pros'''
|R1=0|R2=0|R3=7|R4=7
|Home=Triangles
|H1=0|H2=0|H3=0|H4=0
}}
''November 28, 1920, at Triangle Park''

The Pros were now recognized as the top team in Ohio,<ref name="Carroll1982-2" /> and the Triangles had a rematch against the Pros. The game could have been classified as a World Championship, but the APFA had widened its battlefield with the Buffalo All-Americans and the Decatur Staleys still in contention for the APFA trophy.<ref name="Carroll1982-2" /> 5,000 fans showed up for the game.<ref name="archives" /> Pollard returned a punt for a touchdown in the first quarter and had one receiving touchdown in the third quarter from King,<ref name="nflhist-6">NFL History (2003), p. 6</ref> and the Triangles received their second loss of the year.

== Post-season ==
Hurt by losses to the Akron Pros, the Triangles did not contend for the APFA trophy in 1920. The Triangles' performance of 5–2–2 would be the team's best before folding in 1929.<ref>{{Cite web | url=http://www.pro-football-reference.com/teams/day/ | title=Dayton Triangles Team Encyclopedia | work=''Pro-Football-Reference.com'' | publisher=Sports Reference | accessdate=July 16, 2012}}</ref> Sportswriter Bruce Copeland compiled the 1920 All Pro team; no players made the first team, Sacksteder made the second team, and Bacon made the third team.<ref>{{Cite journal | last=Hogrogian | first=John | year=1984 | url=http://www.profootballresearchers.org/Coffin_Corner/06-01-173.pdf | title=1920 All-Pros | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=6 | issue=1}}</ref> As of 2012, no players from the 1920 Dayton Triangles were enshrined in the Pro Football Hall of Fame.<ref>{{Cite web | url=http://www.profootballhof.com/hof/alphabetical.aspx | title=Alphabetically | publisher=[[Pro Football Hall of Fame]] | accessdate=July 16, 2012}}</ref>

== Roster ==

{| class="toccolours" style="text-align: left;"
|-
! colspan="9" style="background:#00285D; color:white; text-align:center;"|'''Dayton Triangles 1920 roster'''<ref name="roster">{{Cite web | url=http://www.pro-football-reference.com/teams/day/1920_roster.htm | title=1920 Dayton Triangles Starters, Rosters, & Players | work=''[[Pro-Football-Reference.com]]'' | publisher=Sports Reference | accessdate=March 3, 2012}}</ref><ref>{{Cite web | last1=Presar | first1=Steve | last2=Rushle | first2=Bernie | date=July 18, 2007 | title=Dayton Triangles (1920–1929) | url=http://www.rci.rutgers.edu/~maxymuk/home/ongoing/dayton.html | publisher=[[Rutgers University]] | accessdate=March 4, 2012|archiveurl=http://www.webcitation.org/65ydjrmAL|archivedate=March 6, 2012|deadurl=no}}</ref> 
|-
|
| style="font-size:95%; vertical-align:top;"| 
* [[Dick Abrell]] ([[Back (American football)|B]])
* [[Frank Bacon (American football)|Frank Bacon]] (B/E)
* [[Max Broadhurst]] ([[Tackle (American football)|T]])
* [[Bill Clark]] (G/C)
* [[Harry Cutler]] (T)
* [[Doc Davis]] (G/T)
* [[Larry Dellinger]] (G/T)
* [[Guy Early]] (G/B)
* [[Lee Fenner]] (E/B)
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| 
* [[Russ Hathaway]] (T/G)
* [[Earl Hauser]] (E/T)
* [[Chuck Helvie]] (E)
* [[Hobby Kinderdine]] (C)
* [[Pesty Lentz]] (B)
* [[Al Mahrt]] (B)
* [[Lou Partlow]] (B)
* [[Dave Reese]] (E)
* [[George Roudebush]] (B)
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| 
* [[Norb Sacksteder]] (T/B)
* [[Ed Sauer]] (T/G)
* [[Fritz Slackford]] (B/E)
* [[Earl Stoecklein]] (G)
* [[Dutch Thiele]] (E)
* [[Glenn Tidd]] (C/G)
* [[Nelson Talbot]] (Coach)
* [[Tiny Turner]] (G)
* [[Charlie Winston]] ([[Guard (American football)|G]])
|}

== Standings ==
{{1920 APFA standings}}

== Notes ==
{{Reflist|30em}}

== References ==
* {{Cite journal | last1=Braunwart | first1=Bob | last2=Carroll | first2=Bob | year=1981 | url=http://www.profootballresearchers.org/Coffin_Corner/03-02-059.pdf | title=The First NFL Game(s) | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=3 | issue=2 | archiveurl=http://www.webcitation.org/65r52K7At | archivedate=March 1, 2012}}
* {{Cite journal | last=Carroll | first=Bob | year=1982 | url=http://www.profootballresearchers.org/Coffin_Corner/04-12-119.pdf | title=Akron Pros 1920 | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=4 | issue=12 | archiveurl=http://www.webcitation.org/65r52lUNU | archivedate=March 1, 2012}}
* {{Cite book | last=Davis | first=Jeff | title=Papa Bear, The Life and Legacy of George Halas | publisher=McGraw-Hill | year=2005 | isbn=0-07-146054-3}}
* {{Cite book | last=Grabowski | first=John | year=1992 | url=https://books.google.com/books?id=UUy9tHItNu8C&dq=1920+cleveland+tigers&source=gbs_navlinks_s | title=Sports in Cleveland: An Illustrated History | publisher=Indiana University Press | isbn=9780253207470}}
* {{Cite book | last=Peterson | first=Robert | year=1997 | title=Pigskin: The Early Years of Pro Football | publisher=[[Oxford University Press]] | isbn=0-19-507607-9}}
* {{Cite journal | author=PFRA Research | year=1980 | url=http://www.profootballresearchers.org/Coffin_Corner/02-08-038.pdf | title=Happy Birthday NFL? | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=2 | issue=8 | archiveurl=http://www.webcitation.org/65r56f5pq | archivedate=March 1, 2012}}
* {{Cite book | last1=Siwoff | first1=Seymour | last2=Zimmber | first2=Jon | last3=Marini | first3=Matt | year=2010 | title=The Official NFL Record and Fact Book 2010 | publisher=[[National Football League]] | isbn=978-1-60320-833-8}}

{{1920 APFA season by team}}
{{Dayton Triangles seasons}}

{{DEFAULTSORT:1920 Dayton Triangles Season}}
[[Category:Dayton Triangles seasons]]
[[Category:1920 American Professional Football Association season by team|Dayton Triangles]]