{{Use dmy dates|date=December 2013}}
{{Infobox military person
| name          =Francis William Lionel Collings Beaumont
| image         = | caption       =
| birth_date          = {{Birth date|1903|08|6|df=yes}}
| death_date          = {{Death date and age|1941|05|4|1903|08|6|df=yes}} 
| placeofburial_label = 
| placeofburial = [[Liverpool]], England 
| birth_place  =
| death_place  = [[Liverpool]], England
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      = Buster
| allegiance    =
| branch        = {{air force|United Kingdom}}
| serviceyears  = 1923–1928, 1939–1941
| rank          = [[Flight Lieutenant]]
| unit          =
| commands =
| battles       =[[Second World War]]
| awards        =
| relations     = [[Mary Lawson (actress)|Mary Lawson]], wife; [[Sibyl Hathaway]], mother; [[Dudley Beaumont]], father; [[Michael Beaumont, 22nd Seigneur of Sark|Michael Beaumont]], son
| laterwork     = Film producer
}}
'''Francis William Lionel Collings Beaumont''' (6 August 1903 – 4 May 1941), also known as '''F. W. L. C. Beaumont''' or '''“Buster" Beaumont''', was the heir to the [[Seigneur of Sark]], a [[Royal Air Force]] officer, film producer and the husband of actress [[Mary Lawson (actress)|Mary Lawson]]. He and Lawson were killed in 1941 during the [[Liverpool Blitz]].

==Family background==
Francis William Lionel Collings Beaumont was born on 6 August 1903.<ref name= oeaxls>{{cite web
 | publisher= The Old Elizabethan Association
 | url=http://www.oea.org.gg/system/files?file=WW2+details_0.xls
 | title= Old Elizabethans Killed During World War Two
 | accessdate=5 June 2009

}}</ref><ref name= Rootsweb >{{cite web
 | publisher=Rootsweb | year= 1997
 | url=http://archiver.rootsweb.ancestry.com/th/read/CHANNEL-ISLANDS/1997-06/0867496020
 | title=CHANNEL-ISLANDS-L Archives
 | accessdate=5 June 2009

}}</ref> He was the second child of [[Dudley Beaumont|Dudley]] and [[Sibyl Hathaway|Sibyl Beaumont]], daughter of [[William Frederick Collings]], who ruled the island of [[Sark]] as seigneur (feudal lord).<ref name=Rootsweb/><ref name=cwgcbeaumont>
{{cite web
  | publisher=[[Commonwealth War Graves Commission]]
  | url=http://www.cwgc.org/search/casualty_details.aspx?casualty=2412247
  | title=Casualty Details: Francis William Lionel C. Beaumont
  | accessdate=3 June 2009
}}</ref><ref name= cmg >{{cite web
 | publisher= Clan Moffat Genealogy | year= 2003
 | url= http://genealogy.clanmoffat.org/getperson.php?personID=I43587&tree=ClanMoffat 
 | title= Dudley John Beaumont
 | accessdate=5 June 2009

}}</ref> Sark is a self-governing territory that is part of the [[Bailiwick of Guernsey]], a [[British Isles (terminology)|British]] [[Crown Dependency]] in the [[English Channel]] off the coast of [[Normandy]]. The island has been called one of the last [[feudal]] outposts in Western Europe, a term used by Beaumont's mother to describe the island's political system.<ref name=hathaway2>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|page=2}}</ref> Beaumont was named after his grandfather, [[William Frederick Collings]], the 20th Seigneur of Sark,<ref name=cmg2>{{cite web
 | publisher= Clan Moffat Genealogy | year= 2003
 | url= http://genealogy.clanmoffat.org/getperson.php?personID=I43583&tree=ClanMoffat
 | title= William Frederick Collings
 | accessdate=5 June 2009
}}</ref> but was often called by his nickname, Buster.<ref name=hathaway70>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|page=70}}</ref> On his father's side of the family, Beaumont descended from a line of notable [[British Army]] officers. His father served in France during the [[First World War]] and died in 1918 during the [[Spanish flu pandemic]].<ref name=cwgc>
{{cite web
  | publisher=[[Commonwealth War Graves Commission]]
  | url=http://www.cwgc.org/search/casualty_details.aspx?casualty=402127
  | title=Casualty Details: D. J. Beaumont
  | accessdate=3 June 2009
}}</ref><ref name=hathaway59>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography|year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|page=59}}</ref> His grandfather, [[William Spencer Beaumont]], was a captain in the [[14th King's Hussars]] [[Cavalry regiments of the British Army|cavalry regiment]],<ref name=cwgc/> while his great-great grandfather, [[John Thomas Barber Beaumont]] was a well known [[Portrait miniature|miniature painter]] who in 1803, during the [[Napoleonic Wars]], raised [[The Duke of Cumberland's Sharp Shooters]] corps.<ref name=nc>{{cite journal|last= |first= |authorlink= |year= 1886–1887|title=John Thomas Barber Beaumon, 1774–1841 |journal=The Numismatic Chronicle and Journal of the Numismatic Society |volume=VII |issue= |pages=265–268 |id= |url=https://archive.org/details/thirdnumismatic07royauoft |accessdate=7 June 2009 |quote= }}</ref>

In 1927, Beaumont's grandfather died and his mother became inherited the fief. As the oldest son, Beaumont was the heir apparent to the seigneurship.

== Education and early military career ==
Beaumont was educated at [[Elizabeth College, Guernsey|Elizabeth College]], in [[St Peter Port]], [[Guernsey]].<ref name= oeaxls/> Beaumont's mother wanted her son to study at the [[Royal Air Force College Cranwell|Royal Air Force College]] at [[Cranwell]], but she faced financial difficulties following her husband's death. She was forced to sell her farm on Sark and relocate to Guernsey and then [[Cologne]], Germany in order to support her family.<ref name=hathaway6062>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|pages=60–62}}</ref> While still a cadet, Beaumont became an able pilot and participated in an RAF aerial show at [[Hendon Aerodrome|Hendon]].<ref name=hathaway6062>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|pages=68}}</ref> In December 1923 Beaumont graduated from Cranwell, was commissioned as a [[pilot officer]] on 19 December, and posted to [[No. 207 Squadron RAF|No. 207 Squadron]] at [[RAF Eastchurch|Eastchurch]];<ref>{{LondonGazette|issue=32900|supp=yes|startpage=688|date=22 January 1924|accessdate=9 June 2009}}</ref><ref name=flightjan12724>{{cite journal|last= |first= |authorlink= |date=17 January 1924|title=The Royal Air Force |journal=[[Flight International|Flight]] |volume= XVI |issue=3 |page=39 |id= No. 786 |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200039.html |accessdate=7 June 2009 |quote= }}</ref><ref name=flightjan311924>{{cite journal|last= |first= |authorlink= |date=31 January 1924 |title=The Royal Air Force  |journal=[[Flight International|Flight]] |volume=XVI |issue=5 |page=69 |id=No. 788 |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200069.html |accessdate=7 June 2009 |quote= }}</ref> on 19 June 1925 he was promoted to [[flying officer]].<ref>{{LondonGazette|issue=33094|startpage=6776|date=20 October 1925|accessdate=9 June 2009}}</ref><ref name=flight1925oct29>{{cite journal|last= |first= |authorlink= |date=29 October 1925 |title=The Royal Air Force |journal=[[Flight International|Flight]] |volume=XVII |issue=44 |page=711 |id=No. 879 |url= |accessdate=7 June 2009 |quote= }}</ref> While serving Beaumont accumulated personal financial debts that threatened to end his career. His mother intervened with contacts in the RAF and pleaded for her son to be deployed abroad where he would be "away from the crowd of young irresponsibles he is running around with."<ref name=hathaway68>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|page=68}}</ref> In November 1925 Beaumont was assigned to [[No. 45 Squadron RAF|No. 45 Squadron]] in Iraq, where the British had established the [[British Mandate of Mesopotamia|Mandate of Mesopotamia]].<ref name=flightjan726>{{cite journal|last= |first= |authorlink= |date=7 January 1926 |title=The Royal Air Force |journal=[[Flight International|Flight]] |volume=XVIII |issue=1 |page=13 |id= No. 889|url= |accessdate=7 June 2009 |quote= }}</ref> In 1928 Beaumont was assigned to the RAF depot at [[RAF Uxbridge|Uxbridge]] near London,<ref name=flight1928sep20>{{cite journal|last= |first= |authorlink= |date=20 September 1928 |title=The Royal Air Force  |journal=[[Flight International|Flight]] |volume= XX |issue= 38 |page=891 |id= No. 1030 |url=http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200891.html |accessdate=7 June 2009 |quote= }}</ref> and resigned his commission in the RAF on 3 October 1928.<ref>{{LondonGazette|issue=33426|startpage=6349|date=2 October 1928|accessdate=9 June 2009}}</ref><ref name=flight1928oct11>{{cite journal|last= |first= |authorlink= |date=11 October 1928 |title=The Royal Air Force  |journal=[[Flight International|Flight]] |volume=XX |issue=41 |page=967 |id= No. 1033|url=http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200967.html |accessdate=7 June 2009 |quote= }}</ref>

== Marriages ==
In 1924, while serving in the RAF, it was announced that Beaumont was engaged to Enid Corinne Ripley of [[Outwood, Surrey]] and in October 1926 the couple were married in London.<ref name=flightoct21924>{{cite journal|last= |first= |authorlink= |date=2 October 1924 |title=Personals |journal=[[Flight International|Flight]] |volume=XVI |issue=40 |page=645 |id= No. 823|url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200645.html |accessdate=7 June 2009 |quote= }}</ref><ref name=flightoct141926>{{cite journal|last= |first= |authorlink= |date=14 October 1926 |title=Personals |journal=[[Flight International|Flight]] |volume= XVIII |issue=41 |page=678 |id= No. 929 |url=http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200762.html |accessdate=7 June 2009 |quote= }}</ref><ref name=timessep221926>{{cite newspaper The Times |articlename= Marriages|section=Marriages |day_of_week= |date=22 September 1926 |page_number=15 |page_numbers= |issue=44383 |column= C  }}
</ref><ref name=hathaway71>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|page=71}}</ref> In December 1927, Enid Beaumont gave birth to a son,<ref name=timesdec211927>{{cite newspaper The Times |articlename= Births|section=Births|day_of_week= |date=|page_number=1 |page_numbers= |issue=44770|column= A }}</ref> [[John Michael Beaumont]], who would become the 22nd Seigneur of Sark in 1974.

Beaumont met his second wife, actress [[Mary Lawson (actress)|Mary Lawson]], while producing the 1936 film ''Toilers of the Sea'', a film adaption of [[Victor Hugo]]'s 1866 novel ''[[Les Travailleurs de la mer]]''.<ref name=imdbtoilers>{{IMDb title|0028392|Toilers of the Sea}}</ref> Hugo's book is set in Guernsey and Beaumont's mother writes in her 1961 autobiography that scenes from the film were shot on Sark and that her son provided backing for the film, along with French director/producer [[Jean Choux]];<ref name=Choux>{{IMDb name|0159414| Jean Choux}}</ref><ref name=hathaway1045>Hathaway incorrectly writes that the film was shot in 1938. {{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|pages=104–5}}</ref> in the film credits the production company L. C. Beaumont is mentioned, but not Choux.<ref name=Choux/><ref name=Beaumont>{{imdb company|0013160|L.C. Beaumont}}</ref> 
At this time Beaumont was still married to his first wife. It is uncertain when the affair between Lawson and Beaumont began, but Beaumont's wife purchased an announcement in the 30 November 1937 edition of [[The Times]] asking for a "dissolution" of their marriage "on the ground of his adultery with Miss Mary Lawson."<ref name=timesnov301937>{{cite newspaper The Times |articlename=Probate, Divorce, And Admiralty Division Decree Nisi Against Film Producer, Beaumont, E. C. v. Beaumont, F. W. L. C.|section= |day_of_week= |date= 30 November 1937|page_number=4|page_numbers= |issue=47855|column=E}}</ref> That year the Beaumonts were divorced,<ref name=na>[http://www.nationalarchives.gov.uk/catalogue/displaycataloguedetails.asp?CATID=-3173540&CATLN=7&accessmethod= ''Catalogue description for Document No. J 77/3752/4301. Divorce Court File: 4301. Appellant: Enid Corinne Beaumont. Respondent: Francis William Lionel C Beaumont. Type: Wife's petition for divorce [wd&#93;]'' 1937. [[The National Archives]], [[Kew]]</ref> and on 22 June 1938 Beumont and Lawson were married in [[Chelsea, London|Chelsea]].<ref name=timesjun231938>{{cite newspaper The Times |articlename=News in Brief|section= |day_of_week= |date= 23 June 1938|page_number=14|page_numbers= |issue=48028|column=C}}</ref> In her memoirs, Beaumont's mother makes no mention of her son's second wife, rather she praises his first wife as a "charming girl"<ref name="hathaway70"/> Upon marriage Lawson legally changed her name to Mary Elizabeth Beaumont, but she continued to use Mary Lawson as her stage name.<ref name=Gazettejul41>{{LondonGazette|issue=35208|date=4 July 1941|startpage=3854|accessdate=1 June 2009}}</ref>

== Fascist connection and the Second World War ==
In 1937 Beaumont reportedly met with [[Oswald Mosley]], founder of the [[British Union of Fascists]], to discuss the opening of a private radio broadcast station on Sark. In the 1930s, the [[British Broadcasting Corporation]] had a government sanctioned monopoly in Britain on broadcasting television and radio and private commercially broadcasts funded by advertisers were illegal. Mosley wanted to challenge this monopoly in order to raise funds for his fledgling party by setting up broadcast stations outside of the BBC's jurisdiction. Mosley and Beaumont reportedly came to an agreement for a thirty-year lease to set up a ''Radio Sark'' broadcast station on the island.<ref name=amato>Amato quotes national archive document [http://www.nationalarchives.gov.uk/catalogue/displaycataloguedetails.asp?CATLN=6&CATID=135789 HO 283/11], which states that among the property seized following Mosley's arrest by the British government in 1940 was correspondence between Mosley and Beaumont dating from 1937.
{{cite book |last= Amato |first= Joseph Anthony |authorlink= |title= Rethinking home: a case for writing local history
 |year=2002 |publisher=[[University of California Press]] |location=[[Berkeley, California|Berkeley]] |isbn=978-0-520-23293-8 |url=https://books.google.com/?id=3b2g1RD5fUAC&pg=PA278&lpg=PA278&dq=buster+beaumont |pages=278–79 }}</ref><ref name=barnes>{{cite book |last= Barnes |first=James J. |authorlink= |author2=Patience P. Barnes|title=Nazis in pre-war London, 1930–1939: the fate and role of German party members and British sympathizers |year=2005 |publisher=[[Sussex Academic Press]] |location=[[Brighton]] |isbn=978-1-84519-053-8 |url=https://books.google.com/?id=RvzUAubHJ9IC&pg=PA139&dq=beaumont+station+sark#PPA139,M1 |pages= }}</ref> There have been claims that Beaumont was sympathetic to the Mosley's movement and that he and Mosley were amiable. Beaumont is also reported to have hidden the source of the funding of the radio station from his mother in order to obtain her approval.<ref name=barnes/> In addition, British government documents opened to the public in the 1990s reveal that Mosley sought funding from the German [[Nazi government]] for Radio Sark, though Beaumont is not implicated in these dealings.<ref name=independent>{{cite news 
  | last =Crossland 
  | first = John
  | title = Mosley's links with Berlin are revealed 
  | work = 
  | pages = 
  | language = 
  | publisher =[[The Independent]]
  | date = 1 October 1996
  | url = http://www.independent.co.uk/news/mosleys-links-with-berlin-are-revealed-1356152.html
  | accessdate = }}
</ref>

Despite his connections with British fascists, Beaumont promptly rejoined the RAF at the outset of the [[Second World War]] in 1939; he was recommissioned as a pilot officer on probation on 26 September,<ref name=Gazette34700>{{LondonGazette|issue=34700|date=3 October 1939|startpage=6661|endpage=6662|accessdate=7 June 2009}}</ref><ref>{{LondonGazette|issue=34960|date=4 October 1940|startpage=5838|accessdate=7 June 2009}}</ref> and on 1 June 1940 he was promoted to war substantive flying officer.<ref>{{LondonGazette|issue=35042|startpage=284|endpage=286|date=14 January 1941|accessdate=9 June 2009}}</ref><ref name=flight1941jan30>{{cite journal|last= |first= |authorlink= |date=30 January 1941|title=Service Aviation |journal=[[Flight International|Flight]] |volume=XXXIX |issue= |page= 103 |id= No. 1675 |url=http://www.flightglobal.com/pdfarchive/view/1941/1941%20-%200289.html |accessdate=7 June 2009 |quote= }}</ref> He seems later to have been promoted [[flight lieutenant]].<ref name=cwgc /> He was assigned to the Administrative and Special Duties Branch, which includes the [[RAF Intelligence|RAF's intelligence section]].<ref name=Gazette34700 /><ref name=flight1940oct10>{{cite journal|last= |first= |authorlink= |date=10 October 1940  |title=The Royal Air Force  |journal=[[Flight International|Flight]] |volume=XXXVIII |issue= |page=314 |id=No. 1659 |url= |accessdate=7 June 2009 |quote= }}</ref>

During the war the [[German occupation of the Channel Islands|Channel Islands were occupied by German forces]] and contact was severed between Beaumont and his relatives on Sark, including his mother, who remained on the island for the duration of the war.

== Death ==
In May 1941 Beaumont received a week's leave and he, Lawson, friends and family travelled to [[Liverpool]],<ref name=echo>{{cite news 
  | last = Lloyd
  | first = Chris 
  | title =Echo memories – Tragic star whose light was snuffed out too early
  | work = 
  | pages = 6b
  | language = 
  | publisher =[[The Northern Echo]]
  | date = 19 March 2003
  | url = 
  | accessdate = }}
</ref> where they stayed at a hotel at 126 [[Smithdown Road, Liverpool|Smithdown Road]] in the [[Sefton Park]] district.<ref name=Gazettejul41/><ref name=hathaway130>{{cite book |last=Hathaway |first=Sibyl |authorlink=Sibyl Hathaway |title=Dame of Sark: An Autobiography |year=1962 |publisher=[[Coward-McCann, Inc]] |location=New York |id= | url =https://archive.org/details/dameofsark006367mbp 
|pages=130–31}}</ref> On 1 May the German [[Luftwaffe]] began a [[Liverpool Blitz|bombing campaign on Liverpool]] that would last more than a week. On 4 May, as the warning sirens went off, family and friends at the hotel, including Lawson's sister Dorothy, took safety in a shelter, while Lawson and Beaumont stayed in their room.<ref name=echo/> The hotel was destroyed, killing the couple, while all those who sought safety in the shelter survived.<ref name=echo/> The death of Beaumont and Lawson was announced in newspapers around the globe,<ref name=hathaway130/><ref name=timesmay101941beaumont>{{cite newspaper The Times |articlename= Deaths|section=Deaths|day_of_week= |date=10 May 1941|page_number=1 |page_numbers= |issue=48922|column= A  }}
</ref><ref name=timesmay101941>{{cite newspaper The Times |articlename= Second Raid on Humber Area Many Casualties, Other Attacks in North Midlands|section=|day_of_week= |date=10 May 1941|page_number=2 |page_numbers= |issue=48922|column= C }}
</ref><ref name=Chicago>{{cite news 
  | last = 
  | first = 
  | title = Mary Lawson, British Actress, Killed in Raid
  | work = 
  | pages = 8
  | language = 
  | publisher =[[Chicago Tribune]]
  | date = 10 May 1941
  | url = 
  | accessdate = }}
</ref> but news of Beaumont's death was slow to reach Sark because of the German occupation.<ref name=hathaway130/> Beaumont's elder sister, Amice, who was in England at the time, contacted the embassy of the United States of America, which had yet to enter the war, and ask them to convey the news of her brother's passing to German authorities.<ref name=hathaway130/> Beaumont's mother was notified of her son's death by the [[German Commandant in Guernsey]], a Colonel Rudolf Graf von Schmettow, who conveyed the news in a manner that she described as "gently as possible."<ref name=hathaway130/>

Beaumont and Lawson are buried in Kirkdale Cemetery, Liverpool.<ref name=cwgcbeaumont /><ref name=cwgclawson>
{{cite web
  | publisher=[[Commonwealth War Graves Commission]]
  | url=http://www.cwgc.org/search/casualty_details.aspx?casualty=3117577
  | title=Casualty Details: Mary Elizabeth Beaumont
  | accessdate=3 June 2009
}}</ref> A memorial plaque with names of Beaumont and other former pupils of Elizabeth College that fell during the Second World War is located on the school grounds in Guernsey.<ref name= oea.org.ggplaque>
{{cite web
  | publisher= The Old Elizabethan Association
  | url= http://www.oea.org.gg/taxonomy/term/43
  | title= The OE Memorial
  | accessdate=3 June 2009
}}</ref>

== References ==
{{Reflist|2}}

{{DEFAULTSORT:Beaumont, Francis William Lionel Collings}}
[[Category:British military personnel killed in World War II]]
[[Category:Royal Air Force officers]]
[[Category:People from Sark]]
[[Category:British film producers]]
[[Category:1941 deaths]]
[[Category:1903 births]]
[[Category:People educated at Elizabeth College, Guernsey]]
[[Category:Graduates of the Royal Air Force College Cranwell]]
[[Category:Deaths by airstrike during World War II]]