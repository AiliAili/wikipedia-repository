{{For|the Los Angeles bar of the same name|Black Cat Tavern}}
{{coord|37.795818|N| 122.403257|W|type:landmark_region:US-CA|display=title}}
{{Infobox Restaurant
| name              = Black Cat Bar
| image             = Exterior of Black Cat Bar, San Francisco.jpg
| image_caption     = The exterior of the Black Cat. Image courtesy of the San Francisco History Center, San Francisco Public Library
| established       = 1906, 1933
| street-address    = 710 Montgomery St
| city              = San Francisco
| state             = California
| country           = United States
}}
The '''Black Cat Bar''' or '''Black Cat Café''' was a bar in [[San Francisco, California]]. It originally opened in 1906 and closed in 1921. The Black Cat re-opened in 1933 and operated for another 30 years. During its second run of operation, it was a hangout for [[Beat Movement|Beats]] and [[Bohemianism|bohemians]] but over time began attracting more and more of a [[homosexuality|gay]] clientele.

Because it catered to gay people, the bar became a flashpoint for the nascent [[homophile]] movement. The Black Cat was at the center of a legal fight that was one of the earliest court cases to establish legal protections for gay people in the United States. Despite this victory, continued pressure from law enforcement agencies eventually forced the bar's closure in 1964.

==The original Black Cat==
The Black Cat opened in 1906, shortly after the [[1906 San Francisco earthquake]]. In the early years, the bar was located in the basement of the Athens Hotel at 56 Mason Street in San Francisco's [[Tenderloin, San Francisco|Tenderloin]] neighborhood. This building still stands today and is now the Bristol Hotel. When entrepreneur [[Charles Ridley]] acquired the bar in 1911, he turned it into a showplace for [[vaudeville]]-style acts. Over the next several years, Ridley and the Black Cat came under increased police scrutiny as a possible center of [[prostitution]]. In 1921, the bar lost its [[dance permit]] and closed down.<ref name = Boyd56>Boyd p. 56</ref><ref>[http://www.sfgate.com/bayarea/article/Boisterous-dive-s-saga-is-the-story-of-S-F-s-5861766.php Gary Kamiya, "Boisterous Dive's Saga Is the Story of SF's Seamier Side", ''San Francisco Chronicle'' (October 31, 2014)]</ref>

==Beats and bohemians==

With the repeal of [[Prohibition in the United States|Prohibition]], the Black Cat re-opened in 1933 at 710 Montgomery Street,<ref name = Laird>{{cite news 
  | last = Laird
  | first = Cynthia
  | title = News in brief: Legendary gay bar to be remembered
  | work = The Bay Area Reporter Online
  | date = 2007-12-15
  | url = http://www.ebar.com/news/article.php?sec=news&article=2532
  | accessdate = 2008-06-22}}</ref> again under Ridley's proprietorship.<ref name = Boyd56 /> Sol Stoumen bought the bar in the 1940s.<ref name = Boyd57>Boyd p. 57</ref> In the early years of Stoumen's ownership, the Black Cat was a center for the bohemian and [[Beat Generation|Beat]] crowd. [[William Saroyan]] and [[John Steinbeck]] were known to frequent the establishment, and part of [[Jack Kerouac]]'s seminal Beat novel ''[[On the Road]]'' is set in the bar.<ref>Miller p. 346</ref>

==Growing gay clientele==
While the Beats continued to congregate at the Black Cat into the 1950s, in the years following [[World War II]], more and more gay people began patronizing it. The varied crowds mixed and gay Beat poet [[Allen Ginsberg]] described the Black Cat as "the best gay bar in America. It was totally open, bohemian, San Francisco...and everybody went there, heterosexual and homosexual....All the gay screaming queens would come, the heterosexual gray flannel suit types, longshoremen. All the poets went there."<ref name = JD187>D'Emilio p. 187</ref> By 1951, the bar was placed on the [[Armed Forces Disciplinary Control Board|Armed Forces Disciplinary Control Board's]] list of establishments which military personnel were forbidden to enter.<ref name = Boyd57 />

The bar featured live entertainers, the best known of whom was [[José Sarria]]. Sarria, who began as a waiter, wore [[drag (clothing)|drag]] and entertained the crowd by singing parodies of popular [[torch song]]s. Eventually he performed three to four shows a night, along with a regular Sunday afternoon show, with Sarria performing full [[aria]]s. His specialty was a re-working of [[Georges Bizet|Bizet's]] opera ''[[Carmen]]'', set in modern-day San Francisco. Sarria as Carmen would prowl through popular [[Cruising for sex|cruising]] area [[Union Square, San Francisco|Union Square]]. The audience cheered "Carmen" on as she dodged the [[Vice unit|vice squad]] and made her escape.<ref name = JD187 />

[[File:Interior of Black Cat Bar, San Francisco.jpg|left|thumb|The interior of the bar. Tables were pushed together to form a makeshift stage for live entertainment. Image courtesy of the San Francisco History Center, San Francisco Public Library]]
Sarria encouraged patrons to be as open and honest as possible, exhorting the clientele, "There's nothing wrong with being gay–the crime is getting caught," and "United we stand, divided they catch us one by one."<ref name = Shilts52>Shilts p. 52</ref> At closing time, he would lead patrons in singing "God Save Us Nelly Queens" to the tune of "[[God Save the Queen]]". Sometimes he would take the crowd outside to sing the final verse to the men across the street in jail, who had been arrested in raids earlier in the night.<ref name = Shilts52 /> Speaking of this ritual in the film ''[[Word is Out: Stories of Some of Our Lives|Word is Out]]'' (1977), gay journalist [[George Mendenhall]] said: <blockquote>"It sounds silly, but if you lived at that time and had the oppression coming down from the police department and from society, there was nowhere to turn...and to be able to put your arms around other gay men and to be able to stand up and sing 'God Save Us Nelly Queens'...we were really not saying 'God Save Us Nelly Queens.' We were saying 'We have our rights, too.'"<ref>{{Cite video
  | people = Mariposa Film Group
  | title = Word is Out: Stories of Some of Our Lives
  | medium = Theatrical film
  | publisher = Mariposa Film Group
  | location = United States
  | date = 1977}}</ref></blockquote>

Sarria became the first openly gay candidate in the United States to run for public office, running in 1961 for a seat on the [[San Francisco Board of Supervisors]].<ref name = Miller347>Miller p. 347</ref> Sarria almost won by default. On the last day for candidates to file petitions, city officials realized that there were fewer than five candidates running for the five open seats, which would have assured Sarria a seat. By the end of the day, 34 candidates had filed.<ref>Witt, et al. p. 8</ref> Sarria garnered some 6,000 votes,<ref name = Miller347 /> shocking political pundits and setting in motion the idea that a gay voting bloc could wield real power in city politics.<ref>Shilts pp. 56&ndash;7</ref> As Sarria put it, "From that day on, nobody ran for anything in San Francisco without knocking on the door of the gay community."<ref>Lockhart p. 36</ref>

==Police harassment==
In 1948, the [[San Francisco Police Department]] and the Alcohol Beverage Control Commission, in response to the Black Cat's increasing homosexual clientele, began a campaign of harassment against the bar and its patrons. Bar owner Stoumen was charged with such crimes as "keeping a disorderly house" and the State Board of Equalization suspended the bar's [[liquor license]] indefinitely. In response and on principle, Stoumen, who was [[heterosexual]], took the state to court. In 1951, the [[California Supreme Court]], in ''Stoumen v. Reilly'' (37 Cal.2d 713)<ref>[http://scocal.stanford.edu/opinion/stoumen-v-reilly-29515 ''Stoumen v. Reilly'', 37 Cal. 2d 713)]</ref> ruled that "[i]n order to establish 'good cause' for suspension of plaintiff's license, something more must be shown than that many of his patrons were homosexuals and that they used his restaurant and bar as a meeting place." This was one of the earliest legal affirmations of the [[LGBT social movements|rights of gay people]] in the United States. The court qualified its opinion, however, by stating that ABC might still close gay bars with "proof of the commission of illegal or immoral acts on the premises."<ref name = Esk94>Eskridge p. 94</ref>

In response to this legal victory and based on the "illegal or immoral acts" language of the opinion, the state passed a constitutional amendment creating the [[California Department of Alcoholic Beverage Control]] (ABC). The [[California State Assembly]] in 1955 passed a law authorizing broad powers for the ABC to shut down any "resort [for] sexual perverts." The Black Cat was shut down under this authority, along with a number of other establishments. In a [[Test case (law)|test case]] involving an [[Oakland, California|Oakland]] bar, ''Vallerga v. Department of Alcoholic Beverage Control'', the California Supreme Court struck down this new law as unconstitutional.<ref>{{Cite court
  |litigants = Vallerga v. Dept. Alcoholic Bev. Control
  |vol = 53 
  |reporter = Cal.2d 
  |opinion = 313
  |pinpoint = 
  |court = Supreme Court of California
  |date = 12-23-1959
  |url= http://scocal.stanford.edu/opinion/vallerga-v-dept-alcoholic-bev-control-29822}}</ref> This decision was not a complete victory, as the court noted that had the ABC's revocation been based on "reports of women dancing with other women and women kissing other women" it might have upheld the law. Homosexuals, therefore, had won the right to assemble but only if they agreed not to touch.<ref name = Esk94 />

Police and city officials responded to the increasing visibility of the Black Cat and other gay bars in the city, and the Black Cat's success in court, by increasingly cracking down, staging more frequent raids and mass arrests. One favorite tactic was to arrest [[drag queen]]s, since impersonating a member of the opposite sex was, at the time, a crime. Sarria responded by passing out labels for the drag queens to wear reading "I am a boy" so it could not be claimed they were impersonating women.<ref>Shilts p. 53</ref>

==Closure==
By 1963, following some 15 years of unrelenting pressure from the police and the ABC, Stoumen decided he was no longer able financially to sustain the fight. The cost of his long legal battle was more than $38,000.<ref name = JD187 /> Sarria tried to enlist the owners of the city's other gay bars to help Stoumen pay his legal bills, but none offered any assistance. The ABC lifted the bar's liquor license in 1963, the night before its annual [[Halloween]] party. After a final defiant Halloween celebration at which only non-alcoholic beverages were served and an attempt to survive on food and soft drink sales, the Black Cat closed down for good in February 1964.<ref>Gorman p. 150</ref>

The site is now the location of Bocadillos, a [[tapas]]-style [[restaurant]]. On December 15, 2007, a plaque commemorating the Black Cat and its place in San Francisco history was placed at the site.<ref name = Laird />

==Notes==
{{reflist|2}}

==References==
* Boyd, Nan Alamilla (2003). ''Wide-Open Town: A History of Queer San Francisco to 1965''. University of California Press. ISBN 0-520-20415-8
* D'Emilio, John (1983). ''Sexual Politics, Sexual Communities: The Making of a Homosexual Minority in the United States, 1940-1970''. Chicago, University of Chicago Press. ISBN 0-226-14265-5
* Eskridge, Jr., William (2002). ''Gaylaw: Challenging the Apartheid of the Closet''. Boston, Harvard University Press. ISBN 0-674-00804-9
* Gorman, Micael R. (1998). ''The Empress is a Man: Stories From the Life of José Sarria''. New York, Harrington Park Press: an imprint of Haworth Press. ISBN 0-7890-0259-0 (paperback edition)
* Lockhart, John (2002). ''The Gay Man's Guide to Growing Older''. Los Angeles, Alyson Publications. ISBN 1-55583-591-0
* [[Neil Miller (writer)|Miller, Neil]] (1995). ''Out of the Past: Gay and Lesbian History from 1869 to the Present''. New York, Vintage Books. ISBN 0-09-957691-0
* [[Randy Shilts|Shilts, Randy]] (1982). ''The Mayor of Castro Street''. New York: St. Martin's Press. ISBN 0-312-52331-9
* Witt, Lynn, Sherry Thomas & Eric Marcus (1995). ''Out in All Directions: The Almanac of Gay and Lesbian America''. New York, Warner Books. ISBN 0-446-67237-8

==External links==
* [http://www.thinkwalks.org/sfstories/theblackcat.htm Essay on the Black Cat]
* [http://sflib1.sfpl.org:82/search~/a?searchtype=X&searcharg=%22black%22+cat+cafe&SORT=D&x=0&y=0 photos of the Black Cat Cafe at SF Public Library Historic Photo Collection]

{{Early U.S. gay rights movement}}

{{good article}}

[[Category:1950s in LGBT history]]
[[Category:Buildings and structures in San Francisco]]
[[Category:LGBT history in San Francisco]]
[[Category:Restaurants in San Francisco]]
[[Category:LGBT drinking establishments in the United States]]