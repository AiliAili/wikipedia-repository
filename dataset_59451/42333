<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=XF2J-1
 | image=Berliner-Joyce XF2J-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Two-seat Carrier Based Fighter
 | national origin=United States
 | manufacturer=Berliner-Joyce
 | designer=
 | first flight=1933
 | introduced=
 | retired=
 | status=
 | primary user=US Navy
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1<ref>http://www.aerofiles.com/_berlin.html</ref>
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= XFJ-2
 | variants with their own articles=
}}
|}
The '''Berliner-Joyce XF2J''' was the company's second [[biplane]] [[Fighter aircraft|fighter]] for the [[US Navy]]. The XF2J was ordered on 30 June 1931 and although designated as a two-seat fighter, it was used as an observation aircraft.

==Design and Development==
The XF2J's construction was all-metal with a [[aircraft fabric covering|fabric covered]] rudder. The upper wing was "[[gull wing|gulled]]", with a short, sharply upward-angled section, with the remainder of the wing with a slight [[Dihedral (aircraft)|dihedral]]. The lower wing span was shorter than the upper wing, and was braced with [[Interplane strut|"N" struts]] and wires. A .30 calibre [[machine gun]] was located in each of the gulled sections of the upper wing and were [[Synchronization|synchronized]] to fire through the propeller arc.<ref name="forgotten p56">Forgotten Fighters p 56</ref>

The tightly-cowled 9-cylinder [[Pratt & Whitney R-1690C Hornet]] was the engine originally specified, but was changed to the {{convert|625|hp|kW|0|abbr=on}} 14-cylinder [[Wright SR-1510-92 Whirlwind]] before the aircraft flew. The propeller was a metal [[Constant speed propeller|constant speed]] two-blade design.<ref name="forgotten p56" />

The original open cockpits were modified to sliding canopies shortly after delivery to the navy.<ref name="forgotten p56" />
==Operational history==
The XF2J-1 suffered from the same faults as the [[Berliner-Joyce P-16|P-16]], resulting in an unfavourable service trial of the one prototype, which had appeared two years late due to a protracted development phase, exacerbated by financial difficulties that eventually led to the demise of the company <ref>Baugher, Joe.  [http://home.att.net/~jbaugher1/p16.html "Berliner-Joyce P-16/PB-1."] {{webarchive |url=https://web.archive.org/web/20080130173357/http://home.att.net/~jbaugher1/p16.html |date=January 30, 2008 }} ''USAAC/USAAF/USAF Fighter and Pursuit Aircraft'', 7 June 1998. Retrieved: 22 June 2007.</ref> The poor visibility over the nose and the landing characteristics doomed the XF2J-1, especially in light of the availability of the superior [[Grumman FF]]-1.
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (XF2J-1)==
{{Aircraft specs
|ref=''Forgotten Fighters/1''<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=2
|capacity=
|length m=
|length ft=28
|length in= 10
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=36
|upper span in=0
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=303.5
|wing area note=
|aspect ratio=<!-- give where relevant e.g. sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=3210
|empty weight note=
|gross weight kg=
|gross weight lb=4539
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=4539
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Wright SR-1510-92 Whirlwind]]
|eng1 type=14-cylinder air-cooled supercharged two-row radial piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=600
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=315
|max speed mph=196
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|guns= two .30 caliber machine guns mounted in the upper wing root area
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|other armament= 
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
*[[Berliner-Joyce P-16]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Berliner-Joyce F2J}}
;Notes
{{reflist}}

;Bibliography
{{refbegin}}
<!-- insert the reference sources here -->
* Bowers, Peter M, ''Forgotten Fighters/1 US Navy, 1918-1941'' ARCO Publishing, New York, 1971, ISBN 0-668-02404-6
* W.Green, D.Swanborough ''The Complete Book of Fighters'', 2000
{{refend}}
<!-- ==Further reading== -->

==External links==
* [https://www.flickr.com/photos/sdasmarchives/4831844269/in/set-72157624464685135/ Berliner-Joyce XF2J-1]
<!-- Navboxes go here -->

{{Berliner-Joyce Aircraft}}

<!--[[Category: ]]-->
[[Category:Berliner-Joyce aircraft|F2J]]
[[Category:Gull-wing aircraft]]
[[Category:United States fighter aircraft 1930–1939]]
[[Category:Single-engine aircraft]]
[[Category:Carrier-based aircraft]]