{{Infobox journal
| title         = Journal of the Bombay Natural History Society
| cover         = [[Image:JBNHS v1 n1A.jpg|200px]]
| editor        = 
| discipline    = [[Natural history]]
| peer-reviewed = 
| language      = [[English language|English]]
| abbreviation  = J. Bomb. Nat. Hist. Soc., JBNHS
| publisher     = [[Bombay Natural History Society]]
| country       = [[India]]
| frequency     = 
| history       = 1886–present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.bnhs.org/publications/journal-of-bnhs.html
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 1536710
| LCCN          =                      48032581
| CODEN         = 
| ISSN          = 0006-6982
| eISSN         = 
| boxwidth      = 
}}

The '''''Journal of the Bombay Natural History Society''''' (also '''''JBNHS''''') is a [[natural history]] journal published several times a year by the [[Bombay Natural History Society]].  First published in January 1886, and published with only a few interruptions since, the JBNHS is one of the best-known journals in the fields of [[natural history]], [[conservation biology|conservation]], and [[biodiversity]] research.

==Major editors: 1886-1985==
Format: Decade.  ''Major'' Editor(s). (For more details, see {{Harvnb|Pittie|2003}}.)
*1886-1895. [[R. A. Sterndale]], [[E. H. Aitken]], & [[H. M. Phipson]]
*1896-1905. [[H. M. Phipson]] & [[W. S. Millard]]
*1906-1915. [[W. S. Millard]], R. A. Spence & [[N. B. Kinnear]]
*1916-1925. [[W. S. Millard]], R. A. Spence, [[N. B. Kinnear]], & [[S. H. Prater]]
*1926-1935  R. A. Spence, [[S. H. Prater]], P. M. D. Sanderson, & [[Salim Ali (ornithologist)|Sálim Ali]].
*1936-1945: M. J. Dickins, P. M. D. Sanderson, [[S. H. Prater]], [[Charles McCann|C. McCann]], H. M. McGusty & J. F. Caius.
*1946-1955: [[S. H. Prater]], C. McCann, [[Salim Ali (ornithologist)|Sálim Ali]], S. B. Setna, & H. Santapau.
*1956-1965: [[Salim Ali (ornithologist)|Sálim Ali]], H. Santapau, [[Humayun Abdulali|H. Abdulali]], & Z. Futehally.
*1966-1975: H. Santapau, D. E. Reuben, Z. Futehally, [[J. C. Daniel (naturalist)|J. C. Daniel]], & P. V. Bole.
*1976-1985. [[J. C. Daniel (naturalist)|J. C. Daniel]], P. V. Bole & A. N. D. Nanavati.

==Illustrations==
<Center>
<Gallery>
Image:First illustrationJBNHSa.jpg|The first illustration, vol. 1, no. 1, 1886, of the horns of the sheep, ''Ovis hodgsoni'', ''O. vignei'', and of an hybrid found in the Zanskar region of the upper Indus river valley, drawn by [[R. A. Sterndale]]
Image:First part colorJBNHSa.jpg|First partial color illustration, vol. 1, no. 1, 1886, of the [[Finless Porpoise]], subspecies ''Neomeris kurrachiensis''
Image:First photographJBNHSa.jpg|First photograph, vol. 3, no. 2, 1888, of a Black rock [[scorpion]] (''Buthus afer'') displaying simultaneous twin [[parturition]]
Image:First lithographJBNHSa.jpg|First lithograph, vol. 3, no. 2, 1888, showing remains of a partially eaten tiger kill ([[Nilgai]])
</Gallery>
</Center>

==Notes==
{{Reflist}}

==References==
*{{Citation|last=Ali|first=Salim|title=Bombay Natural History Society: The Founders, the Builders and the Guardians|journal=Journal of the Bombay Natural History Society|year=1978|volume=75|issue=3|pages=559–569}}
*{{Citation|last=Anon.|first=|title=Simultaneous twin parturition of ''Buthus afer'', the black rock scorpion (with illustration)|journal=Journal of the Bombay Natural History Society|year=1888|volume=3|issue=2|pages=137–138}}
*{{Citation|last=Inverarity|first=J. D.|title=Unscientific notes on the tiger|journal=Journal of the Bombay Natural History Society|year=1888|volume=3|issue=3|pages=143–154}}
*{{Citation|last=Pittie|first=Aasheesh|title=On the dates of publication of the Journal of the Bombay Natural History Society, volumes 1-100 (1886-2003), and other matters|journal=Journal of the Bombay Natural History Society|year=2003|volume=100|issue=2-3|pages=589–613}}
*{{Citation|last=Sterndale|first=R. A.|authorlink= R. A. Sterndale|title=On a hybrid ''Ovid Hodgsoni, cum Vignei'' discovered and shot by Mons. H. Daubergne|journal=Journal of the Bombay Natural History Society|year=1886|volume=1|issue=1|pages=35–37}}

==External links==
* [http://www.biodiversitylibrary.org/bibliography/7414 Scanned Journal volumes 1–106] at the [[Biodiversity Heritage Library]]

[[Category:Biology journals]]
[[Category:Indian science and technology magazines]]
[[Category:Media in Mumbai]]
[[Category:Natural history of India]]
[[Category:Publications established in 1886]]
[[Category:1886 establishments in British India]]
[[Category:1886 establishments in India]]