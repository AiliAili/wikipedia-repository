{{Good article}}
{{Use British English|date=April 2014}}
{{Use dmy dates|date=April 2014}}
{{Infobox University Boat Race
| name= 1st Boat Race
| image = Henley Bridge.JPG
| caption = [[Henley Bridge]], the finish of the 1st Boat Race
| winner = Oxford
| margin = "easily"
| winning_time= 14 minutes 30 seconds
| overall = 0–1
| umpire = Cyril Page (Oxford)<br>John Stuart Roupell (Cambridge)
| date= {{Start date|1829|06|10|df=y}}
| nextseason= [[The Boat Race 1836|1836]]
}}

The '''1st Boat Race''' took place at [[Henley-on-Thames]] on 10 June 1829.  The race came about following a challenge laid down to the [[University of Oxford]] by [[University of Cambridge]] "to row a match at or near London, each in an eight-oared boat during the ensuing Easter vacation".<ref name=origins/>  Oxford wore dark blue jerseys while Cambridge wore "white with pink waistbands".<ref name=mac/>  In front of a crowd estimated to be around 20,000,  and according to the official record, Oxford won the race "easily" in a time of 14 minutes 30 seconds. [[The Boat Race]] became an annual fixture, and as of 2015, has been contested 161 times.

==Background==
[[Eight (rowing)|Coxed eight]] rowing had been popular at the [[University of Oxford]] for a number of years before a club was established at the [[University of Cambridge]] around 1827. At a meeting of the [[Cambridge University Boat Club]] in February 1829, it was decided to challenge Oxford  "to row a match at or near London, each in an eight-oared boat during the ensuing Easter vacation".<ref name=origins/> The race was deferred to the summer, as rowing did not start at Oxford until after Easter,<ref>MacMichael, pp. 33–34.</ref> and scheduled for 10 June 1829 for a prize of 500 [[Guinea (British coin)|guineas]].<ref name= origins >{{Cite web | url = http://theboatraces.org/origins | title = Boat Race &ndash; Origins | publisher = The Boat Race Company Ltd| accessdate = 20 April 2014}}</ref><ref>{{cite news |url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000203/18290530/012/0004 |title=Sporting Intelligence |work=Leamington Spa Chronicle |page=4 |date=30 May 1829 |accessdate=6 April 2015 |via=[[British Newspaper Archive]] |subscription=yes}}</ref> During the pre-race betting, Cambridge were the favourites to win the race.<ref>{{cite news |url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000358/18290613/009/0003 |title=Grand Rowing Match |work=[[Reading Chronicle|Berkshire Chronicle]] |page=3 |date=13 June 1829 |accessdate=6 April 2015 |via=[[British Newspaper Archive]] |subscription=yes}}</ref>

Oxford wore a dark blue [[Check (pattern)|check]] outfit for the race, while Cambridge wore white with pink waistbands. The two boats were said to be "very handsome, and wrought in a superior style of workmanship" by ''[[The Morning Post]]''; Oxford's green boat was built by Stephen Davies and Isaac King of Oxford, and was slightly the shorter, measuring {{convert|44|ft|m}}. Cambridge's pink boat was {{convert|18|inch|m}} longer, and built by Searle of [[Westminster]].<ref name="mpost"/><ref>{{Cite web | url = http://www.nationalhistoricships.org.uk/register/1846/oxford-1829-boat | title = Oxford 1829 Boat | publisher = National Historic Ships UK | accessdate = 11 April 2015}}</ref>  The umpires for the race were Mr. Cyril Page (for Oxford) and Mr John Stuart Roupell (for Cambridge).  Should the umpires disagree about any aspect of the race, they had recourse to consult the referee, whose name was not recorded.<ref>Burnell, p. 49.</ref><ref>Drinkwater, pp. 11&ndash;12.</ref>

==Crews==
The Cambridge crew weighed an average of 11&nbsp;[[Stone (unit)|st]] 1.75&nbsp;[[Pound (mass)|lb]] (70.5&nbsp;kg);  as the records for the Oxford crew are incomplete, no average weight can be calculated.<ref>Drinkwater, p.13.</ref>
{{Multiple image
<!--image 1-->
 | image1            =Charles Wordsworth.jpg
<!--image 2-->
 | image2            =Charles Merivale.jpg
<!-- Footer -->
 | footer            = Oxford's [[Charles Wordsworth]] ''(left)'' and Cambridge's [[Charles Merivale]] ''(right)'' both rowed at number four for their respective university.
}}
{| class=wikitable
|-
! rowspan= "2"| Seat
! colspan= "3"| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan= "3"| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
| Name
| College
| Weight
| Name
| College
| Weight
|-
| [[Bow (rowing)|Bow]] || A. B. E. Holdsworth || [[Trinity College, Cambridge|1st Trinity]] || 10 st 7 lb ||  J. Carter || [[St John's College, Oxford|St John's]] || 
|-
| 2 || A. F. Bayford || [[Trinity Hall, Cambridge|Trinity Hall]] || 10 st 8 || E. J. Arbuthnot || [[Balliol College, Oxford|Balliol]] || 
|- 
| 3 || C. Warren || [[Trinity College, Cambridge|2nd Trinity]] || 10 st 10 || J. E. Bates || [[Christ Church, Oxford|Christ Church]] ||
|-
| 4 || [[Charles Merivale|C. Merivale]] || [[Lady Margaret Boat Club]] || 11 st 0 lb || [[Charles Wordsworth|C. Wordsworth]] || [[Christ Church, Oxford|Christ Church]] || 11 st 10 lb
|-
| 5 || Thos. Entwistle || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 4 lb || J. J. Toogood || [[Balliol College, Oxford|Balliol]] || 14 st 10 lb
|-
| 6 || W. T. Thompson || [[Jesus College, Cambridge|Jesus]] || 11 st 10 lb || [[Thomas Garnier (Dean of Lincoln)|T. F. Garnier]] || [[Worcester College, Oxford|Worcester]] ||
|-
| 7 || [[George Selwyn (bishop of Lichfield)|G. A. Selwyn]] || [[St John's College, Cambridge|St John's]] || 11 st 13 lb || G. B. Moore || [[Christ Church, Oxford|Christ Church]] || 12 st 4 lb
|-
| [[Stroke (rowing)|Stroke]] || W. Snow (P) || [[Lady Margaret Boat Club]] || 11 st 4 lb || T. Staniforth (C) || [[Christ Church, Oxford|Christ Church]] || 12 st 0 lb
|-
| [[Coxswain (rowing)|Cox]] || B. R. Heath || [[Trinity College, Cambridge|Trinity]] || 9 st 4.75 lb || W. R. Fremantle || [[Christ Church, Oxford|Christ Church]] || 8 st 2 lb
|-
!colspan="7"|Source:<ref name=mac>MacMichael, p. 37.</ref><ref>Dodd, p. 286.</ref><br>(P) &ndash; boat club president<ref>Burnell, p. 51.</ref><br>(C) &ndash; crew captain<ref>Burnell, p. 50.</ref>
|}

==Race==
[[File:River Thames Hambleden Lock to Henley EN.svg|right|thumb|The course of the inaugural Boat Race started at Hambleden Lock and ended at Henley Bridge.]]
The course for the race was a {{convert|2.25|mile|km|adj=on}} stretch of the [[River Thames]] between [[Hambleden Lock]] and [[Henley Bridge]].<ref name=mac34>MacMichael, p. 34.</ref> Cambridge won the [[coin flipping|toss]] and elected to start on the [[Berkshire]] side of the river, handing the [[Buckinghamshire]] side to Oxford.<ref name=p36>MacMichael, p. 36.</ref> According to the author William Fisher MacMichael, "it was as fine a day as our climate allows a June day to be."<ref name=mac34/> The race had originally been scheduled to start at 6:00 p.m. but this was altered to 7:00 p.m. and was further delayed. Upon the start of the race, by which time around 20,000 people were reckoned to be in attendance to watch, the Oxford boat was steered close to the Cambridge boat, forcing it to row close to the shore. This drew complaints from the Cambridge crew, who insisted that the race be restarted. The Oxford crew relented, and the race began for the second time at 7:55 p.m.<ref name="mchron">{{cite news |url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000082/18290613/019/0004 |title=Grand Rowing Match between the Oxonians and Cantabs |work=[[The Morning Chronicle]] |page=4 |date=13 June 1829 |accessdate=6 April 2015 |via=[[British Newspaper Archive]] |subscription=true}}</ref> In the early stages of the race, the two crews were evenly matched, but after they passed an island in the river, Oxford drew ahead.<ref name="mpost">{{cite news |url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000174/18290615/014/0003 |title=The Grand Rowing Match at Henley |work=[[The Morning Post]] |page=3 |date=15 June 1829 |accessdate=6 April 2015 |via=[[British Newspaper Archive]] |subscription=true}}</ref>

Once they had passed the island, and were in the main flow of the river, the Oxford crew demonstrated their strength. ''The Morning Post'' report of the race records that both crews "put out the strength of their arms in excellent style", and although the Cambridge boat maintained a higher [[stroke rate]], Oxford maintained their lead throughout the rest of the race.<ref name="mpost"/> Their margin of victory is variously reported as being between two and four lengths (although is recorded officially as "easily"),<ref name=results/> in a time reported variously between 14 minutes and 14 minutes 30 seconds.<ref name="mpost"/><ref name="mchron"/><ref name=results/>

==Legacy==
The event, subsequently referred to as [[The Boat Race]], or the University Boat Race, was held for a [[The Boat Race 1836|second time]] seven years later, in 1836 on the [[River Thames]].  It was then held intermittently until the [[The Boat Race 1856|1856 race]] after which it became an annual event, interrupted only by the [[First World War|First]] and [[Second World War]]s.  As of 2016, 162 Boat Races have been contested;  Cambridge lead overall with 82 victories to Oxford's 79, excluding the one "dead heat" recorded in the [[The Boat Race 1877|1877 race]].<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 8 April 2015}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-9500638-7-4 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 978-0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}
*{{Cite book | url = https://books.google.com/books?id=uJ8CAAAAYAAJ&pg=PA33 | title = The Oxford and Cambridge Boat Races: From A.D. 1829 to 1869| first = William Fisher | last = MacMichael | publisher = Deighton | date = 1870}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1829}}
[[Category:1829 in English sport]]
[[Category:The Boat Race]]
[[Category:June 1829 events]]