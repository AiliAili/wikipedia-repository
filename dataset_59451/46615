{{multiple issues|
{{Peacock|date=June 2013}}
{{COI|date=June 2013}}
{{POV|date=June 2013}}
{{Underlinked|date=July 2013}}
{{Orphan|date=July 2013}}
}}

[[File:Annie forward.crop1.jpg|thumb|alt=caption|Annie B. Bond at her home in Rhinebeck, NY.]]

'''Annie B. Bond''' (formerly Annie Berthold-Bond) (born 1953) is a green-living advocate, author, editor, entrepreneur, and consultant. She lives in Rhinebeck, [[New York (state)|New York]]. She is currently the editor-in-chief of ''The Wellness Wire''.<ref name="The Wellness Wire">{{cite web|title=Who We Are|url=http://www.thewellnesswire.com/about|work=The Wellness Wire|accessdate=3 June 2013}}</ref> She is also the founder of  Terraspheres.com and ATrueFind.com.<ref name="The Natural Singer">{{cite web|last=Brownfield|first=Susan|title=Interview With Green Living Expert, Annie B. Bond|url=http://www.thenaturalsinger.com/natural-remedies/interview-green-living-expert-annie-b-bond/|work=The Natural Singer|accessdate=3 June 2013}}</ref><ref name="A True Find">{{cite web|title=A True Find|url=http://www.anniebbond.com/my-sites/a-true-find/|work=Annie B. Bond|accessdate=3 June 2013}}</ref> Bond has written five books on green living and was named "the foremost expert on green living" by ''Body & Soul'' magazine in 2009.<ref name="The Wellness Wire" /><ref name="The Natural Singer" /><ref name=Omega>{{cite web|title=Teachers: Annie B. Bond|url=http://www.eomega.org/workshops/teachers/annie-b-bond|work=eOmega|accessdate=3 June 2013}}</ref><ref name="Maid Brigaide">{{cite web|title=Maid Brigade Launches Certified Residential Green Cleaning Service|url=http://www.franchisewire.com/article.php?id=1664|work=Franchise Wire|accessdate=3 June 2013}}</ref>

==Background==
In 1980, Bond was poisoned by a gas leak at the restaurant she worked at that sent 80 people to the hospital, where she was diagnosed with permanent central nervous system damage.<ref name="The Natural Singer" /> Her apartment building was soon after exterminated with a pesticide that was later taken off the market because it was neurotoxic.<ref name="The Natural Singer" /> She was diagnosed with atypical manic depression.<ref name="The Natural Singer" /> Bond was put in touch with Dr. Leo Galland, one of the first environmental medicine MDs, who diagnosed her with organophosphate pesticide poisoning and [[multiple chemical sensitivity]]<ref name="The Natural Singer" /><ref name="Tree Hugger">{{cite web|last=Chua|first=Jasmin Malik|title=The TH Interview: Annie B. Bond, Author and Healthy Living Expert|url=http://www.treehugger.com/culture/the-th-interview-annie-b-bond-author-and-healthy-living-expert.html|work=Tree Hugger|accessdate=3 June 2013}}</ref><ref name="Home Enlightenment">{{cite book|last=Bond|first=Annie B.|title=Home Enlightenment|year=2005|publisher=Holtzbrinck Publishers|location=United States of America|isbn=1-57954-811-3|pages=6–7}}</ref> (a controversial condition not widely recognised). Since then, Bond has made green living her career because of her perceived necessity of living without chemicals.<ref name="Tree Hugger" /><ref name="Home Enlightenment" /><ref name=Post-Gazette>{{cite news|last=Pitz|first=Marylynne|title=Clean and Green: Local cleaning companies rely on earth-friendly products |url=http://www.post-gazette.com/stories/sectionfront/life/clean-and-green-local-cleaning-companies-rely-on-earth-friendly-products-481388/#ixzz2S9ThKKtA|work=Pittsburgh Post-Gazette|publisher=Pittsburgh Post-Gazette|accessdate=3 June 2013|date=17 April 2007}}</ref>

In 1988, Bond gave birth to her only child, Lily Berthold-Bond. Lily also writes about green living.<ref name="Green Girl">{{cite web|last=Berthold-Bond|first=Lily|title=The Adventures of Green Girl|url=http://www.care2.com/greenliving/the-adventures-of-green-girl.html|work=Care2.com|accessdate=3 June 2013}}</ref><ref name=MNN>{{cite web|title=Lily Berthold-Bond|url=http://www.mnn.com/users/lberthold-bond|work=MNN|accessdate=3 June 2013}}</ref>

== Books ==
Bond is the author of five books on green living. Her first book, ''Clean and Green: The Complete Guide to Non-Toxic and Environmentally Safe Housekeeping,'' was published in 1990. It was a best seller.<ref name="The Natural Singer" />

Her second book, ''The Green Kitchen Handbook,'' which Meryl Streep wrote the foreword to, was published in 1997.<ref name="Green Kitchen Handbook">{{cite book|last=Berthold-Bond|first=Annie|title=The Green Kitchen Handbook|year=1997|publisher=HarperCollins|location=New York, NY|isbn=0-06-095186-9|url=}}</ref> ''Better Basics for the Home: Simple Solutions for Less Toxic Living,'' her third book, was published in 1999 and ''Home Enlightenment,'' her fourth book, was published in 2005.<ref name="Home Enlightenment" /><ref name="Better Basics">{{cite book|last=Berthold-Bond|first=Annie|title=Better Basics for the Home|year=1999|publisher=Three Rivers Press|location=New York, NY|isbn=0-609-80325-5|url=}}</ref>

In 2010, Annie B. Bond collaborated with Melissa Breyer and Wendy Gordon (foreword by Alice Waters) on her most recent book, ''True Food: Eight Simple Steps to a Healthier You''.<ref name="True Food">{{cite book|last=Bond|first=Annie B.|title=True Food|year=2010|publisher=National Geographic|location=Washington, DC|isbn=978-1-4262-0594-1|url=}}</ref> ''True Food'' was published by National Geographic and won the 2010 Gourmand Award as one of the Best Health and Nutrition Books in the Lifestyle, Body and World category.<ref name="Gourmand Awards">{{cite web|title=GOURMAND AWARDS 2010 COOKBOOKS|url=http://old.cookbookfair.com/pdf/winners_cook_2010.pdf|work=Cook Book Fair|accessdate=3 June 2013}}</ref>

== Career ==
Bond has been recognized as an expert in green living.<ref name="The Natural Singer" /><ref name="Omega" /><ref name="Maid Brigaide" /> In addition to her five books, Annie was the founder and editor-in-chief of “Green Alternatives for Health and the Environment” from 1989 to 1994.<ref name=Consulting>{{cite web|title=Consulting|url=http://www.anniebbond.com/business/consulting/|work=Annie B. Bond|accessdate=3 June 2013}}</ref> From 1994-1996, she was the editor-in-chief for “The Green Guide,” which was published by Mothers & Others for a Livable Planet and is now owned by National Geographic.<ref name="Green Festival">{{cite web|title=Green Festival Speakers|url=http://www.greenfestivals.org/speakers/bond-269/view-details|work=Green Festivals|accessdate=3 June 2013}}</ref>

Between 2000 and 2008, Bond was the Executive Producer of Healthy and Green Living for Care2.com, where she authored over 4,000 blogs.<ref name=Care2>{{cite web|title=Articles by: Annie B. Bond|url=http://www.care2.com/greenliving/author/annie|work=Care2.com|accessdate=3 June 2013}}</ref> Her newsletters reached 1.8 million subscribers.{{citation needed|date=June 2013}}

Annie has also written blogs and articles for Gaiam, Intent, and ''Body & Soul'', among others.<ref name="Omega" /> She partnered with alternative medicine specialist [[Deepak Chopra]] on a featured blog for The Huffington Post.<ref name="Huffington Post">{{cite web|title=Annie B. Bond|url=http://www.huffingtonpost.com/annie-b-bond|work=Huffington Post|publisher=Huffington Post|accessdate=3 June 2013}}</ref> In 2007, Bond served as the spokesperson for Maid Brigaide, as well as serving as a consult for the company.<ref name="Maid Brigaide" />

Chopra said of Bond, “Annie B. Bond offers a practical guide to create well-being for our home, our environment, and our planet. Follow her advice and both you and our planet will be healthier.”<ref name="Omega" />

Bond has now launched The True Find (www.thetruefind.com) for her Ask Annie B. column and other writings, as well as Bondify Media, a content marketing agency (www.bondifymedia.com). 

== References ==

{{reflist|2}}

== External links ==
* [http://www.thetruefind.com] - Ask Annie B. and The True Find columns
* [http://www.anniebbond.com] Annie B. Bond -[ Personal Website]
* [http://www.greenchicafe.com] - blog site
* [http://www.care2.com/greenliving/author/annie Annie B. Bond] - Profile at Care2.com


{{DEFAULTSORT:Bond, Annie}}
[[Category:1953 births]]
[[Category:Living people]]
[[Category:American non-fiction writers]]
[[Category:The Huffington Post writers and columnists]]
[[Category:American women writers]]
[[Category:Women columnists]]
[[Category:21st-century women writers]]