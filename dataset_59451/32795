{{about||the route in [[London, Ontario]]|Veterans Memorial Parkway|the Niagara Veterans Memorial Highway in [[Niagara Falls, Ontario|Niagara Falls]]|Ontario Highway 420}}
{{featured article}}
{{Infobox road
|province       = ON
|type           = Fwy
|route          = 416
|alternate_name = Veterans Memorial Highway
|map            = Ontario 416 map.svg
|map_notes      = Highway 416 highlighted in red
|history        = Proposed 1966<ref name="history" /><br />Alignment built 1969–1983<br />Freeway built 1990–1999<br />Completed September&nbsp;23, 1999<ref name="opened" />
|length_km      = 76.4
|length_ref     = <ref name="km">{{cite web

 | title      = Annual Average Daily Traffic (AADT) counts
 | author     = Ministry of Transportation of Ontario
 | publisher  = Government of Ontario
 | year       = 2008
 | url        = http://www.raqsb.mto.gov.on.ca/techpubs/TrafficVolumes.nsf/tvweb?OpenForm&Seq=5
 | accessdate = December 20, 2011}}</ref>
|direction_a    = South
|terminus_a     = {{jcon|Hwy|401}} to [[Toronto]]
|junction       = {{jcon|Hwy|16|dir=south}} to [[Johnstown, Ontario|Johnstown]]
|direction_b    = North
|terminus_b     = {{jcon|Hwy|417}} to [[Ottawa]]
|previous_type  = ON
|previous_route = 412
|next_type      = ON
|next_route     = 417
}}
'''King's Highway 416''', commonly referred to as '''Highway 416''' and as the '''Veterans Memorial Highway''', is a [[400-series highway]] in the Canadian province of [[Ontario]] that connects the [[Trans-Canada Highway]] ([[Ontario Highway 417|Highway&nbsp;417]]) in [[Ottawa]] with [[Ontario Highway 401|Highway&nbsp;401]] between [[Brockville]] and [[Cornwall, Ontario|Cornwall]]. The {{convert|76.4|km|mi|adj=mid|-long}} [[freeway]] acts as an important trade corridor from [[Interstate 81]] between [[New York (state)|New York]] and [[Eastern Ontario]] via Highway&nbsp;401, as well as the fastest link between Ottawa and [[Toronto]]. Highway&nbsp;416 passes through a largely rural area, except near its northern terminus where it enters the suburbs of Ottawa. The freeway also serves several communities along its length, notably [[Spencerville, Ontario|Spencerville]] and [[Kemptville]].

Highway&nbsp;416 had two distinct construction phases. Highway&nbsp;416 "North" was the {{convert|21|km|adj=on}} segment starting from an interchange at Highway&nbsp;417 and bypassing the original route of [[Ontario Highway 16|Highway&nbsp;16]] into Ottawa (now [[Prince of Wales Drive]]) along a new [[right-of-way (transportation)|right-of-way]]. Highway&nbsp;416 "South" was the [[twinning (roads)|twinning]] of {{convert|57|km|}} of Highway&nbsp;16&nbsp;New—a two-lane expressway [[bypass (road)|bypassing]] the original highway that was constructed throughout the 1970s and finished in 1983—and the construction of a new interchange with Highway&nbsp;401. Sections of both opened throughout the late 1990s. Highway&nbsp;416 was commemorated as the Veterans Memorial Highway on the 54th anniversary of [[D-Day]] in 1998. The final link was officially opened by a World War I veteran and local officials on September&nbsp;23, 1999.

== Route description ==
[[File:416 into Ottawa.png|thumb|left|Highway 416 entering the western end of Ottawa. The Fallowfield Road interchange is shown.]]
Highway&nbsp;416 begins at an interchange with Highway&nbsp;401, branching to the north near the community of [[Johnstown, Ontario|Johnstown]] in the [[United Counties of Leeds and Grenville]]. This [[Interchange (road)#Two-way interchanges|interchange]] only provides access to and from the west of Highway&nbsp;401, but immediately north of it, a second interchange with the remaining section of Highway&nbsp;16 provides access from Johnstown and to a [[parclo interchange]] with both directions of Highway&nbsp;401, as well as to an international crossing into the United States.<ref name="2010 mapart">{{cite map

 | title       = Ontario Back Road Atlas
 | year        = 2010
 | cartography = [[MapArt]]
 | publisher   = Peter Heiler Ltd
 | isbn        = 978-1-55198-226-7
 | pages       = 50, 66
 | sections    = R62–Y64}}</ref>
Proceeding north, the two [[carriageways]] of the freeway are separated by a {{convert|68|m|ft|adj=mid|-wide}} forested median.<ref name="416south">{{cite web

 | title      = Highway 416 South
 | author     = Ministry of Transportation of Ontario
 | publisher  = Government of Ontario
 | date       = June 17, 2010
 | url        = http://www.mto.gov.on.ca/english/traveller/416/south.shtml
 | accessdate = October 26, 2011}}</ref>
The route is surrounded by thick forests for the next {{convert|10|km|mi}}. As it passes beneath Leeds and Grenville County Road&nbsp;44, the original routing of Highway&nbsp;16 (the Prescott Highway) south of Spencerville, it exits the forest and enters farm fields. The route travels to the east of the community, access to which is provided by an interchange at County Road&nbsp;21, and crosses a swamp and the [[South Nation River]].<ref name="Gmaps">{{Google maps

 | title      = Highway 416 length and route
 | url        = http://goo.gl/maps/DGsxk
 | accessdate = October 26, 2011}}</ref>

Highway&nbsp;416 crosses under the Prescott Highway a second time;<ref name="Gmaps" /> to the north, the two remain roughly parallel but separated as they pass through a mix of farmland and forest. South of the community of [[Kemptville]], the Prescott Highway crosses the route a third time, with an interchange connecting the two highways.<ref name="2010 mapart" />
The freeway curves to the northeast, bypassing Kemptville and featuring an interchange with County Road&nbsp;43 (formerly [[Ontario Highway 43|Highway&nbsp;43]]).<ref name="Gmaps" /><ref>{{cite map

 | title       = Ontario Road Map
 | cartography = Geomatics Office
 | publisher   = Ministry of Transportation
 | year        = 1997
 | section     = N33}}</ref>
It crosses the line of the old [[Bytown and Prescott Railway]],<ref>{{cite map

 | title       = Bytown & Prescott Route & Profile, 1871
 | cartography = Al Craig
 | publisher   = Bytown Railway Society
 | date        = 1979
 | url         = http://www.railways.incanada.net/Articles/art2005_05.jpg
 | accessdate  = December 20, 2011}}</ref>
then curves to the northwest, providing an interchange with River Road. At the southeast corner of the River Road interchange is the Veterans Commemorative Park, dedicated in 2000 by the [[Royal Canadian Legion]].<ref>{{cite web

 | title      = Veterans Commemorative Park
 | author     = Ontario Seniors' Secretariat
 | publisher  = Government of Ontario
 | date       = May 20, 2011
 | url        = http://www.seniors.gov.on.ca/en/veterans/commemorative.php
 | accessdate = December 20, 2011}}</ref><ref group="nb">The tiny Veterans Commemorative Park (45.08174,-75.63193) at the highway's approximate midpoint may be reached on foot directly from a commuter parking lot on the southeast corner of River Road's Highway 416 interchange.</ref>

It crosses the [[Rideau River]] and enters the City of Ottawa.<ref name="2010 mapart" /> Aside from the first couple of kilometres north of the Rideau River, the majority of the freeway cuts through swaths of farmland which fill the [[Ottawa Valley]]. The median also becomes narrower.<ref name="Gmaps" /> The freeway encounters an interchange with Dilworth Road and thereafter with [[Roger Stevens Drive]], the latter providing access to [[North Gower]].<ref name="2010 mapart" />

Continuing north of [[Manotick]] through fields, Highway&nbsp;416 is crossed by the Prescott Highway for the fourth and final time as that road turns northeast and travels into downtown Ottawa as Prince of Wales Drive. Shortly thereafter is an interchange with Brophy Drive / [[Bankfield Road]]; the latter provides access to the Prescott Highway / Prince of Wales Drive. Approaching urban Ottawa, the route passes alongside a large [[quarry]], then jogs to the west along an S-curve, crossing the [[Jock River]] in the process.<ref name="2010 mapart" /> After this, an interchange with [[Fallowfield Road]] provides access to the suburb of [[Barrhaven, Ontario|Barrhaven]] which occupies portions of the land immediately east of the freeway.<ref name="Gmaps" /> The route jogs back to the east along a second S-curve and passes through an aesthetically designed bridge while traveling alongside the [[Stony Swamp]].<ref name="2010 mapart" />

The final section of Highway&nbsp;416 travels parallel to [[Cedarview Road]], which was relocated for the freeway.<ref name="ea report" /> The Stony Swamp lies west of the route while farmland lies to the east.<ref name="Gmaps" /><ref name="416north" /> At the northern end of the swamp is an interchange with [[Hunt Club Road|West Hunt Club Road]]. The freeway continues through a section of greenspace before descending gently into a trench. It passes beneath Bruin Road and the [[Ottawa Central Railway]] while traveling alongside Lynwood Village in [[Bells Corners]]. The highway is crossed by [[Baseline Road (Ottawa)|Baseline Road]] and [[Richmond Road (Ottawa)|Richmond Road]]; the former provides an onramp to southbound Highway&nbsp;416. The freeway ends at a large interchange with the Trans-Canada Highway, Highway&nbsp;417 ([[Exit number|Exit]]&nbsp;131), just south of the [[Lakeview (Ottawa)|Lakeview]] and [[Bayshore, Ottawa|Bayshore]] communities on the [[Ottawa River]]; downtown Ottawa is to the east and [[Kanata]] is to the west.<ref name="2010 mapart" />

=== Design features ===
The Stony Swamp overpass at the southern entrance to Ottawa is a [[Prestressed concrete|pre-tensioned concrete]] arch; the design, which allows the structure to cross the entire right of way with a single span, won the 1996 Award of Excellence from the [[Portland Cement Association]]. The bridge acts as a gateway to the [[National Capital Region]] and is the longest rigid frame bridge in Ontario with a {{convert|59|m|ft|adj=mid|-long}} span.<ref name="416north">{{cite web

 | title      = Highway 416 North
 | author     = Ministry of Transportation of Ontario
 | publisher  = Government of Ontario
 | date       = June 17, 2010
 | url        = http://www.mto.gov.on.ca/english/traveller/416/north.shtml
 | accessdate = October 26, 2011}}</ref>
In the same vicinity, the freeway sinks below ground level in a trench; groundwater-retaining walls were installed to prevent the lowering of the water table in adjacent wetlands, therefore mitigating damage to them.<ref name="leda">{{cite report

 | title      = Freeway Construction Techniques in Areas of Sensitive Clay Soils: Highway 416 Case Study
 | first1     = Darwyn G.
 | last1      = Sproule
 | first2     = Brent
 | last2      = Loken
 | publisher  = Ministry of Transportation of Ontario
 | year       = 1998
 | url        = http://www.mto.gov.on.ca/english/traveller/416/dspaper.shtml
 | accessdate = September 5, 2012}}</ref>

At the Jock River, southwest of Barrhaven, deposits of sensitive [[leda clay]] presented a challenge in designing the crossing for the freeway as well as the [[Canadian National Railway]] overpass to the north. It was feared that the weight of these structures could destabilize the clay deposits beneath and lead to landslides. In place of the standard heavier [[Construction aggregate|aggregate]], lighter [[blast furnace]] [[slag]], at half the weight, was substituted.<ref name="leda" />

Sloped rock cuts line the side of the freeway in numerous locations. With the intent of reducing the severity of collisions against those cuts, the [[Ministry of Transportation of Ontario]] (MTO) tested out numerous alternatives to strike a [[cost–benefit analysis|cost-to-benefit]] balance. The standard slope used by the MTO is vertical, offset from the edge of the  pavement by {{convert|10|m|0}}. The study concluded that although an initially higher investment would be required, the [[grade (slope)|2:1 sloped]] cut with grass overlaid produced the best results.<ref>{{cite report

 | title      = Roadside Safety Improvements in Rock Cuts less than 4 m Deep Using Benefits-Cost Procedures
 | first1     = Todd A
 | last1      = Comfort
 | first2     = Kelly
 | last2      = Oakley
 | publisher  = Ministry of Transportation of Ontario
 | year       = 1999
 | url        = http://www.mto.gov.on.ca/english/traveller/416/tacpaper.shtml
 | accessdate = September 5, 2012}}</ref>

== History ==
[[File:Highway 416- The Veterans Memorial Highway.jpg|thumb|right|Highway 416 is commemorated as the Veterans Memorial Highway. Signs placed along the length of the highway in English and French (in background) indicate this along with the phrase ''Lest we forget''.]]

=== Highway 16 New ===
In 1966, the Department of Highways (DHO), predecessor to today's MTO, published the Eastern Ontario Highway Planning Study, identifying the need for a [[controlled-access highway]] between Ottawa and Highway&nbsp;401.<ref name="history">{{cite web
 | title      = History of Highway 416
 | author     = Ministry of Transportation of Ontario
 | publisher  = Government of Ontario
 | date       = June 17, 2010
 | url        = http://www.mto.gov.on.ca/english/traveller/416/history.shtml
 | accessdate = September 30, 2011}}</ref>
Highway&nbsp;16, which passes over the geologically subdued St.&nbsp;Lawrence Lowlands, was selected over Highway&nbsp;15, which crosses the undulating [[Canadian Shield]] to the west, as the ideal route for the new link.<ref name="ea report">{{cite report
 | title      = EA-86-01
 | publisher  = Environmental Assessment Board
 | date       = July 31, 1987
 | page       = 9
 | url        = http://www.ert.gov.on.ca/files/DEC/ea8601d1.pdf
 | accessdate = September 30, 2011
 | format     = PDF}}</ref>
Highway&nbsp;16 was one of the first roads taken over by the expanding Department of Public Highways in 1918. The important corridor between the Trans-provincial Highway (Highway&nbsp;2) and Ottawa was known as the Prescott Highway. In 1925, the road was given a numerical designation to supplement the name.<ref>{{cite book
 | title     = From Footpaths to Freeways
 | last1     = Shragge
 | first1    = John
 | last2     = Bagnato
 | first2    = Sharon
 | publisher = Ontario Ministry of Transportation and Communications, Historical Committee
 | year      = 1984
 | isbn      = 0-7743-9388-2
 | pages     = 73–74}}</ref><ref>{{Cite journal
 | title     = Provincial Highways Now Being Numbered
 | publisher = Monetary Times Print
 | work      = The Canadian Engineer
 | date      = August 25, 1925
 | volume    = 49
 | issue     = 8
 | page      = 246
 | quote     = Numbering of the various provincial highways in Ontario has been commenced by the Department of Public Highways. Resident engineers are now receiving metal numbers to be placed on poles along the provincial highways. These numbers will also be placed on poles throughout cities, towns and villages, and motorists should then have no trouble in finding their way in and out of urban municipalities. Road designations from "2" to "17" have already been allotted...}}</ref>
This highway served the low traffic volumes of the day, but as the number of vehicles increased over the first half of the 20th&nbsp;century, issues arose with the numerous private driveways along the route. To overcome this issue of abutting properties long-established on the old Highway&nbsp;16 corridor, the DHO began purchasing a new right-of-way between Highway&nbsp;401 and Century Road by late 1967 for a two-lane bypass of the original alignment, avoiding all the built-up areas that the original Highway&nbsp;16 encountered. This route was designed to easily accommodate the eventual upgrade to a freeway when traffic volumes necessitated.<ref name="link">{{cite news
 | title    = New 45-Mile Highway to Link Ottawa with 401
 | work     = The Globe and Mail
 | location = Toronto
 | date     = November 14, 1967
 | page     = 4
 | volume   = 124
 | issue    = 36,795}}</ref>

Construction of the [[super two]], dubbed ''Highway 16 New'',<ref name="82projects" /> took place between 1969 and 1983.<ref name="history" /> The Spencerville Bypass opened by 1971, connecting with the old highway in the south near Crowder Road and in the north near Ventnor Road.<ref name="1971map">{{cite map
 | title       = Ontario Road Map
 | cartography = Photogrammetry Office
 | publisher   = Ontario Department of Transportation and Communications
 | year        = 1971
 | section     = G–H30}}</ref>
By the end of 1973, the new highway was completed from immediately north of Highway&nbsp;401 through Leeds and Grenville United Counties and into [[Regional Municipality of Ottawa–Carleton|Ottawa–Carleton]]. This included a bypass around Kemptville and a new structure over the Rideau River. The new highway ended at Dilworth Road (Regional Road&nbsp;13).<ref name="1973map">{{cite map
 | title       = Ontario Road Map
 | cartography = Photogrammetry Office
 | publisher   = Ministry of Transportation and Communications
 | year        = 1973
 | section     = F–H30}}</ref>

For nearly a decade, no new construction took place. Then, during the summer of 1982, the MTO awarded a contract to construct the route north from Dilworth Road towards Manotick, bypassing North Gower and extending the route as far north as Roger Stevens Drive (Regional Road&nbsp;6), including a structure over Stevens Creek. Following completion of this first contract, a second contract was awarded for the remaining distance north to Century Road (Regional Road&nbsp;8).<ref name="82projects">{{cite report
 | title     = Provincial Highways Construction Projects 1982–83
 | author    = Ministry of Transportation and Communications
 | publisher = Transportation Capital Branch, Ministry of Transportation of Ontario
 | date      = April 1982
 | page      = XXVII}}</ref>
The project was completed in 1983,<ref name="history" /> merging into the original route of Highway&nbsp;16 northeast of the present Prince of Wales Drive overpass.<ref>{{Google maps
 | title       = Site of merger between Highway 16 and old Highway 16 before the construction of Highway 416
 | url         = http://goo.gl/maps/po7qe
 | accessdate  = September 30, 2011}}</ref><ref>{{cite map
 | publisher   = Department of Natural Resources Canada
 | title       = Ottawa 1:250,000 Topographic map (31G & 31F)
 | cartography = Department of Natural Resources Canada
 | year        = 1992–1993
 | url         = http://maps.library.carleton.ca/mapindex/topo2500.phtml
 | accessdate  = October 10, 2012}}</ref>

[[File:Hwy 416 Ottawa 2.JPG|thumb|left|Highway 416 approaching its terminus at Highway&nbsp;417 in Ottawa]]
With the completion of Highway&nbsp;16 New, the MTO needed only to construct interchanges and the southbound lanes in order to create a full freeway corridor. The upgrade to Highway&nbsp;416 took place between 1989 and 1999 and was carried out through two separate projects: Highway 416 North was a {{convert|21|km|mi}} freeway on a new alignment through Ottawa and an interchange at Highway&nbsp;417, and Highway&nbsp;416 South was the twinning of {{convert|57|km|mi}} of Highway&nbsp;16 New and an interchange at Highway&nbsp;401.<ref name="416south" /><ref name="416north" />

=== Change of plans ===
The original plans for Highway&nbsp;416, conceived during the late 1960s, had it enter Ottawa along the ''Merivale Corridor'' to merge with the Queensway approximately {{convert|5|km|mi|spell=in}} east of the present interchange. However, when it came time to construct this section, public attitudes had shifted and environmental concerns had come to the forefront of everyday life; new roads were now subject to intense public scrutiny. Suburbs grew along Merivale Road, prompting the Region of Ottawa–Carleton to request the MTO decommission the right-of-way along the road in 1977, which it did. The passing of the Environmental Assessment Act in 1975, however, meant that new projects were subject to a lengthy investigation of social and environmental concerns.<ref name="ea report" /><ref name="leda report">{{cite report
 | title      = Environmental Approach to Urban Freeway Development
 | first1     = Brian E
 | last1      = Ruck
 | first2     = Jeff C
 | last2      = McCammon
 | publisher  = Ministry of Transportation of Ontario
 | year       = 1995
 | url        = http://www.mto.gov.on.ca/english/traveller/416/brpaper.pdf
 | accessdate = October 4, 2011
 | format     = PDF}}</ref>

In 1981, the MTO began an environmental assessment into a new alignment for the northern connection with the Queensway.<ref name="leda report" /> It was approved in mid-1987, with Cedarview Drive chosen as the ideal alignment for the new freeway. The MTO set out to design a four-lane route to connect the Queensway with Highway&nbsp;16 New, including a three-level free-flow interchange.<ref name="ea report" /> A contract for construction of this interchange was awarded in late 1989 and construction began in 1990. During the 1991 construction season, contracts were awarded to construct several overpasses along the new route.<ref name="firstwork">{{cite report
 | title     = Northern Transportation Construction Projects 1988–89
 | author    = Transportation Capital Branch
 | publisher = Ministry of Transportation of Ontario
 | date      = May 1991
 | page      = 16
 | issn      = 0714-1149}}</ref>
This contract was completed in 1993, after which budgetary restraints prevented the awarding of further contracts. As a result, aside from the interchange at Highway&nbsp;417 and some overpasses, construction activity on Highway&nbsp;416 came to a standstill for two years.

It was during this period that the MTO undertook an engineering review of the entire route in search of cost inefficiencies.<ref name="history" /> Highways 416 and 407 were constructed during a [[recession]] in the mid-1990s. [[Ontario Highway 407|Highway&nbsp;407]] became a tolled highway and for a time it was mentioned in legislative debates that Highway&nbsp;416 would also be tolled, but ultimately this never happened. Instead, a hiatus in construction allowed engineers to evaluate inefficiencies in bridge and cross-section designs, as well as sensitive clay soils near Ottawa. This initiative led to a cost savings of over [[C$]]7 million and several of the unique design features located along the length of the freeway.<ref>{{cite report
 | title      = Highway 416 South: Value Improvement Experiences
 | first1     = Todd A
 | last1      = Comfort
 | first2     = Brent
 | last2      = Loken
 | publisher  = Ministry of Transportation of Ontario
 | year       = 1998
 | url        = http://www.mto.gov.on.ca/english/traveller/416/tcpaper.shtml
 | accessdate = September 5, 2012}}</ref>

=== Twinning and completion ===
Work resumed on Highway&nbsp;416 North following the review. It was opened from Century Road to Hunt Club Road on July&nbsp;16, 1996, and completed on July&nbsp;31, 1997, with the opening of the interchange with Highway 417. The cost of this section was C$196 million.<ref name="416north" /> On December&nbsp;8, 1995, in North Gower, the provincial and federal governments announced a financing deal to ensure Highway&nbsp;416 South was completed by 2000.<ref>{{cite web
 | title      = Parliamentary Debates Hansard
 | publisher  = Parliament of Canada
 | date       = December 8, 1995
 | url        = http://www.parl.gc.ca/HousePublications/Publication.aspx?DocId=2332533&Mode=1&Parl=35&Ses=1&Language=E#HIGHWAY416
 | accessdate = December 20, 2011}}</ref><ref>{{cite press release
 | title       = Highway 416 Gets the Go-Ahead: Completed in the Year 2000
 | author      = Ministry of Transportation of Ontario
 | publisher   = Government of Ontario
 | date        = December 8, 1995
 | url         = http://www.mto.gov.on.ca/english/traveller/416/dec8.htm
 | accessdate  = December 20, 2011
 | archiveurl  = https://web.archive.org/web/20010520170547/http://www.mto.gov.on.ca/english/traveller/416/dec8.htm
 | archivedate = May 20, 2001}}</ref>
This section of the route was constructed through a process known as twinning in which a second [[dual carriageway|carriageway]] is built parallel to an existing road. In addition, existing intersections were rebuilt as [[grade separation|grade-separated]] interchanges.<ref>{{cite web
 | title       = What is Involved in Constructing a Four Lane Highway?
 | publisher   = Ministry of Transportation of Ontario
 | url         = http://www.mto.gov.on.ca/english/traveller/highway69/construction.shtml
 | accessdate  = October 12, 2011
 | archiveurl  = https://web.archive.org/web/20110608073939/http://www.mto.gov.on.ca/english/traveller/highway69/construction.shtml
 | archivedate = June 8, 2011}}</ref>
With the right-of-way along Highway&nbsp;16 New already purchased, construction was able to proceed without disruption to local properties or traffic.<ref name="link" />

The {{convert|57|km|mi|adj=mid|-long}} project was constructed through five contracts. The first was awarded to Tarmac Canada on June&nbsp;10, 1996, calling for twinning of {{convert|7.6|km|mi}} from Century Road south to Roger Stevens Drive. Another contract was awarded one month later to Bot Construction, on August&nbsp;19. This contract involved the section from Roger Stevens Drive south to what was then Highway&nbsp;43, a distance of {{convert|13|km|mi}}.<ref name="416south" /> On June&nbsp;12, 1997, the first section opened, connecting with the Ottawa Bypass at Century Road.<ref name="first section">{{cite press release
 | title       = Transportation Minister Officially Opens Another 7.6 Kilometres of Highway 416
 | author      = Ministry of Transportation of Ontario
 | publisher   = Government of Ontario
 | date        = June 12, 1997
 | url         = http://www.mto.gov.on.ca/english/traveller/416/south.shtml
 | accessdate  = October 26, 2011
 | archiveurl  = https://web.archive.org/web/20010520171046/http://www.mto.gov.on.ca/english/traveller/416/june12.htm
 | archivedate = May 20, 2001}}</ref>
On July&nbsp;10, the third contract was awarded to Armbro Construction to construct the {{convert|10|km|adj=on}} section from Highway&nbsp;43 south to Grenville County Road&nbsp;20 (Oxford Station Road). Another contract followed on October&nbsp;21 for the {{convert|12|km|mi}} south to Grenville County Road&nbsp;20 (Shanly Road) which was awarded to Bot Construction. The fifth and final contract was awarded to Armbro Construction on April&nbsp;8, 1998, calling for the construction of the southern {{convert|9|km|mi|spell=in}} and the two flyover ramps at Highway&nbsp;401.<ref name="416south" /> The section between Roger Stevens Drive and what had now become Leeds and Grenville County Road&nbsp;43, including a second crossing of the Rideau River, opened to traffic on June&nbsp;26, 1998.<ref>{{cite press release
 | title       = New Section of Veterans Memorial Highway Opened Today
 | author      = Ministry of Transportation of Ontario
 | publisher   = Government of Ontario
 | date        = June 26, 1998
 | url         = http://www.newswire.ca/government/ontario/english/releases/June1998/26/c6908.html
 | accessdate  = October 26, 2011
 | archiveurl  = https://web.archive.org/web/20000305080025/http://www.newswire.ca/government/ontario/english/releases/June1998/26/c6908.html
 | archivedate = March 5, 2000}}</ref>
This was followed two months later by the section between Highway&nbsp;43 and Oxford Station Road, which opened on August&nbsp;24.<ref>{{cite press release
 | title       = New Section of Veterans Memorial Highway Opened Today<!-- both this and last ref have same title -->
 | author      = Ministry of Transportation of Ontario
 | publisher   = Government of Ontario
 | date        = August 24, 1998
 | url         = http://www.newswire.ca/government/ontario/english/releases/August1998/24/c4455.html
 | accessdate  = October 26, 2011
 | archiveurl  = https://web.archive.org/web/19991110063704/http://www.newswire.ca/government/ontario/english/releases/August1998/24/c4455.html
 | archivedate = November 10, 1999}}</ref>

On the fifty-fourth anniversary of D-Day, June&nbsp;6, 1998, then Transportation Minister [[Tony Clement]] unveiled two signs in Ottawa and formally declared the entire length of Highway&nbsp;416 as the Veterans Memorial Highway,<ref name="VMH" /> despite earlier reluctance from previous minister [[Al Palladini]].<ref>{{cite news
 | title      = Palladini again (Ontario Transport Minister Al Palladini refuse to designate Highway 416 as Verterans [sic &#93; Memorial Highway)
 | publisher  = S.R. Taylor Publishing
 | work       = Esprit de Corps
 | date       = July 1, 1997
 | url        = http://www.highbeam.com/doc/1G1-30178177.html
 | accessdate = April 16, 2012}}</ref>
Six additional signs were also installed along the length of the route.<ref name="VMH">{{cite news
 | title       = D-Day Anniversary Marked With Naming of Veterans Memorial Highway
 | publisher   = Canadian NewsWire
 | url         = http://www.newswire.ca/government/ontario/english/releases/June1998/06/c1558.html
 | accessdate  = October 13, 2011
 | archiveurl  = https://web.archive.org/web/19991104111852/http://www.newswire.ca/government/ontario/english/releases/June1998/06/c1558.html
 | archivedate = November 4, 1999}}</ref>
At the time, the [[Veterans Memorial Parkway]] in [[London, Ontario|London]] already existed. Since then, two additional veterans highways have been named: on October&nbsp;20, 2002, the Veterans Highway was designated in [[Halton Region]] along Regional Road&nbsp;25;<ref>{{cite news
 | title  = Ceremonies Sunday dedicate Veterans Highway
 | work   = New Tanner
 | date   = October 24, 2002
 | page   = 11
 | volume = 5
 | issue  = 43}}</ref>
on September&nbsp;23, 2010, the Niagara Veterans Memorial Highway was designated in [[Niagara Falls, Ontario|Niagara Falls]] along Regional Road&nbsp;420.<ref>{{cite news
 | title     = Niagara Veterans Memorial Highway now official
 | first     = Tony
 | last      = Ricciuto
 | publisher = Sun Media
 | work      = Niagara Falls Review
 | date      = September 23, 2010}}</ref>
A ceremony was held in Johnstown on September&nbsp;23, 1999 to open the final section of Highway&nbsp;416 that would complete the link from Highway&nbsp;401 to Highway&nbsp;417. Premier [[Mike Harris]], Transportation Minister [[David Turnbull (politician)|David Turnbull]] and World War I Veteran James W. Fraser officially opened the highway.<ref name="opened">{{cite news
 | title      = Ottawa Highway Link Opens
 | agency     = Canadian Press
 | work       = Toronto Star
 | date       = September 24, 1999
 | department = Ontario
 | page       = A4}}</ref>

On December 14, 2009, there was a 60–70 vehicle [[Multiple-vehicle collision|pileup]] due to fog and icy conditions, forcing the closure of the highway in both directions.<ref>{{cite news
 | title       = Fog, Ice Blamed for Huge Ottawa Highway Pileup
 | work        = CBC News
 | date        = December 14, 2009
 | url         = http://www.cbc.ca/canada/montreal/story/2009/12/14/ottawa-highway-416-closed.html
 | accessdate  = October 13, 2011
 | archiveurl  = https://web.archive.org/web/20091217075316/http://www.cbc.ca/canada/montreal/story/2009/12/14/ottawa-highway-416-closed.html
 | archivedate = December 17, 2009}}</ref>

== Exit list ==
{{ONinttop|exit|maint=MTO|length_ref=<ref name="km" />}}
{{ONint
|division         = Leeds and Grenville
|dspan            = 7
|location         = Johnstown
|km               = 0.0
|exit             = &nbsp;
|road             = {{jcon|Hwy|401|city=Kingston|city2=Toronto}}
|notes            = No access from southbound 416 to eastbound 401; no access from westbound 401 to northbound 416; exit 721A on eastbound 401
}}
{{ONint
|location_special = [[Edwardsburgh/Cardinal]]
|lspan            = 2
|km               = 3.1
|exit             = 1
|road             = {{jcon|Hwy|401}} via Highway&nbsp;16&nbsp;– [[Cornwall, Ontario|Cornwall]], [[Montreal]]<br />{{jcon|Hwy|16|dir=south}} to US border
|notes            = Services directions not accessible from previous interchange
}}
{{ONint
|km               = 13.4
|exit             = 12
|road             = {{jcon|LG|21|town=Spencerville}}
|notes            =
}}
{{ONint
|location_special = [[North Grenville]]
|lspan            = 4
|km               = 25.8
|exit             = 24
|road             = {{jcon|LG|20|Oxford Station Road}}
|notes            =
}}
{{ONint
|km               = 30.0
|exit             = 28
|road             = {{jcon|LG|44|Oxford Mills}}
|notes            = Original alignment of [[Ontario Highway 16]] prior to the construction of Highway 16 New in the 1980's.
}}
{{ONint
|km               = 35.6
|exit             = 34
|road             = {{jcon|LG|43|town=Kemptville|town2=Merrickville}}
|notes            =
}}
{{ONint
|km               = 41.6
|exit             = 40
|road             = {{jcon|LG|19|Rideau River Road}}
|notes            =
}}
{{ONint
|municipality     = Ottawa
|lspan            = 8
|km               = 43.8
|exit             = 42
|road             = {{jcon|Ottawa|13|Dilworth Road}}
|notes            =
}}
{{ONint
|km               = 50.3
|exit             = 49
|road             = {{jcon|Ottawa|6|Roger Stevens Drive|town=North Gower}}
|notes            =
}}
{{ONint
|km               = 58.8
|exit             = 57
|road             = {{jcon|Ottawa|8|[[Bankfield Road (Ottawa)|Bankfield Road]]/Brophy Road|town=Manotick|town2=Richmond}}
|notes            =
}}
{{ONint
|km               = 67.4
|exit             = 66
|road             = {{jcon|Ottawa|12|[[Fallowfield Road]]|town=Barrhaven}}
|notes            =
}}
{{ONint
|km               = 72.8
|exit             = 72
|road             = {{jcon|Ottawa|32|[[Hunt Club Road|West Hunt Club Road]]}}
|notes            =
}}
{{ONint
|km               = 75.5
|exit             = 75C
|road             = Holly Acres Road&nbsp;/ [[Richmond Road (Ontario)|Richmond Road]]
|notes            = Northbound exit and southbound entrance
}}
{{ONint
|km               = 76.4
|uspan            = 2
|exit             = 75A
|road             = {{jcon|Hwy|417|The Queensway|dir=west|city=Kanata|town=Pembroke}}
|notes            =
}}
{{ONint
|km               = none
|exit             = 75B
|road             = {{jcon|Hwy|417|The Queensway|dir=east|city=Ottawa}}
|notes            =
}}
{{jctbtm|col=7}}

== See also ==
{{Portal|Canada Roads|Ontario}}
* [[Southern Ontario#Transportation|Southern Ontario Transportation]]{{Clear}}

== References ==
{{Reflist|colwidth=30em}}

== Notes ==
{{reflist|group=nb}}

== External links ==
{{Attached KML|display=title,inline}}
{{Commons category}}
* [http://www.asphaltplanet.ca/ON/highway_416.htm Highway 416 at AsphaltPlanet]
* [https://www.youtube.com/watch?v=rpyNOtYm65A Video of the entire northbound Highway 416]

{{ONT 400 Hwys}}
{{Ottawa Roads}}

[[Category:400-series highways|16]]
[[Category:Monuments and memorials in Ontario]]