[[File:DotNet3.0.svg|thumb|right|300px|This [[application programming interface|API]] is a part of [[.NET Framework]] 3.0]]'''Windows Forms''' (WinForms) is a [[graphical user interface|graphical]] (GUI) [[Library (computing)|class library]] included as a part of [[Microsoft]] [[.NET Framework]],<ref>{{cite book|title=Windows Forms Programming in C#|last=Sells|first=Chris|publisher=Addison-Wesley Professional|edition=1st|date=September 6, 2003|page=xxxviiii}}</ref> providing a platform to write rich client applications for desktop, laptop, and tablet PCs.<ref>{{cite web|url=https://msdn.microsoft.com/en-us/library/ff647339.aspx|title=Design and Implementation Guidelines for Web Clients by Microsoft Pattern and Practices|publisher=Microsoft|date=November 2003}}</ref> While it is seen as a replacement for the earlier and more complex [[C++]] based [[Microsoft Foundation Class Library]], it does not offer a comparable paradigm<ref>{{cite book|chapter=Appendix B|title=Moving from MFC, Windows Forms 2.0 Programming|last1=Sells|first1=Chris|last2=Weinhardt|first2=Michael|publisher=Addison-Wesley Professional|edition=2nd|date=May 16, 2006}}</ref> and only acts as a platform for the user interface tier in a [[Multitier architecture|multi-tier]] solution.<ref>{{cite web|title=Introduction to Windows Forms|type=Visual Studio 2003 documentation|publisher=Microsoft 2003|url=https://msdn.microsoft.com/en-us/library/aa983655(v=vs.71).aspx}}</ref>

== Architecture ==
A ''Windows Forms application'' is an [[event-driven programming|event-driven application]] supported by Microsoft's [[.NET Framework]]. Unlike a [[batch processing|batch program]], it spends most of its time simply waiting for the user to do something, such as fill in a [[text box]] or click a [[push-button|button]].

Windows Forms provides access to native [[Windows USER|Windows User Interface]] Common Controls by wrapping the extant [[Windows API]] in [[managed code]].<ref>{{cite book|url=http://www.informit.com/articles/article.aspx?p=2048355&seqNum=4|title=C# 4.0 Unleashed|last=De Smet|first=Bart|publisher=Sams Publishing|date=January 4, 2011|chapter=Chapter 5}}</ref> With the help of Windows Forms, the .NET Framework provides a more comprehensive abstraction above the Win32 API than Visual Basic or MFC did.<ref>{{cite book|title=NET Windows Forms in a Nutshell|last1=Griffiths|first1=Ian|last2=Adams|first2=Matthew|publisher=O'Reilly Media|date=March 2003|page=4}}</ref>

== Features ==
All visual elements in the Windows Forms class library derive from the Control class. This provides a minimal functionality of a user interface element such as location, size, color, font, text, as well as common events like click and drag/drop. The Control class also has docking support to let a control rearrange its position under its parent. The [[Microsoft Active Accessibility]] support in the Control class also helps impaired users to use Windows Forms better.<ref name="Griffiths2003">{{cite book|title=NET Windows Forms in a Nutshell|last1=Griffiths|first1=Ian|last2=Adams|first2=Matthew|publisher=O'Reilly Media|date=March 2003|pages=27-53}}</ref>

Besides providing access to native Windows controls like button, textbox, checkbox and listview, Windows Forms added its own controls for [[ActiveX]] hosting, layout arrangement, validation and rich data binding. Those controls are rendered using [[Graphics Device Interface|GDI]]+.<ref name="Griffiths2003"/>

== History and future ==
Just like [[Abstract Window Toolkit]] (AWT), the equivalent [[Java (programming language)|Java]] API, Windows Forms was an early and easy way to provide [[graphical user interface]] components to the [[.NET Framework]]. Windows Forms is built on the existing Windows API and some controls merely wrap underlying Windows components.<ref name="monofaq-winform">{{cite web
| url=http://www.mono-project.com/FAQ:_Winforms
| title= FAQ: Winforms
| publisher=mono-project.com
| quote= ''It is very unlikely that the implementation will ever implement everything needed for full compatibility with Windows.Forms. The reason is that Windows.Forms is not a complete toolkit, and to work around this problem some of the underlying Win32 foundation is exposed to the programmer in the form of exposing the Windows message handler''}}</ref> Some of the methods allow direct access to Win32 [[Callback (computer programming)|callbacks]], which are not available in non-Windows platforms.<ref name="monofaq-winform"/>

In .Net 2.0, Windows Forms gained richer layout controls, office 2003 style toolstrip controls, multithreading component, richer design-time and data binding support as well as [[ClickOnce]] for web-based deployment.<ref>{{cite book|chapter=Appendix A. What s New in Windows Forms 2.0|title=Windows Forms 2.0 Programming|last1=Sells|first1=Chris|last2=Weinhardt|first2=Michael|publisher=Addison-Wesley Professional|edition=2nd|date=May 16, 2006}}</ref><ref>{{cite book|title=Data Binding with Windows Forms 2.0: Programming Smart Client Data Applications with .NET|last=Noyes|first=Brian|publisher=Addison-Wesley Professional|edition=1st|date=January 12, 2006|chapter=Preface}}</ref>

With the release of .NET 3.0, Microsoft released a second, parallel API for rendering GUIs: [[Windows Presentation Foundation]] (WPF) based on DirectX,<ref>{{cite book|section=DirectX, not GDI+|title=Pro WPF and Silverlight MVVM: Effective Application Development with Model|last=Hall|first=Gary|publisher=Apress|edition=2010|date=December 27, 2010|page=2}}</ref> together with a GUI declarative language called [[Extensible Application Markup Language|XAML]].<ref>{{cite web
| accessdate = 2011-08-25
| last       = Smith
| first      = Josh
| date       = 2007-09-05
| publisher  = Josh Smith on WPF
| title      = WPF vs. Windows Forms
| quote      = WPF is not intended to replace Windows Forms. [...] Windows Forms is still alive and well, and will continue to be enhanced and supported by Microsoft for years to come. WPF is simply another tool for Windows desktop application developers to use, when appropriate. 
| url        = https://joshsmithonwpf.wordpress.com/2007/09/05/wpf-vs-windows-forms/}}</ref> <!--Since WPF is relatively new, it is unclear if Windows Forms will continue to be improved in future .NET releases.<ref>{{cite web
| url=http://joshsmithonwpf.wordpress.com/2007/09/05/wpf-vs-windows-forms/
| title= WPF vs. Windows Forms
| first=Josh|last=Smith
| date=2007-09-05
| accessdate=2008-07-26}}</ref><ref>{{cite web
| url=http://blogs.msdn.com/mharsh/archive/2004/09/20/231888.aspx
| title= WPF vs. Windows Forms
| first=Mike|last=Harsh
| quote=''If you’re targeting only Windows XP and Longhorn, Avalon is the way to go.  But Windows Forms is still the only way to write managed UI that will run on Win 2K and below''
| date=2004-09-20
| accessdate=2008-07-26}}</ref><ref>{{cite web
| url=http://www.longhorncorner.com/Blogs/BlogDetail.aspx?BlogId=660
| title= Future of Windows Forms and ASP.NET
| first=Mahesh|last=Chand
| date=2008-05-08
| accessdate=2008-07-26}}</ref>-->

During a Q and A session at the [[Build (developer conference)|Build 2014]] Conference, Microsoft explained that Windows Forms was under maintenance mode, with no new features being added, but bugs found would still be fixed.<ref>{{cite web
| url=http://www.infoq.com/news/2014/04/WPF-QA
| title=A WPF Q&A
| publisher=infoq.com
| date=2014-04-03
| quote=''Windows Forms is continuing to be supported, but in maintenance mode. They will fix bugs as they are discovered, but new functionality is off the table''
| accessdate=2014-04-21}}</ref> Most recently, improved high-DPI support for various Windows Forms controls was introduced in updates to .NET Framework version 4.5.<ref>{{Cite web|url = http://www.infoq.com/news/2014/05/DotNet-4-5-2|title = High DPI Improvements for Windows Forms in .NET 4.5.2|date = 2014-05-06|accessdate = 2015-02-10|website = InfoQ|publisher = |last = Allen|first = Jonathan}}</ref>

== Alternative implementation ==
[[Mono (software)|Mono]] is a project led by [[Xamarin]] (formerly by [[Ximian]], then [[Novell]]) to create an [[Ecma International|Ecma]] standard compliant [[.NET Framework|.NET]] compatible set of tools.

[[Mono (software)|Mono]]'s support for System.Windows.Forms as of [[.NET Framework version history#.NET Framework 2.0|.NET 2.0]] is announced as complete;<ref>{{cite web
| url=http://www.mono-project.com/WinForms
| title= WinForms
| publisher=mono-project.com
| quote=''Support for Windows Forms 2.0 is complete. At this point, we are largely just fixing bugs and polishing our code. ''
| accessdate=2011-07-30}}</ref> also System.Windows.Forms 2.0 works natively on Mac OS X.<ref>{{cite web
| url=http://www.mono-project.com/FAQ:_Winforms
| title= WinForms
| publisher=mono-project.com
| quote=''Does Winforms run on OSX? Yes, as of Mono 1.9, Winforms has a native OSX driver that it uses by default''
| accessdate=2011-07-30}}</ref> However, System.Windows.Forms is not actively developed on Mono,<ref>{{cite web
 |url=http://tirania.org/blog/archive/2011/Mar-07.html
 | title=GDC 2011
 | last=de Icaza
 | first=Miguel
 | authorlink=Miguel de Icaza
 |date=2011-03-07
 |accessdate=2011-07-30
 |quote=''For tools that are mostly OpenGL/DirectX based, use Windows.Forms, keeping in mind that some bug fixing or work around on their part might be needed as our Windows.Forms is not actively developed.''}}</ref> and full compatibility with .NET is not achieved and is not possible, because Microsoft's System.Windows Forms is mainly a wrapper around the [[Windows API]], and some of the methods allow direct access to Win32 [[Callback (computer programming)|callbacks]], which are not available in platforms other than Windows.<ref name="monofaq-winform"/>

== See also ==
* [[Microsoft Visual Studio]]
* [[ClickOnce]]
* [[Abstract Window Toolkit]] (AWT), the equivalent [[graphical user interface|GUI]] [[application programming interface]] (API) for the [[Java (programming language)|Java]] programming language
* [[Visual Component Library]] (VCL) from Borland 
* [[Visual Test]], test automation

== References ==
{{reflist|2}}

== External links ==
<!--===========================({{NoMoreLinks}})===============================
    | PLEASE BE CAUTIOUS IN ADDING MORE LINKS TO THIS ARTICLE. WIKIPEDIA IS   |
    | NOT A COLLECTION OF LINKS NOR SHOULD IT BE USED FOR ADVERTISING.        |
    |                                                                         | 
    |               Excessive or inappropriate links WILL BE DELETED.         |
    |  See [[Wikipedia:External links]] and [[Wikipedia:Spam]] for details.   |
    |                                                                         | 
    | If there are already plentiful links, please propose additions or       |
    | replacements on this article's discussion page.  Or submit your link    |
    | to the appropriate category at the Open Directory Project (www.dmoz.org)|
    | and link back to that category using the {{dmoz}} template.             |
    ===========================({{NoMoreLinks}})===============================-->
* [http://www.microsoft.com/events/series/windowsforms.mspx MSDN: Building Windows Forms applications]
* [https://msdn.microsoft.com/en-us/library/dd30h2yb.aspx MSDN : Windows.Forms reference documentation]
* [https://msdn.microsoft.com/en-us/library/ms996405.aspx MSDN : Windows Forms Technical Articles - Automating Windows Form with Visual Test]

{{.NET Framework}}
{{Widget toolkits}}

[[Category:.NET Framework]]
[[Category:Microsoft application programming interfaces|Forms]]
[[Category:Widget toolkits]]