{{Infobox military conflict
|conflict=Battle of Bandiradley
|partof=the [[War in Somalia (2006–2009)]]
|date=December 23&ndash;25, 2006
|image=[[File:Battle-of-bandiradley-12262006.svg|299px]]
|place=[[Bandiradley]], [[Somalia]]
|caption=(click to expand)
|result=Ethiopian/Puntland victory
|combatant1=[[File:Somalia Islamic Courts Flag.svg|22px]] [[Islamic Courts Union]]
|combatant2={{flag|Ethiopia|1996}}<br>{{flag|Puntland}}
|commander1= [[Mohamed Mohamud Agaweine]]<ref name=SOMALI-ISLAMISTS-DEPLOYS-TROOPS-NEAR-ETHIOPIAN-BORDER>[http://www.sudantribune.com/spip.php?article18919 Somali Islamists deploys troops near Ethiopian border] Sudan Times</ref>
|commander2=Puntland: Gen. [[Mohamud Muse Hersi|Adde Musa]]<br>[[Abdulrahman Said Dhegaweyne]]<br>Warlord Col. [[Abdi Qeybdid]]
|strength1=N/A
|strength2=500+
|casualties1=N/A
|casualties2=1 helicopter gunship}}
{{Campaignbox War in Somalia (2006–2009)}}
The '''Battle of Bandiradley''' in [[Somalia]] began on December 23, 2006, when [[Puntland]] and [[Ethiopia]]n forces, along with faction leader [[Abdi Qeybdid]], fought [[Islamic Courts Union]] (ICU) militants defending [[Bandiradley]]. The fighting pushed the Islamists out of Bandiradley and over the border south into [[Adado]] district, [[Galgadud]] region, by December 25.

==Background==
===Somali Civil War===
{{main|Somali Civil War (2006)}}
The battle has roots in the long-standing [[Somali Civil War]]. The areas of Galgudud and Mudug were drawn into the conflict arising between the state of Puntland, and the areas coming under the control of the ICU. While local leaders tried to organize into the autonomous state of [[Galmudug]], over time it was forced to side with the forces of Puntland and Ethiopia in order to repel the ICU.

===Prior Ethiopian interventions in the area===
{{main|War in Somalia (2006–2009)}}

The borders of Galgadud and Mudug regions were under dispute with Ethiopia following the August 1982 border clashes. The towns of [[Balanbale]] and [[Goldogob]] had been under Ethiopian occupation from that time up until June 1988 when all troops were to pull back 9 miles from the disputed borders, and Ethiopia granted back the towns to Somalia.<ref name=SOMALIA-CEDES-ERITREA-FIGHTS>[http://www.financial-journalist.co.uk/financial_journalist/?cat=7 Somalia cedes, Eritrea fights] Financial Journalist</ref>

On March 7–8, 1999, Ethiopia claimed it had made a cross-border incursion into Ballanballe searching for members of [[Al-Itihaad al-Islamiya]] (AIAI) who had reportedly kidnapped a person and stolen medical supplies, and denied reports of looting. Allegations from that time also claim Ethiopia was the supplier of various Somali warlords, while Eritrea was arming other warlords.<ref name=ETHIOPIA-DENIES-LOOTING-SOMALI-BORDER-TOWN>[http://www.irinnews.org/report.asp?ReportID=14333&SelectRegion=Horn_of_Africa ETHIOPIA-SOMALIA: Ethiopia denies looting Somali border town] IRIN</ref><ref name=SOMALIA-EMERGING-THIRD-FRONT-IN-THE-ETHIOPIA-ERITREA-WAR>[https://www.stratfor.com/products/premium/read_article.php?id=100645&selected=Country%20Profiles&showCountry=1&countryId=41&showMore=1 Somalia - Emerging Third Front in the Ethiopia-Eritrea War?]{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }} Stratfor</ref>

==Prelude to battle==
On August 14, 2006, local tribal leaders opposed to the advance of the ICU, as well as wishing autonomy from the state of Puntland, organized  Ethio-Puntland Militias and put Abdi Qeybdid in charge of them.
<ref>[http://www.pinr.com/report.php?ac=view_report&report_id=543&language_id=1 Somalia's Conflict Enters a Phase of Duress BY Michael A. Weinstein] {{webarchive |url=https://web.archive.org/web/20061106003644/http://www.pinr.com/report.php?ac=view_report&report_id=543&language_id=1 |date=November 6, 2006 }} PINR</ref>

On November 12, the ICU took the town of Bandiradley after a firefight with the forces of Abdi Qeybdid.<ref name=SOMALIA-IN-MID-NOVEMBER-SPARRING-AND-WAITING-FOR-SOMEONE-TO-STRIKE>[http://www.pinr.com/report.php?ac=view_printable&report_id=582&language_id=1 Somalia in mid-November: Sparring and Waiting for Someone to Strike] {{webarchive |url=https://web.archive.org/web/20070927004900/http://www.pinr.com/report.php?ac=view_printable&report_id=582&language_id=1 |date=September 27, 2007 }} PINR</ref>

On November 13, the President of Puntland, General [[Mohamud Muse Hersi|Adde Musa]] personally led 50 [[Technical (fighting vehicle)|battlewagon]]s to Galkayo to confront the Islamists.<ref>[http://www.shabelle.net/news/ne1699.htm Somalia: Puntland president deploys 50 battlewagons in Galkayo to avert Islamist fighters] Shabelle Media Network</ref>

On November 22, Ethiopia imposed a curfew on the town of Ballanballe and was searching residents entering or leaving the village in response to attacks on Ethiopian convoys in Somalia<ref name=ETHIOPIAN-TROOPS-IMPOSE-CURFEW-ON-A-SOMALI-TOWN>[http://www.shabelle.net/news/ne1749.htm Ethiopian troops impose curfew on a Somali town] {{webarchive |url=https://web.archive.org/web/20070927200653/http://www.shabelle.net/news/ne1749.htm |date=September 27, 2007 }}</ref>

On November 26, thousands of ICU troops were reported deploying in Abudwaq, within {{convert|15|km|mi|abbr=on}} of the Ethiopian border.<ref name=SOMALI-ISLAMISTS-DEPLOYS-TROOPS-NEAR-ETHIOPIAN-BORDER/> On November 27, troops were forcibly returning people who had fled to avoid fighting to their homes, reassuring them they would not come to harm.<ref name=SOMALIA-ISLAMIC-COURTS-SEND-DISPLACED-PEOPLE-BACK-TO-ABUDWAQ>[http://www.somalispot.com/forums/politics.24/ Somalia: Islamic Courts send displaced people back to Abudwaq] SomaliSpot</ref>

On November 28, Ethiopian forces in the Galkayo, Mudug area were estimated to be about 500-strong, with over 100 vehicles including tanks. There was an exchange of gunfire and missiles. Afterwards, the ICU held a rally in Bandiradley, at which ICU commander Sheikh [[Sharif Sheikh Ahmed]] accused the Ethiopians of firing 12 missiles at Islamist positions.<ref name=TROOP-MOVEMENTS-IN-MUDUG-REGION>[http://www.garoweonline.com/stories/publish/article_6223.shtml Somalia: Troop movements in Mudug region]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Garowe Online</ref>

On December 1, in Galkayo, it was reported 9 clerics of the Islamic [[Tabliq]] sect had been arrested under the orders of Colonel Abdi Qeybdiid.<ref>[http://www.somalispot.com/forums/politics.24/ Somalia: Islamist preachers arrested in Galkaayo city] SomaliSpot</ref>

On December 7–8, the militia of warlord Abdi Qeybdid took part in skirmishes against forces of the ICU near the small settlement of Sadah Higle between Bandiradley and Galkayo.<ref name=SOMALIA-ISLAMIST-FIGHTERS-AND-ETHIOPIAN-BACKED-MILITIA-CLASH-IN-BANDIRADLEY>[http://www.shabelle.net/news/ne1825.htm  Somalia: Islamist fighters and Ethiopian backed militias clash in Bandiradley] {{webarchive |url=https://web.archive.org/web/20070927200541/http://www.shabelle.net/news/ne1825.htm |date=September 27, 2007 }} Shabelle Media Network</ref> This rapidly led to an exchange of shelling between Ethiopian and ICU troops. Hundreds of Ethiopian troops accompanied by forces from Puntland took up position near the town. Puntland forces claimed they had been provoked by rocket and mortar fire. ICU forces stated Ethiopian troops "started firing missiles toward our positions." At least one ICU fighter was claimed killed in the exchange.<ref name=BATTLE-RAGES-BETWEEN-ICU-FIGHTERS-ETHIOPIAN-BACKED-MILIITIA-IN-CENTRAL-SOMALIA>[http://www.somalispot.com/forums/politics.24/ Battle rages between ICU fighters-Ethiopian backed militia in central Somalia] SomaliSpot</ref><ref name=FRESH-FIGHTING-ERUPTS-IN-SOMALIA>[http://english.aljazeera.net/NR/exeres/A7609FD0-D750-41AB-B85C-08786296486E.htm Fresh fighting erupts in Somalia] Al Jezeera</ref>

On December 16, it was reported a local Islamic court named Imamu Shahfici was set up in Abudwaq. It urged Islamists to resist the Ethiopians<ref name=SOMALIAS-ISLAMiSTS-IN-CENTRAL-SOMALIA-URGE-PEOPLE-TO-FIGHT-ETHIOPIA>[http://www.shabelle.net/news/ne1868.htm Somalia’s Islamists in central Somalia urge people to fight with Ethiopia] {{webarchive |url=https://web.archive.org/web/20070927200617/http://www.shabelle.net/news/ne1868.htm |date=September 27, 2007 }} Shabelle Media Network</ref>

Further combat was kept in abeyance until the general outbreak of hostilities on December 20.

On December 19, 18 [[Technical (fighting vehicle)|technicals]] and a large number of Ethiopian troops entered Ballanballe, Galgadud province, to reinforce troops already positioned in the town.<ref name=FRESH-ETHIOPIAN-TROOPS-ENTER-SOMALIAS-BALLANBALLE-AREA>[http://www.somalispot.com/forums/politics.24/ Fresh Ethiopian troops enter Somalia’s Ballanballe area] SomaliSpot</ref> Just prior to the battle, on December 22, Ethiopian troops departed Ballanballe where they had been in occupation for the past three months. This was reportedly done at the urging of the tribal elders, who did not wish fighting to break out between the ICU and Ethiopia in their town.<ref name=SOMALIA-ETHIOPIAN-TROOPS-LEAVE-BALLANBALLE>[http://www.shabelle.net/news/ne1920.htm  Somalia: Ethiopian troops leave Ballanballe] {{webarchive |url=https://web.archive.org/web/20070930180226/http://www.shabelle.net/news/ne1920.htm |date=September 30, 2007 }} Shabelle Media Network</ref>

==Battle==
On December 22, Ethiopian troops were said to be amassing in Galkayo for what might turn into a second front of the war near [[Puntland]].<ref name=ETHIOPIAN-TANKS-ROLL-TOWARDS-BATTLEFRONT>[http://today.reuters.co.uk/news/articlenews.aspx?type=worldNews&storyID=2006-12-22T115611Z_01_L22886890_RTRUKOC_0_UK-SOMALIA-CONFLICT.xml&pageNumber=2&imageid=&cap=&sz=13&WTModLoc=NewsArt-C1-ArticlePage2 Ethiopian tanks roll towards battlefront]{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }} Reuters</ref> The following day, 500 Ethiopian troops and 8 tanks were reported to be heading towards Bandiradley.<ref>[http://news.independent.co.uk/world/africa/article2097758.ece  Ethiopia edges closer to Somalia invasion] {{webarchive |url=https://web.archive.org/web/20070108000000/http://news.independent.co.uk/world/africa/article2097758.ece |date=January 8, 2007 }} The Independent</ref> Islamist fighters retreated from their positions. They were pursued south to the area between [[Galinsoor]] and Bandiradley, where the Islamists were defeated.<ref name=SOMALIA-ISLAMIST-MILITIA-PUSHED-FURTHER-SOUTH-OF-PUNTLAND>[http://www.garoweonline.com/stories/publish/article_6666.shtml Somalia: Islamist militia pushed further south of Puntland]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Garowe Online</ref> The ICU forces were further pursued to Adado in Galgadud, which they abandoned late on December 25, 2006.

==Aftermath==
The ICU abandoned the towns of Dhuusamareb<ref name=ETHIOPIAN-TROOPS-RETURNING-WARLORDS-TO-POWER-IN-SOMALIA>[http://www.garoweonline.com/stories/publish/article_6704.shtml Ethiopian troops returning warlords to power in Somalia] {{webarchive |url=https://web.archive.org/web/20070106000000/http://www.garoweonline.com/stories/publish/article_6704.shtml |date=January 6, 2007 }} Garowe Online</ref> and Abudwaq without fighting. In the wake of their withdrawal from Abudwaq, militias set up checkpoints and began firing their weapons.<ref name=INSECURITY-RAGES-IN-ISLAMIST-ABANDONED-AREAS>[http://www.shabelle.net/news/ne1952.htm Somalia: Insecurity rages in Islamist abandoned areas] {{webarchive |url=https://web.archive.org/web/20070930000000/http://www.shabelle.net/news/ne1952.htm |date=September 30, 2007 }} Shabelle Media Networks</ref>

==References==
{{Reflist|2}}

{{War in Somalia (2006–2009)}}

{{coord missing|Somalia}}

{{DEFAULTSORT:Battle Of Bandiradley}}
[[Category:2006 in Ethiopia]]
[[Category:2006 in Somalia]]
[[Category:Battles involving Ethiopia]]
[[Category:Battles involving Somalia]]
[[Category:Somali Civil War]]
[[Category:Battles of the War in Somalia (2006–2009)]]
[[Category:December 2006 events]]