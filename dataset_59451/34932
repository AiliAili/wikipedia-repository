{{Infobox hurricane season
| Basin=EPac
| Year=1939
| First storm formed=June 12, 1939
| Last storm dissipated=October 25, 1939
| Strongest storm name=Ten
| Strongest storm winds=
| Strongest storm pressure=930
| Total storms=10
| Total hurricanes=4
| Fatalities=93
| Damage=2
| five seasons=[[1937 Pacific hurricane season|1937]],[[1938 Pacific hurricane season|1938]], '''1939''', [[1940 Pacific hurricane season|1940]], [[1941 Pacific hurricane season|1941]]
}}
The '''1939 Pacific hurricane season''' ran through the summer and fall of 1939. Before the [[weather satellite|satellite]] age started in the 1960s, data on east Pacific hurricanes are extremely unreliable. Most east Pacific storms are of no threat to land. However, 1939 saw a large number of storms threaten [[California]].

==Systems==

===Hurricane One===
On June 12, a hurricane was detected. The [[barometric pressure|lowest pressure]] reported by a ship was {{convert|985|mbar|inHg|abbr=on}}. The hurricane was last seen June 13.<ref name=Hurd3906>{{cite journal|format=PDF|title=North Pacific Ocean, June 1939|work=[[Monthly Weather Review]]|accessdate=2011-01-18|first=Willis |last=Hurd|date=June 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/067/mwr-067-06-0190b.pdf}}</ref>

===Possible Tropical Cyclone Two===
A possible tropical cyclone was located off the coast of Mexico on June 27.  A ship reported a gale and a pressure of {{convert|1006|mbar|inHg|abbr=on}}. The system was last seen on June 28.<ref name=Hurd3906/>

===Tropical Cyclone Three===
On July 19, a tropical cyclone was detected. A ship reported a pressure of {{convert|1000.7|mbar|inHg}}.<ref name=Hurd3907>{{cite journal|format=PDF|title=North Pacific Ocean, July 1939|work=[[Monthly Weather Review]]|accessdate=2011-01-18|first=Willis |last=Hurd|date=July 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/067/mwr-067-07-0226b.pdf}}</ref>

===Tropical Cyclone Four===
On July 29, a tropical cyclone was located midway between [[Manzanillo, Colima|Manzanillo]] and [[Acapulco]]. It moved up the coast, and a ship reported a pressure of {{convert|1000|mbar|inHg|abbr=on}} on July 29 as the cyclone made landfall in the vicinity of Manzanillo.<ref name=Hurd3907/>

===Tropical Cyclone Five===
A small tropical cyclone was detected on August 31.  A ship reported gales and a pressure of {{convert|1003.3|mbar|inHg}}.<ref name=Hurd3908>{{cite journal|format=PDF|title=North Pacific Ocean, August 1939|work=[[Monthly Weather Review]]|accessdate=2011-01-18|first=Willis |last=Hurd|date=August 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/067/mwr-067-08-0309b.pdf}}</ref>

===Hurricane Six===
From September 4 to 7, the remnants of a hurricane brought heavy rain to Southern California. The storm delivered over a year's worth of rainfall to [[Blythe, California|Blythe]], while [[Imperial, California|Imperial]] received more than two years' worth.<ref name=Williams>{{cite news|work=[[USA Today]]|first=Jack|last=Williams|title=Background: California's tropical storms|date=2005-05-17|url=http://www.usatoday.com/weather/whhcalif.htm|accessdate=2012-05-12}}</ref> The flooding caused major damage in [[Mecca, California]], and {{convert|3|ft}} of water swamped [[Thermal, California|Thermal]].<ref name=SignificantEvents>{{cite web|url=http://www.wrh.noaa.gov/sgx/document/weatherhistory.pdf|title=A History of Significant Weather Events in Southern California |date=January 2007 |publisher=National Weather Service Forecast Office San Diego|format=PDF |accessdate=2012-05-12}}</ref>

===Hurricane Seven===
A tropical cyclone was first detected south of Acapulco on September 5.  It intensified into a hurricane and moved northwestward. A ship sailing through the eye reported a pressure reading of {{convert|948|mbar|inHg|abbr=on}}. The tropical cyclone made landfall somewhere along the [[Baja California Peninsula]]. It dissipated inland over the northern part of the peninsula on September 12.<ref name=Hurd3909>{{cite journal|format=PDF|title=North Pacific Ocean, September 1939|work=[[Monthly Weather Review]]|accessdate=2011-01-18|first=Willis |last=Hurd|date=September 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/067/mwr-067-09-0356.pdf}}</ref> Remnants of this tropical storm, in association with a [[trough (meteorology)|trough]], caused rain of up to {{convert|4|in|mm}} in southern California on September 11 and 12.<ref name=Williams/>

===Tropical Cyclone Eight===
On September 5, a tropical cyclone formed off the coast of Costa Rica. It also headed northwest and dissipated over the southern part of Baja California on September 15. The lowest reported pressure was {{convert|1004|mbar|inHg|abbr=on}}.<ref name=Hurd3909/> From September 19 to 21, remnants of this tropical cyclone caused rain measuring up to {{convert|3|in|mm}} in Southern California.<ref name=Williams/>

===Hurricane Nine===
{{main|1939 California tropical storm}}
[[File:1939 California storm.jpg|thumb|Map showing affects of Hurricane Nine on California]]
On September 14, a tropical cyclone formed off the coast of Central America.  This tropical storm tracked northwestward and intensified into a hurricane.<ref name=SignificantEvents/>  The sea-level pressure dropped to {{convert|975|mbar|inHg|abbr=on}} or lower.  The hurricane recurved gradually to the northeast and weakened over cool seas. On September 25, this tropical storm made landfall near [[Long Beach, California]], and dissipated inland.<ref name=Hurd3909/>

The tropical storm caught Southern Californians unprepared.<ref name=SignificantEvents/> It brought heavy rain and flooding to the area, which killed 45 people.<ref name=Hurd3909/> At sea, 48 were killed.  The storm caused heavy property damage amounting to $2 million (1939 [[USD]]) in total, mostly to crops and coastal infrastructure.<ref name=SignificantEvents/>

{{clear}}

===Hurricane Ten===
[[File:Hurricane Ten Mexico analysis 25 Oct 1939.png|thumb|Surface analysis of Hurricane Ten making landfall on October&nbsp;25]]
On October&nbsp;23, a tropical cyclone formed south of [[Cabo Corrientes]]. It intensified and headed roughly due north.  A steamer, the ''Nevadan'', caught in the eye of this extremely intense hurricane, recording a corrected central pressure of 930&nbsp;mbar (hPa; 27.46&nbsp;inHg).<ref name="Additional Note 1939">{{cite journal|work=[[Monthly Weather Review]] |first=Willis |last=Hurd |title=Additional Note on the Mexican West Coast Cyclone of October 23–25, 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/068/mwr-068-01-0029b.pdf |accessdate=2011-01-18}}</ref> Even with modern [[tropical cyclone observation]] techniques available, this reading still qualifies this cyclone as one of the most intense on record and would likely have made it a Category 4 or 5 hurricane.{{EPAC hurricane best track}} The steep pressure gradient between the Nevadan and the external hurricane conditions off of [[Manzanillo, Colima]] caused several tarpaulins to burst.<ref name="Additional Note 1939" /> Other shipping was disrupted off the Mexican coast by the intense tropical cyclone.<ref name=Hurd3910 />

The hurricane made landfall near Cabo Corrientes on October&nbsp;25 and dissipated shortly thereafter.<ref name=Hurd3910>{{cite journal|format=PDF|title=North Pacific Ocean, October 1939|work=[[Monthly Weather Review]]|accessdate=2011-01-18|first=Willis |last=Hurd|date=June 1939 |url=http://docs.lib.noaa.gov/rescue/mwr/067/mwr-067-10-0406b.pdf}}</ref> Onshore, the storm caused an extensive swath of damage. Homes were destroyed in the towns of [[Santiago Ixcuintla]] and [[Rosamorada]] in the Mexican state of [[Nayarit]],<ref name=Swept /> displacing hundreds of people.<ref name=EntireTown>{{cite news|title=Hurricane Damages Towns In Mexico|url=https://www.newspapers.com/clip/944777/october_1939_mexico_hurricane/?|accessdate=August 31, 2014|work=The Modesto Bee|date=October 27, 1939|location=Corpus Christi, Texas|page=1|via=[[Newspapers.com]]|agency=Associated Press|volume=20|issue=83}} {{Open access}}</ref> In [[Puerto Vallarta]], a strong [[storm surge]] flooded a section of the town, destroying several homes. [[Tobacco]], [[corn]], and [[rice]] crops in the region suffered considerable damage.<ref name=Swept>{{cite news|title=Mexican Coast Is Swept By Hurricane|url=https://www.newspapers.com/clip/944758/october_1939_mexico_hurricane/?|accessdate=August 31, 2014|work=The Modesto Bee|date=October 27, 1939|location=Modesto, California|page=2|via=[[Newspapers.com]]|agency=Associated Press}} {{Open access}}</ref> The strong winds downed [[power line]]s, resulting in the delayed dissemination of damage reports.<ref name=Rip>{{cite news|title=Storm Rips Mexico Coast|url=https://www.newspapers.com/clip/944794/october_1939_mexico_hurricane/?|accessdate=August 31, 2014|work=El Paso Herald Post|date=October 27, 1939|location=El Paso, Texas|page=5|via=[[Newspapers.com]]|agency=United Press International|volume=59|issue=259}} {{Open access}}</ref> Although no exact casualty total was documented, reports indicated that the tropical cyclone caused a "few casualties". After the storm, US$6,000&nbsp;was donated to help aid the displaced in the states of Nayarit and [[Jalisco]], while doctors and nurses were sent to those areas.<ref name=StormRelief>{{cite news|title=Mexico Speeds Storm Relief|url=https://www.newspapers.com/clip/944816/october_1939_mexico_hurricane/?|accessdate=August 31, 2014|work=Star-Monitor-Herald|date=October 29, 1939|location=Harlingen, Texas|page=5|via=[[Newspapers.com]]|agency=Associated Press|volume=3|issue=16}} {{Open access}}</ref> 
{{clear}}

==See also==
{{portal|tropical cyclones}}
*[[1939 Atlantic hurricane season]]
*[[1939 Pacific typhoon season]]

==References==
{{Reflist|2}}

{{Good article}}

[[Category:1939 in California]]
[[Category:Pacific hurricane seasons]]