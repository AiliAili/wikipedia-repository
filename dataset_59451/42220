__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=A-1
 | image= Antonov A-1 (A-1-83) (9717448281).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Training glider
 | national origin=USSR
 | manufacturer=
 | designer=[[Oleg Konstantinovich Antonov]]
 | first flight=1930
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=ca. 5,700
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Antonov A-1''' and related designs were a family of single-seat training gliders produced in the Soviet Union in the 1930s and 1940s.  All were derived from the '''Standard-2''' (Стандарт-2) (designed and flown by [[Oleg Konstantinovich Antonov]] in 1930<ref name="Sheremetev 20">Sheremetev 1959, 20</ref>), which in turn were derived from the '''Standard-1'''.<ref name="Krasil'shchikov 145">Krasil'shchikov 1991, 145</ref> They were produced in large numbers, with around 5,400 built of the U-s3, U-s4 and P-s2 major versions alone.<ref name="Monino">Central Museum of the Air Force</ref> The same design formed the basis for the [[Antonov A-2]] and its related group of two-seat designs. Altogether, including the two-seaters, production exceeded 7,600 by 1937.<ref name="Krasil'shchikov 143">Krasil'shchikov 1991, 143</ref>

While members of the family varied in detail, they shared the same basic design, and parts were interchangeable between them.<ref name="Sheremetev 20" /><ref name="Shushurin 13">Shushurin 1938, 13</ref> The design featured a typical primary glider layout with a conventional [[empennage]] carried at the end of a long boom in place of a conventional [[fuselage]]. The boom could be folded sideways for storage.<ref name="Shushurin 16">Shushurin 1938, 16</ref> The monoplane wing was carried high on a pylon above this "keel" and was further braced to it with two struts either side.<ref name="Sheremetev 21-22">Sheremetev 1959, 21–22</ref> The pilot sat in front of the wing, and was enclosed in a simple U-shaped wooden fairing that was removed by sliding it forward to allow him or her to enter and leave the craft.<ref name="Sheremetev 40">Sheremetev 1959, 40</ref> The undercarriage consisted of a single skid underneath the "keel", but this could also be fitted with small wooden wheels.<ref name="Sheremetev 42">Sheremetev 1959, 42</ref>

While the original primary training versions (designated ''У'' – "U") featured wings of constant chord,<ref name="Sheremetev 21-22" /> subsequent variants designed for soaring flight (designated ''П'' – "P") had longer-span wings with tapering outer panels and a streamlined nose fairing.<ref name="Krasil'shchikov 146">Krasil'shchikov 1991, 146</ref> The ultimate development in the line were gliders intended for towed flight (designated ''Б'' – "B"), which shared the longer wings and streamlined fairing of the P-types, but added a canopy to enclose the cockpit.<ref name="Krasil'shchikov 146" />

Unlicensed copies were produced in Turkey following World War II by [[Türk Hava Kurumu|THK]] and ''Makina ve Kimya Endüstrisi Kurumu'' - MKEK, as the '''THK-7''' (P-s2), '''THK-4''' (U-s4) and '''MKEK 6'''.<ref name="Deniz">Deniz 2004</ref>

<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==
In each case, the "s" stands for ''serii'' (серии – "series")
;Prototypes
::'''Standard-1''' (Стандарт-1)
::'''Standard-2''' (Стандарт-2)
;''Uchebnyi'' (Учебный – "Trainer")
::'''U-s1''' (У-с1)
::'''U-s2''' (У-с2) - First version built in series<ref name="Krasil'shchikov 145" />
::'''U-s3''' (У-с3) - 1,600 built<ref name="Monino" />
::'''U-s4''' (У-с4) - Redesignated '''A-1''' – major production version (3000 built<ref name="Monino" />)
;''Paritel' '' (Паритель – "Sailplane") aka '''Upar''' (Упар, portmanteau of ''uchebnyi paritel' '' –  учебный паритель – "training sailplane") (800 built<ref name="Monino" />)
::'''P-s1''' (П-с1)
::'''P-s2''' (П-с2)
;''Buksirovochnye'' (Буксировочные – "Towed") (265 built by 1937<ref name="Krasil'shchikov 143" />)
::'''B-s3''' (Б-с3)
::'''B-s4''' (Б-с4)
::'''B-s5''' (Б-с5)
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (A-1) ==
[[File:Antonov A-1.svg|right|300px]]
{{aerospecs
|ref=<!-- reference -->Krasil'shchikov 1991, 230
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=One pilot
|capacity=
|length m=5.60
|length ft=18
|length in=5
|span m=10.56
|span ft=34
|span in=8
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=1.70
|height ft=5
|height in=7
|wing area sqm=15.6
|wing area sqft=168
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=92
|empty weight lb=200
|gross weight kg=164
|gross weight lb=361
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=70
|max speed mph=40
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->1.2
|sink rate ftmin=<!-- sailplanes -->240

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Antonov aircraft}}
* {{cite web |title=Antonov Oleg Konstantinovich |work=Central Museum of the Air Force website |url=http://www.monino.ru/index.sema?a=aviation&sa=info&id=3 |accessdate=2008-10-06}}
* {{cite book |last= Deniz |first= Tuncay |title=Turkish Aircraft Production |year=2004 |publisher=Levent Başara |location=Munich |pages= }}
* {{cite book |last=Krasil'shchikov |first=Aleksandr Petrovich |title=Planery SSSR (Gliders of the USSR) |year=1991 |publisher=Moskva Mashinostroyeniye |location=Moscow}}
* {{cite book |last= Sheremetev |first= Boris Nikolayevich |title=Planery (Gliders) |year=1959 |publisher=[[DOSAAF]] |location=Moscow }}
* {{cite book |last= Shushurin |first=V.V. |title=Atlas konstruktzii planerov (Directory of glider construction) |year=1938 |publisher=Gosudarstvennoe izdatel'stvo oboronnoi promyshlennosti |location=Moscow }}
<!-- ==External links== -->

{{Antonov aircraft}}
{{THK aircraft}}
{{MKEK aircraft}}

[[Category:Soviet sailplanes 1930–1939]]
[[Category:Glider aircraft]]
[[Category:Antonov aircraft|A-01]]