{{Infobox astronaut
| name          = James M. Taylor
| image         = Lt. Col. James M. Taylor 1960s.jpg
| type          = [[USAF]] Astronaut
| nationality   = American
| birth_date    = November 27, 1930
| death_date    = {{Death date and age|1970|09|04|1930|11|27}}
| birth_place   = [[Stamps, Arkansas]], U.S.
| death_place   = [[Palmdale, California]], U.S.
| alma_mater    = [[University of Michigan]], B.S. 1959
| occupation    = [[Test pilot]]
| selection     = [[List of astronauts by selection#1965|1965 USAF MOL Group 1]]
| space_time    =
| missions      = None
| insignia      =
| retirement    = 1969
}}

'''James Martin Taylor''' (November 27, 1930 – September 4, 1970) was a [[United States Air Force]] [[astronaut]] and [[test pilot]]. Although he trained for the USAF [[Manned Orbital Laboratory]] (MOL), the program was cancelled before any of the MOL crews reached space.<ref name=SMC_history>{{cite web |url=http://www.losangeles.af.mil/shared/media/document/AFD-060912-028.pdf |title=Space and Missile Systems Center History |accessdate=2008-10-11 |date=2006-09-12 |publisher=USAF |pages=60–62 |format=[[PDF]]}}</ref>

Taylor was born November 27, 1930, in [[Stamps, Arkansas]], and graduated from the [[University of Michigan]] with a [[Bachelor of Science]] degree in [[Electrical Engineering]] in 1959.<ref name=astrospies_taylor>{{cite web |url=http://www.pbs.org/wgbh/nova/astrospies/prof-06.html |title=Astrospies Taylor biography |accessdate=2008-10-20 |date=December 2007 |work=NOVA |publisher=PBS}}</ref> He joined the USAF and trained as a test pilot, graduating from the [[U.S. Air Force Test Pilot School]] in class 63A and MOL.<ref name=TPS50>{{cite book |last= |first= |title=USAF Test Pilot School 50 Years and Beyond |publisher=Privately Published |year=1994 |page=250}}</ref> In 1965, he was selected as one of the first astronauts to the Air Force's classified Manned Orbital Laboratory.<ref name=astrospies_profile>{{cite web |url=http://www.pbs.org/wgbh/nova/astrospies/profiles.html |title=Secret Astronauts |accessdate=2008-10-11 |date=December 2007 |work=NOVA |publisher=PBS}}</ref> The MOL program, canceled in 1969 before sending any astronauts into space, was to man a military space station with Air Force astronauts using a modified [[Project Gemini|Gemini spacecraft]].<ref name=Nutter>{{cite web |url=http://www.nasa.gov/vision/space/features/found_mol_spacesuits.html |title=Suits for Space Spies |accessdate=2008-10-10 |last=Nutter |first=Ashley |date=2008-06-02 |publisher=[[NASA]]}}</ref> The history of the MOL program was presented in the Public Television series NOVA episode called ''Astrospies'' which first aired February 12, 2008.<ref name=astrospies>{{cite web |url=http://www.pbs.org/wgbh/nova/astrospies/ |title=Astrospies |accessdate=2008-10-10 |date=December 2007 |work=NOVA |publisher=PBS}}</ref>

After the MOL program cancellation, Taylor continued his USAF career as an instructor at the Test Pilot School and served as deputy commandant.<ref name=TPS50_p224>{{cite book |last= |first= |title=USAF Test Pilot School 50 Years and Beyond |publisher=Privately Published |year=1994 |page=224}}</ref> On September 4, 1970, he and French air force exchange test pilot trainee, Pierre J. du Bucq, were killed when their [[T-38 Talon|T-38]] aircraft crashed during a training mission at [[LA/Palmdale Regional Airport|Palmdale Regional Airport]].<ref name=Marrett_p197>{{cite book |last=Marrett |first=George J. |authorlink=George J. Marrett |title=Contrails Over the Mojave: The Golden Age of Jet Flight Testing at Edwards Air Force Base |publisher=Naval Institute Press |year=2008 |page=197 |isbn=1591145112 |url=https://books.google.com/books?id=2k9hHwAACAAJ&dq=Contrails+Over+the+Mojave |accessdate=2008-10-24}}</ref> The crash was caused by severe wake turbulence from a [[C-141 Starlifter|C-141]] that was performing touch-and-goes on an intersecting runway.  In memory of Taylor, the Test Pilot School presented the James M. Taylor Award to the outstanding graduate of the Experimental Test Pilot Course (Phase 1). The award was discontinued after class 71B when the school's curriculum was revised to eliminate the Phase I and II designation.<ref name=TPS50_p224 />

Attended by his fellow MOL astronauts, Taylor was buried at [[McChord Air Force Base]] in [[Pierce County, Washington]].<ref name=astrospies_taylor /> He is survived by his wife, Jacquelyn, and three children.<ref name=Marrett_p197 />

==References==
{{Reflist}}

==External links==
{{Portal|United States Air Force}}
* {{cite web |url=http://www.spacefacts.de/bios/astronauts/english/taylor_james.htm |title=James Taylor biography |publisher=Spacefacts |accessdate=2008-10-20}}
* {{cite web |url=http://www.astronautix.com/astros/taylor.htm |title=James Taylor biography |publisher=Astronautix |accessdate=2008-10-23}}
* {{cite web |url=http://www.aerofiles.com/awards.html |title=Taylor Award |work=Aviation Awards & Honoraria |publisher=Aero Files |accessdate=2008-10-23}}
* {{cite web |url=http://www.msnbc.msn.com/id/6863460/ |title=Some casualties don’t show up on NASA’s list |work=Cosmic Log |publisher=MSNBC |date=2005-01-27 |accessdate=2008-10-23}}

{{DEFAULTSORT:Taylor, James M.}}
[[Category:1930 births]]
[[Category:1970 deaths]]
[[Category:American astronauts]]
[[Category:University of Michigan alumni]]
[[Category:United States Air Force officers]]
[[Category:U.S. Air Force Test Pilot School alumni]]
[[Category:American test pilots]]
[[Category:People from Stamps, Arkansas]]