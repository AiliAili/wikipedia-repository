{{Use dmy dates|date=October 2012}}
{{infobox book <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Moondyne
| image         = <!--prefer 1st edition-->
| author        = [[John Boyle O'Reilly]]
| country       = United States
| language      = English
| genre         = [[Novel]]
| publisher     = [[P. J. Kennedy]]
| release_date  = 1879
| media_type    = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
}}
{{wikisource|Moondyne}}
'''''Moondyne''''' is an 1879 novel by [[John Boyle O'Reilly]]. It is loosely based on the life of the [[Western Australia]]n [[Convict era of Western Australia|convict]] escapee and [[bushranger]] [[Moondyne Joe]].

O'Reilly was a [[Irish Republican Brotherhood|Fenian]] revolutionary who was [[penal transportation|transported]] as a convict to Western Australia. During his time in Western Australia's penal system he would have heard many stories of Moondyne Joe's exploits, although it almost certain that the two men never met.  After thirteen months in Western Australia, O'Reilly escaped the colony on board the American whaling ship ''Gazelle''. He arrived in America in 1869 and settled in [[Boston, Massachusetts|Boston]], where he established himself as a respected journalist, newspaper editor, novelist and poet.

In 1913, film director [[W. J. Lincoln]] made a [[silent film]] of the same name.

== Publication details ==

The novel ''Moondyne'' originally appeared as a serial in O'Reilly's newspaper ''[[The Pilot (newspaper)|The Pilot]]'' in 1878, under the title ''Moondyne Joe''. Applauded by critics, it was published and republished under a variety of titles including:
* ''Moondyne Joe: A Story from the Underworld''<ref>{{cite web|title=Moondyne Joe: A Story from the Underworld|url=https://books.google.com/books?id=lSeRJyPFyEEC&lpg=PP1&ots=IMJ184TK-b&dq=Moondyne%20Joe%3A%20A%20Story%20from%20the%20Underworld&pg=PR5#v=onepage&q=Moondyne%20Joe:%20A%20Story%20from%20the%20Underworld&f=false|work=Google Books (partial preview only)|accessdate=2 June 2014}}</ref>
* ''Moondyne: A Tale of Convict Life in Western Australia''<ref>{{cite web|url=https://archive.org/details/moondynestory00oreirich|title=Moondyne; a story of convict life in Australia (ca. 1909?)|work=archive.org|accessdate=2 June 2014}}</ref>
* ''Moondyne: A Story of Life in West Australia''
* ''Moondyne: An Australian Tale''
* ''Moondyne, or, The Mystery of Mr Wyville''
* ''The Golden Secret, or Bond and Free''
* ''The Moondyne''
* ''A Tale of Bush and Convict Life''
* ''An Múindín'' (Irish-language translation, 1931).

== Plot summary ==

Moondyne Joe is a convict who escapes after being victimised and mistreated by a cruel penal system. While on the run he is befriended by a tribe of [[Indigenous Australians|aborigines]] who share with him their secret of a huge gold mine. Joe uses his new-found wealth to return to England and become a respected humanitarian under the assumed name Wyville. Recognised as possessing expertise in penal reform, he is ultimately sent back to Western Australia to help reform the colony's penal system. In the course of this he becomes involved in several subplots including the case of a young woman named Alice Walmsley who has been wrongly convicted of murdering her own child. Wyville/Moondyne succeeds in saving Alice from false imprisonment, helps to reform Western Australia's penal system, and achieves a number of other admirable ends before dying trying to save Alice and Sheridan from bushfires.

==1913 film==
{{Infobox film
| name           = Moondyne
| image          = Moondyne_Poster.jpg
| caption        = Advertisement for film
| director       = [[W. J. Lincoln]]
| producer       =
| writer         = [[John Boyle O'Reilly]]<br>W.J. Lincoln<ref>[http://recordsearch.naa.gov.au/SearchNRetrieve/Interface/DetailsReports/ItemDetail.aspx?Barcode=202253 Copyright documentation relating to script] at [[National Archives of Australia]]</ref>
| based on = novel by John Boyle O'Reilly
| starring       = [[George Bryant (actor)|George Bryant]]<br>[[Godfrey Cass]]<br>[[Roy Redgrave]]
| cinematography = [[Maurice Bertel]]
| editing        =
| studio         = [[Lincoln-Cass Films]]
| released       = {{Film date|1913|09|01|ref1=<ref>Mary Bateman, 'Lincoln Cass Filmography', ''Cinema Papers'', June–July 1980 p 175</ref><ref>{{cite news |url=http://nla.gov.au/nla.news-article7252366 |title=ENTERTAINMENTS. |newspaper=[[The Argus (Australia)|The Argus (Melbourne, Vic. : 1848 - 1956)]] |location=Melbourne, Vic. |date=6 September 1913 |accessdate=14 April 2012 |page=20 |publisher=National Library of Australia}}</ref>|df=y}}
| runtime        = 3,000 feet<ref>{{cite news |url=http://nla.gov.au/nla.news-article69674384 |title=Advertising. |newspaper=[[Williamstown Chronicle |Williamstown Chronicle (Vic. : 1856 - 1954)]] |location=Vic. |date=4 October 1913 |accessdate=3 February 2012 |page=5 |publisher=National Library of Australia}}</ref>
| rating         =
| country        = Australia
| awards         =
| language       = Silent film<br>English intertitles
| budget         =
}}
In 1913, the [[Melbourne]]-based Lincoln-Cass Film Company produced ''Moondyne'', a [[black-and-white|black and white]] [[silent film]] based on O'Reilly's novel.<ref>{{cite web|url=http://www.austlit.edu.au/austlit/page/6190781|title=''Moondyne'' (film)]|work=[[AustLit]]|accessdate=2 June 2014}}</ref><ref>{{cite web|url=http://www.imdb.com/title/tt0193331/|title=Moondyne (1913)|work=Internet Movie Database (IMDb)|accessdate=9 June 2006}}</ref> It is considered a [[lost film]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article69674411 |title=Advertising. |newspaper=[[Williamstown Chronicle |Williamstown Chronicle (Vic. : 1856 - 1954)]] |location=Vic. |date=4 October 1913 |accessdate=3 February 2012 |page=6 |publisher=National Library of Australia}}</ref>

===Plot===
In 1848, convict Joe is assigned as a labourer to settled Isaac Bowman in Western Australia. Joe escapes and takes refuge with a tribe of aborigines led by Te Mana Roa, who tell him about a mountain of gold.

Bowman recaptures Joe, who tells him about the mine. Bowman goes to the mine, kills the chief and loads his horse with gold, but ends up perishing in the desert, leaving Joe with his aboriginal friends.<ref>{{cite news |url=http://nla.gov.au/nla.news-article69674385 |title="MOONDYNE". |newspaper=[[Williamstown Chronicle |Williamstown Chronicle (Vic. : 1856 - 1954)]] |location=Vic. |date=4 October 1913 |accessdate=14 April 2012 |page=4 |publisher=National Library of Australia}}</ref>

===Cast===
*[[George Bryant (actor)|George Bryant]] as Joe Moondyne; 
*[[Roy Redgrave]] as Isaac Bowman
*[[Godfrey Cass]] as Te Mana Roa.

===Production===
The film was shot in and around Melbourne.

==Reception==
According to one review:
<blockquote>The true story of Joe Gilchrist, though poetised in the drama to some extent, affords scope for much dramatic feeling and scenic display, and the many views of Australian scenery are delightful. The acting is vigorous and full of character, and the photographic work sharp and clear. Generally, it may be said that 'Moondyne' is quite up to American and European standard, and should command much attention in other parts of the world.<ref>{{cite news |url=http://nla.gov.au/nla.news-article165141432 |title=Royal Pictures. |newspaper=[[Prahran Telegraph|The Prahran Telegraph (Vic. : 1889 - 1930)]] |location=Vic. |date=6 September 1913 |accessdate=7 July 2015 |page=5 |publisher=National Library of Australia}}</ref></blockquote>

==See also==
{{portal|Novels}}
*[[Moondyne (disambiguation)]]

==References==
{{reflist}}

==Further reading==
*{{cite journal|last=Ashton|first=Susanna M.|title=O'Reilly and the ''Moondyne''|journal=History Ireland|volume=10|issue=1|pages=38–42|date=Spring 2002}}
*Since 2002, [http://www.ucc.ie/celt/published/E850005-001/index.html an edited and annotated edition of ''Moondyne''] has been available online through [http://www.ucc.ie/en/ University College Cork's] [http://celt.ucc.ie/index.html Corpus of Electronic Texts] (CELT). See especially the Preamble.

==External links==
*[http://www.austlit.edu.au/austlit/page/6190781 ''Moondyne''] at [[AustLit]]
{{W.J. Lincoln}}
{{Convicts in Australia}}

[[Category:1879 novels]]
[[Category:1913 films]]
[[Category:Australian films]]
[[Category:19th-century Australian novels]]
[[Category:Australian black-and-white films]]
[[Category:Novels set in Western Australia]]
[[Category:Australian silent films]]
[[Category:Australian Western (genre) films]]
[[Category:Bushranger films]]
[[Category:Lost films]]