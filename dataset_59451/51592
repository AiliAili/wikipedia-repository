{{Use mdy dates|date=June 2013}}
{{refimprove|date=February 2012}}
{{Infobox organization 
|name = International Communication Association
|image = Lhheader.gif
|caption = ICA logo
|membership = 4,700
|headquarters = Washington, D.C, United States 
|formation = January 1, 1950 
|website = {{URL|http://www.icahdq.org/}}
}}
The '''International Communication Association''' ('''ICA''') is an academic association for scholars interested in the study, teaching and application of all aspects of human and mediated communication.<ref>{{cite web |url=http://www.icahdq.org/about_ica/index.asp |title=About ICA |publisher=International Communication Association |work=www.icahdq.org |accessdate=2012-02-09}}</ref> It began more than 60 years ago as a small organization of U.S. researchers and now is truly international with more than 4500 members in over 85 countries. The Association maintains an active membership of more than 4,500 individuals in 85 countries, approximately two-thirds of whom are academic scholars, professors, and graduate students. Other members are in government, the media, communication technology, business law, medicine and other professions.<ref>{{cite web |url=http://www-bcf.usc.edu/~ica/ |title=International Communication Association Online |publisher=[[University of Southern California]] |work=www-bcf.usc.edu |accessdate=2012-02-09}}</ref> ICA also has 59 institutional members (i.e. university departments or related institutions)and a growing number (16) of association members (i.e. communication associations around the world).

ICA is headquartered in Washington, D.C, United States. Since 2003, ICA has been officially associated with the United Nations as a non-governmental organization (NGO).

==History==
ICA was founded on January 1, 1950 in [[Austin, Texas]] as the National Society for the Study of Communication (NSSC), a subsidiary organization of the Speech Association of America (now the (United States) [[National Communication Association]]), when the SSA determined to exclude basic communication studies from its primary focus on rhetoric. The following year, NSSC published the first issue of its official journal, ''Journal of Communication''.

In 1967, the NSSC formally separated from the SSA and opened its membership to scholars outside of the U.S.  It changed its name two years later to the International Communication Association. The association restructured into divisions to allow members to find and interact with colleagues who shared similar research interests, among the widely diverse disciplines of communication study.

In the face of increasing internationalization, ICA relocated its offices from Austin, TX to the more cosmopolitan and globally accessible Washington, D.C. in 2001. The association purchased a permanent headquarters in Washington in 2006.

==Governance==
<!--commenting out deleted image [[Image:ICA headquarters DC.JPG|thumb|right|200px|ICA's office building in Washington, D.C.]]-->
ICA is governed by a Board of Directors, consisting of:

* the Executive Committee – President, Presidents-Elect and -Elect Select, the three immediate past Presidents – the least recent of whom serves as Finance Chair – and the Executive Director.
* Five Board Members-at-Large, representing five international regions as defined by [[UNESCO]]: the Americas; Europe; East Asia; West & South Asia; and Africa-Oceania.
* Two graduate student representatives.
* The chairs of ICA's Divisions and Interest Groups (although Interest Group representatives do not have voting rights).

The officers (except for the [[Executive Director]]) are elected by the association's members. The balloting for President and regional Members-at-Large is association-wide; student representatives are elected by ICA's student members, while Division and Interest Group chairs are chosen by the Division/Interest Group members. Terms of office begin and end at the conclusion of each year's annual ICA conference.

The president of ICA serves a one-year term; however, the Executive Committee's makeup of incoming and past presidents means that election as ICA president brings with it a six-year commitment to serve on the Executive Committee. The current (2014–2015) president is Amy Jordan ([[University of Pennsylvania]]), the current President-Elect is Peng Hwa Ang ([[Nanyang Technological University]]), and the current President-Elect Select is Paula Gardner ([[McMaster University]]). Past presidents include [[Peter Vorderer]] ([[University of Mannheim]]), Francois Heinderyckx ([[Université Libre de Bruxelles]]) and Cynthia Stohl  ([[University of California, Santa Barbara]]) (2012-2013).

ICA maintains a series of standing and awards committees to conduct its business in relation to membership, internationalization, publications, liaison with other agencies, etc., annual awards, and ad hoc task forces. Committee members are appointed by the president.

The association is administered by the six-member staff at ICA's offices in Washington, D.C. The staff is led by the Executive Director, a position held since 2000 by Michael L. Haley.

==Divisions and Interest Groups==
ICA's members are drawn from several disciplines, focuses, and approaches to the communication discipline. Its membership is thus organized into Divisions and Interest Groups that gather scholars and students of common research interests into formal structures within the association.

New "Interest Groups" are formed when  a group of 30 or more active members of the association may petition the Executive Director to establish an Interest Group. An interest group appoints its own officers, is allocated conference sessions, and is represented in the Board of Directors, but does not have voting rights.

An interest group enrolling at least 1 percent of the members of ICA for at least two consecutive years may apply to become a "Division". A division also appoints its own offices, and is allocated conference sessions, but unlike Interest Groups, Divisions are granted voting status on the Board of Directors.

If Interest Groups or Divisions lose members, their status may be revoked.{{citation needed|date=January 2015}}

As of 2015 there are 22 Divisions and 5 Interest Groups within the association.{{citation needed|date=January 2015}}

==Conference==
ICA sponsors a large annual conference for its members to present research representing the latest advances in the field of communication. The conference is held in a different city each year, with every fourth year's conference located outside of North America.

Each year's conference has a theme, around which the conference planners organize discussion panels designed to integrate and promote discovery, learning, and engagement as panelists and audience members from different regions of the world take up the conversation on particular topics. The conference also includes annual [[plenary session]]s featuring panels or keynote speakers. One of the plenaries is the Interactive Paper Session, in which participants display their work in the form of visual poster presentations and are awarded based on both the merit of their research and the visual quality of their presentations.

In addition, each of ICA's Divisions and Interest Groups organizes presentations and panels that explore the advancements within those Divisions' research focuses. These sessions often examine the conference theme from the aspects of the Divisions/Interest Groups.

ICA's President-Elect is head of the planning committee for the conference immediately following his/her election, and acts as the chair of that conference.

==Publications==
The International Communication Association's publishing partner is [[Wiley-Blackwell]].

===Journals===
ICA currently publishes five scholarly journals:

* ''[[Journal of Communication]]'', ICA's flagship and most prestigious publication, is a general forum for communication scholarship and publishes articles and book reviews examining a broad range of issues in communication theory and research. The journal is open to all methods of scholarly inquiry into communication.
* ''[[Human Communication Research]]'' concentrates on presenting empirical work in any area of human communication. The journal's has a broad social science focus with a strong emphasis on theory-driven research, the development of new theoretical models in communication, and the development of innovative methods for observing and measuring communication behavior.
* ''[[Communication Theory (Journal)|Communication Theory]]'' publishes research articles, theoretical essays, and reviews on topics of broad theoretical interest from across the range of communication studies. Essays, regardless of topic or methodological approach, must make a significant contribution to communication theory. No single approach or set of approaches is privileged.
* ''[[Communication, Culture & Critique]]'' publishes critical, interpretive, and qualitative research examining the role of communication and cultural criticism. Sites for enquiry include all kinds of text- and print-based media, as well as broadcast, still and moving images and electronic modes of communication including the internet and mobile telephony.
* ''[[Journal of Computer-Mediated Communication]]'', an online-only journal, publishes scholarship on computer-mediated communication. Broadly interdisciplinary in scope, the JCMC publishes mostly empirical research making use of social science methods, which should be presented according to the accepted standards for each method.

===Book series===
* ''Communication in the Public Interest'', a series that explores the communication field's contributions to and potential impact on issues that have real world effects and implications, or that affect people's lives.
* ''[[Communication Yearbook]]'', an annual publication featuring state-of-the-discipline literature reviews of communication research. Each chapter includes a comprehensive examination of research on a communication topic and articulates the importance of that literature for a global community of diverse communication scholars and stakeholders.
* ''The Encyclopedia of Communication'', an A-Z, multi-volume work that serves as a reference source for important ideas, people, and publications in the academic field of communication.
* ''Handbooks of Communication'', a series of summaries of current communication scholarship that consider content areas in communication research, methodological approaches to communication research, and theoretical lenses for scholarship in communication.
* ''Theme Sessions Series'', an annual selection of papers from the theme sessions of that year's ICA Annual Conference.

===Newsletter===
ICA publishes the web-based monthly ''ICA Newsletter'' for its members, providing news and updates on the business of the association as well as advertisements for available job positions, calls for papers, and other content related to the discipline in general and ICA in specific.

==See also==
*[[Communication studies]]
*[[Association for Education in Journalism and Mass Communication]]
*[[Central States Communication Association]]
*[[National Communication Association]]
*[[European Communication Research and Education Association]]
*[[Center for Intercultural Dialogue]]

==References==
{{reflist}}

==External links==
*[http://www.icahdq.org International Communication Association]
*[http://environmentalcomm.org/ International Environmental Communication Association]

{{Authority control}}

[[Category:Communications and media organizations]]
[[Category:Organizations established in 1950]]
[[Category:Professional associations based in the United States]]
[[Category:International educational organizations]]
[[Category:1950 establishments in Texas]]
[[Category:Organizations based in Washington, D.C.]]