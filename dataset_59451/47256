{{Infobox church
| name             = Covenanter Church
|image             = Covenanter Church, Grand Pre, Nova Sctia.jpg|thumb
|caption           = Covenanter Church est. 1808, Grand Pre, Nova Scotia, Canada
| location         = [[Grand-Pré, Nova Scotia]]
| founder          = Gillmore, George (Rev.)
| architect        = Gillmore, George (Rev.)
| style            = New England Meeting House
{{Designation list |embed=yes |designation1=NHSC |designation1_offname=Covenanters' Church National Historic Site of Canada
 |designation1_date=1976 | designation2 = Nova Scotia | designation2_type = Provincially Registered Property | designation2_number =00PNS0073 | designation2_date =1988 }}
}}

'''Covenanter's Church''' is a New England [[meeting house]] style structure located in [[Grand Pré, Nova Scotia]], and is the oldest extant [[Presbyterianism|Presbyterian Church]] in [[Canada]]. The meeting house was constructed between 1804 and 1811, with the tower, belfry, and steeple being added in 1818.<ref name="historicplaces">{{CRHP|7325|Covenanters Church|23 October 2012}}</ref>

The meeting house was preceded by a small log church which was demolished in 1795 in anticipation of a larger construction. Under the leadership of Rev. George Gillmore, a graduate of the [[University of Edinburgh]], a new church based on plain, meeting house lines was begun in 1804.<ref>{{cite web|url=http://www.htns.ca/quarterly/articles/church.html |title=Heritage Trust of Nova Scotia |publisher=Htns.ca |date= |accessdate=2012-10-22}}</ref>

In 1833 the Rev. Sommerville established a school and introduced a stricter regime; only allowing psalms to be sung as hymns and conducting prolonged services. The congregation was segregated during this time as well, with men sitting on one side of the church and women on the other. Sommerville and his successor, the Reverend Thomas McFall, were ordained pastors of the [[Reformed Presbyterian Church of Ireland]]. Members of this church called themselves [[Covenanter|'Covenanters']], as successors to those dissenters from the [[Church of Scotland]]. From the time this church became identified with this body, and other Presbyterian churches were built in the community, it has been referred to as the Covenanter Church.<ref name="historicplaces" />

From 1894 until its purchase by the [[Presbyterian Church in Canada|Presbyterian Church of Canada]] in 1912, the church was vacant. The church then joined the [[United Church of Canada]] in 1925. Today, as a member of the Wolfville United Church pastoral charge, the Covenanter Church is used for services only during the summer months.

Adjacent to the church is a small, non-active cemetery where many of the founders of the community, including the Rev. George Gillmore, are buried. Others buried here include Andrew and Eunice (Laird) Borden, parents of Canada's eighth Prime Minister, [[Robert Borden|Sir Robert Borden]].

The church offers Sunday Worship services during the months of July and August at 11:00&nbsp;a.m., and an annual Christmas Eve service on December 24 at 11 p.m.<ref>{{cite web|author=Bill Davenport |url=http://wolfvilleunitedchurch.ca/churches.html |title=Welcome to the United Church of Canada Wolfville Pastoral Charge |publisher=Wolfvilleunitedchurch.ca |date= |accessdate=2012-10-22}}</ref>

Covenanter's Church was designated a [[National Historic Sites of Canada|National Historic Site of Canada]] in 1976,<ref>{{CRHP|12343|Covenanters' Church National Historic Site of Canada|23 October 2012}}</ref> and was declared a Provincially Registered Historical Property in 1988.<ref name="historicplaces"/>

==References==
{{Reflist}}

{{NHSC}}
{{United Church of Canada}}

{{coord|45|05|59|N|64|18|18|W|region:CA_type:landmark_source:kolossus-frwiki|display=title}}

{{DEFAULTSORT:Covenanter Church, Grand Pre, Nova Scotia}}
[[Category:National Historic Sites in Nova Scotia]]
[[Category:Churches in Nova Scotia]]
[[Category:United Church of Canada churches]]
[[Category:Churches completed in 1811]]
[[Category:Buildings and structures in Kings County, Nova Scotia]]