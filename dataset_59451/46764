{{use American English|date=September 2016}}
{{use mdy dates|date=September 2016}}
{{infobox person
| name          = Jacqulyn Buglisi
| image         = Jacqulyn Buglisi.tiff
| image caption = Photo by Bill Biggart
| birth_name    =
| birth_date    = 20th century
| birth_place   = [[New York City]], [[New York (state)|New York]], United States
| alma_mater    =
| occupation    = [[Choreographer]], [[artistic director]], [[dancer]], [[educator]], and founder
| website       = <!--only for a personal website; organization in external links below-->
}}
'''Jacqulyn Buglisi''' is an American [[choreographer]], [[artistic director]], [[dancer]], [[educator]], and founder or co-founder of multiple dance institutions. Buglisi, with [[Terese Capucilli]], [[Christine Dakin]] and Donlin Foreman, founded Buglisi Dance Theatre in 1993/94 (formerly BuglisiForeman Dance).

== Choreography ==
Buglisi's ballets are highly visual and imagistic dances that draw from literature, history and heroic archetypes as primary sources. Buglisi's ballets are rooted in strong physical technique. Her repertoire of more than eighty works has been seen by audiences at venues across the United States including:
{{div col|colwidth=25em}}
* [[John F. Kennedy Center for the Performing Arts]]
* [[Joyce Theater]]
* [[Jacob's Pillow Dance Festival]]
* Society of the Performing Arts [[Wortham Theater Center]] – Cullen Theater, Houston
* Kravis Center for the Performing Arts
* Spring to Dance Festival, St. Louis
* SUNY Purchase Performing Arts Center – Concert Hall
* [[Richard B. Fisher Center for the Performing Arts]] – Sosnoff Theater
* [[University of Arizona]] – Centennial Hall
{{div col end}}

Internationally, her works have appeared at the Melbourne International Festival- State Theater; during International Dance Week in [[Prague]]; the [[Czech Republic]], [[Japan]], [[Italy]], [[France]] and [[Israel]].

Lynn Garafola of ''[[Dance Magazine]]'' wrote, "Buglisi is a rarity in today's world, a woman who delights in the many splendid forms of female being. Probably no woman, other than Graham, has plumbed such emotional depths choreographically"<ref>
  {{cite web
  | title = Duo Layers The Visual with the Emotional
  | work = [[Dance Magazine]]
  | date = May 2001
  | url = http://www.buglisi-foreman.org/dancemag050001.html
  | access-date =October 3, 2012
  }}
</ref> and for Buglisi's www.UndertheButtonwoodTree.com commissioned by LMCC, [[Alastair Macaulay]] of ''[[The New York Times]]'' wrote, "[[Cecil B. DeMille]] would have been proud".<ref>
  {{cite web
  | title = Grand Leaps on Wall Street, for a Change
  | work = [[The New York Times]]
  | date = May 30, 2008
  | url =https://www.nytimes.com/2008/05/30/arts/dance/30bugl.html?pagewanted=print&_r=0
  | access-date =October 3, 2012
  }}
</ref>

Her commissions include The Alvin Ailey American Dance Theater, Ailey 2, The LMCC River to River Festival, New York Flamenco Festival in Madrid; Sadler's Wells Theater London and [[New York City Center]]; The Richmond Ballet; pieces for Ananda Shankar Performing Arts Company of India; the Shanghai Song and Dance Ensemble of China; The Juilliard School, The Martha Graham Dance Company; North Carolina Dance Theater; Joyce Trisler Danscompany (charter member/choreographer) and the Teatro Danza Contemporanea di Roma (co-founder/choreographer).

Buglisi collaborated with Venezuelan environmental painter and filmmaker [[Jacobo Borges]] to create her trilogy, ''Rain'' (score by [[Glen Velez]]), ''Sand'' (music by [[Philip Glass]]), and ''Blue Cathedral'' (music by [[Jennifer Higdon]]. Other collaborations include composers [[Tan Dun]], Paola Prestini, Andy Teirstein, Daniel Bernard Roumain (DBR), Libby Larsen, Daniel Brewbaker, Reza Vali, cellist Maya Beiser, Flamenco guitarist Gerardo Nunex, the Cassatt String Quartet, Carmen de Lavallade, Claire Bloom, mannequin maker Ralph Pucci and lighting designers Clifton Taylor and Jack Mehler.<ref>
  {{cite web
  | title = 2004 Press
  | publisher = Buglisi Dance Theatre
  | year = 2002
  | url = http://www.buglisi-foreman.org/news_press/2007press.html
  | access-date =October 29, 2012
  }}
</ref>

'''The Table of Silence Project 9/11'''<ref>
  {{cite web
  | title = Invitation to the 'Table of Silence,' a 9/11 Commemoration
  | publisher = Thirteen New York Public Media
  | year = 2012
  | url = http://www.thirteen.org/metrofocus/2012/09/invitation-to-the-table-of-silence-a-911-commemoration/
  | access-date =November 6, 2012
  }}
</ref>

On September 11, 2011, 2012, 2013, 2014, 2015, Buglisi Dance Theatre presented 150+ dancers at Lincoln Center in ''The Table of Silence Project 9/11'', a public tribute and ritual honoring peace and tolerance, "a magnificent expression of peace". Live streaming around the globe reaching 102 countries and across all 50 states, www.tableofsilence.org.<ref>{{Cite web|title = tableofsilence|url = http://www.tableofsilence.org|website = tableofsilence|access-date = December 4, 2015}}</ref> Other ''Table of Silence'' Site-specific commemorations were presented at the Pan Am 103 Remembrance Wall during the company's NYSCA residency at [[Syracuse University]]; in Perugia and Assisi, Italy; and UC, Santa Barbara on May 23, 2016.

Her company's repertoire is archived for public viewing at the Jerome Robbins Dance Division of the [[New York Public Library]]. In recognition of her excellence as a choreographer, dancer, and educator in 2016 Buglisi was awarded a lifetime achievement award from Italian International Dance Award.

== Dancer ==
[[File:Frida JB 300dpi.jpg|thumbnail|Photo credit by [[Jack Mitchell (photographer)|Jack Mitchell]]]]
During her 30-year association with the [[Martha Graham Dance Company]], Buglisi danced as a principal artist for 12 years, performing the classic roles such as the Three Mary's (''[[El Penitente]]''), Andromache (''Cortege of Eagles''), the Warrior (''Seraphic Dialogue''), the Lament (''Acts of Light''), the Girl in Yellow (''Diversion of Angels''), Leader of the Night Journey Chorus and Jocasta (''Night Journey''), Creusa (''[[Cave of the Heart]]''), The Spectator (''[[Every Soul is a Circus]]'') and in ''Tangled Night'' created for her by [[Martha Graham]]. She danced in Graham's honor on the nationally televised CBS Presentation of the Kennedy Center Honors and is featured in the PBS film, ''An Evening of Dance and Conversation with Martha Graham''. Coached by [[Jane Sherman]], Buglisi performed the solos of [[Ruth St. Denis]] at Jacob's Pillow and internationally, including Lyon Biennale De La Dancse, on film in ''Trailblazers of American Modern Dance'', and also in the film, ''The Spirit of Denishawn''.<ref>
  {{cite web
  | title = Reviews/Dance; Denishawn Works Are Recreated
  | work = [[The New York Times]]
  | date = March 15, 1994
  | url =https://www.nytimes.com/1994/03/15/arts/reviews-dance-denishawn-works-are-recreated.html
  | access-date =October 30, 2012
  }}
</ref>

== Educator ==
Buglisi has been commissioned by The Juilliard School's Emerging Modern Masters Series, Ailey/Fordham University B.F.A. Program, the [[University of Richmond]], California State University/Long Beach, George Mason University, Purchase Conservatory of Dance, [[Interlochen Center for the Arts]], the State Ballet College of Oslo, Oklahoma Arts Institute, [[Jacob's Pillow Dance Festival]], [[Boston Conservatory of Music]], [[Randolph-Macon College]] and [[National Dance Institute]], among others.<ref>
  {{cite web
  | title = Jacqulyn Buglisi, Council Chair-Artistic Directors
  | publisher = Dance/USA
  | url =http://www.danceusa.org/jacqulynbuglisi
  | access-date =October 30, 2012
  }}
</ref>

In 1970, Buglisi founded the first school of contemporary dance for the community of [[Spoleto]], Italy, and was the Master Artist-in-Residence at the [[Atlantic Center for the Arts]].<ref>
  {{cite web
  | title = Master Artist-in-Residence Program History
  | publisher = Atlantic Center for the Arts
  | url = http://www.aca35.org/master-artist-residence-program-history-0
  | access-date =October 30, 2012
  }}
</ref> She has taught for the Dance Aspen Festival from 1990 to 1995, the [[Julio Bocca]] Center in Argentina, the [[Victoria College, Melbourne|Victoria College]] in Melbourne in 1999, and the Chautauqua Institution and Festival from 1995 to 2005.

As a resident teacher in New York City, she has been chairperson of the Modern Department at The Ailey School, has served on the faculty of [[The Juilliard School]] from 1991 to 2005, The Martha Graham School of Contemporary Dance since 1977, and Ballet Hispanico School of Dance. Buglisi guest teaches at the [[Fiorello H. LaGuardia High School|LaGuardia High School of the Performing Arts]] (alumna) and instructs workshops at Steps on Broadway and Peridance Capezio Center. She was named honorary chair for the [[Marymount Manhattan College]] 2005 Gala and served as a panelist for both the Heinz Family Foundation and the New Jersey State Council for the Arts. Buglisi holds creative and educational residencies at Kaatsbaan International Dance Center, Munson-Williams-Proctor Arts-Institute, SUNY Purchase, California State University, Long Beach, [[George Mason University]], [[University of Richmond]], The Mahayfee Theater Class Act in FSU at Tallahassee, Petersburg Florida, and [[Syracuse University]].<ref>
  {{cite web
  | title = Alvin Ailey American Dance Theater Teacher Bio
  | publisher = Alvin Ailey Dance Theater
  | url =http://www.alvinailey.org/mobile/bio.php?nid=1532
  | access-date =October 30, 2012
  }}
</ref>

Buglisi's awards and honors include the American Dance Guild Award for Artistic Excellence, Italian International Lifetime Achievement Award 2016, Fiorello LaGuardia Award for Excellence in the field of Dance, The 2014 Kaatsbaan International Playing Field Award, The Gertrude Shurr Award for Dance, Altria Group's 2007 Women Choreographer Initiative Award, as well as grants for new work from the [[National Endowment for the Arts]], [[New York State Council on the Arts]], NYC Department of Cultural Affairs, Harkness Foundation for Dance, Howard Gilman Foundation, and The O'Donnell-Green Music & Dance Foundation.<ref>
  {{cite web
  | title = Who's Who in the Company
  | publisher = Buglisi Dance Theatre
  | url = http://www.buglisi-foreman.org/index2.html
  | access-date =October 30, 2012
  }}
</ref>

Buglisi served on the Dance/USA's Board of Trustees as Chair of Artistic Director's Council from 2010 to 2013.<ref>
  {{cite web
  | title = Growing Through Collaboration: Finding Balance in a Shifting Landscape
  | publisher = Dance/USA
  | year = 2012
  | url = http://danceusa.org/ejournal/post.cfm?entry=growing-through-collaboration-finding-balance-in-a-shifting-landscape
  | access-date =November 6, 2012
  }}
</ref>

==See also==
{{portal|Biography|Dance}}
* [[List of choreographers]]
* [[List of people from New York City]]
{{clear}}

== References ==
{{reflist|30em}}

==External links==
* {{URL|buglisi-foreman.org}}, official website of the Buglisi Dance Theatre

{{DEFAULTSORT:Buglisi, Jacqulyn}}
[[Category:Date of birth missing (living people)]]
[[Category:20th-century births]]
[[Category:20th-century American dancers]]
[[Category:20th-century educators]]
<!--[[Category:21st-century dancers]] - added at September 2016, but hidden because category not yet created-->
[[Category:21st-century educators]]
[[Category:American women choreographers]]
[[Category:American choreographers]]
[[Category:American directors]]
[[Category:American female dancers]]
[[Category:American founders]]
[[Category:Artistic directors]]
[[Category:Educators from New York]]
[[Category:Entertainers from New York City]]
[[Category:Living people]]
[[Category:Women directors]]
[[Category:American women educators]]
[[Category:Women founders]]