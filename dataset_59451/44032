<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=185
 | image=Rotax185LazairEngine.png
 | caption=Rotax 185 with Lazair aircraft mount and muffler installed
}}{{Infobox Aircraft Engine
 |type=[[Piston]] [[aircraft engine|aero-engine]]
 |manufacturer=[[Rotax]]
 |national origin=[[Austria]]
 |first run=
 |major applications=[[Ultraflight Lazair]]
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}

The '''Rotax 185''' is a {{convert|9|hp|kW|0|abbr=on}}, single cylinder, [[two-stroke]], direct drive, industrial engine, built by [[Rotax]] of [[Austria]] for use in fire fighting water pumps that has also been adapted as an aircraft engine for use in [[ultralight aircraft]].<ref name="LazairForce1">{{cite web|url = http://mypage.direct.ca/h/highland/Lazair/engine/engine.html|title = Rotax 185 Piston de-carboning Bearings re & re|accessdate = 2009-03-11|last = LazairForce|authorlink = |year = n.d.}}</ref>

==Development==

The Rotax 185 was designed as a fire-fighting pump, but the rights to the design were sold by Rotax to the Wildfire Group who use the engine in their Mark 3 High Pressure Centrifugal Fire Pump.<ref name= "Mark3">{{cite web |url = http://www.wildfire-equipment.com/images/pdf/Mark-3_Op_manual.pdf  |title = Mark-3 High Pressure Centrifugal Fire Pump  |date = 2004 |author =Wildfire |accessdate = 2009-03-12 }}</ref>

Due to its design purpose as a water pump engine it proved reliable enough for use on ultralight aircraft,<ref name="Hunt1">Hunt, Adam & Ruth Merkis-Hunt: ''Skeletal Remains'',  pages 64-70. Kitplanes Magazine, September 2000.</ref><ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page E-21. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref> and was adopted as an engine for the twin-engine [[Ultraflight Lazair]] [[ultralight aircraft]] - as a replacement for the {{convert|5.5|hp|kW|0|abbr=on}} Pioneer chainsaw engines used on the early Series I. The 185 provides enough power to allow the Lazair to be flown on floats.

In the Lazair application the 185 was used to drive two propellers stacked together in [[biplane]] configuration. This was not done for aerodynamic reasons but rather because the Lazair manufacturer had ample quantities of the nylon propellers on hand for its earlier engines and stacking them was more cost efficient than scrapping them and buying new propellers.<ref name= "LazairForce">{{cite web |url = http://www.lazairforce.central5.com  |title = An introduction to the Lazair  |date = n.d. |author = Lazair Force |accessdate = 2007-10-31 }}</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->

==Applications==
[[File:Ultraflight Lazair C-IFYB 01.JPG|thumb|right|A Lazair with two Rotax 185s installed, powering biplane propellers]]
*[[Lazair]]
* Wildfire Mark-3 High Pressure Centrifugal Fire Pump

==Specifications (185) ==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=Rotax 185 Engine Data<ref name="LazairForce2">{{cite web|url = http://mypage.direct.ca/h/highland/Lazair/engine/data.html|title = Rotax 185 Engine Data|accessdate = 2009-03-11|last = LazairForce|authorlink = |year = n.d.}}</ref>
|type=two-stroke air-cooled engine
|bore=2.441 in (62 mm)
|stroke=2.4 in (61 mm)
|displacement=185 cc (11.29 cu in)
|length=
|diameter=
|width=
|height=
|weight=
|valvetrain=piston ports
|supercharger=
|turbocharger=
|fuelsystem=Tillotson pneumatic pump carburettor 
|fueltype=regular autofuel
|oilsystem=premixed in the fuel at 40:1
|coolingsystem=free air
|power=9 hp (7 kW) at 5000 rpm
|specpower=
|compression=
|fuelcon=4 l/hr (1 US gph)
|specfuelcon=
|oilcon=
|power/weight=

|designer=
|reduction_gear=Custom made belt drive reduction units have been employed

|general_other=
|components_other=
|performance_other=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists= 
}}

==References==
{{Reflist}}

==External links==
{{Commons category}}

{{Rotax aeroengines}}

[[Category:Air-cooled aircraft piston engines]]
[[Category:Rotax engines]]
[[Category:Two-stroke aircraft piston engines]]