<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->{|{{Infobox aircraft begin
 |name = A-36 Apache/Invader
 |image = File:A-36A training unit in Louisiana.jpg
 |caption = North American A-36A
}}{{Infobox aircraft type
 |type = Ground-attack/dive bomber
 |manufacturer =[[North American Aviation]]
 |designer = [[Edgar Schmued]]
 |first flight = October 1942
 |introduced = 1942
 |retired = 1945
 |number built = 500
 |status =
 |primary user =[[United States Army Air Forces]]
 |more users =Civilian air racer
 |unit cost = $49,000<ref name="National Museum of the United States Air Force">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=493 "North American A-36A Apache."] ''National Museum of the United States Air Force''. Retrieved: 13 June 2010.</ref>
 |developed from = [[P-51 Mustang]]
 |variants with their own articles =
}}
|}

The '''North American A-36 Apache''' (listed in some sources as '''"Invader"''', but also called '''Mustang''') was the ground-attack/[[dive bomber]] version of the [[North American P-51 Mustang]], from which it could be distinguished by the presence of rectangular, slatted [[dive brake]]s above and below the wings. A total of 500 A-36 dive bombers served in North Africa, the Mediterranean, Italy and the [[China Burma India Theater of World War II|China-Burma-India theater]] during [[World War II]] before being withdrawn from operational use in 1944.

==Design and development==
With the introduction of the North American Mustang I with the [[Royal Air Force|RAF]] [[RAF Army Cooperation Command|Army Co-operation Squadrons]] in February 1942, the new fighter began combat missions as a low-altitude reconnaissance and ground-support aircraft. Supplementing the [[Curtiss P-40 Warhawk|Curtiss P-40 Tomahawk]]s already in service, Mustang Is were first supplied to [[No. 26 Squadron RAF]], then rapidly deployed to 10 additional squadrons by June 1942. First used in combat over the [[Dieppe Raid]] on 19 August 1942, a Mustang of [[No. 414 Squadron RCAF|No. 414 (RCAF) Squadron]] downed one of the formidable [[Focke-Wulf Fw 190]]s, the first victory for a Mustang.<ref>Hess 1970, p. 5.</ref> Despite the limited high-altitude performance of the [[Allison V-1710]] engine, the RAF was enthusiastic about its new mount, which "performed magnificently".<ref>Hess 1970, p. 12.</ref>

[[File:RAF A-36A 1.png|thumb|left|Sole RAF A-36A ''EW998'', showing the slatted dive brakes and carrying American 500lb (226kg) bombs on the racks beneath the wings]]

During the Mustang I's successful combat initiation, [[North American Aviation|North American]]'s president [[James H. Kindelberger|Howard "Dutch" Kindelberger]] pressed the newly redesignated [[U.S. Army Air Forces]] (USAAF) for a fighter contract for the essentially similar P-51, 93 of which had passed into the USAAF when the [[Lend-Lease]] contract with [[United Kingdom|Britain]] ran out of funds. The Mustang IA/P-51 used four [[Hispano-Suiza HS.404|20 mm Hispano]] wing cannon in place of the original armament, a combination of four wing-mounted .[[.30-06 Springfield|30]] caliber (7.62&nbsp;mm) [[M1919 Browning machine gun]]s and four [[.50 BMG|.50 caliber]] (12.7&nbsp;mm) [[M2 Browning machine gun]]s, two of which were mounted in the wings, while the second pair was mounted in the "chin", or lower [[Cowling|engine cowling]], and [[Synchronization gear|synchronized]] to fire through the propeller. No funds were available for new fighter contracts in fiscal year 1942, but General [[Oliver P. Echols]] and Fighter Project Officer [[Benjamin S. Kelsey]]<ref name=GML3>[http://www.gml2007.com/history.shtml "P-51 History: Mustang I."] ''The Gathering of Mustangs & Legends'', 26 March 2009. Retrieved: 13 June 2010.</ref> wanted to ensure that the P-51 remained in production.<ref>Mizrahi 1995, pp. 49–50.</ref>

Since appropriations were available for an attack aircraft, Echols specified modifications to the P-51 to turn it into a dive bomber. The contract for 500 A-36A aircraft fitted with bomb racks, dive brakes, and heavier-duty wing, was signed by Kelsey on 16 April 1942,<ref name= "Gruenhagen p. 61."/> even before the first flight of the first production P-51 in May 1942.<ref>Mizrahi 1995, p. 49.</ref> With orders on the books, North American Aviation (NAA) began modifying the P-51 to accept the bomb shackles which had already been tested in a "long-range ferry" program that the RAF had stipulated.<ref>Grunehagen 1969, p. 60.</ref> Engineering studies totaling 40,000 hours and wind tunnel testing with a ⅛-scale model were completed in June 1942. Utilizing the basic P-51 airframe and Allison engine, structural reinforcing "beefed up" several high stress areas and "a set of hydraulically operated dive brakes were installed in each main wing plane".<ref name= "Grinsell, p. 60.">Grinsell 1984, p. 60.</ref> Due to the slightly inboard placement of the bomb racks and unique installation of four cast aluminum dive brakes, a complete redesign of the P-51 wing was required.<ref name="Gruenhagen p. 61.">Gruenhagen 1969, p. 61.</ref>
[[File:A-36A production Inglewood Oct 1942.jpg|thumb|A-36A production line at NAA Inglewood, October 1942.]]

The first A-36A (''42-83663'') was rolled out of the NAA Inglewood plant in September 1942, rapidly going through flight testing with the first flight in October, with deliveries commencing soon after of the first production machines. The A-36A continued the use of nose-mounted .50&nbsp;in (12.7&nbsp;mm) machine guns along with wing armament of four .50&nbsp;in (12.7&nbsp;mm) caliber machine guns. The USAAF envisaged that the dive bomber would operate mainly at altitudes below 12,000&nbsp;ft (3,658&nbsp;m) and specified the use of a sea level-rated Allison V-1710-87, driving a 10&nbsp;ft 9&nbsp;in (3.28&nbsp;m)-diameter three bladed Curtiss-Electric propeller and delivering 1,325&nbsp;hp (988&nbsp;kW) at 3,000&nbsp;ft (914&nbsp;m).<ref>Kinzey 1996, p. 22.</ref> The main air scoop inlet was redesigned to become a fixed unit with a larger opening, replacing the earlier scoop which could be lowered into the airstream. In addition the A-36 carburetor air intake was later fitted with a tropical air filter to stop sand and grit being ingested into the engine.<ref name= "Gruenhagen p. 61.">Gruenhagen 1969, p. 61.</ref><ref>Gruenhagan 1969, pp. 42, 62, 66, 178.</ref>

The USAAF later ordered 310 P-51As, which were essentially A-36s without the dive-brakes and nose mounted weapons, leaving an armament of four wing-mounted 0.50&nbsp;in (12.7&nbsp;mm) Browning machine guns.<ref name="Kin9641">Kinzey 1996, p. 41.</ref> An Allison V-1710-81 1,200&nbsp;[[horsepower|hp]] (895&nbsp;[[kilowatt|kW]]) was fitted and used the same radiator and air intake as the A-36A. The P-51A was still fitted with bomb racks although it was not intended to be used primarily as a fighter-bomber and the racks were mainly used to carry drop tanks.<ref name="Kin9641"/><ref>Taylor 1969, p. 537.</ref>

==Operational history==
[[File:86fg-a36-italy-1944.jpg|thumb|A-36A of the [[86th Bombardment Group|86th Fighter Bomber Group]] (Dive) in Italy in 1944.]]

The A-36A-1-NA "Apache" (although Apache was the A-36A's official name, it was rarely used)<ref name= "Gunston and Dorr p. 68.">Gunston and Dorr 1995, p. 68.</ref> joined the [[27th Special Operations Group|27th Fighter-Bomber Group]] (27th FBG) composed of four squadrons based at [[Ras el Ma Airfield]] in French Morocco in April 1943 during the campaign in [[Operation Torch|North Africa]].<ref name= "Gunston and Dorr p. 68."/> The 27th had a mixed component of [[Douglas A-20 Havoc]]s and A-36As while the second operational unit, the [[86th Bombardment Group|86th Fighter Bomber Group]] (Dive) arrived in March 1943 with the first pilots trained and qualified on the A-36A.<ref name="Freeman p. 45."/><ref>[http://www.86fighterbombergroup.com/ "WW II History of 86 FG."] ''86fighterbombergroup.com''. Retrieved: 24 June 2008.</ref> On 6 June 1943, both of these A-36A units flew combat missions directed against the island of [[Pantelleria]]. The island fell to Allied attack and became the home base for the two A-36A groups during the [[Allied invasion of Sicily]]. The A-36A proved to be a potent weapon; it could be put into a vertical dive at 12,000&nbsp;ft (3,658&nbsp;m) with deployed dive brakes, thus limiting the dive speed to 390&nbsp;mph (628&nbsp;km/h) ("A36A-1 Flight Manual requires deployment before starting a dive"). Pilots soon recognized that extending the dive brakes after "peel-off" led to some unequal extension of the brakes due to varying hydraulic pressure, setting up an invariable slight roll, which impeded aiming. Proper technique soon cured this anomaly and, subsequently, pilots achieved extremely consistent results.<ref name= "Gruenhagen p. 61."/> Depending on the target and defenses, the bomb release took place between 2,000&nbsp;ft and 4,000&nbsp;ft (610 and 1,219&nbsp;m), followed by an immediate sharp "pull up."<ref name= "Grinsell, p. 60."/>

Dive brakes in the wings gave the A-36A greater stability in a dive; however, a myth has arisen that they were useless due to malfunctions or because of the danger of deploying them and that they should be wired closed.<ref>Hess 1970, p. 13.</ref> Capt. Charles E. Dills, [[522d Fighter Squadron]], 27th FBG, XIIth Air Force emphatically stated in a postwar interview: "I flew the A-36 for 39 of my 94 missions, from 11/43 to 3/44. They were never wired shut in Italy in combat. This 'wired shut' story apparently came from the training group at [[Baton Rouge Metropolitan Airport|Harding Field]], Baton Rouge, LA."
[[File:A-36 "Apache" of the 27th Fighter Bomber Group.jpg|thumb|right|A-36 of the 86th Bombardment Group (Dive), "284067" coded A, lost to flak, 14 January 1944.<ref>[http://www.webcitation.org/query?url=http://www.geocities.com/raf_112_sqdn1/86th_fg.html&date=2009-10-26+01:22:06 "86th Fighter-Bomber Group."] ''geocities.com.'' Retrieved: 24 June 2008.</ref>]]

However, tactical reconnaissance training with P-51 and A-36 aircraft had delivered some disquieting accident rates. At one time, A-36 training had resulted in the type having "the highest accident rate per hour's flying time"<ref>Freeman 1974, pp. 44–45.</ref> of any USAAF aircraft. The most serious incident involved an A-36A shedding both wings when its pilot tried to pull out from a 450&nbsp;mph (724&nbsp;km/h) dive.<ref name= "Freeman p. 45.">{{harvnb|Freeman|1974|p=45.}}</ref> Combat units flying the A-36A were ordered to restrict their approach to a 70° "glide" attack and refrain from using dive brakes.<ref name= "Grinsell p. 69.">Grinsell 1984, p. 69.</ref> This order was generally ignored by experienced pilots, but some units did wire dive brakes shut until modifications were made to the hydraulic actuators.<ref name= "Grinsell p. 69."/> Nevertheless, the A-36 was used with great success as a dive-bomber, acquiring a reputation for precision, sturdiness and silence.<ref name= "Hess p. 14.">Hess 1970, p. 14.</ref>

By late May 1943, 300 A-36As had been deployed to the Mediterranean Theater, with many of the first batch sent to the 27th FBG to re-build the group following losses as well as completing the final transition to an all-A-36A unit.<ref name= "Freeman p. 45."/> Both groups were actively involved in air support during the Sicilian campaign, becoming especially adept at "mopping" up enemy gun positions and other strong points as the Allies advanced. During this operation, the 27th FBG circulated a petition to adopt the name "Invader" for their rugged little bomber, receiving unofficial recognition of the more fitting name.<ref name= "Freeman p. 45."/> Despite the name change, most combat reports preferred the name "Mustang" for all of the variants.<ref>Gruenhagen 1969, p. 62.</ref> The Germans gave it a flattering, if fearsome, accolade, calling the A-36As: "screaming helldivers."<ref name= "Hess p. 14."/>

Besides dive bombing, the A-36A racked up aerial victories, totaling 84 enemy aircraft downed and creating an "ace", Lieutenant Michael T. Russo from the 27th FBG (ultimately, the only ace using the Allison-engined Mustang).<ref name= "Gunston and Dorr p. 68."/> As fighting intensified in all theaters where the A-36A operated, the dive bomber began to suffer an alarming loss rate with 177 falling to enemy action.<ref name= "Gunston and Dorr p. 68."/> The main reason for the attrition was the hazardous missions that placed the A-36A "on the deck" facing murderous ground fire. German defenses in southern Italy included placing cables across hill tops to snare the attacking A-36As.<ref name="Gruenhagen p. 63.">Gruenhagen 1969, p. 63.</ref> Despite establishing a reputation for reliability and performance, the one "Achilles' heel" of the A-36A (and the entire Mustang series) remained its ventral-fuselage location radiator/cooling system, leading to many of the losses.<ref>Hess 1970, p. 18.</ref> By June 1944, A-36As in Europe were replaced by [[Curtiss P-40]]s and Republic [[P-47 Thunderbolt]]s.<ref name= "Gruenhagen p. 61.">Gruenhagen 1969, p. 61.</ref>

A-36As also served with the [[311th Fighter Bomber Group]] in the [[China Burma India Theater of World War II|China-Burma-India theater]]. The 311th had arrived in [[Dinjan]], India by late summer 1943 after being shipped across the Pacific via Australia.<ref>Spick 1997, p. 225.</ref> Two squadrons were equipped with the A-36A while the third flew P-51As. Tasked with reconnaissance, dive bombing, attack and fighter missions, the A-36A was outclassed by its main opposition, the [[Nakajima Ki-43]] "Oscar." The light and highly agile Japanese fighter could outmaneuver the A-36A at all altitudes but did have some weak points: it was lightly armed and offered little protection for pilot or fuel tanks. However, the A-36A fought at a significant disadvantage, having to carry out long-range missions often at altitudes above [[The Hump]] that meant its Allison engine was below peak performance. In a fighter escort mission over Burma, three A-36As were lost without scoring a single victory. The A-36A CBI missions continued throughout 1943–1944 with indifferent results. The A-36A remained in service in small numbers throughout the remaining year of the war, some being retained in the US as training aircraft.

"The type's relatively brief service life should not camouflage the fact that it made a major contribution to the Allied war effort"<ref name= "Gunston and Dorr p. 68."/> especially in the Mediterranean and it amounted to the first USAAF combat use of a Mustang variant. The effectiveness of the A-36 as a ground attack aircraft was demonstrated on 5 June 1944. In a well planned attack on the large, well defended rail depot and ammo dump at [[Orte]], Italy, Lieutenant Ross C. Watson led a flight of four A-36s through a heavy overcast on the approach to the target. Watson's A-36s scored several hits under intense anti-aircraft fire although his aircraft was damaged by ground fire. Under continuing heavy ground fire, Watson pressed home his attack and destroyed the ammo dump before making an emergency landing at an advanced Allied airfield.<ref name="Gruenhagen p. 63."/>

==Operators==
[[File:RAF A-36A 2.png|thumb|Frontal view of the RAF's A-36A ''EW998'', showing that this aircraft did not have the nose-mounted .50 cal Brownings.]]
* {{USA}}
** [[US Army Air Force]]
: [[27th Fighter-Bomber Group]]
: [[86th Fighter-Bomber Group]]
: [[311th Fighter Bomber Group]]

* {{UK}}
** [[RAF]]
: One A-36A was supplied to the RAF in March 1943 for experimental purposes. Its RAF [[United Kingdom military aircraft serials|serial number]] was ''EW998''.

==Survivors==
[[File:North American A-36A Apache USAF.jpg|thumb|A-36A "Margie H" at the [[National Museum of the United States Air Force]], in the scheme of the A-36A flown by [[Captain (land)|Captain]] Lawrence Dye of the 16th Fighter-Bomber Squadron in Tunisia, Sicily and Italy.<ref name="National Museum of the United States Air Force"/>]]
Relatively few A-36As survived the war and the subsequent postwar retirement and scrapping of obsolete types. One A-36A, bearing race number #44, owned and flown by Kendall Everson, was entered in the 1947 Kendall Trophy Race. The old warhorse was able to reach 377.926&nbsp;mph, finishing second to the winning P-51D flown by Steve Beville.<ref>Kinnert 1969, p. 100.</ref>
;Airworthy
;;A-36A
* 42-83731 - Comanche Warbirds Inc. in [[Houston, Texas]]<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=251A "FAA Registry: N251A."] ''FAA.gov'' Retrieved: 23 May 2011.</ref>
* 42-83738 ''Baby Carmen'' - [[Collings Foundation]] in [[Stow, Massachusetts]]<ref>[http://www.collingsfoundation.org/rest_a-36apache.htm "A-36A Apache/42-83738."] ''Collings Foundation.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=4607V "FAA Registry: N4607V."] ''FAA.gov'' Retrieved: 23 May 2011.</ref>

;On display
;;A-36A
* 42-83665 ''Margie H'' - [[National Museum of the United States Air Force]] at [[Wright-Patterson AFB]] in [[Dayton, Ohio]].<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=493 " A-36A Apache/42-83665."] ''National Museum of the United States Air Force.'' Retrieved: 9 April 2012.</ref>{{#tag:ref|In 1971, Charles P. Doyle of [[Rosemount, Minnesota]] obtained ''42-83665'', subsequently restored by the 148th Fighter-Interceptor Group of the [[Minnesota Air National Guard]], under the command of [[Colonel (United States)|Colonel]] Wayne C. Gatlin.<ref name="National Museum of the United States Air Force"/>|group=N}}

==Specifications (A-36A)==
{{aircraft specifications

<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=''National Museum of the United States Air Force''<ref name="National Museum of the United States Air Force"/>
|crew=1
|capacity=
|length main= 32 ft 3 in
|length alt=9.83 m
|span main= 37 ft 0.25 in
|span alt=11.28 m
|height main= 12 ft 2 in
|height alt=3.71 m
|area main=
|area alt=
|airfoil=
|empty weight main=
|empty weight alt=
|loaded weight main= 10,000 lb
|loaded weight alt=4,535 kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[Allison V-1710]]-87
|type of prop= liquid-cooled piston [[V12 engine]]
|number of props= 1
|power main= 1,325 hp
|power alt= 988 kW
|power original=
|max speed main= 365 mph
|max speed alt= 315 kn, 590 km/h
|cruise speed main= 250 mph
|cruise speed alt= 215 kn, 400 km/h
|range main= 550 mi
|range alt= 478 nmi, 885 km
|ceiling main= 25,100ft
|ceiling alt= 7,650 m
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
* 6 × [[0.50 BMG|0.50 in]] (12.7 mm) [[M2 Browning machine gun]]s
* Up to 1,000 lb (454 kg) of bombs on two underwing hardpoints
|avionics=
}}

==See also==
{{Portal|United States Air Force|Aviation}}
{{aircontent|
|related=
* [[P-51 Mustang]]
|similar aircraft=
|lists=
* [[List of aircraft of World War II]]
* [[List of military aircraft of the United States]]
|see also=
}}

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Delve, Ken. ''The Mustang Story''. London: Cassell & Co., 1999. ISBN 1-85409-259-6.
* {{cite book|ref={{harvid|Freeman|1974}}
|last=Freeman|first=Roger A|title=Mustang at War|location=New York|year= |publisher=Doubleday and Company, Inc., 1974|ISBN=0-385-06644-9}}
* Grinsell, Robert. "P-51 Mustang". ''Great Book of World War II Airplanes''. New York: Wing & Anchor Press, 1984. ISBN 0-517-45993-0.
* Gunston, Bill and Robert F. Dorr. "North American P-51 Mustang: The Fighter that Won the War." ''Wings of Fame Vol. 1''. London: Aerospace Publishing Ltd., 1995. ISBN 1-874023-68-9.
* Gruenhagen, Robert W. ''Mustang: The Story of the P-51 Mustang''. New York: Arco Publishing Company, Inc., 1969. ISBN 0-668-03912-4.
* Hess, William N. ''Fighting Mustang: The Chronicle of the P-51''. New York: Doubleday and Company, 1970. ISBN 0-912173-04-1.
* Kinnert, Reed. ''Racing Planes and Air Races: A Complete History, Volume IV: 1946–1967''. Fallbrook, California: Aero Publishers, Inc., 1969 (revised ed.) ISBN 0-8168-7853-6.
* Kinzey, Bert. ''P-51 Mustang in Detail & Scale: Part 1; Prototype through P-51C''. Carrollton, Texas: Detail & Scale Inc., 1996. ISBN 1-888974-02-8
* Mizrahi, Joe. "Pursuit Plane 51." ''Airpower'', Vol. 25, no. 5, September 1995, pp.&nbsp;5–53.
* Smith, Peter C. ''Straight Down! The North American A-36 Dive Bomber in Action''. North Branch, Minnesota: Specialty Press, 2000. ISBN 0-947554-73-4
* Spick, Mike. "The North American P-51 Mustang." ''Great Aircraft of WWII''. Leicester, UK: Abbeydale Press, 1997. ISBN 1-86147-001-0.
* Taylor, John W.R. "North American P-51 Mustang." ''Combat Aircraft of the World from 1909 to the present''. New York: G.P. Putnam's Sons, 1969. ISBN 0-425-03633-2.
* ''United States Air Force Museum Guidebook''. Dayton, Ohio: Air Force Museum Foundation, Wright-Patterson AFB, 1975.
{{Refend}}

==External links==
{{Commons category|A-36 Apache}}
* [http://www.mustang.gaetanmarie.com/index.htm Mustang!]
* [http://www.86fighterbombergroup.com/ WW II History of the 86th Fighter Group]

{{North American Aviation aircraft}}
{{USAF attack aircraft}}

[[Category:United States attack aircraft 1940–1949|North American A-36]]
[[Category:World War II ground attack aircraft of the United States]]
[[Category:World War II dive bombers]]
[[Category:North American Aviation aircraft|A-36]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:North American P-51 Mustang]]