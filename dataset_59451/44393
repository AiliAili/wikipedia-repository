{{Use dmy dates|date=May 2014}}
{{Infobox airport
| name = Varna Airport
| nativename = 
| nativename-a = Летище Варна
| nativename-r = 
| image = VarnaAirport_logo.png
| image-width = 120
| image2 = VAR_Terminal2.jpg
| image2-width = 250
| IATA = VAR
| ICAO = LBWN
| type = Public
| owner = 
| operator = [[Fraport]] Twin Star Airport Management
| city-served = [[Varna]], [[Bulgaria]]
| location = [[Varna]], [[Bulgaria]]
| hub = {{columns-list|1|
*[[Air VIA]]
*[[BH Air]]
*[[Bulgaria Air]]
*[[Bulgarian Air Charter]]
*[[Wizz Air]] (from 21 July 2017)<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518</ref>
}}
| metric-elev = yes
| elevation-m = 70
| elevation-f = 230
| coordinates = {{coord|43|13|55|N|27|49|31|E|display=inline,title}}
| website = {{URL|http://www.varna-airport.com}}
| pushpin_map= Bulgaria
| pushpin_label=VAR
| pushpin_mark=Airplane_silhouette.svg
| pushpin_mapsize=200
| pushpin_map_caption = Location of airport in Bulgaria
| metric-rwy = yes
| r1-number = 09/27
| r1-length-m = 2,517
| r1-length-f = 8,258
| r1-surface = [[Asphalt]]
| stat-year = 2016
| stat1-header = Passengers
| stat1-data   = 1,689,595 {{increase}}
| stat2-header = Aircraft movements
| stat2-data   = 14,818 {{increase}}
| footnotes    = Source: Bulgarian [[Aeronautical Information Publication|AIP]] at [[European Organisation for the Safety of Air Navigation|EUROCONTROL]]
}}

'''Varna Airport''' ([[Bulgarian language|Bulgarian]]: Летище Варна, ''Letishte Varna'') {{airport codes|VAR|LBWN}} is the airport of [[Varna]], the historical maritime capital of [[Bulgaria]]. Varna Airport is the third largest airport in [[Bulgaria]] and an important destination during the summer leisure seasons. The airport is located 10 kilometers from the center of Varna near the town of [[Aksakovo]]. The airport serves Varna and northeastern Bulgaria. There are domestic and international flights to about 71 destinations in 29 countries by more than 53 Bulgarian and foreign airlines. The busiest season for the airport is from the end of May to the beginning of October.

==History==
{{unreferenced section|date=February 2015}}
The history of the airport dates back to 1916 when two sheds for the first hydro-port in [[Bulgaria]] were built in the Peinerdzhik area (present-day ''Chaika'' residential area). Irregular mail-plane service from [[Sofia]] to [[Varna]] was held between 1919 and 1920 and it was not until 1947 that a permanent airline between the two cities was established. What had grown into '''Tihina Airport''' was situated west of the present-day [[Asparuhov most|Asparuhov bridge]] and was indeed quite primitive for the demands of a modern city. Thus in 1946 a decision was made and a new airport was constructed several kilometres west of the city, near the village (now town) of [[Aksakovo]], with local people enthusiastically working on the site together with the constructors. Construction and improvement continued throughout the years, with a new terminal built in 1972 and a new runway in 1974.

In 2013 a new passenger terminal opened and the one constructed in the 20th century was closed down.

There are domestic and international flights form Varna to about 70 destinations in 25 countries, with Bulgarian and foreign airlines. The airport is close to the [[Port of Varna]] and the railway system. The airport has one asphalt-covered runway 09/27 with ILS CAT I system on 09 edge and a parking apron for 24 aircraft.

[[File:VAR Traffic.jpg|left|thumb|Apron view]]
Currently Varna Airport is subject to heavy traffic with the growing tourism industry in Bulgaria and is in need of major investments to modernize, expand and handle projected passenger traffic. In June 2006 the Bulgarian Government awarded ''[[Fraport]] AG Frankfurt Airport Services Worldwide'' a 35-year-long [[concession (contract)|concession]] on both Varna and [[Burgas Airport|Burgas]] airports in return for investments exceeding €500 million, including a new passenger terminal by 2008.

From 15 October 2011 until 28 February 2012, Varna airport was closed for a reconstruction of the runway. All flights were operated to/from [[Burgas Airport]].<ref>{{cite web|url=http://www.novinite.com/view_news.php?id=130853|title=Varna Airport Closes for Runway Overhaul in October|publisher=|accessdate=7 June 2015}}</ref>

In 2016 the airport handled 1,689,595 passengers - a 20.8% increase compared to 2015.

==Terminals==
[[File:VAR RWY.jpg|thumb|The runway]]
[[File:VarT2-18.JPG|thumb|Terminal 2 gate area]]
The airport has three terminals: Terminal 1, built in 1972 (closed), and Terminal 3 (opened in June 2007), which was used during the summer season and the new Terminal 2, opened in August 2013.

===Terminal 1===
Terminal 1 /closed/ has been extended several times over the years. The departures area had 21 check-in counters and six security checkpoints. In the terminal there were various outlets: cafes, fast food restaurants, currency exchange, and [[duty-free shop]]s. There were ten boarding gates. The arrivals area had two luggage belts, as well as currency exchange and a tax-free shop. Terminal 1 still has a VIP room and business lounge. In 2010 the VIP room at Varna Airport was renovated. As of 2014, all flights, including no-frills, are managed by Terminal 2. Hence, the old terminal is closed except the VIP area.

===Terminal 2===
Terminal 2 started operations as of 18 August 2013. In December 2011 the construction work on new Terminal 2 began. T2 has a capacity of 1,800,000 passengers per year and 25 check-in desks. It covers an area of 18,000 m2. T2 buildings are designed so that their capacity could be further increased as an extension to the existing architectural part. The first passenger service on T2 was a domestic flight to [[Sofia]] operated by the national carrier [[Bulgaria Air]] and first international passengers served were on a [[Belavia]] flight from [[Minsk]]. T2 was designed by London-based architecture firm Pascall+Watson. Passengers can enjoy a large variety of retail outlets including a Bulgarian souvenir shop. Popular chains such as McDonald's, [[Costa Coffee]] and beer outlet are available at the gate area right after check-in.

==Airlines and destinations==

===Scheduled flights===
<!-- -->
<!-- --->
<!-- The information is corrected and verified on Varna airport website and timetable. The list will be updated every month !!!! Please do not edit charter list or undo last update !!! All information is correct !!!! Last updated is for summer season 2017 --> 
<!-- -->
<!-- -->
<!-- DO NOT ADD BEGIN/END/RESUME DATES FOR SEASONAL FLIGHTS UNLESS IT IS A NEW FLIGHT OR IS ENDING PERMANENTLY -->
<!-- -->
<!-- -->

{{Airport-dest-list
| 3rdcoltitle=Terminal
<!-- -->
| [[Aeroflot]]<br>{{nowrap|operated by [[Rossiya Airlines]]}}| '''Seasonal:''' [[Pulkovo Airport|Saint Petersburg]] | 2
<!-- -->
| [[Austrian Airlines]]| [[Vienna International Airport|Vienna]] | 2
<!-- -->
| [[BH Air]]| '''Seasonal:''' [[Glasgow Airport|Glasgow]], [[London-Gatwick]], [[Manchester Airport|Manchester]], [[Zürich Airport|Zürich]] | 2
<!--BH Air flights can be booked on the official website of the airline-->
| [[Bulgaria Air]] | [[Burgas Airport|Burgas]] (resumes 11 April 2017), [[Sofia Airport|Sofia]]<br>'''Seasonal:''' [[Moscow-Sheremetyevo]], [[Pulkovo Airport|Saint Petersburg]], [[Ben Gurion Airport|Tel Aviv-Ben Gurion]] | 2
<!-- -->
| [[Condor Flugdienst|Condor]]| '''Seasonal:''' [[Düsseldorf Airport|Düsseldorf]] (begins 30 May 2017),<ref>https://www.condor.com/eu/index.jsp</ref> [[Frankfurt Airport|Frankfurt]] (begins 4 July 2017)<ref>https://www.condor.com/eu/index.jsp</ref> | 2
<!--The flights from DUS and FRA can be booked on official website of Condor-->
| [[Condor Flugdienst|Condor]]<br>{{nowrap|operated by [[Brussels Airlines]]}}| '''Seasonal:''' [[Brussels Airport|Brussels]] (begins 6 May 2017)<ref>https://www.condor.com/eu/index.jsp</ref> | 2
<!-- -->
| [[easyJet]]| '''Seasonal:''' [[Berlin-Schönefeld]] (begins 28 June 2017), [[London-Gatwick]] (begins 2 June 2017)<ref>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/270379/easyjet-s17-new-routes-as-of-13dec16/|title=EasyJet S17 new routes as of 13DEC16|publisher=routesonline|accessdate=13 December 2016}}</ref>| 2
<!-- -->
| [[Edelweiss Air]]| '''Seasonal:''' [[Zürich Airport|Zürich]] | 2
<!-- -->
| [[Eurowings]]| '''Seasonal:''' [[Düsseldorf Airport|Düsseldorf]] | 2
<!-- -->
| [[Eurowings]] <br>{{nowrap|operated by [[Germanwings]]}}| '''Seasonal:''' [[Cologne Bonn Airport|Cologne/Bonn]] (begins 18 July 2017)<ref> https://www.eurowings.com/en.html</ref> | 2
<!-- -->
| [[Finnair]]| '''Seasonal:''' [[Helsinki Airport|Helsinki]] | 2
<!-- -->
| [[Germania (airline)|Germania]]|'''Seasonal:''' [[Bremen Airport|Bremen]], [[Friedrichshafen Airport|Friedrichshafen]], [[Hamburg Airport|Hamburg]] (begins 24 May 2017),<ref>https://www.flygermania.com/en/</ref>  [[Münster Osnabrück International Airport|Münster/Osnabrück]], [[Rostock–Laage Airport|Rostock]] | 2
<!-- -->
| [[Germania Flug]]| '''Seasonal:''' [[Zürich Airport|Zürich]] | 2
<!-- -->
| [[Israir Airlines]]| '''Seasonal:''' [[Ben Gurion Airport|Tel Aviv-Ben Gurion]] | 2
<!-- -->
| [[Luxair]]| '''Seasonal:''' [[Luxembourg – Findel Airport|Luxembourg]] | 2
<!-- -->
| [[Norwegian Air Shuttle]]| '''Seasonal:''' [[Copenhagen Airport|Copenhagen]] (begins 24 June 2017),<ref>http://www.norwegian.com/en/booking/flight-tickets/low-fare-calendar/</ref> [[Helsinki Airport|Helsinki]] (begins 1 July 2017),<ref>{{cite web|url=http://lentoposti.fi/uutiset/norwegian_avaa_reitin_helsinki_vantaalta_varnaan_ja_lisaa_lentoja_osloon_ja_koopenhaminaan/|title=Norwegian adds new Varna service for S17 and more flights to Copenhagen and Oslo|publisher=lentoposti|accessdate=13 February 2017}}</ref> [[Oslo-Gardermoen]] | 2
<!-- -->
| [[Ryanair]]| [[Brussels South Charleroi Airport|Charleroi]] (begins 29 October 2017)<ref>https://money.bg/investments/ryanair-zapochva-poleti-i-ot-varna.html</ref> | 2
<!-- -->
| [[S7 Airlines]]| [[Moscow-Domodedovo]] | 2
<!-- -->
| [[Small Planet Airlines (Poland)|Small Planet Airlines Poland]]| '''Seasonal:''' [[Katowice International Airport|Katowice]] (begins 23 May 2017), [[Warsaw-Chopin]] (begins 23 May 2017)<ref>[https://www.smallplanet.aero/en/book-your-ticket/1209?flight_from=WAW&flight_to=BCN&coupon= Small Planet Poland open reservation for summer seasonal flights from summer schule 2017]</ref> | 2
<!-- -->
|[[SmartWings]]<br>{{nowrap|operated by [[Travel Service Airlines|Travel Service]]}} |'''Seasonal:''' [[Brno–Turany Airport|Brno]], [[Leoš Janáček Airport Ostrava|Ostrava]] (begins 13 June 2017),<ref>[[Prague Václav Havel Airport|Prague]]</ref> [[Václav Havel Airport Prague|Prague]] | 2
<!-- -->
| [[SunExpress Deutschland]]| '''Seasonal:''' [[Cologne Bonn Airport|Cologne/Bonn]], [[Düsseldorf Airport|Düsseldorf]], [[Frankfurt Airport|Frankfurt]], [[Hamburg Airport|Hamburg]], [[Hannover Airport|Hannover]], [[Leipzig Halle Airport|Leipzig/Halle]], [[Munich Airport|Munich]], [[Nuremberg Airport|Nuremberg]], [[Stuttgart Airport|Stuttgart]] | 2
<!-- -->
| [[Thomas Cook Airlines]]| '''Seasonal:''' [[Manchester Airport|Manchester]], [[Newcastle Airport|Newcastle upon Tyne]] | 2
<!-- -->
| [[TUI fly Belgium]]| '''Seasonal:''' [[Brussels Airport|Brussels]], [[Ostend–Bruges International Airport|Ostend/Bruges]] | 2
<!-- -->
| [[Turkish Airlines]]| [[Istanbul-Atatürk]] | 2
<!-- -->
| [[Wizz Air]]| [[Il Caravaggio International Airport|Bergamo]] (begins 22 July 2017),<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518#tab1</ref> [[Dortmund Airport|Dortmund]] (begins 21 July 2017),<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518#tab1</ref> [[Eindhoven Airport|Eindhoven]] (begins 23 July 2017),<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518#tab1</ref> [[Larnaca International Airport|Larnaca]] (begins 23 July 2017),<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518#tab1</ref> [[London-Luton]], [[Memmingen Airport|Memmingen]] (begins 22 July 2017),<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen518#tab1</ref>  [[Sofia Airport|Sofia]], [[Ben Gurion Airport|Tel Aviv-Ben Gurion]] (begins 21 July 2017)<ref>https://book.wizzair.com/en-GB/about_us/news/wizzen527</ref>| 2
<!-- -->
}}

===Charter flights===
<!-- -->
<!-- The information is corrected and verified on Varna airport website and timetable. The list will be updated every month --> 
<!-- -->
<!-- DO NOT ADD BEGIN/END/RESUME DATES FOR SEASONAL FLIGHTS UNLESS IT IS A NEW FLIGHT OR IS ENDING PERMANENTLY -->
<!-- -->
{{Airport-dest-list
| 3rdcoltitle=Terminal
<!-- -->
| [[Aeroflot]]<br>{{nowrap|operated by [[Rossiya Airlines]]}}| '''Seasonal:''' [[Moscow-Vnukovo]] | 2
<!-- -->
| [[Anda Air]]| '''Seasonal:''' [[Kyiv International Airport (Zhuliany)|Kiev-Zhuliany]] (begins 9 June 2017)<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[ASL Airlines France]]| '''Seasonal:''' [[Nantes Atlantique Airport|Nantes]] (begins 12 May 2017)<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[Austrian Airlines]]| '''Seasonal:''' [[Vienna International Airport|Vienna]] (begins 3 June 2017) | 2
<!-- -->
<!--Seasonal charter flight from 3 June 2017 to 2 September 2017 in days 4 and 6-->
<!-- -->
| [[Azur Air]]| '''Seasonal:''' [[Moscow-Domodedovo]] | 2
<!-- -->
| [[Belavia]]| '''Seasonal:''' [[Minsk National Airport|Minsk-National]] | 2
<!-- -->
| [[BH Air]]| '''Seasonal:''' [[Billund Airport|Billund]] (begins 26 June 2017), [[Copenhagen Airport|Copenhagen]] (begins 4 June 2017) | 2
<!-- -->
| [[BRA (airline)|BRA]]| '''Seasonal:''' [[Aalborg Airport|Aalborg]], [[Billund Airport|Billund]], [[Odense Airport|Odense]] | 2
<!-- -->
| [[Bulgaria Air|Bul Air]]| '''Seasonal:''' [[Kuwait International Airport|Kuwait]] (begins 30 June 2017), [[Tehran-Imam Khomeini]], [[Ben Gurion Airport|Tel Aviv-Ben Gurion]] | 2
<!-- -->
| [[Bulgaria Air]]| '''Seasonal:''' [[Beirut–Rafic Hariri International Airport|Beirut]], [[Katowice International Airport|Katowice]] (begins 23 June 2017), [[Paris-Charles de Gaulle]] (begins 20 May 2017), [[Zvartnots International Airport|Yerevan]] | 2
<!-- -->
| [[Bulgarian Air Charter]]| '''Seasonal:''' [[Beirut–Rafic Hariri International Airport|Beirut]], [[Berlin-Schonefeld]], [[Berlin-Tegel]], [[Cologne Bonn Airport|Cologne/Bonn]], [[Dresden Airport|Dresden]] (resumes 28 May 2017), [[Düsseldorf Airport|Düsseldorf]], [[Erfurt–Weimar Airport|Erfurt/Weimar]] (resumes 23 June 2017), [[Frankfurt Airport|Frankfurt]], [[Hamburg Airport|Hamburg]], [[Hannover Airport|Hannover]], [[Katowice International Airport|Katowice]], [[Leipzig/Halle Airport|Leipzig/Halle]], [[Munich Airport|Munich]], [[Munster Osnabruck International Airport|Munster/Osnabruck]], [[Nuremberg Airport|Nuremberg]], [[Paderborn Lippstadt Airport|Paderborn/Lippstadt]] (begins 22 June 2017), [[Stuttgart Airport|Stuttgart]], [[Ben Gurion Airport|Tel Aviv-Ben Gurion]], [[Vienna International Airport|Vienna]], [[Warsaw-Chopin]], [[Copernicus Airport Wrocław|Wroclaw]] | 2
<!-- -->
| [[DART Ukrainian Airlines]]| '''Seasonal:''' [[Kyiv International Airport (Zhuliany)|Kiev-Zhuliany]] (resumes 2 June 2017)<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[Enter Air]]| '''Seasonal:''' [[Gdańsk Lech Wałęsa Airport|Gdańsk]], [[Katowice International Airport|Katowice]], [[John Paul II International Airport Kraków–Balice|Kraków]], [[Poznań–Ławica Airport|Poznań]], [[Warsaw-Chopin]], [[Copernicus Airport Wrocław|Wroclaw]] | 2
<!-- -->
| [[Eurowings]] <br>{{nowrap|operated by [[Germanwings]]}}| '''Seasonal:''' [[Cologne Bonn Airport|Cologne/Bonn]] (begins 18 July 2017), [[Stuttgart Airport|Sttutgart]] | 2
<!-- -->
| [[Germania (airline)|Germania]]| '''Seasonal:''' [[Düsseldorf Airport|Düsseldorf]] | 2
<!-- -->
| [[NordStar]]| '''Seasonal:''' [[Moscow-Domodedovo]], [[Pulkovo Airport|Saint Petersburg]] | 2
<!-- -->
| [[Nordwind Airlines]]| '''Seasonal:''' [[Moscow-Sheremetyevo]], [[Pulkovo Airport|Saint Petersburg]] (begins 12 June 2017)<ref>[http://nordwindairlines.ru/en/flights/mapview Nordwind Airlines destinations map]</ref> | 2
<!-- -->
| [[Pegas Fly]]| '''Seasonal:''' [[Moscow-Sheremetyevo]] (begins 19 May 2017)<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[Primera Air]]| '''Seasonal:''' [[Billund Airport|Billund]], [[Copenhagen Airport|Copenhagen]] | 2
<!-- -->
| [[S7 Airlines]]| '''Seasonal:''' [[Krasnodar International Airport|Krasnodar]] (begins 3 July 2017) | 2
<!-- -->
| [[Small Planet Airlines]]| '''Seasonal:''' [[Kaunas Airport|Kaunas]] (begins 14 July 2017), [[Keflavík International Airport|Reykjavík–Keflavík]] (begins 21 July 2017), [[Vilnius International Airport|Vilnius]] | 2
<!-- -->
| [[Small Planet Airlines (Germany)|Small Planet Germany]]| '''Seasonal:''' [[Paderborn Lippstadt Airport|Paderborn/Lippstadt]] (begins 28 June 2017)<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[Small Planet Airlines (Poland)|Small Planet Poland]]| '''Seasonal:''' [[Katowice International Airport|Katowice]], [[Warsaw-Chopin]], [[Copernicus Airport Wrocław|Wroclaw]] (begins 16 June 2017) | 2
<!-- -->
| [[Smartlynx Airlines Estonia]]| '''Seasonal:''' [[Tallinn Airport|Tallinn]] | 2
<!-- -->
| [[Thomas Cook Airlines Belgium]]| '''Seasonal:''' [[Brussels Airport|Brussels]] | 2
<!-- -->
| {{nowrap|[[Thomas Cook Airlines Scandinavia]]}}| '''Seasonal:''' [[Bergen Airport, Flesland|Bergen]] (begins 19 June 2017), [[Bodø Airport|Bodø]], [[Copenhagen Airport|Copenhagen]], [[Göteborg Landvetter Airport|Gothenburg-Landvetter]], [[Helsinki Airport|Helsinki]], [[Oslo-Gardermoen]], [[Stavanger Airport, Sola|Stavanger]], [[Stockholm-Arlanda]], [[Trondheim Airport, Værnes|Trondheim]] | 2
<!-- -->
| [[Transavia France]]| '''Seasonal:''' [[Paris-Orly]] | 2
<!-- -->
| [[Travel Service Airlines|Travel Service]]| '''Seasonal:''' [[Lille Airport|Lille]], [[Prague Ruzyne Airport|Prague]]<ref name="Varna Airport_Schedule">{{cite web|title=Varna Airport Schedule|url=http://www.varna-airport.bg/Flight/tabid/185/language/en-US/Default.aspx|work=Information and Services|publisher=Varna Airport|accessdate=20 February 2017}}</ref> | 2
<!-- -->
| [[Travel Service Polska|Travel Service Poland]]| '''Seasonal:''' [[Katowice International Airport|Katowice]], [[Warsaw-Chopin]] | 2
<!-- -->
| [[TUI fly Netherlands]]| '''Seasonal:''' [[Amsterdam Airport Schiphol|Amsterdam]] (begins 25 May 2017)<ref>http://www.luchtvaartnieuws.nl/nieuws/categorie/tuifly-in-zomer-naar-porto-santo-en-varna</ref> | 2
<!-- -->
| [[VIM Airlines]]| '''Seasonal:''' [[Moscow-Domodedovo]] (begins 29 May 2017), [[Pulkovo Airport|Saint Petersburg]]<ref>[http://www.vim-avia.com/en/reservation_and_schedule/filght_map/ VIM Airlines destinations map]</ref> | 2
<!-- -->
}}

==Statistics==
<center>
{| class="toccolours" border="1" cellpadding="4" style="border-collapse:collapse"
|+ '''Varna Airport Passenger Traffic Statistics'''
|- bgcolor=lightgrey
! Year
! Domestic
! Change
! International
! Change
! Total
! Change
|-
| 1998
| 35,208
|
| 238,470
|
| 273,678
|
|-
| 1999
| 57,955
|{{increase}} 64.6%
| 453,864
|{{increase}} 90.3%
| 511,819
|{{increase}} 87%
|-
| 2000
| 67,508
|{{increase}} 16.5%
| 624,181
|{{increase}} 37.5%
| 691,689
|{{increase}} 35.1%
|-
| 2001
| 48,121
|{{decrease}} 28.7%
| 884,428
|{{increase}} 41.7%
| 932,549
|{{increase}} 34.8%
|-
| 2002
| 45,457
|{{decrease}} 5.5%
| 1,045,252
|{{increase}} 12.1%
| 1,090,709
|{{increase}} 16.9%
|-
| 2003
| 41,583
|{{decrease}} 8.5%
| 1,144,766
|{{increase}} 9.5%
| 1,186,349
|{{increase}} 8.8%
|-
| 2004
| 47,575
|{{increase}} 14.4%
| 1,271,552
|{{increase}} 11.1%
| 1,319,127
|{{increase}} 11.2%
|-
| 2005
| 49,705
|{{increase}} 4.5%
| 1,496,175
|{{increase}} 17.7%
| 1,546,925
|{{increase}} 17.3%
|-
| 2006
| 54,243
|{{increase}} 9.1%
| 1,468,415
|{{decrease}} 1.8%
| 1,522,658
|{{decrease}} 1.6%
|-
| 2007
| 79,058
|{{increase}} 45.7%
| 1,399,035
|{{decrease}} 4.7%
| 1,478,093
|{{decrease}} 2.9%
|-
| 2008
| 119,459
|{{increase}} 51.1%
| 1,313,244
|{{decrease}} 6.1%
| 1,432,703
|{{decrease}} 3.1%
|-
| 2009
| 155,734
|{{increase}} 30.4%
| 1,050,801
|{{decrease}} 20%
| 1,206,535
|{{decrease}} 15.8%
|-
| 2010
| 154,974
|{{decrease}} 0.5%
| 1,043,982
|{{decrease}} 0.6%
| 1,198,956
|{{decrease}} 0.6%
|-
| 2011
| 117,431
|{{decrease}} 24.2%
| 1,046,453
|{{increase}} 0.2%
| 1,163,884
|{{decrease}} 2.9%
|-
| 2012
| 126,952
|{{increase}} 8.1%
| 1,084,244
|{{increase}} 3.6%
| 1,211,196
|{{increase}} 4.1%
|-
| 2013
| 130,668
|{{increase}} 2.9%
| 1,173,011
|{{increase}} 8.1%
| 1,303,679
|{{increase}} 7.6%
|-
| 2014
| 126,991
|{{decrease}} 2.8%
|1,246,095
|{{increase}} 6.2%
| 1,387,494
|{{increase}} 5.2%
|-
| 2015
|125,860
|{{decrease}} 0.9%
|1,272,834
|{{increase}} 2.2%
| 1,398,694
|{{increase}} 0.8%
|-
| 2016
|134,548
|{{increase}} 6.9%<ref>http://www.caa.bg/page.php?category=27</ref>
| 1,536,594
|{{increase}} 20.7%
| 1,689,595<ref>http://www.fraport.de/content/fraport/de/misc/binaer/investor-relations/verkehrszahlen/2017/jcr:content.file/12-traffic-sheet-2016-dezember_deutsch.pdf</ref>
|{{increase}} 20.8%
|-
| 2017 (01.01 - 31.03)
| 
| {{increase}}
| 
| {{increase}}
| 93,612<ref>http://www.fraport.de/content/fraport/de/misc/binaer/investor-relations/verkehrszahlen/2017/verkehrszahlen-maerz/jcr:content.file/03-traffic-sheet-2017-maerz_deutsch.pdf</ref>
| {{increase}} 13.4%
|}
</center>

==Access==

===Bus===
Bus line 409 connects the airport with Varna city center and resorts nearby (route: ''Varna Airport – [[Mall Varna]] – Varna Bus Station / Grand Mall – City Center – [[Saints Constantine and Helena, Bulgaria|Saints Constantine and Helena]] - [[Golden Sands]]''). The bus stop is located just outside Terminal 2.<ref>{{cite web|url=http://www.varna-airport.bg/PassengerServices/Howtogetthere/Bybus/tabid/180/language/en-US/Default.aspx|title=Начало|publisher=|accessdate=7 June 2015}}</ref>

===Taxi===

The taxi stand is located in front of Terminal 1's departure area. A taxi ride from Varna Airport to the city center takes approximately 15 minutes.

==Incidents and accidents==
*On 24 May 2013, [[Air VIA]] flight 502 from [[Leipzig/Halle Airport|Leipzig/Halle]] to Varna overran runway 09 at Varna Airport after touchdown. Two passengers were injured during evacuation.<ref>{{cite web|url=http://www.dnevnik.bg/morski/2013/05/24/2067707_samolet_izleze_ot_pistata_na_letishte_varna_dve_jeni/|title=Морски - Самолет излезе от пистата на летище Варна, две жени са със счупени крайници (допълнена и видео) - видео - Dnevnik.bg|work=www.dnevnik.bg|accessdate=7 June 2015}}</ref><ref>{{cite web|url=http://avherald.com/h?article=462c0ec6&opt=7168|title=The Aviation Herald|publisher=|accessdate=7 June 2015}}</ref>
*On 5 June 1992 Balkan Bulgarian Airlines Tupolev 154B overran runway 27 in bad weather conditions. There ware no casualties, but the plane was written off.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19920605-0|title=ASN Aircraft accident Tupolev 154B LZ-BTD Varna Airport (VAR)|author=Harro Ranter|date=5 June 1992|publisher=|accessdate=7 June 2015}}</ref>

==See also==
* [[List of airports in Bulgaria]]
* [[List of airlines of Bulgaria]]
* [[List of the busiest airports in Europe by passenger traffic]]

==References==
{{reflist}}

==External links==
{{commonscat-inline}}
* [http://www.varna-airport.com/ Varna Airport Homepage]
* [http://varna.info.bg/english/airport_en.htm History of Varna Airport]

{{Portalbar|Bulgaria|Aviation}}
{{Airports in Bulgaria}}
{{List of airports in Europe}}

[[Category:Airports in Bulgaria]]
[[Category:Buildings and structures in Varna Province]]