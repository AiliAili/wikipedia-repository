{{Infobox journal
| title         = Alcohol and Alcoholism
| cover         = 
| caption       = 
| former_names  = Bulletin on Alcoholism, Journal of Alcoholism, British Journal on Alcohol and Alcoholism
| abbreviation  = Alcohol Alcohol.
| discipline    = [[Addiction medicine]]
| peer-reviewed = 
| language      = English
| editors       = Jonathan D. Chick, Philippe De Witte, Lorenzo Leggio
| publisher     = [[Oxford University Press]]
| country       = 
| history       = 1963-present
| frequency     = Bimonthly
| openaccess    = 
| license       = 
| impact        = 2.724
| impact-year   = 2015
| ISSNlabel     = 
| ISSN          = 0735-0414
| eISSN         = 1464-3502
| CODEN         = ALALDD
| JSTOR         = 
| LCCN          = 83644732
| OCLC          = 817791753
| website       = https://academic.oup.com/alcalc
| link1         = https://academic.oup.com/alcalc/issue/current
| link1-name    = Online access
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
'''''Alcohol and Alcoholism''''' is a bimonthly [[peer-review]]ed [[medical journal]] covering [[alcoholism]] and other [[health effects of alcohol]]. It was established in 1963 as the '''''Bulletin on Alcoholism''''', with [[H.D. Chalke]] as the founding editor.<ref>{{cite journal|last1=Thomson|first1=Allan D.|title=The Journal Evolves|journal=British Journal on Alcohol and Alcoholism|volume=17|issue=4|pages=125-127|url=https://academic.oup.com/alcalc/article-abstract/17/4/125/98879/The-Journal-Evolves?redirectedFrom=PDF}}</ref> In 1968, it was renamed the '''''Journal of Alcoholism''''', and in 1977, it was again renamed, this time to '''''British Journal on Alcohol and Alcoholism'''''. In 1983, it obtained its current name. It is co-owned and co-published by the [[Medical Council on Alcohol]] (MCA) along with [[Oxford University Press]], which bought a 50% stake in the journal in 2011.<ref>{{Cite press release |title=Oxford University Press and Medical Council on Alcohol announce long-term partnership |date=2012-01-24 |url=https://www.eurekalert.org/pub_releases/2012-01/oup-oup012412.php}}</ref> It is the official journal of both the MCA and the [[European Society for Biomedical Research on Alcoholism]]. The [[editors-in-chief]] are Jonathan D. Chick ([[Castle Craig Hospital]]), Philippe de Witte ([[Université Catholique de Louvain]]), and Lorenzo Leggio ([[Bethesda, MD, USA]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.724, ranking it 7th out of 18 journals in the category "Substance Abuse".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Substance Abuse |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}
==External links==
*{{Official website|https://academic.oup.com/alcalc}}

[[Category:Oxford University Press academic journals]]
[[Category:Addiction medicine journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1963]]
[[Category:English-language journals]]
[[Category:Alcohol abuse]]