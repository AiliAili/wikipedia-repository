{{Use mdy dates|date=September 2011}}
{{Infobox journal
| title        = American Journal of Epidemiology
| cover        = 
| editor       = [[Moyses Szklo]]
| discipline   = [[Public health]], Medicine
| language     = English language
| abbreviation = Am J Epidemiol
| country      = {{flagicon|USA}}
| frequency    = Semi-monthly
| history      = 
| openaccess   = 
| website      = http://aje.oxfordjournals.org
| publisher=[[Oxford University Press]] for the [[Johns Hopkins Bloomberg School of Public Health]]
| impact       = 5.230
| impact-year  = 2014
| eISSN        = 1476-6256
| ISSN         = 0002-9262
}}

The '''American Journal of Epidemiology''' (AJE) is a [[peer reviewed|peer-reviewed]] journal for [[empirical research]] findings, opinion pieces, and methodological developments in the field of [[epidemiology|epidemiological]] research. The current Editor-in-Chief is Dr. [[Moyses Szklo]].

Articles published in AJE are indexed by [[PubMed]], [[Embase]], and a number of other databases. AJE offers open access options for authors. It is published semi-monthly. Entire issues have been dedicated to abstracts from academic meetings (Society of Epidemiologic Research, North American Congress of Epidemiology), the history of the [[Epidemic Intelligence Service]] of the [[Centers for Disease Control and Prevention]] (CDC),<ref>{{Cite journal|title = Introduction: the Centers for Disease Control and Prevention's Epi-Aids--a fond recollection|journal = American Journal of Epidemiology|date = 2011-12-01|issn = 1476-6256|pmid = 22135388|pages = S1-3|volume = 174|issue = 11 Suppl|doi = 10.1093/aje/kwr303|first = Jeffrey P.|last = Koplan|first2 = William H.|last2 = Foege}}</ref> the life of [[George W. Comstock]],<ref>{{Cite journal|title = George W. Comstock--an appreciation|journal = American Journal of Epidemiology|date = 2008-10-01|issn = 1476-6256|pmid = 18794222|pages = 667|volume = 168|issue = 7|doi = 10.1093/aje/kwn201|first = Moyses|last = Szklo}}</ref> and the celebration of notable anniversaries of schools of public health ([[UC Berkeley School of Public Health|University of California, Berkeley, School of Public Health]];<ref>{{Cite journal|title = The university of california, berkeley, school of public health: honoring the past, shaping the future|url = http://www.ncbi.nlm.nih.gov/pubmed/?term=20880964|journal = American Journal of Epidemiology|date = 1995-11-01|issn = 0002-9262|pmid = 20880964|pages = S1-2|volume = 142|issue = 9 Suppl|doi = 10.1093/aje/142.Supplement_9.S1|first = P. A.|last = Buffler}}</ref> [[Tulane University School of Public Health and Tropical Medicine]];<ref>{{Cite journal|title = Tulane University School of Public Health and Tropical Medicine 100th anniversary. Introduction. From hygiene and tropical medicine to global health|url = http://www.ncbi.nlm.nih.gov/pubmed/?term=23035133|journal = American Journal of Epidemiology|date = 2012-10-01|issn = 1476-6256|pmid = 23035133|pages = S1-3|volume = 176 Suppl 7|doi = 10.1093/aje/kws253|first = Pierre|last = Buekens}}</ref> [[Johns Hopkins Bloomberg School of Public Health]]).

AJE is currently ranked 4th in the field of epidemiology according to [[Google Scholar]].<ref>{{Cite web|title = Google Scholar Metrics|url = https://scholar.google.com/citations?view_op=top_venues&hl=en&vq=med_epidemiology|website = scholar.google.com|accessdate = 2015-09-10}}</ref> It has an [[impact factor]] of 5.230 (as of 2014) and the 5-year impact factor is 5.632 according to [[Journal Citation Reports]].

== History ==
This journal was founded in 1920 and originally named the ''American Journal of Hygiene''. In 1965, the journal acquired its current name – ''American Journal of Epidemiology''. Since its inception, the journal has been based in the [[JHSPH Department of Epidemiology|Department of Epidemiology]] at [[Johns Hopkins Bloomberg School of Public Health]] and is published in association with the [https://epiresearch.org/ Society of Epidemiologic Research].

== Editors-in-chief: past and present ==
* [[William H. Welch]] (1920–1927)
* Roscoe Hyde (1927–1938)
* Martin Frobisher (1938–1948)
* [[David Bodian]] (1948–1957)
* [[Philip Sartwell|Philip E. Sartwell]] (1957–1958)
* Abraham G. Osler (1958–1965)
* Neal Nathanson (1965–1979)
* [[George W. Comstock]] (1979–1988)
* [[Moyses Szklo]] (1988–present)

==References==
{{reflist|30em}}
{{Johns Hopkins University}}

[[Category:Epidemiology journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Biweekly journals]]
[[Category:Publications established in 1965]]