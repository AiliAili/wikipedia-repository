{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
{{Infobox officeholder
|honorific-prefix = [[The Honourable]]
|name = Sir Henry Ayers
|honorific-suffix =[[Order of St Michael and St George|GCMG]]
|image=Henry Ayers 3.jpeg
|order=8th [[Premier of South Australia]]
|term_start1=15 July 1863
|term_end1=4 August 1864
|term_start2=20 September 1865
|term_end2=23 October 1865
|term_start3=3 May 1867
|term_end3=24 September 1868
|term_start4=13 October 1868
|term_end4=3 November 1868
|term_start5=22 January 1872
|term_end5=22 July 1873
|monarch1          = [[Queen Victoria|Victoria]]
|monarch2          = [[Queen Victoria|Victoria]]
|monarch3          = [[Queen Victoria|Victoria]]
|monarch4          = [[Queen Victoria|Victoria]]
|monarch5          = [[Queen Victoria|Victoria]]
|governor1         = [[Dominick Daly|Sir Dominick Daly]]
|governor2         = [[Dominick Daly|Sir Dominick Daly]]
|governor3         = [[Dominick Daly|Sir Dominick Daly]]
|governor4         = [[Sir James Fergusson, 6th Baronet|Sir James Fergusson]]
|governor5         = [[Sir James Fergusson, 6th Baronet|Sir James Fergusson]]<br>[[Anthony Musgrave|Sir Anthony Musgrave]]
|predecessor1=[[Francis Dutton]]
|predecessor2=[[Francis Dutton]]
|predecessor3=[[James Boucaut|Sir James Boucaut]]
|predecessor4=[[John Hart (South Australian colonist)|Captain John Hart]]
|predecessor5=[[Arthur Blyth|Sir Arthur Blyth]]
|successor1=[[Arthur Blyth|Sir Arthur Blyth]]
|successor2=[[John Hart (South Australian colonist)|Captain John Hart]]
|successor3=[[John Hart (South Australian colonist)|Captain John Hart]]
|successor4=[[Henry Strangways]]
|successor5=[[Arthur Blyth|Sir Arthur Blyth]]
|birth_date= {{birth date|1821|5|1|df=y}}
|birth_place= [[Portsea Island|Portsea]], [[Portsmouth]], [[Hampshire]], [[England]], [[United Kingdom of Great Britain and Ireland|UK]]
|death_date   = {{death date and age|1897|6|11|1821|5|1|df=y}}
|death_place  = [[Adelaide]], [[South Australia]], [[Australia]]
}}
[[File:Henry Ayers 6.jpeg|thumb|right]]
[[File:Henry Ayers 2.jpg|thumb|right]]

'''Sir Henry Ayers''' [[Order of St Michael and St George|GCMG]] (1 May 1821 – 11 June 1897) was the eighth [[Premier of South Australia]], serving a record five times between 1863 and 1873.

Historians note that his lasting memorial is in the name of [[Uluru|Ayers Rock]], which was discovered in [[1873]] by [[William Gosse (explorer)|William Gosse]].<ref>[http://www.ibtauris.com/Books/Biography%20%20True%20Stories/Biography%20general/Biography%20historical%20political%20%20military/Henry%20Ayers%20The%20Man%20Who%20Became%20a%20Rock.aspx Henry Ayers: The Man Who Became a Rock - by Jason Shute, published by [[I.B.Tauris]]]</ref><ref>[http://www.sahistorians.org.au/175/chronology/june/11-june-1897-sir-henry-ayers.shtml On this day, 11 June 1897, Sir Henry Ayers: Professional Historians Association]</ref>

== Overview ==

Ayers was born at [[Portsea Island|Portsea]], [[Portsmouth]], [[Hampshire]], [[England]], the son of William Ayers, of the Portsmouth dockyard, and his wife Elizabeth, née Breakes. Educated at the Beneficial Society's School (Portsea) he entered a law office in 1832. He emigrated, as a carpenter, to [[South Australia]] in 1840 with his wife, Anne (née Potts) with free passages. Until 1845 he worked as a law clerk, he was then appointed secretary of the [[Burra, South Australia|Burra Burra]] mines.<ref name=ADB>{{cite web |url=http://www.adb.online.anu.edu.au/biogs/A030065b.htm |title=Ayers, Sir Henry (1821 - 1897) |accessdate=2008-01-27 |author=S. R. Parr |work=[[Australian Dictionary of Biography]], Volume 3 |publisher=[[Melbourne University Press|MUP]] |year=1969 |pages=63–64}}</ref> [[Henry Roach]] was chief Captain, responsible for day-to-day operations, from 1847 to 1867.<ref>{{citation|ref={{harvid|The Monster Mine – Burra Mine}} |title=The Monster Mine|work=Burra Mine|publisher=Department of Mines and Energy |url=http://www.burrahistory.info/BurraMining.htm|accessdate=2016-08-15}}</ref> Within a year the mine employed over 1000 men.For nearly 50 years, Ayers was in control of this mine, initially as the secretary and later as the managing director. He made his wealth from the Burra Burra Copper Mines, which was known as the "Monster Mine", which secured the wealth of the colony of [[South Australia]].

== Politics ==

On 9 March 1857 Ayers was elected to the first [[South Australian Legislative Council]] under responsible government, the youngest member elected.<ref name="ADB"/> He was continuously a member for over 36 years. For many years the whole colony formed a single electorate for the council; on two occasions (1865 and 1873) Ayers headed the poll.

In March 1863 Ayers was selected as one of the three South Australian representatives at the inter-colonial conference on uniform tariffs and inland customs duties. He also represented the colony at several other conferences from 1864 to 1877.<ref name="ADB"/> 
On 4 July 1863 Ayers became minister without portfolio in the first [[Francis Dutton|Dutton]] cabinet. This ministry resigned just 11 days later however, as council demanded that it should have an executive minister to represent the government and Dutton refused. Ayers formed his first ministry as [[Premier of South Australia|Premier]] and [[Chief Secretary of South Australia|Chief Secretary]] on 15 July 1863.<ref name="ADB"/>  The house was much divided and it was almost impossible to get business done. Ayers reconstructed his ministry on 22 July 1864 but was defeated, and resigned on 4 August 1864. The [[Arthur Blyth|Blyth]] ministry which was then formed included Ayers as chief secretary, but did not survive a general election and resigned on 22 March 1865. When Dutton formed his second ministry Ayers had his old position as chief secretary, and still retaining that office, formed his third administration on 20 September 1865 which lasted just over a month. In spite of dissolutions it was found very difficult to get a workable house. There were 18 ministries tween July 1863 and July 1873. Ayers became the premier again from May 1867 to September 1868, October to November 1868, 27 January 1872 to March 1872, and with an entirely new team of ministers, from March 1872 to July 1873. He held the position of chief secretary in the [[John Colton (politician)|Colton ministry]] from June 1876 to October 1877,<ref name="ADB"/> his last term of office.

In 1881 Ayers was elected [[President of the South Australian Legislative Council]], and until December 1893 carried out his duties with ability, impartiality and courtesy. He died in Adelaide on 11 June 1897. His wife had died in 1881, and he was survived by three sons and a daughter. He was created a [[Order of St Michael and St George|CMG]] in 1870, knighted a KCMG in 1872, and raised to GCMG in 1894.<ref name="ADB"/>

== Legacy ==

Apart from his mining interests, Ayers held important directorates and was for many years a member and chairman of the board of trustees of the [[Savings Bank of South Australia]]; he was re-appointed chairman only a few days before his death. He was the first Chairman of the South Australian Gas Company,  from 1862 was a governor of the [[Adelaide Botanic Gardens]], president of the South Australian Old Colonists' Association, and was for many years on the council of the [[University of Adelaide]]. He was in parliament for an unbroken term of 37 years and in no other Australian colony or state has a politician exercised so much influence or been in so many ministries while a member of the upper house. It is likely, however, that if Ayers had been in the House of Assembly he would have had more control of business, and his seven premierships would have been longer in duration and more fruitful in results. An address he gave on ''Pioneer Difficulties on Founding South Australia'' was published as a pamphlet in 1891.

Ayers resided in [[Ayers House (Adelaide, South Australia)|Ayers House]] from 1855 until 1897 and built it from a 9-room house into a grand mansion in the 1860s. The youngest child, Lucy, was born at [[Ayers House (Adelaide, South Australia)|Ayers House]]. During Sir Henry's parliamentary service, Ayers House was used for Cabinet meetings, parliamentary dinners, and grand balls.

==Family==

Henry Ayers married Anne Potts (1812 – 13 August 1881) at [[Alverstoke]] around 1839.<ref name=ADB/> Anne was a sister of winemaker [[Frank Potts (winemaker)|Frank Potts]]. They had four sons:<ref>[http://trove.nla.gov.au/ndp/del/article/44195537 Provisions of the Will], Barrier Miner (Broken Hill, NSW), 27 August 1897</ref><ref>[http://trove.nla.gov.au/ndp/del/article/35089446 Sir Henry Ayers Will], The Advertiser (Adelaide, SA), 28 August 1897</ref>
*Frank Richman Ayers (ca.1842 - 23 April 1906)
*Henry "Harry" Lockett Ayers (1844–1905), married [[Ada Fisher Morphett]] (5 May 1843 – 1939) on 1 October 1866
*Frederick<!--Frederic in "Register" death notice--> Ayers (ca.1848 - 1 February 1897) married Evelyn Cameron Page on 8 November 1870.
*(Arthur) Ernest Ayers ( - 2 April 1921) married Barbara Agnes Milne on 30 April 1878. Barbara was a daughter of [[William Milne (politician)|William Milne MP.]]
and two daughters:
*Margaret Elizabeth Ayers (ca.1849 - 19 September 1887) married Arthur Robert Lungley on 29 April 1875
*Lucy Josephine Ayers (ca.1857 - 11 May 1945) married John Bagot on 24 September 1878. John was a grandson of [[Charles Hervey Bagot]].

== References ==
{{reflist}} 
*''The Advertiser'', Adelaide, 12 June 1897;
*E. Hodder, ''The History of South Australia''.
*{{Dictionary of Australian Biography|First=Henry |Last=Ayers |shortlink=0-dict-biogA.html#ayers1}}
*Additional sources listed by the ''Australian Dictionary of Biography'':
**L. L. Ayers, ''Sir Henry Ayers, K.C.M.G. and His Family'' (Adel, 1946);
**G. D. Combe, ''Responsible Government in South Australia'' (Adel, 1957);
**''The South Australian Register'', 12 June 1897;
**P. L. Edgar, ''Sir James Boucaut'' (B.A. Hons thesis, University of Adelaide, 1961);
**J. B. Graham letters (National Library of Australia);
**Henry Parkes letters (State Library of New South Wales).

==External links==
*[http://www.parliament.sa.gov.au/pp/html/ayers.shtm SA Parliament - Ayers]
*[http://www.samemory.sa.gov.au/site/page.cfm?u=61&c=3529 Daguerreotype of Sir Henry Ayers, c.1848, State Library of South Australia]

{{s-start}}
{{s-par|au-sa}}
{{s-bef|before=[[John Tuthill Bagot|John Bagot]]<br>[[Arthur Blyth]]<br>[[Francis Dutton]]}}
{{s-ttl|title=Member of the [[South Australian Legislative Council]] | years=1857–1893 | alongside=[[Members of the South Australian Legislative Council, 1857–1861|Multiple Members]]}}
{{s-aft|after=[[Martin Peter Friedrich Basedow|Martin Basedow]]<br />[[James Martin (South Australian politician)|James Martin]]<br>[[William Russell (Australian politician)|William Russell]]}}
|-
{{s-par|au-sa-lc}}
{{s-bef | before=[[William Milne (politician)|William Milne]]}}
{{s-ttl | title=[[President of the South Australian Legislative Council|President of the <br>South Australian Legislative Council]] | years=1881&ndash;1893}}
{{s-aft | after=[[Richard Baker (Australian politician)|Richard Baker]]}}
|-
{{s-off}}
{{s-bef | before = [[Francis Dutton]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1863–1864}}
{{s-aft|after=[[Arthur Blyth]]}}
|-
{{s-bef|before=[[John Hart (South Australian colonist)|John Hart]]}}
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1863–1865}}
{{s-aft|rows=4|after=[[John Hart (South Australian colonist)|John Hart]]}}
|-
{{s-bef | before = [[Francis Dutton]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1865}}
|-
{{s-bef | before = [[James Boucaut]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1867–1868}}
|-
{{s-bef|before=[[Arthur Blyth]]}}
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1867–1868}}
|-
{{s-bef |rows=2| before = [[John Hart (Australian politician)|John Hart]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1868}}
{{s-aft|after=[[Henry Strangways]]}}
|-
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1868}}
{{s-aft|after=[[John Tuthill Bagot|John Bagot]]}}
|-
{{s-bef | before = [[Arthur Blyth]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1872–1873}}
{{s-aft|rows=2|after=[[Arthur Blyth]]}}
|-
{{s-bef|before=[[William Milne (politician)|William Milne]]}}
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1872–1873}}
|-
{{s-bef|before=[[George Charles Hawker|George Hawker]]}}
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1876–1877}}
{{s-aft|after=[[William Morgan (Australian politician)|William Morgan]]}}
{{end}}

{{Premiers of South Australia}}

{{Authority control}}

{{DEFAULTSORT:Ayers, Henry}}
[[Category:Premiers of South Australia]]
[[Category:1821 births]]
[[Category:1897 deaths]]
[[Category:Knights Grand Cross of the Order of St Michael and St George]]
[[Category:English emigrants to colonial Australia]]
[[Category:Presidents of the South Australian Legislative Council]]
[[Category:Settlers of South Australia]]
[[Category:Adelaide Club]]
[[Category:19th-century Australian businesspeople]]
[[Category:19th-century Australian politicians]]