{{Redirect|DH-4|the 1950s flying platform|de Lackner HZ-1 Aerocycle}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see 
[[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name = Airco DH.4
  |image = Airco DH-4.jpg
  |caption = DH.4 above the clouds in France
}}{{Infobox aircraft type
  |type = [[Light bomber]] / General purpose
  |manufacturer = [[Airco]]
  |designer = 
  |first flight = August 1916
  |introduction = March 1917
  |retired = 1932 (United States Army)
  |status = 
  |primary user = United Kingdom
  |more users = United States
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 6295, of which 4846 were built in the United States.<ref name="Jackson p58"/>
  |unit cost = $11,250
  |variants with their own articles = [[Airco DH.9|DH9]], [[Airco DH.9A|DH9A]], [[Dayton-Wright Cabin Cruiser]] 
}}
|}

The '''Airco DH.4''' was a [[United Kingdom|British]] two-seat [[biplane]] day bomber of [[World War I]]. It was designed by [[Geoffrey de Havilland]] (hence "DH") for [[Airco]], and was the first British two-seat light day-bomber to have an effective defensive armament. It first flew in August 1916 and entered service with the [[Royal Flying Corps]] (RFC) in March 1917. The majority of DH.4s were actually built as general purpose two-seaters in the [[United States]], for service with the American forces in France.

The DH.4 was tried with several engines, of which the best was the 375&nbsp;hp (280&nbsp;kW) [[Rolls-Royce Eagle]] engine. Armament and [[Aircraft ordnance|ordnance]] for the aircraft consisted of one 0.303&nbsp;in (7.7&nbsp;mm) [[Vickers machine gun]] for the pilot and one 0.303&nbsp;in (7.7&nbsp;mm) [[Lewis gun]] on a [[Scarff ring]] mounting for the observer. Two 230&nbsp;lb (100&nbsp;kg) bombs or four 112&nbsp;lb (51&nbsp;kg) bombs could be carried. The DH.4 entered service on 6 March 1917 with [[No. 55 Squadron RAF|No. 55 Squadron]] in France.

==Design and development==
The DH.4 was designed by Geoffrey de Havilland as a light two-seat day bomber powered by the new [[Beardmore Halford Pullinger]] (BHP) engine. The prototype first flew in August 1916, powered by a prototype BHP engine rated at 230&nbsp;hp (170&nbsp;kW).<ref name="Jacksonp53">Jackson 1987, p. 53.</ref> While the DH.4 trials were promising, the BHP engine required major redesign before entering production, and the [[Rolls-Royce Eagle]] engine was selected as the DH.4's powerplant. The first order for 50 DH.4s, powered by 250&nbsp;hp (186&nbsp;kW) Eagle III engines was placed at the end of 1916.<ref name="mason"/>

The aircraft was a conventional tractor [[Interplane strut|two bay]] biplane of all-wooden construction. The crew of two were accommodated in widely spaced cockpits, separated by the fuel tank.<ref name="mason"/> It was armed with a single forward-firing synchronised [[Vickers machine gun]] and one or two .303&nbsp;in (7.7&nbsp;mm) [[Lewis gun]]s fitted on a [[Scarff ring]] fired by the observer. A bomb load of 460&nbsp;lb (210&nbsp;kg) could be fitted to external racks. While the crew arrangement gave good fields of view for the pilot and observer, it caused communication problems between the two crew members, particularly in combat, where the [[speaking tube]] linking the cockpits was of limited use.<ref name="Bruce p507">Bruce 1952, p. 507.</ref>

As production continued, DH.4s were fitted with Eagle engines of increasing power, settling on the 375&nbsp;hp (280&nbsp;kW) Eagle VIII, which powered the majority of frontline DH.4s by the end of 1917. Because of the chronic shortage of Rolls-Royce aero engines in general, and Eagles in particular, alternative engines were also investigated, with the BHP (230&nbsp;hp/170&nbsp;kW), the [[Royal Aircraft Establishment|Royal Aircraft Factory]] [[RAF 3|RAF3A]] (200&nbsp;hp/150&nbsp;kW), the [[Siddeley Puma]] (230&nbsp;hp/170&nbsp;kW) and the 260&nbsp;hp (190&nbsp;kW) [[Fiat]], all being used in production aircraft.<ref name="mason"/> None of these engines could match the Rolls-Royce Eagle; however, there were simply not enough Eagles available.

In American production, the new [[Liberty L-12|Liberty engine]] proved suitable as a DH.4 powerplant. The Liberty was also to eventually power the British [[Airco DH.9A|DH.9A]].

===Production===
[[File:EarlyDH4.jpg|thumb|An early production DH.4]]
Production was by [[Airco]], F.W. Berwick and Co, Glendower Aircraft Company, Palladium Autocars, Vulcan Motor and Engineering, and the [[Westland Aircraft]] Works in the UK. A total of 1,449 aircraft (from orders for 1,700 aircraft) were made in the UK for the RFC and [[Royal Naval Air Service|RNAS]].<ref name="Jackson p54">Jackson 1987, p. 54.</ref> [[SABCA]] of [[Belgium]] made a further 15 in 1926.<ref name="Jackson p60">Jackson 1987, p. 60.</ref>

In the United States, the [[Boeing|Boeing Airplane Corporation]], [[Dayton-Wright Airplane Company]], the [[Fisher Body|Fisher Body Corporation]], and the [[Standard Aircraft Corporation]] produced the DH-4 with the [[Liberty L-12|Liberty L-12]] engine for the American air services. A total of 9,500 DH-4s were ordered from American manufacturers, of which 1,885 actually reached France during the war.<ref name="Jackson p58">Jackson 1987, p. 58.</ref>

After the war, a number of firms, most significantly [[Boeing]], were contracted by the U.S. Army to remanufacture surplus DH-4s to DH-4B standard. Known by Boeing as the '''Model 16''', deliveries of 111 aircraft from this manufacturer took place between March and July 1920, with 50 of them returned for further refurbishments three years later.<ref name="Bowersp67">Bowers 1989, p. 67.</ref>

In 1923, the Army ordered a new DH-4 variant from Boeing, distinguished by a fuselage of fabric-covered steel tube in place of the original plywood structure. These three prototypes were designated '''DH-4M-1''' (M for modernized) and were ordered into production alongside the generally similar '''DH-4M-2''' developed by [[Atlantic Aircraft]]. A total of 22 of the 163 DH-4M-1s were converted by the Army into dual-control trainers ('''DH-4M-1T''') and a few more into target tugs ('''DH-4M-1K'''). Thirty of the aircraft ordered by the Army were diverted to the Navy for Marine Corps use, these designated '''O2B-1''' for the base model, and '''O2B-2''' for aircraft equipped for night and [[cross-country flying]].<ref name="bowersp70">Bowers 1989, p. 70.</ref>

==Operational history==

===British military service===
The DH.4 entered service with the RFC in January 1917, first being used by [[No. 55 Squadron RAF|No. 55 Squadron]].<ref name="mason"/> More squadrons were equipped with the type to increase the bombing capacity of the RFC, with two squadrons re-equipping in May, and a total of six squadrons by the end of the year.<ref name="mason"/> As well as the RFC, the RNAS also used the DH.4, both over [[France]] and over [[Italy]] and the [[Aegean Sea|Aegean]] front.<ref name="mason"/> The DH.4 was also used for coastal patrols by the RNAS. One, crewed by the pilot [[Major]] [[Egbert Cadbury]] and [[Captain (land)|Captain]] [[Robert Leckie (aviator)|Robert Leckie]] (later Air Vice-Marshal) as gunner, shot down [[Zeppelin]] ''L70'' on 5 August 1918.<ref name="Thet navyp86">Thetford 1978, p. 86.</ref> Four RNAS DH.4s were credited with sinking the German [[U-boat]] [[SM UB 12|''UB 12'']] on 19 August 1918.<ref name="Thet navyp86"/>

The DH.4 proved a huge success and was often considered the best single-engined bomber of World War I.{{#tag:ref|Quote: "Certainly the DH.4 was without peer among the day-bombing aeroplanes used by the aerial forces of any of the combatant nations."<ref name="Bruce p507"/>|group=N}} Even when fully loaded with bombs, with its reliability and impressive performance, the type proved highly popular with its crews. The Airco DH.4 was easy to fly, and especially when fitted with the Rolls-Royce Eagle engine, its speed and altitude performance gave it a good deal of invulnerability to German fighter interception,<ref name="Jackson p54-6">Jackson 1987, pp. 54–56.</ref> so that the DH.4 often did not require a fighter escort on missions, a concept furthered by [[de Havilland]] in the later [[de Havilland Mosquito|Mosquito]] in World War II.

A drawback of the design was the distance between pilot and observer, as they were separated by the large main fuel tank. This made communication between the crew members difficult, especially in combat with enemy fighters.<ref name="Jackson p56">Jackson 1987, p. 56.</ref> There was also some controversy (especially in American service) that this placement of the fuel tank was inherently unsafe.<ref name="Maurer p.12, 87, 120, 132">Maurer 1979, pp. 12, 87, 120, 132.</ref> In fact, most contemporary aircraft were prone to catching fire in the air.{{#tag:ref|Sometimes derided as the "Flaming Coffin," ''Gorrell's History of the Air Service of the AEF'' refuted the misconception. Quote: "Of 33 DH-4s lost to enemy action by the US Air Service, eight fell in flames- no worse than the average at the time."<ref>Williams 1999, p. 83.</ref>|group=N}} The fire hazard was reduced, however, when the pressurised fuel system was replaced by one using wind-driven fuel pumps late in 1917,<ref name="Jackson p56"/> although this was not initially adopted by American-built aircraft.<ref name="Maurer p551">Maurer 1979, p. 551.</ref> The otherwise inferior [[Airco DH.9|DH.9]] brought the pilot and observer closer together by placing the fuel tank in the usual place, between the pilot and the engine.

Despite its success, numbers in service with the RFC actually started to decline from spring 1918, mainly due to a shortage of engines, and production switched to the DH.9, which turned out to be disappointing, being inferior to the DH.4 in most respects. It was left to the further developed [[Airco DH.9A|DH.9A]], with the American Liberty engine, to satisfactorily replace the DH.4.

When the [[Independent Air Force]] was set up in June 1918 to carry out [[strategic bombing]] of targets in [[Germany]], the DH.4s of 55 Squadron formed part of it, being used for daylight attacks.<ref name="Bruce p507"/> 55 Squadron developed tactics of flying in wedge formations, bombing on the leader's command and with the massed defensive fire of the formation deterring attacks by enemy fighters.<ref name="Williams p84">Williams 1999, p. 84.</ref> Despite heavy losses, 55 Squadron continued in operation, the only one of the day bombing squadrons in the Independent Force which did not have to temporarily stand down owing to aircrew losses.<ref name="Williams p195">Williams 1999, p. 195.</ref>

After the [[Armistice with Germany (Compiègne)|Armistice]], the RAF formed [[No. 2 Communication Squadron RAF|No. 2 Communication Squadron]], equipped with DH.4s to carry important passengers to and from the [[Paris Peace Conference, 1919|Paris Peace Conference]]. Several of the DH.4s used for this purpose were modified with an enclosed cabin for two passengers at the request of [[Bonar Law|Andrew Bonar Law]].<ref name="Jackson 77">Jackson 1987, p. 77.</ref> These aircraft were designated DH.4A, with at least seven being converted for the RAF, and a further nine for civil use.<ref name="Jackson 81">Jackson 1987, p. 81.</ref>

===United States military service===
At the time of its entry into the war, the [[United States Army Air Service]] lacked any aircraft suitable for front line combat. It therefore procured various aircraft from the British and French, one being the DH.4. As the '''DH-4''', it was manufactured mostly by Dayton-Wright and Fisher Body for service with the United States from 1918, the first American built DH-4 being delivered to France in May 1918, with combat operations commencing in August 1918.<ref name="USAF Factsheet">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?fsID=324 "Fact Sheets: De Havilland DH-4."] ''National Museum of the USAF''. Retrieved: 19 April 2008.</ref> The powerplant was a [[Liberty L-12]] of 400&nbsp;hp (300&nbsp;kW) and it was fitted with two .30&nbsp;in (7.62&nbsp;mm) [[Marlin Firearms|Marlin]] (a development of the [[M1895 Colt–Browning machine gun|Colt-Browning]]) machine guns in the nose and two .30&nbsp;in (7.62&nbsp;mm) [[Lewis gun]]s in the rear and could carry 322&nbsp;lb (146&nbsp;kg) of bombs. it could also be equipped with various radios like the [[SCR-68]] for artillery spotting missions. The heavier engine reduced performance compared with the Rolls-Royce powered version, but as the "Liberty Plane" it became the US Army Air Service standard general purpose two-seater, and on the whole was fairly popular with its crews. 
[[File:A Concise History of the U.S. Air Force Page 06-1.jpg|left|thumb|A formation of DH-4s in flight]]

Aircrew operating the DH-4 were awarded four of the six [[Medal of Honor|Medals of Honor]] awarded to American aviators. [[First Lieutenant#U.S. Army, U.S. Marine Corps and U.S. Air Force|First Lieutenant]] [[Harold Ernest Goettler]] and [[Second Lieutenant]] [[Erwin R. Bleckley]] received posthumous awards after being killed on 12 October 1918 attempting to drop supplies to the [[Lost Battalion (World War I)|Lost Battalion]] of the [[77th Infantry Division (United States)|77th Division]], cut off by German troops during the [[Meuse-Argonne Offensive]];<ref name="USAF Factsheet"/> while Second Lieutenant [[Ralph Talbot]] and [[Gunnery Sergeant]] [[Robert G. Robinson]] of the [[United States Marine Corps]] (USMC) were awarded the Medal of Honor for beating off attacks from 12 German fighters during a bombing raid over Belgium on 8 October 1918.<ref name="Workhorse">[http://www.airpower.au.af.mil/airchronicles/apj/apj02/win02/notam5.pdf "The De Havilland DH-4, Workhorse of the Army Air Service."] ''Air & Space Power Journal'', Winter 2002. Retrieved: 9 May 2008.</ref><ref>[http://www.arlingtoncemetery.net/rrobin.htm "Robert Guy Robinson, First Lieutenant, United States Marine Corps."] ''Arlington National Cemetery Website.'' Retrieved: 9 May 2008.</ref> The type flew with 13 U.S. squadrons by the end of 1918.<ref name="Angelucci p79">Angelucci 1981, p. 79.</ref>

Following the end of World War I, America had a large surplus of DH-4s, with the improved DH-4B becoming available, although none had been shipped to France. It was therefore decided that there was no point in returning aircraft across the Atlantic, so those remaining in France, together with other obsolete observation and trainer aircraft, were burned in what became known as the "Billion Dollar Bonfire".<ref name="Swanborough Military p198">Swanborough and Bowers 1963, p. 198.</ref> With limited funds available to develop and purchase replacements, the remaining DH-4s formed a major part of American air strength for several years, used for many roles, with as many as 60 variants produced.<ref name="Bruce p510">Bruce 1952, p. 510.</ref> DH-4s were also widely used for experimental flying, being used as engine testbeds and fitted with new wings. They were used for the first trials of [[Aerial refueling|air-to-air refueling]] on 25 June 1923, and one carried out an endurance flight of 37 hours, 15 minutes on 27–28 August, being refueled 16 times and setting 16 new world records for distance, speed and duration.<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=745 "Fact Sheets: Air-to-Air Refueling."] ''National Museum of the United States Air Force''. Retrieved: 10 May 2008.</ref> The DH-4 remained in service with the [[United States Army Air Corps]], successor to the United States Army Air Service, until 1932.<ref name="Swanborough Military p199">Swanborough and Bowers 1963, p. 199.</ref>

DH-4s were also used by the [[United States Navy]] and United States Marine Corps, both during World War I and postwar. The Navy and Marine Corps received 51 DH-4s during World War I, followed by 172 DH-4B and DH-4B-1 aircraft postwar and 30 DH-4M-1s with welded steel-tube fuselages (redesignated O2B) in 1925.<ref name="Swanborough Navy p156">Swanborough and Bowers 1976, p. 156.</ref> They remained in service with the Marine Corps until 1929, being used against rebel factions in [[United States occupation of Nicaragua|Nicaragua]] in 1927, carrying out the first dive-bombing attacks made by U.S. military forces.<ref name="Swanborough Navy p156"/> The U.S. Navy converted some DH-4M-1s into primitive air ambulances that could carry one stretcher casualty in an enclosed area behind the pilot.<ref>[https://books.google.com/books?id=0N8DAAAAMBAJ&pg=PA765&dq=Junkers+stratosphere&hl=en&ei=9voOTe-zH8qUnQeRrdHyDQ&sa=X&oi=book_result&ct=result&resnum=5&ved=0CDYQ6AEwBDgK#v=onepage&q=Junkers%20stratosphere&f=true "Help From The Skies."] ''Popular Mechanics,'' November 1929.</ref>

===Civil use===
[[File:DH-4 airmail.jpg|thumb|right|264px|Robertson Aircraft Corp. operated DH-4 mailplane (CAM 2) 1926 at the [[Historic Aircraft Restoration Museum]].]]

Following the end of World War I, DH.4s and 4As were used to operate scheduled passenger services in Europe by such airlines as [[Aircraft Transport and Travel]], [[Handley Page Transport]] and the Belgium airline [[SNETA]], ''G-EAJC'' of Aircraft Transport and Travel flying the first British commercial passenger service from [[Hounslow Heath Aerodrome]] to [[Paris–Le Bourget Airport|Paris Le Bourget]] on 25 August 1919, carrying a reporter from the ''[[London Evening Standard|Evening Standard]]'' newspaper and a load of newspapers and other freight.<ref name="Jackson civilv2 p41">Jackson 1973, p. 41.</ref><ref name="Jackson DH p79">Jackson 1987, p. 79.</ref> They were used by Aircraft Transport and Travel until it shut down in 1920, while Handley Page Transport and SNETA continued operating the DH.4 until 1921. One aircraft was used by [[Instone Air Line]]s until its merger into [[Imperial Airways]] in 1924.<ref name="Jackson civilv2 p43">Jackson 1973, p. 43.</ref>

[[File:DeHavilland Biplane stamp 24c 1923 issue.JPG|thumb|244px|left|In 1923 the U.S. Post Office released a stamp featuring the DeHavilland Biplane being used for airmail service<ref>{{cite news |title=24-cent DeHavilland Biplane |publisher=Smithsonian National Postal Museum}}</ref>]]
DH.4s were also used by the [[Australia]]n airline [[Qantas|QANTAS]], flying its first [[airmail]] service in 1922.<ref name="Jackson civilv2 p40">Jackson 1973, p. 40.</ref> Twelve DH.4s forming part of the [[Imperial Gift]] to [[Canada]] were used for forestry patrol and survey work, spotting hundreds of forest fires and helping to save millions of dollars worth of timber, with the last example finally being withdrawn in 1927.<ref name="Jackson DH p70-71">Jackson 1973, pp. 70–71.</ref>

The [[United States Postal Service|U.S. Post Office]] also adopted the DH-4 to carry air mail.<ref name="postal">{{cite web|first1=Nancy A. |last1=Pope |url=http://postalmuseum.si.edu/exhibits/current/airmail-in-america/the-airplanes/dehavilland-dh-4.html |title=deHavilland DH-4|publisher=[[National Postal Museum]], [[Smithsonian Institution]] |accessdate=July 21, 2015}}</ref>   The Service acquired 100 of them from the army in 1918, and retrofitted them to make them safer, denominating them as the DH.4B.<ref name="postal"/>  In 1919, the DH-4B was standardised by the US Post Office, being modified to be flown from the rear cockpit with a 400&nbsp;lb (180&nbsp;kg) watertight mail compartment replacing the forward cockpit. The airmail DH-4B were later modified with revised landing gear and an enlarged [[rudder]].<ref name="Swan Military p201">Swanborough and Bowers 1963, p. 201.</ref> DH-4s were used to establish a coast-to-coast, transcontinental airmail service, between San Francisco and New York, a distance of 2,680&nbsp;mi (4,310&nbsp;km), involving night flight, the first services starting on 21 August 1924.<ref name="postal"/> The DH-4 continued in Post Office service until 1927, when the last airmail routes were passed to private contractors.{{clear}}

==Variants==
[[File:De Havilland DH4 ExCC.jpg|thumb|right]]
[[File:Wright Radial Engine in a De Havilland DH-4B airplane (00910460 163).jpg|thumb|right|Wright Radial Engine (R-1) fitted to a De Havilland DH-4B airframe.]]

===UK variants===
* '''DH.4''' : Two-seat day bomber biplane.
* '''DH.4A''' : Civil version. Built in the United Kingdom. Two passengers in glazed cabin behind pilot.
* '''DH.4R''' : Single seat racer - 450&nbsp;hp (3406&nbsp;kW) [[Napier Lion]] engine.

===Soviet variants===
* copy of DH.4 manufactured by [[Polikarpov]] in the former [[Dux Factory]] in the 1920s

===United States variants===
<ref name="Jackson p67">Jackson 1987, p. 67.</ref>
* '''DH-4''' : Two-seat day bomber biplane, built in the United States.
* '''DH-4A''' : Civil version, built in the United States.
* '''DH-4B''' : Rebuilt version of Liberty powered DH-4 for [[United States Army Air Service|U.S. Air Service]].  Pilot's cockpit relocated to behind fuel tank, adjacent to observer's cockpit.
*** '''DH-4B-1''' : Increased fuel capacity (110&nbsp;US gal/420&nbsp;L).
*** '''DH-4B-2''' : Trainer version.
*** '''DH-4B-3''' : Fitted with 135&nbsp;US gal (511&nbsp;L) fuel tank
*** '''DH-4B-4''' : Civil version
*** '''DH-4B-5''' : Experimental civil conversion with enclosed cabin.
** '''DH-4BD''' :Cropdusting version of DH-4B
** '''DH-4BG''' : Fitted with smokescreen generators
** '''DH-4BK''' : Night flying version
** '''DH-4BM''': Single seat version for communications
*** '''DH-4BM-1''' : Dual control version of BM
*** '''DH-4BM-2''' : Dual control version of BM
** '''DH-4-BP''' : Experimental photo reconnaissance version
*** '''DH-4-BP-1''' : BP converted for survey work
** '''DH-4BS''' : Testbed for supercharged Liberty
** '''DH-4BT''' : Dual control trainer
** '''DH-4BW''' : Testbed for Wright H engine
* '''DH-4C''' : 300&nbsp;hp (220&nbsp;kW) Packard engine
* '''DH-4L''' : Civil version
* '''DH-4M''' : Rebuilt version of DH-4 with steel tube fuselage.
* '''DH-4Amb''' : Ambulance.
* '''DH-4M-1''' - postwar version by Boeing (Model 16) with new fuselage, designated '''O2B-1''' by Navy
** '''DH-4M-1T''' - Dual control trainer conversion of DH-4M
** '''DH-4M-1K''' - target tug conversion
** '''O2B-2''' - cross-country and night flying conversion for Navy
* '''DH-4M-2''' - postwar version by Atlantic
* '''L.W.F. J-2''' - Twin-engine long range development of DH-4 (also known as '''Twin DH'''), powered by two 200&nbsp;hp (150&nbsp;kW) Hall-Scott-Liberty 6 engines and with wingspan of 52&nbsp;ft 6&nbsp;in (16.04&nbsp;m); 20 built for U.S. Post Office, 10 for U.S. Army.<ref>[http://www.aerofiles.com/_lo.html "American airplanes: Lo - Lu."] ''Aerofiles''. Retrieved: 10 May 2008.</ref><ref name="Swan military p202-203">Swanborough and Bowers 1963, pp. 202–203.</ref>

;[[Boeing Model 42|XCO-7]] : (Boeing Model 42) Two-seat observation version with Boeing designed wings, enlarged tailplane and divided landing gear.

;XCO-8
:Was a designation of one Atlantic DH.4M-2 fitted with Loening COA-1 wings and powered by a Liberty 12A engine.

==Operators==

===Civil operators===
;{{ARG}}
*[[The River Plate Aviation Co. Ltd.]]
;{{AUS}}
*[[Qantas|QANTAS]]
;{{BEL}}
*[[Sabena|SNETA]]
;{{GBR}}:
*[[Aircraft Transport and Travel|Aircraft Transport and Travel Limited]]
*[[Handley Page Transport]]
*[[Imperial Airways]]
*[[Instone Air Line]]
;{{USA}}
*[[United States Postal Service|U.S. Post Office]]

=== Military operators ===
;{{BEL}}
*[[Belgian Air Force|Aviation Militaire Belge]]
;{{flag|Canada|1921}}
*[[Canadian Air Force (1918-1920)]]
*[[Royal Canadian Air Force]]
;{{CHL}}
*[[Chilean Air Force]]
;{{CUB}}
*[[Cuban Air Force]] - American built DH-4s
;{{flag|Greece|old}}
*[[Hellenic Air Force]]
*[[Hellenic Navy]]
;{{flag|Iran|1925}}
*[[Imperial Iranian Air Force]]
;{{flag|Mexico|1923}}
*[[Mexican Air Force|Fuerza Aérea Mexicana]]
;{{NIC}}
*[[Nicaraguan Air Force]] - The Nicaragua Air Force received seven DH-4Bs.<ref>Klaus, Erich. [http://www.aeroflight.co.uk/waf/americas/nicaragua/Nicaragua-af-DH4.htm "Nicaragua Air Force Aircraft Types: de Havilland (Airco) DH.-4B."] ''Aeroflight'', 1 June 2003. Retrieved: 10 May 2008.</ref>
;{{NZL}}
*The [[New Zealand Permanent Air Force]] operated two aircraft from 1919 to 1929. It was used by the NZPAF as an advanced training aircraft. The DH.4 has the distinction of being the first aircraft to fly over [[Mount Cook]] on 8 September 1920. It also set a New Zealand altitude record of 21,000&nbsp;ft (6,400&nbsp;m) on 27 November 1919.
;{{flag|South Africa|1912}}
*[[South African Air Force]]
;{{URS}}
*[[Soviet Air Force]]
;{{flagicon|Spain|1785}} [[Kingdom of Spain]]
*[[Spanish Air Force]]
;{{TUR}}
*[[Turkish Air Force]]
;{{GBR}}
*[[Royal Flying Corps]]
*[[Royal Air Force]]
*[[Royal Naval Air Service]]
;{{flag|United States|1912}}
*[[United States Army Air Service]]
**[[8th Aero Squadron]]
**[[11th Aero Squadron]]
**[[20th Aero Squadron]]
**[[50th Aero Squadron]]
**[[91st Aero Squadron]]
*[[United States Navy]]
*[[United States Marine Corps]]

==Surviving aircraft==
[[File:Airco DH-4 NMUSAF.jpg|thumb|The DH-4B on display at the [[National Museum of the United States Air Force]].]]
*21959{{citation needed|date=May 2016}} - The prototype American-built DH-4 is on display at the [[National Air and Space Museum]] in [[Washington D.C.]]<ref>{{cite web|title=De Havilland DH-4|url=http://airandspace.si.edu/collections/artifact.cfm?object=nasm_A19190051000|website=Smithsonian National Air and Space Museum|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4B is on display in the main atrium of the [[National Postal Museum]] in [[Washington, D.C.]]<ref>{{cite web|title=deHavilland DH-4|url=http://postalmuseum.si.edu/exhibits/current/airmail-in-america/the-airplanes/dehavilland-dh-4.html|website=Smithsonian National Postal Museum|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4 is on display at the [[National Museum of the Marine Corps]] in [[Triangle, Virginia]].<ref>{{cite web|title=De Haviland DH-4|url=http://www.virtualusmcmuseum.com/Preview_3.asp|website=National Museum of the Marine Corps Virtual Experience|accessdate=10 May 2016}}</ref> It was restored by Century Aviation.<ref>{{cite web|title=1918 DeHavilland DH-4|url=http://century-aviation.com/essential_grid/1918-dehavilland-dh-4|website=Century Aviation|publisher=Century Aviation|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4B is under restoration by Century Aviation for the [[Pearson Air Museum]] in [[Vancouver, Washington]].<ref>{{cite news|last1=Cromwell|first1=Bob|title=National Park Service Announces the Purchase of a DeHavilland DH-4 Biplane for Exhibit at Pearson Air Museum|url=https://www.nps.gov/fova/learn/news/dehavillandexhibit.htm|accessdate=10 May 2016|work=National Park Service|date=15 October 2014}}</ref><ref>{{cite web|title=1919 DeHavilland DH-4B|url=http://century-aviation.com/essential_grid/1919-dehavilland-dh-4b|website=Century Aviation|publisher=Century Aviation|accessdate=13 August 2016}}</ref>
*Unknown ID - A DH-4B is on display at the [[National Museum of the United States Air Force]] in [[Dayton, Ohio]].<ref>{{cite web|title=De Havilland DH-4|url=http://www.nationalmuseum.af.mil/Visit/MuseumExhibits/FactSheets/Display/tabid/509/Article/197397/de-havilland-dh-4.aspx|website=National Museum of the US Air Force|accessdate=10 May 2016|date=7 April 2015}}</ref> It was restored by Century Aviation.<ref>{{cite web|title=DeHavilland DH-4B|url=http://century-aviation.com/essential_grid/dehavilland-dh-4b|website=Century Aviation|publisher=Century Aviation|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4B is on display at [[Fantasy of Flight]] in [[Polk City, Florida]]. It was restored by Century Aviation.<ref>{{cite web|title=1918 DeHavilland DH-4 Mail Plane|url=http://century-aviation.com/essential_grid/1918-dehavilland-dh-4-mail-plane|website=Century Aviation|publisher=Century Aviation|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4B is under restoration by Century Aviation for Fantasy of Flight in Polk City, Florida. It will be powered by a rebuilt Liberty V-12.<ref>{{cite web|title=1918 De Havilland DH-4 (U.S. Army Air Corps)|url=http://century-aviation.com/essential_grid/1918-de-havilland-dh-4-u-s-army-air-corps|website=Century Aviation|publisher=Century Aviation|accessdate=10 May 2016}}</ref>
*Unknown ID - A DH-4M-1 is on display at the [[Evergreen Aviation and Space Museum]] in [[McMinnville, Oregon]].<ref>{{cite web|title=Airframe Dossier - Airco DH-4, c/r N3258|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=70390|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=10 May 2016}}</ref> This aircraft was previously owned by [[Paul Mantz]].<ref>{{cite book|last1=Skaarup|first1=Harold|title=Washington Warbird Survivors 2002: A Handbook on Where to Find Them|date=1 February 2002|publisher=iUniverse|isbn=9780595216932|url=https://books.google.com/books?id=qHvpxhDOzJAC|accessdate=11 May 2016}}</ref>
*Unknown ID - A DH-4M-2A is on display at the [[Historic Aircraft Restoration Museum]] in [[Maryland Heights, Missouri]].<ref>{{cite web|title=Museum Hangar 4|url=http://www.historicaircraftrestorationmuseum.org/museumhangar4.html|website=Historic Aircraft Restoration Museum|publisher=Historic Aircraft Restoration Museum|accessdate=11 May 2016}}</ref><ref>{{cite web|title=Airframe Dossier - Airco DH-4M-2A, c/r N3249H|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=141812|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=10 May 2016}}</ref><ref>{{cite web|title=DH-4M2|url=http://www.peckaeroplanerestoration.com/projects/dh-4-mail-plane|website=Peck Aeroplane Restoration|publisher=Peck Aeroplane Restoration|accessdate=3 June 2016}}</ref>
*Unknown ID - A DH-4 is on display at the Omaka Aviation Heritage Centre in [[Blenheim, New Zealand]].<ref>{{cite web|title=The Exhibits|url=http://www.omaka.org.nz/exhibits.htm|website=Omaka Aviation Heritage Centre|publisher=New Zealand Aviation Museum Trust|accessdate=10 May 2016}}</ref> This aircraft was previously on display at the [[Crawford Auto-Aviation Museum]] in [[Cleveland, Ohio]] and was at one point loaned to the United States Air Force Museum.<ref>{{cite news|last1=Mazzolini|first1=Joan|title=Western Reserve Historical Society has sold or put up for sale many items from its collection|url=http://blog.cleveland.com/pdextra/2010/03/the_western_reserve_historical.html|accessdate=11 May 2016|work=Cleveland.com|publisher=Advance Ohio|date=7 March 2010}}</ref>
*Replica - A DH-4 replica is on display at the [[Museo del Aire (Madrid)|Museo del Aire]] in [[Madrid, Spain]].<ref>{{cite web|title=Fotografía De Havilland DH-4|url=http://www.ejercitodelaire.mde.es/ea/pag?idDoc=CC080F0B0240A9D7C125747A002415EA&idImg=DA4CA2CBC2AED670C125747A0024C05F|website=Ejército del Aire|publisher=Ejército del Aire|accessdate=10 May 2016}}</ref><ref>{{cite web|title=Airframe Dossier - Airco DH-4|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=2981|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=10 May 2016}}</ref>
*Replica - A DH.4 replica is on display at The Vintage Aviator Limited in [[New Zealand]]. It was built by Century Aviation.<ref>{{cite web|title=1917 Airco DH.4 (British Military)|url=http://century-aviation.com/essential_grid/1917-airco-dh-4-british-military|website=Century Aviation|publisher=Century Aviation|accessdate=10 May 2016}}</ref>

==Specifications (DH.4 - Eagle VIII engine)==
{{Aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=''The British Bomber since 1914''<ref name="mason">Mason 1994, pp. 66–69.</ref>
|crew=two
|capacity=
|payload main=
|payload alt=
|length main= 30 ft 8 in
|length alt= 9.35 m
|span main= 43 feet 4 in
|span alt= 13.21 m
|height main= 11 ft 
|height alt= 3.35 m
|area main= 434 ft²
|area alt= 40 m²
|airfoil=
|empty weight main= 2,387 lb
|empty weight alt= 1,085 kg
|loaded weight main= 3,472 lb
|loaded weight alt= 1,578 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)= [[Rolls-Royce Eagle]] VII 
|type of prop=inline liquid cooled piston 
|number of props= 1
|power main= 375 hp{{#tag:ref|230 hp/171.5 kW for [[BHP Puma]]|group=N}}
|power alt= 289 kW
|power original=
|max speed main= 143 mph {{#tag:ref|106 mph for Puma engine variants|group=N}}
|max speed alt= 230 km/h
|max speed more= at sea level
|cruise speed main= 
|cruise speed alt= 
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 470 mi
|range alt= 770 km
|ceiling main= 22,000 ft
|ceiling alt= 6,700 m
|climb rate main= 1,000 ft/min
|climb rate alt= 305 m/min
|loading main= 8 lb/ft²
|loading alt= 39.5 kg/m²
|power/mass main= 0.108 hp/lb
|power/mass alt=0.266 kW/kg
|more performance=
* '''Climb to 10,000 ft:''' 9 min
|endurance= 3¾ hr
|guns=Forward-firing .303 in (7.7 mm) [[Vickers machine gun]], [[Lewis gun]] on [[Scarff ring]] at rear
|bombs= 460 lb (210 kg) of bombs
|avionics=
}}

==See also==
{{aircontent
|related= 
*[[Airco DH.9]]
*[[Airco DH.9A]]
|similar aircraft=
}}

==References==
;Notes
{{reflist|group=N}}
; Citations
{{Reflist|colwidth=30em}}

;Bibliography
{{refbegin}}
* Angelucci, Enzo, ed. ''World Encyclopedia of Military Aircraft''. London: Jane's, 1991. ISBN 0-7106-0148-4.
* Bruce, J.M. [http://www.flightglobal.com/pdfarchive/view/1952/1952%20-%203038.html "The De Havilland D.H.4."] [[Flight International|''Flight'']], 17 October 1952, pp.&nbsp;506–510.
* Bowers, Peter M. ''Boeing Aircraft since 1916''. London: Putnam, Second edition, 1989. ISBN 0-85177-804-6.
* Jackson, A.J. ''British Civil Aircraft since 1919: Volume 2''. London: Putnam, Second edition, 1973. ISBN 0-370-10010-7.
* Jackson, A.J. ''De Havilland Aircraft since 1909''. London: Putnam, Third edition, 1987. ISBN 0-85177-802-X.
* Mason, Francis K. ''The British Bomber since 1914''. London: Putnam Aeronautical Books, 1994. ISBN 0-85177-861-5.
* Maurer, Maurer, ed. ''The U.S. Air Service in World War I: Volume IV Postwar Review''. Washington, D.C.: The Office of Air Force History Headquarters USAF, 1979.
* Sturtivant, Ray and Gordon Page. ''The D.H.4/D.H.9 File''. Tonbridge, Kent, UK: Air-Britain (Historians) Ltd., 2000. ISBN 0-85130-274-2.
* Swanborough, F.G. and Peter M. Bowers. ''United States Military Aircraft since 1909''. London: Putnam, 1963.
* Swanborough Gordon and Peter M. Bowers. ''United States Naval Aircraft since 1911''. London: Putnam, Second edition, 1976. ISBN 0-370-10054-9.
* Thetford, Owen. ''British Naval Aircraft since 1912''. London: Putnam, Fourth edition, 1978. ISBN 0-370-30021-1.
* ''United States Air Force Museum Guidebook''. Wright-Patterson AFB, Ohio: Air Force Museum Foundation, 1975.
* Williams, George K. ''Biplanes and Bombsights: British Bombing in World War I''. Maxwell Air Force Base, Alabama: Air University Press, 1999. ISBN 1-4102-0012-4.
{{refend}}

==External links==
{{commons category-inline|Airco DH.4}}

{{De Havilland aircraft}}
{{Boeing model numbers}}
{{USAAS observation aircraft}}
{{USN observation aircraft}}

{{DEFAULTSORT:Airco DH.004}}
[[Category:Airco aircraft|DH.004]]
[[Category:British bomber aircraft 1910–1919]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Boeing aircraft]]
[[Category:Atlantic Aircraft aircraft]]
[[Category:Military aircraft of World War I]]