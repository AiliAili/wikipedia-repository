{{Use mdy dates|date=February 2012}}
{{Good article}}
{{Infobox hurricane season
|Track=1970 North Indian Ocean cyclone season summary.jpg
|Basin=NIO
|Year=1970
|First storm formed=May 2, 1970
|Last storm dissipated=November 29, 1970
|Strongest storm name=[[1970 Bhola Cyclone|Bhola Cyclone]]
|Strongest storm winds=100
|Strongest storm pressure=966
|Total disturbances=15
|Total depressions=11
|Total storms=7
|Total intense=3
|Fatalities=300,000–500,000 (Deadliest Tropical Cyclone Season Recorded Worldwide)
|Damages=86.4
|five seasons=[[Pre-1980 North Indian Ocean cyclone seasons]]
|Atlantic season=1970 Atlantic hurricane season
|East Pacific season=1970 Pacific hurricane season
|West Pacific season=1970 Pacific typhoon season
}}

The '''1970 North Indian Ocean cyclone season''' had no bounds, but [[tropical cyclone]]s in the North Indian Ocean tend to form between April and December, with peaks in May and November. The 1970 season saw a total of seven cyclonic storms, of which three developed into severe cyclonic storms. The [[Bay of Bengal]] was more active than the [[Arabian Sea]] during 1970, with all of the three severe cyclonic storms in the season forming there. Unusually, none of the storms in the Arabian Sea made [[landfall (meteorology)|landfall]] this year. The most significant storm of the season was the [[1970 Bhola cyclone|Bhola cyclone]], which formed in the Bay of Bengal and hit [[Bangladesh]] on November 12. The storm killed at least 300,000 and possibly even over half a million people, making it the deadliest tropical cyclone in recorded history.

__TOC__
{{Clear|right}}

==Systems==
===Very Severe Cyclonic Storm One===
{{Infobox Hurricane Small
|Basin=NIO
|Image=03B May 4 1970 0536Z.png
|Track=1970 Indian cyclone 1 track.png
|Formed=May 2
|Dissipated=May 7
|3-min winds=80
|Pressure=975
}}
A low pressure area that developed over the southern [[Andaman Sea]] late in April moved north into the [[Bay of Bengal]] becoming the first depression of the year on May 2. The depression intensified under the influence of a high-level anticyclone and became a cyclonic storm the next day. The storm then turned to the northeast and strengthened into a severe cyclonic storm on May 4. Soon after this, it peaked with winds of 150&nbsp;km/h (90&nbsp;mph) and a well-defined [[eye (cyclone)|eye]] formed. The storm made its [[landfall (meteorology)|landfall]] near [[Cox's Bazar]] in easternmost [[East Pakistan]] (now [[Bangladesh]]) early in the morning of May 7 and dissipated over western [[Burma]] that evening.

The cyclone brought widespread rain to the [[Andaman Islands|Andaman]] and [[Nicobar Islands]] before it moved clear of them on May 3. Effects in East Pakistan are unknown, but [[Akyab]] in western Burma reported winds of 55&nbsp;km/h (35&nbsp;mph) as the cyclone approached land.<ref name="IWR-1">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=4|title=Annual Summary&nbsp;—Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=4}}</ref>
{{Clear}}

===Depression Two===
{{Infobox Hurricane Small
|Basin=NIO
|Image=02B May 23 1970 0522Z.png
|Track=1970 Indian cyclone 2 track.png
|Formed=May 23
|Dissipated=May 24
|3-min winds=25
}}
A depression formed in the northeast of the Bay of Bengal on during the morning of May 23 near the [[East Pakistan]] coast. It moved towards the coast and made landfall to the south of [[Cox's Bazar]] that night, before dissipating over southern [[Assam]] the next day.<ref name="IWR-1"/> Any effects on land are unknown.
{{Clear}}

===Severe Cyclonic Storm Three===
{{Infobox Hurricane Small
|Basin=NIO
|Image=01A May 29 1970 0609Z.png
|Track=1970 Indian cyclone 3 track.png
|Formed=May 28
|Dissipated=June 2
|3-min winds=50
|Pressure=986
}}
A low pressure area developed off the [[Karnataka]]-[[Goa]] coastline in the [[Arabian Sea]] on May 27 and developed into a depression the next day. The system developed further into a cyclonic storm on May 29 as it moved to the north. The storm reached its peak with 95&nbsp;km/h (60&nbsp;mph) soon after this as it turned to the west, but soon degenerated into a deep depression over the northern Arabian Sea on May 31. The depression continued to move west, weakening to a remnant low shortly as it made landfall on the [[Arabian Peninsula]] on June 2. The remnant low dissipated over southern [[Saudi Arabia]] the next day.<ref name="IWR-3">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=12|title=Annual Summary&nbsp;—Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=12}}</ref>

The cyclone itself had minimal effects on land, but the monsoon advanced into western India in late May in association with the system. As the cyclone moved to the west away from the subcontinent, the incursion of moist air from the Arabian Sea persisted over northwestern India for the first three days of June. This brought moderate rainfall to regions of [[Gujarat]], Rajasthan and western [[Madhya Pradesh]].<ref name="IWR-3"/>
{{Clear}}

===Cyclonic Storm Four===
{{Infobox Hurricane Small
|Basin=NIO
|Image=04B Jun 10 1970 0554Z.png
|Track=1970 Indian cyclone 4 track.png
|Formed=June 7
|Dissipated=June 11
|Prewinds=≥
|3-min winds=35
|Pressure=986
}}
A low pressure area developed in the northern Bay of Bengal on June 6 and developed into a depression the next day as it drifted to the north, moving over south of the [[Ganges Delta]]. The depression reversed its course overland, reemerging into the Bay of Bengal on June 8. Once over water, the depression intensified into a cyclonic storm on the morning of June 9. The cyclone's motion shifted to the northwest and it made a second landfall near [[Balasore]] in northern [[Orissa, India|Orissa]] that night. The cyclone quickly weakened to a deep depression overland and tracked to the west over central India, where it degenerated into a broad area of low pressure on June 11.<ref name="IWR-4">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=4|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=4–5}}</ref>

Sustained winds of 65&nbsp;km/h (40&nbsp;mph) were recorded at Sandheads on June 9, while the cyclone was at its peak offshore. High levels of rain affected much of Orissa and [[West Bengal]], with over 100&nbsp;mm (4&nbsp;inches) falling over large areas of both states. As the storm dissipated overland, it brought severe rain to [[Vidarbha]] and southern [[Madhya Pradesh]]. The highest recorded rainfall was at [[Khandwa]] were 280&nbsp;mm (11&nbsp;inches) fell on June 13 alone. The rains led to localised flooding in parts of Vidarbha and disrupted road transport in the region.<ref name="IWR-4"/>
{{Clear}}

===Deep Depression Five===
{{Infobox Hurricane Small
|Basin=NIO
|Image=1970 Indian cyclone 5 track.png
|Formed=June 29
|Dissipated=July 3
|Prewinds=≥
|3-min winds=28
}}
A low pressure area that had developed over the northern [[Bay of Bengal]] the previous day concentrated into a depression early on June 29. The depression intensified as it moved to the northwest, becoming a deep depression shortly before it crossed the [[Orissa, India|Orissa]] coast. After landfall the storm continued to move to the northwest, weakening to a depression again on July 2 over northeast [[Madhya Pradesh]]. It degenerated into a remnant low over central [[Uttar Pradesh]] during the evening on July 3.<ref name="IWR-5">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=5|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=5}}</ref>

The depression brought intense monsoon conditions to Orissa, Madhya Pradesh and [[Vidarbha]] during its life. Over 210&nbsp;mm (8&nbsp;inches) of rain and winds of 85&nbsp;km/h (50&nbsp;mph) were recorded in [[Paradip]] as the depression made landfall on June 30. Inland, the heavy rains caused some rivers in Orissa to overflow and flooded rice paddies in [[Cuttack District]]. Further west, the [[Wainganga River]] overflowed its banks in places. Road transport in Madhya Pradesh was disrupted and there was damage to crops in the [[Raipur]] area. Raipur recorded the highest 24-hour rains associated with the depression, with 230&nbsp;mm (9&nbsp;inches) falling there on July 2.<ref name="IWR-5"/>
{{Clear}}

===Deep Depression Six===
{{Infobox Hurricane Small
|Basin=NIO
|Image=1970 Indian cyclone 6 track.png
|Formed=July 6
|Dissipated=July 8
|3-min winds=30
|Prepressure=≤
|Pressure=992
}}
A low pressure area moved to the west off the [[Burma|Burmese]] coast on July 5 and developed into a depression the next day in the north central [[Bay of Bengal]]. The depression intensified as it moved to the northwest, becoming a deep depression on July 7. The system made landfall on the [[Orissa, India|Orissa]] coast during the morning of July 8 and rapidly degenerated into a broad area of low pressure overland. The remnant low moved north over western [[Bihar]] and it persisted there until July 12 when it was absorbed by the [[monsoon trough]].<ref name="IWR-6">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=5|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=5–6}}</ref>

Winds of 55&nbsp;km/h (30&nbsp;mph) were recorded offshore at Sandheads when the system was at its peak. The depression brought isolated heavy rains to many places throughout [[East India]], with rains in excess of 100&nbsp;mm (4&nbsp;inches) falling every day from the depression's landfall until the absorption of the remnant low.<ref name="IWR-6"/>
{{Clear}}

===Depression Seven===
{{Infobox Hurricane Small
|Basin=NIO
|Image=07B Aug 17 1970 0541Z.png
|Track=1970 Indian cyclone 7 track.png
|Formed=August 17
|Dissipated=August 19
|3-min winds=25
}}
A low pressure area that lay over [[Burma]] on August 15, moved to the west across the [[Bay of Bengal]] and developed into a depression on the evening of August 17 when it was {{convert|100|km|mi|abbr=on}} southeast of [[Gopalpur-on-Sea]]. The depression moved to the northwest and made landfall near Gopalpur-on-Sea and turned to the west overland. The depression had weakened into a remnant low by August 20, when it was over western [[Madhya Pradesh]].<ref name="IWR-7">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=6|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=6}}</ref>

The depression and associated weather brought widespread rain to much of south and central India, with heavy rain falling in some locations. The heavy rain in [[Maharashtra]] disrupted road transport in many areas, especially in the east of the state. The [[Godavari River]] inundated parts of [[Bhadrachalam]] and many villages in regions of [[Andhra Pradesh]]. The rains also damaged rice paddies and disrupted transport in [[Telangana]]. Rains associated with the depression reached as far west as [[Bombay]] with 200&nbsp;mm (8&nbsp;inches) falling there on August 19.<ref name="IWR-7"/>
{{Clear}}

===Cyclonic Storm Eight===
{{Infobox Hurricane Small
|Basin=NIO
|Image=10B Sept 11 1970 0757Z.png
|Track=1970 Indian cyclone 8 track.png
|Formed=September 2
|Dissipated=September 13
|3-min winds=34
}}
A low pressure area that was centred over [[West Bengal]] on August 31 concentrated into a depression on September 2 when it was {{convert|50|km|mi|abbr=on}} to the east of [[Midnapore]]. It intensified as it moved to the west, becoming a deep depression the next day, about {{convert|50|km|mi|abbr=on}} east of [[Ranchi]]. The system continued move west across India, before weakening into a depression on September 7 near [[Ahmedabad]], Gujarat. The depression then turned north and entered southwest [[Rajasthan]].<ref name="IWR-8">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=7|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=7}}</ref> On September 8, the system turned to the southwest and emerged into the [[Arabian Sea]] the following evening. Over water it intensified again, becoming a cyclonic storm by the evening of September 10. The cyclone developed a short-lived [[eye (cyclone)|eye]] on September 11 as it drifted slowly to the west, before degenerating into a broad area of low pressure as it neared the [[Oman]] coast on September 14.<ref name="IWR-3"/>

This system brought widespread rains to a wide swathe of India during its existence. Some heavy rains in West Bengal flooded vast areas of many districts and resulted in some fatalities, whilst in parts of neighbouring [[Orissa, India|Orissa]] the floodwaters damaged fields. 390&nbsp;mm (15&nbsp;inches) of rain fell on [[Bardhaman]] over two days as the depression passed overhead. Several thousand people were made homeless in West Bengal. Both the [[Narmada River|Narmada]] and [[Tapti River]]s inundated parts of [[Madhya Pradesh]] and Gujarat. The floods in Gujarat took many lives and caused severe damage to crops and other properties. Between 300 and 400&nbsp;people were washed away in two villages in [[Bharuch district]].<ref name="IWR-8"/> 260&nbsp;mm (10&nbsp;inches) of rain fell on [[Surat]] on July 7<ref name="IWR-8"/> and a further 80&nbsp;mm (3&nbsp;inches) on [[Kutch District]] as the cyclone moved out to sea.<ref name="IWR-3"/>
{{Clear}}

===Deep Depression Nine===
{{Infobox Hurricane Small
|Basin=NIO
|Image=09B Sept 11 1970 0610Z.png
|Track=1970 Indian cyclone 9 track.png
|Formed=September 8
|Dissipated=September 18
|3-min winds=30
|Pressure=990
}}
A low pressure area moved from [[Burma]] to the [[Bay of Bengal]] on September 8 where it developed into a depression. The depression strengthened as it moved to the northwest and became a deep depression the next day, when it was {{convert|150|km|mi|abbr=on}} southeast of [[Calcutta]]. The system crossed the [[West Bengal]] coast later and moved to the northwest across the [[Chota Nagpur Plateau]]. The depression stalled over [[Uttar Pradesh]] on September 12 and remained near [[Lucknow]] until September 14. The depression then turned to the east and weakened to a remnant low over northern [[Bihar]] on September 18.<ref name="IWR-9">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=7|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=7–8}}</ref>

The depression brought widespread rain to West Bengal, [[Orissa, India|Orissa]], Bihar, Uttar Pradesh and [[Madhya Pradesh]] during its lifespan, with rains of 140&nbsp;mm (5.5&nbsp;inches) as far west as Lucknow. The rains in Uttar Pradesh caused severe property damage and flooding, with some 150&nbsp;people losing their lives in the state. The [[Ganges]] flooded in northern Bihar, inundating parts of [[Munger District]], whilst the [[Gandaki River]] flooded parts of [[Motihari]]. The rains worsened floods in south Bengal, increasing the death toll from the floods there to 80 and affecting 8&nbsp;million people, with considerable damage done to crops and housing. [[Jaleswar]] in Orissa was entirely flooded.<ref name="IWR-9"/>
{{Clear}}

===Depression Ten===
{{Infobox Hurricane Small
|Basin=NIO
|Image=08B Sept 21 1970 0538Z.png
|Track=1970 Indian cyclone 10 track.png
|Formed=September 21
|Dissipated=September 23
|3-min winds=25
}}
A well-defined low pressure area formed over the west central [[Bay of Bengal]] on September 20 and developed into a depression the next morning about {{convert|100|km|mi|abbr=on}} south-east of [[Visakhapatnam]]. The depression then moved overland and after crossing [[Vidarbha]] weakened into a remnant low over [[Gujarat]] on September 23.<ref name="IWR-10">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=8|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=8–9}}</ref>

The depression brought widespread rain to northern [[South India]] as it passed over the country and its remnants brought scattered rain to Gujarat over the following week. Over a period of four hours, heavy rain fell on [[Hyderabad, Andhra Pradesh|Hyderabad]] and [[Secunderabad]], destroying many houses in the two cities. About 130&nbsp;mm (5&nbsp;inches) of rain fell on [[Bombay]] as the system dissipated. The heavy rains claimed about 75 lives.<ref name="IWR-10"/>
{{Clear}}

===Depression Eleven===
{{Infobox Hurricane Small
|Basin=NIO
|Image=1970 Indian cyclone 11 track.png
|Formed=October 11
|Dissipated=October 13
|3-min winds=25
}}
A low pressure area that developed in the Bay of Bengal passed over the southern [[Indian subcontinent]] and developed into a depression in the Arabian Sea off the southern [[Maharashtra]] coast on October 11. The depression did not develop as it drifted west, and it degenerated into an area of low pressure as it was approaching the [[Arabian Peninsula]]. The depression brought widespread rains to southern Maharashtra and [[Karnataka]] states, with 40&nbsp;mm (1.5&nbsp;in) of rain recorded in [[Karwar]]. Scattered heavy rain was also reported in the [[Laccadives]].<ref name="IWR-11">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=12|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=12–13}}</ref>
{{Clear}}

===Very Severe Cyclonic Storm Twelve===
{{Infobox Hurricane Small
|Basin=NIO
|Image=12B Oct 21 1970 0554Z.png
|Track=1970 Indian cyclone 12 track.png
|Formed=October 18
|Dissipated=October 24
|3-min winds=70
|Pressure=980
}}
A depression formed on October 18 in the central [[Bay of Bengal]] and moved to the north. It gradually intensified and turned to the northeast, becoming a cyclonic storm on October 20. The storm became the second severe cyclonic storm of the season the following day, and turned onto a more northerly track towards the [[Ganges Delta]]. The storm peaked with winds of 130&nbsp;km/h (80&nbsp;mph) before it made [[landfall (meteorology)|landfall]] near the [[West Bengal]]-[[East Pakistan]] border during the morning of October 23. The storm then crossed over East Pakistan, before dissipating over southern [[Assam]] on October 24.<ref name="IWR-12">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=9|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=9–10}}</ref>

This cyclone brought widespread rain to [[Tamil Nadu]] as it formed and to the [[Andaman and Nicobar Islands]] as it moved over the Bay of Bengal. After its landfall it brought severe rains to West Bengal, Assam and East Pakistan. The highest recorded rainfall in India was at [[Shillong]] where 220&nbsp;mm (8.6&nbsp;inches) fell on October 24. A gust of 105&nbsp;km/h (65&nbsp;mph) was recorded in [[Calcutta]] as the storm passed by to the east, where it caused the failure of the power supply.<ref name="IWR-12"/>

The cyclone claimed between 200 and 300 lives in East Pakistan,<ref name="IWR-12"/> with the worst of the damage occurring in [[Khulna District]]. Over 200 villages were destroyed in the district leaving several thousand people homeless, and there was extensive damage to crops.<ref name="OctoberNews">{{cite news|author=Staff writer |publisher=Reuters |title=125 Die In Hurricane In Pakistan |url=http://thehurricanearchive.com/cache/21132106.pdf |format=PDF |date=October 27, 1970 |accessdate=April 15, 2007 |work=[[The Daily Gleaner|Daily Gleaner]] }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>
{{Clear}}

===Extremely Severe Cyclonic Storm Thirteen===
{{Infobox Hurricane Small
|Basin=NIO
|Image=BayofBengalTCNov1219700956UTCITOS1.gif
|Track=1970 Bhola cyclone track.png
|Formed=November 8
|Dissipated=November 13
|3-min winds=100
|1-min winds=112
|Pressure=960
}}
{{Main|1970 Bhola cyclone}}
On the morning of November 8 a depression formed in the south-central Bay of Bengal. It moved very slowly to the north, becoming a cyclonic storm the next day. It continued to intensify as it approached the head of the Bay, becoming the third severe cyclonic storm of the season on November 11. That evening it reached its peak with winds of 185&nbsp;km/h (115&nbsp;mph) with a well-developed [[eye (cyclone)|eye]] and became the strongest storm of the season. It then made [[landfall (meteorology)|landfall]] on the coast of [[East Pakistan]] during the night of November 12. The cyclone weakened rapidly as it moved inland and dissipated the next day over south [[Assam]].<ref name="IWR-13">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=10|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|pages=10–11}}</ref>

This cyclone brought a devastating [[storm surge]] of up to 10&nbsp;metres (33&nbsp;ft) high to the [[Ganges Delta]]. Largely as a result of this surge somewhere between 300,000 and 500,000&nbsp;people lost their lives, making this storm the deadliest [[tropical cyclone]] ever recorded and one of the deadliest [[natural disaster]]s of modern times.<ref name="IWM">{{cite web|url=http://www.iwmbd.org/html/PUBS/publications/P024.PDF|title=Cyclonic Storm Surge Modelling for Design of Coastal Polder|author=Kabir, M. M.|author2=Saha B. C.|author3= Hye, J. M. A.|accessdate=April 15, 2007|format=PDF|publisher=[[Institute of Water Modelling]]|archiveurl = https://web.archive.org/web/20070622000638/http://www.iwmbd.org/html/PUBS/publications/P024.PDF |archivedate = June 22, 2007|deadurl=yes}}</ref> The total damage from the storm exceeded $85&nbsp;million (1970 USD, $480&nbsp;million 2008&nbsp;USD) and much of the agricultural and fishing capacity of the region was wiped out. In total some 3.6&nbsp;million people suffered direct effects from this cyclone.<ref name="EM-DAT">{{cite web|author=EM-DAT: the International Disaster Database |year=2007 |title=Disaster List for Bangladesh |publisher=Centre for Research on the Epidemiology of Disasters |accessdate=April 15, 2007 |url=http://www.em-dat.net/index.htm |archiveurl=https://web.archive.org/web/20070513082359/http://www.em-dat.net/index.htm |archivedate=May 13, 2007 |deadurl=yes |df=mdy }}</ref>

The [[Pakistan]]i government was severely criticised for its handling of the relief operations following the storm, both by local political leaders in East Pakistan and in the international media. In the December national elections, the opposition [[Awami League]] gained a [[landslide victory]], gaining 160 of the 162 East Pakistani seats. Continuing unrest between East Pakistanis and the central government trigged the [[Bangladesh Liberation War]], which concluded with the creation of the state of [[Bangladesh]]. This is one of the first times that a natural event helped to trigger a civil war.<ref name="USAID">{{cite web|url=http://www.usaid.gov/our_work/humanitarian_assistance/disaster_assistance/publications/ofda_cjanalysis_02_21-2005.pdf |accessdate=April 15, 2007 |format=PDF |date=February 21, 2005 |title=A Critical Juncture Analysis, 1964–2003 |last=Olson |first=Richard |publisher=[[USAID]] |archiveurl=http://www.webcitation.org/5oSr3LMgA?url=http%3A%2F%2Fwww.usaid.gov%2Four_work%2Fhumanitarian_assistance%2Fdisaster_assistance%2Fpublications%2Fofda_cjanalysis_02_21-2005.pdf |archivedate=March 24, 2010 |deadurl=yes |df=mdy }}</ref>
{{Clear}}

===Deep Depression Fourteen===
{{Infobox Hurricane Small
|Basin=NIO
|Image=14B Nov 20 1970 0607Z.png
|Track=1970 Indian cyclone 14 track.png
|Formed=November 19
|Dissipated=November 20
|Prewinds=≥
|3-min winds=28
}}
A low pressure that had developed over the south [[Andaman Sea]] moved west across the southern [[Bay of Bengal]] and concentrated into a depression on the morning of November 19, when it was about {{convert|600|km|mi|abbr=on}} southeast of [[Madras]]. The system intensified further into a deep depression as it moved west-northwest and hit the [[Tamil Nadu]] the following day. The depression weakened into a remnant low over Tamil Nadu.<ref name="IWR-14">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=11|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=11}}</ref>

The depression brought widespread rain to much of Tamil Nadu, with heavy rain hitting the coastal regions. Low-lying coastal regions from [[Pondicherry (city)|Pondicherry]] to Atirampattinam. There was damage to crops and housing in [[Thanjavur District]] and the rains left thousands homeless in Madras. [[Cuddalore]] experienced exceptionally heavy rains with over 440&nbsp;mm (17&nbsp;inches) falling there, of which 310&nbsp;mm (12&nbsp;inches) fell on November 20 alone.<ref name="IWR-14"/>
{{Clear}}

===Cyclonic Storm Fifteen===
{{Infobox Hurricane Small
|Basin=NIO
|Image=13A Nov 27 1970 0757Z.png
|Track=1970 Indian cyclone 15 track.png
|Formed=November 22
|Dissipated=November 29
|3-min winds=40
}}
The remnant low of a [[#Deep Depression Fourteen|depression]] emerged into the [[Arabian Sea]] off [[Kerala]] late on November 21 and developed into a new depression the next day as it moved west. The depression moved further west, and gradually turned toward the southwest, intensifying as it did so. It became a cyclonic storm on November 28 to the southeast of [[Socotra]] and reached its peak with 75&nbsp;km/h (25&nbsp;mph) winds soon after. The cyclone continued to move west-southwestwards and rapidly weakened into a remnant low pressure area off the [[Somalia]] coast the next day, ending the season.<ref name="IWR-15">{{cite web|url=http://docs.lib.noaa.gov/rescue/cd024_pdf/005ED281.pdf#page=14|title=Annual Summary&nbsp;— Storms & Depressions|work=India Weather Review 1970|author=[[India Meteorological Department]]|year=1970|accessdate=April 15, 2007|format=PDF|page=14}}</ref>

The cyclone brought widespread rain to the [[Laccadives]] from November 22 to November 24, with 60&nbsp;mm (2.4&nbsp;inches) recorded on [[Amini, India|Amini]] on November 23.<ref name="IWR-15"/>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of North Indian Ocean cyclone seasons]]
* [[List of notable tropical cyclones]]
* [[1970 Atlantic hurricane season]]
* [[1970 Pacific hurricane season]]
* [[1970 Pacific typhoon season]]
* Southern Hemisphere tropical cyclone seasons: [[1969–70 Southern Hemisphere tropical cyclone season|1969–70]], [[1970–71 Southern Hemisphere tropical cyclone season|1970–71]]

==References==
{{reflist|30em}}

{{TC Decades|Year=1970|basin=North Indian Ocean|type=cyclone}}

{{DEFAULTSORT:1970 North Indian Ocean Cyclone Season}}
[[Category:Pre-1980 North Indian Ocean cyclone seasons]]
[[Category:Tropical cyclones in India]]
[[Category:1970 in India]]
[[Category:1970 North Indian Ocean cyclone season| ]]