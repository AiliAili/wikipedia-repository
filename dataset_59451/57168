{{Italic title}}
'''''Gnomon'''. Kritische Zeitschrift für die gesamte klassische Altertumswissenschaft'' (Gnomon: Critical Journal of the Entire Field of Scholarship on Classical Antiquity) is a German [[review journal]] covering the [[classics]].<ref>Bengtson. ''Introduction to Ancient History'' (1975), 169.</ref> It was established in 1925, first published by [[Verlag Weidmann]] and since 1949 by [[Verlag C. H. Beck]]. The journal appears in 8 issues each year and contains reviews, obituaries, and notices. Since 1950, odd-numbered volumes contain a "Bibliographic Supplement" of new books, dissertations and submitted journal articles, in addition to the regular contents. The [[editors-in-chief]] are [[Hans-Joachim Gehrke]], [[Martin Hose]], [[Henner von Hesberg]], [[Ernst Vogt]], and [[Paul Zanker]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Arts & Humanities Citation Index]] and [[Current Contents]]/Arts & Humanities.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-04-05}}</ref> In both 2007 and 2011 the journal received an "INT1"  ranking (internationally recognised with high visibility) from the [[European Reference Index for the Humanities]].<ref>[https://dbh.nsd.uib.no/publiseringskanaler/erih/searchForm?discipline=History&cat2007=All&cat2011=All Ranking] of History journals on ERIH Plus</ref>

== Gnomon Bibliographische Datenbank ==
In 1994 the ''Gnomon Bibliographische Datenbank'' (Gnomon bibliographic database) was established, containing data on ''Gnomon'' articles, [[monograph]]s, book chapters, journal articles from over 200 classics journals.<ref>Marcus Sehlmeyer, "Review" ''Historische Zeitschrift''
Bd. 261, H. 2 (Oct., 1995), pp. 486-488</ref> Entries were in German or English. Since the beginning of 1996 there has been a sister project, ''Gnomon Online'', which provides access to the database through the internet and is updated weekly.<ref>"Gnomon Online" ''Gnomon''
68. Bd., H. 2 (1996), p. 191; John Tamm ''Bryn Mawr Classical Review'' 97.4.9 [http://bmcr.brynmawr.edu/1997/97.04.09.html], Isabelle Torrance, ''Aeschylus: Oxford Bibliographies Online Research Guide'' (2010) 5</ref>

== List of editors ==
The following persons have been editor-in-chief of the journal:
{{columns-list|colwidth=30em|
* 1925–1933: [[Ludwig Curtius]]
* 1925–1933: [[Ludwig Deubner]]
* 1925–1933: [[Eduard Fraenkel]]
* 1925–1962: [[Matthias Gelzer]]
* 1925–1933: [[Ernst Hoffmann (historian)|Ernst Hoffmann]]
* 1925–1933: [[Werner Jaeger]]
* 1925–1933: [[Walther Kranz]]
* 1925–1933: [[Karl Meister]]
* 1925–1933: [[Peter von der Mühll]]
* 1925–1933: [[Karl Reinhardt (philologist)|Karl Reinhardt]]
* 1925–1943: [[Gerhart Rodenwaldt]]
* 1925–1933: [[Wilhelm Schubart]]
* 1925–1933: [[Wilhelm Schulze (linguist)|Wilhelm Schulze]]
* 1925–1933: [[Eduard Schwartz]]
* 1925–1933: [[Johannes Stroux]]
* 1925–1933: [[Wilhelm Weber (historian)|Wilhelm Weber]]
* 1930–1943: [[Richard Harder]]
* 1940–1994: [[Erich Burck]]
* 1949–1965: [[Friedrich Matz the Younger|Friedrich Matz]]
* 1954–1977: [[Walter Marg]]
* 1963–1978: [[Hermann Strasburger]]
* 1966–1977: [[Nikolaus Himmelmann]]
* since 1975: [[Ernst Vogt]]
* since 1978: [[Paul Zanker]]
* 1979–1995: [[Walter Schmitthenner]]
* 1988–2007: [[Carl Joachim Classen]]
* since 1996: [[Hans-Joachim Gehrke]]
* since 2000: [[Henner von Hesberg]]
* since 2000: [[Martin Hose]]
}}

== References ==
{{Reflist}}

== External links==
* {{Official website|http://www.chbeck.de/gnomon}}
* [http://www.jstor.org/journal/gnomon Online access] at [[JSTOR]]
* [http://www.gnomon-online.de/ Gnomon Bibliographische Datenbank]
* {{ISSN|0017-1417}}

[[Category:Classics journals]]
[[Category:Archaeology journals]]
[[Category:Publications established in 1925]]