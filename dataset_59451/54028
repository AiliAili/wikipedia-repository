{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox zoo
|zoo_name     = Adelaide Zoo
|logo         = Zoos SA Logo.jpg
|logo_width   = 200px
|logo_caption = Adelaide Zoo
|image        =
|image_width  =
|image_caption=
|date_opened  = 23 May 1883
|date_closed  =
|location     = [[Adelaide]], [[South Australia]], [[Australia]]
|coordinates  = {{Coord|34|54|51|S|138|36|21|E|region:AU_type:landmark|display=inline,title}}
|area         = {{convert|8|ha}}
|num_animals  = over 2,000
|num_species  = 300
|members      = [[Zoo and Aquarium Association|ZAA]],<ref name="zaa_institutional"/> [[World Association of Zoos and Aquariums|WAZA]]<ref name="waza_list"/>
|exhibits     =
|website      = {{URL|http://www.adelaidezoo.com.au/}}
}}
[[File:Sumatran tiger.JPG|thumb|right|One of the [[Sumatran tigers]] at Adelaide Zoo]]

'''Adelaide Zoo''' is [[Australia|Australia's]] second oldest [[zoo]] (after [[Melbourne Zoo]]), and like other leading zoos in Australia, including [[Melbourne Zoo]], [[Taronga Zoo]], and [[Perth Zoo]] it is operated on a non-profit basis.<ref>{{cite web|title=Five Facts About Good Zoos|url=http://www.zoo.org.au/news/five-facts-about-good-zoos/|publisher=Zoos Victoria|accessdate=27 July 2016}}</ref> It is located in the [[Adelaide Parklands|parklands]] just north of the [[Adelaide city centre|city centre]] of [[Adelaide]], [[South Australia]]. It is administered by the Royal Zoological Society of South Australia Incorporated (trading as Zoos SA) which is a full institutional member of the [[Zoo and Aquarium Association]] (ZAA) and the [[World Association of Zoos and Aquariums]] (WAZA). and which also administers the [[Monarto Zoo]] near [[Murray Bridge]].<ref>{{cite web|title=Terms and Conditions - Membership|url=http://www.zoossa.com.au/about-us/terms-and-conditions/|publisher=Zoos SA|accessdate=5 July 2015}}</ref>

The zoo houses about 300 native and exotic species, with over 1,800 animals on site.<ref>[http://www.zoossa.com.au/adelaide-zoo Adelaide Zoo website], www.zoossa.com.au</ref> The zoo's most recent enclosures are in the second phase of the South-East Asia exhibit, known as Immersion, providing visitors with the experience of walking through the jungle, with [[Sumatran tigers]] and [[orangutans]] seemingly within reach.

Five buildings within the zoo have been listed as state heritage places on the South Australian Heritage Register including the front entrance on [[Frome Road, Adelaide|Frome Road]] and the former Elephant House.<ref>{{cite web|title=SA Heritage Register entry for the Main Gates and Walling|url=http://apps.planning.sa.gov.au/HeritageSearch/HeritageItem.aspx?p_heritageno=1563|publisher=Department of Environment, Water and Natural Resources|accessdate=5 July 2015}}</ref><ref>{{cite web|title=SA Heritage Register entry for the Elephant House|url=http://apps.planning.sa.gov.au/HeritageSearch/HeritageItem.aspx?p_heritageno=1561|publisher=Department of Environment, Water and Natural Resources|accessdate=5 July 2015}}</ref> The zoo is also a [[botanical garden]] and the grounds contain significant exotic and native flora, including a [[Moreton Bay fig]] planted in 1877.

The giant panda exhibit, which opened in December 2009, is home to two [[giant panda]]s, [[Wang Wang and Funi]].

==History==
[[File:Adelaide Zoo flamingo.jpg|thumb|upright|left|The last [[greater flamingo]] in Australia died at Adelaide Zoo in January 2014]]
[[File:Ardea picata 1.jpg|thumb|upright|right|A [[pied heron]] in one of the zoo's walk-in [[aviary|aviaries]]]]

Adelaide Zoo first opened on 23 May 1883, occupying {{convert|6.5|ha}} (now {{convert|8|ha}}) of land granted by the Government. It was founded by the South Australian Acclimatization and Zoological Society.<ref>[http://trove.nla.gov.au/work/34746918?q=South+Australian+Acclimatization+and+Zoological+Society&c=article&versionId=43050693 NLA Trove > Annual report of the South Australian Acclimatization and Zoological Society.] Retrieved 20 September 2013.</ref><ref>[http://www.zoossa.com.au/adelaide-zoo/zoo-information/contact-us/history Adelaide Zoo >...> History] Accessed 20 September 2013.</ref> The society later became the [[Royal Zoological Society of South Australia]] after a [[Royal Charter]] was granted by [[George VI of the United Kingdom|King George VI]] in 1937.

The first director of the zoo (from 1882 to 1893) was [[R. E. Minchin]]. He was succeeded by his son A. C. Minchin (from 1893 to 1934), and grandson R. R. L. Minchin (from 1935 to 1940). Another grandson, Alfred Keith Minchin ran the (private) Koala Farm in the North Parklands 1936–1960; the surplus koalas were set free on [[Kangaroo Island]].<ref>Joyce Gibberd, 'Minchin, Alfred Keith (1899–1963)', Australian Dictionary of Biography, National Centre of Biography, Australian National University, http://adb.anu.edu.au/biography/minchin-alfred-keith-7796/text13273, published first in hardcopy 1986, accessed online 26 February 2015.</ref>

In the mid-twentieth century the zoo was involved in the export of live birds, with 99% of Australia's exports of live native birds, mainly [[finches]] and [[parrots]] for [[aviculture]], passing through either Adelaide or Taronga zoos. At a time when the need for conservation of Australia's [[Birds of Australia|native birds]], and control of their trade was becoming increasingly apparent, South Australia lagged behind other states in passing appropriate legislation.

In 1962 a new director of the zoo, William Gasking, was quickly dismissed through the power exerted by the Zoo Council president, Fred Basse, on the grounds that Gasking would not cooperate with the bird trade.  However, when Basse retired the trade in birds dropped to a tenth of what it had been two years before. Since then the zoo's administration has been restructured and the zoo has regained public credibility and scientific status.

The modern zoo has moved away from the traditional housing of species separately in pairs. Now species are grouped together as they would be in the wild, in exhibits that are carefully planned according to region. Enclosures have been designed with the needs of the animals in mind, providing a more natural habitat, which also serves an educational purpose for visitors. Although some of the zoo's heritage listed enclosures such as the Elephant House have been retained, they are no longer used to house animals; (the Elephant House now has educational signs). The last elephant housed at the Adelaide Zoo, Samorn, was moved to Monarto in 1991, where she died three years later.<ref>[http://www.adelaidenow.com.au/news/south-australia/adelaide-remember-when-samorn-the-elephant-and-george-the-orangutan-lived-at-the-zoo/story-fni6uo1m-1226819929541 Adelaide, remember when Samorn the elephant and George the orangutan lived at the zoo? Adelaide, remember when Samorn the elephant and George the orangutan lived at the zoo?] ''The Advertiser'', 6 February 2014. Retrieved 23 September 2016.</ref> 

The flamingo exhibit was opened in 1885, and is one of the few to have remained in the same position to date. Originally it was stocked with 10 flamingos, however most died during a drought in 1915. In 2014, one of two surviving flamingos in the exhibit, thought to be the oldest in the world at 83 years of age, died.<ref>{{cite news |url=http://www.radioaustralia.net.au/international/2014-01-31/flamingo-believed-to-be-worlds-oldest-dies-at-adelaide-zoo-aged-83/1257422 |title=Flamingo believed to be world's oldest dies at Adelaide Zoo aged 83 |publisher=ABC Radio Australia | last=Fedorowytsch | first=Tom | date= 31 January 2014 |accessdate=31 January 2014}}</ref> The remaining Chilean flamingo at Adelaide zoo is now the only flamingo in Australia.<ref>{{cite web|title=Adelaide Zoo - Chilean Flamingo|url=http://www.adelaidezoo.com.au/animals/chilean-flamingo/|publisher=Adelaide Zoo|accessdate=26 August 2015}}</ref>

==Current focus==
The zoo has a particular focus on species from the [[Gondwana]] 'supercontinent' which was made up of [[South America]], [[India]], [[Africa]], [[Australia]] and [[South East Asia]]. The botanic similarities between the regions are featured in the zoo's main exhibits, which include a South East Asian Rainforest, and Australian Rainforest Wetlands walk-through aviary. The South East Asian exhibit combines [[Sumatran orangutan]] and [[siamang]] together. It also combines [[Malayan tapir]] and [[dusky leaf monkey]] together. In the past, in fact almost to the present day, Adelaide Zoo was famous for having the best bird collection and display of all the Australian zoos.

The zoo also has a focus on educational programs. There is a selection of "get to know the zoo" type of tours, a large "children's zoo" area, and from April 2009, an educational area for secondary school students and their teachers. Schools can hire the facility and groups can sleep there, with a member from the zoo supervising. Also, a new educational area called the Envirodome opened in April 2009. Night walks, tours and animal research can be done. More information on the educational programs can be found on the zoo's web site.

==Exhibits==
;Asian Region
:The South East Asia Exhibit called Immersion was built in two parts. The first part was finished in 1995 which gave exhibits to animals such as [[siamang]]s and [[sun bear]]s. In late 2006 most of part two was finished which gave exhibits to [[Sumatran orangutan]]s, [[siamang]]s and [[Sumatran tiger]]s. There is also a large walk-through aviary which takes you past the two gibbon islands to the [[sun bear]] enclosure.

;Australian Region

;Jewels of Asia Aviary

;Children's Zoo

;African Region

;South American Region

;Tamarin House

;Australian Habitat Aviaries

;Australian Bush Aviaries

;Envirodome
:The Envirodome is an interactive visitor experience housed in the old Ape Block along with the Education Centre. The non-animal exhibits are hands-on and are aimed at the conservation of our environment, hoping to educate the public on simple changes they can make to help the environment. The building itself has been largely recycled and has a green roof, rain-water fed toilets, hay-bale walls and solar-panels.

==Incidents==
In 1909, the [[Strand Magazine]] (p.&nbsp;386) reported that a snake had swallowed a rug weighing almost 12 pounds, and which survived undamaged in the snake's stomach until disgorged almost a month later. The short article featured a photograph of the disgorged rug which was 5 feet 3 inches long.

In 1985, two men broke in and killed 64 animals.<ref name=bbc>[http://news.bbc.co.uk/1/hi/world/asia-pacific/7699092.stm Australia youths 'maul flamingo'], 30 October 2008, news.bbc.co.uk</ref>

On 30 October 2008, a 78-year-old blind [[greater flamingo]] named "[[Greater (flamingo)|Greater]]" was beaten, allegedly by a group of teenagers. Four teenagers were charged after visitors reported an incident to zoo staff. The male flamingo was left "extremely stressed."<ref>http://www.abc.net.au/news/stories/2009/05/10/2565795.htm, news.bbc.co.uk</ref><ref>{{cite news|accessdate= |title=Blind flamingo 'bashed' at zoo |work=The Daily Telegraph |date=30 October 2008 |url=http://www.news.com.au/dailytelegraph/story/0,22049,24575343-5005941,00.html |deadurl=yes |archiveurl=https://web.archive.org/web/20081102133832/http://www.news.com.au/dailytelegraph/story/0,22049,24575343-5005941,00.html |archivedate=2 November 2008 }}</ref>

On Mother's Day 2009, the female orangutan, Karta, built an escape route out of plant material and tripped the hot wires with a stick. After a short while on the 'outside' she dropped back into the exhibit with no harm done.<ref>{{cite web|url=http://www.abc.net.au/news/stories/2009/05/10/2565795.htm|title=Orangutan short-circuits fence in zoo breakout|date=10 May 2009|publisher=Australian Broadcasting Commission}}</ref>

==Notes==
{{Reflist |refs=

<ref name="zaa_institutional">
{{ZooOrg|zaa|zoos|accessdate=10 September 2010}}
</ref>

<ref name="waza_list">
{{ZooOrg|waza|zoos|accessdate=29 January 2011}}
</ref>

}}

==References==
* Robin, Libby. (2001). ''The Flight of the Emu: a hundred years of Australian ornithology 1901-2001''. Melbourne University Press: Carlton. ISBN 0-522-84987-3
* [http://www.zoossa.com.au/adelaide-zoo/zoo-information/about-us "Fact file of the Adelaide Zoo"]

==External links==
* {{Commons category-inline}}
* {{Official website|http://www.adelaidezoo.com.au/}}


{{Zoos of South Australia}}
{{Adelaide landmarks}}

[[Category:Tourist attractions in Adelaide|Zoo]]
[[Category:Zoos in Australia]]
[[Category:Buildings and structures in Adelaide]]
[[Category:1883 establishments in Australia]]
[[Category:Parks in Adelaide]]