{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
[[File:Portside messenger.jpg|thumb|right|A copy of the ''Messenger''.]]
'''Messenger Newspapers''' is the publisher of 11 free [[suburban]] [[weekly newspaper]]s together covering the [[Adelaide]] metropolitan area. Established by Roger Baynes in [[Port Adelaide]] in 1951, ''Messenger'' has since acquired other independent suburban titles to become Adelaide's only suburban newspaper group. The paper is a subsidiary of [[News Limited]].<ref>[http://www.messengernews.com.au Messenger Newspapers]</ref>

The ''Messenger'' is delivered weekly to 11 different suburban areas, each paper targeting content to its distribution area with some shared content. The ''Messenger'' group also publishes one monthly lifestyle magazine, ''Adelaide Matters''. More than 710,000 people read the ''Messenger'' every week.<ref>[http://www.community.newsmedianet.com.au/home/groups/group/index.jsp?groupid=4 News Limited Community Newspapers]</ref>

The newspapers cover events in the distribution area, including local council decisions, controversial developments, local social trends, articles about local volunteers or young people, and local sports clubs. There is also a significant classifieds and real estate sections. All ''Messenger'' titles feature regular sections such as Vibe (entertainment guide), Sport, and Your Garden.

In mid-2009, Messenger Newspapers moved from its headquarters at 1 Baynes Place, Port Adelaide to new offices at Sir Keith Murdoch House, 31 Waymouth St, Adelaide. ''The Adelaide Advertiser'', ''Sunday Mail'' and various other News Ltd publications are also based in Sir Keith Murdoch House.

==History==
<!--  Commented out because image was deleted: [[File:Messenger1953.jpg|thumb|right|150px|The Messenger, April 23, 1953]] -->
[[File:Advertiser Building.JPG|thumb|The office of the ''Messenger'' group of newspapers in Waymouth St, Adelaide]]
In 1951, Port Adelaide courier Roger Baynes, in partnership with [[Len Croker]], took over the Largs North Progressive Association's ''Progressive Times''. In March 1951, the ''Progressive Times'' was relaunched as the ''Messenger''. The ''Messenger'' originally operated out of a small room above a Port Adelaide bicycle shop. The business later moved to an old butcher's shop on Commercial Road, Port Adelaide.

In 1954, Croker left the ''Messenger'' to run the ''Woodville Times''. In 1959, Baynes' fellow courier, Ron Mitchell, joined ''Messenger'' to run its newest acquisition, the ''Standard''. Messenger Press continued to acquire suburban newspapers across Adelaide and turned them into ''Messenger'' titles. Messenger Press acquired John Carroll's four ''News Review'' titles, the ''Edwardstown District Community Centre Newspaper'' and the ''Glenelg Guardian'', previously run by the Smedley brothers at Glenelg.<ref>[http://www.holdfastbaychamber.com.au/testimonials/history.asp Holdfast Bay Chamber of Commerce]</ref>

By 1962, ''Messenger Newspapers'' were being delivered to 250,000 homes across Adelaide. Two years later, Baynes sold nearly half of the company's shares to ''[[The Advertiser (Adelaide)|The Advertiser]].'' In 1983, shortly before his death, Baynes sold his remaining shares to ''The Advertiser''.

In the 1970s and 1980s newspapers in the Adelaide Hills, south coast and Barossa Valley were added to the ''Messenger'' stable. In 1988 the ''City Messenger'' was established to cover the Adelaide CBD.

''Messenger'' had several printing firsts, most notably, in 1968, being the first newspaper in the southern hemisphere to own a web offset press - just one year after being the first press to use IBM tape electric typesetting. In 1981 the firm purchased a Mitsubishi L600 colour press, enabling ''Messenger'' to print coloured magazines including ''The Advertiser Magazine'', ''Football Times'', ''South Australian Radio TV Extra'' and ''Adelaide Matters''. However, from 1988 production moved to ''The Advertiser''. From 1991 all ''Messenger'' newspapers were printed by News Limited.

In 2007, ''Messenger Newspapers'' began publishing news online and uploaded web videos for the first time. In January 2008, ''Messenger'' added a number of online interactive features to its websites, including photo galleries and a breaking news feed from Adelaide Now, the online news service of ''The Advertiser'' newspaper.

In October, 2009, Messenger Community News re-launched ''The City Messenger'', featuring a new look, improved design and new weekly features, such as the 60 Second News Tour - a quick snapshot of stories across Messenger's 11 titles.

Also in October, Messenger relaunched a new look Standard Messenger under the new name ''City North Messenger''. 
The newly launched publication now takes in North Adelaide, which was previously covered by ''The City Messenger''. The ''City North'' was launched with a new emphasis on increased coverage of news, sport and lifestyle content. The Standard Messenger website was rebadged www.citynorthmessenger.com.au to reflect the changes.

==Local editions==

{| class=wikitable
!Title
!Region
!Delivery day
!Circulation
!Readership
|-
|[[News Review Messenger|News Review]] || [[Salisbury, South Australia|Salisbury]], [[Elizabeth, South Australia|Elizabeth]] and [[Gawler, South Australia|Gawler]] 
| Wednesday || 92,000 || 96,016
|-
|[[Leader Messenger|Leader]] || [[Modbury, South Australia|Modbury]] and [[Golden Grove, South Australia|Golden Grove]]
| Wednesday || 56,000 || 43,674
|-
|[[Weekly Times Messenger|Weekly Times]] || [[Woodville, South Australia|Woodville]], [[West Lakes, South Australia|West Lakes]], [[West Beach, South Australia|West Beach]] 
| Wednesday || 65,000 || 66,461
|-
|[[Portside Messenger|Portside]] || [[Port Adelaide]] and [[Lefevre Peninsula]] 
| Wednesday || 55,000 || 33,187 
|-
|[[City Messenger|City]] || [[Adelaide]] 
| Thursday || 35,000 || 19,702
|-
|[[East Torrens Messenger|East Torrens]] || [[Payneham, South Australia|Payneham]], [[Athelstone, South Australia|Athelstone]], [[Magill, South Australia|Magill]]
| Wednesday || 43,000 || 35,243
|-
|[[Hills & Valley Messenger|Hills & Valley]] || [[Blackwood, South Australia|Blackwood]], [[Aberfoyle Park, South Australia|Aberfoyle Park]]
| Wednesday || 29,000 || 19,597
|-
|[[City North Messenger]] || [[Prospect, South Australia|Prospect]], [[North Adelaide, South Australia|North Adelaide]], [[Walkerville, South Australia|Walkerville]] and [[Enfield, South Australia|Enfield]]
| Wednesday || 48,000 || 38,043
|-
|[[Eastern Courier Messenger|Eastern Courier]] || [[Norwood, South Australia|Norwood]], [[Burnside, South Australia|Burnside]], [[Goodwood, South Australia|Goodwood]]
| Wednesday || 86,000 || 62,673
|-
|[[Guardian Messenger|Guardian]] || [[Glenelg, South Australia|Glenelg]], [[Marion, South Australia|Marion]], [[Hallett Cove, South Australia|Hallett Cove]]
| Wednesday || 75,000 || 71,252
|-
|[[Southern Times Messenger|Southern Times]] || [[Reynella, South Australia|Reynella]], [[Noarlunga, South Australia|Noarlunga]], [[Sellicks Beach, South Australia|Sellicks Beach]]
| Wednesday || 88,000 || 60,088
|-
|[[Adelaide Matters]] || [[Adelaide, South Australia|Adelaide]]
|Monthly || 106,347 || 127,000
|}

==See also==
*[[List of newspapers in Australia]]

==References==
{{reflist}}

==External links==
* [http://www.messengernews.com.au/ Messenger Newspapers]

{{Messenger Newspapers}}
{{News Corp Australia}}

{{DEFAULTSORT:Messenger, The}}
[[Category:Media in Adelaide]]
[[Category:Newspapers published in South Australia]]