{{Infobox journal
| title = French Politics, Culture & Society
| cover = [[File:Jnl cover frenchpcs.jpg|File:Jnl_cover_frenchpcs.jpg]]
| editor = Herrick Chapman
| discipline = [[Sociology]], [[political science]]
| language = English, French
| publisher = [[Berghahn Books]]
| frequency = Triannually
| history = 1983-present
| website = http://journals.berghahnbooks.com/fpcs/
| link1 = http://berghahn.publisher.ingentaconnect.com/content/berghahn/fpcs
| link1-name = Online access
| ISSN = 1537-6370
| eISSN = 1558-5271
}}
'''''French Politics, Culture & Society''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Berghahn Books]] on behalf of the Conference Group on French Politics & Society (sponsored jointly by the Minda de Gunzburg Center for European Studies at [[Harvard University]] and the Institute of French Studies at [[New York University]]). It covers modern and contemporary [[France]] from the perspectives of the [[social science]]s, [[history]], and [[cultural analysis]]. It also explores the relationship of France to the rest of the world, especially [[Europe]], the [[United States]], and the former [[List of French possessions and colonies|French colonies]]. The [[editor-in-chief]] is Herrick Chapman.

== Abstracting and indexing ==
''French Politics, Culture & Society'' is indexed and abstracted in:
* [[America: History and Life]]
* [[British Humanities Index]]
* [[Columbia International Affairs Online]]
* [[Educational Resources Information Center]]
* [[Historical Abstracts]]
* [[InfoTrac]]
* [[International Bibliography of Book Reviews of Scholarly Literature on the Humanities and Social Sciences]]
* [[International Bibliography of Periodical Literature]]
* [[International Political Science Abstracts]]
* [[Modern Language Association|MLA International Bibliography]]
* [[Sociological Abstracts]]
* [[Worldwide Political Science Abstracts]]

== External links ==
*{{Official website|http://journals.berghahnbooks.com/fpcs/}}

{{DEFAULTSORT:French Politics, Culture and Society (journal)}}
[[Category:Sociology journals]]
[[Category:Political science journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Multilingual journals]]
[[Category:European studies journals]]


{{poli-journal-stub}}