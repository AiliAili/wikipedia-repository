{{good article}}
{{Use dmy dates|date=July 2014}}
{{Infobox International Football Competition
| tourney_name = CECAFA Cup
| year = 2000
| other_titles = 
| image =
| size =
| caption =
| country =  Uganda
| dates = 18 November – 2 December
| num_teams = 9
| confederations = [[CECAFA]]
| venues = 
| cities = 
| champion =  Uganda
| champion-name = Uganda (A)
| count =  8
| second =  Uganda
| second-name = Uganda (B)
| third = Ethiopia
| third-flagvar = 1996
| matches = 20
| goals = 61
| attendance = 
| top_scorer = 
| player = 
| update =
| prevseason= [[1999 CECAFA Cup|1999]]
| nextseason= [[2001 CECAFA Cup|2001]]
}}

The '''2000 [[CECAFA Cup]]''' was the 24th edition of the football tournament, which involves teams from Southern and Central Africa. The matches were played in Uganda, a decision which Tanzania protested, but to no avail.<ref name="RSSSF "/> Tanzania were also banned from international football by [[FIFA]], the world football governing body.<ref name="RSSSF "/> The matches were played from 18 November to 2 December 2000. Prior to the tournament, [[Djibouti national football team|Djibouti]] withdrew due to monetary difficulties, but re-entered. Sudan and Zanzibar also withdrew, but stayed out. The reasoning behind their withdrawal was unknown.<ref name="RSSSF "/>

Nine teams entered the tournament, as opposed to the twelve teams competing at the previous tournament in 1999.<ref name="1999RSSSF" /> However, there were only eight different nations competing in the tournament as Uganda entered two teams: Uganda (A) and Uganda (B). Uganda (A) was in Group A, and Uganda (B) was in Group B, enabling the two the correct set-up to be able to meet each other in the final, as occurred here. Both Ugandas topped their respective groups, and progressed to the knockout stages along with [[Ethiopia national football team|Ethiopia]] and [[Rwanda national football team|Rwanda]]. Uganda (A) beat Rwanda, and Uganda (B) beat Ethiopia to mean that "the Cranes" (Uganda [A]) and "the Lions" (Uganda (B)) would meet in the final. In the final, Uganda (A) captained by [[George Ssimwogerere]] of [[Express F.C.|Express]], won 2–0.<ref name="RSSSF "/><ref name="O">{{cite web | url=http://www.observer.ug/index.php?option=com_content&view=article&id=6520:ugandas-8-cecafa-winning-captains | title=Uganda's 8 CECAFA winning captains | work=The Observer | date=20 December 2009 | accessdate=19 July 2014 | author=Zziwa, Hassan Badru}}</ref> Ethiopia finished third after beating Rwanda on penalties 4–2 after the match finished 0–0.<ref name="RSSSF "/>

== Background ==
The CECAFA Cup is considered Africa's oldest football tournament, and involves teams from Central and Southern Africa. The tournament was originally named the Gossage Cup, contested by the four nations of Kenya, Uganda, Tanganyika (modern day Tanzania), and Zanzibar;<ref name="Futaa">{{cite web | url=http://www.futaa.com/football/article/the-cecafa-fact-file | title=The CECAFA Fact File | work=futaa.com | date=25 November 2009 | accessdate=1 June 2014 | author=Korir, Patrick}}</ref> it ran from 1929 until 1965.<ref name="Book">{{cite book | url=https://books.google.com/books?id=SnqEAwAAQBAJ&pg=PA71&lpg=PA71&dq=1973+cecafa+cup#v=onepage&q=1973%20cecafa%20cup&f=false | title=Identity and Nation in African Football: Fans, Community and Clubs | publisher=Palgrave Macmillan | date=8 April 2014 |author1=Onwumechili, Chuka  |author2=Akindes, Gerard | isbn=9781137355812}}</ref> In 1967, this became the East and Central African Senior Challenge Cup, often shortened to simply the Challenge Cup, which was competed for five years, until 1971, before the CECAFA Cup was introduced in 1973.<ref name="Futaa "/> The 1999 champions were Rwanda (B), in 2000 they emerged from their group second, after Uganda (B), 5 points behind but 2 points ahead of the next team, Eritrea. They were then knocked out in the semi-finals against Uganda (A). The 2000 champions, Uganda, were knocked out in the 1999 semi-finals by none other than Rwanda.<ref name="1999RSSSF">{{cite web | url=http://www.rsssf.com/tablese/eastcentrafr.html#99 | title=Rwanda, Jul 24 – Aug 7, 1999 | publisher=Rec.Sport.Soccer Statistics Foundation (RSSSF) |date=17 April 2014 | accessdate=18 July 2014}}</ref>

== Participants ==
9 teams from 8 countries competed, three teams from the original tournament competed (excluding [[Tanganyika]], which changed names and is currently called [[Tanzania]]), all 8 nations at this tournament had competed at the 1999 CECAFA Cup.<ref name="1999RSSSF "/>
{{col-begin|width=70%}}
{{col-3}}
*{{fb|Burundi}}
*{{fb|Djibouti}}
*{{fb|Eritrea}}
{{col-3}}
*{{fb|Ethiopia|1996}}
*{{fb|Kenya}}
*{{fb|Rwanda|1962}}
{{col-3}}
*{{fb|Somalia}}
*{{fb|Uganda|name=Uganda (A)}}
*{{fb|Uganda|name=Uganda (B)}}<ref name="RSSSF">{{cite web | url=http://www.rsssf.com/tablese/eastcentrafr.html#00 | title=Uganda, Nov 18 – Dec 2, 2000 | publisher=Rec.Sport.Soccer Statistics Foundation (RSSSF) |date=17 April 2014 | accessdate=18 July 2014}}</ref>
{{col-end}}

==Group stage==
The group stage began on 18 November and ended on 27 November with Group A's final match between Ethiopia and Burundi. At the end of the group stage, the teams who finished bottom of their group were eliminated, along with the two teams above them (in Group A), and the team above them (in Group B), whereas the teams positioned in the top two slots in the groups progressed to the knock-out rounds. Due to there being an odd number of teams, Group A contained more matches and an additional team, with a total of 5 to Group B's 4 teams.<ref name="RSSSF "/>

<div id="Tiebreakers">
If two or more teams are equal on points on completion of the group matches, the following criteria are applied to determine the rankings (in descending order):
{{col-begin}}
{{col-2}}
# Number of points obtained in games between the teams involved;
# Goal difference in games between the teams involved;
# Goals scored in games between the teams involved;
# Away goals scored in games between the teams involved;
# Goal difference in all games;
# Goals scored in all games;
# Drawing of lots.

===Group A===
''Played in [[Nakivubo Stadium]], [[Kampala]].''
{{Fb cl2 header navbar}}
{{Fb cl2 team|t={{fb|UGA|name=Uganda (A)}} |w=3 |d=1 |l=0 |gf=17 |ga=3 |bc=#ccffcc}}
{{Fb cl2 team|t={{fb|ETH|1996}} |w=3 |d=1 |l=0 |gf=9 |ga=5 |bc=#ccffcc}}
{{Fb cl2 team|t={{fb|BDI}} |w=2 |d=0 |l=2 |gf=8 |ga=5}}
{{Fb cl2 team|t={{fb|SOM}} |w=0 |d=1 |l=3 |gf=1 |ga=11}}
{{Fb cl2 team|t={{fb|DJI}} |w=0 |d=1 |l=3 |gf=4 |ga=15}}
|}

{{football box
 | date = 18 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}} 
 | score = 2–2
 | team2 = {{fb|ETH|1996}}
 | goals1 = [[Hassan Mubiru|Mubiru]] {{goal|3}} <br/> [[George Ssemogerere|Ssemogerere]] {{goal|87}}
 | goals2 = [[Sintayehu Getachew|Getachew]] {{goal}} <br/> [[Hussein Seman|Seman]] {{goal|73}}
 | stadium =  }}
----
{{football box
 | date = 19 November 2000
 | team1 = {{fb-rt|DJI}} 
 | score = 0–0
 | team2 = {{fb|SOM}}
 | goals1 =  
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 20 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}} 
 | score = 2–1
 | team2 = {{fb|BDI}}
 | goals1 =  [[Mathias Kawesa|Kawesa]] {{goal|53}} <br/> [[Hussein Mbalangi|Mbalangi]] {{goal|87}}
 | goals2 = [[Banga Lewis Kubi]] {{goal|58}}  
 | stadium =  }}
----
{{football box
 | date = 21 November 2000
 | team1 = {{fb-rt|SOM}} 
 | score = 1–2
 | team2 = {{fb|ETH|1996}}
 | goals1 =  [[Abdullahi Mohamed|Mohamed]] {{goal|39}}
 | goals2 =  [[Sintayehu Getachew|Getachew]] {{goal|30}} <br/> [[Hussein Semen|Semen]] {{goal|55}}
 | stadium =  }}
----
{{football box
 | date = 23 November 2000
 | team1 = {{fb-rt|DJI}} 
 | score = 2–4
 | team2 = {{fb|BDI}}
 | goals1 =  [[Ahid Ahmed|Ahmed]] {{goal}} <br/> Hassan Mohamad {{goal}} 
 | goals2 =  [[Banga Lewis Kubi]] {{goal}}{{goal}} <br/> [[Abdullah Irambona|Irambona]] {{goal}}{{goal}}
 | stadium =  }}
----
{{football box
 | date = 23 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}}
 | score = 6–0
 | team2 = {{fb|SOM}}
 | goals1 =  [[Andrew Mukasa|Mukasa]] {{goal}} {{goal}} <br/> [[Fred Makokha|Makokha]] {{goal}} <br/> [[Hassan Mubiru|Mubiru]] {{goal}} <br/> [[Ibrahim Buwembo|Buwembo]] {{goal}} <br/> [[Edward Kalungi|Kalungi]] {{goal}}
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 25 November 2000
 | team1 = {{fb-rt|ETH|1996}} 
 | score = 4–2
 | team2 = {{fb|DJI}}
 | goals1 = [[Ismail Abubeker|Abubeker]] {{goal}} {{goal}} <br/> [[Sintayehu Getachew|Getachew]] {{goal}} <br/> [[Getu Teshome|Teshome]] {{goal}} 
 | goals2 = [[Ali Moktar|Moktar]] {{goal}} <br/> [[Ali Fascal|Fascal]] {{goal}} 
 | stadium =  }}
----
{{football box
 | date = 26 November 2000
 | team1 = {{fb-rt|SOM}} 
 | score = 0–3
 | team2 = {{fb|BDI}}
 | goals1 =  
 | goals2 =  [[Shaban Daoudi|Daoudi]] {{goal|63|pen.}} <br/> [[Majid Ndikumana|Ndikumana]] {{goal|76}} <br/> [[Mohamed Kimada|Kimada]] {{goal|86}}
 | stadium =  }}
----
{{football box
 | date = 27 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}} 
 | score = 7–0
 | team2 = {{fb|DJI}}
 | goals1 =  [[Andrew Mukasa|Mukasa]] {{goal}} {{goal}} {{goal|63}} <br/> [[George Ssemogerere|Ssemogerere]] {{goal|15}} <br/> [[Hassan Mubiru|Mubiru]] {{goal}} <br/> [[Fred Makokha|Makokha]] {{goal|17}} <br/> [[Meddie Nsubuga|Nsubuga]] {{goal}}
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 27 November 2000
 | team1 = {{fb-rt|ETH|1996}} 
 | score = 1–0
 | team2 = {{fb|BDI}}
 | goals1 =  [[Ismail Abubeker|Abubeker]] {{goal|89}}
 | goals2 =  
 | stadium =  }}

===Group B===

''Played in the Municipal Stadium, [[Mbale]].''
{{Fb cl2 header navbar}}
{{Fb cl2 team|t={{fb|UGA|name=Uganda (B)}} |w=3 |d=0 |l=0 |gf=6 |ga=2 |bc=#ccffcc}}
{{Fb cl2 team|t={{fb|RWA|1962}} |w=1 |d=1 |l=1 |gf=5 |ga=5 |bc=#ccffcc}}
{{Fb cl2 team|t={{fb|ERI}} |w=0 |d=2 |l=1 |gf=1 |ga=2}}
{{Fb cl2 team|t={{fb|KEN}} |w=0 |d=1 |l=2 |gf=1 |ga=4}}
|}

{{football box
 | date = 19 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (B)}} 
 | score = 3–2
 | team2 = {{fb|RWA|1962}}
 | goals1 =
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 20 November 2000
 | team1 = {{fb-rt|KEN}} 
 | score = 0–0
 | team2 = {{fb|ERI}}
 | goals1 =  
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 22 November 2000
 | team1 = {{fb-rt|KEN}} 
 | score = 1–2
 | team2 = {{fb|RWA|1962}}
 | goals1 =  [[Robert Mambo Mumba|Robert Mambo]] {{goal|72}}
 | goals2 =  [[Julien Nsengiyumva|Nsengiyumva]] {{goal|32}} <br/> [[Abdu Uwimana|Uwimana]] {{goal|68}}
 | stadium =  }}
----
{{football box
 | date = 23 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (B)}} 
 | score = 1–0
 | team2 = {{fb|ERI}}
 | goals1 =  [[Moses Bantu|Bantu]] {{goal|75}}
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 25 November 2000
 | team1 = {{fb-rt|ERI}} 
 | score = 1–1
 | team2 = {{fb|RWA|1962}}
 | goals1 =  
 | goals2 =  
 | stadium =  }}
----
{{football box
 | date = 26 November 2000
 | team1 = {{fb-rt|KEN}} 
 | score = 0–2
 | team2 = {{fb|UGA|name=Uganda (B)}}
 | goals1 =  
 | goals2 =  [[Sammy Omollo|Omollo]] {{goal|18|o.g.}} <br/> [[Ahmed Kizza|Kizza]] {{goal|30}} 
 | stadium =  }}

==Knockout stage==
{{Round4-with third
<!--Date-Place|Team 1|Score 1|Team 2|Score 2 -->
<!--semi-finals -->
|29 November – |'''{{fb|UGA|name=Uganda (A)}}'''|'''3'''|{{fb|RWA|1962}}|1
|30 November – |{{fb|UGA|name=Uganda (B)}}|'''1'''|'''{{fb|ETH|1996}}'''|0
<!--final -->
|2 December – |'''{{fb|UGA|name=Uganda (A)}}'''|'''2'''|{{fb|UGA|name=Uganda (B)}}|0
<!--third place -->
|2 December – |'''{{fb|ETH|1996}}'''|'''1 (4)'''|{{fb|RWA|1962}}|1 (2)
}}

===Semi-finals===
{{football box
 | date = 29 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}}
 | score = 3–1
 | team2 = {{fb|RWA|1962}}
 | goals1 =  [[Hassan Mubiru|Mubiru]] {{goal|41}} <br/> [[Phillip Obwiny|Obwiny]] {{goal|64}} <br/> [[Mathias Kawesa|Kawesa]] {{goal|90}}
 | goals2 =  [[Abdul Sibomana|Sibomana]] {{goal|27}} 
 | stadium = [[Nakivubo Stadium]], [[Kampala]]
 }}
{{football box
 | date = 30 November 2000
 | team1 = {{fb-rt|UGA|name=Uganda (B)}} 
 | score = 1–0
 | team2 = {{fb|ETH|1996}}
 | goals1 =  [[Jimmy Kidega|Kidega]] {{goal|45}}
 | goals2 =  
 | stadium =  [[Nakivubo Stadium]], [[Kampala]] }}

===Third place match===
{{football box
 | date = 2 December 2000
 | team1 = {{fb-rt|ETH|1996}} 
 | score = 1–1 <br/> <span style="font-size:88%">(4 – 2 pen.)</span>
 | team2 = {{fb|RWA|1962}}
 | goals1 =  [[Hussein Seman|Seman]] {{goal|33}} 
 | goals2 =  [[Jean-Pierre Mulonda|Mulonda]] {{goal|50}}
 | stadium = [[Nakivubo Stadium]], [[Kampala]] }}

===Final===
{{football box
 | date = 2 December 2000
 | team1 = {{fb-rt|UGA|name=Uganda (A)}} 
 | score = 2–0
 | team2 = {{fb|UGA|name=Uganda (B)}}
 | goals1 =  [[Hassan Mubiru|Mubiru]] {{goal|76}} <br/> [[Jamil Kyambadde|Kyambadde]] {{goal|83}}
 | goals2 =  
 | stadium = [[Nakivubo Stadium]], [[Kampala]] }}

{{winners|fb|2000 CECAFA Cup Champions|UGA|8th|name=Uganda (A)}}

== Team statistics ==
Teams are ranked using the same tie-breaking criteria as in the [[#Group stage|group stage]], except for the top four teams.<ref name="RSSSF "/>
{{Fb cl3 header navbar|p=yes|no-extras=yes|sortable=yes|width=75%}}
{{Fb cl3 team|no-extras=yes|t={{fb|UGA|name=Uganda (A)}} |p=1  |w=5  |d=1  |l=0  |gf=22  |ga=4  |bc=#FFD700|h=yes}}
{{Fb cl3 team|no-extras=yes|t={{fb|UGA|name=Uganda (B)}} |p=2  |w=4  |d=0  |l=1  |gf=7  |ga=4  |bc=#C0C0C0}}
|-
|colspan="14"|<span style="display:none">08</span>'''Third-place play-off'''
|-
{{Fb cl3 team|no-extras=yes|t={{fb|ETH|1996}} |p=3  |w=3  |d=2  |l=1  |gf=10  |ga=7  |bc=#CD7F32}}
{{Fb cl3 team|no-extras=yes|t={{fb|RWA|1962}} |p=4  |w=1  |d=2  |l=2  |gf=7  |ga=9}}
|-
|colspan="14"|<span style="display:none">08</span>'''Eliminated in the group stages'''
|-
{{Fb cl3 team|no-extras=yes|t={{fb|BDI}} |p=5  |w=2  |d=0  |l=2  |gf=8  |ga=5  |bc=}}
{{Fb cl3 team|no-extras=yes|t={{fb|ERI}} |p=6  |w=0  |d=2  |l=1  |gf=1  |ga=2  |bc=}}
{{Fb cl3 team|no-extras=yes|t={{fb|KEN}} |p=7  |w=0  |d=1  |l=2  |gf=1  |ga=4  |bc=}}
{{Fb cl3 team|no-extras=yes|t={{fb|SOM}} |p=8  |w=0  |d=1  |l=3  |gf=1  |ga=11  |bc=}}
{{Fb cl3 team|no-extras=yes|t={{fb|DJI}} |p=9  |w=0  |d=1  |l=3  |gf=4  |ga=15  |bc=}}
|-
{{Fb cl3 footer|no-extras=yes|p=yes|statcon=yes|w=6 |d=8 |l=6 |gf=28 |ga=28 |u=2 December 2000}}

==References==
{{reflist}}

{{CECAFA Cup |state=expanded}}

[[Category:CECAFA Cup]]
[[Category:International association football competitions hosted by Uganda]]
[[Category:2000 in Ugandan football|CEc]]