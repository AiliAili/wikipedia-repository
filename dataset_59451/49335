{{Infobox museum
| name                = Liverpool 08 Collection
| native_name         = 
| native_name_lang    = 
| logo                = 
| logo_upright        = 
| logo_alt            = 
| logo_caption        = 
| image               = 
| image_upright       = 
| alt                 = 
| caption             = 
| map_type            = 
| map_relief          = 
| map_size            = 
| map_caption         = 
| latitude            = 
| longitude           = 
| lat_deg             = 
| lat_min             = 
| lat_sec             = 
| lat_dir             = 
| lon_deg             = 
| lon_min             = 
| lon_sec             = 
| lon_dir             = 
| coordinates_type    = 
| coordinates_region  = 
| coordinates_format  = 
| coordinates_display = 
| coordinates         = 
| former_name         = 
| established         = 2008
| dissolved           =2008
| location            = Liverpool John Lennon Airport
| type                = Public art collection
| accreditation       = 
| key_holdings        = Yellow Submarine, John Lennon Statue
| collections         = 
| collection_size     = 
| visitors            = 
| founder             = 
| director            = 
| president           = 
| chairperson         =
| curator             = 
| architect           =
| historian           =
| owner               = 
| publictransit       = 
| car_park            = 
| parking             = 
| network             = 
| website             = <!-- {{URL|example.com}} -->
| embedded            = 
}}

{{Orphan|date=August 2014}}

The '''Liverpool 08 Collection''' was the [[Public art]] collection exhibited by [[Liverpool John Lennon Airport]] in conjunction with the Liverpool Culture Company for the duration of 2008, the year when Liverpool was the [[European Capital of Culture]].  Exhibits were launched before and during the year by various celebreties including [[Yoko Ono]] and [[Phil Redmond]] and much of the exhibition remains in place today.  As well as the iconic sculptures, The [[Yellow Submarine (sculpture)]] and the John Lennon Statue, the different pieces of artwork situated around the public areas of the terminal included; two rare suits worn by John Lennon, photographs by [[Harry Goodwin]] and [[Paul Saltzman]], mosaics from Debbie Ryan, graphic designs by John McFaul, a film by Nick Jordan,<ref>{{cite web|last=Jordan|first=Nick|url=http://www.nickjordan.info/|work=Norwegian Wood|accessdate=13 February 2013}}</ref> a performance of [[Brian Eno]]'s 'Music for Airports' and artwork from 50 schoolchildren at St Ambrose Primary School in Speke

== Public Art and Business Growth at the Airport ==
Liverpool Airport's passenger numbers grew from 700,000 passengers to 5.5 million per annum in just a few years leading up to the Liverpool's [[European Capital of Culture]] year in 2008. This growth was unprecedented for a regional airport.<ref>{{cite web|title=Annual Airport Statistics|url=http://www.caa.co.uk/default.aspx?catid=80&pagetype=88&pageid=3&sglid=3|work=Airport Statistics|publisher=CAA}}</ref> The airport had been renamed after Liverpool’s famous son John Lennon,<ref>{{cite news|title=Yoko Unveils Lennon Airport|url=http://news.bbc.co.uk/1/hi/entertainment/1417595.stm|accessdate=13 February 2013|newspaper=BBC News|date=2 July 2001}}</ref> a move described at the time as ‘utter genius’ by Locum Destination Review.<ref>{{cite journal|title=Above Us Only Sky|journal=Locum Destination Review|date=October 2002|pages=48–50|url=https://docs.google.com/viewer?a=v&q=cache:lOafr3nOuFUJ:www.locumconsulting.com/pdf/LDR10AboveUsOnlySky.pdf+&hl=en&gl=fr&pid=bl&srcid=ADGEEShYrBC8RWGVnfrgzIsatLqCLaTy6HpeW2MCA6egxbrEs19uL5FcWcVLEn_17IDrjMZ5PMOJnAl4B0VgQdAQ5ZzekF4p8wOXHUchGr6ZgtqBA-7QUsahMsV5anXpO2zIoKU7gie3&sig=AHIEtbRPD0bE7w-CFKgFu9-uBRu11v7r9A|accessdate=13 February 2013}}</ref>

In their book The Themed Space,<ref>{{cite book|last=Lukas|first=Scott|title=The Themed Space|year=2007|publisher=Lexington Books.|isbn=978-0-7391-2141-2|pages=153–166}}</ref> editor Scott Lukas and co-author Peter Adey describe how the rebranding to John Lennon Airport changed market perceptions, loyalty and ultimately the passenger numbers using the airport. "The rebranding exercise helped the airport mediate between and collapse the geographical scales and distances. The airport was able to force its way into the global audience that John Lennon and The Beatles had fostered and entertained. By connecting these scales the airport did not only raise awareness but it helped stimulate a sense of belonging and ownership. The rebranding reinforced the sense that the airport was something owned by the locality."

Following the rebranding, the growth of passenger traffic increased by some 4 million passengers in the lead up to 2008. [[Neil Pakey]], managing director of Liverpool Airport, said: 'Putting the [art] on display helps to cement our commitment to the Capital of Culture year and to public arts in general. We hope passengers take the opportunity to arrive at the airport early to enjoy the exhibits on display.'<ref>{{cite news|title=New Taxi project at Liverpool Airport|url=http://www.uk-airport-news.info/liverpool-airport-news-220208.htm|accessdate=13 February 2013|newspaper=UK Airport News|date=22 August 2008}}</ref>

== Community Art ==
At the beginning of the year, John Lennon Airport (JLA) worked alongside the Liverpool Culture Company and issued an ‘art call' to local artists inviting them to display their work at the Airport...<ref>{{cite news|last=Turner|first=Alex|title=City's Culture Arts Projects Take Off at Airport|url=http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2008/09/19/city-s-culture-art-projects-have-taken-off-at-airport-99623-21852889/|accessdate=13 February 2013|newspaper=Liverpool Daily Post|date=19 September 2008}}</ref> JLA received many applications and as a consequence formed an Art Panel which consisted of representatives from JLA, Liverpool Culture Company, Liverpool Biennial and a local artist to review all the submissions and make a selection.

== Public Arts Exhibits At The Airport ==
The first Public art exhibit in the recently opened airport terminal in 2002 was of John Lennon's portraits. opened by Yoko Ono.

Public Arts Exhibits in the Collection ‘08 included:-
* The [[Yellow Submarine (sculpture)]]
* The John Lennon Statue by [[Tom Murphy (artist)]]<ref>{{cite news|title=Lennon Watches Over Airport|url=http://news.bbc.co.uk/1/hi/entertainment/1874238.stm|accessdate=13 February 2013|newspaper=BBC News|date=15 March 2002}}</ref>
* John Lennon Lyrics by [[Rigo 23]], Portuguese artist<ref>{{cite news|title=Lennon's Lyrics in Airport Touchdown|url=http://icliverpool.icnetwork.co.uk/0100news/0100regionalnews/tm_objectid=17793722&method=full&siteid=50061&headline=lennon-s-lyrics-in-airport-touchdown-name_page.html|accessdate=13 February 2013|newspaper=IC Liverpool|date=21 September 2006}}</ref>
* The [[Harry Goodwin]] Beatles Collection
* The Beatles in India by [[Paul Saltzman]]
* Two of John Lennon Suits donated by Brian Fisher<ref>{{cite news|title=Collection 08 launched at John Lennon Airport|url=http://www.airportsinformationblog.co.uk/liverpool-airport/collection-08-launched-at-liverpool-airport/|accessdate=13 February 2013|newspaper=UK Airports Information|date=20 November 2007}}</ref>
* My Mommy Was Beautiful by [[Yoko Ono]]<ref>{{cite news|title=Ono's Artwork On Show At Airport|url=http://news.bbc.co.uk/1/hi/england/merseyside/3662260.stm|accessdate=13 February 2013|newspaper=BBC|date=16 September 2004}}</ref>
* The Legends Collection by [[Harry Goodwin]]<ref>{{cite news|last=Williams|first=Liza|title=Iconic Art Joins arrivals at Liverpool Airport|url=http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2007/11/20/iconic-art-joins-arrivals-at-liverpool-airport-99623-20131815/|accessdate=13 February 2013|newspaper=Liverpool Daily Post|date=20 November 2007}}</ref>
* [[Bagism]] Shagism (from [[Give Peace A Chance]])by [[Rigo 23]]
* Music for Airports - [[Brian Eno]], performed by [[Bang on a Can]]<ref>{{cite news|title=Music For Airports in Liverpool|url=http://www.bbc.co.uk/liverpool/content/articles/2006/10/04/music_for_airports_feature.shtml|accessdate=13 February 2013|newspaper=BBC|date=3 October 2008}}</ref>
* The Air Portal Design Exhibition by John McFaul<ref>{{cite news|title=McFaul Brighten's Up Liverpool|url=http://www.thunderchunky.co.uk/articles/mcfaul-brighten-up-liverpool/|accessdate=13 February 2013|newspaper=Thunder Chunky|date=9 January 2008}}</ref>
* The Media Wall by River Media
* Mosaics by Debbie Ryan<ref>{{cite web|last=Ryan|first=Debbie|url=http://debbieryanmosaics.wordpress.com/|work=Mosaics}}</ref>
* Halewood by Barry Worrall
* Airport Route map by Graeme Currie
* The Turner Prize '08 Taxi Project donated by [[Tate Liverpool]]<ref>{{cite news|title=Culture Taxi goes on its Travels|url=http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2008/02/21/culture-taxi-goes-on-its-travels-99623-20502850/|accessdate=13 February 2013|newspaper=Liverpool Daily Post|date=21 February 2008}}</ref>
* Norwegian Wood - Film by Nick Jordan
* Pastures New by Jude MacPherson<ref>{{cite news|title=Super Grass|url=http://www.bbc.co.uk/liverpool/culture/2003/11/airport/index.shtml|accessdate=13 February 2013|newspaper=BBC News|date=7 November 2003}}</ref>
* Save The Rhinos and Save The Elephants - Gilmour Junior School, Garston
* St. Ambrose Primary and Parklands High, Speke - Various Children's works
* 'The Fossils' Heritage collection

== Fossils ==
The Liverpool 08 Collection also extended to the world of geology, perhaps more by chance than by design. On 25 July 2002 Liverpool John Lennon Airport unveiled its new airport terminal building. Although technically JLA is a modern airport, inside is constructed of limestone slabs that have fossils trapped inside of creatures that lived up to 250 million years ago. The limestone at JLA is from near the small town of Solnhofen in South Germany. Although the slabs of the Airport contain millions of fossils, including each of the fossil types: [[Ammonite]]s, Belemnites ([[Belemnitida]]) and [[Trace fossil]].<ref>{{cite journal|author=Liverpool Genological Society|title=Fossils Mystery Tour|year=2009|url=http://www.liverpoolgeologicalsociety.org.uk/JLA_Fossil_Mystery_Tour2009.pdf|accessdate=13 February 2013}}</ref> Featured on the BBC programme, 'Fossil Detectives',<ref>{{cite web|title=Fossil Detectives|url=http://www.bbcshop.com/bin/venda?bsref=bbc&log=22&mode=add&curpage=&next=&ex=co_disp-shopc&buy=demdvd458&invt=demdvd458&ivref=demdvd458&qty=1&x=60&y=17|work=North of England and Scotland|publisher=BBC Shop (DVD)|accessdate=13 February 2013}}</ref> the airport has become popular for school tours both as a busy airport and for its unique fossils.<ref>{{cite web|title=Liverpool Airport Fossils|url=https://www.liverpoolairport.com/news/2008/09/250-million-year-old-airport-shows-how-passengers-can-share-their-footsteps-with-prehistoric-creatures/|accessdate=20 March 2017}}</ref>

== References ==
{{Reflist}}
 
{{coord|53.337075|-2.854646|display=title}}

[[Category:Art exhibitions in the United Kingdom]]
[[Category:2008 in art]]
[[Category:2008 in the United Kingdom]]
[[Category:2008 in England]]