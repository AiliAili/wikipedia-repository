{{Orphan|date=August 2013}}

[[Image:Principle of calcium encoding.tif|thumb|right|x150px|'''Principle of calcium encoding'''. A schematics of intracellular [[calcium signaling]] and its equivalent from a calcium encoding perspective.]]
'''Calcium encoding''' (also referred to  as '''Ca<sup>2+</sup> encoding''' or '''calcium information processing''') is an intracellular [[signaling pathway]] used by many cells to [[information transfer|transfer]], [[information processing|process]] and [[encoding|encode]] external [[information]] detected by the cell. In [[cell physiology]], external information is often converted into [[intracellular]] [[calcium signaling|calcium dynamics]]. The concept of calcium encoding explains how [[calcium|Ca<sup>2+</sup>]] [[ion]]s act as [[intracellular]] [[wikt:messenger|messengers]], relaying information within cells to regulate their activity.<ref name=Berridge_Nature1998 /> Given the ubiquity of Ca<sup>2+</sup> ions in [[cell physiology]], Ca<sup>2+</sup> encoding has also been suggested as a potential tool to characterize cell physiology in health and disease.<ref name="DePitta_CognProc2009" /><ref name="DePitta_PRER2008" /><ref name="DePitta_JOBP2009" /> The mathematical bases of Ca<sup>2+</sup> encoding have been pioneered by work of [http://universityofcalifornia.edu/senate/inmemoriam/joelekeizer.html Joel Keizer] and [http://www.math.umn.edu/~othmer/ Hans G. Othmer] on calcium modeling in the 1990s and more recently they have been revisited by [[Eshel Ben-Jacob]], [http://ctbp.ucsd.edu/affiliates/showperson.php?id=61 Herbert Levine] and co-workers.

==AM, FM and AFM calcium encoding==
[[Image:Elementary modes of calcium encoding.tif|thumb|left|x350px|'''Elementary modes of calcium encoding'''. AM, FM and AFM encoding Ca<sup>2+</sup> oscillations correspond to analogous [[modulation]]s in electronic communications.<ref name=DePitta_PRER2008 />]]
Although elevations of Ca<sup>2+</sup> are necessary for it to act as a signal, prolonged increases of the [[concentration]] of Ca<sup>2+</sup> in the [[cytoplasm]] can be lethal for the cell. Thus cells avoid death usually delivering Ca<sup>2+</sup> signals as brief [[transient (oscillation)|transients]] - i.e. Ca<sup>2+</sup> elevations followed by a rapid [[decay time|decay]] - or in the form of [[oscillation (mathematics)|oscillations]]. In analogy with [[Information Theory]], either the [[amplitude]] or the [[frequency]] or both features of these Ca2+ [[oscillation]]s define the Ca<sup>2+</sup> encoding mode. Therefore, three classes of Ca<sup>2+</sup> signals can be distinguished on the basis of their encoding mode:<ref name="DePitta_CognProc2009" /><ref name="DePitta_PRER2008" /><ref name="Berridge_Nature1997" />
** '''AM encoding''' Ca<sup>2+</sup> signals: when the strength of the [[stimulus (physiology)|stimuli]] is encoded by ''[[amplitude modulation]]s'' of Ca<sup>2+</sup> oscillations. In other words, stimuli of the same nature but of different strength are associated with different amplitudes of Ca<sup>2+</sup> oscillations but the frequencies of these oscillations are similar;
** '''FM encoding''' Ca<sup>2+</sup> signals: when the strength of the [[stimulus (physiology)|stimuli]] is encoded by ''[[frequency modulation]]s'' of Ca<sup>2+</sup> oscillations. In other words, stimuli of the same nature but of different strength are associated with different frequencies of Ca<sup>2+</sup> oscillations but the amplitudes of these oscillations are similar;
** '''AFM encoding''' Ca<sup>2+</sup> signals: when ''both'' AM and FM encoding modes coexist.
Experiments and biophysical modeling, show that the mode of calcium encoding varies from cell to cell, and that a given cell could even show different types of calcium encoding for different patho-physiological conditions.<ref name=Berridge_NatureRev2003 /><ref name=Goldberg_etal_PCB2010 /> This could ultimately provide a crucial tool in [[medical diagnostics]], to characterize, recognize and prevent [[disease]]s.<ref name=Berridge_NatureRev2003 />

==Mathematical aspects of calcium encoding==
Calcium encoding can be mathematically characterized by [[mathematical model|biophysical models]] of [[calcium signaling]].<ref name="FalckeRev2004" /> [[Phase plane]] and [[bifurcation theory|bifurcation analysis]] of these models, can reveal in fact how frequency and amplitude of calcium oscillations vary as a function of any parameter of the model.<ref name="DePitta_PRER2008" /> Occurrence of AM-, FM- or AFM-encoding can be assessed on the extension of the [[range (computer science)|min-max range]] of amplitude and frequency of Ca<sup>2+</sup> oscillations and the bifurcation structure of the [[system]] under study.

[[Image:Stimulus integration by FM-encoding calcium signals.tif|thumb|right|x200px|'''Stimulus integration'''. Only if the stimulus allows ''accumulation'' (i.e. integration) of IP<sub>3</sub> up to a certain threshold then a FM-encoding Ca<sup>2+</sup> oscillation occurs.<ref name=DePitta_JOBP2009 /><ref name=Goldberg_etal_PCB2010 />]]
A critical aspect of Ca<sup>2+</sup> encoding revealed by [[mathematical model|modeling]], is how it depends on the dynamics of the complex [[metabolic network|network of reactions]] of underlying Ca<sup>2+</sup>-mobilizing signals. This aspect can be addressed considering Ca<sup>2+</sup> models that include both Ca<sup>2+</sup> dynamics and the dynamics of Ca<sup>2+</sup>-mobilizing signals. One simple and biophysically realistic model of this kind is the ''ChI model'', originally developed by [[Eshel Ben-Jacob]] and co-workers,<ref name="DePitta_JOBP2009" /> for [[GPCR|GPCR-mediated]] [[inositol trisphosphate|inositol 1,4,5 trisphospate]] (IP<sub>3</sub>)-triggered [[calcium induced calcium release|Ca<sup>2+</sup>-induced Ca<sup>2+</sup>-release]]. The main conclusion of this study was that the dynamics of the Ca<sup>2+</sup>-mobilizing IP<sub>3</sub> signal is essentially AFM encoding with respect to the stimulus whereas Ca<sup>2+</sup> oscillations can be either FM or AFM but not solely AM.<ref name="DePitta_JOBP2009" /> It was argued that the AFM nature of the Ca<sup>2+</sup>-mobilizing IP<sub>3</sub> signal could represent the ideal solution to optimally [[signal transduction|translate]] [[Digital signal (electronics)|pulsed or discontinuous extracellular signals]] into intracellular [[continuous signal|continuous]]-like Ca<sup>2+</sup> oscillations.<ref name="DePitta_JOBP2009" />

==Computational aspects==
Calcium encoding can be confined within a single cell or involve cell ensembles<ref name=FalckeRev2004 /><ref name=ScemesGiaume_Glia2006 />   and deploy essential computational tasks, such as [[stimulus (physiology)|stimulus]] [[Information Integration Theory|integration]]<ref name="DePitta_JOBP2009" /><ref name=TangOthmer_PNAS1995 /> or regulated activation of [[gene transcription]].<ref name="Dolmetsch_Nature1997" /> Moreover, cells are often organized in [[Biological network|networks]], allowing [[cell signaling|intercellular propagation]] of [[calcium signaling]].<ref name="ScemesGiaume_Glia2006" /> With this regard, the same mode of calcium encoding could be shared by different cells, providing [[synchronization]]<ref name="DePitta_JOBP2009" /> or the functional basis to implement more complex computational tasks.<ref name="Goldberg_etal_PCB2010"/>

Michael Forrest has shown that intracellular calcium dynamics may permit a [[Purkinje neuron]] to perform [[toggle mechanism|toggle]] and gain computations upon its inputs.<ref>{{cite journal |author=Forrest MD |title=Intracellular Calcium Dynamics Permit a Purkinje Neuron Model to Perform Toggle and Gain Computations Upon its Inputs. |journal=Frontiers in Computational Neuroscience |volume=8 |pages=86 |year=2014 | doi=10.3389/fncom.2014.00086 | url=http://journal.frontiersin.org/Journal/10.3389/fncom.2014.00086/full |pmid=25191262 |pmc=4138505}}</ref> So, showing how an ion concentration can be used as a computational variable - in particular, as a memory element: recording a history of firing and inputs, to dictate how the neuron responds to future inputs. Thus, this work hypothesizes that the [[membrane potential]] (V) is not the Purkinje cell's only coding variable but works alongside a calcium memory system. These two interact, with the calcium memory being encoded and decoded by the membrane potential. The toggle and gain computations are likely to be salient to network computations in the [[cerebellum]], in the [[brain]]. Thus, ion computations can be important more globally than a single cell. Forrest terms this hypothesis: "ion to network computation".

==References==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{reflist|
refs=

<ref name="Goldberg_etal_PCB2010">{{
cite journal
| authors  = Goldberg, M. and De Pittà, M. and Volman, V. and Berry, H. and Ben-Jacob, E.
| title   = Nonlinear gap junctions enable long-distance propagation of pulsating calcium waves in astrocyte networks
| journal = PLoS Comput. Biol.
| year    = 2010
| volume  = 6
| pages   = e1000909
| issue  = 8
| doi = 10.1371/journal.pcbi.1000909
}}</ref>

<ref name="DePitta_JOBP2009">{{
cite journal
| authors  = De Pittà, M. and Goldberg, M. and Volman, V. and Berry, H. and Ben-Jacob, E.
| title   = Glutamate-dependent intracellular calcium and IP<sub>3</sub> oscillating and pulsating dynamics in astrocytes
| journal = J. Biol. Phys.
| year    = 2009
| volume  = 35
| pages   = 383–411
| doi     = 10.1007/s10867-009-9155-y
}}</ref>

<ref name="DePitta_CognProc2009">{{
cite journal
| authors  = De Pittà, M. and Volman, V. and Levine, H. and Ben-Jacob, E.
| title   = Multimodal encoding in a simplified model of intracellular calcium signaling
| journal = Cognitive Processing
| year    = 2009
| volume  = 10
| pages   = 55–70
| issue  = S1
| doi     = 10.1007/s10339-008-0242-y
}}</ref>

<ref name="DePitta_PRER2008">{{
cite journal
| authors   = De Pittà, M. and Volman, V. and Levine, H. and Pioggia, G. and De Rossi, D. and Ben-Jacob, E.
| title    = Coexistence of amplitude and frequency modulations in intracellular calcium dynamics
| journal  = Phys. Rev. E
| year     = 2008
| volume   = 77
| pages    = 030903(R)
| issue   = 3
| doi      = 10.1103/PhysRevE.77.030903
}}</ref>

<ref name="Berridge_Nature1997">{{
cite journal
| author  = Berridge, M.J.
| title   = The AM and FM of calcium signaling
| journal = Nature
| year    = 1997
| volume  = 386
| pages   = 759–760
| pmid    = 9126727
| doi=10.1038/386759a0
| issue=6627
}}</ref>

<ref name="Dolmetsch_Nature1997">{{
cite journal
| authors  = Dolmetsch, R. E. and Lewis, R. S. and Goodnow, C. C. and Healy, J. I.
| title   = Differential activation of transcription factors induced by Ca<sup>2+</sup> response amplitude and duration
| journal = Nature
| year    = 1997
| volume  = 386
| pages   = 855–858
| issue  = 24
| doi     = 10.1038/386855a0
}}</ref>

<ref name="Berridge_Nature1998">{{
cite journal
| authors  = Berridge, M. J. and Bootman, M. D. and Lipp, P.
| title   = Calcium – a life and death signal
| journal = Nature
| year    = 1998
| volume  = 395
| pages   = 645–648
| doi     = 10.1038/27094
}}</ref>

<ref name="Berridge_NatureRev2003">{{
cite journal
| authors  = Berridge, M. J. and Bootman, M. D. and Roderick, H. L.
| title   = Calcium signalling: dynamics, homeostasis and remodelling
| journal = Nature Reviews Molecular Cell Biology
| year    = 2003
| volume  = 4
| pages   = 517–529
| doi     = 10.1038/nrm1155
}}</ref>

<ref name="ScemesGiaume_Glia2006">{{
cite journal
|author1=Scemes, E.  |author2=Giaume, C.
| title   = Astrocyte calcium waves: What they are and what they do
| journal = Glia
| year    = 2006
| volume  = 54
| pages   = 716–725
| doi     = 10.1002/glia.20374
}}</ref>

<ref name="FalckeRev2004">{{
cite journal
| author  = Falcke, M.
| title   = Reading the patterns in living cells – The physics of Ca<sup>2+</sup> signaling
| journal = Adv. Phys.
| year    = 2004
| volume  = 53
| pages   = 255–440
| issue  = 3
| doi     = 10.1080/00018730410001703159
}}</ref>

<ref name="TangOthmer_PNAS1995">{{
cite journal
|author1=Tang, Y.  |author2=Othmer, H. G.
| title    = Frequency encoding in excitable systems with applications to calcium oscillations
| journal  = Proc. Natl. Acad. Sci. USA
| year     = 1995
| volume   = 92
| pages    = 7869–7873
| pmc    = 41247
| pmid=7644505
| issue=17
| doi=10.1073/pnas.92.17.7869
}}</ref>

}}

==External links==
* [http://www.biochemj.org/csb/ Cell Signaling Biology]
* [http://star.tau.ac.il/~eshel/ Prof. Eshel Ben-Jacob's homepage]
* [http://ctbp.ucsd.edu/affiliates/showperson.php?id=61 Prof. Herbert Levine's homepage]
* [http://www.inrialpes.fr/Berry/ Prof. Hugues Berry's homepage]
* [http://cnl.salk.edu/~volman/ Dr. Vladislav Volman's homepage]
* [http://sites.google.com/site/mauriziodepitta/ Dr. Maurizio De Pittà's homepage]

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Calcium]]