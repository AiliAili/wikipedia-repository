{{For|''Development'', the journal since 1957 of the Society for International Development|Society for International Development#SID Journal Development}}
{{Infobox journal
| title = Development
| cover = [[File:DEV 138 issue 8.jpg]]
| editor = 
| discipline = [[Developmental biology]]
| abbreviation =
| formernames = Journal of Embryology and Experimental Morphology
| publisher = [[The Company of Biologists]]
| country = [[United Kingdom]]
| frequency = 24/year
| history = 1953–present
| openaccess = After 6 months
| license = 
| impact = 6.059
| impact-year = 2015
| website = http://dev.biologists.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 15088415
| LCCN = 
| CODEN = 
| ISSN = 0950-1991
| eISSN = 1477-9129
| boxwidth = 
}}
'''''Development''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of [[developmental biology]] that covers cellular and molecular mechanisms of animal and plant development. Topics covered include [[stem cell]]s and nuclear reprogramming, [[regional specification]], [[morphogenesis]], [[organogenesis]], [[evolution]] of the developmental process, and developmental studies of disease.

In 2009, the BioMedical & Life Sciences Division of the [[Special Libraries Association]] included ''Development'' in their list of top 100 journals in Biology and Medicine over the last 100 years.<ref>[http://units.sla.org/division/dbio/publications/resources/dbio100.html Top 100 Journals in Biology and Medicine] {{webarchive |url=https://web.archive.org/web/20120415140438/http://units.sla.org/division/dbio/publications/resources/dbio100.html |date=April 15, 2012 }} (accessed 10 February 2011)</ref> It is published by [[The Company of Biologists]].

==Brief history==
Originally called ''Journal of Embryology and Experimental Morphology'' ({{ISSN|0022-0752}}) and established in 1953, the journal provided a periodical that would be primarily devoted to [[morphogenesis]].<ref>[http://dev.biologists.org/cgi/reprint/133/1/1 Smith J. (2006) ''Development'': moving on in 2006 (Editorial) ''Development'' 133: 1] (accessed 17 April 2008)</ref><ref>[http://dev.biologists.org/cgi/reprint/134/1/1 Smith J. (2007) ''Development'': new developments and sad goodbyes (Editorial) ''Development'' 134: 1]</ref> In 1987, the journal was renamed ''Development''. The journal’s full archive from 1953 is available online. ''Development'' now publishes 24 issues annually and content over 6 months old is freely available.

==Past editors==
''Journal of Embryology and Experimental Morphology''
*1953–1978: [[Michael Abercrombie]]
*1979–1982: [[R. J. Cole]], [[R. L. Gardner]], [[R. M. Gaze]], [[P. A. Lawrence]]
*1983–1987: R. L. Gardner, R. M. Gaze, P. A. Lawrence, [[H. R. Woodland]]

''Development''
*1987–2002: [[Chris Wylie]]
*2003–2009: [[Jim Smith (biologist)|Jim Smith]]
*2010–present: [[Olivier Pourquie]]

==References==
{{reflist}}

==External links==
*{{Official website|http://dev.biologists.org/}}

{{DEFAULTSORT:Development (Journal)}}
[[Category:Developmental biology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1953]]
[[Category:English-language journals]]
[[Category:Biweekly journals]]
[[Category:The Company of Biologists academic journals]]