[[File:Afar Chief.jpg|thumb|right|Jon Kalb with an [[Afar people|Afar]] chief]]

'''Jon Kalb''', born August 17, 1941, in [[Houston]], [[Texas]], was a research geologist with the Vertebrate Paleontology Laboratory (Texas Memorial Museum), [[University of Texas at Austin]]. He received a pre-doctoral fellowship from the
[[Carnegie Geophysical Laboratory]] in 1968, a graduate fellowship from Johns Hopkins University in 1969, and a BSc from [[American University]] in 1970.<ref>{{cite web|last=The Expedition School|title=Jon Kalb|url=http://www.expeditionschool.com/about-us/our-faculty/jon-kalb/|publisher=The Expedition School|accessdate=19 October 2011}}</ref>

==Early Experience==

As a teenager Kalb began his career with a Mexican-American expedition searching
for early shipwrecks off the coast of [[Yucatan]].<ref>{{cite book|last=Blair Jr.|first=Clay|title=Diving for Treasure and Pleasure|year=1960|publisher=Cleveland: World Publishing}}</ref> He later joined famed treasure hunter and marine archeologist [[Robert F. Marx|Bob Marx]] exploring reefs in the [[Caribbean]].<ref>{{cite book|last=Marx|first=Robert F.|title=Always Another Adventure|year=1967|publisher=Cleveland: World Publishing Co.}}</ref>

Sidelined by injuries from diving, Kalb was sent to the west coast of [[South America]]
by the [[Smithsonian]] to collect marine fauna.<ref>{{cite journal|last=Kearns|first=Kevin C. A.|title=A transisthmian sea-level canal for Central America:|journal=Journal of Geography|year=1971|volume=170|pages=235|doi=10.1080/00221347108981626}}</ref> He then joined a team of geologists with the [[U.S. Army Corps of Engineers]] in northwest [[Colombia]] mapping a potential
route for a sea-level canal,<ref>{{cite journal|last=U.S. National Science Foundation|title=Results of the Southeast Pacific|journal=R/V Anton Bruun|year=1970|series=Cruise 18 A and B}}</ref> which led him to prospect for gold on the Guinean
Shield for the [[Guyana]] Geological Survey.<ref>{{cite journal|last=Fargher|first=Malcolm|title=United Nations Mineral Survey. Phase II. Interim report. "Geology of the Demerara bend area"|journal=Guyana Geological|year=1968}}</ref> While at Johns Hopkins he became interested in the plate tectonics of the [[Afar Depression]], a triple (rift) junction in northeastern [[Ethiopia]].<ref>{{cite journal|last=Tazieff|first=Haroun|title=The Afar Triangle|journal=Scientific American|year=1970|volume=222|pages=32–40|doi=10.1038/scientificamerican0270-32}}</ref><ref>{{cite journal|last=MacKenzie|first=D. P.|author2=D. Davies |author3=P. Molnar |title=Plate tectonics of the Red Sea and|journal=Nature|year=1970|volume=226|pages=243–248|doi=10.1038/226243a0 }}</ref> In 1971 he moved to [[Addis Ababa]] with his family and over the next seven years explored the [[Awash Valley]] in the central and western
Afar.

==Discoveries==

Kalb was a founder of the International Afar Research Expedition that recovered the
3.2 million year old [[Lucy (Australopithecus)|Lucy]] skeleton,<ref>{{cite journal|last=Taieb|first=M. |author2=Co Y. |author3=Johanson, D. |author4=Kalb, J. |title=Dépôts sédimentares et dufaunes du Plio-pléistocéne de la basse vallée de l’Awash|journal=Comptes rendus des séances de l’Académie des sciences|year=1972|volume=275D|pages=819–822}}</ref><ref>{{cite journal|last=Taieb|first=M. |author2=Co Y. |author3=Johanson, D. |author4=Kalb, J |title=Dépôts sédimentares faunes du Plio-pléistocéne de la basse vallée de l’Awash|journal=Comptes rendus des séances de l’Académie des sciences|year=1974|volume=275D|pages=819–822}}</ref><ref>{{cite book|last=Johanson|first=D.C.|title=Lucy: The Beginnings of Humankind|year=1981|publisher=Simon and Schuster|location=New York}}</ref> and later director of the Ethiopia-based mission that pioneered explorations in the Middle Awash, revealing some of the
most prolific deposits bearing early hominin fossils and artifacts in the world.<ref>{{cite journal|last=Kalb|first=J.E.|author2=E. Oswald |author3=S. Tebedge |title=Geology and stratigraphy of Neogene deposits in the Middle Awash Valley, Afar, Ethiopia|journal=Nature|year=1982|volume=298|pages=17–25|doi=10.1038/298017a0}}</ref><ref>{{cite journal|last=Kalb|first=J.E.|author2=Jolly, C. J. |author3=Mebrate, Assefa |title=Fossil mammals and artifacts from the Middle Awash Valley, Ethiopia|journal=Nature|year=1982|volume=298|pages=25–29|doi=10.1038/298025a0}}</ref> Discoveries included a nearly complete hyper-robust skull of a 600,000-year-old pre-Neanderthal;<ref>{{cite journal|last=Kappelman|first=John|title=The evolution of body mass and relative brain size in fossil hominids|journal=Journal of Human Evolution|year=1996|volume=30|pages=243–276|doi=10.1006/jhev.1996.0021 }}</ref> and a 4.4 million-year-old fossil skeleton ''[[Ardipithecus]]'' found by Tim White.<ref>{{cite journal|last=Gibbons|first=Ann|title=new kind of ancestor: Ardipithecus unveiled|journal=Science|year=2009|volume=326|pages=36–43|doi=10.1126/science.326.5949.36}}</ref> From the Middle Awash site Kalb and Assefa Mebrate described the most complete known record of ancestral elephants (18 species) from a single area,<ref>{{cite journal|last=Kalb|first=J.E.|author2=A. Mebrate|title=Fossil elephantoids from the hominid-bearing Awash Group, Middle Awash Valley, Afar Depression, Ethiopia|journal=Transactions of the American Philosophical Society|year=1993|volume=83|series=1|pages=1–120|doi=10.2307/1006558}}</ref> which fauna serve as an analog to other equally diverse faunal groups recovered from the region, including [[hominids]] and the earliest [[hominins]]. Scores of archeological localities were found, ranging in time from the [[late Pliocene]] with the earliest stone tools to [[late Pleistocene]] sites containing pottery.<ref>{{cite journal|last=Kalb|first=J.E.|author2=Jolly, C. J. |author3=Oswald, E. B. |author4=Whitehead, P |title=Early hominid habitation in Ethiopia|journal=American Scientist|year=1984|volume=72|pages=168–178}}</ref><ref>{{cite journal|last=Clark|first=J.D.|author2=B. Asfaw|title=Paleoanthropological discoveries in the Middle Awash Valley, Ethiopia|journal=Nature|year=1984|volume=307|pages=423–428|doi=10.1038/307423a0}}</ref> In a recent publication Kalb proposed that the illusive land of Punt—a trading partner with ancient Egypt—was situated in the central Afar, a short trek from the
Gulf of Tadjura.<ref>{{cite journal|last=Kalb|first=Jon|title=Awsa and Punt: Into the mix. Nyame Akuma|journal=Bulletin of the|year=2010|volume=71|pages=31–34}}</ref>

==Conflicts==

After Kalb established a model-training program for Ethiopian students, and the
first paleobiology research laboratory in the country, he was expelled from Ethiopia
in mid-1978 amid fabricated allegations he spied for the [[CIA]].<ref>{{cite news|title=Suit on rumor of tie to C.I.A. brings apology to geologist|newspaper=New York Times|date=December 5, 1987}}</ref> In 1977 the U.S. [[National Science Foundation]] declined funds to Kalb’s team based on these same charges, as revealed by documents he obtained under the [[Freedom of Information Act]].<ref>{{cite book|last=Bell|first=Robert|title=Impure Science: Fraud, Compromise, and Political influence in Scientific Research|year=1992|publisher=Wiley}}</ref> A year later he won a court stipulated settlement with NSF concluding that he was denied a fair hearing under the Privacy Act.<ref>{{cite journal|last=Marshall|first=Eliot|title=Gossip and peer review at NSF|journal=Science|year=1987|volume=238|issue=4833 |pages=1502|doi=10.1126/science.3120315}}</ref> A year later he successfully petitioned NSF under the [[First Amendment to the United States Constitution|First Amendment]] to reform its peer review system.<ref>{{cite journal|last=Raloff|first=Janet|title=Revamping peer review: the National Science Foundation will allow more peering into its reviews|journal=Science News|date=April 14, 1990}}</ref>

==Recent Years==

Following more trips to Africa—joining teams with the USGS, the Technical
University of Berlin, and the University of Vienna—Kalb renewed surveys for
Eocene mammals begun in the 1930s along the remote borderlands of West Texas
.<ref>{{cite journal|last=Stovall|first=J.W.|title=Chadron vertebrate fossils below rim of Presidio, County, Texas|journal=American Journal of Science|year=1984|volume=246|pages=78–95|doi=10.2475/ajs.246.2.78}}</ref> Described as the “American Afar,” the region is hot, wild, and minced by faults
of the Rio Grande rift with parallels to the “African Afar.” To date the area has
produced over 4000 extinct mammals, including some of the last known primates in
North America.<ref>{{cite journal|last=Wilson|first=John Andrew|title=Stratigraphic occurrence and correlation of early Tertiary vertebrate faunas, Trans-Pecos, Texas. Part 1: Vieja Area|journal=Texas Memorial Museum Bulletin|year=1977|volume=25|pages=1–42}}</ref>

==Awards==

Robert W. Hamilton Award. University of Texas at Austin. For non-fiction,
''Adventures in the Bone Trade'', 2002

Violet Crown Award, Writers League of Texas. For non-fiction,
''Adventures in the Bone Trade'', 2001.

Court Stipulated Settlement, Kalb vs National Science Foundation. D.D.C., Civ. No.
86-3557, 8 December 1987.

==Select Bibliography==
*Kalb, Jon. 2011. ''Hunting Tapir During the Great Flood, And Other Tales of Exploration and High Adventure''. Special Delivery Books, Alpine, Texas. 288pp.
*Kalb, Jon. 2001. ''Adventures in the Bone Trade: The Race to Discover Early Human Ancestors in Ethiopia’s Afar Depression''. Copernicus Books (imprint of Springer-Verlag) 389pp.
*Kalb, Jon, et al. 2000. ''Bibliography of the Earth Sciences for the Horn of Africa: Ethiopia, Eritrea, Somalia, and Djibouti 1620-1993''. American Geological Institute, Alexandria, Virginia. 149pp.
*Kalb, J. E., D.J. Froehlich, and G. L. Bell. 1996. ''Phylogeny of African and Eurasian Elephantoidea of the late Neogene''. Chapter 12B, 117-123. In: ''The Proboscidea—Trends in Evolution and Paleoecology'', Eds. J. Shoshani and P. Tassy. Oxford University Press.
*{{cite journal | last1 = Froehlich | first1 = D. J. | last2 = Kalb | first2 = J. E. | year = 1995 | title = Three dimensional reconstruction of elephantoid molars: applications for functional anatomy and systematics | url = | journal = Paleobiology | volume = 21 | issue = 3| pages = 379–392 }}
*{{cite journal | last1 = Kalb | first1 = J. E. | year = 1995 | title = Fossil elephantoids, Awash paleolake basins, and the Afar triple junction, Ethiopia | url = | journal = Palaeogeography, Palaeclimatology, Palaeoecology | volume = 114 | issue = | pages = 357–368 | doi=10.1016/0031-0182(94)00088-p}}
*{{cite journal | last1 = Kalb | first1 = J. E. | year = 1993 | title = Refined stratigraphy of the hominid-bearing Awash Group, Afar Depression, Ethiopia | url = | journal = Newsletters on Stratiigraphy | volume = 29 | issue = 1| pages = 21–62 }}
*{{cite journal | last1 = Kalb | first1 = J. E. | last2 = Jolly | first2 = C. J. | last3 = Tebedge | first3 = Sleshi | display-authors = etal   | year = 1992 | title = Vertebrate faunas from the Awash Group, Middle Awash Valley, Afar, Ethiopia | url = | journal = Journal of Vertebrate Paleontology | volume = 2 | issue = 1| pages = 237–238 | doi=10.1080/02724634.1982.10011932}}
*{{cite journal | last1 = Kalb | first1 = J. E. | last2 = Oswald | first2 = E. B. | last3 = Mebrate | display-authors = etal   | title = , 1982. ''Stratigraphy of the Awash Group, Middle Awash Valley, Afar, Ethiopia'' | url = | journal = Newsletters on Stratigraphy | volume = 11 | issue = | pages = 95–127 }}

==Fiction==

Kalb, Jon. 2007. ''The Gift. Discovery, Treachery, and Revenge''.
Special Delivery Books, Alpine, Texas. Reviewed by Nature 451: 128. Also see: [http://www.LabLit.com LabLit.com]

== References ==
{{reflist}}

==External links==
* http://findarticles.com/p/articles/mi_m1134/is_5_110/ai_75247899/ [Adventures in the BoneTrade  John van Couvering, Book Review]
* http://www.tandfonline.com/doi/abs/10.1080/089896292085738217 [Bias Awarding Scientific Grants, T. O. McGarity]

{{DEFAULTSORT:Kalb, Jon}}
[[Category:1941 births]]
[[Category:Living people]]
[[Category:People from Houston]]
[[Category:American geologists]]