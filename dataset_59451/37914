{{Use dmy dates|date=March 2016}}
{{EngvarB|date=March 2016}}
{{good article}}
{{Infobox film
|  name           = Bionicle: Mask of Light
|  image          = Bioniclemask.jpg
|  writer         = {{Plainlist|
*[[Bob Thompson (producer writer)|Bob Thompson]]
*[[Alastair Swinnerton]]
*[[Henry Gilroy]]
*[[Greg Weisman]]
}}
|  starring       = {{Plainlist|
*[[Jason Michas]]
*[[Andrew Francis]]
*[[Scott McNeil]]
*Dale Wilson
*[[Kathleen Barr]]
*[[Lee Tockar]]
}}
|  director       = {{Plainlist|
*Terry Shakespeare
*David Molina
}}
|  producer       = {{Plainlist|
*Sue Shakespeare
*Janice Ross
*Stig Blicher
}}
|  music          = [[Nathan Furst]]
|  editing        = Craig Russo
|  studio         = {{Plainlist|
*[[Lego]]
*Create TV & Film
*[[Creative Capers Entertainment]]
*CGGC
}}
|  distributor    = {{Plainlist|
*[[Miramax Home Entertainment]]
*[[Walt Disney Studios Home Entertainment|Buena Vista Home Entertainment]]
}}
|  released       = 16 September 2003
|  runtime        = 70 minutes
|  country        = {{Plainlist|
*[[Denmark]]
*[[United Kingdom]]
*[[United States]]
*[[Taiwan]]
}}
|  language       = English
|  budget         = US$3.5—5 million
}}
'''''Bionicle: Mask of Light''''', stylized as '''''BIONICLE: Mask of Light&nbsp;— The Movie''''', is a 2003 [[direct-to-video]] [[science fantasy]] [[action film]] based on the [[Bionicle]] toy series created by [[Lego]], being the very first film to be based on a Lego product and the first Lego-related feature film. Set in a universe filled with bio-mechanical beings allied with [[classical element]]-themed tribes, the story follows two friends from the fire-based village of Ta-Koro on a quest to find the owner of the Mask of Light, a mystical artifact that can potentially defeat Makuta, an evil entity threatening the island. The story is based on the latter half of the toyline's 2003 narrative.

The project was first proposed in 2001, during the original run of the Bionicle toyline. Lego contacted multiple writers for the project, including original Bionicle contributors [[Bob Thompson (producer writer)|Bob Thompson]] and [[Alastair Swinnerton]], and Hollywood writers [[Henry Gilroy]] and [[Greg Weisman]]. Production began in 2002, taking approximately a year to complete. A major part of the graphical design was adjusting the characters so they could work in human-like ways while still resembling the original toys. The music was composed by [[Nathan Furst]], who used orchestral and tribal elements to create the score. Voice casting was handled by [[Kris Zimmerman]], and the voicework was done with the setting and mythos of Bionicle in mind.

Multiple studios were involved in the development and distribution of ''Mask of Light'': it was co-produced by Lego and Create TV & Film, developed by [[Creative Capers Entertainment]] and CGCG, and post-production was handled 310 Studios and Hacienda Post. It was released in September 2003 on [[home video]] and [[DVD]], distributed by [[Miramax Films]] and [[Walt Disney Studios Home Entertainment|Buena Vista Home Entertainment]]. Upon release, the film reached high positions in VHS and DVD charts, and received generally positive reviews from journalists. Following ''Mask of Light'', further films based on Bionicle have been released.

==Plot==
''The Mask of Light'' takes place on a tropical island in the [[Bionicle]] universe. According to legend, the [[List of characters in Bionicle#Great Spirit Mata Nui|Great Spirit Mata Nui]] created the island's masked Matoran inhabitants. Mata Nui was sent into a coma by his envious spirit brother [[List of characters in Bionicle#Brotherhood of Makuta|Makuta]], who began a reign of terror on the island until six guardians known as Toa freed the island from his regime. The Matoran, alongside the Toa and Turaga leaders, live in Element-themed regions of the island. The events of ''The Mask of Light'' takes place during the latter half of the toyline's 2002–2003 narrative.<ref name="BZPinterview"/><ref name="GilroyInterview"/>

The film starts when two Matoran from the fire village of Ta-Koro called Jaller and Takua discover a Great Kanohi, a Toa mask imbued with Elemental power. The two Matoran then participate in a multi-tribal game of Kohlii, the island's national sport:<ref name="SciBionicle"/> the match reveals developing tensions between the Fire Toa Tahu and the Water Toa Gali. At the end of the match, the Mask is accidentally revealed and the Turaga recognize its powers. They announce that it heralds the arrival of a seventh Toa destined to defeat Makuta and awaken Mata Nui. Jaller and Takua are sent on a quest to find the Seventh Toa, guided by the Mask. In the meantime, Makuta sends three of his [[List of characters in Bionicle#Rahkshi|Rahkshi]] "sons" to retrieve the Mask. During an attack on Ta-Koro which destroys the village, Tahu is poisoned, causing him to become increasingly erratic and worsening his already-strained relationship with Gali.

During their journey together, Jaller and Takua receive aid from the Air Toa Lewa, and the Ice Toa Kopaka, the latter of whom temporarily immobilizes the Rakhshi by trapping them in a frozen lake. After this, Takua is threatened by Makuta, and abandons Jaller in an attempt to shield him. Makuta then releases three more Rakhshi, who attack the Earth village of Onu-Koro as Takua arrives. Tahu, Kopaka, Lewa and Gali arrive to help the Earth Toa Onua and the Stone Toa Pohatu: during the battle, Tahu is further corrupted and goes insane, forcing the Toa to capture him and flee. Takua decides to rejoin Jaller, while Gali and the other Toa purge the Rakhshi poison from Tahu, resulting in his reconciliation with Gali.

Arriving at Kini Nui, a great temple at the island's centre, Jaller and Takua are confronted by all six Rahkshi. The six Toa mount a united offensive and defeat five of them, but a surviving Rahkshi attacks Takua. Jaller sacrifices himself to protect Takua, and Jaller's final words prompt Takua to don the Mask of Light: the Mask transforms him into Takanuva, the Toa of Light. Defeating the final Rakhshi, he constructs a craft powered by the worm-like creatures inside the Rahkshi to guide him to Makuta. Traveling to his lair beneath Mata Nui, the two hold a Kohlii contest for the island's fate. At Takanuva's bidding, the Toa, Turaga and Matoran gather together in the chamber, and witness Takanuva merging with Makuta to form a single powerful being. With Takanuva's willpower dominant, the being raises a gate leading deeper beneath the island, through which the gathered people flee. The being also revives Jaller before the gate collapses on top of it. The Turaga proceed to awaken Mata Nui using the Mask of Light, which in turn revives Takanuva. The film ends with Takanuva discovering [[Bionicle 2: Legends of Metru Nui|the long-dormant city of Metru Nui]], the Matoran's original home.

==Characters==
*[[Jason Michas]] as  Takua/Takanuva. Portrayed as an inquisitive Ta-Matoran, he is a disguised Av-Matoran (Matoran of Light) destined to become the Toa of Light.<ref name= "BtVAbionicle">{{cite web|url=http://www.behindthevoiceactors.com/movies/Bionicle-Mask-of-Light-The-Movie/side-by-side/|title=Behind the Voice Actors&nbsp;— Bionicle: Mask of Light: The Movie|publisher=Behind the Voice Actors|accessdate=15 July 2014|archiveurl=https://web.archive.org/web/20151230224714/http://www.behindthevoiceactors.com/movies/Bionicle-Mask-of-Light-The-Movie/side-by-side/|archivedate=30 December 2015|deadurl=no}}</ref><ref>{{cite book |title=Bionicle Encyclopedia: Updated|author=Farshtey, Greg|publisher=[[Scholastic Corporation|Scholastic]]|isbn=0-439-91640-2|year=2007|pages=138–139}}</ref>
*[[Andrew Francis]] as Jaller, the Captain of Ta-Koro's Guard, who is designated as the Herald of the Seventh Toa.<ref name= "BtVAbionicle"/><ref>{{cite book |title=Bionicle Encyclopedia: Updated|author=Farshtey, Greg|publisher=[[Scholastic Corporation|Scholastic]]|isbn=0-439-91640-2|year=2007|page=49}}</ref>
*[[Scott McNeil]] as Tahu, the headstrong Toa of Fire; Onua, the wise Toa of Earth; and Graalok the Ash Bear, a beast from Lewa's domain.<ref name= "BtVAbionicle"/>
*Dale Wilson as Lewa Nuva, the Toa of Air; and Turaga Onewa, leader of the Po-Matoran.<ref name= "BtVAbionicle"/>
*[[Kathleen Barr]] as Gali Nuva, the Toa of Water.<ref name= "BtVAbionicle"/>
*[[Lee Tockar]] as the Makuta, the main antagonist; Takutanuva, a being created from the merging of Takanuva and Makuta; and Pewku, Takua's pet racing crab.<ref name= "BtVAbionicle"/>
*[[Christopher Gaze]] as Turaga Vakama, the leader of the Ta-Matoran.<ref name= "BtVAbionicle"/>
*Lesley Ewen as Turaga Nokama, the leader of the Ga-Matoran.<ref name= "BtVAbionicle"/>
*[[Michael Dobson (actor)|Michael Dobson]] as Kopaka Nuva, the Toa of Ice; Hewkii, a Po-Matoran Kohlii player.<ref name= "BtVAbionicle"/>
*[[Trevor Devall]] as Pohatu Nuva, the friendly Toa of Stone.<ref name= "BtVAbionicle"/><ref>{{cite book |title=Bionicle Encyclopedia: Updated|author=Farshtey, Greg|publisher=[[Scholastic Corporation|Scholastic]]|isbn=0-439-91640-2|year=2007|page=114}}</ref>
*[[Chiara Zanni]] as Hahli, a Ga-Matoran and close friend of Jaller.<ref name= "BtVAbionicle"/><ref>{{cite book |title=Bionicle Encyclopedia: Updated|author=Farshtey, Greg|publisher=[[Scholastic Corporation|Scholastic]]|isbn=0-439-91640-2|year=2007|pages=41–42}}</ref>
*Julian B. Wilson as the Ta-Matoran Guard, and the Rahkshi (vocal effects), mechanical "sons" of Makuta driven by fragments of his being.<ref name= "BtVAbionicle"/><ref>{{cite book |title=Bionicle: Makuta's Guide to the Universe|author=Farshtey, Greg|publisher=Ameet|isbn=83-253-0350-6|year=2009|page=26}}</ref>
*Doc Harris as Kolhii announcer.<ref name= "BtVAbionicle"/>

==Production==
The concept for a movie based on the Bionicle toyline was proposed as early as 2001, when Bionicle became an unexpected commercial success for Lego: this idea was originally inspired by the fact that Lego advertised Bionicle as if it were a movie, and they had received inquiry emails and licensing requests from multiple film studios. Deciding to make the movie while retaining full creative control, Lego began discussing with potential partners, knowing that the film needed to come out while Bionicle was still looming large in the public consciousness.<ref name="BZPinterview">{{cite web|url=http://www.bzpower.com/story.php?ID=1380|title=A Visit to Creative Capers|publisher=BZPower.com|date=17 September 2003|accessdate=29 December 2015|archiveurl=https://web.archive.org/web/20151231125544/http://www.bzpower.com/story.php?ID=1380|archivedate=31 December 2015|deadurl=no}}</ref> Lego's eventual partner was production entity Create TV & Film. With the initial partnership set up, Lego created some development assets and went round various animation studios to find one that would develop the film. They narrowed it down to two studios, one of which was [[Creative Capers Entertainment]]. Creative Capers convinced Lego to employ them after producing a well-received short animated segment featuring Lewa. Creative Caper's three principles, directors Terry Shakespeare and David Molina, and producer Sue Shakespear, all took up their respective roles for the film's production. The deal was reportedly worth $5&nbsp;million.<ref name="BZPinterview"/><ref name= "BioMakingOf">{{cite web|url=http://www.awn.com/vfxworld/bionicle-universe-expands-home-entertainment|title=The 'Bionicle' Universe Expands on Home Entertainment|author=Maestri, George|publisher=[[Animation World Network]]|date=16 September 2003|accessdate=20 August 2015|archiveurl=https://web.archive.org/web/20150820114952/http://www.awn.com/vfxworld/bionicle-universe-expands-home-entertainment|archivedate=20 August 2015|deadurl=no}}</ref> In a later interview, Shakespeare referred to the film's aesthetic as "primary colors that were coded to the areas", saying it had a "younger feel" when compared to its prequel. He also referred to the film as "very intimate, very organic".<ref>{{cite web | author=Desowitz, Bill | date=20 October 2004| title=Bionicle 2 DVD Opens Up Characters and Environments | publisher=[[Animation World Network]]| url=http://www.awn.com/news/home-entertainment/bioncle-2-dvd-opens-characters-and-environments | accessdate=26 June 2006|archiveurl=https://web.archive.org/web/20110807161940/http://www.awn.com/news/home-entertainment/bioncle-2-dvd-opens-characters-and-environments|archivedate=7 August 2011|deadurl=no}}</ref>

The film began full production in 2002. While most projects of its type took 18–24 months to complete, the development team completed the film in 13&nbsp;months.<ref name= "BioMakingOf"/> The project's budget was later estimated as being between $3.5 to $5&nbsp;million.<ref name="ANWbudget">{{cite web|url=http://www.awn.com/animationworld/sizing-promise-animation-direct-video|title=Sizing Up the Promise of Animation in Direct-to-Video|author=Singer, Gregory|publisher=[[Animation World Network]]|date=24 March 2005|accessdate=31 December 2015|archiveurl=https://web.archive.org/web/20150115115615/http://www.awn.com/animationworld/sizing-promise-animation-direct-video|archivedate=15 January 2015|deadurl=no}}</ref> In addition to Creative Capers, Taiwanese studio CGCG created most of the animation: at the time, overseas outsourcing was a rarity for CGI movies.<ref name= "BioMakingOf"/> By the end of production, the film ran to 77&nbsp;minutes of raw footage. At this stage, 310 Studios was brought in to handle post production, which mostly entailed cutting the film's length down by 7&nbsp;minutes. According to 310 Studios president Billy Jones, the hardest part of this was deciding which pieces needed to be cut, as they were impressed with everything that had been produced. 310 Studios also created the opening title and ending credit sequences.<ref name="BioPost"/><ref>{{cite web|url=http://www.310studios.com/310_Studios/Bionicle_1.html|title=Visual FX&nbsp;— Bionicle: Mask of Light|publisher=310 Studios|accessdate=30 December 2015|archiveurl=https://web.archive.org/web/20150203211823/http://www.310studios.com/310_Studios/Bionicle_1.html|archivedate=3 February 2015|deadurl=no}}</ref> Lego also partnered with [[Miramax]] to distribute the film, along with developing a future full feature-length theatrical film.<ref>{{cite web | author=Linder, Brian| date=11 July 2002| title=Miramax Plays with LEGO | publisher=[[IGN]]| url=http://uk.ign.com/articles/2002/07/11/miramax-plays-with-lego | accessdate=26 June 2006|archiveurl=https://web.archive.org/web/20050103075439/http://filmforce.ign.com/articles/364/364520p1.html|archivedate=3 January 2005|deadurl=no}}</ref>

Five different people were involved in the creation of the film's story and script: executive producer [[Bob Thompson (producer writer)|Bob Thompson]], original Bionicle co-creators and writers [[Alastair Swinnerton]] and Martin Riber Andersen, and Hollywood writers [[Henry Gilroy]] and [[Greg Weisman]]. The script-writing process began in 2002.<ref name="BZPinterview"/><ref name="GilroyInterview"/> Gilroy became involved after a meeting with Thompson, and enjoyed working on the script as he greatly admired the Bionicle mythos. Two draft scripts were originally created to see who would write the better script: one by Swinnerton, and one from Gilroy. Due to time constraints, the Gilroy script was accepted with some of Swinnerton's ideas included. Gilroy's most difficult task was creating the dialogue for the Toa: he needed to take into account Thompson's own interpretations, and what the majority of fans thought they should sounds like. In the end, he attempted to stay true to original portrayals while giving the voice actors something unique to work with. He also needed to balance their portrayals and screentime, as each of the six needed a chance to shine.<ref name="GilroyInterview">{{cite web | author=Glatzer, Jenna |year=2003| title=Interview with Henry Gilroy | publisher=Absolute Write | url=http://www.absolutewrite.com/screenwriting/henry_gilroy.htm | accessdate=26 June 2006 |archiveurl = https://web.archive.org/web/20060407122343/http://www.absolutewrite.com/screenwriting/henry_gilroy.htm <!-- Bot retrieved archive --> |archivedate = 7 April 2006|deadurl=yes}}</ref> The script went through eight different drafts before the final version, although this was far less than Gilroy was used to seeing on other projects. During planning stages, a lot of time was devoted to how characters interacted with each other: cited examples were Lewa's style of speaking, and Kopaka's stoic behaviour. These traits were accentuated for the film to give the characters more distinction and depth. He also needed to avoid putting in out-of-context pop culture references, which would not fit into the setting of Bionicle.<ref name="BZPinterview"/> As part of the world and character development, expressions and exclamations unique to the world were created: a cited example is Jaller saying "You could have been Lava-Bones" when Takua narrowly escapes being killed by a flood of lava.<ref name="BonusDiscint"/>

===Design===
The characters of Bionicle had been portrayed in various ways across multiple media, including the official comics, [[Adobe Flash|Flash]] animations used for online videos, and CGI commercials for the sets. The team decided to use the models from the CGI commercials as their working base for the film models. Before commencing with designing the characters, the team went to Lego's Denmark headquarters and received lessons in the Bionicle design process. The team were originally not going to alter the characters that much, but they needed them to be emotive, which necessitated a redesign. The first step in creating the character models was redrawing the skeleton, then adding muscle pods that would interweave with the skeleton: the muscle gave the characters a more textured appearance, and were added to the shoulders, calves, abdomen and chest cavity. Each character was given a "heart light", a pulsating light in their chest which would fluctuate even when the character was stationary in-shot. They also adjusted the eyes, making the sockets shallower than on the sets and giving them a pupil-like glow. While redesigning the Toa, the team consulted with the original Lego staff to determine what materials the characters were made of: the muscles were compared to rubber, the bones described as a mesh of titanium alloy and carbon fibre, while the armour was made of Kevlar. In general, the outer shells remained mostly unchanged from the sets, although the visible gears were removed and armour was fitted to their backs.<ref name="BioLegoFeature">{{cite web |year=2003| title=Putting the "bio" in Bionicle | publisher=[[Lego]]|pages=1–2 | url=http://club.lego.com/eng/newsandfeatures/showfeature.asp?contentid=1394 | accessdate=26 June 2006|archiveurl=https://web.archive.org/web/20070608040211/http://club.lego.com/eng/newsandfeatures/showfeature.asp?contentid=1394|archivedate=8 June 2007|deadurl=yes}}</ref> The Toa were also bulked up a little when compared to their sets so their feats would be believable for viewers.<ref name="BZPinterview"/>

Several external features were redesigned for the movie, including the introduction of a movable mouth to allow for a more human character, and a four-pronged mechanical tongue to make them look less like dolls. Other characters used lights behind their masks to form mouth movement. Particular emphasis was placed on the eyebrows and lips of characters.<ref name="BZPinterview"/><ref name="BioMakingOf"/> The main change from the original models was the inclusion of hands, which was a necessity if the character performances were to be made realistic.<ref name="BonusDiscint"/> Custom texture maps were created for each character so they would appear unique.<ref name= "BioMakingOf"/> In addition to reinventing old characters, the team created original character designs with Lego staff. Makuta, who had only been briefly glimpsed in the comics and web animations, was created through a "[[Frankenstein's monster|Frankenstein principle]]", taking parts and pieces from multiple Bionicle sets to create the ultimate Bionicle villain. Other original creations included Takua's pet crab Pewku, and a Gukko Bird designed to transport two Matoran.<ref name="BioMakingOf"/> The Rahkshi are described by Bob Thompson as "like hounds hunting down the [Mask of Light]".<ref name="SciBionicle">{{cite web |year=2003| title=Film Expands Bionicle Universe | publisher=Science Fiction News | url=http://www.scifi.com/sfw/issue309/news.html | accessdate=26 June 2006 |archiveurl = https://web.archive.org/web/20051228132244/http://www.scifi.com/sfw/issue309/news.html <!-- Bot retrieved archive --> |archivedate = 28 December 2009|deadurl=yes}}</ref> They were created by Swinnerton, and comic writer [[Alan Grant (writer)|Alan Grant]] helped develop their characters. They were designed to appear more machine-like than the rest of the cast, and their design were strongly inspired by the bio-mechanical artworks of [[H. R. Giger]].<ref name="BZPinterview"/>

===Audio===
Voice casting was managed by [[Kris Zimmerman]], who chose voice actors that seemed to suit certain roles: the Matoran were voiced by young adults, while the Turaga were voiced by older actors.<ref name="BZPinterview"/> The voice actors and their performances were chosen so they would not sound like they came from a specific area of the world, instead sound like they came from and belonged in the Bionicle universe.<ref name="BonusDiscint">"Making Of Bionicle: Mask of Light". {{cite video |year=2003 | title=Bionicle: Mask of Light | medium=DVD | publisher=Miramax Home Entertainment | location=Santa Monica, California}}</ref> A notable event during recording was a behind-the-scenes incident between Michas and Francis: originally cast in the respective roles of Jaller and Takua, Michas came into a recording session in a muddled state and began reading Takua's lines by mistake. When Francis came in, he read Jaller's lines: when the staff heard them and were favourably impressed, the actors permanently switched roles.<ref name="BZPinterview"/> Voice acting for the project was mostly done in 2002, but dialogue was re-recorded during Additional Dialogue Recording (ADR) into 2003: approximately 30% of the final film's dialogue was done during ADR.<ref name="BZPinterview"/> Audio and music post-production was handled by Hacienda Post. The company's president Tim Borquez served as sound supervisor.<ref name="BioPost">{{cite web|url=http://www.awn.com/news/310-studios-hacienda-post-tackle-bionicle-post|title=310 Studios & Hacienda Post Tackle Bionicle Post|author=Baisley, Sarah|publisher=[[Animation World Network]]|date=22 May 2003|accessdate=29 December 2015|archiveurl=https://web.archive.org/web/20151230212736/http://www.awn.com/news/310-studios-hacienda-post-tackle-bionicle-post|archivedate=30 December 2015|deadurl=no}}</ref> ''Mask of Light'' was the first time in any media that Bionicle characters spoke.<ref name= "BioMakingOf"/>

==Soundtrack==
{{Infobox album
| Name        = Bionicle: Mask of Light<br/>Original Score Soundtrack (14th Anniversary Release)
| Type        = Film score
| Artist      = [[Nathan Furst]]
| Cover       = BionicleMaskofLightOSS.jpg
| Released    = March 10, 2017
| Recorded    =
| Genre       = [[Soundtrack]]
| Length      = 66:27
| Label       = Rising Phoenix Records
| Last album  = ''[[Need for Speed (film)|Need for Speed]]'' <br/> (2014)
| This album  = '''''Bionicle: Mask of Light''''' <br/> (2017)
| Next album  = 
}}

The music was composed by [[Nathan Furst]], whose preferred composition style of grand orchestra and [[electronica]] was what the film's producers were looking for. Initially ignorant of the Bionicle universe, he explored the official website after getting the job as composer to get better context for his music. This and discussions he had with staff helped in the creation of eight or nine specific themes. The overall style is grand orchestral, but includes elements of electronic and tribal music. Furst used elements of African, Polynesian, and Eastern European music to communicate the fact that Mata Nui was an island, and when he could he incorporated his music into the action rather than leaving it as a standalone element within scenes.<ref>{{cite web|url=http://www.bzpower.com/story.php?ID=1413|title=An Interview with Nathan Furst, MoL Composer|publisher=BZPower.com|date=2 October 2003|accessdate=29 December 2015|archiveurl=https://web.archive.org/web/20151027145301/http://www.bzpower.com/story.php?ID=1413|archivedate=27 October 2015|deadurl=no}}</ref>

For fourteen years, barely any of the film's soundtrack was released to the public, with Nathan Furst stating that much of his work for the BIONICLE trilogy was lost in a computer crash. However, in February 2017, it was announced that Furst's score for the film would be officially released, with the majority of the music being previously unreleased. 

;Original Score Soundtrack (14th Anniversary Release)
{{Track listing
| collapsed =
| headline =
| extra_column =
| total_length = 66:27
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits =
| music_credits =
| title1 = Legend of the Bionicle
| length1 = 4:08
| title2 = A Great Kanohi mask
| length2 = 3:01
| title3 = Our New Koli Field
| length3 = 1:57
| title4 = Koli Tournament
| length4 = 2:10
| title5 = Prophecy of a 7th Toa
| length5 = 3:12
| title6 = Gali's Meditation & Birth of the Rahkshi
| length6 = 1:47
| title7 = Trust in the Mask
| length7 = 1:42
| title8 = The Fall of Ta-Koro
| length8 = 7:00
| title9 = Toa Lewa Helps
| length9 = 5:09
| title10 = You Were Following Me
| length10 = 1:46
| title11 = Rahkshi Washed and Chilled
| length11 = 2:51
| title12 = Shadows Are Everywhere
| length12 = 4:13
| title13 = Destruction of Onu-Koro
| length13 = 4:59
| title14 = Healing Tahu
| length14 = 2:41
| title15 = Toa Reunited and Death of Jaller
| length15 = 5:32
| title16 = A New Hero
| length16 = 2:12
| title17 = A Simple Game of Koli
| length17 = 5:45
| title18 = Unity, Duty, Destiny
| length18 = 6:22
}}
{{Clear}}

==Release==
''Mask of Light'' was first announced in April 2002 for release in September the following year. It was Lego Media's (later becoming Create TV & Film in early 2003) first project based on original characters and storylines, and their first direct-to-video production.<ref>{{cite web|url=http://www.lego.com/eng/info/press/morepress.asp?RegionalSiteId=11&id=332|title=The Bionicle legend on direct-to-video|publisher=[[Lego]]|date=24 April 2002|accessdate=31 December 2015|archiveurl=https://web.archive.org/web/20020601153753/http://www.lego.com/eng/info/press/morepress.asp?RegionalSiteId=11&id=332|archivedate=1 June 2002|deadurl=yes}}</ref><ref>{{cite web|url=http://www.animationmagazine.net/tv/new-identity-for-lego-media/|title=New Identity for Lego Media|author=Ball, Ryan|work=[[Animation Magazine]]|date=21 January 2003|accessdate=31 December 2015|archiveurl=https://web.archive.org/web/20151231233341/http://www.animationmagazine.net/tv/new-identity-for-lego-media/|archivedate=31 December 2015|deadurl=no}}</ref> It initially released on 16 September 2003 for [[home video]] and [[DVD]]. It went on to release in 27 different countries over the next eight weeks. It was released under the Miramax Home Entertainment label through [[Walt Disney Studios Home Entertainment|Buena Vista Home Entertainment]].<ref name="ANWbudget"/><ref name="BioPost"/> Prior to its release, the film received a world premiere at [[Legoland]] in [[Carlsbad, California]] on 13 September: the premiere featured a huge mosaic built of [[Lego]] and a special effects show, in addition to special guests and costumed characters.<ref>{{cite web|url=http://www.animationmagazine.net/home-entertainment/bionicle-mask-of-light-to-premiere-at-legoland/|title=Bionicle: Mask of Light to Premiere at Legoland|author=Ball, Ryan|work=[[Animation Magazine]]|date=18 September 2003|accessdate=1 January 2016|archiveurl=https://web.archive.org/web/20140728112818/http://www.animationmagazine.net/home-entertainment/bionicle-mask-of-light-to-premiere-at-legoland/|archivedate=28 July 2014|deadurl=no}}</ref> The following year, the film received its television premiere on [[Cartoon Network]]'s [[Toonami]] program block.<ref name="BionicleCartoon"/>

At release, the film ranked high in multiple sales charts. In the "Top Kid Video" compiled by ''[[Billboard (magazine)|Billboard]]'', it came close to the top of the charts upon release.<ref>{{cite web|url=https://www.theguardian.com/technology/2004/apr/29/shopping.toys|title=Building blocks for the future|author=Widdicombe, Rupert|work=[[The Guardian]]|date=28 April 2004|accessdate=1 January 2016|archiveurl=https://web.archive.org/web/20140913135411/http://www.theguardian.com/technology/2004/apr/29/shopping.toys|archivedate=13 September 2014|deadurl=no}}</ref> Upon release, the film ranked at #2 on [[Amazon.com]]'s VHS best seller list, coming in behind the VHS release of ''[[Lord of the Rings: The Two Towers]]''.<ref>{{cite web|url=http://www.animationmagazine.net/home-entertainment/solid-sales-for-sleeping-beauty-family-guy/|title=Solid Sales for Sleeping Beauty, Family Guy|author=Ball, Ryan|work=[[Animation Magazine]]|date=18 September 2003|accessdate=1 January 2016|archiveurl=https://web.archive.org/web/20160101103150/http://www.animationmagazine.net/home-entertainment/solid-sales-for-sleeping-beauty-family-guy/|archivedate=1 January 2016|deadurl=no}}</ref> According to Lego's 2003 financial report, the game was top-selling VHS release in the United States in its first week, and ranked at #4 in a similar list compiled by ''The Hollywood Reporter''.<ref>{{cite web|url=http://cache.lego.com/downloads/aboutus/uk_report2003_web.pdf|title=Annual Report 2003 - Lego Company|format=PDF|page=6|publisher=[[Lego]]|year=2003|accessdate=30 December 2015|archiveurl=https://web.archive.org/web/20150905171135/http://cache.lego.com/downloads/aboutus/uk_report2003_web.pdf|archivedate=5 September 2015|deadurl=no}}</ref> It was also among the top premiere DVDs of the year.<ref name="BionicleCartoon"/> The game's rental sales were also high, being placed at #8 in the animated direct-to-video charts and totalling $4.24 million revenue by October 2004.<ref name="ANWbudget"/> According to ''[[Animation Magazine]]'', the film is considered to be a commercial success.<ref name="BionicleCartoon">{{cite web|url=http://www.animationmagazine.net/tv/cartoon-network-grabs-bionicle-movie/|title=Cartoon Network Grabs Bionicle Movie|author=Ball, Ryan|work=[[Animation Magazine]]|date=22 March 2004|accessdate=1 January 2016|archiveurl=https://web.archive.org/web/20120819210502/http://www.animationmagazine.net/tv/cartoon-network-grabs-bionicle-movie/|archivedate=19 August 2012|deadurl=no}}</ref>

===Critical reception===
''[[Entertainment Weekly]]'' gave the film a favourable ranking of "B+", calling it a "well-constructed CGI adventure" and saying that those who did not understand the story would enjoy the effects and action sequences.<ref>{{cite web | author=Fretts, Bruce| title= Bionicle: Mask of Light| work=[[Entertainment Weekly]] | date=16 September 2003| url=http://www.ew.com/article/2003/09/19/bionicle-mask-light | accessdate=3 May 2012|archiveurl=https://web.archive.org/web/20110807050126/http://www.ew.com/ew/article/0,,485289,00.html|archivedate=7 August 2011|deadurl=no}}</ref> [[DVDTalk]]'s Don Houston was generally positive about both the film and its additional content: he called the visuals "exceptionally crisp and clear" when compared to other films of its type, and gave high praise to the voice acting and noting darker themes within the film. His main criticism was that it relied heavily on foreknowledge of earlier Bionicle storylines.<ref name="DVDTreview">{{cite web | author=Houston, Don| title=Bionicle- Mask of Light| publisher=[[DVDTalk]] | date=16 September 2003| url=http://www.dvdtalk.com/reviews/7720/bionicle-mask-of-light/ | accessdate=4 March 2006|archiveurl=https://web.archive.org/web/20060924152833/http://www.dvdtalk.com/reviews/read.php?ID=7720|archivedate=24 September 2006|deadurl=no}}</ref> Sci Film.org praised the film's design and visuals, but felt that the film was too short, echoed Houston's criticism of a need for foreknowledge, and said that it was "too politically correct" when compared to other films like ''[[Transformers: The Movie]]''.<ref>{{cite web | title=Bionicle: Mask of Light| publisher=Sci Film.org |date=December 2004| url=http://www.scifilm.org/reviews/bioniclemask.html | accessdate=4 March 2006|archiveurl=https://web.archive.org/web/20041224161405/http://www.scifilm.org/reviews/bioniclemask.html|archivedate=24 December 2004|deadurl=yes}}</ref> Jules Faber of Digital Views Daily said the story had been designed with children in mind, and again praised the visuals despite seeing some stilted animations and poorly-done environmental effects. He was generally positive about the DVD's audio and visual quality.<ref name="DVDreview">{{cite web | author=Faber, Jules| title=Bionicle&nbsp;— Mask of Light: The Movie| work=Digital Views Daily |year=2003| url=http://www.dvd.net.au/review.cgi?review_id=3278 | accessdate=4 March 2006|archiveurl=https://web.archive.org/web/20150312133242/http://www.dvd.net.au/review.cgi?review_id=3278|archivedate=12 March 2015|deadurl=no}}</ref> Both Houston and Faber noted homages to other well-known films, including ''[[The Lord of the Rings (film series)|The Lord of the Rings]]'', ''[[Raiders of the Lost Ark]]'', and the works of [[Ray Harryhausen]].<ref name="DVDTreview"/><ref name="DVDreview"/>

==Legacy==
''Mask of Light'' won a [[Golden Reel Award]] for Best Visual Effects in a DVD Premiere Movie in December 2003.<ref>{{cite web | author=Ball, Ryan| title=Dalmatians II, Two Towers Score DVD Exclusive Awards| work=[[Animation Magazine]] | date=4 December 2003 | url=http://www.animationmagazine.net/home-entertainment/dalmatians-ii-two-towers-score-dvd-exclusive-awards/ | accessdate=4 March 2006|archiveurl=https://web.archive.org/web/20060621074252/http://www.animationmagazine.net/article.php?article_id=1863|archivedate=21 June 2006|deadurl=no}}</ref> In addition, it won the Best DVD release award at the 2004 [[Saturn Awards]].<ref>{{cite web|url=http://www.awn.com/news/return-king-cleans-saturn-awards|title=Return of the King Cleans Up At Saturn Awards|author=DeMott, Rick|publisher=[[Animation World Network]]|date=6 May 2004|accessdate=1 January 2016}}</ref> In 2014, [[Vulture.com]] made mention of the movie in a 2014 retrospective on Lego's history in the movie industry.<ref>{{cite web|url=http://www.vulture.com/2014/02/how-lego-y-were-legos-other-movies.html|title=How Lego-y Were Lego's Other Movies?|author=Rizov, Vadim|publisher=[[Vulture.com]]|date=7 February 2014|accessdate=1 January 2016}}</ref> That same year, ''[[Radio Times]]'' ranked it as among the best movies based on toys, say that the developers "did a better-than-average job of translating the appeal of the toys to screen".<ref>{{cite web|url=http://www.radiotimes.com/news/2014-11-12/the-best-films-based-on-toys|title=The best films based on toys|author=Fullerton, Huw|work=[[Radio Times]]|date=12 November 2014|accessdate=1 January 2016}}</ref>

''Mask of Light'' was promoted by Lego with new toys based on the film's characters, a [[Bionicle: The Game|video game based on its story]] that released in 2003, and a novelisation of the film.<ref>{{cite web | author=Todd, Brett | date=29 July 2003| title=Miramax and Lego Bond on Two More Bionicle Projects | publisher=[[Animation World Network]]| url=http://www.awn.com/news/miramax-and-lego-bond-two-more-bionicle-projects| accessdate=27 June 2006}}</ref> Two further Bionicle movies were confirmed prior to the release of ''Mask of Light'', with the second being another direct-to-video feature and a third for theatrical release.<ref>{{cite web | title=First Bionicle Film On DVD| work=Science Fiction Weekly |year=2003 | url=http://www.scifi.com/sfw/issue335/news.html | accessdate=4 March 2006 |archiveurl = https://web.archive.org/web/20051208125343/http://www.scifi.com/sfw/issue335/news.html <!-- Bot retrieved archive --> |archivedate = 8 December 2005}}</ref> The second movie, ''[[Bionicle 2: Legends of Metru Nui]]'', released in 2004.<ref name="ANWbudget"/> The third movie, ''[[Bionicle 3: Web of Shadows]]'', was released as a direct-to-DVD feature in 2005.<ref name="Bio3Info">{{cite web|url=http://www.awn.com/news/bionicle-3-now-dvd|title=Bionicle 3 Now on DVD|author=DeMott, Rick|publisher=[[Animation World Network]]|date=31 October 2005|accessdate=1 January 2016}}</ref> Both films were produced by the same creative team behind ''Mask of Light''.<ref name="ANWbudget"/><ref name="Bio3Info"/> A fourth film by a different studio, ''[[Bionicle: The Legend Reborn]]'', was released in 2009 through [[Universal Pictures Home Entertainment]].<ref>{{cite web|url=http://deadline.com/2014/09/universal-1440-entertainment-threshold-animation-832687/|title=Universal's Home Vid Production Arm Pacts With Threshold Animation|author=Yamato, Jen|publisher=[[Deadline.com]]|date=10 September 2014|accessdate=1 January 2016}}</ref> A four-episode television series based on the rebooted Bionicle toyline is set to begin airing in the first quarter of 2016 on [[Netflix]].<ref>{{cite web|url=http://variety.com/2015/digital/news/netflix-orders-7-original-kids-series-including-legos-bionicle-and-dreamworks-croods-1201607216/|title=Netflix Orders 7 Original Kids' Series, Including Lego's 'Bionicle' and DreamWorks' 'Croods'|author=Spangler, Todd|work=[[Variety (magazine)|Variety]]|date=1 October 2015|accessdate=1 January 2016}}</ref>

==References==
{{reflist|30em}}

==External links==
{{wikiquote}}
*{{IMDb title|0369281}}
* {{Amg movie|283238}}

{{Bionicle}}

{{DEFAULTSORT:Bionicle 1: Mask Of Light}}
[[Category:2003 films]]
[[Category:American films]]
[[Category:American animated fantasy films]]
[[Category:American animated science fiction films]]
[[Category:Lego films]]
[[Category:Bionicle films]]
[[Category:British animated fantasy films]]
[[Category:British animated science fiction films]]
[[Category:Buena Vista Home Entertainment direct-to-video films]]
[[Category:Chinese animated fantasy films]]
[[Category:Chinese animated science fiction films]]
[[Category:Danish animated fantasy films]]
[[Category:Danish animated science fiction films]]
[[Category:English-language films]]
[[Category:Miramax films]]
[[Category:2000s American animated films]]
[[Category:Computer-animated films]]
[[Category:Miramax animated films]]
[[Category:Toonami]]