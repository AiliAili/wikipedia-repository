{{Infobox person
|name          = Luigi Stipa
|image         = Luigi Stipa.jpg
|image_size    = 200px
|caption       = Luigi Stipa, probably sometime in the 1920s or 1930s
|birth_date    = 1900
|birth_place   = [[Italy]]
|death_date    = 1992
|death_place   = 
|death_cause   = 
|resting_place =
|resting_place_coordinates = 
|residence     = 
|nationality   = 
|ethnicity     = 
|citizenship   = 
|known_for     = 
|employer      = 
|occupation    = [[Aeronautical engineer]], [[hydraulic engineer]], [[civil engineer]]
|years_active  = fl. ca. 1920s–1930s
}}
'''Luigi Stipa''' (1900–1992) was an Italian aeronautical, hydraulic, and civil engineer and aircraft designer who invented the "intubed propeller" for aircraft, a concept that some aviation historians view as the predecessor of the [[turbofan engine]].

==Early life and career==
Stipa was born in [[Ascoli Piceno]], [[Italy]] in 1900. He left school to serve in the [[Italian Army]]'s [[Bersaglieri Corps]] during [[World War I]]. After the war, he earned [[academic degree]]s in [[aeronautical engineering]], [[hydraulic engineering]] and [[civil engineering]].  He went to work for the Italian [[Air Ministry (Italy)|Air Ministry]], where he rose to the position of [[general inspector]] of the Engineering Division of the ''[[Regia Aeronautica]]'' (Italian Royal Air Force).<ref>Guttman, ''Aviation History'', March 2010, p. 18.</ref>

==Intubed propeller==
[[Image:Caproni Stipa from front.jpg|thumb|left|250px|A front view of the [[Stipa-Caproni|Caproni Stipa]] experimental airplane in 1932, showing Stipa's "intubed propeller" design in which the [[propeller (aircraft)|propeller]] and [[Aircraft engine|engine]] are mounted inside a hollow tube which constitutes the airplane's [[fuselage]].]]
In the 1920s, Stipa applied his study of hydraulic engineering to develop a theory of how to make aircraft more efficient as they traveled through the air. Noting that in [[fluid dynamics]]—in accordance with [[Bernoulli's principle]]—a fluid's velocity increases as the diameter of a tube it is passing through decreases, Stipa believed that the same principle could be applied to air flow to make an [[Aircraft engine|aircraft's engine]] more efficient by directing its propeller wash through a [[venturi tube]] in a design he termed an "intubed propeller". In his concept, the [[fuselage]] of a single-engined [[airplane]] designed around an intubed propeller would be constructed as a tube, with the propeller and [[engine nacelle]] inside the tube, and therefore within the fuselage. The propeller would be of the same diameter as the tube, and its slipstream would exit the tube via the opening at the tube's trailing edge at the rear of the fuselage.<ref>Guttman, ''Aviation History'', March 2010, pp. 18–19.</ref>

Stipa spent years studying the idea [[Mathematics|mathematically]], eventually determining that the venturi tube's inner surface needed to be shaped like an [[airfoil]] in order to achieve the greatest efficiency. He also determined the optimum shape of the propeller, the most efficient distance between the leading edge of the tube and the propeller, and the best rate of revolution of the propeller. He appears to have intended the intubed propeller for use in large, multi-engine, [[flying wing]] aircraft—for which he produced several designs—but saw the construction of an experimental single-engine [[prototype]] aircraft as the first step in proving the concept.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

Stipa published his ideas in the Italian aviation journal ''Rivista Aeronautica'' ("Aeronautical Review"), then asked the Air Ministry to build a prototype aircraft to prove his concept. Eager for propaganda opportunities to highlight Italian achievements in technology to the world, and particularly interested in aviation advances, the Italian [[Italian Fascism|Fascist]] government approved of the venture, and contracted with the [[Caproni]] Aviation Corporation to build the prototype in 1932.<ref>Guttman, ''Aviation History'', March 2010, p. 18.</ref>

==The Stipa-Caproni==

[[Image:Caproni Stipa.jpg|250px|thumb|The [[Stipa-Caproni]] experimental airplane in flight on October 7, 1932, piloted by [[Caproni]] company test pilot [[Domenico Antonini]].]]
The prototype, named the [[Stipa-Caproni]],<ref>Guttman refers to it as the "Caproni Stipa" throughout his March 2010 ''Aviation History'' article.</ref> first flew on October 7, 1932. Remarkably ungainly in appearance, the plane nonetheless proved Stipa's concept in that its intubed propeller increased its engine's efficiency, and the airfoil shape of the tube gave it an improved rate of climb compared to conventional aircraft of similar engine power and [[wing loading]]. The Stipa-Caproni also had a very low landing speed and was much quieter than conventional aircraft. Its [[rudder]] and [[Elevator (aircraft)|elevators]] were mounted in the propeller's slipstream in the opening at the trailing edge of the tube in order to improve handling, and this configuration gave the aircraft handling characteristics that it made it very stable in flight.<ref>Guttman, ''Aviation History'', March 2010, p. 18-19.</ref>

The Stipa-Caproni's great drawback was that the intubed propeller design created so much [[aerodynamic drag]] that most of the design's benefits in efficiency were negated by the drag. However, Stipa viewed the Stipa-Caproni as a mere [[testbed]], and probably did not believe that the intubed propeller's aerodynamic drag problem would be significant in the various large, multi-engined flying wing aircraft he had designed.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

After the Caproni company completed initial testing of the Stipa-Caproni, the ''Regia Aeronautica'' took control of it and conducted a brief series of additional tests, but did not develop it further because the Stipa-Caproni offered no performance improvement over aircraft of conventional design.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

==Stipa's later work and influence==

Despite the lack of ''Regia Aeronautica'' interest in developing the intubed propeller concept further, the Italian government publicized the success of Stipa's idea. Stipa patented the intubed propeller in 1938 in [[Germany]], Italy, and the [[United States]], and his work was published in [[France]], Germany, Italy, the [[United Kingdom]], and the United States, where the [[National Advisory Committee for Aeronautics]] studied it. The ''Regia Aeronautica''{{'}}s<!--this is the only legitimate use of this template in this article; and even here it doesn't give any different results than if it were omitted, but might keep some mindless editor from matching up count on each side--> tests also sparked academic interest in the intubed propeller.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

France in the 1930s based its [[ANF- Mureaux BN.4]] advanced night [[bomber]] design on a multi-engine intubed-propeller Stipa design, although the BN.4 was cancelled in 1936 before the first aircraft could be built. In Germany in 1934, [[Ludwig Kort]] designed the [[Kort nozzle]], a [[ducted fan]] similar to Stipa's intubed propeller and still in use, and the German [[Heinkel T]] fighter design bore a similarity to Stipa's concepts.  In Italy, none of Stipa's flying wing designs with intubed propellers ever were built, but the [[Caproni Campini N.1]], an experimental but impractical advanced derivative of the intubed propeller idea powered by a [[motorjet]], appeared in 1940.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

Stipa himself believed that he deserved the credit for inventing the [[jet engine]] via his intubed propeller design, and claimed that the [[pulse jet engine]] the Germans employed on the [[V-1 flying bomb]] of [[World War II]] violated his intubed propeller patent in Germany, although the pulse-jet engine was not in fact closely related to his ideas.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

Stipa died in 1992, embittered over never having received what he viewed as his just recognition for inventing the jet engine. Some aviation historians do at least partially agree with Stipa, noting that the modern turbofan engine has features which show it to be the descendant of his intubed propeller concept.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

==Notes==
{{reflist}}

==References==

*Guttman, Robert. "Caproni Flying Barrel: Luigi Stipa Claimed His 'Intubed Propeller' Was the Ancestor of the Jet Engine." ''Aviation History''. March 2010. ISSN 1076-8858.

{{DEFAULTSORT:Stipa, Luigi}}
[[Category:1900 births]]
[[Category:1992 deaths]]
[[Category:Aircraft designers]]
[[Category:Italian aerospace engineers]]
[[Category:Hydraulic engineers]]
[[Category:Italian civil engineers]]
[[Category:Caproni people]]