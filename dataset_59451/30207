{{featured article}}
{{Infobox Aircraft accident
|name             = 1940 Brocklesby mid-air collision
|image            = Two Avro Ansons (L9162 and N4876) "piggyback" in a paddock near Brocklesby 1.jpg|size=280px|alt=Side view of two military monoplanes lying wheels up on a field, one atop the other
|caption          = The Avro Ansons after landing safely, having collided in mid-air and locked together, 29 September 1940
|occurrence_type  = Accident
|date             = 29 September 1940
|type             = [[Mid-air collision]]
|site             = [[Brocklesby, New South Wales|Brocklesby]], New South Wales, Australia
|coordinates      = {{coord|35|48|S|146|41|E|type:event_source:plwiki|display=inline,title}}
|total_injuries   = 1
|total_fatalities = 0
|total_survivors  = 4

<!--Remaining entries for two/three-aircraft accidents:-->
|plane1_type       = [[Avro Anson]]
|plane1_operator   = [[No. 2 Service Flying Training School RAAF]]
|plane1_tailnum    = N4876
|plane1_origin     = {{nowrap|[[RAAF Base Wagga|RAAF Station Forest Hill]]}}, New South Wales
|plane1_destination = [[Corowa, New South Wales|Corowa]], New South Wales
|plane1_crew       = 2
|plane1_injuries   = 0
|plane1_survivors  = 2
|plane2_type       = Avro Anson
|plane2_operator   = No. 2 Service Flying Training School RAAF
|plane2_tailnum    = L9162
|plane2_origin     = RAAF Station Forest Hill, New South Wales
|plane2_destination = Corowa, New South Wales
|plane2_crew       = 2
|plane2_injuries   = 1
|plane2_survivors  = 2
}}

On 29 September 1940, a [[mid-air collision]] occurred over [[Brocklesby, New South Wales|Brocklesby]], New South Wales, Australia. The accident was unusual in that the aircraft involved, two [[Avro Anson]]s of [[No.&nbsp;2 Service Flying Training School RAAF]], remained locked together after colliding, and then landed safely. The collision stopped the engines of the upper Anson, but those of the machine underneath continued to run, allowing the pair of aircraft to keep flying. Both navigators and the pilot of the lower Anson bailed out. The pilot of the upper Anson found that he was able to control the interlocked aircraft with his [[aileron]]s and [[Flap (aircraft)|flaps]], and made an emergency landing in a nearby [[Field (agriculture)|paddock]]. All four crewmen survived the incident, and the upper Anson was repaired and returned to flight service.

==Training school and flight details==
[[File:AC0001Ansons1941.jpg|thumb|left|260px|Ansons of No.&nbsp;2 SFTS in formation|alt=Five twin-engined military monoplanes in flight, line abreast]]
[[No. 2 Service Flying Training School RAAF|No.&nbsp;2 Service Flying Training School]] (SFTS), based at [[RAAF Base Wagga|RAAF Station Forest Hill]] near [[Wagga Wagga]], New South Wales, was one of several pilot training facilities formed in the early years of World War&nbsp;II as part of Australia's contribution to the [[British Commonwealth Air Training Plan|Empire Air Training Scheme]].<ref name=2SFTS>RAAF Historical Section, ''Units of the Royal Australian Air Force'', pp. 102–103</ref><ref>Gillison, [https://static.awm.gov.au/images/collection/pdf/RCDIG1070724--1-.pdf ''Royal Australian Air Force 1939–1942'', p. 111]</ref> After basic aeronautical instruction at an elementary flying training school, pupils went on to an SFTS to learn techniques they would require as operational (or "service") pilots, including instrument flying, night flying, cross-country navigation, advanced aerobatics, formation flying, dive bombing, and aerial gunnery.<ref name=Stephens>Stephens, ''The Royal Australian Air Force'', pp. 67–70</ref><ref>Gillison, [https://static.awm.gov.au/images/collection/pdf/RCDIG1070724--1-.pdf ''Royal Australian Air Force 1939–1942'', pp. 97, 109]</ref> No.&nbsp;2 SFTS's facilities were still under construction when its first course commenced with [[Avro Anson]]s on 29&nbsp;July 1940.<ref name=2SFTS/><ref>Gillison, [https://static.awm.gov.au/images/collection/pdf/RCDIG1070475--1-.pdf ''Royal Australian Air Force 1939–1942'', p. 56]</ref>

On 29 September 1940, two Ansons took off from the Forest Hill air base for a cross-country training exercise over southern New South Wales.<ref name=Flypast>Parnell; Boughton, ''Flypast'', p. 186</ref> [[aircraft registration|Tail number]] N4876 was piloted by [[Leading Aircraftman]] Leonard Graham Fuller, 22, from [[Cootamundra, New South Wales|Cootamundra]], with Leading Aircraftman Ian Menzies Sinclair, 27, from [[Glen Innes, New South Wales|Glen Innes]], as navigator.<ref name=Flypast/><ref name=Hatching>Ilbery, ''Hatching an Air Force'', p. 16</ref><ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1047933 |title=Fuller, Leonard Graham|work= World War 2 Nominal Roll|publisher=[[Department of Veterans' Affairs (Australia)|Department of Veterans' Affairs]]|accessdate=22 April 2014}}</ref><ref name=WW2Sinclair>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1063481 |title=Sinclair, Ian Menzies|work=World War 2 Nominal Roll|publisher=Department of Veterans' Affairs|accessdate=22 April 2014}}</ref> Tail number L9162 was piloted by Leading Aircraftman Jack Inglis Hewson, 19, from [[Newcastle, New South Wales|Newcastle]], with Leading Aircraftman Hugh Gavin Fraser, 27, from [[Melbourne]], as navigator.<ref name=Flypast/><ref name=Hatching/><ref name=WW2Hewson>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1050819 |title=Hewson, Jack Inglis|work= World War 2 Nominal Roll|publisher=Department of Veterans' Affairs|accessdate=22 April 2014}}</ref><ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1047778|title= Fraser, Hugh Gavin|work=World War 2 Nominal Roll|publisher=Department of Veterans' Affairs|accessdate=22 April 2014}}</ref> Their planned route was expected to take them first to [[Corowa, New South Wales|Corowa]], then to [[Narrandera]], and finally back to Forest Hill.<ref name=Renown>Coleman, ''Above Renown'', pp. 103–104</ref>

==Collision and emergency landing==
{{multiple image
| align     = left
| direction = vertical
| header    = 
| width     = 260

| image1    = Two Avro Ansons (L9162 and N4876) "piggyback" in a paddock near Brocklesby 2.jpg
| alt1      = Rear three-quarter view of two military monoplanes lying wheels down on a field, one atop the other
| caption1  = The interlocked Ansons lying in a paddock

| image2    = WhenAnsonLandsOnAnson1940.jpg
| alt2      = Two military monoplanes lying wheels down on a field, one atop the other
| caption2  = Engines and forward sections of the two aircraft
}}
The Ansons were at an altitude of {{convert|1000|ft|m||abbr=off}} over the township of [[Brocklesby, New South Wales|Brocklesby]], near [[Albury]], when they made a banking turn.<ref name=Flypast/><ref name=Renown/> Fuller lost sight of Hewson's aircraft beneath him and the two Ansons collided amid what Fuller later described as a "grinding crash and a bang as roaring propellors struck each other and bit into the engine cowlings".<ref name=Renown/><ref>{{cite news |url=http://nla.gov.au/nla.news-article78800782 |title=Risks life to save villagers|newspaper=[[Daily News (Perth, Western Australia)|The Daily News]] |location=Perth |date=2 October 1940 |accessdate=22 April 2014 |page=2 |publisher=[[National Library of Australia]]}}</ref> The aircraft remained jammed together, the lower Anson's turret wedged into the other's port wing root, and its fin and rudder balancing the upper Anson's port [[tailplane]].<ref>Wilson, ''Anson, Hudson & Sunderland in Australian Service'', pp. 52–53</ref>

Both of the upper aircraft's engines had been knocked out in the collision but those of the one below continued to turn at full power as the interlocked Ansons began to slowly circle.<ref name=Hatching/><ref name=Renown/> Fuller described the "freak combination" as "lumping along like a brick".<ref>{{cite news |url=http://nla.gov.au/nla.news-article2539446 |title=Pick-a-back planes |newspaper=[[The Canberra Times]] |location=Canberra |date=2 October 1940 |accessdate=22 April 2014 |page=3 |publisher=National Library of Australia}}</ref> He nevertheless found that he was able to control the piggybacking pair of aircraft with his [[aileron]]s and [[Flap (aircraft)|flaps]], and began searching for a place to land.<ref name=Renown/><ref name=Gillison>Gillison, [https://static.awm.gov.au/images/collection/pdf/RCDIG1070477--1-.pdf ''Royal Australian Air Force 1939–1942'', pp. 82–83]</ref> The two navigators, Sinclair and Fraser, bailed out, followed soon after by the lower Anson's pilot, Hewson, whose back had been injured when the spinning blades of the other aircraft sliced through his fuselage.<ref name=2SFTS/><ref name=Renown/>

Fuller travelled five miles (eight kilometres) after the collision, then successfully made an emergency [[pancake landing]] in a large [[Field (agriculture)|paddock]] {{convert|4|mi|km|0|order=flip}} south-west of Brocklesby. The locked aircraft slid {{convert|200|yd|m||abbr=off|order=flip}} across the grass before coming to rest.<ref name=Flypast/><ref name=Renown/> As far as Fuller was concerned, the touchdown was better than any he had made when practising [[circuits and bumps]] at Forest Hill airfield the previous day. His acting commanding officer, Squadron Leader Cooper, declared the choice of improvised runway "perfect", and the landing itself as a "wonderful effort".<ref name=Hatching/> The RAAF's Inspector of Air Accidents, Group Captain [[Arthur William Murphy|Arthur "Spud" Murphy]], flew straight to the scene from Air Force Headquarters in Melbourne, accompanied by his deputy [[Henry Winneke]].<ref>Coleman, ''Above Renown'', pp. 99, 103</ref> Fuller told Murphy:<ref name=Renown/>

{{quote|Well, sir, I did everything we've been told to do in a forced landing—land as close as possible to habitation or a farmhouse and, if possible, land into the wind. I did all that. There's the farmhouse, and I did a couple of circuits and landed into the wind. She was pretty heavy on the controls, though!}}

==Aftermath==
{{multiple image
| align     = right
| direction = vertical
| header    = 
| width     = 260

| image1    = SUK10426BruceFuller1941.jpg
| alt1      = Two men talking, one in dark overcoat with broad-brimmed hat, the other in dark military uniform with forage cap
| caption1  = Sergeant Fuller (right) with Australian High Commissioner [[Stanley Bruce]] in London, 1941

| image2    = BrocklesbyAvroAnsonPropeller.JPG
| alt2      = An aircraft engine and propeller sits under fenced, roofed shelter in a park
| caption2  = The monument to the accident in  Brocklesby, 2009
}}
The freak accident garnered news coverage around the world, and cast a spotlight on the small town of Brocklesby.<ref name=Hatching/><ref name=Renown/> In preventing the destruction of the Ansons, Fuller was credited not only with avoiding possible damage to Brocklesby, but with saving approximately [[Australian pound|£]]40,000 worth of military hardware. Both Ansons were repaired; the top aircraft (N4876) returned to flight service, and the lower (L9162) was used as an instructional airframe.<ref name=Flypast/><ref name=Renown/> Hewson was treated for his back injury at Albury District Hospital and returned to active duty; he graduated from No.&nbsp;2 SFTS in October 1940.<ref name=Hatching/><ref name=Renown/> He was discharged from the Air Force as a [[flight lieutenant]] in 1946.<ref name =WW2Hewson/> Sinclair was discharged in 1945, also a flight lieutenant.<ref name=WW2Sinclair/> Fraser was posted to Britain and flew as a [[pilot officer]] with [[No.&nbsp;206 Squadron RAF]], based in [[RAF Aldergrove|Aldergrove]], Northern Ireland.  He and his crew of three died on 1&nbsp;January 1942 during a routine training flight, when their [[Lockheed Hudson]] collided with a tree.<ref>{{cite web|url=http://www.awm.gov.au/catalogue/research_centre/pdf/rc09125z007_1.pdf |title=RAAF Personnel Serving on Attachment in Royal Air Force Squadrons |page=14 |publisher=[[Australian War Memorial]] |accessdate=18 April 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20140328054907/http://www.awm.gov.au/catalogue/research_centre/pdf/rc09125z007_1.pdf |archivedate=28 March 2014 }}</ref>

Fuller was promoted to [[sergeant]] after his successful landing, but also confined to barracks for fourteen days and docked seven days' pay for speaking about the incident to newspapers without authorisation.<ref name =Renown/><ref>Royal Australian Air Force, ''RAAF Officers Personnel Files'', p. 42</ref> He graduated from No.&nbsp;2 SFTS in October 1940, and subsequently received a commendation from the Australian Air Board for his "presence of mind, courage and determination in landing the locked Ansons without serious damage to the aircraft under difficult conditions".<ref name=Hatching/><ref>Royal Australian Air Force, ''RAAF Officers Personnel Files'', p. 32</ref> Fuller saw active service first in the Middle East, and then in Europe with [[No.&nbsp;37 Squadron RAF]].  He earned the [[Distinguished Flying Medal]] for his actions over [[Palermo]] in March 1942. [[Officer (armed forces)|Commissioned]] later that year, Fuller was posted back to Australia as a [[flying officer]], and became an instructor at [[No. 1 Operational Training Unit RAAF|No.&nbsp;1 Operational Training Unit]] in [[Sale, Victoria|Sale]], Victoria.<ref name=Hatching/><ref name=Gillison/> He died near Sale on 18&nbsp;March 1944, when he was hit by a bus while riding his bicycle.<!--<ref name=Renown/>--><ref name=Gillison/><ref>{{cite news |url=http://trove.nla.gov.au/ndp/del/article/11811366 |title=Pick-a-back pilot's death in road accident |newspaper=[[The Argus (Melbourne)|The Argus]] |date=21 March 1944 |accessdate=3 February 2016 |page=3}}</ref>

==Legacy==
According to the [[Greater Hume Shire|Greater Hume Shire Council]], the 1940 mid-air collision remains Brocklesby's "main claim to fame".<ref>{{cite web|url=http://www.greaterhume.nsw.gov.au/LinkClick.aspx?fileticket=HLJ1ryGx7dk%3D&tabid=105|title=What's on in May|publisher=[[Greater Hume Shire]]|accessdate=22 April 2014}}</ref><ref>{{cite web|url=http://www.greaterhume.nsw.gov.au/VisitOurShire/ShireVillages.aspx|title=Shire Villages|publisher=Greater Hume Shire|accessdate=22 April 2014}}</ref> Local residents commemorated the 50th anniversary of the event by erecting a marker near the site of the crash landing; it was unveiled by [[Tim Fischer]], the [[Division of Farrer|Federal Member for Farrer]] and [[National Party of Australia|Leader of the National Party]], on 29&nbsp;September 1990.<ref>{{cite news| last= Jones | first=Howard| title=Town remembers piggyback planes| date= 3 January 2007| url=http://www.bordermail.com.au/story/30390/town-remembers-piggyback-planes/| work = [[The Border Mail]]|publisher=[[Fairfax Media]]| accessdate=22 April 2014}}</ref><ref>Royal Australian Air Force, ''RAAF Officers Personnel Files'', pp. 4–5</ref> On 26&nbsp;January 2007, a memorial featuring an Avro Anson engine was opened during Brocklesby's [[Australia Day]] celebrations.<ref>{{cite news| last= Mulcahy | first=Mark| title=Gala double for couple| date= 27 January 2007| url=http://www.bordermail.com.au/news/local/news/general/gala-double-for-couple/461771.aspx| work = The Border Mail|publisher=Fairfax Media| accessdate=22 April 2014}}</ref>

==Notes==
{{Reflist|30em}}

==References==
* {{Cite book|last=Coleman | first=Robert |year=1988| title=Above Renown: The Biography of Sir Henry Winneke| location=South Melbourne, Victoria, and Crows Nest, New South Wales| publisher=[[Macmillan Publishers|Macmillan]] in association with [[The Herald and Weekly Times]]| isbn=0-333-47809-6}}
* {{Cite book|last=Gillison | first=Douglas |year=1962| title=Australia in the War of 1939–1945: Series Three (Air) Volume I&nbsp;– Royal Australian Air Force 1939–1942 | location=Canberra| publisher=[[Australian War Memorial]]| url=http://www.awm.gov.au/histories/second_world_war/AWMOHWW2/Air/Vol1/|isbn=|oclc=2000369}}
* {{Cite book|last=Ilbery|first=Peter|title=Hatching an Air Force: 2 SFTS, 5 SFTS, 1 BFTS Uranquinty and Wagga-Wagga|publisher= Banner Books|location=Maryborough, Queensland|year=2002|isbn=1-875593-24-1}}
* {{Cite book|last=Parnell|first=Neville|author2=Boughton, Trevor | title=Flypast: A Record of Aviation in Australia|publisher= [[Australian Government Publishing Service]]|location=Canberra|year=1988|isbn=0-644-07918-5}}
* {{Cite book|author=RAAF Historical Section|year=1995|title=Units of the Royal Australian Air Force: A Concise History. Volume 8: Training Units|location=Canberra| publisher=Australian Government Publishing Service|isbn=0-644-42800-7}}
* {{Cite book|author=Royal Australian Air Force|title=RAAF Officers Personnel Files: Fuller, Leonard Graham |url=http://recordsearch.naa.gov.au/scripts/Imagine.asp?B=5371669|date=1939–1998|publisher=[[National Archives of Australia]]|location=Canberra}}
* {{Cite book|last=Stephens| first=Alan|origyear=2001|year=2006| title=The Royal Australian Air Force: A History|location=London| publisher=[[Oxford University Press]]|isbn=0-19-555541-4}}
* {{Cite book|last=Wilson|first=Stewart|title=Anson, Hudson & Sunderland in Australian Service|publisher=Aerospace|location=Weston Creek, Australian Capital Territory|year=1992|isbn=1-875671-02-1}}

==External links==
*[http://www.ozatwar.com/ozcrashes/nsw144.htm More photos of crash site] at [http://www.ozatwar.com/ Australia@War]
*[https://www.youtube.com/watch?v=lp478Tgm5gg Newsreel footage] at [[YouTube]]
*[http://www.flightglobal.com/pdfarchive/view/1964/1964%20-%200280.html Similar Anson accidents] at [[Flightglobal]]

{{Use dmy dates|date=January 2013}}
{{Aviation accidents and incidents in 1940}}

{{DEFAULTSORT:Brocklesby mid-air collision}}
[[Category:1940 in Australia]]
[[Category:Accidents and incidents involving military aircraft]]
[[Category:Accidents and incidents involving the Avro Anson]]
[[Category:Aviation accidents and incidents in 1940]]
[[Category:Mid-air collisions]]
[[Category:Riverina]]
[[Category:Royal Australian Air Force]]