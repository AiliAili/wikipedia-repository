{{Unreferenced|date=March 2010}}
{{accounting}}
In [[accounting]], '''liquidity''' (or '''accounting liquidity''') is a measure of the ability of a [[debtor]] to pay their debts as and when they fall due. It is usually expressed as a [[ratio]] or a [[percentage]] of current [[liability (accounting)|liabilities]]. Liquidity is the ability to pay short-term obligations.

==Calculating liquidity==
For a corporation with a published [[balance sheet]] there are various [[Acid Test (Liquidity Ratio)|ratios used to calculate a measure of liquidity]]. These include the following:

* The [[current ratio]] is the simplest measure and calculated by dividing the total current assets by the total current liabilities. A value of over 100% is normal in a non-banking corporation. However, some current assets are more difficult to sell at full value in a hurry.
* The [[quick ratio]] is calculated by deducting inventories and prepayments from current assets and then dividing by current liabilities, giving a measure of the ability to meet current liabilities from assets that can be readily sold. A better way for a trading corporation to meet liabilities is from cash flows, rather than through asset sales, so;
* The [[operating cash flow ratio]] can be calculated by dividing the [[operating cash flow]] by [[current liabilities]]. This indicates the ability to service current debt from current income, rather than through asset sales.

==Understanding the ratios==
For different industries and differing legal systems the use of differing ratios and results would be appropriate. For instance, in a country with a legal system that gives a slow or uncertain result a higher level of liquidity would be appropriate to cover the uncertainty related to the valuation of assets. A [[manufacturer]] with stable cash flows may find a lower quick ratio more appropriate than an Internet-based start-up corporation.

==Liquidity in banking==
{{main|Market liquidity#Banking}}
Liquidity is a prime concern in a [[banking]] environment and a shortage of liquidity has often been a trigger for bank failures. Holding [[asset]]s in a highly liquid form tends to reduce the [[income]] from that asset (cash, for example, is the most liquid asset of all but pays no interest) so banks will try to reduce liquid assets as far as possible. However, a bank without sufficient liquidity to meet the demands of their [[Deposit account|depositors]] risks experiencing a [[bank run]]. The result is that most banks now try to [[Forecasting|forecast]] their liquidity requirements and maintain emergency standby [[Credit (finance)|credit]] lines at other banks. [[Bank regulation|Banking regulators]] also view liquidity as a major concern.

==See also==
*[[Financial ratio]]
*[[Going concern]]
*[[Liquidity forecast]]
*[[Solvency]]

==References==
{{Library resources box 
|by=no 
|onlinebooks=no 
|others=no 
|about=yes 
|label=Accounting liquidity }}
{{Reflist}}

[[Category:Financial ratios]]
[[Category:Working capital management]]