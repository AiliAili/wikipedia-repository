<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=F.K.6
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Escort fighter
 | national origin=[[United Kingdom]]
 | manufacturer=[[Armstrong Whitworth Aircraft|Armstrong Whitworth]] 
 | designer=[[Frederick Koolhoven]]
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Armstrong Whitworth F.K.5''' and '''F.K.6''' were experimental [[triplane]]s built as escort fighters by Armstrong Whitworth during the [[First World War]].  They carried two gunners in nacelles mounted on the centre wing.  One example of each type was built, with no further development or production following.

==Development and design==
In early 1916, the British [[War Office]] drew up a specification for a multi-seat escort fighter to be powered by one of the new [[Rolls-Royce Eagle]] engines, with an endurance of at least seven hours, intended to protect formations of bombers from German fighters such as the [[Fokker E.I]], with an additional role of destroying enemy [[airship]]s. Orders were placed for prototypes from Armstrong Whitworth, [[Sopwith Aviation Company|Sopwith]] and [[Vickers Limited|Vickers]], all of which were of unconventional design owing to the need to give their gunners a good field of fire in the absence of an effective [[Synchronization gear|synchronisation gear]] to allow guns to be fired safely though the propeller disc.<ref name="Bruce British p25">Bruce 1957, p.25.</ref>

===F.K.5===
Armstrong Whitworth's chief designer, [[Frederick Koolhoven]]'s first design to meet this requirement, probably designated F.K.5,{{#tag:ref|No definitive list exists of the true sequence of the F.K. designations as applied by Koolhaven to his designs.<ref name="Tapper p8">Tapper 1988, p.8.</ref> While the escort triplanes were probably designated F.K.5 and F.K.6, and were earlier in design than the [[Armstrong Whitworth F.K.8|F.K.8]] and [[Armstrong Whitworth F.K.10|F.K.9 and 10]] designs,<ref name="Tapper p72-73">Tapper 1988, pp. 72-73.</ref><ref>[http://www.collectionstrust.org.uk/aircraft/1113.htm Royal Air Force Museum Aircraft Thesaurus - Armstrong Whitworth] {{webarchive |url=https://web.archive.org/web/20110726190751/http://www.collectionstrust.org.uk/aircraft/1113.htm |date=July 26, 2011 }}. ''Royal Air Force Museum'', 2005. Retrieved 19 July 2009.</ref><ref name="complete fighters p25">Green and Swanborough 1994, p.25.</ref> many other sources refer to the types as the F.K.12.<ref name="Bruce British p25"/><ref name="Bruce v1 p14">Bruce 1965, p.14.</ref><ref name="Mason fighter p81">Mason 1992, p.81.</ref>}} was a large, single-engined [[Tractor configuration|tractor]] [[triplane]] with the middle wing having a much greater span than the upper and lower wings. The gunners were housed in two long nacelles mounted on top of the middle wing, allowing them to be seated ahead of the propeller disc, with the pilot's cockpit situated behind the wings in the slim central fuselage, giving a poor view. The undercarriage consisted of a sprung strut carrying two mainwheels underneath the engine, with two stabilsing wheels at the wingtips of the lower wing, with a tailskid just aft of the trailing edge of the lower wing.<ref name="Tapper p70-1">Tapper 1988, pp. 70-71.</ref><ref name="Bruce v1 p14,6">Bruce 1965, pp.14, 16.</ref> This design never flew, with the head of Armstrong Whitworth's Aircraft department, I. Fairbairn-Crawford, forbidding test flights.<ref name="Bruce v1 p16">Bruce 1965, p.16.</ref>

===F.K.6===
Koolhoven completely reworked the design to produce the F.K.6. While still a triplane with the middle wing of significantly greater span than the upper and lower wings, it was larger, with two-bay wings.  This time, the gunner's nacelles were slung under the middle wing and were shorter, so that the gunners sat behind and outboard the propeller (and less than 2&nbsp;ft (0.6&nbsp;m) from the exhaust manifold).<ref name="Mason fighter p81"/> The fuselage was much deeper than the F.K.5, filling the gap between the middle and lower wings, giving a slightly better view, while the undercarriage had two pairs of wheels with a narrow track under the fuselage and a more conventional tailskid.<ref name="Tapper p71">Tapper 1988, p.71.</ref>

Four examples of the F.K.6 were ordered in April 1916,<ref name="Tapper p71"/> two of which were intended for the [[Royal Naval Air Service]],<ref name="complete fighters p25"/> but only one was built, this demonstrating poor performance when tested. As effective synchronising gears were now available, the type was abandoned, with none of the escort fighters being brought into production.<ref name="Bruce British p26">Bruce 1957, p.26.</ref><ref name="Lewis fighter p99">Lewis 1979, p.99.</ref>

==Specifications (F.K.6) ==

{{aerospecs
|ref=Armstrong Whitworth Aircraft since 1914 <ref name="Bruce v1 p17">Bruce 1965, p.17</ref> 
|met or eng?=eng

|crew=three
|capacity=
|length m=11.30
|length ft=37
|length in=0¾
|span m=18.90
|span ft=62
|span in=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=5.18
|height ft=17
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1 
|eng1 type=[[Rolls-Royce Eagle]] water-cooled [[V12 engine|V-12]] engine
|eng1 kw=187<!-- prop engines -->
|eng1 hp=250<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|perfhide=Y<!-- please remove this line if performance data becomes available -->

|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=2× [[.303 British|.303 in]] [[Lewis gun]]s, one in each nacelle
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=*[[Sopwith L.R.T.Tr.]]
*[[Vickers F.B.11]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957. 
*Bruce, J.M. ''War Planes of the First World War: Volume One Fighters''. London:Macdonald, 1965.
*Green, William and Swanborough, Gordon. ''The Complete Book of Fighters''. New York:Smithmark, 1994. ISBN 0-8317-3939-8.
*Lewis, Peter. ''The British Fighter since 1912''. London:Putnam, Fourth edition, 1979. ISBN 0-370-10049-2.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland, USA:Naval Institute Press, 1992. ISBN 1-55750-082-7
*Tapper, Oliver. ''Armstrong Whitworth Aircraft since 1914''. London:Putnam, 1988. ISBN 0-85177-826-7.
{{refend}}

==External links==
{{commons category|Armstrong Whitworth aircraft}}
*[http://www.koolhoven.com/history/airplanes/aw/ Koolhoven Aeroplanes Foundation]

{{Armstrong Whitworth aircraft}}
{{Koolhoven aircraft}}
{{wwi-air}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Armstrong Whitworth aircraft|F.K.01]]
[[Category:Koolhoven aircraft]]