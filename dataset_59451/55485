{{Infobox journal
| title = American Journal of Psychiatry
| cover = [[File:American-J-Psychiatry-2014-9-cover.png]]
| editor = Robert Freedman
| discipline = [[Psychiatry]]
| abbreviation = Am. J. Psychiatry
| formernames = American Journal of Insanity
| publisher = [[American Psychiatric Association]]
| country = United States
| frequency = Monthly
| history = 1844–present
| openaccess = 
| license = 
| impact = 12.295
| impact-year = 2014
| website = http://ajp.psychiatryonline.org/
| link1 = http://ajp.psychiatryonline.org/toc/ajp/current
| link1-name = Current Issue
| link2 =  http://ajp.psychiatryonline.org/loi/ajp
| link2-name = Online Archive of All Issues
| JSTOR = 
| OCLC = 1480183
| LCCN = 22024537
| CODEN = AJPSAO
| ISSN = 0002-953X
| eISSN = 1535-7228
}}
The '''''American Journal of Psychiatry''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] covering all aspects of [[psychiatry]] and the official journal of the [[American Psychiatric Association]].<ref name="AJP">  
{{cite web 
| url = http://ajp.psychiatryonline.org/about 
| title = About ''The American Journal of Psychiatry'' 
| accessdate = 2016-05-07
| year = 2016
| publisher = American Psychiatric Association}}
</ref> The first volume was issued in 1844, at which time it was known as the ''American Journal of Insanity''. The title changed to the current form with the July issue of 1921. 

The Journal regularly publishes reports of pharmaceutical industry-sponsored clinical trial results of psychiatric drugs.

According to the ''[[Journal Citation Reports]]'', the journal has a 2016 [[impact factor]] of 13.505.<ref name="AJP"/>

==Ethical concerns==
Several complaints, including legal cases, have charged the ''American Journal of Psychiatry'' with being complicit in pharmaceutical industry corruption of clinical trial results.<ref>The citalopram CIT-MD-18 pediatric depression trial: Deconstruction of medical ghostwriting, data mischaracterisation and academic malfeasance'  by Jureidini, Jon, Amsterdam, Jay, McHenry, Leemon, ''International Journal of Risk & Safety in Medicine''. 2016 28[1]:33-43.</ref>  In a Department of Justice case against Forest Pharmaceuticals, Forest pled guilty to the charges of misbranding the drug Celexa (citalopram).<ref>United States v Forest Pharmaceuticals, Plea Agreement, September 15, 2010</ref> The Complaint in Intervention clearly identifies a 2004 ghostwritten article published in the ''American Journal of Psychiatry'' in the names of Wagner ''et al''<ref>Wagner KD, Robb AS, Findling RL, Jin J, Gutierrez MM, Heydorn WE. A randomized, placebo-controlled trial of citalopram for the treatment of major depression in children and adolescents. Am J Psych 2004; 161 (6): 1079-1083.</ref> as a part of this illegal marketing of Celexa for pediatric depression.<ref>United States v Forest Pharmaceuticals, Complaint in Intervention p. 17. section 60.</ref>

==See also==
* [[List of psychiatry journals]]

==References==
{{Reflist}}

==External links==
* {{Official website|http://ajp.psychiatryonline.org/}}

[[Category:Psychiatry journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1844]]
[[Category:English-language journals]]
[[Category:American Psychiatric Association]]


{{psychiatry-journal-stub}}