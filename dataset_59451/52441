{{Infobox journal
| cover = [[File:OS Cover 120x159.png]]
| title = Ocean Science
| discipline = [[Oceanography]]
| publisher = [[Copernicus Publications]] on behalf of the [[European Geosciences Union]]
| editor = William Jenkins
| abbreviation = Ocean Sci.
| frequency = Bimonthly
| history = 2005-present
| impact = 2.232
| impact-year = 2014
| openaccess = Yes
| license = [[Creative Commons License|Creative Commons-BY]]
| website = http://www.ocean-science.net/
| link1 = http://www.ocean-sci.net/volumes_and_issues.html
| link1-name = Online access
| ISSN = 1812-0784
| eISSN = 1812-0792
| OCLC = 58479598
}}
'''''Ocean Science''''' is an [[Open access (publishing)|open-access]] [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Copernicus Publications]] on behalf of the [[European Geosciences Union]]. It covers all aspects of [[oceanography]]. The journal is available [[Online and offline|on-line]] and in print. Papers are published under the [[Creative Commons License|Creative Commons Attribution 3.0]] license.<ref name="CC_BY">
{{cite web |url=http://www.ocean-science.net/general_information/license_and_copyright.html |title=License and Copyright |work=Ocean Science |accessdate=2013-03-30}}
</ref>

The [[editor-in-chief]] is William Jenkins ([[Woods Hole Oceanographic Institution]]).<ref>{{cite web |url=http://www.ocean-science.net/general_information/editorial_board.html |title=Editorial Board |work=Ocean Science |accessdate=2013-03-30}}</ref> The founding editors were David Webb ([[National Oceanography Centre]]) and John Johnson ([[University of East Anglia]]).<ref name=OS1>
{{Cite journal

  | journal = Ocean Science
  | volume = 1
  | number = 2
  | year = 2005
}}</ref>

== Peer-review process ==
The journal makes use of an open reviewing process<ref name="review">
{{cite web |url=http://www.ocean-science.net/review/review_process_and_interactive_public_discussion.html |title=Review Process & Interactive Public Discussion |work=Ocean Science |accessdate=2013-03-30}}
</ref>
developed for ''[[Atmospheric Chemistry and Physics]]'' by its editor, [[Ulrich Pöschl]], and colleagues.<ref name="Pöschl">
{{Cite journal
| last = Pöschl
| first = Ulrich
| title = Interactive Journal Concept for Improved Scientific Publishing and Quality Assurance
| journal = Learned Publishing
| volume = 17
| issue = 2
| year = 2004
| pages = 105–113
| url = http://www.ingentaconnect.com/content/alpsp/lp/2004/00000017/00000002/art00005
| doi=10.1087/095315104322958481
}}
</ref>
Submissions considered suitable for review are first published as unreviewed [[grey literature]] in the sister discussion journal ''Ocean Science Discussions''.<ref name="OSD">
{{cite web |url=http://www.ocean-sci-discuss.net/papers_in_open_discussion.html |title= Papers in Open Discussion |work=Ocean Science Discussions |accessdate=2013-03-30}}
</ref>
They are then subject to interactive public discussion, during which the referees' comments (anonymous or attributed), additional short comments by other members of the scientific community (attributed), and the authors' replies are also published. Authors then have a chance to revise their papers in response to the issues raised and the review process is completed in the normal manner.

After final acceptance, papers are published on-line as soon as authors have approved the typeset version. The printed version of the journal, which is published bimonthly, includes all papers published on-line since the last printed issue.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Science Citation Index Expanded]] , [[Scopus]], [[Chemical Abstracts]], and [[GeoRef]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.962.<ref name=WoS>{{cite book |year=2014 |chapter=Ocean Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
* [[List of scientific journals in earth and atmospheric sciences]]

==References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.ocean-science.net/}}

[[Category:Bimonthly journals]]
[[Category:Oceanography journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2005]]
[[Category:European Geosciences Union academic journals]]
[[Category:Copernicus Publications academic journals]]