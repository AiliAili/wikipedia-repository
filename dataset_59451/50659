{{Infobox non-profit
| name              = Australian Academy of Forensic Sciences
| image             = 
| type              = learned society, forensic sciences; incorporated association (New South Wales)
| founded_date      = {{Start date|1967|04|20|df=yes}}
| tax_id            = 
| registration_id   =
| founder           = Dr. Oscar Rivers Schmalzbach OBE
| location          = 
| coordinates       = <!-- {{Coord|LAT|LON|display=inline,title}} -->
| origins           = 
| key_people        = 
| area_served       = Australia
| product           = 
| mission           = 
| focus             = 
| method            = 
| revenue           = 
| endowment         = 
| num_volunteers    = 
| num_employees     = 
| num_members       = 
| subsid            = 
| opponents         = 
| owner             = 
| non-profit_slogan = 
| former name       =
| homepage          = [http://www.forensicacademy.org/ www.forensicacademy.org]
| dissolved         = 
| footnotes         = 
}}

{{Infobox journal
| title         = Australian Journal of Forensic Sciences
| cover         = 
| editor        = James Robertson
| discipline    = forensic sciences
| former_names  = 
| abbreviation  = 
| publisher     = Taylor & Francis
| country       = Australia
| frequency     = 4 issues per year
| history       = September 1968{{spaced ndash}}
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.tandf.co.uk/journals/titles/00450618.asp
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0045-0618
| eISSN         = 1834-562X
| boxwidth      = 
}}

The '''Australian Academy of Forensic Sciences''' is a multi-disciplinary learned society founded in 1967 modelled on the British Academy of Forensic Sciences. The Academy conducts regular conferences, undertakes liaison with other Australian professional bodies including medico-legal societies established in Australia, and makes submissions on forensic sciences issues to governments and governmental bodies.<ref name=Kirby>Michael Kirby, "Forensic Science - What Have We Learnt". (1987) 20 (1) ''Journal of the Academy of Forensic Science'' p 183 {{doi|10.1080/00450618709410751}} accessed 26 August 2011.</ref>

Since September 1968, the Academy has published the Australian Journal of Forensic Sciences.<ref name=Kirby/><ref name=Sainsbury/>

The Academy awards the Oscar Rivers Schmalzbach Foundation Research Grants, named in honour of the inaugural Secretary-General of the Academy.<ref name=Kirby/>

==History==
After foundation, the first meeting as an Academy on 3 August 1967 adopted a constitution.<ref name=Kirby/> The first president of the Academy was the Honourable Justice [[Russell Le Gay Brereton]], a judge of the Supreme Court of New South Wales. Later office-bearers included Sir [[Harry Gibbs]] (President, 1981), Sir [[Bernard Sugerman]], [[Gordon Samuels]] (President, 1974–1976), [[Michael Kirby (judge)|Michael Kirby]] (President 1987–1989), Sir [[Douglas Miller (medical practitioner)|Douglas Miller]] and Sir [[Kenneth Noad]] (leading medical practitioners), Professor [[Malcolm Chaiken]], [[David Bennett (barrister)|David Bennett]] (President, 2000–2001),<ref>"Solicitor-General heads awards list", The Canberra Times, 1 July 2008, p 36</ref> and Professor [[Peter Beumont]].<ref name=Kirby/><ref name=Sainsbury>Maurice J. Sainsbury, "Recollections on the formation of the Australian Academy of Forensic Sciences on the 40th anniversary of the journal", (2008) 40 (2) Australian Journal of Forensic Sciences p 97</ref><ref>Stephen Touyz and Suzy Baldwin, "Psychiatrist Who Changed Attitudes Toward Anorexia" (obituary), The Sydney Morning Herald, 3 January 2004, p 36.</ref>

==Oscar Schmalzbach==
Oscar Rivers Schmalzbach was born in Poland in April 1912 and died in Sydney in January 1997. He began his tertiary education at the University of Poland but was forced into hiding when Germany invaded and escaped to Britain, serving in the British Army during the war. After, he became a Research Fellow in Physiology at Middlesex Hospital, completing post-graduate work at Maudsley Hospital and the National Institute of Neurology in 1947. In 1949, he migrated to Australia, arriving in Sydney where he was a medical officer at Callan Park Hospital. In 1963, he became Consultant Psychiatrist with the New South Wales Attorney General's Department and was called upon to give evidence and his opinion in major criminal trials.<ref>Michael Kirby, [http://www.michaelkirby.com.au/images/stories/speeches/1990s/vol40/1997/1442-Obituary_-_Oscar_Schmalzbach_(Medical_Journal).pdf "Obituary. Dr Oscar Schmalzbach" for Medical Journal of Australia] accessed 26 August 2011.</ref> On 31 December 1979 he was made and Officer in the Order of the British Empire (Civil division).<ref>Australian Government, [http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm It's an honour] (database online) accessed 26 August 2011.</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.forensicacademy.org/ Australian Academy of Forensic Sciences] homepage

<!--- Categories --->

[[Category:Articles created via the Article Wizard]]
[[Category:Learned societies of Australia]]