{{Orphan|date=July 2013}}

'''IAMISEE''' (aka Christian Stevens, aka I.A.I.S) is an American born Rapper and Producer. In 2007, he provided lyrics and performance for ''Live from Planet B-Boy'' for the film [[Planet B-Boy]].<ref>{{cite web|title=Planet B-boy Soundtrack|url=http://www.imdb.com/title/tt0770796/soundtrack|publisher=iMBD}}</ref>  In 2008, IAMISEE produced ''Laisse-Nous Croire'' on the [[Kery James]] album ''[[À l'ombre du show business]]''. The album reached number 3 on French Music charts in its first week.<ref>{{cite web|title=Kery James|url=http://acharts.us/performer/kery_james|publisher=AC Charts|accessdate=14 April 2013}}</ref>   The record is certified platinum in France and was the biggest selling rap album of 2008 in France.<ref>{{cite web|title=Kery James Full Biography and Discography|url=http://www.rfimusic.com/artist/rap/kery-james/biography|publisher=Radio France International|accessdate=14 April 2013}}</ref>  In 2010, [[Blacksmith Records]] selected ''Threes'', written, produced and performed by IAMISEE to be included on ''Talib Kweli & Year of The Blacksmith Present: The Community Mixtape''.

== Career ==

=== Early career and Ruffhouse Records ===

His career began in 1992, at the age of fourteen, during an era known as [[Golden age hip hop]].  The artist was originally known as ''Divine'' in the duo ''Grace & Divine'' with Michael Lowe.  In 1994 the group had a production and development deal with [[Andy Kravitz]] of [[Ruffhouse Records]].<ref name="Jones2008">{{cite news|last=Jones|first=Gentle|title=IAMISEE Speaks|newspaper=The News Journal|date=11 January 2008}}</ref>  The production deal did not go anywhere, but IAMISEE was introduced to and worked with such Ruffhouse Artists as [[Scott Storch]], while Storch was working with [[The Roots]] on [[Do You Want More?!!!??!]], and [[Chuck Treece]]. He loosely collaborated with artists from the label, but work was released only independently and possible commercial releases may not have been credited (see [[#section name|Controversy]]).  By the late nineties ''Divine & Grace'' split up, Ruffhouse was defunct and most of the artists IAMISEE was affiliated with moved to [[Sigma Sound Studios]].  In 1998 the artist changed his name to IAMISEE (see [[#section name|Meaning of the Name]]).  After going solo he had minor deals with small independent labels. One EP and one LP were released and another album was permanently shelved before the artist began an independent solo career in 2006.<ref name="Jones2008" />   Despite his past affiliations with artists who went on to achieve commercial success, IAMISEE's career took a different route.  He did maintain working relationships with established artists, including Chuck Treece<ref>{{cite web|title=ONWA Lickshot Video|url=https://www.youtube.com/watch?v=q1KgwqwcA6g|publisher=YouTube}}</ref>  and Grammy award winning writer Kameron Houff (see [[#section name|Discography]]), but to date has never signed a recording contract with a Major Recording Label and has no known affiliation to an Agent or Manager. He did become self-taught in various aspects of record production, such as; mixing, graphic design, website development and video editing.<ref>{{cite web|title=IAMISEE: Nature of the Triple Threat|url=http://www.urb.com/2010/11/29/iamisee-threes/|publisher=URB Magazine|accessdate=14 April 2013}}</ref>  IAMSEE's most notable work began after becoming an independent artist.

=== Independent career ===

IAMISEE's work with [[Planet B-Boy]] received international recognition through the production company's web postings, the song reportedly had over 4,000,000 YouTube views before the posting account was shut down.<ref>{{cite web|title=Planet BBoy Theme Song|url=http://www.ourstage.com/media_items/PHODKMIVQPLJ-planet-b-boy-theme-song|publisher=Our Stage|accessdate=14 April 2013}}</ref> In 2012, the song IAMISEE produced for James was selected (by Kery James) with thirteen others from among all of James' catalog for a rerelease on the album ''92.2012''.  It was profiled in a book of the same name.  The album ''90.2012'' peaked at number 12 on the French Music Charts.<ref>{{cite web|title=Kery James|url=http://acharts.us/album/70068|publisher=AC Charts}}</ref>  Both works were created and negotiated by the artist without any label backing or management.  In addition to the success of those works, his 2006 release ''IAMISEE vs DJ Demon'' was listed on the [[CMJ]] Hip Hop Airplay Charts in March 2007.  The song ''Exodus'' was in rotation on the Rodeny P & Skitz Show on [[BBC Radio 1Xtra]] in January 2007.  [[Beyond Race Magazine]] said, "the last four tracks may be the best 17 consecutive minutes of Independent hip hop this year".<ref>{{cite journal|last=Jalonski|first=Emmanuel|title=IAMISEE vs Dj Demon Review|year=2007|series=Summer 07}}</ref>   The single ''Hello'' was featured on the [[DC Shoes]] ''Kings of LA'' skate competition DVD that same year. The album ''IAMISEE vs DJ Demon'' was the last that was written, produced and performed by IAMISEE (scratching provided by DJ Demon).  It was followed by instrumental and collaborative albums that were released as free downloads or through digital download retailers.  All artwork for each album was also done by IAMISEE.

=== Ongoing career ===

IAMISEE is currently collaborating with Belgium's ''DJ Iron'' and is expected to follow up ''IAMISEE vs DJ Demon'' in 2013-14 with another album entirely produced, written, and performed by the artist.

== Controversy ==

Despite the international exposure of the Planet B-boy theme (registered with [[Broadcast Music, Inc.|BMI]]), distribution issues with the film's Production Company caused the artist to see very few royalty payments for the song.  There are no sources for any legal action regarding the matter.  Legal action was, however, taken by IAMISEE's original partner ''Grace'', aka Michael Lowe, against their former friend and collaborator Scott Storch.  In 2005 Lowe lost a Copyright Lawsuit against artists Dr. Dre, Xzibit and Scott Storch for the song ''X'', from [[Restless (Xzibit album)]].  According to the deposition Storch, a credited writer on the song, denied Lowe's involvement with the production of the beat for the song. Ultimately, Lowe lost the lawsuit. The ruling was not because Lowe didn't prove his intellectual property of the recording, but because he stated that he gave the beat to Storch to pass along to Dr. Dre and never expected to be paid.<ref>{{cite web|last=Kaufman|first=Gil|title=Songwriter Who Sued Xzibit Is Ordered To Pay Dr. Dre|url=http://www.mtv.com/news/articles/1499016/songwriter-must-pay-dre.jhtml|publisher=MTV News Top Stories|accessdate=14 April 2013}}</ref>  It is rumored that some of IAMISEE's early productions may have been used in a similar way, but currently there are no lawsuits or public statements.

== Meaning of the name ==

IAMISEE is a phonetic spelling of "I Mic" or "I emcee". Earlier Artist Bios cite the meaning as "I am what I see" or an extension of [[Cogito ergo sum]] (''"I think, therefore I am"'') interpreted as "I am, therefore I MC".  The artist also uses the acronym I.A.I.S. (Interdimensional Atoms in Suspension).

== Discography ==

=== Albums ===

*1999: ''IAMISEE''
*2001: ''83 (A.D. Three)'' (additional production by Gary Gnu)
*2004: ''Adamantium'' (Creep Records)
*2006: ''IAMISEE vs DJ Demon'' (additional scratching by DJ Demon)
*2007: ''The Invisible Man EP'' (mixed by Kameron Houff, digital release only)
*2010: ''Chop Suey'' (Instrumental Album, digital release only)
*2011: ''Fear & Loathing in the Music Business'' (production by Gary Gnu, digital release only)
*2012: ''The Notorious CASH'' (Remix Album, digital release only)

=== Singles ===

*2006: ''Hello'' (Album: ''IAMISEE vs DJ Demon'')
*2006: ''Exodus'' (Album: ''IAMISEE vs DJ Demon'')
*2006: ''Golden Sky'' (Album: ''IAMISEE vs DJ Demon'')
*2007" ''Live from Planet B-boy'' with Woody Pak (Soundtrack) Writer and Performer.
*2008: ''Laisse-Nous Croire'' Kery James and Kanya Samet (Album: ''À l'ombre du show business'', Warner Brothers France) Producer.
*2010: ''Only Human'' (Digital release)
*2010: ''OnWa Lickshot'' featuring Chuck Treece (Video Release) Writer and Performer.
*2011: ''One Love Revisted'' (Digital Release)
*2011: ''Threes'' (Album: ''Talib Kweli & The Year of the Blacksmith Present: Community Mixtape Vol 1'', Blacksmith Records) Producer, Writer, Performer.
*2012: ''Laisse-Nous Croire'' Kery James and Kanya Samet (Album: ''92.2012'') Producer.

== Notes ==
{{Reflist}}

==References==
*Stevens, Christian and Pak, Woody, ''Live from Planet B-boy'' 2007 Soundtrack BMI Work #9626974
*Stevens, Christian; Diamanka, Souleymane; Mathurin, Alix Jules; Malike, Zoubir, ''Laisse Nous Croire'' 2008 "A l'ombre du show buiness" BMI Work #10058436
*IAMISEE, ''Threes'', "Talib Kweli & The Year of the Blacksmith Present: Community Mixtape 2010 Blacksmith Records"
*IAMISEE, Treece, Chuck, Catastrophe, Tony, "OnWa Lickshot" Live in the Studdio, Video 2010
* 6 March 2007 "CMJ: Hip Hop Airplay" ''College Music Journal''
*"Rodney P & Skitz" BBC Radio, Radio 1xtra 8 January 2007
* ''XLR8R Magazine'', "IAMISEE", May 2007
*Jalonschi, Emanuel ''Beyond Race Magazine'', "The Art of the Wordsmith" Issue 09 2008
*http://iamisee.bandcamp.com/ "IAMISEE"

[[Category:American rappers]]