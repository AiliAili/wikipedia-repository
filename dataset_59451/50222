{{Use dmy dates|date=April 2012}}
{{Infobox UN resolution
|number = 1494
|organ = SC
|date = 30 July
|year = 2003
|meeting = 4,800
|code = S/RES/1494
|document = http://undocs.org/S/RES/1494(2003)
|for = 15
|abstention = 0
|against = 0
|subject = The situation in Georgia
|result = Adopted 
|image = Europe Location Georgia.svg
|caption = Georgia
}}

'''United Nations Security Council resolution 1494''', adopted unanimously on 30 July 2003, after reaffirming all [[United Nations Security Council resolution|resolutions]] on [[Abkhazia]] and [[Georgia (country)|Georgia]], particularly [[United Nations Security Council Resolution 1462|Resolution 1462]] (2003), the Council extended the mandate of the [[United Nations Observer Mission in Georgia]] (UNOMIG) until 31 January 2004 and endorsed the establishment of a police component.<ref>{{cite news|url=http://www.un.org/News/Press/docs/2003/sc7832.doc.htm|title=Security Council extends Observer Mission in Georgia until 31 January 2004|date=30 July 2003|publisher=United Nations}}</ref>

==Resolution==
===Observations===
In the preamble of the resolution, the Security Council stressed that the lack of progress on a settlement between the two parties was unacceptable. It condemned the shooting down of an UNOMIG helicopter in October 2001 which resulted in nine deaths and deplored that the perpetrators of the attack had not been identified. The contributions of UNOMIG and [[Commonwealth of Independent States]] (CIS) peacekeeping forces in the region were welcomed, in addition to the United Nations-led peace process. 

===Acts===
The Security Council welcomed political efforts to resolve the situation, in particular the "Basic Principles for the Distribution of Competences between [[Tbilisi]] and [[Sukhumi]]" to facilitate negotiations between Georgia and Abkhazia. It regretted the lack of progress on political status negotiations and the refusal of Abkhazia to discuss the document, further calling on both sides to overcome their mutual mistrust.<ref name=unnc>{{cite news|title=Security Council extends mandate of UN mission in Georgia 6 more months|url=http://www.un.org/apps/news/story.asp?NewsID=7872&Cr=georgia&Cr1=|date=30 July 2003|agency=United Nations News Centre}}</ref> All violations of the 1994 [[Agreement on a Cease-fire and Separation of Forces]] were condemned. The Council also welcomed the easing of tensions in the [[Kodori Valley]] and the signing of a protocol by both parties on 2 April 2002. Concerns of the civilian population were noted and the Georgian side was asked to guarantee the safety of UNOMIG and CIS troops in the valley.  

The resolution urged the two parties to revitalise the peace process, called for urgent progress to be made on issues relating to [[refugee]]s and [[internally displaced person]]s and reaffirmed the unacceptability of demographic changes resulting from the conflict. Both Georgia and Abkhazia were urged to implement recommendations from a joint assessment mission to the [[Gali District, Abkhazia|Gali region]], with Abkhazia in particular called upon to improve law enforcement, address the lack of instruction to ethnic Georgians in their [[first language]] and ensure the safety of returning refugees. 

The Council called again on both parties to take measures to identify those responsible for the shooting down of an UNOMIG helicopter in October 2001, and welcomed safeguards put in place since the downing of the helicopter. Both parties were also asked to dissociate themselves from military rhetoric and illegal armed groups. Additionally, it condemned the abduction and [[hostage]]-taking of four UNOMIG personnel–the sixth such incident–and the lack of identification of any suspects.

Finally, it endorsed the Secretary-General [[Kofi Annan]]'s proposal to establish a police component of 20 officers to strengthen UNOMIG's capacity,<ref name=unnc/> and requested him to keep the Council regularly informed of developments, reporting within three months on the situation.

==See also==
* [[Georgian–Abkhazian conflict]]
* [[List of United Nations Security Council Resolutions 1401 to 1500]] (2002–2003)
* [[United Nations resolutions on Abkhazia]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://undocs.org/S/RES/1494(2003) Text of the Resolution at undocs.org] 
{{wikisource}}

{{UNSCR 2003}}

[[Category:2003 United Nations Security Council resolutions]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:2003 in Georgia (country)]]
[[Category:2003 in Abkhazia]]
[[Category:United Nations Security Council resolutions concerning Georgia (country)]]
[[Category:United Nations Security Council resolutions concerning Abkhazia]]
[[Category:July 2003 events]]