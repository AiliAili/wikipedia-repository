{{Infobox company
| name             = Makelan Corporation
| logo             = 
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = Late 1990s
| founder          = 
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[New Braunfels, Texas]]
| location_country = [[United States]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = J. W. (Jeff) Shoemake (owner)<br>Sylvia Shoemake (manager)
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|www.hatzclassic.com}}
| footnotes        = 
| intl             = 
}}

The '''Makelan Corporation''' is an [[United States|American]] aircraft manufacturer based in [[New Braunfels, Texas]]. The company provides [[homebuilt aircraft|kits]] for the [[Hatz Classic]] [[biplane]] design.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 205. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 61. Belvoir Publications. ISSN 0891-1851</ref>

J. W. (Jeff) Shoemake is the company owner, while  Sylvia Shoemake is manager.<ref name="Contact">{{cite web|url = http://www.hatzclassic.com/Contacts_and_Links/contacts_and_links.html|title = Contact|accessdate = 10 September 2014|last = Makelan Corporation |date = n.d.}}</ref>

The company's sole product is a development of the [[Hatz CB-1]], that was designed by John Hatz in the 1960s. The Hatz Classic is an improved model with additional cockpit room, designed by Billy Dawson of [[Seguin, Texas]] in the 1990s. Dawson's prototype first flew in 1996 and won several awards, including ''Reserve Grand Champion - Plans Built'' at [[Airventure]] 1996, ''Grand Champion Experimental'' and ''Reserve Grand Champion Open Cockpit - Biplane'' at the 1997 Biplane Expo and ''Grand Champion - Plans Built'' at AirVenture 1997.<ref name="Plans">{{cite web|url = http://www.hatzclassic.com/History/history.html|title = History|accessdate = 10 September 2014|last = Makelan Corporation |date = n.d.}}</ref>

By 1998 the company reported that five Hatz Classic kits had been sold and three aircraft were completed and flying.<ref name="Aerocrafter" /> In September 2014 twelve examples were [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=HATZ+CLASSIC&PageNo=1|title = Make / Model Inquiry Results|accessdate = 10 September 2014|last = [[Federal Aviation Administration]]|date = 10 September 2014}}</ref>

== Aircraft ==
[[File:Hatz Classic (NX7WY) - 1.jpg|thumb|right|[[Hatz Classic]] in flight]]
{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Summary of aircraft built by Makelan Corporation'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[Hatz Classic]]'''
|align=center| 1990s
|align=center| at least 12
|align=left| homebuilt biplane
|-

|}

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.hatzclassic.com/}}
{{Makelan aircraft}}

[[Category:Aircraft manufacturers of the United States]]
[[Category:Homebuilt aircraft]]