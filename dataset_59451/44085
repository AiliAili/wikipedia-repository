{{Use mdy dates|date=February 2012}}
{{Infobox Airline
| airline        = LATAM Airlines Colombia
| image          = Latam-logo -v (Indigo).svg
| image_size     = 200
| image_bottom   = 
| fleet_size     = 22
| destinations   = 31
| IATA           = 4C
| ICAO           = ARE
| callsign       = LATAM COLOMBIA
| parent         = [[LATAM Airlines Group]] 
| company_slogan = ''Juntos, más lejos.'' (Spanish)<br/>''Together, further.'' (English) 
| founded        = 20 February 1981<ref>{{cite web|url=http://www.aires.aero/Web/uploads/Website/web_2009/Historia.swf |title=Aires History |language=Spanish |publisher=Aires.aero}}</ref> <small>(as AIRES)</small>
| commenced      = 1981 <small>(as AIRES)</small><br>December 3, 2011 <small>(as LAN Colombia)</small>
| frequent_flyer = LATAM Pass
| alliance       = [[Oneworld]] (affiliate)<ref name="Oneworld">{{cite web|url=http://atwonline.com/finance-amp-data/lan-colombia-join-oneworld-oct-1|title=LAN Colombia to join oneworld Oct. 1|publisher=Air Transport World|date=September 4, 2013|accessdate=September 4, 2013}}</ref>
| headquarters   = [[Bogotá]], [[Colombia]]
| key_people     = Fernando García Poitevin ([[CEO]])
| hubs           = [[El Dorado International Airport]]
| website        = {{URL|http://www.latam.com/}}
}}

[[File:Airbus A320-233, LAN Colombia (Aires Colombia) AN2232077.jpg|thumb|[[Airbus A320 family|Airbus A320]] in LAN Colombia livery]]

'''LATAM Airlines Colombia''', formerly known as '''LAN Colombia''', and previously as '''Aerovías de Integración Regional S.A.''' ([[Acronym]]: ''AIRES'', lit. ''airs''), is a [[Colombia]]n [[airline]]. It is the second-largest air carrier in Colombia.

The airline operates scheduled regional domestic and international services, as well as a domestic cargo service. Its main base is [[El Dorado International Airport]] in [[Bogota|Bogotá]].<ref name="FI">{{cite news | title= Directory: World Airlines | work= [[Flight International]] | pages= 69–70 | date= 2007-03-27}}</ref>

On 28 October 2010, it was announced that 98% of the shares in the previous airline AIRES had been acquired by [[Chile]]an carrier [[LATAM Airlines]]. On 3 December 2011, it started operations as LAN Colombia. It became an affiliate member of the [[Oneworld]] alliance on 1 October 2013.<ref name="Oneworld"/>

== Destinations ==
{{Main|LATAM Colombia destinations}}

== Fleet ==
[[File:AIRES-737-700.JPG|thumb|[[Boeing 737 Next Generation|Boeing 737-700]] in the former AIRES livery. This aircraft operated [[AIRES Flight 8250]], which crashed at [[Gustavo Rojas Pinilla International Airport]], killing 2 people.]]
[[File:LAN Colombia Boeing 737-700 Ramirez.jpg|thumb|[[Boeing 737 Next Generation|Boeing 737-700]] in LAN Colombia livery (2011)]]

LATAM Colombia's fleet consists of the following aircraft (as of August 2016):<ref>{{cite journal|title=Global Airline Guide 2016 (Part One)|journal=Airliner World|issue=October 2016|page=12|accessdate=14 October 2016}}</ref>

<center>
{| class="wikitable" border="1" cellpadding="3" style="border-collapse:collapse;text-align:center"
|+ LATAM Colombia Fleet
|- 
!Aircraft</span>
!In Service</span>
!Orders</span>
!Passengers</span>
!Notes</span>
|-
|[[Airbus A320 family|Airbus A319-100]]
|4
|&mdash;
|144
|
|-
|[[Airbus A320 family|Airbus A320-200]]
|17
|&mdash;
|174
|
|-
!Total
!22
!0
!
!
|}
</center>

== Incidents and accidents ==
*On 28 January 2008, AIRES Flight 053 overran the runway at Bogota's El Dorado airport, en route from Maracaibo, Venezuela after the left hand main gear collapsed. The aircraft was a [[Bombardier Dash 8|Dash 8-202]], registration HK-3997. Probable cause for the crash was that the aircraft was carrying out a landing with an unresolved fault in the left engine, which prevented the aircraft from being able to stop within the length of runway available, causing a runway excursion. A contributing factor was the failure to correct the maintenance reports in a satisfactory manner and failure to properly follow-up on repetitive entries.<ref>[http://aviation-safety.net/database/record.php?id=20080128-1 AIRES Flight 053]</ref>
*On 23 August 2008, AIRES Flight 051 sustained substantial damage following the collapse of the right hand main landing gear on landing at Barranquilla-Ernesto Cortissoz Airport (BAQ), Colombia. None of the 31 occupants were injured. The [[Bombardier Dash 8|Bombardier Dash 8-301]], registered HK-3952, operated on an international flight from Curaçao-Hato International Airport (CUR). The approach and landing were normal, touching down about 770 metres past the threshold of runway 23. The crew noticed a vibration of the right hand main gear. The undercarriage leg collapsed. There was no fire.<ref>[http://aviation-safety.net/database/record.php?id=20080823-2 AIRES Flight 051]</ref>
*On 16 August 2010, [[AIRES Flight 8250]] crashed on landing at [[Gustavo Rojas Pinilla International Airport]], in [[San Andrés (island)|San Andrés]], Colombia, after being struck by lightning during a thunderstorm. The death of one person was reported as a result of a heart attack on the way to the hospital and another 114 were injured.<ref name=bbc>{{Cite news | url = http://www.bbc.co.uk/news/world-latin-america-10988966 | title = Colombia plane crashes after lightning strike| accessdate=2010-08-16| date=2010-08-16| publisher = BBC News}}</ref> One of the injured occupants later died.<ref>"[http://www.aires.aero/comunicado_06.html Comunicado de Prensa 06]." AIRES. Retrieved on September 15, 2010.</ref>

== References ==
{{Reflist|2}}

== External links ==
{{Commons category|Aires Colombia}}
*{{Official website|http://www.lan.cl}}
*{{es icon}} [http://www.revistaconexion.net/ Conexión] (inflight magazine)
*{{es icon}} [http://www.latercera.com/noticia/negocios/2010/10/655-303083-9-bolsa-chilena-sube-tras-dato-de-produccion-y-lan-escala-13-tras-compra-de.shtml LAN Airlines buys Aires]
{{Portalbar|Colombia|Aviation|Companies}}

{{Airlines of Colombia|AIRES}}
{{Oneworld}}
{{LATAM Airlines Group}}
{{IATA members|latinam}}
[[Category:Airlines of Colombia]]
[[Category:LATAM Airlines Group|Colombia]]
[[Category:Oneworld affiliate members]]
[[Category:Latin American and Caribbean Air Transport Association]]
[[Category:IATA members]]
[[Category:Airlines established in 1981]]