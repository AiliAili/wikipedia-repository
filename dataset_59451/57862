{{Infobox journal
| title = Journal of Accounting and Public Policy
| cover = [[File:2015 cover J Account Public Policy.gif]]
| former_name = <!-- or |former_names= -->
| abbreviation = J. Account. Public Policy
| discipline = [[Accounting]], [[public policy]]
| editor = Lawrence A. Gordon
| publisher = [[Elsevier]]
| country = 
| history = 1982-present
| frequency = Bimonthly
| openaccess = [[Hybrid open access journal|Hybrid]]
| license = 
| impact = 0.547
| impact-year = 2014
| ISSN = 0278-4254
| eISSN =
| CODEN = JACPDN
| JSTOR = 
| LCCN = 83642516
| OCLC = 7828244
| website = http://www.journals.elsevier.com/journal-of-accounting-and-public-policy/
| link2 = http://www.sciencedirect.com/science/journal/02784254
| link2-name = Online archive
}}
The '''''Journal of Accounting and Public Policy''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering the interaction between [[accounting]] and [[public policy]]. It is published by [[Elsevier]] and was established in 1982. The [[editor-in-chief]] is [[Lawrence A. Gordon]] ([[University of Maryland]]).

The journal regularly publishes special issues on a focussed topic and sponsors an annual [[academic conference]], which rotates among the [[IE Business School]], the [[London School of Economics]], and the [[Robert H. Smith School of Business]].<ref>{{cite web |url=http://www.lse.ac.uk/accounting/news/JAAP.aspx |title=Journal of Accounting and Public Policy Conference at LSE |work= LSE Accounting: News |publisher=[[London School of Economics]] |accessdate=16 August 2015}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[ABI/Inform]]
* [[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |date=2015-06-01 |accessdate=2015-08-16}}</ref>
* [[RePEc]]
* [[Social Sciences Citation Index]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-08-16}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.547.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Accounting and Public Policy |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/journal-of-accounting-and-public-policy/}}

[[Category:Accounting journals]]
[[Category:Elsevier academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1982]]
[[Category:English-language journals]]


{{Accounting-journal-stub}}