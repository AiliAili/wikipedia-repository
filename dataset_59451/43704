{|{{Infobox Aircraft Begin
 |name=Stipa-Caproni
 |image=Caproni Stipa.jpg
 |caption=The Stipa-Caproni, piloted by [[Caproni]] company test pilot [[Domenico Antonini]], on a test flight on 7 October 1932.
}}{{Infobox Aircraft Type
 |type=Experimental aircraft
 |manufacturer=[[Caproni]]
 |designer=[[Luigi Stipa]]
 |first flight=7 October 1932
 |introduced=
 |retired=
 |status=
 |primary user=Italy
 |more users=
 |produced=
 |number built=1
 |variants with their own articles=
}}
|}
[[File:Stipa-Caproni side view.jpg|300px|right|thumb|The Stipa-Caproni with wheel spats removed.]]
The '''Stipa-Caproni''', also generally called the '''Caproni Stipa''', was an experimental [[Italy|Italian]] aircraft designed in 1932 by [[Luigi Stipa]] (1900–1992) and built by [[Caproni]]. It featured a hollow, barrel-shaped [[fuselage]] with the [[Aircraft engine|engine]] and [[propeller (aircraft)|propeller]] completely enclosed by the fuselage—in essence, the whole fuselage was a single [[ducted fan]]. Although the ''[[Regia Aeronautica]]'' (Italian Royal Air Force) was not interested in pursuing development of the Stipa-Caproni, its design was an important step in the development of [[jet propulsion]].<ref>O.E. Lancaster, ''High Speed Aerodynamics and Jet Propulsion. Vol. XII: Jet Propulsion Engines'', Princeton 1959 claims that "The Stipa Aero plane built by Caproni in 1932 should be classified as a Jet Aircraft. The Stipa Aero plane can be considered as a predecessor of the Jet Aircraft of today."</ref>

==Stipa's design==
[[File:Caproni Stipa from front.jpg|300px|thumb|left|A front view of the Stipa-Caproni, showing Stipa's "[[intubed propeller]]" design in which the propeller and [[Aircraft engine|engine]] are mounted inside a hollow tube which constitutes the airplane's [[fuselage]]. The spats have been removed from the landing gear.]] The design of the Stipa-Caproni was very similar to that of modern [[jet engine]]s; in fact, after having patented his design in Italy, [[Germany]], and the [[United States]] in 1938, Stipa became convinced that German [[rocket]] and jet technology (especially the [[V-1 flying bomb]]) was using his patented invention without giving proper credit. Stipa{{'}}s basic idea—which he called the "[[intubed propeller]]"—was to mount the engine and propeller inside a fuselage that itself formed a tapered duct, or [[venturi tube]], and compressed the propeller's airflow and the engine exhaust before it exited the duct at the trailing edge of the aircraft, essentially applying [[Bernoulli's principle]] of fluid movements to make the aircraft's engine more efficient.

Stipa spent years studying the idea [[Mathematics|mathematically]] while working in the Engineering Division of the Italian [[Air Ministry (Italy)|Air Ministry]], eventually determining that the venturi tube's inner surface needed to be shaped like an [[airfoil]] in order to achieve the greatest efficiency. He also determined the optimum shape of the propeller, the most efficient distance between the leading edge of the tube and the propeller, and the best rate of revolution of the propeller. Finally, he petitioned the Italian [[Italian fascism|Fascist]] government to produce a prototype aircraft. The government, seeking to showcase Italian technological achievement—particularly in aviation—contracted the [[Caproni]] company to construct the aircraft in 1932.<ref>Guttman, ''Aviation History'', March 2010, p. 18.</ref>

The resulting aircraft—a midwing monoplane of mostly wooden construction dubbed the Stipa-Caproni or Caproni Stipa—was strikingly ungainly in appearance. The fuselage was a barrel-like tube, short and fat, open at both ends to form the tapered duct, with twin open cockpits in tandem mounted in a hump on top of it. The wings were elliptical and passed through the duct and the engine [[nacelle]] inside it. The duct itself had a profile similar to that of the airfoils, and a fairly small [[rudder]] and [[Elevator (aircraft)|elevators]] were mounted on the trailing edge of the duct, allowing the ducted propeller wash to flow directly over them as it exited the fuselage to improve handling. The propeller was mounted inside the fuselage tube, flush with the leading edge of the fuselage, and the 120-[[horsepower]] [[de Havilland]] [[de Havilland Gipsy III|Gipsy III]] engine that powered it was mounted within the duct behind it at the midpoint of the fuselage. The aircraft had low, fixed, spatted main landing gear and a tailwheel, making it look as if it was squatting when on the ground. It was painted in a blue-and-cream scheme of the type used on racing aircraft of the day, and its rudder bore the colors of the [[Italian flag]].<ref>Guttman, ''Aviation History'', March 2010, p. 18-19.</ref>

==Test flights==
[[File:Stipa-Caproni front quarter view.jpg|300px||thumb|right|A front quarter view of the Stipa-Caproni with wheel spats removed.]]
The Stipa-Caproni first flew on 7 October 1932 with [[Caproni]] company test pilot [[Domenico Antonini]] at the controls. Initial testing showed that the "intubed propeller" design did increase the engine's efficiency as Stipa had calculated, and the additional lift provided by the airfoil shape of the interior of the duct itself allowed a very low landing speed of only {{convert|68|km/h|mph|abbr=on}} and assisted the Stipa-Caproni in achieving a higher rate of climb than other aircraft with similar power and [[wing loading]]. The placement of the rudder and elevators in the exhaust from the propeller wash at the trailing edge of the tube gave the aircraft handling characteristics that made it very stable in flight, although they later were enlarged to further improve the plane's handling characteristics. The Stipa-Caproni proved to be noticeably quieter than conventional aircraft of the time. Unfortunately, the "intubed propeller" design also induced so much [[aerodynamic drag]] that the benefits in engine efficiency were cancelled out, and the aircraft's top speed proved to be only {{convert|131|km/h|mph|abbr=on}}.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

When Caproni had completed initial testing, the ''Regia Aeronautica'' took control of the plane and transferred it to [[Guidonia Montecelio]] for a brief series of further test flights. All test pilots reported that the plane was extremely stable in flight, to the point where it was difficult to change course; test pilots were also astounded by the very low landing speed and the consequently very short landing run.

[[File:Caproni Stipa on ground.jpg|300px|right|thumb|The Stipa Caproni on the ground with the spats removed from its landing gear. Its light blue and cream paint scheme, similar to that on racing planes of the time, is visible, as is the opening at the trailing edge of its fuselage on which its [[rudder]] and [[Elevator (aircraft)|elevators]] were mounted. The rudder is painted in the colors of the [[Italian flag]].]]
As the plane did not perform noticeably better than conventional aircraft designs, the ''Regia Aeronautica'' decided to cancel further development. No further prototypes were built.

==Influence==
Stipa himself never had intended his "intubed propeller" to be employed on single-engine aircraft like the Caproni-Stipa—which he viewed merely as a [[testbed]]—instead envisioning its use in large, multi-engine [[flying wing]] aircraft he had been designing in which the aerodynamic drag properties would not be significant, and the Italian government publicized the Stipa-Caproni's design as an example of Italian aviation technology prowess. None of Stipa's flying-wing aircraft designs were built, but experiences collected with the Stipa-Caproni did become an important influence in the development of the [[motorjet]]-powered [[Caproni Campini N.1]].<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

The test flights of the Stipa-Caproni also sparked much academic interest, and resulted in Stipa's work being studied in France, Germany, Italy, and the [[United Kingdom]], and by the [[National Advisory Committee for Aeronautics]] in the [[United States]]. France designed—but never constructed—an advanced night [[bomber]] based on a Luigi Stipa design in the mid-1930s, and various aircraft designs such as the German [[Heinkel T]] [[Fighter (aircraft)|fighter]] of 1940 are thought to have incorporated some of Stipa's ideas as demonstrated by the Stipa-Caproni.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

The [[Kort nozzle]] ducted fan of today—designed in Germany in 1934—uses many of Stipa's principles, and the modern [[turbofan engine]] is thought by some aviation historians to be a descendant of the "intubed propeller" demonstrated in the Stipa-Caproni.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

==Replica==

In [[Australia]], [[Lynette Zuccoli]] and [[Aerotect Queensland]] designed a 3/5-scale replica of the Stipa-Caproni, accurate even in terms of paint scheme and markings, powered by an Italian [[Simonini]] racing engine. They built it in 1998 and in October 2001 succeeded in making two directional test flights with it with [[Bryce Wolff]] at the controls. Each flight covered about {{convert|600|m|yd|abbr=off}} and reached an altitude of approximately {{convert|6|m|ft|abbr=off}}, with Wolff reporting that the replica was very stable in flight and performed much as the Italian test pilots reported that the original aircraft had 69 years to the month earlier.<ref>[http://www.seqair.com/Hangar/Zuccoli/Legends/Legends.html Legends in Our Own Lunchtimes: The Stipa Caproni]</ref> The replica may never have flown again, and apparently now is on static display in Australia.<ref>Guttman, ''Aviation History'', March 2010, p. 19.</ref>

==Operators==
;{{flag|Kingdom of Italy}}
:''[[Regia Aeronautica]]''

==Specifications (original Stipa-Caproni)==
[[File:Caproni Stipa.svg|right|200px]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank.

-->
|ref=
<!--
        General characteristics
-->
|crew=1 or 2
|capacity=
|payload main=
|payload alt=
|length main= 5.88 m 
|length alt= 19 ft 4 in
|span main=14.28 m 
|span alt= 46 ft 10 in
|height main=3 m 
|height alt= 9 ft 10 in
|area main= 
|area alt= 
|airfoil=
|empty weight main= 
|empty weight alt= 
|loaded weight main= 800 kg 
|loaded weight alt= 1,760 lb
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
<!--
        Powerplant
-->
|engine (prop)= [[de Havilland Gipsy]] III
|type of prop=inline piston engine
|number of props=1
|power main= 120 hp 
|power alt= 90kW
|power original=
|propeller or rotor?=propeller
|propellers= 1
|number of propellers per engine= 1
|propeller diameter main=
|propeller diameter alt= 
<!--
        Performance
-->
|max speed main= 131 km/h
|max speed alt= 81 mph
|max speed more=
|cruise speed main= 
|cruise speed alt= 
|cruise speed more=
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|range more=
|combat radius main=
|combat radius alt=
|combat radius more=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
*'''Landing speed''': {{convert|68|km/h|mph|abbr=on}}
  
|avionics=

}}

==References==
<references/>

==Sources==
*Guttman, Robert. "Caproni Flying Barrel: Luigi Stipa Claimed His 'Intubed Propeller' Was the Ancestor of the Jet Engine." ''Aviation History''. March 2010. ISSN 1076-8858.
*Thompson, Jonathan W. ''Italian Civil and Military Aircraft, 1930-1945'', Aero Publishers, 1963

==External links==
{{commons category|Stipa-Caproni}}
*[http://www.aerotec.com.au/Luigi_Stipa.html Luigi Stipa - a pioneer of jet flying]
*[http://blog.modernmechanix.com/2006/08/18/new-italian-airplane-for-high-speed-is-a-flying-tunnel/ Modern Mechanix article (January 1933)]
*[https://www.youtube.com/watch?v=xYqr2h_xQRk Flight footage]
*[http://www.seqair.com/Hangar/Zuccoli/Legends/Legends.html Legends in Our Own Lunchtimes: The Stipa Caproni (a replica of the Stipa-Caproni built in 1998)]

{{Caproni aircraft}}

[[Category:Caproni aircraft]]
[[Category:Italian experimental aircraft 1930–1939]]
[[Category:Ducted fan-powered aircraft]]
[[Category:Monoplanes]]