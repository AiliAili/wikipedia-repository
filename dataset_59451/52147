{{Infobox journal
| title = Laboratory Investigation
| cover =
| editor = [[Gene P. Siegal]]
| discipline = [[Pathology]]
| former_names =
| abbreviation = Lab. Invest.
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Monthly
| history =
| openaccess =
| license =
| impact = 3.676
| impact-year = 2014
| website = http://www.nature.com/labinvest/index.html
| link1 = http://www.nature.com/labinvest/archive/index.html
| link1-name = Online archives
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 0023-6837
| eISSN = 1530-0307
}}
'''''Laboratory Investigation''''' is a [[peer review|peer-reviewed]] [[medical journal]] of [[pathology]] published by the [[Nature Publishing Group]]. The journal had a 2014 [[impact factor]] of 3.676, ranking it 13th out of 75 in the category "Pathology" and 30th out of 123 in the category "Medicine, Research & Experimental". It is the official journal of the [[United States and Canadian Academy of Pathology]]. The journal is published monthly, with one supplemental issue per year.

'''''Pathobiology in Focus''''' is a special publication established in 2006, published in parallel with ''Laboratory Investigation''. It publishes select articles deemed to have higher profiles.<ref>
{{cite journal
 |author1=JR Turner |author2=A Maitra |author3=Y Natkunam |author4=BP Rubin |author5=MA Rubin |author6=MA Teitell |year=2006
 |title=Bringing Pathobiology into Focus
 |journal=Laboratory Investigation
 |volume=86 |pages=632
 |doi=10.1038/labinvest.3700433
 |issue=7
}}</ref>

==Abstracting and indexing==
''Laboratory Investigation'' is abstracted and indexed in the following databases
*[[Index Medicus]]
*[[Current Contents]]/Life Sciences
* [[Science Citation Index]]
*[[Excerpta Medica]]

==References==
{{reflist}}

==External links==
*[http://www.nature.com/labinvest/index.html ''Laboratory Investigation'' website]
*[http://www.uscap.org/home.htm United States and Canadian Academy of Pathology website]

[[Category:Pathology journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]