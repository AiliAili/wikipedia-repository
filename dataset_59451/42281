{|{{Infobox aircraft begin
  |name = Eighteen
  |image = StateLibQld 1 126935 Landing of aeroplane on Four Mile Beach, Port Douglas, ca. 1933.jpg
  |caption = Avro 642/2m (VH-UXD), c.1936
}}{{Infobox aircraft type
  |type = [[Airliner]]
  |manufacturer =[[Avro]]
  |designer =
  |first flight =  1934
  |introduced = 1934
  |retired =
  |status =
  |primary user =
  |more users =
  |produced =
  |number built =2
  |unit cost =
  |variants with their own articles =
}}
|}
The '''Avro 642 Eighteen''' was a 1930s [[United Kingdom|British]] [[monoplane]] [[airliner]]. Only two were built &mdash; one twin-engined and the other four-engined.<ref name="ab" /><ref name="Jackson" />

==Development==
As a follow-on to the [[Avro 618 Ten]] the Eighteen was a larger aircraft but used a modified Avro Ten wing.<ref name="ab" /> The wing was moved to the [[Shoulder wing|shoulder position]] with the engines mounted on the wing and a new larger fuselage was used.<ref name="ab" /> When the design was completed in February 1933 it was the largest aircraft designed by Avro.<ref name="ab" /> The new [[fuselage]] was a [[Aircraft fabric covering|fabric-covered]] welded steel structure with seats for 16 passengers, a baggage area and a toilet.<ref name="ab" /> The nose section was made of wood and originally had a semi-circular glazed front, although this was later changed to a more conventional-type windscreen.<ref name="ab" /> The wooden wing was designed to use any of the engines in the Armstrong-Siddeley family and the first aircraft was built with two [[Armstrong Siddeley Jaguar|Armstrong Siddeley Jaguar VID]] engines with four-bladed propellers.<ref name="ab" />

With an order from the [[Midland and Scottish Air Ferries]] the first aircraft was nearly complete by December 1933.<ref name="ab" /> [[United Kingdom aircraft registration|Registered]] ''G-ACFV'' the aircraft performed official performance trials at Martlesham Heath and was then returned to [[Woodford Aerodrome]] when the nose was modified.<ref name="ab" /> On 6 April 1934, G-ACFV was handed over to the customer and was then flown to Castle Bromwich to pick up [[Prime Minister of the United Kingdom|Prime Minister]] [[Ramsay MacDonald]] and the Secretary of State for Air [[Charles Vane-Tempest-Stewart, 7th Marquess of Londonderry|Lord Londonderry]].<ref name="ab" /> With these important passengers on board, G-ACFV was flown to [[Liverpool Airport|Speke Aerodrome]] so that they could declare open a new air service between Glasgow, London and Belfast. At the same time Lord Londonderry named the aircraft ''The Marchioness of Londonderry''. Following the ceremony the Prime Minister and party were flown to Heston Aerodrome in London.<ref name="ab" />

The second aircraft was ordered by the [[Viceroy of India]], [[Freeman Freeman-Thomas, 1st Marquess of Willingdon|Lord Willingdon]] and was completed in November 1934 with four [[Armstrong Siddeley Lynx|Armstrong Siddeley Lynx IVC]] engines and long rectangular windows on each side.<ref name="ab" />

==Operational history==
From 6 April 1934 the Eighteen was used with other aircraft on the Glasgow-London-Belfast service by Midland and Scottish Air Ferries.<ref name="ab" /> In May 1935 G-ACFV was sold to Brian Allen Aviation for use with a variety of associated companies including ''Commercial Air Hire'' and ''Air Dispatch Limited''.<ref name="ab" /> CAH used the aircraft for excursions and joy riding duties while Air Dispatch operated a daily newspaper flight between London and Paris, and a passenger service on the weekend.<ref name="ab" /> Due to the increase in freight business the aircraft was fitted with a larger loading door.<ref name="ab" /> Last flown by Commercial Air Hire in June 1936, a sightseeing trip around the [[RMS Queen Mary]], the aircraft was sold as ''VH-UXD'' to [[Mandated Airlines]] in Australia.<ref name="ab" /> She was destroyed in New Guinea on 21 January 1942 by Japanese military forces.<ref name="ab" />
[[File:Avro 642a.jpg|thumb|right|The Avro 642/2m with original, pre-deleivery semi-circular nose]]   
The four-engined second aircraft [[Aircraft registration|Registered]] ''VT-AFM'' was handed over on 12 December 1934 and named the ''Star of India''.<ref name="ab" /> By arrangement with the Indian Government the aircraft was operated and maintained by [[Indian National Airways]] when not required by the Viceroy.<ref name="ab" /> The aircraft was taken over by the [[Royal Air Force]] as ''L9166'' in 1937 for use by AHQ India and was dismantled at Delhi in 1940.<ref name="Robertson" /> It has been said that the performance of the four-engined plane was poor, even dangerous, due to the reduced wing area caused by the extra "[[power-egg]]s".<ref name="joubert" />

==Variants==
;642/2m
:Two 450&nbsp;hp (336&nbsp;kW) [[Armstrong Siddeley Jaguar|Armstrong Siddeley Jaguar VID]] engines mounted on the wings, one built.<ref name="ab" /><ref name="Jackson" />
;642/4m
:Four 215&nbsp;hp (160&nbsp;kW) [[Armstrong Siddeley Lynx]] engines, one built.<ref name="ab" /><ref name="Jackson" />

==Operators==
;{{AUS}}
*[[Mandated Airlines]]
;{{flagicon|India|British}} [[British Raj|British India]]
*[[Indian National Airways]]
;{{UK}}
*[[Commercial Air Hire]]
*[[Midland and Scottish Air Ferries]]
*[[Royal Air Force]]

==Specifications (641/2m)==
{{aerospecs
|ref=<small>British Civil Aircraft since 1919 Volume 1</small><ref name="Jackson" />
|met or eng?=eng

|genhide=

|crew=two
|capacity=16 passengers
|length m=16.62
|length ft=54
|length in=6
|span m=21.72
|span ft=71
|span in=3
|height m=3.51
|height ft=11
|height in=6
|wing area sqm=67.7
|wing area sqft=728
|empty weight kg=3345
|empty weight lb=7360
|gross weight kg=5363
|gross weight lb=11800

|eng1 number=2
|eng1 type=[[Armstrong Siddeley Jaguar|Armstrong Siddeley Jaguar IVD]]
|eng1 kw=<!-- prop engines -->336
|eng1 hp=<!-- prop engines -->450
|eng2 number=
|eng2 type=

|perfhide=

|max speed kmh=258
|max speed mph=160
|range km=970
|range miles=600
|ceiling m=4275
|ceiling ft=15500
|climb rate ms=4.9
|climb rate ftmin=970
}}

==See also==
{{Aircontent|
|related=
* [[Avro 618 Ten]]
|similar aircraft=
|sequence=
|lists=
* [[List of aircraft of the RAF]]
|see also=
}}

==References==

===Notes===
{{reflist|refs=
<ref name="Jackson">Jackson 1973, pp. 128-129</ref>
<ref name="Robertson">Robertson 1987, p. 100</ref>
<ref name="ab">{{cite journal |last=Monk |first=D.E. |authorlink=  |coauthors= |date=April 1963|title=The Avro Eighteen|journal=Air-Britain's Aviation Review |volume=0 |issue=1 |pages=4–5|publisher=[[Air-Britain]]}}</ref>
<ref name="joubert">Joubert 1964, pp. 104-108</ref>
}}

==Bibliography==
{{commons category|Avro 642 Eighteen}}
{{refbegin}}
*{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 1|year= 1973|publisher= Putnam|location= London|isbn=0-370-10006-9 }}
*{{cite book |last=Robertson |first=Bruce |title= British Military Aircraft Serials 1878-1987 |year=1987 |publisher= Midland Counties Publications| location= Letchworth, England |isbn=0-904597-61-X}}
*{{cite book |last=Joubert |first=Philip |title= Fun and Games |year=1964 |publisher= Huchinson & Co| location= London, England }}

{{refend}}

<!-- ==External links== -->

{{Avro aircraft}}

[[Category:Avro aircraft|642 Eighteen]]
[[Category:British airliners 1930–1939]]