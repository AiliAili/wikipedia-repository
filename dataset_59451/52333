{{Infobox journal
| cover = [[File:MormonStudiesReview.jpg|175px]]
| editor = [[J. Spencer Fluhman]]
| formernames = Review of Books on the Book of Mormon, FARMS Review of Books, FARMS Review
| discipline = [[Mormon studies]]
| abbreviation = Mormon Stud. Rev.
| publisher = [[Neal A. Maxwell Institute for Religious Scholarship]]
| country = United States
| frequency = Annually
| history = 1989-present
| website = http://publications.maxwellinstitute.byu.edu/periodicals/msr/
| link2 = http://publications.maxwellinstitute.byu.edu/periodicals/farms-review/
| link2-name = ''FARMS Review'' archives
| LCCN = 2004212364
| OCLC = 755663963
| ISSN = 2156-8022
| eISSN =
}}
'''''Mormon Studies Review''''' is an annual [[academic journal]] covering [[Mormon studies]] published by the [[Neal A. Maxwell Institute for Religious Scholarship]] at [[Brigham Young University]].  

== History ==
The ''Review of Books on the Book of Mormon'' was established in 1989 by the [[Foundation for Ancient Research and Mormon Studies]] (FARMS), with [[Daniel C. Peterson]] as founding [[editor-in-chief]]. It was renamed to ''FARMS Review of Books'' in 1996,<ref>{{Citation |last= Peterson |first= Daniel C. |authorlink= Daniel C. Peterson |title= Editor's Introduction: Triptych (Inspired by Hieronymus Bosch) |journal= FARMS Review of Books |volume= 8 |issue= 1 |year= 1996 |page= v |url= http://mi.byu.edu/publications/review/?vol=8&num=1&id=199 |accessdate= 2009-07-28}}</ref> to ''FARMS Review'' in 2003,<ref>{{Citation |last= Midgley |first= Louis |authorlink= Louis Midgley |title= Editor's Introduction: On Caliban Mischief |journal= The FARMS Review |volume= 15 |issue= 1 |year= 2003 |page= xi |url= http://mi.byu.edu/publications/review/?vol=15&num=1&id=482 |accessdate= 2009-07-28}}</ref> and finally to ''Mormon Studies Review'' in 2011,<ref name=farmsreview-22-2>{{Citation |last= Peterson |first= Daniel C. |authorlink= Daniel C. Peterson |title= Editor's Introduction: An Unapologetic Apology for Apologetics |journal= The FARMS Review |volume= 22 |issue= 2 |year= 2010 |url= http://maxwellinstitute.byu.edu/publications/review/?vol=22&num=2&id=803 |accessdate= 2011-02-04}}</ref><ref>{{citation |last= Stack |first= Peggy Fletcher |authorlink= Peggy Fletcher Stack |url= http://www.sltrib.com/sltrib/utes/54358137-78/mormon-institute-peterson-studies.html.csp |title= Shake-up hits BYU's Mormon studies institute |newspaper= The Salt Lake Tribune |date= 2012-06-26 |accessdate= 2013-04-05 }}</ref> as the FARMS brand had been phased out<ref name=farmsreview-22-2/> after being absorbed into the Maxwell Institute in 2006.<ref>{{cite web | title=BYU renames ISPART to Neal A. Maxwell Institute for Religious Scholarship | date=March 1, 2006 | work=News Release | publisher=Brigham Young University | url=http://news.byu.edu/archive06-Mar-maxwell.aspx | accessdate=2014-12-15}}</ref>

Under Peterson's editorship, the journal specialized in [[LDS apologetics]].<ref name=SplitEmerges/>  When FARMS joined with BYU in 1997, Peterson said to the ''[[Salt Lake Tribune]],'' "FARMS has often had a polemical edge and we are curious to see how or whether that will be accommodated."<ref>{{citation|first = Peggy Fletcher|last = Stack|authorlink = Peggy Fletcher Stack|url = http://nl.newsbank.com/nl-search/we/Archives?p_product=SLTB&p_theme=sltb&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=100F8F3689D9B08E&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title = Group Trying to Prove LDS Works Joins With BYU; But Can Controversial Questions Continue?|date = November 8, 1997|publisher = Salt Lake Tribune}}</ref>

==Reboot==
In 2012, Peterson was removed as editor and the journal entered hiatus as it sought to become more mainstream to Mormon studies.<ref name=SplitEmerges>{{citation |last= Stack |first= Peggy Fletcher |authorlink= Peggy Fletcher Stack |url= http://www.sltrib.com/sltrib/news/56184763-78/mormon-review-institute-studies.html.csp |title= Split emerges among Mormon scholars |newspaper= [[The Salt Lake Tribune]] |date= April 19, 2013 }}</ref>  In March 2013, the Maxwell Institute announced the journal would relaunch as a new [[religious studies]] [[review journal]], without a primary focus on apologetics.  [[J. Spencer Fluhman]], from BYU's department of history, was appointed editor-in-chief with a new broad-based advisory board.<ref name=announcing>{{citation |url= http://maxwellinstitute.byu.edu/announcing-the-new-mormon-studies-review/ |author= Blair Hodges |title= Announcing the new Mormon Studies Review |work= Maxwell Institute Blog |publisher= [[Maxwell Institute]] |date= 2013-03-25 |accessdate= 2014-12-18}}</ref><ref>{{cite web|url=http://www.mormoninterpreter.com/the-role-of-apologetics-in-mormon-studies/ |title=The Role of Apologetics in Mormon Studies &#124; Interpreter |publisher=Mormoninterpreter.com |date=2012-12-14 |accessdate=2013-09-04}}</ref><ref>{{citation |url= http://universe.byu.edu/2013/03/26/neal-a-maxwell-institute-announces-mormon-studies-review/ |title= Neal A. Maxwell Institute announces "Mormon Studies Review" |newspaper= [[The Universe (BYU)|The Universe]] |first= Anna |last= Wendt |date= March 26, 2013}}</ref><ref>{{citation |author= From BHodges |url= http://maxwellinstitute.byu.edu/seven-questions-for-spencer-fluhman/ |title= Seven Questions for Spencer Fluhman |work= Maxwell Institute Blog |publisher= [[Maxwell Institute]] |date= 2013-03-27 |accessdate= 2014-12-18}}</ref>  The new ''Review'' changed from biannual to annual publication, and restarted its numbering, beginning at volume 1 in 2014, signifying its change in editorial direction as a new publication.<ref>{{cite web | title=Periodicals: FARMS Review| work=Neal A. Maxwell Institute for Religious Scholarship | publisher=Brigham Young University | url=http://publications.maxwellinstitute.byu.edu/periodicals/farms-review/ | accessdate=2014-12-15}}</ref><ref>{{cite journal | author=[[J. Spencer Fluhman]] | title=Friendship: An Editor's Introduction | year=2014 | journal=Mormon Studies Review | volume=1 | issue=1 | page=2 | url=http://publications.maxwellinstitute.byu.edu/fullscreen/?pub=2402&index=1 | accessdate=2014-12-15}}</ref><ref name=announcing/>

== Selected apologia of note ==
A review of [[Christopher Hitchens]]'s ''[[God Is Not Great]],'' published in ''FARMS Review'' by [[William J. Hamblin]] in 2009,<ref>{{cite journal |author=[[William J. Hamblin]] |url=http://maxwellinstitute.byu.edu/publications/review/?vol=21&num=2&id=773 |title=The Most Misunderstood Book: christopher hitchens on the Bible |journal=FARMS Review |volume=21 |issue=2 |publisher=Maxwell Institute |date=2009 |accessdate=2013-09-05}}</ref> was highlighted in a more extensive criticism of Hitchens by atheistic American philosopher and social critic [[Curtis White (author)|Curtis White]] in 2013.<ref>{{citation|url = https://books.google.com/books?id=e2mPgcXPNNIC&pg=PT22&dq=#v=onepage&q&f=false|title = The Science Delusion: Asking the Big Questions in a Culture of Easy Answers|first = Curtis|last = White|authorlink = Curtis White (author)|publisher = [[Melville House Publishing]]|year = 2013|isbn = 9781612192017}}</ref><ref>{{cite web|last=White |first=Curtis |url=http://www.salon.com/2013/06/23/christopher_hitchens_lies_do_atheism_no_favors/singleton/ |title=Christopher Hitchens’ lies do atheism no favors |publisher=Salon.com |date=2013-06-23 |accessdate=2013-09-05}}</ref>

== See also ==
{{Portal|Latter-day Saints}}
* [[Interpreter (journal)|''Interpreter'']]
* [[List of Latter Day Saint periodicals]]
* [[List of theological journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://publications.maxwellinstitute.byu.edu/periodicals/msr/}}
* [http://publications.maxwellinstitute.byu.edu/periodicals/farms-review/ Archive of ''FARMS Review'', predecessor to the ''Mormon Studies Review'']
* [http://maxwellinstitute.byu.edu/intro-msr-v1/ Introducing the inaugural issue of the Mormon Studies Review]

{{LDSChurchpubs}}

{{DEFAULTSORT:Mormon Studies Review}}
[[Category:Annual journals]]
[[Category:English-language journals]]
[[Category:1989 in Christianity]]
[[Category:2014 in Christianity]]
[[Category:Brigham Young University publications]]
[[Category:Publications established in 1989]]
[[Category:Publications established in 2014]]
[[Category:Mormon apologetics]]
[[Category:Maxwell Institute]]
[[Category:1989 establishments in Utah]]
[[Category:2014 establishments in Utah]]
[[Category:Academic journals published by universities and colleges of the United States]]