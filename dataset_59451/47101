{{Multiple issues|
{{primary sources|date=October 2011}}
{{orphan|date=January 2011}}
{{cleanup|date=January 2011}}
}}

'''Cloud''' or '''Cloude''' is a [[surname]] found in early [[England]] and in some [[Indigenous peoples of the Americas|Native American]] families.

==Origins==
It is not known when a name was first used as a family name and passed from a parent to the children (often from father to son), but instances of the practice are found in [[Europe]] approximately 1000 A.D. The family name or surname was often a modification of the father's name, or the name of a [[landmark]] or geographical location, an occupation or defining event, the name of a physical object or phenomenon of nature, etc.{{citation needed|date=October 2011}}

The earliest known use of the Cloude/Cloud surname is in [[medieval England]], where it is also recorded as de la Cloude, Clowd and Clowde.<ref>{{cite web|url=http://cloudhistory.com/1cdocs/delacloud.php | title=Cloudes of Medieval England}}</ref><ref>{{cite web|url=http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=188-dr18_1&cid=1-1-56-30#1-1-56-30 |title=de la Cloude, 13th century |publisher=Nationalarchives.gov.uk}}</ref><ref>{{cite web|url=http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=188-dr18_1&cid=1-1-56-32#1-1-56-32 |title=de la Cloude, 13th century |publisher=Nationalarchives.gov.uk}}</ref><ref>{{cite web|url=http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=188-dr18_1&cid=1-1-56-38#1-1-56-38 |title=de la Cloude, 13th century |publisher=Nationalarchives.gov.uk}}</ref><ref>{{cite web|url=http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=188-dr18_1&cid=1-1-55-3#1-1-55-3 |title=Isolda de la Cloude, dated 1275 |publisher=Nationalarchives.gov.uk}}</ref> The surname may have come from a place name or a geographical feature.

The name is found in Native American families, Chief [[Red Cloud]] being an example, but the name was not passed on to his children. (Early Native Americans (indigenous peoples of America) did not use the naming convention of the Europeans, choosing instead to name their children after places, animals, events, [[religious symbol]]s, etc.) <ref>{{cite web|url=http://www.snowwowl.com/swolfNAnamesandmeanings.html |title=Native American Names & Meanings |publisher=Snowwowl.com |date= |accessdate=2010-12-15}}</ref>

==Variants==
''(Listed in alphabetical order.)''

{{refimprove section|date=October 2011}}

===Known variants===
* Cloud, Cloude, Clowd, Clowde, de la Cloude.

===Likely variants===
''(References found indicate these names are related to or are the same family(s).)''
* Cloudsleigh, Cloudesleigh, Cloudsley, Cloudesley

===Possible variants===
''(Errors in [[transcription (linguistics)|transcription]] or personal preference are some of the reasons surnames change over time. The surnames below have similar spelling, sound similar or reflect [[regional spelling]]s and pronunciations and could be variants of the Cloud/Cloude surname.)''
* Calloud, Caloud, Calud, Chlud, Chludn, Claud, Cleod, Clewed, Cllud, Cloda, Cloedt, Cloet, Clood, Cloode, Cloodt, Clou, Clouad, Clouda, Cloudan, Cloudas, Cloudax, Cloudd, Clouded, Clouden, Clouder, Cloudes, Cloudet, Cloudey, Cloudia, Cloudie, Cloudis, Cloudly, Clouds, Cloudsy, Cloudt, Cloudus, Cloudy, Cloue, Clous, Clout, Cloute, Cloutt, Cloutte, Cloux, Clowdas, Clowdes, Clowdis, Clowdos, Clowds, Clowdus, Cloyd, Clud, Cluda, Cludas, Cludd, Cludde, Clude, Cludia, Cludie, Cludy, Cluids, Clut, Clutton, Colourde, Kloede, Klouda, Kloudos, Kloudt, Klude, Klut, Klutton, MacCloud, McCleod, McCloud, McLeod

==Countries of residence==
Some of the places Cloud surnamed families are known to have lived:<br />
[[Australia]], [[Canada]], [[France]], [[Germany]], [[Republic of Ireland|Ireland]], [[Mexico]], [[South Africa]], [[Netherlands]], [[United Kingdom]], [[United States]].

==People==
''(arranged chronologically by birth date)''
* [[Simon de la Cloude]], [[Isolda de la Cloude|Isolda]] his wife, [[William de la Cloude]] - earliest known (to this author) records of Cloudes, 13th century documents and later.<ref>[http://cloudhistory.com/1cdocs/delacloud.php Simon de la Cloude, Isolda his wife, William de la Cloude] Medieval Clouds</ref>
* [[Joseph Cloud]] (1770–1845), head of the melting and refining dept. of the [[US Mint]], at [[Philadelphia]] from Jan 1798 until Jan 14 1836.<ref>[http://mykindred.com/cloud/TX/getperson.php?personID=I52734&tree=mykindred01 Joseph Cloud] - MyKindred.com</ref><ref>Cloud Family Journal Vol. XIV, No. 4, p. 91</ref>
* [[David C. Cloud]] (1817–1903), first [[attorney general]] for the state of [[Iowa]], U.S.A.<ref>[http://mykindred.com/cloud/TX/getperson.php?personID=I65316&tree=mykindred01 David Comer Cloud] - MyKindred.com</ref>
* Chief [[Red Cloud]] (1822–1909), head Chief of the [[Oglala Lakota]] (Sioux) from 1868 to 1909.
* Col. [[William F. Cloud]] (1825–1905). [[Cloud County, Kansas]] was named for him.<ref name="mykindred1">[http://mykindred.com/cloud/TX/getperson.php?personID=I65320&tree=mykindred01 William Fletcher Cloud] - MyKindred.com</ref><ref>{{cite web|url=http://cloudgenweb.com/getperson.php?personID=I758&tree=cloud |title=CloudGenWeb.com |publisher=CloudGenWeb.com |date= |accessdate=2010-12-15}}</ref>
* [[Henry Roe Cloud]] (1884–1950), [[Winnebago Indian]], educator, college administrator, government official, [[Presbyterian]] minister, and reformer.<ref>[http://www.nsea.org/news/RoeCloudProfile.htm Henry Roe Cloud] {{webarchive |url=https://web.archive.org/web/20120205101426/http://www.nsea.org/news/RoeCloudProfile.htm |date=February 5, 2012 }} NSEA</ref><ref>[http://hrc.alumni.yale.edu/ The third Henry Roe Cloud conference] Yale Alumni</ref>
* [[Roger Cloud]] (1909–1988), Ohio state legislator (R) and auditor 1965-1966, [[Ohio]] [[Republican Party (United States)|Republican]] candidate for governor 1970.<ref>[http://mykindred.com/cloud/TX/getperson.php?personID=I72133&tree=mykindred01 Roger Ellwood Cloud] - MyKindred.com</ref>
* Dr. [[Preston E. Cloud]], PhD (1912–1991), member [[United States National Academy of Sciences|National Academy of Sciences]], paleontologist, geographer and educator.<ref>[http://mykindred.com/cloud/TX/getperson.php?personID=I72717&tree=mykindred01 Preston Cloud] - MyKindred.com</ref>
* [[Jack Cloud|Jack Martin "Flying Jack" Cloud]] (1925–2010), All-American football player at [[William & Mary]], running back for the [[Green Bay Packers]] and [[Washington Redskins]], inducted into the [[College Football Hall of Fame]] (1990), received the [[Jim and Rae Morgan Award]] for his service to the [[Touchdown Club]] of [[Annapolis, Maryland|Annapolis, MD]] (2006), Cherokee Indian.<ref>[http://articles.baltimoresun.com/2010-06-25/news/bs-md-ob-jack-cloud-20100625_1_mr-cloud-college-football-hall-touchdowns Flying Jack Cloud] - Baltimore Sun</ref><ref>[http://articles.dailypress.com/2010-06-26/news/dp-spt-teel-column-cloud-20100626_1_naval-academy-william-oklahoma-state Flying Jack Cloud] Daily Press, Hampton, VA</ref><ref>[http://mykindred.com/cloud/TX/getperson.php?personID=I160429&tree=mykindred01 Flying Jack Cloud] - MyKindred.com</ref>
* [[Jeff Cloud (politician)]] (born 1961), American politician from [[Oklahoma]]
* [[Chris Cloud]] (born 1988), Canadian professional ice hockey player

==Places named for (or thought to be named for) Cloud or variantly spelled families==
* [[Aaron G. Cloud House]], McLeansboro, [[Hamilton County, Illinois]], U.S.A.<ref>{{cite web|url=http://mykindred.com/cloud/TX/getperson.php?personID=I75278&tree=mykindred01 |title=Aaron G. Cloud |publisher=Mykindred.com |date= |accessdate=2010-12-15}}</ref>
* [[Abner Cloud House]], (park), [[Washington, D.C.]], U.S.A. 1802, on the [[Patowmack Canal]] built by [[George Washington]], is the oldest home on the canal.<ref>{{cite web|url=http://www.riverexplorer.com/details.php4?id=523 |title=Potomac River Site Details |publisher=Riverexplorer.com |date= |accessdate=2010-12-15}}</ref><ref name="autogenerated1">{{cite web|author=ERG, Eastern Region Geography, Geographic Names |url=http://geonames.usgs.gov/ |title=U.S. Board on Geographic Names |publisher=Geonames.usgs.gov |date=2009-05-01 |accessdate=2010-12-15}}</ref><ref>{{cite web|url=http://www.hmdb.org/marker.asp?marker=722 |title=Abner Cloud House marker |publisher=Hmdb.org |date= |accessdate=2010-12-15}}</ref>
* Abner Cloud House, 14 Ravine Rd., [[Brandywine Hundred]], [[Wilmington, Delaware]], U.S.A., aka Sawmill Farm; Mansion Farm.<ref>{{cite web|url=http://www.archiplanet.org/wiki/Cloud%2C_Abner%2C_House |title=Abner Cloud house |publisher=Archiplanet.org |date= |accessdate=2010-12-15}}</ref><ref name="nationalregisterofhistoricplaces1">{{cite web|url=http://www.nationalregisterofhistoricplaces.com/DE/New+Castle/state2.html |title=National Register of Historic Places |publisher=National Register of Historic Places |date= |accessdate=2010-12-15}}</ref>
* [[Bill Cloud Lake Dam]], [[Pearl River, Mississippi|Pearl River]], [[Mississippi]], U.S.A.<ref name="autogenerated1"/>
* [[Chief Hazy Cloud County Park]], [[Kent, Michigan]], U.S.A.<ref name="autogenerated1"/>
* [[Chief White Cloud State Monument]], [[Becker, Minnesota]], U.S.A.<ref name="autogenerated1"/>
* Cloud county, Kansas, U.S.A. named for Col. William Fletcher Cloud{{citation needed|date=October 2011}}
* [[Cloud's Town, Alabama|Cloud's Town]] was so named in 1829 by its founder [http://mykindred.com/cloud/TX/getperson.php?personID=I65320&tree=mykindred01 William Cloud]. It is now known as New Hope, Madison County, Alabama, U.S.A.<ref name="mykindred1"/>
* [[Clouds House]], [[Wiltshire]], England, built for [[Percy Wyndham (1835–1911)|Percy]] and Madeline Caroline Frances Eden (Campbell) Wyndham. ''(It's located where the 16th century Cloudes lived -- Wiltshire -- and is included for that reason.)'' <ref>{{cite web|author=Good Stuff IT Services |url=http://www.britishlistedbuildings.co.uk/en-321149-clouds-house-east-knoyle |title=Clouds House, East Knoyle |publisher=Britishlistedbuildings.co.uk |date=1966-01-06 |accessdate=2010-12-15}}</ref><ref>{{cite web|url=http://www.actiononaddiction.org.uk/documents/51/51-banner_wide.jpg |title=picture |date= |accessdate=2010-12-15}}</ref><ref>{{Cite book|url=https://books.google.com/books?id=gbTd5_RuzisC&printsec=frontcover&dq=%2B%22clouds+house%22+%2BWyndham&source=bl&ots=8MPtEazge4&sig=vYUJyw3ePIQcchuAZnJd9LeeRl4&hl=en&ei=wL4HTeezCYL-8Abp3smuCg&sa=X&oi=book_result&ct=result&resnum=3&ved=0CCgQ6AEwAg#v=onepage&q=%2B%22clouds%20house%22%20%2BWyndham&f=false |title=Clouds - the biography of a country house |publisher=Books.google.com |date= |accessdate=2010-12-15}}</ref>
* [[Cloud-Reese House]], 2202 Old Kennett Rd., Wilmington, Delaware, U.S.A.<ref name="nationalregisterofhistoricplaces1"/><ref>[http://www.archiplanet.org/wiki/Cloud--Reese_House Cloud-Reese house]</ref><ref name="landmarkhunter1">{{cite web|url=http://landmarkhunter.com/151379-cloud-home/ |title=LandmarkHunter.com |location=40.682060;-76.194107 |publisher=LandmarkHunter.com |date=1978-05-22 |accessdate=2010-12-15}}</ref>
* [[Cloud-Stark House]], 327 S. Dixon St., Gainesville, Texas, U.S.A.<ref name="autogenerated1"/><ref name="landmarkhunter1" />
* [[Flurnoy Cloud Cemetery]], [[Carroll County, Virginia]], U.S.A. (364014N, 0803436W), 1.5 miles NE of the James Cloud Cemetery.<ref name="autogenerated1"/><ref>{{cite web|url=http://www.histopolis.com/Place/US/VA/Carroll_County/Flurnoy_Cloud_Cemetery |title=Flurnoy Cloud Cemetery |publisher=Histopolis.com |date= |accessdate=2010-12-15}}</ref>
* [[James Cloud Cemetery]], Carroll county, Virginia, U.S.A. (364102N, 0803321W), 1.5 miles SW of the Flurnoy Cloud Cemetery.<ref name="autogenerated1"/><ref>{{cite web|url=http://www.histopolis.com/Place/US/VA/Carroll_County/James_Cloud_Cemetery |title=James Cloud Cemetery |publisher=Histopolis.com |date= |accessdate=2010-12-15}}</ref>
* [[Roe Cloud Hall]], [[Haskell Indian Nations University]], [[Douglas, Kansas]], U.S.A.<ref name="autogenerated1"/>
* [[Roy Cloud School]], [[San Mateo, California]], U.S.A.<ref name="autogenerated1"/><ref>{{cite web|url=http://mykindred.com/cloud/TX/getperson.php?personID=I77673&tree=mykindred01 |title=Roy W. Cloud |publisher=Mykindred.com |date= |accessdate=2010-12-15}}</ref>
* [[Temple Cloud]] is a village within the [[Chew Valley]] in [[Somerset]] in the [[Bath and North East Somerset Council]] area. It is located 10 miles from [[Bristol]] and [[Bath, Somerset|Bath]], very close to [[Clutton, Somerset|Clutton]]. Cloud is thought to come from the personal name Cloda.{{citation needed|date=October 2011}}
* [[Thomas A. Cloud Memorial Park]], [[Dayton, Ohio]], U.S.A. (394924N, 0840738W) <ref name="autogenerated1"/><ref>{{cite web|url=http://mykindred.com/cloud/TX/getperson.php?personID=I136082&tree=mykindred01 |title=Thomas A. Cloud |publisher=Mykindred.com |date= |accessdate=2010-12-15}}</ref>
* [[W.W. Cloud Lake]], Pearl River, Mississippi, U.S.A. (304831N, 0892036W) <ref name="autogenerated1"/>

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://mykindred.com/cloud/ The Cloud Family Association]
* [http://mykindred.com/cloud/dna/ Cloud DNA Project]
* [http://isogg.org/wiki/Cloud_DNA_Project International Society of Genetic Genealogists (ISOGG)]
* [http://mykindred.com/ Cloud on-line genealogies at MyKindred.com]
* [http://cloudgenweb.com/ Cloud on-line genealogies at CloudGenWeb.com]
* [http://cloudhistory.com/ The Cloud One Name Study Official Site]
* [http://www.one-name.org/profiles/cloud.html The Cloud One Name Study, # 5065 at GOONS]
* [http://boards.rootsweb.com/surnames.cloud/mb.ashx The Cloud bulletin board at rootsweb.com]
* [https://www.facebook.com/pages/Cloud-Genealogy/175505624763 Cloud Genealogy on FaceBook]

{{DEFAULTSORT:Cloud (Surname)}}
[[Category:Surnames]]
[[Category:Families]]
[[Category:English-language surnames]]