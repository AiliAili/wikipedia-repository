{{Infobox ice hockey player
| image        = Butch Bouchard.png
| image_size   = 200px
| position     = [[Defenceman (ice hockey)|Defence]]
| shoots       = Right
| height_ft    = 6
| height_in    = 2
| weight_lb    = 205
| played_for   = [[Montreal Canadiens]]
| birth_date   = {{birth date|1919|9|4}}
| birth_place  = [[Montreal]], [[Quebec|QC]], [[Canada|CAN]]
| death_date   = {{death date and age|2012|4|14|1919|9|4}}
| death_place  = [[Longueuil]], [[Quebec|QC]], [[Canada|CAN]]
| career_start = 1941
| career_end   = 1956
| halloffame   = 1966
}}

'''Joseph Émile Alcide "Butch" Bouchard''', [[Order of Canada|CM]], [[National Order of Quebec|CQ]] (September 4, 1919 – April 14, 2012) was a Canadian [[ice hockey]] player who played [[defenceman|defence]] with the [[Montreal Canadiens]] in the [[National Hockey League]] from [[1941–42 NHL season|1941]] to [[1955–56 NHL season|1956]]. He is a member of the [[Hockey Hall of Fame]], won four [[Stanley Cup]]s,  was [[captain (ice hockey)|captain]] of the Canadiens for eight years and was voted to the [[NHL All-Star Team]] four times. Although having a reputation as a clean player, he was also one of the strongest players and best body-checkers of his era. He excelled as a defensive defenceman, had superior passing skills and was known for his leadership and mentoring of younger players. In his early years in the NHL, Bouchard was one of the players who made a major contribution to reinvigorating what was at the time an ailing Canadien franchise.<ref>{{cite book|last=Hunter|first=Douglas|title=
Champions: the illustrated history of hockey's greatest dynasties|publisher=Penguin Group|year=1997}}</ref><ref name="hhof_bio">{{cite web|url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196603&type=Player&page=bio&list=#photo|title=Butch Bouchard Biography|publisher=Hockey Hall of Fame|accessdate=2009-08-12}}</ref><ref name="hab_hero">{{cite book|last=Campbell|first=Ken|title=Hab Heroes: the greatest Canadiens ever from 1 to 100 (Hockey News)|publisher=Transcontinental Books|page=115|isbn=978-0-9809924-0-3}}</ref>

He was born in [[Montreal]], [[Quebec]], and at the time his death resided in [[Saint-Lambert, Quebec]]. In retirement Bouchard was active with several business interests and contributions to his community. In 2008, he received the [[National Order of Quebec]]. On December 4, 2009, Bouchard's No. 3 was retired by the Canadiens as part of their 100th anniversary celebrations. On December 30, 2009, [[Michaëlle Jean]], Governor General of Canada, announced Bouchard as among the appointments to the Order of Canada.<ref>{{cite web|url=http://www.gg.ca/document.aspx?id=13469|title=Order of Canada - Appointments|publisher=The Governor General of Canada|date=2009-12-30|accessdate=2009-12-30}}</ref>

==Youth and learning the game==
Bouchard was born September 4, 1919, in Montreal the son of Regina Lachapelle and Calixte Bouchard.<ref name="EBB_bio">{{cite web|url=http://biographie.emilebutchbouchard.com/|title=Emile "Butch" Bouchard: Biography|publisher=emilebutchbouchard.com|language=french|accessdate=2009-12-14}}</ref><ref name="hhof"/> Growing up poor during the [[Great Depression|depression]], Bouchard did not begin skating until he was 16 and had to learn on rented skates, before borrowing $35 from his brother for a complete set of hockey equipment which included his own pair of skates.<ref name="darcy"/> Bouchard opted for a career in hockey over banking when he was offered $75 a week to play senior hockey and the bank paid $7.<ref name="RDS_Jan96"/> In the minors Bouchard played with the [[Verdun Maple Leafs (ice hockey)|Verdun Maple Leafs]], [[Montreal Junior Canadiens]] and [[Providence Reds]]. It was Verdun team-mate [[Bob Fillion]] who gave Bouchard the nickname "Butch".<ref name="hhof"/> It originated due to the resemblance of his last name to the English word "butcher".<ref name="EBB_bio"/> Bouchard was determined, strong and developed enough skills to impress coach [[Dick Irvin]] in the [[Montreal Canadiens|Canadiens]]’ 1940–41 training camp after which he was signed as a [[free agent]].<ref>{{cite book|last=Leonetti|first=Mike|title=Canadiens Legends|publisher=Raincoast Books|page=46|isbn=1-55192-731-4}}</ref> Bouchard had arrived at training camp in peak condition, which was unusual for [[National Hockey League]] (NHL) players of the time.<ref name="hhof_bio"/> To attend this first training camp he rode a bike {{convert|50|mi|km}}, which also allowed him to pocket the travel expenses the Canadiens had allotted.<ref name="Total Hockey">{{cite book|last=Diamond|first=Dan|title=Total Hockey:The Official Encyclopedia of the National Hockey League|publisher=Total Sports|year=1998|page=1746|isbn=0-8362-7114-9}}</ref><ref name="ourhistory">{{cite web|url=http://ourhistory.canadiens.com/player/Emile-Bouchard|title=Émile Bouchard (1941&ndash;1956)|publisher=NHL|accessdate=2009-08-12}}</ref><ref name="Star_Feb28_53_Dunnell">{{cite news|newspaper=Toronto Daily Star|title=Speaking on Sport|last=Dunnell|first=Milt|date=1953-02-28|page=10}}</ref>

In an era when hockey players were regarded by hockey management as rural and unsophisticated,<ref name="hab_hero"/> Bouchard had already developed his entrepreneurial skills. While still in high school he was working alongside an inspector with the Department of Agriculture when he came across a bee ranch owned by a priest who had just died. Borrowing $500 from his brother he bought the business.<ref name="Star_Feb28_53_Dunnell"/> He turned it into an [[apiary]] of 200 hives which was so successful he earned enough to buy his parents a home.<ref name="hhof"/><ref>{{cite news|first=Andy|last=Lytle|newspaper=Toronto Daily Star|date=1942-03-11|page=14|title=Speaking on Sports: Bouchard as beekeeper}}</ref><ref>{{cite news|newspaper=Toronto Daily Star|date=1941-11-29|page=17|title=Bouchard Has Bees in Bonnet But Not Headed For Nuthouse}}</ref> It was due to this business acuity that prior to signing with the Canadiens he uncovered what [[Ken Reardon]] and [[Elmer Lach]], already playing with the Montreal, were currently earning. Then, over the course of ten days he negotiated a larger contract than either player had been receiving, $3,750<ref name="Star_Feb28_53_Dunnell"/> (${{formatnum:{{Inflation|CA|3750|1941}}}} in {{CURRENTYEAR}} dollars{{inflation-fn|CA}}).<ref name="darcy"/>

==NHL career==

===Arrival to the Canadiens===
Along with a strong work ethic and keen intellect, Bouchard was physically imposing. At {{convert|6|ft|2|in|abbr=on}} and {{convert|205|lb}} he was considered a giant compared to NHL players of the 1940s, when the average height was {{convert|5|ft|8|in|abbr=on}} and average weight was {{convert|165|lb}}.<ref>{{cite web|url=http://pubs.nrc-cnrc.gc.ca/rp/rppdf/h06-012.pdf|title=Physiological profile of professional hockey players — a longitudinal comparison|author=D.L. Montgomery (deceased) McGill University|accessdate=2009-08-12|format=PDF}}</ref> Moreover, since he also practiced heavy [[weight training]] in an era before NHL players were concerned about upper body strength<ref>{{cite web|url=http://www.legendsofhockey.net/htmltimecap/GamesSummarySUM1972.shtml|title=quote by Team Canada assistant coach John Ferguson that most NHL players (prior to 1972) did little off-ice training|publisher=Hockey Hall of Fame|accessdate=2009-07-12}}</ref> he became a very effective defensive presence.<ref name="hhof_bio"/> Hockey Hall of Fame leftwinger and team-mate [[Dickie Moore (ice hockey)|Dickie Moore]] said of Bouchard: "He appeared to have been chiseled out of stone."<ref name="hab_hero"/>

By the time of Bouchard's arrival to the Montreal Canadiens the club had not won the championship for 10 years and attendance at the [[Montreal Forum|Forum]] was very low, often less than 3,000 a game,<ref name="hab_hero"/><ref>{{cite interview|last=Selke|first=Frank|title=quote concerning low Forum attendance in 1930s|work=NFB film The Rocket|url=http://nfb.ca/film/rocket/|year=1998|accessdate=2009-08-15|interviewer=Jacques Payette|publisher=National Film Board of Canada}} 2:27</ref> and there was talk of folding the franchise.<ref name="hhof"/> A few years earlier, in 1935, Canadien owners had seriously considered an offer to sell the team to be moved to Cleveland.<ref>{{cite book|first=Steve|last=Dryden|title=Century of Hockey (Hockey News)|publisher=McLelland & Stewart Ltd.|page=38|isbn=0-7710-4179-9}}</ref> After finishing last or near the bottom of the league for several years, apathy of the fans was matched by the players themselves who had accepted losing as way of hockey life. In his first training camp, he showcased his physical play by body-checking players, including veterans, with abandon. When the season started other teams discovered that with Bouchard in the lineup they could no longer push Canadien players around. Bouchard's presence reinvigorated the Canadiens and he is credited with playing an important part in keeping the franchise from leaving Montreal.<ref name="hhof_bio"/><ref name="hab_hero"/>

However, Bouchard was more than just a physical presence. He learned to play good positional hockey and became skilled at [[pass (ice hockey)|passing]] the puck.<ref name="hhof_bio"/> He also possessed a flair for judging the flow of the game and knew when to join the attack and when to retreat.<ref name="darcy">{{cite book|first=D'Arcy|last=Jenish|title=The Montreal Canadiens: 100 years of glory|year=2008|isbn=978-0-385-66324-3|page=100}}</ref> Despite his role as a "[[Stay-at-home defenceman|stay-at-home]]" defenceman, due to his skills for the long breakout pass, he was a contributor to the style of firewagon hockey<ref>{{cite book|first=Andrew|last=Podnieks|title=The Complete Hockey Dictionary|isbn=978-1-55168-309-6|publisher=Fenn publishing company ltd|year=2007}} Firewagon hockey: style of hockey featuring end-to-end rushes, exciting scoring chances, and wide-open play</ref>  for which the Canadiens exemplified.<ref name="hhof_bio"/><ref name="hhof"/><ref name="ourhistory"/><ref name="hon_can"/>

[[File:Montreal Canadiens hockey team, October 1942.jpg|right|thumb|alt=1942 Montreal Canadiens posing for photo on rink. First row of seven players kneeling and second row of seven players standing behind. |400px|Team photo [[1942&ndash;43 Montreal Canadiens season|1942]] Montreal Canadiens. The team which pulled the ailing franchise back from the brink of moving. Bouchard back row far right.]]
Though he had an immediate impact on the team, Bouchard had not scored many points for the team; in his first season, [[1941–42 NHL season|1941–42]], he collected six points in the [[Nhl regular season|regular season]] and scored the first NHL goal of his career in the Canadiens' [[Stanley Cup Playoff|first-round playoff]] loss to the [[Detroit Red Wings]].<ref>{{cite web|url=http://www.hockey-reference.com/teams/MTL/1942.html|title=Canadiens scoring totals 1941–42|publisher=Sports Reference LLC|accessdate=2009-08-15}}</ref><ref>{{cite web|url=http://www.hockey-reference.com/leagues/NHL_1942.html|title=playoff summary 1941–42|publisher=Sports Reference LLC|accessdate=2009-08-15}}</ref>
[[File:Hockey. Butch Bouchard BAnQ P48S1P12159.jpg|thumb|Bouchard in 1945]]

===NHL star===
The 1942–43 season was Bouchard's breakthrough year as he finished leading all Canadien defencemen in points<ref>{{cite web|url=http://www.hockey-reference.com/teams/MTL/1943.html|title=Canadiens scoring totals 1942–43|publisher=Sports Reference LLC|accessdate=2009-08-15}}</ref> and was key to the Canadiens' first season in several years without a losing record. They finished in fourth place with a record of 19 wins, 19 losses and 12 ties. Although they lost in the first round of the playoffs, the team was building in the right direction.<ref>{{cite web|url=http://www.hockey-reference.com/leagues/NHL_1943.html|title=season summary 1942–43|publisher=Sports Reference LLC|accessdate=2009-08-15}}</ref>

The [[1943–44 NHL season|1943–44 season]] was [[Maurice Richard]]'s first full season with the Canadiens. Richard was not just an exciting player to watch which served to increase attendance, but also had the offensive skills needed to turn the Canadiens into an exceptional team.<ref>{{cite web|title=Maurice Richard bio at Legends of Hockey|url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196108&type=Player&page=bio&list=#photo|publisher=Hockey Hall of Fame|accessdate=2009-08-16}}</ref> The Canadiens proceeded to dominate the regular season finishing well ahead of second-place Detroit. In the playoffs in the first round against [[Toronto Maple Leafs|Toronto]], after losing the opening game, they won the next four straight to win the series. Then, in the final they swept Detroit in four games to win their first [[Stanley Cup]] in thirteen years. While the [[Punch line (ice hockey)|"Punch Line"]] of Richard, [[Toe Blake]] and Lach provided the offensive power it was Bouchard and goal-tender [[Bill Durnan]] who kept the goals out. During the regular season Montreal had allowed only 109 goals, 68 less than second-place Detroit.<ref>{{cite web|title=Season and playoffs 1943–44 summary at Legends of Hockey|url=http://www.legendsofhockey.net/html/spot_pinnaclep196603.htm|publisher=Hockey Hall of Fame|accessdate=2009-08-16}}</ref> Bouchard along with Richard and Lach were named to the [[NHL All-Star Team|NHL All Stars']] second team and goaltender Bill Durnan made the first team and won the [[Vezina Trophy|Vezina]].<ref>{{cite web|url=http://www.hockey-reference.com/leagues/NHL_1944.html|title=NHL standings 1943–44|publisher=Sports Reference LLC|accessdate=2009-08-15}}</ref> Bouchard had become one of the most reliable defencemen in the league. He would be named to the NHL First All Star team, as one of the best defencemen in the league, for the next three seasons.<ref name="Total Hockey"/> He won his second Stanley Cup in 1945–46.<ref name="hhof"/>

As physical on the ice as Bouchard was, he was also regarded as a clean player and only rarely participated in [[Fighting in ice hockey|hockey fights]]. Immensely strong, most players avoided engaging him in fights and Bouchard more often would be the person to break up combatants.<ref name="hhof_bio"/> However, it was a fight involving Bouchard which led to a significant change in the role of [[Official (ice hockey)|referees]]. During the 1946–47 season, Bouchard became involved in a prolonged and one-sided fight with Boston's [[Terry Reardon]]. Due to the fight, [[Clarence Campbell]], president of the NHL, added to the duties of referees; for the first time they had the responsibility of breaking up fights.<ref>{{cite book|title=Years of Glory: 1942–1967 the National Hockey League's official book of the six-team era|first=Dan|last=Diamond|year=1994|page=26|publisher=McClelland & Stewart Inc.|isbn=0-7710-2817-2}}</ref> Then there was the time in March 1947, in a game in Boston, as the Canadiens were coming back onto the ice for the beginning of the third period, a female fan attacked Bouchard spearing him with a hat pin. Bouchard responded by pushing the woman away forcefully. A few moments later, Boston police were leading Bouchard out to a police car. According to Bouchard, [[Pat Egan]] of the [[Boston Bruins]], interceded and talked the police out of the arrest.<ref>{{cite news|newspaper=Gazette|location=Montreal|date=1947-03-31|page=16|last=Carroll|first=Dink}}</ref>

For the [[1947–48 NHL season|1947–48 season]], defenceman [[Doug Harvey (ice hockey)|Doug Harvey]] joined the team. Within a couple years Harvey would become the best offensive-oriented defenceman in the NHL and he and Bouchard would form a long-time and very effective defensive pairing.<ref name="hhof"/> Whenever Harvey undertook one of the offensive rushes for which he became famous, he was confident in the knowledge that Bouchard was backing him up if he was to lose the puck.<ref name="hab_hero"/>

===Leader and mentor===
In 1948, Bouchard became the first Quebec-born [[captain (ice hockey)|captain]] of the Canadiens, a position he retained for eight years until his retirement. At the time of his retirement no player had served more years as captain of the Canadiens than Bouchard.<ref name="hab_hero"/> Hall of Famer [[Jean Beliveau]], a teammate of Bouchard for Beliveau's early years with the Canadiens, said Bouchard was the model for his time as captain in the 1960s.<ref name="hhof"/> Bouchard was a well-respected leader and played a role in supporting and mentoring the younger players.<ref name="hon_can"/><ref name="Denault">{{cite book|last=Denault|first=Todd|title=Jacques Plante|page=81|publisher=McClelland & Stewart|year=2009|isbn=978-0-7710-2633-1}}</ref> Never afraid to speak up to management, in 1950 on Bouchard's recommendation to Selke to "give the kid a shot", [[Bernie Geoffrion]] was given a tryout and eventually joined the Canadiens.<ref name="hhof"/> Geoffrion won the [[Calder Memorial Trophy|Calder]] for rookie of the year and would be near the top of league scoring for years to come. Bouchard commenting on the fact that he was nominated for captain by his teammates: "I don't agree with management nominating you. I can respond to players, not be a yes-man for the proprietor."<ref>{{cite book|first=Chris|last=Mcdonell|title=Hockey Allstars: the NHL honor roll|publisher=Firefly Books|page=31|year=2000}}</ref> He missed a large part of the 1948–49 season after a severe knee injury which threatened his career. Despite medical opinion that he might not be able to continue to play he trained hard and was able to strengthen the knee enough to return to the Canadiens.<ref name="hhof"/>

In 1951, Bouchard was involved in a legal first when he was a defendant in a lawsuit brought by a [[New York Rangers]] fan. The fan claimed Bouchard had struck him with his stick when he was waving to a friend watching the game on TV. Bouchard said the fan had actually raised his fist towards a fellow Canadiens player who was being taken off the ice with an injury and his stick hit the fan accidentally as he tried to ward off the blow. In what may have been the first time in legal history, evidence was taken during a trial from someone witnessing an event on a television as the fan's friend testified he'd seen Bouchard strike the blow. Bouchard won the case when Otis Guernsey, president of [[History of Abercrombie & Fitch|Abercrombie and Fitch]], who was at the game testified he heard "vile language" and saw the fan raise his fist and not wave.<ref>{{cite news|newspaper=The Montreal Gazette|date=1951-01-30|title=Television Testimony Doesn't Help;Butch Bouchard Cleared|author=New York Times Service|page=16}}</ref><ref>{{cite news|newspaper=The Day (New London Evening Day)|agency=Associated Press|date=1951-01-30|title=TV Evidence Accepted in Hockey Suit|page=6}}</ref>

On February 28, 1953, the Canadiens had a "Bouchard Night at the Forum". Bouchard was honoured in a ceremony during the second intermission in a game against the Detroit Red Wings. It was presided over by Montreal Mayor [[Camillien Houde]] and broadcast nationally live over the CBC. Among the gifts Bouchard received was a Buick automobile which was driven out onto the ice. The organizer's plan was to have Bouchard drive off in the car at the end of the ceremony. However, sitting in the car Bouchard discovered the keys were missing. To the roar of the crowd [[Ted Lindsay]], captain of the Red Wings, returned the keys he had stolen and congratulated Bouchard on behalf of the Red Wings<ref name="Star_Feb28_53_Dunnell"/><ref>{{cite news|newspaper=The Montreal Gazette|date=1953-03-02|page=24|title=Crowds Demonstrate as Canadiens Lose to Detroit 4–3}}</ref>

In [[1952–53 NHL season|1952–53]], Montreal and Detroit battled for first place with Detroit coming out on top by the end of the season. In the first round of the playoffs the heavily favored Detroit Red Wings were upset by the Boston Bruins and Montreal won a close seven-game series over the [[Chicago Blackhawks|Chicago Black Hawks]]. The Canadiens then defeated Boston in five games and Bouchard won his third Stanley Cup.<ref>{{cite web|title=1952-53 NHL Season Summary|url=http://www.hockey-reference.com/leagues/NHL_1953.html|publisher=Sports Reference|accessdate=2010-01-07}}</ref>

Eventually injuries began to take their toll and at the conclusion of the 1954–55 season he considered retirement. Toe Blake, who had taken over as coach, talked him into playing one more season to assist the younger players. Bouchard recognized Blake's value as a "player's coach" and used his leadership as captain to ease the transition and encourage Blake's acceptance by the Canadiens players.<ref name="Denault"/> Due to physical problems Bouchard was forced to miss the last half of the season and the playoffs. However, in the deciding game of the Stanley Cup final against Detroit, Blake dressed Bouchard. As the final seconds counted down, with Montreal up 3–1, Blake put Bouchard on the ice and he was able to end his career with one more Stanley Cup celebration.<ref name="hhof"/>

==Personal life==
In 1947, Bouchard married Marie-Claire Macbeth, a painter.<ref name=Star_CP_Apr1412>{{cite news|title=Emile (Butch) Bouchard, former Montreal Canadiens captain, dead at 92|publisher=The Star|author=Canadian Press|location=Toronto|date= April 14, 2012 |accessdate=April 14, 2012|url=http://www.thestar.com/sports/hockey/nhl/article/1161693--emile-butch-bouchard-former-montreal-canadiens-captain-dead-at-92}}</ref> They had five children, Émile Jr., Jean, Michel, Pierre and Susan.<ref name="EBB_bio"/><ref name="RDS_Jan96">{{cite web|author=RDS.ca|title=ÉMILE "BUTCH" BOUCHARD, HOCKEY|language=french|publisher=CTVglobemedia|url=http://www.rds.ca/pantheon/chroniques/204863.html|date=1996-01-01|accessdate=2009-12-15}}</ref>

In the 1970s, his son [[Pierre Bouchard]], also a defenceman, played for the Montreal Canadiens. While father Émile participated in the birth of the Montreal Canadiens' dynasty, thirty years later son Pierre played a part in continuing the Canadien dynasty into the 1970s. With Butch's four and Pierre's five they have the distinction of winning the most Stanley Cups of any father-son combination in NHL history. Bobby and Brett Hull are the only other father and son to have won the Cup.<ref name="hab_hero"/>

In retirement Bouchard remained as active as he was during his NHL career. He received coaching offers soon after his retirement, but his business interests prevented him from leaving Montreal.<ref>{{cite news|first=Milt|last=Dunnell|newspaper=Toronto Daily Star|date=1956-07-25|title=Speaking on Sport|page=12}}</ref> Bouchard owned a popular restaurant Chez Émile Bouchard which operated for many years in Montreal. On March 22, 1953, while Bouchard was traveling to Detroit for the last game of the season, the restaurant was gutted by a fire started in a basement at 3:22am soon after employees and patrons had left.<ref name="hon_can">{{cite book|first=Andrew|last=Podnieks|title=Honoured Canadiens|publisher=Key Porter Books|year=2009|isbn=1-55168-356-3|page=16}}</ref><ref>{{cite news|newspaper=The Montreal Gazette|title=2-Alarm Fire Sweeps Butch Bouchard's|date=1953-03-23|page=3}}</ref> He was also president of the [[Montreal Royals|Montreal Royals Triple-A baseball club]], elected to the [[Longueuil]] municipal council, on the board of directors of Ste. Jeanne-d'Arc Hospital, president of the Metropolitan Junior "A" Hockey League among other activities.<ref name="hhof_bio"/><ref name="hhof">{{cite web|url=http://www.hhof.com/legendsofhockey/html/spot_oneononep196603.htm|title=Legends of Hockey: career and personal|publisher=Hockey Hall of Fame|accessdate=2009-08-11}}</ref>

Bouchard was unafraid to speak his mind when he felt the occasion demanded. In 1957, after an [[International League]] game in Toronto between his Montreal Royals and the [[Toronto Maple Leafs (International League)|Maple Leafs baseball team]] President Bouchard complained about Toronto's excessive conference trips to the mound. He called the Leafs "showspoilers" and then said, for the entire press room to hear, "They're a lot of punks, just like in hockey!"<ref>{{cite news|newspaper=The Globe and Mail|title=Royals Edge Leafs|date=1957-07-09|page=23}}</ref>

Bouchard was a tough opponent even outside of hockey. When the Mafia of the day in Montreal attempted to intimidate him into hiring their people for his restaurant, Bouchard invited the head man to Chez Butch Bouchard for dinner. Bouchard's wife, Marie-Claire, recalled he told them, "Il lui a dit over my dead body. Je n'embaucherai jamais un de tes hommes." which translates "Over my dead body, I will never hire one of your men."<ref>{{cite web|last=King|first=Ronald|publisher=Cyberpresse inc., A Thomson Reuters|title=Bienvenue chez Butch Bouchard...|url=http://www.cyberpresse.ca/opinions/chroniqueurs/ronald-king/200810/09/01-27909-bienvenue-chez-butch-bouchard.php|date=2008-10-08|language=French|accessdate=2009-12-14}}</ref>

A reporter once asked the canny Bouchard what he thought of coaching methods in the NHL. He replied, "Hockey should be more like football, with a coach for the defence, one for the offence and maybe one for the goalies."<ref>{{cite book|first=Jean|last=Beliveau|title=My Life in Hockey|page=155|year=2005|isbn=1-55365-149-9}}</ref> Indicative of his usual foresight it would be many years before such practices would become common in the NHL.<ref>{{cite web|url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SearchPlayer.jsp?player=13867|title=Mike Nykoluk was first NHL assistant coach in 1972|publisher=Hockey Hall of Fame|accessdate=2009-08-17}}</ref>

He died in 2012 at the age of 92.<ref>{{cite web|url=http://www.ctv.ca/CTVNews/Canada/20120414/former-nhler-montreal-canadiens-captain-emile-bouchard-dead-at-92-120414/ |title=Former Habs captain Emile Bouchard dead at 92 &#124; CTV News |publisher=Ctv.ca |date=2009-12-04 |accessdate=2012-04-14}}</ref>

==Honours and recognition==
Bouchard was one of nine players and one builder elected to the [[Hockey Hall of Fame]] in 1966.<ref name="hhof_bio"/> On October 15, 2008, the Montreal Canadiens celebrated their [[Montreal Canadiens centennial|100th season]] by unveiling the Ring of Honour, an exhibit along the wall of the upper deck of the [[Bell Centre]], paying tribute to their 44 players and 10 builders who are members of the Hockey Hall of Fame. Bouchard along with [[Elmer Lach]], the two oldest surviving members, were on hand to drop the ceremonial puck at centre ice.<ref>{{cite web|url=http://www.nhl.com/ice/news.htm?id=386866|title=Canadiens legends Bouchard, Lach drop puck at home opener of 100th season|author=Canadian Press|date=2008-10-16|publisher=NHL.com|accessdate=2010-09-23}}</ref>

In 2008, a grass roots movement had begun to pressure Canadien management to retire Bouchard's #3. During the [[Quebec general election, 2008|Quebec provincial election]] Independent candidate Kevin Côté made one of his platforms to force Canadiens into retiring the number.<ref>{{cite web|url=http://network.nationalpost.com/np/blogs/posted/archive/2008/11/18/habs-great-butch-bouchard-skates-into-election-campaign.aspx|title=National Post:Habs great Butch Bouchard skates into election campaign|publisher=The National Post|date=2008-11-18|accessdate=2009-08-16}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> By March 2009 it reached the [[National Assembly of Quebec|Quebec National Assembly]] where a motion was presented and carried "That the National Assembly support the steps taken and supported by the population of Québec in order that Montreal Canadiens management retire the sweater of Émile "Butch" Bouchard eminent defenceman from 1941 to 1956."<ref>{{cite web|url=http://www.assnat.qc.ca/en/travaux-parlementaires/assemblee-nationale/39-1/journal-debats/20090325/11013.html#_Toc225930098|title=National Assembly First Session 39th Legislature Votes and Proceedings|publisher=National Assembly of Quebec|date=2009-03-25|accessdate=2010-09-23}}</ref>

On December 4, 2009, as part of an 85 minute pre-game ceremony celebrating the Canadiens' 100th anniversary, Bouchard's No. 3 and Elmer Lach's No. 16 were retired. They become the 16th and 17th Canadien players to have their numbers retired.<ref>{{cite web|title=Habs retire jerseys of Hall of Famers Emile Bouchard and Elmer Lach|url=http://www.nhl.com/ice/news.htm?id=508708|publisher=National Hockey League|date=2009-12-04|accessdate=2009-12-04}}</ref><ref>{{cite news|last=Stubbs|first=Dave|title=montreal canadiens 1909-2009; Historic birthday bash|publisher=Montreal Gazette|date=2009-12-05|page=D1}}</ref>

On June 18, 2008, Bouchard received the [[National Order of Quebec|National Order of Quebec (L'Ordre national du Québec)]] presented to him by the Premier of Quebec [[Jean Charest]].<ref name="hhof_bio"/><ref>{{cite web|url=http://www.hhof.com/html/honews.shtml|title=Hockey Hall of Fame news: Emile Bouchard Receives Order of Quebec|publisher=Hockey Hall of Fame|accessdate=2009-08-16}}</ref><ref>{{cite web|url=http://www.ordre-national.gouv.qc.ca/membres/membre.asp?id=2404|title=Quebec government tribute to Bouchard for Order of Quebec|publisher=Ministère du Conseil exécutif Gouvernement du Québec|language=French|accessdate=2008-08-16}}</ref> On December 30th, 2009, he was made a Member of the [[Order of Canada]] "for his contributions to sports, particularly professional hockey, and for his commitment to his community".<ref>{{cite web|url=http://www.gg.ca/document.aspx?id=13469|title=Governor General Announces 57 New Appointments to the Order of Canada|work=Office of the Secretary to the Governor General|accessdate=2009-12-30|date=2009-12-30}}</ref>

==Awards and achievements==
* Member of the Order of Canada (2009)
*National Order of Quebec Chevalier (2008).
*Inducted into the [[Hockey Hall of Fame]] in 1966.
*[[Stanley Cup]] champion: [[1944 Stanley Cup Finals|1944]], [[1946 Stanley Cup Finals|1946]], [[1953 Stanley Cup Finals|1953]], [[1956 Stanley Cup Finals|1956]]
*[[NHL First All-Star Team]]: [[1944–45 NHL season|1945]], [[1945–46 NHL season|1946]], [[1946–47 NHL season|1947]]
*NHL Second All-Star Team: [[1943–44 NHL season|1944]]
*The [[Quebec Major Junior Hockey League|QMJHL]]'s Defenceman of the Year Trophy ([[Emile Bouchard Trophy]]) is named in his honour.

==Career statistics==

===Regular season and playoffs===
{| border="0" cellpadding="1" cellspacing="0"  style="width:75%; text-align:center;"
|- style="background:#e0e0e0;"
! colspan="3" style="background:#fff;"| &nbsp;
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="5" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="5" | [[Playoffs]]
|- style="background:#e0e0e0;"
! [[Season (sports)|Season]]
! Team
! League
! GP !! [[Goal (ice hockey)|G]] !! [[Assist (ice hockey)|A]] !! [[Point (ice hockey)|Pts]] !! [[Penalty (ice hockey)|PIM]]
! GP !! G !! A !! Pts !! PIM
|- 
|1937–38
| [[Verdun Maple Leafs (ice hockey)|Verdun Maple Leafs]]
|MCJHL
| 2 || 0 || 0 || 0 || 2
| 7 || 2 || 1 || 3 || 10
|- style="background:#f0f0f0;"
|1938–39
| Verdun Maple Leafs
| MCJHL
| 9 || 1 || 1 || 2 || 20
| 10 || 0 || 2 || 2 || 12
|-
| 1939–40
| Verdun Maple Leafs
| MCJHL
| — || — || — || — || —
| — || — || — || — || —
|- style="background:#f0f0f0;"
| 1940–41
| [[Montreal Jr. Canadiens]] 
| [[QSHL]]
| 31 || 2 || 8 || 10 || 60
| — || — || — || — || —
|-
| [[1940–41 AHL season|1940–41]]
| [[Providence Reds]]
| [[American Hockey League|AHL]]
| 12 || 3 || 1 || 4 || 8
| 3 || 0 || 1 || 1 || 8
|- style="background:#f0f0f0;"
| [[1941–42 NHL season|1941–42]]
| [[Montreal Canadiens]]
| [[National Hockey League|NHL]]
| 44 || 0 || 6 || 6 || 38
| 3 || 1 || 1 || 2 || 0
|-
| [[1942–43 NHL season|1942–43]]
| Montreal Canadiens
| NHL
| 45 || 2 || 16 || 18 || 47
| 5 || 0 || 1 || 1 || 4
|- style="background:#f0f0f0;"
| [[1943–44 NHL season|1943–44]]
| Montreal Canadiens
| NHL
| 39 || 5 || 14 || 19 || 52
| 9 || 1 || 3 || 4 || 4
|-
| [[1944–45 NHL season|1944–45]]
| Montreal Canadiens
| NHL
| 50 || 11 || 23 || 34 || 34
| 6 || 3 || 4 || 7 || 4
|- style="background:#f0f0f0;"
| [[1945–46 NHL season|1945–46]]
| Montreal Canadiens
| NHL
| 45 || 7 || 10 || 17 || 52
| 9 || 2 || 1 || 3 || 17
|-
| [[1946–47 NHL season|1946–47]]
| Montreal Canadiens
| NHL
| 60 || 5 || 7 || 12 || 60
| 11 || 0 || 3 || 3 || 21
|- style="background:#f0f0f0;"
| [[1947–48 NHL season|1947–48]]
| Montreal Canadiens
| NHL
| 60 || 4 || 6 || 10 || 78
| — || — || — || — || —
|-
| [[1948–49 NHL season|1948–49]]
| Montreal Canadiens
| NHL
| 27 || 3 || 3 || 6 || 42
| 7 || 0 || 0 || 0 || 6
|- style="background:#f0f0f0;"
| [[1949–50 NHL season|1949–50]]
| Montreal Canadiens
| NHL
|69 || 1 || 7 || 8 || 88
| 5 || 0 || 2 || 2 || 2
|-
| [[1950–51 NHL season|1950–51]]
| Montreal Canadiens
| NHL
| 52 || 3 || 10 || 13 || 80
| 11 || 1 || 1 || 2 || 2
|- style="background:#f0f0f0;"
| [[1951–52 NHL season|1951–52]]
| Montreal Canadiens
| NHL
| 60 || 3 || 9 || 12 || 45
| 11 || 0 || 2 || 2 || 14
|-
| [[1952–53 NHL season|1952–53]]
| Montreal Canadiens
| NHL
| 58 || 2 || 8 || 10 || 55
| 12 || 1 || 1 || 2 || 6
|- style="background:#f0f0f0;"
| [[1953–54 NHL season|1953–54]]
| Montreal Canadiens
| NHL
| 70 || 1 || 10 || 11 || 89
| 11 || 2 || 1 || 3 || 4
|-
| [[1954–55 NHL season|1954–55]]
| Montreal Canadiens
| NHL
| 70 || 2 || 15 || 17 || 81
| 12 || 0 || 1 || 1 || 37
|- style="background:#f0f0f0;"
| [[1955–56 NHL season|1955–56]]
| Montreal Canadiens
| NHL
| 36 || 0 || 0 || 0 || 22
| 1 || 0 || 0 || 0 || 0
|- style="background:#e0e0e0;"
! colspan="3" | NHL totals
! 785 !! 49 !! 145 !! 194 !! 863
! 113 !! 11 !! 21 !! 32 !! 123
|}
<small>Career statistics from ''Total Hockey''<ref name="Total Hockey"/></small>

==See also==
*[[list of NHL players who spent their entire career with one franchise]]

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://www.legendsofhockey.net/html/spot_oneononep196603.htm Hockey Hall of Fame profile of Butch Bouchard]
* {{fr icon}} [http://www.emilebutchbouchard.com Émile «Butch» Bouchard's website maintained by the family]
* {{hockeydb|492}}
* [http://www.hockey-reference.com/players/b/bouchbu01.html Pro and Minor stats and hockey-reference.com]
* [https://www.nytimes.com/2012/04/16/sports/hockey/butch-bouchard-imposing-pillar-of-montreal-canadiens-defense-dies-at-92.html NYTimes obituary]

{{s-start}}
{{s-bef | before=[[Bill Durnan]] }}
{{s-ttl | title=[[Montréal Canadiens Captains|Montreal Canadiens captain]] | years=[[1948–49 NHL season|1948]]–[[1955–56 NHL season|56]] }}
{{s-aft | after=[[Maurice Richard]] }}
{{end}}

{{Good Article}}

{{DEFAULTSORT:Bouchard, Emile}}
[[Category:1919 births]]
[[Category:2012 deaths]]
[[Category:Canadian ice hockey defencemen]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Sportspeople from Montreal]]
[[Category:Members of the Order of Canada]]
[[Category:Montreal Canadiens players]]
[[Category:Montreal Junior Canadiens players]]
[[Category:National Hockey League players with retired numbers]]
[[Category:Providence Reds players]]
[[Category:Stanley Cup champions]]
[[Category:Ice hockey people from Quebec]]
[[Category:Knights of the National Order of Quebec]]