{{Distinguish|Treatise of the Three Impostors}}
{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Three Impostors
| title_orig    = 
| translator    = 
| image         = Three imposters.jpg
| caption       = Cover of ''The Three Impostors''
| author        = [[Arthur Machen]]
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = 
| genre         = [[Horror novel]]
| publisher     = [[John Lane (publisher)|John Lane]]
| release_date  = 1895
| english_release_date =
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 215 pp
}}

'''''The Three Impostors''''' is an episodic novel by [[British literature|British]] [[horror fiction]] writer [[Arthur Machen]], first published in [[1895 in literature|1895]] in [[The Bodley Head]]'s Keynote Series. It was revived in paperback by [[Ballantine Books]] as the forty-eighth volume of the [[Ballantine Adult Fantasy series]] in June 1972.

==Controversy ==
Publisher [[John Lane (publisher)|John Lane]], wary of the atmosphere following [[Oscar Wilde#Trials|the trial of Oscar Wilde]], asked Machen to censor his manuscript. Barring the omission of one word, Machen refused to comply.<ref>"John Lane and Arthur Machen: A Correspondence" in ''Faunus: The Journal of the Friends of Arthur Machen'', volume 16 (Summer 2007)
</ref>

==Structure==
The novel incorporates several inset weird tales and culminates in a final denouement of deadly horror, connected with a secret society devoted to debauched pagan rites. The three impostors of the title are members of this society who weave a web of deception in the streets of London—relating the aforementioned weird tales in the process—as they search for a missing Roman coin commemorating an infamous [[orgy]] by the Emperor [[Tiberius]] and close in on their prey: "the young man with spectacles".<ref>Review
of ''The Three Imposters'', ''[[The Bookman (London)|The Bookman]]'', February 1896. Reprinted in [[Jason Colavito]], ''A Hideous Bit of Morbidity: An Anthology of Horror Criticism from the Enlightenment to World War I'' (Jefferson, NC: McFarland, 2008), pp. 227–28. ISBN 978-0-7864-3968-3</ref>

==Machen's later reflections on the novel==
Partly in response to criticism of the [[Robert Louis Stevenson|Stevensonian]] style of the book, Machen altered his approach in writing his next book, ''[[The Hill of Dreams]]''. Following the death of his first wife in 1899, Machen developed a greater interest in the occult, joining the [[Hermetic Order of the Golden Dawn]]. He noted that a number of events in his life seemed to mirror events in ''The Three Impostors'', most notably a conflict in the order between [[William Butler Yeats]] (a "young man with spectacles") and [[Aleister Crowley]], which reached its height around this time. (These experiences are reflected on in [[Alan Moore]]'s ''[[A Disease of Language|Snakes and Ladders]]''.)

In ''Things Near and Far'' (1923) Machen wrote:
<blockquote>
It was in the early spring of 1894 that I set about the writing of the said "Three Impostors," a book which testifies to the vast respect I entertained for the fantastic, "[[The New Arabian Nights|New Arabian Nights]]" manner of [[Robert Louis Stevenson|R. L. Stevenson]], to those curious researches in the byways of [[London]] which I have described already, and also, I hope, to a certain originality of experiment in the tale of terror.
</blockquote>

==Influence==
Two of the novel's inset tales, "The Novel of the Black Seal" and "The Novel of the White Powder", have been cited as major influences on the work of [[H. P. Lovecraft]]. In his survey ''[[Supernatural Horror in Literature]]'', Lovecraft suggested that these stories "perhaps represent the highwater mark of Machen's skill as a terror-weaver".<ref>H. P. Lovecraft, ''Supernatural Horror in Literature'' (Dover, 1973), p. 92.</ref> They have been frequently anthologised.

"The Novel of the Black Seal" has been cited as a model for some of Lovecraft's best-known stories: "[[The Call of Cthulhu]]",<ref>S. T. Joshi and David E. Schultz, "Call of Cthulhu, The", ''An H. P. Lovecraft Encyclopedia'', pp. 28–29.</ref> "[[The Dunwich Horror]]",<ref>Robert M. Price, ''The Dunwich Cycle'', pp. ix–x.</ref> and "[[The Whisperer in Darkness]]".<ref>Robert M. Price, ''The Hastur Cycle'', pp. xi–xiii.</ref> The story also bears strong resemblance to Lovecraft's story "[[The Lurking Fear]]", which tells of a deformed humanoid race living in a rural region of the Catskill Mountains.  "The Novel of the White Powder", which Lovecraft said "approaches the absolute culmination of loathsome fright",<ref>Lovecraft, p. 93.</ref> is pointed to as an inspiration for Lovecraft's stories of bodily disintegration, such as "[[Cool Air]]" and "[[The Colour Out of Space]]".

The story "R<sub>x</sub>... Death!" in ''Tales from the Crypt'' #20 is an adaptation of "The Novel of the White Powder," with the change made that the poisonous "medicine" contains digestive enzymes, rather than a witch's brew.

==References==
{{Reflist}}

==Bibliography==
*{{cite book |last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=188}}
*{{cite book |last=Machen |first=Arthur |title=The Three Impostors |year=2007 |publisher=Dover Publications |location=Mineola, New York |isbn=978-0-486-46052-9}}

==External links==
*[http://www.gutenberg.org/ebooks/35517 ''The Three Imposters'']; Project Gutenberg public domain text
*[http://manybooks.net/titles/machenarother090301561.html ''The Three Impostors'']; complete text
* {{librivox book | title=The Three Impostors | author=Arthur MACHEN}}

{{DEFAULTSORT:Three Impostors, The}}
[[Category:1895 novels]]
[[Category:British fantasy novels]]
[[Category:British horror novels]]
[[Category:The Bodley Head books]]
[[Category:Welsh horror fiction]]
[[Category:Anglo-Welsh novels]]
[[Category:Works by Arthur Machen]]
[[Category:19th-century British novels]]