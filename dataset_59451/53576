{{Accounting}}

'''IFRS 15''' is an [[International Financial Reporting Standard]] (IFRS) promulgated by the [[International Accounting Standards Board]] (IASB) providing guidance on accounting for [[revenue]] from contracts with customers.  It was adopted in 2014 and will become effective in 2018.<ref name=IFRS>{{cite web|title=IASB confirms deferral of effective date by issuing formal amendment to the revenue Standard|url=http://www.ifrs.org/Alerts/PressRelease/Pages/IASB-confirms-deferral-of-effective-date-by-issuing-formal-amendment-to-the-revenue-Standard.aspx|publisher=[[IFRS Foundation]]}}</ref><ref name=ey>{{cite web|title=IASB and FASB issue new revenue recognition standard — IFRS 15|url=http://www.ey.com/Publication/vwLUAssets/EY-Devel80-Revenue-May2014/$File/EY-Devel80-Revenue-May2014.pdf|publisher=[[Ernst & Young]]|accessdate=2014-09-01|date=May 2014}}</ref>  It was the subject of a joint project with the [[Financial Accounting Standards Board]] (FASB), which issues accounting guidance in the United States, and the guidance is substantially similar between the two boards.<ref name=ifrs15>{{cite web|title=IASB and FASB issue converged Standard on revenue recognition|date=28 May 2014|publisher=[[IASB]]|url=http://www.ifrs.org/Alerts/ProjectUpdate/Pages/IASB-and-FASB-issue-converged-Standard-on-revenue-recognition-May-2014.aspx|accessdate=2014-09-01}}</ref>

==History==
A main purpose of the project to develop IFRS 15 was that, although revenue is a critical metric for [[financial statement]] users, there were important differences between the IASB and FASB definitions of revenue, and there were different definitions of revenue even within each board's guidance for similar transactions accounting for under different standards.<ref name=ifrs15/>  The IASB also believed that their guidance for revenue was not detailed enough.<ref name=ifrs15/>

The IASB began working on its revenue project in 2002.<ref name=dt>{{cite web|title=IASB and FASB issue new, converged revenue standards|url=http://www.iasplus.com/en/news/2014/05/ifrs-15|publisher=[[Deloitte]]|accessdate=2014-09-01|date=28 May 2014}}</ref>  The boards released their first discussion paper describing their views on accounting for revenue in 2008, and they released exposure drafts of a proposed standard in 2010 and 2011.<ref name=dt/>  The final standard was issued on 28 May 2014.<ref name=ifrs15/>

==Revenue model==
The IFRS 15 revenue model has five steps:<ref name=ey/><ref name=dt/>
#Identify the contract with a customer
#Identify all the individual performance obligations within the contract
#Determine the transaction price
#Allocate the price to the performance obligations
#Recognize revenue as the performance obligations are fulfilled
Relative to previous accounting guidance, IFRS 15 may cause revenue to be recognized earlier in some cases, but later in others.<ref name="kpmg">{{cite web|title=First Impressions: Revenue from contracts with customers|url=http://www.kpmg.com/Global/en/IssuesAndInsights/ArticlesPublications/first-impressions/Documents/First-Impression-Revenue-2014.pdf|publisher=[[KPMG]]|date=June 2014|accessdate=2014-09-01}}</ref>

=== Identify the contract with a customer ===
According to IFRS 15, the following criteria have to be met before a contract can be identified; 
# both parties have to approve the contract and are committed to perform;<ref name=":0">{{Cite book|title=Introduction to IFRS|last=Koppeschaar|first=ZR|publisher=LexisNexis|year=2015|isbn=9780409118445|location=Durban|pages=290–311}}</ref> 
# and the entity can identify each party’s rights and obligations in terms of the contract; and<ref name=":0" />
# there are clear payment terms in the contract, and the contract has “commercial substance”.<ref name=":0" />

=== Identify all the individual performance obligations within the contract ===
A good or service that is to be delivered in terms of a contract with a customer qualifies as a performance obligation if the good or service is “distinct”. In this context a good or service is distinct if:
* The stipulated item can be consumed by the customer, either on its own, or in combination with other items that are regularly available to the customer; and<ref name=":1">{{Cite web|url=http://www.ey.com/Publication/vwLUAssets/Applying_IFRS:_A_closer_look_at_the_new_revenue_recognition_standard_(June_2014)/$FILE/Applying-Rev-June2014.pdf|title=A closer look at the new revenue recognition standard|last=Ernest and young|first=|date=1 June 2015|website=Ernst and young|publisher=Ernst and young|access-date=6 June 2016}}</ref>
* The promise to transfer goods or services to a customer can be separately identified from other transfers stipulated in the contract.<ref name=":1" />

=== Determine the transaction price ===
In most cases the transaction price to be paid will be stipulated in the contract and quite easy to calculate, however certain circumstances require that a transaction price should be estimated by other methods.

Firstly, an entity has to measure the amount of non-cash consideration in a contract in terms of [[IFRS 13|IFRS 13: fair value measurement]].

Secondly, a contract can have variable consideration (for example, the transaction price is subject to settlement discount should the client pay within a certain time frame). In this case, the transaction price can be calculated by two methods: 
* The most likely amount: the amount that of considerations that has the highest [[probability]] of realizing will be measured as the transaction price, or
* [[Expected value]] approach; in this case the weighed average of possible amounts will be measured as the transaction price.<ref name="dt" />
Both of the above-mentioned are estimates, and should the estimates change, the entity will apply the change retrospectively in terms of the criteria of [[IAS 8]].

Lastly IFRS 15 requires that the entity should test for the existence of a “significant financing component” in the contract, this will occur if: “the timing of payments agreed by the parties to the contract provides the customer or the entity with a significant benefit of financing the transfer of goods or services to the customer”<ref name=":0" />

If the above-mentioned is applicable, the transaction price will be adjusted to eliminate the effect of this benefit. This is simply done by calculating the [[net present value]] of the payments (if the satisfaction of performance obligations is prior to the payment date), or by calculating the net future value (if the payment date is prior to the satisfaction of performance obligations). The difference (between the amount recognized after adjustment for a significant financing component and amount of consideration to be received) is simply recognized as [[interest]] income/ expense in terms of the accrual basis of accounting as mentioned in IAS 1.<ref name="dt" />

=== Recognize revenue as the performance obligations are fulfilled ===
An entity can recognize revenue when performance obligations have been settled, a performance obligation has been settled when the customer has received all the benefits associated with the performance obligation, and is able to use and enjoy the asset to his or her own discretion.<ref name=":2">{{Cite book|title=Guide through IFRS|last=IFRS|first=|publisher=SAICA|year=2015|isbn=9780409121001|location=London|pages=763–824}}</ref>

==== Performance obligations settled over time ====
The performance obligations will be settled in the measure of progress towards completion, the measure of progress can be either based on the inputs (in the case of manufactured goods), or the output method.<ref name=":2" />

== Recognition of a contract asset ==
IFRS 15 requires an entity to recognize a contract [[asset]] if the term of the contract exceeds a year, this asset is determined by summing all the incremental costs of obtaining the contract. This asset must be amortized on a systematic basis that is consistent to the pattern of transfer of goods and services to the customer.<ref name=":2" />

== Effective date ==
IFRS 15, as amended, is effective for the first interim period within annual reporting periods beginning on or after January 1, 2018, with early adoption permitted. The FASB’s standard (ASC 606) is effective for public entities for the first interim period within annual reporting periods beginning after December 15, 2017 (nonpublic companies have an additional year).<ref>{{Cite web|url=https://www.pwc.com/us/en/cfodirect/publications/in-brief/fasb-one-year-deferral-new-revenue-standard-606.html|title=In brief: FASB finalizes one-year deferral of the new revenue standard|last=PricewaterhouseCoopers|website=PwC|access-date=2016-05-18}}</ref><ref>{{Cite web|url=https://www.pwc.com/us/en/cfodirect/publications/in-brief/principal-vs-agent-new-revenue-standard-606.html|title=In brief: IASB proposes changes to revenue standard - more FASB proposals coming soon|last=PricewaterhouseCoopers|website=PwC|access-date=2016-05-18}}</ref>

==References==
{{reflist}}

{{International Financial Reporting Standards}}

[[Category:International Financial Reporting Standards]]
[[Category:Revenue]]