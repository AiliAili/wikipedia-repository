<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, guidelines. -->
{|{{Infobox Aircraft Begin
 |name= B-66 Destroyer
 |image= File:B-66 Destroyer.jpg
 |caption=A Douglas B-66B (53-506) in flight
}}{{Infobox Aircraft Type
 |type=[[Light bomber]]
 |national origin= United States
 |manufacturer=[[Douglas Aircraft Company]]
 |first flight=28 June 1954
 |introduced=1956
 |retired=1973 (USAF)
 |status=
 |primary user=[[United States Air Force]]
 |more users= 
 |produced=
 |number built=294<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=323 "Douglas B-66 Destroyer."] {{webarchive |url=https://web.archive.org/web/20071116061652/http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=323 |date=November 16, 2007 }}  ''National Museum of the United States Air Force''. Retrieved: 5 August 2010.</ref>
 |unit cost=US$2.55 million (RB-66B)<ref name="knaack">Knaack, Marcelle Size. ''Post-World War II Bombers, 1945-1973''. Washington, DC: Office of Air Force History, 1988. ISBN 0-16-002260-6.</ref>
 |developed from = [[Douglas A-3 Skywarrior]]
 |variants with their own articles =
 |developed into = [[Northrop X-21]]
}}
|}

The '''Douglas B-66 Destroyer''' was a [[United States Air Force]] [[light bomber]] based on the [[U.S. Navy]]'s [[Douglas A-3 Skywarrior|A-3 Skywarrior]] carrier-based heavy attack aircraft. The B-66 was intended to replace the [[Douglas A-26 Invader]], and an '''RB-66''' [[photo-reconnaissance]] version was ordered simultaneously. The USAF B-66 retained the three-man crew from the US Navy A-3, but incorporated ejection seats that the US Navy variant lacked.

==Design and development==
At first, the [[United States Air Force]] intended the conversion to be an easy matter of removing the carrier-specific features, so no prototypes were ordered, just five pre-production '''RB-66A''' models (the reconnaissance mission being considered a high priority). The list of modifications grew, and before long, the supposedly easy conversion became what was substantially a new aircraft. Many of the changes were due to the USAF's requirement for low-level operations, while the Navy version had originally been designed and employed as a high-altitude nuclear strike bomber. Two major differences between the A-3 and the B-66 consisted in the types of jet engines used, and the emergency crew egress systems.  The A-3 had two [[Pratt & Whitney J57|J57]] turbojet engines, whereas the B-66 had two [[Allison J71]]s. The B-66 was equipped with ejection seats whereas the A-3 was not.

The first RB-66A pre-production aircraft flew in 1954, whereas the first production '''RB-66B''' aircraft flew in the beginning of 1955.

The basic B-66 design proved to be a versatile one, and was produced or modified into a variety of other versions, including the EB-66, RB-66, and the WB-66. Likewise, many variants of the A-3 Skywarrior were produced.

==Operational history==
[[File:Douglas RB-66B 54-506 19 TRS Scul BWD 18.05.57 edited-2.jpg|thumb|right|RB-66B of 19 Tactical Reconnaissance Squadron based at [[RAF Sculthorpe]] England in 1957]] 

Deliveries to the USAF began in 1956, with 145 of this model produced. RB-66s were used as the primary night photo-reconnaissance aircraft of the USAF during this time, many examples serving with tactical reconnaissance squadrons based in the [[United Kingdom]] and in [[West Germany]]. A total of 72 of the '''B-66B''' bomber version were built, 69 fewer than originally planned. A total of 13 B-66B aircraft later were modified into EB-66B [[electronic countermeasure]]s aircraft for the cold war with Russia, and were stationed at [[RAF Chelveston]] with the 42nd Tactical Reconnaissance Squadron who did the conversion in the early 1960s. They would rotate out of an alert pad in Spain during the time that the 42nd had them. These and the RB-66Cs that the 42nd had would eventually be sent to [[Vietnam War|Vietnam]]. Unlike the U.S. Navy's A-3 Skywarrior, which performed some bombing missions, the Destroyer was not used as a bomber in Vietnam.

The '''RB-66C''' was a specialized electronic reconnaissance and Electronic Counter Measure (ECM) aircraft with an expanded crew of seven, including the additional electronics warfare experts. A total of 36 of these aircraft were built with the additional crew members housed in what was the camera/bomb bay of other variants. RB-66C aircraft had distinctive wingtip pods and were used in the vicinity of [[Cuba]] during the [[Cuban Missile Crisis]] and later over Vietnam. In 1966, these planes were renamed into '''EB-66C'''.

Unarmed EB-66B, EB-66C and EB-66E aircraft flew numerous missions during the Vietnam War. They not only helped to gather electronic intelligence about the North Vietnamese defenses, but also provided protection for the daily bombing missions of the [[Republic F-105 Thunderchief|F-105s]] by jamming North Vietnamese radar systems. Early on, B-66s flew oval "racetrack" patterns over North Vietnam, but after one of the aircraft was shot down by a MiG, the vulnerable B-66 flights were ordered back, just outside North Vietnam.

On 10 March 1964, a [[19th Tactical Electronic Warfare Squadron|19th TRS]] RB-66C flying on a photo-reconnaissance mission from the [[Toul-Rosières Air Base]] in [[France]], was shot down over [[East Germany]] by a [[Soviet Union|Soviet]] [[Mikoyan-Gurevich MiG-21|MiG-21]] after it had crossed over the border due to a compass malfunction. The crew ejected and were taken prisoner briefly before being repatriated.

The final Douglas B-66 variant was the '''WB-66D''' [[weather reconnaissance]] aircraft, 36 of which were constructed.

The EB-66C/E was removed from USAF service by 1975 and most examples either scrapped in place or placed in storage for eventual scrapping.

==Variants==
;RB-66A
:(Douglas Model 1326) All-weather photo-reconnaissance variant, five built.
;RB-66B
:(Douglas Model 1329) Variant of the RB-66A with production J71-A-13 engines and higher gross weight, 149 built.
;B-66B
:(Douglas Model 1327A) Tactical bomber variant of the RB-66B, 72 built.
;NB-66B
:One B-66B used for testing and a RB-66B used for F-111 radar trials.
;RB-66C
:Electronic reconnaissance variant of the RB-66B, included an additional compartment for four equipment operators, 36 built.
;EB-66C
:Four RB-66Cs with uprated electronic counter measures equipment.
;WB-66D
:Electronic weather reconnaissance variant with the crew compartment modified for two observers, 36 built with two later modified to X-21A.
;EB-66E
:Specialized electronic reconnaissance conversion of the RB-66B.

===Northrop X-21===
[[File:Douglas EB-66E Destroyer in flight 061103-F-1234P-006.jpg|thumb|Douglas EB-66E Destroyer in flight. Aircraft of the 355th Tactical Fighter Wing, 41st or 42nd TEWS based at [[Takhli Royal Thai Air Force Base]] over [[Southeast Asia]] on 30 March 1970.]]
{{Main|Northrop X-21}}
The Northrop X-21 was a modified WB-66D with an experimental wing, designed to conduct laminar flow control studies. Laminar-flow control was thought to potentially reduce drag by as much as 25%. Control would be by removal of a small amount of the boundary-layer air by suction through porous materials, multiple narrow surface slots, or small perforations. Northrop began flight research in April 1963 at Edwards Air Force Base, but with all of the problems encountered, and money going into the war, the X-21 would be the last experiment involving this concept.<ref>[http://b66.info/B-66-EXPERIMENTS.htm "B-66 Information."] ''B66.info.'' Retrieved: 5 August 2010.</ref>

==Operators==
;{{USA}} 
* [[United States Air Force]]
**1st Tactical Reconnaissance Squadron (RB-66)
:::Spangdahlem Air Base, West Germany, 1957-59
:::RAF Alconbury, UK  1959-66

*9th Tactical Reconnaissance Squadron (RB/WB-66)
::Shaw AFB, South Carolina  1956-66

*12th Tactical Reconnaissance Squadron (RB-66)
::Yokota  Air Base, Japan  1956-60

*19th Tactical Reconnaissance Squadron (EB/RB-66)
::RAF Sculthorpe, UK  1956-59
::Spangdahlem Air Base, West Germany  1959
::RAF Bruntingthorpe, UK  1959-62
::Toul-Rosieres Air Base, France  1962-65
::Chambley-Bossieres Air Base, France  1965-66
::Shaw AFB, South Carolina  1966-67

*19th Tactical Electronic Warfare Squadron (EB/RB-66)
::Shaw AFB, South Carolina  1967-68
::Itazuke Air Base, Japan  1968-69
::Kadena Air Base, Japan  1969-70

*30th Tactical Reconnaissance Squadron (RB-66C)
::Sembach Air Base, West Germany  1957-58

* 39th Tactical Electronic  Warfare  Squadron  EB-66E/RB-66C  
::Spangdahlem AB, Germany  1969-72

*39th  Tactical Electronic Warfare Training Squadron (EB/RB-66)
::Shaw AFB, South Carolina  1969-74

*41st Tactical Reconnaissance Squadron (EB/RB-66)
::Shaw AFB, South Carolina  1956-59
::Takhli Air Base, Thailand  1965-67

*41st Tactical Electronic Warfare Squadron (EB/RB-66)
::Takhli Air Base, Thailand  1967-69

*42d Tactical Reconnaissance Squadron (B/EB/RB/WB-66)
::Spangdahlem Air Base, West Germany  1956-59
::RAF Chelveston, UK  1959-62
::Toul-Rosieres Air Base, France  1962-63
::Chambley-Bussieres Air Base, France  1963-66

*42d Tactical Electronic Warfare Squadron (EB/RB-66)
::Takhli Air Base, Thailand  1968-70
::Korat  Air Base, Thailand  1970-74

*43d Tactical Reconnaissance Squadron (RB-66)
::Shaw AFB, South Carolina  1956-59

*4411th Combat Crew Training Group (B/EB/RB/WB-66)
::Shaw AFB, South Carolina  1959-66

*4416th Test Squadron (EB/RB-66)
::Shaw AFB, South Carolina  1963-70

*4417th Combat Crew Training Squadron (EB/RB-66)
::Shaw AFB, South Carolina  1966-69

==Aircraft on display==
[[File:RB-66B.jpg|thumb|Douglas RB-66B Destroyer]]

;RB-66B
*53-0466 - Dyess Linear Air Park, [[Dyess AFB]], [[Texas]].<ref>[http://aerialvisuals.ca/LocationDossier.php?Serial=3696 "B-66 Destroyer/53-0466."] ''aerialvisuals.ca'' Retrieved: 4 June 2015.</ref>
*53-0475 - [[National Museum of the United States Air Force]] at [[Wright-Patterson AFB]], [[Ohio]].<ref>[http://www.nationalmuseum.af.mil/Visit/MuseumExhibits/FactSheets/Display/tabid/509/Article/196047/douglas-rb-66b-destroyer.aspx "Douglas RB-66B Destroyer."] ''National Museum of the US Air Force.'' Retrieved: 24 August 2015.</ref>

;RB-66C
*54-0465 - [[Shaw AFB]], [[South Carolina]].<ref>[http://aerialvisuals.ca/LocationDossier.php?Serial=3624 "B-66 Destroyer/54-0465."] ''aerialvisuals.ca'' Retrieved: 4 June 2015.</ref>

;JRB-66D
*53-0412 - [[Chanute Aerospace Museum]] at the former [[Chanute AFB]], [[Illinois]].<ref>[http://www.aeromuseum.org/index.php/douglas-rb-66b-destroyer "B-66 Destroyer/53-0412"] {{webarchive |url=https://web.archive.org/web/20130507000843/http://www.aeromuseum.org/index.php/douglas-rb-66b-destroyer |date=May 7, 2013 }} ''Chanute Aerospace Museum.'' Retrieved: 2 May 2013.</ref>

;WB-66D
*55-0390 - USAF History and Traditions Museum at [[Lackland AFB]], [[Texas]].<ref>[http://aerialvisuals.ca/LocationDossier.php?Serial=3880 "B-66 Destroyer/55-0390."] ''aerialvisuals.ca'' Retrieved: 4 June 2015.</ref>
*55-0392 - [[Museum of Aviation (Warner Robins)|Museum of Aviation]], [[Robins AFB]], [[Georgia (U.S. state)|Georgia]].<ref>[http://www.museumofaviation.org/WB66.php "B-66 Destroyer/55-0392."] {{webarchive |url=https://web.archive.org/web/20120519130107/http://www.museumofaviation.org/WB66.php |date=May 19, 2012 }} ''Museum of Aviation.'' Retrieved: 21 May 2012.</ref>
*55-0395 - [[Pima Air and Space Museum]], adjacent to [[Davis-Monthan AFB]] in [[Tucson, Arizona|Tucson]], [[Arizona]].<ref>[http://www.pimaair.org/visit/aircraft-by-name/item/douglas-wb-66d-destroyer "B-66 Destroyer/55-0395."] ''Pima Air & Space Museum.'' Retrieved: 4 June 2015.</ref>
==Specifications (B-66)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet

<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with an asterisk "*" -->
|crew=3 (Pilot, Navigator and EWO)
|length main=75 ft 2 in
|length alt= 22.9 m
|span main=72 ft 6 in
|span alt=22.1 m
|height main=23 ft 7 in
|height alt=7.2 m
|area main=780 ft²
|area alt=72.5 m²
|empty weight main=42,540 lb
|empty weight alt=19,300 kg
|loaded weight main=57,800 lb
|loaded weight alt=26,200 kg
|max takeoff weight main=83,000 lb
|max takeoff weight alt=38,000 kg
|engine (jet)=[[Allison J71]]-A-11 or -13
|type of jet=[[turbojet]]s
|number of jets=2
|thrust main=10,200 [[pound-force|lbf]]
|thrust alt=45 kN
|max speed main=631 mph
|max speed alt=548 [[knot (unit)|kn]], 1,020 km/h
|combat radius main=900 mi
|combat radius alt=780 [[nautical mile|nmi]], 1,500 km
|ferry range main=2,470 mi
|ferry range alt=2,150 nmi, 3,970 km
|ceiling main=39,400 ft
|ceiling alt=12,000 m
|climb rate main=5,000 ft/min
|climb rate alt=25 m/s
|loading main=74.1 lb/ft²
|loading alt=361.4 kg/m²
|thrust/weight=0.35
|avionics=
* APS-27 and K-5 radars
|guns=2 [[20 mm caliber|20 mm]] [[Hispano-Suiza HS.404#United States|M24 cannons]] in radar-controlled/remotely operated tail turret
|bombs=15000 lb (6,803.9 kg)
}}

==Notable appearances in media==

The shooting down of an EB-66 over North Vietnam and the subsequent [[Rescue of Bat 21 Bravo|rescue of one of its crew]] became the subject for the book ''Bat*21'' by [[William Charles Anderson]], and later a [[Bat*21|film version]] (1988) starring [[Gene Hackman]] and [[Danny Glover]].

==See also==
{{Portal|United States Air Force}}
{{Aircontent
|related=
* [[Douglas A-3 Skywarrior]]
* [[Northrop X-21]]
|similar aircraft=
* [[Ilyushin Il-28]]
* [[Yakovlev Yak-28]]
|lists=
* [[List of bomber aircraft]]
* [[List of military aircraft of the United States]]
|see also=
}}

==References==
===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Baugher, Joe. [http://www.joebaugher.com/usaf_bombers/b66.html "Douglas B-66 Destroyer."] ''USAAC/USAAF/USAF Bomber Aircraft: Third Series of USAAC/USAAF/USAF Bombers'', 2001. Retrieved: 27 July 2006.
* Donald, David and Jon Lake, eds. ''Encyclopedia of World Military Aircraft''. London: AIRtime Publishing, 1996. ISBN 1-880588-24-2.
* [https://web.archive.org/web/20040228092458/http://www.wpafb.af.mil:80/museum/modern_flight/mf31.htm "Douglas RB-66B 'Destroyer'."] ''National Museum of the United States Air Force.'' Retrieved: 27 July 2006.
* Winchester, Jim, ed. "Douglas A-3 Skywarrior." ''Military Aircraft of the Cold War'' (The Aviation Factfile). London: Grange Books plc, 2006. ISBN 1-84013-929-3.
{{Refend}}

==External links==
{{Commons category|Douglas B-66}}
* [http://b66.info/ B-66 "Destroyer" Website History of US Tactical Warfare, website by former B-66 crews]

{{Douglas aircraft}}
{{USAF bomber aircraft}}
{{US reconnaissance aircraft}}

[[Category:Douglas aircraft|B-66 Destroyer]]
[[Category:United States bomber aircraft 1950–1959]]