{{Good article}}
{{Infobox University Boat Race
| name= 110th Boat Race
| winner = Cambridge
| margin = 6 and 1/2 lengths
| winning_time= 19 minutes 18 seconds
| date= {{Start date|1964|3|28|df=y}}
| umpire = [[Kenneth Payne|K. M. Payne]]<br>(Cambridge)
| prevseason= [[The Boat Race 1963|1963]]
| nextseason= [[The Boat Race 1965|1965]]
| overall =61&ndash;48
| women_winner =Cambridge
}}
The 110th [[The Boat Race|Boat Race]] took place on 28 March 1964.  Held annually, the event is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  The Oxford crew was the heaviest in Boat Race history.  The race was won by Cambridge by six-and-half lengths.  Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 24 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 24 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities, followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=24 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 24 August 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1963|previous year's race]] by five lengths, while Cambridge led overall in the event with 60 victories to Oxford's 48 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 12 October 2014}}</ref><ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 24 August 2014}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  This year's women's race was the first to be held since 1952.

Writing in the ''[[Financial Times]]'', [[Joseph Mallalieu]] noted that the Boat Race was subsidised by [[The Varsity Match]] every year.<ref>{{Cite news | title = Two losers on the Tideway | first = Joseph | last= Mallalieu| authorlink=Joseph Mallalieu | date = 28 March 1964 | page = 9 | issue = 23,273}}</ref>  Despite Oxford being "firm favourites" upon their arrival at the Tideway, Cambridge put in better performances in training,<ref name=oxlike/> and by the time of the race were considered the favourites themselves.<ref>{{Cite news | title = Cambridge need an early lead in Boat Race | work = [[The Times]] | issue = 55971 | date = 28 March 1964 | page = 3}}</ref>  The main race was umpired for the eighth and final time by the former Olympian [[Kenneth Payne]] who had rowed for Cambridge in the [[The Boat Race 1932|1932]] and [[The Boat Race 1934|1934 races]].<ref>Burnell, pp. 49, 74</ref><ref>{{Cite web | url = http://www.sports-reference.com/olympics/athletes/pa/kenneth-payne-1.html | publisher = [[Sports Reference]] | title = Kenneth Payne Bio, Stats, and Results| accessdate = 10 October 2014}}</ref>

==Crews==
Although it was the heaviest Cambridge crew ever, they weighed an average of 13&nbsp;[[Stone (unit)|st]] 4.75&nbsp;[[Pound (mass)|lb]] (84.5&nbsp;kg), almost {{convert|3|lb|kg|1}} per rower less than Oxford, who were the heaviest crew in Boat Race history.<ref name=oxlike>{{Cite news | title = Oxford are likely to win | work = [[The Guardian]] | date = 28 March 1964 | page = 10}}</ref>  Oxford saw two former Blues return in Miles Morland and Duncan Spencer,<ref>Dodd, p. 169</ref> while Cambridge's crew included four Boat Race veterans in Donald Legget, Mike Bevan, [[John Lecky]] and Christopher Davey.  Lecky was a Canadian international rower who had won a silver medal in the [[Rowing at the 1960 Summer Olympics|men's eight]] at the [[1960 Summer Olympics]].<ref>{{Cite web | url = http://www.sports-reference.com/olympics/athletes/le/john-lecky-1.html| title = John Lecky Bio, Stats, and Results| accessdate = 22 October 2014 | publisher = [[Sports Reference]]}}</ref>  Six of Oxford's crew came from [[Keble College, Oxford|Keble College]], five of those schooled at [[Eton College]].<ref>Dodd, p. 340</ref>

{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || J. Leigh-Wood || [[Keble College, Oxford|Keble]] || 12 st 3 lb || D. F. Legget || [[Trinity Hall, Cambridge|Trinity Hall]]  || 12 st 10 lb
|-
| 2 || D. W. Steel || [[Keble College, Oxford|Keble]] || 13 st 7 lb || M. V. Bevan || [[Downing College, Cambridge|Downing]] || 13 st 2 lb
|-
| 3 || D. W. A. Cox || [[St Peter's College, Oxford|St Peter's]] || 13 st 7 lb || M. Muir-Smith || [[Christ's College, Cambridge|Christ's]]  || 14 st 0 lb
|-
| 4 || M. Q. Morland  || [[Lincoln College, Oxford|Lincoln]] || 14 st 6 lb || J. W. Fraser || [[Jesus College, Cambridge|Jesus]] || 14 st 2 lb
|-
| 5 || R. C. T. Mead || [[Keble College, Oxford|Keble]] || 14 st 0 lb || [[John Lecky|J. M. S. Lecky]] || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 14 st 3 lb
|-
| 6 || D. D. S. Skailes (P)|| [[Keble College, Oxford|Keble]] || 14 st 3 lb || J. R. Kiely || [[Trinity College, Cambridge|1st & 3rd Trinity]]  || 14 st 0 lb
|-
| 7 || D. G. Bray || [[Keble College, Oxford|Keble]] || 13 st 1 lb || A. Simpson || [[Queens' College, Cambridge|Queens']]  || 12 st 13 lb
|-
| [[Stroke (rowing)|Stroke]] || D. C. Spencer  || [[Christ Church, Oxford|Christ Church]] || 13 st 5 lb  || C. J. T. Davey (P) || [[Jesus College, Cambridge|Jesus]] || 11 st 8 lb
|-
| [[Coxswain (rowing)|Cox]] || M. J. Leigh || [[Keble College, Oxford|Keble]] || 8 st 12 lb || R. G. Stanbury ||  [[Lady Margaret Boat Club]] || 8 st 10 lb
|-
!colspan="7"|Source:<ref name=burn80/><br>(P) &ndash; Boat club president<ref>Burnell, pp. 51&ndash;52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Oxford.<ref name=burn80>Burnell, p. 80</ref>  With a "calm, following wind",<ref>{{Cite news | title = Cambridge virtue rewarded | work = [[The Guardian]] | date = 30 March 1964 | page =10}}</ref> the race commenced at 2.20pm,<ref name=burn80/> and within a minute, Cambridge had a quarter-length lead before Oxford closed the gap at [[Craven Cottage]].  The Light Blues reached the Mile Post three seconds ahead and crossed over to the midstream.  By [[Harrods Furniture Depository]] they had extended their lead out to two lengths and passed below [[Hammersmith Bridge]] a further half-length ahead.  Despite a surge from Oxford, Cambridge were sixteen seconds ahead at Chiswick Steps and increased the gap to twenty seconds by [[Barnes Railway Bridge|Barnes Bridge]].  Cambridge won by six-and-a-half lengths in a time of 19 minutes 18 seconds, 23 seconds ahead of Oxford.<ref name=burn80/><ref name=canter>{{Cite news | title = Cambridge win 110th Boat Race in a canter| first = Ian | last = Thomson | date = 29 March 1964 | work = [[The Guardian]]}}</ref>

The victory was Cambridge's 61st in the contest, taking the overall score to 61&ndash;48.  The Cambridge boat club president and stroke Christopher Davey said: "Everything went as planned, but Oxford hung on more than I would have liked at the end.  I would have liked to have taken it to 10 lengths if possible but Oxford kept going splendidly at the end."<ref name=canter/>

In the 20th running of the [[Henley Boat Races#Women's Boat Race|Women's Boat Race]], Cambridge triumphed, their third consecutive victory.<ref>{{Cite web | archiveurl = https://web.archive.org/web/20140407065044/http://theboatrace.org/women/results | url = http://theboatrace.org/women/results | title = Results &ndash; Women | publisher = The Boat Race Company Limited | accessdate = 19 November 2014 | archivedate = 7 April 2014}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink=Dickie Burnell| year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1964}}
[[Category:1964 in English sport]]
[[Category:1964 in rowing]]
[[Category:The Boat Race]]
[[Category:March 1964 sports events]]