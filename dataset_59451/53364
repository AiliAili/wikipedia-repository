{{about|the company|the U.S. Supreme Court case|Arthur Andersen LLP v. United States}}
{{refimprove|date=January 2016}}
{{use mdy dates|date=July 2016}}
{{infobox company 
| name  = Arthur Andersen
| logo  = [[File:Arthur Andersen logo.jpg|290px|Arthur Andersen]]
| type  = [[Limited liability partnership]]
| foundation    = 1913
| location      = [[Chicago]], [[Illinois]], United States
| num_employees = approx. 200 as of 2007 <br> 85,000 (in 2002)
| industry      = [[Accounting]] <br> [[Professional services]] <br> [[Tax]] <br> [[Consultant|Consulting]] <br> Licenses of Certified Public Accountants surrendered in 2002
| products      = [[Professional services]]
| revenue       = [[United States dollar|US$]]9.3 billion  (in 2002)
| website    = {{URL|http://www.andersen.com/}}
}}

'''Arthur Andersen LLP''', based in [[Chicago]], is an American holding company and formerly one of the [[Big Four auditors|"Big Five" accounting firms]] among [[PricewaterhouseCoopers]], [[Deloitte Touche Tohmatsu]], [[Ernst & Young]] and [[KPMG]], providing [[auditing]], [[tax]], and [[Consultant|consulting]] services to large [[corporation]]s.  In 2002, the firm voluntarily surrendered its licenses to practice as [[Certified Public Accountant]]s in the United States after being [[Enron scandal|found guilty of criminal charges]] relating to the firm's handling of the [[audit]]ing of [[Enron]], an energy corporation based in [[Texas]], which had [[Enron bankruptcy|filed]] for [[bankruptcy]] in 2001.<ref>{{Cite news| url =https://www.wsj.com/news/articles/SB1023409436545200|title= Arthur Andersen's Fall From Grace Is a Sad Tale of Greed and Miscues|author= Ken Brown and Ianthe Jeanne Dugan|publisher=Wall Street Journal |date = June 7, 2002}}</ref>

One of the few revenue-generating assets that the Andersen firm still has is Q Center, a conference and training facility outside of Chicago.<ref>{{cite web | author=Ameet Sachdev | title=Conference center last resort for Andersen | url=http://www.chicagotribune.com/business/chi-0305220361may22,0,7603044.story | work=Chicago Tribune | date=May 22, 2003 | accessdate=2010-06-16| archiveurl= https://web.archive.org/web/20100608203716/http://www.chicagotribune.com/business/chi-0305220361may22,0,7603044.story| archivedate= June 8, 2010 <!--DASHBot-->| deadurl= no}}</ref>

The former consultancy and outsourcing arm of the firm, now known as [[Accenture]], which had separated from the accountancy side in 1987 and renamed themselves after splitting from [[Andersen Worldwide]] in 2000, continues to operate and had become one of the largest multinational corporations in the world until 2002.

==History==

===Founding===
{{main|Arthur E. Andersen}}
[[File:ArthurAndersen.jpeg|thumb|right|[[Arthur E. Andersen|Arthur Andersen]] (1885-1947)<br />In 1913, Arthur Andersen and Clarence Delany, both from [[Price Waterhouse]], bought out The Audit Company of California to form Andersen, Delany & Co., which became Arthur Andersen & Co. in 1918.]]
[[File:Andersen revenue.svg|thumb|400px|Revenue per year in million U.S. dollars.<br />Source: corporate press releases]]
Born 30 May 1885 in [[Plano, Illinois]], and orphaned at the age of 16, Andersen began working as a mail boy by day and attended school at night, eventually being hired as the assistant to the [[comptroller]] of [[Allis-Chalmers]] in Chicago.  In 1908, at age 23, he became the youngest CPA in Illinois.

The firm of Arthur Andersen was founded in 1913 by Arthur Andersen and Clarence DeLany as Andersen, DeLany & Co.<ref>{{cite journal | first=Mary Virginia | last=Moore | author2=Crampton, John | title=Arthur Andersen: Challenging the Status Quo| format=PDF | journal=The Journal of Business Leadership | publisher=American National Business Hall of Fame | url=http://www.anbhf.org/pdf/moore_crampton.pdf | volume=11 | issue=3 | pages=71–89 | year=2000 | accessdate=2008-05-05}}</ref> The firm changed its name to Arthur Andersen & Co. in 1918. Arthur Andersen's first client was the [[Joseph Schlitz Brewing Company]] of [[Milwaukee]].<ref>{{cite book |last= Squires|first=Susan |date=2003 |title=Inside Arthur Andersen: Shifting Values, Unexpected Consequences |url=https://books.google.com/books?id=YZOWTE4VQeAC&pg=PA28&lpg=PA28&dq=Arthur+Andersen+Schlitz+Brewing+Company&source=bl&ots=YHo1DA5K51&sig=qfvwfIk6i1PXAizdQRAJ6X61szQ&hl=en&sa=X&ei=Ol28U-2PFoiVyASf7oGoCg&ved=0CDQQ6AEwAw#v=onepage&q=Arthur%20Andersen%20Schlitz%20Brewing%20Company&f=false |location= |publisher=[[FT Press]] |page=28 |isbn=9780131408968 |accessdate=July 8, 2014 }}</ref> In 1915, due to his many contacts there, the Milwaukee office was opened as the firm's second office. In 1908, after attending courses at night while working full-time, he graduated  from the [[Kellogg School of Management|Kellogg School]] at [[Northwestern University]] with a bachelor's degree in business.<ref>[http://www.anbhf.org/pdf/moore_crampton.pdf ''Arthur Anderson: Challenging the Status Quo'' (Moore, Mary Virginia and John Crampton)]</ref>

Andersen had an unwavering faith in education as the basis upon which the new profession of [[accounting]] should be developed. He created the profession's first centralized training program and believed in training during normal working hours. He was generous in his commitment to aiding educational, civic and charitable organizations. In 1927, he was elected to the Board of Trustees of Northwestern University and served as its president from 1930 to 1932. He was also chairman of the board of certified public accountant examiners of Illinois.

===Reputation===
Andersen, who headed the firm until his death in 1947, was a zealous supporter of high standards in the [[accounting]] industry. A stickler for honesty, he argued that accountants' responsibility was to investors, not their clients' management. During the early years, it is reputed that Andersen was approached by an executive from a local rail utility to sign off on accounts containing flawed accounting, or else face the loss of a major client. Andersen refused in no uncertain terms, replying that there was "not enough money in the city of Chicago" to make him do it. For many years, Andersen's motto was "Think straight, talk straight."

Arthur Andersen also led the way in a number of areas of accounting standards. Being among the first to identify a possible sub-prime bust, Arthur Andersen dissociated itself from a number of clients in the 1970s. Later, with the emergence of [[stock options]] as a form of compensation, Arthur Andersen was the first of the major accountancy firms to propose to the [[FASB]] that stock options should be included on [[expense]] reports, thus impacting on net profit just as cash compensation would.

By the 1980s, standards throughout the industry fell as accountancy firms struggled to balance their commitment to audit independence against the desire to grow their burgeoning consultancy practices. Having established a reputation for IT consultancy in the 1980s, Arthur Andersen was no exception. The firm rapidly expanded its consultancy practice to the point where the bulk of its revenues were derived from such engagements, while audit partners were continually encouraged to seek out opportunities for consulting fees from existing audit clients. By the late-1990s, Arthur Andersen had succeeded in tripling the per-share revenues of its partners.

Predictably, Arthur Andersen struggled to balance the need to maintain its faithfulness to accounting standards with its clients' desire to maximize profits, particularly in the era of quarterly earnings reports. Arthur Andersen has been alleged to have been involved in the fraudulent accounting and auditing of [[Sunbeam Products]], [[Waste Management, Inc]], [[Asia Pulp & Paper]],<ref>{{Cite news| url = https://www.wsj.com/news/articles/SB998243223377564102|title= APP and Arthur Andersen Face Class-Action Lawsuits|author= Sara Webb|publisher=Wall Street Journal |date =  August 20, 2001}}</ref> and the [[Baptist Foundation of Arizona]], [[WorldCom]], as well as the infamous [[Enron]] case, among others.<ref>{{Cite news| url = http://www.washingtonpost.com/wp-dyn/content/article/2006/09/30/AR2006093000928.html|title=Executives Sentenced in Church Fraud|author= Terry Greene Sterling|publisher=The Washington Post |date = October 1, 2006}}</ref><ref>{{Cite news| url = http://www.forbes.com/2002/06/27/0627topnews.html |title=WorldCom: Too Easy, Too Late|author= Dan Ackman|publisher=[[Forbes]] |date = June 27, 2002}}</ref>

Two of the last three Comptrollers General of the US General Accounting Office (now the Government Accountability Office) were top executives of Arthur Andersen.<ref>{{cite web|url=http://www.gao.gov/multimedia/video/gao_s_90th_anniversary|title=U.S. GAO - Video Gallery|publisher=|accessdate=July 2, 2015}}</ref>

===Andersen Consulting and Accenture===
The consulting wing of the firm became increasingly important during the 1970s and 1980s, growing at a much faster rate than the more established accounting, auditing, and tax practice.  This disproportionate growth, and the consulting division partners' belief that they were not garnering their fair share of firm profits, created increasing friction between the two divisions.

In 1989, Arthur Andersen and [[Andersen Consulting]] became separate units of [[Andersen Worldwide Société Coopérative]].  Arthur Andersen increased its use of accounting services as a springboard to sign up clients for Andersen Consulting's more lucrative business.

The two businesses spent most of the 1990s in a bitter dispute.  Andersen Consulting saw a huge surge in profits during the decade.  The consultants, however, continued to resent transfer payments they were required to make to Arthur Andersen.  In August 2000, at the conclusion of  [[International Chamber of Commerce]] arbitration of the dispute, the arbitrators granted Andersen Consulting its independence from Arthur Andersen, but awarded [[United States dollar|US$]]1.2 billion in past payments (held in [[escrow]] pending the ruling) to Arthur Andersen, and declared that Andersen Consulting could no longer use the Andersen name.  As a result, Andersen Consulting changed its name to [[Accenture]] on [[New Year's Day]] 2001 and Arthur Andersen meanwhile now having the right to the Andersen Consulting name rebranded itself as "Andersen".

Four hours after the arbitrator made his ruling, Arthur Andersen CEO [[Jim Wadia]] suddenly resigned.  Industry analysts and business school professors alike viewed the event as a complete victory for Andersen Consulting.<ref>{{cite web|author=Mitchell Martin |title=Arbitrator's Ruling Goes Against Accounting Arm: Consultants Win Battle Of Andersen |url=http://www.iht.com/articles/2000/08/08/consult.2.t.php |work=International Herald Tribune |date=August 8, 2000 |accessdate=2008-05-05 |archiveurl=https://web.archive.org/web/20080308142059/http://www.iht.com/articles/2000/08/08/consult.2.t.php |archivedate=March 8, 2008 |deadurl=no |df=mdy }}</ref> Jim Wadia would provide insight on his resignation years later at a Harvard Business school case activity about the split. It turned out that the Arthur Andersen board passed a resolution saying he had to resign if he didn't get at least an incremental US$4 billion (either through negotiation or via the arbitrator decision) for the consulting practice to split off, hence his quick resignation once the decision was announced.<ref>{{Cite news| url = http://www.telegraph.co.uk/finance/4461169/Andersen-chief-quits-as-14bn-claim-fails.html |title= Andersen chief quits as $14bn claim fails|author= Philip Aldrick|publisher=The Daily Telegraph |date = August 8, 2000}}</ref>
  
Accounts vary on why the split occurred &mdash; executives on both sides of the split cite greed and arrogance on the part of the other side.  The executives on the Andersen Consulting side maintained breach of contract when Arthur Andersen created a second consulting group, AABC (Arthur Andersen Business Consulting) which competed directly with Andersen Consulting in the marketplace. AABC grew quickly, most notably its healthcare and technology practices.  Many of the AABC firms were bought out by other consulting companies in 2002, most notably, [[Deloitte]] (especially in Europe), [[Hitachi Consulting]], PwC Consulting, which was later acquired by IBM, and KPMG Consulting, which later changed its name to [[BearingPoint]].

===Enron scandal===
{{Main|Enron scandal}}
Following the [[Enron scandal|2001 scandal]] in which [[energy company|energy giant]] [[Enron]] was found to have reported $100bn in revenue through institutional and systematic [[accounting scandals|accounting fraud]], Andersen's performance and alleged complicity as an auditor came under intense scrutiny. The Powers Committee (appointed by Enron's board to look into the firm's accounting in October 2001) came to the following assessment: "The evidence available to us suggests that Andersen did not fulfill its professional responsibilities in connection with its audits of Enron's financial statements, or its obligation to bring to the attention of Enron's Board (or the Audit and Compliance Committee) concerns about Enron's internal contracts over the related-party transactions".<ref>Andrew Cornford: ''Internationally Agreed Principles For Corporate Governance And The Enron Case'', [[United Nations Conference on Trade and Development]], G-24 Discussion Paper Series No. 30, [[New York City|New York]], June 2004, p. 30, [http://www.unctad.org/en/docs/gdsmdpbg2420046_en.pdf unctad.org]</ref>

On June 15, 2002, Andersen was convicted of [[obstruction of justice]] for shredding documents related to its audit of [[Enron]], resulting in the [[Enron scandal]]. Although the conviction was [[Arthur Andersen LLP v. United States|later reversed]] by the [[Supreme Court of the United States|Supreme Court]], the impact of the scandal combined with the findings of criminal complicity ultimately destroyed the firm. [[Nancy Temple]] (Andersen Legal Dept.) and [[David Duncan (accountant)|David Duncan]] (Lead Partner for the Enron account) were cited as the responsible managers in this scandal as they had given the order to shred relevant documents.

Since the [[U.S. Securities and Exchange Commission]] cannot accept audits from convicted felons, the firm agreed to surrender its CPA licenses and its right to practice before the SEC on August 31, 2002—effectively putting the firm out of business.  It had already started winding down its American operations after the indictment, and many of its accountants joined other firms.  The firm sold most of its American operations to [[KPMG]], [[Deloitte & Touche]], [[Ernst & Young]] and [[Grant Thornton LLP]].  The damage to Andersen's reputation also destroyed the viability of the firm's international practices. Most of them were taken over by the local firms of the other major international accounting firms.

The Andersen indictment also put a spotlight on its faulty audits of other companies, most notably [[Waste Management, Inc|Waste Management]], [[Sunbeam Products|Sunbeam]], the [[Baptist Foundation of Arizona]] and [[WorldCom]].   The subsequent [[bankruptcy]] of WorldCom, which quickly surpassed Enron as the then biggest bankruptcy in history (and has since been passed by the bankruptcies of [[Bankruptcy of Lehman Brothers|Lehman Brothers]] and [[Washington Mutual#Bankruptcy|WaMu]] in the [[2008 financial crisis]]) led to a [[domino effect]] of accounting and corporate scandals.

On May 31, 2005, in the case ''[[Arthur Andersen LLP v. United States]]'', the [[Supreme Court of the United States]] unanimously reversed Andersen's conviction due to what it saw as serious flaws in the [[jury instructions]].<ref>{{cite court |litigants=Arthur Andersen LLP v. United States |vol=544  |reporter=U.S. |opinion=696  |pinpoint= |court= |date=2005 |url=http://www.law.cornell.edu/supct/html/04-368.ZS.html|quote=}}</ref>  In the court's view, the instructions were far too vague to allow a jury to find obstruction of justice had really occurred.  The court found that the instructions were worded in such a way that Andersen could have been convicted without any proof that the firm knew it had broken the law or that there had been a link to any official proceeding that prohibited the destruction of documents.  The opinion, written by Chief Justice [[William Rehnquist]], was also highly skeptical of the government's concept of "corrupt persuasion"—persuading someone to engage in an act with an improper purpose even without knowing an act is unlawful.

===Demise===
Since  the ruling [[Vacated judgment|vacated]] Andersen's felony conviction, it theoretically left Andersen free to resume operations.  The damage to the Andersen name was so severe, however, that it has not returned as a viable business even on a limited scale.  There are over 100 civil suits pending against the firm related to its audits of Enron and other companies.  Even before voluntarily surrendering its right to practice before the SEC, it had many of its state licences revoked. A new verb, "Enron-ed", was coined by John M. Cunningham, the former Arthur Andersen Director in the Seattle Office, to describe the demise of Arthur Andersen.

From a high of 28,000 employees in the US and 85,000 worldwide, the firm is now down to around 200, based primarily in Chicago.  Most of their attention is on handling the lawsuits and presiding over the orderly dissolution of the company.

{{As of|2011}}, Arthur Andersen LLP has not been formally dissolved nor has it declared bankruptcy.  Ownership of the partnership has been ceded to four limited liability corporations named Omega Management I through IV. {{As of|2011}}, Arthur Andersen LLP still operates the Q Center conference center in [[St. Charles, Illinois]].  The Q center is currently used for training, primarily for internal Accenture personnel, and other large scale companies.{{citation needed|date=July 2015}}

===Migration of partners and local offices to new firms===
Many partners formed new companies or were acquired by other consulting firms.  Examples include:

* [[Accuracy (company)|Accuracy]] which was founded in 2004 by a team of seven former partners and is headquartered in Paris.
*[[Andersen Tax LLC]] which acquired the rights and changed their name from WTAS in 2014. <ref>Revive Arthur Andersen Name|url = https://www.wsj.com/articles/tax-firm-to-revive-arthur-andersen-name-1409626508|newspaper = Wall Street Journal|access-date = 2015-12-15|issn = 0099-9660|first = Michael|last = Rapoport}}</ref>
* [[Huron Consulting Group]]
* [[West Monroe Partners]] which was founded in 2002 by four former consultants, and is based in Chicago.
* [[KPMG]] which absorbed the Computer Forensics division based in Cypress, CA and the Boise, Kansas City, Philadelphia, Portland, Salt Lake City and Seattle offices, among others
* [[Navigant Consulting]] which absorbed eleven partners in Chicago and Washington D.C.
* [[Perot Systems]] which absorbed six partners in the East
* [[Protiviti]] hired approximately 800 former workers
* [[SMART Business Advisory and Consulting]] which absorbed some of the Philadelphia office
* jcba Limited which was founded by a partner from the aviation practice<ref>{{cite web|url=http://www.adame.ae/about/|title=ADA Millennium  " About|publisher=|accessdate=July 2, 2015}}</ref><ref>{{cite web|url=http://jcba.co.uk/the-jcba-team/|title=The jcba team " jcba - jcba|publisher=|accessdate=July 2, 2015}}</ref>
* [[Grant Thornton International]] which absorbed the North Carolina and South Carolina offices.
* True Partners Consulting
* Portions of Andersen Audit practice merged with Deloitte and Ernst & Young.

==See also==
*[[Andersen Tax LLC]]
* [[Accenture]]
* [[Accounting scandals]]
* ''[[Conspiracy of Fools]]''
* [[Corporate abuse]]
* [[David J. Lesar]]
* [[Timeline of the Enron scandal]]

==References==
{{reflist}}

==External links==
* [https://web.archive.org/web/20111217003751/http://www.andersen.com/ U.S. official website] 
* [http://news.findlaw.com/hdocs/docs/enron/usandersen030702ind.html Indictment of U.S. v. Arthur Andersen, LLP.]
* [http://caselaw.lp.findlaw.com/scripts/getcase.pl?court=US&vol=000&invol=04-368&friend=usatoday#opinion1 Supreme Court Overturns Conviction in Arthur Andersen LLP, Petitioner v United States]
* [http://www.salon.com/tech/feature/2002/02/07/arthur_andersen/ Arthur Andersen and the Baptists] (Salon.com)
* [https://web.archive.org/web/20110728083332/http://www.amazon.com/Legacy-Arthur-Andersen-Excellence-ebook/dp/B005CI650Y The Legacy of Arthur Andersen] (Book)
* [http://www.amazon.com/Inside-Arthur-Andersen-Unexpected-Consequences/dp/0131408968/ref=pd_sim_b_14 Inside Arthur Andersen: Shifting Values, Unexpected Consequences] (Book)

{{Consulting}}

[[Category:Accounting scandals]]
[[Category:Companies established in 1913]]
[[Category:Corporate crime]]
[[Category:Defunct financial services companies of the United States]]
[[Category:Enron]]
[[Category:Defunct companies based in Chicago]]
[[Category:Defunct accounting firms of the United States]]
[[Category:1913 establishments in Illinois]]