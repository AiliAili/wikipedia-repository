{{Infobox journal
| title = East European Politics and Societies
| cover = [[File:East European Politics and Societies.tif]]
| editor = Wendy Bracewell, Krzysztof Jasiewicz 
| discipline = [[International relations]]
| former_names = 
| abbreviation = East Eur. Polit. Soc.
| publisher = [[SAGE Publications]]
| country =
| frequency = Quarterly
| history = 1986-present
| openaccess = 
| license = 
| impact = 0.225  
| impact-year = 2013
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201655
| link1 = http://eep.sagepub.com/content/current
| link1-name = Online access
| link2 = http://eep.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 13538372
| LCCN = 87649452 
| CODEN = 
| ISSN = 0888-3254 
| eISSN = 1533-8371 
}}
'''''East European Politics and Societies''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[political science]], especially concerning [[international relations]] of Eastern Europe. The journal's [[editors-in-chief]] are Wendy Bracewell (University College London) and Krzysztof Jasiewicz (Washington and Lee University). It was established in 1986 and is currently published by [[SAGE Publications]] in association with the [[American Council of Learned Societies]] and the [[American Association for the Advancement of Slavic Studies]].

== Abstracting and indexing ==
''East European Politics and Societies'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.225, ranking it 132 out of 157 journals in the category "Political Science" and 46 out of 64 journals in the category "Area Studies." <ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Political Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://eep.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:International relations journals]]
[[Category:European studies journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1986]]