{{Italic title}}
{{Infobox Journal
| title = Expert Opinion on Investigational Drugs
| cover =
| discipline = [[Pharmacology]]
| abbreviation = Expert Opin. Investig. Drugs
| publisher = [[Informa]]
| country = [[United Kingdom]]
| history = 1992-present
| frequency = Monthly
| impact = 5.528
| impact-year = 2014
| website = http://informahealthcare.com/loi/eid
| ISSN = 1354-3784
| eISSN = 1744-7658
| CODEN = EOIDER
| OCLC = 30379431
}}
'''''Expert Opinion on Investigational Drugs''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] covering developments in [[pharmaceutical sciences|pharmaceutical research]], from animal studies through to early clinical investigation. The journal's scope includes [[therapy|therapeutics]] in many areas: [[Lung|pulmonary]]-[[allergy]], [[dermatology]], [[Human gastrointestinal tract|gastrointestinal]], [[arthritis]], [[Infection|infectious disorders]], [[Endocrine system|endocrine]] and [[Metabolism|metabolic]], [[Central nervous system|central]] and [[peripheral]] [[nervous system]], [[Circulatory system|cardiovascular]] and [[Kidney|renal]], and [[oncology]].

The journal is published by [[Informa]] and the Editor-in-Chief is Giuseppe Tonini, Director of Medical Oncology, University Campus Bio-Medico, (Italy). ''Expert Opinion on Investigational Drugs'' was established in 1992 and according to the [[Journal Citation Reports]] has a 2014 [[impact factor]] of 5.528. It is also indexed in [[MEDLINE]]. The journal is available online and in paper format.

== Article categories ==
The journal publishes articles in the following categories:
* [[Review journal|Reviews]]
* Original Research Papers
* Drug Evaluations
* Meeting Highlights
* [[Editorial]]s

[[Category:Pharmacology journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1992]]
[[Category:English-language journals]]
[[Category:Expert Opinion journals]]


{{pharma-journal-stub}}