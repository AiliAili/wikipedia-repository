{{About|the fighter pilot|the singer|Makoto Ogawa}}
{{Infobox military person
|name=Makoto Ogawa
|birth_date= February 1917
|death_date=
|birth_place=[[Shizuoka Prefecture]], Japan
|death_place=
|image=Flying_ace_Makoto_Ogawa,_1945.jpg
|caption=Makoto Ogawa stands next to his fighter aircraft marked with two stylized eagles, indicating his first two aerial victories
|nickname=
|allegiance={{army|Empire of Japan}}
|serviceyears=1935–45
|rank=[[Army ranks of the Japanese Empire during World War II#Officer ranks|Second Lieutenant]]
|commands=
|battles=[[Second Sino-Japanese War]]<br />[[Pacific War]]
|awards=
}}

{{nihongo|'''Makoto Ogawa'''|小川 誠|Ogawa Makoto|born February  1917}} is a retired Japanese army aviator known for achieving [[flying ace]] status against [[Boeing B-29 Superfortress]]es during [[World War II]]. In carrying out his duties, he downed the highest number of B-29s among the pilots in his air group—seven confirmed—and also two [[North American P-51 Mustang]]s.<ref name=Sakaida2001/> He was awarded the ''[[Bukosho]]'', the highest award given by the [[Imperial Japanese Army]] to living soldiers who demonstrated exceptionally valorous action in combat.

==Career==
Ogawa was born in 1917 in [[Shizuoka Prefecture]].<ref name=Sakaida2001>{{cite book |url=https://books.google.com/books?id=4p1pDSXzZZUC&pg=PA125 |pages=125–126 |first1=Henry |last1=Sakaida |first2=Kōji |last2=Takaki |title=B-29 Hunters of the JAAF |publisher=Osprey Publishing |year=2001 |isbn=1841761613}}</ref><ref name=Hata2012>{{cite book |first1=Ikuhiko |last1=Hata |first2=Yashuho |last2=Izawa |first3=Christopher |last3=Shores |title=Japanese Army Fighter Aces: 1931–45 |page=237 |publisher=Stackpole Books |year=2012 |isbn=0811710769}}</ref> He enlisted in the army when he was 18 and was assigned to the 7th Air Regiment based at [[Hamamatsu Air Base|Hamamatsu Airfield]] located north of the city of [[Hamamatsu]] in his home prefecture. After a few years, he enrolled in the Kumagaya Army Flying School to learn to fly [[Fighter aircraft|fighters]]. In August 1938 he graduated as a member of the 72nd class of students. Instead of being posted to a combat squadron in China, he was kept at the school as an assistant instructor.<ref name=Sakaida2001/>

Toward the end of 1941, Ogawa was sent to Manchuria to fly with the 70th Sentai, a fighter wing outfitted with the [[Nakajima Ki-44]] ''Shōki'', called "Tojo" by the [[Allies of World War II|Allies]]. He flew for three years and gained a high level of skill in piloting fighters.<ref name=Aces>{{cite book |url=https://books.google.com/books?id=i1nOLCXhh98C&pg=PA79 |pages=79–80 |first=Henry |last=Sakaida |title=Japanese Army Air Force Aces 1937–45 |publisher=Osprey Publishing |year=1997 |isbn=1855325292 |volume=13 |series=Aircraft of the Aces}}</ref> By then, American heavy bombers had begun to bomb Japan itself, so to counter the attacks the 70th Sentai was transferred, in November 1944, to [[Kashiwa, Chiba]], northeast of Tokyo. For this task, the fighter pilots were given newer 20&nbsp;mm [[Ho-5 cannon]]-equipped [[Nakajima Ki-84]] machines, called "Frank" by the Allies.<ref name=Aces/>

Ogawa found that the B-29s were more vulnerable when they were maintaining level flight in their bombing runs and could not employ evasive maneuvers. Exploiting this weakness during night actions, he shot two of the bombers down by frontal attack, firing at the nose. He continued with his aerial successes and by August 1945 when the war ended, he had built up a confirmed score of seven B-29 bombers downed, as well as two P-51 Mustang fighters. This made him the highest scoring pilot against B-29s in the 70th Sentai, his air group.<ref name=Sakaida2001/> By the order of General [[Shizuichi Tanaka]], on 9 July 1945 Ogawa was awarded the ''[[Bukosho]]'', the highest military honor given to living IJA personnel during [[World War II]]. At the same time he was commissioned as an officer with the rank of second lieutenant.<ref name=Aces/>

After the war Ogawa became a businessman.<!--doing what?--> He lives in Tokyo.<ref name=Aces/>

==References==
{{reflist}}

==External links==
* [http://www.hasegawausa.com/product-pages/hsgs8880.html 1/32 scale Nakajima Ki44-II Hei Shoki (Tojo) Fighter] with Ogawa's markings


{{DEFAULTSORT:Ogawa, Makoto}}
[[Category:1917 births]]
[[Category:Japanese World War II flying aces]]
[[Category:Japanese Army officers]]
[[Category:Military personnel from Shizuoka Prefecture]]
[[Category:Living people]]