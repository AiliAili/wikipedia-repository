{{Use Australian English|date=September 2014}}
{{Use dmy dates|date=October 2015}}
{{Infobox Australian place
| type                  = other
| name                  = Ferndale Park
| state                 = nsw
| iucn_category         = 
| image                 = Swaines Creek Cave & Angophora.JPG
| caption               = "Old Man's Cave" and Swaines Creek
| image_alt             = 45
| latd                  = 33
| latm                  = 47
| lats                  = 58.6
| longd                 = 151
| longm                 = 10
| longs                 = 09.89
| relief                = 1
| map_alt               = 
| nearest_town_or_city  = Sydney
| area                  = 
| area_footnotes        = 
| established           = 
| established_footnotes = 
| visitation_num        = 
| visitation_year       = 
| visitation_footnotes  = 
| managing_authorities  = City of Willoughby
| url                   = http://www.willoughby.nsw.gov.au/Environment---Sustainability/nature/bushland/Bushwalks/Tracks/
}}
'''Ferndale Park''' is located in suburban {{NSWcity|Chatswood West}}, {{convert|8|km|mi|0|abbr=on}} from the centre of [[Sydney]], [[Australia]]. It is an important<ref>A detailed fauna study completed in 2001 by Dr Arthur White of Biosphere Consultancy</ref><ref name=pom>{{cite web|title=LANE COVE RIVER CATCHMENT MANAGEMENT COMMITTEE Bushland Inventory|url=http://ns1.mattheij.com/RainForest/vines/4371/FINALREP.htm|date=November 1998|accessdate=9 October 2015}}</ref> urban forest reserve, preserving [[Eucalyptus|Blackbutt]] forest and the mossy gully [[rainforest]].<ref name="Willoughby City, Bushwalking Tracks">{{cite web|title=Sustainability/nature/bushland/Bushwalks/Tracks|url=http://www.willoughby.nsw.gov.au/Environment---Sustainability/nature/bushland/Bushwalks/Tracks/|work=Bushwalking Tracks|publisher=City of Willoughby|accessdate=10 October 2015}}</ref> Most of this original forest was cleared for agriculture and housing in the 19th and 20th century.

== Geography ==

Average annual rainfall is {{convert|1200|mm|in|0|abbr=on}} at the nearby Chatswood Bowling Club.<ref>http://www.bom.gov.au/ Chatswood Bowling Club Weather Station</ref> Soils are moderately fertile, based on [[Hawkesbury sandstone]] and [[Wianamatta shale|Ashfield Shale]].<ref>[[NSW Government]] Geological Series Sheet 9130 Edition 1, 1983</ref> Most of the reserve is considered part of the [[Sydney Sandstone Gully Forest]].

== History ==
The local [[Indigenous Australians|indigenous Australian people]], the [[Cammeraygal]] occupied this area for at least 5,800 years.<ref>Fact Sheet 8 – Chatswood West – [[City of Willoughby|Willoughby City Council]]</ref> They were known to shelter in "Old Man's Cave" in heatwaves or heavy rain.<ref>[[City of Willoughby]] information sign</ref>

== Recreation ==
Ferndale Park has a series of walking tracks. Office workers from [[Chatswood]] may be seen [[jogging]] through the reserve at lunchtime.<ref>Wild Walks – Ferndale Park [http://www.wildwalks.com/bushwalking-and-hiking-in-nsw/lane-cove-national-park/ferndale-loop.html Wild Walks – Ferndale Park]</ref>

== Flora ==
Noteworthy indigenous flora includes the [[Eucalyptus pilularis|blackbutt]],<ref name="Willoughby City, Bushwalking Tracks"/> [[Trochocarpa laurina|tree heath]], [[celery wood]], [[coachwood]], [[Schizomeria ovata|native crabapple]] and [[hard corkwood]]. An impressive number of fern species grow here,<ref name="Willoughby City, Bushwalking Tracks"/><ref>{{cite web|url=https://www.flickr.com/photos/17674930@N07/15676855083/in/photolist-sz3QSa-raav5u-qJVM5E-qZAorx-qxxieQ-pTiYQT-qQ5VSr-pGLw3p-oMDwug-p4TcSV-p1ouyC-oxN6gs-o4XMSc-o4Rob3-nLaw4f-jYjsU9-jE5rMW-jDHti5-jE37zS-7HX5fb-5Ebnbq|title=Birds Nest Fern|publisher=Flickr|accessdate=10 October 2015}}</ref> including [[Pteris umbrosa|jungle brake]], [[Blechnum nudum|fishbone water fern]], [[Hymenophyllum cupressiforme|filmy fern]], [[Deparia petersenii subsp. congrua|Japanese lady fern]], [[Arthropteris tenella|delicate rock fern]] and the [[Asplenium flabellifolium|necklace fern]]. In early winter, waxcap mushroom species in the [[Hygrophoraceae]] may be seen.<ref>[[North Shore Times]], 26 June 2015 page 23</ref>

== Fauna ==
[[Common ringtail possum|Ring-tail possums]], [[brushtail possum]]s and [[grey-headed flying fox]]es are common. Birds such as [[rainbow lorikeet]]s, [[Australian king parrot]]s, [[crimson rosella]]s, [[currawong]]s, [[Pacific koel|koel]], [[tawny frogmouth]], [[pacific baza]]<ref>Wildlife Willougby Facebook Page (Willoughby City Council) 12 September 2015</ref> and [[powerful owl]]<ref>Wildlife Willougby Facebook Page (Willoughby City Council) 5 October 2015</ref> are some of the many found in the park. [[Microbat]]s are present, including [[Gould's wattled bat]], [[Lesser long-eared bat]] and the [[Little Forest Bat]].<ref name="Willoughby City, Bushwalking Tracks"/> Many species of spider and other invertebrates live in the reserve. The [[Litoria peronii|Emerald Spotted Frog]] is known to occur.<ref>[[North Shore Times]], 17 July 2015 page 21</ref> An important remnant [[marsupial]] is the [[sugar glider]].<ref name=pamphlet>Ferndale Walking Track pamphlet, City of Willoughby</ref>

== Bush regeneration ==
Forest conservation work is in progress. The weeds [[Tradescantia fluminensis|trad]] and [[Ligustrum lucidum|privet]] being particularly troublesome. The shady areas form excellent habitats for fungi and rainforest plants, but disturbed areas are suited to invasive weeds.<ref name=pamphlet />

== References ==
{{Commons category|Ferndale Park}}
{{reflist}}

[[Category:Parks in New South Wales]]
[[Category:Forests of New South Wales]]
[[Category:Geography of Sydney]]