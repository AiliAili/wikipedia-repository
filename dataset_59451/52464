{{Infobox magazine
|publisher=[[National Urban League]]
|frequency=Monthly
|founder         = 
|founded         = 1923
|firstdate       = <!-- {{Start date|year|month|day}} -->
|finaldate       = 1949
|finalnumber     = 
|company         = 
|based           = 
|country=[[United States]]
|language=English
}}
'''''Opportunity: A Journal of Negro Life''''' was an [[academic journal]] published by the [[National Urban League]] (NUL). The journal acted as a sociological forum for the emerging topic of [[African-American studies]] and was known for fostering the literary culture during the [[Harlem Renaissance]]. It was published monthly from 1923 to 1942 and then quarterly through 1949.

== History ==
The studies published in the early issues of ''Opportunity'' were conducted and funded by NUL and clearly supported the social mission of an academic journal connected with the missions of NUL and [[Fisk University]]. Topics centered on the social challenges faced by black people at the time, including access to employment, housing, sanitation and education. The journal's motto "Opportunity not Alms" describes the editorial direction as does the journal's manifesto: "Opportunity is a venture inspired by a long insistent demand, both general and specific, for a journal of Negro life that would devote itself religiously to an interpretation of the social problems of the Negro population.... The policy of Opportunity will be definitely constructive. It will aim to present, objectively, facts of Negro life. It hopes, thru an analysis of these social questions, to provide a basis of understanding; encourage interracial co-operation in the working out of these problems."<ref name="JohnsonCharles">Johnson, Charles, ed. ''Opportunity the Journal of Negro Life'', January 1923: N. pag.</ref>

While the journal was published from 1923 to 1949, its main influence on African-American literature was from 1923 to 1928. The immediate objective of ''Opportunity'' was to publish dependable data concerning black life and race relations. [[Editor-in-chief]] [[Charles S. Johnson]] wrote in the first issue of ''Opportunity'', "Accurate and dependable facts can correct inaccurate and slanderous assertions that have gone unchallenged… and what is most important, to inculcate a disposition to see enough of interest and beauty of their own lives to rid themselves of the inferior feeling of being Negro".<ref name="JohnsonCharles" />

Central to ''Opportunity''<nowiki>'</nowiki>s founding were two patrons: Mrs. Ruth Standish Baldwin, the white widow of a railroad magnate and George Edmund Haynes, a graduate of Fisk University, [[Yale University]], and [[Columbia University]], who would become the NUL's first executive secretary. The interracial character of the League's board was set from its first days and also became the template for Charles Johnson's approach to fostering interest, support, and occasion for African-American art and artists. Critics of the journal as well as the Harlem Renaissance thought that Johnson's literary content may have been pandering to his white audience and patrons. [[Wallace Thurman]] stated "The results of the Renaissance have been sad rather than satisfactory, in that critical standards have been ignored and the measure of achievement has been racial rather than literary" <ref>Ikonne, Chidi. "Opportunity and Black Literature." ''Phylon''. 40.1 (1979): 86-93. Print.</ref>

Under Charles Johnson's editorship the journal's circulation rose to 11,000 in 1928.<ref>N. W. Ayer And Son's American Newspaper Annual And Directory. Library of Congress, n.d. 29 Feb. 2012. N. W. Ayer & Son's Directory of Newspapers and Periodicals.</ref> While not as widely read as ''[[The Crisis]]'' or ''[[The Messenger (magazine)|The Messenger]]'',{{Citation needed|date=May 2012}} the journal was instrumental in providing breaks for new artists through its literary contests and literary parties.{{Citation needed|date=May 2012}} As the first editor-in-chief of ''Opportunity'', Charles Johnson immediately broadened the scope of the journal from a purely sociological journal to a multi-faceted publication that included African-American arts. Photographic essays, artworks, and poetry found a place beside research studies. Powerful photojournalism illustrated the quality of life for working blacks across America.

== Literary contests ==
Johnson is credited for organizing and promoting literary parties, which successfully brought together African-American artists and white patrons of money and letters. From 1924 to 1927, Johnson sponsored three literary contests. [[Eric Walrond]], a regular contributor to the journal, introduced Johnson to Harlem's notorious gambling kingpin, [[Casper Holstein]]. Holstein became a major patron of ''Opportunity'' and the journal's literary contests, the first of which received 732 entries. The literary contest became essential to the promotion of the Harlem Renaissance's writers and artists. The May 1925 issue of ''Opportunity'' lists a number of prizewinners who went on to enjoy successful publishing careers: [[Claude McKay]], [[Zora Neal Hurston]], [[Langston Hughes]], [[Countee Cullen]], [[Sterling Brown]], and [[Franklin Frazier]]. From 1925 to 1927 Johnson provided over three contest award dinners where on average almost 350 black artists and white patrons and publishers attended. According to [[Arna Bontemps]], contributor to ''Opportunity'', these events provided enthusiasm for African-American artists, increased white patronage, and provided exposure to the major [[New York City]] publishers ([[Alfred A. Knopf|Knopf]], [[Macmillan Publishers (United States)|MacMillan]], and [[Harper's Magazine|Harpers]]).<ref>Stroman, Carolyn A. "Charles S(purgeon) Johnson." ''American Magazine Journalists, 1900-1960: First Series 91'' (1990): n.pag. Literature Resource Center. January 24, 2012. </ref>

After 1928, when Johnson accepted the presidency of Fisk University, chief editors of the journal included Elmer Anderson Carter (October 1928 - January 1945), Madeline L. Aldridge (January 1945 - June 1947), and Dutton Ferguson (July 1947 - January 1949). Under Carter's editorship the journal resumed its focus for publishing sociological studies of African Americans and continued with this purpose until it ceased publication in 1949.

==Further reading==
* Gardiner, George L, ''A Bibliography of Charles Spurgeon Johnsons Published Writings'', Nashville, Tenn. Fisk University, 1970, Print.
* Johnson, Charles. "The Rise of the Negro Magazine." ''Journal of Negro History''. 13.1 (1928): 7-21. Print.
* Gilpin, Patrick, "Charles S. Johnson: An Intellectual Biography," PhD dissertation, Vanderbilt University Press, 1973. Print
* Witalec, Janet. ''The Harlem Renaissance: A Gale Critical Companion''. Gale Group, 2002. 1500. Print.

==References==
<references />

[[Category:Defunct journals]]
[[Category:Publications established in 1923]]
[[Category:Publications disestablished in 1949]]
[[Category:Sociology journals]]