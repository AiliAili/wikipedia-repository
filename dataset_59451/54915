{{Infobox Mayor
|image =
|imagesize =
| name=Garri Aiba <br> გარი აიბა
| order=[[Mayor of Sukhumi]]
| term_start=1995
| term_end=2000
| president=
| predecessor=
| successor=Leonid Lolua
| birth_date=
| birth_place=
| death_date=June 9, 2004
| death_place=
| party=
| religion=
| spouse=
}}
'''Garri Aiba''' ({{lang-ka|''გარი აიბა''}}; died June 9, 2004) was an opposition leader in  [[Abkhazia|Republic of Abkhazia]] at the time of his murder. He died when his car came under fire on June 9, 2004.<ref>{{cite web
|url=http://www.freedomhouse.org/modules/mod_call_dsp_country-fiw.cfm?country=6879
|title= Abkhazia [Georgia] (2005)
|publisher=
|accessdate=2007-01-02
}}</ref>

==History==
Garri Aiba was a veteran of the [[War in Abkhazia (1992–1993)|1992-1993 war with Georgia]], in which he led Abkhazia anti-aircraft defenses, and a prominent businessman. From 1995 to 2000 he had been mayor of [[Sukhum]], capital of Abkhazia.
Aiba had become one of the leaders of [[Amtsakhara]], which was one of the main movements in opposition to the government of President [[Vladislav Ardzinba]], two other leaders of which had also been killed in previous years.

==Murder==
On June 9, 2004, Aiba's car came under fire, fifty metres from his home in [[Sukhumi]]. He got out of the car in an attempt to protect his ten-year-old daughter, but was seriously injured, and died later that day in hospital. His death sent shockwaves through Abkhaz politics. Aiba had no business links and many people claimed that the killing was politically motivated. It sparked the resignations of several prominent ministers, including foreign minister (and fellow Amtsakhara leader) [[Sergey Shamba]], interior minister [[Abessalom Beiya]] and [[Givi Agrba]], head of the security services.<ref>{{cite web
|url=http://iwpr.net/?p=crs&s=f&o=160449&apc_state=henicrs2004
|title=Abkhazia Struggle Intensifies
|publisher=
|accessdate=2007-01-02
}}</ref> However, Shamba later denied that his resignation was related to Aiba's death{{Citation needed|date=July 2008}}, and Prime Minister [[Raul Khadjimba]] refused to accept the resignations of Beiya and Agrba.{{Citation needed|date=July 2008}}

No one has yet been charged with Aiba's murder. Former opposition leader [[Sergei Bagapsh]] has since become president.

==Notes==
{{Reflist}}

==External links==
*[http://www.freedomhouse.org/modules/mod_call_dsp_country-fiw.cfm?country=6879  Abkhazia &#91;Georgia&#93; (2005)]{{Dead link|date=July 2011}}
*[http://iwpr.net/?p=crs&s=f&o=160449&apc_state=henicrs2004 Abkhazia Struggle Intensifies]

{{S-start}}
{{succession box | before= | title=[[Mayor of Sukhumi]] | years=1995&ndash;2000 | after=Leonid Lolua}}
{{S-end}}

{{DEFAULTSORT:Aiba, Garri}}
[[Category:Year of birth missing]]
[[Category:2004 deaths]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:Assassinated activists]]
[[Category:People murdered in Georgia (country)]]
[[Category:Assassinated Abkhazian politicians]]
[[Category:Mayors of Sukhumi]]
[[Category:1st convocation of the Sukhumi City Council]]

{{Abkhazia-bio-stub}}
{{Georgia-mayor-stub}}