'''Abraham Jacob Briloff''' (July 19, 1917 - Dec. 12, 2013) was an American accounting scholar and Professor of accounting at [[Baruch College]] in New York. He was known for his contributions to accountancy,<ref>[https://www.nytimes.com/2013/12/16/business/abraham-briloff-accounting-professor-dies-at-96.html?_r=0 Abraham Briloff, Accounting Professor, Dies at 96], The New York Times, December 15, 2013</ref><ref name="AHoF 2015">[http://fisher.osu.edu/departments/accounting-and-mis/the-accounting-hall-of-fame/membership-in-hall/abraham-jacob-briloff/ Abraham Jacob Briloff], The Accounting Hall of Fame. Accessed 2015-02-14.</ref> and also as the "most prominent accounting critic."<ref>Floyd Norris. "[https://economix.blogs.nytimes.com/2013/12/13/abe-briloff-an-accountant-who-saw-through-the-games/?_r=0 Abe Briloff, an Accountant Who Saw Through the Games]," ''economix.blogs.nytimes.com,'' Dec. 13, 2013. Accessed 2015-02-14.</ref>

== Biography ==
Born in Manhattan to Benjamin and Anna Briloff, Briloff obtained his BA in commerce at the City College School of Business and Civic Administration, now [[Baruch College]], in 1937. Later in 1965 he obtained his PhD in accountancy and taxation at the [[New York University]] with the thesis, entitled "The Effectiveness of Accounting Communication."

Briloff began his career in the final days of the [[Great Depression]] as high school teacher, teaching bookkeeping and stenography, and as assistant accountant in the accountancy firm of Apfel & Englander. In 1944 he became partner in that firm, and in 1951 started his own accountancy firm. In 1944 he also started lecturing accounting at the [[City College of New York]], where he became a full-time faculty member in 1965. In 1976 he was appointed Emanuel Saxe Distinguished Professor of Accountancy.<ref>[http://www.baruch.cuny.edu/news/AbrahamBriloffAccountingHallofFame.htm Abraham J. Briloff ('37, MSEd '41) Elected to Accounting Hall of Fame] at baruch.cuny.edu. June 5, 2014. Accessed 2015-02-14.</ref> He later started writing for the [[Barron's (newspaper)|Barron's Magazine]], a financial weekly.

Briloff was inducted into the [[Accounting Hall of Fame]] in 2014. Their argument was, that "for over half a century, he was the conscience of the accounting profession, challenging it to raise standards and to meet its obligations to society. Always motivated by the best interests of the accounting profession, he was respected by both supporters and targets of his criticism."<ref name="AHoF 2015"/>

== Selected publications ==
* Briloff, Abraham J. ''Effectiveness of Accounting Communication.'' (1967).
* Briloff, Abraham J. ''Unaccountable Accounting.'' Harpercollins, 1972.
* Briloff, Abraham J. ''More Debits than Credits.'' Harper and Row, New York, NY (1976).
* Briloff, Abraham J. ''The Truth about Corporate Accounting.'' Harpercollins, 1981.

Articles, a selection:
* Briloff, Abraham J. "Old myths and new realities in accountancy." ''[[Accounting Review]]'' (1966): 484-495.
* Briloff, Abraham J. "Accountancy and society a covenant desecrated." ''[[Critical Perspectives on Accounting]]'' 1.1 (1990): 5-30.
* Briloff, Abraham J. "Garbage in/garbage out: A critique of fraudulent financial reporting: 1987–1997 (the COSO report) and the SEC accounting regulatory process." ''Critical Perspectives on Accounting'' 12.2 (2001): 125-148.

== References ==
{{reflist}}

== External links ==
* [https://www.nytimes.com/2013/12/16/business/abraham-briloff-accounting-professor-dies-at-96.html?_r=0 Abraham Briloff, Accounting Professor, Dies at 96], The New York Times, December 15, 2013
* [http://fisher.osu.edu/departments/accounting-and-mis/the-accounting-hall-of-fame/membership-in-hall/abraham-jacob-briloff/ Abraham Jacob Briloff], The Accounting Hall of Fame

{{Authority control}}

{{DEFAULTSORT:Briloff, Abraham Jacob}}
[[Category:1917 births]]
[[Category:2013 deaths]]
[[Category:American accountants]]
[[Category:American business theorists]]
[[Category:Accounting academics]]
[[Category:Baruch College alumni]]
[[Category:Stern School of Business alumni]]
[[Category:Baruch College faculty]]
[[Category:People from New York City]]