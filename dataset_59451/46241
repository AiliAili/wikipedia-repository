'''Aran knitting patterns''' are heavily textured knitting patterns which are named after the [[Aran Islands]], which are located off the west coast of Ireland from County Galway and County Clare. The patterns are knitted into socks, hats, vests, scarves, mittens, afghans, pillow covers,<ref>{{cite book|title=Knitting: techniques and projects|url=https://books.google.com/books?id=zLghaJnhPF0C|year=1976|publisher=Lane Pub. Co.|isbn=978-0-376-04431-0}}</ref> and, most commonly, sweaters.<ref name="Leslie2007">{{cite book|author=Catherine Amoroso Leslie|title=Needlework Through History: An Encyclopedia|url=https://books.google.com/books?id=lEiGeSLKLjMC&pg=PA10|date=1 January 2007|publisher=Greenwood Publishing Group|isbn=978-0-313-33548-8|pages=10–}}</ref>

== History ==
The first known example of Aran knitting appeared in the 1930s.<ref>{{cite book| last = Starmore| first = Alice| title = Aran Knitting | publisher = David & Charles| year = 1997}}</ref>

The stitches that create the Aran knitting patterns are complex and the knitted goods are time-consuming to create.  For example, a typical [[Aran jumper|Aran sweater]] will have over 100,000 stitches, and may take several months to finish. The three dimensional effect of the twisted stitches also increased the warmth of the clothing by creating air pockets.<ref name="Leslie2007" />

Contrary to popular mythology, Aran knitting patterns were never used to assist in identifying dead fishermen who washed up onshore after accident at sea. This story appears to have been derived from John Millington Synge's play, Riders to the Sea, in which a character is identified by means of stitches dropped in error in a pair of plain socks knitted by his sister.

== Meanings of the stitches ==

The idea that there are meanings associated with the stitches used in Aran knitting derives from Sacred History of Knitting, an entirely fabricated book written by [Heinz Edgar Kiewe]. Kiewe, a self-styled 'textile journalist' who ran a yarn shop in Oxford, purchased one of the first Aran-style sweaters and, noting a chance similarity between the patterns and ancient Irish illuminated manuscripts, began describing the stitchwork in these terms. Before long, his fanciful descriptions were being used to market the sweater abroad, particularly within the Irish Diaspora in the United States, and it became an accepted part of the sweater's lore that the knitting patterns were developed in ancient times, that each stitch pattern had an associated, usually Christian meaning, and that each family on the Aran Islands had its own 'clan Aran'. However, actual historical documentation, including some of the oldest photography shot in Ireland, are at odds with this mythology.

Nevertheless, knitters may find the following meanings entertaining and inspiring.<ref>[http://www.irishcultureandcustoms.com/AEmblem/Sweaters.html "Aran Isle Sweaters - how a dropped stitch gave rise to a popular myth."]. ''Irish Culture and Custome''. by Bridget Haggerty</ref> The meanings of some of these stitches have been embellished in recent times since the knitting became a commercial enterprise.<ref>[http://www.dochara.com/info/things-to-buy/aran-stitches-and-their-meanings/ "Aran Stitches and their Meanings"]. ''Ireland from the Inside''.</ref>

=== Cable stitch ===


The [[Cable knitting|cable stitch]], which is the most common type of stitch seen on Aran sweaters, is said to represent a fisherman’s ropes.<ref name="Walton2010">{{cite book|author=Sally Walton|title=Sweet and Simple Knitting Projects: Teach Yourself|url=https://books.google.com/books?id=k8c0AgAAQBAJ&pg=PP12|date=29 October 2010|publisher=Hodder & Stoughton|isbn=978-1-4441-3412-4|pages=12–}}</ref> There are many different type of cable stitches. The technique for cabling, which involves crossing one stitch over another is one of the easier stitches.

The row on which the stitches crossed over each other is known as the turning row. After the turning row, several plain rows are worked, followed by another turning row. Standard cables have the same number of plain rows between turning rows as there are stitches in the cable.<ref>{{cite book| last = Weiss| first = Rita| title = 50 Knit Aran Stitches | publisher = Leisure Arts| year = 2009}}</ref>

=== Diamond stitch ===


The diamond stitch supposedly symbolises the small fields on the islands. These fields were worked intensively by local farmers, and this stitch may be said to represent hopes of good luck, success and wealth in farming on the Aran Islands.<ref name="Panchyk2004">{{cite book|author=Richard Panchyk|title=American Folk Art for Kids: With 21 Activities|url=https://books.google.com/books?id=dqsLjeIzyGcC&pg=PA55|date=1 September 2004|publisher=Chicago Review Press|isbn=978-1-61374-119-1|pages=55–}}</ref> Diamond patterns might also represent the fishing nets.<ref name="Breffny1983">{{cite book|author=Brian De Breffny|title=Ireland, a cultural encyclopaedia|url=https://books.google.com/books?id=yXQH9M5tQE4C|date=October 1983|publisher=Facts on File|isbn=978-0-87196-260-7}}</ref>

=== Zig Zag stitch ===


Zig zag stitches, sometimes known as Marriage Lines, can be used to represent the typical highs and low of matrimony and marriage life.<ref name="Panchyk2004" /> They are may also be used to represent the twisting cliff paths that are on the islands.

=== Honeycomb stitch ===


In Aran knitting patterns the honeycomb stitch, signifying the bee, is often used to represent both hard work and its rewards.<ref name="OutletStaff1988">{{cite book|author1=Outlet|author2=Outlet Book Company Staff|author3=Rh Value Publishing|title=Complete Book of Handicrafts|url=https://books.google.com/books?id=0gncKF_W8HMC|date=12 December 1988|publisher=Random House Value Publishing|isbn=978-0-517-11667-8}}</ref> The honeycomb stitch may be included as a symbol of good luck, signifying plenty.<ref name="Panchyk2004" />

When only one repetition of the pattern is used, the honeycomb stitch is also known as the Chain Cable.

=== Trellis stitch ===


Trellis stitch recalls the stone-walled fields of the Northwestern farming communities, in the upland areas in Ireland where rock outcrops naturally or large stones exist in quantity in the soil such as in the Aran Islands. The stitch is useful for adding dimension, and might be used as a symbol of protection.

=== Tree of Life stitch ===



The Tree of Life stitch is frequently used as a motif of rites of passage, and of the importance of family. It is sometimes given a religious significance, symbolising a pilgrims path to salvation.<ref name="Breffny1983" /> This stitch is also known as the Trinity stitch.

== Aran knitting patterns in modern times ==

Aran knitting patterns are very adaptable, and are widely used in many types of knitted items, including hats,<ref name="Feller2012">{{cite book|author=Carol Feller|title=Contemporary Irish Knits|url=https://books.google.com/books?id=2IdbpFcHx4wC&pg=PP7|date=2 February 2012|publisher=John Wiley & Sons|isbn=978-1-118-29535-9|pages=7–}}</ref> scarves, skirts,<ref name="Feller2012" /> and even decorative pillows.<ref>http://www.canadianliving.com/crafts/knitting/aran_pillow.php</ref> The recent revival of interest in handcrafts have led to many modern variations of both stitches and designs. The original sweaters were typically boxy, with saddle sleeves, knit flat and sewn together; however, many modern designs are knit in the round with little or no sewing, and are frequently fitted by clever manipulation of the stitch patterns.

Today, the patterns are being used by knitters around the world,<ref name="Pearson2015">{{cite book|author=Michael Pearson|title=Traditional Knitting: Aran, Fair Isle and Fisher Ganseys|url=https://books.google.com/books?id=VatzBgAAQBAJ&pg=PA222|date=16 January 2015|publisher=Courier Dover Publications|isbn=978-0-486-46053-6|pages=222–}}</ref> and the sweaters have become an Irish export commodity.<ref name="LynchStrauss2014">{{cite book|author1=Annette Lynch|author2=Mitchell D. Strauss|title=Ethnic Dress in the United States: A Cultural Encyclopedia|url=https://books.google.com/books?id=tiEvBQAAQBAJ&pg=PA16|date=30 October 2014|publisher=Rowman & Littlefield Publishers|isbn=978-0-7591-2150-8|pages=16–}}</ref>

Due to the success of cheap imports from the Far East from the 1970s onward, both the Irish woollen industry and the associated cottage knitting industry in Ireland which supplied hand-knit Aran-style items to the market were all but destroyed, and today only a few mills and handknitters continue the tradition. As a result, a hand-knit Aran sweater can be quite expensive, and may well be worked in wool and yarn blends imported from overseas.

== References ==
{{Reflist}}
<!-- Be sure to cite all of your sources in<ref>...</ref> tags and they will automatically display when you hit save. The more reliable sources added the better! See [[Wikipedia:REFB]] for more information-->

[[Category:Knitting]]
[[Category:Aran Islands]]