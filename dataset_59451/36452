{{Infobox NRHP
  | name = Anshei Israel Synagogue
  | nrhp_type = nrhp
  | image = LisbonCT_AnsheiIsraelSynagogue.jpg
  | caption = Anshei Israel Synagogue
  | location= 142 Newent Rd. ([[Connecticut Route 138|CT 138]]), [[Lisbon, Connecticut]]
  | coordinates = {{coord|41|36|17|N|72|00|02|W|display=inline,title}}
| locmapin = Connecticut#USA
  | built = {{start date|1936}}
  | builder = George Allen & Sons
  | architecture = Colonial Revival
  | added = July 21, 1995
  | area = less than one acre
  | governing_body = Private 
  | mpsub = {{NRHP url|id=64500078|title=Historic Synagogues of Connecticut MPS}}
  | refnum = 95000861<ref name="nris">{{NRISref|version=2010a}}</ref>
}}
'''Anshei Israel Synagogue''' is a historic [[synagogue]] located in [[Lisbon, Connecticut]], United States. The [[Orthodox Judaism|Orthodox congregation]] was founded with 15 families and constructed the synagogue in 1936. It was built by George Allen & Sons. The interior is a single room that is lined with five benches before an altar which held the sacred ark. The congregation's membership dwindled throughout the 1940s and 1950s, limiting the services to holidays before finally closing in the early 1980s. Rules in the congregation were not as strictly enforced as in the [[Old World]], as there was no curtain to separate the sexes and distant members were allowed to drive part of the way to its services. The Town of Lisbon took ownership of the property in the 1980s. The synagogue is currently maintained by the Lisbon Historical Society. The synagogue was added to the [[National Register of Historic Places]] in 1995.

== Construction==
In the early 1800s of Connecticut, people of the Jewish faith were few, with just twelve persons in the whole of Connecticut. In 1843, Jewish public worship was first permitted after a petition to the Connecticut General Assembly. Early Jewish services were held in private homes or in rented halls and later in the first synagogues, which were usually adapted from churches. Rural synagogues like the Anshei Israel Synagogue were modest structures and "reflect the need of Jewish farming and summer congregations for buildings suitable for worship that were within their limited financial and geographic boundaries."<ref>{{cite web | url=http://www.nps.gov/nr/feature/jewishheritage/2007/synagogues.htm | title=Historic Synagogues of Connecticut | publisher=National Park Service | accessdate=2 April 2014}}</ref>

The land upon which the synagogue was built was donated by Harry Rothenberg around 1936. The congregation, pooled their money to construct the synagogue.<ref name=day1 /> Constructed by George Allen & Sons in 1836, the Anshei Israel Synagogue is a {{convert|20|ft|m}} by {{convert|30|ft|m}} gable-roofed clapboarded building with a {{convert|5|ft|m}} by {{convert|9|ft|m}} central projecting tower with a [[Magen David]] at its top. Flanking the tower on each side is a pair of 2-over-2 windows. Paint remnants show that the sash was previously painted a bright blue.<ref name="np">{{cite web | url={{NRHP url|id=95000861}} | title=National Register of Historic Places - Anshei Israel Synagogue | publisher=National Park Service | date=14 June 1995 | accessdate=28 March 2014 | author=Ransom, David}}</ref>

After passing through the tower, the single room has a platform with an ark at the front. The original altar and sacred ark remains, with the interior concealed by a gold curtain; and a menorah rests on the podium. Chairs and five wooden, backless benches were provided for seating, which was described as "an unusual arrangement in historic Connecticut synagogues".<ref name=day1 /><ref name=np /> Though the building had electricity, it had no heat or plumbing; though a wood stove was used to provide heat and an outhouse was previously behind the synagogue.<ref name=day1 />

== Use ==
The founding congregation of 15 families came from [[Poland]] and [[Russia]] and lived in the surrounding towns of [[Plainfield, Connecticut|Plainfield]], [[Lisbon, Connecticut|Lisbon]], [[Griswold, Connecticut|Griswold]], and [[Jewett City, Connecticut|Jewett City]].<ref name=np /> Rothenberg became the first cantor of the Anshei Israel Synagogue and the service would continue to serve the Orthodox congregation for decades.<ref name=day1 /> The congregation's membership dwindled throughout the 1940s and 1950s, which limited services to holidays. The synagogue finally closed when it could no longer steadily gather a minyin, ten men, in 1987.<ref name=day1 /><ref name="day3">{{cite web | url=http://www.theday.com/article/20060429/DAYARC/304299986/0/SEARCH | title=Jewish Survival, Colonial Revival In Lisbon | work=The Day | date=26 April 2006 | accessdate=29 March 2014 | author=Potter, Chuck}}</ref> The town of Lisbon acquired the synagogue in the 1980s from the synagogue's last six members.<ref name=day1 /> In 2004, the synagogue was open during "Walking Weekend" events.<ref name=day1 />

Caroline Read-Burns, president of the Lisbon Historical Society and Jerome Zuckerbraun, a member of the synagogue, discussed the Orthodox congregation's rules and noted that some rules were not as strictly enforced as in the Old World.<ref name="day1">{{cite web | url=http://www.theday.com/article/20050515/DAYARC/305159952/0/SEARCH | title=Lisbon Meetinghouse May Be One-of-a-kind In Nation | work=The Day | date=15 May 2005 | accessdate=28 March 2014 | author=McNamara Grace, Eileen}}</ref> As an Orthodox congregation, members were to walk to the synagogue, but some distant members would drive and "walk the last mile or so."<ref name=day1 /> The synagogue did not use curtains to separate men and women, as was the norm for Orthodox services in Poland and Russia.<ref name=day1 /> The women's seating was at a table on the right side of the sanctuary, near the door.<ref name=np /> The structure is well-preserved, but not currently in use.<ref name=np />

== Importance ==
The Anshei Israel Synagogue was added to the National Register of Historic Places in 1995.<ref name=np /> It is recognized as an architecturally significant example of "a small country Jewish house of worship".<ref name=np /> Its architecture is the "epitome of simplicity" and it remains an important pre-1945 Jewish house of worship that is in a rural setting and possessing integrity in its design.<ref name=np /> ''The Day'' reflected Read-Burn's comments that the synagogue may "only one of its kind in the country".<ref name=day1 /> In 2001, the building was featuring on ''Connecticut Journal'', a program of [[Connecticut Public Television]].<ref name="tv">{{cite web | url=https://news.google.com/newspapers?nid=1915&dat=20010927&id=8u4gAAAAIBAJ&sjid=pHIFAAAAIBAJ&pg=1690,5912102 | title=Historic Lisbon synagogue on CPTV | work=The Day | date=27 September 2001 | accessdate=2 April 2014}}</ref>

In 2005, the building was in need of some repairs due to neglect and damage from squirrels.<ref name=day1 /> The Lisbon Historical Society received a $5,000 grant from the Quinebaug Shetucket National Heritage Corridor to make repairs on the synagogue.<ref name="day2">{{cite web | url=http://www.theday.com/article/20050520/DAYARC/305209862/0/SEARCH | title=Historical Society Gets Grant To Renovate Synagogue | work=The Day | date=20 May 2005 | accessdate=28 March 2014 | author=McNamara Grace, Eileen}}</ref> Repairs would be done to repair the structure and the electrical wiring would be replaced for free by the students at the [[Norwich Regional Vocational Technical School]].<ref name=day2 />

==See also==
{{Portal|Connecticut}}
*[[National Register of Historic Places listings in New London County, Connecticut]]

==References==
{{Reflist}}

{{National Register of Historic Places}}
{{Good article}}

[[Category:Synagogues in Connecticut]]
[[Category:Jewish-American history]]
[[Category:Religious buildings completed in 1936]]
[[Category:Buildings and structures in New London County, Connecticut]]
[[Category:Lisbon, Connecticut]]
[[Category:Synagogues on the National Register of Historic Places in Connecticut]]
[[Category:National Register of Historic Places in New London County, Connecticut]]