{{Infobox company
 | name   = Symphony Aircraft Industries
 | logo   = [[File:SymphonyLogo.jpg|200px]]
 | slogan         = 
 | type   = Private
 | fate           = Bankrupt 22 January 2007
 | Predecessor    = 
 | successor      = 
 | foundation     = December 2003
 | defunct        = 
 | location       = [[Trois-Rivières]], [[Quebec]], [[Canada]]
 | industry       = [[Aerospace]]
 | products       = [[General aviation]] aircraft
 | key_people     = Paul Costanzo, CEO (September 2003 - January 2007)
 | num_employees  = 
 | parent         = 
 | subsid         = 
}}

'''Symphony Aircraft Industries (SAI)''' was a light aircraft manufacturer based in [[Trois-Rivières, Quebec|Trois-Rivières]], [[Quebec]], [[Canada]].<ref name="COPA01">Hunt, Adam: ''A brief history of Symphony Aircraft'', COPA Flight December 2005</ref>

Between May 2005 and January 2007 SAI manufactured the [[Symphony SA-160]] aircraft for the personal use, touring and flight training market.<ref name="COPA01"/> On January 22, 2007, it declared bankruptcy and ceased operations. The company's motto was ''Certified Fun''<ref name="Archive">{{cite web|url = http://www.symphonyaircraft.com/ |title = Symphony Aircraft|accessdate = 2009-09-12|last = Symphony Aircraft Industries|authorlink = |date=March 2007 |archiveurl = https://web.archive.org/web/20070319210119/http://www.symphonyaircraft.com/ |archivedate = 2007-03-19}}</ref>

==History==
[[File:SymphonyAircraftSA160-03.jpg|thumb|right|The [[Symphony SA-160]]]]
[[File:SAIPlantTroisRivieres03.jpg|thumb|right|An aerial view of the Symphony Aircraft Industries plant in [[Trois-Rivières, Quebec|Trois-Rivières]], August 2005]]
[[File:PaulCostanzo.jpg|thumb|right|Symphony Aircraft Industries President and CEO Paul Costanzo, August 2005]]

===OMF Aircraft===
SAI was started in September 2003 as the North American production arm of [[Ostmecklenburgische Flugzeugbau]] GmbH, (East Mecklenburg Aircraft Works Limited). The company was commonly known as OMF Aircraft. OMF was formed by [[Mathias Stinnes]] in 1998 and was headquartered in [[Neubrandenburg]] Mecklenburg-Western Pomerania.<ref name="COPA01"/>

Stinnes formed OMF Aircraft to produce a FAR 23 certified version of the [[Stoddard-Hamilton Aircraft|Stoddard-Hamilton Glastar]]. He had built and flown one of these amateur-built aircraft in the 1990s and believed that it would make a good production aircraft. The certified version of the Glastar was to be called the [[Symphony SA-160|OMF-100-160 Symphony]] with certification targeted for 2000.<ref name="COPA01"/>

Realizing that the bulk of the market for this aircraft was in North America Stinnes set up a production facility in Trois-Rivières with financial help from the Government of Quebec. The plant building was constructed by the Town of Trois-Rivières and leased to OMF. The plant was opened in September 2003.<ref name="COPA01"/>

OMF suffered from under-financing during its start-up phase and declared bankruptcy in December 2003. Due to agreements in place, the production rights for the OMF-100-160 Symphony resided in Canada while the intellectual property rights were retained in Germany.<ref name="COPA01"/>

===Symphony Aircraft Industries===

The Canadian plant operation was reorganized as a private company under the name of Symphony Aircraft Industries, while the intellectual property rights to the OMF-100-160 Symphony were sold by the bankruptcy trustees to a new German company, OMF Flugzeugwerk.<ref name="COPA01"/>

An agreement was signed between OMF Flugzeugwerk and Symphony Aircraft to produce the design on two production lines in North America and Europe. This soon proved unworkable and OMF Flugzeugwerk sold out their interests to SAI in February 2005.<ref name="COPA01"/>

SAI redesignated the aircraft as the Symphony SA-160 and completed [[Transport Canada]] certification under [[Canadian Aviation Regulations|CAR 523]], with Transport Canada manufacturing approval achieved in March 2005.<ref name="COPA01"/>

On May 4, 2005 SAI completed [[FAA]] Type Certification, based on the Canadian certification and the first SAI produced aircraft received its FAA Certificate of Airworthiness on May 13, 2005.<ref name="COPA01"/>

The company started by producing two Symphony 160s per month in early 2005 with production increasing to three per month in the fall of 2005 and five per month in 2006. The company pursued certification of a [[Ballistic Recovery Systems]] full aircraft parachute system and also an [[Avidyne FlightMax|Avidyne]] [[glass cockpit]] instrumentation installation.<ref name="COPA01"/>

SAI filed for protection from creditors under the Canadian ''Companies' Creditors Arrangement Act'' in June 2006.<ref name="AvWeb28Jun2006">{{cite web|url = http://www.avweb.com/avwebflash/leadnews/192591-1.html|title = Symphony 160: Cash Needed, Customers Waiting|accessdate = 2008-02-05|last = Grady|first = Mary|authorlink = |date=June 2006}}</ref> At that time the company stated that it needed an investment of US$5.5M to continue production. The company was not able to secure the capital in Canada and indicated at that time that it might have to relocate production to attract financing from other sources.<ref name="AvWeb28Jun06">{{cite web|url = http://www.avweb.com/avwebflash/leadnews/192592-1.html|title = The Product Will Prevail, Company Says|accessdate = 2008-02-05|last = Grady|first = Mary|authorlink = |date=June 2006}}</ref> CEO Paul Costanzo indicated that factors involved in this situation included the dramatic fall in value of the US dollar against the Canadian dollar between 2003–2006, start-up and product improvement costs that were greater than expected and especially a venture capital environment in the Province of Quebec that Costanzo described as "dismal". He stated at the time, "We have simply been unable to attract sufficient capital to allow us to effectively exploit the obvious opportunity that we have with the SA-160, let alone fund our planned development of a diesel two-place and a four-place product." He indicated then that the company was focusing on certification of the SA-160's [[glass cockpit]] installation along with product cost reduction measures.

On January 22, 2007, following the withdrawal of its lead investor three days previously, Symphony Aircraft declared bankruptcy, closed its doors and laid off its entire workforce.

===NAFTAA===
On February 3, 2008 it was publicly announced that the former lead investor in Symphony Aircraft, Lou Simons, was planning to restart production of the SA-160 under the name North American Factory for Technologically Advanced Aircraft (NAFTAA). The intention at that time was that aircraft may be produced at a different location and may have a new name. Also planned were additional versions of the SA-160.<ref name="AvWeb03Feb08">{{cite web|url = http://www.avweb.com/avwebflash/news/SymphonyPlanningComeback_197075-1.html|title = Symphony Planning Comeback|accessdate = 2008-02-03|last = Peppler|first = Graeme |authorlink = |date=February 2008}}</ref>

The new company announced in July 2008 that they intended to restart production by the end of 2009 at the previous plant in Trois Riveries, with a plan to produce 15 aircraft in 2009 and ramp up production to 50 to 80 per year by 2012. The aircraft were to have been given a new name and be available in a [[Visual Flight Rules|VFR]] version intended for flight school use, a basic [[Instrument flight rules|IFR]] version and a [[glass cockpit]] equipped  version powered by a 200-hp [[Lycoming IO-390]] powerplant giving it a forecast cruise speed of 148 knots.<ref name="AvWeb01Aug08">{{cite web|url = http://www.avweb.com/eletter/archives/avflash/1176-full.html|title = AVwebFlash Complete Issue: Volume 14, Number 31e|accessdate = 2008-08-01|last = AVweb Editorial Staff |authorlink = |date=August 2008}}</ref> The plan was never completed and no new aircraft were produced.

==Products==
* [[Symphony SA-160]]

==References==
{{reflist}}
* [http://www.avweb.com/avwebflash/12_26b/leadnews/192591-1.html Symphony 160: Cash Needed, Customers Waiting – AvWeb Jun 29 2006]
* [http://www.avweb.com/avwebflash/12_26b/leadnews/192592-1.html The Product Will Prevail, Company Says - AvWeb Jun 29 2006]
* Sport Aviation Magazine, Sept 2006, page 18, Symphony Restructuring

==External links==
* [http://www.symphonyAircraft.com Former location of the Symphony Aircraft Industries website]

[[Category:Aviation in Canada]]
[[Category:Defunct aircraft manufacturers of Canada]]
[[Category:Companies based in Quebec]]
[[Category:Trois-Rivières]]