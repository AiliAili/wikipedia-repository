{{Infobox journal
| title = Science and Christian Belief
| cover = [[File:Science and Christian Belief.jpg]]
| editor = [[Denis Alexander|Denis R. Alexander]], Rodney D. Holder
| discipline = [[Philosophy]], [[religious studies]], [[science and religion]]
| abbreviation = 
| publisher = [[Paternoster Press]] on behalf of [[Christians in Science]] and the [[Victoria Institute]]
| country = United Kingdom
| frequency = Biannually
| history = 1989–present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.scienceandchristianbelief.org/
| link2 = https://www.scienceandchristianbelief.org/browse.php
| link2-name = Online archive
| JSTOR = 
| OCLC = 
| LCCN = 91643099
| CODEN = 
| ISSN = 0954-4194
| eISSN = 
}}
'''''Science and Christian Belief''''' is a biannual [[peer-reviewed]] [[academic journal]] published by [[Paternoster Press]] on behalf of [[Christians in Science]] and the [[Victoria Institute]].<ref>[https://books.google.com/books?id=hBoZAAAAIAAJ&q=%22Science+and+Christian+Belief%22+%22victoria+institute%22&dq=%22Science+and+Christian+Belief%22+%22victoria+institute%22&lr=&num=100&as_brr=0 World Evangelical Fellowship], ''[[Evangelical Review of Theology]]'', Volume 15, p. 191, [[Paternoster Press]] (1991), accessed 5 November 2009</ref><ref>[http://au.christiantoday.com/article/christians/4089.htm "Creation or Evolution: Do We Have To Choose?'", ''[[Christian Today]]'', 14 August 2008, accessed 3 November 2009]</ref> The [[editors-in-chief]] are [[Denis Alexander|Denis R. Alexander]] ([[Faraday Institute for Science and Religion]])<ref>[http://www.churchtimes.co.uk/content.asp?id=53035 Bowder, Bill, "Cambridge science don ticks off the teen rebels," ''[[Church Times]]'', 7 March 2008, accessed 3 November 2009]</ref> and Rodney D. Holder.

The journal was established in 1989, with [[Oliver Barclay]] and A. Brian Robins as co-[[editors-in-chief]].<ref>[http://www.asa3.org/asa/pscf/1990/PSCF3-90Haas.html Haas, Jr., J.W., "Science & Christian Faith in Western Europe: Personal View," ''[[American Scientific Affiliation|PSCF]]'' 42 (March 1990): 39–44, accessed 3 November 2009]</ref> It is abstracted and indexed in ''[[New Testament Abstracts]]'', ''[[Religion Index One]]: Periodicals'', and ''[[Religious & Theological Abstracts]]'', and is distributed by [[EBSCO Information Services]] as part of [[Academic Search]] and other collections. The journal is free to members of Christians in Science.<ref>{{cite web | url=http://www.cis.org.uk/journal | title=Science & Christian Belief |publisher=[[Christians in Science]] | accessdate=16 November 2009 }}</ref>

The Victoria Institute (also known as the [[Philosophical Society of Great Britain]]) published the ''[[Journal of the Transactions of The Victoria Institute]]'', which was established in 1866; it was renamed ''Faith and Thought'' in 1958, and then merged with the (informal) ''CIS Bulletin'' in 1989, obtaining its current name, ''Faith and Thought''.<ref>John W. Haas, Jr., with [[David O. Moberg]], Richard Bube,and Wilbur Bullock, "[http://www.asa3.org/asa/pscf/1998/PSCF12-98Haas2.html "The Journal of the American Scientific Affiliation At 50: Modest Beginnings, Maturing Vision, Continuing Challenges]," ''[[American Scientific Affiliation|Perspectives on Science and Christian Faith]]'', 50 (December 1998): 241–49, accessed 3 November 2009</ref><ref>''Faith & Thought'' (formerly ''Faith and Thought Newsletter'', established in 1985), ISSN 0955-2790, back cover</ref>

==References==
{{Reflist|30em}}

==External links==
* {{Official website|http://www.cis.org.uk/}}
* [[Warren S. Brown]] and [[Malcolm A. Jeeves]], [http://www.asa3.org/aSA/topics/PsychologyNeuroscience/S&CB10-99BrownJeeves.html "Portraits of Human Nature: Reconciling Neuroscience and Christian Anthropology,"] A report from a seminar at the combined meeting of the [[American Scientific Affiliation]] and [[Christians in Science]], [[Churchill College, Cambridge|Churchill College]], [[University of Cambridge|Cambridge University]], August 1998, from ''Science and Christian Belief'' 11 No. 2 (October 1999): 139–50
* [http://www.asa3.org/asa/pscf/1993/PSCF12-93Russell.html Russell, Colin, "Without a Memory"], from ''[[Perspectives on Science and Christian Faith]]'' ([[American Scientific Affiliation]]), 45 (March 1993): 219–21, Reprinted with permission from ''Science & Christian Belief'' (1993) Vol 5. No 1
* [https://archive.org/stream/faithandthought02britgoog/faithandthought02britgoog_djvu.txt Full text of Vol. 1]

{{DEFAULTSORT:Science And Christian Belief}}
[[Category:Religion and science]]
[[Category:Christianity studies journals]]
[[Category:Christian media]]
[[Category:Publications established in 1988]]
[[Category:Christianity and science]]
[[Category:Biannual journals]]
[[Category:English-language journals]]