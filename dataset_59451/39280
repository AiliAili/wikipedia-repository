{{good article}}
{{Infobox video game
| title         = Cannon Fodder 3
| collapsible   = 
| state         = 
| show image    = 
| image         = Cannon Fodder 3 cover art.png
| caption       = 
| developer     = Burut CT
| publisher     = [[Game Factory Interactive]]
| distributor   = 
| series        = 
| engine        = 
| version       = 
| platforms     = [[Microsoft Windows]]
| released      = {{Video game release|RU|December, 2011|WW|February, 2012}}
| genre         = [[Action game|Action]], [[Strategy video game|strategy]]
| modes         = 
}}

'''''Cannon Fodder 3''''' is an [[Action game|action]]-[[Strategy video game|strategy]] [[Personal computer|PC]] game developed and published – originally in Russia – by [[Game Factory Interactive]] (GFI), along with developer Burut CT. The game is the second sequel to ''[[Cannon Fodder (video game)|Cannon Fodder]]'', a commercially and critically successful game released for multiple formats in 1993. [[Jon Hare]] and his company [[Sensible Software]], the developers of ''Cannon Fodder'' and its prior sequel, were uninvolved with ''Cannon Fodder 3''.<ref name="metro"/> GFI instead licensed the intellectual property from now-owner [[Codemasters]].

The game is a combination of action and strategy involving a small number of soldiers battling enemy terrorists. The protagonists are heavily outnumbered and easily killed. The player must rely on strategy and heavy secondary weapons to overcome enemies, their vehicles and installations.

''Cannon Fodder 3'' was released in Russia in December 2011 and – via download service [[GamersGate]] – Europe and North America on 9 February 2012, receiving mixed reviews. Reviewers found the game to be enjoyable but limited and repetitive. The graphics and humour received both positive and negative criticism. Critics derided the poor English translations as well as technical problems.

==Overview==
''Cannon Fodder 3'' has a basic premise involving American soldiers<ref name="pcgamer"/> battling disparate terrorists who have united with the aim of world domination. Like the earlier games in the series, it features a mix of "old school" action and strategy gameplay viewed from an isometric perspective. The player directs a small squad of soldiers, primarily with the mouse though the keyboard is used to deploy additional weaponry.<ref name="digitalspy"/> The control system "is very similar to ‘twin-stick’ shooters like [[Geometry Wars]], except here you control just one of the four soldiers while the others fall in line, shooting when the lead does."<ref name="powerplay"/> Each squad begins with four soldiers. They are fragile (though, possessing [[Health (gaming)|health bars]], are somewhat tougher than the protagonists of the originals),<ref name="strategy informer"/> but increase in status and power should they survive; the player is provided with a replacement squad should all his platoon die. The player can also split the squad into smaller units should strategy so require. The game has an [[Cooperative gameplay|online cooperative]] mode for up to four players,<ref name="digitalspy"/> though Strategy Informer noted this mode is unpopular.<ref name="strategy informer"/> As well as facing large numbers of equally frail enemy infantry, the player must combat vehicles, buildings and turrets which cannot be destroyed with the standard machine guns. For this reason, the player must rely on explosive secondary weapons such as grenades and rockets, which are essential to destroy enemy structures and more powerful units. He can also make use of vehicles – such as tanks and helicopters – as well as various [[power-up]]s.<ref name="powerplay"/> The game features an extensively destructible environment and several settings, including the moon.<ref name="SI interview"/>

==Development==
''Cannon Fodder 3'' is the third instalment in the ''Cannon Fodder'' series, the first two games of which – ''Cannon Fodder'' and ''[[Cannon Fodder 2]]'' – were successful across multiple formats in the 1990s. Those games were created by [[Sensible Software]] led by [[Jon Hare]]; Hare later worked on abortive sequels for both the [[PlayStation 2]] and [[PlayStation Portable]], as well as planning a version for [[smartphone]]s.<ref name="eurogamer iphone"/><ref name="metro iphone"/> However, the publisher [[Codemasters]] had acquired Sensible Software and its intellectual property. In 2008 Codemasters licensed Russian company [[Game Factory Interactive]] (GFI) – which had previously been involved in games such as ''[[Precursors (video game)|The Precursors]]'', ''[[Boiling Point: Road to Hell]]'' and ''[[White Gold: War in Paradise]]'' – to develop ''Cannon Fodder 3''. While English-language media reported on the development in January 2011, GFI was initially permitted only to release the game in Russia and the [[Commonwealth of Independent States]], with the possibility of a wider European or North American release unclear.<ref name="rockpaper 2011"/><ref name="eurogamer 2011"/><ref name="edge"/> GFI, described as an "unknown",<ref name="eurogamer 2011"/> or "little-known" company,<ref name="edge"/> published the game in Russia in December 2011, with both GFI<ref name="eurogamer 3feb"/><ref name="rockpaper 2012"/> and Burut CT<ref name="pcgamer"/><ref name="eurogamer 9feb"/> variously reported as developers. [[Eurogamer]] then reported that Codemasters had clarified the agreement between itself and GFI: Codemasters had reserved the option to publish the game in the UK, but ultimately declined. This allowed GFI to distribute the game out with Russia and dispelled the belief that GFI was not authorised to make such a release.<ref name="eurogamer 9feb"/> The game was released via [[GamersGate]], in Europe and North America, on 9 February 2012.<ref name="digitalspy2"/>

GFI's Oleg Lychaniy stated the developers attempted to retain the most appealing elements of the original ''Cannon Fodder'' while attracting new players. GFI also attempted to retain ''Cannon Fodder's'' "antimilitarist message" and was most proud of the new destructible environment. The developers broadened the variety of weapons and vehicles and changed the level structure by adding sub-missions.<ref name="SI interview"/>

==Reception==
{{Video game reviews
| title = Cannon Fodder 3
| PCGUK = 53%<ref name="pcgamer"/>
| PCPP = 5/10<ref name="powerplay"/>
| rev1 = ''[[Compupress#Key titles|PC Master]]''
| rev1Score = 80%<ref name="metacritic"/>
| rev2 = ''Strategy Informer''
| rev2Score = 7/10<ref name="strategy informer"/>
| rev3 = ''[[Digital Spy]]''
| rev3Score = 3/5
| rev4 = Games.cz
| rev4Score = 4/10<ref name="games.cz"/>
| rev5 = [[Metro (British newspaper)|''Metro'' (UK)]]
| rev5Score = 3/10<ref name="metro review"/>
}}
Richard Cobbet, writing in ''[[PC Gamer#PC Gamer UK|PC Gamer UK]]'' said: "just maybe, this is going too far", in reference to images of soldiers' gory corpses cleared from the interface by windscreen wipers. The reviewer expressed bemusement at "Terrorists with robots and plasma guns" antagonists, but reflected – in reference to ''Cannon Fodder 2'' – "Still, could be worse. They could be time-travelling aliens again." He said the "biggest issue" is that a "[[gung-ho]]" approach to play is infeasible and that the "precise and tactical" approach faithful to earlier games in the series "turns out to be a very different experience from before. Far less satisfying". This is due to the "key problem" of the ease with which enemy weapons such as rocket launcher-armed turrets can destroy the player's soldiers. The player is thus forced "to play with aching, paranoid care, picking away at base defences from as far away as possible, and rubbing up against the save points like a lovely cat finally reunited with its owner. This is not fun." He also complained about the arbitrary death inflicted by exploding barrels "raining down over the entire screen." Cobbet summarised that the departure from the original game meant that ''Cannon Fodder 3'' did not evoke nostalgia and was a "poor substitute" for other action or strategy games. On the other hand, he found the game "admittedly nowhere near as frustrating" as the original. He reflected that it "does have a solid crack at updating the action for a new age, and it's far from as bad as expected" given its obscure development.<ref name="pcgamer"/>

[[File:Cannon Fodder 3 screenshot.png|thumb|right|The player's soldiers destroy an enemy building in a snowy locale.]]
Liam Martin of [[Digital Spy]] noted that the variety of locations and weather effects added to the distinctiveness of individual levels. He praised the "simple but effective", "cartoonish" and colorful visual style, the improved detail from the game's predecessors along with the "light-hearted tone" and "spectacular and bloody" gore effects. He praised the destructible environments but thought the characters too small. He felt use of strategy to be "limited" but the action "enormous amounts of explosive fun" and enjoyed the gratification of destroying enemies and buildings with rifles and tanks. The writer found the game a "little repetitive" with a "tendency to grate", which he blamed on somewhat tedious level design, a lack of variety in objectives, a poor [[Virtual camera system|camera]] and long loading times. The lack of a tutorial or [[mini-map]] proved sometimes confusing. He noted the "welcome" but limited on-line cooperative mode. Martin felt the game was "best played in short bursts" and summarised: "Provided you're not expecting a game with much depth or that's oozing innovation, Cannon Fodder 3 proves that virtual war can still be a lot of fun."<ref name="digitalspy"/>

Nathan Cocks of ''[[PC PowerPlay]]'' complained the need for secondary weapons proves frustrating: "It is not uncommon to run out of the weapons needed for the job, forcing the player to engage in a tedious game of hide and seek as they scour the map in the hopes of finding another cache." He wrote: "In fact, tedium is a frequent bedfellow in Cannon Fodder 3", due to the lack of variety in the gameplay and mission objectives. He felt the player's vehicles to be of limited effectiveness as is the ability to split the squad. Cocks complained of the game's "bargain basement production" ruining the tone of the game due to low quality translations and voice acting, as well as numerous bugs causing problems with running the game, crashes, [[Spawning (video gaming)|spawning]] and using vehicles. The reviewer acknowledged the "wonderfully cartoony" graphics, found the gory windscreen wipers "amusing" and wrote "It’s all over-the-top explosive goodness and contributes a great deal to what little appeal is present." Cocks summarised: "Cannon Fodder 3 is the ultimate coat-tail rider [...] perhaps there is some nostalgic pleasure to be taken in this but for the most part, we suggest leaving well enough alone."<ref name="powerplay"/>

Strategy Informer felt the game had updated and improved upon the original, noting better controls, more forgiving health bars and save points, and more engaging mission goals. The reviewer criticised the poor translations and perceived bordeline ethnic stereotypes in the enemy characters. Overall the reviewer was impressed by the game, saying "in the end CF3 far exceeded my expectations".<ref name="strategy informer"/> ''[[Metro (British newspaper)|Metro]]'' had some praise for the graphics but complained of a lack of any fun and poor camera, controls and level design. The reviewer called the translated dialogue worse than that of ''[[Zero Wing]]''.<ref name="metro review"/> Greece's ''[[Compupress#Key titles|PC Master]]'' praised the game as enjoyable and while somewhat repetitive, a successful update of the ''Cannon Fodder'' series.<ref name="metacritic"/> Czech website Games.cz criticised the game as a failure, with poor graphics, music, sound effects and immature, unimpressive humour, saying the game would only be worth buying at a lower price.<ref name="games.cz"/>

==References ==
{{Reflist|30em|refs=
<ref name="metro">GameCentral, [http://www.metro.co.uk/tech/games/889285-cannon-fodder-3-out-now-in-russia Cannon Fodder 3 out now… in Russia], ''[[Metro (British newspaper)|Metro]]'', 3 Feb 2012, Retrieved 14 July 2012</ref>
<ref name="pcgamer">Richard Cobbet, "Lukewarm War", ''[[PC Gamer#PC Gamer UK|PC Gamer UK]]'', April 2012 (issue 238), p. 116</ref>
<ref name="digitalspy">Liam Martin, [http://www.digitalspy.co.uk/gaming/review/a367840/cannon-fodder-3-review-pc.html 'Cannon Fodder 3' review (PC)], [[Digital Spy]], 27 Feb 2012, Retrieved 13 July 2012</ref>
<ref name="powerplay">Nathan Cocks, [http://www.pcpowerplay.com.au/2012/06/cannon-fodder-3/ Cannon Fodder 3], ''[[PC PowerPlay]]'', 18 June 2012, Retrieved 13 July 2012</ref>
<ref name="eurogamer iphone">Robert Purchese, [http://www.eurogamer.net/articles/2011-01-11-hare-figuring-out-icannon-fodder Hare figuring out iCannon Fodder], [[Eurogamer]], 11 Jan 2011, Retrieved 14 July 2012</ref>
<ref name="metro iphone">GameCentral, [http://www.metro.co.uk/tech/games/852390-cannon-fodder-creator-ponders-iphone-comeback Cannon Fodder creator ponders iPhone comeback], ''[[Metro (British newspaper)|Metro]]'', 11 Jan 2011, Retrieved 14 July 2012</ref>
<ref name="rockpaper 2011">Quintin Smith, [http://www.rockpapershotgun.com/2011/01/18/son-of-a-gun-cannon-fodder-3-announced/ Son Of A Gun: Cannon Fodder 3 Announced], [[Rock, Paper, Shotgun]], 18 Jan 2011, Retrieved 14 July 2012</ref>
<ref name="eurogamer 2011">Wesley Yin-Poole, [http://www.eurogamer.net/articles/2011-01-18-will-cannon-fodder-3-release-in-the-uk Will Cannon Fodder 3 release in the UK?], [[Eurogamer]], 18 Jan 2011, Retrieved 14 July 2012</ref>
<ref name="edge">Nathan Brown, [http://www.edge-online.com/news/cannon-fodder-3-announced Cannon Fodder 3 Announced], ''[[Edge (magazine)|Edge]]'', 18 Jan 2011, Retrieved 14 July 2012</ref>
<ref name="eurogamer 3feb">Wesley Yin-Poole, [http://www.eurogamer.net/articles/2012-02-03-cannon-fodder-3-is-well Cannon Fodder 3 is… well…], [[Eurogamer]], 3 Feb 2012, Retrieved 14 July 2012</ref>
<ref name="rockpaper 2012">John Walker, [http://www.rockpapershotgun.com/2012/02/03/cannon-fodder-3-actually-happened-demo/ Cannon Fodder 3 Actually Happened, Demo], [[Rock, Paper, Shotgun]], 3 Feb 2012, Retrieved 14 July 2012</ref>
<ref name="eurogamer 9feb">Wesley Yin-Poole, [http://www.eurogamer.net/articles/2012-02-09-english-language-cannon-fodder-3-hits-gamersgate English language Cannon Fodder 3 hits GamersGate], [[Eurogamer]], 9 Feb 2012, Retrieved 14 July 2012</ref>
<ref name="digitalspy2">Scott Nichols, [http://www.digitalspy.co.uk/gaming/news/a364920/cannon-fodder-3-gets-english-release.html 'Cannon Fodder 3' gets English release], [[Digital Spy]], 9 Feb 2012, Retrieved 14 July 2012</ref>
<ref name="metacritic">[http://www.metacritic.com/game/pc/cannon-fodder-3/critic-reviews Cannon Fodder 3 PC], [[Metacritic]], Retrieved 14 July 2012; taken from ''[[Compupress#Key titles|PC Master]]'', Apr 2012</ref>
<ref name="games.cz">Karel Vojtisek, [http://games.tiscali.cz/recenze/cannon-fodder-3-recenze-58966 Cannon Fodder 3] (Czech), Games.cz, 29 Mar 2012, Retrieved 14 July 2012</ref>
<ref name="metro review">David Jenkins, [http://www.metro.co.uk/tech/games/907818-cannon-fodder-3-review-insensible-software Cannon Fodder 3 review – insensible software], ''[[Metro (British newspaper)|Metro]]'', 8 Aug 2012, Retrieved 10 Aug 2012</ref>
<ref name="strategy informer">Chris Capel, [http://www.strategyinformer.com/pc/cannonfodder3/1810/review.html Cannon Fodder 3 Review (PC)], Strategy Informer, Retrieved 10 Aug 2012</ref>
<ref name="SI interview">[http://www.strategyinformer.com/pc/cannonfodder3/271/interview.html Cannon Fodder 3 Interview (PC)], Strategy Informer, Retrieved 10 Aug 2012</ref>
}}
{{Portal bar|Video games|2010s}}

[[Category:2011 video games]]
[[Category:Anti-war video games]]
[[Category:Shooter video games]]
[[Category:Strategy video games]]
[[Category:Video game sequels]]
[[Category:Video games developed in Russia]]
[[Category:Video games with isometric graphics]]
[[Category:Windows games]]