{|{{Infobox aircraft begin
|name=Arado E.381
|image=Arado-234 V21 pic2.JPG 
|alt=
|caption=Model of an Arado Ar 234 V21 carrying an Arado E.381 at the [[Technikmuseum Speyer]]
}}
{{Infobox aircraft type
|type=[[Parasite aircraft|Parasite]] [[Fighter aircraft|fighter]]
|national origin=[[Germany]]
|manufacturer=[[Arado Flugzeugwerke|Arado]]
|status=Abandoned
|number built=0 powered aircraft,<ref name=Arado/> 4 unmanned wooden airframes.<ref name=herwig207>Herwig and Rode, p. 207</ref>
|primary user=[[Luftwaffe]]
}}
|}

The '''Arado E.381''' (''Kleinstjäger''&nbsp;– "smallest fighter") was a proposed [[Parasite aircraft|parasite]] [[fighter aircraft]]. Conceived by ''[[Arado Flugzeugwerke]]'' in December 1944 for [[Nazi Germany|German]]y's [[Luftwaffe]] during [[World War II]], the E.381 was to have been carried aloft by and launched from an [[Arado Ar 234]] "mother" aircraft. It would then have activated its [[rocket engine]], which would have propelled it to attack [[Allies (World War II)|Allied]] (mainly American and British) bombers. Development was cancelled due to lack of funds and official support.<ref name="Arado">Krantzhoff, pp. 153–156</ref><ref name="KaySmith">Kay and Smith, p. 388</ref><ref>Albrecht, p.101</ref>

There were three proposed variants; each had fuel capacity for only two target runs, after which the pilot would have been required to glide without power to a landing on under-belly skids.<ref name="KaySmith"/>  To survive close pursuits, the E.381 was designed with the narrowest frontal [[cross section (geometry)|cross section]] possible to increase its chances of surviving shots from the front. This also forced the pilot to lie in a prone position. The cross-section was {{convert|0.45|m2|sqft|sp=us}}, or approximately a quarter of the cross section of the [[Messerschmitt Bf 109]].

==Development==
[[File:Arado 234+381 parasite aircraft.jpg|thumb|left|An Arado E.381 suspended under the belly of the Ar 234C mother ship]]

Near the end of [[World War II]], in December 1944, the German aircraft manufacturers [[Arado Flugzeugwerke|Arado]], [[BMW]], [[Gothaer Waggonfabrik|Gotha]], [[Heinkel]], [[Henschel & Son|Henschel]], and [[Luftschiffbau Zeppelin|Zeppelin]] submitted design proposals for small rocket- or jet-powered aircraft intended for pursuit or ground attack duties. All these proposals exploited the Luftwaffe's concept of "gaining a tactical advantage by placing excessive stress on the man in the cockpit [the German pilot]".<ref name=a103>Albrecht, p. 103</ref> The [[g-forces]] envisioned in these proposals were feasible for aircraft structures but exceeded human capabilities in a normal sitting position. The designers attempted to alleviate this constraint by placing the pilot in the [[prone position]], which increased the sustainable g-force limit. This also allowed a reduction in fuselage size, weight, and [[Drag (physics)|drag]]. A smaller cross section also decreased the likelihood of being hit by enemy gunners, and Arado exploited this opportunity to the fullest. According to their "specific design philosophy",<ref name=a103/> the fighter was designed to fly close to [[Combat box|bomber formations]] and open fire from its [[MK 108 cannon]] at point-blank range.<ref name=a103/>

The E.381 began in a proposal from ''Arado Flugzeugwerke'' to the [[Air Ministry (Germany)|Air Ministry]] for a [[parasite fighter]], carried underneath another aircraft, to destroy Allied bombers.<ref name="KaySmith"/> Three variants of the E.381, named Mark I, II and III, were designed. Each version was essentially an [[armor]]ed tube provided with [[armament]] and a [[Walter HWK 109-509]] [[rocket engine]] for power. The aircraft would have carried enough fuel for two approaches to the target as well as only sixty<ref name="Lepage"/> (some say forty-five) {{convert|30|mm|in|abbr=on}} rounds.<ref name=a103/> After using all his fuel during an attack it was intended that the pilot would [[Gliding|glide]] the fighter to the ground, deploy its [[drogue parachute]], and land the aircraft on a primitive skid [[landing gear]].<ref name=a103/>  None of the designs were ever completed due to its cancellation, though some wooden airframes and a single mockup were constructed in 1944 to provide prone-position training for pilots. The E.381 was cancelled due to a lack of funds and interest by the [[Ministry of Aviation (Germany)|Ministry of Aviation]], along with a scarcity of mother Ar 234 aircraft<ref name=Arado/><ref name=herwig207/><ref name="Ford">Ford, p. 17</ref><ref name="Rocket">Green, pp. 145–146</ref> — the [[Arado Ar 234#Ar 234C|Arado Ar 234C]] four-[[BMW 003]] jet engined aircraft intended for this purpose was never able to be flight tested before the war's end.

==Variants==
[[File:Arado E.381 II (en).svg|thumb|500px|An "x-ray" sideview drawing of the Mark II's fuselage interior. The main features outlined are present in all versions.|alt=A line drawing of the Mark II. The features outlined here are present in all models. A 5 millimeter armor shell protects most of the fuselage, while the windshield is made up of 145 millimeter bullet-proof glass, and is backed by a 140 millimeter glass screen. Above the pilot's body is a hump, containing a 30 millimeter cannon with 45 rounds. Around the pilot's body are the tanks of C-Stoff fuel and behind the pilot's feet are the T-Stoff oxidizer tanks. Behind the T-Stoff fuel are a parachute to slow the plane on landing and the engine.]]
{| class="wikitable sortable"
|+ Comparison table<ref>{{cite web|url=http://www.luft46.com/arado/are381.html|title=Arado Ar E.381|work=Luft '46 – WWII German Aircraft Projects |accessdate=October 4, 2011|author=Johnson, Dan}}</ref>
|-
! scope="col" | Version/Mark
! scope="col" | Length
! scope="col" | Wingspan
! scope="col" | Height
! scope="col" | Wing area
! scope="col" | Empty weight
! scope="col" | Loaded weight
! scope="col" | Wing load
! scope="col" | Maximum speed
|-
| I || {{convert|4.69|m|ft|abbr=on}} || {{convert|4.43|m|ft|abbr=on}} || {{convert|1.29|m|ft|abbr=on}} || {{convert|5|m2|ft2|abbr=on}} || {{convert|830|kg|lb|abbr=on}} || {{convert|1200|kg|lb|abbr=on}} || 240&nbsp;kg/m<sup>2</sup> (49.1&nbsp;lb/ft<sup>2</sup>) || {{convert|900|km/h|mph|abbr=on}}
|-
| II || {{convert|4.95|m|ft|abbr=on}} || {{convert|5|m|ft|abbr=on}} || {{convert|1.15|m|ft|abbr=on}} || {{convert|5|m2|ft2|abbr=on}} || {{convert|0|kg|lb|abbr=on}} || {{convert|1265|kg|lb|abbr=on}} || 235&nbsp;kg/m<sup>2</sup> (51.8&nbsp;lb/ft<sup>2</sup>) || {{convert|885|km/h|mph|abbr=on}}
|-
| III || {{convert|5.7|m|ft|abbr=on}} || {{convert|5.05|m|ft|abbr=on}} || {{convert|1.51|m|ft|abbr=on}} || {{convert|5.5|m2|ft2|abbr=on}} || {{convert|0|kg|lb|abbr=on}} || {{convert|1500|kg|lb|abbr=on}} || 272&nbsp;kg/m<sup>2</sup> (55.8&nbsp;lb/ft<sup>2</sup>) || {{convert|895|km/h|mph|abbr=on}}
|}

===Arado E.381/I===
[[File:Arado E.381 I 3d.svg|right|thumb|Arado E.381/I|alt=A graphic view of the Arado E.381]]
The first design, the Mark I, had a [[fuselage]] with a circular cross section and a small round window in the nose for pilot vision. A {{convert|5|mm|in|adj=on|sp=us}} armored shell protected most of the fuselage. The pilot would have been in a prone position in the very cramped [[cockpit]] (the cross-section was {{convert|0.45|m2|sqft|sp=us}}, or approximately a quarter of the cross section of the [[Messerschmitt Bf 109]].{{#tag:ref|This figure is from Arado. The Bf 109 had a cross section of {{convert|1.8|m2|sqft|sp=us}}<ref name=a103/><ref name=hr103>Herwig and Rode, p. 206</ref>|group=nb}}) behind a removable {{convert|140|mm|in|adj=on|sp=us}} [[bullet-resistant glass]] screen mounted in front of the pilot. Two small bulges were located on the sides of the fuselage for the pilot's elbows. Three [[C-Stoff]] tanks surrounded the pilot, with the [[T-Stoff]] oxidizer tank in the center section between the pilot and the engine. The aircraft had straight wings mounted at the top of the aircraft. In the dorsal area (at the wing mounts) the fuselage humped to accommodate a  blister for a single MK 108 {{convert|30|mm|in|abbr=on}} cannon and 60 (other writers say 45) rounds.<ref name=a103/><ref name="Lepage">Lepage, pp. 257–258</ref> The [[Walter HWK 109-509]]A<ref name=a103/> single-chamber rocket engine was mounted beneath the aft fuselage, which also carried a [[Empennage#Fins|twin-fin empennage]] and the drogue parachute housing.<ref name="KaySmith"/><ref name=a103/>

Landing the aircraft required the extension of the retractable landing skid and the deployment of a braking drogue parachute. As pilots could only access the plane from a hatch above the cockpit, the pilot would have to enter the E.381 before it could be attached to the carrier [[Arado Ar 234#Ar 234C|Ar 234C]] and had no way to escape in case of an emergency while attached to the carrier.<ref name="Griehl">Griehl, pp. 150–155</ref>

===Arado E.381/II===
The second design, the Mark II, was very similar to the Mark I, aside from being larger and having smaller fins<ref name="Arado"/> The variant was planned to have a deeper and shorter {{convert|5|m|ftin|abbr=on}} fuselage and a high mid-wing layout. It was to be powered by a [[Walter HWK 109-509|Walter HWK 109-509 A-2]] engine. The unit was rated at {{convert|1700|kg|lb|abbr=on}} of thrust. About a quarter of the way back from the nose the fuselage deepened in the form of a hump which extended to the tail. This hump housed a single MK 108 cannon with 45 rounds.<ref name="KaySmith"/><ref name=a103/>

===Arado E.381/III===
The third design, the Mark III, was also similar to the Mark I, aside from being larger than any of the preceding variants. The circular cross section of the previous variants became more triangular and the {{convert|30|mm|sp=us|adj=on}} [[MK 108 cannon]] was replaced with six rockets of an unspecified type. Although the landing procedure was unchanged, a [[trapdoor|hatch]] was added on the side to provide for simpler pilot entry and exit.<ref name="Arado"/>

==Specifications (E.381/I)==
[[File:Arado E.381 I 3v.svg|right|300px|alt=A three-view of an Arado E.381/I|thumb|A three-view of an Arado E.381/I]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank.

-->
|ref=''Aircraft of the Luftwaffe 1935–1945: An Illustrated History''<ref name="Lepage"/> for the Arado E.381/I
<!--
        General characteristics
-->
|crew=1 pilot
|capacity=
|payload main=
|payload alt=
|payload more=
|length main= 4.69&nbsp;meters
|length alt=15&nbsp;ft 4&nbsp;in
|span main=4.43&nbsp;meters
|span alt=14&nbsp;ft 6&nbsp;in
|height main=1.29&nbsp;meters
|height alt=4&nbsp;ft 3&nbsp;in
|area main=
|area alt=
|airfoil=
|aspect ratio=
|empty weight main=830&nbsp;kg
|empty weight alt=
|loaded weight main=1200&nbsp;kg
|loaded weight alt=
|useful load main=
|useful load alt=
|max takeoff weight main=N/A
|max takeoff weight alt=
|more general=
<!--
        Powerplant
-->
|engine (jet)= [[Walter HWK 109-509]]A
|type of jet=[[rocket]]
|number of jets=1
|thrust main=
|thrust alt=
|thrust original=
|afterburning thrust main=
|afterburning thrust alt=
|engine (prop)=
|type of prop=
|number of props=
|power main=
|power alt=
|power original=
|propeller or rotor?=<!-- options: propeller/rotor -->
|propellers=
|number of propellers per engine=
|propeller diameter main=
|propeller diameter alt=
<!--
        Performance
-->
|max speed main= 900&nbsp;kilometers per hour
|max speed alt= 559&nbsp;mph
|max speed more=
|cruise speed main=
|cruise speed alt=
|cruise speed more=
|stall speed main=
|stall speed alt=
|stall speed more=
|never exceed speed main=
|never exceed speed alt=
|range main=
|range <style>span.GerbrantEditRegexReplaceHit{font-weight:bold;background:lightsteelblue}span.GerbrantEditRegexReplaceHitOff{font-weight:bold;background:mistyrose}span.GerbrantEditRegexReplaceMaskFailed{font-weight:normal;color:red}</style>alt=
|range more=
|combat radius main=
|combat radius alt=
|combat radius more=
|ferry range main=
|ferry range alt=
|ferry range more=
|endurance=
|ceiling main=
|ceiling alt=
|ceiling more=
|climb rate main=
|climb rate alt=
|climb rate more=
|sink rate main=
|sink rate alt=
|sink rate more=
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
<!--
        Armament
-->
|guns= 1 × {{convert|30|mm|sp=us|adj=on}} [[MK 108 cannon]]
|bombs=
|rockets=6 × RZ73 (Mark III only)<ref name=herwig207/>
|missiles=
|hardpoints=
|hardpoint capacity=
|avionics=

}}

==See also==
{{Portal|Aviation|World War II}}
*[[Emergency Fighter Program]]
*[[List of rocket aircraft]]
*[[Sombold So 344]]

==Notes==
{{commons category|Arado E.381}}
{{Reflist|group=nb}}

==References==
{{Reflist|colwidth=30em}}

==Bibliography==
{{refbegin}}
*{{Cite book|last=Albrecht |first=Ulrich |editor1-first=Monika |editor1-last=Renneberg|pages=88&ndash;125 |editor2-first=Mark |editor2-last=Walker|title=Science, Technology, and National Socialism |publisher=Cambridge University Press|year=2002|chapter=Military Technology and National Socialist Ideology |isbn=0-521-52860-7|location=Cambridge, UK}}
*{{Cite book|last=Ford|first=Roger|title=Germany's Secret Weapons in World War II|publisher=MBI Publishing Co.|year=2000|isbn=0-7603-0847-0|location=Osceola, WI}}
*{{Cite book|last=Green|first=William|series=Ballantine's Illustrated History of World War II: Weapons Book, No. 20|publisher=Ballantine Books|year=1971|title=Rocket Fighter|location=New York, NY|isbn=978-0-345-02163-2}}
*{{Cite book|last=Griehl|first=Manfred|title=Jet Planes of the Third Reich: The Secret Projects, Volume 1|publisher=Monogram Aviation Publications|year=1998|location=Sturbridge, MA|isbn=978-0-914144-36-6}}
*{{Cite book|last1=Herwig|first1=Dieter|last2=Rode|first2=Heinz|title=Luftwaffe Secret Projects: Ground Attack & Special Purpose Aircraft|year=2003|publisher=Midland Publishing|isbn=1-85780-150-4|location=Hinckley, UK}}
*{{Cite book|last1=Kay|first1=Antony L.|last2=Smith|first2=J.R.|title=German Aircraft of the Second World War|publisher=Naval Institute Press|year=2002|isbn=1-55750-010-X|location=Annapolis, MD}}
*{{Cite book|last=Krantzhoff|first=Jörg Armin|title=Arado: History of an Aircraft Company|publisher=Schiffer |year=1997|isbn=0-7643-0293-0|location=Atglen, PA}}
*{{Cite book|last=Lepage|first=Jean-Denis|title=Aircraft of the Luftwaffe 1935–1945: An Illustrated History|year=2009|publisher=McFarland|isbn=0-7864-3937-8|url=https://books.google.com/books?id=hdQBTcscxyQC&pg=PA258&dq=%22Arado+Ar+E.381%22&q|location=Jefferson, NC|accessdate=2013-12-13}}
{{refend}}

{{Arado aircraft}}
{{Good article}}

[[Category:Arado aircraft|E.381]]
[[Category:Abandoned military aircraft projects of Germany]]
[[Category:Rocket-powered aircraft]]
[[Category:Monoplanes]]
[[Category:Parasite aircraft]]