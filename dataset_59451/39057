{{for|the bryological journal|Buxbaumia (journal)}}
{{Italictitle}}
{{Taxobox
| name = ''Buxbaumia''
| image = Buxbaumia viridis 060408b.jpg
| image_width = 240px
| image_caption = ''Buxbaumia viridis'' 
| regnum = [[Plant]]ae
| divisio = [[Moss|Bryophyta]]
| classis = [[Bryopsida]]
| subclassis = '''Buxbaumiidae'''
| subclassis_authority = Doweld
| ordo = '''Buxbaumiales'''
| ordo_authority = M. Fleisch.
| familia = '''Buxbaumiaceae'''
| familia_authority = [[Schimp.]]
| genus = '''''Buxbaumia'''''
| genus_authority = [[Hedw.]], 1801<ref name="Hedwig">{{cite book | last=Hedwig | first=Johann | year=1801 | title=Species Muscorum frondosorum descriptae et tabulis aeneis lxxvii | page=166 | location=Leipzig }}</ref>
| type_species = ''B. aphylla''
| type_species_authority = Hedw.
| subdivision_ranks = Species
| subdivision = 
''See [[#Classification|Classification]]''
}}
'''''Buxbaumia''''' ('''Bug moss''', '''Bug-on-a-stick''', '''Humpbacked elves''', or '''Elf-cap moss''')<ref name="Bold" />  is a genus of twelve species of [[moss]] (Bryophyta). It was first named in 1742 by [[Albrecht von Haller]] and later brought into modern botanical nomenclature in 1801 by [[Johann Hedwig]]<ref name="Schofield 1985" /> to commemorate [[Johann Christian Buxbaum]], a German physician and botanist who discovered the moss in 1712 at the mouth of the [[Volga River]].<ref name="Bold" />  The moss is microscopic for most of its existence, and plants are noticeable only after they begin to produce their reproductive structures. The asymmetrical spore capsule has a distinctive shape and structure, some features of which appear to be transitional from those in primitive mosses to most modern mosses.

==Description==
Plants of ''Buxbaumia'' have a much reduced [[gametophyte]], bearing a [[sporophyte]] that is enormous by comparison.<ref name="Porley">{{cite book 
| last= Porley | first=Ron |author2=Nick Hodgetts | year=2005 
| title=Mosses and Liverworts | page=13 
| location=London | publisher=Collins | isbn=0-00-220212-3 }}</ref>  In most mosses, the gametophyte stage of the [[alternation of generations|life cycle]] is both green and leafy, and is substantially larger than the spore-producing stage.  Unlike these other mosses, the gametophyte of ''Buxbaumia'' is microscopic, colorless, stemless, and nearly leafless.<ref name="Campbell" /><ref name="Schofield 2007">{{cite book 
| last=Schofield | first=W. B. | year=2007 
| chapter=Buxbaumiaceae | url=http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10137
| editor=Flora of North America Editorial Committee (eds.)
| title=Flora of North America | volume=27 | pages=118–120 
| location=New York & Oxford | publisher=Oxford University Press 
| isbn=978-0-19-531823-4 }}</ref>  It consists exclusively of thread-like [[protonema]]ta for most of its existence, resembling a thin green-black felt on the surface where it grows.<ref name="Marshall" />  The plants are [[monoicous|dioicous]], with separate plants producing the male and female organs.<ref name="Crum & Anderson" />  Male plants develop only one microscopic leaf around each [[antheridium]],<ref name="Bold">{{cite book 
| last=Bold | first=Harold C. |author2=Constantine J. Alexopoulos |author3= Theodore Delevoryas  | year=1987 
| title=Morphology of Plants and Fungi | edition=5th | pages=270, 303 
| location=New York | publisher=Harper & Row 
| isbn=0-06-040839-1 }}</ref><ref name="Campbell" /> and female plants produce just three or four tiny colorless leaves around each [[archegonium]].<ref name="Schofield 1985" />

Because of its small size, the gametophyte stage is not generally noticed until the stalked [[sporangium]] develops, and is locatable principally because the sporangium grows upon and above the tiny gametophyte.<ref name="Schofield 2007" />  The extremely reduced state of ''Buxbaumia'' plants raises the question of how it makes or obtains sufficient nutrition for survival.  In contrast to most mosses, ''Buxbaumia'' does not produce abundant [[chlorophyll]] and is [[saprophytic]].<ref name="Campbell">{{cite book 
| last=Campbell | first=Douglas H. | year=1918 
| title=The Structure and Development of Mosses and Ferns | edition=3rd | pages=8, 160–166, 220, 225–226  
| location=London | publisher=The Macmillan Co. }}</ref>  It is possible that some of its nutritional needs are met by [[fungi]] that grow within the plant.<ref name="Schofield 1985">{{cite book 
| last=Schofield | first=W. B. | year=1985 
| title=Introduction to Bryology | pages= 74–83, 404, 411 
| location=New York | publisher=Macmillan | isbn=0-02-949660-8 }}</ref>

The sporophyte at maturity is between 4 and 11&nbsp;mm tall.<ref name="Bold" /> The [[spore]] capsule is attached at the top of the stalk and is distinctive,<ref name="Schofield 2007" /> being asymmetric in shape and oblique in attachment.<ref name="Redfearn">{{cite book 
| last=Conard | first=Henry S. |author2=Paul L. Redfearn, Jr. | year=1979 
| title=How to know the mosses and liverworts | edition=2nd | page=222
| location=Dubuque, IA | publisher=Wm. C. Brown | isbn=0-697-04768-7 }}</ref>  As with most other Bryopsida, the opening through which the spores are released is surrounded by a double [[peristome]] (diplolepidious) formed from the [[cell wall]]s of disintegrated cells.<ref name="Vitt 1984" />  The exostome (outer row) consists of 16 short articulated "teeth".  Unlike most other mosses, the endostome (inner row) does not divide into teeth, but rather is a continuous pleated membrane around the capsule opening.<ref name="Edwards 1984">{{cite book 
| last=Edwards | first=S. R. | year=1984 
| chapter=Homologies and Inter-relationships of Moss Peristomes | pages=658–695 
| title=New Manual of Bryology | volume=2 | editor=R. M. Schuster (ed.) 
| location=Tokyo | publisher=The Hattori Botanical Laboratory | id={{Listed Invalid ISBN|49381633045}} }}</ref>  Only the genus ''[[Diphyscium]]'' has a similar peristome structure, although that genus has only 16 pleats in its endostome, in contrast to the 32 pleats in ''Buxbaumia''.<ref name="Schofield 1985" /><ref name="Vitt 1984">{{cite book 
| last=Vitt | first=Dale H. | year=1984 
| chapter=Classification of the Bryopsida | pages=696–759 
| title=New Manual of Bryology | volume=2 | editor=R. M. Schuster (ed.) 
| location=Tokyo | publisher=The Hattori Botanical Laboratory | id={{Listed Invalid ISBN|49381633045}} }}</ref>  ''Diphyscium'' shares with ''Buxbaumia'' one other oddity of the sporophyte; the foot (stalk base) ramifies as a result of outgrowths, so much so that they may be mistaken for [[rhizoid]]s.<ref name="Chopra">{{cite book 
| last=Chopra | first=R. N. |author2=P. K. Kumra | year=1988 
| title=Biology of Bryophytes | page=167 
| location=New York | publisher=John Wiley & Sons | isbn=0-470-21359-0 }}</ref>

==Distribution and ecology==
[[File:Buxbaumiaaphylla.jpg|thumb|240px|Sporophytes of ''Buxbaumia aphylla'' growing among other mosses. None of the visible leaves belong to ''Buxbaumia'', which is a stemless and nearly leafless plant.]]
Species of ''Buxbaumia'' may be found across much of the temperate to subarctic regions of the [[Northern Hemisphere]], as well as cooler regions of [[Australia]] and [[New Zealand]].<ref name="Schofield 2007" /><ref name="Crum & Anderson" /><ref name="Tan 1984">{{cite book 
| last=Tan | first=Benito C. |author2=Tamás Pócs | year=2000  
| chapter=Bryogeography and conservation of bryophytes | pages=403–448 
| editor=A. Jonathan Shaw & Bernard Goffinet (eds.) | title=Bryophyte Biology | edition=1st 
| location= Cambridge | publisher=Cambridge University Press | isbn=0-521-66097-1 }}</ref><ref name="Stone">{{cite journal 
| last=Stone | first=I. G. | year=1983 
| title=''Buxbaumia'' in Australia, including one new species, ''B. thornsborneae'' 
| journal=Journal of Bryology | volume=12 | pages=541–552 | doi=10.1179/jbr.1983.12.4.541}}</ref>  

The moss is an [[annual plant|annual]] or [[biennial plant]] and grows in [[Disturbance (ecology)|disturbed]] habitats or as a [[pioneer species]].<ref name="Crum & Anderson">{{cite book 
| last=Crum | first=Howard A. |author2=Lewis E. Anderson | year=1980 
| title=Mosses of Eastern North America | volume=2 | pages=1231, 1234–1236 
| location=New York | publisher=Columbia University Press | isbn=0-231-04516-6 }}</ref><ref name="Sullivant">{{cite book 
| last=Sullivant | first=William S. | year=1856 
| chapter=The Musci and Hepaticae of the U. S. east of the Mississippi River | editor=Asa Gray | title=Manual of Botany | edition=2nd | pages=639–640 
| location=New York | publisher=George P. Putnam & Co. }}</ref> The plants grow on decaying wood, rock outcrops, or directly on the soil.<ref name="Schofield 2007" /><ref name="Marshall">{{cite book 
| last=Marshall | first=Nina L. | year=1907 
| title=Mosses and Lichens | pages=57, 260–262 
| location=New York | publisher=Doubleday, Page & Company }}</ref>  They do not grow regularly or reliably at given locations, and frequently disappear from places where they have previously been found.<ref name="Marshall" />  Sporophyte stages begin their development in the autumn, and are green through the winter months.<ref name="Marshall" />  [[Spore]]s are mature and ready for dispersal by the late spring or early summer.<ref name="Schofield 2007" /><ref name="Crum & Anderson" />  The spores are ejected from the capsule in puffs when raindrops fall upon the capsule's flattened top.<ref name="Crum & Anderson" />

The asymmetric sporophytes of ''[[Buxbaumia aphylla]]'' develop so that the opening is oriented towards the strongest source of light, usually towards the south.<ref name="Crum & Anderson" />  The species often grows together with the diminutive [[Marchantiophyta|liverwort]] ''[[Cephaloziella]]'', which forms a blackish crust that is easier to spot than ''Buxbaumia'' itself.<ref name="Crum & Anderson" />

==Classification==
''Buxbaumia'' is the only genus in the family Buxbaumiaceae, the order Buxbaumiales, and the subclass Buxbaumiidae.<ref name="Goffinet 2008" />  It is the [[Cladistics#Clades|sister group]] to all other members of class [[Bryopsida]].<ref name="Mishler & Churchill">{{cite journal 
| last=Mishler | first=B. D. |author2=S. P. Churchill | year=1984 
| title=A cladistic approach to the phylogeny of the "bryophytes" 
| journal=Brittonia | volume=36 | pages=406–424 
| doi=10.2307/2806602 
| issue=4 
| publisher=New York Botanical Garden Press 
| jstor=2806602 }}</ref><ref name="Goffinet & Buck 2004" />  Some older classifications included the [[Diphysciaceae]] within the Buxbaumiales (or as part of the Buxbaumiaceae) because of similarities in the [[peristome]] structure,<ref name="Schofield 1985" /><ref name="Vitt 1984" /> or placed the Buxbaumiaceae in the [[Tetraphidales]].<ref name="Buck 2000">{{cite book 
| last=Buck | first=William R. |author2=Bernard Goffinet | year=2000  
| chapter=Morphology and classification of mosses | pages=71–123 
| editor=A. Jonathan Shaw & Bernard Goffinet (eds.) | title=Bryophyte Biology | edition=1st 
| location= Cambridge | publisher=Cambridge University Press | isbn=0-521-66097-1 }}</ref> Most recent [[cladistics|cladistic]] studies using [[nucleic acid sequence|DNA sequences]] are not conclusive regarding the relationship between ''Buxbaumia'' and ''Diphyscium'', but evidence suggests they are separate lines of a [[paraphyly|paraphyletic]] group.<ref name="Magombo">{{cite journal | last=Magombo | first=Z. L. K. | year=2003 | title=The phylogeny of basal peristomate mosses: Evidence from ''cp''DNA, and implications for peristome evolution | journal=Systematic Botany | volume=28 | pages=24–38 }}</ref> No recent studies favor a placement with the Tetraphidales.<ref name="Goffinet & Buck 2004" />

The genus ''Buxbaumia'' includes twelve species:

{| align="left" style="text-align:left; padding:2.5px; background:#eef"
|-
| style="background:#fff; padding:2.5px" |genus '''''Buxbaumia''''' 
: ''[[Buxbaumia aphylla]]''
: ''[[Buxbaumia colyerae]]''
: ''[[Buxbaumia himalayensis]]''
: ''[[Buxbaumia javanica]]''
: ''[[Buxbaumia minakatae]]''
: ''[[Buxbaumia novae-zelandiae]]''
: ''[[Buxbaumia piperi]]''
: ''[[Buxbaumia punctata]]''
: ''[[Buxbaumia symmetrica]]''
: ''[[Buxbaumia tasmanica]]''
: ''[[Buxbaumia thorsborneae]]''
: ''[[Buxbaumia viridis]]''
|{{clade| style=font-size:75%;line-height:75%
|1={{clade
	|1=[[Oedipodiopsida]]
	|2={{clade
		|1={{clade
			|label3=[[Bryopsida]]
			|1=[[Tetraphidopsida]]
			|2=[[Polytrichopsida]]
			|3={{clade
				|1='''''Buxbaumia'''''
				|2={{clade
					|1=[[Diphysciidae]]
					|2={{clade
						|1=[[Timmiidae]]
						|2=[[Funariidae]]
						|3=[[Dicranidae]]
						|4=[[Bryidae]]
						}}
					}}
				}}
			}}
		}}
	}}
}}
|-style="font-size:90%;"
| colspan=2 | The species and [[phylogenetics|phylogenetic]] position of ''Buxbaumia''.<ref name="Goffinet 2008">{{cite book 
| last=Goffinet| first=B. |author2=W. R. Buck |author3= A. J. Shaw  | year=2008  
| chapter=Morphology and Classification of the Bryophyta | pages=55–138 
| editor=Bernard Goffinet & A. Jonathan Shaw (eds.) | title=Bryophyte Biology | edition=2nd 
| location= Cambridge | publisher=Cambridge University Press | isbn=978-0-521-87225-6 }}</ref><ref name="Goffinet & Buck 2004">{{cite journal | last=Goffinet | first = Bernard |author2=William R. Buck | year=2004 | title=Systematics of the Bryophyta (Mosses): From molecules to a revised classification | series=Molecular Systematics of Bryophytes | journal=Monographs in Systematic Botany | volume=98 | pages=205–239 | publisher= Missouri Botanical Garden Press | isbn = 1-930723-38-5 }}</ref> 
|}{{-}}

Because of the simplicity of its structure, Goebel interpreted ''Buxbaumia'' as a primitive moss, transitional between the [[algae]] and mosses,<ref name="Campbell" /> but subsequent research suggests that it is a secondarily reduced form.<ref name="Crum & Anderson" /><ref name="Goffinet & Buck 2004" />  The unusual peristome in ''Buxbaumia'' is now thought to be a transitional form between the nematodontous (cellular teeth) peristome of the [[Polytrichopsida]] and the arthrodontous (cell wall teeth) peristome of the Bryopsida.<ref name="Goffinet & Buck 2004" />

==References==
{{Reflist}}

== External links ==
{{commons category|Buxbaumia}}
{{Wikispecies|Buxbaumia}}
* W. B. Schofield. 2004. ''Bryophyte Flora of North America'': [http://www.mobot.org/plantscience/BFNA/V1/BuxbBuxbaumiaceae.htm  Buxbaumiaceae]

{{good article}}

[[Category:Moss genera]]
[[Category:Bryopsida]]