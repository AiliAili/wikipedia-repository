{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Lost in Blunderland: The further adventures of Clara
| title_orig   =
| translator   =
| image         = Lost-in-blunderland-cover-1903.png <!--prefer 1st edition - It is the first edition-->
| caption = First edition cover of ''Lost in Blunderland''
| author       = [[Edward Harold Begbie|Caroline Lewis]]
| cover_artist =
| country       = United Kingdom
| language      = English
| genre        = [[Fantasy novel]], [[Parody]]
| publisher    = [[William Heinemann]]
| release_date = 1903
| media_type   = Print (hardback)
| pages        = xvi, 150
| preceded_by  = [[Clara in Blunderland]]
| followed_by  =
}}

'''''Lost in Blunderland: The further adventures of Clara'''''  is a novel by Caroline Lewis (pseudonym for [[Edward Harold Begbie]], J. Stafford Ransome, and M. H. Temple), written in 1903 and published by William Heinemann of London. It is a political [[parody]] of [[Lewis Carroll]]'s two books, ''[[Alice's Adventures in Wonderland]]'' and ''[[Through the Looking-Glass]]''. It is the second of Lewis' parodies, the first being ''[[Clara in Blunderland]]''.

It is critical of the early administration of [[Prime Minister of the United Kingdom|Prime Minister]] [[Arthur Balfour]], who is represented by a little girl named Clara.<ref>Sigler, Carolyn, ed. 1997. ''Alternative Alices: Visions and Revisions of Lewis Carroll's "Alice" Books.'' Lexington, KY, University Press of Kentucky. Pp. 340-347</ref> A number of other notable British politicians are identified in the book. The [[Red Queen (Through the Looking Glass)|Red Queen]] is [[Joseph Chamberlain]] and [[Humpty Dumpty#In Through the Looking-Glass|Crumpty-Bumpty]] is [[Henry Campbell-Bannerman]]. There are additional characters, such as [[the Lion and the Unicorn]], representing Britain and Germany respectively.

The book features 50 drawings after the originals by [[John Tenniel]] which were drawn by journalist J. Stafford Ransome, credited as "S.R.".

[[File:Lost-in-blunderland-cover-2010.png|150px|left|thumb|2010 edition cover of ''Lost in Blunderland'']]

==Bibliography==
*Lewis, Caroline (2010) [http://www.evertype.com/books/lost.html ''Lost in Blunderland: The further adventures of Clara'']. Evertype. ISBN 978-1-904808-50-3

==Notes==
{{Reflist}}

{{alice}}

[[Category:1903 novels]]
[[Category:Books based on Alice in Wonderland]]
[[Category:British fantasy novels]]
[[Category:Works published under a pseudonym]]
[[Category:Novels by Edward Harold Begbie]]


{{1900s-fantasy-novel-stub}}