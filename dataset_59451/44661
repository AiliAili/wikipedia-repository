{{Use mdy dates|date=February 2013}}
{{Infobox journal
| cover = [[File:Jan2006AJSCover.jpg|200px]]
| editor = [[Andrew Abbott]]
| discipline = [[Sociology]]
| abbreviation = Am. J. Sociol.
| publisher = [[University of Chicago Press]]
| country = United States
| history = 1895–present
| frequency = Bimonthly
| impact = 2.574
| impact-year = 2015
| website = http://www.journals.uchicago.edu/ajs
| link1 = http://www.journals.uchicago.edu/toc/ajs/current
| link1-name = Online access
| link2 = http://www.journals.uchicago.edu/loi/ajs
| link2-name = Online archive
| ISSN = 0002-9602
| eISSN = 1537-5390
| JSTOR = 00029602
| OCLC = 42017129
| LCCN = 05031884
| CODEN = AJSOAR
}}
The '''''American Journal of Sociology''''' was established in 1895 by [[Albion Woodbury Small|Albion Small]] and is the oldest [[academic journal]] of [[sociology]] in the United States. The journal is attached to the University of Chicago's sociology department and it is published bimonthly by [[List of University of Chicago Press journals|The University of Chicago Press]]. Its [[editor-in-chief]] is [[Andrew Abbott]] ([[University of Chicago]]). The journal presents work on the theory, methods, practice, and history of sociology. It also publishes sociology-related papers by scholars from outside sociology, speaking to sociologists, social scientists, and the general sociological reader. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.574, ranking it 7th out of 142 journals in the category "Sociology".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Sociology |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== Past editors ==
Past editors-in-chief of the journal have been:
{{columns-list|colwidth=30em|
* [[Albion Woodbury Small|Albion Small]] (1895–1926)
* [[Ellsworth Faris]] (1933–1936)
* [[Ernest Burgess]] (1936–1940)
* [[Herbert Blumer]] (1940–1952)
* [[Everett Hughes (sociologist)|Everett Hughes]] (1952–1957)
* [[Peter Rossi]] (1957–1958)
* [[Everett Hughes (sociologist)|Everett Hughes]] (1959–1960)
* [[Peter Blau]] (1960–1966)
* [[C. Arnold Anderson]] (1966–1973)
* [[Charles Bidwell]] (1973–1978)
* [[Edward Laumann]] (1978–1984)
* [[William Parish]] (1984–1992)
* [[Marta Tienda]] (1992–1996)
* [[Edward Laumann]] (1996–1997)
* [[Roger V. Gould]] (1997–2000)
* [[Andrew Abbott]] (2000–2016)
* [[Elisabeth S. Clemens]] (2016–present)
}}
From 1926 to 1933, the journal was co-edited by a number of different members of the University of Chicago faculty including [[Ellsworth Faris]], [[Robert E. Park]], [[Ernest Burgess]], [[Fay-Cooper Cole]], [[Marion Talbot]], [[Frederick Starr]], [[Edward Sapir]], [[Louis Wirth]], [[Eyler Simpson]], [[Edward Webster (sociologist)|Edward Webster]], [[Edwin Sutherland]], [[William Fielding Ogburn|William Ogburn]], [[Herbert Blumer]], and [[Robert Redfield]].

== Roger V. Gould Prize ==
In 2002, the ''American Journal of Sociology'' created the Roger V. Gould prize in memory of its former editor. The $1,000 prize is awarded annually at the American Sociological Association annual meeting to the paper from the previous volume of the journal that most "clearly embodies Roger’s ideals as a sociologist: clarity, rigor, and scientific ambition combined with imagination on the one hand and a sure sense of empirical interest, importance, and accuracy on the other."<ref>{{cite journal| last1=Abbott| first1=Andrew|date=March 2002| title=Roger V. Gould, 1966–2002| journal=American Journal of Sociology| volume=107| issue=5| pages=ii–iii| location=Chicago| publisher=University of Chicago Press| jstor=10| doi=10.1086/344090}}</ref> Winners include [[Peter Bearman]], [[John Levi Martin]], [[Michael J. Rosenfeld]], [[Elizabeth E. Bruch]], [[Robert D. Mare]], [[Shelley Correll]], and [[Roberto Garvía]].

== References ==
{{Reflist}}

== Further reading ==
* {{cite book| last=Abbott| first=Andrew| title=Department and Discipline: Chicago Sociology at One Hundred| location=Chicago| publisher=[[University of Chicago Press]]| year=1999| isbn=978-0-226-00099-2}}
* {{cite journal| last1=Shanas| first1=Ethel|date=May 1945| title=The ''American Journal of Sociology'' Through Fifty Years| journal=American Journal of Sociology| volume=50| issue=6| pages=522–533| location = Chicago| publisher=University of Chicago Press| jstor=2771397| doi=10.1086/219693}}
* {{cite journal| last1=Tienda| first1=Marta|date=July 1994| title=Editor's Note| journal=American Journal of Sociology| volume=100| issue=1| pages=vii–viii| location=Chicago| publisher=University of Chicago Press| jstor=2782534| doi=10.1086/230496}}

== External links ==
{{Wikisource|The American Journal of Sociology}}
{{Commons category|The American Journal of Sociology}}
* {{Official website|http://www.journals.uchicago.edu/ajs}}

[[Category:Sociology journals]]
[[Category:University of Chicago Press academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1895]]