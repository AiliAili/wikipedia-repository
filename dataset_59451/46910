{{Infobox company
| name = Cenikor Foundation
| logo = [[File:Cenikor Logo Blue with tagline.jpg|225px]]
| type = [[Non-profit]]
| genre = [[Behavioral health]], [[drug]] and [[alcohol addiction]], [[mental illness]] and [[intervention (counseling)|intervention]]
| foundation = [[Lakewood, Colorado]], [[United States|U.S.]] ({{Start date|1967}})
| location_city = [[Houston, TX]], [[Texas (U.S. state)|Texas]]
| locations = 7 facilities
| key_people = Bill Bailey <small>(President and CEO)</small>
| num_employees = 200 (estimate)
| homepage = {{URL|http://www.cenikor.org}}
}}
The '''Cenikor Foundation''' is a private, [[Nonprofit organization|not-for-profit]] [[behavioral health]] organization based in [[Houston, Texas]]. It offers long-term residential, short-term residential, [[Drug detoxification|detoxification]] and [[outpatient]] [[behavioral health]] services for adults and adolescents.<ref name="history_17">[http://www.chron.com/default/article/Cenkor-Foundation-hosts-luncheon-1771628.php "Cenikor Foundation hosts luncheon."] ''[[Houston Chronicle]]'', Sept. 10, 2008.]</ref>
Cenikor provides [[evidence-based]] [[therapeutic community]] [[addiction]] treatment through long-term residential programs in [[Baton Rouge, LA]], [[Deer Park, TX]] and [[Fort Worth, TX]] and detox/short-term residential treatment in [[Waco, TX]]. Cenikor offers outpatient treatment services in [[Baton Rouge, LA]], [[Temple, TX]], [[Killeen, TX]], and [[Waco, TX]]. Cenikor also has an adolescent residential facility in [[Houston, TX]] called [[Odyssey House]] Texas.

==History==

===1960s===
In 1967, inmates at the [[Colorado State Penitentiary]] founded Cenikor. The concept was based on group help and peer identification principals.<ref name="history_01">[http://newspaperarchive.com/clovis-news-journal/1977-07-17/page-2?tag=cenikor&rtserp=tags/cenikor "Cenikor To Open Branch In State."] ''[[Clovis News Journal]]'', July 17, 1977.]</ref> The inmates were committed to breaking the cycle of substance abuse and the criminal behavior that supports their addictions. With little funding, they were determined to reverse lives caught up in the cycle of addiction and develop a program that would show others how to become productive and responsible citizens.<ref name="history_02">[http://newspaperarchive.com/colorado-springs-gazette/1977-10-07/page-2?tag=cenikor&rtserp=tags/Cenikor "Cenikor Reaching Those Who Couldn’t Be Helped."] ''[[Colorado Springs Gazette]]'', Oct. 7, 1977.]</ref>

===1970s===
[[File:Cenikor building downtown Houston.jpg|left|thumb|upright|William Penn Hotel in downtown Houston]] In 1972, Cenikor opened a treatment facility in Houston, TX at 1101 Elder in the historic Jefferson Davis Hospital.<ref>http://www.khou.com/news/local/Historic-Jeff-Davis-Hospital-building-burns-in-overnight-fire-204654961.html</ref><ref name="history_03">"Cenikor-One Good Alternative", by Sherman Ross, The Houston Lawyer Magazine, September 1973</ref> Cenikor moved their treatment facility to the William Penn Hotel in downtown Houston, TX in the late 1970s.<ref name="history_04">[http://houstonist.com/2006/01/26/ask_houstonist_1.php houstonist.com “Ask Houstonist:What’s that disappearing building?”]</ref> In 1977, Cenikor signed a year-round contract with the Astrodomain Corporation, establishing the Cenikor Astrodome Task Force, to handle painting, maintenance, field changeovers, and event set-ups at the [[Astrodome]], the Astrohall, and the [[Astroarena]] complexes.<ref name="history_05">[https://news.google.com/newspapers?id=lKZOAAAAIBAJ&sjid=NPsDAAAAIBAJ&pg=6982,3733098&dq=cenikor&hl=en "Strike taking toll on cities’ finances."] ''[[Lakeland Ledger]]'', July 11, 1981.]</ref><ref name="history_06">[https://news.google.com/newspapers?id=WHI0AAAAIBAJ&sjid=zqQFAAAAIBAJ&pg=6110,4039607&dq=cenikor&hl=en "Brief."] ''[[The Montreal Gazette]]'', June 19, 1981.]</ref><ref name="history_07">"Cenikor: The last stop before prison", by Rosalind Jackler, ''[[The Houston Post]]'', April 29, 1984.</ref> For many years, Cenikor residents also provided the manpower for the [[Houston Livestock Show and Rodeo]].<ref name="history_08">"Cenikor residents help at rodeo", Suburbia-Reporter East, March 7, 1991.</ref>

After two years of operating an in-take facility in the Dallas/Fort Worth area, the [[Winn-Dixie Stores Inc.]] donated two 100,000 square feet buildings in [[Fort Worth]] in January 1979 to help Cenikor establish a north Texas treatment facility.<ref name="history_09">"In Old Fort Worth: Where Kimbell Hung First Paintings", by Mack Williams, ''[[Fort Worth News Tribune]]'', Jan. 19, 1979.</ref> Cenikor residents did the work converting the warehouse and two office buildings into a livable facility that could house 180 residents. The first resident entered the facility for treatment on New Year's Eve 1979.<ref name="history_10">"Cenikor opens its doors for troubled people", by Carolyn Ondrejas, ''[[Fort Worth Star-Telegram]]'', Jan. 3, 1980 Evening Edition.</ref>

===1980s===
In 1983, Cenikor received national recognition from the [[President of the United States]], [[Ronald Reagan]], when he visited the Houston facility on April 29. President Reagan commended Cenikor for its ability to operate without government funding, and for its success in enlisting support from the private sector.<ref name="history_11">"President pays visit to Cenikor facility, praises center’s work", by Jim Simmon, ''[[Houston Chronicle]]'', April 30, 1983.</ref> [[Nancy Reagan]] visited the [[Lakewood, Colorado]] facility on Tuesday, Aug. 10, 1983 during the national anti-drug campaign.<ref name="history_12">"Red Carpet Greets Mrs. Reagan", by Diane Eicher, ''[[The Denver Post]]'', Aug. 10,1983.</ref> [[Nancy Reagan]] also visited the Fort Worth facility in 1986 alongside Texas Governor-elect [[Bill Clements]] as she handed out diplomas to 11 graduates.<ref name="history_13">"First lady gives diplomas to grads of drug rehabilitation program", ''[[Houston Chronicle]]'', Feb. 19, 1986.</ref>

===1990s===
In 1994, Cenikor's Houston facility moved from downtown [[Houston]] to [[Deer Park, Texas|Deer Park]], a suburb in southeast Houston. The new facility, located in the former Deer Park Hospital, is on almost 20 acres of land and 80,000 square feet in size, housing 180 residents.<ref name="history_14">"Reaching out: Cenikor creates downtown facility to expand services", by Bernadette Gillece, ''[[Houston Chronicle]]'', Dec. 14, 1994.</ref> In 1995, Cenikor opened an outreach office in [[Baton Rouge, Louisiana]] in a space provided by the city. This location referred over 150 to the Texas facilities each year.<ref name="history_15">"'One Day at a Time'", by Chante Dionne Warren, ''[[Baton Rouge Advocate]]'', Dec. 7, 1996.</ref>

===2000s===
2007 marked 40 years of providing supportive residential therapeutic treatment services. Cenikor has impacted more than 40,000 lives and in 2007 had more than 500 residents in three long-term treatment facilities located in [[Deer Park, TX]]; [[Fort Worth, TX]]; and [[Baton Rouge, LA]]. In 2007, residents began attending college and [[vocational training]] programs in an effort to improve their quality of life while getting treatment at Cenikor.<ref name="history_16">"Cenikor celebrates 40 years of changing lives", by Carla Rabalais, ''[[Houston Chronicle]]'', Oct. 25, 2007.</ref>

In 2010, Cenikor formed a strategic alliance with [[Odyssey House]] Texas to provide [[therapeutic community]] treatment services to adolescents. In February 2011, Cenikor began serving [[Lake Charles, Louisiana|Lake Charles]] residents in the former state-run Joseph R. Briscoe facility. The 34-bed short-term residential unit maintains a high occupancy rate. The 12-bed medically supported detoxification unit continues to receive referrals from across the state.

On July 10, 2012 a ribbon cutting and open house was held for the new short-term residential facility in Waco. More than 200 people attended the event including Texas State Representative [[Charles "Doc" Anderson]], Waco Mayor Malcolm Duncan, Waco District Attorney Abel Reyna, city councilmen, representatives from Hillcrest Baptist Medical Center executive staff and other community members. The facility began accepting clients on Monday, July 16 for short-term residential and Monday, July 23 for detoxification.

==Locations==
[[File:Cenikor Fort Worth.jpg|thumb|upright|Cenikor Fort Worth, TX facility]]

===Long-term adult residential facilities===
* Lakewood (Denver),CO - 1967 to 2004
* Fort Worth, TX
* Houston (Deer Park), TX
* Baton Rouge, LA
[[File:Cenikor Deer Park.jpg|thumb|upright|Cenikor Deer Park, TX facility]]

===Detoxification===
* Waco, TX
* Houston, TX
* Tyler, TX

===Short-term adult residential facilities===
*Waco, TX
*Tyler, TX
[[File:Cenikor Baton Rouge.jpg|thumb|upright|Cenikor Baton Rouge, LA facility]]

===Outpatient services===
* Care Counseling Services - Baton Rouge, LA
* Care Counseling Services - Temple, TX
* Care Counseling Services - Killeen, TX
* Care Counseling Services - Waco, TX
[[File:Wacooutside.jpg|thumb|upright|Cenikor Waco, TX facility]]

===Adolescent residential===
* [[Odyssey House]] Texas - Houston, TX
[[File:Odyssey House Texas.jpg|thumb|upright|Odyssey House Texas, Houston, TX facility]]

==References==
{{reflist|2}}

==External links==
{{Div col|2}}
* [http://www.cenikor.org Official website]
* [http://www.odysseyhousetexas.com Odyssey House Texas]
* [http://www.thefreemancenter.org/ The Freeman Center Waco, TX]
* [http://www.therapeuticcommunitiesofamerica.org/main/ Therapeutic Communities of America]
* [http://www.asaptexas.org/ Association of Substance Abuse Providers of Texas]
* [http://www.texanshelpingtexans.org/metroplex-directory/cenikor-foundation_1103.html Texans Helping Texans]
* [http://www.bbb.org/charity-reviews/houston/human-services/cenikor-foundation-in-houston-tx-16100 Better Business Bureau]
*[http://www.reagan.utexas.edu/archives/speeches/1983/42983b.htm Remarks at the Cenikor Foundation Center in Houston, Texas]

{{Div col end}}

[[Category:Articles created via the Article Wizard]]
[[Category:Therapeutic community]]
[[Category:Detoxification]]
[[Category:Substance abuse]]
[[Category:Non-profit organizations based in Houston]]