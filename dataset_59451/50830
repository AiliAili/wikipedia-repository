{{Italic title}}
'''''The Canadian Field-Naturalist''''' is a quarterly [[scientific journal]] publishing original scientific papers related to [[natural history]] in [[Canada]]. It accepts submissions by both amateur and professional [[naturalist]]s and field [[biologist]]s.<ref>{{cite web|url=http://www.ofnc.ca/cfn/index.php |title=The Canadian Field-Naturalist |publisher=Ottawa Field-Naturalists' Club |date=2012-03-18 |accessdate=2012-08-06}}</ref><ref>{{cite web|url=http://catalogue.nrcan.gc.ca/opac/en-CA/skin/nrcan-rncan/xml/rdetail.xml?r=8094443&l=1&d=0 |title=The Canadian field-naturalist and its predecessors : a bibliographical survey of 75 years of publication / W. J. Cody & B. Boivin |publisher=NRCan |work=NRCan Library Catalogue |date=2011-03-25 |accessdate=2012-08-06}}</ref>

== History ==
''The Canadian Field-Naturalist'' has been published continuously since 1880, under several names during its early years. For 7 years beginning in 1880, the [[Ottawa Field-Naturalists' Club]] issued the ''Transactions of the Ottawa Field-Naturalists' Club'' annually. With volume 2 in 1887, the ''Transactions'' became a subtitle of volume 1 of ''The Ottawa Naturalist'', a monthly publication. With volume 3 of ''The Ottawa Naturalist'' in 1889 the emphasis changed from local members' reports to national ones, and in 1919 the journal was renamed ''The Canadian Field-Naturalist'' (starting with volume 33 which was volume 35 of the ''Transactions'' but this subtitle was subsequently dropped).<ref>{{cite web|url=http://www.ofnc.ca/cfn/about-CFN.php |title=About The Canadian Field-Naturalist |publisher=Ofnc.ca |date=2009-02-19 |accessdate=2012-08-06}}</ref>

== Society ==
The '''Ottawa Field-Naturalists' Club''' was founded in 1879. It is the oldest natural history society in Canada. It has over 1000 members, with interests in all aspects of the natural world, from [[bird]]s to [[botany]] and [[Conservation (ethic)|conservation]].<ref>{{cite web|url=http://www.ofnc.ca/index.html |title=The Ottawa Field-Naturalists' Club |publisher=Ottawa Field-Naturalists' Club |date= |accessdate=2012-08-06}}</ref>

Notable members have included:
{{expand list|date=January 2015}}
* [[Henri-Marc Ami]] (1858{{ndash}}1931), president 1899{{ndash}}1901
* [[James Fletcher (entomologist)|James Fletcher]] (1852{{ndash}}1908), founding member
* [[Charles Gordon Hewitt]] (1885{{ndash}}1920), president
* [[John Macoun]] (1831{{ndash}}1920)

== References==
{{reflist}}

== Further reading ==
*{{Citation
 |last = Ottawa Field-naturalists' Club
 |first = 
 |title = The Ottawa Naturalist by Ottawa Field-naturalists' Club
 |date = September 8, 2011
 |publisher = Nabu Press 
 |publication-place = Ottawa, Ontario
 |isbn = 
 }}

== External links ==
* {{official website|http://www.ofnc.ca/cfn/}}
* {{ISSN|0008-3550}}

{{Ottawa}}
{{Portal|Ottawa}}

[[Category:Publications established in 1880]]
[[Category:Quarterly journals]]
[[Category:Biology journals]]
[[Category:English-language journals]]