{{Use dmy dates|date=July 2011}}
{{Infobox Organization
| name         = London Mathematical Society
| image        = London Mathematical Society (logo).png
| image_border = 
| image_size   = 250
| caption      = 
| formation    = 1865
| type         = [[Learned society]]
| headquarters = [[London]], [[England]]
| location     = 
| membership   = 
| language     = 
| leader_title = [[President]]
| leader_name  = [[Simon Tavaré]]
| num_staff    = 
| budget       = 
| website      = [http://www.lms.ac.uk/ www.lms.ac.uk]
}}
'''The London Mathematical Society''' ('''LMS''') is one of the United Kingdom's [[Learned society|learned societies]] for [[mathematics]] (the others being the [[Royal Statistical Society]] (RSS) and the [[Institute of Mathematics and its Applications]] (IMA)).

==History==
[[Image:De Morgan House.jpg|thumb|right|De Morgan House]]
The Society was established on 16 January 1865, the first president being [[Augustus De Morgan]].  The earliest meetings were held in [[University College London|University College]], but the Society soon moved into [[Burlington House]], [[Piccadilly]].  The initial activities of the Society included talks and publication of a journal.

The LMS was used as a model for the establishment of the [[American Mathematical Society]] in 1888.

The Society was granted a [[royal charter]] in 1965, a century after its foundation.  In 1998 the Society moved from rooms in Burlington House into '''De Morgan House''' (named after the society's first president), at 57–58 [[Russell Square]], [[Bloomsbury]], to accommodate an expansion of its staff. The Society is also a member of the UK [http://www.sciencecouncil.org Science Council].

===Proposal for unification with the IMA===
On 4 July 2008, the Joint Planning Group for the LMS and IMA proposed a merger of two societies to form a single, unified society. The proposal was the result of eight years of consultations and the councils of both societies commended the report to their members.<ref>{{cite web|url=http://www.newmathsoc.org.uk/|title=New Math Soc|accessdate=21 April 2009}}</ref> Those in favour of the merger argued a single society would give mathematics in the UK a coherent voice when dealing with [[UK Research Councils|Research Councils]].<ref>{{cite web | url = http://groups.google.co.uk/group/newmathsoc/browse_thread/thread/8613d3cc501d7352 | title = Why I believe a united society would be better| first=Alice|last=Rogers|authorlink= Alice Rogers | date=12 May 2009 | accessdate =27 June 2009 }}</ref> While accepted by the IMA membership, the proposal was rejected by the LMS membership on 29 May 2009 by 591 to 458 (56% to 44%).<ref>{{cite web | url = http://www.lms.ac.uk/sgm_results.html | title = LMS Special General Meeting votes against progressing with unification plans | publisher = London Mathematical Society | accessdate =17 June 2009}}</ref>

==Activities==
The Society publishes books and periodicals; organizes mathematical conferences; provides funding to promote mathematics research and education; and awards a number of prizes and fellowships for excellence in mathematical research.

==Publications==
The Society's periodical publications include three printed [[scientific journal|journal]]s:
*'''''Bulletin of the London Mathematical Society'''''
*'''''Journal of the London Mathematical Society'''''
*'''''Proceedings of the London Mathematical Society'''''

Other publications include an electronic journal, the ''[[Journal of Computation and Mathematics]]''; and a regular members' newsletter. It also publishes the journal ''[[Compositio Mathematica]]'' on behalf of its owning foundation, and copublishes ''[[Nonlinearity (journal)|Nonlinearity]]'' with the [[Institute of Physics]]. The Society publishes four book series: a series of ''Monographs'', a series of ''Lecture Notes'', a series of ''Student Texts'', and (jointly with the [[American Mathematical Society]]) the ''History of Mathematics'' series; it also co-publishes four series of translations: ''[[Russian Mathematical Surveys]]'', ''[[Izvestiya: Mathematics]]'' and ''[[Sbornik: Mathematics]]'' (jointly with the [[Russian Academy of Sciences]] and [[Turpion (company)|Turpion]]), and ''[[Transactions of the Moscow Mathematical Society]]'' (jointly with the American Mathematical Society).

==Prizes==
The named prizes are:
* [[De Morgan Medal]] (triennial) — the most prestigious;
* [[Pólya Prize (LMS)|Pólya Prize]] (two years out of three);
*[[Louis Bachelier Prize]] (biennial);
* [[Senior Berwick Prize]];
* [[Senior Whitehead Prize]] (biennial);
* [[Naylor Prize and Lectureship]];
* [[Berwick Prize]];
* [[Fröhlich Prize]] (biennial);
* [[Whitehead Prize]] (annual).

In addition, the Society jointly with the [[Institute of Mathematics and its Applications]] awards the [[David Crighton]] Medal every three years.

==List of presidents==
{{columns-list|3|
* 1865–1866 [[Augustus De Morgan]]
* 1866–1868 [[James Joseph Sylvester]]
* 1868–1870 [[Arthur Cayley]]
* 1870–1872 [[William Spottiswoode]]
* 1872–1874 [[Thomas Archer Hirst]]
* 1874–1876 [[Henry John Stephen Smith]]
* 1876–1878 [[Lord Rayleigh]]
* 1878–1880 [[Charles Watkins Merrifield]]
* 1880–1882 [[Samuel Roberts (mathematician)|Samuel Roberts]]
* 1882–1884 [[Olaus Henrici]]
* 1884–1886 [[James Whitbread Lee Glaisher]]
* 1886–1888 [[James Cockle (lawyer)|James Cockle]]
* 1888–1890 [[John James Walker]]
* 1890–1892 [[Alfred George Greenhill]]
* 1892–1894 [[Alfred Kempe]]
* 1894–1896 [[Percy Alexander MacMahon]]
* 1896–1898 [[Edwin Bailey Elliott|Edwin Elliott]]
* 1898–1900 [[William Thomson, 1st Baron Kelvin]]
* 1900–1902 [[E. W. Hobson]]
* 1902–1904 [[Horace Lamb]]
* 1904–1906 [[Andrew Forsyth]]
* 1906–1908 [[William Burnside]]
* 1908–1910 [[William Davidson Niven]]
* 1910–1912 [[H. F. Baker]]
* 1912–1914 [[Augustus Edward Hough Love]]
* 1914–1916 [[Joseph Larmor]]
* 1916–1918 [[Hector Munro Macdonald|Hector Macdonald]]
* 1918–1920 [[John Edward Campbell]]
* 1920–1922 [[Herbert William Richmond|Herbert Richmond]]
* 1922–1924 [[William Henry Young]]
* 1924–1926 [[Arthur Lee Dixon]]
* 1926–1928 [[G. H. Hardy]]
* 1928–1929 [[E. T. Whittaker]]
* 1929–1931 [[Sydney Chapman (mathematician)|Sydney Chapman]]
* 1931–1933 [[Alfred Cardew Dixon]]
* 1933–1935 [[G. N. Watson]]
* 1935–1937 [[George Barker Jeffery]]
* 1937–1939 [[Edward Arthur Milne]]
* 1939–1941 [[G. H. Hardy]]
* 1941–1943 [[John Edensor Littlewood]]
* 1943–1945 [[Louis J. Mordell|L. J. Mordell]]
* 1945–1947 [[Edward Charles Titchmarsh]]
* 1947–1949 [[W. V. D. Hodge]]
* 1949–1951 [[Max Newman]]
* 1951–1953 [[George Frederick James Temple]]
* 1953–1955 [[J. H. C. Whitehead]]
* 1955–1957 [[Philip Hall]]
* 1957–1959 [[Harold Davenport]]
* 1959–1961 [[Hans Heilbronn]]
* 1961–1963 [[Mary Cartwright]]
* 1963–1965 [[Arthur Geoffrey Walker]]
* 1965–1967 [[Graham Higman]]
* 1967–1969 [[J. A. Todd]]
* 1969–1970 [[Edward Collingwood]]
* 1970–1972 [[Claude Ambrose Rogers]]
* 1972–1974 [[David George Kendall]]
* 1974–1976 [[Michael Atiyah]]
* 1976–1978 [[J. W. S. Cassels]]
* 1978–1980 [[C. T. C. Wall]]
* 1980–1982 [[Barry Edward Johnson|Barry Johnson]]
* 1982–1984 [[Paul Cohn]]
* 1984–1986 [[Ioan James]]
* 1986–1988 [[Erik Christopher Zeeman]]
* 1988–1990 [[John H. Coates]]
* 1990–1992 [[John Kingman]]
* 1992–1994 [[John Robert Ringrose|John Ringrose]]
* 1994–1996 [[Nigel Hitchin]]
* 1996–1998 [[John M. Ball]]
* 1998–2000 [[Martin J. Taylor]]
* 2000–2002 [[John Trevor Stuart|Trevor Stuart]]
* 2002–2003 [[Peter Goddard (physicist)|Peter Goddard]]
* 2003–2005 [[Frances Kirwan]]
* 2005–2007 [[John Toland (mathematician)|John Toland]]
* 2007–2009 [[E. Brian Davies]]
* 2009 (interim) [[John M. Ball]]<ref>{{cite web|url=http://www.lms.ac.uk/council_statement2.html|title=Statement by the Council of the London Mathematical Society|publisher=London Mathematical Society|date=7 July 2009|accessdate=15 July 2009}}</ref>
* 2009–2011 [[Angus Macintyre]]<ref>{{cite web|url=http://www.lms.ac.uk/council_statement4.html|title=Statement following the Council Meeting on 24&nbsp;August&nbsp;2009|publisher=London Mathematical Society|date=26 August 2009}}</ref>
* 2011–2013 [[Graeme Segal]]<ref>{{cite web|url=http://www.lms.ac.uk/content/lms-election-results-2011|title=2011 LMS Election Results|publisher=London Mathematical Society|date=18 November 2011}}</ref>
* 2013–2015 [[Terry Lyons (mathematician)|Terry Lyons]]<ref>{{cite web|url=http://www.lms.ac.uk/content/lms-election-results-2013|title=2013 LMS Election Results|publisher=London Mathematical Society|date=18 April 2014}}</ref>
* 2015-2017 [[Simon Tavaré]]
}}

==See also==
{{commons category|London Mathematical Society}}
* [[American Mathematical Society]]
* [[Edinburgh Mathematical Society]]
* [[European Mathematical Society]]
* [[List of Mathematical Societies]]
* [[Council for the Mathematical Sciences]]
* [[BCS-FACS]] Specialist Group

==References==
{{Reflist}}
*{{Cite book |last=Oakes |first=Susan Margaret |author2=Pears, Alan Robson |author3=Rice, Adrian Clifford |title=The Book of Presidents 1865–1965 |publisher=London Mathematical Society |year=2005 |isbn=0-9502734-1-4}}

==External links==
* [http://www.lms.ac.uk/ London Mathematical Society website]
* [http://www-history.mcs.st-andrews.ac.uk/history/Societies/LMShistory.html A History of the London Mathematical Society]
* [http://www-groups.dcs.st-and.ac.uk/~history/Societies/LMS.html MacTutor: The London Mathematical Society]

{{The European Mathematical Society}}

[[Category:Clubs and societies in London]]
[[Category:Education in the London Borough of Camden]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Mathematical societies]]
[[Category:Mathematics education in the United Kingdom]]
[[Category:Organisations based in the London Borough of Camden]]
[[Category:Organizations established in 1865]]
[[Category:Presidents of the London Mathematical Society|*]]
[[Category:Science and technology in London]]
[[Category:1865 establishments in England]]