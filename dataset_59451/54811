{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Notting Hill Mystery
| image         = The Notting Hill Mystery, section V, Once A Week.jpg
| caption       = Cover art for Section V of ''The Notting Hill Mystery'', ''[[Once A Week (magazine)|Once A Week]]'', 27 December 1862
| author        = Charles Felix ([[pseudonym]] of [[Charles Warren Adams]]<ref name=buckler/><ref name=collins/> ) 
| illustrator   = [[George du Maurier]]
| cover_artist  = 
| country       = United Kingdom
| language      = English
| genre         = [[Detective novel]]
| publisher     = [[Bradbury & Evans]] (serial)<br>Saunders, Otley, and Company (book)
| pub_date      = 1862&ndash;63 (serial)<br>1865 (book)
| media_type    = Print ([[magazine]] and [[hardcover]])
| pages         = 
| isbn          = 
| dewey         = 
| congress      = 
| oclc          = 
}}

'''''The Notting Hill Mystery''''' (1862&ndash;63) is an English-language [[detective novel]] written under the [[pseudonym]] "Charles Felix", with illustrations by [[George du Maurier]]. The author's identity was never formally revealed in his lifetime, but several critics have since suggested [[Charles Warren Adams]] (1833–1903),<ref name=buckler>Buckler, William.  "Once a Week Under Samuel Lucas, 1859-65."  ''PMLA''.  67.7(1952): 924–41.</ref><ref name=collins/> a lawyer who wrote novels pseudonymously. It is arguably regarded as the very first [[detective novel]] in the English language.<ref name=symons/><ref name=guardian>Flood, Alison. [https://www.theguardian.com/books/2012/feb/21/first-detective-novel-notting-hill-mystery "First ever detective novel back in print after 150 years"], ''The Guardian'', 21 Feb 2012</ref>

==History==
''The Notting Hill Mystery'' was first published as an eight-part [[Periodical publication|serial]] in ''[[Once A Week (magazine)|Once A Week]]'' magazine beginning on 29 November 1862, then as a single-volume novel in <!--see talk page-->1865<ref name=collins>{{cite web |archiveurl=https://web.archive.org/web/20120308032736/http://www.nytimes.com/2011/01/09/books/review/Collins-t.html? |archivedate=March 8, 2012 |url=https://www.nytimes.com/2011/01/09/books/review/Collins-t.html |title=Before Hercule or Sherlock, There Was Ralph |work=[[The New York Times Book Review]] |author=[[Paul Collins (American writer)|Collins, Paul]] |date=7 January 2011 |accessdate=25 June 2014}}</ref> by Saunders, Otley, and Company, with illustrations by [[George du Maurier]] (grandfather of [[Daphne du Maurier]]<ref name=collins/>).

The editors of the magazine said the manuscript was submitted to them anonymously under the [[pseudonym]] of "Charles Felix".  In 1952 William Buckler identified [[Charles Warren Adams]] (1833&ndash;1903) as the author of ''The Notting Hill Mystery'' and in January 2011, [[Paul Collins (American writer)|Paul Collins]] &ndash; a writer, editor and academic &ndash; writing in ''[[The New York Times Book Review]]'', came to the same conclusion.<ref name=buckler/><ref name=collins/> Adams, a lawyer, was the sole proprietor of Saunders, Otley & Co., which published another book by "Charles Felix" called ''Velvet Lawn'',<ref name=collins/><ref name=npr/> and published an edition of ''The Notting Hill Mystery'' in 1865. Collins bases his theory on a number of lines of evidence, including an explicit reference to Felix's identity as Adams in a 14 May 1864 "Literary Gossip" column of ''[[The Manchester Times]]'' which read “It is understood that ‘Velvet Lawn,’ by Charles Felix, the new novel announced by Messrs. Saunders, Otley & Co., is by Mr. Charles Warren Adams, now the sole representative of that firm.”<ref name=collins/>

A number of critics, including [[Julian Symons]], a crime writer and poet,<ref name=symons>[[Julian Symons|Symons, Julian]] (1972), ''Bloody Murder: From the Detective Story to the Crime Novel''. [[Faber and Faber]] ([[London]]).  ISBN 978-0-571-09465-3.  pg. 51: "..there is no doubt that the first detective novel, preceding [[Wilkie Collins|Collins]] and [[Émile Gaboriau|Gaboriau]], was ''The Notting Hill Mystery''.</ref> and Collins,<ref name=collins/> believe it to be the first modern [[detective novel]], but it has been overshadowed by works by [[Wilkie Collins]] and [[Émile Gaboriau]] who usually receive that accolade even though they came later.<ref name=symons/> Some aspects of detective fiction can be found in [[R. D. Blackmore]]'s sensation novel ''[[Clara Vaughan]]'' (written in 1853, published in 1864), about the daughter of a murder victim seeking her father's killer,<ref>The Victorian Web: [http://www.victorianweb.org/genre/sensation.html Retrieved 7 February 2013.]</ref> but Adams's novel contains a number of innovative features, such as the main character presenting evidence as his own findings through diary entries, family letters, depositions, chemical analysts report, crime scene map.<ref name=collins/> These techniques would not become common until the 1920s, and Symons said it "quite bowled me over" how far ahead of its time it was.<ref name=collins/>

==Plot==
Source documents compiled by [[Insurance fraud|insurance investigator]] Ralph Henderson are used to build a case against Baron "R___", who is suspected of murdering his wife. The baron's wife died from drinking a bottle of acid, apparently while [[sleepwalking]] in her husband's private laboratory.<ref name=npr>[[Staff writer]] (8 January 2011). [http://www.npr.org/2011/01/08/132760328/Who-Wrote-The-First-Detective-Novel "Who Wrote The First Detective Novel?"].  [[NPR]].  Retrieved 10 January 2011.</ref> Henderson's suspicions are raised when he learns that the baron recently had purchased five [[life insurance]] policies for his wife.<ref name=npr/> As Henderson investigates the case, he discovers not one but three murders.<ref name=npr/> The plot hinges on the dangers of [[Animal Magnetism|mesmerism]], a subject explored in fiction earlier by [[Isabella Frances Romer]].<ref>''Sturmer: a Tale of Mesmerism'' (London, 1841).</ref> Although the baron's guilt is clear to the reader even from the outset, how he did it remains a mystery.<ref name=npr/> Eventually this is revealed, but how to catch him becomes the final challenge; he seems to have committed the [[perfect crime]].<ref name=npr/>

==Editions==
The novel was reprinted in 1945 by Pilot Press Ltd. of London in their anthology ''Novels of Mystery from the Victorian Age''.<ref>Richardson, M., Collins, W., Le, F. J. S., & Stevenson, R. L. (1945). ''Novels of mystery from the Victorian Age: Four complete unabridged novels by J. Sheridan Le Fanu, Anon, Wilkie Collins, R.L. Stevenson''. London: Pilot Press. See {{worldcat|oclc=5436085|name=Novels of mystery from the Victorian Age}}</ref>  In March 2011 the [[British Library]] made the novel available again via print-on-demand.<ref name=guardian/> It sold so many copies they were prompted to make a trade edition, produced using photographs of the 1865 edition, which was published in 2012 on the 150th anniversary of the novel's release.<ref name=guardian/> An ebook version is also available.

==See also==
* [[1862 in literature]]
* [[1863 in literature]]

==References==
{{Reflist}}

==External links==
*''The Notting Hill Mystery'', serialized in [https://archive.org/details/newonceweek05londuoft ''Once a Week''] (original edition illustrated, at [[Internet Archive]]):
**[https://archive.org/stream/onceweek07londuoft#page/616/mode/2up Section 1] (''Once a Week'', Vol. 7, pg. 617, 29 November 1862)
**[https://archive.org/stream/onceweek07londuoft#page/644/mode/2up Section 2] (''Once a Week'', Vol. 7, pg. 645, 6 December 1862)
**[https://archive.org/stream/onceweek07londuoft#page/672/mode/2up Section 3] (''Once a Week'', Vol. 7, pg. 673, 13 December 1862)
**[https://archive.org/stream/onceweek07londuoft#page/700/mode/2up Section 4] (''Once a Week'', Vol. 7, pg. 701, 20 December 1862)
**[https://archive.org/stream/onceweek08londuoft#page/iv/mode/2up Section 5] (''Once a Week'', Vol. 8, pg. 1, 27 December 1862)
**[https://archive.org/stream/onceweek08londuoft#page/28/mode/2up Section 6] (''Once a Week'', Vol. 8, pg. 29, 3 January 1863)
**[https://archive.org/stream/onceweek08londuoft#page/56/mode/2up Section 7] (''Once a Week'', Vol. 8, pg. 57, 10 January 1863)
**[https://archive.org/stream/onceweek08londuoft#page/84/mode/2up Section 8] (''Once a Week'', Vol. 8, pg. 85, 17 January 1863)
*Lewis, Steve (19 December 2010). Norris, J.F. (19 December 2010).  [http://mysteryfile.com/blog/?p=6777 "Reviewed by J. F. Norris: Charles Felix – The Notting Hill Mystery"].  ''Mystery File'' ([[blog]] at mysteryfile.com).  Retrieved 10 January 2011.
* {{librivox book | title=The notting hill mystery | author=Charles Warren Adams}}

{{DEFAULTSORT:Notting Hill Mystery, The}}
[[Category:1863 novels]]
[[Category:British crime novels]]
[[Category:Detective novels]]
<!--[[Category:English language novels]]-- no cat as yet at 10 jan. 2011-->
[[Category:Novels first published in serial form]]
[[Category:London in fiction]]
[[Category:Works originally published in Once A Week (magazine)]]
[[Category:Works published under a pseudonym]]
[[Category:19th-century British novels]]