{{Use dmy dates|date=December 2016}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Konig.jpg|300px]]
| Ship caption = Recognition drawing of a ''König''-class battleship
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship builder = [[AG Weser]], [[Bremen]]
| Ship laid down = November 1911
| Ship launched = 4 June 1913
| Ship commissioned = 1 October 1914
| Ship fate = [[Scuttled]] 21 June 1919 in [[Gutter Sound]], [[Scapa Flow]]
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|König|battleship}}
| Ship displacement =* {{convert|25390|MT|abbr=on}} design
* {{convert|28600|MT|abbr=on}} full load
  
| Ship length = {{convert|175.4|m|ftin|abbr=on}}
| Ship beam = {{convert|29.5|m|ftin|abbr=on}}
| Ship draft = {{convert|9.19|m|ftin|abbr=on}}
| Ship power = {{convert|30,450|kW|shp|lk=in|order=flip|abbr=on}}
| Ship propulsion =* 3 shafts
* 3 [[steam turbine]]s
  
| Ship speed = {{convert|21|kn|lk=in}}
| Ship range = {{convert|8000|nmi|lk=in|abbr=on}} at {{convert|12|kn}}
| Ship crew =* 41 officers
* 1,095 enlisted men
  
| Ship armament =* 10 × [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} guns]]
* 14 × [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} guns]]
* 10 × [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} guns]]
*  5 × {{convert|50|cm|in|1|abbr=on}} torpedo tubes
  
| Ship armor =* Belt: {{convert|350|mm|in|abbr=on|1}}
* Turrets and conning tower: {{convert|300|mm|in|abbr=on|1}}
* Deck: {{convert|30|mm|in|abbr=on}}
  
| Ship notes = 
}}
|}

'''SMS ''Markgraf'''''{{efn|name=SMS}} was the third [[battleship]] of the four-ship {{sclass-|König|battleship|4}}. She served in the [[Imperial German Navy]] during [[World War I]]. The battleship was laid down in November 1911 and launched on 4 June 1913. She was formally commissioned into the Imperial Navy on 1 October 1914, just over two months after the outbreak of war in Europe. ''Markgraf'' was armed with ten {{convert|30.5|cm|in|sp=us|adj=on}} guns in five twin turrets and could steam at a top speed of {{convert|21|kn|link=on}}. ''Markgraf'' was named in honor of the [[Margrave of Baden|royal family of Baden]]. The name Markgraf is a rank of German nobility and is equivalent to the English [[Margrave]], or [[Marquess]].

Along with her three [[sister ship]]s, {{SMS|König||2}}, {{SMS|Grosser Kurfürst|1913|2}}, and {{SMS|Kronprinz||2}}, ''Markgraf'' took part in most of the fleet actions during the war, including the [[Battle of Jutland]] on 31 May and 1 June 1916. At Jutland, ''Markgraf'' was the third ship in the German [[line of battle|line]] and heavily engaged by the opposing British [[Grand Fleet]]; she sustained five large-caliber hits and her crew suffered 23 casualties. ''Markgraf'' also participated in [[Operation Albion]], the conquest of the [[Gulf of Riga]], in late 1917. The ship was damaged by a mine while en route to Germany following the successful conclusion of the operation.

After Germany's defeat in the war and the signing of the [[Armistice with Germany|Armistice]] in November 1918, ''Markgraf'' and most of the [[capital ship]]s of the [[High Seas Fleet]] were interned by the [[Royal Navy]] in [[Scapa Flow]]. The ships were disarmed and reduced to skeleton crews while the [[Allies of World War I|Allied powers]] negotiated the final version of the [[Treaty of Versailles]]. On 21 June 1919, days before the treaty was signed, the commander of the interned fleet, Rear Admiral [[Ludwig von Reuter]], [[Scuttling of the German fleet in Scapa Flow|ordered the fleet to be scuttled]] to ensure that the British would not be able to seize the ships. Unlike most of the scuttled ships, ''Markgraf'' was never raised for scrapping; the wreck is still sitting on the bottom of the bay.

== Construction and design ==
{{main|König-class battleship}}
[[File:König class battleship - Jane's Fighting Ships, 1919 - Project Gutenberg etext 24797.png|thumb|left|Plan and elevation view of a ship of the ''König'' class, from Jane's ''Fighting Ships'' 1919|alt=Schematics for this type of battleship; the ships mount five gun turrets, two forward, one in the center between two smoke stacks, and two aft]]

''Markgraf'' was ordered under the provisional name ''Ersatz Weissenburg'' and built at the [[AG Weser]] shipyard in [[Bremen]] under construction number 186.{{sfn|Gröner|p=27}}{{efn|name=provisional names}} Her [[keel]] was laid in November 1911 and she was launched on 4 June 1913.{{sfn|Campbell "Germany 1906–1922"|p=36}} At her launching ceremony, the ship was christened by [[Frederick II, Grand Duke of Baden]], the head of the [[Margrave of Baden|royal family of Baden]], in honor of which the ship had been named.{{sfn|Koop & Schmolke|p=131}} [[Fitting-out]] work was completed by 1 October 1914, the day she was commissioned into the [[High Seas Fleet]].{{sfn|Gröner|p=28}} She had cost the Imperial German Government 45 million [[German gold mark|''Goldmarks'']].{{sfn|Gröner|p=27}}

''Markgraf'' [[Displacement (ship)|displaced]] {{convert|25,796|t|LT|abbr=on}} as built and {{convert|28,600|t|LT|abbr=on}} fully loaded, with a [[length overall|length]] of {{convert|175.4|m|ftin|abbr=on}}, a [[Beam (nautical)|beam]] of {{convert|19.5|m|ftin|abbr=on}} and a [[Draft (hull)|draft]] of {{convert|9.19|m|ftin|abbr=on}}. She was powered by three Bergmann [[steam turbines]], three oil-fired and twelve coal-fired [[boiler (steam generator)|boilers]], which developed a total of {{convert|30,450|kW|shp|lk=in|order=flip|abbr=on}} and yielded a maximum speed of {{convert|21|kn}}. The ship had a range of {{convert|8,000|nmi|lk=in}} at a cruising speed of {{convert|12|kn}}.{{sfn|Gröner|p=27}} The ship had a crew of 41 officers and 1,095 enlisted sailors.{{sfn|Gröner|p=28}}

She was armed with ten [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} SK L/50 guns]] arranged in five twin [[gun turret]]s: two [[superfire|superfiring]] turrets each fore and aft and one turret amidships between the two funnels. Her secondary armament consisted of fourteen [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45 quick-firing guns]], six [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45 quick-firing guns]] and five {{convert|50|cm|in|abbr=on}} underwater torpedo tubes, one in the bow and two on each beam.{{sfn|Gröner|p=28}} ''Markgraf''{{'}}s 8.8&nbsp;cm guns were removed and replaced with four 8.8&nbsp;cm anti-aircraft guns.{{sfn|Staff|p=27}} The ship's main armored belt was {{convert|350|mm|sp=us}} thick. The deck was {{convert|30|mm|abbr=on}} thick; the main battery turrets and forward [[conning tower]] were armored with {{convert|300|mm|abbr=on}} thick steel plates.{{sfn|Gröner|p=28}}

== Service history ==
Following her commissioning on 1 October 1914, ''Markgraf'' conducted [[sea trial]]s, which lasted until 12 December. By 10 January 1915, the ship had joined III&nbsp;Battle Squadron of the [[High Seas Fleet]] with her three sister ships.{{sfn|Staff|p=35}} On 22 January 1915, III&nbsp;Squadron was detached from the fleet to conduct maneuver, gunnery, and torpedo training in the [[Baltic Sea|Baltic]]. The ships returned to the North Sea on 11 February, too late to assist the [[I Scouting Group]] at the [[Battle of Dogger Bank (1915)|Battle of Dogger Bank]].{{sfn|Staff|p=29}}

In the aftermath of the loss of {{SMS|Blücher}} at the Battle of Dogger Bank, [[Kaiser Wilhelm II]] removed Admiral [[Friedrich von Ingenohl]] from his post as fleet commander on 2 February. Admiral [[Hugo von Pohl]] replaced him as commander of the fleet; von Pohl carried out a series of sorties with the High Seas Fleet throughout 1915.{{sfn|Tarrant|pp=43–44}} The first such operation—''Markgraf''{{'}}s first with the fleet—was a fleet advance to [[Terschelling]] on 29–30 March; the German fleet failed to engage any British warships during the sortie. Another uneventful operation followed on 17–18 April, and another three days later on 21–22 April. ''Markgraf'' and the rest of the fleet remained in port until 29 May, when the fleet conducted another two-day advance into the North Sea. On 11–12 September, ''Markgraf'' and the rest of III Squadron supported a minelaying operation off [[Texel]]. Another uneventful fleet advance followed on 23–24 October.{{sfn|Staff|p=35}}

Vice Admiral [[Reinhard Scheer]] became commander in chief of the High Seas Fleet on 18 January 1916 when Admiral von Pohl became too ill from liver cancer to continue in that post.{{sfn|Tarrant|p=49}} Scheer proposed a more aggressive policy designed to force a confrontation with the British [[Grand Fleet]]; he received approval from the Kaiser in February.{{sfn|Tarrant|p=50}} The first of Scheer's operations was conducted the following month, on 5–7 March, with an uneventful sweep of the [[Hoofden]].{{sfn|Staff|pp=32, 35}} Another sortie followed three weeks later on the 26th, with another on 21–22 April.{{sfn|Staff|p=35}} On 24 April, the [[battlecruiser]]s of Rear Admiral [[Franz von Hipper]]'s I&nbsp;Scouting Group conducted a [[Bombardment of Yarmouth and Lowestoft|raid on the English coast]]. ''Markgraf'' and the rest of the fleet sailed in distant support. The battlecruiser {{SMS|Seydlitz||2}} struck a mine while en route to the target, and had to withdraw.{{sfn|Tarrant|p=53}} The other battlecruisers bombarded the town of Lowestoft unopposed, but during the approach to Yarmouth, they encountered the British cruisers of the [[Harwich Force]]. A short artillery duel ensued before the Harwich Force withdrew. Reports of British submarines in the area prompted the retreat of the I&nbsp;Scouting Group. At this point, Scheer, who had been warned of the sortie of the Grand Fleet from its base in Scapa Flow, also withdrew to safer German waters.{{sfn|Tarrant|p=54}}

=== Battle of Jutland ===

{{main|Battle of Jutland}}
[[File:Map of the Battle of Jutland, 1916.svg|thumb|350px|Maps showing the maneuvers of the British (blue) and German (red) fleets on 30–31 May 1916]]

''Markgraf'' was present during the fleet operation that resulted in the Battle of [[Jutland]] which took place on 31 May and 1 June 1916. The German fleet again sought to draw out and isolate a portion of the Grand Fleet and destroy it before the main British fleet could retaliate. ''Markgraf'' was the third ship in the German line, behind her sisters ''König'' and ''Grosser Kurfürst'' and followed by ''Kronprinz''. The four ships made up the V&nbsp;Division of the III&nbsp;Battle Squadron, and they were the [[wikt:vanguard|vanguard]] of the fleet. The III&nbsp;Battle Squadron was the first of three battleship units; directly astern were the {{sclass-|Kaiser|battleship|2}}s of the VI&nbsp;Division, III&nbsp;Battle Squadron. The III Squadron was followed by the {{sclass-|Helgoland|battleship|5}} and {{sclass-|Nassau|battleship|4}}es of the II&nbsp;Battle Squadron; in the [[rear guard]] were the obsolescent {{sclass-|Deutschland|battleship|0}} pre-dreadnoughts of the I&nbsp;Battle Squadron.{{sfn|Tarrant|p=286}}

Shortly before 16:00 the battlecruisers of I Scouting Group encountered the British [[1st Battlecruiser Squadron]] under the command of Vice Admiral [[David Beatty, 1st Earl Beatty|David Beatty]]. The opposing ships began an artillery duel that saw the destruction of {{HMS|Indefatigable|1909|2}}, shortly after 17:00,{{sfn|Tarrant|pp=94–95}} and {{HMS|Queen Mary||2}}, less than half an hour later.{{sfn|Tarrant|pp=100–101}} By this time, the German battlecruisers were steaming south to draw the British ships toward the main body of the High Seas Fleet. At 17:30, ''König''{{'}}s crew spotted both the I&nbsp;Scouting Group and the 1st Battlecruiser Squadron approaching. The German battlecruisers were steaming to starboard, while the British ships steamed to port. At 17:45, Scheer ordered a [[Boxing the compass|two-point]] turn to port to bring his ships closer to the British battlecruisers, and a minute later, the order to open fire was given.{{sfn|Tarrant|p=110}}{{efn|name=compass points}}

''Markgraf'' opened fire on the battlecruiser {{HMS|Tiger|1913|2}} at a range of {{convert|21000|yd|m}}.{{sfn|Tarrant|p=110}} ''Markgraf'' and her two sisters fired their secondary guns on British destroyers attempting to make torpedo attacks against the German fleet.{{sfn|Tarrant|pp=110–111}}{{efn|name=torpedo attacks}} ''Markgraf'' continued to engage ''Tiger'' until 18:25, by which time the faster battlecruisers managed to move out of effective gunnery range.{{sfn|Tarrant|p=116}} During this period, the battleships {{HMS|Warspite|03|2}} and {{HMS|Valiant|1914|2}} of the 5th Battle Squadron fired on the leading German battleships.{{sfn|Tarrant|p=118}} At 18:10, one of the British ships scored a 15-inch (38&nbsp;cm) shell hit on ''Markgraf''.{{sfn|Campbell ''Jutland''|p=100}} Shortly thereafter, the destroyer {{HMS|Moresby||2}} fired a single torpedo at ''Markgraf'' and missed from a range of about {{convert|8000|yd|m|abbr=on}}.{{sfn|Campbell ''Jutland''|p=101}} {{HMS|Malaya||2}} fired a torpedo at ''Markgraf'' at 19:05, but the torpedo missed due to the long range.{{sfn|Campbell ''Jutland''|p=110}} Around the same time, ''Markgraf'' engaged a cruiser from the 2nd Light Cruiser Squadron before shifting her fire back to the 5th Battle Squadron for ten minutes.{{sfn|Campbell ''Jutland''|p=111}} During this period, two more 15-inch shells hit ''Markgraf'', though the timing is unknown. The hit at 18:10 struck on a joint between two 8-inch-thick side armor plates; the shell burst on impact and holed the armor. The main deck was buckled and approximately {{convert|400|MT|abbr=on}} of water entered the ship. The other two shells failed to explode and caused negligible damage.{{sfn|Campbell ''Jutland''|pp=144–145}}

Shortly after 19:00, the German cruiser {{SMS|Wiesbaden||2}} had become disabled by a shell from the British battlecruiser {{HMS|Invincible|1907|2}}; Rear Admiral [[Paul Behncke]] in ''König'' attempted to position his four ships to cover the stricken cruiser.{{sfn|Tarrant|p=137}} Simultaneously, the British III and IV&nbsp;Light Cruiser Squadrons began a torpedo attack on the German line; while advancing to torpedo range, they smothered ''Wiesbaden'' with fire from their main guns. The obsolescent armored cruisers of the 1st Cruiser Squadron also joined in the melee. ''Markgraf'' and her sisters fired heavily on the British cruisers, but even sustained fire from the battleships' main guns failed to drive them off.{{sfn|Tarrant|p=138}} ''Markgraf'' fired both her 30.5&nbsp;cm and 15&nbsp;cm guns at the armored cruiser {{HMS|Defence|1907|2}}. Under a hail of fire from the German battleships, ''Defence'' exploded and sank;{{sfn|Campbell ''Jutland''|pp=152–153}} credit is normally given to the battlecruiser {{SMS|Lützow||2}}, though ''Markgraf''{{'}}s gunners also claimed credit for the sinking.{{sfn|Campbell ''Jutland''|p=181}}

''Markgraf'' then fired on the battlecruiser {{HMS|Princess Royal||2}} and scored two hits.{{sfn|Campbell ''Jutland''|pp=152–153}} The first hit struck the 9-inch armor covering "X" barbette, was deflected downward, and exploded after penetrating the 1-inch deck armor. The crew for the left gun were killed, the turret was disabled, and the explosion caused serious damage to the upper deck. The second shell penetrated ''Princess Royal''{{'}}s 6-inch belt armor, ricocheted upward off the coal bunker, and exploded under the 1-inch deck armor. The two shells killed 11 and wounded 31.{{sfn|Campbell ''Jutland''|pp=170–172}} At the same time, ''Markgraf''{{'}}s secondary guns fired on the cruiser {{HMS|Warrior|1905|2}}, which was seriously damaged by 15 heavy shells and forced to withdraw. ''Warrior'' [[:wikt:Founder|foundered]] on the trip back to port the following morning.{{sfn|Campbell ''Jutland''|p=153}}

Around 19:30, Admiral [[John Jellicoe, 1st Earl Jellicoe|John Jellicoe]]'s main force of battleships entered the battle;{{sfn|Campbell ''Jutland''|p=155}} {{HMS|Orion|1910|2}} began firing at ''Markgraf'' at 19:32; she fired four salvos of 13.5-inch Armor-Piercing, Capped (APC) shells and scored a hit with the last salvo.{{sfn|Campbell ''Jutland''|p=156}} The shell exploded upon impacting the armor protecting the No. 6 15&nbsp;cm gun casemate. The shell failed to penetrate but holed the armor and disabled the gun. The explosion seriously injured two and killed the rest of the gun crew. A heavy shell nearly struck the ship at the same time, and at 19:44, a bent propeller shaft forced ''Markgraf''{{'}}s crew to turn off the port engine; naval historian John Campbell speculated that this shell was the one that damaged the shaft.{{sfn|Campbell ''Jutland''|pp=193–195}} Her speed dropped to {{convert|17|or|18|kn|abbr=on}}, though she remained in her position in the line.{{sfn|Campbell ''Jutland''|p=162}}

Shortly after 20:00, the German battleships engaged the 2nd Light Cruiser Squadron; ''Markgraf'' fired primarily 15&nbsp;cm shells.{{sfn|Campbell ''Jutland''|p=204}} In this period, ''Markgraf'' was engaged by {{HMS|Agincourt|1913|2}}{{'}}s 12-inch guns, which scored a single hit at 20:14.{{sfn|Campbell ''Jutland''|p=206}} The shell failed to explode and shattered on impact on the 8-inch side armor, causing minimal damage. Two of the adjoining 14-inch plates directly below the 8-inch armor were slightly forced inward and some minor flooding occurred.{{sfn|Campbell ''Jutland''|p=245}} The heavy fire of the British fleet forced Scheer to order the fleet to turn away.{{sfn|Tarrant|pp=172–174}} Due to her reduced speed, ''Markgraf'' turned early in an attempt to maintain her place in the battle line; this, however, forced ''Grosser Kurfürst'' to fall out of formation. ''Markgraf'' fell in behind ''Kronprinz'' while ''Grosser Kurfürst'' steamed ahead to return to her position behind ''König''.{{sfn|Campbell ''Jutland''|p=201}} After successfully withdrawing from the British, Scheer ordered the fleet to assume night cruising formation, though communication errors between Scheer aboard {{SMS|Friedrich der Grosse|1911|2}} and {{SMS|Westfalen||2}}, the lead ship, caused delays.{{sfn|Campbell ''Jutland''|p=275}} Several British light cruisers and destroyers stumbled into the German line around 21:20. In the ensuing short engagement ''Markgraf'' hit the cruiser {{HMS|Calliope|1914|2}} five times with her secondary guns.{{sfn|Campbell ''Jutland''|pp=250–251}} The fleet fell into formation by 23:30, with ''Grosser Kurfürst'' the 13th vessel in the line of 24 capital ships.{{sfn|Campbell ''Jutland''|p=275}}

Around 02:45, several British destroyers mounted a torpedo attack against the rear half of the German line. ''Markgraf'' initially held her fire as the identities of the destroyers were unknown. But gunners aboard ''Grosser Kurfürst'' correctly identified the vessels as hostile and opened fire while turning away to avoid torpedoes, which prompted ''Markgraf'' to follow suit.{{sfn|Campbell ''Jutland''|pp=298–299}} Heavy fire from the German battleships forced the British destroyers to withdraw.{{sfn|Campbell ''Jutland''|pp=300–301}} At 05:06, ''Markgraf'' and several other battleships fired at what they thought was a submarine.{{sfn|Campbell ''Jutland''|p=314}}

The High Seas Fleet managed to punch through the British light forces without drawing the attention of Jellicoe's battleships, and subsequently reached [[Horns Reef]] by 04:00 on 1&nbsp;June.{{sfn|Tarrant|pp=246–247}} Upon reaching Wilhelmshaven, ''Markgraf'' went into harbor while several other battleships took up defensive positions in the outer roadstead.{{sfn|Campbell ''Jutland''|p=320}} The ship was transferred to [[Hamburg]] where she was repaired in [[AG Vulcan]]'s large floating dock. Repair work was completed by 20 July.{{sfn|Campbell ''Jutland''|p=336}} In the course of the battle, ''Markgraf'' had fired a total of 254 shells from her main battery and 214 rounds from her 15&nbsp;cm guns.{{sfn|Tarrant|p=292}} She was hit by five large-caliber shells, which killed 11 men and wounded 13.{{sfn|Tarrant|pp=296, 298}}

=== Subsequent operations ===
Following repairs in July 1916, ''Markgraf'' went into the Baltic for trials. The ship was then temporarily assigned to the I&nbsp;Scouting Group for the [[Action of 19 August 1916|fleet operation on 18–19 August]]. Due to the serious damage incurred by {{SMS|Seydlitz||2}} and {{SMS|Derfflinger||2}} at Jutland, the only battlecruisers available for the operation were {{SMS|Von der Tann||2}} and {{SMS|Moltke||2}}, which were joined by ''Markgraf'', ''Grosser Kurfürst'', and the new battleship {{SMS|Bayern||2}}.{{sfn|Staff|p=35}} The British were aware of the German plans, and sortied the Grand Fleet to meet them. By 14:35, Scheer had been warned of the Grand Fleet's approach and, unwilling to engage the whole of the Grand Fleet just 11&nbsp;weeks after the decidedly close engagement at Jutland, turned his forces around and retreated to German ports.{{sfn|Massie|p=683}}

''Markgraf'' was present for the uneventful advance in the direction of Sunderland on 18–20 October. Unit training with the III Squadron followed from 21 October to 2 November. Two days later, the ship formally rejoined III Squadron. On the 5th, a pair of U-boats grounded on the Danish coast. Light forces were sent to recover the vessels, and III Squadron, which was in the North Sea en route to Wilhelmshaven, was ordered to cover them.{{sfn|Staff|p=35}} During the operation, the British submarine {{HMS|J1||2}} torpedoed both ''Grosser Kurfürst'' and ''Kronprinz'' and caused moderate damage.{{sfn|Preston|p=80}} For most of 1917, ''Markgraf'' was occupied with guard duties in the North Sea, interrupted only by a refit period in January and periodic unit training in the Baltic.{{sfn|Staff|p=35}}

=== Operation Albion ===
[[File:Bundesarchiv Bild 146-1970-074-34, Besetzung der Insel Ösel, Truppenanlandung.jpg|thumb|German troops landing at Ösel|alt=A small boat packed with soldiers passes in front of a cruiser and several transport ships]]
[[File:Operation Albion Map.jpg|thumb|right|Map showing the movements of the German Navy and Army to seize Riga and the islands in the Gulf]]
{{main|Operation Albion}}

In early September 1917, following the German conquest of the Russian port of [[Riga]], the German navy decided to eliminate the Russian naval forces that still held the [[Gulf of Riga]]. The ''Admiralstab'' (Navy High Command) planned an operation to seize the Baltic island of [[Saaremaa|Ösel]], and specifically the Russian gun batteries on the [[Sõrve Peninsula|Sworbe Peninsula]].{{sfn|Halpern|p=213}} On 18 September, the order was issued for a joint operation with the army to capture Ösel and [[Muhu|Moon]] Islands; the primary naval component was to comprise the [[flagship]], ''Moltke'', along with the III and IV&nbsp;Battle Squadrons of the High Seas Fleet. The II&nbsp;Squadron consisted of the four ''König''-class ships, and was by this time augmented with the new battleship ''Bayern''. The IV&nbsp;Squadron consisted of the five ''Kaiser''-class battleships. Along with nine light cruisers, three torpedo boat flotillas, and dozens of [[naval mine|mine]] warfare ships, the entire force numbered some 300 ships, supported by over 100 aircraft and six [[zeppelin]]s. The invasion force amounted to approximately 24,600 [[Officer (armed forces)|officers]] and [[enlisted men]].{{sfn|Halpern|pp=214–215}}

Opposing the Germans were the old Russian [[pre-dreadnought]]s {{ship|Russian battleship|Slava||2}} and {{ship|Russian battleship|Tsesarevich||2}}, the [[armored cruiser]]s {{ship|Russian cruiser|Bayan|1907|2}}, {{ship|Russian cruiser|Admiral Makarov||2}}, and {{ship|Russian cruiser|Diana|1899|2}}, 26 destroyers, and several torpedo boats and gunboats. Three British [[British C-class submarine|C-class]] submarines where also stationed in the Gulf. The Irben Strait, the main southern entrance to the Gulf of Riga, was heavily mined and defended by a number of coastal artillery batteries. The garrison on Ösel numbered nearly 14,000 men, though by 1917 it had been reduced to 60 to 70&nbsp;percent strength.{{sfn|Halpern|p=215}}

The operation began on 12 October, when ''Moltke'' and the four ''König''-class ships covered the landing of ground troops by suppressing the shore batteries covering [[Tagga Bay]].{{sfn|Halpern|p=215}} ''Markgraf'' fired on the battery located on Cape Ninnast. After the successful amphibious assault, III Squadron steamed to [[Putziger Wiek]], although ''Markgraf'' remained behind for several days. On the 17th, ''Markgraf'' left Tagga Bay to rejoin her squadron in the [[Gulf of Riga]], but early on the following morning she ran aground at the entrance to [[Kalkgrund]]. The ship was quickly freed, and she reached the III Squadron anchorage north of Larina Bank on the 19th. The next day, ''Markgraf'' steamed to Moon Sound, and on the 25th participated in the bombardment of Russian positions on the island of [[Kihnu|Kynö]]. The ship returned to [[Kuressaare|Arensburg]] on 27 October, and two days later was detached from Operation Albion to return to the North Sea.{{sfn|Staff|pp=35–36}}

''Markgraf'' struck a pair of mines in quick succession while in the [[Irben Strait]] and took in {{convert|260|MT|sp=us}} of water. The ship continued on to Kiel via [[Neufahrwasser]] in [[Danzig]]; she then went on to Wilhelmshaven, where the mine damage was repaired. The work was completed at the [[Kaiserliche Werft Wilhelmshaven|Imperial Dockyard]] from 6 to 23 November.{{sfn|Staff|p=35}} After repairs were completed, ''Markgraf'' returned to guard duty in the North Sea. She missed an attempted raid on a British convoy on 23–25 April 1918, as she was in dock in Kiel from 15 March to 5 May for the installation of a new foremast.{{sfn|Staff|pp=35–36}}

=== Fate ===
''Markgraf'' and her three sisters were to have taken part in a [[Naval order of 24 October 1918|final fleet action]] at the end of October 1918, days before the [[Armistice with Germany|Armistice]] was to take effect. The bulk of the High Seas Fleet was to have sortied from their base in Wilhelmshaven to engage the British Grand Fleet. Scheer—by now the [[Grand Admiral]] (''Großadmiral'') of the fleet—intended to inflict as much damage as possible on the British navy in order to obtain a better bargaining position for Germany, despite the expected casualties. However, many of the war-weary sailors felt the operation would disrupt the peace process and prolong the war.{{sfn|Tarrant|pp=280–281}} On the morning of 29 October 1918, the order was given to sail from Wilhelmshaven the following day. Starting on the night of 29 October, sailors on {{SMS|Thüringen||2}} and then on several other battleships, including ''Markgraf'', mutinied.{{sfn|Tarrant|pp=281–282}} The unrest ultimately forced Hipper and Scheer to cancel the operation.{{sfn|Tarrant|p=282}} Informed of the situation, the Kaiser stated, "I no longer have a navy."{{sfn|Herwig|p=252}}

[[File:Internment at Scapa Flow.svg|thumb|400px|Map of the scuttled ships showing ''Markgraf'' (#6); [http://upload.wikimedia.org/wikipedia/en/9/92/Internment_at_Scapa_Flow.svg click] for a larger view|alt=A map designating the locations where the German ships were sunk.]]

Following the capitulation of Germany in November 1918, most of the High Seas Fleet ships, under the command of Rear Admiral [[Ludwig von Reuter]], were interned in the British naval base in Scapa Flow.{{sfn|Tarrant|p=282}} Prior to the departure of the German fleet, Admiral [[Adolf von Trotha]] made clear to von Reuter that he could not allow the Allies to seize the ships, under any conditions.{{sfn|Herwig|p=256}} The fleet rendezvoused with the British [[light cruiser]] {{HMS|Cardiff|D58|2}}, which led the ships to the Allied fleet that was to escort the Germans to Scapa Flow. The massive flotilla consisted of some 370 British, American, and French warships.{{sfn|Herwig|pp=254–255}} Once the ships were interned, their guns were disabled through the removal of their [[breech block]]s, and their crews were reduced to 200 officers and enlisted men.{{sfn|Herwig|p=255}}

The fleet remained in captivity during the negotiations that ultimately produced the [[Treaty of Versailles]]. Von Reuter believed that the British intended to seize the German ships on 21 June 1919, which was the deadline for Germany to have signed the peace treaty. Unaware that the deadline had been extended to the 23rd, Reuter [[Scuttling of the German fleet in Scapa Flow|ordered the ships to be sunk]] at the first opportunity. On the morning of 21 June, the British fleet left Scapa Flow to conduct training maneuvers, and at 11:20 Reuter transmitted the order to his ships.{{sfn|Herwig|p=256}} ''Markgraf'' sank at 16:45.{{sfn|Gröner|p=28}} The British soldiers in the guard detail panicked in their attempt to prevent the Germans from scuttling the ships;{{sfn|Herwig|p=257}} they shot and killed ''Markgraf''{{'}}s captain,   Walter Schumann, who was in a lifeboat,{{sfn|Koop & Schmolke|p=131}} and an enlisted man.{{sfn|Staff|p=36}} In total, the guards killed nine Germans and wounded twenty-one. The remaining crews, totaling some 1,860 officers and enlisted men, were imprisoned.{{sfn|Herwig|p=257}}

''Markgraf'' was never raised for scrapping, unlike most of the other capital ships that were scuttled.{{sfn|Gröner|p=28}} ''Markgraf'' and her two sisters had sunk in deeper water than the other capital ships, which made any salvage attempt more difficult. The outbreak of World War II in 1939 put a halt to all salvage operations, and after the war it was determined that salvaging the deeper wrecks was financially impractical.{{sfn|Butler|p=229}} The rights to future salvage operations on the wrecks were sold to Britain in 1962.{{sfn|Gröner|p=28}} Owing to the fact that the steel that composed their hulls was produced before the advent of nuclear weapons, ''Markgraf'' and her sisters are among the few accessible sources of [[low-background steel]], which has occasionally been removed for use in scientific devices.{{sfn|Butler|p=229}} ''Markgraf'' and the other vessels on the bottom of Scapa Flow are a popular dive site, and are protected by a policy barring divers from recovering items from the wrecks.{{sfn|Konstam|p=187}}

== Notes ==
'''Footnotes'''
{{notes|35em
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''", or {{lang-en|His Majesty's Ship}}.
}}

{{efn
| name = provisional names
| German warships were ordered under provisional names. For new additions to the fleet, they were given a single letter; for those ships intended to replace older or lost vessels, they were ordered as "Ersatz (name of the ship to be replaced)". See {{harvnb|Gröner|p=27}}.
}}

{{efn
| name = compass points
| The compass can be divided into 32 points, each corresponding to 11.25 degrees. A two-point turn to port would alter the ships' course by 22.5 degrees.
}}

{{efn
| name = torpedo attacks
| V. E. Tarrant states that {{HMS|Nicator||2}} and {{HMS|Nestor|1915|2}} launched four torpedoes against ''Grosser Kurfürst'' and ''König'', though all four missed their targets. John Campbell, however, states that these two ships instead targeted {{SMS|Derfflinger||2}} and {{SMS|Lützow||2}}, and it was {{HMS|Moorsom|1914|2}} that fired the four torpedoes, though at ''Grosser Kurfürst'' and ''Markgraf''. See: {{harvnb|Tarrant|p=114}}, and {{harvnb|Campbell ''Jutland''|pp=55–56}}, respectively.
}}

}}

'''Citations'''
{{reflist|15em}}

== References ==
{{portal|Battleships}}
{{refbegin}}
* {{cite book
  | last = Butler
  | first = Daniel Allen
  | year = 2006
  | title = Distant Victory: The Battle of Jutland and the Allied Triumph in the First World War
  | publisher = Greenwood Publishing Group
  | location = Westport, CT
  | isbn = 978-0-275-99073-2
  | ref = {{sfnRef|Butler}}
  }}
* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell ''Jutland''}}
  }}
* {{cite book
  | last = Campbell
  | first = John
  | year = 1987
  | chapter = Germany 1906–1922
  | pages = 28–49
  | editor-last = Sturton
  | editor-first = Ian
  | title = Conway's All the World's Battleships: 1906 to the Present
  | location = London
  | publisher = Conway Maritime Press
  | isbn = 978-0-85177-448-0
  | ref = {{sfnRef|Campbell "Germany 1906–1922"}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = [[Annapolis]]
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last = Konstam
  | first = Angus
  | year = 2002
  | title = The History of Shipwrecks
  | publisher = Lyons Press
  | location = New York City
  | isbn = 978-1-58574-620-0
  | ref = {{sfnRef|Konstam}}
  }}
* {{cite book
  | last1 = Koop
  | first1 = Gerhard
  | last2 = Schmolke
  | first2 = Klaus-Peter
  | year = 1999
  | language = German
  | title = Von der Nassau&nbsp;— zur König-Klasse
  | publisher = Bernard & Graefe Verlag
  | location = Bonn
  | isbn = 978-3-7637-5994-1
  | ref = {{sfnRef|Koop & Schmolke}}
  }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = [[New York City]]
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
  }}
* {{cite book
  | last = Preston
  | first = Anthony
  | authorlink = Antony Preston (naval historian)
  | year = 1972
  | title = Battleships of World War I: An Illustrated Encyclopedia of the Battleships of all Nations, 1914–1918
  | location = Harrisburg, PA
  | publisher = Stackpole Books
  | isbn = 978-0-8117-0211-9
  | oclc = 402382
  | ref = {{sfnRef|Preston}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918 (Volume 2)
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-468-8
  | oclc = 449845203
  | ref = {{sfnRef|Staff}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}
{{refend}}

{{König class battleship}}
{{1919 shipwrecks}}
{{Featured article}}

{{DEFAULTSORT:Markgraf}}
[[Category:1913 ships]]
[[Category:König-class battleships]]
[[Category:Ships built in Bremen (state)]]
[[Category:World War I battleships of Germany]]
[[Category:World War I warships scuttled in Scapa Flow]]
[[Category:Maritime incidents in 1919]]