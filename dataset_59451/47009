{{Infobox book <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Children of the Jacaranda Tree
| title_orig   =
| translator   =
| image        = 
| caption= 1st edition
| author       = [[Sahar Delijani]]
| country      = United States
| language     = English
| genre        = Literary Fiction
| publisher    = Atria/Simon&Schuster
| release_date = 2013
| media_type   = Print (hardback & ebook)
| ISBN         = 9781476709093
| pages        = 288
| preceded_by  =
| followed_by  =
}}
'''''Children of the Jacaranda Tree''''' is the internationally acclaimed [[debut novel]] of Iranian writer [[Sahar Delijani]]. Partially inspired by the writer's family history, it is a story about one of the worst best kept secrets of post-revolutionary Iran: the [[1988 executions of Iranian political prisoners]]. The novel depicts not only the lives of the victims but also those of their families and above all their children. The novel is a poignant attempt at describing the genesis of a dictatorship, how it begins, how it affects a people, and what it means to resist it.

''Children of the Jacaranda Tree'' is Delijani's first published and fourth written novel. In her words, "the other novels were more like homework. I had to learn how to write through writing." Completed in 2012, ''Children of the Jacaranda Tree'' took three years to complete. The potential of the story was recognized immediately when a few days after its submission to the publishing world, simultaneous auctions were taking place in the US, UK, Italy, France, Brazil, Germany and Spain. Delijani received six digit sums in advances and rights to the book were sold in 27 territories.<ref>{{cite web |url=https://www.theguardian.com/books/2012/apr/16/publishers-spending-london-book-fair |title=Publishers go on spending spree ahead of London book fair |work=Theguardian |author=Alison Flood |date=April 16, 2012 |accessdate=April 12, 2012}}</ref>
Praised by [[Khaled Hosseini]] and named one of the top books of 2013 by [[The Kansas City Star]],<ref>{{cite web |url=http://www.kansascity.com/2013/11/29/4658194/the-stars-top-books-of-the-year.html |title=The Star's Top Books of the Year |work=The Kansas City Star |date=April 16, 2012 |accessdate=December 2, 2013}}</ref> the novel has been published in 75 countries and translated into 28 languages.

== Plot ==
''Children of the Jacaranda Tree'' is composed of seven different narratives and told from various points of views. The first half of the novel tells the story of political activists imprisoned in the 1980s by the newly established Islamic regime and their children, some born inside prison, some at home forced to watch as their parents are taken away. These children are raised by grandparents and aunts while their parents languish in prison. 
The second half of the novel is set twenty years later, during the [[2009 Iranian election protests]] and the [[Iranian Green Movement]]. The children born in the first half of the book, now grown up, have to face their own decisions, whether they will be bound by their parents' past of a revolution gone astray or whether they will be able to break free, make a new beginning both for themselves and for their country.

== Reception ==

[[Khaled Hosseini]], the author of the Kite Runner says, "Set in post-revolutionary Iran, Delijani’s gripping novel is a blistering indictment of tyranny, a poignant tribute to those who bear the scars of it, and a celebration of the human’s heart’s eternal yearning for freedom."

[[The Guardian]] says "Children of the Jacaranda Tree is a novel with a great weight of history attached to it... This is not an "explaining Iran to those who don't know it" book, but something far more visceral."
"<ref>{{cite news|url=https://www.theguardian.com/books/2013/jul/12/children-jacaranda-tree-delijani-review|title=Children of the Jacaranda Tree-review|last=Shamsie|first=Kamila|date=July 12, 2013|work=The Guardian|accessdate=July 12, 2012}}</ref>

[[Kirkus Review]] says, "Delijani is exceptionally talented as a writer, and the subject matter is both compelling and timely...Delijani falls back on her family’s personal experience to write this searing and somber slice-of-life novel, centered around children whose parents were singled out for persecution by the Iranian government, and scores a win with her grittiness and uncompromising realism."<ref>{{cite news|title=Children of the Jacaranda Tree by Sahar Delijani – review|date=March 3, 2013|work=Kirkus Review|url=https://www.kirkusreviews.com/book-reviews/sahar-delijani/children-of-jacaranda-tree/}}</ref>

[[The Seattle Times]] says, "Sahar Delijani’s debut novel, brutally honest but lyrical, depicts the upheaval of post-revolutionary Iran from 1983 to 2011...It is as though Delijani is saying that even in the most miserable situation, we have a store of beauty inside us. All we have to do is look."<ref>{{cite news|url=http://seattletimes.com/html/books/2021284048_delijanijacarandatreexml.html|title=The Legacy of Repression on Generations of Iranians|last=Kirchner|first=Bharti|date=June 28, 2013|work=Book review|publisher=Kirkus Review|accessdate=June 28, 2013}}</ref>

[[The New Internationalist]] says "Spanning the years 1983 to 2011, Children of the Jacaranda Tree offers a personal and often painful look at post-revolutionary Iranian history, from the height of the Iran-Iraq war to the 2009 election protests and beyond… Evocative and emotive – and highly recommended."

== References ==
{{Reflist}}

{{DEFAULTSORT:Children of the Jacaranda Tree}}
[[Category:2013 novels]]
[[Category:Iranian novels]]
[[Category:Literature by women]]
[[Category:2013 in fiction]]