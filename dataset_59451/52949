{{about|the academic journal|the academic discipline|Technology and society}}
{{refimprove|date=February 2013}}
{{Infobox journal
| cover = [[Image:Technology and culture.gif]]
| discipline = [[Cultural studies]]; [[History of technology]]; [[Science, technology and society]]
| abbreviation= Technol. Cult.
| editor = Suzanne Moon
| publisher = [[Johns Hopkins University Press]] for the [[Society for the History of Technology]]
| country = United States
| history = 1959-present
| frequency = Quarterly
| website = http://www.historyoftechnology.org/publications.html
| link1 = http://muse.jhu.edu/journals/technology_and_culture/
| link1-name = Online access at [[Project MUSE]]
| ISSN = 0040-165X
| eISSN = 1097-3729
| OCLC = 1640126
| LCCN = 62025340
| JSTOR = 0040165X
}}
'''''Technology and Culture''''' is a quarterly [[academic journal]] founded in 1959. It is an official publication of the [[Society for the History of Technology]] (SHOT), whose members routinely refer to it as "T&C." Besides scholarly articles and critical essays, the journal publishes reviews of books and museum exhibitions. Occasionally, the journal publishes thematic issues; topics have included patents, gender and technology, and ecology. ''Technology and Culture'' has had three past [[editors-in-chief]]: [[Melvin Kranzberg]] (1959–1981), Robert C. Post (1982–1995), and John M. Staudenmaier (1996–2010). Since 2011 the journal has been edited at the [[University of Oklahoma]] by Prof. Suzanne Moon. Managing editors have included Joan Mentzer, Joseph M. Schultz, David M. Lucsko, and Peter Soppelsa.

In its inaugural issue, editor Melvin Kranzberg set out a threefold educational mission for the journal: "to promote the scholarly study of the history of technology, to show the relations between technology and other elements of culture, and to make these elements of knowledge available and comprehensible to the educated citizen." No journal then in existence had as its primary focus the history of technology and its relations with society and culture. To adequately address these topics in all their complexity, a truly interdisciplinary approach was needed. And this was to be the unique contribution of ''Technology and Culture''.<ref>{{cite journal|last=Kranzberg|first=Melvin|title=At the Start|journal=Technology and Culture|date=Winter 1959|volume=1|issue=1|pages=9}}</ref>

== See also ==
* [[History of technology]]
* [[Technology and society]]

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.historyoftechnology.org/etc/index.html}}
* https://www.press.jhu.edu/journals/technology_and_culture/
* http://www.techculture.org/

{{Science and technology studies|state=collapsed}}

[[Category:Cultural journals]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1959]]
[[Category:English-language journals]]
[[Category:Science and technology studies journals]]