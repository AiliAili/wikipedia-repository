[[File:Triple Bottom Line graphic.jpg|thumb|400px|Graphic describing the three types of bottom lines]]

'''Triple bottom line''' (or otherwise noted as '''TBL''' or '''3BL''') is an [[accounting]] framework with three parts: social, environmental (or ecological) and financial. Many organizations have adopted the TBL framework to evaluate their performance in a broader perspective to create greater business value.<ref name = TBF-IBR>Slaper, Timothy F. and Hall, Tanya J. (2011). [http://www.ibrc.indiana.edu/ibr/2011/spring/article2.html "The Triple Bottom Line: What Is It and How Does It Work?"] ''Indiana Business Review''. Spring 2011, Volume 86, No. 1.</ref> The term was coined by [[John Elkington (business author)|John Elkington]] in 1994.<ref>{{cite news|title=Triple Bottom Line|url=http://www.economist.com/node/14301663|accessdate=14 August 2014|publisher=The Economist|date=November 17, 2009}}</ref>

In traditional business accounting and common usage, the "[[bottom line]]" refers to either the "profit" or "loss", which is usually recorded at the very bottom line on a statement of revenue and expenses. Over the last 50 years, environmentalists and [[social justice]] advocates have struggled to bring a broader definition of bottom line into public consciousness by introducing [[full cost accounting]]. For example, if a corporation shows a monetary profit, but their asbestos mine causes thousands of deaths from asbestosis, and their [[copper mine]] pollutes a river, and the government ends up spending taxpayer money on health care and river clean-up, how do we perform a full societal [[cost benefit analysis]]? The triple bottom line adds two more "bottom lines”: social and environmental (ecological) concerns.<ref>[http://www.goethe.de/ges/umw/dos/nac/den/en3106180.htm Sustainability – From Principle To Practice] ''[[Goethe-Institut]]'', March 2008.</ref> With the ratification of the [[United Nations]] and [[ICLEI]] TBL standard for urban and community accounting in early 2007,<ref>[http://www.unep.org/delc/Portals/119/industrryRoleOfIndclean.pdf Enhancing the role of industry through for example, private-public partnerships], May 2011. [[United Nations Environment Programme]]</ref> this became the dominant approach to [[public sector]] full cost accounting.  Similar UN standards apply to [[natural capital]] and [[human capital]] measurement to assist in measurements required by TBL, e.g. the EcoBudget standard for reporting [[ecological footprint]]. The TBL seems to be fairly widespread in South African media, as found in a 1990-2008 study of worldwide national newspapers.<ref>What the Papers Say: Trends in Sustainability'' [[Ralf Barkemeyer et al]]'', Spring 2009, Greenleaf Publishing.</ref>

An example of an organization seeking a triple bottom line would be a [[social enterprise]] run as a non-profit, but earning income by offering opportunities for handicapped people who have been labelled "unemployable", to earn a living recycling. The organization earns a profit, which is controlled by a volunteer Board, and ploughed back into the community. The social benefit is the meaningful employment of disadvantaged citizens, and the reduction in the society's welfare or disability costs. The environmental benefit comes from the recycling accomplished. In the [[private sector]], a commitment to  Corporate social responsibility (CSR) implies a commitment to transparent reporting about the business' material impact for good on the environment and people. Triple bottom line is one framework for reporting this material impact. This is distinct from the more limited changes required to deal only with ecological issues. The triple bottom line has also been extended to encompass four pillars, known as the quadruple bottom line (QBL). The fourth pillar denotes a future-oriented approach (future generations, intergenerational equity, etc.). It is a long-term outlook that sets sustainable development and sustainability concerns apart from previous social, environmental, and economic considerations.<ref>[http://www.ccsenet.org/journal/index.php/jms/article/view/27508/18663 SURF Framework for a Sustainable Economy]'' [[Marilyn Waite]]'', November 2013.</ref>

The challenges of putting the TBL into practice relate to the measurement of social and ecological categories. Despite this, the TBL framework enables organizations to take a longer-term perspective and thus evaluate the future consequences of decisions.<ref name = TBF-IBR/>

==Definition==
[[Sustainable development]] was defined by the [[Brundtland Commission]] of the United Nations in 1987. Triple bottom line (TBL) accounting expands the traditional reporting framework to take into account social and environmental performance in addition to financial performance. In 1981, [[Social enterprise|Freer Spreckley]] first articulated the triple bottom line in a publication called 'Social Audit - A Management Tool for Co-operative Working'.<ref>Freer Spreckley 1981 [http://www.locallivelihoods.com/cmsms/uploads/PDFs/Social%20Audit%20-%20A%20Management%20Tool.pdf Social Audit - A Management Tool for Co-operative Working]</ref> In this work, he argued that enterprises should measure and report on financial performance, social wealth creation, and environmental responsibility. 

The phrase "triple bottom line" was articulated more fully by [[John Elkington (business author)|John Elkington]] in his 1997 book ''Cannibals with Forks: the Triple Bottom Line of 21st Century Business.''<ref name = TBF-IBR/><ref>Brown, D., J. Dillard and R.S. Marshall. (2006) [http://www.recercat.net/bitstream/2072/2223/1/UABDT06-2.pdf "Triple Bottom Line: A business metaphor for a social construct."] Portland State University, School of Business Administration. Retrieved on: 2007-07-18.</ref> A ''Triple Bottom Line Investing'' group advocating and publicizing these principles was founded in 1998 by [[Robert J. Rubinstein]].

For reporting their efforts companies may demonstrate their commitment to [[Corporate social responsibility]] (CSR) through the following:

*Top-level involvement (CEO, Board of Directors)
*Policy Investments
*Programs
*Signatories to voluntary standards
*Principles (UN Global Compact-Ceres Principles)
*Reporting (Global Reporting Initiative)

The concept of TBL demands that a company's responsibility lies with [[stakeholder (corporate)|stakeholders]] rather than [[shareholders]]. In this case, "stakeholders" refers to anyone who is influenced, either directly or indirectly, by the actions of the firm. According to the [[stakeholder theory]], the business entity should be used as a vehicle for coordinating stakeholder interests, instead of maximizing shareholder (owner) profit. A growing number of financial institutions incorporate a triple bottom line approach in their work. It is at the core of the business of banks in the [[Global Alliance for Banking on Values]], for example.

The [[Detroit]]-based [[Avalon International Breads]] interprets the triple bottom line as consisting of "Earth", "Community", and "Employees".<ref name="Avalon">{{Cite web|url = http://www.avalonbreads.net/about-us/triple-bottom-line/|title = Triple Bottom Line: Earth, Community, Employees|date = |accessdate = 27 February 2015|website = |publisher = Avalon International Breads|last = |first = }}</ref>

==Bottom lines==
{{refimprove section|date=April 2014}}
The triple bottom line consists of social equity, economic, and environmental factors. "People, planet and profit" succinctly describes the Triple bottom line and the goal of [[sustainability]]. The phrase, "people, planet, profit", was coined by John Elkington in 1994 while at Sustain Ability, and was lateras the title of the Anglo-Dutch oil company Shell's first sustainability report in 1997.  As a result, one country in which the 3P concept took deep root was The Netherlands.

'''"People"''' pertains to fair and beneficial business practices toward labour and the community and region in which a corporation conducts its business. A TBL company conceives a reciprocal [[social structure]] in which the well-being of corporate, labour and other stakeholder interests are interdependent.

An enterprise dedicated to the triple bottom line seeks to provide benefit to many constituencies and not to exploit or endanger any group of them. The "upstreaming" of a portion of profit from the marketing of finished goods back to the original producer of raw materials, for example, a farmer in [[fair trade]] agricultural practice, is a common feature.  In concrete terms, a TBL business would not use child labour and monitor all contracted companies for child labour exploitation, would pay fair salaries to its workers, would maintain a safe work environment and tolerable working hours, and would not otherwise exploit a community or its labour force. A TBL business also typically seeks to "give back" by contributing to the strength and growth of its community with such things as health care and education. Quantifying this bottom line is relatively new, problematic and often subjective.  The [[Global Reporting Initiative]] (GRI) has developed guidelines to enable corporations and [[NGO]]s alike to comparably report on the social impact of a business.

'''"Planet"''' ([[natural capital]]) refers to sustainable environmental practices. A TBL company endeavors to benefit the natural order as much as possible or at the least do no harm and minimise environmental impact.  A TBL endeavour reduces its [[ecological footprint]] by, among other things, carefully managing its consumption of energy and non-renewables and reducing manufacturing waste as well as rendering waste less [[Toxicity|toxic]] before disposing of it in a safe and legal manner. "[[Cradle to grave]]" is uppermost in the thoughts of TBL manufacturing businesses, which typically conduct a [[life cycle assessment]] of products to determine what the true environmental cost is from the growth and harvesting of raw materials to manufacture to distribution to eventual disposal by the end user.

Currently, the cost of disposing of non-degradable or toxic products is borne financially by governments and environmentally by the residents near the disposal site and elsewhere. In TBL thinking, an enterprise which produces and markets a product which will create a waste problem should not be given a free ride by society. It would be more equitable for the business which manufactures and sells a problematic product to bear part of the cost of its ultimate disposal.

Ecologically destructive practices, such as overfishing or other endangering depletions of resources are avoided by TBL companies. Often environmental sustainability is the more profitable course for a business in the long run. Arguments that it costs more to be environmentally sound are often specious when the course of the business is analyzed over a period of time. Generally, sustainability reporting metrics are better quantified and standardized for environmental issues than for social ones. A number of respected reporting institutes and registries exist including the Global Reporting Initiative, CERES, Institute 4 Sustainability and others.

The eco bottom line is akin to the concept of [[Eco-capitalism]].<ref>{{cite book|last=[[Paul Ekins|Ekins]]|first=Paul|title=The Gaia Atlas of Green Economics|year=1992|publisher=Anchor Books|isbn=0-385-41914-7|page=191}}</ref>

'''"Profit"''' is the economic value created by the organization after deducting the cost of all inputs, including the cost of the capital tied up. It therefore differs from traditional accounting definitions of profit. In the original concept, within a sustainability framework, the "profit" aspect needs to be seen as the real economic benefit enjoyed by the host society.  It is the real economic impact the organization has on its economic environment.  This is often confused to be limited to the internal profit made by a company or organization (which nevertheless remains an essential starting point for the computation).  Therefore, an original TBL approach cannot be interpreted as simply traditional corporate accounting profit ''plus'' social and environmental impacts unless the "profits" of other entities are included as a social benefit.

==Subsequent development==

Following the initial publication of the triple bottom line concept, students and practitioners have sought greater detail in how pillars can be evaluated.

The People concept for example can be viewed in three dimensions – the organisation needs, the personal needs and the community issues associated with supplying future people into the business.

Equally, Profit is a function of both a healthy sales stream, which needs a high focus on customer service, coupled with the adoption of a strategy to develop new customers to replace those that die away.

And Planet can be divided into a multitude of subdivisions, although Reduce, Reuse and Recycle is a succinct way of steering though.

==Supporting arguments==
{{refimprove section|date=April 2014}}
The following business-based arguments support the concept of TBL:

*Reaching untapped market potential: TBL companies can find financially profitable niches which were missed when money alone was the driving factor. Examples include:

# Adding [[ecotourism]] or [[geotourism]] to an already rich tourism market such as the [[Dominican Republic]]
# Developing profitable methods to assist existing NGOs with their missions such as fundraising, reaching clients, or creating networking opportunities with multiple NGOs
# Providing products or services which benefit underserved populations and/or the environment which are also financially profitable.

*Adapting to new business sectors: While the number of [[social enterprises]] is growing,<ref>{{cite web|title=The State of Social Enterprise|url=http://www.socialenterprise.org.uk/uploads/files/2013/07/the_peoples_business.pdf|website=Social Enterprise UK|publisher=SEUK|accessdate=15 July 2015}}</ref> and with the entry of the [[B Corporation|B Corp movement]],<ref>{{cite web|last1=Trapp|first1=Roger|title=Business Leaders Urged To Find A Purpose In Life|url=http://www.forbes.com/sites/rogertrapp/2015/03/29/business-leaders-urged-to-find-a-purpose-in-life/|website=Forbes|accessdate=26 August 2015}}</ref> there is more demand from consumers and investors for an accounting for social and environmental impact.<ref>{{cite web|title=Deloitte Millennial Survey 2015|url=http://www2.deloitte.com/content/dam/Deloitte/global/Documents/About-Deloitte/gx-wef-2015-millennial-survey-executivesummary.pdf|publisher=Deloitte|accessdate=15 July 2015}}</ref>  For example, [[Fair trade|Fair Trade]] and Ethical Trade companies require ethical and sustainable practices from all of their suppliers and service providers.

[[Fiscal policy]] of governments usually claims to be concerned with identifying social and natural deficits on a less formal basis. However, such choices may be guided more by [[ideology]] than by [[economics]]. The primary benefit of embedding one approach to measurement of these deficits would be first to direct [[monetary policy]] to reduce them, and eventually achieve a global [[monetary reform]] by which they could be systematically and globally reduced in some uniform way.

The argument is that the [[Earth]]'s [[carrying capacity]] is at risk, and that in order to avoid catastrophic breakdown of [[climate]] or [[ecosystem]]s, there is need for comprehensive reform of [[Global financial system|global financial institutions]] similar in scale to what was undertaken at [[Bretton Woods conference|Bretton Woods]] in 1944.

With the emergence of an externally consistent [[green economics]] and agreement on definitions of potentially contentious terms such as [[full-cost accounting]], [[natural capital]] and [[social capital]], the prospect of formal metrics for ecological and social loss or risk has grown less remote since the 1990s.{{Citation needed|date=August 2015}}

In the [[United Kingdom]] in particular, the London Health Observatory has undertaken a formal programme to address social deficits via a fuller understanding of what "social capital" is, how it functions in a real [[community]] (that being the [[City of London]]), and how losses of it tend to require both [[financial capital]] and significant political and social attention from [[volunteer]]s and professionals to help resolve.  The data they rely on is extensive, building on decades of statistics of the [[Greater London Council]] since [[World War II]].  Similar studies have been undertaken in [[North America]].

Studies of the [[value of Earth]] have tried to determine what might constitute an ecological or natural life deficit. The [[Kyoto Protocol]] relies on some measures of this sort, and actually relies on some [[value of life]] calculations that, among other things, are explicit about the ratio of the price of a human life between developed and developing nations (about 15 to 1). While the motive of this number was to simply assign responsibility for a cleanup, such stark honesty opens not just an economic but political door to some kind of negotiation &mdash; presumably to reduce that ratio in time to something seen as more equitable. As it is, people in developed nations can be said to benefit 15 times more from [[ecological devastation]] than in developing nations, in pure financial terms.  According to the [[Intergovernmental Panel on Climate Change|IPCC]], they are thus obliged to pay 15 times more per life to avoid a loss of each such life to [[climate change]] &mdash; the [[Kyoto Protocol]] seeks to implement exactly this formula, and is therefore sometimes cited as a first step towards getting nations to accept formal [[legal liability|liability]] for damage inflicted on ecosystems shared globally.

Advocacy for triple bottom line reforms is common in [[Green Parties]]. Some of the measures undertaken in the [[European Union]] towards the [[Euro]] currency integration standardize the reporting of ecological and social losses in such a way as to seem to endorse in principle the notion of unified accounts, or [[unit of account]], for these deficits.

==Criticism==
While many people agree with the importance of good social conditions and preservation of the environment, there are also many who disagree with the triple bottom line as the way to enhance these conditions. The following are the reasons why:

* ''Reductive method'': Concurrently the environment comes to be treated as an externality or background feature, an externality that tends not to have the human dimension build into its definition. Thus, in many writings, even in those critical of the triple-bottom-line approach, the social becomes a congeries of miscellaneous considerations left over from the other two prime categories.<ref name="academia.edu">{{Cite journal | year=2010 | last1= Scerri | first1= Andy | last2= James | first2= Paul | authorlink2= Paul James (academic) | title= Accounting for sustainability: Combining qualitative and quantitative research in developing ‘indicators’ of sustainability | url= http://www.academia.edu/3230887/Accounting_for_Sustainability_Combining_Qualitative_and_Quantitative_Research_in_Developing_Indicators | journal= International Journal of Social Research Methodology | volume= 13 | issue= 1 | pages= 41–53}}</ref> Alternative approaches, such as [[Circles of Sustainability]],<ref>Paul James and Andy Scerri, ‘Auditing Cities through Circles of Sustainability’, Mark Amen, Noah J. Toly, Patricia L. Carney and Klaus Segbers, eds, ''Cities and Global Governance'', Ashgate, Farnham, 2011, pp. 111–36.</ref> that treat the economic as a social domain, alongside and in relation to the ecological, the political and the cultural are now being considered as more appropriate for understanding institutions, cities and regions.<ref name="academia.edu"/><ref>{{Cite journal | last1 = Scerri | first1 = A. | doi = 10.1016/j.ecolecon.2012.02.027 | title = Ends in view: The capabilities approach in ecological/sustainability economics | journal = Ecological Economics | volume = 77 | pages = 7–10 | year = 2012 | pmid =  | pmc = }}</ref><ref>Liam Magee, & Andy Scerri (2012) ‘From Issues to Indicators: A Response to Grosskurth and Rotmans’, Local Environment 17(8): 915-933.</ref>
* ''[[Social inertia|Inertia]]'': The difficulty of achieving global agreement on [[simultaneous policy]] may render such measures at best advisory, and thus unenforceable. For example, people may be unwilling to undergo a [[Depression (economics)|depression]] or even sustained [[recession]] to replenish lost [[ecosystem]]s. {{Citation needed|date=March 2014}}
* ''Application'': According to Fred Robins' ''The Challenge of TBL: A Responsibility to Whom?'' one of the major weaknesses of the TBL framework is its ability to be applied in the practical world.
* ''Equating ecology with environment'': TBL is seen to be disregarding ecological sustainability with environmental effects, where in reality both economic and social viability is dependent on environmental well being. While [[greenwashing]] is not new, its use has increased over recent years to meet consumer demand for environmentally friendly goods and services. The problem is compounded by lax enforcement by regulatory agencies such as the Federal Trade Commission in the United States, the Competition Bureau in Canada, and the Committee of Advertising Practice and the Broadcast Committee of Advertising Practice in the United Kingdom. Critics of the practice suggest that the rise of greenwashing, paired with ineffective regulation, contributes to consumer skepticism of all green claims, and diminishes the power of the consumer in driving companies toward greener solutions for manufacturing processes and business operation.
* ''Time dimension'': While the triple bottom line incorporates the social, economical and environmental (People, Planet, Profit) dimensions of sustainable development, it does not explicitly address the fourth dimension: time. The time dimension focuses on preserving current value in all three other dimensions for later. This means assessment of short term, longer term and long term consequences of any action.<ref>Lozano, R. (2012). "Towards better embedding sustainability into companies’ systems: an analysis of voluntary corporate initiatives," ''Journal of Cleaner Production'' 25 pp. 14-26</ref>
*This is summarised below as:
** attempting to divert the attention of regulators and deflating pressure for regulatory change;
** seeking to persuade critics, such as non-government organisations, that they are both well-intentioned and have changed their ways;
** seeking to expand market share at the expense of those rivals not involved in greenwashing; this is especially attractive if little or no additional expenditure is required to change ''performance''; alternatively, a company can engage in greenwashing in an attempt to narrow the perceived 'green' advantage of a rival;
** reducing staff turnover and making it easier to attract staff in the first place;
** making the company seem attractive for potential investors, especially those interested in ethical investment or socially responsive investment.

==Legislation==
A focus on people, planet and profit has led to legislation changes around the world, often through [[social enterprise]] or [[Social investing|social investment]] or through the introduction of a new legal form, the [[Community Interest Company]].<ref>{{cite web|title=Community Interest Companies|url=https://www.gov.uk/government/organisations/office-of-the-regulator-of-community-interest-companies|publisher=UK Government|accessdate=15 July 2015}}</ref> In the United States, the [[BCorp]] movement has been part of a call for legislation change to allow and encourage a focus on social and environmental impact, with BCorp a legal form for a company focused on "stakeholders, not just shareholders".<ref>{{cite web|title=Becoming a Legal BCorp|url=https://www.bcorporation.net/become-a-b-corp/how-to-become-a-b-corp/legal-roadmap|publisher=BCorp|accessdate=15 July 2015}}</ref>

In Australia, the triple bottom line was adopted as a part of the State Sustainability Strategy,<ref>[http://web.archive.org/web/20070830000447/http://www.sustainability.dpc.wa.gov.au/docs/Final%20Strategy/SSSFinal.pdf Government of Western Australia. (2003, September). "Hope for the Future: The Western Australia State Sustainability Strategy."] Accessed: August 30, 2013.</ref> and accepted by the [[Western Australian Government|Government of Western Australia]] but its status was increasingly marginalised by subsequent premiers [[Alan Carpenter]] and [[Colin Barnett]] and is in doubt.

==See also==
{{Portal|Sustainable development|Business}}
{{div col|colwidth=22em}}
*[[B Corporation (certification)]]
*[[Bottom of the pyramid]]
*[[Circles of Sustainability]]
*[[Community interest company]]
*[[Conscious business]]
*[[Double bottom line]], a similar concept predating the UN standard
*[[EC3 Global]]
*[[Grassroots Business Fund]]
*[[Impact investing]]
*[[Institute for Sustainable Communication]]
*[[Low-profit limited liability company]]
*[[Permaculture#Core_tenets_and_principles_of_design|Permaculture ethics]]
*[[Social entrepreneurship]]
*[[Triple top line]]
*[[Value network]]
*[[Value network analysis]]
{{div col end}}

== References ==

=== Citations ===
{{Reflist|30em}}

=== Sources ===
* {{Cite book |year = 1997 |author = John Elkington |title = Cannibals with Forks: The Triple Bottom Line of Twenty-First Century Business |publisher= Capstone, Oxford}}
* {{Cite journal |year = 2010 |last1 = Scerri |first1= Andy |last2 = James |first2 = Paul |authorlink2 = Paul James (academic) |title = Accounting for sustainability: Combining qualitative and quantitative research in developing ‘indicators’ of sustainability |url = http://www.academia.edu/3230887/Accounting_for_Sustainability_Combining_Qualitative_and_Quantitative_Research_in_Developing_Indicators |journal = International Journal of Social Research Methodology |volume= 13 |issue = 1 |pages = 41–53}}

== Further reading ==
* ''Social Audit - A Management Tool for Co-operative Working 1981'' by Freer Spreckley [http://www.locallivelihoods.com/cmsms/uploads/PDFs/Social%20Audit%20-%20A%20Management%20Tool.pdf Local Livelihoods Publications]
* ''The Gaia Atlas of Green Economics'' (Gaia Future Series) [Paperback], by Paul Ekins, Anchor Books
* ''Harvard Business Review on Corporate Responsibility'' by [[Harvard Business School Press]]
* ''The Soul of a Business: Managing for Profit and the Common Good'' by Tom Chappell
* ''Capitalism at the Crossroads: The Unlimited Business Opportunities in Solving the World's Most Difficult Problems'' by Professor Stuart L. Hart
* ''The Triple Bottom Line: How Today's Best-Run Companies Are Achieving Economic, Social and Environmental Success—and How You Can Too'' by Andrew W. Savitz and Karl Weber
* ''The Sustainability Advantage: Seven Business Case Benefits of a Triple Bottom Line (Conscientious Commerce)'' by Bob Willard, [[New Society Publishers]] ISBN 978-0-86571-451-9
* ''SURF Framework for a Sustainable Economy'' by Marilyn Waite [http://www.ccsenet.org/journal/index.php/jms/article/view/27508/18663 Journal of Management and Sustainability]

==External links==
* [https://theconversation.com/explainer-what-is-the-triple-bottom-line-22798 Explainer: what is the triple bottom line? - The Conversation] 
* [http://www.ibrc.indiana.edu/ibr/2011/spring/article2.html The Triple Bottom Line: What Is It and How Does It Work? - Indiana Business Review]
* [http://www.isa.org.usyd.edu.au/publications/balance.shtml Balancing Act - A Triple Bottom Line Analysis of the Australian Economy]
* [http://www.c4cr.org/ Citizens for Corporate Redesign] ([[Minnesota]])
* [http://www.triplepundit.com/ Triple Pundit - Blog on Triple Bottom Line] ([[United States]])
* [http://www.corporate-responsibility.org/ Corporate Responsibility] ([[United Kingdom]])
* [http://www.isa.org.usyd.edu.au/research/tbl.shtml TBL Accounting without boundaries - Australian corporate and government experiences]

{{Social accountability|state=expanded}}

{{DEFAULTSORT:Triple Bottom Line}}
[[Category:Welfare economics]]
[[Category:Sustainable business]]
[[Category:Corporate social responsibility]]
[[Category:Accounting terminology]]
[[Category:Accountability]]