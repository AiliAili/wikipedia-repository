{{Infobox journal
| title         = Journal of Germanic Linguistics
| cover         = [[File:Journal of Germanic Linguistics.jpg]]
| editor        = Robert W. Murray
| discipline    = [[Linguistics]], [[Germanic languages]]
| peer-reviewed = 
| language      = English
| former_names  = American Journal of Germanic Linguistics and Literatures
| abbreviation  = 
| publisher     = [[Cambridge University Press]]
| country       = United Kingdom
| frequency     = Quarterly
| history       = 1989–present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://journals.cambridge.org/action/displayJournal?jid=JGL
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 645360234
| LCCN          = 2001233114
| CODEN         = 
| ISSN          = 1470-5427
| eISSN         = 1475-3014
| boxwidth      = 
}}
'''''Journal of Germanic Linguistics''''' is a [[peer review|peer-reviewed]] [[academic journal]] in the field of [[linguistics]]. It is devoted particularly to [[Germanic languages]], including both their historical and contemporary forms. It was established in 1989 as the ''American Journal of Germanic Linguistics and Literatures'' and was published biannually up to 2001, when it acquired the current title in order to reflect its international scope and, at the same time, the narrowing of its focus to exclusively linguistics.<ref name="editorial">{{cite journal | title=Note from the Chair of the Editorial Committee | author=Louden, Mark L. | journal=Journal of Germanic Linguistics | year=2001 | volume=13 | pages=1}}</ref> It is currently published quarterly by [[Cambridge University Press]] on behalf of the [[Society for Germanic Linguistics]] and the [[Forum for the Society for Germanic Language Studies]]. Its [[editor-in-chief]] is Robert W. Murray ([[University of Calgary]]).

==Abstracting and indexing==
This journal is indexed in the following databases:<ref name=masterlist>
Selective databases that list this journal{{Cite web 
  | title =Master Journal List 
  | year = 2011
  | publisher =Thomson Reuters 
 | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1470-5427 
  | format =Online 
  | accessdate =2011-01-13}}</ref>
*[[Social Sciences Citation Index]] 
*[[Arts & Humanities Citation Index]]

==References==
{{reflist}}

==External links==
*[http://journals.cambridge.org/action/displayJournal?jid=JGL Journal homepage]
[[Category:Linguistics journals]]
[[Category:Germanic philology journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1989]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]