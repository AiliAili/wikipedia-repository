{{infobox book <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = The Bride of Lammermoor
| orig title   =
| translator   =
| image        = Charles Robert Leslie - Sir Walter Scott - Ravenswood and Lucy at the Mermaiden's Well - Bride of Lammermoor.jpg
| caption = ''Ravenswood and Lucy<br>at the Mermaiden's Well''<br>by [[Charles Robert Leslie]]
| author       = Sir [[Walter Scott]]
| cover_artist = 
| country      = Scotland
| language     = English, [[Scots language|Lowland Scots]]
| series       = [[Tales of My Landlord]] (3rd series)
| genre        = [[Historical novel]]
| published    = 1819
| preceded_by  = 
| followed_by  = 
}}

'''''The Bride of Lammermoor''''' is a [[historical novel]] by [[Sir Walter Scott]], published in 1819. The novel is set in the [[Lammermuir Hills]] of south-east Scotland, and tells of a tragic love affair between young Lucy Ashton and her family's enemy Edgar Ravenswood.  Scott indicated the plot was based on an actual incident.  ''The Bride of Lammermoor'' and ''[[A Legend of Montrose]]'' were published together as the third of Scott's ''[[Tales of My Landlord]]'' series. As with all the [[Waverley Novels]], ''The Bride of Lammermuir'' was published anonymously. The novel claims that the story was an oral tradition, collected by one "Peter Pattieson", and subsequently published by "[[Jedediah Cleishbotham]]". The 1830 "Waverley edition" includes an introduction by Scott, discussing his actual sources. The later edition also changes the date of the events: the first edition sets the story in the 17th century; the 1830 edition sets it in the reign of [[Anne, Queen of Great Britain|Queen Anne]], after the [[1707 Acts of Union]] which joined Scotland and England.<ref name=Diaz/> The story is the basis for [[Donizetti]]'s 1835 opera ''[[Lucia di Lammermoor]]''.

==Plot summary==

The story recounts the tragic love of Lucy Ashton, and Edgar, Master of Ravenswood. Edgar's father was stripped of the title for supporting the deposed King [[James II of England|James VII]]. Lucy's ambitious father, Sir William Ashton, then bought the Ravenswood estate. Edgar hates Sir William for this usurpation of his family's heritage, but on meeting Lucy, falls in love with her, and renounces his plans for vengeance.

Sir William's haughty and [[Psychological manipulation|manipulative]] wife, Lady Ashton, is the [[villain]]ess of the story. She is determined to end the initial happy engagement of Edgar and Lucy, and force Lucy into a politically advantageous arranged marriage. Lady Ashton intercepts Edgar's letters to Lucy and persuades Lucy that Edgar has forgotten her. Edgar leaves Scotland for France, to continue his political activities. While he is away, Lady Ashton continues her campaign. She gets Captain Westenho, a wandering soldier of fortune, to tell everyone that Edgar is about to get married in France. She even recruits "wise woman" Ailsie Gourlay (a [[witchcraft|witch]] in all but name) to show Lucy omens and tokens of Edgar's unfaithfulness. Lucy still clings to her troth, asking for word from Edgar that he has broken off with her; she writes to him. Lady Ashton suppresses Lucy's letter, and brings the Reverend Bide-the-bent to apply religious persuasion to Lucy. However, Bide-the-bent instead helps Lucy send a new letter, but there is no answer.

Lady Ashton finally bullies Lucy into marrying Francis, Laird of Bucklaw. But on the day before the wedding, Edgar returns. Seeing that Lucy has signed the betrothal papers with Bucklaw, he repudiates Lucy, who can barely speak. The wedding takes place the next day, followed by a celebration at Ravenswood. While the guests are dancing, Lucy stabs Bucklaw in the bridal chamber, severely wounding him. She descends quickly into insanity and dies. Bucklaw recovers, but refuses to say what had happened. Edgar reappears at Lucy's funeral. Lucy's older brother, blaming him for her death, insists that they meet in a duel. Edgar, in despair, reluctantly agrees. But on the way to the meeting, Edgar falls into quicksand and dies.

==Sources==
[[File:James Dalrymple,Viscount of Stair.jpg|thumb|upright|Viscount Stair (1619–1695) whose daughter provided the model for Lucy Ashton]]
The story is fictional, but according to Scott's introduction to the novel it was based on an actual incident in the history of the [[Earl of Stair|Dalrymple]] and Rutherford families.<ref name=Scott>{{cite web |title=Introduction |work=The Bride of Lammermoor |author=Scott, Walter |year=1819 |url=https://archive.org/details/thebrideoflammer00471gut }}</ref> Scott heard this story from his mother, Anne Rutherford, and his great aunt Margaret Swinton.<ref name=Diaz/> The model for Lucy Ashton was Janet Dalrymple, eldest daughter of [[James Dalrymple, 1st Viscount of Stair]], and his wife Margaret Ross of Balneil. As a young woman, Janet secretly pledged her troth to [[Archibald Rutherfurd, 3rd Lord Rutherfurd|Archibald, third Lord Rutherfurd]], relative and heir of the [[Earl of Teviot]], who was thus the model for Edgar of Ravenswood. When another suitor appeared - David Dunbar, heir of Sir David Dunbar of [[Baldoon Castle]] near [[Wigtown]] - Janet's mother, Margaret, discovered the bethrothal but insisted on the match with Dunbar. Rutherfurd's politics were unacceptable to the Dalrymples: Lord Stair was a staunch [[Whiggism|Whig]], whereas Rutherfurd was an ardent supporter of [[Charles II of England|Charles II]]. Nor was his lack of fortune in his favour. Attempting to intercede he wrote to Janet, but received a reply from her mother, stating that Janet had seen her mistake. A meeting was then arranged, during which Margaret quoted the [[Book of Numbers]] (chapter XXX, verses 2–5), which states that a father may overrule a vow made by his daughter in her youth.<ref name=Scott/>

The marriage went ahead on 24 August 1669,<ref name=chambers>{{cite book |url=https://archive.org/stream/domesticannalsof02chamuoft#page/n335/mode/2up |pages=335–337 |title=Domestic annals of Scotland, from the Reformation to the Revolution |volume=II |author=[[Robert Chambers (publisher born 1802)|Chambers, Robert]] |location=Edinburgh |publisher=W. & R. Chambers |year=1859}}</ref> in the church of [[Old Luce]], [[Wigtownshire]], two miles south of Carsecleugh Castle, one of her father's estates.{{citation needed|date=November 2013}} Her younger brother later recollected that Janet's hand was "cold and damp as marble",<ref name=chambers/> and she remained impassive the whole day. While the guests danced the couple retired to the bedchamber. When screaming was heard from the room, the door was forced open and the guests found Dunbar stabbed and bleeding. Janet, whose [[Shift (clothing)|shift]] was bloody, cowered in the corner, saying only "take up thy bonny bridgroom."<ref name=Scott/> Janet died, apparently insane, on 12 September, without divulging what had occurred. She was buried on 30 September.<ref>{{cite web |title=The Scots peerage |volume=VIII |page=147 |author=[[James Balfour Paul|Paul, James Balfour, Sir]] |location=Edinburgh |publisher=D. Douglas |year=1904 |url=https://archive.org/stream/scotspeeragefoun08pauluoft#page/147/mode/1up/search/Dunbar}}</ref>  Dunbar recovered from his wounds, but similarly refused to explain the event. He remarried in 1674, to Lady Eleanor Montgomerie, daughter of the [[Hugh Montgomerie, 7th Earl of Eglinton|Earl of Eglinton]],<ref>{{cite book |title=Burkes Peerage and Baronetage |edition=107th |volume=II |page=1961}}</ref> but died on 28 March 1682 after falling from a horse between Leith and Edinburgh.<ref name=Scott/> Rutherfurd died in 1685, without children.<ref name=chambers/>

It was generally believed that Janet had stabbed her new husband, though other versions of the story suggest that Rutherfurd hid in the bedchamber in order to attack his rival Dunbar, before escaping through the window. The involvement of the devil or other malign spirits has also been suggested.<ref name=chambers/> Scott quotes the Rev. Andrew Symson (1638–1712), former minister of [[Kirkinner]], who wrote a contemporary elegy "On the unexpected death of the virtuous Lady Mrs. Janet Dalrymple, Lady Baldoon, younger", which also records the dates of the events.<ref name=Scott/><ref>{{cite web |url=https://archive.org/stream/historyofgallowa02mack#page/222/mode/2up |title=Appendix X. |page=222 |author=Symson, Andrew |work=The history of Galloway, from the earliest period to the present time |volume=II |location=Kirkcudbright |publisher=J. Nicholson |year=1841}}</ref> More scurrilous verses relating to the story are also quoted by Scott, including those by Lord Stair's political enemy Sir William Hamilton of Whitelaw.<ref name=Scott/>

It is said{{who|date=November 2013}} that Janet was buried at [[Newliston]] near Edinburgh, but Janet's brother [[John Dalrymple, 1st Earl of Stair|John]], later Earl of Stair, married  Elizabeth Dundas of Newliston in 1669, and he may not have been at Newliston when Janet died. Janet may have been buried by her husband at [[Glenluce]].{{citation needed|date=November 2013}}

Scott's biographers have compared elements of ''The Bride of Lammermuir'' with Scott's own romantic involvement with Williamina Belsches in the 1790s. The bitterness apparent in the relationship between Lucy Ashton and Edgar of Ravenswood after their betrothal is broken has been compared to Scott's disappointment when, after courting her for some time, Belsches married instead the much wealthier [[Sir William Forbes, 6th Baronet|William Forbes]].<ref name=Diaz>{{cite web |url=http://www.eswsc.com/BulletinFiles/TheBrideOfLammermoor.June06.doc |title=Fiction and History in the Tales of My Landlord (3rd Series): ''The Bride of Lammermoor''. [1819] |author=Díaz, Enrique Garcia |year=2006 |publisher=Edinburgh Sir Walter Scott Club}}</ref>

==Locations==
[[File:Fast castle - entrance - 19092010.jpg|thumb|The precipitous [[Fast Castle]], identified with the "Wolf's Crag"]]
The spelling ''Lammermoor'' is an [[Anglicisation]] of the [[Scots language|Scots]] ''[[Lammermuir]]''. The Lammermuir Hills are a range of moors which divide [[East Lothian]] to the north from [[Berwickshire]] in the [[Scottish Borders]] to the south. The fictional castle "Wolf's Crag" has been identified with [[Fast Castle]] on the Berwickshire coast. Scott stated that he was "not competent to judge of the resemblance... having never seen Fast Castle except from the sea." He did approve of the comparison, writing that the situation of Fast Castle "seems certainly to resemble that of Wolf's Crag as much as any other".<ref name=Scott/>

==References==
<references />

==External links==
{{Gutenberg|no=471|name=The Bride of Lammermoor}}

{{Walter Scott}}

{{DEFAULTSORT:Bride Of Lammermoor, The}}
[[Category:1819 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Walter Scott]]
[[Category:East Lothian]]
[[Category:Historical novels]]
[[Category:Novels set in Scotland]]