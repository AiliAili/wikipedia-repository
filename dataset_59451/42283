<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name = Avro 707
|image = Avro 707B VX790 in flight c1951.jpg
|caption = Avro 707B ''VX790'' in flight, 1951. [[NACA]] air intake.
}}{{Infobox Aircraft Type
|type = [[Experimental aircraft]]
|manufacturer = [[Avro]]
|designer = 
|first flight = 4 September 1949
|introduced = 
|retired = 1967
|status = 3 aircraft survive in museums
|primary user = Avro
|more users = [[Royal Aircraft Establishment]] <br/>Australian Aeronautical Research Council
|produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
|number built = 5
|unit cost = 
|variants with their own articles = 
}}
|}

The '''Avro 707''' (also known as '''Type 707''') was a British experimental aircraft built to test the [[tailless aircraft|tailless]] thick [[delta wing]] configuration chosen for the Avro 698 jet [[bomber]], later named the [[Avro Vulcan|Vulcan]]. In particular, the low-speed characteristics of such aircraft were not well known at the time. Aerodynamically, it was a one third scale version of the Vulcan.

==Design and development==
[[File:Avro 707A WD280 in flight c1951.jpg|thumb|An Avro 707A in flight, 1951.]]
The 707 was a "proof-of-concept" delta design that was principally the work of [[Stuart Davies (engineer)|Stuart D. Davies]], Avro chief designer. The diminutive experimental aircraft initially incorporated a wing with about 50° sweep, without a horizontal tail on a fin with trailing edge sweep. The trailing edge of the wing carried two pairs of control surfaces: inboard [[elevator (aircraft)|elevator]]s and outboard [[aileron]]s. Retractable [[air brake (aircraft)|airbrake]]s were provided above and below the wings.<ref>Buttler 2007, p. 54.</ref>
The prototypes were ordered by the [[Ministry of Supply]] to [[List of Air Ministry specifications|Specification E.15/48]]. The aircraft were produced quickly using a few components from other aircraft including the first prototype using a [[Gloster Meteor]] canopy.{{#tag:ref|"Off-the-shelf" parts included an [[Avro Athena]] main undercarriage leg and Gloster Meteor nose leg.<ref name="Harlin and Jenks p. 174">Harlin and Jenks 1973, p. 174.</ref>|group=N}}<ref>Buttler 2007, p. 52.</ref>  The 707 programme provided valuable insights into the Vulcan's flight characteristics, most of the information coming from the second and third prototypes which flew before the Vulcan. All 707s were powered by a single [[Rolls-Royce Derwent]] centrifugal turbojet. The air intake on the first prototype and later 707B was located on the upper rear fuselage.<ref>Winchester 2005, p. 123.</ref> Five 707s were built altogether.<ref>Buttler 2007, pp. 54–55.</ref>{{#tag:ref|The last three Avro 707s flew after the Vulcan's first flight on 30 August 1952.|group=N}}

==Operational history==
[[File:Avro 707 Farnborough 1951.jpg|thumb|The Avro 707B at [[Farnborough Airshow|Farnborough]], in 1951.]]
The first, the Avro '''707''', ''VX784'' first flew from [[Boscombe Down]] on 4 September 1949 with S.E. "Red" Esler, at the controls.<ref name="Harlin and Jenks p. 174"/> The prototype crashed less than a month later, on 30 September, near [[Blackbushe Airport|Blackbushe]].{{#tag:ref|Esler was killed in the crash.|group=N}}<ref>Jackson 1986, p. 38.</ref> The next prototype, ''VX790'', renamed the '''707B''', had a longer nose, different cockpit canopy, a wing of different (51°) sweep and a longer nose wheel leg to provide the high angle of incidence required by deltas for landing and take off. The 707B was given the same dorsal engine intake as the 707, although this was later modified to a [[NACA]] design. It first flew on 6 September 1950. Both these aircraft were built to test low speed characteristics.

The third aircraft, designated '''707A''', ''WD280'' was built for higher speed testing.  Experience with the dorsal intake of the earlier 707 and 707B had shown that as speed increased, the cockpit induced turbulence which interrupted the intake airflow, so the intakes were moved to the wing roots.<ref name="Harlin and Jenks p. 176">Harlin and Jenks 1973, p. 176.</ref> When the Vulcan appeared, it looked very much like an enlarged 707A. Later, this 707A was used to test the compound leading edge sweep subsequently used on all Vulcans. Although the first Vulcan prototype was already flying, a second 707A ''WZ736'' was built to speed the development programme, making its maiden fight on 20 February 1953.

The final variant was the two-seat '''707C'''; originally four examples were ordered by the RAF for use in orientation training revolving around flying aircraft with delta wing configurations. The 707C had "side-by-side" seating with dual-controls but the production order was cancelled with only the sole prototype, ''WZ744'' built.<ref name="Harlin and Jenks p. 176"/> The 707C had its maiden flight on 1 July 1953 and was ultimately employed in other research that did not involve Vulcan development.<ref name="Buttler p. 55">Buttler 2007, p. 55.</ref>

[[File:Avro Vulcan VX770 VX777 FAR 13.09.53 edited-2.jpg|thumb|right|Two Avro 707As, a 707B and a 707C with both Vulcan prototypes at the SBAC [[Farnborough Air Show]] in September 1953]]

Even after the Vulcan development phase was over, the four surviving 707s, resplendent in individual bright blue, red, orange and silver (natural metal) colour schemes, continued in use as research aircraft.<ref name="Buttler p. 55"/> After the compound sweep investigation<ref>Jackson 1965, p. 423.</ref> and a period with the [[Royal Aircraft Establishment]] (R.A.E) carrying out handling trials with powered controls,<ref name="Cooper p.108">Cooper 2006, p. 108.</ref> the first 707A went to the Aeronautical Research Laboratories in Australia for low-speed delta wing airflow measurements. The second 707A was also at the R.A.E from June 1953 for aerodynamic and later, automatic control investigations.

The Avro 707B joined the R.A.E. in September 1952<ref name="Cooper p.108"/> and was one of the aircraft used by the [[Empire Test Pilots School]] from January to September 1956,<ref>Cooper 2006, p. 85.</ref> when it was damaged on landing, and broken up at R.A.E. Bedford.<ref name="Harlin and Jenks p. 176"/> The two-seat 707C joined the R.A.E. January 1956; perhaps its most substantial research contribution was to the development of [[Aircraft flight control systems#Fly-by-wire control systems|fly-by-wire control]] systems, one of the first of their kind, and fitted with a side stick controller. This aircraft was flying with the R.A.E. until September 1966 when it achieved its full airframe time.<ref name="ElectricHunter">Wilson, Michael, Technical editor. [http://www.flightglobal.com/pdfarchive/view/1973/1973%20-%201822.html "Avionics: RAE Electric Hunter."] ''Flight International'', 28 June 1973. Retrieved: 3 July 2011.</ref>

The Avro 707s made public appearances at the [[Farnborough Airshow]]s in both September 1952 and 1953. In 1952, the first prototype Vulcan flew with the 707s A and B and in 1953, the four surviving 707s flew alongside the first two Avro 698 Vulcan prototypes.

==Survivors==
[[File:Avro 707A WZ736 MSIM 10.08.85R edited-2.jpg|thumb|right|The second Avro 707A ''WZ736'' displayed next to an [[Avro Shackleton]] at the [[Museum of Science and Industry (Manchester)|Museum of Science and Industry in Manchester]] in 1985]]

No 707s are now airworthy. Both examples of the Avro 707A variant survive. One, ''WZ736'' is preserved at the [[Museum of Science and Industry in Manchester]], the other, ''WD280'' at the [[RAAF Museum]] in [[Point Cook, Victoria]]. ''WZ744'', the single 707C was displayed at the [[RAF Museum]], Cosford near [[Wolverhampton]] and is currently stored out of public view at the museum with its space in the Test Flight hall having been replaced by the [[British Aerospace EAP]].<ref>Jackson 1965, pp. 422–445.</ref>

==Operators==
;{{AUS}}
*[[Royal Australian Air Force]]
*[[Aeronautical Research Laboratories]]
;{{UK}}
*Aeroplane and Armament Experimental Establishment
*[[Royal Aircraft Establishment]]

==Specifications (707C)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=jet
|crew=2 instructor/pupil
|length main=42 ft 4 in
|length alt=12.90 m
|span main=34 ft 2 in
|span alt=10.41 m
|height main=11 ft 7 in
|height alt=3.53 m
|area main=420 ft²
|area alt=39 m²
|empty weight main= lb
|empty weight alt= kg
|loaded weight main=10,000 lb
|loaded weight alt=4,535 kg
|useful load main=
|useful load alt=
|max takeoff weight main=<!-- lb-->
|max takeoff weight alt=<!-- kg-->
|number of jets=1
|engine (jet)=[[Rolls-Royce Derwent]] 8
|type of jet=[[turbojet]]
|thrust main=3,600 lbf
|thrust alt=16 kN
|max speed main=403 kn
|max speed alt=464 mph, 747 km/h
|cruise speed main=<!-- knots-->
|cruise speed alt=<!-- mph,  km/h-->
|range main=<!-- nm-->
|range alt=<!-- mi,  km-->
|ceiling main=<!-- ft-->
|ceiling alt=<!-- m-->
|climb rate main=<!-- ft/min-->
|climb rate alt=<!-- m/s-->
|loading main=22.6 lb/ft²
|loading alt=110 kg/m²
|thrust/weight=0.38
}}

==See also==
{{aircontent
|related=
* [[Avro Vulcan]]
|similar aircraft=
* [[Boulton Paul P.111]]
* [[Bristol 188]] 
* [[Dassault Mirage|Dassault Mirage I]]
* [[Dassault MD.550 Mystere-Delta]]
* [[English Electric Lightning|English Electric P1A]] 
* [[Fairey Delta 1]]
* [[FMA I.Ae. 37]]
* [[Handley Page HP.88]]
}}

==References==
;Notes
{{reflist|group=N}}
;Citations
{{reflist|2}}
;Bibliography
{{refbegin}}
* Buttler, Tony. "Avro Type 698 Vulcan (Database)." ''Aeroplane,'' Vol. 35, No. 4, Issue No. 408, April 2007.
* Cooper, Peter J. ''Farnborough: 100 years of British Aviation''. Hinkley, UK: Midland Books, 2006. ISBN 1-85780-239-X.
* Harlin, E.A. and G.A. Jenks. ''Avro: An Aircraft Album.'' Shepperton, Middlesex, UK: Ian Allen, 1973. ISBN 978-0-7110-0342-2. 
* Jackson, A.J. ''Avro Aircraft since 1908''.  London: Putnam & Co., 1965.
* Jackson, Robert. ''Combat Aircraft Prototypes since 1945.'' New York: Arco/Prentice Hall Press, 1986. ISBN 0-671-61953-5.
* Winchester, Jim. "Avro 707 (1949)". ''X-Planes and Prototypes''. London: Amber Books Ltd., 2005. ISBN 1-904687-40-7.
{{refend}}

==External links==
{{commons|Avro 707}}
* [http://www.aeroflight.co.uk/types/uk/avro/707/707.htm Aeroflight]

{{Avro aircraft}}

[[Category:Avro aircraft|707]]
[[Category:British experimental aircraft 1940–1949]]
[[Category:Tailless delta-wing aircraft]]
[[Category:Single-engined jet aircraft]]