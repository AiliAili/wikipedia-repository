{{good article}}
{{Infobox Hurricane
| Name=1941 Cabo San Lucas hurricane
| Type=Hurricane
| Year=1941
| Basin=EPac
| Formed=September 8, 1941
| Dissipated=September 13, 1941
| Image location=1941 Cabo San Lucas hurricane analysis 10 Sep.png
| Image name=[[Surface weather analysis]] of the hurricane on September&nbsp;10
| Pressure=1001
| 1-min winds=75
| Damages=
| Inflated=0
| Fatalities=15
| Areas=Baja California Peninsula, California
| Hurricane season=[[1940–48 Pacific hurricane seasons]]
}}
The '''1941 Cabo San Lucas hurricane''' is considered one of the worst [[tropical cyclone]]s on record to affect [[Cabo San Lucas]]. The hurricane was first reported on September&nbsp;8 off the coast of Mexico. It slowly moved northwestward while intensifying. After peaking in intensity, it entered the [[Gulf of California]], and weakened rapidly. It dissipated on September&nbsp;13.

This system brought winds and heavy rain to the southern tip of the [[Baja California Peninsula]]. The hurricane destroyed poorer sections of [[La Paz, Baja California Sur|La Paz]] and its nearby villages. Two villages were completely destroyed. Furthermore, Cabo San Lucas was devastated. The torrential rains damaged roads and left thousands homeless. In addition, the hurricane destroyed the tuna canning industry in San José del Cabo. Throughout the devastated peninsula, 15 people were killed, and many others were injured. Initially following the storm, Cabo San Lucas was abandoned, but after [[World War II]], most of the destroyed buildings were rebuilt. The remnants of the storm later brought rains to California and Arizona, peaking at {{convert|3.54|in|mm|abbr=on|0}} at [[Mormon Lake]].

==Meteorological history==
A tropical storm was first reported on September&nbsp;8, 1941. The storm quickly intensified, becoming a mid-level tropical storm the next day while attaining its lowest reported pressure of {{convert|1001.4|mbar|inHg|abbr=on}}. It slowly moved northwestward, and entered the Gulf of California.<ref name=Hurd4109 /> Subsequently, the hurricane made [[landfall (meteorology)|landfall]] along the southern portion of [[Baja California Sur]],<ref name=Williams>{{cite news|newspaper=[[USA Today]]|first=Jack|last=Williams|title=Background: California's tropical storms|date=May 17, 2005|url=http://www.usatoday.com/weather/whhcalif.htm|accessdate=May 17, 2013}}</ref> with winds of {{convert|85|mph|km/h|disp=5|abbr=on}}. This made the hurricane a Category&nbsp;1 on the modern-day [[Saffir-Simpson hurricane wind scale]]. Even though the [[Monthly Weather Review]] reported that the hurricane was last observed on September&nbsp;12,<ref name=Hurd4109>{{cite journal|format=PDF|first=Willis|last=Hurd|title=Weather on the North Pacific Ocean|work=[[Monthly Weather Review]]|accessdate=May 17, 2013|date=September 1941|url=http://docs.lib.noaa.gov/rescue/mwr/069-081/069/mwr-069-09-0274b.pdf}}</ref> the storm is presumed to have weakened thereafter as it moved up the coast and by September&nbsp;13, only a swirl of clouds remained.<ref name="Tech Memo" />

==Impact and aftermath==
[[File:1941 Cabo San Lucas hurricane analysis 11 Sep.png|left|thumb|Weather map of the hurricane making landfall on September&nbsp;11]]
Throughout the peninsula, 15&nbsp;people were killed, and many were injured. According to press reports from [[Mexico City]], the hurricane was considered the worst system to affect the state since the dawn of the 20th century.<ref name=Hurd4109/>

The port town of Cabo San Lucas was washed away and mostly destroyed due to flooding.<ref name=DPL>{{cite journal|title=Desert Plant Life|year=1944|volume=16-19|page=151}}</ref><ref name=ELC /> The storm brought "great loss of life" to the city.<ref name=AZ>{{cite book|last=Zwinger|first=Ann|title=A desert country near the sea: a natural history of the Cape|year=1983|page=327}}</ref> Furthermore, the hurricane destroyed a tuna packaging plant.<ref name=ACSL /> In all, this hurricane is regarded as one of the worst [[tropical cyclone]]s to affect the city.<ref name=ELC>{{cite book|author1=Browne, John |author2=Murray, Spence |title=Explorations of Lower California|year=1966|pages=2, 14}}</ref> Initially following the system, activities among the surrounding areas of the village ceased; throughout World War II, Cabo San Lucas was essentially abandoned, but most of the buildings destroyed were rebuilt following the war.<ref name=ACSL>{{cite web|publisher=Explore around Mexico|title=About Cabo San Lucas|url=http://www.explorandomexico.com/city/6/Cabo-San-Lucas/about/|accessdate=April 5, 2013}}</ref><ref name=CONANP>{{cite web|title=Cabo San Lucas|url=http://difusion.conanp.gob.mx/CSL/historia.php|publisher=CONANP|accessdate=May 17, 2013}}</ref><ref>{{cite book|author1=Navarre, Monty |author2=Merrick, Harry |title=The Baja Traveler|year=1974|publisher=Airguide Publications, inc|pages=4–67}}</ref> Because of the damage caused by the system, the Cabo San Lucas Bay was forced to relocate {{convert|1|mi|km|abbr=on}} inland from its initial location.<ref name=ELC />

Strong winds and heavy rain lashed the southern tip of the Baja California Peninsula for 48 hours, lasting until late September 12. The wind destroyed poorer sections of [[La Paz, Baja California Sur|La Paz]] and nearby villages. Two villages, [[Santiago, Baja California Sur|Santiago]] and [[Triunfo, Baja California Sur|Triunfo]], were completely destroyed. The torrential rains damaged many highways across the peninsula and left thousands homeless.<ref name=Hurd4109/> The tuna canning industry declined rapidly in [[San José del Cabo, Baja California Sur|San José del Cabo]] following the hurricane because the storm had damaged the equipment needed for the industry.<ref name=MH>{{cite book|last=Gothitoi|first=Niki|title=Moon Baja: Including Cabo San Lucas|year=2011|publisher=Moon Handbooks|url=https://books.google.com/books?id=WfLUXir6ed0C&pg=PT483&dq=cabo+san+lucas+1941+hurricane&hl=en&sa=X&ei=8UhfUd3xB6WJ2AWhnoGgDg&ved=0CDsQ6AEwAA}}</ref> According to personal accounts, a famous church was destroyed in [[Todos Santos, Baja California Sur|Todos Santos]].<ref>{{cite web|last=Hauser|first=Hilary|title=Todos Santos- an Eden in Transition|url=http://www.bajalife.com/v9p70.htm|publisher=Baja For Life|accessdate=April 5, 2013}}</ref> Moisture from the Cabo San Lucas hurricane of 1941 later passed into the [[Southwestern United States]], where it caused up to {{convert|1|in|mm|abbr=on}} of rain in the mountains and deserts of California.<ref name="CSUN">{{cite book|author=Court, Arnold|publisher=[[California State University, Northridge]]|year=1980|title=Tropical Cyclone Effects on California|pages=2, 4, 6, 8, 34|accessdate=April 5, 2013|url=http://babel.hathitrust.org/cgi/pt?id=uc1.31822009671892;seq=10;size=125;view=image}}</ref> Further east, the storm brought heavy rains to portions of Arizona, peaking at {{convert|3.54|in|mm|abbr=on|0}} at [[Mormon Lake]]. Four other weather stations recorded more than {{convert|2|in|mm|abbr=on|0}}.<ref name="Tech Memo">{{cite journal|title=The Effects of Tropical Cyclones on the Southwestern United States|journal=NOAA Technical Memorandum|date=August 1986|url=http://docs.lib.noaa.gov/noaa_documents/NWS/NWS_WR/TM_NWS_WR_197.pdf|accessdate=June 4, 2013|publisher=National Weather Service Western Region}}</ref>

==See also==
*[[Hurricane Ignacio (2003)]]
*[[Hurricane Marty (2003)]]
*[[List of Baja California Peninsula hurricanes]]

==References==
{{Reflist|2}}

[[Category:Pacific hurricanes in Mexico]]
[[Category:Category 1 Pacific hurricanes]]