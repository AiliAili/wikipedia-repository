{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Can I Have It Like That
| Cover          = CanIHaveItLikeThat.png
| Artist         = [[Pharrell Williams|Pharrell]] featuring [[Gwen Stefani]]
| Album          = [[In My Mind (Pharrell album)|In My Mind]]
| Released       = {{Start date|2005|10|31|mf=y}}
| Format         = {{flatlist|
* [[CD single|CD]]
* [[7" vinyl]]
* [[12" vinyl]]
* [[Music download|digital download]]
}}
| Recorded       = 2005
| Genre          = [[East Coast hip hop]]
| Length         = 3:55
| Label          = [[Star Trak]], [[Interscope]]
| Writer         = Pharrell Williams
| Producer       = Pharrell Williams
| Certification  = 
| Chronology     = [[Pharrell Williams|Pharrell]] singles
| Last single    = "[[Let's Get Blown]]"<br />(2004)
| This single    = "'''Can I Have It Like That'''"<br />(2005)
| Next single    = "I Ain't Heard of That"<br />(2005)
| Misc           = {{Extra chronology
| Artist         = [[Gwen Stefani]] singles
| Type           = singles
| Last single    = "[[Cool (Gwen Stefani song)|Cool]]"<br />(2005)
| This single    = "'''Can I Have It Like That'''"<br />(2005)
| Next single    = "[[Luxurious]]"<br />(2005)
}}
}}

"'''Can I Have It Like That'''" is a  song performed by rapper [[Pharrell Williams]] featuring [[Gwen Stefani]].  It was written and produced by Williams for his 2006 debut album ''[[In My Mind (Pharrell album)|In My Mind]]'', and the song is the [[opening track]].  The song's [[hook (music)|hook]] comes from a one-line contribution from Stefani, used as part of a [[call and response (music)|call and response]] in the chorus. "Can I Have It Like That" was released as the album's [[lead single]] in [[2005 in music|October 2005]].  

It was commercially unsuccessful in the United States, but fared somewhat better worldwide.  The song received generally negative reviews from music critics. A [[rock music|rock]] [[remix]] of the song featured [[Travis Barker]] playing the [[drum]]s and Pharrell playing the [[guitar]]. It was later put as one of the songs in [[NBA 2K15]].

== Background and composition ==
[[P. Diddy]] was originally designated to use the track that Williams created.  Several other artists were also interested in using it, but Williams changed his mind and decided to use the song himself.<ref>Bainbridge, Luke.  [http://observer.guardian.co.uk/magazine/story/0,11913,1607765,00.html "Just Williams"].  ''[[The Observer]]''.  November 6, 2005. Retrieved October 17, 2007.</ref>  In mid-2005, he worked with Stefani in [[Miami]], [[Florida]] on her second solo album ''[[The Sweet Escape]]'' (2006).  While she was in the booth recording "Breakin' Up", he told her that he had a song he thought would be his first single.  Stefani asked to hear it, and he said that he wanted to record her for the track after the two finished working on "Breakin' Up".  Stefani, however, insisted that they work on "Can I Have It Like That" then, and Williams recorded her performing the song's [[hook (music)|hook]].<ref>Reid, Shaheem and Bland, Bridget.  [http://www.mtv.com/news/articles/1513788/20051115/williams__pharrell.jhtml "Pharrell Had To Be Sneaky To Record His Solo Album"].  [[MTV News]].  November 15, 2005. Retrieved October 17, 2007.</ref>  She contributed one line, which she performed twice, to the song.<ref name="mtv1">Vineyard, Jennifer.  [http://www.mtv.com/news/articles/1511456/20051013/williams__pharrell.jhtml "Pharrell 'Can't Complain' About Lady Leopards; Gwen Moons Over Him On Video Set"].  [[MTV News]].  November 11, 2005. Retrieved October 17, 2007.</ref>

{{listen
|pos=left 
 | filename     = CanIHaveItLikeThat.ogg
 | title        = "Can I Have It Like That"
 | description  = Most of the song uses an irregular [[Music loop|drum loop]] and prominent bass.
 | format       = [[Ogg]]
}}

Williams' rap is in a throaty [[baritone]],<ref name="IGN">Abbott, Spence.  [http://music.ign.com/articles/721/721849p1.html "Pharrell - ''In My Mind''"].  [[IGN]].  July 28, 2006. Retrieved October 19, 2007.</ref> described as a "honey-coated version" of the styles of [[Snoop Dogg]] and [[Dr. Dre]].<ref name="OMH">Simpson, Claire.  [http://www.musicomh.com/singles/pharrell_1005.htm "Pharrell feat. Gwen Stefani - Can I Have It Like That : single review"].  [[musicOMH]]. Retrieved October 19, 2007.</ref>  He raps about his current lifestyle as well as his life and achievements.  The rap is set over a [[rhythm section]] consisting of an irregular [[Beat (music)|beat]] and a grinding bass.<ref name="IGN"/><ref name="ew">[[Tyrangiel, Josh]].  [http://www.ew.com/ew/article/0,,1217325,00.html "In My Mind | Music Review"].  ''[[Entertainment Weekly]]''.  July 21, 2007. Retrieved October 18, 2007.</ref>  The track reflects Williams' [[minimalist music|minimalist]] style, and it is inflected with [[jazz]] music,<ref>Hoard, Christian.  [http://www.rollingstone.com/reviews/album/10850266/review/10963112/in_my_mind "In My Mind : Pharrell : Review"].  ''[[Rolling Stone]]''.  July 24, 2006.  Retrieved October 23, 2007. {{webarchive |url=https://web.archive.org/web/20071013144528/http://www.rollingstone.com/reviews/album/10850266/review/10963112/in_my_mind |date=October 13, 2007 }}</ref> including a trumpet part during the [[bridge (music)|bridge]].<ref name="stylus">McGarvey, Evan.  [http://www.stylusmagazine.com/reviews/pharrell/in-my-mind.htm "Pharrell - In My Mind - Review"].  ''[[Stylus Magazine]]''.  July 25, 2006. Retrieved October 23, 2007.</ref>

== Critical reception ==
"Can I Have It Like That" received generally negative reviews from music critics.  Josh Tyrangiel of ''[[Entertainment Weekly]]'' called it an "odd opener" to ''In My Mind'' and remarked that Williams "never commits to putting his hips into it", leaving the track without any climax.<ref name="ew"/>  ''[[The Observer]]''<nowiki>'s</nowiki> Luke Bainbridge, however, referred to the song as "a grinding instant smash".<ref>Bainbridge, Luke.  [http://observer.guardian.co.uk/omm/reviews/story/0,13875,1590569,00.html "Pharrell Williams, In My Mind"]. ''[[The Observer]]''. October 16, 2005. Retrieved October 23, 2007.</ref> For [[IGN]], Spence Abbott described Williams' rapping as "a little sticky, but…ruggedly appealing in a manner of speaking", and commented that "it's the shuffling and infectious beat…that will have you swerving and slinking like a cobra with epilepsy."<ref name="IGN"/>  In his review for ''[[Slant Magazine]]'', Preston Jones characterized the song as "curiously inert", adding that it sounded better as a [[sampling (music)|sample]] on [[Girl Talk (musician)|Girl Talk]]'s 2006 [[mashup (music)|mashup]] album ''[[Night Ripper]]''.<ref>Jones, Preston.  [http://www.slantmagazine.com/music/music_review.asp?ID=913 "Music Review: Pharrell: ''In My Mind''"]. ''[[Slant Magazine]]''.  2006. Retrieved October 23, 2007.</ref>

Stefani's presence as a featured artist on the song received negative reviews from music critics. [[About.com]]'s Ivan Rott commented that the song's [[funk]]-influenced sound "definitely [[wikt:knock|knocks]]", but was disappointed in Stefani's contribution, finding its repetition tedious.<ref>Rott, Ivan. [http://rap.about.com/od/albumreviews/fr/PharrelInMyMind.htm "Pharrell - In My Mind"]. [[About.com]]. Retrieved October 18, 2007.</ref> Evan McGarvey of ''[[Stylus Magazine]]'' also found it repetitive, and described "Can I Have It Like That" as "a brief distillation of everything calamitous about [Williams'] and [[Chad Hugo]]'s production style".<ref name="stylus"/> ''[[The Guardian]]'' stated that Stefani's part was the only catchy [[Hook (music)|hook]] on ''In My Mind'', but added that it "seems to be built around a one-line cast-off found on the ''[[Love. Angel. Music. Baby.]]'' factory floor."<ref>[https://www.theguardian.com/music/2006/jul/21/popandrock.urban "Pharrell, In My Mind"]. ''[[The Guardian]]''. July 21, 2006. Retrieved October 18, 2007.</ref> Finding "Can I Have It Like That" a "slow burner" that offered a "promising hint at what else is to come" on ''In My Mind'', Claire Simpson stated in her [[musicOMH]] review that Stefani's contribution was "purely cosmetic and clearly a transparent means of getting her name on the single to not only boost her own credentials but to help draw Pharrell to the attention of her increasingly mainstream following."<ref name="OMH"/>

== Chart performance ==
"Can I Have It Like That" was commercially unsuccessful in the United States.  It debuted at number 97 on the [[Billboard Hot 100|''Billboard'' Hot 100]] and peaked at number 49 three weeks later.  It exited the chart after eight weeks.<ref name="acharts">[http://acharts.us/song/966 "Pharrell and Gwen Stefani - Can I Have It Like That - Music Charts"].  αCharts.us. Retrieved October 18, 2007.</ref>  It was somewhat more successful in [[urban contemporary]] and [[rhythmic contemporary]] markets, reaching number 20 on the [[Hot Rap Tracks]], number 31 on the [[Rhythmic Top 40]], and number 32 on the [[Hot R&B/Hip-Hop Songs]].<ref>
* [http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=370&cfgn=Singles&cfn=Hot+Rap+Tracks&ci=3063124&cdi=8504502&cid=12%2F17%2F2005 "Hot Rap Tracks - Can I Have It Like That"].  ''[[Billboard (magazine)|Billboard]]''.  Retrieved October 18, 2007.
* [http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=378&cfgn=Singles&cfn=Rhythmic+Top+40&ci=3063130&cdi=8504696&cid=12%2F17%2F2005 "Rhythmic Top 40 - Can I Have It Like That"].  ''Billboard''.  Retrieved October 18, 2007.
* [http://www.billboard.com/charts/2006-01-14/r-b-hip-hop-songs "Hot R&B/Hip-Hop Songs - Can I Have It Like That"].  ''Billboard''.  Retrieved October 18, 2007.</ref>  It had some [[crossover (music)|crossover]] success in mainstream music and reached number 51 on the ''Billboard'' [[Pop 100]].<ref>[http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=396&cfgn=Singles&cfn=Pop+100&ci=3063406&cdi=8514838&cid=12%2F31%2F2005 "Pop 100 - Can I Have It Like That"].  ''[[Billboard (magazine)|Billboard]]''.  Retrieved October 18, 2007. {{webarchive |url=https://web.archive.org/web/20071114174756/http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=396&cfgn=Singles&cfn=Pop+100&ci=3063406&cdi=8514838&cid=12%2F31%2F2005 |date=November 14, 2007 }}</ref>

The single was more successful in Europe, where it reached number 11 on the ''Billboard'' European Hot 100 Singles chart.<ref>[http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=349&cfgn=Singles&cfn=European+Hot+100+Singles&ci=3062704&cdi=8488662&cid=11%2F19%2F2005 "European Hot 100 Singles - Can I Have It Like That"].  ''[[Billboard (magazine)|Billboard]]''.  Retrieved October 18, 2007. {{webarchive |url=https://web.archive.org/web/20071114174735/http://www.billboard.com/bbcom/esearch/chart_display.jsp?cfi=349&cfgn=Singles&cfn=European+Hot+100+Singles&ci=3062704&cdi=8488662&cid=11%2F19%2F2005 |date=November 14, 2007 }}</ref> In the United Kingdom, "Can I Have It Like That" debuted and peaked at number three on the [[UK Singles Chart]], behind [[Arctic Monkeys]]' "[[I Bet You Look Good on the Dancefloor]]" and [[Westlife]]'s cover of [[You Raise Me Up]]. It left the chart after 11 weeks.<ref>[http://acharts.us/uk_singles_top_75/2005/45 "UK Singles Top 75 - Music Charts"].  αCharts.us. Retrieved October 19, 2007.</ref> The single remained Williams' highest-charting release in Britain until 2013 when he topped the charts three times with [[Robin Thicke]]'s "[[Blurred Lines]]", [[Daft Punk]]'s "[[Get Lucky (Daft Punk song)|Get Lucky]]", and as the solo artist in "[[Happy (Pharrell Williams song)|Happy]]". The song was less successful throughout the rest of Europe and reached the top 20 in [[Finland]], Ireland, and [[Norway]]; the top 40 in [[Belgium]], Germany, the [[Netherlands]], and [[Switzerland]]; and the top 80 in [[Austria]] and France.<ref name="acharts"/>

"Can I Have It Like That" was somewhat successful elsewhere.  It debuted at number 21 on the [[ARIA Singles Chart]] and after gradually descending the chart, reached a second peak at number 31 seven weeks later.  It remained on the chart for a total of 12 weeks.  After debuting at number 20, the song peaked at number 18 on the [[New Zealand Singles Chart]].

== Music video ==
[[Image:CanIHaveItLikeThatVideo.png|right|thumb|Stefani and Williams performing in front of a sort of equalizer.]]

The song's music video was directed by [[Paul Hunter (director)|Paul Hunter]].  It depicts Williams being watched by a secret organization.  Aware that he is being watched, he talks to a woman, revealed to be Stefani, on the phone.  He makes appearances at an exclusive boat party and at another party in his penthouse.<ref name="mtv2">Vineyard, Jennifer.  [http://www.mtv.com/news/articles/1510483/20050927/williams__pharrell.jhtml "Pharrell's Got Naked Women, Fireballs And, Oh Yeah, Gwen Stefani For Clip"].  [[MTV News]].  September 27, 2005. Retrieved October 18, 2007.</ref>  There are intercut sequences of people including Williams and Stefani dancing in front of a large equalizer, Williams standing in front of explosions, and people skateboarding. And the video also shows Pharrell's [[Enzo Ferrari (automobile)|Enzo Ferrari]].<ref name="mtv1"/>

The music video was filmed in late September 2005.  The cinematography is designed so that the music video becomes the secret organization's surveillance of Williams.  It uses shifting [[camera angle]]s to create tension.<ref name="mtv2"/>  The scenes in front of the equalizer were filmed in front of a [[chroma key|greenscreen]], and the explosion sequences were shot live on the streets of [[Los Angeles]], [[California]].  In the penthouse scene, he is accompanied by two naked women with leopard-pattern [[body paint]] and a baby cougar.  Hunter came up with the idea of showing body-painted women, and Williams suggested sitting with the cougar because he "wanted to exude power."<ref name="mtv1"/>

The video had little success on video chart programs.  To date, it is Williams' only video as a main artist to debut on [[MTV]]'s ''[[Total Request Live]]''.<ref>[http://www.atrl.net/trlarchive/?s=debuts "The TRL Archive - Debuts"].  ATRL. Retrieved October 19, 2007.</ref>  It was on the program's countdown for three days, reaching number nine.<ref>[http://www.atrl.net/trlarchive/?s=recap&y=2005&m=10 "The TRL Archive - Recap - October 2005"].  ATRL. Retrieved October 23, 2007.</ref>  The video was unable to make [[MuchMusic]]'s ''[[Countdown (MuchMusic TV series)|Countdown]]''.<ref>[http://www.muchmusic.com/tv/countdown/ "MuchMusic Countdown"].  [[MuchMusic]]. Retrieved October 19, 2007.</ref>

== Track listings ==
{{col-begin}}
{{col-2}}
;12" vinyl single
# "Can I Have It Like That" (clean) – 3:57
# "Can I Have It Like That" (dirty) – 3:57
# "Can I Have It Like That" (instrumental) – 3:57
# "Can I Have It Like That" (a cappella) – 3:49
{{col-2}}
;American 7" vinyl single
# "Can I Have It Like That" (clean) – 3:57
# "Can I Have It Like That" (dirty) – 3:57

;British CD single
# "Can I Have It Like That" – 3:59
# "Can I Have It Like That" (the Travis Barker remix) – 4:10
{{col-end}}

== Charts ==
{{col-begin}}
{{col-2}}
{| class="wikitable sortable"
!align="left"|Chart (2005–2006)
!align="left"|Peak<br />position
|-
{{singlechart|Australia|21|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Austria|47|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Flanders|32|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Wallonia|35|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
|align="left"|Canada ([[Jam!]])<ref>{{cite web|url=http://jam.canoe.ca/Music/Charts/SINGLES.html |title=Singles: Top 50 (For The Week Ending 8 December, 2005) |publisher=[[Sun Media]] |work=[[Jam!]] |date=December 1, 2005 |accessdate=August 10, 2014 |archiveurl=https://web.archive.org/web/20051212061242/http://jam.canoe.ca/Music/Charts/SINGLES.html |archivedate=December 12, 2005 |deadurl=yes |df= }}</ref>
| style="text-align:center;"|29
|-
{{singlechart|Denmark|8|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Dutch100|15|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Finland|10|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|France|78|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
|align="left"|Germany ([[Media Control Charts|Media Control]]) <ref>{{cite web|author=musicline.de / PhonoNet GmbH |url=http://musicline.de/de/chartverfolgung_summary/artist/Pharrell/single |title=Chartverfolgung - Pharrell |publisher=Musicline.de |date= |accessdate=2010-09-21}}</ref>
| style="text-align:center;"|37
|-
{{singlechart|Italy|16|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
|align="left"|Ireland ([[Irish Singles Chart|IRMA]])
| style="text-align:center;"|12
|-
{{singlechart|New Zealand|18|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Norway|15|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Sweden|60|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
{{singlechart|Swiss|28|artist=Pharrell feat. Gwen Stefani|song=Can I Have It Like That}}
|-
|align="left"|[[UK Singles Chart]]
| style="text-align:center;"|3
|-
|align="left"|US [[Billboard Hot 100|''Billboard'' Hot 100]]
| style="text-align:center;"|49
|-
|align="left"|US ''Billboard'' Hot R&B/Hip-Hop Songs
| style="text-align:center;"|32
|-
|align="left"|US ''Billboard'' Hot Rap Tracks
| style="text-align:center;"|20
|}
{{col-end}}

== References ==
{{reflist|30em}}
{{refbegin}}
# "[http://swedishcharts.com/showitem.asp?key=158434&cat=s Pharrell Feat. Gwen Stefani- Can I Have It Like That (Song).]" Swedishcharts.com. Retrieved on 2006-12-21.
# "[http://top40-charts.com/song.php?sid=15435&sort=chartid Pharrell & Gwen Stefani Can I Have It Like That?]" Top40-Charts.com. Retrieved on 2006-12-21.
{{refend}}

==External links==
*{{YouTube|IDC5yOSuCv0|Music video}}

{{Pharrell Williams singles}}
{{Gwen Stefani songs}}
{{good article}}

[[Category:2005 singles]]
[[Category:Pharrell Williams songs]]
[[Category:Gwen Stefani songs]]
[[Category:Music videos directed by Paul Hunter (director)]]
[[Category:Song recordings produced by Pharrell Williams]]
[[Category:Songs written by Pharrell Williams]]
[[Category:UK R&B Singles Chart number-one singles]]