{{For|the song recorded by M.I.A.|AIM (album)}}
{{Infobox single<!-- See Wikipedia:WikiProject_Songs -->
| Name           = All My People
| Cover          = Alexandra_Stan_All_My_People.JPG
| Alt = Stan shown posing provocant on a bed while surrounded by surveillance cameras.
| Artist         = [[Alexandra Stan]] vs. Manilla Maniacs
| Album          = [[Cliché (Hush Hush) (album)|Cliché (Hush Hush)]]
| Released       = 1 May 2013
| Format         = [[Music download|Digital download]]
| Recorded       = Maan Studios<br>([[Constanța]], Romania)
| Genre          = [[Electro dance]]
| Length         = 3:19
| Label          = {{Flat list|
*[[Blanco y Negro Music|Blanco y Negro]]
*MediaPro
*Vae Victis
*[[Ultra Records|Ultra]]
}}
| Writer         = {{Flat list|
*Marcel Prodan
*Andrei Nemirschi
}}
| Producer       = {{Flat list|
*Prodan
*Nemirschi
}}
| Chronology     = [[Alexandra Stan]] singles
| Last single    = "[[Cliché (Hush Hush) (song)|Cliché (Hush Hush)]]" <br/>(2012)
| This single    = "'''All My People'''" <br/>(2013)
| Next single    = "[[Baby, It's OK!]]" <br/>(2013)
| Misc           = 
{{External music video|{{YouTube|2oIlS-ARtA8|"All My People"}}}}
}}

"'''All My People'''" is a song recorded by [[Romanians|Romanian]] recording artist [[Alexandra Stan]] for her Japan-only reissue album, ''[[Cliché (Hush Hush) (album)|Cliché (Hush Hush)]]'' (2013). Written and produced by Marcel Prodan and Andrei Nemirschi, the track features vocal collaboration from Prodan's fictional character, Manilla Maniacs. His vocals were particularly praised by music critics as being "low-pitched" and "distorted". Musically, "All My People" is a club-friendly [[electro dance]] song and draws influences from Stan's single "[[Mr. Saxobeat]]" (2010). 

Upon release, the track garnered mostly positive reviews from [[Music journalism|music critics]], who called it "catchy" and found the recording to be unconventional when compared to her previous material. Stand uploaded a music video for the song on [[YouTube]] on 3 May 2013—filmed at a heating power plant in [[Sofia, Bulgaria]]—the video was directed by Ilarionov Borisov. Critics pointed out Stan's erotic appearance; the singer is seen lying on a bed sporting lingerie and surrounded by surveillance cameras. Her choreography drew comparisons to work done by American entertainer [[Michael Jackson]], while the singer wore [[Madonna (entertainer)|Madonna]]-esque outfits. "All My People" peaked within the top fifty on music charts in Romania and Japan, while the single peaked at number fifty-five in Italy.

==Background==
{{Listen|pos=left|filename=All_My_People_(sample).ogg|title="All My People"|description=A 16-second sample of the song, portraying Maniacs' low-pitched and distorted voice in the refrain, which was praised by critics.<ref name="direct lyrics"/><ref name="info"/>|format =[[Ogg]]}}The song was written and produced by Marcel Prodan and Andrei Nemirschi, her longtime collaborators, and recorded at their recording studio, Maan Studios, located in [[Constanța]], Romania.<ref name="credits">{{cite AV media notes |title=Cliché (Hush Hush) |others=[[Alexandra Stan]] |year=2013 |type=Liner notes/ CD booklet |publisher=Maan Records (Barcode: 4 988002 658367) |location=[[Constanța]], Romania}}</ref> Stan made the release of new material public through her [[Facebook]] account; she later released an audio version of a new track, "All My People", on [[Soundcloud]].<ref name="mtv">{{cite web |title=Alexandra Stan mai sexy ca niciodata!  |trans_title=Alexandra Stan, hotter than never! |language=Romanian|publisher=[[MTV]] Romania|url=http://www.mtv.ro/muzica/alexandra-stan-mai-sexy-ca-niciodata-vezi-imagini-incendiare-din-cel-mai-nou-clip-al-cantaretei-all-my-people-video.html}}</ref><ref name="direct lyrics"/> The track portrays a featuring with Prodan's fictional character, Manilla Mancias, who provides low-pitched and distorted vocals for the refrain.<ref name="credits"/><ref name="direct lyrics"/> Musically, the single is of the [[electro dance]] genre, featuring an alert rhythm and minimal lyrics.<ref name="info"/> Upon the release of the single, Romanian website Urban.ro pointed out the evolution of Stan's artistry, with them confessing that the instrumentation of "All My People" was club-friendly, unlike her 2012 single, "[[Lemonade (Alexandra Stan song)|Lemonade]]", which featured "sweet beats".<ref name="urban">{{cite web |title=Alexandra Stan - All My People|publisher=Urban.ro|url=http://www.urban.ro/muzica/videoclipuri/alexandra-stan---all-my-people-videoclip-nou| accessdate=26 May 2016 |date=3 May 2013 |author=Scris de Edi|language=Romanian}}</ref> When interviewed, Stan commented that the song was influenced by her 2010 release, "[[Mr. Saxobeat]]".<ref name="devorator">{{cite web |title=Alexandra Stan despre "All My People"|publisher=Devorator Monden|url=http://devoratormonden.ro/alexandra-stan-despre-all-my-people-piesa-este-dedicata-tuturor-fanilor-mei-pentru-ca-sustinerea-din-partea-lor-a-fost-dintotdeauna-extraordinara| accessdate=26 May 2016 |date=16 May 2013 |author=Teddy stie tot|language=Romanian|trans_title=Alexandra Stan about "All My People"}}</ref>

==Critical reception==
Kevin Apaza, writing for [[Direct Lyrics]], called the single very "catchy", saying that it was different from the material she had released before; he said that he "just [doesn't] see [the track] as a European monster hit" and confessed that its lyrics and the video concept didn't "make much sense".<ref name="direct lyrics">{{cite news|url=http://www.directlyrics.com/alexandra-stan-all-my-people-news.html|title=Alexandra Stan - "All My People"|publisher=[[Direct Lyrics]]|accessdate=25 May 2016|date=3 May 2013|author=Apaza, Kevin}}</ref> To close his review, Apaza praised Maniacs's vocal contribution to the song, calling his voice "cool".<ref name="direct lyrics"/> Frederica de Martino, a writer for the Italian music website Canzoni Web, noted the commercial success of the recording, commenting that "it will be for sure as successful as her previous hits".<ref name="canzoni web">{{cite news|url=http://www.canzoniweb.com/video-alexandra-stan-all-my-people-nuovo-singolo/|title=Alexandra Stan: All My People, video ufficiale del nuovo singolo|publisher=Canzoni Web|accessdate=25 May 2016|date=3 May 2013|author=de Martino, Frederica|language=Italian|trans_title=Alexandra Stan: All My People, official video of the new song}}</ref> [[MTV]] Romania expected the track to become a "hit".<ref name="mtv"/>

==Promotion and music video==
{{Double image|right|HungUpSticky3.jpg|117|Michael_Jackson_in_1988.jpg|90|One of the outfits sported by Stan during the video was compared to [[Madonna (entertainer)|Madonna]] (''left''), while the choreography drew comparations to [[Michael Jackson]]'s (''right'') 1980's works.<ref name="urban"/><ref name="info"/>}}To promote the song, Stan embarked on a European tour, where she performed in Italy, United Kingdom, Turkey and Russia.<ref name="mediapro"/> Upon its release, Italian radio stations began playing the track on heavy rotation, thus making the song entering the top five of the iTunes Charts there after four days.<ref name="mediapro">{{cite news|url=http://www.mediapromusic.ro/stiri/10879242-alexandra-stan-lanseaza-un-nou-videoclip-pentru-piesa-all-my-people|title=Alexandra Stan lanseaza un nou videocip, pentru piesa "All My People"|publisher=Media Pro Music|accessdate=26 May 2016|language=Romanian|trans_title=Alexandra Stan releases a new video, for "All My People"}}</ref> An official music video for the song was first previewed with a teaser on 2 May 2016,<ref name="teaser">{{cite news|url=http://www.urban.ro/muzica/videoclipuri/alexandra-stan---all-my-people-teaser-videoclip|title=Alexandra Stan - All My People (teaser)|publisher=Urban.ro|accessdate=26 May 2016|language=Romanian|date=2 May 2013|author=Scris de Edi}}</ref> following which it was released onto [[YouTube]] on 3 May 2016.<ref name="urban"/> Shot in [[Sofia]], Bulgaria, at a heating power plant by Bulgarian director Ilarionov Borisov, Stan confessed about their collaboration that "[she] knew [him] for a long time and [they] wanted to collaborate already from [her] first music video."<ref name="mediapro"/> Furthermore, she described the filming process as being "difficult", as "the crew didn't speak English or Romanian".<ref name="mediapro"/> 

The clip commences with a police car arriving at the heating power plant, followed by Stan lying on a bed surrounded by surveillance cameras, sporting black lingerie. She is then introduced wearing a black hat and white shirt with a tie, while a crowd of male and female dancers perform a synchronized choreography. Following this, Stan makes an appearance wearing a white dress, and a police car is shown arriving. Subsequently, two policemen get out of the car in order to search for Stan. However, they do not find her in the bed and the video ends with Stan leaving the building and the men watching material recorded by the surveillance cameras.<ref>{{cite news|url=https://www.youtube.com/watch?v=2oIlS-ARtA8|title=Alexandra Stan vs. Manilla Maniacs - All My People (Official Music Video)|publisher=[[YouTube]]|accessdate=26 May 2016|date=3 May 2013}}</ref>

Alexandra Necula, writing for Romanian music website Info Music, said that the clip "exploited the sexuality and sensuality of the artist", with her explaining that Stan's appearance in the video portrayed a woman with [[pornographic]] tents.<ref name="info">{{cite news|url=http://www.infomusic.ro/2013/05/alexandra-stan-single-nou-all-my-people-vs-manilla-maniacs-videoclip/|title=Alexandra Stan are un single nou|publisher=Info Music|accessdate=26 May 2016|date=7 May 2013|author=Necula, Alexandra|language=Romanian|trans_title=Alexandra Stan has released a new single}}</ref> She said that Stan's moves were "lascive", and compared one of her outfits throughout the video to Madonna.<ref name="info"/> Urban.ro compared the choreography of the clip to Michael Jackson's 1980s works.<ref name="teaser"/> [[Los 40 Principales]] cited the video for "All My People" as one of Stan's best clips ever.<ref name="los40">{{cite web | title=Los mejores videoclips de Alexandra Stan | trans_title=The best videos of Alexandra Stan | url=http://los40.com/los40/2014/08/14/actualidad/1408031064_063471.html|date=14 August 2014|language=Spanish | publisher=[[Los 40 Principales]]|accessdate=20 June 2016}}</ref>

== Track listing ==
*'''Digital download'''<ref name="iTunes.es"/>
# "All My People" - 3:19

*'''Digital remixes EP'''<ref name="iTunes.it2"/>
# "All My People" (Fedo Mora & Oki Doro Remix Edit) - 2:48
# "All My People" (Fedo Mora & Oki Doro Remix) - 4:50
# "All My People" (Rudeejay Remix Edit) - 2:55
# "All My People" (Rudeejay Remix) - 5:24

==Charts==
{| class="wikitable sortable plainrowheaders"
|-
! Chart (2013–14) 
! Peak<br>position
|-
!scope="row"|Italy ([[FIMI]])<ref>{{cite web |url=http://fimi.it/classifiche#/category:digital/year:2015/id:1544/page:1|title=FIMI: Classifica settimanale WK 20 (dal 2013-05-13 al 2013-05-19) |publisher=[[Federation of the Italian Music Industry]] |accessdate=11 June 2016}}</ref>
|<center>55</center>
|-
!scope="row"|Japan ([[Japan Hot 100]])<ref>{{cite web | url=http://www.billboard.com/biz/search/charts?f[0]=ts_chart_artistname%3Aalexandra%20stan&f[1]=ss_bb_type%3Achart_item&type=2&artist=alexandra%20stan|title=Alexandra Stan – Chart history | publisher=''[[Billboard (magazine)|Billboard]]'' | accessdate=22 October 2016}}</ref>
| <center>50</center>
|-
|}

==Release history==
{|class="wikitable plainrowheaders unsortable"
|-
!Country
!Date
!Format
!Label
|-
!scope="row"|Romania<ref name="iTunes.ro">{{cite news|url=https://itunes.apple.com/ro/album/all-my-people-alexandra-stan/id662746619|title=All My People (Alexandra Stan vs. Manilla Maniacs) - Single by Alexandra Stan on iTunes|date=1 May 2013|work=iTunes Store|accessdate=26 May 2016}}</ref>
|1 May 2013
|rowspan="2"|Digital single
|MediaPro
|-
!scope="row" rowspan="2"|Italy<ref name="iTunes.it">{{cite news|url=https://itunes.apple.com/it/album/all-my-people-alexandra-stan/id642400415|title=All My People (Alexandra Stan vs. Manilla Maniacs) - Single di Alexandra Stan & Manilla Maniacs su iTunes|date=3 May 2013|work=iTunes Store|accessdate=26 May 2016}}</ref><ref name="iTunes.it2">{{cite news|url=https://itunes.apple.com/it/album/all-my-people-remixes-alexandra/id668518830|title=All My People (Remixes) [Alexandra Stan vs. Manilla Maniacs] - EP di Alexandra Stan & Manilla Maniacs su iTunes|date=12 July 2013|work=iTunes Store|accessdate=26 May 2016}}</ref>
|3 May 2013
|rowspan="2"|Vae Victis
|-
|12 July 2013
|Digital remixes EP
|-
!scope="row" rowspan="2"|Spain<ref name="iTunes.es">{{cite news|url=https://itunes.apple.com/es/album/all-my-people-alexandra-stan/id657332258|title=All My People (Alexandra Stan vs. Manilla Maniacs) - Single de Alexandra Stan & Manilla Maniacs en iTunes|date=2 July 2013|work=iTunes Store|accessdate=26 May 2016}}</ref><ref name="iTunes.es2">{{cite news|url=https://itunes.apple.com/es/album/all-my-people-remixes-alexandra/id735200012|title=All My People (Remixes) [Alexandra Stan vs. Manilla Maniacs] - EP de Alexandra Stan & Manilla Maniacs en iTunes|date=2 July 2013|work=iTunes Store|accessdate=26 May 2016}}</ref>
|2 July 2013
|Digital single
|rowspan="2"|Blanco y Negro
|-
|26 November 2013
|Digital remixes EP
|-
!scope="row"|United States<ref name="iTunes.us">{{cite news|url=https://itunes.apple.com/us/album/all-my-people-single/id651096263|title=All My People - Single by Alexandra Stan & Manilla Maniacs on iTunes|date=28 March 2013|work=iTunes Store|accessdate=26 May 2016}}</ref>
|28 May 2013
|Digital single
|rowspan="1"|Ultra
|-
|}

==References==
{{reflist|30em}}

==External links==
* {{MetroLyrics song|alexandra-stan|all-my-people}}
*Alexandra Stan's [http://www.alexandrastantheartist.com official website]

{{Alexandra Stan}}
{{Good article}}

[[Category:2013 singles]]
[[Category:2013 songs]]
[[Category:Alexandra Stan songs]]
[[Category:English-language Romanian songs]]
[[Category:Dance-pop songs]]
[[Category:Romanian songs]]