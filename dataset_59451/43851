{{other uses2|Voisin}}
{{Infobox company 
|name = Société Anonyme des Aéroplanes G. Voisin
|logo = 
|fate = 
|successor = 
|foundation = 1906 <!-- {{Start date|YYYY|MM|DD}} -->
|defunct    = <!-- {{End date|YYYY|MM|DD}} -->
|location = France
|industry = Aerospace
|key_people = [[Gabriel Voisin]]<br>[[Maurice Colieux]]
|products = 
|num_employees = 
|parent = 
|subsid = 
|intl=yes
}}
<!-- This article was once named "Aéroplanes Voisin", but this is not a name very commonly found in sources. Simply "Voisin". -->'''Voisin''' was a [[France|French]] [[aircraft]] manufacturing company, one of the first in the world. It was established in 1906 by [[Gabriel Voisin]] and his brother [[Charles Voisin|Charles]], and was continued by Gabriel after Charles died in an automobile accident in 1912; the full official company name then became '''''Société Anonyme des Aéroplanes G. Voisin'''''<ref>{{cite book|last=Baldwin|first=Nick|title=The World guide to automobile manufacturers|year=1987|publisher=Facts on File Publications|location=New York, N.Y.|isbn=0-8160-1844-8|page=508}}</ref><ref name=a/><ref group=n>Gunston, 1993, says the full name was "Aéroplanes G. Voisin". On the other hand the avions-voisin.org webpage specifies the name as "Société Aéroplanes Voisin, Société Anonyme".</ref> ({{lang-en| Aeroplanes Voisin [[public limited company]]}}). It created Europe's first manned, heavier-than-air powered aircraft capable of a sustained (1&nbsp;km), circular, controlled flight, including take-off and landing, the [[Voisin-Farman I]]. On 28 December 1909, French aviator M. Albert Kimmerling made the first manned, heavier-than-air powered flight in South Africa or even Africa in a [[Voisin 1907 biplane]].<ref>http://www.sapfa.org.za/history/1910-1920-early-flying-south-africa 1910 to 1920 - Early Flying in South Africa</ref>

During [[World War I]], it was a major producer of military aircraft, notably the [[Voisin III]].  After the war Gabriel Voisin abandoned the aviation industry, and set up a company to design and produce luxury automobiles, called [[Avions Voisin]].

==Background==
[[File:Voisin-Farman I.jpg|thumb|Voisin-Farman 1 winning the ''Grand Prix de l'aviation'', 13 January 1908]]
Gabriel Voisin had been employed by [[Ernest Archdeacon]] to work on the construction of gliders and then entered into partnership with  [[Louis Blériot]], to form the company ''Ateliers d' Aviation Edouard Surcouf, Blériot et Voisin'' in 1905.<ref name=g/> Gabriel Voisin bought out Blériot and on 5 November 1906 established the '''''Appareils d'Aviation Les Frères Voisin''''' with his brother Charles <ref name=g/> ({{lang-en|Flying Machines of Voisin Brothers}}). The company, based in the [[Paris]]ian suburb of [[Boulogne-Billancourt|Billancourt]], was the first commercial aircraft factory in the world.<ref>Davilla & Soltan, p. 541</ref>

==Early history==
Like many early aircraft companies, Voisin were prepared to build machines to the designs of customers, this work supporting their own design experiments. The company's first customers were a M. Florencie,<ref>Opdycke 1999 p.263</ref> who commissioned them to build an [[ornithopter]] he had designed, and [[Henri Kapferer]], for whom they built a [[pusher configuration]] biplane of their own design.  The latter was underpowered, having a Buchet engine of only {{convert|20|hp|kW|abbr=on}}, and it failed to fly.  However, Kapferer introduced them to [[Leon Delagrange]], for whom they built a similar machine, powered by a {{convert|50|hp|kW|abbr=on}} [[Antoinette 8V|Antoinette]] engine.  This was first successfully flown by Charles Voisin on 30 March 1907, achieving a straight-line flight of {{convert|60|m|ft|abbr=on}}.<ref>[http://gallica.bnf.fr/ark:/12148/bpt6k6551462k/f115.image Nouveaux Essais de l'Aéroplane Delagrange]''[[l'Aérophile ]]'', April 1907, p.105</ref> In turn Delagrange introduced them to [[Henri Farman]], who ordered an identical aircraft.  These two aircraft are often referred to by their owners' names as the '''Voisin-Delagrange No.1'''{{efn|marked on the side-curtains of the tail unit as '''Léon Delagrange No. 1'''}} and the '''Voisin-Farman No.1''',{{efn|marked on the side-curtains of the tail unit as '''Henri Farman No. 1'''}} and were the foundation of the company's success.  On 13 January 1908 Farman used his aircraft to win the "Grand Prix de l'aviation" offered by [[Ernest Archdeacon]] and [[Henry Deutsch de la Meurthe]] for the first closed-circuit flight of over a kilometre. Since the achievements of the [[Wright Brothers]] were widely disbelieved at the time, this was seen as a major breakthrough in the conquest of the air, and brought Voisin Frères many orders for similar aircraft; around sixty were built.

==Designs of 1907-1914==
[[File:Voisin 1910 2-man aircraft with mitrailleuse.jpg|thumb|1910 experimental two-seater biplane with ''[[mitrailleuse]]'' fired by the passenger]]
[[File:CanardVoisin.jpg|thumb|''[[Voisin Canard]]'' seaplane under trial on the Seine, on August 3, 1911. The front of the aircraft is to the right.]]

*1907 [[Voisin 1907 biplane]]
*1909 Voisin Tractor<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200797.html The New Voisin Biplane]. [[Flight International|''Flight'']], 11 December 1909 p. 799</ref>
::Only one built.
*1910 [[Voisin Type de Course]]
*1910 Voisin Type Militaire
*1910 [[Voisin Type Bordeaux|Type Bordeaux]]
*1911 [[Voisin Canard]]
::Initially flown as a landplane but later fitted with floats. Examples were sold to the French Navy and to Russia.
* 1911 [[Type Tourism]]
*1912 Type Monaco
::Smaller version of the Canard floatplane. Two built to take part in the 1912 Monaco Aero Meeting.
*1912 [[Voisin Icare Aero-Yacht]]
::Flying boat built for [[Henry Deutsch de la Meurthe]]
*1912 Voisin Type L or Voisin Type I
::Developed for the French Army's 1912 trials. It performed successfully, and some seventy were built in France, and a small number in [[Russia]]
*1914 Type LA or Voisin Type III

==Voisin designs in World War I==
{{main|Voisin III}}
production of the Type III  increased with the outbreak of the [[First World War]]. The Voisin III was followed by improved ''Type LB'' and ''Type LBS'', or [[Voisin IV]] and [[Voisin V]] aircraft. The larger ''Type LC'', [[Voisin VII]], followed in 1916, but was not a success and only a hundred were built.

Soon after the outbreak of the First World War, it became apparent that the French aviation industry could not produce aircraft in sufficient numbers to meet military requirements. Manufacturers from various other fields became aviation subcontractors, and later license-builders. The earliest such partnership was between [[Louis Breguet]] and [[Michelin]]. Gabriel Voisin was late to this field, although his designs were produced in quantity by Russian licensees. By 1918, Voisin was involved with the Voisin-Lafresnaye company, a major constructor of airframes, and the Voisin-Lefebvre company, a major builder of aircraft engines.

Following the Voisin VII came the more powerful, and more successful, ''Type LAP'' and ''Type LBP'', known as the [[Voisin VIII]]. This was the French army's main night bomber in 1916&ndash;1917, with over one thousand built. The [[Voisin IX]], or ''Type LC'', was an unsuccessful design for a reconnaissance aeroplane, which lost out to the superior [[Salmson 2]] and [[Breguet 14]]. The [[Voisin X]], ''Type LAR'' and ''Type LBR'', was the Voisin VIII with a more reliable [[Renault]] engine in place of the previous [[Peugeot]] design. Deliveries were much delayed, but some nine hundred were built by the end of the war.

The last significant Voisin design, the [[Voisin XII]], was successful in trials in 1918, but with the end of the war, no production was ordered. Unlike previous Voisins, the Voisin XII was a large, twin-tractor-engined biplane night bomber, rather more elegant than previous, boxy Voisins.

===Voisin X ambulance variant===
In 1918, a Voisin X (No. 3500) was used to create the Voisin 'Aerochir' ('Ambulance').  The aircraft was capable of flying a surgeon, together with an operating table and support equipment, including an [[x-ray]] machine and [[autoclave]], into the battlefield.  Eight hundred pounds (360&nbsp;kg) of equipment could be carried in under-wing [[pannier]]s.<ref>Stamford, Lincs., U.K.: FlyPast, Key Publishing Ltd, ''Flying Hospital'', April 2007 No. 309 p. 14</ref>

==Post World War I==
{{main|Avions Voisin}}
After 1918, Gabriel Voisin abandoned the aviation industry in favor of automobile construction under the brand [[Avions Voisin]].

==Other types of aircraft==
*[[Voisin Triplane]]

==Notes==
{{Reflist|group=n}}

==See also==
*[[Avions Voisin]]

==Notes==
{{notelist}}

==References==
{{commons category|Voisin aircraft}}
{{Reflist|refs=
<ref name=g>{{cite book|last=Gunston|first=Bill|title=World encyclopaedia of aircraft manufacturers: from the pioneers to the present day|year=1993|publisher=Naval Institute Press|isbn=1-55750-939-5|url=https://books.google.com/books?ei=0nD4TfrqOsjKswbkz6CKCQ&ct=result&id=ObAeAQAAMAAJ&dq=%22Appareils+d%27Aviation+Les+Fr%C3%A8res+Voisin%22&q=%22Appareils+d%27Aviation%22#search_anchor|page=318}}</ref>

<ref name=a>http://www.avions-voisin.org/public/rubrique.php3?id_rubrique=12</ref>

}}

===Bibliography===
* {{fr icon}} Carlier, Claude, ''Sera Maître du Monde, qui sera  Maître de l'Air:  La Création de l'Aviation militaire française.'' Paris: Economica/ISC, 2004. ISBN 2-7178-4918-1
* Davilla, James J., & Soltan, Arthur M., ''French Aircraft of the First World War.'' Stratford, Connecticut: Flying Machines Press, 1997. ISBN 0-9637110-4-0
* Opdycke, Leonard E ''French Aeroplanes Before the Great War'' Atglen, PA: Schiffer, 1999 ISBN 0-7643-0752-5
* Voisin, Gabriel, ''Mes 10,000 Cerfs-volants'', Editions La Table Ronde, Paris, 1960.
* ( Italy ) Grassani, Enrico "''Elisa Deroche alias Raymonde de Laroche. La presenza femminile negli anni pionieristici dell'aviazione''" Editoriale Delfino, Milano 2015. ISBN 978-88-97323-46-4

{{Voisin aircraft}}
{{Defunct aircraft manufacturers of France}}

[[Category:Defunct aircraft manufacturers of France]]
[[Category:Companies established in 1906]]
[[Category:Companies disestablished in 1918]]