{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name = Cousin Henry
| title_orig = 
| translator = 
| image = Cousin Henry.jpg
| image_size =170px
| caption = Title page to the first edition in book form
| author = [[Anthony Trollope]]
| illustrator = 
| cover_artist = 
| country = United Kingdom
| language = English
| series = Weekly: 8 March 1879 – 24 May 1879
| genre =
| publisher = [[Chapman &amp; Hall]]
| release_date = October 1879
| english_release_date =
| media_type = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]], and [[Paperback]])
| pages = 
| isbn = 
| preceded_by =
| followed_by = 
}}
'''''Cousin Henry''''' is a novel by [[Anthony Trollope]] first published in [[1879 in literature|1879]]. The story deals with the trouble arising from the indecision of a squire in choosing an heir to his estate.

Of all Trollope's shorter novels, this one has been called one of his most experimental.<ref name=Phelps>{{cite web |url=http://www.victorianweb.org/authors/trollope/tsociety/henry.html  |title=Cousin Henry: An Introduction |author=Gilber Phelps |publisher=victorianweb.org, published with the permission of "The Trollope Society" |accessdate=August 14, 2006}}</ref>

==Plot summary==

Indefer Jones is the aged squire, between seventy and eighty years of age, of a large manor, Llanfeare, in [[Carmarthen]], Wales. His niece, Isabel Brodrick, has lived with him for years after the remarriage of her father, and endeared herself to everyone. However, according to his strong traditional beliefs, the estate should be bequeathed to a male heir.

His sole male blood relative is his nephew Henry Jones, a London clerk. Henry has, in the past, incurred debts that the squire had paid off, been "sent away from Oxford", and generally made a poor impression on his occasional visits to Llanfeare. Nevertheless, Henry is told of his uncle's intention to make him the heir to the estate and is invited to pay a visit. Isabel rejects her uncle's suggestion that she solve his dilemma by marrying Henry, as she cannot stand her cousin. Indefer Jones finds his nephew to be just as detestable as ever. As a result, he overcomes his prejudice and changes his will one final time, in Isabel's favour. Unfortunately, he dies before he can tell anyone.

Finding the document hidden in a book of sermons by accident, Henry vacillates between keeping silent and revealing its location. He is neither good enough to give up the estate nor evil enough to burn the document, fearing disgrace, a long jail sentence and, not least, eternal damnation. Instead, he comforts himself by reasoning that doing nothing cannot be a crime.

Indefer Jones had had his last will witnessed by two of his tenants, but since the will cannot be found despite a thorough search of the house, Henry inherits the estate. However, already extant suspicions are only strengthened by his guilty manner. He endures abuse from everyone; his own servants either quit or treat him with disrespect. He takes to spending hours in the library, where the will is hidden.

The local newspaper begins to publish accounts of the affair that are insulting and seemingly libelous to Henry. It accuses him of destroying the will and usurping the estate from Isabel, whom everybody knows and respects. The old squire's lawyer, Mr Apjohn, himself suspecting that Henry knows more than he lets on, approaches the new squire about the articles, pressuring the unwilling young man into taking legal action against the editor. Henry finds that this only makes things worse. The prospect of being cross examined in the witness box fills him with dread. He realises the truth would be dragged out of him in court.

Mr Apjohn, by clever questioning, gets a good idea about where the will is. Henry knows that time is running out, but once again procrastinates. Mr Apjohn and Mr Brodrick, Isabel's father, visit Henry at home and find the document, despite Henry's ineffectual efforts to stop them. Because he did not destroy the will, Henry is permitted to return to his job in London with his reputation intact and £4000, the amount Isabel was bequeathed in the other will.

==References==
{{reflist}}

== External links ==

* {{gutenberg |no=24103 |title=Cousin Henry}}
* {{librivox book | title=Cousin Henry | author=Anthony Trollope}}

{{Anthony Trollope}}

[[Category:1879 novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Novels set in Wales]]
[[Category:Carmarthenshire]]
[[Category:Chapman & Hall books]]