{|{{Infobox Aircraft Begin
 |name=Sonerai
 |image=Monnett-Sonerai-g-rily.jpg
 |caption=Sonerai II
}}{{Infobox Aircraft Type
 |type=Homebuilt aircraft
 |manufacturer=Monett Aircraft
 |designer=[[John Monnett]]
 |first flight=20 July 1971
 |introduced=1971
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=
 |unit cost= [[US$]]125 (plans only, 2015)<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 110. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>
 |variants with their own articles=
}}
|}
The '''Sonerai''' is a small, [[Volkswagen air-cooled engine|VW]]-powered [[homebuilt aircraft]], designed by [[John Monnett]].<ref>{{cite journal|magazine=Plane and Pilot's Homebuilt Aircraft Annual|date=Winter 1975}}</ref>  The Sonerai began to compete as a single seat mid-wing [[Formula V Air Racing|Formula-V racer]] class formed in 1972.  The Sonerai soon evolved into a two-seat model called the Sonerai II.  Later versions included a low wing Sonerai IIL, a tricycle gear Sonerai IILT and finally the stretched Sonerai IILS and IILST.

John Monnett came up with the name Sonerai from a combination of the words ''Sonic and Cosmic-Ray'' <ref>{{cite journal|magazine=Sport Aviation|date=May 1991|title=Bob Barton's Sonerai IIL|author=Bob Barton}}</ref> Many Sonerais have been built and it remains a very popular design for people seeking a low cost experimental aircraft with good speed and maneuverability.  The airframe cost to build in 1974 was estimated at $2,500. The 2010 airframe cost is approximately $6,000 (US) and the total cost is approximately $15,000 (US) with the addition of hardware, instrumentation, engine and other required items.  The time to build is between 800 and 1000 hours.<ref>{{cite web|url=http://www.greatplainsas.com/soneraihistory.html|title=Sport Aircraft History|publisher=Great Plains Aircraft|accessdate=2008-10-08}}</ref>

==Design==
The Sonerai I design and construction started in 1970 with the goal of a flying aircraft to be demonstrated at the 1971 EAA airshow. The aircraft was to meet the new Formula V rules. Inspired by the [[Supermarine Spitfire|Spitfire]], an elliptical tail profile was incorporated. Elliptical wingtips and a low-wing configuration was dropped, but a low-wing Sonerai II variant was released later.<ref>{{cite journal|magazine=Sport Aviation|date=March 1972|title=Race to Oshkosh|page=6}}</ref> The Sonerai I was designed to use a direct drive 1,600cc VW engine and the Sonerai II was designed to use the 1700cc VW engine. The wings were designed to fold alongside the fuselage for towing without a trailer and compact storage. The Sonerai II was designed to be soloed from the rear seat. The aircraft is built around a fabric covered steel tube fuselage with all aluminum wings and a fiberglass nose cowl. The plans cost $50 and $57 in 1974.<ref>{{cite journal|magazine=Popular Mechanics|title=Flying Twins your choice of one or two|date=Aug 1974|pages=108}}</ref>

[[Great Plains Aircraft Supply Company]] held the rights to the Sonerai series of aircraft until 2015. Sonerai Works LLC purchased the rights to Sonerai plans and parts from Great Plains in 2015.<ref>{{cite journal|magazine=Sport Aviation|date=February 2015|page=15}}</ref> The aircraft is not available as a kit, and is built using plans, although some parts are available.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 104. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Operational history==
The prototype Sonerai 1 was displayed at the [[EAA AirVenture Oshkosh|Experimental Aircraft Association Airshow]] in [[Oshkosh, Wisconsin]] where Monnett eventually relocated. The aircraft was painted a bright green that became the color of all the future prototypes and company marketing. The shade was from a 1971 [[Dodge Charger (B-body)|Dodge Charger]] John Monnett saw painted Sassy Grass Green.<ref>{{cite book|title=John Monnett from Sonerai to Sonex|author=Jim Cunningham}}</ref>

=== World records ===
[[File:Sonerai Formula V.jpg|right|thumb|Dempsey Sonerai I]]
Brian Dempsey built a Sonerai I that set a world record. The C-1a/0 (Landplanes: takeoff weight less than 300&nbsp;kg) Speed over a straight 15/25&nbsp;km course of 292.15&nbsp;km/h on February 19, 1989.<ref>{{cite web|title=FAI world Records|url=http://www.fai.org/fai-record-file/?recordId=1094|accessdate=18 Jan 2015}}</ref> Dempsey's record stood for 20 years.

Robin Austin of Australia built a Sonerai IIL with a 100&nbsp;hp Rotax engine. The aircraft has set the following FAI records for C-1a/0 (Landplanes: takeoff weight less than 300&nbsp;kg).<ref>{{cite journal|magazine=Sport Aviation|author=Robin Austin|title=Four World Records|date=October 2010}}</ref>
* Aeroplane Efficiency : 29.79&nbsp;km/kg 05/06/2008 Jacobs Well, QLD (Australia)<ref>"[http://www.fai.org/fai-record-file/?recordId=15065 FAI Record ID #15065 - Aeroplane Efficiency, C-1a (Landplanes: take off weight 300 to 500 kg) ]" ''[[Fédération Aéronautique Internationale]]'' Record date 5 June 2008. Accessed: 4 October 2015.</ref>
* Speed over a recognized course  404.3&nbsp;km/h 17 May 2008 St. George, QLD (Australia) - Brisbane, QLD (Australia)<ref>"[http://www.fai.org/fai-record-file/?recordId=15060 FAI Record ID #15060 - Speed over a recognized course, C-1a (Landplanes: take off weight 300 to 500 kg)]" ''[[Fédération Aéronautique Internationale]]'' Record date 17 May 2008. Accessed: 4 October 2015.</ref>
* Speed over a recognized course  440.0&nbsp;km/h 28 Jul 2008  Blackall, QLD (Australia) - Rockhampton, QLD (Australia)<ref>"[http://www.fai.org/fai-record-file/?recordId=15100 FAI Record ID #15100 - Speed over a recognized course, C-1a (Landplanes: take off weight 300 to 500 kg)]" ''[[Fédération Aéronautique Internationale]]'' Record date 28 July 2008. Accessed: 4 October 2015.</ref>
* Aeroplane Efficiency 37.22&nbsp;km/kg 08 Jun 2008  C1-b Class<ref>"[http://www.fai.org/fai-record-file/?recordId=15066 FAI Record ID #15066 - Aeroplane Efficiency, C-1b (Landplanes: take off weight 500 to 1000 kg) ]" ''[[Fédération Aéronautique Internationale]]'' Record date 8 June 2008. Accessed: 4 October 2015.</ref>

==Variants==
[[File:Monnett Sonerai I.jpg|right|thumb|Sonerai I single seat racer]]
[[File:Sonerai 2 Frame.jpg|right|thumb|Sonerai II Fuselage]]
[[File:Sonerai-Subaru.jpg|thumb|right|Sonerai II with a Subaru engine]]
;Sonerai I
:Single-seat [[Formula V Racer]]<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 171. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>
;Sonerai II
:Two-seat mid-wing<ref name="Aerocrafter" />
;Sonerai II-L
:Two-seat low-wing<ref name="Aerocrafter" />
;Sonerai II-LT
:Two-seat low-wing [[tricycle gear]] developed in 1983.<ref name="Aerocrafter" /><ref>{{cite journal|magazine=Flying|date=May 1983|page=12}}</ref>
;Sonerai II-LS
:Two-seat low-wing stretch fuselage<ref name="Aerocrafter" />
;Sonerai II-LTS
:Two-seat low-wing stretch tricycle gear<ref name="Aerocrafter" />

==Specifications (Sonerai II) ==
[[File:Sonerai IIL.jpg|thumb|right|Sonerai II-L]]
{{Aircraft specs
|ref=
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=18
|length in=10
|length note=
|span m=
|span ft=18
|span in=8
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=520
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=950
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Volkswagen air-cooled engine]]
|eng1 type=air-cooled [[flat-four]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=200
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=140
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=45<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|range km=
|range miles=245
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=500
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=
|thrust/weight=

|more performance=

|avionics=
}}

==See also==
* [[Monnett Moni]]
* [[Monnett Monerai]]
* [[Sonex Aircraft]]
*[[Southern Aeronautical Scamp]]

==References==
{{reflist}}

==External links==
{{commons category|Monnett Sonerai}}
* {{Official website|http://www.sonerai.com}}

{{Monnett aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Monnett aircraft|Sonerai]]
[[Category:United States civil utility aircraft 1970–1979]]
[[Category:Mid-wing aircraft]]
[[Category:1971 introductions]]