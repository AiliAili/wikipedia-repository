{{Use dmy dates|date=April 2012}}
{{Infobox person
| honorific_prefix          = 
| name                      = Alan Derek Piggott
| honorific_suffix          = MBE
| native_name               = 
| native_name_lang          = 
| image                     = Derek Piggott.JPG
| image_size                = 
| alt                       = 
| caption                   = Derek Piggott in his natural habitat at the Lasham Regional Competition in 2005
| birth_name                = 
| birth_date                = {{Birth date and age|1922|12|27|df=y}}
| birth_place               = 
| disappeared_date          = <!-- {{Disappeared date and age|YYYY|MM|DD|YYYY|MM|DD}} (disappeared date then birth date) -->
| disappeared_place         = 
| disappeared_status        = 
| death_date                = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place               = 
| death_cause               = 
| body_discovered           = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 = 
| residence                 = 
| nationality               = [[England|English]]
| other_names               = 
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = [[United Kingdom]]
| education                 = 
| alma_mater                = 
| occupation                = Retired Flying Instructor and Author
| years_active              = 1942-
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = 
| salary                    = 
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| television                = 
| title                     = 
| term                      = 
| predecessor               = 
| successor                 = 
| party                     = 
| movement                  = 
| opponents                 = 
| boards                    = 
| religion                  = <!-- Religion should be supported with a citation from a reliable source -->
| denomination              = <!-- Denomination should be supported with a citation from a reliable source -->
| criminal_charge           = <!-- Criminality parameters should be supported with citations from reliable sources -->
| criminal_penalty          = 
| criminal_status           = 
| spouse                    = 
| partner                   = <!-- unmarried life partner; use ''Name (1950–present)'' -->
| children                  = 
| parents                   = 
| relatives                 = 
| callsign                  = 
| awards                    = 
| signature                 = 
| signature_alt             = 
| signature_size            = 
| module                    = 
| module2                   = 
| module3                   = 
| module4                   = 
| module5                   = 
| module6                   = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
| box_width                 = 
}}

'''Alan Derek Piggott''' [[Order of the British Empire|MBE]] (born 27 December 1922) is one of Britain's best known [[gliding|glider]] pilots and instructors. He has over 5,000 hours on over 153 types of powered aircraft and over 5,000 hours on over 184 types of [[Glider (sailplane)|glider]]. He has been honoured for his work on the instruction and safety of glider pilots. In 1961 he became the first person to make an officially authenticated take-off and flight in a [[Human powered flight|man-powered aircraft]]. He has also worked as a stunt pilot in several feature films.

==Early years==
He was born in Chadwell Heath, Essex, the son of Rev. William Piggott and Alice Harvey. His father was a conscientious objector in the First World War, led the rent strike against London County Council after the war and was a frequent speaker at Hyde Park. When his mother died, the family moved to Sutton, Surrey where Derek attended Sutton County School. When he left school he became a trainee scientific instrument maker. He had been a very active aero-modeller and helped to form the Sutton Model Aircraft Club. After the war he was selected to be a member of the British Wakefield Cup team, a prestigious aero modelling competition held that year in Akron, Ohio. He first flew in an [[Avro 504]] as a passenger at the age of four.

==Royal Air Force==
Derek Piggott joined the [[Royal Air Force]] in 1942 as aircrew and made a first solo in a [[de Havilland DH.82A Tiger Moth]] after only six hours dual.  He completed his training in Canada and was commissioned as a [[Pilot Officer]] in 1943. He was then sent on a multi-engine instructors' course and then on a course for elementary instructors before returning to England.  By 1944 there was a surplus of trained pilots and he volunteered to fly [[military gliders]].  After a short conversion to the [[Airspeed Horsa]], [[General Aircraft Hotspur]] and [[Waco Hadrian]], he was posted to India and then on to Burma where he flew [[Douglas Dakota]]s dropping supplies to front-line troops. During his stay in India, he instructed [[Indian Air Force]] students and flew low anti-riot patrols just before partition.

Back in the UK he was posted as a Staff Instructor at the [[Central Flying School]] at [[RAF Little Rissington]] where he trained instructors and flew [[North American Harvard]]s, [[Boulton Paul Balliol]]s, [[Avro Athena]]s, [[Gloster Meteor]]s, [[Supermarine Spitfire]]s, [[de Havilland Mosquito]]s and [[Avro Lancaster]]s.  After being awarded an A1 Instructor Rating, he joined the Home Command Gliding Instructors' School at [[Detling]] teaching civilian instructors for the [[Air Training Corps]] on [[Slingsby T.21]] and [[Slingsby Kirby Cadet]] gliders.  As Chief Flying Instructor he introduced training methods that greatly improved safety.  He also taught school teachers in the [[Combined Cadet Force]] how to teach flying in primary gliders.  Flying with an ATC cadet as co-pilot in the national gliding championships, he established a British two-seater altitude record, in a T.21, climbing to over {{convert|17000|ft|m}} in a thunderstorm over [[Sheffield]]. In 1953 Piggott received the Queen's Commendation for work on developing and introducing new instructional techniques for gliding in the ATC.

==Gliding career==
In 1953, he left the RAF as a [[Flight Lieutenant]] and was the Chief Flying Instructor (CFI) for [[Lasham Airfield|Lasham Gliding Society]] between 1953 and 1989, though he took breaks during this time to do stunt flying. 
He has travelled widely, lecturing and advising gliding associations such as the [[Soaring Society of America]] and the Dutch gliding association on instructional techniques such as the use of motor gliders in training.  As a leading authority on [[gliding]], he has written seven books on the subject, an autobiography, several monographs and many magazine articles.<ref name="GlidMagIn">{{cite web|last1=Piggott|first1=Derek|title=List of 'Derek On Instructing'|url=http://www.glidingmagazine.com/features.asp?issue_date=4%2F1%2F2008&search=%22Derek+On+Instructing%22|website=glidingmagazine.com|accessdate=14 August 2015}}</ref> His first book 'Gliding' was first published in 1958 and is still in print in its eighth edition.

In addition he had success as a competition glider pilot winning three regional championships, was the UK National aerobatic glider champion in 1961 and set several national gliding records including the single-seat altitude record of over {{convert|25000|ft|m}} in an active thunderstorm in a [[Slingsby Skylark]].  He holds the [[Fédération Aéronautique Internationale|FAI]] Diamond Badge.  In 2003 at the age of 81, he completed a {{convert|505|km|abbr=on}} task in a [[Fedorov Me7 Mechta]] glider with only a {{convert|12.7|m|abbr=on}} span in a national competition in a time of 7hr 14min. (Several much younger pilots with superior machines failed to complete this task). He ceased flying gliders solo in December 2012 and no longer holds a full [[Private Pilot Licence]] (PPL).

He was a member of a test group for the [[British Gliding Association]] (BGA) and tested a number of prototype [[Glider (sailplane)|gliders]] and foreign machines for approval to be imported.  He made a successful emergency parachute descent from a damaged [[SZD-9 Bocian]] making him a member of the [[Caterpillar Club]].  He researched the effect of sub-gravity sensations as a cause for many serious and fatal gliding accidents.

==Other flying==
On 9 November 1961, flying [[SUMPAC]] ([[Southampton University]]'s Man Powered Aircraft), Derek Piggott became the first person to make an officially authenticated take-off and flight in a [[Man powered flight|man-powered aircraft]].<ref>{{cite news
|url=http://www.bbc.co.uk/news/uk-england-hampshire-15705954
|title=BBC News web-site
|accessdate= 15 November 2012
| date=12 November 2011
}}
</ref> The longest flight was {{convert|650|yd|m|abbr=on}}; turns were attempted, with 80 degrees the best achieved and he made a total of 40 flights in SUMPAC.<ref name=hpf>{{cite web|title=THE FIRST TRUE FLIGHTS SUMPAC| url=http://www.humanpoweredflying.propdesigner.co.uk/html/flights.html |website=http://www.humanpoweredflying.propdesigner.co.uk| accessdate=14 August 2015}}</ref>

He took a break from being a gliding instructor to become a stunt pilot and was also technical adviser on several feature [[film]]s. His role as a stunt pilot, began in 1965 with the film ''[[The Blue Max]]'' which tells the story of the competitive rivalry between two [[Germany|German]] pilots in the [[First World War]]. He was enlisted as one of several pilots who helped recreate the live dog-fight scenes for the film. However, he was the only stunt pilot to agree to fly for the climax of the film in which the two rivals challenge each other to fly beneath the spans of a bridge over a river. Taking the role of both German pilots and with multiple takes from contrasting camera angles, he ended up flying through the wide span of this bridge in Ireland 15 times and 17 times through the narrower span.  The two [[Fokker Dr.I]] replicas had about {{convert|4|ft|m|abbr=on|1}} of clearance on each side when passing through the narrower span.  Piggott was able to fly through the arch reliably by aligning two scaffolding poles, one in the river and one on the far bank.  The director had placed a flock of sheep next to the bridge so that they would scatter as the plane approached in order to demonstrate that the stunt was real and had not used models. However, by later takes, the sheep had become accustomed to the planes and continued to graze, and so they had to be scared by the shepherd.

In ''[[Darling Lilli]]'', he was responsible for the majority of the designs of six replica aircraft and for supervising their construction in a period of nine weeks. Some of the dog fight scenes are considered to be among the best made. However, they had to be re-shot the following year because the film was changed from being comic to serious.

Another notable film role, was Derek Piggott's contribution to ''[[Those Magnificent Men in Their Flying Machines]]'' in which he flew and advised on the construction of several of the early aircraft recreated for use in the film.  Many of the planes employed [[wing warping]] for directional control, which involved re-discovering how to fly them safely. Several of the aircraft had dangerous features and he had a number of narrow escapes.

In ''[[Villa Rides]]'' he had to crash an aircraft that was flying towards a cliff by making the undercarriage collapse.  This stopped it from {{convert|110|km/h|abbr=on}} in about {{convert|10|m|abbr=on}}

[[File:George Cayley Glider (Nigel Coates).jpg|thumb|right||The replica of Cayley's glider flown by Derek Piggott]]
Derek Piggott flew some or all the aerial stunts in several other films:
''[[Von Richthofen and Brown]] (The Red Baron)'';
''[[Agatha (film)|Agatha]]'';
''[[Slipstream]]'';
''[[You Can't Win 'Em All]]'';
''[[Chitty Chitty Bang Bang]]'' and for several television programmes.  For one of these TV  programmes a replica of the [[George Cayley|Sir George Cayley's]] first heavier-than-air flying machine was built in the early 1970s. The machine was flown by Derek Piggott at the original site in [[Brompton Dale]] in 1973 for a TV programme<ref name="glidmag1">{{cite web|last1=Piggott|first1=Derek|title=Gliding 1852 Style|url=http://www.glidingmagazine.com/FeatureArticle.asp?id=360|website=glidingmagazine.com|accessdate=14 August 2015|date=October 2003}}</ref> and again in 1985<ref name="glidmag2">{{cite web|last1=Short|first1=Simine|title=Stamps that tell a story|url=http://www.glidingmagazine.com/FeatureArticle.asp?id=357|website=glidingmagazine.com|accessdate=14 August 2015|date=October 2003}}</ref> for the [[IMAX]] film ''[[On the Wing (1986 film)|On the Wing]]''.

==Piggott Hook==
Derek Piggott also is the inventor of the "Piggott-Hook", which is to prevent air brakes opening on a launch. The system is installed in all new gliders built by [[DG Flugzeugbau]]<ref name=dg>{{cite web|title=The Piggott-Hook for locking the Brake Handle or How an Invention Begins|url=http://www.dg-flugzeugbau.de/piggott-haken-e.html|website=dg-flugzeugbau.de|accessdate=14 August 2015}}</ref>

==Honours==
In 1987 Derek Piggott was appointed a [[Member of the Most Excellent Order of the British Empire]] (MBE). In 2007 Derek Piggott was awarded the [[Royal Aero Club]] Gold Medal - the highest award for aviation in the UK. Also in 2007 the [[Royal Aeronautical Society]] appointed Derek Piggott an Honorary Companion of the Society.  In 2008 he was awarded the [[Lilienthal Gliding Medal]] by the [[Fédération Aéronautique Internationale]] for outstanding service over many years to the sport of gliding.
In July 2016 Derek Piggott was appointed President of the BHPFC (British Human Powered Flying Club) in recognition of his pioneering achievements in the field of human powered aviation.

==Bibliography==
* {{cite book |last=Piggott |first=Derek | title = Delta Papa A Life of Flying | isbn = 0-7207-0979-2 | publisher = Pelham Books |date = 1977 }}
* {{cite book |last=Piggott |first=Derek | title = Going Solo: A Simple Guide To Soaring | isbn = 0-7136-1899-X  | publisher = A & C Black |date = 1978 }}
* {{cite book |last=Piggott |first=Derek | title = Gliding: A handbook on soaring flight | isbn = 0-7136-6148-8 | publisher = A & C Black |date = 2002 }}
* {{cite book |last=Piggott |first=Derek | title = Gliding Safety | isbn = 0-7136-4853-8 | publisher = A & C Black |date = 1998 }}
* {{cite book |last=Piggott |first=Derek | title = Understanding Flying Weather | isbn = 0-7136-4346-3 | publisher = A & C Black |date = 1999 }}
* {{cite book |last=Piggott |first=Derek | title = Understanding Gliding | isbn = 0-7136-6147-X | publisher = A & C Black |date = 2002 }}
* {{cite book |last=Piggott |first=Derek | title = Beginning Gliding | isbn = 0-7136-4155-X | publisher = A & C Black |date = 2002 }}
* {{cite book |last=Piggott |first=Derek | title = Derek Piggott on Gliding | isbn = 0-7136-5799-5 | publisher = A & C Black |date = 1990 }}

His monographs are:
* 'Sub-gravity sensations and gliding accidents'
* 'Stop worrying about stalling and spinning'
* 'Using motor gliders for training glider pilots'
* 'Ground launches'

==References==
{{reflist}}

==External links==
*[http://www.lasham.org.uk/ Lasham Gliding Society]
*[http://www.communitychannel.org/video/8-DM7-_Iy38/the_memory_team_episode_1/ Video interview with Derek Piggott]

{{Authority control}}

{{Recipients of Lilienthal Gliding Medal}}

{{DEFAULTSORT:Piggott, Derek}}
[[Category:English aviators]]
[[Category:Gliding in England]]
[[Category:Glider pilots]]
[[Category:Members of the Order of the British Empire]]
[[Category:Living people]]
[[Category:Flight instructors]]
[[Category:Aviation writers]]
[[Category:Royal Air Force officers]]
[[Category:1922 births]]
[[Category:People from Romford]]
[[Category:British World War II pilots]]
[[Category:Lilienthal Gliding Medal recipients]]