{{Good article}}
{{Infobox song	
| Name          = Bassline
| Artist        = [[Chris Brown (American entertainer)|Chris Brown]]
| Album         = [[Fortune (Chris Brown album)|Fortune]]
| Genre         = {{flat list|
*[[Dubstep]]
*[[electropop]]
*[[electrohop]]
}}
| Length        = {{Duration|m=3|s=58}}
| Label         = [[RCA Records|RCA]]
| Writer        = {{flat list|
*Andrea Simms
*Andrew "Pop" Wansel
*Chris Brown
*David Johnson
*Robert Calloway
*Ronald "Flippa" Colson
*Warren "Oak" Felder
}}
| Producer      = {{flat list|
*Pop Wansel
*Dayvi Jae
}}
| Recorded      = 
| Type          = Song
| prev = "[[Turn Up the Music (Chris Brown song)|Turn Up the Music]]"
| prev_no = 1
| track_no = 2
| next = "[[Till I Die (Chris Brown song)|Till I Die]]"
| next_no = 3
| Misc =
}}

"'''Bassline'''" is a song by American recording artist [[Chris Brown (American entertainer)|Chris Brown]], taken from his fifth studio album ''[[Fortune (Chris Brown album)|Fortune]]'' (2012). It was written by Andrea Simms, Andrew "Pop" Wansel, Brown, David Johnson, Robert Calloway, Ronald "Flippa" Colson and Warren "Oak" Felder. The song was produced by Pop Wansel and Dayvi Jae. Musically, "Bassline" is a [[dubstep]], [[Synthpop|electropop]] and electrohop song, which incorporates elements of [[reggae]]. Instrumentation is provided by a [[Dubstep#Wobble bass|wobble bass]] and [[synth]]esizers. The song contains lyrics about Brown telling a woman to leave the [[nightclub]] with him. "Bassline" garnered mixed reviews from [[Music journalism|music critics]]; some reviewers noted it as one of the standout tracks on the album, while others criticized the song's production and lyrics. It also received comparisons to the songs by [[Kesha]] and [[LMFAO]]. Upon the release of ''Fortune'', "Bassline" debuted at numbers 28 and 122 on the [[UK R&B Chart]] and [[UK Singles Chart]], respectively.

==Development and composition==
"Bassline" was written by Andrea Simms, Andrew "Pop" Wansel, [[Chris Brown (American entertainer)|Chris Brown]], David Johnson, Robert Calloway, Ronald "Flippa" Colson and Warren "Oak" Felder.<ref name="booklet"/> The song was produced by Pop Wansel and Dayvi Jae.<ref name="booklet"/> "Bassline" was recorded by Brian Springer with assistance from Iain Findley.<ref name="booklet"/> The recordings were later [[Audio mixing (recorded music)|mixed]] by [[Jaycen Joshua]] with assistance by Trehy Harris.<ref name="booklet"/> Musically, "Bassline" is a [[dubstep]],<ref>{{cite news|last=Haider|first=Arwa|url=http://www.metro.co.uk/music/reviews/903687-chris-browns-fortune-is-mostly-flimsy-and-gooey|title=Chris Brown, Fortune: Album Review|work=[[Metro (British newspaper)|Metro]]|publisher=[[Associated Newspapers Ltd]]|date=2012-07-02|accessdate=2012-09-10}}</ref> [[Synthpop|electropop]]<ref name="hitfix">{{cite web|last=Newman|first=Melinda|url=http://www.hitfix.com/the-beat-goes-on/album-review-does-chris-brown-have-good-fortune-on-new-set|title=Album Review: Does Chris Brown have good Fortune on new set?|publisher=[[HitFix]]|date=2012-07-02|accessdate=2012-09-10}}</ref> and electrohop song,<ref>{{cite news|last=Rytlewski|first=Evan|url=http://www.avclub.com/articles/chris-brown-fortune,82179/|title=Chris Brown: Fortune|work=[[The A.V. Club]]|publisher=The Onion, Inc|date=2012-07-10|accessdate=2012-09-10}}</ref> that incorporates elements of [[reggae]].<ref>{{cite news|last=Kot|first=Greg|url=http://articles.chicagotribune.com/2012-07-01/entertainment/chi-chris-brown-album-review-fortune-reviewed-20120701_1_album-review-chris-brown-rihanna|title=Chris Brown Album Review: Fortune|work=[[Chicago Tribune]]|publisher=[[Tribune Company]]|date=2012-07-01|accessdate=2012-09-10}}</ref> The song lasts for three minutes and 58 seconds.<ref>{{cite web|url=https://itunes.apple.com/us/album/fortune/id531720906|title=iTunes – Music – Fortune by Chris Brown|work=[[iTunes Store]] (United States)|publisher=[[Apple Inc|Apple]]|accessdate=2012-08-16}}</ref> Instrumentation consists of a [[Dubstep#Wobble bass|wobble bass]] and [[synth]]esizers.<ref>{{cite web|last=Johnston|first=Maura|url=http://www.rollingstone.com/music/albumreviews/fortune-20120716|title=Fortune <nowiki>|</nowiki> Album Review|work=[[Rolling Stone]]|publisher=[[Jann Wenner]]|date=2012-07-16|accessdate=2012-09-10}}</ref><ref>{{cite news|last=Farber|first=Jim|url=http://articles.nydailynews.com/2012-07-03/news/32527349_1_album-review-chris-brown-fortune|title=Album Review: Chris Brown, 'Fortune'|work=[[Daily News (New York)]]|publisher=Daily News, L.P|date=2012-07-03|accessdate=2012-09-10}}</ref> Melinda Newman of [[HitFix]] compared "Bassline" to the songs by [[Kesha]] and [[LMFAO]].<ref name="hitfix"/> Trent Fitzgerald of PopCrush noted that the lyrics are about Brown trying to "convince a hot girl he spots in the club to come back to his crib", in which he sings "Hey girl tell me what you talk / Pretty as a picture on the wall / Hey girl you can get it all / Cause I know you like the way the beat go".<ref name="popcrush">{{cite web|last=Fitzgerald|first=Trent|url=http://popcrush.com/chris-brown-fortune-album-review/|title=Chris Brown, 'Fortune' – Album Review|publisher=PopCrush|date=2012-07-03|accessdate=2012-09-10}}</ref> Brown also declares, "You heard about my image / But I could give a flying motherfuck who's offended".<ref name="ewreview">{{cite web|last=Anderson|first=Kyle|url=http://www.ew.com/ew/article/0,,20607481,00.html|title=Fortune Review|work=[[Entertainment Weekly]]|publisher=[[Time Inc]]|date=2012-06-29|accessdate=2012-09-10}}</ref> Hayley Avron of [[Contactmusic.com]] noted that a [[Robotic voice effects|robot voice]] joins Brown in the [[Hook (music)|hook]] "Girls like my bassline".<ref>{{cite web|last=Avron|first=Hayley|url=http://www.contactmusic.com/album-review/chris-brown-fortune|title=Chris Brown – Fortune Album Review|publisher=[[Contactmusic.com]]|date=2012-08-03|accessdate=2012-09-10}}</ref> Hazel Robinson of ''[[California Literary Review]]'' magazine noted that the word "bassline" is a [[metaphor]] for [[penis]].<ref name="clr">{{cite web|last=Robinson|first=Hazel|url=http://calitreview.com/28045|title=Album Review: Chris Brown's Fortune|work=[[California Literary Review]]|date=2012-07-06|accessdate=2012-07-31}}</ref>

==Reception==
[[File:Chris Brown 5, 2012.jpg|thumb|180px|right|One reviewer criticized the song as "a lazy attempt" from Brown.]]
"Bassline" garnered mixed to negative reviews from [[Music journalism|music critics]]. Sam Wilbur of [[AOL Radio]] viewed it as "the best example" of dubstep tracks on ''Fortune'', while Kyle Anderson of ''[[Entertainment Weekly]]'' noted it as one of the album's best tracks.<ref name="ewreview"/><ref>{{cite web|last=Wilbur|first=Sam|url=http://www.aolradioblog.com/album-review/chris-brown-fortune/|title=Chris Brown, 'Fortune' – Album Review|work=[[AOL Radio]]|publisher=[[AOL]]|date=2012-07-05|accessdate=2012-09-10}}</ref> Scott Kara of ''[[The New Zealand Herald]]'' called the song "irritating" and noted it as "blatant copycat stuff".<ref>{{cite news|last=Kara|first=Scott|url=http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=10818955|title=Album Review: Fortune – Chris Brown|work=[[The New Zealand Herald]]|publisher=[[APN News & Media]]|date=2012-07-12|accessdate=2012-09-09}}</ref> [[Digital Spy]]'s Lewis Corner felt that "Bassline" was "a lazy attempt" from Brown.<ref>{{cite web|last=Corner|first=Lewis|url=http://www.digitalspy.com.au/music/albumreviews/a390657/chris-brown-fortune-album-review.html|title=Chris Brown: 'Fortune' – Album Review|work=[[Digital Spy]]|publisher=[[Hearst Corporation|Hearst Magazines UK]]|date=2012-06-30|accessdate=2012-09-10}}</ref> Randall Roberts of ''[[Los Angeles Times]]'' stated that the worst part of the song is the hook.<ref>{{cite news|last=Roberts|first=Randall|url=http://articles.latimes.com/2012/jul/02/entertainment/la-et-0703-chris-brown-review-20120703|title=Review: Chris Brown's 'Fortune' is brash and commercial|work=[[Los Angeles Times]]|publisher=Tribune Company|date=2012-07-02|accessdate=2012-09-10}}</ref> Hazel Robinson of ''California Literary Review'' magazine was critical of the production and lyrics, labeling it as "bad" and "dodgy".<ref name="clr"/> [[Allmusic]]'s Andy Kellman noted that Brown "clearly feels more emboldened than ever" on "Bassline".<ref>{{cite web|last=Kellman|first=Andy|url=http://www.allmusic.com/album/fortune-mw0002329610|title=Fortune – Chris Brown|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2012-09-10}}</ref> Upon the release of ''Fortune'', due to digital sales, "Bassline" debuted on the [[UK R&B Chart]] at number 28 in the issue dated July 14, 2012.<ref name="ukr&b"/> It also debuted at number 122 on the [[UK Singles Chart]].<ref name="ukchart"/>

==Credits and personnel==
Credits adapted from the liner notes for ''Fortune''<ref name="booklet">{{cite AV media notes|title=Fortune|titlelink=Fortune (Chris Brown album)|others=[[Chris Brown (American entertainer)|Chris Brown]] |year=2012 |publisher=[[RCA Records]]}}</ref>

{{col-begin}}
{{col-2}}
*Chris Brown – lead vocals, [[songwriter]]
*Robert Calloway – songwriter
*Ronald "Flippa" Colson – songwriter
*Warren "Oak" Felder – songwriter
*Iain Findley – assistant recorder
*Trehy Harris – assistant mixer
{{col-2}}
*Dayvi Jae – [[Record producer|producer]]
*David Johnson – songwriter
*Jaycen Joshua – mixer
*Andrea Simms – songwriter
*Brian Springer – recorder
*Andrew "Pop" Wansel – songwriter, producer
{{col-end}}

==Charts==
{{Wikipedia books|Fortune}}

{| class="wikitable sortable plainrowheaders"
! scope="col"|Chart (2012)
! scope="col"|Peak<br/>position
|-
! scope="row"| [[UK R&B Chart|UK R&B]] ([[Official Charts Company]])<ref name="ukr&b">{{cite web|url=http://www.officialcharts.com/archive-chart/_/16/2012-07-14|title=Top 40 R&B Singles Archive 2012-07-14|work=[[UK R&B Chart]]|publisher=[[Official Charts Company]]|accessdate=2012-09-08}}</ref>
| style="text-align:center;"| 28 
|-
! scope="row"| [[UK Singles Chart|UK Singles]] ([[Official Charts Company]])<ref name="ukchart">{{cite web|url=http://www.zobbel.de/cluk/120714cluk.txt|title=UK Singles Chart / CLUK Update (14.07.2012 – Week 27)|work=zobbel.de|publisher=Tobias Zywietz|accessdate=2012-09-08}}</ref>
| style="text-align:center;"| 122
|-
|}

==References==
{{Reflist|2}}

==External links==
* {{MetroLyrics song|chris-brown|bassline}}<!-- Licensed lyrics provider -->

{{Chris Brown singles}}

[[Category:2012 songs]]
[[Category:Chris Brown songs]]
[[Category:Dubstep songs]]
[[Category:Electropop songs]]
[[Category:Songs about sexuality]]
[[Category:Songs written by Andrew Wansel]]
[[Category:Songs written by Chris Brown]]
[[Category:Songs written by Warren Felder]]