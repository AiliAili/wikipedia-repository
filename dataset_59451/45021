{{EngvarB|date=November 2013}}
{{Use dmy dates|date=November 2013}}
{{Infobox comedian
| name        = Shaun Micallef
| image       = Shaun Micallef in ABC Studio 31.jpg
| caption     = Shaun Micallef in July 2012
| birth_name  = Shaun Patrick Micallef
| birth_date  = {{birth date and age|1962|7|18|df=y}}
| birth_place = [[Adelaide|Adelaide, South Australia]], Australia
| education = [[University of Adelaide]]
| medium      = Television, radio, books, internet
| nationality = Australian
| active      = 1987–present
| genre       = [[Sketch comedy]], [[surrealism]], [[dada]]ism, [[absurdism]], [[political satire]]
| subject     = Humour
| influences  = [[The Goons]],<ref name="Age Interview" /> [[Peter Sellers]], [[Marx Brothers]],<ref>{{cite web |author=MacNaughton, Tanya | url=http://www.shaunmicallef.com/articles/xpress.html | title=Shaun Micallef |work=Xpress Online |date=16 February 2005 | accessdate=20 August 2008}}</ref> [[S. J. Perelman]], [[James Thurber]], [[Spike Milligan]],<ref name="comedy bites" /> [[Barry Humphries]], [[Frank Muir]],<ref name="full frontal">{{cite web |author=Dodds, Joy | url=http://www.shaunmicallef.com/articles/cityweekly.html | title=Full Frontal Shaun |work=City Weekly |date=30 September 2004 | accessdate=20 August 2008}}</ref> [[Monty Python]],<ref>{{cite web |author=Micallef, Shaun | url=http://www.shaunmicallef.com/articles/fanqa.html | title=Smowah's Exlusive Fan Q & A with Shaun Micallef |work=Shaun Micallef's Online World Around Him |year=2004 | accessdate=20 August 2008}}</ref> [[Woody Allen]]
| influenced  = [[Sammy J]]<ref>{{cite web |author=Davidson, Erin | url=http://www.thegroggysquirrel.com/articles/2006/06/18/nuts-about-sammy-j/ | title=Nuts About Sammy J |work=The Groggy Squirrel |date=18 June 2007 | accessdate=19 August 2008}}</ref>
| spouse      = {{marriage|Leandra|1989}}
| children    = 3 
| notable_work=''[[The Micallef P(r)ogram(me)]]''<br />''[[Welcher & Welcher]]''<br />''[[Micallef Tonight]]''<br />''[[Newstopia]]''<br />''[[Thank God You're Here]]'' (2006–2007)<br />''[[Talkin' 'Bout Your Generation]]''<br />''[[Shaun Micallef's Mad as Hell]]''
| signature   =
| website     =
| footnotes   =
}}
'''Shaun Patrick Micallef''' (born 18 July 1962) is an Australian actor, comedian and writer. After ten years of working in insurance law as a solicitor in [[Adelaide]], Micallef moved to [[Melbourne]] to pursue a full-time comedy career in 1993.  He first gained recognition as a cast member of the [[sketch comedy]] show ''[[Full Frontal (Australian TV series)|Full Frontal]]'', which in turn led to a number of television roles including his own sketch show, ''[[The Micallef P(r)ogram(me)]]'', the sitcom ''[[Welcher & Welcher]]'' and the variety show ''[[Micallef Tonight]]''.  He also fronted the satirical news comedy series ''[[Newstopia]]'' on [[Special Broadcasting Service|SBS]], hosted the game show ''[[Talkin' 'Bout Your Generation]]'' on [[Network Ten]] for four seasons, and ''[[Shaun Micallef's Mad as Hell]]'' on the [[ABC Television|ABC]]. He also co-created and starred in ''[[Mr & Mrs Murder]]'' on Network Ten. 
 
In addition to his television work Micallef has appeared on stage, most notably in the Australian production of ''[[Boeing Boeing (play)|Boeing Boeing]]'' and on radio as the co-host of Melbourne station [[Vega FM|Vega 91.5 FM]]'s morning program.  He is also a published author with three books: ''[[Smithereens (book)|Smithereens]]'' (2004, re-released 2011), ''[[Preincarnate (book)|Preincarnate]]'' (2010) and ''The President's Desk''  (2014).

==Early life and education==
Micallef was born in [[Adelaide|Adelaide, South Australia]], and is of [[Maltese people|Maltese]] and Irish descent.<ref name="Age Interview">{{cite web |author=Beck, Chris | url=http://www.theage.com.au/news/tv--radio/the-interview/2005/11/08/1131407640193.html?oneclick=true | title=The Interview |work=The Age |date=10 November 2005 | accessdate=20 August 2008}}</ref> His father worked for a company that sold parts for Volvos and his mother was employed at the [[Adelaide Bank]].<ref name="anything">{{cite web |author=Wilkie, Meredith | url=http://www.shaunmicallef.com/articles/anything.html | title=Anything for a laugh |work=The Sun-Herald |date=4 February 2001 | accessdate=20 August 2008}}</ref>

As a child, Micallef lived in [[Clovelly Park, South Australia|Clovelly Park]] and attended St Bernadette's School in [[St. Marys, South Australia|St Marys]] then St Joseph's Catholic School in [[Mitchell Park, South Australia|Mitchell Park]] (now [[Sacred Heart College Middle School]]) before moving on to [[Sacred Heart College Senior, Adelaide|Sacred Heart Senior College]] where he was the College Captain.<ref name="messenger">{{cite web |author=Goldsmith, David | url=http://guardian-messenger.whereilive.com.au/news/story/illustrious-company-for-sacred-heart-old-scholars/ | title=Illustrious company for Sacred Heart old scholars |work=Guardian Messenger |date=16 September 2009 | accessdate=20 October 2010}}</ref>

Micallef studied law at the [[University of Adelaide]], where he was frequently involved in comedy revues, often involving [[Francis Greenslade]] and [[Gary McCaffrie]], with whom he continues to work.<ref>{{cite web |author=Rafalowicz, Alex | url=http://www.shaunmicallef.com/articles/flinders.html | title=Interview: Shaun Micallef |work=Empire Times (Flinders University) |date=February 2005 | accessdate=20 August 2008}}</ref>

==Career==

===Early theatre===
In 1972, having three younger sisters taking ballet classes, ten-year-old Micallef was often asked to help out when a dance routine required a boy. The following year he auditioned for the Bunyip Children's Theatre and over the next four years participated in plays that they performed in the Scott Theatre during school holidays. In 1976 he doubled for [[Humphrey B. Bear]] for personal appearances.<ref>{{cite web|title=Spicks and Specks, Episode Twenty Three|url=http://www.abc.net.au/tv/spicksandspecks/txt/s2287764.htm|publisher=Australian Broadcasting Corporation|accessdate=9 February 2014|quote=He recently admitted on a raido that he played the popular children's character Humphrey B. Bear for a period of three weeks during the 1970s.}}</ref>

===Legal career===
Micallef was a practising solicitor for ten years in the field of insurance law before making the decision to move to Melbourne and pursue a full-time career in comedy in 1993.<ref name="anything" />

He relates the story that, while working as a solicitor, he talked so much about making a career change and becoming a comedian that his wife Leandra gave him an ultimatum: she marked a date on a calendar and told him to quit his job and become a comedian by that date or never talk about it again.<ref>{{cite news|last=Lallo|first=Michael|title=His current affair|url=http://www.theage.com.au/news/tv--radio/btvb-micallef-rewrites-headlines-with-comedy-inewstopiai/2007/10/02/1191091106590.html?page=fullpage|accessdate=9 February 2014|newspaper=The Age|date=4 October 2007|quote=Fed up with his all-talk, no-action attitude, his wife Leandra issued an ultimatum. "She put an X on the calendar and said, 'You have to have done something about it by this date or you have to shut up.'"}}</ref>

===Television and film===
Following early TV appearances on ''[[Theatre Sports]]'' (1987) and ''[[The Big Gig]]'' (1989), in early 1993 Micallef was offered a job writing for the ''[[Jimeoin]]'' show which was soon followed by an offer to also write for the sketch comedy show ''[[Full Frontal (Australian TV series)|Full Frontal]]'' where six months later he took on the role as co-producer with Gary McCaffrie. In 1994, Micallef became a full-time cast member of ''Full Frontal'', where he became well known for characters such as [[Milo Kerrigan]], [[Nobby Doldrums]] and a send-up of Italian [[male model]] [[Fabio Lanzoni|Fabio]].  Micallef recalls that the show was a good introduction to television comedy because, with an ensemble cast, its success did not hinge on his performance and he had more freedom to make and learn from mistakes.  However, he was frustrated with the lack of control he had over his work in the series as well as the repetition of characters and gags.<ref name="anything" />

Micallef's role on ''Full Frontal'' led to a 1996 special ''[[Shaun Micallef's World Around Him]]'' and three seasons of the two-time [[Logie Award]]-winning [[ABC Television|ABC]] series ''[[The Micallef Program]]'' (1998–2001), which he co-wrote and produced with long-time writing partner [[Gary McCaffrie]].<ref>{{cite web |author=Martin, Simon | url=http://www.shaunmicallef.com/articles/mercury.html | title=Shaun Micallef |work=The Mercury |date=October 2004 | accessdate=20 August 2008}}</ref> Since the series' end he has created and starred in two short-lived television series, the sitcom ''[[Welcher & Welcher]]'' (2003) and the variety show ''[[Micallef Tonight]]'' (2003),<ref name=record>{{cite web |author=Witham, Katrina | url=http://www.shaunmicallef.com/articles/couriermail.html | title=Micallef on the Record |work=The Courier Mail |date=9 September 2004 | accessdate=20 August 2008}}</ref> and devised a series of telemovies, ''[[BlackJack (telemovie)|BlackJack]]'' (2003–present).<ref>{{cite web |author=Courtis, Brian | url=http://www.theage.com.au/news/tv--radio/grumpy-old-man/2005/09/08/1125772628702.html | title=Grumpy old man |work=The Age |date=11 September 2005 | accessdate=20 August 2008}}</ref>

Micallef has also had acting roles in the television series ''[[SeaChange]]'' (2000), ''Through My Eyes'' (2004) and ''[[Offspring (TV series)|Offspring]]'' (2010) as well as supporting roles in the films ''[[Bad Eggs]]'' (2003), ''[[The Honourable Wally Norman]]'' (2003), ''[[The Extra]]'' (2005), ''[[Aquamarine (film)|Aquamarine]]'' (2006) and ''[[The King (2007 film)|The King]]'' (2007).  In 2006, he was a recurring guest on the [[Network Ten]] [[Improvisational theatre]] show ''[[Thank God You're Here]]''.

In 2007, along with partners McCaffrie and Michael Ward, Micallef developed the satirical comedy program ''[[Newstopia]]'', which he hosted. The show began airing on 10 October 2007 on SBS and in August 2008 it was announced that a third series had been commissioned.<ref>{{cite web |author=Body, Michael | url=http://www.theaustralian.news.com.au/story/0,25197,24139143-7582,00.html | title=Fremantle gives old favourites a new lease of life |work=The Australian |date=7 August 2008 | accessdate=20 August 2008}}</ref> In 2009, Micallef joined the Ten Network and hosted ''[[Talkin' 'Bout Your Generation]]'', which aired for four seasons.

In 2012, Micallef began hosting [[ABC1]]'s ''[[Shaun Micallef's Mad as Hell]]''.<ref>[http://www.abc.net.au/tv/madashell/ Shaun Micallef's Mad As Hell] – Australian Broadcasting Corporation – Retrieved 1 June 2012.</ref>

He co-created ''[[Mr & Mrs Murder]]'', a crime comedy television series for Channel Ten which aired in 2013, and starred in the lead role of Charlie Buchanan alongside [[Kat Stewart]]. Also that year, Micallef signed on to voice the artificially intelligent robot REEF in the Australian feature length science fiction film ''Arrowhead'' (2014).

===Other work===
In September 2005, Micallef began hosting the breakfast show "Shaun, Beverley and Denise" on Melbourne radio station Vega 91.5 FM with comedian [[Denise Scott]] and television presenter [[Beverley O'Connor]].  In July 2006, comedian [[Dave O'Neil]] took over as host and the show was renamed "Dave and Denise with Shaun Micallef".  Micallef left the network on 23 November 2007.<ref>{{cite web |author=McPhee, Ross | url=http://www.shaunmicallef.com/articles/earlylight.html | title=By Shaun's Early Light |work=Herald Sun |date=28 September 2005 | accessdate=20 August 2008}}</ref>

Micallef released a book, ''Smithereens'', which was published in 2004 and contains a collection of prose, poetry and plays.  He describes it as a collection of "all sorts of bits and pieces I have written".<ref name="comedy bites">{{cite web |author=Brookfield, Joanne | url=http://www.shaunmicallef.com/articles/bigissue.html | title=Comedy Bites |work=The Big Issue No. 214 |date=18 October 2004 | accessdate=20 August 2008}}</ref><ref name="record" />  His second book, a novella titled ''[[Preincarnate (book)|Preincarnate]]'', was released in 2010.

In October 2014, Micallef released his third book, ''The President's Desk: An Alt-History of the United States''; a semi-fictitious history, told from the perspective of the [[Resolute desk]].<ref>{{cite web|title=The President's Desk - Shaun Micallef|url=http://www.hardiegrant.co.uk/books/the-presidents-desk|website=www.hardiegrant.co.uk/books|accessdate=3 October 2014}}</ref>

===Mainstream popularity===
With Micallef's 2009 move to the Ten Network's ''Talkin' 'Bout Your Generation'', a sudden and unprecedented rise in his popularity within the Australian mainstream has been observed.<ref>{{cite web |author=Hassall, Greg | url=http://www.smh.com.au/news/entertainment/tv--radio/talking-bout-regeneration/2009/06/22/1245522773961.html | title=Talking 'bout regeneration |work=The Sydney Morning Herald |date=22 July 2009 | accessdate=23 September 2009}}</ref> His contract with the program marks his first long-term position in a commercial network and has raised his profile in commercial television. The extent of this has been demonstrated with celebrity appearances on [[Rove McManus]]'s ''[[Rove (TV series)|Rove]]'',<ref>http://www.rovedaily.com.au/show-info-the-show-guest-shaun-micallef.htm</ref> ''[[Are You Smarter Than a 5th Grader? (Australian game show)|Are You Smarter Than a 5th Grader?]]''<ref>http://www.imdb.com/name/nm0584017/</ref> and on ''[[9am with David & Kim]]''.<ref name="9am.ten.com.au">http://9am.ten.com.au/9am-05-05-2009.htm</ref>  In 2010, he was voted most popular presenter at the annual [[Logie Award]]s.<ref name="9am.ten.com.au"/>

==Personal life==
Micallef currently lives in [[Williamstown, Victoria]], with his wife Leandra, whom he married in 1988, and their three sons.<ref name="Age Interview" /><ref>{{cite web |author=McCulloch, Janelle | url=http://www.shaunmicallef.com/articles/city.html | title=Interview: Shaun Micallef |work=My City |date=August 2003 | accessdate=20 August 2008 |archiveurl=https://web.archive.org/web/20120330212727/http://www.shaunmicallef.com/articles/city.html |archivedate=30 March 2012 }}</ref>

==List of works==

===Films===
*''Arrowhead'' (2014) – REEF (voice)
*''[[The Cup (2011 film)|The Cup]]'' (2011) – [[Lee Freedman]]
*''[[The King (2007 film)|The King]]'' (2007) – Colin Bednall
*''[[Aquamarine (film)|Aquamarine]]'' (2006) – Storm Banks
*''[[The Extra]]'' (2005) – Paul Ridley
*''[[Bad Eggs]]'' (2003) – Premier Cray
*''[[The Honourable Wally Norman]]'' (2003) – Ken Oats
* The 13th House (2003) – Sir
*''Silly Billy Don't Touch That!'' (unknown)

===Television===
*''[[The Ex-PM]]'' (2015) - Andrew Dugdale
*''[[Shaun Micallef's Stairway to Heaven]]'' (2014) - himself
*''[[Live On Bowen]]'' (2014) - himself (guest)
*''[[It's a Date (TV series)|It's a Date]]'' (2013-) – Roland (Season 2, Episode 2)
*''[[Danger 5]]'' (2012, 2014) - Principal
*''[[Mr & Mrs Murder]]'' (2013) – Charlie Buchanan
*''[[Shaun Micallef's Mad as Hell]]'' (2012–) – host
*''Mollusks'' (2011) – Easty (voice)
*''[[The Bazura Project|The Bazura Project's Guide To Sinema]]'' (2011) – MK-Ultra (voice)
*''[[Laid (TV series)|Laid]]'' (2011–2012) – G-Bomb
*''[[A Quiet Word With&nbsp;...]]'' (2011) – himself (guest)
*''[[Offspring (TV series)|Offspring]]'' (2010) – Lachlan
*''Shaun Micallef's New Year's Rave'' – host
*''[[Are You Smarter Than a 5th Grader? (Australian game show)|Are You Smarter Than a 5th Grader?]]''
*''[[The Chaser's War on Everything]]'' (2009) – himself (guest in the Rudd song sketch)
*''[[Rove (TV series)|Rove]]'' (2009) – himself (guest)
*''Melbourne International Comedy Festival'' (2009) – host
*''[[Talkin' 'Bout Your Generation]]'' (2009–2012) – host
*''[[Good News Week]]'' (2009) – himself (guest)
*''[[Spicks and Specks (TV series)|Spicks and Specks]]'' (2008) – himself (guest)
*''[[Newstopia]]'' (2007–2008) – himself
*''[[Dogstar (TV series)|Dogstar]]'' (2007) – narrator
*''[[Thank God You're Here]]'' (2006–2007) – various (semi-regular guest)
*''[[The Shambles (community television program)|The Shambles]]'' (2006) – (guest cameo)
*''[[Through My Eyes (film)|Through My Eyes]]'' (2005) – Jack Winneke
*''[[BlackJack (telemovie)|BlackJack]]'' (2003–2007) (writer)
*''[[Micallef Tonight]]'' (2003) – himself (also writer)
*''[[Welcher & Welcher]]'' (2003) – Quentin Welcher (also writer and producer)
*''[[SeaChange]]'' (2000) – Warwick Munro
*''[[The Micallef Program]]'' (1998–2001) – various (also writer and producer)
*''[[Under Melbourne Tonight]]'' (1996) -  Guest<ref>http://web.aanet.com.au/~vfok/umt/episodes/list96.htm</ref>
*''[[Shaun Micallef's World Around Him]]'' (1996) – various (also writer and producer)
*''[[The Glynn Nicholas Show]]'' (1996) (writer)
*''[[Full Frontal (Australian TV series)|Full Frontal]]'' (1994–1997) – various (also writer and producer)
*''[[Jimeoin (TV series)|Jimeoin]]'' (1994) – various (also writer)
*''[[The Big Gig]]'' (1989) (writer)

===Theatre===
*''[[Boeing-Boeing (play)|Boeing Boeing]]'' (2008) – Bernard
*''[[Good Evening - Sketches from Dudley Moore and Peter Cook]]'' (2010)
*''[[The Odd Couple]]'' (2016)

===Radio shows===
*''[[Dave and Denise with Shaun Micallef]]'' on [[Vega FM|Vega 91.5 FM]] (2005–2007)
*''[[Hamish & Andy]]'' on [[Fox FM (Melbourne)|Fox FM]] (a guest twice)
*''[[Pete and Myf]]'' (with ''[[Newstopia]]'' writer [[Richard Marsland]] on [[Triple M]] (guest two times)
*''[[Get This]]'' appeared on the final show in the first hour
*''The Comedy Crystal Set'' – Adelaide University Radio, Co-host

===Books===
*''The President's Desk'' (2014)
*''[[Preincarnate (book)|Preincarnate]]'' (2010)
*''[[Smithereens (book)|Smithereens]]'' (2004)

===Music===
* His Generation (2009)

==Awards==
*2012: Named Comedian of the Year at the The GQ Men of the Year Awards, which celebrates the country's finest actors, comedians, sportsmen, musicians and creative visionaries.<ref>{{cite web | author=Joanne Hawkins |  url=http://www.gq.com.au/men+of+the+year/2012+winners/shaun+micallef,20839 | title=Men of the Year 2012 Winners | accessdate= 14 November 2012}}</ref>
*2008: Won [[ARIA Music Awards|Aria Award]] [[ARIA Music Awards of 2008|Best Comedy Release]] – ''The Expurgated Micallef Tonight: The Very Best of Shaun Micallef's Short-Lived but Brilliant Tonight Show''<ref>{{cite web|author=Cameron Adams & Jane Metlikovec |url=http://www.news.com.au/heraldsun/story/0,21985,24521049-2902,00.html |title=Dandenong teenager Gabriella Cilmi: she'll be sweet |work=The Age |date=20 October 2008 |accessdate=20 October 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20081021132227/http://www.news.com.au/heraldsun/story/0,21985,24521049-2902,00.html |archivedate=21 October 2008 }}</ref>
*2010: Won Most Popular Presenter and nominated for the [[Gold Logie]] at the [[Logie Awards]] for his role on ''Talkin' 'Bout Your Generation''.

==References==
{{Reflist|2}}

==External links==
{{Wikiquote}}
*[http://www.shaunmicallefonline.com Unofficial Shaun Micallef fansite]
*[http://wayback.archive.org/web/*/http://www.shaunmicallef.com Original unofficial Shaun Micallef fansite (archived)]
* {{IMDb name|584017}}

{{Portal|Biography}}

{{s-start}}
{{s-ach}}
{{s-bef|before=[[Andrew Denton]]}}
{{s-ttl|title=Host of the [[Logie Awards]]|years=2001}}
{{s-aft|after=[[Wendy Harmer]]}}
{{s-break}}
{{s-bef|before=[[Rove McManus]]}}
{{s-ttl|title=[[Logie Award]]<br />[[Logie Award for Most Popular TV Presenter|Most Popular TV Presenter]]|years=2010<br />for ''[[Talkin' 'Bout Your Generation]]''}}
{{s-aft|after=[[Karl Stefanovic]]}}
{{s-end}}

{{AACTAAward ComedyPerformance}}
{{Logie Awards hosts}}

{{Authority control}}

{{DEFAULTSORT:Micallef, Shaun}}
[[Category:1962 births]]
[[Category:ARIA Award winners]]
[[Category:Australian male comedians]]
[[Category:Australian male film actors]]
[[Category:Australian people of Irish descent]]
[[Category:Australian people of Maltese descent]]
[[Category:Australian solicitors]]
[[Category:Australian television presenters]]
[[Category:Comedians from Melbourne]]
[[Category:Lawyers from Adelaide]]
[[Category:Living people]]
[[Category:People from Williamstown, Victoria]]
[[Category:Talkin' 'Bout Your Generation]]
[[Category:University of Adelaide Law School alumni]]