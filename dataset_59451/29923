{{Infobox horse
| name          = Banker horse
| image         = SpanishMustangsOfCorolla.jpg
| image_caption = Horses near Corolla
| features      = Small, compact conformation
| altname       = 
| nickname      = 
| country       = [[United States]]
| group1        = 
| std1          = 
}}

The '''Banker horse''' is a [[horse breed|breed]] of [[feral horse]] (''Equus ferus caballus'') living on [[barrier islands]] in [[North Carolina]]'s [[Outer Banks]]. It is small, hardy, and has a docile temperament. Descended from domesticated [[Iberian horse|Spanish horses]] and possibly brought to the Americas in the 16th century, the ancestral [[foundation bloodstock]] may have become feral after surviving shipwrecks or being abandoned on the islands by one of the exploratory expeditions led by [[Lucas Vázquez de Ayllón]] or Sir [[Richard Grenville]]. Populations are found on [[Ocracoke, North Carolina|Ocracoke Island]], [[Shackleford Banks]], [[Bodie Island|Currituck Banks]], and in the [[Rachel Carson]] Estuarine Sanctuary.

Although they can trample plants and ground-nesting animals and are not considered to be [[Indigenous (ecology)|indigenous]] to the islands, Bankers are allowed to remain due to their historical significance. They survive by grazing on marsh grasses, which supply them with water as well as food, supplemented by temporary freshwater pools.

To prevent overpopulation and inbreeding, and to protect their habitat from being overgrazed, the horses are managed by the [[National Park Service]], the state of North Carolina, and several private organizations. The horses are monitored for diseases, such as [[equine infectious anemia]], an outbreak of which was discovered and subsequently eliminated on Shackleford in 1996. They are safeguarded from traffic on [[North Carolina Highway 12]]. Island populations are limited by adoptions and by birth control. Bankers taken from the wild and trained have been used for [[trail riding]], [[driving (horse)|driving]], and occasionally for mounted patrols.

== Characteristics ==
[[File:NCbarrierislandsmap.png|thumb|A map showing herd locations]]

The typical Banker is relatively small, standing between {{hands|13.0|and|14.3}} high at the [[withers]]{{sfn|Hendricks|1995|p=63}} and weighing {{convert|800|to|1000|lb|kg}}.<ref name="GaitedHorse" /> The forehead is broad and the facial profile tends to be straight or slightly convex. The chest is deep and narrow and the [[back (horse)|back]] is short with a sloped [[Rump (animal)|croup]] and low-set tail. Legs have an oval-shaped [[Equine forelimb anatomy|cannon bone]],<ref name="Sponeberg" /> a trait considered indicative of "strong bone" or soundness.<ref name="haflingerhorse" />  The callousities known as [[Chestnut (horse anatomy)|chestnuts]] are small, on some so tiny that they are barely detectable.  Most Bankers have no chestnuts on the hind legs.<ref name="HOAInspections" /> The coat can be any [[equine coat color|color]] but is most often [[bay (horse)|brown]], [[bay (horse)|bay]], [[dun gene|dun]], or [[Chestnut (coat)|chestnut]].<ref name="AmericanBreeds" /> Bankers have long-strided [[horse gait|gaits]] and many are able to [[Pacing (horse gait)|pace]]  and [[Ambling|amble]].<ref name="Sponeberg" /> They are [[easy keeper]]s<ref name="HOAInspections" /> and are hardy, friendly, and docile.{{sfn|Hendricks|1995|pp=64–65}}

Several of the Bankers' characteristics indicate that they share ancestry with other [[Colonial Spanish horse]] breeds. The presence of the [[genetic marker]] "Q-ac" suggests that the horses share common ancestry with two other breeds of [[Iberian horse|Spanish descent]], the Pryor Mountain [[Mustang (horse)|Mustang]] and [[Paso Fino]].  These breeds diverged from one another 400&nbsp;years ago.<ref name="shacklefordhorses" /> The breed shares skeletal traits of other Colonial Spanish horses: the wings of the [[atlas (anatomy)|atlas]] are lobed, rather than semicircular; and they are short-backed, with some individuals possessing five instead of six [[lumbar vertebrae]]. No changes in function result from these spinal differences. The convex facial profile common to the breed also indicates Spanish ancestry.<ref name="Sponeberg" />

== Breed history ==
Since they are free-roaming, Bankers are often referred to as [[Wild horse|"wild" horses]]; however, because they descend from domesticated ancestors, they are [[feral horse]]s.{{sfn|Prioli|2007|pp=12–13}} It is thought that the Bankers arrived on the [[barrier islands]] during the 16th century. Several hypotheses have been advanced to explain the horses' origins, but none have yet been fully verified.

[[File:NOAA- Outer Banks.jpg|thumb|left|upright|Aerial view of a barrier island in the North Carolina Outer Banks]]

One theory is that ancestors of the Banker swam ashore from wrecked Spanish [[galleon]]s. Ships returning to Spain from the Americas often took advantage of both the [[Gulf Stream]] and continental [[trade wind]]s, on a route that brought them within {{convert|20|mi|km}} of the Outer Banks. Hidden shoals claimed many victims, and earned this region the name of "[[Graveyard of the Atlantic]]". At least eight shipwrecks discovered in the area are of Spanish origin, dating between 1528 and 1564. These ships sank close enough to land for the horses to have made the shores. Alternatively, during hazardous weather, ships may have taken refuge close to shore, where the horses may have been turned loose. However, the presence of horses on Spanish treasure ships has not been confirmed—cargo space was primarily intended for transporting riches such as gold and silver.{{sfn|Prioli|2007|p=21}}

Another conjecture is that the breed is descended from the 89&nbsp;horses brought to the islands in 1526 by Spanish explorer [[Lucas Vázquez de Ayllón]]. His attempted colonization of [[San Miguel de Gualdape]] (near the [[Santee River]] in [[South Carolina]]) failed, forcing the colonists to move, possibly to North Carolina. Vázquez de Ayllón, and about 450 of the original 600&nbsp;colonists subsequently died as a result of desertion, disease, and an early frost. Lacking effective leadership, the new settlement lasted for only two months; the survivors abandoned the colony and fled to [[Hispaniola]], leaving their horses behind.{{sfn|Prioli|2007|pp=16–20}}

A similar theory is that Sir [[Richard Grenville]] brought horses to the islands in 1585, during an attempt to establish an English naval base. All five of the expedition's vessels ran aground at Wococon (present-day [[Ocracoke, North Carolina|Ocracoke]]).{{sfn|Prioli|2007|pp=25–27}} Documents indicate that the ships carried various types of livestock obtained through trade in Hispaniola, including "mares, kyne [cattle], buls, goates, swine [and] sheep."{{sfn|Quinn|1955|p=187}} While the smaller vessels were easily refloated, one of Grenville's larger ships, the ''Tiger'', was nearly destroyed. Scholars believe that as the crew attempted to lighten the ship, they either unloaded the horses or forced them overboard, letting them swim to shore. In a letter to Sir [[Francis Walsingham]] that same year, Grenville suggested that livestock survived on the island after the grounding of his ships.{{sfn|Prioli|2007|pp=25–27}}

== Life on the barrier islands ==
[[File:Shackleford Horse Digging Well.jpg|thumb|Drinking from a horse-dug water hole on Shackleford Banks]]

About 400&nbsp;Bankers inhabit the long, narrow barrier islands of North Carolina's Outer Banks.<ref name="GaitedHorse" /> These islands are offshore sediment deposits separated from the mainland by a body of water such as an estuary<ref name="NOAA" /> or sound.{{sfn|Hendricks|1995|p=63}} The islands can be up to {{convert|30|mi|km}} from the shore; most are less than one&nbsp;mile (1.6&nbsp;km) wide. Vegetation is sparse and consists mainly of coarse grasses and a few [[Stunt (botany)|stunted]] trees.{{sfn|Blythe|Egeblad|1983|pp=63–72}} Each island in the chain is separated from the next by a tidal [[inlet]].<ref name="NOAA" />

The Bankers' small stature can be attributed, in part, to limited nutrients in their diet.{{sfn|Prioli|2007|p=12}} They graze mostly on ''[[Spartina]]'' grasses  but will feed on other plants such as bulrush (''[[Typha latifolia]]''), [[sea oats]],{{sfn|Rheinhardt|Rheinhardt|2004|pp=253–258}} and even [[poison ivy]].{{sfn|Harrison|2003|pp=211–213}} Horses living closer to human habitation, such as those on [[Bodie Island|Currituck Banks]], have sometimes grazed on residential lawns and landscaping.{{sfn|Rheinhardt|Rheinhardt|2004|pp=253–258}} Domesticated Bankers raised on manufactured horse feed from an early age tend to exhibit slightly larger frames.{{sfn|Prioli|2007|p=12}}

Water is a limiting resource for Bankers, as the islands are surrounded by [[seawater|salt water]] and have no [[freshwater]] springs or permanent ponds.{{sfn|Blythe|Egeblad|1983|pp=63–72}}  The horses are dependent on [[ephemeral]] pools of rainwater and moisture in the vegetation they consume.{{sfn|Rheinhardt|Rheinhardt|2004|pp=253–258}} Bankers will dig shallow holes, ranging from {{convert|2.5|to|4|ft|m}} in depth, to reach fresh groundwater.{{sfn|Blythe|Egeblad|1983|pp=63–72}} Occasionally, they may resort to drinking seawater. This gives them a bloated appearance, a consequence of water retention caused by the body's effort to maintain [[osmoregulation|osmotic balance]].{{sfn|Dohner|2001|pp=400–401}}

== Land use controversies ==
The [[National Park Service]] (NPS) is concerned about the impact of Bankers on the environmental health of North Carolina's barrier islands. Initially, the NPS believed that the non-native Bankers would completely consume the ''[[Spartina alterniflora]]'' grasses and the maritime forests, as both were thought to be essential to their survival.{{sfn|Dohner|2001|pp=400–401}}  Research in 1987 provided information on the horses' diet that suggested otherwise. Half of their diet consisted of ''Spartina'', while only 4% of their nutrients came from the maritime forest. The study concluded that sufficient nutrients were replenished with each ocean tide to prevent a decline in vegetative growth from overgrazing.{{sfn|Wood|Mengak|Murphy|2004|pp=236–244}} A 2004 study declared that the greatest impact on plant life was not from grazing but from the damage plants sustained when trampled by the horses' [[horse hoof|hooves]].{{sfn|Rheinhardt|Rheinhardt|2004|pp=253–258}} Bankers pose a threat to ground-nesting animals such as [[sea turtle]]s and [[wader|shorebird]]s. Feral horses interrupt nesting activities<ref name="Laliberté" /> and can crush the young.{{sfn|Dohner|2001|pp=400–401}}

== Management and adoption ==
As the Bankers are seen as a part of North Carolina's coastal heritage, they have been allowed to remain on the barrier islands.{{sfn|Dutson|2005|pp=323–325}} To cope with the expanding population, prevent [[inbreeding]] and attempt to minimize environmental damage, several organizations partner in managing the herds.

=== Ocracoke ===
[[File:Ocracoke ponies 01.jpg|thumb|A Banker horse on [[Ocracoke, North Carolina|Ocracoke Island]]]]

Since 1959, Bankers on [[Ocracoke, North Carolina|Ocracoke Island]] have been confined to fenced areas of approximately {{convert|180|acre|km2 sqmi|2}}. The areas protect the horses from the traffic of [[North Carolina Highway 12]], as well as safeguarding the island from overgrazing. The NPS, the authority managing the Ocracoke herd, supplements the horses' diet with additional hay and grain.<ref name="NPSCH" /> In 2006, as a precaution against inbreeding, two [[Colt (horse)|colts]] from the Shackleford herd were transported to Ocracoke.{{sfn|Prioli|2007|p=77}}

=== Shackleford ===
[[Act of Congress|Public Law]] 105-229, commonly referred to as the [[Shackleford Banks]] Wild Horses Protection Act, states that the Bankers on Shackleford Island are to be jointly managed by the National Park Service and another qualified nonprofit entity (currently the Foundation for Shackleford Horses). The herd is limited to 120–130&nbsp;horses. Population management is achieved through adoption and by administering the contraceptive vaccine ''[[Porcine zona pellucida]]'' (PZP) to individual mares via dart. The island's horse population is monitored by [[freeze branding]] numbers onto each animal's left hindquarter. The identification of individuals allows the National Park Service to ensure correct gender ratios and to select which mares to inject with PZP.

Since 2000, adoptions of Bankers from Shackleford have been managed by the Foundation for Shackleford Horses. As of 2007, 56&nbsp;horses had found new homes, 10 resided with another herd on [[Cedar Island, North Carolina|Cedar Island]], and two had been moved to the Ocracoke herd.{{sfn|Prioli|2007|pp=65–83}}

[[File:Branded Shackleford Pony.jpg|thumb|left|A freeze branded mare on Shackleford]]

On November 12, 1996, the Shackleford horses were rounded up by the North Carolina Department of Agriculture's Veterinary Division and tested for [[equine infectious anemia]] (EIA). EIA is a potentially lethal disease, a [[lentivirus]] transmitted by bodily fluids and insects. Seventy-six of the 184&nbsp;captured horses tested positive. Those that tested negative were allowed to remain on the island and those with the disease were transported to a temporary quarantine facility. Finding a permanent, isolated area for such a large number of Bankers was a challenging task for the Foundation; eight days later the state declared all proposed locations for the herd unsuitable. It ordered the euthanization of the 76&nbsp;infected horses.  Two more horses died in the process—one that was fatally injured during the roundup, and an uninfected [[foal]] that slipped into the quarantined herd to be with its mother.{{sfn|Prioli|2007|pp=61–63}}
{{clear}}

=== Currituck Banks ===
As a consequence of development in [[Corolla, North Carolina|Corolla]] and [[Sandbridge, Virginia Beach, Virginia|Sandbridge]] during the 1980s, horses on [[Bodie Island|Currituck Banks]] came into contact with humans more frequently.<ref>{{cite web |url=http://articles.dailypress.com/2003-01-07/news/0301070094_1_defense-of-wild-horses-sandbridge-fence |title=Sandbridge Fences, Daily Press January 2003}}</ref> By 1989, eleven Bankers had been killed by cars on the newly constructed Highway 12.<ref name="ncbeaches" /> That same year, the Corolla Wild Horse Fund, a nonprofit organization, was created to protect the horses from human interference. As a result of its efforts, the remainder of the herd was moved to a more remote part of Currituck Banks,<ref name="corollawildhorses home" /> where they were fenced into {{convert|1800|acre|km2 sqmi|2}} of combined federal and privately donated land between Corolla and the [[Virginia]]/North Carolina line. Corolla commissioners declared the site a horse sanctuary.{{sfn|Dohner|2001|pp=400–401}} The population is now  managed by adopting out [[yearling (horse)|yearling]]s, both [[filly|fillies]] and [[gelding|gelded]] colts.<ref name="corollawildhorses adoption" /> Conflicts over the preservation of the horses continued into 2012.<ref name="NYTimes 2012-05-07" />  In 2013, legislation was introduced to help preserve the herd on Currituck.<ref>{{cite web|last=Raia |first=Pat |url=http://www.thehorse.com/articles/32003/corolla-wild-horse-bill-gets-house-nod?utm_source=Newsletter&utm_medium=welfare-industry&utm_campaign=06-06-2013 |title=Corolla Wild Horse Bill Gets House Nod |publisher=TheHorse.com |accessdate=2013-06-06}}</ref>

=== Rachel Carson Site, North Carolina National Estuarine Research Reserve ===
A herd lives on the [[Rachel Carson]] component of the North Carolina National Estuarine Research Reserve, a series of five small islands and several salt marshes.<ref name="nccoastalreserve about" /> There were no horses at the Sanctuary until the 1940s. It is unclear whether the Bankers swam over from nearby Shackleford{{sfn|Hendricks|1995|p=65}} or were left by residents who had used the islands to graze livestock. They are owned and managed by the state of North Carolina and regarded as a cultural resource.

No management action was taken until the late 1980s and early 1990s, when after years of flourishing population, the island's [[carrying capacity]] was exceeded. Malnourishment caused by overcrowding resulted in the deaths of several horses; the reserve's staff instituted a birth control program to restrict the herd to about 40&nbsp;animals.<ref name="NCreserve" />

== Uses ==
Adopted Bankers are often used for [[pleasure riding]] and [[driving (horse)|driving]].{{sfn|Dutson|2005|pp=323–325}} As they have a calm disposition,{{sfn|Hendricks|1995|p=65}} they are used as children's mounts.{{sfn|Dutson|2005|pp=323–325}} The breed has also been used in several mounted patrols.{{sfn|Hendricks|1995|p=65}}

Before 1915, the United States Lifesaving Service used horses for beach watches and rescues. In addition to carrying park rangers on patrols,{{sfn|Prioli|2007|p=48}} the horses hauled equipment to and from shipwreck sites.<ref name="NPSCH" /> During World War II, the [[United States Coast Guard|Coast Guard]] used them for patrols.<ref name="NPSCH" />  In the 1980s Bankers were used for beach duty at [[Cape Hatteras National Seashore]].{{sfn|Prioli|2007|p=48}}

In 1955, ten horses were taken from the Ocracoke herd as a project for [[Mounted Boy Scout Troop 290]]. After taming and [[livestock branding|branding]] the horses, the scouts trained them for public service activities. The Bankers were ridden in [[parade horse|parades]] and used as mounts during programs to spray mosquito-ridden salt marshes.{{sfn|Prioli|2007|p=48}}

== See also ==
* [[The Livestock Conservancy]]
* [[Carolina Marsh Tacky]]
* [[Chincoteague Pony]]
* [[Cumberland Island horse]]
* [[Equus Survival Trust]]
* [[Sable Island Pony]]
* [[Wildlife of North Carolina]]

== References ==
'''Notes'''

{{reflist
| colwidth = 25em
| refs =
<ref name="GaitedHorse">
{{cite web
| last = Campbell Smith
| first = Donna
| title = Breed Profile: Banker Horses
| publisher = The Gaited Horse Magazine
| url = http://thegaitedhorse.com/banker_horses.htm
| accessdate = September 29, 2011
| archiveurl = https://web.archive.org/web/20080706053149/http://www.thegaitedhorse.com/banker_horses.htm
| archivedate = July 6, 2008
}}
</ref>

<ref name="Sponeberg">
{{Cite web |url=http://www.horseweb.com/heritagebreedssouthwest/update.htm |title=North American Colonial Spanish Horse Update |last=Sponenberg |first=D. Phillip |date=August 2005 |publisher=Heritage Breeds Southwest |archive-url=https://archive.is/2013.05.06-223905/http://www.horseweb.com/heritagebreedssouthwest/update.htm |archive-date=May 6, 2013 |dead-url=yes |access-date=January 11, 2009}}
</ref>

<ref name="haflingerhorse">
{{cite web
| title = Breeding Objectives for the American Haflinger Registry
| publisher = American Haflinger Registry
| url = http://www.haflingerhorse.com/documents/Breeding%20Objectives8-05.pdf
| format = PDF
| accessdate = October 19, 2008
| archiveurl= https://web.archive.org/web/20080908010448/http://haflingerhorse.com/documents/Breeding%20Objectives8-05.pdf| archivedate= 8 September 2008 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="HOAInspections">
{{cite web
| last = Ives
| first = Vickie
|author2=Tom Norush|author3=Gretchen Patterson
| date = February 2007
| title = Corolla and Shackleford Horse of the Americas Inspection
| publisher = Horse of the Americas
| url = http://www.corollawildhorses.com/Images/HOA%20Report/hoa-report.pdf
| archiveurl = https://web.archive.org/web/20090318105441/http://www.corollawildhorses.com/Images/HOA%20Report/hoa-report.pdf
| archivedate = 2009-03-18
| format = PDF
| accessdate = January 11, 2009
}}
</ref>

<ref name="AmericanBreeds">
{{cite web
| title = Colonial Spanish Horse
| publisher = American Livestock Breeds Conservancy
| url = http://www.albc-usa.org/cpl/colonialspanish.html
| accessdate = October 19, 2008
| archiveurl= https://web.archive.org/web/20081201053451/http://www.albc-usa.org/cpl/colonialspanish.html| archivedate= 1 December 2008 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="shacklefordhorses">
{{cite web
| last = Mason
| first = Carolyn
| date = November 17, 1997
| title = Shackleford Horses Timeline- History on Hooves: The Horses of Shackleford Banks
| publisher = The Foundation for Shackleford Horses
| url = http://www.shacklefordhorses.org/timeline.htm
| accessdate = January 11, 2009
| archiveurl= https://web.archive.org/web/20090203150239/http://shacklefordhorses.org/timeline.htm| archivedate= 3 February 2009 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="NOAA">
{{cite web
| title = Barrier Islands: Formation and Evolution
| publisher = National Oceanic and Atmospheric Administration
| accessdate = January 12, 2009
| url = http://www.csc.noaa.gov/beachnourishment/html/geo/barrier.htm
| archiveurl = https://web.archive.org/web/20100808200102/http://www.csc.noaa.gov/beachnourishment/html/geo/barrier.htm
| archivedate = August 8, 2010
}}
</ref>

<ref name="Laliberté">
{{cite web
| last = Laliberté
| first = Jennifer
| title = Natural Resource Assessment
| work = National Parks Conservation Association
| publisher = Duke University
| url = http://dukespace.lib.duke.edu/dspace/bitstream/10161/279/1/LaliberteJ.pdf
| format = PDF
| accessdate = January 23, 2009
}}
</ref>

<ref name="NPSCH">
{{cite web
| title = Ocracoke Ponies: The Wild Bankers of Ocracoke Island
| date = November 7, 2003
| work = National Park Service: Cape Hatteras National Seashore
| publisher = U.S. Department of the Interior, National Park Service
| accessdate = November 11, 2008
| url = http://www.nps.gov/caha/historyculture/ocracokeponies.htm
| archiveurl= https://web.archive.org/web/20081211095209/http://www.nps.gov/caha/historyculture/ocracokeponies.htm| archivedate= 11 December 2008 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="ncbeaches">
{{cite news
| publisher = NC Beaches
| year = 2007
| title = Wild Horses of North Carolina
| url = http://www.ncbeaches.com/Features/Wildlife/WildHorsesNorthCarolina/
| accessdate = December 29, 2008
| archiveurl= https://web.archive.org/web/20090125161242/http://www.ncbeaches.com/Features/Wildlife/WildHorsesNorthCarolina/| archivedate= 25 January 2009 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="corollawildhorses home">
{{cite web
| title = What is the Corolla Wild Horse Fund
| publisher = Corolla Wild Horse Fund
| url = http://www.corollawildhorses.com/index.html
| archiveurl = https://web.archive.org/web/20120228210011/http://www.corollawildhorses.com/index.html
| archivedate = 2012-02-28
| accessdate = February 17, 2012
}}
</ref>

<ref name="corollawildhorses adoption">
{{cite web
| title = Adoption Program
| date = December 23, 2008
| publisher = Corolla Wild Horse Fund
| url = http://www.corollawildhorses.com/adoption.html
| accessdate = December 29, 2008
| archiveurl= https://web.archive.org/web/20081204073337/http://www.corollawildhorses.com/adoption.html| archivedate= 4 December 2008 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="NYTimes 2012-05-07">
{{cite news
| last = Beil
| first = Laura
| date = May 7, 2012
| title = Wild Horses' Fate in Outer Banks Lies in Preservation Clash
| newspaper = The New York Times
| url = https://www.nytimes.com/2012/05/08/science/wild-horses-fate-in-outer-banks-lies-in-preservation-clash.html
| accessdate = September 19, 2012
}}
</ref>

<ref name="nccoastalreserve about">
{{cite web
| title = Rachel Carson
| year = 2007
| publisher = North Carolina Coastal Reserve
| url = http://www.nccoastalreserve.net/About-The-Reserve/National-Reserve-Sites/Rachel-Carson/58.aspx
| accessdate = March 15, 2009
| archiveurl= https://web.archive.org/web/20090321001317/http://www.nccoastalreserve.net/About-The-Reserve/National-Reserve-Sites/Rachel-Carson/58.aspx| archivedate= 21 March 2009 <!--DASHBot-->| deadurl= no}}
</ref>

<ref name="NCreserve">
{{cite web
| last = Fear
| first = John
| year = 2008
| title = Rachel Carson Component
| work = North Carolina National Estuarine Research Reserve
| publisher = North Carolina Coastal Reserve
| url = http://www.nccoastalreserve.net/uploads/File/general/siteProfileChapter3%20.pdf
| format = PDF
| accessdate = November 15, 2008
| archiveurl= https://web.archive.org/web/20081219185002/http://www.nccoastalreserve.net/uploads/File/general/siteProfileChapter3%20.pdf| archivedate= 19 December 2008 | deadurl=yes}}
</ref>

}}

'''Bibliography'''

{{refbegin}}
* {{cite journal
  | last1 = Blythe
  | first1 = William B.
  | last2 = Egeblad
  | first2 = K.
  | year = 1983
  | title = The banker ponies of North Carolina and the Ghyben-Herzberg principle
  | journal = Transactions of the American Clinical and Climatological Association
  | volume = 94
  | issue = 6
  | pages = 63–72
  | pmid = 7186237
  | pmc = 2279567
  | ref = harv
  }}
* {{cite encyclopedia
  | last = Dohner
  | first = Janet Vorwald
  | editor =
  | year = 2001
  | encyclopedia = Historic and Endangered Livestock and Poultry Breeds
  | title = Equines: Banker
  | publisher = Yale University Press
  | location = Topeka, Kansas
  | volume =
  | pages = 400–401
  | isbn = 978-0-300-08880-9
  | ref = harv
  }}
* {{cite book
  | last = Dutson
  | first = Judith
  | year = 2005
  | title = Storey's Illustrated Guide to 96 Horse Breeds of North America
  | publisher = Storey Publishing
  | pages = 323–325
  | isbn = 978-1-58017-612-5
  | ref = harv
  }}
* {{cite book
  | last = Harrison
  | first = Molly
  | date = August 1, 2003
  | title = Exploring Cape Hatteras and Cape Lookout National Seashores
  | publisher = Globe Pequot
  | pages = 211–213
  | isbn = 978-0-7627-2609-7
  | ref = harv
  }}
* {{cite book
  | last = Hendricks
  | first = Bonnie Lou
  | year = 1995
  | title = International Encyclopedia of Horse Breeds
  | publisher = University of Oklahoma Press
  | pages = 63–65
  | isbn = 978-0-8061-2753-8
  | ref = harv
  }}
* {{cite book
  | last = Prioli
  | first = Carmine
  | year = 2007
  | title = The Wild Horses of Shackleford Banks
  | publisher = Winston-Salem, N.C.: John F. Blair
  | pages = 15–27
  | isbn = 978-0-89587-334-7
  | ref = harv
  }}
* {{cite book
  | editor-last = Quinn
  | editor-first = David
  | editor-link = David Beers Quinn
  | title = The Roanoke Voyages: 1584–1590
  | year = 1955
  | publisher = Hakluyt Society
  | location = London
  | page = 187
  | isbn = 978-0-486-26513-1
  | ref = harv
  }}
* {{cite journal
  | last1 = Rheinhardt
  | first1 = Richard
  | last2 = Rheinhardt
  | first2 = Martha
  |date=May 2004
  | title = Feral Horse Seasonal Habitat Use on a Coastal Barrier Spit
  | journal = Journal of Range Management
  | volume = 57
  | issue = 3
  | pages = 253–258
  | issn = 1551-5028
  | doi = 10.2111/1551-5028(2004)057[0253:FHSHUO]2.0.CO;2
  | ref = harv
  }}
* {{cite journal
  | last1 = Wood
  | first1 = Gene W.
  | last2 = Mengak
  | first2 = Michael T.
  | last3 = Murphy
  | first3 = Mark
  | year = 2004
  | title = Ecological Importance of Feral Ungulates at Shackleford Banks, North Carolina
  | journal = American Midland Naturalist
  | volume = 118
  | issue = 2
  | pages = 236–244
  | doi = 10.2307/2425780
  | ref = harv
  }}
{{refend}}

{{Featured article}}

{{Equine|state=collapsed}}

[[Category:Endemic fauna of North Carolina]]
[[Category:Mammals of the United States]]
[[Category:Feral horses]]
[[Category:Horse breeds]]
[[Category:Outer Banks]]
[[Category:Horse breeds originating in the United States]]