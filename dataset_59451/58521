{{Infobox journal
| title = Journal of Strategic Information Systems
| cover = [[File:2015 JSIS cover.gif]]
| editor = [[Bob Galliers]]
| discipline = [[Information systems]]
| abbreviation = J. Strateg. Inf. Syst.
| publisher = [[Elsevier]]
| frequency = Quarterly
| history = 1991-present
| impact = 2.692
| impact-year = 2014
| website = http://www.journals.elsevier.com/the-journal-of-strategic-information-systems/
| link2 = http://www.sciencedirect.com/science/journal/09638687
| link2-name = Online archive
| ISSN = 0963-8687
| OCLC = 39189374
| CODEN = JSIYE3
| LCCN = 92645897
}}
The '''''Journal of Strategic Information Systems''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[management]], business, and organizational issues associated with the use of [[information system]]s. It was established in December 1991 and is published by [[Elsevier]]. The [[editors-in-chief]] are Bob Galliers ([[Bentley University]] and [[Loughborough University]]; since 1991) and Sirkka Jarvenpaa ([[University of Texas at Austin]]; since 1999).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-11-04}}</ref>
*[[INSPEC Computer and Control Abstracts]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2015-11-04}}</ref>
*[[Library and Information Science Abstracts]]
*[[Science Citation Index Expanded]]<ref name=ISI/>
*[[Social Sciences Citation Index]]<ref name=ISI/>
*[[PsycINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2015-11-04}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-11-04}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.692.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Strategic Information Systems |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
*{{Official website|http://www.journals.elsevier.com/the-journal-of-strategic-information-systems/}}

[[Category:Information systems]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1991]]
[[Category:Computer science journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]