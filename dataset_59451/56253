{{notability|date=November 2013}}
{{Infobox journal
| title = Classica et Mediaevalia
| cover =
| discipline = [[Philology]], [[history]]
| formernames = Classica et Mediaevalia, Revue danoise de philologie et d'histoire
| abbreviation =
| editor = George Hinge
| language = English, French, German
| publisher = [[Museum Tusculanum Press]]
| country = Denmark
| frequency = Annually
| history = 1938-present
| impact =
| impact-year =
| openaccess =
| website = http://www.mtp.hum.ku.dk/tidsskrift.asp?teln=900002
| ISSN = 0106-5815
| eISSN = 1604-9411
| OCLC = 1770718
| CODEN =
| JSTOR =
}}
'''''Classica et Mediaevalia, Danish Journal of Philology and History,''''' is a [[peer-reviewed]] [[open access]] [[academic journal]] of [[philology]] and [[history]] published annually by [[Museum Tusculanum Press]]. It is based at [[Aarhus University]] and was established in 1938 as ''Classica et Mediaevalia, Revue danoise de philologie et d'histoire''. It publishes articles in English, French, and German.

The [[editor-in-chief]] is George Hinge (Aarhus University). Former editors-in-chief include William Norvin, Franz Blatt, Otto Steen Due, Ole Thomsen, and Tønnes Bekker-Nielsen. The journal publishes contributions relating to the Greek and Latin languages as well as to [[Greek literature|Greek]] and [[Latin literature|Latin]] literature up to and including the [[late Middle Ages]]. It also publishes contributions in the fields of Graeco-Roman history, the classical influence in general history, legal history, the history of philosophy, and ecclesiastical history. Publication of the supplementary series ''Classica et Mediaevalia dissertationes'' has ceased.

''Classica et Mediaevalia'' is ranked "Int1" (history) and "Int2" (classical studies) by the European Reference Index for the Humanities.<ref>{{cite web |url=http://www.mtp.hum.ku.dk/tidsskrift.asp?teln=900002 |publisher=Museum Tusculanum Press |format= | title=Classica et Mediaevalia |accessdate=2013-11-04}}</ref><ref>{{cite web |url=http://www.mtp.hum.ku.dk/details.asp?eln=300332 |publisher=Museum Tusculanum Press |title=Classica et Mediaevalia vol. 63 |format= |work= |accessdate=2013-11-04}}</ref>

== References ==
{{Reflist}}

== External links ==

* {{Official website|1=http://www.mtp.hum.ku.dk/tidsskrift.asp?teln=900002}}

[[Category:Multidisciplinary humanities journals]]
[[Category:Publications established in 1938]]
[[Category:Multilingual journals]]
[[Category:Annual journals]]
[[Category:Aarhus University]]
[[Category:Academic journals published by university presses]]


{{humanities-journal-stub}}