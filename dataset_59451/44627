{{pp|small=yes}} 
{{Use dmy dates|date=May 2014}}
{{Infobox settlement
|official_name          = Sukhumi
|other_name             = Akwa, Sokhumi 
|native_name            = Аҟәа, სოხუმი
|nickname               =
|settlement_type        = City
|motto                  =
|image_skyline          = Sokhumi Collage.jpg
|imagesize              = 
|image_caption          = 
|image_flag             =
|flag_size              =
|image_seal             = Gerb Sukhum.png
|seal_size              =
|image_shield           =
|shield_size            =
|city_logo              =
|citylogo_size          =
|image_map              = <!-- Sokhumi on the map (en).svg -->
|mapsize                =
|map_caption            = location of Sukhumi within Abkhazia
|image_map1             =
|mapsize1               =
|map_caption1           =
|image_dot_map          =
|dot_mapsize            =
|dot_map_caption        =
|dot_x =  |dot_y =
|pushpin_map            = Abkhazia#Georgia
|pushpin_label_position = 
|pushpin_map_caption    = Location of Sukhumi in Georgia
|pushpin_mapsize        = 280
|subdivision_type       = Country
|subdivision_name       = [[Georgia (country)|Georgia]]
|subdivision_type1      = Partially recognized state <!--De facto-->
|subdivision_name1      = [[Abkhazia]]<ref>{{Abkhazia-note}}</ref>
|subdivision_type2      =
|subdivision_name2      =
|subdivision_type3      =
|subdivision_name3      =
|subdivision_type4      =
|subdivision_name4      =
|government_footnotes   =
|government_type        =
|leader_title           =Mayor
|leader_name            =[[Adgur Kharazia]]
|leader_title1          =  <!-- for places with, say, both a mayor and a city manager -->
|leader_name1           =
|leader_title2          =
|leader_name2           =
|leader_title3          =
|leader_name3           =
|leader_title4          =
|leader_name4           =
|established_title      = Settled
|established_date       = 6th century BC
|established_title2     = City Status
|established_date2      = 1848
|established_title3     = 
|established_date3      =
|area_magnitude         =
|area_footnotes         =
|area_total_km2         = 27
|area_land_km2          = <!--See table @ Template:Infobox Settlement for details on automatic unit conversion-->
|area_water_km2         =
|area_total_sq_mi       =
|area_land_sq_mi        =
|area_water_sq_mi       =
|area_water_percent     =
|area_urban_km2         =
|area_urban_sq_mi       =
|area_metro_km2         =
|area_metro_sq_mi       =
|population_as_of       = 2011
|population_footnotes   =
|population_note        =
|population_total       = 62,914 <ref>{{cite web|url=http://www.ethno-kavkaz.narod.ru/rnabkhazia.html |title=п╫п╟я│п╣п╩п╣п╫п╦п╣ п╟п╠я┘п╟п╥п╦п╦ |publisher=Ethno-kavkaz.narod.ru |date= |accessdate=2016-06-26}}</ref>
|population_density_km2 = auto
|population_density_sq_mi       =
|population_metro               =
|population_density_metro_km2   =
|population_density_metro_sq_mi =
|population_urban               =
|population_density_urban_km2   =
|population_density_urban_sq_mi =
|population_blank1_title        =
|population_blank1              =
|population_density_blank1_km2 =
|population_density_blank1_sq_mi =
|timezone               = [[Moscow Time|MSK]]
|utc_offset             = +4
|timezone_DST           =
|utc_offset_DST         =
|coordinates            = {{coord|43|00|12|N|41|00|55|E|region:{{Xb|ABK}}|display=inline}}
|elevation_footnotes    =  <!--for references: use <ref> </ref> tags-->
|elevation_max_m        = 140
|elevation_min_m        = 5
|elevation_ft           =
|postal_code_type       = Postal code
|postal_code            = 384900
|area_code              = +7 840 22x-xx-xx
|registration_plate     = ABH
|blank_name             =
|blank_info             =
|blank1_name            =
|blank1_info            =
|website                = 
|footnotes              =
}}
'''Sukhumi''' or '''Sokhumi'''<ref>[http://www.thefreedictionary.com/Sokhumi|The American Heritage® Dictionary of the English Language, Fourth Edition copyright 2000 by Houghton Mifflin Company.]</ref> ({{lang-ab|Аҟәа}}, ''Aqwa''; {{lang-ka|სოხუმი}}, {{IPA-ka|sɔxumi||Sokhumi.ogg}}; {{lang-ru|link=no|Сухум(и)}}, ''Sukhum(i)'') is a city on the [[Black Sea]] coast. It is the capital of the breakaway Republic of [[Abkhazia]] which has controlled it since the [[War in Abkhazia (1992–93)|1992-93 war in Abkhazia]], although most of the international community considers it legally part of [[Georgia (country)|Georgia]].

Sukhumi's history can be traced back to the 6th century BC, when it was settled by Greeks, who named it Dioscurias. During this time and the subsequent Roman period, much of the city disappeared under the Black Sea. The city was named Tskhumi when it became part of the [[Kingdom of Abkhazia]]. Contested by local princes, it became part of the [[Ottoman Empire]] in the 1570s, where it remained until it was conquered by the [[Russian Empire]] in 1810. Following a period of conflict during [[Russian Civil War]], it became part of the [[Soviet Union]], where it was regarded as a holiday resort. As the Soviet Union broke up in the early 1990s, the city suffered significant damage during the [[Georgian–Abkhazian conflict]]. The present-day population of 60,000 is only half of the population living there towards the end of Soviet rule.

==Naming==
In Georgian, the city is known as სოხუმი (''Sokhumi'') or აყუ (''Aqu''),<ref>[http://www.abkhazworld.com/Pdf/Abkhaz_Loans_in_Megrelian_Chirikba.pdf Abkhaz Loans in Megrelian], p. 65</ref> in [[Megrelian language|Megrelian]] as აყუჯიხა (''Aqujikha''),<ref>Otar Kajaia, 2001–2004, [http://titus.uni-frankfurt.de/texte/etca/cauc/megr/kajaia/kajai.htm Megrelian-Georgian Dictionary] (entry ''aq'ujixa'').</ref> and in Russian as Сухум (''Sukhum'') or Сухуми (''Sukhumi''). The toponym Sokhumi derives from the Georgian word Tskhomi/Tskhumi, meaning ''beech''. It is significant, that "dia" in several dialects of the Georgian language and among them in Megrelian means mother and "skuri" means water.<ref>{{cite web|url=http://www.nplg.gov.ge/dlibrary/collect/0001/001021/Abxazia.pdf |title=Archived copy |accessdate=29 June 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131112021512/http://www.nplg.gov.ge/dlibrary/collect/0001/001021/Abxazia.pdf |archivedate=12 November 2013 }}</ref> In Abkhaz, the city is known as Аҟәа (''Aqwa'') which according to native tradition signifies ''water''.<ref name="colarusso_mp54">{{cite web|url=http://www.safarmer.com/Indo-Eurasian/Pontic-horse.pdf|title=More Pontic: Further Etymologies between Indo-European and Northwest Caucasian|last=Colarusso|first=John|authorlink=John Colarusso|page=54|accessdate=22 August 2009}}</ref>

In the ancient Greek sources (Pseudo-Skilak of Kariand- IV c. B. C. ) the city is referred to as '''Dioscurias'''. According to the antique traditions this name originates from the mythical ''Dioskouri'', the twin brothers [[Castor and Pollux]], sons of [[Zeus]]. It was believed that the town had been established by Castor's and Pollux's coachmen, the [[Argonauts]] Telkius and Amphyst. However the names of the town may simply be the Greek comprehension of the old Georgian word combination.

The medieval Georgian sources knew the town as ''Tskhumi'' (ცხუმი).<ref>[http://titus.uni-frankfurt.de/texte/etcs/cauc/ageo/gh/gh039.htm ''Vita Sanctae Ninonis'']. [[TITUS (project)|TITUS]] Old Georgian hagiographical and homiletic texts: Part No. 39</ref><ref>[http://titus.uni-frankfurt.de/texte/etcs/cauc/ageo/gh/gh041.htm ''Martyrium David et Constantini'']. [[TITUS (project)|TITUS]] Old Georgian hagiographical and homiletic texts: Part No. 41</ref><ref>[http://titus.uni-frankfurt.de/texte/etcs/cauc/ageo/kcx1/kcx1233.htm ''Kartlis Cxovreba'': Part No. 233]. [[TITUS (project)|TITUS]]</ref> Later, under the Ottoman control, the town was known in Turkish as ''Suhum-Kale'', which can be derived from the earlier Georgian form Tskhumi or can be read to mean 'water-sand fortress'.<ref name="goltzgd056">{{cite book|last=Goltz|first=Thomas|title=Georgia Diary|publisher=M.E. Sharpe|location=Armonk, New York / London, England|date=2009|edition=Expanded|page=56|chapter=4. An Abkhazian Interlude|isbn=978-0-7656-2416-1}}</ref><ref>[http://abkhazeti.info/ethno/EkplpAVElyHNzaIaLX2.php Abkhazeti.info] {{ru icon}}</ref> Tskhumi in turn is supposed to be derived from the [[Svan language]] word for 'hot',<ref name="Adrian Room"/> or the Georgian word for '[[hornbeam]] tree'.

The ending -i in the above forms represents the Georgian nominative-suffix. The town was initially officially described in Russian as Сухум (''Sukhum''), until 16 August 1936 when this was changed to Сухуми (''Sukhumi'').{{Citation needed|date=April 2016}} This remained so until 4 December 1992, when the Supreme Council of Abkhazia restored the original version,{{Citation needed|date=April 2016}} that was approved in Russia in autumn 2008,<ref>[http://www.newsru.com/arch/russia/13nov2008/colors.html Абхазию и Южную Осетию на картах в РФ выкрасят в "негрузинские" цвета]</ref> even though Сухуми is also still being used.

In English, the most common form today is ''Sukhumi'', although ''Sokhumi'' is increasing in usage and has been adopted by sources including ''[[Encyclopædia Britannica]]'',<ref>"Sokhumi". (2006). In [[Encyclopædia Britannica]]. Retrieved 6 November 2006, from Encyclopædia Britannica Online: [http://www.britannica.com/eb/article-9070213 Britannica.com]</ref> ''[[MSN Encarta]]'',<ref>"Sokhumi". (2006). In [[Encarta]]. Retrieved 6 November 2006: [http://encarta.msn.com/encyclopedia_761584388/Sokhumi.html Encarta.msn.com]</ref> [[Esri]]<ref>{{cite news|title=Esri ArcGis WebMap|url=http://www.arcgis.com/home/webmap/viewer.html|accessdate=2 May 2016|work=Esri}}</ref> and [[Google Maps]].<ref>{{cite news|title=Google Maps changes Sukhumi to Sokhumi following Georgia’s request|url=http://agenda.ge/news/29597/eng|accessdate=22 February 2015|work=Agenda.ge|date=10 February 2015}}</ref>

==General information==
Sukhumi is located on a wide bay of the eastern coast of the [[Black Sea]] and serves as a port, rail junction and a holiday resort. It is known for its beaches, sanatoriums, mineral-water spas and semitropical climate. Sukhumi is also an important air link for Abkhazia as the [[Sukhumi Dranda Airport]] is located nearby the city. Sukhumi contains a number of small-to-medium size hotels serving chiefly the Russian tourists. [[Sukhumi botanical garden]] was established in 1840, one of the oldest botanical gardens in the Caucasus.

The city has a number of research institutes, the Abkhazian State University and the Sukhumi Open Institute. From 1945 to 1954 the city's electron physics laboratory was involved in the Soviet program to develop nuclear weapons.

The city is a member of the [[International Black Sea Club]].<ref>[http://www.i-bsc.info/emember.php International Black Sea Club, members]</ref>

==History==
[[File:Gamba - view of sukhumi fortress.JPG|thumb|left|The Sohum-Kale fort in the early 19th century.]]
[[File:Colchis coin.jpg|thumb|right|[[Colchis|Colchian]] Coin of Dioscurias, late 2nd century BC. Obverse: Two [[Pileus (hat)|pilei]] surmounted by stars Reverse: [[Thyrsos]], ΔΙΟΣΚΟΥΡΙΑΔΟΣ]]

[[File:Botanical garden.Sukhum.jpg|thumb|[[Sukhumi botanical garden]]]]
[[File:Suhumi Prokudin-Gorskii.jpg|thumb|Sukhumi in 1912. Early color photo by [[Sergei Prokudin-Gorskii]]]]
The history of the city began in the mid-6th century BC when an earlier settlement of the second and early first millennia BC, frequented by local [[Colchis|Colchian]] tribes, was replaced by the [[Miletus|Milesian]] [[Greek colony]] of '''Dioscurias''' ({{lang-el|Διοσκουριάς}}). The city is said to have been so named for the [[Dioscuri]], the twins Castor and Pollux of [[classical mythology]]. It became busily engaged in the commerce between Greece and the indigenous tribes, importing wares from many parts of Greece, and exporting local salt and [[Caucasus|Caucasian]] timber, [[linen]], and [[hemp]]. It was also a prime center of [[slave trade]] in Colchis.<ref>{{cite book |title= An inquiry into the state of slavery amongst the Romans|last= Blair|first= William|year= 1833|publisher= T. Clark|page= 25}}</ref> The city and its surroundings were remarkable for the multitude of languages spoken in its bazaars.<ref>[https://www.gutenberg.org/files/44885/44885-h/44885-h.htm Strabo, The Geography, BOOK XI, II, 16]</ref>

Although the sea made serious inroads upon the territory of Dioscurias, it continued to flourish until its conquest by [[Mithridates VI Eupator]] of [[Pontus]] in the later 2nd century. Under the [[Roman emperor]] [[Augustus]] the city assumed the name of '''Sebastopolis'''<ref name=hewitt_taah>[https://www.google.com/books?vid=ISBN031221975X&id=MNOHKnF1nl8C&printsec=frontcover#PRA1-PA62,M1 Hewitt, George (1998) ''The Abkhazians: a handbook''] St. Martin's Press, New York, p. 62, ISBN 0-312-21975-X</ref> ({{lang-el|Σεβαστούπολις}}). But its prosperity was past, and in the 1st century [[Pliny the Elder]] described the place as virtually deserted though the town still continued to exist during the times of [[Arrian]] in the 130s.<ref>Dioscurias. ''A Guide to the Ancient World'', H.W. Wilson (1986). Retrieved 20 July 2006, from [http://www.xreferplus.com/entry/5073174 Xreferplus.com]</ref> The remains of towers and walls of Sebastopolis have been found underwater; on land the lowest levels so far reached by archaeologists are of the 1st and 2nd centuries AD. In 542 the Romans evacuated the town and demolished its citadel to prevent it from being captured by [[Sassanid dynasty|Sassanid Iran]]. In 565, however, the emperor [[Justinian I]] restored the fort and Sebastopolis continued to remain one of the Byzantine strongholds in Colchis until [[Marwan ibn Muhammad's invasion of Georgia|being sacked]] by the [[Arab]] conqueror [[Marwan II]] in 736.

Afterwards, the town came to be known as '''Tskhumi'''.<ref name="Adrian Room">Room, A. (2005), ''Placenames of the World: Origins and Meanings of the Names for 6,600 Countries, Cities, Territories, Natural Features and Historic Sites''. McFarland & Company, Inc., Publishers, Jefferson, North Carolina, and London, ISBN 0-7864-2248-3, p. 361</ref> Restored by the [[Abkhazian Kingdom|kings of Abkhazia]] from the Arab devastation, it particularly flourished during the [[Georgian Golden Age]] in the 12th–13th centuries, when Tskhumi became a center of traffic with the European maritime powers, particularly with the [[Republic of Genoa]]. The Genoese established their short-lived trading factory at Tskhumi early in the 14th century.

The [[Ottoman Empire|Ottoman]] navy occupied the town in 1451, but for a short time. Later contested between the princes of [[Principality of Abkhazia|Abkhazia]] and [[Mingrelia]], Tskhumi finally fell to the Turks in the 1570s. The new masters heavily fortified the town and called it '''Sohumkale''', with ''kale'' meaning "fort" but the first part of the name of disputed origin. It may represent [[Turkish language|Turkish]] ''su'', "water", and ''kum'', "sand", but is more likely to be an alteration of its earlier Georgian name.<ref name="Adrian Room"/> At the request of the pro-Russian Abkhazian prince, the town was stormed by the [[Russian Marines]] in 1810 and turned, subsequently, into a major outpost in the North West Caucasus. (See [[Russian conquest of the Caucasus#Black Sea Coast]]). Sukhumi was declared the seaport in 1847 and was directly annexed to the [[Russian Empire]] after the ruling [[Shervashidze]] princely dynasty was ousted by the Russian authorities in 1864. During the [[Russo-Turkish War, 1877–1878]], the town was temporarily controlled by the Ottoman forces and Abkhaz-[[Adyghe people|Adyghe]] rebels.

[[File:On quay. Sukhum.jpg|thumb|left|Sukhumi quay]]

Following the [[Russian Revolution of 1917]], the town and Abkhazia in general were engulfed in the chaos of the [[Russian Civil War]]. A short-lived [[Bolshevik]] government was suppressed in May 1918 and Sukhumi was incorporated into the [[Democratic Republic of Georgia]] as a residence of the autonomous People's Council of Abkhazia and the headquarters of the Georgian governor-general. The [[Red Army]] and the local revolutionaries took the city from the Georgian forces on 4 March 1921, and declared Soviet rule. Sukhumi functioned as the capital of the "Union treaty" [[Abkhazian SSR|Abkhaz Soviet Socialist Republic]] associated with the [[Georgian SSR]] from 1921 until 1931, when it became the capital of the Abkhazian Autonomous Soviet Socialist Republic within the Georgian SSR. By 1989, Sukhumi had 110,000 inhabitants and was one of the most prosperous cities of Georgia. Many holiday [[dacha]]s for Soviet leaders were situated there.

Beginning with the [[1989 Sukhum riots|1989 riots]], Sukhumi was a centre of the [[Georgian-Abkhaz conflict]], and the city was severely damaged during the [[War in Abkhazia (1992-1993)|1992–1993 War]]. During the war, the city and its environs suffered almost daily air strikes and artillery shelling, with heavy civilian casualties.<ref>{{cite web|url=https://www.hrw.org/reports/1995/Georgia2.htm |title=The Human Rights Watch report, March 1995 Vol. 7, No. 7 |publisher=Hrw.org |date= |accessdate=4 July 2010}}</ref> On 27 September 1993 the battle for Sukhumi was concluded by a full-scale campaign of [[Ethnic cleansing of Georgians in Abkhazia|ethnic cleansing]] against its majority Georgian population (see [[Sukhumi Massacre]]), including members of the pro-Georgian Abkhazian government ([[Zhiuli Shartava]], [[Raul Eshba]] and others) and mayor of Sukhumi [[Guram Gabiskiria]]. Although the city has been relatively peaceful and partially rebuilt, it is still suffering the after-effects of the war, and it has not regained its earlier ethnic diversity. Its population in 2003 was 43,716, compared to about 120,000 in 1989.<ref name = "2003 Census statistics (in Russian)">[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html 2003 Census statistics] {{ru icon}}</ref>

== Population ==

===Demographics===
Historic population figures for Sukhumi, split out by ethnicity, based on population censuses:<ref name=censuses>[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html Population censuses in Abkhazia: 1886, 1926, 1939, 1959, 1970, 1979, 1989, 2003] {{ru icon}}</ref>

{| class="wikitable"
 !Year
 ![[Abkhaz people|Abkhaz]]
 ![[Armenians in Abkhazia|Armenians]] 
 ![[Estonian people|Estonians]]
 ![[Georgians]]
 ![[Pontic Greeks|Greeks]]
 ![[Russians]]
 ![[Turkish people|Turkish]]
 ![[Ukrainians]]
 !Total
 |-
 |1897 Census
 | align="right" |1.8%<br/><small>(144)</small>
 | align="right" |13.5%<br/><small>(1,083)</small>
 | align="right" |0.4%<br/><small>(32)</small>
 | align="right" |11.9%<br/><small>(951)</small>
 | align="right" |14.3%<br/><small>(1,143)</small>
 | align="right" |0.0%<br/><small>(1)</small>
 | align="right" |2.7%<br/><small>(216)</small>
| style="text-align:right;"|
 | align="right" |7,998
 |-
 |1926 Census
 | align="right" |3.1%<br/><small>(658)</small>
 | align="right" |9.4%<br/><small>(2,023)</small>
 | align="right" |0.3%<br/><small>(63)</small>
 | align="right" |11.2%<br/><small>(2,425)</small>
 | align="right" |10.7%<br/><small>(2,298)</small>
 | align="right" |23.7%<br/><small>(5,104)</small>
 | align="right" |---
 | align="right" |10.4%<br/><small>(2,234)</small>
 | align="right"  |21,568
 |-
 |1939 Census
 | align="right" |5.5%<br/><small>(2,415)</small>
 | align="right" |9.8%<br/><small>(4,322)</small>
 | align="right" |0.5%<br/><small>(206)</small>
 | align="right" |19.9%<br/><small>(8,813)</small>
 | align="right" |11.3%<br/><small>(4,990)</small>
 | align="right" |41.9%<br/><small>(18,580)</small>
 | align="right" |---
 | align="right" |4.6%<br/><small>(2,033)</small>
 | align="right"  |44,299
 |-
 |1959 Census
 | align="right" |5.6%<br/><small>(3,647)</small>
 | align="right" |10.5%<br/><small>(6,783)</small>
 | align="right" |---
 | align="right" |31.1%<br/><small>(20,110)</small>
 | align="right" |4.9%<br/><small>(3,141)</small>
 | align="right" |36.8%<br/><small>(23,819)</small>
 | align="right" |---
 | align="right" |4.3%<br/><small>(2,756)</small>
 | align="right"  |64,730
 |-
 |1979 Census
 | align="right" |9.9%<br/><small>(10,766)</small>
 | align="right" |10.9%<br/><small>(11,823)</small>
 | align="right" |---
 | align="right" |38.3%<br/><small>(41,507)</small>
 | align="right" |6.5%<br/><small>(7,069)</small>
 | align="right" |26.4%<br/><small>(28,556)</small>
 | align="right" |---
 | align="right" |3.4%<br/><small>(3,733)</small>
 | align="right"  |108,337
 |-
 |1989 Census
 | align="right" |12.5%<br/><small>(14,922)</small>
 | align="right" |10.3%<br/><small>(12,242)</small>
 | align="right" |---
 | align="right" |41.5%<br/><small>(49,460)</small>
 | align="right" |---
 | align="right" |21.6%<br/><small>(25,739)</small>
 | align="right" |---
 | align="right" |---
 | align="right"  |119,150
 |-
 |2003 Census
 | align="right" |65.3%<br/><small>(24,603)</small>
 | align="right" |12.7%<br/><small>(5,565)</small>
 | align="right" |0.1%<br/><small>(65)</small>
 | align="right" |4.0%<br/><small>(1,761)</small>
 | align="right" |1.5%<br/><small>(677)</small>
 | align="right" |16.9%<br/><small>(8,902)</small>
 | align="right" |---
 | align="right" |1.6%<br/><small>(712)</small>
 | align="right"  |43,716
 |-
 |2011 Census
 | align="right" |67.3%<br/><small>(42,603 )</small>
 | align="right" |9.8%<br/><small>(6,192)</small>
 | align="right" |---
 | align="right" |2.8%<br/><small>(1,755)</small>
 | align="right" |1.0%<br/><small>(645)</small>
 | align="right" |14.8%<br/><small>(9,288)</small>
 | align="right" |---
 | align="right" |---
 | align="right"  |62,914
 |}

=== Religion ===
Ancient Sebastopolis was a Latin bishopric, but the diocese ceased to exist with the advent of Orthodoxy.

==== Titular see ====
The diocese of '''Sebastopolis in Abasgia''' (meaning 'in [[Abchasia]]') was nominally restored as a Catholic Latin.
 
It has had the following incumbents, but is now vacant:
* Celestino Annibale Cattaneo, [[Capuchin Franciscans]] (O.F.M. Cap.) (1936.03.03 – 1946.02.15)
* Henri-Édouard Dutoit (1949.04.23 – 1953.04.17)
* Aluigi Cossio (1955.08.12 – 1956.01.03)
* [[Giuseppe Paupini]] (1956.02.02 – 1969.04.28), later [[Cardinal (Catholicism)|cardinal]]

== Main sights ==
[[File:Beslet bridge.JPG|thumb|Medieval [[Besleti Bridge|bridge]] over the Besletka river known as the [[Tamar of Georgia|Queen Tamar]] Bridge.]]
Sukhumi houses a number of historical monuments, notably the [[Besleti Bridge]] built during the reign of queen [[Tamar of Georgia]] in the 12th century. It also retains visible vestiges of the defunct monuments, including the Roman walls, the medieval [[Bagrat's Castle|Castle of Bagrat]], several towers of the [[Kelasuri Wall|Great Abkhazian Wall]] constructed by the early modern Mingrelian and Abkhazian princes amid their territorial disputes; the 14th-century Genoese fort and the 18th-century Ottoman fortress. The 11th century [[Kamani Church]] ({{convert|12|km|0|abbr=off}} from Sukhumi) is erected, according to tradition, over the tomb of Saint [[John Chrysostom]]. Some {{convert|22|km|0|abbr=on}} from Sukhumi lies [[New Athos]] with the ruins of the medieval city of Anacopia. The [[Neo-Byzantine]] New Athos Monastery was constructed here in the 1880s on behest of [[Tsar]] [[Alexander III of Russia]].
 
Northward in the mountains is the [[Voronya Cave]], the deepest in the world, with a depth of 2,140 meters.<ref>[http://www.showcaves.com/english/misc/caves/Voronya.html Voronya Peshchera]. ''Show Caves of the World''. Retrieved on 29 July 2008.</ref>

== Climate ==
Sukhumi has a [[humid subtropical climate]] ([[Köppen climate classification|Köppen]] ''Cfa''), that is almost cool enough in summer to be an [[oceanic climate]].
{{Weather box |metric first= Yes |single line= Yes |location= Sukhumi 
|Jan high C= 8  
|Feb high C= 9
|Mar high C= 12
|Apr high C= 16
|May high C= 19
|Jun high C= 23
|Jul high C= 25
|Aug high C= 26
|Sep high C= 23
|Oct high C= 19
|Nov high C= 15
|Dec high C= 11  
|Jan low C= 2 
|Feb low C= 2
|Mar low C= 5
|Apr low C= 8
|May low C= 12
|Jun low C= 17
|Jul low C= 19
|Aug low C= 19
|Sep low C= 15
|Oct low C= 11
|Nov low C= 7
|Dec low C= 3
|precipitation colour=green
|Jan precipitation mm = 137
|Feb precipitation mm = 113
|Mar precipitation mm = 135
|Apr precipitation mm = 123
|May precipitation mm = 115
|Jun precipitation mm = 127
|Jul precipitation mm = 100
|Aug precipitation mm = 123
|Sep precipitation mm = 127
|Oct precipitation mm = 121
|Nov precipitation mm = 133
|Dec precipitation mm = 152
|Jan rain days= 17
|Feb rain days= 15
|Mar rain days= 16
|Apr rain days= 15
|May rain days= 12
|Jun rain days= 11
|Jul rain days= 10
|Aug rain days= 10
|Sep rain days= 10
|Oct rain days= 12
|Nov rain days= 16
|Dec rain days= 16      
|source 1= Weatherbase<ref>{{cite web|url=http://www.weatherbase.com/weather/weatherall.php3?s=6273&refer=&units=metric&cityname=Sukhumi-Georgia |title=Sukhumi, Georgia Travel Weather Averages |publisher=Weatherbase |date= |accessdate=2016-06-26}}</ref>
|source 2= Georgia Travel Climate Information<ref>{{cite web|url=http://www.travel-climate.com/georgia/data.php?cit=37260 |title=Georgia, Sukhumi climate information |publisher=Travel-climate.com |date= |accessdate=2016-06-26}}</ref>
|date= March 2011}}

== Administration ==
On 2 February 2000, President Ardzinba dismissed temporary Mayor [[Leonid Osia]] and appointed [[Leonid Lolua]] in his stead.<ref name=apress-agov-narod>{{cite news|title=СООБЩЕНИЯ АПСНЫПРЕСС|url=http://abkhazia-gov.narod.ru/apsnypress.html|accessdate=21 January 2012|newspaper=[[Apsnypress]]|date=2 February 2000}}</ref> Lolua was reappointed on 10 May 2001 following the March 2001 local elections.<ref name="press2001-92">{{cite news|title=Выпуск № 92|url=http://abkhazia.narod.ru/gb/1579|accessdate=24 April 2016|agency=[[Apsnypress]]|date=10 May 2001}}</ref>

On 5 November 2004, in the heated aftermath of the [[Abkhazian presidential election, 2004|2004 presidential election]], president [[Vladislav Ardzinba]] appointed head of the [[Gulripsh district]] assembly [[Adgur Kharazia]] as acting mayor. During his first speech he called upon the two leading candidates, [[Sergei Bagapsh]] and [[Raul Khadjimba]], to both withdraw.<ref name=rferl041110>{{cite news|url=http://www.hri.org/cgi-bin/brief?/news/balkans/rferl/2004/04-11-10.rferl.html#21|title=MAYOR SUGGESTS ABKHAZ PRESIDENTIAL RIVALS SHOULD WITHDRAW|publisher=RFE/RL|date=10 November 2004|accessdate=1 July 2008}}</ref>

On 16 February 2005, after his election as President, Bagapsh replaced Kharazia with [[Astamur Adleiba]], who had been Minister for Youth, Sports, Resorts and Tourism until December 2004.<ref name=agov1498>{{cite news|url=http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1498|title=Указ Президента Абхазии №5 от 16.02.2005|publisher=Администрация Президента Республики Абхазия|date=16 February 2005|accessdate=1 July 2008}}</ref> In the 11 February 2007 local elections, Adleiba successfully defended his seat in the Sukhumi city assembly and was thereupon reappointed mayor by Bagapsh on 20 March.<ref>{{cite news|url=http://www.apsnypress.info/news2007/march/20.htm|title=Президент Сергей Багапш подписал указы о назначении глав городских и районных администраций|publisher=[[Apsnypress|Апсныпресс]]|date=20 March 2007|accessdate=1 July 2008}}</ref>

In April 2007, while President Bagapsh was in Moscow for medical treatment, the results of an investigation into corruption within the Sukhumi city administration were made public. The investigation found that large sums had been embezzled and upon his return, on 2 May, Bagapsh fired Adleiba along with his deputy [[Boris Achba]], the head of the Sukhumi's finance department [[Konstantin Tuzhba]] and the head of the housing department [[David Jinjolia]].<ref name=iwpr335985>{{cite news|url= http://iwpr.net/report-news/abkhazias-anti-corruption-drive|title=Abkhazia's anti-corruption drive|publisher=[[Institute for War & Peace Reporting]]|date=20 March 2007|accessdate=20 January 2012}}</ref> On 4 June Adleiba paid back to the municipal budget 200,000 rubels.<ref>{{cite news|url=http://www.regnum.ru/news/838958.html&tbb=1|title=Экс-мэр Сухуми вернул в бюджет двести тысяч рублей|publisher=REGNUM|date=5 June 2007|accessdate=2 July 2008}}</ref> and on 23 July, he resigned from the Sukhumi city council, citing health reasons and the need to travel abroad for medical treatment.<ref>{{cite news|url=http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=7220|title=Экс-мэр Сухума намерен покинуть Столичное городское Собрание|publisher=Администрация Президента Республики Абхазия|date=23 July 2007|accessdate=2 July 2008}}</ref>

On 15 May 2007, president Bagapsh released [[Alias Labakhua]] as First Deputy Chairman of the State Customs Committee and appointed him acting Mayor of Sukhumi, a post temporarily fulfilled by former Vice-Mayor [[Anzor Kortua]]. On 27 May Labakhua appointed [[Vadim Cherkezia]] as Deputy Chief of staff.<ref name=uzel115078>{{cite news|title=Заместителем главы администрации столицы Абхазии назначен Вадим Черкезия|url=http://www.kavkaz-uzel.ru/articles/115078/|accessdate=18 April 2012|newspaper=[[Caucasian Knot]]|date=27 May 2007}}</ref> On 2 September, Labakhua won the by-election in constituency No. 21, which had become necessary after Adleiba relinquished his seat. Adleiba was the only candidate and voter turnout was 34%, higher than the 25% required.<ref>{{cite news|url=http://www.apsnypress.info/news2007/september/3.htm|title=АЛИАС ЛАБАХУА ИЗБРАН ДЕПУТАТОМ ГОРОДСКОГО СОБРАНИЯ СУХУМА|publisher=[[Apsnypress|Апсныпресс]]|date=3 September 2007|accessdate=2 July 2008}}</ref> Since Adleiba was now a member of the city assembly, president Bagapsh could permanently appoint him Mayor of Sukhumi on 18 September.<ref>{{cite news|url=http://www.apsnypress.info/news2007/september/18.htm|title=СЕРГЕЙ БАГАПШ ПОДПИСАЛ УКАЗ О НАЗНАЧЕНИИ АЛИАСА ЛАБАХУА ГЛАВОЙ АДМИНИСТРАЦИИ ГОРОДА СУХУМ|publisher=[[Apsnypress|Апсныпресс]]|date=18 September 2007|accessdate=2 July 2008}}</ref>

Following the [[Abkhazian Revolution|May 2014 Revolution]] and the election of [[Raul Khajimba]] as President, he on 22 October dismissed Labakhua and again appointed (as acting Mayor) [[Adgur Kharazia]], who at that point was Vice Speaker of the [[People's Assembly of Abkhazia|People's Assembly]].<ref name=apress13326>{{cite news|title=Адгур Харазия назначен исполняющим обязанности главы администрации г. Сухум|url=http://apsnypress.info/news/13326.html|accessdate=22 October 2014|agency=[[Apsnypress]]|date=22 October 2014}}</ref> Kharazia won the 4 April 2015 by-election to the City Council in constituency no. 3 unopposed,<ref name=alhra30>{{cite web|title=Итоги выборов|url=http://alhra.org/index.php/30-sostoyanie-vybornogo-protsessa-na-12-00-04-04-2015g|website=alhra.org|publisher=Избирательная комиссия по выборам в органы местного самоуправления г.Сухум|accessdate=19 September 2015}}</ref> and was confirmed as mayor by Khajimba on 4 May.<ref name=ukaz120>{{cite web|last1=Khajimba|first1=Raul|authorlink=Raul Khajimba|title=УКАЗ О главе администрации города Сухум|url=http://presidentofabkhazia.org/upload/iblock/ab8/%D0%A3%D0%BA%D0%B0%D0%B7%20%D0%9E%20%D0%B3%D0%BB%D0%B0%D0%B2%D0%B5%20%D0%90%D0%B4%D0%BC%D0%B8%D0%BD%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D0%B8%20%D0%B3.%20%D0%A1%D1%83%D1%85%D1%83%D0%BC.pdf|website=presidentofabkhazia.org|accessdate=19 September 2015}}</ref>

=== List of Mayors ===
{| class="wikitable" style="width:75%;"
|-  style="background:#e9e9e9; font-weight:bold; text-align:left;"
| #
| width=240|Name
| width=200|From
|
| width=200|Until
|
| width=200|President
| width=200|Comments
|-
| colspan="8" style="text-align:center;"|'''Chairmen of the (executive committee of the) City Soviet:'''
|-
|
|[[Vladimir Mikanba]]
|1975
|<ref name=lakoba/>
|1985
|<ref name=lakoba/>
|
|
|-
|
|D. Gubaz
|<=1989
|
|>=1989
|
|
|
|-
|
|[[Nodar Khashba]]
|1991
|<ref name=lakoba>{{cite web|last=Lakoba|first=Stanislav|authorlink=Stanislav Lakoba|title=Кто есть кто в Абхазии|url=http://apsnyteka.narod2.ru/l/abhaziya_posle_dvuh_imperii_xix-xxi_vv/kto_est_kto_v_abhazii_ukazatel_imen/index.html|accessdate=20 January 2012}}</ref>
|
|
|
|First time
|-
|
|[[Guram Gabiskiria]]
|1992
|
|27 September 1993
|
|
|
|-
| colspan="8" style="text-align:center;"|'''Heads of the City Administration:'''
|-
|rowspan=2|
|rowspan=2|[[Nodar Khashba]]
|1993
|<ref name=lakoba/>
|26 November 1994
|
|
|rowspan=2|Second time
|-
|26 November 1994
|
|1995
|<ref name=lakoba/>
|rowspan=5|[[Vladislav Ardzinba]]
|-
|
|[[Garri Aiba]]
|1995
|
|2000
|
|
|-
|
|''[[Leonid Osia]]''
|
|
|2 February 2000
|<ref name=apress-agov-narod/>
|Acting Mayor
|-
|
|[[Leonid Lolua]]
|2 February 2000
|<ref name=apress-agov-narod/>
|5 November 2004
|<ref name=rferl041110/>
|
|-
| 
|''[[Adgur Kharazia]]''
|5 November 2004
|<ref name=rferl041110/>
|16 February 2005
|<ref name=agov1498/>
|Acting Mayor, first time
|-
|
|[[Astamur Adleiba]]
|16 February 2005
|<ref name=agov1498/>
|2 May 2007
|<ref name=iwpr335985/>
|rowspan=3|[[Sergei Bagapsh]]
|
|-
|
|''[[Anzor Kortua]]''
|May 2007
|
|15 May 2007
|
|Acting Mayor
|-
| rowspan=3|
| rowspan=3|[[Alias Labakhua]]
|15 May 2007
|
|29 May 2011
| 
|
|-
|29 May 2011
|
|1 June 2014
|
|[[Alexander Ankvab]]
|
|-
|1 June 2014
|
|22 October 2014
|
|''[[Valeri Bganba]]''
|
|-
|
|[[Adgur Kharazia]]
|22 October 2014
|
|Present
|
|[[Raul Khajimba]]
|Second time
|}

== Transport ==
[[File:Railway station Suhum.jpg|thumb|Railway station]]
The city is served by several [[trolleybus]] and bus routes. Sukhumi is connected to other [[Abkhazia]]n towns by bus routes.{{Citation needed|date=January 2017}}

There is a [[Sukhumi Railway station|railway station]] in Sukhumi, that has a daily train to Moscow via [[Sochi railway station|Sochi]].{{Citation needed|date=January 2017}}

[[Sukhumi Babushara Airport|Babushara Airport]] now handles only local flights due to the disputed status of Abkhazia.{{Citation needed|date=January 2017}}

== International relations ==
{{See also|List of twin towns and sister cities in Georgia}}

===Twin towns&nbsp;— Sister cities===
Sukhumi is twinned with the following cities:
*{{flagicon|RUS}} [[Ufa]], [[Russia]]{{Citation needed|date=January 2017}}
*{{flagicon|RUS}} [[Krasnodar]], [[Russia]]{{Citation needed|date=January 2017}}
*[[File:Flag of Transnistria (state).svg|25px]] [[Tiraspol]], [[Transnistria]]{{Citation needed|date=January 2017}}
*{{flagicon|RUS}} [[Cherkessk]], [[Russia]]{{Citation needed|date=January 2017}}
*{{flagicon|RUS}} [[Podolsk]], [[Russia]]<ref>{{cite web|url=http://www.admpodolsk.ru/index.php?categoryid=126&p2_articleid=286 |title=Сайт Администрации г.Подольска – Побратимы |publisher=Admpodolsk.ru |date=2016-06-15 |accessdate=2016-06-26}}</ref>
*{{flagicon|RUS}} [[Volgograd]], [[Russia]]{{Citation needed|date=January 2017}}
*{{flagicon|RUS}} [[Grozny]], [[Russia]]{{Citation needed|date=January 2017}}
*[[File:Flag of Nagorno-Karabakh.svg|25px]] [[Stepanakert]], [[Nagorno-Karabakh Republic]]{{Citation needed|date=January 2017}}
*{{flagicon|RUS}} [[Arkhangelsk]], [[Russia]]<ref>{{cite web|url=http://apsnypress.info/news/3422.html |title=Новости |publisher=Apsnypress.info |date= |accessdate=2016-06-26}}</ref>
*{{flagicon|RUS}} [[Nizhny Novgorod]], [[Russia]]{{Citation needed|date=January 2017}}
*{{flagicon|ITA}} [[Sant'Antioco]], [[Italy]]<ref>{{cite web|url=http://www.mfaapsny.org/news/index.php?ID=2314 |title=12 мая между городами Абхазии и Италии были подписаны Протоколы о дружбе и сотрудничестве |publisher=Mfaapsny.org |date= |accessdate=2016-06-26}}</ref><ref>{{cite web|url=http://www.laprovinciadelsulcisiglesiente.com/wordpress/2014/05/il-sulcis-rafforza-il-legame-con-i-paesi-dellest-europeo-sottoscritto-questa-sera-un-protocollo-damicizia-con-labkhcazia/ |title=Il Sulcis rafforza il legame con i paesi dell’Est europeo, sottoscritto questa sera un protocollo d’amicizia con l’Abkhcazia |publisher=Laprovinciadelsulcisiglesiente.com |date=2013-04-09 |accessdate=2016-06-26}}</ref>
*{{flagicon|TUR}} [[Side, Antalya|Side]], [[Turkey]]<ref>[http://www.bugun.com.tr/son-dakika/sohum-belediye-baskani-labakhua--haberi/742503 Twin-Town agreement between Side, Antalya, Turkey and Sukhumi, Abkhazia, Georgia]</ref>

== See also ==
* [[Sukhumi District]]
* [[List of twin towns and sister cities in Georgia]]

== References ==
{{reflist|40em}}

== Sources and External links==
{{Commons category}}
{{wikivoyage|Sukhumi}}
* [http://www.gcatholic.org/dioceses/former/t1545.htm GigaCatholic for the titular see, linking to incumbent biographies]
* [http://www.unomig.org/print_multimedia/photogallery/?gid=25 UNOMIG photo gallery of Sukhumi]
* [http://www.abkhazmedia.com/ News from FM Radio Abkhazia]
{{Coord|43|00|N|41|01|E|region:GE_type:city|display=title}}

{{Administrative divisions of Abkhazia}}
{{Cities and towns in Georgia (country)}}
{{List of Asian capitals by region}}
{{Pontic colonies}}

{{Authority control}}

[[Category:Sukhumi| ]]
[[Category:Kutaisi Governorate]]
[[Category:Milesian Pontic colonies]]
[[Category:Capitals in Asia]]
[[Category:Capitals in Europe]]
[[Category:Port cities in Asia]]
[[Category:Port cities in Europe]]
[[Category:Black Sea]]
[[Category:Greek colonies in Colchis]]
[[Category:Georgian Black Sea coast]]