{{good article}}
{{Infobox military conflict
| conflict    = Battle of Lake Pontchartrain
| partof      = the [[Gulf Coast campaign]]
| image       = [[Image:Louisiana Locator Map with US.PNG|200px]]
| caption     = Map showing modern Louisiana.  [[Lake Pontchartrain]] is the large lake in its easternmost area, and is just north of [[New Orleans]]
| date        = September 10, 1779
| place       = [[Lake Pontchartrain]], then British [[West Florida]], now [[Louisiana]]
| result      = Spanish-American victory
| combatant1  = {{flag|United States|1777}}<br>{{flag|Spain|1748}}
| combatant2  = {{flagcountry|Kingdom of Great Britain}}
| commander1  = [[William Pickles (American Revolution)|William Pickles]]
| commander2  = John Payne{{KIA}}<ref>Some short biographies of [[John Willett Payne]] claim that he commanded the ''West Florida''.  This is unlikely, since lengthier biographies place him in European waters at the time of this action.  See e.g. {{cite web|url=http://www.oxforddnb.com/view/article/21648|title=Payne, John Willett|publisher=Oxford Dictionary of National Biography|first=Randolph|last=Cocks|accessdate=2012-01-09}} (subscription required for ODNB)</ref>
| strength1   = [[schooner]] {{USS|Morris|1779|6}}, 57 men
| strength2   = [[sloop-of-war]] [[HMS West Florida|HMS ''West Florida'']], 15 men
| casualties1 = 6–8 killed, some wounded
| casualties2 = 2 killed, 1 wounded, 1 sloop-of-war captured
| campaignbox =
{{Campaignbox Anglo-Spanish War (1779)}}
}}

The '''Battle of Lake Pontchartrain''' was a [[single-ship action]] on September 10, 1779, part of the [[Anglo-Spanish War 1779|Anglo-Spanish War]]. It was fought between the [[Kingdom of Great Britain|British]] [[sloop-of-war]] [[HMS West Florida|HMS ''West Florida'']] and the [[Continental Navy]] [[schooner]] {{USS|Morris|1779|6}} in the waters of [[Lake Pontchartrain]], then in the British province of [[West Florida]].

The ''West Florida'' was patrolling on Lake Pontchartrain when it encountered the ''Morris'', which had set out from [[New Orleans]] with a Spanish and American crew headed by Continental Navy Captain [[William Pickles (American Revolution)|William Pickles]].  The larger crew of the ''Morris'' successfully boarded the ''West Florida'', inflicting a mortal wound on its captain, Lieutenant John Payne.  The capture of the ''West Florida'' eliminated the major British naval presence on the lake, weakening already tenuous British control over the western reaches of West Florida.

==Background==
Significant military activities of the [[American Revolutionary War]] did not occur on the [[Gulf Coast of the United States|Gulf Coast]] until 1779, when [[Spain in the American Revolutionary War|Spain entered the war]].  Before then, [[New Orleans]], then the capital of [[Spanish Louisiana]], served as a semi-secret source of money and [[matériel]] for the [[Patriot (American Revolution)|Patriot]] cause.  The cause was quietly supported by the Spanish governors before 1779, and often mediated by [[Oliver Pollock]], a prominent New Orleans businessman.<ref>Kinnaird, pp. 256–259</ref>  Pollock effectively acted as an agent of the [[Continental Congress]], negotiating with the Spanish governor, and taking other actions, including spending some of his own fortune, on Patriot activities along the lower [[Mississippi River]].<ref>James, pp. 65–71, 241</ref><ref>Ellis, p. 50</ref>

In 1778 [[James Willing]] led a raiding expedition directed against targets in British [[West Florida]].  One prize that he captured on the Mississippi River was a British ship, the ''Rebecca'', which he brought into New Orleans.<ref>Kinnaird, p. 260</ref>  She was brought into the [[Continental Navy]] and rechristened the {{USS|Morris|1778|6}} in honor of [[Philadelphia]] financier [[Robert Morris (financier)|Robert Morris]].<ref name="DANFS">{{cite web|url=http://www.history.navy.mil/danfs/m14/morris-i.htm|title=DANFS entry for USS Morris|publisher=US Naval Historical Center|accessdate=2010-02-12}}</ref>

The British province of West Florida extended from the Mississippi River in the west to the [[Apalachicola River]] in the east.<ref>Ellis, p. 42</ref>  The HMS ''West Florida'' had been cruising [[Lake Pontchartrain]] since 1776 under the command of George Burdon, stopping and searching all manner of shipping, including Spanish merchants destined for New Orleans, to the annoyance of the Spanish.  Burdon was unsuccessful in tracking down Willing during his 1778 raid, and returned to [[Pensacola, Florida|Pensacola]], West Florida's capital, for refit and repair late in 1778.  In January 1779 Burdon was replaced at her helm by Lieutenant John Payne, who had been engaged in survey duty along the West Florida coast and knew the area well.<ref>Rea, pp. 197–199</ref>  The ''West Florida'' was a [[sloop-of-war]] armed, according to its captors, with several four- and six-pound [[cannon]]s and carrying a crew complement of about 30.<ref name=Ellis54/><ref>Rea, p. 197</ref>  (British accounts place the crew size at 15.<ref name=Rea200/>)

==Prelude==
[[File:BritishWestFlorida1776.jpg|thumb|left|300px|Detail from a 1776 map showing the British province of [[West Florida]]]]
Payne cruised West Florida's waters uneventfully until August 1779.  On August 27 he sent a boat with a few men to make contact with a detachment of Lieutenant Colonel Alexander Dickson's men at [[Manchac, Louisiana|Manchac]].  The boat never returned.  On that day [[Bernardo de Gálvez]], the governor of Spanish Louisiana, launched an expedition to gain control of British military posts on the Mississippi, and the boat was captured by his men.<ref>Rea, pp. 199–200</ref>  Gálvez successfully [[Capture of Fort Bute|took the Manchac garrison]] on September 7, and negotiated the surrender of Dickson and the remaining British forces on the Mississippi after the [[Battle of Baton Rouge (1779)|Battle of Baton Rouge]] on September 21.  Payne however was unaware of these activities.<ref name=Rea200>Rea, p. 200</ref>

Pollock used commissioning authority granted him by Congress to give command of the ''Morris'' to Continental Navy Captain [[William Pickles (American Revolution)|William Pickles]].<ref>Ellis, p. 51</ref>  However, she was destroyed in a hurricane (which also delayed the departure of Gálvez' expedition), and Gálvez provided another ship for Pickles' use, variously called {{USS|Morris|1779|2}} or "''Morris''{{'s}} [[Ship's tender|tender]]".<ref>Allen, p. 393</ref>  According to the report of Lieutenant Peter George Rousseau, Pickles' second-in-command, this ship was a [[schooner]] armed with five small (2.5 pound or less) [[cannon]]s and ten [[swivel gun]]s, and that it lacked barricades to protect the men on deck from gunfire.  Furthermore, the crew was not otherwise well prepared for close action, lacking axes, lances, and other tools useful for a boarding action.<ref name=Ellis54>Ellis, p. 54</ref>  Pollock instructed Pickles to harass British military shipping on the lake, which had recently increased in activity.<ref>James, p. 199</ref>

==Battle==
Pickles sailed from New Orleans with a crew of 57 Americans and Spaniards (Rousseau, his second in command, was a Frenchman commissioned into the Continental Navy).<ref name=Ellis54/><ref name=M14/>  To hide his intentions, Pickles flew a British [[ensign]] as a [[false flag]].  Spotted on September 10, the two ships closed, and Payne hailed the ''Morris'' to discover her intentions.  He was told she was a merchant bound for [[Pensacola]] shortly before Pickles had the false colors hauled down and replaced with an American flag.  The Americans and Spaniards then threw grappling hooks to bring the ships together and opened fire with their swivel guns while Lieutenant Rousseau prepared a boarding party.<ref name=M14>Martinez, p. 14</ref>  It is unlikely that either ship fired its larger guns.<ref name=Rea200/>

[[Image:Mandeville Battle of Lake Pontchartrain plaque.JPG|thumb|right|Historic marker in [[Mandeville, Louisiana]]]]
Payne's small crew put up spirited resistance, twice repulsing the boarders.  The third boarding attempt succeeded, and Payne himself went down with a mortal wound in fighting described as "very violent".<ref name=Rea200/><ref name=M14/>  The boarders successfully overwhelmed the British, wounding two men in addition to Payne, while suffering 6 to 8 killed and several wounded.<ref name=Rea200/>

==Aftermath==
Captain Pickles took the prize back to New Orleans, where Pollock had her fitted out.  Pickles cruised with her in West Florida's waters during Governor Gálvez's march up the Mississippi.  Pickles then assisted Gálvez in the [[Battle of Fort Charlotte]], which resulted in the capture of [[Mobile, Alabama|Mobile]], before sailing her to Philadelphia for sale.<ref>Allen, p. 394</ref>

The battle is commemorated by a historic marker in [[Mandeville, Louisiana]].<ref>{{cite news |title=Group restores marker dedicated to Battle of Lake Pontchartrain |author=Tara McLellan |date=2008-11-14 |newspaper=[[The Times-Picayune]] |accessdate=2009-12-21}}</ref> The marker credits William Pickles with ending the Revolutionary War in Louisiana, since some British individuals on the north shore of Lake Pontchartrain evidently surrendered to Captain Pickles in mid-October 1779 (three weeks after the [[Battle of Baton Rouge (1779)|Battle of Baton Rouge]]). In this context, "Louisiana" means the future state of [[Louisiana]] and not the larger area of [[Louisiana (New Spain)|Spanish Louisiana]]. The [[Battle of St. Louis]], also within Spanish Louisiana, took place in 1780.

==See also==
*[[Gálveztown (brig sloop)|''Gálveztown'' (brig sloop)]] &mdash; This was the renamed HMS ''West Florida''. A replica has been built in Spain, more than two centuries later, in honor of Bernardo de Gálvez's contributions to the American Revolutionary War.

==Notes==
{{reflist|colwidth=30em}}

==References==
*{{cite book|title=A Naval History of the American Revolution, Volume 2|first=Gardner Weld|last=Allen|publisher=Houghton Mifflin|year=1913|url=https://books.google.com/books?id=R9pWRDSu7w4C&lr&pg=PA394#v=onepage&f=false|location=Boston|oclc=61923999}}
*{{cite book|last=Ellis|first=Frederick|title=St. Tammany Parish: L'Autre Cote Du Lac|publisher=Pelican Publishing|year=1999|location=Gretna, LA|isbn=978-1-56554-563-2|oclc=46673813|origyear=1981}}
*{{cite book|title=Oliver Pollock: the Life and Times of an Unknown Patriot|first=James Alton|last=James|publisher=Ayer Publishing|year=1937|isbn=978-0-8369-5527-9|location=Freeport, NY|oclc=101066}}
*{{cite book|url=https://books.google.com/books?id=x2CevkQ720gC&pg=PA329&cd=2#v=onepage&f=false|first=Henry|last=Laurens|title=The Papers of Henry Laurens, Volume 15|publisher=University of South Carolina Press|year=1968|isbn=978-1-57003-307-0|location=Columbia, SC|oclc=313782776|display-authors=etal}}
*{{cite journal|title=The Western Fringe of Revolution|first=Lawrence|last=Kinnaird|journal=The Western Historical Quarterly|volume= 7|issue= 3|date= July 1976|pages=253–270|publisher=The Western History Association|jstor=967081}} (subscription required for JSTOR)
*{{cite book|title=Rousseau: The Last Days of Spanish New Orleans|first=Raymond J|last=Martinez|publisher=Pelican Publishing|year=2003|isbn=978-1-58980-089-2|location=Gretna, LA|oclc=52562285}}
*{{cite journal |first=Robert|last=Rea |year= 1981|title=Florida and the Royal Navy's Floridas |journal=Florida Historical Quarterly |volume= 6 |issue= 2 |pages=186–203 |jstor=30146768 |publisher=Florida Historical Society}} (subscription required for JSTOR)
{{Coord|30|11|20|N|90|06|05|W|type:waterbody_region:US_scale:500000|display=title}}
{{DEFAULTSORT:Battle Of Lake Pontchartrain}}

[[Category:United States Marine Corps in the 18th and 19th centuries]]
[[Category:Naval battles of the Anglo-Spanish War (1779–83)|Lake Pontchartrain]]
[[Category:Naval battles involving Spain|Lake Pontchartrain]]
[[Category:Naval battles involving the United States|Lake Pontchartrain]]
[[Category:Naval battles involving Great Britain|Lake Pontchartrain]]
[[Category:Conflicts in 1779]]
[[Category:1779 in the United States]]
[[Category:Louisiana in the American Revolution]]
[[Category:September 1779 events]]