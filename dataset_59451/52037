{{Infobox journal
| title = Journal of Product Innovation Management
| cover = 
| editor = Gloria Barczak
| abbreviation = J. Prod. Innovat. Manag.
| discipline = [[Business]], [[management]]
| publisher = [[Wiley-Blackwell]] on behalf of the [[Product Development and Management Association]]
| country =
| frequency = Bimonthly
| history = 1984-present
| openaccess =
| impact = 1.379
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5885
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5885/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5885/issues
| link2-name = online archive
| CODEN = JPI?DD
| LCCN = 84644704
| OCLC = 09425135
| ISSN = 0737-6782
| eISSN = 1540-5885
}}
The '''''Journal of Product Innovation Management''''' is a bimonthly [[peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the [[Product Development and Management Association]]. The current [[editor-in-chief]] is Gloria Barczak ([[Northeastern University]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.696, ranking it 12th out of 43 journals in the category "Engineering, Industrial",<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Industrial |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> 44th out of 111 journals in the category "Business",<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Business |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref> and 64th out of 185 journals in the category "Management".<ref name=WoS3>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Management |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

The journal received an "A" ranking as a marketing journal by 1100 business scholars in Germany, Austria, and Switzerland.<ref>{{cite web |url=http://vhbonline.org/service/jourqual/vhb-jourqual-3/teilrating-mark/ |title=Teilrating MARK |publisher=Verband der Hochschullehrer für Betriebswirtschaft |work=vhbonline.org |accessdate=22 April 2015}}</ref>

In 2014 the Journal received 471 manuscripts, desk rejected 59% of submissions, and Accepted 6–10%  of the submissions. The top 10 countries submitting papers were: USA (15.5%), UK (11.5%), Germany (9.3%), China (8.1%), Taiwan (7.6%), India (4.9%), Spain and Italy (3.8%), The Netherlands (3.2%), and France (3%).<ref>Barczak, G. (2015), JPIM 2014: The Year in Review. Journal of Product Innovation Management, 32: 308. doi: 10.1111/jpim.12264</ref>

==Controversy==

In 2012, the journal published a study that ranked the [[Henry W. Bloch School of Management]] ([[University of Missouri-Kansas City]]) number 1 in the world for research in [[innovation management]].<ref>{{cite journal |doi=10.1111/j.1540-5885.2011.00898.x |title=Perspective: Ranking of the World's Top Innovation Management Scholars and Universities |journal=Journal of Product Innovation Management |volume=29 |issue=2 |pages=319 |year=2012 |last1=Yang |first1=Pianpian |last2=Tao |first2=Lei}}</ref><ref>{{cite web |url=http://www.umsystem.edu/stories/innovation_management_research |title=UMKC Ranks Number One for Innovation Management Research |publisher=[[University of Missouri]] |accessdate=22 March 2015}}</ref> However, the methodology of the study and the independence of its authors was questioned.<ref>{{cite web |url=http://www.kansascity.com/news/local/article811395.html |title=UMKC's misleading march to the top |work=[[Kansas City Star]] |accessdate=22 March 2015}}</ref><ref>{{cite web |url=http://chronicle.com/blogs/ticker/for-business-schools-no-1-ranking-big-asterisk-looms/82737?cid=at&utm_source=at&utm_medium=en |title=For Business School's No. 1 Ranking, Big Asterisk Looms – The Ticker - Blogs |work=[[The Chronicle of Higher Education]] |accessdate=22 March 2015}}</ref> In March, 2015, the journal published an "expression of concern" regarding the study. However, four independent scholars had later reviewed the article and found its methodology to be acceptable.<ref>{{cite web |url=http://www.kansascity.com/news/local/article15170036.html |title=Journal expresses concern about study that ranked UMKC No. 1 |work=[[Kansas City Star]] |accessdate=22 March 2015}}</ref>

==References==
{{Reflist|30em}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5885}}

[[Category:Wiley-Blackwell academic journals]]
[[Category:Business and management journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1984]]
[[Category:English-language journals]]