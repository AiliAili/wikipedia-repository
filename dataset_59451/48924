{{Use dmy dates|date=September 2012}}
{{Use Australian English|date=September 2012}}
'''Raymond Alfredo Daniel Jones''' (born 18 July 1925) is an Australian [[Modern architecture|Modernist]] architect.<ref name="RJ" /> His work includes many building types, including residential, ecclesiastical, educational, commercial, and prefabricated kit buildings.<ref name="RJ">{{cite book |last= Anderson|first= Simon |title= RAYMOND JONES Architectural Projects: Experiments in Space, Structure and Environmental Design: The Life and Architecture of Raymond Jones |publisher= Vanguard Press |date=March 2011 |isbn= 978-0-646-55288-0}}</ref>

Like his mentor [[Robin Boyd]],<ref name="RJ" /> Jones has continued to experiment with ideas of space and environmental design. His architectural works promote passive environmental systems, and has become a leader in the implementation of sustainable design practices in [[Western Australia]].<ref name="RJ" />

==History==
Raymond Jones was born in [[Geelong]], Victoria, Australia on 18 July 1925.<ref name="RJ" />

From 1943 to 1945, Jones joined the [[Royal Australian Navy]], and was aboard the cruiser {{HMAS|Australia|D84|6}} during the [[Philippines Campaign (1944–45)|Philippines Campaign]].<ref name="RJ" />

In 1946, Jones began his architectural studies at the [[University of Melbourne]], and was taught by Robin Boyd, [[Roy Grounds]], John Mockridge, and [[Frederick Romberg]].<ref name="RJ" /> Jones took a particular interest in the works of Boyd, particularly in relation to Boyd's ideas of spatial continuity between building and landscape, as well as his environmental design philosophy.<ref name="RJ" />

Upon completion of his architectural degree in 1951, Jones worked for [[Melbourne]] architectural firm, Yuncken, Freeman Brothers, Griffiths & Simpson<ref name="RJ" /> (now known as [[Yuncken Freeman]]). At the same time, Jones became part of the design team that won a competition to design an Olympic stadium in [[Carlton, Victoria]], as well as housing for the new town of [[Eildon, Victoria]], which had been constructed to support the construction of [[Lake Eildon]].<ref name="RJ" /><ref name="UWA">[http://www.news.uwa.edu.au/201103223401/arts-and-culture/pioneering-ecological-architect-celebrated/ "Pioneering ecological architect celebrated."], ''University of Western Australia'', 22 March 2011</ref>

During his studies, Jones played [[Australian rules football]] for [[Collingwood Football Club|Collingwood]] and [[Melbourne Football Club|Melbourne]] from 1946 to 1949.<ref name="RJ" /> In 1953, he later played in a premiership with the [[South Fremantle Football Club]].<ref name="RJ" />

Jones moved to Western Australia in 1953 and began his own practice in [[Perth, Western Australia|Perth]].<ref name="RJ" /><ref name="VB" /> During the 1950s Jones had an increased number of staff, ranging from young architects and students, which included; Wallace Greenham, Walter Hunter, Robert Hart, Ken Yewers, and Michael Patroni.<ref name="RJ" />

From 1966 to 1967, Jones had practiced in partnership with Montague Grant, going by the name "Jones Grant Architects".<ref name="RJ" />

His first significant architectural project was his own residence, the 'Jones House', in Haining Avenue, [[Cottesloe]],<ref name="RJ" /><ref name="WTB" /><ref name="LAP">{{cite book |last= Molyneux |first= Ian |title= Looking Around Perth |publisher= Wescolour Press, East Fremantle |year= 1981}}</ref> where Jones heavily references the architectural teachings of Boyd<ref name="RJ" /> Jones begins to experiment with space and structure in responding to the environment, through passive ventilation, northern glazing and thermal massing<ref name="RJ" />

Jones is also noted for his many ecclesiastical projects, and completed a total of six churches for the Catholic Church. All of his churches were inspired by cave-like [[catacombs]], a common metaphor he used in the design of communal spaces.<ref name="RJ" /> The Church of St Cecilia, in Kenmore Crescent [[Floreat]],<ref name="RJ" /><ref name="LAP" /> references this cave-like metaphor, and departs from the typical [[cruciform]] plan. Instead, St. Cecilia's pentagonal form was based on a total of ten planes (five walls planes and five roof planes), which symbolised the [[Ten Commandments]] and the sacrifice of [[Jesus Christ]].<ref name="RJ" />

Jones' educational projects, particularly his work at Preschool Centre in Stirling Highway, North Fremantle;<ref name="RJ" /> New Day Nursery in High Street/Parry Street, Fremantle;<ref name="RJ" /> and 'Winterfold Primary School', in Annie Street, Hamilton Hill,<ref name="RJ" /> have been designed to enable variable use, through the use of large internal and external centralised spaces.

This relationship between space and environmental design culminates in his development of the Tetrakit system.<ref name="RJ" /> Working in the cyclonic areas of north-western Australia in the aftermath of [[Cyclone Tracy]] in 1974, Jones with the help of structural engineer George Katieva, devised prefabricated kit homes. Constructed of prefabricated frames and panels, the Tetrakit kit home would resist the strong wind pressures during a cyclone. Roofs and walls inclined at 15 degrees balanced the cyclonic winds loads placed on the building.<ref name="RJ" />

==Contribution==
His interests and passions in ecological design continued, particularly through the use of [[Mono-pitched roof|skillion]] roofing and courtyard spaces,<ref name="RJ" /> as well as passive ventilation and site orientation, as environmental design strategies.<ref name="UWA" /> Jones is also attributed to the innovative use of concrete raft slabs and swimming pools to suburban housing, in controlling interior temperatures.<ref name="RJ" /> Climate also begun to inform Jones' approach to architecture, experimenting with how architecture is best suited to its particular site and environment.<ref name="RJ" />

==Current thoughts on architecture==
Jones has been a lifelong critic of active heating and cooling systems, particularly mechanical air conditioning,<ref name="SB">Bevis, Stephen, [http://au.news.yahoo.com/thewest/a/-/breaking/9114971/no-need-for-home-air-con-says-top-architect/ "No need for home air-con, says top architect"], ''The West Australian'', 1 April 2011</ref> and always strived to create buildings which rely on more passive systems. He states that, "We ought to legislate that there is no air-conditioning in new homes and that a building license should only be issued if they build along renewable energy principles. You can do it well without building expensively".<ref name="SB" />

Jones is not impressed by a lot of modern architecture in Perth, saying he's depressed by the "proliferation of ugliness".<ref name="VB">Buck, Veronica, [http://www.abc.net.au/news/stories/2011/03/23/3171956.htm "Ahead of the pack in architectural thinking"], ''ABC News'', 24 March 2011</ref> "We are just not going forward at all, we are going backward".<ref name="VB" /> He blames Perth's ugly, impractical and unsustainable modern homes on the laziness and expediency of politicians, city planners and mass-marketed project homes.<ref name="SB" /> Mr Jones says he is a crusader for sensible, environmentally aware design.<ref name="SB" />

Jones is still practicing as an architect as of 2011 and has no interest in quitting entirely any time soon, instead focusing on projects of interest.<ref name="VB" />

==Notable projects==
* 1952: Leonard House Glen Road, [[Malvern, Victoria]]
* 1952: RR Jones House Park Road, [[Park Orchards, Victoria]]
* 1953: Melbourne Olympic Stadium (Competition Entry), Yuncken, Freeman Brothers, Griffiths and Simpson (now known as [[Yuncken Freeman]])
* 1953: Jones House, Haining Avenue, [[Cottesloe, Western Australia]]<ref name="WTB">{{cite book | author1=Pitt Morison, Margaret | author2=White, John Graham | authorlink1=Margaret Pitt Morison | title=Western Towns and Buildings | publisher=UWA Press, Crawley |year= 1979}}</ref><ref name="LAP" />
* 1953: Williams Flats, Broome Street, [[Cottesloe, Western Australia]]
* 1954: Ruse Flats, Kanimbla Road/Karella Street, [[Nedlands, Western Australia]]
* 1954: O'Mahony House, Haining Avenue, [[Cottesloe, Western Australia]]
* 1955: Preschool Centre and Community Hall, Stirling Hwy, [[North Fremantle, Western Australia]]
* 1955: Boxhorn House, Shannon Street/Roscommon Road, [[Floreat, Western Australia]]
* 1956: McMillan House, Allenby Road, [[Dalkeith, Western Australia]]
* 1956: Powell House, Chester Street/Lloyd Street, [[South Fremantle, Western Australia]]
* 1956: Kiernan House, Allenby Road, [[Dalkeith, Western Australia]]<ref name="BAH">{{cite book |last= Clerehan |first= Neil |title= Best Australian Houses |publisher= FW Cheshire, Melbourne |year= 1961}}</ref>
* 1957: Silbert House, Barcoo Avenue/Doonan Road, [[Nedlands, Western Australia]]
* 1957: Ash House, Hammersley Street, [[Trigg, Western Australia]]
* 1958: Gerrard House, Page Street, [[Attadale, Western Australia]]
* 1959: Freedman House, Alyth Road, [[Floreat, Western Australia]]<ref name="LAP" />
* 1959: Staff Club UWA (Competition Entry), [[The University of Western Australia]]
* 1960: Church of St Cecilia, Kenmore Crescent, [[Floreat, Western Australia]]<ref name="LAP" />
* 1962: Church of St Peter, Wood Street, [[Bedford, Western Australia]]
* 1962: Lysaght Offices, Norma Road/McCoy Street, [[Myaree, Western Australia]]
* 1962: Mingenew Court House, Moore Street/William Street, [[Mingenew, Western Australia]]
* 1963: Rankine-Wilson House, The Boulevard, [[Floreat, Western Australia]]<ref name="SB" />
* 1963: Hubbard House, Oceanic Drive, [[Floreat, Western Australia]]
* 1964: Lisle House, Pindari Road, [[City Beach, Western Australia]]
* 1964: WWF Point Peron Camp, [[Point Peron]]
* 1964: Our Lady of Lourdes Memorial Church, Flinders Street, [[Nollamara, Western Australia]]
* 1965: 'New Day Nursery, High Street/Parry Street, [[Fremantle, Western Australia]]
* 1966: Carbon Duplex, Point Walter Road/ Beach Street, [[Bicton, Western Australia]]
* 1968: CIL Offices, Clontarf Road, [[Hamilton Hill, Western Australia]]
* 1969: Premier Motors, Elder Street/ Hay Street, [[Perth, Western Australia]]
* 1970: Winterfold Primary School, Annie Street, [[Hamilton Hill, Western Australia]]
* 1971: Raymond Jones House, Ainslie Road, [[North Fremantle, Western Australia]]
* 1972: Commonwealth Bank Cannington, Albany Highway, [[Cannington, Western Australia]]
* 1974: Edwards House, Lobelia Drive/ Dryandra Crescent, [[Greenmount, Western Australia]]
* 1976: Nulungu Chapel, [[Broome, Western Australia]]
* 1976: Christian Community Village, Buckland Road, Jarradale
* 1979: Parliament House (Competition Entry), [[Canberra, ACT]]
* 1980: Majestic Hotel (Competition Entry), Fraser Road, [[Applecross, Western Australia]]
* 1981: Summers House, Brand Highway (near [[Eneabba, Western Australia]])
* 1981: Webse House, Jarradale Road, Jarradale
* 1984: RAC Albany, Albany Hwy, [[Albany, Western Australia]]
* 1988: Clunies Ross House, Brindal Close, [[Bicton, Western Australia]]
* 1988/9: Tetrakit
* 1990: Brodwyn Graham House, Edina Court, [[Two Rocks, Western Australia]]
* 1991: Cann & Hicks House, Sapho Place, [[Two Rocks, Western Australia]]
* 1992: Desmond Sands Duplex, Haining Avenue, [[Cottesloe, Western Australia]]
* 2008: Tschaplin Jones House, Pensioner Guard Road, [[North Fremantle, Western Australia]]

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

{{DEFAULTSORT:Jones, Raymond}}
[[Category:Australian architects]]
[[Category:Architects from Western Australia]]
[[Category:1925 births]]
[[Category:People from Geelong]]
[[Category:People from Perth, Western Australia]]
[[Category:Australian military personnel of World War II]]
[[Category:Royal Australian Navy personnel of World War II]]
[[Category:Australian rules footballers from Victoria (Australia)]]
[[Category:Collingwood Football Club players]]
[[Category:Melbourne Football Club players]]
[[Category:South Fremantle Football Club players]]
[[Category:Living people]]