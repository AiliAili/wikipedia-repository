{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image= [[File:BenedettoBrin.jpg|300px|Benedetto Brin]]
|Ship caption=''Benedetto Brin''
}}
{{Infobox ship career
|Hide header=
|Ship country=[[Kingdom of Italy|Italy]]
|Ship flag={{shipboxflag|Kingdom of Italy}}
|Ship name= ''Benedetto Brin''
|Ship namesake=[[Benedetto Brin]]
|Ship ordered=
|Ship awarded=
|Ship builder=Castellammare Naval Shipyard
|Ship original cost=
|Ship yard number=
|Ship way number=
|Ship laid down=30 January 1899
|Ship launched=7 November 1901
|Ship sponsor=
|Ship christened=
|Ship completed=1 September 1905
|Ship fate=Destroyed by explosion 27 September 1915
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type={{sclass-|Regina Margherita|battleship|0}} [[pre-dreadnought battleship]]
|Ship displacement=*{{convert|13215|LT|t|0|abbr=on}} (standard)
*{{convert|14737|LT|t|0|abbr=on}} (full load)
|Ship length={{convert|138.65|m|ftin|abbr=on|0}}
|Ship beam={{convert|23.84|m|ftin|abbr=on|0}}
|Ship draft={{convert|9|m|ftin|abbr=on|0}}
|Ship propulsion=2 shafts, [[Marine steam engine#Triple or multiple expansion|triple expansion steam engines]], 28 boilers
|Ship speed={{convert|20|knots|lk=in|0}}
|Ship range={{convert|10000|nmi|lk=in|abbr=on|0}} at {{convert|10|knots|abbr=on|0}}
|Ship complement=812/900
|Ship power={{convert|20475|ihp|0|abbr=on}}
|Ship armament=*2 × 2 - [[Armstrong Whitworth 12 inch /40 naval gun|{{convert|305|mm|in|0|abbr=on}}/40 guns]]
*4 × 1 - {{convert|203|mm|in|0|abbr=on}}/40 guns
*12 × 1 - {{convert|152|mm|in|0|abbr=on}}/40 guns
*20 × 1 - {{convert|76|mm|in|0|abbr=on}}/40 guns
*2 × 1 - {{convert|47|mm|in|1|abbr=on}}/40 guns
*2 × 1 - {{convert|37|mm|in|1|abbr=on}}/40 guns
*4 × {{convert|450|mm|in|abbr=on|1}} [[torpedo tube]]s
|Ship armor=*[[Harvey armor]]
*[[Belt armor|Belt]] and side: {{convert|6|in|mm|0|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|3.1|in|mm|1|abbr=on}}
*[[Turret]]s: {{convert|8|in|mm|0|abbr=on}}
*[[Conning tower]]: {{convert|6|in|mm|0|abbr=on}}
*[[Casemate]]s: {{convert|6|in|mm|0|abbr=on}}
|Ship notes=
}}
|}
'''''Benedetto Brin''''' was a {{sclass-|Regina Margherita|battleship|0}} [[pre-dreadnought battleship]] built for the Italian ''[[Regia Marina]]'' between 1899 and 1905. The ship was armed with a main battery of four {{convert|12|in|adj=on}} guns and was capable of a top speed of {{convert|20|kn|lk=in}}. ''Benedetto Brin'' saw combat in the [[Italo-Turkish War]] of 1911&ndash;1912, including the bombardment of [[Tripoli]] in October 1911. She was destroyed by an internal explosion during [[World War I]] in September 1915, which killed over 450 of the ship's crew.

==Design==
[[File:Regina Margherita line-drawing.png|thumb|left|Line-drawing of the ''Regina Margherita'' class]]
{{main|Regina Margherita-class battleship}}

''Benedetto Brin'' was {{convert|138.65|m|sp=us|0}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|23.84|m|abbr=on|0}} and a [[draft (hull)|draft]] of {{convert|9|m|abbr=on|0}}. She displaced {{convert|14737|MT|0}} at full combat load. Her propulsion system consisted of two [[triple expansion engine]]s. Steam for the engines was provided by twenty-eight coal-fired [[Belleville boiler]]s. The ship's propulsion system provided a top speed of {{convert|20|kn|0}} and a range of approximately {{convert|10000|nmi|lk=in}} at {{convert|10|kn|0}}. ''Benedetto Brin'' had a crew of 812&nbsp;officers and enlisted men.<ref name=G343>Gardiner, p. 343</ref>

As built, the ship was armed with four {{convert|12|in|abbr=on|0}} 40-[[Caliber (artillery)|caliber]] guns placed in two twin [[gun turret]]s, one forward and one aft. The ship was also equipped with four {{convert|8|in|abbr=on|0}} 40-cal. guns in [[casemate]]s in the [[superstructure]], and twelve {{convert|6|in|abbr=on|0}} 40-cal. guns, also in casemates in the side of the hull. Close-range defense against torpedo boats was provided by a battery of twenty {{convert|3|in|abbr=on|0}} 40-cal. guns. The ship also carried a pair of {{convert|47|mm|abbr=on}} guns, two {{convert|37|mm|abbr=on}} guns, and two {{convert|10|mm|abbr=on|1}} [[Maxim gun]]s. ''Benedetto Brin'' was also equipped with four {{convert|17.7|in|abbr=on|0}} [[torpedo tube]]s placed in the hull below the waterline. The ship was protected with [[Harvey armor|Harvey steel]] manufactured in [[Terni]]. The [[belt armor|main belt]] was {{convert|6|in|abbr=on|0}} thick, and the deck was {{convert|3.1|in|abbr=on|0}} thick. The [[conning tower]] and the casemate guns were also protected by 6&nbsp;in of armor plating. The main battery guns had stronger armor protection, at {{convert|8|in|abbr=on|0}} thick.<ref name=G343/>

==Service==
The ship was built by the [[Castellammare di Stabia]] shipyard. Her [[keel]] was laid down on 30 January 1899, and the completed hull was launched on 7 November 1901 in the presence of the King and Queen of Italy, government officials, and the whole Italian Mediterranean squadron.<ref>{{Cite newspaper The Times |articlename=Naval & Military intelligence |day_of_week=Friday |date=8 November 1901 |page_number=6 |issue=36607| }}</ref> [[Fitting out]] work lasted for the next four years, and she was completed on 1 September 1905.<ref name=G343/> It took so long primarily because of non-delivery of material, particularly the heavy armor.<ref>''Journal of the Royal United Service Institution'', p. 1070</ref> After she entered active service, the ship was assigned to the Mediterranean Squadron.<ref>''Journal of the Royal United Service Institution'', p. 1069</ref> The Squadron was usually only activated for seven months of the year in peacetime, which was occupied with training maneuvers, and the rest of the year the ships were placed in reserve. In 1907, the Mediterranean Squadron consisted of ''Benedetto Brin'', her [[sister ship|sister]] {{ship|Italian battleship|Regina Margherita||2}}, and three of the {{sclass-|Regina Elena|battleship|1}}s.<ref>Brassey 1908, p. 52</ref> The ships participated in the annual maneuvers in late September and early October, under the command of Vice Admiral [[Alfonso di Brocchetti]].<ref>Brassey 1908, pp. 77&ndash;78</ref> ''Benedetto Brin'' remained in the active duty squadron through 1910, by which time the fourth ''Regina Elena''-class ship was completed, bringing the total number of front-line battleships to six.<ref>Brassey 1911, p. 56</ref><ref group=Note>These were all [[pre-dreadnought battleship]]s, and were thus obsolescent by this period, but Italy's first [[dreadnought]], {{ship|Italian battleship|Dante Alighieri||2}}, did not enter service until 1913. See: Gardiner & Gray, p. 259</ref>

===Italo-Turkish War===
[[File:Benedetto Brin.png|thumb|''Benedetto Brin'' steaming at high speed]]
On 29 September 1911, Italy declared war on the [[Ottoman Empire]] in order to seize [[Libya]].<ref>Beehler, p. 6</ref> During the [[Italo-Turkish War]] ''Benedetto Brin'' was assigned to the 1st Division of the 2nd Squadron, along with her sister and the two {{sclass-|Ammiraglio di Saint Bon|battleship|1}}s.<ref>Earle, p. 1385</ref> ''Benedetto Brin'' served as the squadron flagship of Vice Admiral Farvelli.<ref>Beehler, p. 9</ref> In early October, she arrived off [[Tripoli]] to relieve {{ship|Italian battleship|Roma|1907|2}} on blockade duty outside the port. On 3&ndash;4 October, she participated in the bombardment of the fortifications protecting Tripoli. The Italian fleet used their medium-caliber guns to preserve their ammunition for the heavy guns. Turkish counter-battery fire was completely ineffective.<ref>Beehler, p. 19</ref>

On 13 April 1912, ''Benedetto Brin'' and the rest of the Squadron sailed from [[Tobruk]] to the [[Aegean Sea]] to rendezvous with the 1st Squadron. The two squadrons met off [[Stampalia]] on 17 April. The next day, the fleet steamed into the northern Aegean and cut several Turkish [[Submarine communications cable|submarine telegraph cables]].<ref>Beehler, p. 67</ref> Most of the ships of the Italian fleet then bombarded the fortresses protecting the [[Dardanelles]] in an unsuccessful attempt to lure out the Turkish fleet. While they were doing this, ''Regina Margherita'', ''Benedetto Brin'', and two [[torpedo boat]]s were detached to cut additional cables between [[Rhodes]] and [[Marmaris]].<ref>Beehler, pp. 67&ndash;68</ref> In July, ''Benedetto Brin'' and the rest of the division had withdrawn to Italy to replace worn-out gun barrels, along with other repairs.<ref>Beehler, p. 87</ref> Also in 1912, the ship had four 3-inch guns added, increasing her battery from 20 to 24 pieces.<ref>Gardiner & Gray, p. 256</ref>

===World War I===
Italy declared neutrality after the outbreak of [[World War I]] in August 1914, but by April 1915, the [[Triple Entente]] had convinced the Italians to enter the war against the [[Central Powers]] which it did in May.<ref>Halpern, p. 140</ref> The primary naval opponent for the duration of the war was the [[Austro-Hungarian Navy]]; the Naval Chief of Staff, Admiral [[Paolo Thaon di Revel]], planned a distant blockade with the battle fleet, while smaller vessels, such as the [[MAS (boat)|MAS boats]] conducted raids. The heavy ships of the Italian fleet would be preserved for a potential major battle in the Austro-Hungarian fleet should emerge from its bases.<ref>Halpern, pp. 141&ndash;142</ref> As a result, the ship's career during the war was limited. In addition to the cautious Italian strategy, ''Benedetto Brin''&mdash;long-since obsolescent&mdash;was reduced to a training ship in the 3rd Division, along with her sister ship.<ref>''The New International Encyclopedia'', p. 469</ref> On 27 September 1915, ''Benedetto Brin'' was destroyed in a huge explosion in the harbor of [[Brindisi]]; at the time, it was believed to have been the result of Austro-Hungarian sabotage.<ref name=G343/> The Italian Navy now believes the explosion to have been accidental.<ref>{{cite web|url=http://www.marina.difesa.it/storiacultura/storia/accaddeil/Pagine/1915_09_27.aspx|title=Le “due vite” della nave da battaglia Benedetto Brin|trans-title=The "Two Lives" of the Battleship Benedetto Brin|language=Italian|accessdate=12 January 2017|publisher=Ministero Della Difesa}}</ref> A total of 8 officers and 379 ratings survived but 454 members of the crew, including Rear-Admiral [[Rubin de Cervin]] died.<ref>Hocking, p. 79</ref> Two of the ship's 12-inch guns were salvaged from the wreck and were reused as coastal guns protecting [[Venice]].<ref>O'Hara, Dickson, & Worth, p. 203</ref>

==Footnotes==
{{Portal|Battleships}}
;Notes
{{reflist|group=Note}}

;Citations
{{Reflist|colwidth=20em}}

==References==
*{{Cite book |last=Beehler|first=William Henry|title=The History of the Italian-Turkish War: September 29, 1911, to October 18, 1912|year=1913|location=Annapolis, MD|publisher=United States Naval Institute|url=https://books.google.com/books?id=OWcoAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false}}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | authorlink = Thomas Brassey, 1st Earl Brassey | year = 1908 | journal = [[Brassey's Naval Annual]] | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | authorlink = Thomas Brassey, 1st Earl Brassey | year = 1911 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal|editor-last=Earle|editor-first=Ralph|journal=United States Naval Institute Proceedings|date=March 1913|volume=39|number=1|publisher=US Naval Institute|location=Annapolis, MD}}
* {{Cite book |editor-last=Gardiner|editor-first=Robert|title=Conway's All the World's Fighting Ships: 1860-1905|year=1979|location=Annapolis|publisher=Conway Maritime Press|isbn=0-85177-133-5}}
* {{cite book | editor1-last = Gardiner | editor1-first = Robert | editor2-last = Gray | editor2-first = Randal | title = Conway's All the World's Fighting Ships: 1906–1922 | year = 1984 | location = Annapolis, MD | publisher = [[Naval Institute Press]] | isbn = 978-0-87021-907-8 }}
*{{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, MD|publisher=Naval Institute Press|isbn=1-55750-352-4}}
*{{Cite book|last=Hocking|first=Charles|title=Dictionary of Disasters at Sea During The Age of Steam|publisher=The London Stamp Exchange|location=London|year=1990|isbn=0-948130-68-7}}
*{{Cite book|last1=O'Hara|first1=Vincent|last2=Dickson|first2=David|last3=Worth|first3=Richard|title=To Crown the Waves: The Great Navies of the First World War|publisher=Naval Institute Press|location=Annapolis, MD|year=2013|isbn=978-1-61251-082-8}}
* {{cite journal|journal=The New International Encyclopaedia|year=1922|volume=XII|publisher=Dodd Mead & Co.|location=New York, NY}}

==Further reading==
*{{cite book|last=Faccaroli|first=Aldo |title=Italian Warships of World War I|location=London|publisher=Ian Allan|year=1970|isbn=978-0-7110-0105-3}}

{{Regina Margherita class battleship}}
{{September 1915 shipwrecks}}
{{Use dmy dates|date=February 2011}}

{{DEFAULTSORT:Benedetto Brin}}
[[Category:Regina Margherita-class battleships]]
[[Category:World War I battleships of Italy]]
[[Category:Maritime incidents in 1915]]
[[Category:Ships sunk by non-combat internal explosions]]
[[Category:World War I shipwrecks in the Adriatic]]
[[Category:Shipwrecks of Italy]]
[[Category:1905 ships]]
[[Category:Ships built in Castellammare di Stabia]]