{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = William Sidebottom
| image         =
| caption       =
| birth_date    = {{birth date|df=yes|1893|10|11}}
| death_date    = {{Death date and age|df=yes|1920|12|8|1893|10|11}}
| birth_place   = Manchester, England
| death_place   = near [[Rostamabad, Gilan|Rostamabad]], [[Persia]]
| placeofburial_label = Commemorated at
| placeofburial = [[Tehran Memorial]], Iran 
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = United Kingdom
| branch        = Royal Navy<br/>Royal Air Force
| serviceyears  = 1917–1920
| rank          = Lieutenant
| unit          = No. 3 (Naval) Squadron RNAS<br/>[[No. 203 Squadron RAF]]<br/>[[No. 30 Squadron RAF]]
| commands      =
| battles       = World War I<br/>{{*}}[[Western Front (World War I)|Western Front]]
| awards        = [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
| relations     =
| laterwork     =
}}
Lieutenant '''William Sidebottom''' {{post-nominals|country=GBR|DFC}} (11 October 1893 – 8 December 1920) was a British World War I [[flying ace]] credited with [[List of World War I aces credited with 11–14 victories|fourteen aerial victories]].<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/sidebottom.php |title=William Sidebottom |work=The Aerodrome |year=2016 |accessdate=26 January 2016}}</ref>{{sfnp|Shores|Franks|Guest|1990|p=338}}

==Military service==
Sidebottom joined the [[Royal Naval Air Service]] on 11 October 1917,{{sfnp|Shores|Franks|Guest|1990|p=338}} and after completing his flying training was posted the No. 3 (Naval) Squadron to fly the [[Sopwith Camel]] single-seat fighter. On 1 April 1918, the RNAS was merged with the Army's [[Royal Flying Corps]] to form the Royal Air Force, and Sidebottom's unit was renamed No. 203 Squadron RAF.{{sfnp|Shores|Franks|Guest|1990|p=338}} 

He scored his first win on 16 June 1918, sharing the destruction of a [[Deutsche Flugzeug-Werke|DFW]] two-seater reconnaissance aircraft with Lieutenant [[Edwin Hayne]] and three other pilots. He then accumulated a series of victories between then and 29 October 1918, sharing in the destruction of two reconnaissance aircraft with Captain [[Leonard Henry Rochford]] and the mid-air burning of another with Captain [[Arthur Whealy]]. Sidebottom's final toll was the destruction of seven enemy aircraft, with five of those shared; seven driven down out of control, including two shared.<ref name="theaerodrome"/>{{sfnp|Shores|Franks|Guest|1990|p=338}}

==Post-war career and death==
Sidebottom was transferred to the RAF's unemployed list on 1 February 1919,<ref>{{London Gazette |date=31 October 1919 |issue=31624 |startpage=13275 |nolink=yes}}</ref> and a week later, on 7 February, his Distinguished Flying Cross was [[gazetted]]. His citation read:
:Lieutenant William Sidebottom, 203rd Squadron.
::"This officer has carried out numerous offensive and low bombing patrols with courage, skill and judgment. He has also proved himself a bold and resolute fighter in aerial combats, having nine enemy machines to his credit."<ref>{{London Gazette |date=7 February 1919 |issue=31170 |startpage=2046 |nolink=yes}}</ref>

However, Sidebottom returned to RAF service on being granted a short service commission with the rank of [[flying officer]] on 24 October 1919.<ref>{{London Gazette |date=24 October 1919 |issue=31616 |startpage=13032 |endpage=13034 |nolink=yes}}</ref> On 8 December 1920 Sidebottom was serving in No. 30 Squadron, part of the [[North Persia Force]]. While flying an [[Airco DH.9A]] on a bombing mission on [[Enzeli]], then part of the [[Persian Socialist Soviet Republic]], he made a forced landing {{Convert|15|mi}} from [[Rostamabad, Gilan|Rostamabad]], and was shot and killed by Bolsheviks while trying to escape. His observer escaped unhurt.<ref>{{cite web |url= http://www.rafmuseumstoryvault.org.uk/archive/sidebottom-w.-william |title=Sidebottom, William: Casualty Card |work=RAF Museum Storyvault |year=2016 |accessdate=25 January 2016}}</ref><ref>{{cite web |url=http://www.rafweb.org/Members%20Pages/Casualties/1920s/Casualties_1920_K-Z.htm |title=RAF Casualties 1920 (K–Z) |first=M. B. |last=Barrass |work=Air of Authority – A History of RAF Organisation |accessdate=26 January 2016}}</ref> Having no known grave he is commemorated on the [[Tehran Memorial]], [[Gholhak Garden]], Iran.<ref>{{cite web |url=http://www.cwgc.org/find-war-dead/casualty/1520049/SIDEBOTTOM,%20WILLIAM |title=Casualty Details: Sidebottom, William |work=[[Commonwealth War Graves Commission]] |year=2016 |accessdate=26 January 2016}}</ref>

==References==
;Notes
{{reflist|30em}}
;Bibliography
* {{cite book |ref=harv |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell F. |last3=Guest |title=Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=978-0-948817-19-9 |lastauthoramp=yes}}

{{DEFAULTSORT:Sidebottom, William}}
[[Category:1893 births]]
[[Category:1920 deaths]]
[[Category:People from Manchester]]
[[Category:Royal Naval Air Service aviators]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:British military personnel killed in action]]