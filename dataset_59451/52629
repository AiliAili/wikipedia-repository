{{Infobox journal
| title = Proceedings of the Institution of Mechanical Engineers, Part A: Journal of Power and Energy
| cover = [[File:Proceedings of the IMechE - A - journal cover.jpg]]
| editors = Richard C. Stone 
| discipline = [[Mechanical engineering]]
| former_names =
| abbreviation = Proc. Inst. Mech. Eng. A J. Power Energy
| publisher = [[Sage Publications]] on behalf of the [[Institution of Mechanical Engineers]]
| country =
| frequency = 8/year
| history = 1990-present
| openaccess = 
| license = 
| impact = 0.596 
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal202015/title
| link1 = http://pia.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pia.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 21531945
| LCCN = 93657487
| CODEN = PMAEET
| ISSN = 0957-6509
| eISSN = 2041-2967
}}
The '''''Proceedings of the Institution of Mechanical Engineers, Part A: Journal of Power and Energy''''' is a [[peer-reviewed]] [[scientific journal]] that covers research on the technology of [[energy conversion systems]]. The journal was established in 1990 and is published by [[Sage Publications]] on behalf of the [[Institution of Mechanical Engineers]].<ref>{{cite web |url=http://www.imeche.org/knowledge/publications |title=Publications |publisher=Institution of Mechanical Engineers |date= |accessdate=2014-01-23}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed by:
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Current Contents]]/Engineering, Computing & Technology
* [[Applied Science & Technology Index]]
* [[Civil Engineering Abstracts]]
* [[Engineered Materials Abstracts]]
* [[Fluidex]]
* [[Global Mobility Database]]
* [[Inspec]]
* [[Mechanical Engineering Abstracts]]
* [[Metals Abstracts]]/[[METADEX]]
}}
According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.596, ranking it 91st out of 126 journals in the category "Engineering, Mechanical".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Mechanical |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal202015/title}}
{{Institution of Mechanical Engineers}}
[[Category:Energy and fuel journals]]
[[Category:English-language journals]]
[[Category:Institution of Mechanical Engineers academic journals]]
[[Category:Publications established in 1990]]
[[Category:SAGE Publications academic journals]]