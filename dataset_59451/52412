{{Infobox journal
| title         = New Media & Society
| cover         = [[File:New media & Society.jpg]]
| editor        = Nicholas Jankowski and Steve Jones             
| discipline    = [[Sociology]]
| peer-reviewed = 
| language      = 
| former_names  =    
| abbreviation  = New Media & Soc.
| publisher     = [[SAGE Publications]]
| country       = 
| frequency     = 8 Times/ Year
| history       = 1999-present
| openaccess    = 
| license       = 
| impact        = 3.110
| impact-year   = 2016
| website       = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200834
| link1         = http://nms.sagepub.com/content/current
| link1-name    = Online access
| link2         = http://nms.sagepub.com/content/by/year
| link2-name    = Online archive
| JSTOR         = 
| OCLC          = 41428149
| LCCN          = 00212321  
| CODEN         = 
| ISSN          = 1461-4448  
| eISSN         = 1461-7315 
| boxwidth      = 
}}

'''''New Media & Society''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[communication]]. The journal's [[Editing|editors]] are Nicholas Jankowski and Steve Jones. It has been in publication since 1999 and is currently published by [[SAGE Publications]].

== Scope ==
''New Media & Society'' publishes research from [[communication]], [[Mass media|media]] and [[cultural studies]].  The international journal is based on research that explores the relationship between theory, policy and practice. ''New media & society'' engages in discussions of the issues arising from the scale and speed of new media development, drawing from disciplinary perspectives and on both theoretical and empirical research.

== Abstracting and indexing ==
''New Media & Society'' is abstracted and indexed in, among other databases: [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 1.091, ranking it 18 out of 67 journals in the category ‘Communication’.<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Communication|title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://nms.sagepub.com/}}

{{DEFAULTSORT:New Media and Society}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]