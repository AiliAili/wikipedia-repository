{{cleanup|date=December 2009}}
'''History of Programming Languages''' ('''HOPL''') is an infrequent [[Association for Computing Machinery|ACM]] [[SIGPLAN]] conference. Past conferences were held in 1978, 1993, and 2007.

== HOPL I ==
HOPL I was held June 1&ndash;3, 1978 in [[Los Angeles, California]].  [[Jean E. Sammet]] was both the General and Program Committee Chair.  [[John A. N. Lee]] was the Administrative Chair.  [[Richard L. Wexelblat]] was the Proceedings Chair.  From Jean Sammet's introduction:  The HOPL Conference "is intended to consider the technical factors which influenced the development of certain selected programming languages."  The languages and presentations in the first HOPL were by invitation of the program committee. The invited languages must have been created and in use by 1967. They also must have remained in use in 1977.  Finally, they must have had considerable influence on the field of computing.

The papers and presentations went through extensive review by the program committee (and revisions by the authors), far beyond the norm for conferences and commensurate with some of the best journals in the field.{{Citation needed|date=June 2009}}  The languages (and speakers) included in HOPL-I were:

* [[ALGOL 60]] - [[Alan J. Perlis]] and [[Peter Naur]]
* [[APL programming language|APL]] - [[Adin Falkoff|Adin D. Falkoff]] and [[Kenneth E. Iverson]]
* [[APT programming language|APT]] - [[Douglas T. Ross]]
* [[BASIC]] - [[Thomas E. Kurtz]]
* [[COBOL]] - [[Jean E. Sammet]]
* [[FORTRAN]] - [[John Backus]]
* [[GPSS]] - [[Geoffrey Gordon (computer scientist)|Geoffrey Gordon]]
* [[JOSS]] - [[Charles L. Baker]]
* [[JOVIAL]] - [[Jules I. Schwartz]]
* [[Lisp programming language|LISP]] - [[John McCarthy (computer scientist)|John McCarthy]]
* [[PL/I]] - [[George Radin]]
* [[Simula|SIMULA]] - [[Kristen Nygaard]]
* [[SNOBOL]] - [[Ralph E. Griswold]]

Preprints of the proceedings were published in "SIGPLAN Notices", volume 13, number 8, August 1978.  The final proceedings, including transcripts of question and answer sessions, was published as a book in the ACM Monograph Series:  "History of Programming Languages", edited by [[Richard L. Wexelblat]]. Academic press, 1981.

== HOPL II ==
HOPL II was held April 20–23, 1993 in [[Cambridge, Massachusetts]].  [[John A. N. Lee]] was the Conference Chair and [[Jean E. Sammet]] was the Program Chair.  In contrast to HOPL I, HOPL II included both invited papers and papers submitted in response to an open call.    The scope also expanded.  Where HOPL I had only papers on the early history of languages, HOPL II solicited contributions on:
* early history of specific languages,
* evolution of a language,
* history of language features and concepts, and
* classes of languages for application-oriented languages and paradigm-oriented languages.
The submitted and invited languages must have been documented by 1982. They also must have been in use or taught by 1985.

As in HOPL I, there was a rigorous multi-stage review and revision process.  The selected papers and authors were:

* [[Monitor (synchronization)|Monitors]] and [[Concurrent Pascal]] - [[Per Brinch Hansen]]
* [[Prolog]] - [[Alain Colmerauer]] and [[Phillipe Roussel]]
* [[Icon programming language|Icon]] - [[Ralph E. Griswold]] and [[Madge T. Griswold]]
* [[Smalltalk]] - [[Alan Kay|Alan C. Kay]]
* [[ALGOL 68]] - [[Charles H. Lindsey|C. H. Lindsey]]
* [[CLU programming language|CLU]] - [[Barbara Liskov]]
* Discrete Event Simulation programming languages - [[Richard E. Nance]]
* [[Forth (programming language)|Forth]] - [[Elizabeth Rather]], [[Donald R. Colburn]], and [[Charles H. Moore]]
* [[C programming language|C]] - [[Dennis Ritchie]]
* [[FORMAC]] - [[Jean E. Sammet]]
* [[Lisp programming language|Lisp]] - [[Guy L. Steele, Jr.]] and [[Richard P. Gabriel]]
* [[C++]] - [[Bjarne Stroustrup]]
* [[Ada programming language|Ada]] - [[William A. Whitaker]]
* [[Pascal programming language|Pascal]] - [[Niklaus Wirth|N. Wirth]]

Preprints of the proceedings were published in "SIGPLAN Notices", volume 28, number 3, March 1993.  The final proceedings, including copies of the presentations and transcripts of question and answer sessions, was published as the ACM Press book [http://portal.acm.org/toc.cfm?id=154766] :  "History of Programming Languages", edited by [[Thomas J. Bergin]] and [[Richard G. Gibson]]. Addison Wesley, 1996.

== HOPL III ==
HOPL III was held June 9–10, 2007 in [[San Diego, California]].  [[Brent Hailpern]] and [[Barbara G. Ryder]] were the Conference co-Chairs.  HOPL III had an open call for participation and asked for papers on either the early history or the evolution of programming languages.  The languages must have come into existence before 1996 and been widely used since 1998, either commercially or within a specific domain.  Research languages that had a great influence on subsequent programming languages were also candidates for submission.

As with HOPL I and HOPL II, the papers were managed with a multiple stage review/revision process.

Accepted Papers for HOPL III were:
* "A history of [[Erlang (programming language)|Erlang]]" by Joe Armstrong
* "A history of [[Modula-2]] and [[Oberon programming language|Oberon]]" by [[Niklaus Wirth]]
* "[[AppleScript]]" by William R. Cook
* "Evolving a language in and for the real world: [[C++]] 1991–2006" by [[Bjarne Stroustrup]]
* "[[Self (programming language)|Self]]" by [[David Ungar]], Randall B. Smith
* "Statecharts in the making: a personal account" by [[David Harel]]
* "The design and development of [[ZPL (programming language)|ZPL]]" by Lawrence Snyder
* "The development of the [[Emerald (programming language)|Emerald]] programming language" by Andrew P. Black, Norman Hutchinson, Eric Jul and Henry M. Levy
* "The evolution of [[Lua (programming language)|Lua]]" by [[Roberto Ierusalimschy]], Luiz Henrique de Figueiredo, and Waldemar Celes
* "A history of [[Haskell (programming language)|Haskell]]:  being lazy with class" by Paul Hudak, [[John Hughes (programming)|John Hughes]], [[Simon Peyton Jones]], and [[Philip Wadler]]
* "The rise and fall of [[High Performance Fortran]]: an historical object lesson" by Ken Kennedy, Charles Koelbel, Hans Zima
* "The when, why and why not of the [[BETA programming language]]" by Bent Bruun Kristensen, Ole Lehrmann Madsen, Birger Møller-Pedersen

The HOPL III programming languages can be broadly categorized into five classes (or [[Programming paradigm|paradigms]]):  [[Object-oriented programming|Object-Oriented]] ([[Modula-2]], [[Oberon programming language|Oberon]], [[C++]], [[Self (programming language)|Self]], Emerald, and [[BETA (programming language)|BETA]]), [[Functional programming|Functional]] ([[Haskell (programming language)|Haskell]]), [[Scripting language|Scripting]] ([[AppleScript]], [[Lua (programming language)|Lua]]), Reactive ([[Erlang (programming language)|Erlang]], StateCharts), and [[Parallel computing|Parallel]] ([[ZPL (programming language)|ZPL]], [[High Performance Fortran]]).  Each HOPL III paper describes the perspective of the creators of the language.

== External links ==
*[http://research.ihost.com/hopl/HOPL-III.html Official HOPL III conference website]
*[http://hopl.info/ HOPL: an interactive Roster of Programming Languages]
*[http://purl.umn.edu/40668 History of Programming Languages Conference Records 1972-1993]. [[Charles Babbage Institute]], University of Minnesota, Minneapolis.
*[http://portal.acm.org/citation.cfm?doid=1230819.1230841 A history of the history of programming languages] by Thomas J. (Tim) Bergin

{{DEFAULTSORT:History Of Programming Languages Conference}}
[[Category:Association for Computing Machinery conferences]]
[[Category:Computer science conferences]]
[[Category:History of software]]
[[Category:Programming languages conferences]]