{| class="infobox" style="width: 28em; font-size: 90%;"
| colspan="2" style="text-align: center; font-size: 110%;"|'''East Carolina–NC State rivalry'''
|-
| colspan="2" style="text-align: center" | [[Image:East Carolina Pirates wordmark.png|125px|East Carolina logo]]&nbsp;&nbsp;&nbsp;&nbsp;[[Image:North Carolina State University Athletic logo.svg|70px|NC State logo]]
|-
| '''Sports'''
| Football, baseball, others
|-
| '''Originated'''
| 1970
|}

The '''East Carolina–NC State rivalry''' is a rivalry between [[East Carolina University]] and [[North Carolina State University]], both of which are located in [[North Carolina]]. The intensity of the rivalry is driven by the proximity (both are [[University of North Carolina|UNC system]] schools and are only 83 miles apart via [[U.S. Route 264|U.S. Highway 264]]) and the size of the two schools (NC State is the largest university in the state and East Carolina is the second largest).

East Carolina was founded in 1907 as a [[normal school]]. It became a four-year institution in 1920 and was renamed East Carolina Teachers College. It then became East Carolina College in 1951 and East Carolina University in 1967. East Carolina joined the UNC System in 1972.

North Carolina State was founded in 1887 as a [[Land-grant university|land-grant college]]. Its original name was North Carolina College of Agriculture and Mechanic Arts. In 1918, it changed its name to North Carolina State College of Agriculture and Engineering. In 1931, the school moved to under the [[University of North Carolina|Consolidated University of North Carolina]] and was renamed North Carolina State College of Agriculture and Engineering of the University of North Carolina.  It once again was renamed North Carolina State of the University of North Carolina at Raleigh in 1963 and received its current name in 1965.

==Football==
{{Infobox college sports rivalry
| name                 = East Carolina–N.C. State rivalry
| team1_image          =East Carolina Pirates wordmark.png
| team1_image_size     =
| team2_image          = North Carolina State University Athletic logo.svg
| team2_image_size     =
| team1                = [[East Carolina Pirates football|East Carolina Pirates]]
| team2                = [[NC State Wolfpack football|NC State Wolfpack]]
| sport                = [[American football|Football]]
| total_meetings       = 28
| series_record        = NC State leads, 16–13 (.551)
| first_meeting_date   = October 10, 1970
| first_meeting_result = NC State 23, East Carolina 6
| last_meeting_date    = September 10, 2016
| last_meeting_result  = East Carolina 33, NC State 30
| next_meeting_date    = 2019
| largest_win_margin   = NC State, 57–8 (1973)
| longest_win_streak   = NC State, 5 (1978–1982)
| current_win_streak   = East Carolina, 3 (2010–present)
| trophy               = Victory Barrel
}}

The most prominent sport in the rivalry is [[American football|football]]. The two teams began competing against each other in 1970. The football series between the two teams was suspended in 1987. [[Jim Valvano]] terminated NC State’s scheduling of East Carolina after Pirate fans tore down goal posts and the playing surface at Carter–Finley in 1987.  NC State’s athletics administration had publicly warned ECU and Pirate fans after two consecutive years of minor vandalisms to the stadium.  The schools would not meet again until the 1992 Peach Bowl.  In 1996, the two schools met in [[Charlotte, North Carolina|Charlotte]]'s [[Bank of America Stadium]] rather than on either school's home field. In 1997, the [[North Carolina Legislature]] proposed a bill demanding that [[University of North Carolina at Chapel Hill|UNC–CH]] and [[North Carolina State University|N.C. State]] must play [[East Carolina University|East Carolina]] on an annual basis. This would officially revive the series between [[East Carolina University|East Carolina]] and [[North Carolina State University|N.C. State]].<ref name="rivalrylaw">{{cite web |url=http://www.wral.com/news/local/story/167278/ |title=New Law Allows State-ECU Rivalry to Go On |accessdate=2009-02-11 |date=1997-11-19 |author=[[WRAL-TV|WRAL]].com |publisher=[[Capitol Broadcasting Company]]}}</ref>

East Carolina and NC State have met on the football field 29 times, with 22 of those games played in Carter–Finley Stadium, 4 in Dowdy–Ficklen Stadium, and 3 in neutral venues. The highest profile game was played in the 1992 [[Peach Bowl]], with 59,322 fans in attendance, this game would officially go on record as the largest attendance at the time for a game between two North Carolina college football teams.<ref name="familyfeud">{{cite web |url=http://www.ecu.edu/cs-admin/mktg/east/Sports-Fall-2007.cfm |title=Family Feud |accessdate=2009-02-11 |author=Bethany Bradsher |publisher=East Magazine}}</ref>

NC State leads the all-time series, with 16 wins to East Carolina's 13 wins.<ref name="futsched">{{cite web |url=http://ecupirates.cstv.com/genrel/092408aag.html |title=Holland Issues Scheduling Statement |accessdate=2009-02-11 |publisher=East Carolina University}}</ref>

===Victory Barrel===
In 2007 the Student Government Associations of both North Carolina State University and East Carolina University in a cooperative agreement began awarding the ‘Victory Barrel’ to the game winner.  The outer face of the barrel is affixed with engraved colored plates denoting the year, final score, and winner of each contest dating back to 1970.

===Football game results===
{{-}}
{{Sports rivalry series table
| legend_tie_text =
| series_summary = yes
| cols = 2
| format = compact
| team1 = East Carolina
| team1style = {{CollegePrimaryStyle|East Carolina Pirates|border=0|color=white}}
| team2 = NC State
| team2style = {{CollegePrimaryStyle|NC State Wolfpack|border=0|color=white}}

| October 10, 1970   | Raleigh, NC    | East Carolina |  6 | NC State | 23
| October 23, 1971   | Raleigh, NC    | East Carolina | 31 | NC State | 15
| October 21, 1972   | Raleigh, NC    | East Carolina | 16 | NC State | 38
| September 8, 1973  | Raleigh, NC    | East Carolina |  8 | NC State | 57
| October 5, 1974    | Raleigh, NC    | East Carolina | 20 | NC State | 24
| September 6, 1975  | Raleigh, NC    | East Carolina |  3 | NC State | 26
| September 18, 1976 | Raleigh, NC    | East Carolina | 23 | NC State | 14
| September 3, 1977  | Raleigh, NC    | East Carolina | 28 | NC State | 23
| September 9, 1978  | Raleigh, NC    | East Carolina | 13 | NC State | 29
| September 8, 1979  | Raleigh, NC    | East Carolina | 20 | NC State | 34
| November 11, 1980  | Raleigh, NC    | East Carolina | 14 | NC State | 36
| September 19, 1981 | Raleigh, NC    | East Carolina | 10 | NC State | 31
| September 11, 1982 | Raleigh, NC    | East Carolina | 26 | NC State | 33
| September 10, 1983 | Raleigh, NC    | East Carolina | 22 | NC State | 16
| September 29, 1984 | Raleigh, NC    | East Carolina | 22 | NC State | 31
| September 7, 1985  | Raleigh, NC    | East Carolina | 33 | NC State | 14
| September 6, 1986  | Raleigh, NC    | East Carolina | 10 | NC State | 38
| September 5, 1987  | Raleigh, NC    | East Carolina | 32 | NC State | 14
| January 1, 1992    | Atlanta, GA    | East Carolina | 37 | NC State | 34
| November 30, 1996  | Charlotte, NC  | East Carolina | 50 | NC State | 29
| November 22, 1997  | Raleigh, NC    | East Carolina | 24 | NC State | 37
| November 20, 1999  | Greenville, NC | East Carolina | 23 | NC State |  6
| November 27, 2004  | Charlotte, NC  | East Carolina | 14 | NC State | 52
| November 25, 2006  | Raleigh, NC    | East Carolina | 21 | NC State | 16
| October 20, 2007   | Greenville, NC | East Carolina | 20 | NC State | 34
| September 20, 2008 | Raleigh, NC    | East Carolina | 24<sup>OT</sup> | NC State | 30
| October 16, 2010   | Greenville, NC | East Carolina | 33 | NC State | 27<sup>OT</sup>
| November 23, 2013  | Raleigh, NC    | East Carolina | 42 | NC State | 28
| September 10, 2016 | Greenville, NC | East Carolina | 33 | NC State | 30
}}

==Baseball==
{{Infobox college sports rivalry
| name                 = East Carolina–N.C. State rivalry
| team1_image          = East Carolina Pirates wordmark.png
| team1_image_size     = 55px
| team2_image          = North Carolina State University Athletic logo.svg
| team2_image_size     = 42px
| team1                = [[East Carolina Pirates baseball|East Carolina]]
| team2                = [[NC State Wolfpack baseball|NC State]]
| sport                = Baseball
| total_meetings       =
| series_record        = '''NC State''' leads {{nobr|68–42–0}}
| first_meeting_date   = 1966
| first_meeting_result =
| last_meeting_date    = April 29, 2016
| last_meeting_result  = '''ECU''' 15, '''NC State''' 3
| next_meeting_date    =
| largest_win_margin   = '''NC State''' 17, '''ECU''' 1<br/>March 15, 1987
| highest_scoring_game = '''ECU''' 16, '''NC State''' 9<br/>April 18, 1967<br/>'''NC State''' 17, '''ECU''' 8<br/>May 6, 1987
| trophy               =
}}
Since 1966, the two teams have met a total of 109 times. N.C. State leads the record 68–41–0. The two teams regularly qualify for the [[NCAA Division I Baseball Championship]] and games often have implications on the national rankings, as both teams are consistently in the top 30.

===Notable games===
'''1968: NCAA District 3 Playoffs'''<br/>
East Carolina entered the NCAA Playoff with an opening win against [[Florida State Seminoles|Florida State]] and advanced to the next round to face N.C. State where they lost 7–5, giving N.C. State a sweep of the Pirates. This would be the first time that the two schools would meet in the tournament, and the two schools would not meet again in the tournament until 1990.<ref>{{cite web |url=http://web1.ncaa.org/ncaa/event.do?championship=13&division=1301990&school=196&event=1545 |title=1990 Men's Division I Baseball Atlantic Regional Game 8 – 1990-05-28 |accessdate=2009-09-09 |publisher=[[NCAA]]}}</ref> East Carolina was eventually knocked out of the tournament by Florida State. N.C. State would advance to the semi-final round of the [[1968 College World Series]], only to be eliminated by the eventual national champion [[USC Trojans]].<ref>{{cite web |url=http://web1.ncaa.org/ncaa/eventlist.do?championship=48&school=490&division=4800009 |title=NCAA Championship Archive |accessdate=2009-05-23 |publisher=[[NCAA]]}}</ref>

==References==
{{Reflist}}

{{East Carolina Pirates football navbox}}
{{NC State Wolfpack football navbox}}
{{American Athletic Conference football rivalry navbox}}
{{Atlantic Coast Conference rivalry navbox}}

{{DEFAULTSORT:East Carolina-NC State rivalry}}
[[Category:College football rivalries in the United States]]
[[Category:East Carolina Pirates football]]
[[Category:NC State Wolfpack football]]
[[Category:1966 establishments in North Carolina]]