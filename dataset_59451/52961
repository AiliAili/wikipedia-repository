'''The Astronomer's Telegram (ATel)''' is an internet service for quickly disseminating information about events relevant for astronomers.<ref name="WayScargle2012">{{cite book|author1=Michael J. Way|author2=Jeffrey D. Scargle|author3=Kamal M. Ali|author4=Ashok N. Srivastava|title=Advances in Machine Learning and Data Mining for Astronomy|url=https://books.google.com/books?id=w-5EnuOC3C0C&pg=PA50|date=29 March 2012|publisher=CRC Press|isbn=978-1-4398-4173-0|pages=50–}}</ref> Examples include [[Gamma ray bursts|gamma-ray bursts]],<ref>[http://www.symmetrymagazine.org/breaking/2009/04/30/gamma-signature-astronomers-telegram-cast-light-on-dazzling-blazar Gamma signature, Astronomer's Telegram cast light on dazzling blazar],''Symmetry'', 30 April 2009</ref><ref name="ParedesReimer2007">{{cite book|author1=Josep M. Paredes|author2=Olaf Reimer|author3=Diego F. Torres|title=The Multi-Messenger Approach to High-Energy Gamma-Ray Sources: Third Workshop on the Nature of Unidentified High-Energy Sources|url=https://books.google.com/books?id=GyP92FQjKsAC&pg=PA5|date=12 November 2007|publisher=Springer Science & Business Media|isbn=978-1-4020-6117-2|pages=5–}}</ref> [[microlenses]], [[supernovae]], [[novae]], or X-ray transients, but there are no restrictions on content matter.

The Astronomer's Telegram was launched on 17 December 1997 by Robert E. Rutledge<ref name="KidgerPérez-Fournon1999">{{cite book|author1=Mark R. Kidger|author2=I. Pérez-Fournon|author3=Francisco Sánchez|title=Internet Resources for Professional Astronomy: Proceedings of the IX Canary Islands Winter School of Astrophysics|url=https://books.google.com/books?id=tKBOAAAAIAAJ&pg=PA24|date=1999|publisher=CUP Archive|isbn=978-0-521-66308-3|pages=24–}}</ref><ref name="presentation">{{cite web|last=Fox|first=Derek B|title=A Transient Astronomy "Free for All" at The Astronomer’s Telegram|url=http://wiki.ivoa.net/internal/IVOA/VOEventSchedule/derek_fox.ppt|work=Caltech|publisher=VO Events Meeting|accessdate=8 May 2013 |archiveurl=https://web.archive.org/web/20140808042953/http://wiki.ivoa.net/internal/IVOA/VOEventSchedule/derek_fox.ppt |archivedate=2014-08-08 |format=ppt}}</ref> with the goal of rapidly (&lt;1 s) sharing information of interest to astronomers. Telegrams are sent out daily by email, but especially time sensitive events can be transmitted instantly.<ref name="article">{{cite journal|last=Rutledge|first=Robert E.|title=The Astronomer's Telegram: A Web‐based Short‐Notice Publication System for the Professional Astronomical Community|journal=Publications of the Astronomical Society of the Pacific|date=1 June 1998|volume=110|issue=748|pages=754–756|doi=10.1086/316184|arxiv=astro-ph/9802256|bibcode = 1998PASP..110..754R }}</ref> Since 2013, information is also broadcast over Twitter and Facebook.

To publish, astronomers request credentials. Credentials are issued to professional astronomers and graduate students, after verification by personal contact.<ref name="presentation"/>  Once credentials have been supplied and telegrams authorized, astronomers can publish telegrams directly, with no further editing.<ref>{{cite web|title=Policies|url=http://www.astronomerstelegram.org/?inform|publisher=Astronomer's Telegram|accessdate=8 May 2013}}</ref>

As of January 2017, over 9000 telegrams have been published.

== History ==

While working at [[Max Planck Institute for Extraterrestrial Physics|Max Planck Institute for Extraterrestrische Physik in Garching]], Bob Rutledge began the site after his experience in using the web in 1995-6 as an aid in the discovery and characterization (by multiple scientists working informally and collaboratively) of the [[Bursting Pulsar]], GRO J1744-28. Operations began in earnest at the department of astronomy of [[UC Berkeley College of Chemistry|UC Berkeley]] where Rutledge was a visiting post-doctoral scholar with Prof. [[Lars Bildsten]].

== Current operations ==

The Astronomer's Telegram currently has an editor in chief, an editor and a co-editor. The ATel service is free, both for publishers and readers of the telegrams. Astronomer's Telegram's editors remind users to report discoveries of supernovae or comets to the [[Central Bureau for Astronomical Telegrams]] as well.

== References ==
{{reflist}}

==External links==
*[http://www.astronomerstelegram.org/ The Astronomer's Telegram] official site

{{AfC postpone G13|2}}

[[Category:Astronomy]]