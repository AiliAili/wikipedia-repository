{{other people|Andrew Irvine}}
{{Infobox person
| name                      = Andrew David Irvine
| image                     = Andrew Irvine (2009).JPG
| image_size                = 
| alt                       = photograph
| caption                   = Irvine at UBC (2009)
| birth_name                = Andrew David Irvine
| birth_date                = {{birth date and age|1958|07|14}}
| birth_place               = [[Estevan]], [[Saskatchewan]], [[Canada]]
| residence                 =
| nationality               = [[Canadians|Canadian]]
| known_for                 = 
| television                =
| education                 = B.A.(Hon.) [[University of Saskatchewan|Sask]]<br> M.A. [[UWO]]<br>Ph.D. [[University of Sydney|Sydney]]
| employer                  = [[University of British Columbia]]
| organization              = 
| notable_works             = [[Socrates on Trial (play)|''Socrates on Trial'']] (2008)
| occupation                = [[Professor]]
| religion                  = 
| spouse                    = 
| children                  =
| parents                   = 
| relations                 =
| awards                    =
| website                   = [http://phil.ok.ubc.ca/faculty/irvine.html UBC - Irvine]
}}

'''Andrew David Irvine''' (born July 14, 1958) is a [[Canadians|Canadian]] [[academic]] who teaches at the [[University of British Columbia]]. He holds a [[PhD]] in philosophy from [[University of Sydney|Sydney University]] and is Head of Economics, Philosophy and Political Science at [[University of British Columbia Okanagan|UBC Okanagan]]. He is a past vice-chair of the 
[http://bog.ubc.ca/ UBC Board of Governors], a past president of the [[BCCLA|British Columbia Civil Liberties Association]], and a member of the board of directors of the [[Society for Academic Freedom and Scholarship]]. He has held visiting positions at several Canadian and American universities and has been recognized as one of British Columbia’s most influential public intellectuals.<ref>Douglas Todd, “Who are B.C.’s Biggest Thinkers? Meet the Top 50,” ''Vancouver Sun'', 25 August 2000, A1, A12</ref>

== Academic work ==

Often cited for his work on the twentieth-century philosopher [[Bertrand Russell]],<ref>Giannis Triantafillou, “Bertrand Russell is Relevant even Today,” ''Eleftherotipia'' (Greece), 21 January 2010</ref><ref>Juan Ignacio Rodriguez Medina, “Las buenas razones de Bertrand Russell,” ''Il Mercurio'' (Chile), 19 August 2012</ref><ref>Anon., “Guides to Russell’s Writings,” Bertrand Russell Archives, 29 November 2010, http://www.mcmaster.ca/russdocs/writings.htm</ref> Irvine has  argued in favour of [[physicalism]]<ref>James Robert Brown, “Logic, Epistemology, Philosophy of Science,” ''The Canadian Encyclopedia'', 2012, {{cite web|url=http://www.thecanadianencyclopedia.com/articles/logic-epistemology-philosophy-of-science |title=Archived copy |accessdate=2013-07-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20130603125532/http://www.thecanadianencyclopedia.com:80/articles/logic-epistemology-philosophy-of-science |archivedate=2013-06-03 |df= }}</ref> and against several commonly held views in the history of modern philosophy, including the claim that [[Gottlob Frege]] succeeded in developing a workable theory of mathematical platonism<ref>Andrew David Irvine, “Frege on Number Properties,” ''Studia Logica'', vol. 96 (2010), 239-60</ref> and the claim that Bertrand Russell was an advocate of epistemic logicism,<ref>Andrew David Irvine, “Epistemic Logicism and Russell’s Regressive Method,” ''Philosophical Studies'', vol. 55 (1989), 303-27</ref><ref>Andrew David Irvine, “Russell on Method,” in Godehard Link (ed.), ''One Hundred Years of Russell’s Paradox'', Berlin and New York: Walter de Gruyter Publishing Company, 2004, 481-500</ref> a claim that one commentator has concluded is now “thoroughly debunked.”<ref>Conor Mayo-Wilson, “Russell on Logicism and Coherence,” ''Russell: the Journal of Bertrand Russell Studies'', vol. 31 (2011), 66; for a less sympathetic appraisal, see [[Anders Kraal]], “The Aim of Russell's Early Logicism: A Reinterpretation,” ''Synthese'', vol. 191 (2014), 1493-1510</ref>

He has defended a two-box solution to [[Newcomb's paradox|Newcomb’s problem]] in which he abandons “the (false) assumption that past observed frequency is an infallible guide to probability”<ref>Andrew David Irvine, “How Braess’ Paradox Solves Newcomb’s Problem,” ''International Studies in the Philosophy of Science'', vol. 7 (1993), 141-60 at 157</ref> and a non-cognitivist solution to the [[liar paradox]], noting that “formal criteria alone will inevitably prove insufficient” for determining whether individual sentence tokens have meaning.<ref>Andrew David Irvine, “Gaps, Gluts, and Paradox,” ''Canadian Journal of Philosophy'', supplementary vol. 18 [''Return of the A priori''] (1992), 273-99 at 294</ref>

In [[modal logic]] (which studies theories of possibility and necessity), he has argued in favour of the non-normal system S7, rather than more traditional systems such as S4 or S5.<ref>Andrew David Irvine, “S7,” ''Journal of Applied Logic'', vol. 11 (2013), 523-529</ref> Unlike other systems, S7 allows logicians to choose between competing logics, each of which, if true, would be necessarily true, but none of which are necessarily the correct system of necessary truths. As Irvine puts it, “just as being ''physically possible'' means nothing more than being consistent with the laws of physics, being ''logically possible'' means nothing more than being consistent with the laws of logic. However, this leaves open the question of which logic and which consistency relation are to be adopted. S7 gives us the language to discuss the possible denial of necessary truths. S7 gives us the language to assert not only that some propositions really are necessary; it gives us the language also to note that their denials, although impossible, remain possibly possible.”<ref>Andrew David Irvine, “S7,” ''Journal of Applied Logic'', vol. 11 (2013), 527</ref> In other words, there is a mechanism in which even sets of necessary truths can be compared to their alternatives.

== Political work ==

An advocate of traditional democratic [[civil liberties]], Irvine has argued in favour of [[Freedom of speech|free speech rights]], both for political reasons<ref>Andrew David Irvine, “Free Speech, Democracy, and the Question of Political Influence,” in W. Wesley Pue (ed.), ''Pepper in Our Eyes: The APEC Affair'', Vancouver: UBC Press, 2000, 29-40</ref><ref>Andrew David Irvine and David Sutherland, “Know Thy Neighbour’s Views,” ''Globe and Mail'', 08 April 2005, A15</ref> and in the context of defending [[academic freedom]].<ref>Andrew David Irvine, “Let Truth and Falsehood Grapple,” ''University of Toronto Quarterly'', vol. 67 (1998), 549-66</ref><ref>Andrew David Irvine, “Bertrand Russell and Academic Freedom,” ''Russell: the Journal of Bertrand Russell Studies'', vol. 16 (1996), 5-36</ref>

Together with Stephen Wexler, he has  argued that modern constitutional protections of the [[rule of law]] can trace their roots as far back as Socrates' demand that even lawmakers must be bound by the law. It was this demand that led to [[Aristotle|Aristotle’s]] distinction between ''psephismata'' (votes of the assembly) and ''nomos'' (statute law), and to the resulting debate over how best to decide questions of legal supremacy within a democracy.<ref>Stephen Wexler and Andrew David Irvine, “Aristotle on the Rule of Law,” ''Polis'', vol. 23 (2006), 116-38</ref> Together with Jason Gratl, he has  argued that, in its modern form, the [[rule of law]] helps resolve tensions between [[national security]] and public accountability<ref>Jason Gratl and Andrew David Irvine, “National Security, State Secrecy and Public Accountability,” ''University of New Brunswick Law Journal'', vol. 55 (2005), 251-71</ref> and, together with John Whyte, he has argued that care needs to be taken with regard to electoral reform, especially when it comes to implementing proposals focusing on [[proportional representation]].<ref>John Whyte and Andrew David Irvine, “Searching for Democratic Vitality: An Analysis of Electoral Reform,” ''Policy Dialogue'', no. 17 (Winter 2008), 3-4</ref> He is often cited in the media on issues ranging from free speech<ref>Neal Hall, “Little Sisters’ Appeals to Highest Court,” ''Vancouver Sun'', 19 February 1999, A6</ref><ref>Shawn Ohler, “Human Rights Tribunal Says Columnist’s Opinion Anti-Semitic,” ''National Post'', 04 February 1999, A4</ref><ref>Colin Freeze, “Protesters Cynical about an Apology,” ''Globe and Mail'', 27 March 2002, A6</ref><ref>Mark Hume, “Teacher’s Suspension Sparks Free Speech Fight,” ''Calgary Herald'', 24 April 2003, A3</ref> and academic freedom <ref>Barbara Kay, “Propaganda in the Classroom” ''National Post'', 15 December 2004, A17</ref> to parliamentary procedure<ref>Jason Unrau, “Professor Queries Speaker’s Ruling,” ''Whitehorse Star'', 12 December 2008</ref> and judicial activism.<ref>Neil Seeman, “Who Runs Canada?” ''National Post'', 24 July 1999, B3</ref>

== Theatre work ==

In 2007 Irvine premiered [[Socrates on Trial (play)|''Socrates on Trial'']], a [[Play (theatre)|play]] depicting the life and death of the ancient Greek philosopher [[Socrates]]. The play tells the story of how Socrates was put on trial for corrupting the youth of Athens and for failing to honour the city’s gods. The play contains adaptations of several classic Greek works including the slapstick comedy [[Clouds (Aristophanes)|''Clouds'']], written by [[Aristophanes]] and first performed in 423 BCE, and the dramatic monologue [[Apology (Plato)|''Apology'']], written by [[Plato]] to record the defence speech Socrates gave at his trial in 399 BCE. The premiere was directed by Joan Bryans of Vital Spark Theatre Company at the [[Chan Centre for the Performing Arts]] in [[Vancouver]].<ref>Anon., “Stage Listings,” ''Vancouver Sun'', 29 May 2008, {{cite web|url=http://www.canada.com/vancouversun/news/westcoastlife/story.html?id%3D56963701-382d-41cc-b75c-53fb57678d7d |title=Archived copy |accessdate=2013-07-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20150215020022/http://www.canada.com/vancouversun/news/westcoastlife/story.html?id=56963701-382d-41cc-b75c-53fb57678d7d |archivedate=2015-02-15 |df= }}</ref>

In the words of one reviewer, “The play is refreshingly illuminating on the relationship between Socrates’ execution and the lasting influence of Aristophanes’ negative depiction of him on the evolution of the Athenian psyche.”<ref>Alapin, Maya, Review of ''Socrates on Trial'', ''Bryn Mawr Classical Review,'' 2008.08.08</ref> According to another, the play not only gives an entertaining portrayal of Plato's famous mentor, but also a fascinating introduction to the “pompous, arrogant and often petulant” individual presented by Aristophanes,<ref>Anon., “Socrates on Trial,” ''Riveting Riffs Magazine,'' 2008, http://www.rivetingriffs.com/Socrates%20On%20Trial.html</ref> giving modern audiences a greater understanding of why Socrates eventually ended up being sentenced to death.

== Bibliographical work ==

In 1999, Irvine produced detailed bibliographies of both the primary<ref>Andrew David Irvine, “Select Primary Bibliography,” in Andrew David Irvine (ed.), ''Bertrand Russell: Life, Work and Influence,'' London: Routledge, 1999, 217-46</ref> and secondary<ref>Andrew David Irvine, “Select Secondary Bibliography,” in Andrew David Irvine (ed.), ''Bertrand Russell: Life, Work and Influence,'' London: Routledge, 1999, 247-312</ref> literature surrounding the Nobel Laureate [[Bertrand Russell]]. Together with Dawn Ogden, he also produced the first bibliographical index for Russell’s influential book, ''A History of Western Philosophy.''<ref>Dawn Ogden and Andrew David Irvine, “A Bibliographical Index for Bertrand Russell’s ''History of Western Philosophy'',” ''Russell'', vol. 19 (1999), no. 1, 63-83</ref> The index is based on the second British edition (of 1979). A conversion table gives page references for both the first American edition (of 1945) and the first British edition (of 1946).

Together with Edmond Rivère, Irvine is the author of the first comprehensive, scholarly bibliography of Canada’s premier literary prize, the [[Governor General's Awards|Governor General’s Literary Awards]].<ref>Andrew David Irvine, “Bibliographic Errata regarding the Cumulative List of Winners of the Governor General’s Literary Awards / Liste cumulative des lauréates et des lauréats des Prix littéraires du Gouverneur général, 2011,” ''Papers of the Bibliographical Society of Canada'', 50 (2012), 51-61; Andrew David Irvine, “The Governor General’s Literary Awards: An Introduction,” ''Papers of the Bibliographical Society of Canada'', 52 (2014), 7-33; Andrew David Irvine, “The Governor General’s Literary Awards: English-language Winners, 1936-2013,” ''Papers of the Bibliographical Society of Canada'', 52 (2014), 35-161; and Andrew David Irvine and Edmond Rivère, “Prix littéraires du Gouverneur général: Lauréats en langue française, 1936-2013,” ''Papers of the Bibliographical Society of Canada'', 52 (2014), 163-267</ref> The bibliography covers the history of the awards from their inception in 1936 through to the end of 2013 and appeared in ''Papers of the Bibliographical Society of Canada'' in 2014.

== Literary references ==

In 1994, Irvine served as inspiration for the character Hardy Orbs in the dystopian novel, ''Fair New World''. The novel was written by [[Lou Marinoff]] under the pseudonym Lou Tafler. A twentieth-anniversary re-issue in 2014 contained a foreword purportedly written by Orbs.<ref>Lou Tafler, ''Fair New World'', second edition, Denver: Argo Navis, 2014, xiii-xxiii</ref>

== Books ==

*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Physicalism in Mathematics | location= Dordrecht | publisher= Kluwer Academic Publishers | year=1990}} ISBN 0-7923-0513-2
*{{cite book | last= Irvine | first= Andrew David & Gary A. Wedeking (eds) | title= Russell and Analytic Philosophy | location=Toronto | publisher= University of Toronto Press | year=1993}} ISBN 0-8020-2875-6
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Bertrand Russell – Life, Work and Influence | location=London | publisher= Routledge | year=1999}} ISBN 0-415-13055-7 (Volume 1 of {{cite book | title= Bertrand Russell: Critical Assessments | location= London | publisher= Routledge | year=1999}} ISBN 0-415-13054-9) 
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Bertrand Russell – Logic and Mathematics | location=London | publisher= Routledge | year=1999}} ISBN 0-415-13056-5 (Volume 2 of {{cite book | title= Bertrand Russell: Critical Assessments | location= London | publisher= Routledge | year=1999}} ISBN 0-415-13054-9) 
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Bertrand Russell – Language, Knowledge and the World | location=London | publisher= Routledge | year=1999}} ISBN 0-415-13057-3 (Volume 3 of {{cite book | title= Bertrand Russell: Critical Assessments | location= London | publisher= Routledge | year=1999}} ISBN 0-415-13054-9) 
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Bertrand Russell – History of Philosophy, Ethics, Education, Religion and Politics | location=London | publisher= Routledge | year=1999}} ISBN 0-415-13058-1 (Volume 4 of {{cite book | title= Bertrand Russell: Critical Assessments | location= London | publisher= Routledge | year=1999}} ISBN 0-415-13054-9) 
*{{cite book | last= Woods | first= John, Andrew David Irvine & Douglas Walton | title= Argument | location=Toronto | publisher= Pearson | year=2000}} ISBN 0-13-085115-9 (1st edn); ISBN 0-13-039938-8 (2nd edn)
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= ''David Stove’s'' On Enlightenment | location=New Brunswick | publisher= Transaction | year=2003}} ISBN 0-7658-0136-1 (cloth), ISBN 978-1-4128-5186-2 (paper)
*{{cite book | last= Peacock | first= Kent & Andrew David Irvine (eds) | title= Mistakes of Reason | location=Toronto | publisher= University of Toronto Press | year=2005}} ISBN 0-8020-3866-2
*{{cite book | last= Irvine | first= Andrew David & John Russell (eds) | title= In the Agora | location=Toronto | publisher= University of Toronto Press | year=2006}} ISBN 0-8020-3895-6 (cloth), ISBN 0-8020-3817-4 (paper)
*{{cite book | last= Irvine | first= Andrew David | title= Socrates on Trial | location=Toronto | publisher= University of Toronto Press | year=2008}} ISBN 978-0-8020-9783-5 (cloth), ISBN 978-0-8020-9538-1 (paper), ISBN 978-0-8020-9538-1 (e-pub)
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= Philosophy of Mathematics | location= Amsterdam | publisher=Elsevier / North-Holland | year=2009}} ISBN 978-0-444-51555-1
*{{cite book | last= Irvine | first= Andrew David (ed.) | title= ''David Stove’s'' What’s Wrong with Benevolence | location=New York | publisher= Encounter Books | year=2011}} ISBN 978-1-59403-523-4

== References ==

{{Reflist |2}}

== External links ==
*[https://news.ok.ubc.ca/ikbarberschool/2013/07/18/exemplary-senior-scholar-joines-barber-school-as-unit-head/ UBC biography page]
*[http://phil.ok.ubc.ca/faculty/irvine.html UBC faculty page]

{{DEFAULTSORT:Irvine, Andrew David}}
[[Category:1958 births]]
[[Category:Canadian philosophers]]
[[Category:Living people]]
[[Category:People from Estevan]]
[[Category:University of British Columbia faculty]]
[[Category:University of Saskatchewan alumni]]
[[Category:University of Sydney alumni]]
[[Category:University of Western Ontario alumni]]