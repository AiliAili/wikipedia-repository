{|{{Infobox Aircraft Begin
 |name         = Learjet 28/29
 |image        = Learjet 28-29.jpg
 |caption      = 
}}{{Infobox Aircraft Type
 |type         = [[Business jet]]
 |manufacturer = [[Learjet]]
 |designer     = 
 |first flight = August 24, 1977
 |introduced   = 
 |retired      = [[NASA]]
 |status       = Retired
 |primary user = 
 |more users   = 
 |produced     = 1977-1982
 |number built = 9
 |unit cost    = 
 |developed from = [[Learjet 25]]
 |developed into = [[Learjet 31]]
}}
|}
The '''Learjet 28''' is an [[United States|American]] eight-to-ten-seat (two crew and six to eight passengers), twin-engine, high-speed [[business jet]], intended to be the successor to the [[Learjet 25]]. The '''Learjet 29''' is identical except for the addition of a long-range fuel tank, resulting in the reduction of the capacity to six (two crew and four passengers). Both were manufactured by [[Learjet]] and were marketed under the '''Longhorn''' name.<ref name="airliners">[http://www.airliners.net/info/stats.main?id=264 A history of the LJ23-LJ29 series on Airliners.net]</ref>

==History==
The first flight of the Learjet 28 took place on August 24, 1977. FAA certification was awarded to both the Learjet 28 and 29 on July 29, 1979.<ref name="wok">[http://www.wingsoverkansas.com/history/article.asp?id=199 Learjet company timeline]</ref>

The Learjet 28/29 was the first production jet aircraft to utilize winglets{{citation needed|date=September 2013}} (entering service in 1977).

The Learjet 28/29 was based on the [[Learjet 25]], and received a completely new wing fitted with winglets which resulted in improved performance and fuel economy.<ref name="airliners"/>  Both models were commercially unsuccessful due to their outdated engines{{citation needed|date=September 2013}} (noise and fuel consumption being too high).

Only five production LearJet 28s, and four LearJet 29s, were constructed before production ceased in 1982. Both types were subsequently replaced by the [[Learjet 35]].<ref name="airliners"/>

==Noise compliance==
In 2013, the FAA modified 14 CFR part 91 rules to prohibit the operation of jets weighing 75,000 pounds or less that are not stage 3 noise compliant after December 31, 2015.  The Learjet 28 is listed explicitly in Federal Register [http://www.gpo.gov/fdsys/granule/FR-2013-07-02/2013-15843/content-detail.html 78 FR 39576].  Any Learjet 28s that have not been modified by installing Stage 3 noise compliant engines or have not had "hushkits" installed for non-compliant engines will not be permitted to fly in the contiguous 48 states after December 31, 2015.  ''14 CFR §91.883 Special flight authorizations for jet airplanes weighing 75,000 pounds or less'' - lists special flight authorizations that may be granted for operation after December 31, 2015.

==Operators==
;{{USA}}
*[[NASA]]

==Specifications (Learjet 28)==
{{Aircraft specs
|ref=Jane's Civil and Military Aircraft Upgrades 1994–95<ref name="Janes up p317-8">Michell 1994, pp. 317–318.</ref><!-- the reference for the data given -->
|prime units?=imp<!-- "imp", "kts" or "met" to display the units in a given order. 
Without an entry here, no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=8 passengers
|length m=14.51
|length ft=
|length in=
|span m=13.35
|span ft=
|span in=
|height m=3.73
|height ft=
|height in=
|wing area sqm=
|wing area sqft=264.5
|airfoil = NACA 64A109
|empty weight kg=3750
|empty weight lb=
|gross weight kg=
|gross weight lb=15000
|fuel capacity=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[General Electric CJ-610]]-8A
|eng1 type=[[turbojet]]
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=2950<!-- jet/rocket engines -->

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=549
|max speed kts=
|max speed mach=<!-- supersonic aircraft -->
|mach speed note=at 25,000 ft (7,620 m)
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=470<!-- if max speed unknown -->
|cruise speed kts=
|cruise speed note=,econ cruise at 51,000 ft (15,545 m)
|stall speed kmh=165
|range km=
|range miles=1309
|range nmi=
|ceiling m=
|ceiling ft=51000
|climb rate ms=
|climb rate ftmin=
|more performance=

|avionics=
}}

==See also==
{{Aircontent|
|related=
*[[Learjet 25]]
*[[Learjet 31]]
|similar aircraft=
|lists=
*[[List of business jets]]
}}

==References==
;Notes
{{reflist}}
;Bibliography
*Michell, Simon. ''Jane's Civil and Military Aircraft Upgrades 1994-95''. Coulsdon, UK:Jane's Information Group, 1994. ISBN 0-7106-1208-7.

==External links==
{{commons category|Learjet 28 and 29}}
*[http://www.airwar.ru/enc_e/aliner/ljet28.html Specs of LJ28]
*[http://www.airwar.ru/enc_e/aliner/ljet29.html Specs of LJ29]
*[http://www.airliners.net/info/stats.main?id=264 A history of the LJ23-LJ29 series on Airliners.net]

{{Learjet}}

[[Category:Learjet aircraft|28 29]]
[[Category:United States business aircraft 1970–1979]]
[[Category:Twinjets]]
[[Category:Low-wing aircraft]]
[[Category:T-tail aircraft]]