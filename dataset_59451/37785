{{good article}}
{{Infobox religious building
| building_name =Temple Beth Israel
| image = Temple Beth Israel (Eugene, Oregon) Southwest.jpg
| image_size = 300px
| caption =
| map_type =Eugene OR
| map_size =
| map_caption =Location in Eugene, Oregon
| location =1175 East 29th Avenue,<br>[[Eugene, Oregon]], {{flag|United States}}
| geo = {{coord|44.025871|-123.075364|display=inline,title}}
| religious_affiliation =[[Reconstructionist Judaism]]
| rite =
| region =
| province =
| territory =
| prefecture =
| sector =
| district =
| cercle =
| municipality =
| consecration_year =
| status =
| functional_status =Active
| heritage_designation =
| leadership =Rabbi: Yitzhak Husbands-Hankin<br>Associate Rabbi: Boris Dolin<ref name=TBIRabbispage/>
| website ={{url|tbieugene.org}}
| architecture =yes
| architect =Mel Solomon and Associates,<br>TBG Architects & Planners<ref name=Harwood/>
| architecture_type =
| architecture_style =
| general_contractor =McKenzie Commercial Construction<ref name=KVAL/>
| facade_direction =
| groundbreaking =
| year_completed =2008<ref name=Haist/>
| construction_cost =$6 million<ref name=Haist/>
| specifications =yes
| capacity =900+<ref name=Haist/>
| length =
| width =
| width_nave =
| height_max =
| dome_quantity =
| dome_height_outer =
| dome_height_inner =
| dome_dia_outer =
| dome_dia_inner =
| minaret_quantity =
| minaret_height =
| spire_quantity =
| spire_height =
| materials =Concrete, steel, wood<ref name=Haist/>
| nrhp =
| added =
| refnum =
| designated =
}}
'''Temple Beth Israel''' ({{lang-he|בית ישראל}}) is a [[Reconstructionist Judaism|Reconstructionist]] [[synagogue]] located at 1175 East 29th Avenue in [[Eugene, Oregon]]. Founded in the early 1930s as a [[Conservative Judaism|Conservative]] congregation, Beth Israel was for many decades the only synagogue in Eugene.
 
The congregation initially worshipped in a converted house on West Eighth Street. It constructed its first building on Portland Street in 1952, and occupied its current [[Leadership in Energy and Environmental Design|LEED]]-compliant facilities in 2008.

In the early 1990s conflict between feminist and traditional members led to the latter leaving Beth Israel, and forming the [[Orthodox Judaism|Orthodox]] Congregation Ahavas Torah. Beth Israel came under attack from [[Neo-Nazism|neo-Nazi]] members of the [[Volksfront]] twice, in 1994 and again in 2002. In both cases the perpetrators were caught and convicted.

Services were [[Laity|lay]]-led for decades. Marcus Simmons was hired as the congregation's first [[rabbi]] in 1959, but left in 1961. After a gap of two years, Louis Neimand became rabbi in 1963, and served until his death in 1976. He was followed by Myron Kinberg, who served from 1977 to 1994, and Kinberg in turn was succeeded by Yitzhak Husbands-Hankin. Maurice Harris joined Husbands-Hankin as associate rabbi in 2003, and served until 2011, when he was succeeded by Boris Dolin. {{As of|2014}}, led by Husbands-Hankin and Dolin, Beth Israel had approximately 400 member households, and was the largest synagogue in Eugene.

==Early history==
Small numbers of [[German Jews]] began settling in Eugene in the late 19th century, but most moved on. In the early 20th century the first Eastern European Jews settled there, and by the 1920s Eugene's Jewish community began gathering [[minyan|prayer quorums]] for holding [[Shabbat|Friday night]] and [[Jewish holiday]] services in individuals' homes. Historian Steven Lowenstein writes that "[a]fter Hymen Rubenstein's death in 1933, his home at 231 West Eighth Street was remodeled and named Temple Beth Israel".<ref name=founded>According to [[#refHaist2008|Haist (2008)]], in 2008 the congregation was 87 years old, indicating a founding year of around 1921. According to the [[#refKVAL2008|KVAL-TV Web Staff (June 11, 2008)]], "The Temple Beth Israel congregation has been in the Eugene community since 1927." According to [[#refLowenstein1987|Lowenstein (1987)]], p.&nbsp;191, it was founded after Hymen Rubenstein's death in 1933. According to [[#refWright2003|Wright & Pinyerd (2003)]], p.&nbsp;12.1. and the [[#refTBIhomepage|Temple Beth Israel website]], it was founded in 1934. According to [[#refWright1996|Wright (1996)]], it was founded after [[World War II]].</ref> It was a traditional [[Conservative Judaism|Conservative]] synagogue,<ref name=Zuckerman89>[[#refZuckerman2003|Zuckerman (2003)]], p.&nbsp;89.</ref> and from that time until the 1990s it was the only synagogue in Eugene.<ref name=Zuckerman87>[[#refZuckerman2003|Zuckerman (2003)]], p.&nbsp;87.</ref><ref name=Reichman>[[#refReichman2007|Reichman (2007)]].</ref>

In 1952, the congregation constructed a one-story synagogue building on an almost {{convert|1|acre}} property at 2550 Portland Street.<ref name=Haist>[[#refHaist2008|Haist (2008)]].</ref><ref name=Lowenstein1987p191>[[#refLowenstein1987|Lowenstein (1987)]], p.&nbsp;191.</ref><ref name=Bjornstad2009>[[#refBjornstad2009|Bjornstad (2009)]].</ref> Designed by architect and [[The Holocaust|Holocaust]]-survivor Heinrich Hormuth (H.H.) Waechter, the building featured an interior courtyard that provided natural lighting, and "a network of ceiling beams painted with symbols and shapes" by Waechter.<ref name=Bjornstad2009/><ref name=Wright2003>[[#refWright2003|Wright & Pinyerd (2003)]], p.&nbsp;12.1.</ref><ref name=AAD1970p955>[[#refAAD|American Architects Directory (1970)]], p.&nbsp;955.</ref>

Temple Beth Israel's services and religious functions were lay-led for decades.<ref name=Tepfer2010>[[#refTepfer2010|Tepfer (2010)]].</ref> Its first [[rabbi]] was Marcus Simmons.<ref name=Lowenstein1987p191/><ref name=RG19610520>[[#refRG19610520|''The Register-Guard'' (May 20, 1961)]].</ref> Originally from England, he was a graduate of [[University of London]] and [[Oxford University]], and was [[semikha|ordained]] at the [[Hebrew Theological Seminary]]. He emigrated to the United States in 1957, and joined Beth Israel in 1959.<ref name=RG19610128>[[#refRG19610128|''The Register-Guard'' (January 28, 1961)]].</ref> The members were not, however, agreed that a full-time rabbi was required,<ref name=Tepfer2010/> and in 1961, he accepted a rabbinical position in [[Downey, California]].<ref name=RG19610520/>

Following a hiatus of two years, Louis Neimand was hired as rabbi in 1963.<ref name=Tepfer2010/><ref name=RG19760906>[[#refRG19760906|''The Register-Guard'' (August 6, 1976)]].</ref> Born in New York City in 1912 to immigrant parents, he was a graduate of [[City University of New York]] and was ordained at the [[Jewish Institute of New York]].<ref name=RG19760906/> He had previously worked for the [[United Jewish Appeal]], and from 1959 to 1963 was the first [[Hillel: The Foundation for Jewish Campus Life|Hillel]] rabbi at [[Syracuse University]].<ref name=RG19760906/><ref name=Greene1996p160>[[#refGreene1996|Greene & Baron (1996)]], p.&nbsp;160.</ref> There was some concern about Neimand's hiring, as he had a police record as a result of his involvement in [[Selma to Montgomery marches|freedom marches]] during the [[Civil Rights Movement]]. He served until his death in 1976.<ref name=Tepfer2010/>

==Kinberg era==
Myron Kinberg was hired as rabbi in 1977.<ref name=Lowenstein1987p191/> Ordained in [[Union for Reform Judaism|Reform Judaism]],<ref name=Wright1996>[[#refWright1996|Wright (1996)]].</ref> he had previously served as a rabbi in [[Topeka, Kansas]] for two years, then lived in Israel for two years, before coming to Eugene.<ref name=MP19941104>[[#refMP19941104|Moscow-Pullman Daily News (November 4, 1994)]].</ref> Kinberg was known for his support for minority rights and [[gay rights]], anti-nuclear and anti-war activism, support of reconciliation between Israel and the Palestinians, and outreach to non-observant members of Eugene's Jewish community.<ref name=Sinks1994>[[#refSinks1994|Sinks (1994)]].</ref><ref name=Bjornstad1996>[[#refBjornstad1996|Bjornstad (1996)]].</ref>

Kinberg attempted to revive the Biblical concept of the "''[[ger toshav]]''" in his approach to intermarriage. He was willing to officiate at an intermarriage if the non-Jewish partner, after discussions with the rabbi, agreed of his or her own free will to fulfill a set of commitments, including "a commitment to a Jewish home life, participation in Jewish life and tradition, and raising future children as Jews". The non-Jewish partner making this commitment became a "''ger toshav''", or "non-Jewish member of the Jewish people".<ref name=Ritualwell>[[#refRitualwell|"Brit Ger Toshav and Brit Nisuin", Ritualwell.org]].</ref><ref name=refAbrams>[[#refAbrams|Abrams]].</ref>

Kinberg's wife Alice was a strong [[feminism|feminist]], and during the 1980s he and his wife supported a number of changes to the liturgy and ritual.<ref name=Myrowitz1995p163>[[#refMyrowitz1995|Myrowitz (1995)]], p.&nbsp;163.</ref><ref name=Zuckerman89-90>[[#refZuckerman2003|Zuckerman (2003)]], pp.&nbsp;89-90.</ref> These included allowing women to [[Torah reading|read from the Torah]] and [[hazzan|lead the prayers]], and changing prayers to be more [[Gender-neutral language|gender inclusive]] - for example, using gender-neutral terms and pronouns for [[God]], and adding references to the [[Matriarchs (Bible)|Biblical matriarchs]] in prayers like the ''[[Amidah]]'', which traditionally only mentioned the [[Patriarchs (Bible)|Biblical patriarchs]]. While most congregation members approved of these changes, a minority resisted them.<ref name=Zuckerman89-90/>

===Schism===
By the early 1990s serious divisions developed among the members of the congregation over a number of issues, including personal antagonisms, the rabbi's activism and "advocacy of 'ultra-liberal' causes", political differences over the [[Israeli–Palestinian conflict]],<ref name=Sinks1994/><ref name=Bjornstad1996/> and
<blockquote>a myriad of additional Jewish cultural/religious issues, such as the acceptance of intermarried couples, adherence to kosher dietary laws, the use of modern language and music during worship services, rewriting of certain prayers such as the ''[[Aleinu|Aleynu]]'' to make them less ethnocentric, and so on.<ref name=Zuckerman88>[[#refZuckerman2003|Zuckerman (2003)]], p.&nbsp;88.</ref></blockquote>
However, the biggest source of division, which underlay all others, was "the roles and rights of men and women in the synagogue."<ref name=Zuckerman88/>

In the early 1990s a group of [[Baal teshuva|newly observant]] members began holding more traditional services in a back room of the synagogue, complete with a ''[[mechitza]]'', a partition separating men and women. The "more feminist-minded" members strongly objected to having a ''mechitza'' anywhere in the Temple Beth Israel building, even if it were not in the services they attended. The latter group eventually circulated a petition which stated that either the ''mechitza'' would have to be taken down, or those members who wanted it would have to leave.<ref name=Bjornstad1996/><ref name=Zuckerman91-93>[[#refZuckerman2003|Zuckerman (2003)]], pp.&nbsp;91-93.</ref> Kinberg also signed the petition.<ref name=Wright19990312>[[#refWright19990312|Wright (March 12, 1999)]].</ref> Faced with this opposition, in 1992 the [[Orthodox Judaism|Orthodox]] members left, renting new premises and hiring their own rabbi, creating Eugene's second synagogue, originally called "The [[halakha|Halachic]] [[Minyan]]", and in 1998 renamed "Congregation Ahavas Torah".<ref name=Bjornstad1996/><ref name=Zuckerman91-93/><ref name=Wright19990312/><ref name=CongregationAhavasTorah>[[#refCongregationAhavasTorah|About Us, Congregation Ahavas Torah website]].</ref>

Kinberg held himself responsible,<ref name=Wright19990312/> and the schism led to his "reassessment of the needs of Temple Beth Israel and his role as a rabbi".<ref name=Bjornstad1996/> As a result, he left Beth Israel in 1994 to lead a synagogue on Long Island.<ref name=Bjornstad1996/><ref name=Wright19990312/> During his tenure at Beth Israel, membership rose from 118 to 350 families.<ref name=Sinks1994/> Kinberg died two years later at age 51.<ref name=Bjornstad1996/>

==Husbands-Hankin era==
Yitzhak Husbands-Hankin succeeded Kinberg in 1995. Husbands-Hankin began his involvement at Temple Beth Israel first as a congregant, then as [[hazzan|cantor]], and then as an assistant rabbi.<ref name=Bennett1987>[[#refBennett1987|Bennett (1987)]].</ref> He was active in forming the [[Jewish Renewal]] movement, and was ordained by its leader [[Zalman Schachter-Shalomi]].<ref name=Ruach>[[#refRuach|"Teachers", 2008 Summer Retreat, Ruach Ha'aretz website]].</ref>

The congregation decided to leave the Conservative movement in 1995, and for a year had no affiliation. In late 1996, after considering both [[Reform Judaism|Reform]] and [[Reconstructionist Judaism|Reconstructionist]] as alternatives, the congregation affiliated with the Reconstructionist movement.<ref name=Wright1996/><ref name=Wright19990312/><ref name=Wright19990415>[[#refWright19990415|Wright (April 15, 1999)]].</ref> By 1999, membership had grown to around 370 families.<ref name=Wright19990312/>

Husbands-Hankin was instrumental in developing the concept of "Ethical Kashrut", the idea that one should only purchase goods that are produced in an ethical way.<ref name=Husbands-Hankin2004>[[#refHusbands-Hankin2004|Husbands-Hankin (2004)]].</ref> His essay, "Ethical Kashrut," was selected for publication in [[Arthur Kurzweil]]'s ''Best Jewish Writing 2003''.<ref name=Kurzweil2003p158>[[#refKurzweil2003|Kurzweil (2003)]], p.&nbsp;158.</ref> A singer, cello and guitar player, he composes and performs Jewish music.<ref name=Elon2000p489>[[#refElon2000|Elon (2000)]], p&nbsp;489.</ref>

Husbands-Hankin has had four assistant or associate rabbis working with him. Shoshana Spergel joined Temple Beth Israel in 1998 as interim rabbi when Husbands-Hankins went on a [[sabbatical]];<ref name=Wright19990613>[[#refWright19990613|Wright (June 13, 1999)]].</ref> Jonathan Seidel was assistant rabbi from 2001 to 2003.<ref name=Seldner2007>[[#refSeldner2007|Seldner (2007)]].</ref> Maurice Harris, a 2003 graduate of the Reconstructionist Rabbinical College, joined as assistant rabbi in 2003.<ref name=RG20030719>[[#refRG20030719|''The Register-Guard'' (July 19, 2003)]].</ref> He is one of the signators of The Open Letter Concerning Religion and Science From American Rabbis, part of the [[Clergy Letter Project]] which "encourages and embraces the teaching of evolution in schools".<ref name=ClergyLetterProject>[[#refCLP|Clergy Letter Project, Jewish Letter, Signatures]].</ref>  In 2011, Boris Dolin joined the congregation as its newest associate rabbi.<ref name=TBIRabbispage/>

===Attacks by neo-Nazis===
On March 20, 1994, Chris Lord, an individual associated with the [[Volksfront]] and [[American Front]], fired ten rounds with an assault rifle into the temple, damaging the interior.<ref name=ADL>[[#refADL|Volksfront - Criminal Activity]], Anti-Defamation League.</ref> The attacks were prompted by a newspaper article about several members of Eugene's Jewish community, including a lesbian. Community organizations, including a local gay rights group, responded by standing vigil outside the synagogue during [[Passover]] services.<ref name=Comstock>[[#refComstock2002|Comstock (2002)]], [https://books.google.com/books?id=ok81T1TM9LkC&pg=PA116 p.&nbsp;116].</ref> Lord and an associate were caught and convicted, and Lord was sentenced to four and a half years in prison.<ref name=ADL/>

On October 25, 2002 Jacob Laskey, his brother Gabriel Laskey, Gerald Poundstone, Jesse Baker, and one other man, all members of the Volksfront, drove to Beth Israel with the intent of intimidating the congregants. While a service with 80 members attending was taking place, the men threw rocks etched with [[Nazi]] [[swastika]]s through the synagogue's stained glass windows, then sped off.<ref name=ADL/> The men were caught, pleaded guilty, and were convicted. They served sentences ranging from a 6-month work release term and five years probation, to eleven years and three months in federal prison for the ringleader, Jacob Laskey.<ref name=SalemNews>[[#refSalemNews2007|''The Salem News'' (November 14, 2007)]].</ref><ref name=USAO2006>[[#refUSAO2006|United States Attorney's Office District of Oregon (August 15, 2006)]].</ref>

===East 29th Avenue building===
[[File:Temple Beth Israel, Eugene.JPG|right|thumb|Temple Israel's East 29th Avenue building|300px]]
Originally sized for 75 families, Temple Beth Israel's Portland Street building had been renovated and enlarged over the years to {{convert|7500|sqft}} to accommodate 250 families and 150 students.<ref name=Haist/><ref name=Wright19990415/> Despite these additions and the loss of members to Congregation Ahavas Torah, the synagogue was not large enough, particularly during the [[High Holidays]], when extra space had to be rented.<ref name=Reichman/> In 1997 the congregation purchased the property of the University Street Christian Church for $500,000 (today ${{formatnum:{{inflation|US|500000|1997|r=-4}}}}),<ref name=Wright19990415/> and began planning for a new facility.<ref name=Haist/> The members considered renovating the existing building on the property, but felt a new building would better suit their requirements, and razed the church.<ref name=Wright19990415/>

In 2003 the congregation got a permit to begin construction of a new facility on the now-vacant {{convert|1.37|acre|sing=on}} plot of land at the northwest corner of East 29th Avenue and University Street.<ref name=Harwood>[[#refHarwood2003|Harwood (2003)]].</ref> An initial capital campaign raised more than $1.8 million, which fully paid for the land, and by August 2007 an additional $1.7 million had been raised towards anticipated overall project costs of $5 million.<ref name=Reichman/>

The environmentally sensitive building was designed by Mel Solomon and Associates of Kansas City and local company TBG Architects & Planners,<ref name=Harwood/> and built by McKenzie Commercial Construction of Eugene.<ref name=KVAL>[[#refKVAL2008|KVAL-TV Web Staff (June 11, 2008)]].</ref> The building used "energy efficient heating, ventilation and lighting":<ref name=Reichman/> specific design issues with the building's energy efficiency included the fact that the largest room in the building, the sanctuary, was also the least-used, and, in accord with Jewish tradition, had to face east (towards [[Jerusalem]]).<ref name=Reeves>[[#refReeves2005|Reeves (2005)]].</ref>

On June 8, 2008 the congregation dedicated its new building at 1175 East 29th Avenue. At approximately {{convert|25000|sqft}},<ref>[[#refKVAL2008|KVAL-TV Web Staff (June 11, 2008)]] says "The new temple is 24,000 square feet", while [[#refHaist2008|Haist (2008)]] calls it a "26,000-square-foot facility".</ref> the facility included a sanctuary, commercial kitchen, banquet facilities, and classrooms, and housed the synagogue, the Lane County Jewish Federation, and the local Jewish Family Service. The project ended up costing $6 million, of which $4 million had been raised.<ref name=Haist/>

Made of concrete, steel, and wood,<ref name=Haist/> the building achieved [[Leadership in Energy and Environmental Design]] compliance "through the integration of stormwater management strategies, high efficiency irrigation, the use of recycled and/or recyclable materials, and drought tolerant plantings."<ref name=Schirmer>[[#refSchirmer|"Temple Beth Israel - Eugene, Oregon", Schirmer + Associates LLC website]].</ref> Completely recyclable materials used in the structure included carpeting and wood beams.<ref name=Reichman/>

==Recent events==
In 2008, Temple Beth Israel participated in Banners Across America, an "interfaith witness against torture coordinated by the [[National Religious Campaign Against Torture]]," as part of the Jewish Campaign Against Torture. Organized by [[Rabbis for Human Rights]]—North America in honor of Torture Awareness Month, the Jewish campaign included over 25 synagogues which hung banners protesting "the use of abusive interrogation techniques by the American military and intelligence community".<ref name=Kahn-Troster>[[#refKahn-Troster2008|Kahn-Troster (2008)]].</ref> That year, congregational membership reached almost 400 families, and the [[Talmud Torah]] and [[pre-school]] had about 200 and 40 students respectively.<ref name=Haist/>

The congregation sold the old synagogue building on Portland Street to Security First (Portland Street) Child Development Center for $815,000 in 2009, carrying the Center's financing. The building was converted for use as an educational center, while retaining some of the original architectural elements.<ref name=Bjornstad2009/> Difficult economic conditions forced the Child Development Center to give up the building in 2011, and Eugene's [[Network Charter School]] planned to move into it in autumn 2011.<ref name=Bjornstad2009/><ref name=KVAL2011>[[#refKVAL2011|KVAL Communities Staff (May 9, 2011)]].</ref>

Harris announced he would be stepping down as rabbi in 2011, and the synagogue hired Boris Dolin as his successor.<ref name=TBInewsletter2011>[[#refTBInewsletter2011|Temple Beth Israel newsletter (May/June 2011)]].</ref> Born and raised in Oregon, Dolin had worked at Temple Beth Israel as a teacher and youth group adviser from 1999 to 2001. A graduate of the University of Oregon, with a master's degree in Jewish Education from the [[Jewish Theological Seminary of America|Jewish Theological Seminary]], he was ordained by the [[Reconstructionist Rabbinical College]].<ref name=TBIRabbispage>[[#refTBIRabbispage|"Our Rabbis", Temple Beth Israel website]].</ref>

{{As of|2011}}, Temple Beth Israel was the largest synagogue in Eugene.<ref name=TBIhomepage>[[#refTBIhomepage|Temple Beth Israel website]].</ref> It was a member of the Community of Welcoming Congregations, "an Oregon and SW Washington interfaith ministry and advocacy organization working toward full inclusion and equality for transgender, lesbian, bisexual, gay and questioning persons."<ref name=CWC>[[#refCWC|Community of Welcoming Congregations, Our Member Congregations]].</ref> The rabbis were Yitzhak Husbands-Hankin and Boris Dolin.<ref name=TBIhomepage/>

==Notes==
{{reflist|colwidth=28em}}

==References==
{{refbegin|colwidth=39em}}
* <span id="refAbrams" class="citation">Abrams, Ruth. [http://www.interfaithfamily.com/spirituality/spirituality/Welcoming_The_Stranger_Or_Just_Welcoming.shtml "Welcoming The Stranger Or Just Welcoming"], [[InterfaithFamily.com]]. Accessed July 10, 2011.</span>
* <span id="refADL" class="citation">[http://www.adl.org/learn/ext_us/volksfront/crime.asp?LEARN_Cat=Extremism&LEARN_SubCat=Extremism_in_America&xpicked=3&item=volksfront Volksfront - Criminal Activity], Extremism in America, [[Anti-Defamation League]]. Accessed July 10, 2011.</span>
* <span id="refAAD" class="citation">{{cite web|url= http://communities.aia.org/sites/hdoaa/wiki/American%20Architects%20Directories/1970%20American%20Architects%20Directory/Bowker_1970_W.pdf |title="Waechter, Heinrich Hormuth" }}&nbsp;{{small|(11.7&nbsp;MB)}}, American Architects Directory, [[R.R. Bowker]], 1970.</span>
* <span id="refBennett1987" class="citation">Bennett, Randi. [https://news.google.com/newspapers?id=t-hVAAAAIBAJ&sjid=eeEDAAAAIBAJ&pg=6558,7644397 "Cantor strives to become rabbi"], ''[[The Register-Guard]]'', October 31, 1987, p.&nbsp;14C.</span>
* <span id="refBjornstad1996" class="citation">Bjornstad, Randi. [https://news.google.com/newspapers/p/register_guard?id=WUdWAAAAIBAJ&sjid=6-oDAAAAIBAJ&pg=2278,4630687 "Rabbi Myron Kinberg dies"], ''[[The Register-Guard]]'', April 20, 1996, pp.&nbsp;1A, 16A.</span>
* <span id="refBjornstad2009" class="citation">Bjornstad, Randi. [http://special.registerguard.com/csp/cms/sites/web/living/lifestyles/18172845-46/story.csp "Handed down. Temple Beth Israel helps child care center move into its old synagogue"], ''[[The Register-Guard]]'', August 9, 2009, p.&nbsp;E1.</span>
* <span id="refCWC" class="citation">[http://www.welcomingcongregations.org/membership.html Our Member Congregations], Community of Welcoming Congregations. Accessed July 10, 2011.</span>
* <span id="refComstock2002" class="citation">Comstock, Gary David. ''Unrepentant, Self-Affirming, Practicing: Lesbian/Bisexual/Gay People Within Organized Religion'', [[Continuum International Publishing Group]], 2002. ISBN 978-0-8264-1429-8</span>
* <span id="refCongregationAhavasTorah" class="citation">[https://web.archive.org/web/20090311002411/http://www.minyan.us/index_files/AboutUs.htm About Us], Congregation Ahavas Torah website, March 11, 2009. Archived at the [[Internet Archive]]. Accessed July 10, 2011.</span>
* <span id="refElon2000" class="citation">[[Ari Elon|Elon, Ari]]. ''Trees, Earth, and Torah: A Tu B'Shvat Anthology'', [[Jewish Publication Society]], 2000. ISBN 978-0-8276-0717-0</span>
* <span id="refGreene1996" class="citation">Greene, John Robert; Baron, Karrie A. ''Syracuse University: The Tolley Years, 1942–1969'', [[Syracuse University Press]], 1996. ISBN 978-0-8156-2701-2
* <span id="refHaist2008" class="citation">Haist, Paul. [http://www.jewishreview.org/node/13283 "Temple Beth Israel celebrates new home"], ''Jewish Review'', Volume 50, Issue 21, July 1, 2008.</span>
* <span id="refHarwood2003" class="citation">Harwood, Joe. [http://www.thefreelibrary.com/Temple+Beth+Israel+to+build+new+synagogue,+school.(Business)-a0105638412 "Temple Beth Israel to build new synagogue, school"], ''[[The Register-Guard]]'', July 15, 2003.</span>
* <span id="refHusbands-Hankin2004" class="citation">Husbands-Hankin, Yitzhak. [http://www.tikkun.org/article.php?story=may2004_husbands-hankin "Justice at the Checkout Counter"], [[Tikkun (magazine)|''Tikkun'' magazine]], May/June 2004.</span>
* <span id="refKahn-Troster2008" class="citation">Kahn-Troster, Rachel. [http://www.rhr-na.org/story/banners_update "More than 25 synagogues hang Stop Torture banners in honor of Torture Awareness Month"], [[Rabbis for Human Rights]] North America website, June 1, 2008. Accessed September 10, 2008.</span>
* <span id="refKurzweil2003" class="citation">[[Arthur Kurzweil|Kurzweil, Arthur]]. ''Best Jewish Writing 2003'', [[Jossey-Bass]], 2003. ISBN 978-0-7879-6771-0</span>
* <span id="refKVAL2011" class="citation">KVAL Communities Staff, [http://southeugene.kval.com/news/community-spirit/school-looks-move-former-synagogue/246620 "School looks to move into former synagogue"], [[KVAL-TV]] website, May 9, 2011.</span>
* <span id="refKVAL2008" class="citation">KVAL Web Staff, [http://www.kval.com/news/19768089.html "Torah moved to new Temple Beth Israel"], [[KVAL-TV]] website, June 11, 2008.</span>
* <span id="refLowenstein1987" class="citation">Lowenstein, Steven. ''The Jews of Oregon, 1850–1950'', Jewish Historical Society of Oregon, 1987. ISBN 978-0-9619786-2-4</span>
* <span id="refMP19941104" class="citation">[https://news.google.com/newspapers?id=C74jAAAAIBAJ&sjid=xNAFAAAAIBAJ&pg=7063,600212 "Rabbi Myron Kinberg bids farewell to Eugene"], ''[[Moscow-Pullman Daily News]]'', November 4, 1994, p.&nbsp;4B.</span>
* <span id="refMyrowitz1995" class="citation">Myrowitz, Catherine Hall. ''Finding a Home for the Soul: Interviews with Converts to Judaism'', Jason Aaronson, 1995. ISBN 978-1-56821-322-4</span>
* <span id="refReeves2005" class="citation">Reeves, Carol. [http://www.gazettetimes.com/articles/2005/05/30/news/religion/satrel01.txt "Extending care for the Earth"], ''[[Corvallis Gazette-Times]]'', May 27, 2005.</span>
* <span id="refRG19610128" class="citation">[https://news.google.com/newspapers?id=6v9VAAAAIBAJ&sjid=0-IDAAAAIBAJ&pg=6944,4223002 "Rabbi Simmons To Talk Monday"], ''[[The Register-Guard]]'', January 28, 1961, p.&nbsp;2.</span>
* <span id="refRG19610520" class="citation">[https://news.google.com/newspapers?id=UfpVAAAAIBAJ&sjid=1eIDAAAAIBAJ&pg=4846,3488479 "Rabbi, Pastors Accept Calls"], ''[[The Register-Guard]]'', May 20, 1961, p.&nbsp;3.</span>
* <span id="refRG19760906" class="citation">[https://news.google.com/newspapers?id=bvBVAAAAIBAJ&sjid=R-EDAAAAIBAJ&pg=5604,1391146 "Service held for Neimand, Eugene rabbi"], ''[[The Register-Guard]]'', August 6, 1976, p.&nbsp;3B.</span>
* <span id="refRG20030719" class="citation">"The Bulletin (Religion)", ''[[The Register-Guard]]'', July 19, 2003.</span>
* <span id="refReichman2007" class="citation">Reichman, Lynn. [http://www.jewishreview.org/node/68 "Eugene’s Temple Beth Israel halfway to new home"], ''Jewish Review'', Volume 49, Issue 24, August 15, 2007.</span>
* <span id="refRitualwell" class="citation">[http://www.ritualwell.org/lifecycles/intimacypartnering/Jewishweddingscommitmentceremonies/sitefolder.2005-06-07.6879878744/31GerToshav.xml "Brit Ger Toshav and Brit Nisuin"], Ritualwell.org, ''Kolot: The Center for Jewish Women’s and Gender Studies'' at the Reconstructionist Rabbinical College. Accessed July 10, 2011.</span>
* <span id="refRuach" class="citation">[http://www.ruachhaaretz.org/teachers.html "Teachers"], 2008 Summer Retreat, Ruach Ha'aretz website. Accessed July 10, 2011.</span>
* <span id="refSalemNews2007" class="citation">[http://www.salem-news.com/articles/november142007/rascist_sentenced_111407.php "White Supremacist from Oregon Sentenced for Attack on Synagogue"], The Salem News, November 14, 2007.</span>
* <span id="refSeldner2007" class="citation">Seldner, Deborah Moon. [http://www.jewishreview.org/node/8028 "Eugene's Or haGan on steep growth curve"], ''Jewish Review'', November 14, 2007.</span>
* <span id="refSinks1994" class="citation">Sinks, James. [https://news.google.com/newspapers/p/register_guard?id=yN9WAAAAIBAJ&sjid=ousDAAAAIBAJ&pg=4015,7292677 "Kinberg says he'll step down"], ''[[The Register-Guard]]'', July 30, 1994, pp.&nbsp;1A, 4A.</span>
* <span id="refSchirmer" class="citation">[http://www.schirmerassociates.com/projects/temple_beth_sust.html "Temple Beth Israel - Eugene, Oregon"], Schirmer + Associates, LLC website, Portfolio - Sustainability. Accessed July 10, 2011.</span>
* <span id="refTBIhomepage" class="citation">[http://www.tbieugene.org/ Temple Beth Israel website]. Accessed August 13, 2011.</span>
** <span id="refTBInewsletter2011" class="citation">[http://tbieugene.org/Newsletter_Maurice/2011_05_06_maurice.html "A Message from Rabbi Maurice"], Temple Beth Israel newsletter, May/June 2011.</span>
** <span id="refTBIRabbispage" class="citation">[http://www.tbieugene.org/rabbis_page.html "Our Rabbis] About TBI, Temple Beth Israel website. Accessed October 5, 2014.</span>
* <span id="refTepfer2010" class="citation">Tepfer, Gary. {{cite web|url= http://www.tbi-eugene.org/divrei_torah/2010_12_12_TAJL_Gary_Tepfer.pdf |title="This Jewish American Life" }}&nbsp;{{small|(268&nbsp;KB)}}, Temple Beth Israel website, December 12, 2010. Accessed July 10, 2011.</span>
* <span id="refUSAO2006" class="citation">United States Attorney's Office District of Oregon, [http://www.justice.gov/usao/or/PressReleases/20060815_Guilty_Pleas_Hate_Crime.htm "Guilty Pleas In Federal Hate Crime Case"], Press Release, August 15, 2006.</span>
* <span id="refWright1996" class="citation">Wright, Jeff. [https://news.google.com/newspapers?id=L0xWAAAAIBAJ&sjid=HOsDAAAAIBAJ&pg=2829,2979700 "Growing temple seeks its identity"], ''[[The Register-Guard]]'', October 12, 1996, pp.&nbsp;1B, 5B.</span>
* <span id="refWright19990312" class="citation">Wright, Jeff. [https://news.google.com/newspapers?id=plRWAAAAIBAJ&sjid=nusDAAAAIBAJ&pg=6736,2909552 "Scholar examines divisions at temple"], ''[[The Register-Guard]]'', March 12, 1999, pp.&nbsp;1C, 2C.</span>
* <span id="refWright19990613" class="citation">Wright, Jeff. [https://news.google.com/newspapers?id=-VZWAAAAIBAJ&sjid=o-sDAAAAIBAJ&pg=6929,3352905 "Women of the Cloth"], ''[[The Register-Guard]]'', June 13, 1999, pp.&nbsp;1A, 12A, 13A.</span>
* <span id="refWright2003" class="citation">Wright, Sally; Pinyerd, David; City of Eugene Planning and Development Department. {{cite web|url= http://www.hp-nw.com/images/Modernism/Eugene%20Modernism%201935-65.pdf |title="Eugene Modernism 1935–65" }}&nbsp;{{small|(9.05&nbsp;MB)}}, Historic Preservation Northwest, June, 2003.</span>
* <span id="refCLP" class="citation">[[Michael Zimmerman (biologist)|Zimmerman, Michael]]. [http://blue.butler.edu/~mzimmerm/Jewish_Clergy/RabbiSignatures.htm#H Rabbi Signatures], [[Clergy Letter Project]], [[Butler University]]. Accessed July 10, 2011.</span>
* <span id="refZuckerman2003" class="citation">Zuckerman, Phil. ''Invitation to the Sociology of Religion'', [[Routledge]], 2003. ISBN 978-0-415-94126-6</span>
{{refend}}

==External links==
* [http://www.tbieugene.org/ Temple Beth Israel website]

{{DEFAULTSORT:Beth Israel}}
[[Category:1930s establishments in Oregon]]
[[Category:20th-century attacks on synagogues and Jewish communal organizations]]
[[Category:21st-century attacks on synagogues and Jewish communal organizations]]
[[Category:Buildings and structures in Eugene, Oregon]]
[[Category:Culture of Eugene, Oregon]]
[[Category:Neo-fascist terrorism]]
[[Category:Reconstructionist synagogues in the United States]]
[[Category:Synagogues in Oregon]]