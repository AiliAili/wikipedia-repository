{{refimprove|date=April 2013}}
{{Infobox academic division
|name         = Goodman School of Business
|image        = File:Logo_Goodman_School_of_Business.jpg
|established  = {{Start date|1964}}
|type         = [[Business school]]
|undergrad    = 557<ref>[http://www.brocku.ca/webfm_send/30297]</ref>
|postgrad     = 198<ref>[http://www.brocku.ca/webfm_send/30297]</ref>
|dean         = Barry Wright (Interim)
|parent       = [[Brock University]]
|coor         = {{Coord|43.119|N|79.249|W|format=dms|region:CA_type:edu|display=inline,title}}
|city         = [[St. Catharines, Ontario|St. Catharines]]
|province     = [[Ontario]]
|country      = [[Canada]]
|former_names = Faculty of Business
|affiliation = [[Association to Advance Collegiate Schools of Business|AACSB]], [[Chartered Professional Accountant|CPA Ontario]], [[Certified General Accountants of Ontario|CGA Ontario]], [[Canadian Institute of Management|CIM]], [[European Foundation for Management Development|EFMD]], [[Beta Gamma Sigma|BGS]]
|website         = {{URL|goodman.brocku.ca}}
|}}

The '''Goodman School of Business''' (colloquially referred to as '''Goodman''') is the [[business school]] of [[Brock University]] in [[St. Catharines]], [[Ontario]], [[Canada]]. The business school offers programs at both the [[undergraduate]] and [[Graduate school|graduate]] level of study.

The Goodman School of Business is among the top five per cent of business schools worldwide to attain accreditation by the [[AACSB]]<ref>[https://www.aacsb.net/eweb/DynamicPage.aspx?Site=AACSB&WebCode=AccredSchlCountry]</ref> as well as membership in [[Beta Gamma Sigma]].<ref>[http://www.betagammasigma.org/chapterlist.htm Beta Gamma Sigma]</ref>

On October 12, 2012 Brock announced that its Faculty of Business was being renamed as the Goodman School of Business. The School is named after the family of [[Ned Goodman]], the businessman and investment expert, who provided the University with a transformational gift to the school.<ref>[http://brocku.ca/news/20741]</ref>

==Accreditations & Rankings==
=== Association to Advance Collegiate Schools of Business (AACSB) ===
The Goodman School of Business is accredited by The [[Association to Advance Collegiate Schools of Business]] (AACSB) International. Fewer than 5 percent of business schools worldwide (only 20 in Canada) have earned this accreditation, the highest standard in management education.<ref>[https://www.aacsb.net/eweb/DynamicPage.aspx?Site=AACSB&WebCode=AccredSchlCountry]</ref>

===Chartered Professional Accountants of Ontario (CPA Ontario)===
The Goodman School of Business received accreditation by the [[Chartered Professional Accountant|Chartered Professional Accountants of Ontario]] in November 2013. Goodman was the first business school in Ontario to be accredited to the Master’s level by CPA Ontario under the new National CPA Accreditation Program. Goodman offers graduates of the CPA-Accredited Program with direct access to the CPA Common Final Evaluation (CFE), which is the national qualifying evaluation for the CPA designation. The accreditation was given for the proposed CPA pathways, delivered through a combination of the Bachelor of Accounting (BAcc) and Master of Accountancy (MAcc) degree programs, based on its substantial equivalence to the new CPA Professional Education Program (CPA PEP).<ref>[http://www.cpaontario.ca/mediaroom/mediareleases/2013mreleases/1009page17500.aspx]</ref>

The Master of Business Administration (MBA) at the Goodman School of Business was accredited by the [[Chartered Professional Accountant|Chartered Professional Accountants of Ontario]] under the new National CPA Accreditation Program in March 2014. Goodman is currently the only business school in Ontario to offer a combined MBA and CPA program. The new accredited stream allows non-accounting university graduates to pursue an MBA and an accounting designation at the same time. The two-year graduate program also includes an optional co-op work term. The CPA/MBA program will offer a CPA-Accredited stream that provides graduates with advanced standing in the CPA Professional Education Program (CPA PEP).The CPA/MBA pathway is available to incoming students in the traditional MBA pathway and the MBA (International Student Pathway).<ref>[http://www.cpaontario.ca/mediaroom/mediareleases/2014mreleases/1009page17843.aspx]</ref>

===Canadian Institute of Management (CIM)===
The Bachelor of Business Administration (BBA) program is accredited by the [[Canadian Institute of Management]]. Upon graduating from the Bachelor of Business Administration program at Brock, students will have met all the academic requirements for the Certified in Management (C.I.M.) professional designation.  After completing two years of verifiable management work experience they may apply to the Canadian Institute of Management for this prestigious designation.

===Beta Gamma Sigma===
The Goodman School of Business is also a member of [[Beta Gamma Sigma]]. Beta Gamma Sigma is the [[honour society]] serving business programs accredited by [[AACSB]] International. Membership in Beta Gamma Sigma is the highest recognition a business student can receive in a business program accredited by [[AACSB]] International.<ref>[http://www.betagammasigma.org/prospectivemembers.htm Beta Gamma Sigma]</ref> Only five other Canadian Business Schools have attained membership in Beta Gamma Sigma.

The Goodman School of Business has its curriculum accredited by the Canadian Professional Sales Association (CPSA). BBA students in the marketing stream can complete the CPSA's Professional Sales Certificate Program while completing their degree.<ref>[http://www.cpsa.com/CSP/Professional_Sales_Certificate.aspx Canadian Professional Sales Association]</ref>

===Ranking===
[[The Globe and Mail]]'s ''2013 University Report Card'', which is a national survey on student satisfaction gave Brock University the following grades in the Medium-sized University category:<ref>[http://www.theglobeandmail.com/news/national/education/canadian-university-report/canadian-university-report-2013-student-satisfaction-survey-results/article4631980]</ref>
* Most Satisfied Students: B+ (4th in Canada)
* Campus Atmosphere: A- (4th in Canada)
* Academic Counselling: B+ (2nd in Canada)

In a survey of Deans from international schools of Business, The Goodman School of Business placed '''first in Canada''' in the Excellent Business Schools nationally strong and/or with continental links category.<ref>http://www.eduniversal-ranking.com/business-school-university-ranking-in-canada.html</ref>

The Goodman School of Business was named the [[Beta Gamma Sigma]] Bronze Collegiate Chapter in 2007 - the highest ranking attained by any Canadian Business School and currently holds the title of "Premier Chapter".<ref>[http://www.betagammasigma.org/outstanding.htm]</ref>

==Undergraduate Programs==
=== Business Administration (BBA) ===

The '''BBA''' program is professionally oriented and combines studies in business disciplines with studies in the Social Sciences, Humanities and Mathematics and Science. The program's first two years provide a foundation of business theory, while the third and fourth years are designed to allow students the option to concentrate on one particular area of business. The program offers a wide variety of concentrations, including: Accounting, Entrepreneurship, Finance, General Management, Human Resources Management, Information Systems, International Business, Marketing, Operations Management and Public Administration.

The BBA Program offers a Co-op component. With a nearly 100% placement rate (one of the highest and most consistent placement rates in the country) and co-op placements with Canadian companies.<ref>http://www.brocku.ca/co-op/future-students/benefits-of-co-op</ref>

===Accounting (BAcc)===
The '''BAcc''' program  leads to an [[accounting]] degree.<ref>http://goodman.brocku.ca/future/undergraduate/programs/bacc</ref> Brock's Accounting Co-op Program is the second largest in [[Canada]] and is accredited by CGA Ontario and is part of the Brock CPA Pathway that is fully accredited by CPA Ontario.

===Dual Degree Program===

The Goodman School of Business' '''Dual Degree Program''' is offered in association with The [[EBS University of Business and Law|EBS Business School]], International University Schloss Reichartshausen (EBS), which is ranked as one of the top business schools in Germany{{citation needed|date=April 2013}}.  GSB students complete the first five semesters at the Goodman School of Business and the remaining three semesters at EBS. Each student, upon successful completion of the course of studies will receive two degrees: The Bachelor of Business Administration degree from Brock University and a Bachelor of Science in Management degree from EBS Business School.<ref>[http://goodman.brocku.ca/future/undergraduate/programs/dual-degree-ebs]</ref>

==Graduate programs==
=== Master of Business Administration (MBA) ===

The '''MBA''' program is a two-year, full-time program starting each September at the [[St. Catharines]], [[Ontario]] campus. It offers [[professional development]] for graduates and executives. The program allows students to specialize in Accounting, Business Analytics, Finance, Operations Management, Marketing, or Human Resource Management.<ref>[http://brocku.ca/business/future/graduate/mbadegrees/mba/curriculum]</ref>

There is also a CPA/MBA Accredited Stream which allows non-accounting university graduates to pursue an MBA and an accounting designation at the same time. The two-year graduate program also includes an optional co-op work term. The CPA/MBA program will offer a CPA-Accredited stream that provides graduates with advanced standing in the CPA Professional Education Program (CPA PEP).<ref>[http://www.cpaontario.ca/MediaRoom/MediaReleases/2014mReleases/1009page17843.aspx]</ref>

The program also offers a Co-op option, where students can attain placements with companies to gain practical experience.

In 2011, [[Canadian Business]] Magazine ranked the Goodman School of Business' MBA '''9th in Canada''' in terms of percentage increase in salary from students entering the program to starting salary upon completion. It is one of three [[Ontario]] universities in this list.<ref>http://goodmbaguide.com/canadian-business-mba-guide-2011/</ref>

===Master of Accountancy (MAcc)===

The '''MAcc''' program is an advanced degree in [[accounting]] and related aspects of [[business]]. The Master of Accountancy (MAcc) program is designed to encourage the development of critical thinking, analysis and communication skills. By facilitating personal growth and the ability to adapt and respond to a complex and changing environment, the program helps you acquire advanced knowledge and accounting and related aspects of business. The BAcc and MAcc are accredited by CPA Canada.<ref>[http://brocku.ca/business/future/graduate/maccdegrees/macc Goodman MAcc program]</ref>

===Master of Science in Management (MSc)===

The '''MSc''' is a two-year, thesis-based program focusing on business research in one of four fields of study: [[accounting]], [[finance]], [[marketing]] or [[management science]]. The MSc curriculum is in a specialized area of study through course work and the completion of a research program.

The program is designed for students from a variety of disciplines who have completed a 4-year undergraduate degree.<ref>[http://brocku.ca/business/future/graduate/researchdegrees/msc Goodman MSc program]</ref>

==International Programs==

The Goodman School of Business has international partnerships with over 40 business schools in 21 different countries. Students can study while paying their regular tuition fees to Brock. They take courses in English that count towards their Brock degree. International exchange partnerships:

* [[University of South Australia]] (Australia)
* Innsbruck School of Management (Austria)
* [[Katholieke Universiteit Leuven]] (Belgium)
* [[Solvay Brussels School of Economics and Management]] (Belgium)
* [[University of São Paulo]] (Brazil)
* [[Beijing University of International Business and Economics]] (China)
* [[Copenhagen Business School]] (Denmark)
* [[Hanken School of Economics]] (Finland)
* Burgundy School of Business (ESC Dijon) (France)
* [[EDHEC Business School (Ecole des Hautes Etudes Commerciales du Nord)]] (France)
* [[Grenoble School of Management]] (France)
* [[IESEG School of Management]] (France)
* KEDGE Business School (formerly EUROMED Management) (France)
* NEOMA (formerly Reims Management School and Rouen Business School) (France)
* [[Paris Dauphine University]](France)
* [[Toulouse Business School]] (France and Spain)
* [[EBS University of Business and Law]] (Germany)
* [[University of Mannheim Business School]] (Germany)
* [[Munich Business School]] (Germany)
* [[Reutlingen University]] (Germany)
* [[WHU-Otto Beisheim School of Management]] (Germany)
* [[Birla Institute of Management Technology]] (India)
* [[Indian Institute of Foreign Trade]] (India)
* [[Indian Institute of Management Indore]] (India)
* [[Indian Institute of Management Lucknow]] (India)
* [[Free University of Bozen-Bolzano]] (Italy)
* [[University of Turin]] (Italy)
* [[Instituto Tecnológico Autónomo de México]] (Mexico)
* ESCA Ecole de Management (Morocco)
* [[BI Norwegian Business School]] (Norway)
* [[Kozminski University]] (Poland)
* [[Singapore Management University]] (Singapore)
* ESIC Business & Marketing School (Spain)
* [[University of Gothenburg]] (Sweden)
* [[Stockholm University School of Business]] (Sweden)
* ZHAW School of Management and Law (Switzerland)
* [[Maastricht University]] (The Netherlands)
* [[Aston Business School]] (United Kingdom)
* [[Leeds University Business School]] (United Kingdom)
* [[Nottingham Business School]] (United Kingdom)
* [[University of Surrey]] (United Kingdom)
* [[Strathclyde Business School]] (United Kingdom)<ref>[http://brocku.ca/business/international/exchange-programs/brock-students International Exchanges at Goodman]</ref>

==Student life==

The Goodman School of Business student associations run the majority of the on-campus activities for students such as inter-collegiate business competitions, socials, networking and professional development events, as well as the annual formal. The following student clubs are part of the Goodman School of Business:

* Goodman Business Students' Association (BSA)
* Goodman Graduate Business Council (GBC)
* Goodman Accounting Students' Association (ASA)
* Brock Innovation Group (BIG) at Goodman School of Business
* Brock Finance and Investment Group (BFIG) at Goodman School of Business
* Goodman Human Resource Management Association (HRMA) 
* Goodman Student Ambassador Association (SAA)
* Brock Marketing Association (BMA) at Goodman School of Business
* Goodman DECA <ref>[http://brocku.ca/business/current/undergraduate/get-involved Student Life at Goodman]</ref>

==Goodman School of Business Consulting Group==
The Goodman School of Business Consulting Group employs students enrolled in The Goodman School of Business' MBA program for one year terms to offer consultancy services to organizations in the Niagara Region. The Consulting Group employs MBA candidates to provide clients with professional services. Through direct consultant experience, or with the support of faculty experts, the Consulting Group assists organizations with Marketing, Accounting, Finance, Strategy and Human Resources.<ref>[http://brocku.ca/business/employers/consulting/ Goodman School of Business Consulting Group]</ref>

==Centre for Innovation, Management and Enterprise Education==

The Centre for Innovation, Management and Enterprise Education (formerly known as the Management Development Centre) offers management and leadership development courses and seminars for middle and senior managers. Operated out of the Goodman School of Business, CIMEE is for managers and executives working in both private and public sector environments.<ref>[http://goodman.brocku.ca/community/CIMEE Centre for Innovation, Management and Enterprise Education]</ref>

==Taro Hall==

Taro Hall houses the Goodman School of Business. It was opened in 1990 and was designed by [[Moriyama & Teshima Architects]]. Taro Hall is primarily used for administrative offices, including the Dean's office and lecture halls. It contains six lecture halls for undergraduate students (BBA & BAcc), three lecture halls for graduate students ([[MBA]], MAcc, IMAcc, and MSc), and a number of break-out rooms for group study. There is also a student lounge and computer commons specifically for graduate business students.

==Alumni==
''For notable alumni and affiliates, see [[List of Brock University people]].''

The Goodman School of Business established the Distinguished Graduate Award to recognize an outstanding graduate who has excelled in their chosen field. Recipients are honoured for their professional, educational, community and/or volunteer leadership and achievements. Past Recipients include:<ref>http://goodman.brocku.ca/alumni/graduates</ref>
* 2002: Doug Wilkinson, CA (BBA, '91) — Partner, National Leader for Control Assurance [[Deloitte & Touche LLP]]
* 2003: Debi Rosati, CA (BAcc, '84) — Venture Capitalist
* 2004: Dr. Paul Ingram (BBA, '90) — Professor, [[Columbia University]]
* 2005: Debbie Sevenpifer CA (BAdmin, '87) — President & CEO, [[Niagara Health System]]
* 2006: Dr. Stephen Young (BAcc, '91) — Managing Director of Accounting Policy, [[Citigroup]]’s Corporate and Investment Bank (CIB)
* 2007: Jim Ryan, CA (BAdmin, '84) — CEO, St. Minver Limited
* 2008: John Zoccoli, CA (BAdmin, '86) — President, Jamzoc Holdings Limited
* 2009: James MacLellan (BAdmin, '87) — Partner, [[Borden Ladner Gervais]] LLP
* 2010: Kristian Knibutat (BAdmin '86) — Partner, [[PricewaterhouseCoopers]] LLP
* 2011: Jeff Park (BAcc '95) — Executive Vice President and CFO, SXC Health Solutions Corp.
* 2012: Fred Losani (BAdmin, '87) - CEO, Losani Homes
* 2013: Anne-Marie Robinson (BBA, '90) - President, [[Public Service Commission of Canada]]
* 2014: Mark Arthur (BA '77) - President, Industrial Alliance Private Weatlh Management
* 2015: Jason Sparaga (BBA '93) - Co-Founder and Co-CEO of Spark Power Corp.

==Notes==
{{Reflist|2}}

==External links==
* {{official|https://brocku.ca/business/}}
* [http://www.ontarioplaques.com/Plaques_MNO/Plaque_Niagara52.html Ontario Plaque Brock University]

{{Brock University}}

[[Category:Brock University]]
[[Category:Business schools in Canada]]
[[Category:Accounting in Canada]]