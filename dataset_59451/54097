{{Use dmy dates|date=March 2012}}
{{Use Australian English|date=January 2012}}
{{Infobox rail line
| name          = Bridgewater railway line
| color         = {{Adelaide Metro color|closed}}
| image         = Mount lofty railway.jpg
| image_alt     = 
| caption       = 
| system        = 
| status        = 
| locale        = [[Adelaide, South Australia]]
| start         = [[Adelaide railway station|Adelaide]]
| end           = [[Bridgewater railway station|Bridgewater]]
| stations      = {{Plainlist|
* [[Goodwood railway station|Goodwood]]
* [[Mitcham railway station, Adelaide|Mitcham]]
* [[Blackwood railway station|Blackwood]]
* [[Belair railway station|Belair]]
* [[Mount Lofty railway station|Mount Lofty]]
* [[Aldgate railway station, Adelaide|Aldgate]]
}}
| open          = 1883
| routes        = 
| event1label   = Re-sleepered ([[concrete sleeper|concrete]])
| event1        = 
| event2label   = Electrified
| event2        = 
| close         = 23 September 1987
| owner         = 
| operator      = 
| depot         = 
| stock         = 
| linelength_km = 37.3
| notrack       = Quadruple Track to Goodwood<br>Single Track to Belair<br>(formerly double track)<br>Closed from Belair<br>(formerly single track)
| gauge         = 
| map           = 
| map_state     = collapsed
}}

The '''Bridgewater railway line''' is a former passenger railway service on the [[Adelaide-Wolseley railway line|Adelaide to Wolseley line]] in the [[Adelaide Hills]]. It was served by [[TransAdelaide]] suburban services from [[Adelaide railway station|Adelaide]]. In September 1987, the service was curtailed to [[Belair railway station|Belair]]. In 1995, the line was converted to [[standard gauge]] as part of the [[One Nation (Infrastructure)|One Nation]] infrastructure program, disconnecting these stations from the broad gauge suburban railway system.

== History ==
The line from Adelaide to [[Belair railway station|Belair]]/[[Bridgewater railway station|Bridgewater]] opened in 1883. The Bridgewater line headed east from [[Belair railway station|Belair]] parallel to the northern side of [[Belair National Park]]. The line then turned south through the national park and then turned east again, where the [[National Park railway station, South Australia|National Park station]] used to be. It continued east past [[Long Gully railway station|Long Gully]] and [[Nalawort railway station|Nalawort]] stations to the [[Upper Sturt railway station|Upper Sturt station]], 28.9 kilometres from [[Adelaide railway station|Adelaide station]]. 500m later the track turned north east and continued to [[Mount Lofty railway station|Mount Lofty station]], 31&nbsp;km from Adelaide.  After that it turned south and reached [[Heathfield railway station, Adelaide|Heathfield station]] (33&nbsp;km), just after the line turned north east. It reached the village of [[Aldgate, South Australia|Aldgate]] just as it passed the [[Madurta railway station|Madurta station]], then the track reached the [[Aldgate railway station, Adelaide|Aldgate station]] (34.5&nbsp;km). The line continued east, passing the [[Jibilla railway station|Jibilla]] and [[Carripook railway station|Carripook]] stations and finally, the line terminated at [[Bridgewater railway station|Bridgewater station]], 37.3&nbsp;km by rail from Adelaide Railway Station.

The Bridgewater line had a fairly steep grade for most of the journey, sometimes resulting in derailments due to the tight bends. Services from Adelaide to Bridgewater usually took an average of one hour (stopping all stations), and about 50 minutes (express). Only one train per two hours operated during off-peak and weekends (most trains terminated at Belair) and no more than two trains per hour in either direction during peak-hours. This was because the line was single track (which is still the case today) with crossing loops located at Belair, Long Gully, Mount Lofty, Aldgate and Bridgewater.

== Services ==
Services on the Bridgewater line were mainly operated by [[Redhen railcar]]s, with the [[2000 class railcar]]s occasionally used in its final years.

On special occasions after 1987, such as the [[Oakbank Easter Racing Carnival]] held every Easter weekend at [[Oakbank, South Australia|Oakbank]], trains ran further east to terminate at [[Balhannah railway station|Balhannah]] station. However, this service ceased prior to the standard gauge conversion, due to the expense of operating the line.{{Citation needed|date=August 2008}}.

== Closure ==
When the more direct [[South Eastern Freeway]] opened in the 1960s, patronage to Bridgewater declined heavily, as more people had access to cars and the car journey was much quicker and shorter. In 1985, the [[State Transport Authority (South Australia)|State Transport Authority]] sought to have the service withdrawn. The line had 12 services on weekdays, nine on Saturdays and five on Sundays.<ref>"South Australia" ''[[Railway Digest]]'' June 1985 page 193</ref> 

On 23 September 1987, passenger services to Bridgewater were withdrawn, attributed to high cost of operation and low passenger numbers. All stations beyond Belair were closed, and all suburban trains now terminate at Belair.<ref name=WHCallaghan>{{cite book|last1=Callaghan|first1=WH|title=The Overland Railway|date=1992|publisher=Australian Railway Historical Society|location=Sydney|isbn=0 909650 29 2|page=217}}</ref>

In 1995, the [[Adelaide-Wolseley railway line|Adelaide to Wolseley line]] was converted from [[broad gauge]] (1600mm) to [[standard gauge]] (1435 mm) ruling out any restoration of local trains to Bridgewater or beyond, and it disconnected a number of the South Australian country broad gauge services from Adelaide. Between [[Goodwood railway station|Goodwood]] and Belair, the former double track route became two parallel single lines, one broad gauge for [[Adelaide Metro]] suburban services, the other standard gauge freight services.<ref>[http://www.artc.com.au/library/RAS_D3.pdf D3 Wolseley to Mile End] Australian Rail Track Corporation</ref>

Along with this conversion, stations on the Belair line at [[Mile End Goods railway station|Mile End Goods]], [[Millswood railway station|Millswood]], [[Hawthorn railway station, Adelaide|Hawthorn]] and [[Clapham railway station, Adelaide|Clapham]] closed. The other Belair line stations each had one platform closed.

[[Millswood railway station|Millswood]] reopened on 12 October 2014 for a 12-month trial. The trial was successful, and the station was reopened permanently.

== Stations ==
{{BS-map|title=Bridgewater Line|title-bg=#000000|title-color=white|top={{center|{{bs-q|STRq|0}} Broad Gauge, {{bs-q|uSTRq|0}} Standard Gauge}}|map=
{{BS2||KINTa|0.0 km|'''{{rwsa|Adelaide}}''' {{rint|tram|1}} <ref>Universal Press (2002), UBD on Disk Adelaide</ref>||}}
{{BS4|dCONTgq|eABZq+r|ABZrf|d||Adelaide Junction|{{right|''to [[Gawler Central railway line|Gawler]], [[Grange railway line|Grange]] & [[Outer Harbor railway line|Outer Harbor]]''}}}}
{{BS4|udCONTgq|xmABZlg|STR|d||||''[[Adelaide-Port Augusta railway line|Adelaide-Port Augusta line]] to [[Crystal Brook, South Australia|Crystal Brook]]''}}
{{BS2|uLSTR|LSTR||||}}
{{BS2|uBHF|STR||'''[[Adelaide Parklands Terminal]]'''||}}
{{BS2|uLSTR|LSTR||||}}
{{BS2|ueHST|INT|5.0 km|{{rwsa|Goodwood}}||}}
{{BS4|udCONTgq|uKRZu|mKRZu|udCONTfq||||[[Glenelg Tram]]}}
{{BS4|dCONTgq|umKRZo|ABZrf|d||Goodwood Junction|{{right|''to [[Seaford railway line|Seaford]] & [[Tonsley railway line|Tonsley]]''}}}}
{{BS2|ueHST|KBHFxe|21.5 km|'''[[Belair railway station|Belair]]'''|{{pad|10px}}''Current terminus of [[Adelaide Metro]] [[Belair railway line|Belair line]] suburban services''}}
{{BS2|uKRWl|exSTR|O2=uKRW+r||||''Crossing loop until 1995''}}
{{BS2||uSKRZ-Au|||''Sir Edwin Avenue''}}
{{BS2||uTUNNEL1|||''No. 6 Tunnel''}}
{{BS2||ueHST|25.4 km||''{{rwsa|National Park|SA}} (Closed April 1987)''}}
{{BS2||ueHST|26.7 km||''{{rwsa|Long Gully}} (Closed April 1987) – Crossing loop until 1995''}}
{{BS2||uTUNNEL1|||''No. 7 Tunnel''}}
{{BS2||ueHST|||''{{rwsa|Nalawort}} (Closed 1950s)''}}
{{BS2||ueHST|28.9 km||''{{rwsa|Upper Sturt}} (Closed April 1987)''}}
{{BS2||uTUNNEL1|||''No. 8 Tunnel''}}
{{BS2||ueBHF|31.0 km||''{{rwsa|Mount Lofty}} (Closed April 1987)'' – Crossing loop}}
{{BS2||uSKRZ-Au|||''Avenue Road''}}
{{BS2||ueHST|33.0 km||''{{rwsa|Heathfield|A}} (Closed April 1987)''}}
{{BS2||uBUE|||''Cricklewood Road''}}
{{BS2||ueHST|33.7 km||''{{rwsa|Madurta}} (Closed April 1987)''}}
{{BS2||ueBHF|34.5 km||''{{rwsa|Aldgate|A}} (Closed April 1987) – Crossing loop until 1995''}}
{{BS2||uSKRZ-Ao|||''Mount Barker Road''}}
{{BS2||uBUE|||''Yatina Road''}}
{{BS2||ueHST|35.6 km||''{{rwsa|Jibilla}} (Closed 1987)''}}
{{BS2||uBUE|||''Kalin Avenue''}}
{{BS2||ueHST|36.2 km||''{{rwsa|Carripook}} (Closed April 1987)''}}
{{BS2||uBUE|||''Bridgewater Road''}}
{{BS2||ueBHF|37.2 km||'''''{{rwsa|Bridgewater}}''' (Closed April 1987)''}}
{{BS2||uCONTf||||''[[Adelaide-Wolseley railway line|Adelaide-Wolseley line]] to [[Southern Cross railway station|Melbourne]]''}}
{{BS2||||||''to [[Victor Harbor railway line|Victor Harbor]] & [[Mount Pleasant railway line|Mount Pleasant]] lines''}}}}

==References==
{{Reflist}}

{{Closed Railway lines of Adelaide}}

[[Category:Closed railway lines in South Australia]]
[[Category:Railway lines opened in 1883]]
[[Category:Railway lines closed in 1987]]
[[Category:Standard gauge railways in Australia]]