{{Infobox mountain
| name = Jebel Hafeet (Al Ain)
| photo = Jabal hafeet shahin.jpg
| photo_caption = 
| elevation_m = 1249
| elevation_ref = 
| prominence_m =
| prominence_ref = 
| parent_peak = 
| listing = 
| range = 
| location = [[Al Ain]], [[Abu Dhabi (emirate)|Abu Dhabi]], [[United Arab Emirates]]
| map = United Arab Emirates
| map_caption = United Arab Emirates
| map_size = 260
| lat_d = 24 | lat_m = 03 | lat_s = 31 | lat_NS = N
| long_d= 55 | long_m= 46 | long_s= 39 | long_EW= E
| coordinates_ref =
| topo = 
| type = 
| first_ascent = 
| easiest_route = 
}}

[[File:Top of Jebel Hafeet.jpg|thumb|Top of Jebel Ḥafeeṫ at Night.]]

'''Jabal Ḥafeeṫ''' ({{lang-ar|جبل حفيت|Jabal Ḥafiṫ}}) (variously transscribed Jabal, Jabel and Jebal and Hafit or Hafeet - literally "empty mountain") is a [[mountain]] located primarily in the environs of [[Al Ain]], which itself is in the [[Emirate of Abu Dhabi]] in the [[UAE]].<ref>Brian McMorrow, [http://www.pbase.com/bmcmorrow/jabal_hafeet JabalHafeet].</ref> Part of the mountain straddles the border with [[Oman]], while the summit is located wholly within United Arab Emirates.

==Geography==
===Orography===
The Jebel Hafeet is about 26 km long and 4-5 km wide, extending from north to south. The range is asymmetric and in the eastern part is much steeper (25 ° -40 °) than the west side.

The mountain rises {{convert|1249|m|ft|0}} and offers an impressive view over the city. Jebel Hafeet was a well-known landmark throughout the area's history and is a contemporary tourist attraction. An extensive natural cave system winds through Jabal Hafeet.

It is often incorrectly labelled UAE's highest mountain (as it is certainly the most well-known); this honor actually belongs to Jabal Jais, a 1,925 m/  6315 ft. mountain in Ras Al Khaimah and is the UAE's highest named peak. Roadworks to reach the peak are now complete.

===Geology===
The mountain is composed of many distortions of individual layers of rock rich in fossils of plankton, while at the foot of the mountain it's possible to find various marine fossils (such as coral or crabs).

The Jebel Hafeet is crossed by a system of caves, some of which have been explored to a depth of no more than 150 meters. In the caves there are stalagmites and stalactites well preserved. Access to the caves is partly natural, while in other parts of the city of Al Ain the entrance is blocked.

At the foot of Jebal Hafeet lies the "Green Mubazarrah", which is a major tourist attraction: several hot springs flow into small streams and form a lake, while pools and hot tubs are spread throughout the area. 

==Flora and fauna==
On the mountain it has been osserved the yellow bloom of ''[[Acridocarpus orientalis]]''.

The caves of Jebal Hafeet are a natural habitat for a wide range of animals, including bats, foxes, snakes and rodents similar to hyraxes. The lizard ''[[Acanthodactylus opheodurus]]'', which until 1982 was considered extinct in the UAE, has been observed in the area. Among the birds, there is the greatest biodiversity of the whole country: a study counted 119 species of birds. Finally, they have been cataloged about 200 different insects and 23 species of butterflies. 

==Archaeology==
{{Infobox World Heritage Site
| WHS         = Cultural Sites of Al Ain (Hafit, Hili, Bidaa Bint Saud and Oases Areas)
| Image       = [[File:Alley in Al Ain Oasis.JPG|260px|alt=A dirt and cobblestone road runs through the center of the image, flanked by low plastered walls and palm trees.|An alley at the Al Ain Oasis]]
| imagecaption= An alley at the Al Ain Oasis
| State Party = [[United Arab Emirates]]
| Type        = Cultural
| Criteria    = iii, iv, v
| ID          = 1343
| Region      = [[List of World Heritage Sites in Western Asia|Arab States]]
| Year        = 2011
| Session     = 35th
| Link        = http://whc.unesco.org/en/list/1343
}}
At the foothills of the mountain,  500 tombs were excavated that dated between 3200 and 2700 BC. While the graves on the north side have been partially destroyed by the construction projects, the southern tombs are preserved. Some of the tombs contain skeletons, some of which are adorned with bronze objects and pearls.  Other objects found in the tombs include ceramics of Mesopotamia, witnessing to the ancient trade relations that existed at that time.

Because of its exceptional archaeological and historical value, in 1993, the "Desert Park and the tombs" (which includes the Jebel Hafeet) was inscribed on the list of UNESCO World Heritage Sites as the "''Cultural Sites of Al Ain: Hafit, Hili, Bidaa Bint Saud and Oases Areas''".

==Tourism==
[[File:Hafeetnight.JPG|thumb|left|Jebel Ḥafeeṫ by night.]]
Famous tourist center of the area from the top of which has a broad view over the whole area, a hotel and a radar station was built onto the mountain.

At the foothills of Jabal Ḥafeeṫ lies the Green Mubazarrah, a well-developed tourist attraction. At the Green Mubazarrah, hot-water springs gush forth in little streams and form a [[lake]]. Swimming pools and jacuzzis are scattered all over the Green Mubazarrah. Jabal Ḥafeeṫ is also home to a wide range of animals including bats, foxes, and snakes.

On top of the mountain, there is a [[radar station]] and a [[Mercure Hotels|Mercure-Hotel]],<ref>[http://www.mercure.com/gb/hotel-3573-mercure-grand-jebel-hafeet-al-ain/index.shtml Mercure Grand Jebel Hafeet Al Ain], [[Mercure Hotels]].</ref> as well as a palace.

== Ḥafeeṫ Mountain Road ==
[[File:Road to Jebel Hafeed.jpg|thumb|left|Road leading to Mount Ḥafeeṫ.]]
[[File:Road to Jabel Hafeeth in Al Ain.jpg|thumb|right|Road to Jabel Hafeeth in Al Ain]]
The Jebel Ḥafeeṫ Mountain Road, built in 1980, extends for 7.4&nbsp;mi (11.7&nbsp;km) up the mountain, rising 4,000&nbsp;ft (1,219&nbsp;m). With 60 turns and three lanes (two climbing and one descending), the immaculate road was called the greatest driving road in the world by [[Edmunds.com]].<ref>{{cite news|author=Gautam Sharma|title=Great driving roads: Jebel Hafeet Mountain Road, Al Ain|newspaper=The National|url=http://www.thenational.ae/arts-lifestyle/motoring/great-driving-roads-jebel-hafeet-mountain-road-al-ain|date=2015-01-29}}</ref> The road scales the mountain and ends at a parking lot with only a hotel and a palace belonging to the country's rulers. Part of the climax of the [[Bollywood]] film ''[[RACE (film)|RACE]]'' was shot on the Jabel Hafeet mountain.{{citation needed|date=October 2010}}

The road was built by [[Strabag International]] of [[Cologne]], [[Germany]].{{citation needed|date=October 2010}}

Ḥafeeṫ Mountain Road is the challenge for cyclists who frequently come over to train. The Jaball Hafeet Mercure Challenge is an annual road cycling competition taking place somewhere in January. National and international riders take part in climbing the 8% average ascent of the mountain.<ref>{{cite web|title=Jebel Hafeet Mountain Road|website=dangerousroads.org|url=http://www.dangerousroads.org/asia/united-arab-emirates/189-jebel-hafeet-mountain-road-uae.html|date=2011-03-29}}</ref> In 2015 it hosted the arrival of the third stage of [[2015 Abu Dhabi Tour|first edition]] of [[Abu Dhabi Tour]], a cycling race organized by [[RCS Sport]], won by Colombian [[Esteban Chaves]].

== References ==
{{reflist}}
[http://www.abudhabidesertsafari.ae/jebel-hafeet-abu-dhabi Jebel Hafeet Abu Dhabi]

== External links ==
{{commons category}}
* [http://maps.google.com.au/maps/ms?hl=en&ie=UTF8&split=0&gl=au&ei=srmaSbjfBdXJkAWWjtG3Cw&t=k&msa=0&msid=117341431900376908843.0004631d3e8844b004106&ll=24.085727,55.766172&spn=0.061433,0.084629&z=14 Jebel Hafeet Mountain Road on Google Maps]

[[Category:Mountains of the United Arab Emirates|Hafeet, Jebel]]
[[Category:Geography of Al Ain]]
[[Category:Tourist attractions in Al Ain]]
[[Category:Mountains of Oman]]
[[Category:Caves of the United Arab Emirates]]