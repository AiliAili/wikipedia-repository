{{Use dmy dates|date=March 2012}}
{{Use British English|date=March 2012}}
{{Infobox military person
| name          = Sir Francis John Linnell
| image         = Linnell meets Critchley 1943 IWM CM 5259.jpg
| image_size    = 300px
| caption       = Air Marshal Linnell (left) meets [[Alfred Critchley]] (right), the newly appointed Director-General of British Overseas Airways Corporation at an airfield in the Middle East, 1943
| nickname      = "Jack"
| birth_date    = {{birth date|1892|03|16|df=yes}}
| birth_place   = [[Margate]], Kent
| death_date    = {{death date and age|1944|11|3|1892|03|16|df=yes}}
| death_place   = Near [[Tetsworth]], Oxfordshire
| placeofburial = Charing Crematorium, [[Charing]]<ref name="Charing">Kent War Memorials Transcription Project, [http://www.kentfallen.com/PDF%20REPORTS/CHARING%20CREMATORIUM.pdf Charing Crematorium] p.15</ref>
| placeofburial_label = Cremated at
| allegiance    = United Kingdom
| branch        = [[Royal Navy]] (1914–18)<br/>[[Royal Air Force]] (1918–44)
| serviceyears  = 1914–44
| rank          = [[Air Marshal]]
| servicenumber = 
| unit          = 
| commands      = [[RAF Mildenhall]] (1934–35)<br/>[[No. 99 Squadron RAF|No. 99 Squadron]] (1934)<br/>[[RAF Hal Far|RAF Base Malta]] (1931–32)
| battles       = [[First World War]]<br/>[[Second World War]]
| awards        = [[Knight Commander of the Order of the British Empire]]<br/>[[Companion of the Order of the Bath]]<br/>[[Mentioned in Despatches]] (3)<br/>[[Legion of Merit|Commander of the Legion of Merit]] (United States)
| relations     = 
| laterwork     = 
}}
[[Air Marshal]] '''Sir Francis John Linnell''', {{postnominals|country=GBR|size=100%|sep=,|KBE|CB}} (16 March 1892 – 3 November 1944) was a senior [[Royal Air Force]] commander during the [[Second World War]]. He was Controller of Research and Development of the [[Ministry of Aircraft Production]] during the development of the [[Bouncing bomb]], the weapon eventually employed in [[Operation Chastise]]. 'Jack' was dubbed Sir Francis in the desert in front of the British press by King [[George VI of the United Kingdom|George VI]] and posthumously invested as a Commander in the [[Legion of Merit]] by U.S. President [[Franklin D. Roosevelt]].

==Career==
Francis John Linnell was born on [[Isle of Thanet]], [[Margate]] and was educated at [[Bloxham School]].<ref>''[[The Oxford Times]]'', 10 November 1944.</ref>

===First World War===
Linnell joined the [[Royal Naval Reserve]] as temporary warrant-telegraphist in September 1914, serving with the [[Royal Naval Air Service]] in France and Belgium.<ref>''The Dark Horse'', August 1943.</ref> He learnt to fly at the [[Grahame-White]] Flying School [[Hendon]]. Flying a Grahame-White [[Biplane]] he gained [[Royal Aero Club]] Certificate No.1338 in June 1915.<ref>{{cite journal |date=25 June 1915|title=Grahame-White Flying School|journal=Flight|pages=428|url=http://www.flightglobal.com/pdfarchive/view/1915/1915%20-%200452.html}}</ref> Flt Sub-Lt Linnell was responsible for carrying out trials in a special Blackburn-built B.E.2c. The unique aircraft No.3999 was ordered by Admiralty for W/T experiments, it was fitted with a 70&nbsp;hp Renault engine and had a top speed of 72&nbsp;mph. Blackburn-built B.E.2cs are recognisable by the ringed airscrew motif on the fin and saw active service in every theatre during the war.<ref name="flymachinesru">[http://flyingmachines.ru/Site2/Crafts/Craft25545.htm flymachines.ru]</ref><ref>{{cite journal |date=16 April 1954|title=Admirality order BE2 no.3999|journal=Flight|pages=482|url=http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%201078.html}}</ref><ref name="FJL">[http://rafweb.org/Biographies/Linnell.htm Air of Authority – A History of RAF Organisation], Air Marshal Sir Francis Linnell</ref>

From March 1916 till the end of 1919 he served with the [[Grand Fleet]] as a pilot on {{HMS|Campania|1914|6}}, {{HMS|Repulse|1916|6}} and {{HMS|Furious|47|6}}<ref name="LM22341">''Kent Messenger'', 22 March 1941.</ref> and was [[mentioned in despatches]]<ref>{{London Gazette|issue=30722|supp=yes|startpage=6521|date=3 June 1918|accessdate=9 January 2012}}</ref><ref>{{London Gazette|issue=31378|supp=yes|startpage=7041|date=3 June 1919|accessdate=9 January 2012}}</ref> twice. He ended the [[World War I|Great War]] with a permanent commission of captain in the newly formed [[Royal Air Force]].<ref>{{London Gazette|issue=31486|startpage=9867|date=1 August 1919|accessdate=11 January 2012}}</ref>

The [[Edwin Harris Dunning|Dunning Memorial Cup]],<ref name="Dunning">{{cite journal |last=|first= |authorlink=  |coauthors= |date=21 July 1921 |title=Dunning Cup – Award for 1920|journal=Flight|pages=495 |url=http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200495.html}}</ref> given annually to the officer that has done the most to further aviation in connection with the fleet, was awarded Linnell in 1920 "... for flights which led to important developments in artillery and reconnaissance observation."<ref name="times41144" />

===Inter war years===
From 1920 he worked as a [[Royal Air Force]] (RAF) Communications Officer at the Department of Civil Aviation, then transferred to Signals Staff duties at the [[Air Ministry]] in 1925. He was appointed an [[Officer of the Order of the British Empire]] in June 1923, the citation reads, "This officer has done exceptionally good work in connection with Signals. His liaison with the Navy and the Army has been excellent."<ref name="Charing" /> He received special thanks from the [[Air Council]] for preparing the handbook on air communications and intelligence systems for the air defence of Great Britain.<ref name="times41144">''[[The Times]]'', 4 November 1944.</ref>

His first command on 20 October 1930 was [[No. 9 Squadron RAF]] located at [[RAF Manston]]<ref>{{cite journal |date=28 November 1930|title=No.9 Squadron RAF, RAF Manston|journal=Flight|pages=1397|url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%201475.html}}</ref> and then in September 1931 [[Wg Cdr]]<ref>{{London Gazette|issue=33731|startpage=4250|date=30 June 1931|accessdate=11 January 2012}}</ref> Linnell was Officer Commanding [[RAF Hal Far|RAF Malta]].<ref>{{cite journal |date=16 October 1931 |title=Taking command of RAF Base Malta|journal=Flight|pages=1053 |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%201123.html}}</ref> He spent 1933 attending the course at [[Old Royal Naval College|RN Staff College]] [[Greenwich]] after which he took command of [[No. 99 Squadron RAF]]<ref>{{cite journal |date=1 February 1934|title=Taking Command of No. 99 Squadron RAF|journal=Flight|pages= |url=http://www.flightglobal.com/pdfarchive/view/1934/1934%20-%200115.html}}</ref>

He became the first Station Commander at [[RAF Mildenhall]] four days before it hosted 70,000 people for the start of the [[Royal Aero Club|RAeC]] [[MacRobertson Air Race]] (England to Australia) of 1934. Then in July 1935, the year of the [[Silver Jubilee]], [[RAF Mildenhall]] welcomed King [[George V]] for the first ever Royal review of the RAF.<ref>Visit [http://www.visitmildenhall.co.uk/#/raf-mildenhall/4535059188 RAF Mildenhall]. The history of RAF Mildenhall</ref><ref>''Sunday Graphic and Sunday News'', 7 July 1935.</ref> Linnell returned to the Air Ministry in July 1935 as Deputy Director of Organisation, Department of A.M.S.O.<ref name="FJL" /><ref>{{cite journal |date=25 July 1935|title=Deputy Director of Organisation|journal=Flight|pages=1397|url=http://www.flightglobal.com/pdfarchive/view/1935/1935%20-2-%200118.html}}</ref>

===Second World War===
After attending [[Royal College of Defence Studies|Imperial Defence College]] from the beginning of 1939 until 25 August, he was attached to HQ [[RAF Bomber Command]] as [[Air Officer]] in Charge of Administration and [[Mentioned in Despatches]]<ref>{{ Hot Central Gazette|issue=34893|supp=yes|startpage=4268|date=11 July 1940|accessdate=9 January 2012}}</ref> in 1940. In February 1941 he joined the Air Ministry as Assistant Chief of the Air Staff in charge of Training. On March, 1941 he was awarded the [[Companion of the Order of the Bath]].<ref name="LM22341" /><ref>{{London Gazette|issue=35107|supp=yes|startpage=1571|date=17 March 1941|accessdate=}}</ref> He was appointed Controller of Research and Development [[Ministry of Aircraft Production]] and an additional Member of the Air Council on 5 June of the same year.<ref>{{London Gazette|issue=35183|startpage=3229|date=6 June 1941|accessdate=11 January 2012}}</ref><ref>{{cite journal |date=19 June 1941|title=Appointment to Aircraft Supply Council, executive in charge of research and development|journal=Flight|pages=421|url=http://www.flightglobal.com/pdfarchive/view/1941/1941%20-%201387.html}}</ref> A post that he held until 19 April 1943.<ref>{{Members of the Air Council 1942-43|url=http://www.ibiblio.org/hyperwar/UN/UK/UK-RAF-II/UK-RAF-II-I.html}}</ref>

[[File:King George knighting Sir John Linnell.jpg|thumb|Knight George VI knights Linnell in the field.]]
From May 1943 [[Air Marshal]] Linnell was Deputy [[Air Officer|AO]] C-in-C, [[RAF Mediterranean and Middle East]]<ref>{{cite journal |date=22 April 1943|title=Higher-Command Changes|journal=Flight|pages=428|url=http://www.flightglobal.com/pdfarchive/view/1943/1943%20-%201050.html}}</ref> He was advanced from an Officer to a [[Knight Commander of the Order of the British Empire]] (KBE) in June 1943,<ref>{{London Gazette|issue=36033|supp=yes|startpage=2429|date=2 June 1943|accessdate=9 January 2012}}</ref><ref>{{cite journal |date=10 June 1943|title=K.B.E.|journal=Flight|pages=611|url=http://www.flightglobal.com/pdfarchive/view/1943/1943%20-%201515.html}}</ref> and knighted in the field by King [[George VI]], at an operational airport in the Tunisian desert.<ref>{{cite journal |date=1 July 1943 |title=Accolade in the desert|journal=Flight|pages=4|url=http://www.flightglobal.com/pdfarchive/view/1943/1943%20-%201684.html}}</ref><ref>''[[Pittsburgh Post-Gazette]]''[https://news.google.com/newspapers?nid=1129&dat=19430626&id=xrI0AAAAIBAJ&sjid=_2kDAAAAIBAJ&pg=2073,4631655 Saturday Morning 26 June 1943] Front Page "Arise, Sir Francis!"</ref> Linnell rejoined the Air Ministry in July 1944 for special duties. He died on active service, 3 November 1944,<ref name="DiedOnActiveService">{{cite journal |date=8 March 1945|title=Died on active Service.|journal=Flight|pages=|url=http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%200464.html}}</ref> in a car accident in [[Oxfordshire]], aged fifty-two.<ref name="FJL" /><ref>{{cite journal |date=16 November 1944|title=Caption for picture reads: Air Marshal Sir Francis J. Linnell formerly deputy Air C-in-C. Middle East, who was killed recently in a road accident in Oxfordshire.|journal=Flight|pages=|url=http://www.flightglobal.com/pdfarchive/view/1944/1944%20-%202394.html}}</ref>  He was cremated at [[Charing]] (Kent County) Crematorium.<ref name=cwgc>[http://www.cwgc.org/find-war-dead/casualty/2824356/LINNELL,%20Sir%20FRANCIS%20JOHN] CWGC Casualty Record.</ref>

Linnell was posthumously invested as a Commander of the United States [[Legion of Merit]] in recognition of his outstanding services in the Mediterranean theatre of operations from 29 February – 29 June 1944. The decoration was presented to his widow, Lady Linnell, by Brigadier General E. F. Koenig at a special ceremony in London.<ref>{{cite journal|date=19 April 1945|title=Posthumous U.S. Award|journal=Flight|pages=414 |url=http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%200744.html}}</ref><ref>{{cite journal|date=19 April 1945|title=Posthumous U.S. Award cont.|journal=Flight|pages=415 |url=http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%200747.html}}</ref><ref>{{London Gazette|issue=36915|supp=yes|startpage=640|date=30 January 1945|accessdate=7 January 2012}}</ref>

====Project ''Upkeep''====
Linnell was apparently against the idea of a [[bouncing bomb]]<ref name="WinLet">[http://www.rafmuseum.org.uk/online-exhibitions/dambusters/4_winterbotham_letter.cfm Royal Air Force Museum London] Wing Commander Winterbotham's Letter</ref> reasoning that it was drawing development resources away from the prototype high altitude bomber [[Vickers Windsor]], his thinking may have been influenced by Charles Craven, the Chairman at [[Vickers-Armstrongs|Vickers]].<ref name="newscientist">[https://books.google.co.uk/books?id=ip4h-vBJcnsC&lpg=PA619&ots=MYJoPTu6aA&dq=new%20scientist%202%20march%201978&pg=PA563#v=onepage&q=new%20scientist%202%20march%201978&f=false New Scientist] 2 March 1978 Page 563-564</ref>

Early in 1943, tests authorised by Linnell in June 1942 at the instigation of [[Henry Tizard|Sir Henry Tizard]],<ref name="newscientist" /> indicated that a [[bouncing bomb]] was technically feasible. A meeting between the Air Ministry, [[RAF Bomber Command]] and MAP decided to continue development of project [[Bouncing bomb|''Upkeep'']] with a proposal to have a fully trained squadron ready by May. [[Air Officer Commanding|AOC]]-in-C of [[RAF Bomber Command]] [[Sir Arthur Harris, 1st Baronet|Harris]] thought differently and saw the project as a waste of precious [[Avro Lancaster|Lancasters]]. However [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] [[Charles Portal, 1st Viscount Portal of Hungerford|Portal]] was convinced by film footage of the tests and ordered three [[Avro Lancaster|Lancasters]] allocated to the project. One week later, at a meeting convened by Linnell, the number of [[Avro Lancaster|Lancasters]] ordered had risen to a squadron and [[Operation Chastise]] was planned for that spring.<ref name="Raid">[https://books.google.co.uk/books?id=phZ9N9kniMQC&printsec=frontcover#v=onepage&q&f=false Dambusters – Operation ''Chastise'' 1943] by Douglas C.Dildy ISBN 978-1-84603-934-8</ref>

==Personal life==
Only son of William Henry and Kate Linnell, he had one sister, Marjorie Clare. On 26 May 1917, 'Jack' married Margaret Christabel Carpenter of [[St Leonards-on-Sea]], [[Sussex]], at [[Turnham Green|Christ Church, Turnham Green]]. They had no children.<ref name="Charing" />

==References==
{{Reflist|30em}}

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Douglas Evill]]}}
{{s-ttl|title=[[Air Officer|AO]][[Adjutant|A]], HQ [[RAF Bomber Command]]|years=1939–1941}}
{{s-aft|after=[[Ronald Graham (RAF officer)|Ronald Graham]]}}
|-
{{s-bef|before=[[Robert Saundby]]}}
{{s-ttl|title= [[Air Ministry]] as Assistant Chief of the Air Staff in charge of Training.|years=1941–1941}}
{{s-aft|after=[[Ralph Sorley]]}}
{{s-end}}

{{RAF WWII Strategic Bombing}}

{{DEFAULTSORT:Linnell, Francis John}}
[[Category:1892 births]]
[[Category:1944 deaths]]
[[Category:Aerial warfare pioneers]]
[[Category:Commanders of the Legion of Merit]]
[[Category:Foreign recipients of the Legion of Merit]]
[[Category:Knights Commander of the Order of the British Empire]]
[[Category:People educated at Bloxham School]]
[[Category:People from Kent]]
[[Category:People from Margate]]
[[Category:People from Thanet (district)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Royal Air Force air marshals]]
[[Category:Royal Air Force air marshals of World War II]]
[[Category:Royal Naval Air Service aviators]]
[[Category:Articles created via the Article Wizard]]