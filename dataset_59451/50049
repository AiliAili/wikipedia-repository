{{Infobox character
| name        = Missandei
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Nathalie Emmanuel]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Missandei-Nathalie Emmanuel.jpg
| caption     = [[Nathalie Emmanuel]] as Missandei
| first       = '''Novel''': <br />''[[A Storm of Swords]]'' (2000) <br />'''Television''': <br />"[[Valar Dohaeris]]" (2013)
| last        =
| occupation  =
| title       = 
| alias       =
| gender      = Female
| family      =
| spouse      =
| children    =
| relatives   = Marselen {{small|(brother)}}<br />Mossador {{small|(brother)}}
| nationality = Naathi
}}

'''Missandei''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 2000's ''[[A Storm of Swords]]'', Missandei was a slave interpreter, before joining [[Daenerys Targaryen]]. She is from the continent of [[Essos]]. She subsequently appeared in Martin's ''[[A Dance with Dragons]]'' (2011).

Missandei is portrayed by [[Nathalie Emmanuel]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/missandei/bio/missandei.html |title=''Game of Thrones'' Cast and Crew: Missandei played by Nathalie Emmanuel |publisher=[[HBO]] | accessdate=September 8, 2016}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html|title=From HBO|publisher=}}</ref>

== Character description ==
Missandei is ten years old at the start ''A Storm of Swords''.<ref name="Chapter 23">{{cite news|title=A Read of Ice and Fire: A Storm of Swords, Part 14|url=http://www.tor.com/2013/01/18/a-read-of-ice-and-fire-a-storm-of-swords-part-14/|accessdate=September 8, 2016|publisher=Tor|date=February 1, 2013}}</ref> She also speaks nineteen languages.<ref>{{cite news|title=Game of Thrones, Season 6, Episode 3: "Oathbreaker"|url=https://reelrundown.com/tv/Game-of-Thrones-Season-6-Episode-3-Oathbreaker|accessdate=September 8, 2016|publisher=Reelrundown.com|date=May 10, 2016}}</ref>

== Overview ==
Missandei is not a [[Narration#Third-person|point of view]] character in the novels, so her actions are witnessed and interpreted through the eyes of other people, especially [[Daenerys Targaryen]]. Missandei is mostly a background character in the novels.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=HBO}}</ref>

== Storylines ==
[[File:Three dragons.png|thumb|140px|alt=A coat of arms showing a red three-headed dragon on a black field over a scroll reading "Fire and Blood."|Coat of arms of House Targaryen]]

===In the books===
As children, Missandei and her three brothers are captured by raiders from the Basilisk Isles and sold into slavery in Astapor. Missandei's talent for learning languages easily is noticed by the masters of Astapor, who train her as a scribe. Missandei's brothers Marselen and Mossador are made into Unsullied (the third brother is also made into an Unsullied but dies during his training).

====''A Storm of Swords''====
Missandei is working as an interpreter for Astapori slaver Kraznys mo Nakloz when Daenerys Targaryen comes to inspect his army of Unsullied.<ref name="Chapter 23"/> After Daenerys strikes a bargain with the Good Masters of Astapor concerning payment for the Unsullied, Kraznys gives Missandei to Daenerys as an interpreter to give them commands. To Missandei's surpise, Daenerys frees her; having nowhere else to go, Missandei accepts Daenerys' offer to stay as her interpreter and handmaiden. Missandei subsequently accompanies Daenerys as she liberates the neighbouring cities of Yunkai and Meereen.

====''A Dance With Dragons''====
Missandei becomes Daenerys' herald, announcing her entry when she meets with the people of Meereen. After Mossador is killed by the Sons of the Harpy, Daenerys offers to let Missandei return to Naath; Missandei refuses, noting that she would be an easy target for slavers.

Following the reopening of the fighting pits and Daenerys' flight from Meereen with her dragon Drogon, Daenerys' new husband Hizdhar zo Loraq removes Missandei from her position as herald. With Daenerys' other servants Irri and Jhiqui joining the Dothraki in their search for Daenerys in the Dothraki Sea, Missandei is left as the only occupant in the Great Pyramid's royal apartments. When Barristan Selmy seizes control of Meereen, he has Missandei tend to the mortally wounded Quentyn Martell.

===In the show===
Missandei's backstory is mostly the same as in the books. However, she is aged up to be a young woman, and no mention is made of her having any brothers.

====Third season====
Most of Missandei's storyline from ''A Storm of Swords'' is retained for the third season. However, when Daenerys agrees for the purchase of Kraznys' Unsullied, she demands Missandei's services as part of the exchange, instead of Kraznys giving her to Daenerys of his own accord.<ref>{{cite web|url=https://www.theguardian.com/tv-and-radio/tvandradioblog/2013/apr/01/game-of-thrones-season-three-epsisode-one-valar-dohaeris|title=Game of Thrones recap: season three, episode one – Valar Dohaeris|last=Hughes|first=Sarah|publisher=[[The Guardian]]|date=April 1, 2013|accessdate=September 8, 2016}}</ref>

====Fourth season====
Missandei befriends Grey Worm, the commander of the Unsullied, and gives him lessons in the Common Tongue. While bathing in a stream she witnesses Grey Worm watching her; although she covers herself, she later admits to Grey Worm that she is glad he saw her. She expresses sorrow that Grey Worm was castrated during his training, though Grey Worm notes that if he had not become Unsullied he never would have met her.

====Fifth season====
Grey Worm is gravely wounded in a skirmish with the Sons of the Harpy, and Missandei stands vigil by his side. When Grey Worm wakes he reveals to her that, in battle, he felt fear for the first time, fear that he would never see Missandei again; Missandei is clearly moved by his sentiment. When the Sons of the Harpy launch an attack at the reopening of the fighting pits, one of the Sons nearly kills Missandei before he is killed by [[Tyrion Lannister]]. Daenerys subsequently flies away on Drogon,<ref>{{cite web|last=Fowler|first=Matt|title= Game of Thrones: "The Dance of Dragons" Review|url=http://ign.com/articles/2015/06/08/game-of-thrones-the-dance-of-dragons-review|publisher=IGN|date=June 8, 2015|accessdate=September 8, 2016}}</ref> and [[Daario Naharis]] decrees that Tyrion, Missandei, and Grey Worm should govern Meereen in her absence.

====Sixth season====
To keep the peace with the slavers, Tyrion grants them seven years to transition from slavery. Although Missandei helps defend Tyrion's decision to the outraged Meereenese freedman, in private she warns Tyrion that the masters will betray him. She is eventually vindicated, when the slavers send a fleet to lay siege to Meereen. She accompanies Daenerys to a parley with the slavers' representatives. When Daenerys leaves on Drogon to destroy the fleet, Missandei tells the masters that Daenerys has ordered one of their deaths as punishment for their treachery, though when two of them offer up the third Grey Worm kills those two instead. Missandei later accompanies Daenerys and her forces as they sail to Westeros.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://moviepilot.com/posts/3982261 |archivedate=August 18, 2016 |url=http://moviepilot.com/posts/3982261 |title=Watch: The Show's Writers Go Inside The Season 6 Finale Of 'Game Of Thrones' |publisher=[[Moviepilot]] |accessdate=September 8, 2016 |date=June 28, 2016 |author=Snowden, Heather|deadurl=no}}</ref>

== TV adaptation ==
[[File:Nathalie Emmanuel by Gage Skidmore.jpg|thumb|right|upright|[[Nathalie Emmanuel]] plays the role of Missandei in the [[Game of Thrones|television series]].]]
Missandei is played by the British actress [[Nathalie Emmanuel]] in the television adaption of the series of books.<ref>{{cite news|title=Game of Thrones reveals new cast members for Season 3!|url=http://io9.com/5925957/game-of-thrones-reveals-new-cast-members-for-season-3|accessdate=September 8, 2016|work=[[io9.com]]|date=July 13, 2012}}</ref><ref>{{cite web |url=https://www.youtube.com/watch?v=rpSDSgChsaI |title=Game of Thrones Season 3: New Cast Members – Comic Con |deadurl=no |accessdate=September 8, 2016}}</ref><ref>{{cite web|url=http://www.ew.com/article/2016/07/22/comic-con-game-thrones-panel|title=Game of Thrones team on season 7, who should be on Iron Throne|work=[[Entertainment Weekly]]|last=Li|first=Shirley|date=July 22, 2016|accessdate=September 8, 2016}}</ref> Emmanuel revealed in an interview about the support of her character by fans of the show, saying "I've really just been so happy to have people be high-fiving me and be like, "Yeah, you're representing strong women." I had someone high-five me and be like, "Yeah, thank you for representing women of color in 'Game of Thrones.'".<ref name="moviefone">{{cite news|title='Game of Thrones': Nathalie Emmanuel Wants More Action, Romance for Missandei|url=http://www.moviefone.com/2016/08/05/game-of-thrones-nathalie-emmanuel-missandei/|accessdate=September 8, 2016|publisher=Moviefone|date=August 5, 2016}}</ref>

Remarking on the character's romance with [[Grey Worm]]:

<blockquote>The romantic side is just such a sweet thing and I love it. Their storyline is so, so, so endearing, and I always enjoy working with Jacob (Anderson). He's just wonderful. So to maybe explore that some more would be really fun. It's so nice to watch. Whenever I see our scenes together, or our little moments together, I'm always like, "yay." It makes me really happy.<ref name="moviefone"/></blockquote>

===Recognition and awards===
{| class="wikitable sortable"
|-
! scope="col" |Year
! scope="col" |Award
! scope="col" |Category
! scope="col" |Result
! scope="col" |{{abbr|Ref.|References}}
|-
! scope="row" |2014
|[[Screen Actors Guild Award]]
|[[Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Drama Series|Outstanding Performance by an Ensemble in a Drama Series]]
|{{nom}}
|<ref>{{cite web|url=http://www.deadline.com/2013/12/sag-awards-2013-nominations-full-list/ | title=SAG Awards Nominations: ''12 Years A Slave'' And ''Breaking Bad'' Lead Way |publisher=Deadline.com | date=December 11, 2013| accessdate=September 8, 2016 }}</ref><ref>{{cite web |url=http://www.deadline.com/2014/01/sag-awards-2014-stunt-winners-lone-survivor-game-of-thrones/ | title=SAG Awards: ''Lone Survivor'', ''Game Of Thrones'' Win Stunt Honors |publisher=Deadline.com | date=January 18, 2014 | accessdate=September 8, 2016}}</ref>
|-
! scope="row" rowspan = '2'| 2015
|Screen Actors Guild Award 
|Outstanding Performance by an Ensemble in a Drama Series
|{{nom}}
|<ref>{{cite news | last=Hipes| first=Patrick | url=http://deadline.com/2014/12/sag-awards-nominations-2015-full-list-nominees-1201317757/|title=SAG Awards Nominations: ''Birdman'' & ''Boyhood'' Lead Film Side, HBO & ''Modern Family'' Rule TV – Full List|publisher=Deadline.com | date=December 10, 2014 | accessdate=September 8, 2016 | archiveurl=https://web.archive.org/web/20150126230813/http://deadline.com/2014/12/sag-awards-nominations-2015-full-list-nominees-1201317757/|archivedate=January 26, 2015 |deadurl=no}}</ref><ref>{{cite news | last=Hipes| first=Patrick | url=http://deadline.com/2015/01/sag-award-winners-2015-screen-actors-guild-awards-winner-list-1201358500/|title=SAG Awards: ''Birdman'' Flies Even Higher & ''Orange Is The New Black'' Shines – List Of Winners |publisher=Deadline.com | date=January 25, 2015| accessdate=September 8, 2016 | archiveurl=https://web.archive.org/web/20150126230702/http://deadline.com/2015/01/sag-award-winners-2015-screen-actors-guild-awards-winner-list-1201358500/ |archivedate=January 26, 2015 |deadurl=no}}</ref>
|-
|[[Empire Award]]
|[[Empire Hero Award]] (Ensemble)
|{{won}}
|<ref>{{cite web |url=http://variety.com/2015/film/global/interstellar-wins-film-director-at-empire-awards-1201462423/ |title=''Interstellar'' wins Film, Director at Empire Awards |work=Variety |first=Leo |last=Barraclough |date=March 30, 2015 |accessdate=June 30, 2016}}</ref><ref>{{cite web |url=http://www.empireonline.com/awards2015/winners/hero.asp |archiveurl=https://web.archive.org/web/20150714065122/http://www.empireonline.com/awards2015/winners/hero.asp |title=Empire Hero Award 2015 |work=[[Empire (film magazine)|Empire]] |accessdate=September 8, 2016 |archivedate=July 14, 2015 |deadurl=yes}}</ref><br />
|-
! scope="row" |2016
|Screen Actors Guild Award 
|Outstanding Performance by an Ensemble in a Drama Series
|{{nom}}
|<ref>{{cite web |url=http://variety.com/2015/film/awards/sag-award-nominations-2016-nominees-full-list-1201657169/ |title=SAG Awards Nominations: Complete List |work=[[Variety (magazine)|Variety]] |date=December 9, 2015 |accessdate=September 8, 2016}}</ref>
|-
! scope="row" |2017
|Screen Actors Guild Award 
|Outstanding Performance by an Ensemble in a Drama Series
|{{nom}}
|<ref>{{cite web|url=http://www.ew.com/article/2016/12/14/2017-sag-awards-nominations|title=SAG Awards nominations 2017: See the full list|first=Joey |last=Nolfi|date=December 14, 2016|accessdate=December 14, 2016|work=Entertainment Weekly}}</ref>
|}

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Missandei}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 2000]]
[[Category:Fictional slaves]]
[[Category:Characters in American novels of the 21st century]]