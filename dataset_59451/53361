An '''annual report''' is a comprehensive [[report]] on a [[Company|company's]] activities throughout the preceding [[year]]. Annual reports are intended to give [[shareholder]]s and other interested people information about the company's activities and financial performance. They may be considered as [[grey literature]]. Most jurisdictions require companies to prepare and disclose annual reports, and many require the annual report to be filed at the company's registry. Companies listed on a [[stock exchange]] are also required to report at more frequent intervals (depending upon the rules of the stock exchange involved).

==Contents==
Typical annual reports will include:<ref>Annual Report and Accounts - Contents http://tutor2u.net/business/accounts/annual-report-and-accounts-contents.htm</ref>

* General corporate information
* Operating and financial review
* [[Director's Report]]
* [[Corporate governance]] information
* [[Chairperson]]s statement
* [[Auditor's report]]
* Contents: non-[[audit]]ed information
* [[Financial statement]]s, including
** [[Balance sheet]] also known as Statement of Financial Position
** [[Income statement]] also profit and loss statement. 
** [[Statement of changes in equity]]
** [[Cash flow statement]]
* [[Notes to the financial statements]]
* [[Accounting]] policies
* Other features

Other information deemed relevant to stakeholders may be included, such as a report on operations for manufacturing firms or [[corporate social responsibility]] reports for companies with environmentally  or socially sensitive operations. In the case of larger companies, it is usually a sleek, colorful, high-gloss publication.

The details provided in the report are of use to investors to understand the company's financial position and future direction. The financial statements are usually compiled in compliance with [[International Financial Reporting Standards|IFRS]] and/or the domestic [[Generally Accepted Accounting Principles|GAAP]], as well as domestic legislation (e.g. the [[Sarbanes-Oxley Act|SOX]] in the U.S.).

In the [[United States]], a more-detailed version of the report, called a [[Form 10-K]], is submitted to the [[U.S. Securities and Exchange Commission]].<ref>Dictionary of Finance and Investment Terms by John Downes and Jordon Elliot Goodman Barron 1995 ISBN 0-8120-9035-7 page 23</ref> A publicly held company may also issue a much more limited version of an annual report, which is known as a "wrap report." A wrap report is a Form 10-K with an annual report cover wrapped around it.<ref>{{cite web|url=http://www.accountingtools.com/definition-wrap-report |title=Wrap Report Definition |publisher=AccountingTools |date= |accessdate=2013-08-27}}</ref>

===Directors' Role===
{{Globalize|section|date=June 2014}}
Statement of Directors' responsibilities for the shareholders' financial statements

The Directors are responsible for preparing the Annual Report and the financial statements in accordance with applicable [[Law of the Republic of Ireland]], including the accounting standards issued by the Accounting Standards Board and published by The Institute of Chartered Accountants. Irish company law requires the directors to prepare financial statements for each financial period which give a true and fair view of the state of affairs of the company and of the profit or loss of the company for that period.

In preparing these financial statements, the Directors are required to:

* select suitable accounting policies and then apply them consistently
* make judgements and estimates that are reasonable and prudent
* prepare the financial statements on the going concern basis unless it is inappropriate to presume that the Company will continue in business

The directors confirm that they have complied with the above requirements in preparing the financial statements. The directors are responsible for keeping proper books of account that disclose with reasonable accuracy at any time the financial position of the company and to enable them to ensure that the financial statements are prepared in accordance with accounting standards generally accepted in Ireland and with Irish statute comprising the Companies Acts 1963 to 2009.

== History ==
In 1903, [[U.S. Steel]] published an annual report whose financial accuracy was certified by [[PriceWaterHouseCoopers|Price, WaterHouse & Co.]] in what is known as the earliest modern corporate annual report.<ref>Wessel D. (2002). [https://www.wsj.com/news/articles/SB1013031072961249880 When Standards Are Unacceptable]. ''WSJ''.</ref>

==See also==
*[[Form 10-K]], the basic information required by the US [[Securities and Exchange Commission]]
*[[US corporate law]]
*[[Green annual report]]
*[[Grey literature]]

==References==
{{reflist}}

==External links==
{{Prone to spam|date=August 2013}}
{{Z148}}<!--     {{No more links}}

       Please be cautious adding more external links.

Wikipedia is not a collection of links and should not be used for advertising.

     Excessive or inappropriate links will be removed.

 See [[Wikipedia:External links]] and [[Wikipedia:Spam]] for details.

If there are already suitable links, propose additions or replacements on
the article's talk page, or submit your link to the relevant category at 
the Open Directory Project (dmoz.org) and link there using {{Dmoz}}.

-->
* [http://www.investopedia.com/articles/basics/10/efficiently-read-annual-report.asp How to Efficiently Read an Annual Report]
* [http://www.cnbc.com/id/101360337 How to Read a 10-K Like Warren Buffet by CNBC]
* [http://www.financeninvestments.com/category/annual-report List of Top Companies Annual Report from 2010 to 2014]
{{Authority control}}

[[Category:Financial statements]]
[[Category:Grey literature]]
[[Category:Annual publications]]