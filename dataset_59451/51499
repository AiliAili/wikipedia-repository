{{More references|date=March 2017}}

The '''International Astronomical Union Circulars''' ('''IAUCs''') are notices that give information about astronomical phenomena. IAUCs are issued by the [[International Astronomical Union]]'s [[Central Bureau for Astronomical Telegrams]] (CBAT) at irregular intervals for the discovery and follow-up information regarding such objects as planetary satellites, [[nova]]e, [[supernova]]e, and [[comet]]s. The first series of IAUCs was published at [[Uccle]] during 1920–1922 when the IAU's first CBAT was located there; the first IAUC published in the present (second) series was published in 1922 at [[Copenhagen Observatory]] after the transfer of the CBAT from Uccle to [[Copenhagen]]. At the end of 1964, the CBAT moved from Copenhagen to the [[Smithsonian Astrophysical Observatory]] in [[Cambridge, Massachusetts]], where it has remained to this day, on the grounds of the [[Harvard College Observatory]]. HCO had maintained a Central Bureau for the Western hemisphere from 1883 until the end of 1964, when its staff took on the IAU's CBAT; HCO had published its own Announcement Cards that paralleled/complemented the IAUCs from 1926 until the end of 1964, but the Announcement Cards ceased publication when the IAUCs began to be issued from the same building.

The IAUCs are delivered via the [[United States Postal Service]], e-mail, and through the [[Central Bureau for Astronomical Telegrams]]/[[Minor Planet Center]] Computer Service. Most of the announcement circulars published at Cambridge, Copenhagen, and Uccle from 1895 to the present day are available for viewing via the CBAT website.

== See also ==
* ''[[Minor Planet Circular]]''
* ''[[Minor Planet Electronic Circular]]''

== External links ==
* {{Official website|http://www.cbat.eps.harvard.edu/services/IAUC.html}}

{{DEFAULTSORT:Iau Circular}}
[[Category:Publications established in 1920]]
[[Category:Astronomy data and publications]]





[[pt:Central Bureau for Astronomical Telegrams#Publicação]]