{{other people|Larry Alexander}}
{{Infobox artist

| bgcolour      = Green
| name          = Larry D. Alexander
| image         = Larry D, Alexander 001.jpg
| imagesize     =
| caption       = 
| birth_name     = Larry Dell Alexander
| birth_date     = {{birth date and age|1953|5|30}}
| birth_place      = [[Dermott, Arkansas]]
| death_date     =
| death_place    =
| nationality   = [[United States of America]]
| field         = [[Visual artist]], [[Author]], [[Catechism]], [[Evangelism|Evangelist]]
| training      = self taught
| movement      = [[Realism (arts)|Realism]]
| works         = ''[[Clinton Family Portrait]]''
| patrons       =
| influenced by =
| influenced    =
| awards        =
}}

'''Larry Dell Alexander''' (born May 30, 1953) is an [[United States|American]] artist, [[Christian]] author and [[Catechist]] from [[Dermott, Arkansas]] in [[Chicot County]].<ref>[http://www.dermott.org/ - "Rural Community Alliance/Harrison, Arkansas"]</ref> Alexander is best known for his creations of elaborate colorful, and black & white "[[pen and ink]]" drawings in his "[[crosshatching]]", or "[[hatching]]" technique, and his acrylic paintings. He also received notoriety and a personal presidential thanks for his personal rendition of a "[[Clinton Family Portrait]]" oil painting which he gave to [[U.S. President]] [[Bill Clinton]] in 1995.<ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-news-023/ - "Larry Alexander strives to inspire young artists" - Fri. Oct. 20, 1995 - page 1A - Lizabeth Cardenas]</ref> It is now a part of the collection at the [[Clinton Presidential Library]] in [[Little Rock, Arkansas]]. He is also known for [[the Arkansas Schools Tours]] that he did between 1996 and 2006. He has written nine devotional [[Quiet Time|Bible studies]] commentaries on several [[New Testament]] books and one on the [[Old Testament]] book of [[Ezekiel]] of the [[Christian Bible]],<ref>[https://www.worldcat.org/title/home-and-church-bible-study-commentaries-from-pauls-letter-to-the-romans/oclc/936029741- Worldcats]</ref> and has also created many drawings and paintings with strong Christian themes over the years. However, in recent years he is perhaps better known for his writings and teachings on Christianity <ref>[http://nl.newsbank.com/nl-search/we/Archives?p_product=ST&p_theme=st&p_action=search&p_maxdocs=200&s_dispstring=irving%20artist%20gives%20his%20presidential%20portrait%20to%20clintons%20AND%20date(all)&p_field_advanced-0=&p_text_advanced-0=(irving%20artist%20gives%20his%20presidential%20portrait%20to%20clintons)&xcal_numdocs=20&p_perpage=10&p_sort=YMD_date:D&xcal_useweights=no Fort Worth Star-Telegram - Archives - "Irving artist gives his presidential portrait to Clintons" - Mon. Aug. 21,1995 - page 10A - Terry Lee Jones]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20002.jpg - "Presidential portrait receives stamp of approval" - Aug. 1995 - page 4 - Artimes Magazine]</ref><ref>[http://www.encyclopediaofarkansas.net/encyclopedia/entry-detail.aspx?entryID=7313 - Encyclopedia of Arkansas - Larry Dell Alexander - 1953-]</ref>

==Early life and career==
[[File:The Artwork of Larry D. Alexander 005.jpg|thumb|"Send in the Clown", a "Pen & Ink" "Crosshatching drawing" by Alexander]]
Born in the small rural town of [[Dermott, Arkansas]] to Robert and Janie Alexander, Larry is the fourth of ten children and the second of the union of his parents.  His father was a truck driver, and his mother was a beautician. Alexander began drawing at about the age of four. He never received any formal art training during any level of his schooling while growing up, as none was available in his small rural hometown. After graduating from [[Dermott High School]] in May 1971, Alexander moved to [[Pine Bluff, Arkansas]], where he studied Architectural Residential Design at Pines Vocational Technical School, now [[Southeast Arkansas College]]. In later years he also attended [[Richland College]] in [[Dallas, Texas]] where he studied [[AutoCAD]].

Today, six pieces of his work from his popular "Dermott Series", a series of paintings he painted about his childhood home of Dermott, are now a part of the permanent collection at [[the Arts and Science Center for Southeast Arkansas]] in [[Pine Bluff, Arkansas]]. Alexander's work is mostly influenced by his experiences in life, such as growing up in the rural south during the 1950s and 1960s, as well as his life in [[Detroit, Michigan]] during the 1970s and 1980s.

Alexander moved to [[Detroit, Michigan]] after two years of school in [[Pine Bluff]] to seek employment in his chosen field. However, he was unsuccessful in the short run, and instead, he ended up finding work in a [[Chrysler]] auto assembly plant and became fascinated with the innerworkings of cars. This led him to become a certified mechanic, a craft he worked at for the next seventeen years. He ultimately met and married his wife, Patricia while living in Detroit, and they moved their family to [[Irving, Texas]] where he opened his own auto repair shop and operated it until 1991.

Encouraged by Patricia, he began using his art talent for the first time in fifteen years. His career as a professional artist can be traced back to this point as he developed his "[[pen and ink]]" style that he calls "[[crosshatching]]", and used it to create several lines of greeting cards under his, now defunct, "Alexander Greeting Cards Company" name. He coined the phrase "The Expressions Line" for his first line of greeting cards in 1991, but did not trademark the name. As a result, [[Hallmark Cards]] now has used the name on a line of their cards since 1997. His line should not be confused or associated with their product line.<ref>[http://cocatalog.loc.gov/cgi-bin/Pwebrecon.cgi?Search_Arg=Alexander%2C+Larry+D.&Search_Code=NALL&PID=7hgPTsH43_NGlNt8hd8twcRb17De&SEQ=20100318205238&CNT=25&HIST=1 - Library of Congress - Alexander, Larry Dell - 1953]</ref><ref>[http://www.faqs.org/copyright/alexander-greeting-card-company-presents-the-save-the/ - Alexander Greeting Cards - Larry Dell Alexander]</ref> Between 1991 and 1994 he created over 80 pieces of "pen and ink" [[fine art]],<ref name="cocatalog.loc.gov">[http://cocatalog.loc.gov/cgi-bin/Pwebrecon.cgi?Search_Arg=Alexander%2C+Larry+D.&Search_Code=NALL&PID=7hgPTsH43_NGlNt8hd8twcRb17De&SEQ=20100318205238&CNT=25&HIST=1 - Library of Congress - Alexander, Larry D. - 1953]</ref> including "Renetta", "Girlfriends", "Cowboy Fiddler", "Young Kennedys", and "Roundup". He also used many of his drawings on his "Fine-arTshirt" T-shirt line of the mid-nineties.<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20025.jpg - "Artist donates design to help homeless" - Artime Magazine/Irving News section - Oct. 1994 - page 2]</ref>

Alexander is also a "[[realist painter]]" who works in a variety of other mediums including [[oil paint|oils]], [[acrylic paint|acrylics]], and [[watercolors]]. He is a self-taught artist who chooses mostly to do exhibits in venues that provide mainstream exposure to a large variety of people such as festivals, schools, malls, libraries, banks, art institutions, and even [[United States Postal Service|U.S. Post Office]] branches on occasion.<ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-in-the-news-2/"Texas artist to show new prints at festival" - Pine Bluff Commercial - Friday May 20, 2005 - page 1D]</ref><ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-news-003/ - "Works by Dermott native accepted by Arts Center" - Thur. Feb. 25, 1999 - page 2A - Eva Marie Pearson]</ref><ref>[http://nl.newsbank.com/nl-search/we/Archives?p_product=ST&p_theme=st&p_action=search&p_maxdocs=200&s_dispstring=widely%20diverse%20artists%20featured%20in%20african-american%20exhibit%20AND%20date(all)&p_field_advanced-0=&p_text_advanced-0=(widely%20diverse%20artists%20featured%20in%20african-american%20exhibit)&xcal_numdocs=20&p_perpage=10&p_sort=YMD_date:D&xcal_useweights=no Fort Worth Star-Telegram - Archives - "Widely diverse artists featured in African-American exhibit" - Fri. Jan. 2, 1995 - page 7A - Terry Lee Jones]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20038.jpg - "Soldiers and Cowboys Remembered" - Feb. 2008 - page 13 - David Turissini]</ref>

===The Dermott Series===
In early 1996 Alexander finished and released his popular "Dermott Series", a 20 piece collection of oil and acrylic paintings that offered a nostalgic look back at his childhood of growing up in rural southeast [[Arkansas]]. The paintings feature images of people, buildings, and sites of [[Dermott, Arkansas]], such as a cotton gin, his childhood house, where he went to school, and other images. Alexander said at the time that, "I did the Dermott Series for many personal reasons, and I'm overwhelmed by the response this collection is creating here in Texas". The series includes, "Birthplace", "Where I grew up", "Picking Cotton", "Cotton Gin", "Hot Grits", "In the kitchen with mama", and the old [[Chicot County]] High School, among others.<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20006.jpg - "Alexander paints series of Dermott" - Chicot County Spectator - May 1, 1996 - pg.7]</ref>

===The Detroit Series===
In 1999 Alexander unveiled his "Detroit Series", a series of [[oil]] and [[acrylic paint|acrylic]] paintings of various sites in [[Detroit, Michigan]] at the American Black Artist Institute on [[West Grand Boulevard]] in [[Detroit]]. The nine piece series includes paintings on [[Belle Isle Park]], the [[Detroit skyline]], the [[Detroit River]], [[Hitsville USA]] (the original home for [[Motown Records]]), [[Greektown Historic District]], the old [[Tiger Stadium (Detroit)]], the old [[J.L. Hudson]] building, former mayor [[Coleman A. Young]], and many more.<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20012.jpg - "Alexander unveils his "Detroit Series"]</ref><ref>[http://blogs.metrotimes.com/news-blawg/happy-birthday-larry-d-alexander/ - Detroit Metro Times - "Happy Birthday Larry D. Alexander" - May 30, 2014 - Michael Jackman]</ref>

===The Delta Series===
[[File:The Artwork of Larry D. Alexander 004.jpg|thumb|"Aunt Eira Mae", an Acrylic painting by Larry D. Alexander - [[The African American Museum (Dallas, Texas)]] - 2004]]

In May 1998 Alexander unveiled his "Delta Series, which was painted entirely with acrylics, during "[[the Arkansas Schools Tours]]". The tour was expanded that year to include stops in [[Greenville, Mississippi]] and [[Memphis, Tennessee]]. This series included paintings of the [[Mississippi Alluvial Plain]] region from [[Monroe, Louisiana]] to Memphis. It includes a painting of [[Graceland]], [[Elvis Presley]]'s former home, the landmark Greenville Courthouse in Greenville, a perspective of [[Beale Street]] in Memphis, and his classic rendition of a "Cotton Farm" among others.<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20014.jpg - Pine Bluff Commercial - "Artist announces plan for "Arkansas Tour" - May, 1998]</ref> Another piece from the Delta Series, "Aunt Eira Mae" was donated to the permanent collection of [[The African American Museum (Dallas, Texas)]] in 2004.

===The Series of P.A.T.R.I.C.E===
Alexander has also donated work to art departments of schools and colleges. In October 1996 at halftime at the inaugural football game, billed as the "[[Mobil]] Gridiron Classic", at [[Texas Stadium]] in [[Irving, Texas]], Alexander presented a piece from one of his series, "The series of P.A.T.R.I.C.E", to the president, and the chancellor, of the participating colleges, respectively, [[Texas Southern University]] and the [[University of Arkansas at Pine Bluff]].<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20019.jpg - Pine Bluff Commercial - "Around and about" - Sun. October 27, 1996]</ref>

===The Sixties Series===
Alexander's most popular [[Pen and Ink]] art series, "The Sixties Series" has been exhibited in schools, libraries, and art institutions in several places since it was completed in 1993. It consists of elaborate drawings of well known figures and events of the 1960s, such as the civil rights [[Selma to Montgomery marches]] of 1965, and portraits of [[Martin Luther King Jr.]], [[Lyndon Johnson]], [[Malcolm X]], [[Medgar Evers]], and [[Rosa Parks]]. This series also covers the [[Vietnam War]].

The theme pieces in the collection are a piece called "Composite Sixties", and one called "Composite Protests", which make up a composite of people, places and events that were prominent in the 1960s. For example, they show images of [[John F. Kennedy]], [[Lyndon Johnson]], and [[Richard M. Nixon]], the [[United States]] presidents who served during the sixties, also [[J. Edgar Hoover]], [[Jackie Kennedy]], [[Ted Kennedy]], [[Robert F. Kennedy]], an image of U.S. astronauts landing on the moon in 1969, and many of the [[United States|American]] protests that took place during this turbulent decade, to name a few.<ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-in-the-news-001/ - Pine Bluff Commercial - "Crawfish Festival brings Dermott native back home" - Fri. May 18, 2001]</ref>

==African-American history book==
In 2001, Alexander finished his first book called ''African-American History at a Glance'', which also included several [[pen and ink]] drawings of [[African-Americans]] who have contributed substantially to the [[United States|American]] success story. "There is a lack of input regarding [[African-Americans]] in the [[American history]] curriculum of schools all over [[United States|America]]" Mr. Alexander says. "There are a lot of schools that offer it as a choice to students, but I think it should be a part of regular [[American history]]".

Alexander's book was used to help create a supplemental text, that was later put together by the [[Irving Independent School District]] to help improve the [[American history]] curriculum in the high schools of [[Irving, Texas]] in 2002. His book deliberately ignores the contributions of [[African-Americans]] in the areas of sports and entertainment, as he feels they are already too well known and over-emphasized in society. Alexander says in his book that, "By no means is this an attempt to downplay the prowess of these particular individuals, or to discourage other individuals who aspire to excel in those areas. This publication is intent on bringing to the forefront, some of the [[African-American]] contributions that have historically been largely ignored".<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20020.jpg Dallas Morning News - "Broadening Minds" - Thur. Feb. 15, 2001 - Irving News section - page 1H]</ref><ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-in-the-news-001/ "Crawfish Festival brings Dermott native back home" - Pine Bluff Commercial - Friday May 18, 2001 - page 1D]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20007.jpg "Schools to include more ethnic studies" - Irvings News - March 4, 2001 - page 3 - Stephen Terry]</ref><ref>[http://www.thelarrydalexander.com/LARRYDALEXANDER/larry-d-alexander-in-the-news/larry-d-alexander-news-008/ "Board creating guide to teaching diversity - Irving News - Feb. 22, 2001 - page 1M - Stephen Terry]</ref>

==Christian books==
In 2006, Alexander wrote his second book entitled ''Sunday school lessons from the book of [[the Acts of the Apostles]]'', which deals with his calling by God to teach the [[Holy Bible]]. ''Sunday school lessons from the book of the Acts of the Apostles'' was the first of a trilogy that he self-published over a one-year period. It was followed in late 2006 by the second book of the trilogy, called ''Sunday school lessons from the Gospel according to [[John Mark]]''. The final book in the trilogy, ''Sunday school lessons from the [[Apostle Paul]]'s [[letter to the Romans]]'' was released in mid-2007.<ref name="cocatalog.loc.gov"/> His other books include ''Home Bible study commentaries from [[the Gospel of John]]'' in 2008, and ''Home and Church Bible study commentaries from the [[Book of Hebrews]]'' in 2009. Alexander also writes a weekly on-line Sunday school lesson commentary based on the International Sunday School Lessons curriculum.<ref>[http://www.larrydalexander.blogspot.com Larry D. Alexander - Weekly Sunday School Lesson]</ref>

==The Arkansas schools tours==
Alexander and his cousin Lawrence “Larry” Crockett, both natives of [[Dermott, Arkansas]] put together the first "Arkansas schools tour" in 1996, and in May of that year, visited ten schools in seven cities in four days. Crockett and his son, B.J., both traveled with Alexander on the initial tour, while they were on a one-week break from the tour of the [[Rodgers and Hammerstein]] [[Broadway musical]] ''[[Carousel (musical)|Carousel]]'', which B.J. had a role in at the time. After the first year however, the tour was continued on by Alexander alone through 2007. Alexander says he wanted to give back to his home state of Arkansas and be a blessing to the children who live there, and, because of the success of the first tour in 1996, Alexander decided to continue doing it once annually for the next ten years. Its major objective was to instill hope and encouragement in the children of Arkansas, and to aid them in making good choices during the developing stages of life. These annual visits to [[high schools]], [[middle schools]], and [[elementary schools]], were also conducted to instruct and encourage children in pursuing their goals and careers in the [[visual arts]], as well as other areas, and to stay away from [[illegal drugs]]. It was during the second tour in 1997 that Alexander incorporated his anti-drug slogan “Eradicate Drug Use Common Among Teens Everywhere” (EDUCATE). Alexander usually ended his tour with a two-day art exhibit and print signing at the [[Dermott, Arkansas]] [[Crawfish]] Festival.<ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20028.jpg The McGehee Times/Dermott News - "Alexander to present artwork at Crawfish Festival" - Wed. May 1, 1996 - page 5]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20006.jpg - Chicot County Spectator - "Alexander paints series of Dermott" - May 1, 1996 - page 7]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20013.jpg "Ex-Arkansan mixes art with anti-drug education" - Arkansas Democrat/Gazette - Sun. May 11, 1997 - pg.2]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20014.jpg "Artist announces plan for "Arkansas Tour" - Pine Bluff Commercial - Monday May 11, 1998]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20036.jpg Pine Bluff Commercial - Larry D. Alexander begins his school tour at Camden" - Mon. May 10, 1999 - page 1C Accent section]</ref>

===Arkansas Schools Tours Stops 1996 - 2006===

* Dermott Elementary - [[Dermott, Arkansas]]
* [[Dunbar Magnet Middle School]] - [[Little Rock, Arkansas]]
* [[Mann Arts and Science Magnet Middle School]] - [[Little Rock, Arkansas]]
* [[Chystal Hills Elementary]] - [[North Little Rock, Arkansas]]
* [[34th Avenue Arts Magnet School]] - [[Pine Bluff, Arkansas]]
* [[Coleman Elementary]] - [[Pine Bluff, Arkansas]]
* Reed Elementary - [[Dumas, Arkansas]]
* Lakeside Elementary - [[Lake Village, Arkansas]]
* McGehee Elementary - [[McGehee, Arkansas]]
* [[Clinton Elementary Magnet School]] - [[Sherwood, Arkansas]]
* Hamburg Junior High - [[Hamburg, Arkansas]]
* Albriton Elementary - [[Hamburg, Arkansas]]
* Wilmot Elementary - [[Wilmot, Arkansas]]
* [[Coleman Middle School]] - [[Greenville, Mississippi]]
* Portland Elementary - [[Portland, Arkansas]]
* [[Pine Bluff High School]] - [[Pine Bluff, Arkansas]]
* [[Camden Fairview Middle School]] - [[Camden, Arkansas]]
* [[Camden Fairview High School]] - [[Camden, Arkansas]]
* [[Indiana Street School]] - [[Pine Bluff, Arkansas]]
* [[Hot Springs High School (Arkansas)]] - [[Hot Springs, Arkansas]]

==Religion==
{{Rquote|right|'''It seems as if God has placed a hunger and thirst inside of each of us, innately from birth, and the only way to satisfy that hunger or thirst is by seeking and ultimately finding Him. It is for the purpose of God that man was ever created in the first place. And just as no man has ever created anything that weren't for his own purpose or benefit, so it is with God. Man was created to serve God. And until we can grasp and understand that, we'll just continue to meander around, looking for physical answers, to problems, that have always been, spiritual'''|Larry D. Alexander}}
Alexander is a devout [[Christian]] who teaches [[Sunday school]] and has also served as a [[Church deacon]]. He has taught [[Sunday school]] for several years and he also teaches through his books, online "Weekly Sunday School Lesson" commentaries, which are based on the international Sunday school lesson system, his online Book by Book Bible Study, and, his national e-mail system.

He also has created a large body of artwork in [[Christian]] and biblical themes over the years, such as his paintings, "The Twenty-third Psalm Series", which are a visual depiction of the verses of [[Psalm 23]] in the [[Holy Bible]], "Memories of St. Paul", which is a depiction of his childhood church in [[Dermott, Arkansas]], "Bible Stories", "The Fall of Man", a depiction of [[Adam]] and [[Eve]] after being evicted from the [[Garden of Eden]], "Sunday Sermon", and many others.

He is a member of Cornerstone Baptist Church in [[Arlington, Texas]] where the Rev. [[Dwight McKissic]] Sr. is the pastor. Alexander has written several books aimed at helping to revive the interest of adults in attending [[Sunday school]] in their respective [[Christian Church]]es, and, to help start up, or restore [[Sunday school]] classes back to those [[Christian Church]]es that are lacking these opportunities to get to know the teachings of [[Jesus Christ]].<ref>{{YouTube|JDfNVkfbkUg|"This do in remembrance of me"}}</ref><ref>{{YouTube|kfwobcEA1k8|"Sunday school lesson commentary from the Gospel of John"}}</ref>

==Personal life==
Alexander and his wife, Patricia,have been together since 1978 and live in [[Texas]]. They have four children, Ken, Leandra, Kawanna, and Patrice, of whom his "Series of P.A.T.R.I.C.E" is named after. The "PATRICE" means "Providing a Touch Relevant in Continuous Education".<ref>[http://thelarrydalexander.com/LARRYDALEXANDER/?page_id=2 - "Larry D. Alexander/American Artist" ]</ref><ref>[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges///home/users/web/b1499/nf.larrydalexander/public_html/LarryD.Alexander-ArtfortheAges//wp-content/uploads/2014/02/Larry%20D.%20Alexander%20News%20019.jpg - Pine Bluff Commercial - "Around and About" - Oct. 27, 1996]</ref>

==Works==

===Gallery===
<gallery perrow="4">
File:The Artwork of Larry D. Alexander 004.jpg|''Aunt Ira Mae''
File:The Artwork of Larry D. Alexander 005.jpg|''	Send in the Clown''
File:The Artwork of Larry D. Alexander 007.jpg|''Sleepy Head''
File:The Artwork of Larry D. Alexander 008.jpg|''Down by the Sea''
File:The Artwork of Larry D. Alexander 009.jpg|''Greenville Courthouse''
File:The Artwork of Larry D. Alexander 010.jpg|''Helping Out''
File:The Artwork of Larry D. Alexander 011.jpg|''African Mothers''
File:The Artwork of Larry D. Alexander 012.jpg|''Moose Call''
File:The Artwork of Larry D. Alexander 013.jpg|''Princess of the Blue''
File:The Artwork of Larry D. Alexander 014.jpg|''Daughter's Loyalty''
File:The Artwork of Larry D. Alexander 016.jpg|''Clinton Family Portrait''
</gallery>
{{Commons category|Larry D. Alexander}}

==Books==
*2001 - ''African-American History at a Glance''"

===The Sunday school lesson series===
*2006 - ''Sunday school lessons from the Book of the Acts of the Apostles'' - ISBN 978-1-4116-9831-4
*2006 - ''Sunday school lessons from the Gospel according to John Mark''  - ISBN 978-0-615-13552-6
*2007 - ''Sunday school lessons from the Apostle Paul's letter to the Romans'' - ISBN 978-0-615-15342-1

===The Home and Church Bible study series===
*2008 - ''Home Bible study commentaries from The Gospel of John'' - ISBN 978-0-615-20347-8
*2009 - ''Home and Church Bible study commentaries from The Book of Hebrews'' - ISBN 978-0-578-03453-9
*2012 - ''Home and Church Bible study commentaries from Paul's letter to the Romans'' - ISBN 978-1-105-66048-1
*2014 - ''Home and Church Bible study commentaries from the books of Galatians, Ephesians, & Philippians'' - ISBN 978-0-578-15400-8
*2016 - ''Home and Church Bible Study Commentaries from the Book of Ezekiel'' - ISBN 978-1-329-94443-5
*2016 - ''Home and Church Bible Study Commentaries from the Book of Acts'' - ISBN 978-1-365-01313-3

===Greeting cards===
*1991 "The Expressions Line" - Library of Congress - copyright PAU 1-608-507
*1991 "The Save the Children Line" - Library of Congress - copyright PAU 1-563-400
*1991 "The Feelings Line" - Library of Congress - copyright PAU 1-563-399
*1991 "The Professions Line" - Library of Congress - copyright PAU 1-566-819

===Pen & ink art===
*1991-1994 Larry D. Alexander/American Artist - "82 pieces of Pen & Ink Art" - Library of Congress - copyright VA 664-679
*1994 - "Roundup"
*1992 - "Renetta"
*1994 - "Cowboy Fiddler"
*1995 - "Girlfriends"
*1994 - "Young Kennedys"

===Paintings===
[[File:The Artwork of Larry D. Alexander 011.jpg|thumb|"African Mothers" an Acrylic painting by Larry D. Alexander - [[The African American Museum (Dallas, Texas)]] - 2004]] 
*1995 "[[Clinton Family Portrait]]" (Oil)
*1995 "The Jockey" (Oil)
*1996 "Cotton Gin" (Acrylics)
*1996 "In the kitchen with mama" (Acrylics)
*1997 "Cotton Farm" (Acrylics)
*1997 "Shelling Peas" (Acrylics)
*1998 "Belle Isle" (Oil)
*1999 "African Mothers" (Acrylics)
*2008 "Washday in Luanda" (Acrylics)

===Art Series===

[[File:The Artwork of Larry D. Alexander 012.jpg|thumb|"Moose Call", a "Pen & Ink Drawing" from "The Animal Series" by Larry D. Alexander]]

*1994 "The Twenty-Third Psalm Series" (Acrylics)
*1993 "The Sixties Series" (Pen & Ink)
*1994 "The Animal Series" (Pen & Ink)
*1995 "The Western Art Series" (Pen & Ink)
*1995 "American Girls" (Oil)
*1996 "The Dermott Series" (Oil and Acrylics)
*1996 "The Series of P.A.T.R.I.C.E." (Oil and Acrylics)
*1997 "The Age of Innocence Series" (Oil and Watercolors)
*1998 "The Delta Series" (Oil and Acrylics)
*1999 "The series that is Detroit" (Oil and Acrylics)

==References==
{{reflist|3}}

==Further reading==
* {{cite web| url= https://books.google.com/books?id=4ZQECwAAQBAJ&pg=PT86&lpg=PT86&dq=clinton+family+portrait&source=bl&ots=Irxz616BW5&sig=zmqrhW43W5Znpx-aqpHwsJqzpYs&hl=en&sa=X&ved=0ahUKEwjfl4_fkN7KAhVHwj4KHdcXCsI4MhDoAQg0MAQ#v=onepage&q=clinton%20family%20portrait&f=false|title= ''Legendary Locals of Grand Prairie'' - ISBN 978-1-4671-0217-9|accessdate=August 12, 2016}}
* {{cite web|url= http://www.encyclopediaofarkansas.net/encyclopedia/entry-detail.aspx?entryID=7313|title= ''Encyclopedia of Arkansas History and Culture - Larry D. Alexander -1953-}}

==External links==
*[http://www.thelarrydalexander.com/LARRYDALEXANDER/ Official website]
*[http://larrydalexander.netfirms.com/LarryD.Alexander-ArtfortheAges/ Art for the ages]
*[http://www.larrydalexander.blogspot.com/ Weekly Sunday school lesson]
*[http://larrydalexanderbiblestudies.blogspot.com/ Larry D. Alexander Book by Book Bible Study]

{{Authority control}}

{{DEFAULTSORT:Alexander, Larry D.}}
[[Category:Living people]]
[[Category:Artists from Arkansas]]
[[Category:African-American artists]]
[[Category:1953 births]]
[[Category:People from Dermott, Arkansas]]
[[Category:American Christian writers]]
[[Category:Christian artists]]
[[Category:Writers from Arkansas]]
[[Category:African-American Christians]]
[[Category:Artists from Texas]]
[[Category:Writers from Texas]]
[[Category:Artists from Detroit]]
[[Category:People from Irving, Texas]]
[[Category:Writers from Detroit]]
[[Category:African-American writers]]
[[Category:20th-century artists]]
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]
[[Category:Artist authors]]
[[Category:American Christian religious leaders]]