{{primary sources|date=March 2013}}
{{Infobox Motorcycle
|name             = BMW F800GT
|image            = [[File:BMW F 800 GT 2013.jpg|250px|alt=BMW F 800 GT]] 
|caption          = 
|aka              = 
|manufacturer     = [[BMW Motorrad]]
|parent_company   = 
|production       = 2013–
|assembly         =
|predecessor      = [[BMW F800ST|F800ST]]
|successor        = 
|class            = [[Sport touring motorcycle|Sport touring]]
|engine           = 798 cc, [[parallel-twin]], liquid cooled, 4-stroke, [[DOHC]], 4 valves per cylinder
|bore_stroke      = {{convert|82|x|75.6|mm|abbr=on}}
|compression      = 12.0:1
|top_speed        = >{{Convert|200|kph|mph|0|abbr=on}}
|power            = {{Convert|66|kW|0|abbr=on}} @ 8,000&nbsp;rpm
|torque           = {{convert|86|Nm|lbft|abbr=on}} @ 5,800&nbsp;rpm
|ignition         = 
|transmission     = Constant mesh 6-speed gearbox, toothed belt with shock damper
|frame            = Aluminium bridge frame, partially [[motorcycle frame#Engine_as_a_stressed_member|load-bearing engine]]
|suspension       = Front: Telescopic fork, Ø&nbsp;{{convert|43|mm|abbr=on}}, {{convert|125|mm|abbr=on}} travel<br>Rear: Single-sided aluminium swingarm, {{convert|125|mm|abbr=on}} travel<br>Optional Electronic Suspension Adjustment (ESA)
|brakes           = Front: Twin disc Ø&nbsp;{{convert|320|mm|abbr=on}}<br>Rear: Single disc Ø&nbsp;{{convert|265|mm|abbr=on}}<br>[[Anti-lock braking system for motorcycles|ABS]]
|tires            = Front: 120/70-ZR17<br>Rear: 180/55-ZR17
|rake_trail       = 63.8°, {{convert|94.6|mm|abbr=on}}
|wheelbase        = {{convert|1514|mm|abbr=on}}
|length           = {{convert|2156|mm|abbr=on}}
|width            = {{convert|905|mm|abbr=on}}
|height           = {{convert|1248|mm|abbr=on}}
|seat_height      = {{convert|800|mm|abbr=on}}
|dry_weight       = 
|wet_weight       = {{convert|213|kg|abbr=on}}
|fuel_capacity    = {{convert|15|l|abbr=on}}
|oil_capacity     = 
|fuel_consumption = {{Convert|90|kph|mph|abbr=on}}: {{Convert|3.4| L/100 km|abbr=on}}<br>{{Convert|120|kph|mph|abbr=on}}: {{Convert|4.3| L/100 km|abbr=on}}
|turning_radius   = 
|related          = [[BMW F800ST|F800ST]], [[BMW F800R|F800R]], [[BMW F800S|F800S]], [[BMW F series parallel-twin|F800GS, F700GS, F650GS]]
|sp               =  uk
}}

The '''BMW F800GT''' is a [[sport touring motorcycle]] manufactured by [[BMW Motorrad]] since 2013. It is the successor to the [[BMW F800ST|F800ST]],<ref name=press>{{cite web|title=The New BMW F800GT - Press Kit|url=https://www.press.bmwgroup.com/pressclub/p/pcgl/pressDetail.html?title=the-new-bmw-f-800-gt&outputChannelId=6&id=T0134019EN&left_menu_item=node__6628#|publisher=BMW Group PressClub Global|accessdate=26 March 2013}}</ref> and joins the F-series range which includes the [[Dual-sport motorcycle|dual-sport]] [[BMW F series parallel-twin|F800GS and F700GS]], and the [[naked bike|naked]] [[BMW F800R|F800R]].

==Engine==

As with other models in the F-series range, the F800GT uses a {{Convert|798|cc|abbr=on}} [[Straight-two engine|parallel-twin engine]], codeveloped by BMW and [[Rotax]]. The engine has a 360° firing order which produces an exhaust note reminiscent of BMW's signature air-cooled [[Flat engine|boxer twins]]. However, this firing order requires both pistons to move up and down at the same time. To counter the significant inertia produced by the pistons reciprocating, BMW devised a third vestigial [[connecting rod]] to a balance weight. The result is a parallel twin with significantly reduced vibration compared to other parallel twin engine designs. The engine is lubricated by a [[dry sump]] system.

The engine produces {{Convert|66|kW|0|abbr=on}} at 8,000&nbsp;rpm, but can be specified with a reduced power output of either {{convert|35|kW|abbr=on}} for European riders on restricted Category A2 licences,<ref name=manual>{{cite web|title=Rider's Manual F800GT|url=http://www.bmw-motorrad.de/de/de/services/bmwmotorrad_betriebsanleitungen/com/PDF/F_0B03_RM_0812_F800GT_01.pdf|publisher=BMW Motorrad Germany|accessdate=26 March 2013|page=120|format=PDF}}</ref><ref>{{cite web | url= https://www.gov.uk/ride-motorcycle-moped/bike-categories-ages-and-licence-requirements | publisher= GOV.UK | title= Riding a motorbike, moped or motor tricycle | accessdate= 23 March 2013}}</ref>
or {{convert|25|kW|abbr=on}}.<ref name=manual />

==Equipment==

The F800GT has a low-maintenance [[Gilmer belt|belt drive]] and single sided [[swingarm]].<ref name=technical>{{cite web|title=F800GT Technical Data|url=http://www.bmw-motorrad.com/com/en/index.html?content=http://www.bmw-motorrad.com/com/en/bike/tour/2012/f800gt/f800gt_data.html&prm_action=&notrack=1&notrack=1|publisher=BMW Motorrad International|accessdate=26 March 2013}}</ref>
[[Anti-lock braking system for motorcycles|ABS]] brakes are standard equipment. In the UK and now the US, factory options include heated grips, [[tyre pressure monitoring system]], onboard computer, LED indicators, anti theft alarm system, main centre stand, [[Traction control system|Automatic Stability Control (ASC)]], pannier fastenings, comfort seat, low seat, and Electronic Suspension Adjustment (ESA).<ref name=specs>{{cite web|title=BMW F800GT Prices & Specification |url=http://www.bmw-motorrad.co.uk/motorcycles/tour/bmw-f-800-gt/prices-and-specifications.html |publisher=BMW Motorrad UK |accessdate=26 March 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130221035516/http://www.bmw-motorrad.co.uk/motorcycles/tour/bmw-f-800-gt/prices-and-specifications.html |archivedate=21 February 2013 |df= }}</ref> BMW Motorrad also offer a range of accessories.<ref>{{cite web|title=F800GT Accessories|url=http://www.bmw-motorrad.com/com/en/bike/tour/2012/f800gt/f800gt_accessories.html?prm_action=|publisher=BMW Motorrad International|accessdate=25 March 2013}}</ref>

==New features==
Compared to its predecessor, the F800GT offers several new features, including:<ref name=press /> power increase of {{Convert|3.5|kW|0|abbr=on}}; redesigned full fairing with improved wind and weather protection; introduction of optional Electronic Suspension Adjustment (ESA) and Automatic Stability Control (ASC); handwheel adjustment of [[Motorcycle suspension#Pre-load_adjustment|rear suspension preload]]; {{convert|50|mm|abbr=on}} longer rear [[swingarm]]; lighter cast aluminium wheels; new vibration-free handlebars with new generation switches and controls; updated instrument dials and standard fuel/temperature gauges; smoke grey turn indicators; increased load capacity (by {{Convert|11|kg|0|abbr=on}}; new exhaust system; new paint finishes; and a newly developed luggage system.

==References==
{{reflist}}

==External links==
* [http://www.bmw-motorrad.com/com/en/bike/tour/2012/f800gt/f800gt_overview.html F800GT at BMW Motorrad International]

{{BMW motorcycles}}
{{DEFAULTSORT:BMW F800GT}}

[[Category:Sport touring motorcycles]]
[[Category:BMW motorcycles|F800GT]]