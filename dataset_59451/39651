{{Infobox medical condition
| Name  = Alcohol dependence
| ICD10 = {{ICD10|F|10||f|10}}.2
| ICD9  = {{ICD9|303}}
| OMIM  = 103780
}}

'''Alcohol dependence''' is a previous psychiatric diagnosis in which an individual is [[physical dependence|physically]] or [[psychological dependence|psychologically]] [[substance dependence|dependent]] upon [[Ethanol|drinking alcohol]]. In 2013 it was reclassified as [[alcohol use disorder]] (alcoholism) along with [[alcohol abuse]] in [[DSM-5]].<ref name=NIH2013>{{cite web|title=Alcohol Use Disorder: A Comparison Between DSM–IV and DSM–5|url=http://pubs.niaaa.nih.gov/publications/dsmfactsheet/dsmfact.htm|accessdate=9 May 2015|date=November 2013}}</ref>

==Definition and diagnosis==
{{Addiction glossary}}
According to the [[DSM-IV]] criteria for alcohol dependence, at least three out of seven of the following criteria must be manifest during a 12-month period:
* Tolerance
* Withdrawal symptoms or clinically defined [[alcohol withdrawal syndrome]]
* Use in larger amounts or for longer periods than intended
* Persistent desire or unsuccessful efforts to cut down on alcohol use
* Time is spent obtaining alcohol or recovering from effects
* Social, occupational and recreational pursuits are given up or reduced because of alcohol use
* Use is continued despite knowledge of alcohol-related harm (physical or psychological)<ref>http://www.alcoholcostcalculator.org/business/about/dsm.html Reference for the whole section.</ref>

===Screening===
The  Alcohol Use Disorders Identification Test (AUDIT) is considered the most accurate alcohol screening tool for identifying potential alcohol misuse, including dependence.<ref>{{cite web|url=http://www.alcohollearningcentre.org.uk/Topics/Browse/BriefAdvice/?parent=4444&child=4896|title=AUDIT - Alcohol Use Disorders Identification Test|date=28 June 2010|publisher=Alcohol Learning Centre|accessdate=3 June 2012}}</ref> It was developed by the World Health Organisation, designed initially for use in primary healthcare settings with supporting guidance.<ref>[http://whqlibdoc.who.int/hq/2001/who_msd_msb_01.6a.pdf Supporting guidance]</ref> Its use has replaced older screening tools such as CAGE but there are many shorter alcohol screening tools,<ref>[http://www.alcohollearningcentre.org.uk/Topics/Browse/BriefAdvice/ Alcohol screening tools]</ref> mostly derived from the AUDIT. The [[Severity of Alcohol Dependence Questionnaire]] (SAD-Q) is a more specific twenty-item inventory for assessing the presence and severity of alcohol dependence.

===Other alcohol-related disorders===
Because only 3 of the 7 DSM-IV criteria for alcohol dependence are required, not all patients meet the same criteria and therefore not all have the same symptoms and problems related to drinking. Not everyone with alcohol dependence, therefore, experiences physiological dependence. Alcohol dependence is differentiated from [[alcohol abuse]] by the presence of symptoms such as [[Drug tolerance|tolerance]] and [[alcohol withdrawal|withdrawal]]. Both alcohol dependence and alcohol abuse are sometimes referred to by the less specific term [[alcoholism]]. However, many definitions of alcoholism exist, and only some are compatible with alcohol abuse. There are two major differences between alcohol dependence and alcoholism as generally accepted by the medical community.

# Alcohol dependence refers to an entity in which only alcohol is the involved addictive agent. Alcoholism refers to an entity in which alcohol or any cross-tolerant addictive agent is involved.
# In alcohol dependence, reduction of alcohol, as defined within DSM-IV, can be attained by learning to control the use of alcohol. That is, a client can be offered a social learning approach that helps them to 'cope' with external pressures by re-learning their pattern of drinking alcohol. In alcoholism, patients are generally not presumed to be 'in remission' unless they are abstinent from alcohol.

The following elements are the template for which the degree of dependence is judged:

# Narrowing of the drinking repertoire.
# Increased salience of the need for alcohol over competing needs and responsibilities.
# An acquired tolerance to alcohol.
# Withdrawal symptoms.
# Relief or avoidance of withdrawal symptoms by further drinking.
# Subjective awareness of compulsion to drink.
# Reinstatement after abstinence.<ref>Clark, David, Background Briefing, Alcohol Dependence, Drink and Drug News, 7 February 2005, p. 11</ref>

==Treatment==
Treatments for alcohol dependence can be separated into two groups, those directed towards severely alcohol-dependent people, and those focused for those at risk of becoming dependent on alcohol.  Treatment for alcohol dependence often involves utilizing [[relapse prevention]], [[support group]]s, [[psychotherapy]],<ref>{{cite web|url=http://www.icap.org/LinkClick.aspx?fileticket=1%2bMZHEUwVUU%3d&tabid=177|title=Alcohol Dependence and Treatment|work=ICAP Blue Book|publisher=International Center for Alcohol Policies|accessdate=2 June 2012}}</ref> and setting [[short-term goal]]s.  The [[Twelve-Step Program]] is also a popular process used by those wishing to recover from alcohol dependence.<ref>{{cite web|url=http://www.medicalbug.com/what-is-alcohol-addiction-what-causes-alcohol-addiction/#more-164|title=What is Alcohol Addiction: What Causes Alcohol Addiction?|date=6 January 2012|publisher=MedicalBug|accessdate=2 June 2012}}</ref>

==Epidemiology==
[[File:The Drunkard's Progress 1846.jpg|thumb|"The Drunkard’s Progress", 1846]]
About 12% of American adults have had an alcohol dependence problem at some time in their life.<ref>{{cite journal |author=Hasin D |title=Prevalence, Correlates, Disability, and Comorbidity of DSM-IV Alcohol Abuse and Dependence in the United States |journal=Archives of General Psychiatry |volume=64 |issue=7 |year=2007 |pages=830–42 |doi=10.1001/archpsyc.64.7.830 |pmid=17606817|display-authors=etal}}</ref> In the UK the [[National Health Service|NHS]] estimates that around 9% of men and 4% of UK women show signs of alcohol dependence.<ref>DrinkAware UK Website http://www.drinkaware.co.uk/check-the-facts/health-effects-of-alcohol/mental-health/alcohol-dependence</ref>

==History==
The term 'alcohol dependence' has replaced 'alcoholism' as a term in order that individuals do not internalize the idea of cure and disease, but can approach alcohol as a chemical they may depend upon to cope with outside pressures.

The contemporary definition of alcohol dependence is still based upon early research.  There has been considerable scientific effort over the past several decades to identify and understand the core features of alcohol dependence.<ref name="WeinerFreedheim2003">{{cite book|author1=Irving B. Weiner|author2=Donald K. Freedheim|author3=George Stricker|author4=Thomas A. Widiger|title=Handbook of Psychology: Clinical psychology|url=https://books.google.com/?id=dVauuLKZar4C&pg=PA201|accessdate=16 April 2010|year=2003|publisher=John Wiley and Sons|isbn=978-0-471-39263-7|pages=201–}}</ref> This work began in 1976, when the British psychiatrist Griffith Edwards and his American colleague Milton M. Gross <ref>Edwards G. & Gross MM, Alcohol dependence: provisional description of a clinical syndrome, BMJ 1976; i: 1O58-106
</ref> collaborated to produce a formulation of what had previously been understood as ‘alcoholism’ – the ''alcohol dependence syndrome''.

The alcohol dependence syndrome was seen as a cluster of seven elements that concur. It was argued that not all elements may be present in every case, but the picture is sufficiently regular and coherent to permit clinical recognition. The syndrome was also considered to exist in degrees of severity rather than as a categorical absolute. Thus, the proper question is not ‘whether a person is dependent on alcohol’, but ‘how far along the path of dependence has a person progressed’.

==See also==
* [[Alcohol dementia]]
* [[Alcohol intoxication]]
* [[Alcoholic beverage]]
* [[High-functioning alcoholic]]
* [[Long-term effects of alcohol]]
;Questionnaires
* [[Alcohol Use Disorders Identification Test]]
* [[CAGE questionnaire]]
* [[CRAFFT Screening Test]]
* [[Paddington Alcohol Test]]
* [[Severity of Alcohol Dependence Questionnaire]]

==Notes==
{{Reflist|33em}}

==External links==
* [http://www.mediconum.com/alcohol-dependence/ Arnold Little, MD Alcohol Dependence - extensive article]
* [http://hamsnetwork.org/app2/ SADD - Short Alcohol Dependence Data Questionnaire]. A brief, self-administered questionnaire sometimes utilised in individual or group treatments.
*[[Garifullin Ramil Ramzievich|R.R.Garifullin]] [http://elibrary.ru/item.asp?id=19521992 Using coding therapy to treat alcohol and drug addiction. Manipulations in psychotherapy. Rostov-on-Don, Feniks, 251 p. 2004. 251 p.] ISBN 5-222-04382-7

{{Psychoactive substance use|state=expanded}}
{{addiction}}
{{alcohealth}}

{{DEFAULTSORT:Alcohol Dependence}}
[[Category:Drinking culture]]
[[Category:Alcohol abuse]]
[[Category:Substance dependence]]