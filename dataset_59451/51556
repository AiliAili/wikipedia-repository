{{original research|date=November 2012}}
{{Infobox journal
| title = Indoor and Built Environment
| cover = [[File:Indoor and Built Environment front cover image.jpg]]
| editor = Chuck Yu
| discipline = [[Environmental studies]], [[built environment]], [[architectural engineering]], [[environmental engineering]], [[environmental health]]
| former_names = Indoor Environment
| abbreviation = Indoor Built Environ.
| publisher = [[Sage Publications]]
| country = 
| frequency = 8/year
| history = 1992-present
| openaccess = 
| license = 
| impact = 1.716
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201661/title
| link1 = http://ibe.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ibe.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 44723764
| LCCN = sn96039455
| CODEN = IBENFP
| ISSN = 1420-326X
| eISSN = 1423-0070
}}
'''''Indoor and Built Environment''''' is a [[peer-reviewed]] [[academic journal]] covering any topic pertaining to the quality of the indoor and built environment and how this affects the efficiency, performance, health, and comfort of those living or working there. Topics range from [[urban infrastructure]], design of buildings, and materials used for laboratory studies including building airflow simulations and health effects.  The [[editor-in-chief]] is Chuck Yu (EnviroAct Consultants). It was established in 1992 by consultants to the tobacco company [[Altria Group|Philip Morris]],<ref>{{cite journal|last1=Tong|first1=EK|last2=England|first2=L|last3=Glantz|first3=SA|title=Changing conclusions on secondhand smoke in a sudden infant death syndrome review funded by the tobacco industry.|journal=Pediatrics|date=March 2005|volume=115|issue=3|pages=e356-66|pmid=15741361}}</ref> and is published by [[Sage Publications]].

== Criticism ==
A 2005 study in ''[[The Lancet]]'' found that between 1992 and 2002 ''Indoor and Built Environment'' was influenced by the [[tobacco industry]]. It was found that up to 61% of articles on indoor smoke published by the journal reached industry-positive conclusions, with up to 90% of these articles having at least one author with a history of association with the industry.<ref>{{cite journal |doi=10.1016/S0140-6736(05)17990-2 |title=Environmental tobacco smoke research published in the journal Indoor and Built Environment and associations with the tobacco industry |year=2005 |last1=Garne |first1=David |last2=Watson |first2=Megan |last3=Chapman |first3=Simon |last4=Byrne |first4=Fiona |journal=The Lancet |volume=365 |issue=9461 |pages=804–9 |pmid=15733724}}</ref>
 
Originally, the journal was funded by the [[International Society of the Built Environment]] (ISBE) and was published by [[Karger Publishers]] from 1992-2002. ISBE was closed in 2002 and  re-launched in 2009 as a limited company by guarantee as a "Community Interest Company" - a not for profit company. The journal, ''Indoor Environment'', was renamed ''Indoor and Built Environment'' in 1996 by John Hoskins, who was [[editor-in-chief]] of the journal from 1994 to March 2009.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Chemical Abstracts Service]], [[ Engineered Materials Abstracts]], [[Scopus]], and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.716, ranking it 7th out of 56 journals in the category "Construction & Building Technology"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Construction & Building Technology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 22nd out of 44 journals in the category "Engineering, Environmental",<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Environmental |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2014-08-20 |series=[[Web of Science]] |postscript=.}}</ref> and 75th out of 160 journals in the category "Public, Environmental & Occupational Health".<ref name=WoS3>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Public, Environmental & Occupational Health |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201661/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1992]]
[[Category:Environmental social science journals]]
[[Category:Bimonthly journals]]