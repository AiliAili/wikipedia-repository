<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name= M.S.325
  |image= File:Morane MS325.jpg
  |caption=Morane-Saulnier M.S.325
}}{{Infobox Aircraft Type
  |type=[[fighter aircraft|Fighter]]
  |national origin=France
  |manufacturer=[[Morane-Saulnier]]
  |designer=
  |first flight=1933
  |introduced= 
  |retired=1934
  |status=
  |primary user=[[French Air Force]] (intended)
  |more users=
  |produced=<!--years in production, e.g. 1970–1999, if still in active use but no longer built-->
  |number built= 1
  |unit cost=
  |variants with their own articles=
}}
|}

The '''Morane-Saulnier M.S.325''' was a [[List of aircraft of the French Air Force during World War II|French Air Force]] [[fighter aircraft]] built by [[Morane-Saulnier]] in 1933 to meet the requirements of 1930 fighter aircraft specification. The design was unsuccessful and was abandoned in 1934.

==Design and development==
In 1930, when the Jockey ''legier Chasse'', or ''Plan Caquot'' light weight fighter program was judged a failure, the Service Technique de l' Aeronautique issued the C1 (monoplace de chasse) requirement. C1 (upgraded on 26 January 1931) called for a single-seat fighter powered by a supercharged engine with a cylinder capacity of between {{convert|26|and|30|l|cuin|abbr=on|2}}. Ultimately no fewer than 10 designs and 12 prototypes were offered, all designed around the {{convert|26|l|cuin|abbr=on|2}} [[Hispano-Suiza 12Xbrs]] developing {{convert|650|hp|kW|abbr=on|disp=flip}} at {{convert|4500|m|ft|abbr=on}}, with proven reliability and a relatively small frontal area. The Morane-Saulnier submissions included the M.S.275 which retained the classic [[parasol]] monoplane configuration of preceding Morane-Saulnier fighters. In the more innovative M.S.325, a low wing, [[duralumin]]-skinned all-metal configuration was employed.

The M.S.325 was relatively modest in its concept still featuring an open cockpit (originally the tail surfaces were fabric covered) and fixed-gear with the semi-elliptical two-spar wings braced by exterior struts. The wings had two jettisonable wing root fuel tanks with a pair of {{convert|7.7|mm|in|abbr=on|3}} [[FM 24/29 light machine gun|Châtellerault machine guns]] mounted one above each of the widely spaced landing gear legs. More unusual was that the incidence of the starboard wing greater than that of the port wing to counter torque; the engine was also canted slightly to port to counter the resultant yaw.<ref name= "Green p. 71"/>

==Operational history==
M.S.325 C3 No. 01 (Works no. 4120) was flown for the first time by company Chief Test Pilot Michael Détroyat early in 1933 from the factory site, Vèlizy-Villacoublay. The first test results were not satisfactory as tail buffeting was encountered leading to modifications that included lowering the tailplane and adding wing root fairings.<ref name= "Green p. 71"/>

Although testing proceeded, the M.S.325 continued to be hampered by handling problems. In measuring up to other C1 competitors, the M.S.325 was relegated to an "also-ran" status and the [[Dewoitine D.500]] became the chosen design.

Development based around a [[Hispano-Suiza 12Xers]] engine with a {{convert|20|mm|in|abbr=on|3}} cannon was proposed but eventually, the M.S.325 design was abandoned in favour of the more promising [[Morane-Saulnier M.S.406|M.S.405 C.1]] with only partial performance tests completed.

==Specifications (M.S.325)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Flying Review International<ref name= "Green p. 71">Green 1968, p. 71.</ref>
|crew=one pilot
|length main=8.25 m
|length alt=27 ft 0.75 in
|span main=11.8 m
|span alt=38 ft 8.5 in
|height main=3.7 m
|height alt=12 ft 1{{fraction|2|3}} in
|area main=19.73 m²
|area alt=212.37 ft²
|empty weight main=1,354 kg
|empty weight alt=2,985 lb
|loaded weight main=1,789 kg
|loaded weight alt=3,944 lb
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[Hispano-Suiza 12Xbrs]]
|type of prop=liquid-cooled V-12
|number of props=1
|power main= 484.71 kW
|power alt=650 hp
|max speed main=365 km/h (estimated)
|max speed alt=227 mph 
|max speed more=at 4,500 m (14,765 ft)
|range main=
|range alt=
|ceiling main=12,000 m (estimated)
|ceiling alt=39,370 ft
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=
|guns=<br>
*
** 2× {{convert|7.7|mm|in|abbr=on|3}} [[MAC 1934|Châtellerault machine guns]]
}}

==References==
{{commons category|Morane-Saulnier}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* Brindley, John. F. ''French Fighters of World War Two''. Windsor, UK: Hylton Lacy Publishers Ltd., 1971. ISBN 0-85064-015-6.
* Green, William. "Facts by Request." ''Flying Review International'', Volume 24, no. 3, November 1968, p.&nbsp;71.
* Green, William. ''War Planes of the Second World War, Volume One: Fighters''. London: Macdonald & Co.(Publishers) Ltd., 1960 (tenth impression 1972). ISBN 0-356-01445-2.
* Pelletier, Alain. ''French Fighters of World War II''. Carrollton, Texas: Squadron/Signal Publications, Inc., 2002. ISBN 0-89747-440-6.
{{refend}}

{{Morane-Saulnier aircraft}}

[[Category:Propeller aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Low-wing aircraft]]
[[Category:French fighter aircraft 1930–1939]]
[[Category:Morane-Saulnier aircraft|MS.325]]