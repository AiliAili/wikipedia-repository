{{Use dmy dates|date=November 2013}}
{{Use British English|date=November 2013}}
{{Infobox military person 
|name= Lionel Wilmot Brabazon Rees
|image= Lionel Rees VC IWM Q 68027.jpg
|image_size= 
|alt= 
|caption= Lionel Rees c.1918
|birth_date= {{birth date|1884|07|31|df=yes}}
|death_date= {{Death date and age|1955|09|28|1884|07|31|df=yes}}
|birth_place= [[Caernarfon]], [[Carnarvonshire]], Wales
|death_place= [[Nassau, Bahamas]]
|placeofburial= Nassau War Cemetery, Bahamas
|nickname= 
|allegiance= {{flag|United Kingdom}}
|branch= [[British Army]]<br/>[[Royal Air Force]]
|serviceyears= 1903–1931<br/>c.1939–1942
|rank= [[Group Captain]]
|unit= [[Royal Garrison Artillery]]<br/>[[Royal Flying Corps]]
|commands= [[No. 11 Squadron RAF|No.&nbsp;11 Squadron RFC]]<br/>[[No. 32 Squadron RAF|No.&nbsp;32 Squadron RFC]]
|battles= [[First World War]]
* [[Western Front (World War I)|Western Front]]
* [[Battle of the Somme]]
[[Second World War]]
|awards= [[Victoria Cross]]<br/>[[Officer of the Order of the British Empire]]<br/>[[Military Cross]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]]
|relations=
|laterwork=
}}
[[Group Captain]] '''Lionel Wilmot Brabazon Rees''' {{post-nominals|country=GBR|size=100%|sep=,|VC|OBE|MC|AFC|RAFr}} (31 July 1884 – 28 September 1955) was a [[Wales|Welsh]] recipient of the [[Victoria Cross]], the highest award for gallantry in the face of the enemy that can be awarded to [[United Kingdom|British]] and [[Commonwealth of Nations|Commonwealth]] forces. He was credited with eight confirmed aerial victories, comprising one enemy aircraft captured, one destroyed, one "forced to land" and five "driven down".<ref name=Above316>''Above the Trenches'', p. 316</ref> Rees and his gunner, [[Flight Sergeant]] [[James McKinley Hargreaves]], were the only two airmen to become aces flying the earliest purpose-built British fighter airplane, the [[Vickers Gunbus]].<ref>{{cite book |title= Pusher Aces of World War 1. |page= 91 }}</ref>

Rees also had a keen interest in archaeology.  While flying from [[Cairo]] to [[Baghdad]] in the 1920s, he took some of the earliest archaeological aerial photographs of sites in eastern [[Emirate of Transjordan|Transjordan]] (now Jordan), and published several articles in ''[[Antiquity (journal)|Antiquity]]'' and the journal of the [[Palestine Exploration Fund]].  He is considered a father of the archaeological studies of this remote area, and a pioneer of aerial archaeology.<ref name="Jordan pages 69-81">"Aerial Archaeology in Jordan",  David Kennedy and Robert Bewley,  ''[[Antiquity (journal)|Antiquity]]'' 83, no 319, pp. 69–81</ref>  He was also an accomplished sailor.

==Early life==
Rees was born at 5 Castle Street, [[Caernarfon]], in 1884 the son of Charles Herbert Rees, a solicitor and honorary colonel  in the Royal Welch Fusilers and his wife Leonara.<ref name="odnb" /> Rees attended [[Eastbourne College]] before entering the [[Royal Military Academy, Woolwich|Royal Military Academy]] at Woolwich in 1902.<ref name="theaerodrome.com">{{cite web |url=http://www.theaerodrome.com/aces/wales/rees.php |title=Lionel Rees |work=The Aerodrome |accessdate= 20 May 2010}}</ref> He was commissioned in on 23 December 1903 into the [[Royal Garrison Artillery]] and was posted to Gibraltar.<ref name="odnb" /><ref>{{cite book |title= Pusher Aces of World War 1 |page= 19 }}</ref> Promoted to [[lieutenant]] in 1906 he moved to Sierra Leone in 1908 and in May 1913 was seconded to the Southern Nigeria Regiment.<ref name="odnb" />

==First World War==
In 1912, Rees learned to fly at his own expense, receiving his [[List of pilots awarded an Aviator's Certificate by the Royal Aero Club in 1913|Aviator's Certificate (no. 392)]] in January 1913. By 1913–14, Rees was attached to the [[West African Frontier Force]] when he was seconded to the [[Royal Flying Corps]] in August 1914, initially as an instructor at [[Upavon]], he was promoted to [[Captain (land)|captain]] in October 1914.<ref name="odnb" /> In early 1915 he took command of the newly formed [[No. 11 Squadron RAF|No.&nbsp;11 Squadron]] at Netheravon and in July they moved to France.<ref name="obit">{{Cite newspaper The Times |articlename=Gp. Capt. L.W.B. Rees V.C. |author= |section=Obituaries |day_of_week=Thursday |date=29 September 1955 |page_number=12|issue=53338 |column=}}</ref> He first saw action flying the Vickers Gunbus with No. 11 Squadron in the mid-1915, earning a reputation as an aggressive pilot and an above average marksman.<ref name="theaerodrome.com"/>

===Military Cross===
Rees was awarded the [[Military Cross]] for his actions in 1915, gazetted as follows:<ref>{{LondonGazette |issue=29344 |supp= yes |startpage=10731 |date=29 October 1915 |accessdate= 7 June 2014}}</ref>

{{quote|For conspicuous gallantry and skill on several occasions, notably the following: —

On 21st&nbsp;September, 1915, when flying a machine with one machine gun, accompanied by [[Flight Sergeant|Flight-Serjeant]] Hargreaves, he sighted a large German biplane with two machine guns 2,000&nbsp;feet below him. He spiralled down and dived at the enemy, who, having the faster machine, manoeuvred to get him broadside on and then opened heavy fire. Despite this, Captain Rees pressed his attack and apparently succeeded in hitting the enemy's engine, for the machine made a quick turn, glided some distance and finally fell just inside the German lines near Herbecourt.

On 28 July he attacked and drove down a hostile monoplane despite the main spar of his machine having been shot through and the rear spar shattered. On 31 August, accompanied by Flight-Sergeant Hargreaves, he fought a German machine more powerful than his own for three-quarters of an hour, then returned for more ammunition and went out to the attack again, finally bringing the enemy's machine down apparently wrecked.|''The London Gazette'', 29 October 1915}}

By this time he had claimed one aircraft captured, one destroyed, one "forced to land" and five "driven down".<ref name=Above316/>

Rees returned to England at the end of 1915 where he took command of the Central Flying School Flight at Upavon.<ref name="obit" /> In June 1916 he took [[No. 32 Squadron RAF|No.&nbsp;32 Squadron]] to France.<ref name="obit" />

===Victoria Cross===
Rees was 31&nbsp;years old and a temporary [[major]] in No.&nbsp;32 Squadron when the following deed took place for which he was awarded the VC.<ref name="times-vc">{{Cite newspaper The Times|articlename=Court Circular|author=|section=Court and Social|day_of_week=Monday|date=18 December 1916|page_number=11|issue=41353|column=}}</ref>

In the first hours of the [[Somme Offensive]], Rees was on patrol, taking off in [[Airco DH.2]] No. 6015 at 0555 hours. His attempt to join a formation of "British" machines brought an attack from one of the Germans. He shot up the attacker, hitting its fuselage between the two aircrew. As it dived away, Rees attacked a [[Luft-Fahrzeug-Gesellschaft|Roland]]. Long range fire from three other Germans did not discourage Rees from closing on it; it emitted a hazy cloud of smoke from its engine from the 30 rounds Rees fired into it and it fled. Rees then single handedly went after five more Germans. A bullet in the thigh paralysed his leg, forcing him to temporarily break off his assault. As the shock of the wound wore off, he was able to pursue the German formation leader, which was leaving after dropping its bomb. He fired his [[Lewis machine gun]] empty. In frustration, he drew his pistol but dropped it into his DH.2's nacelle. Meanwhile, the German two-seater pulled away above him. The German formation was shattered and scattered.<ref>{{cite book|title=The War in the Air|p=332}}</ref><ref>{{cite book |title= Pusher Aces of World War 1 |pages= 34–35 }}</ref>

Rees gave up the futile chase, and returned to base. Once landed, he calmly asked for steps so he could deplane. Once seated on the aerodrome grass, he had a tender fetched to take him to hospital. The valour of his actions earned him the Victoria Cross. Its citation reads:<ref>{{cite book |title= Pusher Aces of World War 1 |pages= 35–36 }}</ref>

{{quote|On 1&nbsp;July 1916 at [[Double Crassieurs]], France, Major Rees, whilst on flying duties, sighted what he thought was a bombing party of our machines returning home, but were in fact enemy aircraft. Major Rees was attacked by one of them, but after a short encounter it disappeared, damaged. The others then attacked him at long range, but he dispersed them, seriously damaging two of the machines. He chased two others but was wounded in the thigh, temporarily losing control of his aircraft. He righted it and closed with the enemy, using up all his ammunition, firing at very close range. He then returned home, landing his aircraft safely.<ref>{{London Gazette |issue=29695|date=4 August 1916  |startpage=7744|endpage=|supp=yes |accessdate=7 April 2015}}</ref>}}

He convalesced for a while due to his injuries from the 1 July action, and went on a War Office mission to the United States, becoming a temporary [[lieutenant colonel]] in May 1917.<ref name="odnb" /> For the remainder of hostilities Rees commanded a [[School of Aerial Fighting]] based at [[RAF Turnberry]].<ref name="odnb" /><ref name="Pusher Aces of World War 1">{{cite book |title= Pusher Aces of World War 1 |page= 36 }}</ref>

==Post-war career==
On 2 November 1918 Rees was award the [[Air Force Cross (United Kingdom)|Air Force Cross]] in recognition of valuable flying services.<ref>{{LondonGazette |issue=30989 |supp= yes |startpage=12958 |date=1 November 1918 |accessdate= 7 June 2014}}</ref> In 1919, Rees was appointed an [[Officer of the Order of the British Empire]].<ref name="obe">{{LondonGazette |issue=31378 |supp= yes |startpage=7028 |date=29 May 1919 |accessdate= 8 June 2014}}</ref> On 1 August 1919 he resigned his commission with the Royal Garrison Artillery and took a permanent commission in the newly formed Royal Air Force as a lieutenant-colonel.<ref>{{LondonGazette |issue=31486 |supp= no |startpage=9865 |date=1 August 1919|accessdate= 7 June 2014}}</ref> In 1920 Rees was presented with a sword and the freedom of Caernarfon.<ref name="times-freedom">{{Cite newspaper The Times |articlename=Airman's Bravery Honoured |author= |section=Official Appointments and Notices |day_of_week=Friday |date=16 January 1920 |page_number=9 |issue=42309 |column= }}</ref>
[[File:Caernarfon Brabazon Rees VC plaque.jpg|thumb|right|<center>Plaque honouring Rees in Caernarfon</center>]]

In June 1920, he took command of the flying wing at [[RAF College Cranwell]], becoming the assistant commandant in March 1923.<ref name="odnb" /> Promoted to [[group captain]] after the RAF moved away from the Army rank structure, Rees became deputy director in the Air Ministry directorate of training and in January 1925 he became an additional air aide-de-camp to the King.<ref name="odnb" />

He was posted to [[RAF Amman]] in May 1926 and given command of the RAF Transjordan and Palestine in October 1926.<ref name="odnb" /> He had a keen interest in archaeology, and while flying on the [[Cairo]] to [[Baghdad]] route during this period, he took some of the earliest archaeological aerial photographs of sites in eastern [[Emirate of Transjordan|Transjordan]] (now Jordan), and published several articles in ''[[Antiquity (journal)|Antiquity]]'' and the journal of the [[Palestine Exploration Fund]].  He is considered a father of the archaeological studies of this remote area, and a pioneer of aerial archaeology.<ref name="Jordan pages 69-81"/>

Rees returned to England in October 1926 to take command of the RAF Depot at [[RAF Uxbridge|Uxbridge]] before he retired  
from the RAF in 1931 with the rank of group captain.<ref name="Pusher Aces of World War 1"/> In 1933, he sailed single-handedly across the Atlantic from Wales to Nassau in the Bahamas in a [[ketch]].  For this achievement he was awarded the prestigious Blue Water Medal by the [[Cruising Club of America]] in 1934.<ref>{{cite web |url=https://www.cruisingclub.org/pdfs/bluewater.pdf |title=The Blue Water Medal Awards 1923&ndash;2004 |accessdate=7 June 2014}}</ref>

When the Second World War broke out, Rees returned to the United Kingdom from the Bahamas and once again joined the RAF. He relinquished his rank of group captain in January 1941 at his own request and was granted the rank of [[Wing commander (rank)|wing commander]].<ref name="times35076">{{LondonGazette |issue=35076 |supp= no |startpage=902 |date=14 February 1941 |accessdate= 8 June 2014}}</ref> He served in Africa.<ref name="odnb">Sweetman, John, ‘Rees, Lionel Wilmot Brabazon (1884–1955)’, ''Oxford Dictionary of National Biography'', Oxford University Press, 2004, [http://www.oxforddnb.com/view/article/71860, accessed 8 June 2014]</ref> On 21 November 1942 Rees reverted to the rank of group captain on the retired list.<ref name="times ">{{LondonGazette |issue=35940 |supp= yes |startpage=1247 |date=12 March 1943 |accessdate= 8 June 2014}}</ref>

Rees returned home to the Bahamas and on 12 August 1947, aged 62, he married Sylvia Williams, a young local woman. They had three children.<ref>{{cite web |url=http://www.caernarfononline.co.uk/wyddech_chi/vc.html |archiveurl=https://web.archive.org/web/20080213030018/http://www.caernarfononline.co.uk/wyddech_chi/vc.html |archivedate=13 February 2008 |accessdate=7 June 2014|title=Caernarfon's One and Only VC |work=Caernarfon &ndash; Did You Know }}</ref>   Rees died at the Princess Margaret Hospital in Nassau on 28 September 1955<ref name="Pusher Aces of World War 1"/> from [[leukemia|leukaemia]].<ref name="odnb" /> He was buried at Nassau war cemetery.<ref name="odnb" /> Described as a real gentleman, during his time in Transjordan and possibly during other parts of his career, Rees was known to give his pay to service charities.<ref name="odnb" />

==Selected publications by Rees==
Rees wrote under the name 'L. W. B. Rees'.
* 1916: ''Fighting in the Air''<ref>{{cite web |url=http://catalogue.nla.gov.au/Record/1711228 |title=Catalogue Item: ''Fighting in the Air'' / by L.W.B. Rees |publisher=National Library of Australia |work=Catalogue |accessdate=7 June 2014}}</ref>
* 1927: 'Ancient Reservoirs near [[Qasr Azraq|Kasr Azrak]]'<ref>''[[Antiquity (journal)|Antiquity]]'', vol 1, pp. 89–92</ref>
* 1929: 'The Transjordan Desert'<ref>''Antiquity'', vol 3, no 12, December 1929, pp. 389–407</ref>
* 1930: 'Transjordan: an ancient and a modern raid' <ref>''The Royal Air Force Quarterly'', January 1930, vol. 1, no 1, pp. 138–159</ref>
* 1948: 'The Route of the Exodus: The First Stage, Ramies to Etham'<ref>''[[Palestine Exploration Quarterly]]'', January – April 1948, pp. 48–58</ref>

==References==
;Citations
{{reflist|30em}}

;Bibliography
* {{cite book |last1=Guttman|first1=Jon |last2=Dempsey |first2=Harvey |year=2009 |title=Pusher Aces of World War 1 |location=Oxford |publisher=Osprey Publishing Company  |isbn=978-1-84603-417-6}}
* {{cite book |series=History of the Great War based on Official Documents by Direction of the Historical Section of the Committee of Imperial Defence |title=The War in the Air: Being the Story of the Part Played in the Great War by the Royal Air Force |volume=II |last=Jones |first=H. A. |authorlink= |year=1928 |publisher=Clarendon Press |location=Oxford |edition=N & M Press 2002 |url=https://ia801609.us.archive.org/29/items/warinairbeingsto02rale/warinairbeingsto02rale.pdf |accessdate=28 May 2014 |isbn=1-84342-413-4}}
* {{cite book |last1=Shores|first1=Christopher |last2=Franks |first2=Norman |last3=Guest|first3=Russell |year=1990 |title=Above the Trenches |publisher=Grub Street |location=London |isbn=978-1-89869-739-8}}
{{refend}}

==Further reading==
* {{cite book |last=Cooksley |first=P.G. |year=1999 |title=[[VCs of the First World War - Air VCs]] |publisher=Sutton |location=Stroud |isbn=9780750922722 }}
* {{cite book |last=Harvey |first= David |year=1999 |title=[[Monuments To Courage]] |publisher=K. and K. Patience |location=Weybridge, Surrey |oclc=45224322 }} 
* {{cite book |author=Staff |title=[[The Register of the Victoria Cross]] |location=Cheltenham, Gloucestershire |publisher=This England Books |edition=3rd |year=1997 |isbn=9780906324271}}
* {{cite book |last=Williams |first=W. Alister |year=1989 |title=Against the Odds – The Life of Group Captain Lionel Rees, VC |location=Wrexham, Clwyd |publisher=Bridge Books |isbn=9781872424002}}

==External links==
* [http://www.theaerodrome.com/aces/wales/rees.html www.theaerodrome.com – Lionel Rees]
* [http://www.old-picture.com/american-legacy/012/LWB-Major-Rees.htm Undated photograph of Rees]
* {{Find a Grave|7723731}}
* [http://www.caernarfononline.co.uk/wyddech_chi/vc.html A short biography]

{{DEFAULTSORT:Rees, Lionel Wilmot Brabazon}}
[[Category:1884 births]]
[[Category:1955 deaths]]
[[Category:British Army personnel of World War I]]
[[Category:British Army recipients of the Victoria Cross]]
[[Category:British World War I recipients of the Victoria Cross]]
[[Category:British World War I flying aces]]
[[Category:Deaths from leukemia]]
[[Category:Graduates of the Royal Military Academy, Woolwich]]
[[Category:Officers of the Order of the British Empire]]
[[Category:People educated at Eastbourne College]]
[[Category:People from Caernarfon]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Recipients of the Military Cross]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Royal Artillery officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal West African Frontier Force officers]]
[[Category:Welsh aviators]]
[[Category:Welsh recipients of the Victoria Cross]]
[[Category:Remote sensing archaeologists]]