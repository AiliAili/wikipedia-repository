{{more footnotes|date=December 2015}}
[[File:Joyce and Ross Bell, June 2010.jpg|thumb|Bell (right) with his wife Joyce in 2010]]
'''Ross Taylor Bell''' (born 23 April 1929) is an American entomologist with particular interest in the invertebrate natural history of [[Vermont]], United States,and [[Ground beetle|carabid]] beetles. Together with his wife, Joyce Bell, his work at the [[University of Vermont]] was largely taxonomic, where they described more than 75% of the [[Rhysodidae|rhysodine]] species known to science. Ross also wrote a number of seminal papers in his chosen field.<ref>{{Cite journal|title = ‘Bellography’: Life and Contributions of Ross and Joyce Bell, two New England Naturalists|journal = ZooKeys|date = 2011-11-16|issn = 1313-2989|pmc = 3286264|pmid = 22371660|pages = 3–13|issue = 147|doi = 10.3897/zookeys.147.1999|first = John R.|last = Spence|first2 = George E.|last2 = Ball|first3 = Robert L.|last3 = Davidson|first4 = Jessica J.|last4 = Rykken}}</ref>

== Biography ==
Ross Bell was born April 23, 1929 in [[Champaign, Illinois]], to parents Alfred Hannam Bell and Dorothy Bell. He had two sisters - Martha and Enid. The family were keen naturalists, and family vacations included long drives to various locations in the USA where the family would pursue their particular interests in botany, zoology, and geology.

Bell's particular interest in insects began with a childhood gift of an insect collecting kit from his parents. He also received a jar of silk worm larvae, which he reared on mulberry leaves. At age 14, he gained employment at the Natural History Survey at the [[University of Illinois system|University of Illinois]] where he sorted and identified sarcophagids, muscids and calliphorids. The following summer, he took a job with the taxonomic survey, though this turned out to provide him with little scope to develop his skills and interests.

Bell went to high school at the [[University Laboratory High School (Urbana, Illinois)|University Laboratory High School]] in Urbana. He spent his summers at his aunt and uncle's farm in Ohio where, in between farm chores, he would collect and attempt to name insects from the fields and nearby stream.

He spent 1946 to 1949 at the University of Illinois where he gained a BS in zoology. He then went on to gain his MS with a thesis about the Carabidae (Simplicia) of Illinois. In the early 1950s, under the sponsorship of entomologist W.V. Balduf, he completed his doctoral dissertation about the comparative [[Morphology (biology)|morphology]] and [[Phylogenetic tree|phylogeny]] of [[Adephaga]].

Whilst at Illinois, Bell became friends with 'Butterfly' Bob Snetsinger. Through him, Bell developed interests in life history and larval biology. He also became interested in ecology and became president of the department's ecology club.

Following the completion of his PhD, Bell was awarded a [[Fulbright Program|Fulbright Fellowship]] to go to India. Before he could take this up, though, he was called for national service, and spent two years at Fort Dietrich, Maryland, then known as America's 'Germ Warfare Centre'. Spending a significant amount of time working with fleas, he discovered a rapid way to differentiate males from females. On discharge, he joined the [[University of Vermont]] (UVM), lecturing on field zoology, invertebrate zoology, entomology and mountain ecology.

In the summer of 1956, he went collecting insects in Mexico with Don Van Horn. It was here that he discovered his first undescribed rhysodine beetle. This led to a lifelong fascination which established him as the world expert on these particular beetles.

In the following year, he married Joyce Elaine Rockenbach of Whitestone, Queens, New York City. The two became inseparable companions in the pursuit of entomology. During the 1960s, they began an active program to document the arthropod fauna of Vermont. Their work built the UVM Entomological Collection into a significant resource for Northern New England. In the 1970s and 1980s, Ross and Joyce extended the boundaries for their entomological work beyond Vermont, stretching as far as New Zealand and Papua New Guinea.

Bell retired from UVM in 2000, but continued to teach a field course in entomology for a further four years. He and his wife continue to study entomology, and in particular rhysodines to the present day.

== Rhysodidae beetles ==
Bell is primarily known for his lifelong fascination with the rhysodidae family of beetles. Of the 350 known species in the family, Bell identified and classified some 260 of them.<ref>{{Cite web|title = In This State: Famous in the world of beetles, Ross Bell is Vermont’s bug virtuoso|url = http://vtdigger.org/2012/03/11/in-this-state-famous-in-the-world-of-beetles-ross-bell-is-vermonts-bug-virtuoso/|website = VTDigger|accessdate = 2015-12-14}}</ref>

He has also published a significant number of papers, including:<ref>{{Cite web|title = Rhysodini|url = http://tolweb.org/tree?group=Rhysodini&contgroup=Carabidae|website = tolweb.org|accessdate = 2015-12-14}}</ref>
* Bell, R. T. 1970. The Rhysodini of North America, Central America and the West Indies. Miscellaneous Publications of the Entomological Society of America 6: 289-324.
* Bell, R. T. 1973. New species of Clinidium from Guatemala (Coleoptera, Carabidae or Rhysodidae). Proceedings of the Entomological Society of Washington 75(3): 279-282.
* Bell, R. T. 1975. Omoglymmius Ganglbauer, a separate genus (Coleoptera: Carabidae or Rhysodidae). The Coleopterists Bulletin 29(4):351-352.
* Bell, R. T. 1979. Zoogeography of Rhysodini: Do beetles travel on driftwood? Pages 331-342 in Carabid Beetles: Their Evolution, Natural History and Classification (T. L. Erwin, G. E. Ball, and D. R. Whitehead, eds.). Proceedings of the First International Symposium of Carabidology. Smithsonoan Institution, Washington, D.C.
* Bell, R. T. 1985. Zoogeography and ecology of New Guinea Rhysodini (Coleoptera: Carabidae). Pages 221-235 in Taxonomy, Phylogeny and Zoogeography of Beetles and Ants (G. E. Ball, ed.). Dr. W. Junk Publishers, Dordrecht.
* Bell, R. T. 1985. Family: Rhysodidae. A Catalog of the Coleoptera of America North of Mexico. United States Department of Agriculture. Agriculture Handbook Number 529-4:1-4.
* Bell, R. T. 1991. Rhysodidae (Adephaga). Pages 304-305 in Immature Insects Volume 2 (F. W. Stehr, ed.). Kendall/Hunt Publishing Company, Dubuque, Iowa.
* Bell, R. T. 1994. Beetles that cannot bite: functional morphology of the head of adult Rhysodines (Coleoptera: Carabidae or Rhysodidae). The Canadian Entomologist 126:667-672.
* Bell, R. T. 1998. Where do the Rhysodini (Coleoptera) belong? Pages 261-272 in Phylogeny and Classification of Caraboidea. XX I.C.E. (1996, Firenze, Italy) (G. E. Ball, A. Casale, and A. Vigna Taglianti, eds.). Museo Regionale di Scienze Naturali, Torino.
* Bell, R. T. and J. R. Bell. 1962. The taxonomic position of the Rhysodidae. The Coleopterists Bulletin 15:99-106.
* Bell, R. T. and J. R. Bell. 1975. Two new taxa of Clinidium (Coleoptera: Rhysodidae or Carabidae) from the Eastern U.S., with a revised key to U.S. Clinidium. The Coleopterists Bulletin 29(2):65-68.
* Bell, R. T. and J. R. Bell. 1978. Rhysodini of the World part I. A new classification of the tribe, and a synopsis of Omoglymmius subgenus Nitiglymmius, new subgenus (Coleoptera: Carabidae or Rhysodidae). Quaestiones Entomologicae 14: 43-88.
* Bell, R. T. and J. R. Bell. 1979. Rhysodini of the World part II. Revisions of the smaller genera (Coleoptera: Carabidae or Rhysodidae). Quaestiones Entomologicae 15: 377-446.
* Bell, R. T. and J. R. Bell. 1982. Rhysodini of the World Part III. Revision of Omoglymmius Ganglbauer (Coleoptera: Carabidae or Rhysodidae) and substitutions for preoccupied generic names. Quaestiones Entomologicae 18: 127-259.
* Bell, R. T. and J. R. Bell. 1985. Rhysodini of the World Part IV. Revisions of Rhyzodiastes and Clinidium, with new species in other genera (Coleoptera: Carabidae or Rhysodidae). Quaestiones Entomologicae 21(1): 1-172.
* Bell, R. T. and J. R. Bell. 1987. A new species of Clinidium Kirby (Coleoptera: Carabidae or Rhysodidae) from Mexico, and descriptions of the females of two Neotropical members of the genus. Annals Of The Carnegie Museum 56(8-14): 193-196.
* Bell, R. T. and J. R. Bell. 1987. A new subtribe, genus and species of Rhysodini from South Africa (Coleoptera: Carabidae or Rhysodidae). Journal Of The Entomological Society Of Southern Africa 50(2): 287-290.
* Bell, R. T. and J. R. Bell. 1987. Rhysodine beetles on the Geneva collection: a new species of Yamatosa, and a major range extension for Omoglymmius sakuraii Nakane (Coleoptera: Carabidae or Rhysodidae). Revue suisse Zool. 94(4):683-686.
* Bell, R. T. and J. R. Bell. 1988. Rhysodini of Sulawesi (Indonesia) and nearby islands (Coleoptera: Carabidae or Rhysodidae). Journal Of The New York Entomological Society 96(1): 7-15.
* Bell, R. T. and J. R. Bell. 1989. Rhysodine beetles on the Geneva collection II: new species of Yamatosa and Omoglymmius, descriptions of undescribed sexes in other species, and some major range extensions (Coleoptera: Carabidae or Rhysodidae). Revue suisse Zool. 96(3):637-642.
* Bell, R. T. and J. R. Bell. 1991. The Rhysodini of Australia (Insecta: Coleoptera: Carabidae or Rhysodidae). Annals Of The Carnegie Museum 60(3): 179-210.
* Bell, R. T. and J. R. Bell. 1993. Rhysodine beetles (Insecta: Coleoptera: Carabidae or Rhysodidae): New species, new data, and revised keys to Omoglymmius (Subgenera Omoglymmius and Pyxiglimmius). Annals of Carnegie Museum 62(2): 165-185 .
* Bell, R. T. and J. R. Bell. 1995. The Rhysodini (Insecta: Coleoptera: Carabidae) of Cuba. Annals of Carnegie Museum 64(3): 185-195.

== References ==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Bell, Ross}}
[[Category:1929 births]]
[[Category:People from Vermont]]
[[Category:People from Champaign, Illinois]]
[[Category:University of Vermont faculty]]
[[Category:Living people]]
[[Category:American entomologists]]
[[Category:Coleopterists]]
[[Category:University of Illinois alumni]]