{{featured article}}
{{Italic title}}
{{Use American English|date=December 2016}}
[[File:Lynd Ward (1930) Madman's Drum cover.jpg|thumb|upright=1.2|alt=A book cover with a black-and-white illustration of a man standing atop an African drum with the image of a face on it.  The title at the top reads "Madman's Drum", and at the bottom reads "A novel in woodcuts by Lynd Ward".|First edition cover of ''Madman's Drum'' (1930)]]
<!-- No infobox, please -->
'''''Madman's Drum''''' is a [[wordless novel]] by American artist [[Lynd Ward]] (1905–1985), published in 1930.  It is the second of Ward's six wordless novels.  The 118 [[wood engraving|wood-engraved]] images of ''Madman's Drum'' tell the story of a slave trader who steals a demon-faced drum from an African he murders, and the consequences for him and his family.

Ward's first wordless novel was {{not a typo|''[[Gods' Man]]''}} of 1929.  Ward was more ambitious with his second work in the medium: the characters are more nuanced, the plot more developed and complicated, and the outrage at social injustice more explicit.  Ward used a wider variety of carving tools to achieve a finer degree of detail in the artwork, and was expressive in his use of symbolism and exaggerated emotional facial expressions.

The book was well received upon release, and the success of Ward's first two wordless novels encouraged publishers to publish more books in the genre.  In 1943 psychologist [[Henry Murray]] used two images from the work in his [[Thematic Apperception Test]] of personality traits.  ''Madman's Drum'' is considered less successfully executed than ''{{not a typo|Gods' Man}}'', and Ward streamlined his work in his next wordless novel, ''[[Wild Pilgrimage]]'' (1932).

==Synopsis==

A slave trader condemns his family to the curse of a demon-faced drum he steals from an African.  The slave trader becomes rich and buys a mansion for his family, in which he displays the drum and the sword he used to kill the drum's original owner.  He catches his son playing on the drum, beats the boy, and insists he read and study.  The slave trader is lost at sea when he tries to return to Africa.

The boy devotes himself to study, while distancing himself from the vices of his peers.  He embraces and then rejects religion, and a cross he tosses to the floor trips and kills his mother.  He becomes a successful scientist, and in middle age marries and has two daughters, but is cold and indifferent to his family.  One by one he loses them: his wife dies after having an affair with a musician, one daughter falls into depression when her labor-organizer lover is hanged for his communist sympathies, and the other daughter falls in love with a man who pimps her to others.  Driven insane by the loss of all who were close to him, he equips himself with the forbidden drum to play music with his wife's musician lover.

==Background==
[[Lynd Ward]] (1905–1985) was born in Chicago{{sfn|Spiegelman|2010b|p=799}} to [[Methodism|Methodist]] minister [[Harry&nbsp;F. Ward]], a social activist and the first chairman of the [[American Civil Liberties Union]].  Throughout his career, the younger Ward's work displayed the influence of his father's interest in social injustice.{{sfn|Beronä|2008|p=41}}  He was drawn to art from an early age,{{sfn|Spiegelman|2010b|p=801}} and contributed images and text to high school and college newspapers.{{sfn|Spiegelman|2010b|pp=802–803}}

[[File:Frans Masereel (1919) Die Sonne self-portrait.jpg|thumb|left|alt=Black-and-white illustration of a man seated and hunched over a table, facing left, holding his art tools.  Out the window on the left, the sun beats down upon the man.|[[Frans Masereel]]'s ''[[The Sun (wordless novel)|The Sun]]'' (1919), the first [[wordless novel]] Ward read]]

After getting a university degree in fine arts{{sfn|Spiegelman|2010a|p=x}} in 1926, Ward married writer [[May McNeer]] and the couple left for an extended honeymoon in Europe.{{sfn|Spiegelman|2010b|pp=803–804}}  Ward spent a year studying [[wood engraving]] in [[Leipzig]], Germany, where he encountered [[German Expressionism|German Expressionist]] art and read the wordless novel ''[[The Sun (wordless novel)|The Sun]]''{{efn|{{lang-de|Die Sonne|links=no}}; originally {{lang-fr|Le Soleil|links=no}}{{sfn|Beronä|2008|p=244}} }} (1919) by Flemish [[woodcut]] artist [[Frans Masereel]] (1889–1972).  Ward returned to the United States and freelanced his illustrations.  In New York City in 1929, he came across the wordless novel ''[[Destiny (wordless novel)|Destiny]]''{{efn|{{lang-de|Schicksal : eine Geschichte in Bildern|links=no}} }} (1926) by German artist [[Otto Nückel]] (1888–1955).{{sfn|Spiegelman|2010b|pp=804–805}}  Nückel's only work in the genre, ''Destiny'' told of the life and death of a prostitute in a style inspired by Masereel's, but with a greater cinematic flow.{{sfn|Spiegelman|2010a|p=x}}  The work inspired Ward to create a wordless novel of his own: ''{{not a typo|[[Gods' Man]]}}'' (1929).{{sfn|Spiegelman|2010b|pp=804–805}}  In his second such work, ''Madman's Drum'', he hoped to explore more deeply the potential of the narrative medium, and to overcome what he saw as a lack of individuality in the characters in {{not a typo|''Gods' Man''}}.{{sfn|Spiegelman|2010a|p=xiv}}

==Production and publishing history==

Ward made 118 woodcuts for ''Madman's Drum''.{{sfn|Beronä|2008|p=52}}  The black-and-white{{sfn|Ward|Beronä|2005|p=iii}} images are not uniform in size—they measure from {{convert|4|x|3|in|cm}} to {{convert|5|x|4|in|cm}}.{{sfn|Beronä|2008|p=52}}  Cape & Smith published the book in October 1930 in trade and deluxe editions,{{sfn|Spiegelman|2010b|p=806}} the latter in a signed edition limited to 309 copies.{{sfn|Ahearn|Ahearn|2002|p=1998}}  Jonathon Cape published the book in the UK in 1930.  It had a Japanese publication in 2002 by {{Interlanguage link multi|Kokusho Kankōkai|ja|3=国書刊行会}}, and  in 2005 [[Dover Publications]] brought it back into print as a standalone edition in the US.{{sfn|Beronä|2008|p=245}}  It appeared in the collected volume ''Storyteller Without Words: The Wood Engravings of Lynd Ward'' in 1974,{{sfn|Beronä|2008|p=245}} and again in 2010 in the [[Library of America]] collection ''Lynd Ward: Six Novels in Woodcuts'', edited by cartoonist [[Art Spiegelman]].<ref>{{worldcat|name=Lynd Ward: Six Novels in Woodcuts|oclc=857302187}}</ref>  The original woodblocks are in the Lynd Ward Collection in the Joseph Mark Lauinger Memorial Library at [[Georgetown University]] in Washington, DC.{{sfn|Smykla|1999|p=53}}

{{clear}}

==Style and analysis==

[[File:Madman's Drum - leaning against tree.gif|thumb|alt=Illustration of a man leaning against a tree|Ward used a greater variety of tools than in ''{{not a typo|[[Gods' Man]]}}'': a multiple-tint tool for the parallel lines in the sky, and rounded tools for organic textures in the trees.]]

''Madman's Drum'' is a more ambitious work than ''Gods' Man'', with a larger cast of characters and more complicated plot.  The book is more explicit in its far-leftist politics, and includes a subplot in which the main character's sister's communist lover is executed for his political beliefs.{{sfn|Spiegelman|2010a|p=xv}}  Late in life Ward described it as "set a hundred years or more ago&nbsp;... in an obviously foreign land", but that the story's situation and characters could be encountered "almost anywhere at any time".{{sfn|Beronä|2003|p=67}}

The art has a variety of line qualities and textures, and more detail than in ''Gods' Man''.  Ward availed himself of a larger variety of engraving tools, such as the multiple-tint tool for making groups of parallel lines, and rounded engraving tools for organic textures.{{sfn|Ward|Beronä|2005|pp=iii–v}}  The large cast of characters is distinguished by visual details in faces and clothing, such as the main character's sharp nose and receding hairline and his wife's checked dress.{{sfn|Ward|Beronä|2005|p=iv}}

A wide range of emotions such as resentment and terror is expressed through exaggerated facial expressions.{{sfn|Ward|Beronä|2005|pp=iv–v}}  Ward broadens his use of visual symbolism, as with a young woman's purity represented by a flower she wears—she is deflowered by a young man whose vest is adorned with flowers.{{sfn|Beronä|2008|p=52}}  His house also displays a floral stucco pattern and is adorned with phallic spears and an exultant rooster as a weathervane.{{sfn|Ward|Beronä|2005|p=iv}}  To French comics scripter {{Interlanguage link multi|Jérôme LeGlatin|fr|3=J&E LeGlatin}}, the "madman" in the title could be interpreted as any of a number of its characters: the laughing image adorning the drum, the subdued African, the slave trader, and even Ward himself.{{sfn|LeGlatin|2012}}

==Reception and legacy==

The book's release in 1930 was well-received,{{sfn|Walker|2007|p=27}} though it did not sell as well as ''{{not a typo|Gods' Man}}''.{{sfn|Beronä|2003|p=67}}  The success of Ward's first two wordless novels led American publishers to put out a number of such books, including Nückel's ''Destiny'' in 1930, as well as books by Americans and other Europeans.{{sfn|Ward|Beronä|2005|p=v}}  Interest in wordless novels was short-lived,{{sfn|Beronä|2003|pp=67–68}} and few besides Masereel and Ward produced more than a single work.{{sfn|Willett|2005|p=131}}  Each of Ward's sold fewer copies than the last, and he abandoned the genre in 1940 after abandoning an attempt at a seventh.{{sfn|Ward|Beronä|2009|p=v}}  In 1943 psychologist [[Henry Murray]] used two images from ''Madman's Drum'' in his [[Thematic Apperception Test]] of personality traits.{{sfn|Ward|Beronä|2005|p=v}}

A reviewer for ''[[The Burlington Magazine]]'' in 1931 judged the book a failed experiment, finding the artwork uneven and the narrative hard to follow without even the chapter titles as textual guidance that ''{{not a typo|Gods' Man}}'' had.{{sfn|E. P.|1931|p=99}}  Cartoonist [[Art Spiegelman]] considered Ward's second wordless novel a "sophomore slump",{{sfn|Spiegelman|2010a|p=xvi}} whose story was bogged down by Ward's attempt to flesh out the characters and produce a more complicated plot.  He believed the artwork was a mix of strengths and weaknesses: it had stronger compositions, but the more finely engraved images were "harder to read",{{sfn|Spiegelman|2010a|p=xv}} and the death of the wife and other plot points were unclear and difficult to interpret.  Spiegelman considered Ward to have broken free from this slump by streamlining his work in his next wordless novel, ''[[Wild Pilgrimage]]'' (1932).{{sfn|Spiegelman|2010a|pp=xv–xvi}}  Jérôme LeGlatin declared ''Madman's Drum'' Ward's first masterpiece, "{{interp|triumphing|original=to triumph}} at every fault, {{interp|succeeding|original=to succeed}} in each failure" as Ward freed himself from the restraint displayed in {{not a typo|''Gods' Man''}}.{{sfn|LeGlatin|2012}}

==Notes==

{{Notelist|40em}}

==References==

{{Reflist|colwidth=20em}}

===Works cited===

{{Refbegin|colwidth=40em}}

* {{cite book
|last1     = Ahearn
|first1    = Allen
|last2     = Ahearn
|first2    = Patricia
|title     = Collected Books: The Guide to Identification and Values
|url       = https://books.google.com/books?id=oUMxWVOI_goC
|year      = 2002
|publisher = Quill & Brush
|isbn      = 978-1-883060-14-5
|ref       = harv}}
* {{cite journal
|title     = Wordless Novels in Woodcuts
|first     = David A.
|last      = Beronä
|journal   = [[Print Quarterly]]
|volume    = 20
|issue     = 1
|date      = March 2003
|pages     = 61–73
|publisher = Print Quarterly Publications
|jstor     = 41826477
|issn      = 0265-8305
|ref       = harv}}
* {{cite book
|last      = Beronä
|first     = David A.
|title     = Wordless Books: The Original Graphic Novels
|url       = https://books.google.com/books?id=YVh2QgAACAAJ
|year      = 2008
|publisher = [[Abrams Books]]
|isbn      = 978-0-8109-9469-0
|ref       = harv}}
* {{cite journal
|author    = E. P.
|title     = Madman's Drum: A Novel in Woodcuts
|journal   = [[The Burlington Magazine|The Burlington Magazine for Connoisseurs]]
|volume    = 59
|issue     = 341
|date      = August 1931
|pages     = 98–99
|publisher = The Burlington Magazine Publications Ltd.
|jstor     = 864796
|ref       = harv}}
* {{cite web
|last       = LeGlatin
|first      = Jérôme
|others     = Translated by Benoît Crucifix
|title      = Lynd Ward: Six Novels in Woodcuts
|date       = February 2012
|work       = du9
|url        = http://www.du9.org/en/chronique/lynd-ward-six-novels-in-woodcuts/
|accessdate = 2014-04-22
|ref        = harv}}
* {{cite book
|last      = Smykla
|first     = Evelyn Ortiz
|title     = Marketing and Public Relations Activities in ARL Libraries: A SPEC Kit
|url       = https://books.google.com/books?id=HAHhAAAAMAAJ
|year      = 1999
|publisher = [[Association of Research Libraries]]
|id        = UOM:39015042082936
|ref       = harv}}
* {{cite book
|last         = Spiegelman
|first        = Art
|authorlink   = Art Spiegelman
|editor-last  = Spiegelman
|editor-first = Art
|chapter      = Reading Pictures
|pages        = ix–xxv
|title        = Lynd Ward: God's Man, Madman's Drum, Wild Pilgrimage
|year         = 2010
|publisher    = [[Library of America]]
|isbn         = 978-1-59853-080-3
|ref          = {{SfnRef|Spiegelman|2010a}} }}
* {{cite book
|last         = Spiegelman
|first        = Art
|authorlink   = Art Spiegelman
|editor-last  = Spiegelman
|editor-first = Art
|chapter      = Chronology
|pages        = 799–821
|title        = Lynd Ward: God's Man, Madman's Drum, Wild Pilgrimage
|year         = 2010
|publisher    = [[Library of America]]
|isbn         = 978-1-59853-080-3
|ref          = {{SfnRef|Spiegelman|2010b}} }}
* {{cite book
|editor-last  = Walker
|editor-first = George
|title        = Graphic Witness: Four Wordless Graphic Novels
|url          = https://books.google.com/books?id=yqQNBAAACAAJ
|year         = 2007
|publisher    = [[Firefly Books]]
|isbn         = 978-1-55407-270-5
|ref          = harv}}
* {{cite book
|last1       = Ward
|first1      = Lynd
|authorlink1 = Lynd Ward
|last2       = Beronä
|first2      = David
|title       = Mad Man's Drum: A Novel in Woodcuts
|year        = 2005
|publisher   = [[Dover Publications]]
|isbn        = 978-0-486-44500-7
|chapter     = Introduction
|pages       = iii–vi
|ref         = harv}}
* {{cite book
|last1     = Ward
|first1    = Lynd
|last2     = Beronä
|first2    = David A.
|title     = Vertigo: A Novel in Woodcuts
|chapter   = Introduction
|pages     = v–ix
|date      = 2009
|publisher = Dover Publications
|isbn      = 978-0-486-46889-1
|ref       = harv}}
* {{cite book
|last         = Willett
|first        = Perry
|title        = A Companion to the Literature of German Expressionism
|editor-last  = Donahue
|editor-first = Neil H.
|year         = 2005
|publisher    = [[Camden House Publishing]]
|isbn         = 978-1-57113-175-1
|pages        = 111–134
|chapter      = The Cutting Edge of German Expressionism: The Woodcut Novel of Frans Masereel and Its Influences|url          = https://books.google.com/books?id=zjvV48n-ngUC&pg=PA111
|ref          = harv}}

{{Refend}}

{{Wordless novels}}
{{portal bar|Comics|Novels|Visual arts}}

[[Category:1930 books]]
[[Category:Wordless novels by Lynd Ward]]