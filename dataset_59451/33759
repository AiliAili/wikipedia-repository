{{featured article}}
{{Infobox film
| name      = Subway Sadie
| image     = File:Subway Sadie lobby card 1926.jpg
| image size   = 275
| border     = 
| alt      = 
| caption    = [[Lobby card]]
| director    =[[Alfred Santell]]
| producer    =Al Rockett
| based on       ={{based on|''Sadie of the Desert''|[[Mildred Cram]]}}
| screenplay = [[Adele Comandini]]<br />[[Paul Schofield (screenwriter)|Paul Schofield]]
| starring    = [[Dorothy Mackaill]]<br />[[Jack Mulhall]]
| cinematography = [[Arthur Edeson]]
| editing    = Hugh Bennett
| studio = Al Rockett Productions
| distributor  = [[First National Pictures]]
| released    = {{Film date|1926|09|12}}
| runtime    = 70 minutes
| country = United States
| language = [[Silent film|Silent]] with intertitles
}}
'''''Subway Sadie''''' is a 1926 American [[comedy-drama]] [[silent film]] directed by [[Alfred Santell]]. Adapted from [[Mildred Cram]]'s 1925 short story "Sadie of the Desert", the film focuses on a relationship between [[New York City|New York]] salesgirl Sadie Hermann ([[Dorothy Mackaill]]) and [[New York City Subway|subway]] guard Herb McCarthy ([[Jack Mulhall]]), who meet on the subway and become engaged. However, after Sadie receives a promotion, she must choose between her new job and marrying Herb. The cast also includes [[Charles Murray (American actor)|Charles Murray]], Peggy Shaw, [[Gaston Glass]], and Bernard Randall.

The film began production in May 1926. [[Arthur Edeson]] served as cinematographer, shooting around [[Central Park]] in areas like casinos and nightclubs. Distributed by [[First National Pictures]], the film premiered in New York on September 12, 1926. Many publications wrote positively of the film, praising its acting and Santell's direction. Today, it remains unknown if a print of ''Subway Sadie'' has survived.

==Plot==
Salesgirl Sadie Hermann ([[Dorothy Mackaill]]), employed in a [[New York City]] fur store, has always dreamed of traveling to [[Paris]]. While riding the [[New York City Subway|subway]] to work one morning, she meets Irish subway guard Herb McCarthy ([[Jack Mulhall]]), and the two strike up a conversation before Herb eventually arranges to have them meet at [[Cleopatra's Needle (New York City)|Cleopatra's Needle]] that Sunday.

Herb and Sadie are soon engaged to be married, but as Sadie has been promoted from saleslady to firm buyer she must cancel the wedding date to sail to Paris for the job, saddening Herb. Sadie prepares to leave, but then receives a message from Herb, which informs her that he is in the hospital as the result of an accident. Sadie chooses to visit him, and she decides to forgo her new job and marry Herb instead, Herb revealing that his father is the president of the subway company.<ref>{{cite web|title=Subway Sadie (1926) – Full Synopsis|url=http://www.tcm.com/tcmdb/title/501203/Subway-Sadie/full-synopsis.html|work=[[Turner Classic Movies]]|accessdate=January 12, 2014}}</ref><ref name=NBOR>{{cite web|title=Subway Sadie|url=https://archive.org/stream/nationalboardofr192628natirich/nationalboardofr192628natirich_djvu.txt|work=[[National Board of Review]]|publisher=[[Internet Archive]]|accessdate=January 12, 2014}}</ref>

==Production==
{{multiple image
| footer    = Dorothy Mackaill and Jack Mulhall, who both had lead roles in ''Subway Sadie''
| image2    = Jack Mulhall - Jul 1920 MP.jpg
| alt2      = Jack Mulhall, American actor, who played Herb McCarthy in ''Subway Sadie''
| width2    = 220

| image1    = Dorothy MacKaill Stars of the Photoplay.jpg
| alt1      = Dorothy Mackaill, actress who played Sadie Hermann in ''Subway Sadie''
| width1= 195
}}
[[Alfred Santell]] directed ''Subway Sadie'',<ref name=NBOR/> from a screenplay written by [[Adele Comandini]] and [[Paul Schofield (screenwriter)|Paul Schofield]]. The pair adapted a short story by [[Mildred Cram]] entitled "Sadie of the Desert", which had first been published in an October 1925 issue of [[Redbook|''The Red Book Magazine'']].<ref name=afi>{{cite web|title=Subway Sadie|url=http://www.afi.com/members/catalog/DetailView.aspx?s=1&Movie=12478|work=[[American Film Institute]]|accessdate=January 12, 2014}}</ref>

On May 3, 1926, the film entered production.<ref>{{cite web|title=Peggy Shaw in Subway Sadie|url=https://archive.org/stream/filmdaily3536newy#page/1248/mode/2up|website=[[The Film Daily]]|publisher=[[Internet Archive]]}}</ref> Santell and Al Rockett, the film's producer and production manager, selected the actors to appear in the film. [[Jack Mulhall]] was cast in the lead as Herb McCarthy. Although he had ridden on a subway many times, he did not take notice of the guards, so as preparation for playing the character he rode on a subway for "practically an entire day" to observe them.<ref>{{cite news|title=Jack Mulhall Rides Subway for Accuracy|url=https://news.google.com/newspapers?nid=2194&dat=19261016&id=AcUuAAAAIBAJ&sjid=o9kFAAAAIBAJ&pg=5532,1806922|accessdate=January 12, 2014|newspaper=[[Ottawa Citizen]]|date=October 26, 1926}}</ref> Mulhall said of Santell:

<blockquote>That man's a great director. He can make people feel perfectly natural. He's got what [[Charlie Chaplin|Chaplin]] did when he directed ''[[A Woman of Paris]]''—he and [[Lewis Milestone]] and [[Mal St. Clair]] all have that same touch, they all belong to the new school of directors, it seems to me. They're not so busy thinking about technique that they have actors turning into marionettes.<ref name=Inez>{{cite web|last1=Sebastian|first1=Inez|title=Discovered – Jack Mulhall|url=https://archive.org/stream/pictureplaymagaz25unse#page/n199/mode/2up|website=Picture-Play Magazine|publisher=[[Internet Archive]]|accessdate=June 26, 2014}}</ref></blockquote>

Chosen to play female lead Sadie Hermann, [[Dorothy Mackaill]] opined that the film would appeal to "every girl in America". She believed that "there is not a situation in this picture which could not happen to any girl. That is one of the things I like about it. There is nothing in it that could not be true."<ref>{{cite news|title='"Subway Sadie" A Film Story of Every Girl,' Says Star.|newspaper=[[Lockport Union-Sun & Journal]]|date=January 18, 1927|page=2}}</ref> The rest of the cast includes [[Charles Murray (American actor)|Charles Murray]] as a driver, Peggy Shaw as Ethel, [[Gaston Glass]] as Fred Perry, and Bernard Randall portraying Brown.<ref name=se>{{cite web|title=Subway Sadie|url=http://www.silentera.com/PSFL/data/S/SubwaySadie1926.html|work=Silent Era|accessdate=January 12, 2014}}</ref>

[[Arthur Edeson]] served as the cinematographer for ''Subway Sadie'', shooting the [[silent film]] in [[black-and-white]].<ref name=se/> Filming took place at several locations in [[Central Park]], including Cleopatra's Needle<ref>{{harvnb|Koszarski|2008|p=129}}</ref> and a local [[casino]] [[inn]], which marked the first time the location had been filmed.<ref>{{cite news|title=Attractions at Showhouse Offer Variety|url=https://news.google.com/newspapers?nid=2206&dat=19261126&id=UkAuAAAAIBAJ&sjid=WNgFAAAAIBAJ&pg=6565,71316|accessdate=January 12, 2014|newspaper=[[The Miami News]]|date=November 26, 1926}}</ref> A nightclub scene was also shot in New York.<ref>{{cite news|title=Plaza|url=https://news.google.com/newspapers?nid=888&dat=19260913&id=KxBPAAAAIBAJ&sjid=oUwDAAAAIBAJ&pg=5460,7294304|accessdate=January 12, 2014|newspaper=[[St. Petersburg Times]]|date=September 13, 1926}}</ref> Hugh Bennett served as the film's editor, while Al Rockett Productions produced.<ref name=se/> Rockett told ''[[Motion Picture News]]'' in June 1926 that the film had been completed.<ref>{{cite web|title=Pictures and People|url=https://archive.org/stream/motionpicture33moti#page/n1063/mode/2up|website=[[Motion Picture News]]|publisher=[[Internet Archive]]}}</ref> [[First National Pictures]] filed a copyright for the film on August 18, 1926.<ref name=afi/> The finished product was seven [[film reel|reel]]s long, and comprised {{Convert|6727|ft}} of film,<ref name=se/> running for about 70 minutes.<ref>{{cite web|authorlink=Hal Erickson (author)|last=Erickson|first=Hal|title=Subway Sadie|url=https://www.nytimes.com/movies/movie/112151/Subway-Sadie/overview|work=[[The New York Times]]|accessdate=January 12, 2014}}</ref>

==Release and reception==
[[File:Subway Sadie film still Picture-Play jpg version.jpg|thumb|left|A scene from the film, featuring Mulhall and Mackaill. Their successful pairing in ''Subway Sadie'' led to them appearing in several other films.]]
First National Pictures handled distribution for ''Subway Sadie'', with the film premiering in New York on September 12, 1926.<ref name=afi/> It received positive reviews; a journalist for ''[[The New York Times]]'' enjoyed the film, calling it "an amusing photoplay". Although the review branded the ending unsurprising, they described it as "nevertheless pleasing".<ref>{{cite news|title=A Subway Guard's Romance|url=https://www.nytimes.com/movie/review?res=9900E2D9163CEE3ABC4B52DFBF66838D639EDE|accessdate=January 12, 2014|newspaper=[[The New York Times]]|date=September 13, 1926}}</ref> ''[[The Evening Independent]]'' praised the film, lauding it as "one of the cleverest and most interesting pictures that has been here this season".<ref>{{cite news|title=Herbert Clifton is Star on Keith Bill This Week|url=https://news.google.com/newspapers?nid=950&dat=19261210&id=re9PAAAAIBAJ&sjid=t1QDAAAAIBAJ&pg=3410,1909719|accessdate=January 12, 2014|newspaper=[[Evening Independent]]|date=December 10, 1926}}</ref>

A review from ''[[Photoplay]]'' applauded Mackaill's performance and described the film as "a true and human story".<ref>{{cite web|title=Photoplay|url=https://archive.org/stream/photoplay3133movi/photoplay3133movi_djvu.txt|work=[[Photoplay]]|publisher=[[Internet Archive]]|accessdate=January 12, 2014}}</ref> The review in the ''[[Motion Picture Herald]]'' assessed it as "a nice little feature, nothing big, but will go over on bargain nights", with praise directed to Mulhall's performance.<ref>{{cite web|title=Exhibitors Herald and Moving Picture World|url=https://archive.org/stream/exhibitorsherald91unse/exhibitorsherald91unse_djvu.txt|work=[[Motion Picture Herald|Moving Picture Herald]]|publisher=[[Internet Archive]]|accessdate=January 12, 2014}}</ref> A ''Berkeley Daily Gazette'' review wrote of the film by saying "sheer brilliance rarely has been equalled" and praised the story, direction, and acting.<ref>{{cite news|title=Subway Sadie To be Exhibited Here|url=https://news.google.com/newspapers?nid=1970&dat=19261211&id=LDEiAAAAIBAJ&sjid=pKYFAAAAIBAJ&pg=3055,6060445|accessdate=January 12, 2014|newspaper=Berkeley Daily Gazette|date=December 11, 1926}}</ref>

''[[The Morning Telegraph]]''{{'}}s review said that ''Subway Sadie'' would "delight the majority of [[straphanger]]s" and that "it is what the boys call excellent box-office". The ''[[New York American]]'' review was similarly positive, describing it as "a light but charming comedy". In the ''[[New York World]]'', the review described the film as "a consistently decent affair" which featured good direction by Santell.<ref name=reviews>{{cite web|title=Subway Sadie advert|url=https://archive.org/stream/filmdaily3738newy#page/728/mode/2up|work=[[The Film Daily]]|publisher=[[Internet Archive]]|accessdate=January 12, 2014}}</ref> A review in ''[[The Daily Mirror]]'' wrote positively of Mackaill's performance and complimented Santell's directing abilities,<ref name=reviews/> while ''[[Reading Eagle]]'' praised the performances of the leads, calling them "a stellar combination".<ref>{{cite news|title=Dorothy Mackaill in Subway Sadie|url=https://news.google.com/newspapers?nid=1955&dat=19261024&id=icMhAAAAIBAJ&sjid=aZ0FAAAAIBAJ&pg=3278,5112765|accessdate=January 12, 2014|newspaper=[[Reading Eagle]]|date=October 24, 1926}}</ref> Not all reviews were positive; a negative review came from ''[[The Educational Screen]]'', whose reviewer found the film to be "pretty trite stuff".<ref>{{cite web|title=Subway Sadie|url=https://archive.org/stream/educationalscree05chicrich#page/610/mode/2up|work=The Educational Screen|publisher=[[Internet Archive]]|accessdate=January 12, 2014}}</ref>

In June 1927, a ''[[Southeast Missourian]]'' journalist wrote that the film had since become "very successful".<ref>{{cite news|title=Ball Game Played by Artificial Light Is Dazzling Success|url=https://news.google.com/newspapers?nid=1893&dat=19270625&id=5IQoAAAAIBAJ&sjid=DtQEAAAAIBAJ&pg=4004,2023301|accessdate=January 12, 2014|newspaper=[[The Southeast Missourian]]|date=June 25, 1927}}</ref> The pairing of Mulhall and Mackaill was described as "such a perfect team" that plans to have them star in many further films occurred.<ref name=Inez/> Films they appeared in after ''Subway Sadie'' include ''[[Smile, Brother, Smile]]'' (1927),<ref>{{cite web|title=Smile, Brother, Smile|url=http://www.tcm.com/tcmdb/title/499516/Smile-Brother-Smile/|website=[[Turner Classic Movies]]|accessdate=June 26, 2014}}</ref> ''[[Ladies' Night in a Turkish Bath]]'' (1928),<ref>{{cite web|last1=Erickson|first1=Hal|title=Ladies' Night in a Turkish Bath|url=http://www.allmovie.com/movie/ladies-night-in-a-turkish-bath-v98444|website=[[Allmovie]]}}</ref> ''[[Lady Be Good (1928 film)|Lady Be Good]]'' (1928),<ref>{{cite web|title=Lady Be Good|url=http://www.afi.com/members/catalog/DetailView.aspx?s=1&Movie=10127|website=[[American Film Institute]]|accessdate=June 26, 2014}}</ref> and ''[[Children of the Ritz]]'' (1929).<ref>{{cite web|last1=Erickson|first1=Hal|title=Children of the Ritz|url=http://www.allmovie.com/movie/children-of-the-ritz-v87181|website=[[Allmovie]]|accessdate=June 26, 2014}}</ref> The 1933 drama ''[[Curtain at Eight]]'' marked the final film they appeared in together.{{sfn|Pitts|1997|p=224}}

Screenings of ''Subway Sadie'' occurred as late as January 12, 1928.<ref>{{cite news|title=Euclid|url=https://news.google.com/newspapers?nid=888&dat=19280112&id=Hl1PAAAAIBAJ&sjid=ck4DAAAAIBAJ&pg=5959,2567832|accessdate=January 12, 2014|newspaper=[[The St. Petersburg Times]]|date=January 12, 1928}}</ref> As of November 2007, it is unclear whether a print of the film exists; it has likely become a [[lost film]].<ref name=se/> A poster of the film can be seen at the [[New York Transit Museum]].<ref>{{cite web|last1=Plitt|first1=Amy|title=Highlights of the New York Transit Museum in New York City|url=http://www.timeout.com/newyork/museums/ten-must-sees-at-the-new-york-transit-museum-cultural-center?pageNumber=2|website=[[Time Out (magazine)|Time Out]]|accessdate=June 26, 2014}}</ref>

==References==
{{Reflist|30em}}

===Bibliography===
{{refbegin}}
*{{cite book|ref=harv|last=Koszarski|first=Richard|title=Hollywood On the Hudson: Film and Television in New York from Griffith to Sarnoff|url=https://books.google.com/books?id=EF2xmI5pEx8C&pg=PA129|date=August 27, 2008|publisher=Rutgers University Press|isbn=978-0-8135-4552-3}}
*{{cite book|ref=harv|last=Pitts|first=Michael R.|title=Poverty Row Studios, 1929–1940: An Illustrated History of 53 Independent Film Companies, with a Filmography for Each|url=https://books.google.com/books?id=hGpZAAAAMAAJ|year=1997|publisher=McFarland & Co.|isbn=978-0-7864-0168-0}}
{{refend}}

==External links==
{{commons category}}
* {{IMDb title|tt0017439}}
* {{tcmdb title|501203|Subway Sadie}}

{{Portal bar|Film|New York City}}

[[Category:1926 films]]
[[Category:American silent feature films]]
[[Category:American films]]
[[Category:American black-and-white films]]
[[Category:Films based on short fiction]]
[[Category:Films directed by Alfred Santell]]
[[Category:Films set in New York City]]
[[Category:Films shot in New York City]]
[[Category:Lost films]]
[[Category:First National Pictures films]]
[[Category:Works set on the New York City Subway]]