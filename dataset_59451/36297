{{Use dmy dates|date=July 2015}}
{{Use British English|date=July 2015}}
{{Infobox church
| name                   = Our Lady Star of the Sea and St Winefride, Amlwch
| image                  = Catholic Church Amlwch.jpg
| imagealt               =
| landscape              = no
| caption                = Our Lady Star of the Sea and St Winefride
| pushpin map            = Wales Anglesey
| pushpin map alt        =
| map caption            = Location in [[Anglesey]]
| latd                   = 53
| latm                   = 24
| lats                   = 42
| latNS                  = N
| longd                  = 4
| longm                  = 21
| longs                  = 05
| longEW                 = W
| coordinates            = {{Coord|53.411596|-4.351442|region:GB|display=title}}
| osgridref              = {{gbmappingsmall|SH 438 931}}
| location               = [[Amlwch]], Anglesey
| country                = [[Wales]], United Kingdom
| denomination           = [[Roman Catholic]]
| website                =
| founded date           = 1932 (construction started)
| consecrated date       = 1937
| founder                =
| dedication             = [[St Mary]] ([[Our Lady, Star of the Sea]]) and [[Winefride|St Winefride]]
| status                 = [[Parish church]]
| architectural type      = [[Church (building)|Church]]
| functional status      = Active
| heritage designation   = [[listed building|Grade II*]]
| designated date        = 12 December 2000
| style                  =
<!-- | length                 = {{convert|}}
| width                  = {{convert|}}
| width nave             = {{convert|}}
| height                 = {{convert|}}
| diameter               = {{convert|}}
| other dimensions       = 
| floor area             = {{convert|}}
 -->
| materials              = [[Reinforced concrete]]
| architect              = [[Giuseppe Rinvolucri]]
| parish                 = Amlwch
| deanery                = [[Caernarfon Deanery]]
| archdeaconry           =
| diocese                = [[Diocese of Wrexham]]
| archdiocese            = [[Archdiocese of Cardiff]]
| priest                 = The Reverend Father Michael Ryan<ref name=Wrex>{{cite web|url=http://www.wrexhamdiocese.org.uk/diocesan_parish_display.asp?parishid=7|publisher=[[Diocese of Wrexham]]|title=The Parish of Our Lady Star Of The Sea And St Winefride, Amlwch in the county of Anglesey|accessdate=19 April 2012}}</ref>
}}
'''Our Lady Star of the Sea and St Winefride, Amlwch''' is a [[Roman Catholic]] church in [[Amlwch]], a town on the island of [[Anglesey]], north Wales. It was built in the 1930s to a design by an Italian architect, [[Giuseppe Rinvolucri]], using [[reinforced concrete]]. The church is in the shape of an upturned boat, reflecting Amlwch's maritime heritage, and is dedicated to [[Our Lady, Star of the Sea]] (a title of [[St Mary]]) and [[Winefride|St Winefride]], a Welsh saint.

The church is a [[listed building|Grade II* listed building]], a designation given to "particularly important buildings of more than special interest",<ref name=Cadwlist>{{cite book|url=http://cadw.wales.gov.uk/docs/cadw/publications/What_Is_Listing_EN.pdf|format=PDF|title=What is listing?|publisher=[[Cadw]]|year=2005|isbn=1-85760-222-6 |page=6}}</ref> because it is a "remarkable inter-war church", built to "a highly unusual and experimental design".<ref name=List>{{cite web|url=http://jura.rcahms.gov.uk/cadw/cadw_eng.php?id=24455|title=Church of Our Lady Star of the Sea, St Winifred <!-- wrong spelling on the Cadw list -->|author=[[Cadw]]|publisher=Historic Wales|year=2009|accessdate=22 June 2010}} (The church's name is spelt incorrectly on the Cadw document.)</ref> The [[Twentieth Century Society]] has called it "a rare and unique church",<ref name=TCS/> and it has also been called "one of Britain's most avant-garde churches".<ref name=Telegraph/>

==Location and history==
The church is on the [[A5025 road]], about {{convert|0.5|mi|km}} to the west of Amlwch, a town on the north coast of [[Anglesey]], Wales.<ref name=Anglesey>{{cite book|last=Jones|first=Geraint I. L. |year=2006|title=Anglesey Churches|publisher=Carreg Gwalch|isbn=1-84527-089-4|page=31}}</ref> It is dedicated to [[St Mary]] (under the title [[Our Lady, Star of the Sea]]) and to [[Winefride|St Winefride]], a 7th-century Welsh noblewoman who is also venerated at [[St Winefride's Well]], [[Flintshire]]. Construction of the church began in 1932, when the foundations were excavated. It was completed in 1937, and the church was consecrated in the same year.<ref name=Anglesey/> The architect was [[Giuseppe Rinvolucri]], an Italian engineer from [[Piedmont]], who settled in [[Conwy]], north Wales, because his English wife was suffering from [[tuberculosis]].  His specialist field was the design of [[Roman Catholic]] churches, and other – more conventional – examples of his work can be found in [[Abergele]] and [[Porthmadog]], also in north Wales.<ref name=List/><ref name=Pev/><ref name=Wooding>{{cite book|title=A guide to the churches and chapels of Wales|publisher=[[University of Wales Press]]|year=2011|isbn=978-0-7083-2118-8|pages=76–77|editor1-first=Jonathan |editor1-last=Wooding |editor2-first= Nigel|editor2-last= Yates|editor2-link=Nigel Yates}}</ref>

The church is part of [[Caernarfon Deanery]] within the [[Diocese of Wrexham]].<ref>{{cite web|url=http://www.wrexhamdiocese.org.uk/ourstandard.asp?pageid=145|title=Wrexham Diocese: Deaneries|publisher=Diocese of Wrexham|accessdate=26 June 2010}}</ref> As of 2012, the parish priest is Father Michael Ryan, of the [[Missionary Oblates of Mary Immaculate]]. The parish of Amlwch also includes the churches of St David, [[Cemaes Bay]], and Our Lady of Lourdes, [[Benllech]].<ref name=Wrex/>

Damage from the weather and deterioration in the concrete meant that the church closed for worship in 2004, requiring worshippers to attend services elsewhere on Anglesey.<ref name=Mail/><ref name=OMI/><ref name=Jenkins/> Demolition was a possibility in 2006.<ref name=Telegraph>{{cite news|url=http://www.telegraph.co.uk/news/uknews/2022075/Save-Our-Churches-lost-without-Our-Lady.html|work=[[The Daily Telegraph]]|title=Save Our Churches: lost without 'Our Lady' |last=Leach|first=Ben|date=24 May 2008|accessdate=25 June 2010}}</ref> An appeal launched raised the estimated £1.2&nbsp;million to £1.4&nbsp;million necessary for repairs, which included replacing the roof coverings, redecorating internally and externally, and repairing the steps.<ref name=Mail>{{cite news|url=http://www.theonlinemail.co.uk/bangor-and-anglesey-news/local-bangor-and-anglesey-news/2010/06/16/amlwch-church-gets-funding-boost-for-renovation-project-66580-26654346/|title=Amlwch church gets funding boost for renovation project|work=[[Bangor and Anglesey Mail]]|last=Hearn|first=Elgan|date=16 June 2010|accessdate=25 June 2010}}</ref> An application for planning permission for the work was submitted in May 2008 to the [[Isle of Anglesey County Council]].<ref>{{cite web|url=http://www.ynysmon.gov.uk/upload/public/attachments/84/12MAY20081.pdf|title=List of Applications Received in the Week 12/5/08 – 18/5/08|format=PDF|publisher=Isle of Anglesey County Council|accessdate=26 June 2010|page=4}}</ref> An application for a grant of £840,000 from the [[Heritage Lottery Fund]] was rejected on 18 March 2009, with the committee concerned about the proposed new extension (terming it "inappropriate"), although recognising the "high heritage merit" of the proposal.<ref>{{cite web|url=http://legacy.hlf.org.uk/NR/rdonlyres/D158817C-76DF-4918-9B8D-AC8CDA86C18C/7596/SummaryReportWalesMarch20091.pdf|format=PDF|title=Heritage Lottery Fund Committee for Wales Meeting on 18 March 2009|publisher=Heritage Lottery Fund|accessdate=26 June 2010|page=3}}</ref> [[Cadw]] (the [[Welsh Assembly Government]] body responsible for the built heritage of Wales) made a grant of £150,000 in 2007; the [[National Churches Trust]] made a grant of £10,000 in June 2010; and other bodies and individuals made donations to the appeal.<ref name=Mail/>  The church reopened after its restoration on 1 May 2011 with a Mass celebrated by the [[Bishop of Wrexham]], [[Edwin Regan]].<ref name=Post>{{cite news|url=http://www.dailypost.co.uk/news/local-north-wales-news/wrexham-news/2011/05/02/doors-open-at-house-of-god-55578-28617770/|title=Doors open at house of God|work=[[Daily Post (North Wales)]]|last=Powell|first=David|date=2 May 2011|accessdate=7 June 2011}}</ref>

==Architecture and fittings==
Built of [[reinforced concrete]], the building is designed in the style of an upturned boat, and the design has a "nautical theme" with elements such as porthole windows.<ref name=List/><ref name=Anglesey/> This is a deliberate reference to Amlwch's history as a port town and its position on the coast.<ref name=Telegraph/><ref name=OMI>{{cite web|url=http://www.omianglesey.org/ourstandard.asp?pageid=80|title=Our Lady Star Of The Sea And St. Winefride RC Church|publisher=Missionary Oblates of Mary Immaculate, Isle of Anglesey|accessdate=25 June 2010}}</ref> The church has six concrete [[parabolic arch]] "ribs" along the outside, with portholes on the base plinth between each rib.<ref name=List/><ref name=Wooding/>

The main entrance is at the south end of the church at the top of some stone steps on either side. A window (shaped, like all the others in the church, like a star) set in mosaic is positioned above the door, and there is a stone cross at the top of the facade. The concrete of the church is dressed with stone on the south side, nearest the main road. The ribs on the outside are visible inside the building; in between them, there are patterns of lights and coloured marble panels on the lower parts of the interior walls. The vestry is to the rear of the church, and there is a parish hall, built from masonry, underneath the church.<ref name=List/><ref name=Anglesey/><ref name=Wooding/> The altar was replaced in 1995 and again on the reopening of the church in 2011, when a carved crucifix, which was brought to Amlwch from a former convent in Liverpool, was also dedicated.<ref name=Wooding/><ref name=Post/> The porch houses a sepulchral slab, dating from the latter half of the 13th century.<ref name=Wooding/>

==Assessment==
The church is a Grade II* [[listed building]] – the second-highest of the three grades of listing, designating "particularly important buildings of more than special interest".<ref name=Cadwlist/> It was given this status on 12 December 2000, and has been listed as "a remarkable inter-war church".<ref name=List/> Cadw describes it as "striking and individual", and "a highly unusual and experimental design which exploits the plastic qualities of its constructional material to create a powerfully expressive religious building."<ref name=List/>

A 2006 guide to the churches of Anglesey describes it as "a very impressive building", that "must surely be the most unusual church in Anglesey."<ref name=Anglesey/> A 2009 guide to the buildings of north Wales describes it as "a piece of Italian architectural daring".<ref name=Pev>{{cite book|title=The Buildings of Wales: Gwynedd|chapter=Anglesey: Amlwch|publisher=Yale University Press|isbn=978-0-300-14169-6|year=2009|page=93|last=Haslam|first=Richard|last2=Orbach|first2=Julian|last3=Voelcker|first3=Adam}}</ref>  Referring to the French structural engineer [[Eugène Freyssinet]], who worked with concrete, the guide rhetorically asks, "What inspired this Futurist church, closer to Freyssinet's 1920s [[airship hangar]]s at Orly, Paris, than to Catholic church design, and so unlike the conservatism of Anglesey building?"<ref name=Pev/> Writing before the church reopened, the heritage writer and journalist [[Simon Jenkins]] has said that the church was worth a visit for the exterior alone, even though it was closed. He noted the "sweeping parabolic arches, perhaps inspired by airship hangars or by upturned boats in Amlwch harbour", as well as the "bold gable with sloping sides" at the west end, concluding "This church must be saved."<ref name=Jenkins>{{cite book|title=Wales: Churches, Houses, Castles|year=2008|isbn=978-0713998931|publisher=Penguin Books|page=48|last=Jenkins|first=Simon|authorlink=Simon Jenkins}}</ref> It has also been described as "one of Britain's most avant-garde churches".<ref name=Telegraph/> The [[Twentieth Century Society]] has said that it is "by far [Rinvolucri's] best work", calling it "a rare and unique church".<ref name=TCS>{{cite web|url=http://www.c20society.org.uk/casework/reports/2005/the-italian-touch.html|title=The Italian Touch|first=Eva |last=Branscome|publisher=[[Twentieth Century Society]]|date=September 2005|accessdate=25 June 2010}}</ref> It noted the "highly individual interpretation of its seaside setting", with a "strikingly modern" parabolic design and a "monumental almost pyramidal aesthetic" at the entrance.<ref name=TCS/> A 2011 guide to religious buildings in Wales (written before the church reopened) described it as "most unusual", but added that the fittings were "not worthy of the building".  It also noted one writer's words that "no Catholic church (nor any church of another denomination) built in Britain between the wars has the frankly radical character of Amlwch".<ref name=Wooding/>

==References==
{{Reflist|colwidth=30em}}

==External links==
{{Commons category|Church of Our Lady Star of the Sea and St Winefride, Amlwch}}
* [http://www.photosofchurches.com/anglesey-amlwch.htm Photographs of the church]
*[http://imagingthebible.llgc.org.uk//site/338 Artwork at Our Lady Star of the Sea and St Winefride, Amlwch]
{{Anglesey churches}}
{{Good article}}

{{DEFAULTSORT:Amlwch, Our Lady Star of the Sea and Saint Winefride}}
[[Category:Grade II* listed churches in Anglesey]]
[[Category:Roman Catholic churches completed in 1937]]
[[Category:Roman Catholic churches in Wales|Amlwch, Our Lady]]
[[Category:Amlwch]]