{{about|mechanical resonance of sound including musical instruments| a general description of  mechanical resonance in [[physics]] and [[engineering]]|mechanical resonance| a general description of resonance| resonance}}
[[File:23. Звучни вилушки.ogv|thumb|right|300px|<small>Experiment using two [[tuning fork]]s [[acoustic oscillations|oscillating]] at the same [[frequency]]. One of the forks is being hit with a rubberized mallet. Although the first tuning fork hasn't been hit, while the other fork is visibly excited due to the oscillation caused by the periodic change in the pressure and density of the air by hitting the other fork, creating an '''acoustic resonance''' between the forks. However, if we place a piece of metal on a prong, we see that the effect dampens, and the excitations become less and less pronounced as resonance isn't achieved as effectively.</small>]]
'''Acoustic resonance''' is a phenomenon where [[acoustics|acoustic systems]] amplify sound waves whose frequency  matches one of its own natural frequencies of vibration (its ''[[resonance]] frequencies'').

The term "acoustic resonance" is sometimes used to narrow [[mechanical resonance]] to the frequency range of human hearing, but since [[acoustics]] is defined in general terms concerning vibrational waves in matter <ref>Kinsler L.E., Frey A.R., Coppens A.B., Sanders J.V., "Fundamentals of Acoustics", 3rd Edition, ISBN 978-0-471-02933-5, Wiley, New York, 1982.</ref> acoustic resonance can occur at frequencies outside the range of human hearing.

An acoustically resonant object usually has more than one resonance frequency, especially at [[harmonic]]s of the strongest resonance.  It will easily vibrate at those frequencies, and vibrate less strongly at other frequencies.  It will "pick out" its resonance frequency from a complex excitation, such as an impulse or a wideband noise excitation.  In effect, it is filtering out all frequencies other than its resonance.

Acoustic resonance is an important consideration for instrument builders, as most acoustic [[Musical instrument|instruments]] use [[resonator]]s, such as the strings and body of a [[violin]], the length of tube in a [[flute]], and the shape of a drum membrane.  Acoustic resonance is also important for hearing.  For example, resonance of a stiff structural element, called the [[basilar membrane]] within the [[cochlea]] of the [[inner ear]] allows [[hair cells]] on the membrane to detect sound.  (For mammals the membrane has tapering resonances across its length so that high frequencies are concentrated on one end and low frequencies on the other.)

Like mechanical resonance, acoustic resonance can result in catastrophic failure of the vibrator.  The classic example of this is breaking a wine glass with sound at the precise resonant frequency of the glass; although this is difficult in practice.<ref>[http://www.physics.ucla.edu/demoweb/demomanual/acoustics/effects_of_sound/breaking_glass_with_sound.html Breaking Glass with Sound<!-- Bot generated title -->]</ref><ref>[http://www.acoustics.salford.ac.uk/acoustics_info/glass/?content=index Tutorial on how to break glass with sound]</ref>

==Vibrating string==
{{details|Vibrating string}}
[[File:Bass Guitar 110Hz TFM.png|thumb|400 px| String resonance of a bass guitar A note with fundamental frequency of 110 Hz.  Computed with from sampled sound with https://sourceforge.net/projects/amoreaccuratefouriertransform/ .]]
In musical instruments, strings under tension, as in [[lute]]s, [[harp]]s, [[guitar]]s, [[piano]]s, [[violin]]s and so forth, have [[Resonance|resonant frequencies]] directly related to the mass, length, and tension of the string.  The wavelength that will create the first resonance on the string is equal to twice the length of the string.  Higher resonances correspond to wavelengths that are integer divisions of the [[fundamental frequency|fundamental]] wavelength.  The corresponding frequencies are related to the speed ''v'' of a [[Vibrating string|wave traveling down the string]] by the equation

:<math>f = {nv \over 2L}</math>

where ''L'' is the length of the string (for a string fixed at both ends) and ''n'' = 1, 2, 3...([[Harmonic]] in an open end pipe (that is, both ends of the pipe are open)).  The speed of a wave through a string or wire is related to its tension ''T'' and the mass per unit length ρ:

:<math>v = \sqrt {T \over \rho}</math>

So the frequency is related to the properties of the string by the equation

:<math>f = {n\sqrt {T \over \rho} \over 2 L} = {n\sqrt {T \over m / L} \over 2 L}</math>

where ''T'' is the [[Tension (mechanics)|tension]], ρ is the mass per unit length, and ''m'' is the total [[mass]].

Higher tension and shorter lengths increase the resonant frequencies.  When the string is excited with an impulsive function (a finger pluck or a strike by a hammer), the string vibrates at all the frequencies present in the impulse (an impulsive function theoretically contains 'all' frequencies).  Those frequencies that are not one of the resonances are quickly filtered out&mdash;they are attenuated&mdash;and all that is left is the harmonic vibrations that we hear as a musical note.

===String resonance in music instruments===
{{main article|String resonance (music)}}
[[String resonance]] occurs on [[string instruments]]. Strings or parts of strings may resonate at their [[Fundamental frequency|fundamental]] or [[overtone]] frequencies when other strings are sounded. For example, an A string at 440&nbsp;Hz will cause an E string at 330&nbsp;Hz to resonate, because they share an overtone of 1320&nbsp;Hz (3rd overtone of A and 4th overtone of E).

==Resonance of a tube of air==<!-- This section is linked from [[Voice organ]] -->
The resonance of a tube of air is related to the length of the tube, its shape, and whether it has closed or open ends.  Musically useful tube shapes are ''conical'' and ''cylindrical'' (see [[bore (wind instruments)|bore]]).  A pipe that is closed at one end is said to be ''stopped'' while an ''open'' pipe is open at both ends.  Modern orchestral [[flute]]s behave as open cylindrical pipes; [[clarinet]]s and lip-reed instruments ([[brass instrument]]s) behave as closed cylindrical pipes; and [[saxophone]]s, [[oboe]]s, and [[bassoon]]s as closed conical pipes.<ref>{{cite web
 |title = Saxophone acoustics: an introduction
 | last = Wolfe
 | first = Joe
 |url = http://www.phys.unsw.edu.au/jw/saxacoustics.html
 |publisher=University of New South Wales
 |accessdate=1 January 2015}}</ref>
Vibrating air columns also have resonances at harmonics, like strings.

===Cylinders===
By convention a rigid cylinder that is open at both ends is referred to as an "open" cylinder, whereas a rigid cylinder that is open at one end and has a rigid surface at the other end is referred to as a "closed" cylinder.

[[File:OpenCylinderResonance.svg|thumb|right|The first three resonances in an open cylindrical tube. The horizontal axis is pressure.]]
[[File:ClosedCylinderResonance.svg|thumb|right|The first three resonances in a one end open and other end closed cylindrical tube. The horizontal axis is pressure.]]

====Open====

{| class="wikitable"
|- class="hintergrundfarbe5"
!Frequency
!Order
!Name 1
!Name 2
!Name 3
!Wave Representation
!Molecular Representation
|-
| ''1 · f '' = &nbsp;&nbsp;440&nbsp;Hz
| ''n = 1''
| 1st partial
| [[fundamental tone]]
| 1st harmonic
|[[File:Pipe001.gif|220px]]
|[[File:Molecule1.gif|220px]]
|-
| ''2 · f '' = &nbsp;&nbsp;880&nbsp;Hz
| ''n = 2''
| 2nd partial
| 1st overtone
| 2nd harmonic
|[[File:Pipe002.gif|220px]]
|[[File:Molecule2.gif|220px]]
|-
| ''3 · f ''= 1320&nbsp;Hz
| ''n = 3''
| 3rd partial
| 2nd overtone
| 3rd harmonic
|[[File:Pipe003.gif|220px]]
|[[File:Molecule3.gif|220px]]
|-
| ''4 · f ''= 1760&nbsp;Hz
| ''n = 4''
| 4th partial
| 3rd overtone
| 4th harmonic
|[[File:Pipe004.gif|220px]]
|[[File:Molecule4.gif|220px]]
|}

An open tube is a tube in which both ends are open. The tube resonates at many frequencies or notes. Its lowest resonance (called its fundamental frequency) occurs at the same frequency as a closed tube of half its length. An open tube will resonate if there is a displacement [[antinode]] at each open end. These displacement antinodes are places where there is a maximum movement of air in and out of the ends of the tube. Displacement antinodes are also pressure nodes. By [[overblowing]] an open tube, a note can be obtained that is an octave above the fundamental frequency or note of the tube. For example, if the fundamental note of an open pipe is C1, then overblowing the pipe gives C2, which is an octave above C1.<ref name="Jaap">Kool, Jaap. ''Das Saxophon''. J. J. Weber, Leipzig. 1931. Translated by [[Lawrence Gwozdz]] in 1987, discusses "open" and "closed" tubes.</ref>

Open cylindrical tubes resonate at the approximate frequencies:

:<math>f = {nv \over 2L}</math>

where ''n'' is a positive integer (1, 2, 3...) representing the resonance node, ''L'' is the length of the tube and ''v'' is the [[speed of sound]] in air (which is approximately {{convert|343|m/s|mph|disp=sqbr}} at {{cvt|20|°C|disp=sqbr}} and at sea level).

A more accurate equation considering an [[end correction]] is given below:

:<math>f = {nv \over 2(L+0.8d)}</math>

where d is the diameter of the resonance tube. This equation compensates for the fact that the exact point at which a sound wave is reflecting at an open end is not perfectly at the end section of the tube, but a small distance outside the tube.

The reflection ratio is slightly less than 1; the open end does not behave like an infinitesimal [[acoustic impedance]]; rather, it has a finite value, called radiation impedance, which is dependent on the diameter of the tube, the wavelength, and the type of reflection board possibly present around the opening of the tube.

So when n is 1:

:<math>f = {v \over 2(L+0.8d)} </math>
:<math> {f(2(L+0.8d))} = v </math>
:<math> {f\lambda} = v </math>
:<math> \lambda = {2(L+0.8d)}</math>

where v is the speed of sound, L is the length of the resonant tube, d is the diameter of the tube, f is the resonate sound frequency, and λ is the resonant wavelength.

====Closed====

A closed tube (also called a "stopped pipe" in the [[Organ (music)|organ]]) is a tube which is closed at one end. The tube has a fundamental frequency but can be overblown to produce other higher frequencies or notes. These overblown registers can be tuned by using different degrees of conical taper. A closed tube resonates at the same fundamental frequency as an open tube twice its length, with a wavelength equal to four times its length. In a closed tube, a displacement [[Node (physics)|node]], or point of no vibration, always appears at the closed end and if the tube is resonating, it will have an [[antinode]], or point greatest vibration at the [[Phi]] point (length × 0.618) near the open end.

By [[overblowing]] a cylindrical closed tube, a note can be obtained that is approximately a twelfth above the fundamental note of the tube. This is sometimes described as one-fifth above the [[octave]] of the fundamental note. For example, if the fundamental note of a closed pipe is C1, then overblowing the pipe gives G2, which is one-twelfth above C1. Alternatively we can say that G2 is one-fifth above C2 — the octave above C1. Adjusting the taper of this cylinder for a decreasing cone can tune the second harmonic or overblown note close to the octave position or 8th. (ref: Horns,Strings and Harmony, by Arthur H. Benade.) Opening a small "speaker hole" at the [[Phi]] point, or shared "wave/node" position will cancel the fundamental frequency and force the tube to resonate at a 12th above the fundamental. This technique is used in a [[Recorder (musical instrument)|Recorder]] by pinching open the dorsal thumb hole. Moving this small hole upwards, closer to the voicing will make it an "Echo Hole" (Dolmetsch Recorder Modification) that will give a precise half note above the fundamental when opened. Note: Slight size or diameter adjustment is needed to zero in on the precise half note frequency.<ref name="Jaap"/>

A closed tube will have approximate resonances of:

:<math>f = {nv \over 4L}</math>

where "n" here is an odd number (1, 3, 5...). This type of tube produces only odd harmonics and has its fundamental frequency an octave lower than that of an open cylinder (that is, half the frequency).

A more accurate equation is given below:

:<math>f = {nv \over 4(L+0.4d)}</math>.

Again, when n is 1:

:<math>f = {v \over 4(L+0.4d)} </math>
:<math> {f(4(L+0.4d))} = v </math>
:<math> {f\lambda} = v </math>
:<math> \lambda = {4(L+0.4d)}</math>

where v is the speed of sound, L is the length of the resonant tube, d is the diameter of the tube, f is the resonate sound frequency, and λ is the resonant wavelength.

===Cones===
An open conical tube, that is, one in the shape of a [[frustum]] of a cone with both ends open, will have resonant frequencies approximately equal to those of an open cylindrical pipe of the same length.

The resonant frequencies of a stopped conical tube &mdash; a complete cone or frustum with one end closed &mdash; satisfy a more complicated condition:

:<math>kL = n\pi - \tan^{-1} kx</math>

where the [[wavenumber]] k is

:<math>k = 2\pi f/v</math>

and ''x'' is the distance from the small end of the frustum to the vertex.  When ''x'' is small, that is, when the cone is nearly complete, this becomes

:<math>k(L+x) \approx n\pi</math>

leading to resonant frequencies approximately equal to those of an open cylinder whose length equals ''L''&nbsp;+&nbsp;''x''.  In words, a complete conical pipe behaves approximately like an open cylindrical pipe of the same length, and to first order the behavior does not change if the complete cone is replaced by a closed frustum of that cone.

===Closed rectangular box===
Sound waves in a rectangular box include such examples as [[loudspeaker enclosure]]s and buildings. Rectangular building have resonances described as [[room modes]]. For a rectangular box, the resonant frequencies are given by<ref name="Kuttruff">{{cite book
  | title = Acoustics: An Introduction
  | isbn = 978-0-203-97089-8
  | year = 2007
  | last = Kuttruff
  | first = Heinrich
  | publisher=Taylor & Francis
  | page = 170
  | url=http://books.google.ca/books/about/Acoustics.html?id=ij9iDkSkpCkC&pg=PA170}}</ref>
:<math>f = {v \over 2} \sqrt{\left({\ell \over L_x}\right)^2 + \left({m \over L_y}\right)^2 + \left({n \over L_z}\right)^2}</math>

where ''v'' is the speed of sound, ''L<sub>x</sub>'' and ''L<sub>y</sub>'' and ''L<sub>z</sub>'' are the dimensions of the box. <math>\ell</math>, <math>m</math>, and <math>n</math> are nonnegative integers that cannot all be zero.
If the small loudspeaker box is airtight the frequency low enough and the compression 
is high enough, the sound pressure (decibel level) inside the box will be the same anywhere inside the box, this is hydraulic pressure.

==Sphere of air (vented)==

The resonant frequency of an rigid cavity of static volume ''V<sub>0</sub> ''  with a necked sound hole of area ''A'' and length ''L'' is given by the [[Helmholtz resonance]] formula<ref>{{cite web
 |title = Helmholtz Resonance
 | last = Wolfe
 | first = Joe
 |url = http://newt.phys.unsw.edu.au/jw/Helmholtz.html
 |publisher=University of New South Wales
 |accessdate=1 January 2015}}</ref>
:<math>f = \frac{v}{2\pi}\sqrt{\frac{A}{V_0 L_{eq}}}</math>
where <math>L_{eq}</math> is the equivalent length of the neck with [[end correction]]
:<math>L_{eq}= L+0.75d</math> {{nb10}} for an unflanged neck<ref name="Raichel2006">{{cite book
  | title = The Science and Applications of Acoustics
  | isbn = 978-0387-26062-4
  | year = 2006
  | last = Raichel
  | first = Daniel R.
  | publisher=Springer
  | pages = 145–149
 }}</ref>
:<math>L_{eq}= L+0.85d</math> {{nb10}} for a flanged neck
[[File:Sphere with a neck.gif|right|150px]]

For a spherical cavity, the resonant frequency formula becomes
:<math>f = \frac{vd}{\pi}\sqrt{\frac{3}{8L_{eq}D^3} }</math>
where
::D = diameter of sphere
::d = diameter of sound hole
[[File:Sphere with sound hole.gif|right|150px]]

For a sphere with just a sound hole, ''L''=0 and the surface of the sphere acts as a flange, so
:<math>f = \frac{v}{\pi}\sqrt{\frac{3d}{8(0.85)D^3} }</math>
In dry air at 20&nbsp;°C,  with ''d'' and ''D'' in metres, ''f'' in [[Hertz]],  this becomes
:<math>f = 72.6\sqrt{\frac{d}{D^3} }</math>

== False tones ==
Some large conical instruments like [[Tuba#Resonance and false tones|tubas]] have a strong and useful resonance that is not in the well-known harmonic series. For example, most large B{{music|flat}} tubas have a strong resonance at low E{{music|flat}} (E{{music|flat}}1, 39&nbsp;Hz), which is between the fundamental and the second harmonic (an octave higher than the fundamental). These alternative resonances are often known as [[Tuba#Resonance and false tones|false tones]] or privileged tones.

The most convincing explanation for false-tones is that the horn is acting as a 'third of a pipe' rather than as a half-pipe. The bell remains an anti-node, but there would then be a node 1/3 of the way back to the mouthpiece. If so, it seems that the fundamental would be missing entirely, and would only be inferred from the overtones. However, the node and the anti-node collide in the same spot and cancel out the fundamental.

== In musical composition ==

Several composers have begun to make resonance the subject of compositions. [[Alvin Lucier]] has used acoustic instruments and sine wave generators to explore the resonance of objects large and small in many of his compositions.  The complex [[inharmonic]] [[Harmonic series (music)#Partial|partial]]s of a swell shaped [[crescendo]] and decrescendo on a [[tamtam]] or other percussion instrument interact with room resonances in [[James Tenney]]'s ''Koan: Having Never Written A Note For Percussion''.  [[Pauline Oliveros]] and [[Stuart Dempster]] regularly perform in large [[reverberation|reverberant]] spaces such as the {{convert|2|e6USgal|m3|adj=on}} cistern at Fort Worden, WA, which has a [[reverberation|reverb]] with a 45-second decay. Malmö Academy of Music composition professor and composer Kent Olofsson's "''Terpsichord'', a piece for percussion and pre-recorded sounds, [uses] the resonances from the acoustic instruments [to] form sonic bridges to the pre-recorded electronic sounds, that, in turn, prolong the resonances, re-shaping them into new sonic gestures."
<ref>{{cite journal |last=Olofsson |first=Kent |last2= |first2= |date=4 February 2015 |title=Resonances and Responses |url= |journal=Divergence Press |publisher=University of Haddersfield Press |volume= |issue=4 |pages= |doi= |access-date=}}</ref>

== See also ==
* [[Harmony]]
* [[Music theory]]
* [[Resonance]]
* [[Reverberation]]
* [[Sympathetic string]]
* [[Reflection phase change]]

==External links==
* [http://www.physics.smu.edu/~olness/www/05fall1320/applet/pipe-waves.html Standing Waves Applet]

== References ==
{{Reflist}}
* Nederveen, Cornelis Johannes, ''Acoustical aspects of woodwind instruments''. Amsterdam, Frits Knuf, 1969.
* Rossing, Thomas D., and Fletcher, Neville H., ''Principles of Vibration and Sound''. New York, Springer-Verlag, 1995.

{{Acoustics}}

{{DEFAULTSORT:Acoustic Resonance}}
[[Category:Acoustics]]
[[Category:Musical instruments]]