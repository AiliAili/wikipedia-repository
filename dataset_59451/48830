{{Infobox person
| name        = Leonard Jackson
| image       = 
| alt         = 
| caption     = 
| birth_date  = February 7, 1928
| birth_place = [[Jacksonville, Florida]], U.S. 
| education   = [[Fisk University]]
| death_date  = {{Death date and age|2013|12|22|1928|2|7}} 
| death_place = [[Manhattan]], [[New York (state)|New York]], U.S. 
| death_cause = [[Alzheimer's disease]]
| resting_place = [[Calverton National Cemetery]]
| resting_place_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nationality = 
| other_names = L. Errol Jaye
| known_for   = 
| occupation  = Actor
| years_active = 1965-1997
}}

'''Leonard Jackson''' (February 7, 1928 &ndash; December 22, 2013) was an [[African American]] stage, film, and television actor, perhaps most widely known for his roles in several PBS television series for children as well as his roles in films such as ''[[The Brother from Another Planet]]'', ''[[Car Wash (film)|Car Wash]]'', and ''[[The Color Purple (film)|The Color Purple]]''.

==Early years and stage career==
Jackson, in his early years known as '''L. Errol Jaye''', was born February 7, 1928 in [[Jacksonville, Florida]].<ref name="willis"/>  He served in the [[United States Navy]] during [[World War II]]. After attending [[Fisk University]], his professional acting debut was on the stage, in [[New York Shakespeare Festival]]'s 1965 [[off-Broadway]] production of ''[[Troilus and Cressida]]''.<ref name="willis">[https://books.google.com/books?id=lw9lAAAAMAAJ&q=L.+Errol+Jaye+jackson&dq=L.+Errol+Jaye+jackson&hl=en&ei=Ya5kTN2JE47QsAP3wtDyCA&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCwQ6AEwAA John Willis' theatre world, Volume 28, page 241] from [[Google Books]]</ref>  In March 1968, he played Mr. Carpentier, the title character, in ''The Electronic Nigger'', part of a trio of one-act plays by [[Ed Bullins]], during [[The American Place Theatre]] production of the play's premiere.<ref>{{cite journal |date= September 1968 |title= The Electronic Nigger: Controversy Over Play's Title Fails to Cloud Author's Acclaim |journal=[[Ebony (magazine)|Ebony]] |volume= 23 |issue=11 |page= 97 |last= Bailey |first= Peter |authorlink= A. Peter Bailey | publisher= [[Johnson Publishing Company|Johnson Publishing]] |issn= 0012-9011 }}</ref><ref>{{cite web| title= The Electronic Nigger and Others| url=http://www.lortel.org/lla_archive/index.cfm?search_by=show&id=3736 | publisher= [[Lortel Archives]] | accessdate= 2010-08-20}}</ref>  He played a pastor in the Broadway premiere of ''[[The Great White Hope]]'', which ran for over 500 performances at the [[Alvin Theatre]] during 1968-1970.<ref>[http://www.ibdb.com/production.php?id=3417 The Great White Hope] from the [[Internet Broadway Database]]</ref>

As Leonard Jackson, he returned to Broadway two years later, first in the premiere of [[Conor Cruise O'Brien]]'s ''Murderous Angels'' and after its short run, to a Broadway revival of the [[Kurt Weill]] musical ''[[Lost in the Stars]]'' at the [[Imperial Theatre]].<ref name="ibdbbio">{{IBDB name|76630|Leonard Jackson}}</ref>

A dozen years later, Jackson returned to Broadway for the premiere of ''[[Ma Rainey's Black Bottom]]'', which ran for 276 performances and was [[New York Drama Critics Circle#Best Play|chosen "Best Play"]] by the [[New York Drama Critics Circle]].<ref>[http://www.playbill.com/news/article/77715-Ma-Raineys-Black-Bottom-with-Goldberg-and-Dutton-Opens-Feb-6 Ma Rainey's Black Bottom, with Goldberg and Dutton, Opens Feb. 6], a February 6, 2003 article from ''[[Playbill]]''</ref>   In 1991, Jackson was part of the cast for the Broadway premiere of ''[[Mule Bone]]'', an unfinished play written by [[Langston Hughes]] and [[Zora Neale Hurston]].<ref name="ibdbbio"/>  The production, mounted for the first time sixty years after it was written, received a negative review by [[Frank Rich]], who said the "three principal performers ... are at best likably amateurish, [though] their efforts are balanced by the assured center-stage turns of such old pros as Leonard Jackson, as a fuming man of the cloth, and [[Theresa Merritt]]."<ref>[https://www.nytimes.com/1991/02/15/theater/review-theater-a-difficult-birth-for-mule-bone.html?pagewanted=all A Difficult Birth For ''Mule Bone''], a February 15, 1991 review by [[Frank Rich]] of ''[[The New York Times]]''</ref>

==Television and film career==
His film roles include:<ref name="imdbbio">{{IMDb name|0413778|Leonard Jackson}}</ref>
* ''[[Conspiracy Theory (film)|Conspiracy Theory]]'' (1997) ... Old Man in Bookstore
* ''[[Basquiat (film)|Basquiat]]'' (1996) ... Jean Michel's Father
* ''[[Palookaville (film)|Palookaville]]'' (1995) ... Bus Driver
* ''[[Boomerang (1992 film)|Boomerang]]'' (1992) ... Lloyd the Chemist
* ''[[A Rage in Harlem]]'' (1991) ... Mr. Clay
* ''[[Basket Case 2]]'' (1990) ... Police Commissioner
* ''[[Second Sight (film)|Second Sight]]'' (1989) ... Doorman
* ''[[Eddie Murphy Raw]]'' (1987) ... Uncle Gus
* ''[[The Color Purple (film)|The Color Purple]]'' (1985) ... Pa Harris
* ''[[The Brother from Another Planet]]'' (1984) .... Smokey
* ''[[Car Wash (film)|Car Wash]]'' (1976) ... Earl
* ''[[Five on the Black Hand Side]]'' (1973) ... John Henry Brooks

On television, he had a recurring role on several PBS television series for children, including ''[[Sesame Street]]'', ''[[Shining Time Station]]'',  and ''[[Square One TV]]''/[[Mathnet]].<ref name="imdbbio"/>  He has also been featured in episodes of dramas such as ''[[Law & Order]]'', ''[[Homicide: Life on the Street]]'', and [[Spenser: For Hire]], and comedies such as [[Amen (TV series)|Amen]], [[The Cosby Show]], and ''[[The Jeffersons]]''.<ref name="tvcom">[http://www.tv.com/leonard-jackson/person/10464/appearances.html Leonard Jackson Credits] from [[TV.com]]</ref>

His made-for-television films include ''[[Separate but Equal (film)|Separate but Equal]]'' (1991) and ''[[Rage of Angels]]'', the 1983 adaptation of the [[Sidney Sheldon]] novel.<ref name="tvcom"/>

==Death==
Jackson died on December 22, 2013 in Manhattan, New York at age eighty-five after losing his battle with Alzheimer's disease. He is survived by his wife, Ada Jackson, brother-in-law, Alexander Edwards and wife, Janice, sister-in-law, Joan Phillips, nephew, Emerson Thompson and wife, Geraldine, nieces Kim Hunter and Patricia Jackson and a host of other cousins, nieces, nephews, family and friends.

== References ==
{{reflist|2}}

==External links==
*{{IMDb name|0413778|Leonard Jackson}}
*{{IBDB name|76630|Leonard Jackson}}
*{{IBDB name|96965|L. Errol Jaye}}
*[http://www.lortel.org/lla_archive/index.cfm?search_by=people&first=Leonard&middle=&last=Jackson Leonard Jackson] and [http://www.lortel.org/lla_archive/index.cfm?search_by=people&first=L.&middle=Errol&last=Jaye L. Errol Jaye] at [[Lortel Archives]]
*[http://www.yalerep.org/press/r_05/rep6/index2.html Press Photos] from [[Yale Repertory Theatre]], including two from 1984 that feature Jackson in ''[[Ma Rainey's Black Bottom]]''

{{Authority control}}

{{DEFAULTSORT:Jackson, Leonard}}
[[Category:1928 births]]
[[Category:Male actors from Jacksonville, Florida]]
[[Category:American male film actors]]
[[Category:American male stage actors]]
[[Category:Fisk University alumni]]
[[Category:African-American male actors]]
[[Category:American male television actors]]
[[Category:2013 deaths]]
[[Category:20th-century American male actors]]
[[Category:Deaths from Alzheimer's disease]]