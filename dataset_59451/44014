<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=F135
 |image=File:F-35A Lightning II Joint Strike Fighter Powerplant on display at Centenary of Military Aviation 2014.jpg
 |long caption=An F135-PW-100 powerplant on display at [[Royal Australian Air Force]] Centenary of Military Aviation 2014
}}{{Infobox Aircraft Engine
 |type=[[Turbofan]]
 |manufacturer=[[Pratt & Whitney]]
 |first run=
 |major applications=[[Lockheed Martin F-35 Lightning II]]
 |number built =  
 |program cost = 
 |unit cost = F135-PW-100, US$ 13.3M per [[Low rate initial production|LRIP]] 9<ref name="lrip9">{{cite web | url = http://www.defense-aerospace.com/articles-view/release/3/172975/pratt-says-latest-f_35-engine-contract-worth-$1.4bn.html | title = F-35 Joint Program Office Awards Pratt & Whitney LRIP 9 Contract for F135 Engines| date = 2016-04-12 }}</ref><br />
F135-PW-600, US$ 19.05M<ref name="lrip9" />
 |developed from = [[Pratt & Whitney F119]]
 |developed into = 
 |variants with their own articles =
}}
|}

The '''Pratt & Whitney F135''' is an [[Afterburner (engine)|afterburning]] [[turbofan]] developed for the [[Lockheed Martin F-35 Lightning II]] single-engine strike fighter. The F135 family has several distinct variants, including a conventional, forward thrust variant and a multi-cycle [[STOVL]] variant that includes a forward [[Rolls-Royce LiftSystem|lift fan]]. The first production engines were scheduled to be delivered in 2009.<ref>"F135 Engine Exceeds 12,000 Engine Test Hours as Pratt & Whitney Prepares to Deliver First Production Engines" (2009). Pratt & Whitney press release. July 28, 2009. [http://www.prnewswire.com/cgi-bin/stories.pl?ACCT=104&STORY=/www/story/07-28-2009/0005067369&EDATE= PR Newswire Link]</ref>

==Development==
The origins of the F135 lie with the [[Lockheed Corporation]] [[Skunk Works]]'s efforts to develop a stealthy STOVL strike fighter for the [[U.S. Marine Corps]] under a 1986 [[DARPA]] program. Lockheed employee [[Paul Bevilaqua]] developed and patented<ref name="LiftFanPatent1">[http://www.patentgenius.com/patent/5209428.html "Propulsion system for a vertical and short takeoff and landing aircraft"], United States Patent 5209428. PDF of original : http://www.freepatentsonline.com/5209428.pdf</ref> a concept aircraft and propulsion system, and then turned to Pratt & Whitney (P&W) to build a demonstrator engine.<ref>"The Shaft Driven Lift Fan Propulsion System For The Joint Strike Fighter" Paul M. Bevilaqua, American Helicopter Society 53rd Annual Forum, Virginia Beach, April 29-May 1, 1997. Fig. 6 Turbine Performance Map</ref> The demonstrator used the first stage fan from a [[Pratt & Whitney F119|F119]] engine for the lift fan, the engine fan and core from the [[Pratt & Whitney F100|F100-220]] for the core, and the larger low-pressure turbine from the [[Pratt & Whitney F100|F100-229]] for the low-pressure turbine of the demonstrator engine. The larger turbine was used to provide the additional power required to operate the lift fan. Finally, a variable thrust deflecting nozzle was added to complete the "F100-229-''Plus''" demonstrator engine. This engine proved the lift-fan concept and led to the development of the current F135 engine.<ref name="JPP">Bevilaqua, Paul M. (2005). [http://pdf.aiaa.org/jaPreview/JPP/2005/PVJA15228.pdf One-page preview of] "Joint Strike Fighter Dual-Cycle Propulsion System." Journal of Propulsion and Power, Vol. 21, 5. pp. 778-783.</ref>

P&W developed the F135 from their [[Pratt & Whitney F119|F119]] turbofan, which powers the [[F-22 Raptor]], as the "F119-JSF". The F135 integrates the F119 core with new components optimized for the JSF.<ref name="Connors 171">Connors, p. 171.</ref>  The F135 is assembled at a plant in [[Middletown, Connecticut]].  Some parts of the engine are made in [[Longueuil]], Quebec, Canada,<ref>{{cite web|url=http://www.ic.gc.ca/eic/site/ic1.nsf/eng/05897.html|title=Home - Industry Canada|author=Communications and Marketing Branch|work=ic.gc.ca|accessdate=1 February 2015}}</ref> and in Poland.<ref>{{cite web|url=http://articles.courant.com/2011-02-18/business/hc-pratt-f-35-victory-20110218_1_f135-pratt-whitney-stephanie-duvall|title=Pratt & Whitney's F-35 Victory Secures 4,250 Connecticut Jobs|work=Hartford Courant|accessdate=1 February 2015}}</ref>

[[File:Engine of F-35.jpg|thumb|left|The F135-PW-600 engine with [[Rolls-Royce LiftSystem|lift fan]], roll posts, and rear vectoring nozzle, as designed for the [[F-35 Lightning II#F-35B|F-35B V/STOL]] variant, at the Paris Air Show, 2007]]

The first production propulsion system for operational service was scheduled for delivery in 2007. The F-35 will serve the U.S., UK, and other international customers.  The initial F-35s will be powered by the F135, but the [[GE Aviation|GE]]/[[Rolls-Royce plc|Rolls-Royce]] team was developing the [[General Electric/Rolls-Royce F136|F136]] turbofan as an alternate engine for the F-35 as of July 2009. Initial Pentagon planning required that after 2010, for the Lot 6 aircraft, the engine contracts will be competitively tendered. However, since 2006 the Defense Department has not requested funding for the alternate F136 engine program, but Congress has maintained program funding.<ref>Trimble, Stephen. [http://www.flightglobal.com/articles/2009/07/23/330100/us-senate-axes-f-35-alternate-engine.html "US Senate axes F-35 alternate engine"]. Flightglobal.com, 23 July 2009.</ref><!-- Update this if FY 2010 budget is signed without F136 funding. -->

The F135 team is made up of [[Pratt & Whitney]], [[Rolls-Royce plc|Rolls-Royce]] and [[Hamilton Sundstrand]].  Pratt & Whitney is the prime contractor for the main engine, and systems integration.  Rolls-Royce is responsible for the vertical lift system for the STOVL aircraft. Hamilton Sundstrand is responsible for the electronic engine control system, actuation system, PMAG, gearbox, and health monitoring systems.  [[Woodward, Inc.]] is responsible for the fuel system.

As of 2009, P&W was developing a more durable version of the F135 engine to increase the service life of key parts. These parts are primarily in the hot sections of the engine (combustor and high-pressure turbine blades specifically) where current versions of the engine are running hotter than expected, reducing life expectancy. The test engine is designated ''XTE68/LF1'', and testing is expected to begin in 2010.<ref name=Jane>Harrington, Caitlin. (2009) "Pratt & Whitney starts development of new F-35 test engine". Jane's Defence Weekly, March 27, 2009.</ref> This redesign has caused “substantial cost growth.”<ref>[http://www.aviationweek.com/aw/generic/story_channel.jsp?channel=defense&id=news/awst/2010/03/08/AW_03_08_2010_p28-208867.xml&headline=Donley:%20No%20JSF%20Alternatives%20Exist Donley: No JSF Alternatives Exist]</ref>

P&W expects to deliver the F135 below the cost of the F119, even though it is a more powerful engine.<ref>Graham Warwick, Amy Butler [http://www.aviationweek.com/aw/generic/story_generic.jsp?channel=awst&id=news/awst/2010/12/06/AW_12_06_2010_p33-273567.xml "Pentagon Ramps Up Pressure On F-35 Price."] ''[[Aviation Week]]'', 3 December 2010.</ref>

In February 2013 a cracked turbine blade was found during a scheduled inspection. The crack was caused by operating for longer periods than typical at high turbine temperatures.<ref>{{cite web|url=http://defense-update.com/20130301_f-35-grounding-lifted.html|title=F-35 Lightning II Resume Flying – ‘Blade Crack Caused By Stressful Testing’|author=News Desk|work=defense-update.com|accessdate=1 February 2015}}</ref>

The 100th engine was delivered in 2013.<ref>{{cite web|url=http://www.pw.utc.com/Press/Story/20130618-0200/|title=Press Releases|work=utc.com|accessdate=1 February 2015}}</ref> LRIP-6 was agreed in 2013 for $1.1 billion for 38 engines of various types, continuing the unit cost decreases.<ref>{{cite web |url=http://www.defensenews.com/article/20131023/DEFREG02/310230033 |title=Pratt & Whitney, Pentagon Reach $1.1B Deal on F-35 Engines |last1=MEHTA |first1=AARON |date=23 October 2013 |website=www.defensenews.com |publisher=Gannett Government Media Corporation |accessdate=23 October 2013}}</ref>

In 2013, a former P&W employee was caught attempting to ship "numerous boxes" of sensitive information about the F135 to Iran.<ref>{{cite web |url=http://www.courant.com/business/hc-pratt-whitney-iran-documents-20140113,0,6907892.story |title=Former Pratt Employee Arrested Trying To Ship F-35 Documents To Iran |last1=DOWLING |first1=BRIAN |date=13 January 2014 |website=www.courant.com |publisher=The Hartford Courant |accessdate=13 January 2014}}</ref>

In December 2013 the hollow first stage fan [[blisk]] failed at 77% of its expected life during a ground test. It will be replaced by a solid part adding {{convert|6|lb|kg|1|abbr=on}} in weight.<ref>{{cite web|url=http://aviationweek.com/defense/investigators-eye-third-stage-turbine-f-35-remains-grounded|title=Investigators Eye Third-Stage Turbine As F-35 Remains Grounded|work=aviationweek.com|accessdate=1 February 2015}}</ref>

F-35 program office executive officer Air Force Lt. Gen. Christopher C. Bogdan has called out P&W for falling short on manufacturing quality of the engines and slow deliveries.<ref>{{cite web |url=http://www.stripes.com/news/us/pentagon-criticizes-pratt-whitney-for-systemic-f-35-production-issues-1.274724 |title=Pentagon criticizes Pratt & Whitney for 'systemic' F-35 production issues |last1=Dowling |first1=Brian |date=March 26, 2014 |website=www.stripes.com |publisher=Hartford (Conn.) Courant |accessdate=March 26, 2014}}</ref> His deputy director Rear Admiral Randy Mahr said that P&W stopped their cost cutting efforts after "they got the monopoly".<ref>{{cite web |url=http://www.reuters.com/article/2014/04/07/us-unitedtechnologies-fighter-engine-idUSBREA361K320140407 |title=Pratt must push harder to cut F-35 engine cost: Pentagon |last1=Shalal |first1=Andrea |date=7 April 2014 |website=www.reuters.com |publisher=Reuters |accessdate=8 April 2014}}</ref> In 2013 the price of the F135 increased by $4.3 billion.<ref>{{cite news |url=http://www.reuters.com/article/2014/04/17/us-lockheed-fighter-idUSBREA3G27K20140417 |title=Cost to buy F-35 up 2 percent; to operate down 9 percent: Pentagon |last1=Shalal |first1=Andrea |date=17 April 2014 |website=www.reuters.com |publisher=Thomson Reuters |accessdate=18 April 2014}}</ref>

In July 2014 there was an uncontained failure of a fan rotor while the aircraft was preparing for take-off. The parts passed through a fuel tank and caused a fire, grounding the F-35 fleet.<ref>{{cite news |url=http://news.usni.org/2014/07/07/sources-engine-definitely-blame-june-f-35-fire |title=Sources: Engine ‘Definitely’ To Blame in June F-35 Fire |last1=Majumdar |first1=Dave |date=7 July 2014 |website=news.usni.org |publisher=U.S. NAVAL INSTITUTE |accessdate=7 July 2014}}</ref> The failure was caused by excessive rubbing at the seal between the fan blisk and the fan stator during [[g-force|high-g]] maneuvering three weeks before the failure. The engine "flex" generated a temperature of over 1,000 °C (1,900 °F) in materials designed to fail at 540 °C (1,000 °F). Microcracks appeared in third-stage fan blades, according to program manager Christopher Bogdan, causing blades to separate from the disk; the failed blades punctured the fuel cell and hot air mixing with jet fuel caused the fire.<ref>Sweetman, Bill, Butler Amy, and Guy Norris, There's the rub, Aviation Week & Space Technology, September 8, 2014, pp.22-23</ref><ref>{{cite news |url=http://aviationweek.com/awin-only/blade-rubbing-root-f-35a-engine-fire |title=Blade 'Rubbing' At Root of F-35A Engine Fire |last1=Butler |first1=Amy |date=13 July 2014 |website=aviationweek.com |publisher=Penton |accessdate=13 July 2014}}</ref><ref name="McGarry15Sep14">{{cite web|url = http://www.dodbuzz.com/2014/09/15/bogdan-f-35-engine-may-be-fixed-by-years-end/|title = Bogdan: F-35 Engine Fix May be Ready by Year’s End|accessdate = 17 September 2014|last = McGarry|first = Brendan|date = 15 September 2014}}</ref> As a short term fix, each aircraft is flown on a specific flight profile to allow the rotor seal to wear a mating groove in the stator to prevent excessive rubbing.<ref>{{cite news |url=http://www.dodbuzz.com/2014/10/31/pentagon-implements-f-35-engine-fixes/ |title=Pentagon Implements F-35 Engine Fixes |last1=Osborn |first1=Kris |date=31 October 2014 |website=www.dodbuzz.com |publisher=Monster |accessdate=31 October 2014}}</ref>

In May 2014, Pratt & Whitney discovered conflicting documentation about the origin of titanium material used in some of its engines, including the F135. The company assessed that the uncertainty did not pose a risk to safety of flight but suspended engine deliveries as a result in May 2014. Bogdan supported P&W's actions and said the problem was now with A&P Alloys, the supplier. The US Defense Contract Management Agency wrote in June 2014 that Pratt & Whitney’s "continued poor management of suppliers is a primary driver for the increased potential problem notifications." A&P Alloys stated that it has not been given access to the parts to do its own testing but stood behind its product. Tracy Miner, an attorney with Boston-based Demeo LLP representing A&P Alloys said, "it is blatantly unfair to destroy A&P’s business without allowing A&P access to the materials in question".<ref>{{cite news |url=https://news.yahoo.com/pratt-suspended-f-35-engine-shipments-may-over-210437013--finance.html |title=Pratt halted F-35 engine shipments in May over titanium |last1=Krauskopf |first1=Lewis |date=29 August 2014 |website=news.yahoo.com |publisher=Reuters |accessdate=29 August 2014}}</ref><ref>{{cite news |url=http://www.defensenews.com/article/20140903/DEFREG02/309030029/F-35-Head-Delays-Coming-Test-Planes-Grounded-Through-September |title=F-35 Head: Delays Coming if Test Planes Grounded Through September |last1=MEHTA |first1=AARON |date=3 September 2014 |website=www.defensenews.com |publisher=Gannett |accessdate=4 September 2014}}</ref><ref name="Capaccio29Aug14">{{cite news|url = https://www.bloomberg.com/news/2014-08-29/pratt-whitney-halted-f-35-engine-delivery-over-titanium.html|title = Pratt & Whitney Halted F-35 Engine Delivery Over Titanium|accessdate = 5 September 2014|last = Capaccio|first = Tony|date = 29 August 2014| work = [[Bloomberg News|Bloomberg]]}}</ref>

Pratt managed to meet their 2015 production goals, but "recurring manufacturing quality issues" in turbine blades and electronic control systems required engines to be pulled from the fleet.<ref>{{cite news |url=https://www.bloomberg.com/news/articles/2016-03-31/united-technologies-f-35-engines-found-to-have-recurring-flaws |title=United Technologies' F-35 Engines Found to Have Recurring Flaws |last1=Capaccio |first1=Anthony |date=31 March 2016 |website=www.bloomberg.com |publisher=Bloomberg L.P. |accessdate=31 March 2016}}</ref>

==Design==
[[File:Jet engine F135(STOVL variant)'s thrust vectoring nozzle N.PNG|thumb|Thrust vectoring nozzle of the F135-PW-600 [[STOVL]] variant]]
[[File:LiftThrust-small1.PNG|thumb|Diagram of [[F-35 Lightning II#F-35B|F-35B]] and smaller [[powered lift]] aircraft]]

The F-135, a mixed-flow afterburning turbofan, was derived from the [[F-119]] engine but was given a new fan and LP turbine.<ref name="codeoneA">{{cite web|url=http://www.codeonemagazine.com/article.html?item_id=28|title=X to F: F-35 Lightning II And Its X-35 Predecessors|work=codeonemagazine.com|accessdate=29 May 2016}}</ref>

There are 3 F-135 variants with the -400 being similar to the -100, the major difference being the use of salt-corrosion resistant materials.<ref name="Jane's Aero">"The Pratt & Whitney F135". ''Jane's Aero Engines''. Jane's Information Group, 2009 [http://search.janes.com/Search/documentView.do?docId=/content1/janesdata/binder/jae/jae_1063.htm@current&pageSelected=allJanes&keyword=F135&backPath=http://search.janes.com/Search&Prod_Name=JAE& (subscription version, dated 10 July 2009)].</ref> The -600 is described below with an explanation of the engine configuration changes that take place for hovering. The engine and [[Rolls-Royce LiftSystem]] make up the Integrated Lift Fan Propulsion System (ILFPS).<ref>{{cite web|url = http://naa.aero/userfiles/files/documents/Press%20Releases/Collier%202001%20PR.pdf|title = Integrated Lift Fan Gets Nod for Collier Trophy|accessdate = 29 May 2016|author=[[National Aeronautic Association]]|date = 25 February 2002}}</ref>

The lift for the STOVL version in the hover is obtained from a 2-stage lift fan (about 46%<ref name="utc">{{cite web|url = http://www.pw.utc.com/Content/F135_Engine/pdf/b-2-4_me_f135_stovl.pdf|title = Power for F-35B Short Take Off and Vertical Landing (STOVL)|accessdate = 29 May 2016|author = [[Pratt & Whitney]]|date = }}</ref>) in front of the engine, a vectoring exhaust nozzle (about 46%<ref name="utc"/>) and a nozzle in each wing using  fan air from the bypass duct(about 8%<ref name="utc"/>). These relative contributions to the total lift are based on thrust values of 18,680&nbsp;lb, 18,680&nbsp;lb and 3,290&nbsp;lb respectively.<ref name="utc"/> Another source gives thrust values of 20,000&nbsp;lb, 18,000&nbsp;lb and 3,900&nbsp;lb respectively.<ref>{{cite web|url = http://www.rolls-royce.com/products-and-services/defence-aerospace/products/combat-jets/rolls-royce-liftsystem/technology.aspx|title = Technology |accessdate = 29 May 2016|author = Rolls-Royce plc|year = 2016}}</ref>

In this configuration most of the bypass flow is ducted to the wing nozzles, known as roll posts. Some is used for cooling the rear exhaust nozzle, known as the 3-bearing swivel duct nozzle (3BSD).<ref>{{cite web|url=http://www.codeonemagazine.com/article.html?item_id=137|title=F-35B Lightning II Three-Bearing Swivel Nozzle|work=codeonemagazine.com|accessdate=29 May 2016}}</ref> At the same time an auxiliary inlet is opened on top of the aircraft to provide additional air to the engine with low distortion during the hover.<ref name="codeoneA"/>

The lift fan is driven from the LP turbine through a shaft extension on the front of the LP rotor and a clutch. The engine is operating as a separate flow turbofan with a higher bypass ratio.<ref name="bevilaqua">"Genesis of the F-35 Joint Strike Fighter" Paul M. Bevilaqua, 2009 Wright Brothers Lecture, Journal of Aircraft, Vol. 46, No. 6, November–December 2009</ref> The power to drive the fan (about 30,000&nbsp;SHP<ref name="bevilaqua"/>) is obtained from the LP turbine by increasing the hot nozzle area.<ref name="bevilaqua"/>

A higher bypass ratio increases the thrust for the same engine power as a fundamental consequence of transferring power from a small diameter propelling jet to a larger diameter one.<ref>"V/STOL by Vertifan" William T. Immenschuh, Flight International, 1 October 1964</ref>
The thrust augmentation for the F-135 in the hover using its higher bypass ratio is about 50%<ref name="utc"/> with no increase in fuel flow. Thrust augmentation in horizontal flight using the afterburner is about 52%<ref name="utc"/> but with a large increase in fuel flow.

The transfer of approximately 1/3<ref name="bevilaqua2">"The Shaft Driven Lift Fan Propulsion System for the Joint Strike Fighter" Paul M. Bevilaqua, American Helicopter Society 53rd Annual Forum, Virginia Beach, April 29-May 1, 1997</ref> of the power available for hot nozzle thrust to the lift fan reduces the temperature and velocity of the rear lift jet impinging on the ground.<ref name="bevilaqua2"/>

Improving engine reliability and ease of maintenance is a major objective for the F135. The engine has fewer parts than similar engines which should improve reliability. All line-replaceable components (LRCs) can be removed and replaced with a set of six common hand tools.<ref>[http://www.pratt-whitney.com/vgn-ext-templating/v/index.jsp?vgnextoid=2e35288d1c83c010VgnVCM1000000881000aRCRD&prid=95068129982de010VgnVCM100000c45a529f____ Pratt & Whitney F135 Press release] {{Dead link|date=July 2009}}</ref> The F135's health management system is designed to provide real time data to maintainers on the ground, allowing them to troubleshoot problems and prepare replacement parts before the aircraft returns to base. According to Pratt & Whitney, this data may help drastically reduce troubleshooting and replacement time, as much as 94% over legacy engines.<ref>Rajagopalan, R., Wood, B., Schryver, M. (2003). ''Evolution of Propulsion Controls and Health Monitoring at Pratt and Whitney''. AIAA/ICAS International Air and Space Symposium and Exposition: The Next 100 Years. 14–17 July 2003, Dayton, Ohio. AIAA 2003-2645.</ref>

The F-35 can achieve a limited 100% throttle cruise without afterburners of Mach&nbsp;1.2 for 150&nbsp;miles.<ref name=tirace>{{cite web|last=Tirpak|first=John|title=The F-35’s Race Against Time|url=http://www.airforce-magazine.com/MagazineArchive/Pages/2012/November%202012/1112fighter.aspx|publisher=[[Air Force Association]]|accessdate=4 November 2012|date=November 2012|quote=while not technically a "supercruising" aircraft, can maintain Mach&nbsp;1.2 for a dash of 150&nbsp;miles without using fuel-gulping afterburners|archiveurl = https://web.archive.org/web/20121108143240/http://www.airforce-magazine.com/MagazineArchive/Pages/2012/November%202012/1112fighter.aspx |archivedate = 8 November 2012}}</ref>

Because the F135 is designed for a fifth generation jet fighter, it is the second afterburning jet engine to use special "low-observable coatings".<ref>Mecham, Michael. [http://www.aviationweek.com/Article.aspx?id=/article-xml/awx_07_09_2012_p0-474581.xml "Lower F-35 Output Challenges Engine Costs."] ''Aviation Week'', 9 July 2012.</ref>

===Planned improvements===
Pratt and Whitney is cooperating with the US Navy on a two-block improvement plan for the F135 engine, although no service has issued a requirement for an upgraded engine. The goals are a 7-10% increase in thrust and a 5-7% lower fuel burn.  Technology to better cool turbine blades is included in the plans, which would increase the longevity of the engine and substantially reduce maintenance costs.  Related to the longer term Block 2 upgrade would be work with the US Air Force's Adaptive Engine Transition Program, intended to introduce technology for an engine rated at 45,000&nbsp;lb of thrust, to be used in a sixth-generation fighter.<ref>Norris, Guy, Power plan, Aviation Week & Space Technology, April 13–26, 2015, p.26</ref>

==Variants==
* '''F135-PW-100''' : Used in the F-35A Conventional Take-Off and Landing variant
* '''F135-PW-400''' : Used in the F-35C carrier variant
* '''F135-PW-600''' : Used in the F-35B Short Take-Off Vertical Landing variant

==Applications==
* [[Lockheed Martin F-35 Lightning II]]

==Specifications (F135-PW-100)==
{{jetspecs
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]]
     Please include units where appropriate (main comes first, alt in parentheses). If data is missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> 
-->
|ref=''F135engine.com''<ref name="F135engine.com">{{cite web|url=http://www.f135engine.com/docs/B-2-4_F135_SpecsChart.pdf|title=F135 Engine|work=f135engine.com|accessdate=29 May 2016}}</ref> 
|type=[[afterburning]] [[turbofan]]
|length={{convert|220|in|cm|0|abbr=on}}
|diameter={{convert|46|in|cm|0|abbr=on}} max., {{convert|43|in|cm|0|abbr=on}} at the fan inlet
|weight= {{convert|3750|lb|kg|0|abbr=on}}
|compressor= 3-stage fan, 6-stage high-pressure [[axial-flow compressor|compressor]] 
|combustion= [[Combustor#Types of combustors|annular combustor]]
|turbine=1-stage high-pressure [[turbine]], 1-stage low-pressure turbine
|fueltype=
|oilsystem=
|power=
|thrust=43,000 lbf (190 kN) max., 28,000 lbf (125 kN) intermediate
|compression= 28:1 overall pressure ratio
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon=
|power/weight=
|thrust/weight= 7.47:1 (dry), 11.467:1 (wet/afterburning)
|bypass ratio= 0.57
}}

==Specifications (F135-PW-600)==
{{jetspecs
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]]
     Please include units where appropriate (main comes first, alt in parentheses). If data is missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> 
-->
|ref=''F135engine.com''<ref name="F135engine.com"/> 
|type=[[Afterburning]] [[Turbofan]] with shaft driven remote lift fan
|length=369 in (937.3 cm)
|diameter= 46 in (116.8 cm) maximum, 43 in (109.2 cm) fan inlet, 53 in (134.6 cm) lift fan inlet
|weight= 
|compressor= 3 stage fan, 6 stage high-pressure [[Axial-flow compressor|compressor]], 2 stage, contra-rotating, shaft driven lift fan 
|combustion= [[Combustor#Types of combustors|annular combustor]]
|turbine=Single stage high pressure [[turbine]], 2-stage low pressure [[turbine]]
|fueltype=
|oilsystem=
|power=
|thrust=41,000 lbf (182 kN) max, 27,000 lbf (120 kN) intermediate, 40,650 lbf (180.8 kN) hover
|compression= 28:1 overall pressure ratio (conventional), 29:1 overall pressure ratio (powered lift),
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon=
|power/weight=
|thrust/weight=
|bypass ratio= 0.56 conventional, 0.51 powered lift 
}}

==See also==
{{aircontent
|see also=<!-- other related articles that have not already linked: -->
* [[Rolls-Royce LiftSystem]]

|related=<!-- designs which were developed into or from this aircraft: -->
* [[Pratt & Whitney F119]]

|similar engines=<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
* [[General Electric/Rolls-Royce F136]]

|lists=<!-- relevant lists that this aircraft appears in: -->
* [[List of aircraft engines]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Reflist|30em}}

;Bibliography
{{refbegin}}
* {{cite book|last= Connors |first= Jack |coauthors= |title= The Engines of Pratt & Whitney: A Technical History |publisher= [[American Institute of Aeronautics and Astronautics]] |location= Reston. Virginia |year= 2010 |isbn= 978-1-60086-711-8 |url= |pages= }}
* Jane's Information Group. ''Pratt & Whitney F135''. Jane's Aero Engines. Modified 10 July 2009.
{{refend}}

==External links==
{{Commons category}}
* [http://www.pw.utc.com/F135_Engine Pratt & Whitney F135 page] 
* [http://www.jsf.mil/gallery/gal_photo_sdd_f135.htm www.jsf.mil: F135 gallery]

{{Navboxes|list1=
{{Lockheed Martin F-35 Lightning II}}
{{P&W gas turbine engines}}
{{USAF gas turbine engines}}
}}

{{DEFAULTSORT:Pratt and Whitney F135}}
[[Category:Low-bypass turbofan engines]]
[[Category:Pratt & Whitney aircraft engines|F135]]
[[Category:Turbofan engines 2000–2009]]