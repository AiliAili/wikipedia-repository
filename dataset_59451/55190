{{redirect|ACGC|the video game|Animal Crossing}}

{{Infobox Journal
| cover	=	
| discipline	=	[[Chemistry]]
| language	=	English
| abbreviation	=	''ACGC Chem. Res. Commun.''
| website 	=	https://web.archive.org/web/20070206003312/http://userpage.chemie.fu-berlin.de:80/~steven/ACGC/ACGC-CRC.html (TOC)
| publisher	=	Asian Coordinating Group for Chemistry (ACGC)
| country	=	[[Malaysia]]
| history	=	1991–present
| ISSN	=	1020-5586 
| CODEN	=	ACRCFA
}}

The '''ACGC Chemical Research Communications''' ({{ISSN|1020-5586}}, [[CODEN]] ACRCFA) is a [[scientific journal]] in chemistry, published by the '''Asian Coordinating Group for Chemistry''' ([[ACGC]]). The ACGC, which was formed in 1984, is an ''ad hoc'' committee of [[UNESCO]] comprising representatives of UNESCO Regional Networks and other organisations active in the promotion and development of chemistry in Asia.

The journal was founded in 1991. The first volumes appeared irregularly, mostly one volume per year, later also two volumes per year.

Volume 1 to 16 were edited by Stephen G. Pyne from the Department of Chemistry at [[University of Wollongong]], Australia. Beginning with volume 17 (2004), Professor Nordin Bin Haji Lajis, Department of Chemistry at [[Universiti Putra Malaysia]] overtook editorship, which changed to Prof. Khozirah Shaari at the Laboratory of Natural Products at University Putra Malaysia with volume 18 (2005).

Volume 11 (2000) of this journal records the Proceedings of the Ninth [[Asian Symposium on Medicinal Plants, Spices and Other Natural Products]] (ASOMPS IX) and includes papers and abstracts delivered by the plenary and invited lecturers.

== External links ==

* Tabe of contents (TOC) of the journal: [https://web.archive.org/web/20070206003312/http://userpage.chemie.fu-berlin.de:80/~steven/ACGC/ACGC-CRC.html]
* Homepage of the ACGC at the Federation of Asian Chemical Societies (FACS): [http://www.facs-as.org/acgc/acgc-index.htm]

[[Category:Chemistry journals]]