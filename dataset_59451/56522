{{Infobox journal
| title        = Dædalus
| caption = Journal of the American Academy of Arts and Sciences
| cover        = [[Image:daedaluslowres.jpg]]
| editor       = Phyllis Bendell, managing
| discipline   = [[Multidisciplinary]]
| former_names = Proceedings of the American Academy of Arts and Sciences
| language     = English
| abbreviation = Daedalus
| publisher    = [[MIT Press]] for the [[American Academy of Arts and Sciences]]
| country      = United States
| frequency    = Quarterly
| history      = 1955-present
| openaccess   = 
| impact       = 0.082
| impact-year  = 2009
| website      = http://www.mitpressjournals.org/loi/daed
| link1        = http://www.mitpressjournals.org/toc/daed/current
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = daedalus
| OCLC         = 1565785
| LCCN         = 
| CODEN        = 
| ISSN         = 0011-5266
| eISSN        = 1548-6192 
}}
{{italictitle}}
'''''Dædalus''''' is a [[peer-reviewed]] [[academic journal]] founded in 1955 as a replacement for the '''''Proceedings of the American Academy of Arts and Sciences''''', the volume and numbering system of which it continues. In 1958 it began quarterly publication as ''The Journal of the American Academy of Arts and Sciences''. The journal is published by [[MIT Press]] on behalf of the [[American Academy of Arts and Sciences]]. Each issue addresses a theme with essays on the arts, sciences, and humanities. Special features include fiction, poetry, and a notes section. Publication is by invitation only. The journal is indexed in ''[[Scopus]]'' and the ''[[Social Sciences Citation Index]]'', among others.

== External links ==
* {{Official website|http://www.mitpressjournals.org/loi/daed}}
*[http://www.amacad.org/ American Academy of Arts and Sciences]

[[Category:Publications established in 1955]]
[[Category:MIT Press academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Multidisciplinary humanities journals]]
[[Category:Academic journals associated with learned and professional societies]]

{{humanities-journal-stub}}