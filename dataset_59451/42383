{{About|the airliner initially developed in the 1990s by McDonnell Douglas as the MD-95|the Boeing Model 717 military transport developed in the 1950s|C-135 Stratolifter|the earlier jetliner temporarily coded as the Boeing 717|Boeing 720}}
{{Use mdy dates|date=December 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= Boeing 717
 |image=  File:Delta Air Lines Boeing 717-2BD N966AT.jpg<!-- Flight images are preferred for aircraft. Do not change image without discussing on the talk page first, thanks. -->
 |caption= [[Delta Air Lines]] 717 
 |alt= 
}}{{Infobox aircraft type
 |type= [[Narrow-body aircraft|Narrow-body]] [[jet airliner]]
 |national origin= [[United States]]
 |manufacturer= [[Boeing Commercial Airplanes]]
 |designer= [[McDonnell Douglas]]
 |first flight= September 2, 1998<ref name=717h>{{cite web|title=Boeing: Historical Snapshot: 717/MD-95 commercial transport|url=http://www.boeing.com/history/products/717-md-95.page|website=Boeing.com|accessdate=June 30, 2015}}</ref><ref name=The_Boeing_717>{{cite web |title=The Boeing 717 |publisher=Boeing Commercial Airplanes |url=http://www.boeing.com/commercial/717/index.html |accessdate=July 4, 2015 |archiveurl=https://web.archive.org/web/20110513235157/http://www.boeing.com/commercial/717/index.html |archivedate=May 13, 2011 |deadurl=yes}}</ref>
 |introduced= October 12, 1999 with [[AirTran Airways]]<ref name=The_Boeing_717/>
 |status=In service <!--The 717's production status is covered with the Produced field below. -->
 |primary user= [[Delta Air Lines]] <!--Limit one (1) primary user. Top 4 users listed in 'primary user' and 'more users' fields based on number of their fleets. -->
 |more users= [[Volotea]] <br/>[[Hawaiian Airlines]] <br/>[[QantasLink]] <!--Limit is THREE (3) total in 'more users' field (4 total users). Listed in by number in use, i.e. top users listed.  Please separate with <br/>.-->
 |produced= 1998–2006
 |number built= 156
 |unit cost= 
 |developed from= [[McDonnell Douglas DC-9]]
 |variants with their own articles= 
}}
|}

The '''Boeing 717''' is a [[Twinjet|twin-engine]], [[Narrow-body aircraft|single-aisle]] [[jet airliner]], developed for the 100-seat market. The airliner was designed and originally marketed by [[McDonnell Douglas]] as the '''MD-95''', a derivative of the [[McDonnell Douglas DC-9|DC-9]] family. Capable of seating up to 134 passengers, the 717 has a design [[Range (aircraft)|range]] of 2,060 nautical miles (3,815&nbsp;km). It is powered by two [[Rolls-Royce BR715]] [[turbofan]] engines mounted at the rear of the fuselage.

The first order was placed in October 1995 by [[ValuJet Airlines]] (later [[AirTran Airways]]); McDonnell Douglas and Boeing merged in 1997<ref name=717h /> prior to production.  The airliner entered service in 1999 as the ''Boeing 717''. Production ceased in May 2006 after 156 were built.<ref name="last 717s">{{Cite press release |title= Boeing Delivers Final 717s; Concludes Commercial Production in California |publisher=Boeing |date= May 23, 2006 |url=http://boeing.mediaroom.com/2006-05-23-Boeing-Delivers-Final-717s-Concludes-Commercial-Production-in-California |accessdate= June 30, 2015}}</ref> There were 154 Boeing 717 aircraft in service as of July 2016.

==Development==

===Background===
[[Douglas Aircraft Company|Douglas Aircraft]] launched the [[McDonnell Douglas DC-9|DC-9]], a short-[[Range (aircraft)|range]] companion to their larger four engined [[Douglas DC-8|DC-8]] in 1963.<ref name="G_Endres">Endres, Gunter. ''McDonnell Douglas DC-9/MD-80 & MD-90''. London: Ian Allan, 1991. ISBN 0-7110-1958-4.</ref> The DC-9 was an all-new design, using two rear fuselage-mounted [[Pratt & Whitney JT8D]] [[turbofan]] engines; a small, efficient wing; and a [[T-tail]].<ref name="douglas_jetliners">Norris, Guy and Mark Wagner. "DC-9: Twinjet Workhorse". ''Douglas Jetliners''. MBI Publishing, 1999. ISBN 0-7603-0676-1.</ref> The DC-9's [[maiden flight]] was in 1965 and entered airline service later that year.<ref name="aijun80 p293">''Air International'' June 1980, p. 293.</ref> When production ended in 1982 a total of 976 DC-9s had been produced.<ref name="douglas_jetliners"/>

The [[McDonnell Douglas MD-80|McDonnell Douglas MD-80 series]], the second generation of the DC-9, began airline service in 1980. It was a lengthened [[DC-9-50]] with a higher [[maximum take-off weight]] (MTOW) and higher fuel capacity, as well as next-generation Pratt and Whitney JT8D-200 series engines and an improved wing design.<ref>{{cite web |url=http://www1.boeing.com/commercial/md-80-90/index.html |title=Boeing: MD-80 Background |publisher=Boeing |accessdate=July 16, 2015 |archiveurl=https://web.archive.org/web/19990302070457/http://www1.boeing.com/commercial/md-80-90/index.html |archivedate=March 2, 1999 |deadurl=yes}}</ref> 1,191 MD-80s were delivered from 1980 to 1999.<ref name="orders-deliveries">{{cite web |url=http://www.boeing.com/commercial/#/orders-deliveries |title=Boeing: Commercial&nbsp;— Orders & Deliveries |accessdate=July 16, 2015}}</ref>

The [[McDonnell Douglas MD-90|MD-90]] was developed from the MD-80 series.<ref name="AI p90">Swanborough 1993, p.90.</ref> It was launched in 1989 and first flew in 1993.<ref>{{cite web |url=http://www.boeing.com/commercial/md-80-90/md90.html |title=Boeing: Commercial Airplanes&nbsp;— MD-90 Background |accessdate=July 16, 2015 |archiveurl=https://web.archive.org/web/20130216043002/http://www.boeing.com/commercial/md-80-90/md90.html |archivedate=February 16, 2013}}</ref> The MD-90 was longer, featured a [[glass cockpit]] (electronic instrumentation) and more powerful, quieter, fuel efficient [[IAE V2500|IAE V2525-D5]] engines, with the option of upgrading that to an [[IAE V2500|IAE V2528]] engine.<ref>{{cite web |url=http://www.boeing.com/commercial/md-90/product.html |title=Boeing: Commercial Airplanes&nbsp;— MD-90 Technical Characteristics |accessdate=July 16, 2015 |archiveurl=https://web.archive.org/web/20130308021328/http://www.boeing.com/commercial/md-90/product.html |archivedate=March 8, 2013 |deadurl=yes}}</ref> A total of 116 MD-90 airliners were delivered.<ref name="orders-deliveries"/>

===MD-95===
The MD-95 traces its history back to 1983 when McDonnell Douglas outlined a study named the ''DC-9-90''. During the early 1980s, as production of the DC-9 family moved away from the smaller Series 30 towards the larger Super 80 (later redesignated [[McDonnell Douglas MD-80|MD-80]]) variants, McDonnell Douglas proposed a smaller version of the DC-9 to fill the gap left by the DC-9-30. Dubbed the DC-9-90, it was revealed in February 1983 and was to be some {{convert|25|ft|4|in|m|abbr=on}} shorter than the DC-9-81, giving it an overall length of {{convert|122|ft|6|in|m|abbr=on}}. The aircraft was proposed with a {{convert|17000|lbf|kN|abbr=on}} thrust version of the [[Pratt & Whitney JT8D|JT8D-200]] series engine, although the [[CFM International CFM56|CFM International CFM56-3]] was also considered. Seating  up to 117 passengers, the DC-9-90 was to be equipped with the DC-9's wing with {{convert|2|ft|m|abbr=on}} tip extensions, rather than the more heavily modified increased area of the MD-80. The aircraft had a design range of around {{convert|1500|nmi|km|abbr=on}}, with an option to increase to {{convert|2000|nmi|km|abbr=on}}, and a gross weight of {{convert|112000|lb|abbr=on}}.<ref name="airclaims"/>

The DC-9-90 was designed to meet the needs of the newly deregulated American airline industry.  However, its development was postponed due to the recession of the early 1980s. When McDonnell Douglas did develop a smaller version of the MD-80, it simply shrunk the aircraft to create the MD-87, rather than offer a lower thrust, lighter aircraft that was more comparable to the DC-9-30. With its relatively high MTOW and powerful engines, the MD-87 essentially became a special mission aircraft and could not compete with the all new 100-seaters then being developed. Although an excellent aircraft for specialized roles, the MD-87 often was not sold on its own, tending to rely on its commonality factor, generally limited sales to existing MD-80 operators.<ref name="airclaims"/>

In 1991, McDonnell Douglas revealed that it was again considering developing a specialized 100-seat version of the MD-80, initially named the ''MD-87-105'' (105 seats). It was to be some {{convert|8|ft|m|abbr=on}} shorter than the MD-87, powered with engines in the {{convert|16000|-|17000|lbf|kN|abbr=on}} thrust class.<ref name="airclaims"/>  McDonnell Douglas, Pratt & Whitney, and the China National Aero-Technology Import Export Agency signed a memorandum of understanding to develop a 105-seat version of the MD-80. At the 1991 Paris Airshow, McDonnell Douglas announced the development of a 105-seat aircraft, designated ''MD-95''.<ref name="airclaims"/> The new name was selected to reflect the anticipated year deliveries would begin.<ref name="douglas"/> McDonnell Douglas first offered the MD-95 for sale in 1994.<ref name="douglas">{{cite book|last= Norris |first= Guy |authorlink= |author2=Wagner, Mark |title= Douglas Jetliners |publisher= MBI Publishing |year=1999 |location= |isbn= 0-7603-0676-1}}</ref><ref>Becher 2002, p. 106.</ref>

In early 1994, the MD-95 re-emerged as similar to the DC-9-30, its specified weight, dimensions, and fuel capacity being almost identical. Major changes included a fuselage "shrink" back to {{convert|119|ft|4|in|m|abbr=on}} length (same as the DC-9-30), and the reversion to the original DC-9 wingspan of {{convert|93|ft|5|in|m|abbr=on}}. At this time, McDonnell Douglas said that it expected the MD-95 to become a family of aircraft with the capability of increased range and seating capacity.<ref name="airclaims"/> The MD-95 was developed to satisfy the market need to replace early DC-9s, then approaching 30 years old. The MD-95 was a complete overhaul, going back to the original [[DC-9-10|DC-9-30]] design and applying new engines, cockpit and other more modern systems.<ref name="douglas"/>

In March 1995, longtime McDonnell Douglas customer [[Scandinavian Airlines System]] (SAS) chose the Boeing 737-600 for its 100-seater over the MD-95.<ref name="douglas"/> In October 1995, U.S. new entrant and low cost carrier [[ValuJet Airlines|ValuJet]] signed an order for 50 MD-95s, plus 50 options.<ref name="douglas"/> McDonnell Douglas president [[Harry Stonecipher]] felt that launching MD-95 production on the basis of this single order held little risk, stating that further orders would "take a while longer".<ref>Lopez, Ramon and Guy Norris. [http://www.flightglobal.com/pdfarchive/view/1995/1995%20-%203046.html "MD-95 Launched with ValuJet"]. Flight International, October 25–31, 1995.</ref> The ValuJet order was the only order received for some two years.<ref>Becher 2002, p. 107.</ref>

===Engines===
As first proposed, the MD-95 was to be powered by a {{convert|16500|lbf|abbr=on}} thrust derivative of the JT8D-200 series with the [[Rolls-Royce RB.183 Tay|Rolls-Royce Tay 670]] also considered as an alternative. This was confirmed in January 1992, when [[Rolls-Royce plc|Rolls-Royce]] and McDonnell Douglas signed a [[memorandum of understanding]] concerning the Tay-powered MD-95. McDonnell Douglas said that the MD-95 project would cost only a minimal amount to develop, as it was a direct offshoot of the IAE-powered MD-90.<ref name="airclaims"/>

During 1993 McDonnell Douglas seemed to be favoring a life extension program of the DC-9-30, under the program name DC-9X, to continue its presence in the 100-120 seat market, rather than concentrating on the new build MD-95. In its evaluation of the engines available for the re-engined DC-9X, McDonnell Douglas found that the [[Rolls-Royce BR700|BMW Rolls-Royce BR700]] engine to be the ideal candidate, and on February 23, 1994 the BR700 was selected as the sole powerplant for the airliner.<ref name="airclaims"/>

===Production site===
[[File:Vistas (3563920446).jpg|thumb|The Boeing 717 was manufactured at the company's facility at the [[Long Beach Airport]], California.]]

McDonnell Douglas was planning for MD-95 final assembly to be undertaken in China, as an offshoot of the Trunkliner program, for which McDonnell Douglas had been negotiating to have up to 150 MD-90s built in China. The MD-90 Trunkliner deal was finalized in June 1992, but the contract was for a total of 40 aircraft, including 20 MD-80Ts and 20 -90Ts. The MD-80 has been license built in Shanghai since the 1980s. However, in early 1993, MDC said that it was considering sites outside China, and was later seeking alternative locations for the assembly line. In 1994, McDonnell Douglas sought global partners to share development costs. It also began a search for a low-cost final assembly site. Halla Group in South Korea was selected to make the wings; Alenia of Italy the entire fuselage; Aerospace Industrial Development Corp. of Taiwan, the tail; ShinMaywa of Japan, the horizontal stabilizer; and a manufacturing division of Korean Air Lines, the nose and cockpit.<ref name="airclaims"/>

In an unprecedented move, McDonnell Douglas announced on November 8, 1994 that final assembly would be taken away from the longtime Douglas plant at to Long Beach Airport,  California. Instead, it tapped a modifications and maintenance operation, Dalfort Aviation in Dallas, Texas, to assemble the MD-95. In early 1995, management and unions in Long Beach reached an agreement to hold down wage costs for the life of the MD-95 program and McDonnell Douglas canceled the preliminary agreement with Dalfort.<ref>{{cite web |url=http://community.seattletimes.nwsource.com/archive/?date=20031130&slug=boeingoutsource30 |title=Business & Technology&nbsp;— Parallels in production: 7E7 and 717 - Seattle Times Newspaper|work=nwsource.com}}</ref>

===Rebranding and marketing===
[[File:Airtran 717.jpg|thumb|left|upright|The first and final Boeing 717s were both delivered to AirTran Airways]]

After McDonnell Douglas was acquired by<ref>https://www.nytimes.com/1996/12/16/news/16iht-merge.t_0.html Retrieved 2016-08-08</ref> Boeing in August 1997,<ref>{{cite web|title=Boeing: History -- Higher, Faster, Further&nbsp;— The Boeing Company ... The Giants Merge|url=http://www.boeing.com/history/narrative/n079boe.html|archiveurl=https://web.archive.org/web/20080124221227/http://www.boeing.com/history/narrative/n079boe.html|archivedate=January 24, 2008|accessdate=July 3, 2015|deadurl=yes}}</ref> most industry observers expected that Boeing would cancel development of the MD-95. However, Boeing decided to go forward with the design under a new name, Boeing 717.<ref name=717_innovations/> It appeared that Boeing had skipped the 717 model designation when the [[Boeing 720|720]] and the [[Boeing 727|727]] followed the [[Boeing 707|707]].  But 717 name was the company's model number for the [[KC-135 Stratotanker]] tanker aircraft. 717 had also been used to promote an early design of the 720 to airlines before it was modified to meet market demands. A Boeing historian notes that the Air Force tanker was designated "717-100" and the commercial airliner designated "717-200".<ref>[http://www.seattlepi.com/business/article/Aerospace-Notebook-Orphan-717-isn-t-out-of-1162508.php "Aerospace Notebook: Orphan 717 isn't out of sequence"]. seattlepi.com, December 22, 2004.</ref> The lack of a widespread use of the 717 name left it available for rebranding the MD-95.

At first Boeing had no more success selling the 717 than McDonnell Douglas. Even the original order for 50 was no certainty in the chaotic post-deregulation United States airline market. Assembly started on the first 717 in May 1997.<ref>Flight International Commercial Aircraft Page 45 (September 3, 1997)</ref> The aircraft had its roll out ceremony on June 10, 1998. The 717's first flight took place on September 2, 1998. Following flight testing, the airliner was awarded a type certification on September 1, 1999. Its first delivery was in September 1999 to [[AirTran Airways]], which was the replacement name for Valujet. Commercial service began the following month.<ref name=717h/><ref name=The_Boeing_717/><ref name=isr>{{cite journal|title=Boeing 717 in-service report|journal=Flight International|date=June 5–11, 2001|pages=42–48|url=http://www.flightglobal.com/pdfarchive/view/2001/2001%20-%201986.html|accessdate=July 4, 2015}}</ref>  [[Trans World Airlines]] (TWA) ordered 50 717s in 1998 with an option for 50 additional aircraft.<ref>http://boeing.mediaroom.com/1998-12-09-TWA-to-Continue-Fleet-Renewal-with-Boeing-717-200s</ref>

Boeing's decision to go ahead with the 717 slowly began to pay off. Early 717 operators were delighted with the reliability and passenger appeal of the type and decided to order more. The small Australian [[regional airline]] [[Impulse Airlines|Impulse]] took a long-term lease on five 717s in early 2000<ref>{{cite press release |author=Boeing |title=Impulse airlines first in Australia with 717s |url=http://boeing.mediaroom.com/2000-04-11-Impulse-airlines-first-in-Australia-with-717s |location= |publisher= |agency= |date=April 11, 2000 |access-date=2015-07-08}}</ref> to begin an expansion into mainline routes.<ref>{{cite press release |author=Boeing |title=Impulse Airlines Boeing 717-200 Cockatoo Takes Off For Home |url=http://boeing.mediaroom.com/2001-01-09-Impulse-Airlines-Boeing-717-200-Cockatoo-Takes-Off-For-Home |location= |publisher= |agency= |date=January 9, 2001 |access-date=2015-07-08}}</ref> The ambitious move could not be sustained in competition with the majors, and Impulse sold out to [[Qantas]] in May 2001.<ref>{{cite news|last1=Gaylord|first1=Becky|title=Qantas to Absorb Competitor As Fare War Takes a Victim|url=https://www.nytimes.com/2001/05/02/business/qantas-to-absorb-competitor-as-fare-war-takes-a-victim.html|accessdate=July 1, 2015|work=[[The New York Times]]|date=May 2, 2011}}</ref>

Within a few months, the 717's abilities became clear, being faster than the BAe 146, and achieving a higher dispatch reliability, over 99%, than competing aircraft. Maintenance costs are low: according to AirTran Airways, a [[C check]] inspection, for example, takes three days and is required once every 4,500 flying hours. (For comparison, its predecessor, the DC-9 needed 21 days for a C check.) The new [[Rolls-Royce BR700#BR715|Rolls-Royce BR715]] engine design is relatively easy to maintain. Many 717 operators, such as Qantas, became converts to the plane. Qantas bought more 717s to replace its BAe 146 fleet,<ref>{{cite press release |author=Qantas Corporate Communication |title=QantasLink to Replace BAe146s with Boeing 717s |url=http://www.qantas.com.au/travel/airlines/media-releases/oct-2004/3177/global/en |location=Sydney |publisher= |agency= |date=October 29, 2004 |access-date=2015-07-07}}</ref> and other orders came from [[Hawaiian Airlines]] and [[Midwest Airlines]].<ref name=profit>{{cite press release |last=Lamb |first=Warren |date=May 28, 2002 |title=Boeing 717: Designed for Airline Profitability |url=http://boeing.mediaroom.com/2002-05-28-Boeing-717-Designed-for-Airline-Profitability |location= |publisher= |agency= |access-date=2015-06-30}}</ref>

Boeing actively marketed the 717 to a number of large airlines, including [[Northwest Airlines]], who already operated a large fleet of DC-9 aircraft, and [[Lufthansa]]. Boeing also studied a stretched, higher-capacity version of the 717, to have been called 717-300, but decided against proceeding with the new model, fearing that it would encroach on the company's 737-700 model. Production of the original 717 continued. Boeing continued to believe that the 100-passenger market would be lucrative enough to support both the 717 and the 737-600, the smallest of the Next-Generation 737 series. While the aircraft were similar in overall size, the 737-600 was better suited to long-distance routes, while the lighter 717 was more efficient on shorter, regional routes.<ref name="Janes"/><ref name=713r>{{cite press release|title=Boeing Releases Proposed 717-300X Rendering|url=http://boeing.mediaroom.com/2003-09-18-Boeing-Releases-Proposed-717-300X-Rendering |date=September 18, 2003 |access-date=2015-07-07}}</ref>

===Assembly line and end of production===
In 2001, Boeing began implementing a moving assembly line for production of the 717 and 737.<ref>[http://www.boeing.com/assets/pdf/companyoffices/financial/finreports/annual/01annualreport/BOEING2001AR_all.pdf Boeing 2001 Annual report]</ref> The moving line greatly reduced production time, which led to lower production costs.<ref name=717_innovations>[http://www.boeing.com/news/frontiers/archive/2005/october/i_ca2.html "Going—but far from gone, 717 innovations live on long after production"]. Boeing Frontiers magazine, October 2005,</ref><ref>{{cite news|title=Boeing's 717 to Hit 100th Delivery|url=http://www.aero-news.net/index.cfm?do=main.textpost&id=bacdc4e3-ba73-425a-abe0-607eb6f2ae64|accessdate=July 9, 2015|work=Aero News Network|date=June 12, 2002}}</ref> Following the slump in airline traffic caused by an economic downturn subsequent to the terrorists attacks on [[September 11 attacks|September 11, 2001]], Boeing announced a review of the type's future. After much deliberation, it was decided to continue with production. Despite the lack of orders, Boeing had confidence in the 717's fundamental suitability to the 100-seat market, and in the long-term size of that market.<ref>{{cite web |url=http://www.boeing.com/commercial/717/pf/pf_overview.html |title=Boeing Committed To 717 Program And 100-Seat Market |work=Boeing |accessdate=July 11, 2015 |archiveurl=https://web.archive.org/web/20020217030533/http://www.boeing.com/commercial/717/pf/pf_overview.html |archivedate=February 17, 2002 |deadurl=yes}}</ref> After 19 worldwide 717 sales in 2000, and just 6 in 2001, Boeing took 32 orders for the 717 in 2002, despite the severe industry downturn.<ref name="orders-deliveries"/>

[[File:HAL Kailua-Kona.jpg|thumb|A [[Hawaiian Airlines]] 717 boarding at [[Kona International Airport]], [[Hawaii]] for an inter-island flight, 2004]]
Increased competition from regional jets manufactured by Bombardier and Embraer took a heavy toll on sales during [[Early 2000s recession|the airline slump]] after 2001. [[American Airlines]] acquired TWA and initially planned to continue the 717 order. American Airlines cancelled TWA's order for Airbus A318's, but eventually also cancelled the Boeing 717's that had not yet been delivered.<ref>http://www.flightrun.com/boeing-717/231-twa-trans-world-airlines Retrieved 2016-08-08</ref> The beginning of the end came in December 2003 when Boeing failed to reach a [[United States dollar|US$]]2.7 billion contract from [[Air Canada]], a long term DC-9 customer, who chose the [[Embraer E-Jets]] and [[Bombardier CRJ200]] over the 717.<ref>{{Cite news|url=http://www.cbc.ca/news/business/air-canada-buying-90-jets-from-bombardier-embraer-1.367681|accessdate=June 30, 2015|title=Air Canada buying 90 jets from Bombardier, Embraer|newspaper=[[CBC News]]|publisher=[[Canadian Broadcasting Corporation|CBC]]|date=December 19, 2003}}</ref> In January 2005, citing slow sales, Boeing announced that it planned to end production of the 717 after it had met all of its outstanding orders.<ref>{{Cite press|url=http://boeing.mediaroom.com/2005-01-15-Boeing-to-Recognize-Charges-for-USAF-767-Tanker-Costs-and-Conclusion-of-717-Production|accessdate=July 3, 2015|title=Boeing to Recognize Charges for USAF 767 Tanker Costs and Conclusion of 717 Production|publisher=[[Boeing]]|date=January 15, 2005}}</ref>

The 156th and final 717 rolled off the assembly line in April 2006 for AirTran Airways, which was the 717's launch customer as well as its final customer. The final two Boeing 717s were delivered to customers AirTran Airways and [[Midwest Airlines]] on May 23, 2006.<ref name=The_Boeing_717/><ref name="last 717s"/> The 717 was the last commercial [[airplane]] produced at Boeing's Long Beach facility in Southern [[California]].<ref name="last 717s"/>

===Program milestones===
*Announced: June 16, 1991 at the Paris Air Show as MD-95 program by McDonnell Douglas.<ref>"MDC Steps into 100-seat arena with MD-95". Flight International&nbsp;— Paris Show Report 1991, June 26, 1991, p. 13.</ref>
*Approval to offer: July 22, 1994 McDonnell Douglas got board approval to offer the aircraft.<ref name="airclaims">Airclaims Jet Programs 1995</ref><ref name="FT">Flight International April 1, 1998 - Page 31 "Classic takes shape"</ref>
*First order: October 10, 1995 from ValuJet (later to become AirTran Airways) for 50 firm and 50 options for MD-95s.<ref name=The_Boeing_717/><ref name="FT" />
*Roll out: June 10, 1998 at [[Long Beach, California]].<ref name=The_Boeing_717/><ref>{{cite press release |author=Boeing |title=Boeing Rolls Out First 717-200 Passenger Jet |url=http://boeing.mediaroom.com/1998-06-10-Boeing-Rolls-Out-First-717-200-Passenger-Jet |location= |publisher= |agency= |date=June 10, 1998 |access-date=2015-07-03}}</ref>
*First flight: September 2, 1998.<ref name=717h/><ref name=The_Boeing_717/>
*FAA certification: September 1, 1999.<ref>FAA Type Certificate Data Sheets A6WE</ref>
*EASA (JAA) certification: September 16, 1999.<ref>EASA Type Certificate Data Sheets IM.A.211</ref>
*Entry into service: October 12, 1999 with AirTran Airways on Atlanta-Washington, D.C. (Dulles) route.<ref name=The_Boeing_717/><ref name=isr/>
*100th aircraft delivery: June 18, 2002 to AirTran Airways.<ref>{{cite press release |author=The Boeing Company |title=Boeing Delivers 100th 717-200 Twinjet at Ceremony |url=http://www.prnewswire.com/news-releases/boeing-delivers-100th-717-200-twinjet-at-ceremony-77926387.html |location=Long Beach, CA |publisher=PR Newswire |agency= |date=June 18, 2002 |access-date=2015-07-09}}</ref><ref>{{cite news|title=Boeing delivers 100th 717-200|url=http://www.bizjournals.com/wichita/stories/2002/06/17/daily17.html|accessdate=July 11, 2015|work=Wichita Business Journal|date=June 18, 2002}}</ref><ref>{{cite press release |author=<!--Staff writer(s); no by-line.--> |title=Boeing Delivers 100th 717-200 Twinjet at Ceremony |url=http://boeing.mediaroom.com/2002-06-18-Boeing-Delivers-100th-717-200-Twinjet-at-Ceremony |location= |publisher= |agency= |date=June 18, 2002 |access-date=2015-07-09}}</ref>
*Last delivery: May 23, 2006 to AirTran Airways.<ref name=The_Boeing_717/><ref>[[n:Boeing delivers final 717 to AirTran, ending Douglas era|Boeing delivers final 717 to AirTran, ending Douglas era]]</ref>

==Design==
[[File:N938AT Boeing 717 flight deck.jpg|thumb|left|An AirTran Airways 717-200 flight deck, showing the two-crew cockpit and six LCD units]]

The 717 features a two-crew cockpit that incorporates six interchangeable liquid-crystal-display units and advanced Honeywell VIA 2000 computers. The cockpit design is called Advanced Common [[glass cockpit|Flightdeck]] (ACF) and is shared with the [[MD-10]] and [[McDonnell Douglas MD-11|MD-11]]. Flight deck features include an Electronic Instrument System, a dual Flight Management System, a Central Fault Display System, and Global Positioning System. Category IIIb automatic landing capability for bad-weather operations and Future Air Navigation Systems are available. The 717 shares the same type rating as the DC-9, such that the FAA approved transition courses for DC-9 and analog MD-80 pilots that could be completed in 11 days.<ref name=alpa>{{cite journal |url=http://cf.alpa.org/internet/alp/2000/marb717.htm |title=Flying the B-717-200 |author=Rogers, Ron |journal=Air Line Pilot |date=March 2000  |page=26}}</ref>

In conjunction with Parker Hannifin, MPC Products of Skokie, Illinois designed a fly-by-wire technology mechanical control suite for the 717 flight deck. The modules replaced much cumbersome rigging that had occurred in previous DC-9/MD-80 aircraft. The [[Rolls-Royce BR715]] engines are completely controlled by an electronic engine system ([[FADEC]]&nbsp;— Full Authority Digital Engine Control) developed by [[BAE Systems]], offering improved controllability and optimization.<ref name=alpa/> The engine has significantly lower fuel consumption compared to other engines of the equivalent amount of thrust.<ref>{{cite web|title=BMW Rolls-Royce Power Plant for the Boeing 717|url=http://www.boeing.com/commercial/aeromagazine/aero_05/textonly/ps03txt.html|website=boeing.com|publisher=Boeing|accessdate=20 December 2016}}</ref>

Like its DC-9/MD-80/MD-90 predecessors, the 717 has a 2+3 seating arrangement in the main economy class, providing only one middle seat per row, whereas other [[narrow-body aircraft|single-aisle]] [[twinjet|twin jets]], such as the [[Boeing 737]] family and the [[Airbus A320 family]], often have 3+3 arrangement with two middle seats per row.<ref>{{cite web |url=http://www.boeing.com/commercial/717/pf/pf_interior_seating.html |title=717-200 Seating Interior Arrangements |accessdate=July 13, 2015 |archive-url=https://web.archive.org/web/20011122095503/http://www.boeing.com/commercial/717/pf/pf_interior_seating.html |archive-date=2001-11-22 |deadurl=yes}}</ref><ref>{{cite web |url=http://www.boeing.com/commercial/717/pf/in_cross.pdf |title=Superior Passenger Seating Comfort 717-200 |work=Boeing |accessdate=July 13, 2015 |archive-url=https://web.archive.org/web/20011122095526/http://www.boeing.com/commercial/717/pf/in_cross.pdf |archivedate=November 22, 2001 |deadurl=yes}}</ref> Unlike its predecessors, McDonnell Douglas decided not to offer the MD-95/717 with the [[Boarding (transport)|boarding]] flexibility of aft [[airstairs]], with the goal of maximizing fuel efficiency through the reduction and simplification of as much auxiliary equipment as possible.<ref name=acaps>[http://www.boeing.com/assets/pdf/commercial/airports/acaps/717.pdf "717-200 Airplane Characteristics for Airport Planning"]. Boeing, May 2011. Retrieved: July 3, 2015.</ref>

==Variants==
[[File:QantasLINK 717-200.JPG|thumb|QantasLink Boeing 717 landing at Perth Airport]]

Three initial variants were proposed by McDonnell Douglas in 1993:<ref name="airclaims"/>
*MD-95-30: Baseline aircraft with 100 seats.
*MD-95-30ER: extended range (with additional fuel).
*MD-95-50: a slightly larger aircraft with standard capacity for 122 passengers.

[[File:EZ-A107 (6877840923).jpg|thumb|Turkmenistan 717]]
===Boeing 717 Business Express===
Boeing 717 Business Express was a proposed corporate version of 717-200, unveiled at the EBACE Convention in Geneva, Switzerland in May 2003. Configurable for 40 to 80 passengers in first and/or business class interior (typically, 60 passengers with seat pitch of 52 inch, 132&nbsp;cm). Maximum range in HGW configuration with auxiliary fuel and 60 passengers was 3,140 nmi (5,815&nbsp;km; 3,613&nbsp;mi). The version complements BBJ family.<ref>[http://boeing.mediaroom.com/2003-05-07-Boeing-Introduces-717-Business-Express-at-EBACE-2003 "Boeing Introduces 717 Business Express at EBACE 2003"]. Boeing, May 7, 2003.</ref>

===Undeveloped variants===
'''Boeing 717-100 (-100X)''': Proposed 86-seat version, formerly MD-95-20; four frames (6&nbsp;ft 3 in, 1.9 m) shorter. Renamed 717-100X; wind tunnel tests began in early 2000; revised mid-2000 to eight-frame (12&nbsp;ft 8 in, 3.86 m) shrink. Launch decision was deferred in December 2000 and again thereafter to an undisclosed date. Shelved by mid-2003.<ref name="Janes"/>

'''Boeing 717-100X Lite''': Proposed 75-seat version, powered by Rolls-Royce Deutschland BR 710 turbofans; later abandoned.<ref name="Janes">Jane's All World Aircraft 2005</ref>

'''Boeing 717-300X''': Proposed stretched version, formerly MD-95-50; studies suggest typical two-class seating for 130 passengers, with overall length increased to 138&nbsp;ft 4 in (42.16 m) by addition of nine frames (five forward and four aft of wing); higher MTOW and space-limited payloads weights; additional service door aft of wing; and 21,000&nbsp;lb (93.4&nbsp;kN) BR 715C1-30 engines. AirTran expressed interest in converting some -200 options to this model. Was under consideration late 2003 by Star Alliance Group (Air Canada, Austrian Airlines, Lufthansa and SAS); interest was reported from Delta, Iberia and Northwest Airlines.<ref name="Janes"/><ref name=713r/>

==Operators==
[[File:Boeing 717-23S - EC-KNE.jpg|thumb|A [[Spanair]] 717 landing]]
{{Main|List of Boeing 717 operators}}

As of July 2016, there were 154 Boeing 717-200s in service with [[Delta Air Lines]] (91), [[Volotea]] (19), [[Hawaiian Airlines]] (18), [[QantasLink]] (20), [[Turkmenistan Airlines]] (6).<ref name=FI16>{{cite web |url= https://www.flightglobal.com/asset/12798 |title= World Airliner Census |date= 2016 |publisher= FlightGlobal}}</ref>

Delta Air Lines began leasing [[AirTran Airways]]' 717 in 2013 and was to eventually lease all 88 of the airline's fleet.<ref name=delta_717s>{{cite web |url=http://www.frequentbusinesstraveler.com/2012/05/delta-to-add-boeing-717-aircraft-to-its-fleet/ |title=Delta to Add Boeing 717 Aircraft to its Fleet |work=frequentbusinesstraveler.com }}</ref> In 2015, [[Blue1]] announced it will sell their 717 fleet to Delta Air Lines and Volotea.<ref>{{cite web|url=http://ch-aviation.com/portal/news/35651-finlands-blue-to-offload-b717-fleet-to-volotea-delta |title=Finland's Blue to offload B717 fleet to Volotea, Delta |work=ch-aviation}}</ref> One 717, the prototype, was scrapped.<ref>http://www.airportspotting.com/catching-boeing-prototype-aircraft/ Retrieved 2016-08-08.</ref>

==Accidents and incidents==
As of May 2016, the Boeing 717 had been involved in five [[Aviation accidents and incidents|accidents and incidents]] with no hull-loss accidents and no fatalities.<ref name=717_index>[https://aviation-safety.net/database/types/Boeing-717/index "Boeing 717 type index"]. Aviation-Safety.net, March 4, 2016.</ref><ref name=717_events/> The accidents and incidents included one on-ground collision while taxiing, an emergency landing where the front landing gear did not extend, and one attempted hijacking.<ref name=717_events>[https://aviation-safety.net/database/types/Boeing-717/database "Boeing 717 type list"]. Aviation-Safety.net, May 7, 2016.</ref>
<!--
Only accidents or notable incidents. See [[WP:AIRCRASH]], [[WP:AIRCRASH-TYPEARTICLE]] and  [[WP:WikiProject_Aviation/Aircraft_accidents_and_incidents#Aircraft_articles]] for project guidelines for inclusion. -->

==Specifications==
[[File:MD82v1.0.png|thumb|Comparison of Boeing 717, [[McDonnell Douglas DC-9]], [[McDonnell Douglas MD-90]] and [[McDonnell Douglas MD-80]] series aircraft ]]

{| class=wikitable style="text-align: center; font-size:97%; color:black"
|-
!
! 717-200 <br>Basic Gross Weight
! 717-200 <br>High Gross Weight
|-
!Cockpit crew
|colspan=2| Two
|-
!Seating capacity
|colspan=2| 134 (1-class, maximum)<ref>[http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/Frameset?OpenPage Type Certificate Data Sheet], FAA</ref> <br/>117 (1-class, typical) <br/>106 (2-class, typical)
|-
!Seat pitch
|colspan=2| {{convert|32|in|cm|abbr=on}} (1-class, typical) <br/>{{convert|36|in|cm|abbr=on}} and {{convert|32|in|cm|abbr=on}} (2-class, typical)
|-
! Length
|colspan=2| 124&nbsp;ft 0 in (37.8 m)
|-
! Wingspan
|colspan=2| 93&nbsp;ft 5 in (28.47 m)
|-
! Tail height
|colspan=2| 29&nbsp;ft 1 in (8.92 m)
|-
! Cabin width, external
 |colspan=2| 131.6 in (334.2&nbsp;cm)
|-
! Cabin width, internal
|colspan=2| 123.8 in (314.5&nbsp;cm)
|-
! Empty Weight, typical
| 67,500&nbsp;lb (30,617&nbsp;kg)
| 68,500&nbsp;lb (31,071&nbsp;kg)
|-
! Max. Landing Weight
| 100,000&nbsp;lb (45,362&nbsp;kg)
| 110,000&nbsp;lb (49,898&nbsp;kg
|-
! Max. takeoff weight
| 110,000&nbsp;lb (49,900&nbsp;kg)
| 121,000&nbsp;lb (54,900&nbsp;kg)
|-
! Cargo capacity
| 935&nbsp;ft³ &nbsp;(26.5&nbsp;m³)
| 730&nbsp;ft³ &nbsp;(20.7&nbsp;m³)
|-
! Ceiling	
|colspan=2| {{convert|37100|ft|m|sigfig=3}} maximum altitude <br>{{convert|34100|ft|m|sigfig=3}} typical altitude
|-
! Cruising speed, typical
|colspan=2| [[Mach number|Mach]] 0.77 (504&nbsp;mph, 438&nbsp;knots, 811&nbsp;km/h) <br />at altitude of {{convert|34100|ft|m|sigfig=3}}
|-
! Range, maximum
| 1,430 nmi (2,645&nbsp;km)
| 2,060 nmi (3,815&nbsp;km)
|-
! Fuel capacity
| 3,673 US gal &nbsp;(13,903 L)
| 4,403 US gal &nbsp;(16,665 L)
|-
! Powerplants (2x)
| [[Rolls-Royce BR700|Rolls Royce BR715-A1-30]]
| Rolls Royce BR715-C1-30
|-
! Engine thrust (2x)
| 18,500 [[pound-force|lbf]] (82.3 k[[Newton (unit)|N]])
| 21,000&nbsp;lbf (93.4&nbsp;kN)
|}
Sources: Boeing 717 characteristics<ref>[http://www.boeing.com/commercial/aeromagazine/aero_19/717_characteristics.pdf 717-200 Technical Characteristics]</ref> 717 Airport planning report<ref name=acaps/> Boeing 717 specifications<ref>{{cite web |url=http://www.airlines-inform.com/commercial-aircraft/Boeing-717.html |title=Boeing 717 Specifications |work=airlines-inform.com}}</ref>

==Orders and deliveries==
[[File:Inside a Boeing 717.jpg|thumb|right|upright=1.4|Boeing 717 interior]]

{| class="wikitable" style="font-size:95%;"
|+ '''Orders'''
|- style="background:#007000;"
!'''&nbsp;Total&nbsp;'''
!1995
!1996
!1997
!1998
!1999
!2000
!2001
!2002
!2003
!2004
|-
|155
|42
|0
|0
|41
|0
|21
|3
|32
|8
|8
|}

{| class="wikitable" style="font-size:95%;"
|+ '''Deliveries'''
|- style="background:#069;"
!'''&nbsp;Total&nbsp;'''
!1997
!1998
!1999
!2000
!2001
!2002
!2003
!2004
!2005
!2006
|-
|155
|0
|0
|12
|32
|49
|20
|12
|12
|13
|5
|}
[[File:B717 Orders Deliveries.jpg|500px]]

Sources: Boeing<ref>[http://active.boeing.com/commercial/orders/displaystandardreport.cfm?cboCurrentModel=717&optReportType=AllModels&cboAllModel=717&ViewReportF=View+Report 717 Orders and Deliveries]. Boeing </ref><ref>[http://www.boeing.com/commercial/#/orders-deliveries Orders & Deliveries (search)] Boeing </ref>

==See also==
{{Portal|Aviation|USA}}
{{aircontent
|see also=<!-- other related articles that have not already linked: -->
|related=<!-- designs which were developed into or from this aircraft: -->
* [[McDonnell Douglas DC-9]]
* [[McDonnell Douglas MD-80]]
* [[McDonnell Douglas MD-90]]
|similar aircraft=<!-- aircraft that are of similar Role, Era, and Capability this design: -->
* [[Airbus A318]]
* [[Antonov An-148|Antonov An-148/An-158]]
* [[BAe 146|Avro RJ/BAe 146]]
* [[Boeing 737 Next Generation|Boeing 737-600]]
* [[Bombardier CRJ700#CRJ1000 NextGen|Bombardier CRJ1000]]
* [[Bombardier CSeries]]
* [[Comac ARJ21|Comac ARJ21-900]]
* [[Embraer E-Jet family|Embraer 190/195]]
* [[Fokker 100]]
* [[Sukhoi Superjet 100|Sukhoi Superjet 100-95]]
* [[Tupolev Tu-334]]
* [[Yakovlev Yak-42]]D
|lists=<!-- relevant lists that this aircraft appears in: -->
* [[List of jet airliners]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
* Becher, Thomas. ''Douglas Twinjets, DC-9, MD-80, MD-90 and Boeing 717''. The Crowood Press, 2002. ISBN 1-86126-446-1.

==External links==
{{Commons and category|Boeing 717}}
*[http://www.gizmohighway.com/transport/boeing_717.htm Boeing 717 profile on gizmohighway.com]
*[http://www.planespotters.net/Production_List/Boeing/717/index.php 717 List on planespotters.net]
*[http://www.boeing.com/history/products/717-md-95.page Boeing Historical Snapshot 717/MD-95]

{{Boeing airliners}}
{{Boeing 7x7 timeline}}
{{Douglas airliners}}
{{McDonnell Douglas timeline}}
{{Boeing model numbers}}

[[Category:Boeing aircraft|717]]
[[Category:McDonnell Douglas aircraft]]
[[Category:United States airliners 1990–1999]]
[[Category:Twinjets]]
[[Category:Boeing 717| ]]
[[Category:T-tail aircraft]]