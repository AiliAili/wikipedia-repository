{{Use British English|date=October 2012}}
{{Use dmy dates|date=March 2015}}
'''Bristol Harbour''' is the [[harbour]] in the city of [[Bristol]], England. The harbour covers an area of {{convert|70|acre|ha|1}}. It has existed since the 13th century but was developed into its current form in the early 19th century by installing [[Canal lock|lock gates]] on a [[tidal river|tidal]] stretch of the [[River Avon, Bristol|River Avon]] in the centre of the city and providing a tidal by-pass for the river. It is often called the '''Floating Harbour''' as the water level remains constant and it is not affected by the state of the tide on the river.

[[Netham Lock]] in east Bristol is the upstream limit of the harbour. Beyond the lock is a junction: on one arm the navigable River Avon continues upstream to [[Bath, Somerset|Bath]], and on the other arm is the tidal River Avon. The first {{convert|1|mi|km|1}} of the floating harbour, downstream from Netham Lock to [[Totterdown Basin]], is an artificial [[canal]] known as the [[Bristol Feeder Canal|Feeder Canal]], while the tidal River Avon follows its original route. Downstream of Totterdown Basin, the floating harbour occupies the former natural course of the River Avon, whilst the tidal River Avon flows through an artificial channel known as the [[New Cut (Bristol)|New Cut]]. This separation of the floating harbour and the tidal River Avon allows boats in the harbour to remain floating at low tide, reduces currents and silting and prevents flooding. 

Between [[Bristol Temple Meads railway station]] and [[Hotwells]], the harbour and the River Avon run parallel at a distance of no more than {{convert|0.65|mi|km|1}} apart. Downstream of Bristol Temple Meads railway station, the floating harbour meanders through [[Bristol city centre]], [[Canon's Marsh]] and [[Hotwells]]. At Hotwells, the floating harbour rejoins the tidal River Avon, via a series of locks, and flows into the [[Avon Gorge]].

[[Image:Bristol - St Augustine's Reach.jpg|thumb|right|300px|St Augustine's Reach and [[Pero's Bridge]], during the 2004 Harbour Festival]]

== The harbour today ==
[[Image:Bristol pw from ms.jpg|thumb|the former Bristol Industrial Museum now M Shed.]]
Bristol Harbour was the original [[Port of Bristol]], but as ships and their cargo have increased in size, it has now largely been replaced by [[Dock (maritime)|docks]] at [[Avonmouth]] and [[Royal Portbury Dock|Portbury]]. These are located 7&nbsp;miles (11.3&nbsp;km) downstream at the mouth of the River Avon.

The harbour is now a [[tourism|tourist attraction]] with museums, galleries, exhibitions, bars and [[nightclub]]s. Former workshops and [[warehouse]]s have now largely been converted or replaced by cultural venues, such as the [[Arnolfini]] art gallery, [[Watershed Media Centre|Watershed media and arts centre]], [[M Shed]] museum, [[Antlers Gallery]] and the [[At-Bristol]] science exhibition centre, as well as a number of fashionable apartment buildings. The [[Bristol Harbour Railway]], operated by M Shed, runs between the museum and the [[Create Centre]] on some weekends and bank holidays. Historic boats are permanently berthed in the harbour. These include [[Isambard Kingdom Brunel]]'s {{SS|Great Britain}}, which was the first iron-hulled and [[propeller]]-driven ocean liner;<ref name=becket>{{cite book | last= Becket | first= Derrick | year= 1980 | title= Brunel's Britain | location= Newton Abbott | publisher= [[David & Charles]] | isbn= 0-7153-7973-9}}</ref> and a [[replica]] of the [[Matthew (ship)|''Matthew'']], in which [[John Cabot]] sailed to North America in 1497. The historic vessels of M Shed museum, which include the steam tug ''[[Mayflower (tugboat)|Mayflower]]'', firefloat ''[[Pyronaut]]'' and motor tug ''John King'', are periodically operated.

[[Bristol Ferry Boats]] and [[Number Seven Boat Trips]] operate ferry services along the harbour from near [[Bristol Temple Meads railway station|Temple Meads station]] to the [[Underfall Yard]], serving landing stages in the city centre and close to most of the harbour-side attractions.<ref>{{Cite web
 |url= http://www.bristolferry.com/
 |title=Welcome to the Bristol Ferry
 |work=Bristol Ferry Boat Company
 |accessdate=3 January 2011
}}</ref><ref>{{Cite web
 |url= http://www.numbersevenboattrips.co.uk/home.html
 |title=Number Seven Boat Trips&nbsp;– Home Page
 |work=numbersevenboattrips.co.uk
 |accessdate=3 January 2011
}}</ref> A separate cross-harbour ferry carries pedestrians from the ferry landing at the ''Great Britain'' to the new harbour inlet opposite.<ref>{{Cite web
 |url= https://travelwest.info/ferry/bristol-ferry-timetables-fares
 |title=Bristol Ferry Timetables and Fares
 |work=travelwest.info
 |accessdate=8 November 2016
}}</ref> [[Bristol Packet Boat Trips]] offer regular harbour tours with commentaries and river cruises on the ''[[Tower Belle]]''  up the River Avon to [[Conham]], [[Hanham]] and [[Bath, Somerset|Bath]] and downstream to [[Avonmouth]].<ref>{{Cite web
 |url= http://www.bristolpacket.co.uk/public_trips.html
 |title=Boat Trips in Bristol's Floating Harbour and on the River Avon
 |work=Bristol Packet
 |accessdate=3 January 2011
}}</ref> 

In late July each year, the [[Bristol Harbour Festival]] is held, resulting in an influx of boats, including [[tall ship]]s, [[Royal Navy]] vessels and [[Lifeboat (rescue)|lifeboats]].<ref>{{Cite web
 |url= http://www.bristolharbourfestival.co.uk/
 |title=Bristol Harbour Festival 2011
 |work=Bristol Harbour Festival
 |accessdate=3 January 2011
}}</ref>

== Sections, quays & harbourside features ==
{{bristol harbour map}}

== History of Bristol docks ==
[[File:Bristol Harbour (St Stephen's Church, St Augustine the Less Church, Bristol Cathedral), BRO Picbox-7-PBA-22, 1250x1250.jpg|thumb|left|Black and white etching showing the towers of [[St Stephen's Church, Bristol|St Stephen's Church]], [[St Augustine the Less Church, Bristol|St Augustine the Less Church]] and [[Bristol Cathedral]], published c. 1850. The view shows the historic Harbour with 10 sailing ships and rowing boats before the channel was filled in 1892–1938.]]
Bristol grew up on the banks of the Rivers Avon and [[River Frome, Bristol|Frome]]. Since the 13th century, the rivers have been modified for use as docks including the diversion of the River Frome in the 1240s into an artificial ''deep channel'' known as "Saint Augustine's Reach", which flowed into the River Avon.<ref>{{cite web | title=Picturing the Docks | work=Responses: Andy Foyle | url=http://www.land2.uwe.ac.uk/cusp/foyle.htm | accessdate=28 January 2007}}</ref><ref name = watson>Watson, Sally (1991) ''Secret Underground Bristol''. Bristol: Bristol Junior Chamber. ISBN 0-907145-01-9</ref> Saint Augustine's Reach became the heart of Bristol's docks with its quays and wharfs.<ref name = watson/> The River Avon within the gorge, and the [[River Severn]] into which it flows, has [[tides]] which fluctuate about {{convert|30|ft|m|0}} between high and low water. This means that the river is easily navigable at high-tide but reduced to a muddy channel at low tide in which ships would often run aground. Ships had no option but to be stranded in the harbour for unloading, giving rise to the phrase "[[wikt:shipshape and Bristol fashion|shipshape and Bristol fashion]]" to describe how ships and their secured cargo were capable of taking the strain of repeated strandings on the mud.<ref>{{cite web | title=Ship-shape and Bristol fashion | work=The phrase finder | url=http://www.phrases.org.uk/meanings/ship-shape%20and%20Bristol%20fashion.html | accessdate=25 August 2006}}</ref><ref>{{cite book |last=Wilson |first=Ian |authorlink= |coauthors= |title=John Cabot and the Matthew |year=1996 |publisher=Redcliffe Press |location=Tiverton |isbn=1-900178-20-6 }}</ref>
[[Image:Bristol - Tallship in Cumberland Lock.jpg|thumb|right|upright|A [[tall ship]] in the Cumberland lock, [[Hotwells]], during the 2004 Harbour Festival.]]
As early as 1420, vessels from Bristol were regularly travelling to [[Iceland]] and it is speculated that sailors from Bristol had made landfall in the Americas before [[Christopher Columbus]] or [[John Cabot]].<ref name="Brace">{{cite book |last=Brace |first=Keith |authorlink= |coauthors= |title=Portrait of Bristol |year=1996 |publisher=Robert Hale |location=London |isbn=0-7091-5435-6 }}</ref> After Cabot arrived in Bristol, he proposed a scheme to the king, [[Henry VII of England|Henry VII]], in which he proposed to reach Asia by sailing west across the north Atlantic. He estimated that this would be shorter and quicker than Columbus' southerly route. The merchants of Bristol, operating under the name of the [[Society of Merchant Venturers]], agreed to support his scheme. They had sponsored probes into the north Atlantic from the early 1480s, looking for possible trading opportunities.<ref name="Brace" /> In 1552 [[Edward VI]] granted a Royal Charter to the Merchant Venturers to manage the port.<ref>{{cite web | title=Bristol harbour history | work=Tangaroa Bristol | url=http://www.tangaroa-sailing.com/Harbour%20history.htm | accessdate=26 May 2007}}</ref>

By 1670, the city had 6,000&nbsp;tons of shipping, of which half was used for importing tobacco. By the late 17th century and early 18th century, this shipping was also playing a significant role in the [[slave trade]].<ref name="Brace"/>

=== Construction of the floating harbour ===
[[Image:Cumberland Basin.jpg|thumb|left|The [[Cumberland Basin (Bristol)|Cumberland Basin]]]]

In the 18th century, the docks in [[Liverpool]] grew larger and so increased competition with Bristol for the tobacco trade. Coastal trade was also important, with the area called "Welsh Back" concentrating on [[trow]]s with cargoes from the [[Slate industry in Wales]], [[Rock (geology)|stone]], timber and coal.<ref>{{cite book |last=Pearson |first=Michael |authorlink= |title=Kennet & Avon Middle Thames:Pearson's Canal Companion |year=2003 |publisher=Central Waterways Supplies|location=Rugby |isbn=0-907864-97-X}}</ref>  The limitations of Bristol's docks were causing problems to business, so in 1802 [[William Jessop]] proposed installing a dam and lock at Hotwells to create the harbour. The £530,000 scheme was approved by [[Parliament of the United Kingdom|Parliament]], and construction began in May 1804. The scheme included the construction of the [[Cumberland Basin (Bristol)|Cumberland Basin]], a large wide stretch of the harbour in Hotwells where the Quay walls and bollards have [[listed building]] status.<ref>{{cite web | title=Quay walls and bollards | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=379470 | accessdate=18 August 2006}}</ref>
[[Image:Launch-of-the-SS-GB.jpg|right|frame|Launch of the SS ''Great Britain'', the revolutionary ship of Isambard Kingdom Brunel, at Bristol in 1843]]

The [[New Cut (Bristol)|tidal new cut]] was constructed from [[Netham]] to Hotwells, with another dam installed at this end of the harbour. The [[Bristol Feeder Canal|Feeder Canal]] between [[Bristol Temple Meads railway station|Temple Meads]] and Netham provided a link to the tidal river so that boats could continue upstream to Bath. However, the new scheme required a way to equalise the levels inside and outside the Dock for the passage of vessels to and from the Avon, and bridges to cross the water. Jessop built Cumberland Basin with two entrance locks from the tidal Avon, of width {{convert|45|ft|m|1|abbr=on}} and {{convert|35|ft|m|1|abbr=on}}, and a {{convert|45|ft|m|1}} wide junction lock between the Basin and what became known as the Floating Harbour. This arrangement provided flexibility of operation with the Basin being used as a lock when there were large numbers of arrivals and sailings. The harbour was officially opened on 1 May 1809.<ref name="Farvis">{{cite web | title=The creation of Bristol City docks | work=Farvis | url=http://www.farvis.com/Brunel's%20locks.htm | accessdate=18 August 2006|archiveurl=https://web.archive.org/web/20060517082618/http://farvis.com/Brunel's%20locks.htm|archivedate=17 May 2006}}</ref>
[[Image:SS Great Britain.jpg|thumb|{{SS|Great Britain}}.]]
[[William Patterson (engineer)|Patterson's]] yard within the harbour was used for the construction of many ships notably Brunel's {{SS|Great Western}} in 1838 and the {{SS|Great Britain}} in 1843.<ref name=becket/> They were some of the largest ships to have been built at the time,<ref name=becket/> and ironically hastened the decline of the city docks by proving the feasibility of large ships.<ref name="Brace"/> The SS ''Great Britain'' was to be towed away from her builders, to have her 1,000 [[horsepower|hp]] engines and interior fitted out on the [[River Thames]],<ref name=becket/> but her {{convert|48|ft|m|1|abbr=on}} beam was too big to pass through the lock. Thus the SS ''Great Britain'' was moored in the Floating Harbour until December 1844, before proceeding into Cumberland Basin after coping stones and lock gate platforms were removed from the Junction Lock.<ref name="Farvis" /> At one time there were dozens of Bristol shipyards, the largest in the harbour being [[Hilhouse]], who became [[Charles Hill & Sons]] in 1845.<ref>Farr, Graeme (1977). ''Shipbuilding in the Port of Bristol'' National Maritime Museum Maritime Monographs and Reports. p3</ref>

=== 19th century improvements ===
The harbour cost more than anticipated and high rates were levied to repay loans, reducing any benefit the new harbour had at drawing companies back from Liverpool.<ref name="Brace"/> In 1848 the city council bought the docks company to force down the rates. They employed Isambard Kingdom Brunel to make improvements, including new lock gates, a [[dredger]] and sluice gates designed to reduce [[siltation]].

By 1867, ships were getting larger and the meanders in the river Avon prevented boats over {{convert|300|ft|m|0|abbr=on}} from reaching the harbour. A scheme to install a much larger lock at [[Avonmouth]] to make the entire river a floating harbour, and to straighten the sharper bends, was dropped after work began on the much cheaper docks at Avonmouth and [[Portishead, Somerset|Portishead]]. The present entrance lock was designed by Thomas Howard and opened in July 1873. This has a width of {{convert|62|ft|m|1|abbr=on}} and is the only entrance lock now in use at the City Docks.<ref name="Farvis" />

From 1893 until 1934 the [[Clifton Rocks Railway]] provided an underground [[Funicular|funicular railway]] link from the western end of the harbour, which is close to the locks, into [[Clifton, Bristol|Clifton]].<ref name = watson/>

==== Railways ====
The original [[Bristol Harbour Railway]] was a joint venture by the GWR and the Bristol and Exeter Railway, opened in 1872 between Temple Meads and the Floating Harbour. Its route included a tunnel under [[St Mary Redcliffe]] church and a steam-powered bascule bridge over the entrance locks at Bathurst Basin. In 1876 the railway was extended by half a mile to Wapping Wharf. In 1906, new branches from the south via the [[Ashton Avenue Bridge|Ashton Avenue]] [[swing bridge]] were built to Canons Marsh on the north side of the Floating Harbour and to Wapping via a line alongside the New Cut. A Coal Concentration Depot owned by Western Fuels was established at Wapping Wharf just behind the Industrial Museum but this closed in 1987.
[[File:Western Fuels Diesel Locomotive at Bristol Docks Summer 1981.jpg|thumb|Western Fuels Diesel locomotive shunting coal trucks at the Coal Concentration Depot in 1981]]

==== Underfall Yard ====
{{Main|Underfall Yard}}
[[Image:Underfall Yard.jpg|thumb|Underfall Yard]]
The docks maintenance facility was established on the land exposed by the damming of the river to construct the harbour and remains sited at this location to the present day. William Jessop had created a weir in the dam at Underfall to allow surplus water to flow back into the New Cut, this was known as the 'Overfall'. By the 1830s, the Floating Harbour was suffering from severe silting. Isambard Kingdom Brunel was, however, able to devise a solution to this problem. In place of the Overfall he advised better use of three shallow [[sluice]]s and one deep scouring sluice between the harbour and the New Cut, together with a dredging vessel. This drag boat would scrape the silt away from the quay walls. When the deep sluice opened at low tide, a powerful undertow sucked the silt into the river to be carried away on the next tide. The shallow sluices enabled adjustment of the dock water level according to weather conditions.<ref>{{cite web | title=Underfall Boatyard history | url=http://www.underfallboatyard.co.uk/history.htm | accessdate=18 August 2006}}</ref>

Several old buildings, which date from the 1880s, remain at Underfall Yard and have [[listed building]] status. The [[octagon]]al brick and [[terracotta]] chimney of the [[Hydraulic engine house, Bristol Harbour|hydraulic engine house]] dates from 1888, and is grade II* listed,<ref>{{cite web | title=Chimney of hydraulic engine house | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380745 | accessdate=18 August 2006}}</ref> as is the hydraulic engine house itself. It is built of red brick with a slate roof and contains pumping machinery, installed in 1907 by Fullerton, Hodgart and Barclay of [[Paisley, Renfrewshire|Paisley]], which powers the dock's hydraulic system of cranes, bridges and locks.<ref>{{cite web | title=hydraulic engine house | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380746 | accessdate=18 August 2006}}</ref> The former [[pattern (casting)|pattern-maker's]] shop and stores date from the same period and are grade II listed,<ref>{{cite web | title=former pattern-maker's shop and stores | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380743 | accessdate=18 August 2006}}</ref> as are the Patent slip and quay walls.<ref>{{cite web | title=Patent slip and quay walls | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380748 | accessdate=18 August 2006}}</ref>

==== Warehouses ====
[[Image:20050319 046 bristol arnolfini.jpg|thumb|left|The [[Arnolfini]].]]
A large number of [[warehouses]] were built around the harbour for storage and trade. Many survive today and some are being converted into apartment blocks but many have been demolished as part of the regeneration of the area.

One which has survived is the A Bond Tobacco Warehouse, which was built in 1905 and was the first of the three brick built [[bonded warehouse]]s in the Cumberland Basin, and is a grade II listed building.<ref>{{cite web | title=A Bond Tobacco Warehouse | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=379481 | accessdate=18 August 2006}}</ref>

[[B Bond Warehouse]] dates from 1908 and was the first in Britain to use Edmond Coignet's reinforced concrete system.<ref>{{cite web | title=B Bond Tobacco Warehouse | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380560 | accessdate=8 September 2007}}</ref> It is now used by [[Bristol City Council]] and houses [[Bristol Archives]], the Create centre and council offices.

[[Robinson's Warehouse, Bristol|Robinson's Warehouse]] built in 1874 by [[William Bruce Gingell]],<ref>{{cite web | title=Robinson's Warehouse  | work=Looking at Buildings | url=http://www.lookingatbuildings.org.uk/default.asp?Document=3.C&Image=784&gst= | accessdate=19 May 2007|archiveurl = https://web.archive.org/web/20070927235234/http://www.lookingatbuildings.org.uk/default.asp?Document=3.C&Image=784&gst= |archivedate = 27 September 2007|deadurl=yes}}</ref> and [[Granary, Bristol|the Granary]]<ref>{{cite web | title=The Granary and attached area walls  | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380814 | accessdate=19 May 2007}}</ref>  on Welsh Back are examples of the [[Bristol Byzantine]] style with coloured brick and [[Moorish architecture|Moorish]] arches.

The [[Arnolfini]] art gallery occupies Bush House, a 19th-century Grade II* [[listed building|listed]] tea warehouse.<ref>{{cite web | title=Bush House | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=380204 | accessdate=18 August 2006}}</ref> and the [[Watershed Media Centre]] occupies another disused warehouse.

=== 20th century improvements ===
[[File:Floating Harbour from Bristol Bridge - geograph.org.uk - 407855.jpg|thumb|left|City Docks looking West from near Redcliffe Bridge in 1971]]
In 1908, the [[Avonmouth Docks|Royal Edward Dock]] was built in [[Avonmouth]] and in 1972 the large deep water [[Royal Portbury Dock]] was constructed on the opposite side of the mouth of the Avon, making the Bristol City Docks in the floating harbour redundant as a freight dock. In 1977 Charles Hill & Sons, the last shipbuilder at the Albion Yard, closed after delivering the 1541 tonne beer [[Tanker (ship)|tanker]] ''Miranda Guinness''.<ref>Farr, Graeme (1977). ''Shipbuilding in the Port of Bristol'' National Maritime Museum Maritime Monographs and Reports. p56</ref> Part of the yard reopened in 1980 when [[Abels Shipbuilders]] began operating, who still produce a steady stream of small ferries, survey vessels, tugs and other craft.<ref>[http://www.suhner-abrasive-expert.com/domains/suhner-abrasive-expert_com/data/free_docs/SUHNER%20Sparks%20March-April% Shuner Sparks Issue 16&nbsp;– March/April 2007]</ref>

[[Amey plc|Amey Roadstone]] (formerly T R Brown and Holms Sand & Gravel) sand dredgers worked from Poole's Wharf in [[Hotwells]] until 1991. Occasionally coastal trading vessels enter the Cumberland Basin to be loaded with large steel silos manufactured by Braby Ltd at their nearby Ashton Gate works.<ref>{{cite web | title=BRABY MAKE LIGHT WORK OF MAMMOTH SILO DELIVERY | work=Braby Ltd | url=http://www.braby.co.uk/Whats_New_Press_Release.htm | accessdate=28 January 2007|archiveurl = https://web.archive.org/web/20070203023113/http://www.braby.co.uk/Whats_New_Press_Release.htm |archivedate = 3 February 2007|deadurl=yes}}</ref>

The old Junction Lock [[swing bridge]] is powered by water pressure from the Underfall Yard [[Hydraulics|hydraulic]] engine house at {{convert|750|psi|bar|0|abbr=on|lk=on}}. The new Plimsoll Bridge, completed in 1965, has a more modern electro-hydraulic system using oil at a pressure of {{convert|4480|psi|bar|0|abbr=on}}.<ref name="Farvis"/>

== Regeneration of the harbourside ==
[[Image:Watersheandbridge.jpg|thumb|[[Watershed Media Centre|The Watershed]] and [[Pero's Bridge]].]]
Since the 1980s, millions of pounds have been spent [[Urban renewal|regenerating]] the harbourside.
In 1999, [[Pero's Bridge|Pero's footbridge]] was constructed, linking the [[At-Bristol]] exhibition with Bristol tourist attractions. In 2000, the At-Bristol centre opened on semi-derelict land at [[Canon's Marsh]] and some of the existing Grade II [[listed building]]s were refurbished and reused. It was funded with £44.3 million from the [[National Lottery (United Kingdom)|National Lottery]], the [[Millennium Commission]], [[South West of England Regional Development Agency]], and a further £43.4 million from Bristol City Council and commercial partners, including [[Nestlé]].<ref name="BCCdev">{{cite web | title=Development areas in Bristol | work=Bristol City Council | url=http://www.bristol.gov.uk/ccm/content/Business/Business-support-and-Advice/development-areas.en | accessdate=27 May 2007|archiveurl = https://web.archive.org/web/20070930020307/http://www.bristol.gov.uk/ccm/content/Business/Business-support-and-Advice/development-areas.en |archivedate = 30 September 2007|deadurl=yes}}</ref> Private investors are also constructing [[studio apartment]] buildings.<ref name="BERI">{{cite web | title=Bristol&nbsp;– Harbourside Management | publisher=Cullinan | url=http://cullinanstudio.com/project/bristol_harbourside_masterplan | accessdate=22 March 2015}}</ref>

The regeneration of the [[Canon's Marsh]] area is expected to cost £240 million.<ref name="BCCdev" /> [[Crest Nicholson]] were the lead [[Real estate developer|developers]], constructing 450 new flats, homes and waterside offices.<ref>{{cite web | title=New Harbourside development given the green light | work=BBC News | url=http://www.bbc.co.uk/bristol/content/news/2001/10/18/news1/harbourside.shtml | accessdate=27 May 2007}}</ref> It is being carried out under the guidance of The Harbourside Sponsors’ Group, which is a partnership between the City Council, key stakeholders, developers, businesses, operators and funders.<ref name="BERI"/>

The [[Cumberland Basin (Bristol)|Cumberland Basin]] & Baltic Wharf are used by a variety of small boats by [http://www.balticwharfsailing.com/ Baltic Wharf Sailing Club] and is surrounded by tourist attractions. The old hydraulic pumping station has been converted into a [[public house]] and is a Grade II listed building.<ref>{{cite web | title=The Pump House Public House | work=Images of England | url=http://www.imagesofengland.org.uk/details/default.aspx?id=379474 | accessdate=18 August 2006}}</ref>

There are three active boat building companies in the harbour: [[Abels Shipbuilders]], [[Bristol Classic Boat Company]] and the [[Slipway Co-operative]] at the [[Underfall Yard]].

== Events ==
[[Image:Bristol harbour festival.jpg|thumb|left|Bristol harbour festival]]
Bristol Harbour hosts the [[Bristol Harbour Festival]] in July of each year, attended by [[tall ship]]s and hundreds of ships and boats of all kinds. About 200,000 visitors view the boats, and watch live music, street performances and other entertainments.

In 1996, the harbour was the setting for the first [[International Festival of the Sea, 1996|International Festival of the Sea]]. A larger version of the annual harbour festivals, this was attended by many tall ships, including the ''[[Eye of the Wind]]'', ''[[Pride of Baltimore]]'', ''[[Rose (ship)|Rose]]'', ''[[Kaskelot (tall ship)|Kaskelot]]'' and ''[[Earl of Pembroke (ship)|Earl of Pembroke]]''. The key theme was [[John Cabot]]'s pioneering voyage of discovery to the Americas and a replica of Cabot's ship, the ''[[Matthew (ship)|Matthew]]'', was dedicated prior to its reenactment of Cabot's voyage the following year.<ref name=eotw>{{cite web | url = http://www.tallshipstales.de/90s/Summer_1996.php | title = Eye of The Wind&nbsp;– Newsletter | publisher = WebRing Inc | accessdate = 31 July 2007 }}</ref> In 2009, the 200th anniversary of the Floating Harbour was commemorated with a series of celebratory events.<ref>{{cite news |url= http://news.bbc.co.uk/local/bristol/hi/people_and_places/history/newsid_8027000/8027396.stm |title=Bristol harbour reaches 200 years |first= |last=Staff |work=[[BBC News]] |date=1 May 2009 |publisher=[[British Broadcasting Corporation|BBC]] |location=London |accessdate=2 July 2011}}</ref>

{{-}}

== See also ==
{{Portal|UK Waterways}}
* [[Port of Bristol]]
* [[Bristol Harbour Festival]]
* [[Bristol Marina]]

== References ==
{{Reflist|colwidth=35em}}

== External links ==
{{Commons category|Bristol Harbour}}
* [http://www.bristol-city.gov.uk/ccm/content/Transport-Streets/Marine-waterway-services/harbour-events-and-attractions.en Bristol City Council: Harbour events and attractions]
* [https://web.archive.org/web/20120707041709/http://www.bristol.gov.uk:80/page/marine-and-waterway-services Bristol City Council: Marine and waterway services]
* Maps of Bristol Harbour c1900&nbsp;– [http://www.somerset.gov.uk/archives/Maps/OS62htm/0602.htm West], [http://www.somerset.gov.uk/archives/Maps/OS62htm/0603.htm East]
* [http://www.bristolfloatingharbour.org.uk/ Bristol Floating Harbour Primary sources from Bristol Museums and Libraries]

{{Coord|51.45|-2.60|type:landmark_region:GB|format=dms|display=title}}
{{Transport in Bristol}}
{{UK Docks}}

{{good article}}

[[Category:Bristol Harbourside]]
[[Category:Geography of Bristol]]
[[Category:Buildings and structures in Bristol]]
[[Category:Culture in Bristol]]
[[Category:Grade II listed buildings in Bristol]]
[[Category:Grade II* listed buildings in Bristol]]
[[Category:History of Bristol]]
[[Category:Ports and harbours of the Bristol Channel]]
[[Category:Port of Bristol]]
[[Category:River navigations in the United Kingdom]]
[[Category:Transport in Bristol]]
[[Category:Tourist attractions in Bristol]]
[[Category:Works of Isambard Kingdom Brunel]]
[[Category:Sports venues in Bristol]]