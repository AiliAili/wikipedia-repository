{{Infobox journal
| title = Concurrent Engineering: Research and Applications
| cover = [[Image:Concurrent Engineering cover.jpg]]
| editor = Biren Prasad
| discipline = [[Computer science]]s, [[industrial design]], [[manufacturing engineering]], [[systems science]]
| former_names = 
| abbreviation = Concurr. Eng. Res. Appl.
| publisher = [[Sage Publications]]
| country =
| frequency = Quarterly
| history = 1993-present
| openaccess = 
| license = 
| impact = 0.851
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201578/title
| link1 = http://cer.sagepub.com/content/current
| link1-name = Online access
| link2 = http://cer.sagepub.com/content/by/year
| link2-name = Online archive
| link3 = http://icei.weebly.com/
| link3-name = CE Institute membership
| JSTOR = 
| OCLC = 44708471
| LCCN = 93658619
| CODEN = CRAPEM
| ISSN = 1063-293X
| eISSN = 1531-2003
}}
'''''Concurrent Engineering: Research and Applications''''' is a quarterly [[peer-reviewed]] [[scientific journal]] that publishes research on computer-aided [[concurrent engineering]]. The journal deals with all basic tracks that enable CE, including: information modeling, teaming & sharing, networking & distribution, planning & scheduling, reasoning & negotiation, collaborative decision making, and organization and management of CE. This journal is a member of the Committee on Publication Ethics (COPE). The journal was established in 1993 and is published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in over two-dozen places, including [[Scopus]], [[Science Citation Index]]. The list of indexing at present (2015) includes:
•Abstracts in New Technologies & Engineering
•Abstracts in New Technology & Engineering
•Aluminium Industry Abstracts
•Business Source Corporate
•Civil Engineering Abstracts
•Compendex
•CompuMath Citation Index
•Computer Info. & Systems Abstracts
•Computer Literature Index
•Computer Science Index
•Current Contents / Engineering, Comp, & Tech
•Current Contents, Engineering, Comp, & Tech
•Ergonomics Abstracts
•Health Source
•ISI Alerting Services
•Inspec
•Mechanical & Transportation Engineering Abstracts
•Mechanical Engineering Abstracts
•Metal Abs./METADEX
•Metals Abs./METADEX
•SciSearch
•SciVal
•Science Citation Index
•Scopus
•Vocational Search

According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 0.851, ranking it 78th out of 102 journals in the category "Computer Science, Interdisciplinary Applications",<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Computer Science, Interdisciplinary Applications |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 31st out of 40 journals in the category "Engineering, Manufacturing",<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Manufacturing |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> and 56th out of 81 journals in the category "Operations Research & Management Science".<ref name=WoS3>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Operations Research & Management Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==Journal Publisher link==
* {{Official website|http://www.sagepub.com/journals/Journal201578/title}}

==Concurrent Engineering Institute link==
* {{Official website|http://icei.weebly.com/}}

==Journal Author Gateway==
* {{Official website|http://uk.sagepub.com/en-gb/eur/page/journal-author-gateway}}

==Journal Submission Guidelines==
* {{Official website|https://us.sagepub.com/en-us/nam/concurrent-engineering/journal201578#submission-guidelines}}
Concurrent Engineering is hosted on ScholarOne™ Manuscripts, a web-based online submission and peer review system - SAGE track. Please read the Manuscript Submission guidelines below, and then simply visit 
* {{official website|http://mc.manuscriptcentral.com/cera}}

[[Category:Computer science journals]]
[[Category:Engineering journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1993]]
[[Category:English-language journals]]
[[Category:SAGE Publications academic journals]]