{{Infobox person
| name          = Isssam Darwish
| image         = <!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt           = 
| caption       = 
| birth_name    = <!-- only use if different from name -->
| birth_date    = <!-- {{Birth date and age|YYYY|MM|DD}} or {{Birth-date and age|birth date†}} for living people supply only the year unless the exact date is already widely published, as per [[WP:DOB]] -->
| birth_place   = 
| death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|death date†|birth date†}}  -->
| death_place   = 
| nationality   = [[Lebanon|Lebanese]]
| other_names   = 
| occupation    = Businessman
| years_active  =
| known_for     = 
| notable_works =
}}

'''Issam Darwish''' (in [[Arabic language|Arabic]] عصام درويش) is a Lebanese-Nigerian [[entrepreneur]] with over 20 years of experience in the [[telecommunications]] industry.<ref>Nsehe, Mfonobong. [http://www.forbes.com/sites/mfonobongnsehe/2014/11/03/nigerian-multi-millionaire-tycoon-issam-darwish-raises-2-6-billion-for-telecom-towers/ "Nigerian Multi-Millionaire Tycoon Issam Darwish Raises $2.6 Billion For Telecom Towers"], ''Forbes'', 3 November 2014. Accessed 3 September 2015.</ref><ref name=bloomberg>[https://www.bloomberg.com/research/stocks/people/person.asp?personId=61132347&ticker=IHS:NL&previousCapId=4841290&previousTitle=Investec%20Asset%20Management%20Limited "Issam Darwish"], ''Bloomberg [profile]''. Accessed 3 September 2015.</ref> He is the co-founder, Executive Vice-Chairman and CEO of IHS Towers, the largest mobile telecommunications infrastructure provider in Africa, Europe and the Middle East.<ref>Smith, Matt. [http://www.reuters.com/article/2014/11/03/us-nigeria-telecommunications-ihs-idUSKBN0IN0DN20141103 "Africa phone tower firm IHS raises $2.6 billion"], ''Reuters'', November 3, 2014. Accessed 27 July 2015.</ref> He is a frequent speaker at industry and academic events such as the 2015 Wendel Investor Forum and the 2016 Economist Nigeria Summit, as well as on international broadcast channels.<ref>[http://naija247news.com/2016/01/the-economist-events-nigeria-summit-to-take-place-on-march-7th-and-8th-2016/ " The Economist Events’ Nigeria Summit to take place on March 7th and 8th 2016"], ''Naija 247 News''. Accessed 2 February 2016.</ref><ref>[https://www.youtube.com/watch?v=S8DmzhahbUQ&feature=youtu.be "IHS CEO Issam Darwish Speaks at the Wendel Investors Forum December 2015"] [video], 3 December 2015. Accessed 13 January 2016.</ref> As of 2014, the US Forbes wealth list estimated his net worth to be [[List of Lebanese by net worth|$200 million]].<ref name=interview />

==Early life and education==

Issam was brought up and educated in [[Beirut]] during the [[Lebanese Civil War]].<ref name=interview>Darwish, Issam. Interviewed by Owolawi, Abisola. [http://www.slideshare.net/ihstowers/issam-darwish-cofounder-of-ihs-towers-speaks-to-forbes-africa "Bus or Food? The First Crossroad on the Path to Being a Millionaire"] [pdf], ''Forbes Africa'', March 2014. Accessed 27 July 2015.</ref> He went on to graduate with distinction from the [[American University of Beirut]], where he majored in Computer and Communications Engineering.<ref name=feaweb>[https://feaweb.aub.edu.lb/feasac/11/PDFs/Issam_Darwish.pdf "Issam Darwish"][pdf]. Accessed 29 July 2015.</ref> He also holds a Certificate in Quality Control from [[TeliaSonera|SONERA]] (previously Telecom Finland) and one in Network Management from Mobilcom, Austria.<ref name=bloomberg />

==Business career==

He began his professional career in 1992, in Beirut, after joining MCI as an executive.<ref name=bloomberg />  At the time, the company was one of the world’s largest telecoms carriers. He then joined Libancell, now known as MTC Touch, where he assumed a leading role in establishing the first Lebanese mobile network.<ref>Clémençot, Julien. [http://www.jeuneafrique.com/6708/economie/issam-darwish-a-plus-d-une-tour-dans-son-sac/ "Issam Darwish a plus d’une tour dans son sac"], ''Jeune Afrique'', 24 September 2014. Accessed 3 September 2015.</ref> Subsequently, in 1998, he was appointed Deputy Managing Director of Motophone, Nigeria’s first GSM operator.<ref>[http://www.reuters.com/finance/stocks/companyOfficers?symbol=IHS.LG "IHS Nigeria PLC"], ''Reuters''. Accessed 3 September 2015.</ref>

Following the Nigerian government’s 2001 plan to privatize its telecommunications industry, he set up a mobile infrastructure company, [[IHS Towers]], which he has led since then.<ref name=telfinance>Darwish, Issam. Interviewed by Landon, Claire. [https://cdn.shopify.com/s/files/1/0798/3123/files/TelecomFinance240.pdf?9053195710412710704 "Q&A:  Issam Darwish, IHS Towers co-founder"], ''Telecom Finance'', December 2015. Accessed 13 January 2016.</ref><ref>Rice, Xan. [http://www.ft.com/cms/s/0/ad1b2780-30ad-11e2-bd24-00144feab7de.html?siteedition=uk#axzz3kgzf1w5P "IHS: Local knowledge is important in fulfilling towering ambitions"], ''The Financial Times'', 27 November 2012. Accessed 3 September 2015.</ref> Under his tenure as CEO, the company has been named one of the largest equity fundraisers in Africa, as well as one of the overall largest fundraisers of the past decade.<ref>Thomas, Daniel; Blas, Javier. [http://www.ft.com/cms/s/0/917836a0-611e-11e4-8f87-00144feabdc0.html?siteedition=uk#axzz3jj3Nutiq "IHS in biggest African fund raising since crisis"], ''Forbes'', 2 November 2014. Accessed 4 September 2015.</ref><ref>[http://www.tmtfinance.com/news/ihs-ceo-issam-darwish-talks-towers "IHS CEO Issam Darwish talks towers"], ''TMT Finance'', 26 November 2015. Accessed 13 January 2016.</ref> Moreover, IHS Towers has won a series of awards and nominations for its rapid growth and high-profile tower transactions, such as the 2015 African Company of the Year at the CEO Forum Awards and Africa Investor’s 2015 ICT Project Developer of the year, as well as the Middle East and Africa Deal of the Year at the Telecomfinance Awards in 2013 and 2015, and 2014 EMEA Finance’s prize for the Best Telecommunications Deal in EMEA region.<ref>[http://www.slideshare.net/AfricaCEOForum/rapport-de-couverture-mdiatique-africa-ceo-forum-2015 "Rapport de couverture médiatique AFRICA CEO FORUM 2015 "] [pdf], ''Africa CEO Forum'', 20 May 2015. Accessed 22 October 2015.</ref><ref>[http://www.towerxchange.com/ihs-africa-awarded-middle-east-africa-deal-of-the-year/ "IHS Africa awarded Middle East & Africa Deal of the Year "], ''TowerXchange'', 25 February 2013. Accessed 22 October 2015.</ref><ref>[http://www.biztechafrica.com/article/ihs-towers-awarded-telecom-finance-deal-year/9756/#.Vij_KSuoMTZ "IHS Towers awarded Telecom Finance Deal of the Year"], ''Biztech Africa'', 20 February 2015. Accessed 22 October 2015.</ref><ref>[http://www.africainvestor.com/article.asp?id=13211 "Africa’s infrastructure project development leaders recognised at Ai Project Developer Awards"], ''Africainvestor'', 4 June 2015. Accessed 22 October 2015.</ref><ref>[https://www.emeafinance.com/live/magazine/actual-edition/2634-project-finance-awards-emea-winners "Project Finance Awards - EMEA winners"], ''EMEA Finance'', 29 April 2015. Accessed 22 october 2015.</ref>

Issam is a supporter of green energy research and has overseen the roll out of renewable energy solutions throughout the continent.<ref>Sikhakhane, Zweli. [http://www.biznisafrica.co.za/ihs-holding-acquires-mtn-towers-in-rwanda-zambia/ "IHS Holding acquires MTN towers in Rwanda, Zambia"], ''Biznis Africa'', 2 May 2014. Accessed 4 September 2015.</ref> He is also a proponent for the development of the mobile banking industry across the continent and in Nigeria in particular.<ref>Darwish, Issam. Interviewed by Karlsson, Markus. [http://www.france24.com/en/20111125-africa-s-mobile-phone-boom-issam-darwish-ihs-nigeria "Issam Darwish, CEO of IHS Africa"], ''France 24'', 28 November 2011. Accessed 4 September 2015.</ref>

In 2015, he was nominated Business Leader of the Year for West Africa as part of the All Africa Business Leaders Awards in collaboration with CNBC Africa.<ref>[http://www.aablawards.com/west-africa.php "AABLA: All Africa Business Leaders Awards"], ''All Africa Business Leaders Awards''. Accessed 14 October 2015.</ref><ref>[http://www.mytvnews.co.za/2015-all-africa-business-leaders-awards-the-nominees-are/ "2015 All Africa Business Leaders Awards: The nominees are…"], ''My TV News'', 7 September 2015. Accessed 14 October 2015.</ref> In 2016, he won the award for his founding and continued leadership of IHS Towers.<ref>{{cite web|title=IHS Towers CEO wins AABLA award|url=http://guardian.ng/technology/ihs-towers-ceo-wins-aabla-award/|publisher=The Guardian|date=October 26, 2016}}</ref>

==Other ventures==
Issam has founded additional businesses in the US and Middle East such as Vorex, a software provider for small enterprises across the US.<ref name=interview /> He is also the Founder and President of Singularity Investments, Dar Properties and Dar Telecom.<ref>[http://singularityinvest.com "Africa"], ''Singularity Invest''. Accessed 3 September 2015.</ref><ref>[http://www.dartelecom.com/aboutus/Presidents-Profile "President's Profile"], ''Dar Telecom Consulting''. Accessed 3 September 2015.</ref>

Issam is involved in setting up incubator programs for aspiring tech entrepreneurs in Lagos and oversees local community projects throughout Nigeria.<ref name=interview /> He has been a financier behind the establishment of educational facilities in underserved areas throughout Africa and is a believer of the fact that increased employment opportunities is the key to boosting Africa’s economy.<ref name=telfinance />

In September 2015, he served on an entrepreneur judging panel for She Leads Africa, a venture which invests in promising women entrepreneurs from across the continent.<ref>Adebiyi, Deola. [http://www.ngrguardiannews.com/2015/09/guaranty-trust-bank-gtbank-joins-she-leads-africa-for-live-entrepreneur-showcase-on-september-26-2015/ "Guaranty Trust Bank (GTBank) Joins She Leads Africa for Live Entrepreneur Showcase"], ''The Guardian'', 21 September 2015. Accessed 14 October 2015.</ref><ref>[http://www.cnbcafrica.com/news/western-africa/2015/03/09/yasmin-belo-osagie-billionaires-africa/ "How two young West African women are creating Africa's next billionaires"], ''CNBC Africa'', 9 March 2015. Accessed 14 October 2015.</ref> In the same year, he was asked to join the Africa 2.0 Advisory Board alongside individuals such as [[Jesse Jackson|Reverend Jesse Jackson]], [[Joaquim Chissano]], the former President of Mozambique, and Samba Bathily.<ref>[http://africa2point0.org/index.php?option=com_content&view=article&id=220&Itemid=402&lang=en "Africa 2.0 - Advisory Board"], ''Africa 2.0''. Accessed 13 January 2016.</ref> Africa 2.0 is a pan-African project aimed towards driving sustainable growth throughout the continent.

== Personal life ==

Issam Darwish is also a Nigerian citizen and holds the chieftain title “The Adimula of Erin Ile” in the state of Kwara.<ref name=interview />

He counts Raphael Udeogu, the former managing director of Motorola’s Nigerian division, and Bashir El-Rufai, the former Managing Director of Intercellular Nigeria Limited,  amongst his personal mentors.<ref name=interview />

Issam is a member of New York's Intrepid Sea, Air & Space Museum’s Anchor Society which is focused on sustaining and expanding the Museum’s innovative education programs, exhibitions and collection of historic artifacts.

Issam is married to mechanical engineer and [[Southern Methodist University]] MBA holder, Hiba Darwish, and they have two sons Samo and Jason.

==References==
{{reflist|2}}

*
*
*
*

{{DEFAULTSORT:Darwish, Issam}}
[[Category:Lebanese people]]
[[Category:Telecommunications]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:People with acquired Nigerian citizenship]]
[[Category:Nigerian people of Lebanese descent]]
[[Category:Nigerian businesspeople]]