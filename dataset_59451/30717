{{featured article}}
{{Infobox news event
| title                    = <!-- Title to display, if other than page name -->
| image                    =
| image_name               =
| image_size               =
| caption                  =
| date                     = July 12, 1979
| time                     = 6 p.m. and following
| place                    = [[Comiskey Park]], [[Chicago]], [[Illinois]], US
| cause                    = Promotional event admitting those with a disco record for $0.98
| participants             = [[Steve Dahl]], [[Bill Veeck]], and several thousand attendees
| outcome                  = Game 2 of the Tigers/White Sox doubleheader forfeited to Detroit.
| reported death(s)        = None
| reported injuries        = Between 0 and 30
| reported property damage = Damage to the field of Comiskey Park
| suspects                 = About 39
| charges                  = Disorderly conduct
}}
'''Disco Demolition Night''' was an ill-fated [[baseball]] [[promotional event|promotion]] that took place on July&nbsp;12, 1979, at [[Comiskey Park]] in [[Chicago]], [[Illinois]]. At the climax of the event, a crate filled with [[disco]] records was blown up on the field between games of the [[Doubleheader (baseball)#Twi-night|twi-night doubleheader]] between the [[1979 Chicago White Sox season|Chicago White Sox]] and the [[1979 Detroit Tigers season|Detroit Tigers]]. Many of those in attendance had come to see the explosion rather than the games and rushed onto the field after the detonation. The playing field was damaged both by the explosion and by the rowdy fans to the point where the White Sox were required to [[forfeit (baseball)|forfeit]] the second game of the doubleheader to the Tigers.

In the late 1970s, dance-oriented [[Disco|disco music]] was very popular in the United States, particularly after being featured in hit films such as ''[[Saturday Night Fever]]'' (1977). Despite its popularity, disco sparked a backlash from [[rock music]] fans. This opposition was prominent enough that the White Sox, seeking to fill seats at Comiskey Park during a lackluster season, engaged Chicago [[shock jock]] and anti-disco campaigner [[Steve Dahl]] for the promotion at the July&nbsp;12 doubleheader. Dahl's sponsoring radio station was 97.9 [[WLUP-FM]], so attendees would pay 98 cents and bring a disco record; between games, Dahl would destroy the collected [[Gramophone record|vinyl]] in an explosion.

White Sox officials had hoped for a crowd of 20,000,&nbsp;about 5,000&nbsp;more than usual.  Instead, at least 50,000 people—including tens of thousands of Dahl's adherents—packed the stadium, and thousands more continued to sneak in even after gates were closed. Many of the records were not collected by staff and were thrown like [[flying disc]]s from the stands. After Dahl blew up the collected records, thousands of fans stormed the field and remained there until dispersed by riot police. The second game was initially postponed, but was forfeited by the White Sox the next day by order of [[American League]] president [[Lee MacPhail]]. Disco Demolition Night preceded, and may have helped precipitate, the decline of disco in late 1979; some scholars and disco artists have described the event as expressive of [[racism]] and [[homophobia]] while others have denied this connection. Disco Demolition Night remains well known as one of the most extreme promotions in [[Major League Baseball|major league]] history.

== Background ==

=== Musical ===
{{main|Disco}}
The genre known as disco, named for its popularity in {{Sic|hide=y|discotheques}}, evolved in the early 1970s in inner-city New York clubs, where [[disc jockey]]s would play imported [[dance music]] to get the crowd moving. With roots in [[African-American music|African-American]] and [[Latin American music]], and in [[gay culture]], disco became mainstream by the mid-1970s. Even white artists associated with a much more sedate style of music had disco-influenced hits, such as [[Barry Manilow]] with "[[Copacabana (song)|Copacabana]]".<ref name = "today">{{cite web|last=Sciafani|first=Tony|title=When 'Disco Sucks!' echoed around the world|publisher=TODAY.com|url=http://www.today.com/id/31832616/site/todayshow/ns/today-entertainment/t/when-disco-sucks-echoed-around-world/#.UR12R6Xy8lI|date=July 10, 2009|accessdate=February 14, 2013}}</ref> By 1977, disco was very popular in the United States, especially after the release that year of the hit movie ''[[Saturday Night Fever]]''.{{sfn|Young|p=11}} This film starred [[John Travolta]] and featured music by the [[Bee Gees]]—the fact that both actor and performers were white and presented a heterosexual image did much to make disco widely popular.  As [[Al Coury]], president of [[RSO Records]] (which had released the bestselling soundtrack album for the film) put it, ''Saturday Night Fever'' "kind of took disco [[Coming out|out of the closet]]."{{sfn|Frank|p=288}}

Despite disco's popularity, there were many who disliked it.  Some felt the music too mechanical—''[[Time (magazine)|Time]]'' magazine deemed it a "diabolical thump-and-shriek".{{sfn|Young|p=11}}<ref name = "Behrens" />  Others hated the music for the lifestyle associated with it, feeling that in the disco scene, personal appearance and style of dress were overly important.{{sfn|Young|p=11}}<ref name = "Behrens" /> The media, in discussing disco, emphasized its roots in gay culture.  According to historian Gillian Frank, "by the time of the Disco Demolition in Comiskey Park, the media commonly emphasized that disco was gay and cultivated a widespread perception that disco was taking over".{{sfn|Frank|pp=289–290}} Performers who cultivated a gay image, such as the [[Village People]] (described by ''[[Rolling Stone]]'' as "the face of disco") did nothing to efface these perceptions, and fears that [[rock music]] would die at the hands of disco increased after disco albums dominated the [[21st Grammy Awards]] in February 1979.{{sfn|Frank|pp=290–291}}

In 1978, New York's [[WBMP (FM)|WKTU]]-FM, a low-rated rock station, switched to disco and became the most popular station in the country; this led other stations to try to emulate its success.{{sfn|Frank|p=288}} In Chicago, a 24-year-old Dahl was working as a disc jockey for local [[radio station]] [[WLS-FM|WDAI]] when he was fired on Christmas Eve 1978 as part of the station's switch from rock to disco; Dahl was subsequently hired by rival [[Album-oriented rock|album-rock]] station [[WLUP-FM|WLUP]], "The Loop". Sensing an incipient anti-disco backlash<ref name = "Behrens" /><ref name=DiscoInferno>{{cite news|url=http://www.independent.co.uk/news/world/americas/disco-inferno-680390.html |title=Disco inferno |work=The Independent |date=December 11, 2004|accessdate=February 13, 2013}}</ref> and playing off the publicity surrounding his firing (he frequently mocked WDAI's "Disco DAI" slogan on the air as "Disco DIE"), Dahl created a mock organization called "The Insane Coho Lips", an anti-disco army consisting of his listeners.<ref name = "USA" /> According to Andy Behrens of [[ESPN]], Dahl and his broadcast partner [[Garry Meier]] "organized the Cohos around a simple and surprisingly powerful idea: Disco Sucks".<ref name = "Behrens" />

According to Dahl, in 1979, the Cohos were locked in a war "dedicated to the eradication of the dreaded musical disease known as DISCO".{{sfn|Frank|p=297}} In the weeks leading up to Disco Demolition Night, Dahl promoted a number of anti-disco public events, several of which became unruly.  When a {{Sic|hide=y|discotheque}} in [[Linwood, Indiana]], switched from disco to rock in June, Dahl showed up, as did several thousand Cohos, and the police had to be called.  Later that month, Dahl and several thousand Cohos occupied a teen disco in the Chicago suburbs. At the end of June, Dahl urged his listeners to throw marshmallows at a WDAI promotional van, which was at a shopping mall where a teen disco had been built. The Cohos chased the van and driver and cornered them in a nearby park, though the situation ended without violence.  On July 1, a near-riot occurred in [[Hanover Park, Illinois]], when hundreds of Cohos could not enter a sold-out promotional event, and fights broke out.  Some 50 police officers were needed to control the situation. When disco star [[Van McCoy]] died suddenly on July 6, Dahl marked the occasion by destroying one of his records, "[[The Hustle (song)|The Hustle]]", on the air.{{sfn|Frank|pp=297–298}}

{{quote box | align = right | width = 23em | quoted = true | salign = left
| quote = <poem>
''Do ya think I'm disco''
''Cuz I spend so much time'' 
''Blow drying out my hair?''
''Do ya think I'm disco''
''Cuz I know the dance steps'' 
''Learned them all at Fred Astaire?''
</poem>
| source = Steve Dahl, "Do You Think I'm Disco?" (1979)<ref name = "dahl2014">{{cite news|url=http://www.chicagobusiness.com/article/20140710/OPINION/140709818/disco-demolition-35-years-later-thats-the-way-i-liked-it|last=Dahl|first=Steve|date=July 10, 2014|title=Disco Demolition 35 years later: That's the way I liked it|website=Chicagobusiness.com|publisher=[[Crain's Chicago Business]]|location=Chicago, Illinois|accessdate=July 27, 2014}}</ref>
}}
Dahl and Meier regularly mocked disco records on the radio. Dahl also recorded his own parody: "Do Ya Think I'm Disco?", a satire of [[Rod Stewart]]'s disco-oriented hit "[[Da Ya Think I'm Sexy?]]"<ref name = "USA">{{cite news|last=Beaton|first=Rod|title=No anniversary party for disco debacle|newspaper=[[USA Today]]|date=July 12, 2004|page=C.03|url=http://usatoday30.usatoday.com/sports/baseball/2004-07-12-disco-demolition-anniversary_x.htm|accessdate=February 14, 2013}}</ref><ref>{{cite journal|title=WLUP Chicago Reminisces|journal=[[Billboard (magazine)|Billboard]]|date=April 22, 1989|volume=101|issue=16|page=10}}</ref> This parody song presented {{Sic|hide=y|discotheques}} as populated by effeminate men and frigid women. The lead character, named Tony like Travolta's character in ''Saturday Night Fever'', is unable to attract a woman until he abandons the disco scene, selling his three-piece white suit at a garage sale and melting down his gold chains for a [[Led Zeppelin]] belt buckle.{{sfn|Frank|pp=283–294, 297}}

A number of anti-disco incidents took place elsewhere in the first half of 1979, showing that "the Disco Demolition was not an isolated incident or an aberration".  In Seattle, hundreds of rock fans attacked a mobile dance floor, while in Portland, Oregon, a disc jockey destroyed a stack of disco records with a chainsaw as thousands looked on and cheered. In New York, a rock deejay played [[Donna Summer]]'s sexualized disco hit, "[[Hot Stuff (Donna Summer song)|Hot Stuff]]"; he received protests from his listeners.{{sfn|Frank|p=278}}

=== Baseball ===
Since the 1940s, [[Chicago White Sox]] owner [[Bill Veeck]] had been noted for using promotions to attract fan interest; he stated "you can draw more people with a losing team plus bread and circuses than with a losing team and a long, still silence".  His son, Mike, was the promotions director for the White Sox in 1979.  Mike Veeck wrote in a letter to a fan before the season that team management intended to make sure that whether the White Sox won or lost, the fans would have fun.{{sfn|Young|p=12}}

On May&nbsp;2, 1979, the [[Detroit Tigers]] –&nbsp;Chicago White Sox game at Comiskey Park was rained out. Officials rescheduled it as part of a [[doubleheader (baseball)|twi-night doubleheader]] on July&nbsp;12.<ref name = "History">{{cite web|title=Disco is dealt death blow by fans of Chicago White Sox|url=http://www.history.com/this-day-in-history/disco-is-dealt-death-blow-by-fans-of-the-chicago-white-sox|accessdate=February 13, 2013|publisher=The History Channel}}</ref> Already scheduled for the evening of July&nbsp;12 was a promotion aimed at teenagers, who could purchase tickets at half the regular price.{{sfn|Young|p=12}}

The White Sox had had a "Disco Night" at Comiskey Park in 1977; Mike Veeck, WLUP Sales Manager Jeff Schwartz, and WLUP Promotions Director Dave Logan discussed the possibility of an anti-disco night promotion after Schwartz mentioned that the White Sox were looking to do a promotion with the station.  The matter had also been brought up early in the 1979 season when Schwartz told Mike Veeck of Dahl and his plans to blow up a crate of disco records while live on the air from a shopping mall. During a meeting at WLUP, Dahl was asked if he would be interested in blowing up records at Comiskey Park on July&nbsp;12. Since the radio frequency of WLUP was 97.9, the promotion for "Disco Demolition Night" (in addition to the offer for teenagers) was that anyone who brought a disco record to the ballpark would be admitted for 98&nbsp;cents.  Dahl was to blow up the collected records between games of the doubleheader.{{sfn|Young|p=12}}

== Event ==
[[File:Old comiskey park.jpg|thumb|right|Comiskey Park in 1990]]
In the weeks before the event Dahl invited his listeners to bring records they wanted to see destroyed to Comiskey Park.{{sfn|Frank|p=276}} The disc jockey feared that the promotion would fail to draw people to the ballpark, and that he would be humiliated.  The previous night's attendance had been 15,520,<ref name = "Behrens" /> and Comiskey Park had a capacity of 44,492.<ref>{{cite book|title=The Sporting News Baseball Guide|year=1979|publisher=The Sporting News Company|page=11|location=St. Louis}}</ref> The White Sox were not having a good year, and were {{win-loss record|w=40|l=46}}&nbsp;going into the July&nbsp;12 doubleheader.<ref name = "Behrens" /> The White Sox and WLUP hoped for a crowd of 20,000 people.<ref name="History" /> Mike Veeck hired enough security for 35,000.{{sfn|Dickson|p=314}}

Owner Bill Veeck was concerned the promotion might turn into a disaster and checked himself out of the hospital, where he had been undergoing tests.{{sfn|Dickson|p=314}} The elder Veeck's fears were substantiated when he saw the people walking towards the ballpark that afternoon; many carried signs that described disco in profane terms.<ref name = "Behrens" />{{sfn|Young|p=12}}

The doubleheader sold out, leaving at least 20,000&nbsp;people outside the ballpark.{{sfn|Young|p=12}}  Some were not content to remain there, leaping [[turnstile]]s, climbing fences, and entering through open windows.{{sfn|Young|p=13}}  The attendance was officially reported as 47,795.<ref name=NYT>{{cite news|author=LaPointe, Joe|url=https://www.nytimes.com/2009/07/05/sports/baseball/05disco.html|title=The Night Disco Went Up in Smoke|newspaper=[[The New York Times]]}}</ref> Bill Veeck estimated that there were anywhere from 50,000 to 55,000 in the park—easily the largest crowd of his second stint as White Sox owner.<ref name="Youngstown"/> The Chicago Police Department closed off-ramps from the [[Dan Ryan Expressway]] near the stadium.<ref name="Behrens">{{cite web|url=http://sports.espn.go.com/espn/page3/story?page=behrens/040809|publisher=ESPN.com|title=Disco demolition: Bell-bottoms be gone!|date=July 12, 2009|accessdate=February 13, 2013|first=Andy|last=Behrens|work=ESPNChicago.com}}</ref> Attendees were supposed to deposit their records into a large box, some {{convert|4|by|6|by|5|ft}} tall; once the box was overflowing, many people brought their discs to their seats.<ref name = "Postcards" />

The first game was to begin at 6&nbsp;pm, with the second game to follow.<ref>{{cite web|title=Baseball|url=https://news.google.com/newspapers?id=KL88AAAAIBAJ&sjid=7y0MAAAAIBAJ&dq=schedule&pg=1491%2C4524494|newspaper=Bangor Daily News|date=July 12, 1979|accessdate=February 16, 2012}}</ref> Lorelei, a model who did public appearances for WLUP and who was very popular in Chicago that summer for her sexually provocative poses in the station's advertisements, threw out the first pitch.{{sfn|Frank|p=277}}<ref name = "lor" /> As the first game began, Mike Veeck got word that thousands of people were trying to get into the park without tickets.  He sent his security personnel to the stadium gates to keep the would-be [[gate crashing|gate crashers]] at bay. This left the field unattended, and fans began throwing the uncollected disco [[LP record|LPs]] and [[45rpm single|singles]] from the stands.  Tigers [[designated hitter]] [[Rusty Staub]] remembered that the records would slice through the air, and land sticking out of the ground.  He urged teammates to wear batting helmets when playing their positions, "It wasn't just one, it was many.  Oh, God almighty, I've never seen anything so dangerous in my life."{{sfn|Dickson|p=315}} Attendees also threw firecrackers, empty liquor bottles, and lighters onto the field. The game was stopped several times because of the rain of foreign objects.{{sfn|Young|p=12}} Dozens of hand-painted banners with such slogans as "Disco sucks" were hung from the ballpark's seating decks.{{sfn|Frank|p=277}} White Sox broadcaster [[Harry Caray]] could see groups of people, who were clearly music rather than baseball fans, wandering through the stadium.  Others sat intently in their seats, awaiting the explosion.{{sfn|Fingers|p=144}} Mike Veeck later remembered an odor of marijuana in the grandstand and said of the attendees, "This is the Woodstock they never had."{{sfn|Dickson|p=314}}  The miasma permeated the press box, which both Caray and his broadcast partner, [[Jimmy Piersall]], commented on over the air.<ref name = "Postcards" /> The crowds outside the stadium threw records as well, or gathered them together and burned them in bonfires.{{sfn|Dickson|p=315}}   Detroit won the first game, 4–1.<ref name = "Behrens" />

== Explosion ==
[[File:Garry Meier.jpg|thumb|right|200px|Garry Meier]]
The first game ended at 8:16 pm; at 8:40 Dahl, dressed in army fatigues and a helmet,{{sfn|Frank|p=277}} emerged onto the playing surface together with Meier and Lorelei. They proceeded to [[Baseball field#outfield|center field]]  where the vinyl-filled box awaited, though they first did a lap of the field in a Jeep, showered (according to Dahl, lovingly) by his troops with firecrackers and beer. The large box containing the collected records had been rigged with explosives. Dahl and Meier warmed up the crowd, leading attendees in a chant of "disco sucks". Lorelei recalled that the view from center field was surreal. On the [[Baseball field#Pitcher's mound|mound]], White Sox pitcher [[Ken Kravec]], scheduled to start the second game, began to warm up. Other White Sox, in the [[Dugout (baseball)|dugout]] and wearing [[batting helmet]]s, looked out upon the scene. Fans who felt events were getting out of control and who wished to leave the ballpark had difficulty doing so; in an effort to deny the intruders entry, security had padlocked all but one gate.<ref name = "Behrens" />{{sfn|Young|p=13}}

Dahl told the crowd,

{{quote |
This is now officially the world's largest anti-disco rally! Now listen—we took all the disco records you brought tonight, we got 'em in a giant box, and we're gonna blow 'em up ''reeeeeeal goooood''.<ref name = "Postcards">{{cite web|url=http://www.chicagoreader.com/chicago/postcards-from-disco-demolition-night/Content?oid=1148642|title=Postcards from Disco Demolition Night|publisher=''Chicago Reader''|last=Costello|first=Brian|date=July 9, 2009|accessdate=February 14, 2013}}</ref>
}}

Dahl set off the explosives, destroying the records and tearing a large hole in the outfield grass.{{sfn|Fingers|p=144}}  With most of the security personnel still watching the gates per Mike Veeck's orders, there was almost no one guarding the playing surface.<ref name=NYT/> Soon, the first of what would be thousands of attendees rushed onto the field, causing Kravec to flee the mound and join his teammates in a barricaded clubhouse.  Between 5,000&nbsp;and 7,000&nbsp;people are estimated to have taken the field. Some climbed the foul poles, others set records on fire, or ripped up the grass.  The [[batting cage]] was destroyed; the bases were pulled up and stolen. Among those taking the field was 21-year-old aspiring actor [[Michael Clarke Duncan]]; during the melee, Duncan slid into third base, had a silver belt buckle stolen,<ref name="st099">{{cite news|last=Zwecker|first=Bill|title=Love may have bloomed again on set for 'Garden State' star|url=http://www.highbeam.com/doc/1P2-1642647.html|newspaper=Chicago Sun-Times|date=September 28, 2006|accessdate= March 7, 2013|subscription=y}}</ref> and went home with a bat from the dugout.<ref name="ct909">{{cite news|last=Caldarelli|first=Adam|title=From the Cubicle|newspaper=Chicago Tribune|date=May 20, 2006|url=http://www.chicagotribune.com/sports/cs-060520cubicle,0,7421730,print.column|accessdate=February 16, 2013}}</ref> As Bill Veeck stood with a microphone near where home plate had been, begging people to return to the stands, a bonfire raged in center field.<ref name = "Behrens" /><ref name = "History" />{{sfn|Young|p=13}}{{sfn|Dickson|pp=315–316}}

Years later, Lorelei remembered that she had been waving to the crowd when she was grabbed by two of the bodyguards who had accompanied the Jeep and placed her back in the vehicle.  The party was unable to return to home plate because of the rowdy fans, so the Jeep was driven out of the stadium and through the surrounding streets, to the delight of the many Cohos outside the stadium who recognized the occupants.  They were driven to the front of the stadium, ushered back inside, and taken up to the press room where they had spent most of the first game.<ref name = "lor">{{cite web|title=Flashing Back with Disco Demolition's Lorelei|publisher=White Sox Interactive|last=Bova|first=George|url=http://www.whitesoxinteractive.com/rwas/index.php?category=11&id=2300|accessdate=February 15, 2013}}</ref>

Caray unsuccessfully attempted to restore order by the public address system. The scoreboard, flashing "PLEASE RETURN TO YOUR SEATS", was ignored as was the playing of "[[Take Me Out to the Ball Game]]". Some of the attendees were dancing in circles around the burning vinyl shards.<ref name = "Postcards" /> Dahl offered his help to get the rowdy fans to leave, but it was declined.<ref name = "kansas" >{{cite news|title=Disco Demolition embarrassed Sox|url=https://news.google.com/newspapers?id=X5EzAAAAIBAJ&sjid=suYFAAAAIBAJ&dq=disco%20demolition%20night&pg=4190%2C3517956|date=July 13, 1989|accessdate=February 15, 2013|newspaper=''AP via'' Lawrence Journal-World|page=5D}}</ref>

At 9:08 pm,{{sfn|Frank|p=278}} Chicago police in full [[riot gear]] arrived to the applause of the baseball fans remaining in the stands. Those on the field hastily dispersed upon seeing the police.  Thirty-nine people were arrested for disorderly conduct; estimates of injuries to those at the event range from none to over thirty.{{sfn|Young|p=13}} Bill Veeck wanted the teams to play the second game once order was restored.  However, the field was so badly torn up that umpiring crew chief [[Dave Phillips (umpire)|Dave Phillips]] felt that it was still not playable even after White Sox groundskeepers spent an hour clearing away debris.  Tigers manager [[Sparky Anderson]] refused to allow his players to take the field in any event due to safety concerns.  Phillips called American League president [[Lee MacPhail]], who postponed the second game to Sunday after hearing a report on conditions.  Anderson, however, demanded that the game be [[forfeit (baseball)|forfeited]] to the Tigers.  He argued that under baseball's rules, a game can only be postponed due to an [[act of God]], and that, as the home team, the White Sox were responsible for field conditions. The next day, MacPhail forfeited the second game to the Tigers 9–0.  In a ruling that largely upheld Anderson's arguments, MacPhail stated that the White Sox had failed to provide acceptable playing conditions.<ref name="Behrens"/><ref name = "Youngstown">{{cite news|title=Disco riot rocks, rolls Chisox park|url=https://news.google.com/newspapers?id=QkBKAAAAIBAJ&sjid=ZoUMAAAAIBAJ&dq=disco%20demolition%20night%20act%20of%20god%20postponement&pg=1397%2C4873831|date=July 13, 1979|accessdate=February 14, 2013|newspaper=Youngstown Vindicator|page=18}}</ref><ref name="Graves"/><ref>{{cite news|title=Phister ousts Tim Gullikson|page=3, Part 2|url=https://news.google.com/newspapers?id=IXVQAAAAIBAJ&sjid=-hEEAAAAIBAJ&dq=disco%20demolition&pg=6661%2C2429259|newspaper=The Milwaukee Sentinel|date=July 14, 1979|accessdate=February 14, 2013}}</ref>

==Reaction and aftermath==
[[File:Steve Dahl.jpg|thumb|right|Dahl in 2008]]
The day after, Dahl began his regular morning broadcast by reading the indignant headlines in the local papers.  He mocked the coverage, "I think for the most part everything was wonderful. Some maniac Cohos got wild, went down on the field. Which you shouldn't have done. Bad little Cohos."  Tigers manager Anderson stated of the events, "Beer and baseball go together, they have for years. But I think those kids were doing things other than beer."<ref name = "Behrens" /> Columnist [[David Israel]] of the ''[[Chicago Tribune]]'' commented on July 12 that he was not surprised by what had occurred, "It would have happened any place 50,000 teenagers got together on a sultry summer night with beer and reefer." White Sox pitcher [[Rich Wortham]], a Texan, suggested, "This wouldn't have happened if they had country and western night."{{sfn|Young|p=15}}

Although Bill Veeck took much of the public criticism for the fiasco, his son Mike suffered repercussions as the actual front-office promoter behind it.  Mike Veeck remained with the White Sox until late 1980, when he resigned; his father sold the team to [[Jerry Reinsdorf]] soon afterward.  He was unable to find a job in baseball for several years, leading him to claim that he had been [[blackballing|blackballed]] from the game.  For several years, he worked for a [[jai-alai]] [[Fronton (court)|fronton]] in Florida, battling [[alcoholism]].{{sfn|Dickson|p=317}} As Mike Veeck related, "The second that first guy shimmied down the outfield wall, I knew my life was over!"{{sfn|Fingers|p=144}} Mike Veeck has since become an owner of minor league baseball teams<ref>{{cite news|title=How I did it: Mike Veeck|last=Veeck|first=Mike|url=http://www.inc.com/magazine/20050401/how-i-did-it.html|newspaper=[[Inc. (magazine)|Inc]]|date=April 1, 2005|accessdate=February 16, 2013}}</ref> 
and in July 2014 the [[Charleston RiverDogs]], of whom Veeck is president, held a promotion involving the destruction of [[Justin Bieber]] and [[Miley Cyrus]] merchandise.<ref name=wp-riverdogsredux>{{cite web|title=Charleston RiverDogs will destroy music of Justin Bieber, Miley Cyrus in Disco Demolition Night redux|url=http://www.washingtonpost.com/blogs/early-lead/wp/2014/07/16/charleston-riverdogs-will-destroy-music-of-justin-bieber-miley-cyrus-in-disco-demolition-night-redux/|website=[[The Washington Post]]|accessdate=July 19, 2014|date=July 16, 2014}}</ref><ref name=Schwartz>{{cite web|last1=Schwartz|first1=Nick|title=A minor league team destroyed Justin Bieber and Miley Cyrus music in a postgame explosion|url=http://ftw.usatoday.com/2014/07/a-minor-league-team-destroyed-justin-bieber-and-miley-cyrus-music-in-a-postgame-explosion|website=[[USA Today|For The Win: USA Today Sports]]|publisher=[[Gannett Company]]|accessdate=September 15, 2014|date=July 20, 2014}}</ref><ref name=Nati>{{cite web|last1=Nati|first1=Michelle|title=Charleston River Dogs Host Disco Demolition 2: Justin Bieber & Miley Cyrus Music Destroyed At Baseball Game (VIDEO)|url=http://www.mstarz.com/articles/33977/20140720/charleston-river-dogs-host-disco-demolition-2-justin-bieber-miley-cyrus-music-destroyed-at-baseball-game-video.htm|website=Mstars News|accessdate=September 15, 2014|date=July 20, 2014}}</ref> Dahl is still a radio personality in Chicago and also reaches his listeners through [[podcasting]].<ref>{{cite news|title=Steve Dahl's pay podcast marks 1st anniversary|last=Channick|first=Robert|url=http://articles.chicagotribune.com/2012-08-05/business/ct-biz-0805-dahl-blogging-20120805_1_steve-dahl-chicago-radio-garry-meier|newspaper=Chicago Tribune|date=August 5, 2012|accessdate=February 16, 2013}}</ref>

The popularity of disco declined significantly in late 1979 and 1980. Many disco artists continued, but record companies began labeling their recordings as dance music.<ref name = "today" /> Dahl stated in a 2004 interview that disco was "probably on its way out. But I think it [Disco Demolition Night] hastened its demise".<ref>{{cite web|url=http://www.msnbc.msn.com/id/5429592/ns/msnbc_tv-about_msnbc_tv/|publisher=MSNBC.com|title='Countdown with Keith Olbermann' Complete Transcript for July 12, 2004|date=July 12, 2004|accessdate=February 15, 2013}}</ref> According to Frank, "the Disco Demolition triggered a nationwide expression of anger against disco that caused disco to recede quickly from the American cultural landscape".{{sfn|Frank|p=302}}

Music critic [[Dave Marsh]] recalled his feelings after Disco Demolition Night, "It was your most paranoid fantasy about where the ethnic cleansing of the rock radio could ultimately lead."<ref name = "today" /> Marsh, who wrote for ''Rolling Stone'', was one of the few who at the time deemed the event an expression of [[bigotry]], writing in a column, "white males, eighteen to thirty-four are the most likely to see disco as the product of homosexuals, blacks, and Latins, and therefore they're the most likely to respond to appeals to wipe out such threats to their security.  It goes almost without saying that such appeals are racist and sexist, but broadcasting has never been an especially civil-libertarian medium."<ref>{{cite journal|last=Marsh|first=Dave|journal=[[Rolling Stone]]|title=The flip sides of '79|date=December 22, 1979|page=28}}</ref> [[Nile Rodgers]], producer and guitarist for the disco-era group [[Chic (band)|Chic]], deemed the event akin to Nazi book burning. [[Gloria Gaynor]], who had a huge disco hit with "[[I Will Survive]]", stated, "I've always believed it was an economic decision—an idea created by someone whose economic bottom line was being adversely affected by the popularity of disco music. So they got a mob mentality going."<ref name = "today" />

Historian J. Zeitz suggests that while "an obvious explanation for the Disco Demolition Night riot might center on the desire of white, [[working-class]] baseball fans to strike out against an art form that they associated with African Americans, gays and lesbians, and Latinos".{{sfn|Zeitz|pp=673–674}} Concerning the event, [[University of East London]] professor Tim Lawrence also proposes that, "Following the unexpected commercial success of ''Saturday Night Fever'', major record companies had started to invest heavily in a sound that their white straight executive class did not care for, and when the overproduction of disco coincided with a deep recession, the homophobic (and also in many respects sexist and racist) 'disco sucks' campaign culminated with a record burning rally that was staged at the home of the Chicago White Sox in July 1979."<ref>Tim Lawrence (2011): "Disco and the Queering of the Dance Floor, Cultural Studies", 25:2, 230-243</ref>

Nevertheless, [[Harry Wayne Casey]], singer for the disco act [[KC and the Sunshine Band]], did not believe Disco Demolition Night itself was discriminatory, and stated his belief that Dahl was simply an idiot. Dahl himself rejects the notion that prejudice was his motivation for Disco Demolition Night. "The worst thing is people calling Disco Demolition homophobic or racist. It just wasn't&nbsp;... We weren't thinking like that."<ref name= "Behrens" />

In 2014 Dahl stated that the racist/homophobic view of Disco Demolition Night stemmed from a 1996 [[VH1]] documentary, ''The Seventies'', which presented it in that light;<ref name = "dahl2014" /> Mark W. Anderson, in response, suggested that the event gave the participants an opportunity "to say they didn't like&nbsp;... who they saw as the potential victors in a cultural and demographic war&nbsp;... its {{sic}} hard to believe few if any of those who organized the event didn't see that underlying reality for what it was".<ref>{{cite news|last=Anderson|first=Mark W.|url=http://www.nbcchicago.com/blogs/ward-room/Mists-of-Time-Obscure-Disco-Demolition-Meaning-266767341.html|title=Time Obscures Meaning of Disco Demolition: Saturday is the 35th Anniversary of the infamous Disco Demolition Night|date=July 11, 2014|website=nbcchicago.com|publisher=[[WMAQ-TV]]|accessdate=July 27, 2014|location=Chicago, Illinois}}</ref>

The unplayed second game remains the last American League game to be forfeited.<ref name="lytle">{{cite book|last=Lytle|first=Ken|title=The Little Book of Big F*#k Ups: 220 of History's Most-Regrettable Moments|url=https://books.google.com/books?id=2-SrRW5l74IC|date=April 15, 2011|location=Adams, Mass.|publisher=Adams Media|isbn=1-4405-1252-3|page=154}}</ref> The last [[National League]] game to be forfeited was on August 10, 1995, when a baseball giveaway promotion at [[Dodger Stadium]] went awry, forcing the [[Los Angeles Dodgers]] to concede the game to the [[St. Louis Cardinals]].<ref name="snyder365">{{cite book|last=Snyder|first=John|title=365 Oddball Days in Dodgers History|location=Cincinnati|url=https://books.google.com/?id=mmZO7XSc7i4C|date=March 16, 2010|page=10|publisher=Clerisy Press|isbn=1-57860-452-4}}</ref>  According to baseball analyst Jeremiah Graves, "To this day Disco Demolition Night stands in infamy as one of the most ill-advised promotions of all-time, but arguably one of the most successful as 30&nbsp;years later we're all still talking about it."<ref name = "Graves">{{cite web|last=Graves|first=Jeremiah|work=[[Bleacher Report]]|date=July 12, 2009|title=30 years later: Disco Demolition Night|accessdate=February 14, 2013|url=http://bleacherreport.com/articles/216590-30-years-later-disco-demolition-night}}</ref>

== Game results ==
Game 1:

{{Baseballbox
|date = July 12, 1979
|time = day game
|team1 = Detroit Tigers
|score = 4–1
|boxurl = http://www.retrosheet.org/boxesetc/1979/B07121CHA1979.htm
|team2 = Chicago White Sox
|stadium = Comiskey Park
|attendance =47,795
|umpire = HP: [[Dave Phillips (umpire)|Dave Phillips]] (cc)<br>1B: [[Dan Morrison (umpire)|Dan Morrison]]<br>2B: [[Dallas Parks]]<br>3B: [[Durwood Merrill]]
}}

Game 2 forfeited to Detroit, 9–0.

==See also==
* [[Bounty Bowl|Bounty Bowl II]]
* [[Forfeit (baseball)]] for a list of similar events
* ''[[The Last Days of Disco]]''
* [[Ten Cent Beer Night]]
{{Portal bar|1970s|Baseball|Chicago|Illinois|Radio}}

==References==
{{Reflist|colwidth=30em}}

==Further reading==
*{{cite book
 | last1 = Dahl
 | first1 = Steve
 || author-link1 = Steve Dahl
 | last2 = Hoekstra
 | first2 = Dave
 | url =https://books.google.co.jp/books?id=3GMBswEACAAJ&dq=isbn:9781940430751&hl=ja&sa=X&ved=0ahUKEwiSyJHrm6zQAhUSNrwKHWh1B4sQ6AEIHTAA
 | title =Disco Demolition: The Night Disco Died
 | date = August 2016
 | isbn = 978-1-9404-3075-1
 | publisher =Curbside Splendor Publishing
 | location = Chicago, Illinois
 | others=Foreword by [[Bob Odenkirk]] and photographs provided by Paul Natkin
}}
* {{cite book
 | last = Dickson
 | title = Bill Veeck: Baseball's Greatest Maverick
 | url = https://books.google.com/books?id=cDlqpKMu6kcC
 | isbn = 978-0-8027-1778-8
 | year = 2012
 | location = New York
 | publisher = Walker Publishing Co., Inc
 | ref = {{sfnRef|Dickson}}
 | first = Paul
}}
* {{cite book
 | last = Fingers
 | title = Rollie's Follies: A Hall of Fame Revue of Baseball Stories and Stats Lists and Lore
 | url = https://books.google.com/books?id=QQ1YkONXsycC
 | isbn = 978-1-57860-335-0
 | year = 2008
 | location = Cincinnati
 | publisher = Clerisy Press
 | ref = {{sfnRef|Fingers}}
 | first = Rollie
}}
* {{cite journal
 | last = Frank
 | first = Gillian
 | title = Discophobia: Antigay Prejudice and the 1979 Backlash against Disco
 | journal = Journal of the History of Sexuality
 | date = May 2007
 | volume = 16
 | issue = 2
 | publisher = University of Texas Press
 | location = Austin, Texas
 | jstor =30114235
 | pages = 276–306
 | ref = {{sfnRef|Frank}}
 | doi=10.1353/sex.2007.0050
| pmid = 19244671
 }}
* {{cite journal
 | last = Young
 | first = Christopher J.
 | title = 'When Fans Wanted to Rock, the Baseball Stopped': Sports, Promotions, and the Demolition of Disco on Chicago's South Side
 | journal = The Baseball Research Journal
 | date = Summer 2009
 | volume = 38
 | issue = 1
 | publisher = [[Society for American Baseball Research]]
 | location = Scottsdale, Az.
 | pages = 11–16
 | ref = {{sfnRef|Young}}
}}
* {{cite journal
 | last = Zeitz
 | first = J.
 | title = Rejecting the center: Radical grassroots politics in the 1970s—second-wave feminism as a case study
 | journal = Journal of Contemporary History
 | date = October 2008
 | volume = 43
 | issue = 4
 | publisher = SAGE Publications, Ltd
 | location = Thousand Oaks, Calif.
 | jstor =40543229
 | pages = 673–678
 | ref = {{sfnRef|Zeitz}}
 | doi=10.1177/0022009408095422
}}

==External links==
<!--Hiding this unitl its copyright status can be discussed* [http://www.slate.com/blogs/browbeat/2016/09/20/watch_disco_demolition_night_unfold_in_real_time.html Magazine article containing the complete game video]-->
* [http://whitesoxinteractive.com/History&Glory/DiscoDemolition.htm Whitesoxinteractive.com's Disco Demolition story page]
* [http://www.fuzzymemories.tv/index.php?c=4548 WBBM-TV news report, July 13, 1979]
* [http://www.outernetweb.com/focal/disco/headlines/index.html Disco Demolition Night News Headlines]
* [http://www.outernetweb.com/focal/disco/coholips/index.html More about the Insane Coho Lips]

{{Chicago White Sox}}
{{Detroit Tigers}}

[[Category:1979 in American music]]
[[Category:1979 in baseball]]
[[Category:1979 in Illinois]]
[[Category:1979 Major League Baseball season]]
[[Category:1979 riots]]
[[Category:Chicago White Sox]]
[[Category:Detroit Tigers]]
[[Category:Disco]]
[[Category:Hooliganism]]
[[Category:Major League Baseball games]]
[[Category:Music of Chicago]]
[[Category:Riots and civil disorder in Chicago]]
[[Category:Sports riots]]
[[Category:July 1979 sports events]]