'''FUTON bias''' is the tendency of [[scholar]]s to cite [[academic journal]]s with [[open access]]—that is, journals that make their full text available on the [[Internet]] without charge—in preference to [[Subscription business model|toll-access publications]]. ("FUTON" is an [[acronym]] for <u>fu</u>ll <u>t</u>ext <u>o</u>n the <u>N</u>et.) Scholars in some fields can more easily discover and access articles whose full text is available online, which increases authors' likelihood of reading and citing these articles, an issue that was first raised and has been mainly studied in connection with [[medical research]].<ref name="cite pmid| 15301326">{{Cite journal
| last1 = Murali | first1 = N. S.
| last2 = Murali | first2 = H. R.
| last3 = Auethavekiat | first3 = P.
| last4 = Erwin | first4 = P. J.
| last5 = Mandrekar | first5 = J. N.
| last6 = Manek | first6 = N. J.
| last7 = Ghosh | first7 = A. K.
| title = Impact of FUTON and NAA bias on visibility of research
| journal = Mayo Clinic proceedings. Mayo Clinic
| volume = 79
| issue = 8
| pages = 1001–1006
| year = 2004
| pmid = 15301326
| url = http://www.mayoclinicproceedings.com/content/79/8/1001.full.pdf | doi=10.4065/79.8.1001
}}</ref><ref name="cite pmid| 12937253">{{Cite journal
| last1 = Ghosh | first1 = A. K.
| last2 = Murali | first2 = N. S.
| title = Online access to nephrology journals: The FUTON bias
| journal = Nephrology, dialysis, transplantation : official publication of the European Dialysis and Transplant Association - European Renal Association
| volume = 18
| issue = 9
| pages = 1943; author reply 1943
| year = 2003
| pmid = 12937253
| url = http://ndt.oxfordjournals.org/content/18/9/1943.1.full
 | doi=10.1093/ndt/gfg247
}}</ref><ref name="cite pmid|16517987 ">{{Cite journal
| last1 = Mueller | first1 = P. S.
| last2 = Murali | first2 = N. S.
| last3 = Cha | first3 = S. S.
| last4 = Erwin | first4 = P. J.
| last5 = Ghosh | first5 = A. K.
| title = The effect of online status on the impact factors of general internal medicine journals
| journal = The Netherlands journal of medicine
| volume = 64
| issue = 2
| pages = 39–44
| year = 2006
| pmid = 16517987
| url = http://www.njmonline.nl/njm/getpdf.php?t=a&id=10000037
}}</ref><ref name="cite pmid|18974812">{{Cite journal
| last1 = Krieger | first1 = M. M.
| last2 = Richter | first2 = R. R.
| last3 = Austin | first3 = T. M.
| doi = 10.3163/1536-5050.96.4.010
| title = An exploratory analysis of PubMed's free full-text limit on citation retrieval for clinical questions
| journal = Journal of the Medical Library Association : JMLA
| volume = 96
| issue = 4
| pages = 351–355
| year = 2008
| pmid = 18974812
| pmc =2568849
}}</ref> In the context of [[evidence-based medicine]], articles in expensive journals that do not provide open access (OA) may be "priced out of evidence", giving a greater weight to FUTON publications.<ref>{{cite web |author=Gilman, Isaac |title=Opening up the Evidence: Evidence-Based Practice and Open Access |year=2009 |work=Faculty Scholarship (PUL) |publisher=Pacific University Libraries|url=http://commons.pacificu.edu/libfac/4 }}</ref> FUTON bias may increase the [[impact factor]] of open-access journals relative to journals without open access.<ref name="cite pmid| 12401287" />

One study concluded that authors in medical fields "concentrate on research published in journals that are available as full text on the internet, and ignore relevant studies that are not available in full text, thus introducing an element of bias into their search result".<ref name="cite pmid| 12401287">{{Cite journal
| last1 = Wentz | first1 = R.
| title = Visibility of research: FUTON bias
| doi = 10.1016/S0140-6736(02)11264-5
| journal = The Lancet
| volume = 360
| issue = 9341
| pages = 1256–1256
| year = 2002
| pmid = 12401287
| pmc =
}}</ref> Authors of another study conclude that "the OA advantage is a quality advantage, rather than a quality bias", that authors make a "self-selection toward using and citing the more citable articles—once OA self-archiving has made them accessible", and that open access "itself will not make an unusable (hence uncitable) paper more used and cited".<ref>{{Cite journal | last1 = Gargouri | first1 = Y. | last2 = Hajjem | first2 = C. | last3 = Larivière | first3 = V. | last4 = Gingras | first4 = Y. | last5 = Carr | first5 = L. | last6 = Brody | first6 = T. | last7 = Harnad | first7 = S. | editor1-last = Futrelle | editor1-first = Robert P | title = Self-Selected or Mandated, Open Access Increases Citation Impact for Higher Quality Research | doi = 10.1371/journal.pone.0013636 | journal = PLoS ONE | volume = 5 | issue = 10 | pages = e13636 | year = 2010 | pmid =  20976155| pmc =2956678 }}{{open access}}</ref>

"No abstract available bias" is a scholar's tendency to cite journal articles that have an [[abstract (summary)|abstract]] available online more readily than articles that do not— this affects articles' citation count similarly to FUTON bias.<ref name="cite pmid| 15301326" /><ref name="cite pmid| 12401287" />

==See also==
* [[Availability bias]]
* [[Digital divide]]
* [[List of cognitive biases]]
* [[Not invented here]]
* [[Open access journal]]
* [[Paywall]]

==References==
{{Reflist|30em}}

==Further reading==
* {{cite web|url=http://epc.buffalo.edu/authors/goldsmith/if_it_doesnt_exist.html
|title=If It Doesn't Exist on the Internet, It Doesn't Exist
|accessdate= 2008-11-05
|last=Goldsmith
|first=Kenneth
|coauthors=
|date=September 27, 2005
|work=Elective Affinities Conference
|publisher=[[State University of New York, Buffalo]]
}}

{{Academic publishing}}
{{Biases}}
{{Portal bar|Science|Statistics|Internet}}

{{DEFAULTSORT:FUTON bias}}
[[Category:Bias]]
[[Category:Research]]
[[Category:Academic publishing]]
[[Category:Electronic publishing]]
[[Category:Academic terminology]]