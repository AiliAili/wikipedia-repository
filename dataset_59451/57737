{{Use dmy dates|date=October 2011}}
{{Infobox organization
| name   = ISZS
| logo   = ISZS.jpg
| type   = Nonprofit organization
| founded_date      = 2004<br/>Beijing, China
| location          = Beijing, China (international)
| key_people        = Zhibin Zhang, President<br>Chunxu Han, Secretary General
| area_served       = Worldwide
| focus             = [[Zoology]], science
| method            = Communication, research, [[:wikt:coordination|coordination]]
| homepage          = [http://www.globalzoology.org/ www.globalzoology.org]
}}
The '''International Society of Zoological Sciences (ISZS)''' was founded to encourage research, education, and communication in [[zoology]]. The society includes both individual scholars, and [[Professional association|professional organizations]]. It particularly tries to increase the availability of research resources and enhance cooperation between different [[Zoology#Branches of zoology|branches of zoology]].

== History ==

To summarize from Francis Dov Por's "The Road to the International Society of Zoological Sciences"<ref>http://www.globalzoology.org/index-new/road-to-iszs.htm</ref> "Zoology ... was among the very first disciplines to build an [[international organization]]. Congresses of Zoology started in 1889 on the initiative of the [[Société zoologique de France]] on the occasion of the [[Exposition Universelle (1889)|International Exposition]] in Paris. Congresses were then held at regular intervals and attendance increased from congress to congress, from a few tens in Paris and in Moscow to 700 in [[Budapest]] in 1927. The London Congress set a target of 1000 and in Washington the number reached 2500.

The Congresses were also growing in complexity, resulting in part because of the emerging [[Academic specialization|specialization]] of different fields of zoology. In London in 1958 there were eight to nine daily parallel sessions. In Washington an attempt was made to organize [[Academic conference|symposia]] instead and there were no less than 29: the problem of unifying subjects became more important than the numbers of participants.

The Washington Congress decided that the Board of the Division of Zoology of the newly founded [[IUBS]], would assume in the future the role of the 'Comite Permanent' and would be responsible for ensuring the continuity of Zoological Congresses. This did not work and the new International Congresses of Systematic and Evolutionary Biology (ICSEB) took over the role of the Zoological Congresses.

A gallant effort was made in 1972 by [[Étienne de la Vaissière|Vaissiere]] and French colleagues to convene a XVII International Congress of Zoology in [[Monte Carlo]]. However, attendance was poor and the proceedings were never published. A long hiatus then began. An international focus became the problem of [[Extinction|vanishing species]]. Instead, what followed in the next two decades was the vanishing of zoology from the international academic agenda.  The names of university departments and research centers were rapidly changed in order to avoid use of the word 'zoology'.

With the aid of [[Telecommunication#Modern telecommunication|modern communications]] and technology a reunification of the fractured specialties of zoology became possible. The concept of an integrative zoology, synthesizing data and results ranging from [[molecular biology]] to [[behavior]], gained wide acceptance. The XVIII International Congress of Zoology was held in [[Athens]] in Greece in August 2000. The symposium program of the Congress presented a cross-zoological picture of the many levels of zoological inquiry, both horizontal and vertical. Attendance in Athens was far from the incommunicable thousands in the last congresses, but was considered by all as an unmitigated success.

Subsequently, the IUBS approved the (re)formation of an international zoological body (the ISZS) as proposed by Zhibin Zhang, John Buckeridge and Francis Dov Por in Beijing in 2004, and an Executive Committee was elected and charged with organizing International Congresses of Zoology and providing a global voice for zoologists."

== Structure ==

As if at the end of 2013,the ISZS has 1032 individual members and 113 institutional members, having a coverage of over 30,000 zoologists,scientists and research workers all over the world. The Institutional Members include national zoological organizations, universities, non-governmental organizations and [[Academy of Sciences|academies of science]].

ISZS is managed by an international Executive Committee.<ref name="test">[http://www.globalzoology.org/dct/page/70010 Official site of the ISZS]</ref>  The current President is [[Zhibin Zhang]] (China). Immediate Past Presidents include: Jean-Marc Jallon (France), John Buckeridge (Australia) and Dov Por (Israel).

The Secretariat of the ISZS is located at C-506, Institute of Zoology, [[Chinese Academy of Sciences]], 1 Beichen West Road, Chaoyang District, Beijing 100101, China.The current Secretary General is Chunxu Han (China).

==Activities==

=== International Congresses of Zoology ===
Meetings of the [[International Congress of Zoology]] have been held every four  years, but with gaps and irregularities.
* 1889 Paris (I)<ref>Publication: Compte-rendu des séances du Congrès international de zoologie, OCLC 10503051</ref>
* 1892 Moscow (II)<ref>Publication: Congrès international de zoologie. Deuxième session, à Moscow du 10/22-18/30 août 1892. OCLC 10516783</ref>
* 1895 Leyden (III)<ref>Publication: Compte-rendu des séances du troisième Congres international de zoologie, Leyden, 16–21 septembre, 1895 OCLC 10502568</ref>
* 1898 Cambridge, UK (IV)<ref>Publication: Proceedings of the fourth International Congress of Zoology, Cambridge, 22–27 August 1898 OCLC 28701624</ref>
* 1901 Berlin (V)<ref>Publication: Verhandlungen des V. Internationalen Zoologen-Congresses zu Berlin, 12–16 August 1901. OCLC 10503154</ref>
* 1904 Berne (VI)<ref>Publication: Compte-rendu des séances du sixième Congrès international de zoologie, tenu à Berne du 14 au 16 août 1904  OCLC 10503186</ref>
* 1907 Boston (VII)<ref>Publication: VIIth International Congress of zoology, Boston, 1907 : proceedings. OCLC 233806914</ref>
* 1910 Graz (VIII)<ref>Publication: Verhandlungen des VIII. internationalen zoologenkongresses zu Graz, 15.-20 August 1910 OCLC  28701836</ref>
* 1913 Monaco (IX)<ref>Publication: IXe Congres international de zoologie tenu à Monaco, du 25 au 30 mars 1913 OCLC 53524546</ref>
* 1927 Budapest (X)<ref>Publication: Holznahrung und Symbiose : Vortrag gehalten auf dem X. Internationalen Zoologentag zu Budapest am 8 September 1927 OCLC 11966793</ref>
* 1930 Padova (XI)<ref>Publication: XIo Congresso internazionale di zoologia tenuto a Padova dal 4 all' 11 settembre 1930 ... Atti pubblicati a cura della presidenza. OCLC 10504999</ref>
* 1935 Lisbon (XII)<ref>Publication:  XII. International Zoological Congress. Programme, Lisbon, 15–21 September 1935 OCLC 12177803</ref>
* 1948 Paris (XIII)<ref>Publication: XIIIe Congrès international de zoologie, tenu a Paris du 21 au 27 juillet 1948. Comptes rendus. OCLC 10511365</ref>
* 1953 Copenhagen (XIV)<ref>Publication: Proceedings (des) 14. International Congress of Zoology, Copenhagen, 5. 12. Aug. 1953.</ref>
* 1958 London (XV)<ref>Published: 15th international congress of zoology : proceedings. OCLC 67675772</ref>
* 1963 Washington (XVI)<ref>Published Proceedings : XVI International Congress of Zoology, Washington, 20–27 August 1963)  OCLC 67208392</ref>
* 1972 Monte Carlo (XVII)<ref>Publication: XVIIe Congrès international de zoologie, Monte-Carlo, 25–30 septembre 1972. OCLC 10511502</ref>
* 2000 Athens, Greece (XVIII)<ref>Publication: The new panorama of animal evolution : proceedings, XVIII International Congress of Zoology = XVIIIème Congrés international de zoologie OCLC 52645678  ISBN 978-954-642-164-7</ref>
* 2004 Beijing, China (XIX)<ref>Publication: Summary report on the 19th International Congress of Zoology, Beijing, August 2004 in ''Integrative Zoology,'' 1, no. 1 (2006)</ref>
* 2008 Paris, France (XX)<ref>published: XX International Congress of Zoology, 26–29 August 2008 : abstracts from XX
International Congress of Zoology, hosted by the University Pierre et Marie Curie, University Paris-Sud and the Museum National d'Histoire Naturelle.</ref>
* 2012 Haifa, Israel (XXI)
* 2016 Japan (XXII)

=== International Symposia of Integrative Zoology ===
International Symposia of Integrative Zoology are held between Congresses, and provide a more intimate avenue for zoological exchange and research networking. They commenced in 2006 and are heavily supported by the Chinese Academy of Sciences.
* 2006 1st International Symposium of Integrative Zoology
* 2007 2nd International Symposium of Integrative Zoology
* 2009 3rd International Symposium of Integrative Zoology: Biological Consequences of Global Change and Darwin 200 Events
* 2010 4th International Symposium of Integrative Zoology: Data Collection and Sharing
* 2013 5th International Synposium of Integrative Zoology: Biological Consequences of Global Change (BCGC)

=== Scientific journal, Integrative Zoology ===
[[File:INZ Logo.gif|thumb|200px]]
[http://www.blackwellpublishing.com/INZ Integrative Zoology] is the official journal of the ISZS and published jointly by Wiley-Blackwell, [[John Wiley & Sons]] and the Institute of Zoology, [[Chinese Academy of Sciences]]. It is a quarterly, peer-reviewed and multidisciplinary [[Academic journal|journal]] publishing original research, reviews, essays and opinion pieces on the zoological sciences. The Editor-in-Chief is Zhibin Zhang of the [[Chinese Academy of Sciences]] and the Honorary Editor-in-Chief is John Buckeridge of [[RMIT University]].

==Research Programs==
The ISZS coordinates and funds a number of multidisciplinary programs. Currently, the society is undertaking an international research program titled 'Biological Consequences of Global Change (BCGC)'.

'''Focus'''

The focus of the BCGC program is to organize a diverse group of international experts, with expertise in many scientific disciplines, and develop an understanding of the consequences of global change and human activity on evolutionary mechanisms, biological structures, endangered species and biological disasters.

The BCGC program provides a platform for scientists around the world to collaborate on topics, such as the impact of global change on biodiversity, ecological infectious diseases, agricultural pests, invasive species and many other topics of interest. The program is expanding its research networks to include more scientists and scientific disciplines from various places around the globe.

 
'''Goals'''

Promote understanding of BCGC and improve management of the earth
Promote international collaboration
Promote influence and leadership of IUBS and ISZS through the BCGC program

 
'''Questions to address'''

Impact of global change on biological disasters like disease and pest outbreaks
Impact of global change and its relation to alien species invasion
Impact of global change on abundance and range shifts of endangered species
Impact of global change on community structure and biodiversity of various ecosystems
Nonlinearity and interactions of global change in affecting biological populations

'''Action plan for 2013–2015'''

Program network expansion
* Attract more scientists and partners
* Promote program influence and increase research capabilities
Website and database construction
* Build an open access database
* Promote data sharing among scientists
International symposia or workshops
* Organize 2 international symposia or workshops
* Discuss range shifts of species under global warming
* Examine biodiversity change due to human disturbance
Publications
* Publish 2 special issues of BCGC in Integrative Zoology
International training courses
* Organize 2 international training classes under the theme of BCGC

 
'''Get involved'''

To find out more information about how to get your research team involved
Visit www.globalzoology.org
Or email iszs2@ioz.ac.cn

==References==
{{Reflist}}

== External links ==
* [http://www.globalzoology.org Official site]  of the ISZS.
* [http://www.blackwellpublishing.com/INZ Integrative Zoology]

[[Category:Zoology organizations]]