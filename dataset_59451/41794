{{Infobox military conflict
| conflict   = Warsaw Airlift
| partof     = / In support of "[[Operation Tempest]]", World War II
| image      = [[File:Warsaw Uprising by RAF - Stolica 162.jpg|280px]]
| caption    = American Boeing B-17 Flying Fortress aircraft leaving Warsaw and heading East after air drops on September 18. Wisła river visible as well as Wilanów Palace gardens in upper left part of image.
| date       = August to September 1944
| place      = [[Warsaw]], [[Poland]]
| result     =
| combatant1 = {{flagicon image|Flaga PPP.svg}} [[Polish Underground State|Polish resistance]]<br/>
{{flagicon|Poland}}  [[No. 301 Polish Bomber Squadron|1568th Polish SD Flight]]<br/>
{{flagicon|United Kingdom}} [[No. 148 Squadron RAF]] <br/>
{{flagicon|United Kingdom}} [[No. 178 Squadron RAF]]  <br/>
{{flagicon|United Kingdom}} [[No. 624 Squadron RAF]]  <br/>
{{flagicon|South Africa|1928}} [[31 Squadron SAAF]]  <br/>
{{flagicon| South Africa|1928 }} [[34 Squadron SAAF]]  <br/>
{{flagicon|United States}} 3 Div [[8th Air Force]] [[USAAF]]  <br/>
{{flagicon| Soviet Union }} [[Soviet Air Forces]]
| combatant2 = {{flagicon|Nazi Germany}} [[Nazi Germany]]
| commander1  =
| commander2  =
| commander3  =
| strength1   =
| strength2   =
| casualties1 =
| casualties2 =
| casualties3 =
| campaignbox =
}}

The '''Warsaw Airlift''' was an [[Western allies|Allied]] air operation to re-supply the besieged [[Armia Krajowa|Polish Home Army]] (AK) in [[Warsaw]] during the [[World War II|Second World War]].  It took place between 4 August and 28 September 1944 and was conducted by Polish, British and South African aircraft flying from [[Foggia Airfield Complex|Celone]] and [[Brindisi]] in Italy and Soviet aircraft from occupied Ukraine.  One airdrop, launched on 18 September by United States aircraft was launched from Great Britain and the aircraft were forced to land at [[Poltava]] in Soviet Ukraine as the distance to the drop-zone precluded the aircraft returning to base.  The flights from Italy were night operations with low level cargo drops, conducted without fighter escort while the single [[United States Army Air Forces]] mission of 18 September 1944 was a high-altitude, daylight operation consisting of 107 [[Boeing B-17 Flying Fortress|B-17s]] protected by [[North American P-51 Mustang|P-51 fighters]].   From the night of 13/14 September the Soviets began their own airdrops, dropping about 130 tons in total until 27/28 September.  Initially, this cargo was dropped without parachutes, resulting in much of the payload being damaged or destroyed.

Allied aircraft dropped a total of 370 tons{{#tag:ref|239 tons by Western Allies and 130 tons by Soviet Air Forces|group="Note"}} of supplies in the course of the two months of operations, of which at least 50% fell into German hands. The airlift proved to be ineffective and could not provide sufficient supplies to sustain the Polish resistance, who were overrun by Nazi forces on 2 October 1944. The airlift was further hampered by the Soviet Union not allowing Western Allies the use of its airfields for several weeks,<ref>{{Cite episode |title= Pincers (August 1944 – March 1945) |episodelink= |url= |accessdate= |series= [[The World at War]] |serieslink= |first= |last= |network= [[ITV (TV network)|ITV]] |station= |city= |airdate= 20 March 1974 |began= |ended= |season= |seriesno= |number= 19 |minutes= 21 |time= |transcript= |transcripturl= |quote= Stalin was very suspicious of the underground, but it was utterly cruel that he wouldn't even try to get supplies in. He refused to let our aeroplanes fly and try to drop supplies for several weeks. And that was a shock to all of us. I think it played a role in all our minds as to the heartlessness of the Russians. [[W. Averell Harriman|Averell Harriman]] U.S. Ambassador to Russia 1943-46 |language=}}</ref> forcing flights to operate at extended ranges from Italy and Britain and in so doing, reducing payload and limiting the number of sorties. An estimated 360 airmen and 41 British, Polish, South African and American aircraft were lost.

==Background==
{{main|Warsaw Uprising}}
By the beginning of July 1944, [[Operation Bagration|Soviet forces had repelled the German formations over a wide front]], from Lithuania in the north [[Lvov-Sandomierz Offensive|to the Black Sea in the south]].  Vilnius capitulated to the Russians on 13 July and thereafter the main Soviet spearhead was headed towards the Vistula River.  Over the next two weeks, Brest-Litovsk as well as Lvov had fallen to the Soviets and then the Red Army swung north towards Warsaw.  By 1 August Soviet troops had entered the suburb of Praga east of the Vistula River.<ref name=Fritz418_419>Fritz (2011) pp.418-419</ref>  In their rush towards Warsaw, the Soviets had neglected intelligence collection, flank protection and had over-extended their supply lines<ref name="Fritz419">Fritz (2011) pp.419</ref> - to such an extent that a deliberate attack launched by General [[Walter Model]] stopped the Red Army's advance just short of Warsaw, preventing them from crossing the Vistula - this was after Hitler had finally consented to releasing four experienced and fresh panzer divisions to Model.{{#tag:ref| 3rd SS Panzer, 5th SS Panzer, Hermann Göring and Grossdeutschland |group="Note"}}  This action caused the Soviets to pause in order to re-group and bought Army Group Centre the time needed to deal with the resistance encountered within Warsaw itself.<ref name=Fritz419 />

In accordance with plans initiated by the [[Polish government-in-exile|London Poles]], General [[Tadeusz Bór-Komorowski]] launched the Polish Home Army insurrection on 1 August 1944 in an attempt to seize Warsaw from the Germans before it was overrun by the Red Army.<ref name=Taylor181_182>Taylor (1998) pp.181–182</ref> They managed to occupy large areas of downtown Warsaw but failed to secure the four bridges over the Vistula and were therefore unable to hold the eastern suburbs of the city.<ref name=Glantz212>Glantz (1995) pp.213</ref>  With the Red Army stalled on the Vistula, German counterattacks and insurrection suppression operations lead to the Polish Home Army (as well as the city) being  systematically destroyed.  The plight of the Poles captured the imagination of the Western Allies and the British [[Royal Air Force]] (RAF), [[South African Air Force]] (SAAF) and [[United States Army Air Forces]] (USAAF) were ordered to fly supplies in to the beleaguered Polish resistance.<ref name=Taylor181_182 />

Supplies were to be dropped in special waterproofed metal containers, {{convert|2.45|m|ft|abbr=on|1}} long and  {{convert|0.9|m|ft|abbr=on|1}} in diameter, weighing  {{convert|150|kg|lb|abbr=on|1}} each.  Each aircraft could carry 12 containers (a load of {{convert|1800|kg|lb|abbr=on|1}}) and with 20 aircraft per mission, it was hoped to deliver  {{convert|35.5|t|lb|abbr=on|0}}  of supplies per night.<ref name="Martin249_250">Martin (1978) pp.249–250</ref>  Missions from Italy would follow the route along the Vistula, accessing Warsaw from the south along the river using the four bridges across the river as their aiming reference points.{{#tag:ref|Poniatowski Bridge, the Railway Bridge between Eastern and Central Stations, the Kierbedzia Bridge between Old Town and the suburb of Praga and the Citadel Bridge.<ref name=Martin249_250 /> |group="Note"}} Supplies were to be released from a height of  {{convert|500|ft|m|abbr=on|1}} at an airspeed of  {{convert|225|km/h}} (the slow speed to avoid separation of parachutes from their containers).<ref name=Martin249_250 />

==Airlift operations==

===Missions from Italy===
Warsaw lay {{convert|1311|km|mi|abbr=on|1}} north east from the Allied bases in Apulia and Brindisi in Italy.  The route from Italy was planned to take the aircraft north east from their home airfields over the Adriatic and Croatia at sunset to reach the Danube in Hungary in darkness.  They would then climb north east over the Carpathians and into Soviet held territory, to approach Warsaw from the south east.  The return leg was routed over eastern Germany and eastern Austria with the aircraft arriving back at their point of origin by mid morning the following day.<ref name=Davies307_308>Davies (2004) pp.307–308</ref>  These aircraft flew without fighter escort and had to rely on their on-board armament to ward of German night fighters vectored in on their flight-paths by German ground based controllers.  A Luftwaffe night-fighter training school at [[Krakow]] presented a continual problem as did ground based [[Anti-aircraft warfare|AAA]] along the route.<ref name=Davies307_308 />  Aircraft also reported having been attacked by Russian fighters as well as Russian AAA close to Warsaw.<ref>Davies (2004), pp.309</ref>   Major General [[Jimmy Durrant]] of [[No. 205 Group RAF]] was in command of operations from Italy and assigned No. 334 Special Operations Wing RAF ([[No. 148 Squadron RAF|No. 148]] and [[No. 624 Squadron RAF]], each equipped with 14 Halifaxes and [[No. 1586 Flight RAF|1586 Polish Special Duty Flight]] equipped ten aircraft being a mixture of Halifaxes and B-24 Liberators) to supply Warsaw.  [[No. 178 Squadron RAF]] was later also assigned to support the airlift when [[No. 624 Squadron RAF|No. 624 Squadron]] was disbanded on 5 September 1944.<ref>{{cite web|last=History of War|title=No. 624 Squadron (RAF): Second World War|url=http://www.historyofwar.org/air/units/RAF/624_wwII.html|accessdate=10 January 2012}}</ref>{{#tag:ref|No. 148 and 178 Squadrons RAF contained many Australian air-crew.<ref name=Storr>{{cite web|last=Storr|first=Alan|title=Australian War Memorial|url=http://www.awm.gov.au/catalogue/research_centre/pdf/rc09125z006_1.pdf|work=RAAF World War II Fatalities: rc09125z006|accessdate=22 January 2012}}</ref>|group="Note"}}    2 Wing SAAF contributed 31 and 34 Squadrons for operations, both equipped with Liberators.<ref name="Davies310">Davies (2004), pp.310</ref>
[[File:Warsaw airlift air routes 1944.png|right|thumb|300px|<center>Air routes used for the airlift.<br>Black: Allied flights from Italy.<br>Black broken line: Later egress routes used back to Italy.<br>Blue: USAAF route</center>]]

The first air-drops from Italy were conducted by 1586 Polish Special Duty Flight accompanied by seven Halifaxes from [[No. 148 Squadron RAF]], successfully delivering their cargo to [[Krasiński Square]] and to Vola on the night of 4/5 August.<ref name=Davies310 /><ref>Davies (2004), pp.248</ref>  Flights continued through August and into early September when all flights were suspended due to bad weather.  This time was used to test a new bomb-sight which would allegedly have permitted more accurate supply delivery from a higher altitude.<ref>Davies (2004), pp.377 and 380</ref>  An aborted mission took place on 10/11 September with the last sorties taking place on 21/22 September, flown by 31 and 34 Squadrons SAAF as the Polish resistance was nearing total suppression by the Germans.<ref name=Orpen159 />

===USAAF mission===
At the [[Tehran Conference]] in November 1943, the Allied leaders had devised a new bombing strategy whereby American heavy bombers stationed in Britain and Italy would fly strike missions into central Germany and occupied Eastern Europe and would then land at secret American air bases (to be defended by the Soviets) located inside Soviet Russia.  Here the aircraft would be re-armed and fueled and would return to their home bases attacking second targets on the way home.  These operations went under the name of [[Operation Frantic]].<ref name=Russel27_28>{{cite web |last=Russell |first=Edward T. |year=1999|title=Leaping the Atlantic Wall: Army Air Forces Campaigns in Western Europe, 1942–1945 |url=http://www.usaaf.net/ww2/atlanticwall/awpg8.htm |work=[http://www.airforcehistory.hq.af.mil/ United States Air Force History and Museums Program] |publisher=USAAF.net |pages=27–28}}</ref>  However, on the night of 21/22 June 1944, German and Hungarian [[Heinkel He 111|He 111]] bombers had conducted a raid on one such airfield (Poltava in occupied Ukraine), destroying 43 B-17 Flying Fortresses on the ground.<ref>{{cite web|last=Rickard|first=J|title=History of War|url=http://www.historyofwar.org/articles/weapons_he111_combat.html|work=Heinkel He 111 Combat Record}}</ref> This raid left the Soviets "smarting and sensitive" and left the Americans determined to send their own anti-aircraft defenses as protection for the future. With these preceding events, the request for landing facilities issued by President Roosevelt on 14 August 1944 met with a brusque Soviet reply that the re-supply operations were a ''British and American affair'' to which the Soviet government could not object, but that no landing facilities would be granted to British or American aircraft once they had completed their mission over Warsaw.<ref name=Erickson283_4>Erickson (1983) pp.283–284</ref> Only after three weeks of negotiation by both Churchill and Roosevelt, was the final Soviet reply delivered to the British Ambassador in Moscow on 9 September 1944, stating that the Soviet Union would take no responsibility for what was happening in Warsaw, that they themselves would commence with their own air-supply missions and that American and British aircraft would be granted landing rights with prior arrangement.<ref>Erickson (1983) pp.287</ref>   The single mission flown by the USAAF took place on 18 September and due to the high altitude of the drop as well as strong prevailing winds, only 288 of the 1,284 containers dropped,<ref name=Craven317>Craven (1951) pp.317</ref><ref>Davies (2004), pp.377</ref> reaching the besieged Polish forces.<ref name="Erickson288">Erickson (1983) pp.288</ref>

There was a strong disposition in Allied circles to approve a second USAAF mission; the Polish premier in exile in London, [[Stanisław Mikołajczyk]] appealed to Churchill who telephoned the [[United States Strategic Air Forces in Europe]] on 27 September to request a second mission after which, Roosevelt too ordered a second [[Operation Frantic|Frantic]] delivery to Warsaw.  The second supply mission was never cleared by the Russians and Stalin formally refused permission on 2 October 1944.<ref name=Craven317 />

===Soviet missions===
On the night of 13 September 1944, Soviet aircraft commenced their own re-supply missions, dropping arms, medicines and food supplies. Initially these supplies were dropped in canisters without parachutes<ref>Davies (2004), pp.359</ref> which lead to damage and loss of the contents<ref name=Churchill>Churchill (1953) pp.144–145</ref> - also, a large number of canisters fell into German hands. Over the following two weeks, the Soviet Air Forces flew 2535 re-supply sorties with small bi-plane [[Polikarpov Po-2|Polikarpov Po-2's]], delivering a total of 156 50-mm mortars, 505 anti-tank rifles, 1478 sub-machine guns, 520 rifles, 669 carbines, 41 780 hand grenades, 37 216 mortar shells, over 3 mln. [[Cartridge (firearms)|cartridges]], 131.2 tons of food and 515&nbsp;kg of medicine.<ref>Доклад командования 1-го Белорусского фронта Верховному главнокомандующему И.В. Сталину о масштабах помощи повстанцам Варшавы от 02.10.1944 № 001013/оп (секретно)<br />цит. по: Зенон Клишко. Варшавское восстание. Статьи, речи, воспоминания, документы. М., Политиздат, 1969. стр.265-266.</ref><ref name=Erickson288 />

==Results and aftermath==
[[File:Plaque to south african flyer shot down in 2nd WW, Łódź Doły Cemetery.jpg|thumb|150px|upright|Plaque in memory of Herbert J. Brown, 31st squadron SAAF airman killed during the airlift: [[Łódź Doły Cemetery]], Poland]]

Even if the air-supply missions had delivered their full consignment of supplies, and these had reached their intended recipients, there is little likelihood that it would have altered the outcome of the Warsaw Uprising.<ref name=Taylor181_182 /> However, the Red Army did not move against Warsaw, even when the resistance was crushed and instead they cleared their flanks in the Balkans and the Baltic,<ref name=Taylor181_182 /> waiting for the total destruction of the non-Communist Poles.<ref name=Churchill />

The RAF and SAAF lost one aircraft for every ton of supplies delivered and a total of forty one allied aircraft were destroyed out of 306 dispatched.<ref name=Hastings>Hastings (2004) pp.118 and 120</ref>  In 1992, 67 ex-members of the SAAF 31 and 34 Squadrons were awarded the Polish [[Warsaw Cross of the Uprising]] for their role in the relief operations.<ref>{{cite web|title=South African Air Force (official website)|url=http://www.af.mil.za/about_us/history.html|work=History of the South African Air Force|publisher=South African Air Force}}</ref>
{{Clear}}

==Air Forces involved==
{|class="wikitable"
|style="background:#EBEBEB;" colspan="10"  align="center"|'''Forces and Equipment committed'''
|- valign="top"
!Air Force
!Attached Formation
!Squadron / Flight
!Aircraft
!Sorties
!Mission Type
!width=160pt|Mission Dates
!Aircraft Losses
|- valign="top"
|[[Polish Air Force]]
|rowspan="5"|[[Mediterranean Allied Air Forces]]
| [[No. 301 Polish Bomber Squadron|1586th Special Duty Flight]]<ref name=Shores77>Shores (1973) pp.77</ref>
|[[Consolidated B-24 Liberator|Consolidated Liberator Mk V]]<br/>[[Handley Page Halifax]]<ref name=Orpen149_150>Orpen (1984) pp.149–150</ref>
|rowspan="5" align=right|'''186'''<br>(attempted)<br/>'''92'''<br>(considered successful)<ref name="Orpen160">Orpen (1984), pp.160</ref>
|rowspan="7"|Re-supply
|4/5 August 1944<ref name=Davies310 /><br/> 8/9 August 1944<ref name=SAAFMuseum /><br/> 16/17 August 1944<ref name="Orpen131">Orpen (1984), pp.131</ref> <br/>26/27 August 1944<ref name=SAAFMuseum /><br/>27/28 August 1944<ref name=SAAFMuseum /> <br/>1/2 September 1944<ref>Orpen (1984), pp.150</ref>
|rowspan="5"|39<ref name=Hastings />
|- valign="top"
|[[Royal Air Force]]
|[[No. 148 Squadron RAF]]
|[[Handley Page Halifax]]<ref name=Davies310 /> <br/> [[Consolidated B-24 Liberator|Consolidated Liberator]]<ref name=Shores77 />
| 4/5 August 1944<ref name=Davies310 /> <br/>14/15 August 1944<ref>Orpen (1984), pp.103</ref><br/>16/17 August 1944<ref name=Orpen131 />
|- valign="top"
|[[Royal Air Force]]
|[[No. 178 Squadron RAF]]
| [[Consolidated B-24 Liberator|Consolidated Liberator Mk. VI]]
|13/14 August 1944<ref name=SAAFMuseum /><br/> 14/15 August 1944<ref name=SAAFMuseum /> <br/>16/17 August 1944<ref name=Orpen131 /> <br/>10/11 September 1944<ref>Orpen (1984), pp.153</ref>
|- valign="top"
|[[South African Air Force]]
|[[31 Squadron SAAF]]
|[[Consolidated B-24 Liberator|Consolidated Liberator]]
|13/14 August 1944<ref name=SAAFMuseum>{{cite web|last=Shell|first=Charlie|title=South African Air force Museum|url=http://www.saafmuseum.org.za/features/2011/398/|work=The SAAF and the Warsaw Flights}}</ref><br/> 14/15 August 1944<ref name=Shores77 /><br/>  15/16 August 1944<ref name=Shores77 /><br/>  16/17 August 1944<ref name=Shores77 /> <br/>  21/22 September 1944<ref name="Orpen159">Orpen (1984), pp.159</ref>
|- valign="top"
|[[South African Air Force]]
|[[34 Squadron SAAF]]
|[[Consolidated B-24 Liberator|Consolidated Liberator]]
|16/17 August 1944<ref name=SAAFMuseum /><br/>10/11 September 1944<ref name=SAAFMuseum /><br/>15/16 September 1944<ref name=SAAFMuseum /> <br/>  21/22 September 1944<ref name=Orpen159 />
|- valign="top"
|[[United States Army Air Forces]]
|[[8th Air Force]]
|Aircraft from:<br/>[[95th Air Base Wing|95th Bombardment Group (Heavy)]]<ref name="Orpen158">Orpen (1984), pp.158</ref> <br/>100th Bomber Group<ref name=Orpen158 />  <br/>390th Bomber Group<ref name=Orpen158 /><br/>[[355th Fighter Wing|355th Fighter Group]]{{#tag:ref|The 355th Fighter Group provided sixty P-51D Mustang escorts for the 107 Flying Fortresses.<ref name=Orpen158 /> |group="Note"}}
|[[Boeing B-17 Flying Fortress]]
|align=right|'''1+2'''{{#tag:ref|One comprising B-17s (43-38175 390th BG 568th BS) and two consisting of P-51's (42-26386, 44-19735, 355th FG 386th/368th FS)|group="Note"}}
|18 September 1944
|2<ref name="Orpen159"/>
|- valign="top"
|- valign="top"
|[[Soviet Air Forces]]
|
|
|[[Polikarpov Po-2]]
|align=right|'''2,000+'''<ref name=Erickson288 />
|13–27 September 1944<ref name=Erickson288 />
|Unknown
|- valign="top"
|[[Royal Air Force]]
| [[Mediterranean Allied Air Forces]]
|[[No. 267 Squadron RAF]]<ref name=Shores78 />
|[[Douglas C-47 Skytrain|Douglas Dakota]]<ref name=Shores78 />
|align=right|'''3'''<ref name="Shores78">Shores (1973), pp.78</ref>
|Agent infiltration<ref name=Shores78 />
|September 1944
|None
|- valign="top"
|}

==See also==
* [[Lack of outside support during the Warsaw Uprising]]
* [[Military history of the Warsaw Uprising]]
* [[Operation Frantic]]
* [[Prelude to the Warsaw Uprising]]
* [[Warsaw Uprising]]

==Notes and references==
;Footnotes
{{Reflist|group=Note|2}}

;Citations
{{Reflist|2}}

;Bibliography
* {{cite book|last1=Craven (Ed)|first1=Wesley Frank|last2=Cate|first2=James Lea|title=The Army Air Forces in World War II Volume III, Europe: Argument to V-E Day: January 1944 to May 1945|year=1951|publisher=University of Chicago Press|location=Chicago}}
* {{cite book|last=Churchill|first=Winston S.|title=The Second World War:  Triumph and Tragedy|year=1953|publisher=Houghton Mifflin Company|location=Boston}}
* {{cite book|last=Davies|first=Norman|title=Rising '44:  The Battle for Warsaw|year=2004|publisher=Pan MacMillan|location=London|isbn=978-0-330-48863-1}}
* {{cite book|last=Erickson|first=John|title=The Road to Berlin: Stalin's War with Germany: Volume 2|year=1983|publisher=Weidenfeld and Nicolson|location=London|isbn=0-297-77238-4}}
* {{cite book|last=Fritz|first=Stephen G.|title=Ostkrieg: Hitlers War of Extermination in the East|year=2011|publisher=University Press of Kentucky|location=Louisville|isbn=978-0-8131-3416-1}}
* {{cite book|last1=Glantz|first1=David. M.|last2=House|first2=Jonathan M.|title=When Titans Clashed: How the Red Army stopped Hitler|year=1995|publisher=University Press of Kansas|location=Lawrence, Kansas|isbn=0-7006-0717-X}}
* {{cite book|last=Hastings|first=Max|title=Armageddon:  The Battle for Germany 1944-1945|year=2004|publisher=Pan MacMillan|location=London|isbn=978-0-330-49062-7}}
* {{cite book|last=Martin|first=H.J. Lt-Gen|title=Eagles Victorious: South African Forces World War II|year=1978|publisher=Purnell|location=Cape Town|isbn=0-86843-008-0|author2=Orpen, N.D. }}
* {{cite web |last=Russell |first=Edward T. |year=1999|title=Leaping the Atlantic Wall: Army Air Forces Campaigns in Western Europe, 1942–1945 |url=http://www.usaaf.net/ww2/atlanticwall/awpg8.htm|work=[http://www.airforcehistory.hq.af.mil/ United States Air Force History and Museums Program] |publisher=USAAF.net}}
* {{cite book|last=Shores|first=Christopher F.|title=Pictoral History of the Mediterranean Air War: Volume II|year=1973|publisher=Ian Allan Ltd|location=Shepperton, Surry|isbn=0-7110-0433-1}}
* {{cite book|last=Taylor|first=A.J.P.|title=The Second World War and its Aftermath|year=1998|publisher=Folio Society|location=London}}

==External links==
* [http://www.nationalmuseum.af.mil/factsheets/factsheet_media.asp?fsID=1653 Operation Frantic Soviet-American Photos]
* [http://samilitaryhistory.org/vol131pm.html South African Military History Society: The Warsaw Airlift, A Triumph of SA Bravery by Dr Moller] and [http://samilitaryhistory.org/vol062ve.html The Warsaw Airlift: Major J.L. van Eyssen, DFC]
* [http://www.warsawuprising.com/res.htm Warsaw Uprising website]

[[Category:Warsaw Uprising|Airlift]]
[[Category:Battles and operations of World War II]]
[[Category:Non-combat military operations involving the United States]]
[[Category:Non-combat military operations involving the United Kingdom]]
[[Category:Soviet Union–United Kingdom relations]]
[[Category:Soviet Union–United States relations]]
[[Category:Airbridge]]