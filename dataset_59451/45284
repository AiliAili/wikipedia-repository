{{Use British English|date=October 2016}}
{{Use dmy dates|date=June 2013}}
{{Infobox writer <!-- for more information see [[:Template:Infobox writer/doc]] -->
| name = Dame Barbara Cartland <br /><small>[[Order of the British Empire|DBE]] [[Venerable Order of Saint John|CStJ]]</small>
| image = Barbara Cartland in 1925.jpg
| birth_name = Mary Barbara Hamilton Cartland
| birth_date = 9 July 1901
| birth_place = [[Edgbaston]], [[Birmingham]], England
| death_date = {{Death date and age|2000|5|21|1901|7|9|df=y}}
| death_place = Camfield Place near [[Hatfield, Hertfordshire]], England
| spouse = Alexander McCorquodale (m. 1927–1933)<br />Hugh McCorquodale (m. 1936–1963)
| nationality = English
| period = 1925–2000
| occupation = Novelist
| genre = [[Romance novel|Romance]]
| children = [[Raine Spencer, Countess Spencer|Raine McCorquodale]]<br />Ian Hamilton McCorquodale<br />Glen McCorquodale
| relatives = [[Diana, Princess of Wales]] (step-granddaughter)}}
'''Dame Barbara Cartland''' [[Order of the British Empire|DBE]] [[Venerable Order of St John|CStJ]] (9 July 1901&nbsp;– 21 May 2000), born as  '''Mary Barbara Hamilton Cartland''', was an English author of romance novels, one of the [[List of best-selling fiction authors|best-selling authors]] as well as one of the [[List of prolific writers|most prolific]] and commercially successful worldwide of the twentieth century. Her 723 novels were translated into 38 languages and she continues to be referenced in the ''[[Guinness World Records]]'' for the most novels published in a single year in 1976.<ref>{{cite news| url=https://www.nytimes.com/2000/05/22/books/barbara-cartland-98-best-selling-author-who-prized-old-fashioned-romance-dies.html?pagewanted=all | work=New York Times | title=Barbara Cartland, 98, Best-Selling Author Who Prized Old-Fashioned Romance, Dies |first=Richard |last=Severo |date=May 22, 2000}}</ref> As '''Barbara Cartland''' she is known for her numerous [[romance novel|romantic novels]] but she also wrote under her married name of '''Barbara McCorquodale''' and briefly under the pseudonym of '''Marcus Belfry'''. She wrote more than 700 books,<ref name="telegraph" /> as well as plays, music, verse, drama, magazine articles and operetta, and was a prominent philanthropist. She reportedly sold more than 750 million copies.<ref name="telegraph" /> Other sources estimate her book sales at more than two billion copies.<ref>{{cite news |url=http://www.cbsnews.com/stories/2000/12/20/2000/main258620.shtml?source=RSS&attr=_258620 |title=Final Curtain Calls |date=20 December 2000 |publisher=CBS News|language=|accessdate=21 May 2010}}</ref> She specialised in 19th-century [[Victorian era]] pure romance. Her novels all featured portrait-style artwork, particularly the cover art, usually designed by Frances Marshall.

As head of Cartland Promotions, she also became one of London's most prominent society figures, of the latter always presenting herself in pink gown and plumed hat, she was one of Britain's most popular media personalities, right up until her death in 2000.<ref name="telegraph">{{cite news |url=http://www.telegraph.co.uk/news/obituaries/culture-obituaries/books-obituaries/1366803/Dame-Barbara-Cartland.html |title=Dame Barbara Cartland |publisher=[[The Daily Telegraph]] |accessdate=22 April 2013 |date=22 May 2000 |location=London}}</ref>

==Early life==
Born Mary Barbara Hamilton Cartland at 31 Augustus Road, [[Edgbaston]], Birmingham, England, Cartland was the only daughter and eldest child of a British army officer, Major Bertram "Bertie" Cartland<ref>{{Cite web|title=CARTLAND, JAMES BERTRAM FALKNER|url=http://www.cwgc.org/search/casualty_details.aspx?casualty=725611 |publisher=[[Commonwealth War Graves Commission]]}}</ref> (born James Bertram Falkner Cartland 1876; died 27 May 1918, [[Berry-au-Bac]]), and his wife, [[Mary Hamilton Scobell]], known as "Polly" (1877–1976). Cartland had two brothers: [[Ronald Cartland]], a Member of Parliament (born in 1907), and James Anthony "Tony"  Hamilton Cartland, (born in 1912). Though she was born into an enviable degree of middle-class comfort, the family's security was severely shaken after the [[suicide]] of her paternal grandfather, James Cartland, a financier, who shot himself in the wake of bankruptcy. According to the entry in the probate registry he left £92,000, suggesting little evidence of bankruptcy.<ref name="telegraph" /> 

This was followed soon afterward by her father's death on a [[Flanders]] battlefield, in [[World War I]]. However, Cartland's enterprising mother opened a London dress shop to make ends meet, and to raise Cartland and her two brothers, both of whom were eventually [[killed in action|killed in battle]] in 1940.<ref name=BU>{{cite web |title=Cartland, Barbara |url=http://www.bu.edu/phpbin/archives-cc/app/details.php?id=7540 |website=[[Howard Gotlieb Archival Research Center]] |publisher=[[Boston University]] |accessdate=22 April 2013 |archive-url=https://web.archive.org/web/20121109225543/http://www.bu.edu/phpbin/archives-cc/app/details.php?id=7540 |archive-date=November 9, 2012 }}</ref>

Cartland was educated at private girls' schools: [[The Alice Ottley School]], [[Malvern Girls' College]], and Abbey House, an educational institution in [[Hampshire]], Cartland soon became successful as a society reporter after 1922, and a writer of romantic fiction. Cartland admitted she was inspired in her early work by the novels of Edwardian author [[Elinor Glyn]], whom she idolized and eventually befriended.

==Marriage and relationships==
According to an obituary published in ''[[The Daily Telegraph]]'',<ref name="telegraph" /> Cartland reportedly broke off her first engagement, to a Guards officer, when she learned about [[sexual intercourse]] and recoiled. This claim fits with her image as part of a generation for whom such matters were never discussed, but sits uneasily with her having produced work controversial at the time for its sexual subject matter, as described above. She was married to Captain Alexander "Sachie" George McCorquodale, on 23 April 1927, a British Army officer from Scotland and heir to a printing fortune. They divorced in 1933,<ref name="a">{{cite news|work=Daily Mail|title=A drunken husband and five secret lovers: The novel Barbara Cartland never wanted you to read|last=Thornton |first= Michael |date=24 October 2008 |url=http://www.dailymail.co.uk/femail/article-1080454/A-drunken-husband-secret-lovers-The-novel-Barbara-Cartland-wanted-read.html}}</ref> and he died from heart failure in 1964.<ref name="telegraph" />

Their daughter, [[Raine Spencer, Countess Spencer|Raine McCorquodale]] (9 September 1929 – 21 October 2016), who Cartland later alleged was the daughter of [[George Sutherland-Leveson-Gower, 5th Duke of Sutherland]] or [[Prince George, Duke of Kent]], became "Deb of the Year" in 1947. After the McCorquodales' 1933 divorce, which involved charges and countercharges of infidelity, Cartland married her former husband's cousin, Hugh McCorquodale, on 28 December 1936, in Guildfield, London a former military officer. Cartland and her second husband, who died in 1963, had two sons, Ian McCorquodale (born 11 October 1937), a former [[Debretts]] publisher, and Glen McCorquodale (born 1939), a stockbroker.<ref name="BU" /><ref name="telegraph" />

Cartland maintained a long friendship with [[Louis Mountbatten, 1st Earl Mountbatten of Burma|Lord Mountbatten of Burma]], whose 1979 death she said was the "greatest sadness of my life". Mountbatten supported Cartland in her various charitable works, particularly for [[United World Colleges]], and even helped her write her book ''Love at the Helm'', providing background naval and historical information. The Mountbatten Memorial Trust, established by Mountbatten's great-nephew [[Charles, Prince of Wales]] after Mountbatten was assassinated in Ireland, was the recipient of the proceeds of this book on its release in 1980.{{cn|date=May 2016}}

Cartland did not get on with her step-granddaughter [[Diana, Princess of Wales]], who notably did not invite Cartland to [[Wedding of Charles, Prince of Wales, and Lady Diana Spencer|her wedding]] to the [[Prince of Wales]]. Cartland was openly critical of Diana's subsequent divorce, though the rift between them was mended shortly before [[Death of Princess Diana|Diana's fatal car crash]] in Paris, in 1997.<ref name="DailyMail18Oct2008">{{cite news| url=http://www.dailymail.co.uk/femail/article-1078685/Oh-mummy-naughty--Dame-Barbara-Cartlands-son-reveals-racy-life.html | location=London | work=Daily Mail | title=Oh, mummy you were naughty - Dame Barbara Cartland's son reveals all about her racy life | date=18 October 2008 | first= Kathryn | last=Knight |accessdate=May 20, 2016 }}</ref> According to [[Tina Brown]]'s book on the late Princess, Cartland once remarked, "The only books Diana ever read were mine, and they weren't awfully good for her."<ref>{{Cite web
| url   = http://www.dailymail.co.uk/debate/article-2393972/With-160-lost-romances-published--Im-proud-feminist-Barbara-Cartland-sets-heart-flutter-FRANCES-WILSON.html | title = With 160 of her lost romances about to be published&nbsp;... I'm a proud feminist but Barbara Cartland still sets my heart a-flutter | last  = Wilson | first = Frances | accessdate = 11 February 2010 | location=London | website=Daily Mail|date=16 August 2013}}</ref>
[[File:Dame Barbara Cartland Allan Warren.jpg|thumb|right|Dame Barbara Cartland]]
==Novels==
{{See also|Barbara Cartland bibliography}}

After a year as a gossip columnist for the ''[[Daily Express]]'', Cartland published her first novel, ''Jigsaw'' (1923), a risqué society thriller that became a bestseller. She also began writing and producing somewhat racy plays, one of which, ''Blood Money'' (1926), was banned by the [[Lord Chamberlain's Office]]. In the 1920s and 1930s, Cartland was a prominent young hostess in London society, noted for her beauty, energetic charm, and daring parties. Her fashion sense also had a part, and she was one of the first clients of designer [[Norman Hartnell]]; she remained a client until he died in 1979. He made her presentation and wedding dresses; the latter was made to her own design against Hartnell's wishes, and she admitted it was a failure.

In 1950, Cartland was accused of plagiarism by author [[Georgette Heyer]], after a reader drew attention to the apparent borrowing of Heyer's character names, character traits, dialogue, and plot points in Cartland's early historical romances. In particular, ''A Hazard of Hearts'' (1949), which replicated characters (including names) from Heyer's ''[[Friday's Child (novel)|Friday's Child]]'' (1944) and ''The Knave of Hearts'' (1950) which, Heyer alleged, "the conception&nbsp;... , the principal characters, and many of the incidents, derive directly from an early book of my own, entitled ''[[These Old Shades]]'', first published in 1926.&nbsp;... For minor situations and other characters she has drawn upon four of my other novels." Heyer completed a detailed analysis of the alleged plagiarisms for her solicitors, but the case never came to court.<ref name="Kloester275">Kloester, Jennifer (2012). ''Georgette Heyer: Biography of a Bestseller''. London: William Heinemann. ISBN 978-0-434-02071-3. pp. 275–79.</ref>

Cartland's image as a self-appointed "expert" on romance drew some ridicule in her later years, when her social views became more conservative. Indeed, although her first novels were considered sensational, Cartland's later (and arguably most popular) titles were comparatively tame with virginal heroines and few, if any, suggestive situations. Almost all of Cartland's later books were historical in theme, which allowed for the believability of chastity (at least, to many of her readers).

Despite their tame storylines, Cartland's later novels were highly successful. By 1983, she rated the longest entry in ''[[Who's Who (UK)|Who's Who]]'' (though most of that article was a list of her books), and she was named the top-selling author in the world by the ''[[Guinness World Records]]''.{{cn|date=May 2016}} Additionally, in 1983, Cartland wrote 23 novels, earning her the Guinness World Record for the most novels written in a single year.{{cn|date=May 2016}}{{cn|date=May 2016}} 

In the mid-1990s, by which time she had sold over a billion books, ''[[Vogue (magazine)|Vogue]]'' called Cartland "the true Queen of Romance". She became a mainstay of the popular media in her trademark pink dresses and plumed hats, discoursing on matters of [[love]], [[marriage]], politics, religion, health, and fashion. She was publicly opposed to the removal of [[prayer]] from state schools, and spoke against [[infidelity]] and [[divorce]], although she admitted to being acquainted with both of these subjects.

==Contribution to aviation==
Privately, Cartland took an interest in the early [[gliding]] movement. Although [[aerotowing]] for launching gliders first occurred in Germany, she thought of long-distance tows in 1931 and did a 200-mile (360&nbsp;km) tow in a two-seater glider.  The idea led to [[Military glider|troop-carrying gliders]]. In 1984, she was awarded the Bishop Wright Air Industry Award for this contribution.<ref>[http://www.barbaracartland.com/static/life.aspx Official Website: Life Story]</ref>

She regularly attended [[Brooklands]] aerodrome and motor-racing circuit during the 1920s and 30s, and the [[Brooklands Museum]] has preserved a sitting-room from that era and named it after her.

==Political influence==
After the death during [[World War II]] of her brother [[Ronald Cartland]], a [[Conservative Party (UK)|Conservative]] [[Member of Parliament]], Cartland published a biography of him with a preface by the [[Prime Minister of the United Kingdom|Prime Minister]], [[Winston Churchill]].{{cn|date=May 2016}}

The war marked the beginning of a lifelong interest in civic welfare and politics for Cartland, who served the [[War Office]] in various charitable capacities as well as the [[St. John Ambulance Brigade]]. In 1953, she was invested at [[Buckingham Palace]] as a [[Order of Saint John (chartered 1888)|Commander of the Order of St. John of Jerusalem]] for her services.{{cn|date=May 2016}}

In 1955, Cartland was elected a councillor on [[Hertfordshire County Council]]<ref name="BU" /> as a Conservative and served for nine years. During this time she campaigned successfully for nursing home reform, improvement in the salaries of midwives, and the legalization of education for the children of [[Gypsy|Gypsies]]. She also founded the National Association of Health, promoting a variety of medications and remedies, including an [[anti-aging cream]] and a so-called "brain pill" for increasing mental energy.{{cn|date=May 2016}}

==Music==
In 1978, Cartland released ''An Album of Love Songs'' through [[State Records]], produced by Sir [[Norman Newell]].<ref>{{Cite web|url=https://itunes.apple.com/gb/album/album-love-songs-feat.-royal/id536072058|title=Album of Love Songs (feat. Royal Philharmonic Orchestra|publisher=[[iTunes]]}}</ref> The album featured Cartland performing a series of popular standards with the [[Royal Philharmonic Orchestra]], including "I'll Follow My Secret Heart" and "[[A Nightingale Sang in Berkeley Square (song)|A Nightingale Sang in Berkeley Square]]".<ref>http://www.popmusic4synch.com/music/index.php?search_artist_id=50003{{broken link|date=March 2017}}</ref>

==Honours==
In January, 1988, Cartland received the Médaille de [[Vermeil]] de la Ville de Paris, the highest honour of the city of Paris, for publishing 25 million books in France.{{cn|date=May 2016}}

In 1991, Cartland was invested by [[Elizabeth II|Queen Elizabeth II]] as a Dame Commander of the [[Order of the British Empire]] in honour of the author's almost 70 years of literary, political, and social contributions.<ref name="BU" />

A waxwork of Cartland was on display at [[Madame Tussauds]], though according to her son Ian, Cartland was displeased because it wasn't "pretty enough".<ref name="DailyMail18Oct2008" />

==Interviews in later life==
[[File:Cartland-Bigham-last-year (1).jpg|thumb|right|''Dame'' Barbara Cartland with reporter Randy Bryan Bigham, in one of her last photos, 2000]]

Cartland's physical and mental health, particularly eyesight, began to fail in her mid-90s,{{cn|date=May 2016}} but she remained a favourite with the press, granting interviews to international news agencies even during the final months of her life.<ref name="a" /> Two notable interviews were with the [[BBC]]{{when|date=May 2016}}{{cn|date=May 2016}} and U.S. journalist Randy Bryan Bigham, in 2000 (see image).{{cn|date=May 2016}}

==Death and legacy==
Cartland died peacefully in her sleep, on 21 May 2000, seven weeks before her 99th birthday (and the same day as prominent British actor [[Sir John Gielgud]]), at her residence, Camfield Place, near [[Hatfield, Hertfordshire]]. She had been suffering from ill health and dementia for six months beforehand, and was subsequently bedridden and sequestered.<ref name="a" /> Both of her sons, Ian and Glen McCorquodale, were present at her bedside when she died. Shortly afterward, Cartland's daughter from her first marriage, Raine, travelled to the family home.<ref>{{cite news
| url   = http://www.telegraph.co.uk/culture/books/10252426/Barbara-Cartland-My-mum-always-played-the-heroine.html | title = Barbara Cartland: My mum always played the heroine | last  = Levin | first = Angela | accessdate = 3 May 2014 | work=Daily Telegraph | date=19 Aug 2013 }}</ref> 

After originally deciding she would like to be buried in her local parish church, featuring a coffin of marble construction, covered in angels, this was later changed; Cartland was buried in a cardboard coffin, because of her concerns for environmental issues.<ref>{{cite news |last=Rowe |first=Mark |title=Undertakers Say No to Green Burials; Cardboard Coffins May Be Good for the Environment, but They Are Much Less Profitable Than Traditional Ceremonies |date=25 June 2000 |url=http://www.highbeam.com/doc/1P2-5077391.html |work=The Independent |accessdate=2 May 2014}} {{subscription|via=[[Highbeam]]}}</ref> She was interred at her private estate in [[Hatfield, Hertfordshire]], under an oak that had been planted by [[Elizabeth I of England|Queen Elizabeth I]].<ref name="a" />

==Posthumous publications==
Cartland left behind a series of 160 unpublished novels, known as the ''Barbara Cartland Pink Collection''. These are being published in ebook format by her son Ian McCorquodale; each month, a new novel is published from the collection.<ref>{{cite web|website=BarbaraCartland.com|title=The Pink Collection|url=http://www.barbaracartland.com/static/pink.aspx}}</ref> 

In 2010, to mark the 10th anniversary of her death, Cartland's first novel, ''Jig-Saw'' (first published in 1925), was reprinted.<ref>{{cite book|title=Jig-Saw|author=Cartland, Barbara|publisher= Barbara Cartland.com |date=4 November 2010|isbn=978-1906950200}}</ref>

"As a tribute to Her Majesty the Queen on her [[Diamond Jubilee]] and to Barbara’s enduring appeal to romantics everywhere, her publishers have re-released her catalogue collection, entitled - "The Eternal Collection." This collection, released beginning November 27, 2013, includes such backlist titles as ''Elizabethan Lover'', ''The Little Pretender'', ''[[A Ghost in Monte Carlo]]'', and ''A Duel of Hearts'', which were novels all published at the time [[Queen Elizabeth II]] ascended to the throne in 1952.<ref>{{cite web|website=BarbaraCartland.com|title=Newsflash: THE BARBARA CARTLAND ESTATE RELEASES HER GREATEST ROMANCES AS E-BOOKS|url=http://www.barbaracartland.com/static/newsflash.aspx}}</ref><ref>{{cite book|author=Cartland, Barbara|title=Introduction to the Eternal Collection|publisher= Barbara Cartland.Ebooks ltd|edition= First |date=November 27, 2013|asin=
 B008654SO0}}</ref>

In addition, her collections of [[ebooks]] are available in Spanish, Italian and German.

==Feature films==
[[BBC Four]] aired a biopic drama film, titled ''[[In Love with Barbara]]'' (26 October 2008), starring [[Anne Reid]] as Cartland and [[David Warner (actor)|David Warner]] as Lord Mountbatten. The film was written by [[Jacquetta May]]

Her last project was to be filmed and interviewed for her life story (directed by Steven Glen for Blue Melon Films). The documentary, ''Virgins and Heroes'', includes unique early home [[Cine film|ciné]] footage and Dame Barbara launching her website with pink computers, in early 2000.<ref>{{cite book|author=Glen, Steven (Director)|publisher=Blue Melon Films|title=Virgins and Heroes|url=https://www.youtube.com/watch?v=K4PeMJMMuiM}}</ref> At that time, her publishers estimated that since her writing career began in 1925, Cartland had produced a total of 723 titles.<ref name="DailyMail18Oct2008" />

==References==
{{reflist|30em}}

==External links==
{{Wikiquote}}
* [http://www.barbaracartland.com Official website]
* [http://www.bigredbook.info/barbara_cartland_1.html Barbara Cartland's appearance on This Is Your Life]

===Archives===
* Some papers of Barbara Cartland are held at [[The Women's Library]] at the [http://www.lse.ac.uk/library/Home.aspx Library of the London School of Economics], ref [http://twl-calm.library.lse.ac.uk/CalmView/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqCmd=Overview.tcl&dsqSearch=(RefNo='7BCA')  7BCA]

{{Authority control}}

{{DEFAULTSORT:Cartland, Barbara}}
[[Category:1901 births]]
[[Category:2000 deaths]]
[[Category:People from Edgbaston]]
[[Category:People from Welwyn Hatfield (district)]]
[[Category:People educated at Malvern St James]]
[[Category:People educated at The Alice Ottley School]]
[[Category:Brooklands people]]
[[Category:Burials in Hertfordshire]]
[[Category:British chick lit writers]]
[[Category:Conservative Party (UK) politicians]]
[[Category:Councillors in Hertfordshire]]
[[Category:Dames Commander of the Order of the British Empire]]
[[Category:Commanders of the Order of St John]]
[[Category:English aviators]]
[[Category:English romantic fiction writers]]
[[Category:Female aviators]]
[[Category:People associated with Malvern, Worcestershire]]
[[Category:Writers from Birmingham, West Midlands]]
[[Category:20th-century English novelists]]
[[Category:20th-century women writers]]
[[Category:English women novelists]]
[[Category:English people of Scottish descent]]
[[Category:English people of American descent]]
[[Category:Women romantic fiction writers]]
[[Category:British female aviators]]