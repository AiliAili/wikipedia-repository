{{About|a fighter aircraft of the 1920s|the main battle tank|Type 10}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = 1MF 
  |image = Mitsubishi 1MF3A.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Carrier Fighter
  |manufacturer = [[Mitsubishi Internal Combustion]]
  |designer = [[Herbert Smith (engineer)|Herbert Smith]] 
  |first flight = 1921
  |introduced = 1923
  |retired = 1930
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Imperial Japanese Navy]] 
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 138
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Mitsubishi 1MF''' was a [[Empire of Japan|Japanese]] [[Aircraft carrier|carrier]] [[fighter aircraft]] of the 1920s. Designed for the [[Mitsubishi Aircraft Company]] by the [[United Kingdom|British]] aircraft designer [[Herbert Smith (engineer)|Herbert Smith]], the 1MF, also known as the '''Navy Type 10 Carrier Fighter''' was operated by the [[Imperial Japanese Navy]] from 1923 to 1930.

==Development and design==
The [[Empire of Japan|Japanese]] shipbuilding company [[Mitsubishi|Mitsubishi Shipbuilding and Engineering Co Ltd]] set up a subsidiary company, the [[Mitsubishi Internal Combustion|Mitsubishi Internal Combustion Engine Manufacturing Co Ltd]] (''Mitsubishi Nainenki Seizo KK'') in 1920 to produce aircraft and [[automobile]]s at [[Nagoya]]. It quickly gained a contract from the [[Imperial Japanese Navy]] to produce three types of aircraft for operation from [[aircraft carrier]]s: a fighter, a [[torpedo bomber]] and a [[reconnaissance aircraft]]. To produce these aircraft, it hired [[Herbert Smith (engineer)|Herbert Smith]], formerly of the [[Sopwith Aviation Company]] to assist the design of these aircraft, Smith bringing to Japan Jack Hyland and a team of six other British engineers .<ref name="Mikesh">{{cite book |last= Mikesh|first= Robert C|author2=Abe, Shorzoe |title= Japanese Aircraft 1910-1941|year= 1990|publisher= Putnam Aeronautical Books|location=London |isbn= 0-85177-840-2}}</ref>

The fighter designed by Smith and his team, designated the '''1MF''' by Mitsubishi, and known as the '''Navy Type 10 Carrier Fighter''' by the Japanese Navy (referring to the year of design of 1921, the 10th year of the [[Taishō period]]), first flew in October 1921.<ref name="Mikesh"/>

The 1MF was a single-seat, single-bay [[biplane]] with unequal-span wings and all-wooden construction, powered by a {{convert|224|kW|abbr=on}} [[Hispano-Suiza 8]] engine (license produced as the [[Mitsubishi Hi]] engine). It was fitted with claw-type [[Arresting gear#Components|arrestor gear]] for use with British-style fore and aft arrestor cables.<ref name="johan1MF">{{cite web |url= http://www.j-aircraft.com/drawings/johan/1mf.htm |title=Mitsubishi Type 10 Carrier Fighter - Brief history of the Type 10 |accessdate=2007-08-09|author=Johan Myhrman |work= j-aircraft.com}}</ref>

After successful flight testing, the aircraft was accepted by the Japanese Navy as a standard fighter, with 138 of various versions being built, production continuing until 1928.<ref name="donald world">{{cite book|author=Donald, David (Editor)
|title = The Encyclopedia of World Aircraft|year = 1997|publisher = Aerospace Publishing
|isbn =1-85605-375-X}}</ref>

==Operational history==
The 1MF entered service with the Imperial Japanese Navy in 1923, replacing the [[Gloster Sparrowhawk]].<ref name="johan sparrowhawk">{{cite web |url= http://www.j-aircraft.com/drawings/johan/sparrowhawk.htm|title=The Sparrowhawk Ship Born Fighter - Brief history of the Sparrowhawk |accessdate=2007-08-09|author=Johan Myhrman |work= j-aircraft.com}}</ref> A 1MF aircraft became the first aircraft to take-off from and land on Japan's new aircraft carrier {{Ship|Japanese aircraft carrier|Hōshō||2}} on 28 February 1923.<ref name="johan1MF"/> The 1MF series proved a tough and reliable aircraft,<ref name="donald world"/> operating from the carriers {{Ship|Japanese aircraft carrier|Akagi||2}} and {{Ship|Japanese aircraft carrier|Kaga||2}} - as well as from ''Hōshō'' - when they entered service in 1927 and 1928 respectively.<ref name="johan1MF"/> It continued in service until 1930, being replaced by the [[Nakajima A1N]].

==Variants==
;1MF1
:Initial prototype.  Fitted with car-type radiator on front of nose. Navy designation '''Navy Type 10-1 Carrier Fighter'''.
;1MF1A
:Experimental version with increased wing area. Navy designation '''Navy Type 10-1 Carrier Fighter'''.
;1MF2
:Experimental prototype with two bay wing.   Navy designation '''Navy Type 10-1 Carrier Fighter'''.
;1MF3
:Production version with Lamblin radiators under nose replacing original car-type radiators. Navy designation '''Navy Type 10-2 Carrier Fighter'''.
;1MF4
:Revised production version with cockpit moved forwards. Navy designation '''Navy Type 10-2 Carrier Fighter'''.
;1MF5
:Minor changes. Navy designation '''Navy Type 10-2 Carrier Fighter'''.
;1MF5A
:Carrier trainer version with jettisonable wheeled undercarriage and floats under wings to allow safe ditching. Navy designation '''Navy Type 10-2 Carrier Fighter'''.

==Operators==
;{{JPN}}
*[[Imperial Japanese Navy Air Service]]

==Specifications (1MF3)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref= Japanese Aircraft 1910-1941 <ref name="Mikesh"/>
|crew=One
|capacity=
|length main= 6.90 m
|length alt= 22 ft 7½ in
|span main= 8.50 m
|span alt= 27 ft 10½ in
|height main= 3.10 m
|height alt= 10 ft 2 in
|area main= 
|area alt= 
|airfoil=
|empty weight main= 940 kg
|empty weight alt= 2,073 lb
|loaded weight main= 1,280 kg
|loaded weight alt= 2,821 lb
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Mitsubishi Hi]]  
|type of prop= V-8 water-cooled
|number of props=1
|power main= 224 kW
|power alt= 300 hp
|power original=
|max speed main= 213 km/h
|max speed alt= 115 kn, 132 mph
|cruise speed main=  
|cruise speed alt=  
|stall speed main=  
|stall speed alt=  
|never exceed speed main=   
|never exceed speed alt=  
|range main=  
|range alt= 
|ceiling main= 7,000 m
|ceiling alt= 23,000 ft
|climb rate main=  
|climb rate alt=  
|loading main=  
|loading alt=  
|thrust/weight=
|power/mass main= 0.18 kW/kg
|power/mass alt= 0.11 hp/lb
|more performance=*'''Endurance:''' 2½ hours
*'''Climb to 3,000 m (9,800 ft):''' 10 min
|armament=
*2 × 7.7 mm (.303 in) [[machine gun]]s.
|avionics=
}}

==See also==
{{aircontent|
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Mitsubishi 2MR]]<!-- related developments -->
|similar aircraft=*[[Fairey Flycatcher]] 
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

==External links==
{{commons category|Mitsubishi 1MF}}
* [http://smmlonline.com/articles/hosho/hosho.html IJN Hosho and her aircraft]

{{Mitsubishi aircraft}}
{{Japanese Navy Carrier Fighters}}

[[Category:Japanese fighter aircraft 1920–1929]]
[[Category:Mitsubishi aircraft|1MF]]