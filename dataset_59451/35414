{{good article}}
{{NASCAR race season infobox
| Type           = NAS
| country        = United States
| Race Name      = Price Chopper 400 presented by Kraft Foods
| Details ref    = <ref name="fx" >{{cite web|url=http://espn.go.com/racing/schedule/_/year/2010|title=Sprint Cup Series Schedule|location=ESPN|accessdate=2 October 2010}}</ref><ref name="weather">{{cite web|title=Price Chopper 400|url=http://www.rotoworld.com/content/features/column.aspx?sport=NAS&columnid=229&articleid=36357|publisher=rotoworld.com|accessdate=3 October 2010| archiveurl= https://web.archive.org/web/20101002000920/http://www.rotoworld.com/content/features/column.aspx?sport=NAS&columnid=229&articleid=36357| archivedate= 2 October 2010 <!--DASHBot-->| deadurl= no}}</ref><ref name="kaskscorespole">{{cite news|title=Kahne paces RPM sweep of Kansas front row|url=http://www.nascar.com/2010/news/headlines/cup/10/01/kkahne-pole-cup-kansas-chase/index.html|accessdate=2 October 2010|newspaper=NASCAR|date=1 October 2010| archiveurl= https://web.archive.org/web/20101004130228/http://www.nascar.com/2010/news/headlines/cup/10/01/kkahne-pole-cup-kansas-chase/index.html| archivedate= 4 October 2010 <!--DASHBot-->| deadurl= no}}</ref><ref name="raceinfo">{{cite web|title=Race Information|url=http://msn.foxsports.com/nascar/cup/raceTrax?gameId=20101003039|publisher=Fox Sports|accessdate=10 October 2010}}</ref>
| Fulldate       = {{Start date|2010|10|3}}
| Year           = 2010
| Race_No        = 29
| Season_No      = 36
| Image          = KansasSpeedway all.PNG
| Caption        = Layout of Kansas Speedway
| Location       = [[Kansas Speedway]], [[Kansas City, Kansas]]
| Course_mi      = 1.5
| Course_km      = 2.414
| Distance_laps  = 267
| Distance_mi    = 400.5
| Distance_km    = 644.542
| Weather        =  Sunny with a high of 60; wind out of the East at 8 mph.
| Avg            = {{convert|138.077|mi/h|km/h}}
| Pole_Driver    = [[Kasey Kahne]]
| Pole_Team      = [[Richard Petty Motorsports]]
| Pole_Time      = 30.920
| Most_Driver    = [[Tony Stewart]]
| Most_Team      = [[Stewart Haas Racing]]
| Most_laps      = 76
| Car            = 16
| First_Driver   = [[Greg Biffle]]
| First_Team     = [[Roush Fenway Racing]]
| Network        = [[ESPN]]
| Announcers     = [[Marty Reid]], [[Dale Jarrett]] and [[Andy Petree]]
}}
The '''2010 Price Chopper 400''' was a [[NASCAR]] [[Sprint Cup Series]] [[stock car]] race that was held on October 3, 2010 at [[Kansas Speedway]] in [[Kansas City, Kansas|Kansas City]], [[Kansas]]. The 300 lap race was the twenty-ninth in the [[2010 NASCAR Sprint Cup Series]]. The race was also the third event in the ten round [[Chase for the Sprint Cup]] competition, which would conclude the 2010 season. [[Greg Biffle]], of the [[Roush Fenway Racing]] team, won the race, with [[Jimmie Johnson]] finishing second and [[Kevin Harvick]] third.

[[Pole position]] driver [[Kasey Kahne]] maintained his lead on the first lap to begin the race, as [[Jeff Gordon]], who started in the third position on the [[List of motorsport terminology#G|grid]], remained behind him. Twenty-three laps later [[Jeff Gordon]] became the leader of the race. After the final pit stops, [[Paul Menard]] became the leader of the race, but with less than fifty laps remaining, Biffle passed him.  He maintained the first position to lead a total of sixty laps, and to win his second race of the season.

There were five cautions and twenty lead changes among twelve different drivers throughout the course of the race. It was Greg Biffle's second win in the 2010 season, and the sixteenth of his career. The result moved Biffle up to eighth in the [[List of NASCAR Sprint Cup Series champions|Drivers' Championship]], eighty-five points behind Jimmie Johnson and sixteen ahead of [[Jeff Burton]]. [[Chevrolet]] maintained its lead in the Manufacturers' Championship, thirty-seven ahead of [[Toyota]] and seventy-seven ahead of [[Ford]], with seven races remaining in the season. A total of 100,000 people attended the race, while 5.25 million watched it on television.

==Report==

===Background===
[[File:The Kansas Speedway.jpg|thumb|200px|left|[[Kansas Speedway]], the race track where the race was held.]]
[[Kansas Speedway]] is one of ten [[Oval track racing#Intermediate tracks|intermediate]] to hold [[NASCAR]] races.<ref name="tacksnascar">{{cite web|title=NASCAR Race Tracks|url=http://www.nascar.com/races/tracks/|accessdate=30 September 2010|location=NASCAR| archiveurl= https://web.archive.org/web/20101012082703/http://www.nascar.com/races/tracks/| archivedate= 12 October 2010 <!--DASHBot-->| deadurl= no}}</ref> The standard track at Kansas Speedway is a four-turn [[Oval track racing#D-shaped oval|D-shaped oval]] track that is {{convert|1.5|mi|km}} long.<ref name="kanstrack">{{cite web|title=NASCAR Tracks—The Kansas Speedway|url=http://www.kansasspeedway.com/Track-Info/Track-Facts.aspx|publisher=Kansas Speedway|accessdate=30 September 2010| archiveurl= https://web.archive.org/web/20100906071545/http://www.kansasspeedway.com/Track-Info/Track-Facts.aspx| archivedate= 6 September 2010 <!--DASHBot-->| deadurl= no}}</ref> The track's turns are [[Banked turn|banked]] at fifteen [[Degree (angle)|degrees]], while the front stretch, the location of the finish line, is 10.4 degrees. The [[Straight (racing)|back stretch]], opposite of the front, is at only five degrees.<ref name="kanstrack" /> The racetrack has seats for 82,000 spectators.<ref name="kanstrack" />

Before the race, [[Denny Hamlin]] led the [[List of NASCAR Sprint Cup Series champions|Drivers' Championship]] with 5,368 points, and [[Jimmie Johnson]] stood in second with 5,333. [[Kyle Busch]] was third in the Drivers' Championship with 5,323 points, fourteen ahead of [[Kurt Busch]] and twenty ahead of [[Kevin Harvick]] in fourth and fifth. [[Carl Edwards]] with 5,295 was seven ahead of [[Jeff Burton]], as [[Jeff Gordon]] with 5,285 points, was fifty-seven ahead of [[Greg Biffle]], and seventy-nine in front of [[Tony Stewart]].<ref name="driver">{{cite web|title=Driver's Championship Classification|url=http://www.nascar.com/races/cup/2010/28/data/standings_official.html|location=NASCAR|accessdate=30 September 2010| archiveurl= https://web.archive.org/web/20101007164939/http://www.nascar.com/races/cup/2010/28/data/standings_official.html| archivedate= 7 October 2010 <!--DASHBot-->| deadurl= no}}</ref> [[Matt Kenseth]] and [[Clint Bowyer]] was eleventh and twelfth with 5,203 and 5,130 points. In the [[NASCAR Manufacturers' Championship|Manufacturers' Championship]], [[Chevrolet]] was leading with 206 points, thirty-five ahead of [[Toyota]]. [[Ford]], with 126 points, was thirteen points ahead of [[Dodge]] in the battle for third.<ref name="manufacturer">{{cite web|title=Manufactures' Championship Classification|url=http://www.jayski.com/stats/2010/manu2010.htm|location=Jayski.com|accessdate=30 September 2010| archiveurl= https://web.archive.org/web/20101007154735/http://jayski.com/stats/2010/manu2010.htm| archivedate= 7 October 2010 <!--DASHBot-->| deadurl= no}}</ref> [[Tony Stewart]] was the race's defending champion.<ref name="2009 race">{{cite web|title=2009 Price Chopper 400 Presented by Kraft Foods|url=http://www.racing-reference.info/race/2009_Price_Chopper_400_Presented_by_Kraft_Foods/W|publisher=racing-reference.com|accessdate=30 September 2010}}</ref>

===Practice and qualifying===
Three practice sessions were held before the Sunday race, one on Friday, and two on Saturday. The first session lasted 90 minutes, the second session lasted 45 minutes,  and the final session lasted 60 minutes.<ref name="eventsch">{{cite web|title=Event Schedule|url=http://jayski.com/next/2010/29kan2010.htm#prac|accessdate=1 October 2010 |location=Jaski.com| archiveurl= https://web.archive.org/web/20100930180216/http://jayski.com/next/2010/29kan2010.htm| archivedate= 30 September 2010 <!--DASHBot-->| deadurl= no}}</ref> During the first practice session, [[Juan Pablo Montoya]], for the [[Earnhardt Ganassi Racing]] team, was quickest ahead of [[Ryan Newman]] in second and [[Jimmie Johnson]] in the third position.<ref name="prac1">{{cite web|title=Practice One Timing and Scoring|url=http://www.nascar.com/races/cup/2010/29/data/practice1_speeds.html|publisher=NASCAR|accessdate=1 October 2010| archiveurl= https://web.archive.org/web/20101004113640/http://www.nascar.com/races/cup/2010/29/data/practice1_speeds.html| archivedate= 4 October 2010 <!--DASHBot-->| deadurl= no}}</ref> [[Paul Menard]] was scored fourth, and [[Jamie McMurray]] managed fifth.<ref name=prac1 /> [[Carl Edwards]], [[Clint Bowyer]], [[Joey Logano]], [[Denny Hamlin]], and [[David Reutimann]] rounded out the top ten quickest drivers in the session.<ref name=prac1 />
[[File:KaseyKahneBristolAugust2007.jpg|thumb|150px|left|[[Kasey Kahne]] won the [[pole position]], after having the fastest time of 30.920 seconds.]]

During qualifying, forty-six cars were entered, but only forty-three were able because of NASCAR's [[Go or Go Home|qualifying procedure]].<ref>{{cite web|title=Qualifying Order|url=http://www.nascar.com/races/cup/2010/29/data/qual_order.html|accessdate=1 October 2010|location=NASCAR| archiveurl= https://web.archive.org/web/20101004130233/http://www.nascar.com/races/cup/2010/29/data/qual_order.html| archivedate= 4 October 2010 <!--DASHBot-->| deadurl= no}}</ref> [[Kasey Kahne]] clinched his eighteenth [[pole position]] in the [[Sprint Cup Series]], with a time of 30.920.<ref name="lineup">{{cite web|title=Race Lineup: Kansas|url=http://www.nascar.com/races/cup/2010/28/data/lineup.html|publisher=NASCAR|accessdate=1 October 2010| archiveurl= https://web.archive.org/web/20100927012405/http://www.nascar.com/races/cup/2010/28/data/lineup.html| archivedate= 27 September 2010 <!--DASHBot-->| deadurl= no}}</ref> He was joined on the front row of the [[List of motorsport terminology#G|grid]] by Menard.<ref name=lineup /> [[Jeff Gordon]] qualified third, [[Joey Logano]] took fourth, and [[Greg Biffle]] started fifth.<ref name=lineup /> [[Kevin Harvick]], one of the drivers in the [[Chase for the Sprint Cup]], qualified twenty-fourth, while Carl Edwards was scored thirty-first.<ref name=lineup /> The three drivers that failed to qualify for the race were [[Jason Leffler]], [[Mike Bliss]], and [[Joe Nemechek]].<ref name=lineup /> Once qualifying concluded, Kahne stated, "Coming to the green I felt great. I got through one and two really good and just got a little bit free into three so I missed the entrance a touch and then was able to gather it back and get rolling. I gave up a little bit of time there."<ref name="kahentop">{{cite news|title=Kahne Tops RPM Front Row|url=http://www.inracingnews.com/nascar-news/nascar/kahne-tops-rpm-front-row/|accessdate=2 October 2010|newspaper=In Racing News|date=1 October 2010}}</ref> He also added, "I noticed Paul Menard gained a lot right there in three going out late. He actually came really close to clipping us off. It was good. I’m glad we got the pole for Budweiser and Ford. It’ll be exciting, a couple of RPM guys on the front row on Sunday. That’s pretty exciting for the whole company. They did a real nice job."<ref name=kahentop />

On the next morning, [[Kurt Busch]] was quickest in the second practice session, ahead of Gordon and [[Matt Kenseth]] in second and third.<ref name="prac2">{{cite web|title=Practice Two Timing and Scoring|url=http://www.nascar.com/races/cup/2010/29/data/practice2_speeds.html|publisher=NASCAR|accessdate=2 October 2010| archiveurl= https://web.archive.org/web/20101005032059/http://www.nascar.com/races/cup/2010/29/data/practice2_speeds.html| archivedate= 5 October 2010 <!--DASHBot-->| deadurl= no}}</ref> Reutimann was fourth quickest, and Greg Biffle took fifth.<ref name=prac2 /> Johnson, [[Dale Earnhardt, Jr.]], Kahne, [[David Ragan]], and Edwards followed in the top-ten.<ref name=prac2 /> Other drivers in the chase, such as Kyle Busch, was twenty-first, and Kevin Harvick, who was twenty-second.<ref name=prac2 /> Also in the second practice session, Biffle, who was scored fifth, spun sideways coming to the finish line, but received minor damage.<ref name="pracspin">{{cite web|title=Practice Spins|url=http://www.nascar.com/video/cup/2010/highlights/10/02/cup_kan_prac_spins.nascar/index.html|publisher=NASCAR|accessdate=2 October 2010| archiveurl= https://web.archive.org/web/20101005033514/http://www.nascar.com/video/cup/2010/highlights/10/02/cup_kan_prac_spins.nascar/index.html| archivedate= 5 October 2010 <!--DASHBot-->| deadurl= no}}</ref> During the third, and final practice session, [[Mark Martin]], with a fastest time of 31.574, was quickest.<ref name="prac3">{{cite web|title=Practice Three Timing and Scoring|url=http://www.nascar.com/races/cup/2010/29/data/practice3_speeds.html|publisher=NASCAR|accessdate=2 October 2010| archiveurl= https://web.archive.org/web/20101005032104/http://www.nascar.com/races/cup/2010/29/data/practice3_speeds.html| archivedate= 5 October 2010 <!--DASHBot-->| deadurl= no}}</ref> Jimmie Johnson and Greg Biffle followed in second and third with times of 31.596 and 31.609 seconds.<ref name=prac3 /> Logano managed to be fourth fastest, ahead of Jeff Gordon and Paul Menard.<ref name=prac3 /> Edwards was scored seventh, McMurray took eighth, Bowyer was ninth, and Ragan took tenth.<ref name=prac3 /> In this practice, both Martin and Johnson spun sideways at different times. Neither, Martin or Johnson sustained major damages to their race cars during the accidents.<ref name=pracspin />

===Race===
The race, the twenty-ninth out of a total of thirty-six in the [[2010 NASCAR Sprint Cup Series|season]], began at 1:00 p.m. [[Eastern Daylight Time|EDT]] and was televised live in the United States on [[ESPN]].<ref name=fx /> Prior to the race, weather conditions were sunny with the air temperature around {{convert|60|°F|°C}}.<ref name="summary">{{cite web|title=Race Summary|url=http://www.nascar.com/2010/races/lapbylap/10/03/lap-by-lap-kansas/index.html|location=NASCAR|accessdate=3 October 2010| archiveurl= https://web.archive.org/web/20101006101457/http://www.nascar.com/2010/races/lapbylap/10/03/lap-by-lap-kansas/index.html| archivedate= 6 October 2010 <!--DASHBot-->| deadurl= no}}</ref> Steve Schulze, pastor of Parsons Foursquare Church, began pre-race ceremonies, by giving the [[invocation]]. Next, Betti O., an Army veteran from [[Manhattan, Kansas]], performed the [[United States National Anthem|national anthem]], and Kelli Fuller, a [[Price Chopper / Price Mart|Price Chopper]] contest winner, gave the command for drivers to start their engines. On the [[parade lap|pace laps]], three drivers had to move the rear of the [[List of motorsport terminology#G|grid]]; they were [[Mark Martin]], [[Marcos Ambrose]], both because of an [[engine]] change, and [[Casey Mears]] because of a [[transmission (mechanics)|transmission]] change.<ref name=summary />

[[Kasey Kahne]] retained his [[pole position]] lead into the first corner, followed by [[Paul Menard]] in the second position. On the same lap, [[Jeff Gordon]] passed Menard to claim second. [[Greg Biffle]], who had started fifth, fell three position to eighth by lap two. Afterward, Biffle had fallen to ninth, and he started complaining about his [[car handling]]. By the ninth lap, Kahne had a 1.2 second lead over Gordon. Menard challenged Gordon for the second position on lap twelve. [[Matt Kenseth]] passed [[Joey Logano]] for fifth, while Biffle moved three positions to sixth. Kahne's lead grew more by lap 17, but less than two laps later, Gordon began to catch him. On lap 24, [[Kevin Conway (NASCAR)|Kevin Conway]] was put a lap behind, after being passed by Kahne. Four laps later, Gordon became the new leader, after Kahne led twenty-seven laps.<ref name=summary />

On lap 29, Gordon was the leader, ahead of Kahne in second, [[Ryan Newman]] in third, [[Matt Kenseth]] was fourth, and Biffle in fifth. During the laps, 30 through 34, several drivers were put a lap down, which included, [[Tony Raines]], [[Bobby Labonte]], and [[Travis Kvapil]]. Afterward, [[Landon Cassill]] drove his car to the garage, as Gordon passed [[J. J. Yeley]] to put him a lap behind. had a lead of 3.4 secs when a caution was shown because [[Juan Pablo Montoya]] collided into the [[SAFER barrier]]. The drivers on the same lap as the leader made a pit stop during the caution, but [[Michael McDowell (NASCAR)|Michael McDowell]] stayed on the race track to lead one lap before giving the lead back to Jeff Gordon.<ref name=summary />

On lap 49, [[Kurt Busch]] passed Kahne for the third position. Three laps later, the second caution was given because [[Kyle Busch]] crashed into [[David Reutimann]], causing him to spin into the SAFER barrier. Most drivers stayed on the race track, but some exceptions were [[Jamie McMurray]], [[Jimmie Johnson]], and [[Clint Bowyer]]. One lap after the restart, Kenseth became the leader, after passing Gordon. After the collision, Reutimann was several laps behind the leader. After [[Rolling start|restarting]] second, Newman had fallen to sixth, while [[Tony Stewart]] moved into third. Seven laps later, Stewart claimed second from Gordon. Gordon was passed by Biffle on lap 72, as Stewart was catching Kenseth. Newman fell to eighth on lap 77, after Kyle Busch and [[Kevin Harvick]] passed him.<ref name=summary />

Two laps later, Harvick claimed the sixth position from Kyle Busch. Afterwards, Stewart became the leader, passing Kenseth on lap 81. By lap 86, Stewart had a 1.4 second lead over Kenseth. Less than five laps later, Biffle passed Kenseth for the second position. On lap 93, green flag [[pit stop]]s began. Stewart, followed by Biffle came to pit road, giving the lead back to Matt Kenseth two laps later. When Kenseth made his pit stop, he gave the lead to Jeff Gordon. Allmendinger became the leader, after Gordon, [[Jeff Burton]], Johnson, Mark Martin, and Kurt Busch made pit stops. Once most of the drivers completed their pit stops, [[Patrick Carpentier]] was the leader. On lap 99, [[Sam Hornish, Jr.]] drove to the garage because of [[drive shaft]] problems. Four laps later, [[Travis Kvapil]], who was running third, made his pit stop. Kvapil was followed by the second running car, [[David Gilliland]]. On lap 108, Carpentier drove to pit road for his pit stop, giving the lead to Stewart.<ref name=summary />

Five laps later, Johnson passed Kahne for the twelfth position. By the 123rd lap, Stewart had a 2.2 second lead over Biffle. [[Brad Keselowski]] and [[Elliott Sadler]] became a lap behind the leader, after Stewart passed them on lap 135. Nine laps later, green flag pit stops began. Afterward, Stewart gave the lead to Harvick, when he made a pit stop. Once Harvick came to pit road, Burton became the leader. On lap 149, Stewart reclaimed the lead. Three laps later, Reutimann collided with Kyle Busch, prompting the third caution on the race to be given. Most drivers made pit stops during the caution, but Harvick was an exception. Harvick led on the restart, as Stewart moved into second. On lap 163, Biffle passed Mark Martin for fourth, as Newman fell to sixth.<ref name=summary />

One lap later, Kahne collided into the SAFER barrier to cause the fourth caution. Most drivers after the ninth position made pit stops during the caution. Harvick led during the restart, but Stewart managed to pass Harvick to lead the next lap. Afterward, Harivck fell to third, as Biffle passed him. On lap 175, Burton moved to fifth, while Stewart had a 1.2 second lead over second. Then on lap 186, the fifth caution was given because Conway's engine failed. On the restart, McMurray was the leader, but he fell to second one lap later, after Menard passed him. On lap 202, Harvick passed McMurray for the fifth position. Five laps later, Biffle passed Menard to become the leader.<ref name=summary />

Afterward, Johnson moved to seventh, after passing Newman and McMurray. Gordon passed McMurray three laps later. By lap 220, Biffle had a 2.1 second lead over Menard, as Harvick passed Edwards for fourth. With 39 laps remaining, Biffle expanded his lead to 3.2 seconds. Four laps later, Johnson moved to sixth, after passing Edwards. On lap 135, the final green flag pit stops began. Once they began, Edwards became the leader, but one lap later, Biffle reclaimed the lead. After the pit stops concluded, Biffle was first, ahead of Stewart, Kenseth, Johnson, and Harvick in the top-five positions. By lap 244, Biffle had a 5.1 second lead over Stewart. One lap later, Johnson passed Kenseth for the third position. With less than ten laps remaining Stewart fell to fourth, after Johnson and Harvick passed him. Greg Biffle maintained the lead to win his second race of the 2010 season. Jimmie Johnson finished second, ahead of Harvick in third and Stewart in fourth. Gordon clinched the fifth position, after starting third.<ref name=summary /><ref name="nascarresult">{{cite web|title=NASCAR Race Results|url=http://www.nascar.com/races/cup/2010/29/data/results_official.html|publisher=NASCAR|accessdate=10 October 2010| archiveurl= https://web.archive.org/web/20101008055430/http://www.nascar.com/races/cup/2010/29/data/results_official.html| archivedate= 8 October 2010 <!--DASHBot-->| deadurl= no}}</ref>

===Post-race===
{{quote box|quote=
"It was a great day for us. The car ran flawless. [Crew chief Greg] Erwin just made great calls in the pits, and the car just kept getting better and better and better. I was too loose for about the first three-quarters of the race. I didn't want to adjust on it, because I was running in the top three, but they tightened it up, and off it went, man. All it needed was a little bit of wedge in it to take off."|source=[[Greg Biffle]], speaking after the race.<ref name="chasetight">{{cite news|title=Biffle gets needed Kansas victory; Chase tightens|url=http://www.nascar.com/2010/news/headlines/cup/10/03/gbiffle-wins-cup-kansas-chase/index.html|accessdate=10 October 2010|newspaper=NASCAR|date=4 October 2010|author=Sporting News Wire Service| archiveurl= https://web.archive.org/web/20101006133510/http://www.nascar.com/2010/news/headlines/cup/10/03/gbiffle-wins-cup-kansas-chase/index.html?| archivedate= 6 October 2010 <!--DASHBot-->| deadurl= no}}</ref>|width=37%|align=right}}
[[Greg Biffle]] appeared in victory lane after his [[victory lap]] to start celebrating his second win of the season, in front of a crowd of 100,000 people.<ref name=chasetight /><ref name="crowd">{{cite web|title=2010 Price Chopper 400 Presented by Kraft Foods|url=http://www.racing-reference.info/race/2010_Price_Chopper_400_Presented_by_Kraft_Foods/W|accessdate=10 October 2010|work=racing-reference.info}}</ref> Afterward, he said, "We were kind of down in the dumps about Dover, one of our best race tracks, and we got trapped [when forced to pit off-sequence] – we had a top-10 car there. But we can't go back and do it over again. We'll keep doing the best we can. Everybody asks if we're out of it, and I said, 'We're going to do the best we can. If we win 'em all, we win 'em all."<ref name=chasetight />

"We had to change a lot of the race car around to get it right for today. It leads to a sleepless night and a frustrating morning, having anxious moments before the race, said second place finisher, [[Jimmie Johnson]].<ref name="takesove">{{cite news|last=Ryan|first=Nate|title=Biffle claims win in Kansas as Johnson takes over points lead|url=http://www.usatoday.com/sports/motor/nascar/2010-10-03-kansas-sprint-cup-biffle_N.htm|accessdate=10 October 2010|newspaper=USA Today|date=4 October 2010}}</ref> Then, [[Denny Hamlin]], who finished twelfth, stated, "It's not the kind of run we wanted, but we're not out of this by any means. The good part is we didn't panic."<ref name=takesove />

Following the race, Johnson became the Drivers' Championship point standings leader with 5,503.<ref name="points">{{cite web|title=Point Standings|url=http://www.nascar.com/races/cup/2010/29/data/standings_official.html|location=NASCAR|accessdate=10 October 2010| archiveurl= https://web.archive.org/web/20101013063738/http://www.nascar.com/races/cup/2010/29/data/standings_official.html| archivedate= 13 October 2010 <!--DASHBot-->| deadurl= no}}</ref> Hamlin stood in second, eight points behind Johnson, and twenty-two ahead of [[Kevin Harvick]].<ref name="points" /> [[Carl Edwards]], after finishing sixth in the race, remained in the fourth position with 5,450 points.<ref name="points" /> [[Jeff Gordon]] was fifth, as [[Kurt Busch]], [[Kyle Busch]], Biffle, [[Jeff Burton]], and [[Tony Stewart]] followed in the top-ten positions.<ref name="points" /> The final two positions available in the [[Chase for the Sprint Cup]] was occupied with [[Matt Kenseth]] in eleventh and [[Clint Bowyer]] in twelfth.<ref name="points" /> In the Manufacturers' Championship, Chevrolet maintained their lead with 212 points.<ref name=manufacturer /> Toyota remained second with 175 points.<ref name=manufacturer /> Ford followed with 135 points, nineteen points ahead of Dodge in fourth.<ref name=manufacturer /> 5.25 million people watched the race on television.<ref name="tvrate">{{cite web|title=2010 NASCAR Sprint Cup TV Ratings|url=http://www.jayski.com/pages/tvratings2010.htm|accessdate=10 October 2010|location=Jayski.com}}</ref> The race took two hours, fifty-four minutes and two seconds to complete, and the margin of victory was 7.638 seconds.<ref name="nascarresult"/>

==Results==

===Qualifying===
{| class="wikitable"
|-

! No
! Driver
! Team
! Manufacturer
! Time (in [[second]]s)
!Speed (in MPH)
! Grid
|-
|  9 ||[[Kasey Kahne]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||30.920 ||174.644 ||1
|-
|  98 ||[[Paul Menard]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||30.951 ||174.469 ||2
|-
|  24 ||[[Jeff Gordon]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||30.958 ||174.430 ||3
|-
|  20 ||[[Joey Logano]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||30.979 ||174.312 ||4
|-
|  16 ||[[Greg Biffle]] ||[[Roush Fenway Racing]] ||[[Ford]] ||30.989 ||174.255 ||5
|-
|  39 ||[[Ryan Newman]] ||[[Stewart Haas Racing]] ||[[Chevrolet]] ||31.008 ||174.149 ||6
|-
|  42 ||[[Juan Pablo Montoya]] ||[[Earnhardt Ganassi Racing]] ||[[Chevrolet]] ||31.043 ||173.952 ||7
|-
|  17 ||[[Matt Kenseth]] ||[[Roush Fenway Racing]] ||[[Ford]] ||31.052 ||173.902 ||8
|-
|  2 ||[[Kurt Busch]] ||[[Penske Racing]] ||[[Dodge]] ||31.057 ||173.874 ||9
|-
|  6 ||[[David Ragan]] ||[[Roush Fenway Racing]] ||[[Ford]] ||31.076 ||173.768 ||10
|-
|  21 ||[[Bill Elliott]] ||[[Wood Brothers Racing]] ||[[Ford]] ||31.079 ||173.751 ||11
|-
|  11 ||[[Denny Hamlin]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||31.088 ||173.701 ||12
|-
|  78 ||[[Regan Smith]] ||[[Furniture Row Racing]] ||[[Chevrolet]] ||31.102 ||173.622 ||13
|-
|  14 ||[[Tony Stewart]] ||[[Stewart Haas Racing]] ||[[Chevrolet]] ||31.139 ||173.416 ||14
|-
|  19 ||[[Elliott Sadler]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||31.146 ||173.377 ||15
|-
|  00 ||[[David Reutimann]] ||[[Michael Waltrip Racing]] ||[[Toyota]] ||31.151 ||173.349 ||16
|-
|  09 ||[[Bobby Labonte]] ||[[Phoenix Racing (NASCAR team)|Phoenix Racing]] ||[[Chevrolet]] ||31.156 ||173.321 ||17
|-
|  77 ||[[Sam Hornish, Jr.]] ||[[Penske Racing]] ||[[Dodge]] ||31.173 ||173.227 ||18
|-
|  18 ||[[Kyle Busch]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||31.182 ||173.177 ||19
|-
|  88 ||[[Dale Earnhardt, Jr.]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||31.200 ||173.077 ||20
|-
|  48 ||[[Jimmie Johnson]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||31.207 ||173.038 ||21
|-
|  1 ||[[Jamie McMurray]] ||[[Earnhardt Ganassi Racing]] ||[[Chevrolet]] ||31.208 ||173.033 ||22
|-
|  31 ||[[Jeff Burton]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||31.210 ||173.021 ||23
|-
|  29 ||[[Kevin Harvick]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||31.221 ||172.960 ||24
|-
|  56 ||[[Martin Truex, Jr.]] ||[[Michael Waltrip Racing]] ||[[Toyota]] ||31.226 ||172.933 ||25
|-
|  12 ||[[Brad Keselowski]] ||[[Penske Racing]] ||[[Dodge]] ||31.235 ||172.883 ||26
|-
|  33 ||[[Clint Bowyer]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||31.252 ||172.789 ||27
|-
|  5 ||[[Mark Martin]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||31.267 ||172.706 ||28
|-
|  46 ||[[Michael McDowell (NASCAR)|Michael McDowell]] ||[[Whitney Motorsports]] ||[[Chevrolet]] ||31.284 ||172.612 ||29
|-
|  43 ||[[A. J. Allmendinger]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||31.294 ||172.557 ||30
|-
|  99 ||[[Carl Edwards]] ||[[Roush Fenway Racing]] ||[[Ford]] ||31.299 ||172.529 ||31
|-
|  47 ||[[Marcos Ambrose]] ||[[JTG Daugherty Racing]] ||[[Toyota]] ||31.321 ||172.408 ||32
|-
|  82 ||[[Scott Speed]] ||[[Red Bull Racing Team]] ||[[Toyota]] ||31.387 ||172.046 ||33
|-
|  83 ||[[Reed Sorenson]] ||[[Red Bull Racing Team]] ||[[Toyota]] ||31.391 ||172.024 ||34
|-
|  64 ||[[Landon Cassill]] ||[[Gunselman Motorsports]] ||[[Toyota]] ||31.398 ||171.986 ||35
|-
|  26 ||[[Patrick Carpentier]] ||[[Latitude 43 Motorsports]] ||[[Ford]] ||31.444 ||171.734 ||36
|-
|  13 ||[[Casey Mears]] ||[[Germain Racing]] ||[[Toyota]] ||31.446 ||171.723 ||37
|-
|  37 ||[[David Gilliland]] ||[[Front Row Motorsports]] ||[[Ford]] ||31.485 ||171.510 ||38
|-
|  36 ||[[J. J. Yeley]] ||[[Tommy Baldwin Racing]] ||[[Chevrolet]] ||31.506 ||171.396 ||39
|-
|  34 ||[[Travis Kvapil]] ||[[Front Row Motorsports]] ||[[Ford]] ||31.631 ||170.719 ||40
|-
|  7 ||[[Kevin Conway (NASCAR)|Kevin Conway]] ||[[Robby Gordon Motorsports]] ||[[Toyota]] ||31.707 ||170.309 ||41
|-
|  71 ||[[Tony Raines]] ||[[TRG Motorsports]] ||[[Chevrolet]] ||31.870 ||169.438 ||42
|-
|  38 ||[[Dave Blaney]] ||[[Front Row Motorsports]] ||[[Ford]] ||31.530 ||171.266 ||43
|-
|colspan="8"|<center>'''Failed to qualify'''</center>
|-
|  87 ||[[Joe Nemechek]] ||[[NEMCO Motorsports]] ||[[Toyota]] ||31.532 ||171.255 ||
|-
|  55 ||[[Mike Bliss]] ||[[Prism Motorsports]] ||[[Toyota]] ||31.661 ||170.557 ||
|-
|  66 ||[[Jason Leffler]] ||[[Prism Motorsports]] ||[[Toyota]] ||31.865 ||169.465 ||
|-
|colspan="8"|{{center|{{small|''Source:<ref name=lineup /><ref name="motorracingnetworklineup">{{cite web|title=MRN Race Lineup|url=http://www.motorracingnetwork.com/Race-Series/NASCAR-Sprint-Cup/Statistics/Season-Stats/Statistics.aspx?RaceID=1187&StatType=Qualifying|publisher=[[Motor Racing Network]]|accessdate=3 October 2010}}</ref><ref name="jaskilineup">{{cite web|title=Price Chopper 400 at Kansas Speedway Starting Grid|url=http://jayski.com/stats/2010/grids/29kan2010grid.htm|publisher=Jaski.com|accessdate=3 October 2010| archiveurl= https://web.archive.org/web/20101003103707/http://jayski.com/stats/2010/grids/29kan2010grid.htm| archivedate= 3 October 2010 <!--DASHBot-->| deadurl= no}}</ref>''}}}}
|}

===Race results===
{| class="sortable wikitable" border="1"
|-
! Pos
! Grid
! Car
! Driver
! Team
! Manufacturer
! Laps Run
! Points
|-
| 1||5 ||16 ||[[Greg Biffle]] ||[[Roush Fenway Racing]] ||[[Ford]] ||267 ||190{{smallsup|1}}
|-
| 2||21 ||48 ||[[Jimmie Johnson]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||267 ||170
|-
| 3||24 ||29 ||[[Kevin Harvick]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||267 ||170{{smallsup|1}}
|-
| 4||14 ||14 ||[[Tony Stewart]] ||[[Stewart Haas Racing]] ||[[Chevrolet]] ||267 ||170{{smallsup|2}}
|-
| 5||3 ||24 ||[[Jeff Gordon]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||267 ||160{{smallsup|1}}
|-
| 6||31 ||99 ||[[Carl Edwards]] ||[[Roush Fenway Racing]] ||[[Ford]] ||267 ||155{{smallsup|1}}
|-
| 7||8 ||17 ||[[Matt Kenseth]] ||[[Roush Fenway Racing]] ||[[Ford]] ||267 ||151{{smallsup|1}}
|-
| 8||2 ||98 ||[[Paul Menard]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||267 ||147{{smallsup|1}}
|-
| 9||6 ||39 ||[[Ryan Newman]] ||[[Stewart Haas Racing]] ||[[Chevrolet]] ||267 ||138
|-
| 10||30 ||43 ||[[A. J. Allmendinger]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||267 ||134
|-
| 11||22 ||1 ||[[Jamie McMurray]] ||[[Earnhardt Ganassi Racing]] ||[[Chevrolet]] ||267 ||135{{smallsup|1}}
|-
| 12||12 ||11 ||[[Denny Hamlin]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||267 ||127
|-
| 13||9 ||2 ||[[Kurt Busch]] ||[[Penske Racing]] ||[[Dodge]] ||267 ||124
|-
| 14||28 ||5 ||[[Mark Martin]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||267 ||121
|-
| 15||27 ||33 ||[[Clint Bowyer]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||267 ||118
|-
| 16||10 ||6 ||[[David Ragan]] ||[[Roush Fenway Racing]] ||[[Ford]] ||267 ||115
|-
| 17||4 ||20 ||[[Joey Logano]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||267 ||112
|-
| 18||23 ||31 ||[[Jeff Burton]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||267 ||114{{smallsup|1}}
|-
| 19||33 ||82 ||[[Scott Speed]] ||[[Red Bull Racing Team]] ||[[Toyota]] ||267 ||106
|-
| 20||25 ||56 ||[[Martin Truex, Jr.]] ||[[Michael Waltrip Racing]] ||[[Toyota]] ||267 ||103
|-
| 21||19 ||18 ||[[Kyle Busch]] ||[[Joe Gibbs Racing]] ||[[Toyota]] ||266 ||100
|-
| 22||20 ||88 ||[[Dale Earnhardt, Jr.]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||266 ||97
|-
| 23||26 ||12 ||[[Brad Keselowski]] ||[[Penske Racing]] ||[[Dodge]] ||266 ||94
|-
| 24||37 ||13 ||[[Casey Mears]] ||[[Germain Racing]] ||[[Toyota]] ||266 ||91
|-
| 25||11 ||21 ||[[Bill Elliott]] ||[[Wood Brothers Racing]] ||[[Ford]] ||266 ||88
|-
| 26||13 ||78 ||[[Regan Smith]] ||[[Furniture Row Racing]] ||[[Chevrolet]] ||266 ||85
|-
| 27||36 ||26 ||[[Patrick Carpentier]] ||[[Latitude 43 Motorsports]] ||[[Ford]] ||266 ||87{{smallsup|1}}
|-
| 28||15 ||19 ||[[Elliott Sadler]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||266 ||79
|-
| 29||7 ||42 ||[[Juan Pablo Montoya]] ||[[Earnhardt Ganassi Racing]] ||[[Chevrolet]] ||266 ||76
|-
| 30||34 ||83 ||[[Reed Sorenson]] ||[[Red Bull Racing Team]] ||[[Toyota]] ||265 ||73
|-
| 31||43 ||38 ||[[Dave Blaney]] ||[[Front Row Motorsports]] ||[[Ford]] ||261 ||70
|-
| 32||38 ||37 ||[[David Gilliland]] ||[[Front Row Motorsports]] ||[[Ford]] ||261 ||67
|-
| 33||40 ||34 ||[[Travis Kvapil]] ||[[Front Row Motorsports]] ||[[Ford]] ||261 ||64
|-
| 34||32 ||47 ||[[Marcos Ambrose]] ||[[JTG Daugherty Racing]] ||[[Toyota]] ||257 ||61
|-
| 35||16 ||00 ||[[David Reutimann]] ||[[Michael Waltrip Racing]] ||[[Toyota]] ||256 ||58
|-
| 36||18 ||77 ||[[Sam Hornish, Jr.]] ||[[Penske Racing]] ||[[Dodge]] ||245 ||55
|-
| 37||1 ||9 ||[[Kasey Kahne]] ||[[Richard Petty Motorsports]] ||[[Ford]] ||218 ||57{{smallsup|1}}
|-
| 38||41 ||7 ||[[Kevin Conway (NASCAR)|Kevin Conway]] ||[[Robby Gordon Motorsports]] ||[[Toyota]] ||180 ||49
|-
| 39||42 ||71 ||[[Tony Raines]] ||[[TRG Motorsports]] ||[[Chevrolet]] ||87 ||46
|-
| 40||29 ||46 ||[[Michael McDowell (NASCAR)|Michael McDowell]] ||[[Whitney Motorsports]] ||[[Chevrolet]] ||60 ||48
|-
| 41||17 ||09 ||[[Bobby Labonte]] ||[[Phoenix Racing (NASCAR team)|Phoenix Racing]] ||[[Chevrolet]] ||58 ||40
|-
| 42||39 ||36 ||[[J. J. Yeley]] ||[[Tommy Baldwin Racing]] ||[[Chevrolet]] ||49 ||37
|-
| 43||35 ||64 ||[[Landon Cassill]] ||[[Gunselman Motorsports]] ||[[Toyota]] ||33 ||34
|- class="sortbottom"
|colspan="9"|{{center|{{small|''Source:<ref name="mrnraceresults">{{cite web|title=MRN Race Results|url=http://www.motorracingnetwork.com/Race-Series/NASCAR-Sprint-Cup/Statistics/Season-Stats/Statistics.aspx?RaceID=1187|publisher=[[Motor Racing Network]]|accessdate=5 October 2010}}</ref>''}}}}
|-class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|1}} Includes five bonus points for leading a lap}}}}
|- class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|2}} Includes ten bonus points for leading the most laps}}}}
|}

==Standings after the race==
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{| class="sortable wikitable " style="font-size: 95%;"
|-
! Pos
! Driver
! Points
|-
| 1
| [[Jimmie Johnson]]
| style="text-align:center;"| 5,503
|-
| 2
| [[Denny Hamlin]]
| style="text-align:center;"| 5,495
|-
| 3
| [[Kevin Harvick]]
| style="text-align:center;"| 5,473
|-
| 4
| [[Carl Edwards]]
| style="text-align:center;"| 5,450
|-
| 5
| [[Jeff Gordon]]
| style="text-align:center;"| 5,445
|-
| 6
| [[Kurt Busch]]
| style="text-align:center;"| 5,433
|-
| 7
| [[Kyle Busch]]
| style="text-align:center;"| 5,423
|-
| 8
| [[Greg Biffle]]
| style="text-align:center;"| 5,418
|-
| 9
| [[Jeff Burton]]
| style="text-align:center;"| 5,402
|-
| 10
| [[Tony Stewart]]
| style="text-align:center;"| 5,376
|-
| 11
| [[Matt Kenseth]]
| style="text-align:center;"| 5,354
|-
| 12
| [[Clint Bowyer]]
| style="text-align:center;"| 5,251
|}
{{col-2}}
[[File:JimmieJohnsonAugust2007.jpg|thumb|right|200px|[[Jimmie Johnson]] became the points leader, after finishing second in the race.]]
;Manufacturers' Championship standings<ref name=manufacturer />
{|class="wikitable" style="font-size: 95%;"
|-
! Pos
! Manufacturer
! Points
|-
| 1
| [[Chevrolet]]
| style="text-align:right;"| 212
|-
| 2
| [[Toyota]]
| style="text-align:right;"| 175
|-
| 3
| [[Ford]]
| style="text-align:right;"| 135
|-
| 4
| [[Dodge]]
| style="text-align:right;"| 116
|}

*<small>'''Note''': Only the top twelve positions are included for the driver standings. These drivers qualified for the [[Chase for the Sprint Cup]].</small>
{{col-end}}

==References==
{{reflist|colwidth=30em}}
{{Wikinews|NASCAR: Greg Biffle wins 2010 Price Chopper 400}}
{{NASCAR next race|
  Series        = Sprint Cup Series|
  Season        = 2010 |
  Previous_race = [[2010 AAA 400]] |
  Next_race     = [[2010 Pepsi Max 400]] |
}}

{{2010 NASCAR Sprint Cup |state=collapsed}}

[[Category:2010 in Kansas|Price Chopper 400]]
[[Category:2010 NASCAR Sprint Cup Series|Price Chopper 400]]
[[Category:NASCAR races at Kansas Speedway]]