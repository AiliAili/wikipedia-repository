<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Arcus
 | image=File:Yellow paraglider in flight (9 May 2009).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Paraglider]]
 | national origin=[[Germany]]
 | manufacturer=[[Swing Flugsportgeräte]]
 | designer=
 | first flight=
 | introduced=late 1990s
 | retired=
 | status=In production (Arcus 7, 2016)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= late 1990s-present
 | number built=8,500
 | program cost= <!--Total program cost-->
 | unit cost= [[Pound sterling|£]]1,617 (Arcus 3, 2004)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Swing Arcus''' ({{lang-en|''arch'' or ''rainbow''}}) is a series of [[Germany|German]] single-place and two-place  [[paraglider]]s, designed and produced by [[Swing Flugsportgeräte]] of [[Landsberied]]. In 2016 it remained in production as the Arcus 7.<ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 32. Pagefast Ltd, Lancaster OK, 2003. {{ISSN|1368-485X}}</ref>

==Design and development==
The Arcus was designed as a beginner to intermediate glider.<ref name="WDLA04"/>

The design has progressed through seven generations of models, the Arcus 1, Arcus 2002, 3, 4, 5, 6 and 7, each improving on the last. The models are each named for their approximate projected wing area in square metres or their relative size.<ref name="WDLA04"/>

The Arcus 3 series was introduced in 2003 and included the Arcus XL Twin, a two-place [[tandem]] glider for [[flight training]].<ref name="WDLA04"/>

More than 8,500 Arcus gliders have been sold, making it one of the most produced paragliders.<ref>{{cite web|url = http://www.swing.de/about-swing.html|title = About Swing|accessdate = 17 November 2016|author = [[Swing Flugsportgeräte]]|date = }}</ref>
<!-- ==Operational history== -->

==Variants==
===Arcus 1===
;Arcus 1 S
:Small-sized model for lighter pilots. Its {{convert|11.4|m|ft|1|abbr=on}} span wing has a wing area of {{convert|27.0|m2|sqft|abbr=on}}, 43 cells and the [[Aspect ratio (aeronautics)|aspect ratio]] is 4.85:1. The pilot weight range is {{convert|65|to|85|kg|lb|0|abbr=on}}. The glider model is [[Deutscher Hängegleiterverband e.V.]] (DHV) 1-2 GH certified.<ref name="Arcus1">{{cite web|url = http://www.swing.de/arcus-1-en.html|title = Arcus 1|accessdate = 18 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 1 M
:Mid-sized model for medium-weight pilots. Its {{convert|12.1|m|ft|1|abbr=on}} span wing has a wing area of {{convert|29.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.0:1. The pilot weight range is {{convert|80|to|105|kg|lb|0|abbr=on}}. The glider model is DHV 1 GH certified.<ref name="Arcus1"/>
;Arcus 1 L
:Large-sized model for heavier pilots. Its {{convert|12.8|m|ft|1|abbr=on}} span wing has a wing area of {{convert|32.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.0:1. The pilot weight range is {{convert|95|to|125|kg|lb|0|abbr=on}}. The glider model is DHV 1 GH certified.<ref name="Arcus1"/>
;Arcus 1 XL
:Extra large-sized model for even heavier pilots. Its {{convert|13.3|m|ft|1|abbr=on}} span wing has a wing area of {{convert|34.7|m2|sqft|abbr=on}}, 48 cells and the aspect ratio is 5.1:1. The pilot weight range is {{convert|105|to|140|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 GH certified.<ref name="Arcus1"/>
;Arcus 1 XL Twin
:Designed as a [[tandem]] glider for [[flight training]]. Its {{convert|13.3|m|ft|1|abbr=on}} span wing has a wing area of {{convert|34.7|m2|sqft|abbr=on}}, 48 cells and the aspect ratio is 5.1:1. The pilot weight range is {{convert|105|to|170|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 GH certified.<ref name="Arcus1"/>
===Arcus 2002===
;Arcus 2002 S
:Small-sized model for lighter pilots. Its {{convert|11.4|m|ft|1|abbr=on}} span wing has a wing area of {{convert|27.0|m2|sqft|abbr=on}}, 43 cells and the aspect ratio is 4.85:1. The pilot weight range is {{convert|65|to|85|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 GH certified.<ref name="Arcus2002">{{cite web|url = http://www.swing.de/arcus-1-en.html|title = Arcus 2002|accessdate = 18 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 2002 M
:Mid-sized model for medium-weight pilots. Its {{convert|12.1|m|ft|1|abbr=on}} span wing has a wing area of {{convert|29.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.0:1. The pilot weight range is {{convert|80|to|105|kg|lb|0|abbr=on}}. The glider model is DHV 1 GH certified.<ref name="Arcus2002"/>
;Arcus 2002 L
:Large-sized model for heavier pilots. Its {{convert|12.8|m|ft|1|abbr=on}} span wing has a wing area of {{convert|32.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.0:1. The pilot weight range is {{convert|95|to|125|kg|lb|0|abbr=on}}. The glider model is DHV 1 GH certified.<ref name="Arcus2002"/>
;Arcus 2002 XL
:Extra large-sized model for even heavier pilots. Its {{convert|13.3|m|ft|1|abbr=on}} span wing has a wing area of {{convert|34.7|m2|sqft|abbr=on}}, 48 cells and the aspect ratio is 5.1:1. The pilot weight range is {{convert|105|to|140|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 GH certified.<ref name="Arcus2002"/>

===Arcus 3===
;Arcus 3 24
:Small-sized model for lighter pilots. Its {{convert|11.6|m|ft|1|abbr=on}} span wing has a wing area of {{convert|26.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The pilot weight range is {{convert|60|to|85|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 certified.<ref name="WDLA04"/>
;Arcus 3 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.2|m|ft|1|abbr=on}} span wing has a wing area of {{convert|28.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The pilot weight range is {{convert|75|to|100|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 certified.<ref name="WDLA04"/>
;Arcus 3 28
:Large-sized model for heavier pilots. Its {{convert|12.6|m|ft|1|abbr=on}} span wing has a wing area of {{convert|30.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The pilot weight range is {{convert|90|to|115|kg|lb|0|abbr=on}}. The glider model is DHV 1-2 certified.<ref name="WDLA04"/>
;Arcus 3 30
:Extra large-sized model for even heavier pilots. Its {{convert|13.0|m|ft|1|abbr=on}} span wing has a wing area of {{convert|32.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The pilot weight range is {{convert|105|to|130|kg|lb|0|abbr=on}}. The glider model is DHV 1 certified.<ref name="WDLA04"/>
;Arcus XL Twin
:Designed as a tandem glider for flight training. Its {{convert|13.3|m|ft|1|abbr=on}} span wing has a wing area of {{convert|34.7|m2|sqft|abbr=on}}, 48 cells and the aspect ratio is 5.1:1. The pilot weight range is {{convert|105|to|170|kg|lb|0|abbr=on}}. The glider model is DHV 2 certified.<ref name="WDLA04"/>
===Arcus 4===
;Arcus 4 22
:Extra small-sized model for much lighter pilots. Its {{convert|11.4|m|ft|1|abbr=on}} span wing has a wing area of {{convert|25|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The takeoff weight range is {{convert|55|to|75|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6|kg|lb|2|abbr=on}}. The glider model is DHV LTF 1 certified with and without an accelerator.<ref name="Arcus4">{{cite web|url = http://www.swing.de/arcus-4-en.html|title = Arcus 4|accessdate = 17 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 4 24
:Small-sized model for lighter pilots. Its {{convert|11.8|m|ft|1|abbr=on}} span wing has a wing area of {{convert|26.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The takeoff weight range is {{convert|60|to|85|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.25|kg|lb|2|abbr=on}}. The glider model is LTF 1 and certified without an accelerator and LTF 1-2 certified with accelerator.<ref name="Arcus4"/>
;Arcus 4 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.2|m|ft|1|abbr=on}} span wing has a wing area of {{convert|28.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The takeoff weight range is {{convert|75|to|100|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.5|kg|lb|2|abbr=on}}. The glider model is LTF 1 and certified without an accelerator and LTF 1-2 certified with accelerator.<ref name="Arcus4"/>
;Arcus 4 28
:Large-sized model for heavier pilots. Its {{convert|12.6|m|ft|1|abbr=on}} span wing has a wing area of {{convert|30.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The takeoff weight range is {{convert|90|to|115|kg|lb|0|abbr=on}} and glider empty weight is {{convert|7|kg|lb|2|abbr=on}}.  The glider model is LTF 1 and certified without an accelerator and LTF 1-2 certified with accelerator.<ref name="Arcus4"/>
;Arcus 4 30
:Extra large-sized model for even heavier pilots. Its {{convert|13|m|ft|1|abbr=on}} span wing has a wing area of {{convert|32.5|m2|sqft|abbr=on}}, 46 cells and the aspect ratio is 5.17:1. The takeoff weight range is {{convert|105|to|140|kg|lb|0|abbr=on}} and glider empty weight is {{convert|7.3|kg|lb|2|abbr=on}}. The glider model is DHV LTF 1 certified with and without an accelerator.<ref name="Arcus4"/>

===Arcus 5===
;Arcus 5 22
:Extra small-sized model for much lighter pilots. Its {{convert|11.62|m|ft|1|abbr=on}} span wing has a wing area of {{convert|26|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|55|to|80|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus5">{{cite web|url = http://www.swing.de/arcus-5-en.html|title = Arcus 5|accessdate = 17 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 5 24
:Small-sized model for lighter pilots. Its {{convert|12.06|m|ft|1|abbr=on}} span wing has a wing area of {{convert|28|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|65|to|90|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.2|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus5"/>
;Arcus 5 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.49|m|ft|1|abbr=on}} span wing has a wing area of {{convert|30|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|80|to|105|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.5|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus5"/>
;Arcus 5 28
:Large-sized model for heavier pilots. Its {{convert|12.79|m|ft|1|abbr=on}} span wing has a wing area of {{convert|31.5|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|90|to|115|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.9|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus5"/>
;Arcus 5 30
:Extra large-sized model for even heavier pilots. Its {{convert|13.19|m|ft|1|abbr=on}} span wing has a wing area of {{convert|33.5|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|105|to|130|kg|lb|0|abbr=on}} and glider empty weight is {{convert|7.3|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus5"/>
===Arcus 6===
;Arcus 6 22
:Extra small-sized model for much lighter pilots. Its {{convert|11.62|m|ft|1|abbr=on}} span wing has a wing area of {{convert|25.7|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|60|to|80|kg|lb|0|abbr=on}} and glider empty weight is {{convert|5.35|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6">{{cite web|url = http://www.swing.de/arcus-6-en.html|title = Arcus 6|accessdate = 17 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 6 24
:Small-sized model for lighter pilots. Its {{convert|12.06|m|ft|1|abbr=on}} span wing has a wing area of {{convert|28|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|65|to|90|kg|lb|0|abbr=on}} and glider empty weight is {{convert|5.65|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6"/>
;Arcus 6 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.49|m|ft|1|abbr=on}} span wing has a wing area of {{convert|29.7|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|80|to|105|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6"/>
;Arcus 6 28
:Large-sized model for heavier pilots. Its {{convert|12.7|m|ft|1|abbr=on}} span wing has a wing area of {{convert|30.7|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|90|to|115|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.15|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6"/>
;Arcus 6 30
:Extra large-sized model for even heavier pilots. Its {{convert|13.19|m|ft|1|abbr=on}} span wing has a wing area of {{convert|33.5|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|105|to|130|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.5|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6"/>
;Arcus 6 32
:Extra large-sized model for much heavier pilots. Its {{convert|13.55|m|ft|1|abbr=on}} span wing has a wing area of {{convert|35|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.25:1. The takeoff weight range is {{convert|120|to|150|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.65|kg|lb|2|abbr=on}}. The glider model is LTF 1 and CEN A certified without an accelerator and LTF 1-2 and CEN B certified with accelerator.<ref name="Arcus6"/>

===Arcus 7===
;Arcus 7 22
:Extra small-sized model for much lighter pilots. Its {{convert|11.51|m|ft|1|abbr=on}} span wing has a wing area of {{convert|25.5|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|60|to|80|kg|lb|0|abbr=on}} and glider empty weight is {{convert|5.75|kg|lb|2|abbr=on}}. The glider model is DHV LTF/A and CEN A certified without an accelerator and LTF/B and CEN B certified with accelerator.<ref name="Arcus7">{{cite web|url = http://www.swing.de/arcus-7-en.html|title = Arcus 7|accessdate = 17 November 2016|author = [[Swing Flugsportgeräte]] }}</ref>
;Arcus 7 24
:Small-sized model for lighter pilots. Its {{convert|11.9|m|ft|1|abbr=on}} span wing has a wing area of {{convert|27.25|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|70|to|90|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.05|kg|lb|2|abbr=on}}. The glider model is LTF/A and CEN A certified without an accelerator and LTF/B and CEN B certified with accelerator.<ref name="Arcus7"/>
;Arcus 7 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.28|m|ft|1|abbr=on}} span wing has a wing area of {{convert|29.0|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|80|to|105|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.3|kg|lb|2|abbr=on}}. The glider model is LTF/A and CEN A certified without an accelerator and LTF/B and CEN B certified with accelerator.<ref name="Arcus7"/>
;Arcus 7 28
:Large-sized model for heavier pilots. Its {{convert|12.64|m|ft|1|abbr=on}} span wing has a wing area of {{convert|30.75|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|90|to|115|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.55|kg|lb|2|abbr=on}}. The glider model is LTF/A and CEN A certified without an accelerator and LTF/B and CEN B certified with accelerator.<ref name="Arcus7"/>
;Arcus 7 30
:Extra large-sized model for even heavier pilots. Its {{convert|13.00|m|ft|1|abbr=on}} span wing has a wing area of {{convert|32.5|m2|sqft|abbr=on}}, 44 cells and the aspect ratio is 5.2:1. The takeoff weight range is {{convert|105|to|130|kg|lb|0|abbr=on}} and glider empty weight is {{convert|6.75|kg|lb|2|abbr=on}}. The glider model is LTF/A and CEN A certified without an accelerator and LTF/B and CEN B certified with accelerator.<ref name="Arcus7"/>
<!-- ==Aircraft on display== -->

==Specifications (Arcus 3 28) ==
{{Aircraft specs
|ref=Bertrand<ref name="WDLA04" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=12.6
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=30.5
|wing area sqft=
|wing area note=
|aspect ratio=5.17:1
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=52
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|sink rate ms=1.1
|sink rate ftmin=
|sink rate note=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
}}

==References==
{{reflist}}

==External links==
{{Commons category}}
*{{Official website|http://www.swing.de/arcus-7-en.html}}
{{Swing aircraft}}
[[Category:Swing aircraft|Arcus]]
[[Category:Paragliders]]