{{Infobox military person
| honorific_prefix  = 
| name              = Duerson Knight
| honorific_suffix  = 
| native_name       = 
| native_name_lang  = 
| image             = Duerson Knight.jpg 
| image_size        = 250
| alt               = 
| caption           = Duerson Knight, 1918
| birth_date        = {{birth date|1893|01|21}}
| death_date        = {{Death date and age|1983|04|23|1893|01|21}}
| birth_place       = Chicago, Illinois
| death_place       = Walnut Creek, California
| placeofburial     =  
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname          = 
| birth_name        = 
| allegiance    = {{flag|United States|23px}}
| branch        = [[Royal Air Force]] (United Kingdom)
| unit          = Royal Air Force
* [[No. 1 Squadron RAF]]
| battles       = [[File:World War I Victory Medal ribbon.svg|50px]]&nbsp;[[World War I]]
}}

'''Duerson Knight''' (21 January 1893 – 23 April 1983) was an American pursuit pilot and a [[flying ace]] in [[World War I]].<ref name="Franks">Franks (1992) Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918 , Grub Street the Basement; First edition (May 1992), ISBN 0948817542</ref>

He died in Walnut Creek, California on 23 April 1983.<ref name="AD">[http://www.theaerodrome.com/aces/usa/knight2.php theaerodrome.com Duerson Knight]</ref>

==Biography==
Born in Chicago Illinois, he was the son of Thomas Duerson Knight, an attorney, and Helen Whitaker (Fowle) Knight, daughter of Governor Fowle of North Carolina.  As a teen, he was a track star at University High School and the University of Chicago (class of 1916).<ref name="AD"/>

Knight joined the United States Air Service in 1917. He was commissioned as a 1st Lieutenant on 22 March 1918.<ref name="AD"/>  After training near Toronto, he was deployed to France and was assigned to RAF No. 1 Squadron in 1918 to gain combat experience.  Flying SE-5A single-seaters, he claimed ten aerial victories, one of which was a Fokker triplane which was forced to land inside Allied lines and was captured intact.  The pilot of the plane was Gefr Preiss of Jasta 14, whose parachute was sent to England for further study.  In August he helped bring down another German scout plane inside Allied lines.<ref name="Franks"/>

Knight then was transferred to the Air Service, United States Army in October 1918, but saw no action.   After the war, he returned to the United States, and returned to the University of Chicago in 1919. Eventually settling in California.  He died at the age of 90 in April 1983.<ref name="Franks"/>

==See also==
{{Portal|World War I|Biography}}
* [[List of World War I flying aces from the United States]]

==References==
{{Reflist}}
{{Refbegin}}
{{Refend}}

==External links==

{{DEFAULTSORT:Knight, Duerson}}
[[Category:1893 births]]
[[Category:1983 deaths]]
[[Category:American World War I flying aces]]