{{Infobox museum
| name                = Harriet Tubman Underground Railroad Visitor Center
| logo                = 
| logo_upright        = 
| logo_alt            = 
| logo_caption        = 
| image               = Harriet Tubman Underground Railroad Visitor Center.jpg
| image_upright       = 
| alt                 = 
| caption             = 
| map_type            = 
| map_relief          = 
| map_size            = 
| map_caption         = 
| coordinates         = {{coord|38.443695|-76.144747|display=inline,title}}
| established         = {{Start date|2017|03|10}}
| dissolved           = <!-- {{End date|YYYY|MM|DD}} -->
| location            = 4068 Golden Hill Road, [[Church Creek, Maryland]], U.S.
| type                = [[History museum]]
| accreditation       = 
| key_holdings        = 
| collections         = 
| collection_size     = 
| visitors            = 
| director            = Dana Paterra, [[Maryland Park Service]]
| curator             = 
| architect           = GWWO, Inc., Architects
| owner               = [[National Park Service]] and the [[Maryland|State of Maryland]]
| parking             = 
| website             = {{URL|https://www.nps.gov/hatu/index.htm}}
}}
The '''Harriet Tubman Underground Railroad Visitor Center''' is a visitors' center and [[history museum]] located on the grounds of the [[Harriet Tubman Underground Railroad State Park]] (a [[Maryland]] state park) in [[Church Creek, Maryland]], in the United States. The state park is surrounded by the [[Blackwater National Wildlife Refuge]], whose north side is bordered by the [[Harriet Tubman Underground Railroad National Monument|Harriet Tubman Underground Railroad National Historical Park]]. Jointly created and managed by the [[National Park Service]] and [[Maryland Park Service]], the visitor center opened on March 10, 2017.

==Building the center==
===History of the parks===
[[File:Harriet Tubman Underground Railroad State Park.jpg|thumb|Location (in red) of the Harriet Tubman Underground Railroad State Park within the Blackwater National Wildlife Refuge (in yellow).]]
Harriet Tubman was born Araminta Ross in the early 1820s{{sfn|Larson|2004|page=xvi}}{{sfn|Humez|2003|page=12}}{{sfn|Clinton|2004|page=4}} on the plantation of Anthony Thompson near the village of [[Madison, Maryland|Madison]] in [[Dorchester County, Maryland|Dorchester County]] on [[Eastern Shore of Maryland|Maryland's Eastern Shore]]. A year or two after she was born, Edward Brodess claimed Tubman, her mother, and her four siblings as an inheritance and took them away to his farm near Bucktown, about {{convert|10|mi|km}} to the east. After Brodess died in 1849, Tubman was at risk of being sold. Instead, she fled slavery and moved to [[Pennsylvania]], a state where slavery was outlawed. Over roughly the next decade, Tubman gained national fame by returning to Maryland repeatedly to lead her siblings and other slaves to freedom via what is now called the [[Underground Railroad]].{{sfn|Larson|2004|pages=xvi=xviii}}

The entire area remained in private hands until 1933, when the {{convert|28000|acre|km2|adj=on}} Blackwater National Wildlife Refuge was established.{{sfn|Arnett|Brugger|Papenfuse|1999|page=200}} This protected area encompassed much of the shoreline of the [[Blackwater River (Maryland)|Blackwater River]], but did not protect areas such as the Thompson farm, Brodess farm, or other areas important to Tubman's life and the history of the Underground Railroad.

On March 9, 2013—the 100th anniversary of Tubman's death—the State of Maryland and the [[National Park Service]] broke ground for a new protected area within the Blackwater National Wildlife Refuge.  This {{convert|17|acre|m2|adj=on}} state-owned site lay entirely within the refuge's boundaries on [[Maryland Route 335]]. The state designated the land a state park, and named it the Harriet Tubman Underground Railroad State Park. The Maryland Park Service and the National Park Service jointly provided funding to construct the Harriet Tubman Underground Railroad Visitor Center. On the same date, the State of Maryland unveiled the {{convert|125|mi|km|adj=on}} [[Maryland Scenic Byways#Harriet Tubman Underground Railroad|Harriet Tubman Underground Railroad Scenic Byway]], a route along an existing system of county, state, and federal roads which mirrored the route Tubman took while rescuing slaves.<ref>{{cite web|title=Harriet Tubman Underground Railroad State Park|website=Maryland Department of Natural Resources|date=2017|accessdate=March 6, 2017|url=http://dnr2.maryland.gov/publiclands/Pages/eastern/tubman.aspx}}</ref><ref name=stodghill>{{cite news|last=Stodghill|first=Ron|title=Harriet Tubman's Path to Freedom|work=The New York Times|date=February 24, 2017|accessdate=March 6, 2017|url=https://www.nytimes.com/interactive/2017/02/24/travel/underground-railroad-slavery-harriet-tubman-byway-maryland.html}}</ref>

On March 25, 2013, President Barack Obama declared much of the area adjacent to the northern boundary of Blackwater National Wildlife Refuge as a [[National Monument (United States)|national monument]]. A little under two years later, on December 19, 2014, Congress enacted H.R. 3979, which incorporated the Harriet Tubman National Historical Parks Act (H.R.664) as an amendment.  This act established the Harriet Tubman Underground Railroad National Historical Park as a unit of the National Park Service. The park boundaries are essentially the same as the national monument's, and protect nearly all the important sites associated with Tubman's life.<ref>{{cite web|title=Harriet Tubman Underground Railroad National Historical Park|website=Maryland Department of Natural Resources|date=2017|accessdate=March 6, 2017|url=http://dnr2.maryland.gov/publiclands/Pages/eastern/tubman.aspx}}</ref><ref name=houstonstyle>{{cite news|title=Secretary Salazar Hails Establishment of National Monument Honoring Harriet Tubman|work=Houston Style Magazine|date=March 27, 2013|accessdate=March 6, 2017|url=http://stylemagazine.com/news/2013/mar/27/secretary-salazar-hails-establishment-national-mon/}}</ref><ref name=mccauley>{{cite news|last=McCauley|first=Mary Carole|title=New Harriet Tubman park presents the Dorchester County she grew up in, fled from|work=The Baltimore Sun|date=March 3, 2017|accessdate=March 6, 2017|url=http://www.baltimoresun.com/entertainment/arts/bs-ae-tubman-visitor-center-opening-20170305-story.html}}</ref>

===The visitors' center===
Beginning in the 1970s, descendants of Harriet Tubman and her siblings began advocating for a state park to commemorate Tubman, her legacy, and her connection to rural Maryland. In 2007, Maryland acquired {{convert|17.3|acre|m2}} adjacent to the Blackwater National Wildlife Refuge for a park. The onset of the [[Great Recession]] significantly hurt both state and federal budgets, stalling any move toward construction of the park.<ref name=fuller>{{cite news|last=Fuller|first=Nicole|title=Tubman Underground Railroad center on Shore gets funding|work=The Baltimore Sun|date=August 16, 2011|accessdate=March 6, 2017|url=http://www.baltimoresun.com/news/maryland/bs-md-tubman-museum-20110816-story.html}}</ref>

In 2008, Maryland and the National Park Service (NPS) entered into negotiations to jointly create the Tubman park.<ref name=ruane>{{cite news|last=Ruane|first=Michael E.|title=Harriet Tubman fled a life of slavery in Maryland. Now a new visitor center opens on the land she escaped|work=The Washington Post|date=March 4, 2017|accessdate=March 6, 2017|url=https://www.washingtonpost.com/local/harriet-tubman-fled-a-life-of-slavery-in-maryland-now-a-new-visitor-center-opens-on-the-land-she-escaped/2017/03/03/1134955e-fd0e-11e6-8ebe-6e0dbe4f2bca_story.html}}</ref> The two sides reached an agreement in 2009, after the National Park Service endorsed a plan to create national parks at Tubman-associated sites in Maryland and New York. The NPS plan required legislative approval from Congress, but none was immediately forthcoming.<ref name=fuller /> The negotiations for a state park nevertheless bore fruit in August 2011. Funding for the {{convert|15000|sqft|m2|adj=on}}, $21 million project came from a state appropriation, several federal grants, and $8.5 million in Federal Transportation Enhancement Program money. Plans called for an exhibit hall with displays, and a garden with a walking trail. The goal was to have the center completed in 2013 in time for the 100th anniversary of Tubman's death.<ref name=fuller />

The visitor center and gardens were designed by GWWO, Inc., Architects, an architectural design firm based in [[Baltimore]]. Chris Elcock was the lead architect.<ref name=mccauley /> The buildings were located on the site and oriented to face north, the direction in which Tubman led slaves to freedom.<ref name=dorchester /> Plans called for four buildings, three dedicated to exhibits and one to house administrative offices. Each of the buildings is shaped like a [[barn]], since Tubman and the slaves she was leading to freedom often slept in barns at night and barns are evocative of the rural architecture of the area. The three exhibit buildings are clad in a light green [[zinc]] siding which will dull with time. Zinc was chosen because it is a [[self-healing material]] which the architects felt reflected the healing which has gone on since the [[American Civil War]]. The administration building is clad in wood.<ref name=mccauley /> Construction of the buildings utilized a number of craftsman. Building materials included stone and reclaimed wood, and the architectural style featured exposed beams and timbers.  The buildings were designed to have windows which featured views of the surrounding wildlife refuge, little of which had changed since Tubman's day.<ref name=dorchester>{{cite news|title=Harriet Tubman Visitor Center on the rise|work=Dorchester Banner|date=February 9, 2016|accessdate=March 6, 2017|url=http://www.dorchesterbanner.com/local-history/harriet-tubman-visitor-center-on-the-rise/}}</ref> All the structures were designed to be [[green building]]s with at least a [[Leadership in Energy and Environmental Design#Certification level|LEED Silver]] certification. The buildings feature [[Geothermal heat pump|geothermal heating and cooling]], [[green roof]]s, [[permeable paving]] in parking areas and on pathways, and [[Solar lamp|solar-powered external lighting]].<ref name=dorchester />

Various construction and funding delays meant that the center did not open in 2013. That year, officials estimated completion in 2015.<ref name=houstonstyle /> The 2015 deadline was missed as well, and officials estimated in February 2016 that the facility would finally open in March 2017.<ref name=dorchester />

The final cost of the state park and visitor center was $22 million.<ref name=ruane />

==About the center==
[[File:Harriet Tubman Underground Railroad Visitor Center back view.jpg|thumb|Rear view of the visitors center, showing the distinctive zinc siding of the three exhibit buildings and the wood siding of the administration building.]]
The Harriet Tubman Underground Railroad Visitor Center is on the grounds of the {{convert|17|acre|m2|adj=on}} Harriet Tubman Underground Railroad State Park.<ref name=stodghill />

The visitor center consists of {{convert|15000|sqft|m2}} of exhibition and administrative spread over four buildings.<ref name=mccauley /> Visitors enter through a south door, and proceed through a series of corridors and galleries to the north. The initial spaces are narrow and have low ceilings, reflecting the restrictions of slavery. Interior areas become more spacious and feature more natural light as the visitor moves to the north. The west wall of the northernmost building consists of 18 windows of varying shape, each with a different stained glass design depicting a different season of the year.<ref name=mccauley /> The {{convert|10000|sqft|m2|adj=on}} exhibit halls contains a number of interactive exhibits focused on three themes: The [[Choptank River]] area, the local community, Tubman's family, and Tubman's Christian faith; the Underground Railroad; and how Tubman and her activities remain relevant today.<ref name=dorchester /> Exhibits include a recreation of the [[corn crib]] in which Tubman and her brothers hid in 1854' a sculpture depicting the [[Raid at Combahee Ferry]], which Tubman helped lead; and displays about the disguises Tubman used, her method of soundlessly walking through the forest to avoid capture, and her use of [[Spiritual (music)|spirituals]] as a means of communication.<ref name=mccauley /> There is also a small movie theater, which will screen a 10-minute film about Tubman.<ref name=dorchester />

The {{convert|5000|sqft|m2|adj=on}} administration building provides space for Maryland Park Service and National Park Service personnel. It will also serve as the national headquarters for the National Park Service's National Underground Railroad Network to Freedom Program, which works to promote public understanding about the Underground Railroad and acts as a coordinating umbrella organization for a wide range of private, local, state, and federal Underground Railroad sites.<ref name=dorchester />

The Harriet Tubman Underground Railroad Visitor Center also features a memorial garden, {{convert|0.75|mi|km}} of walking paths through the local landscape, and a {{convert|2700|sqft|m2|adj=on}} outdoor pavilion.<ref name=dorchester /> The memorial garden contains three distinctive areas: Closely mowed lawns, meadows of knee-high grass, and woodland consisting of waist-high grass, shrubs, and trees. The garden mimics the kind of concealment (or lack of it) which Tubman and her escapees faced. Two paths lead the visitor through the garden, just as Tubman and escaping slaves often had to choose which path to take.<ref name=mccauley /> The pavilion features a stone fireplace and picnic tables, and may be reserved by large groups.<ref name=dorchester />

The state park and visitor center are managed by Dana Paterra of the Maryland Park Service.<ref name=dorchester />

The National Park Service and Maryland Park Service opened the visitor center on March 10, 2017.<ref>{{cite news|last=Washington|first=Ukee|title=Harriet Tubman Visitor's Center Opens In Maryland|work=CBS Philadelphia|date=March 10, 2017|accessdate=March 14, 2017|url=http://philadelphia.cbslocal.com/2017/03/10/harriet-tubman-visitors-center-opens-in-maryland/}}</ref>

==References==
{{reflist|2}}

==Bibliography==
*{{cite book|ref=harv|last1=Arnett|first=Earl|last2=Brugger|first2=Robert J.|last3=Papenfuse|first3=Edward C.|title=Maryland: A New Guide to the Old Line State|location=Baltimore, Md.|publisher=Johns Hopkins University Press|year=1999|isbn=9780801859793|url=https://books.google.com/books?id=lncOLHYhcrsC&printsec=frontcover#v=onepage&q&f=false}}
*{{cite book|ref=harv|last=Clinton|first=Catherine|title=Harriet Tubman: The Road to Freedom|location=New York|publisher=Little, Brown and Company|date=2004|isbn=9780316144926|url=https://books.google.com/books?id=9f5-Kg5DgbsC&printsec=frontcover#v=onepage&q&f=false}}
*{{cite book|ref=harv|last=Humez|first=Jean|title=Harriet Tubman: The Life and Life Stories|location=Madison|publisher=University of Wisconsin Press|year=2003|isbn=9780299191207|url=https://books.google.com/books?id=k9l_lA3Inw4C&printsec=frontcover#v=onepage&q&f=false}}
*{{cite book|ref=harv|last=Larson|first=Kate Clifford|title=Bound For the Promised Land: Harriet Tubman, Portrait of an American Hero|location=New York|publisher=Ballantine Books|year=2004|isbn=9780345456274|url=https://books.google.com/books?id=0-IUC00guXEC&printsec=frontcover#v=onepage&q&f=false}}

==External links==
*[http://harriettubmanbyway.org/byway-sites/ Harriet Tubman Underground Railroad Byway]
 
{{Harriet Tubman}}
{{Underground Railroad|state=expanded}}
{{Protected areas of Maryland}}

[[Category:2017 establishments in Maryland]]
[[Category:National parks in Maryland]]
[[Category:African-American museums in Maryland]]
[[Category:Parks in Dorchester County, Maryland]]
[[Category:State parks of Maryland]]
[[Category:Memorials to Harriet Tubman]]