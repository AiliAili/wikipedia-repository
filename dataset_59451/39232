{{otherships|HMS Campania}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Campania 1.jpg|300px]]
|Ship caption=HMS ''Campania'' after her second refit
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|United Kingdom|naval}}
|Ship name=HMS ''Campania''
|Ship builder=Fairfield, [[Glasgow]], [[Scotland]]
|Ship original cost=
|Ship yard number=
|Ship way number=
|Ship laid down=1892
|Ship launched=8 September 1892
|Ship sponsor=
|Ship christened=
|Ship completed=
|Ship acquired=27 November 1914
|Ship commissioned=17 April 1915
|Ship recommissioned=
|Ship decommissioned=
|Ship out of service=
|Ship refit=
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=Sank in the [[Firth of Forth]], 5 November 1918
|Ship status=
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type=[[Aircraft carrier|Aircraft]]/[[Seaplane tender|Seaplane carrier]]
|Ship displacement={{convert|20570|LT|t|abbr=on}}
|Ship length={{convert|622|ft|m|1|abbr=on}}
|Ship beam={{convert|65|ft|m|1|abbr=on}}
|Ship draught={{convert|28|ft|5|in|m|1|abbr=on}}
|Ship power={{convert|28000|ihp|kW|lk=in|abbr=on}}
|Ship propulsion=2 × shafts, 2 × 5-cylinder [[Marine steam engine#Triple or multiple expansion|VTE steam engines]]
|Ship speed={{convert|19.5|kn|lk=in|abbr=on}}
|Ship range=
|Ship capacity=
|Ship complement=600
|Ship armament=*6 × {{convert|4.7|in|mm|abbr=on}} QF guns
*1 × {{convert|3|in|mm|abbr=on}} [[Anti-aircraft warfare|AA gun]]
|Ship aircraft=10–12
|Ship aircraft facilities=1 × flying-off deck forward
|Ship notes=
}}
|}

'''HMS ''Campania''''' was a [[seaplane tender]] and [[aircraft carrier]], converted from an elderly [[ocean liner]] by the [[Royal Navy]] early in the [[First World War]]. After her conversion was completed in mid-1915 the ship spent her time conducting trials and exercises with the [[Grand Fleet]]. These revealed the need for a longer [[flight deck]] to allow larger aircraft to take off, and she was modified accordingly. ''Campania'' missed the [[Battle of Jutland]] in May 1916, but made a number of patrols with elements of the Grand Fleet. She never saw combat and was soon relegated to a training role because of her elderly machinery. In November 1918 ''Campania'' was anchored with the capital ships of the Grand Fleet when a sudden storm caused her anchors to drag. She hit several of the ships and the collisions punctured her hull; she slowly sank, with no loss of life.

==Early career==
{{main|RMS Campania}}

Originally built as a passenger liner for [[Cunard Line]]'s service between Liverpool and New York in 1893, {{RMS|Campania}} was the holder of the [[Blue Riband]] award for speed early in her career. In October 1914, she was sold to the shipbreakers [[Thos W Ward]] as she was wearing out.<ref>Gardiner, p. 66</ref>

==Purchase and conversion==
The [[Royal Navy]] purchased ''Campania'' from the shipbreakers on 27 November 1914 for £32,500, initially for conversion to an [[armed merchant cruiser]] equipped with eight [[quick-firing gun|quick-firing]] {{convert|4.7|in|adj=on}} guns. The ship was converted by [[Cammell Laird]] to an aircraft carrier instead and the two forward 4.7-inch guns were deleted in favour of a {{convert|160|ft|1|adj=on}} [[Flight deck|flying-off deck]]. Two [[derrick]]s were fitted on each side to transfer seaplanes between the water and the two [[hold (ship)|holds]]. The amidships hold had the capacity for seven large seaplanes. The forward hold, underneath the flight deck, could fit four small seaplanes, but the flight deck had to be lifted off the hold to access the airplanes. HMS ''Campania'' was [[Ship commissioning|commissioned]] on 17 April 1915.<ref>Friedman, pp. 44–45</ref>

The first takeoff from the flight deck did not occur until 6 August 1915 when a [[Sopwith Schneider]] [[floatplane]], mounted on a wheeled trolley,<ref>Maber, p. 102</ref> used {{convert|130|ft|m|1}} of the flight deck while the ship was steaming into the wind at {{convert|17|kn}}. The Sopwith aircraft was the lightest and highest-powered aircraft in service with the [[Royal Naval Air Service]], and the close call in a favourable wind demonstrated that heavier aircraft could not be launched from the flight deck.<ref name=f5>Friedman, p. 45</ref>

By October 1915 ''Campania'' had exercised with the Grand Fleet seven times, but had only flown off aircraft three times as the [[North Sea]] was often too rough for her seaplanes to use. Her captain recommended that the flying-off deck be lengthened and given a steeper slope to allow gravity to boost the aircraft's acceleration and the ship was accordingly modified at Cammell Laird between November 1915 and early April 1916. The forward funnel was split into two funnels and the flight deck was extended between them and over the [[bridge (nautical)|bridge]] to a length of {{convert|245|ft|m|1}}, so that aircraft from both holds could use the flight deck. A canvas windscreen was provided to allow the aircraft to unfold their wings out of the wind, and a [[kite balloon]] and all of its supporting equipment were added in the aft hold. ''Campania'' now carried seven [[Short Type 184]] [[torpedo bomber]]s and three or four smaller fighters or scouts; a Type 184 made its first takeoff from the flight deck on 3 June 1916, also using a wheeled trolley. This success prompted the [[Board of Admiralty|Admiralty]] to order the world's first aircraft designed for carrier operations, the [[Fairey Campania]]. The ship received the first of these aircraft in late 1917 where they joined smaller [[Sopwith 1½ Strutter]] scouts.<ref name=f5/> At various times ''Campania'' also carried the [[Sopwith Baby]] and [[Sopwith Pup]].<ref name=l0>Layman, p. 50</ref>

''Campania'' failed to receive the signal to deploy when the [[Grand Fleet]] departed [[Scapa Flow]] on 30 May 1916 en route to the [[Battle of Jutland]], but she sailed two hours and fifteen minutes later. Even though she was slowly overtaking the fleet early in the morning of 31 May, she was ordered to return to Scapa Flow as she lacked an escort and German [[submarine]]s had been reported in the area.<ref>Corbett, pp. 326a-b</ref> The ship participated in some anti-submarine and anti-Zeppelin patrols, but she was later declared unfit for fleet duty because of her defective machinery and became a seaplane training and balloon depot ship.<ref name=l0/> In April 1918 ''Campania'', along with the Grand Fleet, was transferred from Scapa Flow to [[Rosyth]].<ref>Maber, p. 103</ref>

[[Image:HMS Campania (1914) sinking.jpg|thumb|left|200px|''Campania'' sinking, 5 November 1918]]
On the morning of 5 November 1918, ''Campania'' was lying at anchor off [[Burntisland]] in the [[Firth of Forth]]. A sudden [[Beaufort scale|Force 10]] squall caused the ship to drag anchor. She collided first with the [[Bow (ship)|bow]] of the nearby [[battleship]] {{HMS|Royal Oak|08|2}}, and then scraped along the side of the [[battlecruiser]] {{HMS|Glorious||2}}. ''Campania''{{'}}s hull was breached by the initial collision with ''Royal Oak'', flooding her [[engine room]] and shutting off all main electrical power. The ship then started to settle by the [[stern]], and sank some five hours after breaking free. The ship's crew were all rescued by neighbouring vessels. A Naval [[Board of Inquiry]] into the incident held ''Campania''{{'}}s watch officer largely responsible for her loss, citing specifically the failure to drop a second anchor once the ship started to drift.<ref>Maber, pp. 103, 107</ref>

The wreck of HMS ''Campania'' was designated in 2001 under the [[Protection of Wrecks Act 1973]] as a site of historic importance, making it an offence to dive it without a licence.<ref>{{cite web|url=http://www.historic-scotland.gov.uk/campaniasitedescription.pdf#xml=http://web1:10700/texis/webinator/search/pdfhi.txt?query=campania&pr=default1&prox=page&rorder=500&rprox=500&rdfreq=500&rwfreq=500&rlead=500&rdepth=0&sufs=0&order=r&cq=&id=4cc242a57|title=Scotland’s designated wreck sites (Protection of Wrecks Act 1973) HMS Campania, off Burntisland, Firth of Forth, Fife|publisher=Historic Scotland|accessdate=24 October 2010}}</ref> The remains of the four ''Campania'' aircraft and seven 1½ Strutters that she had on board when she sank are still entombed in her wreck.<ref name=f5/>

==Footnotes==
{{Reflist|2}}

==References==
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations|edition=reprint of the 1940 second|series=History of the Great War: Based on Official Documents|volume=III|year=1997|publisher=Imperial War Museum in association with the Battery Press|location=London and Nashville, TN|isbn=1-870423-50-X}}
* {{cite book|last=Friedman|first=Norman|title=British Carrier Aviation: The Evolution of the Ships and Their Aircraft|year=1988|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-054-8}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1922|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5}}
* {{cite book|last=Layman|first=R. D.|title=Before the Aircraft Carrier: The Development of Aviation Vessels 1859–1922|year=1989|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-210-9}}
*{{cite book|year=1983|title=Warship VII|publisher=Conway Maritime Press|editor=Roberts, John|location=London|isbn=0-85177-630-2|chapter=HMS Campania 1914–1918|first=John M.|last=Maber}}

==External links==
{{Commons category|Campania (ship, 1893)}}
* [http://www.divebunker.co.uk/dive_site_pages/hms_campania.htm DiveBunker: Dive sites]

{{WWIBritishShips}}
{{November 1918 shipwrecks}}
{{Good article}}

{{DEFAULTSORT:Campania (1914)}}
[[Category:Seaplane carriers of the Royal Navy]]
[[Category:Govan-built ships]]
[[Category:1892 ships]]
[[Category:World War I aircraft carriers of the United Kingdom]]
[[Category:Maritime incidents in 1918]]
[[Category:Ships sunk in collisions]]
[[Category:World War I shipwrecks in the North Sea]]