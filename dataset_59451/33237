{{Infobox Weapon|is_artillery=yes|
|image=[[File:M256.png|300px]]
|caption=A diagram of M256 gun
|name= Rheinmetall 120 mm tank gun
|era=modern
|target=general
|origin= [[Germany]]
|type= Smoothbore tank gun
|manufacturer=[[Rheinmetall]]
|service= 1979–present
|prod_date= 1974–present
|used_by= see [[Rheinmetall 120 mm gun#Operators|the operators section]]
|wars=
|caliber= 120 mm
|part_length= 44–55 calibers
|velocity= {{convert|1580|to|1750|m/s|ft/s|abbr=on}}
|range= {{convert|4000|m|yd|sp=us}} with DM63<ref name=Eshel96 /> <br>{{convert|8000|m|yd|sp=us}} with [[LAHAT]]<ref name=JanesIAI />
|weight= {{convert|1190|kg|lb|abbr=on}} Gun barrel<br>{{convert|3317|kg|lb|abbr=on}} Gun mount
|length= L/44: {{convert|5.28|m|ft|abbr=on|sp=us}}<br> L/55: {{convert|6.6|m|ft|abbr=on|sp=us}}
}}
[[File:Rheinmetall 120 mm gun-Leoaprd 2E.jpg|300px|thumb|Muzzle of a Rheinmetall 120 mm L/55 tank gun on a [[Spain|Spanish]] [[Leopard 2E]]]]

The '''Rheinmetall 120&nbsp;mm gun''' is a [[smoothbore]] [[tank gun]] designed and produced by the West German [[Rheinmetall|Rheinmetall-DeTec AG]] company, developed in response to Soviet advances in armor technology and development of new armored threats. Production began in 1974, with the first version of the gun, known as the L/44 as it was 44 [[caliber (artillery)|calibers long]], used on the German [[Leopard 2]] tank and soon produced under license for the American [[M1A1 Abrams]] and other tanks. The American version, the M256, uses a coil spring recoil system instead of a hydraulic system.<ref>http://id3486.securedata.net/fprado/armorsite/abrams.htm</ref> The {{convert|120|mm|in|adj=on|sp=us}} gun has a length of {{convert|5.28|m|ft|sp=us}}, and the gun system weighs approximately {{convert|3317|kg|lb}}.

By 1990, the L/44 was not considered powerful enough to deal with future Soviet armour, which stimulated an effort by Rheinmetall to develop a better main armament. This first involved a {{convert|140|mm|in|adj=on|sp=us}} tank gun named ''Neue Panzerkanone 140'' ("new tank gun 140"), but later turned into a compromise which led to the development of an advanced 120&nbsp;mm gun, the L/55, based on the same internal geometry as the L/44 and installed in the same breech and mount. The L/55 is {{convert|1.3|m|ft|sp=us}} longer, giving increased muzzle velocity to ammunition fired through it. As the L/55 retains the same barrel geometry, it can fire the same ammunition as the L/44.

This gun was retrofitted into German and Dutch Leopard 2s, and chosen as the main gun of the Spanish [[Leopard 2E]] and the Greek Leopard 2HEL. It was tested on the British [[Challenger 2]] as a potential replacement for its current weapon, the rifled [[L30]] 120&nbsp;mm cannon.

A variety of ammunition has been developed for use by tanks with guns based on Rheinmetall's original L/44 design. This includes a series of [[kinetic energy penetrators]], such as the American [[M829 (munition)|M829]] series, and [[High-explosive anti-tank warhead|high explosive anti-tank warhead]]<nowiki/>s. Recent ammunition includes a range of anti-personnel rounds and demolition munitions. The [[LAHAT]], developed in Israel, is a gun-launched missile which has received interest from Germany and other Leopard 2 users, and is designed to defeat both land armour and combat helicopters. The Israelis also introduced a new anti-personnel munition which limits collateral damage by controlling the fragmentation of the projectile.

==Background==
[[File:Leo2Br80.jpg|thumb|left|Prototype of the [[Leopard 2]]]]
Because of concerns about the inability of the {{convert|105|mm|in|adj=on|sp=us}} [[Royal Ordnance L7|L7 tank gun]] then in use across [[NATO]] forces to penetrate new Soviet armor, as proved in German tests on four T-62 Soviet tanks captured by Israel following the June 1967 Six Day War, Rheinmetall was paid for the development of a new tank gun, a project started in 1965, as the Bundeswehr felt a more powerful gun was needed for its new tanks.<ref>Rheinmetall, [http://www.rheinmetall-ag.com/index.php?fid=1657&lang=3 Leopard 2: the world's most advanced main battle tank], accessed {{Nowrap|9 January}} 2009</ref><ref name="Janes44">Jane's Armour & Artillery (subscription), [http://www.janes.com/extracts/extract/jaau/jaau0021.html Rheinmetall {{Nowrap|120 mm}} L44 smoothbore gun (Germany)], accessed {{Nowrap|6 November}} 2008, claims development began in 1964.</ref> The first instance of a larger Soviet tank gun was witnessed on the chassis of a modified [[T-54/55|T-55]] in 1961.<ref>Norman, p. 14</ref> In 1965, the [[Soviet Union]]'s [[T-62]] made its first public appearance, armed with a {{convert|115|mm|in|sp=us|adj=on}} [[smoothbore]] tank gun.<ref>Zaloga (1979), p. 20</ref> The Soviet decision to increase the power of its tank's main armament had come when, in the early 1960s, an [[Iran]]ian tank commander defected over the Soviet border in a brand-new [[M60 Patton]] tank, which was armed with the British Royal Ordnance L7.<ref name="Zaloga045">Zaloga (2004), p. 5</ref> Despite the introduction of the T-62, in 1969 their [[T-64]] tank was rearmed with a new {{convert|125|mm|in|sp=us|adj=on}} tank gun,<ref name="Zaloga045" /> while in 1972 [[Nizhny Tagil]] began production of the [[T-72]] tank, also armed with the {{convert|125|mm|in|sp=us|adj=on}} gun.<ref>Zaloga (2004), p. 7</ref> For example, at the fighting at Sultan Yakoub, during the [[1982 Lebanon War]], the [[Israel]]i government claimed to have destroyed nine [[Syria]]n T-72s with the [[Merkava]] main battle tank, armed with an Israeli production version of the American M68 {{convert|105|mm|in|sp=us|adj=on}} tank gun (which in turn was based on the British L7).<ref>Warford (2006), pp. 23–24</ref> Whether or not true, the Soviets test-fired a number of Israeli M111 ''Hetz'' [[Kinetic energy penetrator|armor-piercing discarding sabot]] rounds at [[Kubinka]], finding the {{convert|105|mm|in|sp=us|adj=on}} round was able to perforate the T-72's sloped [[glacis|front section plate]] but not its turret armor.<ref>Warford (2006), p. 24</ref> In response, the Soviets developed the T-72M1.<ref>Warford (2006), p. 25</ref> This led Israel to opt for a 120&nbsp;mm tank gun during the development process of the Merkava&nbsp;III main battle tank.<ref>Katz (1997), 38</ref> This case is similar to the American decision to replace the M68 {{convert|105|mm|in|sp=us|adj=on}} tank gun with Rheinmetall's 120&nbsp;mm gun in 1976; the introduction of the T-64A had raised the question within the armor community whether the new ammunition for the existing gun caliber could effectively deal with the new Soviet tank.<ref>Green (2005), 32–33</ref>

In 1963, Germany and the United States had already embarked on a joint tank program, known as the [[MBT-70]]. The new tank carried a three-man crew, with the driver in the turret, an automatic loader for the main gun, a {{convert|20|mm|in|sp=us|adj=on}} [[autocannon]] as secondary armament, an active [[hydropneumatic suspension]] and spaced armour on the [[glacis]] plate and the front turret.<ref>Hilmes (2001), p. 17</ref> The new tank concept also had improved armament, a {{convert|152|mm|in|sp=us|adj=on}} missile-launching main gun, designed to fire the [[MGM-51 Shillelagh]] [[Anti-tank guided missile|anti-tank missile]].<ref>Zaloga (1982), p. 19</ref> However, the [[Bundeswehr|German Army]] was interested in a tank gun which could fire conventional ammunition. Although there were attempts to modify the {{convert|152|mm|in|sp=us|adj=on}} tank gun to do so, the process proved extremely difficult, and the Germans began development of the future Rheinmetall 120&nbsp;mm gun instead.<ref>McNaugher (1981), p. vi</ref>

[[File:M1-A1 Abrams Fire.jpg|thumb|250px|An [[M1 Abrams|M1A1 Abrams]], firing its US built M256 120 mm tank gun]]

In 1967, the German Ministry of Defense decided to re-open a Leopard 1 improvement program, known as the ''Vergoldeter Leopard'' ("Gilded Leopard"), later renamed the ''Keiler'' ("Wild Boar"). [[Krauss-Maffei]] was chosen as the contractor, and two prototypes were developed in 1969 and 1970.<ref>Jerchel (1998), p. 5</ref> This program grew into the [[Leopard 2]]; the first prototype of the new tank was delivered in 1972, equipped with a {{convert|105|mm|in|sp=us|adj=on}} smoothbore main gun. Between 1972 and 1975, a total of 17 prototypes were developed.<ref>Hilmes (2001), p. 18</ref> The new 120&nbsp;mm gun's ten-year development effort, which had begun in 1964, ended in 1974.<ref name="Janes44" /> Ten of the 17 turrets built were equipped with the 105&nbsp;mm smoothbore gun, and the other seven were equipped with the larger 120&nbsp;mm gun.<ref>Jerchel (1998), p. 6</ref> Another program aimed to mount the {{convert|152|mm|in|sp=us|adj=on}} missile-gun was also developed in an attempt to save components from the MBT-70, but in 1971 the program was ended for economic reasons.<ref>Jerchel (1998), pp. 6–7</ref> Instead, the Germans opted for Rheinmetall's 120&nbsp;mm L/44 smoothbore tank gun.<ref>Jerchel (1998), p. 7</ref>

==Design features==
[[File:Rheinmetall 120 mm gun-inside-muzzle view PNr°0109.JPG|thumb|right|The smoothbore barrel of an Austrian Leopard 2A4.]]
Rheinmetall's L/44 tank gun has a [[caliber]] of 120&nbsp;mm, and a [[Caliber#Caliber as measurement of length|length]] of 44 calibers ({{convert|5.28|m|ft|sp=us}}).<ref>Rheinmetall Defense, ''[http://www.rheinmetalldefence.com/index.php?fid=1448&lang=3&pdb=1 {{Nowrap|120 mm}} L44 Tank Gun]'', accessed {{Nowrap|9 November}} 2008; barrel length can be found by multiplying the caliber length by the caliber diameter.</ref> The gun's barrel weighs {{convert|1190|kg|lb}},<ref name="Maxwell0282">Maxwell (2002), p. 82</ref> and on the [[M1 Abrams]] the gun mount weighs {{convert|3317|kg|lb}},<ref>Green (2005), p. 61</ref> while the new barrel (L/55) is 55 calibers long, {{convert|1.30|m|ft|sp=us}} longer.
The [[bore evacuator]] and the gun's thermal sleeve, designed to regulate the temperature of the barrel, are made of [[glass-reinforced plastic]], while the barrel has a chrome lining to increase barrel life.<ref name="Janes44" /> Originally the gun had an EFC barrel life of ~1,500 rounds,<ref>http://www.inetres.com/gp/military/cv/weapon/M256.html</ref> but with recent advances in [[propellant]] technology the average life has increased even further.<ref>http://www.rheinmetall-defence.com/en/rheinmetall_defence/systems_and_products/weapons_and_ammunition/direct_fire/large_calibre/index.php</ref> The gun's recoil mechanism is composed of two hydraulic retarders and a hydropneumatic assembly.<ref name="Janes44" />

===Rheinmetall L/44 120mm===
Production of the German Leopard 2 and the new 120&nbsp;mm tank gun began in 1979, fulfilling an order for the German Army.<ref>Jerchel (1998), p. 11</ref> Although the American M1 Abrams was originally armed with the M68A1 105&nbsp;mm gun (a version of the L7),<ref name="Green9256">Green (1992), p. 56</ref> the [[United States Army]] had planned to fit the tank with a larger main gun at a later date,<ref>Chait (2005), p. 12</ref> and the tank's turret had been designed to accommodate a larger 120&nbsp;mm gun.<ref name="Green9256"/> The larger gun was integrated into the M1A1 Abrams, with the first vehicle coming off the production line in 1985<ref>Green (2005), pp. 24–29</ref> The gun, known as the M256, was based on the L/44 tank gun, although manufactured at [[Watervliet Arsenal]] and modified to increase the resistance of the barrels to fracture and fatigue.<ref>Chait (2005), pp. 12–13</ref> Tanks armed with versions of Rheinmetall's gun produced under licence include Japan's [[Type 90 Kyū-maru|Type 90]]<ref>Bolté (1997), p. 25</ref> and [[South Korea]]'s [[K1 88-Tank|K1A1]].<ref>Clemens (1999), p. 15; based on the United States' M256 gun.</ref> The gun had made a huge turn in technological history.<ref name="Green9256"/>

===Rheinmetall L/55 120mm===
[[File:Leopard 2A6 with new smoke dispenser.jpg|right|thumb|Two [[Leopard 2#Leopard 2A6|Leopard 2A6]]s of the [[German Army]] with L55s]]

The appearance of new Soviet tanks such as the T-80B during the late 1970s and early 1980s demanded the development of new technologies and weapons to counter the threat posed to Western armor.<ref>Jerchel (1998), p. 24</ref> The T-80B had increased firepower<ref>Baryatinskiy (2006), pp. 23–25</ref> and a new [[Composite armour|composite ceramic armor]].<ref>Baryatinskiy (2006), p. 14</ref> The [[T-72]] also went through a modernization program in an attempt to bring it up to the standards of the T-80B. In 1985 the new T-72B version entered production, with a new laminate armor protection system; its turret armor,designed primarily to defeat anti-tank missiles, surpassed the T-80B's in protection.<ref>Zaloga (1993), p. 10</ref>

The German government began the development of the Leopard 3, although this was canceled after the fall of the [[Soviet Union]].<ref>Jerchel (1998), pp. 33–34</ref> On {{Nowrap|29 October}} 1991, the governments of Switzerland, the Netherlands and Germany agreed to cooperate in the development of a modernization program for the Leopard 2. Part of this program included the introduction of a longer 120&nbsp;mm tank gun,<ref>Jerchel (1998), p. 34</ref> a cheaper alternative to a brand new tank gun,<ref name="Hilmes0476">Hilmes (2004), p. 76</ref> increasing the maximum range of the gun by an estimated {{convert|1500|m|yd|abbr=on|sp=us}}. Although the gun is longer, allowing for a higher {{convert|580|MPa|psi|0|abbr=on}} peak pressure from the propellant, the geometry remains the same, allowing the gun to fire the same ammunition as that fired from the shorter version.<ref name="Janes55">Jane's Armour & Artillery Upgrades (subscription), ''[http://www.janes.com/extracts/extract/jaau/jaau0020.html Rheinmetall {{Nowrap|120 mm}} L55 smoothbore gun (Germany)]'', accessed {{Nowrap|10 November}} 2008</ref> The longer barrel allows ammunition to attain higher velocities; for example, with new kinetic energy penetrators ammunition can reach velocities of around {{convert|1800|m/s|ft/s|abbr=on}}.<ref>Rheinmetall Defence, ''[http://www.rheinmetalldefence.com/index.php?fid=1449&lang=3&pdb=1 {{Nowrap|120 mm}} L55 Tank Gun]'', accessed {{Nowrap|10 November}} 2008</ref> The new barrel weighs {{convert|1347|kg|lb|abbr=on}}.<ref name="Maxwell0282" />

The longer tank gun has been retrofitted into the Leopard 2, creating a model known as the Leopard 2A6.<ref name="Hilmes0476" /> Both the Spanish [[Leopard 2E]] and the Greek Leopard 2HEL, as derivatives of the Leopard 2A6, use the 55 caliber-long tank gun.<ref>Candil (2007), p. 66</ref>

==Ammunition==
[[File:USARMY-M829A2.gif|thumb|American [[M829 (munition)#Variants#M829A2|M829A2]] APFSDS DU round]]

A variety of rounds have been developed for Rheinmetall's tank gun. For example, a long line of armor-piercing discarding sabot (APDS) rounds was developed by Rheinmetall. Originally, the Leopard 2 was outfitted with the DM23 kinetic energy penetrator,<ref name="Jerchel9822">Jerchel (1998), p. 22</ref> based on the Israeli M111 ''Hetz''.<ref>Jane's Ammunition Handbook (subscription), ''[http://www.janes.com/extracts/extract/jah/jah_0250.html {{Nowrap|105 mm}} M111 IMI APFSDS-T round (Israel), Tank and anti-tank guns]'', accessed {{Nowrap|11 November}} 2008</ref> The DM23 was eventually replaced by the DM33, which was also adopted by Japan, Italy, Netherlands and Switzerland. The DM33 has a three-part aluminum [[sabot]] and a two-part [[tungsten]] penetrator, and is said to be able to penetrate {{convert|560|mm|in|sp=us}} of steel armor at a range of {{convert|2000|m|yd|sp=us}}.<ref name="JanesDM33">Jane's Ammunition Handbook (subscription), ''[http://www.janes.com/extracts/extract/jah/jah_0303.html {{Nowrap|120 mm}} DM 33A1 and DM 43A1 APFSDS-T rounds (Germany)]'', accessed {{Nowrap|11 November}} 2008</ref>  The DM43 is a further development of this round, co-developed between Germany and France. The introduction of the longer barrel came hand in hand with the introduction of a new kinetic energy penetrator, the [[DM53]]. With the projectile including sabot weighing in at 8.35 kilograms with a 38:1 length to diameter ratio and with a muzzle velocity of {{convert|1750|m/s|ft/s|sp=us}}, the DM53 has an effective engagement range of up to {{convert|4000|m|yd|sp=us}}.<ref name=Eshel96>Eshel (2005), p. 96.</ref> A further development, called the DM63, improved upon the round by introducing a new temperature-independent propellant, which allows the propellant to have a constant pattern of expansion between ambient temperatures inside the gun barrel from {{convert|-47|C|F}} to {{convert|+71|C|F}}. The new propellant powders, known as surface-coated double-base (SCDB) propellants, allow the DM63 to be used in many climates with consistent results.<ref>Hilmes (2007), p. 93.</ref> The new ammunition has been accepted into service with the Dutch and Swiss, as well as German, armies.<ref>Jane's Ammunition Handbook (subscription), ''[http://www.janes.com/extracts/extract/jah/jah_0304.html {{Nowrap|120 mm}} DM53 and DM63 LKE II APFSDS-T round (Germany)]'', accessed {{Nowrap|11 November}} 2008</ref>

The United States developed its own [[kinetic energy penetrator]] (KEP) tank round in the form of an Armor-Piercing Fin-Stabilized Discarding-Sabot ([[APFSDS]]) round, using a [[depleted uranium]] (DU) alloy long-rod penetrator (LRP), designated as the [[M829 (munition)|M829]],<ref>Green (2005), p. 68.</ref> followed by improved versions. An immediate improvement, known as the M829A1, was called the "Silver Bullet" after its good combat performance during the [[Gulf War]] against [[Iraq]]i [[T-54/55|T-55s]], [[T-62]]s and [[T-72]] tanks.<ref>Green (1992), p. 74</ref> The M829 series centers around the depleted uranium penetrator, designed to penetrate enemy armor through kinetic energy and to shatter inside the turret, doing much damage within the tank.<ref>Green (1992), pp. 76–77.</ref> In 1998, the United States military introduced the M829A2, which has an improved depleted uranium penetrator and composite sabot petals.<ref>Green (2005), p. 69.</ref> In 2002, production began of the ($10,000 per round) [[M829 (munition)#M829A3|M829A3]] using a more efficient [[propellant]] (RPD-380 stick),<ref>{{cite web|url=http://www.atk.com/products-services/120mm-m829a3-apfsds-t-ammunition | title=ATK Specifications: 120mm M829A3 APFSDS-T Ammunition | accessdate=April 30, 2014}}</ref> a lighter injection-molded sabot, and a longer (800mm) and heavier (10&nbsp;kg / 22&nbsp;lb) DU penetrator, which is said to be able to defeat the [[Kontakt-5|latest versions]] of Russian [[Kontakt-5]] [[explosive reactive armor]] (ERA).<ref>Green (2005), p. 70.</ref> This variant is unofficially referred to by Abrams tank crews as the "super sabot".<ref>{{cite web | url=http://www.defense-update.com/products/digits/120ke.htm | title=120mm Tank Gun KE Ammunition | publisher=Defense Update | accessdate=2007-09-03 |date=2006-11-22}}</ref> In response to the M829A3, the Russian army designed [[Kontakt-5|Relikt]], the most modern Russian ERA, which is claimed to be twice as effective as Kontakt-5. A further improved M829E4 round with a segmented penetrator to defeat Relikt has been under development since 2011 and was to be fielded as the M829A4 in 2015.

Both Germany and the United States have developed several other rounds. These include the German DM12 multi-purpose anti-tank projectile (MPAT), based on the technology in a [[high explosive anti-tank]] (HEAT) warhead.<ref name="Jerchel9822" /> However, it has been found that the DM12's armor-killing abilities are limited by the lack of blast and fragmentation effects, and that the round is less valuable against lightly armored targets.<ref>Eshel (2005), p. 98.</ref> The United States also has a MPAT type projectile, known as the [[M830]].<ref>Green (2005), p. 71.</ref> This was later developed into the M830A1, which allows the M1 Abrams to use the round against helicopters.<ref>Fogg (1994), p. 12.</ref> The M1 Abrams can use the M1028 canister round, which is an anti-personnel/anti-helicopter munition, packed with over 1,000 tungsten balls.<ref>Green (2005), p. 72.</ref> The United States Armed Forces accepted a new demolition round, called the M908 Obstacle Defeating Round, based on the M830A1 MPAT, but with the proximity fuse replaced by a hardened nose cap. The cap allows the round to impact and embed itself in concrete, then exploding inside the target and causing more damage.<ref>Hilmes (2007), pp. 92–93.</ref>

The Israeli Army introduced a new round known as the Laser Homing Anti-Tank ([[LAHAT]]) projectile.<ref name=JanesIAI>Jane's Armour & Artillery Upgrades (subscription), ''[http://www.janes.com/extracts/extract/jaau/jaau9019.html Israel Aerospace Industries LAser Homing Anti-Tank (LAHAT) projectile (Israel), Gun-launched guided projectiles]'', accessed {{Nowrap|13 November}} 2008</ref> Using a semi-active laser homing guidance method, the LAHAT can be guided by the tank's crew or by teams on the ground, while the missile's trajectory can be selected to either attack from the top (to defeat enemy armor) or direct attack (to engage enemy helicopters). Furthermore, the missile can be fired by both {{convert|105|mm|in|sp=us|adj=on}} and 120&nbsp;mm tank guns.<ref>Gelbart (2004), pp. 40–41</ref> The LAHAT has been offered as an option for the Leopard 2, and has been marketed by both Israel Military Industries and Rheinmetall to Leopard 2 users.<ref>Eshel (2005), p. 100</ref> Israeli Merkavas make use of a round known as the APAM, which is an anti-personnel munition designed to release fragmentation at controlled intervals to limit the extent of damage. Fragments are shaped to have enough kinetic energy to penetrate body armor.<ref>Eshel (2003), p. 46</ref>

Poland has introduced a series of projectiles for Rheinmetall's tank gun, including an armor-piercing penetrator target practice round (APFSDS-T-TP), a high-explosive round, and a high-explosive target practice (HE-TP) projectile. The ammunition is manufactured by ''Zakłady Produkcji Specjalnej Sp. z o.o.''<ref>Bumar, ''{{Nowrap|120 mm}} Rounds for Rh 120 L 44 Tank Gun''</ref>

==Rheinmetall 130 mm gun==
Rheinmetall introduced a larger 130&nbsp;mm tank gun at [[Eurosatory]] 2016 in June 2016.  Development commenced in 2015, financed entirely using internal funding, as a response to the Russian introduction of new generation armored vehicles like the [[T-14 Armata]] tank, and the first technical demonstrator (TD) was completed in May 2016.  The new 130&nbsp;mm gun has an L/51 chrome-lined smoothbore barrel with a vertical sliding breech mechanism, increased chamber volume, no muzzle brake, a thermal sleeve, and a muzzle reference system (MRS) enabling it to be bore sighted on a more regular basis without the crew needing to leave the platform.  Compared to the {{convert|2700|kg|lb|abbr=on}} 120&nbsp;mm gun, the 130&nbsp;mm has a {{convert|1400|kg|lb|abbr=on}} barrel and an all-up weight of {{convert|3000|kg|lb|abbr=on}} including the recoil system.

Rheinmetall is developing a new generation APFSDS round featuring a semi-combustible cartridge case, new propellant, and new advanced long rod tungsten penetrator as well as a high-explosive air-bursting munition (HE ABM) based on the 120&nbsp;mm DM11 HE ABM in parallel with the gun; the cartridges are {{convert|30|kg|lb|abbr=on}} and {{convert|1.3|m|ft|abbr=on|sp=us}} long that, according to the company, with the increase of 8% in caliber results in 50% more kinetic energy over the 120&nbsp;mm gun.

Engineers believe the weapon can only be used with an automatic loader and new turret design.  The gun commenced static firing trials at Rheinmetall's proving ground following Eurosatory, while engineers hope to receive a new NATO standard by the end of 2016, although development of the gun and ammunition will likely take 8-10 years.  The 130&nbsp;mm is designed to equip the Main Ground Combat System (MGCS), a joint effort between Germany and France to produce a successor to the Leopard 2 and Leclerc, possibly to be launched between 2025-2030.<ref>[http://www.defensenews.com/story/defense/show-daily/eurosatory/2016/06/15/tank-gun-german-rheinmetall-130mm/85920592/ German Rheinmetall works on new 130mm tank gun] - Defensenews.com, 15 June 2016</ref><ref>{{cite web |url=http://www.janes.com/article/61255/eurosatory-2016-rheinmetall-lifts-the-lid-on-new-130-mm-tank-gun|title=Eurosatory 2016: Rheinmetall lifts the lid on new 130 mm tank gun|last1=Foss|first1=Christopher F |date=16 June 2016 |website=[[Jane's Information Group]] |access-date=7 September 2016}}</ref><ref>[http://defense-update.com/20160614_rheinmetall-ups-tank-firepower-with-new-130mm-gun.html Rheinmetall Ups Tank Firepower with new 130mm Gun] - Defense-Update.com, 14 June 2016</ref>

==Operators==
[[File:120mm Rheinmetall operators.png|thumb|400px|Map with Rheinmetall 120mm operators in blue with former operators in red]]
Due to tank sales, Rheinmetall's L/44 tank gun has been manufactured for other nations. For example, the Leopard 2 armed with the 44 caliber long gun, has been sold to the [[Netherlands]], [[Switzerland]], [[Sweden]], [[Spain]], [[Austria]], [[Denmark]], [[Finland]], and other countries.<ref>Jerchel (1998), pp. 36–42</ref> [[Egypt]] had manufactured 700–800 M1A1 Abrams by 2005,<ref>Green (2005), p. 25</ref> and in 2008 requested permission to build another 125 tanks; their M256 main guns (the US version of the L/44) were manufactured by [[Watervliet Arsenal]].<ref>Defense Industry Daily, ''[http://www.defenseindustrydaily.com/egypt-847m-request-for-125-m1a1-tanks-03684/ Egypt: $889M Request for 125 M1A1 Tanks]'', accessed {{Nowrap|9 November}} 2008</ref> The M1A1 has also been exported to Australia,<ref>Jane's Defense Weekly (subscription), ''[http://www.janes.com/extract/jdw2004/jdw08159.html Australia prepares for M1A1s]'', accessed {{Nowrap|9 November}} 2008</ref> while the M1A2 Abrams has been exported to [[Saudi Arabia]] and [[Kuwait]].<ref>Green (2005), p. 34</ref> The American license-built M256 has also been offered by [[General Dynamics Land Systems]] as part of the [[M60-2000 Main Battle Tank]] which would upgrade older [[M60 Patton]] tanks to have capabilities of their M1A1 Abrams at a reduced cost, though the company has not yet found a buyer.

{| class="wikitable" border=0 cellspacing=0 cellpadding=3 style="border-collapse:collapse; text-align:left;" summary="L/44 Gun Use"
|+ Use of Rheinmetall's L/44 Tank Gun
|- style="vertical-align:bottom; border-bottom:1px solid #999;"
! style="text-align:left;" | Tank
! style="text-align:left;" | Designer
! style="text-align:left;" | Country
! style="text-align:left;" | Gun
! style="text-align:left;" | Users
|- style="vertical-align:top; "
! style="text-align:right;" | [[Leopard 2]]
| [[Krauss-Maffei]]
| {{flagcountry|Germany}}
| Rheinmetall 120&nbsp;mm L/44
| Austria, Canada, Chile, Denmark, Finland, Greece, Indonesia, the Netherlands, Norway, Poland, Portugal, Singapore, Spain, Sweden, Switzerland, Turkey
|- style="vertical-align:top; "
! style="text-align:right;" | [[M1 Abrams|M1A1 Abrams]]
| [[General Dynamics Land Systems]]
| {{flagcountry|United States}}
| M256 (L/44)
| Australia, Egypt, Iraq, Kuwait, Saudi Arabia
|- style="vertical-align:top; "
! style="text-align:right;" | [[Type 90 Kyū-maru|Type 90]]
| [[Mitsubishi Heavy Industries]]
| {{flagcountry|Japan}}
| Rheinmetall 120&nbsp;mm L/44
|
|- style="vertical-align:top; "
! style="text-align:right;" | [[K1 88-Tank|K1A1]]
| [[Hyundai Rotem]]
| {{flagcountry|South Korea}}
| KM256
|
|- style="vertical-align:top; "
! style="text-align:right;" | [[C1 Ariete ]]
| [[OTO Melara]]
| {{flagcountry|Italy}}
| Rheinmetall 120 mm L/44
|
|}

The Leopard 2A6 and its longer L/55 main gun have been exported for use by the [[Canadian Army]], and the Netherlands upgraded part of its original fleet of Leopard 2s with the more powerful armament.<ref>Defense Industry Daily, ''[http://www.defenseindustrydaily.com/tanks-for-the-lesson-leopards-too-for-canada-03208/ Tanks for the Lesson: Leopards, too, for Canada]'', accessed {{Nowrap|10 November}} 2008</ref> The [[British Army]] has tested Rheinmetall's longer gun, possibly looking to replace the current [[L30]]A1 120&nbsp;mm L/55 rifled main gun on the [[Challenger 2]].<ref>Rheinmetall Defence, ''[http://www.rheinmetall-detec.de/index.php?lang=3&fid=3210 Rheinmetall {{Nowrap|120 mm}} smoothbore technology for Britain's Challenger]'', accessed {{Nowrap|10 November}} 2008</ref> Two Challenger 2s were modified to undergo firing trials.<ref>Hilmes (2007), p. 88</ref> Although South Korean [[K2 Black Panther]] is equipped with a L/55 main gun and shows similar characteristics as its German counterpart, it is indigenously developed by Agency for Defense Development and [[World Industries Ace Corporation]] (WIA), a Korea-based powertrain company affiliated with Hyundai Kia Motors Group.<ref>http://consensus.hankyung.com/hankyung/file_down.php?pdf=SK20160105%B1%E2%B0%E8.pdf., p.14</ref>

{| class="wikitable" border=0 cellspacing=0 cellpadding=3 style="border-collapse:collapse; text-align:left;" summary="L/55 Gun Employment"
|+ Use of Rheinmetall's L/55 Tank Gun
|- style="vertical-align:bottom; border-bottom:1px solid #999;"
! style="text-align:left;" | Tank
! style="text-align:left;" | Designer
! style="text-align:left;" | Country
! style="text-align:left;" | Gun
! style="text-align:left;" | Proliferation
|- style="vertical-align:top; "
! style="text-align:right;" | Leopard 2A6
| [[Krauss-Maffei]]
| {{flagcountry|Germany}}
| Rheinmetall 120&nbsp;mm L/55
| Canada, Finland, Greece, Netherlands, Portugal, Spain
|- style="vertical-align:top; "
! style="text-align:right;" | [[Altay (tank)]]
| [[Roketsan]], [[Aselsan]], [[Otokar]], [[Hyundai Rotem]], [[MKEK]]
| {{flagcountry|Turkey}}
| Rheinmetall 120&nbsp;mm L/55
| Turkey
|}

==See also==
;Weapons of comparable role, performance and era
* [[L30]]: British rifled equivalent
* [[EXP-28M1 120mm rifled tank gun]]: Experimental British weapon of the late 1970s/early 1980s. Was to have equipped the MBT-80.
* [[GIAT CN120-26/52]]: French equivalent
* [[IMI 120 mm gun]]: Israeli equivalent
* [[WIA 120 mm gun]]: South Korean equivalent
* [[2A46 125 mm gun]]: Russian 125-mm equivalent
* [[2A82 125 mm gun]]: new Russian 125-mm equivalent

==Notes==
{{Reflist|colwidth=30em}}

==Sources==
{{Refbegin|2|colwidth=60em}}
* {{Cite web
 | title = 120 mm Rounds for Rh 120 L 44 Tank Gun
 | location = Warsaw, Poland
 | publisher = Bumar
|format=pdf
 | url = http://www.bumar.com/files/document/249.pdf}}
* {{cite book
 | last = Baryatinskiy
 | first = Mikhail
 | authorlink =
 | title = Main Battle Tank T-80
 | publisher = Ian Allen
 | year = 2007
 | location = Surrey, United Kingdom
 | isbn = 0-7110-3238-6
 | page = 96}}
* {{cite journal
 | last = Bolté
 | first = Philip L.
 |author2=Iwao Hayashi
  | title = Japanese Armored Vehicle Development
 | journal = ARMOR
 | publisher = U.S. Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 January 1997}}
* {{cite journal
 | last = Burton
 | first = Larry
 |author2=Robert Carter |author3=Victor Champagne | title = Army Targets Age Old Problems with New Gun Barrel Technologies
 | journal = AMPTIAC Quarterly
 | publisher = Advanced Materials and Processes Technology Information Analysis Center
 | location = Rome, New York
 | volume = 8
 | issue = 4
 | date = 1 January 2004|display-authors=etal}}
* {{cite journal
 | last = Candil
 | first = Antonio
 | title = The Spanish Leopard 2E: A Magnificent Tool
 | journal = Military Technology
 | pages = 2
 | publisher = Mönch Editorial Group
 | date = 1 February 2007
 }}
* {{cite journal
 | last = Chait
 | first = Richard
 | author2 = John Lyons |author3 = Duncan Long
 | title = Critical Technology Events in the Development of the M1 Abrams
 | publisher = Center for Technology and National Security Policy
 | date = 1 December 2005}}
* {{cite journal
 | last = Clemens
 | first = Jon
 | title = Tank Assessment Survey Ranks Leopard 2A6 Tops, With the M1A1 the Runner-up
 | journal = ARMOR
 | publisher = U.S. Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 July 1999}}
* {{cite book
 | last = Dunstan
 | first = Simon
 | authorlink =
 | title = Challenger 2 Main Battle Tank 1987–2006
 | publisher = Osprey
 | year = 2006
 | location = Oxford, United Kingdom
 | isbn = 1-84176-815-4
 | page = 48}}
* {{cite journal
 | last = Eshel
 | first = David
 | title = The Merkava Mk 4 – Israel's Newest MBT Enters Service
 | journal = ARMOR
 | publisher = U.S. Armor Center
 | location = Fort Knox, KY
 | date = 1 January 2003}}
* {{cite journal
 | last = Eshel
 | first = Tamir
 | title = Improving the Leopard Firepower: More Potent Ammunition Prepares the Leopard to Face a Wide Spectrum of Missions
 | journal = Military Technology
 | publisher = Mönch Editorial Group
 | date = 1 February 2005
 }}
* {{cite journal
 | last = Fogg
 | first = William
 |author2=Robert Horner
  | title = The New MPAT Round
 | journal = ARMOR
 | publisher = U.S. Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 May 1994}}
* {{cite book
 | last = Gelbart
 | first = Marsh
 | authorlink =
 | title = Modern Israeli Tanks and Infantry Carriers 1985–2004
 | publisher = Osprey
 | year = 2008
 | location = Oxford, United Kingdom
 | isbn = 1-84176-579-1
 | page = 48}}
* {{cite book
 | last = Green
 | first = Michael
 | authorlink =
 |author2=Greg Stewart
  | title = M1 Abrams At War
 | publisher = Zenith Press
 | year = 2005
 | location = St. Paul, Minnesota
 | isbn = 0-7603-2153-1
 | page = 127}}
* {{cite book
 | last = Green
 | first = Michael
 | authorlink =
 | title = M1 Abrams Main Battle Tank: The Combat and Development History of the General Dynamics M1 and M1A1 Tanks
 | publisher = Motorbooks International
 | year = 1992
 | location = Osceola, Wisconsin
 | isbn = 0-87938-597-9
 | page = 96}}
* {{cite journal
 | last = Hilmes
 | first = Rolf
 | title = Arming Future MBTs – Some Considerations
 | journal = Military Technology
 | publisher = Mönch
 | date = 1 December 2004}}
* {{cite journal
 | last = Hilmes
 | first = Rolf
 | title = Development Trends in Tank Armament
 | journal = Military Technology
 | publisher = Mönch
 | date = 1 March 2007}}
* {{cite book
 | last = Jerchel
 | first = Michael
 | authorlink =
 |author2=Uwe Schnellbacher
  | title = Leopard 2 Main Battle Tank 1979–1998
 | publisher = Osprey
 | year = 1998
 | location = Oxford, United Kingdom
 | isbn = 1-85532-691-4
 | page = 48}}
* {{cite book
 | last = Katz
 | first = Sam
 | authorlink =
 | title = Merkava Main Battle Tank MKs&nbsp;I, II & III
 | publisher = Osprey
 | year = 1997
 | location = Oxford, United Kingdom
 | isbn = 1-85532-643-4
 | page = 48}}
* {{cite book
 | last = Lathrop
 | first = Richard
 | authorlink =
 |author2=John McDonald
  | title = M60 Main Battle Tank 1960–91
 | publisher = Osprey
 | year = 2003
 | location = Oxford, United Kingdom
 | isbn = 1-84176-551-1
 | page = 48}}
* {{cite journal
 | last = Maxwell
 | first = David
 | title = New Tanks for the Old, Part&nbsp;II: Tank Top Upgrades
 | journal = Armada International
 | id = 0252-9793
 | date = 1 June 2002}}
* {{cite journal
 | last = Maxwell
 | first = David
 | title = Try a 120 For Size
 | journal = Armada International
 | id = 0252-9793
 | date = 1 February 2003}}
* {{cite book
 | last = Norman
 | first = Michael
 | authorlink =
 | title = Soviet Mediums T44, T54, T55 & T62
 | publisher = Profile Publications Ltd
 | location = Berkshire, United Kingdom}}
* {{cite journal
 | last = Warford
 | first = James M.
 | title = The Secret Testing of Israeli M111 "Hetz" Ammunition: A Model of Failed Commander's Responsibility
 | journal = ARMOR
 | publisher = U.S. Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 September 2006}}
* {{cite book
 | last = Zaloga
 | first = Steven J.
 | authorlink =
 | author2 = (Lt. Col.) James W. Loop
 | title = Modern American Armor: Combat Vehicles of the United States Army Today
 | publisher = Arms and Armour Press
 | year = 1982
 | location = New York City, New York
 | isbn = 0-85368-248-8
 | page = 88 }}
* {{cite book
 | last = Zaloga
 | first = Steven J.
 | authorlink =
 | title = Modern Soviet Armor: Combat Vehicles of the USSR and Warsaw Pact Today
 | publisher = Prentice Hall
 | year = 1979
 | location = Edinburg, United Kingdom
 | isbn = 0-13-597856-4
 | page = 88 }}
* {{cite book
 | last = Zaloga
 | first = Steven J.
 | authorlink =
 | title = T-54 and T-55 Main Battle Tanks 1944–2004
 | publisher = Osprey
 | year = 2004
 | location = Oxford, United Kingdom
 | isbn = 1-84176-792-1
 | page = 48}}
* {{cite book
 | last = Zaloga
 | first = Steven J.
 | authorlink =
 | title = T-72 Main Battle Tank 1974–93
 | publisher = Osprey
 | year = 1993
 | location = Oxford, United Kingdom
 | isbn = 1-85532-338-9
 | page = 48}}
* {{cite book
 | last = Zaloga
 | first = Steven J.
 | authorlink =
 | title = The M47 and M48 Patton Tanks
 | publisher = Osprey
 | year = 1999
 | location = Oxford, United Kingdom
 | isbn = 1-85532-825-9
 | page = 48}}
{{Refend}}

==External links==
{{Commons category|Rheinmetall 120 mm guns}}
*{{cite web|last=Huls|first=Harlan|title=Firing US 120mm Tank Ammunition in the Leopard 2 Main Battle Tank|url=http://www.dtic.mil/ndia/2008gun_missile/6526Huls.pdf|work=NDIA Guns and Missiles Conference (dtic.mil)|publisher=Alliant Techsystems (ATK)|accessdate=14 January 2012|date=22 April 2008}}
*[http://www.rheinmetall-defence.com/en/rheinmetall_defence/systems_and_products/weapons_and_ammunition/direct_fire/large_calibre/index.php Website of ''Rheinmetall Defence'' ]
{{Rheinmetall}}
{{Featured article}}

{{DEFAULTSORT:Rheinmetall 120 Mm Gun}}
[[Category:120 mm artillery]]
[[Category:Tank guns of Germany]]
[[Category:Cold War artillery of Germany]]
[[Category:Rheinmetall|120 mm gun]]
[[Category:Tank guns]]