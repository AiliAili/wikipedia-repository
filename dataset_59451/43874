<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Wag-Aero CHUBy CUBy
 | image=Wag-Aero SPORTSMAN 2+2 C-GMTL 01.JPG
 | caption=Sportsman 2+2
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Wag-Aero]]
 | designer=Dick Wagoner, Tom Iverson
 | first flight=May 8, 1982
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=40 (December 2011)<ref name="KitplanesDec2011" />
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Wag-Aero Wag-a-Bond]]
 | variants with their own articles=
}}
|}

[[File:SuperChub.jpg|thumb|SC-360 Super Chub. A modified Wag-Aero Sportsman 2+2 with a [[Lycoming O-360]] engine]]
The '''Wag-Aero CHUBy CUBy''' is a high-wing four-seat [[Homebuilt aircraft|homebuilt]] cabin [[monoplane]] of [[tube-and-fabric construction]], it is a modern representation of the  [[Piper PA-14 Family Cruiser|Piper PA-14]] taildragger with elements from other Piper family members. The plane is currently marketed as the '''Wag-Aero Sportsman 2+2'''.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2011 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 53. Belvoir Publications. {{ISSN|0891-1851}}</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', page 290. BAI Communications. ISBN 0-9636409-4-1</ref><ref name="WAGAEROHome">{{Cite web|url = http://www.wagaero.com/sport2x2.html|title = Sportsman 2+2|accessdate = 3 October 2010|last = Wag-Aero|authorlink = |year = n.d.}}</ref>

==Design and development==
The CHUBy CUBy was the third homebuilt replica of a Piper product from parts supplier Wag-Aero. The [[Piper PA-14 Family Cruiser|PA-14]] line was a popular aircraft for Alaska floatplane operations, the CHUBy CUBy was put to market to allow new examples to be built. The aircraft has an optional large opening to the baggage compartment similar to the [[Piper J-5|Piper HE-1]] ambulance style door.<ref name="WAGAEROHome" />

The CHUBy CUBy closely resembles the [[Piper PA-14 Family Cruiser|Piper PA-14]], but has several modifications. The recommended engine is the [[Lycoming O-320|Lycoming O-320-E2D]] of {{convert|150|hp|kW|0|abbr=on}} or [[Lycoming O-290]] of {{convert|135|hp|kW|0|abbr=on}} hung on a custom-designed swing-out engine mount. The CHUBy CUBy has swing-up doors on both sides of the cabin and two wing-mounted fuel tanks with a small header tank. The [[fuselage]] is welded from [[4130 steel]] tubing rather than 1020 grade steel used in the original. The wings include spoilers to keep the aircraft on the ground and avoid floating.<ref name="Cox">{{cite journal|magazine=Sport Aviation|title=The CHUBY CUBBY|author=Jack Cox|date=July 1982}}</ref>

Wag-Aero company president Dick Wagoner flew the CHUBy CUBy for the first time on May 8, 1982.<ref name="Cox" />
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications CHUBy  CUBy==
[[File:WagAero Sportsman 2+2 C-GMTL 07.JPG|right|thumb|Sportsman 2+2 on skiis]]
{{Aircraft specs
|ref=Manufacturer{{Citation needed|date=August 2013}}<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=three passengers
|length m=
|length ft=23
|length in=4.4
|length note=
|span m=10.896
|span ft=35
|span in=9
|span note=
|height m=
|height ft=6
|height in=5
|height note=
|wing area sqm=
|wing area sqft=174.12
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=USA 35B Modified
|empty weight kg=
|empty weight lb=1080
|empty weight note=
|gross weight kg=
|gross weight lb=2200
|gross weight note=
|fuel capacity=39 gallons
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming O-360]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=180<!-- prop engines -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=134
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=124
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=34<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=670
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=14800
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=800
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=11.47
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{commons category|Wag-Aero CHUBy CUBy}}
{{commons category|Wag-Aero Sportsman 2+2}}
*{{Official website|http://www.wagaero.com/sport2x2.html}}

{{Wag-Aero}}

[[Category:Wag-Aero aircraft]]
[[Category:Homebuilt aircraft]]
[[Category:United States civil utility aircraft 1980–1989]]