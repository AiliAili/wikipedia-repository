{{Infobox musical artist
| background   = solo_singer
| image        = Galen Crew.jpg
| birth_date   = {{birth date and age|1990|7|11}}
| birth_place  = 
| death_date   = 
| death_place  = 
| occupation   = Artist, music producer, singer-songwriter
| instrument   = Acoustic guitar, electric guitar, ukulele, piano
| years_active = 2009–present
| website      = {{URL|http://www.galencrew.com}}
}}

'''Galen Michael Crew''' (born July 11, 1990) is an [[Americans|American]] [[singer-songwriter]], [[guitarist]] and [[Record producer|producer]]. Crew played in many different local [[Musical ensemble|bands]] through middle school, high school, and college, most notably "Seraph", a [[hard rock]] band in high school, and "Vinyl Relay".<ref>{{Cite web|url=http://www.williamsonherald.com/features/w_life/article_f25e09e5-11c7-579d-a484-6d4774a6eab1.html|title=Good times and good tunes at fair's Battle of the Bands|access-date=2016-07-17}}</ref> The single "Princess" from the album ''Better than a Fairy Tale'' was featured on the [[American Broadcasting Company|ABC]] show [[Good Morning America]].<ref>{{Cite web|url=http://www.syracuse.com/entertainment/index.ssf/2013/12/princess_annie_wish.html|title=Princess Annie's greatest wishes come true at Make-A-Wish Grand Royal Ball|access-date=2016-07-17}}</ref> Crew's songs went viral in [[China]], reaching in the tens of millions on Chinese music streaming websites, such as [[NetEase]], in 2015.<ref name=":1">{{Cite web|url=http://v.youku.com/v_show/id_XMTU1MjMyODUxMg==.html?from=y1.2-1-102.3.2-2.1-1-1-1-0|title=Sleepyhead|website=v.youku.com|access-date=2016-07-17}}</ref><ref>{{Cite web|url=http://yue.ifeng.com/a/20161115/39767024_0.shtml|title=口碑响至全球 网易云音乐迎来欧美大牌艺人批量入驻|website=yue.ifeng.com|access-date=2017-04-10}}</ref> In response to new following in China, Crew was sponsored by the Chinese company XiaoKang (Healthy Household) and toured the country for the first time in May 2016.<ref name=":2">{{Cite web|url=http://en.damai.cn/event/tickets_102217/|title=Galen Crew 2016 China Tour In Shanghai|website=en.damai.cn|access-date=2016-07-17}}</ref><ref name=":3">{{Cite web|url=http://galen.xiaokang.com/|title=盖伦 Galen Crew|website=galen.xiaokang.com|access-date=2016-07-17}}</ref> Crew filmed the music video for the single ''Fragrance'' in China while on tour.<ref>{{Cite web|url=http://www.wolfinasuit.com/2017/03/20/top-5-new-indie-pop-week-11/|title=Top 5 New Indie Pop Week 11|date=2017-03-20|website=Wolf in a Suit|access-date=2017-04-10}}</ref>

== Early life ==
Crew's high school band was Seraph, which was composed of Crew on electric guitar, Josh Caste as lead vocals, Nicky Hackett on bass, and Jordan Thompson on drums.<ref>{{Cite web|url=http://s1.purevolumecdn.com/seraphtn|title=Seraph (TN)|publisher=PureVolume|access-date=2016-07-17}}</ref>

== Touring ==
In 2013, Crew toured with [[Roger Cook (songwriter)|Roger Cook]] in the United Kingdom.<ref>{{Cite web|url=http://www.bristolpost.co.uk/review-roger-cook-st-george-s-ndash-bristol-born/story-18684795-detail/story.html|title=Review: Roger Cook at St George's –  Bristol-born legend we should be very proud of by Keith Clark 8/10|work=The Bristol Post|date=2013-04-12|access-date=2016-07-27}}</ref> In 2014, Crew toured with [[Phil Joel]] across the United States.<ref>{{Cite web|url=http://www.sosradio.net/node/13466|title=Phil Joel & Galen Crew – SLC|date=2011-11-14|website=SOS Radio|access-date=2016-07-17}}</ref><ref>{{Cite web|url=http://eventful.com/sanford_nc/events/galen-crew-phil-joel-/E0-001-065565228-4|title=Galen Crew – Phil Joel|website=Eventful|access-date=2016-07-17}}</ref> Crew's total streaming song listens have reached in the tens of millions on Chinese sites.<ref name=":1" /><ref>{{Cite web|url=http://www.kuwo.cn/album/508563/|title=Acoustic Daydreams – Galen Crew|website=kuwo.cn|access-date=2016-07-27}}</ref>

Crew was sponsored by the Chinese company XiaoKang and toured the China for the first time in May 2016.<ref name=":2" /><ref name=":3" /> Crew has toured in [[Shanghai]],<ref>{{Cite web|url=http://www.cityweekend.com.cn/shanghai/article/top-things-to-do-week-shanghai-100|title=Top Things To Do This Week in Shanghai|website=cityweekend.com.cn|access-date=2016-07-17}}</ref> [[Guangzhou]], [[Beijing]], and [[Hong Kong]]. Crew returned to China for another tour in the Fall of 2016, touring in 13 cities, including [[Zibo]].<ref>{{Cite web|url=https://twitter.com/galencrew/status/747629127184744448|title=Galen Crew|via=Twitter|access-date=2016-07-17}}</ref><ref>{{Cite web|url=https://digitaltourbus.com/features/galen-crew-crazy-tour-stories/|title=Galen Crew – CRAZY TOUR STORIES {{!}} Digital Tour Bus|website=digitaltourbus.com|language=en-US|access-date=2017-04-10}}</ref>

== Discography ==
Crew has released three EPs and three LPs:
* ''Better Than a Fairy Tale'' (2012)
* ''Acoustic Daydreams'' (2012)
* ''Like Fire'' EP (2013)
* ''Christmas Spirit'' EP (2013)
* ''Let Them Sing'' (2014)
* ''Sleepyhead'' EP (2016)

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.galencrew.com/}}
* {{allmusic|artist/galen-crew-mn0002753384}}

{{DEFAULTSORT:Crew, Galen}}
[[Category:1990 births]]
[[Category:Living people]]