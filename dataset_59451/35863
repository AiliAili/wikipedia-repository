{{Infobox Christian leader
| name = Æthelbert
| title = [[Archbishop of York]]
| image = 
| imagesize = 
| alt = 
| caption = 
| appointed = 766
| ended = 8 November 780
| predecessor = [[Ecgbert (archbishop of York)|Ecgbert]]
| successor = [[Eanbald I]]
| consecration = 24 April 767
| death_date = 8 November 780
}}

'''Æthelbert'''{{efn|Sometimes '''Æthelberht''',<ref name=Handbook224/> '''Albert''',<ref name=Barr488/> '''Ælberht''',<ref name=DNB/> '''Aethelberht''',<ref name=Kirby127/> or '''Ælbert'''<ref name=Levinson153>Levinson ''England and the Contintent'' p. 153</ref><!--''''Adalberht''',  '''Aldbert''' or '''Ethelbert''' - all these are uncited, leaving in nowiki limbo while awaiting citations for them&nbsp;– Ealdgyth 25 July 2013-->}} (died 8 November 780) was an eighth century scholar, teacher, and [[Archbishop of York]]. Related to his predecessor at York, he became a monk at an early age and was in charge of the cathedral's library and school before becoming archbishop. He taught a number of missionaries and scholars, including [[Alcuin]], at the school. While archbishop Æthelbert rebuilt the cathedral and sent missionaries to the Continent. Æthelbert retired before his death, and during his retirement built another church in York.

==Early life==

Æthelbert, was the teacher and intimate friend of [[Alcuin]], whose poem on the saints and prelates of the Church of York, ''Versus de Patribus Regibus et de Sanctis et Pontificibus Ecclesiæ Eboracensis'', is the principal source of information concerning Æthelbert's life.<ref name=DNB>Rollason "Ælberht" ''Oxford Dictionary of National Biography''</ref> He was a kinsman of his predecessor [[Ecgbert, Archbishop of York|Ecgbert]], who was brother to [[Eadberht of Northumbria|Eadberht]], [[List of monarchs of Northumbria|King of Northumbria]].<ref name=Duckett19/> Æthelbert's family placed him in a monastery as a young child,<ref name=DNB/> where he was a pupil in the school founded at [[York]] by Ecgbert. Ecgbert ordained Æthelbert as a priest put him in charge of the school.<ref name=Duckett19>Duckett ''Alcuin'' pp. 19–22</ref>

Æthelbert was instrumental in forming a library at York,<ref name=Hindley153>Hindley ''Brief History of the Anglo-Saxons'' p. 152</ref> which was probably the largest contemporary collection of books to be found in Europe outside of Rome. Alcuin mentions several Latin and Greek classical authors, as well as the Fathers and other Christian writers that formed the 8th century [[Western canon|canon]]. Æthelbert, in his search for books, travelled far, and we know that he visited Rome among other places.<ref name=Duckett19/> Alcuin's poem ''Versus'' lists 41 different authors, including some who wrote in Hebrew.<ref name=Hill10>Hill and Brooke "From 627 until the Early Thirteenth Century" ''History of York Minster'' p. 10</ref> He taught both the ''[[Trivium (education)|trivium]]'' as well as the ''[[quadrivium]]'', plus how to figure the dates of church festivals and natural science.<ref name=DNB/>

==Archbishop==

In 766 Æthelbert succeeded Ecgbert as archbishop; he was consecrated 24 April 767,<ref name=Handbook224>Fryde, et al. ''Handbook of British Chronology'' p. 224</ref> the feast day of his predecessor [[Wilfrid]]. This may have been deliberate and a sign that Æthelbert wished to revive Wilfrid's ambitions for the archiepiscopal see. Æthelbert received his [[pallium]] from Pope [[Pope Adrian I|Adrian I]] in 773.<ref name=DNB/> Alcuin was appointed head of the cathedral school after Æthelbert became archbishop.<ref name=Levinson153/> Much of Alcuin's description of Æthelbert's time as archbishop has the flavour of a panegyric, as Alcuin praised Æthelbert as a model bishop suitable for other bishops to use as a role model.<ref name=DNB/>

Æthelbert rebuilt [[York Minster]], which had been destroyed by fire in 741, giving [[Eanbald I|Eanbald]] and Alcuin the job of overseeing the construction.<ref name=Duckett27>Duckett ''Alcuin'' p. 27</ref> Alcuin wrote that it had bright windows and ceilings and that the liturgical vessels and altars were decorated with precious metals and gems.<ref name=DNB/> The new building also had 30 altars as well as upper apartments.<ref name=Making202>Kirby ''Making of Early England'' p. 202</ref> Æthelbert dedicated one of the altars to Saint Paul and it was located on the location where [[Edwin]], the first Christian king of Northumbria, was baptized.<ref name=DNB/>

Æthelbert sent out missionaries to the pagans of Northern Europe, among them [[Alubert]] and [[Ludger|Liudger]], who went to northern Germany.<ref name=Duckett31>Duckett ''Alcuin'' p. 31</ref> Liudger had earlier been a pupil at the school in York, and went on to become the first [[Bishop of Munster]].<ref name=Stenton189>Stenton ''Anglo-Saxon England'' p. 189</ref> Æthelbert was the recipient of letters from one of the missionaries - [[Lullus|Lull]], the [[Archbishop of Mainz]],<ref name=Stenton174>Stenton ''Anglo-Saxon England'' p. 174</ref> assuming that Lull's correspondent "Coena", who is an archbishop and who was being asked for the works of Bede, is actually Æthelbert,<ref name=DNB/> as most historians seem convinced of.<ref name=DNB/><ref name=Stenton174/>{{efn|If this is correct, "Coena" was likely a nickname of Æthelbert's.<ref name=DNB/>}} Books were sent to the missionaries from the York library.<ref name=Barr488>Barr "Minster Library" ''History of York Minster'' pp. 488–489</ref>{{efn|The library was likely destroyed in 866 and 867 when the Danes burnt the city of York and the cathedral.<ref name=Barr488/>}}

In 774, Æthelbert called a council which deposed [[Alhred of Northumbria|Alhred]] the [[King of Northumbria]] and sent the ex-king north into exile with the [[Picts]]. The cause of the deposition may have been related to missionary work.<ref name=Ashley291>Ashely ''Mammoth Book of British Kings & Queens'' pp. 291–292</ref> The historian D. P. Kirby feels that Æthelbert was not a supporter of Alhred prior to his deposition. Alhred was replaced with [[Æthelred I of Northumbria|Æthelred]], who was replaced in 778 by [[Ælfwald I of Northumbria|Ælfwald]], the son of Oswulf. Kirby sees Æthelbert's withdrawal of support as instrumental in the deposition of Æthelred, noting that Ælfwald was closely related to Æthelbert, unlike both Alhred and Æthelred.<ref name=Kirby127>Kirby ''Earliest English Kings'' pp. 127–129</ref> Kirby also notes that medieval chroniclers noted that Æthelbert is said to have not "spared evil kings".<ref name=Kirby128>Kirby ''Earliest English Kings'' p. 128</ref>

==Retirement and death==

Æthelbert retired some time before his death, consecrating Eanbald as his successor.<ref name=Duckett32>Duckett ''Alcuin'' p. 32</ref> The exact date this occurred is unclear. Alcuin gives a date corresponding to July 778, but it could be 777 too. Eanbald's position may have just been as an associate bishop, with Æthelbert remaining in office until his death while sharing the office with Eanbald.<ref name=DNB/> During his retirement, he had constructed a new church dedicated to Alma Sophia.<ref name=Gee113>Gee "Architectural History until 1290" ''History of York Minster'' p. 113</ref>{{efn|The remains of a burial ground, found under the south transept of the present-day [[York Minster]] may be connected with this church.<ref name=Gee113/>}} He lived long enough to consecrate the new church, ten days before his death on 8 November.<ref name=Handbook224/><ref name=Duckett34>Duckett ''Alcuin'' p. 34</ref>

==Notes==
{{notelist|60em}}

==Citations==
{{reflist|40em}}

==References==
{{refbegin|60em}}
* {{cite book |author=Ashley, Mike |authorlink= Mike Ashley (writer) |title=The Mammoth Book of British Kings & Queens |year=1998 |publisher=Carroll & Graf |location=New York |isbn=0-7867-0692-9 }}
* {{cite encyclopedia |author=Barr, C. B. L.| title=The Minster Library |encyclopedia = A History of York Minster |editor1=Aylmer, G. E. |editor2=Cant, Reginald |publisher=Clarendon Press |location=Oxford, UK |isbn=0-19-817199-4 |pages=487–539}}
* {{cite book |author= Duckett, Eleanor Shipley |title= Alcuin, Friend of Charlemagne: His World and His Work  |year=1951 |publisher= MacMillan |location= New York|oclc= 1010576  }}
* {{cite book |author1=Fryde, E. B. |author2=Greenway, D. E. |author3=Porter, S. |author4=Roy, I. |title=Handbook of British Chronology|edition=Third revised |publisher=Cambridge University Press |location=Cambridge, UK |year=1996 |isbn=0-521-56350-X }}
* {{cite encyclopedia |author=Gee, Eric A.| title=Architectural History until 1290 |encyclopedia = A History of York Minster |editor1=Aylmer, G. E. |editor2=Cant, Reginald |publisher=Clarendon Press |location=Oxford, UK |isbn=0-19-817199-4 |pages=110–148}}
* {{cite encyclopedia |author1=Hill, Rosalind M. T.|author2=Brooke, Christopher N. L. |author2link=Christopher N. L. Brooke| title=From 627 until the Early Thirteenth Century |encyclopedia = A History of York Minster |editor=Aylmer, G. E. |editor2=Cant, Reginald |publisher=Clarendon Press |location=Oxford, UK |isbn=0-19-817199-4 |pages=1–43}}
* {{cite book |author= Hindley, Geoffrey |title=A Brief History of the Anglo-Saxons: The Beginnings of the English Nation |year= 2006|publisher= Carroll & Graf Publishers |location=New York |isbn=978-0-7867-1738-5  }}
* {{cite book |author=Kirby, D. P.  |title=The Earliest English Kings |publisher=Routledge |location=New York |year=2000 |isbn=0-415-24211-8 }}
* {{cite book |author=Kirby, D. P.   |title= The Making of Early England |year=1967 |publisher=Schocken Books |location=New York|edition=Reprint |oclc= 399516}}
* {{cite book |author=Levison, Wilhelm |authorlink= Wilhelm Levison |title=England and the Continent in the Eighth Century |publisher=Clarendon Press |location=Oxford, UK |year=1973 |isbn=0-19-821232-1 |origyear=1946 |edition= Reprint}}
* {{cite encyclopedia |author=Rollason, David |authorlink=David Rollason |title=Ælberht (d. 779/80) |encyclopedia=Oxford Dictionary of National Biography |publisher= Oxford University Press |year=2004 |url=http://www.oxforddnb.com/view/article/49425 |accessdate=9 November 2007 | doi= 10.1093/ref:odnb/49425}}{{ODNBsub}}
* {{cite book |author=Stenton, F. M. |authorlink= Frank Stenton |title= Anglo-Saxon England  |year= 1971|publisher= Oxford University Press |location=Oxford |edition=Third |isbn=978-0-19-280139-5  }}
{{refend}}

==External links==
* {{PASE|10874|Æthelberht 8}}

{{good article}}
{{s-start}}
{{s-rel| [[Christianity|Christian]] titles}}
{{s-bef | before=[[Ecgbert (archbishop of York)|Ecgbert]] }}
{{s-ttl| title=[[Archbishop of York]] | years=766–780}}
{{s-aft| after=[[Eanbald I]] }}
{{s-end}}

{{Archbishops of York}}

{{Authority control}}
{{Use British English|date=June 2013}}

{{DEFAULTSORT:Aethelbert Of York}}
[[Category:780 deaths]]
[[Category:Archbishops of York]]
[[Category:8th-century archbishops]]
[[Category:Year of birth unknown]]