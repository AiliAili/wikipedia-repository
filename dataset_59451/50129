{{Use dmy dates|date=November 2016}}
{{distinguish|Abzakhs}}
{{Infobox ethnic group
| group            = Abkhaz, Abkhazians
| native_name      = Аҧсуа
| native_name_lang = Abkhaz
| image            =  <!-- filename -->
| image_caption    = 
| total            = {{circa|200,000}}
| total_year       =  <!-- year of total population -->
| total_source     =
| total_ref        =  <!-- references supporting total population -->
| region1          = '''Former Soviet Union'''
| pop1             = &#32;
| ref1             = &#32;
| region2          = {{flag|Abkhazia}}
| pop2             = 122,040 (2003 census)
| ref2             = <ref name=census>(2003) [http://www.ethno-kavkaz.narod.ru/rnabkhazia.html 2003 Census statistics] {{ru icon}}</ref>
| region3          = {{flag|Russia}}
| pop3             = 11,366 (2002 census)
| ref3             = <ref>[http://www.perepis2002.ru/ct/doc/TOM_04_01.xls 2002 Census statistics] {{ru icon}}</ref>
| region4          = {{flag|Georgia}} <small>(without Abkhazia)</small>
| pop4             = 3,527
| ref4             = 
| region5          = {{flag|Ukraine}}
| pop5             = 1,458
| ref5             = <ref>[http://www.ukrcensus.gov.ua/eng/results/nationality_population/nationality_1/s5/?box=5.1W&out_type=&id=&rz=1_1&rz_b=2_1&k_t=00&id=&botton=cens_db]</ref>
| region9          = '''Diaspora'''
| pop9             = &#32;
| ref9             = &#32;
| region10         = {{flag|Turkey}}
| pop10            = 15–100,000 {{small|(by descent)}}
| ref10            = <ref>see [[#Diaspora|diaspora section]]</ref>
| region11         = {{flag|Syria}}
| pop11            = 5–10,000
| ref11            = <ref name="VoR05052012">{{cite web|url=http://english.ruvr.ru/2012_05_05/73887817/|title=Abkhaz Syrians return home|date=5 May 2012|accessdate=9 May 2012|work=[[Voice of Russia]]}}</ref>
| region12         = {{flag|Germany}}
| pop12            = 5,000
| ref12            = {{cn|date=October 2016}}
| languages        = [[Abkhaz language|Abkhaz]] (native), [[Russian language|Russian]], [[Georgian language|Georgian]]
| religions        = Predominantly [[Abkhazian Orthodox Church|Abkhazian Orthodox Christianity]] and [[Sunni Islam]]
| related_groups   = Other [[Northwest Caucasian languages|Northwest Caucasians]]
| footnotes        = 
}}
'''Abkhazians''' or the '''Abkhaz''' ([[Abkhaz language|Abkhaz]]: Аҧсуа, ''Apswa''; {{lang-ka|აფხაზები}} {{IPA-ka|ɑpʰxɑzɛbi|}}) are a [[Caucasus|Caucasian]] [[ethnic group]], mainly living in [[Abkhazia]], a disputed region on the [[Black Sea]] coast. A large Abkhaz [[diaspora]] population resides in Turkey, the origins of which lie in the emigration from the Caucasus in the late 19th century known as [[Muhajir (Caucasus)|muhajirism]]. Many Abkhaz also live in other parts of the former [[Soviet Union]], particularly in Russia and Ukraine.<ref>{{cite web|url=http://www.silk.european-heritage.net/mont_illustr/e_mont_il_01.html|title=Caucasus background|publisher=silk.european-heritage.net|deadurl=yes|archiveurl=https://web.archive.org/web/20060720150021/http://www.silk.european-heritage.net:80/mont_illustr/e_mont_il_01.html|archivedate=20 July 2006|df=dmy}}</ref>

==Ethnology==
The [[Abkhaz language]] belongs to the isolate [[Northwest Caucasian languages|Northwest Caucasian language family]], also known as Abkhaz–Adyghe or North Pontic family, which groups the dialectic continuum spoken by the [[Abazins|Abaza]]–Abkhaz (Abazgi) and [[Circassians|Adyghe]] ("Circassians" in English).<ref name="Pereltsvaig2012">{{cite book|author=Asya Pereltsvaig|title=Languages of the World: An Introduction|url=https://books.google.com/books?id=8q06xer0vHkC&pg=PA66|date=9 February 2012|publisher=Cambridge University Press|isbn=978-1-107-00278-4|pages=66–}}</ref> The Abkhaz is closely ethnically related to Circassian.<ref name="Gammer2004">{{cite book|author=Moshe Gammer|title=The Caspian Region, Volume 2: The Caucasus|url=https://books.google.com/books?id=MJWRAgAAQBAJ&pg=PT79|date=25 June 2004|publisher=Routledge|isbn=978-1-135-77540-7|pages=79–}}</ref> Classical sources speak of several tribes dwelling in the region, but their exact identity and location remain controversial due to Abkhaz–Georgian historiographical conflict (see the [[#history|history section]]).

===Subgroups===
There are also three subgroups of the Abkhaz people. The Bzyb (Бзыҧ, Bzyph) reside in the [[Bzyb River]] region, and speak their [[Bzyb dialect|own dialect]].<ref name=EB>{{cite encyclopedia |editor-first=Dale H. |editor-last=Hoiberg|encyclopedia=Encyclopedia Britannica |title=Abkhaz|edition = 15th |year=2010|publisher=Encyclopedia Britannica Inc. |volume=I: A-ak Bayes |location=Chicago, IL |isbn=978-1-59339-837-8|pages=33}}</ref> The Abzhui (Абжьыуа, Abzhwa) live in the [[Kodori River]] region, and also speak their own dialect, which the Abkhaz literary language is based upon.<ref name=EB/> Finally, there is the Zamurzakan who reside in the southeast of Abkhazia.<ref name=EB/>

==History==
 {{see also|History of Abkhazia}}
Some scholars deem the ancient [[Heniochi]] tribe the progenitors of the Abkhaz.{{sfn|Olson|1994|p=6}} This warlike people came into contact with Ancient Greeks through the colonies of Dioskourias and Pitiuntas.{{sfn|Olson|1994|p=6}} In the Roman period, the [[Abasgoi]] are mentioned as inhabiting the region.{{sfn|Olson|1994|p=6}} These Abasgoi (Abkhaz) were described by [[Procopius]] as warlike, worshippers of three deities, under the suzerainty of the [[Kingdom of Lazica]].{{sfn|Olson|1994|p=6}} The Abkhazian view is that the [[Apsilae]] and Abasgoi are ancestors of the [[Northwest Caucasian languages|Abkhaz–Adyghe group of peoples]], while the Georgian view is that those were [[Kartvelian languages|Kartvelians]] ([[Georgians]]).{{sfn|Smith|1998|p=55}}

When the [[Achba]] dynasty established the [[Kingdom of Abkhazia]] in the 780s and freed themselves from the Byzantine hegemony, Abkhazia became a part of the Georgian cultural world. The local nobility, clergy and educated class used Georgian as a language of literacy and culture. Georgian would remain the second language for many Abkhaz until Russian replaced it in the early 20th century. From the early 11th to the 15th century, Abkhazia was a part of the all-[[Kingdom of Georgia|Georgian monarchy]], but then became a separate [[Principality of Abkhazia]] only to be conquered by the Ottomans.

Towards the end of the 17th century, the region became a theatre of widespread slave trade and piracy.{{citation needed|date=January 2012}} According to a controversial theory developed by [[Pavle Ingorokva]] in the 1950s and some other Georgian scholars, at that time a number of the [[North Caucasus|Northwest Caucasian]] pagan [[Abaza people|Abaza]] tribes migrated from the north and blended with the local ethnic elements, significantly changing the region's demographic situation. These views were described as ethnocentric and having little historical support.<ref>{{cite book |last=Smith |first=Graham |year=1998 |title=Nation-building in the post-Soviet borderlands: the politics of national identities |publisher=Cambridge University Press |page= 55 |isbn=978-0-521-59968-9}}</ref><ref>[http://www.amazon.com/Politics-Ethnic-Separatism-Russia-Georgia/dp/0230613594/ref=sr_1_fkmr1_3?s=books&ie=UTF8&qid=1325874802&sr=1-3-fkmr1 ]</ref> They served as intellectual support to the Stalin-era assimilation policy<ref>{{cite book |title=The Caucasus: an introduction |last=de Waal |first=Thomas |authorlink=Thomas de Waal |year=2010 |publisher=Oxford University Press |page=151 |isbn=978-0-19-539976-9}}</ref> and had a profound influence on the Georgian nationalism in the 1980s.<ref>{{cite book |last=Coppieters |first=Bruno |date=2004 |title=Europeanization and conflict resolution: case studies from the European periphery |publisher=America Press |page=196 |isbn=978-90-382-0648-6}}</ref>

The Russian conquest of Abkhazia from the 1810s to the 1860s was accompanied by a massive expulsion of Muslim Abkhaz to the [[Ottoman Empire]] and the introduction of a strong [[Russification]] policy. As a result, the Abkhaz diaspora is currently estimated to measure at least twice the number of Abkhaz that reside in Abkhazia. The largest part of the diaspora now lives in Turkey, with estimates ranging from 100,000 to 500,000, with smaller groups in Syria (5,000 – 10,000) and Jordan. In recent years, some of these have emigrated to the West, principally to Germany (5,000), the Netherlands, Switzerland, Belgium, France, the United Kingdom, Austria and the United States (mainly to [[New Jersey]]).<ref name=chirikba08p6-8>Chirikba 2003 p6-8</ref>

After the [[Russian Revolution of 1917]], Abkhazia was a part of the [[Democratic Republic of Georgia]], but was [[Red Army invasion of Georgia|conquered by the Red Army]] in 1921 and eventually entered the [[Soviet Union]] as a [[Socialist Soviet Republic of Abkhazia|Soviet Socialist Republic]] associated with the [[Georgian SSR]]. The status of Abkhazia was downgraded in 1931 when it became an [[Abkhaz Autonomous Soviet Socialist Republic|Autonomous SSR]] within the Georgian SSR. Under [[Joseph Stalin]], a forcible [[collectivization]] was introduced and the native communist elite purged. The influx of Armenians, Russians and Georgians into the growing agricultural and tourism sectors was also encouraged, and Abkhaz schools were briefly closed. By 1989, the number of Abkhaz was about 93,000 (18% of the population of the autonomous republic), while the Georgian population numbered 240,000 (45%). The number of Armenians (15% of the entire population) and Russians (14%) grew substantially as well.

[[File:Abkhazia02.png|thumb|Modern Abkhazia|250px]]
The [[War in Abkhazia (1992-1993)|1992–1993 War in Abkhazia]] left the Abkhaz an ethnic plurality of ca. 45%, with Russians, Armenians, Georgians, Greeks, and Jews comprising most of the remainder of the population of Abkhazia. The 2003 census established the total number of Abkhaz in Abkhazia at 94,606.<ref name=census/> However, the exact demographic figures for the region are disputed and alternative figures are available.<ref>[http://poli.vub.ac.be/publi/Georgians/notes.html Georgians and Abkhazians. The Search for a Peace Settlement]
(Notes and References section), by ''various authors'', Vrije Universiteit Brussel, August 1998.</ref> The de facto Abkhaz president [[Sergey Bagapsh]] suggested, in 2005, that less than 70,000 ethnic Abkhaz lived in Abkhazia.<ref>[http://www.civil.ge/eng/article.php?id=10923 Bagapsh Speaks of Abkhazia’s Economy, Demographic Situation]. [[Civil Georgia]]. 10 October 2005</ref>

At the time of the 2011 Census, 122,175 Abkhaz were living in Abkhazia. They were 50.8% of the total population of the republic.<ref>[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html 2011 Census results]</ref>
 
In the course of the [[Syrian uprising (2011–present)|Syrian uprising]], a number of Abkhaz living in Syria remigrated to Abkhazia.<ref name="VoR05052012">{{cite web|url=http://english.ruvr.ru/2012_05_05/73887817/|title=Abkhaz Syrians return home|date=5 May 2012|accessdate=9 May 2012|work=[[Voice of Russia]]}}</ref> By mid-April 2013, approximately 200 Syrians of Abkhaz descent had arrived in Abkhazia.<ref name="VoA15042013">{{cite web|url=http://www.voanews.com/content/syrian_refugees_go-home_to_former_russian_riviera/1642027.html|title=Syrian Refugees Go 'Home' to Former Russian Riviera|date=15 April 2013|author=James Brooke|work=[[Voice of America]]|accessdate=22 April 2013}}</ref><ref name="AW02042013">{{cite web|url=http://www.abkhazworld.com/news/diaspora/967-over-two-hundred-representatives-of-the-abkhazian-diaspora.html|title=Over two hundred representatives of the Abkhazian diaspora in Syria want to return to their historical homeland|date=2 April 2013|accessdate=22 April 2013|work=Abkhaz World}}</ref> A further 150 were due to arrive by the end of April.<ref name="VoA15042013" /> The Abkhazian leadership has stated that it would continue the repatriation of Abkhaz living abroad.<ref name="AW02042013" /> As of August 2013, 531 Abkhaz had arrived from Syria according to the Abkhazian government.<ref name="TM07082013">{{cite web|url=http://www.messenger.com.ge/issues/2920_august_7_2013/2920_econ_two.html|title="Repatriates" settling in Abkhazia|date=7 August 2013|accessdate=31 August 2013|work=The Messenger}}</ref>

==Economy==
The typical economy is strong on the breeding of cattle, [[beekeeping]], [[viticulture]], and agriculture.<ref name=EB/>

==Religion==
{{see also|Religion in Abkhazia}}
The Abkhaz people are principally divided into [[Abkhazian Orthodox Church|Abkhazian Orthodox Christian]] and [[Sunni Muslim]] (Hanafi) communities<ref name=EB/> (prevalent in Abkhazia and Turkey respectively) but the indigenous non-Abrahamic beliefs have always been strong.<ref>Johansons, Andrejs. (Feb. 1972) The Shamaness of the Abkhazians. ''History of Religions.'' Vol. 11, No. 3. pp. 251–256.</ref> Although Christianity made its first appearance in the realm of their Circassian neighbours in the first century CE via the travels and preaching of the [[Andrew the Apostle|Apostle Andrew]],<ref>{{cite book |last=Taylor |first=Jeremy |date=1613–1667 |title= Antiquitates christianæ, or, The history of the life and death of the holy Jesus as also the lives acts and martyrdoms of his Apostles: in two parts |page=101}}</ref> and became the dominant religion of Circassians in the 3rd to 4th centuries, Christianity became the dominant religion of Abkhazians in the 6th century during the reign of [[List of Byzantine Emperors|Byzantine emperor]] [[Justinian I]], and continued to be followed under the kings of Georgia in the [[High Middle Ages]]. The Ottomans introduced Islam in the 16th century and the region became largely Muslim gradually until the 1860s. When [[Ethnic cleansing of Circassians|much of the Muslim population was ethnically cleansed]] in the late 19th century, Christians once again became the majority in the region.

==Diaspora==
Many Muslim Cherkess, Abkhaz and Chechens migrated to the Ottoman Empire following revolts against Russian rule.<ref name="German2016">{{cite book|author=Tracey German|title=Regional Cooperation in the South Caucasus: Good Neighbours Or Distant Relatives?|url=https://books.google.com/books?id=7vTsCwAAQBAJ&pg=PA110|date=8 April 2016|publisher=Routledge|isbn=978-1-317-06913-3|pages=110–}}</ref> It is believed that the Abkhaz community in Turkey is larger than that of Abkhazia itself.<ref name="German2016"/> Some 250 Abkhaz-Abaza villages are estimated throughout Turkey.<ref name="German2016"/> According to [[Andrew Dalby]], Abkhazian-speakers might number more than 100,000 in Turkey,{{dubious-inline|date=October 2016}}<ref>{{cite book|author=Andrew Dalby|title=Dictionary of Languages: The definitive reference to more than 400 languages|url=https://books.google.com/books?id=7dHNCgAAQBAJ&pg=PA1|date=28 October 2015|publisher=Bloomsbury Publishing|isbn=978-1-4081-0214-5|pages=1–}}</ref> however, the 1963 census only recorded 4,700 native speakers and 8,000 secondary speakers.{{sfn|Gachechiladze|2014|p=81}} Of the 15,000 ethnic Abkhaz in Turkey, only 4,000 speak the language, the rest having assimilated into Turkish.<ref name="Danver2015">{{cite book|author=Steven L. Danver|title=Native Peoples of the World: An Encylopedia of Groups, Cultures and Contemporary Issues|url=https://books.google.com/books?id=vf4TBwAAQBAJ&pg=PA259|date=10 March 2015|publisher=Routledge|isbn=978-1-317-46400-6|pages=259–}}</ref>{{better source|date=October 2016}}

==Gallery==
<gallery>
File:Apsua_Childs_waving_Apsny_Flag.jpg|Children waving miniature Abkhazian flags
File:Abkhazia, Georgia — Bichvinta Cathedral.jpg|[[Pitsunda Cathedral]], seat of [[Abkhazian Orthodox Church]]
File:Apsua_Holding_Apsny_Flag.jpg|Parade exhibiting Abkhazian flags
File:Abkhaz and Georgian generals (A).jpg|Abkhaz and Georgian generals
File:Sobranie cherkesskikh knyazey.PNG|Conference of Abkhazian nobility in 1839
File:Abkhaz-deputatklk.jpg|Abkhaz in the mid-19th century
File:Apsua Ladies in Flag Clothes.jpg|Woman wearing dress modelled after the Abkhazian flag
</gallery>

==See also==
* [[Afro-Abkhazians]]
* [[List of Abkhazians]]
* [[Women in Abkhazia]]

==References==
{{Reflist|2}}

==Sources==
{{refbegin}}
*{{cite book|author=George Hewitt|title=The Abkhazians: A Handbook|url=https://books.google.com/books?id=-YUfAgAAQBAJ&pg=PA37|date=19 November 2013|publisher=Routledge|isbn=978-1-136-80205-8|pages=37–}}
*{{cite book|author1=Gachechiladze, Revaz|title=The New Georgia: Space, Society, Politics|url=https://books.google.com/books?id=hGVuBwAAQBAJ&pg=PA81|date=17 January 2014|publisher=Routledge|isbn=978-1-317-76256-0|pages=81–}}
*{{cite book |last=Chirikba |first=Viacheslav |title=Abkhaz |publisher=LINCOM EUROPA |location=[[Munich]] |year=2003 |series=Languages of the World/Materials |volume=119 |isbn=3-89586-136-7}}
*{{cite encyclopedia | article = ABḴĀZ | last =  Giunashvili | first = Dzh. | authorlink = | url = http://www.iranicaonline.org/articles/abkaz | editor-last = | editor-first =  | editor-link = | encyclopedia  = Encyclopaedia Iranica, Vol. I, Fasc. 2 | pages = 222-224 | location = | publisher = | year = 1982 | isbn = |ref=harv}}
*{{cite book|last=Olson|first=James Stuart|title=An Ethnohistorical Dictionary of the Russian and Soviet Empires|url=https://books.google.com/books?id=CquTz6ps5YgC|year=1994|publisher=Greenwood Publishing Group|isbn=978-0-313-27497-8|ref=harv}}
*{{cite book|last=Smith|first=Graham|title=Nation-building in the Post-Soviet Borderlands: The Politics of National Identities|url=https://books.google.com/books?id=BtzEeq9QcRMC&pg=PA55|year=1998|publisher=Cambridge University Press|isbn=978-0-521-59968-9|pages=55–|ref=harv}}
* David Marshall Lang, ''Caucasian Studies'', University of London, 1964, Vol.1
* Roger Rosen, ''Georgia: Sovereign Country of the Caucasus'', Odyssey, 2004, ISBN 962-217-748-4
{{refend}}

{{Circassian diaspora}}
{{Peoples of the Caucasus}}
{{Ethnic groups in Georgia}}
{{Eastern Orthodox and Oriental Orthodox Christians}}
{{European Muslims}}

{{Authority control}}

[[Category:Abkhaz people|Abkhaz people]]
[[Category:Indigenous peoples of Europe]]
[[Category:Peoples of the Caucasus]]
{{Use dmy dates|date=May 2014}}