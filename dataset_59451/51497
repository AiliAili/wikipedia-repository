{{Infobox journal
| title         =Hypertension 
| cover         = [[File:Hypertension (journal).gif]]
| editor        =[[Anna Dominiczak]]
| discipline    =Hypertension 
| peer-reviewed = 
| formernames   = 
| abbreviation  = 
| publisher     =American Heart Association 
| country       = 
| frequency     =Monthly 
| history       =1979 - present 
| openaccess    = 
| license       = 
| impact        =7.632
| impact-year   =2013 
| website       =http://hyper.ahajournals.org/ 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           =http://www.ahajournals.org/rss/ 
| atom          = 
| JSTOR         = 
| OCLC          =4798824 
| LCCN          =80645000 
| CODEN         =HPRTDN 
| ISSN          =0194-911X 
| eISSN         =1524-4563 
| boxwidth      = 
}}

'''''Hypertension''''' is a monthly, [[peer reviewed]], [[scientific journal]], established with the 1979 (Jan. & Feb.) issue (vol. 1), and published by the [[American Heart Association]] (AHA). The [[Editor in Chief]] is Anna F. Dominiczak.<ref>{{cite web|title=General Statistics|url=http://hyper.ahajournals.org/site/misc/stats.xhtml|work=Hypertension|publisher=American Heart Association|accessdate=March 2014}}</ref>

Publishing formats include original manuscripts (6000 words), invited review summaries (6000 words), invited case-based reviews (6000 words), recent study highlights (3000 words), invited brief commentaries (1500 words), scientific or technical tutorials (6000 words), letter to the editor (500 words), and novel findings of unusual interest (1000 words).<ref name=author/>

==Scope==
The focus of this journal are different aspects of studying [[hypertension]], which includes [[pathophysiology]], [[blood pressure regulation]], [[clinical treatment]], and prevention. Original contributions span topics such as [[basic research]], [[clinical studies]], and [[population studies]] of hypertension. These studies include related fields such as nephrology, endocrinology, neuroscience, vascular biology, physiology, pharmacology, cellular and molecular biology, and genetics.<ref name=author>
{{Cite web
  | title =Instructions to Authors 
  | work =Overview, Editor in Chief 
  | publisher =American Heart Association 
  | date =August 2010 
  | url =http://hyper.ahajournals.org/misc/ifora.shtml 
  | accessdate =2010-08-29}}</ref>

The journal ranks #1 among all hypertension journals and #3 among 67 journals in the Peripheral Vascular Disease category (2012 Journal Citation Reports®, Thomson Reuters, 2013).

==Abstracting and indexing==
Hypertension is indexed in the following databases:<ref name=mstrjnlhyp>
{{Cite web
  | title =Master Journal List search
  | work =Bibliographic information for this journal 
  | publisher =Thomson Reuters 
  | date =August 2010 
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&Word=Hypertension
  | accessdate =2010-08-29}}</ref><ref name=LCC>
{{Cite web
 |title=Catalog Record (online) 
 |work=Bibliographic information for this journal 
 |publisher=Library of Congress 
 |date=August 2010 
 |url=http://lccn.loc.gov/80645000 
 |accessdate=2010-08-29 
}}{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref name=cassi>{{Cite web
  | title =CAS Source Index (CASSI) 
  | work =Bibliographic information for this journal 
  | publisher =American Chemical Society 
  | date =August 2010 
  | url =http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXffo3v_Z4FJMhTugx6LjttiTLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfimWeKaEpuKwLXvcitRsQMA
  | accessdate =2010-08-29}}</ref>

*[[BIOSIS Previews]]
*[[Chemical Abstracts]] 0009-2258
*[[Chemical Abstracts Service]] - [[CASSI]]
*[[Current Contents]] - Clinical Medicine 
*Current Contents - Life Sciences
*[[Science Citation Index]] 
*[[SciSearch]]

==References==
{{Reflist}}

==External links==
*[http://www.heart.org/HEARTORG/ Home page]. American Heart Association. August 2010.
*[http://www.americanheart.org/presenter.jhtml?identifier=3004556 Scientific Statements] regarding Hypertension. American Heart Association. (2001–2009).

[[Category:Publications established in 1979]]
[[Category:Hypertension journals]]
[[Category:Monthly journals]]
[[Category:American Heart Association academic journals]]