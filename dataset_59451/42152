<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= AW119 Koala
 |image= NYPD helicopter N319PD.jpg
 |caption= 
}}{{Infobox Aircraft Type
 |type= Utility helicopter
 |manufacturer= [[Leonardo S.p.A.|Leonardo]], before Finmeccanica, [[AgustaWestland]], [[Agusta]]
 |designer=
 |first flight= February [[1995 in aviation|1995]]
 |introduced= 2000
 |retired=
 |status= Active in production
 |primary user= [[Mexico State|Mexico State Government]] 
 |more users=  [[Finnish Border Guard]]
 |produced= 2000–present
 |number built= 200+ (2015)<ref>[http://www.defense-aerospace.com/article-view/release/161566/life-flight-orders-eleven-aw119-koalas.html "Life Flight Network to Expand Fleet with Eleven Additional AgustaWestland Helicopters."] ''AgustaWestland'', 4 March 2015.</ref>
 |unit cost=USD$1.85 million in 2000<ref name="flug-revue.rotor.com">{{cite web |title = Flug Revue |url = http://www.flug-revue.rotor.com/FRtypen/FRA119Ko.htm  |accessdate = 20 May 2007 |archiveurl= https://web.archive.org/web/20070615030031/http://www.flug-revue.rotor.com/FRTypen/FRA119Ko.htm |archivedate= 15 June 2007 |deadurl= no}}</ref>
 |developed from = [[Agusta A109]]
 |variants with their own articles=  
}}
|}

The '''AgustaWestland AW119 Koala''', produced by [[Leonardo S.p.A.|Leonardo]] since 2016, is an eight-seat utility [[helicopter]] powered by a single [[turboshaft]] engine produced for the civil market. Introduced as the '''Agusta A119 Koala''' prior to the Agusta-Westland merger, it is targeted at operators favoring lower running costs of a single-engine aircraft over redundancy of a twin.<ref name="airliners.net">{{cite web |title = The Agusta A-119 Koala |publisher = airliners.net |url = http://www.airliners.net/info/stats.main?id=16| accessdate = 2007-05-20 }}</ref>

==Development==

The ''A119'' designation was first applied to a proposed 11-seat stretched version of the A109 in the 1970s;<ref>{{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=41 }}</ref> however this concept did not emerge and no such rotorcraft actually built. The helicopter that was eventually to enter production as the A119 was conceived in 1994, as Agusta was recovering from a period of financial woes that had nearly put the company out of business.<ref name="Simpson 1998 33">{{cite book |last= Simpson |first= R. W. |title=Airlife's Helicopters and Rotorcraft |year=1998 |publisher=Airlife Publishing |location=Ramsbury |pages=33 }}</ref> In February 1995, the second of two prototypes conducted its first flight.<ref name="flug-revue.rotor.com" /> The first prototype was used for static tests.<ref name="dgualdo.it">{{cite web | last = Gualdoni | first = Damiano | title = Damiano Gualdoni Aviation Enthusiast's Website| url = http://www.dgualdo.it/prod-a119.htm  | accessdate = 2007-05-20 }}</ref> Civil certification was originally anticipated in [[1997 in aviation|1997]], this deadline was missed allegedly due to multiple issues such as personnel problems, the need to concentrate resources on the development of the A109 Power, and further development to increase the aircraft's performance to meet customer expectations.<ref name="flug-revue.rotor.com" /><ref name = "gray 40">Gray 2000, p. 40.</ref>

By way of a solution to the latter concern, the decision was taken to change the A119's powerplant. The prototypes were originally fitted with [[Turboméca Arriel]] 2K1 turboshafts, but the ubiquitous [[Pratt & Whitney Canada PT6]]B was chosen in its place.<ref name="World Aircraft Information Files">{{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing |location=London |pages=File 889 Sheet 32}}</ref> In [[1998 in aviation|1998]], the prototypes were remanufactured with this engine, and assigned new serial numbers.<ref name="dgualdo.it" /> Certification was now expected by the fourth quarter of that year, but this date slipped to July [[1999 in aviation|1999]], and it was eventually December before Italian [[Registro Aeronautico Italiano|RAI]] certification was awarded.<ref name="flug-revue.rotor.com" /> US [[FAA]] certification was awarded in February the following year.<ref name="flug-revue.rotor.com" /> Customer deliveries began soon thereafter,<ref name="flug-revue.rotor.com" /> the first commercial example was delivered to Australian logistics company [[Linfox]] (serial 14007, registration ''VH-FOX'').<ref name="dgualdo.it" />

In April 2007, the AW119Ke (''Ke'' standing for ''Koala Enhanced'') was formally unveiled at Heli-Expo; changes included modified [[rotor blade]] design and a higher rotor rpm, increasing both payload and hot-and-high performance, cabin flexibility was also improved.<ref>[http://www.aviationtoday.com/rw/personal-corporate/personal-ac/Rotorcraft-Report-AgustaWestland-Unveils-Enhanced-Version-of-AW119_9829.html#.VsD0am_cuUk "Rotorcraft Report: AgustaWestland Unveils Enhanced Version of AW119."] ''Rotor&Wing'', 1 April 2007.</ref> The fuselages of the AW119 are manufactured by [[PZL Swidnik]] of Poland, later a subsidiary of AgustaWestland.<ref>[https://www.flightglobal.com/news/articles/business-good-week-bad-week-323772/ "Business: Good Week - Bad Week."] ''Flight International'', 16 March 2009.</ref> Final assembly and other manufacturing activity initially took place at [[Vergiate]], [[Italy]]; by the time the improved AW119Ke variant began production, the final assembly line had been transferred from Vergiate to AgustaWestland's facility in [[Philadelphia]], [[United States of America|United States]].<ref>O'Keeffe, Niall. [https://www.flightglobal.com/news/articles/agustawestland-looks-beyond-national-boundaries-318236/ "AgustaWestland looks beyond national boundaries."] ''Flight International'', 31 October 2008.</ref><ref>[https://books.google.co.uk/books?id=EUspAQAAIAAJ "Aviation News, Volume 69."] ''HPC Pub'', 2007. p. 24.</ref>

==Design==
[[File:Agusta A119 Koala, HeliDuero JP6382390.jpg|thumb|Agusta AW119 Koala, 2008]]
The AW119 is a single-engine multirole helicopter, AgustaWestland promote the type as possessing excellent flight qualities with high levels of controllability, maneuverability and inherent safety.<ref name = "finm 118kx">[http://www.finmeccanica.com/-/aw119kx "AW119Kx: Fast and Flexible."] ''Finmeccanica'', Retrieved: 13 February 2016.</ref> The design of the rotorcraft is derived from Agusta's earlier and highly successful [[Agusta A109|A109]] helicopter, differing primarily by only being equipped with a single engine (as the A109 was originally designed),<ref name="Simpson 1998 33" /> a [[Pratt & Whitney Canada PT6|Pratt & Whitney PT6B-37A]] [[turboshaft]] engine, and using fixed skids in place of the A109's retractable wheeled landing gear arrangement.<ref name = "kx broch">[http://www.finmeccanica.com/documents/63265270/69071306/body_AW119Kx.pdf "AW119Kx."] ''Finmeccanica'', Retrieved: 13 February 2016.</ref> The AW119 shares the same cockpit and cabin of the AW109, along with commonality with various other systems, while costing roughly half of the latter's price tag.<ref name = "gray 40"/> According to [[Flight International]], the AW119 is competitively priced and provides good levels of accessibility, maintainability, comfort, noise levels, and speed.<ref name = "gray 43">Gray 2000, p. 43.</ref>

The AW119 employs a four-bladed fully articulated main rotor; the composite [[rotor blade]]s are designed to produce maximum lift with minimum noise, and feature tip caps to reduce noise and elastomeric bearings with no lubrication requirements. Aluminium [[honeycomb]] structural panels are used throughout the airframe, which absorb both noise and vibration, thus requiring no additional vibration absorption systems to be employed.<ref>Gray 2000, pp. 40-41.</ref> The PT6B-37A powerplant of the AW119, located in the same area as the AW109 is capable of providing high power margins along with generous speeds and endurance.<ref name = "finm 118kx"/><ref name = "gray 41">Gray 2000, p. 41.</ref> According to AgustaWestland, the AW119 retains the system redundancy of dual engine helicopters, such as the [[hydraulics]] and the dual independent stability augmentation systems; the [[gearbox]] has a 30-minute dry run capability.<ref name = "kx broch"/><ref name = "gray 41"/>

The AW119 Koala has been used for various roles, including utility, [[emergency medical services]] (EMS), offshore, (USA only), [[law enforcement]], and executive transport.<ref name = "kx broch"/> A key selling point of the type is its wide-body fuselage, which allows for up to seven passengers to be seated in a three-abreast configuration in the cabin; for the EMS mission, up to two litters along with medical attendants and full emergency medical equipment suite can be accommodated, whereas most similar-sized helicopters can only carry one.<ref name="airliners.net" /> The unobstructed cabin area and separate baggage compartment can be rapidly reconfigured to suit a range of different missions and roles. Several different cabin interiors may be adopted to accommodate different missions and operations, such as executive/VIP, EMS, and utility options; the cockpit can also be isolated from the cabin.<ref name = "finm 118kx"/> The AW119 has been promoted as possessing the largest cabin in its class; the reported cabin volume is approximately 30% greater than other rotorcraft in its class.<ref name="World Aircraft Information Files"/><ref name = "finm 118kx"/>

[[File:AgustaWestland AW119 Koala Ke (ZK-ISR) at the Wagga Wagga Airport heli-pad (3).jpg|thumb|left|AW119 Koala Ke at [[Wagga Wagga Airport]], 2012]]

A wide range of avionics have been integrated upon the AW119, which are typically housed within the rotorcraft's nose.<ref name = "gray 42">Gray 2000, p. 42.</ref> Initial production models featured conventional flight instruments;<ref name = "gray 42"/> the [[Garmin G1000]] [[glass cockpit]] is integrated on the newer AW119Kx variant, which is claimed to improve situational awareness, reduce pilot workload, and increase safety. Primary flight and other key information is displayed to the pilots upon two large 10.4 inch multi-function displays in the cockpit; an independently powered stand-by display is also present in case of system failure. Other avionics used include a 3-axis [[aircraft flight control system]] (AFCS), Synthetic Vision System (SVS), Highway In The Sky (HITS) depiction, [[moving map display]], [[radio altimeter]], [[VHF omnidirectional range|VOR]]/[[instrument landing system|ILS]]/[[Global Positioning System|GPS]]/[[Wide Area Augmentation System|WAAS]] navigation, Aural Warning Generator, and embedded [[Terrain awareness and warning system|Helicopter Terrain Avoidance Warning System]] (HTAWS).<ref name = "finm 118kx"/>

A variety of equipment can be equipped, dependent on operator choice and role; these include an external hoist, dual [[cargo hook (helicopter)|cargo hook]], dual [[flight control]]s, baggage compartment extension, snow skis, windshield wipers, rotor brake, multi-band [[radio]]s, active noise reduction headsets, [[soundproof]]ing, oxygen systems, [[loud speaker]]s, search lights, retractable landing light, emergency floatation equipment, reinforced windshield, [[wire strike protection system]], [[rappel]]ling kit, fire fighting belly tank, and a [[forward looking infrared]] (FLIR) camera.<ref name = "finm 118kx"/> Three fuel tanks, located behind the rear seats in the cabin, are installed as standard; up to two additional optional tanks can be fitted for a total of five, providing a flight endurance of nearly six hours.<ref name = "gray 40"/>

==Operational history==
[[File:Agusta A119 Koala.jpg|thumb|right|Agusta A119 Koala at [[HeliRussia]] 2008]]
{{expand section|date=April 2015}}
Since 2009, there has been reports that final assembly of the AW119 is to be transferred to India as a part of a measure to increase sales within that market.<ref>''Aviation Week & Space Technology'', Volume 170, Issues 1-10. 2009. p. 95.</ref><ref>[http://www.thehindu.com/news/national/agustawestland-firm-on-expanding-footprint-in-india/article5161252.ece "AgustaWestland firm on expanding footprint in India."] ''The Hindu'', 24 September 2013.</ref> In February 2010, it was announced that AgustaWestland and [[Tata Group]] were to form a joint venture to produce the AW119 in India; the first Indian-manufactured units were originally planned to commence deliveries in 2011.<ref>Pustam, Anil R. [http://www.aviationbusiness.com.au/news/rotor-market-strong-on-technology "Rotor market strong on technology."] ''Aviation Business'', 17 June 2010.</ref> In October 2015, following two years of deliberation, India's Foreign Investment Promotion Board approved a proposal to locally assemble the AW119Kx in [[Hyderabad]], [[Telangana]]; the facility is to be operated by Indian Rotorcraft Ltd (IRL), the joint venture between AgustaWestland and Tata.<ref>[http://www.firstpost.com/business/fipb-clears-tata-aw-chopper-facility-proposal-in-hyderabad-2468724.html "FIPB clears Tata-AW chopper facility proposal in Hyderabad."] ''First Post'', 14 October 2015.</ref><ref name = "janes ind appr">Grevatt, Jon. [http://www.janes.com/article/55113/agustawestland-s-indian-joint-venture-finally-approved-by-authorities "AgustaWestland's Indian joint venture finally approved by authorities."] ''IHS Jane's Defence Industry'', 7 October 2015.</ref>

In September 2014, AgustaWestland issued a legal challenge to a [[United States Army|US Army]] decision to procure the [[Eurocopter UH-72 Lakota]] as a trainer without a competition, stating that both the AW119 and the AW109 had lower acquisition and operating costs; the challenge was dismissed in December 2014.<ref>Head, Elan. [http://www.verticalmag.com/news/article/28804 "AgustaWestland sues to block Army purchase of EC145 helicopters."]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} ''Vertical'', 23 September 2014.</ref><ref>[http://www.ainonline.com/aviation-news/defense/2014-12-07/army-moves-ahead-airbus-trainer-plan "Army Moves Ahead with Airbus Trainer Plan."] ''AIN Online'', 7 December 2014.</ref> In early 2015, AgustaWestland and [[Bristow Helicopters]] jointly offered an upgraded variant of the AW119 as a replacement for [[United States Navy|US Navy]]’s existing fleet of 117 Bell TH-57 (based on the [[Bell 206]]) trainer helicopters under a fee-for-service contract; AgustaWestland and have claimed that over a four-year period the AW119 fleet could be introduced at an equal or lesser cost than the operating costs than continuing to operate the aging TH-57s.<ref>Trimble, Stephen. [http://www.flightglobal.com/news/articles/agustawestland-bristow-offer-aw119-to-replace-us-navy-th-57-411261/ "AgustaWestland, Bristow offer AW119 to replace US Navy TH-57 fleet."] ''Flight International'', 15 April 2015.</ref><ref>Clevenger, Andrew. [http://www.defensenews.com/story/defense/show-daily/sea-air-space/2015/04/15/agustawestland-navy-model-helo-training-aw119/25831187/ "Group Offers Navy New Helo Training Model."] ''DefenseNews'', 15 April 2015.</ref>

==Variants==
* '''A119''' - designation for the original production version
* '''AW119''' - designation for the ''A119'' following the merger of [[Agusta]] and [[Westland Helicopters]]
** '''AW119 Ke''' - improved version, featuring redesigned rotors, greater payload, and better fuel efficiency.<ref>{{cite web|title=AgustaWestland AW119Ke sales brochure |url=http://www.agustawestland.com/dindoc/AW119Ke.pdf |format=PDF |accessdate=2007-05-20 |archiveurl=https://web.archive.org/web/20070927025616/http://www.agustawestland.com/dindoc/AW119Ke.pdf |archivedate=2007-09-27 |deadurl=no |df= }}</ref>
** '''AW119 KXe''' - successor to the ''Ke'' model, produced in [[Philadelphia]], USA and [[Hyderabad]], India.<ref name = "janes ind appr"/><ref name=ain2015-03-03>{{cite news |first=Mark |last=Huber |url=http://www.ainonline.com/aviation-news/business-aviation/2015-03-03/agustawestland-build-aw609-philadelphia |title=AgustaWestland to build AW609 in Philadelphia  |work=Aviation International News |date=3 March 2015 |accessdate=5 March 2015 |deadurl=no}}</ref>

==Operators==
[[File:Phoenix Police A119-2.JPG|thumb|right|A119 of the Phoenix Police Department]]
[[File:Life Flight Network AW119Kx N536LF.jpg|thumb|right|Life Flight Network AW119Kx N536LF at [[PeaceHealth Southwest Medical Center]], Vancouver, WA]]
;{{ALG}}
* [[Algerian Air Force]]<ref>http://secret-difa3.blogspot.com/2013/10/un-nouvel-helicoptere-dentrainement.html</ref> 
;{{AUS}}
* [[Westpac Life Saver Rescue Helicopter Service#Aircraft|Westpac Rescue Helicopter Service]]<ref>{{cite web |url=http://surflifesavingwa.com.au/safety-rescue-services |title=Helicopters |work=Safety & rescue services |publisher=Surf Life Saving Western Australia |year= |accessdate=31 December 2013 }}</ref>
;{{BRA}}
* [[Goiás|State of Goiás]]<ref>{{cite news |url=http://www.pilotopolicial.com.br/?p=12157 |title=Estado de Goiás receberá 3 helicópteros AW119 Koala |author=Mena Barreto|language=Portuguese |work=Piloto Policial |date=2010-08-30 |accessdate=2010-08-30}}</ref>
* [[Santa Catarina (state)|State of Santa Catarina]]<ref>{{cite news |url=http://www.pilotopolicial.com.br/koala-da-policia-militar-de-santa-catarina-completa-1-500-horas-de-voo/}}</ref>
* [[Rio Grande do Sul|State of Rio Grande do Sul]]<ref>{{cite news |url=http://www.pilotopolicial.com.br/bavbmrs-helicoptero-transfere-menino-com-avc/}}</ref><ref>http://www.pilotopolicial.com.br/novo-helicoptero-do-bavrs-transporta-paciente-para-santa-mariars/</ref>
;{{FIN}}
*[[Finnish Border Guard]]<ref>{{cite web|url= http://www.aviationnews.eu/2010/08/25/the-finnish-border-guard-takes-delivery-of-its-first-aw119ke/ |title= The Finnish Border Guard Takes Delivery Of Its First AW119Ke |publisher= aviationnews.eu |date=|accessdate=17 February 2013}}</ref> 
;{{KOR}}
*[[National Police Agency (Republic of Korea)|National Police Agency]] <ref>{{cite web|url= http://helihub.com/2010/07/21/korea-national-police-agency-takes-delivery-of-an-aw119ke/ |title= Korea National Police Agency takes delivery of an AW119Ke |publisher= helihub.com |date=|accessdate=10 February 2013}}</ref>
'''{{MYS}}'''

* [[Weststar Aviation (Malaysia)|Weststar General Aviation]]<ref>{{Cite web|url = http://my.agustawestland.com/content/finmeccanica-agustawestland-and-weststar-sign-orders-three-helicopters|title = Finmeccanica-AgustaWestland and Weststar Sign Orders for Three Helicopters|date = |accessdate = 2015-03-18|website = my.agustawestland.com|publisher = |last = |first = }}</ref>
;{{MEX}}
*[[Mexico State|Mexico State Government]]<ref>{{cite web|url= http://www.aero-news.net/index.cfm?do=main.textpost&id=0caa1351-b032-429d-9b42-f92a607aeb62|title= Mexico's State Gov't Air Rescue Unit Completes NGV Instructor Training |publisher= aero-news.net |date=|accessdate=10 February 2013}}</ref> 
;{{USA}}
* [[New York City Police Department]] <ref>{{cite web|url=http://www.aviationnews.eu/2012/06/26/nypd-aw119-fleet-achieve-20000-hour-milestone/ |title= NYPD AW119 Fleet Achieve 20,000 Hour Milestone |publisher= aviationnews.eu |date=|accessdate=10 February 2013}}</ref> 
* [[Phoenix Police Department]] <ref>{{cite web|url=http://www.ainonline.com/aviation-news/paris-air-show/2006-10-24/cop-shot-takes-koala |title= Cop shot takes koala |publisher= ainonline.com |date=|accessdate=10 February 2013}}</ref>
* Life Flight Network (https://www.lifeflight.org/about/fleet)

==Specifications (AW119Ke)==
{{External media|topic= |width=20% |align=right |video1=[https://www.youtube.com/watch?v=ademO8NK1LQ AgustaWestland AW119Kx promotional video] |video2=[https://www.youtube.com/watch?v=aFA3q0PzQ0I Description of AW119Kx cockpit] |video3=[https://www.youtube.com/watch?v=soVx1pOhOek Pellissier Helicopter AW119Kx operations]}}
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
 
|plane or copter?=copter
|jet or prop?=prop

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with * 
-->
|ref= AgustaWestland website<ref name = "kx broch"/><ref>{{cite web|title=AgustaWestland website |url=http://www.agustawestland.com/products01_02.asp?id_product=4&id=4 |accessdate=2007-05-20 |archiveurl=https://web.archive.org/web/20070927025547/http://www.agustawestland.com/products01_02.asp?id_product=4&id=4 |archivedate=2007-09-27 |deadurl=no |df= }}</ref>

|crew=1 pilot
|capacity=6-7 passengers
|length main= 13.01 m
|length alt=42 ft 8 in
|span main= 10.83 m
|span alt= 35 ft 6 in
|height main= 3.77 m
|height alt= 12 ft 4 in
|area main= 92.1 m²
|area alt= 991 ft²
|empty weight main=1,430 kg
|empty weight alt=3,152 lb
|loaded weight main= kg
|loaded weight alt= lb
|max takeoff weight main= 2,720 kg
|max takeoff weight alt= 6283 lb
|engine (prop)= [[Pratt & Whitney Canada PT6]]B-37A [[turboshaft]]
|number of props=1
|power main= 747 kW
|power alt= 1,002 hp
|max speed main= 267 km/h
|max speed alt= 166 mph, 152 knots
|ferry range main= 991 km
|ferry range alt=  618 miles, 535 nm
|ceiling main= 6,096 m
|ceiling alt= 15,000 ft
|climb rate main= 
|climb rate alt=
|armament=
}}

==See also==
{{aircontent
|related= 
* [[Agusta A109]]
|similar aircraft=
* [[Eurocopter Ecureuil]]
* [[Bell 407]]
* [[MD 600N]]
|lists=
* [[List of rotorcraft]]
|see also=
}}

==References==

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
* Gray, Peter. [https://www.flightglobal.com/pdfarchive/view/2000/2000-1%20-%200992.html "Flight Test: Koala bared."] ''Flight International'', 18 September 2000. pp.&nbsp;40–43.
{{refend}}

==External links==
{{Commons category|Agusta A119}}
* [http://www.leonardocompany.com/en/-/aw119kx Leonardo AW119Kx page]
* [http://www.leonardocompany.com/-/kestrel-india-aw119ke AW119Ke announcement press release]

{{AgustaWestland aircraft}}

[[Category:Italian civil utility aircraft 1990–1999]]
[[Category:Italian helicopters 1990–1999]]
[[Category:AgustaWestland aircraft|AW119]]