{{Infobox journal
| title = boundary 2
| italic title = no
| cover = [[File:Boundary 2 journal low resolution cover.jpg]]
| editor = [[Paul Bové|Paul A. Bové]]
| discipline = [[Literature]]
| publisher = [[Duke University Press]]
| country = United States
| frequency = Triannually
| history = 1972-present
| website = http://boundary2.dukejournals.org
| link1 = http://muse.jhu.edu/journals/boundary/
| link1-name = Online access at [[Project MUSE]]
| JSTOR = 01903659
| OCLC = 1408678
| LCCN = 72626433
| ISSN = 0190-3659
| eISSN = 1527-2141
}}
{{DISPLAYTITLE:''boundary 2''}}
'''''boundary 2''': an international journal of literature and culture'' is a triannual [[Peer review#Scholarly peer review|peer-reviewed]]<ref name=Ulrichs>{{cite web |title=boundary 2 |url=https://ulrichsweb.serialssolutions.com/title/1337368669877/56579 |work=Ulrich'sWeb |publisher=[[ProQuest]], LLC |accessdate=18 May 2012}} {{subscription required}}</ref> [[academic journal]] of [[postmodernism|postmodern]] [[theory]], [[literature]], and [[culture]].<ref name=minn>{{cite journal |last=Williams |first=Jeffrey J. |title=The Counter-Memory of Postmodernism: An Interview with William V. Spanos |journal=The Minnesota Review |date=Fall 2006 |issue=67 |url=http://www.theminnesotareview.org/journal/ns67/interview_spanos.shtml |accessdate=18 May 2012}}</ref> Established in 1972<ref name=Ulrichs /> by [[William V. Spanos]] and [[Robert Kroetsch]] ([[Binghamton University]]), under the title ''boundary 2, a journal of postmodern literature'', the journal moved to [[Duke University Press]] in the late 1980s<ref>{{cite web |title=boundary 2 (Editorial website) |url=http://boundary2.org/about/ |accessdate=18 May 2012}}</ref> and is now [[Editor-in-chief|edited]] by [[Paul Bové|Paul A. Bové]] ([[University of Pittsburgh]]).<ref>{{cite web| title=Academic journal boundary 2, edited in Pittsburgh, has a national reputation.|last = Colman |first = Adam| url=http://www.pittsburghcitypaper.ws/pittsburgh/academic-journal-boundary-2-edited-in-pittsburgh-has-a-national-reputation/Content?oid=1340024 | work= Pittsburgh City Paper |date=April 10, 2008 |accessdate = 18 May 2012}}</ref>

Since the early 2000s the journal has been closed to unsolicited submissions.<ref>{{cite web |title=Editorial correspondence (Back Matter boundary 2)  |url=http://boundary2.dukejournals.org/content/36/3/local/back-matter.pdf |publisher=[[Duke University Press]] |accessdate=18 May 2012 | quote = The editors of ''boundary 2'' announce that they no longer intend to publish in the standard professional areas, but only materials that identify and analyze the tyrannies of thought and action spreading around the world and that suggest alternatives to these emerging configurations of power.}}</ref> This policy was described by [[Jeffrey J. Williams|Jeffrey Williams]], editor of [[Minnesota Review]], as one that "seems a little too closed, and would go in the opposite direction of taking chances".<ref name="Williams 2006">{{cite journal |last=Williams |first=Jeffrey J. |title=The Counter-Memory of Postmodernism: An Interview with William V. Spanos |journal=[[The Minnesota Review]] |date=Fall 2006 |issue=67 |url=http://www.theminnesotareview.org/journal/ns67/interview_spanos.shtml |accessdate=18 May 2012}}</ref> ''boundary 2'' has published special issues focusing on postmodernism in individual countries such as Greece<ref>{{cite journal |last=Papanikolaou |first=Dimitris |title=Greece as a postmodern example: Boundary 2 and its special issue on Greece |journal=ΚΑΜΠΟΣ: CAMBRIDGE PAPERS IN MODERN GREEK |year=2005 |issue=13 |url=http://www.mod-langs.ox.ac.uk/files/docs/greek/boundary2kambos.pdf |accessdate=18 May 2012}}</ref> or Canada,<ref>{{cite book |last=Kroetsch |first=Robert |title=Re: Reading the Postmodern: Canadian Literature and Criticism After Modernism |date=October 2010 |publisher=University of Ottawa Press |isbn=9780776607399 |pages=1–7 |url=https://books.google.com/books?isbn=0776607391 |editor=Robert David Stacey |accessdate=18 May 2012 |chapter=boundary 2 and the Canadian postmodern }}</ref> as well as a book of articles previously published in the journal.<ref>{{cite book |last=Bové |first=Paul |authorlink=Paul Bové |title=Early Postmodernism: Foundational Essays |year=1995 |publisher=Duke University Press |isbn=9780822316497 |url=https://books.google.com/books?id=OiowPzKwscwC}}</ref> In an interview published in the ''Minnesota Review'', Spanos describes the history of the journal, its financial and editorial problems, and the motivations for various changes over the years, including the journal's practice of publishing articles by invitation only, refusing unsolicited submissions.<ref name="Williams 2006"/>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic Search Elite]]
* [[Academic Search Premier]]
* [[Arts and Humanities Citation Index]]
* [[Current Contents]]/Arts and Humanities
* [[Current Contents]]/ Social & Behavioral Sciences
* [[Expanded Academic ASAP]]
* [[EBSCO|EBSCO databases]]
* [[International Bibliography of Periodical Literature]]
* [[MLA Bibliography]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[Sociological Abstracts]]
}}

== References ==
{{reflist|30em}}

== Further reading ==
* {{cite book
 | last = Bové
 | first = Paul A. (editor)
 | authorlink =
 | title = Early Postmodernism: Foundational Essays
 | publisher = Duke University Press
 | series = a boundary 2 book 
 | volume =
 | edition =
 | year = 1995
 | location = Durham, NC
 | pages = 276
 | url = https://books.google.com/books?id=OiowPzKwscwC&printsec=frontcover&dq=Early+Postmodernism:+Foundational+Essays&hl=en&sa=X&ei=10z3Ucb0F8e34AOTjoCQCg&ved=0CDEQuwUwAA#v=onepage&q=Early%20Postmodernism%3A%20Foundational%20Essays&f=false
 | doi =
 | quote =
 | isbn = 978-0-8223-1649-7|OCLC=32238059}}
*{{cite journal
 | last =Spanos
 | first =William V. 
 | authorlink =William V. Spanos
 | format = free full text copy
 | title = The Detective and the Boundary: Some Notes on the Postmodern Literary Imagination
 | journal =boundary 2
 | volume =01
 | issue =01
 | pages =147–168
 | publisher = Duke University Press
 | location =Durham, NC
 | date =Autumn 1972
 | url =http://www.english.txstate.edu/cohen_p/postmodern/Theory/Spanos.html 
 | jstor = 302058}}

== External links ==
* {{Official website|http://www.dukeupress.edu/boundary2/}}

[[Category:Cultural journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Duke University Press academic journals]]
[[Category:Publications established in 1972]]