{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = L'Œuvre
| title_orig    = 
| translator    = 
| image         = File:ZolaMasterpiece.jpg<!--prefer 1st edition-->
| caption = ''The Masterpiece'' (1886) by Émile Zola (1840-1902)
| author        = [[Émile Zola]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Les Rougon-Macquart]]
| genre         = [[Novel]]
| publisher     = Charpentier (book form)
| release_date  = 1885-1886 (serial) and 1886 (book form)
| english_release_date =
| media_type    = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| preceded_by   = [[Germinal (novel)|Germinal]]
| followed_by   = [[La Terre]]
}}

'''''L'œuvre''''' is the fourteenth novel in the [[Les Rougon-Macquart|Rougon-Macquart]] series by [[Émile Zola]]. It was first serialized in the periodical ''[[Gil Blas (periodical)|Gil Blas]]'' beginning in December 1885 before being published in novel form by Charpentier in 1886.

The title, translated literally as "The Work" (as in [[work of art]]), is often rendered in English as ''The Masterpiece'' or ''His Masterpiece''. It refers to the struggles of the [[protagonist]] Claude Lantier to paint a great work reflecting his talent and genius.

''L'œuvre'' is a fictional account of Zola's friendship with [[Paul Cézanne]] and a fairly accurate portrayal of the Parisian art world in the mid 19th century. Zola and Cézanne grew up together in [[Aix-en-Provence]], the model for Zola's Plassans, where Claude Lantier is born and receives his education. Like Cézanne, Claude Lantier is a revolutionary artist whose work is misunderstood by an art-going public hidebound by traditional subjects, techniques and representations. Many of the characteristics ascribed to Claude Lantier are a compound taken from the lives of several [[Impressionism|impressionist painters]] including [[Claude Monet]], [[Édouard Manet]], as well as Paul Cézanne. Zola's self-portrait can be seen in the character of the novelist Pierre Sandoz.

The book is often blamed for ending the friendship between Cézanne and Zola. The story of a groundbreaking artist unable to live up to his potential must have seemed intensely personal to Cézanne; no correspondence exists between the two after a letter in which Cézanne thanks Zola for sending him the novel.

The novel covers about 15 years, ending in 1870. Besides depicting the [[bohemian]] art world of 19th-century [[Paris]], ''L'œuvre'' explores the rise of [[realism (arts)|Realism]], [[Naturalism (visual art)|Naturalism]] and [[Impressionism]] in [[painting]]. Zola also looks at contemporary [[sculpture]], [[literature]], [[architecture]], [[music]] and [[journalism]], as well as the [[commodification]] of art. In creating his portrayal of the Parisian art world Zola includes several characters who are composites of real-life art world related figures; artists, writers, art dealers, and friends that he knew.

''L'œuvre'' was translated into English by [[Ernest A. Vizetelly]] in 1886 (reprinted by [[Barnes & Noble]] in 2006); other translations have appeared since. One of the most readily accessible is that by Thomas Walton (1957), revised in 1993 for [[Oxford World's Classics]].

==Plot summary==
Painter Claude Lantier advocates painting real subjects in real places, most notably outdoors. This is in stark contrast to the artistic establishment, where artists painted in the studio and concentrated on mythological, historical and religious subjects. His art making is revolutionary and he has a small circle of like-minded friends equally intent on shaking up the art world and challenging the establishment. His best friends are his childhood comrades Pierre Sandoz, novelist and Louis Dubuche, an architect. Like Zola, Sandoz contemplates a series of novels about a family based in science and incorporating modern people and everyday lives. Dubuche is not half as bold as Claude and, although a painter, finds music to be his passion.  He chooses a more conventional course, opting for the security of a middle-class life and a [[bourgeoisie|bourgeois]] marriage. Sandoz also pursues marriage&nbsp;– not for love but stability and to better understand what he is writing about.
The outcry in the artistic community over the sidelining of new artists in favor of popular, established, traditional artists at the annual [[Salon (Paris)|Salon]] of the [[Académie des Beaux-Arts]] leads to the creation of a [[Salon des Refusés]] for the rejected artists to display their work. No painting gathers more interest or generates more criticism than Claude's. Entitled ''Plein Air'' (''Open Air''), it depicts a nude female figure in the front center and two female nudes in the background, with a fully dressed man, back to the viewer in the foreground. (Zola deliberately invokes ''[[The Luncheon on the Grass|Le déjeuner sur l'herbe]]'' by [[Édouard Manet]], which provoked outcries at the actual Salon des Refusés in 1863.)

Claude moves to the country to soak up more of the 'Open Air' atmosphere he revelled in as a child and to create more masterpieces. Accompanying him is Christine Hallegrain, who served as the model for Claude's nude and they have a son. Claude is unable to paint much and grows more and more depressed. For the sake of his health, Christine convinces him to return to Paris. Claude has three paintings in three years rejected by the Salon before a spectacular view of the [[Île de la Cité]] captures his imagination. He becomes obsessed with this vision and constructs a massive canvas on which to paint his masterpiece. He is unable to project his ideas successfully or combine them into a meaningful whole. He begins adding incongruous elements (like a female nude bather), reworks and repaints until the whole enterprise collapses into disaster, then starts over. His inability to create his masterpiece deepens his depression. The slow breakup of his circle of friends contributes to his decaying mental state, as does the success of one of his confreres, a lesser talent who has co-opted the 'Open Air' school and made it a critical and financial triumph.

Christine, whom he has at last married, watches as the painting&nbsp;– and especially the nude&nbsp;– begins to destroy his soul. When their son dies, Claude is inspired to paint a picture of the dead body that is accepted by the Salon (after considerable politicking). The painting is ridiculed for its subject and its execution and Claude again turns to his huge landscape. Christine watches as he spirals further into obsession and madness. A last-ditch effort to free him from Art in general and from his wished-for masterpiece in particular has an effect but in the end Claude hangs himself from his scaffolding. The only ones of his old friends who attend his funeral are Sandoz and Bongrand, an elder statesman of the artistic community who recognized and helped nurture Claude's genius.

==Relation to the other Rougon-Macquart novels==

Claude Lantier (b. 1842, the son of Gervaise Macquart and Auguste Lantier) is first introduced briefly as a child in ''[[La Fortune des Rougon|La fortune des Rougon]]''. In ''[[L'Assommoir|L'assommoir]]'', he comes to Paris with his parents but returns to Plassans under the sponsorship of a local patron who recognizes his artistic talent. In ''[[Le Ventre de Paris|Le ventre de Paris]]'', Claude has returned to Paris and is discovered in the [[Les Halles]] marketplace searching for realistic subjects to paint.

Zola's plan for the Rougon-Macquart novels was to show how [[heredity]] and environment worked on a family over the course of the [[Second French Empire]]. Claude is the son (and grandson) of [[alcoholism|alcoholics]] and inherits their predisposition for self-destruction. All of the descendants of Adelaïde Fouque (Tante Dide), Claude's great-grandmother demonstrate what today would be called [[obsessive-compulsive disorder|obsessive-compulsive]] behaviors. In Claude, this is manifested in his obsessive approach to making art.

Claude's brothers are Jacques Lantier (''[[La Bête humaine|La bête humaine]]''), the engine driver who becomes a murderer and Étienne Lantier (''[[Germinal (novel)|Germinal]]''), the miner who becomes a revolutionary and union agitator. Their half-sister is the [[prostitution|prostitute]] Anna (Nana) Coupeau (''[[Nana (novel)|Nana]]'').

Claude's son Jacques also figures in ''L'œuvre'', his death from unspecified causes being brought about by his parents' neglect. In him, Zola shows what happens when energy and natural creativity are stifled.

==Historical basis==
The book includes a few autobiographical details. As a young journalist, Zola wrote many articles on art and he was deeply interested in the newest ways of painting; he was one of the earliest champions of the work of [[Édouard Manet]]. The character of Sandoz, a young writer whose ambition is to write a story of a family that would portray the present epoch, is most clearly a self-portrait of the author. The basis of some of the other characters, including Claude Lantier, is murkier. Though Claude is most often understood as being based on [[Cézanne]], the [[Impressionist]] painters [[Édouard Manet]] and [[Claude Monet]] are often cited as other possible sources. (In fact, Claude Lantier's first painting in the book is based on Manet's ''Le déjeuner sur l'herbe''.). In a letter written after the novel's appearance in 1886, Claude Monet (who was acquainted with Cézanne and Manet) indicated that he did not recognize himself or any of his fellow painters in the character.  Other parallels between the author’s life and the novel include Lantier’s dead child painting being similar to Monet’s portrait of the deceased Camille (his first wife), Lantier’s idea of mobile studios mirroring Monet’s and loose ties equating Fagerolles and Manet.  In the book, the Open Air school got its name from the title of Lantier’s first mentioned painting.  In real life, the Impressionists got their name from Monet’s [[Impression: Sunrise]]. Open Air ([[Plein air]]) and Impressionism were insulting names given by critics and jeering crowds.

==Sources==
*Brown, F. (1995). ''Zola: A life''. New York: Farrar, Straus & Giroux.
*Zola, E. ''L'œuvre'', translated as ''The Masterpiece'' by Thomas Walton (1957, rev. 1993)

==External links==
{{Gutenberg|no=17517|name=L'oeuvre}} (French)
{{Gutenberg|no=15900|name=His Masterpiece}} (English)
*{{IMDb title|id=0356956|title=L'Oeuvre}} (1967) (TV) (French)
*Aruna D'Souza, [http://www.19thc-artworldwide.org/index.php/autumn04/295-paul-cezanne-claude-lantier-and-artistic-impotence "Paul Cézanne, Claude Lantier, and Artistic Impotence."] &nbsp;In ''Nineteenth-Century Art Worldwide'', autumn 2004.
*See also Zola and The Quest for The Absolute in Art, by Thomas Zamparelli, Yale French Studies © 1969 Yale University Press

{{Les Rougon-Macquart}}

{{Authority control}}

{{DEFAULTSORT:Oeuvre}}
[[Category:1886 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Gil Blas (periodical)]]
[[Category:Novels about artists]]
[[Category:Novels set in Paris]]