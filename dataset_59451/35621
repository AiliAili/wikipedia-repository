{{Taxobox
| image = Abantiades_latipennis.jpg
| regnum = [[Animal]]ia
| phylum = [[Arthropod]]a
| classis = [[Insect]]a
| ordo = [[Lepidoptera]]
| familia = [[Hepialidae]]
| genus = ''[[Abantiades]]''
| species = '''''A. latipennis'''''
| binomial = ''Abantiades latipennis''
| binomial_authority = [[Norman Tindale|Tindale]], 1932
}}
'''''Abantiades latipennis''''', known as the '''Pindi moth''', is a species of [[moth]] in the [[Hepialidae]] family. It may also be referred to as a swift moth or a ghost moth, as this is a common name associated with Hepialidae.<ref name="capinera"/> Endemic to [[Australia]] and identified in 1932, it is most populous in [[temperate rainforest]] where [[Eucalyptus|eucalypti]] are prevalent, as the larvae feed primarily on the roots of these trees. Females lay eggs during flight in a scattering fashion. The larvae live for over eighteen months underground, while adult moths survive for approximately one week, as they have no mouthparts with which to feed. The moths are preyed upon by a number of predators, including bats and owls. Brown in colour overall, males are paler and the identifying silver bars of the male's wings are more prominent than those of the female's, with dark margins. Male adults are generally smaller.

Established clearfelling practices have been shown to favour the Pindi moth, and could lead to it being considered a pest due to opportunistic proliferation of the species. The resulting damage caused to the trees on which it feeds may be considered significant.

== Taxonomy and naming ==

''Abantiades latipennis'' is one of fourteen species currently identified within the genus ''[[Abantiades]]'', all of which are [[endemism|found exclusively]] in Australia. The species was first described in 1932 by [[Norman Tindale]], an Australian entomologist and anthropologist. Tindale described the species, under its current name, but provided no [[etymology]] for the [[specific name (zoology)|specific epithet]] ''latipennis''. He based the species description on specimens from [[Lorne, Victoria|Lorne]] (including the [[holotype]] male and [[Type_(biology)#In_zoology|allotype]] female), [[Pomonal, Victoria|Pomonal]] and [[Mount Mistake]] (in the [[Langi Ghiran State Park]]), [[Victoria (Australia)|Victoria]], and from [[Zeehan, Tasmania|Zeehan]], [[Eaglehawk Neck, Tasmania|Eaglehawk Neck]] and [[Launceston, Tasmania|Launceston]], [[Tasmania]].<ref name="Tindale"/><ref name="nielsen"/>

Being a member of the Hepialiade family, ''A.&nbsp;latipennis'' is considered [[Primitive (phylogenetics)|phylogenetically primitive]], possessing several features that are indicative of earlier evolutionary development.  The jugum on the forewings of the adult is an archaic wing coupling mechanism; further primitive characteristics include, as adults, the lack of mouthparts, large spacing between the fore- and hindwings, and the arrangement of the female genitalia.<ref name="Tindale"/><ref name="nielsen"/>

== Distribution and habitat ==

''A.&nbsp;latipennis'' is endemic to the [[Australasia]]n region.  More specifically, the moths inhabit [[New South Wales]], [[Tasmania]], [[Victoria (Australia)|Victoria]], and [[Australian Capital Territory]].  As with other ''Abantiades'' species, the moth's habitat is temperate rainforest, both [[old-growth forest|primary]] and [[secondary forest|secondary]].  During its larval stage, the moth feeds on the roots of trees, and its prospering is thought to have an economic impact on the timber industry.<ref name="Tindale"/><ref name="au f dir"/>

== Life cycle and behaviour ==

Female moths "lay" their eggs by scattering them during flight; up to 10,000 eggs are released at once. Larvae then hatch from the eggs in the [[plant litter|leaf litter]] on the forest floor and begin tunnelling, in search of suitable host roots.  The number of [[instar]]s and the period of the larval stage is yet unknown, with field observations suggesting a larval stage of more than eighteen months.<ref name="herbison-evans"/><ref name="Kile"/>

[[File:Abantiades latipennis 2.jpg|thumb|left|An adult ''Abantiades latipennis'', possibly a female, photographed in [[Austins Ferry]], Tasmania]]

The [[herbivore|phytophagous]] larvae of ''A.&nbsp;latipennis'' feed primarily on the root systems of two species of tree, ''[[Eucalyptus obliqua]]'' (messmate stringybark) and ''[[Eucalyptus regnans]]'' (mountain ash). Both of these species are present in old growth forests and dominant in regrowth forests, contributing to the moth's success in its own habitat. Forming simple and vertical tunnels, lined with silk, the caterpillars are [[wikt:subterranean|subterranean]] before and during [[pupa]]tion, emerging for their [[metamorphosis]].  The tunnel entrances, 6 to 10 millimetres in diameter, are covered with silk webbing and leaf litter, and can be up to {{convert|60|cm|ft|0}} deep, although depth is more usually 12 to 35&nbsp;cm.<ref name="Kile"/><ref name="clearfelling study"/><ref name="common"/><ref name="grehan"/>

By chewing the [[taproot|tap]] and [[lateral root|lateral]] roots of trees, caterpillars feed on the [[vascular cambium|cambium]] growth, produced by the tree at the site of the injury. The larvae may [[girdling|girdle]] a root, thereby causing its death, or the lesions may be only partial, allowing the root to continue functioning, albeit with some deformity.<ref name="Kile"/><ref name="common"/><ref name="grehan"/>

The subterranean habitat of the larvae generally provides protection from predation, but the larvae are sometimes parasitised by [[Tachinidae|tachina flies]].  The [[parasitoid]] larvae of the tachinid ''[[Rutilotrixa diversa]]'', usually hosted by [[Scarabaeidae|scarab beetles]], have been discovered infecting ''A.&nbsp;latipennis''.<ref name="Kile"/><ref name="cantrell"/>

Adult ''A.&nbsp;latipennis'' are [[crepuscular]] and males are strongly attracted to lights, forming [[lek (biology)|leks]] at dusk, most notably after rains in autumn and late summer.   Females use [[pheromone]]s to attract males for mating.  As a primitive species, they lack [[Insect mouthparts|mouthparts]] as adults and are therefore unable to feed. Their lifespan as winged creatures is understandably short, lasting approximately a week, barring predation. Predation is commonly by bats, owls, and possums, though several other animals, from spiders to cats, occasionally consume them and contribute to the brief lifespan of the metamorphosed moths.<ref name="dugdale"/>

== Ecology ==

As part of a study measuring the impact that the practice of [[clearcutting|clearfelling]] has on biodiversity in the [[Weld Valley]] of Tasmania, it was found that ''A.&nbsp;latipennis'' was one of the few species that thrived in regrowth forests that were previously clearfelled.  An earlier study, conducted in other regions of southern Tasmania, examined the relationship between the moth and ''Eucalyptus regnans'' and ''E.&nbsp;obliqua'' and reached the same conclusion.  The tunnelling and feeding habit of ''A.&nbsp;latipennis'' larvae on the roots of these two eucalypt species is mostly responsible for its abundance in clearfelled forests, as the trees are the typical regrowth of logged areas.  This success may also be due in part to the caterpillar's lack of dependence on decaying vegetation, a characteristic of the ''Abantiades'' genus and dissimilar to other Tasmanian genera—such as ''[[Eudonia]]'' and ''[[Barea (genus)|Barea]]''—that have not fared as well in clearfelled forest.<ref name="Kile"/><ref name="clearfelling study"/>

The crowns of the eucalypts infected by ''A.&nbsp;latipennis'' have exhibited no consistent indication of root disturbance, with most trees studied appearing healthy and of average size for secondary forest.  ''E.&nbsp;regnans'' and ''E.&nbsp;obliqua'' have demonstrated slow growth in regrowth stands, but this tendency has been attributed to significant root and crown competition.  Some trees appeared [[wikt:chlorosis|chlorotic]] (a yellowing effect in plants, caused by a reduction in [[chlorophyll]]), but this was not a reliable indicator of root infestation, and may be the result of other influences.  Well-established clearfelling practices in Tasmania could exacerbate the favouring of this species, and its proliferation could lead to extensive eucalypt damage and pest concerns, though the potential effect of this threat is as yet undetermined.<ref name="Kile"/><ref name="clearfelling study"/>

Lesions on the roots caused by the larval feeding provide ideal sites for the establishment of [[root rot]]-causing fungi, once the larvae depart the root system for pupation.  Areas on the roots with past feeding damage were found to have ''[[Armillaria]]'' sp. flourishing in the lesions.  In rarer cases, the pathogen ''[[Perenniporia medulla-panis]]'' was also found attacking the roots at the damaged locations.  Other cases of decay and discolouration were noted, but attributed to unidentified microorganisms.<ref name="Kile"/>

== Morphology and identification ==

''A.&nbsp;latipennis'' larvae vary in size and colour during growth, but can be grouped small and large. The small caterpillars are generally {{convert|12|mm|in|abbr=off}} long and an overall milky grey, with a light brown head capsule that is about {{convert|3|mm|in|abbr=on}} wide. Large caterpillars may be milky grey or a dark green-brown, {{convert|60|to|90|mm|in|abbr=on}} long, with a {{convert|6|to|9|mm|in|abbr=on}} wide brown head capsule.<ref name="Kile"/>

Females are larger than males, with the wingspan of the male adult approximately {{convert|80|mm|in|abbr=on}}. A female specimen collected in 1979 had a wingspan of {{convert|108|mm|in|abbr=on}}, but Tindale recorded female wingspans at {{convert|150|mm|in|abbr=on}} in 1932. The [[insect wing|forewings]] of both males and females feature silvery-white bars, although the bars of the male moth's wings are more prominent and with dark margins. The female's body colour is usually a darker brown than the male's pale brown, as noted by Tindale, although a grey-brown female was collected in 1979.<ref name="herbison-evans"/><ref name="Kile"/><ref name="au moths online"/>

Hepialidae have short, pectinate [[antenna (biology)|antennae]] and, unusually primitive for Lepidoptera, lack a functional [[proboscis]] or [[Wing coupling (Lepidoptera anatomy)#retinaculum|retinaculum]] and are therefore non-feeding.  The moths possess several other morphological features that are considered [[Phylogenetics|phylogenetically]] primitive.  The gap between the fore- and hindwing is distinct and the wings are covered in scale-like hairs.  At the base of the forewing is a jugum, a small lobe that joins the fore- and hindwings during flight.  In females, the configuration of the genitalia is [[External morphology of Lepidoptera#Genitalia|exoporian]], typified by an external groove along which [[spermatophore]]s are transferred after mating, from the copulatory opening (the ostium bursae), to the [[ovipore]] for fertilisation.<ref name="Tindale"/><ref name="nielsen"/><ref name="common"/><ref name="kristensen"/>

==References==
{{Reflist|2|refs=
<ref name="capinera">{{cite book 
| last = Capinera | first = John L. 
| title = Encyclopedia of Entomology 
| publisher = Springer 
| year = 2008 | page = 1613 
| url = https://books.google.com/books?id=i9ITMiiohVQC&pg=PA1613&dq=Hepialidae+(Insecta:+Lepidoptera).+Fauna+of+New+Zealand 
| isbn = 978-1-4020-6242-1}}</ref>

<ref name="au f dir">{{cite web 
| title = Species ''Abantiades latipennis'' Tindale, 1932 
| work = Australian Biological Resources Study—[[Australian Faunal Directory]] 
| publisher = [[Department of the Environment, Water, Heritage and the Arts]] 
| date = 9 October 2008 
| url = http://www.environment.gov.au/biodiversity/abrs/online-resources/fauna/afd/taxa/Abantiades_latipennis 
| accessdate = 2009-06-02}}</ref>

<ref name="clearfelling study">{{cite web 
| last1 = Green | first1 = Graham 
| last2= Gray | first2 = Alan
| last3= McQuillan | first3 = Peter 
| title = Biodiversity impacts and sustainability implications of clearfell logging in the Weld Valley, Tasmania 
| publisher = Timber Workers for Forests 
| date = March 2004 
| url = http://www.warra.com/documents/publications/Green_etal_2004.pdf | format = pdf 
| accessdate = 2009-06-03}}</ref>

<ref name="Kile">{{cite journal 
| last1 = Kile | first1 = G. A. 
| last2 = Hardy | first2 = R. J.
| last3 = Turnbull | first3 = C. R. A. 
| title = The Association between ''Abantiades latipennis'' (Lepidoptera, family Hepialidae) and ''Eucalyptus obliqua'' and ''Eucalyptus regnans'' in Tasmania 
| journal = [[Australian Journal of Entomology]] 
| volume = 18 | issue = 1 | pages = 7–17 | publisher = Wiley Blackwell | date = April 1979 
| url = http://www3.interscience.wiley.com/cgi-bin/fulltext/119608075/PDFSTART | format = pdf 
| doi = 10.1111/j.1440-6055.1979.tb00801.x 
| accessdate = 2009-06-03}}</ref>

<ref name="common">{{cite book 
| last = Common | first = Ian Francis Bell 
| title = Moths of Australia 
| publisher = Brill 
| year = 1990 | pages = 27–28, 149–150 
| url = https://books.google.com/books?id=magzbmvdRvQC&printsec=frontcover#PRA1-PA150,M1 
| isbn = 978-90-04-09227-3}}</ref>

<ref name="dugdale">{{cite journal 
| last = Dugdale | first = J. S. 
| title = Hepialidae (Insecta: Lepidoptera); Popular Summary 
| journal = Fauna of New Zealand 
| issue = 30 
| publisher = [[Landcare Research]], [[University of Auckland]] 
| date = 1 March 1994 
| url = http://www.landcareresearch.co.nz/research/biosystematics/invertebrates/faunaofnz/Extracts/FNZ30/fnz30pop.asp 
| issn = 0111-5383 | accessdate = 2009-06-03}}</ref>

<ref name="herbison-evans">{{cite web 
| last = Herbison-Evans | first = Don 
|author2= Crossley, Stella|author3= Marriott, Peter 
| title = ''Abantiades latipennis'' Tindale, 1932 
| work = Hepialidae 
| publisher = [[University of Technology, Sydney]] 
| date = 11 February 2009 
| url = http://www-staff.it.uts.edu.au/~don/larvae/hepi/latipen.html 
| accessdate = 2009-06-03}}</ref>

<ref name="kristensen">{{cite book 
| last = Kristensen | first = Niels P. 
| title = Lepidoptera, Moths and Butterflies 
| work = Handbook of Zoology 
| volume = IV 
| publisher = [[Walter de Gruyter]] 
| year = 1999 | location = Berlin | pages = 61–62 
| url = https://books.google.com/books?id=B9rdQ1gHuAAC&pg=PA41&dq=The+non-Glossatan+Moths 
| isbn = 978-3-11-015704-8}}</ref>

<ref name="au moths online">{{Cite web 
| title = ''Abantiades latipennis'' 
| work = Australia Moths Online 
| publisher = CSIRO Australia 
| year = 2007 
| url = http://www1.ala.org.au/gallery2/v/Hepialidae/Abantiadeslatipennis/ 
| accessdate = 2010-08-05}}</ref>

<ref name="nielsen">{{Cite journal 
| last1 = Nielsen | first1 = Ebbe S. | authorlink = Ebbe Nielsen 
| last2 = Robinson | first2=G. S.
| last3 = Wagner | first3=D. L. 
| title = Ghost-moths of the world: a global inventory and bibliography of the Exoporia (Mnesarchaeoidea and Hepialoidea) (Lepidoptera) 
| journal = [[Journal of Natural History]] 
| volume = 34 | issue = 6 | pages = 823–878 | date = 6 June 2000 
| doi = 10.1080/002229300299282 }}</ref>

<ref name="Tindale">{{cite journal 
| last=Tindale | first=N. B. | authorlink=Norman Tindale 
| year = 1932 
| title = Revision of the Australian ghost moths (Lepidoptera Homoneura, family Hepialidae). Part 1 
| journal = Records of the South Australian Museum 
| volume = 4 |pages = 497–536 
| url=http://www.samuseum.sa.gov.au/Journals/RSAM/RSAM_v004/rsam_v004_p497p536.pdf 
| format = [[Portable Document Format|PDF]]}}</ref>

<ref name="grehan">{{Cite journal
  | last = Grehan | first = J. R.
  | title = Larval feeding habits of the Hepialidae (Lepidoptera)
  | journal = [[Journal of Natural History]]
  | volume = 23 | issue = 4 | pages = 803–824
  | publisher = Taylor & Francis
  | year = 1989
  | doi = 10.1080/00222938900770421
  }}</ref>

<ref name="cantrell">{{Cite journal
  | last = Cantrell
  | first = Bryan K. 
  | title = An updated host catalogue for the Australian Tachinidae (Diptera)
  | journal = Journal of the Australian Entomological Society
  | volume = 25
  | issue = 3
  | pages = 255–265
  | year = 1986
  | doi = 10.1111/j.1440-6055.1986.tb01112.x
  }}</ref>
}}

{{Good article}}

{{DEFAULTSORT:Abantiades Latipennis}}
[[Category:Hepialidae]]
[[Category:Moths of Australia]]
[[Category:Endemic fauna of Australia]]
[[Category:Moths described in 1932]]