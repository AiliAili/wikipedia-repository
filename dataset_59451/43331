{|{{Infobox aircraft begin
 |name = NA-16/BT-9/NJ-1/ Harvard&nbsp;I/NA-57/Sk 14   <!--NOT a typo - the NJ-1 was a precursor to the more advanced SNJ-1-->
 |image = File:NA-16 FAH-21 EDUARDO SOSA 2005.jpg
 |caption = NA-16-2A/NA-42 "FAH-21" displayed outside at the Honduras Air Museum at [[Toncontín International Airport|Toncontín]]
}}{{Infobox aircraft type
 |type = Trainer
 |manufacturer = [[North American Aviation]]
 |first flight = 1 April 1935
 |introduced =
 |retired =
 |status = retired
 |primary user = [[United States Army Air Corps]]
 |more users = [[Royal Australian Air Force]]<br>[[Swedish Air Force]]<br>[[French Air Force]]
 |produced = 1935 to 1939
 |number built = 1,935
 |unit cost =
 |variants with their own articles = [[North American BT-9]] <br> [[CAC Wirraway]]
 |developed into = [[North American T-6 Texan]] <br> [[North American P-64]]
}}
|}

The '''North American Aviation NA-16''' was the first trainer aircraft built by [[North American Aviation]], and was the beginning of a line of closely related North American trainer aircraft that would eventually number more than 17,000 examples.

==Design and development==
[[File:North American NA-16 prototype NX2080.jpg|thumb|First NA-16 with initial canopy and still in civilian registration.]]
The NA-16 is a family of related single-engine, low-wing monoplanes with tandem seating.<ref>Hagedorn 1997, p. 4.</ref>

Variants could have an open cockpit (the prototype and the NA-22) or be under a glass greenhouse that covered both cockpits.<ref name="Hagedorn 1997, pp. 20–21">Hagedorn 1997, pp. 20–21.</ref> On some variants, the rear of the canopy could be opened for a gunner to fire to the rear.<ref>Hagedorn 1997, p. 21.</ref> A variety of air-cooled [[radial engine]]s, including the [[Wright Whirlwind]], [[Pratt & Whitney Wasp]] and [[Pratt & Whitney Wasp Junior]] of varying horsepowers, could be installed depending on customer preferences.<ref>Hagedorn 1997, pp. 6–7.</ref> The fuselage was built up from steel tubes and normally fabric covered; however, later versions were provided with aluminium monocoque structures.<ref>Hagedorn 1997, p. 12.</ref>

During the development of the design, a six&nbsp;inch stretch was made by moving the rudder post aft.<ref>Hagedorn 1997, p. 53.</ref> Many versions had a fixed landing gear, but later versions could have retractable gear, mounted in a widened wing center section (which could have either integral fuel tanks or not).<ref>Hagedorn 1997, p. 61.</ref> Most had a straight trailing edge on the outer wing while again, some had the wing trailing edge swept forward slightly in an attempt to fix a problem with stalls and spins.<ref>Hagedorn 1997, pp. 14, 19.</ref> Several different rudders were used, with early examples having a round outline, intermediate examples having a square bottom on the rudder (Harvard I) and late examples using the triangular rudder of the AT-6 series, due to a loss of control at high angles of attack with the early types.<ref>Hagedorn 1997, p. 19.</ref> Horizontal and vertical tails were initially covered in corrugated aluminum, but later examples were smooth-skinned, and the horizontal stabilizer was increased in chord near its tips on later versions.<ref>Hagedorn 1997, pp. 14–15.</ref>

The NA-16 flew for the first time on 1 April 1935, and was submitted to the [[United States Army Air Corps]] for evaluation as a basic trainer.<ref name="Hagedorn 1997, p. 8">Hagedorn 1997, p. 8.</ref> The Army accepted the trainer for production but with some detail changes. The modified NA-16 was redesignated by North American as the '''NA-18''', with production examples entering Air Corps service as the [[North American BT-9]] (NA-19). Similar aircraft continued to be sold outside the U.S. under the NA-16 designation.<ref>Hagedorn 1997, p. 15.</ref>

=== Foreign developments ===
;Australia
The [[Commonwealth Aircraft Corporation]] produced 755 units of a modified version of the NA-16-2K known there as the [[CAC Wirraway|Wirraway]] between 1939 and 1946.<ref name="Francillon">{{cite book|last=Francillon|first= René J|title=The Royal Australian Air Force & Royal New Zealand Air Force in the Pacific|series=Aero Pictorials 3|publisher=Aero Publishers Inc, 1970 |location=California|year=|isbn=978-0-8168-0308-8|id=Library of Congress Number 76-114412}}</ref>

;Argentina
Experience with the NA-16-4P and deteriorating political relations with the US led to the local development of the I.Ae. D.L. 21, which shared the NA-16 fuselage structure; however it proved too difficult to produce. As a result of this, an entirely new design (the [[I.Ae. D.L. 22]]) was built instead; it had similar configuration, but was structurally different and optimized to available materials.<ref name="Wooden Warriors">von Rauch, Georg and David L. Veres. "Argentina's Wooden Warriors". ''Air Classics'' (Challenge Publications), Volume 19, March 1983, pp. 14–21.</ref>

;Japan
The NA-16-4RW and NA-16-4R inspired the development of the [[Kyushu K10W]] when the [[Imperial Japanese Navy]] instructed Kyushu to develop something similar.<ref name="Arawasi">Starkings, Peter. [http://www.arawasi.jp/ "From American Acorn to Japanese Oak".] ''Arawasi'' (Asahi Process, Tokyo), Issue 7, 2007, pp. 26–31. Retrieved: 8 September 2011.</ref> The resulting aircraft owed little to the NA-16, however Allied Intelligence saw so few examples that the error was not corrected and some drawings show a modified NA-16.<ref name="Arawasi" />

==Variants==
{{refimprove section|date=April 2014}}

[[File:North American BT-9 manual drawing.jpg|thumb|North American BT-9]]

Listing includes aircraft built specifically under NA-16 designation for export, and similar aircraft built for use by the United States armed forces.

;NA-16
:One for [[United States Army Air Corps|United States Army Air Corps (USAAC)]] (trials) developed into '''NA-18''' and '''BT-9''' series.
:powered by [[Wright Whirlwind|Wright R-975 Whirlwind]]
When the North American NA-16 was first conceived, five different roles were intended for the design, designated '''NA-16-1 thru NA-16-5''':<ref>Hagedorn 1997, p. 7.</ref>
;NA-16-1:General purpose two-seat aircraft - which became the '''Harvard I'''<ref>Hagedorn 1997, p. 41.</ref>
;NA-16-2:Two-seat fighter - produced under licence in Australia as the '''[[CAC Wirraway]]'''.<ref name="Smith">Smith 2000, p. 96.</ref>
;NA-16-3:Two-seat light attack bomber. The first aircraft in this category was the retractable gear '''NA-26'''<ref>Hagedorn 1997, pp. 20–22.</ref> which evolved into the '''NA-36''' ('''BC-1'''). The fabric-covered fuselage was replaced by an all-metal monocoque to create the '''NA-44''',<ref>Hagedorn 1997, pp. 37–38.</ref> which provided the basis for a line of light attack bombers whose improvements would result in the '''[[North American T-6 Texan|AT-6]]'''.<ref>Hagedorn 1997, p. 46.</ref>
;NA-16-4:Advanced trainer - became the '''[[North American BT-9|BT-9]]''' for the USAAC and which provided the bulk of early production. The improvement of the BT-9 with a longer metal skinned fuselage as on the NA-44 would create the '''NA-64 (Yale)''' and improved wings would result in the '''BT-14'''. 
;NA-16-5:Single-seat fighter - although this designation was never used, it became the '''NA-50''' for Chile, and later the '''NA-68''', which saw limited USAAF service as the '''[[North American P-64|P-64]]'''.<ref>Hagedorn 1997, pp. 41–42, 51.</ref>

;BT-9 (NA-19)
:42 built for USAAC 	- Minor changes from NA-18, new canopy
:powered by Wright R-975 Whirlwind

;BT-9A (NA-19A)
:40 built for USAAC 	- Armed BT-9 with one cowl gun, one rear flexible gun and modified canopy.
:powered by Wright R-975 Whirlwind

;NA-16-2H (NA-20)
:One built for trials, sold to Honduras (FAH)
:powered by Wright R-975 Whirlwind

;NA-22
:One built for USAAC trials but rejected as severely underpowered. Open cockpits as per early NA-16 and [[Townend ring]] on engine.
:powered by Wright R-760 Whirlwind

;BT-9B  (NA-23)
:117 built for USAAC - Unarmed with fixed rear on canopy.
:powered by Wright R-975 Whirlwind

;BT-9D (NA-23)
:One modified BT-9B for USAAC - BT-14 prototype with new outer wings, Harvard type canopy, lengthened fabric covered fuselage, triangular rudder and detail alterations.
:powered by [[Pratt & Whitney R-985 Wasp Junior]]

;NA-16-3 ''Basic Combat demonstrator'' (NA-26)
:One armed demonstrator and the first variant with retractable undercarriage, eventually sold to [[Royal Canadian Air Force|RCAF]] who modified it with Yale and Harvard parts.
:powered by [[Pratt & Whitney R-1340 Wasp]]

;NA-16-2H  (NA-27)
:One armed demonstrator sold to [[Royal Netherlands Air Force]] - not the same as the previous NA-16-2H.
:powered by Pratt & Whitney R-1340 Wasp

;NJ-1 (NA-28) 	<!--not a typo - the NJ-1 was a precursor to the more advanced SNJ-1-->
:40 built to US Navy specifications, up engined BT-9B as advanced trainer with fixed gear.
:powered by Pratt & Whitney R-1340 Wasp

;BT-9C  (NA-29)
:66 built for USAAC - BT-9A with minor changes.
:powered by Wright R-975 Whirlwind

;Y1BT-10 (NA-29)
:One built for USAAC - BT-9 with larger engine, similar to USN NJ-1 but armed and detail differences in engine installation.
:powered by Pratt & Whitney R-1340 Wasp

;BT-10 (NA-30)
:Cancelled production version of Y1BT-10 for USAAC
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-4M (NA-31)
:138 built for Sweden's [[Flygvapnet]] as '''Sk 14'''/'''Sk 14A'''. '''Sk 14N''' trialled nosewheel for [[SAAB 21]].
:powered by Wright R-975 Whirlwind (Sk 14) or Piaggio P VIIc (Sk 14A)

;NA-16-1A (NA-32)
:One built for [[Royal Australian Air Force]] but rejected in favour of NA-16-2K, fixed landing gear, similar to Y1BT-10.
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-2K (NA-33)
:756 for Royal Australian Air Force in Australia with local improvements as '''[[CAC Wirraway]]'''
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-4P (NA-34)
:29 built for Argentina (Army Aviation) - 1st major export order (previous orders involved licence production).
:powered by Wright R-975 Whirlwind

;NA-16-4R (NA-37)
:One built for [[Imperial Japanese Navy]] as a technology demonstrator '''KXA-1''' with fixed u/c and three-blade prop.
:powered by Pratt & Whitney R-985 Wasp Junior

;NA-16-4 (NA-41)
:35 built for China (RoCAF) - Fixed gear, fabric covered fuselage
:powered by Wright R-975 Whirlwind

;NA-16-2A (NA-42)
:Two built for Honduras (FAH) 
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-1G (NA-43)
:Intended for Brazil (Army) but order cancelled. Was to have been similar to BT-9C
:powered by Wright R-975 Whirlwind

;NA-16-1GV (NA-45)
:Three built for Venezuela (FAV) similar to USAAC NA-36 BC-1 but with round rudder and bomb racks under wing center section.
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-4 (NA-46)
:12 built for [[Brazilian Navy]]
:powered by Wright R-975 Whirlwind

;NA-16-4RW (NA-47)
:One built for Imperial Japanese Navy as a technology demonstrator '''KXA-2''' similar to NA-16-4R but smaller engine.
:powered by Wright R-975 Whirlwind

;NA-16-3C (NA-48)
:15 built for China (RoCAF) - Retractable undercarriage, fabric covered fuselage
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-1E (NA-49/NA-61)
:430 for [[Royal Air Force]] and [[Royal Canadian Air Force]] as the '''Harvard I''' with new canopy and square rudder. Also used by South Africa and Southern Rhodesia.
:powered by Pratt & Whitney R-1340 Wasp

;NA-16-4 (NA-56)
:50 built for China (RoCAF) - Entirely new design with longer metal fuselage, triangular rudder and later T-6 style wing. Basically a BT-14 with the AT-6s R-1340 engine and canopy.
:powered by Pratt & Whitney R-1340 Wasp

;NA-57
:230 improved NA-23s for France as '''NAA 57-P-2''', most captured and used by Germany, some retained by Vichy France.
:powered by Wright R-975 Whirlwind<ref>Parker, Dana T. ''Building Victory: Aircraft Manufacturing in the Los Angeles Area in World War II,'' p. 89, Cypress, CA, 2013. ISBN 978-0-9897906-0-4.</ref>

;NA-16-3 (NA-71)
:Three built for Venezuela (FAV)
:powered by Pratt & Whitney R-1340 Wasp

;I.Ae. D.L. 21
:An [[Argentine|Argentinian]] version incorporating the NA-16-1 fuselage with locally designed wings. Rejected in favour of the [[I.Ae. 22 DL]], an original design from the ''[[Fabrica Militar de Aviones]]'' (FMA).

==Operators==
{{refimprove section|date=April 2014}}

;{{ARG}}
* [[Army Aviation Service]]<ref group=n>Some sources list the NA-16 as having been used by the Argentine Air Force however it was with its predecessor, the ''Army Aviation Service'' which was dissolved in 1945 when the Air Force was created.</ref>
;{{AUS}}
* [[Royal Australian Air Force|Royal Australian Air Force (RAAF)]] (samples for licence production)
;{{BRA}}
* [[Brazilian Navy|Brazilian Navy (Marinha do Brasil)]]
;{{China as ROC}}
* [[Republic of China Air Force|Republic of China Air Force (RoCAF)]]
;{{FRA}}
* [[French Air Force|French Air Force (Armée de l'Air)]]
* [[French Naval Aviation|French Naval Aviation (Aéronavale)]]
* [[Vichy French Air Force]]
;{{flag|Germany|Nazi}}
* [[Luftwaffe]]
;{{HON}}
* [[Honduran Air Force|Honduran Air Force (Fuerza Aérea Hondureña/FAH)]]
;{{flag|Japan|naval}}
* [[Imperial Japanese Navy Air Service|Imperial Japanese Navy Air Service(IJNAS)]] (2 examples for evaluation only)
;{{NLD}}
* [[Royal Netherlands Air Force|Royal Netherlands Air Force (Koninklijke Luchtmacht/KLu)]]
;{{flag|South Africa|1928}}
* [[South African Air Force|South African Air Force (SAAF)]]
;{{flag|Southern Rhodesia}}
* [[Southern Rhodesian Air Force|Southern Rhodesian Air Force (SRAF)]]
;{{SWE}}
* [[Swedish Air Force|Swedish Air Force (Flygvapnet)]]
;{{UK}}
* [[Royal Air Force|Royal Air Force (RAF)]]
;{{flag|United States|1912}}
* [[United States Army Air Corps|United States Army Air Corps (USAAC)]]/[[United States Army Air Forces|United States Army Air Forces (USAAF)]]
* [[United States Navy|United States Navy (USN)]]
;{{VEN}}
* [[Venezuelan Air Force|Venezuelan Air Force (Fuerza Aérea Venezolana/FAV)]]

==Surviving aircraft==
* The only intact surviving example of an American built NA-16 is the NA-16-2A/NA-20 "FAH-21" displayed at the Honduras Air Museum at [[Toncontín International Airport|Toncontín]].<ref>[http://www.airliners.net/photo/Honduras---Air/North-American-NA-42/0602735/M/ Airliners.net Picture of the North American NA-42 aircraft] Accessdate:September 2014</ref>
* A Swedish NA-16-4M (locally designated as Sk 14) was built from an ex-RAAF [[CAC Wirraway]] (s/n A20-223) with additional parts from an ex-RCAF [[North American NA-64 Yale]] and is on display at the [[Swedish Air Force Museum]].<ref>[https://www.flickr.com/photos/36603228@N00/9384436256/ Flickr - Sk 14, North American NA 16-4M, Swedish Air Force Museum, Flygvapenmuseum, Linköping] Accessdate:Sept 2014</ref>
* The [[CAC Wirraway]] (originally NA-16-2K) was first modified to British standards and equipment, then later models diverged further from the NA-16 in minor details such as the fitting of dive brakes etc. The sole surviving CA-1 Wirraway is A20-10, the 8th production Wirraway, effectively a licence built NA-16-2K, and held at the [[Australian National Aviation Museum]] <ref>[http://www.aarg.com.au/cac-wirraway-a20-10.html The Australian National Aviation Museum - CAC CA-1 Wirraway A20-10] Accessdate:Sept 2014</ref>
A total of 10 are on the Australian civil aircraft register.<ref>[http://casa-query.funnelback.com/search/search.cgi?collection=casa_aircraft_register "Search aircraft model: CA-1/CA-3/CA-7/CA-8/CA-16."] ''CASA''. Retrieved: 17 September 2013.</ref> Further examples (in Australia unless noted) are at [[Temora Aviation Museum]], [[Australian National Aviation Museum]], [[Aviation Heritage Museum of Western Australia|Aviation Heritage Museum]], Museum of Victoria, Queensland Air Museum, [[RAAF Museum]] (Stored) and the [[Fantasy of Flight]] (Florida - stored).

==Specifications (NA-16)==
{{Aircraft specs
|ref=<!-- reference -->
|prime units?=<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification,
met(ric) first for all others. You MUST choose a format, or no specifications will show -->imp
<!--
        General characteristics
-->
|genhide=
|crew=two
|capacity=
|length m=
|length ft=27
|length in=7
|length note=
|span m=
|span ft=42
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|airfoil=
|empty weight kg=
|empty weight lb=3,078
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Wright R-975 Whirlwind]]
|eng1 type=air cooled radial
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->400
|eng1 note=
|power original=
|prop blade number=2
|prop name=Hamilton Standard
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=170
|max speed kts=
|max speed note=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range miles=700
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following
specific parameters, remove this parameter-->
|guns=
|bombs=
}}

==See also==
{{Aircontent
||related=
* [[North American BT-9]]<ref>Hagedorn 1997, pp. 15–16.</ref>
* [[North American BC-1]]<ref name="Hagedorn 1997, pp. 20–21"/>
* [[North American A-27]]<ref>Hagedorn 1997, pp. 22–23.</ref>
* [[North American T-6 Texan]]<ref name="Hagedorn 1997, p. 8"/>
* [[North American P-64]]<ref>Hagedorn 1997, pp. 47–48.</ref>
* [[CAC Wirraway]] <ref name="Francillon" />
* [[FMA 21|I.Ae. 21 DL]]<ref name="Wooden Warriors" />
|similar aircraft=
<!--WW2 radial engined low wing monoplane trainers with tandem seating-->
* [[CAC Wackett]]
* [[Curtiss-Wright CW-22|Curtiss-Wright CW-22/SNC]]
* [[Fleet Fort]]
* [[I.Ae. 22 DL|FMA I.Ae. 22 DL]]<ref name="Wooden Warriors" />
* [[Kyushu K10W1]]<ref name="Arawasi" />
* [[Miles Martinet]]
* [[Miles Master]]
* [[VL Pyry]]
* [[Vultee BT-13 Valiant]]
|lists=
* [[List of military aircraft of the United States]]
* [[List of aircraft of World War II]]
* [[North American T-6 Texan variants]]
|see also=
}}

==References==

===Notes===
{{reflist|group=n}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Fletcher, David C. and Doug MacPhail. ''Harvard! the North American Trainers in Canada''. San Josef, British Columbia, Canada: DCF Flying Books, 1990. ISBN 0-9693825-0-2.
* Hagedorn, Dan. ''North American NA-16/AT-6/SNJ (WarbirdTech Volume 11)''. North Branch, Minnesota: Speciality Press, 1997. ISBN 0-933424-76-0.
* MacPhail, Doug and Mikael Östberg. ''Triple Crown BT-9: The ASJA/Saab Sk 14, A Pictorial Essay'' (in English/Swedish). San Josef, British Columbia, Canada: DCF Flying Books, 2003.
* Smith, Peter Charles. ''North American T-6: SNJ, Harvard and Wirraway''. Ramsbury, Marlborough, Wiltshire, UK: The Crowood Press Ltd., 2000. ISBN 1-86126-382-1.
* Starkings, Peter. ''From American Acorn to Japanese Oak - The tale of an unsung Japanese training aircraft with roots extending across the Pacific Ocean''. Arawasi International, Asahi Process, September–December 2007, Issue 7.
* von Rauch, Georg and David L. Veres. ''Argentina's Wooden Warriors''. Air Classics, Challenge Publications, March 1983, Volume 19  Issue 3, pp.&nbsp;14–21.
{{Refend}}

== Further reading ==
* Hagedorn, Dan. ''Texans and Harvards in Latin America''. Staplefield, West Sussex: Air-Britain, 2009. ISBN 978-0-85130-312-3.

==External links==
{{commons category-inline|North American NA-16}}

{{North American Aviation aircraft}}

{{Use dmy dates|date=January 2012}}

{{DEFAULTSORT:North American Na-16}}
[[Category:United States military trainer aircraft 1930–1939]]
[[Category:North American Aviation aircraft|NA-16]]