{{Infobox journal
| title = The British Journal of Criminology
| cover = 
| discipline = [[Criminology]]
| abbreviation = Br. J. Criminol.
| editor = Sandra Walklate
| publisher = [[Oxford University Press]] on behalf of the [[Centre for Crime and Justice Studies]]
| country = United Kingdom
| frequency = Bimonthly
| history = 1960-present
| impact = 1.442
| impact-year = 2014
| website = http://bjc.oxfordjournals.org/
| link1 = http://bjc.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://bjc.oxfordjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 0007-0955
| eISSN = 1464-3529
| OCLC = 605898468
| LCCN = 62052872
| CODEN = BJCDAR
| JSTOR = 00070955
}}
'''''The British Journal of Criminology''''' is a monthly [[peer-reviewed]] [[law journal]] focusing on British [[criminology]]. It is published by [[Oxford University Press]] on behalf of the [[Centre for Crime and Justice Studies]] and its [[editor-in-chief]] is Sandra Walklate ([[University of Liverpool]]).<ref name=About>{{cite web |title=About this journal |work=The British Journal of Criminology |url=http://www.oxfordjournals.org/our_journals/crimin/about.html |publisher=[[Oxford University Press|Oxford Journals]] |accessdate=10 June 2015}}</ref>

== Abstracting and indexing ==
{{columns-list|colwidth=30em|
* [[CSA (database company)|CSA]]
* [[Current Contents]]/Social and Behavioral Sciences
* [[ProQuest|ProQuest databases]]
* [[PsycLIT]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.442, ranking it 15th out of 55 journals in the category "Criminology & Penology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Criminology & Penology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://bjc.oxfordjournals.org/}}

{{DEFAULTSORT:British Journal of Criminology, The}}
[[Category:Bimonthly journals]]
[[Category:British law journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1960]]



{{Law-journal-stub}}