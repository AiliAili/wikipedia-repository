{{good article}}
{{distinguish|Bip Bop}}
{{Infobox single
| Name           = Bop Bop
| Cover          = InnaBopBop2.jpg
| Alt            = Duplicated face of Inna in front of a black backdrop.
| Artist         = [[Inna]] featuring [[Eric Turner (singer)|Eric Turner]]
| Album          = [[Inna (album)|Inna]] and [[Inna (album)|Body and the Sun]]
| Released       = 14 July 2015
| Format         = [[Music download|Digital download]]
| Genre          = [[Dance-pop]]
| Length         = 3:25
| Label          = {{flat list|
*[[Roton (label)|Roton]]
*Empire
}}
| Writer         = {{flat list|
*[[Play & Win|Sebastian Barac]]
*Carl Bjorsell
*[[Play & Win|Marcel Botezan]]
*[[J-Son|Julimar J-Son Santos]]
*[[Thomas Troelsen]]
*[[Eric Turner (singer)|Eric Turner]]
}}
| Producer       = {{flat list|
*Bjorsell
*Troelsen
*Turner
*[[Play & Win]]
}}
| Certification  = 
| Chronology   = [[Inna]] singles
| Last single  = "[[We Wanna]]"<br />(2015)
| This single  = "'''Bop Bop'''"<br />(2015)
| Next single = "[[Yalla (Inna song)|Yalla]]"<br />(2015)
{{Extra chronology
| Artist = [[Eric Turner (singer)|Eric Turner]] singles
| Type = single
| Last single = "[[Dancing in My Head]]"<br />(2012)
| This single = "'''Bop Bop'''"<br />(2015)
| Next single = 
}}
{{Extra album cover
  | Upper caption = Alternative covers
  | Type          = single
  | Cover         = InnaBopBop.jpg
  | Alt           = Inna wearing the officer outfit from the music video while leaning on the words "BOP BOP" placed on a black backdrop.  
  | Lower caption = Promotional cover artwork
  }}
}}
'''"Bop Bop"''' is a song recorded by Romanian singer [[Inna]] for [[Inna (album)|her eponymous and fourth studio album]] (2015) and its Japanese counterpart, ''[[Inna (album)|Body and the Sun]]'' (2015). Featuring the vocals of American recording artist [[Eric Turner (singer)|Eric Turner]], it was made available for [[Music download|digital consumption]] on 14 July 2015 through [[Roton (label)|Roton]] and Empire Music. The recording was written by [[Play & Win|Sebastian Barac]], Carl Bjorsell, [[Play & Win|Marcel Botezan]], [[J-Son|Julimar J-Son Santos]], [[Thomas Troelsen]] and [[Eric Turner (singer)|Eric Turner]], while production was handled by Bjorsell, Troelsen, Turner and [[Play & Win]]. A [[dance-pop]] track, "Bop Bop" was described as a departure from the singer's previous work.

While [[Music journalism|music critics]] were favorable towards both Inna's and Turner's vocal delivery, the single was awarded with "Best Dance-Pop Song" at the 2015 [[Radio România Actualități|Radio România Actualități Awards]] gala. An accompanying music video for the recording—which was heavily aired on Romanian television—premiered on Inna's [[YouTube]] channel on 13 July 2015, with it being shot by Dimitri Caceaune, John Perez and David Gal in [[Bucharest]]. Promoted by various live performances, "Bop Bop" reached number two on native [[Kiss FM (Romania)|Airplay 100]], and position 94 on the [[Polish Society of the Phonographic Industry|Polish Airplay Top 100]].

==Composition==
{{Listen|pos=left|filename=Bop_Bop_(Sample).ogg|title="Bop Bop"|description=A 15-second sample of "Bop Bop", a [[dance-pop]] song featuring a "coherent" production and "antrenant" beats.<ref name="Hitfire"/><ref name="Meltly"/><ref name="Kiss FM"/>|format =[[Ogg]]}}
The recording was written by [[Play & Win|Sebastian Barac]], Carl Bjorsell, [[Play & Win|Marcel Botezan]], [[J-Son|Julimar J-Son Santos]], [[Thomas Troelsen]] and [[Eric Turner (singer)|Eric Turner]], while the production process was handled by Bjorsell, Troelsen, Turner and [[Play & Win]].<ref name="credits">{{cite AV media notes |title=Inna |others=[[Inna]] |year=2015 |type=Liner notes/ CD booklet |publisher=[[Roton (label)|Roton]]|location=[[Bucharest]], Romania. (Barcode: 5948204528423)}}</ref><ref name="credits2">{{cite AV media notes |title=Body and the Sun |others=[[Inna]] |year=2015 |type=Liner notes/ CD booklet |publisher=[[Warner Music|Warner Music Japan]]|location=[[Tokyo]], Japan. (Barcode: 4943674214976)}}</ref> Musically, "Bop Bop" is a [[dance-pop]] track with a "coherent" production and "antrenant" beats.<ref name="Hitfire">{{cite news|url=http://www.hitfire.eu/blog/news/717/videopremiere-inna-feat-eric-turner-mit-bop-bop/|title=Videopremiere: Inna feat. Eric Turner mit "Bop Bop"|language=German|publisher=Hitfire|date=15 July 2015|trans_title=Video premiere: Inna feat. Eric Turner with "Bop Bop"|accessdate=3 November 2016}}</ref><ref name="Meltly">{{cite web | title=Inna isi lanseaza noul album pe 15 octombrie! | trans_title=Inna releases her new studio album on 15 October  | url=http://www.melty.ro/inna-isi-lanseaza-noul-album-pe-15-octombrie-a308.html|language=Romanian | publisher=Melty.ro| accessdate=25 December 2015|archivedate=25 December 2015|archiveurl=https://web.archive.org/web/20151225104923/http://www.melty.ro/inna-isi-lanseaza-noul-album-pe-15-octombrie-a308.html}}</ref><ref name="Kiss FM">{{cite news|url=http://www.kissfm.ro/article/5946/Inna-Bop-bop-o-piesa-care-o-sati-placa-la-nebunie-|title=Inna – Bop Bop", o piesa care o sa-ti placa la nebunie|trans_title="Bop Bop", a track which you will adore|publisher=[[Kiss FM (Romania)|Kiss FM]]|language=Romanian|accessdate=3 November 2016|date=13 July 2015}}</ref> While Turner delivers vocals for the refrain—which almost entirely consists of the song's name—Inna is responsible for the strophes.<ref name="Hitfire"/> Spanish [[LGTB]] website Grupo EGF stated that the rhythm of the recording is "danceable", further explaining that it incorporates a "retro air" and an "interesting mix of sounds".<ref name="LGTB">{{cite news|url=http://www.empresasgayfriendly.com/noticias/bop-bop-aires-lesbicos-en-el-exitoso-nuevo-video-de-inna-000387/|title="Bop Bop", aires lésbicos en el exitoso nuevo video de Inna|trans_title="Bop Bop", Lesbian vibe in Inna's new work|publisher=Grupo EGF|date=17 July 2015|language=Spanish|accessdate=3 November 2016}}</ref> Hitfire saw "Bop Bop" as a departure from Inna's previous work.<ref name="Hitfire"/> Echoing this thought, the singer confessed in an interview with Romanian radio station [[Kiss FM (Romania)|Kiss FM]] that;
<blockquote>
It is a collaboration with one of my friends from the international music industry, Eric Turner, a very talented guy from Sweden, and with the boys from Play & Win. It is very particular. I consider that I went through all stages, from [[Popcorn (music style)|popcorn]] to [[dance music|dance]], [[club music|club]] and [[pop music]], so I considered that I have reached a maturity which allows me to release a song which I like, which makes me move in a club and makes me want to hear it on the radio, and I'm very fulfilled.
</blockquote>

==Reception and accolades==
Upon its release, "Bop Bop" was met with good reviews from [[music journalism|music critics]]. Jaromír Koc of Czech music website Musicserver praised Turner's contribution to the song through his "unusually colored" voice.<ref name="music server">{{cite news|url=http://musicserver.cz/clanek/50938/inna-feat-eric-turner-bop-bop/|title=Inna připravila nové album, na singlu "Bop Bop" vypomáhá i Eric Turner|trans_title=Inna prepares a new album, and a new song, "Bop Bop", with Eric Turner assists|language=Czech|author=Koc, Jaromír|publisher=Musicserver|date=19 July 2015|accessdate=28 October 2016}}</ref> While Italian publication R&B Junk's Olivio Umberto found that Inna changed her "formula" by not collaborating with a [[rapper]] but with another singer,<ref name="R&B Junk">{{cite news|url=http://www.rnbjunk.com/inna-feat-eric-turner-bop-bop-video-testo-traduzione/|title=Inna feat. Eric Turner – Bop Bop {{!}} Video Premiere|publisher=R&B Junk|language=Italian|accessdate=3 November 2016|date=13 July 2015|author=Olivio, Umberto}}</ref> Grupo EGF labelled "Bop Bop" as "contagious" and "mature", further acclaiming Turner's performance.<ref name="LGTB"/> Music website Hitfire wrote that the song's title is "simple", and went on praising the refrain and Inna's vocal delivery.<ref name="Hitfire"/> The singer herself saw the single as "cheerful", "crazy" and "energetic".<ref name="Cancan"/>

Commercially, "Bop Bop" debuted at number 64 on Romania's [[Kiss FM (Romania)|Airplay 100]] on 26 July 2016, remaining her fourth charting song that week alongside "Mai stai" (position 47), "[[Diggy Down]]" (position 61) and "[[We Wanna]]" (position 76).<ref>{{cite news|url=http://www.kissfm.ro/emisiuni/54/Airplay-100.html |title=Airplay 100 – 26 Iulie 2015 |date=26 July 2015 |publisher=[[Kiss FM (Romania)|Kiss FM]] |work=[[Media Forest]] |language=Romanian |accessdate=3 November 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20130426000000/http://www.kissfm.ro/emisiuni/54/Airplay-100.html |archivedate=26 April 2013 |df= }}</ref> Subsequently, the recording moved up 37 places to number 27 on the following week,<ref>{{cite news|url=http://www.kissfm.ro/emisiuni/54/Airplay-100.html |title=Airplay 100 – 2 August 2015 |publisher=[[Kiss FM (Romania)|Kiss FM]] |work=[[Media Forest]] |language=Romanian |date=2 August 2015 |accessdate=3 November 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20130426000000/http://www.kissfm.ro/emisiuni/54/Airplay-100.html |archivedate=26 April 2013 |df= }}</ref> and reached its peak position at number two on 11 October 2015.<ref name="Romania">{{cite news|url=http://www.kissfm.ro/emisiuni/54/Airplay-100.html |title=Airplay 100 – 11 Octombrie 2015 |publisher=[[Kiss FM (Romania)|Kiss FM]] |date=11 October 2015 |work=[[Media Forest]] |language=Romanian |accessdate=3 November 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20130426000000/http://www.kissfm.ro/emisiuni/54/Airplay-100.html |archivedate=26 April 2013 |df= }}</ref> The single  also reached number 94 on the [[Polish Society of the Phonographic Industry|Polish Airplay Top 100]].<ref name="Poland"/> Additionally, "Bop Bop" was awarded with "Best Dance-Pop Song" at the 2015 [[Radio România Actualități|Radio România Actualități Awards]] gala, where it was nominated alongside "Când vremea e rea" (2015) by Sore and "Falava" (2015) by [[Andra (singer)|Andra]].<ref name="rra2016">{{cite web| title = Gala Premiilor Muzicale Radio Romania| url =http://www.romania-actualitati.ro/gala_premiilor_muzicale_radio_romania_editia_a_xiv_a-87992| accessdate = 29 May 2016|publisher=Romania Actualitati|language=Romanian|trans_title=Radio Romania music awards gala|archivedate=26 March 2016|archiveurl=https://web.archive.org/web/20160326080229/http://romania-actualitati.ro/gala_premiilor_muzicale_radio_romania_editia_a_xiv_a-87992}}</ref><ref name="rra20162">{{cite web| title = Premiile Radio Romania 2016 - Lista castigatori| url =http://www.urban.ro/muzica/premiile-radio-romania-2016---lista-castigatori/|author=Scris de Alex|date=21 March 2016 |accessdate = 29 May 2016|publisher=Urban.ro|language=Romanian|trans_title=Radio Romania Awards 2016 - Winners list}}</ref>

==Music video==

===Background and release===
The music video for "Bop Bop" was teased on July 9, 2015, featuring a short sequence of Inna and six fellow backup dancer performing synchronized dance moves on a kind of stage or runway. While the dancers were dressed in black and white outfits, the singer sports a sparkling silver top.<ref>{{cite web|url=https://www.youtube.com/watch?v=U1HF7uLSYmc |title=Inna – Bop Bop (feat. Eric Turner) – Video Teaser |publisher=YouTube |date=9 July 2015 |accessdate=12 July 2015}}</ref> Three additional videos were released during July 2015 in which the track is played for the first time to unspecified citizens of [[Los Angeles]] and Inna's fellow Romanian stars; both videos captured extremely positive input, with some listeners predicting its success.<ref>{{cite web|url=https://www.youtube.com/watch?v=TZBwllVtrJo |title=Inna – Bop Bop – First Listen – Los Angeles |publisher=[[YouTube]] |date=7 July 2015 |accessdate=12 July 2015}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=uh3nD2JVt90 |title=Inna – Bop Bop – First Listen With The Stars – Part 1 |publisher=YouTube |date=9 July 2015 |accessdate=12 July 2015}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=G8af-_bsBog |title=Inna – Bop Bop – First Listen With The Stars – Part 2 |publisher=[[YouTube]] |date=11 July 2015 |accessdate=12 July 2015}}</ref><ref name="adevarul">{{cite news|url=http://adevarul.ro/entertainment/muzica/inna-lansat-single-ul-bop-bop-reactionatvedetele-romania-auditie-piesei-1_55a50bc8f5eaafab2c90e661/index.html|title=Inna a lansat single-ul "Bop Bop". Cum au reactionat vedetele din Romania la prima auditie a piesei|publisher=''[[Adevărul]]''|language=Romanian|accessdate=3 November 2016|author=Dumitru, Bianca|date=14 July 2015|trans_title=Inna has released the single "Bop Bop". See how Romanian celebrities reacted to it after their first hearing}}</ref> The idea behind the visual was based on Inna's desire to create a "fashion" video, for which clothes were procured by international brands Marni, Givenchy, HBA HOOD by Air, Balmain, KTZ, Ashish, Pierre Hardy, and Romanian designers Cristina Savulescu and Madalina Dorobantu from Pas du Tout.<ref name="Cancan">{{cite news|url=http://www.cancan.ro/actualitate/inna-lanseaza-bop-bop-un-super-single-cu-videoclip-realizat-in-colaborare-cu-eric-turner.html|title=Inna lanseaza "Bop Bop", un super single cu videoclip realizat in colaborare cu Eric Turner!|publisher=''Cancan''|date=14 July 2015|accessdate=24 July 2015|language=Romanian|trans_title=Inna releases "Bop Bop", a super single and video in collaboration with Eric Turner!}}</ref> The accompanying clip for the recording finally premiered on Inna's [[YouTube]] channel on 13 July 2015,<ref name="video">{{cite web|url=https://www.youtube.com/watch?v=-eu-cQbPkNI|title=Inna feat. Eric Turner – Bop Bop (Official Video) |publisher=YouTube |date=13 July 2014 |accessdate=24 July 2015}}</ref> being shot by Dimitri Caceaune, John Perez and David Gal in [[Bucharest]], Romania; George Dascalescu served as its [[director of photography]].<ref name="Cancan"/> The clip has no plot.<ref name="Hitfire"/>

===Synopsis===
[[File:Inna-Bop-Bopvideoshot.jpg|thumb|left|215px|Inna (''middle'') and her backup dancers performing a synchronized choreography in the music video for "Bop Bop".<ref name="video"/>]] The video opens with an old-fashioned TV being turned on, and displaying the word "BOP" on its screen. Following this, several photos of women are seen scattered on the floor of a dark-lightened room, which is then followed by Turner appearing to sing the first refrain of the song on the TV's display,<ref name="Hitfire"/> wearing black sunglasses and a silver jacket; two fellow females are watching him. Inna is then shown singing to herself in front of a mirror, and subsequently makes appearance sporting an officer outfit consisting of a black hat and a tie. For the second refrain of "Bop Bop", the singer and her backup dancers are portrayed performing a synchronized choreography on a runaway.

Following this, Inna appears wearing a yellow dress while standing on a stair, with her shadow not moving alike to her. When the track's breakdown is played, Inna is presented lying on a XXL-photo of her face, and she and her backup dancers are particularly shown clapping their hands and performing subtle movements. The visual ends with Inna appearing on the TV's display from the beginning, before it is switched off by a woman previously shown. Scenes interspersed through the main plot portray people performing tribal moves while the video's frame is vertically constrained to just the center of the screen, and two lesbian girls touching their body and picturing themselves.<ref name="video"/>

===Reception and analysis===
While Musicserver was generally positive towards the music video,<ref name="music server"/> [[Tony Sokol]] from American music website KpopStarz called it a "scorcher" and Grupo EGF noticed Lesbian vibe.<ref name="LGTB"/><ref name="KpopStarz">{{cite news|url=http://www.kpopstarz.com/articles/220012/20150714/romanian-pop-star-inna-drops-bop-new-music-video-featuring.htm|title=Romanian Pop Star Inna Drops "Bop Bop", New Music Video Featuring Eric Turner|publisher=KpopStarz|author=Tony Sokol|accessdate=3 November 2016|date=14 July 2015}}</ref> German website Hitfire labelled the outfits used during the clip "interesting", praised the performance of the background dancers, and called the video "recommendable".<ref name="Hitfire"/> Jonathan Currinn, writing in his own music website, described the visual as being "fashion involved", and particularly saw Inna's appearance as sexy and confident. Going on, Currin further likened Turner's appearance on a TV screen to that of [[Pitbull (rapper)|Pitbull]]'s in Inna's "[[Good Time (Inna song)|Good Time]]" (2014) and [[Flo Rida]]'s in [[The Saturdays]]'s "[[Higher (The Saturdays song)|Higher]]" (2010), while being positive to the choreography although stating that it "isn't in depth enough".<ref name="Currinn">{{cite news|url=http://www.criticjonni.com/2015/09/inna-eric-turner-bop-bop.html|title=Inna Featuring Eric Turner – Bop Bop|author=Currinn, Jonathan|date=12 September 2015|accessdate=3 November 2016|publisher=Critic Jonni}}</ref> The music video peaked at number two on [[Media Forest]]'s TV Airplay Chart.<ref name="TV Airplay">{{cite news|url=http://mediaforest.ro/weeklycharts/HistoryWeeklyCharts.aspx|title=Media Forest – Know You Are On Air|publisher=[[Media Forest]]|date=4 October 2015|accessdate=3 November 2016}} ''Note: Select '2015' and 'Week 40. Period: 28-09-15 04-10-15', and then click on 'Songs–TV'. The Romanian and international positions are rendered together by the number of plays before resulting an overall chart.''</ref>

==Live performances and other usage==
"Bop Bop" was set on the track list of concert tours that promoted her album ''Inna'' and its Japanese counterpart ''Body and the Sun'' in Europe and Japan.<ref name="credits"/><ref name="credits2"/> Additionally, Inna sung the song for [[Kiss FM (Romania)|Kiss FM]] and at the Grand Bazaar in [[Istanbul]], Turkey.<ref name="Kiss FM"/><ref>{{cite news|url=https://www.youtube.com/watch?v=9LeAV-ZE8DA|title=Inna – Bop Bop (Grand Bazaar Istanbul – Take Over) {{!}} Exclusive online video|publisher=YouTube|accessdate=3 November 2016|date=20 July 2015}}</ref> The singer also provided a live performance of the recording at the [[World Trade Center Mexico]], where she additionally sung a cover version of [[Justin Bieber]]'s "[[Love Yourself]]" (2015) and a stripped-down version of "[[Endless (Inna song)|Endless]]" (2011),<ref name="mexico">{{cite web| title =Detalles del concierto de Inna en México| url =http://entretenimiento.starmedia.com/musica/detalles-concierto-inna-en-mexico.html| accessdate = 25 August 2016|publisher=Star Media Latinoamerica|language=Spanish|trans_title=Details about Inna's concert in Mexico|date=18 March 2016|author=Orduña, Nancy}}</ref> and opened the [[Untold Festival]] in 2016.<ref>{{cite news|url=http://www.infomusic.ro/2016/08/inna-untold-2016-poze/|title=Inna a deschis Untold Festival 2016|trans_title=Inna has opened the Untold Festival 2016|publisher=Info Music|language=Romanian|accessdate=3 November 2016|date=5 August 2016|author=Tanasă, Raluca}}</ref> For Romanian reality talent show [[Te cunosc de undeva!]], Carmen Simonescu impersonated Inna and delivered a performance of "Bop Bop".<ref>{{cite news|url=http://a1.ro/te-cunosc-de-undeva/video/carmen-simionescu-se-transforma-in-inna-bop-bop-id549998.html|title=Carmen Simonescu i-a dat viata Innei pe scena "Te cunosc de undeva!"|trans_title=Carmen Simonescu impersonated Inna at "Te cunosc de undeva!"|language=Romanian|publisher=[[Antena 1 (Romania)|Antena 1]]|accessdate=3 November 2016|date=28 November 2015}}</ref>

==Track listing==
*'''Digital download'''<ref name="iTunes.ro"/>
# "Bop Bop" (feat. Eric Turner) – 3:25

*'''Official remixes'''<ref name="Remixes EP">{{cite news|url=https://itunes.apple.com/de/album/bop-bop-remixes-feat.-eric/id1031557701|title=Bop Bop (Remixes) [feat. Eric Turner] von Inna auf Apple Music|accessdate=3 November 2016|date=18 August 2015|publisher=iTunes Store}}</ref>
# "Bop Bop" (feat. Eric Turner) [Deepierro Remix] – 3:27
# "Bop Bop" (feat. Eric Turner) [Embody Remix] – 3:24
# "Bop Bop" (feat. Eric Turner) [Global B Remix] – 4:15
# "Bop Bop" (feat. Eric Turner) [House of Titans Remix] – 3:41
# "Bop Bop" (feat. Eric Turner) [Jordan Viper Remix] – 5:14
# "Bop Bop" (feat. Eric Turner) [Shandree Remix] – 4:12

==Charts==

===Weekly charts===
{{col-begin}}
{{col-2}}

{|class="wikitable plainrowheaders sortable" 
!align="left"|Chart (2015)
!align="center"|Peak<br>position
|-
!scope="row" {{singlechart|Poland|94|year=2015|chartid=1804|accessdate=3 November 2016|refname="Poland"}}
|-
!scope="row"|Romania ([[Kiss FM (Romania)|Airplay 100]])<ref name="Romania"/>
|style="text-align:center;"|2
|-
|}
{{col-2}}

===Year-end charts===
{| class="wikitable plainrowheaders sortable"
|-
!Chart (2015)
!Position
|-
!scope="row"|Romania ([[Kiss FM (Romania)|Airplay 100]])<ref>{{cite news|url=http://www.kissfm.ro/emisiuni/54/Airplay-100.html |title=Airplay 100 – 3 Ianuarie 2016 |date=3 January 2016 |publisher=[[Kiss FM (Romania)|Kiss FM]] |work=[[Media Forest]] |accessdate=3 November 2016 |language=Romanian |deadurl=yes |archiveurl=https://web.archive.org/web/20130426000000/http://www.kissfm.ro/emisiuni/54/Airplay-100.html |archivedate=26 April 2013 |df= }}</ref>
| style="text-align:center;"|37
|}
{{col-end}}

==Release history==
{|class="wikitable plainrowheaders unsortable"
!Country
!Date
!Format
!Label
|-
!scope="row" rowspan="1"|France<ref name="iTunes.fr">{{cite news|url=https://itunes.apple.com/fr/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single par Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725215127/https://itunes.apple.com/fr/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25 July 2015}}</ref>
|rowspan="7"|14 July 2015
|rowspan="8"|Digital download
|rowspan="8"|Roton/<br />Empire
|-
!scope="row" rowspan="1"|Germany<ref name="iTunes.de">{{cite news|url=https://itunes.apple.com/de/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single von Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150724122606/https://itunes.apple.com/de/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=24 July 2015}}</ref>
|-
!scope="row" rowspan="1"|Italy<ref name="iTunes.it">{{cite news|url=https://itunes.apple.com/it/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single di Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725223619/https://itunes.apple.com/it/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25 July 2015}}</ref>
|-
!scope="row" rowspan="1"|Netherlands<ref name="iTunes.nl">{{cite news|url=https://itunes.apple.com/nl/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single vaan Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725234227/https://itunes.apple.com/nl/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25 July 2015}}</ref>
|-
!scope="row" rowspan="1"|Romania<ref name="iTunes.ro">{{cite news|url=https://itunes.apple.com/ro/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single de Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725215507/https://itunes.apple.com/ro/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25 July 2015}}</ref>
|-
!scope="row" rowspan="1"|Russia<ref name="iTunes.ru">{{cite news|url=https://itunes.apple.com/ru/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Инна свободных|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725225906/https://itunes.apple.com/ru/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25  July 2015}}</ref>
|-
!scope="row" rowspan="1"|United Kingdom<ref name="iTunes.gb">{{cite news|url=https://itunes.apple.com/gb/album/bop-bop-feat.-eric-turner/id1018761122|title=Bop Bop (feat. Eric Turner) – Single by Inna|date=14 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015|archiveurl=https://web.archive.org/web/20150725215125/https://itunes.apple.com/gb/album/bop-bop-feat.-eric-turner/id1018761122|archivedate=25 July 2015}}</ref>
|-
!scope="row" rowspan="1"|United States<ref name="iTunes.us">{{cite news|url=https://itunes.apple.com/us/album/bop-bop-feat.-eric-turner/id1019976959|title=Bop Bop (feat. Eric Turner) – Single by Inna|date=24 July 2015|publisher=iTunes Store|language=|accessdate=25 July 2015}}</ref>
|rowspan="1"|24 July 2015
|-
|}

==References==
{{reflist|30em}}

==External links==
*[http://www.metrolyrics.com/bop-bop-lyrics-inna.html Full lyrics of this song] at [[MetroLyrics]]
*[https://www.youtube.com/watch?v=-eu-cQbPkNI Official music video] on [[YouTube]]
*[http://innaofficial.com/ Inna's official website]

{{Inna}}

{{DEFAULTSORT:Bop Bop}}
[[Category:2015 singles]]
[[Category:Inna songs]]
[[Category:Song recordings produced by Play & Win]]
[[Category:Romanian songs]]
[[Category:English-language Romanian songs]]
[[Category:2014 songs]]
[[Category:Songs written by Eric Turner (singer)]]
[[Category:Songs written by Thomas Troelsen]]