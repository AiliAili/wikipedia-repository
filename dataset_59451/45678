{{Refimprove|date=April 2009}}
{{Infobox military person
|honorific_prefix=Dr.-Ing h.c.
|name= Ernst Udet
|birth_date= 26 April 1896
|death_date= {{Death date and age|df=y|1941|11|17|1896|4|26}}
|birth_place= [[Frankfurt am Main]], [[German Empire]]
|death_place= [[Berlin]], [[Nazi Germany]]
|placeofburial= [[Invalidenfriedhof Cemetery|Invalidenfriedhof]] in [[Berlin]]
|image= Bundesarchiv Bild 146-1984-112-13, Ernst Udet.jpg
|image_size= 250
|caption= Ernst Udet
|nickname=
|allegiance=
{{plainlist|
*{{flag|German Empire}} (1915-1918)
*{{Flag|Weimar Republic}} (1918-1919)
*{{flag|Nazi Germany}} (1934-1941)
}}
|serviceyears= 1914–1919, 1934–1941
|battles=[[World War I]]
|rank=
{{plainlist|
*''[[Oberleutnant]]'' (German Empire)
*''[[Generaloberst]]'' (Nazi Germany)
}}
|branch=
{{plainlist|
*[[Image:Cross-Pattee-Heraldry.svg|18px|link=]] ''[[Luftstreitkräfte]]'' (1915-1919)
*{{Luftwaffe}} (1934-1941)
}}
|commands= World War I: ''Jasta ''37, ''Jasta'' 4
|unit= World War I: FA 68, FA(A) 206, KEK Habsheim, ''Jastas'' 4, [[Jasta 11|11]], 15, 37
|awards=
{{plainlist|
*[[Iron Cross]]
*[[House Order of Hohenzollern]]
*''[[Pour le Mérite]]''
*[[Pilot/Observer Badge]] in Gold with Diamonds
*[[Knight's Cross of the Iron Cross]]
}}
}}

'''Ernst Udet''' (26 April 1896&nbsp;– 17 November 1941) was a [[Nazi Germany|German]] pilot and [[air force]] general during [[World War II]].

Udet joined the [[Deutsche Luftstreitkräfte|Imperial German Air Service]] at age 19, eventually becoming a notable [[flying ace]] of [[World War I]], scoring 62 confirmed victories by the end of his life. The highest scoring German fighter pilot to survive that war, and the second-highest scoring after [[Manfred von Richthofen]], his commander in the [[Jagdgeschwader 1 (World War 1)|Flying Circus]], Udet rose to become a squadron commander under Richthofen, and later under [[Hermann Göring]].<ref name=Acepilots1>{{cite web|last=Sherman|first=Stephen|title=Ernst Udet - Second Highest German Ace of WWI|url=http://www.acepilots.com/wwi/ger_udet.html|work=acepilots.com|accessdate=24 February 2012}}</ref> Udet spent the 1920s and early 1930s as a [[stunt pilot]], international [[Barnstorming|barnstormer]], light aircraft manufacturer, and [[playboy (lifestyle)|playboy]]. 

In 1933, Udet joined the [[Nazi Party]] and became involved in the early development of the [[Luftwaffe]], where he was appointed director of [[research and development]]. Influential in the adoption of [[dive bombing]] techniques as well as the [[Junkers Ju 87|Stuka]] dive bomber, by 1939 Udet had risen to the post of [[Director-General]] of Equipment for the Luftwaffe. The stress of the position and his distaste for administrative duties led to Udet developing [[alcoholism]]. 

The launch of [[Operation Barbarossa]], combined with issues with the Luftwaffe's needs for equipment outstripping Germany's production capacity and increasingly poor relations with the Nazi Party, caused Udet to commit [[suicide]] on 17 November 1941 by shooting himself in the head.

== Early life ==
Ernst Udet was born on 26 April, 1896, in [[Frankfurt am Main]], [[German Empire]]. Udet grew up in [[Munich]], and was known from his early childhood for his sunny temperament and fascination with [[aviation]]. In his youth he hung out at a nearby [[airplane]] factory and an army airship detachment. In 1909, he helped found the Munich Aero-Club.<ref name="historynet"/> After crashing a [[Glider (aircraft)|glider]] he and a friend constructed, he finally flew in 1913 with a [[test pilot]] in the nearby Otto Works owned by [[Gustav Otto]], which he often visited.<ref name="historynet"/>

== Military career ==
=== World War I ===
[[File:Theresien-Gymnasium Muenchen 1.JPG|thumb|left|Udet attended the Theresien-Gymnasium in Munich.]]
Shortly after the beginning of [[World War I]], Udet attempted to enlist in the [[Imperial German Army]] on 2 August 1914, but at only {{convert|160|cm|ftin|1|abbr=on}} tall he did not then qualify for enlistment.<ref name="historynet">{{cite web|last=Browne|first=O'Brien|title=Ernst Udet: The Rise and Fall of a German World War I Ace|url=http://www.historynet.com/ernst-udet-the-rise-and-fall-of-a-german-world-war-i-ace.htm|accessdate=27 February 2012|date=June 2006}}</ref> Later that month, when the ''Allgemeiner Deutscher Automobil-Club'' appealed for volunteers with [[motorcycles]], Udet applied and was accepted. Udet's father had given him a motorcycle when he had passed his first year examination, and along with four friends, Udet was posted to the [[26th Reserve Division (German Empire)|26. ''Württembergischen'' Reserve Division]] as a "messenger rider." After injuring his shoulder when his motorcycle hit a crater from an [[artillery]] shell explosion, he was sent to a [[military hospital]], and his motorcycle was sent for repairs. When Udet tried to track down the 26th Division, he was unable to find it and decided to serve in the vehicle depot in [[Namur (city)|Namur]]. During this time, he met officers from the [[Chauny]] flying sector, who advised him to transfer as an [[aerial observer]]. However, before he received his orders, the army dispensed with the volunteer motorcyclists, and Udet was sent back to the recruiting officials.<ref name="historynet"/>

Udet tried to return to the fighting, but he was unable to get into either the pilot or aircraft mechanic training the army offered. However, he learned that if he were a trained pilot, he would be immediately accepted into army aviation. Through a family friend, Gustav Otto, owner of the aircraft factory he had hung out around in his youth, Udet received private flight training. This cost him 2,000 marks<ref name="historynet"/> (about $400 in 1915 U.S. dollars) and new bathroom equipment from his father's firm. Udet received his civilian pilot's license at the end of April 1915 and was immediately accepted by the [[Deutsche Luftstreitkräfte|Imperial German Air Service]].<ref name="historynet"/>

==== Artillery ranging ====
Udet at first flew in ''Feld Flieger-Abteilung'' 206 (FFA&nbsp;206)—an observation unit—as an ''Unteroffizier'' (non-commissioned) pilot with observer ''Leutnant'' Justinius.<ref name="historynet"/> He and his observer won the [[Iron Cross]] (2nd class for Udet and 1st class for his lieutenant)for nursing their damaged [[Aviatik B.I]] two-seater back to German lines after a shackle on a wing-cable snapped.<ref name=Acepilots1 />  Justinius had climbed out to hold the wing and balance it rather than landing behind the enemy lines and being captured. The structural failure of the Aviatik that caused Udet and Justinius to go down, and a similar incident in which ''Leutnant'' Winter and ''Vizefeldwebel'' Preiss lost their lives, the Aviatik B was retired from active service. 

Later, Udet was [[court-martialed]] for losing an aircraft in an incident the flying corps considered a result of bad judgement. Overloaded with fuel and bombs, the aircraft stalled after a sharp bank and plunged to the ground. Miraculously, both Udet and Justinius survived with only minor injuries. Udet was placed under arrest in the guardhouse for seven days.<ref name="the English translation of Mein Fliegerleben by Udet.">{{cite book|last=Udet|first=Ernst|title=Ace of the Iron Cross|year=1970|publisher=Ace Books|isbn=9780668051613}}</ref> On his way out of the guardhouse, he was asked to fly ''Leutnant'' Hartmann to observe a bombing raid on [[Belfort]]. A bomb thrown by hand by the ''leutnant'' became stuck in the landing gear, but Udet performed aerobatics and managed to shake it loose.<ref name=Acepilots1 /> As soon as the Air Staff Officer heard about Udet's performance during the incident, he ordered Udet transferred to the fighter command.

==== Fighter pilot ====
Udet was assigned a new [[Fokker]] to fly to his new fighter unit—FFA&nbsp;68—at [[Habsheim]]. Mechanically defective, the plane crashed into a hangar when he took off, and was then given an older Fokker to fly. In this aircraft he experienced his first aerial combat, which almost ended in disaster. While lining up on a French [[Caudron]], Udet found he could not bring himself to fire on another person and was subsequently fired on by the Frenchman. A [[bullet]] grazed his cheek and smashed his flying goggles.<ref name=Acepilots1 /><ref name="historynet"/> Udet survived the encounter but from then on learned to attack aggressively and began scoring victories, downing his first French opponent on 18 March 1916. On that occasion, he had scrambled to attack two French aircraft, instead finding himself faced with a formation of 23 enemy aircraft. He dived from above and behind, giving his [[Fokker E.III]] full throttle, and opened fire on a [[Farman F.40]] from close range.<ref>{{cite book|last=Franks|first=Norman|title=Sharks Among Minnows|year=2002|publisher=Grub Street|isbn=9781902304922}}</ref> Udet pulled away, leaving the flaming bomber trailing smoke, only to see the observer fall from the rear seat of the stricken craft. The victory won Udet the [[Iron Cross]] First Class, later describing it: "The fuselage of the Farman dives down past me like a giant torch... A man, his arms and legs spread out like a frog's, falls past--the observer. At the moment, I don't think of them as human beings. I feel only one thing--victory, triumph, victory."<ref>{{cite book |title=''Early German Aces of World War I'' |page= 49 }}</ref>

That year, FFA&nbsp;68 was renamed ''Kampfeinsitzer Kommando'' Habsheim before becoming [[Jagdstaffel 15|''Jagdstaffel'' 15]] on 28 September 1916. Udet would claim five more victims, before transferring to ''Jasta'' 37 in June 1917. In the first of his victories on 12 October 1916, Udet forced a French [[Breguet Aviation|Breguet]] to land safely in German territory, then landed nearby to prevent its destruction by its crew. The bullet-punctured tires on Udet's Fokker flipped the plane forward onto its top wings and fuselage. Udet and the French pilot eventually shook hands next to the Frenchman's aircraft.<ref>{{cite book |title=''Early German Aces of World War I'' |pages= 86–87 }}</ref> In January 1917, Udet was commissioned as a ''Leutnant der Reserve'' (lieutenant of reserves). The same month, ''Jasta'' 15 re-equipped with the [[Albatros D.III]], a new fighter with twin synchronized [[Maschinengewehr 08]] machine guns.<ref name="historynet"/>

During his service with ''Jasta'' 15, Udet later wrote he had encountered [[Georges Guynemer]], a notable French ace, in single combat at {{convert|5000|m|ft|abbr=on}}. Guynemer, who preferred to hunt enemy planes alone, by this time was the leading French ace with more than 30 victories. Udet saw Guynemer and they circled each other, looking for an opening and testing each other's turning abilities. They were close enough for Udet to read the "''Vieux''" of "''Vieux Charles''" written on Guynemer's [[Spad S.VII]]. The opponents tried every aerobatic trick they knew and Guynemer fired a burst through Udet's upper wing, however maneuvered for advantage. Once Udet had Guynemer in his sights, his machine guns jammed and while pretending to [[dogfight]] he pounded on them with his fists, desperate to unjam them. Guynemer realized his predicament and instead of taking advantage of it, simply waved a farewell and flew away.<ref name="historynet"/> Udet wrote of the fight, "For seconds, I forgot that the man across from me was Guynemer, my enemy. It seems as though I were sparring with an older comrade over our own airfield." Udet felt that Guynemer had spared him because he wanted a fair fight, while others have suggested that the French ace was impressed with Udet's skills and hoped they might meet again on equal terms.

Eventually, every pilot in ''Jasta'' 15 was killed except Udet and his commander, [[Heinrich Gontermann]], who said to Udet: "The bullets fall from the hand of God ... Sooner or later they will hit us." Udet applied for a transfer to [[Jasta 37|''Jasta'' 37]], and Gontermann was killed three months later when the upper wing of his new Fokker Dr. 1 tore off as he was flying it for the first time. Gontermann lingered for 24 hours without awakening and Udet later remarked, "It was a good death." By late November, Udet was a triple ace and ''Jastaführer'', modelling his attacks after those of Guynemer, coming in high out of the sun to pick off the rear aircraft in a squadron before the others knew what was happening. His commander in ''Jasta'' 37, [[Kurt Grasshoff]], selected Udet for command, when Grasshoff was transferred, over more senior men after witnessing one of these attacks.<ref name="historynet"/> Udet's ascension to command on 7 November 1917, was followed six days later by award of the [[Royal House Order of Hohenzollern]].<ref name=Hohenzollern1>{{cite web|title=World War I Military Medals and Decorations - Royal House Order of Hohenzollern (Prussia)|url=http://www.theaerodrome.com/medals/germany/prussia_rhoh.php?pageNum_recipients=8&totalRows_recipients=91#recipients|work=theaerodrome.com|accessdate=27 February 2012}}</ref> Despite his seemingly frivolous nature, drinking late into the night and womanizing lifestyle, Udet proved an excellent squadron commander. He spent many hours coaching new fighter pilots, with an emphasis on marksmanship as being essential for success.<ref name="historynet"/>

==== The Flying Circus ====
Udet's success attracted attention for his skill, earning him an invitation to join the "Flying Circus", [[Jagdgeschwader 1 (World War I)|''Jagdgeschwader'' 1]] (JG&nbsp;1), an elite unit of German fighter aces under the command of [[Manfred von Richthofen]], popularly known as the Red Baron. Richthofen drove up to Udet one day as he was trying to pitch a tent in [[Flanders]] in the rain, pointing out that Udet had 20 kills, Richthofen said, "Then you would actually seem ripe for us. Would you like to?", which Udet accepted. After watching him shoot down an artillery spotter by frontal attack, Richthofen gave Udet command of [[Jagdstaffel 11|''Jasta'' 11]], von Richthofen's former squadron command.<ref name=Acepilots1 /><ref name=Jasta11>{{cite web|title=Jasta 11|url=http://www.theaerodrome.com/services/germany/jasta/jasta11.php|work=theaerodrome.com|accessdate=27 February 2012}}</ref> The group commanded by Richthofen also contained ''Jastas'' 4, 6 and 10.<ref name=Acepilots1 /> Udet's enthusiasm for Richthofen was unbounded, who demanded total loyalty and dedication from his pilots, immediately [[cashiering]] anyone who fell out of line. At the same time, Richthofen treated them with every consideration and when it came time to requisition supplies he traded favors for [[autographed]] photos of himself that read: "Dedicated to my esteemed fighting companion." Udet remarked that because of the signed photographs, " ... sausage and ham never ran out." One night, the squadron invited a captured English flyer for dinner, treating him as a guest. When he excused himself for the bathroom, the Germans secretly watched to see if he would try to escape. On his return the Englishman said, "I would never forgive myself for disappointing such hosts"; the English flyer did escape later from another unit.

Richthofen was killed in April 1918 in France, where Udet was not at the front as he had been sent on leave due to a painful ear infection which he avoided having treated as long as he could. Udet said about Richthofen: "He was the least complicated man I ever knew. Entirely Prussian and the greatest of soldiers." before returning to JG&nbsp;1 against the doctor's advice and remained there to the end of the war, commanding ''Jasta'' 4. While at home, Udet had reacquainted himself with his childhood sweetheart, Eleanor "Lo" Zink. Notified that he had received the ''[[Pour le Mérite]]'', he had one made up in advance so that he could impress her, and painted her name on the side of his [[Albatros Flugzeugwerke|Albatros]] fighters and [[Fokker D.VII|Fokker D VII]]. Also on the tail of his Fokker D VII was the message "''Du doch nicht''" - "Definitely not you."<ref>This account and translation from Stanley M. Ulanoff, the editor of ''Ace of the Iron Cross'', An Ace Book, 1970 - the English translation of ''Mein Fliegerleben'' by Udet. Udet does not mention the dare.</ref> Udet scored 20 victories in August 1918 alone, mainly against British aircraft and would become a national hero with 62 confirmed kills to his credit.

On 29 June 1918, Udet was one of the early fliers to be saved by [[parachuting]] from a disabled aircraft, when he jumped after a clash with a French [[Breguet Aviation|Breguet]]. His harness caught on the rudder and he had to break off the rudder tip to escape.<ref name="historynet"/> His parachute did not open until he was {{convert|250|ft|m|abbr=on}} from the ground, causing him to sprain his ankle on landing. On 28 September 1918, Udet was wounded in the thigh, for which he was still recovering on Armistice Day, November 11, 1918, when the war ended in Germany's defeat.<ref name="historynet"/>

=== Inter-war period ===
[[Image:ErnstUdet-coloured-photo.jpg|thumb|right|230px|Ernst Udet, a recoloured portrait]]
[[Image:Bundesarchiv Bild 183-R15623, Ernst Udet.jpg|thumb|left|200px|Ernst Udet 1928]]

The adventure of Udet's life continued without pause after the war: on his way home from the military hospital, he had to defend himself against a [[Communist]] who wished to rip the medals off his chest. Udet and [[Robert Ritter von Greim|Ritter von Greim]] performed mock dogfights at weekends for the [[POW]] Relief Organization, using surplus aircraft in [[Bavaria]]. He was invited to start the first International Air Service between Germany and Austria, but after the first flight the Entente Commission confiscated his aircraft. Udet married Eleanor "Lo" Zink on February 25, 1920, however the marriage lasted less than three years and they were divorced on February 16, 1923. The marriage is believed to have ended due to Udet having had many affairs. His talents were numerous - among these were [[juggling]], drawing [[cartoon]]s, and party entertainment.

During the inter-war period, Udet was known primarily for his work as a stunt pilot and for playboy-like behavior. He flew for [[Film|movies]] and for [[airshow]]s (e.g. picking a cloth from the ground with his wingtip). He appeared with [[Leni Riefenstahl]] in three films: ''[[Die weiße Hölle vom Piz Palü]]'' (1929), ''[[Stürme über dem Montblanc]]'' (1930), and ''[[S.O.S. Eisberg]]'' (1933). Udet's stunt pilot work in films took him to California. In the October 1933 issue of ''[[New Movie Magazine]]'', there is a photo of [[Carl Laemmle, Jr.]]'s party for Udet in [[Hollywood]]. Laemmle was head of [[Universal Studios]] which made ''SOS Eisberg,'' a US-German co-production. Udet was invited to attend the [[National Air Races]] at [[Cleveland, Ohio]].  In 1935 he appeared in ''Wunder des Fliegens: Der Film eines deutschen Fliegers'' (1935; 79 mins.) directed by [[Heinz Paul]].   His co-star [[Jürgen Ohlsen]], who had previously starred (uncredited) in the extremely popular Nazi propaganda film ''[[Hitlerjunge Quex: Ein Film vom Opfergeist der deutschen Jugend]]'', played a youth who lost his pilot father in World War I and was befriended and encouraged by Udet, his idol.<ref>Rentschler, p. 233, 288.</ref><ref>{{cite web|url=http://www.filmportal.de/film/wunder-des-fliegens_26b079c95ad547e18b921d188682a34e|title=Wunder des Fleigens - |publisher=filmportal.de|language=German|accessdate=17 September 2012}}</ref>

These efforts were good publicity for Udet. An American, William Pohl of [[Milwaukee]], telephoned him with an offer to back an aircraft manufacturing company. Udet Flugzeug was born in a shed in [[Milbertshofen]]. Its intent was to build small aircraft that the general public could fly. It soon ran into trouble with the Entente Commission and transferred its operations to a beehive and chicken coop factory.

The first aeroplane that Udet's company produced was the [[Udet U 2|''U2'']]. Udet took the second model, the U4, to the Wilbur Cup race in [[Buenos Aires]] at the expense of Aero Club Aleman. It was outclassed, and the club wanted him to do cigarette commercials to reimburse them for the expense, but he refused. He was rescued by the Chief of the Argentinian Railways, a man of Swedish descent named Tornquist, who settled the debt.

In 1924, Udet left Udet Flugzeug when they decided to build a four-engine aircraft, which was larger and not for the general population. He and another friend from the war, Angermund, started an exhibition flying enterprise in Germany, which was also successful, but Udet remarked, "In time this too begins to get tiresome. ... We stand in the present, fighting for a living. It isn't always easy. ... But the thoughts wander back to the times when it was worthwhile to fight for your life."

Udet and another wartime comrade-Suchocky—became pilots to an African filming expedition. The cameraman was another veteran, Schneeberger, whom Udet called "Flea," and the guide was Siedentopf, a former East African estate owner. Udet described one incident in Africa in which lions jumped up to claw at the low-flying aircraft, one of them removing a strip of Suchocky's wing surface. Udet engaged in hunting while in Africa.

=== World War II ===
==== Building the ''Luftwaffe'' ====
[[Image:Curtis export hawk II cracow aviation museum.jpg|thumb|right|200px|Udet's [[Curtiss-Wright|Curtiss]] Hawk Export (D-IRIK) as on display in the [[Polish Aviation Museum]].]]

Though not interested in politics, Udet joined the [[Nazi party]] in 1933 when [[Hermann Göring]] promised to buy him two new U.S.-built [[Curtiss Hawk]] II biplanes (export designation of the F11C-2 [[Curtiss F11C Goshawk|Goshawk Helldiver]]). The planes were used for evaluation purposes and thus indirectly influenced the German idea of dive bombing aeroplanes, such as the [[Junkers Ju 87]] (''Stuka'') dive bombers. They were also used for aerobatic shows held during the [[1936 Summer Olympics]]. Udet piloted one of them, which survived the war and is now on display in the [[Polish Aviation Museum]].
 [[Image:Udet Board Bar.JPG|thumb|left|Udets's board bar from his [[Siebel Fh 104|Siebel Fh 104 A-0]] on display in the [[German Museum of Technology (Berlin)|''Deutsches Technikmuseum'' Berlin]].]]

After the trials of the Ju&nbsp;87, a confidential directive issued on 9 June 1936 by [[Field Marshal (Germany)|''Generalfeldmarschall'']] [[Wolfram Freiherr von Richthofen|Wolfram von Richthofen]] called for the cessation of all further Ju&nbsp;87 development, although the Ju&nbsp;87 had been awarded top marks and was about to be accepted.  However, Udet immediately rejected von Richthofen's instructions and Ju&nbsp;87 development continued. Udet became a major proponent of the [[dive bomber]], taking credit for having introduced it to the ''[[Luftwaffe]]''. By 1936 he had, through his political connections, been placed in command of the ''T-Amt'' (the development wing of the ''[[Reichsluftfahrtministerium]]'' of the ''Reich'' Air Ministry. Udet had no real interest in this job, especially the bureaucracy of it, and the pressure led to him developing an addiction to [[alcohol]], drinking large amounts of [[brandy]] and [[cognac]].

In January 1939, Udet visited [[Italian North Africa]] (''Africa Settentrionale Italiana'', or ASI), accompanying [[Marshal of the Air Force|''Maresciallo dell{{'}}Aria'']] (Marshal of the Air Force) [[Italo Balbo]] on a flight, because at the time there were distinct signs of German military and diplomatic co-operation with the Italians.<ref>Kelly, Saul, ''The Lost Oasis'', p. 130</ref> In February 1939 Udet became ''Generalluftzeugmeister'' (''Luftwaffe'' Director-General of Equipment).

When [[World War II]] began, his internal conflicts grew more intense as aircraft production requirements were much more than the German industry could supply, given limited access to raw materials such as [[aluminium]]. Hermann Göring responded to this problem by simply lying about it to [[Adolf Hitler]], and after the ''Luftwaffe''{{'}}s defeat in the [[Battle of Britain]], Göring tried to deflect Hitler's ire by blaming Udet. On 22 June 1941, the launch of [[Operation Barbarossa]], the German invasion of the Soviet Union, drove Udet further into despair. In April and May 1941, Udet had led a German delegation inspecting Soviet aviation industry in accordance with  he [[Molotov–Ribbentrop Pact]]. Udet informed Göring that the Soviet air force and aviation industry were very strong and technically advanced. Göring decided not to report this to Hitler, hoping that a surprise attack would quickly destroy Russia.<ref>«Боевые операции люфтваффе», Москва 2008 г., изд. Яуза-пресс, по «Rise and fall of the German Air Force», Лондон 1948 г., пер. П.Смирнов, ISBN 978-5-9955-0028-5</ref> Udet realized that the upcoming war on Russia might destroy Germany. He tried to explain this to Hitler but, torn between truth and loyalty, suffered a psychological breakdown. Göring kept Udet under control by giving him drugs at drinking parties and hunting trips. Udet's drinking and psychological condition became a problem, and Göring used Udet's dependency to manipulate him.<ref>Who is who in the Third Reich (Кто был кто в Третьем рейхе. Биографический энциклопедический словарь. М., 2003)</ref>

== Death ==
[[Image:Udet Grab.jpg|thumb|right|Ernst Udet's grave in [[Invalidenfriedhof Cemetery]], Berlin]]

On 17 November 1941, Ernst Udet committed [[suicide]] by shooting himself in the head while on the phone with his girlfriend, Inge Bleyle.<ref name="Udet">{{cite web|url= http://www.theaerodrome.com/aces/germany/udet.php|title=Ernst Udet|work=The Aerodrome|accessdate=19 April 2009}}</ref> Udet's suicide was concealed from the public, and at his funeral he was lauded as a hero who had died in flight while testing a new weapon. On their way to attend Udet's funeral, the World War II fighter ace [[Werner Mölders]] died in a plane crash in [[Breslau]], and high ''Luftwaffe'' executive ''[[General der Flieger]]'' [[Helmuth Wilberg]] died in another plane crash near Dresden. Udet was buried next to Manfred von Richthofen in the [[Invalidenfriedhof Cemetery]] in Berlin. Mölders was buried next to Udet.

According to Udet's biography, ''The Fall of an Eagle'', he wrote a [[suicide note]] in red pencil which included: "Ingelein, why have you left me?" and "Iron One, you are responsible for my death." "Ingelein" referred to his girlfriend, Inge Bleyle, and "Iron One" to Hermann Göring. The book ''The Luftwaffe War Diaries'' similarly states that Udet wrote "''Reichsmarschall'', why have you deserted me?" in red on the headboard of his bed. It is possible that an affair Udet had with [[Martha Dodd]], daughter of the U.S. ambassador to Germany and Soviet sympathizer during the 1930s might have had some importance in these events. Records made public in the 1990s confirm Soviet security involvement with Dodd's activities. Evidence indicates that Udet's unhappy relationship with Göring, [[Erhard Milch]], and the Nazi Party in general was the cause of a [[mental breakdown]].<ref name="Udet"/>

==Portrayals==
*[[Carl Zuckmayer]]'s 1946 play ''Des Teufels General'' ("The Devil's General") was a fictional treatment of Udet's final days.
*''[[Des Teufels General]]'' was a 1955 film version of the Zuckmeyer play, with [[Curd Jurgens|Curd Jürgens]] in the title role.
*In the film ''[[Von Richthofen and Brown]]'' (1971), Udet was portrayed by [[Robert La Tourneaux]]
*The character of "Ernst Kessler" in the 1975 film ''[[The Great Waldo Pepper]]'' is clearly based upon Ernst Udet. Kessler was portrayed by actor [[Bo Brundin]]. It also contains dogfighting scenes between a [[Fokker Dr.I]] and a [[Sopwith Camel]].
*In the movie ''[[The Red Baron (2008 film)|The Red Baron]]'', Udet is portrayed by Jiří Laštovka.

==See also==
*[[Udet U 12]]

==Notes==
{{Reflist|30em}}

==References==
*{{cite book | author=[[Barker, Ralph]] | title=The Royal Flying Corps in World War I | publisher= Robinson | year=2002 | isbn=1-84119-470-0}}
*{{cite book | author=Bekker, Cajus | title=The Luftwaffe War Diaries | publisher=Da Capo Press | year=1994 | isbn=0-306-80604-5}}
*{{cite book | author=Herlin, Hans| title=UDET - A Man's Life | publisher=MacDonald| year=1960}}
*{{cite book | author=Kelly, Saul| title=The Lost Oasis: The Desert War and the Hunt for Zerzura | publisher=Westview Press | year=2002 | isbn=0-7195-6162-0}}
*{{cite book | author=[[Knopp, Guido]] | title=Hitlers Krieger | location=München | publisher=Goldmann Verlag | year=2000 | isbn=3-442-15045-0}}
*{{cite book | author=Udet, Ernst| editor=Stanley M. Ulanoff | title=Ace of the Iron Cross | publisher=Arco | year=1981 | isbn=0-668-05163-9}}
*{{cite book|last=Udet|first=Ernst|title=Mein Fliegerleben (My Life of Flying)|date=1935|publisher=Im Deutschen Verlag, Ullstein A.G.|location=Berlin, Germany}}
*{{cite book | author=van Ishoven, Armand | title=The Fall of an Eagle: The Life of Fighter Ace Ernst Udet | publisher=Kimber & Co | year=1979 | isbn=0-7183-0067-X}}
*VanWyngarden, Greg, ''et al.'' (2006) ''Early German Aces of World War I.'' Osprey Publishing. ISBN 1-84176-997-5, ISBN 978-1-84176-997-4.

==External links==
*[http://www.theaerodrome.com/aces/germany/udet.php Ernst Udet page on theaerodrome.com]
*[http://www.histaviation.com/Udet_U-12_Udets_Flamingo.html Udet's U-12 Flamingo]
*[http://www.riefenstahl.org/ Leni's Rising Star] Includes info on the films Udet was in with Leni Riefenstahl. Also has video downloads.
*[http://www.historynet.com/magazines/aviation_history/3032886.html "Ernst Udet: The Rise and Fall of a German World War I Ace"]
*{{Find a Grave|8193}}
*{{youtube|M2CBx7x5GCI|Ernst Udet performs an exhibition precision dead stick loop and side slip landing}}

{{wwi-air}}
{{Recipients of the Pour le Mérite and Knight's Cross}}
{{Generaloberst of the Third Reich}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War I
| portal5=World War II
| commons=y
}}

{{DEFAULTSORT:Udet, Ernst}}
[[Category:1896 births]]
[[Category:1941 deaths]]
[[Category:People from Frankfurt]]
[[Category:People from Hesse-Nassau]]
[[Category:German World War I flying aces]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Recipients of the Pour le Mérite (military class)]]
[[Category:Luftwaffe World War II generals]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:Aviators who committed suicide]]
[[Category:German military personnel who committed suicide]]
[[Category:Nazis who committed suicide by firearm in Germany]]
[[Category:Burials at the Invalids' Cemetery]]
[[Category:Nazis who committed suicide in Berlin]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Recipients of the clasp to the Iron Cross, 1st class]]
[[Category:Nazis who served in World War I]]
[[Category:Colonel generals of the Luftwaffe]]