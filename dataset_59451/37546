{{Infobox military person
|name=Powhatan Beaty
|birth_date= {{Birth date|1837|10|8}}
|death_date= {{Death date and age|1916|12|6|1837|10|8}}
|birth_place=[[Richmond, Virginia]]
|death_place=[[Cincinnati, Ohio]]
|placeofburial= [[Union Baptist Cemetery (Cincinnati, Ohio)|Union Baptist Cemetery]]
|placeofburial_label= Place of burial
|image= Powhatan Beaty.jpg
|image_size= 200px
|caption=First Sergeant Powhatan Beaty, of the [[5th United States Colored Infantry Regiment]], who was awarded the [[Medal of Honor]], for taking command of his company at the [[Battle of Chaffin's Farm]], after all officers had been killed and/or wounded.
|allegiance=
{{plainlist|
* [[United States|United States of America]]
* [[Union (American Civil War)|Union]]
}}
|branch=
{{plainlist|
* [[United States Army]]
* [[Union Army]]
}}
|serviceyears=1863–1865
|rank=[[First Sergeant]]
|unit=[[5th United States Colored Infantry Regiment|5th U.S. Colored Infantry Regiment]]
|battles=
{{plainlist|
* [[American Civil War]]
*: [[Battle of Chaffin's Farm]]
*: [[Battle of Fair Oaks & Darbytown Road]]
}}
|awards=[[Medal of Honor]]
|laterwork=[[Actor]]
}}
'''Powhatan Beaty''' (October 8, 1837 – December 6, 1916) was an [[African American]] soldier and actor. During the [[American Civil War]], he served in the [[Union Army]]'s [[5th United States Colored Infantry Regiment]] throughout the [[Richmond–Petersburg Campaign]]. He received America's highest military decoration, the [[Medal of Honor]], for taking command of his company at the [[Battle of Chaffin's Farm]], after all officers had been killed and/or wounded.

Following the war, he became an orator and actor, appearing in amateur theater productions in his home of [[Cincinnati, Ohio]]. His most well-known stage performance was an 1884 appearance at Ford's Opera House on 9th Street in [[Washington, D.C.]], opposite [[Henrietta Vinton Davis]].

==Early life==
Beaty was born into [[Slavery in the United States|slavery]] on October 8, 1837, in [[Richmond, Virginia]]. He moved to [[Cincinnati, Ohio]], in 1849,<ref name="Hanna16">Hanna, 2002, p. 16</ref> where he received an education.<ref name="Hill61">Hill, 1984, pp. 61–3</ref> He gained his freedom sometime on or before April 19, 1861; the exact date is unknown and may have been before his move to Ohio.<ref name="Hanna16"/> While in school, he developed an interest in [[theater]] and made his public acting debut at a school concert. After leaving school, he was apprenticed to a black [[cabinet maker]]<ref name="Hill61"/> and eventually worked as a [[Turning|turner]].<ref name="Hanna16"/> He continued to study acting privately and received training in the field from several coaches, including [[James E. Murdock]], a retired professional stage actor from [[Philadelphia]].<ref name="Hill61"/>

A year after the outbreak of the Civil War, with the Confederate victory at the [[Battle of Richmond]], [[Kentucky]], on August 30, 1862, rumors of an impending Confederate attack on Cincinnati began to circulate. Richmond was one hundred miles to the south of Cincinnati, and no organized Union troops lay between the two cities. An attack by Confederate [[Colonel (United States)|Colonel]] [[John Hunt Morgan]], who had led his cavalry on a raid behind Union lines in Kentucky the previous month, was also feared.<ref name="Claxton54–55">Claxton and Puls, 2006, pp. 54–5</ref> On September 2, the men of Cincinnati were organized into work units to build fortifications around the city.<ref>Clark, 1969, pp. 5–6</ref>

Although Cincinnati's African Americans were initially pressed into service at [[bayonet]] point, after the appointment of William Dickson as commander of the black troops their treatment improved significantly.<ref>Clark, 1969, pp. 7–9</ref> Dickson promised that they would be treated fairly and kept together as a distinct unit, to be called the [[Black Brigade of Cincinnati|Black Brigade]]. He then allowed them to return home to prepare for military service, with orders to report the next morning for duty.<ref>Clark, 1969, p. 17</ref> About four hundred men were released that day, September 4, and the next morning about seven hundred reported for duty.<ref>Clark, 1969, p. 18</ref> Among those men was Beaty, who served in Company Number 1 of the Brigade's 3rd Regiment.<ref name="Clark28">Clark, 1969, p. 28</ref> Despite the danger of Confederate attack, the unarmed unit<ref name="Clark10">Clark, 1969, p. 10</ref> was assigned to build defenses near the [[Licking River (Kentucky)|Licking River]] in Kentucky,<ref name="Clark19">Clark, 1969, p. 19</ref> far in advance of the Union lines.<ref name="Clark10"/> For the next fifteen days, they cleared forests, constructed forts, magazines and roads, and dug trenches and rifle pits.<ref name="Clark19"/> The brigade was disbanded on September 20, the threat of attack having receded.<ref>Clark, 1969, p. 20</ref>

==United States Colored Troops service==
[[File:127th Ohio Volunteer Infantry.jpg|thumb|left|A portion of the 127th Ohio Volunteer Infantry, later re-designated the 5th USCT, in [[Delaware, Ohio]]]]
By June 1863, Ohio had not yet fielded an African American combat unit, but Ohio blacks were being recruited for service in the regiments of other states.<ref name="Williams133">Williams, 1888, p. 133</ref> Beaty enlisted from Cincinnati on June 7, 1863<ref name="Hanna16"/> for a three-year term of service in the Union Army;<ref name="Claxton54–55"/> he was among a group of men recruited for a [[Massachusetts]] regiment.<ref name="Williams134">Williams, 1888, p. 134</ref> He joined as a [[private (rank)|private]] but was promoted to [[Sergeant#United States|sergeant]] only two days later.<ref name="Hanna16"/> He was placed in charge of a squad of forty-seven other recruits and ordered to report to [[Columbus, Ohio]], from where they would be sent to [[Boston]]. Upon arriving in Columbus on June 15, however, they learned that the Massachusetts regiments were full and unable to accept their service. The Governor of Ohio, [[David Tod]], immediately requested permission from the [[United States Department of War|Department of War]] to form an Ohio regiment of African Americans. Permission was granted, and on June 17, Beaty and his squad became the first members of the 127th Ohio Volunteer Infantry,<ref name="Williams134"/> later re-designated the [[5th United States Colored Troops]].<ref name="Hanna16"/> After three months of recruitment and organization in [[Camp Delaware]], on the [[Olentangy River]] outside of [[Delaware, Ohio]], the unit set out for [[Virginia]].<ref>{{Cite web |url = http://www.itd.nps.gov/cwss/regiments.cfm |title = 5th Regiment, United States Colored Infantry |work = Civil War Soldiers and Sailors System |publisher = National Park Service |accessdate = April 18, 2008}}</ref>

By the Battle of Chaffin's Farm on September 29, 1864, Beaty had risen to the rank of [[first sergeant]] in Company G. His regiment was among a [[Division (military)|division]] of black troops assigned to attack the center of the Confederate defenses at New Market Heights. The defenses consisted of two lines of [[abatis]] and one line of [[palisade]]s manned by [[Brigadier general (United States)|Brigadier General]] [[John Gregg (CSA)|John Gregg]]'s [[Texas Brigade]]. The attack was met with intense Confederate fire and was turned back after reaching a line of abatis. During the retreat, Company G's [[color bearer]] was killed; Beaty returned through about 600 yards of enemy fire to retrieve the flag and return it to the company lines. The regiment had suffered severe casualties in the failed charge.<ref name="Hanna17">Hanna, 2002, p. 17</ref> Of Company G's eight officers and eighty-three enlisted men who entered the battle, only sixteen enlisted men, including Beaty, survived the attack unwounded.<ref name="Hill61"/> With no officers remaining, Beaty took command of the company and led it through a second charge at the Confederate lines. The second attack successfully drove the Confederates from their fortified positions,<ref name="Hanna17"/> at the cost of three more men from Company G.<ref name="Hill61"/> By the end of the battle, over fifty percent of the black division had been killed, captured, or wounded.<ref name="Hanna16"/> For his actions, Beaty was commended on the battlefield by General [[Benjamin Butler (politician)|Benjamin Butler]]<ref name="Hill61"/> and seven months later, on April 6, 1865, awarded the Medal of Honor.<ref name=citation>{{Cite web |publisher = [[United States Army Center of Military History]] |title = Civil War Medal of Honor recipients (A–L) |work = Medal of Honor citations |date = April 27, 2005 |url = http://www.history.army.mil/html/moh/civwaral.html
|accessdate = January 8, 2007}}</ref>

Beaty continued to distinguish himself in the 5th Regiment's further engagements. His actions during the [[Battle of Fair Oaks & Darbytown Road]] in October 1864 earned him a mention in the [[general order]]s to the [[Army of the Potomac]]. The regimental commander, Colonel Giles Shurtleff, twice recommended him for a promotion to [[commissioned officer]]. Nothing came of Colonel Shurtleff's requests, however Beaty did receive a [[Brevet (military)|brevet]] promotion to [[lieutenant]]. By the time he was mustered out of the Army he had participated in thirteen battles and numerous skirmishes.<ref name="Hill61"/>

==Post-war life==
After the war, Beaty returned to Cincinnati and raised his family. His son, [[A. Lee Beaty]], became an [[Ohio General Assembly|Ohio state legislator]] and an assistant [[U.S. District Attorney]] for southern Ohio.<ref name="Hanna17"/> He resumed his career as a turner and pursued amateur acting and [[public speaking]] engagements. He gave public readings for charitable causes and became a well-known [[elocutionist]] among the African American community of Cincinnati. Through the 1870s he acted in local theaters and directed music and drama exhibitions in the city.<ref name="Hill61"/> He wrote a [[Play (theatre)|play]] about a rich southern planter entitled ''Delmar, or Scenes in Southland'', which was performed in January 1881 with himself in the lead role. Set in Kentucky, Mississippi, and Massachusetts, the work covered the end of slavery and transition to freedom for blacks from 1860 to 1875.<ref name="HillHatch82">Hill and Hatch, 2003, pp. 82–3</ref> The privately run play was well received, but Beaty did not engage in self-promotion and it never moved into public theaters.<ref name="Hill61"/>

[[File:Ladydavis1a.jpg|thumb|Henrietta Vinton Davis]]
In January 1884, Beaty was working as an assistant [[engineer]] at the Cincinnati water works when [[Henrietta Vinton Davis]], a prominent African American actress, came to perform in the city. Together, he and Davis put on a large musical and dramatic festival in [[Melodeon Hall]] which proved to be very successful.<ref name="Hill61"/> Included in the show were productions of ''[[Ingomar, the Barbarian]]''<ref name="Hill67">Hill, 1984, pp. 67–69</ref> and [[Robert Montgomery Bird]]'s ''[[The Gladiator (play)|The Gladiator]]'', in which Beaty took the role of [[Spartacus]]. The culmination of the festival was a performance of selected scenes from ''[[Macbeth]]'', with Beaty playing the title role and Davis as Lady Macbeth. Newspapers in both the black and white communities of Cincinnati praised the performances of the two actors, with the ''Commercial'' stating that Beaty "threw himself into his part with masterly energy and power".<ref name="Hill61"/>

The successful festival led to Beaty being invited to play as a principal actor in a [[Washington, D.C.]], [[Shakespeare]]an production organized by Davis. A company including Davis, Beaty, and amateur actors from the D.C. area performed ''[[Richard III (play)|Richard III]]'' almost in entirety, three scenes from ''Macbeth'', and one scene from ''Ingomar, the Barbarian''. Davis, the premier black Shakespearean actress of the time, was the star of the show and Beaty played opposite her as Macbeth, King Henry VI, and Ingomar. The May 7, 1884, production was played in [[Ford's Opera House]] to a full house of more than 1,100 people; among them was [[Frederick Douglass]]. There was some [[heckler|heckling]] during the play, primarily from some of the white attendees, however a reviewer from ''[[The Washington Post]]'' reported that "the earnestness and intelligence of several of the leading performers were such as to command the respect of those most disposed to find cause for laughter in everything that was said or done".<ref name="Hill67"/> Washington newspapers praised the principal actors, but noted that the inexperience of some of the supporting cast was evident. Reviewers for African American newspapers were especially pleased to see such a production in an important venue like Ford's Theater. The ''[[New York Globe]]'' wrote of the performance "[t]hus leap by leap the colored man and woman encroach upon the ground so long held sacred by their white brother and sister".<ref name="Hill67"/>

Beaty continued to tour with Davis and performed a show in [[Philadelphia]] before returning to Cincinnati. He helped form his city's Literary and Dramatic Club and, in 1888, became the organization's drama director.<ref name="HillHatch82"/> He lived out the rest of his life in Cincinnati and died at age seventy-nine on December 6, 1916; he was buried at [[Union Baptist Cemetery (Cincinnati, Ohio)|Union Baptist Cemetery]].<ref name="Hanna17"/>

==Medal of Honor citation==
'''Citation:'''

<blockquote>Took command of his company, all the officers having been killed or wounded, and gallantly led it.<ref name=citation/></blockquote>

==See also==
{{Portal|Biography|United States Army|American Civil War|African American}}
* [[List of American Civil War Medal of Honor recipients: A–F#B|List of American Civil War Medal of Honor recipients: A–F]]
* Melvin Claxton and Mark Puls, ''Uncommon valor : a story of race, patriotism, and glory in the final battles of the Civil War'', (Wiley, 2006) (ISBN 0471468231)

==Notes==
{{Reflist|2}}

==References==
* {{Cite book
 |last = Clark
 |first = Peter H.
 |authorlink = Peter H. Clark
 |title = The Black Brigade of Cincinnati
 |publisher = Arno Press and The New York Times
 |year = 1969
 |location = New York
 |url = http://dbs.ohiohistory.org/africanam/det.cfm?ID=2487
}}
* {{Cite book
 |last = Claxton
 |first = Melvin
 |author2 = Mark Puls
 |title =  Uncommon Valor: A Story of Race, Patriotism, and Glory in the Final Battles of the Civil War 
 |publisher = John Wiley & Sons
 |year = 2006
 |location = Hoboken
 |isbn = 978-0-471-46823-3
}}
* {{Cite book
 |last = Hanna
 |first = Charles W.
 |title = African American Recipients of the Medal of Honor: A Biographical Dictionary, Civil War through Vietnam War
 |publisher = McFarland & Company, Inc., Publishers
 |year = 2002
 |location = Jefferson, North Carolina
 |pages = 16–7
 |isbn = 0-7864-1355-7
}}
* {{Cite book
 |last = Hill
 |first = Errol
 |title = Shakespeare in Sable: A History of Black Shakespearean Actors
 |publisher = [[University of Massachusetts Press]]
 |year = 1984
 |location = Amherst
 |pages = 61–3, 67–9
 |isbn = 0-585-08360-6
}}
* {{Cite book
 |last = Hill
 |first = Errol
 |author2 = James Vernon Hatch
 |title = A History of African American Theatre
 |publisher = Cambridge University Press
 |year = 2003
 |location = Cambridge
 |pages = 82–3
 |isbn = 0-521-62443-6
}}
* {{Cite book
 |last = Williams
 |first = George W.
 |authorlink = George Washington Williams
 |title = A History of the Negro Troops in the War of the Rebellion, 1861–1865
 |publisher = [[Harper & Brothers]]
 |year = 1888
 |location = New York
 |pages = 133–4
 |url = https://books.google.com/books?id=eTAOAAAAIAAJ
}}

==External links==
* {{Find a Grave|8562387|Sgt Powhatan Beaty}}

{{good article}}

{{DEFAULTSORT:Beaty, Powhatan}}
[[Category:1837 births]]
[[Category:1916 deaths]]
[[Category:Male actors from Cincinnati]]
[[Category:American slaves]]
[[Category:African Americans in the Civil War]]
[[Category:African-American male actors]]
[[Category:African-American theatre]]
[[Category:19th-century American male actors]]
[[Category:American male stage actors]]
[[Category:United States Army Medal of Honor recipients]]
[[Category:People from Cincinnati]]
[[Category:People of Ohio in the American Civil War]]
[[Category:Union Army soldiers]]
[[Category:Cincinnati in the American Civil War]]
[[Category:American Civil War recipients of the Medal of Honor]]