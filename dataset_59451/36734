{{good article}}
{{Taxobox
| image = Ascocoryne sarcoides.jpg
| image_width = 220px
| image_caption =
| regnum = [[Fungi]]
| divisio = [[Ascomycota]]
| classis = [[Leotiomycetes]]
| ordo = [[Helotiales]]
| familia = [[Helotiaceae]]
| genus = ''[[Ascocoryne]]''
| species = '''''A. sarcoides'''''
| binomial = ''Ascocoryne sarcoides''
| binomial_authority = ([[Nikolaus Joseph von Jacquin|Jacq.]]) [[James Walton Groves|J.W.Groves]] & [[Doreen E. Wilson|D.E.Wilson]] (1967)
| synonyms = *''Lichen sarcoides'' <small>Jacq. (1781)</small>
*''Coryne sarcoides'' <small>(Jacq.) [[Louis René Tulasne|Tul.]] & [[Charles Tulasne|C.Tul.]] (1865)</small>
*''Bulgaria sarcoides'' <small>(Jacq.) [[James J. Dickson|Dicks.]]</small>
*''Octospora sarcoides'' <small>([[Christian Hendrik Persoon|Pers.]]) [[Samuel Frederick Gray|Gray]] (1821)</small>
*''Pirobasidium sarcoides'' <small>(Jacq.) [[Franz Xaver Rudolf von Höhnel|Höhn]] (1902)</small>
}}

'''''Ascocoryne sarcoides''''' is a species of [[fungus]] in the family [[Helotiaceae]]. The species name is derived from the Greek sarkodes (fleshy). Formerly known as ''Coryne sarcoides'', its [[Taxonomy (biology)|taxonomical]] history has been complicated by the fact that it may adopt both sexual and [[imperfect fungi|asexual forms]]. Colloquially known as '''jelly drops'''<ref name=Hall1985/> or the '''purple jellydisc''',<ref name="urlPurple Jellydisc - Ascocoryne sarcoides | Wild About Britain"/> this common fungus appears as a gelatinous mass of pinkish or purple-colored discs. Distributed widely in North America, Europe and Asia, ''A.&nbsp;sarcoides'' is a [[saprobic]] fungus and grows in clusters on the trunks and branches of a variety of dead woods. [[Field study|Field studies]] suggest that colonization by ''A.&nbsp;sarcoides'' of the [[heartwood]] of [[black spruce]] confers some resistance to further infection by [[wood-decay fungus|rot-causing fungi]]. ''A.&nbsp;sarcoides'' contains the [[antibiotic]] compound ascocorynin, shown in the laboratory to inhibit the growth of several [[Gram-positive]] bacteria.

==Taxonomy==
[[File:Nikolaus Joseph von Jacquin.jpg|thumb|left|Nikolaus Joseph von Jacquin first described ''A.&nbsp;sarcoides'' in 1781]]
The [[Taxonomy (biology)|taxonomical]] history of this fungus has been complicated by the fact that its [[biological life cycle|life cycle]] allows for both an [[imperfect fungi|imperfect]] (making asexual spores, or [[conidia]]) or perfect (making sexual spores) form; at various times authors have assigned names to one or the other form, but these names have often been at odds with the accepted rules of fungal nomenclature. It was originally described in 1781 by the Dutch scientist [[Nikolaus Joseph von Jacquin]] as ''Lichen sarcoides''.<ref name=Jacquin1781/> [[Christian Hendrik Persoon]] called it ''Peziza sarcoides'' in 1801. [[Elias Magnus Fries]], in his 1822 publication ''Systema Mycologicum'',<ref name="urlnzfungi.landcareresearch.co.nz"/> described the imperfect state of the fungus under the name ''Tremella sarcoides''. The genus name ''Coryne'' was first used in 1851 by Bonorden, who proposed ''Coryne sarcoides'' for the imperfect state; in 1865 the Tulasne brothers ([[Charles Tulasne|Charles]] and [[Louis René Tulasne|Louis René]]) used ''Coryne'' to refer to both the perfect and imperfect forms. It was designated the [[type species]] for the genus in a 1931 publication by Clements and Shear.

Several decades later it became apparent that the name ''Coryne sarcoides'' violated the naming conventions imposed by fungal taxonomists—specifically, the species was named after the imperfect state, so in 1967, Groves and Wilson proposed the new genus name ''Ascocoryne'' to accommodate the perfect state.<ref name=Groves1967/> The conidial state of this fungus is ''Coryne dubia'' Persoon ex S.F. Gray (synonymous with ''Pirobasidium sarcoides'' von Hoehnel).<ref name=Quack1982/> The [[specific name (botany)|specific epithet]] is derived from [[Greek language|Greek]] and means "fleshy, flesh-like", from σάρξ (''sarx'', ''sarc''- in compounds), "flesh", and the common adjectival ending -οειδής (-''oeides''), "similar, -like".<ref name="stearn"/>

==Description==
{{Multiple image|direction=vertical|align=right|image1=Ascocoryne sarcoides 70468.jpg|image2=Ascocoryne sarcoides 71810.jpg|width=160|caption1=Close-up of apothecia|caption2=Light microscopy of the spores}}
This fungus is characterized by a [[fruiting body]] (technically an [[apothecia]]) with a pinkish-purple color and more or less gelatinous consistency. The apothecia, typically {{convert|0.5|to|1.5|cm|in|1}} in diameter, start with a roughly spherical shape, then eventually flatten out to become shallowly cup-shaped with a wavy edge and smooth upper surface. The lower surface may be covered with small particles (granular), and the apothecia are either attached directly to the growing surface ([[Sessility (botany)|sessile]]), or have a rudimentary stem.<ref name=Jordan2004/> The apothecia are accompanied by a [[conidium|conidial]] form, where non-sexual spores are generated. The conidial form consists of sporodochia, a cushion-like asexual fruiting body mass consisting of short conidiophores (specialized stalks that bear conidia). The sporodochia are similar in color and consistency to the apothecia but very variable in shape, typically club-, spoon-, or tongue-shaped, and bearing minute, cylindrical, straight or curved conidia.<ref name=Groves1967/> As the fungus matures and the apothecia enlarge and press against each other, the apothecia coalesce to form a gelatinous, irregular mass.<ref name="urlAscocoryne sarcoides"/>  The [[trama (mycology)|flesh]], similar to the appearance of the fungus, is pinkish-purple and gelatinous. The odor and taste of ''A.&nbsp;sarcoides'' are not distinctive.<ref name=Jordan2004/> ''Ascocoryne sarcoides'' is not considered [[edible mushroom|edible]].<ref name=Jordan2004/>

===Microscopic features===
The [[ascospores|spores]] are translucent ([[hyaline]]), smooth, have an ellipsoid shape, with dimensions of 12–16 by 3–5&nbsp;[[micrometre|µm]]. Spores contain one or two oil droplets. The imperfect (conidial) form of the fungus produces smooth, hyaline spores that are 3–3.5 by 1–2&nbsp;µm.<ref name="urlCalifornia Fungi: Ascocoryne sarcoides"/> The [[ascus|asci]] – sexual spore-bearing cells – have a cylindrical shape, with dimensions of 115–125 by 8–10&nbsp;µm. The [[paraphyses]] (sterile filamentous cells interspersed among the asci) are cylindrical with slightly swollen tips, and few branches.<ref name=Jordan2004/>

===Similar species===
''Ascocoryne cylichnium'', another small and gelatinous violet-colored species, has apothecia that are more often cup-shaped, and has larger spores—20–24 by 5.5–6&nbsp;µm.<ref name=Jordan2004/> Because of its resemblance to the [[jelly fungus|jelly fungi]], ''A.&nbsp;sarcoides'' has been mistaken for the basidiomycete species ''[[Auricularia auricula]]'' and ''[[Tremella foliacea]]''. ''T.&nbsp;foliacea'' is larger, brown, and leafy in appearance. ''Auricularia auricula'' is also larger, typically brown, is disc- or ear-shaped, with a ribbed undersurface. Microscopically, ''Tremella foliacea'' and ''Auricularia auricula'' are easily distinguished from ''A.&nbsp;sarcoides'' by the presence of basidia (rather than asci).<ref name="urlCalifornia Fungi: Ascocoryne sarcoides"/>

==Habitat and distribution==
This species has a broad distribution in forested areas of North America and Europe. A [[saprobic]] fungus, it derives nutrients from decaying [[organic matter]], and as such is usually found growing on the stumps and logs of fallen [[deciduous]] trees. However, it is also found on a variety of living trees as well. For example, in Europe it has been found on the stems of living spruce (''[[Picea abies]]'') in Finland,<ref name=Kallio1974/> France,<ref name=Delatour1976/> Great Britain,<ref name=Pawsey1971/> Norway,<ref name=RollHansen1976/> and Germany.<ref name=Zycha1969/>

Other collections sites include Australia,<ref name=Fuhrer1985/> Chile,<ref>Stinson, M., Ezra, D., Hess, W.M., Sears, J., and Strobel, G. (2003). An endophytic Gliocladium sp. of Eucryphia cordifolia producing selective volatile antimicrobial compounds. Plant Science 165, 913–922.</ref> China,<ref name=Zhuang1999/> Cuba,<ref name=Dennis1954/> Iceland,<ref name=Hallgrimsson1990/> Korea,<ref name=Lee2000/> and Taiwan.<ref name=Liou1977/> In [[Hawaii]], it grows on trunks of fallen ''[[Cibotium]]''<ref name=Gilbertson1997/> and ''[[Aleurites]]'' trees.<ref name=Cash1938/> ''A.&nbsp;sarcoides'' occurs most frequently in late summer and autumn.<ref name=Jordan2004/>

==Role in tree decay==
[[File:Ascocoryne sarcoides JPG.jpg|thumb|left|Specimen found in a [[beech]] and [[oak]] forest in [[Belgium]]]]
A number of field studies conducted in the [[Boreal forest of Canada|boreal forest]] region of [[Northern Ontario]] (Canada) showed that ''A.&nbsp;sarcoides'' was found to be frequently associated with various [[deciduous]] and [[coniferous]] tree hosts that had been affected by the fungal disease known as [[heart rot]]; this discovery was noted as unusual, as most fungal tree infections are known to be caused by [[Basidiomycetes]], not [[Ascomycetes]].<ref name=Basham1973/><ref name=Etheridge1970/><ref name=Basham1973b/><ref name=Etheridge1967/> In the case of the commercially valuable tree species black spruce (''[[Picea mariana]]''), it was determined that prior colonization by ''A.&nbsp;sarcoides'' reduces the incidence of subsequent infection by common fungal pathogens, such as ''[[Fomes pini]]'' and ''[[Scytinostroma galactina]]''; furthermore, ''A.&nbsp;sarcoides'' can exist in the wood with no noticeable harmful effects on the host.<ref name=Basham1973/> A similar relationship was shown later to exist with jack pine trees (species ''[[Pinus banksiana]]''), whereby ''A.&nbsp;sarcoides'' inhibited ''[[Peniophora pseudopini]]'', but had little effect on the subsequent growth of ''Fomes pini''.<ref name=Basham1975/> The study also showed that ''A.&nbsp;sarcoides'' is isolated more frequently from defective wood as the age of the tree increases (trees examined in the study were over 80 years old), and that it can infect both uninfected [[heartwood]] as well as previously decayed wood; in the latter case it usually coexists with the causal fungi.

==Bioactive compounds==

[[File:Ascocorynin.svg|thumb|[[Skeletal formula]] of ascocorynin]]
Terphenylquinones are chemical compounds that are widely distributed among the [[Basidiomycetes]] division of fungi. ''Ascocoryne sarcoides'' has been shown to contain a terphenylquinone named ascocorynin—a chemical [[derivative (chemistry)|derivative]] of the compound [[benzoquinone]]. This [[pigment]], when in [[alkaline]] solution, turns a dark [[violet (color)|violet]], similar in color to the fruit bodies of the fungus. Ascocorynin has moderate [[antibiotic]] activity, and was shown in laboratory tests to inhibit the growth of several [[Gram-positive]] bacteria, including the widely distributed food spoilage organism ''[[Bacillus stearothermophilus]]''; however, it has no effect on the growth on [[Gram-negative]] bacteria, nor does it have any [[fungicide|anti-fungal]] activity.<ref name=Quack1982/>

==Volatile Organic Compounds==
In 2008, an isolate of ''A. sarcoides'' was observed to produce a series of volatiles including 6 to 9 carbon alcohols, ketones and alkanes.<ref>{{cite journal | last1 = Strobel | first1 = G.A. | last2 = Knighton | first2 = B. | last3 = Kluck | first3 = K. | last4 = Ren | first4 = Y. | last5 = Livinghouse | first5 = T. | last6 = Griffin | first6 = M. | last7 = Spakowicz | first7 = D. | last8 = Sears | first8 = J. | year = 2008 | title = The production of myco-diesel hydrocarbons and their derivatives by the endophytic fungus Gliocladium roseum (NRRL 50072) | url = | journal = Microbiology | volume = 154 | issue = | pages = 3319–28 | doi=10.1099/mic.0.2008/022186-0 | pmid=18957585}}</ref> This mixture was called "Mycodiesel" because of its similarity to some existing fuel mixtures. The researchers have suggested that this, combined with its ability to digest [[cellulose]], make it a potential source of [[biofuel]].<ref name="Press association">{{Cite news |title=Fungus 'manufactures diesel'|url=http://ukpress.google.com/article/ALeqM5jjnnio78MYmhWnWQRnIW5pF1P7bA |publisher=''[[Press Association]]'' |date=2008-11-04 |accessdate=2008-11-04 }}{{dead link|date=November 2012|bot=Legobot}}</ref> The isolate was originally identified at ''[[Gliocladium roseum]] ''but its taxonomy was later revised to ''Ascococoryne sarcoides''.<ref>{{cite journal | last1 = Griffin | first1 = MA | last2 = Spakowicz | first2 = DJ | last3 = Gianoulis | first3 = TA | last4 = Strobel | first4 = SA | date = Dec 2010 | title = Volatile organic compound production by organisms in the genus Ascocoryne and a re-evaluation of myco-diesel production by NRRL 50072 | url = | journal = Microbiology | volume = 156 | issue = 12| pages = 3814–29 | doi = 10.1099/mic.0.041327-0 | pmid = 20705658 }}</ref> Its genome was sequenced in 2012 in an effort to determine the genetic basis for the production of these volatiles.<ref>{{cite journal | last1 = Gianoulis | first1 = T.A. | last2 = Griffin | first2 = M.A. | last3 = Spakowicz | first3 = D.J. | last4 = Dunican | first4 = B.F. | last5 = Alpha | first5 = C.J. | last6 = Sboner | first6 = A. | last7 = Sismour | first7 = A.M. | last8 = Kodira | first8 = C. | last9 = Egholm | first9 = M. | last10 = Church | first10 = G.M. | display-authors = etal   | year = 2012 | title = Genomic Analysis of the Hydrocarbon-Producing, Cellulolytic, Endophytic Fungus Ascocoryne sarcoides | url = | journal = PLoS Genet | volume = 8 | issue = | page = e1002558 | doi = 10.1371/journal.pgen.1002558 | pmid=22396667 | pmc=3291568}}</ref>

==References==
{{Reflist|colwidth=30em|refs=

<ref name=Basham1973>{{cite journal |doi=10.1139/x73-014 |author=Basham JT. |year=1973 |title=Heart rot of black spruce in Ontario. I. Stem rot, hidden rot, and management considerations |journal=Canadian Journal of Forestry Research |volume=3 |issue=1 |pages=95–104}}</ref>

<ref name=Basham1973b>{{cite journal |author=Basham JT.|year=1973|title=Heart rot of black spruce in Ontario. 2. Mycoflora in defective and normal wood of living trees |journal=Canadian Journal of Botany |volume=51 |issue=7 |pages=1379–92 |doi=10.1139/b73-173}}</ref>

<ref name=Basham1975>{{cite journal |author=Basham JT.|year=1975 |title=Heart rot of Jack Pine in Ontario. IV. Heartwood-inhabiting fungi, their entry and interactions with living tree |journal=Canadian Journal of Forest Research |volume=5 |issue=4 |pages=706–21 |url=http://article.pubs.nrc-cnrc.gc.ca/RPAS/RPViewDoc?_handler_=HandleInitialGet&calyLang=eng&journal=cjfr&volume=5&articleFile=x75-097.pdf |accessdate=2010-01-15 |format=PDF |doi=10.1139/x75-097}}</ref>

<ref name=Cash1938>{{cite journal |author=Cash EK. |year=1938 |title=New records of Hawaiian discomycetes |journal=Mycologia |volume=38 |issue=1|pages=97–107 |jstor=3754484 |doi = 10.2307/3754484}}</ref>

<ref name=Delatour1976>{{cite journal |doi= 10.1051/forest/19760402 |author= Delatour C. |last2= Sylvestre |year=1976 |first2= Gilberte|title=Microflore interne de tissus ligneux de l'epicéa commun sur pied |journal=Annales des Sciences Forestières |volume=33 |issue= 4|pages=199–219 |language=French}}</ref>

<ref name=Dennis1954>{{cite journal |doi=10.2307/4114399 |author=Dennis RWG.|year=1954  |title=Some inoperculate Discomycetes of Tropical America|journal=Kew Bulletin |volume=9 |issue=2 |pages=289–348 |jstor=4114399}}</ref>

<ref name=Etheridge1967>{{cite journal |vauthors=Etheridge DE, Morin LA |year=1967 |title= The microbiological condition of wood of living balsam fir and black spruce in Quebec|journal=Canadian Journal of Botany |volume=45 |issue=7 |pages=1003–10 |url=http://article.pubs.nrc-cnrc.gc.ca/RPAS/RPViewDoc?_handler_=HandleInitialGet&calyLang=eng&journal=cjb&volume=45&articleFile=b67-104.pdf |accessdate=2010-01-15 |doi=10.1139/b67-104}}</ref>

<ref name=Etheridge1970>{{cite journal |author=Etheridge DE.|year=1970 |title=''Ascocoryne sarcoides'' (Jacq. ex Gray) Groves and Wilson and its association with decay of conifers |journal=Fonds Rech. For. Univ. Laval|volume=13 |issue= |pages=19–26}}</ref>

<ref name=Fuhrer1985>{{cite book |author=Fuhrer BA. |title=A Field Companion to Australian Fungi |publisher=Five Mile Press |location=Fitzroy, Vic., Australia |year=1985 |pages=144–45 |isbn=0-86788-063-5}}</ref>

<ref name=Gilbertson1997>{{cite journal |vauthors=Gilbertson RL, Hemmes DE |year=1997 |title=Notes on fungi on Hawaiian tree ferns |journal=Mycotaxon |volume=62 |issue= |pages=465–87}}</ref>

<ref name=Groves1967>{{cite journal |doi=10.2307/1217104 |vauthors=Groves JW, Wilson DE |year=1967 |title=The nomenclatural status of ''Coryne'' |journal=Taxon |volume=16 |issue=1 |pages=35–41 |jstor=1217104}}</ref>

<ref name=Hall1985>{{cite book |vauthors=Hall JH, Cassidy FG |title=Dictionary of American regional English |publisher=Belknap Press of Harvard University Press|location=Cambridge |year=1985 |page=114 |isbn=0-674-20519-7 |url=https://books.google.com/books?id=eEB0YFR2EowC&pg=PA114}}</ref>

<ref name=Hallgrimsson1990>{{cite journal |vauthors=Hallgrimsson H, Gotzsche HF |year=1990 |title=Notes on Ascomycetes. II. Discomycetes |journal=Acta Botanica Islandica |volume=10 |issue= |pages=31–36}}</ref>

<ref name=Jacquin1781>{{cite book |title=Miscellanea austriaca ad botanicum, chemiam et historiam naturalem spectantia |volume=2 |author=Jacquin NJ. |year=1781|page=20|language=Latin}}</ref>

<ref name=Jordan2004>{{cite book |author=Jordan M. |title=The Encyclopedia of Fungi of Britain and Europe |publisher=Frances Lincoln |location=London |year=2004 |page=64 |isbn=0-7112-2378-5 |url=https://books.google.com/books?id=ULhwByKCyEwC&pg=PA63}}</ref>

<ref name=Kallio1974>{{cite journal |vauthors=Kallio T, Tamminen P |year=1974|title=Decay of spruce (''Picea abies'' [L.] Karst.) in the Åland Islands  |journal=Acta Forestalia Fennica |volume=138 |issue= |pages=1–42}}</ref>

<ref name=Lee2000>{{cite journal |vauthors=Lee JB, Kim SC, Oh DC |year=2000 |title=Higher fungal flora of Jeju-do (3): Unrecorded fungi (Ascomycota) |journal=Mycobiology |volume=28 |issue=4 |pages= 232–32|type=Meeting abstract}}</ref>

<ref name=Liou1977>{{cite journal |vauthors=Liou SC, Chen ZC |year=1977 |title=Notes on Taiwan discomycetes. Part I: Pezizales and Helotiales |journal=Tawainia |volume=22 |issue= |pages=29–43}}</ref>

<ref name=Pawsey1971>{{cite journal |author=Pawsey RG. |year=1971 |title=Some recent observations on decay of conifers associated with extraction damage, and on butt rot caused by ''Polyporus sehweinitzii'' and ''Sparassis crispa'' |journal=Quarterly Journal of Forestry |volume=65 |issue= |pages=193–208}}</ref>

<ref name=Quack1982>{{cite journal |doi=10.1016/0031-9422(80)85069-2 |vauthors=Quack W, Scholl H, Budzikiewicz H |year= 1982 |title=Ascorynin, a terphenylquinone from ''Ascocoryne sarcoides'' |journal=Phytochemistry |volume=21 |issue=12 |pages=2921–23}}</ref>

<ref name=RollHansen1976>{{cite journal |vauthors=Roll-Hansen F, Roll-Hansen H |year=1976 |title=Microflora of sound-looking wood in ''Picea abies'' stems |journal=European Journal of Forest Pathology|volume=9 |issue= |pages=308–16}}</ref>

<ref name=stearn>{{cite book|author=Stearn WT. |title=Botanical Latin |edition=2nd annot. and rev. |year=1973 |location=Newton Abbot |publisher=David & Charles |pages=265–266, 278}}</ref>

<ref name="urlAscocoryne sarcoides">{{cite web |url=http://www.messiah.edu/Oakes/fungi_on_wood/jelly%20fungi/species%20pages/Ascocoryne%20sarcoides.htm |author=Emberger G|title=''Ascocoryne sarcoides'' |publisher=Messiah College |work=Fungi on Wood |accessdate=2009-06-18}}</ref>

<ref name="urlCalifornia Fungi: Ascocoryne sarcoides">{{cite web |url=http://www.mykoweb.com/CAF/species/Ascocoryne_sarcoides.html |vauthors=Wood M, Stevens F |title=''Ascocoryne sarcoides'' |publisher=MycoWeb |work=California Fungi |accessdate=2009-06-18| archiveurl= https://web.archive.org/web/20090628135158/http://www.mykoweb.com/CAF/species/Ascocoryne_sarcoides.html| archivedate= 28 June 2009 <!--DASHBot-->| deadurl= no}}</ref>

<ref name="urlPurple Jellydisc - Ascocoryne sarcoides | Wild About Britain">{{cite web |url=http://www.wildaboutbritain.co.uk/purple-jellydisc |title=Purple Jellydisc - ''Ascocoryne sarcoides'' |work=Wild About Britain |accessdate=2009-06-18}}</ref>

<ref name="urlnzfungi.landcareresearch.co.nz">{{cite web |url=http://nzfungi.landcareresearch.co.nz/html/data_article_details.asp?ID=&AID=12445&SynName=Lichen~sarcoides&SID=20704&NAMEPKey=595&from=SN |title=nzfungi.landcareresearch.co.nz |publisher=New Zealand Landcare Research |work=NZFUNGI - New Zealand Fungi (and Bacteria) |accessdate=2010-01-15}}</ref>

<ref name=Zhuang1999>{{cite journal |author= Zhuang W-Y. |year=1999 |title=Discomycetes of tropical China. VI. Additional species from Guangxi |journal=Fungal Diversity |volume=3 |pages=187–96 |url=http://www.fungaldiversity.org/fdp/sfdp/FD_3_187-196.pdf |format=PDF}}</ref>

<ref name=Zycha1969>{{cite journal |doi=10.1007/BF02735875 |vauthors=Zycha H, Dimitri L |year= 1968|title=Ausmaß und Ursache der Kernfäule in einer Fichtenprobefläche in Reinhausen (Niedersachsen) |journal=Forstwiss. Cbl. |volume=87 |issue= 1|pages=331–41 |language=German}}</ref>

}}

==External links==
{{commons}}
*{{IndexFungorum|326593}}

[[Category:Helotiaceae]]
[[Category:Fungi described in 1781]]
[[Category:Fungi of Asia]]
[[Category:Fungi native to Australia]]
[[Category:Fungi naturalized in Australia]]
[[Category:Fungi of North America]]
[[Category:Fungi of Europe]]