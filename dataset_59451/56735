{{Infobox journal
| title = Emu
| cover =
| editor =
| discipline = [[Ornithology]]
| abbreviation = Emu
| publisher = [[CSIRO Publishing]] for the [[Royal Australasian Ornithologists Union]]
| country = Australia
| frequency = Quarterly
| history = 1901-present
| openaccess =
| license =
| impact = 1.895
| impact-year = 2012
| website = http://www.publish.csiro.au/journals/emu
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 1567848
| LCCN =
| CODEN =
| ISSN = 0158-4197
| eISSN = 1448-5540
}}

'''''Emu''''', subtitled ''Austral Ornithology'', is the [[peer review|peer-reviewed]] [[scientific journal]] of the [[Royal Australasian Ornithologists Union]]. The journal was established in 1901 and is the oldest ornithological journal published in Australia.<ref>[http://www.publish.csiro.au/paper/MU972051.htm Marchant, S (1972) A critical history of ''Emu''. ''Emu'' 72: 51-69]</ref> The current [[editor-in-chief]] is [[Kate Buchanan]] ([[Deakin University]]). The journal is published quarterly for the Royal Australasian Ornithologists Union in print and online by [[CSIRO Publishing]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.895, ranking it 4th out of 22 journals in the category "Ornithology".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Ornithology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-08 |work=Web of Science |postscript=.}}</ref>

== See also ==
*[[List of ornithology journals]]

==References==
{{reflist}}

==Further reading==
* {{cite book |author=Robin, Libby |title=The flight of the emu: a hundred years of Australian ornithology, 1901-2001 |publisher=Melbourne University Press |location=Carlton, Vic |year=2001 |pages= |isbn=0-522-84987-3 |oclc= |doi= |accessdate=}}

== External links ==
* {{Official website|http://www.publish.csiro.au/journals/emu}}

[[Category:Journals and magazines relating to birding and ornithology]]
[[Category:Publications established in 1901]]
[[Category:CSIRO Publishing academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Ornithology in Australia]]
[[Category:1901 establishments in Australia]]


{{zoo-journal-stub}}