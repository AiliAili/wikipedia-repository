{{Infobox company
| name             = Fisher Flying Products
| logo             = [[File:Fisher Flying Products Logo.png|200px]]
| type             = Private company
| genre            = 
| foundation       = 1984
| founder          = Mike Fisher and Wayne Ison<ref name="FlightGlobal1984">{{cite web|url = http://www.flightglobal.com/pdfarchive/view/1984/1984%20-%200709.html|title = Fisher FP-202 Super Koala |accessdate = 2009-10-18|last = Flight Global Archive|authorlink = |year = 1984}}</ref>
| location_city    = [[Dorchester, Ontario]]
| location_country = [[Canada]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = Gene and Darlene Jackson-Hanson, owners 1984-2007<ref name="EAA2007">{{cite web|url = http://www.eaa.org/news/2007/2007-05-03_fisher.asp|title = End Of An Era For Fisher Flying Products |accessdate = 2009-10-18|last = [[Experimental Aircraft Association]]|authorlink = |date=May 2007}}</ref><br>Paul Riedlinger, owner 2007-2014.<br>Dave Hertner owner 2014-present<ref name="Hertner">{{cite web|url = http://fisherflying.com/history-fisher-flying/|title = History of Fisher Flying|accessdate = 30 November 2016|author =  Fisher Flying Products|year = 2016}}</ref>
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]
| services         = 
| market cap       = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = [http://www.fisherflying.com/ www.fisherflying.com]
| footnotes        = 
| intl             = 
}}

'''Fisher Flying Products''' is a [[Canada|Canadian]] aircraft manufacturer that produces kits for a wide line of light aircraft. The company's kits all feature wooden construction with [[aircraft fabric]] covering. Many of the designs are reproductions of classic aircraft, such as the company's 80% [[Fisher R-80 Tiger Moth]] that is based upon the [[de Havilland Tiger Moth]].<ref name="KitplanesDec2004">Downey, Julia: ''Kit Aircraft Directory 2005'', Kitplanes, Volume 21, Number 12, December 2004, page 57. Belvoir Publications. ISSN 0891-1851</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', pages 158-162. BAI Communications. ISBN 0-9636409-4-1</ref><ref name="KitplanesDec1998">Kitplanes Staff: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 47. Primedia Publications. IPM 0462012</ref><ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page B-22 Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref><ref name="Fisher">{{cite web|url = http://www.fisherflying.com/index.php?option=com_content&view=article&id=7&Itemid=9|title = History |accessdate = 2009-10-16|last = Fisher Flying Products|authorlink = |year = n.d.}}</ref><ref name="Fisher" /><ref name="AvWeb31Jul2000">{{cite web|url = http://www.avweb.com/news/osh2002/183961-1.html|title =Fisher Tiger Moth Replicas To Let You See Africa Low And Slow |accessdate = 2009-10-18|last = AvWeb Staff|authorlink = |date=July 2000}}</ref>

==History==
Founded by Michael E. Fisher and Wayne Ison in the early 1980s as Lite Flite Inc, Aero Visions and later Fisher Aero Corporation, ownership of Fisher Flying Products was later passed to Gene and Darlene Jackson-Hanson in 1984. The company was originally based in [[South Webster, Ohio]] and later [[Edgeley, North Dakota]], [[United States|USA]]. In 2007 the Jackson-Hansons decided to sell the company and retire.<ref name="FlightGlobal1984" /><ref name="EAA2007" /><ref name="KitplanesDec2004" /><ref name="Aerocrafter" /><ref name="KitplanesDec1998" /><ref name="Cliche" /><ref name="Fisher" /><ref name="Janes8687">Taylor, John WR: ''Janes All the Worlds Aircraft 1986-87'' Page 662, Janes Publishing Company, 1986. ISBN 0-7106-0835-7</ref>

The company was purchased by Paul Riedlinger and moved to [[Woodbridge, Ontario]], [[Canada]]. By early 2009 the new owner had re-established the manufacturing operation and commenced producing kits, starting with the [[Fisher Dakota Hawk|Dakota Hawk]] and the [[Fisher FP-202 Koala|FP-202]]. By late 2009 all kits were once again available.<ref name="Fisher" /><ref name="Kitplanes30Jan09">{{cite web|url = http://www.kitplanes.com/news/news/8603-1.phtml|title =Fisher Flying Reintroduces Two Aircraft |accessdate = 2009-10-18|last = Kitplanes Magazine|authorlink = |date=January 2009}}</ref>

The first two designs the company built were the [[Fisher Flyer]], which incorporated a new fuselage and tail and the existing wings from the [[UFM Easy Riser]] [[hang glider]] and the [[Fisher Barnstormer]], a negative [[Stagger (aviation)|stagger]] [[biplane]]. Plans and kits for the latter design were offered in the early 1980s.<ref name="Janes8283">Taylor, John WR: ''Janes All the Worlds Aircraft 1982-83'' page 642, Janes Publishing Company, 1982. ISBN 0-7106-0748-2</ref>

On 15 January 2014 president Paul Riedlinger announced that the company was for sale.<ref name="Sale15Jan14">{{cite news|url = http://www.fisherflying.com/index.php?option=com_content&view=article&id=117:changes-at-ffp-we-are-going-plans-only&catid=5:media&Itemid=12|title = Changes At FFP - We Are Going Plans Only|accessdate = 15 January 2014|last = Riedlinger|first = Paul|date = 15 January 2014| work = Fisher Flying Products}}</ref> In mid-2014 the company was sold to Dave Hertner, owner of [[Effectus AeroProducts]], and moved to [[Dorchester, Ontario]].<ref name="Hertner"/>

== Aircraft ==
[[File:Fisher FP-101 C-IAPV 03.JPG|thumb|right|[[Fisher FP-101]]]]
[[File:FISHER CELEBRITY C-GDDQ.JPG|thumb|right|[[Fisher Celebrity]] biplane]]
The company currently has 15 different designs available and had over 3,500 aircraft flying in 2007.<ref name="EAA2007" />

{| class="wikitable" align=center style="font-size:90%;"
|-
|+'''Summary of aircraft built by Fisher'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built (as of)
! Type
! Replica of, or inspired by
|-
|align=left| [[Fisher Flyer]]
|align=center| {{avyear|1980}}
|align=center| 1
|align=left| Single seat biplane ultralight
|align=left| [[UFM Easy Riser]]
|-
|align=left| [[Fisher Barnstormer]]
|align=center| {{avyear|1982}}
|align=center| 
|align=left| Single seat negative stagger biplane ultralight
|align=left|
|-
|align=left| [[Fisher Boomerang]]
|align=center| {{avyear|1982}}
|align=center| 
|align=left| Single seat high wing ultralight
|align=left|
|-
|align=left| [[Fisher Culex]]
|align=center| 
|align=center| 
|align=left| Tandem 2-seat twin-engined mid-wing monoplane
|align=left|
|-
|align=left| [[Fisher Mariah]]
|align=center| 
|align=center| 
|align=left| Tandem 2-seat low-wing monoplane
|align=left|
|-
|align=left| [[Fisher FP-101]]
|align=center| {{avyear|1982}}
|align=center| 
|align=left| Single seat high wing ultralight
|align=left| [[Piper J-3 Cub]]
|-
|align=left| [[Fisher FP-202 Koala]]
|align=center| {{avyear|1981}}
|align=center| 325 (2004)
|align=left| Single seat high wing ultralight
|align=left| [[Piper J-3 Cub]]
|-
|align=left| [[Fisher FP-303]]
|align=center| {{avyear|1982}}
|align=center| 200 (2004)
|align=left| Low wing, single seat ultralight
|align=left|
|-
|align=left| [[Fisher FP-202 Koala|Fisher Super Koala]]
|align=center| {{avyear|1983}}
|align=center| 100 (2004)
|align=left| Two seat high wing amateur-built
|align=left| [[Piper J-3 Cub]]
|-
|align=left| [[Fisher FP-404]]
|align=center| {{avyear|1984}}
|align=center| 350 (2004)
|align=left| Single seat biplane amateur-built
|align=left|Single seat version of [[Fisher Classic]]
|-
|align=left| [[Fisher FP-505 Skeeter]]
|align=center| {{avyear|1984}}
|align=center| 50 (2004)
|align=left| Single seat [[parasol wing]] ultralight
|align=left| [[Pietenpol Air Camper]]
|-
|align=left| [[Fisher FP-606 Sky Baby]]
|align=center| {{avyear|1986}}
|align=center| 25 (2004)
|align=left| Single seat high wing ultralight
|align=left| [[Cessna 150]]
|-
|align=left| [[Fisher Classic]]
|align=center| {{avyear|1987}}
|align=center| 150 (2004)
|align=left| Two seat biplane amateur-built
|align=left| Two seat version of [[Fisher FP-404]]
|-
|align=left| [[Fisher Celebrity]]
|align=center| {{avyear|1989}}
|align=center| 30 (2004)
|align=left| Two seat biplane amateur-built
|align=left|
|-
|align=left| [[Fisher Horizon 1]]
|align=center| {{avyear|1990}}
|align=center| 50 (2004)
|align=left| Two seat high wing amateur-built
|align=left| [[Bellanca Citabria]]
|-
|align=left| [[Fisher Horizon 2]]
|align=center| {{avyear|1991}}
|align=center| 30 (2004)
|align=left| Two seat high wing amateur-built
|align=left| [[O-1 Bird Dog]]
|-
|align=left| [[Fisher Dakota Hawk]]
|align=center| {{avyear|1993}}
|align=center| 25 (2004)
|align=left| High wing two seat amateur-built
|align=left|
|-
|align=left| [[Fisher Avenger]]
|align=center| {{avyear|1994}}
|align=center| 50 (2004)
|align=left| Low wing, single seat ultralight
|align=left|
|-
|align=left| [[Fisher R-80 Tiger Moth]]
|align=center| {{avyear|1994}}
|align=center| 6 (2004)
|align=left| Two seat biplane amateur-built
|align=left| [[de Havilland Tiger Moth]]
|-
|align=left| [[Fisher Youngster]]
|align=center| {{avyear|1994}}
|align=center| 10 (2004)
|align=left| Single seat biplane amateur-built
|align=left| [[Bücker Bü 133|Bücker Bü 133 Jungmeister]]
|-
|}

==References==
{{Reflist|2}}

==External links==
{{Commons category|Fisher Flying Products aircraft}}
{{Official website|http://www.fisherflying.com/}}

{{Fisher Flying Products}}

[[Category:Aircraft manufacturers of Canada]]