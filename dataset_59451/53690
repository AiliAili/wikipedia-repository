{{Orphan|date=June 2014}}

{{Infobox company
|name                = Paradigm Partners
|logo        = [[File:Paradigm partners logo.jpg, 160px, Paradigm Partners.jpg]]
|type                = Tax Consulting Service
| foundation        =  2006
|industry            = [[Professional services]]
|location            = [[Houston, TX]], U.S.
|services            = Research & Development Tax Credit<br /><br />State and Local Tax Services<br /><br />Domestic Production Deduction<br /><br />Hiring Incentives & UCM<br /><br />Cost Segregation<br /><br />Patents 
|key_people          = Brian Cameron, <small>Chief Executive Officer</small><br /><br />Saqib Dhanani, <small>[[Juris Doctor|J.D.]], Managing Director</small><br /> <br />Karim Solanji, <small>[[Juris Doctor|J.D.]], Managing Director</small><br /><br />Zee Makhani, <small>Managing Director</small>
| homepage           = [http://www.paradigmlp.com/ www.paradigmlp.com]
}}

'''Paradigm Partners''' is a niche consulting firm specializing in complex international, federal, and state tax and funding incentives for both public and private entities, across a host of industries. The company’s staff consists of intellectual property and tax attorneys, engineers, and PhDs.

== Short History ==

'''Paradigm Partners''' was founded in <strong>2006</strong> and is headquartered in [[Houston, TX]], United States.

== Services ==

Paradigm works with [[CPA firms]] and directly with businesses.

The company's core consulting portfolio includes:
*Global R&D Tax Credit analyses
*Hiring and Location- Based Incentives (WOTC)
*Interest Charge – Domestic International Sales Corporation (IC-DISC) services
*Domestic Production Activities Deduction (DPD) analyses
*Grant and Non-Dilutive Funding advising
*Cost Segregation Studies
*Patent services 
*Tax Controversy and Audit Defense Services

== Current Management ==

*<strong>Brian Cameron</strong>, Chief Executive Officer

Brian Cameron received his [[B.B.A]] in Finance from [[Texas A&M University]]. Because of his proven leadership and management skills, he was offered a high level management position with a $1 billion retailer but led by his entrepreneurial spirit, made the decision to start a multi-line insurance agency and grew it to over $3 million in premiums collected in less than one year. His passion for bringing value add services to corporations and creative sales techniques led Brian to co-found Paradigm Partners, a national tax consulting/ [[engineering]] firm focusing on helping Fortune 1000 companies, [[CPA firms]], and manufacturers of all sizes with niche tax benefits such as the [[Research & Development]] [[Tax Credit]] and Hiring Credits and Incentives. Brian has negotiated several national alliances and formed relationships with many of the Top 100 [[CPA firms]] which has propelled Paradigm Partners to be recognized as a 2011 [[Inc. 500]] company and the 9th fastest growing private company in [[Houston, TX]].

*<strong>Saqib Dhanani</strong>, [[Juris Doctor|J.D.]], Managing Director
Received his B.S. in Computer Science from the University of Houston and his [[Juris Doctor|J.D.]] from South Texas College of Law. While at South Texas College of Law, he was a member of the South Texas Law Review and served as a member of Phi Delta Phi. He brings over six years of software engineering and applications development experience from various Fortune 500 companies, and several years of [[Research & Development]] [[Tax Credit]] and audit experience from national tax consulting firms. Saqib Dhanani also served as a Lead Systems Analyst at [[Unocal Corporation]] (currently [[Chevron Corporation]]), where he researched and analyzed potential compatibility issues with proprietary company software. In addition, he has worked with Dynegy, Nalco-[[Exxon]] Energy Chemicals, and other oil and gas companies to develop software applications and implement server hardware and software solutions. Currently, Saqib Dhanani is spearheading the audit defense division at Paradigm Partners, where he manages a team of Tax Controversy Attorneys. Saqib Dhanani is licensed to practice before the United States Tax Court and before all state courts of Texas.

*<strong>Karim Solanji</strong>, [[Juris Doctor|J.D.]], Managing Director
Karim Solanji received his [[Juris Doctor|J.D.]] from South Texas College of Law where he was a member of the South Texas Law Review. He began his career working for major investment banks including Morgan Stanley and Merrill Lynch. During law school, Karim Solanji worked as a Judicial Legal Assistant to the Honorable Judge Jeff Bohm of the U.S. Federal Bankruptcy Court. He also spent two years practicing corporate law with a highly respected mid-size firm. Upon graduation from law school, Karim Solanji joined a national tax consulting firm specializing in the [[Research & Development]] [[Tax Credit]]. During his experience there, he worked with a variety of clients including software development firms, medical technology companies, manufacturing companies, and engineering companies. Karim Solanji is licensed to practice before the U.S. Tax Court and before all state courts of [[Texas]].

*<strong>Zee Makhani</strong>, Managing Director

Zee Makhani received his [[B.S.]] in Electrical and Industrial Engineering from [[Cornell University]]. Zee Makhani brings over ten years of R&D experience. Currently, Zee Makhani is leading the technology efforts within <strong>Paradigm Partners</strong> where he has developed an innovative and unique hiring incentive solution called <ref>{{cite news| url=http://www.paradigmlp.com/services/hiring-location-incentives/ | work=ParadigmLp | title=HIREtech: A Paradigm Hiring Solution}}HireTech</ref> which allows <strong>Paradigm Partners</strong> and its Clients to maximize their credits and provides a very efficient and user friendly process and environment. Prior to that, Zee Makhani was involved in training and supporting Paradigm’s sales force where he was instrumental in building new relationships and obtaining new clients for Paradigm Partners. Before getting involved in business development, Zee Makhani conducted hundreds of [[Research & Development]] [[Tax Credit]] studies, generating millions in tax credits for a wide variety of clients, primarily: [[engineering]] firms, [[manufacturing]] companies, environmental and recycling firms, chemical engineering firms, [[food processing]] industry. Zee Makhani is CPE certified by the [[National Association of State Boards of Accountancy]] on IRC Section 41 (R&D Tax Credits) and other programs <strong>Paradigm Partners</strong> specializes in. Prior to Paradigm Partners, Zee Makhani headed the operations department for a wireless technology [[startup company]] for over three years as a managing director, and was involved in all aspects of the company’s engineering and logistical operations. Before that, he worked at [[Advanced Micro Devices]] ([[AMD]]) for over three years as a design engineer, and then later as a technical product manager, where he worked closely with the engineering and manufacturing department to ensure product and client specifications were met.

== Locations ==

<strong>Paradigm Partners</strong> is headquartered in [[Houston]], [[Texas|TX]], while also maintaining offices in the following locations:

* Melbourne, Australia
* Sydney, Australia
* Mississauga, Ontario, Canada

== Awards ==

* Inc 500 – America's Fastest Growing Companies 2011 <ref>{{cite news| url=http://www.inc.com/profile/paradigm-partners | work=Inc 500 | title=Inc 500 – America's Fastest Growing Companies 2011}}</ref>
* Houston Business Journal's 2011 Fast 100 - Rank 14 <ref>{{cite news| url=http://www.bizjournals.com/houston/print-edition/2011/09/23/inside-scoop-on-houstons-fast-100.html?page=all | work=BizJournals | title=Houston Business Journal's 2011 Fast 100 - Rank 14}}</ref>
* [[Subway (restaurant)|Subway]] 2013 Rookie Vendor of the Year <ref>{{cite news| url=http://news.silobreaker.com/paradigm-partners-named-subway-rookie-vendor-of-the-year-5_2267068113184882832 | work=SiloBreaker | title=2013 Rookie Vendor of the Year}}</ref>

== Notes and references ==
{{reflist}}

==External links==
*[http://www.paradigmlp.com/ Official web site]
*[http://www.investinamericasfuture.org R&D Credit Coalition]

[[Category:Tax credits]]
[[Category:Research and development in the United States]]
[[Category:Accounting firms of the United States]]
[[Category:Companies based in Houston]]