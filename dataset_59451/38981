{{Infobox Single 
| Name           = Burn the Bastards/Burn the Beat
| Cover          = The KLF - Burn the Beat.jpg
| Artist         = [[The KLF]]
| Border         = yes
| from Album     = [[Who Killed The JAMs?]] by [[The Justified Ancients of Mu Mu]]
| Released       = 5 March 1988 <small>(''Burn the Beat'')</small>
18 April 1988 <small>(''Burn the Bastards'')</small>
| Format         = [[Gramophone record|7"]], [[12-inch single|12"]]
| Recorded       = [[Trancentral]]
| Genre          = [[House music|House]]
| Length         = 4:54 <small>(''Burn the Beat (Club Mix)'')</small>
4:07 <small>(''Burn the Bastards (LP edit)'')</small>
| Label          = [[KLF Communications]] <small>(UK)</small>
| Writer         = 
| Producer       = [[Bill Drummond|Drummond]]/[[Jimmy Cauty|Cauty]]
| Chart position = 
| Misc           = 
{{Extra chronology
| Artist = [[Bill Drummond|Drummond]] & [[Jimmy Cauty|Cauty]]
| Type = singles
| Last single ="[[Down Town]]"<br/>(1987)
| This single ="'''Burn the Bastards'''"/"'''Burn the Beat'''"<br/>(1988)
| Next single = "[[Doctorin' the Tardis]]"<br/>(1988)
}}
}}
"'''Burn the Bastards'''" is a 1988 song by [[Bill Drummond]] and [[Jimmy Cauty]] as [[The Justified Ancients of Mu Mu]] (The JAMs), from their second, and final before changing names, album ''[[Who Killed The JAMs?]]''.  The "bastards" of the title are copies of The JAMs first album, ''[[1987 (What the Fuck Is Going On?)]]'', which Drummond and Cauty burnt on a bonfire in a [[Sweden|Swedish]] field after a copyright dispute with the Swedish pop group [[ABBA]]. The song (which is based upon [[Sly and the Family Stone|Sly and the Family Stone's]] "[[Dance to the Music (song)|Dance to the Music]]") was released as a single, along with a separate single of remixes titled "'''Burn the Beat'''".  Both singles were credited to [[The KLF]], marking a change of name and with it a change of musical genre, from The JAMs' sample-fuelled political hip-hop to The KLF's upbeat and uptempo [[house music]].

==Conception==
===Background===
Early in 1987, [[Jimmy Cauty]] and [[Bill Drummond]] formed a musical outfit, The Justified Ancients of Mu Mu (The JAMs), later to also be known as [[The KLF|The Timelords]] and, more famously, [[The KLF]]. The JAMs deliberately invited controversy by spending a year producing incendiary electronic music that was built around plagiarised [[sampling (music)|samples]] of other artists, underpinned by [[beatbox]] rhythms and political [[rapping|raps]].  The song "Burn the Bastards", which was the duo's final single in this mould, was inspired in part by the legal backlash of their provocative output.  

Their debut album, ''[[1987 (What the Fuck Is Going On?)]]'', had been investigated by the [[Mechanical-Copyright Protection Society]], who in August 1987 ordered The JAMs to recall and destroy all unsold copies of ''1987'',<ref>See for example Davage, I., a letter from the [[Mechanical Copyright Protection Society|MCPS]] to [[The JAMs]], reproduced in "The KLF 1987 Completeist List" [sic], an insert supplied with the album ''Who Killed The JAMs?'', [[KLF Communications]] JAMS LP2, 1988.</ref> for its illegal use of extensive samples from [[ABBA|ABBA's]] "[[Dancing Queen]]".  The JAMs journeyed to Sweden—with their unsold LPs and an ''[[NME]]'' journalist in tow—in an attempt to negotiate with ABBA.<ref>Brown, J., [http://www.libraryofmu.net/display-resource.php?id=44 "Thank you for the music"], ''[[New Musical Express]]'', 19 August 1987.</ref>  When this failed, The JAMs made a bonfire in the Swedish countryside and burnt the LPs.  Back in the UK, they continued with their plagiaristic productions, which culminated with a second LP, ''Who Killed The JAMs?''.  Its sleeve depicts the ''1987'' bonfire,<ref>Sleeve and labels, ''Who Killed The JAMs?'', [[KLF Communications]] JAMS LP2, 1988.</ref> and it contains "Burn the Bastards", a sample-heavy celebration of the fire set to [[house music]]. [[Ritual]]istic burnings became a recurring aspect of Drummond and Cauty's work, including the burning of a 60-ft (18-m) [[wicker man]] during the 1991 [[Midsummer|summer solstice]] (''[[The Rites of Mu]]''), and, as the [[K Foundation]] in 1995, their burning of [[pound sterling|£]]1 million.

===Release===
On 5 March 1988, Drummond and Cauty released [[The KLF|The KLF's]] debut single "Burn the Beat", an instrumental house music version of "Burn the Bastards", on their own [[KLF Communications]] label. The single also featured instrumental remixes of other tracks from ''Who Killed The JAMs?''.  All 5,000 pressed copies of the single—catalogue number JAMS 26T—were exported. On 18 April 1988, another single, "Burn the Bastards", was released in the UK, to fill the hitherto overlooked catalogue number KLF 002.<ref name="info">[[Bill Drummond|Drummond, B.]], [http://www.libraryofmu.net/display-resource.php?id=502 "Info Sheet One"], [[KLF Communications]] newsletter, 10 March 1988.</ref> This single, also by The KLF, featured the LP version of "Burn the Bastards" alongside another instrumental version, "Burn the Beat (Club Mix)".<ref name="discog">Longmire, Ernie et al. (2005). [http://www.klf.de/discography/ KLF discography] {{webarchive |url=https://web.archive.org/web/20080111071100/http://www.klf.de/discography/ |date=January 11, 2008 }} Compiled by Ernie Longmire, this has been the authoritative KLF discography on the internet for some 10 years or more and has been the subject of long-term scrutiny and peer review by KLF fans and collectors. It is now maintained by the fan site klf.de.</ref>  The single releases marked a change in direction of Drummond and Cauty's music, to an upbeat and uptempo house music tone.  Indeed, the record label of "Burn the Bastards" stated, "This is a transition record".<ref>Label notes, "Burn the Bastards", [[KLF Communications]] JAMS 26, 1988.</ref>  Neither "Burn the Bastards" nor "Burn the Beat" entered the UK Singles Chart, although the release peaked at number 15 in the UK Indie Singles Chart.

==Composition==
{{listen 
 | filename     = The JAMs - Burn the Bastards (excerpt).ogg
 | title        = "Burn the Bastards"
 | description  = Drummond sings "Throw them on, and watch the bastards burn", accompanied by the "Mu Mu!" sample.  [[The Jackson Five]] and [[Michael Jackson|Michael Jackson's]] "[[Bad (Michael Jackson song)|Bad]]" are used extensively.
 | format       = 
}}
"Burn the Bastards" is a celebratory house music song based upon [[Sly and the Family Stone|Sly Stone's]] "[[Dance to the Music (song)|Dance to the Music]]":<ref name="Sounds">''Who Killed The JAMs?'' review, ''[[Sounds magazine|Sounds]]'' Magazine, 13 February 1988.</ref> a trumpet [[break (music)|break]] and drum line are sampled, and the lyrical structure of that song is also mirrored. Whereas "Dance to the Music" vocally introduces the instruments used, so "Burn the Bastards" has Drummond sing of The JAMs' methods, such as "All we need is a beatbox, for people who only need a beat".  The choral line "Dance to the music" is modified to "JAMs have a party".  

Referring to the fate of the ''1987'' LPs, Drummond sings "Build a fire, stoke it good, throw them on, and watch the bastards burn", accompanied by a stark, [[ring modulation|ring modulated]] chorus, "Mu Mu!".  A later portion of the lyrics alludes to [[New Year's Eve]] 1987: "Five to twelve, almost gone.  1987, what the fuck have we done?".

A driving [[time signature|4/4]] rhythm and a sampled [[Roland TB-303]] loop provide [[acid house]] overtones.  These elements are brought further to the fore in "Burn the Beat", which  dispenses with Drummond's vocals.

Most of The KLF's work was highly [[self-reference|self-referential]]: lyrics were usually  enigmatic narratives of The KLF's real and fictional exploits, and vocal samples were re-used in a variety of musical contexts.  The signatory "Mu Mu!" refrain, which first appeared on this song, recurred throughout the duo's music, including "[[What Time Is Love?|What Time Is Love? (Live at Trancentral)]]" (1990), "[[Last Train to Trancentral|Last Train to Trancentral (Live from the Lost Continent)]]" and "[[America: What Time Is Love?]]" (1991), and "[[Fuck the Millennium]]" (1997).

The song also contains samples of "[[Bad (Michael Jackson song)|Bad]]" by Michael Jackson.<ref>http://www.whosampled.com/sample/view/136964/J.A.M.S-JAM's%20Have%20a%20Party%20(Burn%20the%20Bastards)_Michael%20Jackson-Bad/</ref>

==Reviews==
Announcing a change of name in January 1988, Bill Drummond had said "We might put out a couple of 12" records under the name The K.L.F., these will be rap free just pure dance music, so don't expect to see them reviewed in the music papers".<ref name="info88">Drummond, B., [http://www.libraryofmu.net/display-resource.php?id=501 KLF Communications Info Sheet], 22 January 1988.</ref> As predicted, "Burn the Beat" and "Burn the Bastards" attracted little attention from the music press.<!-- Not sure this is totally correct, as the quote reads: "Other than "Who Killed The JAMs?" and "Burn The Beat" ... there wont be anything new out by The JAMs this year.... We might put out a couple of 12" records under the name The K.L.F., these will be rap free just pure dance music, so don't expect to see them reviewed in the music papers...."-->

Reviewing ''Who Killed The JAMs?'', ''[[Sounds magazine|Sounds]]'' described "Burn the Bastards" as "a JAMs manifesto" which "assumes a sinister edge alongside the pile of blazing copies of ''1987'' pictured on the sleeve", citing the track as evidence that The JAMs were "defiant, outspoken, still a wily step ahead".<ref name="Sounds" />

==Formats and track listings==
"Burn the Beat" was originally a [[KLF Communications]] 12" limited to 5,000 copies exported from the UK.  In 1989, it was released in the US by [[TVT Records]]. "Burn the Bastards" was released by KLF Communications for a UK audience.

{|class="wikitable"
! align="center" rowspan=2 |Format (and countries)
! colspan=4 |Track number
|-
!1
!2
!3
!4
|-
| colspan=5 align="left" | '''Burn the Beat'''
|- 
| align="left" | 12" single (UK, KLF Communications JAMS 26T)
|J
|M
|G
|PR
|-
| align="left" | 7" single, 12" single (US, released 1989)
|J 
|M
|C
|L
|-
| colspan=5 align="left" | '''Burn the Bastards'''
|- 
| align="left" | 7" single (UK, KLF Communications KLF 002)
|l
|P
|
|
|-
| align="left" | 12" single (UK, KLF Communications KLF 002T)
|l
|C
|
|
|-
|}

'''Key'''
*l - "Burn the Bastards" (LP edit) (4:07)
*L - "Burn the Bastards" (LP version) (6:28)
*J - "Burn the Bastards (JAMs Have A Party Mix)" (4:11)    
*C - "Burn the Beat (Club Mix)" (4:51)
*M - "Burn the Beat (Mu Mu Mix)" (4:43)
*P - "The Porpoise Song" (5:43)
*PR - "The Porpoise Song (Instrumental Remix)" (5:09)
*G - "Prestwich Prophet's Grin (Instrumental Remix)" (4:14)

==Notes and references==
{{reflist}}

{{The KLF}}
{{The KLF singles}}

{{good article}}

[[Category:1988 singles]]
[[Category:KLF Communications singles]]
[[Category:The KLF songs]]
[[Category:TVT Records singles]]
[[Category:Song recordings produced by Bill Drummond]]
[[Category:Song recordings produced by Jimmy Cauty]]
[[Category:1988 songs]]