{{Infobox military person
|birth_name= John Sidney McCain
|birth_date= {{birth date|1884|8|9}}
|death_date= {{death date and age|1945|9|6|1884|8|9}}
|birth_place= [[Carroll County, Mississippi]]
|death_place= [[Coronado, California]]
|placeofburial=Arlington National Cemetery
|placeofburial_label= Place of burial
|image= Vice-Admiral John S McCain.jpg
|caption= 
|nickname= "Slew"
|allegiance= {{USA}}
|branch = [[File:United States Department of the Navy Seal.svg|25px]] [[United States Navy]]
|serviceyears= 1906–1945
|rank= [[File:US-O10 insignia.svg|35px]] [[Admiral (United States)|Admiral]]
|commands= {{USS|Ranger|CV-4|6}}<br/>[[Air Forces for Western Sea Frontier and the South Pacific Force]] <br/>[[Bureau of Aeronautics]] <br/>[[Second Fast Carrier Force]]<br/>[[Task Group 38.1]]<br/> [[Fast Carrier Task Force|Task Force 38]] 
|unit=
|battles= [[World War I]]<br/>[[World War II]]
*[[Pacific War]]
*[[Guadalcanal campaign|Guadalcanal Campaign]]
*[[Mariana and Palau Islands campaign|Mariana Campaign]]
*[[Philippines campaign (1944–45)|Philippines Campaign]]
*[[Battle of Okinawa]]
|awards= [[Navy Cross (United States)|Navy Cross]]<br/>[[Navy Distinguished Service Medal]] (3)
|relations=[[Admiral (United States)|ADM]] [[U.S. Navy|(USN)]] [[John S. McCain Jr.]] (son)<br/>[[Captain (United States)#U.S. Navy, U.S. Coast Guard, U.S. Public Health Service, and National Oceanic and Atmospheric Administration|CAPT]] [[U.S. Navy|(USN)]]/[[United States Senator|Sen.]] [[John McCain|John S. McCain III]] (grandson)<br/>[[Brigadier General (United States)|BG]] [[U.S. Army|(USA)]] [[William Alexander McCain]] (brother)<br/>[[Private (rank)|PVT]] [[Confederate States Army|(CSA)]] William Alexander McCain (grandfather)
|spouse=Catherine Davey Vaulx
}}

'''John Sidney McCain, Sr.''' (August 9, 1884 – September 6, 1945), nicknamed '''"Slew"''', was a [[United States Navy|U.S. Navy]] [[Admiral (United States)|admiral]]. He held several command assignments during the [[Pacific War|Pacific campaign]] of [[World War II]].

McCain was a pioneer of aircraft carrier operations.<ref name="timberg-bio-ch1">Timberg, ''An American Odyssey'', [https://www.nytimes.com/books/first/t/timberg-mccain.html pp. 17–34.]</ref> Serving in the [[Pacific Ocean theater of World War II]], in 1942 he commanded all land-based air operations in support of the [[Guadalcanal campaign]], and in 1944-45 he aggressively led the [[Fast Carrier Task Force]]. His operations off the [[Philippines campaign (1944–45)|Philippines]] and [[Battle of Okinawa|Okinawa]], and air strikes against [[Taiwan|Formosa]] and the [[Japanese home islands]], caused tremendous destruction of Japanese naval and air forces in the closing period of the war.<ref name="alexander-13">Alexander, ''Man of the People'', pp. 13–14.</ref>  He died four days after the [[Japanese Instrument of Surrender|formal Japanese surrender ceremony]].

He was the father of Admiral [[John S. McCain Jr.]]; they became the first father-son pair ever to achieve [[four star admiral]] rank in the U.S. Navy. He was the grandfather of [[U.S. Senator]] from [[Arizona]] and [[John McCain presidential campaign, 2008|2008 Republican presidential nominee]] Navy Captain [[John McCain|John S. McCain III]] and the great-grandfather of John S. McCain IV. All four generations graduated from the [[United States Naval Academy]].

==Early life, education and family==
McCain was born in [[Carroll County, Mississippi]], the son of [[Planter (American South)|planter]]<ref>McCain and Salter, ''Faith of My Fathers'', p. 21.</ref> John Sidney McCain (b. [[Mississippi]], 1851 – d.&nbsp;1934) and wife Elizabeth-Ann Young (b. Mississippi, 1855 – d.&nbsp;1922), who married in 1877. His grandparents were William Alexander McCain (b. 1812, [[North Carolina]] – 1864) and Mary Louisa McAllister, who were married in 1840.

He attended the [[University of Mississippi]] for two years, where he joined the [[Phi Delta Theta Fraternity]], and then decided to attend the [[United States Military Academy at West Point]], where his brother William Alexander McCain was enrolled.<ref name="ff-22">McCain and Salter, ''Faith of My Fathers'', pp. 22–23.</ref>  To practice for its entrance exams, he decided to take the ones for the United States Naval Academy; when he passed those and earned an appointment, he decided to attend there instead.<ref name="ff-22"/>  In doing so, he would leave behind his Mississippi plantation and adopt the Navy's itinerant life.<ref name="wapo083108"/>

At the Naval Academy, his performance was lackluster.<ref name="timberg-bio-ch1"/>  He failed his annual physical on account of defective hearing, but the condition was waived due to the great need for officers.<ref name="ff-22"/>  When he graduated in 1906, he ranked 79 out of 116 in his class, and the yearbook labeled him "The skeleton in the family closet of 1906."<ref name="timberg-bio-ch1"/>

He married Catherine Davey Vaulx, who was eight years his senior (b. [[Fayetteville, Arkansas]], January 9, 1876 – d. [[San Diego, California]], May 29, 1959), on August 9, 1909, at [[Colorado Springs, Colorado]].

==Early career and World War I==
[[File:President Theodore Roosevelt - NH 1836.jpg|thumb|left|McCain as a young ensign listens to President [[Theodore Roosevelt]] as he stands on a gun turret to address the officers and men of the USS ''Connecticut'' (BB-18), upon its return as a part of the [[Great White Fleet]] in February 1909 in [[Hampton Roads, Virginia]]]]

Soon after earning his commission, McCain sailed aboard the [[Great White Fleet]]'s world cruise from 1907 to 1909, joining the battleship {{USS|Connecticut|BB-18|6}} for the last stretch home.<ref name="reynolds">Reynolds, ''Famous American Admirals'', p. 206.</ref>  His next assignment was to the [[Asiatic Squadron]], after which the Navy ordered him to the naval base at [[San Diego, California]].

During 1914 and 1915 he was [[executive officer]] and [[engineering officer]] aboard the armored cruiser {{USS|Colorado|ACR-7|6}}, patrolling off the Pacific coast of then-troubled [[Mexico]].<ref name="reynolds"/>  In September 1915, he joined the armored cruiser [[USS California (ACR-6)|USS ''San Diego''<!--sic-->]], flagship for the [[CINCPAC|Pacific Fleet]].<ref name="reynolds"/>

With U.S. entry into [[World War I]], McCain and ''San Diego'' served on convoy duty in the [[Atlantic Ocean|Atlantic]],<ref name="reynolds"/> escorting shipping through the first dangerous leg of their passages to Europe. Based out of [[Tompkinsville, New York]], and [[City of Halifax|Halifax]], the ''San Diego'' operated in the weather-torn, [[submarine]]-infested [[North Atlantic]]. McCain left the ''San Diego'' in May 1918, two months before she was sunk, when he was assigned to the [[Bureau of Navigation]].<ref name="reynolds"/>

==Interwar period==
[[File:McCain Nomination.JPG|thumb|alt= |McCain's Rear Admiral nomination]]
In the 1920s and early 1930s, McCain served aboard the {{USS|Maryland|BB-46|6}}, the {{USS|New Mexico|BB-40|6}}, and the {{USS|Nitro|AE-2|6}}. His first command was the {{USS|Sirius|AK-15|6}}. In 1935, McCain enrolled in flight training. Graduating at 52 in 1936, he became one of the oldest men to become a [[naval aviator]]<ref>His contemporary Admirial [[William Halsey]] also graduated at age 52 as a flight aviator.</ref> and from 1937 to 1939 he commanded the aircraft carrier the {{USS|Ranger|CV-4|6}}. In January 1941, after promotion to [[Rear admiral (United States)|rear admiral]], he commanded the Aircraft Scouting Force of the Atlantic Fleet.<ref name="Boatner-351"/>

Physically short in stature and of rather thin frame, McCain was known for being gruff and very profane; he liked to drink and gamble.<ref name="wapo083108"/>  He also showed courage and was regarded as a natural, inspirational leader of men.<ref name="wapo083108"/>  In the words of one biographical profile, McCain "preferred contentious conflict to cozy compromise."<ref name="wapo083108"/>

==World War II==
[[File:John McCain, Sr. & William Halsey on USS New Jersey 04-0431M.JPG|thumb|right|Vice Admiral McCain (L) and Admiral William Halsey, Commander of the Third Fleet, hold conference on board battleship ''New Jersey'' en route to the Philippines in December 1944.]]
[[File:Halsey Mccain-adjusted.jpg|185px|thumb|right|upright|Admiral McCain (R) with Admiral William Halsey on the battleship ''Missouri'' on September 2, 1945, shortly after the ceremony in which Japan surrendered, ending World War II.]]
After Japan [[Attack on Pearl Harbor|attacked Pearl Harbor]] in December 1941, the Navy appointed McCain as Commander, Aircraft, South Pacific in May 1942. As COMAIRSOPAC, he commanded all land-based [[Allies of World War II|Allied]] air operations supporting the [[Guadalcanal campaign]] in the [[Solomon Islands]] and south Pacific area. Aircraft under McCain's command, including the [[Cactus Air Force]], located at [[Henderson Field (Guadalcanal)|Henderson Field]] on Guadalcanal, were key in supporting the successful effort to defend Guadalcanal from Japanese efforts to retake the island during this time.<ref name="Boatner-351">Boatner, ''Biographical Dictionary'', p. 351.</ref>

In October 1942, the Navy ordered him to [[Washington, D.C.]], to head the [[Bureau of Aeronautics]]. In August 1943, he became Deputy Chief of Naval Operations for Air with the rank of vice admiral.<ref name="Boatner-351"/>

McCain returned to combat in the Pacific in August 1944 with his appointment as commander of a carrier group in [[Marc Mitscher]]'s [[Task Force 58]] (TF&nbsp;58), part of [[Raymond Spruance]]'s [[United States Fifth Fleet#History|Fifth Fleet]]. In this role, McCain participated in the [[Mariana and Palau Islands campaign|Marianas campaign]], including the [[Battle of the Philippine Sea]], and the beginning of the [[Philippines campaign (1944–45)|Philippines campaign]].<ref name="Boatner-351"/>  At the [[Battle of Leyte Gulf]], Admiral [[William Halsey]] left in pursuit of a decoy force, leaving Rear Admiral [[Clifton Sprague|Clifton "Ziggy" Sprague]]'s Task Unit 77.4.3 (usually referred to by its radio callsign, "Taffy&nbsp;3") to continue supporting forces ashore, defended by only a light screen of [[destroyer]]s and [[destroyer escort]]s.

Taffy 3 came under attack from a much heavier Japanese force, under Vice Admiral [[Takeo Kurita]], provoking the [[Battle off Samar]]. Sprague promptly began to plead for assistance from Halsey who was responsible for protecting the northern approach to the landing site. Halsey had contemplated detaching a battle group, Task Force 34 (TF&nbsp;34), but chose to bring all available battle-groups north to pursue the Japanese carrier force. Hearing Sprague's pleas (including messages in plain language, not even bothering to encrypt them, as the situation grew desperate), Admiral Nimitz sent Halsey a terse message, 
<code>TURKEY TROTS TO WATER GG FROM CINCPAC ACTION COM THIRD FLEET INFO COMINCH CTF SEVENTY-SEVEN X WHERE IS RPT WHERE IS TASK FORCE THIRTY FOUR RR [[The world wonders|THE WORLD WONDERS]]</code>. Halsey was infuriated (not recognizing the final phrase as padding, chosen for the anniversary of the [[Charge of the Light Brigade]], until a communications officer shortly thereafter explained and cleared it up for him.) and sent McCain's Task Group 58.1 (TG&nbsp;58.1) to assist.<ref>McCain and Salter, ''Faith of My Fathers'', 40–41.</ref>

McCain had been monitoring the original pleas for help and, recognizing the seriousness of the situation, had turned around without awaiting orders.<ref>This makes one wonder at Halsey's claim of being unaware.</ref> His ships raced downwind towards the battle, briefly turning into the wind to recover returning planes. At 10:30, a force of [[Curtiss SB2C Helldiver]]s, [[Grumman TBF Avenger]]s, and [[Grumman F6F Hellcat]]s was launched from {{USS|Hornet|CV-12|6}}, {{USS|Hancock|CV-19|6}}, and {{USS|Wasp|CV-18|6}} at the extreme range of 330 miles (610&nbsp;km). Though the attack did little damage, it strengthened Kurita's decision to retire.<ref>{{Cite web
 | url= http://www.history.navy.mil/danfs/w3/wasp-ix.htm
 | title= ''Wasp''
 | date= 2005-05-11 | work= [[Dictionary of American Naval Fighting Ships|DANFS]]
 | publisher= U.S. [[Naval Historical Center]]
 | accessdate= 2008-09-04}}</ref>

On October 30, 1944, McCain assumed command of [[Fast Carrier Task Force|Task Force 38]] (TF&nbsp;38). He retained command of the fast carrier task force that he led through the [[Battle of Okinawa]] and raids on the Japanese mainland.<ref name="Boatner-351"/>

While conducting operations off the Philippines, McCain, as Chief of Staff of Third Fleet, participated in Halsey's decision to keep the combined naval task force on station rather than avoid a major storm, [[Typhoon Cobra (1944)|Typhoon Cobra]] (later known also as "Halsey's Typhoon") which was approaching the area. The storm sank three destroyers and inflicted heavy damage on many other ships. Some 800&nbsp;men were lost, in addition to 146&nbsp;aircraft. A Navy court of inquiry found that while Halsey committed an error of judgment in sailing into the typhoon, it stopped short of unambiguously recommending sanction.<ref>Drury, ''Halsey's Typhoon''.</ref>

==Death==
[[File:McCainFatherandGrandfather.jpg|thumb|right|John S. McCain, Sr.|John S. "Slew" McCain, Sr., and [[John S. McCain, Jr.|John S. "Jack" McCain, Jr.]], on board a U.S. Navy ship in [[Tokyo Bay]], c. September 2, 1945. The senior McCain died four days later.]]
By war's end in August 1945, the stress of combat operations had worn McCain down to a weight of only 100&nbsp;pounds. He requested home leave to recuperate but Halsey insisted that he be present at the [[Japanese Instrument of Surrender|Japanese surrender ceremony in Tokyo Bay]] on September 2, 1945. Departing immediately after the ceremony, McCain died just four days later of a [[myocardial infarction|heart attack]] at his home in [[Coronado, California]], on September 6, 1945. His death was front page news.<ref name="timberg-bio-ch1"/>  McCain Sr. was buried at [[Arlington National Cemetery]].

McCain was posthumously promoted to full admiral in 1949, based upon a resolution of the U.S. Congress.<ref name="nyt082849"/>  This followed a recommendation of [[Secretary of the Navy]] [[Francis P. Matthews]], who said that McCain's combat commendations would have earned him the promotion had he not died so soon after the war.<ref name="nyt082849">{{Cite news | url=https://select.nytimes.com/mem/archive/pdf?res=F30C10F7385D13728DDDA10A94D0405B8988F1D3 | title=M'Cain Promotion Passed | publisher=[[Associated Press]] for ''[[The New York Times]]'' | date=1949-08-28}}</ref>  During his career McCain was awarded the [[Navy Distinguished Service Medal|Distinguished Service Medal]] and two [[Gold Star (disambiguation)|Gold Stars]] in lieu of subsequent awards.<ref>USS ''John S. McCain'' (DDG 56), [http://navysite.de/dd/ddg56.htm].</ref>

==Family heritage==
His grandfather, William Alexander McCain (b. [[North Carolina]], 1812 – d.&nbsp;1864), is often cited on the internet as having "died while serving as a private in Company&nbsp;I, 5th&nbsp;Mississippi Cavalry Regiment, [[Confederate States Army]]".  However, that William McCain was recruited in Attala, Mississippi and is listed in the 1860 census in Attala County.  The correct McCain ancestor lived in Carrollton, Carroll County, Mississippi in 1860. According to a 1930 application to the Sons of the American Revolution he was Dr. William Alexander McCain b. May 2, 1817 d. Jun 20, 1863 and did not die a POW in 1864. At least three William McCain's can be found in the 1860 US census in Mississippi and in the Fold3 databases in Confederate service. One was recruited from Choctaw County (Co K, 1st Regiment Miss Reserves), one from Attala County (Co I, 5th Regiment MS Cav), and a third from an unknown County who served in Co K 28th Regiment Miss Cav. It is possible that early genealogy records were not so easily accessed as today. During his life, he owned a {{convert|2000|acre|km2|sing=on}} [[plantations in the American South|plantation]] in [[Carroll County, Mississippi]], known alternately as "Teoc" (the Choctaw name for the creek it was located upon) and "Waverly", as well as 52&nbsp;[[slave]]s (some of whose descendants share the surname and call themselves the "[[black McCains]]").<ref>{{cite news | last=Chideya | first=Farai | title=Black McCains Share Family's Struggles, Triumphs | publisher=[[NPR]] | url=http://www.npr.org/templates/story/story.php?storyId=95937654&ft=1&f=1021 | date=2008-10-21 | accessdate=2008-10-26}}</ref>  He was married in 1840 to Mary Louisa McAllister (b. [[Alabama]], 1812 – d.&nbsp;1882).

McCain's father, the first John Sidney McCain, known as J.&nbsp;S. McCain (which may explain the apparent discrepancy in Senator John S. McCain being the III, rather than the IV), served as Sheriff and, later, President of the Board of Supervisors of Carroll County.

McCain's older brother, another William Alexander McCain, also attended the [[University of Mississippi]] before transferring to the [[United States Military Academy]]. William A. McCain would eventually retire with the rank of [[Brigadier general (United States)|Brigadier General]], and was awarded the Distinguished Service Medal for actions in World War I, as well as the Oak Leaf Cluster during World War II. An uncle, [[Henry Pinckney McCain]] (b. [[Mississippi]], 1861 – d.&nbsp;1941), also attended West Point and later retired from the Army as a [[Major-General]]. Camp McCain, a [[World War II]] training base and current Mississippi National Guard training site, located in [[Grenada County, Mississippi]], is named for him.

Admiral McCain's son, [[John S. McCain, Jr.]], was a submarine commander in World War II and later served as CINCPAC, Commander in Chief Pacific Command, during the Vietnam War.

His grandson, [[John McCain|John S. McCain III]], was a U.S. Navy pilot during the Vietnam war, and was shot down and spent over five years as a [[P.O.W.]] in the infamous "[[Hanoi Hilton]]" and other North Vietnamese camps. After his release, he was elected to the U.S. House of Representatives and the United States Senate from Arizona. He ran for President in 2000 (losing the Republican nomination to [[George W. Bush]]) and then again in 2008 where he won the [[Republican Party (United States)|Republican Party]]'s nomination only to lose in the general election to [[Barack Obama]]. John S. McCain III wrote a book, ''[[Faith of My Fathers]],'' concerning his navy family and his own experiences as a [[midshipman]] at [[United States Naval Academy|Annapolis]], a naval aviator and prisoner of war. Senator McCain's brother [[Joe McCain]] attended the US Navy Academy but served in the US Navy as an enlisted man.

John S. McCain III claims a royal connection on his campaign website: "McCain’s family roots in Europe are [[Ulster Scots people|Scots-Irish]]. His great-aunt was a descendant of [[Robert the Bruce]], an early Scottish king. McCain's roots in America date to the American Revolution. John Young, an early McCain ancestor, served on Gen. [[George Washington]]'s staff."<ref>McCain and Salter, ''Faith of My Fathers'', p. 19.</ref> John Young's ancestry has been traced to John Lamont, [[Baron McGorrie]] (the "red baron of Inverchaolain and Knockdow"; 1540–1583).<ref>{{Cite web |url=http://www.geocities.com/charryoung/early.html |title=Ancestors of one Young family in America |accessdate=2007-07-05|archiveurl=https://web.archive.org/web/20060210024044/http://www.geocities.com/charryoung/early.html|archivedate=2006-02-10}}</ref><ref>[http://www.wargs.com/political/mccain.html Ancestry of John McCain (b. 1936)<!-- Bot generated title -->]</ref> According to DNA Testing, Senator McCain is related via his maternal ancestry to John Washington, a great-great-grandfather of President [[George Washington]].<ref>PBS [[Finding Your Roots]] broadcast February 9, 2016; according to DNA testing, Senator McCain also has [[Native Americans in the United States|Native American]] ancestory.</ref>
 
Admiral McCain's great-grandson John Sidney "Jack" McCain&nbsp;IV attended and graduated from the U.S. Naval Academy in 2009; <ref>{{Cite news|url=https://news.yahoo.com/s/ap/20090522/ap_on_go_pr_wh/us_obama_naval_academy |title=Obama vows not to send people to war without cause |author=Superville, Darlene |agency=[[Associated Press]] |publisher=[[Yahoo! News]] |date=2009-05-22 |accessdate=2009-05-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20090525223917/http://news.yahoo.com/s/ap/20090522/ap_on_go_pr_wh/us_obama_naval_academy |archivedate=May 25, 2009 }}</ref> and is a Naval aviator. Jack McCain IV was awarded his diploma at Annapolis by President Barack Obama, the man who defeated his father in 2008. Jack McCain IV is married to Capt. Renee Swift-McCain (USAF Reserve). Another great-grandson, James Hensley "Jimmy" McCain, enlisted in the [[United States Marine Corps|Marine Corps]] in 2006.<ref>{{Cite news
|url=http://www.marinecorpstimes.com/story.php?f=1-292925-1990039.php |title=Sen. McCain’s youngest son joins Marine Corps
|work=Marine Corps Times |publisher=Associated Press |date=July 31, 2006|accessdate=2006-08-01}}</ref> He finished a tour of duty in the [[Iraq War]] in 2008.<ref>{{Cite news
|url=http://www.navytimes.com/news/2008/03/marine_mccainskids_030808/ |title=McCain win might stop sons from deploying |work=Navy Times |publisher= |date=March 10, 2008|accessdate=2008-03-17}}</ref>  Another, Douglas McCain, served as a Navy [[A-6E Intruder]] carrier pilot before turning to commercial aviation.<ref name="nyt122707">{{Cite news |url=https://www.nytimes.com/2007/12/27/us/politics/27mccainkids.html |title=Bridging 4 Decades, a Large, Close-Knit Brood |author=Jennifer Steinhauer |publisher=[[The New York Times]] |date=2007-12-27 |accessdate=2007-12-27}}</ref>

==Namesakes==
McCain Field, the operations center at [[Naval Air Station Meridian]], [[Mississippi]], was named in his honor.<ref name="wapo083108">{{Cite news | url=http://www.washingtonpost.com/wp-dyn/content/article/2008/08/30/AR2008083001786_pf.html | title=A Turbulent Youth Under a Strong Father's Shadow | author=Leahy, Michael | publisher=''[[The Washington Post]]'' | date=2008-08-31 | accessdate=2008-11-08 | authorlink=Michael Leahy (author)}}</ref>

The guided-missile destroyer {{USS|John S. McCain|DL-3}} (in service 1953–1978) was named for him, and the destroyer {{USS|John S. McCain|DDG-56}} (in service 1994–present) was named for both of the Admirals John S. McCain.

McCain was a would-be author who wrote fiction that was never published, including some adventure stories under the name Casper Clubfoot.<ref name="nyt101208ff">{{Cite news | url=https://www.nytimes.com/2008/10/13/us/politics/13mccain.html | title=Writing Memoir, McCain Found a Narrative for Life | author=Kirkpatrick, David D. | publisher=''[[The New York Times]]'' | date=2008-10-12 | accessdate=2008-10-13}}</ref>

==Awards==
<center>&nbsp;
{|
|colspan=3 align=center|[[File:Naval Aviator Badge.jpg|200px]]
|-
|colspan=3 align=center|{{ribbon devices|number=0|type=award-star|ribbon=Navy Cross ribbon.svg|width=103}} 
|-
|{{Ribbon devices|number=2|type=award-star|name=Navy Distinguished Service ribbon|width=103}}
|{{ribbon devices|number=0|type=service-star|ribbon=Combat Action Ribbon.svg|width=103}}
|{{ribbon devices|number=0|type=award-star|ribbon=United States Navy Presidential Unit Citation ribbon.svg|width=103}}
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Mexican Service Medal ribbon.svg|width=103}}
|{{ribbon devices|number=1|type=service-star|ribbon=World War I Victory Medal ribbon.svg|width=103}}
|{{ribbon devices|number=1|type=service-star|ribbon=American Defense Service ribbon.svg|width=103}}
|-
|{{ribbon devices|number=0|type=award-star|ribbon=American Campaign Medal ribbon.svg|width=103}}
|{{ribbon devices|number=3|type=service-star|ribbon=Asiatic-Pacific Campaign ribbon.svg|width=103}}
|{{ribbon devices|number=0|type=award-star|ribbon=World War II Victory Medal ribbon.svg|width=103}}
|-
|{{ribbon devices|number=0|type=award-star|ribbon=Army of Occupation ribbon.svg|width=103}}
|{{ribbon devices|number=0|type=service-star|ribbon=Order of the British Empire (Military) Ribbon.png|width=103}}
|{{ribbon devices|number=1|type=service-star|ribbon=Phliber rib.png|width=103}}
|}

{| class="wikitable"
|-
|colspan=3 align=center|[[Naval Aviator|Naval Aviator badge]]
|-
|colspan=3 align=center|[[Navy Cross (United States)|Navy Cross]] 
|-
|align=center|[[Navy Distinguished Service Medal]]<br>with two [[5/16 inch star|stars]] 
|align=center|[[Combat Action Ribbon]] <br>(posthumous)
|align=center|[[Presidential Unit Citation (United States)|Distinguished Unit Citation]]
|-
|align=center|[[Mexican Service Medal]]
|align=center|[[World War I Victory Medal (United States)|World War I Victory Medal]]<br>with "ESCORT" [[medal bar|clasp]]
|align=center|[[American Defense Service Medal]]<br>with "FLEET" clasp
|-
|align=center|[[American Campaign Medal]]
|align=center|[[Asiatic-Pacific Campaign Medal]]<br>with three [[Service star|stars]]
|align=center|[[World War II Victory Medal (United States)|World War II Victory Medal]]
|-
|align=center|[[Navy Occupation Service Medal]]<br>with "ASIA" clasp <br>(posthumous)
|align=center|[[Order of the British Empire|Order of the British Empire<br>(Military Division)]]
|align=center|[[Philippine Liberation Medal]]<br>with one star
|}
</center>

==Notes==
{{reflist}}

==References==

===Books===
*{{Cite book |title = Man of the People: The Life of John McCain |first = Paul |last = Alexander |authorlink=Paul Alexander (American writer) |isbn = 0-471-22829-X |year = 2002 |publisher = [[John Wiley & Sons]]|location=Hoboken, New Jersey}}
*{{Cite book
 | last = Boatner
 | first = Mark M.
 | authorlink =
 | year = 1996
 | chapter = 
 | title = The Biographical Dictionary of World War II
 | publisher = Presidio Press
 | location = Novato, California
 | isbn = 0-89141-548-3
}}
*{{Cite book
|url=http://www.amazon.com/Halseys-Typhoon-Fighting-Admiral-Untold/dp/0871139480
|author=Robert Drury, Tom Clavin
|title=Halsey's Typhoon: The True Story of a Fighting Admiral, an Epic Storm, and an Untold Rescue
|year=2006
|publisher=Atlantic Monthly Press
|isbn=0-87113-948-0}}
*{{Cite book
 | last = Gilbert
 | first = Alton
 | authorlink =
 | year = 2006
 | chapter = 
 | title = A Leader Born: The Life of Admiral John Sidney McCain, Pacific Carrier Commander
 | publisher = Casemate
 | location =
 | isbn = 1-932033-50-5
}}
*{{Cite book | last=McCain | first=John | authorlink=John McCain |author2=Salter, Mark |authorlink2=Mark Salter  | title=[[Faith of My Fathers]] | publisher=[[Random House]] | year=1999 |location=New York | isbn=0-375-50191-6}}
*{{Cite book | last=Reynolds | first=Clark G. | authorlink=Clark G. Reynolds | title=Famous American Admirals | publisher=[[Naval Institute Press]] | year=2002 | isbn=1-55750-006-1}}
*{{Cite book |last=Timberg |first=Robert |title=[[John McCain: An American Odyssey]] |publisher=[[Touchstone Books]] |year=1999 |location=New York |isbn=0-684-86794-X}}  Online access to [https://www.nytimes.com/books/first/t/timberg-mccain.html Chapter 1] is available.

{{DANFS}}

==External links==
* [http://www.arlingtoncemetery.net/jsmccain.htm John Sidney McCain at arlingtoncemetery.net site]

{{John McCain|state=collapsed}}

{{Authority control}}

{{DEFAULTSORT:McCain, John S., Sr.}}
[[Category:1884 births]]
[[Category:1945 deaths]]
[[Category:County supervisors in Mississippi]]
[[Category:United States Navy admirals]]
[[Category:United States Navy World War II admirals]]
[[Category:United States Naval Academy alumni]]
[[Category:University of Mississippi alumni]]
[[Category:American military personnel of World War II]]
[[Category:People from Carroll County, Mississippi]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Navy Cross (United States)]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:American people of Scotch-Irish descent]]
[[Category:McCain family]]