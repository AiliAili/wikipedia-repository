{{Infobox dam
| name                 = Enguri Dam
| name_official        =
| image                = Enguridam.jpg
| image_size           = 
| image_caption        = 
| image_alt            = 
| location_map         = Georgia
| location_map_size    = 
| location_map_caption = 
| coordinates          = {{coord|42|45|36|N|42|01|48|E|type:landmark|display=inline,title}}
| country              = [[Georgia (country)|Georgia]]
| location             = [[Jvari (town)|Jvari]]
| status               = O
| construction_began   = 1961
| opening              = 1978
| demolished           = 
| cost                 = 
| owner                = Engurhesi Ltd (Georgian Government)
| dam_type             = [[Arch dam]]
| dam_height           = {{Convert|271.5|m|ft|0|abbr=on}}
| dam_height_thalweg   =
| dam_height_foundation=
| dam_length           = 
| dam_width_crest      = 
| dam_width_base       = 
| dam_volume           = 
| dam_elevation_crest  =
| dam_crosses          = [[Enguri River]]
| spillway_count       = 
| spillway_type        = 
| spillway_capacity    = 
| res_name             = 
| res_capacity_total   = 
| res_capacity_active  =
| res_capacity_inactive=
| res_catchment        = 
| res_surface          = 
| res_elevation        =
| res_max_depth        = 
| res_max_length       =
| res_max_width        =
| res_tidal_range      = 
| plant_operator       = [[Ltd. Engurhesi]]
| plant_commission     = 
| plant_decommission   = 
| plant_type           = 
| plant_turbines       = 5 × 260&nbsp;[[Megawatt|MW]]
| plant_capacity       = 1,300&nbsp;[[Megawatt|MW]]
| plant_annual_gen     = 4.3&nbsp;[[TWh]]
| website              = 
| extra                = 
}}
The '''Enguri Dam''' is a [[hydroelectric]] [[dam]] on the [[Enguri River]] in [[Georgia (country)|Georgia]]. Currently it is the world's second highest concrete [[arch dam]] with a height of {{convert|271.5|m|ft}}.<ref name=ebrd>
{{cite web
 |url=http://www.ebrd.com/projects/psd/psd1998/4304.htm 
 |title=Enguri Hydro power Plant Rehabilitation project. Project summary document 
 |publisher=[[European Bank for Reconstruction and Development]] 
 |date=2006-09-08 
 |accessdate=2008-11-08 
 |archiveurl=https://web.archive.org/web/20080527202746/http://www.ebrd.com/projects/psd/psd1998/4304.htm 
 |archivedate=2008-05-27 
 |deadurl=yes 
 |df= 
}}
</ref><ref name=britannica>
{{cite web
 | url = http://www.britannica.com/eb/article-9042423/Inguri-Dam
 | title = Inguri Dam
 | accessdate = 2007-01-01
 | publisher = [[Britannica]]}}
</ref><ref name=xiaowan>
{{cite web
 | url = http://www.karen.org/news2/messages/244.html
 | title = China's Xiaowan hydroelectric power station succeeds 
 | date = 2008-10-28
 | accessdate = 2008-11-08
 | publisher = [[Xinhua]]}}
</ref> It is located north of the town [[Jvari (town)|Jvari]]. It is part of the Enguri hydroelectric power station (HES) which is partially located in [[Abkhazia]], the separatist region of [[Georgia (country)|Georgia]].

==History==
Soviet President [[Nikita Khrushchev]] initially proposed a major dam and hydroelectric power scheme on the [[Bzyb River]] as his favourite resort was located near the mouth of the river at [[Pitsunda]]. However, his experts informed him that a dam built on the Bzyb River would have had catastrophic effects in causing beach erosion at Pitsunda, so in the end the dam was built on the Enguri River instead, where the impact upon the coastline was assessed to be considerably less pronounced.<ref>{{cite book|url=https://books.google.com/books?id=Lb-66oPHwGIC&pg=PA221|title=Reflections on water: new approaches to transboundary conflicts and cooperation|publisher=MIT Press|year=2001|pages=221–2|isbn=0-262-02487-X|last1=Blatter|first1=Joachim|last2=Ingram|first2=Helen M.}}</ref>

Construction of the Enguri dam began in 1961. The dam became temporarily operational in 1978, and was completed in 1987. In 1994, the dam was inspected by engineers of [[Hydro-Québec]], who found that the dam was "in a rare state of dilapidation".<ref name=bankwatch>
{{cite journal
 | url = http://bankwatch.org/documents/khudoni_dam_study.pdf
 | title = The Khudoni dam: a necessary solution to the Georgian energy crisis?
 | author = Manana Kochladze, Rezo Getiashvili,
 | publisher = CEE Bankwatch Network
 | format = PDF
 | year = 2007
 | accessdate = 2008-11-08}}
</ref>  In 1999, the European Commission granted €9.4&nbsp;million to Georgia for urgent repairs at the Enguri HES, including replacing the stoplog at the arch dam on the Georgian side and, refurbishing one of the five generators of the power station at the Abkhaz side.<ref name=ec/>  In total, €116&nbsp;million loans were granted by the [[EBRD]], the European Union, the Japanese Government, [[KfW]] and Government of Georgia.<ref name=ebrd/> In 2011 The European Investment Bank (EIB) loaned €20 million in order to complete the rehabilitation of the Enguri hydropower plant and to ensure safe water evacuation towards the Black Sea at the Vardnili hydropower cascade.<ref>[http://www.enpi-info.eu/maineast.php?id_type=1&id=23701&lang_id=450 Hydropower in Georgia receives boost from EIB  (ENPI Info Centre)]</ref>

== Technical features ==
The Enguri hydroelectric power station (HES) is a cascade of hydroelectric facilities including, in addition to the dam - diversion installation of the Enguri HES proper, the near-dam installation of the Perepad HES-1 and three similar channel installations of the Perepad HESs-2, -3, and -4 located on the tailrace emptying into the Black Sea.<ref>[http://www.globalsecurity.org/military/world/war/georgia-2008-2.htm Georgia 2008 Daily Chronology], globalsecurity.org</ref> While the arch dam is located on the Georgian controlled territory in [[Svaneti|Upper Svanetia]], the power station is located in the Gali District of region [[Abkhazia]] of [[Georgia (country)|Georgia]].<ref name=ec>
{{cite journal
 | url = http://www.delgeo.ec.europa.eu/en/programmes/Overview%20EC%20projects%20Enguri%20Hydropower%20plant.doc
 | title = Brief overview of EC Rehabilitation projects of the Enguri Hydro-Power Plant – Georgia
 | publisher = European Commission Delegation to Georgia and Armenia
 | format = DOC
 | date = 2006-10-20
 | accessdate = 2008-11-08}}
</ref>  Enguri HES has 20&nbsp;turbines with a nominal capacity of 66&nbsp;[[Watt|MW]] each,<ref>[http://www.ebrdrenewables.com/sites/renew/Lists/Projects/DispForm.aspx?ID=657&Source=http%3A%2F%2Fwww%2Eebrdrenewables%2Ecom%2Fsites%2Frenew%2Fcountries%2FGeorgia%2Fdefault%2Easpx Enguri Hydro Power Plant]</ref> resulting in a total capacity of 1,320 [[Watt|MW]]. Its average annual capacity is 3.8 TW/h, which is approximately 46% of the total electricity supply in Georgia as of 2007.<ref>http://www.minenergy.gov.ge/index.php?m=349 Ministry of Energy of Georgia</ref>

The facility's arched dam, located at the town of [[Jvari (town)|Jvari]], was inscribed in the list of cultural heritage of Georgia in 2015.<ref>{{cite news|title=Enguri HPP’s arched dam granted cultural heritage status|url=http://agenda.ge/news/41067/eng|accessdate=1 January 2016|work=Agenda.ge|date=20 August 2016}}</ref>

== See also ==
{{Portal|Abkhazia|Georgia (country)|Soviet Union|Energy|Water|Renewable energy}}
* [[List of power stations in Georgia]]
* [[Energy in Georgia (country)]]

== References ==
{{Reflist|colwidth=35em}}

{{supertall}}

[[Category:Dams completed in 1987]]
[[Category:Hydroelectric power stations in Abkhazia]]
[[Category:Hydroelectric power stations in Georgia (country)]]
[[Category:Hydroelectric power stations built in the Soviet Union]]
[[Category:Dams in Georgia (country)]]
[[Category:Arch dams]]