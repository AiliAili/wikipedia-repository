'''Bistatic range''' refers to the basic measurement of range made by a [[radar]] or [[sonar]] system with separated transmitter and receiver.  The receiver measures the time difference of arrival of the signal from the transmitter directly, and via reflection from the target.  This defines an ellipse of constant bistatic range, called an iso-range contour, on which the target lies, with foci centred on the transmitter and receiver.  If the target is at range ''R<sub>rx</sub>'' from the receiver and range ''R<sub>tx</sub>'' from the transmitter, and the receiver and transmitter are a distance ''L'' apart, then the bistatic range is ''R<sub>rx</sub>''+''R<sub>tx</sub>''-''L''.  Motion of the target causes a rate of change of bistatic range, which results in [[bistatic Doppler shift]].<ref>Cherniakov, Mikhail (ed). (2007). ''Bistatic Radar: Principles and Practice.'' Wiley. ISBN 0-470-02630-8</ref><ref>Willis, Nicholas. (2007). ''Bistatic Radar.'' SciTech Publishing. 2nd ed. ISBN 1-891121-45-6</ref><ref>{{cite book|title=Advances in bistatic radar|first1=Nicholas J.|last1=Willis|first2=Hugh D.|last2=Griffiths|publisher=SciTech Publishing|year=2007|ISBN=1-891121-48-0}}</ref>

[[File:BistaticRange.png|frame|Bistatic range geometry]]

==Iso-range contour==
Generally speaking, constant bistatic range points draw an ellipsoid with the transmitter and receiver positions as the focal points.  The iso-range contours are where the ground slices the ellipsoid.  When the ground is flat, this intercept forms an ellipse. Note that except when the two platforms have equal altitude, these ellipses are not centered on the specular point.

==See also==
*[[Bistatic imaging]]
*[[Bistatic radar]]

==References==
{{reflist}}

{{DEFAULTSORT:Bistatic Range}}
[[Category:Radar]]
[[Category:Passive radars]]
[[Category:Sonar]]