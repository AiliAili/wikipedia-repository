<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=MF.8
 | image=Marinens Flyvebaatfabrikk M.F.8.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Training seaplane
 | national origin=Norway
 | manufacturer=[[Marinens Flyvebaatfabrikk]]
 | designer=[[Johann E. Høver]]
 | first flight=30 May [[1924 in aviation|1924]]
 | introduced=
 | retired=
 | status=
 | primary user=[[Royal Norwegian Navy Air Service]]
 | number built=8
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Marinens Flyvebaatfabrikk MF.8''' (also known as the '''Høver MF.8''', after its designer) was a military training seaplane built in Norway in the 1920s.<ref name="JEA">Taylor 1989, 620</ref><ref name="IEA">''The Illustrated Encyclopedia of Aircraft'', 2414</ref>

==Design and development==
It was a conventional two-bay biplane design based on the [[Marinens Flyvebaatfabrikk M.F.6|M.F.6]] and [[Marinens Flyvebaatfabrikk M.F.7|M.F.7]], with open cockpits in tandem for the student and instructor.<ref name="Frebergsvik">{{cite web |last=Frebergsvik  |first=Lars |title=Flyfabrikken fra sped start i 1912 til nedleggelse i 1965 |work=Borreminne |publisher=Borre Historielag |url=http://borreminne.hive.no/aargangene/1993/03-flyfabrikk.htm |accessdate=2008-10-23 |language=Norwegian}}</ref> The main difference between the M.F.8 and the M.F.7 were that M.F.8 had a new tail rudder and wing profile, in order to increase landing speed. The new profile had been developed by the [[Sweden|Swedish]] professor Nordensvan, in cooperation with the [[Norwegian Army Air Service]].<ref name="Hafsten101">Hafsten 2003: 101</ref>

===Engine problems===
The first building of the two first aircraft began in December 1923, F.6 (III) being finished 30 May 1924 and F.12 in August the same year. The first aircraft were delivered with {{convert|100|hp|abbr=on}} [[Scania-Vabis]] engines, which proved too weak and was replaced with {{convert|160|hp|abbr=on}} [[William Beardmore and Company|Beardmore]] or Sunbeam engines. For the next two aircraft produced the [[Royal Norwegian Navy Air Service]] (RNNAS) settled on a {{convert|160|hp|abbr=on}} Mercedes engine and aircraft with this engine were redesignated as M.F.8Bs. However, in 1932 [[Marinens Flyvebaatfabrikk]] had to find a replacement for the Mercedes engines as spare parts became hard to procure. A [[Bristol Lucifer]] was borrowed from Kjeller Aircraft Factory and a [[Walter Regulus]] from a factory in [[Prague]],<ref name="Hafsten101"/> were both tested at Horten. Still, both engines proved too costly to finance. Barely used [[Armstrong Siddeley Cheetah]] IIA engines were found in [[United Kingdom|British]] surplus stockpiles and purchased for 500 [[Norwegian krone]]r apiece. After rebuilding with a larger propeller engine output was reduced from {{convert|265|hp|abbr=on}} to {{convert|180|hp|abbr=on}} making them suitable for a trainer, and these were fitted to both M.F.8 and M.F.10 trainers. By early summer 1936 all M.F.8s had been rebuilt with Armstrong Siddeley Cheetah IIA engines however with the engine out of production spare parts once more became a problem.<ref name="Hafsten102">Hafsten 2003: 102</ref>
[[File:Marinens Flyvebaatfabrikk M.F.8 rear view.jpg|thumb|Rear view of an M.F.8.]]
The Walter Regulus engine had performed well during the testing in Horten and examples were purchased, however the engine did not perform as well as anticipated and in 1938 the Czech factory ceased production. The American [[Jacobs L-4]]M engine was used to replace the Walter Regulus during the winter of 1939-1940.<ref name="Hafsten102"/>

==Operational use==
In all the M.F.8 design was very successful, with the pilots of the RNNAS receiving their first training on it up until the [[Operation Weserübung|German invasion of Norway]], the last of the type having been delivered in October 1935. Plans to replace it with a version of the Army's [[Tiger Moth]] trainer equipped with floats failed when tests in 1934 showed that the Tiger Moth was of little value as a seaplane. Marinens Flyvebaatfabrikk  began designing and prototype production on the monoplane trainer [[Marinens Flyvebaatfabrikk M.F.12]] from 15 June 1937, intended as a replacement for the M.F.8, but internal disagreements delayed the introduction of the new type until the German invasion halted all work.<ref name="Hafsten102"/> Five of the eight M.F.8s produced were still in service at the outbreak of the [[World War II|Second World War]], flying for the last times on 13 and 14 December 1939 before being placed in storage at [[Karljohansvern]] where they were all captured by the Germans on 9 April 1940.<ref>Hafsten 2003, 225&ndash;226</ref>

<!-- ==Development== -->
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aerospecs
|ref=<!-- reference -->Hafsten 2003, p.225
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=Two, pilot and instructor
|length m=9.70
|length ft=31
|length in=10
|span m=14.50
|span ft=47
|span in=7
|height m=3.50
|height ft=11
|height in=6
|wing area sqm=
|wing area sqft=
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=975
|empty weight lb=2,150
|gross weight kg=1,275
|gross weight lb=2,810
|eng1 number=1
|eng1 type=[[Armstrong Siddeley Cheetah]] IIA
|eng1 kw=210
|eng1 hp=280
|max speed kmh=105
|max speed mph=65
|cruise speed kmh=80
|cruise speed mph=50
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=370
|range miles=230
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|climb rate ms=
|climb rate ftmin=
|armament1=
|armament2=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
* [[List of aircraft of World War II]]
}}

==Notes==
{{reflist|2}}

==References==
{{commons category|Marinens Flyvebaatfabrikk M.F.8}}
* {{cite book |last= Hafsten |first= Bjørn |author2=Tom Arheim |title=Marinens Flygevåpen 1912&ndash;1944 |year=2003 |publisher=TankeStreken AS |location=Oslo |pages=45, 49, 101&ndash;102, 225&ndash;226 |language=Norwegian |ISBN =82-993535-1-3 }}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London |pages= }}
* {{cite web |last=Stein |first=Gulli|title=Skoleflyet MF-12 |work=Borreminne |publisher=Borre Historielag |url=http://borreminne.hive.no/aargangene/1998_99/08-skoleflyet.htm |accessdate=2008-10-23 |language=Norwegian}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
<!-- ==External links== -->

{{Marinens Flyvebaatfabrikk aircraft}}

[[Category:Norwegian military trainer aircraft 1920–1929]]
[[Category:Norwegian military trainer aircraft 1930–1939]]
[[Category:Floatplanes]]
[[Category:Marinens Flyvebaatfabrikk aircraft|MF08]]
[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Propeller aircraft]]