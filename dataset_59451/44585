{{Use mdy dates|date=July 2013}}
{{Infobox person
 | name             = Katee Sackhoff
 | image            = Katee Sackhoff by Gage Skidmore.jpg
 | imagesize        = 
 | caption          = Sackhoff at the 2013 [[San Diego Comic-Con International]]
 | birthname        = Kathryn Ann Sackhoff
 | birth_date       = {{birth date and age|1980|4|8}}
 | birth_place      = [[Portland, Oregon|Portland]], [[Oregon]], United States
 | othername        = 
 | occupation       = Actress
 | yearsactive      = 1998–present
 | homepage         = 
 | spouse           =
 | partner           = [[Karl Urban]] (2014–present)
}}
'''Kathryn Ann "Katee" Sackhoff''' (born April 8, 1980) is an American actress best known for playing [[Kara Thrace|Lieutenant Kara "Starbuck" Thrace]] on the [[Syfy|Sci Fi Channel]]'s television program ''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]'' (2003–2009). She was nominated for four [[Saturn Award]]s for her work on ''Battlestar Galactica'', winning the award for [[Saturn Award for Best Supporting Actress on Television|Best Supporting Actress on Television]] in 2005.

Sackhoff has also starred in the short-lived TV series ''[[The Fearing Mind]]'' (2000–2001) and ''[[The Education of Max Bickford]]'' (2001–2002); had recurring roles in the TV series ''[[Bionic Woman (2007 TV series)|Bionic Woman]]'' (2007), ''[[Robot Chicken]]'' (2007–2016), ''[[Nip/Tuck]]'' (2009), ''[[CSI: Crime Scene Investigation]]'' (2010–2011) and ''[[Star Wars: The Clone Wars (2008 TV series)|Star Wars: The Clone Wars]]'' (2012–2013); and had a lead role in the [[24 (season 8)|eighth season]] of ''[[24 (TV series)|24]]'' as [[Minor government agents in 24#Dana Walsh|Dana Walsh]] (2010). She voices several characters including B*tch Puddin' on [[Adult Swim]]'s stop motion animation TV show ''Robot Chicken'' (TV series 2005–present). Since 2012 she has been starring in the [[Netflix]] series ''[[Longmire (TV series)|Longmire]]'' as Deputy Sheriff Victoria "Vic" Moretti.<ref>{{cite news|url=http://variety.com/2014/digital/news/netflix-picks-up-longmire-for-season-4-1201360562/ |title=Netflix Picks up 'Longmire' for Season 4 |work=[[Variety (magazine)|Variety]] |accessdate=February 28, 2015}}</ref>

She had lead roles in the films ''[[Halloween: Resurrection]]'' (2002); ''[[White Noise: The Light]]'' (2007); ''[[Batman: Year One (film)|Batman: Year One]]'' (2011); ''[[The Haunting in Connecticut 2: Ghosts of Georgia]]'', ''[[Sexy Evil Genius]]'' and ''[[Riddick (film)|Riddick]]'' (2013), and a starring role in ''[[Oculus (film)|Oculus]]''.

==Early life==
Sackhoff was born in [[Portland, Oregon|Portland]], [[Oregon]],<ref>{{cite web|url=http://www.wweek.com/portland/article-7900-katee_sackhoff.html|work=Willamette Week|title=Katee Sackhoff: The Portland-born actress talks Beaverton, Battlestar and bionic boobs|date=2007-10-24|author=Crawford, William|accessdate=2015-08-16}}</ref> and grew up in [[St. Helens, Oregon|St. Helens]], Oregon. Her mother, Mary, worked as an [[English language learning and teaching|ESL]] program coordinator, and her father, Dennis, is a land developer.<ref>{{cite web|url=http://www.filmreference.com/film/46/Katee-Sackhoff.html |title=Katee Sackhoff Film Reference biography|publisher=Filmreference.com |accessdate=April 18, 2010}}</ref>

Her brother, Erick, is co-owner of a vehicle modification shop near Portland.<ref>{{cite web|last=Uno |first=Wesley |url=http://www.oregonlive.com/washingtoncounty/index.ssf/2008/12/at_portland_speed_industries_c.html |title=At Portland Speed Industries, car dreams become a reality |work=The Oregonian |date=December 18, 2008 |accessdate=February 21, 2012}}</ref><ref>{{cite web|url=http://tunedbypsi.com/staff.php |title=Staff – Portland Speed Industries |publisher=Tunedbypsi.com |accessdate=September 9, 2012}}</ref> She graduated from [[Sunset High School (Beaverton)|Sunset High School]] in [[Beaverton, Oregon|Beaverton]] in 1998. She began swimming at an early age and by high school was planning to pursue a career in the sport until her right knee was injured. This led her to begin practicing [[yoga]]—which she continues today—and to pursue an interest in acting.<ref>{{cite news |last=Marks |first=Joshua |date=July 11, 2006 |title=Katee Sackhoff: Yoga |url=http://variety.com/2006/scene/markets-festivals/katee-sackhoff-2-1200340136/ |work=[[Variety (magazine)|Variety]] |access-date=August 28, 2016}}</ref>

==Career==
[[File:KateeSackhoff.jpg|thumb|left|Sackhoff at the 2008 Wizard World Convention]]
Her first acting role was in the Lifetime movie ''[[Fifteen and Pregnant]]'' in which she played a teenager with a baby. The movie starred [[Kirsten Dunst]] and motivated her to move to Hollywood and pursue a career in acting after graduating high school. Sackhoff's first recurring role was Annie in MTV's ''[[Undressed (TV series)|Undressed]]'', next gaining a supporting role as Nell Bickford in ''[[The Education of Max Bickford]]''.  Sackhoff made her motion picture debut in ''[[My First Mister]]'', and next appeared in film as Jenna "Jen" Danzig in ''[[Halloween: Resurrection]]''.

Sackhoff's most notable role is as [[Kara Thrace|Kara "Starbuck" Thrace]] in the miniseries and follow-up TV series ''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]'', for which she won a [[Saturn Award]] in 2006 for Best Actress on Television. The actress's persona led the writers to develop the character of Starbuck as a more volatile and complex character.  ''Galactica'' executive producer [[Ronald D. Moore|Ron Moore]] described her as having magnetism while producer [[David Eick]] expanded stating: "We saw this whole other side that was all because of Katee: vulnerability, insecurity, desperation. We started freeing ourselves up to explore the weakness of the character, because we knew Katee could express those things without compromising the character's strength."<ref name="Catch">Jensen, Jeff and Vary, Adam B. (April 4, 2008). "Catch a Rising Starbuck". ''[[Entertainment Weekly]]''. Issue 985</ref> Sackhoff said her performance was inspired by [[Linda Hamilton]]'s portrayal of [[Sarah Connor (Terminator)|Sarah Connor]] in ''[[Terminator 2: Judgment Day]]'':  "I think that was the one character that I kind of looked to as far as body image and strength. I think I looked to her character and said, 'OK, that's kind of what you need to embody.{{' "}}<ref>{{cite web|url=http://www.scifi.com/scifiwire/art-sfc.html?2003-07/10/13.00.sfc |title=''T2'' Inspired Sackhoff's Starbuck |publisher=Sci Fi Wire ([[Syfy|Sci Fi Channel]]) |date=July 10, 2003 |archiveurl=https://web.archive.org/web/20030804021552/http://www.scifi.com/scifiwire/art-sfc.html?2003-07%2F10%2F13.00.sfc |archivedate=August 4, 2003 |deadurl=yes |df=mdy }}</ref>
Toward the end of the filming of ''Battlestar Galactica'', Sackhoff began feeling physically weak. Soon after filming wrapped, she was diagnosed with [[thyroid neoplasm|thyroid cancer]]. After surgery to remove her thyroid, she required no radiation treatments and by February 2009 was in remission.<ref>{{cite web|url=http://www.dose.ca/tv/story.html?id=8a74aed8-54e2-4140-804f-363ca4d8d0f7 |title=Interview: Starbuck Steps It Up |publisher=Dose.ca|date=February 6, 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20110914011032/http://www.dose.ca/tv/story.html?id=8a74aed8-54e2-4140-804f-363ca4d8d0f7 |archivedate=September 14, 2011 }}</ref>
In 2007, Sackhoff was cast as the evil [[cyborg]] Sarah Corvus in the short-lived NBC series ''[[Bionic Woman (2007 TV series)|Bionic Woman]]''. David Eick, executive producer for the show, stated, "She's a very special find. Those actors who can combine the qualities of strength and vulnerability—they usually call those people movie stars."<ref name="Catch" />
Sackhoff plays the female lead in the action/sci-fi movie ''[[The Last Sentinel]]'' and the supernatural thriller ''[[White Noise: The Light]]''.

[[File:Katee Sackhoff May 2015.jpg|thumb|right|Katee Sackhoff in May 2015]]
Sackhoff appears as the main character in the Lifetime Original Movie ''[[How I Married My High School Crush]]''.<ref>{{cite web|url=http://www.lmn.tv/movies/details.php?id=MOVE+4107 |title=How I Married My High School Crush |publisher=LMN.tv |accessdate=August 20, 2011}}</ref> She has made guest appearances in ''[[Cold Case (TV series)|Cold Case]]'', ''[[ER (TV series)|ER]]'', ''[[Law & Order]]'', and ''[[Robot Chicken]]''.
Sackhoff provided the voice of a female marine in the video game ''[[Halo 3]]'' and is featured in the viral marketing campaign for ''[[Resistance 2]]''. In 2011, she provided the voice for Black Cat 2099 in ''[[Spider-Man: Edge of Time]]''. She voiced Sarah Essen in the DC Comics animated film, ''[[Batman: Year One]]''.
She appears in four episodes of the fifth season of the series ''[[Nip/Tuck]]'' playing a new doctor, Dr. Theodora Rowe.<ref>{{cite web|url=http://community.tvguide.com/blog-entry/TVGuide-News-Blog/Todays-News/Katee-Sackhoff-Niptuck/800039967 |title=Katee Sackhoff: From Starbuck to Nip/Tuck |work=TV Guide |deadurl=yes |archiveurl=https://web.archive.org/web/20080521015241/http://community.tvguide.com/blog-entry/TVGuide-News-Blog/Todays-News/Katee-Sackhoff-Niptuck/800039967 |archivedate=May 21, 2008 }}</ref><ref>{{cite news|url=http://featuresblogs.chicagotribune.com/entertainment_tv/2009/02/battlestar-galactica-katee-sackhoff.html |title='Battlestar' and 'Caprica' notes, plus video of Katee on 'Nip/Tuck' |work=Chicago Tribune |date=February 6, 2009 |accessdate=August 20, 2011}}</ref> However, for the sixth season Sackhoff was replaced by [[Rose McGowan]] for the role due to scheduling conflicts.<ref>{{cite web|last=Maerz |first=Melissa |url=http://ausiellofiles.ew.com/2008/09/rose-mcgowan-re.html |title=Rose McGowan In, Katee Sackhoff Out On 'Nip/Tuck' |work=Entertainment Weekly  |date=September 12, 2008 |accessdate=August 20, 2011}}</ref><ref>{{cite web|url=http://community.livejournal.com/_nip_tuck/70829.html |title=nip_tuck: Casting: Both Katee Sackhoff and Rose McGowan to play Teddy Lowe? |publisher=Community.livejournal.com |date=December 10, 2008 |accessdate=April 18, 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20110603235912/http://community.livejournal.com/_nip_tuck/70829.html |archivedate=June 3, 2011 |df=mdy-all }}</ref>
Sackhoff headlined NBC's [[Dick Wolf]]-produced cop drama ''Lost and Found'' as Tessa, "an offbeat female LAPD detective who, after butting heads with the higher-ups, is sent as a punishment to the basement to work on John Doe and Jane Doe cases." The pilot was filmed in January 2009. NBC decided not to pick up the series.<ref>[http://www.seattlepi.com/tvguide/406118_tvgif12.html ]{{dead link|date=October 2009}}</ref>
In 2009, she appeared as herself in "[[List of The Big Bang Theory episodes#ep49|The Vengeance Formulation]]" episode of the [[CBS]] situation comedy ''[[The Big Bang Theory]]''. In the episode, she is fantasized as [[Howard Wolowitz]]'s dream girl.<ref>{{cite news |last=Stanhope |first=Kate | title=  Katee Sackhoff to Appear on The Big Bang Theory| url= http://www.tvguide.com/News/Katee-Sackhoff-Appear-1011117.aspx | work=TV Guide}}</ref> She appears again in season 4, in the same role.
Sackhoff appeared as a series regular in the eighth season of the TV series ''[[24 (season 8)|24]]'', playing [[Minor government agents in 24#Dana Walsh|Dana Walsh]], a [[Counter Terrorist Unit|CTU]] data analyst with a secret.
In February 2010, Sackhoff signed on to play the lead in an ABC crime drama pilot, ''Boston's Finest''. ABC decided not to pick up the series.<ref>{{cite web|url=http://www.tvguide.com/News/Katee-Sackhoff-Signs-1015303.aspx|title=Katee Sackhoff Signs On to ABC Crime Drama|work=TV Guide}}</ref>

Sackhoff is set to star in the action–werewolf thriller ''Growl''.<ref>{{cite web|url=http://www.shocktillyoudrop.com/news/topnews.php?id=15360 |title=Exclusive Photos: Katee Sackhoff & More in Growl |publisher=Shocktillyoudrop.com |date=June 1, 2010 |accessdate=August 20, 2011}}</ref> She made a special appearance in the ''[[Futurama]]'' episode "[[Lrrreconcilable Ndndifferences]]".<ref name="Digital Spy">{{cite web |url=http://www.digitalspy.com/ustv/news/a264941/katee-sackhoff-to-guest-on-futurama.html|title=Katee Sackhoff to guest on 'Futurama'|first=Mike |last=Moody |date=August 20, 2010 |work=[[Digital Spy]] |accessdate=August 20, 2010}}</ref>
In the fall of 2010, Sackhoff joined the cast of ''[[CSI: Crime Scene Investigation]]'' as Detective Reed, a smart investigator who does not do well with sensitivity.<ref>{{cite web|url= http://www.tvguide.com/News/Katee-Sackhoff-CSI-1022713.aspx |title=CSI Books Battlestar Galactica's Katee Sackhoff|work=TV Guide|accessdate=September 8, 2010}}</ref>
In 2011, Sackhoff guest starred in an episode of ''[[Workaholics]]'' as a homeless drug addict named Rachel.
Sackhoff co-stars as the lead female role in ''[[Longmire (TV series)|Longmire]]'', an [[A&E (TV channel)|A&E]]/[[Netflix]] television series based on the novels by [[Craig Johnson (author)|Craig Johnson]]. Sackhoff plays Sheriff's Deputy Vic Moretti.<ref>{{cite web|url=http://www.tvline.com/2011/03/katee-sackhoff-longmire-pilot/|title=Pilot Scoop: Katee Sackhoff, Smallville Vet, Others Join A&E's Longmire|publisher=TVLine}}</ref><ref>{{cite news|url=http://www.hollywoodreporter.com/live-feed/battlestar-galactica-star-katee-sackhoff-171607|title='Battlestar Galactica' Star Katee Sackhoff Lands A&E Pilot|work=The Hollywood Reporter| first=Lesley|last=Goldberg|date=March 25, 2011}}</ref>
Sackhoff played Dahl, a mercenary hired to track down Vin Diesel's intergalactic convict character in ''[[Riddick (film)|Riddick]]''.<ref>{{cite web|last=Radish|first=Christina|url= http://collider.com/katee-sackhoff-riddick-casting-story-interview/137829/ |title=Katee Sackhoff Talks RIDDICK; Reveals the Wild Story on How She Got Cast|publisher=Collider.com|date=January 13, 2012|accessdate=December 13, 2012}}</ref>

In August 2012, Sackhoff became the co-host of the [[Schmoes Know Movies]] podcast on The Toad Hop Network. One of her first shows was with guest [[Sean Astin]].<ref>{{cite web|url=http://www.toadhopnetwork.com/f/Schmoes |title=Radio Worth Watching: Schmoes Know Movies Episodes Guide |publisher=The Toad Hop Network |accessdate=September 9, 2012}}</ref>

Sackhoff announced in April 2015 a new TV series project, ''Rain'', which she wrote and is executive producing though her Fly Free Productions.<ref name=Rain>{{cite news|title=Katee Sackhoff To Star In & Create Futuristic TV Series For Reunion Pictures|url=http://deadline.com/2015/04/katee-sackhoff-star-create-rain-futuristic-series-reunion-pictures-1201408997/|accessdate=26 June 2015|work=Deadline Hollywood|date=13 April 2015}}</ref>  She portrayed Pink Ranger Kimberly in ''[[Power/Rangers]]'', a short depicting a [[dystopian]] future in the ''[[Power Rangers]]'' universe.<ref>{{cite web|url=http://www.vulture.com/2015/02/watch-an-unofficial-power-rangers-deboot.html|title=What If Power Rangers Were R-Rated and Starred James Van Der Beek?|date=February 24, 2015|work=vulture.com|accessdate=January 4, 2017}}</ref>

==Personal life==
Sackoff and her ''Battlestar Galactica'' co-star [[Tricia Helfer]] co-founded the Acting Outlaws, a motorcycle-riding charity with which they have worked to raise awareness and money and for causes and organizations including the Gulf Restoration Network, the [[The Humane Society of the United States|Humane Society]], the [[Red Cross]] [[amfAR]].<ref>Radish, Christina (November 21, 2012). [http://collider.com/katee-sackhoff-acting-outlaws-riddick-female-expendables-movie/ "Katee Sackhoff Talks ACTING OUTLAWS with Tricia Helfer, RIDDICK and Why It’s Rated R, the All-Female EXPENDABLES Movie, and More"]. [[Collider (website)|Collider]].</ref><ref>Dowling, Dar (October 16, 2014). [http://www.huffingtonpost.com/dar-dowling/acting-outlaws-battlestar_b_5986416.html "Acting Outlaws: ''Battlestar Galactica''’s Katee Sackhoff Riding Full Throttle for a Cause"]. ''[[The Huffington Post]]''.</ref>

She has been dating actor [[Karl Urban]] since November 2014.<ref>Glucina, Rachel (January 1, 2015). [http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=11383763 "The Diary: Karl Urban's new romance gets serious"]. ''[[The New Zealand Herald]]''.</ref><ref>[http://www.eonline.com/photos/19201/comic-con-2016-star-sightings/711001 "Comic-Con 2016: Star Sightings: Katee Sackhoff & Karl Urban"]. [[E! News]]. July 2016.</ref>

Of her practice of [[Transcendental Meditation technique|Transcendental Meditation]], begun in 2015, Sackhoff has said, "What it taught me was that you can't fail at meditation."<ref>{{cite web|title=Transcendental Meditation & Seeing Beyond|url=https://player.fm/series/me-paranormal-you-77322/experience-147-katee-sackhoff-transcendental-meditation-seeing-beyond|website=Me & Paranormal You|accessdate=20 November 2016|archiveurl=http://www.webcitation.org/6mAS6aKzI?url=https://player.fm/series/me-paranormal-you-77322/experience-147-katee-sackhoff-transcendental-meditation-seeing-beyond|archivedate=20 November 2016|date=18 November 2016|quote=I meditate every day, for 20 minutes twice a day ... Transcendental Meditation [10–11']}}</ref>

== Filmography ==

===Film===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
|2001
|''[[My First Mister]]''
|Ashley
|
|-
|2002
|''[[Halloween: Resurrection]]''
|Jen
|
|-
|2007
|''[[White Noise: The Light]]''
|Sherry Clarke
|Main role
|-
|2007
|''{{sortname|The|Last Sentinel|nolink=1}}''
|Girl
|
|-
|2011
|''[[Batman: Year One (film)|Batman: Year One]]''
|Det. [[Sarah Essen]] (voice)
|Video
|-
|2012
|''Campus Killer''
|Suzanne
|
|-
|2013
|''{{sortname|The|Haunting in Connecticut 2: Ghosts of Georgia}}''
|Joyce
|
|-
|2013
|''[[Sexy Evil Genius]]''
|Nikki Franklyn
|
|-
|2013
|''[[Riddick (film)|Riddick]]''
|Dahl
|
|-
|2014
|''[[Oculus (film)|Oculus]]''
|Marie Russell
|
|-
|2014
|[[Tell (2014 film)|''Tell'']]
|Beverley
|
|-
|2015
|''[[Power/Rangers]]''
|Kimberly Scott (née Hart)
|Short film
|-
| 2015
| ''Girl Flu''
| Jenny
|
|-
| 2016
| ''[[Don't Knock Twice (2016 film)|Don't Knock Twice]]''
|Jess
|
|-
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
|1998
|''[[Fifteen and Pregnant]]''
|Karen Gotarus
|TV film
|-
|1999
|''Locust Valley''
|Claire Shaw
|TV film
|-
|1999
|''[[Zoe, Duncan, Jack and Jane]]''
|Susan
|Episode: "Sympathy for Jack"
|-
|1999
|''Chicken Soup for the Soul''
|Claire
|Episode: "Starlight, Star Bright"
|-
|1999
|''Hefner: Unauthorized''
|Mary
|TV film
|-
|2000
|''[[Undressed]]''
|Annie
|4 episodes
|-
|2000–2001
|''{{sortname|The|Fearing Mind}}''
|Lenore Fearing
|13 episodes
|-
|2001–2002
|''{{sortname|The|Education of Max Bickford}}''
|Nell Bickford
|22 episodes
|-
|2002
|''[[ER (TV series)|ER]]''
|Jason's Girlfriend
|Episode: "A Hopeless Wound"
|-
|2003
|''[[Battlestar Galactica (TV miniseries)|Battlestar Galactica]]''
|[[Kara Thrace|Kara "Starbuck" Thrace]]
|TV miniseries
|-
|2003
|''[[Boomtown (2002 TV series)|Boomtown]]''
|Holly
|Episode: "The Big Picture"
|-
|2004
|''[[Cold Case]]''
|Terri Maxwell (1969)
|Episode: "Volunteers"
|-
|2004–2009
|''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]''
|Kara "Starbuck" Thrace
|71 episodes
|-
|2007
|''{{sortname|The|Wedding Wish|nolink=1}}''
|Sara Jacob
|TV film
|-
|2007
|''[[Bionic Woman (2007 TV series)|Bionic Woman]]''
|Sarah Corvus
|5 episodes
|-
|2007
|''[[Battlestar Galactica: Razor]]''
|Capt. Kara "Starbuck" Thrace
|TV film
|-
|2007–2016
|''[[Robot Chicken]]''
|Various (voice)
|10 episodes
|-
|2008
|''[[Law & Order]]''
|Dianne Cary
|Episode: "Knock Off"
|-
|2009
|''Lost and Found''
|Tessa Cooper
|TV pilot
|-
|2009
|''[[Nip/Tuck]]''
|Dr. Theodora "Teddy" Rowe
|4 episodes
|-
|2009, 2010
|''{{sortname|The|Big Bang Theory}}''
|Herself
|2 episodes
|-
|2010
|''Boston's Finest''
|Julia Scott
|TV film
|-
|2010
|''[[24 (TV series)|24]]''
|[[Minor government agents in 24#Dana Walsh|Dana Walsh]]
|20 episodes
|-
|2010
|''[[Futurama]]''
|Grrrl (voice)
|Episode: "Lrreconcilable Ndndifferences"
|-
|2010–2011
|''[[CSI: Crime Scene Investigation]]''
|Det. Frankie Reed
|3 episodes
|-
|2011
|''{{sortname|The|Super Hero Squad Show}}''
|[[She-Hulk]] (voice)
|Episode: "So Pretty When They Explode!"
|-
|2011
|''[[Workaholics]]''
|Rachel
|Episode: "Karl's Wedding"
|-
|2012–2013
|''[[Star Wars: The Clone Wars (2008 TV series)|Star Wars: The Clone Wars]]''
|Bo-Katan (voice)
|4 episodes
|-
|2012–Present
|''[[Longmire (TV series)|Longmire]]''
|Victoria "Vic" Moretti
|43 episodes
|-
|2015
|''Rain''
|
|in production(writer, exec prod)
|-
|2016
|''[[Star Wars Rebels]]''
|Bo-Katan Kryze (voice)
|}

===Video games===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
|2007
|''[[Halo 3]]''
|Female Marine 3 (voice)
|Video game
|-
|2008
|''[[Resistance 2]]''
|Cassie Aklin (voice)
|Video game
|-
|2011
|''[[Spider-Man: Edge of Time]]''
|[[Black Cat (comics)|Black Cat 2099]] (voice)
|Video game
|-
|2015
|''[[Call of Duty: Black Ops III]]''
|Sarah Hall
|Voice role<br />[[Motion capture]] performance
|-
|2016
|''[[Eve: Valkyrie]]''
|Rán Kavik
|Voice role
|}

==Awards and nominations==
{|class="wikitable sortable"
|-
! Year
! Award
! Category
! Nominated work
! Result
|-
|2003
|rowspan = '4'|[[Saturn Award]]
|[[Saturn Award for Best Supporting Actress on Television|Best Supporting Actress in a Television Series]]
|rowspan = '4'|''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]''
|{{nom}}
|-
|2005
|[[Saturn Award for Best Supporting Actress on Television|Best Supporting Actress in a Television Series]]
|{{won}}
|-
|2006
|[[Saturn Award for Best Actress on Television|Best Actress on Television]]
|{{nom}}
|-
|2008
|[[Saturn Award for Best Supporting Actress on Television|Best Supporting Actress in a Television Series]]
|{{nom}}
|-
|[[2010 Teen Choice Awards|2010]]
|[[Teen Choice Awards]]
|[[Teen Choice Award for Choice TV Actress Action|Choice TV Actress: Action]]
|''[[24 (TV series)|24]]''
|{{nom}}
|-
|2015
|[[Fangoria|Fangoria Chainsaw Awards]]
|[[Fangoria|Best Supporting Actress]]
|''[[Oculus (film)|Oculus]]''
|{{won}}
|-
|}

==References==
{{reflist|2}}

==External links==
{{Commons category|Katee Sackhoff}}
* {{official website|http://www.kateesackhoff.com}}
* {{IMDb name|0755267|Katee Sackhoff}}
* {{TV Guide person |katee-sackhoff/190292}}
* [http://www.post-gazette.com/pg/07085/770732-352.stm Tuned in Interview with Ronald D. Moore]
* [http://www.thescifiworld.net/interviews/katee_sackhoff_01.htm Katee Sackhoff interview at The Scifi World]
* [http://entertainment.aol.ca/article/qa-katee-sackhoff-on-the-end-of-battlestar-galactica/566827/ Katee Sackhoff interview with AOL Canada]

{{Saturn Award for Best Supporting Actress on Television}}

{{Authority control}}

{{DEFAULTSORT:Sackhoff, Katee}}
[[Category:1980 births]]
[[Category:20th-century American actresses]]
[[Category:21st-century American actresses]]
[[Category:Actresses from Oregon]]
[[Category:Actresses from Portland, Oregon]]
[[Category:American film actresses]]
[[Category:American podcasters]]
[[Category:American television actresses]]
[[Category:American video game actresses]]
[[Category:American voice actresses]]
[[Category:Cancer survivors]]
[[Category:Living people]]
[[Category:People from St. Helens, Oregon]]
[[Category:Sunset High School (Beaverton, Oregon) alumni]]
[[Category:Transcendental Meditation practitioners]]