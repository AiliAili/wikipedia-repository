{{Use dmy dates|date=July 2014}}
{{Infobox company
|name       = CohnReznick
|logo       = CohnReznick_logo.jpg
|type       = [[Limited Liability Partnership]]
|area_served= Worldwide
|key_people = Frank Longobardi, [[Certified Public Accountant|CPA]] {{small|([[CEO|CEO(official)]])}} 
|industry   = [[Professional services]]
|revenue                  = US $500 Million (2014)<ref name="accounting today">{{cite web|url=http://digital.accountingtoday.com/accountingtoday/top_100_firms_supplement_2014#pg1 |publisher=Accounting Today Magazine |title=Accounting Today Top 100 Firms }}</ref>
|num_employees            = 2500+ (2014)<ref name="Facts and Figures">{{cite web|url=http://www.cohnreznick.com/about/facts-and-figures-0 |publisher=CohnReznick |title=Facts and Figures }}</ref>
|divisions          = [[Consulting firm|Advisory]], [[Tax advisor|Tax]], [[Financial Audit|Audit]]
|homepage                 = {{URL|www.cohnreznick.com}}
|foundation = 2012; combination of J.H Cohn and Reznick Group <ref name="cr history">{{cite web|url=http://www.cohnreznick.com/about/our-history |publisher=CohnReznick |title= Our History }}</ref>
|location   =[[New York City]], [[New York (state)|New York]], United States<ref name="CohnReznick Locations">[http://www.cohnreznick.com/office-locations CohnReznick HQ]</ref>
}}

'''CohnReznick LLP''' is a national [[professional services]] firm headquartered in New York, NY. It is the tenth largest public accounting firm in the United States based on aggregated 2013 revenues.<ref name="cr history" />

The Firm’s services are aligned in three segments: accounting, tax, and advisory. It has 26 offices, 2,500 employees as of May 1, 2014, and reported annual revenue of $508 million (for fiscal year ending 1/31/14).<ref name="CohnReznick Locations" /> CohnReznick was formed in 2012 through a combination of two firms J.H. Cohn (Roseland, NJ) and Reznick Group (Bethesda, MD).<ref name="accounting today" /> J.H. Cohn was founded in 1919 in Newark, NJ by Julius H. Cohn. Reznick Group, formerly Reznick, Fedder & Silverman, was formed in 1977 in Washington, DC by three partners – David Reznick, Stuart Fedder, and Ivan Silverman.

== International ==
CohnReznick is a member of Nexia International, a worldwide network of independent accounting and consulting firms. The Nexia network includes more than 23,000 professionals operating in over 100 countries.<ref name="Facts and Figures" />

==Employer Recognition==
Recent recognition for CohnReznick as an employer includes:
* Among the 2014 Best Accounting Firms for Women<ref>[http://afwa.org/2014/05/27/2014-best-accounting-firms-women/\#.U7we_PldX5U-http://afwa.org/2014/05/27/2014-best-accounting-firms-women/]</ref>
* Among the 2015 Vault Accounting 50 of the best accounting employers in North America<ref>[http://www.vault.com/company-rankings/accounting/vault-accounting-50/-http://www.vault.com/company-rankings/accounting/vault-accounting-50/]</ref>
* Among the 2014 Best Places to Work in Austin<ref>[http://www.bizjournals.com/austin/news/2014/05/20/austins-best-places-to-work-for-2014.html?page=all-http://www.bizjournals.com/austin/news/2014/05/20/austins-best-places-to-work-for-2014.html?page=all]</ref>
* Among the Best in Career Development by [[Crain's Chicago Business|Crain’s Chicago Business]]<ref>[http://www.chicagobusiness.com/article/20140404/ISSUE02/304059996/best-places-for-career-development\#-http://www.chicagobusiness.com/article/20140404/ISSUE02/304059996/best-places-for-career-development]</ref>

==Community Service==
In 2013 CohnReznick introduced CohnReznick CARES, an umbrella program for the charitable activities and initiatives it supports on both a national and office (local) level. On a national basis, CohnReznick supports four major charities:

* [http://www.joetorre.org/ Joe Torre Safe At Home Foundation]
* [[Make-A-Wish Foundation]]<ref>[http://www.accountingtoday.com/acto_blog/cohnreznick-grants-wishes-bowl-a-thon-67683-1.html-http://www.accountingtoday.com/acto_blog/cohnreznick-grants-wishes-bowl-a-thon-67683-1.html]</ref>
* [https://morechildren.org/ Medhen Orphan Relief Effort (M.O.R.E.)]
* [[Special Operations Warrior Foundation]]<ref>[http://www.specialops.org/news/129199/CohnReznick-Golf-Outing-Raises-75000-for-SOWF.htm-http://www.specialops.org/news/129199/CohnReznick-Golf-Outing-Raises-75000-for-SOWF.htm]</ref>

CohnReznick was honored for "Impressive Good Works" by the Commerce and Industry Association of New Jersey.<ref>[http://www.cianj.org/events/chairmans-reception-saluting-the-generosity-of-business-because-caring-works/-http://www.cianj.org/events/chairmans-reception-saluting-the-generosity-of-business-because-caring-works/]</ref> The Firm was also ranked at #26 by the [[American City Business Journals|Washington Business Journal]] for Corporate Philanthropy based on 2013 Metro-Area Giving.<ref>[http://www.bizjournals.com/washington/event/78821-http://www.bizjournals.com/washington/event/78821]</ref>

== References ==
{{Reflist|30em}}

== External links ==
*{{Official website|http://www.cohnreznick.com/}}

[[Category:Companies established in 1919]]
[[Category:International management consulting firms]]
[[Category:Accounting firms of the United States]]
[[Category:Companies based in New York City]]