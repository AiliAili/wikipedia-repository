'''Jarosław Kapuściński''' (Polish/Canadian: [jaˈrɔswaf kapuɕˈt͡ɕiɲski] ) (born December 12, 1964 in [[Warsaw]], [[Poland]]) is a composer and pianist specializing in [[intermedia]]. He is Associate Professor of Music at [[Stanford University]],<ref>{{cite web|title=Jaroslaw Kapuscinski|url=https://music.stanford.edu/people/jaroslaw-kapuscinski/|publisher=Stanford University}}</ref> regularly teaching at the Center for Computer Research in Music and Acoustics (CCRMA).<ref>{{cite web|title=Jaroslaw Kapuscinski|url=https://ccrma.stanford.edu/people/jarek-kapuscinski/|publisher=Stanford Center for Computer Research in Music and Acoustics}}</ref>

Kapuściński trained as a classical pianist and composer at the Chopin Academy of Music in Warsaw  (now the [[Fryderyk Chopin University of Music]]) and first engaged in video art and animation during a residency at the [[Banff Centre]] in Canada (1988–89). He developed that area of work during doctoral studies at the [[University of California, San Diego]] (1992-1997) and as a postdoctoral student at [[McGill University]] in Montreal (2001).<ref>{{cite web|title=Jaroslaw Kapuscinski|url=http://culture.pl/pl/tworca/jaroslaw-kapuscinski/|publisher=CULTURE.PL}}</ref>

In addition to his work at Stanford, Kapuściński has taught at McGill University (2001)<ref>{{cite web|title=Jaroslaw Kapuscinski: Composing and performing audio-visually|url=http://www.cirmmt.org/activities/seminars/Kapuscinski/|publisher=CIRMMT}}</ref> and the [[University of the Pacific (United States)]] (2004-2008). He has lectured internationally on intermedia composition and performance, at [[IRCAM]] in [[Paris]],<ref>{{cite web|title=Research & Creativity meeting : the composer Jaroslaw Kapuscinski|url=http://forumnet.ircam.fr/espresso_event/research-creativity-meeting-the-composer-jaroslaw-kapuscinski//|publisher=Forumnet IRCAM}}</ref> the Centre for Interdisciplinary Research in Music Media and Technology at McGill University, at the [[University of Oxford]],<ref>{{cite web|title=The Composer Speaks|url=http://www.music.ox.ac.uk/event/the-composer-speaks/|publisher=Oxford University}}</ref> [[Tokyo University of the Arts]], and at [[Columbia University]], among others. He co-authored with François Rose a multi-lingual website about orchestration in  the Japanese traditional court music, [[Gagaku]].<ref>{{cite web|title=Orchestration in Gagaku Music|url=https://ccrma.stanford.edu/groups/gagaku/|publisher=Stanford University’s Freeman Spogli Institute for International Studies Japan}}</ref>

==Selected compositions and projects==

===''Mondrian Variations''===
This piece, produced in 1992 at [[Institut national de l'audiovisuel]] (INA) in Paris and revised in 2011, is an audiovisual exploration of artwork by [[Piet Mondrian]].  The work has awards from the Locarno VideoArt Festival (1992), the Festival du Film d'Art de l'UNESCO, and most recently First Prize at the Fresh Minds Festival.<ref>{{cite web|title=First Prize, 2013-2013|url=http://perf.tamu.edu/freshminds/2014/mondrian-variations/|publisher=The Fresh Minds Festival}}</ref>

===''Where is Chopin?''===
This performance/installation, created in 2010 for the bicentennial of [[Chopin]]'s birth, brings together video, and recomposed music based on Chopin's Preludes Op. 28. It explores the impact of Chopin's work in the minds and on the faces of listeners from around the world.<ref>{{cite web|title=A Polish 'Nationalist' Whose Music Also Resonates Across China|url=https://www.nytimes.com/2010/07/06/arts/06iht-chopin.html/|publisher=The New York Times}}</ref>  To create the work, Kapuściński traveled internationally, interviewing and videotaping the participants.  ''Where is Chopin?'' has been presented around the world. The installation version utilizes 3 screen video projection, stereo sound, and [[Disklavier]] piano.<ref>{{cite web|title=Professor Documents the Emotional Impact of Chopin's Music|url=http://museum.stanford.edu/news_room/where-is-chopin.html/|publisher=Cantor Arts Center at Stanford University}}</ref>

===''Juicy''===
Composed in 2010 this work for piano and computer controlled image and sound was produced using stop motion animation of fruit. It has been performed internationally, including by [[Jenny Q. Chai]] at Spectrum in New York, CNMAT in Berkeley, the Festival Leo Brouwer de Música de Cámara in Havana, Cuba, at the China Shanghai International Arts Festival. Kapuściński has performed the work at over 25 venues including the [http://www.tokyo-ws.org/english/archive/2012/12/tokyo-experimental-festival-soundart-performancevol7.shtml Tokyo Experimental Festival], the [https://www.facebook.com/pages/Re-new-digital-arts-festival-conference/162474673811981 Re-New Digital Arts Festival] in [[Copenhagen]], the [http://www.sarc.qub.ac.uk/ Sonic Arts Research Centre] (SARC) in [[Belfast]], [[Run Run Shaw Creative Media Centre]] at [[City University of Hong Kong]] and [[EMPAC]] at the [[Rensselaer Polytechnic Institute]].<ref>{{cite web|title=Jaroslaw Kapuscinski "Catch the Tiger" @ EMPAC, 4/17/11|url=http://blog.timesunion.com/localarts/jaroslaw-kapuscinski-catch-the-tiger-empac-41711/15744/|publisher=The Times Union}}</ref> ''Juicy'' has been released as a [http://www.amazon.com/Juicy-Works-Piano-Mondrian-Variations/dp/B005CSBU3G/ DVD], along with ''Mondrian Variations'', ''Catch the Tiger'', ''Koan'', and ''Oli's Dream''.

===''Linked Verse''===
Written for cellist [[Maya Beiser]], [[Shō (instrument)|shō]] player Ko Ishikawa, 8 channel sound and stereoscopic projection, ''Linked Verse'' was produced in collaboration with [[The OpenEnded Group]], and explores the connections and differences between Japanese and American cultures.<ref>{{cite web|title=Cellist Maya Beiser: Creating Music, the Mantra to Move People|url=https://www.sfcv.org/events-calendar/artist-spotlight/cellist-maya-beiser-creating-music-the-mantra-to-move-people/|publisher=San Francisco Classical Voice}}</ref>

==References==
{{Reflist}}

==External links==
{{Wikiquote}}
* {{Official website|http://jaroslawkapuscinski.com}}

{{DEFAULTSORT:Kapuscinski, Jarek}}
[[Category:1964 births]]
[[Category:Polish composers]]
[[Category:Living people]]
[[Category:Musicians from Warsaw]]
[[Category:Canadian composers]]
[[Category:Musicians from California]]
[[Category:Polish emigrants to Canada]]