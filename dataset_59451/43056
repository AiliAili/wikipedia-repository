<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Ki-88
  |image = Kawasaki Ki-88.jpg
  |caption = The mock-up of the Kawasaki Ki-88 in 1943.
}}{{Infobox Aircraft Type
  |type = [[Fighter aircraft]]
  |manufacturer = [[Kawasaki Aerospace Company|Kawasaki Kōkūki Kōgyō K.K.]]
  |designer = 
  |first flight = 
  |introduced = 
  |retired = 
  |status = 
  |primary user = [[Imperial Japanese Army Air Force]] (intended)
  |more users = 
  |produced =
  |number built = None
  |unit cost =
  |variants with their own articles = 
  |developed into = 
}}
|}

The '''Kawasaki Ki-88''' was a proposed [[Japan]]ese [[World War II]] [[fighter aircraft]] intended for use by the [[Imperial Japanese Army Air Force]]. Its anticipated performance was disappointing, and only a mock-up was completed.<ref name="Franc">{{cite book|last=Francillon|first=Rene J.|title=Japanese Aircraft of the Pacific War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1979|pages=483|isbn= 0-87021-313-X}}</ref>

==Design and development==

Faced with delays in development of the [[Kawasaki Ki-64]] [[Fighter (aircraft)|fighter]], the [[Imperial Japanese Army]] was open in 1942 to alternative fighter designs that might reach combat units more quickly.<ref>[http://www.en.valka.cz  en.valka.cz Kawasaki Ki-88]</ref> Kawasaki proposed the Ki-88, a design inspired by the [[Bell P-39 Airacobra]] fighter then in service with the [[United States Army Air Forces]].<ref name="Franc"/>

Kawasaki began design work on the Ki-88 in August 1942. The Ki-88 was to have a 1,117-kW (1,500-hp) [[Kawasaki Ha-140]] engine behind its [[cockpit]], driving a [[Tractor configuration|tractor]] [[propeller]] through an extension shaft. It was to mount a 37-mm cannon in its propeller shaft and two 20-mm cannon in its lower nose.<ref name="Franc"/>

When design work had progressed far enough to allow it, Kawasaki built a full-scale mock-up of the Ki-88, which bore a strong resemblance to the P-39. After inspection of the mockup, the Japanese calculated a maximum speed for the aircraft of 600&nbsp;km/hr (373&nbsp;mph) at an altitude of 6,000 m (19,685 feet). This was only slightly faster than the [[Kawasaki Ki-61|Kawasaki Ki-61 ''Hien'']] fighter, which already was in production. As a result, Kawasaki discontinued design work on the Ki-88 less than a year after beginning it.<ref name="Franc"/>
<!-- ==Operational history==
==Variants==
==Operators== -->

==Specifications==
{{Aircraft specs
|ref=Japanese Aircraft of the Pacific War<ref name="Franc"/>
|prime units?=met
<!--
        General characteristics
-->
|crew=1
|length m=10.2
|span m=12.4
|height m=4.15
|gross weight kg=3,900
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Kawasaki Ha-140]]
|eng1 type=18-cyl. liquid-cooled piston engine
|eng1 kw=1,120
<!--
        Performance
-->
|perfhide=

|max speed kmh=600
|max speed note=at 6,000 m (19,685 ft)
|range km= 1,200
<!--
        Armament
-->
|armament=
* 1× 37 mm cannon
* 2× 20 mm cannon
}}

==See also==
{{aircontent
<!-- |related= -->
|similar aircraft=
* [[Bell P-39 Airacobra]]
* [[Bell P-63 Kingcobra]]
|lists=
* [[List of military aircraft of Japan]]
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Francillon|first=Rene J.|title=Japanese Aircraft of the Pacific War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1979|pages=483|isbn= 0-87021-313-X}}
{{refend}}

==External links==
{{commons category|Kawasaki aircraft}}
* [http://en.valka.cz/viewtopic.php/title/Kawasaki-Ki-88/t/31861 Kawasaki Ki-88 on valka.cz]

{{Kawasaki aircraft}}
{{Japanese Army Aircraft Designation System}}
{{Allied reporting names}}

[[Category:Japanese fighter aircraft 1940–1949|Ki-088, Kawasaki]]
[[Category:Kawasaki aircraft|Ki-088]]
[[Category:World War II Japanese fighter aircraft|Ki-088, Kawasaki]]
[[Category:Abandoned military aircraft projects of Japan|Ki-088, Kawasaki]]