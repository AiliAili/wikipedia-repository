{{Infobox journal
| title = Journal of European Integration
| cover = 
| discipline = [[Political science]], [[international relations]]
| abbreviation = J. Eur. Integrat.
| editor = Thomas Christiansen<br />Simon Duke
| publisher = [[Routledge]]
| country =
| frequency = 7 per year
| history = 1977–present
| impact = 1.132
| impact-year = 2015
| website = http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=geui20
| link1 = http://www.tandfonline.com/toc/geui20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/geui20
| link2-name = Online archive
| ISSN = 0703-6337
| eISSN = 1477-2280
| OCLC = 4093765
| LCCN = 
}}
The '''''Journal of European Integration (Revue d'Intégration Européenne)''''' is a [[peer-reviewed]] [[academic journal]] published seven times a year with a focus on European integration via an [[Multidisciplinary approach|inter-disciplinary persepective]] particularly in regards to European political economy, law, history, and sociology.<ref name=About>{{cite web |title=Journal of European Integration: Aims & scope |url= http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=geui20 |publisher=[[Taylor and Francis]] |accessdate=26 February 2016 }}</ref>

== History ==
The journal was initially published three times a year by the Centre d'Etudes et de Documentation Européennes ([[Université de Montréal]], Canada).<ref>{{cite journal |title=Editorial board |journal=Journal of European Integration |volume=1 |issue=1 |pages=ebi–ebii |publisher=[[Taylor and Francis]] |doi= 10.1080/07036337708428681 |date=September 1977 |ref=harv |postscript=.}}</ref> From January 1981 the journal fell under the auspices of the Canadian Council for European Affairs (Conseil Canadien des Affaires Européennes).<ref>{{cite journal |title=Editorial board |journal=Journal of European Integration |volume=4 |issue=2 |pages=ebi–ebii |publisher=[[Taylor and Francis]] |doi= 10.1080/07036338108428787 |date=January 1981 |ref=harv |postscript=.}}</ref>

Since January 1998 it has been published by [[Routledge]]<ref>{{cite journal |title=Editorial board |journal=Journal of European Integration |volume=21 |issue=1 |pages=ebi–ebii |publisher=[[Taylor and Francis]] |doi= 10.1080/07036339808429058 |date=January 1998 |ref=harv |postscript=.}}</ref> and the current [[editors-in-chief]] are Thomas Christiansen ([[Maastricht University]]) and Simon Duke (European Institute of Public Administration).<ref name=Editor>{{cite web |title=Journal of European Integration: editorial board |url= http://www.tandfonline.com/action/journalInformation?show=editorialBoard&journalCode=geui20 |publisher= [[Taylor and Francis]] |accessdate=26 February 2016 }}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[CSA (database company)|CSA Linguisitics and Language Behavior Abstracts]]
* [[CSA (database company)|CSA Social Services Abstracts]]
* [[CSA (database company)|CSA Sociological Abstracts]]
* [[CSA Worldwide Political Science Abstracts]]
* [[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-02-27}}</ref>
* [[PAIS International]]
* [[Social Sciences Citation Index]]<ref name=ISI/>
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.132, ranking it 58th out of 163 journals in the category "Political Science"<ref name=WoS1>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref> and 28th out of 86 journals in the category "International Relations".<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: International Relations |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]
* [[List of international relations journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=geui20}}

{{DEFAULTSORT:Journal of European Integration}}
[[Category:English-language journals]]
[[Category:Political science journals]]
[[Category:Publications established in 1977]]
[[Category:Routledge academic journals]]


{{Poli-journal-stub}}