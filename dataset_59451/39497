{{Other uses}}
{{Infobox short story| <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name = The Masque of the Red Death
| title_orig = The Mask of the Red Death: A Fantasy
| translator =
| image = [[File:The dagger dropped gleaming upon the sable carpet - Harry Clarke (BL 12703.i.43).tif|200px]]
| caption = Illustration for "The Masque of the Red Death" by [[Harry Clarke]], 1919
| author = [[Edgar Allan Poe]]
| country = United States
| language = English
| series =
| genre = [[Gothic fiction]], [[Horror fiction|Horror]] [[short story]]
| published_in =
| publisher = ''[[Graham's Magazine]]''
| media_type =
| pub_date = May 1842
| english_pub_date =
| preceded_by =
| followed_by =
}}

"'''The Masque of the Red Death'''", originally published as "'''The Mask of the Red Death: A Fantasy'''" (1842), is a [[short story]] by [[Edgar Allan Poe]].  The story follows Prince Prospero's attempts to avoid a dangerous [[pandemic|plague]], known as the Red Death, by hiding in his [[abbey]]. He, along with many other wealthy [[nobility|nobles]], hosts a [[masquerade ball]] within seven rooms of the abbey, each decorated with a different color. In the midst of their revelry, a mysterious figure disguised as a Red Death victim enters and makes his way through each of the rooms. Prospero dies after confronting this stranger, whose "costume" proves to contain nothing tangible inside it; the guests also die in turn.

The story follows many traditions of [[Gothic fiction]] and is often analyzed as an [[allegory]] about the inevitability of death, though some critics advise against an allegorical reading. Many different interpretations have been presented, as well as attempts to identify the true nature of the titular disease. The story was first published in May 1842 in ''[[Graham's Magazine]]'' and has since been adapted in many different forms, including a [[The Masque of the Red Death (1964 film)|1964 film]] starring [[Vincent Price]]. Additionally, it has been alluded to by other works in many types of media.

==Plot summary==
[[File:27 rackham poe masquereddeath.jpg|thumb|right|Illustration of Prince Prospero confronting the "Red Death" by [[Arthur Rackham]], 1935]]
The story takes place at the [[wikt:castellated|castellated]] abbey of the "happy and dauntless and sagacious" Prince Prospero. Prospero and 1,000 other [[nobility|nobles]] have taken refuge in this walled abbey to escape the Red Death, a terrible [[Plague (disease)|plague]] with gruesome symptoms that has swept over the land. Victims are overcome by "sharp pains", "sudden dizziness", and [[hematidrosis]], and die within half an hour. Prospero and his court are indifferent to the sufferings of the population at large; they intend to await the end of the plague in luxury and safety behind the walls of their secure refuge, having welded the doors shut.

One night, Prospero holds a [[masquerade ball]] to entertain his guests in seven colored rooms of the abbey. Each of the first six rooms is decorated and illuminated in a specific color: blue, purple, green, orange, white, and violet. The last room is decorated in black and is illuminated by a scarlet light, "a deep blood color" cast from its stained glass windows. Because of this chilling pairing of colors, very few guests are brave enough to venture into the seventh room. A large ebony clock stands in this room and ominously chimes each hour, upon which everyone stops talking or dancing and the orchestra stops playing. Once the chiming stops, everyone immediately resumes the masquerade.

At the chiming of midnight, the revelers and Prospero notice a figure in a dark, blood-splattered robe resembling a [[Shroud|funeral shroud]]. The figure's mask resembles the rigid face of a corpse and exhibits the traits of the Red Death. Gravely insulted, Prospero demands to know the identity of the mysterious guest so they can [[Hanging|hang]] him. The guests, too afraid to approach the figure, instead let him pass through the six chambers. The Prince pursues him with a drawn [[dagger]] and corners the guest in the seventh room. When the figure turns to face him, the Prince lets out a sharp cry and falls dead. The enraged and terrified revelers surge into the black room and forcibly remove the mask and robe, only to find to their horror that there is nothing underneath. Only then do they realize the figure is the Red Death itself, and all of the guests contract and succumb to the disease. The final line of the story sums up, "And Darkness and Decay and the Red Death held [[Wikt:illimitable|illimitable]] dominion over all".

==Analysis==
[[File:Aubrey Beardsley - Edgar Poe 4.jpg|thumb|Illustration by [[Aubrey Beardsley]], 1894–1895.]]
In "The Masque of the Red Death", Poe adopts many conventions of traditional [[Gothic fiction]], including the castle setting. The multiple single-toned rooms may be representative of the human mind, showing different personality types. The imagery of blood and time throughout also indicates corporeality. The plague may, in fact, represent typical attributes of human life and mortality,<ref>{{cite news|authors=Fisher, Benjamin Franklin & Hayes, Kevin J.|title=Poe and the Gothic tradition|work=The Cambridge Companion to Edgar Allan Poe|location=New York City|publisher= Cambridge University Press|date=2002|isbn=0-521-79727-6 |page= 88}}</ref> which would imply the entire story is an [[allegory]] about man's futile attempts to stave off death (a commonly accepted interpretation).<ref>{{cite news|authors=Roppolo, Joseph Patrick & Regan, Robert (Editor)|title=Meaning and 'The Masque of the Red Death'|work=Poe: A Collection of Critical Essays|location= Englewood Cliffs, NJ|publisher= Prentice-Hall, Inc.|date= 1967|page= 137}}</ref> However, there is much dispute over how to interpret "The Masque of the Red Death"; some suggest it is not allegorical, especially due to Poe's admission of a distaste for [[didacticism]] in literature.<ref>{{cite news|authors=Roppolo, Joseph Patrick & Regan, Robert (Editor)|title=Meaning and 'The Masque of the Red Death'|work=Poe: A Collection of Critical Essays|location= Englewood Cliffs, NJ|publisher= Prentice-Hall, Inc.|date= 1967|page= 134}}</ref> If the story really does have a moral, Poe does not explicitly state that moral in the text.<ref>{{cite book|author=Quinn, Arthur Hobson|title=Edgar Allan Poe: A Critical Biography|location=Baltimore|publisher= The Johns Hopkins University Press|date= 1998|isbn= 0-8018-5730-9|page= 331}}</ref>

Blood, emphasized throughout the tale, along with the color red, serves as a dual symbol, representing both death and life. This is emphasized by the masked figure&nbsp;– never explicitly stated to be the Red Death, but only a reveler in a costume of the Red Death&nbsp;– making his initial appearance in the easternmost room, which is colored blue, a color most often associated with birth.<ref>{{cite news|authors=Roppolo, Joseph Patrick & Regan, Robert (Editor)|title=Meaning and 'The Masque of the Red Death'|work=Poe: A Collection of Critical Essays|location= Englewood Cliffs, NJ|publisher= Prentice-Hall, Inc.|date= 1967|page= 141}}</ref>

Though Prospero's castle is meant to keep the sickness out, it is ultimately an oppressive structure. Its maze-like design and tall and narrow windows become almost [[Burlesque (literature)|burlesque]] in the final black room, so oppressive that "there were few of the company bold enough to set foot within its precincts at all".<ref>{{cite news|author=Laurent, Sabrina|title=Metaphor and Symbolism in 'The Masque of the Red Death' |work=Boheme: An Online Magazine of the Arts, Literature, and Subversion|date= July 2003|url=http://www.boheme-magazine.net/php/modules.php?name=News&file=article&sid=46 }}</ref> Additionally, the castle is meant to be an enclosed space, yet the stranger is able to sneak inside, suggesting that control is an illusion.<ref>{{cite news|author=Peeples, Scott|title=Poe's 'constructiveness' and 'The Fall of the House of Usher'|work=The Cambridge Companion to Edgar Allan Poe|publisher= Cambridge University Press|date= 2002|isbn= 0-521-79727-6 |page= 186}}</ref>

Like many of Poe's tales, "The Masque of the Red Death" has been interpreted autobiographically, by some. In this point of view, Prince Prospero is Poe as a wealthy young man, part of a distinguished family much like Poe's [[foster parent]]s, the Allans. Under this interpretation, Poe is seeking refuge from the dangers of the outside world, and his portrayal of himself as the only person willing to confront the stranger is emblematic of Poe's rush towards inescapable dangers in his own life.<ref>{{cite book|author=Rein, David M. |title=Edgar A. Poe: The Inner Pattern|location= New York|publisher= Philosophical Library|date= 1960|page= 33}}</ref> Prince Prospero is also the name of a character in [[William Shakespeare]]'s ''[[The Tempest]]''.<ref>{{cite book|last1=Barger|first1=Andrew|title=Phantasmal: The Best Ghost Stories 1800-1849|date=2011|publisher=Bottletree Books LLC|location=USA|isbn=978-1-933747-33-0|page=138}}</ref>

===The "Red Death"===
The disease called the Red Death is fictitious. Poe describes it as causing "sharp pains, and sudden dizziness, and then profuse bleeding at the pores" leading to death within half an hour.

The disease may have been inspired by [[tuberculosis]] (or consumption, as it was known then), since Poe's wife [[Virginia Eliza Clemm Poe|Virginia]] was suffering from the disease at the time the story was written. Like the character Prince Prospero, Poe tried to ignore the fatality of the disease.<ref>{{cite book|author=Silverman, Kenneth|title=Edgar A. Poe: Mournful and Never-ending Remembrance|publisher= Harper Perennial|date= 1991|isbn= 0-06-092331-8|pages= 180–1}}</ref> Poe's mother [[Eliza Poe|Eliza]], brother [[William Henry Leonard Poe|William]], and foster mother Frances had also died of tuberculosis. Alternatively, the Red Death may refer to [[cholera]]; Poe would have witnessed an [[epidemic]] of cholera in [[Baltimore, Maryland]] in 1831.<ref>{{cite book|author=Meyers, Jeffrey|title=Edgar Allan Poe: His Life and Legacy|publisher= Cooper Square Press|date= 1992|isbn= 0-8154-1038-7 |page= 133}}</ref> Others have suggested the pandemic is actually [[Bubonic plague]] or the [[Black death]], emphasized by the climax of the story featuring the "Red" Death in the "black" room.<ref>{{cite web|url=http://www.cummingsstudyguides.net/Guides2/Masque.html|website= Cummings Study Guides|title=The Masque of the Red Death}}</ref> One writer likens the description to that of a [[viral hemorrhagic fever]] or [[necrotizing fasciitis]].<ref>{{cite book|title=Molecules of Death|edition=2nd |authors=Waring, R.H. & Steventon, G.B. & Mitchell, S.C.|location= London|publisher= Imperial College Press|date= 2007}}</ref> It has also been suggested that the Red Death is not a disease or sickness at all but a weakness (like "[[original sin]]") that is shared by all of humankind inherently.<ref>{{cite news|authors=Roppolo, Joseph Patrick & Regan, Robert (Editor)|title=Meaning and 'The Masque of the Red Death'|work=Poe: A Collection of Critical Essays|location= Englewood Cliffs, NJ|publisher= Prentice-Hall, Inc.|date= 1967|pages= 139–140}}</ref>

==Publication history==
Poe first published the story in the May 1842 edition of ''[[Graham's Magazine|Graham's Lady's and Gentleman's Magazine]]'' as "The Mask of the Red Death", with the tagline "A Fantasy". This first publication earned him $12.<ref>Ostram, John Ward. "Poe's Literary Labors and Rewards" in ''Myths and Reality: The Mysterious Mr. Poe''. Baltimore: The Edgar Allan Poe Society, 1987. p. 39</ref>  A revised version was published in the July 19, 1845 edition of the ''[[Broadway Journal]]'' under the now-standard title "The Masque of the Red Death."<ref>[http://www.eapoe.org/works/info/pt038.htm Edgar Allan Poe — "The Masque of the Red Death"] at the Edgar Allan Poe Society online</ref> The original title emphasized the figure at the end of the story; the new title puts emphasis on the masquerade ball.<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001. p. 149. ISBN 0-8160-4161-X</ref>

==Adaptations==

===Audio adaptations===
* [[Basil Rathbone]] read the entire short story in his Caedmon LP recording ''The Tales of Edgar Allan Poe'' (early 1960s). Other audiobook recordings have featured [[Christopher Lee]], [[Hurd Hatfield]], [[Martin Donegan]] and [[Gabriel Byrne]] as readers.
* The story was adapted by [[George Lowther (writer)|George Lowther]] for a broadcast on the ''[[CBS Radio Mystery Theater]]'' (January 10, 1975), starring [[Karl Swenson]] and [[Staats Cotsworth]].
* A radio reading was performed by [[Winifred Phillips]], with music she composed. The program was produced by [[Winnie Waldron]] as part of [[National Public Radio]]'s ''[[Radio Tales|Tales by American Masters]]'' series.

===Comics adaptations===
* In 1952, Marvel published "The Face of Death" in Adventures Into Weird Worlds #4. Adaptation & art by Bill Everett.
* In 1952, Charlton published "The Red Death" in The Thing #2. Adaptation & art by Bob Forgione.
* In 1960, Editora Continental (Brazil) published "A Mascara Da Morte Rubra" in Classicos De Terror #9. Adaptation & art by Manoel Ferreira. It was reprinted by Editora Taika in Album Classicos De Terror #11 (1974) and by Editora Vecchia in Spektro #6 (1978).
* In 1961, Marvel published "Masqeurade Party" in Strange Tales #83. Story & art by Steve Ditko. It was reprinted by Editora Taika (Brazil) in Almanaque Fantastic Aventuras #1 (1973) and by Marvel in Chamber Of Chills #16 (1975).
* In 1964, Dell published "The Masque Of The Red Death". Adapted from the 1964 film, art by Frank Springer.
* In 1967, Warren published "The Masque Of The Red Death" in Eerie #12. Adaptation by Archie Goodwin, art by Tom Sutton. This version has been reprinted multiple times.
* In 1967, Editora Taika published "A Mascara Da Morte Rubra" in Album Classicos De Terror #3. Adaptation by Francisco De Assis, art by Nico Rosso with J.B. Rosa. This was reprinted in Almanaque Classicos De Terror #15 (1976).
* In 1969, Marvel published "The Day Of The Red Death" in Chamber Of Darkness #2. Adaptation by Roy Thomas, art by Don Heck. This was reprinted by La Prensa (Mexico) in El Enterrador #4 (1970) and by Marvel in Chamber Of Darkness Special #1 (1972).
* In 1972, Milano Libri Edizioni (Italy) published "La Maschera Della Morte Rossa" in Linus #91. Adaptation & art by Dino Battaglia. This was reprinted in Corto Maltese #7 (1988) and multiple other times.
* In 1974, Skywald published "The Masque Of The Red Death" in Psycho #20. Adaptation by Al Hewetson, art by Ricardo Villamonte. This was reprinted by Garbo (Spain) in Vampus #50 (1975) and by Eternity in The Masque Of The Red Death And Other Stories #1 (1988).
* In 1975, Warren published "Shadow" in Creepy #70. Adaptation by Richard Margopoulos, art by Richard Corben. The ending was changed to incorporate elements of "The Masque Of The Red Death". This was reprinted multiple times.
* In 1975, Charlton published "The Plague" in Haunted #22. Adaptation by Britton Bloom, art by Wayne Howard. This was reprinted in Haunted #45 (1979) and by Rio Grafica Editora Globo (Portugal) in Fetiche #1 (1979).
* In 1975, Ediciones Ursus (Spain) published "La Mascara Da la Muerte Roja" in Macabro #17. Art by Francisco Agras.
* In 1979, Bloch Editores S.A. (Brazil) published "A Mascara Da Morte Rubra" in Aventuras Macabras #12. Adaptation by Delmir E. Narutoxdc, art by Flavio Colin.
* In 1982, Troll Associates published "The Masque Of The Red Death" as a children's book. Adaptation by David E. Cutts, art by John Lawn.
* In 1982, Warren published "The Masque of The Red Death" in Vampirella #110. Adaptation by Rich Margopoulos, art by Auraleon. This has been reprinted multiple times.
* In 1984, Editora Valenciana (Spain) published "La Mascara De La Muerte Roja" in SOS #1. Adaptation & art by A.L. Pareja.
* In 1985, Edizione Editiemme (Italy) published "La Masque De La Morte Rouge" in Quatro Incubi #1. Adaptation & art by Alberto Brecchi. This has been reprinted multiple times.
* In 1987, Kitchen Sink Press published "The Masque of The Red Death" in ''[[Death Rattle (comics)|Death Rattle]]'' v.2 #13. Adaptation & art by Daryl Hutchinson.
* In 1988, Last Gasp published "The Masque of The Red Death" in ''[[Strip Aids U.S.A.]]'' Adaptation & art by [[Steve Leialoha]].
* In 1995, Mojo Press published "The Masque of The Red Death" in Weird Business. Adaptation by Erick Burnham, art by [[Ted Naifeh]].
* In 1999, Albin Michel – L’Echo des Savanes (France) published "De La Mascara De La Muerte Roja" in Le Chat Noir. Adaptation & art by Horacio Lalia. This has been reprinted multiple times.
* In 2004, Eureka Productions published "The Masque of The Red Death" in Graphic Classics #1: Edgar Allan Poe (2nd edition). Adaptation by David Pomplun, art by Stanley W. Shaw. This has been reprinted in the 3rd edition (2006), and in Graphic Classics #21: Edgar Allan Poe's Tales Of Mystery (2011).
* In 2008, Go! Media Entertainment published ''[[Wendy Pini's Masque of the Red Death]]''. Adaptation by Richard Pini, art by Wendy Pini. This version is an erotic, illustrated webcomic, set in the future.
* In 2008, Sterling Press published "The Masque of The Red Death" in Nevermore (Illustrated Classics): A Graphic Adaptation Of Edgar Allan Poe's Short Stories. Adaptation by Adam Prosser, art by Erik Rangel.
* In 2013, Dark Horse published "The Masque of The Red Death" in The Raven And The Red Death. Adaptation & art by Richard Corben. This has been reprinted in Spirits Of The Dead (2014).
*''[[Wendy Pini's Masque of the Red Death]]'', is an erotic, illustrated webcomic version set in the future.
* In spring 2017, [[UDON Entertainment]]'s Manga Classics line will publish ''The Stories of Edgar Allan Poe'', which will include a manga format adaptation of "The Masque of the Red Death".<ref>http://www.animenewsnetwork.com/news/2016-07-21/udon-ent-to-release-street-fighter-novel-dragon-crown-manga/.104545 "Udon Ent. to Release Street Fighter Novel, Dragon's Crown Manga", ''Anime News Network'', July 21, 2016</ref>

===Film adaptations===

* The story was adapted by [[Roger Corman]] as a film, ''[[The Masque of the Red Death (1964 film)|The Masque of the Red Death]]'' (1964), starring [[Vincent Price]].
* Corman produced, but did not direct a [[Masque of the Red Death (1989 film)|remake]] of the film in 1989, starring [[Adrian Paul]] as Prince Prospero.<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001. p. 150. ISBN 0-8160-4161-X</ref>
* Corman also voiced Prince Prospero in "The Masque of the Red Death" segment of Raúl García's animated anthology ''Extraordinary Tales'' (2015).

===Stage adaptations===
* The story has been adapted by [[Punchdrunk]] Productions in collaboration with [[Battersea Arts Centre]], as a [[promenade theatre]] performance ([[The Masque Of The Red Death (2007 play)|''The Masque of the Red Death'' (play)]]) from September 17, 2007 to April 12, 2008.<ref>[http://www.nationaltheatre.org.uk/?lid=23686 National Theatre online]</ref>

===Events===
* The story has been turned into an annual Halloween event called The Masquerade Of The Red Death, held in the area of [[Detroit, Michigan]]. The annual event began in 2013, and is said to pit the guests in the center of Poe's story.<ref>[http://www.TheMasqueradeOfTheRedDeath.com MasqueradeOfTheRedDeath.com]</ref><ref>[http://www.freep.com/apps/pbcs.dll/gallery?Avis=C4&Dato=20131028&Kategori=ENT1005&Lopenr=310280100&Ref=PH The Detroit Free Press]</ref>

==In popular culture==
* [[The Masque of the Red Death in popular culture]]

==See also==
{{portal|Novels}}
* [[Bal des Ardents]]
*[[Ghost story]]

==References==
{{Reflist|2}}

==External links==
{{Wikisource}}
*[http://books.eserver.org/fiction/poe/masque_of_the_red_death.html "The Masque of the Red Death"] at EServer.org
*[http://poestories.com/read/masque "The Masque of the Red Death" with annotated vocabulary] at PoeStories.com
* {{librivox book | title=The Masque of the Red Death | author=Edgar Allan Poe}}

{{Edgar Allan Poe}}
{{The Masque of the Red Death}}

{{DEFAULTSORT:Masque of the Red Death}}
[[Category:1842 short stories]]
[[Category:Infectious diseases in fiction]]
[[Category:Personifications of death in fiction]]
[[Category:Short stories adapted into films]]
[[Category:Short stories by Edgar Allan Poe]]
[[Category:Works originally published in Graham's Magazine]]