{{Unreferenced|date=November 2008}}
{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name              = The Lagoon
| image             = 
| title_orig        =
| translator        =
| author            = [[Joseph Conrad]]
| country           = England
| language          = English
| series            =
| genre             = [[Short story]]
| published_in      = ''The Cornhill Magazine''
| publication_type  = [[Magazine]]
| publisher         =
| media_type        =
| pub_date          = January, 1897
| english_pub_date  =
| preceded_by       =
| followed_by       =
}}

'''"The Lagoon"''' is a [[short story]] by [[Joseph Conrad]] composed in 1896 and first published in ''[[Cornhill Magazine]]'' in 1897. The story is about a white man, referred to as "Tuan" (the equivalent of "Lord" or "Sir"), who is travelling through an Indonesian [[rainforest]] and is forced to stop for the night with a distant Malay friend named Arsat.  Upon arriving, he finds Arsat distraught, for his lover is dying. Arsat tells the distant and rather silent white man a story of his past.

==Plot summary==
The story that Arsat tells Tuan is about sadness and betrayal.  Arsat tells of the time when he and his brother kidnapped Diamelen (his lover, who was previously a servant of the [[Rajah]]'s wife).  They all fled in a boat at night and travelled until they were exhausted.  They stopped on a bit of land jutting out into the water to rest.  Soon however, they spotted a large boat of the Rajah's men coming to find them.  Arsat's brother told Diamelen and Arsat to flee to the other side, where there was a fisherman's hut.  He instructed them to take the fisherman's boat and then stayed back, telling them to wait for him while he dealt with the pursuers.  However, Arsat did everything but wait for his brother.  As he pushed the boat from shore, he saw his brother running down the path, being chased by the pursuers.  Arsat's brother tripped and the enemy was upon him.  His brother called out to him three times, but Arsat never looked back; he had betrayed his brother for the woman he loved. Towards the end of the story, symbolically, the sun rises and Diamelen dies.  Arsat has nothing now; not a brother nor a wife.  He has lost everything. He plans to return to his home village to avenge his brother's death.  The story concludes with "Tuan"{{'}}s simply leaving, and Arsat's staring dejectedly into the sun and "a world of illusion".

The story is full of symbols and contrasts - such as the use of dark/light, black/white, sunrise/sunset, water/fire, and possibly the most important one, movement/stillness. Arsat's clearing is still, nothing moves, yet everything outside the clearing moves.  Earlier in the story, his brother tells Arsat that he is only half of a man for Diamelen has his heart and he is not whole. With Diamelen's death, Arsat becomes a whole man again. At the end of the story, motion finally enters Arsat's clearing. The movement  signifies his leaving of "a world of illusion" and the fact that Arsat is finally a "free man". In the story, darkness represents ignorance and denial, whereas light represents enlightenment.

==External links==
{{Wikisource|The Lagoon}}
*[http://www.conradfirst.net/view/serialisation?id=88 "The Lagoon"] as originally published in [[Cornhill Magazine]]
*[http://www.eastoftheweb.com/short-stories/UBooks/Lago.shtml "The Lagoon"] by Conrad

{{Conrad}}

{{DEFAULTSORT:Lagoon, The}}
[[Category:1897 short stories]]
[[Category:Short stories by Joseph Conrad]]
[[Category:Works originally published in Cornhill Magazine]]