{{EngvarB|date=November 2015}}
{{Use dmy dates|date=November 2015}}
{{Infobox book| <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Macdermots of Ballycloran
| title_orig    =
| translator    =
| image         = The Macdermots.jpg
| caption = First edition title page
| author        = [[Anthony Trollope]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = Novel
| publisher     = [[Thomas Cautley Newby]]
| release_date  = 1847
| english_release_date =
| media_type    = Print (hardback)
| pages         =
| isbn          = 978-0-548-28018-8
| oclc= 178460375
| preceded_by   =
| followed_by   =
}}
'''''The Macdermots of Ballycloran''''' is a novel by [[Anthony Trollope]].  It was Trollope's first published novel, which he began in September 1843 and completed by June 1845.  However, it was not published until 1847.  The novel was "an abysmal failure with the reading public."<ref name=Sutherland>Sutherland, John (1989). ''The Stanford Companion to Victorian Fiction'', p. 393. Stanford University Press. ISBN 0-8047-1842-3.</ref>

The novel was written while Trollope was staying in the village of [[Drumsna]], [[County Leitrim]], Ireland.<ref>{{Cite web| title=Welcome to Drumsna | work=GoIreland | url=http://www.goireland.com/leitrim/drumsna.htm | accessdate=25 June 2008}}</ref>

==Plot summary==
The narrative of ''The Macdermots of Ballycloran'' "chronicles the tragic demise of a small Catholic landowning family in the Protestant-dominated Ireland of the mid nineteenth century. It focuses on the struggle of Thady Macdermot to keep his sinking property afloat. Thady lives with his father Larry Macdermot in a dilapidated mansion in Co. Leitrim, which is mortgaged to their enemy, the vulgar builder Joe Flannelly. They cannot keep up the payments on the mortgage. Enmity between the Macdermot and Flannelly families is sharpened by Thady's having declined to marry Joe's daughter, Sally.  Larry Macdermot's daughter, Feemy, is seduced by the English police officer, Captain Myles Ussher, who is hated by the local Catholic majority for his brutal enforcement of the excise laws against [[poteen]] distilling. One night Thady comes home to find Ussher abducting Feemy and kills him in the ensuing struggle.  Despite the mitigating circumstances, the Protestant-dominated courts find Thady guilty of murder, in the context of a panic about crime, and possibly anti-British terrorism.  Thady is hanged, his father Larry goes mad, Feemy dies bearing Ussher's bastard and the Ballycloran house is finally vacated of Macdermots."<ref name=Sutherland/>

==Quote==
Trollope, in his autobiography, said the following concerning ''The Macdermots of Ballycloran'':
<blockquote>"As to the plot itself, I do not know that I ever made one so good,- or, at any rate, one so susceptible of pathos. I am aware that I broke down in the telling, not having yet studied the art. Nevertheless, ''The Macdermots'' is a good novel, and worth reading by anyone who wishes to understand what Irish life was before the potato disease, the [[Great Famine (Ireland)|famine]], and the Encumbered Estates Bill."<ref>Anthony Trollope, An Autobiography, pg 71, Oxford World's Classics, 1999.</ref></blockquote>

==References==
{{Reflist}}

==External links==
* [https://trollopesociety.org/book/macdermots-ballycloran/ Plot summary and notes]
* {{gutenberg |no=29000 |title=The Macdermots of Ballycloran}}
* {{librivox book | title=The Macdermots of Ballycloran | author=Anthony TROLLOPE}}

{{Anthony Trollope}}

{{DEFAULTSORT:Macdermots Of Ballycloran}}
[[Category:1847 novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Novels set in Ireland]]
[[Category:Debut novels]]
[[Category:County Leitrim]]


{{1840s-novel-stub}}