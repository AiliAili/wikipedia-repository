{{Infobox journal
| title = Journal of Near-Death Studies
| cover =
| editor = Janice Holden
| discipline = [[Near-death studies]]
| formernames = Anabiosis
| abbreviation = J. Near Death Stud.
| publisher = [[International Association for Near-Death Studies]]
| country =
| history = 1982-present
| frequency = Quarterly
| website = http://www.iands.org/publications/journal-of-near-death-studies.html
| ISSN = 0891-4494
| eISSN = 1573-3661
| OCLC = 45254332
| LCCN = 88648131
| CODEN = JNDAE7
}}
The '''''Journal of Near-Death Studies''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] devoted to the field of [[near-death studies]]. It is published by the [[International Association for Near-Death Studies]].<ref name="Anderson 1999">Anderson, Jon. "Almost Blinded By The Light Near-death Experiences Share Common Thread". ''The Chicago Tribune'', September 29, 1999</ref><ref name="Anderson 2004">Anderson, Jon. "Shedding light on life at death's door". ''The Chicago Tribune'', published online May 13, 2004 </ref><ref name="Graves 2007">Graves, Lee. "Altered States. Scientists analyze the near-death experience". ''The University of Virginia Magazine'', Summer 2007 Feature</ref><ref name="Williams 2007">Williams, Daniel. "At the Hour Of Our Death". ''TIME Magazine''. Friday, Aug. 31, 2007</ref><ref name="Griffith 2009"> Griffith, Linda J. Near-Death Experiences and Psychotherapy. ''Psychiatry'' (Edgmont). 2009 October; 6(10): 35–42.</ref><ref name="Beck 2010">Beck, Melinda. ''Seeking Proof in Near-Death Claims''. ''The Wall Street Journal'' (Health Journal), October 25, 2010</ref>

The journal's founding [[editor-in-chief]] was [[Kenneth Ring]].<ref>Author biography in Kenneth Ring and Evelyn Elsaesser Valarino, ''Lessons from the Light: What we can learn from the near-death experience'', Needham, MA: Moment Point Press (1998). ISBN 978-0-306-45983-2</ref> Subsequent editors were [[Bruce Greyson]] and Janice Holden.

== History ==
The journal was established in 1982 as ''Anabiosis'' and obtained its current title in 1987 with the start of volume 6.<ref>{{cite web |url=http://www.iands.org/publications/journal-of-near-death-studies.html |title=Journal of Near-Death Studies |publisher=[[International Association for Near-Death Studies]] |work=Homepage |accessdate=2011-02-06}}</ref> From 1997–2003 the journal was published by [[Springer Science+Business Media|Kluwer Academic Publishers]], but this arrangement was discontinued upon completion of Volume 21.<ref>{{cite web |url=http://springerlink.metapress.com/content/105586/ |title=Journal of Near-Death Studies |publisher=[[Springer Science+Business Media]] |work=SpringerLink |accessdate=2011-02-06}}</ref>

== See also ==
* ''[[Journal of Parapsychology]]''
* ''[[Journal of the American Society for Psychical Research]]''
* [[Near-death experience]]
* [[Near-death studies]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.iands.org/publications/journal-of-near-death-studies.html}}

[[Category:Publications established in 1982]]
[[Category:Psychology journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Near-death experiences]]