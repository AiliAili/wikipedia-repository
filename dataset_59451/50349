{{Infobox journal
| title = Acta Linguistica Hungarica
| cover = [[File:Acta Linguistica Hungarica.jpg]]
| editor = Ferenc Kiefer
| discipline = [[Linguistics]]
| language = English, German, French
| abbreviation = Acta Linguistica Hung.
| publisher = [[Akadémiai Kiadó]]
| country = Hungary
| frequency = Quarterly
| history = 1951–present
| openaccess = 
| license =
| impact = 0.062
| impact-year = 2011
| website = http://akkrt.hu/19/journals/products/linguistics/acta_linguistica_hungarica_eng
| link1 = http://www.akademiai.com/content/119702/
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 320493650
| LCCN = 94640314
| CODEN = 
| ISSN = 1216-8076
| eISSN = 1588-2624
}}
'''''Acta Linguistica Hungarica''''' is a quarterly [[Hungary|Hungarian]] [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[linguistics]]. It is published by [[Akadémiai Kiadó]] and the current [[editor-in-chief]] is Ferenc Kiefer ([[Hungarian Academy of Sciences]]).

== Aims and scope ==
The scope of the journal is wider than general linguistics, including such areas as [[Psycholinguistics|psycho-]] and [[neurolinguistics]], discourse analysis, and [[language typology]]. The main emphasis is placed on [[Finno-Ugric languages]], in particular [[Hungarian language|Hungarian]]. Apart from research papers, both theoretical and practical, the journal has a book and dissertation review section.<ref name=aims>{{Cite web |title=Acta Linguistica Hungarica |publisher=Akadémiai Kiadó |date=2010 |url=http://akkrt.hu/19/journals/products/linguistics/acta_linguistica_hungarica_eng |accessdate=2010-12-25}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name="aims" />
* [[Arts and Humanities Citation Index]]
* [[Linguistic Bibliography|Linguistic Bibliography/Bibliographie Linguistique]]
* International Bibliographies IBZ and IBR
* Linguistics and Language Behaviour Abstracts
* [[MLA International Bibliography]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.062.<ref name=WoS>{{cite book |year=2013 |chapter=Acta Linguistica Hungarica |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://akkrt.hu/19/journals/products/linguistics/acta_linguistica_hungarica_eng}}

[[Category:Linguistics journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1951]]
[[Category:Multilingual journals]]