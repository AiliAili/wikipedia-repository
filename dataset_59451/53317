{{Infobox college sports rivalry
| name                 = 
| rivalry_image = 
| rivalry_image_size = 200
| team1                = [[North Carolina Tar Heels football|North Carolina Tar Heels]]
| team1_image          = University of North Carolina Tarheels Interlocking NC logo.svg
| team2                = [[Virginia Cavaliers football|Virginia Cavaliers]]
| team2_image          = U of Virginia text logo.svg
| sport                = [[College football|Football]]
| total_meetings       = 121
| series_record        = North Carolina leads 63-54-4
| first_meeting_date   = October 22, 1892
| first_meeting_result = Virginia 30, North Carolina 18
| last_meeting_date    = October 22, 2016
| last_meeting_result  = North Carolina 35, Virginia 14
| next_meeting_date    = 2017
| largest_win_margin   = Virginia 66, North Carolina 0 (November 26, 1912)
| longest_win_streak   = 
| current_win_streak   = North Carolina, 7 (2010–present)
| trophy               = 
}}

The '''South's Oldest Rivalry''' is an American [[college football]] [[college rivalry|rivalry]] game played annually by the [[Virginia Cavaliers football]] team of the [[University of Virginia]] and the [[North Carolina Tar Heels football]] team of the [[University of North Carolina at Chapel Hill]].  Both universities have been members of the [[Atlantic Coast Conference]] since 1953 but the Cavaliers and Tar Heels played their first two football games in 1892 (Virginia won the first and North Carolina the second), over sixty years before the formation of the ACC.

== Series history ==
Long being the most played game among all [[Football Bowl Subdivision]] series in the [[Southeastern United States]], it has become known over the years simply as the ''South's Oldest Rivalry''. It is also the oldest series in this highest division [[East Coast of the United States|in the east]]. The 2014 meeting marked the 119th edition of this game (played continuously since 1919), five more than the [[Army–Navy Game]] (played continuously since 1930), and one more than the "''Deep'' South's Oldest Rivalry" ([[Deep South's Oldest Rivalry|Georgia–Auburn]], played continuously since 1944).

The game was first twice played in 1892 ([[1892 Virginia Cavaliers football team|Virginia]] won the first, and [[1892 North Carolina Tar Heels football team|North Carolina]] the second, splitting the southern title). Virginia then claims a southern championship for every year of 1893–1897, with North Carolina gaining a [[Southern Intercollegiate Athletic Association]] title in 1895 (only loss to Virginia) and 1898. Both overshadowed by [[1899 Sewanee Tigers football team|Sewanee]] in 1899, Virginia again went on a tear from the turn of the century until 1905 when North Carolina pulled the upset.<ref>{{cite news|work=The Tar Heel|title=Carolina Athletic Record Over 37 Year Period High|date=January 7, 1926|url=https://www.newspapers.com/clip/1924487//|accessdate=March 5, 2015|via=[[Newspapers.com]]}} {{Open access}}</ref> It is the third [[Most played rivalries in NCAA Division I FBS#Most played FBS series|most played rivalry game]] nationwide among college football's [[Automatic Qualifying conference]] schools, and soon to be the second-most played. Due to the [[2010–13 NCAA conference realignment]] the UVA-UNC rivalry will surpass the [[Lone Star Showdown]] between Texas and Texas A&M as the third most played national rivalry on October 25, 2014 in Charlottesville. It will then surpass the now-defunct [[Border War (Kansas–Missouri rivalry)|Border War]] between Kansas and Missouri in the fall of 2016, also in Charlottesville.  Beginning in that year, it will be the second most-played national rivalry behind the [[Paul Bunyan's Axe]] rivalry between the [[Wisconsin Badgers]] and [[Minnesota Golden Gophers]].

Virginia and North Carolina have faced each other 120 times. North Carolina leads the all-time series, 63–54–4.{{ref|a|a}}  In 2010 UNC broke a long losing streak in Charlottesville, UNC's first road win in the series since 1981. It ended what many UNC fans mockingly described as the "Charlottesville Curse."  UVA led the series from 1893 to 1944, and UNC has since led from 1945 onward. Virginia closed to within two games in 2009, but UNC has won seven in a row since. Even after the losing streak, Virginia is 20–13–1 in the rivalry since 1983.

Second-most played is 103 for North Carolina versus the [[Wake Forest Demon Deacons]], and second-most played for Virginia is 95 [[Virginia-Virginia Tech rivalry|against Virginia Tech]] for the [[Commonwealth Cup (Virginia)|Commonwealth Cup]].

== Nature of the Rivalry ==
There is considerable historical lineage and academic standing between the two universities involved. The University of Virginia was founded by third [[President of the United States]] and founding father [[Thomas Jefferson]], whereas the University of North Carolina was the [[University of North Carolina at Chapel Hill#History|first operational state university]] in the United States. [[William Faulkner]] was Writer-in-Residence at UVA, and [[Peter Matthew Hillsman Taylor|Peter Taylor]] was on the UVA faculty and retired in Charlottesville.  UNC is the ''alma mater'' of [[Thomas Wolfe]], [[Walker Percy]], and [[Shelby Foote]]. President [[Woodrow Wilson]] attended the University of Virginia and was President of its [[Jefferson Literary and Debating Society]],  whereas President [[James K. Polk]] attended UNC and was a Senator in its [[Dialectic and Philanthropic Societies]]

When the 1985 Richard Moll book was published listing the original eight "[[Public Ivy|Public Ivies]]," public colleges with rigorous academic standards, there were only two sharing a common athletic conference: the University of Virginia and the University of North Carolina. For at least nine consecutive years, ''[[U.S. News & World Report]]'' has ranked UVA second and UNC fifth among all public universities, and they are first and second in the east.<ref>Ranked above both is the [[University of California, Berkeley]] and UVA is tied with [[UCLA]].  UNC then trails only the [[University of Michigan]] for fourth nationwide.</ref>  The two were also the first future members of the [[Atlantic Coast Conference]] to be elected to the prestigious [[Association of American Universities]]: UVA was elected in 1904 and UNC in 1922.  Only [[Duke University]] would join them, in 1938, before the ACC was formed in 1953.

The rivalry is often called a "Gentlemen's Rivalry."  One reason for this moniker is the prestigious image, both academically and socially, of both universities in their states and throughout the region. The institutions' student bodies also tend to somewhat mirror one another from a social and academic standpoint.  As for today and recent decades, the rivalry itself has been lackluster and less heated despite a few recent historical wins by UNC.  Neither program has finished at the top of the ACC since the 1990s, nor has UVA yet played in the [[ACC Championship Game]].

==Contributing factors==

==="Benedict Ronald"===
Often considered the best high school football player of all time from the state of Virginia,<ref>{{cite web|url=http://www.davesez.com/archives/000372.php |title=The Amazing Ronald Curry |publisher=Dave Sez |date=2004-08-12 |accessdate=2012-07-26}}</ref> and the only junior ever to be named the nation's top high school quarterback by ''[[USA Today]]'', [[Ronald Curry]] announced a verbal commitment to [[George Welsh (American football)|George Welsh]]'s Virginia program on September 4, 1997 during [[ESPN]] coverage of that night's game between Virginia and Auburn.<ref>"Virginia Won Big Before It Took The Field"; Richmond Times – Dispatch – Richmond, Va.; Bob Lipper; Sep 5, 1997; Page D1</ref>  With the commitment from Curry, Welsh declined to recruit [[Michael Vick]], whose own stellar career in the same high school district was largely overshadowed by Curry's.  While Curry's high school football coach, 12-time state champion Mike Smith, was happy that Curry would attend Virginia, Curry's AAU basketball coach Boo Williams told Curry he should decommit and go to a "basketball school" like North Carolina to get a better shot at the NBA.<ref>Ronald Curry Has All the Moves; The Washington Post – Washington, D.C.; Angie Watts; Apr 8, 1998; page C1</ref>

Curry decommitted from UVA on signing day, causing him to be called "Benedict Ronald" and "Benedict Curry" by the Virginia faithful who blamed him not only for the program losing out on his own services, but for losing out on the unrecruited Vick. Curry was lampooned in the media, earning the title "Sports Jerk of the Year" in the nationally syndicated ''[[Tank McNamara]]'' comic strip.  At North Carolina, Curry set many records including most career passing yards and most career total yards. He was twice named the most valuable player of post-season bowl games, doing so at the 1998 Las Vegas Bowl and the 2001 Peach Bowl. He also played basketball for two years, along with now-Green Bay Packers' defensive end Julius Peppers.  After an inspired combine, the physically impressive Curry spent seven seasons in the NFL as a wide receiver. The Cavaliers defeated UNC during Curry's freshman year (when he played behind starter Oscar Davenport), and he went 0–2 against UVA the next two season as a starting quarterback. The Tar Heels did finally beat Virginia when he was a senior and shared time under center with freshman Darian Durant.

===Famous Spectators===
[[File:Coolidge after signing indian treaty.jpg|thumb|[[Calvin Coolidge|President Calvin Coolidge]] attended the 1928 game held on [[Thanksgiving Day]] in [[Charlottesville, Virginia|Charlottesville]].]]
Probably the most famous spectator of this rivalry was present on Thanksgiving Day 1928. United States President [[Calvin Coolidge]] and First Lady [[Grace Goodhue Coolidge|Grace Anna Goodhue Coolidge]] were among the 20,000 spectators watching the game at Charlottesville to see North Carolina win 24–20 over Virginia.<ref>O'Neals (1968) ''Pictorial History of the University of Virginia''. Charlottesville, Virginia: University Press of Virginia (p.&nbsp;154)</ref>

==Game results==
{{Sports rivalry series table
| legend_tie_text = 
| legend_forfeit_text = Forfeits 
| cols = 2
| series_summary = yes
| format = compact
| team1 = North Carolina
| team1style = {{CollegePrimaryStyle|North Carolina Tar Heels|border=0|color=white}}
| team2 = Virginia
| team2style = {{CollegeSecondaryStyle|Virginia Cavaliers|border=0|color=white}}
| 1892 | Charlottesville, VA | North Carolina | 18 | Virginia | 30
| 1892 | Atlanta, GA | North Carolina | 26 | Virginia | 0
| 1893 | Richmond, VA | North Carolina | 0 | Virginia | 16
| 1894 | Richmond, VA | North Carolina | 0 | Virginia | 34
| 1895 | Richmond, VA | North Carolina | 0 | Virginia | 6
| 1896 | Richmond, VA | North Carolina | 0 | Virginia | 46
| 1897 | Richmond, VA | North Carolina | 0 | Virginia | 12
| 1898 | Richmond, VA | North Carolina | 6 | Virginia | 2
| 1900 | Norfolk, VA | North Carolina | 0 | Virginia | 17
| 1901 | Norfolk, VA | North Carolina | 6 | Virginia | 23
| 1902 | Richmond, VA | North Carolina | 12 | Virginia | 12
| 1903 | Richmond, VA | North Carolina | 16 | Virginia | 0
| 1904 | Richmond, VA | North Carolina | 11 | Virginia | 12
| 1905 | Norfolk, VA | North Carolina | 17 | Virginia | 0
| 1907 | Richmond, VA | North Carolina | 4 | Virginia | 9
| 1908 | Richmond, VA | North Carolina | 0 | Virginia | 31
| 1910 | Richmond, VA | North Carolina | 0 | Virginia | 7
| 1911 | Richmond, VA | North Carolina | 0 | Virginia | 28
| 1912 | Richmond, VA | North Carolina | 0 | Virginia | 66
| 1913 | Richmond, VA | North Carolina | 7 | Virginia | 26
| 1914 | Richmond, VA | North Carolina | 3 | Virginia | 20
| 1915 | Richmond, VA | North Carolina | 0 | Virginia | 14
| 1916 | Richmond, VA | North Carolina | 7 | Virginia | 0
| 1919 | Chapel Hill, NC | North Carolina | 6 | Virginia | 0
| 1920 | Charlottesville, VA | North Carolina | 0 | Virginia | 14
| 1921 | Chapel Hill, NC | North Carolina | 7 | Virginia | 3
| 1922 | Charlottesville, VA | North Carolina | 10 | Virginia | 7
| 1923 | Chapel Hill, NC | North Carolina | 0 | Virginia | 0
| 1924 | Charlottesville, VA | North Carolina | 0 | Virginia | 7
| 1925 | Chapel Hill, NC | North Carolina | 3 | Virginia | 3
| 1926 | Charlottesville, VA | North Carolina | 0 | Virginia | 3
| 1927 | Chapel Hill, NC | North Carolina | 14 | Virginia | 13
| 1928 | Charlottesville, VA | North Carolina | 24 | Virginia | 20
| 1929 | Chapel Hill, NC | North Carolina | 41 | Virginia | 7
| 1930 | Charlottesville, VA | North Carolina | 41 | Virginia | 0
| 1931 | Chapel Hill, NC | North Carolina | 13 | Virginia | 6
| 1932 | Charlottesville, VA | North Carolina | 7 | Virginia | 14
| 1933 | Chapel Hill, NC | North Carolina | 14 | Virginia | 0
| 1934 | Charlottesville, VA | North Carolina | 25 | Virginia | 6
| 1935 | Chapel Hill, NC | North Carolina | 61 | Virginia | 0
| 1936 | Charlottesville, VA | North Carolina | 59 | Virginia | 14
| 1937 | Chapel Hill, NC | North Carolina | 40 | Virginia | 0
| 1938 | Charlottesville, VA | North Carolina | 20 | Virginia | 0
| 1939 | Chapel Hill, NC | North Carolina | 19 | Virginia | 0
| 1940 | Charlottesville, VA | North Carolina | 10 | Virginia | 7
| 1941 | Chapel Hill, NC | North Carolina | 7 | Virginia | 28
| 1942 | Charlottesville, VA | North Carolina | 28 | Virginia | 13
| 1943 | Norfolk, VA | North Carolina | 54 | Virginia | 7
| 1944 | Norfolk, VA | North Carolina | 7 | Virginia | 26
| 1945 | Chapel Hill, NC | North Carolina | 27 | Virginia | 18
| 1946 | Charlottesville, VA | North Carolina | 49 | Virginia | 14
| 1947 | Chapel Hill, NC | North Carolina | 40 | Virginia | 7
| 1948 | Charlottesville, VA | North Carolina | 34 | Virginia | 12
| 1949 | Chapel Hill, NC | North Carolina | 14 | Virginia | 7
| 1950 | Charlottesville, VA | North Carolina | 13 | Virginia | 44
| 1951 | Charlottesville, VA | North Carolina | 14 | Virginia | 34
| 1952 | Chapel Hill, NC | North Carolina | 7 | Virginia | 34
| 1953 | Charlottesville, VA | North Carolina | 33 | Virginia | 7
| 1954 | Charlottesville, VA | North Carolina | 26 | Virginia | 14
| 1955 | Chapel Hill, NC | North Carolina | 26 | Virginia | 14
| 1956 {{ref|Vacated Win|*}} | Charlottesville, VA | North Carolina | 21 | Virginia | 7 | win61 = Virginia
| 1957 | Chapel Hill, NC | North Carolina | 13 | Virginia | 20
| 1958 | Charlottesville, VA | North Carolina | 42 | Virginia | 0
| 1959 | Chapel Hill, NC | North Carolina | 41 | Virginia | 0
| 1960 | Charlottesville, VA | North Carolina | 35 | Virginia | 8
| 1961 | Chapel Hill, NC | North Carolina | 24 | Virginia | 0
| 1962 | Charlottesville, VA | North Carolina | 11 | Virginia | 7
| 1963 | Chapel Hill, NC | North Carolina | 11 | Virginia | 7
| 1964 | Charlottesville, VA | North Carolina | 27 | Virginia | 31
| 1965 | Chapel Hill, NC | North Carolina | 17 | Virginia | 21
| 1966 | Chapel Hill, NC | North Carolina | 14 | Virginia | 21
| 1967 | Charlottesville, VA | North Carolina | 17 | Virginia | 40
| 1968 | Chapel Hill, NC | North Carolina | 6 | Virginia | 41
| 1969 | Charlottesville, VA | North Carolina | 12 | Virginia | 0
| 1970 | Chapel Hill, NC | North Carolina | 30 | Virginia | 15
| 1971 | Charlottesville, VA | North Carolina | 32 | Virginia | 20
| 1972 | Chapel Hill, NC | North Carolina | 23 | Virginia | 3
| 1973 | Charlottesville, VA | North Carolina | 40 | Virginia | 44
| 1974 | Chapel Hill, NC | North Carolina | 24 | Virginia | 10
| 1975 | Charlottesville, VA | North Carolina | 31 | Virginia | 28
| 1976 | Chapel Hill, NC | North Carolina | 31 | Virginia | 6
| 1977 | Charlottesville, VA | North Carolina | 35 | Virginia | 14
| 1978 | Chapel Hill, NC | North Carolina | 38 | Virginia | 20
| 1979 | Charlottesville, VA | North Carolina | 13 | Virginia | 7
| 1980 | Chapel Hill, NC | North Carolina | 26 | Virginia | 3
| 1981 | Charlottesville, VA | North Carolina | 17 | Virginia | 14
| 1982 | Chapel Hill, NC | North Carolina | 27 | Virginia | 14
| 1983 | Charlottesville, VA | North Carolina | 14 | Virginia | 17
| 1984 | Chapel Hill, NC | North Carolina | 24 | Virginia | 24
| 1985 | Charlottesville, VA | North Carolina | 22 | Virginia | 24
| 1986 | Chapel Hill, NC | North Carolina | 27 | Virginia | 7
| 1987 | Charlottesville, VA | North Carolina | 17 | Virginia | 20
| 1988 | Chapel Hill, NC | North Carolina | 24 | Virginia | 27
| 1989 | Charlottesville, VA | North Carolina | 17 | Virginia | 50
| 1990 | Chapel Hill, NC | North Carolina | 10 | Virginia | 24
| 1991 | Charlottesville, VA | North Carolina | 9 | Virginia | 14
| 1992 | Chapel Hill, NC | North Carolina | 27 | Virginia | 7
| 1993 | Charlottesville, VA | North Carolina | 10 | Virginia | 17
| 1994 | Charlottesville, VA | North Carolina | 10 | Virginia | 34
| 1995 | Chapel Hill, NC | North Carolina | 22 | Virginia | 17
| 1996 | Charlottesville, VA | North Carolina | 17 | Virginia | 20
| 1997 | Chapel Hill, NC | North Carolina | 48 | Virginia | 20
| 1998 | Charlottesville, VA | North Carolina | 13 | Virginia | 30
| 1999 | Chapel Hill, NC | North Carolina | 17 | Virginia | 20
| 2000 | Charlottesville, VA | North Carolina | 6 | Virginia | 17
| 2001 | Chapel Hill, NC | North Carolina | 30 | Virginia | 24
| 2002 | Charlottesville, VA | North Carolina | 27 | Virginia | 37
| 2003 | Chapel Hill, NC | North Carolina | 13 | Virginia | 38
| 2004 | Charlottesville, VA | North Carolina | 24 | Virginia | 56
| 2005 | Chapel Hill, NC | North Carolina | 7 | Virginia | 5
| 2006 | Charlottesville, VA | North Carolina | 0 | Virginia | 23
| 2007 | Chapel Hill, NC | North Carolina | 20 | Virginia | 22
| 2008 | Charlottesville, VA | North Carolina | 13 | Virginia | 16
| 2009 | Chapel Hill, NC | North Carolina | 3 | Virginia | 16
| 2010 | Charlottesville, VA | North Carolina | 44 | Virginia | 10
| 2011 | Chapel Hill, NC | North Carolina | 28 | Virginia | 17
| 2012 | Charlottesville, VA | North Carolina | 37 | Virginia | 13
| 2013 | Chapel Hill, NC | North Carolina | 45 | Virginia | 14
| 2014 | Charlottesville, VA | North Carolina | 28 | Virginia | 27
| 2015 | Chapel Hill, NC | North Carolina | 26 | Virginia | 13
| 2016| Charlottesville, VA | North Carolina | 35 | Virginia | 14
}}

<div class="NavEnd">&nbsp;</div>
:<sup>{{note|Vacated Win|*}}</sup>North Carolina forfeits due to using an ineligible player

==Other sports==
===Basketball===
Carolina currently leads the series 127–50.

== The Deep South's Oldest Rivalry ==
The [[Deep South's Oldest Rivalry]] (Auburn-Georgia) may eventually surpass "South's Oldest Rivalry" (UNC-Virginia) in number of games played due to the conference expansion of the [[Southeastern Conference|SEC]] and ACC conferences. With the possibility of a same-season rematch in the [[SEC Championship]], Auburn and Georgia can play a second game in the same season; North Carolina and Virginia, however, are in the same division of the ACC, making a similar North Carolina vs. Virginia ACC Championship matchup impossible. Currently the UVA-UNC series leads the AUB-UGA series by one game.  However, the UVA-UNC series has played far more consecutive years, as AUB-UGA was suspended during [[World War II]].

== See also ==
* [[Most played rivalries in NCAA Division I FBS]]

== Notes ==
{{note|a|a}} North Carolina forfeited the 1956 game to Virginia for using an ineligible player.<ref>{{cite web|url=http://www.virginiasports.com/ViewArticle.dbml?DB_OEM_ID=17800&ATCLID=1603210 |title=Wahoos Play Host to No. 18/22 UNC Saturday – University of Virginia Cavaliers Official Athletic Site |publisher=VirginiaSports.com |date= |accessdate=2012-07-26}}</ref><ref>[http://www.collegian.psu.edu/archive/2007/10/04/forfeits_uncommon_in_realm_of.aspx Jon Blau, Penn State Daily Collegian, "Forfeits uncommon in realm of college sports"] {{webarchive |url=https://web.archive.org/web/20120222054633/http://www.collegian.psu.edu/archive/2007/10/04/forfeits_uncommon_in_realm_of.aspx |date=February 22, 2012 }}</ref><ref>[http://sportsillustrated.cnn.com/vault/article/magazine/MAG1133140/index.htm Sports Illustrated, 1957 Football Issue, September 23, 1957]</ref>  The UNC athletic department does not acknowledge the forfeit when reporting on the result, and chooses to count the game as a UNC win in its marketing materials.<ref>[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/101608aaa.html ] {{webarchive |url=https://web.archive.org/web/20081019042938/http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/101608aaa.html |date=October 19, 2008 }}</ref>

<sup>1</sup>Virginia won the first game played in 1892.<br />
<sup>2</sup>North Carolina won the second game played in 1892.

==References==
{{Reflist|1}}

{{North Carolina Tar Heels football navbox}}
{{Virginia Cavaliers football navbox}}
{{Atlantic Coast Conference rivalry navbox}}

[[Category:College football rivalries in the United States]]
[[Category:North Carolina Tar Heels football]]
[[Category:Virginia Cavaliers football]]
[[Category:University of North Carolina at Chapel Hill rivalries]]