 {{Infobox journal
| title = Physical Review
| cover = 
| editor = [[Pierre Meystre]] (Editor in Chief)
| discipline = [[Physics]]
| peer-reviewed = 
| language = English
| abbreviation = Phys. Rev.
| publisher = [[American Physical Society]]
| country = [[United States]]
| frequency = 
| history = 1893–1913 ''Series&nbsp;I''<br>
1913–1970 ''Series&nbsp;II''<br>
{{nowrap|1970–Present ''Series&nbsp;III''}}<br>
{{nowrap|1970–present ''Phys. Rev. A'', ''B'', ''C'', ''D''}}<br>
1993–present ''Phys. Rev. E''<br>
1998–present ''Phys. Rev. AB''<br>
2005–present ''Phys. Rev. PER''<br>
2008–present ''Physics''<br>
2011–present ''PRX''<br>
2014–present ''Phys. Rev. Applied'' <br>
2016–present ''Phys. Rev. Fluids''
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://journals.aps.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC =
| LCCN = 
| CODEN = 
| ISSN = 
| eISSN = 
}}
'''''Physical Review''''' is an American [[Peer review|peer-reviewed]] [[scientific journal]] established in 1893 by [[Edward Leamington Nichols|Edward Nichols]]. It publishes [[original research]] as well as [[scientific review|scientific]] and [[literature review]]s on all aspects of [[physics]]. It is published by the [[American Physical Society]] (APS). The journal is in its third series, and is split in several sub-journals each covering a particular field of physics. It has a sister journal, ''[[Physical Review Letters]]'', which publishes shorter articles of broader interest.

== History ==
''Physical Review'' commenced publication in July 1893, organized by [[Cornell University]] professor [[Edward Leamington Nichols|Edward Nichols]] and helped by the new president of Cornell, [[J. Gould Schurman]]. The journal was managed and edited at Cornell in upstate [[New York (state)|New York]] from 1893 to 1913 by Nichols, [[Ernest Merritt]], and [[Frederick Bedell]]. The 33 volumes published during this time constitute ''Physical Review Series&nbsp;I''.

The [[American Physical Society]] (APS), founded in 1899, took over its publication in 1913 and started ''Physical Review Series&nbsp;II''. The journal remained at Cornell under [[editor-in-chief]] [[G. S. Fulcher]] from 1913 to 1926, before relocating to the location of editor [[John Torrence Tate, Sr.]]<ref group=nb>Not to be confused with his son, the [[number theory|number theorist]] [[John Torrence Tate|John Torrence Tate Jr.]]</ref> at the [[University of Minnesota]]. In 1929, the APS started publishing ''[[Reviews of Modern Physics]]'', a venue for longer review articles.

During the [[Great Depression]], wealthy scientist [[Alfred Lee Loomis|Alfred Loomis]] anonymously paid the journal's fees for authors who could not afford them.<ref>
{{cite book
 |last=Conant |first=Jennet
 |year=2002
 |title=Tuxedo Park
 |page=106
 |publisher=[[Simon & Schuster]]
 |location=New York
 |isbn=0-684-87287-0
}}</ref>

After Tate's death in 1950, the journals were managed on an interim basis still in Minnesota by [[E. L. Hill]] and [[J. William Buchta]] until [[Samuel Goudsmit]] and Simon Pasternack were appointed and the editorial office moved to [[Brookhaven National Laboratory]] on Eastern [[Long Island]], [[New York City|New York]]. In July 1958, the sister journal ''[[Physical Review Letters]]'' was introduced to publish short articles of particularly broad interest, initially edited by [[George L. Trigg]], who remained as editor until 1988.

In 1970, ''Physical Review'' split into sub-journals ''Physical Review A'', ''B'', ''C'', and ''D''. A fifth member of the family, ''Physical Review E'', was introduced in 1993 to a large part to accommodate the huge amount of new research in [[nonlinear dynamics]]. Combined, these constitute ''Physical Review Series III''.

The editorial office moved in 1980 to its present location across the expressway from Brookhaven National Laboratory. Goudsmit retired in 1974 and Pasternack in the mid-1970s. Past Editors in Chief include [[David Lazarus (physicist)|David Lazarus]] (1980—1990; [[University of Illinois at Urbana-Champaign]]), [[Benjamin Bederson]] (1990—1996; [[New York University]]), [[Martin Blume]] (1996—2007; Brookhaven National Laboratory), and [[Gene Sprouse]] (2007—2015; [[SUNY Stony Brook]]). The current Editor in Chief is [[Pierre Meystre]], whose term began in August 2016.<ref>
{{cite web
 |date=July 2016
 |author=David Voss 
 |url=https://www.aps.org/publications/apsnews/201607/editor-chief.cfm
 |title=APS Selects Editor in Chief
 |publisher=[[American Physical Society]]
}}</ref>

To celebrate the hundredth anniversary of the journal, a memoir was published jointly by the APS and AIP.<ref>
{{cite book
 |last=Hartman |first=Paul
 |year=1994
 |title=A Memoir on The Physical Review: A history of the first hundred years
 |page=212
 |publisher=[[American Physical Society]] & [[American Institute of Physics]]
 |location=New York
 |isbn=1-56396-282-9
}}</ref>

In 1998, the first issue of ''Physical Review Special Topics: Accelerators and Beams'' was published, and in 2005, ''Physical Review Special Topics: Physics Education Research'' was launched. In January 2016 the names of both journals were changed to remove "Special Topics".<ref name="special-topics-change">[http://journals.aps.org/edannounce/renaming-the-aps-special-topics-series] Renaming the APS Special Topics Series, American Physical Society, December 31, 2015</ref> ''Physical Review'' also started an online magazine, ''[[Physical Review Focus]]'', in 1998 to explain, and provide historical context for, selected articles from ''Physical Review'' and ''Physical Review Letters''. This was later merged into ''[[Physics (American Physical Society journal)|Physics]]''. The Special Topics journals are [[open access]]; ''Physics Education Research'' requires page charges from the authors, but ''Physical Review Special Topics: Accelerators and Beams'' does not. Though not fully open access, ''Physical Review Letters'' also requires an author page charge, although this is voluntary. The other journals require such a charge only if manuscripts are not prepared in one of the preferred formats.<ref>
{{cite web
 |date=March 2008
 |url=http://authors.aps.org/esubs/guidelines.html
 |title=Submission guidelines
}}</ref> Authors can pay extra charges to make their papers open access.<ref name="oa">[http://publish.aps.org/edannounce/CC-launch-press-release] APS Open Access announcement, American Physical Society, 15 February 2011</ref> Such papers are published under the terms of the [[Creative Commons Attribution 3.0 License]] (CC-BY).<ref>[http://creativecommons.org/licenses/by/3.0/] Details of Creative Commons license</ref> ''Physical Review Letters'' celebrated their 50th birthday in 2008.<ref>
{{cite web
 |url=http://prl.aps.org/50years
 |title=''Physical Review Letters'' Celebrates 50 Years 
 |publisher=[[American Physical Society]]
}}</ref> The APS has a [[copyright]] policy to permit the author to reuse parts of the published article in a derivative or new work, including on [[Wikipedia]].<ref>
{{cite web
 |date=1 October 2008
 |author=Gene D. Sprouse 
 |url=http://publish.aps.org/edannounce/PhysRevLett.101.140001
 |title=APS now leaves copyright with authors for derivative works
 |publisher=[[American Physical Society]]
}}</ref>

The APS has a publication entitled ''Physics'',<ref>
{{cite web
 |title=''Physics''
 |url=http://physics.aps.org/
 |publisher=[[American Physical Society]]
}}</ref> aiming to help physicists and physics students to learn about new developments outside of their own subfield. This now includes the longer general-interest articles that appeared as ''Focus''. It also publishes ''Physical Review X (PRX)'',<ref>
{{cite web
 |title=''Physical Review X (PRX)''
 |url=http://prx.aps.org/
 |publisher=[[American Physical Society]]
}}</ref> an online-only gold [[open access journal]]. It is a peer-reviewed journal that publishes, as timely as possible, original research papers from all areas of pure, applied, and interdisciplinary physics. In 2014, the newest APS journal 
''Physical Review Applied''<ref>
{{cite web
 |title=''Physical Review Applied''
 |url=http://journals.aps.org/prapplied/
 |publisher=[[American Physical Society]]
}}</ref> began publishing research across all aspects of experimental and theoretical applications of physics, including their interactions with other sciences, engineering, and industry. In 2015, the APS announced the creation of a new journal ''Physical Review Fluids''
<ref>
{{cite web
 |title=''Physical Review Fluids''
 |url=http://journals.aps.org/prfluids/
 |publisher=[[American Physical Society]]
}}</ref> to better serve the Fluids community.

==Journals==
<!-- Abbreviations for the names of the journals are commonly used in citations, hence why they are given here. -->
{| class="wikitable"
|-
! Journal
! Abbreviation
! Editor(s)
! [[Impact factor]] (2015)
! Published
! Scope
! ISSN
! Website
|-
| ''Physical Review Series I''
| Phys. Rev.
|
| 
| 1893–1912
| All of [[Physics]]
|
| [http://prola.aps.org/ All volumes]
|-
| ''Physical Review Series II''{{refn| group="nb" |name="ser2-ab"|Volumes 133-140 of the Series II in years 1964 and 1965 were split into issues A and B. Later they were unified into a single series again.<ref>{{cite web|url=http://ci.nii.ac.jp/ncid/AA00773577?l=en|title= The physical review. Second series. A}} {{cite web|url=http://ci.nii.ac.jp/ncid/AA00773588?l=en|title= The physical review. Second series. B|publisher=[[National Institute of Informatics]]|accessdate=2016-12-28}}</ref> They are different from ''Phys. Rev. A'' and ''B'' of the third series. For example "[http://journals.aps.org/pr/abstract/10.1103/PhysRev.133.A1 Phys. Rev. '''133''' A1 (1964)]" is an article of Ser. II, while "[http://journals.aps.org/pra/abstract/10.1103/PhysRevA.1.1 Phys. Rev. A '''1''' 1 (1970)] is of Phys. Rev. A.}} 
| Phys. Rev.
|
|
| 1913–1969
| All of Physics
|
| [http://prola.aps.org/ Archive of All volumes]
|-
| ''[[Physical Review Letters]]''
| [http://journals.aps.org/prl/ Phys. Rev. Lett.]
| [[Hugues Chate]]<br>{{nowrap|[[Reinhardt B. Schuhmann]]}}<br>{{nowrap|[[Robert Garisto]]}}<br>[[Sami Mitra]]
|7.645
| 1958–present
| Important fundamental research in all fields of physics
| {{nowrap|{{ISSN|0031-9007}} (print)}}<br>{{eISSN|1079-7114}} (web)
| [http://prola.aps.org/ 1958–2002]
[http://prl.aps.org/ 2003–present]
|-
| ''[[Physical Review A]]''<ref group="nb" name="ser2-ab"/>
| [http://pra.aps.org/ Phys. Rev. A]
| [[Gordon W. F. Drake]]<br>[[Thomas Pattard]]
| 2.765
| 1970–present
| [[Atomic, molecular, and optical physics]] and [[quantum information]]
| {{ISSN|1050-2947}} (print)<br>{{eISSN|1094-1622}} (web)
| [http://prola.aps.org/ 1970–2002]
[http://pra.aps.org/ 2003–present]
|-
| ''[[Physical Review B]]''<ref group="nb" name="ser2-ab"/>
| [http://prb.aps.org/ Phys. Rev. B]
| [[Laurens W. Molenkamp]]<br>[[Anthony M. Begley]]
| 3.718
| 1970–present
| [[Condensed matter physics|Condensed matter]] and [[materials science|materials physics]]
| {{ISSN|1098-0121}} (print)<br>{{eISSN|1550-235X}} (web)
| [http://prola.aps.org/ 1970–2002]
[http://prb.aps.org/ 2003–present]
|-
| ''[[Physical Review C]]''
| [http://prc.aps.org/ Phys. Rev. C]
| [[Benjamin F. Gibson (physicist)|Benjamin F. Gibson]]<br>[[Christopher Wesselborg]]
| 3.146
| 1970–present
| [[Nuclear physics]]
| {{ISSN|0556-2813}} (print)<br>{{eISSN|1089-490X}} (web)
| [http://prola.aps.org/ 1970–2002]
[http://prc.aps.org/ 2003–present]
|-
| ''[[Physical Review D]]''
| [http://prd.aps.org/ Phys. Rev. D]
| [[Erick J. Weinberg]]<br>[[Urs Heller]]
| 4.506
| 1970–present
| [[Particle physics|Particles]], [[Quantum field theory|fields]], [[gravitation]], and [[physical cosmology|cosmology]]
| {{ISSN|1550-7998}} (print)<br>{{eISSN|1550-2368}} (web)
| [http://prola.aps.org/ 1970–2002]
[http://prd.aps.org/ 2003–present]
|-
| ''[[Physical Review E]]''
| [http://pre.aps.org/ Phys. Rev. E]
| [[Eli Ben-Naim]]<br>[[Dirk Jan Bukman]]
| 2.252
| 1993–present
| [[Statistical physics|Statistical]], [[chaos theory|nonlinear]], [[biology|biological]] and [[soft matter|soft matter physics]]
| {{ISSN|1539-3755}} (print)<br>{{eISSN|1550-2376}} (web)
| [http://prola.aps.org/ 1970–2002]
[http://pre.aps.org/ 2003–present]
|-
| ''[[Physical Review X]]''
| [http://prx.aps.org/ Phys. Rev. X]
| [[Cristina Marchetti]]<br>[[Jean-Michel Raimond]]<br>[[Ling Miao]]
| 8.701
| 2011–present
| "Broad subject coverage encouraging communication across related fields"
| {{ISSN|2160-3308}} (web)
| [http://prx.aps.org/ All volumes]
|-
| ''[[Physical Review Accelerators and Beams]]''
| [http://prst-ab.aps.org/ Phys. Rev. AB]
| [[Frank Zimmermann]]
| 1.500	
| 1998–present
| [[Particle accelerator]]s and beams
| {{ISSN|2469-9888}} (web)
| [http://journals.aps.org/prab/ All volumes]
|-
| ''[[Physical Review Physics Education Research]]''
| [http://journals.aps.org/prper/ Phys. Rev. PER]
| Charles Henderson 
| 1.316
| 2005–present
| [[Physics education research]]
| {{ISSN|2469-9896}} (web)
| [http://journals.aps.org/prper/ All volumes]
|-
| ''[[Physics (American Physical Society journal)|Physics]]''
| [http://physics.aps.org/ Physics]
| Jessica Thomas
|
| 2008–present
| All of [[Physics]]
| {{ISSN|1943-2879}} (web)
| [http://physics.aps.org/ All volumes]
|-
| ''[[Physical Review Applied]]''
| [http://journals.aps.org/prapplied/ Phys. Rev. Appl.]
| [[Troy Shinbrot]]<br>[[Julie Kim-Zajonz]]
|4.061
| 2014–present
| "All aspects of experimental and theoretical applications of physics"
| {{ISSN|2331-7019}} (web)
| [http://journals.aps.org/prapplied/ All volumes]
|-
|-
| ''Physical Review Fluids''
| [http://journals.aps.org/prfluids/ Phys. Rev. Fluids]
| [[John Kim (professor)|John Kim]]<br>[[L. Gary Leal]]
| 
| 2016–present
| "Innovative research that will significantly advance the fundamental understanding of fluids"
| {{ISSN|2469-990X}} (web)
| [http://journals.aps.org/prfluids/ All volumes]
|-
|}

==Notes and references==
;Notes:
{{reflist|group=nb}}

;References:
{{reflist}}

==External links==
*[http://www.aps.org/ American Physical Society]
*[http://journals.aps.org/ Journals of the APS]
*[http://physics.aps.org/ New APS publication ''Physics'']
*[http://journals.aps.org/archive/ Online archive of all back issues of ''Physical Review'' (subscription required)]

[[Category:Physics journals]]
[[Category:Publications established in 1893]]
[[Category:English-language journals]]
[[Category:American Physical Society academic journals]]