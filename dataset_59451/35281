{{Good article}}
{{EngvarB|date=April 2014}}
{{Use dmy dates|date=April 2014}}
{{Infobox football match
|                   title = 2005 Football League Cup Final
|                   image = 2005 Football League Cup programme.jpg
|               imagesize = 200px
|                 caption = Match programme cover
|                   event = [[2004–05 Football League Cup]]
|                   team1 = [[Liverpool F.C.|Liverpool]]
|        team1association =
|              team1score = 2
|                   team2 = [[Chelsea F.C.|Chelsea]]
|        team2association =
|              team2score = 3
|                 details = After [[Overtime (sports)#Association football|extra time]]
|                    date = 27 February 2005
|                 stadium = [[Millennium Stadium]]
|                    city = [[Cardiff]]
|      man_of_the_match1a = [[John Terry]] (Chelsea)<ref name="Man of match"/>
|                 referee = [[Steve Bennett (referee)|Steve Bennett]] ([[Kent]])
|              attendance = 78,000
|                 weather =
|                previous = [[2004 Football League Cup Final|2004]]
|                    next = [[2006 Football League Cup Final|2006]]
}}

The '''2005 Football League Cup Final''' was a [[association football|football]] match played between [[Liverpool F.C.|Liverpool]] and [[Chelsea F.C.|Chelsea]] on 27 February 2005 at the [[Millennium Stadium]], [[Cardiff]]. It was the final match of the [[2004–05 Football League Cup]], the 45th season of the [[Football League Cup]], a football competition for the 92 teams in the [[Premier League]] and [[The Football League|Football League]]. Liverpool were appearing in their tenth final; they had previously won seven and lost two while Chelsea were appearing in the final for the fourth time. They had previously won twice and lost once.

As both teams were in the Premier League, they entered the competition in the third round. Liverpool's matches were generally close affairs, with only two victories secured by two goals or more. A 2–0 win over [[Middlesbrough]] in the fourth round was followed by a 4–3 victory in a [[Penalty shootout (association football)|penalty shootout]] after their quarter-final match with [[Tottenham Hotspur F.C.|Tottenham Hotspur]] finished 1–1. Chelsea's matches were also close affairs, their only victory by more than one goal was against [[Newcastle United F.C.|Newcastle United]] in the fourth round, which they won 2–0.

Watched by a crowd of 78,000 Liverpool scored inside the first minute when [[John Arne Riise]] volleyed a [[Fernando Morientes]] pass into the Chelsea goal. Neither side was able to score despite a number of chances, until the 79th minute when Liverpool captain [[Steven Gerrard]] headed a [[Paulo Ferreira]] free-kick into his own net. [[José Mourinho]] was [[Ejection (sports)|ordered to the stands]] after Chelsea's equaliser for making a gesture to the Liverpool fans. Despite the incident, Chelsea continued their dominance into [[Overtime (sports)|extra-time]] and a goal each from [[Didier Drogba]] and [[Mateja Kežman]] gave them a 3–1 lead. A minute later [[Antonio Núñez Tena|Antonio Núñez]] of Liverpool scored a header to reduce the deficit to 3–2, but Chelsea held to win the match and the League Cup for the third time.

==Route to the final==
{{main|2004–05 Football League Cup}}

===Liverpool===
{| class="wikitable plainrowheaders" style="text-align:center;margin-left:1em;float:right"
! scope="col" style="width:25px;"| Round
! scope="col" style="width:100px;"| Opponents
! scope="col" style="width:50px;"| Score
|-
!scope=row style="text-align:center"|3rd
| [[Millwall F.C.|Millwall]] (a)
| 3–0
|-
!scope=row style="text-align:center"|4th
| [[Middlesbrough F.C.|Middlesbrough]] (h)
| 2–1
|-
!scope=row style="text-align:center"|QF
| [[Tottenham Hotspur F.C.|Tottenham Hotspur]] (a)
| 1–1 (4–3 [[Penalty shoot-out (association football)|pen]])
|-
!scope=row style="text-align:center" rowspan="2" | SF
| [[Watford F.C.|Watford]] (h)
| 1–0
|-
| Watford (a)
| 1–0
|}
As Liverpool were competing in the Premier League, they entered the competition in the third round and were drawn against [[Millwall F.C.|Millwall]], who were in the [[Football League Championship|Championship]]. Liverpool fielded a team that did not feature many of their regular starters including [[Steven Gerrard]] and [[Xabi Alonso]]. Despite this, they won the match at Millwall's home ground [[The New Den]], 3–0 courtesy of two goals from striker [[Milan Baroš]] and midfielder [[Salif Diao]].<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/3947041.stm |title=Millwall 0–3 Liverpool |publisher=BBC Sport |date=26 October 2004 |accessdate=5 June 2015 }}</ref> Liverpool were drawn against fellow Premier League side [[Middlesbrough F.C.|Middlesbrough]] in the fourth round. Again Liverpool manager [[Rafael Benítez]] did not field many of his first choice players, however two goals from striker [[Neil Mellor]] secured a 2–0 victory at their home ground of [[Anfield]].<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/3988401.stm |title=Liverpool 2–0 Middlesbrough |publisher=BBC Sport |date=9 November 2004 |accessdate=5 June 2015 }}</ref>

[[Tottenham Hotspur F.C.|Tottenham Hotspur]] were the opposition in the quarter-final held at their opponent's home ground [[White Hart Lane]]. With neither side scoring during 90 minutes, the match went to [[extra time]]. Tottenham took the lead in the 108th minute when striker [[Jermain Defoe]] scored. Tottenham appeared to be on course for victory, but a handball in their penalty area by [[Frédéric Kanouté]] resulted in a Liverpool penalty, which striker [[Florent Sinama-Pongolle]] scored. With no further goals, the match went to a [[Penalty shoot-out (association football)|penalty shoot-out]]. Liverpool won the subsequent shootout 4–3 to progress to the semi-final.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4047619.stm |title=Tottenham 1–1 Liverpool |publisher=BBC Sport |date=1 December 2004 |accessdate=5 June 2015 }}</ref>

Championship side [[Watford F.C.|Watford]] were the opposition in the semi-final which were held over two legs. Unlike previous rounds, Benítez picked a near full-strength team for the first leg at Anfield. The only goal of the match was scored in the 56th minute by Liverpool captain Gerrard.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4157241.stm |title=Liverpool 1–0 Watford |date=11 January 2005 |accessdate=5 June 2015 }}</ref> The second leg at Watford's home ground, [[Vicarage Road]] finished in the same scoreline, with Gerrard again scoring the winning goal. As a result, Liverpool won the match 1–0 and 2–0 on aggregate to progress to the final.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4197547.stm |title=Watford 0–1 Liverpool |publisher=BBC Sport |date=25 January 2005 |accessdate=5 June 2015 }}</ref>

===Chelsea===
{| class="wikitable plainrowheaders" style="text-align:center;margin-left:1em;float:right"
! scope="col" style="width:25px;"| Round
! scope="col" style="width:100px;"| Opponents
! scope="col" style="width:50px;"| Score
|-
!scope=row style="text-align:center"|3rd
| [[West Ham United F.C.|West Ham United]] (h)
| 1–0
|-
!scope=row style="text-align:center"|4th
| [[Newcastle United F.C.|Newcastle United]] (a)
| 2–0 ([[Overtime (sports)#Association football|a.e.t]])
|-
!scope=row style="text-align:center"|QF
| [[Fulham F.C.|Fulham]] (a)
| 2–1 
|-
!scope=row style="text-align:center" rowspan="2" | SF
| [[Manchester United F.C.|Manchester United]] (h)
| 0–0 (h)
|-
| Manchester United (a)
| 2–1
|}
[[West Ham United F.C.|West Ham United]] were Chelsea's opposition, as they entered the competition in the third round due to being in the Premier League. The match held at Chelsea's home ground [[Stamford Bridge (stadium)|Stamford Bridge]], saw a [[Mateja Kežman]] goal secure a 1–0 victory for Chelsea.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/3947055.stm |title=Chelsea 1–0 Newcastle |publisher=BBC Sport |date=27 October 2014 |accessdate=5 June 2015 }}</ref> Their opponents in the fourth round were fellow Premier League team [[Newcastle United F.C.|Newcastle United]]. The match, held at Newcastle's home ground [[St James' Park]], was goalless for the first 90 minutes and went to extra-time. Goals from [[Eiður Guðjohnsen]] and [[Arjen Robben]] resulted in a 2–0 victory for Chelsea.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/3988419.stm |title=Newcastle 0–2 Chelsea |date=10 November 2004 |accessdate=5 June 2015 }}</ref>

Local rivals [[Fulham F.C.|Fulham]] were Chelsea's opposition in the quarter-final, with the match held at Fulham's home ground, [[Craven Cottage]]. A goal in the 55th minute from midfielder [[Damien Duff]] gave Chelsea a 1–0 lead. However, Fulham equalised in the 74th minute when striker [[Brian McBride]] scored. With both teams pushing forward to score the winning goal, substitute [[Frank Lampard]] scored in the 88th minute to secure a 2–1 victory for Chelsea.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4047489.stm |title=Fulham 1–2 Chelsea |date=30 November 2004 |accessdate=5 June 2015 }}</ref>

Chelsea were drawn against [[Manchester United F.C.|Manchester United]] in the semi-final. The first leg at Stamford Bridge finished 0–0 with both teams missing chances to score.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4157097.stm |title=Chelsea 0–0 Manchester United |date=12 January 2005 |accessdate=5 June 2015 }}</ref> The second leg was held at United's home ground [[Old Trafford]]. Chelsea took an early lead when Lampard scored in the 29th minute, but United equalised in the 67th minute when midfielder [[Ryan Giggs]] scored. In the 87th minute, Duff scored from a {{convert|50|yd|m}} [[Direct free kick|free kick]] to make the score 2–1. Thus, Chelsea won the tie 2–1 on aggregate to progress to the final.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4197611.stm |title=Manchester United 1–2 Chelsea |date=26 January 2005 |accessdate=5 June 2015 }}</ref>

==Match==

===Background===
[[File:Inside the Millennium Stadium, Cardiff.jpg|left|thumb|The [[Millennium Stadium]], the venue of the Football League Cup Final.]]
Liverpool were appearing in their tenth final they had won seven ([[1981 Football League Cup Final|1981]], [[1982 Football League Cup Final|1982]], [[1983 Football League Cup Final|1983]], [[1984 Football League Cup Final|1984]], [[1995 Football League Cup Final|1995]], [[2001 Football League Cup Final|2001]], [[2003 Football League Cup Final|2003]]) and lost two ([[1978 Football League Cup Final|1978]], [[1987 Football League Cup Final|1987]]). This was Chelsea's fourth appearance in the final. They had won twice in [[1965 Football League Cup Final|1965]] and [[1998 Football League Cup Final|1998]] and lost once in [[1972 Football League Cup Final|1972]]. The last match between the two sides before the final was on 1 January 2005, when an 80th-minute goal by midfielder [[Joe Cole]] secured a 1–0 victory for Chelsea at Anfield.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/eng_prem/4114653.stm |title=Liverpool 0–1 Chelsea |publisher=BBC Sport |date=1 January 2005 |accessdate=5 June 2015 }}</ref> Both sides last match before the final was in the [[2004–05 UEFA Champions League]]. They were playing the first leg of their round of 16 matches. Liverpool faced German team [[Bayer 04 Leverkusen|Bayer Leverkusen]], whom they beat 3–1,<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/europe/4281707.stm |title=Liverpool 3–1 Bayer Leverkusen |publisher=BBC Sport |date=22 February 2005 |accessdate=5 June 2015 }}</ref> while Chelsea were beaten 2–1 by Spanish team [[FC Barcelona|Barcelona]].<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/europe/4281719.stm |title=Barcelona 2–1 Chelsea |publisher=BBC Sport |date=23 February 2005 |accessdate=5 June 2015 }}</ref>

Liverpool manager [[Rafael Benítez]] was adamant that opponents Chelsea were the team under the most pressure to win the match: "There will be more pressure on them, they have spent a lot of money and are the best team in the league, because they are in first position, but they've lost their last two important games. That means they will be under pressure. People will see them as the favourites, I'm sure, so the pressure is on them and not us." Benítez was confident that playing in a cup final would be beneficial to his players: "We need experiences like this to progress, when I decided to come here I said I wanted to win trophies. Our challenge is to win the Premier League, the Champions League, the Uefa Cup, the FA Cup and the Carling Cup. This is a chance to start."<ref>{{cite news|url=https://www.theguardian.com/football/2005/feb/26/newsstory.carlingcup0405 |title='The pressure is on them' claims Benítez |work=The Guardian |location=London |date=26 February 2005 |accessdate=6 June 2015 |first=Dominic |last=Fifield }}</ref> Goalkeeper [[Jerzy Dudek]] believed winning the competition in 2003 would benefit Liverpool: "It was great to win there before and I also had one of my best performances for Liverpool. There's a great buzz around the place; we know that the club expects to win trophies, and we're in a position to deliver that later this week."<ref>{{cite news|url= https://www.theguardian.com/football/2005/feb/24/newsstory.carlingcup0405 |title=Dudek vows to keep calm in Cardiff as mistakes mount |work=The Guardian |location=London |date=24 February 2005 |accessdate=6 June 2015 |first=Dominic |last=Fifield }}</ref>

Chelsea midfielder Joe Cole was keen to win his first trophy in the final: "We don't feel under any great pressure, but we do feel excited about the possibility of winning our first trophy, we've just been imagining what it would be like to lift the trophy, who's going where in the line if we win it, and how we'll run round Cardiff with it." Teammate Frank Lampard was also keen to win his first trophy: "If you haven't won anything, a runners-up medal is not what you want at all." He also stated that Chelsea manager [[José Mourinho]]'s desire to win had been rubbing off on the players: "He actually gets angrier with each one, so every time we lose it's the worst I've seen him, he's a winner, though, that's what you expect, and that's the attitude that's needed at Chelsea these days."<ref>{{cite news|url=https://www.theguardian.com/football/2005/feb/27/newsstory.carlingcup0405 |title=Cole burns with blue ambition |work=The Guardian |location=London |date=27 February 2005 |accessdate=6 June 2015 |first=Paul |last=Wilson }}</ref>

Chelsea goalkeeper [[Carlo Cudicini]], who had played in four matches during the competition was suspended after being shown a red card in a [[FA Cup]] match against [[Newcastle United F.C.|Newcastle United]]. He would be replaced by [[Petr Čech]].<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/teams/c/chelsea/4284059.stm |title=Cudicini misses Carling Cup final |publisher=BBC Sport |date=22 February 2005 |accessdate=6 June 2015 }}</ref> Captain Steven Gerrard and striker [[Fernando Morientes]] were expected to return to the starting line-up for Liverpool, while midfielder [[Xabi Alonso]] would miss out after breaking his leg in the last fixture between the two sides. Going into the match Chelsea were the favourites with the bookmakers at [[Odds#Even odds|Even odds]] to win the match, while Liverpool were 9-4.<ref>{{cite news|url=https://www.theguardian.com/football/2005/feb/27/newsstory.carlingcup04051 |title=Baros finally has chance to settle old score |work=The Guardian |location=London |date=27 February 2005 |accessdate=6 June 2015 |first=Amy |last=Lawrence }}</ref> However, a poll on the [[BBC Sport]] website had 63% of fans backing Liverpool to win.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4277763.stm |title=Tactical masters fight for glory |publisher=BBC Sport |date=25 February 2005 |accessdate=6 June 2015 |first=Phil |last=McNulty }}</ref>

===First half===
Within a minute of the kick-off, Liverpool had scored. Striker Fernando Morientes ran over to the right-hand side of the pitch, his cross to the edge of the Chelsea penalty area was met by midfielder [[John Arne Riise]], who volleyed the ball into the Chelsea goal to give Liverpool a 1–0 lead. {{As of|2015}} The goal was the fastest scored in a League Cup final.<ref>{{cite web |title=Liverpool 2–3 Chelsea |url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4279679.stm |publisher=BBC Sport |date=27 February 2005 |accessdate=16 February 2014}}</ref> Liverpool continued to attack and two minutes later Riise had another chance, however his shot hit Chelsea defender [[Paulo Ferreira]].<ref name=guardian>{{cite news|url=https://www.theguardian.com/football/2005/feb/27/minutebyminute.carlingcup0405 |title=Chelsea 3 Liverpool 2 |work=The Guardian |location=London |date=27 February 2005 |accessdate=6 June 2015 |first=Paul |last=Doyle }}</ref> Liverpool defender [[Sami Hyypiä]] received the first yellow card of the match in the 13th minute for a challenge on [[Didier Drogba]].<ref name=bbc>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4302317.stm |title=Carling Cup clockwatch |publisher=BBC Sport |date=27 February 2005 |accessdate=6 June 2015 }}</ref> Chelsea started to come into the game and they were awarded a [[Direct free kick|free-kick]] in the 19th minute when Liverpool defender [[Steve Finnan]] was penalised for a foul on Damien Duff. However, the free kick from Ferreira was cleared by Liverpool. A run by Finnan in the 27th minute saw him go past Duff and defender [[William Gallas]] before he passed to [[Luis García Sanz|Luis García]], but his shot was blocked by [[Ricardo Carvalho]]. 
[[File:Didier Drogba'14.JPG|thumb|right|[[Didier Drogba]], who scored Chelsea's second goal in the match.]]
A minute later Chelsea midfielder Frank Lampard was shown a yellow card for a foul on García. The resulting free-kick found Steven Gerrard, but he fell to the ground after a challenge from [[Claude Makélélé]]. The referee [[Steve Bennett (referee)|Steve Bennett]] ignored Liverpool claims for a penalty and Chelsea went straight on the attack, but an effort by Drogba went wide of the Liverpool goal.<ref name=bbc /> Chelsea continued to attack in an effort to score an equalising goal, but attempts by Duff and [[Joe Cole]] failed to find the target.<ref name=guardian /> Liverpool defender [[Djimi Traoré]] was the third player to be shown a yellow card for a foul on Ferreira in the 35th minute. However, minutes later he made an important tackle as Duff beat the Liverpool [[offside trap]], but a poor touch outside of the Liverpool penalty area allowed Traoré to dispossess him.<ref name=bbc /> As half-time approached Liverpool were content to remain in their own half and allow Chelsea to attack them, but any attack was dealt with by the Liverpool defence.<ref name=guardian />

===Second half===
Chelsea replaced midfielder [[Jiří Jarošík]] with striker [[Eiður Guðjohnsen]] at the start of the second half, and started the second half in the same manner that they finished the first, with Liverpool withholding the pressure on their penalty area.<ref name=bbc /> Cole passed the ball over the Liverpool defence to Drogba in the 54th minute, but defender [[Jamie Carragher]] was able to get back and clear the ball. The ball was played back into the penalty area and met by Guðjohnsen, whose header was saved by Dudek, the ball fell to Gallas and Dudek again saved the subsequent shot.<ref name=guardian /> Midfielder [[Antonio Núñez Tena|Antonio Núñez]] replaced [[Harry Kewell]] in the 56th minute. Against the run of play, Liverpool had an attack in the 64th minute. A counter-attack results in a [[Dietmar Hamann]] shot, but it was saved by Čech. Liverpool made another substitution following the attack as [[Igor Bišćan]] replaced an injured [[Djimi Traoré]].<ref name=bbc /> Chelsea continued to push forward and were close to a goal in the 71st minute, but Riise cleared a Gallas after the ball was played to him by Makélélé. Both teams made substitutions in the following minutes. Striker [[Milan Baroš]] replaced Morientes for Liverpool while Chelsea substituted Gallas for striker [[Mateja Kežman]].<ref name=guardian />

Liverpool had a chance to extend their lead in the 74th minute, but Gerrard was unable to score from a cross by Núñez, with his shot going wide of the Chelsea goal.<ref>{{cite news|url=http://edition.cnn.com/2005/SPORT/football/02/27/carling.chelsea/ |title=Chelsea claim English League Cup |work=CNN |date=28 February 2005 |accessdate=6 June 2015 }}</ref> Liverpool pressed forward and a sliding tackle from Chelsea captain [[John Terry]] prevented Baroš from shooting in the Chelsea penalty area. Chelsea were awarded a free-kick in the 79th minute which was taken by Ferreira, the ball was met by Gerrard who intended to head it clear, but scored an [[own goal]] after heading the ball into the Liverpool goal. The score was now 1–1.<ref name=guardian /> Chelsea made their final substitution following the goal, with [[Glen Johnson (English footballer)|Glen Johnson]] replacing Cole. Chelsea manager Mourinho was sent off following the goal for making a gesture towards the Liverpool fans.<ref>{{cite news|url=http://www.independent.co.uk/sport/football/premier-league/mourinhos-dismissal-mars-his-first-chelsea-triumph-13221.html |title=Mourinho's dismissal mars his first Chelsea triumph |work=The Independent |location=London |date=28 February 2005 |accessdate=7 June 2015 |first=Sam |last=Wallace }}</ref> Chelsea nearly took the lead in the 82nd minute, but Duff's shot was saved by Dudek. Chelsea had a few chances to score in the following exchanges, but all the efforts missed the Liverpool goal. Liverpool's only chance in the rest of the half fell to Baroš, but he also put his shot wide. With the scores at 1–1, following the end of 90 minutes the match went to extra time.<ref name=guardian />

===Extra time===
Chelsea had the first chance in extra time when Duff's cross was met by Drogba, but his header hit the post. The pace of the game had slowed in extra time, neither side dominated the match.<ref name=bbc /> The best chances for both sides came in the final minutes of the first half of extra time. A García cross was met by Bišćan, but he headed the ball over the crossbar while Kežman had a shot saved by Dudek.<ref name=bbc /> However, in the first minute of the second half of extra time Chelsea had scored. A long throw-in to the near post of the Liverpool goal was missed by Terry and Hyypiä, but Drogba beat Carragher to the ball  and gave Chelsea a 2–1 lead. Four minutes later, Chelsea had extended their lead to 3–1. A Lampard free-kick was punched clear by Dudek, but the ball fell to Guðjohnsen who played the ball across the Liverpool goal to Kežman who scored.<ref name=guardian /> Two minutes later Liverpool scored. A Gerrard free-kick was headed in by Núñez to make the score 3–2. However, Liverpool were unable to find an equalising goal in the remaining minutes and Chelsea won 3–2 to win the League Cup for the third time.<ref name=bbc />

===Details===
{{football box
|date=27 February 2005
|time=15:00
|team1=[[Liverpool F.C.|Liverpool]]
|score=2–3
|aet=yes
|team2=[[Chelsea F.C.|Chelsea]]
|report=[http://news.bbc.co.uk/sport1/hi/football/league_cup/4279679.stm Report]
|goals1=[[John Arne Riise|Riise]] {{goal|1}}<br />[[Antonio Núñez Tena|Núñez]] {{goal|113}}
|goals2=[[Steven Gerrard|Gerrard]] {{goal|79|[[Own goal|o.g.]]}}<br />[[Didier Drogba|Drogba]] {{goal|107}}<br />[[Mateja Kežman|Kežman]] {{goal|112}}
|stadium=[[Millennium Stadium]], Cardiff
|attendance=78,000
|referee=[[Steve Bennett (referee)|Steve Bennett]] ([[Kent County Football Association|Kent]]) }}

{| width=92% |
|-
|{{Football kit
| pattern_la =
| pattern_b  =_liverpool0405h
| pattern_ra =
| leftarm    = FF0000
| body       = FF0000
| rightarm   = FF0000
| shorts     = FF0000
| socks      = FF0000
| title      = Liverpool
}}
|{{Football kit
| pattern_b   =_CFC 03 05 Blue
| pattern_sh  =_CFC 03 05 blue
| pattern_la  =_chelsea03 05 Blue
| pattern_ra  =_chelsea03 05 Blue
| pattern_so  =_bluestripe
| leftarm1    = 0000FF
| rightarm1   = 0000FF
| shorts1     = 0000FF
| socks1      = FFFFFF
| title       = Chelsea
}}
|}

{| style="width:100%;"
|-
|  style="vertical-align:top; width:50%;"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|-
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' ||{{flagicon|POL}} [[Jerzy Dudek]]
|-
|RB ||'''3''' ||{{flagicon|IRL}} [[Steve Finnan]]
|-
|CB ||'''23'''||{{flagicon|ENG}} [[Jamie Carragher]] || {{yel|117}}
|-
|CB ||'''4''' ||{{flagicon|FIN}} [[Sami Hyypiä]] || {{yel|13}}
|-
|LB ||'''26'''||{{flagicon|MLI}} [[Djimi Traoré]] || {{yel|35}} || {{suboff|67}}
|-
|RM ||'''10'''||{{flagicon|ESP}} [[Luis García Sanz|Luis García]]
|-
|CM ||'''8''' ||{{flagicon|ENG}} [[Steven Gerrard]] ([[Captain (association football)|c]])
|-
|CM ||'''16'''||{{flagicon|GER}} [[Dietmar Hamann]] || {{yel|79}}
|-
|LM ||'''6''' ||{{flagicon|NOR}} [[John Arne Riise]]
|-
|SS ||'''7''' ||{{flagicon|AUS}} [[Harry Kewell]] || || {{suboff|56}}
|-
|CF ||'''19'''||{{flagicon|ESP}} [[Fernando Morientes]] || || {{suboff|74}}
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||'''20'''||{{flagicon|ENG}} [[Scott Carson]]
|-
|DF ||'''12'''||{{flagicon|ARG}} [[Mauricio Pellegrino]]
|-
|DF ||'''25'''||{{flagicon|CRO}} [[Igor Bišćan]] || || {{subon|67}}
|-
|MF ||'''18'''||{{flagicon|ESP}} [[Antonio Núñez Tena|Antonio Núñez]] || || {{subon|56}}
|-
|FW ||'''5''' ||{{flagicon|CZE}} [[Milan Baroš]] || || {{subon|74}}
|-
|colspan=3|'''Manager:'''
|-
|colspan="4"|{{flagicon|ESP}} [[Rafael Benítez]]
|}
|valign="top"|[[File:Liverpool vs Chelsea 2005-02-27.svg|300px]]
|  style="vertical-align:top; width:50%;"|
{| cellspacing="0" cellpadding="0" style="font-size:90%; margin:auto;"
|-
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' ||{{flagicon|CZE}} [[Petr Čech]]
|-
|RB ||'''20'''||{{flagicon|POR}} [[Paulo Ferreira]]
|-
|CB ||'''6''' ||{{flagicon|POR}} [[Ricardo Carvalho]]
|-
|CB ||'''26'''||{{flagicon|ENG}} [[John Terry]] ([[Captain (association football)|c]])
|-
|LB ||'''13'''||{{flagicon|FRA}} [[William Gallas]] || || {{suboff|74}}
|-
|DM ||'''4''' ||{{flagicon|FRA}} [[Claude Makélélé]]
|-
|CM ||'''27'''||{{flagicon|CZE}} [[Jiří Jarošík]] || || {{suboff|45}}
|-
|CM ||'''8''' ||{{flagicon|ENG}} [[Frank Lampard]] || {{yel|27}}
|-
|RW ||'''10'''||{{flagicon|ENG}} [[Joe Cole]] || || {{suboff|81}}
|-
|LW ||'''11'''||{{flagicon|IRL}} [[Damien Duff]] || {{yel|114}}
|-
|CF ||'''15'''||{{flagicon|CIV}} [[Didier Drogba]] || {{yel|108}}
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||'''40'''||{{flagicon|ENG}} [[Lenny Pidgeley]]
|-
|DF ||'''2''' ||{{flagicon|ENG}} [[Glen Johnson (English footballer)|Glen Johnson]] || || {{subon|81}}
|-
|MF ||'''30'''||{{flagicon|POR}} [[Tiago Mendes|Tiago]]
|-
|FW ||'''22'''||{{flagicon|ISL}} [[Eiður Guðjohnsen]] || || {{subon|45}}
|-
|FW ||'''9''' ||{{flagicon|SCG}} [[Mateja Kežman]] || {{yel|81}} || {{subon|74}}
|-
|colspan=3|'''Manager:'''
|-
|colspan="4"|{{flagicon|POR}} [[José Mourinho]] || {{sent off|0|80}}
|}
|}

{|  style="width:100%; font-size:90%;"
|-
|  style="width:50%; vertical-align:top;"|

'''Man of the match'''
* [[John Terry]] (Chelsea)<ref name="Man of match">{{cite web |title=Alan Hardaker Trophy Winners |url=http://www.capitalonecup.co.uk/competition/the-alan-hardaker-award/ |publisher=The Football League |date=26 February 2012 |accessdate=8 May 2012}}</ref>
|  style="width:50%; vertical-align:top;"|
'''Match rules'''
* 90&nbsp;minutes.
* 30&nbsp;minutes of extra-time if necessary.
* Penalty shoot-out if scores still level.
* Five named substitutes.
* Maximum of three substitutions.
|}

==Post-match==
[[File:JoseMourinho.jpg|thumb|Chelsea manager [[José Mourinho]] defended the gesture which saw him dismissed in the final]]
Following the match, Chelsea manager José Mourinho claimed that the gesture he made had been intended for the media and not Liverpool fans: "The signal of close your mouth was not for them but for the press, they speak too much and in my opinion they try to do everything to disturb Chelsea. Wait, don't speak too soon. We lost two matches and in my opinion you (the media) try to take confidence from us and put pressure on us." Mourinho was happy that Chelsea had won but said the victory was not special: "It's just one more. I had a few before this, I'm very happy to win. It's important for the fans, for the club and especially for the players." Striker Eiður Guðjohnsen said Chelsea's victory was down to their hard work ethic: "It was a very intense and even game and it was hard to break them down, but hard work pays off in the end."<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4296791.stm |title=Mourinho blasts press after win |publisher=BBC Sport |date=27 February 2005 |accessdate=7 June 2015 }}</ref>

Liverpool captain Steven Gerrard was determined to move on from the final after scoring an own goal: "Losing cup finals and scoring an own goal is a bad day for myself, but I'll look forward to the next game now." He also said that Chelsea deserved the victory: "We scored early on and tried to see the clock out because we were 15 minutes away from lifting the cup, but credit to Chelsea they came back and deserved the win." Liverpool manager Rafael Benítez believed that Liverpool would have won the match had they scored when they were leading 1–0: "If you have clear chances at 1–0 and you score the second goal you finish the game, but we made some mistakes and at the end we conceded in our own goal." Despite the defeat he was proud of his players' efforts: "I say to the players we must be proud because we played a good game, Chelsea controlled the game when they started playing, but we re-organised the team with three midfielders and had some opportunities."<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/league_cup/4296747.stm |title=Dejected Gerrard accepts defeat |publisher=BBC Sport |date=27 February 2005 |accessdate=7 June 2015 }}</ref>

The two sides faced each other again in the semi-finals of the 2004–05 UEFA Champions League. A goalless first leg at Stamford Bridge was followed by a 1–0 victory for Liverpool in the second leg as they progressed to the final. A 3–2 victory in a penalty shootout against [[A.C. Milan|Milan]] after the [[2005 UEFA Champions League Final|match]] finished 3–3 ensured Liverpool won the [[UEFA Champions League]] for the fifth time.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/europe/4573159.stm |title=AC Milan 3–3 Liverpool (aet) |publisher=BBC Sport |date=25 May 2005 |accessdate=7 June 2015 }}</ref> Chelsea would go on to win the [[2004–05 FA Premier League]], which was their first League title since 1955.<ref>{{cite news|url=http://news.bbc.co.uk/sport1/hi/football/eng_prem/4476063.stm |title=Bolton 0–2 Chelsea |publisher=BBC Sport |date=30 April 2005 |accessdate=7 June 2015 }}</ref>

==References==
{{reflist|colwidth=30em}}

{{Football League Cup seasons}}
{{2004–05 in English football}}
{{Chelsea F.C. matches}}
{{Liverpool F.C. matches}}

[[Category:2004–05 Football League|Cup Final]]
[[Category:Chelsea F.C. matches|League Cup Final 2005]]
[[Category:Liverpool F.C. matches|League Cup Final 2005]]
[[Category:2004–05 in Welsh football|League Cup Final]]
[[Category:EFL Cup Finals]]
[[Category:21st century in Cardiff]]
[[Category:February 2005 sports events]]