<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=XF6B
 | image=Boeing XF6B-1.jpg
 | caption=The XF6B-1 in the early 1930s
}}{{Infobox Aircraft Type
 | type=carrier based fighter/bomber
 | national origin=United States
 | manufacturer=[[Boeing]]
 | designer=
 | first flight=1 February 1933<ref name="Angel">Angelucci, 1987. pp. 85-86.</ref>
 | introduced=
 | retired=
 | status=Cancelled
 | primary user=
 | number built=1
 | developed from= [[Boeing F4B]]
 | variants with their own articles=
}}
|}

The '''Boeing XF6B-1 / XBFB-1''' was Boeing's last [[biplane]] design for the [[United States Navy]]. Only the one prototype, ''Model 236'', was ever built; although first flying in early 1933, it rammed into a [[crash barrier]] in 1936 and the design was not pursued further.

==Design and development==
Ordered by the U.S. Navy on 30 June 1931, the fighter aircraft was a derivative of the [[Boeing F4B]]; it was almost entirely of metal construction, with only the wings still fabric-covered. The aircraft was powered by a 625&nbsp;hp [[Pratt & Whitney R-1535]]-44 Twin Wasp engine.<ref name="Angel"/>

The intended role of this design turned out to be uncertain. While its rugged construction was capable of withstanding high ''g''-forces, it weighed in at 3,704 pounds (700 pounds more than the F4B), and did not have the maneuverability needed in a fighter aircraft. It was, however, suitable as a [[fighter-bomber]], and in March 1934 the prototype was redesignated '''XBFB-1''' in recognition of its qualities. Even so, various ideas were tried to improve its fighter qualifications, such as an improved engine [[cowling]], streamlining around the landing gear, and even a three-bladed propeller (two-bladed props being standard).<ref name="Angel"/>

==Operational history==
Performance of the Boeing XF6B remained unsatisfactory with the U.S. Navy instead opting for the [[Curtiss F11C Goshawk]].<ref name="Angel"/>

==Operators==
;{{flag|United States|1912}}
*[[United States Navy]]

==Specifications==
{{aerospecs
|ref=Angelucci, 1987. pp. 85-86.<ref name="Angel"/>
|met or eng?=eng
|crew=one
|capacity=
|length m=6.73
|length ft=22
|length in=1.5
|span m=8.68
|span ft=28
|span in=6
|height m=3.22
|height ft=10
|height in=7
|wing area sqm=23.41
|wing area sqft=252
|empty weight kg=1,281
|empty weight lb=2,823
|gross weight kg=1,680
|gross weight lb=3,704
|eng1 number=1
|eng1 type=[[Pratt & Whitney R-1535]]-44
|eng1 kw=466
|eng1 hp=625
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|max speed kmh=322
|max speed mph=200
|cruise speed kmh=274
|cruise speed mph=170
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=845
|range miles=525
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|climb rate ms=6.04
|climb rate ftmin=1190
|armament1=2x .30in machine guns
|armament2=500lb (227kg) bombs
|armament3=
|armament4=
|armament5=
|armament6=
}}

{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Boeing BFB}}

===Citations===
{{Reflist}}

===Bibliography===
{{Refbegin}}
* ''The Illustrated Encyclopedia of Aircraft''. London: Aerospace Publishing, 1965.
* Jones, Lloyd S. ''U.S. Naval Fighters''. Fallbrook California: Aero Publishers, 1977, pp.&nbsp;115–117. ISBN 0-8168-9254-7.
* Taylor, Michael J. H. ''Jane's Encyclopedia of Aviation''. London: Studio Editions, 1989. ISBN 0-517-69186-8.
* ''World Aircraft Information Files''. London: Bright Star Publishing, 1985.
{{Refend}}

{{Boeing military aircraft}}
{{Boeing model numbers}}
{{USN bomber aircraft}}
{{USN fighters}}

[[Category:Boeing aircraft|F6B]]
[[Category:United States fighter aircraft 1930–1939]]
[[Category:United States bomber aircraft 1930–1939|Boeing BFB-01]]
[[Category:Single-engined tractor aircraft]]
[[Category:Carrier-based aircraft]]
[[Category:Biplanes]]