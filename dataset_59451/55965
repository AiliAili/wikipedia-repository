{{Userspace draft|source=ArticleWizard|date=November 2016}} 
{{Infobox journal
| title = Bothalia - African Biodiversity & Conservation
| cover = File:Bothaliacover.gif
| editor = Michelle Hamer
| discipline = [[Botany]], [[Zoology]], [[Biodiversity]]
| former_names = Bothalia
| abbreviation =
| publisher = AOSIS
| country = South Africa
| frequency = Yearly
| history =  1922–present
| openaccess = Yes
| license = [[Creative Commons Attribution 4.0]]
| impact = 0.486
| impact-year = 2015
| website = http://www.abcjournal.org/index.php/ABC
| link2 = http://www.abcjournal.org/index.php/abc/issue/archive
| link2-name = Online archive
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 0006-8241
| eISSN = 2311-9284
}}
'''''Bothalia - African Biodiversity & Conservation''''' is a [[South African]] [[peer-reviewed]] [[open access]] [[scientific journal]] covering the fields of [[botany]], [[zoology]] and [[biodiversity]], produced by the [[South African National Biodiversity Institute]].{{sfn|SANBI|2016}} According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.486.<ref name=WoS>{{cite book  |year=2016 |chapter=Bothalia - African Biodiversity & Conservation |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of  Science]]}}</ref>

== Description ==
The journal is produced yearly, but articles are published on-line continually. When the journal was renamed in 2014, as well as broadening the scope it was made open access and its contents made freely available under the [[Creative Commons Attribution 4.0]] International (CC BY 4.0) license. The entire contents back to the first issue are available on the journal's website.{{sfn|Bothalia|2016}}

==History==
The journal was established in 1921, producing its first issue in 1922, as an in house journal of South Africa's National Botanical Institute. The journal was formally known by the name ''Bothalia'' alone, from 1922 to 2014 when the title was expanded to better reflect the role of the South African National Biodiversity Institute.<ref name=name/>

In 2004 the National Botanical Institute was absorbed into a broader configuration, the South African National Biodiversity Institute, by the National Environmental Management: Biodiversity Act. The journal was named after the first [[Prime Minister of South Africa]] [[Louis Botha]] who was Minister of Agriculture from 1910 to 1913.{{sfn|Bothalia|2016}}

After the new journal was launched a scam website (bthla-journal.org) appeared purporting to be the journal.{{sfn|Bothalia|2016}}

==References==
{{Reflist|refs=
<ref name=name>{{harvnb|SANBI|2016|loc=[http://www.sanbi.org/news/journal-bothalia-changes-african-biodiversity-conservation The journal Bothalia changes to African Biodiversity & Conservation Feb 2014]}}</ref>
}}

==Bibliography==
{{refbegin}}
*{{cite web|last1=Bothalia|title=Bothalia - African Conservation and Biodiversity|url=http://www.abcjournal.org/index.php/ABC/index|publisher=AOSIS|accessdate=28 November 2016|date=2016|format=Official website|ref=harv}}
*{{cite web|title=South African National Biodiversity Institute (SANBI)|url=http://www.sanbi.org/|accessdate=16 November 2016|date=2016|ref={{harvid|SANBI|2016}}}}
{{refend}}

[[File:Bothaliacover2013.jpg|thumb|left|2013 cover of ''Bothalia'']]

{{commons}}
{{authority control}}

{{DEFAULTSORT:Bothalia - African Biodiversity and Conservation}}
[[Category:Articles created via the Article Wizard]]
[[Category:Botany journals]]
[[Category:Biodiversity]]