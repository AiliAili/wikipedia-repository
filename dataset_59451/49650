{{Infobox book  <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
 | name          = Family Happiness
 | title_orig    = Семейное счастье (Semeynoye Schast'ye)
 | image         = Familys happiness novel by Leo Tolstoy.jpg
 | image_size = 250px
 | caption  = ''The Russian Messenger'', № 7–8 April 1859
 | translator   = April Fitz Lyon (1953) 
 | cover_artist   = 
 | author         = [[Leo Tolstoy]]
 | illustrator   =  
 | country        = Russia
 | language       = Russian
 | series         = 
 | genre          = Fiction
 | subject        =  
 | publisher      = 
 | release_date  = 1859
 | english_release_date = 
 | media_type     =  
 | pages          = 214 p. (Hardcover)
 | isbn           = 
 | preceded_by    = 
 | followed_by    = 
}}

'''''Family Happiness''''' ({{lang-ru|Семейное счастье}} [''Semeynoye Schast'ye'']) is an 1859 [[novella]] written by [[Leo Tolstoy]], first published in ''[[The Russian Messenger]]''. 

==Plot==
The story concerns the love and marriage of a young girl, Mashechka (17 years old), and the much older Sergey Mikhaylych (36), an old family friend. The story is narrated by Masha. After a courtship that has the trappings of a mere family friendship, Masha's love grows and expands until she can no longer contain it. She reveals it to Sergey Mikhaylych and discovers that he also is deeply in love. If he has resisted her it was because of his fear that the age difference between them would lead the very young Masha to tire of him. He likes to be still and quiet, he tells her, while she will want to explore and discover more and more about life. Ecstatically and passionately happy, the pair immediately engages to be married. Once married they move to Mikhaylych's home. They are both members of the landed Russian upper class. Masha soon feels impatient with the quiet order of life on the estate, notwithstanding the powerful understanding and love that remains between the two. To assuage her anxiety, they decide to spend a few weeks in St. Petersburg.  Sergey Mikhaylych agrees to take Masha to an aristocratic ball. He hates "society" but she is enchanted with it. They go again, and then again. She becomes a regular, the darling of the countesses and princes, with her rural charm and her beauty. Sergey Mikhaylych, at first very pleased with Petersburg society's enthusiasm for his wife, frowns on her  passion for "society"; but he does not try to influence Masha. Out of respect for her, Sergey Mikhaylych will scrupulously allow his young wife to discover the truth about the emptiness and ugliness of "society" on her own. But his trust in her is damaged as he watches how dazzled she is by this world. Finally they confront each other about their differences. They argue but do not treat their conflict as something that can be resolved through negotiation. Both are shocked and mortified that their intense love has suddenly been called into question. Something has changed. Because of pride, they both refuse to talk about it. The trust and the closeness are gone. Only courteous friendship remains. Masha yearns to return to the passionate closeness they had known before Petersburg. They go back to the country. Though she gives birth to children and the couple has a good life, she despairs. They can barely be together by themselves. Finally she asks him to explain why he did not try to guide and direct her away from the balls and the parties in Petersburg. Why did they lose their intense love? Why don't they try to bring it back? His answer is not the answer she wants to hear, but it settles her down and prepares her for a long life of comfortable "Family Happiness". 

==In popular culture==

A passage of the book is quoted in the book and film [[Into the Wild (film)|''Into the Wild'']]:

"I have lived through much, and now I think I have found what is needed for happiness.  A quiet secluded life in the country, with the possibility of being useful to people to whom it is easy to do good, and who are not accustomed to have it done to them; then work which one hopes may be of some use; then rest, nature, books, music, love for one's neighbor—such is my idea of happiness.  And then, on top of all that, you for a mate, and children perhaps—what more can the heart of man desire?"

A passage is also quoted in the book [[Into the Wild (book)|''Into the Wild'']]:

{{cquote|I wanted movement and not a calm course of existence. I wanted excitement and danger and the chance to sacrifice myself for my love. I felt in myself a superabundance of energy which found no outlet in our quiet life.” 
which had been found highlighted in the book ''Family Happiness'' among Chris McCandless's remains.}}

The last page of the story is also quoted in full in the [[Philip Roth]] novel ''[[The Counterlife]]''.

[[The Mountain Goats]] song "Family Happiness" takes its name from the novella and includes the line "Started quoting Tolstoy into the [[Cassette deck|machine]]/I had no idea what you meant".

==References==

[http://fomenko.theatre.ru/performance/family/ Theater Atelier Piotr Fomenko] in Moscow adapted the novella to the stage. The play premiered in September 2000 and remains part of the theater's repertoire.

==External links==
{{wikisource|Family Happiness}}
*[http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0039.shtml/ Full text of ''Семейное счастье'' in the original Russian]

{{Leo Tolstoy}}

{{Authority control}}
[[Category:1859 novels]]
[[Category:Novels by Leo Tolstoy]]
[[Category:Russian novellas]]


{{1850s-novel-stub}}