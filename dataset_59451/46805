'''Alexander Calandra''', PhD (January 12, 1911 - March 8, 2006) was a scientist,<ref name="Brunvand2012">{{cite book|author=Jan Harold Brunvand|authorlink=Jan Harold Brunvand|title=Encyclopedia of Urban Legends, Updated and Expanded Edition: A-L|url=https://books.google.com/books?id=9xOb-19lXx8C&pg=PA52|year=2012|publisher=ABC-CLIO|isbn=978-1-59884-720-8|pages=52–}}</ref> educator, and author, perhaps best remembered for his short story, "Angels on a Pin (101 Ways to Use a Barometer)."

==Early life and education==
Calandra was born in Brooklyn, New York and received his bachelor's degree in Chemistry from [[Brooklyn College]] in 1935.  He taught there  while pursuing his PhD at [[NYU]], where he received an MA in 1938.<ref>{{cite news|url=http://query.nytimes.com/mem/archive/pdf?res=F70E11FE3E55157A93CAA9178DD85F4C8385F9|newspaper=New York Times|date=June 8, 1938|title=N. Y. U. to Hold Its 106th Commencement for 4,100 on Ohio Field This Morning}}</ref>  and a PhD in Chemistry  in 1940.<ref>{{cite news|title=N.Y.U. to Hold Its 108th Commencement Exercises Today on Campus in Bronx - Eight to Receive Honorary Degrees at New York University|url=http://query.nytimes.com/mem/archive/pdf?res=F70E11F9355D10728DDDAC0894DE405B8088F1D3|newspaper=New York Times|date=June 5, 1940}}</ref>

==Career==
Calandra was a visiting professor of Chemistry at the [[University of Chicago]] from 1945-1948, acting as assistant to [[Enrico Fermi]], at which time Calandra shifted the focus of his studies from chemistry to physics.  During this period, Fermi was working on the nuclear bomb. Fermi brought Calandra to the attention of the Nobel laureate physicist [[Arthur Holly Compton]], and when Compton moved to St Louis to become chancellor of Washington University in 1948, he invited Calandra there, to develop a program of science education for liberal arts students. Calandra advocated for "science as organized common sense."<ref name="scitation">{{cite journal|last=Bartlett|first=Albert|title=Millikan award for 1979|journal=Phys. Teach.|year=1979|volume=17|issue=498|doi=10.1119/1.2340340|pages=498}}</ref>

In 1969 Calandra joined the faculty of [[Webster College]] as chairman of the science department, where he worked to develop programs until 1980.<ref>{{cite book|title=Chicken soup for the College Soul}}</ref>   He also served on the Ministry of Education in Jamaica.<ref name="news.wustl">{{cite web|url=http://news.wustl.edu/news/Pages/6786.aspx|title=Obituary: Alexander Calandra, professor emeritus of physical science in physics in Arts & Sciences. Newsroom, Washington University in St. Louis}}</ref>   He also taught in the department of Physics at [[Washington University in Saint Louis]] from 1948 until   retirement in   1979.<ref name="news.wustl" />

Calandra served as a consultant to the school systems in St. Louis and other areas.<ref name="Sciences1955">{{cite book|author=National Research Council (U.S.). Division of Mathematical and Physical Sciences|title=Summary of Programs and Recommendations Relating to the Improvement of Teaching of Secondary School Science and Mathematics|url=https://books.google.com/books?id=cEErAAAAYAAJ&pg=PA38|year=1955|publisher=National Academies|pages=38–|id=NAP:16838}}</ref>  An article in the New York Times of January 25, 1964, ("Grade Schools Accused of Stressing Sensation and Ignoring Basic Facts, 'Sputnik Panic' Blamed: Professor of Physics Tells at Parley Here of Two Major Defects"), outlined Dr. Calandra's views, as presented to a symposium on elementary school science teaching at the annual meeting of the American Association of Physics Teachers.<ref>{{cite news|last=Terte|first=Robert|title=Modern Teaching of Science Scored|url=http://query.nytimes.com/mem/archive/pdf?res=F10C1EFD3E5415738DDDAC0A94D9405B848AF1D3|newspaper=New York Times|date=January 25, 1964}}</ref>  He was an educational consultant for several institutions and foundations, including the Ford Foundation, the Thomas Alva Edison Foundation, and the National Science Foundation.<ref name="news.wustl" /> He also consulted for the Educational Testing Service (then called the Cooperative Test Service).<ref>{{cite journal|doi=10.1021/ed017p296|title=New England Association of Chemistry Teachers summer conference program|year=1940|last1=Weaver|first1=Elbert C.|journal=Journal of Chemical Education|volume=17|issue=6|pages=296}}</ref>

Calandra died March 8, 2006.<ref name="Kelly">{{cite book|author=Carla Kelly|title=Stop Me If You've Read This One: Prairie Lite|url=https://books.google.com/books?id=2g47NoUYfT0C&pg=PT116|publisher=Cedar Fort|isbn=978-1-4621-0457-4|pages=116–}}</ref>

==Angels on the Head of a Pin==
Calandra was an advocate of non-traditional teaching and learning methods.<ref>{{cite book|title=Newsweek|url=https://books.google.com/books?id=GvIeAQAAMAAJ|date=April 1965|publisher=Newsweek, Incorporated}}</ref> His 'Barometer Story', which came to be known as "Angels on the Head of a Pin", continues to be widely read and discussed.<ref name="KentLancour1997">{{cite book|author1=[[Allen Kent]]|author2=Harold Lancour|title=Encyclopedia of Library and Information Science: Volume 61 - Supplement 24|url=https://books.google.com/books?id=qgKWdpZqwhUC&pg=PA298|date=16 September 1997|publisher=CRC Press|isbn=978-0-8247-2061-2|pages=298–}}</ref><ref name="Fernández-Galiano1970">{{cite book|author=Dimas Fernández-Galiano|title=Ciencias Naturales|url=https://books.google.com/books?id=1MmHMWhvjD8C&pg=PA32|year=1970|publisher=Noveduc Libros|pages=32–|id=GGKEY:CQJG554ZBD3}}</ref>  It first appeared in 1961 in Calandra's book, "The Teaching of Elementary Science and Mathematics" Washington University Press, St. Louis, 1961.  It was popularized through its appearance in 'The Saturday Review" (Dec. 21, 1968, p.&nbsp;60), with the title "Angels on the Head of a Pin. A Modern Parable".  The story has since been published (in both legal and illegal formats) hundreds of times.  An article written by John A. Osmundsen in the New York Times, entitled "Science Teacher Chides Teachers: Tale of a Student Outlines an 'Unscientific Method." and including a photo of Calandra, appeared on March 8, 1964. Osmundsen discussed the story as an attack on closed-minded teaching methods, advocating encouragement of nontraditional problem-solving techniques.<ref>{{cite news|last=Osmundson|first=John|url=http://query.nytimes.com/mem/archive/pdf?res=F10C16FA395415738DDDA10894DB405B848AF1D3|newspaper=New York Times|date=March 8, 1964|title=Science Teacher Chides Teachers: Tale of a Student Outlines an 'Unscientific Method'}}</ref><ref name="Sanders2011">{{cite book|author=Roy E. Sanders|title=Chemical Process Safety: Learning from Case Histories|url=https://books.google.com/books?id=QCwOmZJfUesC&pg=PA196|date=30 August 2011|publisher=Gulf Professional Publishing|isbn=978-0-08-047648-3|pages=196–}}</ref>

==Honors==
In 1979 Calandra received the Robert A. Millikan award for excellence in the teaching of Physics.  A biography by Albert A Bartlett (University of Colorado) was written at the time.,<ref name="scitation" /> where he describes Calandra as having "devoted a lifetime to the challenging task of imparting an interest in and an understanding of science to students of all ages."

His correspondence with Richard Feynman (dating 1968-1979) is at Caltech, in the Feynman Papers.

==Publications==
*"The College Chemistry Testing Program 1941-42", J. Chem. Educ., 1943, 20 (3), p.&nbsp;141.
*"Teaching Signed Numbers in Grade 9", The Arithmetic Teacher, Vol. 5, No. 5 (November 1958), pp.&nbsp;259–260
*"Integrated Science and Mathematics in Junior High School", The American Mathematical Monthly, Vo. 69, No. 1 (Jan., 1962), pp.&nbsp;55–56
*"The Role of Mathematics in an Integrated 9th Grade Science-Mathematics Course", The American Mathematical Monthly, Vol. 66, No. 5 (May, 1959), pp.&nbsp;415–417
*"Experiment in the Teaching of Russian in the Elementary School", (with Charles J. McClain) *The Modern Language Journal, Vol. 43, No. 4 (Apr., 1959), pp.&nbsp;183–184
*"The Art of Teaching Physics" 1979
*"What is Physics?" American Journal of Physics 38: 126, 1970.

Reviews: Robert Kalin, "Symbols by Alexander Calandra" The Arithmetic Teacher, Vol. 9, No. 6 (October 1962), pp.&nbsp;346–347, 354

==References==

{{reflist}}

==External links==
*[http://content.cdlib.org/view?docId=kt5n39p6k0&doc.view=entire_text&brand=default Correspondence between Richard Feynman and Alexander Calandra at Caltech].
*[http://www.rbs0.com/baromete.htm Angels on the Head of a Pin]

{{Authority control}}

{{DEFAULTSORT:Calandra, Alexander}}
[[Category:1911 births]]
[[Category:2006 deaths]]
[[Category:American education writers]]
[[Category:Brooklyn College alumni]]
[[Category:New York University alumni]]
[[Category:Washington University in St. Louis faculty]]