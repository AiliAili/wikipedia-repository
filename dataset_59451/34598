{{For|the 2015 television adaptation|12 Monkeys (TV series)}}
{{Use mdy dates|date=June 2015}}
{{Infobox film
| name           = 12 Monkeys
| image          = Twelve monkeysmp.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = [[Terry Gilliam]]
| producer       = [[Charles Roven]]
| screenplay     = {{Plainlist|
* [[David Peoples]]
* [[Janet Peoples]]}}
| based on       = {{Based on|''[[La Jetée]]''|[[Chris Marker]]}}
| starring       = {{Plainlist|
* [[Bruce Willis]]
* [[Madeleine Stowe]]
* [[Brad Pitt]]
* [[Christopher Plummer]]}}
| music          = [[Paul Buckmaster]]
| cinematography = [[Roger Pratt (cinematographer)|Roger Pratt]]
| editing        = [[Mick Audsley]]
| studio         = {{Plainlist|
* [[Atlas Entertainment]]
* Classico}}
| distributor    = [[Universal Studios|Universal Pictures]]
| released       = {{Film date|1995|12|29}}
| runtime        = 129 minutes<!--Theatrical runtime: 129:18--><ref>{{cite web | url=http://bbfc.co.uk/releases/twelve-monkeys-1970-6 | title=''TWELVE MONKEYS'' (15) | work=[[British Board of Film Classification]] | date=February 1, 1996 | accessdate=June 3, 2015}}</ref>
| country        = United States
| language       = English
| budget         = $29.5 million
| gross          = $168.8 million<ref name="mojo"/>
}}

'''''12 Monkeys''''', also known as '''''Twelve Monkeys''''', is a 1995 American [[neo-noir]] [[science fiction film]] directed by [[Terry Gilliam]], inspired by [[Chris Marker]]'s 1962 [[short film]] ''[[La Jetée]]'', and starring [[Bruce Willis]], [[Madeleine Stowe]], and [[Brad Pitt]], with [[Christopher Plummer]] and [[David Morse (actor)|David Morse]] in supporting roles. After [[Universal Studios]] acquired the rights to remake ''La Jetée'' as a full-length film, [[David Peoples|David]] and [[Janet Peoples]] were hired to write the script.

Under Gilliam's direction, Universal granted the filmmakers a $29.5 million budget, and filming lasted from February to May 1995. The film was shot mostly in [[Philadelphia]] and [[Baltimore]], where the story was set.

The film was released to critical praise and grossed $168 million worldwide. Pitt was nominated for the [[Academy Award for Best Supporting Actor]], and won a [[Golden Globe Award for Best Supporting Actor – Motion Picture|Golden Globe]] for his performance. The film also won and was nominated for various categories at the [[Saturn Award]]s.

==Plot==
A deadly virus released in 1996 wipes out almost all of humanity, forcing remaining survivors to live underground. A group known as the Army of the Twelve Monkeys is believed to be behind the release of the virus. In 2035, James Cole (Willis) is a prisoner living in a subterranean compound beneath the ruins of [[Philadelphia]]. Cole is selected for a mission, where he is trained and sent back in time to locate the original virus in order to help scientists develop a cure.<ref>{{citation|quote='''James Cole''': I just have to locate them because they have the virus in its pure form, before it mutates. When I locate them, they'll send a scientist back here; that scientist will study the virus, and then when he goes back to the present, he and the rest of the scientists will make a cure.|title=12 Monkeys}}</ref> Meanwhile, Cole is troubled by recurring dreams involving a foot chase and an airport shooting.

Cole arrives in [[Baltimore]] in 1990, not 1996 as planned. He is arrested, then hospitalized in a mental hospital on the diagnosis of Dr. Kathryn Railly (Stowe). There he encounters Jeffrey Goines (Pitt), a mental patient with fanatical views. After an escape attempt, Cole is sedated and locked in a cell, but he disappears moments later, and wakes up back in his own time. Cole is interrogated by the scientists, who play a distorted voicemail message which asserts the association of the Army of the Twelve Monkeys with the virus. He is also shown photos of numerous people suspected of being involved, including Goines. The scientists offer Cole a second chance to complete his mission and send him back in time. He arrives at a battlefield of World War I where he is shot in the leg, and then he is suddenly transported to 1996.

In 1996, Railly gives a lecture about the [[Cassandra metaphor|Cassandra complex]] to a group of scientists. At the post-lecture book signing, Dr. Peters (Morse) points out to Railly that apocalypse alarmists represent the sane vision, while humanity's gradual destruction of the environment is the real lunacy. Cole arrives at the venue after seeing flyers publicizing it, and when Railly departs, he kidnaps her and forces her to take him to Philadelphia. They learn that Goines is the founder of the Army of the Twelve Monkeys, and set out in search of him. When they confront him, however, Goines denies any involvement with the group and says that in 1990 Cole originated the idea of wiping out humanity with a virus stolen from Goines' virologist father (Plummer).

Cole convinces himself that he is insane, but Railly confronts him with evidence of his time travel. They decide to spend their remaining time together in the [[Florida Keys]] before the onset of the plague. On their way to the airport, they learn that the Army of the Twelve Monkeys was not the source of the epidemic; the group's major act of protest is releasing animals from a zoo and placing Goines' father in an animal cage.

At the airport, Cole leaves a last message telling the scientists that in following the Army of the Twelve Monkeys they are on the wrong track, and that he will not return. He is soon confronted by Jose (Seda), an acquaintance from his own time, who gives Cole a handgun and ambiguously instructs him to follow orders. At the same time, Railly spots Dr. Peters, and recognizes him from a newspaper photograph as an assistant at Goines' father's virology lab. Peters is about to embark on a tour of several cities that match the locations and sequence of the viral outbreaks.

Cole forces his way through a security checkpoint in pursuit of Peters. After drawing the gun he was given, Cole is fatally shot by police. As Cole lies dying in Railly's arms, she makes eye contact with a small boy—the young James Cole witnessing the scene of his own death, which will replay in his dreams for years to come. Peters, aboard the plane with the virus, sits down next to Jones (Florence), one of the scientists from the future.

==Cast==
* [[Bruce Willis]] as James Cole
** Joseph Melito as young James Cole
* [[Madeleine Stowe]] as Kathryn Railly
* [[Brad Pitt]] as Jeffrey Goines
* [[Christopher Plummer]] as Dr. Goines
* [[David Morse (actor)|David Morse]] as Dr. Peters
* [[Jon Seda]] as Jose
* [[Christopher Meloni]] as Lt. Halperin
* [[Frank Gorshin]] as Dr. Fletcher
* [[Vernon Campbell]] as Tiny
* [[Lisa Gay Hamilton]] as Teddy
* [[Bob Adrian]] as Geologist
* [[Simon Jones (actor)|Simon Jones]] as Zoologist
* [[Carol Florence]] as Astrophysicist/Jones
* [[Bill Raymond]] as Microbiologist
* [[Annie Golden]] as Woman Cabbie
* [[Thomas Roy]] as a [[street preacher]]

==Production==

===Development===
The genesis of ''12 Monkeys'' came from [[executive producer]] Robert Kosberg, who had been a fan of the French short film ''[[La Jetée]]'' (1962). Kosberg persuaded the film's director, [[Chris Marker]], to let him [[Pitch (filmmaking)|pitch]] the project to [[Universal Studios|Universal Pictures]], seeing it as a perfect basis for a full-length science fiction film. Universal reluctantly agreed to purchase the [[film rights|remake rights]] and hired [[David Peoples|David]] and [[Janet Peoples]] to write the screenplay.<ref>{{cite news |author=Chris Nashawaty |url=http://www.ew.com/ew/article/0,,1219922_1,00.html |title=They Call Him Mr. Pitch |work=[[Entertainment Weekly]] |date=July 28, 2006 |deadurl=no |accessdate=2012-04-10}}</ref> Producer [[Charles Roven]] chose Terry Gilliam to direct, because he believed the filmmaker's style was perfect for ''12 Monkeys''{{'}} nonlinear storyline and time travel subplot.<ref name="note"/> Gilliam had just abandoned a [[film adaptation]] of ''[[A Tale of Two Cities]]'' when he signed to direct ''12 Monkeys''.<ref name="first">{{cite book |author=Ian Christie; [[Terry Gilliam]] |title=Gilliam on Gilliam |pages=220–225 |publisher=[[Faber and Faber]] |month = |year=1999 |isbn=0-571-20280-2 |location=London}}</ref> The film also represents the second film for which Gilliam did not write or co-write the screenplay. Although he prefers to direct his own scripts, he was captivated by Peoples' "intriguing and intelligent script. The story is disconcerting. It deals with time, madness and a perception of what the world is or isn't. It is a study of madness and dreams, of death and re-birth, set in a world coming apart."<ref name="note">DVD production notes</ref>

Universal took longer than expected to approve ''12 Monkeys'', although Gilliam had two stars (Willis and Pitt) and a firm budget of $29.5 million (low for a Hollywood science fiction film). Universal's production of ''[[Waterworld]]'' (1995) had resulted in various [[cost overrun]]s. To get ''12 Monkeys'' approved for production, Gilliam persuaded Willis to lower his normal [[asking price]].<ref name="second">Christie, Gilliam, pp.226–230</ref> Because of Universal's strict production incentives and his history with the studio on ''[[Brazil (1985 film)|Brazil]]'', Gilliam received [[final cut privilege]]. The [[Writers Guild of America]] was skeptical of the "inspired by" credit for ''La Jetée'' and Chris Marker.<ref name="comment"/>

===Casting===
Gilliam's initial casting choices were [[Nick Nolte]] as James Cole and [[Jeff Bridges]] as Jeffrey Goines, but Universal objected.<ref name="first"/> Gilliam, who first met Bruce Willis while casting Jeff Bridges' role in ''[[The Fisher King (film)|The Fisher King]]'' (1991), believed Willis evoked Cole's characterization as being "somebody who is strong and dangerous but also vulnerable."<ref name="note"/> The actor had a trio of tattoos drawn onto his scalp and neck each day when filming: one that indicated his prisoner number, and a pair of [[barcode]]s on each side of his neck.

Gilliam cast [[Madeleine Stowe]] as Dr. Kathryn Railly because he was impressed by her performance in ''[[Blink (film)|Blink]]'' (1994).<ref name="note"/> The director first met Stowe when he was casting his abandoned film adaptation of ''A Tale of Two Cities''.<ref name="first"/> "She has this incredible ethereal beauty and she's incredibly intelligent", Gilliam said of Stowe. "Those two things rest very easily with her, and the film needed those elements because it has to be romantic."<ref name="note"/>

Gilliam originally believed that Pitt was not right for the role of Jeffrey Goines, but the casting director convinced him otherwise.<ref name="first"/> Pitt was cast for a comparatively small salary, as he was still relatively unknown at the time. By the time of ''12 Monkeys''{{'}} release, however, ''[[Interview with the Vampire (film)|Interview with the Vampire]]'' (1994), ''[[Legends of the Fall]]'' (1994), and ''[[Seven (1995 film)|Se7en]]'' (1995) had been released, making Pitt an [[A-list]] actor, which drew greater attention to the film and boosted its box-office standing. In [[Philadelphia]], months before filming, Pitt spent weeks at [[Temple University]]'s hospital, visiting and studying the psychiatric ward to prepare for his role.<ref name="note"/>

===Filming===
[[Principal photography]] lasted from February 8 to May 6, 1995. Shooting on location in Philadelphia and Baltimore (including the [[Senator Theatre]])<ref name="main"/><ref>{{cite news |author=Jeff Gordinier |url=http://www.ew.com/ew/article/0,,297297,00.html |title=Brass Bald |work=[[Entertainment Weekly]] |date=May 19, 1995 |deadurl=no |accessdate=2012-04-10}}</ref> in winter was fraught with weather problems. There were also technical glitches with the futuristic mechanical props. Because the film has a nonlinear storyline, continuity errors occurred, and some scenes had to be reshot. Gilliam also injured himself when he went horseback riding. Despite setbacks, however, the director managed to stay within the budget and was only a week behind his [[shooting schedule]]. "It was a tough shoot", acknowledged Jeffrey Beecroft (''[[Mr. Brooks]]'', ''[[Dances with Wolves]]''), the [[production designer]]. "There wasn't a lot of money or enough time. Terry is a perfectionist, but he was really adamant about not going over budget. He got crucified for ''[[The Adventures of Baron Munchausen|Munchausen]]'', and that still haunts him."<ref name="main">{{cite news |url=https://www.nytimes.com/1995/12/24/movies/film-terry-gilliam-going-mainstream-sort-of.html?pagewanted=3 |author=Jill Gerston |title=Terry Gilliam: Going Mainstream (Sort Of) |work=[[The New York Times]] |date=December 24, 1995 |deadurl=no |accessdate=2012-04-10}}</ref>

The filmmakers were not allowed the luxury of [[sound stage]]s; thus, they had to find abandoned buildings or landmarks to use.<ref name="comment">[[Terry Gilliam]], [[Charles Roven]], DVD [[audio commentary]], 1998, [[Universal Home Video]].</ref> The exteriors of the climactic airport scene were shot at the [[Baltimore-Washington International Thurgood Marshall Airport|Baltimore-Washington International Airport]], while the interior scenes were shot at the [[Pennsylvania Convention Center]] (formerly, [[Reading Terminal]]). Filming at the psychiatric hospital was done at the [[Eastern State Penitentiary]].<ref name="third"/>

===Design===
Gilliam used the same filmmaking style as he had in ''Brazil'' (1985), including the [[art direction]] and [[cinematography]] (specifically using [[fresnel lens]]es).<ref name="second"/> The appearance of the interrogation room where Cole is being interviewed by the scientists was based on the work of [[Lebbeus Woods]]; these scenes were shot at three different [[power station]]s (two in Philadelphia and one in Baltimore). Gilliam intended to show Cole being interviewed through a multi-screen interrogation TV set because he felt the machinery evoked a "nightmarish intervention of technology. You try to see the faces on the screens in front of you, but the real faces and voices are down there and you have these tiny voices in your ear. To me that's the world we live in, the way we communicate these days, through technical devices that pretend to be about communication but may not be."<ref>{{cite news |url=http://www.smart.co.uk/dreams/tgmonkex.htm |author=Nick James |title=Time and the Machine |work=[[Sight and Sound]] |date=April 1996 |deadurl=no |accessdate=2012-04-10}}</ref>

The [[art department]] made sure that the 2035 underground world would only use pre-1996 technology as a means to depict the bleakness of the future. Gilliam, Beecroft, and Crispian Sallis (set decorator) went to several [[flea markets]] and salvage warehouses looking for materials to decorate the sets.<ref name="note"/> The majority of visual effects sequences were created by Peerless Camera, the London-based effects studio that Gilliam founded in the late 1970s with [[visual effects supervisor]] Kent Houston (''[[The Golden Compass (film)|The Golden Compass]]'', ''[[Casino Royale (2006 film)|Casino Royale]]''). Additional digital compositing was done by [[The Mill (post-production)|The Mill]], while [[Cinesite]] provided film scanning services.<ref name="note"/>

===Music===
The [[Film score|film's score]] was composed, arranged, and conducted by English musician [[Paul Buckmaster]]. The main theme is based on [[Argentine people|Argentine]] [[Tango music|tango]] musician/composer [[Ástor Piazzolla]]'s ''[[Suite Punta del Este]]''.<ref>{{cite web |url=http://www.piazzolla.org/works2/suite.html |title=Suite Punta del Este |work=Ástor Piazzolla |archiveurl=https://web.archive.org/web/20101007193223/http://piazzolla.org/works2/suite.html |archivedate=2010-10-07 |deadurl=yes |accessdate=2012-04-10}}</ref>

==Themes==

===Memory, time, and technology===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 95%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "Cole has been thrust from another world into ours and he's confronted by the confusion we live in, which most people somehow accept as normal. So he appears abnormal, and what's happening around him seems random and weird. Is he mad or are we?"
|-
| style="text-align: left;" | — Director Terry Gilliam<ref name="second"/>
|}
''12 Monkeys'' studies the subjective nature of memories and their effect upon perceptions of reality. Examples of false memories include Cole's recollection of the airport shooting, altered each time he has the dream, and a "mentally divergent" man at the asylum who has false memories.<ref name="comment2">[[Chris Marker|Terry Gilliam]], [[short film|Charles Roven]], DVD ''audio commentary'', 1998, [[La Jetée|Universal Home Video]].
</ref>

References to time, time travel, and monkeys are scattered throughout the film, including the [[Woody Woodpecker]] cartoon "Time Tunnel" playing on the TV in a hotel room, the [[Marx Brothers]] film ''[[Monkey Business (1931 film)|Monkey Business]]'' (1931) on TV in the asylum and the subplots of monkeys (drug testing, news stories and animal rights). The film is also a study of modern civilization's declining efforts to communicate with each other due to the interference of technology.<ref name="comment"/>

===Allusions to other films and media===
''12 Monkeys'' is inspired by the French short film ''La Jetée'' (1962); as in ''La Jetée'', characters are haunted by the image of their own death.<ref name="third"/> Like ''La Jetée'', ''12 Monkeys'' contains references to [[Alfred Hitchcock]]'s ''[[Vertigo (film)|Vertigo]]'' (1958). Toward the end of the film, Cole and Railly hide in a theater showing a 24-hour Hitchcock marathon and watch a scene from ''Vertigo''. Railly then transforms herself with a blonde wig, as Judy ([[Kim Novak]]) transformed herself into blonde Madeleine in ''Vertigo''; Cole sees her emerge within a red light, as Scottie ([[James Stewart]]) saw Judy emerge within a green light.<ref name="third"/> Brief notes of [[Bernard Herrmann]]'s film score can also be heard. Railly also wears the same coat Novak wore in the first part of ''Vertigo''. The scene at [[Muir Woods National Monument]], where Judy (as Madeleine) looks at the growth rings of a felled redwood and traces back events in her past life, resonates with larger themes in ''12 Monkeys''. Cole and Railly later have a similar conversation while the same music from ''Vertigo'' is repeated.<ref name="third"/> The Muir Woods scene in ''Vertigo'' is also re-enacted in ''La Jetée''. In a previous scene in the film, Cole wakes up in a hospital bed with scientists of the future talking to him in chorus. This is a direct homage to the "[[Dem Bones|Dry Bones]]" scene in [[Dennis Potter]]'s ''[[The Singing Detective]]''.<ref>{{cite web |title=SALON Reviews:12 Monkeys |url=http://www.salon.com/05/reviews/monkey2.html |work=[[Salon (website)|Salon]] |deadurl=yes |archiveurl=https://web.archive.org/web/20050531001122/http://www.salon.com/05/reviews/monkey2.html |archivedate=2005-05-31 |accessdate=2012-04-10}}</ref> Posters for the recently renamed English rock band [[Muse (band)|Muse]] cover the buildings in 1996 Philadelphia with a modified version of their [[Muse (EP)|1998 EP]]. The then-obscure band would later win the [[Grammy Award for Best Rock Album]] in 2011 and 2016.{{importance example|date=July 2016}} The character James Cole is a notable [[christ figure]] in film.<ref name="Kozlovic">{{cite web| first=Anton Karl| last=Kozlovic| url=http://www.usask.ca/relst/jrpc/art8-cinematicchrist.html| archiveurl=https://web.archive.org/web/20050223221011/http://www.usask.ca/relst/jrpc/art8-cinematicchrist.html| archivedate=2005-02-23| title=The Structural Characteristics of the Cinematic Christ-figure| work=Journal of Religion and Popular Culture| accessdate=2016-01-06}}</ref><ref>{{citation|title=Roman Catholicism in Fantastic Film|chapter=Blasphemy in the Name of Fantasy: The Films of Terry Gilliam in a Catholic Context|author=Christopher McKittrick|editor=Regina Hansen|publisher=[[McFarland & Company|McFarland]]|year=2011|isbn=9780786487240|pp=34–35}}</ref> The film is significant in the genre of science-fiction film noir, and it alludes to various "canonical noir" films.<ref>{{citation|title=Transgressing Women: Investigating Space and the Body in Contemporary Noir Thrillers|date=January 2005|publisher=Lancaster University|author=Jamaluddin Bin Aziz|section=Future Noir}}</ref>

==Release==

===Box office===
''12 Monkeys'' was given a [[limited release]] in the United States on December 29, 1995. When the 1,629 theater [[wide release]] came on January 5, 1996, the film earned $13.84&nbsp;million in its opening weekend. ''12 Monkeys'' eventually grossed $57,141,459 in US totals and $111,698,000 in other countries, coming to a worldwide total of $168,839,459.<ref name="mojo">{{cite web |url=http://www.boxofficemojo.com/movies/?id=twelvemonkeys.htm |title=12 Monkeys |publisher=[[Box Office Mojo]] |deadurl=no |archiveurl=https://web.archive.org/web/20090331134516/http://boxofficemojo.com/movies/?id=twelvemonkeys.htm |archivedate=2009-03-31 |accessdate=2012-04-10}}</ref> The film held the No.&nbsp;1 spot on box office charts for two weeks in January, before dropping due to competition from ''[[From Dusk Till Dawn (film)|From Dusk till Dawn]]'', ''[[Mr. Holland's Opus]]'' and ''[[Black Sheep (1996 film)|Black Sheep]]''.<ref>{{cite web | url = http://www.the-numbers.com/movies/1995/012MN.php | title = Twelve Monkeys | publisher = [[The Numbers (website)|The Numbers]] | accessdate = 2009-04-01}}</ref>

===Critical reception===
Based on 59 reviews collected by [[Rotten Tomatoes]], 88% of the critics gave the film positive reviews, with an average rating of 7.4/10. The consensus reads: "The plot's a bit of a jumble, but excellent performances and mind-blowing plot twists make ''12 Monkeys'' a kooky, effective experience."<ref>{{cite web |url=http://www.rottentomatoes.com/m/12_monkeys/ |title=12 Monkeys |publisher=[[Rotten Tomatoes]] |deadurl=no |accessdate=2012-04-10}}</ref> By comparison, [[Metacritic]] calculated a 74 out of 100 rating, based on 20 reviews.<ref>{{cite web |url=http://www.metacritic.com/movie/twelve-monkeys |title=12 Monkeys (1995): Reviews |publisher=[[Metacritic]] |deadurl=no |accessdate=2012-04-10}}</ref>

{{quote box|quote=
The film's startling depiction of the world in 2035—where human life has been driven underground by a 1990s viral outbreak that annihilated 99 percent of human life—may not always make sense. But ''12 Monkeys'' rattles with insightful sound and fury, and its bleak visions are hard to shake.|source=—Peter Stack, writing for the ''San Francisco Chronicle''<ref>Stack, Peter (5 January 1996). [http://www.sfgate.com/entertainment/article/12-Monkeys-Is-Not-Exactly-a-Barrel-of-Laughs-3000037.php {{"-}}'12 Monkeys' Is Not Exactly a Barrel of Laughs / Willis, Pitt in grimy futuristic thriller about killer virus]. ''San Francisco Chronicle''. Retrieved 2015-07-26.</ref>|align=left|width=40%|fontsize=85%|bgcolor=#FFFFF0|quoted=2}}

[[Roger Ebert]] observed ''12 Monkeys''{{'}} depiction of the future, finding similarities with ''[[Blade Runner]]'' (1982; also scripted by [[David Peoples]]) and ''[[Brazil (1985 film)|Brazil]]'' (1985; also directed by Terry Gilliam). "The film is a celebration of madness and doom, with a hero who tries to prevail against the chaos of his condition, and is inadequate", Ebert wrote. "This vision is a cold, dark, damp one, and even the romance between Willis and Stowe feels desperate rather than joyous. All of this is done very well, and the more you know about movies (especially the technical side), the more you're likely to admire it. And as entertainment, it appeals more to the mind than to the senses."<ref>{{cite news |author=Roger Ebert |url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19960105/REVIEWS/601050301/1023 |title=12 Monkeys |work=[[Chicago Sun-Times]] |date=1996-01-05 |authorlink=Roger Ebert |archiveurl=https://web.archive.org/web/20090215021903/http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19960105/REVIEWS/601050301/1023 | archivedate=2009-02-15 |deadurl=no |accessdate=2012-04-10}}</ref>

[[Desson Thomson]] of ''[[The Washington Post]]'' praised the art direction and set design. "Willis and Pitts's performances, Gilliam's atmospherics and an exhilarating momentum easily outweigh such trifling flaws in the script", Thomson reasoned.<ref>{{cite news |author=Desson Howe |url=http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/twelvemonkeys.htm#howe |title=Gilliam's Barrel of 'Monkeys' Shines |work=[[The Washington Post]] |date=January 5, 1996 |authorlink=Desson Howe |deadurl=no |accessdate=2012-04-10}}</ref> [[Peter Travers]] from [[Rolling Stone (magazine)|''Rolling Stone'' magazine]] cited the film's success on Gilliam's direction and Willis' performance.<ref>{{cite news |author=Peter Travers |url=http://www.rollingstone.com/movies/reviews/12-monkeys-19950101 |title=12 Monkeys |work=[[Rolling Stone (magazine)|Rolling Stone]] |date=January 1, 1995 |authorlink=Peter Travers |deadurl=no |accessdate=2012-04-10}}</ref> Internet reviewer [[James Berardinelli]] believed the filmmakers took an intelligent and creative motive for the time travel subplot. Rather than being sent to change the past, James Cole is instead observing it to make a better future.<ref>{{cite web |author=James Berardinelli |url=http://www.reelviews.net/movies/t/twelve_mon.html |title=Twelve Monkeys |work=[[ReelViews]] |authorlink=James Berardinelli |accessdate=2009-04-01}}</ref> [[Richard Corliss]] of [[Time (magazine)|''Time'' magazine]] felt the film's time travel aspect and apocalyptic depiction of a bleaker future were [[cliché]]s. "In its frantic mix of chaos, carnage and zoo animals, ''12 Monkeys'' is ''[[Jumanji (film)|Jumanji]]'' for adults", Corliss wrote.<ref>{{cite news |author=Richard Corliss |url=http://www.time.com/time/magazine/article/0,9171,983933,00.html |title=Back To The Bleak Future |work=[[Time (magazine)|Time]] |date=January 8, 1996 |authorlink=Richard Corliss |deadurl=no |accessdate=2012-04-10}}</ref>

===Accolades===
[[File:Brad Pitt Cannes 2012.jpg|thumb|upright|Supporting actor Brad Pitt was nominated for an Academy Award for his portrayal of Jeffrey Goines.]]
Pitt was nominated for an [[Academy Award for Best Supporting Actor]], but lost to [[Kevin Spacey]] in ''[[The Usual Suspects]]''. Costume designer [[Julie Weiss]] was also nominated for her work, but lost to [[James Acheson]] of ''[[Restoration (1995 film)|Restoration]]''.<ref>{{cite web |url=http://www.oscars.org/oscars/ceremonies/1996 |title=The 68th Academy Awards (1996) Nominees and Winners |work=[[Academy of Motion Picture Arts and Sciences]] |accessdate=2012-12-01}}</ref> However, Pitt was able to win a [[Golden Globe Award for Best Supporting Actor – Motion Picture|Golden Globe Award for Best Supporting Actor in a Motion Picture]].<ref>{{cite web |url=http://www.goldenglobes.org/browse/film/23446 |title=12 Monkeys |work=[[Hollywood Foreign Press Association]] |deadurl=no |accessdate=2012-04-10}}</ref>

Gilliam was honored for his directing duties at the [[46th Berlin International Film Festival]].<ref name="third">Christie, Gilliam, pp. 231–233</ref> ''12 Monkeys'' received positive notices from the science fiction community. The film was nominated for the [[Hugo Award for Best Dramatic Presentation]]<ref>{{cite web |title=1996 Hugo Awards |work=The [[Hugo Award]]s Organization |url=http://www.thehugoawards.org/hugo-history/1996-hugo-awards/ |deadurl=no |accessdate=2012-04-10}}</ref> and the Academy of Science Fiction, Fantasy & Horror Films awarded ''12 Monkeys'' the [[Saturn Award for Best Science Fiction Film]]. Pitt and Weiss also won awards at the 22nd [[Saturn Award]]s. Bruce Willis, Madeleine Stowe, Gilliam and writers David and Janet Peoples received nominations.<ref>{{cite web |title=Past Saturn Awards |work=Academy of Science Fiction, Fantasy & Horror Films |url=http://www.saturnawards.org/past.html#film |deadurl=no |accessdate=2012-04-10}}</ref>

===Home media===
[[Universal Studios Home Entertainment]]'s [[special edition]] of ''12 Monkeys'', released on May 10, 2005, contains an [[audio commentary]] by director Terry Gilliam and producer Charles Roven, ''The Hamster Factor and Other Tales of Twelve Monkeys'' (a [[making-of]] documentary) and production notes.<ref>{{cite web |url=http://www.amazon.com/exec/obidos/ASIN/B0007PALZ2 |title=12 Monkeys (Special Edition) (1996) |work=[[Amazon.com]] |deadurl=no |accessdate=2012-04-10}}</ref> An [[HD DVD]] of ''12 Monkeys'' was released on March 4, 2008, and includes the same special features as the special edition [[DVD]].<ref>{{cite web |url=http://www.amazon.com/exec/obidos/ASIN/B000HT3Q14 |title=12 Monkeys (Special Edition) (1996) |work=[[Amazon.com]] |deadurl=no |accessdate=2012-04-10}}</ref> A [[Blu-ray Disc]] of ''12 Monkeys'' was released on July 28, 2009, and includes the same special features as the previous special edition DVD and HD DVD.<ref>{{cite web |url=http://www.amazon.com/exec/obidos/ASIN/B0026FCNK2 |title=12 Monkeys (Special Edition) (1996) |work=[[Amazon.com]] |deadurl=no |accessdate=2012-04-10}}</ref>

==Post-release==

===Lebbeus Woods lawsuit===
In the beginning of the film, Cole is brought into the interrogation room and told to sit in a chair attached to a vertical rail on the wall. A sphere supported by a metal armature is suspended directly in front of him, probing for weaknesses as the inquisitors interrogate him.<ref name="ben"/> Architect [[Lebbeus Woods]] filed a lawsuit against Universal in February 1996, claiming that his work "Neomechanical Tower (Upper) Chamber" was used without permission. Woods won his lawsuit, requiring Universal to remove the scenes, but he ultimately allowed their inclusion in exchange for a "high six-figure cash settlement" from Universal.<ref name="ben">{{cite web |url=http://www.benedict.com/Visual/Monkeys/Monkeys.aspx |archiveurl=https://web.archive.org/web/20070202102337/http://benedict.com/visual/Monkeys/Monkeys.aspx|archivedate=February 2, 2007|title=Copyright Casebook: 12 Monkeys - Universal Studios and Lebbeus Woods |work=Benedict.com |deadurl=yes |accessdate=2012-04-10}}</ref><ref>''[http://scholar.google.ca/scholar_case?case=15497279041078373388 Woods v. Universal City Studios, Inc.]'', 920 [[F.Supp.]] 62 ([[S.D.N.Y.]] 1996)</ref>

===Trilogy claims===
[[File:Aaron Stanford - Nikita (cropped).jpg|thumb|upright|right|Actor [[Aaron Stanford]], who portrays the role of James Cole in the television adaptation.]]
After the release of ''[[The Zero Theorem]]'' in 2013, claims were made that Gilliam had meant it as part of a trilogy. A 2013 review for ''The Guardian'' newspaper said; "Calling it [''The Zero Theorem''] the third part of a trilogy formed by earlier dystopian satires ''[[Brazil (1985 film)|Brazil]]'' and ''Twelve Monkeys'' [sic] [...]";<ref>{{cite web|url=https://www.theguardian.com/film/2013/sep/02/terry-gilliam-zero-theorem-internet-breakdown-real-relationships|title=Terry Gilliam blames internet for the breakdown in 'real relationships'|last=Pulver|first=Andrew|publisher=''[[The Guardian]]''|date=2 September 2013|accessdate=7 September 2013}}</ref> however, Gilliam was interviewed by Alex Suskind for ''Indiewire'' in late 2014, saying "Well, it’s funny, this trilogy was never something I ever said, but it’s been repeated so often it’s clearly true [laughs]. I don’t know who started it but once it started it never stopped".<ref name=IWGint01>{{cite web|author1=Alex Suskind|title=Interview: Terry Gilliam On ‘The Zero Theorem,’ Avoiding Facebook, ‘Don Quixote’ And His Upcoming Autobiography|url=http://blogs.indiewire.com/theplaylist/interview-terry-gilliam-on-the-zero-theorem-avoiding-facebook-don-quixote-and-his-upcoming-autobiography-20140917|publisher=Indiewire|accessdate=4 March 2015|date=2014-09-17}}</ref>

===TV adaptation===
{{Main article|12 Monkeys (TV series)}}
On August 26, 2013, ''[[Entertainment Weekly]]'' announced that [[Syfy]] was developing a [[12 Monkeys (TV series)|''12 Monkeys'' television series]] based on the film. Production began in November 2013. The [[television pilot|pilot]] was written by Terry Matalas and Travis Fickett, who had written for the series ''[[Terra Nova (TV series)|Terra Nova]]''. Due to the series being labeled as "cast contingent", the series did not move forward until the roles of Cole and Goines were cast.<ref>{{cite web | author=Lynette Rice | title=SyFy orders '12 Monkeys' pilot | work=Entertainment Weekly | url=http://insidetv.ew.com/2013/08/26/syfy-orders-12-monkeys-pilot | date=August 26, 2013 | accessdate=August 27, 2013}}</ref> In April 2014, Syfy [[green-light]]ed the first season, which consisted of 13 episodes, including the pilot filmed in 2013. The series premiered on January 16, 2015.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2014/04/04/syfy-greenlights-12-episodes-of-12-monkeys/251263/ |title=Syfy Greenlights 12 Episodes of '12 Monkeys' (Updated)|work=TV by the Numbers|author=Bibel, Sara|date=April 4, 2014|accessdate=April 5, 2014}}</ref> On March 12, 2015, the series was renewed for a second season that began airing in April 2016.<ref>{{Cite web|url=http://tvline.com/2015/03/12/12-monkeys-renewed-season-2-syfy/|title=12 Monkeys Renewed for Season 2|publisher=[[TVLine]] |last=Roots|first=Kimberly|date=March 12, 2015|accessdate=March 12, 2015}}</ref> On June 29, 2016, the series was renewed for a 10-episode third season, set to premiere in 2017.<ref>{{Cite web|url=http://www.ew.com/article/2016/06/29/12-monkeys-season-3-renewal|title=12 Monkeys renewed for season 3 — exclusive|publisher=Entertainment Weekly|last=Abrams|first=Natalie|date=June 29, 2016|accessdate=July 17, 2016}}</ref>
{{clear}}

==References==
{{reflist|colwidth=30em}}

==External links==
{{wikiquote}}
<!-- The names used here are the respective page titles at the websites -->
* {{Official website|http://www.uphe.com/movies/12-monkeys}}
* {{IMDb title|0114746|12 Monkeys}}
* {{mojo title|twelvemonkeys|12 Monkeys}}
* {{rotten-tomatoes|12_monkeys|12 Monkeys}}
* {{metacritic film|twelve-monkeys|12 Monkeys}}
* [http://www.dailyscript.com/scripts/twelve_monkeys.html ''12 Monkeys''] at DailyScript.com

{{Terry Gilliam}}
{{David Peoples}}
{{Saturn Award for Best Science Fiction Film 1991–2010}}

{{good article}}

[[Category:1995 films]]
[[Category:1990s science fiction films]]
[[Category:American films]]
[[Category:American science fiction films]]
[[Category:Apocalyptic films]]
[[Category:Dystopian films]]
[[Category:English-language films]]
[[Category:Environmental films]]
[[Category:Features based on short films]]
[[Category:Films about animal rights]]
[[Category:Films about viral outbreaks]]
[[Category:Films directed by Terry Gilliam]]
[[Category:Films set in 1990]]
[[Category:Films set in 1996]]
[[Category:Films set in 2035]]
[[Category:Films set in Baltimore]]
[[Category:Films set in Philadelphia]]
[[Category:Films set in psychiatric hospitals]]
[[Category:Films shot in Baltimore]]
[[Category:Films shot in Pennsylvania]]
[[Category:Nonlinear narrative films]]
[[Category:Post-apocalyptic films]]
[[Category:Screenplays by David Peoples]]
[[Category:Time travel films]]
[[Category:Atlas Entertainment films]]
[[Category:Universal Pictures films]]
[[Category:PolyGram Filmed Entertainment films]]
[[Category:American remakes of French films]]