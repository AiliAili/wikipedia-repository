<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Caproni Ca.331 Raffica
 |image=Caproni Ca.331.jpg
 |caption=The first Caproni Vizzola Ca.331, completed as the Ca.331 O.A. (''Osservazione Area'') [[reconnaissance aircraft]] [[prototype]].
}}{{Infobox Aircraft Type
 |type=Reconnaissance aircraft, bomber, and night fighter
 |national origin=Italy
 |manufacturer=[[Caproni]]
 |designer=[[Engineer's degree|Ing]] [[Cesare Pallavacino]]
 |first flight=Ca.331 O.A.: 31 August [[1940 in aviation|1940]]<br/>Ca331 C.N.: Summer [[1942 in aviation|1942]]
 |introduced=
 |retired=
 |status=
 |primary user=''[[Regia Aeronautica]]'' (Italian Royal Air Force)
 |more users=
 |produced=
 |number built=3 <ref>[http://www.alieuomini.it/catalogo/dettaglio_catalogo/caproni_ca_a_b,42.html Caproni Ca.331]</ref>
 |variants with their own articles=
}}
|}
The '''Caproni Ca.331 ''Raffica''''' ("Gust of Wind" or "Fire Burst") was an [[Italy|Italian]] aircraft built by [[Caproni]] in the early 1940s as a tactical [[reconnaissance aircraft]]/light [[bomber]] and also as a [[night fighter]].

==Development==

===Ca.331 O.A. (Ca.331A)===
[[File:Caproni Ca.331 O.A.jpg|left|thumb|The Caproni Ca.331 O.A. [[prototype]].]]In response to a 1938 Italian [[Air Ministry (Italy)|Air Ministry]] requirement for a new tactical reconnaissance aircraft with combat capability, [[Engineer's degree|Ing]] [[Cesare Pallavacino]]<ref name="Green">{{cite book|last=Green|first=William|author2=Gordon Swanborough|title=The Complete Book of Fighters: An Illustrated Encyclopedia of Every Fighter Aircraft Built and Flown|pages=108|publisher=SMITHMARK Publishers|location=New York|year=1994|isbn=0-8317-3939-8}}</ref> of the Caproni company{{'}}s Caproni Bergamaschi subsidiary began the design of the Ca.331 O.A. (O.A. standing for ''Osservazione Area'', [[Italian language|Italian]] for "Area Observation"), also designated Ca.331A, in October 1938. It was innovative for Caproni in being of all-metal construction. The Ca.331 O.A. prototype, a twin-engine low-wing [[monoplane]] with an unstepped cockpit and glazed nose, had [[duralumin]] stressed skin on both its fuselage and wings, and its wings were of an inverted gull-wing configuration. It had two [[Isotta-Fraschini]] Delta RC.40 engines rated at 574 [[kilowatt]]s (770 [[horsepower]]) each. The aircraft employed a three-man crew of pilot, observer/gunner, and radioman/gunner, and was armed with four 12.7-millimeter (0.5-inch) [[Breda-SAFAT machine gun]]s—two of them in fixed mounts in the wing roots firing forward, one in a dorsal turret, and one in a ventral mount. The Ca.331 O.A. also had a [[bomb bay]] capable of carrying up to 1,000 kilograms (2,205 pounds) of bombs and four external bomb racks under its wings.<ref name="modelingmadness.com">''Aerofan'' Issue 76, January–March 2001, via [http://modelingmadness.com/reviews/axis/ity/bus331.htm modelingmadness.com LF Models 1/72 Caproni Ca.331  OA/CN] {{webarchive |url=https://web.archive.org/web/20100210094006/http://modelingmadness.com/reviews/axis/ity/bus331.htm |date=February 10, 2010 }}.</ref><ref name="warbirdsforum.com">[http://warbirdsforum.com/showthread.php?t=1776 warbirdsform.com GOT: The Caproni Ca. 331].</ref>

The Ca.331 O.A. prototype first flew on 31 August 1940, at [[Ponte San Pietro]] with Caproni test pilot [[Ettore Wengi]] at the controls. Its original [[Piaggio]] [[propeller (aircraft)|propeller]]s proved inadequate, but their replacement with [[Alfa Romeo]]-built propellers in 1941 resulted in the aircraft having improved performance which, in fact, exceeded expectations. In 1941, Caproni Bergamaschi delivered the prototype to the ''[[Regia Aeronautica]]'' (Italian Royal Air Force), which began official tests at [[Guidonia Montecelio]] with good results. However, the ''Regia Aeronautica'' handed the aircraft back to Caproni Bergamaschi without a production order. The German Air Force (''[[Luftwaffe]]'') then requested control of the aircraft for trials at its test center at [[Rechlin]] in Germany. Although the ''Luftwaffe'' was impressed with the aircraft—probably more so than the Italians—no German order for it resulted.<ref name="modelingmadness.com"/><ref name="warbirdsforum.com"/>{{Verify source|date=January 2010}}

The Ca.331 O.A. won widespread praise from both the Italian and German pilots—including German [[fighter ace]] [[Werner Mölders]] (1913–1941) -- who tested it, and the chief of the German ''Luftwaffe'', [[Hermann Göring]] (1893–1946), was given a demonstration of its capabilities at Guidonia Montecelio in January 1942. The reasons for the lack of a production order by either Italy or Germany remain obscure and a matter of debate. Reasons given by various sources include a supposed overreliance on the use of materials in the Ca.331{{'}}s construction that may have been in scarce supply (although this itself is controversial) in wartime Italy and Germany; the use of too many parts in the Ca.331, uncommon with other Italian aircraft (although it is not clear what specifically these may have been); political problems that Caproni company founder [[Gianni Caproni]] (1886–1957) may have had with the [[Italian fascism|Italian Fascist]] government (which have not been identified clearly); an unwillingness of the Italian military to cooperate too closely with their German counterparts for fear of being dominated by Germany in the event of a German victory in [[World War II]]; or simply that the Ca.331 was under development for so long that the requirement for it disappeared before it could enter production, a common problem in the Italian aircraft industry during the World War II years.<ref>For a discussion of these issues, see [http://warbirdsforum.com/showthread.php?t=1776 warbirdsform.com GOT: The Caproni Ca. 331].{{Verify source|date=January 2010}}</ref>
The sole Ca.331 O.A. prototype was at the Caproni company airfield at [[Taliedo]] at the time the [[Armistice between Italy and Allied armed forces|Italian armistice]] with the [[Allies of World War II|Allies]] took effect on 8 September 1943. Seized by German forces, it was disassembled, shipped to Germany, and later scrapped.<ref name="modelingmadness.com"/><ref name="warbirdsforum.com"/>

===Ca.331 C.N. (Ca.331B)===
[[File:Caproni Ca.331 C.N.jpg|left|thumb|The second Caproni Ca.331 [[prototype]]. Ordered as a second Ca.331 O.A. [[reconnaissance aircraft]] prototype, it was completed instead as the first prototype of the Ca.331 C.N. [[night fighter]] as seen here, probably during weapons testing at the [[Furbara]] firing range.]]By 1942, the war situation had changed to the point where Italy perceived a greater need for air defense capabilities. Accordingly, in May 1942 the Italian Air Ministry ordered the second Ca.331 prototype, originally planned as a second Ca.331 O.A., to be completed instead as the first prototype of a night fighter version of the aircraft. The night fighter prototype was designated Ca.331 C.N., with "C.N." standing for ''Caccia Notturna'', Italian for "Night Fighter"<ref name="Green"/> it also was known as the Ca.331B.<ref name="warbirdsforum.com"/>

The Ca.331 C.N., which first flew in the summer of 1942, differed from the Ca.331 O.A. in having a stepped cockpit and less nose glazing. Its armament was installed in the spring of 1943 and consisted of four fixed forward-firing 20-millimeter [[Mauser]] [[MG 151 cannon]] and four 12.7-millimeter (0.5-inch) Breda-SAFAT machine guns—two of them forward-firing and fixed, one in a dorsal turret, and one in a ventral mount.  Its original 596-[[kilowatt]] (800-[[horsepower]]) 12-cylinder air-cooled [[Isotta-Fraschini]] Delta IV engines were replaced by the spring of 1943 with 633-kilowatt (850-horsepower) versions of the Delta IV with two-stage [[supercharger]]s.<ref name="Green"/> Like the Ca.331 O.A. prototype, it was at the Caproni company airfield at Taliedo when the Italian armistice with the Allies took effect on 8 September 1943, and it suffered the same fate: The Germans seized it there, disassembled it and shipped it to Germany.<ref name="modelingmadness.com"/><ref name="warbirdsforum.com"/>

A second Ca.331 C.N. prototype was constructed, differing from the first in having an armament of two 20-millimeter Ikaria cannon and four 12.7-millimeter Breda-SAFAT machine guns, all mounted in the nose. It was still being assembled when Italy surrendered to the Allies on 8 September 1943, was seized by the Germans and shipped to Germany, destined never to fly.<ref name="Green"/><ref name="modelingmadness.com"/><ref name="warbirdsforum.com"/>

Various modifications were proposed for production models of the Ca.331 C.N. Engines proposed for production models included the German 1,099-kilowatt (1,475-horsepower) [[Daimler-Benz DB 605]], the Italian 931-kilowatt (1,250-horsepower) [[Isotta-Fraschini]] Zeta R.C.42, and the [[Fiat]] RA-1050 RC.58 ''Tifone'' (Italian for "Typhoon"), an Italian copy of the DB 605 expected to give the Ca.331 C.N. a top speed of {{convert|644|km|mi|sp=us}} per hour. Italian authorities also made various aggressive plans for production of Ca.331 C.N. aircraft, with as many as 100 built by May 1942 toward an eventual total production run of 1,000 aircraft. However, all production plans were cancelled in January 1943.<ref name="warbirdsforum.com"/>

===Other variants===
Germany considered building a dual-control combat [[Trainer (aircraft)|trainer]] version of the Ca.331 to be designated the '''Ca.331G'''. Nothing came of the proposal. An [[Anti-tank warfare|anti-tank]] version mounting a 37-millimeter cannon also was proposed, but none were built.<ref name="warbirdsforum.com"/>

==Operators==
;{{flag|Kingdom of Italy}}
*''[[Regia Aeronautica]]'' for evaluation only.
;{{flagcountry|Nazi Germany}}
*''[[Luftwaffe]]''

==Variants==
;Caproni Ca.331 O.A. (or Caproni Ca.331A)
:Tactical reconnaissance aircraft/light bomber prototype (one built)
;Caproni Ca.331 C.N. (or Caproni Ca.331B)
:Night fighter prototype (two built)
;Caproni Ca.331G
:Proposed German dual-control combat trainer (none built)

==Specifications (Ca.331 C.N.) ==
{{Aircraft specs
|ref=Italian Civil & Military Aircraft 1930-1945<ref>{{cite book|last=Thompson|first=Jonathan|title=Italian Civil & Military Aircraft 1930-1945|publisher=Aero Publishers Inc.|location=New York|year=1963|edition=1st|pages=114–117|isbn=0-8168-6500-0|oclc=}}</ref><!-- reference -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=three
|capacity=
|length m=
|length ft=38
|length in=6.25
|length note=
|span m=
|span ft=53
|span in=9.75
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=10
|height in=6.25
|height note=
|wing area sqm=
|wing area sqft=414.4
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=10,140
|empty weight note=
|gross weight kg=
|gross weight lb=14,992
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Isotta-Fraschini Delta IV]] piston
|eng1 type=inverted air-cooled V-12 piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=840<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=314
|max speed kts=
|max speed note=at {{convert|5,303.5|m|ft|abbr=on|1}}
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=994
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=26,550
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude={{convert|3,999|m|ft|abbr=on|1}} in 9 minutes 35 seconds
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->

|guns=<br/>
;;;Original armament
::6 x {{convert|12.7|mm|in|abbr=on|1}} [[Breda-SAFAT machine gun]]s in the stepped "solid" nose.
::2 x {{convert|12.7|mm|in|abbr=on|1}} [[Breda-SAFAT machine gun]]s machine guns in dorsal and ventral defensive positions.
;;;Tested with
::2 x {{convert|12.7|mm|in|abbr=on|1}} [[Breda-SAFAT machine gun]]s machine guns and 4 x {{convert|20|mm|in|abbr=on|1}} [[Mauser cannon]].
::or
::6 x {{convert|20|mm|in|abbr=on|1}} [[20mm Ikaria cannon|Ikaria]] cannon in a ventral bulge.
;;;Studies made for
::1 x {{convert|37|mm|in|abbr=on|1}} cannon in a ventral bulge.
|bombs= <br/>
;;;In the bomb bay
::{{convert|1,000|kg|lb|abbr=on|0}} of small bombs.
::or
::1 x {{convert|500|kg|lb|abbr=on|0}} bomb.
::and
::1 x {{convert|519|l|USgal|abbr=on|0}} auxiliary fuel tank.
|rockets= 
|missiles= 
|hardpoints=2
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=2 x {{convert|100|kg|lb|abbr=on|0}}
|hardpoint other=2 x underwing tanks

|avionics=
}}

==Notes==
{{reflist}}

==References==
*{{cite book|last=Thompson|first=Jonathan|title=Italian Civil & Military Aircraft 1930-1945|publisher=Aero Publishers Inc.|location=New York|year=1963|edition=1st|pages=114–117|isbn=0-8168-6500-0|oclc=}}
*''Aerofan'' Issue 76, January–March 2001
*{{cite book|last=Green|first=William|author2=Gordon Swanborough|title=The Complete Book of Fighters: An Illustrated Encyclopedia of Every Fighter Aircraft Built and Flown|pages=108|publisher=SMITHMARK Publishers|location=New York|year=1994|isbn=0-8317-3939-8}}

==External links==
{{Commons category}}
*[http://warbirdsforum.com/showthread.php?t=1776 warbirdsforum.com GOT: The Caproni Ca. 331]
*[https://web.archive.org/web/20100210094006/http://modelingmadness.com/reviews/axis/ity/bus331.htm modelingmadness.com LF Models 1/72 Caproni Ca.331  OA/CN]

{{Caproni aircraft}}

[[Category:Caproni aircraft|Ca.331]]
[[Category:Italian fighter aircraft 1940–1949]]
[[Category:Italian military reconnaissance aircraft 1940–1949]]
[[Category:Italian bomber aircraft 1940–1949]]