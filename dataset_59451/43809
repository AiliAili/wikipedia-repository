<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=W-B
 | image=W-Ba 02.png
 | caption=On the slipway at de Kok
}}{{Infobox Aircraft Type
 | type=Long range [[reconnaissance aircraft|reconnaissance]] [[sea plane]]
 | national origin=[[Netherlands|Nederlands]]
 | manufacturer=Van Berkel's Patent Company Ltd, [[Rotterdam]]
 | designer=Von Baumhauer<ref name=DNV/>
 | first Flight=13 September 1920<ref name=DNV/>
 | introduced=1920/1
 | retired=1933
 | status=
 | primary user= [[Dutch Naval Aviation Service]] (MLD)
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=7<ref name=DNV/>
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Hansa-Brandenburg W.29]]
 | variants with their own articles=
}}
|}

The '''Van Berkel W-B''' was a single engine [[Netherlands|Dutch]] long range reconnaissance [[seaplane]] built in the early 1920s for work in the [[Dutch East Indies]]. Six were operated by the [[Dutch Naval Aviation Service]] (MLD) with disappointing results, though the last two were not decommissioned until 1933.

==Design and development==
[[File:W-Ba 01.png|right|thumb|W-B head on]]

As neutrals in [[World War I]], the Dutch armed forces experienced difficulties in obtaining military aircraft from abroad.  To ease the problems of both the Dutch [[Luchtvaartafdeling|Army Aviation Group (LVA)]] and the MLD, in August 1917 the Ministry of War encouraged companies like the car makers [[Spyker]] to become involved in aircraft production. The only other manufacturer to volunteer was Van Berkel's Patent Company Ltd, formed to make meat processing cutters. They began by building the [[Hansa-Brandenburg W.12]] [[floatplane]] [[fighter aircraft|fighter]] under licence as the Van Berkel W-A, then in 1919 the MLD invited to company to design and build a long range [[reconnaissance]] floatplane for use in the [[Dutch East Indies]], based on the [[Hansa-Brandenburg W.29]].  The resulting Van Berkel W-B was broadly similar to the W.29 but both larger and more powerful: its span was increased by more than 40% and it had a 360&nbsp;hp rather than a 150&nbsp;hp engine.<ref name=DNV/>

The W-B was a braced [[monoplane#Types of monoplane|low-wing monoplane]] with wings of constant [[chord (aircraft)|chord]].  These were braced from below with pairs of parallel steel tube [[lift strut]]s on each side, assisted by small [[jury strut]]s, connecting the wing to the top of the appropriate float. Each float was mounted on the [[fuselage]] lower [[longeron]]s by basically N-form struts, the leading member doubled into a narrow V, sharing common junctions with both the wing struts and two horizontal cross braces between the floats. Two more V-form struts linked the centres of the cross members vertically upwards to the lower fuselage. The floats, with rounded noses, three steps and vertical stern posts were made of aluminium alloy since wood degraded rapidly in tropical waters.<ref name=Flight/>

The fuselage was of simple, rectangular cross section.  The 270&nbsp;kW (360&nbsp;hp) [[Rolls-Royce Eagle]] VIII [[water-cooled]] [[V12 engine|V-12 engine]] was installed in the nose with its upper [[cylinder (engine)|cylinders]] exposed and a long external exhaust down the fuselage starboard side.  Underneath the nose there were two "lobster pot" style [[Lamblin]] [[radiator (engine cooling)|radiator]]s.  The pilot sat in an open [[cockpit]] just aft of the engine, with a second cockpit for the gunner some way behind.  The fuselage remained deep and high sided back to the tail, possibly offering some directional stability in the absence of a fixed [[fin]].  The tailplane was mounted on top of the fuselage just ahead of the extreme tail, where an unusual [[rudder]] which extended both above and below the fuselage was hinged. This was one of the features that visually distinguished the W-B from the W.29, on which the rudder was entirely below the top of the fuselage.<ref name=Flight/>

The aviation department of Van Berkel Patent was closed in June 1921<ref name=DNV/> and the proposed civilian version of the W-B, the '''W-F''' was never built.

==Operational history==
[[File:W-Ba 03.png|right|thumb|Takeoff from a lake near Rotterdam]]
The W-B flew for the first time on 3 September 1920.  After testing, which showed that the specification parameters were met, six production aircraft were delivered to the MLD.  The last went to the [[Tanjung Priok|Priok]] Naval Air Station in [[Indonesia]] in December 1923. Their performance in operational conditions was disappointing, with poor flight characteristics and the first four were withdrawn from service in 1926; the last two remained in use for another seven years.<ref name=DNV/>

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref=Flight 13 April 1921<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|capacity=
|length ft=36
|length note=
|span ft=64
|height ft=14
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight note=
|useful load lb=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=782 L (172 Imp gal; 207 US gal)
|more general=
*'''Useful load:''' 950 kg (2,100 lb), of which approximately 610 kg (1,350 lb) is fuel.
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rolls-Royce Eagle]] VIII
|eng1 type=water cooled V-12
|eng1 hp=360
|eng1 note=
|power original=
|more power=

|prop blade number=4
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=180
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range miles=1000
|range note=approximately, at cruising speed<ref name=DNV/>
|endurance=7.5 h at full throttle<ref name=DNV/>
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=
<!--
        Armament
-->
|guns= 3 machine guns, two forward firing, one on mounting in gunner's cockpit
|bombs= "a nest"
|other armament= 
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|Van Berkel W-B}}
{{reflist|refs=

<ref name=DNV>{{cite book |title= De Nederlandse vliegtuigen |last=Wesselink|first=Theo|last2= Postma|first2=Thijs|year=1982|publisher=Romem  |location=Haarlem |isbn=90 228 3792 0|page=19}}</ref>

<ref name=Flight>{{cite magazine|date=14 April 1921|title= THE VAN BERKEL TYPE W.B. SEAPLANE MONOPLANE|magazine=[[Flight International|Flight]]|volume=XIII |issue=15 |pages=260–1|url=http://www.Flightglobal.com/pdfarchive/view/1921/1921%20-%200260.html}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->

[[Category:Dutch aircraft 1920–1929]]