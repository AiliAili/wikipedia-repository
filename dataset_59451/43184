{|{{Infobox Aircraft Begin
  |name = Marinens Flyvebaatfabrikk M.F.11
  |image = M.F. 11 F.322.jpg
  |caption = M.F.11 F.322 in RNNAS service
}}{{Infobox Aircraft Type
  |type = Maritime reconnaissance
  |manufacturer = [[Marinens Flyvebaatfabrikk]]<ref name="Kjæraas03 3">Kjæraas 2003: 3</ref>
  |designer = [[Captain (naval)|Captain]] [[Johan E. Høver]]<ref name="Kjæraas03 3"/>
  |first flight = 29 September [[1931 in aviation|1931]]<ref name="Kjæraas03 3"/>
  |introduced = [[1932 in aviation|1932]]<ref name="Kjæraas03 3"/>
  |retired = <!--date the aircraft left military or revenue service. if vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Norwegian Navy Air Service]]<ref name="Kjæraas03 3"/>
  |more users = [[Luftwaffe]]<ref name="Kjæraas03 21">Kjæraas 2003: 21</ref><br />[[Finnish Air Force]]<ref name="Kjæraas30"/><ref>FAF in Color: [http://www.sci.fi/~ambush/faf/fafincolor.html Finnish Air Force Aircraft: Høver M.F. 11] {{en icon}}</ref>
  |produced = 11 October 1930 - 1 January 1939<ref name="Kjæraas03 3"/>
  |number built = 29<ref name="Kjæraas03 3, 21">Kjæraas 2003: 3, 21</ref>
  |unit cost = 
  |developed from = [[Marinens Flyvebaatfabrikk M.F.10]]
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Marinens Flyvebaatfabrikk M.F.11''' (sometimes known as the '''Høver M.F.11''' after its designer) was a three-seat, single-engined [[biplane]] used by the [[Royal Norwegian Navy Air Service]] for maritime reconnaissance in the decade before the [[World War II|Second World War]].

The M.F. 11 was the main aircraft of the Royal Norwegian Navy Air Service up until the [[Operation Weserübung|German invasion of Norway]] in 1940.

==Design and production==
As the final [[Hansa Brandenburg W.33]] seaplane left the [[Marinens Flyvebaatfabrikk]] in [[Horten]] in 1929 it was already clear that a new machine was needed to fulfil the needs of the RNoNAS. Thus, by the summer of 1929 Marinens Flyvebaatfabrikk and its chief designer, [[Captain (naval)|Captain]] J.E. Høver was ordered to design and construct a new seaplane. In the span of little over a year Høver, in cooperation with pilots, observers and other specialists designed the Høver M.F. 11. During the design period several foreign designs were also evaluated by the Norwegian military, but by 11 October 1930 the [[Norwegian Ministry of Defence]] ordered the production of the M.F. 11, which at that time was referred to as a "self-defence scout plane".<ref name="Kjæraas03 3"/>

At the outset the Norwegian naval pilots wanted a [[monoplane]] design for their new naval aircraft, but due to the RNoNAS demanding the new plane to have a maximum wingspan of 15.4 m, to allow it to fit into the existing hangars, a biplane structure became necessary.<ref name="Kjæraas03 3"/>

The fuselage of the M.F. 11 was made of welded steel bars and wood formers, covered with canvas.  Crew members used [[Speaking tube|gosport tubes]] for communication.  Twenty-nine aircraft were produced in total.<ref name="Kjæraas03 3, 21"/>

The first aircraft, F.300, made its first flight on 29 September 1931.<ref name="Kjæraas03 3"/> The M.F. 11s were equipped with the British-designed Armstrong Siddeley Panther II [[radial engine]], the first 14 of which were made in the [[United Kingdom]]. From 1934 license manufacturing of the Armstrong Siddeley Panther II began in Norway at Marinens Minevesen in Horten, with F.314 being the first aircraft equipped with a Norwegian-made engine. As the handmade Norwegian power plants were installed they soon proved to be of superior quality to the machine manufactured originals.<ref name="Kjæraas03 28">Kjæraas 2003: 28</ref> The same engine was also produced for the [[Norwegian Army Air Service]] aircraft.<ref name="Kjæraas03 3"/> A total of 17 Panther IIs were made in Norway, seven for Navy and ten for Army aircraft. The licence production ended in 1938 as import engines became freely available at a low cost after the Panther II was abandoned by the UK armed forces as it could no longer be used to propel the increasingly powerful British aircraft.<ref name="Kjæraas03 28"/>

==The M.F. 11 in Norwegian service==

===Pre-war service===
The M.F. 11 entered service with the RNoNAS in 1932 and was used for numerous tasks along the coast of Norway and in Norwegian territorial waters.

After the outbreak of the Second World War in 1939 this included taking part in all major military exercises, searching for mines and missing ships, and being stationed at the various coastal fortifications around the country as reconnaissance assets. At the dawn of the German invasion some of the robust aircraft had logged close to 900 hours of flight time.<ref name="Kjæraas03 3"/> Shortly before the war the Royal Norwegian Navy Air Service had decided to find a replacement for the M.F.11, and on 8 March 1940 24 [[Northrop N-3PB]]s were ordered from the US. None of these were delivered before the 9 April 1940 German invasion of Norway.<ref>Hafsten 2003: 120&ndash;122</ref>

===Trøndelag Naval District===
M.F. 11s saw active service all along the Norwegian coastline following the German invasion, from [[Western Norway]] to [[North Norway]].  One M.F. 11, F.342, was among the first Norwegian units to make contact with the invasion forces.  On 8 April 1940  Lieutenants Kaare Strand Kjos and Magnus Lie of the [[Trøndelag]] Naval District were dispatched to the [[Kornstadfjord]] near [[Lyngstad, Møre og Romsdal|Lyngstad]] in [[Eide]] where a German [[Arado Ar 196]] had made an emergency landing.  After the two German pilots, Oberleutnant Werner Techam and Leutnant Hans Polzin, had approached the locals trying to purchase fuel for their airplane they were captured by a group of civilians.  Thereafter they were reported to and arrested by armed police officers. As Kjos and Lie landed shortly thereafter they took command and organised the transport of the Germans and their plane to [[Kristiansund]] for internment. The Arado would later turn out to have been catapult launched from the German cruiser [[German cruiser Admiral Hipper|''Admiral Hipper'']].  A few days earlier the Royal Norwegian Navy Air Service' Trøndelag Air Group had been reduced from two to one M.F. 11 when F.340 capsized in [[Trondheim]] harbour due to heavy winds.<ref>Sivertsen 1999: 105, 115-116</ref>

===Romsdalsfjord Air Group===
'''The Air Group's composition'''
One of the Norwegian air units in which the M.F. 11 saw action was the highly improvised Romsdalsfjord Air Group (Norw. ''Romsdalsfjordgruppa'').  The air group was eventually to consist of a total of four aircraft; one M.F. 11 (F.342, originally from Trøndelag Naval District), one Arado Ar 196 (the one captured at Lyngstad) and two [[Fleet Air Arm]] [[Supermarine Walrus]]'.  The first Walrus (P5649) of [[700 Naval Air Squadron]] was one released by Norwegian authorities after having been interned in [[Kristiansund]] on 8 April after failing to return to the battleship [[HMS Rodney (29)|HMS ''Rodney'']] after a scouting mission due to high waves.<ref>Sivertsen 1999: 105</ref> The other arrived in [[Molde]] on 13 April to inform the [[Royal Norwegian Navy]] command of the imminent arrival of an RN task force.  After meeting with Captain Ullring, the commander of the local RNoN district in the [[Romsdal]] area, the crew of the Walrus decided on joining the Romsdalsfjord Air Group for the time being.<ref name="Sivertsen99: 117">Sivertsen 1999: 117</ref> The air group was based out of [[Eidsøra]], where the newly built school was utilised as a barracks and the local rifle association provided a guard force of riflemen for observations of air and naval activity.  A group of local women provided the force of around twenty-five officers and men with food and other supplies.<ref name="Sivertsen99: 118">Sivertsen 1999: 118</ref>

'''Operations'''
Operations of the Romsdalsfjord Air Group, including M.F. 11 F.342, was almost completely restricted to scouting the coast of Romsdal for enemy forces.  This was because the group had only 2,000 rounds of machine gun ammunition, no tracer ammunition or bombs and only fuel for a few days if all four planes were to be used.<ref name="Sivertsen99: 117"/> On 12 April, on one such mission, F.342 discovered and reported the exact position of two German merchant vessels to Captain Ullring.<ref>Sivertsen 1999: 117-118</ref> This report lead helped RNoN warships in seizing the German ships that same day (see: [[HNoMS Sleipner (1936)|HNoMS ''Sleipner'']]).  Actual contact with German forces occurred very seldom; on one reconnaissance mission over Trøndelag F.342 came under fire from German aircraft, but escaped without suffering any damage. The only real combat involving the air group came off [[Vigra]] on 14 April when F.342 encountered a German Heinkel He 115 and exchanged fire with it from ranges varying from 300 to 50 meters.  Neither the German or the Norwegian aircraft was hit and when the He 115 turned to escape northwards F.342 lacked the speed to chase after it.<ref name="Sivertsen99: 118"/>

'''Escape to the UK'''
On 17 April ltns Kjos and Diesen met with RNoN Commander Gottwaldt and decided that the four aircraft had to be evacuated as fuel reserves were down to only five to six hours flying time.  The British pilots' wish to return to their own units also played in on the decision.  The hope and plan was for the Norwegian pilots to return as soon as possible with better aircraft and supplies.  At 0330hrs the next day the Arado Ar 196 flew off first with a crew of three, making the journey to [[Shetland]] without problems and landing safely at around 0630.  As the air group had only two sets of maps F.342 had to fly in formation with the two Walrus' planes.  When the planes approached [[Orkney]] F.342 had straggled behind the British aircraft and was intercepted by three [[Gloster Gladiator]]s from [[Scapa Flow]].  The Gladiator crews misread the situation, believing that the Norwegian biplane was chasing the two Walrus' flying boats, and began to attack the M.F 11. Luckily for the Norwegians lieutenant Diesen managed to immediately land on the water. Although their aircraft was hit by forty to fifty machine gun rounds the crew survived without injury.<ref name="Sivertsen99: 122">Sivertsen 1999: 122</ref> After this initial [[friendly fire]] episode the Norwegians were welcomed as allies and gave the British commanders a report of the situation in the Romsdal area.  The German Arado Ar 196 attracted especial attention amongst the British and a Fleet Air Arm Commander was given the task of flying it down to [[Helensburgh]] where aircraft designers were waiting to disassemble and study the modern German design.  This did however not go quite as planned as the plane tipped over and sank during landing, although the pilot made it out alive.<ref name="Sivertsen99: 122"/> Even though the Norwegian officers expressed their desire to return to Romsdal to continue the fight there no one of them were to return to Norway in time to take part in the remaining fighting there.<ref name="Sivertsen99: 122"/>

==Use in Finland==
[[File:Høver M.F.11.jpg|thumb|right|[[Finnish Air Force|FAF]] Høver M.F. 11 NK-172 (ex-Norwegian F.336) sometime during the [[Continuation War]].]]
As the [[Norwegian Campaign]] was coming to an end in June 1940, Norwegian military pilots were ordered to prevent their aircraft from falling into German hands.  Most of the Norwegian He 115s were flown to the [[UK]] in order to keep them in the war, but for the three operational M.F. 11s and one He 115 (F.50)<ref name="Norw. 115">Luftwaffe.no: [http://www.luftwaffe.no/SIG/Artikler/115.html Heinkel He 115 in Norway] {{en icon}}</ref> this escape route was not possible and flying to Finland was left as the only option for evacuation.<ref name="Kjæraas30">Kjæraas 2003: 30</ref>

On 8 June 1940 M.F. 11s F.310, F.336 and F.346 landed on Salmijärvi Lake in [[Pechengsky District|Petsamo]], immediately being put in internment by Finnish authorities.  The three Norwegian aircraft were first stored and repaired at both the [[Finnish Polytechnic School]] and the [[Valtion lentokonetehdas|Finnish State Aircraft Factory]] before being handed over to the [[No. 15 Squadron, Finnish Air Force|Lentolaivue 15]] in August 1941, with the designation numbers NK-171—173. All three M.F. 11s were fitted with shackles for 200&nbsp;kg depth charges. In addition to normal maintenance NK-172 also received a new engine and propeller.<ref name="Kjæraas30"/>

During the autumn of 1941 the aircraft carried out around 20 reconnaissance and propaganda missions in the [[Lake Ladoga]] area before ice conditions put them into winter storage.<ref name="Kjæraas30"/> The aircraft were also used during the [[Continuation War]] to support [[Long Range Reconnaissance Patrol|long-range patrols]] behind the Soviet lines.<ref name="Hafsten333">Hafsten 2005: 333</ref>

For the summer of 1942 the three aircraft were handed over to [[No. 6 Squadron, Finnish Air Force|Lentolaivue 6]] and flew [[Anti-submarine warfare|anti-submarine]] missions over the [[Baltic Sea]], flying from [[Mariehamn]], [[Åland]]. At two occasions during the summer months depth charges were dropped at Soviet submarines, without any observable results.<ref name="Kjæraas30"/>

During the summers of 1943 and 1944 similar types of missions were flown over the Baltic but, at least in part due to an anti-submarine net having been positioned across the [[Gulf of Finland]], no submarines were spotted. The Baltic missions ended on 21 August 1944. Following the [[Moscow Armistice]] on 4 September 1944, the M.F. 11s were sent to Detachment Jauri<ref>Finnish Air Force Aircraft - Maritime, Short-Range Reconnaissance and Transport Planes
1939 - 1945: [http://www.webcitation.org/query?url=http://www.geocities.com/finnmilpge/fmp_faf_searecon.html&date=2009-10-25+22:13:28 Høver M.F. 11]</ref> in the far north of Finland to participate in the [[Lapland War]],<ref name="Hafsten333"/>  flying around 60 transport missions in October. In November 1944 the M.F. 11s were put into permanent storage.<ref name="Kjæraas30"/>

In Finnish service the M.F. 11s were given the designation numbers NK-171—173, after the Finnish abbreviation for "Norwegian Machine" ("''norjalainen<ref>English-Finnish-English Dictionary: [http://efe.scape.net/index.php?criteria=norjalainen&page=1 norjalainen]</ref> kone<ref>English-Finnish-English Dictionary: [http://efe.scape.net/index.php?criteria=kone&page=1 kone]</ref>''").

Two of the Finnish-operated M.F. 11s were offered for sale to Norway in 1948 and 1950, but no one showed any interest to acquire them before they were scrapped.<ref name="Kjæraas30"/>

==German-operated M.F. 11s==
During and after the Norwegian Campaign the German invasion forces captured perhaps as many as 16 Norwegian M.F. 11s. The aircraft were seized in repair shops (F.302 and F.318), abandoned at [[Drøbak]] (F.3204, F.308 and F.338), at [[Flatøy]] near [[Bergen]] (F.322), at [[Sola Air Station]] (F.324) and at Skattøra Air Station in [[Tromsø]] (F.312, F.334 and F.344). F.314 II and F.326 disappeared from records, but may have been used by the Germans. Also captured were F.348, F.350, F.352 and F.354, still under construction at Horten.<ref name="Kjæraas03 21"/>

The captured aircraft were probably used for communications and mail flights in support of the [[Occupation of Norway by Nazi Germany|German occupation of Norway]], as well as fishery supervision. Records from the German aircraft company Walther Bachman Flugzeugbau shows that operational M.F. 11s were still accepted for maintenance as late as February 1942.<ref name="Kjæraas03 21"/>

During their German careers the aircraft were operated in an area stretching from [[Finnmark]] in the north of Norway to [[Warnemünde]] in the north east of Germany.<ref>Kjæraas 2003: 23</ref>

==Users==
; {{FIN}}: 
* [[Finnish Air Force]] - (Three aircraft) Norwegian aircraft that were interned in June 1940 after evacuating from [[North Norway]] to Petsamo<ref name="Kjæraas30"/>
; {{flag|Nazi Germany|name=Germany}}: 
* [[Luftwaffe]] - Up to 16 captured Norwegian aircraft used to support the German occupation of Norway<ref name="Kjæraas03 21"/>
; {{NOR}}:
* [[Royal Norwegian Navy Air Service]] - (25 aircraft)<ref name="Kjæraas03 3"/>

==Specifications (Finnish Air Force Høver M.F. 11)==
{{Aircraft specifications

<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->

<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). 
If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")" and start a new, fully formatted line beginning with * -->
|ref=[http://www.sci.fi/~ambush/faf/fafincolor.html FAF in Color:]
|crew=3
|capacity=
|length main=11.72 m
|length alt=38 ft 5 in
|span main=15.4 m
|span alt=50 ft 6 in
|height main=4.45 m
|height alt=14 ft 7 in
|area main=53.5 m²
|area alt=575.9 ft²
|airfoil=
|empty weight main=1850 kg
|empty weight alt=4,070 lb
|loaded weight main=
|loaded weight alt=
|useful load main=
|useful load alt=
|max takeoff weight main=2850 kg
|max takeoff weight alt=6,285 lb
|more general=
|engine (prop)=[[Armstrong Siddeley Panther]] IIA
|type of prop=14-cylinder, air-cooled, two-row [[radial engine]]
|number of props=1
|power main=430 kW
|power alt=575 hp
|power original=
|max speed main=235 km/h
|max speed alt=127 knots, 146 mph
|cruise speed main=170 km/h
|cruise speed alt=92 knots, 106 mph
|range main=800 km
|range alt=432 nm, 496 mi
|ceiling main=5,000 m
|ceiling alt=16,405 ft
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
*1× [[.303 British|7.7 mm (0.303 in)]] [[Vickers machine gun]]
*1× 200 kg (440 lb) [[depth charge]]
|avionics=
}}

==See also==
{{Aircontent
|related=
|similar=
|sequence=
|lists=
* [[List of aircraft of World War II]]
*[[List of military aircraft of Norway]]
}}

==Notes==
{{reflist|2}}

==Bibliography==
* {{cite book |last=Hafsten |first=Bjørn |first2=Tom |last2=Arheim |title=Marinens Flygevåpen 1912&ndash;1944 |year=2003 |publisher=TankeStreken AS |location=Oslo |isbn=82-993535-1-3 |language=Norwegian }}
* {{cite book|last=Hafsten |first=Bjørn |author2=Ulf Larsstuvold |author3=Bjørn Olsen |author4=Sten Stenersen  |title=Flyalarm &ndash; luftkrigen over Norge 1939&ndash;1945 |edition=2nd, revised |publisher=Sem og Stenersen AS |location=Oslo| date=2005 |isbn=82-7046-074-5 |language=Norwegian}}
* Heinonen, Timo: ''Thulinista Hornetiin'', 1992, Keski-Suomen ilmailumuseo, ISBN 951-95688-2-4 {{fi icon}}
* Kjæraas, Arild (ed.): {{lang|no|''Profiles in Norway no. 2: Høver M.F. 11'', Profiles in Norway, Andebu 2003}} {{en icon}}&{{no icon}}
* Sivertsen, Svein Carl (ed.): {{lang|no|''Jageren Sleipner i Romsdalsfjord sjøforsvarsdistrikt april 1940'', Sjømilitære Samfund ved Norsk Tidsskrift for Sjøvesen, Hundvåg 1999}} ISBN 82-994738-3-7 {{no icon}}

{{Marinens Flyvebaatfabrikk aircraft}}

[[Category:Norwegian military reconnaissance aircraft 1930–1939]]
[[Category:Marinens Flyvebaatfabrikk aircraft|MF11]]
[[Category:Floatplanes]]
[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Propeller aircraft]]