{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
|name = S.8 Calcutta
|image = Short Calcutta.jpg
|caption = View shows the pilot in the open cockpit.
}}{{Infobox Aircraft Type
|type = [[Biplane]] [[airliner]] [[flying boat]]
|manufacturer = [[Short Brothers]]
|designer = 
|first flight = 14 February 1928
|introduced = [[1928 in aviation|1928]]
|retired = 
|status = 
|primary user = [[Imperial Airways]]
|more users = <!--up to three more. please separate with <br/>.-->
|produced = 
|number built = 7
|unit cost = 
|developed from=[[Short Singapore]]
|variants with their own articles = [[Short Rangoon]]<br>[[Short Kent]]<br>[[Breguet 521]]
}}
|}

The '''Short Calcutta''' or '''S.8''' was a civilian [[biplane]] [[airliner]] [[flying boat]] made by [[Short Brothers]].

==Design and development==
The Calcutta biplane flying boat originated from an [[Imperial Airways]] requirement to service the Mediterranean legs of its services to and from India. Derived from the [[Short Singapore]] military flying boat, the Calcutta was noteworthy for being the first [[stressed skin]], metal-hulled flying boat. It was equipped with three [[Bristol Jupiter]] engines mounted between the wings. The two pilots flew the plane from an open cockpit while the [[radio operator]] shared the main cabin with 15 passengers.

==Operational history==
The S.8 Calcutta made its first flight on 14 February 1928, having been launched the previous day and left at its mooring overnight to assess the hull for signs of leakage. Shorts' Chief [[Test pilot|Test Pilot]], [[John Lankester Parker]] was at the controls, with [[Major]] [[Herbert G. Brackley]] of Imperial Airways as co-pilot. On 15 March 1928, this aircraft (registered as ''G-EBVG'') was delivered by Parker and Brackley to the [[Marine Aircraft Experimental Establishment]], Felixstowe, for its [[Certificate of Airworthiness|airworthiness]] and sea handling checks; these were successfully completed on 27 July of the same year and the aircraft was flown back to Shorts on the same day.<ref name="Barnes and James, p. 227">Barnes and James, p. 227.</ref> ''G-EBVG'' was handed over to Imperial Airways on 9 August 1928.<ref>Barnes and James, p. 228.</ref>

The S.8 Calcutta was introduced in 1928 and was used by Imperial Airways flying the [[Mediterranean]]-to-[[Karachi]] leg of the [[UK|Britain]]-to-[[India]] route.

A total of seven aircraft were built. A military version of the Calcutta, originally known as the ''Calcutta (Service type)'', was built as the '''[[Short Rangoon]]'''. In [[1924 in aviation|1924]], a Calcutta was purchased by the [[France|French]] [[Breguet Aviation|Breguet Company]] from which they developed a military version for the French Navy known as the '''Breguet S.8/2''', which was similar to the Rangoon version. Four aircraft were built under licence by Breguet at Le Havre. Breguet later developed an improved version, the [[Breguet 521|Breguet 521 Bizerte]].

On 1 August 1928, Parker, accompanied by [[Oswald Short]], flew ''G-EBVG'' to [[Westminster]], setting it down on the [[Thames]] between [[Vauxhall Bridge|Vauxhall]] and [[Lambeth Bridge|Lambeth]] Bridges; it was moored off the [[Albert Embankment]] for three days for inspection by [[Members of Parliament]] (including the then [[Chancellor of the Exchequer]], [[Winston Churchill]]), members of the [[House of Lords]] and others.<ref name="Barnes and James, p. 227"/><ref>[http://www.bamuseum.com/museumhistory20-30.html "History of the British Airways Museum."] ''British Airways Archive and Museum Collection''. Retrieved: 1 February 2007.</ref>

==Accidents and incidents==

The ''City of Rome'' (registration G-AADN) made a forced landing in high winds and poor weather in the [[Ligurian Sea]] off [[La Spezia]], [[Italy]], during a scheduled passenger flight from [[Naples International Airport]] in [[Naples]], Italy, to [[Genoa Cristoforo Colombo Airport]] outside [[Genoa]], Italy, on 26 October 1929. It sank during efforts to tow it to shore, killing all seven people on board.<ref>[http://aviation-safety.net/database/record.php?id=19291026-0 Aviation Safety Network: Accident Description]</ref>

The ''City of Khartoum'' (G-AASJ) suffered a catastrophic failure of all three engines shortly before the end of its flight between [[Crete]] and [[Alexandria, Egypt|Alexandria]], [[Egypt]], just after nightfall on 31 December 1935. The pilot was the only survivor; nine passengers and three crew were killed either by the impact with water or by drowning when the aircraft was overwhelmed by heavy waves. An inquiry by the British Air Ministry found that the [[carburetor|carburettors]] had been altered in a way which increased fuel consumption, resulting in the aircraft running out of fuel.<ref>"Report of the investigation of the accident to the aircraft G-AASJ "City of Khartoum" off Alexandria on the 31st of December, 1935" ([[Command paper|Cmd. 5220]]), HMSO, 1936.</ref>

==Operators==
;{{UK}}
*[[Imperial Airways]]

==Specifications==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref= British Civil Aircraft 1919-1972:Volume III <ref name="Jackson V3 p140">Jackson 1988, p. 140.</ref> 
|crew= three
|capacity=15
|payload main= 
|payload alt=
|length main= 66 ft 9 in
|length alt= 20.35 m
|span main= 93 ft 0 in
|span alt= 28.35 m
|height main= 23 ft 9 in
|height alt= 7.24 m
|area main= 1,825 ft²
|area alt= 170 m²
|airfoil=
|empty weight main= 13,845 lb
|empty weight alt= 6,293 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 22,500 lb
|max takeoff weight alt= 10,227 kg
|more general=
|engine (prop)=[[Bristol Jupiter]] IXF 
|type of prop= radial engine
|number of props= 3
|power main= 540 hp
|power alt= 403 kW
|power original=
|max speed main= 118 mph <ref name="Barnes p233">Barnes and James 1989, p. 233.</ref> 
|max speed alt= 103 kn, 190 km/h
|cruise speed main= 97 mph
|cruise speed alt= 84 kn, 156 km/h
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 650 mi
|range alt= 565 [[nautical mile|nmi]], 1,046 km
|ceiling main= 13,500 ft
|ceiling alt= 4,120 m
|climb rate main= 750 ft/min
|climb rate alt= 3.8 m/s
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Short Cromarty]]
*[[Felixstowe F.5|Short S.2]]
*[[Short Singapore]]
*[[Saunders Severn]]
*[[Short Rangoon]]
*[[Short Kent]]
*[[Kawanishi H3K]]
*[[Breguet 521|Breguet 521 Bizerte]]
<!-- aircraft similar in appearance or function to this design: -->
|similar aircraft=
<!-- the manufacturer or operator (military etc) sequence this aircraft belongs in: -->
<!-- any lists that are appropriate: -->
|lists=
<!-- other articles that could be useful to connect with: -->
|see also=
}}

==References==
;Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
* Barnes C.H. and D.N. James. ''Shorts Aircraft since 1900''. London: Putnam, 1989. ISBN 0-85177-819-4.
*''The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)''. London: Orbis Publishing, 1985.
* Jackson, A.J. ''British Civil Aircraft 1919-1972: Volume III''. London: Putnam, 1988. ISBN 0-85177-818-6.
{{refend}}

==External links==
{{Commons category}}
*[http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%201145.html Short "Calcutta"], Flight, 3 October 1930

{{Short Brothers aircraft}}

[[Category:British airliners 1920–1929]]
[[Category:Flying boats]]
[[Category:Short Brothers aircraft|Calcutta, S.08]]
[[Category:Trimotors]]
[[Category:Biplanes]]