{{use dmy dates|date=July 2016}}
{{use Indian English|date=July 2016}}
{{Infobox legislation
| imagesize = 100
| enacted_by = [[Parliament of India]]
| status = In Force
}}

The '''Hindu Adoptions and Maintenance Act''' was enacted in [[India]] in 1956 as part of the [[Hindu]] Code Bills. The other [[legislation]]s enacted during this time include the Hindu Marriage Act (1955), the Hindu Succession Act (1956), and the Hindu Minority and Guardianship Act (1956). All of these acts were put forth under the leadership of [[Jawaharlal Nehru]], and were meant to [[Codification (law)|codify]] and standardise the current Hindu legal tradition. The Adoptions and Maintenance Act of 1956 dealt specifically with the legal process of [[adoption|adopting]] children by a Hindu adult, and with the legal obligations of a Hindu to provide "maintenance" to various family members including their wife or wives, parents, and in-laws.

==Application==
This act applies to Hindus and all those considered under the umbrella term of Hindus, which includes:
* a Hindu by religion in any of its forms or development;
* a [[Buddhist]], [[Jain]] or [[Sikh]];
* a child legitimate or [[illegitimate child|illegitimate]] whose parents are Hindus, Buddhists, Jains or Sikhs;
* a child legitimate or illegitimate one of whose parents are Hindus, Buddhists, Jains or Sikhs and has been so brought up;
* an abandoned child, legitimate or illegitimate of unknown parentage brought up as a Hindu, Buddhist, etc.; and
* a convert to the Hindu, Buddhist, Jain or Sikh religion.

Persons who are [[Muslims]], [[Christians]], [[Parsis]] or [[Jews]] are excluded from this definition.

The act does not also apply to adoptions that took place prior to the date of enactment. However, it does apply to any marriage that has taken place before or after the Act had come into force. Moreover, if the wife is not a Hindu then the husband is not bound to provide maintenance for her under this Act under [[modern Hindu Law]].<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref>

==Adoptions==
===Who can adopt?===
Under this act only Hindus may adopt subject to their fulfilment of certain criteria. 
The first of these asserts that the adopter has the legal right to (under this Act that would mean they are a Hindu). Next, they have to have the capacity to be able to provide for the adopted child. 
Thirdly the child must be capable of being adopted. 
Lastly, compliance with all other specifications (as outlined below) must be met to make the adoption valid.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/hinduadoptionsact.htm</ref>

Men can adopt if they have the [[consent]] of their wife or of all of their wives. 
The only way of getting around obtaining the permission of the wife or of the wives is if she or if they are unsound, if they have died, if they have completely and finally renounced the world, and if they have ceased to be a Hindu. Men who are unmarried can adopt as well as long as they are not a minor. However, if a man were to adopt a daughter, the man must be twenty one years of age or older.<ref>http://indiacode.nic.in/fullact1.asp?tfnm=195678</ref>

Only unmarried Hindu women can legally adopt a child. 
A married woman can only give her consent to adoption by her husband. A married woman whose husband adopts a child is to be considered the mother. <ref>http://indiacode.nic.in/fullact1.asp?tfnm=195678</ref> If the child is adopted and there are more than one wife living in the household, then the senior wife is classified as the legal mother of the adopted child.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsa</ref>

===Who can be adopted?===
The adopted child can be either male or female. The adopted child must fall under the Hindu category. The adoptee also needs to be unmarried; however, if the particular custom or usage is applicable to the involved parties then the adoptee can be married. The child cannot be the age of sixteen or older, unless again it is custom or the usage is applicable to the involved parties. An adoption can only occur if there is not a child of the same sex of the adopted child still residing in the home. In particular, if a son were to be adopted then the adoptive father or mother must not have a legitimate or adopted son still living in the house.<ref>http://indiacode.nic.in/fullact1.asp?tfnm=195678</ref>

===Legal implications for an adopted child===
From the date of the adoption, the child is under the legal [[guardianship]] of the new adopted parent(s) and thus should enjoy all the benefits from those family ties. This also means that this child, therefore, is cut off from all legal benefits ([[property]], [[inheritance]], etc.) from the family who had given him or her up for adoption.<ref>http://indiacode.nic.in/fullact1.asp?tfnm=195678</ref>

==Maintenance==
===Maintenance of a wife===
A Hindu wife is entitled to be provided for by her husband throughout the duration of her lifetime per Section 18 of HAMA '56.{{Citation needed|date=November 2016}} Regardless of whether the marriage was formed before this Act was instated or after, the Act is still applicable. The only way the wife can null her maintenance is if she renounces being a Hindu and converts to a different religion, or if she commits adultery.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref>

The wife is allowed to live separately from her husband and still be provided for by him. This separation can be justified through a number of different reasons, including if he has another wife living, if he has converted to a different religion other than Hinduism, if he has treated her cruelly, or even has a violent case of leprosy.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref>

If the wife is [[widow]]ed by her late husband, then it is the duty of the father-in-law to provide for her. This legal obligation only comes into effect if the widowed wife has no other means of providing for herself. If she has land of her own, or means of an income and can maintain herself then the father-in-law is free from obligation to her. Additionally, if the widow remarries then her late husband's father-in-law does is not legally bound by this Act anymore as well.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref>

===Maintenance of a child or of aged parents===
Under this Act, a child is guaranteed maintenance from his or her parents until the child ceases to be a [[minor (law)|minor]]. This is in effect for both legitimate and illegitimate children who are claimed by the parent or parents. Parents or infirmed daughters, on the other hand, must be maintained so long as they are unable to maintain for themselves.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref> Sections 20-22 of the Act cover the same, and provide for the maintenance of any dependents of an individual.

<!-- Image with unknown copyright status removed: [[Image:elderly Hindu couple abadoned by son - November 5, 2005.jpg|thumb|left|Elderly Hindu couple abandoned by their son and entitled to maintenance]] -->

===Amount of maintenance provided===
The amount of maintenance awarded, if any, is dependent on the discretion of the courts. Particular factors included in the decision process include the position or status of the parties, the number of persons entitled to maintenance, the reasonable wants of the claimants, if the claimant is living separately and if the claimant is justified in doing so, and the value of the claimant's estate and income. If any debts are owed by the deceased, then those are to be paid before the amount of maintenance is awarded or even considered.<ref>http://www.vakilno1.com/bareacts/hinduadoptionsact/s18.htm</ref>

==References==
{{Reflist}}

{{Adopt}}
{{Indian legislations}}

[[Category:Adoption law]]
[[Category:1956 in law]]
[[Category:Indian family law]]