{{italictitle}}
{{Unreferenced|date=December 2009}}
[[File:Round the Sofa.jpg|thumb|right|200px|First edition title page from ''Round the Sofa''; Vol 1 contains ''My Lady Ludlow'' in its entirety.]]
'''''My Lady Ludlow''''' is a novel (over 77,000 words in the [[Project Gutenberg]] text) by [[Elizabeth Gaskell]]. It appeared in the magazine ''[[Household Words]]'' in 1858, and was republished in ''[[Round the Sofa]]'' in 1859, with framing passages added at the start and end.  

It recounts the daily lives of the widowed Countess of Ludlow of Hanbury and the spinster Miss Galindo, whose father was a Baronet, and their caring for other single women and girls. It is also concerned with Lady Ludlow's man of business, Mr Horner, and a poacher's son named Harry Gregson whose education he provides for.

==TV Adaptation==
With ''[[Cranford (novel)|Cranford]]'', ''[[The Last Generation in England]]'' and ''[[Mr. Harrison's Confessions]]'', ''My Lady Ludlow'' was adapted for television in 2007 as ''[[Cranford (2007 TV  series)|Cranford]]'', with [[Francesca Annis]] as the eponymous character, with [[Alex Etel]] as Harry Gregson and [[Emma Fielding]] as Laurentia Galindo. Mr Horner's name was changed to Mr Carter, and was played by [[Philip Glenister]]. The character of Lord Septimus, the Countess' seventh child, is mentioned in the first series as he is in the novel. The book, however, was extended in the first episode of the second series ''[[Return to Cranford]]'', featuring the death of Lady Ludlow from bone cancer, and Lord Septimus' return from Italy to claim his estate, where his ne'er-do-well personality is revealed. In the episode, Lord Septimus was portrayed by [[Rory Kinnear]], with Annis, Etel and Fielding reprising their roles.

==External links==
* {{citenews|first=Jenny|last=Uglow|work=The Guardian|title= Band of women|url=http://books.guardian.co.uk/review/story/0,,2204287,00.html|date=3 November 2007|accessdate=2007-11-06 }}
* [http://www.informaworld.com/smpp/content~content=a758352784~db=all~jumptype=rss Susan Colon, Professional Frontiers in Elizabeth Gaskell's My Lady Ludlow] (Women's Writing, Volume 13, Issue 3 October 2006, pages 475 - 494)
* {{Gutenberg|no=2524|name=My Lady Ludlow}}
* {{librivox book | title=My Lady Ludlow | author=Elizabeth Cleghorn GASKELL}}

{{Elizabeth Gaskell}}

[[Category:Novels by Elizabeth Gaskell]]
[[Category:1858 novels]]
[[Category:Novels adapted into television programs]]
[[Category:Works originally published in Household Words]]


{{1850s-novel-stub}}