{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Twin Blackburn
 |image=Blackburn Twin Blackburn.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Anti-[[Zeppelin]] fighter
 |manufacturer=[[Blackburn Aircraft|Blackburn]]
 |designer=
 |first flight=August 1915
 |introduced=1916
 |retired=1917
 |status=
 |primary user= [[Royal Naval Air Service]]
 |more users=
 |produced=
 |number built=9
 |variants with their own articles=
}}
|}

The '''Blackburn TB''' (for "Twin Blackburn") was one of the most specialized aircraft ever constructed - a long-range twin-engined anti-[[Zeppelin]] [[seaplane]]. It was [[Blackburn Aircraft|Blackburn]]'s first multi-engine aircraft to fly.

==Design and development==
The first attacks by [[Germany|German]] bombing airships on the United Kingdom in the winter of 1914-15 resulted in the British [[Admiralty]] issuing a requirement for a two-seat aircraft with long endurance to attack [[Zeppelin]]s by dropping [[Incendiary device|incendiary]] [[Ranken dart]]s onto the airships in the hope of igniting their gas envelopes, with an order for nine '''Blackburn TB'''s being placed in March [[1915 in aviation|1915]].<ref name="mason bomber p42">Mason 1994, p. 42.</ref>

The TB was a seaplane with twin [[fuselage]]s, situated 10&nbsp;ft (3.35&nbsp;m) apart, with the pilot in one fuselage and the observer in the other, having no means of communication other than hand signals.<ref name="mason bomber p43">Mason 1994, p. 43.</ref> Each of the twin fuselages was a wooden structure with fabric covering, with a tractor engine in front of each fuselage.<ref name ="mason bomber p43"/> It had fabric-covered, wooden [[Stagger (aviation)|unstaggered]], unswept and unequal-span wings. The upper wing extension was [[Flying wires|wire braced]] to steel pylons above the wing. The Blackburn had [[aileron]]s on the upper wings only.<ref name ="mason bomber p43"/><ref name="Jackson black p94">Jackson 1968, p. 94.</ref>

Although it was intended that the TB would be powered by two 150&nbsp;hp (112&nbsp;kW) [[Smith Static]] [[radial engines]], which promised low weight and good fuel consumption, these proved to be unsatisfactory, and 100&nbsp;hp (75&nbsp;kW) [[Gnome Monosoupape 9 Type B-2]] had to be substituted, this allowing the first aircraft to fly in August 1915.<ref name="mason bomber p42-3">Mason 1994, pp. 42–43.</ref>

Testing showed that the two Gnome engines gave insufficient power, with the aircraft being unable to climb above 8,000&nbsp;ft (2,438&nbsp;m) carrying the required three canisters of 24 1&nbsp;lb (.5&nbsp;kg) Ranken darts. The weapon load was therefore reduced to two canisters.<ref name ="mason bomber p43"/>

The ninth (and final) TB was fitted with 110&nbsp;hp (80&nbsp;kW) [[Clerget]] engines with the hope of improving performance. This modification was found to have little effect.<ref name ="mason bomber p43"/>

==Operational history==
Seven TBs were delivered to the Royal Naval Air Service. They did not see much action, four serving for a short time at [[RNAS Killingholme]] in 1917.<ref name="mason bomber p43"/> All seven, together with the remaining two that were held in store, were broken up in 1917.<ref name="Jackson Black p96-7">Jackson 1968, pp 96–97.</ref>

==Operators==
;{{UK}}
*[[Royal Naval Air Service]]

==Specifications (TB)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=The British Bomber since 1914 <ref name ="mason bomber p43"/>
|crew=Two
|capacity=
|length main= 36 ft 6 in
|length alt= 11.13 m
|span main= 60 ft 6 in
|span alt= 18.45 m
|height main= 13 ft 6 in
|height alt= 4.12 m
|area main= 585 ft²
|area alt= 54.4 m²
|airfoil=
|empty weight main= 2,310 lb
|empty weight alt= 1,050 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 3,500 lb
|max takeoff weight alt= 1,591 kg
|more general=
|engine (prop)=[[Gnome Monosoupape 9 Type B-2]] 
|type of prop=[[rotary engine]]
|number of props=2
|power main= 100 hp
|power alt= 75 kW
|power original=
|max speed main= 75 kn
|max speed alt= 86 mph, 138 km/h
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 
|range alt= 
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight
|power/mass alt= 
|more performance=*'''Endurance:''' 4 hours
*'''Climb to 5,000 ft (1,520 m):''' 12 min
|armament=3 × canisters, each with 24 × 1 lb (.5 kg) Ranken incendiary darts (intended)
|avionics=
}}

==References==
{{Reflist}}

==Sources==
*{{cite book|last= Jackson|first= A.J.|title= Blackburn Aircraft since 1909 |year=1968 |publisher=Putnam |location=London |isbn= 0-370-00053-6|page= |pages= |url= |accessdate=}}
*{{Cite book |author=Mason, Francis K |title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books |year=1994 |isbn= 0-85177-861-5}}

==External links==
{{commons category|Blackburn Aircraft}}
*[https://web.archive.org/web/20070930184052/http://www.britishaircraft.co.uk/aircraftpage.php?ID=423 Blackburn T.B.] at the British Aircraft Directory

{{Blackburn aircraft}}

[[Category:Blackburn aircraft|Twin Blackburn]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Floatplanes]]
[[Category:Twin-fuselage aircraft]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Rotary-engined aircraft]]
[[Category:Biplanes]]