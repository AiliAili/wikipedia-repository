{{Use mdy dates|date=May 2014}}
{{Cleanup|article|reason=The information in some sections is years out of date|date=July 2013}}
{{Infobox college athletics
| name =  Miami Hurricanes
| logo = Miami_Hurricanes_logo.svg
| logo_width = 200
| university = [[University of Miami]]
| association = NCAA
| conference = [[Atlantic Coast Conference]]
| director = Blake James
| division = [[NCAA Division I|Division I]]
| city = [[Coral Gables, Florida|Coral Gables]]
| state = [[Florida]]
| stadium = [[Hard Rock Stadium]]
| basketballarena = [[Watsco Center]]
| baseballfield = [[Mark Light Field at Alex Rodriguez Park]]
| soccerstadium = [[Cobb Stadium]]
| natatorium = [[Whitten University Center Pool]]
| mascot = [[Sebastian the Ibis]]
| nickname = Hurricanes
| fightsong = Hail to the Spirit of Miami U, Miami U How-Dee-Do<ref>{{cite web|url=http://www.miami.edu/index.php/ug/connect/downloads/|title=Sounds of the "U" |work=Miami.edu |publisher=University of Miami|accessdate=October 9, 2009}}</ref>
| pageurl = http://www.hurricanesports.com/
}}

The '''Miami Hurricanes''' (known informally as '''UM''' or '''UMiami''' or '''The U''') are the [[varsity team|varsity sports teams]] of the [[University of Miami]], located in the [[Coral Gables, Florida|Coral Gables]] suburb of [[Miami metropolitan area|Miami]], Florida. They compete in the Coastal Division of the [[Atlantic Coast Conference]] (ACC). The university fields 15 [[college athletics|athletic]] teams for 17 varsity sports.<ref>{{cite web|url=http://hurricanesports.cstv.com/ |title=hurricanesports.com – University of Miami Official Athletic Site |publisher=Hurricanesports.cstv.com |date= |accessdate=October 31, 2010}}</ref> Men's teams compete in [[college baseball|baseball]], [[college basketball|basketball]], [[cross country running|cross-country]], [[diving]], [[college football|football]], [[tennis]], and [[track and field]].  Women's teams compete in basketball, cross-country, [[swimming (sport)|swimming]] and diving, [[golf]], [[College rowing|rowing]], [[college soccer|soccer]], tennis, track and field, and [[volleyball]]. UM has approximately equal participation by male and female varsity athletes in these sports.<ref name=DOE>{{cite web|title=Dept of Education Title IX Compliance Survey|url=http://surveys.ope.ed.gov/athletics/DataForms.aspx |accessdate=September 18, 2009}}</ref>

Team colors are green, orange, and white.  The school mascot is [[Sebastian the Ibis]]. The [[ibis]] was selected as the school's mascot because, according to university legend, it is the last animal to flee an approaching [[hurricane]] and the first to reappear after the storm, making it a symbol of leadership and courage. The school's athletics logo is a simple green and orange, color of an orange tree, letter "U."  The school's [[marching band]] is the [[Band of the Hour]].

Aside from being an independent in baseball, the Hurricanes were a full member of the [[Big East Conference (1979–2013)|Big East Conference]] from 1991 to 2004. In 2004, the school became a member of the [[Atlantic Coast Conference]] (ACC).

==Rivalries==
Miami's traditional [[college rivalry#United States|athletic rivals]] include the [[Florida State Seminoles|Seminoles]] of [[Florida State University]]<ref>{{cite web|url=http://www.miamibeach411.com/news/index.php?/news/comments/hurricanes-vs-seminoles/|title=Hurricanes Vs. Seminoles Will Maintain Usual Intensity – Miami Beach 411|accessdate=September 6, 2009|date=September 3, 2009}}</ref> and the [[Florida Gators|Gators]] of the [[University of Florida]].<ref>{{cite web|url=http://www.palmbeachpost.com/sports/content/sports/epaper/2009/03/19/0319nit.html|title=Florida Gators, Miami Hurricanes bring football rivalry to basketball court|accessdate=September 6, 2009|date=March 19, 2009}}</ref> The Hurricanes have played more football games against the Seminoles (60) than against any other opponent (Florida being second with 55 games); the series began in 1951, and has been played annually since 1969.  The teams' only bowl meeting was the [[2004 Orange Bowl]], prior to Miami leaving the Big East Conference to join the Atlantic Coast Conference (in which Florida State has been a member since 1992.)  As of the 2016 meeting, the Hurricanes hold a 31–30 series lead against the Seminoles.<ref>http://mcubed.net/ncaaf/series/miaf/flast.shtml</ref>

The Hurricanes first played the Gators in football in 1938; and the teams played annually (except in 1943 when Florida didn't field a team due lack of players due to WWII) until 1987.  Since then, Miami and Florida have met only six times (four during the regular season in 2002, 2003, 2008, and 2013, and in two bowl games: the 2001 [[Sugar Bowl]] and the 2004 [[Chick-fil-A Bowl]]). The Hurricanes and the Gators last scheduled meeting was the game in Miami on September 7, 2013.  As of the fall of 2013, Miami holds a 29–26 series lead against Florida.<ref>"University of Miami All-Time Football Record Book", updated November 29, 2011; link: http://issuu.com/miamihurricanes/docs/umfootbalrecordbook | accessdate = July 27, 2012</ref><ref>http://mcubed.net/ncaaf/series/miaf/fla.shtml</ref>

==Teams==
The University of Miami sponsors teams in seven and a half* men's and ten women's [[NCAA]] sanctioned sports:<ref>http://www.hurricanesports.com/</ref>
{{col-start}}{{col-2}}
'''Men's Intercollegiate Sports'''
* [[College Baseball|Baseball]]
* [[College Basketball|Basketball]]
* [[Cross country running|Cross Country]]
* [[Diving]] *
* [[College football|Football]]
* [[Tennis]]
* [[Track & Field]] ([[Track & Field#Indoor|Indoor]] & [[Track & Field#Outdoor|Outdoor]])
{{col-2}}
'''Women's Intercollegiate Sports'''
* [[College Basketball|Basketball]]
* [[Cross country running|Cross Country]]
* [[Golf]]
* [[College rowing (United States)|Rowing]]
* [[College soccer|Soccer]]
* [[Swimming (sport)|Swimming]] & [[Diving]]
* [[Tennis]]
* [[Track & Field]] ([[Track & Field#Indoor|Indoor]] & [[Track & Field#Outdoor|Outdoor]])
* [[Volleyball]]
{{col-end}}

* * = Under the NCAA's "gender equity" provisions for balancing the number of athletic scholarships awarded to men and women at schools sponsoring football, Miami not only fields more teams for women than for men, but it has only a diving team for men, while having a swimming and diving team for women; with the reduction in scholarships, the men's diving team is counted as only half of a team by the NCAA.

===Baseball===
[[File:Mark Light1.jpg|thumb|right|250px|The home venue for [[Miami Hurricanes baseball]] is [[Mark Light Field at Alex Rodriguez Park]] on the campus of the [[University of Miami]].]]
{{Main article|Miami Hurricanes baseball}}
{{See also|1982 College World Series|1985 College World Series|1999 College World Series|2001 College World Series}}
UM has won four national championships (1982, 1985, 1999 and 2001) and reached the  [[College World Series]] 22 times in the 34 seasons since 1974. Five UM graduates are currently active on MLB teams.{{Citation needed|date=November 2009}}

The team is currently coached by [[Jim Morris (baseball coach)|Jim Morris]], the former head coach of the [[Georgia Tech Yellow Jackets baseball|Georgia Tech baseball team]].  Former coach [[Ron Fraser]] was inducted into the [[College Baseball Hall of Fame]] in July 2006.  The team plays its games on the UM campus, in [[Alex Rodriguez Park at Mark Light Field]]. Morris' contract as coach has been extended through 2015. Morris has established a record of 850–344–3 (.711) in 19 seasons at Miami. His teams reached the College World Series in his first six seasons at UM, an NCAA record.<ref>{{cite web|url=http://hurricanesports.cstv.com/sports/m-basebl/spec-rel/092009aab.html|title=Morris Signs Contract Extension at Miami Through 2015|date=September 20, 2009|accessdate=September 21, 2009}}</ref> The Mascot for the baseball team is The Miami Maniac.

Miami holds the longest consecutive post season appearance streak at 44 consecutive years (1973–2016).  This streak is the longest of any men's NCAA Div. 1 major sport, topping the football post season streak of 35 seasons (Nebraska 1972–2006) and the basketball streak of 27 seasons (North Carolina 1974–2001).

===Men's basketball===
[[File:BankUnited Center.JPG|thumb|The [[Watsco Center]] on the [[University of Miami]] campus is the home arena of the Hurricanes' [[Miami Hurricanes men's basketball|men's]] and women's basketball teams.]]
{{main article|Miami Hurricanes men's basketball}}
The Miami Hurricanes men's basketball team has produced three players who are currently on [[National Basketball Association|NBA]] rosters. [[Rick Barry]], who played his collegiate basketball at UM, is a member of the [[Basketball Hall of Fame]]. Barry is the Hurricanes' only consensus All-American in basketball and led the nation in scoring his senior year with a 37.4 average during the 1964–65 campaign.  The team plays its home games at the [[Watsco Center]] on the University of Miami's Coral Gables campus.

The Board of Trustees attempted to shut the program down in the middle of the 1970 season, which forced [[Will Allen (urban farmer)|Will Allen]] to organize his teammates and strike because it was not sufficient notice for the players to transfer schools. They held a press conference and this caught the attention of the national press, and the university actually dropped the program after the 1971 season,<ref name="Biography Today">{{Cite book|last=|first=|title=Biography Today|year=2009|page=9|publisher=Omnigraphics|location=[[Detroit, Michigan]] |isbn=978-0-7808-1052-5}}</ref> with the board citing inadequate facilities, sagging attendance, and serious financial losses as the reasons for the decision.<ref>{{cite web| title=2006 University of Miami Basketball Media Guide| accessdate=November 11, 2006|url=http://graphics.fansonly.com/photos/schools/mifl/sports/m-baskbl/auto_pdf/0607_mg_history.pdf|format=PDF|publisher=HurricaneSports.com}}</ref>  The program was revived before the 1985–86 season, though UM would be minimally competitive over the next several years. The program's fortunes turned around in 1990 when Miami hired [[Leonard Hamilton]] as head basketball coach and accepted an invitation to join the [[Big East Conference (1979–2013)|Big East]].  By the end of the decade, Hamilton had turned UM into one of the better basketball programs in the Big East and had guided UM to three straight [[NCAA Men's Division I Basketball Championship|NCAA tournament]] appearances (1998–2000), including a #2 seed in the 1999 tournament and a Sweet 16 appearance in 2000. The 1998 tournament appearance was UM's first since 1960.

Hamilton left at the end of the 2000 season to become head coach of the [[National Basketball Association|NBA's]] [[Washington Wizards]] and was replaced by Perry Clark.  During Clark's second season (2001–02) the team won 24 games and a #5 seed in the NCAA tournament.  With the 2002–03 season, the team moved into its newly completed on-campus arena, the [[Watsco Center]].  Despite a win over powerhouse [[North Carolina Tar Heels|North Carolina]] to christen the new arena, Clark's teams performed woefully over the next two seasons, leading{{Clarify|date=September 2010}} to his dismissal following the 2003–04 season (UM's last season in the Big East).  Clark was replaced by [[Frank Haith]], whose teams were competitive{{Clarify|date=September 2010}} in UM's first two seasons as a member of the [[Atlantic Coast Conference]].

In the 2007/2008 season, after being picked to finish last in the [[Atlantic Coast Conference]] the Hurricanes finished the year 23–11 (8–8 in the ACC) and reached the second round of the [[NCAA Men's Division I Basketball Championship|NCAA Tournament]] before falling to the second seeded [[University of Texas at Austin]]. This was the team's first NCAA tournament bid since the 2001–2002 season.

For the 2009/2010 season, Miami had a winning record overall (20–13), but finished in last place in the ACC with a record of 4–12.

On April 4, 2011, Miami Coach [[Frank Haith]] accepted a Head Coaching position at the [[University of Missouri]].

On April 22, 2011, [[George Mason Patriots]] Head Coach [[Jim Larranaga]] accepted the Head Coaching position after coaching the Patriots for 14 seasons.

For the '12–'13 season, Miami (FL) knocked down No. 1 Duke 90-63, won their first 13 ACC games, and attained the highest AP ranking in school history, attaining a #2 ranking. However, the Hurricanes lost to Wake Forest, 80-65, ruining at the time, a perfect record in ACC play. Miami clinched an ACC regular season title with a home triumph over Clemson.  Miami entered the ACC Tournament as the #1 seed, and won said tournament with a win over the North Carolina Tar Heels.  Thanks to this very successful season, multiple members of the program were recognized.   Starting point guard Shane Larkin was named the ACC Player of the Year, senior shooting guard Durand Scott was named ACC Defensive Player of the Year, & Jim Larranaga was named the ACC Coach of the Year. Miami (FL) was selected to be the No. 2 seed in East Region of the NCAA Tournament. Their first opponent would be Pacific University. They defeated the Tigers 78-49. Their next opponent would be the University of Illinois. They defeated the Fighting Illini 63-59, allowing them to advance into the Regional Semifinals, where they would lose to Marquette

===Women's basketball===
{{See also|Miami Hurricanes women's basketball}}
UM forward Shenise Johnson, during the summer of 2009, competed on the  gold medal-winning USA Team at the 2009 U19 World Championships.<ref>[http://hurricanesports.cstv.com/sports/w-baskbl/spec-rel/080209aaa.html Johnson Records Double-Double in Team USA's Gold Medal Win – MIAMI OFFICIAL ATHLETIC SITE]. Hurricanesports.cstv.com (August 2, 2009). Retrieved on November 26, 2010.</ref>  The team plays its home games at the [[Watsco Center]] on the University of Miami's Coral Gables campus.

In 2009–10, Miami finished last in the ACC. A year later, the Lady Canes went 26–3 (12–2 ACC) in the regular season to finish alongside Duke as regular season ACC champions. Miami went undefeated at the [[Watsco Center]] extending their home winning streak to 24 straight games. Despite a quarterfinal exit in the ACC Tournament, Miami's performance was enough to merit the program's first [[2011 NCAA Women's Division I Basketball Tournament|NCAA tournament]] bid since 1992. After cruising past [[Gardner-Webb Runnin' Bulldogs|Gardner-Webb]] in the first round, they lost to [[Oklahoma Sooners women's basketball|Oklahoma]] in the second. Head coach [[Katie Meier]] won National Coach of the Year, along with Connecticut's Geno Auriemma and Stanford's Tara VanDerveer.<ref>{{Cite web|url=http://espn.go.com/womens-college-basketball/story/_/id/11694604/miami-hurricanes-extend-coach-katie-meier|title=Coach Meier extended, donates $75K to Miami|website=ESPN.com|access-date=2016-04-13}}</ref> Junior guards Shenice Johnson and Riquana Williams were named to the All-ACC first team, sophomore forward Morgan Stroman was named to the all-conference third team, and Johnson was a third-team All-American.<ref>{{Cite web|url=http://www.hurricanesports.com/ViewArticle.dbml?DB_OEM_ID=28700&ATCLID=205548903|title=Miami's Johnson and Williams Named First Team All-ACC|website=Hurricanesports.com|access-date=2016-04-13}}</ref>

The 2011–12 team returned every player from the 2010–11 squad and was picked in the preseason to win the ACC, though they finished 2nd. In the past seasons (2010-11 to 2015-16), they have made the NCAA Tournament five times.

===Cross country===
On July 22, 2008, [[Amy Deem]] was promoted to Director of Track and Field/Cross Country and heads both the men's and women's [[cross country running]] programs. She was head women's track and field coach for the prior seven years.<ref name="autogenerated1">[http://hurricanesports.cstv.com/sports/m-track/spec-rel/072208aaa.html Deem Name Director of Track and Field/Cross-Country – MIAMI OFFICIAL ATHLETIC SITE]. Hurricanesports.cstv.com (July 22, 2008). Retrieved on November 26, 2010.</ref>

In the 2006 ACC Cross Country Championships, UM's men finished 12th out of 12 teams,<ref>[http://hurricanesports.cstv.com/sports/c-xc/stats/2008-2009/accxc-men.html Miami Official Athletic Site – Cross Country]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref> and UM's women finished also finished last out of 12.<ref>[http://hurricanesports.cstv.com/sports/c-xc/stats/2008-2009/accxc-women.html Miami Official Athletic Site – Cross Country]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref>

At the 2009 ACC Cross Country Championship, UM's men<ref>[http://www.flashresults.com/2010_Meets/xc/ACC/Search/09ACCChampionshipsMen_list.php?pagesize=500 ACC Cross Country Championships – Flash Results, Inc]. Flashresults.com. Retrieved on November 26, 2010.</ref>  and women<ref>[http://www.flashresults.com/2010_Meets/xc/ACC/Search/09ACCChampionshipsWomen_list.php?pagesize=500 ACC Cross Country Championships – Flash Results, Inc]. Flashresults.com. Retrieved on November 26, 2010.</ref> again finished last out of 12 teams.

===Diving===
UM has both men's and women's [[diving]] teams. In 2008, the men's team finished 11th (57 pts.) at the ACC Championships and finished 18th (40 pts.) at the NCAA Championships.<ref name=sd>{{cite web|url=http://grfx.cstv.com/photos/schools/mifl/sports/c-swim/auto_pdf/2009-10_swimdive_quickfacts.pdf|title=Quick Facts|accessdate=September 21, 2009}}</ref>

===Football===
[[File:Miami Orange Bowl.jpg|thumb|The [[Miami Orange Bowl]] was the home field for [[Miami Hurricanes football]] until its 2008 demolition. Since then, the Hurricanes have played at [[Hard Rock Stadium]] in [[Miami Gardens, Florida|Miami Gardens]].]]
{{Main article|Miami Hurricanes football}}
{{See also|List of Miami Hurricanes in the NFL}}
Historically, the Hurricanes are one of the most predominant [[college football]] programs in the nation.  They have won five Division I national football championships (1983, 1987, 1989, 1991, and 2001), and are currently ranked fourth on the list of all-time [[Associated Press]] National Poll Championships, behind Notre Dame, Oklahoma and Alabama.<ref>{{cite web|url=http://web1.ncaa.org/web_files/stats/football_records/DI/2008/FBS%20compiled.pdf|title=Football Bowl Subdivision Records|work=NCAA|page=85|accessdate=October 16, 2009}}</ref>

{{As of|2017}}, UM has produced two [[Heisman Trophy]] winners, [[Vinny Testaverde]] (in 1986) and [[Gino Torretta]] (in 1992).  Five former UM football players—[[Warren Sapp]], [[Ted Hendricks]], [[Michael Irvin]], [[Jim Kelly]], [[Cortez Kennedy]] and [[Jim Otto]]—have been voted into the [[Pro Football Hall of Fame]] following their [[National Football League|NFL]] careers. Two other former UM players, [[Ottis Anderson]] and [[Ray Lewis]], have been named [[Super Bowl MVP]]s (for [[Super Bowl XXV]] and [[Super Bowl XXXV]], respectively).  Since the 2008 demolition of the [[Miami Orange Bowl]], the team has played its home games at [[Sun Life Stadium]] in [[Miami Gardens, Florida|Miami Gardens]].

As of the 2011 [[National Football League]] season, UM had the most players active in the NFL of any university in the nation, with 42.<ref>
[http://www.palmbeachpost.com/sports/hurricanes/despite-lackluster-college-careers-nfl-teams-see-quality-2318520.html "Despite lackluster college careers, NFL teams see quality in Miami Hurricanes' prospects", ''The Palm Beach Post'', April 21, 2012.]</ref>

===Women's golf===
The Hurricanes won the national [[Association for Intercollegiate Athletics for Women championships#Golf|golf championships]] in 1970, 1972, 1977, 1978 and 1984.<ref name=wg>[http://hurricanesports.cstv.com/sports/w-golf/sched/mifl-w-golf-sched.html Miami Official Athletic Site – Women'S Golf]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref> In 1959, 1965, 1972 and 1977, [[Judy Street (golfer)|Judy Eller]], [[Roberta Albers]], [[Ann Laughlin]] and [[Cathy Morse]], respectively, won the women's intercollegiate [[Association for Intercollegiate Athletics for Women championships#Individual|individual golf championship]] (an event conducted by the [[AIAW|Division of Girls' and Women's Sports]] — [[AIAW Champions|AIAW]] from 1972 — which was succeeded by the current NCAA women's golf championship).  The team plays its home matches at Deering Bay Yacht & Country Club in Coral Gables, Florida.<ref name=wg/>

===Women's rowing===
{{see also|College rowing}}
In July 2009, Andrew Carter, a former assistant coach at Clemson University, was selected as head rowing coach at UM. Carter has over 20 years of experience coaching at the collegiate and international levels. Marc DeRose was hired as assistant coach.<ref>[http://hurricanesports.cstv.com/sports/c-rowing/spec-rel/081909aaa.html Miami Hires Marc DeRose as Assistant Rowing Coach – MIAMI OFFICIAL ATHLETIC SITE]. Hurricanesports.cstv.com (August 19, 2009). Retrieved on November 26, 2010.</ref>

===Women's soccer===
Miami added a [[college soccer|soccer]] team in 1957. It offered scholarships from the beginning by phasing in 3 scholarships per year over a four-year period.  The [[Cobb Stadium]] for Soccer, Track and Field was built on the Coral Gables campus in 1998 for the program.

===Women's swimming===
{{see also|Swimming (sport)}}
In 2009, the team finished  8th (219 pts.) at the ACC Championships and 24th (25 pts.) at the [[NCAA]] Championships.<ref name=sd/> The team won the [[AIAW]] national championship in 1975 and 1976.<ref>[http://hurricanesports.cstv.com/trads/mifl-natl-championships.html Miami Official Athletic Site – Traditions]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref><ref name="miami1">[http://scholar.library.miami.edu/umhistory/DisplaySubjects.php?DisplayPages&subject_id=Athletics&current_page=2 Display Selected University of Miami Legacy Images]. Scholar.library.miami.edu. Retrieved on November 26, 2010.</ref> UM does not currently have a varsity men's swimming team.

===Tennis===
[[File:RussellIndoor.jpeg|thumb|[[Michael Russell (tennis)|Michael Russell]]]]
[[Michael Russell (tennis)|Michael Russell]] played number one singles for the University of Miami in 1996–97.<ref name="autogenerated2">{{cite web |url= http://sports.espn.go.com/sports/tennis/usopen07/news/story?id=2978555 |title= Perseverance paying off for Michael Russell |publisher= ESPN |date=August 22, 2007 |accessdate= November 10, 2013}}</ref> He was named 1997 NCAA Rookie of the Year and an [[All-America]]n, and finished No. 7 in collegiate rankings (and No. 1 among freshmen).<ref name="autogenerated2"/><ref name="autogenerated2b">{{cite web |url= http://www.usta.com/Active/News/Pro-Tennis/USTA-Pro-Circuit/81221_Michael_Russell_Circuit_Player_of_the_Week/ |title= Michael Russell: Circuit Player of the Week |publisher= USTA |date= May 25, 2008 |accessdate= November 10, 2013}}</ref><ref name="atpworldtour1">{{cite web |url= http://www.atpworldtour.com/Tennis/Players/Ru/M/Michael-Russell.aspx |title= Michael Russell |publisher= ATP World Tour |date= |accessdate= November 10, 2013}}</ref><ref>{{cite web |url=http://cjnews.com/node/86159 |title=Oldest player schools young guns at Rogers Cup |publisher=Cjnews.com |date= August 25, 2010 |accessdate= November 11, 2013}}</ref> His 39 singles-match wins were a school record, and he was the first freshman since 1986 to win the Rolex National Intercollegiate Indoor Championships, defeating [[Fred Niemeyer]] in the final.<ref name="autogenerated1"/> He was also named to the 1997 Rolex Collegiate All-Star Team, selected by the [[Intercollegiate Tennis Association]] and ''[[Tennis Magazine]]'', and the [[Big East]] Championship Most Outstanding Player.<ref>{{cite web |url= http://www.hurricanesports.com/ViewArticle.dbml?ATCLID=205712704 |title= All-American Monday – Michael Russell |publisher= Hurricanesports.com |accessdate= November 11, 2013}}</ref>

{{As of|2009}}, Kevin Ludwig was the head coach, and there were 9 men<ref>[http://hurricanesports.cstv.com/sports/m-tennis/mtt/mifl-m-tennis-mtt.html Miami Official Athletic Site – Men'S Tennis]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref> and 7 women on the [[tennis]] team.<ref>[http://hurricanesports.cstv.com/sports/w-tennis/mtt/mifl-w-tennis-mtt.html Miami Official Athletic Site – Women'S Tennis]. Hurricanesports.cstv.com. Retrieved on November 26, 2010.</ref>  The men's team is coached by [[Mario Rincon]], and women's team by Paige Yaroshuk-Tews.

===Track and field===
In July 2008, Mike Ward, who served for five years as an assistant and 11 years as head coach in the University of Miami's track and cross country programs, retired.<ref>[http://hurricanesports.cstv.com/sports/m-track/spec-rel/071108aaa.html Um Men'S Track And Cross Country Coach Retires – Miami Official Athletic Site]. Hurricanesports.cstv.com (July 11, 2008). Retrieved on November 26, 2010.</ref> Amy Deem, who had been the women's coach for 17 years became the Director of Track and Field/Cross Country.<ref name="autogenerated1"/>  Perhaps UM's most notable athlete is [[Lauryn Williams]] '04, who earned nine All-American honors. Internationally, Williams won the silver medal at the [[2004 Athens Olympics]], gold at the [[2005 World Championships in Athletics]],  and finished 5th at the [[2009 World Championships in Athletics]].,<ref>[http://hurricanesports.cstv.com/sports/c-track/spec-rel/081709aab.html Lauryn Williams Places Fifth in World 100-Meter Final – MIAMI OFFICIAL ATHLETIC SITE]. Hurricanesports.cstv.com (August 17, 2009). Retrieved on November 26, 2010.</ref> all in the 100m dash.  The team plays its home games at [[Cobb Stadium]] on the University of Miami's Coral Gables campus.

===Volleyball===
The women's [[volleyball]] team had a 26–6  2008 season overall with a record of 14–6 in conference matches.<ref>{{cite web|url=http://hurricanesports.cstv.com/sports/w-volley/stats/2008-2009/teamcume.html#TEAM.TEM|title=Overall Team Statistics|accessdate=November 16, 2009}}</ref>

==Former varsity sports==
UM has sponsored other varsity sports in the past. The University of Miami polo team was undefeated in tournament play from 1948–1951. However, the games were poorly attended and the program ran a $15,000 deficit in 1950. The sport was dropped the following year.<ref name="miami1"/> Boxing was one of the most popular and successful athletic programs on campus through the 1950s. Varsity boxing matches attracted sizeable crowds.<ref>[http://scholar.library.miami.edu/umhistory/DisplaySubjects.php?subject_id=Athletics Display Selected University of Miami Legacy Images]. Scholar.library.miami.edu (March 4, 1952). Retrieved on November 26, 2010.</ref>

==Club sports==

===Rugby===
{{main article|University of Miami Rugby Football Club}}

==Championships==

===NCAA team championships===
Miami has won 5 NCAA team national championships.<ref>http://fs.ncaa.org/Docs/stats/champs_records_book/Overall.pdf</ref>

*'''Men's (4)'''
**[[NCAA Division I Baseball Championship#Team titles|Baseball]] (4): 1982, 1985, 1999, 2001
*'''Women's (1)'''
**[[NCAA Division I Women's Golf Championships#Team titles|Golf]] (1): 1984
*see also:
**[[Atlantic Coast Conference#NCAA team championships|ACC NCAA team championships]]
**[[List of NCAA schools with the most NCAA Division I championships#NCAA Division I Team Championships|List of NCAA schools with the most NCAA Division I championships]]

===Other team championships===
Below are 16 national team titles that were not bestowed by the NCAA:

*'''Men's (9)''':
**Football (5): 1983, 1987, 1989, 1991, 2001
**Polo (4): 1947, 1948, 1949, 1950
*'''Women's (7)''':
**Crew, ''overall'' (1): 1999 ([[Intercollegiate Rowing Association|IRA]])<ref>{{cite web|archive-url=https://web.archive.org/web/19990830003317/http://row2k.com/ira/index.shtml|url= http://row2k.com/ira/index.shtml |dead-url=yes|archive-date=30 Aug 1999|title=Intercollegiate Rowing Association Championships May 27–29, 1999 |accessdate=2016-06-09}}</ref>
**Golf (4): 1970, 1972, 1977, 1978 ([[AIAW]])
**Swimming and Diving (2): 1975, 1976 (AIAW)

==Graduation Success Rate==
The Miami Hurricanes most recent (2013) overall program Graduation Success Rate (GSR) of 92 percent, according to the rates released by the NCAA. Miami's GSR is tied for third among ACC schools and 10th overall among FBS programs. UM has exceeded the NCAA average GSR in each of the last nine years. In 2012, Miami set a school-record with a 93 percent GSR, a year after (2011) establishing a then-record at 89 percent. Eight teams compiled GSR marks of 100 percent. Men's diving and golf each posted their ninth consecutive perfect marks, while women's tennis recorded its sixth straight GSR score of 100 percent. Baseball, women's track & field / cross country, rowing, women's swimming and volleyball also tallied perfect GSR scores. It was volleyball's third straight year with a perfect GSR; rowing and women's swimming posted perfect scores for the second straight year.<ref>http://www.hurricanesports.com/ViewArticle.dbml?DB_OEM_ID=28700&ATCLID=209289862</ref>

==Gender equity==
{| class="wikitable" style="float:right;"
|-
|colspan="3" style="text-align: center;"|'''Student-Athletes as of 2014-2015'''<br />(Some Student-Athletes complete in multiple sports,<br />and are counted multiple times)
|-
!Sport!!Male Athletes!!Female Athletes
|-
|Baseball||34|| N/A
|-
|Basketball||14||13
|-
|Cross Country||9||13
|-
|Diving <small>(half sport)</small>||3||N/A
|-
|Football||106|| N/A
|-
|Golf||N/A||7
|-
|Rowing||N/A||46
|-
|Swimming & Diving||N/A||20
|-
|Track & Field||25||30
|-
|Tennis||9||7
|-
|Volleyball||N/A||14
|-
|'''Total'''||'''201'''||'''176'''

|}

The [[University of Miami]], as articulated by its former president [[Donna Shalala]], has been very supportive of achieving gender equity and complying with [[Title IX]].  Shalala wrote of her support in a ''[[The Miami Herald|Miami Herald]]'' column on the 30th anniversary of that law.<ref>{{Cite news|url=http://www6.miami.edu/UMH/CDA/UMH_Main/0,1770,8548-1;14076-3,00.html|work=Miami Herald|title=30 years of leveling playing field|first=Donna|last=Shalala|date=June 23, 2002|accessdate=September 5, 2009}}</ref> However, in the past, female athletes filed complaints with the federal government in the 1970s and 1980s alleging unequal funding and facilities for UM women's sports.<ref>{{Cite book|url=http://www6.miami.edu/womens-commission/dissertation.pdf|page=24|title=Women's Commission Dissertation|accessdate=November 16, 2009}}</ref>

Of the $46.8 million in annual athletic expenditures, $23.9 million were spent on men's team, $9.8 million were spent on women's teams, and $13 million can not be allocated based on gender.<ref name=DOE/>

Miami has notable differences between the graduation rates of male and female student athletes. {{As of|2002}}, UM graduation rates had 64.1% graduating within 4 years, 75.1% graduating within 5 years, and 76.8% graduating within 6 years.<ref>{{cite web|url=http://www6.miami.edu/UMH/CDA/UMH_Main/0,1770,2409-1;44530-2;4829-3,00.html|title=Graduation and Retention Rates|accessdate=September 11, 2009}}</ref> Male student athletes have a 52% 4 year graduation rate, and 72% of female student athletes graduate within 4 years.<ref>{{cite web|url=http://web1.ncaa.org/app_data/inst2007/415.pdf|title=University of Miami (Florida) Cohort Graduation Rates|accessdate=September 11, 2009}}</ref>

Some critics of Miami's allocation of fiscal resources within the Athletics Department have blamed the decision to drop certain men's teams on Title IX compliance.<ref>{{Cite news|url=https://www.nytimes.com/2000/02/29/sports/plus-colleges-miami-drops-swimming-for-men.html|title=Miami Drops Swimming for Men|date=February 29, 2000|work=[[New York Times]]|accessdate=October 8, 2009}}</ref><ref>{{Cite news|url=http://sportsillustrated.cnn.com/vault/article/magazine/MAG1018934/index.htm|title=Use A Scalpel, Not An Ax|last=Maisel|first=Ivan|date=April 17, 2000|work=[[Sports Illustrated]]|accessdate=October 16, 2009}}</ref><ref>{{cite web|url=http://www.themiamihurricane.com/2002/07/29/swim-dive-teams-make-a-splash/|title=Swim, dive teams make a splash|date=July 29, 2002|work=[[The Miami Hurricane]]|accessdate=October 16, 2009}}</ref>

==UM Sports Hall of Fame==
The University of Miami Sports [[Hall of Fame]]<ref>[http://umsportshalloffame.com/ University of Miami Sports Hall of Fame official website]. Retrieved on February 27, 2012.</ref> is located next to the Hecht Athletic Center on campus.  It houses many artifacts and memorabilia from the Hurricanes' athletic teams over the last 80 years.  Each year, the Hall of Fame inducts former athletes who have been out of school at least 10 years, or coaches and administrators, in an annual banquet.

Since its inception in 1966, the UMSHoF has inducted over 250 or the greatest student-athletes.

==Notable alumni==
:''See: [[List of University of Miami alumni#Athletics]]''

==See also==
{{Portal|ACC}}
*[[List of college athletic programs in Florida]]

==References==
{{Reflist|30em}}

==External links==
* {{Official website}}

{{University of Miami}}
{{Navboxes
|titlestyle = {{CollegePrimaryStyle|Miami Hurricanes|color=white}}
|list =
{{Atlantic Coast Conference navbox}}
{{South Florida Sports}}
{{Division1floridacolleges}}
{{Florida College Sports}}
}}

[[Category:Miami Hurricanes|*]]