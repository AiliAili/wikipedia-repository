{|{{Infobox Aircraft Begin
|name = Baby
|image =
|caption = 
}}{{Infobox Aircraft Type
|type = Flying boat fighter
|manufacturer = [[Supermarine]]
|designer = F. J. Hargreaves
|first flight = 
|introduction = 1918
|retired = 
|status =Prototype
|primary user =  
|more users =
|produced = 
|number built = 1
|unit cost = 
|variants with their own articles =[[Supermarine Sea King]]<br />[[Supermarine Sea Lion I]]
}}
|}

The '''Supermarine Baby''' was a [[United Kingdom|British]] [[flying boat]] fighter aircraft of the [[World War I|First World War]] designed and built by the [[Supermarine|Supermarine Aviation Works]]. Although only one was built, it formed the basis for the later [[Supermarine Sea King|Sea King]] fighter and [[Supermarine Sea Lion I|Sea Lion I]] racer.

==Design and development==
The Baby was designed to meet an [[Admiralty]] requirement for a single seat [[floatplane]] or flying boat fighter, capable of operating from the [[Royal Navy]]'s [[Seaplane tender|seaplane carriers]], demanding a speed of 110&nbsp;mph (177&nbsp;km/h) and a ceiling of 20,000&nbsp;ft (6,100&nbsp;m).<ref name="Andrews & Morgan p27-8">Andrews and Morgan 1987, pp.27—28.</ref> Supermarine received an order for three aircraft,<ref name="Mason Fighter p125"/> while orders were also placed for prototypes from [[Westland Aircraft|Westland]] (the [[Westland N.1B]]) and [[Blackburn]] (the [[Blackburn N.1B]])

The aircraft was a single engined [[Pusher configuration|pusher]] [[biplane]], with folding, single-bay wings and a [[T-tail]]. It had a streamlined wooden hull with the pilot's cockpit located in the nose.<ref name="Andrews & Morgan p28">Andrews and Morgan 1987, p.28.</ref> The first prototype, [[United Kingdom military aircraft serials|serial number]] ''N59'' flew in February 1918, powered by a 200&nbsp;hp (149&nbsp;kW) [[Hispano-Suiza 8]] engine. It was later fitted with a [[Sunbeam Arab]] of similar power.<ref name="Bruce p647">Bruce 1957, p.647.</ref> By this time, however, the [[Royal Naval Air Service]] was operating [[Sopwith Pup]] landplanes from flying off platforms aboard ships, and the success of the Pup (and later the [[Sopwith Camel]]) lead to the abandonment of the N.1B programme. The second prototype was delivered as spare parts to support testing of the first prototype, while the third was not completed.<ref name="London p37">London 2003, p.37.</ref>

Despite the Baby's abandonment, it formed the basis for the [[Supermarine Sea Lion I]] racing aircraft, which flew in the 1919 [[Schneider Trophy]],<ref name="Andrews & Morgan p57">Andrews and Morgan 1987, p.57.</ref> and the [[Supermarine Sea King]] fighter.<ref name="Andrews & Morgan p52">Andrews and Morgan 1987, p.52.</ref>

==Specifications==
{{aircraft specifications|
|plane or copter?= plane
|jet or prop?= prop
|ref=Supermarine Aircraft since 1914 <ref name="Andrews & Morganp29">Andrews and Morgan 1987, p.29</ref>
|crew= 1, pilot
|capacity = 
|length main= 26 ft 4 in
|length alt= 8.02 m
|span main= 30 ft 6 in
|span alt= 9.29 m
|height main= 10 ft 7 in
|height alt= 3.22 m
|area main= 309 ft²
|area alt= 28.7 m²
|empty weight main=1,699 lb
|empty weight alt=770 kg
|loaded weight main= 1,699 [[pound (mass)|lb]]
|loaded weight alt= 1,407 [[kilogram|kg]]
|engine (prop)= [[Hispano-Suiza 8]]
|number of props= 1
|power main= 200 [[horsepower|hp]]
|power alt= 149 [[kilowatt|kW]]
|max speed main= 116 mph
|max speed alt= 101 kn, 187 km/h
|range main= 
|range alt=
|ceiling main= 10,700 ft <ref name="Bruce p647"/><ref>Sunbeam Arab engine.</ref>
|ceiling alt= 3,260 m
|more performance=*'''Endurance:''' 3 hr
*'''Climb to 10,000 ft (3,050 m):''' 25 min 10 s <ref name="Mason Fighter p125">Mason 1992, p.125.</ref>
|armament= 
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->

|related=*[[Supermarine Sea King]]
*[[Supermarine Sea Lion I]]

|similar aircraft=
*[[Westland N.1B]]
<!-- similar or comparable aircraft -->

|lists=
*[[List of seaplanes and amphibious aircraft]]<!-- related lists -->

|see also=<!-- other relevant information -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
* Andrews, C.F. and Morgan, E.B. ''Supermarine Aircraft Since 1914''. London: Putnam Books Ltd.,2nd revised edition 2003. ISBN 0-85177-800-3.
* Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957.
* London, Peter. ''British Flying Boats''. Stroud, UK: Sutton Publishing, 2003. ISBN 0-7509-2695-3.
* Mason, Francis K. ''The British Fighter since 1912''. Annapolis, USA:Naval Institute Press, 1992. ISBN 1-55750-082-7.
{{refend}}

==External links==
{{commonscat|Supermarine}}
* [http://www.seawings.co.uk/photogallery.htm Photo]

{{Supermarine aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Flying boats]]
[[Category:Supermarine aircraft|Baby]]
[[Category:Biplanes]]
[[Category:Single-engined pusher aircraft]]