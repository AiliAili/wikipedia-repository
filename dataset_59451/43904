<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Wright Model E
 | image=Wright Model E, quarter view inflight, Simms Station near Dayton, Ohio, 1913. (10479 A.S.).jpg
 | caption=Wright Model E, over Simms Station near [[Dayton, Ohio]], 1913
}}{{Infobox Aircraft Type
 | type=[[Biplane]]
 | national origin=United States of America
 | manufacturer=[[Wright Company]]
 | designer=[[Wright brothers]]
 | first flight=1913
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Wright Model E''' was the first in the series of Wright Flyers that used a single propeller<ref>{{cite web|title=1913 Wright Model E|url=http://www.wright-brothers.org/Information_Desk/Just_the_Facts/Airplanes/Model_E.htm|accessdate=3 Feb 2011}}</ref> The aircraft was also the test demonstrator for the first [[Autopilot|automatic pilot]] control.

<!-- ==Development== -->

==Design==
The Model E featured 24 inch tires. It was flown with four and six cylinder Wright engines.

The model E was fitted with a prototype autopilot that used a wind driven generator and pendulums to drive the wing warping controls. The design was quickly eclipsed by a gyroscopic autopilot developed by [[Lawrence Sperry]] for the competing [[Curtiss Aeroplane Company]].<ref>{{cite book|title=The Bishop's boys: a life of Wilbur and Orville Wright|author=Tom D. Crouch}}</ref>

==Operational history==
On 31 December 1913, Orville Wright demonstrated a Model E with an "automatic stabilizer" flying seven circuits around [[Huffman Prairie]] field with his hands above his head.<ref>{{cite news|newspaper=New York Times|title=Wright Automatic Stabilizer|date=6 January 1914}}</ref>
The Model E demonstrations earned the Wright Brothers the 1913 [[Collier Trophy]] from [[Aero Club of America]].

[[Albert Elton]] (1881–1975) purchased the sole Wright Model E for exhibition flights.<ref>{{cite journal|magazine=American Aviation Historical Society journal, Volumes 9-11|author=American Aviation Historical Society}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Wright Model E)==
{{Aircraft specs
|ref=<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
 General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=
|length ft=27
|length in=9
|length note=
|span m=
|span ft=32
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=316
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=730
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|fuel capacity=
|more general=
<!--
 Powerplant
-->
|eng1 number=1
|eng1 name=aircraft engine
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=fixed pitch
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=7<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
<!--
 Performance
-->
|perfhide=Y<!-- If you are going to enter any performance specs, please remove this "Y" so they will display -->

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category}}
{{reflist}}
<!-- ==External links== -->
{{Wright aircraft}}

[[Category:United States sport aircraft 1910–1919]]
[[Category:Wright aircraft|Model E]]