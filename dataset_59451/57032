{{Infobox journal
| title = Fordham Law Review
| cover = [[Image:Fordhamlrev header.jpg|196px]]
| discipline = [[Law]]
| abbreviation = Fordham Law Rev.
| publisher = [[Fordham Law School]]
| country = [[United States]]
| history = 1914–1917, 1935–present
| frequency = Six times a year
| website = http://www.fordhamlawreview.org
| link1 = http://ir.lawnet.fordham.edu/flr/
| link1-name = Online archives
| ISSN = 0015-704X
| OCLC = 1569695
| LCCN = 97660501
}}
The '''''Fordham Law Review''''' is a student-run [[law review|law journal]] associated with the [[Fordham University School of Law]] that covers a wide range of legal scholarship.

== Overview ==
The ''Fordham Law Review'' is the 9th most cited law journal by journals, and the 6th most cited by courts.<ref>Stephanie Miller, [http://lawlib.wlu.edu/LJ/index.aspx Washington and Lee University, School of Law Library - Most-Cited Legal Periodicals: U.S. and selected non-U.S.], 2014 rankings of law school journals.</ref> The journal's content consists generally of academic articles and essays, symposia, and student-written notes and comments. The journal receives about 1,500 submissions per year and selects approximately 15 manuscripts for publication.{{Citation needed|date=April 2010}}<ref>lawlib.wlu.edu/LJ/index.aspx</ref>

== History ==
The ''Fordham Law Review'' was established in 1914 at the [[Fordham University School of Law]]. However, it suspended publication after only three years, following the United States' entry into [[World War I]].<ref>Robert M. Hanlon, Jr., A History of Fordham Law School, ''Fordham Law Rev.'' 49:xvii-xxii (1980)</ref> The final issue before suspension provided a brief explanatory statement:

<blockquote>
Owing to the war, the Review will close this year with this number. Some of the Board of Editors are in military service, with national and state organizations. Others are at the training camps for reserve officers.<ref>''Fordham Law Rev.'' 3:121 (1917)</ref>
</blockquote>

The journal did not restart publication until 1935 amidst the [[Great Depression]]. Soon thereafter it garnered attention for its publication of Fordham Law School Dean Ignatius M. Wilkinson’s testimony before the Senate Judiciary Committee condemning [[Franklin D. Roosevelt]] [[Judiciary Reorganization Bill of 1937]]. Wilkinson’s testimony, published in the May 1937 edition of the journal, warned Congress that the President's plan "reaches down to and shakes the foundations of our constitutional structure."<ref>Ignatius N. Wilkinson, The President's Plan Respecting the Supreme Court, ''Fordham Law Rev.'' 6:179-189 (1937)</ref>

In 2011, the journal launched ''Res Gestae'', an online companion. ''Res Gestae'' provides a forum for responses to articles published in the regular journal and to comment on contemporary legal issues. Articles published in ''Res Gestae'' are available on the journal's website and on [[Digital Commons]].

== Membership ==
The journal is managed by a board of up to eighteen student editors. It selects approximately fifty-nine staff members each year to assist with production. Membership on the ''Fordham Law Review'' is open to all first-year Fordham law students and transfer students. The journal offers positions to approximately twenty-eight students on the basis of first-year grades and thirty-one students on the basis of their submissions to a writing competition.<ref>[http://www.fordhamlawreview.org/information/about/membership-selection Fordham Law Review, Staff Selection]</ref>

== Notable alumni ==
*[[Vincent L. Briccetti]], United States District Judge, Southern District of New York
*[[Denny Chin]], Judge, United States Court of Appeals for the Second Circuit
*[[Robert J. Corcoran]], late Justice of the Arizona Supreme Court
*[[Jeffery Deaver]], Writer
*[[Steven B. Derounian]], late United States Congressman and New York State judge
*[[Daniel M. Donovan, Jr.]], District Attorney, Staten Island, New York
*[[Claire Eagan]], United States District Judge, Northern District of Oklahoma
*[[John Feerick]], former dean of Fordham Law School
*[[Denis Reagan Hurley]], United States District Judge, Eastern District of New York
*[[Joseph M. McLaughlin]], Judge, United States Court of Appeals for the Second Circuit
*[[William Hughes Mulligan]], late judge of the United States Court of Appeals for the Second Circuit
*[[Lawrence W. Pierce]], former Judge of United States Court of Appeals for the Second Circuit
*[[Mario Procaccino]], late New York City Comptroller and mayoral candidate
*[[Cathy Seibel]], United States District Judge, Southern District of New York

== Notable articles ==
{{says who|date=February 2013}}
*Deborah W. Denno, ''The Lethal Injection Quandary: How Medicine Has Dismantled the Death Penalty'', ''Fordham Law Rev.'' 76:49 (2007)
*[[Harold Hongju Koh]], ''A World Drowning in Guns'', ''Fordham Law Rev.'' 71:2333 (2003)
*Constantine N. Katsoris, ''The Arbitration of a Public Securities Dispute'', ''Fordham Law Rev.'' 53:279 (1984)
*Comment, ''DES and a Proposed Theory of Enterprise Liability'', ''Fordham Law Rev.'' 46:963 (1978)
*[[Warren E. Burger]], ''Are Specialized Training and Certification of Advocates Essential to Our System of Justice?'', ''Fordham Law Rev.'' 42:227 (1973)
*[[John Feerick]], ''The Proposed Twenty-Fifth Amendment to the Constitution'', ''Fordham Law Rev.'' 34:173 (1965)
*Comment, ''Tortious Acts as a Basis for Jurisdiction in Products Liability Cases'', ''Fordham Law Rev.'' 33:671 (1965)<ref>E. J. Dionne Jr., [https://www.nytimes.com/1987/09/18/us/biden-admits-plagiarism-in-school-but-says-it-was-not-malevolent.html?scp=1&sq=biden%20fordham%20law%20review&st=cse Biden Admits Plagiarism in School But Says It Was Not 'Malevolent'], ''New York Times'', Sept. 18, 1987</ref>
*Ignatius N. Wilkinson, ''The President's Plan Respecting the Supreme Court'', ''Fordham Law Rev.'' 6:179 (1937)
*Michael A. Woronoff & Jonathan A. Rosen, ''Understanding Anti-Dilution Provisions in Convertible Securities'', ''Fordham Law Rev.''  74:129 (2007)

==References==
{{Reflist}}

== External links ==
*{{Official website|http://fordhamlawreview.org/}}

{{Fordham University}}

[[Category:American law journals]]
[[Category:Fordham University publications|Law Review]]
[[Category:General law journals]]
[[Category:Publications established in 1914]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Law journals edited by students]]