{{Infobox journal
| title         = Estuarine, Coastal and Shelf Science
| cover         = [[File:ECSSCover.gif]]
| caption       = 
| former_name   = Estuarine and Coastal Marine Science
| abbreviation  = Estuar Coast Shelf Sci
| discipline    = [[Ocean sciences]]
| peer-reviewed = 
| language      = 
| editors       = {{hlist|T.S. Bianchi|M. Elliott|I. Valiela|E. Wolanski}}
| publisher     = [[Elsevier]] on behalf of the Estuarine Coastal Sciences Association
| country       = 
| history       = 1973–present
| frequency     = 
| openaccess    = [[Hybrid open access journal|Hybrid]]
| license       = [[CC BY]] 4.0 or [[CC BY-NC-ND]] 4.0
| impact        = 2.057
| impact-year   = 2014
| ISSNlabel     = 
| ISSN          = 0272-7714
| eISSN         = 1096-0015
| CODEN         = ECSSD3
| JSTOR         = 
| LCCN          = 
| OCLC          = 6929198
| website       = http://www.journals.elsevier.com/estuarine-coastal-and-shelf-science/
| link1         = http://www.sciencedirect.com/science/journal/02727714/
| link1-name    = Online access
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}

'''''Estuarine, Coastal and Shelf Science''''' is a [[peer-review]]ed [[academic journal]] on [[ocean sciences]], with a focus on coastal regions ranging from [[estuaries]] up to the edge of the [[continental shelf]]. It's published by [[Elsevier]] on behalf of the Estuarine Coastal Sciences Association and edited by T.S. Bianchi, M. Elliott, I. Valiela, and E. Wolanski.<ref>{{cite web |url=http://www.elsevier.com/journals/estuarine-coastal-and-shelf-science/0272-7714/guide-for-authors |title=Guide for Authors |work=Estuarine, Coastal and Shelf Science |publisher=Eslevier |accessdate=October 10, 2015}}</ref> The journal began in 1973 as ''Estuarine and Coastal Marine Science'' before the name was changed in 1981. The journal is [[abstracting and indexing|abstracted and indexed]] in the [[Science Citation Index]], [[Scopus]], [[PASCAL (database)|PASCAL]], [[Biosis]], [[INSPEC]], [[Geobase]], and [[Academic Search Premier]].<ref>{{cite web |url=http://miar.ub.edu/2015/?q=issn/0272-7714 |title=ESTUARINE : COASTAL AND SHELF SCIENCE - 0272-7714 |work=MIAR |accessdate=October 11, 2015 |edition=ICDS 2015}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.057.<ref name=WoS>{{cite book |year=2015 |chapter=Estuarine, Coastal and Shelf Science|title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref><ref>{{cite web |url=http://www.researchgate.net/journal/1096-0015_Estuarine_Coastal_and_Shelf_Science |title=Estuarine Coastal and Shelf Science Impact Factor & Description |work=researchgate.net |publisher=ResearchGate |accessdate=October 11, 2015}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/estuarine-coastal-and-shelf-science/}}
* [http://www.ecsa-news.org/ Estuarine Coastal Sciences Association]

[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Oceanography journals]]
[[Category:Publications established in 1973]]
[[Category:Hybrid open access journals]]

{{science-journal-stub}}