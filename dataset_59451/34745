{{good article}}
{{Infobox planet
| minorplanet=yes
| background=#FFFFC0
| name=253 Mathilde
| pronounced={{IPAc-en|m|ə|ˈ|t|ɪ|l|d|ə}}
| image=[[File:(253) mathilde crop.jpg|275px]]
| caption={{longitem|253 Mathilde as seen by [[NEAR Shoemaker|NEAR]] in 1997|style=padding: 4px 0;}}
| discoverer=[[Johann Palisa]]
| discovered=12 November 1885
| discovery_ref=<ref name="moore"/>
| mp_name=(253) Mathilde
| alt_names={{mp|1949 OL|1}}, A915 TN
| named_after=Mathilde Loewy
| mp_category=[[Asteroid belt|Main belt]]
| epoch=31 July 2016 ([[Julian day|JD]] 2457600.5)
| orbit_ref=<ref name="jpl">Unless otherwise noted, parameters are per: {{cite web
  | last=Yeomans | first=Donald K. | date=August 29, 2003
  | url=http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=253
  | title=253 Mathilde
  | work=JPL Small-Body Database Browser
  | publisher=NASA | accessdate=12 May 2016 }}</ref>
| aphelion={{Convert|3.35003411|AU|Gm|abbr=on|lk=on}}
| perihelion={{Convert|1.9467702|AU|Gm|abbr=on}}
| semimajor={{Convert|2.648402147|AU|Gm|abbr=on}}
| eccentricity=0.26492652
| period=4.31 [[Julian year (astronomy)|yr]] (1574.3 [[Julian year (astronomy)|d]])
| mean_anomaly=170.584348[[Degree (angle)|°]]
| inclination=6.7427122°
| asc_node=179.58936°
| arg_peri=157.39642°
| avg_speed=17.98 km/s<ref>For semi-major axis ''a'', orbital period ''T'' and eccentricity ''e'', the average orbital speed is given by:
:<math>\begin{align}
v_o & = \frac{2\pi a}{T}\left[1-\frac{e^2}{4}-\frac{3e^4}{64} - \dots \right] \\
    & = 18.31\ \mbox{km/s} \left[ 1 - 0.0177 - 0.00008 - \cdots \right] \\
    & \approx 17.98\ \mbox {km/s} \\
\end{align}\!\,</math>
For the circumference of an ellipse, see:
{{cite book
 | author=H. St̀eocker
 | author2=J. Harris | date=1998
 | title=Handbook of Mathematics and Computational Science
 | pages=386 | publisher=Springer
 | isbn=0-387-94746-9 }}</ref>
| dimensions=52.8&nbsp;km<ref name="jpl"/><br>(66&times;48&times;46&nbsp;km<ref name="icarus140"/>)
| mass=1.033(±0.044)&times;10<sup>17</sup>&nbsp;kg<ref name="Yeomans 1997"/>
| density=1.3&nbsp;g/cm³<ref name="Yeomans 1997"/>
| surface_grav={{Gr|1.033e-4|26.4|5}}&nbsp;m/s²
| escape_velocity=22.9&nbsp;m/s
| rotation={{Convert|417.7|h|d|abbr=on|lk=on}}<ref name="jpl"/><br />17.406±0.010&nbsp;d<ref>{{cite journal
  | author=Stefano Mottola | title=The slow rotation of 253 Mathilde
  | journal=Planetary and Space Science
  | date=1995 | volume=43 | issue=12 | pages=1609–1613
  | bibcode=1995P&SS...43.1609M
  | doi=10.1016/0032-0633(95)00127-1 |display-authors=etal}}</ref><br>(17&nbsp;d 9&nbsp;h 45&nbsp;[[minute|min]])
| spectral_type=[[C-type asteroid|Cb]]<ref name="jpl"/>
| abs_magnitude=10.3<ref name="jpl"/>
| albedo={{val|0.0436|0.004}}<ref name="jpl"/>
| single_temperature=≈ 174<ref>For asteroid albedo ''α'', semimajor axis ''a'', [[solar luminosity]] ''<math>L_0</math>'', [[Stefan-Boltzmann constant]] ''σ'' and the asteroid's [[infrared]] emissivity ''ε'' (≈ 0.9), the approximate mean temperature ''T'' is given by:
:<math>\begin{align}
T & = \left ( \frac{(1 - \alpha) L_0}{\epsilon \sigma 16 \pi a^2} \right )^{\frac{1}{4}} \\
  & = \left ( \frac{(1 - 0.0436) (3.827 \times 10^{26}\ \mbox{W})} {0.9 (5.670 \times 10^{-8}\ \mbox{W/m}^2\mbox{K}^4) 16 \cdot 3.142 (3.959 \times 10^{11}\ \mbox{m})^2} \right )^{\frac{1}{4}} \\
  & = 173.7\ \mbox{K}
\end{align}</math>

See: {{cite book
 | author=Torrence V. Johnson
 | author2=Paul R. Weissman
 | author3=Lucy-Ann A. McFadden
 | date=2007 | title=Encyclopedia of the Solar System
 | pages=294 | publisher=Elsevier
 | isbn=0-12-088589-1 }}</ref> [[kelvin|K]]

| mean_motion={{Deg2DMS|0.22868000|sup=ms}} / day
| observation_arc=130.38 yr (47622 d)
| uncertainty=0
| moid={{Convert|0.939032|AU|Gm|abbr=on}}
| jupiter_moid={{Convert|2.06073|AU|Gm|abbr=on}}
| tisserand=3.331
}}

'''253 Mathilde''' {{IPAc-en|m|ə|ˈ|t|ɪ|l|d|ə}} is a [[Asteroid belt|main-belt]] [[asteroid]] about 50&nbsp;km in diameter that was discovered by [[Johann Palisa]] in 1885. It has a relatively [[ellipse|elliptical]] [[orbit]] that requires more than four years to circle the [[Sun]]. This asteroid has an unusually slow rate of rotation, requiring 17.4&nbsp;days to complete a 360° [[wikt:revolution|revolution]] about its axis. It is a primitive [[C-type asteroid]], which means the surface has a high proportion of [[carbon]]; giving it a dark surface that reflects only 4% of the light that falls on it.<ref name="flyby"/>

This asteroid was visited by the [[NEAR Shoemaker]] spacecraft during June 1997, on its way to asteroid [[433 Eros]]. During the flyby, the spacecraft imaged a hemisphere of the asteroid, revealing many large craters that have gouged out depressions in the surface. It was the first C-type asteroid to be explored and, until [[21 Lutetia]] was visited in 2010, it was the largest asteroid to be visited by a spacecraft.

==Observation history==
In 1880, Johann Palisa, the director of the Austrian Naval Observatory, was offered a position as an assistant at the newly completed [[Vienna Observatory]]. Although the job represented a demotion for Johann, it gave him access to the new {{Convert|27|in|mm|sing=on}} [[refractor]], the largest telescope in the world at that time. By this point Johann had already discovered 27 asteroids, and he would employ the Vienna {{Convert|27|in|mm|sing=on}} and {{Convert|12|in|mm|sing=on}} instruments to find an additional 94 asteroids before he retired.<ref>{{cite web
 | last=Raab | first=Herbert | date=2002
 | url=http://www.astrometrica.at/Papers/Palisa.pdf
 | format=PDF
 | title=Johann Palisa, the most successful visual discoverer of
 | publisher=Astronomical Society of Linz
 | accessdate=2007-08-27
| archiveurl= https://web.archive.org/web/20070928170558/http://www.astrometrica.at/Papers/Palisa.pdf| archivedate= 28 September 2007 <!--DASHBot-->| deadurl= no}}</ref>

Among his discoveries was the asteroid 253 Mathilde, found on November 12, 1885. The initial [[orbital elements]] of the asteroid were then computed by V. A. Lebeuf, {{refnec|another Austrian astronomer working at the observatory|date=October 2016}}. The name of the asteroid was suggested by Lebeuf, after Mathilde, the wife of [[Moritz Loewy]]&mdash;who was the vice director of the [[Paris Observatory]].<ref name="moore">{{cite book
 | first=Sir Patrick  | last=Moore | authorlink=Patrick Moore
 | date=1999 | title=The Wandering Astronomer
 | publisher=CRC Press | isbn=0-7503-0693-9 }}</ref><ref name="NEAR_press">
{{cite web
 | author=Savage, D.
 | author2=Young, L.
 | author3=Diller, G.
 | author4=Toulouse, A.
 | date=February 1996
 | url=http://www.nasa.gov/home/hqnews/presskit/1996/NEAR_Press_Kit/NEARpk.txt
 | title=Near Earth Asteroid Rendezvous (NEAR) Press Kit
 | publisher=NASA | accessdate=2007-08-29 }}</ref>

In 1995, ground-based observations determined that 253 Mathilde is a C-type asteroid. It was also found to have an unusually long period of rotation.<ref name="NEAR_press"/>

On June 27, 1997, the [[NEAR Shoemaker]] spacecraft passed within 1,212&nbsp;km of 253 Mathilde while moving at a velocity of 9.93&nbsp;km/s. This close approach allowed the spacecraft to capture over 500 images of the surface,<ref name="flyby"/> and provided data for more accurate determinations of the asteroid's dimensions and mass (based on gravitational perturbation of the spacecraft).<ref name="Yeomans 1997"/> However, only one hemisphere of 253 Mathilde was imaged during the fly-by.<ref name="aisr33">{{cite journal
 | last=Cheng | first=Andrew F.
 | title=Implications of the NEAR mission for internal structure of Mathilde and Eros
 | journal=Advances in Space Research | date=2004 | volume=33
 | issue=9 | pages=1558–1563
 | bibcode=2004AdSpR..33.1558C
 | doi=10.1016/S0273-1177(03)00452-6 }}</ref> This was only the third asteroid to be imaged from a nearby distance, following [[951 Gaspra]] and [[243 Ida]].

==Description==
[[Image:253 Mathilde side.png|right|thumb|300px|Damodar, a 20km-wide crater on 253 Mathilde.]]

253 Mathilde is very dark, with an [[albedo]] comparable to fresh [[asphalt]],<ref>{{cite web
 | last=Pon | first=Brian | date=June 30, 1999
 | url=http://eetd.lbl.gov/HeatIsland/Pavements/Albedo/
 | title=Pavement Albedo | publisher=Heat Island Group
 | accessdate=2007-08-27
| archiveurl= https://web.archive.org/web/20070829153207/http://eetd.lbl.gov/HeatIsland/Pavements/Albedo/| archivedate= 29 August 2007 <!--DASHBot-->| deadurl= no}}</ref> and is thought to share the same composition as CI1 or CM2 [[carbonaceous chondrite]] [[meteorite]]s, with a surface dominated by [[Silicate minerals|phyllosilicate minerals]].<ref>{{cite conference
 | author=Kelley, M. S.
 | author2=Gaffey, M. J.
 | author3=Reddy, V.
 | title=Near-IR Spectroscopy and Possible Meteorite Analogs for Asteroid (253)
 | booktitle=38th Lunar and Planetary Science Conference
 | pages=2366 | publisher=Lunar & Planetary Institute
 | date=March 12–16, 2007 | location=League City, Texas
 | url = http://adsabs.harvard.edu/abs/2007LPI....38.2366K
 | bibcode = 2007LPI....38.2366K
 | accessdate = 2007-08-29 }}</ref> The asteroid has a number of extremely large [[Impact crater|crater]]s, with the individual craters being named for [[coal]] fields and basins around the world.<ref>{{cite web
 | last=Blue | first=Jennifer | date=August 29, 2007
 | url=http://planetarynames.wr.usgs.gov/append6.html#Asteroids
 | title=Categories for Naming Features on Planets and Satellites
 | publisher=USGS | accessdate=2007-08-29 | archiveurl= https://web.archive.org/web/20070824115900/http://planetarynames.wr.usgs.gov/append6.html| archivedate= 24 August 2007 <!--DASHBot-->| deadurl= no}}</ref> The two largest craters, Ishikari (29.3&nbsp;km) and Karoo (33.4&nbsp;km), are as wide as the asteroid's average radius.<ref name="icarus140">{{cite journal
  | author=J. Veverka | title=NEAR Encounter with Asteroid 253 Mathilde: Overview
  | journal=Icarus | date=1999 | volume=140
  | issue=1 | pages=3–16
  | bibcode=1999Icar..140....3V  | doi = 10.1006/icar.1999.6120
|display-authors=etal}}</ref> The impacts appear to have spalled large volumes off the asteroid, as suggested by the angular edges of the craters.<ref name="flyby">{{cite web
 | last=Williams | first=David R.
 | date=December 18, 2001
 | url=http://nssdc.gsfc.nasa.gov/planetary/mission/near/near_mathilde.html
 | title=NEAR Flyby of Asteroid 253 Mathilde
 | publisher=NASA | accessdate=2006-08-10 | archiveurl= https://web.archive.org/web/20060818193400/http://nssdc.gsfc.nasa.gov/planetary/mission/near/near_mathilde.html| archivedate= 18 August 2006 <!--DASHBot-->| deadurl= no}}</ref> No differences in brightness or colour were visible in the craters and there was no appearance of layering, so the asteroid's interior must be very homogeneous. There are indications of material movement along the downslope direction.<ref name="icarus140"/>

The density measured by NEAR Shoemaker, 1,300&nbsp;kg/m³, is less than half that of a typical carbonaceous chondrite; this may indicate that the asteroid is very loosely packed [[rubble pile]].<ref name="Yeomans 1997">{{cite journal
  | author=D. K. Yeomans | title=Estimating the mass of asteroid 253 Mathilde from tracking data during the NEAR flyby
  | journal=[[Science (journal)|Science]]
  | volume=278 | issue=5346 | date=1997 | pages=2106–9
  | pmid=9405343
  | url=http://www.sciencemag.org/cgi/content/full/278/5346/2106
  | accessdate=2007-08-29
  | doi=10.1126/science.278.5346.2106 |bibcode = 1997Sci...278.2106Y | archiveurl= https://web.archive.org/web/20071001011145/http://www.sciencemag.org/cgi/content/full/278/5346/2106| archivedate= 1 October 2007 <!--DASHBot-->| deadurl= no|display-authors=etal}}</ref> The same is true of several C-type asteroids studied by ground-based telescopes equipped with [[adaptive optics]] systems ([[45 Eugenia]], [[90 Antiope]], [[87 Sylvia]] and [[121 Hermione]]). Up to 50% of the interior volume of 253 Mathilde consists of open space. However, the existence of a 20-km-long scarp may indicate that the asteroid does have some structural strength, so it could contain some large internal components.<ref name="aisr33"/> The low interior density is an inefficient transmitter of impact shock through the asteroid, which also helps to preserve the surface features to a high degree.<ref name="icarus140"/>

Mathilde's [[orbit]] is [[eccentricity (orbit)|eccentric]], taking it to the outer reaches of the main belt. Nonetheless, the orbit lies entirely between the orbits of [[Mars]] and [[Jupiter]]; it does not cross the planetary orbits. It also has one of the slowest rotation periods of the known asteroids&mdash;most asteroids have a rotation period in the range of 2&ndash;24 hours.<ref>{{cite web
 | last=Lang | first=Kenneth R. | date=2003
 | url=http://ase.tufts.edu/cosmos/view_chapter.asp?id=15&page=3
 | title=2. Asteroids and meteorites, Size, color and spin
 | work=NASA's Cosmos | publisher=NASA
 | accessdate=2007-08-29
}}</ref> Because of the slow rotation rate, NEAR Shoemaker was only able to photograph 60% of the asteroid's surface. The slow rate of rotation may be accounted for by a satellite orbiting the asteroid, but a search of the NEAR images revealed none larger than 10&nbsp;km in diameter out to 20 times the radius of 253 Mathilde.<ref>{{cite journal
 | author=W. J. Merline | title=Search for Satellites of 253 Mathilde from Near-Earth Asteroid Rendezvous Flyby Data
 | journal=Meteoritics & Planetary Science
 | date=1998 | volume=33
 | issue=S4 | pages=A105 
 | doi=10.1111/j.1945-5100.1998.tb01327.x
 | bibcode=1998M&PSA..33..105M
 |display-authors=etal}}</ref>

==See also==
*[[List of craters on 253 Mathilde]]

==References==
{{Reflist}}

==External links==
{{Commons|(253) Mathilde|253 Mathilde}}
*{{cite web
 | author=Bowell, Ted
 | author2=Koehn, Bruce
 | last-author-amp=yes | date=September 2, 2007
 | url=ftp://ftp.lowell.edu/pub/elgb/astorb.html
 | title=The Asteroid Orbital Elements Database
 | publisher=Lowell Observatory | accessdate=2007-09-02 }}
*{{cite web
 | author=Staff | date=August 28, 2007
 | url=http://cfa-www.harvard.edu/iau/lists/NumberedMPs.html
 | title=Discovery Circumstances: Numbered Minor Planets
 | publisher=Minor Planet Center | accessdate=2007-09-02 | archiveurl= https://web.archive.org/web/20070830024215/http://cfa-www.harvard.edu/iau/lists/NumberedMPs.html| archivedate= 30 August 2007 <!--DASHBot-->| deadurl= no}}
*{{cite news
 | last=Hall | first=Alan | date=June 30, 1997
 | title=NEAR-ing Mathilde | publisher=Scientific American
 | url=http://www.sciam.com/article.cfm?articleID=000944D0-BE16-1CF3-93F6809EC5880000
 | accessdate=2007-08-29 }}
*[http://near.jhuapl.edu/Mathilde/mfb.gif Flyby gif] [http://near.jhuapl.edu/Mathilde/images.html parent page]
*{{JPL small body}}

{{Minor planets navigator|252 Clementina|number=253|254 Augusta}}
{{Small Solar System bodies}}

{{DEFAULTSORT:000253}}
[[Category:Main-belt asteroids]]
[[Category:Numbered minor planets]]
[[Category:Discoveries by Johann Palisa|Mathilde]]
[[Category:Minor planets named for people|Mathilde]]
[[Category:Named minor planets|Mathilde]]
[[Category:253 Mathilde| ]]
[[Category:Minor planets visited by spacecraft|19970627]]
[[Category:Cb-type asteroids (SMASS)]]
[[Category:Astronomical objects discovered in 1885|18851112]]