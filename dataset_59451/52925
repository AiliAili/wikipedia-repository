{{Infobox journal
| title = Superconductor Science and Technology
| cover =
| abbreviation = Supercond. Sci. Technol.
| discipline = [[Superconductivity]]
| language = English
| editor = Cathy. P. Foley
| publisher = [[IOP Publishing]]
| country = United Kingdom
| frequency = Monthly
| history = 1988-present
| impact =2.717
| impact-year = 2015
| openaccess = [[Hybrid open access|Hybrid]]
| website = http://iopscience.iop.org/0953-2048/
| ISSN = 0953-2048
| eISSN = 1361-6668
| CODEN = SUSTEF
| LCCN = 88651712
| OCLC = 664561486
}}
'''''Superconductor Science and Technology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering research on all aspects of [[superconductivity]], including theories on superconductivity, the basic [[physics]] of superconductors, the relation of [[microstructure]] and growth to superconducting properties, the theory of novel devices, and the fabrication and properties of thin films and devices. The [[editor-in-chief]] is Cathy P Foley (CSIRO). It was established in 1988 and it is published by [[IOP Publishing]]. According to the ''[[Journal Citation Reports]]'', the journal had an [[impact factor]] of 2.325 for 2014<ref name=WoS>{{cite book |year=2015 |chapter=Superconductor Science and Technology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-02-08 |series=[[Web of Science]]}}</ref> and 2.717 for 2015 according to its official website.

==Article types==
The journal publishes articles in the following categories:
* Papers: regular articles reporting original research in superconductivity and its application without formal length restrictions
* Letters: short articles reporting very substantial new advances and no longer than 5 journal pages or 4500 words including figures
* Topical reviews: [[literature review|review papers]] commissioned by the editors

==References==
<references/>

== External links ==
* {{Official website|http://iopscience.iop.org/0953-2048/}}

[[Category:Physics journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1988]]
[[Category:English-language journals]]
[[Category:IOP Publishing academic journals]]
[[Category:Hybrid open access journals]]