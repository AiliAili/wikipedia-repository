{{good article}}
{{Infobox NCAA team season
  |Mode=Basketball
  |Year=2011–12
  |Prev year=2010–11
  |Next year=2012–13
  |Team=Princeton Tigers
  |Image=Princeton Tigers logo.png
  |ImageSize=150
  |Conference=Ivy League
  |Division=
  |ShortConference=Ivy, 3rd
  |CoachRank=
  |APRank=
  |Record=20–12
  |ConfRecord=10–4
  |HeadCoach=[[Mitch Henderson]]
  |HCYear = 1st
  |AsstCoach1=[[Brian Earl]]
  |AsstCoach2=Dan Geriot
  |AsstCoach3=Marcus Jenkins
  |AsstCoach4=
  |Captain=Douglas Davis
  |Captain2=Patrick Saunders	  	 
  |StadiumArena=[[Jadwin Gymnasium]]
  |Champion=
  |BowlTourney=[[2012 College Basketball Invitational|CBI]]
  |BowlTourneyResult=Quarterfinals
}}
{{2011–12 Ivy League men's basketball standings}}
The '''2011–12 Princeton Tigers men's basketball team''' represented [[Princeton University]] during the [[2011–12 NCAA Division I men's basketball season]]. The Tigers, led by first year head coach [[Mitch Henderson]], played their home games at [[Jadwin Gymnasium]] and are members of the [[Ivy League]]. The team [[captain (sports)|captains]] were seniors Douglas Davis and Patrick Saunders. They finished the season 20–12, 10–4 in Ivy League play to finish in third place. They were invited to the [[2012 College Basketball Invitational]] where they defeated [[2011–12 Evansville Purple Aces men's basketball team|Evansville]] in the first round before falling in the quarterfinals to [[2011–12 Pittsburgh Panthers men's basketball team|Pittsburgh]]. The season was highlighted by wins over a ranked [[2011–12 Harvard Crimson men's basketball team|Harvard team]] and the [[2011–12 Florida State Seminoles men's basketball team|Florida State Seminoles]].  The team was led by unanimous first team All-Ivy League selection [[Ian Hummer]] and second team selection Douglas Davis.

==Preview==
Princeton entered the season having won the [[2010–11 Ivy League men's basketball season]] championship and having earned the resulting [[2011 NCAA Men's Division I Basketball Tournament]] invitation.  The team entered the season having lost senior captains [[Dan Mavraides]] and Kareem Maddox. Maddox was the reigning Ivy League Defensive Player of the Year and a unanimous first team All-Ivy selection.  Mavraides had been a second team All-Ivy selection. The team returned second team All-Ivy selection Hummer.<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/Mens_Basketball_All-Ivy_--_2010-11|title=Men's Basketball All-Ivy – 2010–11|accessdate=2011-03-12|date=2011-03-09|website=IvyLeagueSports.com}}</ref>

Entering the [[2011–12 NCAA Division I men's basketball season]] [[head coach]] [[Mitch Henderson]] began his tenure at [[Princeton Tigers men's basketball|Princeton]], taking over for [[Sydney Johnson]].<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/Mitch_Henderson_Named_Princeton_Mens_Basketball_Coach|title=Mitch Henderson Named Princeton Men's Basketball Coach|accessdate=2012-03-12|date=2011-04-20|website=IvyLeagueSports.com}}</ref> With the move that resulted from Johnson taking a new coaching job, Princeton has six alumni who are active Division I head coaches, a total second only to eight by the [[North Carolina Tar Heels men's basketball]] program.<ref name=GNMBtOCwET/> The Ivy League media poll selected Princeton to be tied with [[2011–12 Yale Bulldogs men's basketball team|Yale]] for second place behind [[2011–12 Harvard Crimson men's basketball team|Harvard]].<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2011-12/weeklyreleases/2011-12_MBB_Release_Week_18.pdf|title=2011–12 Ivy League MEN’S BASKETBALL|accessdate=2012-03-12|date=2012-03-05|website=IvyLeagueSports.com|page=12}}</ref> Princeton had been the preseason favorite the year before.<ref name=ILMBAwPHEt2S>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/Mens_Basketball_Abound_with_Preseason_Honors|title=Ivy League Men's Basketball Abound with Preseason Honors Entering the 2010–11 Season|accessdate=2011-03-06|date=2010-10-15|website=IvyLeagueSports.com}}</ref>

==Roster==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-
|'''Number'''
|'''Name'''
|'''Position'''
|'''Height'''
|'''Weight'''
|'''Year'''
|'''Hometown'''
|-
| 3
| Jimmy Sherburne
| Guard
| 6–3
| 197
| Junior
| [[Whitefish Bay, Wisconsin]]
|-
| 4
| Denton Koon
| Forward
| 6–8
| 205
| Freshman
| [[Liberty, Missouri]]
|-
| 5
| T.J. Bray
| Guard
| 6–5
| 205
| Sophomore
| [[New Berlin, Wisconsin]]
|-
| 11
| Brian Fabrizius
| Forward
| 6–11
| 210
| Freshman
| [[Arlington Heights, Illinois]]
|-
| 14
| Chris Clement
| Guard
| 6–2
| 190
| Sophomore
| [[Round Rock, Texas]]
|-
| 15
| John Comfort 
| Forward
| 6–7
| 197
| Senior
| [[La Grange, Illinois]]
|-
| 20
| Douglas Davis
| Guard
| 5–11
| 157
| Senior
| [[Philadelphia, Pennsylvania]]
|-
| 22
| Patrick Saunders
| Forward
| 6–8
| 205
| Senior
| [[Gilford, New Hampshire]]
|-
| 23
| Clay Wilson
| Guard
| 6–3
| 176
| Freshman
| [[Tulsa, Oklahoma]]
|-
| 25
| Tom Noonan
| Forward
| 6–8
| 235
| Sophomore
| [[Havertown, Pennsylvania]]
|-
| 32
| Mack Darrow
| Forward
| 6–9
| 230
| Junior
| [[Lake Barrington, Illinois]]
|-
| 34
| [[Ian Hummer]]
| Forward
| 6–7
| 230
| Junior
| [[Vienna, Virginia]]
|-
| 40
| Bobby Garbade
| Center
| 6–11
| 245
| Freshman
| [[Binghamton, New York]]
|-
| 41
| Daniel Edwards
| Forward
| 6–8
| 225
| Sophomore
| [[Dallas, Texas]]
|-
| 44
| Brendan Connolly
| Center
| 6–11
| 255
| Junior
| [[Brentwood, Tennessee]]
|-
|}

==Schedule==
With a new first-time head coach, the team got off to slow starts. It started the season with a 1–5 record, but won 18 of its final 24 games and started its conference schedule with a 2–3 record, but won 8 of its final 9 games.<ref name=GNMBtOCwET>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46548&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=205396168|title=Game Notes: Men's Basketball to Open CBI with Evansville Tuesday|accessdate=2012-03-13|date=2012-03-11|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref> The recovery enabled the team to qualify for its 32nd post season appearance (24 NCAA, 5 NIT and 2 CBI).<ref name=MBPR2/>

Princeton earned its first home win against a ranked opponent since the [[1976–77 Princeton Tigers men's basketball team|1976–77 team]]'s January 3, 1977 victory over [[1976–77 Notre Dame Fighting Irish men's basketball team|Notre Dame]] by defeating Harvard (No. 21 [[Coaches' Poll|Coaches]]/25 [[AP Poll|AP]]) on February 11, 2012.<ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=320420163|title=Princeton hands No. 21 Harvard first Ivy League loss|accessdate=2012-03-13|date=2012-02-11|publisher=[[ESPN]]}}</ref> The win was also its first against a ranked opponent on any court since November 11, 1997,<ref name=GNMBtOCwET/> which is when the [[1997–98 Princeton Tigers men's basketball team|1997–98 team]] opened its season with a victory over a ranked [[1997–98 Texas Longhorns men's basketball team|Texas]] team at [[Meadowlands Arena]] (now named Izod Center) in [[East Rutherford, NJ]].<ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?DB_OEM_ID=10600&ATCLID=3749695|title=Men's Basketball Record Book • All-Time Results|accessdate=2012-03-13|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref><ref name=DIR6883>{{cite web|url=http://web1.ncaa.org/web_files/stats/m_basketball_RB/2010/D1.pdf|title=Division I Records|accessdate=2010-08-28|publisher=[[National Collegiate Athletic Association]]|page=80}}</ref> Princeton also defeated eventual [[2012 ACC Men's Basketball Tournament]] Champion [[2011–12 Florida State Seminoles men's basketball team|Florida State]] five weeks after Harvard did<ref>{{cite web|url=http://www.theacc.com/sports/m-baskbl/recaps/031112aaa.html|title=Florida State Wins the #ACCTRNY 85–82 over North Carolina: This is Florida State's first ACC Championship|accessdate=2012-03-14|date=2012-03-11|publisher=[[CBS Interactive]]|author=TheACC.com}}</ref><ref>{{cite web|url=http://espn.go.com/mens-college-basketball/team/schedule/_/id/52/year/2012/florida-state-seminoles|title=Florida State Seminoles Schedule – 2011–12|accessdate=2012-03-12|publisher=[[ESPN]]}}</ref> as well as [[Big East Conference (1979–2013)|Big East Conference]] member and New Jersey rival {{cbb link|2011|sex=men|team=Rutgers Scarlet Knights|school=Rutgers University|title=Rutgers}}. In addition, the team's schedule included the [[Atlantic Coast Conference|ACC]]'s [[2011–12 NC State Wolfpack men's basketball team|NC State]]. Its results against Harvard, Florida State and NC State give the team a 2–2 record against teams in the [[2012 NCAA Men's Division I Basketball Tournament]].<ref name=GNMBtOCwET/>

For the 18th consecutive season, the Ivy League men's basketball schedule concluded with a Tuesday [[Penn–Princeton basketball rivalry]] game against the [[2011–12 Penn Quakers men's basketball team|2011–12 Quakers]].<ref name=PPMTwQAfT>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46548&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=205392062|title=Princeton, Penn Meet Tuesday with Quakers Aiming for Title Share|accessdate=2013-03-13|date=2012-03-05|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref> Princeton's 62&ndash;52 victory enabled it to retain slim 26–25 and 24–23 leads in terms of Ivy League Championships and Ivy League team NCAA Tournament appearances, respectively.<ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46548&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=205393058|title=Men's Hoops Denies Penn Title Share, Finishes Perfect at Home in Ivy |accessdate=2013-03-13|date=2012-03-06|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref><ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=320660163|title=Princeton defeats Pennsylvania; Harvard clinches first Ivy League title|accessdate=2013-03-13|date=2012-03-06|publisher=[[ESPN]]}}</ref>

In the first round of the [[2012 College Basketball Invitational|2012 CBI Tournament]], senior Douglas Davis scored a career-high 31 points to lead Princeton to a 95–86 victory over [[2011–12 Evansville Purple Aces men's basketball team|Evansville]].<ref>{{cite web|url=http://sports.espn.go.com/ncb/boxscore?gameId=320730339|title=Princeton 95 (20–11, 10–4 Ivy); Evansville 86 (16–16, 9–9 MVC)|accessdate=2012-03-14|date=2012-03-13|publisher=[[ESPN]]}}</ref><ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPID=4231&DB_OEM_ID=10600&ATCLID=205397490|title=Davis's Career-High 31 Leads Princeton to First-Round CBI Win|accessdate=2012-03-14|date=2012-03-13|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref> Although Davis posted another 20 points in the subsequent quarterfinal game against [[2011–12 Pittsburgh Panthers men's basketball team|Pittsburgh]] to surpass [[Kit Mueller]] for second place on the Princeton scoring list, Princeton lost 82&ndash;61 to end the season.<ref>{{cite web|url=http://espn.go.com/ncb/boxscore?gameId=320790221|title=Princeton 61 (20-12, 10-4 Ivy); Pittsburgh 82 (19-16, 5-13 Big East)|accessdate=2012-03-20|date=2012-03-19|publisher=[[ESPN]]}}</ref><ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46548&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=205400657|title=Davis Becomes School's Second-Leading Scorer, but Tigers Lose to Pitt|accessdate=2012-03-20|date=2012-03-19|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref> Davis' appearance in the March 19 contest also enabled him to surpass [[Ryan Wittman]] as the Ivy League's all-time leader in games played (122).<ref name=MBPR2>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2011-12/releases/Mens_Basketball_Postseason_Release_2|title=Men's Basketball Postseason Release 2|accessdate=2012-03-20|date=2012-03-18|website=IvyLeagueSports.com}}</ref> Pittsburgh eventually went on to win the tournament.<ref>{{cite web|url=http://scores.espn.go.com/ncb/recap?gameId=320900221|title=Pittsburgh rides hot shooting by Washington State for CBI crown|accessdate=2012-03-31|date=2012-03-30|publisher=[[ESPN]]}}</ref>
{{CBB schedule start|time=|rank=|ranklink=[[2011-12 NCAA Division I men's basketball rankings]]|rankyear=2011|tv=|record=yes|attend=yes}}
|-
!colspan=9| Regular Season
{{CBB schedule entry
| date         = 11/12/2011
| time         = 5:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      =
| rank         =
| opponent     = [[2011–12 Wagner Seahawks men's basketball team|Wagner]]
| opprank      =
| site_stadium = [[Jadwin Gymnasium]]
| site_cityst  = [[Princeton, New Jersey|Princeton, NJ]]
| gamename     =
| attend       = 2,444
| tv           = [[FiOS1]]
| score        = 57–73
| overtime     =
| record       = 0–1
}}
{{CBB schedule entry
| date         = 11/16/2011
| time         = 7:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2011–12 NC State Wolfpack men's basketball team|NC State]]
| opprank      =
| site_stadium = [[RBC Center]]
| site_cityst  = [[Raleigh, North Carolina|Raleigh, NC]]
| gamename     = Legends Classic
| attend       = 12,140
| tv           = [[ESPNU]]
| score        = 58–60
| overtime     = 
| record       = 0–2
}}
{{CBB schedule entry
| date         = 11/19/2011
| time         = 12:30 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      =
| rank         =
| opponent     = [[2011–12 Buffalo Bulls men's basketball team|Buffalo]]
| opprank      =
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| attend       = 1,715
| tv           = FiOS1
| score        = 61–53
| overtime     = 
| record       = 1–2
}}
{{CBB schedule entry
| date         = 11/22/2011
| time         = 7:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      =
| rank         =
| opponent     = [[2011–12 Elon Phoenix men's basketball team|Elon]]
| opprank      =
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| attend       = 1,498
| tv           = FiOS1
| score        = 55–56
| overtime     = 
| record       = 1–3
}}
{{CBB schedule entry
| date         = 11/25/2011
| time         = 6:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2011–12 Bucknell Bison men's basketball team|Bucknell]]
| opprank      =
| site_stadium = [[Sojka Pavilion]]
| site_cityst  = [[Lewisburg, Pennsylvania|Lewisburg, PA]]
| gamename     = Legends Classic
| attend       = 2,488
| tv           = 
| score        = 56–62
| overtime     = 
| record       = 1–4
}}
{{CBB schedule entry
| date         = 11/26/2011
| time         = 2:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      = yes
| rank         =
| opponent     = [[Morehead State Eagles|Morehead State]]
| opprank      = 
| site_stadium = Sojka Pavilion
| site_cityst  = Lewisburg, PA
| gamename     = Legends Classic
| attend       = NA
| tv           = 
| score        = 56–68
| overtime     = 
| record       = 1–5
}}
{{CBB schedule entry
| date         = 11/27/2011
| time         = 2:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      = yes
| rank         =
| opponent     = [[West Alabama Tigers|West Alabama]]
| opprank      = 
| site_stadium = Sojka Pavilion
| site_cityst  = Lewisburg, PA
| gamename     = Legends Classic
| attend       = NA
| tv           = 
| score        = 66–42
| overtime     = 
| record       = 2–5
}}
{{CBB schedule entry
| date         = 11/30/2011
| time         = 7:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      =
| rank         =
| opponent     = [[Lafayette Leopards|Lafayette]]
| opprank      = 
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| attend       = 1,542
| tv           = FiOS1
| score        = 69–54
| overtime     = 
| record       = 3–5
}}
{{CBB schedule entry
| date         = 12/07/2011
| time         = 7:30 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[Rutgers Scarlet Knights men's basketball|Rutgers]]
| opprank      = 
| site_stadium = [[Louis Brown Athletic Center|The RAC]]
| site_cityst  = [[Piscataway, New Jersey|Piscataway, NJ]]
| gamename     = [[Princeton–Rutgers rivalry#Men's basketball|Rivalry]]
| attend       = 3,406
| tv           = 
| score        = 59–57
| overtime     = 
| record       = 4–5
}}
{{CBB schedule entry
| date         = 12/10/2011
| time         = 4:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2011–12 Drexel Dragons men's basketball team|Drexel]]
| opprank      = 
| site_stadium = [[Daskalakis Athletic Center]]
| site_cityst  = [[Philadelphia|Philadelphia, PA]]
| gamename     = 
| attend       = 2,225
| tv           = 
| score        = 60–64
| overtime     = 
| record       = 4–6
}}
{{CBB schedule entry
| date         = 12/14/2011
| time         = 7:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[Rider Broncs|Rider]]
| opprank      = 
| site_stadium = [[Alumni Gymnasium (Rider University)|Alumni Gymnasium]]
| site_cityst  = [[Lawrenceville, New Jersey|Lawrenceville, NJ]]
| gamename     = 
| attend       = 1,650
| tv           = [[Comcast Network|TCN]]
| score        = 72–71
| overtime     = OT
| record       = 5–6
}}
{{CBB schedule entry
| date         = 12/18/2011
| time         = 1:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[Northeastern Huskies|Northeastern]]
| opprank      = 
| site_stadium = [[Matthews Arena]]
| site_cityst  = [[Boston|Boston, MA]]
| gamename     = 
| attend       = 938
| tv           = 
| score        = 71–62
| overtime     = 
| record       = 6–6
}}
{{CBB schedule entry
| date         = 12/22/2011
| time         = 7:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[Siena Saints men's basketball|Siena]]
| opprank      = 
| site_stadium = [[Times Union Center]]
| site_cityst  = [[Albany, New York|Albany, NY]]
| gamename     = 
| attend       = 6,471
| tv           = [[Time Warner Cable Sports Channel (New York)|TWCSN]]
| score        = 59–63
| overtime     = 
| record       = 6–7
}}
{{CBB schedule entry
| date         = 12/30/2011
| time         = 7:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[2011–12 Florida State Seminoles men's basketball team|Florida State]]
| opprank      = 
| site_stadium = [[Donald L. Tucker Center]]
| site_cityst  = [[Tallahassee, Florida|Tallahassee, FL]]
| gamename     = 
| attend       = 6,670
| tv           = [[ESPN3]]
| score        = 75–73
| overtime     = 3OT
| record       = 7–7
}}
{{CBB schedule entry
| date         = 01/01/2012
| time         = 3:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[Florida A&M Rattlers basketball|Florida A&M]]
| opprank      = 
| site_stadium = [[Teaching Gym (Florida A&M)|Teaching Gym]]
| site_cityst  = Tallahassee, FL
| gamename     = 
| attend       = 353
| tv           = 
| score        = 76–61
| overtime     = 
| record       = 8–7
}}
{{CBB schedule entry
| date         = 01/08/2012
| time         = 2:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = [[The College of New Jersey]]
| opprank      = 
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| attend       = 2,246
| tv           = 
| score        = 79–68
| overtime     = 
| record       = 9–7
}}
{{CBB schedule entry
| date         = 01/13/2012
| time         = 7:00 PM
| w/l          = l
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent      = [[Cornell Big Red men's basketball|Cornell]]
| opprank      = 
| site_stadium  = [[Newman Arena]]
| site_cityst   = [[Ithaca, New York|Ithaca, NY]]
| gamename     = 
| attend       = 1,843
| tv           = 
| score        = 59–67
| overtime     = 
| record       = 9–8 (0–1)
}}
{{CBB schedule entry
| date         = 01/14/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[Columbia Lions men's basketball|Columbia]]
| opprank      = 
| site_stadium = [[Levien Gymnasium]]
| site_cityst  = [[Manhattan|New York, NY]]
| gamename     = 
| attend       = 1,764
| tv           = 
| score        = 62–58
| overtime     = 
| record       = 10–8 (1–1)
}}
{{CBB schedule entry
| date         = 01/30/2012
| time         = 7:00 PM
| w/l          = l
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent      = [[2011–12 Penn Quakers men's basketball team|Penn]]
| opprank       = 
| site_stadium  = [[Palestra|The Palestra]]
| site_cityst   = Philadelphia, PA
| gamename     = [[Penn–Princeton basketball rivalry|Penn–Princeton Rivalry]]
| attend       = 6,835
| tv           = 
| score        = 67–82
| overtime     = 
| record       = 10–9 (1–2)
}}
{{CBB schedule entry
| date         = 02/03/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent      = [[Brown Bears men's basketball|Brown]]
| opprank       =
| site_stadium  = [[Pizzitola Sports Center]]
| site_cityst   = [[Providence, Rhode Island|Providence, RI]]
| gamename     = 
| attend       = 905
| tv           = 
| score        = 77–63
| overtime     = 
| record       = 11–9 (2–2)
}}
{{CBB schedule entry
| date         = 02/04/2012
| time         = 7:00 PM
| w/l          = l
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[2011–12 Yale Bulldogs men's basketball team|Yale]]
| opprank       = 
| site_stadium  = [[Payne Whitney Gymnasium]]
| site_cityst   = [[New Haven, Connecticut|New Haven, CT]]
| gamename     = 
| attend       = 2,175
| tv           = 
| score        = 54–58
| overtime     = 
| record       = 11–10 (2–3)
}}
{{CBB schedule entry
| date         = 02/10/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = [[Dartmouth Big Green men's basketball|Dartmouth]]
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 2,152
| tv           = 
| score        = 59–47
| overtime     = 
| record       = 12–10 (3–3)
}}
{{CBB schedule entry
| date         = 02/11/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent      = [[2011–12 Harvard Crimson men's basketball team|Harvard]]
| opprank       = 25
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 5,266
| tv           = ESPNU
| score        = 70–62
| overtime     = 
| record       = 13–10 (4–3)
}}
{{CBB schedule entry
| date         = 02/17/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = Columbia
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 2,751
| tv           = ESPNU
| score        = 77–66
| overtime     = 
| record       = 14–10 (5–3)
}}
{{CBB schedule entry
| date         = 02/18/2012
| time         = 6:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = Cornell
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 3,147
| tv           = 
| score        = 75–57
| overtime     = 
| record       = 15–10 (6–3)
}}
{{CBB schedule entry
| date         = 02/24/2012
| time         = 7:00 PM
| w/l          = l
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = Harvard
| opprank       = 
| site_stadium = [[Lavietes Pavilion]]
| site_cityst  = [[Cambridge, Massachusetts|Cambridge, MA]]
| gamename     = 
| attend       = 2,195
| tv           = 
| score        = 64–67
| overtime     = 
| record       = 15–11 (6–4)
}}
{{CBB schedule entry
| date         = 02/25/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = Dartmouth
| opprank       = 
| site_stadium = [[Leede Arena]]
| site_cityst  = [[Hanover, New Hampshire|Hanover, NH]]
| gamename     = 
| attend       = 1,008
| tv           = 
| score        = 85–61
| overtime     = 
| record       = 16–11 (7–4)
}}
{{CBB schedule entry
| date         = 03/02/2012
| time         = 7:00 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = Yale
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 2,293
| tv           = 
| score        = 64–57
| overtime     = 
| record       = 17–11 (8–4)
}}
{{CBB schedule entry
| date         = 03/03/2012
| time         = 7:30 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = Brown
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = 
| attend       = 2,219
| tv           = 
| score        = 81–47
| overtime     = 
| record       = 18–11 (9–4)
}}
{{CBB schedule entry
| date         = 03/06/2012
| time         = 7:30 PM
| w/l          = w
| nonconf      = 
| homecoming   =
| away         = 
| neutral      = 
| rank         =
| opponent     = Penn
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| gamename     = Penn–Princeton Rivalry
| attend       = 3,590
| tv           = ESPN3
| score        = 62–52
| overtime     = 
| record       = 19–11 (10–4)
}}
|-
!colspan=9| [[2012 College Basketball Invitational|2012 CBI]]
{{CBB schedule entry
| date         = 03/13/2012
| time         = 8:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[2011–12 Evansville Purple Aces men's basketball team|Evansville]]
| opprank       = 
| site_stadium  = [[Ford Center (Evansville)|Ford Center]]
| site_cityst   = [[Evansville, Indiana|Evansville, IN]]
| gamename     = First Roud
| attend       = 3,012
| tv           = [[HDNet]]
| score        = 95–86
| overtime     = 
| record       = 20–11
}}
{{CBB schedule entry
| date         = 03/19/2012
| time         = 7:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         =
| opponent     = [[2011–12 Pittsburgh Panthers men's basketball team|Pittsburgh]]
| opprank       = 
| site_stadium  = [[Peterson Events Center]]
| site_cityst   = [[Pittsburgh|Pittsburgh, PA]]
| gamename     = Quarterfinals
| attend       = 2,001
| tv           = 
| score        = 61&ndash;82
| overtime     = 
| record       = 20–12
}}
{{CBB schedule end|rank=| poll=[[AP Poll]]|timezone=[[Eastern Time Zone|Eastern Time]]}}

===All-Ivy===
The following players earned Ivy League postseason recognition:<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2011-12/releases/Mens_Basketball_All-Ivy_--_2011-12|title=Men's Basketball All-Ivy – 2011–12|accessdate=2012-03-12|date=2012-03-07|website=IvyLeagueSports.com}}</ref>
 
;First Team All-Ivy
*^[[Ian Hummer]], Princeton (Jr., F, Vienna, Va.)
 
;Second Team All-Ivy
*Douglas Davis, Princeton (Sr., G, Philadelphia)
*^Unanimous Selection

===Other===
The [[National Association of Basketball Coaches]] announced their Division&nbsp;I All‐District District 13 team on March 14, recognizing the nation’s best men’s collegiate basketball [[student-athlete]]s. Ian Hummer was a second team selection.<ref>{{cite web|url=http://www.nabc.com/NABC_Releases/releases/2011-12_NABC_Releases/2012_NABC_Division_I_All-District_teams.pdf|format=PDF| work=| publisher=[[National Association of Basketball Coaches]]| title=National Association of Basketball Coaches Announces 2012 Division I All-District Teams| accessdate=2012-03-14|date=2012-03-14}}</ref>

==References==
{{reflist|30em}}

==External links==
*[http://espn.go.com/mens-college-basketball/team/_/id/163/princeton-tigers Princeton Tigers] @ [[ESPN.com]]
*[http://espn.go.com/mens-college-basketball/team/stats/_/id/163/year/2012/princeton-tigers 2011 stats] @ [[ESPN.com]]

{{Princeton Tigers men's basketball navbox}}

{{DEFAULTSORT:2011-12 Princeton Tigers Men's Basketball Team}}
[[Category:Princeton Tigers men's basketball seasons]]
[[Category:2011–12 Ivy League men's basketball season|Princeton]]
[[Category:2012 College Basketball Invitational participants|Princeton]]