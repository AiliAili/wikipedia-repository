<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= J-5
 |image=Piper J-5AX O-M category C-GZHU 01.JPG
 |caption=
}}{{Infobox Aircraft Type
 |type=Multipurpose light civil aircraft
 |manufacturer=[[Piper Aircraft]]
 |designer=
 |first flight=July 1939
 |introduced=
 |produced=1940-1946
 |number built=1,507
 |unit cost=[[United States dollar|US$]]1,995 (1947)
 |variants with their own articles= [[Piper J-3]] <br />[[Piper PA-12]]
}}
|}
The '''Piper J-5 'Cub Cruiser'''' was a larger, more powerful version of the basic [[Piper J-3|Piper J-3 Cub]].  It was designed just two years after the J-3 Cub, and differed by having a wider fuselage with the pilot sitting in the front seat and two passengers sitting in the rear seat. Equipped with a 75-hp [[Continental engine]] the plane's cruising speed was 75&nbsp;mph. Though officially a three-seater, it would be more accurately described as a "two-and-a-half-seater", as two adults would find themselves quite cramped in wider rear seat. The Cruiser sold for $1,995 when it was first designed.{{Citation needed|date=June 2012}}

Production of the three models of the J-5 (-A, -B, -C), fall into two categories that differ considerably.  The obvious difference can be seen in the landing gear.  Early versions, of which 783 were built between 1940 and early 1942, have external bungees. Those built from 1944 to 1946 included design changes developed for the United States Navy HE-1 flying ambulance, and these models have internal bungees.  Currently over 500 J-5s remain on the FAA registry.{{Citation needed|date=June 2012}}

==History==
[[File:Piper AE-1 N203SA 30274 KEM 09.05.09R edited-3.jpg|thumb|right|Ex US Navy Piper AE-1 showing the hinged rear decking for access for ambulance work]]
Throughout World War II, Piper modified the basic structure of the '''J-5A'''. The '''J-5B''' had a 75 h.p. [[Lycoming O-145|Lycoming GO-145-C2]] engine.  The later '''J-5C''' also built as the '''HE-1'''  (later '''AE-1''') ambulance for the U.S. Navy with rear hinged fuselage decking, used the fully cowled 100 h.p. [[Lycoming O-235|Lycoming Military O-235-2 or Civilian O-235-B]] engine with an electrical system, and redesigned landing gear.<ref name="Peperell1987p43">Peperell, 1987, p. 43 ref name FAA ATC 725</ref>
[[File:Piper J-5A Cub Cruiser N41236 N.Perry FL 03.87.jpg|thumb|right|Piper J-5A Cub Cruiser with wing endplates and banner-towing gear at North Perry airport, Florida, in March 1987]]

After the war, Piper dropped the J- designation system in exchange for the PA- system, and the J-5C became the [[Piper PA-12|PA-12 "Super Cruiser"]]. The Super Cruiser was more popular than the basic J-5A, with 3,759 being built.<ref name="Simpson2005p430">Simpson, 2005, p. 430</ref>

Piper also produced a four-seat variant of the Super Cruiser with a 115-hp engine. It was designated the [[PA-14 Family Cruiser]]. It was the least successful of the three Cruiser designs in terms of aircraft sold, with 238 being built in 1948/49,<ref name="Simpson2005p430" /> and only about one hundred remain in existence.

==Variants==
;J-5
:75hp Continental A-75-8 powered variant.
;J-5A
:Continental A-75-9 powered variant.
;J-5A-80
:J-5As modified with an 80hp Continental A-80-8 engine.
;J-5B
:75hp Lycoming O-145-B powered variant.
;J-5C
:100hp Lycoming O-235-B powered variant.
;J-5CA
:Prototype ambulance variant produced as the HE-1 for the United States Navy.
;J-5CO
:Prototype observation variant, later modified as the L-4X to be a prototype for the military L-14 version.
;J-5D
:1946 built aircraft with a 125hp Lycoming engine.

===Military designations===
;YL-14
:Prototype liaison aircraft for the United States Army Air Force, five built.
;L-14
:Production variant of the YL-14, order for 845 cancelled and nine under construction completed for the civilian market.
;AE-1
:HE-1 redesignated in 1943 in the Ambulance category.
;HE-1
:Hospital variant for the United States Navy with hinged fuselage top for stretcher access, 100 built later re-designated AE-1.
;UC-83
:Four J-5A aircraft impressed into military service in Panama later becoming the L-4F.
;L-4F
:Four UC-83s re-designated and an additional 39 J-5As impressed.
;L-4G
:J-5B impressed into military service, 34 aircraft.

==Specifications (J-5)==
{{Aircraft specs
|ref=Peperell, 1987, p. 43
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=two passengers
|length m=
|length ft=22
|length in=6
|length note=
|span m=
|span ft=35
|span in=6
|span note=
|height m=
|height ft=6
|height in=10
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=830
|empty weight note=
|gross weight kg=
|gross weight lb=1450
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming GO-145|Lycoming GO-145-C2]]
|eng1 type=four cylinder, [[horizontally-opposed]] aircraft engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=75<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=wooden
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=96
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=86
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=42
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=430
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=10200
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=460
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{Aircontent|
|related=
* [[Piper J-3 Cub]]
* [[Piper PA-12]]
|similar aircraft=
|sequence=
|lists=
|see also=

}}

==References==
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
*{{cite book|last=Peperell|first=Roger|title=Piper Aircraft and their forerunners|year=1987|publisher=Air-Britain (Historians) Ltd|isbn=0-85130-149-5}}
*{{cite book|last=Simpson|first=Rod|title=Airlife's World Aircraft|year=2001|publisher=Airlife Publishing Ltd|isbn=1-84037-115-3}}
{{refend}}

==External links==
{{commons category|Piper J-5}}
*[http://www.a2oxford.info/pages/spain/piperl14/ Detailed photos - Piper L-14 Army Cruiser]

{{Piper aircraft}}
{{Piper Cub}}
{{lone designation|system=[[USN]] air ambulances pre-1962|designation=AE}}
{{USN hospital aircraft}}
{{USAF liaison aircraft}}
{{USAF transports}}

[[Category:United States civil utility aircraft 1940–1949]]
[[Category:High-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Piper aircraft|J-5]]