'''Jean Hegland''' (born 1956) is an American novelist. She was born in 1956 and raised in Pullman, Washington, just eight miles from the Washington/Idaho border. Her mother taught high school and college level English, and was the Pullman High School librarian for many years, and her father was a professor of English at Washington State University. From them she learned to love books, reading, and writing. In addition, her father had a profound influence on her own teaching philosophy and style.Jean began college at Fairhaven College in Bellingham, Washington, and received her BA in Liberal Arts from Washington State University in 1979. In 1984, after working at a variety of jobs—from making stained glass windows for local businesses to housekeeping at a nursing home—she received a MA in Rhetoric and the Teaching of Composition from Eastern Washington University.

==Novels==

Hegland's first novel, ''Into the Forest'' (Calyx 1996, Bantam 1998), unlike the usual pattern of publishing first in hardback, was first published as a paperback by the nonprofit press  Calyx in Oregon and later as a hardbound from Bantam Books in New York, who purchased the rights to the book from Calyx.<ref>{{cite web|url=http://www.sfgate.com/entertainment/article/Word-of-Mouth-Speaks-Volumes-as-Novel-Goes-3006949.php|title=Word of Mouth Speaks Volumes as Novel Goes National|work=SFGate}}</ref><ref>{{cite web|url=http://www.sfgate.com/entertainment/article/A-Path-Cleared-For-Forest-Hegland-finally-3006966.php|title=A Path Cleared For `Forest' / Hegland finally earns recognition, money|work=SFGate}}</ref> ''Into the Forest'' is a feminist novel about the end of contemporary civilization and the survival of two sisters.<ref>Kirkus Reviews https://www.kirkusreviews.com/book-reviews/jean-alma-hegland-2/into-the-forest-3/</ref> It is a dystopian novel about gender inequality and over-reliance on technology.<ref>{{cite web|url=https://www.theguardian.com/technology/gamesblog/2013/jul/01/last-of-us-bioshock-infinite-male-view|title=The Last of Us, Bioshock: Infinite and why all video game dystopias work the same way|author=Keith Stuart|work=the Guardian}}</ref> ''Into the Forest'' was a New York Times Independent Bookstore bestseller,<ref>https://www.nytimes.com/books/98/10/18/bsp/paperfictioncompare.html</ref> and has been translated into eleven languages. A feature film adaptation of ''[[Into the Forest]]'' starring [[Ellen Page]] and [[Evan Rachel Wood]] premiered at the [[Toronto International Film Festival]] in 2015.<ref>{{cite news |url=http://www.pressdemocrat.com/news/3546913-181/authors-journey-into-the-forest|publisher=Press Democrat|title=Anna Hegland's Journey into the Forest|date=27 February 2015|first=Ann |last=Carranza}}</ref>

Hegland's second novel, ''Windfalls'', (Atria/Simon & Schuster, 2004, Washington Square Press 2005) explores the value of work, art and family ties, as well as the  bond between women and their children.<ref>{{cite journal|journal=Publisher's Weekly|volume=251|issue=4|page=227|url=http://www.publishersweekly.com/978-0-7434-7007-0|title=WINDFALLS - Jean Hegland, Author|accessdate=21 October 2015}}</ref>

Hegland's most recent novel, ''Still Time'', is about a Shakespearean scholar who falls victim to Alzheimer's and struggles to come to terms with his estranged daughter, using the only tools that remain within his reach—his understanding of and love for Shakespeare's late plays.<ref>{{cite journal|last1=Maguire|first1=Susan|journal=Booklist|volume=111|issue=22|page=30|title=Still Time by Jean Hegland Review}}</ref><ref name="Still Time by Jean Hegland Book Review">{{cite news|last1=Skane|first1=Rebecca|title=Still Time by Jean Hegland Book Review|url=http://portsmouthreview.com/still-time-by-jean-hegland-book-review/|publisher=Portsmouth Review|date=August 25, 2015}}</ref>

==Bibliography==
*Hegland, Jean. Into the Forest. New York: Bantam Books, 1997. ISBN 9780553106688
*Hegland, Jean. Windfalls: A Novel. New York: Atria Books, 2004. ISBN 9780743470070
*Hegland, Jean. Still Time: A Novel. New York: Arcade, 2015. ISBN 9781628725797

==References==
{{reflist}}

==External links==
*{{Official website|jean-hegland.com}}

{{DEFAULTSORT:Hegland, Jean}}
[[Category:1956 births]]
[[Category:Living people]]
[[Category:20th-century American novelists]]
[[Category:21st-century American novelists]]
[[Category:American women novelists]]
[[Category:21st-century women writers]]
[[Category:20th-century women writers]]