{{Infobox journal
| title = BMC Evolutionary Biology
| cover =
| discipline = [[Evolutionary biology]]
| abbreviation = BMC Evol. Biol.
| publisher = [[BioMed Central]]
| country =
| openaccess = Yes
| license = [[Creative Commons Attribution]]
| history = 2001–present
| impact = 3.407
| impact-year= 2013
| eISSN = 1471-2148
| OCLC = 47657384
| CODEN = BEBMCG
| website = http://www.biomedcentral.com/bmcevolbiol
| link2 = http://www.biomedcentral.com/bmcevolbiol/content
| link2-name = Online archive
}}
'''''BMC Evolutionary Biology''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] covering all fields of [[evolutionary biology]], including [[phylogenetics]] and [[palaeontology]]. It was established in 2001 and is part of a series of [[BMC journals]] published by [[BioMed Central]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/100966975 |title=BMC Evolutionary Biology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-01-02}}</ref>
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-01-02}}</ref>
* [[Chemical Abstracts Service]]
* [[EMBASE]]
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=Scopus coverage lists |accessdate=2015-01-02}}</ref>
* [[The Zoological Record]]<ref name=ISI/>
* [[CAB International]]
* [[Science Citation Index]]<ref name=ISI/>
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 3.407.<ref name=WoS>{{cite book |year=2014 |chapter=BMC Evolutionary Biology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Commonscat|Media from BMC Evolutionary Biology|BMC Evolutionary Biology}}
{{reflist}}

== External links ==
* {{Official website|http://www.biomedcentral.com/bmcevolbiol}}

[[Category:BioMed Central academic journals|Evolutionary Biology]]
[[Category:Creative Commons Attribution-licensed journals]]


{{biology-journal-stub}}