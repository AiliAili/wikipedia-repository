<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=YP-24<br>Y1A-9
 |image=Detroit-Lockheed YP-24 060906-F-1234P-012.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Two-seat fighter<br/>Ground attack
 |manufacturer=[[Detroit Aircraft Corporation|Detroit Lockheed]]
 |designer=[[Robert J. Woods]]
 |first flight=[[1931 in aviation|1931]]
 |introduced=
 |retired=
 |produced=
 |number built=1 YP-24
 |status=Prototype
 |unit cost=
 |primary user=[[United States Army Air Corps]]
 |more users=
 |developed from=
 |variants with their own articles=[[Consolidated P-30]]
}}
|}

The '''Lockheed-Detroit YP-24''' was a 1930s prototype two-seat [[fighter aircraft]]. An attack version called the '''A-9''' was also proposed. The YP-24 is most remarkable for being the first fighter aircraft to bear the Lockheed name.

==Design and development==
In 1930, [[Detroit Aircraft Corporation]] undertook a private venture to develop a new fighter ("pursuit aircraft" in contemporary terminology) for [[US Army Air Corps]] based on the successful [[Lockheed Altair]] transport plane. Designed by Robert J. Woods, the aircraft was completed in 1931 with Detroit Aircraft fabricating the metal fuselage and Lockheed providing the wooden wings, essentially identical to the Altair. [[Wright Field]] assigned the prototype the designation '''XP-900'''. [[Vance Breese]] was hired to be the chief test pilot for the project.<ref>{{cite journal|magazine=Skyways|title=Breese-Dallas Model 1|author=Robert F. Pauley|page=61}}</ref> The aircraft was purchased by USAAC in September 1931 and redesignated '''YP-24''', serial number ''32-320''. Early testing was sufficiently impressive to generate an order for five Y1P-24 fighters and four Y1A-9 attack aircraft intended to replace the [[Berliner-Joyce P-16]]. The A-9 differed in having four forward-firing machine guns, underwing racks for bombs, and a V-1570-27 engine better low-altitude performance.<ref name="USAF">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2247 "Fact Sheet: Detroit-Lockheed YP-24."] ''Museum of the United States Air Force.'' Retrieved: 16 February 2011.</ref>

==Operational history==
[[File:Detroit-Lockheed YP-24 side view.jpg|thumb|right|Detroit-Lockheed YP-24]]
On 19 October 1931, the sole aircraft crashed. The aircraft had a partially stuck landing gear, and Wright Field pilots painted messages on the side of their [[Boeing P-12|P-12D]] and [[Douglas O-2|O-25C]] aircraft, indicating to test pilot Lt. Harrison Crocker to bail out.<ref>''Skyways,'' April 2001, p. 59.</ref>

Shortly after, in October 1931, events in the [[Great Depression]] forced [[Detroit Aircraft Corporation|Detroit Aircraft]] into [[bankruptcy]]  with Lockheed following suit in June 1932.<ref name="USAF"/> Although Lockheed was resuscitated by a group of investors only five days after it closed doors, the financial hardships had taken their toll and the P-24/A-9 project was cancelled with no aircraft built beyond the original prototype. Four pre-production Y1P-24s, ''32-321/324'', were cancelled.<ref>Andrade 1979, p. 144.</ref> However, after Robert Woods left Detroit Aircraft for [[Consolidated Aircraft]], he continued to develop the YP-24/A-9 concept into '''Consolidated Y1P-25/Y1A-11''' which eventually entered service as [[Consolidated P-30]].<ref name="USAF 1">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2197 "Fact Sheet: Consolidated P-30."] ''Museum of the United States Air Force.'' Retrieved: 16 February 2011.</ref>

==Specifications (YP-24)==
{{Aircraft specifications
|jet or prop?=prop
|plane or copter?=plane
|ref=Lockheed Aircraft since 1913<ref name="frnc1st p114-6">Francillon 1982, pp. 114–116.</ref>
<!-- please include units. if something doesn't apply, leave it blank. -->
|crew=two
|capacity=
|length main=28 ft 9 in
|length alt=8.76 m
|span main=42 ft 9½ in
|span alt=13.04 m
|height main=8 ft 6 in
|height alt=2.59 m
|area main=292 ft²
|area alt=27.1 m²
|empty weight main=3,010 lb
|empty weight alt=1,365 kg
|loaded weight main=4,360 lb
|loaded weight alt=1,978 kg
|useful load main=<!-- lb-->
|useful load alt=<!-- kg-->
|max takeoff weight main= lb
|max takeoff weight alt= kg

|engine (prop)=[[Curtiss V-1570]]-23 "Conqueror"
|type of prop=liquid-cooled [[V12 engine]]
|number of props=1
|power main=600 hp
|power alt=448 kW
|propellers=3-bladed
|propeller or rotor?=propeller

|max speed main=235 mph
|max speed alt=204 knots, 378 km/h
|cruise speed main=215 mph
|cruise speed alt=187 knots, 346 km/h
|range main=556 mi
|range alt=483 nm, 895 km
|ceiling main=25,000 ft
|ceiling alt=7,620 m
|climb rate main=1,820 ft/min
|climb rate alt=9.3 m/s
|loading main=<!-- lb/ft²-->
|loading alt=<!-- kg/m²-->
|power/mass main=<!-- hp/lb-->
|power/mass alt=<!-- W/kg-->

|guns=<br/>
** 1× 0.50 in (12.7 mm) [[M2 Browning machine gun|machine gun]] firing through the propeller
** 1× 0.30 in (7.62 mm) [[M1919 Browning machine gun|machine gun]] firing through the propeller
** 1× 0.30 in machine gun in rear cockpit

}}

==See also==
{{Aircontent
<!-- include as many lines as are appropriate. additional lines/entries with a carriage return. -->
|related=
* [[Consolidated P-30]]
* [[Lockheed Altair]]
|similar aircraft=
|lists=
* [[List of fighter aircraft]]
* [[List of Lockheed aircraft]]
* [[List of military aircraft of the United States]]
|see also=
}}

==References==

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
* Andrade, John M. ''U.S. Military Aircraft Designations and Serials since 1909''. Earl Shilton, Leicester, UK: Midland Counties Publications, 1979. ISBN 0-904597-21-0. 
* Bowers, Peter M. and Enzo Angellucci. ''The American Fighter''. New York: Orion Books, 1987. ISBN 0-517-56588-9.
* Francillon, René J. ''Lockheed Aircraft Since 1913''. London: Putnam, 1982. ISBN 0-370-30329-6.
* Francillon, René J. ''Lockheed Aircraft Since 1913''. Annapolis, Maryland: Naval Institute Press, 1987. ISBN 0-87021-897-2.
* Swanborough, Gordon and Peter M. Bowers. ''United States Military Aircraft Since 1909''. Washington, DC: Smithsonian Publications, 1989. ISBN 0-87474-880-1.
{{refend}}

==External links==
{{commons category|Detroit-Lockheed YP-24}}
* [http://www.wpafb.af.mil/museum/research/p24.htm USAF Museum page on P-24/A-9]

{{Lockheed}}
{{USAF attack aircraft}}
{{USAF fighters}}

[[Category:Detroit Aircraft Corporation aircraft]]
[[Category:United States fighter aircraft 1930–1939|Lockheed P-24]]
[[Category:United States attack aircraft 1930–1939|Lockheed P-24]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]