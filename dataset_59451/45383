{{refimprove|date=November 2016}}
{{Infobox military person
|name=Horst Hannig
|birth_date=13 November 1921
|death_date={{death-date and age|df=yes|15 May 1943|13 November 1921}}
|image=Horst Hannig.jpg
|cation=
|birth_place=[[Ząbkowice Śląskie|Frankenstein]], [[Lower Silesia]]
|death_place=near [[Rocquancourt]], [[Normandy]]
|placeofburial=German War Cemetery at [[German War Graves Commission|St. Desir-de-Lisieux]]
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1939–43
|rank=[[Oberleutnant]] (posthumously)
|commands=7./[[JG 54]], 2./[[JG 2]]
|unit=[[JG 54]], [[JG 2]] "Richthofen"
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]]
|laterwork=}}

'''Horst Hannig''' (13 November 1921 – 15 May 1943) was a [[Luftwaffe]] [[fighter ace]] and [[Posthumous recognition|posthumous]] recipient of the [[Knight's Cross of the Iron Cross with Oak Leaves]] ({{lang-de|Ritterkreuz des Eisernen Kreuzes mit Eichenlaub}}) during [[World War II]]. The Knight's Cross of the Iron Cross and its higher grade Oak Leaves was awarded to recognise extreme battlefield bravery or successful military leadership. A flying ace or fighter ace is a [[military aviation|military aviator]] credited with shooting down five or more enemy [[aircraft]] during aerial combat.<ref>Spick 1996, pp. 3–4.</ref> Hannig is credited with 98 aerial victories claimed in over 350 combat missions. He was killed in action following combat with [[Royal Air Force]] (RAF) [[Spitfire]]s on 15 May 1943.

==Career==
Born in 1921 in [[Ząbkowice Śląskie|Frankenstein]], [[Lower Silesia]], Hannig joined the military service in the Luftwaffe as a ''[[Fahnenjunker]]'' (officer cadet) in October 1939. He was posted to the 6./[[Jagdgeschwader 54]] "''Grünherz''" (JG 54—54th fighter wing) in early 1941.<ref name="Obermaier p60">Obermaier 1989, p. 60.</ref> His brother, Walter Hannig, received the [[German Cross]] in Gold ({{Lang|de|''Deutsches Kreuz in Gold''}}) on 28 April 1943 as an observer with ''Aufklärungsgruppe'' (reconnaissance group) 4.(F)/14 of the Luftwaffe.<ref name="Patzwall & Scherzer p164">Patzwall & Scherzer 2001, p. 164.</ref> Horst Hannig claimed his first aerial victory, a [[Tupolev SB-2]], on the first day of [[Operation Barbarossa]], the German invasion of the Soviet Union on 22 June 1941.<ref>Weal 2007, pp.7–8.</ref> He achieved his first 30 victories up to November 1941. On 9 May 1942, ''[[Leutnant]]'' (second Lieutenant) Hannig was awarded the [[Knight's Cross of the Iron Cross]] ({{Lang|de|''Ritterkreuz des Eisernen Kreuzes''}}) having flown over 200 operations and claiming 48 victories. He and ''Leutnant'' [[Hans Beißwenger]] received the Knight's Cross from ''[[General der Flieger]]'' [[Helmuth Förster]] at [[Siverskaya]]. On 21 July 1942 he claimed his 54th victory, a [[Petlyakov Pe-2]] reconnaissance aircraft, near [[Lake Ilmen]]. It was JG 54 2,500th aerial victory.<ref>Bergström, Dikov, Antipov and Sundin 2006, p. 100.</ref>

[[File:Fw 190 A4 Hanning.jpg|thumb|left|upright=1.5|Focke Wulf Fw 190 A-4 of I./JG 2, flown by Leutnant Hannig, early 1943]]
By early 1943 he had achieved 90 kills on the Eastern Front, and became ''[[Staffelkapitän]]'' (squadron leader) of 2./[[Jagdgeschwader 2]] "Richthofen" (JG 2—2nd fighter fing) in Northern Europe. While with 2./JG 2 he achieved another 8 victories, including 1 four-engine [[United States Army Air Forces]] (USAAF) heavy bomber shot down on 16 February 1943.

Horst Hannig was killed in action on 15 May 1943 against [[Royal Air Force]] (RAF) operations that targeted [[Caen – Carpiquet Airport|Caen-Carpiquet Airdrome]] and [[Poix]] Airdrome. He was shot down by [[Squadron Leader]] J. Charles leading Yellow Section of [[No. 611 Squadron RAF|No. 611 Squadron]], and thus becoming the 1,000th aerial victory of the [[Biggin Hill]] Wing. He had managed to bail out but his parachute failed to open.<ref>Weal 2000, p. 100.</ref> Hannig was posthumously awarded the 364th [[Knight's Cross of the Iron Cross with Oak Leaves]] (''Ritterkreuz des Eisernen Kreuzes mit Eichenlaub'') on 3 January 1944 and posthumously promoted to ''[[Oberleutnant]]'' (first Lieutenant). He was interred at the German war cemetery at St. Desir-de-Lisieux, block 3 row 15 grave 445.

== Awards ==
* [[Iron Cross]] (1939)
** 2nd Class (17 July 1941)<ref name="Thomas p242">Thomas 1997, p. 242.</ref>
** 1st Class (September 1941)<ref name="Thomas p242"/>
* ''[[Ehrenpokal der Luftwaffe]]'' (15 September 1941)<ref name="Obermaier p60"/>
* [[German Cross]] in Gold on 24 November 1941 as ''[[Leutnant]]'' in the II./JG 54<ref name="Patzwall & Scherzer p164"/>
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 9 May 1942 as ''Leutnant'' and pilot in the 6./JG 54<ref>Fellgiebel 2000, p. 213.</ref>{{refn|According to Scherzer as pilot in the 5./JG 54.<ref name="Scherzer p365">Scherzer 2007, p. 365.</ref>|group="Note"}}
** 364th Oak Leaves on 3 January 1944 (posthumously) as ''Leutnant'' and ''[[Staffelkapitän|Staffelführer]]'' of the 2./JG 2 "Richthofen"<ref name="Scherzer p365"/><ref>Fellgiebel 2000, p. 76.</ref>

==Notes==
{{Reflist|group="Note"}}

== References ==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last1=Bergström
  |first1=Christer
  |last2=Dikov
  |first2=Andrey
  |last3=Antipov
  |first3=Vlad
  |last4=Sundin
  |first4=Claes
  |year=2006
  |title=Black Cross / Red Star Air War Over the Eastern Front, Volume 3, Everything for Stalingrad
  |location=Hamilton MT
  |publisher=Eagle Editions
  |isbn=978-0-9761034-4-8
}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst 
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |last2=Scherzer
  |first2=Veit 
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last=Scherzer
  |first=Veit 
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2000
  |title=Jagdgeschwader 2 'Richthofen'
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84176-046-9
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2007
  |title=More Bf 109 Aces of the Russian Front
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84603-177-9
}}

{{Refend}}

{{Knight's Cross recipients of JG 54}}
{{Knight's Cross recipients of JG 2}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Hannig, Horst}}
[[Category:1921 births]]
[[Category:1943 deaths]]
[[Category:People from Ząbkowice Śląskie]]
[[Category:People from the Province of Lower Silesia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]
[[Category:German military personnel killed in World War II]]
[[Category:Aviators killed by being shot down]]