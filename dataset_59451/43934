{{DISPLAYTITLE:Zeppelin LZ 120 ''Bodensee''}}
{{Infobox aircraft
| name           = LZ 120 ''Bodensee''
| image          = File:Airship Bodensee, Oct. 1919.jpg
| caption        =
| alt            = <!-- Alt text for main image -->
|type           = Passenger airship
|national origin = [[Germany]]
|manufacturer   = [[Zeppelin Luftschiffbau]]
|designer       = [[Paul Jaray]]
|first flight   = 20 August 1919
|introduced     = <!--Date the aircraft entered or will enter military or revenue service-->
|retired        = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status         = <!--In most cases, redundant; use sparingly-->
|primary user   = [[DELAG]]|more users     = <!-- Limited to THREE (3) 'more users' here (4 total users).  Separate users with <br/>. -->
|produced       = <!--Years in production (eg. 1970-1999) if still in active use but no longer built -->
|number built   =
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from = <!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
}}

'''LZ&nbsp;120 ''Bodensee''''' was a passenger-carrying [[airship]] built by [[Zeppelin Luftschiffbau]] in 1919 to operate a passenger service between [[Berlin]] and [[Friedrichshafen]]. It was later handed over to the Italian Navy  as war reparations in place of airships that had been sabotaged by their crews and renamed ''Esperia''. A sister-ship, [[Zeppelin LZ 121 Nordstern|LZ 121 ''Nordstern'']], was  built in 1920: it was handed over to France and renamed ''Méditerranée''.

==Design==
The ''Bodensee'', designed by [[Paul Jaray]], had an innovative hull shape of relatively low [[fineness ratio]], (ratio of length to diameter). This was arrived at after [[wind-tunnel]] tests conducted at the [[University of Göttingen]] had shown that this would significantly reduce [[Aerodynamic drag|drag]].<ref name=blimpinfo/>  The framework consisted of eleven 17-sided main transverse frames with a secondary ring frame in each bay, connected by longitudinal girders  with a stiffening keel.<ref name=blimpinfo/>  The forward-mounted control car was combined with the passenger accommodation and was constructed as an integral part of the hull structure rather than being suspended beneath it.<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%201287.html|date= 25 September 1919|journal=[[Flight International|Flight]]|title=The New German "DELAG" Commercial Airship "Bodensee" |page=1289}}</ref>   Passenger accommodation consisted of five compartments seating four people and a VIP cabin for one. An additional six passengers could be carried on wicker chairs in the gangway between the compartments. A galley and toilets were also fitted.<ref>Swinfield, John ''Airship''. London: Conway, 2012. ISBN 978 1844861 385 p.112</ref> It was powered by four {{convert|260|hp|kW|disp=flip|abbr=on}} [[Maybach Mb.IVa]] engines, two in a centrally mounted aft gondola driving a single {{convert|5.2|m|ftin|abbr=on}} diameter two-bladed [[pusher configuration|pusher]] propeller, the other two in a pair of amidships engine cars mounted either side of the hull. These drove {{convert|3.2|m|ftin|abbr=on}} two-bladed propellers via a reversing gearbox to enable reverse thrust for manoeuvering when landing.<ref name=blimpinfo/>

A sister-ship '''LZ 121 ''Nordstern''''', similar to the lengthened ''Bodensee'' but with modified passenger accommodation, was completed in 1920.

==Operational history==
''Bodensee'' was first flown on 20 August 1919 piloted by Captain [[Bernhard Lau]]. The first passenger-carrying flight was made on 24 August, with [[Hugo Eckener]] in command.<ref name=blimpinfo>{{cite web|url=http://www.blimpinfo.com/wp-content/uploads/2012/08/Zeppelin%E2%80%99s-LZ-120-%E2%80%93-%E2%80%9CBodensee%E2%80%9D.pdf|title=Zeppelin's LZ-120 ''Bodensee''  |website=National Lighter-Than-Air Historical Center  |publisher=The Lighter Than Air Society |format=pdf  |accessdate=25 February 2014}}</ref>
It made over 100 flights, carrying 2,322 passengers over a total distance of {{convert|50000|km|abbr=on}}
<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200683.html?search=bodensee|journal=[[Flight International|Flight]]|title=Civil Aviation in Germany|date=1 July 1920| page=683}}</ref>  These flights included a 17-hour voyage between Berlin and [[Stockholm]].

On 3 November 1919 ''Bodensee'' suffered a partial engine failure, leading to an accident at [[Staaken]] when attempting to land. One of the ground handling crew was killed and several injured, and the airship, lightened after five passengers had jumped out, was then carried off by the wind and eventually brought down near [[Magdeburg]]<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%201450.html|journal=[[Flight International|Flight]]|title=Adventures with the "Bodensee" |date=6 November 1919  |page=1452 }}</ref>

''Bodensee''  had suffered some damage in the accident, and while being repaired was also modified: the controls had proved oversensitive, so the control surfaces were cut down and it was lengthened by {{convert|10|m|ftin|abbr=on}}<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200280.html|journal=[[Flight International|Flight]]|date= 12 April 1928|title=H.M. Airship R101 |page=252}}</ref><ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200892.html|journal=[[Flight International|Flight]]|title=Civil Aviation October, 1919, To March, 1920  |date=12 August 1920  |page=894}}</ref>

In July 1921 ''Bodensee'' was handed over to the [[Italy|Italian]] government as  compensation for the Zeppelins which were to have been handed over as war reparations but had been sabotaged by their crews. Two stowaways accompanied the flight to [[Rome]]: a German bank clerk and an American [[cinematographer]].<ref>{{Cite newspaper The Times|articlename=Stowaways in an Airship|section=News in Brief|day_of_week=Wednesday|date=6 July 1921|page_number=12|issue=42765|column= G}}</ref>  In Italian service, renamed ''Esperia'' it made at least one long flight in Italian service, a {{convert|1500|mi|km|disp=flip|abbr=on}} voyage lasting 25&nbsp;hours from Rome to [[Barcelona]] and [[Toulon]] before being broken up for scrap in July 1928.<ref>{{cite journal| url=http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200362.html|journal=[[Flight International|Flight]]|title=  Italian Airship's Long Flight |date=11 June 1925  |page= 362 }}</ref><ref>{{cite web|url=http://www.globalsecurity.org/military/world/europe/it-dirigibile.htm   |publisher=Global Security   |title=Italian Dirigibile   |accessdate= 24 February 2014}}</ref>

The ''Bodensee''s sister-ship, LZ 121 ''Nordstern'', was also covered by the reparations decided as part of [[Treaty of Versailles|the peace treaty of June 1919]] and was confiscated by the Allies,<ref>{{citation |url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%200088.html |title =Twenty-One Years Of Airship Progress |first=W Lockwood |last= Marsh |page =88 |date =3 January 1930 |journal=Flight }}</ref> ''Nordstern'' was delivered to [[France]] as a war reparation on 13 June 1921 and renamed ''Méditerranée''.

==Specifications (after enlarging)==
{{Aircraft specs
|ref=The Lighter Than Air Society<ref name=blimpinfo />
|prime units?=met
<!--
        General characteristics
-->
|genhide=
|crew=12
|capacity=27
|length m=120.8
|length ft=
|length in=
|length note=
|dia m=18.71
|dia ft=
|dia in=
|dia note=
|height m=
|height ft=
|height in=
|height note=
|volume m3=20000
|volume ft3=<!-- lighter-than-air -->
|volume note=
|empty weight kg=13646
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=9593
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=4
|eng1 name=[[Maybach Mb.IVa]]
|eng1 type=six cylinder inline piston engines
|eng1 kw=<!-- prop engines -->
|eng1&nbsp;hp=260
|eng1 shp=<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=132.5
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=1700
|range miles=
|range nmi=
|range note=
|ceiling m=
|ceiling ft=
|ceiling note=

|more performance=

}}

==Notes==
{{reflist}}

==References==
{{LZ Navbox}}
{{Use dmy dates|date=January 2017}}


[[Category:Zeppelins|LZ 120]]
[[Category:Rigid airships]]
[[Category:Hydrogen airships]]
[[Category:Airships of Germany]]
[[Category:Airships of France]]
[[Category:Airships of Italy]]