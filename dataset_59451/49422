{{Use dmy dates|date=October 2016}}
{{Use Australian English|date=October 2016}}
'''Anthony Macris''' (born 29 June 1962) is an Australian novelist, critic and academic. He has been shortlisted for the Prime Minister’s Literary Awards,<ref>{{Cite web|title = 2012 Prime Minister’s Literary Awards - Ministry for the Arts|url = http://arts.gov.au/topics/pms-literary-awards/past-prime-ministers-literary-awards/2012-prime-minister%E2%80%99s-literary-award|website = arts.gov.au|access-date = 2016-02-14}}</ref> the ''Age'' Book of the Year,<ref>{{Cite web|title = The picks of the crop: the 38th Age Book of the Year awards|url = http://newsstore.fairfax.com.au/apps/viewDocument.ac;jsessionid=D76F5D046FF95B039E2F53BBE45F4A41?sy=afr&pb=all_ffx&dt=selectRange&dr=1month&so=relevance&sf=text&sf=headline&rc=10&rm=200&sp=brs&cls=170&clsPage=1&docID=AGE1108061PBMK6QUST4|website = newsstore.fairfax.com.au|access-date = 2016-02-14}}</ref> and been named a ''[[Sydney Morning Herald]]'' Best Young Australian Novelist.<ref>{{Cite web|title = A fairytale beginning - Books - Entertainment - smh.com.au|url = http://www.smh.com.au/news/books/a-fairytale-beginning/2007/05/31/1180205414334.html?page=fullpage|website = www.smh.com.au|access-date = 2016-02-14}}</ref> His creative work has been supported by grants from the Literature Board of the Australia Council.<ref>{{Cite web|title = Anthony Macris|url = http://www.austlit.edu.au/austlit/page/A31956|website = www.austlit.edu.au|access-date = 2016-02-14|last = Austlit}}</ref> He is Associate Professor of Creative Writing at the [[University of Technology, Sydney]].<ref>{{Cite web|title = Anthony Macris {{!}} University of Technology Sydney|url = http://www.uts.edu.au/staff/anthony.macris|website = www.uts.edu.au|access-date = 2016-02-14}}</ref> He has been a regular contributor of book reviews, feature articles and essays to the national media, principally in the area of international literary fiction.

== The ''Capital'' Novels ==
The main concerns of the ''Capital'' novels are "the increasing penetration of market forces into everyday life" ([http://www.sydneyreviewofbooks.com/on-the-road-again/ ''Sydney Review of Books'']), and the effect of "the last several decades of capitalist ‘progress’ … [on] ‘the life-worlds of ordinary people" ([http://theconversation.com/the-case-for-capital-volume-one-by-anthony-macris-24287 ''The Conversation'']).

== Awards ==
1998: ''Sydney Morning Herald'' Best Young Australian Novelist: listed for ''Capital,'' Volume One

1998: Commonwealth Writers’ Prize Best First Book: South-East Asian Section (shortlist): ''Capital'', Volume One<ref>{{Cite web|title = Radical ride through the moronic inferno|url = http://www.theaustralian.com.au/arts/review/radical-ride-through-the-moronic-inferno/story-fn9n8gph-1226544026150?nk=ed7ed48523b8bd894aedccf14f75f3f5-1455450544|website = TheAustralian|access-date = 2016-02-14}}</ref>

2003: Sussex-Samuel Award, Australasian Languages and Literature Association: "Claude Simon and the Emergence of the Generative ''mise en abyme''"<ref>{{Cite web|title = Contemporary Narrative and Poetics (CNP)|url = http://lha.uow.edu.au/taem/research/UOW034172.html|website = lha.uow.edu.au|access-date = 2016-02-14|language = en-au}}</ref>

2011: ''Age'' Book of the Year (shortlist): ''When Horse Became Saw''

2012: Prime Minister’s Literary Awards: non-fiction (shortlist): ''When Horse Became Saw''

== Bibliography ==

=== Fiction ===
''Capital,'' Volume One. Allen & Unwin, 1997. (2nd ed. UWAP 2013)

''Great Western Highway: a love story'' (''Capital'', Volume One, Part Two). UWAP, 2012

''Inexperience and other stories.'' UWAP, 2016

=== Non-fiction ===
''When Horse Became Saw'': a family’s journey through autism. Viking Penguin, 2011

=== Selected essays and journal articles ===
[http://www.sydneyreviewofbooks.com/the-novel-sense-making-and-mao/ 'The Novel, Sense-Making, And Mao'], ''Sydney Review of Books''

[http://www.axonjournal.com.au/author/anthony-macris-and-anthony-uhlmann 'Perception And Sensation In The ''Capital'' Novels: Representing the city in literature'], ''Axon Journal''

[http://www.screeningthepast.com/2013/10/the-immobilised-body-stanley-kubrick%E2%80%99s-a-clockwork-orange/ 'The Immobilised Body: Stanley Kubrick’s ''A Clockwork Orange'''], ''ScreeningThePast''

=== Selected literary journalism ===
[http://www.smh.com.au/news/book-reviews/against-the-day/2006/12/15/1165685879188.html?page=fullpage 'Against the Day'], ''Sydney Morning Herald''

[http://www.smh.com.au/news/book-reviews/my-revolutions/2007/09/07/1188783464125.html 'My Revolutions'], ''Sydney Morning Herald''

[http://www.smh.com.au/news/book-reviews/falling-man/2007/06/15/1181414529946.html?page=2 'Falling Man'], ''Sydney Morning Herald''

== Interviews ==
[http://seizureonline.com/an-interview-with-anthony-macris/ 'An Interview with Anthony Macris'], ''Seizure''

[http://verityla.com/mounting-the-fight-an-interview-with-anthony-macris/ 'Mounting the Fight: an interview with Anthony Macris'], Verity La

[http://www.abc.net.au/7.30/content/2011/s3200772.htm 'Living With Autism'], 7.30 ABC TV

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->{{Reflist}}

{{DEFAULTSORT:Macris, Anthony}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Living people]]
[[Category:1962 births]]
[[Category:Australian novelists]]
[[Category:Australian academics]]