{{good article}}
{{Use mdy dates|date=January 2016}}
{{Infobox judge
|name         = Stephen Breyer
|image        = Stephen Breyer, SCOTUS photo portrait.jpg
|office       = [[Associate Justice of the Supreme Court of the United States]]
|nominator    = [[Bill Clinton]]
|term_start   = August 3, 1994
|term_end     = 
|predecessor  = [[Harry Blackmun]]
|successor    = 
|office1      = Chief Judge of the [[United States Court of Appeals for the First Circuit]]
|term_start1  = March 1990
|term_end1    = August 3, 1994
|predecessor1 = [[Levin H. Campbell|Levin Campbell]]
|successor1   = [[Juan R. Torruella|Juan Torruella]]
|office2      = Judge of the [[United States Court of Appeals for the First Circuit]]
|nominator2   = [[Jimmy Carter]]
|term_start2  = December 10, 1980
|term_end2    = August 3, 1994
|predecessor2 = Position established
|successor2   = [[Sandra Lynch]]
|birth_name   = Stephen Gerald Breyer
|birth_date   = {{birth date and age|1938|8|15}}
|birth_place  = {{nowrap|[[San Francisco]], [[California]], U.S.}}
|death_date   = 
|death_place  = 
|party        = [[Democratic Party (United States)|Democratic]]<ref>{{cite web|url=http://www.washingtonpost.com/wp-dyn/content/article/2008/02/11/AR2008021102753.html |title=As on Bench, Voting Styles Are Personal |work=[[The Washington Post]] |date= February 12, 2008 |accessdate=February 14, 2012}}</ref>
|spouse       = Joanna Hare {{small|(1967–present)}}
|children     = 3
|education    = [[Stanford University]] {{small|([[Bachelor of Arts|BA]])}}<br>[[Magdalen College, Oxford]] {{small|([[Bachelor of Arts|BA]])}}<br>[[Harvard University]] {{small|([[Bachelor of Laws|LLB]])}}
}}
'''Stephen Gerald Breyer''' ({{IPAc-en|ˈ|b|r|aɪər}}; born August 15, 1938) is an [[Associate Justice of the Supreme Court of the United States|Associate Justice]] of the [[Supreme Court of the United States]]. Appointed by President [[Bill Clinton]] in 1994, Breyer is generally associated with the more [[Modern liberalism in the United States|liberal]] side of the Court.<ref>{{cite journal |last=Kersch |first=Ken |year=2006 |title=Justice Breyer's Mandarin Liberty |publisher=University of Chicago Law Review |volume=73 |page=759|quote=As his decision to characterize both the New Deal and Warren Courts as centrally committed to democracy and 'active liberty' makes clear, Justice Breyer identifies his own constitutional agenda with that of these earlier courts, and positions himself, in significant respects, as a partisan of midcentury constitutional liberalism. }}</ref>

Following a [[Law clerk|clerkship]] with Supreme Court Associate Justice [[Arthur Goldberg]] in 1964, Breyer became well known as a law professor and lecturer at [[Harvard Law School]], starting in 1967. There he specialized in [[administrative law]], writing a number of influential textbooks that remain in use today. He held other prominent positions before being nominated for the Supreme Court, including special assistant to the [[United States Assistant Attorney General]] for Antitrust,  assistant [[special prosecutor]] on the [[Watergate]] Special Prosecution Force in 1973, and serving on the [[United States Court of Appeals for the First Circuit|First Circuit Court of Appeals]] from 1980 to 1994.

In his 2005 book ''[[Active Liberty]]'', Breyer made his first attempt to systematically lay out his views on legal theory, arguing that the [[judiciary]] should seek to resolve issues in a manner that encourages popular participation in governmental decisions.

==Early life and education==
Breyer was born in San Francisco, California, the son of Anne A. (née Roberts) and Irving Gerald Breyer,<ref>[http://freepages.genealogy.rootsweb.com/~battle/celeb/breyer.htm Genealogy records], Ancestry.com. Retrieved October 26, 2007</ref> and raised in a middle-class Jewish family. Irving Breyer was legal counsel for the [[San Francisco Board of Education]].<ref>[http://www.oyez.org/justices/stephen_g_breyer/ Oyez Bio]. Retrieved March 21, 2007</ref> Both Breyer and his younger brother, [[Charles R. Breyer|Charles]], who is a federal district judge, are [[Eagle Scout (Boy Scouts of America)|Eagle Scouts]] of San Francisco's Troop 14.<ref name="honor">{{cite book | last = Townley | first = Alvin |url= http://www.thomasdunnebooks.com/TD_TitleDetail.aspx?ISBN=0312366531| title = Legacy of Honor: The Values and Influence of America's Eagle Scouts | publisher=St. Martin's Press| location = New York |pages=56–59| isbn = 0-312-36653-1 |accessdate=December 29, 2006 | year = 2007| origyear= December 26, 2006}}</ref><ref name="honor2">{{cite web | last = Ray | first = Mark | year =2007 | url =http://www.scoutingmagazine.org/issues/0701/a-what.html | title =What It Means to Be an Eagle Scout | work=[[Scouting (magazine)|Scouting]] | publisher=Boy Scouts of America | accessdate =January 5, 2007}}</ref> Breyer's paternal great-grandfather emigrated from Romania to the United States, settling in [[Cleveland]], where Breyer's grandfather was born.<ref name="bookref1">{{cite book|last=Elinor Slater & Robert Slater|title=Great Jewish Men|publisher=Jonathan David Publishers Inc|date=January 1996|page = 73|isbn=9780824603816}}</ref> In 1955, Breyer graduated from [[Lowell High School (San Francisco)|Lowell High School]]. At Lowell, he was a member of the [[Lowell Forensic Society]] and debated regularly in high school tournaments, including against future [[Governor of California|California governor]] [[Jerry Brown]] and future Harvard Law School professor [[Laurence Tribe]].<ref name="Oyez Bio">[http://www.oyez.org/justices/stephen_g_breyer/ Oyez Bio]. Retrieved March 21, 2007 (For Brown; need cite for Tribe)</ref>

Breyer received a Bachelor of Arts in [[Philosophy]] from [[Stanford University]], a Bachelor of Arts from [[Magdalen College, Oxford|Magdalen College]] at Oxford University as a [[Marshall Scholarship|Marshall Scholar]],<ref>{{cite AV media |people= Justice Breyer|date= April 8, 2015|title= 60th Anniversary Marshall Alumni Lecture|trans-title= |medium= youtube video|url= https://www.youtube.com/watch?t=14&v=Uo8kof3-zRw|access-date= June 1, 2015}}</ref> and a [[Bachelor of Laws]] (LL.B) from Harvard Law School. He is also fluent in French.<ref>{{cite web |url=http://www.washingtonlife.com/issues/holiday-2006/pollywood/ | title=Inaugural D.C. French Festival launches sans the Freedom Fries |publisher=Washington Life Magazine |date=October 12, 2006 |accessdate=August 30, 2010}}</ref>

In 1967, he married the Hon. Joanna Freda Hare, a [[psychologist]] and member of the [[British nobility|British aristocracy]], as the youngest daughter of [[John Hare, 1st Viscount Blakenham]]. The Breyers have three adult children: Chloe, an [[Episcopal Church in the United States of America|Episcopal]] priest and author of ''The Close''; Nell, and Michael.<ref name="Supreme Court Bio">[{{SCOTUS URL|about/biographies.aspx}} The Justices of the Supreme Court]. Retrieved April 6, 2012</ref>

==Legal career==
Breyer served as a [[law clerk]] to Associate Justice [[Arthur Goldberg]] during the 1964 term ([[List of law clerks of the Supreme Court of the United States#Arthur J. Goldberg|list]]), and served briefly as a fact-checker for the [[Warren Commission]]. He was a special assistant to the [[United States Assistant Attorney General]] for Antitrust from 1965 to 1967 and an assistant [[special prosecutor]] on the [[Watergate scandal|Watergate]] Special Prosecution Force in 1973. Breyer was a special counsel to the [[United States Senate Committee on the Judiciary|U.S. Senate Committee on the Judiciary]] from 1974 to 1975 and served as chief counsel of the committee from 1979 to 1980.<ref name="Supreme Court Bio" /> He worked closely with the chairman of the committee, Senator [[Edward M. Kennedy]], to pass the [[Airline Deregulation Act]] that closed the [[Civil Aeronautics Board]].<ref name="Oyez Bio" /><ref>Thierer, Adam (December 21, 2010) [http://www.cbsnews.com/8301-501465_162-20026346-501465.html Who'll Really Benefit from Net Neutrality Regulation?], ''[[CBS News]]''</ref>

Breyer was a lecturer, assistant professor, and law professor at Harvard Law School starting in 1967. He taught there until 1994, also serving as a professor at Harvard's [[Kennedy School of Government]] from 1977 to 1980. At Harvard, Breyer was known as a leading expert on [[administrative law]].<ref>[http://findarticles.com/p/articles/mi_qa3622/is_199404/ai_n8720105 The dilemmas of risk regulation&nbsp;– Breaking the Vicious Circle by Stephen Breyer]{{dead link|date=October 2016}}, by Sheila Jasanoff. Issues in Science and Technology, Spring 1994.</ref> While there, he wrote two highly influential books on deregulation: ''Breaking the Vicious Circle: Toward Effective Risk Regulation'' and ''Regulation and Its Reform''. In 1970, Breyer wrote "[[The Uneasy Case for Copyright]]", one of the most widely cited skeptical examinations of copyright. Breyer was a visiting professor at the College of Law in Sydney, Australia, the [[University of Rome La Sapienza|University of Rome]],<ref name="Supreme Court Bio" /> and the [[Tulane University Law School]].<ref>{{cite web|url=http://www.law.tulane.edu/abroad/index.aspx?ekmensel=c580fa7b_168_0_4386_1 |title=Tulane Law School - Study Abroad |publisher=Law.tulane.edu |date=June 16, 2011 |accessdate=February 14, 2012}}</ref>

==Judicial career==
{{external media | width = 210px | float = right | headerimage=[[File:StephenBreyer.jpg|210px]]
 | video1 = [https://www.youtube.com/watch?v=3OByUKNPIJU Justice Stephen Breyer: The Court And The World], 1:14:57, [[WGBH Educational Foundation|WGBH Forum Network]]<ref name="wgbh">{{cite web | title =Stephen Breyer: The Court and the World | work = | publisher =[[WGBH Educational Foundation|WGBH Forum Network]] | date =November 6, 2015  | url =http://forum-network.org/lectures/justice-stephen-breyer-court-and-world/ | accessdate =April 9, 2015 }}</ref> }}
From 1980 to 1994, Breyer was a judge on the [[United States Court of Appeals for the First Circuit]]; he was the court's [[Chief Judge]] from 1990 to 1994.<ref name="Supreme Court Bio" />  In the last days of President [[Jimmy Carter]]'s administration, on November 13, 1980, Carter nominated Breyer to the First Circuit, and the U.S. Senate confirmed him on December 9, 1980, by an 80–10 vote.<ref>{{cite news |title=Sharp Questions for Judge Breyer |url=https://query.nytimes.com/gst/fullpage.html?res=9C0DE0DF153FF933A25754C0A962958260 |work=The New York Times |date=July 10, 2004 |accessdate=March 8, 2008 }}</ref> He served as a member of the [[Judicial Conference of the United States]] between 1990 and 1994 and the [[United States Sentencing Commission]] between 1985 and 1989.<ref name="Supreme Court Bio" /> On the sentencing commission, Breyer played a key role in reforming federal criminal sentencing procedures, producing the [[Federal Sentencing Guidelines]], which were formulated to increase uniformity in sentencing.<ref>{{cite news |title=Justice Breyer Should Recuse Himself from Ruling on Constitutionality of Federal Sentencing Guidelines, Duke Law Professor Says |url=http://www.dukenews.duke.edu/2004/09/breyertip_0904.html |work=Duke University News |date=September 28, 2004 }}</ref>

In 1993, President [[Bill Clinton]] considered him for the seat vacated by [[Byron White]] that ultimately went to Justice [[Ruth Bader Ginsburg]].<ref>{{cite news |first=Richard |last=Berke |title=The Overview; Clinton Names Ruth Ginsburg, Advocate for Women, to Court |url=https://query.nytimes.com/gst/fullpage.html?res=9F0CE7DB163EF936A25755C0A965958260 |work=The New York Times |date=June 15, 1993 }}</ref>  Breyer's appointment came shortly thereafter, however, following the retirement of [[Harry Blackmun]] in 1994, when Clinton nominated Breyer as an Associate Justice of the Supreme Court on May 13, 1994. Breyer was confirmed by the U.S. Senate in an 87 to 9 vote and took his seat August 3, 1994.<ref name="Supreme Court Bio" />
Breyer was the second-longest-serving junior justice in the history of the Court, close to surpassing the record set by Justice [[Joseph Story]] of 4,228 days (from February 3, 1812, to September 1, 1823); Breyer fell 29 days short of tying this record, which he would have reached on March 1, 2006, had Justice [[Samuel Alito]] not joined the Court on January 31, 2006.

==Judicial philosophy==

===In general===
{{further information|Purposive approach}}
Breyer's [[Pragmatism|pragmatic]] approach to the law "will tend to make the law more sensible"; according to [[Cass Sunstein]], Breyer's "attack on [[originalism]] is powerful and convincing".<ref name="115 YALE L.J. 1719">{{cite journal|last=Sunstein|first=Cass R.|title=Justice Breyer's Democratic Pragmatism|journal=The Yale Law Journal|date=May 2006|volume=115|issue=7|pages=1719–1743|doi=10.2307/20455667}}</ref> In 2006, Breyer said that in assessing a law's constitutionality, while some of his colleagues "emphasize language, a more literal reading of the [Constitution's] text, history and tradition", he looks more closely to the "purpose and consequences".<ref>{{cite web |title=Justice Breyer Favors 'Less Literal' Readings |url=http://archive.newsmax.com/archives/ic/2006/2/9/130027.shtml |archive-url=https://web.archive.org/web/20120527130035/http://archive.newsmax.com/archives/ic/2006/2/9/130027.shtml |dead-url=yes |archive-date=May 27, 2012 |publisher=newsmax.com |date=February 9, 2006 |accessdate=September 16, 2010 }}</ref>

Breyer has consistently voted in favor of [[abortion]] rights,<ref name="Wittes2005">{{cite news |first=Benjamin |last=Wittes |title=Memo to John Roberts: Stephen Breyer, a cautious, liberal Supreme Court justice, explains his view of the law |url=http://www.washingtonpost.com/wp-dyn/content/article/2005/09/22/AR2005092201017.html |work=The Washington Post |date=September 25, 2005 }}</ref><ref>''[[Stenberg v. Carhart]]'', {{ussc|530|914|2000}}.</ref> one of the most controversial areas of the Supreme Court's docket. He has also defended the Supreme Court's use of foreign law and [[international law]] as persuasive (but not binding) authority in its decisions.<ref>[http://domino.american.edu/AU/media/mediarel.nsf/1D265343BDC2189785256B810071F238/1F2F7DC4757FD01E85256F890068E6E0?OpenDocument Transcript of Discussion Between Antonin Scalia and Stephen Breyer]. AU Washington College of Law, Jan. 13. Retrieved March 21, 2007</ref><ref>{{cite news |first=Deborah |last=Pearlstein |title=Who's Afraid of International Law |url=http://www.prospect.org/web/page.ww?section=root&name=ViewWeb&articleId=9456 |work=American Prospect Online |date=April 5, 2005 |accessdate=March 21, 2007 |archiveurl=https://web.archive.org/web/20050407212728/http://www.prospect.org/web/page.ww?section%3Droot%26name%3DViewWeb%26articleId%3D9456 <!--Added by H3llBot--> |archivedate=April 7, 2005 }}</ref><ref>''[[Roper v. Simmons]]'', {{ussc|543|551|2005}}; ''[[Lawrence v. Texas]]'', {{ussc|539|558|2003}}; ''[[Atkins v. Virginia]]'', {{ussc|536|304|2002}}.</ref> However, Breyer is also recognized to be deferential to the interests of law enforcement and to legislative judgments in the Supreme Court's [[First Amendment to the United States Constitution|First Amendment]] rulings. Breyer has also demonstrated a consistent pattern of deference to Congress, voting to overturn congressional legislation at a lower rate than any other Supreme Court justice since 1994.<ref>{{cite news |first=Paul |last=Gewirtz |author2=Golder, Chad  |title=So Who Are the Activists? |url=https://www.nytimes.com/2005/07/06/opinion/06gewirtz.html |work=The New York Times |date=July 6, 2005 |accessdate=March 23, 2007 }}</ref>

Breyer's extensive experience in [[administrative law]] is accompanied by his staunch defense of the [[Federal Sentencing Guidelines]]. Breyer rejects the strict interpretation of the [[Sixth Amendment to the United States Constitution|Sixth Amendment]] espoused by [[Antonin Scalia|Justice Scalia]] that all facts necessary to criminal punishment must be submitted to a jury and proved beyond a reasonable doubt.<ref>''[[Blakely v. Washington]]'', {{ussc|542|296|2004}}.</ref>  In many other areas on the Court, too, Breyer's pragmatism is considered the intellectual counterweight to Scalia's [[textualism|textualist]] philosophy.<ref>{{cite news |first=Kathleen M. |last=Sullivan |title=Consent of the Governed |url=https://www.nytimes.com/2006/02/05/books/review/05sullivan.html |work=The New York Times |date=February 5, 2006 }}</ref>

In describing his interpretive philosophy, Breyer has sometimes noted his use of six interpretive tools: text, history, tradition, precedent, the purpose of a statute, and the consequences of competing interpretations.<ref>{{cite news |first=Dalia |last=Lithwick |title=Justice Grover Versus Justice Oscar |url=http://www.slate.com/id/2154993/ |work=Slate |date=December 6, 2006 |accessdate=March 19, 2007 }}</ref>  Breyer notes that only the last two differentiate him from textualists on the Supreme Court such as Scalia. Breyer argues that these sources are necessary, however, and in the former case (purpose), can in fact provide greater objectivity in legal interpretation than looking merely at what is often ambiguous statutory text.<ref>{{cite news |title=Interview with Nina Totenberg |url=http://www.npr.org/templates/story/story.php?storyId=4929668 |publisher=NPR |date=September 30, 2005 |accessdate=March 19, 2007 }}</ref>  With the latter (consequences), Breyer argues that considering the impact of legal interpretations is a further way of ensuring consistency with a law's intended purpose.<ref>Sunstein at 12 ("Breyer thinks that as compared with a single-minded focus on literal text, his approach will tend to make the law more sensible, almost by definition. He also contends that it 'helps to implement the public's will and is therefore consistent with the Constitution's democratic purpose.'  Breyer concludes that an emphasis on legislative purpose 'means that laws will work better for the people they are presently meant to affect. Law is tied to life; and a failure to understand how a statute is so tied can undermine the very human activity that the law seeks to benefit' (p. 100).")</ref>

===''Active Liberty''===
Breyer expounded on his judicial philosophy in 2005 in ''[[Active Liberty|Active Liberty: Interpreting Our Democratic Constitution]]''. In it, Breyer urges judges to interpret legal provisions (of the Constitution or of statutes) in light of the purpose of the text and how well the consequences of specific rulings will fit those purposes. The book is considered a response to the 1997 book ''A Matter of Interpretation'', in which [[Antonin Scalia]] emphasized adherence to the original meaning of the text alone.<ref name="Wittes2005" /><ref>{{cite news |first=Mark |last=Feeney |title=Author in the Court: Justice Stephen Breyer's New Book Reflects His Practical Approach to the Law |work=Boston Globe |date=October 3, 2005 }}</ref>

In ''Active Liberty'', Breyer argues that the [[Founding Fathers of the United States|Framers of the Constitution]] sought to establish a democratic government involving the maximum liberty for its citizens. Breyer refers to [[Isaiah Berlin]]’s ''Two Concepts of Liberty''. The first Berlinian concept, being what most people understand by liberty, is "freedom from government coercion". Berlin termed this "[[negative liberty]]" and warned against its diminution; Breyer calls this "modern liberty". The second Berlinian concept&nbsp;– "[[positive liberty]]"&nbsp;– is the "freedom to participate in the government". In Breyer's terminology, this is the "active liberty" which the judge should champion. Having established what "active liberty" is, and positing the primary importance (to the Framers) of this concept over the competing idea of "negative liberty", Breyer argues a predominantly [[utilitarian]] case for judges making rulings that give effect to the [[Original intent|democratic intentions]] of the [[United States Constitution|Constitution]].

Both of the books' historical premises and practical prescriptions have been challenged. For example, according to [[Peter Berkowitz]],<ref>{{cite web |url=http://www.peterberkowitz.com/democratizingtheconstitution.pdf |format=PDF|title=Democratizing the Constitution |accessdate=October 26, 2007 |last=Berkowitz |first=Peter}}</ref> the reason that "[t]he primarily democratic nature of the Constitution's governmental structure has not always seemed obvious", as Breyer puts it, is "because it's not true, at least in Breyer's sense that the Constitution elevates active liberty above modern [negative] liberty".  Breyer's position "demonstrates not fidelity to the Constitution", Berkowitz argues, "but rather a determination to rewrite the Constitution's priorities".  Berkowitz suggests that Breyer is also inconsistent, in failing to apply this standard to the issue of abortion, instead preferring decisions "that protect women's modern liberty, which remove controversial issues from democratic discourse".  Failing to answer the [[Textualism|textualist]] charge that the [[Living Constitution|Living Documentarian]] Judge is a law unto himself, Berkowitz argues that ''Active Liberty'' "suggests that when necessary, instead of choosing the consequence that serves what he regards as the Constitution’s leading purpose, Breyer will determine the Constitution’s leading purpose on the basis of the consequence that he prefers to vindicate".

Against the last charge, [[Cass Sunstein]] has defended Breyer, noting that of the nine justices on the late Rehnquist Court, Breyer showed the highest percentage of votes to uphold acts of Congress and also to defer to the decision of the [[Executive branch of the United States|executive branch]].<ref>Sunstein, pg. 7, citing Lori Ringhand, "Judicial Activism and the Rehnquist Court", available on ssrn.com and Cass R. Sunstein and Thomas Miles, "Do Judges Make Regulatory Policy? An Empirical investigation of Chevron", ''U Chi L Rev'' (forthcoming 2006).</ref> However, according to [[Jeffrey Toobin]] in ''[[The New Yorker]]'', "Breyer concedes that a judicial approach based on 'active liberty' will not yield solutions to every constitutional debate," and that, in Breyer's words, "Respecting the democratic process does not mean you abdicate your role of enforcing the limits in the Constitution, whether in the Bill of Rights or in separation of powers."<ref>{{cite news |first=Jeffrey |last=Toobin |title=Breyer's Big Idea |url=http://www.newyorker.com/archive/2005/10/31/051031fa_fact?currentPage=1 |work=The New Yorker |date=October 31, 2005 }}</ref>

To his point, and from a discussion at the [[New York Historical Society]] in March 2006, Breyer has noted that "democratic means" did not bring about an end to [[slavery]], or the concept of "one man, one vote", which allowed corrupt and discriminatory (but democratically-inspired) state laws to be overturned in favor of [[civil rights]].<ref name=NYHS>{{cite web|last=Pakaluk|first=Maximilian|title=Chambered in a 'Democratic Space'. Justice Breyer explains his Constitution|work=National Review|date=March 13, 2006|url=http://www.nationalreview.com/comment/pakaluk_200603130802.asp|archive-url=https://web.archive.org/web/20060318104617/http://www.nationalreview.com/comment/pakaluk_200603130802.asp|dead-url=yes|archive-date=March 18, 2006|accessdate=October 31, 2007}}</ref>

===Other books===

In 2010, Breyer released a second book, ''Making Our Democracy Work: A Judge's View'' (ISBN 978-0307269911).<ref>{{cite news |first=David |last=Fontana |title=Stephen Breyer's "Making Democracy Work", reviewed by David Fontana |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/10/01/AR2010100103520.html |work=The Washington Post |date=October 3, 2005 |accessdate=October 8, 2010}}</ref>

In 2015, Breyer released a third book, ''The Court and the World: American Law and the New Global Realities'', examining the interplay between US and international law and how the realities of a globalized world need to be considered in US cases.<ref>{{cite news|last1=Witt|first1=John Fabian|title=Stephen Breyer’s ‘The Court and the World’|url=https://www.nytimes.com/2015/09/20/books/review/stephen-breyers-the-court-and-the-world.html|work=The New York Times|date=September 14, 2015}}</ref><ref>{{cite web|title=The Court and the World: American Law and the New Global Realities|url=http://www.penguinrandomhouse.com/books/253016/the-court-and-the-world-by-stephen-breyer/|publisher=Penguin Random House|accessdate=October 27, 2015}}</ref>

===Other views===
In an interview on ''[[Fox News Sunday]]'' on December 12, 2010, Breyer stated that based on the values and the historical record, the [[Founding Fathers of the United States]] never intended guns to go unregulated and that history supports his and the other dissenters' views in ''[[District of Columbia v. Heller]]''. He summarized:
{{Quote|We're acting as judges. If we're going to decide everything on the basis of history – by the way, what is the scope of the right to keep and bear arms? Machine guns? Torpedoes? Handguns? Are you a sportsman? Do you like to shoot pistols at targets? Well, get on the subway and go to Maryland. There is no problem, I don't think, for anyone who really wants to have a gun.<ref>{{cite news |url=http://www.foxnews.com/politics/2010/12/12/breyer-founding-fathers-allowed-restrictions-guns/#content |title=Breyer: Founding Fathers Would Have Allowed Restrictions on Guns |publisher= Fox News Channel |date=December 12, 2010 |accessdate=April 2, 2011}}</ref>}}

In the wake of the controversy<ref>Nagraj, Neil (January 28, 2010) [http://www.nydailynews.com/news/politics/2010/01/28/2010-01-28_justice_alito_mouths_not_true_when_obama_blasts_supreme_court_ruling_in_state_of.html "Justice Alito mouths 'not true' when Obama blasts Supreme Court ruling in State of the Union address"], ''[[Daily News (New York)|New York Daily News]]''</ref> over Justice [[Samuel Alito]]'s [[2010 State of the Union Address#Justice Alito's response|reaction]] to President [[Barack Obama]]'s [[Citizens United v. FEC#Politicians and political parties|criticism]] of the Court's ''[[Citizens United v. FEC]]'' ruling in his [[2010 State of the Union Address|2010]] [[State of the Union Address]], Breyer said he would continue to attend the address:{{quote|I think it's very, very, very important – very important – for us to show up at that State of the Union, because people today are more and more visual. What [people] see in front of them at the State of the Union is that federal government. And I would like them to see the judges too, because federal judges are also a part of that government.<ref>Blake, Aaron (December 12, 2010) [http://voices.washingtonpost.com/44/2010/12/breyer-ill-go-to-state-of-the.html "Justice Breyer: I'll go to State of the Union"], ''[[The Washington Post]]''</ref>}}

===Honors===
In 2007, Breyer was honored with the [[Distinguished Eagle Scout Award]] by the [[Boy Scouts of America]].<ref name="breyerDE">{{Cite journal| title = Distinguished Eagle Scout Award | journal=Scouting | issue = November&nbsp;– December 2007| page = 10| year = 2007 | url = http://www.scoutingmagazine.org/issues/0711/d-news.html |accessdate=November 1, 2007}}</ref>

==See also==
{{Portal|Biography|Supreme Court of the United States}}
*[[Bill Clinton Supreme Court candidates]]
*[[Demographics of the Supreme Court of the United States]]
*[[List of Justices of the Supreme Court of the United States]]
*[[List of law clerks of the Supreme Court of the United States]]
*[[List of U.S. Supreme Court Justices by time in office]]
*[[List of United States Supreme Court cases by the Rehnquist Court|United States Supreme Court cases during the Rehnquist Court]]
*[[List of United States Supreme Court cases by the Roberts Court|United States Supreme Court cases during the Roberts Court]]

==Footnotes==
{{reflist|colwidth=30em}}

==Further reading==
* [[Clinton, Bill]] (2005). ''[[My Life (Bill Clinton autobiography)|My Life]]''. Vintage. ISBN 1-4000-3003-X.
* Stephen Breyer, ''The Federal Sentencing Guidelines and Key Compromises on Which They Rest'', 17 Hofstra L. Rev. 1 (1988)
* Ronald Collins, "[http://www.concurringopinions.com/archives/2014/02/hypothetically-speaking-justice-breyers-dialectical-propensities.html Hypothetically Speaking: Justice Breyer’s Dialectical Propensities]" (Concurring Opinions Blog, February 28, 2014

==External links==
{{Sister project links|wikt=no|b=no|q=Stephen Breyer|s=author:Stephen Breyer|commons=Category:Stephen Breyer|n=no|v=no|species=no}}
{{FJC Bio}}
* {{Ballotpedia|Stephen_Breyer}}
* [http://www.OnTheIssues.org/Stephen_Breyer.htm Issue positions and quotes] at [[OnTheIssues]]
* {{C-SPAN|stephengbreyer}}
* [http://www.logosjournal.com/issue_5.2/braun.htm Review of Stephen Breyer's Active Liberty: Interpreting our Democratic Constitution]
* [http://www.newenglishreview.org/custpage.cfm?frm=3637&sec_id=3637 'Stephen Breyer, the court's necromancer'], a book review of ''Active Liberty: Interpreting Our Democratic Constitution'' in the New English Review
* [http://www.npr.org/templates/story/story.php?storyId=4965766 "'Active Liberty' from Justice Stephen Breyer"], October 20, 2005 NPR's ''Fresh Air''
* [http://www.npr.org/templates/story/story.php?storyId=4929668 "Supreme Court Justice Breyer on 'Active Liberty'" Part 1 of Interview], September 29, 2005 NPR's ''Morning Edition''
* [http://www.npr.org/templates/story/story.php?storyId=4930456 "Justice Breyer: The Case Against 'Originalists'" Part 2 of Interview], September 30, 2005 NPR's ''Morning Edition''
*[http://www.npr.org/templates/rundowns/rundown.php?prgId=35&prgDate=03-24-2007&view=storyview Justice Breyer's appearance] on NPR's quiz show ''Wait Wait... Don't Tell Me'', March 24, 2007
* [http://forum.wgbh.org/wgbh/forum.php?lecture_id=1274 WGBH Forum Network: one and a half hours with US Supreme Court Justice of Law Stephen Breyer, September 8, 2003]
* {{Internet Archive film clip|id=openmind_ep1667|description="The Open Mind - "Active Liberty" by Mr. Justice Breyer, Part I (2005)"}}
* {{Internet Archive film clip|id=openmind_ep1668|description="The Open Mind - "Active Liberty" by Mr. Justice Breyer, Part II (2005)"}}

{{s-start}}
{{s-legal}}
{{s-new|office}}
{{s-ttl|title=Judge of the [[United States Court of Appeals for the First Circuit]]|years=1980–1994}}
{{s-aft|after=[[Sandra Lynch]]}}
|-
{{s-bef|before=[[Levin H. Campbell|Levin Campbell]]}}
{{s-ttl|title=Chief Judge of the [[United States Court of Appeals for the First Circuit]]|years=1990–1994}}
{{s-aft|after=[[Juan R. Torruella|Juan Tourruella]]}}
|-
{{s-bef|before=[[Harry Blackmun]]}}
{{s-ttl|title=[[Associate Justice of the Supreme Court of the United States]]|years=1994–present}}
{{s-inc}}
|-
{{s-prec|usa}}
{{s-bef|before=[[Ruth Bader Ginsburg]]|as=Associate Justice of the Supreme Court}}
{{s-ttl|title=[[United States order of precedence|Order of Precedence of the United States]]<br>''{{small|as Associate Justice of the Supreme Court}}''|years=}}
{{s-aft|after=[[Samuel Alito]]|as=Associate Justice of the Supreme Court}}
{{s-end}}

{{stephenbreyeropinions}}
{{start U.S. Supreme Court composition| CJ=[[William Rehnquist|Rehnquist]]}}
{{U.S. Supreme Court composition court lifespan|cj=William Hubbs Rehnquist|years=1986–2005}}
{{U.S. Supreme Court composition 1994–2005}}
{{U.S. Supreme Court composition CJ| CJ=[[John Glover Roberts, Jr.|Roberts]]}}
{{U.S. Supreme Court composition court lifespan|cj=John Glover Roberts, Jr.|years=2005–present}}
{{U.S. Supreme Court composition 2005–2006}}
{{U.S. Supreme Court composition 2006–2009}}
{{U.S. Supreme Court composition 2009–2010}}
{{U.S. Supreme Court composition 2010–2016}}
{{U.S. Supreme Court composition 2017–present}}
{{end U.S. Supreme Court composition}}
{{Authority control}}

{{DEFAULTSORT:Breyer, Stephen}}
[[Category:1938 births]]
[[Category:20th-century American judges]]
[[Category:21st-century American judges]]
[[Category:Alumni of Magdalen College, Oxford]]
[[Category:American legal scholars]]
[[Category:American people of Romanian-Jewish descent]]
[[Category:California lawyers]]
[[Category:Distinguished Eagle Scouts]]
[[Category:Harvard Law School alumni]]
[[Category:Harvard Law School faculty]]
[[Category:Judges of the United States Court of Appeals for the First Circuit]]
[[Category:Law clerks of the Supreme Court of the United States]]
[[Category:Lawyers from San Francisco]]
[[Category:Légion d'honneur recipients]]
[[Category:Living people]]
[[Category:Marshall Scholars]]
[[Category:Members of the United States Sentencing Commission]]
[[Category:Scholars of administrative law]]
[[Category:Stanford University alumni]]
[[Category:Tulane University Law School faculty]]
[[Category:United States court of appeals judges appointed by Jimmy Carter]]
[[Category:United States federal judges appointed by Bill Clinton]]
[[Category:United States Supreme Court justices]]