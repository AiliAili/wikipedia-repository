{{Use Australian English|date=May 2015}}
{{Use dmy dates|date=May 2015}}
{{Infobox person
| name                  = Edward "Teddy" Howell
| image                 =
| imagesize             =
| caption               = 
| birthname             =Edward Welsford Rowsell Howell
| birth_date            = {{birth date|1902|6|15|df=yes}}
| birth_place           = Bromley, Kent, England
| death_date            = {{Death date and age|1986|8|20|1902|6|15|df=yes}}
| death_place           = [[Sydney, New South Wales]], Australia
| death_cause           = 
| occupation            = actor, writer, director and producer
| years_active          = 1910–1986
| spouse                = Mary Cecillia Long (known professionally as Therese Desmond) (m-1927-1961)
| children              =Madeline Howell
| domesticpartner       =
| website               =
}}

'''Edward Welsford Rowsell "Teddy" Howell''' (15 June 1902 – 20 August 1986) was a [[British Australian]], character actor, radio producer and director and scriptwriter. He was notable for his career in Australia all genres of the entertainment industry including radio, stage, television and film. In 1927 he appeared in the early Australian film ''[[For the Term of His Natural Life]]'', at the time the highest-grossing film in Australian cinema.<ref name=ADB>{{cite web |url= http://adb.anu.edu.au/biography/howell-edward-welsford-teddy-12660|title= Howell, Edward Welsford (Teddy) (1902–1986)|last1= Arrow |first1= Michelle |date= 2007|website= Australian Dictionary of Biography |publisher= |access-date= 22 May 2015|quote=}}</ref>

==Early life==
Howell was born on 15 July 1902 in [[Bromley, Kent]], England, the youngest son of bank clerk and actor Edwin Gilburt Howell and his wife Madeleine Ann (née Rowsell).<ref name=ADB/>

==Early roles==
As an eight year old in 1912, he was brought to Australia with his brother, Lewis, and father to appear in [[J. C. Williamson]]'s stage production of the [[Maurice Maeterlinck]] play, ''[[The Blue Bird (play)|The Blue Bird]]''.<ref name=SMH>{{cite news |last= |first= |date= 22 August 1986|title= Teddy Howell, theatre and film actor, dies |url= https://news.google.com/newspapers?nid=1301&dat=19860822&id=yqpWAAAAIBAJ&sjid=LOgDAAAAIBAJ&pg=3132,4607416&hl=en|newspaper=The Sydney Morning Herald |location= |access-date= 22 May 2015}}</ref> After the family decided to stay in Australia permanently, he completed his education at [[Sydney Grammar School|Sydney Grammar]]. With his father moving to settle in [[Suva]], young Ted soon followed, studying law while working in the government's legal department, before joining the Colonial Sugar Refining Co. Ltd.<ref name=ADB/>

==Theatre ==
Whilst in Suva, Edward and father Edwin founded the Suva Dramatic Actor Guild. He returned to Australia in 1924 and joined the Playbox Theatre,<ref name="Fred and Maggie : radio program">{{Citation | author1=Howell, Edward | title=Fred and Maggie : radio program | publication-date=1900 | url=http://trove.nla.gov.au/work/35652462 | accessdate=23 May 2015 }}</ref> and later, with his wife Molly, ran Sydney's (Royal) Academy of Dramatic Arts.<ref>{{cite news |url=http://nla.gov.au/nla.news-article165957564 |title=Academy of Dramatic Art. |newspaper=[[The Sydney Mail|Sydney Mail (NSW : 1912 - 1938)]] |location=NSW |date=5 April 1933 |accessdate=23 May 2015 |page=11 |publisher=National Library of Australia}}</ref>

==Radio==
In 1929, he began a career in radio when he was asked by the Australian Broadcasting Corporation (then Commission) to produce a play for the network. As an author of one of the first successful variety shows, he had a very prominent career in the sector as a writer, producer and director, as well as appearing in productions as an actor. He was best known as the creator and visionary behind the popular long-running serial ''Fred and Maggie Everybody'',<ref name="Fred and Maggie : radio program"/><ref>{{cite news |url=http://nla.gov.au/nla.news-article107293616 |title=FRED AND MAGGIE'S 1200th. |newspaper=[[The Cumberland Argus and Fruitgrowers’ Advocate|The Cumberland Argus and Fruitgrowers Advocate (Parramatta, NSW : 1888 - 1950)]] |location=Parramatta, NSW |date=12 November 1941 |accessdate=23 May 2015 |page=12 |publisher=National Library of Australia}}</ref> that ran under a number of titles between 1932 and 1953. The series depicted the life of a middle class couple played by Edward and his wife, Molly. At its height it was heard on fifty six stations throughout Australia and was sold to numerous countries including New Zealand.

Edward worked for [[Amalgamated Wireless (Australasia)|Amalgamated Wireless]] (AWA), where he served as the chief producer of drama, before going freelance as producer and actor. In 1949, he returned to his native England and took up a post at the BBC, writing and producing radio productions as well as stage plays, and returned to Sydney in 1950, where he continued his radio and stage career as a prominent scriptwriter.<ref>{{Citation | author1=Howell, Edward | author2=De Berg, Hazel, 1913-1984. (Interviewer) | title=Edward Howell interviewed by Hazel de Berg in the Hazel de Berg collection | publication-date=1978 | url=http://trove.nla.gov.au/work/12932016 | accessdate=23 May 2015 }}</ref>

==Television and film==
After a lengthy career in radio and on stage he had a prominent career on the television, appearing  in numerous Australian serials, including ''[[My Name's McGooley, What's Yours?]], [[Skippy the Bush Kangaroo]], [[Homicide (Australian TV series)|Homicide]]'' and ''[[Division 4]]''. He was best known for his recurring role as Bert Griffiths in the long-running rural soap ''[[A Country Practice]]''.

In film he appeared in ''[[For the Term of His Natural Life (1927 film)|For the Term of his Natural  Life]], [[The Cars That Ate Paris]]'' and ''[[Careful, He Might Hear You]]''.<ref name=ADB/>

==Personal life ==
He was married to Mary Cecilia Long on 11 May 1927, an English actress known professionally as Therese Desmond,<ref>{{Citation | author1=Hood, Sam, 1872-1953 | title=2CH Old Time Dance at the Town Hall. Therese Desmond being presented with a bouquet | publication-date=1938 | url=http://trove.nla.gov.au/work/11298994 | accessdate=23 May 2015 }}</ref> and nicknamed Molly, whom he had met whilst appearing with Sydney's Playbox Theatre, marrying at the St. Mary's Catholic Cathedral in Sydney, Australia. Molly suffered a stroke in 1955 and died in 1961.<ref name=ADB/> Edward died on 20 August 1986, in [[Chatswood, New South Wales]] at the age of 84, and was cremated.<ref name=SMH/>

==References==
{{reflist}}

==External links==
*National Library of Australia collection of newspaper and journal cuttings about Teddy Howell. NLA reference number [http://catalogue.nla.gov.au/Record/1930901?lookfor=subject:%22Howell,%20Teddy.%22&offset=1&max=1 42654244]
*{{IMDb name|id=0397968|name=Edward Howell}}

{{DEFAULTSORT:Howell, Edward}}
[[Category:Australian radio personalities]]
[[Category:1902 births]]
[[Category:Australian male television actors]]
[[Category:Australian male film actors]]
[[Category:Australian male radio actors]]
[[Category:20th-century Australian male actors]]
[[Category:1986 deaths]]
[[Category:English emigrants to Australia]]