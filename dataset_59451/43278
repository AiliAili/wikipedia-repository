<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Type H
  |image = MS H LeB 09.05.07R.jpg
  |caption = Morane Saulnier Type H on display at the [[Musée de l'Air et de l'Espace]] at Paris Le Bourget airport
}}{{Infobox Aircraft Type
  |type = Sport aircraft
  |manufacturer = [[Morane-Saulnier]]
  |designer = 
  |first flight = 1913
  |introduced = 
  |retired = 
  |status =
  |primary user = 
  |more users =  
  |produced = 
  |number built =
  |unit cost = 
  |developed from = [[Morane-Saulnier G]]
  |variants with their own articles = [[Morane-Saulnier L]]
}}
|}

The '''Morane-Saulnier H''' was a sport aircraft produced in France in the years before the First World War,<ref name="JEA">Taylor 1989, p.648</ref><ref name="IEA">"The Illustrated Encyclopedia of Aircraft", p.2539</ref> a single-seat derivative of the successful [[Morane-Saulnier G]] with a slightly reduced wingspan<ref name="IEA" /> Like the Type G, it was a successful sporting and racing aircraft.
[[File:AL-88 Al Menasco Album Image 000143 (14174596079).jpg|right|thumb|Albert Menasco in his Morane-Saulnier H, during his Asian tour]]
==Operational history==
During the second international aero meet, held at [[Wiener Neustadt]] in June 1913, [[Roland Garros (aviator)|Roland Garros]] won the precision landing prize in a Type H.<ref name="Hartmann 11">Hartmann 2001, 11</ref> Later that same year, A Morane-Saulnier H was used to complete the first non-stop flight across the [[Mediterranean Sea|Mediterranean]], from [[Fréjus]] in the south of France to [[Bizerte]] in [[Tunisia]].<ref>[http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201052.html Flying the Mediterranean] [[Flight International|''Flight'']] 27 September 1913</ref>

The French Army ordered a batch of 26 aircraft, and the British Royal Flying Corps also acquired a small number, these latter machines purchased from [[Grahame-White]], who was manufacturing the type in the UK under licence.<ref name="IEA" /> The French machines saw limited service in the opening stages of World War I, with pilots engaging in aerial combat using revolvers and carbines.<ref name="IEA" />

The type was also produced under licence in Germany by [[Pfalz Flugzeugwerke]], who built it as the '''E.I''', '''E.II''', '''E.IV''', '''E.V''', and '''E.VI''', with increasingly powerful engines.<ref name="IEA P">''The Illustrated Encyclopedia of Aircraft'', p.2698</ref><ref name="Grosz">Grosz 1996</ref> These were armed with a single, synchronised [[MG 08#Aircraft versions|lMG 08]] machine gun.<ref name="IEA P"/><ref name="Grosz">Grosz 1996</ref>

Another slightly longer German-built copy featured a steel-framed fuselage, a redesigned undercarriage integrated with the under-wing bracing pylons and a comma shaped rudder. It entered production as the [[Fokker M.5]] and when armed in 1915 with a [[synchronization gear|synchronised]] machine gun became first of the [[Fokker Eindecker|Fokker "Eindecker" monoplane fighters]].<ref>Brannon (1996), pp.7-9</ref>

==Survivors==
A Type H is preserved at the [[Musée de l'Air et de l'Espace]] in [[Le Bourget]].

==Variants==
===Morane-Saulnier versions===
* '''MoS.1 H''' (single seater)
* '''MoS.2 G'''  (two seater)
* '''MoS.3 L'''  (parasol monoplane)
* '''MoS.13 M'''  (armoured single seater)

===Pfalz versions===
* '''E.I''' - with [[Oberursel U.0]] rotary engine (45 built)<ref name="Herris">Herris 2001, p.10</ref>
* '''E.II''' - with [[Oberursel U.I]] rotary engine (130 built)<ref name="Herris">Herris 2001, p.16</ref>
* '''E.IV''' - with [[Oberursel U.III]] rotary engine (46 built)<ref name="Herris">Herris 2001, p.21</ref>
* '''E.V''' - with [[Mercedes D.I]] water-cooled, inline engine (20 built)<ref name="Herris">Herris 2001, p.24</ref>
* '''E.VI''' - with Oberursel U.I engine, lengthened fuselage, enlarged tail fin and reduced bracing (20 built as trainers)<ref name="Herris">Herris 2001, p.25</ref><ref name="Grosz 27">Grosz 1996, p.27</ref>

== Operators ==
[[File:Morane-Saulnier H with RFC number on rudder.jpg|thumb|Morane-Saulnier H with RFC number on rudder]]
; {{FRA}}
* ''[[Aéronautique Militaire]]''
; {{flag|Austria-Hungary}} 
* [[Austro-Hungarian Navy]] - (Pfalz-built versions)
; {{BEL}}
* [[Belgian Air Force]]
; {{DNK}}
* [[Royal Danish Air Force|Army Flying Service]] - 2 examples.
; {{flag|Germany|empire}}
* ''[[Luftstreitkräfte]]'' - (Pfalz-built versions)
; {{POR}}
* [[Portuguese Air Force]] - one aircraft.
; {{UK}}
* [[Royal Flying Corps]]
; {{flag|Russian Empire|name=Russia}}
* [[Imperial Russian Air Service]]
; {{CHE}}
* [[Swiss Air Force]] - two aircraft

==Specifications==
{{aerospecs
|ref=<!-- reference -->flugzeuginfo.net
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=One pilot
|capacity=
|length m=5.84
|length ft=19
|length in=2
|span m=9.12
|span ft=29
|span in=11
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.26
|height ft=7
|height in=5
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=188
|empty weight lb=415
|gross weight kg=444
|gross weight lb=979
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Le Rhône|Le Rhône 9C]]
|eng1 kw=<!-- prop engines -->60
|eng1 hp=<!-- prop engines -->80
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=120
|max speed mph=75
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=177
|range miles=111
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=1,000
|ceiling ft=3,280
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category|Morane-Saulnier H}}
===Bibliography===
* {{cite book |last=Brannon |first=D. Edgar|title=Fokker Eindecker in Action|publisher=Squadron/Signal Publication|location=Carrolton, Texas|year=1996 }}
* {{cite book|last1=Davilla|first1=Dr. James J.|last2=Soltan|first2=Arthur |title=French Aircraft of the First World War|publisher=Flying Machines Press|location=Mountain View, CA|year=1997|isbn=978-1891268090}}
* {{cite book |last=Grosz |first=P.M. |title=Pfalz E.I–E.VI |publisher=Albatros Publications |location=Berkhamsted, Hertfordshire |year=1996 }}
* {{cite web |last=Hartmann |first=Gérard |title=L'incroyable Morane-Saulnier hydro |work=La Coupe Schneider et hydravions anciens/Dossiers historiques hydravions et moteurs |year=2001 |url=http://www.hydroretro.net/etudegh/moranesaulnierhydro.pdf |accessdate=2008-11-07}}
*{{cite book |last=Herris |first=Jack |title=Pflaz Aircraft of World War I |year=2001 |publisher=Flying Machines Press |location=Boulder, Colorado |isbn=1-891268-15-5}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London }}
* {{cite web |title=Morane-Saulnier Type H |work=flugzeuginfo.net |url=http://www.flugzeuginfo.net/acdata_php/acdata_morane_h_en.php |accessdate=2008-11-07}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London }}
*{{cite book| last = Angelucci| first = Enzo|title = The Rand McNally encyclopedia of military aircraft, 1914-1980| publisher = The Military Press| year = 1983| pages = 20| isbn = 0-517-41021 4 }}
<!-- == External links == -->

{{Morane-Saulnier aircraft}}
{{Idflieg E-class designations}}

[[Category:French sport aircraft 1910–1919]]
[[Category:Morane-Saulnier aircraft|H]]
[[Category:French military trainer aircraft 1910–1919]]
[[Category:French military reconnaissance aircraft 1910–1919]]