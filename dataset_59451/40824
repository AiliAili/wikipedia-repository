{{Infobox religious building
| building_name         =Zion Plovdiv Synagogue
| infobox_width         =
| image                 =Synagogue in Plovdiv_D.jpg
| image_size            =
| alt                   =The Synagogue in Plovdiv
| caption               =The Synagogue in Plovdiv
| map_type              =
| map_size              =
| map_caption           =
| location              ={{flagicon|Bulgaria}} [[Plovdiv]], [[Bulgaria]]
| geo                   = {{Coord|42|9|2|N|24|44|27|E|type:landmark_region:BG|display=inline,title}}
| latitude              =
| longitude             =
| coord_region          =
| religious_affiliation =
| rite                  =[[Sephardi Jews]]
| region                =[[Romaniotes|Romaniote]]
| state                 =
| province              =
| territory             =
| prefecture            =
| sector                =
| district              =
| cercle                =
| municipality          =
| consecration_year     =
| status                = active
| functional_status     =
| heritage_designation  =
| leadership            =
| website               =
| architecture          =
| architect             =
| architecture_type     =
| architecture_style    = Ottoman
| founded_by            =
| funded_by             =
| general_contractor    =
| facade_direction      =
| groundbreaking        =
| year_completed        = 1892<ref name="Jenc"/>
| construction_cost     =
| specifications        =
| capacity              =
| length                =
| width                 =
| width_nave            =
| height_max            =
| dome_quantity         =
| dome_height_outer     =
| dome_height_inner     =
| dome_dia_outer        =
| dome_dia_inner        =
| minaret_quantity      =
| minaret_height        =
| spire_quantity        =
| spire_height          =
| materials             =
| nrhp                  =
| added                 =
| refnum                =
| designated            =
}}

The '''Zion Plovdiv Synagogue ''' is a [[synagogue]] in the city of [[Plovdiv]] located in [[Bulgaria]]. This synagogue is one of the only 2 synagogues that remain active to this day in Bulgaria (with the [[Sofia Synagogue]]).

==History==
According to the archaeological research a Synagogue had been constructed in ancient [[Plovdiv|Philippopolis]] dating back to the reign of
Emperor [[Alexander Severus]] in the first half of 3rd century AD.<ref>{{cite web|last=Hazan|first=Elko Z.|title=Synagogues in Bulgaria: A testimony of eighteen centuries of Jewish presence in the Balkans|url=http://www.thebulgarianjews.org.il/_Uploads/dbsAttachedFiles/Hazan.pdf|accessdate=3 January 2013}}</ref> It is followed by several renovations, the last one – from the beginning of 5th century (M. Martinova).<ref>{{cite web| url=http://wikimapia.org/24209233/Synagogue |title=Synagogue (Plovdiv)}}</ref> In 1360, when the city was conquered by the Turks  certain Jews who emigrated from Aragon in 1492 settled in Philippopolis and built a synagogue called "K. K. Aragon," which was standing in 1540, but is no longer in existence. In 1892<ref name="Jenc">{{Jewish Encyclopedia|article=PHILIPPOPOLIS |url=http://www.jewishencyclopedia.com/articles/12104-philippopolis|accessdate=3 January 2013}}</ref> following Bulgaria liberation from Ottoman domination in 1878 one of the first synagogues to be erected was the (Zion) Synagogue in Plovdiv. It was built in the remnants of a small courtyard in what was once a large Jewish quarter called '''Orta Mezar''' during the Turkish rule. The location of the Sephardic synagogue is now called Tsar Kaloyan Street 13. Before the Second World War, the Jewish quarter had a population of 7000.<ref name="Singer"/> The Synagogue is one of the best-preserved examples of the so-called "Ottoman-style" synagogues in the Balkans. According to author [[Ruth E. Gruber]], the interior is a "hidden treasure…a glorious, if run-down, burst of color."{{Citation needed|date=January 2013}} An exquisite Venetian glass chandelier hangs from the center of the ceiling, which has a richly painted dome. All surfaces are covered in elaborate, Moorish-style, geometric designs in once-bright greens and blues. Torah scrolls are kept in the gilded Aron-ha-Kodesh.<ref>{{cite web|url=http://www.heritageabroad.gov/projects/bulgaria2.html |title=Synagogue of Plovdiv, Bulgaria |publisher=Heritageabroad.gov |date=5 October 2009 |accessdate=14 June 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20090907192519/http://www.heritageabroad.gov/projects/bulgaria2.html |archivedate=September 7, 2009 }}</ref>

In 1904 the Jewish community possessed three other synagogues: Jeshurun, built in 1710 according to the inscription on a marble slab in the synagogue; Ahabat-Shalom, built in 1880; Shebeṭ Aḥim or Mafṭirim, founded in 1882 by emigrants from Karlovo, whence the Jews fled during the Turko-Russian war (1877-1878).<ref name="Jenc"/>

===Rabbis===
Since the end of the eighteenth century the following have been chief rabbis of the city:
* '''[[Abraham Sidi]]''' (according to Zedner, l.c. p.&nbsp;397, "Sa'id"; 1790-1810);
* [[Judah Sidi]] (1810–12), brother of the preceding, and author of ''[[Ot Emet]]'', on the laws relating to reading the Torah, Salonica, 1799; and of ''[[Ner Miẓwah]]'', on [[Maimonides]]' ''[[Yad]]'' and his [[Sefer ha-Miẓwot]]'', with indexes to the hermeneutic works of Solomon and Israel [[Jacob Algazi]], ib. 1810-11;
* '''[[Abraham ibn Aroglio]]''' (1812–19);
* '''[[Abraham Ventura]]''' (1823–29);
* '''[[Moses ha-Levi]]''' (1830–32);
* '''[[Jacob Finzi]]''' (1832–33);
* '''[[Ḥayyim ibn Aroglio]]''' (1833–57), with [[Abraham ibn Aroglio]] joint author of ''[[Mayim ha-Ḥayyim]]'', responsa, Salonica, 1846;
* '''[[Moses Behmoiras]]''' (1857–76); [[Ḥayyim Meborah]] (1876-92);
* '''[[Ezra Benaroyo]]''' who has held office since 1892.<ref name="Jenc"/>
* '''[[Shmuel Behar]] <ref>{{cite book|last=Paldiel|first=Mordecai|title=Churches and the Holocaust : unholy teaching, good samaritans, and reconciliation|year=2006|publisher=Ktav|location=Jersey City, NJ|isbn=088125908X|url=https://books.google.com/books?id=psleDQml1WsC|page=308}}</ref>

==Legacy==
Nowadays, the Jewish community in Bulgaria is very small (863 in 1994)<ref name="Singer"/> because of the [[Holocaust]], [[secularity]] of the local Jewish population due to many years of communism and subsequent [[Aliyah|Aliya]] (Jewish immigration to Israel).

In 1994 the synagogue was mostly inactive.<ref name="Singer">{{cite news|last=Natasha Singer|title=Unearthing Bulgarian Jewry in Communism's Rubble.|url=http://www.highbeam.com/doc/1P1-2285793.html|accessdate=3 January 2013|newspaper=Forward|date=06-03-1994}}</ref> but the community is undergoing a revival <ref>{{cite web|last=Astaire|first=Libi|title=Unlocking  Plovdiv’s Past|url=http://halachicadventures.com/wp-content/uploads/2009/09/ari.pdf|work=MISHPACHA|accessdate=4 January 2013}}</ref>
In 2003 the  synagogue was  restored. The city's mayor, the U.S. and Israeli ambassadors to Bulgaria, were present at its  inauguration.
The funding for the restoration of the 19th-century Zion Synagogue. was raised by the [[U.S. Commission for the Preservation of America's Heritage Abroad]] (USD 26,000) <ref>{{cite web|title=Synagogue of Plovdiv, Bulgaria|url=http://www.heritageabroad.gov/Projects/SynagogueofPlovdivBulgaria.aspx|accessdate=3 January 2013}}</ref> and the
London-based Hanadiv Charitable Foundation.<ref>{{cite news|title=News at a Glance|url=http://www.highbeam.com/doc/1P1-88510964.html|accessdate=3 January 2013|newspaper=Jewish Telegraphic Agency|date=December 11, 2003}}{{Paywall}}</ref>

==Photo gallery==
<gallery>
File:PLOVDIVSY1.jpg
File:PLOVDIVSY2.jpg
File:PLOVDIVSY3.jpg|Detail of the dome
File:Synagogue in Plovdiv B.jpg
File:Synagogue in Plovdiv C.jpg
File:Synagogue in Plovdiv D.jpg
File:Synagogue in Plovdiv F.jpg
File:Synagogue in Plovdiv G.jpg
File:Synagogue in Plovdiv H.jpg
File:Synagogue in Plovdiv J.jpg
File:Synagogue in Plovdiv K.jpg
File:Synagogue in Plovdiv L.jpg
File:Synagogue in Plovdiv M.jpg
File:Synagogue in Plovdiv N.jpg
File:Synagogue in Plovdiv O.jpg
File:Synagogue in Plovdiv P.jpg
File:The Synagogue Entrance.jpg
File:The Synagogue in Plovdiv a.jpg
</gallery>

==See also==
* [[Plovdiv]]
* [[Sofia Synagogue]]
* [[History of the Jews in Bulgaria]]
* [[List of synagogues in Bulgaria]]

==References==
{{commons category|The Synagogue in Plovdiv}}
{{Reflist}}

{{Plovdiv}}

[[Category:Buildings and structures in Plovdiv]]
[[Category:Tourist attractions in Plovdiv]]
[[Category:Romaniote synagogues]]
[[Category:Sephardi Jewish culture in Europe]]
[[Category:Sephardi synagogues]]
[[Category:Synagogues in Bulgaria]]
[[Category:Ottoman architecture in Bulgaria]]
[[Category:Culture in Plovdiv]]