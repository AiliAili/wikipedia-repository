{{Infobox journal
| title = Personal and Ubiquitous Computing
| cover =PUC.png
| editor = [[Peter James Thomas]]
| discipline = [[Ubiquitous computing]]
| abbreviation =
| formernames = Personal Technologies
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Bimonthly
| history = 1997–present
| impact = 1.498
| impact-year = 2015
| openaccess = Partly
| license = [[Creative Commons licenses|CC-BY-NC 2.5]]
| website = http://www.springer.com/computer/hci/journal/779
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=1617-4909&issue=current
| link1-name = Online access
| ISSN = 1617-4909
| eISSN = 1617-4917
}}
'''''Personal and Ubiquitous Computing''''' is a [[peer review|peer-reviewed]] [[scientific journal]] that was established in 1997. It covers [[original research]] on [[ubiquitous computing|ubiquitous and pervasive computing]], [[ambient intelligence]], and handheld, [[Wearable computer|wearable]] and [[Mobile computing|mobile information devices]], with a focus on [[user experience]] and [[interaction design]] issues. The journal publishes a mixture of issues themed on specific topics, or organised around scientific workshops, and original research papers.

[[Google Scholar]] reports the h-index as 36 (h-median:57). According to ''[[Journal Citation Reports]]'', the journal has a 2016 [[impact factor]] of 1.498 (5-year IF 1.461).

PUC has a team of 23 Editors and an editorial board of 26 academic and commercial researchers. The [[editor-in-chief]] is [[Peter James Thomas|Peter Thomas]] (Manifesto Group). The journal is published by [[Springer Science+Business Media]].

== Editors ==

EDITOR-IN-CHIEF
* Peter Thomas Manifesto Group / Brunel University / The [[Leasing Foundation]]

EDITORS
* Stephen Brewster University of Glasgow, UK
* Anind K. Dey Carnegie Mellon University, USA 
* Hans-W. Gellersen Lancaster University, UK 
* Lars Erik Holmquist  Södertörn University, Sweden
* Matt Jones University of Wales, Swansea, UK 
* Jofish Kaye, Yahoo Labs, US
* Chris Schmandt MIT Media Lab, US 
* Rob Macredie Brunel University, UK
* Phil Stenton BBC, UK
*[http://nottingham.academia.edu/AlanChamberlain Alan Chamberlain], University of Nottingham, UK
* [[Albrecht Schmidt (computer scientist)|Albrecht Schmidt]], University of Stuttgart, Germany
* Alexandra Weilenmann, University of Gothenburg, Sweden
* Bin Guo, Northwestern Polytechnical University, China
* Eija Kaasinen, VTT, Finland
* Li Bo, Samsung Research America
* Emilia Barakova, Eindhoven University of Technology, Netherlands
* Enrico Rukzio, Lancaster University, UK
* Gillian Hayes, UC Irvine, USA
* Mark Dunlop University of Glasgow, UK
* Mikael Wiberg, Umeå University, Sweden
* Yunchuan Sun, Beijing Normal University, China
* Zhiwen Yu, Northwestern Polytechnical University, China

EDITORIAL BOARD
* Bert Arnrich, Bogazici University, Turkey
* Tilde Bekker, Eindhoven University of Technology, The Netherlands
* [[Victoria Bellotti]], Xerox PARC, USA
* Rongfang Bie, Beijing Normal University, China
* Mark Billinghurst, University of Washington, USA
* Jose Bravo, University of Castilla-La Mancha, Spain
* Luca Chittaro, Universita di Udine, Italy
* Paul Dourish, University of California, USA
* Gheorghita Ghinea, Brunel University, UK
* Karamjit S. Gill, University of Wales, Newport, UK
* Kostas Karpouzis, University of the Aegean, Greece
* James Katz, Bellcore, USA
* [[Richard Ling]], [[Nanyang Technological University]], Singapore and [[Telenor]] R&D, Norway
* Patti Maes, MIT Media Laboratory, USA
* Tom Martin, Virginia Tech, USA
* Friedemann Mattern, ETH Zurich Inst Informationssyteme, Switzerland
* John McCarthy, University College Cork, Ireland
* Jose M. Noguera, University of Jaen, Spain
* Jong Hyuk Park, Seoul National University of Science and Technology (SeoulTech), Korea
* Reza Rawassizadeh, Dartmouth College, UK
* Boon-Chong Seet, Auckland University of Technology, New Zealand
* Elhadi M Shakshuki, Acadia University, Canada
* Yasuyuki Sumi, Kyoto University, Japan
* Harold Thimbleby, UCC, UK
* Chai-Wen Tsai, Ming Chuan University, Taiwan
* Bieke Zaman, KU Leuven - iMINDS, Belgium

== Abstracting and indexing ==
The journal is abstracted and indexed in [[InfoTrac|Academic OneFile]], [[EBSCO Publishing|Academic Search]], [[Compendex]], [[Computer Science Index]], [[Current Abstracts]], [[Current Contents]]/Engineering, Computing and Technology, [[Digital Bibliography & Library Project]], [[Ergonomics Abstracts]], [[Inspec]], [[io-port.net]], [[Science Citation Index|Science Citation Index Expanded]], and [[Scopus]].

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/computer/hci/journal/779}}

[[Category:English-language journals]]
[[Category:Ubiquitous computing]]
[[Category:Computer science journals]]
[[Category:Bimonthly journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1997]]