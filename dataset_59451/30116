{{Use mdy dates|date=April 2012}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Body Count
| Type        = studio
| Artist      = [[Body Count]]
| Cover       = Body Count Album Cover.jpg
| Released    = {{Start date|1992|3|31}}
| Recorded    = September–December 1991<br />One-on-One Recorders, Syndicate Studio West <br /><small>([[North Hollywood, California]])</small>
| Genre       =  {{flatlist|<!-- All genres are sourced in the article body. -->
*[[Speed metal]]
*[[thrash metal]]
*[[groove metal]]
}}
| Length      = 52:59
| Label       = [[Sire Records|Sire]]/[[Warner Bros. Records|Warner Bros.]]
| Producer    = {{flatlist|
*[[Ice-T]]
*[[Ernie C]].
}}
| This album  = '''''Body Count'''''<br/>(1992)
| Next album  = ''[[Born Dead]]''<br/>(1994)
 {{Singles
  | Name           = Body Count
  | Type           = studio
  | single 1       = [[There Goes the Neighborhood (Body Count song)|There Goes the Neighborhood]]
  | single 1 date  = 1992
  | single 2       = [[Cop Killer (song)|Cop Killer]]
  | single 2 date  = 1992
  }}
}}
'''''Body Count''''' is the eponymous debut album of American [[Heavy metal music|heavy metal]] band [[Body Count]]. Released on March 31, 1992, the album's material focuses on various social and political issues ranging from [[police brutality]] to drug abuse. The album presents a turning point in the career of [[Ice-T]], who co-wrote the album's songs with lead guitarist [[Ernie C]] and performed as the band's lead singer. Previously known only as a rapper, Ice-T's work with the band helped establish a [[Crossover (music)|crossover]] audience with rock music fans. The album produced one single, "[[There Goes the Neighborhood (Body Count song)|There Goes the Neighborhood]]".<ref name="GreatRockDiscography">{{cite book |last=Strong |first=Martin Charles |editor= |others= |title=The Great Rock Discography |origyear= |month= |url= |edition=7th |series= |year=2002 |publisher=Canongate |isbn=1-84195-615-5 |oclc= |id= |pages=724–725 |chapter= |chapterurl= |quote= }}</ref>

''Body Count'' is well known for the inclusion of the controversial song "[[Cop Killer (song)|Cop Killer]]", which was the subject of much criticism from various political figures, although many defended the song on the basis of the group's right to [[freedom of speech]]. Ice-T eventually chose to remove the song from the album,<ref name="IceOpinion">{{cite book |author=Ice T |author2=Sigmund, Heidi |editor= |others= |title=The Ice Opinion: Who Gives a Fuck? |origyear= |month= |url= |edition= |series= |year=1994 |publisher=St. Martin's Press |isbn=0-312-10486-3 |oclc= |id= |pages=99–101; 108; 166–180 |chapter= |chapterurl= |quote= }}</ref> although it continues to be performed live. It was voted the 31st best album of the year in ''[[The Village Voice]]''{{'}}s [[Pazz & Jop]] critics poll,<ref name="VillageVoice">{{cite news |title=The 40 Best Albums Of 1992 |url=http://www.robertchristgau.com/xg/pnj/pjres92.php |publisher=[[The Village Voice]] |id= |date=March 2, 1993 |quote= }}</ref> and is believed to have helped pave the way for the mainstream success of the [[rap metal]] genre,<ref name="RSdeaths">{{cite web |url=http://www.rollingstone.com/artists/icet/articles/story/6436921/body_count_guitarist_dead |title=Body Count Guitarist Dead |accessdate=2007-10-20 |last=Devenish |first=Colin |date=August 19, 2004 |work=Rolling Stone |quote= }}</ref><ref name="CNN">{{cite news |url=http://www.cnn.com/SHOWBIZ/Music/9910/27/ice.t/index.html?iref=newssearch |title=No thaw for rapper Ice T |accessdate=2007-10-20 |last=Freydkin |first=Donna |date=October 27, 1999 |publisher=CNN |quote= }}</ref> although the album itself does not feature [[rapping]] in any of its songs.<ref name=IceCentury-127/>

==Conception==
[[File:Body Count cover.jpg|thumb|left|upright|Body Count, as pictured in the album's liner notes.]]
[[Ice-T]] formed Body Count out of this interest.<ref name="IceOpinion"/> The band comprised musicians Ice-T had known from [[Crenshaw High School]].<ref name="AllMusic">{{cite web |url={{Allmusic|class=artist|id=p26209|pure_url=yes}} |title=Body Count Biography |accessdate=2007-10-20 |last=Erlewine |first=Stephen Thomas |publisher=Allmusic |quote= }}</ref> Ice-T states that "I knew we didn't want to form an R&B group. [...] Where am I gonna get the rage and the anger to attack something with that? [...] We knew Body Count had to be a rock band. The name alone negates the band from being R&B."<ref name="IceOpinion"/>

Ice-T co-wrote the band's music and lyrics with lead guitarist [[Ernie C]], and took on the duties of lead vocalist. Ice-T states that "I knew I couldn't sing, but then I thought, 'Who ''can'' sing in rock 'n' roll?'"<ref name="IceOpinion"/> Aside from Ice-T and Ernie C, the original line-up consisted of Mooseman on bass, [[Beatmaster V]] on drums and [[D-Roc The Executioner|D-Roc]] on rhythm guitar. According to Ice-T, "We named the group Body Count because every Sunday night in L.A., I'd watch the news, and the newscasters would tally up the youths killed in gang homicides that week and then just segue to sports. 'Is that all I am,' I thought, 'a body count?'"<ref name="IceOpinion"/>

Ice-T introduced the band at [[Lollapalooza]] in 1991, devoting half of his set to his hip hop songs, and half to Body Count songs, increasing his appeal with both [[alternative rock]] fans and middle-class teenagers.<ref>{{cite web |url={{Allmusic|class=artist|id=p89063|pure_url=yes}} |title=Ice-T Biography |accessdate=2007-10-20 |last=Erlewine |first=Stephen Thomas |publisher=Allmusic |quote= }}</ref> Many considered the Body Count performances to be the highlight of the tour.<ref>{{cite book |last=Apter |first=Jeff |editor= |others= |title=Fornication: The Red Hot Chili Peppers Story  |origyear= |month= |url= |edition= |series= |year=2004 |publisher=Omnibus Press |isbn=1-84449-381-4 |oclc= |id=  |chapter= |chapterurl= |quote=|page=250 }}</ref> The group made its first album appearance on Ice-T's 1991 solo album ''[[O.G. Original Gangster]]''. The song "Body Count" was preceded by a staged interview in which the performer referred to the group as a "black [[hardcore punk|hardcore]] band," stating that "as far as I'm concerned, music is music. I don't look at it as rock, R & B, or all that kind of stuff. I just look at it as music. [...] I do what I like and I happen to like rock 'n' roll, and I feel sorry for anybody who only listens to one form of music."<ref>Ice-T (1991). "Body Count". ''O.G. Original Gangster''. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6492-2</ref>

Recording sessions for the group's self-titled debut took place from September to December 1991.<ref name="AMG">{{cite web |url={{Allmusic|class=album|id=r71242|pure_url=yes}} |title=''Body Count'' review |accessdate=2007-10-20 |last=Erlewine |first=Stephen Thomas |authorlink=Stephen Thomas Erlewine |publisher=Allmusic |quote= }}</ref> The album was released on March 31, 1992, on compact disc, [[LP album|vinyl]], and [[audio cassette]].<ref name="GreatRockDiscography"/> Ice-T states that ''Body Count'' was intentionally different from his solo hip hop albums in that "An Ice T album has intelligence, and at times it has ignorance. Sometimes it has anger, sometimes it has questions. But ''Body Count'' was intended to reflect straight anger. It was supposed to be the voice of the angry brother, without answers. [...] If you took a kid and you put him in jail with a microphone and asked him how he feels, you'd get ''Body Count'': 'Fuck that. Fuck school. Fuck the police.' You wouldn't get intelligence or compassion. You'd get raw anger."<ref name="IceOpinion"/> From the album, "There Goes The Neighborhood" was released as a single,<ref name="GreatRockDiscography"/> while "Body Count's in the House" was featured in the film ''[[Universal Soldier (1992 film)|Universal Soldier]]''.<ref>{{cite web |url=http://imdb.com/title/tt0105698/soundtrack |title=Soundtracks for ''Universal Soldier'' (1992) |publisher=[[Internet Movie Database]] |quote= }}</ref>

==Music and lyrics==
{|  border="0" style="font-size:80%; float:right;; color:black;; width:130px;"
|
{{Listen|filename=Body Count There Goes the Neighborhood.ogg|title="There Goes the Neighborhood" (sample)|description=|format=[[Ogg]]}}
|}
Ernie C and Ice-T conceived the album with the dark, ominous tone and [[Satan]]ic lyrical themes of [[Black Sabbath]] in mind.<ref name=IceCentury-127>{{cite book |last1=Marrow |first1=Tracy |authorlink1= |last2=Century |first2=Douglas |title=Ice: A Memoir of Gangster Life and Redemption—from South Central to Hollywood |year=2011 |publisher=Random House |isbn=978-0-345-52328-0 |pages=127–140 |chapter=Freedom of Speech }}</ref> However, Ice-T felt that basing his lyrics in reality would be scarier than the fantasy basis in Black Sabbath's lyrics; the inner artwork depicts a man with a gun pointed at the viewer's face. Ice-T states, "To us ''that'' was the devil [...] what's more scary than [...] some gangster with a gun pointed at you?"<ref name=IceCentury-127/> Ice-T defined the resulting mix of heavy metal and reality-based lyrics as "a rock album with a rap mentality."<ref name="Dellamora">{{cite book |last=Dellamora |first=Richard |editor= |others= |title=Postmodern Apocalypse: Theory and Cultural Practice at the End |origyear= |month= |url= |edition= |series= |year=1995 |publisher=University of Pennsylvania Press |isbn=0-8122-1558-3 |oclc= |id=  |chapter= |chapterurl= |quote=|page=251 }}</ref> The album's musical style is primarily described as [[speed metal]],<ref name="GreatRockDiscography"/><ref name="Rose">{{cite book |last=Rose |first=Tricia |editor= |others= |title=Black Noise: Rap Music and Black Culture in Contemporary America |origyear= |month= |url= |edition= |series= |year=1994 |publisher=Wesleyan University Press |isbn=0-8195-6275-0 |oclc= |id=  |chapter= |chapterurl= |quote=|page=130 }}</ref><ref name="Austin">{{cite book |last=Austin |first=Joe |author2=Willard, Michael Nevin |editor= |others= |title=Generations of Youth: Youth Cultures and History in Twentieth-century America  |origyear= |month= |url= |edition= |series= |year=1998 |publisher=NYU Press |isbn=0-8147-0646-0 |oclc= |id= |pages=401–402 |chapter= |chapterurl= |quote= }}</ref> [[thrash metal]]<ref name="Dellamora"/><ref name="Brackett">{{cite book |last=Brackett |first=Nathan |editor= |others= |title=The New Rolling Stone Album Guide |origyear= |month= |url= |edition= |series= |year=2004 |publisher=Simon and Schuster |isbn=0-7432-0169-8 |oclc= |id= |chapter= |chapterurl= |quote= }}</ref><ref name="Christie">{{cite book |last=Christie |first=Ian |editor= |others= |title=Sound of the Beast: The Complete Headbanging History of Heavy Metal |origyear= |month= |url= |edition= |series= |year=2003 |publisher=HarperCollins |isbn=0-380-81127-8 |oclc= |id=  |chapter= |chapterurl= |quote=|page=300 }}</ref> and [[Heavy metal music|heavy metal]].<ref name="ew review"/> [[Jon Pareles]] of ''[[The New York Times]]'' wrote that with ''Body Count'', Ice-T "has recognized a kinship between his gangster raps and post-punk, hard-core rock, both of which break taboos to titillate fans. But where rap's core audience is presumably in the inner city, hard-core appeals mostly to suburbanites seeking more gritty thrills than they can get from Nintendo or the local mall."<ref>{{cite news |first=Jon |last=Pareles |authorlink=Jon Pareles |title=POP VIEW; Dissing the Rappers Is Fodder for the Sound Bite |url=https://query.nytimes.com/gst/fullpage.html?res=9E0CE4DE1E3AF93BA15755C0A964958260 |work=New York Times |id= |date=June 28, 1992 |accessdate=2007-10-20 |quote= }}</ref>

Despite Ice-T's attempts to differentiate ''Body Count'' from his work in the hip hop genre, the press focused on the group's rap image.<ref name="Christie"/> Ice-T felt that politicians had intentionally referred to the song "[[Cop Killer (song)|Cop Killer]]" as rap to provoke negative criticism. "There is absolutely no way to listen to the song 'Cop Killer' and call it a rap record. It's so far from rap. But, politically, they know by saying the word ''rap'' they can get a lot of people who think, 'Rap-black-rap-black-ghetto,' and don't like it. You say the word ''rock'', people say, 'Oh, but I like [[Jefferson Airplane]], I like [[Fleetwood Mac]] — that's rock.' They don't want to use the word rock & roll to describe this song."<ref name="Rose" /> ''Body Count'' has since been credited for pioneering the [[rap metal]] genre popularized by groups such as [[Rage Against the Machine]] and [[Limp Bizkit]],<ref name="RSdeaths"/><ref name="CNN"/><ref name="MorningCall">{{cite news |first=Aaron |last=Yoxheimer |title=Despite a high body count of its own, band is a survivor |url=http://www.popmatters.com/pm/article/despite-a-high-body-count-of-its-own-band-is-a-survivor |publisher=[[The Morning Call]] |id= |date=April 6, 2007 |accessdate=2007-10-20 |quote= }}</ref> although Ice-T does not rap on any of the album's tracks.<ref name=IceCentury-127/> Ernie C stated that "A lot of rappers want to be in a rock band, but it has to be done sincerely. You can't just get anybody on guitar and expect it to work. [...] [We] really loved the music we were doing, and it showed."<ref name="MorningCall"/>

==Lyrical themes==
[[File:There Goes the Neighborhood music video.jpg|thumb|upright|The final image of the "There Goes the Neighborhood" music video.]]
Like Ice-T's gangsta rap albums, ''Body Count'''s material focused on various social and political issues, with songs focusing on topics ranging from police brutality to drug abuse. According to Ernie C, "Everybody writes about whatever they learned growing up, and we were no exception. Like [[The Beach Boys]] sing about the beach, we sing about the way we grew up."<ref name="MorningCall"/> Ice-T states that "''Body Count'' was an angry record. It was meant to be a protest record. I put my anger in it, while lacing it with dark humor."<ref name="IceOpinion"/> The spoken introduction, "Smoked Pork" features Ice-T taking on the roles of a gangster pretending to be seemingly stranded motorist and a police officer who refuses to aid. The track begins with Mooseman and Ice-T driving their car towards a police car, and then Ice-T asks for the gun Mooseman has and tells Mooseman to stay in the car, much to Mooseman's chagrin, as Mooseman wanted to kill the cop in this round. Ice-T then walks up to the policeman, pretending to be a stranded motorist, asking for help, but the policeman refuses, saying: "Nah, that's not my job! My job's not to help your fuckin' ass out!", then telling him that "my job is eatin' these doughnuts". When the officer recognizes Ice-T, gunshots are heard. The final voice on the track is Ice-T confirming his identity.<ref>Body Count (1992). "Smoked Pork". ''Body Count''. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6878-2</ref>

In the lyrics of "KKK Bitch," Ice-T describes a sexual encounter with a woman who he soon learns is the daughter of the [[Grand Wizard]] of the [[Ku Klux Klan]]. The lyrics go on to describe a scenario in which members of Body Count "crash" a Klan meeting to "get buck wild with the white freaks". Ice-T makes humorous reference to "[falling] in love with [[Tipper Gore]]'s two twelve year old nieces", and ponders the possibility of the Grand Wizard coming after him "when his grandson's named little Ice-T."<ref name="Lyrics">Body Count (1992). ''Body Count''. Lyrics sheet. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6878-2</ref> In ''The Ice Opinion: Who Gives a Fuck?'', Ice-T wrote that "'KKK Bitch' was ironic because the sentiments were true. We'd play Ku Klux Klan areas in the [[Southern United States|South]] and the girls would always come backstage and tell us how their brothers and fathers didn't like black folks. [...] We knew that 'KKK Bitch' would totally piss off the Ku Klux Klan. There's humor in the song, but it fucks with them. It's on a punk tip."<ref name="IceOpinion"/>

"Voodoo" describes a fictional encounter between Ice-T and an old woman with a [[voodoo doll]].<ref name="Lyrics"/> "The Winner Loses" describes the downfall of a [[crack cocaine]] user.<ref name="Lyrics"/> "There Goes the Neighborhood" is a sarcastic response to critics of Body Count, sung from the point of view of a racist white rocker who wonders "Don't they know rock's just for whites? / Don't they know the rules? / Those niggers are too hardcore / This shit ain't cool."<ref name="Lyrics"/> For the song's music video, the word "[[nigger]]" was replaced with the phrase "black boys".<ref name="IceOpinion"/> The music video ends with a black musician implanting an electric guitar into the ground and setting it on fire. The final image is similar to that of a [[cross burning|burning cross]].<ref>Body Count (1992). "There Goes The Neighborhood". Music video. ''Body Count''. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6878-2</ref>

"Evil Dick" focuses on [[male promiscuity]]. Its lyrics describe a married man who is led to seek strange women after his "evil [[Penis|dick]]" tells him "Don't sleep alone, don't sleep alone."<ref name="Lyrics"/> "Momma's Gotta Die Tonight" follows the account of a black teenager who murders and [[dismemberment|dismember]]s his racist mother after she reacts negatively when he brings a [[white people|white]] girl home.<ref name="Lyrics"/> In ''The Ice Opinion: Who Gives a Fuck?'', Ice-T wrote that the song's lyrics are metaphorical, explaining that "Whoever is still perpetuating racism has got to die, not necessarily physically, but they have to kill off that part of their brain. From now on, consider it dead. The entire attitude is dead."<ref name="IceOpinion"/>

Ice-T referred to the album's final track, "Cop Killer" as a protest song, stating that the song is "[sung] in the first person as a character who is fed up with police brutality."<ref name="McKinnon">{{cite news |url=http://newsworld.cbc.ca/arts/music/hangthemcday2.html |archiveurl=https://web.archive.org/web/20071111055016/http://newsworld.cbc.ca/arts/music/hangthemcday2.html |archivedate=November 11, 2007 |title=Hang the MC Blaming hip hop for violence: a four-part series |accessdate=2007-10-20 |last=McKinnon |first=Matthew |date=February 7, 2006 |publisher=CBC News }}</ref> The song was written in 1990, and had been performed live several times, including at Lollapalooza, before it had been recorded in a studio.<ref name="MorningCall"/> The album version mentions then-Los Angeles police chief [[Daryl Gates]] and the black motorist [[Rodney King]], whose beating by [[Los Angeles Police Department|LAPD]] officers was recorded on videotape.<ref name="Lyrics"/> In ''The Ice Opinion: Who Gives a Fuck?'', Ice-T wrote that the song "[is] a warning, not a threat—to authority that says, 'Yo, police: We're human beings. Treat us accordingly.'"<ref name="IceOpinion"/> In an interview for ''[[Rolling Stone]]'', Ice-T stated that "We just celebrated the [[Independence Day (United States)|fourth of July]], which is really just national Fuck the Police Day [...] I bet that during the [[American Revolutionary War|Revolutionary War]], there were songs similar to mine."<ref>{{cite web |url=http://www.robertchristgau.com/xg/music/rbicet-92.php |title=Ice-T Blinks |accessdate=2007-10-20 |last=Christgau |first=Robert |authorlink=Robert Christgau |date=August 11, 1992 |publisher=[[Village Voice]] |quote= }}</ref>

==Release and reception==
{{Album ratings
| rev1 = [[AllMusic]] 
| rev1Score ={{Rating|3|5}}<ref name="AMG" />
| rev2 = ''[[Chicago Tribune]]''
| rev2Score = {{Rating|3|4}}<ref name="Kot"/>
| rev3 = ''[[Entertainment Weekly]]'' 
| rev3Score = A−<ref name="ew review">{{cite journal|last=DiMartino|first=Dave|issue=May 8|year=1992|url=http://www.ew.com/ew/article/0,,310413,00.html|title=Body Count Review|journal=[[Entertainment Weekly]]|location=New York|accessdate=January 8, 2015}}</ref> 
| rev4 = ''[[Kerrang!]]''
| rev4Score = 4/5<ref name="kerrang" >{{cite book | last1 = Kaye | first1 = Don | title = [[Kerrang!]] | chapter = Body Count 'Body Count' | volume = 387 | publisher = [[EMAP]] | date = April 11, 1992 | location = London, UK }}</ref>
| rev5 = ''[[Rolling Stone]]'' 
| rev5Score ={{Rating|3|5}}<ref name="RSreview" />
| rev6 = ''[[The Rolling Stone Album Guide]]''
| rev6Score = {{Rating|2|5}}<ref name="Relic">{{cite book|last=Relic|first=Peter|chapter=Ice-T|page=401|editor1-first=Nathan|editor1-last=Brackett|editor1-link=Nathan Brackett|editor2-first=Christian|editor2-last=Hoard|title=[[The Rolling Stone Album Guide|The New Rolling Stone Album Guide]]|edition=4th|publisher=[[Simon & Schuster]]|year=2004|isbn=0-7432-0169-8|chapter-url=https://books.google.com/books?id=t9eocwUfoSoC&pg=PA401#v=onepage&q&f=false|accessdate=January 8, 2015}}</ref>
| rev7 = ''[[Spin Alternative Record Guide]]'' 
| rev7Score = 9/10<ref name="Spin">{{cite book | last= Weisbard| first= Eric |author2=Craig Marks | title= Spin Alternative Record Guide |publisher= Vintage Books |year= 1995 |isbn= 0-679-75574-8 |page=190 }}</ref>
| rev8 = ''[[The Village Voice]]''
| rev8Score = A−<ref name="Christgau"/>
}}
Initial copies of the album were shipped out in black [[body bag]]s, a promotional device that drew minor criticism.<ref name="Heston">{{cite book |last=Heston |first=Charlton |authorlink=Charlton Heston |title=In the Arena: An Autobiography |year=1995 |publisher=Simon & Schuster |isbn=0-684-80394-1 |chapter= |page=567 }}</ref> The album debuted at No. 32 on [[Billboard (magazine)|''Billboard'']]'s Top 50 albums,<ref name="IceOpinion"/> peaking at No. 26 on the [[Billboard 200|''Billboard'' 200]].<ref>{{cite web |url={{Allmusic|class=album|id=r71242|pure_url=yes}} |title=Charts and Awards for ''Body Count'' |accessdate=2007-11-03 |publisher=Allmusic}}</ref> By January 29, 1993, the album sold 480,000 copies, according to ''[[Variety (magazine)|Variety]]''.<ref>{{cite news |author=Augusto, Troy J. |author2=Turman, Katherine |title=WB board put Ice-T out in cold |url=http://www.variety.com/article/VR103473.html?categoryid=16&cs=1 |work=Variety  |id= |date=January 29, 1993 |accessdate=2007-10-20 |quote= }}</ref> However, according to the [[Recording Industry Association of America]], ''Body Count'' was certified gold for sale shipments in excess of 500,000 copies, with a certification date back to August 4, 1992.<ref>{{cite web|title=Gold & Platinum|url=http://www.riaa.com/gold-platinum/?tab_active=default-award&se=body+count#search_section|website=RIAA|accessdate=April 1, 2016|language=en-US}}</ref>

In a positive review for ''[[The Village Voice]]'', music critic [[Robert Christgau]] said Ice-T "takes rap's art-ain't-life defense over the top" on a heavy metal album which utilizes and parodies "the style's whiteskin privilege".<ref name="Christgau">{{cite news|last=Christgau|first=Robert|authorlink=Robert Christgau|issue=April 21|year=1992|url=http://www.robertchristgau.com/xg/cg/cgv492-92.php|title=Consumer Guide|newspaper=[[The Village Voice]]|location=New York|accessdate=January 8, 2015}}</ref> He wrote that the music is "flat-out hard rock, short on soloistic intricacy and fancy structures", but that it is set apart from other metal by Ice-T, who "describes racism in language metalheads can understand, kills several policemen, and cuts his mama into little pieces because she tells him to hate white people. This can be a very funny record."<ref>{{cite journal|last=Christgau|first=Robert|issue=March|year=1992|journal=[[Playboy]]|url=http://www.robertchristgau.com/xg/play/1992-03.php|accessdate=January 8, 2015|title=Body Count, Beastie Boys, Giant Sand, Mzwakhe Mbuli}}</ref> [[Greg Kot]], writing in the ''[[Chicago Tribune]]'', felt the lyrics on some songs are pathologically flawed and off-putting, but the band's take on metal styles is impressive and, "on the stereotype-bashing 'There Goes the Neighborhood,' the humor, message and music coalesce brilliantly".<ref name="Kot">{{cite news|last=Kot|first=Greg|authorlink=Greg Kot|issue=May 21|year=1992|url=http://articles.chicagotribune.com/1992-05-21/features/9202150441_1_star-body-count-rapper-ice-t|title=Body Count (Sire)|newspaper=[[Chicago Tribune]]|accessdate=January 8, 2015}}</ref> Don Kaye of ''[[Kerrang!]]'' called ''Body Count'' a "noisy, relentless musical attack".<ref name="kerrang" />

In a less enthusiastic review for ''[[Rolling Stone]]'', [[J. D. Considine]] wrote that "messages" are less important here than "the sort of sonic intensity parental groups fear even more than four-letter words,"<ref name="RSreview">{{cite web |url=http://www.rollingstone.com/reviews/album/220871/review/5943705 |title=''Body Count'' review |accessdate=2007-10-20 |last=Considine |first=J.D. |authorlink=J.D. Considine |work=Rolling Stone |quote= }}</ref> while [[AllMusic]]'s [[Stephen Thomas Erlewine]] called the album "a surprisingly tepid affair" partly because "all of Ice-T's half-sung/half-shouted lyrics fall far short of the standard he established on his hip-hop albums."<ref name="AMG"/> In the [[Pazz & Jop]], an annual poll of prominent critics published by ''The Village Voice'', ''Body Count'' was voted the 31st best album of 1992.<ref name="VillageVoice"/> Christgau, the poll's supervisor, ranked it 22nd on his own year-end list.<ref>{{cite news|last=Christgau|first=Robert|issue=March 2|year=1993|url=http://www.robertchristgau.com/xg/pnj/deans92.php|title=Pazz & Jop 1992: Dean's List|newspaper=The Village Voice|location=New York|accessdate=January 8, 2015}}</ref>

==Controversy==
{{see also|Cop Killer (song)}}

The album was originally set to be distributed under the title ''Cop Killer'',<ref name="Heston"/><ref>{{cite book |last=Raymond |first=Emilie |title=From My Cold, Dead Hands: Charlton Heston and American Politics |year=2006 |publisher=University Press of Kentucky |isbn=0-8131-2408-5 |chapter= |page=260 }}</ref> named for the song of the same name, which criticizes violent police officers.<ref name="IceOpinion"/><ref name="Time">{{cite news |url=http://www.time.com/time/magazine/article/0,9171,976203,00.html |title=Ice T Melts |accessdate=2007-10-20 |date=August 10, 1992 |publisher=[[Time (magazine)|Time]] |quote= }}</ref> During the production of the album, Warner Bros. executives were aware of the potential controversy that the album and song could cause, but supported it.<ref name=IceCentury-127/> At a Time-Warner shareholders' meeting, actor [[Charlton Heston]] stood and read lyrics from the song "KKK Bitch" to an astonished audience and demanded that the company take action.<ref name="YouthMedia"/> Sire responded by changing the title to ''Body Count'', but did not remove the song.<ref name="Heston"/> In an article for the ''[[Washington Post]]'', Tipper Gore condemned Ice-T for songs like "Cop Killer," writing that "Cultural economics were a poor excuse for the South's continuation of [[Slavery in the United States|slavery]]. Ice-T's financial success cannot excuse the vileness of his message [...] [[Adolf Hitler|Hitler]]'s [[anti-Semitism]] sold in [[Nazi Germany]]. That didn't make it right."<ref>{{cite news |first=Tipper |last=Gore |title=Hate, rape and rap |url= |work=Washington Post |id= |date=January 8, 1990 |quote= }}</ref> The Dallas Police Association and the Combined Law Enforcement Association of Texas launched a campaign to force [[Warner Bros. Records]] to withdraw the album.<ref name="YouthMedia">{{cite book |last=Osgerby |first=Bill |editor= |others= |title=Youth Media |origyear= |month= |url= |edition= |series= |year=2004 |publisher=Routledge |isbn=0-415-23808-0 |oclc= |id= |pages=68–70 |chapter= |chapterurl= |quote= }}</ref> CLEAT called for a boycott of all products by Time-Warner in order to secure the removal of the song and album from stores.<ref name="Austin"/> Within a week, they were joined by police organizations across the United States.<ref name="YouthMedia"/> Ice-T asserted that the song was written from the point of view of a fictional character, and told reporters that "I ain't never killed no cop. I felt like it a lot of times. But I never did it. If you believe that I'm a cop killer, you believe David Bowie is an astronaut," in reference to Bowie's song "[[Space Oddity]]".<ref name="McKinnon"/>
{|  border="0" style="font-size:80%; float:right;; color:black;; width:130px;"
|
{{Listen|filename=Body Count Cop Killer.ogg|title="Cop Killer" (sample)|description=|format=[[Ogg]]}}
|}
The [[National Black Police Association (United States)|National Black Police Association]] opposed the boycott of [[Time-Warner]] and the attacks on "Cop Killer," identifying police brutality as the cause of much anti-police sentiment, and proposed the creation of independent civilian review boards "to scrutinize the actions of our law enforcement officers" as a way of ending the provocations that caused artists such as Body Count "to respond to actions of police brutality and abuse through their music. [...] Many individuals of the law enforcement profession do not want anyone to scrutinize their actions, but want to scrutinize the actions of others."<ref name="Austin"/> Critics argued that the song could cause crime and violence.<ref name="YouthMedia"/><ref name="Jones">{{cite book |last=Jones |first=Thomas David |editor= |others= |title=Human Rights: group defamation, freedom of expression, and the law of nations |origyear= |month= |url= |edition= |series= |year=1998 |publisher=Martinus Nijhoff Publishers |isbn=90-411-0265-5 |oclc= |id= |pages=126–129 |chapter= |chapterurl= |quote= }}</ref> Others defended the album on the basis of the group's right to [[freedom of speech]], and cited the fact that Ice-T had portrayed a police officer in the film ''[[New Jack City]]''.<ref name="Shuker">{{cite book |last=Shuker |first=Roy |editor= |others= |title=Understanding Popular Music |origyear= |month= |url= |edition= |series= |year=2001 |publisher=Routledge |isbn=0-415-23510-3 |oclc= |id= |pages=227–229 |chapter= |chapterurl= |quote= }}</ref> Ice-T is quoted as saying that "I didn't need people to come in and really back me on the First Amendment. I needed people to come in and say 'Ice-T has grounds to make this record.' I have the right to make it because the cops are killing my people. So fuck the First Amendment, let's deal with the fact that I have the right to make it."<ref name="Roc">{{cite web |url=http://www.theroc.org/roc-mag/textarch/roc-11/roc11-09.htm |title=A Roc Exclusive: Ice-T Speaks Out on Censorship, Cop Killer, His Leaving Warner Bros., and More |accessdate=2007-10-20 |last=Heck |first=Mike |quote= }}</ref>

Over the next month, controversy against the band grew. Vice President [[Dan Quayle]] branded "Cop Killer" as being "obscene," and President [[George H.W. Bush]] publicly denounced any record company that would release such a product.<ref name="YouthMedia"/> ''Body Count'' was removed from the shelves of a retail store in [[Greensboro, North Carolina]] after local police had told the management that they would no longer respond to any emergency calls at the store if they continued to sell the album.<ref name="Austin"/> In July 1992, the New Zealand Police Commissioner unsuccessfully attempted to prevent an Ice-T concert in [[Auckland]], arguing that "Anyone who comes to this country preaching in obscene terms the killing of police, should not be welcome here,"<ref name="Shuker"/> before taking Body Count and Warner Bros. Records to the [[Indecent Publications Tribunal]], in an effort to get it banned under New Zealand's Indecent Publications Act. This was the first time in 20 years that a sound recording had come before the censorship body, and the first ever case involving popular music.<ref name="Shuker"/> After reviewing the various submissions, and listening carefully to the album, the Tribunal found the song "Cop Killer" to be "not exhortatory," saw the album as displaying "an honest purpose," and found Body Count not indecent.<ref name="Shuker"/>
<!-- Deleted image removed: [[File:Body Count Album Cover Reissue.jpg|right|thumb|180px|For the reissue, the artwork was altered to remove reference to the controversial song.]] -->
The controversy escalated to the point where death threats were sent to Time-Warner executives, and [[stockholder]]s threatened to pull out of the company. Finally, Ice-T decided to remove "Cop Killer" from the album of his own volition,<ref name="IceOpinion"/><ref name="Time"/><ref name="Roc"/> a decision which was met by criticism from other artists who derided Ice-T for "caving in to external pressure."<ref>{{cite book |last=Oxoby |first=Marc |editor= |others= |title=The 1990s |origyear= |month= |url= |edition= |series= |year=2003 |publisher=Greenwood Press |isbn=0-313-31615-5 |oclc= |id=  |chapter= |chapterurl= |quote=|page=171 }}</ref> In an interview, Ice-T stated that "I didn't want my band to get pigeon-holed as that's the only reason that record sold. It just got outta hand and I was just tired of hearing it. I said, 'fuck it,' I mean they're saying we did it for money, and we didn't. I'd gave the record away, ya know, let's move on, let's get back to real issues, not a record, but the cops that are out there killing people."<ref name="Roc"/>

"Cop Killer" was replaced by a new version of "Freedom of Speech," a song from Ice-T's 1989 solo album ''[[The Iceberg/Freedom of Speech...Just Watch What You Say]]''. The song was re-edited and remixed to give it a more rock-oriented sound, using a looped sample from the [[Jimi Hendrix]] song "[[Foxy Lady]]."<ref name="AMG"/> Alongside the album's reissue, [[Warner Bros.]] issued "Cop Killer" as a single.<ref name=IceCentury-127/> Ice-T left Warner Bros. Records the following year because of disputes over his solo album ''[[Home Invasion (album)|Home Invasion]]'',<ref name="IceOpinion"/> taking Body Count with him. The studio version of "Cop Killer" has not been re-released, although a live version of the song appears on Body Count's 2005 release ''Live in L.A.''<ref>{{cite web |url=http://www.dvdverdict.com/reviews/bodycountliveinla.php |title=Review of Body Count: ''Live in L.A.'' |accessdate=2007-10-20 |last=Gibron |first=Bill |date=February 16, 2006 |publisher=DVD Verdict}}</ref> According to Ernie C, the controversy over the song "still lingers for us, even now. I'll try to book clubs and the guy I'm talking to will mention it and I'll think to myself 'Man, that was 17 years ago.' But I meet a lot of bands who ask me about it, too, and I'm real respected by other artists for it. But it's a love/hate thing. Ice gets it too, even though he plays a cop on TV now on ''Law & Order SVU''."<ref name="MorningCall"/>

In Australia, the track listings on copies of the new version of the album sold there ended at track 16, omitting "Freedom of Speech" (or "Cop Killer" and its spoken word intro, "Out in the Parking Lot"). This was likely because the track "Freedom of Speech" refers to the speech protections of the First Amendment to the United States Constitution, which Australia does not have an equivalent to in its own Constitution, thus the track is not as relevant to Australian audiences.

==Track listing==

{{Track listing
| collapsed       = 
| headline        = 26878 (original version)<ref name="Withdrawn">Body Count (1992). ''Body Count''. Track listing. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6878-2</ref>
| extra_column    = 
| total_length    = 52:59

| lyrics_credits  = yes
| music_credits   = yes

| title1          = Smoked Pork
| lyrics1         = Ice-T
| length1         = 0:46

| title2 = Body Count's in the House
| length2 = 3:24
| lyrics2 = Ice-T
| music2 = Ernie C

| title3 = Now Sports
| length3 = 0:04 
| lyrics3 = Ice-T

| title4 = Body Count
| length4 = 5:17
| lyrics4 = Ice-T
| music4 = Ernie C

| title5 = A Statistic
| length5 = 0:06
| lyrics5 = Ice-T

| title6 = Bowels of the Devil
| length6 = 3:43
| lyrics6 = Ice-T
| music6 = Ernie C

| title7 = The Real Problem
| length7 = 0:11
| lyrics7 = Ice-T

| title8 = KKK Bitch
| length8 = 2:52
| lyrics8 = Ice-T
| music8 = Ernie C

| title9 = C Note
| length9 = 1:35
| music9 = Ernie C

| title10 = Voodoo
| length10 = 5:00
| lyrics10 = Ice-T
| music10 = Ernie C

| title11 = The Winner Loses
| length11 = 6:32
| lyrics11 = Ernie C
| music11 = Ernie C

| title12 = [[There Goes the Neighborhood (Body Count song)|There Goes the Neighborhood]]
| length12 = 5:50
| lyrics12 = Ice-T
| music12 = Ernie C

| title13 = Oprah
| length13 = 0:06
| lyrics13 = Ice-T

| title14 = Evil Dick
| length14 = 3:58 
| lyrics14 = Ice-T
| music14 = Ernie C

| title15 = Body Count Anthem
| length15 = 2:46
| lyrics15 = Ice-T
| music15 = Ernie C

| title16 = Momma's Gotta Die Tonight
| length16 = 6:10
| lyrics16 = Ice-T
| music16 = Ernie C

| title17 = Out in the Parking Lot
| length17 = 0:30
| lyrics17 = Ice-T

| title18 = [[Cop Killer (song)|Cop Killer]]
| length18 = 4:09
| lyrics18 = Ice-T
| music18 = Ernie C
}}

{{Track listing
| collapsed       = 
| headline        = 45139 (reissue)<ref name="AMG"/>
| extra_column    = 
| total_length    = 53:03

| lyrics_credits  = yes
| music_credits   = yes

| title17 = Ice-T/Freedom of Speech
| lyrics17 = Ice-T, [[Jello Biafra]]
| music17 = [[Jimi Hendrix]]
| length17 = 4:41 
}}

==Personnel==
*[[Ice-T]] – lead vocals
*[[Ernie C]]. – lead, rhythm and acoustic guitars
*[[Mooseman]] – bass
*[[D-Roc The Executioner|D-Roc]] – rhythm guitar
*[[Beatmaster V|Beatmaster "V"]] – drums

==References==
{{reflist|30em}}

{{Body Count}}

{{featured article}}

[[Category:1992 debut albums]]
[[Category:Body Count albums]]
[[Category:English-language albums]]
[[Category:Sire Records albums]]
[[Category:Obscenity controversies in music]]