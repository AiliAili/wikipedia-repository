{{about|the city in Abu Dhabi, United Arab Emirates|other uses|El Ain (disambiguation)}}
{{refimprove|date=August 2016}}
{{Infobox settlement
|name=Al-`Ain
|official_name = Al Ain, United Arab Emirates
|native_name = العين {{ar icon}}
|settlement_type = [[city]]
|translit_lang1_type=[[Arabic script|Arabic]]
|translit_lang1_info = العين
|image_skyline=Jabal hafeet shahin.jpg
|image_caption=A view over Green Mubazarrah
|image_flag = 
|image_coat = 
|pushpin_map = UAE
|pushpin_map_caption = Location of Al Ain in the UAE
|government_type = [[Municipality]]
|subdivision_type = Country
|subdivision_name = United Arab Emirates
|subdivision_type1 = [[Emirates of the United Arab Emirates|Emirate]]
|subdivision_name1 = Abu Dhabi
|parts_type = Subdivisions
|parts = [[Town]]s and [[villages]]
|p1 = Al Jimi
|p2 = Al Qattara
|p3 = Al Muaiji
|p4 = Al Mutaredh
|p5 = Al Towayya
|p6 = Al Foah
|p7 = Al Masoudi
|p8 = Al Khrair
|p9 = Al Sarooj
|p10 = Hili
|p11 = Falaj Hazza
|p12 = Zakher
|p13 = Al Maqam
|p14 = Sh'ab Al Ashkher
|p15 = Al Khalidiya
|p16 = sAl Shoaibah
|p17 = Al Bateen
|p18 = Al Agabiyya
|p19 = Al Khabisi
|p20 = Al Markhaniya
|p21 = Ne'mah
|p22 = Al Niyadat
|p23 = Al Kuwaitat
|p24 = Al Jahli
|p25 = Al Salamat
|p26 = Al Yahar= Mezyad
|p28 = Al Dhahir
|p29 = Um Ghafah
|p30 = Oud Al Tobah
|p31 = Al Hiyar
|p32 = Nahil
|p33 = Sweihan
|p34 = Al Sad
|p35 = Rimah
|p36 = Al Khazna
|p37 = Al Arad
|p38 = Al Dhahrah
|p39 = Al Manaseer
|p40 = Al Basrah
|p41 = Al Wagan
|p42 = Al Qoua
|leader_title = [[Sheikh]]
|leader_name = [[Khalifa bin Zayed Al Nahyan]]
|leader_title1= Ruler's Representative of the Eastern Region of the United Arab Emirates
|leader_name1= [[Tahnoun bin Mohammed Al Nahyan]]
|area_magnitude = 
|area_metro_km2 = 
|area_total_km2 = 13,100
|population_as_of = 2013
|population_total = 650,000
|population_density_km2 = auto
|utc_offset = +4
|timezone = UAE Standard Time
|coordinates = {{coord|24|12|27|N|55|44|41|E|region:AE|display=inline,title}}
|elevation_m = 292
|footnotes = 
|}}
{{Infobox World Heritage Site
| WHS         = Cultural Sites of Al Ain (Hafit, Hili, Bidaa Bint Saud and Oases Areas)
| Image       = [[File:Alley in Al Ain Oasis.JPG|260px|alt=A dirt and cobblestone road runs through the center of the image, flanked by low plastered walls and palm trees.|An alley at the Al Ain Oasis]]
| imagecaption= An alley at the Al Ain Oasis
| State Party = [[United Arab Emirates]]
| Type        = Cultural
| Criteria    = iii, iv, v
| ID          = 1343
| Region      = [[List of World Heritage Sites in Western Asia|Arab States]]
| Year        = 2011
| Session     = 35th
| Link        = http://whc.unesco.org/en/list/1343
}}
'''Al-`Ain''' ({{lang-ar|العين}}, {{transl|ar|''al-ʿayn''}}, literally ''The Spring''), also known as the ''Garden City'' due to its greenery, is the largest inland city in the [[United Arab Emirates]], the fourth-largest overall (after [[Dubai]], [[Abu Dhabi]], and [[Sharjah]]), and the second-largest in the [[Abu Dhabi (emirate)|Emirate of Abu Dhabi]]. With a population of 650,000  (as of 2013),<ref>http://www.thenational.ae/uae/government/abu-dhabi-population-doubles-in-eight-years</ref> it is located approximately {{convert|160|km}} east of the capital Abu Dhabi and about {{convert|120|km}} south of Dubai.<ref>[http://www.scad.ae/Publications/YearBook/Population%20and%20Demography-English%20SYB%202011.pdf ]{{dead link|date=April 2013}}</ref> Al-`Ain is the birthplace of [[Sheikh Zayed bin Sultan Al Nahyan]], the founder of the United Arab Emirates, and has the highest proportion of Emirati nationals (30.8%).

Al-`Ain is located in the Emirate of Abu Dhabi, inland on the border with [[Oman]]. The freeways connecting Al-`Ain, Abu Dhabi, and [[Dubai]] form a geographic triangle in the country, each city being roughly {{convert|130|km}} from the other two.

==History==
Historically a part of Ṫawam or [[Al Buraimi]] [[Oasis]]. Al-`Ain has been inhabited for over 4,000 years, with archaeological sites showing human settlement at Al-Hili and Jabel Ḥafeeṫ. These early cultures built "beehive" tombs for their dead and engaged in hunting and gathering in the area. The oasis provided water for early farms until the modern age.

A [[Sahabah|companion]] of the Islamic [[Prophets in Islam|prophet]], [[Muhammad]], Ka`ab Bin Ahbar, was reportedly sent to the region to introduce [[Islam]] to the people. He settled and died in the oasis.<ref>{{cite web|url=https://www.youtube.com/watch?v=25g60ivHuzI |title=Hazrat Subhan Qadri at Mazar e Mubarak Hazrat Kaab Bin Ahbar RA and other Companions, Al-`Ain ,UAE |publisher=YouTube |date=2011-02-20 |accessdate=2013-04-22}}</ref>

The forts currently in Al-`Ain were built in the late 19th or early 20th century to solidify Abu Dhabi's control over the oasis. [[Wilfred Thesiger]] visited Al-`Ain in the late 1940s during his travels across the [[Empty Quarter]]. He met [[Sheikh Zayed]] and stayed with him at Al-Muwaiji Fort.  This network of fortresses protected the Oases and settlements from bandit attacks.

In 1952 Saudi Arabia sent raiders to capture Al-`Ain's fortresses and incorporate the oasis into the Saudi kingdom. Forces from the [[Trucial Oman Scouts]], as well as the army of Muscat-Oman, arrived to recapture the oasis. With British intervention, the Saudi forces surrendered, leaving the oasis back in the hands of Abu Dhabi and Muscat-Oman.

In 1971 [[Queen Elizabeth&nbsp;II]] visited the Hilton Hotel in the area, still in use, during her tour of the Persian Gulf. Following independence in 1971, Al-`Ain experienced rapid growth and investment as part of the emirate of Abu Dhabi, quickly becoming larger and more successful than `Oman's Al-Buraimi. In 1972 `Oman and Abu Dhabi agreed on the final borders to divide Al-Buraimi and Al-`Ain. Until Sheikh Zayed's death in 2004, Al-`Ain's municipal code forbade construction of buildings over four storeys, with the exceptions of the Hilton, Danat Al Ain Resort, and Rotana hotels.

In the 1990s, a serious uprising occurred among the labourers of the industrial district of Al-`Ain, Aṣ-Ṣana`iya. This uprising was suppressed by the [[UAE army]] and local police forces. All the labourers involved were interned and deported.

Until 2006, Al-Buraimi and Al-`Ain shared an open border.  This border was closed in November 2006, and passport controls were imposed.

==Geography==
Al-Ain is located in the eastern region of [[Abu Dhabi Emirate]] just south of [[Dubai]] and east of [[Abu Dhabi]]. The Eastern region covers an area of approximately 13,100&nbsp;km². [[Oman]] lies to the east, Dubai and [[Sharjah (emirate)|Sharjah]] to the north, Abu Dhabi to the west and the [[Empty Quarter]] desert and [[Saudi Arabia]] to the south. The topography of Al-`Ain is unique and varies as one travel to the east. [[Jebel Hafeet]] (Hafeet mountain) is considered one of the monuments of Al-`Ain, lying just to the southeast and rising to 1,300 m in elevation. Sand dunes of varying  texture that are tinged red with iron oxide lie to the north and east of Al-Ain.

==Climate==
In Al-`Ain, the mean annual rainfall is 96&nbsp;mm and the average relative humidity is 60% (United Arab Emirates University, 1993). Low humidity in Al-`Ain, particularly during the summers, makes it a popular destination for many people at that time of year. Boer (1997) classified the UAE climate as hyper-arid and divided it into four climatic regions: the coastal zone along the Persian Gulf, the mountain areas northeast of UAE, the gravel plains around Al-`Ain area, and the central and southern sand desert. More rainfall and lower temperatures occur in the northeast than in the southern and western regions. The monthly average rainfall around Al-`Ain was (100–120&nbsp;mm) from the period 1970 to 1992.{{Citation needed|date=May 2008}}

{{Weather box
|location= Al Ain International Airport (1996-2015)
|metric first=yes
|single line=yes
|Jan high C = 24.3
|Feb high C = 27.5
|Mar high C = 31.7
|Apr high C = 36.5
|May high C = 41.6
|Jun high C = 43.9
|Jul high C = 44.2
|Aug high C = 44.0
|Sep high C = 41.5
|Oct high C = 37.3
|Nov high C = 30.8
|Dec high C = 26.4
|Jan mean C = 18.7
|Feb mean C = 21.3
|Mar mean C = 24.9
|Apr mean C = 29.5
|May mean C = 34.1
|Jun mean C = 36.2
|Jul mean C = 37.5
|Aug mean C = 37.5
|Sep mean C = 34.7
|Oct mean C = 30.7
|Nov mean C = 25.0
|Dec mean C = 20.7
|Jan low C = 13.1
|Feb low C = 15.1
|Mar low C = 18.0
|Apr low C = 22.5
|May low C = 26.5
|Jun low C = 28.4
|Jul low C = 30.7
|Aug low C = 30.9
|Sep low C = 27.9
|Oct low C = 24.1
|Nov low C = 19.3
|Dec low C = 15.1

|source 1 = Weatheronline  <ref name= Weatheronline>{{cite web
  |url = http://www.weatheronline.co.uk/weather/maps/city?LANG=en&PLZ=_____&PLZN=_____&WMO=41218&CONT=asie&R=0&LEVEL=162&REGION=0023&LAND=VR&MOD=tab&ART=TEM&NOREGION=1&FMM=1&FYY=1996&LMM=12&LYY=2015
  |title = Climate Robot: Al Ain Intl. Airport (262m)
  
  |accessdate = February 18, 2016}}</ref>
}}

==Present-day Al-`Ain==
[[File:Top of Jebel Hafeet.jpg|thumb|left|View of Al Ain from top of [[Jebel Hafeet]]]]
[[File:Sheikha Salama mosque - Alain.jpg|thumb|left|Aerial view of Al Ain from top of the [[Sheikha Salama mosque]], with Jabal Hafeet in the background]]

Al-`Ain has a higher proportion of Emirati nationals than elsewhere in the country, but the majority of its residents are [[expatriate]]s particularly from the [[India]]n sub-continent. Many people are from [[Bangladesh]], [[Pakistan]] and some from [[Afghanistan]].{{citation needed|date=October 2010}} There are fewer other expatriates than in the larger centres of [[Abu Dhabi]] and [[Dubai]].

Al-`Ain is often called the 'Garden City of The Gulf' given the many oases, parks, tree-lined avenues and decorative roundabouts within the city. Strict height controls on new buildings, to no more than four floors, emphasise the greenery of the city.

==Tourism and recreation==
Al-`Ain is developing as a tourist destination. The dry desert air makes it a welcome retreat from the coastal humidity of the larger cities. Many Emirati nationals in [[Abu Dhabi]] have holiday houses in the city making it a popular weekend destination for families from the capital city. Its attractions include the [[Al Ain National Museum]], the [[Al Ain Palace Museum]], several restored forts and the [[Hili Archaeological Park]] site, dating back to the [[Bronze Age]]. [[Jebel Hafeet]], a 1340-metre-high mountain, dominates the surrounding area. It is popular to visit to the mineral springs at the base and to drive to the mountaintop at sunset. Other attractions include the [[Al Ain Oasis]] in the city centre, other oases dotted around the area — all cool retreats in the middle of the summer heat — [[Al Ain Zoo]], an [[amusement park]] named "[[Hili Fun City]]", many well-maintained parks popular with families in the summer evenings, and a heritage village. Opened in 2012, [[Wadi Adventure]] is located near Jebel Hafeet and provides a range of water-based activities including surfing, kayaking and rafting.

On top of [[Jabel Hafeet]] is the [[Mercure Hotels|Mercure]] Hotel.

Al-`Ain has three major malls — [[Al Ain Mall]] in the town centre, [[Al-Jimi Mall]] in the [[Al-Jimi]] district, and [[Bawadi Mall]] located in the [[Al-Khrair]] District. Most commercial activity is centred in and around town centre.

Another popular pastime for Emiratis and expatriates alike is spending time in coffee shops and [[shisha cafe]]s. There are many Café's in Al-`Ain, ranging in size and quality.

Al-`Ain also has an International standard [[go-kart]] circuit. Al-`Ain Raceway was selected to host the 2007 Rotax Max World Karting Finals, an event which saw 220 drivers from over 55 different countries compete for the Karting world title. Al-`Ain Raceway opened to the general public in May 2008 and proves a popular activity for local Emiratis and tourists alike. It was announced in late 2010 that the 2011 Rotax Max World Karting Finals will be held at Al-`Ain Raceway, this will bring nearly 1000 tourists to the small garden city.

Like the rest of the [[UAE]], Al-`Ain has strict laws governing the consumption and distribution of alcohol. Five facilities in the city currently serve alcohol, Four of which are hotels. The Al-`Ain [[Rotana Hotels|Rotana]], [[Hilton Hotels|Hilton]], [[ACCOR hotels|Mercure Grand Jebel Hafeet]] and Danat Al-`Ain Resort, hotels all have pubs, bars, or night clubs. In addition to the hotels, the [[Al Ain Equestrian, Shooting & Golf Club]]<ref>[http://www.aesgc.ae aesgc.ae]</ref> in [[Al-Maqam]] also serves alcohol.

Currently, there are only four locations that sell alcohol for private use — [[Spinneys]] near Al-Jimi District, an outlet to the left of the Hilton hotel (next to the hotel's staff quarters), High Spirits Bottle Shop behind Lulu Hypermarket Sana`iya and the North Africa Market in [[Sanaiya]].

The city has two English-language radio stations — 100.1 Star FM, which plays English-speaking hits alternating with Arabic-speaking hits, and 105.2 Abu Dhabi Classic FM, which plays [[classical music]].

==Oasis==
[[File:Falaj at al ain.jpg|thumb|upright|The falaj irrigation system at [[Al Ain Oasis]]]]
{{main article|Al Ain Oasis}}

Al-`Ain's oases are known for their underground irrigation system "falaj" (or qanāt from Arabic قناة) that brings water from boreholes to water farms and palm trees. [[Falaj]] irrigation is an ancient system dating back thousands of years and is used widely in `[[Oman]], the [[UAE]], [[China]], [[Iran]] and other countries. Al-`Ain has seven oases; the largest is [[Al Ain Oasis]], near to [[Old Sarooj]], and the smallest is Al-Jahili Oasis. The rest are Al-Qaṭṭara, Al-Mu`ṫaredh, Al-Jimi, Al-Muwaiji, and Al-Hili.

==Commerce and industry==
Al-`Ain is an important services centre for a wide area extending into `[[Oman]]. There are three major shopping centres, [[Al Ain Mall]], [[Al Jimi Mall]] and [[Al Bawadi Mall]] (opened in 2009 in the [[Al Khrair]] area) as well as traditional souqs for fruit and vegetables and livestock. Industry is growing, but is still on a small scale, and includes the [[Coca-Cola]] bottling plant and the Al-`Ain Portland Cement Works. The water in Al-`Ain is very good. Service industries such as car sales, mechanics and other artisans are located in the area known as Sanaiya and Pattan Market. Social and governmental infrastructure includes [[United Arab Emirates University]], [[Higher Colleges of Technology]], [[Abu Dhabi University]] (Al-`Ain campus), well-equipped medical facilities including the teaching hospital at Tawam, military training areas and [[Al Ain International Airport]].

==Education==
Al-`Ain is home to the main federal university in the UAE, the [[United Arab Emirates University]], and to two campuses of the [[Al Ain Men's College|Higher Colleges of Technology]] - [[Al Ain Men's College]] and [[Al Ain Women's College]]. Al-`Ain is also the home of Horizon International flight academy, [[Etihad Airways]]'s cadet pilot training centre. Private higher education institutions include the [[Al Ain University of Science and Technology]] and [[Abu Dhabi University]] (Al-`Ain campus).

Al-`Ain also houses the eastern zone headquarters of the [[Abu Dhabi Education Council]], Abu Dhabi's education authority.

Many of Al-`Ain's private schools, catering mainly to the expatriate population, are located in the Al-Manaseer area. They include Al-`Ain International school (British curriculum, private school, part of the Aldar group), [[Al Ain English Speaking School]], [[Al Dhafra Private School]], Manor Hall School, Al-Sanawbar School, Liwa International School, Al-Madar International School, Global English School, Emirates Private School, a branch of the [[International School of Choueifat]], and an Institute of Applied Technology campus. Other private schools include the [[Central Board of Secondary Education|CBSE]] affiliated school Indian School, Al-`Ain, [[Our Own English High School]], and [http://www.alainjuniors.com/ Al Ain Juniors School].

==Health==
The first hospital in Al-`Ain was [[Oasis Hospital, Al Ain|Oasis Hospital]], established in 1960 at the invitation of Sheikh Zayed. Oasis serves all nationalities, and provides training for medical students from UAE University. Ground has been broken for a new hospital to be completed in 2011.{{citation needed|date=October 2010}} Oasis Hospital is part of [[CURE International]].

Al-`Ain is the home of [[Tawam Hospital]], a training and research hospital linked with the UAE University. It was officially inaugurated on 17 December 1979. In  March 2006, [[Johns Hopkins Hospital]] ([[Johns Hopkins Medicine International]]) (JHMI) took over the management of Tawam hospital.

[[Al Ain Hospital]] (abbr: AAH, also known as Al-Jimi Hospital) is the general hospital delivering health services to all Al-`Ain patients regardless of their nationality. It is centrally located in the Al-Jimi district and is linked with the UAE University. Al-`Ain Hospital still occupies old 1970s buildings, but a new building is planned. AAH currently has about 450 beds and provides services in all medical disciplines. In September 2007, the [[Medical University of Vienna|Medical University of Vienna International]]<ref>[http://www.meduniwien.ac.at/homepage/en Medical University of Vienna].</ref> (MUVI) took over the management of AAH.

Al-Khwarizmi International College<ref>[http://www.khwarizmi.com khwarizmi.com]</ref> has started a Campus at Al-`Ain and is offering BBA programme and various other licensed, accredited and approved courses.

==Sport, culture, and the arts==
Al-`Ain is a cultural retreat for residents of [[Dubai]] and [[Abu Dhabi]] cities. It is home to a major festival of [[classical music]].

Al-`Ain City is the home of [[Al Ain Club]], which is one of the most successful football clubs in the UAE and [[Asia]].<ref>{{Cite web|url=http://www.soccer24.com/asia/afc-champions-league-2002-2003/|title=AFC Champions League 2002/2003 Results - Asia Soccer|website=www.soccer24.com|access-date=2016-04-21}}</ref> It has many titles and championships to its name.<ref>{{Cite web|url=http://www.emaratalyoum.com/sports/local/2014-04-09-1.665451|title=الأهلي يستعيد أمجاد العـــقد الأول.. والعين "زعيم" بـ 58 لقباً|date=2014-04-09|website=الإمارات اليوم|language=ar-AR|access-date=2016-04-21}}</ref> Al-`Ain Club contains also eight other games which are: handball, volleyball, basketball, swimming, Table Tennis, Athletics, Jiu jitsu, and Taekwondo.

Hili Fun City hosts two ice hockey teams, the Al Ain Vipers<ref>[http://www.alainvipers.com alainvipers.com]</ref> and Ghantoot. Each team has adult and youth teams starting from age 4. The Al-`Ain Vipers Men's Team won the Emirates Hockey League in the 2009-10 season.

The Palm Resort to the west of the town hosts a popular rugby club with adult and youth teams, and the Al-`Ain International Soccer Club which has three youth teams, including one for 7-9 year old's.

There is a water sports centre called Wadi Adventure with a wave pool and surf instructors. Additionally, the park has facilities for kayaking and rafting on an artificial river.

==Gallery==
<gallery>
Museum courtyard surfaces.jpg|Museum courtyard surfaces
Museum wall.jpg|Museum wall
Al ain street scene.jpg|A street scene in central Al Ain
</gallery>

==See also==
{{Portal|Abu Dhabi}}
* [[Qanat]] water supply system
* [[Al Ain International Airport]]
{{clear}}

==References==
{{Reflist}}

==External links==
{{Wikivoyage}}
{{commons category|Al Ain (Abu Dhabi)}}
* [http://www.edarabia.com/universities/al-ain/ List of Universities in Al Ain]
* [http://am.abudhabi.ae/ Al Ain City Municipality]
* [https://web.archive.org/web/20130820192235/http://www.awpr.ae/home/about/welcome.aspx Al Ain Wildlife Park and Resort]
* [https://web.archive.org/web/20130820195000/http://www.awpr.ae/home/about/preventing-desert-wh/1.aspx Al Ain Wildlife Park and Resort. Preserving desert wildlife & habitats]
* [https://web.archive.org/web/20130810063252/http://awpr.ae/home/about/expansion-project.aspx Al Ain Wildlife Park and Resort. Expansion project]
* [https://web.archive.org/web/20120503021748/http://aaw.hct.ac.ae/english/al_ain/al_ain_main.htm Al Ain: Oasis in the Desert (Photographic essay from Al Ain Women's College)]

{{Abu Dhabi}}

[[Category:Al Ain| ]]
[[Category:Oman–United Arab Emirates border crossings]]
[[Category:Persian Gulf]]
[[Category:Populated places in Abu Dhabi (emirate)]]
[[Category:World Heritage Sites in the United Arab Emirates]]