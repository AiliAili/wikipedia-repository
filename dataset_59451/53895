{{Other uses}}
{{refimprove|date=January 2008}}

[[File:Two dim standing wave.gif|right|frame|A standing wave in a rectangular cavity resonator.]]

A '''resonator''' is a device or system that exhibits [[resonance]] or resonant behavior, that is, it naturally [[Oscillation|oscillates]] at some [[frequency|frequencies]], called its [[Resonance frequency|resonant frequencies]],  with greater [[amplitude]] than at others.  The oscillations in a resonator can be either [[Electromagnetic radiation|electromagnetic]] or mechanical (including [[Acoustics|acoustic]]). Resonators are used to either generate waves of specific frequencies or to select specific frequencies from a signal.  Musical instruments use [[Musical acoustics|acoustic]] resonators that produce sound waves of specific tones. Another example is [[crystal oscillator|quartz crystals]] used in electronic devices such as [[radio transmitter]]s and [[quartz clock|quartz watches]] to produce oscillations of very precise frequency.

A '''cavity resonator''' is one in which waves exist in a hollow space inside the device.  In electronics and radio, [[Microwave cavity|microwave cavities]] consisting of hollow metal boxes are used in microwave transmitters, receivers and test equipment to control frequency, in place of the [[tuned circuit]]s which are used at lower frequencies.  Acoustic cavity resonators, in which sound is produced by air vibrating in a cavity with one opening, are known as [[Helmholtz resonance|Helmholtz resonators]].

==Explanation==

A physical system can have as many [[resonant frequency|resonant frequencies]] as it has [[degrees of freedom (engineering)|degrees of freedom]]; each degree of freedom can vibrate as a [[harmonic oscillator]]. Systems with one degree of freedom, such as a mass on a spring, [[pendulum]]s, [[balance wheel]]s, and [[RLC circuit|LC tuned circuits]] have one resonant frequency. Systems with two degrees of freedom, such as [[Double pendulum|coupled pendulums]] and [[Transformer|resonant transformers]] can have two resonant frequencies. A [[crystal lattice]] composed of ''N'' atoms bound together can have ''N'' resonant frequencies. As the number of coupled harmonic oscillators grows, the time it takes to transfer energy from one to the next becomes significant. The vibrations in them begin to travel through the coupled harmonic oscillators in waves, from one oscillator to the next.

The term ''resonator'' is most often used for a homogeneous object in which vibrations travel as waves, at an approximately constant velocity, bouncing back and forth between the sides of the resonator. The material of the resonator, through which the waves flow, can be viewed as being made of millions of coupled moving parts (such as atoms). Therefore, they can have millions of resonant frequencies, although only a few may be used in practical resonators.  The oppositely moving waves [[interference (physics)|interfere]] with each other, and at its [[resonant frequency|resonant frequencies]] reinforce each other to create a pattern of [[standing wave]]s in the resonator. If the distance between the sides is <math>d\,</math>, the length of a round trip is <math>2d\,</math>. To cause resonance, the [[phase (waves)|phase]] of a [[sinusoidal]] wave after a round trip must be equal to the initial phase so the waves self-reinforce. The condition for resonance in a resonator is that the round trip distance, <math>2d\,</math>, is equal to an integral number of wavelengths <math>\lambda\,</math> of the wave:

:<math>2d = N\lambda,\qquad\qquad N \in \{1,2,3,\dots\}</math>

If the velocity of a wave is <math>c\,</math>, the frequency is <math>f = c / \lambda\,</math> so the resonant frequencies are:

:<math>f = \frac{Nc}{2d}\qquad\qquad N \in \{1,2,3,\dots\}</math>

So the resonant frequencies of resonators, called [[normal modes]], are equally spaced multiples ([[harmonic]]s) of a lowest frequency called the [[fundamental frequency]]. The above analysis assumes the medium inside the resonator is homogeneous, so the waves travel at a constant speed, and that the shape of the resonator is rectilinear. If the resonator is inhomogeneous or has a nonrectilinear shape, like a circular [[drum]]head or a cylindrical [[microwave cavity]], the resonant frequencies may not occur at equally spaced multiples of the fundamental frequency. They are then called [[overtone]]s instead of [[harmonic]]s. There may be several such series of resonant frequencies in a single resonator, corresponding to different modes of vibration.

==Electromagnetic==
{{Electromagnetism|Network}}
{{main|electrical resonance}}

An electrical circuit composed of discrete components can act as a resonator when both an [[inductor]] and [[capacitor]] are included.  Oscillations are limited by the inclusion of resistance, either via a specific [[resistor]] component, or due to [[electrical resistance|resistance]] of the inductor windings.  Such [[resonant circuit]]s are also called [[RLC circuit]]s after the circuit symbols for the components.

A [[distributed element model|distributed-parameter]] resonator has capacitance, inductance, and resistance that cannot be isolated into separate lumped capacitors, inductors, or resistors.  An example of this, much used in [[filter (signal processing)|filtering]], is the [[helical resonator]].

A single layer coil (or [[solenoid]]) that is used as a secondary or tertiary winding in a [[Tesla coil]] or [[magnifying transmitter]] is also a distributed resonator.

===Cavity resonators===
{{main|Microwave cavity}}
A ''cavity resonator'' is a hollow closed conductor such as a metal box or a cavity within a metal block, containing [[electromagnetic wave]]s (radio waves) reflecting back and forth between the cavity's walls.  When a source of radio waves at one of the cavity's [[resonant frequency|resonant frequencies]] is applied, the oppositely-moving waves form [[standing wave]]s, and the cavity stores electromagnetic energy.

Since the cavity's lowest resonant frequency, the fundamental frequency, is that at which the width of the cavity is equal to a half-wavelength (λ/2), cavity resonators are only used at [[microwave]] frequencies and above, where wavelengths are short enough that the cavity is conveniently small in size.

Due to the low resistance of their conductive walls, cavity resonators have very high [[Q factor]]s; that is their [[bandwidth (signal processing)|bandwidth]], the range of frequencies around the resonant frequency at which they will resonate, is very narrow.  Thus they can act as narrow [[bandpass filter]]s.   Cavity resonators are widely used as the frequency determining element in [[electronic oscillator|microwave oscillator]]s.  Their resonant frequency can be tuned by moving one of the walls of the cavity in or out, changing its size.

===Examples===

[[File:Aust.-Synchrotron,-RF-Cavities-of-Linac-(Bunchers),-14.06.2007.jpg|left<!--image on left to prevent a huge block of whitespace at the end-->|thumb|RF cavities in the [[Linear particle accelerator|linac]] of the [[Australian Synchrotron]] are used to accelerate and bunch beams of [[electron]]s; the linac is the tube passing through the middle of the cavity.]]

[[File:US Patent 2424267 Figs 1a, 1b, 1c.PNG|right|thumb|An illustration of the electric and magnetic field of one of the possible modes in a cavity resonator.]]
{{further|cavity magnetron|klystron}}

The [[cavity magnetron]] is a vacuum tube with a filament in the center of an evacuated, lobed, circular cavity resonator. A perpendicular magnetic field is imposed by a permanent magnet. The magnetic field causes the electrons, attracted to the (relatively) positive outer part of the chamber, to spiral outward in a circular path rather than moving directly to this anode. Spaced about the rim of the chamber are cylindrical cavities. The cavities are open along their length and so they connect with the common cavity space. As electrons sweep past these openings they induce a resonant high frequency radio field in the cavity, which in turn causes the electrons to bunch into groups. A portion of this field is extracted with a short antenna that is connected to a waveguide (a metal tube usually of rectangular cross section). The [[waveguide]] directs the extracted RF energy to the load, which may be a cooking chamber in a microwave oven or a high gain antenna in the case of radar.

The [[klystron]], tube waveguide, is a beam tube including at least two apertured cavity resonators. The beam of charged particles passes through the apertures of the resonators, often tunable wave reflection grids, in succession. A collector electrode is provided to intercept the beam after passing through the resonators. The first resonator causes bunching of the particles passing through it. The bunched particles travel in a field-free region where further bunching occurs, then the bunched particles enter the second resonator giving up their energy to excite it into oscillations. It is a [[particle accelerator]] that works in conjunction with a specifically tuned cavity by the configuration of the structures. On the [[beamline]] of an accelerator system, there are specific sections that are cavity resonators for [[Radio frequency|RF]].

The [[reflex klystron]] is a klystron utilizing only a single apertured cavity resonator through which the beam of charged particles passes, first in one direction. A repeller electrode is provided to repel (or redirect) the beam after passage through the resonator back through the resonator in the other direction and in proper phase to reinforce the oscillations set up in the resonator.

In a [[laser]], light is amplified in a cavity resonator that is usually composed of two or more mirrors. Thus an ''[[optical cavity]]'', also known as a resonator, is a cavity with walls that reflect [[electromagnetic waves]] ([[light]]). This allows standing wave modes to exist with little loss outside the cavity.

== Mechanical ==
{{main|mechanical resonance}}

Mechanical resonators are used in [[electronic circuit]]s to generate signals of a precise [[frequency]]. For example, [[crystal oscillator|piezoelectric resonators]], commonly made from [[quartz crystal|quartz]], are used as frequency references. Common designs consist of electrodes attached to a piece of quartz, in the shape of a rectangular plate for high frequency applications, or in the shape of a [[tuning fork]] for low frequency applications. The high dimensional stability and low temperature coefficient of quartz helps keeps resonant frequency constant. In addition, the quartz's [[Piezoelectricity|piezoelectric]] property converts the mechanical vibrations into an oscillating [[voltage]], which is picked up by the attached electrodes. These [[crystal oscillator]]s are used in [[quartz clock]]s and watches, to create the [[clock signal]] that runs computers, and to stabilize the output signal from [[radio transmitter]]s.  Mechanical resonators can also be used to induce a standing wave in other media. For example, a multiple degree of freedom system can be created by imposing a base excitation on a cantilever beam. In this case the  [[standing wave]] is imposed on the beam.<ref>{{ citation | title= The development of a virtual probe tip with application to high aspect ratio microscale features|author1=M.B. Bauza |author2=R.J Hocken |author3=S.T Smith |author4=S.C Woody | publisher=Rev. Sci Instrum, 76 (9) 095112  | year=2005 }} .</ref>  This type of system can be used as a [[sensor]] to track changes in [[natural frequency|frequency]] or [[phase shift|phase]] of the [[resonance]] of the fiber.  One application is as a measurement device for [[dimensional metrology]].<ref>http://www.insitutec.com</ref>

== Acoustic ==
{{main|acoustic resonance}}

The most familiar examples of acoustic resonators are in [[musical instrument]]s.  Every musical instrument has resonators.  Some generate the sound directly, such as the wooden bars in a [[xylophone]], the head of a [[drum]], the strings in [[stringed instrument]]s, and the pipes in an [[organ (instrument)|organ]].  Some modify the sound by enhancing particular frequencies, such as the sound box of a [[guitar]] or [[violin]].  [[Organ pipe]]s, the bodies of [[woodwind]]s, and the sound boxes of stringed instruments are examples of acoustic cavity resonators.

===Automobiles===
[[File:Exhaust with Resonator.jpg|right|thumb|A sport motorcycle, equipped with exhaust resonator, designed for performance.]]
The exhaust pipes in automobile [[exhaust system]]s are designed as acoustic resonators that work with the [[muffler]] to reduce noise, by making sound waves "cancel each other out".<ref>[http://auto.howstuffworks.com/muffler4.htm How stuff works: muffler]</ref> The "exhaust note" is an important feature for some vehicle owners, so both the original manufacturers and the [[aftermarket (automotive)|after-market suppliers]] use the resonator to enhance the sound.  In "[[tuned exhaust]]" systems designed for performance, the resonance of the exhaust pipes can also be used to remove combustion products from the combustion chamber at a particular engine speed or range of speeds.<ref>''Advanced Automotive Technology'', p. 84, [[Office of Technology Assessment]], Diane Publishing, September 1995 ISBN 1428920021.</ref>

=== Percussion instruments ===

In many [[keyboard percussion]] instruments, below the centre of each note is a tube, which is an [[Acoustic resonance|acoustic cavity resonator]]. The length of the tube varies according to the pitch of the note, with higher notes having shorter resonators. The tube is open at the top end and closed at the bottom end, creating a column of air that [[resonance|resonates]] when the note is struck. This adds depth and volume to the note. In string instruments, the body of the instrument is a resonator. The [[tremolo]] effect of a [[vibraphone]] is achieved via a mechanism that opens and shuts the resonators.

=== Stringed instruments ===
[[File:Steel guitar-KayEss.1.jpeg|thumb|upright|A [[Dobro]]-style [[resonator guitar]]]]
String instruments such as the bluegrass [[banjo]] may also have resonators. Many five-string banjos have removable resonators, so players can use the instrument with a resonator in [[Bluegrass music|bluegrass]] style, or without it in [[folk music]] style. The term ''resonator'', used by itself, may also refer to the [[resonator guitar]].

The modern [[ten-string guitar]], invented by [[Narciso Yepes]], adds four sympathetic string resonators to the traditional classical guitar. By tuning these resonators in a very specific way (C, B♭, A♭, G♭) and making use of their strongest partials (corresponding to the octaves and fifths of the strings' fundamental tones), the bass strings of the guitar now resonate equally with any of the 12 tones of the chromatic octave. The [[EBow|guitar resonator]] is a device for driving guitar string harmonics by an electromagnetic field. This resonance effect is caused by a feedback loop and is applied to drive the fundamental tones, octaves, 5th, 3rd to an infinite [[sustain]].

==See also==
{{Wikipedia books|Resonance}}

* [[Coupling coefficient of resonators]]
* [[Crab cavity]]
* [[Nuclear magnetic resonance]]
* [[Optical ring resonators]]
* [[Superconducting RF]]

==References and notes==
{{reflist}}

[[Category:Acoustics]]
[[Category:Electromagnetism]]
[[Category:Musical instrument parts and accessories]]