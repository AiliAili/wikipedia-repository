{{Infobox person
|name = Andreas Syggros
|image = Andreas Syggros.JPG
|caption = 
|birth_date = {{Birth date|df=yes|1830|10|12}}
|birth_place = [[Istanbul]], [[Ottoman Empire]]
|death_date = {{Death date and age|df=yes|1899|02|13|1830|10|12}}
|death_place = [[Athens]], [[Greece]]
|occupation   = [[banker]], [[philanthropist]]
|nationality  = Greek
}}
'''Andreas Syngros''' ({{lang-el|Ανδρέας Συγγρός}}; October 12, 1830 &ndash; February 13, 1899) was a [[Greeks|Greek]] banker from [[Istanbul]], at the time known internationally as Constantinople, and a philanthropist.
Born in Istanbul to [[Chios|Chiot]] parents, Syngros was one of the founders of the Bank of Constantinople along with [[Stephanos Skouloudis]].  Syngros married Iphigenia Mavrokordatou of the wealthy merchant [[Mavrocordatos family]]; they never had any children. They moved to [[Athens]] in 1871 where Syngros planned to found a new bank.  Buying land from the widow of [[Dimitrios Rallis]], Syngros engaged the well-known Athenian architect Nikolaos Soutsos who built his home based on plans by the German [[Ernst Ziller]], across from the Royal Palace.<ref>[http://www2.mfa.gr/www.mfa.gr/en-US/The+Ministry/Historical+Background/Facilities  Greek Foreign Ministry] {{webarchive |url=https://web.archive.org/web/20071121060315/http://www2.mfa.gr/www.mfa.gr/en-US/The+Ministry/Historical+Background/Facilities |date=November 21, 2007 }}</ref>  Today the mansion is the headquarters of the [[Minister for Foreign Affairs (Greece)|Greek Foreign Ministry]], having been left to the state by his widow.

He, together with other members of the Constantinople [[Greek diaspora]] and the Odessa Greek diaspora, led by [[Evangelis Valtatzis]], founded the General Credit Bank (Ethniki Pistotiki Trapeza).<ref>[http://www.levantine.plus.com/testi44.htm  Levantine Heritage] {{webarchive |url=https://web.archive.org/web/20060827133355/http://www.levantine.plus.com/testi44.htm |date=August 27, 2006 }}</ref> 

In 1882, he founded the Privileged Bank of Epirus and Thessaly (alternatively Epyrothessaly) in [[Volos]] to help the economy of the newly annexed Greek territories of Epiros and Thessaly. The annexation led to an influx of investment from expatriates who bought large estates previously belonging to Muslims who had made investments in the area. The "Privileged" in the bank's name refers to the bank receiving the right to issue banknotes in these territories.  The [[National Bank of Greece]] acquired the bank after Syngros's death.

Syngros also became involved in numerous works of public philanthropy, including building an avenue from the Royal Palace to the bay at [[Palaio Faliro]] (named after him today, [[Andrea Syngrou Avenue]]) and he was responsible for completing the [[Corinth Canal]], one of the great feats of engineering in Greece, in 1893. Syngros died in Athens in 1899, and is buried in the [[First Cemetery of Athens]].

==References==
{{reflist}}

{{Authority control}}
{{DEFAULTSORT:Syngrou, Andreas}}
[[Category:1830 births]]
[[Category:1899 deaths]]
[[Category:Constantinopolitan Greeks]]
[[Category:People from Chios]]
[[Category:Greek accountants]]
[[Category:Greek bankers]]
[[Category:Greek philanthropists]]
[[Category:Burials at the First Cemetery of Athens]]