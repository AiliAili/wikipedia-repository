<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=XSB3C-1
 |image=Curtiss XSB3C-1 3-view.jpg
 |caption=Three-view drawing of the XSB3C-1 from Johnson 2008.
}}{{Infobox Aircraft Type
 |type=[[Torpedo bomber|Torpedo]]/[[dive bomber]]
 |manufacturer=[[Curtiss-Wright]]
 |designer=
 |first flight=
 |introduced=
 |retired=
 |status=
 |primary user=[[United States Navy]]
 |more users=
 |produced=
 |number built=None
 |unit cost=
 |developed from= [[Curtiss SB2C Helldiver]]
 |variants with their own articles=
 |developed into=
}}
|}

The '''Curtiss XSB3C''' was a proposed development by [[Curtiss-Wright]] of the [[Curtiss SB2C Helldiver]] [[dive bomber]], submitted to meet a [[U.S. Navy]] requirement for a new dive bomber to replace the SB2C in service. Considered inferior to the competing [[Douglas XSB2D]] and requiring higher grade fuel than was provided on aircraft carriers, the project was cancelled before any aircraft were built.

==Design and development==
In response to a Navy request for proposals issued on 3 February 1941 for a replacement for the SB2C,<ref name="Norton">Norton 2008</ref><ref name="Lawson">Lawson 2001 p.82</ref> Curtiss designed an improved and enlarged version of the Helldiver, which was, at the time, still only in the process of flight testing. A larger tail, revised wing planform and tricycle landing gear distinguished the aircraft from its predecessor,<ref name="Norton"/> in addition to the provision of heavier armament.

An internal [[bomb bay]] in the midsection of the aircraft could carry up to {{convert|4000|lb}} of bombs, or alternatively, two torpedoes could be carried in semi-submerged mountings. In addition, hardpoints for two {{convert|500|lb|adj=on}} bombs were fitted under the wings.<ref name="Norton"/> Forward-firing armament proposals were for the aircraft to be fitted with either six .50-calibre [[machine gun]]s or four 20mm cannon in the wings, while defensive armament was planned to be fitted in a power-operated turret.<ref name="Norton"/>

Power was intended to be provided by a [[Wright R-3350]] of {{convert|2500|hp|kW}}, while the [[Pratt & Whitney R-4360]], giving {{convert|3000|hp|kW}}, was considered for future installation.<ref name="Johnson">Johnson 2008, pp. 425-426.</ref> Impressed with the inspection of the [[mockup]] of the massive aircraft in December 1941, the Navy ordered two prototypes, and parts of the design were tested by the XSB2C-6.<ref name="Norton"/>

As the project progressed during 1942, however, it was determined to be inferior to the competing Douglas aircraft.<ref name="Norton"/> This, combined with the aircraft's requirement for [[Avgas#91.2F96 .26 115.2F145|115/145 octane]] fuel, which was considered difficult to handle aboard ship,<ref name="Norton"/> and the decision by the [[Bureau of Aeronautics]] that future attack aircraft would be single-seat aircraft,<ref name="Johnson"/> led to the Navy's decision to cancel the prototype contract, and no examples of the XSB3C were ever built.<ref name="Norton"/>

==Specifications (XSB3C-1)==
{{Aircraft specs
|ref=<ref name="Norton"/>
|prime units?=imp
|genhide=
|crew=2 (pilot and gunner)
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
|eng1 number=1
|eng1 name=[[Wright R-3350]]-8
|eng1 type=[[radial piston engine]]
|eng1 kw=
|eng1 hp=2300
|eng1 note=
|power original=
|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=
|perfhide=Y <!-- please remove this line if performance data becomes available -->
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
|armament= *6 .50-cal machine guns or 4 20mm cannon in wings<br/>
*Unspecified defensive guns
*{{Convert|4000|lb}} bombs or 2 torpedoes in internal bay
*Hardpoints for two {{convert|500|lb|adj=on}} bombs under wings
|avionics=
}}

==See also==
{{Aircontent
|related=
*[[Curtiss SB2C Helldiver]]
|similar aircraft=
*[[Blackburn Firebrand]]
*[[Martin AM Mauler|Martin AM/BTM Mauler]]
*[[Douglas BTD Destroyer|Douglas SB2D/BTD Destroyer]]
|lists=
*[[List of military aircraft of the United States (naval)]]
|see also=
}}

==References==
===Citations===
{{Reflist}}
===Bibliography===
{{Refbegin}}
* Johnson, E.R. ''American Attack Aircraft Since 1926''. McFarland, 2008. ISBN 0-7864-3464-3.
* Lawson, Robert and Barrett Tillman. ''U. S. Navy Dive and Torpedo Bombers of World War II''. MBI Publishing, 2001, p.&nbsp;82-83. ISBN 0-7603-0959-0.
* Norton, Bill. ''U.S. Experimental & Prototype Aircraft Projects: Fighters 1939-1945''. North Branch, Minnesota: Specialty Press, 2008, p.&nbsp;111. ISBN 978-1-58007-109-3.
{{Refend}}

==External links==
* [https://web.archive.org/web/20080519055615/http://home.att.net/~jbaugher/thirdseries1.html XSB3C-1 serials]

{{Curtiss aircraft}}
{{USN scout aircraft}}

[[Category:Curtiss aircraft|SB03C]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Carrier-based aircraft]]
[[Category:World War II dive bombers]]