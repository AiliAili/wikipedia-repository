{{for|the publishing company|Bilingual Review Press}}
{{notability|date=November 2016}}
{{Infobox Journal
| title = Bilingual Review/Revista Bilingüe
| cover = [[File:Bilingual Review (journal) cover.jpg]]
| editor = Howard L. Smith
| discipline = Bilingual studies, [[Hispanic and Latino Americans|Hispanic American]] literature
| language = English, Spanish
| abbreviation = Biling. Rev.
| publisher = [[University of Texas at San Antonio]]
| country = United States
| frequency = Triannually
| history = 1974–present
| openaccess = Yes
| license =
| impact = 
| impact-year = 
| website = http://bilingualreview.utsa.edu/
| link1 = http://bilingualreview.utsa.edu/index.php/br/issue/current
| link1-name = Online access
| link2 = http://bilingualreview.utsa.edu/index.php/br/issue/archive
| link2-name = Online archive
| JSTOR = 00945366
| OCLC = 1084374
| LCCN = 74645066
| CODEN = 
| ISSN =
| eISSN = 0094-5366
}}
The '''''Bilingual Review/La revista bilingüe''''' is a triannual [[peer-reviewed]] [[open access]] academic and literary [[academic journal|journal]] covering research on Spanish-English [[bilingualism]], [[bilingual education]], and [[Latin American literature|Hispanic American literature]]. It was previously published by [[Bilingual Review Press]], which is affiliated with the [[Hispanic Research Center]] at [[Arizona State University]]. The current publisher is the [[University of Texas at San Antonio]].

The journal was established in 1974 by [[Gary D. Keller]] ([[City College of New York]]), who served as [[editor-in-chief]] until 2014.<ref>{{cite journal |author=Hernández-G., Manuel de Jesús |date=Winter 2006 |title=Dr. Gary Keller-Cárdenas: 2006 NACCS Scholar |url=http://www.naccs.org/naccs/NL_Scholar_EN.asp?SnID=2 |archive-url=https://web.archive.org/web/20110718211158/http://www.naccs.org/naccs/NL_Scholar_EN.asp?SnID=2 |dead-url=yes |archive-date=2011-07-18 |journal=Noticias de NACCS |location=San Jose, CA |publisher=[[National Association of Chicana and Chicano Studies]] |volume=34 |issue=3 |oclc=36780353}}
</ref> He was succeeded by Howard L. Smith (University of Texas at San Antonio).<ref>{{cite web |title=Department of Bicultural-Bilingual Studies acquires Bilingual Review journal |url=http://www.utsa.edu/today/2014/09/bilingual-review.html |work=UTSA Today |publisher=University of Texas at San Antonio |accessdate=2016-11-02}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|http://bilingualreview.utsa.edu/}}

{{Portal|Hispanic and Latino Americans}}

{{DEFAULTSORT:Bilingual Review}}
[[Category:Linguistics journals]]
[[Category:American literary magazines]]
[[Category:Latin American literature]]
[[Category:Latin American studies]]
[[Category:Publications established in 1974]]
[[Category:Bilingualism]]
[[Category:Multilingual journals]]


{{ling-journal-stub}}