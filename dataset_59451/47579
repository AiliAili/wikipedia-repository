{{Use dmy dates|date=September 2012}}
{{Use Australian English|date=September 2012}}
{{Infobox musical artist
| name                = Double Dragon
| image                 = ropediggy.jpg
| background          = group_or_band
| origin              = [[Adelaide]], Australia
| genre               = [[Heavy metal music|Heavy Metal]], [[Metalcore]], [[Melodic Death Metal]]
| years_active        = 2003&ndash;2012 (Now formally Sons of Asena)
| label               = Truth Inc Records/[[MGM]]
| website                 = [http://www.myspace.com/doubledragonheaymetal Official website]
}}
 
'''Double Dragon''' was a [[heavy metal music|heavy metal]] band from [[Adelaide]], Australia. Their name is a homage to the 1980s cult video game ''[[Double Dragon]]''. Double Dragon's musical style is heavy metal, but has also been described variously as melodic death metal or metalcore, featuring twin [[guitar]] [[harmonies]], prominent lead guitar solos, and both clean and screamed vocals.

==Band history==
Double Dragon originally formed in 2002 when upon the demise of his previous band Screwface:13, guitarist Matthew 'Roady' Johnston decided to create a new project that focused more on melody than the down tuned style of 1990s [[nu metal]]. After starting rehearsals with friend Danial Busch on drums, then bass player Jason Moon soon after, the three began writing material. Eventually they recruited Busch's long-time friend Shane Christford as their singer. After recording a two-track demo (unofficially dubbed 'Pirate Metal' due to its home-made skull and crossbones style artwork), Christford left the band during mid-2003 due to personal differences.

Whilst still searching for a replacement frontman in late 2003, they were joined by lead guitarist Ben Murphy, who had previously been a member of Adelaide hardcore–metal band Three Chain Break and death metal band Scorched Earth Policy before that. Murphy also later spent a brief period playing with Adelaide screamo band Hi End Audio.{{Citation needed|date=January 2009}}

Within two months of Murphy's addition to Double Dragon, the band were joined by ex The Undamynd frontman Lee( Liggy )Gardiner as their vocalist. With the lineup now solidified, Double Dragon performed regularly to local audiences, and in the latter half of 2004 recorded their first 5-track demo/EP "Have Them Destroyed" (a name the band considered before settling upon 'Double Dragon'), which was eventually released independently in January 2005.{{Citation needed|date=January 2009}}

In between a successful CD launch and beginning to tour interstate on a regular basis, the band continued writing material for their next recording. However, in August 2005 singer Gardiner was injured in a domestic house fire, suffering third-degree burns to both thighs and hands. During his rehabilitation period, the remaining members of the band began writing heavier and more aggressive songs – some of which would eventually be lyrically based around Gardiner's injuries and the impact it had on the band. These would be the songs that made up their official debut EP, "Scars Of Fire", which was recorded from late 2005 to early 2006 with producer Ian Miller. Upon its completion the band was signed by Adelaide-based record label Truth Inc Records. "Scars Of Fire" was released Australia wide in June 2006.{{Citation needed|date=January 2009}}

Following the success of their debut EP, Double Dragon went on to headline shows Australia wide, as well as earning the opportunity to support international tours featuring bands such as [[Slayer]], [[Arch Enemy]], [[Dark Tranquillity]], [[The Black Dahlia Murder (band)|The Black Dahlia Murder]], [[Cephalic Carnage]], and [[Mastodon (band)|Mastodon]]. The band also played on the Local Produce stage at the 2007 Adelaide [[Big Day Out]].

Double Dragon received airplay on [[Triple J]]'s [[Full Metal Racket]] as well as community radio nationally. Their debut film clip for "Dead But Still Killing" appeared several times on music program [[Rage (TV program)|Rage]] and on music channel [[Channel V]]. Double Dragon were also part of V energy drink's promotions on Channel V to coincide with their performance at the 2007 [[Big Day Out]]. {{Citation needed|date=January 2009}}

In late 2007 Double Dragon began recording their next album, "Devastator", eventually released through Truth Inc Records in July 2008. Again engineered and produced by Ian Miller, the album featured ten tracks stylistically varying from traditional heavy metal, death metal, straight out hard rock, as well as two ballads. Track seven, "[[Breathing Fire]]", featured additional guest vocals by [[Brisbane]] singer–songwriter [[Emma Dean (musician)|Emma Dean]]. The album debuted at number one on the SAMIA Adelaide charts, and the band received a nomination for 'Best Metal Act' in local street press magazine dB Magazine's annual poll. The album's official launch show in Adelaide in July 2008 marked the farewell performance for lead guitarist Ben Murphy, who decided to leave the band to spend more time with his family. He was briefly replaced by former 'A Thousand Nightmares' guitarist James Cunningham, but eventually the position was permanently filled by 'A Red Dawn' guitarist Davin Buttery. In 2008 Double Dragon was the main support act for Australian tours by Swedish metal bands [[The Haunted]] and [[Soilwork]], as well as opening for [[Carcass (band)|Carcass]] from the UK in Adelaide.{{Citation needed|date=January 2009}}

In December 2008, Double Dragon were mentioned in ''[[Blunt Magazine|Blunt]]'' magazine's "25 Australian bands to watch in 2009".{{Citation needed|date=January 2009}}

On 27 February 2009 the band performed on the metal stage at the Adelaide leg of the Soundwave tour alongside [[Lamb of God (band)|Lamb of God]], [[In Flames]], [[DevilDriver]], [[Lacuna Coil]], [[36 Crazyfists]], [[Every Time I Die]], [[Poison the Well (band)|Poison The Well]], [[Evergreen Terrace]], [[Unearth]] and [[All That Remains (band)|All That Remains]]. Also performing at the festival were acts [[Nine Inch Nails]], [[Alice in Chains]], [[The Bloodhound Gang]], [[The Dillinger Escape Plan]] and many more.

Double Dragon spent the latter part of 2009 playing at the annual Against The Grain metal festival in Adelaide, this time in its fifth year, as well as opening for U.S. metal bands [[Slayer]] and [[Megadeth]] around Australia on their October 2009 national tour. In [[Melbourne]] Slayer vocalist [[Tom Araya]] had a throat illness, which led to Double Dragon vocalist Gardiner doing the vocals for Slayer song "[[Hate Worldwide]]".<ref>[https://www.youtube.com/watch?v=ecFSRZ7_CbU]([[YouTube]])</ref> After supporting [[Obituary (band)|Obituary]] in Adelaide in November 2009, the band toured the country as special guests on U.S. metal band [[Chimaira]]'s 'Spreading The Infection' Australian tour in January 2010. After a brief hiatus beginning in July 2010 in which the band temporarily dissolved, Double Dragon reformed in September of the same year with new drummer Liam Weedall and new bass player Josh Lamont.

In December 2010 through to July 2011 Double Dragon recorded their sophomore album "Sons of Asena" at Adelaide's Against The Grain Studio with engineer Andy Kite. Based on a fictional story written by Gardiner, with loose ties to an ancient Turkish wolf myth, the album featured 11 songs which displayed a different direction to the band's previous work, stemming from a wider songwriting collaboration between the band's members. [[Brisbane]] singer/songwriters [[Kate Miller-Heidke]] and [[Keir Nuttall]] appear as guest musicians on the closing track "Asena".<ref>[http://www.killyourstereo.com/news/8722/kate-miller-heidke-to-appear-on-new-double-dragon-album/]([[Kill your stereo]])</ref> Jason North from fellow Adelaide band Truth Corroded appears on the track "Transfusion". The album was mixed and mastered by Dan Murtagh at Melbourne's Sing Sing Studios in November 2011, and released for free digital download from the band's specifically created website www.sonsofasena.com on 4 December.

==Influences==
Double Dragon are heavily influenced by bands such as [[Black Sabbath]] and [[Judas Priest]], [[new wave of British heavy metal]] acts like [[Iron Maiden]], thrash metal bands [[Slayer]] and [[Pantera]], death metal pioneers [[Carcass (band)|Carcass]] and [[Entombed (band)|Entombed]], alternative acts [[Faith No More]] and [[Mr. Bungle]], Swedish metal bands like [[Soilwork]] and [[The Haunted]], and to a lesser extent progressive artists such as [[Tool (band)|Tool]] and [[Opeth]]. The band are also influenced by a wide range of non metal performers such as [[The Beatles]], [[Pink Floyd]] and [[Tori Amos]], to name a few.

==Band members==
*Lee Gardiner - Vocals
*Matthew Johnston - Guitar
*Davin Buttery - Guitar
*Joshua Lamont - Bass
*Liam Weedall - Drums

==Discography==

===Studio albums===
{| class="wikitable plainrowheaders" style="text-align:center;"
! scope="col" rowspan="2" style="width:13em;"| Title
! scope="col" rowspan="2" style="width:17em;"| Album details
! scope="col" colspan="1"| Peak chart positions
! scope="col" rowspan="2" style="width:12em;"| [[Music recording sales certification|Certifications]]
|-
! scope="col" style="width:3em;font-size:90%;"| [[ARIA Charts|AUS]]
|-
! scope="row" | ''Devastator''
|
* Released: 2008
* Label: Truth Inc., MGM
* Formats: [[Compact disc|CD]], [[Music download|digital download]]
| -
|
|-
! scope="row" | ''Sons Of Asena''
|
* Released: 2011
* Label: Independent
* Formats: [[Compact disc|CD]], [[Music download|digital download]]
| -
|
|-
|}

===Extended plays===
{| class="wikitable plainrowheaders" style="text-align:center;"
! scope="col" rowspan="2" style="width:13em;"| Title
! scope="col" rowspan="2" style="width:17em;"| Album details
! scope="col" colspan="1"| Peak chart positions
! scope="col" rowspan="2" style="width:12em;"| [[Music recording sales certification|Certifications]]
|-
! scope="col" style="width:3em;font-size:90%;"| [[ARIA Charts|AUS]]
|-
! scope="row" | ''Have Them Destroyed''
|
* Released: 2005
* Label: Independent
* Formats: [[Compact disc|CD]], [[Music download|digital download]]
| -
|
|-
! scope="row" | ''Scars of Fire''
|
* Released: 2006
* Label: Truth Inc., MGM
* Formats: [[Compact disc|CD]], [[Music download|digital download]]
| -
|
|-
! scope="row" | ''Devastation vs. Decimation'' (with Truth Corroded)
|
* Released: 2009
* Label: Truth Inc., MGM
* Formats: [[Compact disc|CD]], [[Music download|digital download]]
| -
|
|-
|}

==Sources==
{{Reflist}}
*[http://www.myspace.com/doubledragonheavymetal Double Dragon  - ''Myspace'' site]
*https://web.archive.org/web/20081221230603/http://www.beat.com.au:80/review.php?id=1399
*https://web.archive.org/web/20081231142753/http://www.roadrunnerrecords.com:80/blabbermouth.net/news.aspx?mode=Article&newsitemID=111637
*https://web.archive.org/web/20080802222714/http://www.dbmagazine.com.au/403/iv-DoubleDragon.shtml
*http://www.metal-archives.com/
*[http://www.fasterlouder.com.au/reviews/events/14912/Against-the-Grain-4--Fowlers-Live-Adelaide-130908.htm - ''Fasterlouder Against The Grain Review'' site]
*http://www.tsunamimag.com/?process=
*https://web.archive.org/web/20080926161457/http://www.roadrunnerrecords.com:80/blabbermouth.net/news.aspx?mode=Article&newsitemID=103866
*https://web.archive.org/web/20081221230603/http://www.beat.com.au:80/review.php?id=1399
*https://web.archive.org/web/20081122035212/http://www.musicsa.com.au/artists/doubledragon
*http://www.spirit-of-metal.com/groupe-groupe-Double_Dragon-l-en.html
*https://web.archive.org/web/20080810175540/http://www.dbmagazine.com.au/352/mr-DoubleDragon.shtml
*http://www.fasterlouder.com.au/reviews/events/6598/Arch-Enemy-Double-Dragon—Fowlers-Live-Adelaide-191006.htm
*https://web.archive.org/web/20080723064813/http://ripitup.com.au/stateofsound/1081
*https://web.archive.org/web/20080820042050/http://www.ravemagazine.com.au/content/view/9243/82/

{{Authority control}}

{{DEFAULTSORT:Double Dragon (Band)}}
[[Category:Australian heavy metal musical groups]]