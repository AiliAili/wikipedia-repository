{{Infobox Politician (general)
| name                = José Celso Barbosa
| honorific-suffix    =
| image               = Jose Celso Barbosa.jpg
| imagesize           = 140px
| caption             = Dr. José Celso Barbosa
| office              = Member of the Executive Cabinet
| term_start          = 1900
| term_end            = 1917
| predecessor         =
| successor           =
| constituency        =
| majority            =
| order2              =
| office2             = Member of the [[Senate of Puerto Rico]]
| term_start2         = 1917
| term_end2           = 1921
| vicepresident2      =
| viceprimeminister2  =
| deputy2             =
| lieutenant2         =
| monarch2            =
| president2          =
| primeminister2      =
| governor2           =
| succeeding2         =
| predecessor2        =
| successor2          =
| constituency2       =
| majority2           =
| birth_date          = {{Birth date|1857|7|27}}
| birth_place         = [[Bayamón, Puerto Rico]]
| death_date          = {{death date and age|1921|9|21|1857|7|27}}
| death_place         = [[San Juan, Puerto Rico]]
| nationality         = [[Puerto Rican People|Puerto Rican]]
| party               = [[Puerto Rican Republican Party]]
| otherparty          =
| spouse              = Belén Sánchez y Jimenez
| children            = Twelve (12)
| alma_mater          = [[University of Michigan]] MED
| occupation          =
| profession          = Medical [[physician]], [[sociologist]], and political leader in [[Puerto Rico]]
| religion            = Christianity 
| footnotes           =
}}

Dr. '''José Celso Barbosa Alcala'''<ref group=note>{{Spanish name|Barbosa|Alcala|plain=yes}}</ref> (July 27, 1857 &ndash; September 21, 1921) was a [[Puerto Rican citizenship|Puerto Rican]] [[physician]], [[sociologist]] and political leader. Known as the father of the Statehood for Puerto Rico movement,<ref name=doak>{{cite book|last1=Doak|first1=Robin|title=Struggling to Become American|date=2009|publisher=Infobase Publishing|isbn=9781438103976|page=18|url=https://books.google.com/books?id=oEBm4hxNbMIC&lpg=PA18&ots=h88V1_YTba&dq=Jos%C3%A9%20Celso%20Barbosa&pg=PA18#v=onepage&q=Jos%C3%A9%20Celso%20Barbosa&f=false|accessdate=3 October 2015}}</ref> Barbosa was the first Puerto Rican, and one of the first persons of African descent to earn a medical degree in the [[United States]].

After his return to the island in 1880, Barbosa made many contributions to medicine and public health. He initiated an early form of health insurance, encouraging employers to pay a fee to cover future needs of their employees. In 1900 Barbosa was among the first five Puerto Rican leaders appointed to the Executive Cabinet under Governor [[Charles H. Allen]], in the first civilian government organized by the United States. He served in the Cabinet until 1917. From 1917–1921, Barbosa served in the [[1st Puerto Rican Senate|first elected Puerto Rican Senate]].

==Early years ==
Barbosa was born in 1857 in the city of [[Bayamón, Puerto Rico|Bayamón]], [[Captaincy General of Puerto Rico|Puerto Rico]] to parents of African and European ancestry. He received both his primary and secondary education in Puerto Rico. He was the first person of multi-ethnic ancestry to attend Puerto Rico's prestigious Jesuit Seminary. After graduating from the seminary, Barbosa tutored private students to save money to attend college. In 1875, he moved to [[New York City]] to attend prep school, where he learned [[English language|English]] in a year.<ref name="Barbosa">{{cite web|last1=Virgen de los Angeles Cedeño Torres|title=José Celso Barbosa y Alcalá|website=La Red Biogr&aacute;fica de Puerto Rico|url=http://www.angelfire.com/ny/conexion/barbosa_alcala_jose_celso.html|accessdate=2 October 2015}}</ref>

==Education==
[[File:House of José Celso Barbosa.jpg|left|thumb|250px|Museo José Celso Barbosa, the doctor's birthplace and childhood home]]
Originally Barbosa wished to become a lawyer, but after he suffered a bout of [[pneumonia]] in [[New York City]], his doctor recommended that he study medicine. In 1877, he was admitted to the medical school of the [[University of Michigan]], where he graduated as [[valedictorian]] of the class of 1880.  Barbosa was the first person from Puerto Rico to earn a medical degree in the United States. He returned to Puerto Rico, where he set up his practice in his hometown of Bayamón.<ref name=rivera>{{cite web|last1=Rivera|first1=Magaly|title=Famous Puerto Ricans|url=http://welcome.topuertorico.org/culture/famousprA-C.shtml|website=Welcome to Puerto Rico!|accessdate=2 October 2015}}</ref>

At first the Spanish colonial government did not recognize Barbosa's medical degree, as it was not from a known European university. The American consul to the island intervened on behalf of Barbosa to have his United States degree recognized, so that he could practice.

Barbosa provided medical care all over the island. He introduced the novel idea of employers paying a fee for the future healthcare needs of their employees (a very early [[health insurance]] system). In 1893, Barbosa founded the first Puerto Rican cooperative and named it ''El Ahorro Colectivo.''<ref name="Barbosa" />

==Political career==
During the Spanish colonial period, Barbosa was a member of the Autonomous Party led by [[Román Baldorioty de Castro]], but left because of ideological differences.

In 1898, when the United States bombarded and blockaded San Juan during the [[Spanish–American War]], Barbosa and other doctors who lived in Bayamón, traveled to the town of [[Cataño, Puerto Rico|Cataño]] and took a ferry to the capital. Barbosa, as a member of the [[International Red Cross and Red Crescent Movement|Red Cross]], went to the aid of the wounded Puerto Rican and Spanish soldiers. He and his party on the ferry had to travel across [[San Juan Bay]] at risk, as they were under cannon fire. Barbosa and those with him were recommended by the Spanish government for the ''Cruz de la Orden del Mérito Naval'' (The Cross of the Order of Naval Merit) for their bravery.<ref name=iriarte>{{cite web|last1=Iriarte|first1=Luis M.|last2=Quiñones|first2=Denise|title=El Museo del 98|url=http://home.coqui.net/sarrasin/museo8.htm#anchor121033|website=1898 La Guerra Hispano Americana en Puerto Rico|accessdate=2 October 2015}}</ref>

As a result of the war, the United States made Puerto Rico one of its territories. On July 4, 1899, Barbosa formed the pro-statehood [[Republican Party of Puerto Rico|Puerto Rican Republican Party]]. He became known as the father of the statehood for Puerto Rico movement.<ref name="doak"/>
{{clear}}

==Later years==
[[Image:Bust of Jose Celso Barbosa.jpg|225px|thumb|{{pad|1.5em}}Bust of Dr. Barbosa]]
On June 5, 1900, President [[William McKinley]] named Barbosa, together with [[Rosendo Matienzo Cintrón]], [[José de Diego]], Manuel Camuñas and Andrés Crosas, as part of an Executive Cabinet under U.S.-appointed Governor [[Charles H. Allen]], the first civilian governor of the island. The Executive Cabinet also included six American members.<ref name="PRSAW">{{cite web|title=Chronology of Puerto Rico in the Spanish-American War|url=http://www.loc.gov/rr/hispanic/1898/chronpr.html|website=The World of 1898: The Spanish-American War|publisher=Library of Congress|accessdate=2 October 2015}}</ref> Barbosa served on the Executive Cabinet until 1917, dealing with a variety of governors appointed by the US during that period, and providing continuity in administration.

During this period, in 1907 he founded the newspaper ''[[El Tiempo (Puerto Rico)|El Tiempo]],'' the first bilingual newspaper published on the island.

With representative elections authorized in 1917, Barbosa ran for an [[at-large]] seat. He was elected, serving as a member of the [[1st Puerto Rican Senate|first Puerto Rican Senate]], from 1917 to 1921.

José Celso Barbosa died in [[San Juan, Puerto Rico|San Juan]] on September 21, 1921. He was buried in [[Santa Maria Magdalena de Pazzis Cemetery]] in [[Old San Juan]]. His daughter [[Pilar Barbosa]] became a historian, serving as the [[Official Historian of Puerto Rico]] from 1993 to 1997. She was also a political activist who carried on her father's work.<ref name=vargas-rosario>{{cite web|last1=Cirilo Toro Vargas|title=Pilar Barbosa de Rosario|url=http://www.angelfire.com/ny/conexion/barbosa_de_rosario_pilar.html|website=La Red Biogr&aacute;fica de Puerto Rico|accessdate=2 October 2015}}</ref>{{self published-inline|date=August 2014}}

==Legacy and honors==

*Puerto Rico has declared his birthday, July 27, an official holiday.<ref name=doak />
*[[José Celso Barbosa House Museum|Barbosa's house in Bayamón]] has been preserved and is operated as a [[historic house museum]]. Many of his awards, certificates, books and other artifacts of interest are on exhibit.<ref>[http://travelandsports.com/mjcb961.htm Museo José Celso Barbosa], ''Travel and Sports'' {{webarchive |url=https://web.archive.org/web/20070927014733/http://travelandsports.com/mjcb961.htm |date=September 27, 2007 }}</ref>  
*On August 1, 2006, the [[United States Postal Service]] Post Office at 100 Avenida RL Rodriguez in Bayamón was named as the "Dr. Jose Celso Barbosa Post Office Building".  The act for this was signed by President [[George W. Bush]].<ref name=pl-109-253>{{USPL|109|253}}</ref>

==See also==
{{Portal|Puerto Rico|biography}}
*[[List of Puerto Ricans]]
*[[Pilar Barbosa]]
{{Clear}}
==Notes==
<references group=note/>

==References==
{{Wikisource|Public Law 109-253}}
{{Commons category|José Celso Barbosa}}
{{Reflist}}
{{1917 PR Senate}}

{{Authority control}}

{{DEFAULTSORT:Barbosa, Jose Celso}}
[[Category:1857 births]]
[[Category:1921 deaths]]
[[Category:Burials at Santa María Magdalena de Pazzis Cemetery]]
[[Category:People from Bayamón, Puerto Rico]]
[[Category:Puerto Rican party leaders]]
[[Category:Puerto Rican people of African descent]]
[[Category:Puerto Rican people of European descent]]
[[Category:Puerto Rico statehood campaign]]
[[Category:Republican Party (Puerto Rico) politicians]]
[[Category:Members of the Senate of Puerto Rico]]
[[Category:University of Michigan alumni]]
[[Category:University of Michigan Medical School alumni]]