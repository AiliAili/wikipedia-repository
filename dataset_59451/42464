{{For|the vectored thrust engine produced by Bristol Siddeley|Rolls-Royce Pegasus}}
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name=Pegasus
 |image=Bristol-pegasus-brooklands.jpg
 |caption=Preserved Bristol Pegasus on display at the [[Brooklands Museum]]. The red circles are temporary blanking plates removed when the [[exhaust manifold]] is fitted
}}{{Infobox aircraft engine
 |type=[[Reciprocating engine|Piston]] [[Aircraft engine|aero engine]]
 |manufacturer=[[Bristol Aeroplane Company]]
 |first run=c.{{Avyear|1932}}
 |major applications=[[Fairey Swordfish]]<br>[[Short Sunderland]]<br>[[Vickers Wellington]]
 |number built = c.32,000
 |program cost =
 |unit cost =
 |developed from = [[Bristol Jupiter]]
 |developed into = [[Bristol Draco]]<br>[[Bristol Phoenix]]
 |variants with their own articles =
}}
|}

The '''Bristol Pegasus''' is a British nine-cylinder, single-row, air-cooled [[Radial engine|radial]] [[Aircraft engine|aero engine]]. Designed by [[Roy Fedden]] of the [[Bristol Aeroplane Company]] it was used to power both civil and military aircraft of the 1930s and 1940s. Developed from the earlier [[Bristol Mercury|Mercury]] and [[Bristol Jupiter|Jupiter]] engines, later variants could produce 1,000 [[horsepower]] (750&nbsp;kW) from its [[Engine displacement|capacity]] of 1,750 [[cubic inch]]es (28 L) by use of a geared [[supercharger]].

Further developments of the Pegasus created the [[fuel injection|fuel-injected]] [[Bristol Draco]] and the [[Diesel engine|diesel cycle]] [[Bristol Phoenix]], both types being produced in limited numbers. In contrast, by the end of production over 30,000 Pegasus engines had been built. Aircraft applications ranged from single-engine [[biplane]]s to the four-engined [[Short Sandringham]] and [[Short Sunderland|Sunderland]] [[flying boat]]s. Several [[Flight altitude record|altitude]] and [[Flight distance record|distance]] records were set by aircraft using the Pegasus.

The [[Bristol Siddeley]] company reused the name many years later for the [[turbofan]] engine used in the [[Hawker Siddeley Harrier]] and which became known as the [[Rolls-Royce Pegasus]] when [[Rolls-Royce Limited|Rolls-Royce]] took over that company. Two Bristol Pegasus engines remain airworthy in 2010, powering [[Fairey Swordfish]] aircraft operated by the [[Royal Navy Historic Flight]], other examples are preserved and on public display in [[aviation museums]].

==Design and development==

The Pegasus was designed by Sir [[Roy Fedden]] as the follow-on to the [[Bristol Aeroplane Company]]'s very successful [[Bristol Jupiter]], using lessons learned in development of the [[Bristol Mercury|Mercury]]. Although having a capacity (25 L) almost 15% smaller, the Mercury produced about as much power as the Jupiter, through a combination of [[supercharger|supercharging]] to improve the "charge", and various changes to increase the operating [[RPM]]. The power of a piston engine can be calculated by multiplying the charge per cylinder by the number of cycles per second; the Mercury improved both and thereby produced more power for a given size. The primary advantage was a much improved [[power-to-weight ratio]] due to better [[volumetric efficiency]].<ref name="Lumsden 2003, p.104">Lumsden 2003, p.104.</ref>
[[File:BristolPegasus.JPG|thumb|left|Bristol Pegasus fitted to a [[Fairey Swordfish]]]]
The Pegasus was the same size, displacement and general steel/aluminium construction as the Jupiter, but various improvements allowed the maximum engine speed to be increased from 1,950 to 2,600 rpm for take-off power. This improved performance considerably from the Jupiter's 580&nbsp;hp (430&nbsp;kW), to the first '''Pegasus II''' with 635&nbsp;hp (474&nbsp;kW), to 690&nbsp;hp (515&nbsp;kW) in the first production model '''Pegasus III''', and eventually to the late-model '''Pegasus XXII''' with 1,010&nbsp;hp (750&nbsp;kW) thanks to the two-speed [[supercharger]] (introduced on the '''Pegasus XVIII''') and [[octane rating|100-octane]] fuel. This gave rise to the claim "one pound per horsepower" reflecting the excellent power-to-weight ratio.

Some notable users of the Pegasus were the [[Fairey Swordfish]], [[Vickers Wellington]], and [[Short Sunderland]]. It was also used on the [[ANBO IV|Anbo 41]], [[Bristol Bombay]], [[Saro London]], [[Short Empire]], [[Vickers Wellesley]] and the [[Westland Wallace]]. Like the Jupiter before it, the Pegasus was also licensed by the [[PZL]] company in [[Poland]]. It was used on the [[PZL.23 Karaś]] and [[PZL.37 Łoś]] bombers.
In Italy [[Alfa Romeo]] built both the Jupiter (126-RC35) and the Pegasus under licence, with the engine based on the Pegasus designated as the [[Alfa Romeo 125|Alfa Romeo 126-RC34]] with the civil version as the 126-RC10.<ref name="aroca-qld.com">{{Cite web|url=http://www.arocaqld.com/library_articles/alfa_romeo_aero_engines.php|title=Alfa Aero Engines|accessdate=19 December 2010|work=aroca-qld.com}}</ref>  In [[Czechoslovakia]] it was built by [[Walter Engines]] and was known as the '''Pegas'''.<ref>[http://www.walterengines.com/about-us/history.htm Walter engines history - Walterengines.com] {{webarchive |url=https://web.archive.org/web/20080509084946/http://www.walterengines.com/about-us/history.htm |date=9 May 2008 }} Retrieved: 3 August 2009</ref>

Approximately 32,000 Pegasus engines were built.<ref name="Lumsden 2003, p.104"/> The Pegasus set three height records in the [[Bristol Type 138]]: in 1932, 1936 and 1937. It was used for the first flight over [[Mount Everest]] in the [[Westland Wallace]], and in 1938 set the world's long-distance record in [[Vickers Wellesley]]s.<ref>Bridgman (Jane's) 1998, p.270.</ref>

==Variants==
The Pegasus was produced in many variants, early prototype engines were unsupercharged but the majority used a geared supercharger, either single-speed or two-speed. Variant differences included [[compression ratio]]s, propeller reduction gear ratios and accessories.

==Applications==
[[File:Saro London 01.jpg|thumb|right|[[Saro London]]]]
''Note:''<ref>List from Lumsden (British aircraft only), the Pegasus may not be the main powerplant for these types</ref>
{{columns-list|4|
*[[ANBO IV]]
*[[Blackburn Baffin]]
*[[Blackburn Ripon]]
*[[Blackburn Shark]]
*[[Boulton Paul Mailplane]]
*[[Boulton Paul Overstrand]]
*[[Boulton Paul Sidestrand]]
*[[Bristol Bombay]]
*[[Bristol Type 118]]<ref>Barnes 1970 p242</ref>
*[[Bristol Type 120]]
*[[Bristol Type 138]]
*[[Douglas DC-2]]
*[[Fairey S.9/30|Fairey TSR I]]
*[[Fairey Seal]]
*[[Fairey Swordfish]]
*[[Fokker C.X]]
*[[Fokker D.XXI-5]]
*[[Fokker T.V]]
*[[Gloster Goring]]
*[[Handley Page H.P.43]]
*[[Handley Page H.P.47]]
*[[Handley Page H.P.51]]
*[[Handley Page Hampden]]
*[[Handley Page H.P.54 Harrow]]
*[[Hawker Audax]]
*[[Hawker Hart]]
*[[Hawker Osprey]]
*[[Junkers Ju 52]]
*[[Ju86|Junkers Ju 86K-4]]
*[[Koolhoven FK.52]]
*[[Letov Š-328]]
*[[LWS-6 Żubr]]
*[[PZL.23 Karaś]]
*[[PZL.37 Łoś]]
*[[Saro London]]
*[[Savoia-Marchetti SM.95]]
*[[Short Mayo Composite]]
*[[Short Sandringham]]
*[[Short Sunderland]]
*[[Short Empire]]
*[[Short Scylla|Short Syrinx]]
*[[Supermarine Stranraer]]
*[[Supermarine Walrus]]
*[[Vickers Type 253]]
*[[Vickers Valentia]]
*[[Vickers Vanox]]
*[[Vickers Vellox]]
*[[Vickers Vespa]]
*[[Vickers Viastra|Vickers Viastra X]]
*[[Vickers Victoria]]
*[[Vickers Vildebeest]]
*[[Vickers Vincent]]
*[[Vickers Virginia]]
*[[Vickers Wellington]]
*[[Vickers Wellesley]]
*[[Westland Wallace]]
*[[Westland Wapiti]]
*[[Westland PV.7]]
*[[Westland PV-3|Westland-Houston PV-3]]
}}

==Engines on display==
Bristol Pegasus engines can be viewed installed in aircraft at the [[Royal Air Force Museum London]] and the [[Imperial War Museum Duxford]]. An unrestored Pegasus recovered from the sea bed is in the care of the Bristol Aero Collection, which is closed while moving from Kemble Airport to Filton.
There is also an engine on display at the [[Brooklands Museum]] at Weybridge.

==Survivors==
As of October 2010 two Bristol Pegasus engines remain airworthy in England. They power the two Fairey Swordfish aircraft operated by the [[Royal Navy Historic Flight]].<ref>[http://www.royalnavyhistoricflight.org.uk/aircraft/ Royal Navy Historic Flight - Aircraft] Retrieved: 13 October 2010</ref>

==Specifications (Pegasus XVIII)==
[[File:Bristol Pegasus labeled.jpg|thumb|300px|Bristol Pegasus engine with some components labelled]]

{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully-formatted line with <li> -->
|ref=''Lumsden.''<ref>Lumsden 2003, p.112.</ref>
|type=9-cylinder, single-row, supercharged, air-cooled [[radial engine]]
|bore=5.75 in (146 mm)
|stroke=7.5 in (190 mm)
|displacement=1,753 in³ (28.7 L)
|length=61 in (1,549 mm)
|diameter=55.3 in (1,405 mm)
|width=
|height=
|weight=1,111 lb (504 kg)
|valvetrain=Four [[pushrod]]-actuated valves per cylinder – two intake and two [[sodium]]-filled exhaust
|supercharger=Two-speed [[centrifugal type supercharger]]
|turbocharger=
|fuelsystem=[[Claudel-Hobson]] [[carburetor|carburettor]]
|fueltype=100 [[octane rating|Octane]] [[gasoline|petrol]]
|oilsystem=[[Dry sump]] with one combination pressure/scavenge pump
|coolingsystem=Air-cooled
|power=<br>
* 965 hp (720 kW) at 2,475 rpm for takeoff at sea level
* 835 hp (623 kW) at 2,250 rpm at 8,500 ft (2,590 m), maximum continuous climb power
* 965 hp (720 kW) at 2,600 rpm at 13,000 ft (3,960 m), maximum power (emergency combat - 5 minutes only)
|specpower=0.55 hp/in³ (25 kW/L)
|specfuelcon=0.52 lb/(hp·h) (319 g/(kW·h))
|oilcon=0.28 oz/(hp·h) (11 g/(kW·h))
|power/weight=0.86 hp/lb (1.42 kW/kg)
|compression=6.25:1
|reduction_gear=[[Farman Aviation Works|Farman]] [[epicyclic gearing]], 0.5:1
|components_other='''Propeller:''' [[Dowty Rotol|Rotol]] three-blade with variable pitch
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=
* [[Bristol Jupiter]]
* [[Bristol Mercury]]
* [[Bristol Phoenix]]
* [[Alfa Romeo 125]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Pratt & Whitney R-1690]]
*[[Wright R-1820]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book |title=Bristol Aircraft since 1910|last= Barnes|first=C.H. |authorlink= |coauthors= |year=1970 |publisher=Putnam Publishing |location=London |isbn= 0-370-00015-3|page= |pages= |url=|ref=harv }}
* Bridgman, L, (ed.) (1998) ''Jane's fighting aircraft of World War II.'' Crescent. ISBN 0-517-67964-7
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
* Gunston, Bill. ''Development of Piston Aero Engines''. Cambridge, England. Patrick Stephens Limited, 2006. ISBN 0-7509-4478-1
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
{{refend}}

==Further reading==
* Gunston, Bill. ''Development of Piston Aero Engines''. Cambridge, England. Patrick Stephens Limited, 2006. ISBN 0-7509-4478-1

==External links==
{{Commons category|Bristol Pegasus}}
*[http://www.bristolaero.i12.com/exengines.htm Bristol Pegasus at the Bristol Aero Collection]
*[http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200953.html Bristol Pegasus use in altitude record - ''Flight'', 1932]
*[http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%202979.html "Improving the  Breed"] a 1936 ''Flight'' article on the Pegasus and Mercury

{{BristolAeroengines}}
{{Alfa Romeo aeroengines}}
{{Walter aeroengines}}

[[Category:Radial engines]]
[[Category:Bristol aircraft engines|Pegasus]]
[[Category:Aircraft piston engines 1930–1939]]