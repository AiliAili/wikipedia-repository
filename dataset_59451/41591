{{Infobox military conflict
|conflict = Battle of Gondar
|partof = the [[East African Campaign (World War II)|East African Campaign]] of [[World War II]]
|image = <!--[[File:EritreaCampaign1941 map-en.svg|300px]]-->[[File:The Battle of Gondar (1941).jpeg|300px]]
|caption = <!--Eritrean campaign, 1941-->Ethiopian painting of the battle
|date = 13–27 November 1941
|place = [[Gondar]], [[Ethiopia]]
|coordinates = {{Coord|12|36|00|N|37|28|00|E|display=INLINE,title}}
|map_type=Ethiopia
|latitude = 12.6
|longitude = 37.466667
|map_size=200
|map_caption={{center|Gondar, city and district [[Districts of Ethiopia|(woreda)]], in the [[Semien Gondar Zone]] of the [[Amhara Region]], north of [[Tana Lake]]}}
|map_label = Gondar
|result = Allied victory
|territory = Fall of [[Italian East Africa]]<br />Restoration of the [[Ethiopian Empire]]
|combatant1 = {{flagcountry|Kingdom of Italy}}<br />[[Royal Corps of Colonial Troops|''Regio Corpo Truppe Coloniali'']]
|combatant2 = {{flag|United Kingdom}}<br />{{flagicon|United Kingdom}} Commonwealth troops<br />{{flagicon|Ethiopia|1897}} Ethiopian irregulars
|commander1 = {{flagicon|Kingdom of Italy}} [[Guglielmo Nasi]]
|commander2 = {{flagicon|United Kingdom}} [[William Platt]]<br /> {{flagicon|United Kingdom}} [[Charles Christopher Fowkes|Charles Fowkes]]
|strength1 = 41,000<br />70 guns<br />1 aircraft
|strength2 = 2 East African Infantry brigades<br />Camforce (Ethiopian Patriots)<br />Kenya Armoured Car Regiment<br />South African Light Armoured Detachment{{sfn|Playfair|2004|p=320}}
|casualties1 = June–November: 300 Italian, 3,700 Ascari killed<br />8,400 wounded or sick{{sfn|Maravigna|1949|p=191}}<br />22,000 prisoners<br />1 aircraft{{sfn|Playfair|2004|p=321}}
|casualties2 =  final assault: 32 killed<br />182 wounded<br />6 missing<br />15 aircraft{{sfn|Playfair|2004|p=321}}
|campaignbox = {{Campaignbox East African Campaign (World War II)}}
}}
The '''Battle of Gondar''' or '''Capture of Gondar''' was the last stand of the Italian forces in [[Italian East Africa]] during the [[World War II|Second World War]]. The battle took place in November 1941, during the [[East African Campaign (World War II)|East African Campaign]]. Gondar was the main town of Amhara in the mountains north of Lake Tana in Ethiopia, at an elevation of {{convert|7000|ft|m|abbr=on}} and had an Italian [[garrison]] of 40,000, commanded by [[Italian Army ranks|''Generale'']] [[Guglielmo Nasi]].

==Background==
{{see also|Battle of Keren}}
After the defeat of the Italians at the Battle of Keren (1 April 1941), many of the remaining Italians withdrew to the strongholds of [[Amba Alagi]], [[Jimma]] and [[Gondar]]. Amba Alagi fell in May and Jimma fell in July.{{sfn|Playfair|2004|pp=200, 310–311, 313}} Gondar is the capital of Amhara on the high ground north of Lake Tana at {{convert|7000|ft|m|abbr=on}}. In 1941 it was a road junction but only the Amhara road had an all-weather surface. At Wolchefit, guarded by a garrison of Italian troops, {{convert|70|mi|km|abbr=on}} towards Amhara, the road [[chicane]]d up a {{convert|4000|ft|m|abbr=on}} escarpment, some parts having been cut into a vertical cliff. From Wolchefit to Gondar the road traced the edge of the escarpment and at Dabat, {{convert|30|mi|km|abbr=on}} short of Gondar and at Amba Giorgis were small garrisons. Only a minor road from Um Hagar to the north had a junction with the main road. West from the town, a fair-weather road in poor repair, led to Gallabat and had a garrison at Chilga. There were rough tracks to the west of Lake Tana which met at Gorgora and a better road ran east to Debra Tabor, also garrisoned and Dessie. At Kulkaber, {{convert|30|mi|km|abbr=on}} from Gondar, the road passed between Lake Tana and the hills; from Debra Tabor to Dessie, it was a soil road and impassable in rain.{{sfn|Playfair|2004|p=312}}

==Prelude==
{{Further|Battle of Culqualber}}
The possession of the Wolchefit and Kulkaber mountain passes was instrumental for attacking Gondar.
Wolchefit was defended by a garrison of about 4,000 men under Colonel Mario Gonella. The stronghold was besieged by irregular Ethiopian forces, led by British Major Ringrose, since May 1941; the besieging force was later augmented by the arrival of units from the British Indian Army and part of the 12th Afrian Division. Several attacks and counterattacks were launched between May and August 1941. On 28 September 1941, after losing in combat 950 men and running out of food, Gonella surrendered with 1,629 Italians and 1,450 colonial soldiers.{{sfn|Boca|2014|p=}}

On 13 November, a mixed force from the British [[2nd (African) Division (United Kingdom)|12th (African) Division]] under [[Major-General (United Kingdom)|Major-General]] [[Charles Christopher Fowkes|Charles Fowkes]]—supported by [[Ethiopia]]n irregular troops—attacked the key defensive position of Kulkaber and were repelled.{{sfn|Playfair|2004|p=319}} Kulkalber was besieged since early September and had already been subjected to several attacks and bombardments. A second attack on 21 November from several directions was resisted until the afternoon, when Italian posts began to surrender.{{sfn|Mead|2007|p=142}} In the final attack there were 206 British and Ethiopian casualties and 2,423 Italian and Ethiopian prisoners taken (Italian sources list Italian casualties as 1,003 killed, 804 wounded and 1,900 prisoners).{{sfn|Playfair|2004|p=320}}<ref>http://www.ilcornodafrica.it/st-melecaculqualber.pdf</ref>

By this point the Allies had total control of the skies: the Italians had one [[Fiat CR.42]] left, piloted by Sergente Giuseppe Mottet. On 22 November, in the ''[[Regia Aeronautica]]'''s final sortie in East Africa, he made a strafing run on British artillery at Kulkaber that killed the [[Commander, Royal Artillery]], Lieutenant-Colonel Ormsby. Afterwards, Mottet landed at Gondar, destroyed the plane and fought on with the army.{{sfn|Gustavsson|2014|nopp=y}}

==Battle==

===Mountain passes===

[[File:Capture of Gondar, November 1941.jpg|thumb|{{centre|British and Ethiopian movements against the Italians at Gondar}}]]
There were two [[mountain pass]]es that overlooked the town which were controlled by the Italian troops. They were invested by the two [[brigade]]s of the 12th (African) Division. The two Italian groups in the passes were cut off and were forced to [[Surrender (military)|surrender]] when their supplies ran out.{{sfn|Playfair|2004|pp=320–321}}

===Gondar town===

Once the Allied troops had taken the passes, they gained control of the heights overlooking Gondar and reached the town on 23 November. The garrison of Gondar was seriously depleted, since many Askari, having gone unpaid by the Italians, had [[desertion|deserted]]. The final assault on Gondar, where Nasi had his [[headquarters]], started at 5:30 a.m. on 27 November. The Azozo [[airfield]] was the initial objective; it was captured by midday of 27 November and shortly afterwards, Commonwealth troops reached the [[Fasilides]] Castle.<ref>[https://books.google.it/books?id=aHC-BAAAQBAJ&pg=PA568&lpg=PA568&dq=fasilides+1941&source=bl&ots=BCHSiGFjvS&sig=G6nXYIPD4BnSxHUGR6MBLUZY7O8&hl=it&sa=X&ved=0ahUKEwjA3Zvg5orRAhULBMAKHTGECm8Q6AEIPjAG#v=onepage&q=fasilides%201941&f=false The King's African Rifles - Volume 2]</ref> At 4:30 p.m., while the Kenya Armoured Car Regiment penetrated the outskirts of the town, Nasi sent his last message to Italy, explaining that the reserve brigade had been deployed on the southern front but had been unable to stop the attack, that enemy troops had passed the [[barbed wire]] and enemy armoured vehicles had entered the town. Nasi surrendered soon after.{{sfn|Mead|2007|p=142}}{{sfn|Playfair|2004|p=321}} Some Italian outposts fought on until 30 November, marking the end of the battle.

==See also==
{{Portal|World War II}}
* [[East African Campaign (World War II)]]
* [[List of military engagements of World War II]]
* [[Unatù Endisciau]]

==References==
{{Reflist|20em}}

==Sources==
{{refbegin}}
* {{cite book |ref={{harvid|Boca|2014}}
|series=Storia e società (Editori Laterza) |title=Gli italiani in Africa Orientale: La caduta dell'Impero |volume=III |trans_title=The Italians in East Africa: The Fall of the Empire |last=Boca |first=A. del |authorlink= |year=2014 |orig-year=1982 |publisher=Bari, Laterza |location=Roma |edition= |isbn=978-88-520-5496-9}}
* {{cite web |ref={{harvid|Gustavsson|2014}}
|last1=Gustavsson |first1=Håkan |title=Maresciallo Giuseppe Mottet |url=http://surfcity.kund.dalnet.se/italy_mottet.htm |date=14 August 2014 |website=Biplane Fighter Aces from the Second World War |accessdate=29 February 2016}}
* {{cite book |ref={{harvid|Maravigna|1949}}
|last=Maravigna |first=P. |title=Come abbiamo perduto la guerra in Africa |publisher=Tosi |location=Roma |year=1949 |language=Italian |oclc=716558562}}
* {{cite book |ref={{harvid|Mead|2007}}
|first=Richard |last=Mead |title=Churchill's Lions: A Biographical Guide to the key British Generals of World War II |year=2007 |publisher=Spellmount |location=Stroud (UK) |isbn=978-1-86227-431-0}}
* {{cite book |ref={{harvid|Playfair|2004}}
|series=History of the Second World War United Kingdom Military Series |title=The Mediterranean and Middle East: The Germans come to the help of their Ally (1941) |volume=II |last1=Playfair |first1=I. S. O. |last2=Flynn |first2=F. C. |last3=Molony |first3=C. J. C. |last4=Toomer |first4=S. E. |editor-last=Butler |editor-first=J. R. M. |authorlink=I. S. O. Playfair |editor-link=James Ramsay Montagu Butler |year=2004 |orig-year=1956 |publisher=[[HMSO]] |location=London |edition=Naval & Military Press |url=http://www.ibiblio.org/hyperwar/UN/UK/UK-Med-II/UK-Med-2-index.html |accessdate=25 June 2014 |isbn=978-1-84574-066-5 |display-authors=1}}
* {{cite book |first=David |last=Shireff |title=Bare Feet and Bandoliers: Wingate, Sandford, the Patriots and the Liberation of Ethiopia |year=1995 |publisher=Pen & Sword Military 2009 |isbn=978-1-84884-029-4}}
{{refend}}

==Further reading==
* {{cite book |title=Dust Clouds in the Middle East: Air War for East Africa, Iraq, Syria, Iran and Madagascar, 1940–42 |first=Christopher |last=Shores |year=1996 |location=London |publisher=Grub Street |isbn=978-1-898697-37-4}}

==External links==
* [http://www.desertwar.net/battle-of-gondar.html?PageSpeed=noscript Battle of Gondar: text and maps]

{{World War II}}

{{DEFAULTSORT:Gondar}}
[[Category:Battles and operations of World War II]]
[[Category:East African Campaign (World War II)|Battle of Gondar]]
[[Category:Conflicts in 1941]]
[[Category:Battles of World War II involving Italy]]
[[Category:Ethiopia in World War II]]
[[Category:Gondar|Battle of Gondar]]
[[Category:Italian East Africa|Battle of Gondar]]
[[Category:1941 in Ethiopia|Battle of Gondar]]
[[Category:1941 in Italy|Battle of Gondar]]
[[Category:Battles involving Ethiopia]]
[[Category:Battles of World War II involving the United Kingdom]]
[[Category:November 1941 events]]