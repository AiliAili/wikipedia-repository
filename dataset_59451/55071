'''Al Maryah Island''' (formerly known as '''Sowwah Island'''<ref name="thenationalstaff">{{cite web|url=http://www.thenational.ae/news/uae-news/sowwah-island-changes-its-name|title=Sowwah Island changes its name|author=The National staff|publisher=}}</ref>) is located northeast of [[Abu Dhabi]], the capital of the [[United Arab Emirates]]. It is named for the [[Arabian oryx]] ("al maryah" in Arabic), a large number of which once inhabited the surrounding area.<ref name="thenationalstaff"/>

Al Maryah Island covers approximately 114 hectares and is currently being developed by [[Mubadala Development Company|Mubadala]] to be a new city center. It is one of the largest construction sites in Abu Dhabi.<ref>{{cite web|url=http://www.emirates247.com/property/al-sowwah-is-now-called-al-maryah-island-2012-04-22-1.454984|title=Al Sowwah is now called Al Maryah Island|author=Staff|work=Emirates 24-7}}</ref> <ref>{{cite web|url=http://gulfnews.com/news/gulf/uae/general/abu-dhabi-renames-sowwah-island-as-al-maryah-1.1010507|title=Abu Dhabi renames Sowwah Island as Al Maryah|author=Staff Report|publisher=}}</ref> The island is being developed in several phases which will include construction of offices, residential buildings, hospitals like the [[Cleveland Clinic Abu Dhabi]] and leisure infrastructure.

Construction was started in 2007 and the final phase of the development is planned to be finished in 2014. The center of the island will be the newly constructed [[Sowwah Square]] which will accommodate the new stock exchange of Abu Dhabi as well as several other business towers.

The following buildings are planned to be built on Al Maryah Island:

*[[Sowwah Square Tower 1]], 37 floors
*[[Sowwah Square Tower 2]], 37 floors
*[[NBAD Tower]], 35 floors
*[[Rosewood Hotel (Abu Dhabi)|Rosewood Hotel]], 34 floors
*[[Sowwah Square Tower 3]], 31 floors
*[[Sowwah Square Tower 4]], 31 floors
*[[Al Hilal Bank Tower]], 24 floors
*[[Abu Dhabi Securities Exchange]] (Stock Exchange), 4 floors
*[[Cleveland Clinic Abu Dhabi]]
*[[Viceroy Abu Dhabi]]

== References ==
{{Reflist}}

== External links ==
* http://www.almaryah.ae/
* http://www.mubadala.com/en/what-we-do/real-estate/al-maryah-island
* http://www.thenational.ae/news/uae-news/sowwah-island-changes-its-name
* http://www.arabianbusiness.com/592288-sowwah-island
{{coord|24.50|54.39|display=title}}

{{Developments in Abu Dhabi}}
{{Abu Dhabi}}

[[Category:Buildings and structures under construction in the United Arab Emirates]]