__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Harakka
 | image=H-56_Harakka_II_(Helsinki_Museum_2001-09-15).JPG
 | caption=Harakka II glider at Suomen Ilmailumuseo (Finnish Aviation Museum), 2001
}}{{Infobox Aircraft Type
 | type=Primary glider
 | national origin=Finland
 | manufacturer=
 | designer=
 | first flight=February 1945
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=
 | developed from= 
 | variants with their own articles=
}}
|}

The '''''Harakka'''''  ("[[European magpie]]") was a [[primary glider]] produced for pilot training in Finland in the 1940s. Its design was typical of this class of aircraft, a "keel" with a pilot's seat suspended beneath a high, strut-braced monoplane wing, and carrying a conventional empennage at the end of an open framework.<ref name="H-12">"Harakka I (H-12)"</ref> First flown in February 1945, the type was built from plans by Finnish gliding clubs and soon replaced earlier primary gliders such as the [[Grunau 9]],<ref name="Vaasan">"Grunau 9 ja PIK-7 Harakka, kerhon 1940-50-lukujen alkeiskoulukoneet"</ref> becoming a standard piece of equipment in the clubs.<ref name="H-12"/><ref name="Hardy">Hardy 1982, p.74</ref>

In 1946, Raimo Häkkinen and Juhani Heinonen from [[Polyteknikkojen Ilmailukerho]] redesigned the Harakka to strengthen it.<ref name="PIK">"PIK-sarjan lentokoneet"</ref> This improved version became known as the '''Harakka II''' or '''PIK-7'''.<ref name="PIK">"PIK-sarjan lentokoneet"</ref><ref name="JAE">Taylor 1989, p.726</ref><ref name="H-57">"Harakka II (H-57)"</ref> In 1948, a single example of a more radically redesigned version designated '''Harakka III''' flew.<ref name="H-34">"Harakka III/PIK-7 (H-34)"</ref> This had the framework that supported the tail replaced by a single boom.<ref name="H-34"/>

Examples of the Harakka I and Harakka II are preserved at the [[Suomen ilmailumuseo]]<ref name="SI">"Aircraft on display" <nowiki>[sic]</nowiki></ref> and the [[Karhulan ilmailukerho Aviation Museum]],<ref name="H-12"/><ref name="H-57"/> with the sole Harakka III also preserved at the latter museum.<ref name="H-34"/>

<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==
* '''Harakka I''' - initial version 
* '''Harakka II''' - strengthened version (several dozen built)<ref name="PIK"/>
* '''Harakka III''' - version with redesigned tail (1 built)<ref name="H-34"/>

<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Harakka II) ==
{{aerospecs
|ref=<!-- reference -->"Harakka II (H-57)"
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=One pilot
|capacity=
|length m=5.72
|length ft=18
|length in=9
|span m=10.60
|span ft=34
|span in=9
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=1.30
|height ft=4
|height in=3
|wing area sqm=15
|wing area sqft=161
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=92
|empty weight lb=200
|gross weight kg=200
|gross weight lb=440
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=117
|max speed mph=73
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->10.5:1
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->1.2
|sink rate ftmin=<!-- sailplanes -->236

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category|Harakka}}
* {{cite web |title= Aircraft on display |work=The Finnish Aviation Museum website |url=http://www.suomenilmailumuseo.fi/en/planes.html |accessdate=2009-01-10 |archiveurl=https://web.archive.org/web/20081219054915/http://www.suomenilmailumuseo.fi/en/planes.html |archivedate=2008-12-19}}
* {{cite web |title=Grunau 9 ja PIK-7 Harakka, kerhon 1940-50-lukujen alkeiskoulukoneet
 |work=Vaasan Lentokerho website |url=http://www.vaasanlentokerho.fi/?action=INavigation::showArticleViewPage(6083) |accessdate=2009-01-10}}
* {{cite book |last= Hardy |first= Michael |title=Gliders and Sailplanes of the World |year=1982 |publisher=Ian Allan |location=Shepperton }}
* {{cite web |title=Harakka I (H-12) |work=Karhulan Ilmailukerho website |url=http://users.kymp.net/mode0449/har1.htm |accessdate=2009-01-10}}
* {{cite web |title=Harakka II (H-57) |work=Karhulan Ilmailukerho website |url=http://users.kymp.net/mode0449/har2.htm |accessdate=2009-01-10}}
* {{cite web |title=Harakka III/PIK-7 (H-34) |work=Karhulan Ilmailukerho website |url=http://users.kymp.net/mode0449/har3.htm |accessdate=2009-01-10 |archiveurl=https://web.archive.org/web/20120224205611/http://users.kymp.net/mode0449/har3.htm |archivedate=2012-02-24}}
* {{cite web |title=PIK-sarjan lentokoneet |work=Polyteknikkojen Ilmailukerho website |url=http://pik.tky.fi/joomla/index.php?option=com_content&task=view&id=43&Itemid=108 |accessdate=2009-01-08 }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London }}
<!-- ==External links== -->

{{PIK aircraft}}

[[Category:Finnish sailplanes 1940–1949]]
[[Category:PIK aircraft]]
[[Category:Glider aircraft]]