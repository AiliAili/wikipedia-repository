{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=John Albert Axel Gibson
|image=
|caption=
|birth_date={{Birth date|1916|08|24|df=yes}}
|death_date={{Death date and age|2000|07|01|1916|08|24|df=yes}}
|placeofburial_label=
|placeofburial=
|birth_place=[[Brighton]], [[England]]
|death_place=[[Nottingham]], [[England]]
|placeofburial_coordinates= <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname=Johnny
|birth_name=Axel John Albert von Wichmann
|allegiance={{flag|United Kingdom}}
|branch={{air force|United Kingdom}}
|serviceyears=
|rank=[[Squadron Leader]]
|servicenumber=
|unit=
|commands=[[No. 15 Squadron RNZAF]]
|battles=[[World War II]]
|battles_label=
|awards=[[Distinguished Service Order]]<br>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
|relations=
|laterwork=Airline Pilot and Executive
}}
'''John Albert Axel "Johnny" Gibson''' [[Distinguished Service Order|DSO]], [[Distinguished Flying Cross (United Kingdom)|DFC]] (24 August 1916 &ndash; 1 July 2000) was [[Royal Air Force]] officer and a noted [[World War II]] [[air ace]].

== Early life ==
John Gibson was born in [[Brighton]] on 24 August 1916, the only child of Violet Lilian (born Wells) and Axel Charles von Wichmann, later Wickman (the [[Coventry]] machine tool manufacturer A.C. Wickman 1894-1970) of Brighton and Hove. In 1920 he moved with his mother's family to [[New Zealand]], where she married James Gibson. John was educated in [[Auckland]] and at [[New Plymouth]] Boy's High School. He returned to the [[United Kingdom]] in 1938, where he began a short service commission in the [[Royal Air Force]].<ref name="dailytelegraph">{{cite news|url=http://www.telegraph.co.uk/news/obituaries/1349214/Squadron-Leader-Johnny-Gibson.html|title=Squadron Leader Johnny Gibson|date=19 July 2000|work=[[The Daily Telegraph]]|accessdate=2009-11-06}}</ref> He was commissioned as an [[acting pilot officer]] on probation on 9 July 1938.<ref>{{London Gazette|issue=34535|startpage=4814|endpage=4815|date=26 July 1938|accessdate=2009-11-06}}</ref> His commission was confirmed on 16 August 1939,<ref>{{London Gazette|issue=34713|startpage=7039|endpage=7040|date=20 October 1939|accessdate=2009-11-06}}</ref> just a few weeks before the UK entered World War II in September 1939.

== World War II ==
In May 1940, as [[Battle of France|France was invaded]] by [[Nazi Germany]], Gibson was posted to [[No. 501 Squadron RAF|No. 501 (City of Bristol) Squadron]], [[Royal Auxiliary Air Force]], and his squadron was dispatched from [[RAF Tangmere]] across the [[English Channel]] to [[Bétheniville]]. The squadron flew [[Hawker Hurricane]]s, which Gibson had not flown before, and on 27 May he scored his first kill when he destroyed a [[Heinkel 111]] bomber, and shared in another kill, before being shot down and landing close to [[Rouen]]. He was again shot down on 10 June over [[Le Mans]], and a week later his squadron withdrew to [[Jersey]] and then regrouped at [[Croydon Airport]]. On 29 August after shooting down a [[Me 109]], he was rescued from the English Channel after his aircraft was shot down and he parachuted to safety. The next day he was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]], the citation read:

{{quote|Pilot Officer John Albert Axel GIBSON (40969).

In August, whilst on an offensive patrol over Dover this officer engaged and destroyed a Junkers 87 and was afterwards shot down himself. Although his aircraft was in flames he steered it away from the town of Folkestone and did not abandon the aircraft until it had descended to 1,000 feet. Pilot Officer Gibson has destroyed eight enemy aircraft, and has displayed great courage and presence of mind.<ref>{{London Gazette|issue=34935|startpage=5289|date=30 August 1940|accessdate=2009-11-06}}</ref>}}

He was promoted [[flying officer]] on 3 September 1940,<ref>{{London Gazette|issue=34949|startpage=5582|endpage=5583|date=20 September 1940|accessdate=2009-11-06}}</ref> and later in the month he was wounded and hospitalised.<ref name="dailytelegraph"/>

In May 1941, Gibson was posted as an air firing instructor, and returned to flying as a [[flight (unit)|flight]] commander with [[No. 457 Squadron RAAF|No. 457 Squadron]], [[Royal Australian Air Force]]. On 3 September 1941 he was promoted to war substantive [[flight lieutenant]].<ref>{{London Gazette|issue=35309|startpage=5968|date=14 October 1941|accessdate=2009-11-06}}</ref> In May 1942, the Squadron moved to [[Australia]] and Gibson was posted to [[New Zealand]] with the [[Royal New Zealand Air Force]]. He joined [[No. 15 Squadron RNZAF|No. 15 Squadron]] of the Royal New Zealand Air Force and went to [[Tonga]] and flew [[Curtiss P-40|Kittyhawks]] as a flight commander.<ref name="dailytelegraph" /><ref name="nzfpm">{{cite web|url=http://www.nzfpm.co.nz/article.asp?id=gibson|title=Squadron Leader John Albert Axel Gibson|last=Brodie|first=Ian|publisher=New Zealand Fighter Pilots Museum|accessdate=2009-11-06}}</ref>

Gibson returned to New Zealand in December 1942 and attended a course at the Staff College in April 1943. In December 1943, he rejoined 15 Squadron at [[New Georgia]] in the [[Solomon Islands]] as [[Commanding Officer]].<ref name="nzfpm"/> He was confirmed as a temporary [[squadron leader]] on 1 July 1944.<ref>{{London Gazette|issue=36618|supp=yes|startpage=3399|date=18 July 1944|accessdate=2009-11-06}}</ref> On 16 March 1945 he was awarded the [[Distinguished Service Order]].<ref>{{London Gazette|issue=36986|supp=yes|startpage=1447|date=13 March 1945|accessdate=2009-11-06}}</ref> He returned to Europe and joined [[No. 80 Squadron RAF|No. 80 Squadron]] flying the [[Hawker Tempest]], and was again shot down and injured in March 1945.<ref name="dailytelegraph"/> After recovering,<ref name="dailytelegraph"/> he formally transferred to the RNZAF on 1 December 1945.<ref>{{London Gazette|issue=37497|supp=yes|startpage=1336|date=8 March 1946|accessdate=2009-11-06}}</ref> However, he soon returned to RAF service, and served successively as pilot and aide to [[Bernard Montgomery]] and [[Arthur Tedder]].<ref name="dailytelegraph"/><ref>{{London Gazette|issue=38318|startpage=3417|date=4 June 1948|accessdate=2009-11-06}}</ref> He resigned his commission in 1950,<ref name="dailytelegraph"/> but returned to the RAF in 1952, initially reverting to the rank of flight lieutenant.<ref>{{London Gazette|issue=39596|supp=yes|startpage=3809|date=11 July 1952|accessdate=2009-11-06}}</ref> He was promoted to squadron leader again on 1 January 1953.<ref>{{London Gazette|issue=39739|supp=yes|startpage=54|date=30 December 1952|accessdate=2009-11-06}}</ref> He finally retired from the RAF on 31 December 1954.<ref>{{London Gazette|issue=40390|supp=yes|startpage=502|date=21 January 1955|accessdate=2009-11-06}}</ref>

In the 1960s, Gibson formed [[Bechuanaland National Airways]] and [[Botswana National Airways]]. Beginning with a [[Douglas DC-3]], he expanded the airlines until they operated two DC-3s, two [[Douglas DC-4]]s, and a [[Air transports of heads of state and government|presidential aircraft]] for [[President of Botswana|Batswana President]] [[Seretse Khama]]. During the [[Biafran War]], Gibson was operating from [[Gabon]], and with his son Michael as co-pilot, he flew in supplies and helped to evacuate children. He later formed a charter airline Jagair, based at [[Kariba Dam]], [[Rhodesia]] ([[Zimbabwe]]) and after ceasing operations due to political difficulties, he was employed by his friend and fellow wartime Royal Air Force pilot, [[Ian Smith]], the [[Prime Minister of Rhodesia|Prime Minister]] of [[Rhodesia]], as an aviation operations officer.<ref name="dailytelegraph"/>

Gibson retired in 1982, and returned to the United Kingdom in 1987 where he settled in [[Nottingham]]. He died on 1 July 2000.<ref name="dailytelegraph" /><ref name="nzfpm"/>

== References ==
{{reflist}}

{{DEFAULTSORT:Gibson, John Albert Axel}}
[[Category:1916 births]]
[[Category:2000 deaths]]
[[Category:British aviators]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:New Zealand aviators]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:New Zealand World War II pilots]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:The Few]]