{{Infobox individual darts tournament
|tournament_name = [[World Darts Trophy]]
|image = 
|dates = 7 December 2002 – 13 December 2002
|venue = [[:nl:Vechtsebanen|De Vechtsebanen]]
|location = [[Utrecht (city)|Utrecht]], [[Utrecht (province)|Utrecht]]
|country = the [[Netherlands]]
|organisation = [[British Darts Organisation|BDO]] / [[World Darts Federation|WDF]]
|format = Men<br>[[Set (darts)|Sets]]<br>Final – Best of 11 [[Set (darts)|Sets]]<br>Women<br>[[Set (darts)|Sets]]<br>Final – Best of 5 [[Set (darts)|Sets]]
|prize_fund =
|winners_share =
|nine_dart =
|high_checkout = '''170''' {{flagicon|ENG}} [[Tony O'Shea]]
|winner = {{flagicon|AUS}} [[Tony David]] (men)<br>{{flagicon|NED}} [[Mieke de Boer]] (women)
|prev =
|next = [[2003 World Darts Trophy|2003]]}}

The '''2002 World Darts Trophy''' was the first edition of the [[World Darts Trophy]], a professional darts tournament held at the [[:nl:Vechtsebanen|De Vechtsebanen]] in [[Utrecht]], the [[Netherlands]], run by the [[British Darts Organisation]] and the [[World Darts Federation]].

The final of the first men's event was between [[Tony David]] and [[Tony O'Shea]], with Tony David beating Tony O'Shea in straight sets, 6–0. The [[2002_BDO_World_Darts_Championship#Men|BDO World Champion]] [[Mervyn King (darts player)|Mervyn King]] was eliminated in the second round also in straight sets by Tony David. In the final of the first women's event, [[Mieke de Boer]] defeated Crissy Howat, 3–1 in sets. [[Trina Gulliver]], the [[2002_BDO_World_Darts_Championship#Women|BDO World Champion]], was earlier beaten in the women's event by [[Anne Kirk]] in the quarter finals.

==Seeds==

'''Men'''
# {{flagicon|ENG}} [[John Walton (darts player)|John Walton]]
# {{flagicon|ENG}} [[Martin Adams]]
# {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
# {{flagicon|NED}} [[Raymond van Barneveld]]
# {{flagicon|FIN}} [[Marko Pusa]]
# {{flagicon|FIN}} [[Jarkko Komula]]
# {{flagicon|ENG}} [[Andy Fordham]]
# {{flagicon|SCO}} [[Bob Taylor (darts player)|Bob Taylor]]

== Prize Money ==

=== Men ===
{| class="wikitable" style="text-align: center;"
! style="background:#ffffff; color:#000000;"| Pos
! style="background:#ffffff; color:#000000;"| Money (Euros)
|-
| Winner
| 45,000
|-
| Runner-up
| 22,500
|-
| Semi-Finals
| 11,250
|-
| Quarter-Finals
| 6,000
|-
| Last 16
| 3,000
|-
| Last 32
| 2,000
|}

== Men's Tournament ==
{{32TeamBracket
| RD1=First Round<br />Best of 5 sets<ref name=2002WDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=271 | title=2002 World Darts Trophy Results | publisher=Darts Database | accessdate=6 June 2015}}</ref><ref name=2002WDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-men/2002 | title=2002 BDO World Darts Trophy Men | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD2=Second Round<br />Best of 5 sets<ref name=2002WDT/><ref name=2002WDT2/>
| RD3=Quarter-Finals<br />Best of 9 sets<ref name=2002WDT/><ref name=2002WDT2/>
| RD4=Semi-Finals<br />Best of 9 sets<ref name=2002WDT/><ref name=2002WDT2/>
| RD5=Final<br />Best of 11 sets<ref name=2002WDT/><ref name=2002WDT2/>
| score-width=15
| RD1-seed01=
| RD1-team01= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD1-score01= '''3'''
| RD1-seed02=
| RD1-team02= {{flagicon|ENG}} [[Shaun Greatbatch]]
| RD1-score02= 0
| RD1-seed03=
| RD1-team03= {{flagicon|ENG}} [[Colin Monk]]
| RD1-score03= 1
| RD1-seed04=
| RD1-team04= {{flagicon|ENG}} '''[[John Walton (darts player)|John Walton]]'''
| RD1-score04= '''3'''
| RD1-seed05=
| RD1-team05= {{flagicon|SCO}} '''[[Mike Veitch]]'''
| RD1-score05=  '''3'''
| RD1-seed06=
| RD1-team06= {{flagicon|WAL}} [[Richie Davies]]
| RD1-score06= 2
| RD1-seed07=
| RD1-team07= {{flagicon|NED}} Frans Harmsen
| RD1-score07= 1
| RD1-seed08=
| RD1-team08= {{flagicon|SCO}} '''[[Bob Taylor (darts player)|Bob Taylor]]'''
| RD1-score08= '''3'''
| RD1-seed09=
| RD1-team09= {{flagicon|ENG}} '''[[Steve Coote]]'''
| RD1-score09= '''3'''
| RD1-seed10=
| RD1-team10= {{flagicon|SWE}} [[Stefan Nagy]]
| RD1-score10= 0
| RD1-seed11=
| RD1-team11= {{flagicon|ENG}} [[Bobby George]]
| RD1-score11= 1
| RD1-seed12=
| RD1-team12= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD1-score12= '''3'''
| RD1-seed13=
| RD1-team13= {{flagicon|ENG}} '''[[Ted Hankey]]'''
| RD1-score13= '''3'''
| RD1-seed14=
| RD1-team14= {{flagicon|ENG}} [[Wayne Mardle]]
| RD1-score14= 2
| RD1-seed15=
| RD1-team15= {{flagicon|NED}} [[Jan van der Rassel]]
| RD1-score15= 1
| RD1-seed16=
| RD1-team16= {{flagicon|ENG}} '''[[Andy Fordham]]'''
| RD1-score16= '''3'''
| RD1-seed17=
| RD1-team17= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD1-score17= '''3'''
| RD1-seed18=
| RD1-team18= {{flagicon|NED}} Jean Paul van Acker
| RD1-score18= 0
| RD1-seed19=
| RD1-team19= {{flagicon|NED}} [[Vincent van der Voort]]
| RD1-score19= 0
| RD1-seed20=
| RD1-team20= {{flagicon|DEN}} '''[[Brian Sorensen|Brian Buur]]'''
| RD1-score20= '''3'''
| RD1-seed21=
| RD1-team21= {{flagicon|NED}} '''[[Co Stompe]]'''
| RD1-score21= '''3'''
| RD1-seed22=
| RD1-team22= {{flagicon|FIN}} [[Marko Pusa]]
| RD1-score22= 2
| RD1-seed23=
| RD1-team23= {{flagicon|ENG}} [[Matt Clark (darts player)|Matt Clark]]
| RD1-score23= 0
| RD1-seed24=
| RD1-team24= {{flagicon|ENG}} '''[[Wayne Jones (darts player)|Wayne Jones]]'''
| RD1-score24= '''3'''
| RD1-seed25=
| RD1-team25= {{flagicon|SCO}} '''[[Peter Johnstone (darts player)|Peter Johnstone]]'''
| RD1-score25= '''3'''
| RD1-seed26=
| RD1-team26= {{flagicon|FIN}} [[Jarkko Komula]]
| RD1-score26= 1
| RD1-seed27=
| RD1-team27= {{flagicon|ENG}} [[Andy Smith (darts player)|Andy Smith]]
| RD1-score27= 0
| RD1-seed28=
| RD1-team28= {{flagicon|ENG}} '''[[Tony Eccles]]'''
| RD1-score28= '''3'''
| RD1-seed29=
| RD1-team29= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD1-score29= '''3'''
| RD1-seed30=
| RD1-team30= {{flagicon|NED}} Johnn Snijers
| RD1-score30= 2
| RD1-seed31=
| RD1-team31= {{flagicon|BEL}} [[Erik Clarys]]
| RD1-score31= 0
| RD1-seed32=
| RD1-team32= {{flagicon|AUS}} '''[[Tony David]]'''
| RD1-score32= '''3'''
| RD2-seed01=
| RD2-team01= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD2-score01= '''3'''
| RD2-seed02=
| RD2-team02= {{flagicon|ENG}} [[John Walton (darts player)|John Walton]]
| RD2-score02= 2
| RD2-seed03= 
| RD2-team03= {{flagicon|SCO}} [[Mike Veitch]]
| RD2-score03= 1
| RD2-seed04=
| RD2-team04= {{flagicon|SCO}} '''[[Bob Taylor (darts player)|Bob Taylor]]'''
| RD2-score04= '''3'''
| RD2-seed05=
| RD2-team05= {{flagicon|ENG}} '''[[Steve Coote]]'''
| RD2-score05= '''3'''
| RD2-seed06=
| RD2-team06= {{flagicon|ENG}} [[Martin Adams]]
| RD2-score06= 2
| RD2-seed07=
| RD2-team07= {{flagicon|ENG}} [[Ted Hankey]]
| RD2-score07= 1
| RD2-seed08=
| RD2-team08= {{flagicon|ENG}} '''[[Andy Fordham]]'''
| RD2-score08= '''3'''
| RD2-seed09=
| RD2-team09= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD2-score09= '''3'''
| RD2-seed10=
| RD2-team10= {{flagicon|DEN}} [[Brian Sorensen|Brian Buur]]
| RD2-score10= 2
| RD2-seed11=
| RD2-team11= {{flagicon|NED}} [[Co Stompe]]
| RD2-score11= 1
| RD2-seed12=
| RD2-team12= {{flagicon|ENG}} '''[[Wayne Jones (darts player)|Wayne Jones]]'''
| RD2-score12= '''3'''
| RD2-seed13=
| RD2-team13= {{flagicon|SCO}} '''[[Peter Johnstone (darts player)|Peter Johnstone]]'''
| RD2-score13= '''3'''
| RD2-seed14=
| RD2-team14= {{flagicon|ENG}} [[Tony Eccles]]
| RD2-score14= 2
| RD2-seed15=
| RD2-team15= {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
| RD2-score15= 0
| RD2-seed16=
| RD2-team16= {{flagicon|AUS}} '''[[Tony David]]'''
| RD2-score16= '''3'''
| RD3-seed01=
| RD3-team01= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD3-score01= '''5'''
| RD3-seed02=
| RD3-team02= {{flagicon|SCO}} [[Bob Taylor (darts player)|Bob Taylor]]
| RD3-score02= 0
| RD3-seed03=
| RD3-team03= {{flagicon|ENG}} [[Steve Coote]]
| RD3-score03= 1
| RD3-seed04=
| RD3-team04= {{flagicon|ENG}} '''[[Andy Fordham]]'''
| RD3-score04= '''5'''
| RD3-seed05=
| RD3-team05= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD3-score05= '''5'''
| RD3-seed06=
| RD3-team06= {{flagicon|ENG}} [[Wayne Jones (darts player)|Wayne Jones]]
| RD3-score06= 0
| RD3-seed07=
| RD3-team07= {{flagicon|SCO}} [[Peter Johnstone (darts player)|Peter Johnstone]]
| RD3-score07= 2
| RD3-seed08=
| RD3-team08= {{flagicon|AUS}} '''[[Tony David]]'''
| RD3-score08= '''5'''
| RD4-seed01=
| RD4-team01= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD4-score01= '''5'''
| RD4-seed02=
| RD4-team02= {{flagicon|ENG}} [[Andy Fordham]]
| RD4-score02= 4
| RD4-seed03=
| RD4-team03= {{flagicon|NED}} [[Raymond van Barneveld|R van Barneveld]]
| RD4-score03= 4
| RD4-seed04=
| RD4-team04= {{flagicon|AUS}} '''[[Tony David]]'''
| RD4-score04= '''5'''
| RD5-seed01=
| RD5-team01= {{flagicon|ENG}} [[Tony O'Shea]]
| RD5-score01= 0
| RD5-seed02=
| RD5-team02= {{flagicon|AUS}} '''[[Tony David]]'''
| RD5-score02= '''6'''
}}

== Women's Tournament ==
{{8TeamBracket-Compact-NoSeeds|RD2=Quarter-Finals<br>Best of 3 sets<ref name=2002WWDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=823 | title=2002 Womens World Darts Trophy Results | publisher=Darts Database | accessdate=6 June 2015}}</ref><ref name=2002WWDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-ladies/2002 | title=2002 BDO World Darts Trophy Ladies | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD3=Semi-Finals<br>Best of 3 sets<ref name=2002WWDT/><ref name=2002WWDT2/>
| RD4=Final<br>Best of 5 sets<ref name=2002WWDT/><ref name=2002WWDT2/>
| team-width=200px
| RD1-seed01=
| RD1-team01= {{flagicon|BEL}} Vicki Pruim
| RD1-score01= 0
| RD1-seed02=
| RD1-team02= {{flagicon|ENG}} '''Crissy Howat'''
| RD1-score02= '''2'''
| RD1-seed03=
| RD1-team03= {{flagicon|ENG}} [[Trina Gulliver]]
| RD1-score03= 1
| RD1-seed04=
| RD1-team04= {{flagicon|SCO}} '''[[Anne Kirk]]'''
| RD1-score04= 2
| RD1-seed05=
| RD1-team05= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD1-score05= '''2'''
| RD1-seed06=
| RD1-team06= {{flagicon|SWE}} Carina Ekberg
| RD1-score06= 0
| RD1-seed07=
| RD1-team07= {{flagicon|NED}} [[Karin Krappen]]
| RD1-score07= 1
| RD1-seed08=
| RD1-team08= {{flagicon|NED}} '''[[Mieke de Boer]]'''
| RD1-score08= '''2'''
| RD2-seed01=
| RD2-team01= {{flagicon|ENG}} '''Crissy Howat'''
| RD2-score01= '''2'''
| RD2-seed02=
| RD2-team02= {{flagicon|SCO}} [[Anne Kirk]]
| RD2-score02= 1
| RD2-seed03=
| RD2-team03= {{flagicon|NED}} [[Francis Hoenselaar]]
| RD2-score03= 0
| RD2-seed04=
| RD2-team04= {{flagicon|NED}} '''[[Mieke de Boer]]'''
| RD2-score04= '''2'''
| RD3-seed01=
| RD3-team01= {{flagicon|ENG}} Crissy Howat
| RD3-score01= 1
| RD3-seed02=
| RD3-team02= {{flagicon|NED}} '''[[Mieke de Boer]]'''
| RD3-score02= '''3'''
}}

== References ==
{{Reflist|3}}

== External links ==

[[Category:Articles created via the Article Wizard]]
[[Category:Darts in the United Kingdom]]
[[Category:Competitions]]