{{Italic title}}
The '''''International Journal of Nursing Studies''''' is a monthly [[peer-reviewed]] [[nursing journal]] published by [[Elsevier]]. It covers the delivery of care in the fields of [[nursing]] and [[midwifery]].

==History==
The journal was established in 1963 and was originally published on a quarterly basis by [[Pergamon Press]]. The founding [[editor-in-chief]] was Elsie Stevenson ([[University of Edinburgh]]).<ref>{{cite web |url=http://www.ed.ac.uk/polopoly_fs/1.88532!/fileManager/Elsie%20Stephenson%20Lecture%20Notes%20-%20March%202012.pdf |last1= Ross |first1=Fiona |title=Elsie Stevenson Memorial Lecture: Elsie Stevenson's legacy: nurse leaders in universities today |format=PDF |date=2012-03-15 |accessdate=2015-01-02}}</ref> She was succeeded in 1968 by K. J. W. Wilson (University of Edinburgh, later [[University of Birmingham]]). Wilson stepped down in 1982 and was replaced by Rosemary Crow ([[Northwick Park Hospital]]) and Caroline Cox ([[University of London]]).<ref>{{cite journal |last1=Wilson |first1=Kathleen J. W. |title=Editor's announcement |url=http://www.sciencedirect.com/science/article/pii/002074898190002X |subscription=yes |journal=International Journal of Nursing Studies |volume=18 |issue=1 |pages=1 |year=1981 |doi=10.1016/0020-7489(81)90002-X}}</ref> Cox stepped down in 1992,<ref>{{cite journal |doi = 10.1016/0020-7489(92)90055-L|title = Editorial|journal = International Journal of Nursing Studies|volume = 29|pages = 1|year = 1992|last1 = Crow|first1 = Rosemary}}</ref> and Crow (then at the [[University of Surrey]]), remained as sole editor-in-chief for a further eight years, a total of 18 years making her the longest-serving editor to date.

From 2000-2004, Jenifer Wilson-Barnett ([[King's College London]]) was editor.<ref>{{cite journal |doi=10.1016/S0020-7489(99)00074-7|pmid=10684949|title=Editorial|journal=International Journal of Nursing Studies|volume=37|issue=2|pages=93–4|year=2000|last1=Wilson-Barnett|first1=J}}</ref><ref>{{cite journal |
doi=10.1016/j.ijnurstu.2004.02.002|
title=Editorial|
journal=International Journal of Nursing Studies|
volume=41|
issue=4|
pages=343|
year=2004|
last1=Wilson-Barnett|
first1=Jenifer}}</ref> She was succeeded in 2005 by the present editor [[Ian Norman]].

== Abstracting and indexing ==
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.901, ranking it first out of 100 journals in the category "Nursing".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Nursing |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.journalofnursingstudies.com/}}

[[Category:Publications established in 1963]]
[[Category:Monthly journals]]
[[Category:General nursing journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]