{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox University Boat Race
| name= 152nd Boat Race
| winner = Oxford
| margin = 5 lengths
| overall = 78&ndash;72
| winning_time= 18 minutes 26 seconds
| date= 2 April 2006
| umpire = Simon Harris<br>(Oxford)
| prevseason= [[The Boat Race 2005|2005]]
| nextseason= [[The Boat Race 2007|2007]]
| reserve_winner = Goldie
| women_winner = Oxford
}}
The 152nd [[The Boat Race|Boat Race]] took place on 2 April 2006. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford, whose crew contained the first French rower in the history of the event, won the race by five lengths which was umpired by former Oxford rower Simon Harris.

In the reserve race [[Goldie (Cambridge University Boat Club)|Goldie]] beat Isis and Oxford won the [[Henley Boat Races#Women's Boat Race|Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate =7 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 7 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=7 July 2014}}</ref><ref>{{Cite web | url = http://theboatrace.org/men/tv-and-radio| archiveurl=https://web.archive.org/web/20141006123036/http://theboatrace.org/men/tv-and-radio | archivedate= 6 October 2014 | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 7 July 2014}}</ref> Oxford went into the race as reigning champions, having won the [[The Boat Race 2005|2005 race]] by two lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher =The Boat Race Company Limited | title = Boat Race – Results| accessdate = 5 June 2014}}</ref> while Cambridge led overall with 78 victories to Oxford's 72 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher =The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 8 April 2014}}</ref> The race was sponsored by [[Xchanging]] for the second consecutive year,<ref>{{Cite web | url = http://www.bbc.co.uk/news/business-12888300 | publisher = BBC News| title = Boat Race sponsor Xchanging to end contract | date = 29 March 2011 | accessdate = 5 June 2014}}</ref> and was umpired by former Oxford [[Blue (university sport)|Blue]] Simon Harris.<ref>{{Cite web | url = https://www.theguardian.com/uk/2006/apr/03/media.topstories3 | title = Men going backwards make a splash| first = Sam | last = Wollaston | date = 3 April 2006 | accessdate  = 5 July 2014| work = [[The Guardian]]}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

==Crews==
The Oxford crew, whose average age was 24, comprised four Britons, two Canadians, two Americans and, in Bastien Ripoll, the first French rower to participate in the contest.<ref>{{Cite web | url = http://news.bbc.co.uk/sport2/hi/other_sports/rowing/4778632.stm| title = Oxford include Frenchman Ripoll | publisher = BBC Sport | accessdate = 6 June 2014 | date = 6 March 2006}}</ref> Cambridge's crew, with an average age of 26, consisted of three Britons, three Germans, an Australian, an American and a Canadian.
{| class=wikitable
|-
! rowspan="2"| Seat
! colspan="3"| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3"| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! Nationality
! Age
! Name
! Nationality
! Age
|-
| [[Bow (rowing)|Bow]] || [[Robin Esjmond-Frey]] || British || 20 || Luke Walton || American || 26
|-
| 2 || [[Colin Smith (rower)|Colin Smith]] || British || 22 || Tom Edwards (P) || Australian || 28
|-
| 3 || Tom Parker || British || 23 || [[Sebastian Thormann]] || German || 30
|-
| 4 || Paul Daniels || American || 24 ||  [[Thorsten Engelmann]] || German || 24
|-
| 5 || [[Jamie Schroeder]] || American || 24 || [[Sebastian Schulte]] || German || 27
|-
| 6 || [[Barney Williams]] (P) || Canadian || 29 || [[Kieran West]] || British || 28
|-
| 7 || [[Jake Wetzel]] || Canadian|| 29 || [[Tom James]] || British || 22
|-
| [[Stroke (rowing)|Stroke]] || Bastien Ripoll || French || 25 || [[Kristopher McDaniel|Kip McDaniel]] || Canadian || 24
|-
| [[Coxswain (rowing)|Cox]] || Seb Pearce || British || 23 || Peter Rudge || British || 24
|-
!colspan="7"| Sources:<ref name="BBC "/><ref>{{cite web|title=2006 Oxford Crew|url=http://www.oubc.org.uk/crew_lists/BB2006crewlist.html|publisher=Oxford University Boat Club|accessdate=11 May 2014|archiveurl=https://web.archive.org/web/20120419101956/http://www.oubc.org.uk/crew_lists/BB2006crewlist.html|archivedate=19 April 2012}}</ref><br>(P) &ndash; Boat club president
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss and elected to start from the Surrey station where they were afforded some shelter from the inclement conditions with strong winds creating choppy water.<ref name=BBC>{{cite web|url=http://news.bbc.co.uk/sport1/hi/other_sports/rowing/4870298.stm | title = Oxford triumph in 152nd Boat Race | date = 2 April 2006| accessdate = 20 April 2014| publisher = BBC Sport}}</ref> Under cloudy skies, Oxford took an early lead but Cambridge came back into contention and held a half-a-length lead at [[Harrods]].<ref name=toss>{{Cite web | url = http://www.telegraph.co.uk/sport/olympics/rowing/2334768/Oxford-the-masters-of-tossing-and-turning.html | work = [[The Daily Telegraph]] | first = Rachel | last = Quennell| date = 3 April 2006 | title = Oxford the masters of tossing and turning| accessdate= 6 July 2014}}</ref><ref>{{Cite web | url = http://news.bbc.co.uk/sport2/hi/photo_galleries/4870646.stm | title = Boat Race photos| publisher = BBC Sport | date = 2 April 2006 | accessdate = 6 June 2014}}</ref> As they crews passed under [[Hammersmith Bridge]], "wind and waves engulfed the boats and both seemed to check".<ref name=conq>{{Cite web | url = http://www.independent.co.uk/sport/general/oxford-conquer-the-waves-to-win-boat-race-472620.html | work = [[The Independent]] | title = Oxford conquer the waves to win Boat Race | accessdate =7 July 2014 | first =  Christopher | last =  Dodd | date = 3 April 2006}}</ref>  Oxford drew level and their cox Seb Pearce called for a push; his crew responded, moving away from Cambridge in the rough water.<ref name=toss/> Extending their lead out to two lengths by the Bandstand, Oxford pulled further away winning by five lengths in a time of 18 minutes and 26 seconds.<ref name=toss/>

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] beat Oxford's Isis.  Earlier, Oxford won the 61st [[Women's Boat Race]] by half-a-length in a time of 5 minutes 54 seconds.<ref name=results/>

==Reaction==
Cambridge coach Duncan Holland suggested "it's pretty hard to row with a boat full of water" while his cox Rudge claimed "Oxford were a little bit sheltered and it made a big difference".<ref name=blame/> His counterpart, Oxford cox Pearce, was jubilant: "It was awesome. I didn't expect it work out that way but it was great.  All along we were the better crew. The bookies just got it wrong."<ref name=blame>{{Cite web | url = http://news.bbc.co.uk/sport1/hi/other_sports/rowing/4870662.stm | title = Cambridge blame water for defeat| accessdate = 5 June 2014 | date = 2 April 2006 | publisher = BBC Sport | first = Martin | last = Gough}}</ref> Cambridge Boat Club president Edwards said "They handled the conditions better than us and that was it ... There was no more we could have done."<ref name=blame/> Oxford's president Barney Williams said "We were pushed really hard" while his French crew-mate Ripoll claimed "the key moment was along the island, in a washing machine".<ref name=conq/>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 2006}}
[[Category:The Boat Race]]
[[Category:2006 in English sport]]
[[Category:2006 in rowing]]