{{merge|Center for Naval Analyses|discuss=Talk:Center for Naval Analyses#Proposed merge with CNA (nonprofit)|date=October 2015}}
{{Infobox organization
| name = CNA
| logo = 
| type = 
| founded_date = 1942
| founder = 
| location = [[Arlington County, Virginia|Arlington]], [[Virginia]]
| key_people = Dr. Katherine A.W. McGrady, President and CEO 
| area_served =
| focus = Research and analysis services
| method =
| revenue = 
| endowment =
| num_volunteers =
| num_employees = 625
| num_members =
| owner =
| homepage = [http://www.cna.org/ www.cna.org]
| dissolved =
| footnotes =
}}

'''CNA''', formerly known as the '''CNA Corporation''', is a nonprofit research and analysis organization based in [[Arlington County, Virginia|Arlington, VA]]. CNA has around 625 employees.<ref name= "BeyondAcademia">{{citation |author= |url= http://www.beyondacademia.org/hiring-phds-cna/ |title= Hiring PhDs: Interview with the Director of Human Resources at CNA Corporation |website=BeyondAcademia.org |accessdate= September 4, 2015 |postscript= .}}</ref>

==General==
CNA operates:<ref name= "WashingtonPost">{{citation |author= The Washington Post |author-link= The Washington Post |url= http://projects.washingtonpost.com/top-secret-america/companies/center-for-naval-analyses/ |title= Center for Naval Analyses |accessdate= September 4, 2015 |postscript= .}}</ref>

* the [[Center for Naval Analyses]]. CNA's Center for Naval Analyses is the [[Federally funded research and development centers|federally funded research and development center (FFRDC)]] for the United States [[United States Navy|Navy]] and [[United States Marine Corps|Marine Corps]]. It has seven divisions: Advanced Technology & Systems Analysis, China Studies, Marine Corps Program, Operations & Tactics Analysis, Operations Evaluation Group, Resource Analysis, and Strategic Studies.<ref name= "CNASolutionCenters">{{citation |author= CNA |url= https://www.cna.org/centers/ |title= Solution Centers |accessdate= September 4, 2015 |postscript= .}}</ref>
* the Institute for Public Research. CNA's Institute for Public Research conducts research and analysis on domestic policy issues for federal, state, and local government agencies, including the [[United States Department of Homeland Security]], the [[United States Department of Justice]], the [[Federal Emergency Management Agency]], the [[Federal Aviation Administration]], and the [[United States Department of Education]].<ref name="BeyondAcademia"/> It has four divisions: Education; Energy, Water, & Climate; Enterprise Systems and Data Analysis; and Safety & Security.<ref name="CNASolutionCenters"/>
* the [[Military Advisory Board]]. The CNA Military Advisory Board is an American defense [[Advisory board|advisory group]] composed of retired three-star and four-star [[general officer|generals]] and [[admiral]]s from the [[United States Army|Army]], [[United States Navy|Navy]], [[United States Air Force|Air Force]], and [[United States Marine Corps|Marine Corps]] that studies pressing issues of the day to assess their impact on America's [[national security]].

==History==
In the early 1990s, the [[Center for Naval Analyses]] established CNA and in 1993 founded the Institute for Public Research in order to expand its work beyond the Department of Defense.<ref name= "History">{{citation |author= CNA |url= https://www.cna.org/about/history |title= History |accessdate= October 6, 2015 |postscript= .}}</ref> The first President and CEO of CNA was [[Robert J. Murray]].

==Leadership==

Katherine A.W. McGrady, Ph.D. is President and Chief Executive Officer of CNA.<ref name= "CNALeadership">{{citation |author= CNA |url= https://www.cna.org/about/leadership-bios#katherine-mcgrady |title= Katherine A.W. McGrady, Ph.D. |accessdate= September 4, 2015 |postscript= .}}</ref> She was previously CNA's Chief Operating Officer.<ref name= "WashingtonPost2">{{citation |author= Charity Brown |url= http://www.washingtonpost.com/wp-dyn/content/article/2009/09/06/AR2009090602331.html |newspaper= [[The Washington Post]] | title= New at the Top: Katherine A.W. McGrady |date= September 7, 2009 |postscript= .}}</ref>

'''Board of Trustees'''<ref name= "Board">{{citation |author= CNA |url= https://www.cna.org/about/board |title= CNA Board of Trustees |accessdate= October 6, 2015 |postscript= .}}</ref>
* [[Rozanne L. Ridgway]], Chair
* Frederick S. Benson, III
* Lieutenant General Robert R. Blackman, Jr., USMC (Ret.)
* [[Walter Broadnax]]
* [[Richard N. Cooper]]
* Daniel A. Domenech
* [[Carol Graham]]
* [[Maura Harty]]
* General [[Joseph P. Hoar]], USMC (Ret.)
* Katherine A.W. McGrady
* [[Gil Omenn|Gilbert Omenn]]
* Vice Admiral [[Adam M. Robinson, Jr.]], USN (Ret.)
* Laurie O. Robinson
* Roderick K. von Lipsey

==Recent research==

CNA research covers a variety of topics including national defense, education, energy, water and climate, air traffic management, and safety and security. A few recently released reports include:

* Retirement Choice 2015 (November 2015)<ref name= "RetirementChoiceCalculator">{{citation |author= Anita Hattiangadi |author2= Lewis G. Lee |author3= Robert Shuford |author4= Aline Quester |last-author-amp= yes |url= https://www.cna.org/cna_files/pdf/DRM-2015-U-011595-2Rev.pdf |publisher= CNA | title= Retirement Choice 2015 |date= November 17, 2015 |postscript= .}}</ref><ref name= "MilitaryTimesRCC">{{citation |author= Andrew Tilghman |url= http://www.militarytimes.com/story/military/benefits/retirement/2015/11/17/retirement-calculator-lets-troops-compare-options/75945446/ |newspaper= Military Times | title= Retirement 'calculator' lets troops compare options |date= November 17, 2015 |postscript= .}}</ref>
* Appalachia Rising: A Review of Education Research (September 2015)<ref name= "AppalachiaRising">{{citation |author= Patricia Kannapel |author2= Michael Flory |author3= Eric Cramer |author4= Renee Carr |last-author-amp= yes |url= https://www.cna.org/cna_files/pdf/CRM-2015-U-011063.pdf |publisher= CNA | title= Appalachia Rising: A Review of Education Research |date= September 16, 2015 |postscript= .}}</ref><ref name= "EducationWeek">{{citation |author= Jackie Mader |url= http://blogs.edweek.org/edweek/rural_education/2015/09/education_in_middle_appalachia_improving_obstacles_remain_report_says.html |newspaper= [[Education Week]] | title= Education in Middle Appalachia Improving, Obstacles Remain, Report Says |date= September 22, 2015 |postscript= .}}</ref>
* North Korea's Provocation and Escalation Calculus: Dealing with the Kim Jong-un Regime (August 2015)<ref name= "NorthKoreaReport">{{citation |author= Ken Gause |url= https://www.cna.org/CNA_files/PDF/COP-2015-U-011060.pdf |publisher= CNA | title= North Korea's Provocation and Escalation Calculus: Dealing with the Kim Jong-un Regime |date= August 31, 2015 |postscript= .}}</ref>
* The Potential Environmental Impacts of Fracking in the Delaware River Basin (August 2015)<ref name= "DRBFrackingReport">{{citation |author1=Steven Habicht |author2=Lars Hanson |author3=Paul Faeth |url= https://www.cna.org/CNA_files/PDF/IRM-2015-U-011300.pdf |publisher= CNA | title= The Potential Environmental Impacts of Fracking in the Delaware River Basin |date= August 11, 2015 |postscript= .}}</ref><ref name= "TimesTribune">{{citation |author= Brendan Gibbons |url= http://thetimes-tribune.com/news/report-details-risks-of-lifting-drilling-moratorium-1.1925858 |newspaper=[[The Times-Tribune (Scranton)]] | title= Report details risks of lifting drilling moratorium |date= August 12, 2015 |postscript= .}}</ref><ref name= "ThinkProgress">{{citation |author= Natasha Geiling |url= http://thinkprogress.org/climate/2015/08/12/3690443/study-fracking-delaware-river-basin/ |newspaper=[[ThinkProgress]] | title= Study: Fracking In The Delaware River Basin Would Threaten Health Of 45,000 |date= August 12, 2015 |postscript= .}}</ref>
* National Security and the Accelerating Risks of Climate Change (May 2014)<ref name= "2014MABReport">{{citation |author= CNA Military Advisory Board |author-link= Military Advisory Board |url= https://www.cna.org/cna_files/pdf/MAB_5-8-14.pdf |publisher= CNA Military Advisory Board | title= National Security and the Accelerating Risks of Climate Change |date= May 2014 |postscript= .}}</ref><ref name= "NYTimes">{{citation |author= Coral Davenport |url= https://www.nytimes.com/2014/05/14/us/politics/climate-change-deemed-growing-security-threat-by-military-researchers.html |newspaper=[[The New York Times]] | title= Climate Change Deemed Growing Security Threat by Military Researchers |date= May 13, 2014 |postscript= .}}</ref>
* In February 2014, CNA released an "Independent Assessment of the Afghan National Security Forces." This assessment, tasked by the United States Congress, outlines the likely security environment for [[Afghanistan]] over the next decade as well as an overview of the [[Afghan National Security Forces]] capabilities.<ref name= "CFR">{{citation |author= Council on Foreign Relations |url= http://www.cfr.org/afghanistan/cnas-independent-assessment-afghan-national-security-forces/p32450 |title= CNA's Independent Assessment of the Afghan National Security Forces |date= January 24, 2014 |postscript= .}}</ref><ref name= "StarsAndStripes">{{citation |author= Jon Harper |url= http://www.stripes.com/news/us/study-current-plans-for-afghan-security-forces-risk-failure-1.268976 |newspaper= [[Stars and Stripes (newspaper)|Stars and Stripes]] | title= Study: Current plans for Afghan security forces risk failure |date= February 21, 2014 |postscript= .}}</ref>

==References==
{{reflist|2}}

==External links==
* {{Official website|https://www.cna.org/ }}

[[Category:Non-profit corporations]]