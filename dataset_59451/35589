{{Good article}}
{{Use mdy dates|date=February 2012}}
{{Infobox television episode 
| title = 522666
| image =
| caption = 
| series = [[Millennium (TV series)|Millennium]]
| season = 1
| episode = 5
| airdate = November 22, 1996
| production = 4C05
| writer = [[Glen Morgan]]<br/>[[James Wong (producer)|James Wong]]
| director = [[David Nutter]]
| guests =
* [[Brittany Tiplady]] as [[Jordan Black (Millennium)|Jordan Black]]
*[[Sam Anderson]] as Agent Jack Pierson
*Robert Lewis as Agent Sullivan
*Joe Chrest as Raymond Dees
*[[Hiro Kanagawa]] as Agent Takahashi
*William MacDonald as Agent Nolan
*Roger Barnes as Agent Smith
*Deryl Hayes as Officer Mark Stanton
| prev = [[The Judge (Millennium)|The Judge]]
| next = [[Kingdom Come (Millennium)|Kingdom Come]]
| episode_list = [[Millennium (season 1)|List of season 1 episodes]]<br>[[List of Millennium episodes|List of ''Millennium'' episodes]]
}}

"''''522666'''" is the fifth episode of the [[Millennium (season 1)|first season]] of the American [[Crime (genre)|crime]]-[[Thriller (genre)|thriller]] television series ''[[Millennium (TV series)|Millennium]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] on November 22, 1996. The episode was written by [[Glen Morgan]] and [[James Wong (producer)|James Wong]], and directed by [[David Nutter]]. "522666" featured guest appearances by [[Sam Anderson]], [[Hiro Kanagawa]] and Joe Chrest.

[[Millennium Group]] consultant [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) is approached by the [[Federal Bureau of Investigation|FBI]] when a series of bombs are detonated in Washington, DC. Black's investigation soon reveals that the culprit seeks to be seen as a hero, setting off explosions in order to rescue people from the scenes; leaving Black to track down the fame-hungry bomber before more people are killed.

"522666" was one of many collaborations between Nutter, Morgan and Wong, with the three having worked together on several television series previously. The episode opens with a reference to [[existentialism|existentialist]] philosopher [[Jean-Paul Sartre]], and featured Henriksen performing all of his own stunts.

==Plot==

Outside a bar in Washington DC, Raymond Dees (Joe Chrest) calls 911 on a payphone. He says nothing, simply typing the numbers 522666 on the phone's keypad. Later, he watches the bar from a parking garage nearby, masturbating as the bomb he has left inside detonates.

[[Millennium Group]] consultant [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) watches the aftermath of the explosion on the news, knowing that the group will ask for his assistance with the case. Dees is among the rescuers seen on the broadcast. Black travels to DC and meets up with fellow group member [[Peter Watts (Millennium)|Peter Watts]] ([[Terry O'Quinn]]). The two join the [[Federal Bureau of Investigation|FBI]] task force investigating the bombing, led by [[special agent]]s Pierson ([[Sam Anderson]]) and Takahashi ([[Hiro Kanagawa]]). Watts and Black quickly dismiss several false claims of responsibility by terrorist groups. Black listens to the 911 call left by Dees, deducing that the numbers dialled spell the word ''kaboom'' on a [[telephone keypad]].

Black and the FBI investigate the crime scene; Black not only realises the bomber's proficiency with explosives, but is able to work out that he viewed the bombing from the parking garage. In a bin in the garage, they find a tissue covered in Dees' semen. Black informs the FBI that the bomber is smart enough to be able to tap into their phonecalls, and volunteers to bait him into eavesdropping on his mobile phone. Black's deduction is correct, and as he attempts to stall Dees on the phone while the FBI trace the call, he realises from Dees' language that the bomber is seeking to become famous through his actions. Dees informs the FBI that he has planned another bombing for the next morning.

The FBI task force rush to locate the bomb, tracing the phonecall to a small section of the city that might house it. Scanning the area, Black notices another parking garage opposite an office block, and attempts to have the building evacuated. However, Dees has planted a second bomb which detonates fifteen minutes early, while Black is inside the building. However, he is pulled to safety by a stranger, who is interviewed on the news following the explosion—Raymond Dees.

Black comes to in a hospital bed, tended to by his wife [[Catherine Black (Millennium)|Catherine Black]] ([[Megan Gallagher]]). She explains to him what has happened, and turns on the evening news to show him the interview with his rescuer. However, watching Dees speak, Black quickly realises he is the bomber. The FBI locate Dees' home, but his electronic surveillance had alerted him long before, and he has escaped before they even arrive. However, as Black sits in his car, he receives a call from Dees, who has booby-trapped the car. The FBI are able to monitor this call with Dees' equipment. Dees tells Black that they will both soon be famous, letting Black know that he has a remote detonator for the car's explosives. Before he can use it, he is killed by a police marksman.

When Black's car is searched, it is clear it was never rigged with anything—Dees had planned the whole thing, knowing that he would be killed. As news reports spread concerning the bomber's identity and his death at the hands of the police, Black sees that Dees has achieved the fame he longed for.

==Production==

[[File:Lance Henriksen Dragon Con.jpg|thumb|left|140px|Henriksen performed his own stunts in "522666".]]

"522666" was directed by [[David Nutter]], who had directed both "[[Pilot (Millennium)|Pilot]]" and "[[Gehenna (Millennium)|Gehenna]]" previously, and would also direct "[[Loin Like a Hunting Flame]]" later in the season.<ref name="season1book">{{cite AV media notes |title=Millennium: The Complete First Season |titlelink=Millennium (season 1) |year=1996–1997 |others=[[David Nutter]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> Writers [[James Wong (producer)|James Wong]] and [[Glen Morgan]] had previously written "[[Dead Letters (Millennium)|Dead Letters]]", and would go on to write an additional thirteen episodes during the first and [[Millennium (season 2)|second]] seasons of the series.<ref name="season1book"/><ref name="season2book">{{cite AV media notes |title=Millennium: The Complete Second Season |titlelink=Millennium (season 2) |year=1997–1998 |others=[[Thomas J. Wright]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> Nutter, Morgan and Wong had all previously collaborated on both ''Millennium''{{'s}} [[sister show]] ''[[The X-Files]]'',<ref>Edwards, p. 48</ref> and the Morgan and Wong-created series ''[[Space: Above and Beyond]]''.<ref name="above">{{cite episode| episodelink=Pilot (Space: Above and Beyond) | title=Pilot | series=[[Space: Above and Beyond]] |credits =[[David Nutter]] (director); [[Glen Morgan]] & [[James Wong (producer)|James Wong]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 1 | number = 1 |airdate= September 24, 1995}}</ref>

Guest star [[Hiro Kanagawa]], who portrays FBI special agent Takahashi, has appeared several times in ''Millennium''{{'s}} [[sister show]] ''The X-Files'', in the [[The X-Files (season 2)|second]]<ref>Edwards, p. 106</ref> and [[The X-Files (season 4)|fourth]] seasons;<ref>Edwards, p. 212</ref> as well as in ''The X-Files''{{'}} spin-off series ''[[The Lone Gunmen (TV series)|The Lone Gunmen]]''.<ref name="jimmybond">{{cite episode| episodelink=Bond, Jimmy Bond | title=Bond, Jimmy Bond | series=The Lone Gunmen | serieslink=The Lone Gunmen (TV series) |credits =[[Bryan Spicer]] (director); [[Vince Gilligan]], [[John Shiban]], & [[Frank Spotnitz]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 1 | number = 2 |airdate=March 11, 2001}}</ref> Kanagawa would also make several more appearances on ''Millennium'', acting in unrelated roles in the episodes "[[The Time Is Now (Millennium)|The Time Is Now]]",<ref name="timenow">{{cite episode| episodelink=The Time Is Now (Millennium) | title=The Time Is Now | series=Millennium | serieslink=Millennium (TV series) |credits =[[Thomas J. Wright]] (director); [[Glen Morgan]] & [[James Wong (producer)|James Wong]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 2 | number = 23 |airdate= May 15, 1998}}</ref> "[[Human Essence]]",<ref name="essence">{{cite episode| episodelink=Human Essence | title=Human Essence | series=Millennium | serieslink=Millennium (TV series) |credits =[[Thomas J. Wright]] (director); [[Michael Duggan]] (writer) | network = [[Fox Broadcasting Company|Fox]] | season = 3 | number = 8 |airdate= December 11, 1998}}</ref> and "[[Bardo Thodol (Millennium)|Bardo Thodol]]".<ref name="bardo">{{cite episode| episodelink=Bardo Thodol (Millennium) | title=Bardo Thodol | series=Millennium | serieslink=Millennium (TV series) |credits =[[Thomas J. Wright]] (director); [[Chip Johannessen]] & [[Virginia Stock]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 3 | number = 18 |airdate= April 23, 1999}}</ref> The episode also featured a guest appearance by William MacDonald as FBI Agent Nolan; MacDonald would later reappear in the [[Millennium (season 3)|third season]] episode "[[Through a Glass Darkly (Millennium)|Through a Glass Darkly]]".<ref name="darkly">{{cite episode| episodelink=Through a Glass Darkly (Millennium) | title=Through a Glass Darkly | series=Millennium | serieslink=Millennium (TV series) |credits =[[Thomas J. Wright]] (director); [[Patrick Harbinson]] (writer) | network = [[Fox Broadcasting Company|Fox]] | season = 3 | number = 7 |airdate= November 13, 1998}}</ref>

Lance Henriksen performed all his own stunts in this episode, having been introduced to acting by a stuntman friend of his.<ref>Genge, p. 60</ref> The episode opens with a quote from French [[existentialism|existentialist]] philosopher and writer [[Jean-Paul Sartre]]—"I am responsible for everything... except my very responsibility", which was taken from the 1943 treatise ''[[Being and Nothingness]]''. Sarte's writing echoes the motivations of the character of Raymond Dees, with author N. E. Genge noting that both believed that "just because the individual was incapable of changing destiny alone was no reason for him to stop trying".<ref>Genge, p. 135</ref>

==Broadcast and reception==

"522666" was first broadcast on the [[Fox Broadcasting Company|Fox Network]] on November 22, 1996,<ref>Shearman and Pearson, p. 108</ref> and earned a [[Nielsen rating]] of 7.6, meaning that roughly {{nowrap|7.6 percent}} of all television-equipped households were tuned in to the episode.<ref>Genge, p. ''xviii''</ref>

The episode received mixed to positive reviews from critics. ''[[The A.V. Club]]''{{'s}} Zack Handlen rated the episode a B-, finding that the episode's 'cat and mouse' chase between Black and Dees was "well constructed" and "exciting", and drawing comparisons between the episode and the film ''[[Seven (1995 film)|Seven]]''. However, he felt that the ending was poor, and that Megan Gallagher's portrayal of Catherine Black let the episode down.<ref name="AVC">{{cite web |url=http://www.avclub.com/articles/the-field-where-i-died522666,46673/ |title="The Field Where I Died" / "522666" {{!}} The X-Files/Millennium {{!}} TV Club |first=Zack |last=Handlen |publisher=''[[The A.V. Club]]'' |date=October 23, 2010 |accessdate=September 23, 2011}}</ref> Bill Gibron, writing for [[DVD Talk]], rated the episode 3 out of 5, finding that the focus on technological investigative techniques was "decidedly dull", and that although the episode's premise was initially "interesting", it grew "derivative after a while".<ref name="dvdtalk">{{cite web |url=http://www.dvdtalk.com/reviews/11615/millennium-season-1/ |title=Millennium: Season 1: DVD Talk Review of the DVD Video |first=Bill |last=Gibron |publisher=[[DVD Talk]] |date=July 20, 2004 |accessdate=September 23, 2011}}</ref> Robert Shearman and Lars Pearson, in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', note that the episode's premise is the first in the series "to play it entirely straight", avoiding Black's paranormal abilities. Shearman and Pearson rated the episode four-and-a-half stars out of five, describing it as not only "a chilling study of one man's madness, but an indictment upon the modern obsession with celebrity".<ref>Shearman and Pearson, pp. 108–109</ref> This interpretation has also been echoed by Mark Pizzato, in his work ''Inner Theatres of Good and Evil''. Pizzato claims that the episode "reflects the media's melodramatic fetishizing of villains and heroes, showing the bomber not only as a vulgar [[masturbation|onanist]], but also as Frank's savior and a martyr to the mass audience".<ref>Pizzato, p. 269</ref>

==Footnotes==
{{reflist|2}}

===References===

*{{Cite book |title=X-Files Confidential |first=Ted |last=Edwards |publisher=Little, Brown and Company |year=1996 |isbn=0-316-21808-1  }}
*{{cite book | year=1997|first1=N. E. |last1=Genge | title=Millennium: The Unofficial Companion |publisher=Century|isbn=0-7126-7833-6}}
*{{Cite book |title=Inner Theatres of Good and Evil: The Mind's Staging of Gods, Angels and Devils |first=Mark |last=Pizzato |publisher=McFarland |year=2010 |isbn=0-7864-4260-3}}
*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=0-9759446-9-X}}

==External links==
* {{IMDb episode|0648215}}
* {{Tv.com episode|25870}}

{{Millennium episodes|1}}

[[Category:Millennium (TV series) episodes]]
[[Category:1996 television episodes]]