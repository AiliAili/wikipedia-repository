<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = AD Scout
 |image =
 |caption =
}}{{Infobox Aircraft Type
 |type = [[fighter plane|Fighter]]
 |manufacturer = [[Air Department]]
 |designer =Harris Booth<ref name="Jackson">{{cite book|last=Jackson|first=Aubrey Joseph|title=Blackburn Aircraft since 1909|publisher=Putnam & Company Ltd.|location=London|date=16 March 1989|edition=1st|pages=98–101|isbn=0-85177-830-5}}</ref>
 |first flight =1915
 |introduction =
 |retired =
 |status =
 |primary user = [[Royal Naval Air Service]] - testing only
 |more users =
 |produced = 
 |number built = 4<ref name="Jackson"/>
 |unit cost =
 |developed from =
 |variants with their own articles =
}}
|}

The '''AD Scout''' (also known as the '''Sparrow''') was designed by [[Harris Booth]] of the [[British Admiralty]]'s [[Air Department]] as a [[fighter aircraft]] to defend Britain from [[Zeppelin]] bombers during [[World War I]].<ref name="Jackson"/>

==Design and development==
The Scout was a very unconventional aircraft - a biplane with a fuselage pod mounted on the ''upper'' wing. A twin-rudder tail was attached by four booms, and it was provided with an extremely narrow-track undercarriage. The primary armament was intended to be a 2-pounder [[recoilless rifle|recoilless]] [[Davis Gun]], but this was never fitted.<ref name="Jackson"/> Four prototypes were ordered in 1915 and two each were built by [[Hewlett & Blondeau]] and [[Blackburn Aircraft|the Blackburn Aeroplane & Motor Company]].

==Operational history==
Trials flown by pilots of the [[Royal Naval Air Service]] at [[Chingford]] proved the aircraft to be seriously overweight, fragile, sluggish, and difficult to handle, even on the ground. The project was abandoned and all four prototypes scrapped.<ref name="Jackson"/>

==Operators==
;{{UK}}
*[[Royal Naval Air Service]]

==Specifications (AD Scout)==
{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=The British Fighter since 1912<ref name="Mason fighter p42">*{{cite book|last=Mason|first=Francis K.|title=The British Fighter since 1912|publisher=Putnam & Company Ltd.|location=Annapolis, USA|year=1992|edition=|pages=|isbn=1-55750-082-7|page=42}}</ref>
|crew=one
|capacity=
|payload main=
|payload alt=
|length main= 22 ft 9 in
|length alt=6.93 m
|span main=33 ft 5 in
|span alt=10.18 m
|height main=10 ft 3 in
|height alt=3.12 m
|area main= 
|area alt= 
|airfoil=
|empty weight main= 
|empty weight alt= 
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)=[[Gnome Monosoupape]]<ref name="Bruce v1 p5">{{cite book|last=Bruce|first=J.M.|title=War Planes of the First World War: Volume One Fighters|publisher=Macdonald|location=London|year=1965|edition=|page=5|isbn=}}</ref>
|type of prop=[[rotary engine]]
|number of props=1
|power main= 100 hp 
|power alt=75 kW
|power original=
|max speed main= 84 mph <ref name="Lewis fighter p392-3">*{{cite book|last=Lewis|first=Peter|title=The British Fighter since 1912|publisher=Putnam & Company Ltd.|location=London|year=1979|edition=4th|isbn=0-370-10049-2|pages=392–393}}</ref>
|max speed alt=73 knots, 135 km/h
|cruise speed main= 
|cruise speed alt= 
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 210 miles {{Citation needed|date=October 2009}}
|range alt=336 km
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=
|guns= 1x 2-pounder (40 mm) [[Davis gun|Davis recoilless gun]] (intended, but never fitted in view of the fragility of the Scout's construction)
}}

==See also==
{{aircontent
<!-- designs which were developed into or from this aircraft: -->
|related=
<!-- aircraft similar in appearance or function to this design: -->
|similar aircraft=
<!-- the manufacturer or operator (military etc) sequence this aircraft belongs in: -->
<!-- any lists that are appropriate: -->
|lists=
<!-- other articles that could be useful to connect with: -->
|see also=
}}

==References==
;Notes
{{reflist}}


{{Air Department aircraft}}
{{wwi-air}}

[[Category:Cancelled military aircraft projects of the United Kingdom]]
[[Category:Air Department aircraft|Scout]]
[[Category:Blackburn aircraft]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Single-engined pusher aircraft]]
[[Category:Twin-boom aircraft]]