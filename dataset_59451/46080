{{Use dmy dates|date=April 2013}}
{{BLP sources|date=November 2012}} 
'''Zahra Al-Harazi''' (born c. 1971<ref name="Hers 2012">[http://issuu.com/calgaryherald/docs/hers_fall_2012/17 'By Her Own Design'], ''Hers'' Magazine (''Calgary Herald''), Fall 2012, page 17. Retrieved 2012-11-05.</ref>) was born in [[Uganda]], subsequently moving to Canada where she became an award-winning entrepreneur. She is the founder and director of [http://foundrycommunications.ca/ Foundry Communications].

==Early life and education==
Zahra Al-Harazi was born in Yemen. She immigrated to Canada in 1996 with her children.<ref name="AV Oct 2011">Drew Anderson, [http://albertaventure.com/2011/10/next-up-a-perfect-circle/ 'Next Up: A Profile of Zahra Al-Harazi of Foundry Communications'], ''Alberta Venture'', 15 October 2011. Retrieved 2012-11-05.</ref> Al-Harazi taught ESL at the Yemen American Language Institute until 1995.

Al-Harazi received her Bachelor of Design in Visual Communications from the [[Alberta College of Art and Design]] in [[Calgary, Alberta]].

==Career==

In 2006 Al-Harazi started Foundry Communications, with her business partner.<ref name="AV Oct 2011" /> Al-Harazi is now sole owner and Creative Director of Foundry Communications – an internationally awarded<ref name=(1)>http://www.summitawards.com/winners/winner-search.html?sobi2Task=sobi2Details&catid=0&sobi2Id=6627</ref>
<ref>http://marcomawards.com/list2011.php?a=F</ref> marketing and communications studio known for its strategic approach to design.   

During her career in the marketing and communications industries, Al-Harazi has been awarded multiple accolades, including Calgary’s Top 40 Under 40 (2009),<ref name=(2)>http://www.avenuecalgary.com/top-40-under-40/zahra-alharazi</ref> Business in Calgary's Leader of Tomorrow, Canadian Woman Entrepreneur of the Year by Chatelaine (2011),<ref name=(3)>http://site.chatelaine.com/womenoftheyear/entrepreneurs.aspx</ref> and has been named one of the most powerful women in Canada by the Women’s Executive Network (WXN).<ref name=(4)>	http://www.top100women.ca/?w=top100.2011winners</ref> She has also been awarded the [[Governor General]]'s [[Diamond Jubilee Medal]] for making a significant contribution to Canada. 

Al-Harazi has given speeches at institutions such as Young Women of Influence,<ref>[http://www.womenofinfluence.ca/zahra-al-harazi/ Woman of Influence keynote Speech]</ref> Ace Nationals,<ref>[https://vimeo.com/49249049 Ace Nationals Student Entrepreneurship Awards Keynote Speech]</ref> The Globe and Mail business conference, the Toronto Board of Trade, Mount Royal University, Women in Philanthropy,<ref>[https://vimeo.com/49272141 Women In Philanthropy Keynote Speech]</ref> Women's Executive Network and Entrepreneurs Organization Istanbul University. She has also been a judge for many national design competitions such as Design Edge, Applied Arts, Elevated HR, New ARs, and the Ace Student Entrepreneurship Awards.

Al-Harazi has written in the ''Financial Post''' about how her experiences and how her identity have shaped her into a successful businesswomen. Her advice to aspiring entrepreneurs is to "celebrate your individuality. In business and in life, it's a competitive advantage like no other".<ref>http://www.nationalpost.com/Celebrate+your+individuality/6286468/story.html here</ref>

== Personal Accolades ==

Al-Harazi has received several honors and awards throughout her career. Some of her recent accolades include:
* Calgary’s Top 40 Under 40 (2009)  <ref name="(2)"/>
* Business in Calgary’s “Leader of Tomorrow” (2010) 
* Canadian Woman Entrepreneur of the Year by Chatelaine (2011) <ref name="(3)"/>
* Most powerful woman in Canada by WXN (2011) <ref name="(4)"/>
* Governor General's Diamond Jubilee Medal

== Associations / Memberships ==
{{BLP unsourced section|date=November 2012}}
Al-Harazi is a member of several professional associations. She has made many contributions through her charitable work and has garnered respect from her colleagues and the design community at large. She has been asked to judge many national design competitions including Design Edge, New ARs and Applied Arts to name a few. Membership includes:

* Alberta Women Entrepreneurs (AWE) <ref name="awebusiness.com">http://www.awebusiness.com/pages/home/default.aspx</ref>
* Canadian Women in Communications <ref>http://www.cwc-afc.com/home.cfm</ref>
* Graphic Designers of Canada <ref>http://www.gdc.net/</ref>
* Sit at the Table (SATT) <ref>http://www.sitatthetable.org/</ref>
* Type Directors Club International <ref>http://tdc.org/</ref>
* Women’s Executive Network (WXN) <ref>http://www.wxnetwork.com/</ref>
* Women Working in Calgary (WWC)

== Board of Directors ==

* Learning and Events Chair – Entrepreneurs Organization (2011 – present) <ref>http://www.eonetwork.org/Pages/welcome.aspx</ref>
* Board member – Alberta Women Entrepreneurs (2010–2011) <ref name="awebusiness.com"/>
* Former member – Ad Rodeo Association (2008–2010) <ref>http://www.adrodeo.com/</ref>
* Former board of directors – Immigrant Services Calgary (2007–2010) <ref>http://www.immigrantservicescalgary.ca/</ref>
* board of directors – Lunchbox Theater

== Press ==
* [http://video.citytv.com/video/detail/1496100968001.000000/international-womens-day%E2%80%94march-8/ City TV interview on Breakfast Television]
* [http://www.sunnewsnetwork.ca/video/featured/news/868018287001/eminent-entrepreneur/1526422543001 Sun TV interview with Pat Bolland at the Roundtable]
* [http://vimeo.com/40256735 CTV interview with Jocelyn Laidlaw]
* [http://www.avenuecalgary.com/top-40-under-40/zahra-alharazi Avenue Magazine: Top 40 Under 40]
* [http://www.avenuecalgary.com/articles/page/item/calgary-mad-men-and-women Avenue Magazine: Mad Women]
* [http://www2.canada.com/calgaryherald/news/calgarybusiness/story.html%3Fid=4f0f9fbf-8ca9-410d-abf6-3ddc8be067eb Calgary Herald: Foundry lures top talent to Calgary] {{broken citation|date=November 2012}}
* [http://foundrycommunications.ca/blog/%3Fp=243 Business in Calgary: Leader of tomorrow] {{broken citation|date=November 2012}}
* [http://site.chatelaine.com/womenoftheyear/entrepreneurs.aspx Canada’s Women Entrepreneur of the Year]
* [http://www.theglobeandmail.com/report-on-business/careers/career-advice/my-career/lacking-canadian-credentials-foundry-owner-forged-a-new-path/article2211536/ Globe and Mail: Report on Business]
* [http://www.theglobeandmail.com/news/national/time-to-lead/photos-immigrant-entrepreneurs-past-and-present/article2115764/ Globe and Mail: Immigrant entrepreneurs past and present]
* [http://albertaventure.com/2011/10/next-up-a-perfect-circle/ Alberta Venture Magazine: A profile on Zahra Al-Harazi]
* [http://www.profitguide.com/article/4201--ones-to-watch Profit: W100 Canada’s top women entrepreneurs]
* [http://www.avenuecalgary.com/blogs/top-40-under-40-how-to-network Avenue Magazine – How to Network]
* [http://momventures.com/zahra-al-harazis-inspirational-rise-from-immigrant-mom-to-acclaimed-creator-entrepreneur-mentor-philanthropist/ Mom Ventures]
* [http://www.nationalpost.com/Celebrate+your+individuality/6286468/story.html National Post – Celebrate Your Individuality] {{broken citation|date=November 2012}}
* [http://issuu.com/calgaryherald/docs/hers_fall_2012/17 HERS Magazine: Fall 2012 Issue]
* [http://www.dolcemag.com/successstories/zahra-al-harazi/12270 Dolce Magazine]
* [http://www.anokhimagazine.com/dollars-sense/intelligent-design Anokhi Magazine: Intelligent Design]
* [http://blog.eonetwork.org/2013/01/project-octane-when-passion-meets-purpose/ Entrepreneurs' Organization: Project Octane]

== References ==
{{reflist}}

== External links ==

*Zahra Al-Harazi on Twitter [https://twitter.com/zahrasays @zahrasays]
*[http://www.foundrycommunications.ca www.foundrycommunications.ca Foundry Communications]

{{DEFAULTSORT:Harazi, Zahra}}
[[Category:Living people]]
[[Category:Yemeni businesspeople]]
[[Category:Yemeni emigrants to Canada]]
[[Category:Canadian women in business]]
[[Category:Yemeni women in business]]