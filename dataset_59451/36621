{{Infobox road
|state=AZ
|type=SR
|route=67
|alternate_name=Kaibab Plateau&nbsp;&ndash; North Rim Parkway
|maint=[[Arizona Department of Transportation|ADOT]] and [[National Park Service|NPS]]
|map=Arizona State Route 67 map.svg
|map_notes=A map of Northern Arizona delineating SR 67 in red.
|length_mi=43.4
|length_round=1
|length_ref=<ref name="ADOT Highway Log">{{cite web |date=September 5, 2003|url=http://www.azdot.gov/mpd/data/Reports/PDF/2008SHSLog.pdf |title=2008 ADOT Highway Log |accessdate=July 16, 2007 |page=143&ndash;144|format=PDF|author=Multimodal Planning Division|publisher=[[Arizona Department of Transportation]]|authorlink=Arizona Department of Transportation#Multimodal Planning Division}}</ref><ref name="bing"/>
|tourist=[[File:MUTCD D6-4.svg|20px|alt=|link=]] Kaibab Plateau–North Rim Parkway
|established=1941
|direction_a=South
|terminus_a=Bright Angel Point south of [[North Rim, Arizona|North Rim]]
|direction_b=North
|terminus_b={{jct|state=AZ|US|89A}} near [[Jacob Lake, Arizona|Jacob Lake]]
|previous_type=AZ
|previous_route=66
|next_type=AZ
|next_route=68
}}

'''State Route 67''' ('''SR 67''') is a {{convert|43.4|mi|km|abbr=on}} long, north&ndash;south state highway in northern [[Arizona]].  Also called the '''Kaibab Plateau&nbsp;&ndash; North Rim Parkway''', SR 67 is the sole road that links [[U.S. Route 89A (Arizona)|U.S. Route 89A]] (US 89A) at [[Jacob Lake, Arizona|Jacob Lake]] to the North Rim of [[Grand Canyon National Park]]. Along the route, the road heads through the national park as well as [[Kaibab National Forest]] and is surrounded by evergreen trees. The section inside the national park is maintained by the [[National Park Service]] (NPS), whereas the section north of the entrance, completely within Kaibab National Forest, is owned by the [[Arizona Department of Transportation]] (ADOT). The road was built in the late 1920s and improved through the 1930s. In 1941, the road received its number, and was given its designation as the parkway in the 1980s. The parkway has received designations as a [[National Forest Scenic Byway]] as well as a [[National Scenic Byway]].

==Route description==
Signage for SR 67 begins at Bright Angel Point along the North Rim of the Grand Canyon National Park. ADOT does not officially own this section of road, but it is signed as SR 67.<ref name="ADOT Highway Log"/> The road heads north as the [[Kaibab Plateau]]&nbsp;&ndash; North Rim Parkway through the small town of [[North Rim, Arizona|North Rim]],<ref>{{cite map|url=http://www.nps.gov/grca/planyourvisit/upload/GRCAmap2.pdf|title=Grand Canyon Area Map|publisher=[[National Park Service]]|format=PDF|accessdate=March 14, 2011}}</ref> surrounded by evergreen trees.<ref>{{cite web|url=http://www.arizonascenicroads.com/northern/kaibab_plateau_article_1.html|title=Kaibab Plateau&nbsp;&ndash; North Rim National Scenic Byway|work=Arizona Scenic Roads|author=Clayton, Robin|publisher=Arizona Office of Tourism|accessdate=March 14, 2011| archiveurl= https://web.archive.org/web/20110412233602/http://www.arizonascenicroads.com/northern/kaibab_plateau_article_1.html| archivedate= 12 April 2011 <!--DASHBot-->| deadurl= no}}</ref> The parkway enters a small clearing before meeting the park entrance, where ownership by ADOT begins. Heading into Kaibab National Forest on a northward path,<ref>{{cite map|url=http://www.fs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb5180563.pdf|format=PDF|title=Kaibab NF Vicinity Map|publisher=[[United States Department of Agriculture]]|accessdate=March 14, 2011}}</ref> the roadway is surrounded by a narrow meadow bordered by evergreen trees.<ref name="kaibab">{{cite web|url=http://www.arizonascenicroads.com/northern/kaibab_plateau_index.html|title=Kaibab Plateau&nbsp;&ndash;North Rim National Scenic Byway|author=Arizona Scenic Roads|publisher=Arizona Office of Tourism|accessdate=March 12, 2011| archiveurl= https://web.archive.org/web/20110412232132/http://www.arizonascenicroads.com/northern/kaibab_plateau_index.html| archivedate= 12 April 2011 <!--DASHBot-->| deadurl= no}}</ref> As it passes the nearby Deer Lake, SR 67 meets an unpaved [[Forest Highway|National Forest road]]. The landscape around the route is crisscrossed by these routes as SR 67 makes several turns, turning back toward the north. The highway, with the new name of Grand Canyon Highway in addition to its other designation,<ref name="bing"/> makes several turns as it heads north through the woods. It takes a more northwesterly path as it runs through [[Coconino County, Arizona|Coconino County]].<ref>{{cite map|url=http://www.azdot.gov/mpd/gis/maps/pdf/tbd5.pdf|title=Transportation District 5 Milepost System|cartography=Multimodal Planning Division|publisher=Arizona Department of Transportation|format=PDF|accessdate=March 14, 2011| archiveurl= https://web.archive.org/web/20110413001215/http://www.azdot.gov/MPD/gis/maps/pdf/tbd5.pdf| archivedate= 13 April 2011 <!--DASHBot-->| deadurl= no}}</ref> Near its terminus, the road turns back northeast toward its terminus at US 89A in Jacob Lake.<ref name="bing">{{bing maps|url=http://www.bing.com/maps/#Y3A9MzYuNDU2MzYzODA4MTUyODN+LTExMi4xMzU5NjUyMjgwODA3NSZsdmw9MTAmZGlyPTAmc3R5PXImcnRwPXBvcy4zNi4xOTc2MjI4MTQyOTU4MDVfLTExMi4wNTI5MjU1NjQ3ODc4N19uZWFyJTIwS2FpYmFiJTIwUGxhdGVhdS1Ob3J0aCUyMFJpbSUyMFBrd3klMkMlMjBOb3J0aCUyMFJpbSUyQyUyMEFaJTIwODYwNTJfX19hX35wb3MuMzYuNzEzNjc3MTE5Mzk0NDdfLTExMi4yMTU1MDQ1NTk1OTQ3OF9fX19hXyZtb2RlPUQmcnRvcD0wfjB+MH4=|title=SR 67|accessdate=March 10, 2011}}</ref>
[[File:AZ State Route 67 NF Entrance.jpg|left|thumb|alt=A photograph of|SR 67 at the Kaibab National Forest entrance]]
The northern segment of the highway is maintained by ADOT, who is responsible for maintaining SR 67 like all other highways around the state. As part of this job, ADOT periodically surveys traffic along its routes. These surveys are most often presented in the form of [[average annual daily traffic]], which is the number of vehicles who use the route on any average day during the year. In 2009, ADOT calculated that around 1,100 vehicles used the route daily at its northern terminus in Jacob Lake.<ref>{{cite web|url=http://www.azdot.gov/mpd/data/Reports/PDF/CurrentAADT.pdf|title=State Highway Traffic Log|publisher=Arizona Department of Transportation|page=18|author=Arizona Department of Transportation|format=PDF|accessdate=March 10, 2011| archiveurl= https://web.archive.org/web/20110413022509/http://www.azdot.gov/mpd/data/Reports/PDF/CurrentAADT.pdf| archivedate= 13 April 2011 <!--DASHBot-->| deadurl= no}}</ref> The [[Federal Highway Administration]] (FHWA) lists the highway as a National Scenic Byway, and the [[National Forest Service]] has also  designated it a National Forest Service Byway.<ref name=NSB-NFSB/> No part of the highway has been listed in the [[National Highway System (United States)|National Highway System]],<ref>{{cite map|url=http://www.azdot.gov/mpd/gis/maps/pdf/NHS.pdf|author=Multimodal Planning Division|date=September 2009|title=National Highway System|format=PDF|publisher=Arizona Department of Transportation|accessdate=March 7, 2011| archiveurl= https://web.archive.org/web/20110413001259/http://www.azdot.gov/MPD/gis/maps/pdf/NHS.pdf| archivedate= 13 April 2011 <!--DASHBot-->| deadurl= no}}</ref> a system of roads in the United States important to the nation's economy, defense, and mobility.<ref>{{cite web |url=http://www.fhwa.dot.gov/planning/national_highway_system/ |title= The National Highway System |publisher=Federal Highway Administration |date= August 26, 2010 |accessdate= January 1, 2011}}</ref> Due to the closure of park facilities on the north rim of the Grand Canyon during winter, winter maintenance is not undertaken after December 1, with the result that SR 67 is usually closed to vehicular traffic from December 1 until spring.<ref>{{cite news|url=http://www.deseretnews.com/article/695234738/Grand-Canyon-road-closed-for-winter.html|title=Grand Canyon Road Closed for Winter |newspaper=[[Deseret News]] |location=Salt Lake City |date=December 10, 2007 |accessdate=March 15, 2011}}</ref><ref>{{cite web|url=http://www.nps.gov/grca/planyourvisit/hours.htm|title=Grand Canyon National Park Operating Hours and Seasons|author=National Park Service|publisher=[[United States Department of the Interior]]|accessdate=March 15, 2011| archiveurl= https://web.archive.org/web/20110303012942/http://www.nps.gov/grca/planyourvisit/hours.htm| archivedate= 3 March 2011 <!--DASHBot-->| deadurl= no}}</ref>

==History==
[[File:Arizona State Route 67 2.jpg|right|thumb|alt=A photograph of|SR 67 in Kaibab National Forest]]
SR 67 existed as a route to reach the north rim of the Grand Canyon National Park as early as 1927 as a dirt road.<ref>{{cite map|url=http://www.aaroads.com/west/maps/1926-az.php|title=Map of Arizona|publisher=Arizona Highway Department|date=1926|accessdate=April 10, 2012}}</ref><ref>{{cite map|publisher=[[Rand McNally]]|title=Auto Road Map of Arizona and New Mexico|url=http://www.arizonaroads.com/maps/index.html|accessdate=April 29, 2008|year=1927| archiveurl= https://web.archive.org/web/20080506234706/http://www.arizonaroads.com/maps/index.html| archivedate= 6 May 2008 <!--DASHBot-->| deadurl= no}}</ref>  By 1935, the road had been improved to a [[gravel road]], and by 1938 it had been paved.<ref>{{cite map|publisher=Arizona State Highway Department|title=Road Map of Arizona|url=http://www.arizonaroads.com/maps/index.html|accessdate=April 29, 2008|year=1935| archiveurl= https://web.archive.org/web/20080506234706/http://www.arizonaroads.com/maps/index.html| archivedate= 6 May 2008 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite map|cartography=Rand McNally|publisher=[[Sinclair Oil]]|title=Road Map of Arizona and New Mexico|url=http://www.arizonaroads.com/maps/index.html|accessdate=April 29, 2008|year=1938| archiveurl= https://web.archive.org/web/20080506234706/http://www.arizonaroads.com/maps/index.html| archivedate= 6 May 2008 <!--DASHBot-->| deadurl= no}}</ref>  In 1941, the road was designated as a state highway and signed as SR 67.<ref>{{cite web|author=Arizona Department of Transportation|accessdate=April 29, 2008|date=February 25, 1941|title=ADOT Right-of-Way Resolution 1941-P-133|url=http://www.azhighwaydata.com/resolutions/?resnum=1941-P-133}}</ref>  In 1985, the highway received the designation of Kaibab Plateau&nbsp;&ndash; North Rim Parkway as an Arizona State Scenic Byway. The designation included SR 67 in its entirety as well as extending south into Grand Canyon National Park.<ref name="kaibab"/><ref>{{cite web|author=Miller, Charles|publisher=Arizona Department of Transportation|accessdate=April 29, 2008|title=ADOT Right-of-Way Resolution 1985-09-C-067|date=September 20, 1985|url=http://www.azhighwaydata.com/resolutions/pdf/resolutionfull/1985-09-C-067.pdf}}</ref> Two years later, ADOT obtained the [[right-of-way (transportation)|right-of-way]] for improvement of the highway from its northern terminus approximately {{convert|10|mi|km}} south.<ref>{{cite web|url=http://www.azhighwaydata.com/resolutions/?syear=1986&eyear=1988&crc=&rtnum=67&submit1=Submit|title=ADOT Right-of-Way Resolution 1987-04-A-020|date=April 17, 1987|author=Arizona Department of Transportation|publisher=Arizona Department of Transportation|accessdate=March 14, 2011}}</ref> In 1989, an additional right-of-way was acquired by ADOT from its southern terminus approximately {{convert|5|mi|km}} north.<ref>{{cite web|url=http://www.azhighwaydata.com/resolutions/?syear=1989&eyear=1990&crc=&rtnum=67&submit1=Submit|publisher=Arizona Department of Transportation|date=April 17, 1989|title=ADOT Right-of-Way Resolution 1989-03-A-020|author=Arizona Department of Transportation|accessdate=March 14, 2011}}</ref> By June of that year, the parkway received the designation of a National Forest Scenic Byway, and in June 1998, the Kaibab Plateau&nbsp;&ndash; North Rim Parkway received another designation, this time as a National Scenic Byway.<ref name=NSB-NFSB>{{cite web |url=http://www.byways.org/explore/byways/10465/designation.html |title= Kaibab Plateau&nbsp;– North Rim Parkway: Official Designations |publisher= [[Federal Highway Administration]] |work=America's Byways |accessdate=March 15, 2011}}</ref> Since establishment, the route has not been realigned and retains its original routing.<ref name="bing"/>

{{Clear}}

==Junction list==
{{jcttop|state=AZ|county=Coconino|length_ref=<ref name="ADOT Highway Log"/><ref name="bing"/>}}
{{AZint
|location=none
|mile=43.4
|road=Bright Angel Point
|notes=Southern terminus}}
{{AZint
|location=none
|mile=30.81
|road=Entrance to [[Grand Canyon National Park]]
|notes=ADOT ownership begins}}
{{AZint
|location=Jacob Lake
|mile=0.00
|road={{jct|state=AZ|US|89A}}
|notes=Northern terminus}}
{{jctbtm}}
[[File:GrandView1 pq.jpg|thumb|left|500px|alt=A panorama of the|View of the Grand Canyon from Grand Canyon Lodge near Bright Angel Point]]
{{Clear}}

==References==
{{reflist|2}}

==External links==
{{Commons category|Arizona State Route 67}}
*[http://arizonaroads.com/arizona/az67.html SR 67 at Arizona Roads]
*[http://www.byways.org/browse/byways/10465/ Kaibab Plateau&nbsp;&ndash; North Rim Parkway]
*[http://www.azdot.gov/Highways/districts/Flagstaff/index.asp ADOT Flagstaff District]

{{good article}}
{{Attached KML|display=title, inline}}

[[Category:State highways in Arizona|067]]
[[Category:Grand Canyon, North Rim]]
[[Category:National Scenic Byways]]
[[Category:Scenic highways in Arizona]]
[[Category:Transportation in Coconino County, Arizona]]
[[Category:Tourist attractions in Coconino County, Arizona]]