{{Infobox journal
| discipline = [[Educational technology]]
| former_names = 
| abbreviation = Brit. J. Educ. Technol.
| editor = Nick Rushby
| publisher = [[Wiley-Blackwell]] on behalf of the [[British Educational Research Association]]
| country = United Kingdom
| frequency = Bimonthly
| history = 1970-present
| impact= 2.098
| impact-year = 2010
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8535
| OCLC = 475047389
| LCCN = 72624376
| CODEN = BJEDTK
| ISSN = 0007-1013
| eISSN = 1467-8535
}}
The '''''British Journal of Educational Technology''''' is a [[peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the [[British Educational Research Association]]. The journal covers developments in [[educational technology]] and articles cover the whole range of education and training, concentrating on the theory, applications, and development of educational technology and communications.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic Search]]
* [[British Education Index]]
* [[CSA Biological Sciences Database]]
* [[CSA Environmental Sciences & Pollution Management Database]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[EBSCO Professional Development Collection]]
* [[EBSCO Sociological Collection]]
* [[Ecology Abstracts]]
* [[Education Index]]/Abstracts
* [[Educational Research Abstracts Online]]
* [[Ergonomics Abstracts]]
* [[ERIC Database]]
* [[FRANCIS]]
* [[International Bibliographies of Periodical Literature]]
* [[Inspec]]
* [[Linguistics & Language Behavior Abstracts]]
* [[Multicultural Education Abstracts]]
* [[OMNIFILE]] Full Text Mega Edition
* [[ProQuest]]
* [[Psychological Abstracts]]/[[PsycINFO]]
* [[Scopus]])
* [[Social Sciences Citation Index]]
* [[Sociology of Education Abstracts]]
* [[Studies on Women & Gender Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 2.098, ranking it 12th out of 206 journals in the category "Education & Educational Research".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Education & Educational Research |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8535}}

[[Category:Education journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]  
[[Category:Educational technology journals]] 
[[Category:Publications established in 1970]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:1970 establishments in the United Kingdom]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]