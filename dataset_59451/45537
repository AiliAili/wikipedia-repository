{{Infobox military person
|name=Gerhard Raht
|birth_date=6 June 1920
|death_date={{death date and age|1977|1|11|1920|6|6|df=y}}
|image=File:Gerhard Raht.jpg
|birth_place= [[Reinfeld, Schleswig-Holstein]]
|death_place= [[Reinfeld, Schleswig-Holstein]]
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1939–45
|rank=[[Hauptmann]]
|unit=[[NJG 3]], [[NJG 2]]
|commands=I./[[NJG 2]]
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]]
|laterwork=}}

'''Gerhard Ferdinand Otto Raht''' (6 June 1920 – 11 January 1977) was a German [[Luftwaffe]] night [[fighter ace]] and recipient of the [[Knight's Cross of the Iron Cross| Knight's Cross of the Iron Cross with Oak Leaves]] during [[World War II]]. The Knight's Cross of the Iron Cross and its higher grade Oak Leaves was awarded to recognise extreme battlefield bravery or successful military leadership. Raht claimed 58 nocturnal aerial victories in 171 combat missions.{{sfn|Obermaier|1989|p=75}}
Raht was [[List of German World War II night fighter aces|the tenth]] most successful night fighter pilot in the history of aerial warfare.{{sfn|Scutts|1998|p=88}}

==Early life and career==
Gerhard Raht was born on 6 June 1920 in [[Reinfeld, Schleswig-Holstein]] near [[Bad Oldesloe]], in [[Weimar Germany]]. He was born into a family a carpenters  who ran a furniture making business. After attending school and passing his ''[[Abitur]]'' (School Leaving Certificate) he opted to join the [[Luftwaffe]] as a ''[[Fahnenjunker]]'' (Officer candidate) on 1 October 1939, one month after the [[Polish Campaign|German-Soviet invasion of Poland]], which began [[World War II]] in [[Europe]]. On the 1 February 1941, as an officer candidate, he advanced to the rank of ''[[Leutnant]]''. In early 1942 he was assigned to 4 ''[[Organization of the Luftwaffe (1933–1945)#Staffel|Staffel]]'' (Squadron) of [[Nachtjagdgeschwader 3|''Nachtjagdgeschwader'' 3]] (''NJG'' 3—Night Fighter Wing 3).{{sfn|Stockert|2011|p=85}}

==World War II==
''NJG'' 3 was initially based as [[Stade]], defending northern Germany. On 3 April 1942 Raht was awarded the [[Iron Cross]] second class for his service.{{sfn|Stockert|2011|p=85}} It is not clear whether Raht had claimed any aerial successes prior to the award. 4./''NJG'' 3 received credit for the destruction of three bombers on the night of the 1/2 April 1942, between 00:20 and 01:28. The names of the claiming crews are not listed and the type of enemy aircraft and location is also omitted from German records.{{sfn|Foreman|Parry|Matthews|2004|p=37}}

Raht's first official victory was recorded on the night of the 26/27 July 1942 when he shot down a [[Royal Air Force]] (RAF) [[Handley-Page Halifax]] southwest of [[Pellworm]] at 01:019.{{sfn|Foreman|Parry|Matthews|2004|p=51}} This night, [[Air Marshal]] [[Sir Arthur Harris, 1st Baronet|Arthur Harris]], [[Air Officer Commanding]] (AOC) [[RAF Bomber Command]] sent a force of 181 [[Vickers Wellington]]s, 77 [[Avro Lancaster]], 73 Halifax, 39 [[Short Stirling]] and 33 [[Handley-Page Hampden]]s against [[Hamburg]], in one of the first raids against the city, before Operation ''Gomorrah'', exactly a year later.{{sfn|Everitt|Middlebrook|2014|p=41}} By the date of this first victory, Raht had transferred to 5 ''staffel''.{{sfn|Foreman|Parry|Matthews|2004|p=51}} Raht's second victory occurred on 17 January 1943 when he claimed a Lancaster over [[Hademarschen]] at 21:59. The victory was recorded with 4 ''staffel'', indicating he had returned to his original unit.{{sfn|Foreman|Parry|Matthews|2004|p=65}} Raht was promoted to ''[[Oberleutnant]]'' on 1 February 1943.{{sfn|Stockert|2011|p=85}}

===Ruhr and Baltic===
The night war escalated dramatically in 1943. Harris felt that Bomber Command has amassed sufficient resources to attack and destroy the [[Ruhr]] where large concentrations of German war industry was located, and particularly the [[steel]] producing centres of Germany. Harris had 53 squadrons available, of which 17 were [[medium bomber]] units. Harris also had pathfinder units equipped with [[H2S radar]] which mapped the terrain and could penetrate through haze and smoke for greater bombing accuracy. Bomber Command also utilised [[Chaff (countermeasure)|"window"]] to confound German ground radar in July. The attacks were dubbed the [[Battle of the Ruhr]] (March–July 1943). Raht and ''NJG'' 3 were heavily involved in countering Bomber Command's operations.{{sfn|Foreman|Parry|Matthews|2004|p=69}}

On the night of 10/11 March 1943 Raht claimed his third victory. At 22:12 south of [[Middelfart]], [[Denmark]]. This victory report suggests Raht had once again returned to 5 ''staffel''. He was also the only German night fighter pilot in the Luftwaffe to claim a victory this night.{{sfn|Foreman|Parry|Matthews|2004|p=70}} Bomber Command reported the loss of two Lancasters on mine-laying operations this night.{{sfn|Everitt|Middlebrook|2014|p=134}} The bomber was identified as Lancaster I [http://www.flensted.eu.com/19430013.shtml ''ED305''], KM-S, [[No. 44 Squadron RAF]] crashed into [[Lille Bælt]]. Pilot [[Sergeant]] Brian T. C. Smith, Flight Engineer Sergeant Gordon R. Black [[RCAF]], navigator [[Flying Officer]] Robert H. Carr, [[Bombardier (aircrew)|Bombardier]] Sergeant Charles H. D. Cook, wireless operator Geoffrey S. Love, air gunners Sergeant Charles V. Brown and Alfred Healey were [[killed in action]].{{sfn|Chorley|1996|p=69}} On 13 March Raht was awarded the Iron Cross First Class and appointed ''[[Staffelkapitän]]'' of 1./''NJG'' 3.{{sfn|Stockert|2011|p=85}}

On 30 March at 00:50, over [[Welmbuttel]], Raht shot down a Halifax.{{sfn|Foreman|Parry|Matthews|2004|p=72}} Raht became a night fighter ace on 21/22 June when he claimed a Stirling west of [[Antwerp]], [[Belgium]] at 01:32.{{sfn|Foreman|Parry|Matthews|2004|p=87}} His victim was claimed out of a 705-strong force that bombed [[Krefeld]]. 262 Lancaster, 209 Halifax, 117 Stirling, 105 Wellington bombers, supported by 12 [[de Havilland Mosquito]]s took part. Bomber Command lost 44; 17 Halifax and exactly nine each of the Lancaster, Stirling, and Wellington types were lost.{{sfn|Everitt|Middlebrook|2014|p=171}} In on the night of 29/30 July 1943 Raht achieved his most successful night to date, downing four bombers. Over [[Hanover]] at 23:10 and southeast [[Heide]] at 00:23 he claimed an unidentified heavy bomber and a Lancaster to increase his personal telly to 8.{{sfn|Foreman|Parry|Matthews|2004|p=98}} Hovering in the same sector he downed a Halifax southeast of Heide at 00:40 and then northeast of the town at Raht caught and shot down another Halifax at 00:52 for his 10th victory.{{sfn|Foreman|Parry|Matthews|2004|p=98}} One of the bombers was [[No. 158 Squadron RAF]] Halifax II, [[United Kingdom military aircraft serials|''JD277'']] NP-G. Flying Officer A. H. Boyle survived but pilot Flight Sergeant N. R. McDonald and five others were killed.{{sfn|Chorley|1996|p=250}} The other may have been Halifax II ''JB956'', KN-O of [[No. 77 Squadron RAF]] flown by [[Flight Sergeant]] George Henry Sutton. All eight men were killed on their 11th operation. Raht was flying a [[Dornier Do 217]] on this operation.{{sfn|Bowman|2016|p=176}}

On 17/18 August 1943, Harris ordered [[Operation Hydra (1943)|Operation ''Hydra'']], a series of attacks against the [[Peenemünde Army Research Center]] producing [[V-weapons]]. Raht scrambled to intercept but only caught one Lancaster with his radar operator east of [[Flensburg]] at 02:57.{{sfn|Foreman|Parry|Matthews|2004|p=104}} Raht spotted the bomber caught in searchlights and fired off an identification flare to force the anti-aircraft batteries to cease-fire against the bomber. Raht swiftly dispatched the bomber, identified as ''ED725'' PM-P. It belonged to [[No. 103 Squadron RAF]] and P. J. O'Donnell and his crew were killed.{{sfn|Middlebrook|2014|p=183}}{{sfn|Chorley|1996|p=273}} On 23/24 August Raht claimed his 12th victory southeast of Thomsdorf at 00:58.{{sfn|Foreman|Parry|Matthews|2004|p=106}} Raht did not score a victory again until 27/28 September 1943, when he accounted for a Lancaster over [[Hanover]]. The report lists Raht as belonging to 4./''NJG'' 3 at this time.{{sfn|Foreman|Parry|Matthews|2004|p=117}} Raht claimed his 14th victory northeast of [[Stendal]]. The report described the enemy aircraft as a four engine bomber by did not specify the type.{{sfn|Foreman|Parry|Matthews|2004|p=123}} The 15th claim was filed that same night, when he reported a Lancaster shot down over [[Hagenau]].{{sfn|Foreman|Parry|Matthews|2004|p=124}}

===Berlin and North Sea=== 
On 18 November 1943 Harris began his [[Battle of Berlin (RAF campaign)|Berlin offensive]]. [[Stab (Luftwaffe designation)|''Stab'']]./''NJG'' 3 under [[Helmut Lent]] was based at Stade, I./''NJG'' 3 under ''[[Hauptmann]]'' Walter Milius, based at [[Vechta]], II./''NJG'' 3 under [[Egmont Prinz zur Lippe-Weißenfeld]], at Schleswig, III./''NJG'' 3 under ''Major'' Bart, also at Stade, and IV./''NJG'' 3 under ''Major'' Simon at [[Grove, Germany|Grove]], prepared to the meet the offensive. II./''NJG'' 3, to which Raht's most recent unit, 4 ''staffel'', belonged was equipped with an assortment of [[Junkers Ju 88]]C, [[Messerschmitt Bf 110]]G and [[Dornier Do 217]] night fighters.{{sfn|Foreman|Parry|Matthews|2004|p=127}}

Raht's 16th and first success in the defence of [[Berlin]] occurred in the early evening of the 1 December 1943, when he claimed a Stirling shot down over the [[Frisian Islands]] at 15:54. It was one of only two claims submitted by a German night fighter pilot on this night. The other was reported shot down by a ''[[Feldwebel]]'' Weilding north of [[Aschaffenburg]] at 21:15.{{sfn|Foreman|Parry|Matthews|2004|p=130}} Bomber Command sent 19 Stirling and 12 Halifax aircraft to the Frisians and to the east coast of Denmark on mine-laying operations. Two Stirlings were reported missing.{{sfn|Everitt|Middlebrook|2014|p=240}} Stirling III ''EH880'' of [[No. 75 Squadron RAF]], piloted by Warrant Officer G. J. S. Kerr was one of the missing aircraft. All the crew perished when it crashed after reaching base. It hit a house at the end of the airfield and five children were killed.{{sfn|Chorley|1996|p=408}} Stirling III, ''EF191'', WP-H, of [[No. 90 Squadron RAF]] also crashed near [[Esjberg]] in Denmark. Sergeant J. L. Blackwood, E. Draper, J. H. Flack, Warrant Officer J. E. Nixon, Flight Sergeant C. E. Quickfall, Sergeants H. E. Steele and R. G. Whitmarsh were killed.{{sfn|Chorley|1996|p=408}} 

On 28/29 January 1944 Raht scrambled with 4 ''staffel'' and shot down a Halifax and Lancaster at 02:34 and 02:37. The location of these successes are not recorded.{{sfn|Foreman|Parry|Matthews|2004|p=144}} Seventeen days later accounted for victory 19 northeast of "Hallersleben" (possibly [[Fallersleben]]) at 20:03.{{sfn|Foreman|Parry|Matthews|2004|p=147}} On this date, Raht was awarded the [[German Cross in Gold]].{{sfn|Patzwall|Scherzer|2001|p=364}} Raht reached the 20-mark on 24/25 February 1944 when he shot down a heavy bomber at 22:02 between [[Metz]] and [[Baden Baden]].{{sfn|Foreman|Parry|Matthews|2004|p=151}}

Bomber Command switched their effort to [[Augsburg]] on 25/26 February 1944. Harris committed 594 bombers, including 461 Lancasters and 123 Halifax bombers supported by 10 Mosquitos. South of Augsburg he accounted for his 21st victory at 22:43.{{sfn|Foreman|Parry|Matthews|2004|p=152}} On 15/16 March Raht gained a 22nd victory on 23:12 southwest of [[Stuttgart]].{{sfn|Foreman|Parry|Matthews|2004|p=152}} On the night of the 22/23 March Raht achieved two victories at 21:28 near Hanover and two north and northwest of [[Frankfurt]] at 21:48 and 21:54 to reach 25. Raht scored again at 22:14, his fourth bomber of the night.{{sfn|Foreman|Parry|Matthews|2004|pp=156–157}} North of Berlin scored again on 24/25 March at 22:28 as the Berlin offensive came to an end.{{sfn|Foreman|Parry|Matthews|2004|p=158}}

On the night of the 30/31 March 1944, Bomber Command suffered heavy losses on a raid to [[Nürnberg]]. The British dispatched 795 aircraft, including 572 Lancasters, 214 Halifaxes and nine Mosquitos. A further 49 Halifax aircraft were sent on minelaying operations in the [[Heligoland]] area, 13 Mosquito night fighters were sent to German night-fighter airfields, 34 Mosquitos flew on diversions to [[Aachen]], [[Cologne]] and [[Kassel]]. 95 bombers were lost: 64 Lancasters and 31 Halifaxes which amounted to 11.9 per cent of the force. It was the largest Bomber Command loss of the war.{{sfn|Everitt|Middlebrook|2014|p=278}} Some German night fighter pilots scored heavily this night but ''Hauptmann'' Raht accounted for just a single claim—a Lancaster—southwest of [[Bonn]] at 00:26.{{sfn|Foreman|Parry|Matthews|2004|p=160}}

===Pointblank and Normandy===
In April 1944 Bomber Command turned to support the American [[United States Army Air Force]] (USAAF) [[Pointblank directive]] with greater regularity. Against the wishes of Harris, Bomber Command now turned to attacking rail yards, bridges and communications to facilitate [[Operation Overlord]] and the [[D-Day landings]] when they occurred. II./''NJG'' 3 was based at Vechta on 1 April 1944, at the outset of the offensive.{{sfn|Foreman|Parry|Matthews|2004|p=163}} On night of the 22/23 April 1944 Bomber Command attacked targets throughout Germany. Raht caught a Halifax south west of [[Düsseldorf]] at 01:23 and another northwest of the city at 01:44 for his 30th victory.{{sfn|Foreman|Parry|Matthews|2004|p=163}} One of Raht's victims was Avro Lancaster I ''DV394'', KC-M, from the famous [[No. 617 Squadron RAF]] "Dambusters". [[Flight Lieutenant]] J. L. Cooper [[Distinguished Flying Cross (United Kingdom)|DFC]] was taken prisoner with all but one of crew—J. H. C. A Lepine [[Distinguished Flying Medal|DFM]] ([[RCAF]] seconded) was the only member of the crew killed.{{sfn|Chorley|1997|p=190}} 24/25 April Raht downed a Lancaster {{convert|30 to 40|km|mi|abbr=off}} south of [[Echterdingen]].{{sfn|Foreman|Parry|Matthews|2004|p=170}} The next night over [[Schweinfurt]] at 02:15 Raht claimed another Lancaster.{{sfn|Foreman|Parry|Matthews|2004|p=170}} On 1 May 1944 Raht was promoted to ''[[Hauptmann]]''.{{sfn|Stockert|2011|p=85}}

Raht did not score again until the Allied invasion had begun on 6 June 1944. The month he transferred to [[Nachtjagdgeschwader 2|''Nachtjagdgeschwader'' 2]] (''NJG'' 2) as ''[[Gruppenkommandeur]]'' of I./''NJG'' 2 in France.{{sfn|Stockert|2011|p=85}} Raht's combat claim for the night of the 11/12 June 1944 simply stated he shot down a four-engine bomber over the French coast at 00:53.{{sfn|Foreman|Parry|Matthews|2004|p=186}} Another unidentified bomber was claimed southeast of [[Compiegne]] at 01:01 on 23 June. His 34th victory was one of 11 claims made by German night fighter crews on this night.{{sfn|Foreman|Parry|Matthews|2004|p=192}} The main thrust of Bomber Command operations this night had been inland. 221 aircraft—111 Lancaster and 100 Halifax bombers, supported by 10 Mosquitos from No. 1, 4 and 8 Groups attacked railway yards at [[Laon]] and [[Rheims]]. Four Halifax were lost from the Laon raid and four Lancaster bombers from the Rheims raid were reported missing. Minelaying sorties around French ports did not incur losses from German defences.{{sfn|Everitt|Middlebrook|2014|p=333}} Raht was awarded the [[Knight's Cross of the Iron Cross]] ({{lang|de|''Ritterkreuz des Eisernen Kreuzes''}}) on 24 June 1944 for 34 night victories.{{sfn|Fellgiebel|2000|p=348}}

A Lancaster over [[Bailleau-le-Pin]] at 01:44 on 1 July and another three at 01:04, 01:11 and sector "TD-TE" at 01:29 on 8 July brought his tall to 38. Another double claim at 02:07 and 02:17 over [[Coulommiers, Seine-et-Marne|Coulommiers]] and [[Gien]] took him to 40.{{sfn|Foreman|Parry|Matthews|2004|pp=195, 198, 199, 202}} On the night of the 28/29 July 1944 494 Lancasters and two Mosquitos of No. 1, 3, 5 and 8 Groups attacked Stuttgart. German fighters intercepted the bomber stream while over France on the outward flight; there was a [[full moon]] and 39 Lancaster bombers were shot down, 19 per cent of the force. 307 aircraft (187 Halifax, 106 Lancaster, 14 Mosquitos from No. 1, 6 and 8 Groups) raided Hamburg. German fighters again appeared, this time on the homeward flight, and 18 Halifax and four Lancaster bombers were lost, 12 per cent of the force. The bombing was judged ineffective in the later attack. 119 aircraft of No. 1, 4 and 8 Groups also attacked the flying bomb stores area at Forêt De Nieppe without loss. 95 training aircraft flew on a diversionary sweep over the North Sea, and 13 Mosquitos flew to Frankfurt, 50 Mosquito mounted night fighter patrols while five Halifax bombers carried out minelaying in the [[River Elbe]] without loss. The conditions and size of the attack saw the Luftwaffe mount a considerable response. This night German night fighters claimed 87 bombers shot down.{{sfn|Foreman|Parry|Matthews|2004|p=205}} Raht claimed a bomber shot down his 41st victory over France at 00:45.{{sfn|Foreman|Parry|Matthews|2004|p=205}} On 25/26 August 1944 Raht downed his last of the campaign at 01:38 at an unknown location.{{sfn|Foreman|Parry|Matthews|2004|p=211}}

===Defence of the Reich===
In southern Germany, Raht commanded the ''Gruppe'' against night intrusions. On 2/3 1945 he accounted for a Halifax and on 3/4 February a Lancaster. Southwest of [[Mannheim]], he claimed a Halifax at 23:49 on the first night and on the second a Lancaster at 20:00 over an unreported location.{{sfn|Foreman|Parry|Matthews|2004|p=234}} On the night of 7/8 February 1945, Raht became an "[[Aviators who became ace in a day|ace-in-a-day]]" after he claimed six four-engined bombers of unknown type. His victories were claimed at 22:22, at 22:41, at 23:00, at 00:08, at 00:16 and at 00:23 respectively: his 46th to 51st victories.{{sfn|Foreman|Parry|Matthews|2004|p=234}} This achievement was mentioned in the addendum of the ''[[Wehrmachtbericht]]'', a [[propaganda]] radio report, on 10 February 1945.{{sfn|''Die Wehrmachtberichte 1939–1945 Band 3''|p=575}}

On the night of the 3/4 March 1945 Raht took part in [[Operation Gisela|Operation ''Gisela'']], an intruder mission over England. At 23:00 Raht took off leading I./''NJG'' 2 from their base in [[Twente]] to intercept a homeward bound Bomber Command raid. Over England, at 01:04 and 01:26 he claimed two four-engine bombers.{{sfn|Foreman|Parry|Matthews|2004|p=240}} According to [[Heinz Rökker]], one of Raht's pilots in the group, they flew the [[Junkers Ju 88]]G-6 on the mission. The Ju 88s were loaded with bombs and crews were ordered to attack the airfields as well as attack bombers. The group was ordered to fly to the [[Grimsby]]-[[Lincoln, England|Lincoln]] area. Rökker was not successful and stated that he believed Raht to be the only successful pilot of the group that night.{{sfn|Boiten|1997|pp=54–55}}

On 15/16 March 1945 Bomber Command sent a force of 267 aircraft(134 Lancaster and 122 Halifax bombers plus 11 Mosquitos) of 4, 6 and 8 Groups to bomb [[Hagen]]. Six Lancaster and four Halifax bombers were lost lost. This area attack took place in clear visibility and caused severe damage. The main attack fell in the centre and eastern districts. Another 257 Lancasters and eight Mosquitos of 1 and 8 Groups attacked the refinery at [[Misburg]], on the outskirts of Hannover. Four Lancasters were lost. Ten aircraft—seven four-engine bombers, a [[B-25 Mitchell]], a Mosquito and a Lancaster were claimed shot down by German night fighter pilots this night. Rökker claimed two heavy bombers, the B-25 and Mosquito. [[Wilhelm Johnen]] scored his 34th victory against a Lancaster. Raht claimed the other five between 20:49 and 21:20. The locations for each claim are not recorded. Raht had claimed his 54th to 58th, and last victories, of the war.{{sfn|Foreman|Parry|Matthews|2004|p=242}}

Raht remained in command of I./''NJG'' 2 until the end of the war. On 15 April 1945 Raht became the 833rd Oak Leaves as ''[[Hauptmann]]'' and ''[[Gruppenkommandeur]]'' of the I./NJG 2. He was taken prisoner when the German forces surrendered on 8/9 May 1945. He joined the [[German Air Force]] as a reservist and died on 11 January 1977.

==Awards==
* [[Iron Cross]] (1939)
** 2nd Class (3 April 1942){{sfn|Thomas|1998|p=180}}
** 1st Class  (13 March 1943){{sfn|Thomas|1998|p=180}}
* [[Honour Goblet of the Luftwaffe]] (''Ehrenpokal der Luftwaffe'') on 6 September 1943 as ''[[Oberleutnant]]'' and pilot{{sfn|Patzwall|2008|p=121}}{{refn|According to Stockert on 30 August 1943.{{sfn|Stockert|2011|p=85}}|group="Note"}}
* [[German Cross]] in Gold on 28 January 1944 as ''[[Oberleutnant]]'' in the II./NJG 3{{sfn|Patzwall|Scherzer|2001|p=364}}
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 24 June 1944 as ''Oberleutnant'' and ''[[Staffelkapitän]]'' of the 1./NJG 2{{sfn|Fellgiebel|2000|p=348}}{{refn|According to Scherzer as ''Staffelkapitän'' of the 4./NJG 3.{{sfn|Scherzer|2007|p=611}}|group="Note"}}
** 833rd Oak Leaves on 15 April 1945 as ''[[Hauptmann]]'' and ''[[Gruppenkommandeur]]'' of the I./NJG 2{{sfn|Scherzer|2007|p=611}}{{sfn|Fellgiebel|2000|p=102}}
* Mentioned in the addendum of the ''[[Wehrmachtbericht]]'' on 10 February 1945{{sfn|''Die Wehrmachtberichte 1939–1945 Band 3''|p=575}}

==Notes==
{{reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Boiten
  |first=Theo
  |year=1997
  |title=Nachtjagd: the night fighter versus bomber war over the Third Reich, 1939–45
  |publisher=Crowood Press
  |location=London
  |isbn=978-1-86126-086-4
  |ref=harv
}}
* {{Cite book
  |last=Bowman
  |first=Martin
  |year=2016
  |title=Nachtjagd, Defenders of the Reich 1940–1943
  |location=Barnsley, South Yorkshire
  |publisher=[[Pen and Sword Books]]
  |isbn=978-1-4738-4986-0
  |ref=harv
}}
* {{Cite book
  |last=Chorley
  |first=W. R
  |year=1996
  |title=Royal Air Force Bomber Command Losses of the Second World War: Aircraft and crew losses: 1943
  |trans_title=
  |language=English
  |location=
  |publisher= Midland Counties Publications
  |isbn= 978-0-9045-9791-2
  |ref=harv
}}
* {{Cite book
  |last=Chorley
  |first=W. R
  |year=1997
  |title=Royal Air Force Bomber Command Losses of the Second World War: Aircraft and crew losses: 1944
  |trans_title=
  |language=English
  |location=
  |publisher= Midland Counties Publications
  |isbn= 978-0-9045-9791-2
  |ref=harv
}}
* {{Cite book
  |last1=Cooper
  |first1=Alan. W
  |authorlink1=
  |year=2013
  |title=Bombers over Berlin: The RAF Offensive November 1943 - March 1944
  |publisher=Pen & Sword
  |isbn= 978-1-78159-065-2
  |ref=harv
}}
* {{Cite book
  |last1=Everitt
  |first1=Chris
  |last2=Middlebrook
  |first2=Martin
  |authorlink2=Martin Middlebrook
  |year=2014
  |origyear=1985
  |title=The Bomber Command War Diaries: An Operational Reference Book
  |publisher=Pen & Sword
  |isbn=978-1-78346360-2
  |ref=harv
}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
  |ref=harv
}}
* {{Cite book
  |last1=Foreman
  |first1=John
  |last2=Parry
  |first2=Simon
  |last3=Matthews
  |first3=Johannes
  |year=2004
  |title=Luftwaffe Night Fighter Claims 1939–1945
  |trans_title=
  |language=English
  |location=Walton on Thames
  |publisher=Red Kite
  |isbn=978-0-9538061-4-0
  |ref=harv
}}
* {{Cite book
  |last1=Middlebrook
  |first1=Martin
  |authorlink1=Martin Middlebrook
  |year=2014
  |origyear=2006
  |title=The Peenemunde Raid: The Night of 17-18 August 1943
  |publisher=Pen & Sword
  |isbn=978-1-84415-336-7
  |ref=harv
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last=Scutts
  |first=Jerry
  |year=1998
  |title=German Night Fighter Aces of World War 2
  |trans_title=
  |language=English
  |location=Oxford
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-85532-696-5
  |ref=harv
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Stockert
  |first=Peter
  |year=2011
  |title=Die Eichenlaubträger 1939–1945 Band 9
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 9
  |language=German
  |location=Bad Friedrichshall, Germany
  |publisher=Friedrichshaller Rundblick
  |oclc=76072662
  |ref=harv
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1998
  |title=Die Eichenlaubträger 1939–1945 Band 2: L–Z
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 2: L–Z
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2300-9
  |ref=harv
}}
* {{Cite book
  |year=1985
  |title=Die Wehrmachtberichte 1939–1945 Band 3, 1. Januar 1944 bis 9. Mai 1945
  |trans_title=The Wehrmacht Reports 1939–1945 Volume 3, 1 January 1944 to 9 May 1945
  |language=German
  |location=München, Germany
  |publisher=Deutscher Taschenbuch Verlag GmbH & Co. KG
  |isbn=978-3-423-05944-2
  |ref={{sfnRef|''Die Wehrmachtberichte 1939–1945 Band 3''}}
}}
{{Refend}}

{{Knight's Cross recipients of NJG 2}}
{{Top German World War II night fighter aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Raht, Gerhard}}
[[Category:1920 births]]
[[Category:1977 deaths]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]