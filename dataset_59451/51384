{{Multiple issues|
{{disputed|date=November 2012}}
{{update|date=November 2012}}
}}

{{Infobox Journal
| title 	= 	General systems: Yearbook of the Society for the Advancement of General Systems Theory
| editor 	= 	[[Anatol Rapoport]] (1956- ), [[Ludwig von Bertalanffy]] (1956- ), [[Richard L. Meier]] (1961)
| discipline 	= 	[[General System Theory]], [[Systems Science]]
| language 	= 	[[English language|English]]
| abbreviation 	= 	Gen Syst (via [[United States National Library of Medicine|NLM]])
| publisher 	= 	[[Society for General Systems Research]]
| country 	= 	USA: Washington, D.C., 1968-1976; Louisville, Ky., 1977- ; New York, NY, USA -1988; Louisville, Ky., 1989-
| frequency 	= 	Annual
| history 	= 	1956-1987 (vol. 1-32) annual, after 1987 as a special issue of [[Systems Research and Behavioral Science]], (part 5, Sept./Oct.)
| website 	= 	http://www3.interscience.wiley.com/journal/117946258/issueyeargroup?year=2008
| OCLC 	        = 1429672
| ISSN 	        = 0072-0798
| LCCN          = 62000913
| ISSN2         = 0097-336X
| boxwidth 	= 30em
}}
'''''General Systems: Yearbook of the Society for General Systems Research''''', known as '''''General Systems''''', is the first annual journal in the field of [[systems science]] initiated in 1956, and initially edited by [[Ludwig von Bertalanffy]] and [[Anatol Rapoport]].

Since 1998, it has been published as issue 5 of [[Systems Research and Behavioral Science]].

== Overview ==
General Systems has been the first journal of the [[Society for General Systems Research]] published independently until the 1980s. Ever since it has been published as one of the items of the [[Systems Research and Behavioral Science]]. The journal started as a selecting of publications by several of the "foundational authors of the [[systems sciences]]",<ref>[http://pespmc1.vub.ac.be/journals.html Cybernetics and Systems Journals], retrieved 28 May 2008.</ref> and contains some of the classic works in the field of systems theory,<ref>Benjamin Frankel (1996), ''Roots of Realism''. pp 53.</ref> such as:
* [[Ludwig von Bertalanffy]], "General Systems Theory," in ''General Systems Yearbook'' 1 (1956), 
* [[Kenneth Boulding]], "General Systems Theory -- The Skeleton of Science," General Systems Yearbook, I (1956), pp.&nbsp;11–17. 
* [[W. Ross Ashby]], "General systems theory as a new discipline," General Systems Yearbook, 3, (1958).
* [[Charles A. McClelland]], "Systems and History in International Relations," General Systems Yearbook, III (1958).

The General Systems Yearbook also contains examples of the third kind of general systems activity — creating new laws and refining old.<ref>Gerald M. Weinberg (1975), ''An Introduction to General Systems Thinking''. John Wiley, p. 46.</ref>

In 1998, the ''General Systems Yearbook'' was transitioned<ref>Lane Tracy1,  Tracy, L. and [[Jennifer Wilby|Wilby, J.]] (1998), Introduction to the special issue. Syst. Res., 15: 357–358. doi: 10.1002/(SICI)1099-1743(1998090)15:5<357::AID-SRES263>3.0.CO;2-F</ref> to be included each year as issue 5 of ''Systems Research and Behavioral Sciences''.

== See also ==
* [[List of journals in systems science]]

== References ==
{{reflist}}

== External links ==
* [http://isss.org/projects/general_system_yearbook GENERAL SYSTEM YEARBOOK Volume I - 1956] content.
* [http://isss.org/projects/general_system_yearbook_volume_ii_-_1957 GENERAL SYSTEMS YEARBOOK Volume II - 1957] content.

[[Category:Publications established in 1956]]
[[Category:Systems journals]]