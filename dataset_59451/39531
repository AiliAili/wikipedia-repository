{{For|the song|The Bear and the Maiden Fair (song)}}
{{Use mdy dates|date=July 2013}}
{{Infobox television episode
 |image_size = 280px
 |image = Game of Thrones-S03-E07 Brienne and the bear.jpg
 |caption = [[Brienne of Tarth]] is forced to fight a bear.
 |title = {{nowrap|The Bear and the Maiden Fair}}
 |series = [[Game of Thrones]]
 |season = 3
 |episode = 7
 |director = [[Michelle MacLaren]]
 |writer = [[George R. R. Martin]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = Chris Seager
 |editor = Frances Parker
 |production = 
 |airdate = {{Start date|2013|5|12}}
 |length = 57 minutes<!-- 57 minutes 38 seconds -->
 |guests =*[[Mackenzie Crook]] as Orell
* [[Gwendoline Christie]] as Brienne of Tarth
* [[Richard Dormer]] as Beric Dondarrion
* [[Paul Kaye]] as Thoros of Myr
* [[Noah Taylor]] as Locke
* [[Natalia Tena]] as Osha
* [[Anton Lesser]] as Qyburn
* [[Michael McElhatton]] as Roose Bolton
* [[Clive Russell]] as Brynden Tully
* [[Tobias Menzies]] as Edmure Tully
* [[Ian McElhinney]] as Barristan Selmy
* [[Iwan Rheon]] as Boy
* [[Kristofer Hivju]] as Tormund Giantsbane
* [[Thomas Sangster|Thomas Brodie Sangster]] as Jojen Reed
* [[Ellie Kendrick]] as Meera Reed
* [[Nathalie Emmanuel]] as Missandei
* [[Jacob Anderson]] as Grey Worm
* [[Kristian Nairn]] as Hodor
* [[Philip McGinley]] as Anguy
* {{Interlanguage link multi|George Georgiou (actor){{!}}George Georgiou|de|3=George Georgiou}} as Razdal mo Eraz
* Jamie Michie as Steelshanks
* [[Charlotte Hope]] as Myranda
* Stephanie Blacker as Violet
* [[Art Parkinson]] as Rickon Stark
 |season_list =
 |prev = [[The Climb (Game of Thrones)|The Climb]]
 |next = [[Second Sons]]
 |episode_list = [[Game of Thrones (season 3)|''Game of Thrones'' (season 3)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}
"'''The Bear and the Maiden Fair'''" is the seventh episode of the [[Game of Thrones (season 3)|third season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 27th episode of the series overall. The episode was written by [[George R. R. Martin]], the author of the ''[[A Song of Ice and Fire]]'' novels on which the series is based, and was directed by [[Michelle MacLaren]], her directorial debut for the series.

The plot of the episode advances the storylines of Daenerys' arrival to the city of Yunkai, the repercussions of the upcoming marriage of Tyrion Lannister and Sansa Stark, and Brienne's fate at the ruined castle of Harrenhal.

==Plot==

===In King's Landing===
Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]) is comforted by Lady Margaery Tyrell ([[Natalie Dormer]]), who has heard of Sansa's betrothal to Tyrion Lannister ([[Peter Dinklage]]). Elsewhere, Tyrion and Bronn ([[Jerome Flynn]]) are also discussing the match and how it will affect Shae ([[Sibel Kekilli]]). Tywin Lannister ([[Charles Dance]]) meets his grandson, King Joffrey Baratheon ([[Jack Gleeson]]), and teaches him a lesson in ruling; Joffrey asks what they should do about the rumors of Daenerys Targaryen and her dragons, but Tywin claims there is no threat. When they meet later, Shae tells Tyrion that she will not continue their relationship once he marries Sansa.

On Blackwater Bay, Melisandre ([[Carice van Houten]]) reveals to Gendry ([[Joe Dempsie]]) that his blood is noble and Gendry says "I'm just a bastard" and Melisandre then says "the bastard of Robert of the house Baratheon" revealing that his father was the late King Robert Baratheon.

===In the North===
Jon Snow ([[Kit Harington]]) and the wildling party led by Tormund Giantsbane ([[Kristofer Hivju]]) continue their journey south. Ygritte ([[Rose Leslie]]) stops with Jon to discuss their next move. Soon after, Orell ([[Mackenzie Crook]]) offers some harsh wisdom to Jon, and later speaks to Ygritte, confessing his love for her and trying to convince her that Jon is still loyal to the Night's Watch. Later, Jon tells Ygritte that the wildling cause is hopeless, but she remains undaunted.

Theon Greyjoy ([[Alfie Allen]]) is freed from his constraints by two young women, who give him water and clean his wounds. Theon is apprehensive about their aid, until they disrobe and begin pleasuring him. The three are soon interrupted by his tormenter ([[Iwan Rheon]]), who had ordered the women to seduce Theon in order to torment him further. The boy draws a knife and mocks Theon's sexual prowess, before ordering his men to restrain Theon as he removes Theon's genitals.

Heading for the Wall, Bran ([[Isaac Hempstead-Wright]]) speaks to Jojen Reed ([[Thomas Sangster|Thomas Brodie Sangster]]) while Osha ([[Natalia Tena]]) and Hodor ([[Kristian Nairn]]) set up camp. Osha continues to grow suspicious of the Reeds, calling Jojen's visions "black magic". When she says they have to continue to the Wall, Jojen reveals that the three-eyed raven is north of the Wall, and that is their destination. Osha refuses to allow them to go north of the Wall, relating to them the story of her husband's death and resurrection as a wight.

===In the Riverlands===
King Robb Stark ([[Richard Madden]]), his advisors, and his army are delayed by rain in their march toward the Twins for Edmure Tully's ([[Tobias Menzies]]) wedding to Roslin Frey. Catelyn Stark ([[Michelle Fairley]]) and Brynden Tully ([[Clive Russell]]) discuss their distaste for Walder Frey, who will see their delay and Robb's oath-breaking as slights against his family. When the Tullys depart, Queen Talisa Stark ([[Oona Chaplin]]) reveals to Robb that she is pregnant.

At the Brotherhood's hideout, Arya Stark ([[Maisie Williams]]) berates Beric Dondarrion ([[Richard Dormer]]) and Thoros of Myr ([[Paul Kaye]]) for selling Gendry to Melisandre. When Anguy ([[Philip McGinley]]) tells Beric of a Lannister raiding party near them, Beric orders the men to move out in pursuit. Arya calls Beric a liar, as he promised to take her to Riverrun but is instead holding her hostage. She runs away, and is pursued by several of the Brotherhood soldiers, but before they can locate her, she is taken captive by Sandor "The Hound" Clegane ([[Rory McCann]]).

===Outside Yunkai===
Daenerys ([[Emilia Clarke]]), her knights Ser Jorah Mormont ([[Iain Glen]]) and Ser Barristan Selmy ([[Ian McElhinney]]), and her army of Unsullied reach the yellow city of Yunkai. Though she is advised that Yunkai holds no strategic value, Daenerys tells Jorah she will take the city to free its 200,000 slaves. She orders Grey Worm ([[Jacob Anderson]]) to send a messenger to the city demanding their surrender, or she will sack the city. Razdal mo Eraz ({{Interlanguage link multi|George Georgiou (actor){{!}}George Georgiou|de|3=George Georgiou}}) is sent by Yunkai to offer terms of peace, which include chests filled with [[gold bar]]s and as many ships as Daenerys wants. Daenerys refuses his offer, demanding the slaves of the city be freed and paid for their service, which Razdal angrily refuses, and departs the tent.

===At Harrenhal===
Brienne of Tarth ([[Gwendoline Christie]]) is visited in her cell by Jaime Lannister ([[Nikolaj Coster-Waldau]]), who tells her that he will be departing for King's Landing the next day, and that she will be left at Harrenhal under the care of Locke ([[Noah Taylor]]), as Roose Bolton ([[Michael McElhatton]]) is departing for Edmure Tully's wedding at the Twins. Before leaving, Brienne makes Jaime swear to uphold his oath to Catelyn Stark and return the Stark girls to their mother. As he leaves, Jaime tells Roose to tell Robb Stark that he is sorry that he can't attend the wedding, but that "the Lannisters send their regards."

While on the road, Qyburn ([[Anton Lesser]]) checks on the condition of Jaime's right arm, and reveals that he lost his maester's chain for experimenting on living humans. When Qyburn informs Jaime that Brienne will not be ransomed by Locke, Jaime manipulates the party leader, Steelshanks (Jamie Michie), to order their return to Harrenhal. Upon their arrival, Jaime finds that Brienne has been thrown into a pit by Locke by his men and has been forced to defend herself from a [[bear]] with a wooden sword. Jaime tries to ransom Brienne, but is unsuccessful, and instead leaps into the pit to protect her. The bear is shot with a crossbow by Steelshanks but continues to attack, before Jaime boosts Brienne out of the pit, and is then lifted to safety. Confronted by Steelshanks and his men, who are ordered to ensure Jaime's safety, Locke relents and lets Jaime and Brienne depart for King's Landing.

==Production==

===Writing===
[[File:George R. R. Martin by Gage Skidmore 2.jpg|right|thumb|[[George R. R. Martin]], author of the ''[[A Song of Ice and Fire]]'' novels, scripts one episode per season.]]
The episode was written by [[George R. R. Martin]], author of the novels of the  ''[[A Song of Ice and Fire]]'' saga that the show adapts. "The Bear and the Maiden Fair" is based on material from the third book of his series, ''[[A Storm of Swords]]'', adapting chapters 42 to 46 (Jon V, Daenerys IV, Arya VIII, Jaime VI and Catelyn V).<ref name=Westeros.org>{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/The_Bear_and_the_Maiden_Fair/Book_Spoilers/#Book_to_Screen|title=EP307: The Bear and the Maiden Fair|last=Garcia|first=Elio|last2=Antonsson|first2=Linda|work=Westeros.org|date=May 14, 2013|accessdate=November 11, 2014}}</ref>

In some of the scenes, Martin had to take into account the changes done by the production to some of his original plots or characters, writing scenes that could never happen in the novels: the books have Talisa's counterpart stay in Riverrun instead of following Robb, Melisandre never interacts with Gendry, and Sansa does not get to confide with Margaery.<ref name=Westeros.org />

Martin initially titled the episode "Autumn Storms", because it was supposed to be raining in many of the scenes. When he was forced to change it because most of the rains had been cut from his script in pre-production, he came up with the title "Chains", that worked both in a literal and metaphorical level.<ref>{{Cite web|url=http://grrm.livejournal.com/307647.html|title=Title Change|work=Not a blog|first=George R.R.|last=Martin|date=January 10, 2013|accessdate=May 6, 2013}}</ref> However, later on, the final scene including the bear that had been originally written by showrunners [[David Benioff|Benioff]] and [[D. B. Weiss|Weiss]] for the next episode was incorporated, and the episode was given its final title.<ref>{{Cite web|url=
http://grrm.livejournal.com/314444.html|title=UnChained|work=Not a blog|first=George R.R.|last=Martin|date=February 26, 2013|accessdate=May 6, 2013}}</ref>

===Casting===
To play the part of the bear at Harrenhal, the producers chose the nearly nine-foot-tall [[Alaska]]n [[brown bear]] [[Bart the Bear 2]] (a.k.a. "Little Bart"), who was born in 2000 and trained by Doug and Lynne Seus (the same trainers of his well-known predecessor, the original [[Bart the Bear]]).<ref>{{Cite web|url=http://grrm.livejournal.com/310379.html?thread=17206891#t17206891|title=It's the Pits|work=Not a blog|first=George R.R.|last=Martin|date=January 21, 2013|accessdate=May 6, 2013}}</ref>

===Filming locations===
[[File:Aït Benhaddou2 (js).jpg|thumb|The episode introduces Yunkai, based on the Moroccan city of [[Aït Benhaddou]].]]
The production continued to use Morocco to depict the Slaver's Bay. While the coastal city of [[Essaouira]] had doubled as Astapor, this episode used the city of [[Aït Benhaddou]] (near [[Ouarzazate]]) to depict Yunkai. Daenerys' camp was built in the nearby location of Little Barrage.

The scenes with Jon Snow and the wildlings were filmed in the forests around [[Toome]], in [[County Antrim]], [[Northern Ireland]]. The scenes in Northern Ireland were filmed six weeks before the production moved to Iceland to film several scenes for the previous episodes.<ref>{{cite web|url=http://www.makinggameofthrones.com/production-diary/2013/5/29/leeches-dragons-and-a-bear-behind-the-scenes-of-episodes-307.html|title=Leeches, Dragons and a Bear: Behind the Scenes of Episodes 307 and 308|last=Taylir|first=Cat|work=[[Entertainment Weekly]]|date=May 29, 2013|accessdate=May 31, 2013}}</ref>

Due to the legal restrictions and the difficulties involved in the transport of large animals, the scenes with the bear Little Bart had to be filmed in the USA. Although it was only used for a single scene, this was the fifth country where the production filmed during the season (after Northern Ireland, Morocco, Croatia, and Iceland).<ref>{{cite web|url=http://insidetv.ew.com/2013/01/28/game-of-thrones-country-bear/|title='Game of Thrones' add 5th country, casts bear|last=Hibberd|first=James|work=[[Entertainment Weekly]]|date=January 28, 2013|accessdate=May 6, 2013}}</ref>  The actual bear-pit set was built in Northern Ireland:  the bear was filmed where it was living in Los Angeles, interacting with its trainer, and was later digitally added into the footage from the bear pit set in Northern Ireland.

==Reception==

===Ratings===
4.84 million viewers watched the premiere airing of "The Bear and the Maiden Fair", a decrease of 0.67 million compared to the previous week. This ended the streak set during the four previous episodes, each of which established a new series highs in ratings. 1.12 million people watched the second airing, bringing the total viewership of the night to 5.96 million.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2013/05/14/sunday-cable-ratings-game-of-thrones-wins-night-breaking-amish-mad-men-long-island-medium-river-monsters-more/182590/|title=Sunday Cable Ratings: 'Game of Thrones' Wins Night + 'Breaking Amish', 'Mad Men', 'Long Island Medium', 'River Monsters' & More|publisher=TV by the Numbers|first=Amanda|last=Kondolojy|date=May 14, 2013|accessdate=May 14, 2013}}</ref> In the United Kingdom, the episode was seen by 1.023 million viewers on [[Sky Atlantic]], being the channel's highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (13 - 19 May 2013)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
The critical reception to the episode was generally favorable, although most commentators agreed that it was not among the best episodes of the third season, or the ones written by Martin. [[Review aggregator]] [[Rotten Tomatoes]] surveyed 21 reviews of the episode and judged 81% of them to be positive. The website's critical consensus reads, "'The Bear and the Maiden Fair" feels like a bit of a holding pattern as Game of Thrones moves its pieces into place for the final three episodes."<ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s03/e07/|title=The Bear and the Maiden Fair|work=[[Rotten Tomatoes]]|accessdate=May 3, 2016}}</ref> The quality of the dialogue and characterization was widely praised. ''[[The A.V. Club]]''{{'}}s David Sims found that the interactions felt more natural,<ref name="av1">{{cite web|url=http://www.avclub.com/articles/the-bear-and-the-maiden-fair-for-newbies,97160/|title="The Bear and the Maiden Fair" (for newbies)|last=Sims|first=David|publisher=[[The A.V. Club]]|date=May 12, 2013|accessdate=May 13, 2013}}</ref> and Elio Garcia from [[Westeros.org]] suggested that the characters "oozed a richer version of themselves".<ref name="westeros">{{cite web|url=
http://www.westeros.org/GoT/Episodes/Entry/The_Bear_and_the_Maiden_Fair/Book_Spoilers/|title=EP307: The Bear and the Maiden Fair|publisher=Westeros.org|first=Elio|last=Garcia|accessdate=May 15, 2013}}</ref> Another aspect that was mentioned as an improvement was the treatment of the romantic relationships.
{{quote box
|width = 30em
|border = 1px
|align=left
|bgcolor=#c6dbf7
|fontsize = 85%
|quote = "'The Bear and the Maiden Fair' is much more of a piece-mover episode, getting characters from Point A to Point B either on the map (the wildlings marching towards Castle Black, Melisandre and Gendry sailing back towards Dragonstone) or on their emotional journey (Sansa and Tyrion both struggling to make peace with their impending nuptials). It's one of the more thorough of its kind, touching on nearly every plot this season has set in motion. And because it's directed by Michelle MacLaren (...) and written by George R.R. Martin himself, it's about as good (and good-looking) a piece-mover episode as you're going to see."
|salign = right
|source = — Alan Sepinwall, [[HitFix]]<ref name=hf>{{Cite web|url=http://www.hitfix.com/whats-alan-watching/review-game-of-thrones-the-bear-and-the-maiden-fair-come-blow-your-horn|title=Review: 'Game of Thrones' – 'The Bear and the Maiden Fair': Come blow your horn|work=[[HitFix]]|last=Sepinwall|first=Alan|accessdate=May 15, 2013|date=May 12, 2013}}</ref> 
}}
Many reviews signaled the lack of focus as the main flaw of the episode, although they agreed that the story required to prepare the stage for the final part of the season: Todd VanDerWerff wrote at ''The A.V. Club'' that it was a "somewhat disjointed hour, full of characters moving into place for what’s next (...) it nonetheless accomplishes what it sets out to do".<ref name="av2">{{cite web|url=http://www.avclub.com/articles/the-bear-and-the-maiden-fair-for-experts,97159/|title="The Bear and the Maiden Fair" (for experts)|last=VanDerWerff|first=Todd|publisher=[[The A.V. Club]]|date=May 12, 2013|accessdate=May 13, 2013}}</ref> According to Myles McNutt from Cultural Learnings, the episode "never evolves into a particularly exciting hour of television, content mostly to sketch out the boundaries of the season’s storylines in preparation for the oncoming climax."<ref name="cl">{{cite web|url=http://cultural-learnings.com/2013/05/12/game-of-thrones-the-bear-and-the-maiden-fair/|title=Game of Thrones – "The Bear and the Maiden Fair"|last=McNutt|first=Myles|publisher=Cultural Learnings|date=May 12, 2013|accessdate=May 15, 2013}}</ref>

The final scene was very well received: [[IGN]]'s Matt Fowler called it "a spectacular moment",<ref name="ign">{{cite web|url=http://ca.ign.com/articles/2013/05/13/game-of-thrones-the-bear-and-the-maiden-fair-review|title=Game of Thrones: "The Bear and the Maiden Fair" Review|last=Fowler|first=Matt|publisher=[[IGN]]|date=May 12, 2013|accessdate=May 13, 2013}}</ref> [[HitFix]]'s Alan Sepinwall deemed it "gorgeously staged and executed",<ref name="hf"/> and David Sims found it "tense, thrilling television".<ref name="av1"/> Other scenes that were highlighted were the parley between Daenerys and the slaver, and the confrontation between Tywin and Joffrey. In the latter, the camera work used by director Michelle MacLaren was lauded.

In contrast, the scene featuring Theon's torture was criticized for what was seen as its gratuitous violence and nudity, and for the repetitiveness of the storyline over the season. Sepinwall declared that he had no need "to witness more of The Passion of the Greyjoy",<ref name="hf"/> and Sims considered it "boring and confusing to watch".<ref name="av1"/> VanDerWerff concluded: "Endless torture sequences don’t make for terribly exciting fiction, and that’s more or less bearing out here."<ref name="av2"/> On the other end of the spectrum, the reviewer for ''[[Time (magazine)|Time]]'', [[James Poniewozik]], considered it "chilling".<ref>{{cite web|url=http://entertainment.time.com/2013/05/13/game-of-thrones-watch-dragons-and-eagles-and-bears-oh-my/|title=Game of Thrones Watch: Dragons and Eagles and Bears, Oh, My!|last=Poniewozik|first=James|work=[[Time (magazine)|Time]]|date=May 13, 2013|accessdate=May 15, 2013}}</ref>

== References ==
{{Reflist|2}}

== External links ==
{{wikiquotepar|Game_of_Thrones_(TV_series)#The_Bear_and_the_Maiden_Fair_.5B3.07.5D|The Bear and the Maiden Fair}}
* [http://www.hbo.com/game-of-thrones/episodes/3/27-the-bear-and-the-maiden-fair/index.html "The Bear and the Maiden Fair"] at [[HBO.com]]
* {{IMDb episode|2178814}}
* {{tv.com episode|game-of-thrones/the-bear-and-the-maiden-fair-2674248}}

{{Game of Thrones Episodes}}
{{George R. R. Martin}}

{{DEFAULTSORT:Bear and the Maiden Fair}}
[[Category:2013 American television episodes]]
[[Category:Game of Thrones episodes]]
[[Category:Screenplays by George R. R. Martin]]