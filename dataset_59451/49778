{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Rameau's Nephew, <br> or the Second Satire
| title_orig    = Le Neveu de Rameau <br> ou La Satire seconde
| translator    = 
| image         = File:DiderotRameauNephew.jpg<!--prefer 1st edition-->
| caption = 
| author        = [[Denis Diderot]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = 
| genre         = [[Philosophical novel]]
| publisher     = 
| release_date  = [[1805 in literature|1805]], [[1891 in literature|1891]]
| english_release_date =
| media_type    =
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}

'''''Rameau's Nephew, or the Second Satire''''' (or '''The Nephew of Rameau''', {{lang-fr|Le Neveu de Rameau ou La Satire seconde}}) is an imaginary philosophical conversation by [[Denis Diderot]], written predominantly in 1761-2 and revised in 1773-4.<ref name="jcb">{{cite book|last=Bonnet|first=Jean-Claude|title=Le Neveu de Rameau: Présentation|year=2013|page=8|publisher=Flammarion|isbn=9782081297142}}</ref>

It was first published in [[1805 in literature|1805]] in German translation by [[Johann Wolfgang von Goethe|Goethe]],<ref name="jcb"/> but the French manuscript used had subsequently disappeared. The German version was translated back into French by de Saur and Saint-Geniès and published in 1821. The first published version based on French manuscript appeared in 1823 in the [[Brière]] edition of Diderot's works. Modern editions are based on the complete manuscript in Diderot's own hand found by Georges Monval, the librarian at the [[Comédie-Française]] in 1890, while buying music scores from a second-hand bookshop in Paris.<ref>[http://www.litencyc.com/php/sworks.php?rec=true&UID=4074 The Literary Encyclopedia]</ref> Monval published his edition of the manuscript in [[1891 in literature|1891]]. Subsequently, the manuscript was bought by the [[Pierpont Morgan Library]] in New York. It is unclear why Diderot never had it published in his time. Given the satirical tone of the work, it has been suggested that the author prudently refrained from giving offence.

==Description==
The recounted story takes place in the [[café de la Regence]], where '''Moi''' ("Me"), a narrator-like persona (often mistakenly supposed to stand for Diderot himself), describes for the reader a recent encounter he's had with the character '''Lui''' ("Him"), referring to — yet not literally meaning — Jean-François Rameau, the nephew of the [[Jean-Philippe Rameau|famous composer]], who's engaged him in an intricate battle of wits, self-reflexivity, allegory and allusion.

Recurring themes in the discussion include the [[Querelle des Bouffons]] (the French/Italian opera battle), education of children, the nature of genius and money. The often rambling conversation pokes fun at numerous prominent figures of the time.

In the prologue that precedes the conversation, the first-person narrator frames Lui as eccentric and extravagant, full of contradictions, "a mixture of the sublime and the base, of good sense and irrationality". Effectively being a provocateur, Lui seemingly extols the virtues of crime and theft, raising love of gold to the level of a religion. Moi appears initially to have a didactic role, while the nephew (Lui) succeeds in conveying a cynical, if perhaps immoral, vision of reality.

Michel Foucault, in his ''Madness and Civilization'', saw in the ridiculous figure of Rameau's nephew a kind of exemplar of a uniquely modern incarnation of the Buffoon.

==Summary==

===Preface===
The narrator has made his way to his usual haunt on a rainy day, the ''[[Café de la Régence]]'', France's chess mecca, where he enjoys watching such masters as [[François-André Danican Philidor|Philidor]] or [[Legall de Kermeur|Legall]]. He is accosted by an eccentric figure: ''I do not esteem such originals. Others make them their familiars, even their friends. Such a man will draw my attention perhaps once a year when I meet him because his character offers a sharp contrast with the usual run of men, and a break from the dull routine imposed by one's education, social conventions and manners. When in company, he works as a pinch of leaven, causing fermentation and restoring each to his natural bend. One feels shaken and moved; prompted to approve or blame; he causes truth to shine forth, good men to stand out, villains to unmask. Then will the wise man listen and get to know those about him.''<ref name="ram">[http://abu.cnam.fr/cgi-bin/go?neveu2 Translated from ''Rameau's Nephew'']</ref>

===Dialogue===
The dialogue form allows Diderot to examine issues from widely different perspectives. The character of Rameau is presented as extremely unreliable, ironical and self-contradicting, so that the reader may never know whether he is being sincere or provocative. The impression is that of nuggets of truth artfully embedded in trivia.

A parasite in a well-to-do family, Rameau has recently been kicked out because he refused to compromise with the truth. Now he will not humble himself by apologizing. And yet, rather than starve, shouldn't one live at the expense of rich fools and knaves as he once did, pimping for a lord? Society does not allow the talented to support themselves because it does not value them, leaving them to beg while the rich, the powerful and stupid poke fun at men like ''[[Georges-Louis Leclerc, Comte de Buffon|Buffon]], [[Charles Pinot Duclos|Duclos]], [[Montesquieu]], [[Jean-Jacques Rousseau|Rousseau]], [[Voltaire]], [[Jean le Rond d'Alembert|D'Alembert]],  Diderot''.<ref name="ram"/> The poor genius is left with but two options: to crawl and flatter or to dupe and cheat, either being repugnant to the sensitive mind. ''If virtue had led the way to fortune, I would either have been virtuous or pretended to be so like others; I was expected to play the fool, and a fool I turned myself into.''<ref name="ram"/>

==History==
In ''Rameau's Nephew'', Diderot attacked and ridiculed the critics of the [[Age of Enlightenment|Enlightenment]], but he knew from past experience that some of his enemies were sufficiently powerful to have him arrested or the work banned. Diderot had done a spell in prison in 1749 after publishing his [[Lettre sur les aveugles à l'usage de ceux qui voient|''Lettre sur les aveugles'']] (Letter about the Blind) and his [[Encyclopédie]] had been banned in 1759. Prudence, therefore, may have dictated that he showed it only to a select few.

After the death of Diderot, the manuscript or a copy of it probably made its way to Russia. In 1765, Diderot had faced financial difficulties, and the Empress [[Catherine II of Russia|Catherine of Russia]] had come to his help by buying out his library. The arrangement was quite a profitable one for both parties, Diderot becoming the paid librarian of his own book collection, with the task of adding to it as he saw fit, while the Russians enjoyed the prospect of one day being in possession of one of the most selectively stocked European libraries, not to mention Diderot's papers.<ref>[http://users.skynet.be/fernand.verhaegen/catherine_ii_de_russie_et_diderot.htm Catherine and Diderot] {{fr icon}}, consulted December 16, 2007</ref><ref>The Russian National Library owns a unique collection of papers and books from Diderot's library.</ref>

An appreciative Russian reader communicated the work to [[Friedrich von Schiller|Schiller]], who shared it with [[Johann Wolfgang von Goethe|Goethe]] who translated it into German in 1805.<ref name="jcb"/>

[[Hegel]] quotes ''Rameau's Nephew'' in [https://www.marxists.org/reference/archive/hegel/works/ph/phc2b1a.htm#m522 §522] of his [[Phenomenology of Spirit]].

==Footnotes==
<references />

==External links==
*  {{en icon}} [http://www.openbookpublishers.com/product/498/denis-diderot-rameaus-nephew----le-neveu-de-rameau--a-multi-media-bilingual-edition/ "Multi-media bilingual edition; translation by KE Tunstall and C Warman; edition by Marian Hobson and Pascal Duc"]
* {{fr icon}} [http://abu.cnam.fr/cgi-bin/go?neveu2  ''Rameau's Nephew'' online text ABU]
* {{fr icon}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/diderot-denis-le-neveu-de-rameau.html/ ''Rameau's Nephew'', audio version] [[Image:Speaker Icon.svg|20px]]
* {{en icon}} Rameau's Nephew – Project Guttenberg [http://gutenberg.net.au/ebooks07/0700101h.html], PDF [http://tems.umn.edu/pdf/Diderot-RameausNephew.pdf]
* ''Le Neveu de Rameau'' is available on French [[Wikisource]].
* [https://librivox.org/rameaus-nephew-by-denis-diderot/ 'Rameau's Nephew'', English audio from Librivox]

{{Denis Diderot}}

{{Authority control}}
[[Category:1805 novels]]
[[Category:Novels by Denis Diderot]]
[[Category:French philosophical novels]]
[[Category:Novels set in Paris]]
[[Category:Satirical novels]]