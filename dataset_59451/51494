{{broader|Open access journal}}
{{Use mdy dates|date=May 2012}}
A '''hybrid open access journal''' is a subscription journal in which some of the articles are [[open access]]. This status typically requires the payment of a publication fee (also called an [[article processing charge]] or APC) to the publisher.

==History==
{{see also|:Category:Hybrid open access journals}}

The concept was first proposed in 1998 when Thomas Walker suggested that authors could purchase extra visibility at a price.<ref>{{cite journal |author=Walker, Thomas|journal=American Scientist |year=1998|title= Free Internet Access to Traditional Journals|doi=10.1511/1998.5.463|volume= 86 |issue=5|pages= 463}}</ref> The first journal recognized as using this model was Walker's own ''Florida Journal of Entomology''; it was later extended to the other publications of the [[Entomological Society of America]].  The idea was later refined by David Prosser in 2003<ref>{{cite journal|doi=10.1087/095315103322110923 |url=http://eprints.rclis.org/archive/00001179/ |journal=Learned Publishing |year=2003 |title=From here to there: a proposed mechanism for transforming journals from closed to open access |author=David Prosser |volume=16 |issue=3 |pages=163 |deadurl=yes |archiveurl=https://web.archive.org/web/20060501002050/http://eprints.rclis.org/archive/00001179/ |archivedate=May 1, 2006 |df=mdy-all }}</ref> in the journal ''Learned Publishing''.

Publishers that offer a hybrid open access option often use different names for it. The [[SHERPA/RoMEO]] site provides a list of publishers and the names of their options.<ref>http://www.sherpa.ac.uk/romeo/PaidOA.php</ref>

Hybrid journals are low risk for publishers to set up, because they still receive subscription income, but the high price of hybrid APCs has led to low uptake of the hybrid open access option.<ref name=LP>{{cite journal|last1=Björk|first1=Bo-Christer|last2=Solomon|first2=David|title=How research funders can finance APCs in full OA and hybrid journals|journal=Learned Publishing|date=2014|volume=27|doi=10.1087/20140203}}<!--|accessdate=2 June 2014--></ref> In 2014 the average APC for hybrid journals was calculated to be almost twice as high as APCs from full open access publishers.<ref>{{cite journal|last1=Björk|first1=Bo-Christer|last2=Solomon|first2=David|title=Developing an Effective  Market for Open Access  Article Processing Charges|date=March 2014|url=http://www.wellcome.ac.uk/stellent/groups/corporatesite/@policy_communications/documents/web_document/wtp055910.pdf|accessdate=2 June 2014}}</ref>

==Funding==
Some universities, research centers, foundations, and government agencies have funds designed to pay publication fees (APCs) of fee-based open access journals. Of these, some will pay publication fees of hybrid open access journals. However, policies about such payments differ. The Open Access Directory<ref>http://oad.simmons.edu/oadwiki/Main_Page Open Access Directory</ref> provides a list of funds that support open access journals, and provides information about which funds will pay fees of hybrid open access journals.<ref name="OA journal funds">{{cite web|title=OA journal funds|url=http://oad.simmons.edu/oadwiki/OA_journal_funds|website=Open Access Directory|accessdate=22 April 2016}}</ref> A substantial number of funds (40%) will not reimburse APCs in hybrid journals, including [[Harvard University]], [[CERN]], [[Deutsche Forschungsgemeinschaft]], [[Columbia University]] and the [[Research Council of Norway|Norwegian Research Council]].<ref name="OA journal funds"/>

Since one source of funds to pay for open access articles is the library subscription budget, it has been proposed that there needs to be a decrease in the subscription cost to the library in order to avoid 'double dipping' where an article is paid for twice – once through subscription fees, and again through an APC.<ref name=LP /> For example, the Open Access Authors Fund of the [[University of Calgary]] Library (2009/09) requires that: "''To be eligible for funding in this'' [hybrid open access] ''category, the publisher must plan to make (in the next subscription year) reductions to the institutional subscription prices based on the number of open-access articles in those journals.''"<ref>[http://library.ucalgary.ca/services/for-faculty/open-access-authors-fund-0 Open Access Authors Fund]</ref> On November 12, 2009, [[Nature Publishing Group]] issued a news release on how open access affected its subscription prices.<ref>[http://www.nature.com/press_releases/emboopen.html Open Access uptake prompts 9% price reduction for The EMBO Journal and EMBO reports]</ref>

A report on work carried out by the University of Nottingham since 2006 to introduce and manage an institutional open access fund has been published by Stephen Pinfield in ''Learned Publishing''.<ref>http://www.ingentaconnect.com/content/alpsp/lp/2010/00000023/00000001/art00008 ''Learned Publishing'' (Jan 2010)</ref> In this article, the author comments that: ''"As publishers' income has increased from OA [open-access] fees in the hybrid model, there has been little or no let-up in journal subscription inflation, and only a small minority of publishers have yet committed to adjusting their subscription prices as they receive increasing levels of income from OA options."''

==Advantages and disadvantages to the author==
The author wanting to publish in an open-access journal, is not limited to the relatively small number of "full" open-access journals, but can also choose from the available hybrid open-access journals, which includes journals published by many of the largest academic publishers.

The author must still find the money. Many funding agencies are ready to let authors use grant funds, or apply for supplementary funds, to pay publication fees at open-access journals.  (Only a minority of open-access journals charge such fees, but nearly all hybrid open occess journals do so.)  So far, the funding agencies that are willing to pay these fees do not distinguish between full and hybrid open-access journals. On October 19, 2009, one such funding agency, the [[Wellcome Trust]], expressed concerns about hybrid open-access fees being paid twice, through subscriptions and through publication fees.<ref>http://ukpmc.blogspot.com/2009/10/wellcome-trust-calls-for-greater.html Wellcome Trust calls for greater transparency from journals on open-access publishing costs</ref>

==Variations==
The [[American Society of Plant Biologists]] has adopted a policy<ref>http://www.aspb.org/pressreleases/oaformembers.cfm</ref> that articles contributed by society members to its journal, [[Plant Physiology (journal)|Plant Physiology]], will be made open access immediately on publication at no additional charge. Non-member authors can receive OA through payment of $1,000, but since membership is only $115/year,<ref>[http://www.aspb.org/membership/ membership]</ref> it is expected this initiative will boost membership.

Partial open access exists when only research articles are open (as in the ''[[BMJ]]'' journal) or vice versa, when only news and editorials are open (as in ''[[Genome Biology]]'').

==See also==
* [[List of open-access journals]]
* [[Scientific journal]]

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://www.earlham.edu/~peters/fos/newsletter/09-02-06.htm#hybrid Nine questions for hybrid journal programs] by Peter Suber, ''SPARC Open Access Newsletter'', issue No. 101, September 2, 2006.
* [http://www.earlham.edu/~peters/fos/2007/11/more-on-society-publishers-with-oa.html More on society publishers with OA journals] by Peter Suber, ''Open Access News'', November 3, 2007.
* [http://dx.doi.org/10.1371/journal.pbio.0050285 When Is Open Access Not Open Access?] by Catriona J. MacCallum, ''PLoS Biology'', 2007; 5(10): e285.

{{Open access navbox}}

[[Category:Open access (publishing)]]
[[Category:Hybrid open access journals| ]]