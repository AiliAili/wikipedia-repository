{{Infobox journal
| title = ACS Catalysis
| cover = [[File:Accacs.gif|150 px]]
| editor = [[Christopher W. Jones]]
| discipline = [[Chemistry]], [[catalysis]]
| abbreviation = ACS Catal.
| publisher = [[American Chemical Society]]
| country = United States
| frequency = Monthly
| history = 2011-present
| impact       = 9.312
| impact-year  = 2014
| website = http://pubs.acs.org/journal/accacs
| RSS = http://pubs.acs.org/action/showFeed?ui=0&mi=qjmolc&ai=53h&jc=accacs&type=etoc&feed=rss
| CODEN = ACCACS
| eISSN = 2155-5435
}}
'''''ACS Catalysis''''' is a monthly online [[peer review|peer-reviewed]] [[scientific journal]] established in 2011 by the [[American Chemical Society]]. The journal publishes original research on [[heterogeneous catalysis|heterogeneous]], [[homogeneous catalysis|homogeneous]], and [[biocatalysis]]. <ref name=about>{{Cite web
 | title =About the journal
 | work =ACS Catalysis
 | publisher =American Chemical Society 
 | date =July 2012
 | url = http://pubs.acs.org/page/accacs/about.html
 | accessdate =2012-07-20}}</ref> The [[editor-in-chief]] is [[Christopher W. Jones]] (School of Chemical & Biomolecular Engineering and School of Chemistry and Biochemistry, [[Georgia Institute of Technology]]). 

== Types of content ==
''ACS Catalysis'' publishes [[Scientific journal#Types of articles|Letters]], [[Scientific journal#Types of articles|Articles]], [[Scientific journal#Types of articles|Reviews]], and Perspectives and Viewpoints. Reviews, Perspectives and Viewpoints appear mostly on invitation.<ref name="NtA">{{cite web
| last =
| first =
| year = 2012
| url = http://pubs.acs.org/paragonplus/submission/accacs/accacs_authguide.pdf
| title = Information for Authors
| accessdate = 2012-07-20}}</ref> 

== Scope ==
''ACS Catalysis'' covers experimental and theoretical research and reviews on molecules, macromolecules, or materials that are catalytic in nature, that is, they exhibit catalytic turnover. Application coverage includes life sciences, drug discovery & development, household products, polymer discovery & production, environmental protection and energy & fuels.<ref name=about/>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Chemical Abstracts Service]]<ref name=cassi>{{Cite web
 | title =CAS Source Index (CASSI)
 | work = 
 | publisher =American Chemical Society
 | date =July 2012 
 | url = http://cassi.cas.org/publication.jsp?P=eCQtRPJo9AQyz133K_ll3zLPXfcr-WXfx4NAvgu_yHJBOddVnYhPjzLPXfcr-WXfMs9d9yv5Zd9dPe2jm4Ja1TLPXfcr-WXfMs9d9yv5Zd94XjESzk1Z5Q
 | format = 
 | accessdate =2012-07-20}}</ref> and the [[ISI Web of Knowledge]].<ref name=about/>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://pubs.acs.org/journal/accacs/}}

{{DEFAULTSORT:Acs Catalysis}}
[[Category:American Chemical Society academic journals]]
[[Category:Chemistry journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 2011]]
 [[Category:English-language journals]]