{{For|the Irish Roman Catholic priest|Martin Glynn (priest)}}
{{Infobox officeholder
|name         = Martin Glynn
|image        = Governor Martin H. Glynn.jpg
|order        = 40th [[Governor of New York]]
|lieutenant   = [[Robert F. Wagner]] {{small|(Acting)}}
|term_start   = October 17, 1913
|term_end     = December 31, 1914
|predecessor  = [[William Sulzer]]
|successor    = [[Charles S. Whitman]]
|office1      = [[Lieutenant Governor of New York]]
|governor1    = [[William Sulzer]]
|term_start1  = January 1, 1913
|term_end1    = October 17, 1913
|predecessor1 = [[Thomas F. Conway]]
|successor1   = [[Robert F. Wagner]]
|office2      = 39th [[New York State Comptroller|Comptroller of New York]]
|governor2    = [[Charles Evans Hughes]]
|term_start2  = January 1, 1907
|term_end2    = December 31, 1908
|predecessor2 = [[William C. Wilson]]
|successor2   = [[Charles H. Gaus]]
|state3       = [[New York (state)|New York]]
|district3    = {{ushr|NY|20|20th}}
|term_start3  = March 4, 1899
|term_end3    = March 4, 1901
|predecessor3 = [[George N. Southwick]]
|successor3   = [[George N. Southwick]]
|birth_name   = Martin Henry Glynn
|birth_date   = {{birth date|1871|9|27}}
|birth_place  = [[Valatie, New York]], [[United States|U.S.]]
|death_date   = {{death date and age|1924|12|14|1871|9|27}}
|death_place  = [[Albany, New York]], [[United States|U.S.]]
|party        = [[Democratic Party (United States)|Democratic]]
|education    = [[Fordham University]] {{small|([[Bachelor of Arts|BA]])}}<br>[[Union University (New York)|Union University, New York]] {{small|([[Bachelor of Laws|LLB]])}}
}}
'''Martin Henry Glynn''' (September 27, 1871{{spaced ndash}}December 14, 1924) was an American politician. He was the [[List of Governors of New York|40th Governor of New York]] from 1913 to 1914, the first [[Irish American]] [[Roman Catholic]] head of government of what was then the most populated state of the [[United States]].

== Life ==
Glynn was born in the town of [[Kinderhook, New York|Kinderhook, N.Y.]], and grew up in one of Kinderhook's villages, the [[Valatie, New York|village of Valatie]],  He graduated from [[Fordham University]] in 1894, then studied at [[Albany Law School]] of [[Union University (New York)|Union University, New York]], and was admitted to the bar in 1897. From 1896 on, he wrote for the ''[[Albany Times-Union]]'' daily newspaper, becoming eventually its editor, publisher and owner.

Glynn was elected as a [[United States Democratic Party|Democrat]] to the [[56th United States Congress]], and served from March 4, 1899, to March 3, 1901. He was [[New York State Comptroller]] from 1907 to 1908, elected in [[New York state election, 1906|1906]], but defeated for re-election in [[New York state election, 1908|1908]] by Republican [[Charles H. Gaus]].

He was elected [[Lieutenant Governor of New York]] in [[New York state election, 1912|1912]] on the ticket with [[William Sulzer]], and succeeded to the governorship upon Sulzer's [[impeachment]] and removal from office in 1913. He was the first [[Catholic]] New York governor, but was defeated for re-election by [[Charles S. Whitman]] in [[New York state election, 1914|1914]].

He was a delegate to the [[1916 Democratic National Convention|1916]] and [[1924 Democratic National Convention]]s. As the keynote speaker at the 1916 National Democratic Convention, Glynn delivered one of his most famous speeches, praising the accomplishments of President Woodrow Wilson and the platform of the Democratic Party.

Martin Glynn was active in the Progressive movement and had an interest in Irish-American affairs.

Glynn committed suicide by gunshot in 1924, after having suffered throughout his adult life from chronic back pain caused by a spinal injury.  Though the cause of death was listed on his death certificate, the local media reported that Glynn died of heart trouble.<ref>{{cite news |author= |coauthors= |title=Ex-Gov. Glynn Dies Suddenly In Albany Home. Stricken With a Heart Attack After His Return From a Boston Sanitarium |url=https://query.nytimes.com/gst/abstract.html?res=9E06E1DE113BE233A25756C1A9649D946595D6CF |quote=Former Governor Martin H. Glynn died in his home here today. Mr. Glynn returned yesterday from a hospital in the suburbs of Boston, where he had been under treatment during the last two months for spinal trouble of long standing. Members of his family said he complained last night of not feeling well, but attributed it to the trip |newspaper=[[New York Times]] |date=December 14, 1924 |accessdate=2014-08-01 }}</ref>  The true story of his death was publicized in Dominick Lizzi's 1994 biography.<ref>Dominick C. Lizzi, ''Governor Martin H. Glynn, Forgotten Hero'', Valatie Press.  LOC Catalog Card Number:94-96495</ref><ref>Paul Grondahl, Albany Times-Union, [http://web.timesunion.com/specialreports/tu150/overview/overview4.asp Big News, Small-Town Flavor: 1924 is a Turning Point], retrieved December 18, 2013</ref>  He was buried at the [[St. Agnes Cemetery]] in [[Menands, New York]].<ref>{{Findagrave|6844305}}</ref>

== "The Crucifixion of Jews Must Stop!" ==
Glynn's article "The Crucifixion of Jews Must Stop!" was published in the October 31, 1919, issue of ''[[The American Hebrew]]''; in it he lamented the poor conditions for European Jews after [[World War I]]. Glynn referred to these conditions as a potential "[[Wiktionary:holocaust|holocaust]]" and asserted that "six million Jewish men and women are starving across the seas".<ref>[http://codoh.com/library/document/871 html of complete text]</ref><ref>[http://www.jrbooksonline.com/images/091031crucifixion.gif Image of the text]</ref><ref>[https://www.jewishvirtuallibrary.org/jsource/Holocaust/workcite.html reference to article in Jewish Virtual Library]</ref> [[Robert N. Proctor]] observes that "[this] oddity has been exploited by Holocaust deniers but is simply a remarkable coincidence and nothing more." <ref>[[Robert N. Proctor|Proctor, Robert N.]] (2000). ''The Nazi War on Cancer''. [[Princeton University Press]]. p. 11.</ref>

== Notes ==
{{reflist}}

== Sources ==
* ''[https://query.nytimes.com/mem/archive-free/pdf?res=9E0DEEDB1631E733A2575AC0A9679D946797D6CF Glynn Ill in Germany, May Decline Office; Comptroller-elect Suffering from Injury to His Spine]'', New York Times, November 9, 1906
* [http://www.valatielibrary.org/glynn1.htm Martin H. Glynn Biography] at Valatie, New York Library
* [http://politicalgraveyard.com/bio/glovinsky-gochenour.html Martin H. Glynn] at Political Graveyard (gives as birthplace [[Kinderhook, New York|Kinderhook]], the town which includes the Village of Valatie)
* [http://www.nysl.nysed.gov/msscfa/sc21255.htm Martin Henry Glynn Papers, 1913-1924] at the [[New York State Library]]
* ''[http://findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=62173179 Mary Magrane Glynn]'' at Find a Grave {{unreliable source?|date=January 2016}}

{{s-start}}
{{s-par|us-hs}}
{{s-bef|before=[[George N. Southwick]]}}
{{s-ttl|title=Member of the [[List of United States Representatives from New York|U.S. House of Representatives]]<br>from [[New York's 20th congressional district]]|years=1899–1901}}
{{s-aft|after=[[George N. Southwick]]}}
|-
{{s-off}}
{{s-bef|before=[[William C. Wilson]]}}
{{s-ttl|title=[[New York State Comptroller|Comptroller of New York]]|years=1907–1908}}
{{s-aft|after=[[Charles H. Gaus]]}}
|-
{{s-bef|before=[[Thomas F. Conway]]}}
{{s-ttl|title=[[Lieutenant Governor of New York]]|years=1913}}
{{s-aft|after=[[Robert F. Wagner]]}}
|-
{{s-bef|before=[[William Sulzer]]}}
{{s-ttl|title=[[Governor of New York]]|years=1913–1914}}
{{s-aft|after=[[Charles S. Whitman]]}}
|-
{{s-ppo}}
{{s-bef|before=[[William Sulzer]]}}
{{s-ttl|title=[[Democratic Party (United States)|Democratic]] nominee for [[Governor of New York]]|years=1914}}
{{s-aft|after=[[Samuel Seabury (judge)|Samuel Seabury]]}}
|-
{{s-bef|before=[[Alton B. Parker]]}}
{{s-ttl|title=Keynote Speaker of the [[Democratic National Convention]]|years=[[1916 Democratic National Convention|1916]]}}
{{s-aft|after=[[Homer Cummings]]}}
{{s-end}}

{{Governors of New York|selected=|state1=expanded|state2=expanded}}
{{NYSComptroller}}
{{Authority control}}

{{DEFAULTSORT:Glynn, Martin Henry}}
[[Category:1871 births]]
[[Category:1924 deaths]]
[[Category:Albany Law School alumni]]
[[Category:American newspaper editors]]
[[Category:American people of Irish descent]]
[[Category:American politicians who committed suicide]]
[[Category:American Roman Catholics]]
[[Category:Burials at St. Agnes Cemetery, Menands, New York]]
[[Category:Democratic Party members of the United States House of Representatives]]
[[Category:Democratic Party state governors of the United States]]
[[Category:Fordham University alumni]]
[[Category:Governors of New York]]
[[Category:Irish diaspora politicians]]
[[Category:Journalists from New York]]
[[Category:Jurists who committed suicide]]
[[Category:Lieutenant Governors of New York]]
[[Category:Members of the United States House of Representatives from New York]]
[[Category:New York Democrats]]
[[Category:New York lawyers]]
[[Category:New York State Comptrollers]]
[[Category:People from Albany, New York]]
[[Category:People from Valatie, New York]]
[[Category:Progressive Era in the United States]]
[[Category:Suicides in New York]]