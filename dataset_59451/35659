{{good article}}
{{Geobox|Protected area
| name = Above All State Park
| category = [[List of Connecticut state parks|Connecticut State Park]]
| image = AboveAllSP2.jpg
| image_caption = 
| image_size =280
| country = {{flag|United States}}
| state = {{flag|Connecticut}}
| region_type = County
| region = [[Litchfield County, Connecticut|Litchfield]]
| district_type = Town
| district = [[Warren, Connecticut|Warren]]
| location = 
| elevation_imperial = 1470
| elevation_round = 0
| elevation_note = <ref name=gnis>{{cite gnis|205037|Above All State Park}}</ref>
| lat_d = 41
| lat_m = 43
| lat_s = 36
| lat_NS = N
| long_d = 73
| long_m = 21
| long_s = 10
| long_EW = W
| coordinates_note = <ref name=gnis/>
| area_unit = acre
| area_imperial = 31
| area_round = 0
| area_note = <ref name=pristaff/>
| established_type = Established
| established = 1927
| management_body = Connecticut Department of Energy and Environmental Protection
| map_locator = Connecticut
| map = Connecticut Locator Map.PNG
| map_caption = Location in Connecticut
| map_size =280
| website = [http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284#AboveAll Above All State Park]
}}

'''Above All State Park''' is an undeveloped public recreation area located in the [[New England town|town]] of [[Warren, Connecticut|Warren]], [[Connecticut]].<ref name=DEEP/> Remnants of a [[Cold War|Cold War-era]] military [[radar]] installation (pictured at right) may be seen. The only park amenities are informal trails not maintained by the [[Connecticut Department of Energy and Environmental Protection]].

== History ==
Because of its reputation as one of the best lookouts in Litchfield County, the Above All peak was the site of a wooden observation tower in the years before the [[American Civil War|Civil War]].<ref name=commissionreports/> A mountain-top summer resort planned in the 1880s, that would have featured a {{convert|125|ft|adj=on}} observation tower for the viewing of far distant sights, never materialized.<ref name="book2"/> The park originated in 1927 when the heirs of Seymour Strong gave three acres of land to the state.<ref name=theday/> The state's purchase of 28 adjoining acres from the Stanley estate followed in December 1927.<ref name=telegram/> In 1934, the ''State Register and Manual'' identified Above All as Connecticut's 36th state park.<ref name=register/>

From June 1957 to June 1968, the state park became a [[Semi-Automatic Ground Environment]] (SAGE) Air Defense Network radar site. The military installation was called the New Preston Gap-Filler RADAR Annex P-50A /Z-50A. The site was an unmanned gap-filler "providing low altitude coverage" that "consisted of the radar and tower along with the building which contained the radar equipment and a diesel generator."<ref name=cold/><ref name=green/> In 1968, a dirt road and cinder block building were added to the top of the hill as part of an upgrade to the site.<ref name=leary /> In 1981, the park was the subject of a study by [[Northeast Utilities]] as a possible [[wind power]] site. An [[anemometer]] and wind vane were placed atop a {{convert|65|ft|adj=on}} tower to record wind data.<ref name=theday2/>

==Park name==
According to the [[Works Progress Administration|WPA]] writers who created ''Connecticut: A Guide to Its Roads, Lore, and People'' in the 1930s, the park's name came from its "top of the world" isolation.<ref name="book1"/> In his book on Connecticut's state parks, Joseph Leary traces the name to the land's use by the Stone family, who claimed it was the highest working farm by elevation in all of Connecticut.<ref name=leary2/> 

==Status==
Accessing the park off [[Connecticut Route 341]] requires passing a barred gate. Informal trails near the top of the park are not maintained or marked and there are no facilities.<ref name=leary /> Structures on the site include the radar equipment building, footings for the radar tower, and supports for the generator's fuel tank. Photos displayed on the ''Radome'' website show the condition of the site in 2001 and 2006 with the equipment building in "excellent condition," and the radar tower and chain-link fencing missing. The site has been vandalized by graffiti.<ref name=radome /> 

==References==
{{reflist|refs=
<ref name=DEEP>{{cite web |url=http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284&deepNav_GID=1650#AboveAll |title=Above All State Park |work=State Parks and Forests |publisher=Connecticut Department of Energy and Environmental Protection |accessdate=February 5, 2013}}</ref>

<ref name=pristaff>{{cite web |url=http://www.cga.ct.gov/pri/docs/2013/State%20Parks%20and%20Forests%20Funding%20Staff%20Findings%20and%20Recommendations%20.pdf |title=Appendix A: List of State Parks and Forests |work=State Parks and Forests: Funding |series=Staff Findings and Recommendations |publisher=Connecticut General Assembly |date=January 23, 2014 |page=A-1 |accessdate=September 2, 2016}}</ref>

<ref name=green>A writer for the Connecticut Green Party has made unsubstantiated claims that the installation was a Nike missile launch site and that the existence of the park has reverted to secrecy. {{cite web |url=http://ct.greens.org/articles/bedell_2008_08.shtml |title=Connecticut's Nuclear Weapons |first=David |last=Bedell |publisher=Green Party of Connecticut |date=August 2008 |accessdate=March 20, 2014}}</ref>

<ref name="book1">{{cite book |url=https://archive.org/details/connecticut00federich |title=Connecticut: A Guide to Its Roads, Lore, and People |author=Federal Writers' Project |series=American Guide Series |location=Boston |publisher=Houghton Mifflin Company |year=1938 |page=455 |accessdate=July 8, 2015}}</ref>

<ref name="book2">Bookseller Ray Boas discussing the contents of {{cite book |title=Through the Housatonic Vally to the Hills and Homes of Berkshire |author=Bryan, Clark W. |location=Bridgeport, Conn. |publisher=The Housatonic Railroad |year=1884 |url=http://www.rayboasbookseller.com/conn.htm |accessdate=September 3, 2016}}</ref>

<ref name=theday>{{cite news |url=https://news.google.com/newspapers?nid=1915&dat=19280112&id=N6AtAAAAIBAJ&sjid=e3EFAAAAIBAJ&pg=3808,823106 | title=Park commission to buy 125 acres for state forest |newspaper=The Day |location=New London, Conn. |date=January 12, 1928 |accessdate=May 19, 2014}}</ref>

<ref name=theday2>{{cite news | url=https://news.google.com/newspapers?nid=1915&dat=19810117&id=kBUiAAAAIBAJ&sjid=CXUFAAAAIBAJ&pg=1080,2523696 |title=Northeast Utilities studying wind power |newspaper=The Day |location=New London, Conn.  |date=January 17, 1981 | accessdate=19 May 2014}}</ref>
<ref name=telegram>{{cite news |url=https://www.newspapers.com/clip/1471198/purchase_of_land_for_above_all_state/ |title=State to buy 28 acres of land on mountain |newspaper=The Bridgeport Telegram |location=Bridgeport, Conn. |date=December 9, 1927 |page=13 |accessdate=January 1, 2015}}</ref>

<ref name=register>{{cite web |url=https://archive.org/stream/register34conn#page/n233/mode/2up |title=State Parks |work=State Register and Manual 1934 |publisher=State of Connecticut |year=1934 |page=227 |accessdate=September 3, 2016}}</ref>

<ref name="leary">{{cite book |title=A Shared Landscape: A Guide & History of Connecticut's State Parks & Forests |first=Joseph |last=Leary |publisher=Friends of the Connecticut State Parks, Inc. |location=Hartford, Conn. |year=2004 |pages=228 |isbn=0974662909}}</ref>

<ref name="leary2">Leary opines that despite its name, the park is not "above all" in terms of scale, views or elevation and cites the scale of [[Lake Waramaug State Park]], the views of [[Mount Tom State Park]], and the elevation of [[Dennis Hill State Park]]. Leary, p. 228</ref>

<ref name=cold>{{cite web |url=http://coldwar-ct.com/Nike_Warren.html |title=SAGE New Preston |work=Coldwar-Ct.com |accessdate=May 19, 2014}}</ref>

<ref name=radome>{{cite web |url=http://www.radomes.org/museum/parsehtml.php?key=NewPrestonCT.html&type=recent_html |title=Recent photos of New Preston, CT GFA |publisher=Radomes, Inc. - The Air Defense Radar Veterans Association |accessdate=May 19, 2014}} The ''Radomes'' website states that the site is "not within a state park as earlier reported," then gives the same directions and site description as Leary and Bedell.</ref>

<ref name=commissionreports>{{cite book |url=https://books.google.com/books?id=tNxWAAAAMAAJ&pg=RA7-PA27 |title=Report of the State Park and Forest Commission to the Governor (1928) |publisher=State of Connecticut  |location=Hartford, Conn. |date=December 22, 1928 |page=27 |accessdate=September 2, 2016}}</ref>
}}

==External links==
{{commons category|Above All State Park}}
*[http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284#AboveAll Above All State Park] Connecticut Department of Energy and Environmental Protection

{{Protected areas of Connecticut}}

[[Category:State parks of Connecticut]]
[[Category:Parks in Litchfield County, Connecticut]]
[[Category:Warren, Connecticut]]