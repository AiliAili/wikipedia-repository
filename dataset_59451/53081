The '''University of Missouri Press''' is a [[university press]] operated by the [[University of Missouri]] in [[Columbia, Missouri]] and [[London|London, England]]; it was founded in 1958 primarily through the efforts of English professor William Peden.<ref>{{Cite web|url=http://press.umsystem.edu/pages/history_mission.aspx |title=History and Mission |publisher=University of Missouri Press |accessdate=May 26, 2012}}</ref> Many publications are by, for, and about Missourians. The Press also emphasizes the areas of American and world history; military history; intellectual history; biography; journalism; African American studies; women's studies; American, British, and Latin American literary criticism; political science; regional studies; and creative nonfiction. The Press has published 2,000 books since its founding and currently publishes about 30 mostly academic books a year.

==Notable Publications==
Among its notable publications were:
*Collected works of [[Langston Hughes]]
*Collected works of [[Eric Voegelin]]
*[[Robert H. Ferrell]]'s Give 'em Hell, Harry series about [[Harry Truman]]

==Series==
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=American%20Military%20Experience&sf=ss=American%20Military%20Experience The American Military Experience Series], edited by John C. McManus.

The books in this series portray and analyze the experience of Americans in military service during war and peacetime from the onset of the twentieth century to the present. The series emphasizes the profound impact wars have had on nearly every aspect of recent American history and considers the significant effects of modern conflict on combatants and noncombatants alike. Titles in the series may include accounts of battles, campaigns, and wars; unit histories; biographical and autobiographical narratives; investigations of technology and warfare; studies of the social and economic consequences of war; and in general, the best recent scholarship on Americans in the modern armed forces. The books in the series are written and designed for a diverse audience that encompasses nonspecialists as well as expert readers.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Collected%20Work%20Langston%20Hughes&sf=ss=Collected%20Work%20Langston%20Hughes The Collected Works of Langston Hughes]
 
The University of Missouri Press is proud to have completed the publication of The Collected Works of Langston Hughes, a compilation of the novels, short stories, poems, plays, essays, and other published work by one of the twentieth century's most prolific and influential African American authors. The series makes available Hughes's most famous works as well as lesser-known and out-of-print selections, providing readers and libraries with a comprehensive source for the first time. The volumes of the Collected Works have been published with the same goal that Hughes pursued throughout his lifetime: making his books available to the people. Each volume includes a chronology by Arnold Rampersad as well as an introduction by the volume's editor.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Collected%20Works%20Eric%20Voegelin&sf=ss=Collected%20Works%20Eric%20Voegelin The Collected Works of Eric Voegelin]
 
Eric Voegelin (1901-1985) was one of the most original and influential philosophers of our time. Born in Cologne, Germany, he studied at the University of Vienna, where he became a professor of political science in the Faculty of Law. In 1938, he and his wife, fleeing Hitler, immigrated to the United States. They became American citizens in 1944. Voegelin spent much of his career at Louisiana State University, the University of Munich, and the Hoover Institution at Stanford University.

During his lifetime he published many books and more than one hundred articles. The Collected Works of Eric Voegelin makes available in a uniform edition all of Voegelin's major writings. In thirty-four volumes the Collected Works includes the monumental, never-before-published History of Political Ideas; important published essays not previously collected in book form or published only in German; previously unpublished essays; selected correspondence; translations of seven books, four of them never before available in English; and the five-volume Order and History.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Eric%20Voegelin%20Inst%20Series&sf=ss=Eric%20Voegelin%20Inst%20Series The Eric Voegelin Institute Series in Political Philosophy]
 
A series of studies in political theory, constitutionalism, and intellectual history, The Eric Voegelin Institute Series in Political Philosophy consists of original, single-authored scholarly studies addressing the perennial issues of political philosophy in its theoretical, institutional, biographical, and historical aspects.

[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Give%20%27%27Em%20Hell%20Harry&sf=ss=Give%20%27%27Em%20Hell%20Harry The Give 'Em Hell Harry Series], edited by Robert H. Ferrell.
 
Harry S. Truman, the "man from Missouri" who served as the thirty-third president of the United States, has been the subject of many books. Historians, political figures, friends, foes, and family members—all have sought to characterize, understand, and interpret this figure who continues to live in the minds and imaginations of a broad reading public. The Give 'Em Hell Harry Series is designed to keep available in reasonably priced paperback editions the best books that have been written about Harry S. Truman.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Mark%20Twain%20%26%20His%20Circle&sf=ss=Mark%20Twain%20%26%20His%20Circle The Mark Twain and His Circle Series], edited by Tom Quirk.
 
This series incorporates books on Mark Twain and the several circles he inhabited (domestic, political, artistic, and other) to provide a venue for new research in Twain studies and, from time to time, to reprint significant studies that have been too long out of print.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Missouri%20Biography%20Series&sf=ss=Missouri%20Biography%20Series The Missouri Biography Series], edited by William E. Foley.
 
The Missouri Biography Series accomplishes a valuable goal—making available biographies of notable Missourians. This series focuses on citizens who were born in the state and those whose careers had significant impact on the region. From baseball great Stan Musial to renowned musician Scott Joplin to former president Harry S. Truman, this series highlights the lives of some of the notable individuals who have dwelt in the state of Missouri.

[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Missouri%20Heritage%20Readers&sf=ss=Missouri%20Heritage%20Readers The Missouri Heritage Readers Series], edited by Rebecca B. Schroeder.
 
Each Missouri Heritage Reader explores a particular aspect of the state's rich cultural heritage. Focusing on people, places, historical events, and the details of daily life, these books illustrate the ways in which people from all parts of the world contributed to the development of the state and the region. The books incorporate documentary and oral history, folklore, and informal literature in a way that makes these resources accessible to all Missourians. Intended primarily for adult new readers, these books will also be invaluable to readers of all ages interested in the cultural and social history of Missouri.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Shades%20of%20Blue%20%26%20Gray&sf=ss=Shades%20of%20Blue%20%26%20Gray The Shades of Blue and Gray Series], edited by Herman Hattaway, Jon Wakelyn, and Clayton E. Jewett.
 
Military history today addresses the relationship between society and warfare. Thus biographies and thematic studies that deal with civilians, soldiers, political leaders are increasingly important to a larger public. The Shades of Blue and Gray Series offers Civil War studies for the modern reader—Civil War buff and scholar alike. 
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Sports%20%26%20American%20Culture&sf=ss=Sports%20%26%20American%20Culture The Sports and American Culture Series], edited by [[Roger Launius]].
 
This spirited series explores sports in relation to the main currents of American culture. The series, which will include biographies, autobiographies, and team histories, as well as explorations of exciting eras, events, and moments, will address the issues of race, gender, ethnicity, class, immigration, urban life, and the revolutionary role of the media in the contemporary presentation of sports.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Southern%20Women&sf=ss=Southern%20Women The Southern Women Series], edited by Theda Perdue, Betty Brandon, and Virginia Bernhard.
 
[http://press.umsystem.edu/Catalog/ProductSearch.aspx?ExtendedSearch=false&SearchOnLoad=true&rhl=Brick%20Lecture%20Series&sf=ss=Brick%20Lecture%20Series The Paul Anthony Brick Lectures], which include works by such notable scholars as John Hope Franklin and Sissela Bok.

==Reorganization==
The University proposed in spring 2012 to close the press and terminate its ten employees in order to end the University's subsidy to the press, estimated by the University administration as $400,000 per year and by outside critics of the closure decision as under $250,000 a year.<ref>William Least Heat-Moon, [http://www.columbiatribune.com/commentary/op-ed/university-press-closure-casts-bad-light-on-um-system/article_86d060a1-2c3f-5c27-82b4-7e1971533ba6.html "University Press closure casts bad light on UM System,"] ''Columbia Daily Tribune,'' July 15, 2012.</ref> The decision was reversed in August 2012 after public outcry.<ref>Jennifer Howard,  [http://chronicle.com/article/After-Outcry-Over-Closure-U/133988/?cid=at&utm_source=at&utm_medium=en "After Outcry Over Closure, U. of Missouri Press Is Back to Printing Books," ''Chronicle of Higher Education,'' Aug. 28, 2012]</ref> As part of the reorganization the Press now reports up to the main Columbia campus rather than the [[University of Missouri System]].<ref>http://www.kansascity.com/2013/06/01/4267865/university-of-missouri-press-emerges.html</ref> A new director was hired in 2013.

==References==
{{reflist}}

==External links==
*[http://www.umsystem.edu/upress/ University of Missouri Press]

{{Mizzou}}

[[Category:University presses of the United States|Missouri, University of]]
[[Category:University of Missouri]]
[[Category:Columbia, Missouri]]
[[Category:Publishing companies established in 1958]]
[[Category:Book publishing companies based in Missouri]]
[[Category:1958 establishments in Missouri]]