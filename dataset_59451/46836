'''''Canticum Canticorum''''' (''Song of Solomon'') is a cycle of 29 motets by  [[Giovanni Pierluigi da Palestrina]]. Originally titled ''Motettorum - Lieder Quartus'', this  [[Renaissance]] work is one of Palestrina's largest collections of [[Religious music|Sacred]] motets.

The work is in [[Latin]] and based upon excerpts from the book in the [[Hebrew Bible]]: [[Song of Songs|The Song of Songs]] in the [[Old Testament]]. The Song of Songs is thought to be an allegorical representation of the relationship of God and Israel as husband and wife. The text uses the main image of a man and wife almost throughout, the poem suggesting movement from courting to consummation.<ref>{{cite book|last1=Sweeney|first1=Marvin A.|title=Tanak: A Theological and Critical Introduction to the Jewish Bible|date=2011|publisher=Fortress Press|url=https://books.google.co.za/books?hl=en&lr=&id=qgNaaCGVx9wC&oi=fnd&pg=PT11&dq=Tanak:+A+Theological+and+Critical+Introduction+to+the+Jewish+Bible&ots=J1cZAGdrpM&sig=0ydfiOXoFcrhovFP97R_KOIJ-O4#v=onepage&q=Tanak%3A%20A%20Theological%20and%20Critical%20Introduction%20to%20the%20Jewish%20Bible&f=false}}</ref>

Palestrina's setting of the text is scored for 5 voices: SATTB (1 - 18, 29), SAATB (19 - 22, 27 - 28), and  SSATB (23 - 26); and was published, after revision, by [[Breitkopf and Härtel]] in the collection ''Palestrina Werke, vol. 4''.

== History ==

''Canticum Canticorum'' was written in the year 1584. This work, as with many of Palestrina's works around this time, was dedicated to Pope Gregory XIII. The work can, however, also be seen as a social statement which challenged what music was commonly accepted in the church at the time.<ref name="Dickson">{{cite journal|last1=Dickson|first1=Douglas|title=Palestrina's "Song of Solomon"|journal=Music and Letters|date=April 1937|volume=18|issue=2|pages=150–157|jstor=728380|publisher=Oxford University Press}}</ref>

It is assumed that during its composition, he kept in mind that other church composers had lost their jobs for composing madrigals, and he, also being guilty of this fault, would ‘repent’ by composing music that celebrates divine love. His illustration of passion in this piece exceeds that of any of his other sacred works. Through the work, Palestrina insists upon the independence of musicians, firstly through the composition thereof, and secondly through admitting awareness of what he has created - a musical setting of profane love songs, but ultimately a celebration of divine love. Palestrina famously declared: "Sic enim rem ipsam postulare intelligebam" - indicating that he realised that he was creating something new which may not be suitable for use in the church. He did, however, continue to refer to the motets as sacred.<ref name="Dickson" />

== Structure ==

The entire piece forms a dramatic lyric consisting of a series of individual motets. The work is also divided into larger sections, or episodes, distinguishable by mode. It is often questioned whether the motets were designed to be performed as a whole or separately, although it seems Palestrina attempted to arrange the text according to coherence and climax - strong evidence that the work is of a single design.

By grouping the motets together in modes, there is a sense of dramatic unity. Palestrina did not use the biblical versus in their original order, but arranged them to achieve a dramatic context. Motets 1 to 10 are in the G Dorian mode. The third motet ends with the lyrics “they made me the keeper of the vineyards”, while motet 4 opens with the text “but mine own vineyard have I not kept”. Motet 4 then continues with a question which is in turn answered in motet 5.
The entire work is set-up in this way, one motet connecting to the next -  telling a story that continues until the end of the first episode (motet 10). The following episodes continue with the same dramatic development. It is interesting to note how episode 2 (in G Mixolydian) ends unexpectedly with the use of a single motet in the Hypoaolian mode, the only one in the collection (motet 19). The final 2 episodes are in the E Phrygian mode and F Ionian mode respectively.<ref name="Dickson" />

The Motets are organized in the collection as follows:<ref name="Dickson" /><ref>{{cite web|title=Motettorum - Lieder Quartus (Palestrina)|url=http://imslp.org/wiki/Motettorum_-_Liber_Quartus_(Palestrina,_Giovanni_Pierluigi_da)|website=IMSLP|publisher=Petrucci Music Library|accessdate=4 May 2016}}</ref>

{| class="wikitable"
|-
!  !! Name !! Translation !! Text (within Song of Songs) !! Mode !! Voicing 
|-
| 1 || Osculetur me (osculo oris sui) || Let him kiss me (with the kiss of his mouth) || Chapter 1: 1-2 || G Dorian || SATTB
|-
| 2 || Trahe me post te || Draw me after you || Chapter 1: 3 || G Dorian || SATTB 
|-
| 3 || Nigra sum sed formosa || I am black, but comely || Chapter 1: 4 - 5 || G Dorian || SATTB
|-
| 4 || Vinean mean non custodivi || My vineyard I have not kept || Chapter 1: 6 - 7 || G Dorian || SATTB
|-
| 5 || Si ignotas te || If you do not know || Chapter 1: 8 - 9 || G Dorian || SATTB
|-
| 6 || Pulchrae sunt genae tuae || Thy cheeks are beautiful || Chapter 1:10 || G Dorian || SATTB
|-
| 7 || Fasciculus myrrahe (dilectus meus mihi)  || A bundle of Myrrh (is my beloved to me) || Chapter 1:13 - 15 || G Dorian || SATTB
|-
| 8 || Ecce tu pulcher es || Behold thou art fair || Chapter 1:16 - 2:1 || G Dorian || SATTB
|-
| 9 || Tota pulcher es || You are althougher beautiful || Chapter 4: 7 - 8 || G Dorian || SATTB
|-
| 10 || Vulnerasti cor meum || You have ravished my heart || Chapter 4:9-10 || G Dorian || SATTB
|-
| 11 || Sicut lilium inter spinas || A Lilly among thorns || Chapter 2:2 - 3 || G Mixolydian || SATTB
|-
| 12 || Introduxit me rex (in cellam vinariam) || He brought me (into the cellar of wine) || Chapter 2:4 - 5 || G Mixolydian || SATTB
|-
| 13 || Laeva eius (sub capite meo) || His left hand (is under my head) || Chapter 2:6 - 7 || G Mixolydian || SATTB
|-
| 14 || Vox dilecti mei || The voice of my beloved || Chapter 2:8 - 10 || G Mixolydian  || SATTB
|-
| 15 || Surge, propera amica mea || Arise, my love || Chapter 2: 10 - 13 || G Mixolydian || SATTB
|-
| 16 || Surge, amica mea (speciosa mea et veni) || Arise, my love (my fair one, and come away) || Chapter 2:13 - 14 || G Mixolydian || SATTB
|-
| 17 || Dilectus meus mihi, (et ego illi) || My beloved is mine, (and I am his) || Chapter 2: 16 - 17 || G Mixolydian  || SATTB
|-
| 18 || Surgam et circuibo civitatem || I will rise and go about the city || Chapter 3: 2 || G Mixolydian || SATTB
|-
| 19 || Adiuro vos (filiae Jerusalem) || I charge you (O ye daughters of Jerusalem) || Chapter 5: 8 - 10 || A Hypoaolian || SAATB
|-
| 20 || Caput eius (aurum optimum) || His head (is the finest gold) || Chapter 5: 11 - 12 || E Phrygian || SAATB
|-
| 21 || Dilectus meus descendit (in hortum suum) || My beloved has gone down (into his garden) || Chapter 6: 2 - 3 || E Phrygian || SAATB
|-
| 22 || Pulchra es, amica mea || Thou art beautiful, oh my love || Chapter 6: 4 - 5 || E Phrygian || SAATB
|-
| 23 || Quae est ista (quae progreditur quasi) || Who is she (that cometh like the dawn) || Chapter 6: 9 || E Phrygian || SSATB
|-
| 24 || Descendi in hortum nucum || I went down into the garden of nuts || Chapter 6: 10 || E Phrygian  || SSATB
|-
| 25 || Quam pulchri sunt (gressus tui in calceamentis) || How graceful are (your feet in sandals) || Chapter 7: 1 - 2 || F Ionian || SSATB
|-
| 26 || Duo ubera tua (sicut duo hinnuli) || Thy two breasts (are like two young roses) || Chapter 7: 3 - 5 || F Ionian || SSATB
|-
| 27 || Quam pulchra es et quam decora || How fair and pleasant you are || Chapter 7: 6 - 8 || F Ionian || SAATB
|-
| 28 || Guttur tuum sicut (vinum optimum) || Thy throat is like (the best wine) || Chapter 7: 9 - 10 || F Ionian || SAATB
|-
| 29 || Veni, veni dilecte mi, (egrediamur in agrum)  || Come, come my beloved, (let us go forth into the fields) || Chapter 7: 11 - 12 || F Ionian || SATTB
|}

== Analysis ==

The work is in five-part polyphony, which was considered un dramatic at the time. Therefore, some writers emphasised that if Palestrina intended for these motets to be a single quasi-dramatic work then the textual content would not have been set to five-part polyphony. On the other hand, Palestrina was deeply rooted in traditional polyphony, questioning the value of that claim. Nevertheless, the polyphony itself forms an important element in the work - ''for example'', in motet XIX, Palestrina makes use of polyphony to individualise the Lover. Shs asks the Daughters of Jerusalem about her lover and they reply. The polyphony symbolises the crowd, as well as expressing the Lover as an individual.<ref name="Dickson" />

In terms of form, Palestrina was restricted by the musical practices of his day. It was common for him and his contemporaries to overcome this obstacle by creating very long pieces that can be divided into shorter works, each clearly distinguishable by their episodic character. A unity was created by building the work at large around a specific theme. Interestingly, no form of the time was suited to anything as large as even the first 10 motets, which form the first episode of this work.<ref name="Dickson" />

Stylistically the individual pieces are in fact a combination of the madrigal and the motet. The texture is notably tighter than those typically found  in motets or masses of the time. There is also a powerful, passionate tone throughout the work, distinguishing it from others of its time.<ref name="Dickson" />

== Notes ==
{{reflist}}

{{italic title}}
{{Giovanni Pierluigi da Palestrina}}

[[Category:Compositions by Giovanni Pierluigi da Palestrina]]
[[Category:1584 compositions]]
[[Category:Motets]]