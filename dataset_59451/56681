{{refimprove|date=May 2015}}
{{Infobox journal
| title = Education About Asia
| cover = [[File:Education_About_Asia_(cover).jpg|200px]]
| former_name = <!-- or |former_names= -->
| abbreviation = Educ. Asia
| discipline = [[Asian studies]]
| editor = Lucien Ellington
| publisher = [[Association for Asian Studies]]
| country = United States
| history = 1996-present
| frequency = Triannually
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| ISSN = 1090-6851
| eISSN =
| CODEN =
| JSTOR = 
| LCCN = 97652872
| OCLC = 34427670
| website = http://www.asian-studies.org/Publications/EAA/About
| link1 = http://aas2.asian-studies.org/EAA/TOC/index.asp
| link1-name = Online access
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}
'''''Education About Asia''''' is a triannual [[peer-reviewed]] [[academic journal]] published by the [[Association for Asian Studies]] especially for the use of classroom teachers. The journal covers the entire field of [[Asian studies]], including classical cultures and literature to current events. Other frequent features are guides to print and digital resources, such as movies (both feature films and documentaries), teachings materials, and web resources. The journal frequently publishes theme issues; topics have included Islam in Asia, marriage and family, youth culture, religion, economics and business, visual and performing arts, and Asia countries in world history.

The journal was established in 1996 in order to support the teaching and general knowledge of the members of the association and others involved in teaching. All articles are available free online. The founding and current [[editor-in-chief]] is Lucien Ellington ([[University of Tennessee at Chattanooga]]). <ref>[https://www.asian-studies.org/EAA/EAA-TOC-SearchS.asp Volume 1 No 1 (Spring 1996)]</ref>

"Especially for university faculty who are new to teaching about Asia," says one scholar, "the publication is an indispensable starting place for both designing courses and building a general store of knowledge about Asia." {{sfnb|Frank|2011| p =81}}

== Notes==
{{Reflist}}
==References==
* {{cite| last = Frank |first= Adam D. | year = 2011| url = http://digitalcommons.unl.edu/nchchip/138 | title = Rethinking Asian Studies in the Interdisciplinary Honors Setting | journal= Honors in Practice Paper 138 (Online Archive) | ref = harv}}

== External links ==
* [http://www.asian-studies.org/Publications/EAA/About Education About Asia]

[[Category:Articles created via the Article Wizard]]
[[Category:Asian studies journals]]
[[Category:Publications established in 1996]]
[[Category:English-language journals]]
[[Category:Triannual journals]]
[[Category:Academic journals published by learned and professional societies]]

{{Humanities-journal-stub}}