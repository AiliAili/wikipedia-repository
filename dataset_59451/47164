{{italic title}}
'''''Commodore Perry in the Land of the Shogun''''' is a 1985 children's book by [[Rhoda Blumberg]]. This large-format book tells the story of [[Matthew C. Perry|Commodore Perry]] and the [[Perry Expedition|Black Ships]] that coerced Japan into ending its [[Sakoku|policy of complete isolation]] by establishing commercial and diplomatic relationships with other nations in 1854. The book is illustrated with period prints.<ref name="BriefReview">{{cite news|title=Children's Books; Bookshelf|url=https://www.nytimes.com/1985/06/23/books/children-s-books-bookshelf-050552.html|accessdate=5 May 2016|publisher=New York Times|date=23 June 1985}}</ref> Two-thirds of the illustrations are by Japanese artists, the remainder by artists with the American fleet.<ref name="Perrin"/>

==Critical reception==
Told from the perspective of the Americans involved, and some reviewers criticized it for underplaying the military threat to Japan implicit in Perry's expedition, and ignoring the broader context of American expansionism.<ref name="KirkusReview">{{cite news|title=Commodore Perry in the Land of the Shogun|url=https://www.kirkusreviews.com/book-reviews/rhoda-blumberg-6/commodore-perry-in-the-land-of-the-shogun/|accessdate=5 May 2016|publisher=Kirkus|date=20 January 1985}}</ref> Others praised it for its evenhanded treatment of cultural misunderstanding; a Japanese guest on board one of the Black Shops drinks a glass of olive oil, but an American sailor ashore tastes and buys a bottle of Japanese hair oil thinking it is liquor.<ref name="KirkusReview"/> [[Noel Perrin]] praised the book for telling both sides, "how the Japanese looked to the Americans, but also how the Americans looked to the Japanese;" he called Blumberg's book, "irresistible."<ref name="Perrin">{{cite book|last1=Perrin|first1=Noel|title=A Child's Delight|date=2003|publisher=University Press of New England|isbn=1584653523|page=145}}</ref>

==Awards==
The book won the 1986 [[Golden Kite Award]], the 1985 [[Boston Globe–Horn Book Award]], and was named a 1986 [[Newbery Medal]] honor book.<ref name="LosAngelesTimes">{{cite news|last1=Gregory|first1=Christiana|title=Commodore Perry in the Land of the Shogun|url=http://articles.latimes.com/1986-05-11/books/bk-5325_1_children-s-bookshelf|accessdate=5 May 2016|publisher=Los Angeles Times|date=11 May 1986}}</ref>

==Details==
''Commodore Perry in the Land of the Shogun''. New York : Lothrop, Lee & Shepard Books, 1985. 144 pages. ISBN 9780688037239

==References==
{{reflist|2}}

[[Category:American children's books]]
[[Category:1985 books]]
[[Category:Newbery Honor-winning works]]
[[Category:Children's history books]]

{{literature-stub}}