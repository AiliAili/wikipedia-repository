{|{{Infobox aircraft begin
|name           = Student Prince<!-- avoid stating manufacturer (it's stated 3 lines below) unless name used by other aircraft manufacturers -->
|image          = N10471 a 1929 Adcox Special Student Prince, serial number 2 (3832161673).jpg
|caption        = <!-- Image caption; if it isn't descriptive, please skip. -->
|alt            = <!-- Alt text for main image -->
}}
{{Infobox aircraft type
|type            = Sport lightplane
|national origin = [[United States of America]]<!-- Use the main nation (ie. UK), not constituent country (England); don't use "EU". List collaborative programs of only 2 or 3 nations; for more than 3, use "Multi-national:. -->
|manufacturer    = [[Adcox Aviation Trade School|Adcox]]<ref name="AerofileAb">{{cite web|url=http://aerofiles.com/_ab.html |title=Aircraft Ab - Ak |last=Eckland |first=K.O. |date=2 May 2009 |publisher=aerofiles.com |accessdate=26 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121205051134/http://aerofiles.com/_ab.html |archivedate=5 December 2012 |df= }}</ref>
|designer        = Basil B Smith<ref name="AerofileAb"/><!--Only appropriate for single designers, not project leaders-->
|first flight    = 17 September 1929<ref name="AerofileAb"/><!--If this hasn't happened, skip this field!-->
|introduced      = <!--Date the aircraft entered or will enter military or revenue service-->
|retired         = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status          = <!--In most cases, redundant; use sparingly-->
|primary user    = <!-- List only one user; for military aircraft, this is a nation or a service arm. Please DON'T add those tiny flags, as they limit horizontal space. -->
|more users      = <!-- Limited to THREE (3) 'more users' here (4 total users).  Separate users with <br/>. -->
|produced        = <!--Years in production (eg. 1970–1999) if still in active use but no longer built -->
|number built    = 6<ref name="AerofileAb"/><!-- Total number of flight-worthy aircraft completed. -->
|program cost    = <!--Total program cost-->
|unit cost       = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from  = <!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
|developed into  = <!--For derivative aircraft based on this aircraft-->
|-
}}|}

The '''Adcox Student Prince''' was a two-seat open-[[cockpit]] [[biplane]] designed by Basil Smith and built by the students of the US [[Adcox Aviation Trade School]] in [[1929 in aviation|1929]]. It was based on the one-off [[Adcox Special]], and the first example flew on 17 September.

A single example of a '''Student Prince X''' was produced in 1931 powered by a 90&nbsp;hp ACE engine.

As of 2004, a single example remains registered in the United States, which was successively re-engined with 100&nbsp;hp [[Kinner K-5]], then 110&nbsp;hp [[Warner Scarab Junior]], [[Comet 150hp]], 150&nbsp;hp [[Wright-Hisso A]] and 220&nbsp;hp [[Continental E-225]] in 1963.<ref name="AerofileAb"/>

==Specifications (Student Prince)==
{{Aircraft specs
|ref=<ref name="AerofileAb"/><!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=1
|length m=
|length ft=23
|length in=6
|length note=
|span m=
|span ft=30
|span in=3
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Cirrus Mk III]]
|eng1 type=4-cyl. air-cooled in-line piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=85<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=110
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=500
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following 
specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{Aircontent|
|related=
[[Adcox Special]]
|similar aircraft=
|sequence=
|lists=
|see also=
}}

==References==
;Notes
{{Reflist}}

==External links==
{{commons category|Adcox Student Prince}}

*{{cite web|url=http://aerofiles.com/_ab.html |title=Aircraft Ab - Ak |last=Eckland |first=K.O. |date=2 May 2009 |publisher=aerofiles.com |accessdate=26 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121205051134/http://aerofiles.com/_ab.html |archivedate=5 December 2012 |df= }}

{{Adcox aircraft}}

[[Category:Adcox aircraft|Student Prince]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:United States sport aircraft 1920–1929]]