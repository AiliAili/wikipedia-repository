{{Infobox military unit
|unit_name=Armed Boats Squadron Dubrovnik
|image=[[File:Odred naoruzanih brodova Dubrovnik amblem.svg|250px]]
|caption=Armed Boats Squadron Dubrovnik emblem
|dates= 1991&ndash;1992
|country= [[Croatia]]
|allegiance=
|branch= [[Croatian navy|Navy]]
|type= Volunteer unit
|role= [[Blockade runner]]s
|size= {{plainlist|
* 117 personnel
* 23 boats
}}
|command_structure=
|garrison=[[Dubrovnik]]
|garrison_label=
|equipment=
|equipment_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|battles= {{plainlist |
* [[Croatian War of Independence]]:
** [[Siege of Dubrovnik]]
}}
|anniversaries=6 December<!--veteran's day in Dubrovnik--->
|decorations= [[Order of Nikola Šubić Zrinski]]
|battle_honours=
|disbanded= 29 December 1992
<!-- Commanders -->
|current_commander=
|current_commander_label=
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment=
|colonel_of_the_regiment_label=
|notable_commanders=[[Lieutenant Colonel]] [[Aljoša Nikolić]]
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''Armed Boats Squadron Dubrovnik''' ({{lang-hr|Odred naoružanih brodova Dubrovnik}}) was a volunteer unit of the [[Croatian Navy]] that [[blockade runner|ran the naval blockade]] during the [[siege of Dubrovnik]] which formed part of the [[Croatian War of Independence]] in 1991–1992. It consisted of 23 vessels, mostly of the [[Runabout (boat)|runabout]] type, lightly armed and armoured. The unit was crucial in the defence of [[Dubrovnik]], providing a resupply route for the Dubrovnik population and troops defending the besieged city. Boats assigned to the squadron transported approximately 6,000 troops and civilians, about 100 wounded and 2,000&nbsp;tonnes of various cargo. A total of 117 personnel served with the squadron during the siege, suffering two fatalities.

The siege of Dubrovnik and the associated blockade running operations of the squadron took place from September 1991 until May 1992, during the peak of the fighting in the Croatian War of Independence. The commencement of the siege preceded the declaration of [[Independence of Croatia|Croatian independence]] on 8 October 1991. In early 1992, the Dubrovnik area saw fierce fighting while combat operations largely ceased elsewhere in [[Croatia]] following the [[Sarajevo Agreement]] of 2 January 1992.

''Sveti Vlaho'', the first vessel of the squadron, sunk in late 1991, was refloated in 2001 and placed on permanent exhibition in Dubrovnik. In 2006, the squadron was collectively decorated with the [[Order of Nikola Šubić Zrinski]] for bravery during the Croatian War of Independence. Surviving squadron members founded a squadron association in 2011 to preserve the heritage of the unit.

==Background==
{{Main|Croatian War of Independence}}
The [[Log Revolution|1990 revolt]] of the [[Serbs of Croatia|Croatian Serbs]] was centred on the predominantly Serb-populated areas of the [[Dalmatia]]n hinterland around the city of [[Knin]],<ref>{{cite news|newspaper=[[The New York Times]]|url=https://www.nytimes.com/1990/08/19/world/roads-sealed-as-yugoslav-unrest-mounts.html|agency=[[Reuters]]|title=Roads Sealed as Yugoslav Unrest Mounts|date=19 August 1990|accessdate=31 October 2012}}</ref> parts of [[Lika]], [[Kordun]], [[Banovina (region)|Banovina]] regions and in [[eastern Croatia]]n settlements with significant Serb population,<ref name="Martić-Verdict">{{cite web |title=The Prosecutor vs. Milan Martic - Judgement |url=http://www.icty.org/x/cases/martic/tjug/en/070612.pdf |publisher=[[International Criminal Tribunal for the former Yugoslavia]] |date=12 June 2007 |accessdate=11 August 2010|format=PDF}}</ref> and these areas were subsequently declared by the Serbs as the [[Republic of Serbian Krajina]] (RSK). The RSK declared its intention to  integrate politically with [[Republic of Serbia (1990-2006)|Serbia]], and this action was viewed by the [[Government of Croatia]] as a rebellion.<ref>{{cite news|newspaper=[[The New York Times]]|url=https://www.nytimes.com/1991/04/02/world/rebel-serbs-complicate-rift-on-yugoslav-unity.html|title=Rebel Serbs Complicate Rift on Yugoslav Unity|author=[[Chuck Sudetic]]|date=2 April 1991|accessdate=18 January 2013}}</ref> By March 1991, the conflict had escalated to war&mdash;the [[Croatian War of Independence]].<ref>{{cite news|newspaper=[[The New York Times]]|url=https://www.nytimes.com/1991/03/03/world/belgrade-sends-troops-to-croatia-town.html|author=Stephen Engelberg|title=Belgrade Sends Troops to Croatia Town|date=3 March 1991|accessdate=18 January 2013}}</ref> In June 1991, [[Independence of Croatia|Croatia declared its independence]] as [[Breakup of Yugoslavia|Yugoslavia disintegrated]].<ref>{{cite news|newspaper=The New York Times |url=https://www.nytimes.com/1991/06/26/world/2-yugoslav-states-vote-independence-to-press-demands.html |title=2 Yugoslav States Vote Independence To Press Demands |author=Chuck Sudetic |date=26 June 1991 |accessdate=12 December 2010 |archiveurl=http://www.webcitation.org/69Wj75HVR?url=http%3A%2F%2Fwww.nytimes.com%2F1991%2F06%2F26%2Fworld%2F2-yugoslav-states-vote-independence-to-press-demands.html |archivedate=29 July 2012 |deadurl=no |df= }}</ref> This was followed by a three-month moratorium on the decision,<ref>{{cite news|newspaper=[[The New York Times]]|url=https://www.nytimes.com/1991/06/29/world/conflict-in-yugoslavia-2-yugoslav-states-agree-to-suspend-secession-process.html|title=Conflict in Yugoslavia; 2 Yugoslav States Agree to Suspend Secession Process|author=[[Chuck Sudetic]]|date=29 June 1991 |accessdate=12 December 2010}}</ref> but the decision came into effect on 8 October.<ref>{{cite journal | url = http://narodne-novine.nn.hr/clanci/sluzbeni/1991_10_53_1265.html | journal = [[Narodne novine]] | issue = 53/1991 | language = Croatian | title = Odluka [Klasa: 021-03/91-05/07] | author = [[Croatian Parliament]] | date = 8 October 1991 | accessdate = 12 July 2012}}</ref>

As the [[Yugoslav People's Army]] (JNA) increasingly supported the RSK and the [[Croatian Police]] were unable to cope with the situation, the [[Croatian National Guard]] (ZNG) was formed in May 1991. The ZNG was renamed the [[Croatian Army]] (HV) in November.<ref>{{cite book | title=Eastern Europe and the Commonwealth of Independent States |year=1999| url=https://books.google.com/books?id=qmN95fFocsMC| ref = |publisher=Routledge | isbn=978-1-85743-058-5|pages=272–278}}</ref> The development of the [[military of Croatia|Croatian armed forces]] was hampered by a [[United Nations]] (UN) [[arms embargo]] introduced in September.<ref>{{cite news | newspaper = [[The Independent]] | url = http://www.independent.co.uk/news/world/croatia-built-web-of-contacts-to-evade-weapons-embargo-1556500.html | title = Croatia built 'web of contacts' to evade weapons embargo | author= Christopher Bellamy | date =10 October 1992 | accessdate=28 December 2012}}</ref> The final months of 1991 saw the fiercest fighting of the war, culminating in the [[Battle of the barracks]],<ref>{{cite news | newspaper = The New York Times | url = https://query.nytimes.com/gst/fullpage.html?res=9D0CE7DD1038F937A1575AC0A967958260&pagewanted=all | title = Serbs and Croats: Seeing War in Different Prisms | author= Alan Cowell | authorlink= Alan S. Cowell | date = 24 September 1991 | accessdate=28 December 2012}}</ref> the [[Siege of Dubrovnik]],<ref>{{cite book|ref=harv|title=Confronting the Yugoslav Controversies: A Scholars' Initiative|url=https://books.google.com/books?id=t0nYdgFrdG8C|editor=Charles W. Ingrao|editor2=Thomas Allan Emmert|chapter=The War in Croatia, 1991–1995|first=Mile|last= Bjelajac|first2=Ozren |last2=Žunec|publisher=[[Purdue University Press]]|year=2009|isbn= 978-1-55753-533-7|accessdate=29 December 2012|pages=249–250}}</ref> and the [[Battle of Vukovar]].<ref>{{cite news|newspaper=[[The New York Times]]|url=https://www.nytimes.com/1991/11/18/world/croats-concede-danube-town-s-loss.html| title=Croats Concede Danube Town's Loss|author=[[Chuck Sudetic]]|date=18 November 1991|accessdate=28 December 2012}}</ref> Even though the [[Sarajevo Agreement]] led to a ceasefire in most areas of Croatia,<ref>{{cite news|newspaper=The New York Times|url=https://www.nytimes.com/1992/01/03/world/yugoslav-factions-agree-to-un-plan-to-halt-civil-war.html|title=Yugoslav Factions Agree to U.N. Plan to Halt Civil War|author=[[Chuck Sudetic]]|date=3 January 1992|accessdate=16 December 2010}}</ref> the siege and fighting around Dubrovnik continued until May 1992.<ref>{{cite web|publisher=Dubrovački list|url=http://www.dulist.hr/kolumna/19795/teske-svibanjske-i-lipanjske-borbe|language=Croatian|title=Teške svibanjske i lipanjske borbe|trans_title=Heavy fighting in May and June|author=Varina Jurica Turk|date=2 May 2012|accessdate=2 February 2013}}</ref>

==Wartime history==
[[File:Ship Sveti Vlaho.JPG|left|thumb|''Sveti Vlaho'', the first vessel of the Armed Boats Squadron Dubrovnik is on a permanent exhibit in Dubrovnik]]
{{further|Siege of Dubrovnik}}
The Armed Boats Squadron Dubrovnik was established on 23 September 1991 as a volunteer unit of the [[Croatian Navy]].<ref>{{cite web|url=http://www.hrvatski-vojnik.hr/hrvatski-vojnik/3632011/vremeplov.asp|publisher=[[Hrvatski vojnik]]|author=Leon Rizmaul|date=26 September 1996|language=Croatian|title=Vremeplov|trans_title=Timeline|accessdate=26 January 2013}}</ref> It served during the Siege of Dubrovnik, and was a key factor in the successful defence of the city.<ref name="SD-SvVlaho">{{cite news| newspaper= [[Slobodna Dalmacija]]| language= Croatian| url= http://arhiv.slobodnadalmacija.hr/20011206/dubrovnik.htm| title= "Sveti Vlaho" u punom sjaju| trans_title= "Sveti Vlaho" in full glory| date=6 December 2001| accessdate=23 September 2011}}</ref><ref name="SD-SvVlaho2">{{cite news| newspaper= [[Slobodna Dalmacija]]| language= Croatian| url= http://arhiv.slobodnadalmacija.hr/20011206/dubrovnik.htm| title= Veteran opet na vezu| trans_title= Veteran docks again| date=6 December 2001| accessdate=23 September 2011}}</ref> The unit's first commander was [[Lieutenant Colonel]] [[Aljoša Nikolić]].<ref>{{cite news|newspaper=[[24sata (Croatia)]]|url=http://www.24sata.hr/news/dubrovcani-se-oprostili-od-pukovnika-hv-a-nikolica-182964|language=Croatian|title=Dubrovčani se oprostili od pukovnika HV-a Nikolića|trans_title=Dubrovnik bids farewell to Croatian Army Lieutenant Colonel Nikolić|date=20 July 2010|author=Mladen Gojun|accessdate=26 January 2013}}</ref>

Upon formation, squadron was tasked with breaching the Dubrovnik naval [[blockade]] and maintaining a route that was instrumental to delivery of materiel necessary to successfully defend the city, including food, fuel, medical supplies, arms and ammunition. The squadron also transported reinforcements arriving from other parts of Croatia and evacuated wounded out of Dubrovnik, and was a lifeline for the besieged troops and civilian population. Its operations began on 23 September 1991, when the [[Runabout (boat)|runabout]] ''Sveti Vlaho'' ({{lang-en|[[Saint Blaise]]}}) undertook its first voyage. The squadron consisted of 23 craft of various sizes and 117 volunteers.<ref name="HRT-ONB-20">{{cite web| publisher=[[Croatian Radiotelevision]]| language= Croatian| url= http://vijesti.hrt.hr/20-godina-odreda-naoruzanih-brodova| title= 20 godina Odreda naoružanih brodova| trans_title= 20 years of the Armed Boats Squadron| date=23 September 2011| accessdate=23 September 2011}}</ref> Craft armaments were light&mdash;the ''Sveti Vlaho'' had {{convert|6|mm|adj=on}} steel plate armour as a protection and a [[Bren light machine gun]]. All the vessels of the squadron were relatively fast, the fastest among them being capable of achieving {{convert|55|knots|abbr=off}}. The squadron typically deployed its craft in pairs or threes, sailing as close as possible to the Yugoslav Navy vessels when passing [[Koločep]] island to reach relative safety of the [[Rijeka Dubrovačka]] inlet, in order to utilize blind spots of naval guns.<ref name="ONB-Jutarnji">{{cite news| newspaper= [[Jutarnji list]]| language= Croatian| url= http://www.jutarnji.hr/blagdan-sv--nikole-dubrovnik-obiljezava-kao-dan-branitelja/234623/| title= Blagdan Sv. Nikole Dubrovnik obilježava kao Dan branitelja| trans_title= Feast of St. Nicholas marked by Dubrovnik as day of defenders| date=5 December 2007| author= Lidija Crnčević| accessdate=23 September 2011}}</ref><ref name="Oko-Vlaho">{{cite web| publisher= Portal Oko| url= http://www.portaloko.hr/clanak/kako-su-talijanski-svercerski-brodovi-u-pravim-rukama-spasili-dubrovnik-foto/0/17640/|archiveurl=https://web.archive.org/web/20120402210106/http://www.portaloko.hr/clanak/kako-su-talijanski-svercerski-brodovi-u-pravim-rukama-spasili-dubrovnik-foto/0/17640/|archivedate=2 April 2012| language= Croatian| title= Kako su talijanski švercerski brodovi u pravim rukama spasili Dubrovnik| trans_title= How did Italian smuggling boats end up in right hands to save Dubrovnik| date=19 August 2011| author= Ivana Brailo| accessdate=23 September 2011}}</ref><ref name="DL-lifeline">{{cite web| publisher= Dubrovački list| date=9 November 2011| url= http://www.dulist.hr/kolumna/15958/ljudi-velikog-znanja-i-jos-veceg-srca| language= Croatian|title=Ljudi velikog znanja i još većeg srca| trans_title= People of great knowledge and greater harts| accessdate=11 March 2013|author=Varina Jurica Turk}}</ref> During its existence, the squadron runabouts motored more than {{convert|52000|nmi|abbr=off}}, and transported approximately 6,000 troops and civilians, about 100 wounded and 2,000&nbsp;tonnes of various cargo.<ref name="HRT-ONB-20"/> The unit suffered two combat fatalities.<ref>{{cite news|newspaper=[[Slobodna Dalmacija]]|url=http://urednik.slobodnadalmacija.hr/Dubrovnik/tabid/75/articleType/ArticleView/articleId/149873/Default.aspx|language=Croatian|title=Odred naoružanih brodova; Prevezli tisuće vojnika i prevalili 40.000 kilometara|trans_title=Armed Boats Squadron transported thousands of soldiers and travelled 40,000 kilometres|date=23 September 2011|author=Ahmet Kalajdžić|accessdate=2 February 2013}}</ref>

At dawn on 31 October 1991, the squadron sailed out of Dubrovnik to meet the [[Libertas convoy]]&mdash;a fleet of civilian vessels, the largest among them being the [[Jadrolinija]] shipping line's ''Slavija'', which was endeavoring to deliver [[humanitarian aid]] to the city under siege. The fleet sailed from [[Rijeka]] and made several port calls, growing to 29 vessels as it approached Dubrovnik. The convoy, accompanied by [[List of heads of state of Yugoslavia|the last President of Yugoslavia]] [[Stjepan Mesić]] and the [[Prime Minister of Croatia]] [[Franjo Gregurić]], was stopped and searched by the [[Yugoslav Navy]] off the island of [[Mljet]] before the squadron linked up with the convoy and escorted it to Dubrovnik. The event marked the first large delivery of aid to the city since the beginning of the siege.<ref>{{cite news|newspaper=Dubrovački vjesnik|url=http://www.dubrovacki.hr/clanak/12489/|date=25 October 2009|title=Ususret obilježavanju 18. obljetnice Konvoja ''Libertas''|trans_title=Ahead of the 18th anniversary of the Libertas convoy|language=Croatian|accessdate=27 January 2013}}</ref>

{{Location map+
| Croatia
| float = right
| caption = Dubrovnik on the map of Croatia. RSK and Yugoslav Army-held area near Dubrovnik in early 1992 are highlighted red.
| overlay_image = Cro-occup-lines-Jan92.svg
| places = {{Location map~
 | Croatia
 | label = [[Dubrovnik]]
 | lat = 42.640278
 | long = 18.108333
 | region = HR
 | label_size = 75
 | marksize = 6
 | position = left
 }}
}}
''Sveti Vlaho'', the first naval vessel to fly the flag of the Republic of Croatia in combat and the first ship of the squadron, was originally an Italian smuggling runabout that was confiscated by the authorities, fitted with armour and used by the squadron in resupply and  blockade-running  operations. During one of these missions, while sailing back from a trip to [[Bol, Croatia|Bol]] on the Croatian island of [[Brač]], she was chased by a Yugoslav gunboat and driven aground on a beach near Babin Kuk, just {{convert|2|mi|abbr=off}} north of Dubrovnik. ''Sveti Vlaho'' was recovered and continued in service until 6 December 1991, when she was sunk at [[Gruž]] by a [[9K11 Malyutka]] [[anti-tank missile]]. The unit ceased operations in 1992 as the forces besieging Dubrovnik had been defeated.<ref name="SD-SvVlaho2"/><ref name="Oko-Vlaho"/> ''Sveti Vlaho'' was the second vessel used by the Croatian Navy, preceded only by a [[landing craft]] designated DJB-103, which was brought into use eight days earlier.<ref name="SD-DJB103">{{cite news| newspaper=[[Slobodna Dalmacija]]| language= Croatian| url= http://www.slobodnadalmacija.hr/Split/tabid/72/articleType/ArticleView/articleId/30907/Default.aspx| title= Naš prvi ratni brod šminka se za muzej| trans_title= Our first warship is tidying up for the museum| date=20 November 2008| author= Denis Krnić| accessdate=23 September 2011}}</ref> The squadron was disbanded on 29 December 1992.<ref>{{cite news|newspaper=Dubrovački vjesnik|url=http://dubrovacki.hr/clanak/32519/stoj-tko-ide-i-onda-je-zapucalo|language=Croatian|title=VIDEO: Stoj! Tko ide? I onda je zapucalo|trans_title=VIDEO: Stop! Who's approaching? And then shooting started|date=1 October 2011|accessdate=26 January 2013|author=Anton Hauswitschka}}</ref>

==Postwar decorations and heritage==
''Sveti Vlaho'' was refloated in 2001, restored and put on exhibit in Batala Park in Dubrovnik.<ref name="SD-SvVlaho"/><ref name="HRT-ONB-20"/> In May 2006, the squadron was collectively decorated with the [[Order of Nikola Šubić Zrinski]] for the bravery of its members during the Croatian War of Independence; however, no individual decorations were awarded to squadron members, nor were any promoted as a result of their service.<ref name="Decoration-ABS">{{cite news| newspaper= [[Narodne Novine]]| language= Croatian| url= http://narodne-novine.nn.hr/clanci/sluzbeni/127363.html| title= Odluka o odlikovanju Redom Nikole Šubića Zrinskog| trans_title= Decision on award of Order of Nikola Šubić Zrinski| date=22 May 2006| accessdate=23 September 2011}}</ref> On the 15th anniversary of the arrival of the Libertas convoy in besieged Dubrovnik a celebration was held, but no squadron volunteers were invited to attend.<ref name="15Libertas">{{cite news| newspaper= Jutarnji list| language= Croatian| url= http://www.jutarnji.hr/libertas-je-svijetu-pokazao-ratne-zlocine-u-dubrovniku/160308/| title= Libertas je svijetu pokazao ratne zločine u Dubrovniku| trans_title= Libertas exposed Dubrovnik war crimes to the world| date=30 October 2006| author= Goran Cvjetinović| accessdate=23 September 2011}}</ref> On 11 August 2011, surviving members of the squadron founded the Armed Boat Squadron Association whose objective is to preserve the heritage of the squadron, document its contribution to the Croatian War of Independence, and render assistance to its members and other similar associations in Croatia to preserve and promote the role that Croatian soldiers played in achieving the Republic of Croatia's independence.<ref name="DV-ABS-Udruga">{{cite news| newspaper= Dubrovački vjesnik| language= Croatian| url= http://www.dubrovacki.hr/clanak/31424/onb-20-godisnjicu-cemo-obiljeziti-23-rujna| title= ONB: 20. godišnjicu ćemo obilježiti 23. rujna| trans_title= ABS: The 20th anniversary shall be marked on 23 September| date=18 August 2011| accessdate=23 September 2011}}</ref>

{| class="plainrowheaders wikitable" style="background:none; cellspacing=2px; text-align:left; font-size:90%;"
|+Unit decorations<ref name="Decoration-ABS"/>
|- style="font-size:100%; text-align:right;"
! scope="col" | Ribbon !! style="width:400px;" scope="col" | Decoration 
|-
| [[File:Ribbon of an Order of Nikola Šubić Zrinski.png|60px]] || [[Order of Nikola Šubić Zrinski]]
|}

== References ==
{{Reflist|30em}}

{{good article}}

[[Category:Croatian Navy]]
[[Category:Military units and formations established in 1991]]
[[Category:Military history of Croatia]]
[[Category:Military units and formations of the Croatian War of Independence]]
[[Category:Maritime incidents in 1991]]
[[Category:Order of Nikola Šubić Zrinski recipients]]
[[Category:Military units and formations disestablished in 1992]]
[[Category:1991 establishments in Croatia]]