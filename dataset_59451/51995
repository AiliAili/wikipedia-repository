{{Infobox journal
| title = Journal of Organizational Behavior
| cover =
| formernames = Journal of Occupational Behavior
| discipline = [[Organizational behavior]]
| abbreviation = J. Occup. Behav.
| editor = Suzanne S. Masterson
| publisher = [[John Wiley & Sons]]
| country =
| frequency = 8/year
| history = 1980-present
| impact = 2.986
| impact-year = 2015
| openaccess =
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1379
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1379/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1379/issues
| link2-name = Online archive
| ISSN = 0894-3796
| eISSN = 1099-1379
| OCLC = 66937523
| CODEN = JORBEJ
| LCCN = 88648174
}}
The '''''Journal of Organizational Behavior''''' is a [[peer-reviewed]] [[academic journal]] published eight times a year by [[Wiley-Blackwell]]. The journal publishes [[empirical]] reports and [[theoretical]] reviews spanning the spectrum of [[organizational behavior]] research. It was established in 1980 as the ''Journal of Occupational Behavior'', obtaining its current title in 1988. The founding [[editor-in-chief]] was [[Cary Cooper]] ([[Manchester Business School]]), who was succeeded by [[Neal Ashkanasy]] ([[UQ Business School]]). The current editor-in-chief is Suzanne S. Masterson ([[University of Cincinnati]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-03-27}}</ref> [[Scopus]],<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/30020 |title=Source details: Journal of Organizational Behavior |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-03-27}}</ref> [[ProQuest]], [[Cambridge Scientific Abstracts]], [[EBSCO Information Services|EBSCO databases]], and [[Emerald Management Reviews]].<ref>{{Cite web |url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1379/homepage/ProductInformation.html |title=Abstracting and Indexing |accessdate=2012-06-20}}</ref> According to the ''[[Journal Citation Reports]]'', it has a 2015 [[impact factor]] of 2.986.<ref name=WoS>{{cite book |year=2016 |chapter=Journal of Organizational Behavior |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

== Best dissertation-based paper ==
The journal sponsors the [[Academy of Management]]'s Organizational Behavior Division's annual "Best Dissertation-Based Paper" prize, which recognizes one paper, based on a dissertation, which makes a significant contribution to the OB discipline.<ref>{{Cite web |url=http://www.obweb.org/index.php?option=com_content&view=article&id=55&Itemid=62 |title=Best Dissertation-Based Paper Award |accessdate=2012-11-23 |publisher=Organizational Behavior Division of the Academy of Management}}</ref>

== ''International Review of Industrial and Organizational Psychology'' ==
In 2012, it was announced that the ''International Review of Industrial and Organizational Psychology'' would be published as an annual review issue of the ''Journal of Organizational Behavior''. This issue will be co-edited by Gerard P. Hodgkinson ([[Warwick Business School]]) and J. Kevin Ford ([[Michigan State University]]).<ref>{{Cite web |url=http://onlinelibrary.wiley.com/doi/10.1002/job.1845/full#job1845-sec-0005 |title=IRIOP Annual Review Issue |accessdate=2013-01-25}}</ref>

==See also==
* [[Industrial and organizational psychology]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1379}}

[[Category:Publications established in 1980]]
[[Category:Business and management journals]]
[[Category:English-language journals]]
[[Category:John Wiley & Sons academic journals]]