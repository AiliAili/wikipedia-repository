{{Use Irish English|date=May 2013}}
{{Use dmy dates|date=May 2013}}
{{Infobox GAA tournament
| name                 = All-Ireland Higher Education Gaelic Football Championship
| image                = 
| caption              = 
| irish                = Trófaí Hodges Figgis
| code                 = Gaelic Football
| founded              = 1976
| abolished            = 1983/84
| region               = 
| trophy               = '''Hodges Figgis Trophy'''
| teams                = Sigerson Cup Winner v Trench Cup Winner
| firstwin             = National College of Physical Education, Limerick (now [[University of Limerick]])
| title holders        = Northern Ireland Polytechnic (now [[University of Ulster Jordanstown]])
| most titles          = N.C.P.E./Thomond College Limerick (now [[University of Limerick]])
| mostordinal          = 3
| sponsors             = 
}}

The '''Hodges Figgis Trophy''' was presented to the winner of the All-Ireland Higher Education Senior [[Gaelic Football]] Championship. The annual match was played between the [[Sigerson Cup]] champion and the [[Trench Cup]] Champion.<ref name="McAnallen">''The Cups That Cheered: A History of the Sigerson, Fitzgibbon and Higher Education Championships'', Dónal McAnallen, 2012, The Collins Press, Cork</ref>

==History==
The Higges Figgis Trophy was presented in 1976 to the Comhairle Ard Oideachais (CAO) Cumann Lúthchleas Gael, the Higher Education Council of the [[Gaelic Athletic Association]], by Mr Allen Figgis of the Dublin-based book retailing chain Hodges Figgis.<ref name="Figgis">''The Irish Times'', 7 April 1976, p. 4</ref> The trophy was a silver replica of an open book, measuring 14 inches by 8 inches, mounted on a wooden base.<ref name="McAnallen"/><ref name="Figgis" /> The trophy was competed for between the winners of the [[Sigerson Cup]] and the [[Trench Cup]] and marked the culmination of the CAO's programme of Gaelic football championships within Third-Level institutions. It was widely seen among students as a battle between the universities (Sigerson Cup) and the non-university third-level institutions (Trench Cup). The Trench Cup was inaugurated in the 1975-76 season to cater for the third-level colleges which did not compete in the Sigerson Cup championship, inaugurated in 1911. The inaugural Hodges Figgis Cup match took place at Croke Park, Dublin in April 1976 between St Patrick's College, Maynooth (now [[NUI Maynooth]]), who beat [[University College Dublin]] in the Sigerson Cup final, and the National College of Physical Education, Limerick, who beat Coláiste Phádraig, Drumcondra in the inaugural Trench Cup final. Both teams were star-studded with county GAA players. N.C.P.E. Limerick became the inaugural Hodges Figgis Trophy winners.<ref>''The Irish Independent'', 12 April 1976, p. 8</ref> In 1983 Northern Ireland Polytechnic (now [[University of Ulster Jordanstown]]) won their match against [[University College Galway]] to lift the Hodges Figgis Trophy. The valuable trophy was lost.<ref name="McAnallen" /> With no trophy to play for, the Sigerson v Trench Cup winners series ceased. In 1978 the Hodges Figgis chain of bookshops, long owned by the Figgis family, was acquired for £210,000 by the U.K. bookselling company, Pentos Retailing Ltd.<ref>''The Irish Times'', 28 July 1978, p. 12; ''Irish Press'', 14 June 1979, p. 9</ref>

==Roll of Honour==
{|class="wikitable"
|- style="background:#efefef"
! Year
! Winners
! Cup
! Score
! Finalists
! Cup
! Score
! Where Played
! Date
! Referee
! County
|- align="center"
| 1983
| Northern Ireland Polytechnic
| Trench
| 
| University College Galway
| Sigerson
| 
| [[Croke Park]], Dublin
| 
| 
| 
|- align="center"
| 1982
| Thomond College<ref name="Thomond">''Irish Independent'', 24 March 1982, p. 14; ''The Irish Times'', 31 March 1982, p. 4; ''Irish Press'', 31 March 1982, p. 18; ''Irish Independent'', 31 March 1982, p. 17</ref>
| Trench
| 0-9
| Queen's University Belfast
| Sigerson
| 0-6
| [[Parnell Park]], Dublin
| 30 March 1982
| M Forde
| [[Dublin GAA|Dublin]]
|- align="center"
| 1981
| University College Galway<ref>''The Irish Times'', 4 May 1981, p. 3 (reported result as UCG 1-13 Sligo RTC 0-06, with scorers); Irish Press, 6 May 1981, p. 15 (reported result as UCG 1-13 Sligo RTC 0-06); ''Irish Independent'', 6 May 1981, p. 13 (reported result as UCG 1-12 Sligo RTC 0-06);  Connacht Tribune, 8 May 1981, p. 16 & 18 (reported result as UCG 1-13 Sligo RTC 0-08)</ref>
| Sigerson
| 1-13
| Sligo RTC
| Trench
| 0-6
| Dr Hyde Park, Roscommon
| 3 May 1981
| 
| 
|- align="center"
| 1980
| Northern Ireland Polytechnic<ref name="Poly">''The Irish Times'', 7 April 1980, p. 3</ref>
| Trench 
| 2-11
| University College Galway
| Sigerson
| 1-6
| [[Croke Park]], Dublin
| 6 April 1980
| K Greene
| [[Dublin GAA|Dublin]]
|- align="center"
| 1979
| UCD v Cork RTC
| 
| 
| 
| 
| 
| 
| 26 May-6 June 1979?
| 
| 
|- align="center"
| 1978
| UCD 
| Sigerson
| 
| St Joseph's TC Belfast
| Trench
| 
| Knockbridge, County Louth
| 4 May 1978
| 
| 
|- align="center"
| 1977
| Thomond College, Limerick<ref name="Thomond2">''Irish Independent'', 27 April 1977, p. 14; ''Irish Press'', 27 April 1977, p. 16</ref>
| Trench
| 1-12
| University College Dublin
| Sigerson
| 1-2
| Durrow, County Laois
| 26 April 1977
| M. Meally
| [[Kilkenny GAA|Kilkenny]]
|- align="center"
| 1976
| National College of Physical Education<ref name="NCPE">''Irish Independent'', 12 April 1976, p. 8; ''The Irish Times'', 12 April 1976, p. 3</ref>
| Trench
| 2-16
| St Patrick's College, Maynooth
| Sigerson
| 0-5
| [[Croke Park]], Dublin
| 11 April 1976
| M. Meally
| [[Kilkenny GAA|Kilkenny]]

|}

==Team Sheets==
{|class="wikitable"
|- style="background:#efefef"
! Year
! Winning Team
! Finalist Team
|- align="left"
| 1983
| '''Northern Ireland Polytechnic''': ''Eugene Young (Derry), Aidan Browne (Down), Colm Harney (Armagh), D. Durkin, Dominic O'Hanlon (Down), Dominic Corrigan (Fermanagh), Anthony McArdle (Armagh)''
| '''University College Galway''': ''James Reidy (Mayo), Seán Twomey, Peter Forde (Mayo), T. McWalter, Jimmy Egan, Tomás Tierney (Galway), Hugh Heskin, Richie Lee (Galway), Michael Brennan, Pádraic "Dandy" Kelly, Brian O'Donnell (Galway), Tommy Carr (Tipperary), Anthony Finnerty, M. Clarke, Páraic Duffy (Mayo)''
|- align="left"
| 1982
| '''Thomond College, Limerick''':<ref name="Thomond"/> Tommy Doonan (Cavan), Michael McBrearty (Donegal), Brian Ladden (Kerry), Mick Loughnane (Clare), Enda Timoney (Monaghan), Mark Kavanagh (0-01, Laois), Éamonn Quigley (Dublin), Pat Roe (Laois), Tommy Sheehy (Kerry), Pat Critchley (0-01, Laois), Pauric Gallagher (0-04, Donegal), Jimmy Keogh (Dublin), Paul Marron (0-02, Offaly), Séamus Meehan (Donegal), Mick O'Donovan (0-01, Cork), Sub: Brian Looney (Kerry) for Enda Timoney, Aengus Murphy (Galway) for Séamus Meehan [Other panel members: Frank Walsh (Dublin), Declan O'Leary (Cork), Paul Marron (Offaly), Larry Lynam (Offaly), Mick Doyle (Cork), Kevin O'Brien (Cork), Donncha McNeillis (Donegal), Séamus Reilly (Cavan), Eugene O'Riordan (Cork)]
| '''Queen's University Belfast''': Paddy Mahon (Down), Joe Ferran (Armagh), Seán Gordon (Armagh), B. Downey (Antrim), Donagh O'Kane(Down), Séamus Boyd (Antrim), L. Keegan, John McAleenan (Down), Seán McAuley (0-02, Antrim), Dermot Dowling (Armagh), Joey Donnelly (Armagh), Aidan Short (Armagh), Brian McErlain (Derry), Éamon Larkin (0-02, Down), Dónal Armstrong (0-01, Antrim), Subs: Seán Leonard (0-01, Fermanagh) for Brian McErlain, Brendan Rafferty for Joey Donnelly
|- align="left"
| 1981
| '''University College Galway''': Gay McManus (0-05, Galway), B. O'Connell (0-04), Pádraic Mitchell (1-00, Galway), Michael "Micksey" Clarke (0-02, Westmeath), T.J. Kilgallon (0-01, Mayo), Anthony Finnerty (Mayo), Kieran O'Malley (0-01, Mayo), Ritchie Lee (Galway), Séamus McHugh (Galway), Seán Forde (Galway), Des Bergin (Kildare), Pádraig "Oxie" Moran (Galway)
| '''Sligo RTC''': S. Duignan (0-04), Gerry Smith (0-01), Mick McHugh (0-01), Shane Durkin (Sligo), Sean Clarke (Sligo), Jim Watters
|- align="left"
| 1980
| '''Northern Ireland Polytechnic''':<ref name="Poly"/><ref>''Irish Independent'', 2 April 1980, p. 14</ref> Pat Doonan (Down), Eamonn McMorrow (Fermanagh), Michael McNally (Cavan), L. McCreesh, James McCartan (Down), Mick Sands (Down), S. Gallagher, Eugene Young (Derry), James Devlin (0-01, Tyrone), J. Gunn, P. Walsh (0-03), Martin McCann (0-04, Down), P.J. O'Hare (1-00, Antrim), Kevin McCabe (1-02, Tyrone), Séamus Fearon (Down), Subs: G. Skelton (Tyrone) for Séamus Fearon, H. McGuirk (0-01, Armagh) for P.J. O'Hare
| '''University College Galway''': Gay Mitchell (Galway), Joe Kelly (Galway), C. McCutchen, Seán Liskin (Mayo), J. Costelloe (Mayo), Pádraig Monaghan (Mayo), Séamus McHugh (Galway), Richie Lee (0-02, Galway), T.J. Kilgallon (Mayo), Pádraic Mitchell (Galway), Gay McManus (1-01, Galway), Pat O'Brien (0-01, Mayo), Michael "Micksey" Clarke (0-01, Westmeath), Seán Forde (0-01, Galway), P. O'Riordan, Sub: Tom McHugh (Galway) for Michael "Micksey" Clarke
|- align="left"
| 1979
| U.C.D. v Cork RTC
|
|- align="left"
| 1978
| '''U.C.D.''': Paddy O'Donoghue (Kildare), Mick Carty (Wexford), Pat O'Neill (Dublin), Michael Hickey (Dublin), Gerry McCaul (Dublin), Jackie Walsh (Kerry), Gerry McEntee (Meath), Capt., Mick Hickey (Dublin), Mick Fennelly (Kildare), Vincent O'Connor (Kerry). Mickey "Ned" O'Sullivan (Kerry)
| '''St. Joseph's Training College, Belfast''': ''Liam Austin (Down), Willie McKenna (Tyrone), Peter McGinnity (Fermanagh), Dessie McKenna (Tyrone), Barry Campbell (Tyrone), Phil McElwee, P. Kane, Jim McGuinness (Antrim), Sean McGourity (Antrim), Micky Darragh (Antrim), Dan Morgan, Peter Trainer (Armagh), Phil McElwee''
|- align="left"
| 1977
| '''Thomond College''':<ref name="Thomond2"/> Brian Murtagh (Westmeath), Michael Houston (Donegal), Seán O'Shea (Longford), Eddie Mahon (Meath), Martin Connolly (Mayo),  Brian Talty (Galway), Denis O'Boyle (1-00, Mayo), Tony Harkin (Donegal), Michael Spillane (0-02, Kerry), Jimmy Dunne (0-02, Offaly), Richie Bell (0-01, Mayo), Gerry Dillon (0-02, Kerry), Michael Kilcoyne (Westmeath), Pat Spillane (0-04, Kerry), Capt., Johnny O'Connell (0-01, Kerry), Sub: Declan Smyth (Galway) for Michael Kilcoyne
| '''U.C.D.''': Ivan Heffernan (Mayo), Dave Billings (Dublin), Bernie Jennings (Mayo), Seán Hunt (Roscommon), P.J. O'Halloran (Meath), Mick Carthy (Wexford), Pat O'Neill (Dublin), Gerry McEntee (Meath), Ogie Moran (Kerry), A. King (Cavan), Tony McManus (Roscommon), J.P. Kean (1-01, Mayo), M. Flannery (Mayo), Ger Griffin (Kerry), Tommy Murphy (0-01, Wicklow)
|- align="left"
| 1976
| '''N.C.P.E.''':<ref name="NCPE"/> Tony Owens (Cork), Michael Houston (Donegal), Liam Fardy (Wexford), Eddie Mahon (Meath), Joe Mulligan (Offaly), Hugo Clerkin (Monaghan), Michael Spillane (Kerry), Brian Mullins (1-02, Dublin), Capt., Fran Ryder (Dublin), Declan Smyth (0-03, Galway),  Richie Bell (0-03, Mayo), Pat Spillane (1-01, Kerry), Jimmy Dunne (0-01, Offaly),  Brian Talty (0-02, Galway), John Tobin (0-04, Galway), Sub: Ger Dillon (Kerry) for John Tobin [Other panel members: Brian Murtagh (Westmeath), Tony Harkin (Donegal), Larry McCarthy (Cork), Martin Connolly (Mayo), Mick Caulfield (Wexford), Dick Dunne (Laois)]  
| '''S.P.C., Maynooth''': Jack Fitzgerald (Cork), Anthony "Tony" O'Keeffe (Kerry), Dan O'Mahony (Mayo), Capt., Tom Barden (Longford), Liam Kelly (Kerry), Michael McElvaney (Longford), John Clarke (Westmeath), Eamon Whelan (Laois), Patrick Henry (Sligo), Declan Brennan (0-01, Sligo), Martin Nugent (0-01, Offaly), Patrick "Paddy" McGovern (0-01, Cavan), Patrick "Pat" Donnellan (Galway), John McParland (0-01, Down), Peter Burke (0-01, Longford) - Sub: Seán McKeown (Kildare) for Paddy McGovern [Other panel members: L. White (Galway) S. O'Mahony (Sligo), Mick Marren (Sligo), Seán Hegarty (Armagh), F. Murray (Longford)]
|}

==Winning Captains==
{|class="wikitable"
|- style="background:#efefef"
! Year
! Player
! College
! County
|- align="center"
| 1983
| Anthony McArdle<ref name="PC">Personal Communication, John "Tommy-Joe" Farrell, UU Jordanstown GAA Hon Secretary 1981, Chairman of CAO 1987-89</ref>
| Northern Ireland Polytechnic
| Armagh
|- align="center"
| 1982
| 
| Thomond College, Limerick
| 
|- align="center"
| 1981
| Gay McManus<ref name="Sigerson">[[Sigerson Cup]]</ref>
| University College Galway
| Galway
|- align="center"
| 1980
| [[Kevin McCabe (Gaelic footballer)|Kevin McCabe]]
| Northern Ireland Polytechnic
| Tyrone
|- align="center"
| 1979
| 
| 
| 
|- align="center"
| 1978
| [[Gerry McEntee]]<ref name="Sigerson">[[Sigerson Cup]]</ref><ref>''Irish Independent'', 3 May 1978, p. 16</ref>
| University College Dublin
| Meath
|- align="center"
| 1977
| [[Pat Spillane]]<ref>''Irish Independent'', 29 March 1977, p. 10; ''Irish Independent'', 26 April 1977, p. 18</ref>
| Thomond College, Limerick
| Kerry
|- align="center"
| 1976
| [[Brian Mullins]]<ref name="Figgis"/><ref>''Irish Independent'', 7 April 1976, p. 13</ref>
| National College of Physical Education
| Dublin
|}

==References==
{{Reflist|2}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Gaelic football competitions]]