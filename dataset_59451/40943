{{Infobox military person
|name=Cornelius C. Smith
|birth_date={{birth date|1869|4|7}}
|death_date={{death date and age|1936|1|10|1869|4|7}}
|birth_place=[[Tucson]], [[Arizona Territory]], [[United States]]
|death_place=[[Riverside, California]]
|placeofburial=[[Evergreen Cemetery (Riverside, California)|Evergreen Memorial Park]]
|placeofburial_label=Place of burial
|image=File-Cornelius C. Smith.jpg
|caption=Cornelius C. Smith displaying his Medal of Honor.
|nickname=
|allegiance=[[United States|United States of America]]
|branch=[[United States Army]]
|serviceyears=1889–1920
|rank=[[Colonel#United States|Colonel]]
|commands=[[Philippine Constabulary]]<br>[[5th Cavalry Regiment (United States)|5th U.S. Cavalry]]<br />[[10th Cavalry Regiment (United States)|10th U.S. Cavalry]]
|unit=[[4th Cavalry Regiment (United States)|4th U.S. Cavalry]]<br />[[2nd Cavalry Regiment (United States)|2nd U.S. Cavalry]]<br />[[6th Cavalry Regiment (United States)|6th U.S. Cavalry]]
|battles=[[American Indian Wars|Indian Wars]]
* [[Sioux Wars]]
[[Spanish–American War]]<br />[[Philippine Insurrection]]<br />[[World War I]]
|relatives=Gilbert Cole Smith (father)<br>[[Cornelius Cole Smith, Jr.]] (son)
|awards=[[Medal of Honor]]
|laterwork=
}}
[[Colonel#United States|Colonel]] '''Cornelius Cole Smith''' (April 7, 1869 – January 10, 1936) was an [[United States|American]] officer in the [[United States Army|U.S. Army]] who served with the [[6th Cavalry Regiment (United States)|6th U.S. Cavalry]] during the [[Sioux Wars]]. On January 1, 1891, he and four other cavalry troopers successfully defended a U.S. Army supply train from a force of 300 Sioux warriors at the [[White River (Missouri River)|White River]] in [[South Dakota]], for which he received the [[Medal of Honor]]. He was the last man to receive the award in battle against the Sioux, and in a major [[American Indian Wars|Indian war]].

In his later career, Smith served as an officer during the [[Spanish–American War]] and the subsequent [[Philippine–American War|Philippine Insurrection]] under Generals [[Leonard Wood]] and [[John J. Pershing]]. In 1910, he was appointed by Pershing as commander of the [[Philippine Constabulary]] and served at [[Fort Huachuca]] as commanding officer of Troop G, [[5th Cavalry Regiment (United States)|5th U.S. Cavalry]] from 1912 to 1914. It was in this capacity that he accepted the surrender of Colonel [[Emilio Kosterlitzky]], commander of [[Mexican Army|Mexican federal forces]] at [[Sonora]], on March 13, 1913. In 1918, he was appointed commander of Huachuca and the [[10th Cavalry Regiment (United States)|10th U.S. Cavalry]]. Prior to his retirement, he also oversaw the construction of [[Biggs Army Airfield|Camp Owen Beirne]], adjacent to [[Fort Bliss]], which served as the model for similar camps built following the end of [[World War I]].

Smith's son, [[Cornelius Cole Smith, Jr.]], who also served as a colonel in the Philippines during [[World War II]],<ref name="Stokely">{{cite news |title=Pearl Harbor veteran, author, sculptor dies |author=Stokely, Sandra |url=http://www.pe.com/localnews/riverside/stories/PE_News_Local_corny06.a1e24.html |newspaper=[[The Press-Enterprise (California)|The Press-Enterprise]] |date=May 6, 2004 |accessdate=December 29, 2010}}</ref> was a successful author, historian and illustrator who wrote several books on the [[Southwestern United States]] including his [[biography]] entitled "Don't Settle for Second: Life and Times of Cornelius C. Smith" (1977).

==Biography==

===Early life===
Cornelius Cole Smith was born on April 7, 1869, in the frontier town of [[Tucson]] in the [[Arizona Territory]]. A member of a distinguished military family dating back to the [[American Revolutionary War|Revolutionary War]],<ref name="Stokely" /> his father, Gilbert Cole Smith, had served an officer in the [[Union Army]]'s famed [[California Column]] during the [[American Civil War]] and later became [[Quartermaster#United States Army|quartermaster]] at [[Fort Lowell]] in Tucson.<ref name="UAP">{{cite journal |editor=[[University of Arizona]] |year=1978 |title=Review of "Don't Settle for Second: Life and Times of Cornelius C. Smith" |journal=Arizona and the West |volume=20 |issue=2 |pages=89–90 |publisher=University of Arizona Press |location=[[Tucson, Arizona]] |accessdate=December 28, 2010}}</ref><ref name="Finley">{{cite journal |editor=Finley, James P. |year=1996 |title=Roll Call: Col. Cornelius C. Smith |journal=Huachuca Illustrated |volume=2 |issue=2 |publisher=Huachuca Museum Society |location=[[Fort Huachuca, Arizona]] |url=http://net.lib.byu.edu/estu/wwi/comment/huachuca/HI2-11.htm |accessdate=December 28, 2010}}</ref> He was also related to brothers William and [[Granville Henderson Oury|Granville H. Oury]]. His family lived at several outposts in the Arizona and [[New Mexico Territory|New Mexico Territories]], wherever his father happened to be stationed, until December 1882 when they finally settled at [[Vancouver Barracks]] in the [[Washington Territory]]. Smith was then sent back east to [[Louisiana, Missouri]], and later to [[Baltimore, Maryland]] in 1884. Four years later, Smith moved to [[Helena, Montana]] where he joined the [[Montana National Guard]] on May 22, 1889.<ref name="Thrapp">Thrapp, Dan L. ''Encyclopedia of Frontier Biography: P–Z''. Vol. III. Norman: University of Nebraska Press, 1991. (pg. 1324) ISBN 0-8032-9420-4</ref><ref name=mtMOH>{{cite web|title=125 Montana Newsmakers: Medal of Honor Winners|url=http://www.greatfallstribune.com/multimedia/125newsmakers4/medal1.html|work=Great Falls Tribune|author=Tribune Staff|accessdate=August 27, 2011}}</ref>
[[File:Lt. Cornelius C. Smith 1895.jpg|thumb|Smith poses with his favorite horse "Blue" in front of his quarters at [[Fort Wingate]] in 1895.]]

At age 21, Smith followed in his father's footsteps by enlisting in the [[United States Army]] in [[Helena, Montana]] on April 9, 1890, and was immediately sent out with [[6th Cavalry Regiment (United States)|6th U.S. Cavalry Regiment]] for frontier duty in the [[Dakota Territory]].<ref name="UAP"/><ref name="Finley"/><ref name="MOH">Senate Committee on Veterans Affairs. ''Medal of Honor recipients, 1863–1973, 93rd Cong., 1st sess''. Washington, DC: US Government Printing Office, 1973. (pg. 228)</ref><ref name="Hannings">Hannings, Bud. ''A Portrait of the Stars and Stripes''. Glenside, Pennsylvania: Seniram Publishing, 1988. (pg. 400) ISBN 0-922564-00-0</ref><ref name="O'Neal">O'Neal, Bill. ''Fighting Men of the Indian Wars: A Biographical Encyclopedia of the Mountain Men, Soldiers, Cowboys, and Pioneers Who Took Up Arms During America's Westward Expansion''. Stillwater, Oklahoma: Barbed Wire Press, 1991. (pg. 35) ISBN 0-935269-07-X</ref><ref name="MilitaryTimes">{{cite web |url=http://militarytimes.com/citations-medals-awards/recipient.php?recipientid=528|title=Military Times Hall of Valor: Cornelius Cole Smith
|accessdate= December 29, 2010 |author=Army Times Publishing Company |work=Awards and Citations: Medal of Honor |publisher=MilitaryTimes.com }}</ref><ref name="HomeofHeroes">{{cite web |url=http://www.homeofheroes.com/moh/citations_1865_ind/smith_cornelius.html |title=MOH Citation for Cornelius Smith |accessdate= December 29, 2010 |author=Sterner, C. Douglas |year=1999 |work=MOH Recipients: Indian Campaigns |publisher=HomeofHeroes.com }}</ref><ref name="HomeofHeroes2">{{cite web |url=http://www.homeofheroes.com/gravesites/states/pages_pz/smith_cornelius_ca.html |title=Photo of Grave site of MOH Recipient |accessdate= December 29, 2010 |author=Sterner, C. Douglas |year=1999 |work=Medal of Honor Recipient Gravesites In The State of California |publisher=HomeofHeroes.com }}</ref>

===Battle at White River===
Within a year, Smith reached the rank of [[Corporal#United States Army|corporal]] and saw his first action during the [[Sioux Wars#Ghost Dance Wars|Pine Ridge Campaign]]. On January 1, 1891, two days after the [[Battle at Wounded Knee Creek]], he accompanied a fifty-three man escort of a U.S. Army supply train to the regiment's camp at the battle site. While preparing to cross the [[White River (Missouri River)|White River]], partially ice-covered during the winter, the supply train was suddenly attacked by a group of approximately 300 Sioux braves. In an attempt to save the wagon train, he and Sergeant Frederick Myers chose advanced positions from a [[Hillock|knoll]] 300 yards from the river and held back the initial Sioux assault with four other troopers successfully defended their position against repeated enemy attacks.<ref name="Beyer">Beyer, Walter F. and Oscar Frederick Keydel, ed. ''Deeds of Valor: From Records in the Archives of the United States Government; how American Heroes Won the Medal of Honor; History of Our Recent Wars and Explorations, from Personal Reminiscences and Records of Officers and Enlisted Men who Were Rewarded by Congress for Most Conspicuous Acts of Bravery on the Battle-field, on the High Seas and in Arctic Explorations''. Vol. 2. Detroit: Perrien-Keydel Company, 1906. (pg. 327, 330)</ref> After they had withdrawn, Smith and the others chased after the war party for a considerable distance before breaking off their pursuit.<ref name="Thrapp"/><ref name="MOH"/><ref name="MilitaryTimes"/><ref name="HomeofHeroes"/><ref name="HomeofHeroes2"/>

Smith's actions at White River prevented the Sioux from capturing the supply wagons and he was cited for distinguished bravery in the face of a numerically superior enemy force. For his actions he received the [[Medal of Honor]]<ref name="Thrapp"/><ref name="MOH"/><ref name="Hannings"/><ref name="O'Neal"/><ref name="Beyer"/> on February 4, 1891.<ref name="MilitaryTimes"/><ref name="HomeofHeroes2"/>

===Service in Cuba and the Philippines===
[[File:Major Cornelius C. Smith 1904.jpg|left|thumb|Smith as a captain with the 14th U.S. Cavalry in the Philippines during 1904.]]

The following year, on November 19, 1892, Smith was made a commissioned officer as a [[Second Lieutenant#United States|second lieutenant]] with the [[2nd Cavalry Regiment (United States)|2nd U.S. Cavalry]]. He later fought in [[Cuba]] during the [[Spanish–American War]] and in [[the Philippines]] during the [[Philippine Insurrection|Filipino]] and [[Moro Rebellion]]s under Generals [[Leonard Wood]] and [[John J. Pershing]] respectively. A captain with the [[14th Cavalry Regiment (United States)|14th U.S. Cavalry]],<ref name="RDS">[[United States Department of State]]. ''Register of the Department of State, December 17, 1917''. Washington, DC: Government Printing Office, 1918. (pg. 138)</ref> he served in [[Mindanao]] under General Wood from 1903 to 1906, during which time he helped publish ''A Grammar of the Maguindanao Tongue According to the Manner of Speaking It in the Interior and on the South Coast of the Island of Mindanao'' (1906) with Spanish Jesuit Rev. Father Jacinto Juanmart, before accepting a two-year position as [[Administrator (business)|superintendent]] of [[California]]'s [[Sequoia National Park|Sequoia]] and [[General Grant Grove|Grant National Parks]] in 1908. He returned to the Philippine Islands in 1910<ref name="Thrapp"/> where he was appointed commander of the [[Philippine Constabulary]] by General Pershing.<ref name="UAP"/><ref name="Finley"/>

===Fort Huachuca and World War I===
[[File:Major Cornelius C. Smith 1910.jpg|thumb|Smith (far right) as commander of the [[Philippine Constabulary]] with Brig. Gen. [[John J. Pershing]] and [[Moro people|Moro chieftains]] in 1910. Smith participated in expeditions against the Moro rebels for much of his time in the Philippines.]]
After a nine-year tour of duty in the Philippines, Smith was brought back to the U.S. in 1912 and transferred to the [[4th Cavalry Regiment (United States)|4th U.S. Cavalry Regiment]] at [[Fort Huachuca]], in his home state of [[Arizona]], that fall.<ref name="RDS"/> He remained at Huachuca when the 4th U.S. Cavalry was sent for [[job rotation|rotation]] to Hawaii and was reassigned to the [[5th Cavalry Regiment (United States)|5th U.S. Cavalry]] serving as commanding officer of the 5th U.S. Cavalry's Troop G from December 1912 to December 1914. It was while with the 5th Cavalry that, on March 13, 1913, he formally accepted the surrender of Colonel [[Emilio Kosterlitzky]], commander of [[Mexican Army|Mexican federal forces]] of [[Sonora]], and his 209 followers in [[Nogales, Arizona]] following his defeat by General [[Álvaro Obregón]] days earlier.<ref name="UAP"/><ref name="Finley"/><ref name="Thrapp"/> The surrender was conducted in a formal ceremony, with Kosterlitzky presenting Smith his sword, and the two officers later became lifelong friends.<ref>Trimble, Marshall. ''Arizona: A Panoramic History of a Frontier State''. New York: Doubleday and Company, 1977. ISBN 0-385-14064-9</ref>

A [[military attaché]] in [[Bogota]] and [[Caracas]] for a time during 1915, he rose through the ranks from major to colonel of cavalry within the next two years.<ref name="UAP"/><ref name="Finley"/><ref name="Thrapp"/><ref name="RDS"/> Smith trained several regiments during [[World War I]] but was denied further promotion and a field command in the Europe due to the feud between General Pershing, then commander-in-chief of the [[American Expeditionary Forces]] on the [[Western Front (World War I)|Western Front]], and [[Chief of Staff of the United States Army|Army Chief-of-Staff]] [[Peyton C. March|Peyton March]]. In 1918, he returned to Fort Huachuca where he assumed command of the post and the [[10th Cavalry Regiment (United States)|10th U.S. Cavalry Regiment]]. His last assignment was at [[Fort Bliss]] where he had [[Biggs Army Airfield|Camp Owen Beirne]] built, the model for similar bases constructed for servicemen following World War I, before his retirement in 1920 at the rank of [[Colonel#United States|colonel]].<ref name="UAP"/><ref name="Finley"/>

===Retirement and later years===
Smith taught [[military science]] and [[military tactics|tactics]] at the [[University of Arizona]] after leaving the U.S. Army and was later hired as a [[technical advisor]] for [[war film]]s in [[Hollywood]]. In 1928, he became a member of the American Electoral Committee which oversaw the [[Nicaraguan general election, 1928|presidential elections]] in [[Nicaragua]].<ref name="UAP"/> That same year, he was also a contributing editor for Alice Baldwin's [[biography]] on her late husband Major General [[Frank Baldwin]], ''Memoirs of the late Frank D. Baldwin, Major General, U.S.A.'', in which Smith related his experiences with Baldwin during the Spanish–American War. He went on to become a prolific author of articles relating to the [[American Old West|American frontier]] in the [[Southwestern United States]]. Smith died in [[Riverside, California]] on January 10, 1936, at the age of 66.<ref name="Thrapp"/><ref name="HomeofHeroes2"/> He is buried at [[Evergreen Cemetery (Riverside, California)|Evergreen Memorial Park and Mausoleum]],<ref>[http://www.evergreen-cemetery.info/founders.php?id=22319 Evergreen Memorial Historic Cemetery - Founders' Stories]</ref><ref>{{cite news |title=Evergreen Memorial gets a spruce up |author=Haberman, Doug |url=http://www.pe.com/localnews/inland/stories/PE_News_Local_R_revergreen18.5140a3b.html |newspaper=[[The Press-Enterprise (California)|The Press-Enterprise]] |date=January 18, 2008|accessdate=December 29, 2010}}</ref> along with 1,000 other veterans.<ref name="Haberman">{{cite news |title=Ever green |author=Haberman, Doug |url=http://www.pe.com/localnews/riverside/stories/PE_News_Local_cemetery20.f0ca.html |newspaper=[[The Press-Enterprise (California)|The Press-Enterprise]] |date=January 20, 2005 |accessdate=December 29, 2010}}</ref>

==Grave site restoration==
In November 2003, a special ceremony was held at Smith's grave site to display a new 18-foot-tall flagpole and stone bench nearby. Smith's son, [[Cornelius Cole Smith, Jr.]], was in attendance. Initially started by 16-year-old Michael Emett for an [[Eagle Scout (Boy Scouts of America)|Eagle Scout]] project, this was the first of a planned restoration campaign for the graves of Riverside's military veterans and town founders. The story was covered by ''[[The Press-Enterprise]]'' and encouraged community leaders to raise money for an endowment to provide for the upkeep of older rundown areas of the cemetery that are not watered or maintained.<ref name="Paul">{{cite news |title=Scout's project honors vet |author=Paul, Jaqcuie |url=http://www.pe.com/localnews/riverside/stories/PE_NEWS_nrspot14.57b22.html |newspaper=[[The Press-Enterprise (California)|The Press-Enterprise]] |date=November 14, 2003 |accessdate=December 29, 2010}}</ref> A few years before, the [[California Department of Consumer Affairs]] ordered that sprinklers in the historic section be shut off because it lacked an [[Financial endowment|endowment]] to pay for the water.<ref name="Haberman"/><ref>{{cite news |title=Cemetery volunteers help clean, raise funds |author=Mendez, Lys |url=http://www.pe.com/localnews/riverside/stories/PE_News_Local_R_rcemetery03.3d962a5.html |newspaper=[[The Press-Enterprise (California)|The Press-Enterprise]] |date=June 2, 2006 |accessdate=December 29, 2010}}</ref>

==Medal of Honor citation==
Rank and organization: Corporal, Company K, 6th U.S. Cavalry. Place and date: Near White River, S. Dak., 1 January 1891. Entered service at: Helena, Mont. Birth: Tucson, Ariz. Date of issue: 4 February 1891.

'''Citation:'''

<blockquote>With 4 men of his troop drove off a superior force of the enemy and held his position against their repeated efforts to recapture it, and subsequently pursued them a great distance.<ref name="AMOHW">{{cite web |accessdate=June 29, 2009|url=http://www.history.army.mil/html/moh/indianwars.html |title=Medal of Honor recipients |work=Indian War Campaigns |publisher=[[United States Army Center of Military History]] |date=June 8, 2009}}</ref></blockquote>

==Bibliography==

===Books===
* ''A Grammar of the Maguindanao Tongue According to the Manner of Speaking It in the Interior and on the South Coast of the Island of Mindanao'' (1906, contributing editor with Jacinto Juanmart)
* ''Memoirs of the late Frank D. Baldwin, Major General, U.S.A.'' (1928, contributing editor with Alice B. Baldwin)

===Articles===
* ''Old Military Forts of the Southwest'' (1930)
* ''Some Unpublished History of the Southwest'' (1931)

==See also==
{{Portal|Biography|United States Army}}
* [[List of Medal of Honor recipients for the Indian Wars]]

==References==
{{Reflist}}

==Further reading==
* Smith, Jr., Cornelius C. ''Don't Settle for Second: Life and Times of Cornelius C. Smith''. San Rafael, California: Presidio Press, 1977. ISBN 0-89141-007-4
* Wilson, D. Ray. ''Terror on the Plains: A Clash of Cultures''. Dundee, Illinois: Crossroads Communications, 1999. ISBN 0-916445-47-X

==External links==
* {{Find a Grave|6807847|Cornelius Cole Smith|work=Indian Wars Medal of Honor Recipient|author=John "J-Cat" Griffith|date=September 28, 2002|accessdate=July 1, 2011}}
* [http://www.army.mil/medalofhonor/citations3.html#S Indian War Campaigns Medal of Honor Recipients for the United States Army] at [[Army Knowledge Online]]

{{DEFAULTSORT:Smith, Cornelius Cole}}
[[Category:1869 births]]
[[Category:1936 deaths]]
[[Category:American Indian Wars recipients of the Medal of Honor]]
[[Category:American military personnel of the Indian Wars]]
[[Category:American military personnel of the Spanish–American War]]
[[Category:American military personnel of the Philippine–American War]]
[[Category:American military personnel of World War I]]
[[Category:Burials at Evergreen Cemetery (Riverside, California)]]
[[Category:People from Tucson, Arizona]]
[[Category:People from Riverside, California]]
[[Category:United States Army Medal of Honor recipients]]
[[Category:United States Army soldiers]]
[[Category:People from Fort Huachuca, Arizona]]