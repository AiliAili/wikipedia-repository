{{Use dmy dates|date=May 2014}}
{{Use British English|date=May 2014}}
{{Infobox scientist
| name = Henry Roy Dean
| image = HR_Dean.jpg
| image_size = 70px
| birth_date = {{birth date|1879|2|19|df=y}}
| birth_place = [[Bournemouth]], Dorset, England
| death_date = {{death date and age|1961|2|13|1879|2|19|df=y}}
| death_place = [[Cambridge]], England
| nationality = [[United Kingdom]]
| fields = [[Medicine]] ([[Pathology#Pathology as a medical specialty|Pathology]])
| workplaces = [[Lister Institute]]<br />[[University of Sheffield]]<br />[[University of Manchester]]<br />[[University of Cambridge]]
| alma_mater = [[New College, Oxford]]
| known_for = Department of Pathology of the [[University of Cambridge]]<br />Master of [[Trinity Hall, Cambridge]]
| awards =
| influenced = [[Max Barrett]]}}

'''Henry Roy Dean''', [[Doctor of Medicine|MD]], [[LL.D]], [[D.Sc]], [[Royal College of Physicians|FRCP]] (19 February 1879 – 13 February 1961), also known as Prof. H. R. Dean, was a professor of [[Pathology#Pathology as a medical specialty|Pathology]] at the [[University of Cambridge]] and Master of [[Trinity Hall, Cambridge]].<ref name=HRD1>{{cite journal |pmid=14447876 |year=1961 |title=H. R. Dean, M.D., LL.D., D.Sc., F.R.C.P |volume=1 |issue=5225 |pages=595–6 |pmc=1953813 |journal=British Medical Journal |doi=10.1136/bmj.1.5225.595-c}}</ref><ref name=HRD2>{{cite journal |doi=10.1002/path.1700830242 |title=Henry Roy Dean, 19th February 1879-13th February 1961 |year=1962 |last1=Dible |first1=J. Henry |journal=The Journal of Pathology and Bacteriology |volume=83 |issue=2 |pages=587–97}}</ref>

==Biography==
Henry Roy Dean was born in [[Bournemouth]], Dorset, England  to Joshua Dean and [[MacCormac family of County Armagh, Northern Ireland|Elizabeth Dean]], née [[MacCormac family of County Armagh, Northern Ireland|MacCormac]]. Elizabeth Dean was a member of a [[MacCormac family of County Armagh, Northern Ireland|distinguished Northern Irish medical family]] and she was the daughter of [[Henry MacCormac (physician)|Henry MacCormac]] and the sister of [[Sir William MacCormac]]. Dean was educated at [[Sherborne School]] and he attended with [[first-class honours]] the School of Natural Science at [[New College, Oxford]], to be graduated [[MB BCh]] in 1904, after medical training at [[St Thomas' Hospital]], where he was medical registrar and after resident assistant physician. After a senior demyship at [[Magdalen College, Oxford]], he took [[Membership of the Royal College of Physicians|MRCP]] in 1906, a Radcliffe Travelling Fellowship in 1909 (to study at Wassermann Laboratory, Berlin), [[Doctor of Management|D.M.]] in 1912 and [[Royal College of Physicians|FRCP]] in 1913.<ref name=HRD1 />

From 1910 he was assistant bacteriologist at the [[Lister Institute]], London before to become in 1912 Professor of Pathology and Bacteriology at the [[University of Sheffield]]. Then he was Professor of Pathology first in the [[University of Manchester]] from 1915, where he was also a Major ([[R.A.M.C.]]) during the war, then in the [[University of Cambridge]] in August 1922, where he was also deputy professor of [[Regius Professor of Physic (Cambridge)|physic]] substituting Prof. [[John Ryle (professor)|John Ryle]] during the second world war.<ref name=HRD1 />

In Cambridge, in the then small Department of Pathology in Downing Street, early Dean was able to let include in 1925 Pathology as a subject for Part II of the [[Natural Science Tripos]]. That was a successful choice, even for the history of pathology: many students who had taken the Part II Pathology course would go on to occupy important positions in pathology and other branches of medicine (among them was [[Max Barrett]]).<ref name=HRD1 /><ref name=AMB2 /> Dean was engaged to design a new building of the Department of Pathology in Tennis Court Road, where it is today from September 1928.<ref name=HRD1 /><ref>{{cite web|title=About the Department: History |url=http://www.path.cam.ac.uk/about/history.html |work=Department of Pathology |publisher=University of Cambridge |accessdate=13 February 2012 |year=2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20120205195249/http://www.path.cam.ac.uk/about/history.html |archivedate=5 February 2012 |df=dmy-all }}</ref> In 1946 he improved his course (58 lectures) with a training scheme for the would-be pathologists (2 or 3 years of experience of laboratory work). Apart his own works, he guided others to their subsequent experimental works, as well as to their publications on immunology.<ref name=HRD1 /><ref name="AMB2">{{cite journal |doi=10.1002/path.1700870128 |title=Arthur Max Barrett. 28 July 1909—11 December 1961 |year=1964 |last1=Dixon |first1=Kendal C. |last2=Herbertson |first2=B. M. |journal=The Journal of Pathology and Bacteriology |volume=87 |pages=191–6 |pmid=14106346}}</ref>

From 1929 to 1954 he was Master of [[Trinity Hall, Cambridge]] (he was a Fellow there since he came to Cambridge in 1922) and from 1937 to 1939 [[Vice-Chancellor of the University of Cambridge]]. He was also Chairman of the [[Imperial Cancer Research Fund]] (1941-1956), member of the Medical Research Council ([[Medical Research Council (UK)|MRC]]), founder of the East Anglian Pathologists Club, and, from 1920 to 1954, secretary of the [[Pathological Society of Great Britain and Ireland|Pathological Society]].<ref name=HRD1 /><ref>{{cite book|last=O'Connor|first=W. J.|title=British physiologists 1885-1914: A biographical dictionary|year=1991|publisher=Manchester University Press|location=Manchester|isbn=978-0-7190-3282-0|pages=63|url=https://books.google.com/books?id=KrToAAAAIAAJ&pg=PA63&dq=henry+roy+dean}}</ref> Working for various universities he became: honorary Legum Doctor ([[LL.D]]) at University of Aberdeen and at Western Reserve University, honorary Doctor of Science ([[D.Sc]]) at University of Liverpool, and Honorary Fellow of New College, University of Oxford (from 1953). During the second world war he organized several blood transfusion donor services, while the department also accommodated the Galton Laboratory blood-grouping unit and the [[Medical Research Council (UK)|MRC]] Emergency Public Health Laboratory. After the war the Department of Pathology rose again, more closely bound to medicine.<ref name=HRD1 />

{{quote|text=To my mind pathology and medicine form one whole, and it is as difficult to think of pathology without medicine as I trust it is impossible to think of medicine without pathology.
:Prof. H. R. Dean speech at the Edinburgh Pathological Club, 1918<ref name=HRD1 />}}

==Personal life==
Henry Dean married Irene Wilson, and was the father of [[Sir Patrick Henry Dean]], the [[List of Ambassadors of the United Kingdom to the United States|United Kingdom Ambassador]] to the [[United States]].

==Works==
List of works in [[MLA format]] taken from the results of the search engines in the websites ''Wiley Online Library'' (for the works on ''[[The Journal of Pathology|The Journal of Pathology and Bacteriology]]'')<ref>{{cite web|title=Wiley Online Library: Search Results Page|url=http://onlinelibrary.wiley.com/advanced/search/results?searchRowCriteria%5b0%5d.queryString=%22H.%20R.%20Dean%22&searchRowCriteria%5b0%5d.fieldName=author|work=Wiley Online Library|accessdate=14 February 2012}}</ref> and ''National Center for Biotechnology Information'' (for all the other works).<ref>{{PubMedAuthorSearch|Dean|HR}}</ref>

*{{cite journal |pmid=16992586 |year=1901 |last1=Dean |first1=HR |title=The isometric value of active muscle excited directly and indirectly |volume=27 |issue=3 |pages=257–68 |pmc=1540478 |journal=The Journal of Physiology |doi=10.1113/jphysiol.1901.sp000870}}
*{{cite journal |doi=10.1002/path.1700120203 |title=Observations on the leucocytosis produced by the toxin of the diphtheria bacillus, with especial reference to the changes which follow the injection of antitoxin |year=1908 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=12 |issue=2 |pages=154–165}}
*{{cite journal |pmid=19974501 |year=1910 |last1=Dean |first1=HR |title=An Examination of the Blood Serum of Idiots by the Wassermann Reaction |volume=3 |issue=Neurol Sect |pages=117–23 |pmc=1960861 |journal=Proceedings of the Royal Society of Medicine}}
*{{cite journal |pmid=19975404 |year=1911 |last1=Dean |first1=HR |title=Studies in Complement Fixation with Strains of Typhoid, Paratyphoid, and Allied Organisms |volume=4 |issue=Pathol Sect |pages=251–78 |pmc=2005008 |journal=Proceedings of the Royal Society of Medicine}}
*{{cite journal |pmid=19975865 |year=1912 |last1=Dean |first1=HR |title=A Discussion on Syphilis, with special reference to (a) its Prevalence and Intensity in the Past and at the Present Day; (b) its Relation to Public Health, including Congenital Syphilis; (c) the Treatment of the Disease |volume=5 |issue=Gen Rep |pages=151–62 |pmc=2007148 |journal=Proceedings of the Royal Society of Medicine}}
*{{cite journal |pmid=19976282 |year=1912 |last1=Dean |first1=HR |title=Ulcerative Endocarditis produced by the Pneumococcus in a Child, aged 3 |volume=5 |issue=Pathol Sect |pages=185–6 |pmc=2004977 |journal=Proceedings of the Royal Society of Medicine}}
*{{cite journal |pmid=19976292 |year=1912 |last1=Dean |first1=HR |title=The Relation between the Fixation of Complement and the Formation of a Precipitate |volume=5 |issue=Pathol Sect |pages=62–103 |pmc=2004976 |journal=Proceedings of the Royal Society of Medicine}}
*{{cite journal |pmid=20474490 |year=1912 |last1=Ledingham |first1=JC |last2=Dean |first2=HR |title=The Action of the Complement-Fractions on a Tropin-B. Typhosus System with Comparative Haemolytic Experiments |volume=12 |issue=2 |pages=152–94 |pmc=2167385 |journal=The Journal of hygiene |doi=10.1017/S0022172400017113}}
*{{cite journal |pmid=20474495 |year=1912 |last1=Dean |first1=HR |title=On the Mechanism of Complement Fixation |volume=12 |issue=3 |pages=259–89 |pmc=2167398 |journal=The Journal of hygiene |doi=10.1017/S0022172400005027}}
*{{cite journal |pmid=20767973 |year=1916 |last1=Dean |first1=HR |last2=Mouat |first2=TB |title=The Bacteria of Gangrenous Wounds |volume=1 |issue=2872 |pages=77–83 |pmc=2346865 |journal=British Medical Journal |doi=10.1136/bmj.1.2872.77}}
*{{cite journal |pmid=20768111 |year=1916 |last1=Dean |first1=HR |last2=Adamson |first2=RS |title=Preliminary Note ON a METHOD FOR THE PREPARATION OF a NON-TOXIC DYSENTERY VACCINE |volume=1 |issue=2887 |pages=611–4 |pmc=2347429 |journal=British Medical Journal |doi=10.1136/bmj.1.2887.611}}
*{{cite journal |pmid=20768380 |year=1916 |last1=Dean |first1=HR |title=The Horace Dobell Lecture ON THE MECHANISM OF THE SERUM REACTION: Delivered before the Royal College of Physicians of London |volume=2 |issue=2918 |pages=749–52 |pmc=2355053 |journal=British Medical Journal |doi=10.1136/bmj.2.2918.749}}
*{{cite journal |doi=10.1002/path.1700210204 |title=The influence of temperature on the fixation of complement |year=1918 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=21 |issue=2 |pages=193–214}}
*{{cite journal |doi=10.1002/path.1700250114 |title=German Sims Woodhead. K.B.E., M.D., LL.D. Born April 29th, 1855-Died December 29th, 1921 |year=1922 |last1=Ritchie |first1=James |last2=Boycott |first2=A. E. |last3=Dean |first3=H. R. |journal=The Journal of Pathology and Bacteriology |volume=25 |pages=118–137}}
*{{cite journal |doi=10.1002/path.1700250305 |title=The histology of a case of anaphylactic shock occurring in a man |year=1922 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=25 |issue=3 |pages=305–315}}
*{{cite journal |doi=10.1002/path.1700250312 |title=A lumbar puncture needle for bacteriological work |year=1922 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=25 |issue=3 |pages=398}}
*{{cite journal |doi=10.1002/path.1700270106 |title=The morbid anatomy and histology of anaphylaxis in the dog |year=1924 |last1=Dean |first1=H. R. |last2=Webb |first2=R. A. |journal=The Journal of Pathology and Bacteriology |volume=27 |pages=51–64}}
*{{cite journal |doi=10.1002/path.1700270107 |title=The blood changes in anaphylactic shock in the dog |year=1924 |last1=Dean |first1=H. R. |last2=Webb |first2=R. A. |journal=The Journal of Pathology and Bacteriology |volume=27 |pages=65–78}}
*{{cite journal |doi=10.1002/path.1700290412 |title=The influence of optimal proportions of antigen and antibody in the serum precipitation reaction |year=1926 |last1=Dean |first1=H. R. |last2=Webb |first2=R. A. |journal=The Journal of Pathology and Bacteriology |volume=29 |issue=4 |pages=473–492}}
*{{cite journal |doi=10.1002/path.1700300415 |title=Complement fixation in mixtures of toxin and antitoxin |year=1927 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=30 |issue=4 |pages=675–685}}
*{{cite journal |doi=10.1002/path.1700310108 |title=The determination of the rate of antibody (precipitin) production in rabbit's blood by the method of "optimal proportions" |year=1928 |last1=Dean |first1=H. R. |last2=Webb |first2=R. A. |journal=The Journal of Pathology and Bacteriology |volume=31 |pages=89–99}}
*{{cite journal |pmid=20775678 |year=1930 |last1=Dean |first1=HR |title=Educational Number, Session 1930-31: A Review of the Medical Curriculum |volume=2 |issue=3635 |pages=341–5 |pmc=2450311 |journal=British Medical Journal |doi=10.1136/bmj.2.3635.341}}
*{{cite journal |last1=Dean |first1=H. R. |pmc=2520887 |title=L.C.C. Pathological Service |journal=British Medical Journal |volume=1 |issue=3723 |year=1932 |pages=909–10 |doi=10.1136/bmj.1.3723.909}}
*{{cite journal |pmid=20475263 |year=1935 |last1=Dean |first1=HR |last2=Taylor |first2=GL |last3=Adair |first3=ME |title=The Precipitation Reaction: Experiments with an Antiserum containing Two Antibodies |volume=35 |issue=1 |pages=69–74 |pmc=2170611 |journal=The Journal of hygiene |doi=10.1017/S0022172400018982}}
*{{cite journal |pmid=20475355 |year=1936 |last1=Dean |first1=HR |last2=Williamson |first2=R |last3=Taylor |first3=GL |title=Passive Anaphylaxis following the immediate injection of Antigen after Antiserum |volume=36 |issue=4 |pages=570–87 |pmc=2171025 |journal=The Journal of hygiene |doi=10.1017/S002217240004393X}}
*{{cite journal |doi=10.1002/path.1700450321 |title=The reaction of isamine blue with serum |year=1937 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=45 |issue=3 |pages=745–771}}
*{{cite journal |last1=Balfour |first1=. |first2=A. E. |last2=Boycott |first3=S. Lyle |last3=Cummins |last4=Dawson |first5=H. R. |last5=Dean |first6=J. Henry |last6=Dible |first7=J. B. |last7=Duguid |first8=Herbert L. |last8=Eason |first9=Francis R. |last10=Fraser |first10=F. R. |last11=Gask |first11=G. E. |last12=Gordon |first12=M. H. |last13=Gye |first13=W. E. |last14=Hadfield |first14=G. |last15=McIntosh |first15=J. |last16=Muir |first16=R. |last17=Murray |first17=J. A. |last18=Orenstein |first18=A. J. |last19=Proctor |first19=A. H. |last20=Ryle |first20=J. A. |last21=Sheen |first21=A. W. |last22=Spilsbury |first22=B. |last23=Sprigge |first23=S. |last24=Stewart |first24=M. J. |last25=Topley |first25=W. W. C. |last26=Wilson |first26=C. M. |last9=Fraser |pmc=2088913 |title=Memorial to Professor E. H. Kettle |journal=British Medical Journal |volume=1 |issue=3986 |year=1937 |pages=1134 |doi=10.1136/bmj.1.3986.1134 |first4=S. L.|display-authors=8 }}
*{{cite journal |doi=10.1002/path.1700560324 |title=William Whiteman Carlton Copley. Born 19th January 1886. Died 21st January 1944 |year=1944 |last1=Dean |first1=H. R. |last2=Wilson |first2=G. S. |journal=The Journal of Pathology and Bacteriology |volume=56 |issue=3 |pages=451–469}}
*{{cite journal |doi=10.1002/path.1700580338 |title=George Lees Taylor, Born 26th June 1897. Died 9th March 1945 |year=1946 |last1=Dean |first1=H. R. |journal=The Journal of Pathology and Bacteriology |volume=58 |issue=3 |pages=593–597}}
*{{cite journal |pmid=19993415 |year=1946 |last1=Dean |first1=HR |title=The Pathological Society of London |volume=39 |issue=12 |pages=823–7 |pmc=2182434 |journal=Proceedings of the Royal Society of Medicine}}

==References==
{{Reflist}}

==External links==
* [http://www.path.cam.ac.uk/ University of Cambridge: Department of Pathology]

{{s-start}}
{{s-aca}}

{{succession box
 | title = [[Trinity Hall, Cambridge|Master of Trinity Hall, Cambridge]]
 | years = 1929–1954
 | before = Henry Bond
 | after = [[Sir William Ivor Jennings]]
}}

{{succession box
 |title = [[Vice-Chancellor of the University of Cambridge]]
 |before = [[Godfrey Harold Alfred Wilson]]
 |after = [[Ernest Alfred Benians]]
 |years = 1937–1939
}}
{{s-end}}

{{DEFAULTSORT:Dean, Henry Roy}}
[[Category:Masters of Trinity Hall, Cambridge]]
[[Category:Vice-Chancellors of the University of Cambridge]]
[[Category:1879 births]]
[[Category:1961 deaths]]
[[Category:MacCormac family of County Armagh, Northern Ireland]]