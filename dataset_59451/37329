{{other people||Stephen Báthory (disambiguation)}}
{{Good article}}
{{Use dmy dates|date=May 2013}}

<!-- SCROLL DOWN TO EDIT THE ARTICLE -->
{{Infobox royalty
|              name=Stephen Báthory
|        image= File:Stefan Batory 11.PNG
|       image_size = 235
|      caption = An 18th-century depiction of Stephen Báthory by [[Bacciarelli]]
|      succession= [[King of Poland]]<br>[[Grand Duchy of Lithuania|Grand Duke of Lithuania]]
|      predecessor=[[Anna Jagiellon]]
|      successor=[[Sigismund III Vasa]]
|         birth_date={{Birth date|df=yes|1533|9|27}}
|        birth_place=[[Şimleu Silvaniei|Szilágysomlyó]], [[Transylvania]] (now Şimleu Silvaniei, [[Romania]])
|         death_date={{death date and age|df=yes|1586|12|12|1533|9|27}}
|        death_place=[[Hrodna|Grodno]], [[Polish-Lithuanian Commonwealth]] (now in [[Belarus]])
|      place of burial=[[Wawel Cathedral]], Saint Mary's Crypt (buried May 1588)
|       reign=1 May 1576 – 12 December 1586
|     coronation=14 December 1575<br>1 May 1576
|     cor-type=Election<br>Coronation
|            house=[[Báthory family|Báthory]]
|            father= [[Stephen VIII Báthory|Stephen Báthory of Somlyó]]
|            mother=[[Catherine Telegdi]]
|         spouse=[[Anna Jagiellon]]
|        signature=Autograph-StefanBatory.svg
}}
{{Eastern name order|Báthory István}}
'''Stephen Báthory''' ({{lang-hu|Báthory István}}; {{lang-pl|Stefan Batory}}; {{lang-be|Sciapan Batura}}; {{lang-lt|Steponas Batoras}}; {{lang-ro|Ştefan Báthory}}; 27 September 1533 – 12 December 1586) was [[Voivode of Transylvania]] (1571–76), [[Prince of Transylvania]] (1576–86), from 1576 Queen [[Anna Jagiellon]]'s husband and ''[[jure uxoris]]'' [[King of Poland]] and [[Grand Duke of Lithuania]] (1576-1586).

The son of [[Stephen VIII Báthory]] and a member of the [[Hungary|Hungarian]] [[Báthory|Báthory noble family]], Báthory was a ruler of [[Transylvania]] in the 1570s, defeating another challenger for that title, [[Gáspár Bekes]]. In 1576 Báthory became the third [[royal election|elected]] king of Poland. He worked closely with chancellor [[Jan Zamoyski]]. The first years of his reign were focused on establishing power, defeating a fellow claimant to the throne, [[Maximilian II, Holy Roman Emperor]], and quelling rebellions, most notably, the [[Danzig rebellion]]. He reigned only a decade, but is considered one of the most successful [[Monarchs of Poland|kings]] in [[Polish history]], particularly in the realm of military history. His signal achievement was his victorious [[Livonian campaign of Stephen Báthory|campaign in Livonia]] against [[Tsardom of Russia|Russia]] in the middle part of his reign, in which he repulsed a Russian invasion of Commonwealth borderlands and secured a highly favorable treaty of peace (the [[Peace of Jam Zapolski]]).

==Life==
[[File:Bathory coat of arms.svg|thumb|left|upright|Báthory coat of arms]]
Stephen Báthory was born on 27 September 1533 in [[Șimleu Silvaniei Fort|the castle]] at Somlyó, also known as Szilágysomlyó (today's [[Şimleu Silvaniei]]).<ref name=psb114/> He was the son of [[Stephen VIII Báthory]] (d. 1534) of the noble [[Hungary|Hungarian]] [[Báthory|Báthory family]] and his wife [[Catherine Telegdi]].<ref name="psb114"/> He had at least five siblings: two brothers and three sisters.<ref name=psb114/>

Little is known about his childhood. Around 1549-1550 he briefly visited Italy and likely spent a few months attending lectures at the [[Padua University]].<ref name=psb114/> Upon his return, he joined the army of [[Ferdinand I, Holy Roman Emperor]], and took part in his military struggle against the [[Ottoman Empire|Turks]].<ref name=psb114/> Some time after 1553 Báthory was captured by the Turks, and after Ferdinand I refused to pay his ransom, Báthory joined the opposing side, supporting [[John II Sigismund Zápolya]] in his struggle for power in the [[Eastern Hungarian Kingdom]].<ref name=psb114/> As Zápolya's supporter, Báthory acted both as a feudal lord, military commander and a diplomat.<ref name=psb114/><ref name="psb115"/> During one of his trips to [[Vienna]] he was put under house arrest for two years.<ref name=psb115/> During this time Báthory fell out of favour at Zápolya's court, and his position was largely assumed by another Hungarian noble, [[Gáspár Bekes]].<ref name=psb115/> Báthory briefly retired from politics, but he still wielded considerable influence and was seen as a possible successor to Zápolya.<ref name=psb115/>

After Zápolya's death in 1571, the Transylvanian estates elected Báthory [[Voivode of Transylvania]].<ref name=psb115/> Bekes, supported by the Habsburgs, disputed his election, but by 1573 Báthory emerged victorious in the resulting civil war and drove Bekes out of Transylvania.<ref name=psb115/> He subsequently attempted to play the Ottomans and the Holy Roman Empire against one another in an attempt to strengthen the Transylvania position.<ref name="psb116"/>

==Elected king==
[[Image:Stefan Batory King.jpg|thumb|right|Báthory, by [[Jan Matejko]]]]
In 1572, the throne of the [[Polish–Lithuanian Commonwealth]], at the time the largest and one of the most populous states in Europe, was vacated when King [[Sigismund II Augustus|Sigismund II of Poland]] died without heirs.<ref name=psb116/> The [[Sejm]] was given the power to [[Royal elections in Poland|elect]] a new king, and in the [[Polish–Lithuanian royal election, 1573]] chose [[Henry III of France|Henry of France]]; Henry soon ascended the French throne and forfeited the Polish one by returning to France.<ref name=psb116/> Báthory decided to enter into the election; in the meantime he had to defeat another attempt by Bekes to challenge his authority in Transylvania, which he did by defeating Bekes at the [[Battle of Sinpaul]].<ref name=psb116/>

On 12 December 1575, after an [[interregnum]] of roughly one and a half years, [[primate of Poland]] [[Jakub Uchański]], representing a pro-Habsburg faction, declared [[Maximilian II, Holy Roman Emperor|Emperor Maximilian II]] as the new monarch.<ref name=psb116/> However, chancellor [[Jan Zamoyski]] and others opponents of Habsburgs persuaded many of the lesser [[szlachta|nobility]] to demand a ''[[Piast dynasty|Piast king]]'', a Polish king.<ref name=psb116/><ref name="Stone2001-122"/><ref name="Lerski1996"/> After a heated discussion, it was decided that [[Anna Jagiellon]], sister of former King [[Sigismund II Augustus]], should be elected King of Poland and marry Stephen Báthory.<ref name="psb117"/> In January 1576 Báthory passed the mantle of Voivode of Transylvania to his brother [[Christopher Báthory]] and departed for Poland.<ref name=psb117/> On 1 May 1576 Báthory married Anna and was crowned [[King of Poland]] and [[Grand Duke of Lithuania]].<ref name=psb117/> After being chosen as king in the [[Polish–Lithuanian royal election, 1576]], Báthory also began using the title of the [[Prince of Transylvania]].<ref name=psb115/>

==Establishing power==
[[File:Batory Jagiellonka.jpg|thumb|left|200px|Báthory and wife [[Anna Jagiellon]], by Matejko]]
Báthory's position was at first extremely difficult, as there was still some opposition to Báthory's election. Emperor Maximilian, insisting on his earlier election, fostered internal opposition and prepared to enforce his claim by military action.<ref name=psb118/> At first the representatives of the Grand Duchy of Lithuania refused to recognize Báthory as Grand Duke, and demanded concessions - that he return the estates of his wife Anne to the Lithuanian treasury, hold Sejm conventions in both Lithuania and Poland, and reserve the highest governmental official offices in Lithuania for Lithuanians. He accepted the conditions.<ref name="Greenbaum1995">{{cite book|author=Masha Greenbaum|title=The Jews of Lithuania: a history of a remarkable community, 1316-1945|url=https://books.google.com/books?id=E2_ag8xGWMQC&pg=PA22|date=1 January 1995|publisher=Gefen Publishing House Ltd|isbn=978-965-229-132-5|page=22}}</ref> In June Báthory was recognized as [[Grand Duchy of Lithuania|Grand Duke of Lithuania]], Duke of [[Ruthenia]] and [[Samogitia]].{{efn|His official titles were ''Stephanus Dei gratia rex Poloniae et magnus dux Lithuaniae, Russiae, Prussiae, Masoviae, Samogitiae, Kiioviae, Voliniae, Podlachiae, Livoniaeque, necnon. princeps Transylvaniae.'' in [[Latin]].}}<ref name=psb117/><ref name="psb118"/>

With Lithuania secure, the other major region refusing to recognize his election was [[Prussia]].<ref name=psb118/> Maximilian's sudden death improved Báthory's situation, but the city of [[Danzig]] (Gdańsk) still refused to recognize his election without significant concessions.<ref name=psb118/> The [[Hanseatic League]] city, bolstered by its immense wealth, fortifications, and the secret support of Maximilian, had supported the Emperor's election and decided not to recognize Báthory as legitimate ruler. The resulting conflict was known as the [[Danzig rebellion]]. Most armed opposition collapsed when the prolonged [[Siege of Danzig (1577)|Siege of Danzig]] by Báthory's forces was lifted as an agreement was reached.<ref name=psb118/><ref name="psb119"/> The Danzig army was utterly defeated in a field battle on 17 April 1577.<ref name="Jabłonka2007"/> However, since Báthory's armies were unable to take the city by force, a compromise was reached.<ref name="Jabłonka2007"/><ref name="Stone2001-123"/> In exchange for some of Danzig's demands being favorably reviewed, the city recognised Báthory as ruler of Poland and paid the sum of 200,000 [[zloty]]s in gold as compensation.<ref name=psb119/><ref name="Stone2001-123"/> Tying up administration of the Commonwealth northern provinces, in February 1578 he acknowledged [[George Frederick, Margrave of Brandenburg-Ansbach|George Frederick]] as the ruler of [[Duchy of Prussia]], receiving his feudal tribute.<ref name=psb119/>

==Policies==
[[File:Kober Stephen Bathory.jpg|thumb|Báthory, by [[Marcin Kober]]]]
After securing control over Commonwealth, Báthory had a chance to devote himself to strengthening his authority, in which he was supported by his [[kanclerz|chancellor]] [[Jan Zamoyski]], who would soon become one of the king's most trusted advisers.<ref name="Lerski1996"/><ref name="Stone2001-123"/> Báthory reorganised the judiciary by formation of legal tribunals (the [[Crown Tribunal]] in 1578 and the [[Lithuanian Tribunal]] in 1581).<ref name="Stone2001-125"/> While this somewhat weakened the royal position, it was of little concern to Báthory, as the loss of power was not significant in the short term, and he was more concerned with the hereditary Hungarian throne.<ref name=psb119/><ref name="Stone2001-125"/> In exchange, the Sejm allowed him to raise taxes and push a number of reforms strengthening the military, including the establishment of the ''[[piechota wybraniecka]]'', an infantry formation composed of peasants.<ref name=psb119/> Many of his projects aimed to modernize the Commonwealth army, reforming it in a model of Hungarian troops of Transylvania.<ref name="psb124"/> He also founded the [[Vilnius University|Academy of Vilnius]], the third university in the Commonwealth, transforming a prior [[Jesuit]] college into a major university.<ref name="Stone2001-126"/> He founded several other Jesuit colleges, and was active in propagating [[Catholicism]], while at the same time being respectful of the Commonwealth policy of [[religious tolerance]], issuing a number of decrees offering protection to [[Polish Jews]], and denouncing any [[religious violence]].<ref name=psb124/>

In external relations, Báthory sought peace through strong alliances. Though Báthory remained distrustful of the Habsburgs, he maintained the tradition of good relations that the Commonwealth had with its Western neighbor and confirmed past treaties between the Commonwealth and Holy Roman Empire with diplomatic missions received by Maximilian's successor, [[Rudolf II, Holy Roman Emperor|Rudolf II]].<ref name=psb120/> The troublesome south-eastern border with the [[Ottoman Empire]] was temporarily quelled by truces signed in July 1577 and April 1579.<ref name="psb120"/> The [[Sejm]] of January 1578 gathered in [[Warsaw]] was persuaded to grant Báthory subsidies for the inevitable war against [[Tsardom of Russia|Muscovy]].<ref name=psb119/>

A number of his trusted advisers were Hungarian, and he remained interested in the Hungarian politics.<ref name=psb124/> He wished to recreate his native country into an independent, strong power, but the unfavorable international situation did not allow him to significantly advance any of his plans in that area.<ref name="psb122"/> In addition to Hungarian, he was well versed in [[Latin]], and spoke Italian and German; he never learned the Polish language.<ref name=psb124/>

In his personal life, he was described as rather frugal in his personal expenditures, with hunting and reading as his favorite pastimes.<ref name=psb124/>

==War with Muscovy==
[[Image:Stephen Báthory at Pskov by Jan Matejko (1872).png|thumb|left|300px|''Báthory at [[Pskov]]'', by [[Jan Matejko|Matejko]]]]
Before Báthory's election to the throne of the Commonwealth, [[Ivan IV of Russia|Ivan the Terrible]] of Russia had begun encroaching on its sphere of interest in the northeast, eventually invading the Commonwealth borderlands in [[Livonia]]; the conflict would grow to involve a number of nearby powers (outside Russia and Poland-Lithuania, also [[Sweden]], the [[Kingdom of Livonia]] and [[Denmark-Norway]]). Each of them was vying for control of Livonia, and the resulting conflict, lasting for several years, became known as the [[Livonian War]].<ref name="Stone2006"/> By 1577 Ivan was in control of most of the disputed territory, but his conquest was short-lived.<ref name="Stone2006"/> In 1578 Commonwealth forces scored a number of victories in Liviona and begun pushing Ivan's forces back; this marked the turning point in [[Livonian campaign of Stephen Báthory|the war]].<ref name=psb120/> Báthory, together with his chancellor Zamoyski, led the army of the Commonwealth in a series of decisive campaigns taking [[siege of Polotsk (1579)|Polotsk]] in 1579 and [[siege of Velikiye Luki|Velikiye Luki]] in 1580.<ref name=psb120/>

In 1581 Stephen penetrated once again into [[Tsardom of Russia|Russia]] and, on 22 August, [[Siege of Pskov|laid siege to the city of Pskov]]. While the city held, on 13 December 1581 Ivan the Terrible began negotiations that concluded with the [[Truce of Jam Zapolski]] on 15 January 1582.<ref name="psb121"/> The treaty was favorable to the Commonwealth, as Ivan ceded [[Polatsk]], [[Velizh|Veliz]] and most of the [[Duchy of Livonia]] in exchange for regaining [[Velikiye Luki]] and [[Nevel (town)|Nevel]].<ref name=psb121/><!-- where Báthory revoked the noble privileges granted in the [[Treaty of Vilnius (1561)]] and initiated [[counter-reformation]].-->

==Final years==
[[Image:StefanBatory.jpg|thumb|160px|Polish coin with likeness of Báthory]]
In 1584 Báthory allowed Zamoyski to execute [[Samuel Zborowski]], whose death sentence for treason and murder had been pending for roughly a decade.<ref name="Stone2001-125"/><ref name="psb123"/> This political conflict between Báthory and the [[Zborowski family]], framed as the clash between the monarch and the nobility, would be a major recurring controversy in internal Polish politics for many years.<ref name="Stone2001-125"/><ref name="psb123"/> In external politics, Báthory was considering another war with Russia, but his plans were delayed to the lack of support from the Sejm, which refused to pass requested tax raises.<ref name=psb123/>

Báthory's health had been declining for several years.<ref name=psb123/> He died on 12 December 1586. <ref name="psb125"/><!-- while in [[Old Hrodna Castle|Old Grodno Castle]]--> He had no legitimate children, though contemporary rumours suggested he might have had several illegitimate children. None of these rumours have been confirmed by modern historians.<ref name=psb125/> His death was followed by an interregnum of one year. Maximilian II's son, [[Maximilian III, Archduke of Austria|Archduke Maximilian III]], [[Free election, 1587|was elected king]] but [[War of the Polish Succession (1587–88)|was contested]] by the Swedish [[Sigismund III Vasa]], who defeated Maximilian at the [[Battle of Byczyna|Byczyna]] and succeeded as ruler of the Commonwealth.<ref name="Stone2001-132132"/>

==Remembrance==

[[Image:Zbroja Batorego.jpg|thumb|left|200px|Báthory's [[armor]], 19th-century painting by Jan Matejko]]

Báthory actively promoted his own legend, sponsoring a number of works about his life and achievements, from historical treatises to poetry.<ref name=psb124/> In his lifetime, he was featured in the works of [[Jan Kochanowski]], [[Mikołaj Sęp Szarzyński]] and many others.<ref name=psb124/> He became a recurring character in Polish poetry and literature and featured as a central figure in poems, novels and drama by [[Jakub Jasiński]], [[Józef Ignacy Kraszewski]], [[Julian Ursyn Niemcewicz]], [[Henryk Rzewuski]] and others.<ref name=psb126/> He has been a subject of numerous paintings, both during his life and posthumously.  Among the painters who took him as a subject were [[Jan Matejko]] and [[Stanisław Wyspiański]].<ref name=psb126/><ref name="psb127"/>

A statue of Báthory by [[Giovanni Ferrari (sculptor)|Giovanni Ferrari]] was raised in 1789 in [[Padua]], Italy, sponsored by the last king of the Commonwealth, [[Stanisław August Poniatowski]].<ref name=psb127/> Other monuments to him include one in the [[Łazienki Palace]] (1795 by Andrzej Le Brun) and one in [[Sniatyn]] (1904, destroyed in 1939).<ref name=psb127/> He was a patron of the [[Vilnius University]] (then known as the Stefan Batory University) and several units in the [[Polish Army]] from 1919 to 1939.<ref name=psb127/> His name was borne by two 20th-century passenger ships of the [[Polish Merchant Navy]], the [[MS Batory]] and [[TSS Stefan Batory]].<ref name=psb127/> In modern Poland, he is the namesake of the [[Batory Steelmill]], a nongovernmental [[Stefan Batory Foundation]], the [[Polish 9th Armored Cavalry Brigade]], and numerous Polish streets and schools.<ref name=psb127/> [[Chorzów Batory|One of the districts]] of the town of [[Chorzów]] is named after him.<ref name=psb127/>
[[File:Stephen Bathory founding Academy in Vilnius.PNG|thumb|right|230px|Founding of the [[University of Vilnius|Vilnius Academy]] by Stephen Báthory in 1579]]
Immediately after his death, he was not fondly remembered in the Commonwealth.  Many nobles took his behavior in the Zborowski affair and his domestic policies as indicating an interest in curtailing the nobility's [[Golden Freedoms]] and establishing an [[absolute monarchy]].<ref name=psb125/> His contemporaries were also rankled by his favoritism toward Hungarians over nationals of the Commonwealth.<ref name=psb123/>  He was also remembered, more trivially, for his Hungarian-style cap and saber ([[szabla]] ''batorówka'').<ref name="psb126"/>

His later resurgence in Polish memory and historiography can be traced to the 19th-century era of [[partitions of Poland]], when the Polish state lost its independence.<ref name=psb126/>  He was remembered for his military triumphs and praised as an effective ruler by many, including [[John Baptist Albertrandi]], [[Jerzy Samuel Bandtkie]], [[Michał Bobrzyński]], [[Józef Szujski]] and others.<ref name=psb126/> Though some historians like [[Tadeusz Korzon]], [[Joachim Lelewel]] and [[Jędrzej Moraczewski]] remained more reserved, in 1887 [[Wincenty Zakrzewski]] noted that Báthory is "the darling of both the Polish public opinion and Polish historians".<ref name=psb126/> During the interwar period in the [[Second Polish Republic]] he was a cult figure, often compared - with the government's approval - to the contemporary dictator of Poland, [[Józef Piłsudski]].<ref name=psb126/> After the [[Second World War]], in the communist [[People's Republic of Poland]], he became more of a controversial figure, with historians more ready to question his internal politics and attachment to Hungary.<ref name=psb126/> Nonetheless his good image remained intact, reinforced by the positive views of a popular Polish historian of that period, [[Paweł Jasienica]].<ref name=psb126/>

==Ancestry==
{{ahnentafel top|width=100%}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Stephen Báthory, King of Poland'''
|2= 2. [[Stephen VIII Báthory]]
|3= 3. [[Catherine Telegdi]]
|4= 4. Nicholas Báthory
|5= 5. Sophia Bánffy of Losoncz
|6= 6. Stephen Telegdi
|7= 7. Margaret Bebek
|8= 8. Stephen Báthory
|9= 9. Dorothea Várdai
|10= 10. John Bánffy of Losoncz
|11= 11. Margaret Malacz
|12= 12. John Telegdi
|13= 13. Elisabeth Báthori of Ecsed
|14= 14. George Bebek of Pelsőc
|15= 15. Francesca Héderváry of Hédervár
|16= 16. Stanislaus Báthory, Ispán of Szabolcs
|17= 17. Anna Szikszói
|18= 18. Nicholas Várdai
|19= 19. Anna Chyre of Álmosd
|20= 20. Stephen Bánffy of Losoncz
|21= 21. Margaret Jakcs of Kusaly
|22= 
|23=
|24= 24. John Telegdi
|25= 25. Dorothea N.
|26= 26. Stephen Báthori of Ecsed
|27= 27. Barbara Buthkai
|28= 28. Stephen Bebek, Ban of Macsó
|29= 29. Margaret Pálóci
|30= 30. Paul Héderváry of Hédervár
|31= 31. Catherine Bánfi of Gara
}}</center>
{{ahnentafel bottom}}

==Notes==
{{notelist}}

==See also==
{{commons category|Stefan Batory}}
* [[History of Poland (1569–1795)]]
* [[Muscovite wars]]
* [[Nyírbátor]]

==References==
{{reflist|refs=

<ref name="Jabłonka2007">{{cite book|author=Krzysztof Jabłonka|title=Wielkie bitwy Polaków: 40 potyczek, batalii i kampanii decydujących o losach Polski|url=https://books.google.com/books?id=ErUjAQAAIAAJ|accessdate=5 September 2013|year=2007|publisher=Rosikon Press|isbn=978-83-88848-43-8|page=49}}</ref>

<ref name="Lerski1996">{{cite book|author=Halina Lerski|title=Historical Dictionary of Poland, 966-1945|url=https://books.google.com/books?id=luRry4Y5NIYC&pg=PA678|accessdate=2 July 2012|date=30 January 1996|publisher=ABC-CLIO|isbn=978-0-313-03456-5|page=678}}</ref>

<ref name="Stone2001-122">{{cite book|author=Daniel Stone|title=The Polish-Lithuanian State, 1386-1795|url=https://books.google.com/books?id=LFgB_l4SdHAC&pg=PA122|accessdate=5 September 2013|year=2001|publisher=University of Washington Press|isbn=978-0-295-98093-5|page=122}}</ref>

<ref name="Stone2001-123">{{cite book|author=Daniel Stone|title=The Polish-Lithuanian State, 1386-1795|url=https://books.google.com/books?id=LFgB_l4SdHAC&pg=PA123|accessdate=5 September 2013|year=2001|publisher=University of Washington Press|isbn=978-0-295-98093-5|page=123}}</ref>

<ref name="Stone2001-125">{{cite book|author=Daniel Stone|title=The Polish-Lithuanian State, 1386-1795|url=https://books.google.com/books?id=LFgB_l4SdHAC&pg=PA125|accessdate=5 September 2013|year=2001|publisher=University of Washington Press|isbn=978-0-295-98093-5|page=125}}</ref>

<ref name="Stone2001-126">{{cite book|author=Daniel Stone|title=The Polish-Lithuanian State, 1386-1795|url=https://books.google.com/books?id=LFgB_l4SdHAC&pg=PA126|accessdate=5 September 2013|year=2001|publisher=University of Washington Press|isbn=978-0-295-98093-5|page=126}}</ref>

<ref name="Stone2001-132132">{{cite book|author=Daniel Stone|title=The Polish-Lithuanian State, 1386-1795|url=https://books.google.com/books?id=LFgB_l4SdHAC&pg=PA131|accessdate=6 September 2013|year=2001|publisher=University of Washington Press|isbn=978-0-295-98093-5|pages=131–132}}</ref>

<ref name="Stone2006">{{cite book|author=David R. Stone|title=A Military History of Russia: From Ivan the Terrible to the War in Chechnya|url=https://books.google.com/books?id=ok7iVsgiNmAC&pg=PA15|accessdate=6 September 2013|year=2006|publisher=Greenwood Publishing Group|isbn=978-0-275-98502-8|pages=15–17}}</ref>

<ref name="psb114">Besala and Biedrzycka (2005), p.114</ref>

<ref name="psb115">Besala and Biedrzycka (2005), p.115</ref>

<ref name="psb116">Besala and Biedrzycka (2005), p.116</ref>

<ref name="psb117">Besala and Biedrzycka (2005), p.117</ref>

<ref name="psb118">Besala and Biedrzycka (2005), p.118</ref>

<ref name="psb119">Besala and Biedrzycka (2005), p.119</ref>

<ref name="psb120">Besala and Biedrzycka (2005), p.120</ref>

<ref name="psb121">Besala and Biedrzycka (2005), p.121</ref>

<ref name="psb122">Besala and Biedrzycka (2005), p.122</ref>

<ref name="psb123">Besala and Biedrzycka (2005), p.123</ref>

<ref name="psb124">Besala and Biedrzycka (2005), p.124</ref>

<ref name="psb125">Besala and Biedrzycka (2005), p.125</ref>

<ref name="psb126">Besala and Biedrzycka (2005), p.126</ref>

<ref name="psb127">Besala and Biedrzycka (2005), p.127</ref>
}}

==Bibliography==
* {{EB1911|wstitle=Stephen Báthory}}
* {{cite encyclopedia|author1=Jerzy Besala|author2=[[Agnieszka Biedrzycka]]|title=Stefan Batory |work=Polski Słownik Biograficzny |volume=XLIII|year=2004–2005|ref=|language=Polish}}
*[https://www.msz.gov.pl/resource/21badcbf-0c18-4fb8-8b19-3d382469d25f:JCR ''Winged Hussars''], Radoslaw Sikora, Bartosz Musialowicz, ''BUM Magazine'', October 2016.

==External links==
* {{pl icon}} Stephen Báthory's ''szkofia'' in the National Museum in Kraków [http://www.muzeum.krakow.pl/Dzial-XIV-Galeria.362.0.html?&L=0&no_cache=1&tx_devablegallery_pi1].

{{s-start}}
{{s-hou|[[Báthory|House of Báthory]]|1533|27 September|1586|12 December|name=Stephen Báthory}}
{{s-reg}}
|-
{{s-vac|last=[[István Dobó|Stephen Dobó]]<br />[[Francis Kendi]]}}
{{s-ttl|title=[[Voivode of Transylvania]]|years=1571–1576}}
{{s-aft|after=[[Christopher Báthory]]}}
|-
{{s-vac|last=[[John II Sigismund Zápolya|John Sigismund Zápolya]]}}
{{s-ttl|title=[[Prince of Transylvania]]|years=1576–1586}}
{{s-aft|after=[[Sigismund Báthory]]}}
{{s-bef|before=[[Anna of Poland|Anne]]|as=sole monarch}}
{{s-ttl|title=[[King of Poland]]<br>[[Grand Duke of Lithuania]]|years=1576–1586|regent1=[[Anna of Poland|Anne]]}}
{{s-aft|after=[[Sigismund III Vasa|Sigismund III]]}}
{{s-ref|[http://ellone-loire.net/obsidian/regindex.html Regnal Chronologies]}}
{{Monarchs of Poland}}
{{Monarchs of Lithuania}}

{{Authority control}}


{{DEFAULTSORT:Bathory, Stephen}}
[[Category:Roman Catholic monarchs]]
[[Category:Báthory family|Stephen 09]]
[[Category:Polish monarchs]]
[[Category:16th-century monarchs in Europe]]
[[Category:Grand Dukes of Lithuania]]
[[Category:Voivodes of Transylvania]]
[[Category:Monarchs of Transylvania]]
[[Category:Hungarian Roman Catholics]]
[[Category:Polish Roman Catholics]]
[[Category:Jure uxoris kings]]
[[Category:1533 births]]
[[Category:1586 deaths]]
[[Category:People from Șimleu Silvaniei]]
[[Category:Hungarian nobility]]
[[Category:16th-century Hungarian people]]
[[Category:Polish people of the Livonian campaign of Stephen Báthory]]
[[Category:Burials at Wawel Cathedral]]
[[Category:Polish people of Hungarian descent]]
[[Category:Deaths from kidney disease]]