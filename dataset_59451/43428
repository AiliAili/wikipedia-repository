<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = P.V.7/ Grain Kitten <!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = <!--in the ''image:filename'' format, no image tags-->
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = [[Fighter plane|Fighter]]
  |manufacturer = [[Port Victoria Marine Experimental Aircraft Depot|RNAS Marine Experimental Depot, Port Victoria]]
  |designer = W H Sayers<!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 22 June 1917<!--if it hasn't happened, leave it out!-->
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype<!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 1
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}
The '''Port Victoria P.V.7 Grain Kitten''' was a prototype [[United Kingdom|British]] Fighter aircraft of the [[World War I|First World War]] designed and built by the [[Port Victoria Marine Experimental Aircraft Depot]] on the [[Isle of Grain]]. A very small and light [[biplane]] intended to fly off platforms on [[Royal Navy]] [[Destroyer]]s, it was unsuccessful, only a single prototype being built.

==Design and development==
Following [[Royal Navy]] experience in operating land planes from platforms on ships, in late 1916, the British [[Admiralty]] came up with the idea of a lightweight fighter aircraft, capable of flying off short platforms on the [[forecastle]] of [[Destroyers]] in order to provide large numbers of aircraft at sea capable of intercepting and destroying German [[Airship]]s. It therefore instructed the Marine Aircraft Experimental Department at Port Victoria on the Isle of Grain, and the [[RNAS]] Experimental Flight at [[Eastchurch]] to each produce a design to meet this requirement.<ref name="Bruce V1 p180">Bruce 1965, p.180.</ref>

The Port Victoria aircraft, designed by W.H. Sayers, was designated '''P.V.7'''. It was a very small single bay [[Tractor configuration|tractor]] [[biplane]], of ''sesquiplane'' configuration, with its lower wing much smaller than its upper wing. The wings featured the same high-lift section as used in previous Port Victoria aircraft, and were fitted with [[aileron]]s only on the upper wing. It was intended, as was the competing Eastchurch design, to use a 45&nbsp;hp (34&nbsp;kW) geared [[ABC Gnat]] two-cylinder air-cooled engine. Armament was a single [[Lewis gun]] mounted above the upper wing.<ref name="Collyer p52">Collyer 1991, p.52.</ref><ref name="Mason Fighter p111">Mason 1992, p.111.</ref>

While the Port Victoria design was designed and built, the commander of the Experimental flight as Eastchurch, Harry Busteed took over command of the Port Victoria Marine Aircraft Experimental Department, taking the designer of the Eastchurch competitor and the part built prototype with him to the Isle of Grain, with the Eastchurch design gaining the Port Victoria designation [[Port Victoria P.V.8|P.V.8]].  The P.V.7 acquired the name '''Grain Kitten''' to distinguish it from the P.V.8, which was named the '''Eastchurch Kitten'''.

The P.V.7 first flew on 22 June 1917, powered by a 35&nbsp;hp (26&nbsp;kW) ungeared Gnat engine, as the geared engine was unavailable.<ref name="Mason Fighter p111"/>  The P.V.7 proved to be tail heavy in the air and difficult to handle on the ground, with its sesquiplane layout and high lift wings being considered unsuitable for such a small aircraft.  The Gnat engine proved to be extremely unreliable, with test flights being forced to remain within gliding distance of an airfield.<ref name="Bruce V1 p182"/>

When the P.V.8 first flew in September, it proved superior, although similarly hamstrung by the 35&nbsp;hp Gnat.  The P.V.7 was rebuilt with new wings of conventional aerofoil section, a modified tail and a new undercarriage to eliminate some of the problems found in testing.<ref name="Bruce V1 p182"/>  The low power and unreliability of the Gnat, however, prevented either aircraft being suitable for the intended use, and the P.V.7 was not flown after it was rebuilt.<ref name="Collyer p53">Collyer 1991, p.53.</ref>

==Specifications ==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane  
|jet or prop?=prop 
|ref=War Planes of the First World War: Volume One Fighters <ref name="Bruce V1 p182">Bruce 1965, p.182.</ref>

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=1
|capacity=
|length main= 14 ft 11 in
|length alt=4.55 m
|span main= 18 ft 0 in
|span alt= 5.49 m
|height main= 5 ft 3 in
|height alt= 1.60 m
|area main= 85 ft²
|area alt= 7.9 m²
|airfoil=
|empty weight main= 284 lb
|empty weight alt= 129 kg
|loaded weight main= 491 lb
|loaded weight alt= 223 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[ABC Gnat]]
|type of prop=two-cylinder horizontally opposed air-cooled
|number of props=1
|power main= 35 hp
|power alt= 26 kW
|power original=
   
|max speed main=74 knots
|max speed alt=85 mph, 137 km/h
|max speed more= at 6,500 ft (1,980 m)
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= <!-- nm -->
|range alt=<!--  mi,  km -->
|ceiling main= 11,900 ft
|ceiling alt=3,630 m
|climb rate main= <!-- ft/min-->
|climb rate alt= <!-- m/s-->
|loading main= 5.77 lb/ft²
|loading alt= 28.2 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main=0.071&nbsp;hp/lb
|power/mass alt=0.12 kW/kg
|more performance=*'''Climb to 6,500 ft (1,980 m):''' 10 min 50 s
|guns=1x [[.303 British|.303 in]] [[Lewis gun]] above upper wing.
|avionics=

}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=*[[Port Victoria P.V.8]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==
{{Reflist}}

*Bruce, J.M. ''War Planes of the First World War: Volume One Fighters''. London:Macdonald, 1965.
*Collyer, David. "Babies Kittens and Griffons". ''[[Air Enthusiast]]'', Number 43, 1991. Stamford, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;50–55.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland:Naval Institute Press, 1992. ISBN 1-55750-082-7.

<!-- ==External links== -->
{{Port Victoria Aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Sesquiplanes]]
[[Category:Port Victoria aircraft|PV7]]
[[Category:Single-engined tractor aircraft]]