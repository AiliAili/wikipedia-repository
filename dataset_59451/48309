{{Orphan|date=May 2014}}

'''Rosine Guiterman''' (1886–1960) was a Sydney activist, teacher, poet and humanitarian.<ref>Material for this article, unless otherwise referenced, is drawn from an anonymous document, apparently produced shortly after her death, available on the website of the University of Sydney’s Fisher Library at: http://hdl.handle.net/2123/9193</ref>
[[File:Rosine Guiterman.jpg|thumb|right|Rosine Guiterman]]

== Early life ==

Guiterman was born '''Rosine Lion''' in London in 1886 and came to Australia with her family in 1893. She [[Matriculation#Australia|matriculated]] from high school  and enrolled in Arts at [[Sydney University]]. She had a great love of [[Shakespeare]]'s plays, this love having been instilled in her since early childhood by her mother. Her interest in Shakespeare came to the notice of Sir [[Mungo William MacCallum|Mungo MacCallum]], Sydney University’s foundation Professor of Modern Language and Literature, and he inspired her to a lifetime study of Shakespeare. She graduated with a [[Bachelor of Arts]] degree in 1908.

In 1911 she gained her [[Master of Arts]] degree in English, having achieved considerable prominence as a poet. Indeed, her poem "Sic Vos Non Vobis" (''Thus do ye, but not for yourselves'') won the coveted Sydney University Prize for English verse. She was the first woman to achieve this distinction.

While at Sydney University she was very active in student activities, particularly in the Dramatic Society which gave her the skills which she would put to good use in her later commitment to theatre in Sydney. She also joined the Sydney University Women's Social Service Society and served on its committee from 1908 to 1910, being secretary from 1908 to 1909. In May 1909 she was instrumental in the Society and helped found the [[Sydney University Settlement]] which continues to provide social welfare services in the [[Chippendale, New South Wales|Chippendale]] and [[City of South Sydney|South Sydney]] area.<ref>http://thesettlement.org.au/History and http://thesettlement.org.au/file/view/TheSettlement_1891-1986.pdf/347390822/TheSettlement_1891-1986.pdf</ref> This was an early indication of her strong commitment to social causes that was to be such an important part of her later life. It was in this period she became a close friend of the Australian feminist [[Jessie Street]] and other early social activists.<ref>Marilyn Lake, Faith: Faith Bandler, Gentle Activist, Allen & Unwin, Crows Nest, 2002, pp. 69-70</ref>

== Travels ==

In 1911, Rosine set off for London by boat, and she fell in love with a businessman, David Guiterman, who had boarded the ship in Colombo, Sri Lanka. By the time they reached England they were engaged. David Guiterman was, and always remained, a convinced socialist, with progressive ideas on religion, politics and human morality. Until they met, such explicit political ideas had never come Rosine’s way.

From 1911 to 1912 Rosine Lion with her sister, a trained kindergarten teacher, travelled extensively in France and Germany, teaching as she went. She then returned to Australia and Rosine and David were married at the [[Great Synagogue, Sydney]] on 10 September 1913. Their twenty-nine years of married life, in spite of much trouble, were happy. Their married life began with very bright prospects, David having been appointed to represent a German firm doing good business in Australia. However, eleven months later the [[First World War]] broke out. There followed a period of increasing social repression in Australia, in which the Australian government destroyed the white multiculturalism and plurality that had characterized pre-World War I Australia.<ref>Gerhard Fischer, Enemy Aliens (University of Queensland Press, St Lucia, 1989)</ref> This was to particularly affect those many Australians of German descent and heritage or those, like David Guiterman, who were associated with this large and vibrant component of Australian society.<ref>John Docker in his “Dilemmas of Identity: The Desire for the Other in Colonial and Post Colonial Cultural History”, Working Papers in Australian Studies, No. 74 (Sir Robert Menzies Centre for Australian Studies, University of London 1992. P.13).</ref>

In this atmosphere of repression, the Australian Government confiscated the assets of David’s firm, including his own money, and things became very difficult indeed for the newlyweds. David was an American of German descent, with a German name. In consequence David found it impossible to get another job. Rosine was ostracised by many former friends and even urged by some to leave her husband "at least for the duration of the war". It was strongly suggested that David Guiterman should change his name, but this he proudly refused to do, saying that it was a name that his parents and grandparents had borne with honour. His stand was almost universally condemned. They were both unalterably opposed to war as a method of settling international disputes and did not fear to say so. Rosine proudly stood by him and with unshakable courage had her first experience of taking an unpopular stance. In all likelihood it was this experience that led her to fight for the rest of her life for social inclusion, tolerance and human rights.

<!--[insert photo of Rosine Guiterman here]
[insert photo of Rosine and david Guiterman here]-->
These{{which|date=September 2013}} photographs of Rosine and David, and of Rosine, were probably taken in 1940 or 1941. David Guiterman died in 1942.<ref>Photo source: Dr Fred Wechsler.</ref>
David and Rosine had two children, Gertrude and Pauline. Gertrude inherited her parents’ strong socialist and activist views. She married John Williamson Legge in 1940 and they were both active members of the [[Australian Communist Party]].<ref>Joyce Stevens Taking the Revolution Home: Work Among Women in the Communist Party of Australia: 1920-1945 Melbourne: Sybylla Co-operative Press and Publications, 1987, p.225-226]</ref>

== Teaching career ==

In 1916, due to their straitened family financial circumstances as a result of the seizure of David’s assets and his inability to gain employment, Rosine began to teach again - becoming a tutor of English and drama with the Workers' Educational Association. In 1917 after the birth of their two daughters, Rosine began part-time school teaching. Through her teaching career, three generations of school students benefited from her knowledge and love of the theatre. She worked tremendously hard. She may have neglected herself but never her family or her students. Her association with the W.E.A. lasted from 1916 to 1930, during which period she conducted very many classes and lectured at many summer schools.

Rosine Guiterman engaged in school teaching continuously until 1950, including at [[SCEGGS Darlinghurst]]. Her main subjects were English and History and she was always the staff member to teach English and History honours. However, from time to time she also taught French and Latin. During this period she made many school broadcasts which were well received by both teacher and student listeners.<ref>[http://trove.nla.gov.au/newspaper/result?q=rosine+Guiterman and http://nla.gov.au/nla.news-article17204548?searchTerm=rosine%20Guiterman&searchLimits=]</ref> In about 1940 Rosine commenced private coaching and continued to be a most successful teacher in this field up to the time of her death in 1960; her last lesson was given ten days before she died.

== Theatre ==

Throughout her life, Rosine made a significant contribution to theater in Australia. In 1925 she was a co-founder of the Workers' Education Association Drama League.<ref>Marilyn Lake Faith: Faith Bandler, Gentle Activist, Allen & Unwin, Crows Nest, 2002, pp. 69-70</ref> She was also active in the left-wing New Theatre in Sydney which was founded in 1932 and was where she could combine her theatrical skills with her social commitment. She acted in many plays, and gained early prominence through playing the leading role in Chekhov’s "The Schoolmistress". Rosine also gained a significant reputation through her innovative Shakespearean productions.  By 1938 she was sufficiently prominent in Sydney society to be the subject of an [[Archibald Competition]] painting by [[Joseph Wolinski]].<ref>http://m.artgallery.nsw.gov.au/prizes/archibald/1938/19422/</ref>
In 1933 Hitler came to power in Germany and there was a growing awareness among social activists in Australia of the growth in fascism in Germany. Rosine combined her commitment to the theatre with her concern at the growing menace of Nazism. On 21 November 1936 the ''[[Sydney Morning Herald]]'' reported:

<blockquote>"Mrs. Rosine Guiterman is producing Lessing's play, "Nathan the Wise," this evening at the Maccabean Hall. A prologue will be spoken by Miss Amalia Lessing, a lineal descendant of the author, and by Mr. Arthur Mendelssohn, a descendant of Lessing's dearest friend, Moses Mendelssohn, whose character and ideas are expressed in the play. Miss Grace Barrow has designed the costumes and settings after a beautiful production she saw in Zurich. The proceeds of the performance will benefit the German-Jewish Relief Fund".<ref>http://nla.gov.au/nla.news-article17289315</ref> </blockquote>

== Refugee assistance ==

During the latter part of the 1930s, any [[German Jews]] who could possibly do so, escaped to any country where they could gain admittance, and many came to Australia. During this period the Guiterman home was ever open to newcomers in trouble, just as David Guiterman's office door was also always open. These two once calculated that approximately 600 to 700 refugees had come to their home with some type of problem.

Many German children were coached in English by Rosine and subsequently shepherded through all ramifications of the Education Department, to ensure that they were graded according to ability and not held back through imperfect English. Many refugees, still with very little English, were assisted to obtain necessary medical or hospital attention. Education and medical assistance were just two of the host of problems which confronted the Jewish refugees, all of whom faced the trauma of being strangers in a strange land.

== Social activism ==

Immediately after [[World War Two]], the Peace Council came into being with the object of working for the replacement of war by international co-operation. From its very inception, Rosine Guiterman identified herself with this movement, assisting to the maximum of her capacity in every way possible. She was also very active in many progressive causes including the [[Australia-China Association]] and the [[Union of Australian Women]].

She was an active member of the [[Australian-Aboriginal Fellowship]] and took part in the movement that brought about the successful [[Australian referendum, 1967 (Aboriginals)|1967 referendum]] that changed the [[Constitution of Australia|Constitution]] to allow the Federal Government to make laws relating to Aboriginal Australians. In this struggle, she played a particularly strong role in challenging the official government policy of "assimilation". She urged the adoption of the term "integration" to signal support of Aborigines' retaining a sense of group identity.<ref>Marilyn Lake, Faith: Faith Bandler, Gentle Activist, Allen & Unwin, Crows Nest, 2002, pp. 69-70]</ref>

== Writer and poet ==

In 1949, despite her full-time work as a teacher and social activist, she found time to write a book, ''Harriet Newcomb and Margaret Hodge: A Short Account of Two Pioneers in Education'', in order to help to keep alive the memory of two early feminists and educators.

In 1949 she also published an article [“Emma Lazarus Centenary”, Unity: A Magazine of Jewish Affairs, (Vol. 2 No. 4.; Nov-Dec 1949; pp.&nbsp;8–9) ]  celebrating the centenary of the birth of the American poet [[Emma Lazarus]] and travelled to England where she became good friends with the poet Robert Browning who, Rosine reported, was delighted with her poems of indignation concerning abuses in the social system, or aiming at righting some wrong.

Her strong and public commitment to social justice, despite the censure then being levelled at activists during the [[Cold War]] by the conservative government of R.G. Menzies, was well illustrated in 1953. When the impending execution of [[Julius and Ethel Rosenberg]] on charges of espionage galvanized liberal reaction around the world, she drew on her poetic skills, for which she had been awarded the Sydney University medal in 1911, to write an impassioned attack on the ''Sydney Morning Herald'' in reaction to the paper’s headline: "The execution of the Rosenbergs will end a two years' legal battle".

Her commitment to her family, to social action, to the theatre and to teaching meant that she led a full and active life with what seemed an inexhaustible reserve of energy. In fact the day before she died, when her sight and physical strength had almost failed, she wrote a letter setting out clearly and concisely all details for the next function of the Peace Group of which she was the President.

== Notes ==

{{reflist|2}}

==References==
*  Ron Witton, “Rosine Guiterman: A Forgotten Australian Activist”, Australian Jewish Historical Society Journal, June 2014 (forthcoming).
* anonymous document, Fisher Library, University of Sydney’s  http://hdl.handle.net/2123/9193 . Retrieved on 20 June 2013
* http://thesettlement.org.au/History Retrieved on 20 June 2013  
* http://thesettlement.org.au/file/view/TheSettlement_1891-1986.pdf/347390822/TheSettlement_1891-1986.pdf Retrieved on 20 June 2013
* Marilyn Lake Faith: Faith Bandler, Gentle Activist, Allen & Unwin, Crows Nest, 2002, pp.&nbsp;69–70
* Gerhard Fischer, Enemy Aliens (University of Queensland Press, St Lucia, 1989). 
* John Docker  “Dilemmas of Identity: The Desire for the Other in Colonial and Post Colonial Cultural History”, Working Papers in Australian Studies, No. 74 (Sir Robert Menzies Centre for Australian Studies, University of London 1992. P.13  
*Gloria Garton in Joyce Stevens Taking the Revolution Home: Work Among Women in the Communist Party of Australia: 1920-1945 Melbourne: Sybylla Co-operative Press and Publications, 1987, p.&nbsp;225-226
* http://trove.nla.gov.au/newspaper/result?q=rosine+Guiterman Retrieved on 20 June 2013 and * http://nla.gov.au/nla.news-article17204548?searchTerm=rosine%20Guiterman&searchLimits= Retrieved on 20 June 2013
* http://m.artgallery.nsw.gov.au/prizes/archibald/1938/19422/ Retrieved on 20 June 2013
* http://nla.gov.au/nla.news-article17289315 Retrieved on 20 June 2013
* “Emma Lazarus Centenary”, Unity: A Magazine of Jewish Affairs, (Vol. 2 No. 4.; Nov-Dec 1949; pp.&nbsp;8–9).

{{DEFAULTSORT:Guiterman, Rosine}}
[[Category:1886 births]]
[[Category:1960 deaths]]
[[Category:Australian anti-war activists]]
[[Category:Australian schoolteachers]]
[[Category:University of Sydney alumni]]
[[Category:English emigrants to Australia]]