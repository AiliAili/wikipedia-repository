{{Use dmy dates|date=May 2013}}
{{Infobox journal
| title = International Small Business Journal
| cover = [[File:International Small Business Journal front cover image.jpg]]
| editor = Robert Blackburn
| discipline = [[Economics]]
| former_names = European Small Business Journal (until 1983, {{ISSN|0264-6560}})
| abbreviation = Int. Small Bus. J.
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 1982-present
| openaccess =
| license = 
| impact = 1.397
| impact-year = 2013
| website = http://isb.sagepub.com/
| link1 = http://isb.sagepub.com/content/current
| link1-name = Online access
| link2 = http://isb.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 67056182
| LCCN = 86015275
| CODEN = 
| ISSN = 0266-2426
| eISSN = 1741-2870
}}
The '''''International Small Business Journal''''' ('''ISBJ''') is a [[Peer review|peer-reviewed]] [[academic journal]] that covers the fields of [[economics]] and [[entrepreneurship]], especially  [[small business]]es. The journal's [[editor-in-chief]] is Robert Blackburn ([[Kingston University]]). It was established in 1982 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Current Contents]], the [[Business Periodicals Index]], the [[International Bibliography of the Social Sciences]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.397, ranking it 55th out of 110 journals in the category "Business"<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: business |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-08-24 |work=Web of Science |postscript=.}}</ref> and 70th out of 172 journals in the category "Management".<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: management |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science |postscript=.}}</ref>

== Virtual Issues ==
The ''International Small Business Journal'' has brought together several collections of research, previously published in the journal, in ‘virtual issues’ centered on key themes in the field. Recent virtual issues include ''Female Entrepreneurship'', which explores the journal’s contribution to female entrepreneurship research,<ref>[http://isb.sagepub.com/site/Virtual_Special_Issues/Female_Entrepreneurship.xhtml ''Female Entrepreneurship'']</ref> and ''Remembering Jason Cope'' which celebrates the life, and academic legacy, of the late entrepreneurship scholar.<ref>[http://isb.sagepub.com/site/Virtual_Special_Issues/Jason_Cope_In_Memoriam.xhtml ''Remembering Jason Cope'']</ref>

== Best Paper Award ==
In 2008 ''The International Small Business Journal'' founded the ISBJ Best Paper Prize, which is awarded annually to the authors of one paper, published in the journal, in the year of the award. The papers are assessed in terms of "focus […]; theoretical positioning; methodological rigour; policy and or practitioner implications; and interest for future research activity".<ref>{{Cite web |url=http://isb.sagepub.com/site/includefiles/isb_best_papers.xhtml|title= ISBJ Best Paper Prizes |accessdate=29 September 2014}}</ref>

==Editors==
*Editor-in-chief: Robert Blackburn
*Editor: Susan Marlow
*Consulting editors: Dirk De Clercq, Rachel Doern, Francis Greene, Vishal Gupta, Robert T Hamilton, Dilani Jayawarna, Peter Jennings, Teemu Kautonen, Einar Rasmussen, [[:fr:Erno Tornikoski|Erno Tornikoski]]
*Social Media Editor: Sarah Drakopoulou Dodd<ref>[http://www.sagepub.com/journals/Journal201573/boards]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://isb.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1982]]
[[Category:Business and management journals]]
[[Category:Bimonthly journals]]