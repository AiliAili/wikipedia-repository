<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox Aircraft Begin
  |name = B-2<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Blackburn B2 flying.jpg
  |caption = Blackburn B2 at the [[Shuttleworth Collection]]
}}{{Infobox Aircraft Type
  |type = Trainer
  |manufacturer = [[Blackburn Aircraft]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = [[1931 in aviation|1931]]
  |introduced = [[1932 in aviation|1932]]
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = [[1942 in aviation|1942]]
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Air Force]]
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 42
  |unit cost = 
  |developed from = [[Blackburn Bluebird IV]]<!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Blackburn B-2''' was a [[United Kingdom|British]] [[biplane]] side-by-side [[Trainer (aircraft)|trainer]] aircraft of the 1930s. Designed and built by [[Blackburn Aircraft]], 42 were built.

==Development==
The '''Blackburn B-2''' was developed by Blackburn as a successor for its earlier [[Blackburn Bluebird|Bluebird IV]] trainer, retaining the layout and side-by-side seating of the earlier aircraft, but having a semi-[[monocoque]] all-metal [[fuselage]], instead of the metal and fabric-covered fuselage used by the earlier aircraft. The single-[[interplane strut|bay]] [[biplane]] wings were of similar structure to those of the Bluebird IV and could be folded for easy storage. [[Leading edge slot]]s were fitted to the upper wing to improve low-speed handling, with [[aileron]]s on the lower wings only. The [[conventional landing gear]] was fixed, with the mainwheels supported on telescopic legs and a spung tailskid.<ref name="Jack black p330">Jackson 1968, p. 330.</ref> The prototype B-2 (registered ''G-ABUW'') first flew on 10 December 1932,<ref name ="donald world">Donald 1997, p. 127.</ref> powered by a {{convert|120|hp|kW|abbr=on}} [[de Havilland Gipsy]] III engine, although the {{convert|130|hp|kW|abbr=on}} [[de Havilland Gipsy Major]] and 120&nbsp;hp [[Cirrus Hermes]] IV engines were also fitted to production aircraft.<ref name="Jack black p331"/><ref name="Jack black p336">Jackson 1968, p. 336.</ref> Testing proved successful, with the aircraft proving to be very manoeuvrable, and the first production aircraft flew in 1932.<ref name="Jack black p331">Jackson 1968, p. 331.</ref>

==Operational history==
The B-2 was aimed mainly at the military trainer market,<ref name="Jack black p332">Jackson 1968, p. 332.</ref> and the prototype B-2 was shipped to [[Lisbon]] in September 1933 for evaluation by [[Portugal]]. Although it performed well in the evaluation, the Portuguese preferred a tandem layout, and purchased the [[de Havilland Tiger Moth]].<ref name="Jacksonv1 p219">Jackson 1974, p. 219.</ref> Although not successful in competing for major military orders, the B-2 continued in production to equip civilian flying schools in the United Kingdom that were busy training pilots for the [[Royal Air Force]] under the RAF expansion scheme, with the B-2 equipping flying schools owned by Blackburn at [[Brough Aerodrome]] and [[London Air Park|London Air Park, Hanworth]]. A total of 42 B-2s, including the prototype, were built, with production continuing until 1937.<ref name="Jack black p333-4">Jackson 1968, pp. 333–334.</ref>

The last three B-2s were sold to the Air Ministry and issued to the Brough flying school where they were operated in RAF markings.<ref name="Jack black p333-4"/>

On the outbreak of the [[World War II|Second World War]], the aircraft at Hanworth were moved to Brough, where the two training schools merged, becoming No 4 Elementary Flying Training School.<ref name="Jack black p334">Jackson 1968, p. 334.</ref> The school at Brough continued to be operated by Blackburn, with the aircraft remaining with civilian registrations (although they were repainted with wartime training markings with yellow fuselages, camouflaged wings and RAF roundels).<ref name="Jacksonv1 p220">Jackson 1974, p. 220.</ref> The remaining aircraft were taken over by the RAF in February 1942,<ref name ="donald world"/> being handed over to the [[Air Training Corps]], where they were used as instructional airframes.<ref name="Jack black p334"/>

==Operators==
;{{UK}}
*[[Royal Air Force]]
**[[No. 4 EFTS]]

==Survivors==
[[File:BlackburnB2.JPG|thumb|right|Blackburn B-2 at Old Warden]]
Only two B-2s survived to fly postwar; one crashed in 1951,<ref name="Jacksonv p220-1">Jackson 1974, pp. 220–221.</ref> and the sole survivor (G-AEBJ) is preserved and maintained in airworthy condition by Blackburn (now part of [[British Aerospace]]). G-AEBJ is located with the [[Shuttleworth Collection]] at [[Old Warden]].<ref>{{cite web |url= http://www.aeroplanemonthly.com/news/Blackburn_B2_biplane_trainer_moves_to_the_Shuttleworth_Collection_news_114797.html|title=Aeroplane Monthly - Blackburn B2 moves to the Shuttleworth Collection|accessdate=2007-06-23 |date= 2007-04-01|work= }}</ref>
Another fuselage was for many years seen up a tree in an Essex scrapyard before being rescued in the 1980s. The aircraft displays two identities, G-ACBH and G-ADFO and is preserved, still wearing its original paint, at the [[South Yorkshire Aircraft Museum]].

==Specifications (B-2)==
[[File:BlackBB2.jpg|thumb|right|The first production B-2]]   
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=British Civil Aircraft since 1919, Volume 1 <ref name="Jacksonv1 p221">Jackson 1974, p. 221.</ref> 
|crew=two
|capacity=
|length main= 24 ft 3 in
|length alt= 7.39 m
|span main= 30 ft 2 in
|span alt= 9.20 m
|height main= 9 ft 0 in
|height alt= 2.74 m
|area main= 246 ft²
|area alt= 22.9 m²
|airfoil=
|empty weight main= 1,175 lb
|empty weight alt= 534 kg
|loaded weight main= 1,850 lb
|loaded weight alt= 841 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[de Havilland Gipsy]] III
|type of prop=4 cylinder in-line engine
|number of props=1
|power main= 120 hp
|power alt= 90 kW
|power original=
|max speed main= 97 kn
|max speed alt= 112 mph, 180 km/h
|cruise speed main= 83 kn
|cruise speed alt= 95 mph, 153 km/h
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 278 nmi
|range alt= 320 mi, 515 km
|ceiling main=  
|ceiling alt=  
|climb rate main= 700 ft/min
|climb rate alt= 3.6 m/s
|loading main= 7.52 lb/ft²
|loading alt= 36.7 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.065 hp/lb
|power/mass alt= 0.11 kW/kg
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related= *[[Blackburn Bluebird]]
*[[Blackburn Bluebird IV]]<!-- related developments -->
|similar aircraft= *[[de Havilland Tiger Moth]]
*[[Miles Magister]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

*{{cite book|author=Donald, David (Editor)|title = The Encyclopedia of World Aircraft|year = 1997|publisher = Aerospace Publishing|isbn= 1-85605-375-X}}
*{{cite book|last=Jackson|first=A.J.|title=Blackburn Aircraft since 1909|year=1968|publisher=Putnam|location=London|isbn=0-370-00053-6}}
*{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919, Volume 1|year= 1974|publisher= Putnam|location= London|isbn=0-370-10006-9 }}

==External links==
{{commons category|Blackburn B-2}}
* [http://www.happyorange.org.uk/blackburn-b2-trainer/ Audio recording of the Blackburn B2]

{{Blackburn aircraft}}

[[Category:British civil trainer aircraft 1930–1939]]
[[Category:Blackburn aircraft|B-02]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]