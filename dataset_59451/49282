{{Infobox political party
| country = Denmark
| native_name = 
| colorcode = #cc0000
|logo = LibSoc-logo.png
| caption = The logo of Libertære Socialister (Denmark)
| leader = Collective leadership
| foundation = {{Start date|2009|11|08|df=yes}}
| ideology = [[Anarchist Communism]], [[Platformism]], [[Especifismo]], [[Libertarian Socialism]], [[Anarcho-Syndicalism]], [[Social Anarchism]], [[Social insertion]]
| position = 
| international = [[Anarkismo.net|Anarkismo]]
| european = [[EuroAnarkismo]]
| colours = 
| website = [http://libsoc.dk/ libsoc.dk]
}}

'''Libertære Socialister''' (abbreviated to '''LS''') is a Danish political left-wing organization which was founded on November 8, 2009 at its third initial meeting in [[Horsens]]. The organisation is working on the political ground of an anti-capitalist, revolutionary and [[Libertarian socialism|libertarian socialist]] platform.<ref>{{cite web|url=http://libsoc.dk/?page_id=1264 |title=Libertære Socialister » Platform |publisher=Libsoc.dk |date=2011-03-27 |accessdate=2013-08-01}}</ref> LS has local groups in [[Copenhagen]], [[Aalborg]] and [[Odense]] and these groups are linked together in a [[Federalism#Federalism as the Anarchist Mode of Political Organization|federation]], which is described in details the organization's rules.<ref>{{cite web|url=http://libsoc.dk/?page_id=1261 |title=Libertære Socialister » Regler |publisher=Libsoc.dk |date=2011-03-27 |accessdate=2013-08-01}}</ref> There is no official number of members, but according to a report from Roskilde University LS has approximately 300 members and is described as "''the most important and most visible representative of the anarchist trend in Denmark''".<ref>{{cite web|url=http://diggy.ruc.dk/bitstream/1800/9344/1/Political_extremism_in_Denmark.pdf#page=31&zoom=100,84,370 |format=PDF |title=Political Extremism in Denmark |author=Chris Holmsted |publisher=Diggy.ruc.dk |accessdate=26 February 2015}}</ref> LS describe themselves on their website as "''a federation of independent local groups that seek to promote the fight for [[libertarian socialism]]: a [[Anarchist communism|stateless socialist]] council society based on [[Federalism#Federalism as the anarchist and libertarian socialist mode of political organization|federalism]] and [[direct democracy]].''".<ref>{{cite web|url=http://libsoc.dk/?page_id=4 |title=Libertære Socialister » Om LS |publisher=Libsoc.dk |date= |accessdate=2013-08-01}}</ref> LS is an anti-parliamentarist organization and reject any participation in elections. Instead LS advocates and uses the [[direct action|direct-action-methods]] of [[Black Flame: The Revolutionary Class Politics of Anarchism and Syndicalism (Counter-Power vol. 1)#Mass anarchism (including syndicalism) versus insurrectionist anarchism|mass anarchism]] and [[Anarcho-Syndicalism|syndicalism]]: strikes, blocades, occupations, [[sabotage]], [[work-to-rule]], [[boycotts]], [[slowdown]], demonstrations, etc. LS consider the revolutionary [[general strike]] and the popular insurrection as the ultimate weapons that can end [[capitalism]] and [[statism]] and bring about a libertarian socialist society.

==Activities==
The local LS-groups arranges lectures and discussion meetings and get involved in political struggles and movements relating to environment and climate, [[Class conflict|industrial and social conflicts]], [[anti-racism]], [[Antimilitarism|anti-militarism]], [[Queer anarchism|gender politics]] etc.<ref>{{cite web|url=http://www.socialister.dk/avis/visartikel.asp?art=31338 |title="Vi kæmper for en revolution" : Socialistisk Arbejderavis 313 |publisher=socialister.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://www.autonominfoservice.net/2012/01/09/interview-vigtig-at-sammenk%C3%A6de-socialisme-og-anarkisme/ |title=Interview: "Vigtig at sammenkæde socialisme og anarkisme..." |publisher=autonom infoservice |date=2012-01-09 |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://www.autonominfoservice.net/2012/11/27/libert%C3%A6re-socialister-vi-har-brug-for-en-organisering-som-forbinder-de-kampe-vi-har-haft/ |title=Libertære Socialister: "Vi har brug for en organisering som forbinder de kampe vi har haft ..." |publisher=autonom infoservice |date=2012-11-27 |accessdate=2013-08-01}}</ref> The organisation has in several cases defended the use of political militancy under certain circumstances.<ref>{{cite web|url=http://www.dr.dk/P1/Klubvaerelset/Udsendelser/2011/11/16122923.htm |title=Militans - P1/Klubværelset |publisher=DR.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|author=Af Søren Astrup |url=http://politiken.dk/indland/ECE1618368/rapport-viser-danske-ekstremisters-vaesen/ |title=Rapport viser danske ekstremisters væsen |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref> During the protests at the [[2009 United Nations Climate Change Conference|UN Climate Change Conference (COP15)]] in 2009, 913 were arrested in a mass arrest and as part of this maneuver all involved in the LS block in the demonstration were arrested.<ref>{{cite web|author=Af Frank Hvilsom |url=http://politiken.dk/indland/ECE858699/anholdt-demonstrant-anholdelser-var-tortur/ |title=Anholdt demonstrant: "Anholdelser var tortur" |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=tc2JTIiP-Ak |title=COP15 Demonstration Copenhagen Denmark 20091212 Massive Arrest |publisher=YouTube |date=2009-12-13 |accessdate=2013-08-01}}</ref> Many of those arrested sued the police for wrongful arrest and at the end of the case the arrests were found to be illegal,<ref>{{cite web|author=Af Søren Astrup og Frank Hvilsom |url=http://politiken.dk/indland/ECE1143990/byretten-masseanholdelser-var-ulovlige/ |title=Byretten: Masseanholdelser var ulovlige |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref> and the police were ordered to pay damages.<ref>{{cite web|url=http://politiken.dk/klima/ECE1843288/politiet-har-betalt--millioner-til-anholdte-under-klimatopmoedet/ |title=Politiet har betalt millioner til anholdte under klimatopmødet |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref> At the labor movements [[International Workers' Day|May Day]] meetings in 2013<ref>{{cite web|url=http://localeyes.dk/65736-2816-revolutionaer-1-maj/ |title=Revolutionær 1. Maj |publisher=Localeyes.dk |date= |accessdate=2013-08-01}}</ref> LS participated in protests against the [[Social Democrats (Denmark)|Social Democrats]] and the [[Socialist People's Party (Denmark)|Socialist People's Party]] and the policy they have led after the change of government in 2011 (In Copenhagen LS played a leading role in these protests).<ref>{{cite web|author=Af Rune Eltard-Sørensen |url=http://modkraft.dk/artikel/derfor-blev-regeringen-angrebet-i-f%C3%A6lledparken |title=Derfor blev regeringen angrebet i Fælledparken &#124; Modkraft |language=da |publisher=Modkraft.dk |date= |accessdate=2013-08-01}}</ref> During the protests there were claims by both sides of aggressive behaviour from the other side,<ref>{{cite web|author=Af Søren Astrup |url=http://politiken.dk/politik/ECE1960382/sfer-ubehagelige-oplevelser-1-maj/ |title=SF'er: Ubehagelige oplevelser 1. maj |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|author=Af Peter Burhøi |url=http://www.b.dk/politiko/under-den-roede-fane |title=Under den røde fane |publisher=www.b.dk |date= |accessdate=2013-08-01}}</ref> some of which were later retracted.<ref>{{cite web|author=Af Søren Munch |url=http://jyllands-posten.dk/politik/ECE5418267/sf-minister-jeg-blev-altsa-ikke-slaet/ |title=SF-minister: Jeg blev altså ikke slået |language=da |publisher=Jyllands-posten.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|author=Af Rune Eltard-Sørensen |url=http://modkraft.dk/artikel/venstreradikale-afviser-politikeroverfald |title=Venstreradikale afviser politikeroverfald &#124; Modkraft |language=da |publisher=Modkraft.dk |date= |accessdate=2013-08-01}}</ref> The protests had the effect that speakers from the Social Democrats and SF had to interrupt their speeches ahead of time at the meetings in [[Copenhagen]], [[Aalborg]], [[Aarhus]] and other cities.<ref>{{cite web|author=Af Jesper Friis |url=http://politiken.dk/politik/ECE1958863/politikendks-reporter-midt-i-pibekoncerten-thorning-fik-ikke-en-chance/ |title=Politiken.dk's reporter midt i pibekoncerten: "Thorning fik ikke en chance" |language=da |publisher=Politiken.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://spip.modkraft.dk/tidsskriftcentret/tidslinje/article/2013#20130501 |title=2013 · Modkraft.dk |publisher=Spip.modkraft.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://hvidbog.revo1maj.dk/ |accessdate=July 20, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130508173350/http://hvidbog.revo1maj.dk/ |archivedate=May 8, 2013 }}</ref>

==''Direkte Aktion''==
LS publishes the magazine ''Direkte Aktion'', which began to be published in February 2011.<ref>{{cite web|url=http://libsoc.dk/?page_id=779 |title=Libertære Socialister » Direkte Aktion |publisher=Libsoc.dk |date=2011-02-21 |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://www.anarkismo.net/article/18855 |title=Direkte Aktion, No 1 - Vol.1 |publisher=Anarkismo |date=2011-02-22 |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://www.autonominfoservice.net/2013/06/25/direkte-aktion-p%C3%A5-tryk/ |title=DIREKTE AKTION – på tryk! |publisher=autonom infoservice |date=2013-06-25 |accessdate=2013-08-01}}</ref>

==International relations==
Internationally, LS joined the [[Anarkismo.net|Anarkismo-network]]<ref>{{cite web|url=http://www.anarkismo.net/about_us |title=About Us |publisher=Anarkismo |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://libsoc.dk/?p=638 |title=Libertære Socialister » Fra vores egen verden: LS’ 2. ordinære kongres |publisher=Libsoc.dk |date= |accessdate=2013-08-01}}</ref> and is helping to edit the website [[Anarkismo.net]]<ref>{{cite web|url=http://www.anarkismo.net/ |title=Features |publisher=Anarkismo |date= |accessdate=2013-08-01}}</ref> and the ''Anarchist Black Cat'' forums.<ref>{{cite web|url=http://www.anarchistblackcat.org/ |accessdate=July 20, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130530205243/http://www.anarchistblackcat.org/ |archivedate=May 30, 2013 }}</ref> LS participates in international conferences of the [[Anarkismo.net|Anarkismo-network]] and is working together with other affiliated organizations on joint campaigns. The [[Anarkismo.net|Anarkismo-network's]] European branch has created ''EuroAnarkismo''<ref>{{cite web|url=http://europa.anarkismo.org/ |title=Who we are &#124; EuroAnarkismo |publisher=Europa.anarkismo.org |date= |accessdate=2013-08-01}}</ref> with the aim of strengthening the international cooperation and the anarchist movement in Europe.<ref>{{cite web|url=http://libsoc.dk/?p=1039 |title=Libertære Socialister » Europæisk Anarkismo-konference 2011 |publisher=Libsoc.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://libsoc.dk/?p=1230 |title=Libertære Socialister » De europæiske Anarkismo-organisationers konference i London |publisher=Libsoc.dk |date= |accessdate=2013-08-01}}</ref><ref>{{cite web|url=http://www.anarkismo.net/article/19100 |title=Conference of European Anarkismo organizations in London |publisher=Anarkismo |date= |accessdate=2013-08-01}}</ref> By joining the [[Anarkismo.net|Anarkismo-network]] LS has placed itself within the [[Platformism|platformist]]-[[Especifismo|especifist]] tradition of the international [[Anarchism|anarchist]] movement.<ref>{{cite web|url=http://anarchistplatform.wordpress.com/ |title=Anarchism and the Platformist Tradition &#124; An Archive of Writings on the Platformist Tradition within Anarchism |publisher=Anarchistplatform.wordpress.com |date= |accessdate=2013-08-01}}</ref>

== See also ==
* [[Anarchism]]
* [[Anarchist communism]]
* [[Platformism]]
* [[Especifismo]]
* [[Anarkismo.net]]
* [[Libertarian socialism]]
* [[Anarcho-syndicalism]]
* [[Social anarchism]]
* [[Workers' self-management]]
* [[Social insertion]]
* [[Black Flame: The Revolutionary Class Politics of Anarchism and Syndicalism (Counter-Power vol. 1)|Black Flame]]
* [[Dielo Truda]]
* [[Friends of Durruti Group]]
* [[Makhnovism]]

==References==
{{Reflist|colwidth=30em}}

==External links==
* [http://libsoc.dk/ Official website]
* [http://libsoc.dk/?page_id=779 The magazine Direkte Aktion]
* [http://www.anarkismo.net/ Anarkismo.net]
* [http://europa.anarkismo.org/ EuroAnarkismo]
* [http://anarchistplatform.wordpress.com/ An Archive of Writings on the Platformist Tradition within Anarchism]

{{Platformism}}
{{Anarchism}}

{{DEFAULTSORT:Libertaere Socialister}}
[[Category:Anarchism in Denmark]]
[[Category:Platformist organizations]]
[[Category:Platformism]]
[[Category:Politics of Denmark]]
[[Category:Anarchist communism]]
[[Category:Anarchism]]
[[Category:Libertarian socialist organisations]]
[[Category:Social anarchism]]
[[Category:Anarcho-syndicalism]]
[[Category:Anarchist organizations in Denmark]]
[[Category:Socialist parties in Denmark]]