<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= DA42 Twin Star
 |image=OH-DAC_Tour_de_Sky_Oulu_20140810_02.JPG
 |caption=
}}{{Infobox Aircraft Type
 |type=Light twin-engined utility and [[trainer aircraft]]
 | national origin= [[Austria]]
 |manufacturer=[[Diamond Aircraft Industries]]
 |designer=
 |first flight=9 December 2002
 |introduced=2004
 |retired=
 |status=In production<ref name="AvWeb16Mar09" />
 |primary user=
 |more users=
 |produced=
 |number built=
 |unit cost=US$600,000
 |variants with their own articles= [[Aeronautics Defense Dominator]]
}}
|}

[[File:diamond da42 twin star hb-sdm riat 2010 arp.jpg|thumb|right|DA42 Twin Star]]
[[File:DA42Panel.JPG|thumb|Diamond DA42 Twin Star instrument panel showing the [[Garmin]] [[G1000]] glass cockpit installation]]
[[File:DiamondDA-42TwinStarN131TSThielertEngineDetail.jpg|thumb|Diamond DA42 Twin Star [[Thielert]] Centurion 1.7 diesel engine]]
[[File:DiamondDA-42TwinStarN131TS04.jpg|thumb|right|Diamond DA42 Twin Star showing prominent [[Wingtip device|winglets]]]]
The '''Diamond DA42 Twin Star''' is a four seat, twin engine, propeller-driven [[airplane]] manufactured by [[Diamond Aircraft Industries]]. Its airframe is made largely of [[composite material]]s.<ref name='DA42 composite'>{{cite web |url=http://www.airforce-technology.com/projects/diamond-da42-twin-star-utility-trainer/ |title=Diamond DA42 Twin Star Utility and Trainer Aircraft, Austria |work=Airfoce-technology.com |accessdate=2014-08-19 }}{{Unreliable source?|reason=domain on WP:BLACKLIST|date=September 2016}}
</ref>

==Development==
The DA42 Twin Star was certified in Europe in 2004<ref>{{cite web|url = http://www.diamondair.com/news/05_13_04.php|title = DA42 Twin Star EASA certification|last = Diamond Aircraft|date=May 2004}}</ref> and in the United States in 2005.<ref>{{cite web|url = http://www.diamondair.com/news/07_26_05.php|title = DA42-TDI Twin Star FAA certified|last = Diamond Aircraft|date=July 2005}}</ref>

The airplane is made of carbon composite material.<ref name='DA42 composite'/>  
It is equipped with a [[Garmin G1000]] glass cockpit.<ref>{{cite web|url=http://www.diamondair.com/news/04_28_03.php |title=Garmin's G1000 System offered in the DA42 Twin Star |accessdate=2008-04-15 |last=Diamond Aircraft |authorlink= |year=2003 |archiveurl=https://web.archive.org/web/20080511074609/http://www.diamondair.com/news/04_28_03.php |archivedate=2008-05-11 |deadurl=yes |df= }}</ref>

The DA42 Twin Star was the first diesel-powered [[fixed-wing aircraft]] to make a non-stop crossing of the North Atlantic, in 12.5 hours, with an average fuel consumption of {{convert|21.73|litre}} per hour ({{convert|10.86|litre}} per hour per engine).<ref>{{cite web|url=http://www.diamondair.com/news/08_20_04.php |title=DA42 Twin Star crosses Atlantic non-stop: first Atlantic crossing of a diesel powered aircraft |accessdate=2008-04-15 |last=Diamond Aircraft |authorlink= |year=2004 |archiveurl=https://web.archive.org/web/20080331041108/http://www.diamondair.com/news/08_20_04.php |archivedate=2008-03-31 |deadurl=yes |df= }}</ref>

In June 2010 a DA42 powered by [[Austro AE300]] engines became the first aircraft to be publicly flown on algae-derived jet fuel.<ref name="AvWeb09Jun10">{{cite web|url = http://www.avweb.com/avwebflash/news/algae_fuel_diamond_da42_berlin_efficient_202700-1.html |title = EADS: Algae-Fueled DA42 A "World's First" (And Better)|accessdate = 10 June 2010|last = Pew|first = Glenn|authorlink = |date=June 2010}}</ref>

By March 2012 the DA42 had become the major income driver at Diamond Aircraft. Company CEO Christian Dries indicated that the market focus of the company had been changed by the recession of 2008—2010 and that the company now derives two-thirds of its revenue from military and government contracts, primarily for manned and unmanned ([[Aeronautics Defense Dominator]]) [[surveillance aircraft]].<ref name="Bertorelli13Mar12A">{{cite news|url = http://www.avweb.com/blogs/insider/AvWebInsider_SensorTech_206312-1.html|title = Diamond's Commercial/Military Turn|accessdate = 15 March 2012|last = Bertorelli|first = Paul|date = 13 March 2012| work = AVweb}}</ref>

Also in March 2012 Diamond aircraft announced they were developing a [[fly-by-wire]] version of the DA42, with the aim of reducing the accident rate in light aircraft. The system is expected to eventually include [[flight envelope protection]], turbulence righting and [[autoland]] capabilities. The system will also include damage-tolerant by-pass capabilities, allowing flight with jammed or missing controls.<ref name="Bertorelli13Mar12B">{{cite news|url = http://www.avweb.com/avwebflash/news/DiamondsDries_FlyByWireAsElectronicParachute_206315-1.html|title = Diamond's Dries: Fly By Wire As "Electronic Parachute"|accessdate = 15 March 2012|last = Bertorelli|first = Paul|date = 13 March 2012| work = AVweb}}</ref><ref name="Pew29Nov12">{{cite news|url = http://www.avweb.com/avwebflash/news/da42_flybywire_diamond_autonomous_control_pilot_207775-1.html|title = DA42 Fly By Wire Progress|accessdate = 3 December 2012|last = Pew|first = Glenn|date = 29 November 2012| work = AVweb}}</ref> The autonomous DA42 was flown and landed without ground support in 2015.<ref name="Bertorelli-9sep15">{{cite news|url = http://www.avweb.com/avwebflash/news/Diamonds-Autonomous-DA42-Completes-Autolanding-Tests-224826-1.html |title = Diamond's Autonomous DA42 Completes Autolanding Tests |accessdate = 9 September 2015|last = Bertorelli|first = Paul|date = 8 September 2015| work = AVweb}}</ref><ref>{{youtube|RlaDeojkLcU|Diamond DA42 Autoland}}</ref>

==Design==
The DA42 is a composite-constructed, twin-engined, low-wing cantilever monoplane with a retractable tricycle landing gear and a T-tail. The enclosed cabin has four seats with a front-hinged canopy for access to the front seats and a top-hinged door on the left side for access to the rear seats.

==Powerplants==
The DA42NG "New Generation" is powered by Diamond's 3rd Generation Austro Turbo Diesel Engine. The Lycoming IO-360 engine is also available as an option.  The {{convert|125|kW|hp|0|abbr=on}} Austro diesel replaces the Thielert Centurion 1.7 and 2.0 engines. It is known for its good fuel efficiency, it uses {{convert|12.1|litre}}  per hour while loitering at maximum endurance or {{convert|30|litre|-0}} per hour at maximum continuous power (92%).  It is also available with optional "on top" exhaust mufflers that reduce noise levels to below 59 decibels at a height of {{convert|500|ft|-1}}.<ref name="Diamond Airborne Sensing Facts">{{cite web|url = http://www.diamond-sensing.at/index.php?id=2239 |title = Diamond Airborne Sensing Facts}}</ref><ref name="Diamond Aircraft News">{{cite web|url = http://www.diamondaircraft.com/news/index.php |title = Diamond Aircraft News}}</ref><ref name="Clarity Aerial Sensing Law Enforcement">{{cite web|url = http://www.clarityaerialsensing.com/law-enforcement.php |title = Clarity Aerial Sensing Law Enforcement}}</ref>

Thielert Aircraft Engines ended its production of the 1.7 litre Centurion engines (designated as TAE 125-01 Centurion 1.7) in favour of a new 2.0 litre. (TAE 125-02-99) engine.<ref>{{cite web|url = http://web.thielert.com/typo3/index.php?id=660&backPID=660&begin_at=10&tt_news=863&L=1|title = Thielert engine in next development stage|accessdate = 2008-04-15|last = Thielert Aviation Engines|authorlink = |date=January 2007}}</ref>  Diamond began installing this new 2.0 L. engine in early 2007.  Although engine displacement increased, it was de-rated to produce the same power {{convert|101|kW|abbr=on}} and torque {{convert|409|Nm|abbr=on}} as the 1.7 L. engine.<ref>{{cite web|url = http://www.diamondair.com|title = Diamond Aircraft Product pages}}</ref>

In late 2007, Diamond aircraft announced it would begin building and installing its own aerodiesels, through a subsidiary, Austro Engine GmbH, and with other partners that included Mercedes Benz Technologies. The use of Thielert engines on the DA42 came into question due to Thielert filing for insolvency in April 2008.<ref name="AvWeb24Apr08">{{cite web|url = http://www.avweb.com/avwebflash/news/MoreTroubleFor_Thielert_197722-1.html|title = More Trouble For Thielert|accessdate = 2008-04-24 |last = Niles |first= Russ|authorlink = |date=April 2008}}</ref><ref name="AvWeb05May08">{{cite web|url = http://www.avweb.com/avwebflash/news/CessnaSuspendsDiesel172SalesDiamondStepsUpSupport_197780-1.html |title = Cessna Suspends Diesel 172 Sales, Diamond Steps Up Support |accessdate = 2008-05-05 |last = AvWeb Staff|authorlink = |date=May 2008}}</ref><ref name="Diamond Says Thielert Insolvency Administrator Is Playing Hardball">{{cite web|url = http://www.aero-news.net/index.cfm?ContentBlockID=6dd57ffd-afc0-4bf4-81dc-823a4ec89292 |title = Diamond Says Thielert Insolvency Administrator Is Playing Hardball |accessdate = 2008-04-24}}</ref><ref name="Thielert: How To Kill A Company (Maybe Two)">{{cite web|url = http://www.avweb.com/blogs/insider/AVwebInsiderBlog_Thielert_HowToKillACompany_197956-1.html |title = Thielert: How To Kill A Company (Maybe Two) |accessdate = 2008-05-27 |last = Bertorelli | first = Paul |authorlink = |date=May 2008}}</ref><ref name="Avweb15May08">{{cite web|url = http://www.avweb.com/avwebflash/news/ThielertCancelsWarrantySupport_197880-1.html|title = Thielert: No Warranty Support For Diamond Diesels |accessdate = 2008-05-15|last = Bertorelli|first = Paul |authorlink = |date=May 2008}}</ref>

Due to the insolvency of Thielert and the decisions of the insolvency administrator, including cancelling warranty support and the prorating of time-between-overhaul for the Thielert engines that power the DA42, Diamond announced in July 2008 that production of the DA42 was suspended. At the time production was suspended the DA42 was reported to have 80 percent of the piston twin market.<ref name="AvWeb16Mar09" /><ref name="Avweb26Jul08">{{cite web|url = http://www.avweb.com/avwebflash/news/DiamondTriesToSootheDA42Customers_198391-1.html|title = Diamond Tries To Soothe DA42 Customers |accessdate = 2008-05-28|last = Niles|first = Russ |authorlink = |date=July 2008}}</ref><ref name="AvWeb06Nov08">{{cite web|url = http://www.avweb.com/news/aopa/AOPAExpo2008_Diamond_ThielertDieselEngine_Relief_199144-1.html|title = Diamond's Thielert Problems Ease |accessdate = 2008-11-07|last = Niles|first = Russ|authorlink = |date=November 2008}}</ref>

In March 2009 Diamond achieved EASA certification for the [[Austro Engine E4|Austro Engine AE 300]] and returned the DA42 to production as the DA42 NG. The new engine produces 20% more power, while giving better fuel economy than the Thielert engines and results in a higher gross weight and increased performance. The first Austro-powered DA42 was delivered to a customer in Sweden in April 2009, with the first US customer aircraft expected in mid-2010. The Austro-powered DA42 NG received FAA certification on 9 April 2010.<ref name="AvWeb16Mar09">{{cite web|url = http://www.avweb.com/avwebbiz/news/ReenginedDieselTwinStarEASACertified_199971-1.html|title = Re-engined Diesel Twin Star EASA Certified |accessdate = 2009-03-19|last = Niles|first = Russ|authorlink = |date=March 2009}}</ref><ref name="Avweb05Apr09">{{cite web|url = http://www.avweb.com/avwebflash/news/DieselDA50HeadedToU.S._200082-1.html|title = Diesel DA50 Headed To U.S.|accessdate = 2009-04-13|last = Peppler|first = Graeme|authorlink = |date=April 2009}}</ref><ref name="AvWeb09Apr10">{{cite web|url = http://www.avweb.com/avwebflash/news/diamond_da42_ng_certified_FAA_EASA_fiki_202322-1.html|title = Diamond's DA42 NG, FAA/EASA Certified|accessdate = 12 April 2010|last = Pew|first = Glenn|authorlink = |date=April 2010}}</ref>

==Variants==
[[File:Diamond DA42 with Mag Booms.jpg|thumb|DA42 outfitted for [[aerial survey]]]]
[[File:Diamond DA42 at 2007 MAKS Airshow.jpg|thumb|DA42 at the [[MAKS Airshow]] in Moscow, 2007]]
[[File:G-DOSC-DA42-1678.jpg|thumb|right|DA-42 MPP at the 2010 Farnborough Airshow]]

;DA42
:Production aircraft built in Austria and Canada
;DA42 M
:Special Mission variant built in Austria, modification from standard DA42 and new production.
;DA42 L360
:[[Lycoming IO-360]] {{convert|134|kW|hp|0|abbr=on}} equipped version using [[Avgas|100LL]] fuel instead of [[Jet fuel|Jet-A1]]. This model is intended for the North American flight training market.<ref name="DiamondPress06Nov08">{{cite web|url = http://www.diamondaircraft.com/news/news-article.php?id=38|title = Diamond Aircraft unveils new Lycoming-powered DA42 L360 twin at AOPA Expo 2008|accessdate = 2008-12-27|last = [[Diamond Aircraft]]|authorlink = |date=November 2008}}</ref><ref name="DiamondPress28Jul08">{{cite web|url = http://www.diamondaircraft.com/news/news-article.php?id=1|title = Diamond announces DA42 New Horizons: Austro-Engine, Lycoming powered DA42s|accessdate = 2008-12-27|last = [[Diamond Aircraft]]|authorlink = |date=July 2008}}</ref>
;DA42 NG
:[[Austro Engine AE 300]] {{convert|127|kW|hp|0|abbr=on}} equipped version. [[European Aviation Safety Agency|EASA]] certified March 2009; [[Federal Aviation Administration|FAA]] certified April 2010; [[Transport Canada]] certified 16 April 2012.<ref name="AvWeb16Mar09" /><ref name="AvWeb09Apr10" /><ref name="DiamondPress28Jul08" /><ref>{{cite web|url = http://www.aero-news.net/index.cfm?contentBlockId=92cb0e1d-44c6-40d6-bfa5-4ad8930e4954#d|title = Diamond Aircraft Receives FAA Certification For Austro Engine AE300 Powerplant!|accessdate = 2009-07-31|last = Aero-News Network |authorlink = |year = 2009}}</ref><ref name="DiamondPressRel16Apr12">{{cite web|url = http://www.diamondaircraft.com/news/news-article.php?id=135|title = Diamond DA42 NG receives Transport Canada certification |accessdate = 17 April 2012|last = [[Diamond Aircraft]]|date = 16 April 2012}}</ref>
;DA42 MPP
:[[Diamond Airborne Sensing]] a wholly owned subsidiary of Diamond produces the MPP or "Multi Purpose Platform" variant which is modified to carry aerial sensing, mapping and surveillance payloads.<ref name="Diamond Airborne Sensing">{{cite web|url = http://www.diamond-sensing.com|title = Diamond Airborne Sensing website}}</ref> The [[Ministry of Defence (United Kingdom)|UK Ministry of Defence]] specified the DA42 MPP variant for its surveillance systems project, converted by DO Systems. Two ordered in June 2008.<ref name="AFMonthlyOct08">{{cite web|url = http://www.airforcesmonthly.com|title = Air Forces Monthly (UK edition), Oct 2008, pp 70}}</ref>
;DA42 MPP Centaur OPA
:Aurora Flight Sciences developed an Optionally Piloted Aircraft (OPA) as an Intelligence, Surveillance and Reconnaissance (ISR) platform and can be operated as either unmanned or with pilots on board. One in service with the Swiss military procurement agency [[Armasuisse]].<ref name="armasuisse website">{{cite web|url = http://www.lw.admin.ch/internet/luftwaffe/en/home/dokumentation/assets/aircraft/da42.html|title = armasuisse webpage: Diamond DA42 Centaur OPA}}</ref>
;Dominator II
:[[Aeronautics Defense Systems Ltd]] developed an [[Unmanned aerial vehicle|UAV]] version of the DA42, designated as the [[Aeronautics Defense Dominator]] and first flown in July 2009. Has an endurance of 28 hours with a {{convert|408|kg|lb|0|abbr=on}} payload and speed of 140–354 km/h (75–190 knots) to a maximum altitude of {{convert|9144|m|ft|0|abbr=on}}.<ref name="AvWeb11Aug09">{{cite web|url = http://www.avweb.com/avwebbiz/news/DA42_UAV_Tested_200979-1.html|title = DA42-Based UAV Tested |accessdate = 2009-08-13|last = Niles|first = Russ|authorlink = |date=August 2009}}</ref><ref name="DefPro31Jul09">{{cite web|url = http://www.defpro.com/news/details/8886/|title = Aeronautics Defense Systems successfully tested unmanned Diamond DA42|accessdate = 2009-08-13|last = defence.professionals GmbH|authorlink = |date=July 2009}}</ref>
;DA42-VI
:Improved DA42 introduced in March 2012 with new propeller and aerodynamic clean-ups to the rudder and engine cowling, resulting in greatly increased cruise speed performance.<ref name="Bertorelli17Mar12">{{cite news|url = http://www.aopa.org/aircraft/articles/2012/120418diamond-shows-off-new-airplanes-and-powerplants.html|title = Diamond shows off new airplanes and powerplants |accessdate = 20 April 2012|last = A. Horne|first = Thomas|date = 20 April 2012| work = AOPA}}</ref>

==Operators==

===Civil operators===
<!--

NOTE: As per [[WP:AIRCRAFT-OPERATORS]] please do not add lists of small non-notable operators of this type here.

--->
The DA42 is mainly operated by flight training schools, aerial surveillance and mapping operators.

===Military operators===
;{{ARG}}
* [[Argentine Army]] – 3 DA-42M surveillance aircraft.<ref>{{cite news|title=Misiones: la AFIP retuvo tres aviones que el Gobierno le compró al ejército de EE.UU.|url=http://www.lanacion.com.ar/1863141-misiones-la-afip-retuvo-tres-aviones-que-el-gobierno-le-compro-al-ejercito-de-eeuu|accessdate=18 January 2016|work=La Nacion|publisher=La Nacion|date=18 January 2016}}</ref>
;{{GHA}}
*[[Ghana Air Force]]  – 2 DA42M surveillance aircraft and 1 DA42 training aircraft<ref>{{cite web|url=http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=17911:ghana-air-force-to-spend-us200-million-on-five-aircraft&catid=35:Aerospace&Itemid=107 |title=Ghana Air Force to spend US$200 million on five aircraft |publisher=DefenceWeb |date=2011-08-10 |accessdate=2012-06-28}}</ref>
;{{NIG}}
*[[Niger Air Force]] – 2 DA42M surveillance aircraft<ref>{{cite web|url=http://www.diamond-air.at/news_detail+M504e2bac34f.html |title=Diamond Aircraft :: Diamond Airborne Sensing Sells DA42 MPP Surveillance Aircraft to Niger |publisher=Diamond-air.at |date=2008-06-01 |accessdate=2012-06-28}}</ref>
;{{RUS}}
* [[Federal Security Service]] (FSB) – 2 DA-42M-NGs fitted with reconnaissance equipment ordered in May 2016, with delivery due by November 2017.<ref>{{Cite magazine |last= Butowski |first=Piotr |title=Russian Diamonds |magazine=[[Air International]] |date=June 2016 |volume=90 |issue=6 |page=4|issn=0306-5634}}</ref>
;{{THA}}:
*[[Royal Thai Air Force]] – 6 on order. Delivery in mid-2009.<ref>[http://www.diamond-air.at/news_detail+M52b92d461e3.html Diamond Aircraft Industries] Royal Thai Air Force chooses 6 DA42 for it's (sic) training program</ref>
;{{SUI}}:
*Swiss military procurement agency [[Armasuisse]] – 1 – Delivery in end of 2012<ref name="armasuisse website"/>
;{{TKM}}:
*[[Turkmenistan]] – 5 x DA-42MPP Guardian<ref>[http://archive.defensenews.com/article/20110627/DEFFEAT06/106270313/Market-Expands-Light-ISR-Aircraft Market Expands for Light ISR Aircraft]</ref>
;{{flag|Ukraine}}:
*[[State Border Guard Service of Ukraine|Border Guard Service]] operates three DA42 planes for border patrol missions. One aircraft was lost and its crew killed in a 2012 accident in [[Zakarpattia Oblast|Zakarpattia]].<ref>[http://ua-reporter.com/novosti/119031 Фотографии с места катастрофы самолета Diamond DA-42 в Закарпатье]</ref>
;{{UK}}:
*[[Royal Air Force]] – 2 x DA-42MPP operated in 2008 and 2009 in surveillance role.<ref name="AFMonthlyOct08"/><ref>[http://www.theregister.co.uk/2010/02/18/da42_opv_announced/ The Register] British Forces operating two modified DA42 with 39 squadron in ISTAR role.</ref><ref>[http://www.ukserials.com/results.php?serial=ZA Displaying Serials in range ZA]</ref>

==Aircraft on display==
* [[Technisches Museum Wien]] – prototype DA42<ref name="Kauh28Nov14">{{cite news|url = http://www.avweb.com/avwebflash/news/Diamonds-First-DA42-on-Display-in-Austrian-Museum223163-1.html|title = Diamond's First DA42 On Display In Austrian Museum|accessdate = 1 December 2014|last = Kauh |first = Elaine|date = 28 November 2014| work = AVweb}}</ref>

==Specifications (DA42 Twin Star)==
{{Aircraft specs
|ref=Type Certificate Data Sheet
|prime units?=<!-- eng for US/UK aircraft, met for all others -->met
|crew=1 pilot
|capacity=3 passengers
|length m=8.56
|length ft=28
|length in=1
|span m=13.42
|span ft=44
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.49
|height ft=8
|height in=2
|wing area sqm=16.29
|wing area sqft=175
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=1251
|empty weight lb=2761
|gross weight kg=1700
|gross weight lb=3748
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=2
|eng1 name=Austro
|eng1 type=turbocharged [[diesel engine]]
|eng1 kw=<!-- prop engines -->125
|eng1 hp=<!-- prop engines -->168
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=356
|max speed mph=222
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=1693
|range miles=1055
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=5486
|ceiling ft=18,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=6.5
|climb rate ftmin=1280
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
|similar aircraft=
* [[Piper Seminole]]
* [[Beechcraft Duchess]]
* [[Aeronautics Defense Dominator]]
|sequence=
|see also=
|lists=
}}

==References==
{{reflist|30em}}

== External links ==
{{Commons category}}
*[http://www.diamond-sensing.at Diamond Sensing]
*[http://www.diamond-air.at Diamond Aircraft]
*[http://www.austroengine.at Austro Engines]

{{Diamond Aircraft}}

[[Category:Austrian civil utility aircraft 2000–2009]]
[[Category:Diamond aircraft|DA42]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Diesel-engined aircraft]]
[[Category:T-tail aircraft]]