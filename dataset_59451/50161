<!-- Infobox begins -->
{{Infobox settlement
|name                   = Gagra
|native_name            = {{lang|ka|გაგრა}}<small>{{ge icon}}</small><br/>{{lang|ab|Гагра}}<small>{{ab icon}}</small>
|nickname               = 
|settlement_type        = [[Administrative divisions of Abkhazia|Town]]
|motto                  = 
|image_skyline          = Old Gagra.jpg
|imagesize              = 
|image_shield= Герб Гагры.jpg
|image_map= Gagra na mapě.svg
|image_caption          = Old Gagra
|pushpin_map            = Georgia
|mapsize                =
|map_caption            = Location of Gagra in Abkhazia
|pushpin_map_caption    = Location of Gagra in Georgia
|subdivision_type       = [[List of sovereign states|Country]]
|subdivision_name       = [[Abkhazia]],<ref name=status group=note/> [[Georgia (country)|Georgia]]
|subdivision_type1      = [[Administrative divisions of Abkhazia|District]]
|subdivision_name1      = [[Gagra district|Gagra]]
|leader_title           = Mayor<ref name=mayor group=note>The Governor of [[Gagra District]] is at the same time Mayor of the Gagra municipality.</ref>
|leader_name            = ''[[Beslan Bartsits]]'' {{small|Acting}}
|established_title      =
|established_date       = 
|area_total_km2         = 
|area_footnotes         = 
|population_as_of       = 
|population_total       = 
|population_density_km2 =
|timezone               =
|utc_offset             = 
|timezone_DST           = 
|utc_offset_DST         = 
|coordinates            = {{coord|43|20|N|40|13|E|region:{{Xb|ABK}}|display=inline}}
|elevation_m            = 
|area_code              =
|blank_name             = [[Köppen climate classification|Climate]]
|blank_info             = [[Humid subtropical climate|Cfa]] 
|website                = 
}}

'''Gagra''' ({{lang-ka|გაგრა}}; [[Abkhaz language|Abkhaz]] and [[Russian language|Russian]]: Гагра) is a town in [[Abkhazia]],<ref name=status group=note>{{Abkhazia-note}}</ref> sprawling for 5&nbsp;km on the northeast coast of the [[Black Sea]], at the foot of the [[Caucasus Mountains]]. Its [[subtropical]] climate made Gagra a popular health resort in [[Imperial Russia]]n and [[USSR|Soviet]] times.

It had a population of 26,636 in 1989, but this has fallen considerably due to the [[ethnic cleansing of Georgians in Abkhazia]] and other demographic shifts during and after the [[War in Abkhazia (1992–93)]].

Gagra is the centre of the [[Gagra District|district of the same name]]. It is located in the western part of Region of [[Abkhazia]], and river [[Psou]] serves as a border with [[Krasnodar Kray]] of [[Russia]].

== Etymology ==
According to the [[Georgian people|Georgian]] scholars, ''Gagra'' is derived from ''Gakra'' meaning [[walnut]] in the [[Svan language]].<ref>Topchishvili, Roland (2005), [http://www.nplg.gov.ge/dlibrary/collect/0001/000070/Georgian_Mountein_Regions.pdf History of Georgian Mountain Regions: Svaneti and Its Inhabitants (Ethno-historical Studies)]. [[National Parliamentary Library of Georgia]]</ref> According to the Soviet [[sports tourism]] master Bondaryev, the name of the city originates from the local ''Gagaa'' clan.<ref>{{cite book |title= В ГОРАХ АБХАЗИИ (''In Abkhazian mountains'')|last= БОНДАРЕВ|first= Н.Д.|year= 1981|publisher= Физкультура и спорт|location= Moscow|language=Russian|url= http://wkavkaz.narod.ru/book/abhazia/index.html}}</ref> According to Professor V. Kvarchija, Gagra (< *ga-kʼə-ra) means ‘the holder of the coast’ in Abkhaz (Gagra was mentioned as Kakara, Kakkari on old maps).<ref>Кәарҷиа В. Е. Аҧсны атопонимика — Аҟәа. 2002. P. 92</ref>

== History ==
The town was established as a [[Greeks|Greek]] colony in the kingdom of [[Colchis]], called Triglite, inhabited by Greeks and [[Colchis|Colchians]]. [[Colchis]] came under the control of the kingdom of [[Pontus]] in the 1st century BC before being absorbed by the [[Roman Empire]], which renamed the town as ''Nitica''. Its geographical position led the Romans to fortify the town, which was repeatedly attacked by [[Goths]] and other invaders. After the fall of Rome, its successor, the [[Byzantine Empire]], took control of the town and whole [[Colchis]]. It became a major trading settlement in which [[Genoan]] and [[Venice|Venetian]] merchants were prominent, trading in the town's main exports - wood, honey, wax and slaves. The name "Gagra" appeared for the first time on a map in 1308, on a map of the caucasus made by the Italian Pietro Visconti, which is now in the Library of Saint Mark in [[Venice]].

=== Gagra within the Russian Empire ===
[[File:Oldenburg palace.jpg|thumb|left|Palace of the Prince of Oldenburg]]
In the 16th century, Gagra and the rest of western Georgia was conquered by the [[Ottoman Empire]]. The western merchants were expelled and the town entered a prolonged period of decline, with much of the local population fleeing into the mountains. By the 18th century the town had been reduced to little more than a village surrounded by forests and disease-ridden swamps. Its fortunes were restored in the 19th century when the [[Russian Empire]] expanded into the region, annexing whole Georgia. The swamps were drained and the town was rebuilt around a new military hospital. Its population, however, was still small: in 1866, a census recorded that 336 men and 280 women, mostly local families or army officers and their dependents, lived in Gagra. The town suffered badly in the [[Russo-Turkish War, 1877-1878]], when Turkish troops invaded, destroyed the town and expelled the local population. Russia won the war, however, and rebuilt Gagra again. [[File:Gagry.jpg|250px|thumb|View of Gagra's wharf sometime between 1905 and 1915.]]

After the war, the town was "discovered" by [[Duke Peter Alexandrovich of Oldenburg|Duke Alexander Petrovich of Oldenburg]], a member of the Russian royalty. He saw the potential of the region's subtropical climate and decided to build a high-class resort there. Having raised a large sum of money from the government, he built himself a palace there and constructed a number of other buildings in an eclectic variety of styles from around Europe. A park was laid out with tropical trees and even parrots and monkeys imported to give it an exotic feel. Despite the expensive work, the resort was not initially a success, although it did later attract a growing number of foreign tourists visiting on cruises of the Black Sea.

=== Gagra under the Soviet Union ===
In the [[Russian Revolution of 1905]], a local uprising produced a revolutionary government in the town, which founded a short-lived Republic of Gagra. This was soon defeated and the revolutionaries arrested ''en masse''. The [[World War I|First World War]] a few years later was a disaster for Gagra, destroying the tourist trade on which it depended. The [[Russian Revolution of 1917|Russian Revolution]] shortly afterwards saw the [[Bolsheviks]] take over the town; despite a brief [[France|French]] attempt to repel them during the [[Russian Civil War]], the town was firmly incorporated into the new [[Soviet Union]] within [[Georgia (country)|Georgia]]n SSR.

The Bolshevik leader, [[Vladimir Lenin]], issued a decree in 1919 establishing a "worker's resort" in Gagra, nationalising the resort that had been built by Oldenburg. It became a popular holiday resort for Soviet citizens and during [[World War II]] gained a new role as a site for the rehabilitation of wounded soldiers. After the war, various state-run [[sanatorium]]s were built there. The resort grew and was developed intensively as part of the "Soviet Riviera".

=== Gagra in post-soviet Abkhazia ===
In the late 1980s, tensions grew between the Georgian and Abkhazian communities in the region. All-out war erupted between 1992-1993 which ended in a defeat of the Georgian government's forces. Hundreds of thousands of ethnic Georgians were expelled from their homes in Abkhazia in an outbreak of mass [[Ethnic cleansing of Georgians in Abkhazia|ethnic cleansing]] in which thousands of Georgian civilians were massacred.<ref>Murphy, Paul J. (2004), ''The Wolves of Islam: Russia and the Faces of Chechen Terror''. Brassey's, ISBN 1-57488-830-7.</ref><ref>Human Rights Watch Arms Project. [[Human Rights Watch]]/[[Helsinki]]. March 1995 Vol. 7, No. 7. [https://www.hrw.org/reports/1995/Georgia2.htm Georgia/Abkhazia: Violations of the Laws of War and Russia’s Role in the Conflict]</ref> Gagra and the Abkhazian capital [[Sukhumi]] were at the centre of the fighting and suffered heavy damage.

{{see also|Battle of Gagra}}

== Monuments ==
[[File:Anacopia church.jpg|thumb|right|An early medieval church of [[the Protection]] of the Virgin]]
The chief landmarks of Gagra are:
*Ruins of the Abaata Fortress (4th-5th century AD;
*A 6th-century [[Church of Gagra]], said to be the oldest in Abkhazia;
*Marlinsky defensive tower (1841);
*19th-century palace of the Prince of Oldenburg.

==International relations==
{{See also|List of twin towns and sister cities in Georgia}}

===Twin towns&nbsp;— Sister cities===
Gagra is twinned with the following city:
*{{flagicon|RUS}} [[Vladimir, Russia|Vladimir]], [[Russia]].<ref name=gcity60>{{cite news|title=Города Гагра и Владимир будут сотрудничать |url=http://gagra.biz/2-new/60-goroda-gagra-i-vladimir-budut-sotrudnichat |archive-url=https://archive.is/20130115042423/http://gagra.biz/2-new/60-goroda-gagra-i-vladimir-budut-sotrudnichat |dead-url=yes |archive-date=15 January 2013 |accessdate=28 May 2012 |newspaper=Gagra District Administration |date=14 May 2012 }}</ref>

== See also ==
* [[Sochi conflict]]

==Notes==
{{Reflist|group=note}}

== References ==
{{reflist}}

== External links ==
{{commons category|Gagra}}
*{{wikivoyage-inline|Gagra}}
*[http://webcam.abhazia.com webcamera in Gagra]

{{coord|43|20|N|40|13|E|region:GE_type:city|display=title}}

{{Gagra District}}
{{Administrative divisions of Abkhazia}}
{{Cities and towns in Georgia (country)}}

[[Category:Populated places in Gagra District]]
[[Category:Populated coastal places in Georgia (country)]]
[[Category:Greek colonies in Colchis]]
[[Category:Georgian Black Sea coast]]