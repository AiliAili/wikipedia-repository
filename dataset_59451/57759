{{multiple issues|
{{notability|date=August 2015}}
{{one source|date=August 2015}}
{{third-party|date=August 2015}}
}}
{{Infobox journal
| title = Interstate Journal of International Affairs
| former_names = State, Interstate - Journal of Political Affairs, Aberystwyth Journal of World Affairs
| discipline = [[International relations]]
| language = English, [[Welsh language|Welsh]]
| abbreviation = Interstate J. Int. Aff.
| publisher = Department of International Politics, Aberystwyth University
| country = United Kingdom
| history = 1965-present
| frequency = Biannual
| openaccess = Yes
| ISSN = 2051-6932
| OCLC = 7830152
| website = http://www.interstate1965.wordpress.com/
| link1 = https://www.aber.ac.uk/en/interpol/current-students/undergraduate/interstate/
| link1-name = Journal page at university website
| link2 = https://interstate1965.wordpress.com/archives/
| link2-name = Online tables of contents
| link3 = http://www.studentpulse.com/journals/10/interstate--journal-of-international-affairs
| link3-name = Online archives
}}
The '''''Interstate Journal of International Affairs''''' is a biannual [[peer-reviewed]] [[academic journal]] produced by undergraduates and postgraduates from the Department of International Politics at [[Aberystwyth University]].

== Editors ==
The following persons have been [[editor-in-chief]] of the journal:
{{columns-list|colwidth=30em|
* J. Lynton Jones, 1965-1966
* Andrew Guy, 1967-1968
* [[Michael MccGwire]], 1968-1969
* [[Michael Clarke (academic)|Michael Clarke]], Ian Hopwood, 1969-1970
* Anne Marek, 1970-1971
* [[Michael Clarke (academic)|Michael Clarke]], 1971-1972
* [[Michael Clarke (academic)|Michael Clarke]], Anne Marek, Ian Palmer, 1972-73
* Richard Comotto, 1974-1975
* William Bebb, 1975-1976
* Jim Evans, 1976-1977
* Tom Evans, 1977-1978
* Guy Kitching, Ian McMahon, 1978-1979
* Gregory Luton, 1979-1980
* Sam Pease, 1980-1981
* David Knowles, 1981-1982
* Mandy Owen, 1983-1984
* Mike Smith, 1984-1985
* Charles Payne, 1987-1988
* Malcolm Cockburn, Gerald Guenther, 1988-1989
* Malcolm Cockburn, 1990-1991
* David Landew, 1991-1993
* Will Trevor, 1993-1994
* Derek Morgan, 1994-1995
* Tracy Slawson, 2000-2001
* Sam Garbett, 2009-2010
* Eilif Swensen, 2010-2011
* Yvonne K. Rinkart, 2011-2012
* Alex Serafimov, 2012-2013
* Alex Middleton, 2013-2014
* Akos Erzse, 2014-2015
* Alexandra Hird, 2015-2016
}}

== External links ==
* {{Official website}}
* [http://www.inquiriesjournal.com/journals/10/interstate--journal-of-international-affairs Online archives]

[[Category:Publications established in 1965]]
[[Category:Biannual journals]]
[[Category:Academic journals edited by students]]
[[Category:International relations journals]]