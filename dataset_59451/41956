[[File:Pygmy Goat on a Log.jpg|alt=Pygmy goats resting|thumb|Caramel with black markings]]
The '''Pygmy goat''' is a breed of miniature domestic [[goat]]. The pygmy goat can be used for many different things and that is why a pygmy goat is strictly classified as a multi purpose animal as stated on NPGA or national pygmy goat association. The pygmy goat is quite hardy, an asset in a wide variety of settings, and can adapt to virtually all [[climate]]s.<ref name="Colby, Brian">Colby, Brian, et al. ''Dairy goats-breeding/feeding/management''. American Goat Society. 1972.</ref> Castrated males are called wethers and can be shown in pet classes. There are also pedigree classes at shows for entire goats.  There are separate [[Pedigree (animal)|pedigree]] lineages in Europe and Africa (where they are sometimes called the '''African Pygmy''') the United States. And this is because there were only a few hundred pygmy goats imported from Africa around 60 years ago. 

==Appearance==

Females, called does, weigh {{convert|24|to|34|kg|lb|abbr=on|lnk=on}} and males, called bucks, weigh {{convert|27|to|39|kg|lb|abbr=on}}. [[Withers|Wither]] height ranges from {{convert|41|to|58|cm|in|abbr=on}}. Castrated males are called Wethers. In the U.S. Pygmies only come in 7 breed standard approved colors, and can be categorized into caramel patterned, [[Agouti (coloration)|agouti]] patterned, and black patterned.  Within these categories, there are caramel with black markings, caramel with brown markings, brown agouti, grey agouti, black agouti, black with white markings, and solid black. The faulting matrix allows for faulting random white markings from moderate to very serious and these animals will be judged accordingly. White belly bands are not considered random markings. The U.S. African Pygmy will also only have brown colored eyes.<ref>National Pygmy Goat Association. ''Best of Memo I''. 1982.</ref><ref name="colors">National Pygmy Goat Association. [http://www.npga-pygmy.com/resources/conformation/color_chart.asp "Color Choices for Registration"]. 2013.</ref>

==Reproduction==

Pygmy goats are [[precocial]] and [[polyestrous]] breeders; bearing one to four young every nine to 12 months after a five-month gestation period. Does are usually bred for the first time at about 18 to 24 months, although they may conceive as early as two months if care is not taken to separate them early from male kids. Breeding under age does often results in c-sections. Newborn kids will nurse almost immediately, begin eating grain and roughage within a week, and are weaned by ten weeks of age.

Polyestrous sexual behavior means that they experience heat and can be freshened (made to come into milk production) year-round. If milking is a priority, a continuous supply of milk can be obtained by breeding two or more pygmy does alternately. Some full-size dairy breeds are also noted for polyestrous sexual behavior.

==Origin==
[[File:Hand Rearing - Pygmy Goat.JPG|thumb|Agouti Pygmy Kid being bottle fed]]
The Pygmy goat was developed from the [[west African Dwarf goat|west african dwarf goat]], a [[landrace]] of [[Central Africa|Central]] and [[West Africa]], found most commonly in the [[Cameroon]] Valley. They were taken to Europe primarily by the British during the [[British West Africa|colonial era]]. They were later imported into the [[United States]] from European [[zoo]]s in the 1950s for use in zoos as well as research animals. They were eventually acquired by private breeders and quickly gained popularity as pets and exhibition animals due to their good-natured personalities, friendliness and hardy constitution. Today, they are common as back-yard pets and in [[petting zoo]]s.<ref>National Pygmy Goat Association "Pygmies For All Reasons". 2003.</ref>

==Housing and care==
[[File:African Pygmy Goat 003.jpg|thumb|Grey Agouti Pygmy Buck]]
Pygmy goats are adaptable to most climates. Their primary diet consists of [[grass|green]]s and [[grain]]s. They enjoy having items to jump on and may be able to leap onto small [[vehicle]]s. They are also in need of a shed and open area accessible at all times. They also need a companion, which doesn't necessarily have to be its own [[species]]. They are [[prey]] animals and should therefore be sheltered in a predator-proof area -especially at night. Goats require fresh water at all times or they won't drink it. Pygmy goats are often affectionate if they are treated with respect. They can also be trained, though it requires quite a bit of work. It is important to make sure pygmy goats are comfortable and warm during the winter months.<ref name=henjoe>{{cite web|url=http://www.henryandjoey.co.uk/looka.php|title=Pet Pygmy Goats: Henry and Joey|author=David Watts|work=henryandjoey.co.uk|accessdate=11 December 2014}}</ref> Simple measures such as feeding pygmy goats [[wikt:lukewarm|lukewarm]] water and lukewarm food, as well as ensuring their living quarters are free from drafts, can make pygmy goats a lot happier during the winter.<ref name = care>Kinne, Maxine. ''Pygmy goat care and management''. 1987.</ref>

==General information==
* Scientific name: Capra aegagrus hircus<ref>{{cite web|url=http://www.agriculture.com/livestock/speciality-livestock/pygmy-goat-profile_293-ar13324|title=Speciality Livestock - Agriculture.com|work=agriculture.com|accessdate=11 December 2014}}</ref>
* Average lifespan:   8–18 years
* Normal body temperature:   101.5-104&nbsp;°F (39.1 - 40&nbsp;°C)
* Normal pulse rate:   60-90 beats per minute (faster for kids)
* Normal respiration rate:   15-30 per minute
* [[Rumen]] movement:           1-1.5 per minute
* Gestation period:   145–155 days (average 150 days)
* Heat ([[oestrus]]) cycle: 18–24 days (average 21 days)
* Length of heat:           12–48 hours (average 1 day)
* Weaning age (recommended):  8–10 weeks
* Males sexually mature:  9–12 weeks
* Females onset of heat:   3–12 months*
* Dehorning (by veterinary surgeon): By 7 days
* [[Castration]]:
** Using [[Elastration|elastrator]] ring: 8 weeks and older (recommended to limit risk of Urinary Calculi)[http://www.ansc.purdue.edu/SP/MG/Documents/SLIDES/Urinary%20calculi.pdf]
** Surgical method (by vet):   No age limit

==References==
{{reflist|30em}}

== External links ==
{{Commons category|Pygmy goats}}
* [http://www.henryandjoey.co.uk/infopg.php The anatomy of a pygmy goat]
*[http://www.pygmygoatclub.org/ Pygmy Goat Club (United Kingdom)]
*[http://www.npga-pygmy.com/ National Pygmy Goat Association (United States)]

[[Category:Goats]]
[[Category:Goat breeds]]