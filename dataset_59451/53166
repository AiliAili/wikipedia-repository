{{Infobox journal
| title = Zeitschrift für Naturforschung B
| cover = [[File:Cover of 2012 67(10) issue of Zeitschrift für Naturforschung B.jpg]]
| editor = Gerhard Müller
| discipline = [[Chemistry]]
| language = English, German
| formernames = 
| abbreviation = Z. Naturforsch. B Chem. Sci.
| publisher = De Gruyter
| country =
| frequency = Monthly
| history = 1947&ndash;present
| openaccess =
| license =
| impact = 0.744
| impact-year = 2014
| website = www.degruyter.com/view/j/znb
| link1 = 
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR = 
| OCLC = 476230307
| LCCN = 87642615
| CODEN = ZNBSEN
| ISSN = 0932-0776
| eISSN =
}}
The '''''Zeitschrift für Naturforschung B''': A Journal of Chemical Sciences'' (often abbreviated ''Z. Naturforsch. B'') is a monthly [[Peer review|peer-reviewed]] [[scientific journal]]. The journal publishes "fundamental studies in all areas of [[inorganic chemistry]], [[organic chemistry]], and [[analytical chemistry]]"<ref name = "info4authors">{{cite web|url = http://www.znaturforsch.com/iab.htm|title = Verlag Z. Naturforsch. - Information for Authors B|publisher = Verlag der Zeitschrift für Naturforschung|accessdate = December 8, 2012}}</ref> in both English and German. Articles in German are required to be accompanied by an English-language title and abstract.<ref name = "articles_in_German">{{cite web|url = http://www.znaturforsch.com/zfnbtemplate-german.doc|title = Manuskriptvorlage Z. Naturforsch. B (deutsch)|publisher = Verlag der Zeitschrift für Naturforschung|accessdate = December 8, 2012}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.744.<ref name=WoS>{{cite book |year=2015 |chapter=Zeitschrift für Naturforschung B |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> The [[editors-in-chief]] are Gerhard Müller ([[University of Konstanz]]) and Annette Schier ([[Technical University Munich]]).

== History ==
The ''[[Zeitschrift für Naturforschung]]'' (English: ''Journal for Nature Research'') was established in 1946 by the [[Max Planck Institute]] and the physical sciences ([[Zeitschrift für Naturforschung A|Part A]]) were separated from the other natural sciences (Part B) from 1947. A further separation occurred in 1973 when the biological sciences were moved to a new journal ([[Zeitschrift für Naturforschung C|Part C]]).<ref name = "history">{{cite web|url = http://www.znaturforsch.com/publ.htm|title = VZN - Publisher|accessdate = December 5, 2012|publisher = Verlag der Zeitschrift für Naturforschung}}</ref> The titles used for Part B have been:
*Zeitschrift für Naturforschung. B, Anorganische, Organische und Biologische Chemie, Botanik, Zoologie und verwandte Gebiete. (1947&ndash;1961)
*Zeitschrift für Naturforschung. B, Chemie, Biochemie, Biophysik, Biologie und verwandte Gebiete. ({{ISSN|0044-3174}}, 1962&ndash;1971)
*Zeitschrift für Naturforschung. Teil B, Anorganische Chemie, Organische Chemie, Biochemie, Biophysik, Biologie. (1972)
*From 1973, the part B journal was split into Zeitschrift für Naturforschung. Teil B, Anorganische Chemie, Organische Chemie ({{ISSN|0340-5087}}, 1973&ndash;1987) and the new part C journal [[Zeitschrift für Naturforschung C|''Zeitschrift für Naturforschung. Teil C, Biochemie, Biophysik, Biologie, Virologie'']], separating the chemical and biological sciences
*Zeitschrift für Naturforschung. B, A Journal of Chemical Sciences. ({{ISSN|0932-0776}}, 1987&ndash;present)

== References ==
<references />

== External links ==
* {{Official website|www.degruyter.com/view/j/znb}}

{{DEFAULTSORT:Zeitschrift fur Naturforschung B}}
[[Category:Chemistry journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1946]]
[[Category:Multilingual journals]]