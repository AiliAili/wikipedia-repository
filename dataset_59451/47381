'''Najwan Darwish''' (Arabic: نجوان درويش ); born December 8, 1978 in [[Jerusalem]], [[Palestine (region)|Palestine]] is one of the foremost [[Arabic]]-language poets of his generation.<ref>{{cite web|title=Najwan Darwish|url=http://www.nybooks.com/books/authors/najwan-darwish/|website=New York Review Books|accessdate=16 January 2015}}</ref>

In 2014, [[NPR]] included his book ''Nothing More To Lose'' as one of the best books of the year.<ref>{{cite news|title=Nothing More To Lose|url=http://apps.npr.org/best-books-2014/#/book/nothing-more-to-lose|accessdate=16 January 2015|agency=National Public Radio|date=3 December 2014}}</ref> In 2009, [[Hay Festival]] Beirut pronounced him one of the 39 best Arab writers under the age of 40.<ref>{{cite web|title=Najwan Darwish|url=http://www.poetryinternationalweb.net/pi/site/poet/item/21912|website=Poetry International Rotterdam|accessdate=16 January 2015}}</ref>

Named as "[https://electronicintifada.net/content/edgily-modern-poetry-najwan-darwish/13414 one of Arabic literature’s biggest new stars]", Darwish's work was translated to over 20 languages. .

Darwish is a speaker and lecturer. Past lectures include "The Sexual Image of Israel in the Arab Imagination" at Homeworks (Beirut, 2008) and "To Be a Palestinian Intellectual After Oslo" at the House of Culture (Oslo, 2009).

== Career ==
Darwish is a poet, journalist, editor and cultural critic. Currently he is the Chief Editor of the Cultural Section of Al Araby Al Jadeed newspaper<ref>{{cite news|last1=Handal|first1=Nathalie|title=Kareem James Abu-Zeid: A Search for Justice and Expansive Identities|url=https://www.guernicamag.com/daily/kareem-james-abu-zeid-a-search-for-justice-and-expansive-identities/|accessdate=16 January 2015|agency=Guernica|date=21 August 2014}}</ref> and serves as the literary advisor to the [[Palestine Festival of Literature]].<ref>{{cite web|title=Participants|url=http://palfest.org/participants|website=PalFest|accessdate=16 January 2015}}</ref> In the past he has worked as the Chief Editor of Min wa Ila Magazine,<ref>{{cite web|last1=Adnan|first1=Amani|title=Najwan|url=https://prezi.com/doz48_9yxzz3/najwan/|website=Prezi|accessdate=16 January 2015}}</ref> and as the cultural critic for [[Al Akhbar (Lebanon)|Al Akhbar]] Newspaper from 2006 to 2012, amongst other key positions in cultural journalism.<ref>{{cite web|title=Najwan Darwish|url=http://www.nybooks.com/books/authors/najwan-darwish/|website=New York Review Books|accessdate=16 January 2015}}</ref>

Al-Feel Publications was established by Darwish in 2009 and several books by Palestinian and Arab writers have since been published including ''Letter's From the Earth's Navel'' in 2011.

== Critical Reception ==
* "Darwish's poetry is a welcome change in poetic writing in Arabic" according to acclaimed critic [[Issa J. Boullata]].<ref>http://www.worldliteraturetoday.org/2015/march/nothing-more-lose-najwan-darwish Nothing More to Lose by Najwan Darwish</ref> Darwish's work informed by Arabic and Western poetic traditions<ref>{{cite web|title=Najwan Darwish|url=http://www.nybooks.com/books/authors/najwan-darwish/|website=New York Review Books|accessdate=16 January 2015}}</ref> and has been translated into twenty languages.
* [[Raúl Zurita]], one of the most importantpoetic voices in Latin America today wrote, "I’ve seen nothing of what I believed, but if a God exists it is the same God for me and for the Palestinian poet Najwan Darwish."<ref>{{Cite web|url=http://prairieschooner.unl.edu/excerpt/paradise-zurita-interview-ra%C3%BAl-zurita|title=Paradise in Zurita: An Interview with Raúl Zurita {{!}} Prairie Schooner|website=prairieschooner.unl.edu|access-date=2016-09-29}}</ref>
* [https://electronicintifada.net/blogs/sarah-irving Sarah Irving] of ''The Electric Intifada'' wrote, "With this collection of Najwan Darwish’s poetry — beautifully translated by Kareem James Abu-Zeid — The New York Review of Books has made available to English-language readers the work of one of Arabic literature’s biggest new stars...Where the classic Palestinian resistance poets — Mahmoud Darwish, Samih al-Qasim and their comrades — sought to describe and depict Palestinian culture and their people’s oppression, and to present often nostalgic or romantic views of the society they remembered or aspired to, this new political poetry is in your face, and often cynical... That is not to say that the nostalgia and the deep-rooted appreciation of Palestinian history and culture aren’t still there; Darwish is far too intelligent a writer to resort to slogans and stock images. He well knows that the “political” points he makes are all the more ravagingly poignant because they are set in contexts which are beautiful, heartfelt and/or evocatively melancholy."<ref>{{cite news|last1=Irving|first1=Sarah|title=The edgily modern poetry of Najwan Darwish|url=http://electronicintifada.net/content/edgily-modern-poetry-najwan-darwish/13414|accessdate=16 January 2015|agency=The Electric Intifada|date=27 May 2014}}</ref>
* [[Kareem James Abu-Zeid]], translator of ''Nothing More to Lose'' wrote, "As the translator of several different Arab poets and novelists, I have often faced the challenge of finding the right tone, of keeping the language consistent and unified as it is in the original. With Darkish's work I've had to suppress this tendency, and instead consider each poem as its own singular entity. I am not translating one poet, but many, I often told myself as I grappled with–and learned to embrace-the apparent inconsistencies in his poetry. I have come to realize that this wide range of voices is behind much of Darkish's remarkable success as a poet: no Palestinian has every written poetry quite like this before."<ref>{{cite book|last1=Darwish|first1=Najwan|last2=Abu-Zeid|first2=Kareem James|title=Nothing More to Lose|date=2014|publisher=New York Review of Books|location=New York|isbn=978-1-59017-730-3|page=113|edition=1|url=http://www.nybooks.com/books/imprints/nyrb-poets/nothing-more-to-lose/|accessdate=16 January 2015}}</ref>
* Emily Dische-Becker wrote, "While his poetry is at times political, it embodies a universal message, reminiscent of the great mystical poets like Rumi. From Jerusalem (Palestine) where he works and lives, Darwish has become a distinguished voice for his nation’s struggle. His poetry renders the particularity of the Palestinian experience in luminous imagery and piercing observations, but his imagination and interests are not limited by borders."<ref>{{cite web|last1=Dische-Becker|first1=Emily|title=Najwan Darwish|url=http://www.poetryinternationalweb.net/pi/site/poet/item/21912|website=Poetry International Rotterdam|accessdate=16 January 2015}}</ref>
* [https://amalelmohtar.com/about/ Amal El-Mohtar], writer and critic wrote, "a voice simultaneously so passionate and so matter-of-fact that it stops the breath".<ref>{{Cite web|url=http://www.npr.org/2014/04/29/307745945/nothing-more-to-lose-forges-a-connection-to-palestine|title='Nothing More To Lose' Forges A Connection To Palestine|website=NPR.org|access-date=2016-09-29}}</ref>

== Selected Books ==
* ''Nothing More To Lose'' [[New York Review of Books]], New York, 2014. Translated by Kareem James Abu-Zeid.
*  Sleeping in Gaza, The Chinese University Press, Hong Kong, 2016
* ''Je me lèverai un jour'' Al-Feel Publications, Jerusalem, Palestine, 2012. Translated by Antoine Jockey. 
* ''Fabrications'' Al-Feel Publications, Jerusalem, Palestine, 2013. Translated into English by Sousan Hammad. Translated into Spanish by Beverly Perez Rego

== Selected Poetry ==
[http://www.wordswithoutborders.org/article/life-in-mount-carmel '''Life in Mount Carmel''']<ref>{{Cite web|url=http://www.wordswithoutborders.org/article/life-in-mount-carmel|title=Life in Mount Carmel - Words Without Borders|last=Darwish|first=Najwan|access-date=2016-09-29}}</ref>

Though I’m right beside it,

I can’t call out to the sea:

neighbor, come join me for coffee.

Instead, my other neighbor Carmel

visits me through the window

without my permission

and never even once

tries to enter through the door

(anyway, it owns the place).

Sometimes church bells reach me

from the depths of Wadi Nisnas,

other times the morning call to prayer

comes quietly from the Istiqlal Mosque

(that the old breeze carries from Wadi Salib),

the Baha’is keep donating,

and filling the city with Persian gardens

that escaped from Shiraz,

and in Kababir,

the followers of Mirza Ghulam Ahmad

maintain their naps of devotion

and hunt the truth in tales,

as for the holy men among the Druze,

their poems reach me from their temple

at the foot of Mount Hermon

like the white headscarves of their womenthe ones that hide a thousand years of darkness.

And I, aimless,

between the mountain and the sea,

I, who follow no one but myself,

what should I do among all these devotees,

here,

where time has found its end?

[https://www.poetryfoundation.org/poetrymagazine/poems/detail/56902 '''Mary''']<ref>{{Cite web|url=https://www.poetryfoundation.org/poetrymagazine/poems/detail/56902|title=Mary|access-date=2016-09-29}}</ref>

My mother is obsessed with reading about Jesus these days.

I see books piled by her bed, most of them borrowed from my library: novels, handbooks, sectarian polemics, writers coming to blows. Sometimes when I’m passing by her room she calls on me to step between them and resolve their disputes. (A little while ago I came to the aid of a historian called Kamal Salibi, whose forehead had been split open by a Catholic stone.)

What a diligent reader she is when she’s searching for Jesus, this woman I never failed to disappoint: I was not martyred in the first intifada, nor in the second, nor in the third. And just between you and me, I won’t be martyred in any future intifada either, nor will I be killed by some booby-trapped stork.

As she reads, her orthodox imagination crucifies me with every page.... while I do nothing but supply it with more books and nails.

[https://www.poetryfoundation.org/poetrymagazine/poems/detail/56904 '''A Moment of Silence''']<ref>{{Cite web|url=https://www.poetryfoundation.org/poetrymagazine/poems/detail/56904|title=A Moment of Silence|access-date=2016-09-29}}</ref>

And what did the Armenians say?

An Umayyad monk

spins wheat and wool above us

Time is a scarecrow

'''[http://www.poetryinternationalweb.net/pi/site/poem/item/22096/auto/0/IDENTITY-CARD Identity Card]'''<ref>{{Cite web|url=http://www.poetryinternationalweb.net/pi/site/poem/item/22096/auto/0/IDENTITY-CARD|title=IDENTITY CARD (poem) - Najwan Darwish - Palestine - Poetry International|website=www.poetryinternationalweb.net|access-date=2016-09-29}}</ref>

Despite—as my friends joke—the Kurds being famous for their severity, I was gentler than a summer breeze as I embraced my brothers in the four corners of the world.

And I was the Armenian who did not believe the tears beneath the eyelids of history’s snow

that covers both the murdered and the murderers.

Is it so much, after all that has happened, to drop my poetry in the mud?

In every case I was a Syrian from Bethlehem raising the words of my Armenian brother, and a Turk from Konya entering the gate of Damascus.

And a little while ago I arrived in Bayadir Wadi al-Sir and was welcomed by the breeze, the breeze that alone knew the meaning of a man coming from the Caucasus Mountains, his only companions his dignity and the bones of his ancestors.

And when my heart first tread on Algerian soil, I did not doubt for a moment that I was an Amazigh.

Everywhere I went they thought I was an Iraqi, and they were not wrong in this.

And often I considered myself an Egyptian living and dying time and again by the Nile with my African forebears.

But above anything I was an Aramaean. It is no wonder that my uncles were Byzantines, and that I was a Hijazi child coddled by Umar and Sophronius when Jerusalem was opened.

There is no place that resisted its invaders except that I was of one its people; there is no free man to whom I am not bound in kinship, and there is no single tree or cloud to which I am not indebted. And my scorn for Zionists will not prevent me from saying that I was a Jew expelled from Andalusia, and that I still weave meaning from the light of that setting sun.

In my house there is a window that opens onto Greece, an icon that points to Russia, a sweet scent forever drifting from Hijaz,

and a mirror: No sooner do I stand before it than I see myself immersed in springtime in the gardens of Shiraz, and Isfahan, and Bukhara.

And by anything less than this, one is not an Arab.

'''[https://www.freewordcentre.com/explore/like-these-trees-by-najwan-darwish Like These Trees]'''<ref>{{Cite web|url=https://www.freewordcentre.com/explore/like-these-trees-by-najwan-darwish|title=Like These Trees by Najwan Darwish {{!}} Free Word|date=2012-03-20|language=en-GB|access-date=2016-09-29}}</ref>

The trees are bent on swaying without falling
because here fallen trees are not taken in by the land
nor by anyone or anything;
yet because they could no longer bear the rotting of their roots
and because they chose to grow in the wind
they must pay the price, and fall forever.

So when you sway and stagger on the sidewalk
I beg you not to fall
because you too will fall forever.

Go ahead and imagine trees swaying with you
and an air that welcomes your fall,
you who lived like these trees, without land, without roots.

'''[https://cordite.org.au/translations/abu-zeid-darwish-sakr/ Fabrications]'''<ref>{{Cite web|url=https://cordite.org.au/translations/abu-zeid-darwish-sakr/|title=Six Poems by Najwan Darwish|date=2016-05-03|language=en-US|access-date=2016-09-29}}</ref>

All these years you’ve been mourning the loss of your country.

Shame on you: Loss is a fabrication.

'''[https://cordite.org.au/translations/abu-zeid-darwish-sakr/2/ We Never Stop]'''<ref>{{Cite web|url=https://cordite.org.au/translations/abu-zeid-darwish-sakr/2/|title=Six Poems by Najwan Darwish|date=2016-05-03|language=en-US|access-date=2016-09-29}}</ref>

I’ve got no country to return to

and no country to be banished from:

a tree whose roots

are a running river:

if it stops it dies

and if it doesn’t stop

it dies

I spent the best of my days

on the cheeks and arms of death

and the land I lost each day

I gained each day anew

The people had but a single land

while mine multiplied in defeat

renewed itself in loss

Its roots, like mine, are water:

if it stops it will wither

if it stops it will die

We’re both running

with a river of sunbeams

a river of gold dust

that rises from ancient wounds

and we never stop

We keep on running

never thinking to pause

so our two paths can meet

I’ve got no country to be banished from

and no country to return to:

stopping

would be the death of me

'''[https://cordite.org.au/translations/abu-zeid-darwish-sakr/2/ The Ones Hanging]'''<ref>{{Cite web|url=https://cordite.org.au/translations/abu-zeid-darwish-sakr/2/|title=Six Poems by Najwan Darwish|date=2016-05-03|language=en-US|access-date=2016-09-29}}</ref>

The ones hanging

are tired

Bring us down

so we can have some rest

We haul histories

bereft of land and sky

Lord

sharpen your knife

and give your sacrifice its rest

<nowiki>***</nowiki>

You had no mother or father

and you never saw your brothers

hanging

from the cold talons of dawn

you loved no one

and no one ever left you

and death never ate from your hands…

You cannot know our pain

<nowiki>***</nowiki>

I’m not King David

to sit at contrition’s gate

and sing you psalms of lamentation

after the sin

<nowiki>***</nowiki>

Bring me down—

I want some rest

== Selected Anthologies ==
* ''In Ramallah, Running'' By Guy Mannes-Abbott, Black Dog Publishing, London, 2012. ISBN 978-1907317675. 
* ''Printemps Arabes, Le Souffle et les Mots'' By Gilles Kraemer & Alain Jauson, Riveneuve Editions, France, 2012. ISBN 978-2360130849.
* ''Voix Vives de Méditerranée en Méditerranée, Anthologie Sète 2011'' Éditions Bruno Doucey, Paris, 2011. ISBN 978-2-36229-019-0. 
* ''Revolutionary Poets Brigade'' Edited by Jack Hirschman, Caza de Poesia, California, 2010
* ''Beirut39'' Bloomsbury Publishing, London, 2010
* ''Wherever I Lie Is Your Bed (Two Lines World Writing in Translation)'' Edited by Margaret Jull Costa and Marilyn Hacker, Center for the Art of Translation, San Francisco, 2009. ISBN 978-1931883160.
* ''Language for A New Century, Contemporary Poetry from the Middle East, Asia, and Beyond'' By Tina Chang. W. W. Norton & Company, New York, 2008. ISBN 978-0393332384. 
* ''Le Poème Palestinien Contemporain, Le Taillis Pré, Belgium, 2008
* ''Palabras Por la Lectura'' Edited by Javier Pérez Iglesias, Castilla-La Mancha, Spain, 2007
* ''Pères'' by Taysir Batniji, with texts by Catherine David and Najwan Darwish, Loris Talmart, Paris, 2007. ISBN 978-2903911843. 
* ''En Tous Lieux Nulle Part Ici: Une Anthologie'' Edited by Henri Deluy, Le Blue Ciel, Coutras, 2006. ISBN 978-2915232325.

== Selected Reviews ==
* [http://www.npr.org/2014/04/29/307745945/nothing-more-to-lose-forges-a-connection-to-palestine '''Nothing More To Lose' Forges A Connection To Palestine'' by Amal El-Mohtar]
* [http://electronicintifada.net/content/edgily-modern-poetry-najwan-darwish/13414 ''The edgily modern poetry of Najwan Darwish'' by Sarah Irving]
* [http://therumpus.net/2014/06/nothing-more-to-lose-by-najwan-darwish/ ''Nothing More to Lose by Najwan Darwish'' by Eric Dean Wilson]
* [https://www.guernicamag.com/daily/kareem-james-abu-zeid-a-search-for-justice-and-expansive-identities/ ''Kareem James Abu-Zeid: A Search for Justice and Expansive Identities'' by Nathalie Handal] 
* [http://arablit.org/2014/07/23/translating-najwan-darwish-are-there-any-more-to-come-or-oh-give-me-more/ ''Translating Najwan Darwish: ‘Are There Any More to Come?’ or ‘Oh Give Me More!’'']
* [http://www.kenyonreview.org/kr-online-issue/2016-winter/selections/nothing-more-to-lose-by-najwan-darwish-738439/ THE BODY PALESTINE: A REVIEW OF NAJWAN DARWISH’S NOTHING MORE TO LOSE], by Adam day
* [http://www.earthsimaginedcorners.com/2015/01/nothing-more-to-lose-by-najwan-darwish.html The Round Earth's Imagined Corners, Nothing More to Lose, By Najwan Darkish (Book Darwish)]
* [http://www.nyrb.com/products/nothing-more-to-lose?variant=1094933721 New York Review Books] 
* [https://arablit.org/2015/04/08/btba/ Best Translated Book Award Linguists Najwan Darwish's 'Nothing More to Lose']

== Selected Poetry in Spanish ==

'''FOBIAS'''<ref>{{Cite web|url=https://trabazonblog.es/nada-mas-perder-darwish/|title=Nada más que perder, Darwish. Trabazon{{!}} Blog de literatos|date=2016-09-08|language=es-ES|access-date=2016-09-29}}</ref>

Me expulsarán de la ciudad

antes de que caiga la noche: alegarán

que me negué a pagar por el aire.

Me expulsarán de la ciudad antes de que llegue la noche: alegarán

que no pagué rentas por el sol

ni cuotas por las nubes.

Me expulsarán de la ciudad antes de que salga el sol: dirán

que hice sufrir a la noche

y que fracasé al elevar mis rezos a las estrellas.

Me expulsarán de la ciudad

antes de salir del vientre

porque todo lo que hice durante siete meses

fue escribir poemas y esperar para existir.

Me expulsarán de la existencia

porque tengo debilidad por la nada.

Me expulsarán de la nada

por dudosos lazos hacia la existencia.

Me expulsarán a la vez de la existencia y de la nada

porque nací para existir.

Me expulsarán.

== Interviews & Articles ==
* [http://www.thenational.ae/arts-culture/books/poetic-justice-the-writer-najwan-darwish-on-palfest-and-his-first-volume-of-poetry Poetic justice: The writer Najwan Darwish on PalFest and his first volume of poetry , The National UAE, May 27, 2013 by Jessica Holland]
* [http://cordite.org.au/translations/abu-zeid-darwish-sakr/ Six Poems in Translation by Najwan Darwish] in ''Cordite Poetry Review''
* [https://arablit.org/2014/07/23/translating-najwan-darwish-are-there-any-more-to-come-or-oh-give-me-more/ Translating Najwan Darwish: 'Are there any more to come? or 'oh give me more!'] by [https://arablit.org/author/mlynxqualey/ MLYNXQUALEY]

== Videos ==
* [https://www.youtube.com/watch?v=0K1ynaT8q74 PEN America: Translation Slam: Najwan Darwish]
* [https://www.youtube.com/watch?v=TOgIZFFMRlQ Poetry Jam / Impatience from Najwan Darwish's Nothing More to Lose]
* [https://www.youtube.com/watch?v=-PclB1Q0UoE Najwan Darwish at the 2014 Palestine Festival of Literature at Bethlehem University]
* [https://vimeo.com/44093013 Najwan Darwish: Jerusalem, Poetry International] 
* [https://vimeo.com/44096345 14 June 2012- Najwan Darwish, Karen Solie, Tomaz Salamun, Poetry International] 
* [https://www.youtube.com/watch?v=hQMwbQ7C7T4 Najwan Darwish, lectură la FILB 2014 (Festivalul Internațional de Literatură de la București (FILB)] 
* [https://www.youtube.com/watch?v=VoCyPg9JkDk Uskudar International Poetry Fest | Najwan Darwish]

== References ==
{{reflist}}

{{DEFAULTSORT:Darwish, Najwan}}
[[Category:Living people]]
[[Category:International Writing Program alumni]]
[[Category:Palestinian poets]]