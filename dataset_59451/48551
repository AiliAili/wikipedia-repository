The '''Holomorphic Embedding Load-flow Method''' ('''HELM'''<ref group="note">HELM is a trademark of Gridquant Inc.</ref>) is a solution method for the [[Power flow study|power flow]] equations of electrical power systems. Its main features are that it is [[Direct method (computational mathematics)|direct]] (that is, non-iterative) and that it mathematically guarantees a consistent selection of the correct operative branch of the multivalued problem, also signalling the condition of voltage collapse when there is no solution. These properties are relevant not only for the reliability of existing off-line and real-time applications, but also because they enable new types of analytical tools that would be impossible to build with existing iterative load flows (due to their convergence problems). An example of this would be [[decision support software|decision-support tools]] providing validated action plans in real time.

The HELM load flow algorithm was invented by Antonio Trias and has been granted two US Patents.<ref>
{{cite patent
 | country = US
 | number = 7519506
 | status = patent
 | title = System and method for monitoring and managing electrical power transmission and distribution networks
 | gdate = 2009-04-14
 | invent1 = Antonio Trias
}}
* {{cite patent
 | country = US
 | number = 7979239
 | status = patent
 | title = System and method for monitoring and managing electrical power transmission and distribution networks
 | gdate = 2011-07-12
 | invent1 = Antonio Trias
}}</ref>  A detailed description was presented at the 2012 IEEE PES General Meeting, and published in.<ref name="helmpaper">A. Trias, "The Holomorphic Embedding Load Flow Method", ''IEEE Power and Energy Society General Meeting 2011'', 22–26 July 2012.</ref>
The method is founded on advanced concepts and results from [[complex analysis]], such as [[Holomorphic function|holomorphicity]], the theory of [[algebraic curve]]s, and [[analytic continuation]]. However, the numerical implementation is rather straightforward as it uses standard linear algebra and [[Padé approximant|Padé approximation]]. Additionally, since the limiting part of the computation is the factorization of the admittance matrix and this is done only once, its performance is competitive with established fast-decoupled loadflows.  The method is currently implemented into industrial-strength real-time and off-line packaged EMS applications.

== Background ==

The [[Power flow study|load-flow]] calculation is one of the most fundamental components in the analysis of power systems and is the
cornerstone for almost all other tools used in [[power system simulation]] and [[Energy management system|management]].  The load-flow equations can be written in the following general form:

{{NumBlk|:|<math>\sum_k Y_{ik} V_k + Y_i^{\text{sh}} V_i = \frac{S_i^*}{V_i^*}</math>|{{EquationRef|1}}}}

where the given (complex) parameters are the admittance matrix
{{math|<var>Y<sub>ik</sub></var>}}, the bus shunt admittances
{{math|<var>Y<sub>i</sub></var><sup>sh</sup>}}, and the bus power
injections {{math|<var>S<sub>i</sub></var>}} representing
constant-power loads and generators.

To solve this non-linear system of algebraic equations, traditional
load-flow algorithms were developed based on three iterative
techniques: the [[Gauss-Seidel]] method
<ref>J. B. Ward and H. W. Hale, "Digital Computer Solution of Power-Flow Problems," ''Power Apparatus and Systems, Part III. Transactions of the American Institute of Electrical Engineers'', vol.75, no.3, pp.398-404, Jan. 1956.
* A. F. Glimn and G. W. Stagg, "Automatic Calculation of Load Flows", ''Power Apparatus and Systems, Part III. Transactions of the American Institute of Electrical Engineers'', vol.76, no.3, pp.817-825, April 1957.
* Hale, H. W.; Goodrich, R. W.; , "Digital Computation or Power Flow - Some New Aspects," ''Power Apparatus and Systems, Part III. Transactions of the American Institute of Electrical Engineers'', vol.78, no.3, pp.919-923, April 1959.</ref>
, which has poor convergence properties but very little memory requirements and is
straightforward to implement; the full [[Newton-Raphson]] method
<ref>W. F. Tinney and C. E. Hart, "Power Flow Solution by Newton's Method," ''IEEE Transactions on Power Apparatus and Systems'', vol. PAS-86, no.11, pp.1449-1460, Nov. 1967.
* S. T. Despotovic, B. S. Babic, and V. P. Mastilovic, "A Rapid and Reliable Method for Solving Load Flow Problems," ''IEEE Transactions on Power Apparatus and Systems'', vol. PAS-90, no.1, pp.123-130, Jan. 1971.</ref>
, which has fast (quadratic) iterative convergence
properties, but it is computationally costly; and the Fast Decoupled
Load-Flow (FDLF) method
<ref name="FDLF">B. Stott and O. Alsac, "Fast Decoupled Load Flow," ''IEEE Transactions on Power Apparatus and Systems'', vol. PAS-93, no.3, pp.859-869, May 1974.</ref>
, which is based on Newton-Raphson, but greatly reduces its computational cost by means of a decoupling approximation that is valid in most transmission networks. Many other incremental improvements exist; however, the underlying technique in all of them is still an iterative solver, either of Gauss-Seidel or of Newton type. There are two fundamental problems with all iterative schemes of this type. On the one hand, there is no guarantee that the iteration will always converge to a solution; on the other, since the system has multiple solutions,<ref group="note" name="multsol">It is a well-known fact that the load flow equations for a power system have multiple solutions. For a network with {{math|<var>N</var>}} non-swing buses, the system may have up to {{math|2<sup><var>N</var></sup>}} possible solutions, but only one is actually possible in the real electrical system. This fact is used in stability studies, see for instance: Y. Tamura, H. Mori, and S. Iwamoto,"Relationship Between Voltage Instability and Multiple Load Flow Solutions in Electric Power Systems", '' IEEE Transactions on Power Apparatus and Systems'', vol. PAS-102 , no.5, pp.1115-1125, 1983.</ref> it is not possible to control which solution will be selected. As the power system approaches the point of voltage collapse, spurious solutions get closer to the correct one, and the iterative scheme may be easily attracted to one of them because of the phenomenon of Newton fractals: when the Newton method is applied to complex functions, the basins of attraction for the various solutions show fractal behavior.<ref group="note">This is a general phenomenon affecting the Newton-Raphson method when applied to equations in
''complex'' variables. See for instance [[Newton's method#Complex functions]].</ref>  As a result, no matter how close the chosen initial point of the iterations (seed) is to the correct solution, there is always some non-zero chance of straying off to a different solution. These fundamental problems of iterative loadflows have been extensively documented
.<ref>R. Klump and T. Overbye, “A new method for finding low-voltage power flow solutions", ''in IEEE 2000 Power Engineering Society Summer Meeting,'', Vol. 1, pp. 593-–597, 2000.
* J. S. Thorp and S. A. Naqavi, "Load flow fractals", ''in Proceedings of the 28th IEEE Conference on Decision and Control, Vol. 2, pp. 1822--1827, 1989.
* J. S. Thorp, S. A. Naqavi, and H. D. Chiang, "More load flow fractals", ''in Proceedings of the 29th IEEE Conference on Decision and Control, Vol. 6, pp. 3028--3030, 1990.
* S. A. Naqavi, ''Fractals in power system load flows'', Cornell University, August 1994.
* J. S. Thorp, and S. A. Naqavi, S.A., "Load-flow fractals draw clues to erratic behaviour", IEEE Computer Applications in Power, Vol. 10, No. 1, pp. 59--62, 1997.
* H. Mori, "Chaotic behavior of the Newton-Raphson method with the optimal multiplier for ill-conditioned power systems", in ''The 2000 IEEE International Symposium on Circuits and Systems (ISCAS 2000 Geneva), Vol. 4, pp. 237--240, 2000.
</ref>  A simple
illustration for the two-bus model is provided in<ref>[http://www.elequant.com/products/agora/demo/iterativeloadflow/ Problems with Iterative Load Flow], Elequant, 2010.</ref> Although there exist [[Homotopy|homotopic]] [[Numerical continuation|continuation]] techniques that alleviate the problem to some degree,<ref>V. Ajjarapu and C. Christy, "The continuation power flow: A tool for steady state voltage
stability analysis", ''IEEE Trans. on Power Systems'', vol.7, no.1, pp. 416-423, Feb 1992.</ref> the fractal nature of the basins of attraction precludes a 100% reliable method for all electrical scenarios.

The key differential advantage of the HELM is that it is fully deterministic and unambiguous: it guarantees that the solution always
corresponds to the correct operative solution, when it exists; and it signals the non-existence of the solution when the conditions are such that there is no solution (voltage collapse). Additionally, the method is competitive with the FDNR method in terms of computational cost. It brings a solid mathematical treatment of the load-flow problem that provides new insights not previously available with the iterative numerical methods.

== Methodology and applications ==

HELM is grounded on a rigorous mathematical theory, and in practical terms it could be summarized as follows:
# Define a specific (holomorphic) embedding for the equations in terms of a complex parameter {{math|<var>s</var>}}, such that for {{math|<var>s</var>{{=}}0}} the system has an obvious correct solution, and for {{math|<var>s</var>{{=}}1}} one recovers the original problem.
# Given this holomorphic embedding, it is now possible to compute univocally power series for voltages as analytic functions of {{math|<var>s</var>}}. The correct load-flow solution at {{math|<var>s</var>{{=}}1}} will be obtained by analytic continuation of the known correct solution at {{math|<var>s</var>{{=}}0}}.
# Perform the analytic continuation using algebraic approximants, which in this case are guaranteed to either converge to the solution if it exists, or not converge if the solution does not exist (voltage collapse).

HELM provides a solution to a long-standing problem of all iterative load-flow methods, namely the unreliability of the iterations in finding the correct solution (or any solution at all).

This makes HELM particularly suited for real-time applications, and mandatory for any EMS software based on exploratory algorithms, such as contingency analysis, and under alert and emergency conditions solving operational limits violations and restoration providing guidance through action plans.

== Holomorphic embedding ==

For the purposes of the discussion, we will omit the treatment of controls, but the method can accommodate all types of controls. For the constraint equations imposed by these controls, an appropriate holomorphic embedding must be also defined.

The method uses an embedding technique by means of a complex parameter {{math|<var>s</var>}}.
The first key ingredient in the method lies in requiring the embedding to be holomorphic, that is, that the system of equations for voltages {{math|<var>V</var>}} is turned into a system of equations for functions {{math|<var>V(s)</var>}} in such a way that the new system defines {{math|<var>V(s)</var>}} as holomorphic functions (i.e. complex analytic) of the new complex variable {{math|<var>s</var>}}. The aim is to be able to use the process of analytic continuation which will allow the calculation of {{math|<var>V(s)</var>}} at {{math|<var>s</var>{{=}}1}}. Looking at equations ({{EquationNote|1}}), a necessary condition for the embedding to be holomorphic is that {{math|<var>V<sup>*</sup></var>}} is replaced under the embedding with {{math|<var>V<sup>*</sup>(s<sup>*</sup>)</var>}}, not {{math|<var>V<sup>*</sup>(s)</var>}}. This is because complex conjugation itself is not a holomorphic function. On the other hand, it is easy to see that the replacement {{math|<var>V<sup>*</sup>(s<sup>*</sup>)</var>}} does allow the equations to define a holomorphic function {{math|<var>V(s)</var>}}. However, for a given arbitrary embedding, it remains to be proven that {{math|<var>V(s)</var>}} is indeed holomorphic. Taking into account all these considerations, an embedding of this type is proposed:

{{NumBlk|:|<math>\sum_k Y_{ik} V_k(s) + Y_i^{\text{sh}} V_i(s)  = s\frac{S_i^*}{V_i ^*(s^*)}</math>|{{EquationRef|1}}}}

With this choice, at {{math|<var>s</var>{{=}}0}} the right hand side terms become zero, (provided that the denominator is not zero), this corresponds to the case where all
the injections are zero and this case has a well known and simple operational solution: all voltages are equal and all flow intensities are zero. Therefore, this choice for the embedding provides at s=0 a well known operational solution.

Now using classical techniques for variable elimination in polynomial systems<ref>B. Sturmfels, "Solving Systems of Polynomial Equations”, CBMS Regional Conference Series in Mathematics 97, AMS, 2002.</ref> (results from the theory of [[Resultants]] and [[Gröbner basis#Elimination|Gröbner basis]] it can be proven that equations ({{EquationNote|1}}) do in fact define {{math|<var>V(s)</var>}} as holomorphic functions. More significantly, they define {{math|<var>V(s)</var>}} as [[algebraic curves]]. It is this specific fact, which becomes true because the embedding is holomorphic that guarantees the uniqueness of the result. The solution at {{math|<var>s</var>{{=}}0}} determines uniquely the solution everywhere (except on a finite number of branch cuts), thus getting rid of the multi-valuedness of the load-flow problem.

The technique to obtain the coefficients for the power series expansion (on {{math|<var>s</var>{{=}}0}}) of voltages {{math|<var>V</var>}} is quite straightforward, once one realizes that equations ({{EquationNote|2}}) can be used to obtain them order after order.  Consider the power series expansion for <math>\textstyle V(s)=\sum_{n = 0}^\infty a[n] s^n</math> and <math>\textstyle 1/V(s)=\sum_{n = 0}^\infty b[n] s^n</math>. By substitution into equations ({{EquationNote|1}}) and identifying terms at each order in {{math|<var>s<sup>n</sup></var>}}, one obtains:

{{NumBlk|:|<math>\sum_k Y_{ik} a_k[n] + Y_i^{\text{sh}} a_i[n]  = S_i^* b_i^*[n-1] \qquad (n=0, \ldots, \infty)</math>|{{EquationRef|2}}}}

It is then straightforward to solve the sequence of linear systems ({{EquationNote|2}}) successively order after order, starting from {{math|<var>n</var>{{=}}0}}. Note that the coefficients of the expansions for {{math|<var>V</var>}} and {{math|<var>1/V</var>}} are related by the simple convolution formulas derived from the following identity:
{{NumBlk|:|
<math>
\begin{align}
1 & = V(s)V^{- 1} (s) \\
   & = \left(\sum_{n=0}^\infty a_n s^n\right) \left(\sum_{n = 0}^\infty b_n s^n\right) \\
   & = a_0 b_0  + \left(\sum_{k=0}^1 a_{1-k} b_k\right) s + \left(\sum_{k=0}^2 a_{2-k} b_k\right) s^2  +  \cdots  + \left(\sum_{k = 0}^n a_{n-k} b_k\right) s^n  +  \ldots
\end{align}
</math>
|{{EquationRef|3}}}}
so that the right-hand side in ({{EquationNote|2}}) can always be calculated from the solution of the system at the previous order. Note also how the procedure works by solving just [[System of linear equations|linear systems]], in which the matrix remains constant.

A more detailed discussion about this procedure is offered in Ref.<ref name="helmpaper" />

== Analytic continuation ==
Once the power series at {{math|<var>s</var>{{=}}0}} are calculated to the desired order, the problem of calculating them at {{math|<var>s</var>{{=}}1}} becomes one of [[analytic continuation]]. It should be strongly remarked that this does not have anything in common with the techniques of [[Homotopy#Applications|homotopic continuation]]. Homotopy is powerful since it only makes use of the concept of continuity and thus it is applicable to general smooth nonlinear systems, but on the other hand it does not always provide a reliable method to approximate the functions (as it relies on iterative schemes such as Newton-Raphson).

It can be proven<ref>L. Ahlfors, ''Complex analysis (3rd ed.)'', McGraw Hill, 1979.</ref> that algebraic curves are complete [[global analytic function]]s, that is, knowledge of the power series expansion at one point (the so-called germ of the function) uniquely determines the function everywhere on the complex plane, except on a finite number of [[Branch point#Branch cuts|branch cuts]]. Stahl’s extremal domain theorem<ref>G. A. Baker Jr and P. Graves-Morris, ''Padé Approximants'' (Encyclopedia of Mathematics and its Applications), Cambridge University Press, Second Ed. 2010, p. 326.</ref> further asserts that there exists a maximal domain for the analytic continuation of the function, which corresponds to the choice of branch cuts with minimal [[Conformal radius#Version from infinity: transfinite diameter and logarithmic capacity|logarithmic capacity]] measure. In the case of algebraic curves the number of cuts is finite, therefore it would be feasible to find maximal continuations by finding the combination of cuts with minimal capacity. For further improvements, Stahl’s theorem on the convergence of Padé Approximants<ref>H. Stahl, “The Convergence of Padé Approximants to Functions with Branch Points”, ''J. Approx. Theory'', '''91''' (1997), 139-204.
* G. A. Baker Jr and P. Graves-Morris, ''Padé Approximants'' (Encyclopedia of Mathematics and its Applications), Cambridge University Press, Second Ed. 2010, p. 326-330.</ref> states that the diagonal and supra-diagonal Padé (or equivalently, the continued fraction approximants to the power series) converge to the maximal analytic continuation. The zeros and poles of the approximants remarkably accumulate on the set of [[Branch point#Branch cuts|branch cuts]] having minimal capacity.

These properties confer the load-flow method with the ability to unequivocally detect the condition of voltage collapse: the algebraic approximations are guaranteed to either converge to the solution if it exists, or not converge if the solution does not exist.

== See also ==
* [[Power flow study]]
* [[Power system simulation]]

== Notes ==
{{Reflist|group=note}}

== References ==
{{Reflist}}

[[Category:Electrical engineering]]
[[Category:Power engineering]]