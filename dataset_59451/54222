{{Use dmy dates|date=June 2015}}
{{Use Australian English|date=June 2015}}
'''Ivor Pengelly Francis''' (13 March 1906  – 6 November 1993) was an artist and art critic and teacher in South Australia. He has been called South Australia's premier surrealist painter.

==History==
Ivor Francis was born in [[Uckfield]], England<ref>{{Cite web|url=http://www.daao.org.au/bio/ivor-pengelly-francis/|title=Ivor Pengelly Francis|publisher=Design and Art of Australia Online|accessdate=12 February 2015}}</ref> and emigrated to South Australia on the ''Moreton Bay'' in 1924 as a [[Barwell Boys|Barwell scheme]] "apprentice farm boy",<ref>{{cite news |url=http://nla.gov.au/nla.news-article57471541 |title=General News |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=21 February 1924 |accessdate=12 February 2015 |page=13 |publisher=National Library of Australia}}</ref> and went to work at Oolooloo station at [[Elliston, South Australia|Elliston]] on [[Eyre Peninsula]].

He joined the Adelaide Teachers College in 1925 and on qualifying started teaching at [[Jamestown, South Australia|Jamestown]] in 1929, then [[Prospect, South Australia]] from 1930. He took classes part-time at the [[South Australian School of Arts and Crafts|School of Arts and Crafts]] 1928–1940, where he developed a long-term friendship with fellow-artist [[Mary Packer Harris]] (no relation to Max Harris). He contributed to the art scene in Adelaide by letters to the newspapers and lectures at the [[Art Gallery of South Australia|Art Gallery]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article131414019 |title=National Gallery Lectures |newspaper=[[The News (Adelaide)|The News]] |location=Adelaide |date=2 May 1941 |accessdate=12 February 2015 |page=8 |publisher=National Library of Australia}}</ref> He was one of a group of young artists, which included [[Ruth Tuck]], Shirley Adams, [[Dave Dallwitz]] and Douglas Roberts, who in 1942 held the [[Royal South Australian Society of Arts]]' first exhibition of modernist art.<ref>{{cite news |url=http://nla.gov.au/nla.news-article74618294 |title=Out Among the People |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=6 May 1942 |accessdate=12 February 2015 |page=8 |publisher=National Library of Australia}}</ref> From 1944 to 1947 he taught art subjects at [[Adelaide Technical High School]], then from 1948 to 1968 he worked for the [[Australian Broadcasting Corporation|ABC Radio]] as supervisor of Education programmes.<ref>{{cite web|url=http://www.surrealismcentre.ac.uk/papersofsurrealism/journal6/acrobat%20files/wachpdf.pdf|author=Ken Wach|title=Ivor Francis’s Schizophrenia of 1943: Australia’s First Psychological Painting|accessdate=12 February 2015}}</ref>

He was one of the first to join the South Australian branch of the [[Contemporary Art Society (Australia)|Contemporary Art Society]] being promoted by [[Max Harris (poet)|Max Harris]] and became his vice-president. In 1944 he became art critic for [[The News (Adelaide)|The News]]. Although known for his abstract and surrealist paintings, his landscapes were also well received.<ref>1948 'Exhibition By Ivor Francis.', The Advertiser (Adelaide, SA : 1931–1954), 1 September, p. 5, viewed 12 February 2015, http://nla.gov.au/nla.news-article43781227</ref>

==Family==
He married Ethel <!-- Louisa Eileen--> Saunders of Jamestown on 21 January 1931;<ref>{{cite news |url=http://nla.gov.au/nla.news-article29860099 |title=Family Notices. |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=17 January 1931 |accessdate=12 February 2015 |page=8 |publisher=National Library of Australia}}</ref> they lived at 5 Labrina Avenue, Prospect until around 1950, when they moved to a home in Picadilly Road, [[Crafers, South Australia|Crafers]], where he died. They had no children.

== References ==
{{Reflist}}

{{DEFAULTSORT:Francis, Ivor}}
[[Category:Australian painters]]
[[Category:Australian cartoonists]]
[[Category:Australian art critics]]
[[Category:Australian art teachers]]
[[Category:Australian journalists]]
[[Category:1906 births]]
[[Category:1993 deaths]]