{{Use dmy dates|date=March 2011}}
{{Unreferenced|date=January 2010}}
{{ Infobox V8 Supercar race report
| flag              = South Australia
| title             = 2006 Clipsal 500 Adelaide
| round_no          = 1
| season_no         = 13
| year              = 2006
| series            = V8SC
| track_map         = Adelaide (short route).svg
| date              = 23–26 March
| location          = [[Adelaide]], [[South Australia]]
| circuit           = Adelaide Street Circuit
| weather           = Fine
| race1_laps        = 78
| race1_dist        = 250
| pole1             = [[Mark Skaife]]
| pole1_team        = [[Holden Racing Team]]
| pole1_time        = 1:21.6102
| winner1           = [[Craig Lowndes]]
| winner1_team      = [[Triple Eight Race Engineering (Australia)|Triple Eight Race Engineering]]
| winner1_time      = 2:04:18.5229
| race2_laps        = 78
| race2_dist        = 250
| winner2           = [[Jamie Whincup]]
| winner2_team      = [[Triple Eight Race Engineering (Australia)|Triple Eight Race Engineering]]
| winner2_time      = 2:01:28.4014
| first_round       = [[Jamie Whincup]]
| first_round_team  = [[Triple Eight Race Engineering (Australia)|Triple Eight Race Engineering]]
| first_pts         = 310
| second_round      = [[Todd Kelly]]
| second_round_team = [[Holden Racing Team]]
| second_pts        = 290
| third_round       = [[Rick Kelly]]
| third_round_team  = [[HSV Dealer Team]]
| third_pts         = 305
}}

The '''2006 Clipsal 500 Adelaide''' was the first round of the [[2006 V8 Supercar Championship Series]]. It took place from 23 to 26 March 2006 and was the eighth in a sequence of "[[Adelaide 500]]" events for [[V8 Supercars]] to be held at the [[Adelaide Parklands Circuit]] in [[Adelaide]], [[South Australia]].

==Qualifying==

===Qualifying===
The qualifying for the [[Clipsal 500 Adelaide]] took place on Friday, 24 March 2006. As in the case of most V8 Supercar races, there was a [[Top Ten Shootout]] which could make the person that qualified first drop to tenth and vice versa. Kiwi [[Jason Richards]], in his Tasman Motorsport Holden qualified first.

===Top Ten Shootout===
The official name for the winner of the top team shootout winner is the Armor-All Pole Position Award.
The Top Ten Shootout also took place on the Friday. The person who finished tenth in qualifying goes first, and First goes last. The drivers get only one lap to record their best time.

The [[Holden Racing Team]] got a one-two result in this, with [[Mark Skaife]] finishing first and [[Todd Kelly]] finishing second. The Lion (Holden) dominated the top ten shootout with 8 of the 10 drivers driving a VZ Commodore. There were only two Ford's in the Top Ten, Team Triple 888's [[Jamie Whincup]] and [[Craig Lowndes]].

===Qualifying Results===
{| class="wikitable" style="font-size: 85%;"
|-
! Pos
! No
! Name
! Team
! Car
! Time
! Gap
|-
! 1
| 3
| {{flagicon|NZL}} [[Jason Richards]]
| [[Tasman Motorsport]]
| [[Holden VZ Commodore]]
| 1:22.4429
| 
|-
! 2
| 88
| {{flagicon|AUS}} [[Jamie Whincup]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| [[Ford BA Falcon]]
| 1:22.4429
| 0:00.0135
|-
! 3
| 2
| {{flagicon|AUS}} [[Mark Skaife]]
| [[Holden Racing Team]]
| [[Holden VZ Commodore]]
| 1:22.5251
| 0:00.0822
|-
! 4
| 22
| {{flagicon|AUS}} [[Todd Kelly]]
| [[Holden Racing Team]]
| [[Holden VZ Commodore]]
| 1:22.6433
| 0:00.2004
|-
! 5
| 16
| {{flagicon|AUS}} [[Garth Tander]]
| [[HSV Dealer Team]]
| [[Holden VZ Commodore]]
| 1:22.7321
| 0:00.2892
|-
! 6
| 11
| {{flagicon|AUS}} [[Paul Dumbrell]]
| [[Perkins Engineering]]
| [[Holden VZ Commodore]]
| 1:22.9215
| 0:00.4786
|-
! 7
| 5
| {{flagicon|AUS}} [[Mark Winterbottom]]
| [[Ford Performance Racing]]
| [[Ford BA Falcon]]
| 1:22.9480
| 0:00.5051
|-
! 8
| 4
| {{flagicon|AUS}} [[James Courtney]]
| [[Stone Brothers Racing]]
| [[Ford BA Falcon]]
| 1:23.0660
| 0:00.6231
|-
! 9
| 021
| {{flagicon|NZL}} [[Paul Radisich]]
| [[Team Kiwi Racing]]
| [[Holden VZ Commodore]]
| 1:23.0676
| 0:00.6247
|-
! 10
| 15
| {{flagicon|AUS}} [[Rick Kelly]]
| [[HSV Dealer Team]]
| [[Holden VZ Commodore]]
| 1:23.0940 	
| 0:00.6511
|-
! 11
| 8
| {{flagicon|BRA}} [[Max Wilson]]
| [[WPS Racing]]
| [[Ford BA Falcon]]
| 1:23.1155 	
| 0:00.6726
|-
! 12
| 888
| {{flagicon|AUS}} [[Craig Lowndes]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| [[Ford BA Falcon]]
| 1:23.1709 	
| 0:00.7280
|-
! 13
| 17
| {{flagicon|AUS}} [[Steven Johnson (racing driver)|Steven Johnson]]
| [[Dick Johnson Racing]]
| [[Ford BA Falcon]]
| 1:23.2279 	
| 0:00.7850
|-
! 14
| 67
| {{flagicon|AUS}} [[Paul Morris (racing driver)|Paul Morris]]
| [[Paul Morris Motorsport]]
| [[Holden VZ Commodore]]
| 1:23.2337 	
| 0:00.7908
|-
! 15
| 1
| {{flagicon|AUS}} [[Russell Ingall]]
| [[Stone Brothers Racing]]
| [[Ford BA Falcon]]
| 1:23.2831 	
| 0:00.8402
|-
! 16
| 55
| {{flagicon|AUS}} [[Steve Owen (racing driver)|Steve Owen]]
| [[Rod Nash Racing]]
| [[Holden VZ Commodore]]
| 1:23.3637 	
| 0:00.9208
|-
! 17
| 51
| {{flagicon|NZL}} [[Greg Murphy]]
| [[Paul Weel Racing]]
| [[Holden VZ Commodore]]
| 1:23.3992 	
| 0:00.9563
|-
! 18
| 18
| {{flagicon|AUS}} [[Will Davison]]
| [[Dick Johnson Racing]]
| [[Ford BA Falcon]]
| 1:23.4642 	
| 0:01.0213
|-
! 19
| 12
| {{flagicon|AUS}} [[John Bowe (racing driver)|John Bowe]]
| [[Brad Jones Racing]]
| [[Ford BA Falcon]]
| 1:23.5471 	
| 0:01.1042
|-
! 20
| 34
| {{flagicon|AUS}} [[Dean Canto]]
| [[Garry Rogers Motorsport]]
| [[Holden VZ Commodore]]
| 1:23.5671 	
| 0:01.1242
|-
! 21
| 21
| {{flagicon|AUS}} [[Brad Jones (racing driver)|Brad Jones]]
| [[Brad Jones Racing]]
| [[Ford BA Falcon]]
| 1:23.5826 	
| 0:01.1397
|-
! 22
| 50
| {{flagicon|AUS}} [[Cameron McConville]]
| [[Paul Weel Racing]]
| [[Holden VZ Commodore]]
| 1:23.7558 	
| 0:01.3129
|-
! 23
| 23
| {{flagicon|AUS}} [[Andrew Jones (racing driver)|Andrew Jones]]
| [[Tasman Motorsport]]
| [[Holden VZ Commodore]]
| 1:23.8131
| 0:01.3702
|-
! 24
| 6
| {{flagicon|AUS}} [[Jason Bright]]
| [[Ford Performance Racing]]
| [[Ford BA Falcon]]
| 1:23.8927
| 0:01.4498
|-
! 25
| 39
| {{flagicon|AUS}} [[Alan Gurr]]
| [[Paul Morris Motorsport]]
| [[Holden VZ Commodore]]
| 1:23.9569 	
| 0:01.5140
|-
! 26
| 7
| {{flagicon|AUS}} [[Steven Richards]]
| [[Perkins Engineering]]
| [[Holden VZ Commodore]]
| 1:23.9585 	
| 0:01.5156
|-
! 27
| 33
| {{flagicon|AUS}} [[Lee Holdsworth]]
| [[Garry Rogers Motorsport]]
| [[Holden VZ Commodore]]
| 1:24.0235 	
| 0:01.5806
|-
! 28
| 10
| {{flagicon|AUS}} [[Jason Bargwanna]]
| [[WPS Racing]]
| [[Ford BA Falcon]]
| 1:24.0301 	
| 0:01.5872
|-
! 29
| 25
| {{flagicon|AUS}} [[Warren Luff]]
| [[Britek Motorsport]]
| [[Ford BA Falcon]]
| 1:24.0447 	
| 0:01.6018
|-
! 30
| 20
| {{flagicon|AUS}} [[Marcus Marshall]]
| [[Paul Cruickshank Racing]]
| [[Ford BA Falcon]]
| 1:24.9228 	
| 0:02.4799
|-
! 31
| 26
| {{flagicon|AUS}} [[José Fernández (racing driver)|José Fernández]]
| [[Britek Motorsport]]
| [[Ford BA Falcon]]
| 1:25.6745 	
| 0:03.2316
|-
|}

===Top Ten Shootout Results===

{| class="wikitable" style="font-size: 85%;"
|-
! Pos
! No
! Name
! Team
! Time
! Gap
|-
! 1
| 2
| {{flagicon|AUS}} [[Mark Skaife]]
| [[Holden Racing Team]]
| 1:21.6102
| 
|-
! 2
| 22
| {{flagicon|AUS}} [[Todd Kelly]]
| [[Holden Racing Team]]
| 1:22.0198 	
| + 0.4096s
|-
! 3
| 16
| {{flagicon|AUS}} [[Garth Tander]]
| [[HSV Dealer Team]]
| 1:22.2222 	
| + 0.6120s
|-
! 4
| 3
| {{flagicon|NZL}} [[Jason Richards]]
| [[Tasman Motorsport]]
| 1:22.2318 	
| + 0.6216s
|-
! 5
| 11
| {{flagicon|AUS}} [[Paul Dumbrell]]
| [[Perkins Engineering]]
| 1:22.2323 	
| + 0.6221s
|-
! 6
| 88
| {{flagicon|AUS}} [[Jamie Whincup]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 1:22.2405 	
| + 0.6303s
|-
! 7
| 888
| {{flagicon|AUS}} [[Craig Lowndes]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 1:22.5121 	
| + 0.9019s
|-
! 8
| 15
| {{flagicon|AUS}} [[Rick Kelly]]
| [[HSV Dealer Team]]
| 1:22.5367 	
| + 0.9265s
|-
! 9
| 021
| {{flagicon|NZL}} [[Paul Radisich]]
| [[Team Kiwi Racing]]
| 1:22.5690 	
| + 0.9588s
|-
! 10
| 51
| {{flagicon|NZL}} [[Greg Murphy]]
| [[Paul Weel Racing]]
| 1:23.1496 	
| + 1.5394s
|-
|}

== Race Results ==

=== Race 1 ===

{| class="wikitable" style="font-size: 85%;"
|-
! Pos
! No
! Driver
! Team
! Laps
! Time
! Grid
|-
! 1
| 888
| {{flagicon|AUS}} [[Craig Lowndes]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 78
| 2:04:18.5229
| 7
|-
! 2
| 15
| {{flagicon|AUS}} [[Rick Kelly]]
| [[HSV Dealer Team]]
| 78
| 2:04:19.7823
| 8
|-
! 3
| 88
| {{flagicon|AUS}} [[Jamie Whincup]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 78
| 2:04:20.1415
| 6
|-
! 4
| 50
| {{flagicon|AUS}} [[Cameron McConville]]
| [[Paul Weel Racing]]
| 78
| 2:04:21.2972
| 22
|-
! 5
| 51
| {{flagicon|NZL}} [[Greg Murphy]]
| [[Paul Weel Racing]]
| 78
| 2:04:23.8766
| 17
|-
! 6
| 22
| {{flagicon|AUS}} [[Todd Kelly]]
| [[Holden Racing Team]]
| 78
| 2:04:25.2578
| 2
|-
! 7
| 7
| {{flagicon|NZL}} [[Steven Richards]]
| [[Perkins Engineering]]
| 78 	
| 2:04:26.9040
| 26
|-
! 8
| 16
| {{flagicon|AUS}} [[Garth Tander]]
| [[HSV Dealer Team]]
| 78
| 2:04:27.9997
| 3
|-
! 9
| 1
| {{flagicon|AUS}} [[Russell Ingall]]
| [[Stone Brothers Racing]]
| 78	
| 2:04:32.5528
| 15
|-
! 10
| 17
| {{flagicon|AUS}} [[Steven Johnson (racing driver)|Steven Johnson]]
| [[Dick Johnson Racing]]
| 78
| 2:04:37.6592
| 13
|-
! 11
| 67
| {{flagicon|AUS}} [[Paul Morris (racing driver)|Paul Morris]]
| [[Paul Morris Motorsport]]
| 78	
| 2:04:43.9176
| 14
|-
! 12
| 11
| {{flagicon|AUS}} [[Paul Dumbrell]]
| [[Perkins Engineering]]
| 78
| 2:04:47.0540
| 5
|-
! 13
| 26
| {{flagicon|AUS}} [[José Fernández (racing driver)|José Fernández]]
| [[Britek Motorsport]]
| 78 	
| 2:05:23.9995
| 31
|-
! 14
| 18
| {{flagicon|AUS}} [[Will Davison]]
| [[Dick Johnson Racing]]
| 77 	
| 2:04:33.2334
| 18
|-
! 15
| 23
| {{flagicon|AUS}} [[Andrew Jones (racing driver)|Andrew Jones]]
| [[Tasman Motorsport]]
| 77
| 2:04:38.1095
| 23
|-
! 16
| 8
| {{flagicon|BRA}} [[Max Wilson]]
| [[WPS Racing]]
| 77
| 2:04:40.6557
| 11
|-
! 17
| 25
| {{flagicon|AUS}} [[Warren Luff]]
| [[Britek Motorsport]]
| 77 	
| 2:04:45.5730
| 28
|-
! 18
| 33
| {{flagicon|AUS}} [[Lee Holdsworth]]
| [[Garry Rogers Motorsport]]
| 77
| 2:04:46.2466
| 27
|-
! 19
| 12
| {{flagicon|AUS}} [[John Bowe (racing driver)|John Bowe]]
| [[Brad Jones Racing]]
| 77	
| 2:04:47.6441
| 19
|-
! 20
| 34
| {{flagicon|AUS}} [[Dean Canto]]
| [[Garry Rogers Motorsport]]
| 77	
| 2:04:58.1453
| 20
|-
! 21
| 39
| {{flagicon|AUS}} [[Alan Gurr]]
| [[Paul Morris Motorsport]]
| 77	
| 2:05:26.5529
| 25
|-
! 22
| 5
| {{flagicon|AUS}} [[Mark Winterbottom]]
| [[Ford Performance Racing]]
| 74
| 2:04:46.7748
| 7
|-
! 23
| 021
| {{flagicon|NZL}} [[Paul Radisich]]
| [[Team Kiwi Racing]]
| 67
| 2:04:52.8263
| 9
|-
! 24
| 20
| {{flagicon|AUS}} [[Marcus Marshall]]
| [[Paul Cruickshank Racing]]
| 64 	
| 2:05:05.6860
| 30
|-
! 25
| 6
| {{flagicon|AUS}} [[Jason Bright]]
| [[Ford Performance Racing]]
| 60
| 2:04:57.7336
| 24
|-
! DNF
| 14
| {{flagicon|AUS}} [[Brad Jones (racing driver)|Brad Jones]]
| [[Brad Jones Racing]]
| 73
| 1:57:38.3104
| 21
|-
! DNF
| 55
| {{flagicon|AUS}} [[Steve Owen (racing driver)|Steve Owen]]
| [[Rod Nash Racing]]
| 57	
| 1:31:47.2884
| 16
|-
! DNF
| 10
| {{flagicon|AUS}} [[Jason Bargwanna]]
| [[WPS Racing]]
| 50 	
| 1:18:33.5801
| 28
|-
! DNF
| 4
| {{flagicon|AUS}} [[James Courtney]]
| [[Stone Brothers Racing]]
| 42
| 1:04:14.4473
| 8
|-
! DNF
| 2
| {{flagicon|AUS}} [[Mark Skaife]]
| [[Holden Racing Team]]
| 34
| 0:48:57.2431
| 1
|-
! DNF
| 3
| {{flagicon|NZL}} [[Jason Richards]]
| [[Tasman Motorsport]]
| 5
| 0:07:13.6780
| 4
|-
|}

===Race 2===
{| class="wikitable" style="font-size: 85%;"
|-
! Pos
! No
! Name
! Team
! Laps
! Time
! Grid
|-
! 1
| 88
| {{flagicon|AUS}} [[Jamie Whincup]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 78
| 2:01:28.4014
| 3
|-
! 2
| 22
| {{flagicon|AUS}} [[Todd Kelly]]
| [[Holden Racing Team]]
| 78
| 2:01:30.3278
| 6
|-
! 3
| 15
| {{flagicon|AUS}} [[Rick Kelly]]
| [[HSV Dealer Team]]
| 78
| 2:01:38.3490
| 2
|-
! 4
| 16
| {{flagicon|AUS}} [[Garth Tander]]
| [[HSV Dealer Team]]
| 78
| 2:01:44.1111
| 8
|-
! 5
| 7
| {{flagicon|NZL}} [[Steven Richards]]
| [[Perkins Engineering]]
| 78 	
| 2:01:46.0367
| 7
|-
! 6
| 11
| {{flagicon|AUS}} [[Paul Dumbrell]]
| [[Perkins Engineering]]
| 78
| 2:01:47.2022
| 12
|-
! 7
| 1
| {{flagicon|AUS}} [[Russell Ingall]]
| [[Stone Brothers Racing]]
| 78	
| 2:01:47.7886
| 9
|-
! 8
| 8
| {{flagicon|BRA}} [[Max Wilson]]
| [[WPS Racing]]
| 78
| 2:01:48.7274
| 16
|-
! 9
| 17
| {{flagicon|AUS}} [[Steven Johnson (racing driver)|Steven Johnson]]
| [[Dick Johnson Racing]]
| 78
| 2:01:49.2540
| 10
|-
! 10
| 3
| {{flagicon|NZL}} [[Jason Richards]]
| [[Tasman Motorsport]]
| 78
| 2:01:49.8126
| 31
|-
! 11
| 10
| {{flagicon|AUS}} [[Jason Bargwanna]]
| [[WPS Racing]]
| 78 	
| 2:01:52.2221
| 28
|-
! 12
| 021
| {{flagicon|NZL}} [[Paul Radisich]]
| [[Team Kiwi Racing]]
| 78
| 2:01:53.1155
| 23
|-
! 13
| 18
| {{flagicon|AUS}} [[Will Davison]]
| [[Dick Johnson Racing]]
| 78	
| 2:01:59.5902
| 14
|-
! 14
| 12
| {{flagicon|AUS}} [[John Bowe (racing driver)|John Bowe]]
| [[Brad Jones Racing]]
| 78
| 2:02:01.2411
| 19
|-
! 15
| 6
| {{flagicon|AUS}} [[Jason Bright]]
| [[Ford Performance Racing]]
| 78
| 2:02:18.6223
| 25
|-
! 16
| 25
| {{flagicon|AUS}} [[Warren Luff]]
| [[Britek Motorsport]]
| 78 	
| 2:02:19.4268
| 17
|-
! 17
| 67
| {{flagicon|AUS}} [[Paul Morris (racing driver)|Paul Morris]]
| [[Paul Morris Motorsport]]
| 77
| + 1 Lap
| 11
|-
! 18
| 34
| {{flagicon|AUS}} [[Dean Canto]]
| [[Garry Rogers Motorsport]]
| 77	
| + 1 Lap
| 20
|-
! 19
| 5
| {{flagicon|AUS}} [[Mark Winterbottom]]
| [[Ford Performance Racing]]
| 76
| + 2 Laps
| 22
|-
! 20
| 14
| {{flagicon|AUS}} [[Brad Jones (racing driver)|Brad Jones]]
| [[Brad Jones Racing]]
| 75
| + 3 Laps
| 26
|-
! 21
| 3
| {{flagicon|AUS}} [[Andrew Jones (racing driver)|Andrew Jones]]
| [[Tasman Motorsport]]
| 75
| + 3 Laps
| 15
|-
! 22
| 39
| {{flagicon|AUS}} [[Alan Gurr]]
| [[Paul Morris Motorsport]]
| 74
| + 4 Laps
| 21
|-
! 23
| 33
| {{flagicon|AUS}} [[Lee Holdsworth]]
| [[Garry Rogers Motorsport]]
| 72
| + 6 Laps
| 18
|-
! Ret
| 20
| {{flagicon|AUS}} [[Marcus Marshall]]
| [[Paul Cruickshank Racing]]
| 72
| Retired
| 24
|-
! Ret
| 51
| {{flagicon|NZL}} [[Greg Murphy]]
| [[Paul Weel Racing]]
| 56
| Accident
| 5
|-
! Ret
| 2
| {{flagicon|AUS}} [[Mark Skaife]]
| [[Holden Racing Team]]
| 56
| Accident
| 30
|-
! Ret
| 4
| {{flagicon|AUS}} [[James Courtney]]
| [[Stone Brothers Racing]]
| 50
| Accident
| 29
|-
! Ret
| 50
| {{flagicon|AUS}} [[Cameron McConville]]
| [[Paul Weel Racing]]
| 50
| Accident
| 4
|-
! Ret
| 888
| {{flagicon|AUS}} [[Craig Lowndes]]
| [[Triple Eight Race Engineering (V8 Supercars)|Triple Eight Race Engineering]]
| 48
| Retired
| 1
|-
! EXC
| 26
| {{flagicon|AUS}} [[José Fernández (racing driver)|José Fernández]]
| [[Britek Motorsport]]
| 
|  	
| 
|}

==References==
{{Reflist}}

==External links==
*[http://www.v8supercars.com.au V8 Supercars Website]

{{V8 race report|
 Name_of_race = Clipsal 500 |
 Year_of_race = 2006 |
 Previous_race_in_season = [[2005 Grand Finale]] |
 Next_race_in_season = [[2006 PlaceMakers V8 Supercars]] |
 Previous_year's_race = [[2005 Clipsal 500]] |
 Next_year's_race = [[2007 Clipsal 500]]
}}

[[Category:2006 in V8 Supercars|Clipsal 500 Adelaide]]
[[Category:2000s in Adelaide]]
[[Category:Motorsport in Adelaide]]