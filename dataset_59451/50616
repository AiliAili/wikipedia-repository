{{Infobox journal
| title = Asian Perspectives
| formernames = Bulletin of the Far-Eastern Prehistory Association (American Branch)
| cover = [[File:AsianPerspectives.gif]]
| discipline = [[Archaeology]]
| abbreviation = Asian Perspect.
| frequency = Biannual
| publisher = [[University of Hawaii Press]]
| country = United States
| history = 1957–present
| website = http://www.uhpress.hawaii.edu/journals/ap/
| link1 = http://muse.jhu.edu/journals/asian_perspectives/
| link1-name = Online access at [[Project MUSE]]
| link2 = http://scholarspace.manoa.hawaii.edu/handle/10125/14928
| link2-name = Online access at [[ScholarSpace]]
| ISSN = 0066-8435
| eISSN = 1535-8283
| OCLC = 898817061
}}
'''''Asian Perspectives: The Journal of Archaeology for Asia and the Pacific''''' is an [[academic journal]] covering the [[History of Asia|history]] and prehistory of [[Asia]] and the [[Pacific]] region. In addition to [[archaeology]], it features articles and book reviews on [[ethnoarchaeology]], [[palaeoanthropology]], [[physical anthropology]], and [[ethnography]]. The journal was established in 1957 as the ''Bulletin of the Far-Eastern Prehistory Association (American Branch)'' under the [[Editor-in-chief|editorship]] of [[Wilhelm G. Solheim II]], then followed its editor to other institutions. Volumes II (1958) through VIII (1964) were published by [[Hong Kong University Press]], and volumes IX (1966) through XI (1968) by the Social Science Research Institute at the [[University of Hawaii]]. The [[University of Hawaii Press]] became the publisher from volume XII (1969), adding the subtitle ''A Journal of Archaeology and Prehistory of Asia and the Pacific.''<ref>{{cite web |hdl=10125/16993 |title=Editorial |author=Wilhelm G. Solheim II |work=Asian Perspectives XXX:iii-iv |year=1991 |publisher=University of Hawaii Press}}</ref> In 1992, the editorship passed to Michael W. Graves and the subtitle was changed to ''The Journal of Archaeology for Asia and the Pacific''. Miriam Stark at the University of Hawai{{okina}}i served as editor from 2000 through 2006, then the editorship passed to three-person team: Deborah Bekken ([[Field Museum]]), Laura Lee Junker ([[University of Illinois at Chicago]]), and Anne P. Underhill ([[Yale University]]).

The journal appears biannually in March and September. Its first electronic edition appeared in 2000 (vol. 39) on [[Project MUSE]]. Back issues are being added to an open-access archive in the University of Hawaii at Mānoa's [[ScholarSpace]] [[institutional repository]].

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.uhpress.hawaii.edu/journals/ap/}}
* [http://uhpjournals.wordpress.com/2000/11/06/asian-perspectives-vols-1-39-1957-2000/ Online index to vols. 1-39]

[[Category:English-language journals]]
[[Category:University of Hawaii Press academic journals]]
[[Category:Archaeology journals]]
[[Category:Publications established in 1957]]
[[Category:Biannual journals]]
[[Category:Asian studies journals]]
[[Category:Oceania studies journals]]