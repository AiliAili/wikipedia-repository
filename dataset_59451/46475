{{COI|date=October 2016}}
{{Infobox person
| honorific_prefix =
| name             = Morris B. Belknap
| honorific_suffix =
| image            = Morris-B.-Belknap.jpg
| image_size       =
| alt              =
| caption          = Portrait of Belknap from Johnson's ''A History of Kentucky and Kentuckians'' (1912)
| birth_name       = Morris Burke Belknap
| birth_date       = {{birth date|1856|06|07}}
| birth_place      = [[Louisville, Kentucky]]
| death_date       = {{death date and age|1910|04|13|1856|06|07}}
| death_place      =
| death_cause      = [[Pernicious anemia]]
| resting_place    = [[Cave Hill Cemetery]]
| resting_place_coordinates = <!-- {{coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments        =
| residence        =
| alma_mater       = [[Sheffield Scientific School]] ([[Yale University]])
| occupation       = Businessman
| known_for        = Vice-president of [[Belknap Hardware and Manufacturing Company]]; nominee for [[Governor of Kentucky]]
| home_town        =
| title            =
| term             =
| predecessor      =
| successor        =
| party            = [[Republican Party (United States)|Republican]]
| boards           =
| religion         = [[Christianity|Christian]]
| denomination     = [[Presbyterianism|Presbyterian]]
| spouse           = {{marriage|Lily Buckner|June 14, 1883|1893|end=died}}<br />{{marriage|Marion S. Dumont|July 16, 1900}}
| children         = 4
| parents          = [[W. B. Belknap|William Burke Belknap]] and Mary (Richardson) Belknap
| relatives        = Son-in-law of [[Simon Bolivar Buckner]]
| awards           =
| signature        = Morris-B.-Belknap-sig.jpg
| signature_alt    = Morris B. Belknap
| signature_size   =
}}
'''Morris Burke Belknap''' (June 7, 1856 – April 13, 1910), also known as '''Colonel Morris Burke Belknap''', was a businessman from [[Louisville, Kentucky]], who was the [[Republican Party (United States)|Republican]] nominee for [[Governor of Kentucky]] in 1903. After earning a degree from the [[Sheffield Scientific School]] of [[Yale University]], he worked at his father's hardware company. Later, he co-founded an agricultural implement company. In 1883, he married Lily Buckner, with whom he fathered four children. Following the death of his father, Belknap became vice-president of his hardware company, a position which he held for the rest of his life. Lily Buckner Belknap died in 1893, and he married Marion S. Dumont in 1900.

In addition to his business career. Belknap served in the Kentucky State Guard, eventually rising to the rank of [[lieutenant colonel]]. In 1898, during the [[Spanish–American War]], he served briefly in [[Puerto Rico]] prior to the end of hostilities. He was promoted to [[colonel]] before his honorable discharge in 1899.

In 1903, Belknap accepted the nomination for state governor after being chosen at the Kentucky Republican convention in that year. He campaigned on his business experience and opposition to policies of the current governor, but he was ultimately defeated by the incumbent [[J. C. W. Beckham]].

After his unsuccessful gubernatorial run, he spent his later life involved in a variety of civil and religious organizations. On April 13, 1910, he died of [[pernicious anemia]], a condition which is believed to have been worsened by a mysterious ailment he contracted while in Puerto Rico.

==Early life and family==
Belknap was born at his parents' home in [[Louisville, Kentucky]], on June 7, 1856.<ref name=johnson1154>{{cite book |last=Johnson |first=E. Polk |title=A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities |volume=3 |publisher=Lewis Publishing Company |location=[[Chicago, Illinois]] |year=1912 |url=https://books.google.com/books?id=aksVAAAAYAAJ |page=1154}}</ref> He was the youngest child of [[W. B. Belknap|William Burke]] and Mary (Richardson) Belknap.<ref name=stephens81>{{cite book |last=Stephens |first=Thomas E. |chapter=Belknap, Morris Burke |title=The Encyclopedia of Louisville |editor=John E. Kleber |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |year=2001 |url=https://books.google.com/books?id=pXbYITw4ZesC}}</ref> Until the age of 17, he was educated in the private school of Benjamin B. Huntoon.<ref name=johnson1154 /><ref name=stephens81 /> In 1873, he and his brother, [[William Richardson Belknap]] (1849–1914), spent a year traveling in [[Europe]].<ref name=johnson1154 /><ref name="yalebulletin">{{cite journal |title=Obituary Records of Graduates of Yale University |journal=Bulletin of Yale University |pages=1311–1312 |url=https://books.google.com/books?id=5rk3AAAAMAAJ&pg=PA1311 |publisher=[[Yale University]] |date=July 1910 |volume=6 |number=9}}</ref> On his return, he enrolled in the [[Sheffield Scientific School]] of [[Yale University]], where he was a classmate of future [[President of the United States|U.S. President]] [[William Howard Taft]].<ref name=johnson1154 /> He earned his degree in 1877, but spent an additional year of postgraduate study in chemistry.<ref name=johnson1154 /><ref name="yalebulletin" />

Returning to Louisville in 1879, Belknap worked for his father's hardware company, [[Belknap Hardware and Manufacturing Company|W. B. Belknap and Company]], for a short time before partnering with Thomas Miekle and Barry Coleman to form Thomas Miekle and Company, an agricultural implement manufacturer.<ref name=stephens81 /> He returned to W. B. Belknap and Company in 1883 as a secretary.<ref name=stephens81 />

On June 14, 1883, Belknap married Lily Buckner, the daughter of [[Confederate States of America|Confederate]] general [[Simon Bolivar Buckner]].<ref name=johnson1155>{{cite book |last=Johnson |first=E. Polk |title=A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities |volume=3 |publisher=Lewis Publishing Company |location=[[Chicago, Illinois]] |year=1912 |url=https://books.google.com/books?id=aksVAAAAYAAJ |page=1155}}</ref> The couple had four children – Gertrude, Walter Kingsbury, Lily, and Morris, Jr.<ref name=johnson1155 /> Lily Belknap died in 1893.<ref name=johnson1155 /> After her death, Belknap constructed a stone bridge – dubbed Belknap Bridge – in Louisville's [[Cherokee Park]] in her honor.<ref>Johnson, E. Polk (1912). A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities 3. Chicago, Illinois: Lewis Publishing Company. p. 1154.</ref><ref>[http://digital.library.louisville.edu/cdm/singleitem/collection/matlack/id/451/rec/9 Belknap Bridge. :: Claude C. Matlack Collection]</ref>	

Belknap's father died in 1889, and after incorporating his company as [[Belknap Hardware and Manufacturing Company]], Morris Belknap became the company's vice-president, a position he held for the remainder of his life.<ref name=johnson1154 /> As vice-president, he began promoting his company on a national scale and became one of the South's leading businessmen.<ref name=stephens81 /> He was the youngest person elected president of the Louisville Board of Trade, and he held that position for many years.<ref name=johnson1154 />

==Military service==
In 1879, Belknap enlisted as a [[private (rank)|private]] in the First Infantry Regiment of the Kentucky State Guard, known as the [[Louisville Legion]].<ref name=johnson1154 /> His father-in-law was elected [[Governor of Kentucky]] in 1887 and appointed Belknap to his military staff with the rank of [[Colonel (United States)|colonel]].<ref name=johnson1155 /> In 1890, he was elected [[Captain (United States)|captain]] of Company A of the First Regiment in the Kentucky State Guard, and in 1893, he was chosen as the unit's [[Lieutenant colonel (United States)|lieutenant colonel]].<ref name=johnson1155 />

At the commencement of the [[Spanish–American War]], Governor [[William O'Connell Bradley]] called up Belknap's unit for service.<ref name=stephens81 /> He was mustered into federal service on May 13, 1898, and departed the same day, spending several weeks at [[Lexington, Kentucky]], before arriving at [[Newport News, Virginia]], on July 28, 1898.<ref name=johnson1155 /> On August 1, General [[Frederick Dent Grant]] ordered six companies under Belknap to board the [[USRC Hudson (1893)|USRC ''Hudson'']] bound for [[Puerto Rico]].<ref name=johnson1155 /> Arriving at [[Ponce, Puerto Rico|Ponce]] on August 11, the men were forwarded to [[Mayagüez, Puerto Rico|Mayagüez]] by General [[Nelson A. Miles]].<ref name=johnson1155 /> The next day, the men disembarked and were ordered by General [[Theodore Schwan]] to follow the [[11th Infantry Regiment (United States)|11th Infantry Regiment]] inland as soon as transport could be secured.<ref name=johnson1155 />

On August 13, a special messenger informed Schwan that hostilities had ceased.<ref name=johnson1155 /> Belknap and his men remained at Mayagüez until August 26, then returned to Ponce, reuniting with the regiment's six other companies under [[John Breckinridge Castleman]] on August 29 and 30.<ref name=johnson1155 /> Upon Castleman's elevation to [[Brigadier general (United States)|brigadier general]], Belknap was promoted to colonel and given command of the First Kentucky.<ref name=stephens81 /> The unit returned to Louisville on December 12, 1898; Belknap was honorably [[Military discharge|discharged]] on February 24, 1899.<ref name=stephens81 /><ref name=johnson1155 /> Although he saw no active service, Belknap contracted a mysterious disease in Puerto Rico from which he never fully recovered and which was believed to have contributed to his death.<ref name=stephens81 />

On July 16, 1900, Belknap married Marion S. Dumont of [[Plainfield, New Jersey]].<ref name=johnson1155 />

==Campaign for governor==
At the 1903 [[Republican Party (United States)|Republican]] state convention, party leaders [[John W. Yerkes]] and [[W. Godfrey Hunter]] supported Belknap as the party's gubernatorial nominee.<ref name=klotter206>{{cite book |authorlink=James C. Klotter |last=Klotter |first=James C. |title=Kentucky: Portraits in Paradox, 1900–1950 |publisher=The University Press of Kentucky |year=1996 |isbn=0-916968-24-3 |url=https://books.google.com/books?id=o58mJavC4msC}}</ref> Former governor Bradley, however, supported a fellow Louisvillian, attorney [[Augustus E. Willson]].<ref name=klotter206 /> When a pro-Willson delegation was unseated by a challenge, Willson withdrew and Bradley bolted the convention.<ref name=klotter206 /> Consequently, Belknap was chosen as the party's nominee on the first ballot.<ref name=klotter206 /><ref name="bee1903">{{cite news |url=https://www.newspapers.com/clip/6991920// |title=BELKNAP WINS. Bradley's Name Placed Before Convention Without His Consent. Pratt Secures Next Greatest Amount of Votes—Wilson's Name Not Mentioned. |work=The Bee |location=Earlington, Kentucky |date=July 23, 1903 |via=[[Newspapers.com]]}} {{free access}}</ref> With the [[Democratic Party (United States)|Democrats]] divided by factionalism, the Republicans tried to woo erstwhile Democrats by nominating former Democratic congressman [[William M. Beckner]] for a down-ballot office.<ref name=klotter206 /> Former Democratic governor Buckner also supported his son-in-law's candidacy.<ref name=tapp462>{{cite book |last=Tapp |first=Hambleton |author2=James C. Klotter |title=Kentucky: Decades of Discord, 1865–1900 |publisher=The University Press of Kentucky |year=1977 |isbn=0-916968-05-7 |url=https://books.google.com/books?id=n7JIP_B_vQMC |location=[[Lexington, Kentucky]]}}</ref>

Belknap touted his business management experience, contrasting it with charges that his opponent, incumbent Governor [[J. C. W. Beckham]], had mismanaged the state's [[Charity (practice)|eleemosynary]] institutions.<ref name=klotter206 /> He also attacked Beckham for having risen to power by building a [[political machine]].<ref name=klotter206 /> Belknap lacked name recognition outside Louisville. He was a poor public speaker, and he was unable to make the race truly competitive.<ref name=klotter206 /> The [[New York Times]] reported, "Col. Morris B. Belknap, the Republican nominee for Governor of Kentucky, is an athlete as well as a politician and a millionaire businessman. It has just leaked out that the Colonel used his muscles with good effect the other day in order to keep a speaking appointment at [[Central City, Kentucky|Central City]], where several hundred coal miners had assembled to hear him speak. He missed his train at [[Russellville, Kentucky|Russellville]] and tried to make the trip to Central City by [[Driving (horse)|driving]]. Fearing that he would be too late, he arranged for a [[handcar|hand-car]] at a small station. Two men were given him to pump him over to Central City. They became tired and the Colonel relieved them by taking a hand himself. He worked desperately and the hand-car pulled into the little town a few minutes ahead of time."<ref>{{cite news|title=An Athletic Candidate: Col. Belknap, Nominee for Governor of Kentucky, Keeps an Appointment by Pumping a Handcar|newspaper=The New York Times|date=September 8, 1903}}</ref> Election day was a rowdy one throughout the state, and a judge supporting Belknap was shot by a sheriff at a polling place in Louisville.<ref>{{cite news|title=An Unfortunate Shooting Mars the Day at a Voting Place in Louisville--The Returns by Counties|newspaper=Cincinnati Enquirer|location=Cincinnati, Ohio|date=November 4, 1903|page=3}}</ref> Victory went to Belknap's opponent, and Beckham won the election 229,014 to 202,764, marking the first time in sixteen years that the Democrats had gained a majority of the votes cast.<ref name=klotter206 />

==Later life==
Belknap continued to be active in business, attending the International Congress of Chambers of Commerce in [[Liège]] in 1905.<ref name=johnson1155 /> From 1907 to 1909, he was chairman of the Louisville Board of Parks.<ref name=stephens81 /> He was a member of several civic and religious organizations, including the [[Pendennis Club]], the Louisville Country Club, the [[Salmagundi Club]] of Louisville, and the Warren Memorial Presbyterian Church of Louisville, where he was a [[deacon]] and chairman of the board of trustees.<ref name=johnson1155 /> He was president of the [[Yale alumni|Yale Alumni Association]] in Louisville and director of that city's [[YMCA]].<ref name=johnson1155 />[[File:Midlands, Louisville.jpg|thumb|Midlands, Louisville]]

Belknap fell ill in late 1907, and his poor health continued into mid-1908.<ref name=obit>{{cite journal |title=Death of Col. Morris Belknap |magazine=Hardware Dealers' Magazine |volume=33 |date=January  1910 |url=https://books.google.com/books?id=SMRNAAAAMAAJ}}</ref>{{page needed|date=October 2016}} In July 1908, he traveled to Europe in order to improve his health, but after his return in October, his condition worsened again.<ref name=obit /> From January to June 1909, he was bedfast.<ref name=obit /> In early April 1910, he fell into a [[coma]] from which he never awakened.<ref name=obit /> He died of [[pernicious anemia]] on April 13, 1910, and was buried in [[Cave Hill Cemetery]].<ref name=stephens81 /> The [[Midlands (Louisville, Kentucky)|Midlands]], a private house in Louisville listed on the [[National Register of Historic Places]], was built (1913–1914) for Marion Dumont Belknap after her husband's death.

==References==
{{Reflist}}

==External links==
* [http://www.theodorerooseveltcenter.org/en/Research/Digital-Library/Record.aspx?libID=o44070 1904 letter from Belknap to Theodore Roosevelt supporting Roosevelt's re-election]

{{DEFAULTSORT:Belknap, Morris B.}}
[[Category:1856 births]]
[[Category:1910 deaths]]
[[Category:American military personnel of the Spanish–American War]]
[[Category:American Presbyterians]]
[[Category:Articles created via the Article Wizard]]
[[Category:Burials at Cave Hill Cemetery]]
[[Category:Businesspeople from Kentucky]]
[[Category:Kentucky gubernatorial candidates]]
[[Category:Kentucky Republicans]]
[[Category:Politicians from Louisville, Kentucky]]
[[Category:Yale School of Engineering & Applied Science alumni]]
[[Category:19th-century American politicians]]