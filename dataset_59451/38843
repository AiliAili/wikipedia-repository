{{Infobox Simpsons episode
| episode_name = Brother from the Same Planet
| image        =
| image_caption    =
| episode_no   = 73
| prod_code    = 9F12
| airdate      = February 4, 1993<ref name="variety" />
| show runner  = [[Al Jean]] & [[Mike Reiss]]
| writer       = [[Jon Vitti]]<ref name="martyn" />
| director     = [[Jeffrey Lynch]]<ref name="martyn" />
| blackboard   = "The Principal's toupee is not a frisbee"
| couch_gag = The rear wall rotates taking the family to another room and leaving an empty couch behind.<ref name="martyn" />
| guest_star   = [[Phil Hartman]] as Tom and Mr. Muntz<ref name="deming">{{cite news  | last =Deming  | first =Mark  | title =The Simpsons: Brother From the Same Planet  | work =[[Allmovie]]  | publisher =Macrovision Corporation  | year =2008  | url =http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:284902 | accessdate = 2008-09-03 }}</ref>
| focus         = [[Bart Simpson|Bart]]<br>[[Lisa Simpson|Lisa]]
| commentary   = [[Matt Groening]]<br />[[Al Jean]]<br />[[Mike Reiss]]<br />[[Jon Vitti]]<br />[[Jeffrey Lynch]]
| season       = 4
}}
"'''Brother from the Same Planet'''" is the fourteenth episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 4)|fourth season]]. After [[Homer Simpson|Homer]] is late to pick up [[Bart Simpson|Bart]] from soccer practice, Bart turns to the program The Bigger Brothers, and is assigned a man named Tom. Homer gets himself a little brother named Pepi. Homer and Tom fight, and Homer reconciles with Bart, and Tom becomes Pepi's bigger brother. Meanwhile, [[Lisa Simpson|Lisa]] becomes addicted to the Corey hotline—a phone service where television fans can listen to the voice of a fictional actor based on popular actors [[Corey Feldman]] and [[Corey Haim]].

Originally airing on the [[Fox Broadcasting Company|Fox network]] in the United States on February 4, 1993, the episode was written by [[Jon Vitti]] and directed by [[Jeffrey Lynch]]. The producers tried to cast [[Tom Cruise]] for the role of Tom, but Cruise refused, and they used [[Phil Hartman]] instead. "Brother from the Same Planet" received favorable reception in books and in the media, and was highlighted among the five best episodes of the series by the writers of the Fox program ''[[King of the Hill]]''.

==Plot==
After soccer practice, [[Bart Simpson|Bart]] waits for [[Homer Simpson|Homer]] to pick him up, though his father has forgotten. When Homer finally remembers, he reasons to an angered Bart that they should just both admit they are wrong and try to put the issue behind them. Later, Bart views a commercial for a mentor program called The Bigger Brothers, which pairs up fatherless boys with positive male role models. Still angry at Homer, Bart goes to the agency pretending to be a young boy whose father was a drunken gambler who abandoned him. Barts story is so sad that the receptionist pairs him up with their best Bigger Brother, a military test pilot named Tom. The two spend time together doing a variety of activities, though Bart begins to feel guilty over taking up Tom's time despite not actually being fatherless. Eventually, Homer finds out about Bart's Bigger Brother and confronts Bart for it. Homer then decides to go to the Bigger Brothers Agency to get revenge by being assigned with a replacement son; a young poor boy named Pepi. Just like Tom and Bart, Homer and Pepi spend time together doing activities.

Later on, it is Bigger Brothers Day in Marine World, where the Bigger Brothers and their boys attend to celebrate (including Homer, Tom, Bart, and Pepi). After running into Homer, Tom begins to brawl with him, in a fight which rages across Springfield. In the end, Homer winds up getting poked on the back by a fire hydrant, breaking his spine, much to Tom's shock. As a result, Homer is sent to a hospital on a stretcher, and Bart holds himself responsible for this. Later on, Tom sadly talks about how much he will miss being a Bigger Brother, while Pepi is sad over losing his Bigger Brother. Fortunately, Bart suggests Tom become Pepi's big brother, to which they happily agree, and both Tom and Pepi walk out into the sunset holding hands. At the same time, Homer (whose back is now fixed) and Bart reconcile, as Homer teaches Bart how to brawl due to his experience with Tom.

In the subplot, [[Marge Simpson|Marge]] finds an anomalously high phone bill for calls made by [[Lisa Simpson|Lisa]] to the Corey hotline — a [[premium rate]] phone service where television fans can listen to the voice of a fictional actor based on [[The Two Coreys]]. Lisa promises to stop increasing the family's phone bill, but continues to make calls to the hotline from such places as [[Julius Hibbert|Doctor Hibbert]]'s office and a telephone at [[Springfield Elementary School|Springfield Elementary]]. After [[Seymour Skinner|Principal Skinner]] catches her, Marge suggests that Lisa try to go until midnight without calling the hotline; if she can do so, she will have conquered her addiction. Although tempted throughout the rest of the day, Lisa beats her addiction.

==Production==
[[File:Tom Cruise avp 2014 4.jpg|thumb|The character Tom was originally written for [[Tom Cruise]].]]
"Brother from the Same Planet" was written by [[Jon Vitti]] and directed by [[Jeffrey Lynch]].<ref name="martyn" /> It originally aired in the United States on February 4, 1993, on [[Fox Broadcasting Company|Fox]].<ref name="variety" /> The writers wrote the role of Tom for actor [[Tom Cruise]].<ref name="Reiss"/> However, when offered the part, Cruise repeatedly turned it down, so the producers used [[Phil Hartman]] instead.<ref name="Reiss">Reiss, Mike (2004). Commentary for "Brother from the Same Planet", in ''The Simpsons: The Complete Fourth Season'' [DVD]. 20th Century Fox.</ref> The writers based the Corey character on the actors [[Corey Feldman]] and [[Corey Haim]], known as [[The Two Coreys]].<ref name="Jean">Jean, Al (2004). Commentary for "Brother from the Same Planet", in ''The Simpsons: The Complete Fourth Season'' [DVD]. 20th Century Fox.</ref> Pepi was based on the fictional character [[Dondi]] from the daily comic strip of the same name.<ref name="Jean"/>

In the episode, Bart and Tom watch ''[[The Ren & Stimpy Show]]''. The producers contacted [[Nickelodeon (TV channel)|Nickelodeon]] to get authorization to use the two characters for that sequence.<ref name="Jean"/> Nickelodeon was strict about what ''The Simpsons'' was allowed to do, and the producers were not allowed to have the savageness that they wanted.<ref name="Jean"/> ''The Ren & Stimpy Show''<nowiki>'</nowiki>s animators offered to do the layouts of Ren and Stimpy for the episode.<ref name="Vitti">Vitti, Jon (2004). Commentary for "Brother from the Same Planet", in ''The Simpsons: The Complete Fourth Season'' [DVD]. 20th Century Fox.</ref>

The television show Bart watches, ''Tuesday Night Live'', is a parody of [[NBC]]'s ''[[Saturday Night Live]]''. Krusty appears in a sketch called "The Big Ear Family", and says that the sketch goes on for twelve more minutes, even though the joke's punchline has already been established.<ref name="Vitti"/> That was [[Jon Vitti]]'s way of criticizing ''Saturday Night Live'' for having overlong sketches with thin joke premises.<ref name="Vitti"/> The sequence originally had a longer version of the ''Tuesday Night Live'' band playing into the commercial break, but it was cut because Vitti, who was a writer on ''Saturday Night Live'' during the 1985–86 season along with fellow ''The Simpsons'' writers, [[George Meyer]] and [[John Swartzwelder]], did not want to come off as being bitter.<ref name="Vitti"/>

The writing staff was looking for a way to end the episode and executive producer [[Sam Simon]] suggested that they watch the film ''[[The Quiet Man]]''. The writers came in on a Saturday to watch the film together. They were inspired by the film's fight scene between [[John Wayne]] and [[Victor McLaglen]]'s characters to do a fight scene between Homer and Tom in the episode.<ref name="Vitti"/> The scene was difficult for the producers to sound-mix because they wanted it to be funny but not horrifying. They discovered that the more realistic the effects used sounded, the funnier the scene became.<ref name="Groening">Groening, Matt (2004). Commentary for "Brother from the Same Planet", in ''The Simpsons: The Complete Fourth Season'' [DVD]. 20th Century Fox.</ref> The producers tried all sorts of different sounds for when Homer cracks his back on the fire hydrant and chose the tiniest realistic sound, because they believed that it was the most painful and "hilarious".<ref name="Groening"/>

==Cultural references==
* The scene in which [[Milhouse Van Houten|Milhouse]] writes "Trab pu kcip! "Trab pu kcip!" on the wall is a reference to "red rum" from [[Stanley Kubrick]]'s 1980 film ''[[The Shining (film)|The Shining]]''.<ref>{{cite news|last=Rogers|first=Nicole E.|title=Latest Book Feeds Mania|work=Wisconsin State Journal|page=D1|publisher=Madison Newspapers, Inc.|date=October 22, 2002}}</ref><ref>{{cite news|last=Star-Ledger Staff|title=Readers point out more evidence of 'Simpsons'-Kubrick connection|work=[[The Star-Ledger]]|page=43|date=March 13, 1999}}</ref>
* The woman Bart mistakes for Homer is singing the [[Helen Reddy]] song "[[I Am Woman]]".<ref name="martyn"/>
* While Bart is stuck in the storm waiting for Homer, a nun is lifted up by the wind, a reference to the TV series ''[[The Flying Nun]]''.<ref name="martyn"/>
* Bart and Tom watch ''[[The Ren & Stimpy Show]]''.<ref name="Jean"/>
* Homer watches an [[NFL Films]] production about [[Bart Starr]], the [[quarterback]] on the [[Green Bay Packers]] who led the team to victory in the first two [[Super Bowls]].<ref name="Jean"/>
* The scene where Homer accuses Bart of seeing his big brother is a reference to the 1966 film ''[[Who's Afraid of Virginia Woolf? (film)|Who's Afraid of Virginia Woolf?]]'', where [[Richard Burton]] accuses [[Elizabeth Taylor]] of [[adultery]].<ref name="martyn"/>
* In a made-up story Homer tells Pepi, Bart tells Homer to shut up and shoves half a grapefruit in his face, a reference to the 1931 film ''[[The Public Enemy]]''.<ref name="Jean"/>
* Bart watches ''Tuesday Night Live'', a parody of [[NBC]]'s ''[[Saturday Night Live]]''.<ref name="Vitti"/>
* The background music used for the fight scene is a knockoff of the music used in the fight scene in ''[[The Quiet Man]]''.<ref name="Reiss"/>
* Skinner's creepy monologue about his mother watching him is a parody of Norman Bates' similar dialogue from the 1960 Hitchcock film ''[[Psycho (1960 film)|Psycho]]''.<ref>{{cite web |url=http://www.imdb.com/title/tt0054215/movieconnections |title=Psycho - Connections |publisher=IMDb |accessdate=February 18, 2016}}</ref>
* The title of the episode is a reference to the 1984 film ''[[The Brother from Another Planet]]''.<ref name=clipd>{{cite web |url=http://www.clipd.com/tv/39991/14-classic-movie-references-in-the-simpsons-that-you-totally-missed/#page=3 |title=14 Classic Movie References In "The Simpsons" That You Totally Missed |publisher=Clipd |accessdate=February 18, 2016}}</ref>

==Reception==
In its original broadcast, "Brother from the Same Planet" finished 18th in ratings for the week of February 1–7, 1993, with a [[Nielsen ratings|Nielsen rating]] of 14.9, equivalent to approximately 13.9 million viewing households. It was the highest-rated show on the Fox network that week, beating ''[[Martin (TV series)|Martin]]''.<ref>{{cite news |title='Skylark' helps CBS soar to no. 1|work=Sun-Sentinel|author=Elber, Lynn|page=3E|date=February 11, 1993}}</ref>

In their section on the episode in the book ''I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide'', Warren Martyn and Adrian Wood comment: "We love Homer sitting at home trying to remember to pick up Bart—he's watching a TV show about a baseball star called Bart, with pictures of Bart on all sides, and even Maggie seems to be calling her brother's name."<ref name="martyn">{{cite book|last=Martyn|first=Warren|author2=Adrian Wood|title=I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide|publisher=Virgin Books|date=February 10, 2000|pages=|url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season4/page15.shtml|isbn=0-7535-0495-2}}</ref>  Writing in the compilation work ''[[The Psychology of The Simpsons]]'', Robert M. Arkin and Philip J. Mazzocco reference a scene from the episode where Homer "argues with his own brain about a desired course of action" to illustrate [[Self-Discrepancy Theory|self-discrepancy theory]], the idea that "humans will go to great lengths to attain and preserve self-esteem".<ref>{{cite book|last=Brown|first=Alan|authorlink=|author2=Chris Logan|title=[[The Psychology of The Simpsons]]|publisher=Benbella Books|date=March 1, 2006|isbn=1-932100-70-9|page=127}}</ref>

The writers of the FOX program ''[[King of the Hill]]'' put "Brother from the Same Planet" among the five best episodes of ''The Simpsons'', including "[[Homer the Heretic]]", "[[Lisa's Wedding]]", "[[Lisa's Substitute]]", and "[[Behind the Laughter]]".<ref name="variety">{{cite news | last =Staff | title ='King' scribes chime in with best bets | work =[[Variety (magazine)|Variety]] | page =A8 | publisher =Reed Elsevier Inc. | date =February 13, 2003}}</ref> Mikey Cahill of the ''[[Herald Sun]]'' highlighted the quote "PickupBart? What the hell is PickupBart?" by Homer in his list of "Fab Fives" related to ''The Simpsons''.<ref>{{cite news | last =Cahill | first =Mikey | title =Fab Five | work =[[Herald Sun]] | page =I10 | date =July 26, 2007 }}</ref> When asked to pick his favorite season out of ''The Simpsons'' seasons [[The Simpsons (season 1)|one]] through [[The Simpsons (season 20)|twenty]], Paul Lane of the ''[[Niagara Gazette]]'' picked season four and highlighted "Brother from the Same Planet" and "[[Mr. Plow]]" which he called "excellent", along with "the sweetly funny" "[[Lisa's First Word]]", and "Homer the Heretic".<ref>{{cite news | last =Dzikiy | first =Phil |author2=Paul Lane | title =TELEVISION: 20 years&nbsp;— A 'Simpsons' extravaganza | work =[[Niagara Gazette]] | date =September 25, 2008 }}</ref> In a review of ''The Simpsons'' season four, Lyndsey Shinoda of ''Video Store'' cited "Brother from the Same Planet" and "[[I Love Lisa]]" among her "personal favorites" from the season.<ref>{{cite news | last =Shinoda | first =Lyndsey | title =The Simpsons: the Complete Fourth Season | work =Video Store | publisher =Advanstar Communications | date =June 13, 2004 }}</ref>

==References==
{{Reflist|2}}

==Further reading==
*{{cite book  | last =Irwin  | first =William   |author2=Mark T. Conard|author3=Aeon J. Skoble  | title =[[The Simpsons and Philosophy|The Simpsons and Philosophy: The D'oh! of Homer]]  | publisher =Open Court  | year =2001| isbn =0-8126-9433-3  | page =241 }}

==External links==
{{Portal|The Simpsons}}
{{Wikiquote|The_Simpsons/Season_4#Brother_from_the_Same_Planet|"Brother from the Same Planet"}}
*{{cite web|url=http://www.thesimpsons.com/#/recaps/season-4_episode-14|title=Brother from the Same Planet|work=The Simpsons.com|publisher=Fox.com}}
*{{Snpp capsule|9F12}}
*{{IMDb title|id=0764185}}
*{{Amg movie|284902}}
*{{tv.com episode|the-simpsons/brother-from-the-same-planet-1358}}

{{The Simpsons episodes|4}}
{{Good article}}

[[Category:The Simpsons (season 4) episodes]]
[[Category:1993 American television episodes]]
[[Category:Works about addiction]]

[[it:Episodi de I Simpson (quarta stagione)#Fratello dello stesso pianeta]]