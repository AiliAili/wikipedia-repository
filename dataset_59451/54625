{{italic title}}
{{Underlinked|date=May 2013}}

[[File:Kept In The Dark.jpg|thumb|right|200px|First edition title page.]]
'''''Kept in the Dark''''' is a novel by the 19th-century English novelist [[Anthony Trollope]]. One of his lesser and later works, it nonetheless has interest. It was published in eight monthly instalments in ''[[Good Words]]'' in 1882, and also in book form in the same year.

==Plot introduction==
The plot is a simple one - Cecilia Holt ends her engagement to Sir Francis Geraldine because of his indifference to her; she goes abroad and meets Mr George Western, who has been jilted by a beautiful girl. They marry, but she does not tell him she has been previously engaged, although he has told her his story. When Western is informed of the previous engagement by Sir Francis, he leaves his wife and goes abroad; Cecilia returns to Exeter to live with her mother. Her sister-in-law in the end effects a reconciliation. There is a comic sub-plot, as so often with Trollope, involving one of Cecilia's friends who attempts to marry Sir Francis. The novel is principally about duty and truth in marriage, and the relationship of a couple to society.

==Reaction==
On its appearance the novel received middling notices;{{citation needed|date=January 2013}} Trollope died in the year following its publication.

== External links ==
* {{gutenberg |no=22000 |title=Kept in the Dark}}
* {{librivox book | title=Kept in the Dark | author=Anthony Trollope}}

==References==
{{reflist}}

{{Anthony Trollope}}

[[Category:1882 novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Good Words]]


{{1880s-novel-stub}}