{{other people|James Turner}}
{{Infobox officeholder
|name         = James Milton Turner
|image        = File:James Milton Turner later in life.jpg
|office      = Assistant superintendent of Missouri schools
|term_start  = After Civil War
|term_end    = pre-1871 
|office2      = American ambassador to [[Liberia]]
|term_start2  = 1871
|term_end2    = 1878
|birth_name   =  
|birth_date   = 1840
|birth_place  = [[St. Louis, Missouri]]
|death_date   = Nov 1, 1915 (75 years old)<ref name= "Tulsa daily world p. 1">{{harvnb|Tulsa daily world|1915|p=1}}</ref>
|death_place  = [[Ardmore, Oklahoma]]
|party        = [[Radical Republicans]]
|nationality  = American
|spouse       = 
|children     = 
|residence =  
|alma_mater   =[[Oberlin College]]<br /> [[John Berry Meachum]]'s floating Freedom School
|religion     =  
|signature    =  
|website      =  
}}

'''James Milton Turner''' (1840 – November 1, 1915) was a [[Reconstruction Era|post Civil War]] political leader, activist, educator, and diplomat. As ambassador to [[Liberia]], he was the first African-American to serve in the U.S. diplomatic corps.

==Early life==

Turner was born into [[Slavery in the United States|slavery]] in [[St. Louis, Missouri]]. When he was a child he was sold on the steps of the St Louis courthouse for $50 (US$ {{formatnum:{{Inflation|US|50|1850|r=-2}}}} in {{CURRENTYEAR}}).<ref name= "Tulsa daily world p. 1"/>  His father, John Turner, was a "horse doctor" who was eventually able to purchase freedom for himself and his family.  At fourteen, James Turner attended [[Oberlin College]] in Ohio for one term until he had to return to St. Louis to care for his family following his father's death in 1855.  There,  Turner attended [[John Berry Meachum]]'s floating Freedom School on a steamboat on the Mississippi River, which Meachum had set up to evade the [[History of slavery in Missouri|Missouri law against education for blacks that was passed in 1854.]]

==Career==

When the [[American civil war]] broke out, Turner enlisted in the [[United States Colored Troops|Union Army]] and served as body servant for Col. Madison Miller. A wound he received during the war left him with a permanent limp. After the war, Miller's brother-in-law, Missouri Governor [[Thomas Clement Fletcher|Thomas Fletcher]], appointed him assistant superintendent of schools.  As such, he was responsible for setting up schools for black Missourians.  He helped establish the Lincoln Institute in Jefferson City, the first institution of higher education for African-Americans in Missouri.  The Institute's name was later changed to [[Lincoln University (Missouri)|Lincoln University]]. As a politician, Turner, an outspoken member of the [[Radical Republicans]] and a leader of the Missouri Equal Rights League, was held in high regard for his oratorical skills.<ref name= "Jack p. ">{{harvnb|Jack|2007|p=}}</ref> In 1868 he was installed as the principal of [[Lincoln College Preparatory Academy|Lincoln School]] the first school for blacks in Kansas City, Missouri. In this position he was succeeded by [[J. Dallas Bowser]].<ref>Coulter, Charles Edward. Take Up the Black Man's Burden: Kansas City's African American Communities, 1865-1939. University of Missouri Press, 2006. p23</ref>

In 1871, he was appointed ambassador to [[Liberia]] by President Ullyses S.Grant. He relocated to [[Monrovia]] and held that post until 1878. When he returned to St. Louis, he played an important role in helping to resettle black refugees from the ex-[[Confederacy (American Civil War)|confederate]] states in [[The Southern United States|the South]] and in organizing blacks as a political force.  He took part in the relief efforts for African-Americans who had left the South for [[Kansas]] as part of the [[Exodusters|Exoduster Movement of 1879.]], many of whom ended up in St. Louis.  In 1881, he was responsible, with Hannibal Carter,for organizing the Freedmen’s Oklahoma Immigration Association to promote black homesteading in [[Oklahoma]].  As chairman of the Negro National Republican Committee, he proposed nominating another African-American,  senator [[Blanche Bruce]] , as the vice presidential candidate on the [[Republican Party (United States)|Republican]] ticket in 1880.<ref name= "Gilder Lehrman Institute of American History p. ">{{harvnb|Gilder Lehrman Institute of American History|1880|p=}}</ref>

Turner spent the last two decades of his life devoted himself to fighting for the rights of Cherokee, Choctaw, and Chickasaw [[Cherokee freedmen|freedmen]] in the Indian Territory.<ref name= "Hine-Jenkins p. 71">{{harvnb|Hine|Jenkins|2001|p=71}}</ref> He successfully lobbied Congress for  the nearly 4,000 freed slaves of the [[Cherokee]] to receive $75,000 (US$ {{formatnum:{{Inflation|US|75000|1888|r=-2}}}} in {{CURRENTYEAR}}) from funds that the US. government paid the tribe for their land. In Ardmore, Oklahoma, while he was representing the freedmen in a legal dispute,  a nearby railroad car exploded and the debris from it cut his left hand.

Turner died from blood poisoning from the explosion in 1915 in [[Ardmore, Oklahoma]].<ref name= "Dillard p. 372-411">{{harvnb|Dillard|1934|pp=372–411}}</ref><ref name= "Turner-Dilliard p. 1-11">{{harvnb|Turner|Dilliard|1941|pp=1–11}}</ref><ref name= "Kremer p. ">{{harvnb|Kremer|1991|p=}}</ref><ref name= "Appel p. ">{{harvnb|Appel|2010|p=}}</ref>

==Bibliography==
'''Notes'''
{{reflist|20em}}
'''References''' 
* {{cite book |ref=harv|last=Appel|first=Phyllis | authorlink =  | title = The Missouri Connection: Profiles of the Famous and Infamous|year=2010| publisher = Graystone Enterprises LLC| isbn= 9780984538102 }} <small>- Total pages: 216</small>
*{{cite book |ref=harv|last=Jack|first=Bryan M. | authorlink =  | title = The St. Louis African American Community and the Exodusters|year=2007| publisher = [[University of Missouri Press]]| isbn= 9780826266163 }} <small>- Total pages: 178</small>
*{{cite journal |ref=harv|last=Dillard |first= Irving |authorlink= |coauthors= |date=October 1934|title=James Milton Turner, A Little Known Benefactor of His People. |trans_title= |journal= [[The Journal of Negro History]]|volume=  19|issue= 4 |pages=372–411|id= |jstor=i347364 |quote=|publisher= |issn=1548-1867}}
*{{cite web |ref=harv|last=Gilder Lehrman Institute of American History|authorlink=Gilder Lehrman Institute of American History|date=1880|url = https://www.gilderlehrman.org/history-by-era/reconstruction/resources/nominating-african-american-for-vice-president-1880 |title =Nominating an African-American for President|format = |publisher = [[Gilder Lehrman Institute of American History]]| accessdate = November 2, 2015 |quote=}}
*{{cite book |ref=harv|last=Hine|first=Darlene Clark |last2=Jenkins|first2=Earnestine | authorlink =  | title = A Question of Manhood: A Reader in U.S. Black Men's History and Masculinity, Volume 2|year=2001| publisher = [[Indiana University Press]]| isbn= 9780253214607 }} <small>- Total pages: 482</small>
* {{cite book |ref=harv|last=Kremer|first=Gary R. | authorlink =  | title = James Milton Turner and the Promise of America: The Public Life of a Post-Civil War Black Leader|year=1991| publisher = [[University of Missouri Press]]| isbn= 9780826207807 }} <small>- Total pages: 245</small>
*{{cite news |ref=harv|title= Sold as slave ardmore fire is fatal to him|first= |last=Tulsa daily world |authorlink=Tulsa World |author2= |url=http://chroniclingamerica.loc.gov/lccn/sn85042344/1915-11-02/ed-1/seq-1/ |format= |newspaper=[[Tulsa World|Tulsa daily world]] |publisher= |location=[[Tulsa, Oklahoma]] |isbn= |issn=2330-7234 |oclc=4450824 |bibcode= |doi= |id= |date=November 2, 1915|page= |pages= |at= |accessdate= November 2, 2015 |language= |quote= |archiveurl=http://chroniclingamerica.loc.gov/lccn/sn85042344/1915-11-02/ed-1/seq-1/ |archivedate=November 2, 2015 }}
* {{cite journal |ref=harv|last2=Dilliard|first2=Irving|last=Turner|first= J. Milton|authorlink=James Milton Turner |coauthors= |date= January 1941 |title=Dred Scott Eulogized by James Milton Turner: A Historical Observance of the Turner Centenary: 1840-1940 |trans_title= |journal= [[The Journal of Negro History]]|volume=  26|issue= 1 |pages= 1–11 |id= |jstor=2715047|quote=|publisher= |issn=1548-1867}}

==Further reading==

*{{cite book |ref=harv|last3=Stevenson|first3=Robert L.|last2=Rosser|first2=James Bernard |last=Walton|first=Hanes | authorlink = | title = Liberian Politics: The Portrait by African American Diplomat J. Milton Turner|year=2002| publisher = [[Rowman & Littlefield|Lexington Books]]| isbn= 9780739103449 }} <small>- Total pages: 417 </small>

{{DEFAULTSORT:Turner, James Milton}}
[[Category:1840 births]]
[[Category:1915 deaths]]
[[Category:Reconstruction Era]]
[[Category:African-American people in Missouri politics]]
[[Category:African-American educators]]
[[Category:African-American diplomats]]
[[Category:People of American Reconstruction]]
[[Category:People from St. Louis]]
[[Category:People of Missouri in the American Civil War]]
[[Category:Ambassadors of the United States to Liberia]]