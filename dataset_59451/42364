<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Prime
 | image=Blackshape Prime (RA-0149A).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[Italy]]
 | manufacturer=[[Blackshape srl]]
 | designer=
 | first flight=2007
 | introduced=2009
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]134,000 (assembled, 2011)
 | developed from= [[Millennium Master]]
 | variants with their own articles=
}}
|}
The '''Blackshape Prime''' is an [[Italy|Italian]] [[ultralight aircraft]], produced by [[Blackshape srl]], the company founded in [[Monopoli]] by [[Luciano Belviso]] and Angelo Petrosillo. The aircraft first flew in 2007 and was introduced at the Aero show held in [[Friedrichshafen]] in 2009. It is supplied as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 33. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 35. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
The Prime started as the [[Millennium Master]], but the design was later acquired and further developed by Blackshape. The aircraft was designed to comply with the [[Fédération Aéronautique Internationale]] microlight rules. It features a cantilever [[low-wing]], a two-seats-in-[[tandem]] enclosed cockpit under a [[bubble canopy]], retractable [[tricycle landing gear]] and a single engine in [[tractor configuration]].<ref name="WDLA11" /><ref name="WDLA15"/>

The aircraft is made from [[Carbon-fiber-reinforced polymer|pre-preg carbon fibre]]. Its {{convert|7.94|m|ft|1|abbr=on}} span wing has an area of {{convert|9.96|m2|sqft|abbr=on}} and double slotted [[Flap (aircraft)|flaps]]. The standard engine available is the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]] [[four-stroke]] powerplant, driving a two bladed [[constant speed propeller]], which gives it a maximum level speed of {{convert|300|km/h|mph|0|abbr=on}} and a cruise speed of {{convert|275|km/h|mph|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Prime) ==
{{Aircraft specs
|ref=Bayerl, Tacke and Blackshape<ref name="WDLA11" /><ref name="WDLA15"/><ref name="Specs">{{cite web|url = http://www.blackshapeaircraft.info/prime/|title = Specs|accessdate = 19 August 2016|author = Blackshape|year = }}</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=7.18
|length ft=
|length in=
|length note=
|span m=7.94
|span ft=
|span in=
|span note=
|height m=2.41
|height ft=
|height in=
|height note=
|wing area sqm=9.96
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=296.5
|empty weight lb=
|empty weight note=
|gross weight kg=472.5
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|66|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=75<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=2
|prop name=[[constant speed propeller]]
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=300
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=275
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=65
|stall speed mph=
|stall speed kts=
|stall speed note=flaps down
|never exceed speed kmh=340
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=47.44
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[Pelegrin Tarragon]], another design based upon the Millennium Master

==References==
{{reflist}}

==External links==
{{commons category|Blackshape Prime}}
*{{Official website|http://www.blackshapeaircraft.com/}}
*[https://www.youtube.com/watch?v=D5pKqE3AOzY Official video]

[[Category:Italian ultralight aircraft 2000–2009]]
[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]