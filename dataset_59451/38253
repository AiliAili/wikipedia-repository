{{good article}}
{{Use British English|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{Infobox University Boat Race
| name= 79th Boat Race
| winner = Cambridge
| women_winner = Oxford
| margin = 3 lengths
| winning_time= 20 minutes 14 seconds
| overall = 38–40
| umpire = [[Charles Burnell]]<br>(Oxford)
| date= {{Start date|1927|4|2|df=y}}
| prevseason= [[The Boat Race 1926|1926]]
| nextseason= [[The Boat Race 1928|1928]]
}}

The '''79th Boat Race''' took place on 2 April 1927.  Held annually, [[the Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford's crew was marginally heavier than their opponents, and saw five participants return with Boat Race experience, compared to Cambridge's four.  Umpired for the first time by former Oxford rower [[Charles Burnell]], Cambridge won by three lengths in a time of 20 minutes 14 seconds.  It was the first race in the history of the event to be broadcast live on [[BBC Radio]]. The victory took the overall record in the event to 40&ndash;38 in Oxford's favour.  The [[Women's Boat Race 1927|inaugural]] [[Women's Boat Race]] was contested this year, with Oxford securing the victory.

==Background==
[[File:Nickalls 5537493922 6b328c147b o.jpg|right|thumb|upright|[[Guy Nickalls]] provided commentary from the umpire's launch for the first time in the event's history.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2014, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1926|1926 race]] by five lengths, with Oxford leading overall with 40 victories to Cambridge's 37 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 25 August 2014}}</ref><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>  This year also saw [[Women's Boat Race 1927|the inaugural]] running of the [[Women's Boat Race]], between female crews from the two universities.<ref name=women>{{Cite web | url = http://www.telegraph.co.uk/sport/othersports/boat-race/11530463/Womens-Boat-Race-a-historic-day-on-theThames.html | title = Women's Boat Race: a historic day on the Thames | work = [[The Daily Telegraph]] | accessdate = 12 April 2015 | date = 12 April 2015}}</ref>

Oxford were coached by H. R. Baker (who rowed for the Dark Blues in the [[The Boat Race 1908|1908]] and [[The Boat Race 1909|1909 races]]), G. C. Bourne (who had rowed for the university in the [[The Boat Race 1882|1882]] and [[The Boat Race 1883|1883 races]]), R. C. Bourne (who had rowed four times between 1909 and 1912) and P. C. Mallam (a Dark Blue from 1921 to 1924 inclusive).  Cambridge were coached by [[William Dudley Ward]] (who had rowed in [[The Boat Race 1897|1897]], [[The Boat Race 1899|1899]] and [[The Boat Race 1900|1900 races]]), Francis Escombe and David Alexander Wauchope (who had rowed in the [[The Boat Race 1895|1895 race]]).<ref>Burnell, pp. 110&ndash;111</ref>  For the first year the umpire was [[Charles Burnell]], who had rowed for Oxford in the [[The Boat Race 1895|1895]], [[The Boat Race 1896|1896]], [[The Boat Race 1897|1897]] and [[The Boat Race 1898|1898 races]].<ref>Burnell, pp. 49, 97</ref>

It was the first time that the progress of the race was broadcast on [[BBC Radio]] from the umpire's [[Launch (boat)|launch]] ''Magician''.<ref>Dodd, p. 218</ref>  Poet [[J. C. Squire]] and Olympic gold medallist and former Oxford rower [[Guy Nickalls]] provided the commentary, with transmission equipment on the boat weighing in excess of {{convert|1000|lb|kg}}, and using a number of specially built reception points along the course.<ref name=drink147/><ref name=dodd219>Dodd, p. 219</ref>  It was the second live outdoor commentary ever broadcast, the first being the England versus Scotland match of the [[1927 Five Nations Championship]].<ref name=dodd219/>

==Crews==
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 8.625&nbsp;[[Pound (mass)|lb]] (79.9&nbsp;kg), {{convert|2.25|lb|kg|1}} per rower more than their opponents.  Cambridge's boat contained four participants with Boat Race experience, including [[Coxswain (rowing)|cox]] J. A. Brown who was steering the Light Blues for the fourth consecutive year.  Oxford saw five members of the previous year's crew return, including E. C. T. Edwards and [[Sir Douglas Thomson, 2nd Baronet|James Douglas Wishart Thomson]], both of whom were rowing for the third time in the event.<ref name=burn72/>  Cambridge's Australian number six J. B. Bell and his opposite number, American Howard T. Kingsbury of [[Yale University]],<ref>{{Cite web | url = https://www.newspapers.com/image/83380870/ | publisher = Brooklyn Public Library | work = Brooklyn Life and Activities of Long Island Society | date = 9 April 1927 | title = At School and College | page = 16}}</ref>  were the only non-British participants registered in the race.<ref>Burnell, p. 39</ref>

According to author and former Oxford rower George Drinkwater, neither crew could "be classed in a very high standard", claiming Cambridge's selection was poor and Oxford's coaches indecisive.<ref name=drink147>Drinkwater, p. 147</ref> Ten days before the race, Oxford's W. S. Llewellyn was struck down by [[Rubella|German measles]] and was replaced by A. M. Hankin who was placed at [[Stroke (rowing)#Stroke seat|stroke]].<ref name=drink147/>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || N. E. Whiting ||  [[Worcester College, Oxford|Worcester]] || 11 st 9 lb || [[John Maclay, 1st Viscount Muirshiel|Hon. J. S. Maclay]] || [[Trinity College, Cambridge|1st Trinity]] || 11 st 9 lb
|-
| 2 || P. Johnson ||[[Magdalen College, Oxford|Magdalen]] || 11 st 11 lb || T. E. Letchworth || [[Pembroke College, Cambridge|Pembroke]] || 12 st 7 lb
|-
| 3 ||E. C. T. Edwards ||  [[Christ Church, Oxford|Christ Church]] || 12 st 10 lb || J. C. Holcroft || [[Pembroke College, Cambridge|Pembroke]] || 12 st 5 lb
|-
| 4 || [[Sir Douglas Thomson, 2nd Baronet|J. D. W. Thomson]] (P)|| [[University College, Oxford|University]] || 13 st 6 lb || [[Richard Beesly|R. Beesly]] || [[Trinity College, Cambridge|1st Trinity]] || 12 st 11.5 lb
|-
| 5 || W. Rathbone || [[Christ Church, Oxford|Christ Church]] || 13 st 13 lb || L. V. Bevan ||  [[Lady Margaret Boat Club]] || 13 st 3.5 lb
|-
| 6 || H. T. Kingsbury ||  [[The Queen's College, Oxford|Queen's]] || 14 st 2 lb || J. B. Bell || [[Jesus College, Cambridge|Jesus]] || 13 st 3 lb
|-
| 7 || T. W. Shaw || [[Christ Church, Oxford|Christ Church]] || 12 st 7 lb || S. K. Tubbs (P) || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 12 st 3 lb
|-
| [[Stroke (rowing)|Stroke]] || A. M. Hankin  ||[[Worcester College, Oxford|Worcester]] || 10 st 11 lb || R. J. Elles || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 9 lb
|-
| [[Coxswain (rowing)|Cox]] || [[Croft baronets|Sir J. H. Croft]] || [[Brasenose College, Oxford|Brasenose]] || 8 st 12 lb || J. A. Brown || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 8 st 10.5 lb
|-
!colspan="7"|Source:<ref name=burn72>Burnell, p. 72</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50, 52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=burn72/>  In a strong wind and "big spring tide", Burnell started the race at 1:30&nbsp;p.m.  After a level start, Oxford held a [[Canvas (rowing)|canvas-length]] lead by the time the crews passed Craven Steps, but the Light Blues levelled the race using the advantage of the bend in the river at [[Craven Cottage]].<ref name=drink147148>Drinkwater, pp. 147&ndash;148</ref>  The crews passed the Mile Post level whereupon Oxford retook the lead, slightly [[stroke rate|out-rating]] their opponents and passed below [[Hammersmith Bridge]] with a half-length advantage.  Nearly clear by The Doves pub, the Dark Blues ran into strong wind and rough water and Cambridge started to reduce their lead.<ref name=drink148/>

A lead of one-third of a length at Chiswick Steps was soon overhauled by the Light Blues who were almost clear at [[Barnes Railway Bridge|Barnes Bridge]].  They rowed on to win "comfortably" by three lengths in a time of 20 minutes 14 seconds.<ref name=drink148>Drinkwater, p. 148</ref>  It was Cambridge's fourth consecutive victory and their eighth win in nine races, and took the overall record in the event to 40&ndash;38 in Oxford's favour.<ref name=results/>  Following the race, the tide was so high that spectators were forced to wade through water that was knee-deep.<ref name=drink148/>

Oxford won the inaugural [[Women's Boat Race 1927|women's race]] by 15 seconds despite not rowing together; the crews were not permitted to compete side-by-side, that style of competition being considered "unladylike".<ref name=women/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-95-006387-4 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1927}}
[[Category:1927 in English sport]]
[[Category:The Boat Race]]
[[Category:April 1927 sports events]]
[[Category:1927 in rowing]]