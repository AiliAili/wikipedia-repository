{{italic title}}
{{about||the Greek term|phronesis|the theological journal|Asian Theological Seminary}}
'''''Phronesis''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering the study of [[ancient philosophy]]. It is indexed by [[PhilPapers]] and the [[Philosopher's Index]].<ref>[http://philpapers.org/journals PhilPapers Publications List]</ref><ref>[http://philindex.org/downloads/PIC_Alphabetical_Coverage.pdf Philosopher's Index Publications List]</ref> The journal was established in 1955 by Donald James Allan and Joseph Bright Skemp, who wrote in the first issue that the goal of the journal was to bring together philosophers and [[classics|classicists]] from across national borders so as to improve the specialty of ancient philosophy, but also to include insights for those in [[medieval studies]].<ref>Allan and Skemp, "Editorial", ''Phronesis'', vol. 1, no. 1 (1955), p. 1.</ref> ''Phronesis'' has been described as "pioneering"<ref>Vanhaelen, M., "Philosophy" in Gagarin, M. (ed.), ''The Oxford Encyclopedia of Ancient Greece and Rome'' ([[Oxford University Press]], 2010), p. 2053.</ref> and one of the major English-language journals for ancient philosophy.<ref>Preus, A., ''Historical Dictionary of Ancient Greek Philosophy'' ([[Scarecrow Press]], 2007), p. 307.</ref> The journal is published by [[Brill Publishers]] and the [[editors-in-chief]] are George Boys-Stones ([[Durham University]]) and Ursula Coope ([[Oxford University]]).<ref>[http://www.brill.com/phronesis#EDIBOA_1 Phronesis | Brill]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.brill.com/phronesis}}
* [http://philpapers.org/pub/1141 PhilPapers listing for ''Phronesis'']

[[Category:Ancient philosophy journals]]
[[Category:Publications established in 1955]]
[[Category:Brill Publishers academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]