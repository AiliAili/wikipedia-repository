{{DISPLAYTITLE:''All American'' (aircraft)}}
{{about|the World War II bomber aircraft|other uses of the term|All American (disambiguation)}}
{|{{Infobox Aircraft Begin
  |name = ''All American''
  |image = File:19430201AllAmericanB17inFlight.jpg
  |caption = The ''All American'' returning to base after its collision with an enemy fighter
}}
{{Infobox Aircraft Career
  |sole example of type?= N
  |type                 = [[Boeing B-17 Flying Fortress|Boeing B-17F-5-BO Flying Fortress]]<ref name=MuseumUSAFsideview/>
  |other names          = <!--Other names (nicknames, nose art names) this aircraft is known by-->
  |manufacturer         = [[Boeing]]
  |construction number  = <!-- manufacturer's construction number -->
  |construction date    = þ
  |civil registration   = 
  |military serial      = 41-24406<ref name=MuseumUSAFsideview/>
  |radio code           = 
  |first flight         = <!-- date of first flight -->
  |owners               = 
  |in service           = 
  |flights              = <!-- number of flights made by this aircraft, usually only relevant for an aircraft no longer flying -->
  |total hours          = <!-- total number of hours flown by this aircraft, usually only relevant for an aircraft no longer flying -->
  |total distance       = <!-- total distance flown by this aircraft, usually only relevant for an aircraft no longer flying -->
  |fate                 = Dismantled for salvage at Lucera Airfield, Italy, on March 6, 1945.<ref name=ThisDayInAviation19430201/>
  |preservation         = 
}}
|}

The '''''All American''''' (proper name '''''All American III'''''<ref name=MuseumUSAFsideview/>) was a World War II [[Boeing B-17 Flying Fortress|Boeing  B-17F Flying Fortress]] bomber aircraft that was able to return safely to its base after having its [[empennage|rear fuselage]] nearly cut off by an in-flight collision with a German fighter over enemy-held territory. The bomber's flight is said to have yielded one of the most famous photographs of World War II, and has been linked with the phrase "Comin' in on a Wing and a Prayer." It inspired the 414th Bombardment Squadron's emblem, an image of a puppy praying atop an aircraft's tail section.

==The aircraft==
The ''All American'' was a B-17F-5-BO, serial number 41-24406, in the 97th Bomb Group, [[414th Expeditionary Reconnaissance Squadron|414th Bombardment Squadron]].<ref name=MuseumUSAFsideview/>

==The mission==
On February 1, 1943, bombers of the [[414th Expeditionary Reconnaissance Squadron|414th Bombardment Squadron]] departed their base near [[Biskra]], Algeria, to attack the German-controlled seaports, [[Bizerte]] and [[Tunis]], Tunisia.<ref name=USAFcharleston20150203/>  After dropping their bombloads and returning toward base, the bombers were attacked by German fighters,<ref name=USAFcharleston20150203/> believed to be [[Messerschmitt Bf 109]]s.<ref name=AvHistMuseum20130828/> Two fighters attacked respective bombers, the lead B-17 and the ''All American'' which was flying next to it in formation.<ref name=USAFcharleston20150203/> The bombers' machine gun fire downed the first fighter, but the second pressed its head-on attack against the ''All American''.<ref name=USAFcharleston20150203/> Apparently struck by machine gun rounds, the second fighter could not complete its roll to pull down and away from the ''All American'', the fighter pilot apparently having been killed or disabled.<ref name=USAFcharleston20150203/><ref name=Waterland20120921/>  The German pilot was reported as being 16-victory ace Erich Paczia of [[Jagdgeschwader 1 (World War II)|I]]/[[Jagdgeschwader 53]].<ref name=WingsOfFame1997/>

[[File:Damaged tail of B-17.jpg | thumb | right | The aircraft returned safely to base despite the extensive damage to its rear fuselage.]]
[[File:414th Bombardment Squadron - Emblem.png | thumb | right | Emblem of the [[414th Expeditionary Reconnaissance Squadron|414th Bombardment Squadron]] in World War II ]]
Out of control, the second fighter's wing collided with the top rear fuselage of the ''All American'', almost cleaving the bomber's tail section off, leaving a large diagonal gash from the base of the ''All American's'' [[vertical stabilizer|tail]] and severing the left [[horizontal stabilizer]] completely off the plane.<ref name=USAFcharleston20150203/> Metal in the airframe near the right tailplane was the only thing keeping the tail section, housing the rear gunner, attached to the aircraft.<ref name=USAFcharleston20150203/><ref name=EAA2015/> The fighter broke apart but left some pieces in the bomber's fuselage.<ref name=EAA2015/>

The bomber squadron maintained formation to protect the ''All American'' until they were beyond range of enemy fighters, with the crew donning parachutes in expectation of having to bail out.<ref name=USAFcharleston20150203/><ref name=AvHistMuseum20130828/><ref name=Waterland20120921/>  However, the aircraft was piloted to a safe landing at its base, and despite damage to the aircraft none of the crew was injured on that mission.<ref name=USAFcharleston20150203/>

The ''All American'' was repaired and returned to service as a [[Station hack|hack]]<ref name=AvHistMuseum20130828/> with the 352nd Bombardment Squadron (Heavy), 301st Bombardment Group (Heavy), and flew until its March 1945 dismantlement.<ref name=ThisDayInAviation19430201/>

The ''All American'' is reputed to be the source of the phrase, "Comin' in on a Wing and a Prayer,"<ref name=USAFcharleston20150203/><ref name=AvHistMuseum20130828/><ref name=Waterland20120921/> and inspired the 414th Bombardment Squadron's emblem.<ref name=USAFcharleston20150203/> An image of a puppy praying atop the rear fuselage formed the unit badge.<ref name=AvHistMuseum20130828/><ref name=Waterland20120921/> The aircraft was the subject of what was called one of the most famous photographs of World War II.<ref name=Waterland20120921/><ref name=EAA2015/>

==Mythology==
Several false myths accrued in the lore of the ''All American,''<ref name=WarbirdsNews20130627/> some of which were refuted in a 2012 interview of her bombardier Ralph Burbridge.<ref name=Waterland20120921/> Burbridge explained that the aircraft returned to her base in North Africa, and could not have made a long trip back to England as widely recounted.<ref name=Waterland20120921/> The base near Biskra, Algeria, was a more reasonable 300 miles from the bombing target.<ref name=WarbirdsNews20130627/>

Burbridge also said that the collision occurred when the bomber group was returning to base ''after'' having dropped its bombs on target, so that the aircraft did not complete a bombing run after being damaged as had been incorrectly recounted.<ref name=Waterland20120921/> Burbridge's account confirms that the ten crew members donned their parachutes, contradicting stories that the crew sacrificed some of their parachutes to hold the plane together or for an in-flight rescue of crew members from the isolated tail section.<ref name=Waterland20120921/>

The Harold Adamson and Jimmy McHugh 1943 song “[[Comin' in on a Wing and a Prayer]]” was not written about ''All American'' as sometimes reported, but was about another 97th Bomb Group B-17, ''[[Thunderbird (aircraft)|Thunderbird.]]''<ref name=WarbirdsNews20130627/>

==Crew members==
Navigator Harry C. Nuessle listed the ''All American's'' crew on its February 1, 1943 flight:<ref name=AvHistMuseum20130828/><ref name=EAA2015/>
* Pilot - Ken Bragg Jr. 
* Copilot - G. Boyd Jr. 
* Navigator - Harry C. Nuessle 
* Bombardier - Ralph Burbridge 
* Engineer - Joe C. James 
* Radio Operator - Paul A. Galloway 
* Ball Turret Gunner - Elton Conda 
* Waist Gunner - Michael Zuk 
* Tail Gunner - Sam T. Sarpolus 
* Ground Crew Chief - Hank Hyland 

==References==
{{Reflist|2|refs=
<ref name=USAFcharleston20150203>{{cite web |last1=Axberg |first1=Jason, 628th Air Base Wing historian |title=A new perspective on a challenging day at work |url=http://www.charleston.af.mil/news/story.asp?id=123438088 |publisher=U.S. Air Force, Joint Base Charleston |archiveurl=https://web.archive.org/web/20160807134932/http://www.charleston.af.mil/news/story.asp?id=123438088 |archivedate=August 7, 2016 |date=February 3, 2015 |deadurl=no }}</ref>

<ref name=EAA2015>{{cite web |title=A Flying Fortress Miracle |url=http://www.eaa.org/en/eaa/aviation-communities-and-interests/eaa-warbirds-of-america-warbird-and-ex-military-aircraft/articles/2014-09-a-flying-fortress-miracle |publisher=Experimental Aircraft Association (EAA) |archiveurl=https://web.archive.org/web/20150108091121/http://www.eaa.org/en/eaa/aviation-communities-and-interests/eaa-warbirds-of-america-warbird-and-ex-military-aircraft/articles/2014-09-a-flying-fortress-miracle |archivedate=January 8, 2015 |date=2015 |deadurl=no }}</ref>

<ref name=AvHistMuseum20130828>{{cite web |title=A Wing and a Prayer |url=http://www.aviation-history.com/boeing/b17tail.html |publisher=The Aviation History Online Museum |archiveurl=https://web.archive.org/web/20160705162833/http://www.aviation-history.com/boeing/b17tail.html |archivedate=July 5, 2016 |date=August 28, 2013 |deadurl=no }}</ref>

<ref name=Waterland20120921>Interview with bombardier Ralph Burbridge published by {{cite web |last1=Nichols |first1=Ralph |title=Local B-17 Bombardier Recalls ‘Wing and a Prayer’ Mission on the ''All American'' |url=http://waterlandblog.com/2012/09/21/local-b-17-bombardier-recalls-wing-and-a-prayer-mission-on-the-all-american/ |publisher=The Waterland Blog <!--considered a Google News source despite its name--> |archiveurl=https://web.archive.org/web/20160617034648/http://waterlandblog.com/2012/09/21/local-b-17-bombardier-recalls-wing-and-a-prayer-mission-on-the-all-american/ |archivedate=June 17, 2016 |date=September 21, 2012 |deadurl=no }}</ref>

<ref name=WarbirdsNews20130627>{{cite web |title=WWII’s B-17 ''All American:'' Separating Fact and Fiction |url=http://www.warbirdsnews.com/warbird-articles/wwiis-b-17-all-american-separating-fact-fiction.html |publisher=Warbirds News |archiveurl=https://web.archive.org/web/20160405014119/http://www.warbirdsnews.com/warbird-articles/wwiis-b-17-all-american-separating-fact-fiction.html |archivedate=April 5, 2016 |date=June 27, 2013 |deadurl=no }}</ref>

<ref name=WingsOfFame1997>Aviation History Museum (2013-08-28) cites {{cite book |last1=Hess |first1=William N. |title=Wings of Fame, Boeing B-17 Flying Fortress |date=1997 |publisher=Aerospace Publishing Ltd. |location=London |page=63 }}</ref>

<ref name=MuseumUSAFsideview>{{cite web |title=Boeing B-17F |url=http://www.nationalmuseum.af.mil/Upcoming/Photos.aspx?igphoto=2000571925 |publisher=National Museum of the U.S. Air Force |accessdate=August 8, 2016 |archiveurl=https://web.archive.org/web/20160808011525/http://www.nationalmuseum.af.mil/Upcoming/Photos.aspx?igphoto=2000571925 |archivedate=August 8, 2016 |deadurl=no }}</ref>

<ref name=ThisDayInAviation19430201>{{cite web |title=1 February 1943 |url=http://www.thisdayinaviation.com/1-february-1943/ |publisher=This Day in Aviation |archiveurl=https://web.archive.org/web/20150203072829/http://www.thisdayinaviation.com/1-february-1943/ |archivedate=February 3, 2015 |deadurl=no}}</ref>

}}

[[Category:Individual aircraft of World War II]]
[[Category:Boeing B-17 Flying Fortress]]