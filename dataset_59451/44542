{{for|the village of Redburn|Redburn, Northumberland}}
{{Infobox book
| name = Redburn
| image = Redburn His First Voyage.jpg
| caption = First edition title page
| alt = 
| author = [[Herman Melville]]
| illustrator = 
| cover_artist = 
| country = United States, England
| language = English
| subject = 
| genre = [[Travel literature]]
| published = {{Plainlist|
* 1849 {{small|(New York: Harper & Brothers)}}
* 1849 {{small|(London: Richard Bentley)}}
}}
| media_type = Print
| awards = 
| isbn = 
| oclc = 
| dewey = 
| congress = 
| preceded_by = [[Mardi]]
| followed_by = [[White Jacket]]
}}

'''''Redburn: His First Voyage'''''<ref>The full title is ''Redburn: His First Voyage: Being the Sailor-boy Confessions and Reminiscences of the Son-of-a-Gentleman, in the Merchant Service.'' See the Library of America edition edited by George Thomas Tanselle. ISBN 0-940450-09-7</ref> is the fourth book by the American writer [[Herman Melville]], first published in London in 1849. The book is semi-autobiographical and recounts the adventures of a refined youth among coarse and brutal sailors and the seedier areas of [[Liverpool]]. Melville wrote ''Redburn'' in less than ten weeks. While one scholar describes it as "arguably his funniest work,"<ref>Blum (2011), 159</ref> scholar [[F.O. Matthiessen]] calls it "the most moving of its author's books before ''Moby-Dick''".<ref>Matthiessen (1941), 396</ref>

==Plot==
Unable to find employment at home, young Wellingborough Redburn signs on the ''Highlander'', a [[Cargo ship|merchantman]] out of [[New York City]] bound for [[Liverpool]], England. Representing himself as the "son of a gentleman" and expecting to be treated as such, he discovers that he is just a green hand, a "boy", the lowest rank on the ship, assigned all the duties no other sailor wants, like cleaning out the "pig-pen", a longboat that serves as a shipboard sty. The first mate promptly nicknames him "Buttons" for the shiny ones on his impractical jacket. Redburn quickly grasps the workings of social relations aboard ship. As a common seaman he can have no contact with those "behind the mast" where the officers command the ship. Before the mast, where the common seaman work and live, a bully named Jackson, the best seaman aboard, rules through fear with an iron fist. Uneducated yet cunning, with broken nose and squinting eye, he is described as "a [[Cain]] afloat, branded on his yellow brow with some inscrutable curse and going about corrupting and searing every heart that beat near him." Redburn soon experiences all the trials of a greenhorn: seasickness, scrubbing decks, climbing masts in the dead of night to unfurl sails, cramped quarters, and bad food. 
[[File:Lancelots Hey.jpg|thumb|Launcelott's Hey, 1843]]
When the ship lands in Liverpool he is given liberty ashore. He rents a room and walks the city every day. One day in a street called Launcelott's Hey he hears "a feeble wail" from a cellar beneath an old warehouse and looking into it sees "the figure of what had been a woman. Her blue arms folded to her livid bosom two shrunken things like children, that leaned toward her, one on each side. At first I knew not whether they were alive or dead. They made no sign; they did not move or stir; but from the vault came that soul-sickening wail." He runs for help but is met with indifference by a ragpicker, a porter, his landlady, even by a policeman who tells him to mind his own business. He returns with some bread and cheese and drops them into the vault to the mother and children, but they are too weak to lift it to their mouths. The mother whispers "water" so he runs and fills his tarpaulin hat at an open hydrant. The girls drink and revive enough to nibble some cheese. He clasps the mother's arms and pulls them aside to see "a meager babe, the lower part of its body thrust into an old bonnet. Its face was dazzlingly white, even in its squalor; but the closed eyes looked like balls of indigo. It must have been dead for some hours." Judging them beyond the point at which medicine could help, he returns to his room. A few days later he revisits the street and finds the vault empty: "In place of the woman and children, a heap of quick-lime was glistening."

On the docks he meets Harry Bolton, a dandy who claims to be a sailor looking for a job, and Redburn helps him procure a berth on the ''Highlander'' for the return voyage. They become fast friends and make a trip to London where they visit a luxurious private club, Aladdin's Palace, with an exotic environment Redburn struggles to make sense of, concluding it must be  a gambling house. The ship soon departs for New York and Bolton's deficits as sailor become apparent. Redburn suspects that Bolton has never been to sea before and Bolton is tormented by the crew. Jackson, after being ill in bed for four weeks, returns to active duty: he climbs to the topsail yard, then suddenly vomits "a torrent of blood from his lungs", and falls headfirst into the sea and disappears. The crew never speak his name again. Reaching port, Redburn heads for his home and Bolton signs on a whaler. Redburn later hears that Bolton, far out in the Pacific, fell over the side and drowned.

== Character List ==
* Wellingborough Redburn (a.k.a., Buttons)
* Redburn's elder brother (unnamed in the book)
* Mr. Jones
* Captain Riga
* '''The Highlander Crew'''
** The suicidal sailor
** Jackson
** Max the Dutchman
** The Greenlander
** Mr Thompson, the cook, aka, the Doctor
** Lavender
** Jack Blunt
** Larry
** Gun-Deck
* '''The Liverpool Docks'''
** Danby
** Mary, Danby's wife
** Bob Still, Danby's old crony
** Townspeople, other foreign sailors, policemen, the poor, the beggars, the depraved
* Harry Bolton
* Miguel Saveda
* Carlo
* The O'Briens and the O'Regans
* Goodwell

==Composition and publication history==
Melville alluded to ''Redburn'' for the first time in a letter to his English publisher in the late spring of 1849, in which he wrote that the novel would be practical rather than follow the "unwise" course of his previous novel, ''Mardi'', which had been harshly criticized:<ref>Letter to Richard Bentley, June 5, 1849</ref>
{{quote|I have now in preparation a thing of a widely different cast from "Mardi":—a plain, straightforward, amusing narrative of personal experience—the son of a gentleman on his first voyage to sea as a sailor—no metaphysics, no conic-sections, nothing but cakes & ale. I have shifted my ground from the South Seas to a different quarter of the globe—nearer home—and what I write I have almost wholly picked up by my own observations under comical circumstances.|}} 
Melville adopted this more commercial approach to writing as his family obligations increased and his working conditions became more difficult. Living with him in the small house in New York City were his wife, child, mother, sisters, and his brother Allen with ''his'' wife and child. Melville later portrayed himself at this time as being forced to write "with [[wikt:dun|duns]] all around him, & looking over the back of his chair—& perching on his pen & diving in his inkstand—like the devils about [[Anthony the great|St. Anthony]]."<ref name="NN edition">{{cite book|editor-first=Hershel |editor-last=Parker |title=Redburn|year=1969|origyear=First published 1849|publisher=Northwestern-Newberry |location=Chicago|chapter=Historical Note|isbn=0-8101-0016-9|pages=318–319}}</ref>

The book is a fictional narrative based loosely on Melville's own first voyage to Liverpool in 1839. The manuscript was completed in less than ten weeks and, without any attempt at polishing it, Melville submitted it to his American publisher [[Harper & Bros]] who published it in November 1849. Melville checked the proof sheets, which came out in August, and sent them along to Bentley for publication in England, where it appeared six weeks before the American version. In 1922 it was published as a volume of the Constable edition of Melville's complete works. Since then it has been continuously in print in inexpensive hard cover editions and since 1957 in paperback.<ref>Parker, 345</ref>

==Reception==
Melville referred to ''Redburn'' and his next book ''[[White-Jacket]]'' as "two ''jobs'' which I have done for money—being forced to it as other men are to sawing wood".<ref name=Delbanco111>[[Andrew Delbanco|Delbanco, Andrew]]: ''Melville, His World and Work''. New York: Alfred A. Knopf, 2005: 111. ISBN 0-375-40314-0</ref> It was reviewed favorably in all the influential publications, American and British, with many critics hailing it as Melville's return to his original style. The critics were divided along national lines when reviewing the scene in Launcelots Hey, the British dubbing it "improbable", the Americans "powerful". In 1884 [[William Clark Russell]], the most popular writer of [[nautical fiction|sea stories]] in his generation, praised the book's force and accuracy in print. He also sent Melville a personal letter where, among other items, he said "I have been reading your ''Redburn'' for the third or fourth time and have closed it more deeply impressed with the descriptive power that vitalises every page."<ref>Parker, 344</ref> [[John Masefield]] would later single the book out as his favorite of Melville's works. When ''Redburn'' was praised, Melville wrote in his journal, "I, the author, know [it] to be trash, & wrote it to buy some tobacco with".<ref name=Delbanco111/> He later complained: "What I feel most moved to write, that is banned—it will not pay. Yet, altogether, write the ''other'' way I cannot. So the product is a final hash, and all my books are botches."<ref>Parker, 323</ref>

==Assessments==
[[Elizabeth Hardwick (writer)|Elizabeth Hardwick]] finds that passages of the book display a rhetorical brilliance: "Throughout Melville's writings there is a liberality of mind, a freedom from vulgar superstition, occasions again and again for an oratorical insertion of enlightened opinion."<ref name=Hardwick>{{cite book|last=Hardwick |first=Elizabeth |title=Herman Melville|year=2000|publisher=Viking|location=New York|isbn=0-670-89158-4|pages=27}}</ref> She points to the passage in chapter 33 where Melville describes the German immigrants preparing for their voyage to America:
{{quote|There is something in the contemplation of the mode in which America has been settled that, in a noble breast, should forever extinguish prejudices or national dislikes.... You cannot spill a drop of American blood without spilling the blood of the whole world.... Our blood is as the flood of the Amazon, made up of a thousand currents all pouring into one. We are not a nation, so much as a world.}}

Interpretations of ''Redburn'' generally fall into two schools. The first, usually called the biographical school, may be found in studies of Melville written in the 1920s by critics such as [[Raymond Weaver]], John Freeman, and [[Lewis Mumford]]. Typifying this school's approach is Mumford's statement that:<ref>{{cite book|last=Mumford|first=Lewis|title=Herman Melville: A Study of His Life and Vision|year=1929|location=New York|page=71}}</ref>
{{quote|In ''Redburn'', Melville went back to his youth and traced his feelings about life and his experience up to his eighteenth year. The book is autobiography, with only the faintest disguises: Bleecker Street becomes Greenwich Street, and the other changes are of a similar order.}} 
By the 1950s, a second school arose which might be called the "mythic" school. [[Newton Arvin]] wrote:<ref>{{cite book|last=Arvin|first=Newton|title=Herman Melville|year=1950|publisher=William Sloane Associates, Inc.|location=New York|isbn=0802138713|url=https://books.google.com/books/about/Herman_Melville.html?id=NfhRnGSnFtIC|page=104}}</ref>
{{quote|The outward subject of the book is a young boy's first voyage as a sailor before the mast; its inward subject is the initiation of innocence into evil—the opening of the guileless spirit to the discovery of "the wrong," as [[Henry James|James]] would say, "to the knowledge of it, to the crude experience of it." The subject is a permanent one for literature, of course, but it has also a peculiarly American dimension.}}
This new approach represented a broad trend that sought to reinterpret American literature—Irving, Cooper, Hawthorne, Twain, James, Faulkner, and Hemingway—in the light of mythic quests and patterns.<ref>{{cite journal|last=Schroeter|first=James|title=Redburn and the Failure of Mythic Criticism|journal=American Literature|date=November 1967|volume=39|issue=3|pages=279–297|jstor=2923295|doi=10.2307/2923295}}</ref> In one view, ''Redburn'' is "a stronger indictment of an American embarking upon a voyage of international experience than Henry James would produce and a more ironic and embittered portrait of the young protagonist's incapacity for art than James Joyce would produce."<ref>{{cite book|editor-last1=Marovitz|editor-first1=Sanford E.|editor-last2=Christodoulou |editor-first2=A.C. |title=Melville "Among the Nations"|date=2001|publisher=Kent State University Press|page=50|url=https://books.google.com/books?id=rpUY_8OjqHUC&pg=PA50&|accessdate=May 18, 2015 |first1=Marvin |last1=Fisher| chapter= The American Character, the American Imagination, and the Test of International Travel in ''Redburn''}}</ref>

Critics have clarified or called attention to particular points.  After some debate in the 1980s when the notion was first proposed, critics seem to agree that Aladdin's Palace is a "male brothel", including Elizabeth Hardwick who describes it as "a strange, fastidiously observed, rococo urban landscape unlike any other dramatic intrusion in Melville's writings or in American literature at the time."<ref>{{cite news|last1=Hardwick|first1=Elizabeth|title=Melville in Love|url=http://www.nybooks.com/articles/archives/2000/jun/15/melville-in-love/|accessdate=May 17, 2015|work=The New York Review of Books|date=June 15, 2000}}</ref><ref>{{cite book|last1=Martin|first1=Robert K.|title=Hero, Captain, Stranger: Male Friendship, Social Critique and Literary Form in the Sea Novels of Herman Melville|date=1986|publisher=University of North Carolina Press|pages=49–51}}</ref>

In addition to the social criticism of the Launcelott's Hey incident (Ch. 37), Melville attacks the evils of alcohol and the exploitation of emigrants by shipping services. The chapter "A Living Corpse", like [[Charles Dickens]]' ''[[Bleak House]]'', contains an example of [[spontaneous human combustion]] in literature (Ch. 48).

==References==
{{Reflist}}

== Sources ==
* Blum, Hester. (2011). "Melville and the novel of the sea." ''The Cambridge History of the American Novel''. Eds. Leonard Cassuto, Clare Virginia Eby, and Benjamin Reiss. Cambridge, UK: Cambridge University Press. ISBN 9780521899079
* [[F.O. Matthiessen|Matthiessen, F.O.]] (1941). ''American Renaissance: Art and Expression in the Age of Emerson and Whitman''. London, New York, Toronto: Oxford University Press.

==Further reading==
*{{cite journal|last=Bowen|first=Merlin|title=''Redburn'' and the Angle of Vision|journal=Modern Philology|date=November 1954|volume=52|issue=2|jstor=434718}}
*{{cite book|last=Gilman|first=William Henry|title=Melville's Early Life and ''Redburn''|year=1951|publisher=New York University Press|location=New York|url=https://books.google.com/books/about/Melville_s_early_life_and_Redburn.html?id=s4odAAAAIAAJ}}
*{{cite journal|last=Kosok|first=Heinz|title=A Sadder and a Wiser Boy: Herman Melville's ''Redburn'' as a Novel of Initiation|journal=Jahrbuch für Amerikastudien|year=1965|volume=10|jstor=41155394}}
*{{cite journal|last=Miller|first=James|title=Redburn and White Jacket: Initiation and Baptism|journal=Nineteenth-Century Fiction|date=March 1959|volume=13|issue=4|jstor=3044311}}

==External links==
{{wikisource}}
* Entry for ''Redburn'' @ [http://www.melville.org/hmredbrn.htm Melville.org]
* {{gutenberg|no=8118|name=Redburn}}
* {{librivox book | title=Redburn | author=Herman Melville}}

{{Herman Melville}}

[[Category:1849 novels]]
[[Category:Novels by Herman Melville]]
[[Category:19th-century American novels]]
[[Category:Novels set in Liverpool]]