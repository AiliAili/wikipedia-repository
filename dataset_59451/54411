{{Use Australian English|date=September 2014}}
{{Use dmy dates|date=March 2013}}
[[File:MATS recommendations small2.jpg|400px|right|thumb|The Conclusions and Recommendations of MATS]]

The '''Metropolitan Adelaide Transport Study''', or "'''MATS Plan'''" as it became known, was a comprehensive transport plan released in 1968 proposing a number of road and rail [[transport]] projects for the city of [[Adelaide]], [[South Australia]].

It recommended the construction of 98 kilometres of [[freeways]], 34 kilometres of [[Limited-access road|expressway]] and the widening of 386 kilometres of existing [[Arterial road|arterial]] roads. It also featured new arterial roads and a new bridge over the [[Port River]]. For public transport, it proposed the closure of the [[Glenelg Tram]], 20 rail [[grade separation]]s and 14 kilometres of new train line, including a [[rapid transit|subway]] under [[King William Street, Adelaide|King William Street]].<ref name="NorleyJournal">{{cite journal|last1=Norley|first1=Kim|title=Urban rail infrastructure – the path  from comprehensive  transport plans to  the recent experience|journal=Australasian Transport Research Forum|date=September 2011|url=http://atrf.info/papers/2011/2011_Norley_A.pdf|accessdate=30 October 2016}}</ref>

The estimated cost of land acquisition and construction was $436.5 million in 1968, which equates to approximately $4,580 million in 2010 with [[inflation]].<ref>[http://www.rba.gov.au/calculator/calc.go Inflation calculator] [[Reserve Bank of Australia]]</ref> Very few of the plan's recommendations were ultimately brought to fruition in their original form due to political and public opposition.<ref name="NorleyJournal" />

==History==
Like other states of [[Australia]], there was a strong movement towards private car travel following the [[Second World War]] in South Australia. [[Rationing|Fuel rationing]] was a thing of the past and private car ownership was increasing.<ref>Radcliff p. 126</ref> The car was seen as a personal liberator following the constraints of the wartime periods, with many cities around the world building their urban forms around the needs of private cars.<ref>{{cite journal|last1=McIntosh|first1=James|last2=Trubka|first2=Roman|last3=Kenworthy|first3=Jeff|last4=Newman|first4=Newman|title=The role of urban form and transit in city car dependence: Analysis of 26 global cities from 1960 to 2000|journal=Transportation Research Part D|date=2014|volume=33|pages=95–110|accessdate=30 October 2016}}</ref>

Adelaide continued to expand rapidly due to people continuing to choose to live in suburbs as well as population growth and by 1966, Adelaide's population had increased by 90% on post war levels. Experts had been warning of the consequences of unplanned urban sprawl leading to a renewed interest in [[Urban planning|planning]]. In 1955 the Town Planning Act was amended to make a requirement for a coordinated plan to guide the future development of Adelaide.<ref name="1962MetroAdelaideReport">{{cite report|title=Report on the Metropolitan Area of Adelaide|date=1962|publisher=Government of South Australia|location=Adelaide|url=http://www.thinkdesigndeliver.sa.gov.au/__data/assets/pdf_file/0006/118797/Report_on_the_Metropolitan_Area_of_Adelaide_-_1962.pdf}}</ref>

The Report of Metropolitan Adelaide was released in 1962 and featured proposals for the construction of freeways.<ref name="1962MetroAdelaideReport" /> In 1964 the state [[Liberal Party of Australia|Liberal]] Premier [[Sir Thomas Playford|Thomas Playford]] announced the commencement of a comprehensive infrastructure planning study for the future of Adelaide's transport needs. This report, titled the ''Metropolitan Adelaide Transportation Study (MATS)'' was released in August 1968 together with an announcement that six months would be allowed for public comment before commencement of work.{{Citation needed|reason=This claim needs a reliable source; could not find any source to back up the claim of public comment.|date=October 2016}}

== Recommended freeways ==

=== North-South Freeway ===
[[File:Kwinana Freeway Perth small.jpg|280px|right|thumb|The [[Kwinana Freeway]] in [[Perth, Western Australia]]]]
The North-South Freeway was one of the most important parts of the plan, allowing travel north and south of Adelaide. The Report on the Metropolitan Area of Adelaide predicted that the city would eventually stretch more than 70&nbsp;km, from [[Elizabeth, South Australia|Elizabeth]] in the north to [[Sellicks Beach, South Australia|Sellicks Beach]] in the south by the 1980s.

Travel from Salisbury to Noarlunga was estimated to take approximately 30 minutes. The freeway consisted of two sections: the Noarlunga Freeway and the Salisbury Freeway. The Noarlunga freeway would serve the rapidly growing residential, industrial and recreational to the south connecting to major highways to [[Victor Harbor]] and [[Yankalilla]].

Starting at Old Noarlunga, it was to follow a path adjacent to Main South Road (this has since been completed and is known as the [[Southern Expressway (Australia)|Southern Expressway]]), then continuing north on a path roughly parallel to South Road then land near the west parkland with off ramps to the CBD, until joining the North Adelaide Connector. An interchange north of Anzac Highway would connect it to the proposed Glenelg Expressway that would have replaced the Glenelg tram line.

The Salisbury Freeway was the six-lane continuation of the Noarlunga Freeway starting at the Hindmarsh Interchange then roughly followed the west of the [[Gawler]] rail line through [[Wingfield, South Australia|Wingfield]] and north to [[Edinburgh, South Australia|Edinburgh]]. The Noarlunga Freeway was proposed to be eight lanes, cost $34,000,000 in land acquisition, $58,000,000 in construction and carry 93,000 cars on an average weekday by 1986.

[[Main North Road]] comparably carries approximately 46,000 cars on an average weekday in 2007.<ref>[http://www.transport.sa.gov.au/transport_network/facts_figures/traffic_volumes.asp Transport SA: Traffic Volumes] accessed 21 November 2007</ref> The [[Mitchell Freeway|Mitchell]] and [[Kwinana Freeway|Kwinana]] Freeways in [[Perth, Western Australia|Perth]] forming a North-South Freeway present a comparable example to the proposed North-South Freeway in Adelaide.

=== Port Freeway ===
This was to be a freeway constructed in the wide [[median strip]] of [[Port Road, Adelaide|Port Road]] that had been left in earlier years for a possible [[canal]] leading from [[Port Adelaide]] to the Adelaide city centre. It was to go from the Hindmarsh Interchange to the Old Port Road intersection. It would feature [[Pedestrian separation structure|pedestrian overpasses]] but was still criticised for blocking communications across Port Road.<ref>Thomas Wilson, p117</ref> Following Port Road, Commercial Road was to continue over a new [[bridge]] over the [[Port River]] connecting to Victoria Road making a continuous [[arterial road]].

=== Hindmarsh Interchange ===
The largest construction project in the plan. The intersection of the Port Freeway, North-South Freeway and North Adelaide Connector would have required a four-level spaghetti interchange with many flyovers that would have almost engulfed the suburb of [[Hindmarsh, South Australia|Hindmarsh]].<ref name="NorleyJournal" /> Pictures of similar sized [[Interchange (road)|interchanges]] in [[Los Angeles]] were used to good effect by opponents of MATS. There were four different designs proposed including one that was to be sited in the [[Adelaide Parklands|parklands]], reducing the need for land acquisition.

=== North Adelaide Connector ===
This was to connect the western and eastern sides of the city starting from the Hindmarsh Interchange and connecting to the Modbury Freeway. It would have been constructed through [[Adelaide Parklands|parkland]] and travel partially underground to reduce the loss of open space.

Arterial road projects have since taken place in the area, particularly on roads lining the parklands including Park Terrace, Fitzroy Terrace and Park Road. And overpass on Park Terrace over the Gawler railway line has been constructed but not over the [[Outer Harbor railway line, Adelaide|Outer Harbor]] railway line.

The Hawker Street tram bridge over the Gawler Railway line was demolished in the 1970s due to lack of maintenance and safety concerns. An overpass over the [[Gawler railway line, Adelaide|Gawler railway line]] at [[Torrens Road, Adelaide|Torrens Road]] has not been constructed as proposed but land set aside remains. A report on the [[Torrens Road, Adelaide|Torrens Road]] upgrade in 2005 stated grade separation was no longer a priority.<ref>[http://www.parliament.sa.gov.au/NR/rdonlyres/4E6AA0E3-02D1-4EE8-82F5-9E9D92068C03/2914/finalreport.pdf Torrens Road Upgrade Final Report] accessed 1 February 2008 {{Dead link|date=October 2010|bot=H3llBot}}</ref>

=== Hills Freeway ===
[[File:Mountosmondinterechange sefreeway.JPG|right|280px|thumb|The [[Mount Osmond, South Australia|Mount Osmond]] Interchange on the [[South Eastern Freeway]]]]
This was proposed to be a connection between the new [[South Eastern Freeway]] (then under construction) and the CBD. It would have cut a swathe through [[College Park, South Australia|College Park]], [[St. Peters, South Australia|St. Peters]], [[Norwood, South Australia|Norwood]], [[Rose Park, South Australia|Rose Park]], [[Myrtle Bank, South Australia|Myrtle Bank]] and [[Urrbrae, South Australia|Urrbrae]] before leaving the city at Belair Road. Many of Adelaide's most affluent suburbs would have been broken up. This proved to be the most controversial part of the entire MATS plan. It was dropped from subsequent proposals but served to turn public opinion against the rest of the project.{{Citation needed|reason=This claim needs a reliable source; could not find any source to back up the claims about public opinion.|date=October 2016}}

=== Foothills Expressway ===
A proposed link between the North-South Freeway at [[Darlington, South Australia|Darlington]] (approximately the point where South Road meets Sturt Road) travelling in a north-east direction to meet up with the Hills Freeway at Belair Road. The expressway was scrapped from subsequent proposals with the Hills Freeway.

=== Modbury Freeway ===
Starting at the North Adelaide Connector, this would have followed the [[River Torrens]] (the [[O-Bahn Busway]] was later constructed in this corridor instead). At approximately 1&nbsp;km from the O-Bahn's current terminus in [[Modbury, South Australia|Modbury]], it would have then turned further north along what is now [[McIntyre Road, Adelaide|McIntyre Road]] through [[Golden Grove, South Australia|Golden Grove]].

An [[express bus]] service along the freeway giving a similar service to the current O-Bahn was proposed. The freeway would have required the relocation of the [[River Torrens]] in various sections.

=== Dry Creek Expressway ===
Starting at Port Adelaide, this would have been an east-west connector running roughly parallel to and slightly north of [[Grand Junction Road, Adelaide|Grand Junction Road]] and terminating at the Modbury Freeway. The section west of [[Main North Road]] has been completed since as the [[Port River Expressway]]. However it has not been extended eastward.

{{Clear}}
{| class="wikitable" style="width: 75%; margin: 0 auto 0 auto;"
|+ '''Attributes Table'''
|-
!
! Noarlunga Freeway
! Hindmarsh Interchange
! North Adelaide Connector
! Port Freeway
! Salisbury Freeway
! Hills Freeway
! Modbury Freeway
|-
! Cost Land Acquisition
|34,000,000
|16,700,000
|5,700,000
|100,000
|9,800,000
|20,000,000
|7,400,000
|-
! Cost Construction
|58,000,000
|13,000,000
|11,500,000
|15,100,000
|14,000,000
|32,000,000
|42,500,000
|-
! Cost Total
|92,000,000
|29,700,000
|17,200,000
|15,200,000
|23,800,000
|52,000,000
|49,900,000
|-
! Estimated Cars Weekday 1986
|93,000
|N/A
|62,000
|68,000
|62,000
|58,500
|79,000
|-
! Length in Kilometres
|34.4
|2.6
|3.2
|3.3
|12.7
|2.9
|21.2
|-
! Number of Lanes
|8
|N/A
|6
|6
|6
|6
|8
|-
| 
|}

==Public transport changes==
There were various changes to public transport proposed. While the report stated that it was important in directing and shaping urban growth and improving the [[Central business district|city centre]], only 14 kilometres of new railway was proposed compared to 131 kilometres of freeway.<ref name="NorleyJournal" /> The only remaining part of the [[Trams in Adelaide|Adelaide tram network]], the Glenelg Tram, was also to be removed in favour of an expressway.

===Train===
The existing rail system was to be turned into a rapid rail network which would be aimed at providing efficient long distance, high speed suburban transport. Many [[train station|railway station]]s were to be rationalised and some would be relocated to link to main roads. Closure of some stations allowed higher running speeds of trains and reduced running and maintenance costs.

Locating stations closer to main roads made them more accessible and visible. Railway stations would be supported by feeder bus services thereby increasing their serviceable range. Competing bus services were to be removed. A distance of 3.2 kilometres between railway stations was said to be optimal.

The [[Willunga railway line|Noarlunga rail line]], which then only went as far as [[Hallett Cove]], was to be extended to [[Christie Downs, South Australia|Christie Downs]].

====King William Street Subway====
The most significant proposed public transport project was an underground railway beneath the city to bring rail to the core of the CBD. It was to link the main north-south [[Gawler Central railway line|Gawler]] and [[Noarlunga Centre railway line|Noarlunga]] lines with a new arrangement of through-stations. This would have eliminated the limitations in service-frequency in railways with a central terminus. After skirting underground to the north of the [[Adelaide railway station]], the subway was to proceed under the central King William Street and serve it with three stations, before returning to the surface just south of Greenhill Road where it is crossed by the current [[Glenelg Tram]]. The envisioned closure of the latter would have allowed its corridor to be used by the new railway instead, until [[Glandore, South Australia|Glandore]], although options to accommodate four tracks and both services in this section of corridor could have existed. From [[Glandore, South Australia|Glandore]], the new railway would have rejoined the existing Noarlunga Line at [[Edwardstown, South Australia|Edwardstown]] via an approximately 1,200m-long new railway corridor running parallel to South Road and about 200m to its west.

Apart from the direct public transport benefits, it was said the subway would also lead to increased land values and encourage development at the southern end of the city. The Toronto [[Yonge Street]] Subway was used as an example. Furthermore, the heritage [[Adelaide railway station]] and much of the Adelaide rail yards would have become available for [[value capture]] property development.

The subway was estimated to cost $32,000,000. Construction costs were to be reduced by using a [[cut and cover]] construction technique involving the temporary removal of King William Street.

===Tram===
The report recommended the closure of the [[Glenelg Tram]] and its replacement with the Glenelg Expressway. The report stated that significant investment would be required to integrate the tram line with the proposed new [[Rapid transit|rapid transit rail system]] and to construct [[grade separation]]s along existing roads. Based on these assumptions, it recommended that the continued operation of the tram would not be cost-effective.

It was predicted that overall public transport usage would fall to below 5% by around the year 2000, and proceeding with the recommended changes would maintain the number at 7%.{{Citation needed|reason=This claim needs a reliable source. The report doesn't seem to mention these statistics.|date=October 2016}}

==Reaction and opposition==
In concert with changing community attitudes in Australia at the time,<ref>{{cite journal|last1=Scrafton|first1=D|title=Managing Transport Integration in South Australia|journal=Australian Journal of Public  Administration|date=1982|volume=7|pages=339–354|accessdate=30 October 2016}}</ref> many South Australians were cautious about the construction of large networks of freeways. Opposition was similar to the [[Freeway and expressway revolts]] experienced in the [[United States]].

Large-scale property acquisition proved to be one of the most contentious issues, with the very large designs of several freeway interchanges seemingly undertaken without expectations of such an issue. The Noarlunga Freeway alone would have required the acquisition as many as 3,000 properties, including 817 residential dwellings. The Hills Freeway would have required the demolition of significant areas of the historic suburbs of inner south-eastern Adelaide.<ref name="History">{{cite web | last =  | first =  | date =  | title = Adelaide’s Freeways: A History from MATS to the Port River Expressway | work = OZROADS | url =  http://www.ozroads.com.au/SA/freeways.htm | accessdate = 19 October 2009 }}</ref>

The impact of freeways on the urban landscape also proved to be a large source of concern. It was feared freeways would create social issues, with people looking at the urban problems cities such as [[Los Angeles]] experienced such as the division of neighbourhoods leading to the creation of urban [[ghettos]], leading to concern that the same would happen in Adelaide if freeways were constructed.<ref name="NorleyJournal" /> The [[Report on the Metropolitan Area of Adelaide 1962]] contained images of the city of Los Angeles and its extensive freeways to represent a potential model for Adelaide's future transport solutions.

==Implementation==
In 1969 the State Cabinet with [[Liberal and Country League]] coalition as government and [[Steele Hall]] as premier approved the MATS Plan excluding some proposals which were to be further reviewed including:
* Closure of the Grange Railway line
* Foothills Expressway and Hills Freeway
* Selections of the Modbury, Noarlunga Freeways and Dry Creek Expressway
* Rerouting railway from [[Edwardstown, South Australia|Edwardstown]] to [[Goodwood, South Australia|Goodwood]]

===Abandonment===
In 1970 a new [[Australian Labor Party|Labor]] government under [[Don Dunstan]] was elected and shelved MATS, but did not go as far as selling the corridors already acquired. However, the [[Adelaide Festival Centre]] was developed over the optimal portal location for the proposed King William Street subway. Dunstan resisted urban sprawl, but initiated the ill-fated concept of the [[Monarto]] satellite city, as well as investigated new technologies in [[public transport]].<ref>[http://www.samemory.sa.gov.au/site/page.cfm?c=2023&mode=singleImage Eric Franklin, The Advertiser: Labour Will Revise MATS Plan], 6 May 1970</ref> Dunstan attempted to construct a light rail line in the north-east along the Modbury corridor. This was not built, but instead replaced by the [[O-Bahn Busway|O-Bahn]] bus route instead.

In 1980, the [[Liberal party of Australia|Liberal party]] won government on a platform of [[fiscal conservatism]] and the premier [[David Tonkin]] committed his government to selling off the land acquired for the MATS plan ensuring that even if needs or [[public opinion]] changed, the construction of most MATS-proposed freeways would be impossible. However debate continued on a North-South Freeway to replace [[South Road, Adelaide|South Road]]. In 1982, the Minister for Transport [[Michael Wilson (Australian politician)|Michael Wilson]], abandoned the idea of a high speed freeway and instead began widening South Road between [[Torrens Road, Adelaide|Torrens Road]] and [[Daws Road, Adelaide|Daws Road]] as a short term solution, while retaining the key central portion of the North-South Corridor between [[Dry Creek, South Australia|Dry Creek]] and [[Darlington, South Australia|Darlington]] as a concept.<ref name="History"/>

In June 1983, the North-South Corridor, the ''last surviving element of MATS'', was completely abandoned by [[John Bannon]]'s new Labor government, who cited land sales as the reason. The abandonment had a significant impact on the Highways Department as it was the first time in its history that a government had rejected the recommendations of the Commissioner.<ref name="History"/>

== Post-abandonment assessment ==
Attitudes towards MATS in the present day are mixed. The then premier Steele Hall still believed abandoning the plan was a severe mistake and has continued to push for the plan to be implemented.<ref>[http://www.news.com.au/adelaidenow/story/0,22606,21440849-5006301,00.html The Advertiser: Adelaide Nations Traffic Basket] accessed 22 February 2008</ref> Freight transport and motoring lobbies generally favoured the plan heavily and, periodically, refer to the rejection of the MATS plan as a lost opportunity. Major road lobby groups as of 2007 continue to call for a North-South freeway in particular with the State Government joining calls for funding under the Federal Government's [[AusLink]] Program.<ref>[http://www.news.com.au/adelaidenow/story/0,22606,22093238-5006301,00.html $2 billion super road link] accessed 17 July 2007</ref>

Public transport activists criticised the plan due to its limited benefits for public transport and the potential effects on [[urban sprawl]].<ref name="NorleyJournal" /> Others believe varying degrees of the plan were too ruthless towards the environment and would have ruined the character of Adelaide.{{Citation needed|reason=This claim needs a reliable source. Had trouble finding any sources to back up any claims in this section other than random news articles.|date=October 2016}}

=== Present-day ===
Some roads constructed since MATS are reminiscent of the original proposed freeways in MATS due to Adelaide's inherent transport pressures. The [[Southern Expressway (Australia)|Southern Expressway]] has been constructed partly following the alignment the southern section of the Noarlunga freeway. It remained viable to construct as much land remained undeveloped. The South Eastern Freeway, was completed in 1979 and had partly began construction at the time of MATS. The [[Port River Expressway]] was opened in 2005, which partially follows the original [[Modbury, South Australia|Modbury]] to [[Port Adelaide, South Australia|Port Adelaide]] ("Dry Creek") Expressway proposed by MATS but does not extend eastwards.

The Rann Labor government stated it believed [[South Road]] should be upgraded into a non stop north-south route, only to decline a 2007 $1bn pre-election funding offer from the then Howard Federal Government which would have upgraded substantial sections of the road. Construction of an underpass at the intersection of South Road and Anzac highway began in 2007 and was completed in 2009,<ref>http://dtei.sa.gov.au/infrastructure/south_road_upgrade/content/anzac_highway</ref> however other underpasses proposed for the intersections of [[Grange Road, Adelaide|Grange Road]], [[Port Road, Adelaide|Port Road]] and the [[Outer Harbor railway line, Adelaide|Outer Harbor]]/[[Grange railway line, Adelaide|Grange railway line]] appeared to be shelved. The same fate appeared to befall that government's 2010 proposal for South Rd/Sturt Rd underpass.

==See also==
* [[Transport in Adelaide]]
* [[State Transport Authority (South Australia)|State Transport Authority]]
* [[Railways in Adelaide]]
* [[Freeways in Australia]]
* [[South Eastern Freeway]]
* [[South Road, Adelaide|South Road]]
* [[History of Adelaide]]

'''Melbourne:'''
* [[1969 Melbourne Transportation Plan]]

'''Hobart:'''
* [[Hobart Area Transportation Study]]

==References==
{{reflist}}

==Bibliography==
* De Leuw. Cather & Company. Rankine & Hill Alan M. Voorhees & Associates, 1968, ''Metropolitan Adelaide Transport Study 1968'', Adelaide
* The University of Adelaide Department of Adult Education, ''the Metropolitan Adelaide Transportation Study and the Future Development of Adelaide'', 1968, Adelaide
* Thomas Wilson, ''The Relationship Between a Transport Link and Land Use Development between Adelaide and Port Adelaide South Australia'', Adelaide
* J.C. Radcliff. C.J.M. Steele, ''Adelaide Road Passenger Transport 1836 - 1958'', Libraries Board of South Australia, Adelaide, 1974

== External links ==
* [http://www.atlas.sa.gov.au/go/resources/atlas-of-south-australia-1986/the-course-of-settlement/urban-planning Atlas of South Australia - The course of settlement]

[[Category:Proposed transport infrastructure in Australia]]
[[Category:History of South Australia]]
[[Category:1965 in law]]
[[Category:Transport in Adelaide]]
[[Category:History of Adelaide]]
[[Category:Freeways and highways in Adelaide]]
[[Category:Government reports]]
[[Category:Politics of South Australia]]