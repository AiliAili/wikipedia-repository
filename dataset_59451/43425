<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = P.V.2 <!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = <!--in the ''image:filename'' format, no image tags-->
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Floatplane Fighter
  |national origin =[[United Kingdom]]
  |manufacturer =  [[Port Victoria Marine Experimental Aircraft Depot]]|RNAS Marine Experimental Aircraft Depot, Port Victoria
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 16 June 1916<!--if it hasn't happened, leave it out!-->
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype only<!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 1
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}
The '''Port Victoria P.V.2''' was a [[United Kingdom|British]] prototype [[floatplane]] fighter of the [[World War I|First World War]], designed and built at the [[Royal Naval Air Service]]'s [[Port Victoria Marine Experimental Aircraft Depot]] on the [[Isle of Grain]]. Only a single aircraft was built, with the type not being chosen for production.

==Design and development==

The Port Victoria Depot's second design, designated '''Port Victoria P.V.2''' was a [[floatplane]] fighter intended to intercept German [[Zeppelin]]s. The P.V.2 was a small single engined [[biplane]], powered by a [[Gnome Monosoupape]] [[rotary engine]] driving a four blage propellor.  It was of wood and fabric construction, and of ''sesquiplane'' configuration, i.e. with its lower wing much smaller than its upper wing (both of which used the high-lift wing sections pioneered by the [[Port Victoria P.V.1|P.V.1]]). Unusually, the aircraft's wing bracing struts also carried the aircraft's floats, forming a "W" shape when viewed from the front.  The upper wing was attached directly to the top of the fuselage, giving a good field of fire for the intended armament of a single 2-lb [[Recoilless rifle#History|Davis gun]] recoilless gun.<ref name="Mason Fighter p65"/>

The P.V.2 first flew on 16 June 1916, and demonstrated good performance and handling.  The upper wing, however, while giving excellent upwards view to the pilot, gave a poor downwards view of the pilot, particularly during landing, while the Davis gun had lost favour with the Admiralty as an anti-Zeppelin weapon.  The P.V.2 was therefore rebuilt as the '''P.V.2bis''' with a revised, longer span upper wing mounted 12&nbsp;inches (0.30 m) above the fuselage and the Davis gun replaced by two [[Lewis gun]]s mounted above the wing, firing over the propellor. The modified aircraft first flew in this form in early 1917.<ref name="Collyer p50">Collyer 1991, p.50.</ref>

While the P.V.2bis again showed excellent handling, the RNAS's requirement for a floatplane anti-Zeppelin fighter had lapsed, and no production was ordered.<ref name="Collyer p50"/>

==Specifications (P.V.2bis) ==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=The British Fighter since 1912 <ref name="Mason Fighter p65">Mason 1992, p.65.</ref>

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=1
|capacity=
|length main= 22 ft 0 in
|length alt= 6.71 m
|span main= 29 ft 0 in
|span alt= 8.84 m
|height main= 9 ft 4 in
|height alt= 2.85 m
|area main= 180 ft²
|area alt= 16.7 m²
|airfoil=
|empty weight main= 1,211 lb
|empty weight alt= 550 kg
|loaded weight main= 1,702 lb
|loaded weight alt= 774 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)=[[Gnome Monosoupape]]
|type of prop=[[rotary engine]]
|number of props=1
|power main= 100 hp
|power alt= 75 kW
|power original=
   
|max speed main= 81 knots
|max speed alt= 93 mph, 150 km/h
|cruise speed main= <!-- knots -->
|cruise speed alt= <!-- mph,  km/h -->
|never exceed speed main= <!-- knots -->
|never exceed speed alt=<!--  mph,  km/h -->
|stall speed main= <!-- knots -->
|stall speed alt= <!-- mph,  km/h -->
|range main= <!-- nm -->
|range alt= <!-- mi,  km -->
|ceiling main= 10,000 ft
|ceiling alt= 3,050 m
|climb rate main= <!-- ft/min -->
|climb rate alt=<!--  m/s -->
|loading main= <!-- lb/ft² -->
|loading alt= <!-- kg/m² -->
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= <!-- hp/lb -->
|power/mass alt= <!-- W/kg -->
|more performance=*'''Climb to 3,000 ft (915 m):''' 6 min
|guns=2x [[.303 British|.303 in]] [[Lewis gun]]s above upper wing.
|avionics=

}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Port Victoria P.V.9]]<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==
{{Reflist}}

*Bruce, J.M. "[http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%201755.html The Sopwith Tabloid,Schneider and Baby: Historic Military Aircraft No.17 Part IV]". ''[[Flight International|Flight]]'', 29 November 1957. pp.&nbsp;845–848.
*Collyer, David. "Babies Kittens and Griffons". ''[[Air Enthusiast]]'', Number 43, 1991. Stamford, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;50–55.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland:Naval Institute Press, 1992. ISBN 1-55750-082-7.

{{Port Victoria Aircraft}}

[[Category:Floatplanes]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Sesquiplanes]]
[[Category:Single-engine aircraft]]
[[Category:Rotary-engined aircraft]]
[[Category:Port Victoria aircraft|PV2]]