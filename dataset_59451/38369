{{good article}}
{{Use dmy dates|date=January 2016}}
{{Infobox person
| name = Milutin Bojić
| honorific_prefix = 
| honorific_suffix = 
| image = Milutin Bojic.jpg
| image_size = 250px
| alt = Mustachioed man looking at the camera
| caption = Milutin Bojić, {{circa|1912}}
| native_name = Милутин Бојић
| native_name_lang = Serbian
| birth_date = {{OldStyleDate|18 May|1892|7 May}}
| birth_place = [[Belgrade]], [[Kingdom of Serbia|Serbia]]
| death_date = {{OldStyleDate|8 November|1917|25 October}} (aged 25)
| death_place = [[Thessaloniki]], [[Kingdom of Greece|Greece]]
| resting_place = [[Novo groblje|New Cemetery]], Belgrade
| occupation = Poet<br/>Playwright<br/>Literary critic<br/>Soldier
| language = Serbia
| nationality = Serbian
| alma_mater = [[University of Belgrade]]
| notablework = ''[[Plava grobnica]]''  
| partner = Radmila Todorović     
| years_active = 1907{{dash}}1917
| website = {{URL|http://www.milutinbojic.org.rs/}}
}}
'''Milutin Bojić''' ({{lang-sr-Cyrl|Милутин Бојић}}; {{OldStyleDate|18 May|1892|7 May}}&nbsp;– {{OldStyleDate|8 November|1917|25 October}}) was a Serbian poet, theatre critic, playwright, and soldier.

A native of [[Belgrade]], he began writing poetry at an early age and published a number of literary reviews under a [[pseudonym]] while he was still a teenager. He rose to prominence during the [[Balkan Wars]], writing about his experiences in territories newly retaken from the [[Ottoman Empire]]. The outbreak of [[World War I]] interrupted Bojić's studies at the [[University of Belgrade]] and forced him to postpone marrying his girlfriend, Radmila Todorović. The couple were separated in the chaos of war, and Bojić left Belgrade with his family and relocated to [[Niš]], where he worked as a military censor and wrote articles for a local newspaper to pay his family's bills. In October 1915, the [[Serbian Army]] was overwhelmed by a combined Austro-Hungarian, Bulgarian and German invasion and forced to retreat to neutral Greece via Albania. Bojić and his younger brother joined the exodus, marching for several weeks through Kosovo, Montenegro and northern Albania, where they were finally reunited with Bojić's fiancée. Bojić was not allowed to accompany his brother and fiancée on a ship destined for Italy because he was of fighting age, and had to continue marching to Greece without them.

In early 1916, Bojić reached the Greek island of [[Corfu]], where he was recruited to work for Serbian military intelligence. That summer, he was transferred to [[Thessaloniki]], where he continued working for the military. In August, he was granted leave and sailed to France, where he reunited with his fiancée and his brother. Bojić returned to Greece several weeks later, and was diagnosed with [[tuberculosis]] in September the following year. Owing to the political connections of his patrons, he managed to find care at an exclusive military hospital in Thessaloniki, but by late October his condition worsened. He died in early November at the age of 25. He was initially buried at the [[Allies of World War I|Allied]] military cemetery at [[Zeitenlik]], but in 1922, his siblings had his remains relocated to Belgrade, where they were reburied beside those of his parents.

Bojić's popularity grew exponentially following his death. Many of his poems received widespread critical acclaim for their portrayal of the Serbian Army's retreat during the winter of 1915–16 and its stay on Corfu, where thousands of soldiers succumbed to disease and exhaustion and were [[Burial at sea|buried at sea]]. Bojić's work remained popular in Yugoslavia for much of the 20th century, cementing his reputation as one of the greatest Serbian poets of the [[Romanticism|Romantic period]].

==Life and career==

===Family===
Milutin Bojić was born in [[Belgrade]] on {{OldStyleDate|18 May|1892|7 May}}, the eldest child of Jovan and Sofia Bojić (''{{née}}'' Bogojević). His father's family was originally from [[Herzegovina]]. Following the [[First Serbian Uprising]], Bojić's great-grandfather fled his ancestral homeland and settled in the Austro-Hungarian city of [[Zemun|Semlin]] (modern Zemun) to escape [[Ottoman Empire|Ottoman]] persecution. Bojić's father and grandfather were born in Semlin, and were good-standing members of the town's fledgling [[Serbs|Serb]] community. In 1875, Bojić's father was conscripted into the [[Austro-Hungarian Army]]. Shortly afterwards, he entered into an argument with a high-ranking Hungarian officer, insulted him, then fled to the Serbian capital to escape punishment. He attended trade school in Belgrade and became a successful shoemaker.{{sfn|Bojić|Knežević|1968|p=19}}

Bojić's matrilineal line traces its origins to the town of [[Tetovo]], in what is now northern [[Republic of Macedonia|Macedonia]]. His mother's family had fled the town in 1690 as part of the [[Great Serb Migration]] and settled in [[Zrenjanin|Bečkerek]] (modern Zrenjanin). Bojić's maternal grandfather, Jovan, had lived in the border town of [[Pančevo]] since he was a child, married and started a family there. Much like Bojić's father, he was a successful shoemaker, and in 1890 he offered to marry his only daughter to him. After initial disagreement over the price of the [[dowry]], Bogojević gave the marriage his blessing and Bojić's parents were soon married.{{sfn|Bojić|Knežević|1968|p=19}}

At the time of Bojić's birth, his parents were living in a small flat at No. 4 Sremska Street, situated just above his father's shop in Belgrade's [[Stari Grad, Belgrade|Stari Grad]].{{refn|group=nb|The building was destroyed in the German [[Operation Retribution (1941)|bombing of Belgrade]] during World War II.{{sfn|Đorđević|1977|p=15}}}} Over the next decade, they had four more children&nbsp;– Jelica (<abbr title="born">b.</abbr> 1894), Danica (<abbr title="born">b.</abbr> 1896), Radivoje (<abbr title="born">b.</abbr> 1900) and Dragoljub (<abbr title="born">b.</abbr> 1905). All of their children survived to adulthood. When Bojić was a child, he and his family were frequently visited by his mother's cousin Jovan Sremac, the brother of humourist [[Stevan Sremac]]. Jovan is said to have greatly influenced the young Bojić, having introduced him to Serbian folk tales and medieval legends at an early age.{{sfn|Bojić|Knežević|1968|pp=16–17}}

===Education and first publications===
Bojić began attending the Terazije Elementary School in 1898, and finished with excellent grades.{{sfn|Bojić|Knežević|1968|pp=13–14}} He likely began writing poetry between the ages of eight and ten, and this caught the attention of some of his teachers. Jovan Dravić, who taught Serbian in Bojić's school, wrote: "One of my pupils has been writing poetry since his first year of elementary school. His father, a shoemaker on Sremska Street, is very proud of his son's work. He keeps his poems locked away in a safe as great treasures, convinced that they are of immense value and proof of his son's brilliant future."{{sfn|Đorđević|1977|p=15}}

In 1902, the Bojićes moved into a small house on Hilandarska Street. That autumn, Bojić enrolled into Belgrade's Secondary School No. 2, which taught children from the ages of ten to eighteen. Over the next several years, Bojić distinguished himself as an excellent student. In 1907, he was recognized as the best student in his school, and was exempt from final examinations.{{sfn|Đorđević|1977|pp=16–17}} By this time, Bojić's poems started appearing in his school's periodical. He also began writing literary reviews for [[Jovan Skerlić]] and [[Milan Grol]]'s Daily News (''Dnevni list''), and became the paper's youngest contributor. All of Bojić's contributions there were written under a [[pseudonym]] because he feared he would not be taken seriously if his true age was uncovered. Bojić also authored articles in the newspapers Artwork (''Delo''), Wreath (''Venac'') and The Serbian Literary Gazette (''Srpski književni glasnik'').{{sfn|Bojić|Knežević|1968|p=22}} In 1908, during the [[Bosnian crisis|Austro-Hungarian annexation of Bosnia and Herzegovina]], Bojić penned his first stage play, titled "The Blind Despot" (''Slepi despot''). It was meant to be the first play of a trilogy titled "The Despot's Crown" (''Despotova kruna''). Bojić shared the play with his friend Radoslav Vesnić, who then showed it to Rista Odović, the director of the [[National Theatre in Belgrade]].{{sfn|Đorđević|1977|pp=16–17}}

By the end of his secondary school studies, Bojić was elected chairman of his school's literary club.{{sfn|Đorđević|1977|pp=16–17}} Upon turning eighteen, in May 1910, he was deemed unfit to serve in the [[Serbian Army]] and excused from mandatory service.{{sfn|Bojić|Knežević|1968|p=23}} After graduation, in the autumn of 1910, he enrolled in the [[University of Belgrade]]'s [[University of Belgrade Faculty of Philosophy|Faculty of Philosophy]], where he studied the works of [[Immanuel Kant]], as well as German, Italian and South Slavic literature with varying rates of success.{{sfn|Đorđević|1977|pp=16–17}} Shortly after entering the university, Bojić began contributing theatrical reviews to ''Pijemont'', an ultra-nationalist [[daily newspaper|daily]] strongly opposed to the [[Prime Minister of Serbia]], [[Nikola Pašić]], and his [[People's Radical Party]] ({{lang-sr|''Narodna radikalna stranka''; NRS}}).{{refn|group=nb|''Pijemont'' is the Serbian rendering of [[Piedmont]], and a reference to the [[Kingdom of Sardinia]] (Piedmont-Sardinia), which greatly contributed to the [[Italian unification]]. Equating Serbia with Piedmont reflected the Serbian nationalist belief that Belgrade would unite all Serbs into a single state. The newspaper ran from 1911 until 1915, with only a short interruption in the fall of 1912 due to the [[First Balkan War]].{{sfn|Đorđević|1977|pp=17–18}}}} ''Pijemont'' was also the quasi-official paper of the secret organization Unification or Death (better known as the [[Black Hand (Serbia)|Black Hand]]), which played an important role in Serbian political life between 1903 and 1914.{{sfn|Đorđević|1977|pp=17–18}}

Milutin K. Dragutinović was one of Bojić's greatest influences during his teenage years. Dragutinović was one of Bojić's former secondary school teachers, as well as a literary critic and member of the National Theatre's artistic committee. He advised Bojić to continue writing poetry and dramatic verse.{{sfn|Đorđević|1977|pp=16–17}} In 1911, Bojić shared the first draft of a stage play titled "Chains" (''Lanci'') with Dragutinović, who encouraged him to submit it to a competition held by the Literary Committee of the National Theatre in Belgrade. Bojić's play was one of forty-two works that were submitted. A number of prominent writers took part in the competition, including [[Ivo Vojnović]], [[Branislav Nušić]], [[Aleksa Šantić]], and [[Svetozar Ćorović]]. "Chains" was rejected as being too "naïve" and "full of youthful exaggerations", and the prize went to another contestant.{{sfn|Đorđević|1977|p=20}} In May, Bojić's father died suddenly of a [[Myocardial infarction|heart attack]], aged 56.{{sfn|Bojić|Knežević|1968|p=23}}

===Rise to prominence and coverage of the Balkan Wars===
Despite his busy schedule, during his university years Bojić spent many of his nights in cafés and bars, mingling with other artists in [[Skadarlija]], Belgrade's Bohemian quarter.{{sfn|Đorđević|1977|pp=18–19}} He was extremely popular among the writers and artists of his generation.{{sfn|Đorđević|1977|p=20}}

During this time, many of Belgrade's merchant elite became art patrons. Bojić's patrons were Ljuba Jovanović-Patak and his wife Simka, who used their power and influence to publicize his works.{{sfn|Đorđević|1977|pp=18–19}} During the [[Balkan Wars]], Bojić accompanied the Serbian Army as a war reporter and wrote several [[travel literature|travelogue]]s about his experiences in Kosovo and Macedonia.{{sfn|Lalić|1974|p=23}} The expulsion of the Turks, who had occupied much of the Balkans for 500 years, seemed to fill Bojić with optimism about the future. Helena Malířová, a Czech volunteer nurse with the 17th Reserve Hospital of the Serbian Army's 7th Regiment, recalled: "His spirit was in constant opposition to everything; at the same time he was an enthusiast. He was bursting with desires, and he drank the sap of life through his senses."{{sfn|Đorđević|1977|pp=18–19}}{{refn|group=nb|Malířová was the wife of author [[Ivan Olbracht]]. She later translated two of Bojić's works into [[Czech language|Czech]], and saw to their publication in the literary magazine ''Svetozar''.{{sfn|Đorđević|1977|pp=18–19}}}} Historian Mihailo Đorđević writes:

<blockquote>[Bojić] was an impulsive young man. From rare photographs and the testimony of his contemporaries emerges [...] a figure of medium stature, with slightly drooped shoulders, thick brown hair, and a pale oval face. His eyebrows were dark, and one of them was almost always raised, giving his face an expression of intense irony. His eyes burned with constant passion, and his lips were full and sensuous. There was also something young and vulnerable in his smile. He smiled often, and his conversation was reputed to be brilliant.{{sfn|Đorđević|1977|p=18}}</blockquote>

As the wars raged, Bojić wrote a historical drama titled "The King's Autumn" (''Kraljeva jesen''), which received considerable praise from Skerlić. The drama premiered at the National Theatre in October 1913. Another one of Bojić's works, "Ms. Olga" (''Gospođa Olga'') premiered soon after. In early 1914, publishing magnate [[Svetislav Cvijanović]] printed Bojić's first poetry collection, consisting of 48 works.{{sfn|Lalić|1974|p=23}}

===Outbreak of World War I and retreat to Corfu===
[[File:Serbian retreat WWI.jpg|thumb|A column of Serbian soldiers retreating through the Albanian countryside, {{circa|1916}}]]
At the outbreak of [[World War I]], Bojić was in his final year at the University of Belgrade and had gotten engaged to his girlfriend, Radmila Todorović.{{refn|group=nb|Todorović was three years Bojić's junior; her father was a [[colonel]] in the Serbian Army.{{sfn|Đorđević|1977|p=20}}}} The war put a stop to his education, and Bojić decided to postpone the marriage until peace was restored. He remained employed at ''Pijemont'' until 1915, when he left Belgrade with his family, never to return.{{sfn|Đorđević|1977|p=20}} While Todorović remained in the city, the Bojićes moved to the town of [[Aranđelovac]] in the Serbian interior. Shortly after, they moved to [[Niš]] as it was further from the frontlines. There, Bojić worked as a military censor. He wrote articles for the Niš Gazette (''Niški glasnik'') to pay his family's bills.{{sfn|Đorđević|1977|p=21}} In early February 1915, Bojić's mother died of cancer, leaving him to take care of his younger siblings.{{sfn|Bojić|Knežević|1968|pp=73–74}}

Bojić devoted most of his creative energy to the completion of an epic poem titled ''Cain'', which was published just before the combined Austro-Hungarian, Bulgarian and German invasion of Serbia in October 1915. A deeply patriotic work, ''Cain'' compares Bulgaria's impending attack on Serbia to the Biblical story of [[Cain]] jealously murdering his brother [[Abel]]. Upon capturing Niš, the Bulgarians burned every copy of the poem, and the only one that survived was the one that Bojić carried with him out of the city.{{refn|group=nb|This copy was to serve as the basis for a second edition published after the war.{{sfn|Đorđević|1977|p=21}}}} Upon leaving Niš, Bojić and his brother Radivoje joined the Serbian Army in its [[Serbian army's retreat through Albania|retreat]] to the [[Adriatic]] coast. Their sisters and infant brother went to live with a cousin in occupied [[Kraljevo]]. The Bojić brothers' journey took them through [[Kuršumlija]], [[Mitrovica, Kosovo|Mitrovica]], [[Pristina]], [[Prizren]], [[Gjakova|Đakovica]], [[Deçan|Dečani]] and [[Peć]], from where they continued to [[Andrijevica]] and [[Podgorica]]. The two intended to join retreating [[Montenegrin Army]] columns and head on to [[Shkodër|Scutari]], where they hoped that [[Allies of World War I|Allied]] ships would evacuate them and other Serbian troops to [[Kingdom of Italy|Italy]].{{sfn|Đorđević|1977|p=21}}

The Serbs quickly realized that they had no hope of being evacuated at Scutari due to the Austro-Hungarian naval presence off the northern Albanian coast, and the Serbian and Montenegrin military leaderships elected to retreat further into the Albanian interior rather than surrender. The Serbs and Montenegrins marched south, first to [[Durrës]] and then to [[Vlorë]]. From there, they intended to meet up with a French expeditionary force that was to evacuate them to the Greek island of [[Corfu]]. Thousands of soldiers either died of hunger or succumbed to the cold. Many were ambushed by hostile Albanian tribesmen and killed while traversing the countryside.{{sfn|Judah|2000|p=100}} While marching through the wilderness, Bojić began writing a poetry anthology titled "Songs of Pride and Suffering" (''Pesme bola i ponosa''), which would comprise some of his best known works. He also started working on a new drama in verse titled "The Marriage of Uroš" (''Uroševa ženidba'').{{sfn|Đorđević|1977|p=22}}

Stojan Živadinović, a friend who accompanied Bojić on the difficult journey, recalled:

<blockquote>He was dressed in a strange way. A ''[[šajkača]]'' was pulled to the front of his head and almost completely covered his forehead. His eyebrows were hardly visible. Under its weight, the ears seemed to have collapsed. He wore a kind of long, black coat, covered in mud and held in place by a string. He used another piece of string for a belt, and from it hung a pot for boiling water, a tea strainer, a canteen, and other useful things. He walked slightly bent forward, followed everywhere by the noise of the things clashing at his waist.{{sfn|Đorđević|1977|p=22}}</blockquote>

The occupation of Serbia by the Central Powers pained Bojić far more than the everyday struggles of the exodus itself. He is said to have slept very little, telling Živadinović: "You don't realize what you are missing by sleeping. In circumstances like these, the entire soul must be kept constantly awake. Whole centuries have never painted such a vast fresco. Never has death been so greedy, nor heroes so indifferent to it."{{sfn|Đorđević|1977|p=22}} In December 1915, Bojić reached [[Shëngjin]] with the others and reunited with his fiancée. Živadinović recalled: "I shall never forget the day that we saw the sea. Only then did Bojić begin to speak about the future. He was making plans for a vast novel in verse, for comedies, dramas, tragedies, all intended to bring the great events of our history back to life." Upon reaching the coast, Bojić was told that he could not board the ship to Italy because he was of fighting age. His fiancée was allowed to board and his fifteen-year-old brother was judged to be too young for military service and granted passage as well. Unable to go with them, Bojić and his companions continued down the length of the Albanian coast until they reached Corfu, where the Allies had sent ships to transport the remnants of the Serbian Army to the Greek mainland.{{sfn|Đorđević|1977|p=23}}

Upon reaching Corfu, thousands of Serb troops began showing symptoms of [[typhus]] and had to be quarantined on the island of [[Vido]], where 11,000 died over the span of two months. Seven thousand of these had to be buried at sea because there was not enough space for their remains to be interred on land; corpses were simply piled onto [[barge]]s and tossed overboard.{{sfn|Judah|2000|p=101}} Bojić was appalled by the suffering. "Our church bells toll dead instead of hours," he wrote.{{sfn|Kurapovna|2009|p=16}} Witnessing the disposal of bodies inspired Bojić to write the poem "Ode to a Blue Sea Tomb" (''[[Plava Grobnica]]''), which Đorđević describes as "Bojić's best&nbsp;... a masterpiece of Serbian patriotic poetry."{{sfn|Đorđević|1977|p=50}} Shortly after arriving at Corfu, Bojić was recruited to work for Serbian military intelligence. In mid-1916, he was transferred to [[Thessaloniki]].{{sfn|Bojić|Knežević|1968|p=113}}

===Last years and death===
{{Quote box|width=20%|align=right|quote="I shall return to you the same&nbsp;– joyful, risen, fearless&nbsp;...<br/>And appear proud, as you once knew me, on glory-gilded fields"|source="Departure" (''Odlazak''), {{circa|1916}}}}
Upon reaching Thessaloniki, Bojić spent much of his spare time reading the works of French authors and writing poetry. In August 1916, he received a month's leave and sailed for France, where his fiancée and brother had gone to escape from the war, shortly after landing in Italy. Bojić spent the month with his fiancée in [[Nice]].{{sfn|Đorđević|1977|p=24}} Upon returning to Greece, Bojić resumed his army service, and continued writing poetry. "Songs of Pride and Suffering" was first published in Thessaloniki in mid-1917. Nearly every copy of the anthology was destroyed in the [[Great Thessaloniki Fire of 1917|Great Fire of Thessaloniki]], in August 1917. The only copy that survived was one sent by Bojić to his fiancée in France and this became the basis for the post-war edition.{{sfn|Đorđević|1977|p=23}}

In September 1917, Bojić was diagnosed with [[tuberculosis]]. Owing to the influence of his old patron, Ljuba Jovanović-Patak, he was admitted to a military hospital in central Thessaloniki which tended exclusively to Serbian Army officers, where Jovanović's wife often visited him. Bojić continued writing poetry, and as his condition deteriorated, his poems began to take a melancholy tone. Nevertheless, he remained optimistic that he would recover from his illness and see his family again.{{sfn|Đorđević|1977|p=24}} One month before his death, he sent his fiancée and brother a telegram assuring them that he was "...&nbsp;only slightly ill" and promised they would be reunited.{{sfn|Đorđević|1977|p=25}} Bojić died of tuberculosis on {{OldStyleDate|8 November|1917|25 October}}. Even a few hours before his death, he appeared convinced of his survival. According to a fellow patient, Bojić "...&nbsp;died choking in half-sleep from a fit of coughing that destroyed the tissues of his lungs." He was buried at the [[Zeitenlik]] military cemetery in Thessaloniki. His family was shocked by the news of his death, particularly his brother and fiancée, who believed him to be on the road to recovery.{{sfn|Đorđević|1977|p=26}} In 1922, Bojić's remains were exhumed and transferred to Belgrade's [[Novo groblje|New Cemetery]], where they were reburied beside those of his parents.{{sfn|Kovijanić|1969|p=88}}

All of Bojić's siblings survived the war; his brother Radivoje became a diplomat and worked in the Yugoslav [[Ministry of Foreign Affairs (Yugoslavia)|Ministry of Foreign Affairs]] until the [[Axis powers|Axis]] [[invasion of Yugoslavia|invasion]] of the country in April 1941, when he left Yugoslavia with his family and emigrated to the West. Bojić's youngest brother, Dragoljub, spent the rest of his life in Belgrade and became a high school geography teacher. His sister Jelica married and lived in Belgrade until her death in 1942. His youngest sister, Danica, died single in 1952.{{sfn|Đorđević|1977|pp=15, 26}} Bojić's fiancée Radmila remained loyal to him and never married after his death; she returned to Serbia after the war and died in Belgrade in 1971.{{sfn|Đorđević|1977|p=20}}

==Influences, themes and legacy==
[[File:Milutin Bojić 1.JPG|thumb|upright=0.75|220px|A [[bust (sculpture)|bust]] of Bojić in Belgrade]]
{{Quote box|width=20%|align=right|quote="Halt, imperial galleons! Restrain your mighty rudders!<br/>Walk with silent tread as I utter this proud Requiem amid the evening chill<br/>O'er these sacred waters"|source="Ode to a Blue Sea Tomb" (''Plava Grobnica''), 1917}}
While at the University of Belgrade, Bojić studied [[The Bible]] in his free time and read the works of [[Victor Hugo]], [[Friedrich Nietzsche]], [[Charles Baudelaire]], [[Leo Tolstoy]], [[Anton Chekhov]] and [[Sigmund Freud]], among others.{{sfn|Kovijanić|1969|p=50}} Đorđević notes that Bojić's early poems reflect the degree to which he was influenced by the "exalted sensuality" of Baudelaire's works.{{sfn|Đorđević|1977|p=9}} He contends that Baudelaire's influence on Bojić's early work is "rather unfortunate", as by striving to imitate Baudelaire's style, Bojić failed to achieve originality and instead merely used Baudelaire as a "crutch in the absence of spontaneous emotion".{{sfn|Đorđević|1977|pp=73–74}}

As he matured, Bojić was influenced by the works of Irish writer [[Oscar Wilde]], particularly the play ''[[Salome (play)|Salome]]'', which was being performed at the National Theatre in Belgrade during Bojić's university years. The play influenced Bojić to such an extent that he later composed a poem of the same name. According to Đorđević, Bojić was "thrilled by the beauty of Wilde's descriptions", and reading his work inspired Bojić to use Biblical rhythm and accentuation. Đorđević notes that the archaic phrase "thou art" only appears in Bojić's poems after he began reading Wilde.{{sfn|Đorđević|1977|pp=74–75}} Wilde's influence can also be observed in Bojić's lyric plays, especially "The King's Autumn". Bojić was also inspired by French playwright [[Edmond Rostand]], whose play ''[[L'Aiglon]]'' was Bojić's "dramatic ideal incarnate". Rostand's influence is most clearly felt in "The Marriage of Uroš", where Bojić paid homage to Rostand by writing the entire drama in verse, a style that was considered obsolete at the time. Serbian history and medieval legends had the greatest influence on Bojić's plays; Đorđević notes that "all Bojić's dramas, published or not, are inspired by Serbian medieval motifs".{{sfn|Đorđević|1977|pp=76–77}}

The Balkan Wars inspired Bojić to write his first patriotic poetry, where he attempted to emulate Hugo's rhetorical verse.{{sfn|Đorđević|1977|p=43}} By 1914, much of Bojić's poetry revolved around patriotic themes, and by 1917 this theme had overtaken all others. In his last months, the only non-patriotic poems that Bojić wrote were about the love he felt towards his fiancée.{{sfn|Đorđević|1977|pp=44–45}} The historian John K. Cox opines that Bojić's "personal suffering&nbsp;... [embodied] Serbian history at the crossroads of greatness and disaster."{{sfn|Cox|2002|p=67}}

Cox considers Bojić the most famous of the many Serbian artists and writers who perished during World War I.{{sfn|Cox|2002|p=67}} Đorđević contends that Bojić achieved more during his short career than a number of other writers and poets who led far longer lives. He writes that Bojić's influence on Serbian literature would have been even greater had he survived the war, and praises him as "one of the great poets of the 20th century".{{sfn|Đorđević|1977|pp=79–81}}

In May 2014, the inaugural Milutin Bojić Prize was awarded to a student from [[Čačak]] by the Milutin Bojić Library, a Belgrade-based learning institution dedicated exclusively to studies on Bojić's life and work. The Library is funded by the [[Ministry of Culture (Serbia)|Serbian Ministry of Culture]],<ref>{{cite web|publisher=[[Politika]]|title=Prvi put dodeljena nagrada 'Milutin Bojić'|trans_title=Milutin Bojić Prize first awarded|language=Serbian|date=20 May 2014|url=http://www.politika.rs/rubrike/kultura-i-zabava/Prvi-put-dodeljena-nagrada-Milutin-Bojic.lt.html}}</ref> and operates an extensive online database known as the Milutin Bojić Virtual Library.<ref>{{cite web|publisher=[[B92]]|title=Šta nudi digitalna biblioteka "Milutin Bojić"?|trans_title=What's the Milutin Bojić Digital Library got to offer?|language=Serbian|date=15 December 2014|url=http://www.b92.net/kultura/vesti.php?nav_category=272&yyyy=2014&mm=12&dd=15&nav_id=936381}}</ref> In August 2014, researchers discovered the original manuscript of "Ode to a Blue Sea Tomb" in the Library's archives. The yellowed slip of paper was immediately given to experts at the [[National Library of Serbia]], who went about restoring it so as to prevent further decay.<ref>{{cite web|publisher=[[Radio Television of Serbia]]|title=Pronađen rukopis 'Plave grobnice'|trans_title=Original draft of 'Ode to a Blue Sea Tomb' discovered|language=Serbian|date=11 October 2014|url=http://www.rts.rs/page/stories/sr/%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%B8+%D1%80%D0%B0%D1%82/story/2216/Srbija+u++ratu/1718293/Prona%C4%91en+rukopis+%E2%80%9EPlave+grobnice%22+.html}}</ref>

==References==

===Endnotes===
{{reflist|group=nb}}

===Citations===
{{reflist|30em}}

==Bibliography==
{{refbegin|2}}
* {{cite book
  | last1 = Bojić
  | first1 = Radivoje
  | last2 = Knežević
  | first2 = Radoje
  | authorlink2 = Radoje Knežević
  | year = 1968
  | title = Milutin Bojić: Pesnik Srbije
  | trans_title = Milutin Bojić: Serbia's Poet
  | language = Serbian
  | publisher = Avala Press
  | location = [[Windsor, Ontario]]
  | oclc = 7888777 
  | ref = harv
  }}
* {{cite book
  | last = Cox
  | first = John K.
  | year = 2002
  | title = The History of Serbia
  | publisher = Greenwood Press
  | location = [[Westport, Connecticut]]
  | isbn = 978-0-313-31290-8
  | url = https://books.google.ca/books?id=U765FGDfbPoC
  | ref = harv
  }}
* {{cite book
  | last = Đorđević
  | first = Mihailo
  | year = 1977
  | title = Serbian Poetry and Milutin Bojić
  | publisher = Colombia University Press
  | location = [[New York City|New York]]
  | isbn = 978-0-914710-27-1
  | ref = harv
  }}
* {{cite book
  | last = Judah
  | first = Tim
  | authorlink = Tim Judah
  | year = 2000
  | origyear = 1997
  | edition = 2nd
  | title = The Serbs: History, Myth and the Destruction of Yugoslavia
  | publisher = Yale University Press
  | location = [[New Haven, Connecticut]]
  | isbn = 978-0-300-08507-5
  | url = https://books.google.ca/books?id=B4YbP0fPcMYC
  | ref = harv
  }}
* {{cite book
  | last = Kovijanić
  | first = Gavrilo
  | year = 1969
  | title = Život i književni rad Milutina Bojića
  | trans_title = The Life and Literary Work of Milutin Bojić
  | language = Serbian
  | publisher = Naučna knjiga
  | location = [[Belgrade]]
  | oclc = 16699889 
  | ref = harv
  }}
* {{cite book
  | last = Kurapovna
  | first = Marcia Christoff
  | year = 2009
  | title = Shadows on the Mountain: The Allies, the Resistance, and the Rivalries that Doomed WWII Yugoslavia
  | publisher = John Wiley & Sons
  | location = New Brunswick, New Jersey
  | isbn = 978-0-470-61566-9
  | url = https://books.google.com/books?id=wfy7-5K74gMC
  | ref = harv
  }}
* {{cite book
  | last = Lalić
  | first = Ivan V.
  | year = 1974
  | title = Milutin Bojić
  | language = Serbian
  | publisher = Narodna knjiga
  | location = Belgrade
  | oclc = 499950696 
  | ref = harv
  }}
{{refend}}

==External links==
{{Wikiquote}}
*[http://www.milutinbojic.org.rs/ Milutin Bojić Digital Library ] {{sr icon}}

{{DEFAULTSORT:Bojic, Milutin}}
[[Category:Serbian military personnel of World War I]]
[[Category:Serbian poets]]
[[Category:World War I poets]]
[[Category:1892 births]]
[[Category:1917 deaths]]
[[Category:20th-century deaths from tuberculosis]]