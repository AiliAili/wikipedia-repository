{{good article}}
{{Use British English|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox University Boat Race
| name= 57th Boat Race
| winner = Cambridge
| margin = 20 lengths
| winning_time= 18 minutes 45 seconds
| overall = 24–32
| umpire = [[Frank Willan (rower)|Frank Willan]] <br> (Oxford)
| date= {{Start date|1900|3|31|df=y}}
| prevseason= [[The Boat Race 1899|1899]]
| nextseason= [[The Boat Race 1901|1901]]
}}

The '''57th Boat Race''' took place on 31 March 1900.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge won by twenty lengths in a record-equalling time of 18 minutes 45 seconds, taking the overall record in the event to 32&ndash;24 in Oxford's favour.

==Background==
[[File:Muttlebury SD Vanity Fair 1890-03-22.jpg|right|thumb|[[Stanley Muttlebury]] coached the Cambridge crew.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1899|1899 race]] by three-and-a-quarter lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 20 August 2014}}</ref> while Oxford led overall with 32 victories to Cambridge's 23 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>  Leading up to the race, Oxford suffered a variety of misfortune:  M. C. McThornhill was ordered by his doctor not to row, H. J. Hale was injured and president [[Felix Warre]] contracted [[scarlet fever]].<ref name=drink108/>

Cambridge were coached by James Brookes Close, who had rowed for the Light Blues three times between 1872 and 1874, and [[Stanley Muttlebury]], five-time Blue for Cambridge between 1886 and 1890.  Oxford's coaches were [[Harcourt Gilbey Gold]] (Dark Blue president the previous year and four-time Blue) and [[Douglas McLean (rower)|Douglas McLean]] (an Oxford Blue five times between 1883 and 1887).<ref>Burnell, pp. 110&ndash;111</ref> The umpire for the race for the eleventh year in a row was [[Frank Willan (rower)|Frank Willan]] who had won the event four consecutive times, rowing for Oxford in the [[The Boat Race 1866|1866]], [[The Boat Race 1867|1867]], [[The Boat Race 1868|1868]] and [[The Boat Race 1869|1869 races]].<ref>Burnell, pp. 49, 59</ref>

==Crews==
The Cambridge crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 4.625&nbsp;[[Pound (mass)|lb]] (78.1&nbsp;kg), {{convert|0.25|lb|kg|1}} per rower more than their opponents.<ref name=burn67>Burnell, p. 67</ref>  Oxford's crew contained three members with Boat Race experience: C. E. Johnston, C. W. Tomlkinson and [[Coxswain (rowing)|cox]] G. S. Maclagan.  Cambridge saw six of their 1899 crew return, including [[William Dudley Ward]] and [[Raymond Etherington-Smith|Raymond Broadly Etherington-Smith]], both of whom were rowing in their third race.<ref>Burnell, pp. 66&ndash;67</ref>  Eight of the nine Light Blues were students at [[Trinity College, Cambridge|Trinity College]].<ref name=burn67/> Oxford's [[Stroke (rowing)#Stroke seat|stroke]] [[Henry Hampden Dutton|H. H. Dutton]], a native of South Australia,<ref>{{Cite news | title = Easy win for the Light Blues | work = [[The West Australian]]| date= 2 April 1900 | page = 5}}</ref> was the only non-British participant registered in the race.<ref>Burnell, p. 39</ref>  Author and former Oxford rower George Drinkwater suggested that this year's Cambridge crew, along with the Oxford crew which rowed in the [[The Boat Race 1897|1897 race]], "stand in a class by themselves among University crews."<ref name=drink107>Drinkwater, p. 107</ref>  He also described the Oxford crew as "one of the poorest that ever came from [[the Isis]]".<ref name=drink107/>
[[File:Etherington-Smith RB Vanity Fair 1908-08-05.jpg|right|thumb|upright|[[Raymond Etherington-Smith]] rowed for Cambridge in the number five seat.]]
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[Henry Hampden Dutton|H. H. Dutton]] || [[Magdalen College, Oxford|Magdalen]] || 10 st 9.5 lb || S. P. Cockerell || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 10 lb
|-
| 2 || R. H. Culme-Seymour || [[New College, Oxford|New College]] || 11 st 7.5 lb || C. J. M. Adie || [[Trinity College, Cambridge|1st Trinity]] || 12 st 3 lb
|-
| 3 || C. E. Johnston || [[New College, Oxford|New College]] || 12 st 12 lb || [[Bertram Willes Dayrell Brooke|B. W. D. Brooke]] || [[Trinity College, Cambridge|1st Trinity]] || 11 st 10.25 lb
|-
| 4 || C. W. Tomkinson || [[Balliol College, Oxford|Balliol]] || 11 st 13 lb || J. E. Payne || [[Peterhouse, Cambridge|Peterhouse]] || 13 st 0 lb
|-
| 5 || [[James Grimston, 4th Earl of Verulam|Lord Grimston]] || [[Christ Church, Oxford|Christ Church]] || 13 st 10.75 lb || [[Raymond Etherington-Smith|R. B. Etherington-Smith]] || [[Trinity College, Cambridge|1st Trinity]] || 12 st 11.25 lb
|-
| 6 || H. B. Kittermaster || [[Christ Church, Oxford|Christ Church]] || 14 st 6 lb || [[Ronald Sanderson|R. H. Sanderson]] || [[Trinity College, Cambridge|1st Trinity]] || 12 st 13.25 lb
|-
| 7 || T. B. Etherington-Smith || [[Oriel College, Oxford|Oriel]] || 11 st 5.75 lb || [[William Dudley Ward|W. Dudley Ward]] (P) || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 9 lb
|-
| [[Stroke (rowing)|Stroke]] || C. P. Rowley || [[Magdalen College, Oxford|Magdalen]] || 11 st 12.5 lb || J. H. Gibbon || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 8 lb
|-
| [[Coxswain (rowing)|Cox]] || [[Gilchrist Maclagan|G. S. Maclagan]] || [[Magdalen College, Oxford|Magdalen]] || 8 st 5 lb || [[George Lloyd, 1st Baron Lloyd|G. A. Lloyd]] || [[Trinity College, Cambridge|3rd Trinity]] || 9 st 0 lb
|-
!colspan="7"|Source:<ref name=dodd314>Dodd, p. 314</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref><br>Felix Warre was the Oxford's non-rowing president.
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=burn67/>  In good conditions, umpire Willan got the race under way at 2:00{{nbsp}}p.m. whereupon Cambridge took the lead immediately.  By Craven Steps they were three lengths ahead and continued to draw away from the Dark Blues,<ref name=drink108>Drinkwater, p. 108</ref> to win by 20 lengths in a time of 18 minutes 45 seconds.  It was the fastest winning time in the history of the event, equalling that set by Oxford in the [[The Boat Race 1893|1893 race]].  Although it was the Light Blues' second consecutive victory, it followed a run of nine consecutive wins for Oxford &ndash; overall the Dark Blues led 32&ndash;24.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1900}}
[[Category:1900 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1900 sports events]]
[[Category:1900 in rowing]]