{{Orphan|date=March 2016}}

[[File:James01.jpg|thumb|361x361px|James Ketchell on the completion of his round the world cycle.]]<!-- Do not remove this line! -->

<gallery>
[[File:James everest01.JPG|thumb|2011 challenge of climbing Mt Everest]]
[[File:James everest03.JPG|thumb|Reaching the summit of Mt Everest]]
[[File:James speaking.jpg|thumb|PIC BY STEWART TURKINGTON]]
[[File:Jamescycle01.jpg|thumb|James began his cycle around the world on June 30th, 2013]]
[[File:James row boat.JPG|thumb|Arriving in Antigua after rowing solo across the Atlantic ocean]]
[[File:James01.jpg|thumb|Adventurer, presenter and speaker James Ketchell]]
</gallery>

== James Ketchell ==
James Ketchell (born 11 January 1982), commonly known as 'Captain Ketch', is a British adventurer and Scouting Ambassador. He recently completed what the media dubbed the 'Ultimate Global Triathlon'. Having made numerous television appearances.<ref>{{Cite web
| url = http://www.itv.com/news/london/update/2014-02-02/london-scout-leader-cycles-his-way-into-record-books/
| title = London scout leader cycles his way into record books
| website = ITV News
| access-date = 2016-03-01
}}</ref><ref>{{Cite web
| url = http://www.abc.net.au/news/2015-07-03/british-rowers-reattempt-western-australia-mauritius-crossing/6593906
| title = Rowers leave WA in fresh Mauritius crossing attempt
| website = ABC News
| language = en-AU
| access-date = 2016-03-01
}}</ref> discussing his achievements and recently completing his first book, Ketchell is also currently pursuing a career as a writer, television presenter and documentary journalist.

== Early life ==
Ketchell was born in Basingstoke and educated at [[The Costello School]] formerly known as Harriet Costello Secondary School. Ketchell’s first job was a trainee green keeper at Dummer Golf Club in North Hampshire, he eventually moved into the health and fitness industry some years later.

== Global Triathlon Challenge ==
[[File:James everest03.JPG|thumb|James Ketchell on the summit of Everest]]
On 1 February 2014, Ketchell became the first and only person to have rowed across the [[Atlantic Ocean]], successfully summited [[Mount Everest]] and cycled 18,000 miles around the world.<ref>{{Cite web
| url = http://www.independent.co.uk/news/uk/home-news/serial-adventurer-completes-supertriathlon-after-he-cycled-rowed-and-climbed-across-the-globe-9103771.html
| title = Serial adventurer completes super-triathlon after he cycled, rowed and
| website = The Independent
| language = en-GB
| access-date = 2016-03-01
}}</ref>

Seven years prior to this, he was recovering from a serious motorcycle accident that left him with a broken leg as well as a severely broken and dislocated ankle. The prognosis was that he was likely to suffer a permanent walking impairment and would certainly not be able to continue the active physical lifestyle he had enjoyed up to that point.

Whilst recovering from the accident that left him temporarily unable to walk, Ketchell decided to respond to this crippling situation the only way he knew how, and with dogged determination, set about physical tasks that most of us can only imagine experiencing.

In 2010, Ketchell rowed single-handed across the [[Atlantic Ocean]] in 110 days, four hours and four minutes from La Gomera to [[Antigua]].<ref>{{Cite web
| url = http://sidetracked.com/edition-05/oceanrow.php
| title = James Ketchell - Ocean Rower, High Altitude Climber
| last = JSummerton
| website = sidetracked.com
| access-date = 2016-03-01
}}</ref>

In 2011, on the 16th May, Ketchell reached the summit of [[Mount Everest]]. Upon his descent he succumbed to [[pneumonia]] and spent a week in hospital after arriving back into the UK.<ref>{{Cite web
| url = http://www.hampshire-life.co.uk/people/charity/intrepid_adventurer_james_ketchell_on_rowing_across_the_atlantic_and_summit_everest_1_1646499
| title = Intrepid adventurer James Ketchell on rowing across the Atlantic and summit Everest
| website = Hampshire
| access-date = 2016-03-01
}}</ref>

In 2013, on the 30th June, Ketchell departed Greenwich Park and embarked on an 18,000 mile unsupported global cycle, through 20 countries and cycling on average 100 miles a day.<ref>{{Cite web
| url = http://metro.co.uk/2014/02/02/british-adventurer-completes-super-triathlon-after-cycling-around-the-world-4288386/
| title = British adventurer completes super-triathlon after cycling round the world
| last = Metro.co.uk
| first = Aidan Radnedge for
| website = Metro
| access-date = 2016-03-01
}}</ref>

==  Indian Ocean 2015 ==
Ketchell attempted to row 3,600 miles across the [[Indian Ocean]] from [[Geraldton]] to [[Mauritius]] in 2015 with fellow Scouting Ambassador, Ashley Wilson.<ref>{{Cite web
| url = http://www.abc.net.au/news/2015-07-03/british-rowers-reattempt-western-australia-mauritius-crossing/6593906
| title = Rowers leave WA in fresh Mauritius crossing attempt
| website = ABC News
| language = en-AU
| access-date = 2016-03-01
}}</ref> Their aim was to raise awareness of epilepsy among young people as well as supporting other charities (including Young Epilepsy and [[Scouting|The Scouts]].)

The expedition ended 200 miles off the coast of Western Australia when Ketchell’s rowing partner sustained a serious head injury during a storm and needed to be rescued.<ref>{{Cite news
| url = https://www.theguardian.com/australia-news/2015/jul/08/britons-attempting-to-row-across-indian-ocean-rescued-for-second-time
| title = Britons attempting to row across Indian Ocean rescued for second time
| last = Press
| first = Australian Associated
| date = 2015-07-08
| newspaper = The Guardian
| language = en-GB
| issn = 0261-3077
| access-date = 2016-03-01
}}</ref> A 100,000 ton crude oil tanker called the Dubai Charm came to their rescue.<ref>{{Cite web
| url = http://www.basingstokegazette.co.uk/news/13501549.WATCH___moment_James_Ketchell_was_rescued_from_Indian_Ocean/
| title = WATCH - moment James Ketchell was rescued from Indian Ocean
| website = Basingstoke Gazette
| access-date = 2016-03-01
}}</ref>

== Public and Corporate Speaking ==
[[File:James speaking.jpg|thumb|239x239px|James Ketchell is regularly asked to speak for companies and at schools worldwide]]
Ketchell has been an accomplished speaker for many years and continues to lecture on his experiences around the world.<ref>{{Citation|last = Pushing Limits|title = James Ketchell - Serial Adventurer & Motivational Speaker|date = 2014-04-04|url = https://www.youtube.com/watch?v=IbJ8pdoudTY|accessdate = 2016-03-01}}</ref> To date he has given keynote speeches to many [[List of Fortune 500 computer software and information companies|Fortune 500]] companies including, [[Cisco Systems|Cisco]], [[Mattel]], [[Tata Group]], [[Avnet]] and one of the world's leading military leadership establishments, Royal Military Academy [[Royal Military Academy Sandhurst|Sandhurst]].

== Charity Work ==
Ketchell is an ambassador for children's charity, [[Over the Wall (charity)|Over The Wall]]. Climbing Everest, cycling around the world and attempting to row across the Indian Ocean for good causes. In summer 2016 Ketchell plans to row solo and unsupported around Great Britain in support of [[Over the Wall (charity)|Over The Wall]].<ref>{{Cite web
| url = http://www.otw.org.uk/?gclid=CjwKEAiAmNW2BRDL4KqS3vmqgUESJABiiwDTMpcMb-6MqBVSHCB4w2wX0rp-HDUt3c1uEY4D1ZsJShoCTQHw_wcB
| title = Home - Over The Wall childrens charity - camps for children with serious illness and siblings of children with Cancer, heart disease, sickle cell
| website = Over The Wall childrens charity - camps for children with serious illness and siblings of children with Cancer, heart disease, sickle cell
| language = en-US
| access-date = 2016-03-01
}}</ref>

== Media ==
Ketchell has received extensive coverage in the national and local media for his various challenges. The national press follows his adventures and he has featured in numerous articles in [[The Daily Telegraph|The Telegraph]],<ref>{{Cite web
| url = http://www.telegraph.co.uk/news/worldnews/australiaandthepacific/australia/11617437/A-day-after-attempting-world-record-Indian-Ocean-voyage-British-rowers-rescued-at-sea.html
| title = A day after attempting world record Indian Ocean voyage British rowers rescued at sea
| website = Telegraph.co.uk
| access-date = 2016-03-01
}}</ref> [[The Times]]<ref>{{Cite web
| url = http://www.thetimes.co.uk/tto/news/uk/article4419606.ece
| title = Epileptic cancer patient to race across the ocean {{!}} The Times
| website = The Times
| language = en-GB
| access-date = 2016-03-01
}}</ref> and [[The Guardian]].<ref>{{Cite news
| url = https://www.theguardian.com/australia-news/2015/jul/08/britons-attempting-to-row-across-indian-ocean-rescued-for-second-time
| title = Britons attempting to row across Indian Ocean rescued for second time
| last = Press
| first = Australian Associated
| date = 2015-07-08
| newspaper = The Guardian
| language = en-GB
| issn = 0261-3077
| access-date = 2016-03-01
}}</ref> He has also writes his own blog and has appeared in many magazines including Mens Health<ref>{{Cite web
| url = http://www.menshealth.co.uk/fitness/sports-training/Training-tips-from-the-ultimate-triathlete#!
| title = Training tips from the ultimate triathlete
| website = www.menshealth.co.uk
| access-date = 2016-03-01
}}</ref> and Sidetracked magazine<ref>{{Cite web
| url = http://sidetracked.com/edition-05/oceanrow.php
| title = James Ketchell - Ocean Rower, High Altitude Climber
| last = JSummerton
| website = sidetracked.com
| access-date = 2016-03-01
}}</ref>

== References ==
{{reflist}}
BBC News - http://www.bbc.co.uk/news/uk-26005266

BBC Newsround - http://www.bbc.co.uk/newsround/33714360

BBC – Newsround - http://www.bbc.co.uk/newsround/26006828

ITV News - http://www.itv.com/news/london/update/2014-02-02/london-scout-leader-cycles-his-way-into-record-books/

ITV News Merdian - http://www.itv.com/news/meridian/story/2014-02-01/ultra-triathlon-finish-line-in-sight-for-hampshire-man/

Channel 5 - http://www.channel5.com/shows/5-news/features-archived/after-the-everest-the-atlantic-and-the-world-ultimate-triathlete-reaches-end-of-epic-journey

Metro - http://metro.co.uk/2014/02/02/british-adventurer-completes-super-triathlon-after-cycling-around-the-world-4288386/

Independent - http://www.independent.co.uk/news/uk/home-news/serial-adventurer-completes-supertriathlon-after-he-cycled-rowed-and-climbed-across-the-globe-9103771.html

The Times - http://www.thetimes.co.uk/tto/news/uk/article4419606.ece

The Guardian - https://www.theguardian.com/australia-news/2015/jul/08/britons-attempting-to-row-across-indian-ocean-rescued-for-second-time

The Telegraph - http://www.telegraph.co.uk/news/worldnews/australiaandthepacific/australia/11617437/A-day-after-attempting-world-record-Indian-Ocean-voyage-British-rowers-rescued-at-sea.html

The Telegraph - http://www.telegraph.co.uk/men/active/11550727/Meet-the-men-overcoming-all-odds-to-row-the-Indian-Ocean.html

ABC News - http://www.abc.net.au/news/2015-07-03/british-rowers-reattempt-western-australia-mauritius-crossing/6593906

Adelaide Now - http://www.adelaidenow.com.au/news/national/british-rowers-relaunch-bid-to-row-5800km-from-geraldton-to-mauritius/story-fnii5thp-1227427114877

Basingstoke Gazette - http://www.basingstokegazette.co.uk/news/13501549.WATCH___moment_James_Ketchell_was_rescued_from_Indian_Ocean/

Basingstoke Gazette - http://www.basingstokegazette.co.uk/news/13382569.Basingstoke_adventurer_James_Ketchell_spots_great_white_shark_while_rowing_across_Indian_Ocean/

Basingstoke Gazette - http://www.basingstokegazette.co.uk/news/13315475.False_start_for_Basingstoke_adventurer_James_Ketchell_s_attempt_to_row_across_Indian_Ocean/

Basingstoke Gazette - http://www.basingstokegazette.co.uk/news/11274136.Sports_Hero_Award___James_Ketchell/

Basingstoke Observer - http://www.observergroup.co.uk/captain-ketch-finally-casts-off

Sport 360 - http://sport360.com/article/health-and-fitness/36109/epileptic-ashley-wilson-navigates-across-indian-ocean

Daily Echo - http://www.dailyecho.co.uk/news/12958556.Duo_take_on_tough_rowing_challenge/

The Hindu - http://www.thehindu.com/features/metroplus/addicted-to-adventure/article5048682.ece

Mens Health - http://www.menshealth.co.uk/fitness/sports-training/Training-tips-from-the-ultimate-triathlete#!

Hampshire Life Magazine - http://www.hampshire-life.co.uk/people/charity/intrepid_adventurer_james_ketchell_on_rowing_across_the_atlantic_and_summit_everest_1_1646499

Active Traveller - http://www.active-traveller.com/people/james-ketchell-s-ultimate-triathlon

Sidetracked Magazine http://sidetracked.com/edition-05/oceanrow.php

Opinionated World - http://opinionatedworld.co.uk/james-ketchell-interview-ultimate-triathlon/

== External links ==
* [http://www.jamesketchell.net www.jamesketchell.net]
* [http://www.everest1953.co.uk/james-ketchell Mount Everest James Ketchell Interview]

{{DEFAULTSORT:Ketchell, James}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:British male triathletes]]
[[Category:1982 births]]
[[Category:Living people]]
[[Category:People from Basingstoke]]