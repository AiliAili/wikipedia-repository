<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=205 Club Libelle
 | image= Glasflugel H-205 Club Libelle Glider.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[West Germany]]
 | manufacturer=
 | designer=
 | first flight=
 | introduced=1975
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=171 (as of 1983)
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Glasflügel H201]]
 | variants with their own articles=
}}
|}
The '''Glasflügel 205 Club Libelle''' is a [[high wing]], [[T-tail]]ed, single seat [[Glider (sailplane)|glider]] that was designed and produced in [[West Germany]] by [[Glasflügel]] for club and rental use.<ref name="SD">{{Cite web|url=http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=61 |title=Club Libelle 205 Glasflugel |accessdate=10 July 2011 |last=Activate Media |authorlink= |year=2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20120825220954/http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=61 |archivedate=25 August 2012 |df= }}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 80, [[Soaring Society of America]] November 1983. USPS 499-920</ref>

==Design and development==
[[File:GLASFLUGEL H.205 CLUB LIBELLE vl.jpg|thumb|right|Club Libelle]]
[[File:Glasflugel Club Libelle.jpg||thumb|right|Club Libelle]]
[[File:Club Libelle with retractable landing gear.jpg|thumb|Club Libelle with retractable undercarriage, possibly a unique modification.]]

The [[Glasflügel H-201 Standard Libelle]] proved immensely popular, but was not an optimal aircraft for [[Flying club|club]] and rental use, due to its mid-wing and low tail which could both be damaged in off-airport landings. Also its small cockpit fits only a certain demographic percentage of soaring pilots. The company developed the 205 with the design goals of a simple, rugged aircraft that would withstand club and [[fixed-base operator]] rental use. The resulting aircraft has a high-wing and a T-tail to provide more obstacle clearance as well as a larger cockpit. To eliminate gear-up landings the monowheel landing gear is non-retractable, <ref name="SD" /><ref name="SoaringNov83" /> although at least one Club Libelle exists (D-2468) which is equipped with retractable landing gear.  

The aircraft is constructed from [[fibreglass]]. New wings were designed for the 205 that use a double-taper planform and incorporate combination [[Spoiler (aeronautics)|spoilers]] and [[Flap (aircraft)|flaps]] that occupy two thirds of the wing's [[trailing edge]]. The {{convert|15.0|m|ft|1|abbr=on}} span wing employs a Wortmann FX 66-17A II 182 [[airfoil]], the same as is used on the Standard Libelle and the [[Glasflügel 206 Hornet]].<ref name="SD" /><ref name="SoaringNov83" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 1 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

The Club Libelle was [[Type certificate|type certified]] in [[Germany]] as well as in the [[United States]]. The US certification was granted on 8 September 1975 and includes [[aerobatic]] approval for [[Spin (flight)|spins]], [[Aerobatic maneuver|loops]], [[hammerhead turn]]s and [[lazy eight]]s. Due to its fibreglass construction, the aircraft's certification contains the restriction: "All external portions of the glider exposed to sunlight must be painted white. Registration and competition numbers must be painted blue-gray or in any other light color."<ref name="G12eu">{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/9310864f227884b885256720004c9f39/$FILE/G12eu.PDF|title = Type Certificate Data Sheet SHEET NO.G12EU|accessdate = 10 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=September 1975}}</ref>

==Operational history==
In July 2011 there were six 205s on the United States [[Federal Aviation Administration]] registry.<ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=GLASFLUGEL&Modeltxt=205&PageNo=1|title = Make / Model Inquiry Glasflugal 205
|accessdate = 10 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref>
<!-- ==Aircraft on display== -->

==Specifications (205) ==
[[File:GLASFLUGEL H.205 CLUB LIBELLE hl.jpg|thumb|right|Club Libelle]]
{{Aircraft specs
|ref=Sailplane Directory, Soaring and US Type Certificate G12eu<ref name="SD" /><ref name="SoaringNov83" /><ref name="G12eu" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=15.0
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=105.5
|wing area note=
|aspect ratio=23:1
|airfoil=Wortmann FX 66-17A II 182
|empty weight kg=
|empty weight lb=440
|empty weight note=
|gross weight kg=350
|gross weight lb=
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=38
|stall speed note=with dive brakes closed
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=108
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=35:1 at {{convert|56|mph|km/h|0|abbr=on}}
|sink rate ms=
|sink rate ftmin=110
|sink rate note= at {{convert|42|mph|km/h|0|abbr=on}}
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=6.9
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
* [[List of gliders]]
}}

==References==
{{reflist}}

==External links==
{{Commons category|H205 Club Libelle}}
{{Glasflügel aircraft}}

{{DEFAULTSORT:Glasflugel 205 Club Libelle}}
[[Category:German sailplanes 1970–1979]]
[[Category:Glasflügel aircraft]]