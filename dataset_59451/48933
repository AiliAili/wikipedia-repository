{{Infobox person
| name        = Joseph Hoover Mackin
| image       = <!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt         = 
| caption     = 
|parents      = William David Mackin  <small>(Father)</small> <br /> Catherine Hoover Mackin  <small>(Mother)</small>
| birth_name  = <!--only use if different from name-->
| birth_date  = November 16, 1905
| birth_place = [[Oswego, New York]]
| death_date  = {{Death date and age|1968|08|12|1905|11|16|df=yes}}
| death_place = 
|alma_mater   = [[New York University]]<br/>[[University of Texas at Austin]]
| nationality = [[United States|American]]
| other_names = J. Hoover Mackin
| occupation  = geologist
| known_for   = 
}}

'''Joseph Hoover Mackin''' (November 16, 1905 – August 12, 1968)   was an American geologist.  As a tribute to him, a huge plateau in Antarctica bears the name [[Mackin Table]] and a large lunar crater named ''Mackin'' (originally named ''Mackin-Apollo'') marks the location of the [[Apollo 17]] landing site.<ref>{{cite web|url=https://the-moon.wikispaces.com/Mackin |title=the-moon - Mackin |publisher=The-moon.wikispaces.com |date=2010-08-20 |accessdate=2014-04-19}}</ref>

==Biography==
Mackin was born November 16, 1905, in [[Oswego, New York]], the youngest of seven children of William David Mackin and Catherine Hoover Mackin.  Hoover spent two years immobilized in a cast after being stricken with [[poliomyelitis]] at the age of four, but outgrew the effects of his childhood illness and eventually played football both at Oswego High School and Oswego Normal School.  After graduation in 1924, Mackin entered [[New York University]]. Initially intending to become a journalist, he switched his major to geology after hearing lectures by Professor George I. Finley.

He received the B.S. degree in geology from NYU in 1930 and then entered the graduate school of Columbia University, where he was granted an M.A. degree in 1932.  Mackin accepted an appointment as an instructor at the University of Washington and eventually received a Ph.D. from Columbia in 1936. His teaching career would span thirty-four years—twenty-eight years at Washington and six years as Farish Professor of Geology at the [[University of Texas at Austin]].

==Research==
While Mackin considered himself a geomorphologist, his bibliography reveals far greater scope to his actual research activities. In his doctoral thesis on the origin of surface features of the [[Big Horn Basin]] in Wyoming, he introduced the concept of lateral plantation by a stream essentially at grade, producing gravel-mantled terraces as the stream gradually deepens its valley, as opposed to formation of terraces by stream dissection of earlier alluvial plains. Mackin's further analysis led to publication in 1948 of the classic paper, ''Concept of the Graded River'',<ref>{{cite web|author=J Hoover Mackin |url=http://gsabulletin.gsapubs.org/content/59/5/463 |title=Concept Of The Graded River |publisher=Gsabulletin.gsapubs.org |date=1946-06-03 |accessdate=2014-04-19}}</ref> which has been cited over 700 times. In this paper Mackin refers to the rapidity with which a graded stream responds to artificial changes - a warning of profound importance to stream engineers against altering natural equilibrium by diversion or damming or channel-improvements.

One of Mackin's earliest papers, written with the famous British geologist E. B. Bailey, dealt with the complex folding in the Pennsylvania Piedmont area of the [[Pennsylvania Regions]] and the use of b-lineation in structural analysis. This paper was perhaps the first attempt to apply the concepts of recumbent folding and nappe structure to geologic interpretation of the piedmont. Since then, these concepts have been shown to be widely applicable.  A by-product of the initial study was Mackin's 1950 paper on the "down structure" method of viewing and interpreting geologic maps.

In World War II, Mackin became affiliated with the [[U.S. Geologic Survey]] studying sources of strategic materials. His major effort, which continued after the end of World War II, was on the iron deposits of the [[Iron Springs District]] of Utah and also included, quicksilver deposits near Morton, Washington, and placer deposits containing radioactive minerals in Idaho.  From these studies, Mackin demonstrated beyond reasonable doubt precisely where the iron that forms the major economic deposits of the district came from, how it was separated from the parent body of intrusive quartz [[monzonite]], why it was deposited in adjacent limestone in the particular places now found, and when this process took place in the igneous and structural history of the area. In the course of the study, Mackin demonstrated that in certain types of magmatic flow, [[phenocrysts]] and inclusions become oriented normal, rather than parallel, to the direction of magma movement.  Mackin’s work is considered to have contributed significantly to the understanding of ore deposits and of granite [[tectonics]]. 
 
In addition to teaching and research, Mackin was chairman of the Earth Sciences Division of the [[National Research Council (United States)|National Research Council]] from 1963 to 1965; delegate of the [[National Academy of Sciences]] to the 1967 meetings of the [[International Association of Hydrological Sciences]] in Istanbul and of the [[International Union of Geodesy and Geophysics]] in Zurich in the same year; and the keynote speaker at the Symposium on Pediments, held in Budapest early in 1968.

Mackin was selected as one of several experts to examine samples returned from the Moon.  He participated actively in the early planning and design of the lunar geology experiments as a member of the U.S. Geological Survey team sponsored by the [[National Aeronautics and Space Administration]], and he initiated the idea of a sampling tube to be driven into the lunar soil, which became nicknamed the “Hoov Tube.”

==Honors==
Throughout his career Mackin was a guest lecturer at many universities, and was the Distinguished Lecturer for the [[American Association of Petroleum Geologists]] in 1953 and National Lecturer for [[Sigma Xi]] in 1963. Mackin was a member of the National Academy of Sciences, the [[Geological Society of America]] (Council, 1950-1953; chairman of Cordilleran Section, 1950), the [[Society of Economic Geologists]], the [[American Geophysical Union]], the American Association of Petroleum Geologists, the [[American Association for the Advancement of Science]], and Sigma Xi.

Mackin died on August 12, 1968,  while preparing to serve as delegate of the U.S. National Committee on Geology to the [[International Geological Congress]]  to be held in Prague later in 1968. The University of Texas, where he taught, has an endowment benefiting geosceiences, the J. Hoover Mackin Memorial Scholarship Fund.<ref>{{cite web|url=http://endowments.giving.utexas.edu/page/mackin-j-hoover-mem-schl/1318/ |title=J. Hoover Mackin Memorial Scholarship Fund |publisher=Endowments.giving.utexas.edu |date=1969-03-14 |accessdate=2014-04-19}}</ref> The [[Geological Society of America]] also offers a memorial scholarship for students in [[quaternary geology]] and [[geomorphology]] from the J. Hoover Mackin Award Fund.<ref>{{cite web|url=http://www.gsafweb.org/StudentResearchGrants/jhoovermackin.html |title=J. Hoover Mackin Award Fund |publisher=Gsafweb.org |date= |accessdate=2014-04-19}}</ref>

==References==
{{Reflist}}
 
==Further reading==
* "Memorial to Joseph Hoover Mackin," James Gilluly, Proceedings of the Geological Society of America, 1968, p.&nbsp;206.
* [http://www.nap.edu/html/biomems/jmackin.pdf Joseph Hoover Mackin, 1905—1968, A Biographical Memoir] by Harold L. James. [[National Academies Press]].

{{DEFAULTSORT:Mackin, Joseph Hoover}}
[[Category:Planetary scientists]]
[[Category:American geologists]]
[[Category:1905 births]]
[[Category:1968 deaths]]
[[Category:People with poliomyelitis]]