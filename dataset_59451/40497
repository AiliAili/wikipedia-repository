The '''Harris Tweed Authority''' is an independent [[statutory body|statutory public body]] created by the [[Harris Tweed Act 1993]] replacing the [[Harris Tweed Association]] which formed in 1910.

The Harris Tweed Authority is charged with the general duty of furthering the [[Harris Tweed]] industry as a means of livelihood for those who live in the [[Outer Hebrides]] of [[Scotland]].

The Harris Tweed Authority is responsible for safeguarding the standard and reputation of Harris Tweed, promoting awareness of the cloth internationally and disseminating information about material falling within the definition of Harris Tweed and articles made from it.

In addition, the Harris Tweed Authority is involved in instigating [[litigation]] against [[counterfeiting]] as well as the process of inspections and issuing of the Harris Tweed Orb [[trade mark]].

The authority has its seat in the town of [[Stornoway]] in the [[Isle of Lewis]].

==The Harris Tweed Association 1910-1993==

The Harris Tweed Association was the predecessor of the Harris Tweed Authority and existed from 1910 - 1993 whereupon it was replaced under the terms of the Harris Tweed Act 1993.

At the turn of the 20th century the development of the Harris Tweed industry was growing. Small independent producers, often entrepreneurial general [[merchants]], had largely supplanted the [[landlord]] proprietors in both [[Harris, Scotland|Harris]] and Lewis as middlemen between [[weaving|weavers]] and textile [[wholesalers]] in the south of the UK.<ref>D. Bremner, The Industries of Scotland: their Rise, Progress and Present Condition, 155.</ref>

The role of general merchants as the middlemen in the sale of Harris Tweed was a vital factor in expanding the industry away from the [[patronage]] of the land-owning [[gentry]] and into the hands of island [[entrepreneurs]]. Those merchants who built up a business dealing in tweeds often became independent producers in their own right. They would take orders for Harris Tweed, send the [[yarn]] to their chosen weavers, take back the tweeds for finishing, either locally by hand, or later by some mainland finishing company and finally dispatch the tweed to the customer.

In addition to commissioning tweeds, the general merchants also bought tweed from local weavers, using the [[truck system]] i.e. by giving credit in their store instead of cash. The merchants then sold the tweed to contacts in the south of the country.

A weaver who earned his livelihood from commercial weaving, as opposed to domestic weaving, had to have a ready supply of yarn and often it was only mill-spun yarn bought in from the mainland of Scotland could give him that supply. The great danger of using machine-spun yarn from a mainland mill was that nobody could guarantee that the yarn which came back had been made from the island wool which had been sent to the mill, or even that the yarn was made from 100% pure virgin [[wool]] as was tradition. It was by no means unheard of for unscrupulous spinning mills, particularly in the north of England, to introduce a proportion of re-cycled wool or even [[cotton]] "shoddy", to make the new wool go further.<ref>Gwatkin, 'Notes on the Historical Development of the Harris Tweed Industry and the Part Played by the Harris Tweed Association Ltd., 11.</ref>

As the demand for Harris Tweed expanded in the first decade of the 20th century, there was also an influx of inexperienced weavers into the industry, frequently men who had had to abandon traditional [[fishing]] work due to industry decline.

The result of these two factors saw the increase in poor quality tweed, made by inexperienced weavers from imported, mainland mill-spun yarn and this inferior tweed in turn affected the market for traditional produced Harris Tweed made by experienced weavers from hand-spun island yarn.

It became clear to the local general merchants that strong legal protection of the good name of Harris Tweed by a [[trade mark]] and an established standard definition had therefore become essential to the developing industry. This led to groups of merchants in both Lewis and Harris applying to the [[Board of Trade]] for a registered trade mark.

On 9 December 1909 a group of these merchants joined together to create The Harris Tweed Association Ltd. a company limited by guarantee with a [[registered office]] in London, formed with the intention of protecting the use of the name ‘Harris Tweed’ from imitations, such as the so-called ‘Harris Tweed’ of Henry Lyons or from the inferior standards of production which produced ‘Stornoway Tweed’ and also to establish a Harris Tweed [[certification mark]].<ref>{{cite web|author=Janet Hunter |url=http://www.acairbooks.com/all-products/non-fiction-titles/the-islanders-and-the-orb/prod_49.html |title=All Products - Non-fiction Titles - The Islanders and the Orb - Acair Ltd - Gaelic, English and Bilingual books |publisher=Acairbooks.com |date= |accessdate=2013-03-02}}</ref>

When this trade mark, the Orb, was eventually granted, the Board insisted that it should be granted to all the islands of the Outer Hebrides i.e. to Lewis, North and South [[Uist]], Benbecula and Barra, as well as to Harris, the rationale for this decision being that the tweed was made in exactly the same way in all those islands.<ref>http://www.angusmacleodarchive.org.uk/view/index.php?path=%2F9.+Harris+Tweed%2F4.+Notes+on+the+Harris+Tweed+Industry.pdf</ref>

The Harris Tweed Association existed until 1993 when it was replaced by the Harris Tweed Authority under the terms of the Harris Tweed Act of 1993.

==Harris Tweed Authority 1993–present==
[[File:Stamping of Harris Tweed cloth.jpg|thumb|Stamping Harris Tweed with the Orb Mark]]

The Harris Tweed Authority was established in 1993, replacing the Harris Tweed Association under the terms of the Harris Tweed Act 1993.

In early 1990 the UK was reviewing its trade mark law with the intention of moving towards the single trade mark system for the whole [[European Community]].

The Harris Tweed Association had already faced difficulties presented by different trade mark laws in different countries leaving the association concerned that the new trade mark laws could move direct control of their Orb Mark to the owners of the vested interests of the Harris Tweed companies. This move of control from an independent association to the commercial producers threatened an erosion of Harris Tweed's craft status and connection to the islands of the Outer Hebrides due to inevitable economic pressures to reduce costs and move production elsewhere.<ref>Report by Duncan Martin, HTA Chairman, to Western Isles "Economy In Crisis Conference"</ref>

The association concluded the best option was to transform the association into a public law body, i.e., legal persons governed by public law with statutory functions, one of which would be safeguarding the Orb trade mark.<ref>HTA Minutes. 30. 3. 1990</ref>

Taking a lead from two previous Acts of Parliament, the [[Scotch Whisky Act 1988]]<ref>{{cite web|url=http://www.legislation.gov.uk/ukpga/1988/22/contents |title=Scotch Whisky Act 1988 |publisher=Legislation.gov.uk |date= |accessdate=2013-03-02}}</ref> and the [[Sea Fish Industry Authority]] under the [[Fisheries Act 1981]]<ref>{{cite web|url=http://www.legislation.gov.uk/uksi/1989/1190/contents/made |title=The Fisheries Act 1981 (Amendment) Regulations 1989 |publisher=Legislation.gov.uk |date=2012-01-27 |accessdate=2013-03-02}}</ref> both of which had set out an appropriate mechanism for the protection and promotion of a Scottish product, a proposal was submitted to the [[Department of Trade and Industry (United Kingdom)|Department of Trade and Industry.]]

The proposal included the statutory definition of Harris Tweed outlining the legal remedies it could undertake, an appeals procedure, provision for the dissolution of the Harris Tweed Association Ltd. and for a new Harris Tweed Authority to take over, assuming control of the assets and trademarks of the old association.

A draft bill for a Harris Tweed Act was also drawn up to reflect these proposed changes with the express aim of protecting the intellectual property of Harris Tweed as a local asset to the communities of the Outer Hebrides. By December 1990 the final draft of the bill and been circulated and by April 1991 the eleven members of the Harris Tweed Association unanimously approved the terms subject to such alterations as the [[Parliament of the United Kingdom]] might think fit to make to it.

Readings of the bill took place in early 1991 and, after some procedural difficulties<ref>Supplementary report of the Secretary of State for Scotland on the Harris Tweed Bill, 12. 7. 1991</ref> with regard to European Law, received the Royal Assent in July 1993.<ref>{{cite web|author=Madam Deputy Speaker |url=http://hansard.millbanksystems.com/commons/1993/jul/20/royal-assent#S6CV0229P0_19930720_HOC_466 |title=Royal Assent (Hansard, 20 July 1993) |publisher=Hansard.millbanksystems.com |date=1993-07-20 |accessdate=2013-03-02}}</ref>

After 82 years as voluntary guardian of the Harris Tweed industry and Orb trade marks the Harris Tweed Association became the Harris Tweed Authority, a legal statutory body charged under UK law with safeguarding the industry in the years ahead.

The definition of Harris Tweed contained in the Harris Tweed Act 1993 clearly defines Harris Tweed as a tweed which -

:"(a) has been handwoven by the islanders at their homes in the outer hebrides, finished in the outer hebrides, and made from pure virgin wool dyed and spun in the outer hebrides; and"
:"(b) possesses such further characteristics as a material is required to possess under regulations from time to time in force under the provisions of schedule 1 to the act of 1938 (or under regulations from time to time in force under any enactment replacing those provisions) for it to qualify for the application to it, and use with respect to it, of a harris tweed trade mark."

The act also set out -

:"to make provision for the establishment of a Harris Tweed Authority to promote and maintain the authenticity, standard and reputation of Harris Tweed; for the definition of Harris Tweed; for preventing the sale as Harris Tweed of material which does not fall within the definition; for the Authority to become the successor to The Harris Tweed Association Limited; and for other purposes incidental thereto."

The entire content and provisions of the Act can be found at the [http://www.legislation.gov.uk/ukla/1993/11/pdfs/ukla_19930011_en.pdf Legislation.gov] website managed by the United Kingdom's [[National Archives (United Kingdom)|National Archives]].

==The Orb Mark==

The Harris Tweed Orb Mark is the United Kingdom's oldest certification mark and is recognised all over the world.<ref>{{cite web|url=http://www.ipo.gov.uk/news/newsletters/ipinsight/ipinsight-201210/ipinsight-201210-2.htm |title=Intellectual Property Office - IP Insight, Edition 67, October 2012, Tweed the world |publisher=Ipo.gov.uk |date= |accessdate=2013-03-02}}</ref>

Certification marks are trademarks with a difference. This ancient method of identifying products has its roots in the medieval [[guild system]]. Groups of traders, characterised by profession or location, were recognised through their guild and the reputation that was associated with it, guaranteeing that goods or services meet a defined standard or possess a particular characteristic.

An application was submitted in the name of the Harris Tweed Association Ltd for a standardisation mark, now known as a certification mark, affording much stronger protection that an ordinary trademark would. The trademark was applied for in February 1910 to the Board of Trade as Application No. 319214 under section 62 of the [[Trade Marks Act 1905]] in Class 34 and was finally registered in October 1910.<ref>Mckenna and Co., on behalf of HTA Ltd. to Board of Trade, 23. 2.. 1910, HRA/D 190 I [m]).</ref>

The registered design consisted of a [[Globus cruciger|globe]] surmounted by a [[Maltese Cross]], studded with 13 jewels and with the words "Harris Tweed" in the first line and, in the second line, the words "Made in Harris", "Made in Lewis" or "Made in Uist", according to the place of manufacture.<ref>Report to Board Of Agriculture for Scotland on Home Industries in the Highlands and Islands in 1914, 12.</ref>

Every 58 metre and 75 metre length of Harris Tweed produced by the Harris Tweed mills is inspected by a Harris Tweed Authority inspector and "stamped" with an iron-on transfer of the Orb certification mark as outline above. Typically the mark is applied at the [[selvedge]], one at the corner of each end and one at the half-way point. Customers may request additional marks to be applied at different points also.

The Orb certification mark is also applied to woven labels which are issued to customers when they purchase Harris Tweed.

The Harris Tweed Orb is a registered trademark and must not be used or reproduced without the permission of the Harris Tweed Authority.

==Structure==
[[File:Newalls mill advert.jpg|thumb|Vintage advert for Harris Tweed Orb from Newalls Mill, Stornoway.]]
Based in the island capital of [[Stornoway]] the active office of the Harris Tweed Authority consists of a [[Chief Executive]], Secretary and two Inspectors / Stampers.

There is also an overseeing board of ten unpaid members drawn from the local community who oversee the work of the Authority and meets in Stornoway.<ref>{{cite web|url=http://www.harristweed.org/company/board-of-directors.php |title=Harris Tweed Authority Board Members |publisher=Harristweed.org |date= |accessdate=2013-03-02}}</ref>

* Lorna Macaulay (Chief Executive) - Former Head of Developing Skills and Customer Service with [[Highlands & Islands Enterprise]] in Stornoway and studied Distribution with Fashion Merchandising in Glasgow before taking up posts with the [[Western Isles Council]], the Western Isles Health Board and Western Isles Enterprise.
* Donald Martin (Chairman) - A native of Harris and former chief executive of ''[[Comunn na Gaidhlig]]'' and prior to that worked with ''[[Comhairle nan Eilean Siar]]'' as Depute Director of Administration for over 20 years. He has served on numerous national and local voluntary organisations and joined the Harris Tweed Authority board in 1997. He has been its chairman since 2007 and also serves on the board of [[Lews Castle College]] and its Audit Committee.
* Norman L Macdonald (Vice Chairman) - Fellow of the [[Institute of Financial Accountants]] and has served as a local authority councillor and also been a member of the Stornoway Port Authority. He has been on the Board of the Authority since 1999.
* Catriona Stewart - A ''[[Comhairle nan Eilean Siar]]'' representative on the HTA since 2007.
* Ian Maciver - Solicitor in private practice in Stornoway. From 1972 he has been involved in a professional capacity with a number of Harris Tweed producer organisations negotiating on behalf of mill employees and weavers. Ian has been a Member of the Board since 2007.
* Rhoda Campbell - Owns and manages award winning Blue Reef Cottages. Rhoda joined the authority in 2009 and with a strong marketing and promotions background has brought this expertise to the Board.
* Murdo Morrison - Joined the Board of the Harris Tweed Authority in 1999. He served in the [[Royal Navy]], moving on to be a Training Manager and then PR Manager of an international oil company.
* Angela Mackinnon - Member of the Board since 2002. She lives on the West Side of Lewis and is employed by [[BBC Radio nan Gaidheal]].
* Alasdair Macleod - A ''Comhairle nan Eilean Siar;; representative on the Board since June 2012. He is also a Member of the Highlands and Islands Joint Valuation Board, ''Comunn na Gaidhlig'', ''[[An Lanntair]]'' Arts Centre and an advisor to Gearrannan Blackhouse Village in Carloway.
* Angus Campbell - Managing Director of a number of retail outlets in the Outer Hebrides and is currently Leader of the Western Isles Council. He has been a member of the HTA Board since 2007.
* Dr Calum Macleod - From the Isle of Harris and was appointed to the HTA in 2008. He lives in [[Bridge of Allan]] and is an independent consultant specialising in sustainable rural and regional development.

==Duties and responsibilities==

Under the Harris Tweed Act 1993, the Harris Tweed Authority is charged with the general duty of furthering the Harris Tweed industry as a means of livelihood for those who live in the Outer Hebrides.<ref>http://www.legislation.gov.uk/ukla/1993/11/pdfs/ukla_19930011_en.pdf</ref>

They do this by safeguarding the standard and reputation of, promoting awareness in all parts of the world of, and disseminating information about, material falling within the definition of Harris Tweed and articles made from it.<ref>{{cite web|url=http://www.harristweed.org/about-us/guardians-of-the-orb.php |title=About Us - Guardians of the Orb |publisher=Harristweed.org |date= |accessdate=2013-03-02}}</ref>

Their most important duty is to take steps to ensure that material which does not comply with the Harris Tweed Act's definition of Harris Tweed is not represented as being Harris Tweed including undertaking legal proceedings, in order to defend against infringement or likely infringement any [[intellectual property]] rights associated with the cloth and Orb trade mark.

In addition they seek to prevent, or put a stop to, any person selling, exposing or offering for sale or having in their possession any material which is represented as Harris Tweed but is not Harris Tweed or any garment or other article which is represented as made (wholly or partly) from material which is Harris Tweed when it is not.

Other roles and responsibilities include publicising, through [[advertisements]] or otherwise, the nature, origin and qualities of material falling within the definition of Harris Tweed and registering and maintaining in any part of the world intellectual property rights including patents, trade marks and other marks and designs, and to authorise the user of such intellectual property on such lawful terms and conditions as the Authority may think fit.

The Authority works with a team of lawyers and pursues cases in over 30 countries in which the Orb mark is registered.<ref>{{cite web|url=http://www.harristweed.org/about-us/a-global-presence.php |title=About Us - A Global Presence |publisher=Harristweed.org |date= |accessdate=2013-03-02}}</ref>

The Authority is responsible for inspecting every metre of Harris Tweed produced by the island mills and has two inspectors who visit each mill to inspect the processes and final product and issue the Orb Mark stamps and labels. The inspectors also visit local weavers [[loom]] sheds to ensure their Harris tweed is being woven by hand at all times.

{{As of|2011}} the Harris Tweed Authority has a comprehensive internet presence consisting of a dedicated information website and a range of [[social media]] pages including Facebook,<ref>{{cite web|url=https://www.facebook.com/harristweedauthority |title=Harris Tweed Authority - Stornoway, United Kingdom - Organization |publisher=Facebook |date= |accessdate=2013-03-02}}</ref> Twitter,<ref>{{cite web|url=https://twitter.com/harristweedauth |title=harristweedauthority (@harristweedauth) op Twitter |publisher=Twitter.com |date= |accessdate=2013-03-02}}</ref> Instagram, Vimeo, [[KILTR]]<ref>{{cite web|url=http://www.kiltr.com/harristweedauthority |title=Harris Tweed Authority on KILTR |publisher=Kiltr.com |date=2012-08-21 |accessdate=2013-03-02}}</ref> and Pinterest<ref>{{cite web|url=http://pinterest.com/harristweedauth/ |title=Harris Tweed Authority (harristweedauth) on Pinterest |publisher=Pinterest.com |date= |accessdate=2013-03-02}}</ref> which are used to interact with customers and disseminate news, information and images pertaining to Harris Tweed history, heritage and ongoing activities.

==Litigation==

[[File:Harris Tweed plaid.jpg|thumb|Harris Tweed cloth in vibrant pink plaid check.]]
Since the formation of the Harris Tweed Association in 1909 there have been numerous legal challenges to the definition of genuine Harris Tweed. Great importance is placed on Harris Tweed to the fragile economy of the Outer Hebrides and its historic connection to the islands but the success of the industry has meant that competitors often try to imitate Harris Tweed or "pass off" other fabrics, made elsewhere, as genuine.

Much of these have arisen over issues regarding [[methodology]] and origin of Harris Tweed's manufacture and claim to the name Harris Tweed applied to the cloth.  As such, the Harris Tweed Authority regularly asserts legal action to protect the authenticity of Harris Tweed, curtail counterfeiting and other threats to the industry on behalf of the islanders and local weavers.<ref>{{cite web|url=http://www.bbc.co.uk/news/uk-scotland-scotland-business-17384421 |title=BBC News - Zara apologises for misusing Harris Tweed name |publisher=Bbc.co.uk |date=2012-03-15 |accessdate=2013-03-02}}</ref><ref>{{cite web|last=Davidson |first=Ross |url=http://www.pressandjournal.co.uk/Article.aspx/3083623 |title=Article - Harris Tweed and TK Maxx settle labelling row out of court |publisher=Press and Journal |date=2013-01-16 |accessdate=2013-03-02}}</ref>

Until the 1960s areas of concern were predominantly focussed on cloth claiming to be Harris Tweed that was not being woven at the islander's homes, tweed not being woven by hand, [[yarn]] being spun outside the Outer Hebrides, tweed being finished outside the Outer Hebrides, use of yarn that was not made from 100% Pure New Wool and the selling and marketing of Harris Tweed that had not been inspected or stamped with the Orb certification mark.

===The Independent Harris Tweed Producers Ltd.===
In 1958 three mainland mills interests formed a group called the ‘Independent Harris Tweed Producers Ltd’, they were: Argyll-shire Weaver’s of Oban, A.&J. McNaughton of Pitlochry’ and Scottish Crofters Weavers of Aberdeen or Leith.

McNaughton’s of Pitlochry was a traditional supplier of mainland yarn to Lewis small producers. Scottish Crofters Weavers Ltd. was associated with Robert Laidlaw Ltd Wool Mills of Leith who was also a traditional supplier of mainland yarn to Lewis small producers.

Each of these mills were marketing and selling cloth marked as Harris Tweed but that had not been produced authentically or stamped with the Orb Mark. As such they found themselves excluded from the lucrative American market where the Orb Mark had recently been registered.

Also in 1958 the Independent Harris Tweed Producers Ltd announced a group emblem which was in fact a [[heraldic shield]] registered by Argyll-shire Weavers earlier. This shield was used as a trade mark by the Independent Producers.

By 1958 there were therefore two kinds of Harris Tweed on the market and two associations promoting them: the Orb Harris Tweed promoted by the Harris Tweed Association Ltd. and the Shield Harris Tweed promoted by the Independent Harris Tweed Producers.

In 1962 court proceedings were initiated by the Harris Tweed Association against the Shield producers, in an English Court while in February 1961 court proceedings were initiated by the Shield group in the High Court of Edinburgh, Scotland.

The [[Lord Hunter]] Harris Tweed case in Edinburgh [[High Court]] was between Argyll-shire Weavers and others v A. Macaulay (Tweeds) Ltd. and others.

It was the longest court case in Scottish legal history<ref>http://www.law.du.edu/documents/judge-david-edward-oral-history/2000-trade-marks.pdf</ref> and Lord Hunter finally found against the Shield group. Lord Hunter’s opinion was that, a tweed to be legitimately described and marketed as Harris Tweed had at least to conform to the definition approved by the Board of Trade in relation to Certification Trade Mark No 319214 which is the Orb Trade Mark.<ref>{{Cite journal | url = https://books.google.com/books?id=rXtpDv9pLqEC&pg=PA723&lpg=PA723&dq=lord+hunter+harris+tweed+case#v=onepage&q=lord%20hunter%20harris%20tweed%20case&f=false | title = Contemporary Intellectual Property: Law and Policy | isbn = 9780199263394 | author1 = MacQueen | first1 = Hector L | last2 = Waelde | first2 = Charlotte | last3 = Laurie | first3 = G. Graeme T | date = 2007-10-25}}</ref>

The final decision re-enforced the 1934 definition that tied all production processes only to the Outer Hebrides islands and this finally removed the threat of mainland competition.

==See also==
*[[Harris Tweed]]
*[[Outer Hebrides]]
*[[Tweed (cloth)]]

==Notes==
{{reflist|2}}

==Sources==
* Hunter, Janet:''The Islanders and the Orb'' Acair Ltd. 2001. ISBN 0-86152-736-4

==External links==
*[http://www.harristweed.org/ Harris Tweed Authority]
*[http://www.facebook.com/harristweedauthority/ Harris Tweed Authority Facebook Page]

{{Scotland topics}}
{{fabric}}

[[Category:Organisations based in the Outer Hebrides]]
[[Category:Woven fabrics]]
[[Category:Harris, Outer Hebrides]]
[[Category:Goods manufactured in Scotland]]