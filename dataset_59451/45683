{{Infobox military person
|name=Witold Urbanowicz
|birth_date={{birth date|df=y|1908|3|30}}
|death_date={{death date and age|df=y|1996|8|17|1908|3|30}}
|birth_place= [[Olszanka, Augustów County]], [[Poland]]
|death_place= [[New York City]], [[United States]]
|image=Witold Urbanowicz.jpg
|image_size=250
|caption=Witold Urbanowicz
|nickname=
|allegiance=[[Poland]]
|serviceyears=1930–1946
|rank=[[Air Vice Marshal]]
|branch=
{{plainlist|
*{{air force|Poland}}
*{{air force|United Kingdom}}
}}
|commands=
|battles=
[[World War II]]
*[[Invasion of Poland]]
*[[Battle of Britain]]
*[[Second Sino-Japanese War|Chinese theater]]
|awards=
{{plainlist|
*[[Virtuti Militari]]
*[[Cross of Valour (Poland)|Cross of Valour]]
*[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
*[[Air Medal]] (United States)
*Flying Cross (China)
}}
|laterwork= [[Air Attaché]] (USA)
}}

'''Witold Urbanowicz''' (30 March 1908 – 17 August 1996) was a [[Poland|Polish]] [[fighter ace]] of the [[Second World War]]. According to the official record, Witold Urbanowicz was the second highest-scoring Polish fighter ace, with 17 confirmed wartime kills and 1 probable, not counting his pre-war victory.  He was awarded with several decorations, among others the [[Virtuti Militari]] and British [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]. He also published several books of [[memoir]]s.

==Biography==
Urbanowicz was born in [[Olszanka, Augustów County]]. In 1930 he entered the Szkola Podchorazych Lotnictwa [[cadet]] flying school in [[Dęblin]], graduating in 1932 as a 2/Lt. Observer. He was then posted to the night bomber squadron of the 1st Air Regiment in [[Warsaw]]. Later he completed an advanced pilotage course to become a fighter pilot. In the 1930s he flew with the 113th and No. 111th "Kosciuszko" Squadron.

In August 1936, flying an [[PZL P.11]]a, he shot down a [[Soviet Union|Soviet]] [[reconnaissance plane]] which had crossed into Polish airspace.  He was officially reprimanded and unofficially congratulated by his superior officer and as "punishment" in October 1936 was transferred to an air force training school in [[Deblin]] where he was nicknamed "[[Cobra]]".

===Second World War===
During the [[Invasion of Poland (1939)|Invasion of Poland]] in 1939, Urbanowicz was in an improvised Ulez Group, composed of flying instructors, flying obsolete [[PZL P.7]]a fighters and covering the Dęblin and [[Ułęż]] airfields. Despite a few encounters with enemy airplanes the Polish fighters (which could barely match the speed of German bombers) were not able to shoot down any enemy planes. On 8 September the school was evacuated from Ulez and Dęblin.<ref name="King 339">King 2010, p.339</ref>

He was ordered with the cadets to [[Romania]], where they were told to await re-equipment: [[United Kingdom|British]] and [[France|French]] aircraft were rumoured to have been sent, but no aircraft arrived. Urbanowicz returned to Poland to continue to fight, but after the [[Soviet invasion of Poland (1939)|Soviet invasion on Poland]], he was captured by a Soviet [[irregular military|irregular]] unit. The same day he managed to escape with two cadets and crossed the Romanian border and eventually found his way to France where, after the fall of Poland, a new Polish army was being formed.

While in France he and a group of other Polish pilots were invited to join the [[Royal Air Force]] in [[Great Britain]]. After initial training with 1 School of Army Co-operation at Old Sarum, he was sent to 6 OTU for re-training on fighters in July 1940. In August he was assigned to [[No. 145 Squadron RAF]], and became operational on 4 August 1940. On 8 August he shot down a [[Messerschmitt Bf 110]] Major Joachim Schlichting of V./[[LG 1]] while flying with No. 601 Squadron (although he was never officially attached to the unit) and on 12 August a [[Junkers Ju 88]] of [[KG 51]].<ref name="King 339"/>

On 21 August he was transferred to the Polish-manned [[No. 303 (Polish) Squadron RAF|No. 303 Squadron]], flying a [[Hawker Hurricane]] as "A" Flight commander. On 6 September he shot down a Bf 109 of [[JG 52]]. On 7 September he became [[Squadron Leader]], after [[Zdzisław Krasnodębski]] was badly burned.  On 15 September Urbanowicz claimed two Dornier Do. 17's of [[KG 2]].<ref name="King 339"/>

On 18 September 1940 Urbanowicz was awarded the Silver Cross of the Virtuti Militari by the Commander-in-Chief of the Polish Forces, General [[Władysław Sikorski|Sikorski]]. On 24 October, he was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]].<ref>King 2010, p.338</ref>

On 27 September he was officially credited with shooting down four aircraft: two Ju 88s of [[KG 77]], a Bf 109 and a Bf 110 of 15.[[LG 1]]. On 30 September he claimed three Bf 109s (one from stab.[[JG 26]], one of II./[[JG 53]] and one of 4.[[JG 54]]) and one [[Dornier Do 17]] of [[KG 3]]. Despite his success Urbanowicz was never popular at Polish headquarters and on 21 October he was forced to hand over command of the Squadron to [[Zdzisław Henneberg]].

During the [[Battle of Britain]], he claimed 15 confirmed kills and 1 probable, which made him one of the top Polish aces (second only to [[Stanisław Skalski]]) and in the top ten Allied aces of the battle.

Between 15 April 1941 and 1 June 1941 he commanded the [[1st Polish Fighter Wing]] based at [[RAF Northolt]], before being posted to staff work at [[No. 11 Group RAF]] HQ. In June 1941 he was assigned as the 2nd Air [[Attaché]] in the Polish Embassy in the [[United States]].<ref name="King 339"/>

In September 1943 Urbanowicz joined the [[Fourteenth Air Force|USAAF 14th Air Force]] on attachment in [[China]]. On 23 October he joined the [[75th Fighter Squadron]] of the [[23rd Fighter Group]] ([[Flying Tigers]]). Flying a [[P-40 Warhawk]] he took part in several combat missions. On 11 December  he fought against six Japanese [[Mitsubishi Zero]]s and claimed two shot down (these were actually Nakajima Ki-44 "Tojo" fighters of the 85th Sentai ).<ref name="AotA100">Aircraft of the Aces 100, Ki-44 "Tojo" Aces by Nicholas Millman, Osprey Publishing, p.26</ref>

According to his reports he also shot other airplanes over China, and destroyed some on the ground but those victories were not officially confirmed. According to Kenneth K. Koskodian, he downed 11 Japanese aircraft while being [[Claire Lee Chennault]]'s guest.<ref>Koskodian 2009, p. 98.</ref> He was later awarded the US [[Air Medal]] and a Chinese Flying Cross.

In December 1943 he returned to the [[United Kingdom]] and later became an [[Air Attaché]] in the USA again.<ref name="King 339"/>

During the war none of Witold Urbanowicz's planes were hit by a single enemy bullet.

===After the war===
[[File:Hawker Hurricane gate guardian, RAF Uxbridge.jpg|thumb|right|Hurricane gate guardian at RAF Uxbridge in the colours of Witold Urbanowicz's 303 Squadron aircraft]]
In 1946, he returned to Poland, but was arrested four times by the [[communism|communist]] [[Służba Bezpieczeństwa]] [[secret police]] as a suspected [[spy]]. Fortunately, he was not imprisoned. After his release, he fled to the USA. He lived in [[New York City]] working for [[American Airlines]], [[Eastern Airlines]] and [[Republic Aviation]], retiring in 1973. In 1991 he visited Poland after the fall of communism and again in 1995 when he was promoted to the rank of [[General]]. He died in New York on 17 August 1996.<ref>King 2010, p.340</ref>

A fibreglass Hawker Hurricane gate guardian was unveiled at [[RAF Uxbridge]] in September 2010 in the colours of Urbanowicz's aircraft from the Battle of Britain.<ref>{{cite web |url=http://www.thisislocallondon.co.uk/news/8370646.RAF_services_to_commemorate_Battle_of_Britan/ |title=RAF commemorates Battle of {{sic|nolink=y|Britan}} with services at RAF Uxbridge and Polish War Memorial |date=3 September 2010 |publisher=This is Local London |accessdate=14 June 2011}}</ref>

==Awards==
:[[File:Virtuti Militari Ribbon.png|40px]] [[Virtuti Militari]], Silver Cross (18 September 1940)
:[[File:POL Krzyż Walecznych (1940) 4r BAR.PNG|40px]] [[Cross of Valour (Poland)|Cross of Valour]], four times
:[[File:DistinguishedFlyingCrossUKRibbon.jpg|40px]] [[Distinguished Flying Cross (United Kingdom)]]
:[[File:Air Medal ribbon.svg|40px]] [[Air Medal]] (United States)
:[[Flying Cross]]{{Clarify|date=June 2011}} (China)

==References==
;Citations
{{reflist}}

;Bibliography
* King, Richard. (2010) ''303 (Polish) Squadron Battle of Britain Diary''. Surrey: Red Kite ISBN 978-1-906592-03-5
* Koskodian, Kenneth. K. (2009) ''No Greater Ally''. New York City: Osprey Publishing
* Fiedler, Arkady. Translation by Jarek Garlinski. (2010) ''303 Squadron: The Legendary Battle of Britain Fighter Squadron''. Los Angeles: Aquila Polonica Publishing ISBN 978-1-60772-004-1

==External links==
{{Commons category|Witold Urbanowicz (pilot)}}
* http://www.washingtontimes.com/news/2010/nov/30/how-polish-airmen-succeeded/

{{Use dmy dates|date=April 2012}}

{{Authority control}}

{{DEFAULTSORT:Urbanowicz, Witold}}
[[Category:1908 births]]
[[Category:1996 deaths]]
[[Category:People from Augustów County]]
[[Category:Polish diplomats]]
[[Category:Polish generals]]
[[Category:Polish World War II flying aces]]
[[Category:Silver Crosses of the Virtuti Militari]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Cross of Valour (Poland)]]
[[Category:Royal Air Force officers]]
[[Category:The Few]]
[[Category:Polish emigrants to the United States]]
[[Category:Royal Air Force pilots of World War II]]