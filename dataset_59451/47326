{{multiple issues|
{{peacock|date=December 2012}}
{{Context|date=December 2012}}
{{Orphan|date=December 2012}}
}}
A '''customer value model''' (CVM) is a data-driven representation of the worth, in monetary terms, of what a company is doing or could do for its customers.<ref>Anderson, James C; and Narus, James A, (1998), "Business Marketing: Understanding What Customers Value", Harvard Business Review, March, p 53-65</ref><ref name = "oak">Dupuie, Jeff:  [http://0101.nccdn.net/1_5/35d/340/3cb/OakStone-Using-Customer-Value-Models-to-Improve-B2B-NPD.pdf Using Customer Value Models to Improve B2B New Product Development], OakStone Partners</ref>{{self-published inline|date=November 2011}} Customer value models are tools used primarily in B2B markets where the choice of a given product, service, or offering is based primarily upon the amount customer value created. Customer value is defined as Value = Benefits - Price. Thus, customer benefits are quantified in a CVM - product features and capabilities are translated into dollars. Customer value models are different from [[customer lifetime value]] models, which seek to quantify the value of a customer to its suppliers.

==Firms using customer value models==
Many firms have been reported to use customer value models,<ref>Anderson, James C;  Narus, James A; and van Rossum, Wouter, (2006), "Customer Value Propositions in Business Markets", ''[[Harvard Business Review]]'', March, p 91-99</ref> including [[General Electric]], [[Alcoa]], [[W.W. Grainger]], [[Qualcomm]], [[Sonoco]], BT Industries Group, [[Rockwell Automation]], and [[Akzo Nobel]]..

==Uses of customer value models==

#New product and service development and refinement: The dialog and customer immersion that is part of a CVM is used to discover and determine which potential product features and functionality would create the most value for customers. This on-site interaction can be used to frame and define those features and functionality.  Often a key is to focus on product or service capabilities rather than on features.  Successful CVM efforts change the basis of the customer-supplier product conversation away from features and functions and toward problems, benefits, and value.<ref name = "oak"/><ref>Lindstedt, Per and Berenius, Jan, (2003), "The Value Model: How to Master Product Development and Create Unrivaled Customer Value", Nimba Publishers</ref>
#Sales tools: CVMs can serve as a quantified statement of value and benefits for a customer that is used by the vendor sales staff to both sell into a new account, as well as to reaffirm and validate value created for current customers as a means to retain and grow current customer. CVMs also can help firms to determine the more rational promotion cost.<ref name = "oak"/>

==Customer value model methods==
There are several methods and approaches used to create customer value models.  All of these approaches appear to depend on substantial customer interaction and on-site interviews and observations of customers' challenges related to the product or service being valued.  The CVMs are of varying complexity.  One consulting firm has found it useful to reverse-engineer customer P&Ls (profit and loss statements) to establish a clear connection between the product benefits and the customer bottom-line.<ref name = "oak"/>

== References ==
{{Reflist}}

<!--- Categories --->

[[Category:Articles created via the Article Wizard]]
[[Category:Customer relationship management|Value]]