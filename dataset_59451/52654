{{Infobox journal
| title = Projections
| cover = [[File:Jnl_cover_proj.jpg]]
| editor = Stephen Prince
| discipline = [[Area studies]]
| publisher= [[Berghahn Books]]
| history = 2009-present
| frequency = Biannually
| website = http://journals.berghahnbooks.com/proj/
| ISSN = 1934-9688
}}
'''''Projections''': The Journal for Movies and Mind''  is an [[interdisciplinarity|interdisciplinary]] [[peer-reviewed]] [[academic journal]] that explores the way in which the [[mind]] experiences, understands, and interprets the [[audio-semantic]] and [[narrative structures]] of [[Film|cinema]] and other [[visual media]]. It is published by [[Berghahn Books]] in association with [[The Society for Cognitive Studies of the Moving Image]] and [[The Forum for Movies and Mind]] and it is edited by [[Stephen Prince]]. The journal was the recipient of the 2008 AAP/PSP Prose Award for Best New Journal in Social  Sciences and Humanities.<ref>{{cite web|url=http://www.proseawards.com/current-winners-2009.html |title=Winners |publisher=PROSE Awards |date= |accessdate=2011-09-13}}</ref>

== Indexing and abstracting ==
''Projections'' is indexed and abstracted in:
*[[British Humanities Index]]
*[[GEOBASE (database)|GEOBASE]]
*[[FIAF International Index to Film Periodicals]]
*[[International Bibliography of Book Reviews of Scholarly Literature on the Humanities and Social Sciences]]
*[[International Bibliography of Periodical Literature]]
*[[MLA International Bibliography]]

== References ==
<references />

== External links ==
* {{Official website|http://journals.berghahnbooks.com/proj/}}

[[Category:Film studies journals]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Berghahn Books academic journals]]
[[Category:Publications established in 2009]]