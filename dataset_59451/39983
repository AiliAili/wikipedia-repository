{{Infobox military person
|name= Harrison R. Thyng
|birth_date= {{birth date|1918|4|12}}
|death_date= {{death date and age|1983|9|24|1918|4|12}}
|birth_place= [[Laconia, New Hampshire]]
|death_place= [[Pittsfield, New Hampshire]]
|placeofburial=
|placeofburial_label= Place of burial
|image=Thyng hr.jpg
|caption= Official photo of General Harrison Thyng
|nickname=Harry
|allegiance= [[United States|United States of America]]
|branch= [[File:Seal of the US Air Force.svg|25px]] [[United States Air Force]]
|serviceyears= 1940-1966
|rank= [[File:US-O7 insignia.svg|20px]] [[Brigadier General#United States|Brigadier General]]|commands=
|unit=31st Fighter Group<br/>413th Fighter Group<br/>[[4th Fighter Wing|4th Fighter-Interceptor Wing]]
|battles= [[World War II]]<br/>[[Korean War]]
|awards= [[Silver Star]] (3)<br/>[[Legion of Merit]] (2)<br/>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] (5)<br/>[[Air Medal]] (34)<br/>[[Purple Heart]]
|laterwork=US Senate candidate from New Hampshire
}}
[[Brigadier general (United States)|Brigadier General]] '''Harrison Reed Thyng''' (April 12, 1918 – September 24, 1983) was a [[fighter pilot]] and a [[general officer|general]] in the [[United States Air Force]] (USAF).  He is notable as one of only six USAF fighter pilots to be recognized as an [[Flying ace|ace]] in two wars.  On retiring from the military, Thyng became a [[New Hampshire]] candidate to the [[United States Senate]].

==Biography==
Born in [[Laconia, New Hampshire]], the second of two sons of Herbert and Elizabeth Thyng, "Harry" Thyng was raised in [[Barnstead, New Hampshire|Barnstead]]. He was educated in a rural school system, attending a "one-room" school through the 8th Grade, then attending [[Pittsfield, New Hampshire|Pittsfield High School]]. He was an avid athlete, participating in [[American football|football]], [[baseball]] and [[track and field|track]], in all of which he [[varsity letter|lettered]], and graduated in 1935.

He obtained a [[Bachelor of Arts]] pre-law degree from the [[University of New Hampshire]] in 1939. An ROTC graduate, he was given a reserve commission as a [[second lieutenant#United States|second lieutenant]], [[Infantry]], at graduation but enlisted in the [[U.S. Army Air Corps]] as a flying cadet. He trained at [[Parks College of Engineering, Aviation and Technology|Parks Air College]] near [[East St. Louis, Illinois]], for primary, [[Randolph Air Force Base|Randolph Field]] for basic, and [[Kelly Air Force Base|Kelly Field]] for advanced, where he obtained his wings and commission in the Air Corps on March 23, 1940. His first assignment was as a pursuit pilot with the [[94th Fighter Squadron|94th Pursuit Squadron]], [[1st Operations Group|1st Pursuit Group]], at [[Selfridge Air Force Base|Selfridge Field]], [[Michigan]].

The personnel of the 1st Pursuit Group provided cadre and instructors for new pursuit groups being mobilized by the [[U.S. Army Air Forces]] in preparation for World War II. One of these new groups was the 31st Pursuit Group, the first to be equipped with the [[Bell P-39|Bell P-39 Airacobra]]. On October 10 he was transferred to the [[41st Flying Training Squadron|41st Pursuit Squadron]] of the newly activated [[31st Operations Group|31st Pursuit Group]], then promoted to 1st lieutenant on November 1, 1941.

==World War II==

===Operations in Europe===
After the United States entered the war, several squadrons of the 35th Pursuit Group in the [[Philippines]] became total losses in combat and the newly trained squadrons of the 31st PG, including the 41st PS, were detached on January 15, 1942, to form the core of a new 35th Group and moved to the West Coast for immediate deployment to the Pacific. The U.S. Army Air Forces then created three new squadrons to become the flying units of the 31st fighting Group
On January 30, 1942, [[first lieutenant|1st Lt.]] Thyng became the first commanding officer of the newly created 309th Fighter Squadron, 31st Fighter Group.<ref>"Pursuit" designations officially became "fighter" in the USAAF on May 1, 1942.</ref> Initially equipped with [[Curtiss P-40|Curtiss P-40B Warhawk]] fighters, the 309th FS relocated to [[New Orleans]] to transition to the P-39, and trained during the spring of 1942 for deployment overseas to England. Thyng was promoted to captain on April 4. In May, it staged to [[Manchester-Boston Regional Airport|Grenier Field]], New Hampshire, to train for long-distance over-water flights using [[drop tanks]], for which the P-39 was found to be unsuitable.

The headquarters and ground echelon of the 309th FS shipped out to England on June 4, 1942, aboard {{HMS|Queen Elizabeth|1913|6}} as part of [[Operation Bolero]]. Arriving at its new base at [[High Ercall]] without aircraft on June 11, the squadron began flight training on [[Supermarine Spitfire|Spitfire V]] fighters provided by the [[RAF]] beginning June 26. Its RAF instructors declared the 31st FG ready for operations in late July, the first U.S. combat group to be so rated. On July 26, the group headquarters and its three-squadron commanders, including [[Major (United States)|Major]] Thyng, flew a combat mission with No. 412 Squadron ([[Royal Canadian Air Force|RCAF]]) based at [[RAF Biggin Hill]], a fighter sweep near [[Saint-Omer]], France, that resulted in the loss of one 31st FG Spitfire.

[[File:USAAF Spitfire in UK 309FS 31FG.jpg|thumb|300px|Thyng's Spitfire VB "WZ-A", named ''Mary & James'', in 1942.]]
Thyng's 309th FS was relocated twice, first to [[RAF Warmwell]], [[Dorset]], in late July, and then to [[RAF Tangmere|RAF Westhampnett]], [[Sussex]], on August 4, where it became operational, flying its first operational mission the next day. Its scheduled missions were "Rodeos", [[feint]]s to decoy German fighter opposition, and [[convoy]] escorts, but on August 9 Major Thyng and a wingman flew a defensive patrol over the [[English Channel]] in which Thyng claimed a [[Junkers Ju 88]] damaged, the first claim by a U.S. fighter during the war. His personal aircraft was a Spitfire V he nicknamed ''Mary & James'' after his wife and son, bearing the squadron identification codes WZ—A.

On August 19, 1942, the 31st Fighter Group flew eleven missions and 123 [[sortie]]s in support of [[Operation Jubilee]], the Allied raid on [[Dieppe, France]]. There it encountered its first opposition from [[Luftwaffe]] fighters and recorded its first kills. Thyng was granted a "probable" kill of an [[Fw 190]] and was awarded the [[Silver Star]] for flying top cover for a rescue mission of a downed 31st FG pilot.

On August 20 the 309th FS conducted the first American escort mission for U.S. [[B-17 Flying Fortress]] bombers, and on August 29 the 31st FG flew its first group mission. The 31st flew several days of escort missions for U.S. [[Douglas A-20]] light bombers at the beginning of September, and then stood down from operations except for defensive reactions until a final escort mission on October 2, its last before transferring to the [[Twelfth Air Force]].

On October 26, 1942, the 31st shipped its Spitfires by sea to [[Gibraltar]], to provide air support for [[Operation Torch]] as part of the [[Twelfth Air Force]].

===North African combat===
The advanced command post of the Twelfth Air Force ordered two squadrons of the 31st Fighter Group to fly into Tafaraoui Airfield near [[Oran]], newly captured by the [[U.S. 1st Infantry Division]]. 24 Spitfires of the 308th and 309th FS, including Major Thyng, took off from Gibraltar at 15:40. They arrived in Algeria at 17:00 and observed four aircraft circling overhead, mistakenly identified as [[RAF]] [[Hawker Hurricane]]s. The 12 Spitfires of the 308th FS landed without incident but as the 309th began landing, it was attacked by the four aircraft, now seen to be [[Vichy French]] [[Dewoitine D.520]] fighters. A 309th Spitfire was shot down and its pilot killed. Major Thyng and two other 31st FG pilots counter-attacked and shot down three of the four D.520's. (''USAF Historical Study No. 105, Air Phase of the North African Invasion, November 1942'', Thomas J. Mayock)

The 31st deployed to a forward base at [[Thelepte]], [[Tunisia]], which it temporarily evacuated during the German breakthrough at the [[Battle of the Kasserine Pass]]. Thyng won a second Silver Star attacking German [[armored force]]s during the battle and was shot down twice, once by British [[Anti-aircraft warfare|anti-aircraft fire]]. Suffering a broken ankle during his recovery from the shoot down by the latter, Thyng continued flying with the aid of a sling rigged by his crew chief to enable him to operate the Spitfire's rudder.

Thyng officially was credited with shooting down 4 [[Bf 109]] fighters while commanding the 309th to be recognized as an [[flying ace|ace]] on May 6, 1943.  Thyng, promoted to [[Lieutenant colonel (United States)|lieutenant colonel]] in February, moved up to second-in-command of the 31st Fighter Group n May 12, 1943, and continued operations until wounded in action. Lt. Col. Thyng officially was credited with 162 combat sorties and 5 planes destroyed. Although many unofficial accounts credit him with as many as eight kills, including an Italian fighter, only five are recognized officially by the Air Force (''USAF Historical Study No. 85, USAF Credits for Destruction of Enemy Aircraft, World War II'', Wesley P. Newton et al.).

===Duty in the Central Pacific===
[[File:Republic P-47N Thunderbolt in flight.jpg|left|250px|thumb|Republic P-47N Thunderbolt]]
Thyng was promoted to full colonel at the age of 26 and returned to the United States, where on November 1, 1944, he was made commander of the [[413th Flight Test Group|413th Fighter Group]] at [[Wilmington International Airport|Bluethenthal Field]], [[North Carolina]]. This group, consisting of [[Republic P-47|Republic P-47N Thunderbolt]] fighters, trained for long-range escort operations for [[B-29 Superfortress|B-29]] bombers of the [[Twentieth Air Force]].

On May 19, 1945, the group deployed to the Pacific. It conducted several [[strafing]] missions from [[Saipan]] to the [[Caroline Islands]] in May before beginning operations from [[Ie Shima]] in June. The group engaged in dive-bombing and strafing attacks on factories, radar stations, airfields, small ships, and other targets in Japan, and made several attacks on shipping and airfields in China during July. Thyng's group flew its sole B-29 escort mission on August 8, 1945, to [[Yawata]], Japan.

Col. Thyng is credited with 22 sorties but despite some accounts asserting that he shot down one of the 16 Japanese aircraft credited to his group, he was not awarded any kills in this theater and the credit is likely based on submission of a "probable".  Col. Thyng remained in command of the 413th FG until October 14, 1945.

==USAF career==
Col. Thyng was granted a commission in the [[Regular Army]] in 1946 and in the United States Air Force on September 18, 1947, when that service became an independent arm. From September 1947 through May 1950, he served as an instructor for the [[Air National Guard]] and was instrumental in the founding of the Air Guard in the states of [[Maine]], [[Vermont]], and his home state of [[New Hampshire]].

On June 15, 1950, Col. Thyng was named commander of the 33rd Fighter-Interceptor Group, flying [[F-86|North American F-86 Sabres]] from [[Otis Air Force Base]], [[Massachusetts]], and moved up to command its parent 33rd Fighter-Interceptor Wing in April 1951.

===Korean War duty===
<!-- Deleted image removed: [[File:ColHarrisonThyng.JPG|thumb|right|Col. Harrison Thyng and his F-86 Sabre {{Pufc|1=ColHarrisonThyng.JPG|date=11 February 2013}}]] -->

Thyng deployed to [[Gimpo International Airport|Kimpo Air Base]], [[South Korea]] in October 1951 and while still on unassigned duty recorded his first [[MiG 15]] kill on October 24, 1951, flying with the [[4th Fighter Wing|4th Fighter-Interceptor Wing]]. Leading a flight of [[F-86 Sabre]]s, Thyng attacked a formation of 11 MiGs and hit the leader, causing him to [[ejection seat|eject]]. Thyng was made commander of the wing on November 1, 1951, at a period of time when [[United Nations]] [[air superiority]] over [[North Korea]] was being severely challenged by the [[communist]] forces.

His first severe test as commander came in January 1952 when the activation of a second F-86 wing resulted in a serious shortage of fuel wing tanks and replacement parts, dropping in-commission rates to 55%. Thyng, going over the heads of the [[chain of command]], warned [[Chief of Staff of the United States Air Force|USAF Chief of Staff]] General [[Hoyt Vandenberg]] that "I can no longer be responsible for air superiority in northwest Korea" because of an inability to field sufficient numbers of F-86s to conduct combat operations. The situation was rapidly addressed by the Air Force as a result and in-commission rates rose to greater than 75%.

The spring of 1952 saw a surge in the destruction of MiGs by both F-86 wings in Korea, but particularly in the 4th FIW. Flying with the [[335th Fighter Squadron|335th Fighter-Interceptor Squadron]], Col. Thyng recorded four additional MiG kills to become a jet ace on May 20, 1952, and was awarded his third Silver Star. Kenneth P. Werrell, in his study ''Sabres over MiG Alley'', states that Thyng's kills in March and April likely took place over China. He cites RAF Air Marshal Sir John M. Nicholls KCB CBE DFC AFC, then a [[flight lieutenant]] exchange pilot with the 4th FIW, as stating Thyng sent him low over the primary MiG base at [[Dandong Langtou Airport|Antung]] "to stir them up" and then shot down a reacting MiG-15 after it had taken off. He next quotes USAF Lt. Gen. Charles G. Cleveland, then a 1st lieutenant in the 335th FIS, as being in a flight led by Thyng that resulted in a shoot down north of [[Shenyang|Mukden]], although the claim submission placed the location at the mouth of the [[Yalu River]].

Col. Thyng commanded the 4th FIW through October 2, 1952, and flew 114 missions. Although credited with the destruction of five MiGs, many accounts assert that after his 5th jet credit he began giving claims for his shoot-downs to his [[wingman|wingmen]]. Thyng flew a number of aircraft during his Korean tour, but his personal aircraft was F-86E 50-0623 which carried the nickname ''Pretty Mary and the J's'', after his family, on the lower portion of the nose.<ref>The four "J"s were his children, James, Judy, Joanna, and Jeanie.</ref>

After his return to the United States, Col. Thyng had a succession of assignments with the [[Air Defense Command]] and [[NORAD]]. He served as deputy of operations for the Western Air Defense Area,  as vice commander and commander of an ADC Air Division, and after promotion to [[Brigadier general (United States)|brigadier general]] in May 1963, as vice commander of NORAD North Region at [[CFB North Bay]], [[Ontario]]. He also saw duty in [[The Pentagon|Headquarters USAF]] and with the [[Federal Aviation Agency]].

In 1966, just prior to his retirement, Gen. Thyng observed the testing of air-to-air missiles in [[Southeast Asia]] and flew several combat sorties. He retired from the Air Force on April 1, 1966, to go into politics. Gen. Thyng had over 650 hours of combat flight time on 307 sorties in three wars, with 10 aircraft officially credited shot down and another 6 unofficially attributed to him. Gen. Thyng had operational experience flying the [[Curtiss P-40|P-40]], [[P-39]], [[Supermarine Spitfire|Spitfire]] Vb, [[P-47 Thunderbolt|P-47]]N, [[P-80 Shooting Star|F-80]], [[F-84]], [[F-86 Sabre|F-86]], [[F-89 Scorpion|F-89]], [[F-94 Starfire|F-94]], [[F-100 Super Sabre|F-100]], [[F-102 Delta Dagger|F-102]], and [[F-106 Delta Dart|F-106]] fighter aircraft.

==Politics and retirement==
Thyng and his wife, Mary Evans Thyng, whom he married on March 23, 1940 (the day of his commissioning), retired to Pittsfield, New Hampshire.

In [[United States Senate elections, 1966|1966]] Thyng ran as the [[Republican Party (United States)|Republican Party]] candidate for the [[United States Senate]] seat from New Hampshire held by [[Thomas J. McIntyre]]. He prevailed in a crowded Republican primary that included former governors [[Lane Dwinell]] and [[Wesley Powell]], Party chair William R. Johnson, and [[Doloris Bridges]], widow of 25-year U.S. Senator [[Styles Bridges]].  In the general election, McIntyre was a strong supporter of President [[Lyndon B. Johnson]]'s [[Vietnam War]] policy, neutralizing much of Thyng's appeal as a conservative and a [[War Hawk|Hawk]]. Because of financial support from [[H.L. Hunt]] and others, and his position on the war, Thyng was successfully painted as a far-right candidate and was defeated 54% to 45.9%.

Thyng founded the New England Aeronautical Institute in 1965, which later merged with Daniel Webster Junior College to become [[Daniel Webster College]] in [[Nashua, New Hampshire]], and served as its first president. He retired to [[Pittsfield, New Hampshire]].

On July 17, 2004, a memorial to General Thyng was dedicated in Pittsfield by the Pittsfield Historical Society, with [[United States Senator]] [[Judd Gregg]]; General [[Ronald Fogleman]], former [[Chief of Staff of the United States Air Force]]; Lt Gen [[Daniel James III]], Director of the [[Air National Guard]]; and Maj Gen John Blair, [[New Hampshire National Guard]], in attendance.<ref name="mem">{{cite journal| last = Thyng| first = James| year = 2004| title = It Was a Day to Remember| journal = Sabre Jet Classics| volume = 12| issue = 3| pages =  | url = http://sabre-pilots.org/classics/v123remember| accessdate =August 3, 2009 | quote = }}</ref>

==Awards and decorations==
{|
|-
|[[File:COMMAND PILOT WINGS.png|100px]]
|[[United States Aviator Badge|US Air Force Command Pilot Badge]]
|-
|}
{|
|-
|{{ribbon devices|number=2|type=oak|name=Silver Star ribbon|width=80}}
|[[Silver Star]] with two bronze [[oak leaf cluster]]s
|-
|{{ribbon devices|number=1|type=oak|name=Legion of Merit ribbon|width=80}}
|[[Legion of Merit]] with bronze oak leaf cluster
|-
|{{ribbon devices|number=3|type=oak|name=Distinguished Flying Cross ribbon|width=80}}
|[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] with three bronze oak leaf clusters
|-
|{{ribbon devices|number=0|type=oak|name=Purple Heart BAR|width=80}}
|[[Purple Heart]]
|-
|{{ribbon devices|number=20|type=oak|name=Air Medal ribbon|width=80}}
|[[Air Medal]] with four silver oak leaf clusters
|-
|{{ribbon devices|number=11|type=oak|name=Air Medal ribbon|width=80}}
|[[Air Medal]] with two silver and one bronze oak leaf cluster ''(second ribbon required for accouterment spacing)''
|-
|{{ribbon devices|number=0|type=oak|name=American Defense Service ribbon|width=80}}
|[[American Defense Service Medal]]
|-
|{{ribbon devices|number=0|type=service-star|name=American Campaign Medal ribbon|width=80}}
|[[American Campaign Medal]]
|-
|{{ribbon devices|number=3|type=service-star|name=European-African-Middle Eastern Campaign ribbon|width=80}}
|[[European-African-Middle Eastern Campaign Medal]] with three bronze [[service star]]s
|-
|{{ribbon devices|number=4|type=service-star|name=Asiatic-Pacific Campaign ribbon|width=80}}
|[[Asiatic-Pacific Campaign Medal]] with four bronze [[Service star|campaign stars]]
|-
|{{ribbon devices|number=0|type=oak|name=World War II Victory Medal ribbon|width=80}}
|[[World War II Victory Medal (United States)|World War II Victory Medal]]
|-
|{{ribbon devices|number=0|type=oak|name=Army of Occupation ribbon|width=80}}
|[[Army of Occupation Medal]]
|-
|{{ribbon devices|number=4|type=service-star|name=KSMRib|width=80}}
|[[Korean Service Medal]] with four bronze service stars
|-
|{{ribbon devices|number=0|type=oak|name=United Nations Service Medal for Korea Ribbon|width=80}}
|[[United Nations Korea Medal|United Nations Service Medal for Korea]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Croix de guerre 1939-1945 with palm (France) - ribbon bar.png|width=80}}
|French [[Croix de guerre 1939–1945|Croix de Guerre]] with bronze palm
|}
{|
|-
|{{Ribbon devices|number=5|type=oak|ribbon=Air Force Longevity Service ribbon.svg|width=80}}
| [[Air Force Longevity Service Award]] with one silver leaf cluster
|-
|}

[[File:Presidential Unit Citation (Korea).svg|80px]]&nbsp;&nbsp;[[Republic of Korea Presidential Unit Citation]]

[[File:United Nations Korea Medal ribbon.svg|80px]]&nbsp;&nbsp;[[United Nations Service Medal]]

[[File:Korean War Service Medal ribbon.svg|80px]]&nbsp;&nbsp;[[Korean War Service Medal]]

==Aerial victory credits==
Gen. Thyng is one of six USAF pilots and seven U.S. pilots overall who achieved ace status as both a piston-engined pilot in World War II and as a jet pilot in a later conflict (the others are Col. [[Francis S. Gabreski]], Col. [[James P. Hagerstrom]], Major [[William T. Whisner]], Col. [[Vermont Garrison]], Major [[George A. Davis, Jr.]], and Lt.Col. [[John F. Bolt]], USMC), and the only one to achieve [[flag rank|flag-general officer rank]].  His credited victories:
{|align=center class="wikitable" style="width:600px;"
|-style="color:white;"
! style="background-color: #3399ff"|Date !! style="background-color: #3399ff"|Type !! style="background-color: #3399ff"|Location !! style="background-color: #3399ff"|Aircraft flown !! style="background-color: #3399ff"|Unit
|-
|November 8, 1942||align=center|[[Dewoitine D.520]]||align=center|[[Oran|Tafaraoui]], [[Algeria]]||align=center|[[Spitfire|Spitfire Vb]]||align=center|309 FS, 31 FG
|-style="background: #eeeeee;"
|February 15, 1943||align=center|[[Me 109]]||align=center|[[Thelepte]], [[Tunisia]]||align=center|Spitfire Vb||align=center|309 FS, 31 FG
|-
|March 29, 1943||align=center|Me 109||align=center|[[Tunisia]]||align=center|Spitfire Vb||align=center|309 FS, 31 FG
|-style="background: #eeeeee;"
|April 1, 1943||align=center|Me 109||align=center|Tunisia||align=center|Spitfire Vb||align=center|309 FS, 31 FG
|-
|May 6, 1943||align=center|Me 109||align=center|[[Tunis]], Tunisia||align=center|Spitfire Vb||align=center|309 FS, 31 FG
|-style="background: #eeeeee;"
|October 24, 1951||align=center|[[MiG 15]]||align=center|[[North Korea]]||align=center|[[F-86 Sabre|F-86]]E||align=center|4 FIW
|-
|December 14, 1951||align=center|MiG 15||align=center|North Korea||align=center|F-86E||align=center|4 FIW
|-style="background: #eeeeee;"
|March 10, 1952||align=center|MiG 15||align=center|[[Dandong]], China?||align=center|F-86E||align=center|335 FIS, 4 FIW
|-
|April 18, 1952||align=center|MiG 15||align=center|[[Shenyang]], China?||align=center|F-86E||align=center|335 FIS, 4 FIW
|-style="background: #eeeeee;"
|May 20, 1952||align=center|MiG 15||align=center|North Korea||align=center|F-86E||align=center|4 FIW
|-
|colspan="5" style="font-size:90%;" |'''Sources:''' ''Air Force Historical Study 85: USAF Credits for the Destruction of Enemy Aircraft, World War II'' and ''Air Force Historical Study 81: USAF Credits for the Destruction of Enemy Aircraft, Korean War''
|}

==References==
{{Reflist}}

==Further reading==
* [[Roger A. Freeman|Freeman, Roger A.]] ''The Mighty Eighth'' (1993 edition). ISBN 0-87938-638-X
*Freeman, Roger A. ''The Mighty Eighth War Diary'' (1990). ISBN 0-87938-495-6
* {{cite book
 |last = Werrell
 |first = Kenneth P.
 |authorlink =
 |year = 2005
 |title = Sabres over MiG Alley
 |publisher = Naval Institute Press
 |location =
 |isbn = 1-59114-933-9
}}

==External links==
{{Commons category}}
{{Portal|United States Air Force}}
* [http://www.pittsfield-nh.com/HTM%20Pages/page41.htm Dedication Brigadier Harrison Thyng Memorial July 17, 2004]
* [http://www.unh.edu/army/AlumniPage/pages/Thyng.html University of New Hampshire ROTC Hall of Fame biography]
* {{webarchive |date=2012-12-12 |url=http://archive.is/20121212041549/http://www.af.mil/information/bios/bio.asp?bioID=7386 |title=Air Force Link, Official Biography}}
* [http://www.nationalmuseum.af.mil/factsheets/factsheet_media.asp?fsID=1138 National Museum of the United States Air Force site, photographs]
* [http://sabre-pilots.org/classics/v101thyng.htm "Harrison R. Thyng", Larry Davis and James Thyng, ''Sabrejet Classics'' (Winter 2002) 16-17]
* [http://www.31stfightergroup.com/31stReference/history/309th.html 309th Fighter Squadron history]
* [http://www.star-games.com/exhibits/spitfire/spitops.html USAAF Spitfire Operations in the Mediterranean Theater, Air University paper]
* [http://www.airforce-magazine.com/MagazineArchive/Pages/1989/January%201989/0189valor.aspx "A Thyng of Valor", ''Air Force Magazine'', January 1989, Vol. 72 No. 1]

{{Authority control}}

{{DEFAULTSORT:Thyng, Harrison}}
[[Category:American military personnel of World War II]]
[[Category:American Korean War flying aces]]
[[Category:American World War II flying aces]]
[[Category:Aviators from New Hampshire]]
[[Category:People from Belknap County, New Hampshire]]
[[Category:Recipients of the Silver Star]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Croix de guerre (France)]]
[[Category:Recipients of the Air Medal]]
[[Category:United States Air Force generals]]
[[Category:1918 births]]
[[Category:1983 deaths]]
[[Category:New Hampshire Republicans]]