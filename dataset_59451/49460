{{Infobox software
| name                   = Razor
| logo                   = 
| screenshot             = 
| caption                = 
| developer              = [[Microsoft]]
| released               = {{start date and age|2010|06}}
| latest_release_version = 3.2.3
| latest_release_date    = {{start date and age|2015|02|09}}<ref>{{cite web |title=Microsoft ASP.NET Razor |url=https://www.nuget.org/packages/Microsoft.AspNet.Razor/ |website=[[NuGet]]}}</ref>
| latest preview version = 4.0.0-rc1
| latest preview date    = {{Start date and age|2015|11|18}}
| genre                  = [[Web application framework]]
| license                = [[Apache License|Apache 2.0]]<ref>{{cite web|url=https://github.com/aspnet/Razor/blob/dev/LICENSE.txt|title=Razor/LICENSE.txt at dev · aspnet/Razor · GitHub|work=GitHub}}</ref>
| programming language   = [[C Sharp (programming language)|C#]], [[Visual Basic .NET|VB.NET]], [[Html|HTML]]
| operating system       = [[Microsoft Windows]]
| website                = {{URL|http://www.asp.net/web-pages}}
}}
{{Infobox file format
| name                   = Razor File Formats
| icon                   = 
| logo                   = 
| extension              = .cshtml, .vbhtml
| mime                   = text/html
| magic                  = 
| owner                  = [[Microsoft]]
| released               = <!-- {{Start date|YYYY|mm|dd|df=yes/no}} -->
| latest release version = 
| latest release date    = <!-- {{Start date and age|YYYY|mm|dd|df=yes/no}} -->
| genre                  = 
| container for          = 
| contained by           = 
| extended from          = 
| extended to            = 
| standard               = 
| free                   = 
| url                    = 
}}
'''Razor''' is an [[ASP.NET]] programming syntax used to create [[dynamic web page]]s with the [[C Sharp (programming language)|C#]] or [[Visual Basic .NET]] programming languages. Razor was in development in June 2010<ref name="IntroducingRazor">{{cite web|url=http://weblogs.asp.net/scottgu/archive/2010/07/02/introducing-razor.aspx|title=ScottGu's Blog - Introducing "Razor" – a new view engine for ASP.NET|work=asp.net}}</ref> and was released for Microsoft Visual Studio 2010 in January 2011.<ref name="RazorsyntaxSupport">{{cite web|url=http://blogs.msdn.com/b/webdevtools/archive/2011/01/12/how-to-get-razor-syntax-support-in-visual-studio-2010.aspx|title=MSDN Blogs|publisher=Microsoft|work=msdn.com}}</ref> Razor is a simple-syntax view engine and was released as part of [[ASP.NET MVC|MVC]] 3 and the [[WebMatrix]] tool set.<ref name="RazorsyntaxSupport" />

== Design==
The Razor syntax is a template markup syntax, based on the C# programming language, that enables the programmer to use an HTML construction workflow.{{clarify|date=June 2013}} Instead of using the ASP.NET Web Forms (.aspx) markup syntax with <code><%= %></code> symbols to indicate code blocks, Razor syntax starts code blocks with a <code>@</code> character and does not require explicit closing of the code-block.

The idea behind Razor is to provide an optimized syntax for HTML generation using a code-focused templating approach, with minimal transition between HTML and code.<ref name=MVC3rve>{{cite web|url=http://www.asp.net/mvc/videos/mvc-3/mvc-3-razor-view-engine|title=MVC 3 - Razor View Engine|author=Jon Galloway|work=The Official Microsoft ASP.NET Site}}</ref> The design reduces the number of characters and keystrokes, and enables a more fluid coding workflow by not requiring explicitly denoted server blocks within the HTML code.<ref name="IntroducingRazor" /> Other advantages that have been noted:<ref name=pro1>{{cite web|url=https://stackoverflow.com/questions/1451319/asp-net-mvc-view-engine-comparison|title=ASP.NET MVC View Engine Comparison|work=stackoverflow.com}}</ref>
* Supports [[IntelliSense]] – statement completion support
* Supports "layouts" – an alternative to the "master page" concept in classic [[ASP.NET|Web Forms]] (.aspx)
* [[Unit testing|Unit testable]]

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.asp.net/web-pages/overview/getting-started/introducing-aspnet-web-pages-2/getting-started Getting Started with WebMatrix and ASP.NET Web Pages]
* [http://www.asp.net/mvc ASP.NET Model View Controller]
* {{GitHub|aspnet/Razor}}

{{.NET Framework}}
{{Web frameworks}}

[[Category:ASP.NET|Razor]]
[[Category:Template engines]]
[[Category:Web frameworks]]
[[Category:Microsoft application programming interfaces]]
[[Category:Microsoft Visual Studio]]