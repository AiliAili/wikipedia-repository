{{Infobox journal
| title = Journal des sçavans
| cover = [[File:1665 journal des scavans title.jpg|200px]]
| editor =
| discipline = 
| country = France
| language = French
| frequency = 
| history = 
| openaccess = 
| license =
| impact = 
| impact-year = 
| website = http://www.persee.fr/web/revues/home/prescript/revue/jds
| link2 =
| link2-name =
| JSTOR =
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN =
| eISSN = 1775-383X
}}
The '''''Journal des sçavans''''' (later renamed '''''Journal des savants'''''), established by [[Denis de Sallo]], was the earliest [[academic journal]] published in Europe. Its content included obituaries of famous men, church history, and legal reports.<ref name="Dibner">[http://www.sil.si.edu/libraries/Dibner/newacq_2000.htm The Amsterdam printing of the Journal des sçavans], Dibner Library of the Smithsonian Institution</ref> The first issue appeared as a twelve-page [[quarto]] [[pamphlet]]<ref>Brown, 1972, p. 368</ref> on Monday, 5 January 1665.<ref>Hallam, 1842, p. 406.</ref> This was shortly before the first appearance of the ''[[Philosophical Transactions of the Royal Society]],'' on 6 March 1665.<ref>''Philosophical Transactions of the Royal Society'' Vol.1, Issue 1, is dated March 6, 1665. See also ''History of the Journal'' at http://publishing.royalsociety.org/index.cfm?page=1244</ref> The 18th-century French physician and [[Encyclopédistes|encyclopédiste]] [[Louis-Anne La Virotte]] (1725–1759) was introduced to the journal through the protection of chancellor [[Henri François d'Aguesseau]].

The journal ceased publication in 1792, during the [[French Revolution]], and, although it very briefly reappeared in 1797 under the updated title ''Journal des savants'', it did not re-commence regular publication until 1816. From then on, the ''Journal des savants'' was published under the patronage of the Institut de France. From 1908, it was published under the patronage of the Académie des Inscriptions et Belles-Lettres. It continues to be a leading academic journal in the Humanities.

==References==
{{Reflist}}

==Further reading==
*Brown, Harcourt (1972). "History and the Learned Journal". ''Journal of the History of Ideas'', '''33'''(3), 365–378.
*Hallam, Henry (1842). ''Introduction to the Literature of Europe in the Fifteenth, Sixteenth, and Seventeenth Centuries''. Harper & Brothers.
*[[Ioan James|James, Ioan]] (2004). ''Remarkable Physicists: From Galileo to Yukawa''. Cambridge: Cambridge University Press. ISBN 0-521-01706-8
*Kilgour, Frederick G. (1998). ''The Evolution of the Book''. New York: Oxford University Press. ISBN 0-19-511859-6

==External links==
{{Commons}}
*[http://gallica.bnf.fr/ark:/12148/cb343488023/date ''Journal des sçavans''] at [[Gallica]]
*[http://www.persee.fr/web/revues/home/prescript/revue/jds The modern ''Journal des Savants'']
{{Authority control}}
{{DEFAULTSORT:Journal Des Scavans}}
[[Category:French-language journals]]
[[Category:Publications established in 1665]]
[[Category:French literary magazines]]
[[Category:Multidisciplinary academic journals]]
[[Category:1665 establishments in France]]