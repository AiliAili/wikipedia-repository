{{Use dmy dates|date=March 2015}}
{{Use British English|date=March 2015}}
{{Infobox military award
| name             = British War Medal
| image            = [[File:WW1 British War Medal.jpg|300px]]
| caption          = 
| awarded_by       = [[George V|the Monarch of the United Kingdom and the British Dominions, and Emperor of India]]
| country          = [[File:Flag of the United Kingdom.svg|x14px]] [[United Kingdom]]
| type             = [[Campaign medal]]
| eligibility      = British and Imperial forces
| for              = Campaign service
| campaign         = First World War
| status           = 
| description      = 
| clasps           = 
| established      = 26 July 1919
| first_award      =
| last_award       =
| total            = 6,500,000 silver<br>110,000 bronze
| posthumous       =
| recipients       =
| precedence_label = Order of wear
| individual       =
| higher           = [[1914–15 Star]]
| same             =
| lower            = [[Victory Medal (United Kingdom)]]<br>[[Victory Medal (South Africa)]]
| related          = 
| image2           = [[File:Ribbon - British War Medal.png|x29px]]
| caption2         = Ribbon bar
}}

The '''British War Medal''' is a campaign medal of the [[United Kingdom]] which was awarded to officers and men of British and Imperial forces for service in the [[First World War]]. Two versions of the medal were produced. About 6.5 million were struck in silver and 110,000 were struck in bronze, the latter for award to Chinese, Maltese and Indian Labour Corps.<ref name="IWM"/>

==Institution==
The British War Medal was instituted on 26 July 1919 for award to those who had rendered service between 5 August 1914, the day following the British declaration of war against the [[German Empire]], and the [[armistice of 11 November 1918]], both dates inclusive. Consideration was given to the award of clasps to commemorate certain battles and theatres of operations and some 68 clasps were proposed for Naval recipients and 79 for the Army. While the Naval clasps were authorised in August 1920, none were awarded and the idea was abandoned in 1923.<ref name="IWM"/><ref name="NewZealand"/><ref name="Canada">[http://www.veterans.gc.ca/eng/remembrance/medals-decorations/campaign-stars-medals-1866-1918/bwm Veterans Affairs Canada – British War Medal] (Access date 29 March 2015)</ref>

==Award criteria==
Unlike the [[1914 Star]] and the [[1914–15 Star]], the British War Medal could be awarded to all officers and men of British and Imperial forces who had served for a prescribed period during any stage of the war, or who had died on active service before the completion of this period. Eligibility for the award of the medal was subsequently extended to cover service in 1919 and 1920 in mine-clearing at sea as well as participation in operations in North and South Russia, the eastern [[Baltic region]], [[Siberia]], the [[Black Sea]] and the [[Caspian Sea]].<ref name="IWM"/><ref name="NewZealand">[http://medals.nzdf.mil.nz/category/h/h6.html New Zealand Defence Force – British Commonwealth war and campaign medals awarded to New Zealanders – The British War Medal] {{webarchive |url=https://web.archive.org/web/20070615113743/http://medals.nzdf.mil.nz/category/h/h6.html |date=15 June 2007 }} (Access date 29 March 2015)</ref><ref>Mackay, J and Mussel, J (eds) – ''Medals Yearbook – 2006'', (2005), Token Publishing. p169.</ref>

===Navy===
For the [[Royal Navy]], [[Royal Marines]] and the Dominion and Colonial naval forces, the criteria were 28 days mobilised service, but without a requirement for overseas service. The medal was presented to the next-of-kin of all casualties, including those who were killed before the completion of this 28 days period. The medal was also awarded, with the same criteria, to members of the [[Women's Royal Naval Service]], to members of Queen Alexandra's Royal Naval Nursing Service and Royal Naval Nursing Service Reserve, and to a number of non-Naval personnel who served on Royal Navy ships, such as canteen and medical staff.<ref name="IWM"/>

===Army===
Officers and men of the [[British Army]], including Dominion and Colonial forces, were required to have either entered an active theatre of war or to have left the United Kingdom for service overseas between 5 August 1914 and 11 November 1918, and to have completed 28 days mobilised service. The medal was also awarded in the event of death on active service before the completion of the prescribed period. The same criteria for eligibility were applied to members of the Women's Auxiliary Forces and staff of officially recognised military hospitals and members of recognised organisations such as the [[British Red Cross]] and the [[Order of Saint John (chartered 1888)|Order of Saint John]] who actually tended the sick and wounded.<ref name="IWM">[http://archive.iwm.org.uk/server/show/ConWebDoc.1231 Imperial War Museum – British War Medal 1914–1920] (Access date 29 March 2015)</ref>

===Air Force===
For the Air Forces, the [[Royal Naval Air Service]] and the [[Royal Flying Corps]] which were amalgamated into the new [[Royal Air Force]] towards the end of the war on 1 April 1918, eligibility was broadly the same as for the British Army. It required overseas service, but members of the Air Forces who had seen combat whilst based in the United Kingdom, who had ferried aircraft to France or who had served on ships carrying aircraft were eligible for the award of the medal.<ref name="IWM"/><ref>{{cite web |url=https://www.raf.mod.uk/history/raftimeline19181929.cfm |title=RAF Timeline 1918–1929 |year=2011 |publisher=Royal Air Force |accessdate=15 August 2012}} (Access date 29 March 2015)</ref>

==Order of wear==
Campaign medals and stars are not listed by name in the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], but are all grouped together as taking precedence after the [[Queen's Medal for Chiefs]] and before the [[Polar Medal]]s, in order of the date of the campaign for which awarded.<ref name="London Gazette"/>

The order of precedence of the First World War campaign stars and medals is as follows:<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3352|date=17 March 2003}}</ref>
* The [[1914 Star]].
* The [[1914–15 Star]].
* The '''British War Medal'''.
* The [[Victory Medal (United Kingdom)]].
* The [[Territorial War Medal]].
* The [[Mercantile Marine War Medal]].

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African orders, decorations and medals awarded to South Africans on or after that date. Of the official British campaign medals which were applicable to South Africans, the British War Medal takes precedence as shown.<ref name="London Gazette"/><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 – ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:Ribbon - 1914 Star.png|x37px|1914–15 Star]] [[File:Ribbon - British War Medal.png|x37px|British War Medal]] [[File:Ribbon - Victory Medal.png|x37px|Victory Medal (South Africa)]]
* Preceded by the [[1914-15 Star]].
* Succeeded by the [[Victory Medal (South Africa)]].

==Description==
The medal is a silver or bronze disk, 36 millimetres in diameter, with a straight clasp suspender without swivel. The medal was designed by W. McMillan and struck by the Royal Mint. The recipient's name, rank, service number and unit are impressed on the bottom edge of the medal. On medals awarded to Army officers, with the exception of the Royal Artillery, the name of the regiment or corps was omitted.<ref name="IWM"/>

;Obverse
The obverse shows Sir Bertram Mackennal's bareheaded effigy of [[George V|King George V]] facing left, with the legend "GEORGIVS V BRITT: OMN: REX ET IND: IMP:" (George V, King of all the British Isles and Emperor of India).<ref name="IWM"/><ref name="NewZealand"/>

;Reverse
The reverse shows [[Saint George]] naked on horseback and armed with a short sword, an allegory of the physical and mental strength which achieved victory over [[Prussianism]]. The horse tramples on the [[Kingdom of Prussia|Prussian]] eagle shield and the emblems of death, a skull and cross-bones. In the background are ocean waves and just off-centre near the right upper rim is the risen sun of Victory. The years "1914" and "1918" appear on the perimeter in the left and right fields respectively.<ref name="IWM"/><ref name="NewZealand"/><ref name="PipSqueak"/>

;Ribbon
The [[Moire (fabric)|watered]] silk ribbon is 32 millimetres wide, with a 3 millimetres wide royal blue band, a 2 millimetres wide black band and a 3 millimetres wide white band, repeated in reverse order and separated by a 16 millimetres wide orange band. The colours are not thought to have any particular symbolic significance.<ref name="IWM"/><ref name="NewZealand"/><ref name="PipSqueak"/>

==Recipients==
Some 6,610,000 medals were awarded, 6,500,000 struck in silver and 110,000 struck in bronze. The bronze medals were mostly awarded to Chinese, Maltese and Indians who served in the [[labour battalion]]s of such organisations as the [[Chinese Labour Corps]].<ref name="IWM"/><ref name="NewZealand"/><ref name="Archives">[http://www.nationalarchives.gov.uk/documentsonline/medals.asp The National Archives – British Army medal index cards 1914–1920] (Access date 27 March 2015)</ref>

Altogether 427,993 medals were awarded to Canadians who served in the [[Canadian Expeditionary Force]].<ref name="Canada"/>

==Nicknames==
The trio of First World War medals, either one of the [[1914 Star]] or the [[1914-15 Star]], the British War Medal and the [[Victory Medal (United Kingdom)|Victory Medal]], were collectively irreverently referred to as ''[[Pip, Squeak and Wilfred]]'', after three comic strip characters, a dog, a penguin and a rabbit, which were popular in the immediate post-war era. ''Pip'' represented either of the two Stars, ''Squeak'' represented the British War Medal and ''Wilfred'' represented the Victory Medal.<ref name="PipSqueak">{{cite web|url=http://www.1914-1918.net/pipsqueak.htm |title=Pip, Squeak and Wilfred |accessdate=26 June 2008 |work=The Long, Long Trail |date= |archiveurl=https://web.archive.org/web/20080227010804/http://www.1914-1918.net/pipsqueak.htm |archivedate=27 February 2008 |deadurl=yes |df=dmy }}</ref><ref>{{cite web | url=http://www.firstworldwar.com/atoz/pipsqueakwilfred.htm | title=Pip, Squeak and Wilfred | accessdate=2008-06-26 | work=First World War.com |date= | archiveurl= https://web.archive.org/web/20080511214022/http://www.firstworldwar.com/atoz/pipsqueakwilfred.htm| archivedate= 11 May 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="Mutt&Jeff">[http://www.greatwar.co.uk/medals/ww1-campaign-medals.htm The Great War 1914–1918 – A Guide to British Campaign Medals of WW1]</ref>

Similarly, when only the British War Medal and Victory Medal are worn together, they are referred to as the ''[[Mutt and Jeff]]'' pair.<ref name="Mutt&Jeff"/>

==References==
{{Reflist|30em}}
{{British campaign medals}}
{{Australian Campaign Medals}}
{{New Zealand campaign medals}}
{{South African military decorations and medals}}

[[Category:Decorations of the British Army]]
[[Category:British campaign medals]]
[[Category:Australian campaign medals]]
[[Category:New Zealand campaign medals]]
[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|402.19143]]
[[Category:Military decorations and medals of South Africa pre-1952]]