{{Infobox journal
| title         = McGill Law Journal
| cover         = [[File:Cropped MLJ Logo.jpg|200px]]
| editor        = 
| discipline    = 
| language      = English, French
| former_names  = 
| abbreviation  = 
| publisher     = 
| country       = Canada
| frequency     = Quarterly
| history       = 1952-present
| openaccess    = Yes
| license       = 
| impact        = 
| impact-year   = 
| website       = http://lawjournal.mcgill.ca/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0024-9041
| eISSN         = 
| boxwidth      = 
}}
The '''''McGill Law Journal''''' is a scholarly legal publication affiliated with the student body of the [[McGill University Faculty of Law]] in [[Montreal]], [[Quebec]], published by a non-profit corporate institution independent of the faculty run exclusively by students.

It also publishes the ''[[Canadian Guide to Uniform Legal Citation]]'' (also known as the ''[[McGill Guide]]''), Canada’s [[legal citation]] [[reference work]].<ref>{{cite web|url=http://www.carswell.com/description.asp?docid=6692 |title=Canadian Guide to Uniform Legal Citation, 7th Edition HC |publisher=Carswell.com |date=2010-08-17 |accessdate=2012-07-26}}</ref> The ''Journal'' was ranked as the best overall student-run law journal in the world outside of the United States in 2010 by the [[Washington and Lee University School of Law]].<ref>{{cite web|url=http://lawlib.wlu.edu/LJ/index.aspx |title=Law Journals: Submissions and Ranking |publisher=Lawlib.wlu.edu |date=2011-08-22 |accessdate=2012-07-26}}</ref>

==Overview==
Over the years the ''McGill Law Journal'' has garnered significant recognition in [[Canada]] and around the world.<ref>{{cite web|url=http://www.mcgill.ca/channels/announcements/item/?item_id=107276 |title=Announcement: McGill Law Journal rated among world’s best by Australia |publisher=Mcgill.ca |date=2009-06-15 |accessdate=2012-07-26}}</ref> Since its first citations in the early 1970s, it has been cited more often than any other university-affiliated law journal in the world by the [[Supreme Court of Canada]].<ref>{{cite web|url=http://www.thecourt.ca/2007/03/07/what-is-the-supreme-court-reading/ |title=The Court » Blog Archive » What Is The Supreme Court Reading? |publisher=Thecourt.ca |date=2007-03-07 |accessdate=2012-07-26}}</ref> Subscribers to the ''Journal'' reside in over forty countries across six continents. In addition, the ''Journal'' actively contributes to the development of Canadian legal methodology by publishing the [[Canadian Guide to Uniform Legal Citation]], which has become the standard reference work for almost all Canadian [[law review]]s, Canadian [[law school]]s, and courts.<ref>{{cite web|url=http://lawjournal.mcgill.ca/citeguide.php |title=McGill Law Journal |publisher=Lawjournal.mcgill.ca |date= |accessdate=2012-07-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20130210184155/http://lawjournal.mcgill.ca/citeguide.php |archivedate=February 10, 2013 }}</ref> The ''McGill Law Journal'''s citation style became the official style for Canadian legal citation by the [[Bluebook]], America's equivalent to the McGill Guide, before the first edition of the Canadian Guide to Uniform Legal Citation.

Students of the Faculty of Law of McGill University founded the ''McGill Law Journal'' in 1952,<ref>{{cite web|url=http://www.mcgill.ca/law/about/history/timeline/ |title=Historical high points |publisher=Mcgill.ca |date=2007-03-15 |accessdate=2012-07-26}}</ref> led by Founding Editor-in-Chief [[Jacques-Yvan Morin]]. From that founding, the ''Journal'' has promoted the development of legal [[scholarship]] by appealing to an audience that includes professors of law, practicing lawyers, and law students. Given that the [[Province of Quebec]] is a [[jurisdiction]] where the legal traditions of [[common law]] and [[Civil law (legal system)|civil law]] intersect in matters of [[private law]], the first editors of the ''Journal'' immediately appreciated its potential as a catalyst for the development of civilian legal scholarship published in English. The ''Journal'' is recognized as an important forum<ref>http://www.newcastle.edu.au/Resources/Faculties/Faculty%20of%20Business%20and%20Law/Research/Funding/LawjournalrankinglistMar08v3.pdf</ref> for the critical analysis of contemporary legal issues in the realms of [[public law]] and [[private law]], as well as [[international law]].

The ''Journal'' is a [[bilingual]] publication. The editorial team includes both [[francophone]] and [[English language|anglophone]] students tasked with the selection of articles and their preparation for publication. The ''Journal'' publishes a variety of articles pertaining to the civil and common law traditions in both of Canada's [[official languages]]. Part of its mandate is to contribute to the development of [[legal research]] that is comparative or [[transsystemic]] in nature.<ref>{{cite web|url=http://lawjournal.mcgill.ca/about.php |title=McGill Law Journal |publisher=Lawjournal.mcgill.ca |date= |accessdate=2012-07-26}}</ref>

Similar to its American counterparts and unlike many of its Canadian competitors, the ''McGill Law Journal'' is entirely student-run. In order to ensure the quality of its content, all manuscripts selected for publication are [[peer review]]ed by scholars from Canada and around the world using a double-blind system.

==''McGill Law Journal'''s Annual Lecture==
Since 2000 (and from 1984-92), the ''McGill Law Journal'' has invited an important guest each year to provide a lecture to the legal community of McGill and Montreal at large. The lecture is published in the ''McGill Law Journal'' and is one of the most important events of the year at the Faculty. Previous lecturers have included [[judge|Justice]] of the Supreme Court of Canada [[Charles Gonthier]] (2000), [[Philippe Kirsch]], President of the [[International Criminal Court]] (2001), Canadian Senator [[Gérald A. Beaudoin]] (2003), Justice [[John Gomery]] (2006), Canadian philosopher [[John Ralston Saul]] (2009), Justice of the Supreme Court of Canada [[Rosalie Abella]] (2010), and iconic Mexican novelist [[Carlos Fuentes]] (2011). Before its current incarnation, previous lecturers included [[Jacques-Yvan Morin]] (1984) and future [[Chief Justice]] of the Supreme Court of Canada [[Beverley McLachlin]] (1991).

==Books==
In 2013 ''The Journal: 60 years of people, prose, and publication'' by [[James Cummins (author)|James Cummins]] was published by 8th House Publishing in Montreal, detailing the history of the MLJ from volume one through volume fifty-seven upon the sixtieth anniversary of the institution.<ref>{{cite web |url=http://publications.mcgill.ca/droit/2012/10/22/rdm60ans/ |title=Soixante ans d’excellence : La Revue de droit de McGill s’éclate! |date=October 2012 |accessdate=October 23, 2012 |author=Bridget Wayland |publisher=''Focus Online''}}</ref> It was also featured in the book ''A Noble Roster: One Hundred and Fifty Years of Law at Mcgill'' by McGill Law alum Ian C. Pilarczyk.<ref>{{cite book |title=A Noble Roster: One Hundred and Fifty Years of Law at Mcgill |author=Ian C. Pilarczyk |publisher=McGill University Faculty of Law |date=1999 |page=36, 37, 126, 127, 130, 131, and 135-137}}</ref>

==Editors-in-chief==
The following list is taken from the mastheads of the ''McGill Law Journal'' volumes 1 through 59:
{{columns-list|colwidth=30em|
* Vol 1: [[Jacques-Yvan Morin]] (issue one), [[Fred Kaufman]] (issues two and three), and William H. Reynolds (issue four)
* Vol 2: John E. Lawrence
* Vol 3: Harold W. Ashenmil
* Vol 4: [[Raymond Barakett]]
* Vol 5: Henri P. Lafleur
* Vol 6: [[A. Derek Guthrie]]
* Vol 7: Norman M. May
* Vol 8: Alan Z. Golden
* Vol 9: Mark M. Rosenstein
* Vol 10: Joseph J. Oliver
* Vol 11: Larry S. Sazant
* Vol 12: [[Stephen Allan Scott]]
* Vol 13: Douglas Pascal
* Vol 14: Ronald I. Cohen
* Vol 15: Leonard Serafini
* Vol 16: André T. Mécs
* Vol 17: Joel King
* Vol 18: Michael David Kaylor
* Vol 19: Graham Nevin
* Vol 20: [[Frank H. Buckley]]
* Vol 21: Laura Falk Scott
* Vol 22: Louise Pelly
* Vol 23: Cally Jordan
* Vol 24: Neil J. Smitheman
* Vol 25: Mona R. Paul
* Vol 26: Linda R. Ganong (1979–1980) and [[Patrick Healy (judge)|Patrick Healy]] (1980–1981)
* Vol 27: F. Jasper Meyers
* Vol 28: [[Stephen Toope]]
* Vol 29: Daniel Gogek
* Vol 30: Peter Oliver
* Vol 31: Henry K. Schultz
* Vol 32: Marc Lemieux
* Vol 33: M. Kevin Woodall
* Vol 34: Gary F. Bell
* Vol 35: Daniel Torsher
* Vol 36: Julia E. Hanigsberg
* Vol 37: David A. Chemla
* Vol 38: Mark Phillips
* Vol 39: Erica Stone
* Vol 40: Jodi Lackman
* Vol 41: Mary-Pat Cormier
* Vol 42: Martin J. Valasek
* Vol 43: Sébastien Beaulieu
* Vol 44: Karlo Giannascoli
* Vol 45: Azim Hussain
* Vol 46: [[Robert Leckey]]
* Vol 47: Kevin MacLeod
* Vol 48: Carole Chan
* Vol 49: Toby Moneit
* Vol 50: Fabien Fourmanoit
* Vol 51: Kristin Ali
* Vol 52: David Sandomierski
* Vol 53: Benjamin Moss
* Vol 54: Erin Morgan
* Vol 55: Seo Yun Yang
* Vol 56: Sara Ross
* Vol 57: Will Colish
* Vol 58: Marie-Eve Goulet
* Vol 59: Olga Redko
* Vol 60: William Stephenson
* Vol 61: Fraser Harland
* Vol 62: Laura Cárdenas
* Vol 63: Éléna Sophie Drouin }}

==Other alumni==
Additional alumni from the ''Journal'' include justices [[Benjamin J. Greenb
erg]], [[Morris Fish]], [[John Gomery]], [[Jean-Louis Baudouin]], [[Brian Riordan (judge)|Brian Riordan]], [[Allan Lutfy]], [[Suzanne Coupal]], [[Brigitte Gouin]], [[Ronna Brott]], [[Nicholas Kasirer]], and [[Max M. Teitelbaum]]; politicians including [[Irwin Cotler]] and [[Yoine Goldstein]]; board chairmen like [[David P. O'Brien]] and [[Bernard Amyot]]; academic figures like [[Dick Pound]] and [[Bartha Knoppers]]; and entertainment professionals like [[Lionel Chetwynd]].

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website}}

{{DEFAULTSORT:Mcgill Law Journal}}
[[Category:Articles created via the Article Wizard]]
[[Category:Canadian law journals]]
[[Category:McGill University]]
[[Category:Multilingual journals]]
[[Category:1952 establishments in Quebec]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1952]]
[[Category:Law journals edited by students]]