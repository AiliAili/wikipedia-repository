{{good article}}
{{Infobox mountain
| name = Amak Volcano 
| photo = Izembek Lagoon Amak Island.jpg
| photo_caption = [[Amak Island]] and volcano at top
| elevation_ft  = 1601
| elevation_ref = 
| prominence_ft =
| prominence_ref= 
| listing = 
| location = North Pacific, part of [[Alaska]]
| range = [[Aleutian Islands]]
| map = USA Alaska | region_code = US-AK
| lat_d  = 55.41728 | lat_m = | lat_s = |lat_NS =N
| long_d =163.14687 | long_m= | long_s= |long_EW=W
| coordinates_ref= 
| topo = 
| type = [[Stratovolcano]]
| age = 
| volcanic_arc = [[Aleutian Arc]]
| last_eruption = 1796
| first_ascent = 
| easiest_route = 
}}

'''Amak Volcano''' is a [[basaltic andesite]] [[stratovolcano]] in the [[Aleutian Islands]] of [[Alaska]], USA, {{convert|618|mi|km}} from [[Anchorage]].<ref name=AVO>{{cite web|url=http://www.avo.alaska.edu/volcanoes/volcinfo.php?volcname=Amak|title=Amak description and statistics|publisher=[[United States Geological Survey]]|work=Alaska Volcano Observatory|accessdate=July 6, 2009}}</ref> It is located on [[Amak Island|the eponymous island]], {{convert|31|mi|km|0}} from [[Frosty Volcano]] and near the edge of the [[Alaskan Peninsula]]'s western flank. Only boats are allowed to access the island with a certain permit.

Blocky (dotted with flat blocks of minerals and crystals) lava flows<ref name=GVP>{{cite gvp|vn=311390|name=Amak |accessdate=July 6, 2009}}</ref> stream from its summit to its flanks. Three historical eruptions have taken place – two within the 18th century, the first from 1700–1710, and the latter in 1796. The earliest prehistoric eruption was believed to have taken place between 3050 and 2050&nbsp;BCE.

== Accessibility ==
[[Cold Bay, Alaska|Cold Bay]], the city nearest Amak, is easily accessible by plane. Amak is accessible only by boat; airplanes are not permitted to land on the island. Private boat rides to Amak are available in Cold Bay, but for access to the Aleutian Islands, a permit is required from the [[United States Fish and Wildlife Service]].<ref name=Wood>Wood and Kienle, p. 51.</ref>

== Geography and geology ==
Amak Island lies in the [[Bering Sea]], north of the main [[archipelago]] of the Aleutians. It is one of two islands (along with [[Bogoslof Island]]) that are {{convert|31|mi|km|0}} north<ref name=Marsh>{{cite journal|author1=Marsh, B.D.|author2=Leit, R.E.|title=Geology of Amak Island, Aleutian Islands, Alaska|work=[[Journal of Geology]]|publisher=The University of Chicago Press|date=November 1979|volume=87|issue=6|pages=715–723|<!--accessdate=July 9, 2009-->|bibcode = 1979JG.....87..715M |doi = 10.1086/628461 }}</ref> of the main range.<ref name=Wood/>

The United States has the most active volcanoes in the world, many of them geologically young.<ref>{{cite web|url=http://pubs.usgs.gov/fs/2006/3142|title=The National Volcano Early Warning System (NVEWS): U.S. Geological Survey Fact Sheet FS 2006-3142|publisher=[[United States Geological Survey]]|author1=Ewert, John|author2=Guffanti, Marianne|author3=Cervelli, Peter|author4=Quick, James|year=2006|accessdate=July 9, 2009}}</ref>
In Alaska, at least 50 volcanoes, including those in the Aleutian archipelago, have erupted in historical time.<ref name=NL>{{cite journal|url=http://www.dggs.dnr.state.ak.us/pubs/pubs?reqtype=citation&ID=16061|title=Alaska GeoSurvey News: NL 2008-1|publisher=Alaska Division of Geological & Geophysical Surveys|date=March 2008|accessdate=July 9, 2009|volume=11|issue=1|pages=1–14| archiveurl= https://web.archive.org/web/20090810162558/http://www.dggs.dnr.state.ak.us/pubs/pubs?reqtype=citation&ID=16061| archivedate= 10 August 2009 <!--DASHBot-->| deadurl= no}}</ref>
The state accounts for ~80% of the United States' volcanoes, excluding the [[seamount]]s in the area, ~8% of world volcanoes, and most of these are located among the Aleutian Islands.<ref name=NL/> The Aleutian Islands arc serves as the northern boundary of the [[Pacific Ring of Fire]],<ref name=NL/> where tectonic activity generates earthquakes and volcanic eruptions in masses.

The volcano is [[basaltic]]-[[andesite|andesitic]] in composition.<ref name=Wood/> It is a modest stratovolcano,<ref name=GVP/> rising no more than {{convert|1683|ft|m|0|sp=us}} above sea level.<ref name=AVO/> The volcanic crater is distinct, and has erupted in historical times, only "blocky" lava flows.<ref name=GVP/> Charles Wood and Jürgen Kienle, [[volcanologist]]s, propose that earlier activity, 4,000–5,000 years ago, consisted primarily of lavas of ethereal (fine) platy and thick andesite.<ref name=Wood/> Amak Volcano is unique in that its andesitic lavas, while composed the same as the other Aleutians, contain an abundance of [[potash]]. They also could contain more [[sodium carbonate]] and [[rare earth element]] deposits than the Aleutian norm.<ref name=Marsh/> Between Bogoslof, the other Aleutian island north of the main arc, and Amak, Amak's lavas are more [[Igneous rock|alkalic]] and [[silicic]].<ref name=Marsh/>

Glaciation took place around the volcano roughly 6700 years BP, carving out U-shaped valleys. At the southwest flank of the island, a crater, likely a [[maar]], can be found amid an [[alluvial plain]].<ref name=Wood/>

=== Eruptive history ===
The Amak Volcano has erupted three times in historical times: circa 2550 BC, from 1700–1710, and in 1796; the first of these events was identified with [[tephrochronology]]. Each eruption has been characterized by [[lava flow]]s, and the two most recent eruptions included a crater eruption.<ref name=GVP/>

==See also==
:{{portal|Mountains}}

== References ==
{{reflist}}

=== Bibliography ===
*{{cite book| editor1 = Wood, Charles A. | editor2 = Kienle, Jürgen
  | year = 1990
  | title = Volcanoes of North America: United States and Canada
  | location = New York | publisher = [[Cambridge University Press]], 354 p.
  | isbn = 0-521-43811-X}}

==External links==
* {{Commons-inline|Category:Amak Island|Amak Volcano}}

[[Category:Volcanoes of the Aleutian Islands]]
[[Category:Volcanoes of Aleutians East Borough, Alaska]]
[[Category:Volcanoes of Alaska]]
[[Category:Stratovolcanoes of the United States]]