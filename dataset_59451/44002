[[File:Lorraine-Dietrich 12 HP Torpedo 1912.jpg|thumb|right|Lorraine-Dietrich 12 HP Torpedo 1912]]

'''Lorraine-Dietrich''' was a [[France|French]] [[automobile]] and [[aircraft engine]] manufacturer from 1896 until 1935, created when railway locomotive manufacturer '''''Société Lorraine des Anciens Etablissements de Dietrich et Cie de Lunéville''''' (known as ''De Dietrich et Cie'', founded in 1884 by Jean de Dietrich) branched into the manufacture of automobiles. The [[Franco-Prussian War]] divided the company's manufacturing capacity, one plant in [[Niederbronn-les-Bains]], [[Alsace]], the other in [[Lunéville]], [[Lorraine (region)|Lorraine]].{{Sfn|Burgess-Wise|1974|p=507}}

==Beginnings==
In 1896, managing director of the Lunéville plant, Adrien, [[Baron de Turckheim]], bought the rights to a design by [[Amédée Bollée]].{{Sfn|Burgess-Wise|1974|p=507}} This used a front-mounted{{Sfn|Georgano|1990|p=15}} horizontal twin engine with sliding clutches and belt drive.{{Sfn|Burgess-Wise|1974|p=507}} It had a folding top, three [[acetylene]] headlights,{{Sfn|Georgano|1990|p=15}} and, very unusual for the period, plate glass windshield.{{Sfn|Georgano|1990|p=15}} While the company started out using engines from Bollée, de Dietrich eventually produced the entire vehicle themselves.{{Sfn|Burgess-Wise|1974|p=508}}

In 1898, de Dietrich debuted the ''Torpilleur'' (Torpedo) racer, which featured a four-cylinder engine and [[independent suspension]] in front,{{Sfn|Burgess-Wise|1974|p=508}} for the [[Paris-Amsterdam Trial]]; Gaudry wrecked ''en route'', but still placed third.{{Sfn|Burgess-Wise|1974|p=508}} The response was substantial, exceeding one million gold ''[[franc]]''s.{{Sfn|Burgess-Wise|1974|p=508}} The 1899 ''torpilleur'' was less successful, despite underslung chassis, a rear-mounted [[monobloc cylinders|monobloc]] four, and twin [[carburettor]]s; poor preparation left none of the works teams able to complete the [[Tour de France (automobile)|''Tour de France'']].{{Sfn|Burgess-Wise|1974|p=508}}

The Bollée-inspired design was supplanted by a [[licence]]-built [[Belgium|Belgian]] [[Vivinus]] ''[[voiturette]]'' at Niederbronn and a [[Marseilles]]-designed [[Turcat-Méry]] at Lunéville,{{Sfn|Burgess-Wise|1974|p=508}} following a 1901 deal with that cash-strapped company.<ref>{{harvnb|Burgess-Wise|1974|p=509}}, in caption</ref>

In 1902, de Dietrich hired 21-year-old [[Ettore Bugatti]], who produced prize-winning cars in 1899 and 1901, and he designed an [[overhead valve]] 24&nbsp;hp (18&nbsp;kW) four-cylinder with four-speed [[Transmission (mechanics)|transmission]]{{Sfn|Burgess-Wise|1974|p=508}} to replace the Vivinus.{{Sfn|Burgess-Wise|1974|p=508}} He also created their 30/35 of 1903, before quitting to join [[Mathis (cars)|Mathis]] in 1904.{{Sfn|Burgess-Wise|1974|p=508}}

The same year, management at Niederbronn quit car production, leaving it entirely to Lunéville,{{Sfn|Burgess-Wise|1974|p=508}} with the Alsace market being sold a Turcat-Méry [[badge engineering|badge-engineered]] as a de Dietrich.{{Sfn|Burgess-Wise|1974|p=508}} Even at the time, this was seen with some disdain, and Lunéville put the [[cross of Lorraine]] on the grille to distinguish them. Nevertheless, under the skin they were little different, nor would they be until 1911.{{Sfn|Burgess-Wise|1974|p=508}} For all that, the Lorraine-Dietrich was a prestige [[marque]], ranking with [[Crossley Motors|Crossley]] and [[Itala]],{{Sfn|Burgess-Wise|1974|p=508}} while attempting to break into the "super-luxury" market between 1905 and 1908 with a handful of ₤4,000 (US$20,000) six-wheeler ''limousines de voyage''.{{Sfn|Burgess-Wise|1974|p=508}}

[[File:Lorraine Dietrich CR2 Craner Curves-2.jpg|thumb|1905 Lorraine-Dietrich CR2 racing car]]
Like [[Napier & Son|Napiers]] and [[Mercedes (car)|Mercedes]], Lorraine-Dietrich's reputation was built in part on racing, which was "consistent if not distinguished",{{Sfn|Burgess-Wise|1974|p=508}} including [[Charles Jarrott (racing driver)|Charles Jarrott]]'s  third in the 1903 [[Paris-Madrid Rally]] and a 1-2-3 in the 1906 ''[[Circuit des Ardennes]]'', led by ace works driver [[Arthur Duray]].{{Sfn|Burgess-Wise|1974|p=508}}

De Dietrich bought out [[Isotta-Fraschini]] in 1907,{{Sfn|Burgess-Wise|1974|p=508}} producing two OHC cars to Isotta-Fraschini designs, including a 10&nbsp;hp (7.5&nbsp;kW) allegedly created by Bugatti.{{Sfn|Burgess-Wise|1974|p=508}} Also that year, Lorraine-Dietrich took over [[Ariel Mors Limited]] of [[Birmingham]], for the sole British model, a 20&nbsp;hp (15&nbsp;kW) four, shown at the [[Olympia, London|Olympia Motor Show]] in 1908, offered as bare chassis, [[Tickford#Salmons & Sons|Salmons & Sons]] [[convertible]], and [[Mulliners (Birmingham)|Mulliner]] [[Cabriolet (automobile)|cabriolet]].{{Sfn|Burgess-Wise|1974|p=508}} (The British branch was not a success, lasting only about a year.){{Sfn|Burgess-Wise|1974|p=508}}

For 1908, de Dietrich offered a line of chain-driven [[touring car|touring]] fours, the 18/28&nbsp;hp, 28/38&nbsp;hp, 40/45&nbsp;hp, and 60/80&nbsp;hp, priced between ₤550 and ₤960, and a 70/80&nbsp;hp six at ₤1,040.{{Sfn|Burgess-Wise|1974|p=508}} The British version differed, having shaft drive.{{Sfn|Burgess-Wise|1974|p=508}} That year, the names of the automotive and aero-engine divisions were changed to Lorraine-Dietrich.{{Citation needed|date=April 2008}}

By 1914, all de Dietrichs were shaft-driven, and numbered a 12/16, an 18/20, a new 20/30 tourers, and a sporting four-cylinder 40/75{{Sfn|Burgess-Wise|1974|p=508}} (in the mold of [[Mercer (car)|Mercer]] or [[Stutz Motor Company|Stutz]]), all built at [[Argenteuil]], [[Seine-et-Oise]] (which became company headquarters postwar).{{Sfn|Burgess-Wise|1974|p=508}}

==Post-World War I==
After [[World War I]], with Lorraine restored to France, the company restarted manufacture of automobiles and aero-engines. Their 12-cylinder aero-engines were used by [[Société Anonyme des Ateliers d'Aviation Louis Breguet|Breguet]], [[Industria Aeronautică Română|IAR]], and [[Aero Vodochody|Aero]], among others.

In 1919, new technical director [[Marius Barbarou]] (late of [[Delaunay-Belleville]]){{Sfn|Burgess-Wise|1974|p=508}} introduced a new model in two [[wheelbase]]s, the A1-6 and B2-6,{{Sfn|Burgess-Wise|1974|p=508}} joined three years later by the B3-6, with either short or long wheelbase.{{Sfn|Burgess-Wise|1974|p=508}} All fell in the 15 CV [[Tax horsepower#France|fiscal horsepower]] category, sharing the {{convert|3445|cc|cuin|abbr=on}} six cylinder engine, which had overhead valves, [[Hemi engine|hemispherical]] [[cylinder head|head]], aluminium [[piston]]s, and four-bearing [[crankshaft]].{{Sfn|Burgess-Wise|1974|p=508}}

The performance was such in 1923, three tourers "put up a passable showing"{{Sfn|Burgess-Wise|1974|p=509}} at the first [[24 Hours of Le Mans]], leading to the creation for 1924 of the 15 Sport, with twin carburetion, larger valves, and [[Dewandre-Reprusseau]] [[Servomechanism|servo]]-assisted four-wheel brakes{{Sfn|Burgess-Wise|1974|p=509}} (at a time when four-wheel brakes of any kind were a rarity); they ran second and third, and were comparable to the 3 litre Bentleys.{{Sfn|Burgess-Wise|1974|p=509}} The 15 CV Sport did better in 1925, winning Le Mans, followed home by a sister in third, while in 1926, [[Robert Bloch (racing driver)|Bloch]] and [[André Rossignol|Rossignol]] won at an average 106&nbsp;km/h (66&nbsp;mph), leading a 1-2-3 sweep by Lorraines.{{Sfn|Burgess-Wise|1974|p=509}} Lorraine-Dietrich thus became the first marque to win Le Mans twice and the first to win in two consecutive years.

This publicity contributed to touring 15s being bodied by [[Gaston Grummer]], also Argenteuil's director, who produced [[coachwork]] for the likes of Aurora, [[Olympia (car)|Olympia]], [[Gloriosa (automobile)|Gloriosa]], and [[Chiquita (automobile)|Chiquita]].<ref>{{harvnb|Burgess-Wise|1974|p=509}}, under "Believe it, or not."</ref> The 15 CV was joined by the 12 CV, a {{convert|2297|cc|cuin|abbr=on}} four-cylinder car (until 1929), and the 30 CV, with a {{convert|6107|cc|cuin|abbr=on}} six cylinder engine (until 1927), while the 15 CV survived until 1932; the 15 CV Sport fell in 1930, losing its last race, the 1931 [[Monte Carlo Rally]], when [[Donald Healey]]'s [[Invicta (car)|Invicta]] edged [[Jean-Pierre Wimille]] by a tenth of a point.{{Sfn|Burgess-Wise|1974|p=509}}

==Name change==
The de Dietrich family sold its share in the company, which became simply known as Lorraine from 1928 on.

==End of automobile production==
The 15 CV was supplanted by the 20 CV, which had a {{convert|4086|cc|cuin|abbr=on}} engine, of which just a few hundred were made.{{Sfn|Burgess-Wise|1974|p=509}} Automobile production eventually became unprofitable and, after the failure of their 20 CV model, the concern ceased production of automobiles in 1935.

In 1930, de Dietrich was absorbed by ''[[Société Générale Aéronautique]]'', and the Argenteuil plant was converted to making [[aircraft engine]]s and six-wheel [[truck]]s licensed from [[Tatra (company)|Tatra]].{{Sfn|Burgess-Wise|1974|p=509}} By 1935, Lorraine-Dietrich had disappeared from the automobile industry.{{Sfn|Burgess-Wise|1974|p=509}} Until [[World War II]], Lorraine concentrated on the military market, manufacturing vehicles such as the [[Lorraine 37L]] armoured carrier.

The Lunéville plant returned to rail [[locomotives]].{{Sfn|Burgess-Wise|1974|p=509}} In 2007, it still operated as [[De Dietrich Ferroviaire]].

==Aircraft engines==
[[File:Lorraine-Dietrich 8Be.jpg|thumb|Lorraine-Dietrich 8Be aircraft engine.]]
*[[Lorraine 5P]] – 5 cyl radial
*[[Lorraine 6A]] – (AM) 110&nbsp;hp
*[[Lorraine 7M Mizar]] – 7 cyl radial
*[[Lorraine 8A]] – V-8
*[[Lorraine 8B]] – V-8
*[[Lorraine 9N Algol]] – 9 cyl radial
*[[Lorraine Dietrich 12Cc]] ? Dc in error?
*{{Interlanguage link multi|Lorraine 12 D|it}} – V-12
*[[Lorraine 12E Courlis]] – W-12 450&nbsp;hp
*[[Lorraine 12F Courlis]] – W-12 600&nbsp;hp
*[[Lorraine 12H Pétrel]] – V-12
*[[Lorraine 12Q Eider]]
*[[Lorraine 12R Sterna]] – V-12 700&nbsp;hp
*[[Lorraine 12Rcr Radium]] – inverted V-12 with turbochargers 2,000&nbsp;hp
*[[Lorraine 14A Antarès]] – 14 cylinder radial 500&nbsp;hp
*[[Lorraine 14E]] – 14 cylinder radial 470&nbsp;hp{{Sfn|Hartmann|2002|p=46}}
*[[Lorraine 18F Sirius]] 18-cyl radial
*[[Lorraine 18G Orion]] – W-18
*[[Lorraine 18K]] – W-18
*[[Lorraine-Dietrich 18Kd]]
*[[Lorraine 24]] – W-24 1,000&nbsp;hp
*[[Lorraine 24E Taurus]] – 24 cyl radial (six banks of 4-inline?) 1,600&nbsp;hp
*[[Lorraine AM]] (''moteur d’Aviation Militaire (A.M.)'') – derived from German 6-cyl in-line engines
*[[Lorraine Algol Junior]] – 230&nbsp;hp
*[[Lorraine-Latécoère 8B]]
*[[Lorraine Diesel]] – built in 1932, rated at 200&nbsp;hp

==See also==
* [[De Dietrich Ferroviaire]]
* [[List of 24 Hours of Le Mans winners]]

==Notes==
{{reflist|30em}}

==References==
*{{cite encyclopedia|last= Burgess-Wise|first= David|authorlink= David Burgess-Wise|title= De Dietrich: France's Veteran Car Manufacturer|editor-last= Ward|editor-first= Ian|encyclopedia=The World of Automobiles|volume= 5|pages= 507–9|location= London|publisher= Orbis Publishing|year= 1974|ref=harv}}
* {{cite book|last= Georgano|first= G. N.|authorlink= Georgano, G.N.|title= Cars: Early and Vintage 1886-1930|location= London|publisher= Grange-Universal|year= 1990|ref=harv}}
*{{cite book|last=Hartmann|first=Gérard|title=Liore et Olivier|year=2002|publisher=E-T-A-I|location=Boulogne-Billancourt|page=46|isbn=2 7268 8607 8|ref=harv}}

==External links==
* [http://www.cartype.com/page.cfm?id=833&alph=ALL&dec=ALL CarType's Lorraine-Dietrich page]
* [http://lorraine-dietrich.ru/ 1913 Lorraine-Dietrich catalog]
{{Commons category|Lorraine-Dietrich|position=left}}



{{Lorraine aeroengines}}


[[Category:Car manufacturers of France]]
[[Category:Defunct aircraft engine manufacturers of France]]
[[Category:Defunct motor vehicle manufacturers of France]]
[[Category:Luxury motor vehicle manufacturers]]
[[Category:Truck manufacturers]]
[[Category:1900s automobiles]]
[[Category:1910s automobiles]]
[[Category:1920s automobiles]]
[[Category:1930s automobiles]]
[[Category:Brass Era vehicles]]
[[Category:Veteran vehicles]]
[[Category:Vintage vehicles]]
[[Category:Companies of France]]
[[Category:Vehicle manufacturing companies established in 1896]]
[[Category:Companies disestablished in 1935]]