{{other uses}}
{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Adolphe: a story found among the papers of an unknown writer
| title_orig    = ''Adolphe: Anecdote trouvée dans les papiers d'un inconnu''
| translator    = [[Alexander Walker (physiologist)|Alexander Walker]]
| image         = Adolphe novel 1842 title.jpg
| image_size    = 200px
| caption = Title page from the 1842 edition of ''Adolphe''
| author        = [[Benjamin Constant]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| genre         = [[Novel]]
| publisher     = Henry Colburn (London)
| release_date  = [[1816 in literature|1816]]
| english_release_date = [[1816 in literature|1816]]
| media_type    = Print ([[Hardcover]] & [[Paperback]])
| pages         = 
}}

'''''Adolphe''''' is a classic French novel by [[Benjamin Constant]], first published in 1816. It tells the story of an alienated young man, Adolphe, who falls in love with an older woman, Ellénore, the Polish mistress of the Comte de P***. Their illicit relationship serves to [[social isolation|isolate them from their friends and from society]] at large. The book eschews all conventional descriptions of exteriors for the sake of detailed accounts of feelings and states of mind.

Constant began the novel on 30 October 1806, and completed it some time before 1810.<ref>D. Milačić, „Beleška o piscu“, in B. Konstan, Adolf - Sesila. Beograd: Rad, 1964, pp. 143-148.</ref> While still working on it he read drafts to individual acquaintances and to small audiences, and after its first publication in [[London]] and [[Paris]] in June 1816 it went through three further editions: in July 1816 (new preface), July 1824 in [[Paris]] (restorations to Ch. 8, third preface), and in 1828. Many variants appear, mostly alterations to Constant's somewhat archaic spelling and punctuation.

== Plot summary ==

Adolphe, the narrator, is the son of a government minister. Introverted from an early age, his melancholy outlook has been formed by conversations with an elderly friend, whose insight into the folly and hypocrisy of the world has hindered rather than helped her in life. When the novel opens, he is 22 years old and has just completed his studies at the [[University of Göttingen]]. He travels to the town of D*** in Germany, where he becomes attached to the court of an enlightened Prince. During his stay he gains a reputation for an unpleasant wit. A friend's project of seduction inspires him to try something similar with the 32-year-old lover of the Comte de P***, a beautiful Polish refugee named Ellénore. The seduction is successful, but they both fall in love, and their relationship becomes all-consuming, isolating them from the people around them.

Eventually Adolphe becomes anxious as he realises that he is sacrificing any potential future for the sake of Ellénore. She persuades him to extend his stay by six months, but they quarrel, and when she breaks with the Comte de P*** and leaves her two children in order to be with him, and tends him after he is injured in a duel, he finds himself hopelessly indebted to her.

When he leaves the town of D***, Ellénore follows him, only to be expelled from his home town by Adolphe's father. Adolphe is furious and together they travel to her newly regained estate in Poland. However, a friend of the father, the Baron de T***, manipulates Adolphe into promising to break with Ellénore for the sake of his career. The letter which contains the promise is forwarded to Ellénore and the shock leads to her death. Adolphe loses interest in life, and the alienation with which the book began returns in a more serious form.<ref>B. Konstan, ''Adolf - Sesila''. Beograd: Rad, 1964.</ref>

== Controversy ==
The novel was partly inspired by Constant's relationships with [[Madame de Staël]] and [[Charlotte von Hardenberg]], and at the time of its publication it was widely assumed to be a ''[[roman à clef]]''. Constant was indignant and wrote a letter to the ''Morning Chronicle'' of London (23 June 1816) denying any such correspondence between fiction and life, and these objections animate his preface to the second edition. Framing letters written by an "editor" serve to distance the author from the work. However, close parallels between figures in his autobiography and characters in the novel have led to a great deal of speculation ever since.

== Film adaptation ==
{{Main|Adolphe (film)}}
The novel was adapted into a [[Adolphe (film)|2002 film]] directed by [[Benoît Jacquot]] and starring [[Isabelle Adjani]] as Ellénore.

== References ==
{{Reflist}}

== External links ==
* {{Gutenberg | no=13861 | name=Adolphe | language=French}}
* {{fr icon}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/constant-benjamin-adolphe.html/ Adolphe, audio version] [[File:Speaker Icon.svg|20px]]

{{Authority control}}

[[Category:1816 novels]]
[[Category:French autobiographical novels]]
[[Category:Psychological novels]]
[[Category:19th-century French literature]]
[[Category:French novels adapted into films]]