{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{{good article}}{{infobox weapon
| name               = 18 inch Mark I
| image              =Furious Turret pic.jpg
| caption            = An 18-inch gun fitted to {{HMS|Furious|47|2}} in a single-gun turret (1917)
| origin             = [[United Kingdom]] 
| type               = Naval gun
<!-- Type selection -->
| is_ranged          = y
| is_bladed          = 
| is_explosive       = y
| is_artillery       = y
<!-- Service history -->
| service            = 1918
| used_by            = [[Royal Navy]]
| wars               = [[World War I]]
<!-- Production history -->
| designer           = [[Armstrong Whitworth#Elswick Ordnance Company|Elswick Ordnance Company]]
| design_date        = 1915–16
| manufacturer       = Elswick Ordnance Company
| unit_cost          = 
| production_date    = 1916–17
| number             = 3
| variants           = 
<!-- General specifications -->
| spec_label         = 
| weight             = {{convert|149|LT}}
| length             = {{convert|62|ft|m|sigfig=3}}
| part_length        = {{convert|60|ft|m|sigfig=3}}L/39
| width              = 
| height             = 
| diameter           = 
| crew               = 
<!-- Ranged weapon specifications -->
| cartridge          = 
| cartridge_weight   = {{convert|3320|lb|kg}}
| caliber            = {{convert|18|in|mm}}
| barrels            = 
| action             = 
| rate               = 
| velocity           = {{convert|2420|ft/s|m/s|abbr=on}}
| range              = {{convert|31400|yd|m}}
| max_range          = {{convert|40500|yd|m}}
| feed               = 
| sights             = 
<!-- Artillery specifications -->
| breech             = [[Welin breech block]]
| recoil             = hydro-pneumatic
| carriage           = 
| elevation          = +22° to +45°
| traverse           = 10°
<!-- Explosive specifications -->
| filling            = 
| filling_weight     = {{convert|243|lb|kg}}
| detonation         = 
| yield              = 
}}
The '''BL 18-inch Mk I naval gun''' was a [[Breech-loading weapon|breech-loading]] [[naval gun]] used by the [[Royal Navy]] during [[World War I]]. It was the largest and heaviest gun ever used by the British.<ref>Hodges, p. 83</ref> Only the [[World War II|Second-World-War]] Japanese [[40 cm/45 Type 94]] had a larger calibre, {{convert|18.1|in|cm}}, but the British shell was heavier. The gun was a scaled-up version of the [[BL 15 inch Mk I naval gun]] and was developed to equip the "large light cruiser" (a form of battlecruiser) {{HMS|Furious|47|2}}. Three guns were built, but they did not see combat with ''Furious'', before they were removed from her and transferred to the {{sclass-|Lord Clive|monitor|3|warship}}s {{HMS|General Wolfe|1915|2}} and {{HMS|Lord Clive|1915|2}} for coast bombardment duties. Only 85 rounds were [[fired in anger]] before the war ended. All three were removed from service in 1920 and served as proving guns for [[cordite]] tests. Two were scrapped in 1933 and the last one survived until it was scrapped in 1947.

==Design and development==
The 18-inch gun had its genesis in the insistence of the [[First Lord of the Admiralty]], [[John Fisher, 1st Baron Fisher|Admiral Fisher]], for the biggest possible gun mounted on the fastest possible ship. He conceived of what he called "large light cruisers" carrying four {{convert|15|in|adj=on}} guns, which became the {{sclass-|Courageous|battlecruiser|4}}, but he wanted their [[sister ship|half-sister]] ''Furious'' to carry an even bigger gun.<ref>Burt, p. 308</ref> The [[Armstrong Whitworth#Elswick Ordnance Company|Elswick Ordnance Company]] was the only company capable of manufacturing such a large gun and began design work in 1915. It was designated as the "15-inch B" to conceal its real size and was derived from the design of the 15-inch Mk I already in service.<ref name="Buxton, p. 225">Buxton, p. 225</ref>

The gun and its breech mechanism weighed a total of {{convert|149|LT|0}}, almost half again as much as the 15-inch gun's {{convert|100|LT|0}}. It was mounted in a single-gun turret, also designated as the 15-inch B, derived from the twin-gun 15-inch Mark I/N turret. The [[barbette]]s of ''Furious'' were designed to accommodate either turret, in case problems arose with the 18-inch gun's development. The gun could depress to &minus;3° and elevate to a maximum of 30°. Ammunition development for the gun was naturally focused on anti-ship shells for ''Furious'', and it fired a {{convert|3320|lb|kg|adj=on}}, 4 [[List of British ordnance terms#C.R.H.|crh]] [[Armor-piercing shot and shell|armour-piercing, capped]] (APC) shell, at a [[muzzle velocity]] of {{convert|2270|ft/s|m/s|abbr=on}} to a distance of {{convert|28900|yd|m}}. It could fire one round per minute. The turret's revolving mass was {{convert|826|LT|0}}, only a slight 2% more than the {{convert|810|LT|0}} of its predecessor.<ref name="nw">{{cite web|title=Britain 18"/40 (45.7 cm) Mark I|url=http://www.navweaps.com/Weapons/WNBR_18-40_mk1.htm|website=www.navweaps.com|accessdate=4 June 2016}}</ref>

The guns proved to be too powerful for ''Furious''{{'}} light hull, and they became available for other uses during 1917, after trials showed the ship could not handle the stress of firing. [[Admiral]] Sir [[Reginald Bacon]], commander of the [[Dover Patrol]], conceived a plan to mount two guns inside the shell of the Palace Hotel in [[Westende]] from where they could bombard the naval facilities at [[Zeebrugge]] and [[Bruges]], provided that the hotel was captured during the upcoming [[Battle of Passchendaele]]. He planned to transport the guns across the [[English Channel]] lashed to the [[torpedo bulge]]s of monitors. He also thought that they could be used on the decks of monitors and as such a dual purpose carriage was designed for the guns, which could be used both afloat and ashore. Only a limited amount of traverse was required for either role, but elevation had to be increased to 45° to maximize range. The concept was approved 23 September 1917, and Elswick was ordered to design the new 'B CD' mounts for delivery in five months.<ref>Buxton, pp. 225–26</ref>

The original concept for land use involved a special elevating slide that could traverse 6° to either side. Ammunition handling, elevation and ramming were to be done via hydraulic pump, but the breech was hand-worked. The gun was to be installed in a turf-covered concrete dome with a gunport for the barrel. As much as possible of the gun and its mount was designed to be assembled out of range of German artillery and then moved on a special broad-gauge railway to the site on specially-designed wheels. The transportable section weighed {{convert|210|LT|0}}.<ref>Hodges, pp. 81–82</ref>

[[File:The Surrender of the German High Seas Fleet, November 1918 Q19294.jpg|thumb|right|A view of the stern of HMS ''Lord Clive'', showing her 18-inch gun in its fixed mounting (November 1918)]]
After the British Army failed to capture Westende, the mounting was optimised for use on a monitor. It was very simple, consisting of two large girders connected together at each end with the gun and its carriage between them. The mount could only traverse 10° inside its fixed, ½-inch (12.7&nbsp;mm) [[gun shield]] and was aimed over the starboard side of the monitor. It was loaded at a fixed angle of 10°, but it could only fire between 22° and 45° to equalize the stresses on the carriage and the ship. It was provided with hydraulically powered cranes, loading tray, rammer and breech mechanism to minimize the crew's workload, but the ammunition parties had to use muscle power. The shells were stowed below deck and had to be moved by overhead rail to the hatch in the deck behind the gun to be lifted up and loaded. The [[cordite]] propellant charges were kept in eighteen steam-heated storage tanks mounted on the forecastle deck abaft the funnel and moved to the gun on a [[bogie]] mounted on rails, two one-sixth charges at a time, which reduced the rate of fire to about one round about every 3–4 minutes. The monitors had to be extensively modified to handle the gun. Numerous additional structural supports had to be added underneath the gun to support its weight of {{convert|384|LT|0}}; the sides had to be plated in to accommodate the additional crewmen and the interior rearranged for the 18–inch shells and the loading arrangements.<ref>Buxton, pp. 73–74, 226–27</ref>

==Service==
[[File:HMS Furious-1.jpg|thumb|right|HMS ''Furious'', fitted with a single, aft 18 inch gun and a forward flight deck (1917)]]

A total of three guns were built by Armstrong Whitworth, two for ''Furious'' and a spare. The forward gun was removed from ''Furious'' in March 1917, before she was completed, when she was ordered to be converted to a seaplane carrier. The second gun was removed later in 1917, and she was converted into an [[aircraft carrier]].<ref name="Buxton, p. 225"/> The new 'B CD' mounts were delayed, and the mount for {{sclass-|Lord Clive|monitor|3|warship}} {{HMS|General Wolfe|1915|2}} was not delivered until 20 June 1918. The gun from ''Furious''{{'}} 'A' turret was lifted aboard on 9 July, but the ''General Wolfe'' was not ready to begin firing trials until 7 August. She was given the nickname of '[[Elephant and Castle]]', as the enormous gun-mount structure dominated the ship's profile.<ref>Buxton, p. 73</ref>

[[File:The Surrender of the German High Seas Fleet, November 1918 Q19292.jpg|thumb|right|On board ''Lord Clive''; her BL 18 inch gun is at its full elevation, November 1918]]
While the new mounting was being designed, further effort was put into the ammunition to extend the range as much as possible. Use of a supercharge, where one of the six charges was increased in weight to {{convert|165|lb|1}}, making a total of {{convert|690|lb|1}} propellant, and increasing the elevation to 45° extended the range to about {{convert|36900|yd}} with the existing 4 crh shells. New 8 crh [[Explosive material#High explosives|high explosive]] shells, with a longer, thinner ballistic cap, were ordered, but only two shells had been delivered before the end of the war. Some of the existing stock of 500 APC and 500 CPC (common, pointed, capped) shells on hand from ''Furious'' were modified with the new cap and were probably the only shells used during the war.<ref>Buxton, p. 227</ref>

''General Wolfe'' was assigned to the Dover Patrol on 15 August 1918, but did not fire on any targets until 28 September, when a large force of monitors was gathered to harass German lines of communication. She was anchored bow and stern, broadside to her target, and had difficulties dealing with the tidal currents. She opened fire on the railway bridge at Snaeskerke ({{convert|4|mi|1}} south of [[Ostend]]) at a range of {{convert|36000|yd|0}} and made naval history as the heaviest shell fired from the largest gun at the longest range in action to date.<ref>Buxton, pp. 66–67</ref> She fired 52 shells that day and found that the recoil from her 18–inch gun moved her sideways with her shallow hull and also caused her to roll, which slowed her rate of fire. She fired a total of 81 rounds before the end of the war.<ref>Buxton, p. 66-68</ref>

The second gun, ''Furious''{{'}} spare, was mounted in {{HMS|Lord Clive|1915|2}}, but she was not ready for combat until 13 October 1918. She fired three rounds the following day, but had to cease fire to avoid hitting friendly advancing troops. One round had already been loaded when the order came to cease fire so she fired it, with a reduced charge, into a minefield to seaward.<ref>Buxton, pp. 67–68, 227</ref> A total of 85 18-inch shells were fired in action by both guns.<ref name=nw/>    Wear on ''General Wolfe''{{'}}s gun was measured at about 0.37 inch after firing 161 [[List of British ordnance terms#Effective Full Charge|effective full charge]]s (EFC) - 105 rounds including [[Proof test#Large caliber proof testing|proof]] and practice, with 57 being supercharges.  This indicates the gun would have been good for well over 300 EFC, comparable with most other British ordnance using [[Cordite#Cordite (Mk I) and Cordite MD|Cordite MD]].<ref>Buxton, p. 227-228</ref>

The third gun, from ''Furious''{{'}} 'Y' turret, was intended for {{HMS|Prince Eugene|1915|2}}, which had been modified to accept it earlier in the year, but the war ended before it was mounted, although the monitor was ordered to Portsmouth to have it fitted on 19 October. The guns were removed from the monitors in December 1920. Gun No. 1, from ''Furious''{{'}} 'Y' turret, was lined down to {{convert|16|in}} and used in cordite-proving tests for the [[BL 16 inch Mk I naval gun|BL 16-inch Mk I gun]], intended for the cancelled [[G3 battlecruiser]]s, and used in the {{sclass-|Nelson|battleship|2}}s. It remained in use until 1942 and was scrapped in 1947. The other two guns were used at [[Shoeburyness]] and Yantlet artillery ranges in the [[Thames Estuary]] for similar duties; they were scrapped in 1933.<ref>Buxton, pp. 68, 74, 227–28</ref>

One mount survived and was used to mount a spare [[BL 14 inch Mk VII naval gun|BL 14-inch Mk VII gun]] from the battleship {{HMS|King George V|41|2}}. It was emplaced near [[Dover]] in 1940, and the combination was named "Pooh", after [[Winnie-the-Pooh]].<ref>Buxton, p. 228</ref>

==See also==
* [[40 cm/45 Type 94]] World War II Japanese 18-inch gun

==Notes==
{{Reflist|2}}

==Bibliography==
* {{cite book|last=Burt|first=R. A.|title=British Battleships of World War One|publisher=Naval Institute Press|location=Annapolis, MD|date=1986|isbn=0-87021-863-8}}
* {{cite book|last=Buxton|first=Ian|title=Big Gun Monitors: Design, Construction and Operations 1914–1945|publisher=Naval Institute Press|location=Annapolis, MD|date=2008|edition=2nd, revised and expanded|isbn=978-1-59114-045-0}}
* {{cite book|last=Campbell|first=N. J. M.|title=Warship III|year=1979|publisher=Conway Maritime Press|location=London|isbn=0-85177-204-8|pages=196–201|chapter=British Super-Heavy Guns, Part 3}}
* {{cite book|last=Hodges|first=Peter|title=The Big Gun: Battleship Main Armament 1860-1945|year=1981|publisher=Naval Institute Press|location=Annapolis, MD|isbn=0-87021-917-0}}
* {{cite book|last=Roberts|first=John|title=Battlecruisers|publisher=Naval Institute Press|location=Annapolis, MD|date=1997|isbn=1-55750-068-1}}

==External links==
{{Commons category|BL 18 inch naval gun}}
* [http://www.navweaps.com/Weapons/WNBR_18-40_mk1.htm page from Nav weapons.com]

{{GreatWarBritishNavalWeapons}}

[[Category:Naval guns of the United Kingdom]]
[[Category:460 mm artillery]]
[[Category:Elswick Ordnance Company]]
[[Category:World War I naval weapons of the United Kingdom]]