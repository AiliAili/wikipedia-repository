{{Use dmy dates|date=October 2013}}
{{Infobox Website
| name           = RotoHog
| logo           = RotoHog Black Small No Balls.jpg
| screenshot     = [[File:RotoHog Home.png|300px]]
| caption        = RotoHog.com homepage (March 2008)
| url            = [http://www.rotohog.com/corporate www.rotohog.com/corporate]
| commercial     = Yes
| type           = [[Fantasy sport]]
| registration   = Required to play
| owner          = Sports Composite DE, Inc.
| author         = 
| launch date    = 2007
| current status = Defunct
}}
'''RotoHog''' is the consumer facing [[Fantasy Sports]] website for [[Fastpoint Games]], a digital platform developer  that designs, implements and markets fantasy services for media and advertising partners.<ref>[http://entrepreneur.com/magazine/entrepreneur/2009/september/203140.html Entrepreneur Magazine Article] retrieved 25 August 2009</ref>  The company builds, delivers and manages co-branded fantasy sports games for major media companies, sports companies and professional sports leagues.

Their signature stock exchange game is a budget-based, high-roster-turnover style [[fantasy sports]] game that combines traditional fantasy scoring with a stock market-style trading floor for [[baseball]], [[basketball]], [[American football]], and [[Association football|Association Football (soccer)]].  The company also delivers traditional commissioner and pick-em style fantasy sports games.<ref>[http://www.socaltech.com/interview_with_kelly_perdew__rotohog.com/s-0016692.html Online News Article 1] retrieved 6 November 2008</ref>

RotoHog has also branched out to entertainment games with the Rose Ceremony game for the [[Reality TV]] show [[The Bachelor (US TV series)|The Bachelor]]<ref>[http://www.socaltech.com/rotohog_powers_abc_s_the_bachelor/s-0025963.html Online News Article] retrieved 19 January 2010</ref> and the [[Us Weekly]] Celebrity Fantasy League.<ref>[http://www.sdnn.com/sandiego/2010-03-24/local-county-news/former-carlsbad-resident-and-apprentice-winner-returns-for-new-venture San Diego News Network Article] retrieved 30 March 2010</ref>

==Fantasy Game Platform==
RotoHog is also the provider of nba.com's [[NBA]] Stock Exchange and commissioner games<ref>[http://www.nba.com/global/top_10_fantasy_picks_071011.html NBA posting with reference to RotoHog partnership] retrieved 10 January 2008</ref> and Brazilian media company Grupo RBS's first ever [[Fantasy football (soccer)|Fantasy Soccer]] game.<ref>[http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=35228348 Business Week Company Profile] retrieved 12 June 2008</ref>  In 2009, RotoHog began to provide games for Fox Sports en Español and the [[Association of Volleyball Professionals|AVP]] Pro Beach Volleyball tour.<ref>[http://www.socaltech.com/rotohog_signs_fox_sports_on_latin_american_deal/s-0020575.html News article - SoCalTech] retrieved 19 March 2009</ref>
<ref>[http://www.mediapost.com/publications/?fa=Articles.showArticle&art_aid=101626 News article - MediaPost] retrieved 19 March 2009</ref>

In June 2009, RotoHog closed a multi-year deal with [[Sporting News]] to power the sports media company's suite of fantasy sports games.  Sporting News VP Jeff Gerttula commented, "In order to take our games to the next level, we wanted to align with somebody intensely focused on the space, was committed to a strong [business-to-business] solution, and wasn’t satisfied with developing the same old off-the-shelf products," <ref>[http://www.sportsbusinessjournal.com/article/62768 News Article - Sports Business Journal] retrieved 8 June 2009</ref>

In March 2010, RotoHog began powering the MySpace Bracket Challenge for the 2010 NCAA college basketball tournament.  At the same time, the company announced that it would be launching its first commissioner-style baseball game on its own site.<ref>[http://www.socaltech.com/rotohog_powers_myspace_march_madness_brackets/s-0027474.html Online News Article] retrieved 29 March 2010</ref>

[[File:RotoHog Trading Floor.png|thumb|right|Trading Floor|300px]] 
[[File:RotoHog Confirm.png|thumb|write|Buy Order|300px]]

Like most fantasy sports games, the core elements of RotoHog's flagship stock exchange game involve building a team and setting a line-up to earning points. To this traditional core game, RotoHog adds a liquid [[Market (economics)|market]] for players that all team managers use to trade players. Players can be traded at almost any time and player prices reflect up-to-the minute supply and demand.  This trading system removes many of the problems with unfair trades or collusion between managers that can occur in traditional fantasy games.<ref>[http://www.thealarmclock.com/mt/archives/2007/08/fantasy_sports_1.html Online News Article 2] retrieved 11 January 2008</ref>

RotoHog also provides a [[social networking]] platform that allows users to compete in unlimited size leagues grouped by location, team allegiance, company affiliations, or local watering holes in addition to smaller traditional private leagues made up of a dozen or so  friends.<ref>[http://killerstartups.localhost.net.ar/Web20/rotohog--A-New-Era-Of-Fantasy-Sports/ Review site] retrieved 11 January 2008</ref> This social networking platform allows diverse groups to create custom leagues for their members. For example, the non-profit group ''Hire-a-Hero'' used RotoHog as a way to help military [[veterans]] connect with each other and transition back to civilian life.<ref>[http://sportsillustrated.cnn.com/2007/writers/melissa_segura/11/09/onthemoney/index.html Online Sports Magazine Article 1] retrieved 20 January 2008</ref> RotoHog have agreed to donate a portion of the [[advertisement]] [[revenue]] received from the Military Fantasy Football League (MFFL) to the ''Hire-a-Hero'' program. They have also decided to donate the $10 received for their premium statistical service, with which users can register to receive the latest news, injury reports and scouting reports from the site.<ref>Online Sports Magazine Article 1</ref>

===Prizes===
RotoHog has awarded various prizes include cash to the top teams in weekly, monthly and season long contests.  The 2007 Baseball and Football champions won $100,000 each.<ref>[http://www.canada.com/saskatoonstarphoenix/news/sports/story.html?id=3c557b55-1eae-47ad-9b4e-e5d17303c927&k=50943 Online Newspaper Article 3] retrieved 8 February 2008</ref><ref>[http://www.colonyleader.com/articles/2007/11/04/the_colony_courier-leader/sports/a-sportstc10.txt Online Newspaper Article 4] retrieved 8 February 2008</ref>

The owner of the second place Football team won $25,000, and third place $10,000.  The remaining top 100 finishers also earned cash prizes.<ref>[http://www.toacorn.com/news/2007/0823/Sports/078.html Online News Article 5] retrieved on 19 February 2008</ref>

===Strategy===
RotoHog-specific game strategies used by some players include the following:<ref>Online Newspaper Article 3</ref><ref>Online Newspaper Article 4</ref>
* Make money trading on fluctuations on player prices early in the season then use the extra cash then play the best players later in the season.
* Play hunches on undervalued players based on scouting and analysis of team match-ups.
* Rely on ability to make real time trades to make daily roster adjustments

=== Comparisons with other Fantasy Sports games ===
RotoHog differs from other [[fantasy sports]] games in several ways:
* Player [[trading floor]] prices continuously change based on the buying and selling behavior of team managers. Pricing in other games is either set by the game provider or is based directly on the on field performance of the players.
* Team managers can increase their initial budget by purchasing players who they think will appreciate in value and selling them for a profit.  Traditional [[salary cap]] games do not allow managers to profit from transactions.
* Team managers have an unlimited number of transactions and can make [[roster]] changes up until just before games start.  Traditional games only allow managers to make a few trades per season and often require managers to set their line-ups a week at a time.

RotoHog differs from [[Fantasy Sports stock simulation|fantasy sports stock simulations]] in that the goal of the game is to score the most fantasy points by fielding the best team of players. Stock simulation games focus on increasing your [[portfolio (finance)|portfolio]] value by anticipating price movements of players.

=== Criticisms ===
As a hybrid between traditional [[fantasy sports]] games and [[Fantasy Sports stock simulation|fantasy sports stock simulations]], there are many predictable ways for team managers to increase their budgets through day trading. These managers can then assemble teams with essentially no budget constraint. This gives managers with the time to play the market a financial edge over managers who do not.{{Citation needed|date=February 2008}}

==Company, financing and sponsorship==
{{Infobox company
 | name             = Sports Composite DE, Inc.
 | logo             = 
 | type             = [[Privately held company|Private]] [[Venture Capital|Venture Capital-backed]]
 | genre            = 
 | foundation       = [[Delaware]], [[United States|U.S.]] (2006)
 | founder          = [[David Wu (entrepreneur)|David Wu]] & [[Kent Smetters]]
 | location_city    = [[Los Angeles, California|Los Angeles, CA]]
 | location_country = [[United States of America|U.S.]]
 | location         = 
 | locations        = 
 | area_served      = Worldwide
 | key_people       = [[Kelly Perdew]], CEO
 | industry         = [[Internet]]
 | products         = RotoHog.com, NBA Stock Exchange
 | services         = Fantasy Sports Gaming Platform
 | revenue          = 
 | operating_income = 
 | net_income       = 
 | assets           = 
 | equity           = 
 | owner            = 
 | num_employees    = 
 | parent           = 
 | divisions        = 
 | subsid           = 
 | caption           = Fantasy Sports Like You've Never See Before!
 | homepage         = [http://www.rotohog.com/corporate www.rotohog.com]
 | footnotes        = 
 | intl             = 
}}
Sports Composite DE, Inc., the company that operates the RotoHog website, was founded by entrepreneur [[David Wu (entrepreneur)|David Wu]] and [[Wharton School|Wharton Business School]] Professor [[Kent Smetters]] in 2006 and is based in Inglewood, California.<ref>[http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=35228348 Financial Website Company Profile] retrieved 8 February 2008</ref><ref>[http://www.entrepreneur.com/magazine/entrepreneur/2008/may/192752.html Entrepreneur Magazine Article] retrieved 21 April 2008</ref>  [[Kelly Perdew]], winner of [[The Apprentice (TV series)|The Apprentice]] Season 2, was named CEO in May 2008.<ref>[http://www.fightticker.com/story_0523081200_kelly_perdew_resigns_proelitedotcom Fight Ticker News Article] retrieved 12 June 2008</ref>

The company raised $6 million in its first [[venture round]] in August 2007. This funding was raised via [[DFJ DragonFund China]] and Mission Ventures, with additional investment coming from Allen & Co. and SCP Worldwide.<ref>[http://venturebeat.com/2007/08/08/rotohog-a-new-twist-on-fantasy-sports Online News Article 5] retrieved 10 January 2008</ref><ref>[http://www.paidcontent.org/entry/419-fantasy-sports-site-rotohog-gets-6-million-funding/ Business News Website article] retrieved 11 January 2008</ref> [[StubHub]], an [[online marketplace]], co-founder Jeff Fluhr also invested in the online firm.<ref>[http://www.dmwmedia.com/news/2007/08/09/fantasy-sports-site-rotohog-com-lands-6-million Online News Article 6] retrieved 10 January 2008</ref>

The company raised an additional $2 million in March 2009.  The round was led by Mission Ventures and DFJ Dragon.<ref>[http://www.washingtonpost.com/wp-dyn/content/article/2009/03/10/AR2009031001859.html News Article Washington Post] retrieved 19 March 2009</ref>

The company's board of directors includes Leo Spiegel of Mission Ventures and Andy Tang of [[DFJ DragonFund China]].<ref>[http://www.socaltech.com/rotohog.com_launches_fantasy_football_site/s-0010527.html News article - Socal tech] retrieved 4 March 2008</ref>

Sports Composite DE, Inc. earns revenues from advertising and optional statistical packages.  RotoHog leagues and competitions have been sponsored by ex-American football player [[Marshall Faulk]] and [[hall of fame]] baseball players [[Fred Lynn]], [[Wade Boggs]], and [[Ozzie Smith]].

In December 2008, RotoHog along with domain name company, [[GoDaddy.com]], launched an [[NFL playoffs, 2008–09|NFL Playoff]] Game on [[Facebook]].  RotoHog used the game to entice users to play other its other games, while GoDaddy used the game to gain exposure to the RotoHog and Facebook communities while supporting its [[Super Bowl XLIII|Super Bowl]] ad campaign.<ref>[http://sportsbusinessjournal.com/article/61011 News article - Sports Business Journal] retrieved 29 December 2008</ref>

In April 2009, RotoHog developed an exclusive partnership with RazorGator that will allow RotoHog users to purchase tickets to live events through the global ticket reseller.<ref>[http://www.philly.com/philly/columnists/20090423_Morning_Bytes__Catching_Phillie__fever.html News Article - Philadelphia Inquirer] retrieved 23 April 2009</ref>

===Awards===
RotoHog has been the recipient of the following industry honors:
* 2009 [[Fantasy Sports Trade Association]] Baseball Stats Projection Accuracy Award
* 2009 [[Fantasy Sports Trade Association]] Football Stats Projection Accuracy Award <ref>[http://www.fantasysportsbusiness.com/wordpress/2010/01/27/cbs-rotohog-win-accuracy-awards/ Online News Article] retrieved 29 March 2010</ref>
* 2009 AlwaysOn OnHollywood Top 100 Media and Entertainment Companies:  Enabling Technology category Winner<ref>[http://www.earthtimes.org/articles/show/alwayson-unveils-the-coveted-2009-onhollywood-100,779545.shtml Article - Earth times] retrieved 1 July 2009</ref>  
* 2009 Tech Industry Awards:  Entertainment & Gaming category finalist alongside Future Ads, and category winner [[Blizzard Entertainment|Blizzard Entertainment, Inc.]]<ref>[http://www.reuters.com/article/pressRelease/idUS191692+16-Apr-2009+MW20090416 Article - Reuters] retrieved 1 July 2009</ref>

== See also ==
*[[Fantasy Sports]]
*[[Fantasy Sports stock simulation]]
*[[Fantasy baseball|Fantasy Baseball]]
*[[Fantasy basketball|Fantasy Basketball]]
*[[Fantasy football (American)|Fantasy Football (American)]]
*[[Fantasy football (soccer)|Fantasy Football (Soccer)]]

==References==
{{reflist}}

==External links==
*[http://www.rotohog.com/ Official Site]
*[http://www.hardballtimes.com/main/fantasy/article/building-a-statistical-model-for-rotohog/ Hardball Times: Building a statistical model for RotoHog]
{{Fantasy sports}}

{{DEFAULTSORT:Rotohog}}
[[Category:Online games]]
[[Category:Fantasy sports]]
[[Category:Privately held companies based in California]]