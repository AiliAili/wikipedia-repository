{{Infobox person
| image =<!-- Only freely-licensed images may be used to depict living people.  See [[WP:NONFREE]].  -->
| name        = Pinkney Near 
| birth_date   = 1927 
| birth_place  = New York City
| employer = [[Cincinnati Art Museum]], 
  [[Virginia Museum of Fine Arts]] 
| death_date = August 29, 1990
| death_place  = Hartford, Connecticut
| spouse = Henrietta Lloyd Near
| nationality = United States 
| education  = [[Johns Hopkins University]], [[Harvard]]
| occupation = Museum curator, art historian, author

}}
'''Pinkney Near''' (1927 - 1990)  was curator of the [[Cincinnati Museum of Art]] and afterward curator of the [[Virginia Museum of Fine Arts]] for thirty years. He was responsible for the [[VMFA]]'s acquisition of many treasured works of art, including arranging for the museum to purchase from [[John Lee Pratt]] the [[Francisco Goya]] portrait of General Nicolas Guye<ref>{{cite news|last1=Merritt|first1=Robert|title=Pinkney Near Left a Mark on the Museum|publisher=Richmond Time-Dispatch|date=September 30, 1990|location=Richmond, Virginia|page=1}}</ref><ref>{{cite news|last1=Barriault|first1=Anne B.|title=Enrich collections: the extraordinary collections of the Virginia Museum of Fine Arts|accessdate=15 March 2016|publisher=Apollo on High Beam|date=May 1, 2010|quote=...colleagues from the Metropolitan Museum of Art, the Brooklyn Museum of Art, Dumbarton Oaks, and the American Museum of Natural History . . . consulted with [visionary director Leslie Cheek, Jr.] and curator Pinkney Near to establish the Asian, Ancient Egyptian, Greek Roman, Byzantine, and Pre-Columbian collections.}}</ref> and from the collection of [[Karol Lanckoroński|Count Karol Lanckoroński]] of Vienna, Austria, a rare marble [[sarcophagus]] dating to the 2nd century B.C.<ref>{{cite web|title=Rare Marble Funeral Monument Given to Museum|url=https://www.newspapers.com/clip/4653261/sarcophagus_2nd-century_to_vmfa_says/|website=Newspapers.com|accessdate=18 March 2016}}</ref> The Guye portrait was long believed to be the most valuable single work of art in the [[VMFA]]'s collection. The Goya portrait of General Guye is on view prominently in the posthumously created Pinkney Near Gallery at the [[VMFA]]. 
==Biography==

Pinkney Near was born in New York City and was raised in [[Baltimore, Maryland]].<ref name=obit>{{cite news|title=Obituary: Pinkney L. Near|url=http://articles.baltimoresun.com/1990-09-20/news/1990263193_1_pinkney-paul-episcopal-church-rebecca-n|accessdate=26 October 2015|publisher=Baltimore Sun|date=September 20, 1990}}</ref> In 1944 he graduated from [[St. Paul's School (Brooklandville, Maryland)|St. Paul's School]], and in 1950 he became the first art history graduate of [[Johns Hopkins University]]. He earned his Master's degree from Harvard in 1951, and he studied [[Romanesque art|French Romanesque]] sculpture in Paris for three years on a [[Fulbright scholarship]]. He remained in Europe an additional year for research on a Sachs fellowship.<ref>{{cite news|last1=Merritt|first1=Robert|title=Pinkney Near left mark on museum|publisher=Richmond Times-Dispatch|date=September 30, 1990|page=1}}</ref>
He married the former Henrietta Lloyd.<ref>http://articles.baltimoresun.com/1990-09-20/news/1990263193_1_pinkney-paul-episcopal-church-rebecca-n</ref> Every summer Near, a specialist in European art,<ref>{{cite web|last1=Library|first1=VMFA|title=Special Collections in European Art: The Pinkney Near Collection|url=http://vmfa.museum/library/collections/|website=Virginia Museum of Fine Arts|accessdate=6 January 2016|location=Richmond, Virginia}}</ref> and his artist wife Henrietta Lloyd Near, a former colleague at the [[VMFA]], traveled to Europe to acquire art for the [[Virginia Museum of Fine Arts]]. Among the cities they visited were Barcelona, London, Rome, and Venice.<ref>{{cite news|title=People in the Fan: Henrietta Near|publisher=Fan District Newsletter|date=Summer 2015|location=Richmond, Virginia|pages=8, 9}}</ref> They were sometimes accompanied by philanthropists [[Sydney Lewis|Sydney and Frances Lewis]], donors of major collections to the museum.

Near began his work at the [[VMFA]] in 1958, and, except for three years (1962-1965) when he did graduate work at [[Harvard]] and was a curator at the [[Cincinnati Art Museum]], he served at the VMFA in Richmond the remainder of his life.<ref>{{cite news|last1=Merritt|first1=Robert|title=Curator left lasting mark|work=art critic|issue=March 30, 1990|publisher=Richmond Times-Dispatch|page=13}}</ref> In the early years, when the museum was run by a director and a curator with a small staff, Near became the first full-time curator of the [[Virginia Museum of Fine Arts]], one of the first state-supported art museums in the United States.

Pinkney Near died August 29, 1990 of pneumonia at a hospital in [[Hartford, Connecticut]], where he and his wife were visiting relatives. He was 63. His memorial service was held at [[St. Paul's Episcopal Church, Richmond, Virginia|St. Paul's Episcopal Church]] in [[Richmond, Virginia|Richmond,Virginia]], where he lived.<ref name=obit />

==Curator of the [[Cincinnati Museum of Art]]==
In 1964 Pinkney Near, curator of paintings, selected American paintings for the Cincinnati Museum's survey exhibition, ranging from early painters such as [[John Trumbull]], [[Gilbert Stuart]], and [[Washington Allston]] to later painters [[John Marin]], [[Max Weber]], [[George Bellows]], [[Childe Hassam]], [[Robert Vickrey]], [[Marsden Hartley]], [[Stuart Davis (painter)|Stuart Davis]], [[Mark Tobey]], [[Jane Freilicher]], [[Milton Avery]], and [[Tom Wesselmann]].<ref>{{cite web|last1=Darack|first1=Arthur|title=American Painting Surveyed in Great Style at Museum Show|website=Newspapers.com|publisher=Cincinnati Enquirer|location=Cincinnati Enquirer, Kentucky edition|page=80|date=April 5, 1964|quote=He [Pinkney Near] has done a superb job of selection and I cannot recall a more interesting panorama of American painting.}}</ref>

==Curator of the [[Virginia Museum of Fine Arts]]==
Pinkney Near first came to the VMFA in 1954 for a brief visit, when he had just returned from Paris where he studied Romanesque sculpture with his Fulbright Scholarship. A year later, in 1955 he left the museum for a Sachs fellowship which sponsored further study in Europe. However, it was during this brief stay that he began the evolution of the Collections Division, and, when he returned in 1956 at the age of 29, he became curatorial assistant at the museum. Two years later he was promoted to curator but decided to pursue his graduate work at Harvard. When he returned, he became Chief Curator.<ref>{{cite news|title=Hiring of chief curator marked start of Collections Division|accessdate=20 March 2016|publisher=Richmond Times-Dispatch|date=January 12, 1986|location=Richmond, Virginia|page=5}}</ref> Pinkney Near served as VMFA curator for 30 years, much of that time as chief administrative curator. In 1988 <ref>{{cite news|title=Va. Museum chooses curator for Mellon Collection|accessdate=20 March 2016|publisher=The Richmond Afro-American|date=December 24, 1988|location=Richmond, Virginia|page=A6|quote="In selecting Mr. Near to be the first holder of this prestigious title, Mr. Mellon has recognized Mr. Near's lifelong commitment to scholarship and connoisseurship," says Paul N. Perrot, director of the museum.}}</ref> Near was named to the newly created post of Paul Mellon Curator and senior research curator, a post in which he worked closely with the Mellon Collection and with [[Paul Mellon]], who provided important gifts of art, both paintings and sculpture portraying animals, to the museum.<ref>Richmond Times-Dispatch, September 30, 1990.</ref><ref><http://articles.baltimoresun.com/1990-09-20/news/1990263193_1_pinkney-paul-episcopal-church-rebecca-n</ref>

Through the years Near acquired for the museum some of its greatest treasures, including paintings by [[Théodore Géricault]], [[Claude Lorraine]]'s "Battle on a Bridge," [[Aristide Maillol]]'s "The River," [[Claude Monet]]'s "Irises by the Pond," [[Charles Willson Peale]]'s "William Smith and His Grandson," [[Angelica Kauffmann]]'s "Cornelia Pointing to Her Children as Her Treasures," and [[Jean-Baptiste Belin|Jean-Bapiste Blin de Fontenay]]'s monumental "Buffet Under a Trellis."

In 1976 Near announced the [[VMFA|VMFA's]] purchase of American sculptor [[Isamu Noguchi|Isamu Noguchi's]] polished stainless steel sculpture "Open Lock," completed in 1964.<ref>{{cite news|title=Virginia Museum Acquires Sculpture by Isamu Noguchi|accessdate=20 March 2016|publisher=The Danville Register|date=August 9, 1976|location=Danville, Virginia|page=6}}</ref>

Among the exhibits curated by Near was one called ''Three Masters of Landscape: Fragonard, Robert and Boucher'' which, although small, presented interesting issues due to the variability of the works. The exhibit made use of a recently acquired Fragonard entitled ''Landscape With Three Washerwomen".<ref name=nyt1981>{{cite news |title=Gallery View; THREE FRENCH LANDSCAPISTS WITH A COMMON VISION; RICHMOND, Va.|publisher=New York Times|date=November 29, 1981|first=John |last=Russell}}</ref>

==Research and publications==
Pinkney Near wrote ''The Whistlers:A Family Reunion'', published in 1965 by the [[Cincinnati Museum of Art]], followed in 1981 by ''Three Masters of Landscape: Fragonard, Robert, and Boucher'', which was published by the [[Virginia Museum of Fine Arts]]. While at the [[VMFA]], the museum also published his book ''Apollo''. In 1985, his book ''French Paintings'' was published by the [[University of Virginia Press]].

Near, along with two other art experts, [[Julien Binford]] (professor emeritus at [[Mary Washington College]] in [[Fredericksburg, Virginia]]) and William Campbell (Curator of American Paintings at the [[National Gallery of Art]] in Washington) selected the paintings by the deceased artist [[Gari Melchers]] for permanent display in the Gari Melchers Memorial Gallery at Belmont, the [[Falmouth, Virginia]] home of the artist.<ref>{{cite news|last1=Culbertson|first1=Drena|title=Final Melchers Collection selection is due this week|publisher=The Free Lance Star|date=July 26, 1976|location=Fredericksburg, Virginia}}</ref>

He was an author of many of the VMFA exhibition catalogs and illustrated books including ''French Paintings: The Collection of Mr. and Mrs. Paul Mellon''.

==List of major works==
*Near, Pinkney L, and John Rewald. French Paintings: The Collection of Mr. and Mrs. Paul Mellon in the Virginia Museum of Fine Arts. Richmond: The Museum, 1985. ISBN 9780917046193
*Near, Pinkney L, Jean-Honoré Fragonard, Hubert Robert, and François Boucher. Three Masters of Landscape, Fragonard, Robert, and Boucher: A Loan Exhibition on Display at the Virginia Museum, November 10 to December 27, 1981. Richmond: Virginia Museum of Fine Arts, 1981. ISBN 9780917046117
*Near, Pinkney L. Treasures in the Virginia Museum. Richmond: Virginia Museum, 1974.  ISBN 9780813906003
*Near, Pinkney L, and William Blake. William Blake's Illustrations of the Book of Job. , 1975.
*Near, Pinkney L. Apollo. Richmond, VA, Virginia Museum of Fine Arts, 1985

==Pinkney Near memorial lectures in art history==
Following the death of Pinkney Near in 1990 a series of memorial lectures in art history featuring notable art historians was begun at [[Art6|Art6 Gallery]] in Richmond in his honor. All but the last of the series was presented in the main gallery at Art6. Speakers in the series included Mia Genoni, Suzanne Foley, [[Robert Hobbs]], Matthew Affron, Richard Woodward, Joseph Dye III, Mitchell Merling, Belle Pendleton, and Susanne Kilgore Arnold. The final lecture in the series was given by then [[VMFA]] curator of contemporary art John B. Ravenal at the [[Virginia Museum of Fine Arts]],<ref>{{cite web|title=Art6 Presents|url=http://www.richmondpubliclibrary.org/calendar_more.asp?calendarID=2260|website=Richmond Public Library|accessdate=18 March 2016}}</ref> now director at the [[DeCordova Museum and Sculpture Park]].<ref>{{cite web|last1=Ravenal|first1=John|title=Curating Jasper Johns+Edvard Munch|url=https://vimeo.com/171099523|website=Vimeo|accessdate=18 June 2016}}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Near, Pinkney}}
[[Category:Art curators]]
[[Category:Fulbright Scholars]]
[[Category:Art historians]]
[[Category:1927 births]]
[[Category:Johns Hopkins University alumni]]
[[Category:1990 deaths]]
[[Category:Harvard University alumni]]