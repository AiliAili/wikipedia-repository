{{Use dmy dates|date=April 2014}}
[[Image:S and P 500 pe ratio to mid2012.png|320px|right|thumb|[[Robert Shiller]]'s plot of the S&P composite real '''price–earnings ratio''' and interest rates (1871–2012), from ''[[Irrational Exuberance (book)|Irrational Exuberance]]'', 2d ed.<ref name="IE2">{{cite book |last=Shiller |first=Robert |title=[[Irrational Exuberance (book)|Irrational Exuberance (2d ed.)]]|year=2005 |publisher=[[Princeton University Press]] |isbn= 0-691-12335-7 }}</ref> In the preface to this edition, Shiller warns that "the stock market has not come down to historical levels: the price–earnings ratio as I define it in this book is still, at this writing [2005], in the mid-20s, far higher than the historical average. ... People still place too much confidence in the markets and have too strong a belief that paying attention to the gyrations in their investments will someday make them rich, and so they do not make conservative preparations for possible bad outcomes."]]

The '''price/earnings ratio''' (often shortened to the '''P/E ratio''' or the '''PER''') is the ratio of a company's stock price to the company's earnings per share. The ratio is used in valuing companies.

==Versions==
There are multiple versions of the P/E ratio, depending on whether earnings are projected or realized, and the type of earnings.
* "Trailing P/E" uses the weighted average number of common shares in issue divided by the [[net income]] for the [[Trailing twelve months|most recent 12-month period]]. This is the most common meaning of "P/E" if no other qualifier is specified. Monthly earnings data for individual companies are not available, and in any case usually fluctuate seasonally, so the previous four quarterly earnings reports are used and [[earnings per share]] are updated quarterly. Note, each company chooses its own [[financial year]] so the timing of updates varies from one to another.
* "Trailing P/E from continued operations" uses [[operating earnings]], which exclude earnings from discontinued operations, extraordinary items (e.g. one-off windfalls and write-downs), and accounting changes.
* "Forward P/E": Instead of [[net income]], this uses estimated net earnings over next 12 months. Estimates are typically derived as the mean of those published by a select group of analysts (selection criteria are rarely cited).

As an example, if stock A is trading at $24 and the earnings per share for the most recent 12-month period is $3, then stock A has a P/E ratio of 24/3 or 8. Put another way, the purchaser of the stock is investing $8 for every dollar of earnings. Companies with losses (negative earnings) or no profit have an undefined P/E ratio (usually shown as "not applicable" or "N/A"); sometimes, however, a negative P/E ratio may be shown.

Some people mistakenly use the formula [[market capitalization]] / [[net income]] to calculate the P/E ratio. This formula often gives the same answer as [[market price]] / [[earnings per share]], but if new capital has been issued it gives the wrong answer, as [[Market capitalisation|market capitalization]] = [[market price]] × ''current'' number of shares whereas [[earnings per share]]= [[net income]] / ''weighted average'' number of shares.

Variations on the standard trailing and forward P/E ratios are common. Generally, alternative P/E measures substitute different measures of earnings, such as rolling averages over longer periods of time (to attempt to "smooth" volatile or cyclical earnings, for example),<ref>{{cite journal |last=Anderson |first=K. |last2=Brooks |first2=C. | title=The Long-Term Price-Earnings Ratio | version= | pages= | publisher= | year=2006 | doi= | ssrn=739664}}</ref> or "corrected" earnings figures that exclude certain extraordinary events or one-off gains or losses. The definitions may not be standardized. For companies that are loss-making, or whose earnings are expected to change dramatically, a "primary" P/E can be used instead, based on the earnings projections made for the next years to which a discount calculation is applied.

==Interpretation==
The price/earnings ratio (PER) is the most widely used method for determining whether shares are “correctly” valued in relation to one another. But the PER does not in itself indicate whether the share is a bargain. The PER depends on the market’s perception of the risk and future growth in earnings. A company with a low PER indicates that the market perceives it as lower risk or lower growth or both as compared to a company with a higher PER. The PER of a listed company’s share is the result of the collective perception of the market as to how risky the company is and what its earnings growth prospects are in relation to that of other companies. Investors use the PER to compare their own perception of the risk and growth of a company against the market’s collective perception of the risk and growth as reflected in the current PER. If the investor feels that his perception is superior to that of the market, he can make the decision to buy or sell accordingly.<ref>{{Cite web|url = https://sites.google.com/site/investmentsinshares/home/3-valuation|title = Valuation|date = |access-date = |website = |publisher = |last = |first = }}</ref>

==Historical P/E ratios for the U.S. stock market==
[[Image:Price-Earnings Ratios as a Predictor of Twenty-Year Returns (Shiller Data).png|370px|right|thumb|Price-Earnings ratios as a predictor of twenty-year returns based upon the plot by [[Robert Shiller]] (Figure 10.1,<ref name="IE2"/> <small> [http://irrationalexuberance.com/shiller_downloads/ie_data.xls source]</small>). The horizontal axis shows the [[:Image:IE Real SandP Price-Earnings Ratio, Interest 1871-2006.png|real price-earnings ratio of the S&P Composite Stock Price Index]] as computed in ''Irrational Exuberance'' (inflation adjusted price divided by the prior ten-year mean of inflation-adjusted earnings). The vertical axis shows the geometric average real annual return on investing in the S&P Composite Stock Price Index, reinvesting dividends, and selling twenty years later. Data from different twenty-year periods is color-coded as shown in the key. See also [[:Image:Price-Earnings Ratios as a Predictor of Ten-Year Returns (Shiller Data).png|ten-year returns]]. Shiller stated in 2005 that [[:Image:Price-Earnings Ratios as a Predictor of Ten-Year Returns (Shiller Data).png|this plot]] "confirms that long-term investors—investors who commit their money to an investment for ten full years—did do well when prices were low relative to earnings at the beginning of the ten years. Long-term investors would be well advised, individually, to lower their exposure to the stock market when it is high, as it has been recently, and get into the market when it is low."<ref name="IE2"/>]]

Since 1900, the average P/E ratio for the [[S&P 500]] index has ranged from 4.78 in Dec 1920 to 44.20 in Dec 1999.<ref>{{cite web |url=http://seekingalpha.com/article/124295-s-p-p-e-ratio-is-low-but-has-been-lower |title=Seeking Alpha blog comment... }}</ref> However, except for some brief periods, during 1920–1990 the market P/E ratio was mostly between 10 and 20.<ref>[http://www.investopedia.com/articles/technical/04/020404.asp Is the P/E Ratio a Good Market-Timing Indicator?<!-- Bot generated title -->]</ref>

The average P/E of the market varies in relation with, among other factors, expected growth of earnings, expected stability of earnings, expected inflation, and yields of competing investments.  For example, when U.S. treasury bonds yield high returns, investors pay less for a given [[earnings per share]] and P/E's fall.

The average U.S. equity P/E ratio from 1900 to 2005 is 14 (or 16, depending on whether the [[geometric mean]] or the [[arithmetic mean]], respectively, is used to average).{{Citation needed|date=August 2012}}

Jeremy Siegel has suggested that the average P/E ratio of about 15 <ref>{{cite web |url=http://www.investorsfriend.com/S%20and%20P%20500%20index%20valuation.htm |title=Is the S&P 500 Index now over-valued? What Return Can You Reasonably Expect From Investing in the S&P 500 Index? |publisher=investorsfriend.com |accessdate=18 December 2010 }}</ref> (or earnings yield of about 6.6%) arises due to the long term returns for stocks of about 6.8%. In ''[[Stocks for the Long Run]]'', (2002 edition) he had argued that with favorable developments like the lower capital gains tax rates and transaction costs, P/E ratio in "low twenties" is sustainable, despite being higher than the historic average.

[[Image:S&P 500 index and price-to-earnings ratios.svg|right|thumb|300px]]

Set out below are the recent year end values of the S&P 500 index and the associated P/E as reported.<ref>{{cite web |url=http://www2.standardandpoors.com/spf/xls/index/SP500EPSEST.XLS |title=S&P 500 Earnings and Estimate Report }}</ref> For a list of recent contractions ([[recession]]s) and expansions see [http://www.nber.org/cycles.html U.S. Business Cycle Expansions and Contractions].

{|class="wikitable sortable"
|-
! Date
! Index
! P/E
! EPS growth %
! Comment
|-
|2009-06-30||919.32||122.41|| – ||
|-
|2009-03-31||797.87||116.31|| – ||
|-
|2008-12-31||903.25||60.70|| – ||
|-
|2007-12-31||1468.36||22.19||1.4||
|-
|2006-12-31||1418.30||17.40||14.7||
|-
|2005-12-31||1248.29||17.85||13.0||
|-
|2004-12-31||1211.92||20.70||23.8||
|-
|2003-12-31||1111.92||22.81||18.8||
|-
|2002-12-31||879.82||31.89||18.5||
|-
|2001-12-31||1148.08||46.50||−30.8||[[Early 2000s recession|2001 contraction]] resulting in P/E peak
|-
|2000-12-31||1320.28||26.41||8.6|| [[Dot-com bubble]] burst: 10 March 2000
|-
|1999-12-31||1469.25||30.50||16.7||
|-
|1998-12-31||1229.23||32.60||0.6||
|-
|1997-12-31||970.43||24.43||8.3||
|-
|1996-12-31||740.74||19.13||7.3||
|-
|1995-12-31||615.93||18.14||18.7||
|-
|1994-12-31||459.27||15.01||18.0||Low P/E due to high recent [[earnings growth]].
|-
|1993-12-31||466.45||21.31||28.9||
|-
|1992-12-31||435.71||22.82||8.1||
|-
|1991-12-31||417.09||26.12||−14.8||
|-
|1990-12-31||330.22||15.47||−6.9||[[Early 1990s recession|July 1990 – March 1991 contraction]].
|-
|1989-12-31||353.40||15.45||.||
|-
|1988-12-31||277.72||11.69||.|| Bottom ([[Black Monday (1987)|Black Monday]] was 19 Oct 1987)
|}

Note that at the height of the [[Dot-com bubble]] P/E had risen to 32. The collapse in earnings caused P/E to rise to 46.50 in 2001. It has declined to a more sustainable region of 17. Its decline in recent years has been due to higher [[earnings growth]].

==The P/E ratio in business culture ==
The P/E ratio of a company is a major focus for many managers. They are usually paid in company stock or options on their company's stock (a form of payment that is supposed to align the interests of management with the interests of other stock holders). The stock price can increase in one of two ways: either through improved earnings or through an improved multiple that the market assigns to those earnings. In turn, the primary drivers for multiples such as the P/E ratio is through higher and more sustained earnings growth rates.

Consequently, managers have strong incentives to boost earnings per share, even in the short term, and/or improve long term growth rates. This can influence business decisions in several ways:
* If a company wants to acquire companies with a higher P/E ratio than its own, it usually prefers paying in cash or debt rather than in stock. Though in theory the method of payment makes no difference to value, doing it this way offsets or avoids earnings dilution (see [[accretion/dilution analysis]]).
* Conversely, companies with higher P/E ratios than their targets are more tempted to use their stock to pay for acquisitions.
* Companies with high P/E ratios but volatile earnings may be tempted to find ways to smooth earnings and diversify risk—this is the theory behind building [[Conglomerate (company)|conglomerates]].
* Conversely, companies with low P/E ratios may be tempted to acquire small high growth businesses in an effort to "rebrand" their portfolio of activities and burnish their image as growth stocks and thus obtain a higher PE rating.
* Companies try to smooth earnings, for example by "[[slush fund]] accounting" (hiding excess earnings in good years to cover for losses in lean years). Such measures are designed to create the image that the company always slowly but steadily increases profits, with the goal to increase the P/E ratio.
* Companies with low P/E ratios are usually more open to leveraging their balance sheet. As seen above, this mechanically lowers the P/E ratio, which means the company looks cheaper than it did before leverage, and also improves earnings growth rates. Both of these factors help drive up the share price.
* Strictly speaking, the ratio is measured in years, since the price is measured in dollars and earnings are measured in dollars per year. Therefore, the ratio demonstrates how many years it takes to cover the price, if earnings stay the same.

==Related measures==
* [[Cyclically adjusted price-to-earnings ratio]]
* [[PEG ratio|Price-to-earnings-growth ratio]]
* [[Dividend yield|Price-to-dividend ratio]]
* [[Social earnings ratio]]
* [[EV/EBITDA]]
* [http://aarwinsworldoffinance.com/2016/11/earnings-per-share-eps-cfa-level-1.html/ Earnings per Share]

==See also==
{{Div col}}
* [[Cyclically adjusted price-to-earnings ratio]]
* [[Fundamental analysis]]
* [[List of finance topics]]
* [[Stock market]]
* [[Stock market bubble]]
* [[Stock market crash]]
* [[Stock valuation]]
* [[Value investing]]
* [[Valuation using multiples]]
{{Div col end}}

==References==
{{Reflist|2}}

==External links==
*Book values not generally relevant for valuation https://sites.google.com/site/investmentsinshares/book-values
*How companies are valued https://sites.google.com/site/investmentsinshares/home/3-valuation
*Free Video Tutorial In Which Price To Earnings Ratio Is Explained In Plain, Simple, Easy To Understand, Jargon Free Language 
* [http://www.fool.com/investing/value/2005/08/19/how-to-use-the-pe.aspx How to Use the P/E]
* [http://www.macroaxis.com/invest/ratio/Price_to_Earning Global P/E filter]  Check current P/E indicators of public companies
* [http://www.stockcalculation.com/2014/07/pe-calculator.html Price-To-Earning Ratio calculator]
* [http://marketcapitalizations.com/historical-data/pe-ratio-sp-100-companies/ Historical P/E Ratios of S&P 100 companies]
{{Financial ratios}}

{{DEFAULTSORT:Price-earnings ratio}}
[[Category:Financial ratios]]