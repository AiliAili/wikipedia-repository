<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R-120
 | image=CLASS Bush Caddy R120 C-GZCC 02.JPG
 | caption=CLASS R-120 BushCaddy
}}{{Infobox Aircraft Type
 | type=[[Kit aircraft]]
 | national origin=[[Canada]]
 | manufacturer=[[Canadian Light Aircraft Sales and Service]]<br />[[Bushcaddy]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 
 | number built=43 (December 2011)
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]24,759 (kit, 2011)
 | developed from= [[Bushcaddy R-80]]
 | variants with their own articles=[[Bushcaddy L-160]]
}}
|}
The '''Bushcaddy R-120''' is a [[Canada|Canadian]] [[kit aircraft]] produced by [[Canadian Light Aircraft Sales and Service]] (CLASS) of [[Montréal/Saint-Lazare Aerodrome|St. Lazare, Quebec]] and later [[Les Cedres, Quebec]] and now [[Bushcaddy]] of [[Lachute, Quebec]] and more recently [[Cornwall Regional Airport]] in [[Summerstown, Ontario]].<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 35. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 47. Belvoir Publications. ISSN 0891-1851</ref><ref name="Specs">{{cite web|url = http://bushcaddy.com/?page_id=119|title = R-120 Two place|accessdate = 19 May 2012|last = Bushcaddy|year = 2012|archiveurl = http://web.archive.org/web/20120208210657/http://bushcaddy.com/?page_id=119|archivedate = 8 February 2012 }}</ref>

The R-120 is a development of the [[Bushcaddy R-80]] and is supplied as a kit for [[Homebuilt aircraft|amateur construction]].<ref name="WDLA11" /><ref name="KitplanesDec2011" /><ref name="Specs" />

==Design and development==
The aircraft was designed to comply with the Canadian  and United States amateur-built aircraft rules. It features a [[strut-braced]] [[high-wing]], a two-seats-in-[[side-by-side configuration]] enclosed cockpit, fixed [[tricycle landing gear]] or [[conventional landing gear]] and a single engine in [[tractor configuration]]. The R-120 is structurally strengthened over the R-80 to allow a higher gross weight and accept larger engines.<ref name="WDLA11" /><ref name="KitplanesDec2011" /><ref name="Specs" />

The aircraft is made from [[6061-T6 aluminum]] sheet over a cage of welded aluminum square 6061-T6 aluminum tube. The tail boom is conventional [[semi-monocoque]] construction. The non-tapered planform wings have 6061-T6 [[Rib (aircraft)|ribs]] and [[Spar (aviation)|spars]] and employ a [[NACA]] 4413 (mod) [[airfoil]]. The airfoil modification removes the undercamber on the bottom of the wing, which makes construction easier, without giving up low speed performance. The aircraft's structure uses [[2024-T3 aluminium]] for critical parts where extra strength is required, such as the spar, float and strut attachments as well as other critical components like the rudder horns. The R-120's structure is covered with 6061-T6 sheet of varying thicknesses. The wings are supported by conventional "V" struts. 6061-T6 is predominantly used for its lower cost and also its better corrosion resistance, since many R-120s are flown on floats. Its {{convert|32|ft|m|1|abbr=on}} span wing has an area of {{convert|168|sqft|m2|abbr=on}}. [[Flap (aircraft)|Flaps]] are optional.<ref name="WDLA11" /><ref name="KitplanesDec2011" /><ref name="Specs" /><ref name="Hunt">Hunt, Adam: ''Elegant Utility - Flying the CLASS R-80 BushCaddy'', Canadian Flight July 2005, page B-1. [[Canadian Owners and Pilots Association]].</ref>

Standard engines used on the R-120 include the {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912UL]], the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]], the [[turbocharged]] {{convert|115|hp|kW|0|abbr=on}} [[Rotax 914]], the {{convert|100|hp|kW|0|abbr=on}} [[Continental O-200]], the {{convert|108|hp|kW|0|abbr=on}} [[Lycoming O-235]], the {{convert|120|hp|kW|0|abbr=on}} [[Jabiru 3300]] and other [[four-stroke]] powerplants. The aircraft can also be mounted on floats and skis. Controls include a central "Y" [[Centre stick|control stick]].<ref name="WDLA11" /><ref name="KitplanesDec2011" /><ref name="Specs" />

Construction time for the R-120 from the factory kit is 1,000 hours.<ref name="Specs" />
<!-- ==Operational history== -->

==Variants==
[[File:CLASS Bush Caddy R120 C-GZCC 01.JPG|thumb|right|CLASS R-120 BushCaddy on wheels]]
;CLASS R-120 BushCaddy
:Version produced by CLASS of [[Saint-Lazare, Quebec]]. Production was later moved to [[Les Cedres, Quebec]].
;Bushcaddy R-120
:Current production version produced by Bushcaddy of Lachute, Quebec after buying the rights from CLASS in 2011.<ref name="Specs" />
<!-- ==Aircraft on display== -->

==Specifications (R-120) ==
{{Aircraft specs
|ref=KitPlanes and Bushcaddy<ref name="KitplanesDec2011" /><ref name="Specs" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=22
|length in=1
|length note=
|span m=
|span ft=32
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=168
|wing area note=
|aspect ratio=
|airfoil=[[NACA]] 4413 (mod)
|empty weight kg=
|empty weight lb=832
|empty weight note=
|gross weight kg=
|gross weight lb=1500
|gross weight note=(1700 lbs (771kg) with factory approval)
|fuel capacity={{convert|155|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=110
|cruise speed kts=
|cruise speed note=at 75% power
|stall speed kmh=
|stall speed mph=34
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=130
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=16000
|ceiling note=
|g limits=+5.7/-3.8
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=800
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=8.9
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}
<!-- ==See also== -->

==References==
{{reflist}}

==External links==
{{Commons category|Bushcaddy}}
*[http://www.bushcaddy.com/model-r120.html Official website]
{{Bushcaddy aircraft}}

[[Category:Canadian ultralight aircraft 1990–1999]]
[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engine aircraft]]