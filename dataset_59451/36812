{{Good article}}
{{NHL Team
|team_name = Atlanta Flames
|text_color = #000000
|bg_color = background:#FFFFFF; border-top:#E03A3E 5px solid; border-bottom:#FDB727 5px solid;
|logo_image = Atlanta Flames Logo.svg
|founded = 1972
|history = '''Atlanta Flames'''<br />[[1972–73 NHL season|1972]]–[[1979–80 NHL season|1980]]<br />'''[[Calgary Flames]]'''<br />[[1980–81 NHL season|1980]]–present
|arena = [[Omni Coliseum]]
|city = [[Atlanta|Atlanta, Georgia]]
|team_colors = Red, yellow and white <br /> {{Color box|#E03A3E}} {{Color box|#FDB727}} {{Color box|#FFFFFF}}
|stanley_cups = '''0'''
|conf_titles = '''0'''
|division_titles = '''0'''
}}

The '''Atlanta Flames''' were a professional [[ice hockey]] team based in [[Atlanta|Atlanta, Georgia]] from 1972 until 1980. They played out of the [[Omni Coliseum]] and were members of the [[West Division (NHL)|West]] and later [[Patrick Division|Patrick]] divisions of the [[National Hockey League]] (NHL).  Along with the [[New York Islanders]], the Flames were created in 1971 as part of the NHL's conflict with the rival [[World Hockey Association]] (WHA). The team enjoyed modest success on the ice, qualifying for the post-season in six of its eight seasons, but failed to win a playoff series and won only two post-season games total. The franchise struggled to draw fans, and after averaging only 10,000 per game in [[1979–80 NHL season|1979–80]], was sold and relocated to [[Alberta]] to become the [[Calgary Flames]].

[[Eric Vail]] was the Flames' top goal scorer with 174 while [[Tom Lysiak]] led with 431 points.  [[Guy Chouinard]] was the lone player to score [[List of NHL players with 50-goal seasons|50 goals]] in one season. Goaltender [[Dan Bouchard]] led the team in wins (166) and [[shutout]]s (20). Two Flames players won the [[Calder Memorial Trophy]] as the NHL's top rookie: Vail in [[1974–75 NHL season|1974–75]] and [[Willi Plett]] in [[1975–76 NHL season|1975–76]].  [[Bob MacMillan]] won the [[Lady Byng Memorial Trophy]] as the most gentlemanly player in [[1978–79 NHL season|1978–79]]. General Manager [[Cliff Fletcher]] is the lone member of the Atlanta team to be named to the [[Hockey Hall of Fame]].

==History==

===Formation===
[[Image:Atlantaflamesgoalhug.jpg|thumb|250px|right|[[Tom Lysiak]] (left) celebrates with [[David Shand|Dave Shand]] and [[Harold Phillipoff]] after a goal against the [[Colorado Rockies (NHL)|Colorado Rockies]] in 1978. |alt=Two Atlanta players (Lysiak and Shand) embrace following a goal as Phillipoff skates in to join them.]]
The [[National Hockey League]] (NHL), which had grown from six teams in 1966 to fourteen in 1970, had not planned further expansion until at least 1973.  The formation of a rival major league (the [[World Hockey Association]] (WHA) in 1971) altered the NHL's plans and resulted in the two leagues battling for players and markets.<ref name="LongWar">{{citation |last=Mellor |first=Bob |url=https://news.google.com/newspapers?id=07oyAAAAIBAJ&sjid=wOwFAAAAIBAJ&pg=4878%2C3197514 |title=The start of a long war |work=Ottawa Citizen |date=1971-11-09 |accessdate=2012-08-26 |page=23}}</ref> The NHL sought to keep the WHA out of the newly constructed [[Nassau Veterans Memorial Coliseum|Nassau Coliseum]] in [[Long Island, New York]].<ref name="1971Expansion">{{citation |last=Bock |first=Hal |url=https://news.google.com/newspapers?id=owsrAAAAIBAJ&sjid=XZoFAAAAIBAJ&pg=2798,6860396 |title=NHL admits Long Island, Atlanta; sees two more |work=Reading Eagle |date=1971-11-10 |accessdate=2012-08-26 |page=65}}</ref> The league also opted to place a team in the American south.<ref name="2008ASGRetrospective">{{citation |last=McGourty |first=John |url=http://www.nhl.com/ice/news.htm?id=370370 |title=Former Flames recall hot times in Atlanta |publisher=National Hockey League |date=2008-01-24 |accessdate=2015-01-21}}</ref> The NHL announced on November 9, 1971, that it was expanding to Long Island and Atlanta.<ref name="1971Expansion" /> The Atlanta franchise was awarded to [[Tom Cousins]], who also owned the [[Atlanta Hawks]] basketball team, and would play out of the [[Omni Coliseum]].<ref name="Boer13">{{harvnb|Boer|2006|p=13}}</ref> The team cost $6 million.<ref name="LongWar" /> Cousins named the franchise the Flames in homage to the [[Atlanta in the American Civil War#The fall of Atlanta (Sept. 1 – 2, 1864)|burning of Atlanta]] by U.S. Army general [[William Sherman]] during the [[American Civil War]].<ref name="Boer13" />

The Flames hired [[Cliff Fletcher]], formerly of the [[St. Louis Blues]], to serve as the team's general manager.<ref>{{harvnb|Boer|2006|p=14}}</ref> Former [[Montreal Canadiens]] player [[Bernie Geoffrion]] was hired as the team's head coach.<ref>{{citation |last=Blackman |first=Ted |url=https://news.google.com/newspapers?id=yYIuAAAAIBAJ&sjid=O6EFAAAAIBAJ&pg=5476,1970634 |title=Boomer wanted to quit hockey—'but I got a helluva contract' |work=Montreal Gazette |date=1972-05-22 |accessdate=2012-08-26 |page=33}}</ref> The team stocked its roster via an [[1972 NHL Expansion Draft|expansion draft]] held on June 6, 1972. Fletcher focused on goaltending, choosing [[Phil Myre]] with his first selection and rookie [[Dan Bouchard]] with his second.<ref>{{citation |url=https://news.google.com/newspapers?id=e-0vAAAAIBAJ&sjid=j-wDAAAAIBAJ&pg=4155,3061612 |title=Expansion clubs draft for youth |work=Spokane Spokesman-Review |date=1972-06-07 |accessdate=2012-08-26 |page=16}}</ref> Fletcher drafted a competent roster, but one that was young and inexperienced.<ref>{{harvnb|Boer|2006|p=15}}</ref> Two days later, the Flames selected [[Jacques Richard]] as the second overall pick in the [[1972 NHL Amateur Draft]].<ref>{{citation |url=https://news.google.com/newspapers?id=029kAAAAIBAJ&sjid=in0NAAAAIBAJ&pg=1018%2C2949465 |title=Habs' Sammy again runs draft show |work=Calgary Herald |date=1972-06-09 |accessdate=2012-08-26 |page=33}}</ref>

===1972–1975===
The Flames made their NHL debut in Long Island against their expansion cousins, the [[New York Islanders]], on October 7, 1972.  They won the game 3–2; [[Morris Stefaniw]] scored the first goal in franchise history and the first NHL goal in [[Nassau Veterans Memorial Coliseum]].<ref>{{harvnb|Boer|2006|p=17}}</ref> The team made its home debut one week later on October 14.  Hosting the first event in Omni Coliseum history, the Flames tied the [[Buffalo Sabres]], 1–1, before a sellout crowd of 14,568.<ref>{{citation |last=Saladino |first=Tom |url=https://news.google.com/newspapers?id=QwJfAAAAIBAJ&sjid=aWQNAAAAIBAJ&pg=5153%2C4780248 |title=Omni opens in Atlanta with hockey reigning |work=Waycross Journal-Herald |date=1972-10-16 |accessdate=2012-08-27 |page=P9}}</ref> The team was respectable through much of the season on the strength of Bouchard and Myre's goaltending performances,<ref>{{harvnb|Boer|2006|p=18}}</ref> and by mid-January, had a 20–19–8 win-loss-tie record.  The Flames won only five more games through the rest of the season, finishing at 25–38–15.<ref name="0708MG144">{{harvnb|Hanlon|Kelso|2007|p=144}}</ref> Atlanta finished in seventh place in the [[West Division (NHL)|West Division]] and missed the playoffs.<ref name="0708MG105">{{harvnb|Hanlon|Kelso|2007|p=105}}</ref> The team was reasonably successful at the gate: it sold nearly 7,000 season tickets by the start of the season,<ref>{{citation |url=https://news.google.com/newspapers?id=vTYyAAAAIBAJ&sjid=O7cFAAAAIBAJ&pg=1114,3977954 |title=Atlanta opens complex |work=The Palm Beach Post |date=1972-10-08 |accessdate=2012-08-27 |page=E9}}</ref> and averaged 12,516 fans per game.<ref name="0708MG105p194">{{harvnb|Hanlon|Kelso|2007|p=194}}</ref>

[[Tom Lysiak]], selected second overall at the [[1973 NHL Amateur Draft]], joined the Flames for the [[1973–74 NHL season|1973–74 season]] and made an immediate impact.<ref>{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SearchPlayer.jsp?player=13449 |title=Tom Lysiak profile |publisher=Hockey Hall of Fame |accessdate=2012-08-27}}</ref> Lysiak led the Flames in scoring with 64 points and finished second to the Islanders' [[Denis Potvin]] in voting for the [[Calder Memorial Trophy]] as the NHL's top rookie.<ref>{{harvnb|Boer|2006|p=22}}</ref>  Improving to 30–34–14, the Flames finished fourth in the West and qualified for the [[1974 Stanley Cup playoffs]].<ref name="0708MG105p194" /> They made their post-season debut against the division-winning [[Philadelphia Flyers]]. The first game, played April 9, 1974, was a 4–1 victory for the Flyers.<ref>{{citation |url=https://news.google.com/newspapers?id=JEI_AAAAIBAJ&sjid=2EwMAAAAIBAJ&pg=982%2C3805095 |title=Flyers whip Flames 4–1 in opener |work=Ottawa Citizen |date=1974-04-10 |accessdate=2012-08-27 |page=25}}</ref> Philadelphia went on to defeat the Flames in their best-of-seven series with four consecutive wins.<ref name="0708MG219">{{harvnb|Hanlon|Kelso|2007|p=219}}</ref> Geoffrion was praised for his coaching of the club and finished second in voting for the [[Jack Adams Award]] as top coach.<ref>{{harvnb|Dowbiggin|Hanson|Short|1982|p=52}}</ref>

The NHL's expansion to 18 teams in [[1974–75 NHL season|1974–75]] resulted in realignment.  The league moved to a four division format, placing the Flames in the [[Patrick Division]].<ref>{{citation |url=https://news.google.com/newspapers?id=0B9OAAAAIBAJ&sjid=Ue0DAAAAIBAJ&pg=3224,2541374 |title=Name's the same, but little else |work=Spokane Spokesman-Review |date=1974-10-06 |accessdate=2012-08-27 |page=4}}</ref> Lysiak repeated as the Flames' top scorer with 77 points while [[Eric Vail]], playing his first full season, led with 39 goals.<ref name="0708MG195">{{harvnb|Hanlon|Kelso|2007|p=195}}</ref> Vail's total led all rookies and earned him the Calder Trophy.<ref>{{harvnb|Ornest|1980|p=27}}</ref> The team overcame an eight-game losing streak in December and injuries to several key players to post their first winning season with a 34–31–15 record.<ref>{{harvnb|Boer|2006|p=24}}</ref><ref name="8YearsinAtlanta">{{citation |title=Calgary Flames: their eight years in Atlanta |work=Calgary Herald |date=1980-10-08 |page=E10}}</ref> However, they finished fourth in the Patrick Division and failed to qualify for the post-season.<ref name="0708MG105" />  Citing personal reasons, Geoffrion resigned as head coach late in the season.  He was replaced with [[Fred Creighton]], who had been coaching the Flames' minor league affiliate, the [[Omaha Knights]].<ref>{{citation |url=https://news.google.com/newspapers?id=8HBEAAAAIBAJ&sjid=VbIMAAAAIBAJ&pg=1399,548577 |title=Geoffrion resigns as Flames' coach |work=Albany Herald |date=1975-02-04 |accessdate=2012-08-27 |page=8}}</ref> Fletcher later credited Geoffrion's outgoing personality as being the primary reason why people in Atlanta followed the Flames in the franchise's first seasons while the team's players later stated an appreciation for Creighton's more technical coaching and teaching style.<ref name="2008ASGRetrospective" />

===1975–1980===
[[File:Eric vail atlanta flames 1978.jpg|thumb|250px|left|[[Eric Vail]] battles for position in front of the net against the [[Colorado Rockies (NHL)|Colorado Rockies]] in 1978.|alt=Atlanta player Eric Vail and an unknown Colorado player press against each other as they battle for position in front of the Colorado net.]]
Creighton produced a consistent, but not outstanding team, as the Flames finished third in the Patrick for the following three seasons and typically won a few games more than they lost each year.<ref>{{harvnb|Boer|2006|p=25}}</ref>  The team qualified for the playoffs all three years, but lost in the preliminary round each time.<ref name="0708MG105" /> In [[1975-76 NHL season|1975–76]], they were defeated by the [[Los Angeles Kings]] in a best of three series 2 games to 0.  The Kings again eliminated the Flames in [[1976-77 NHL season|1976–77]], but Atlanta earned its first playoff victory in franchise history in the second game of the series.<ref name="0708MG219" />  Vail scored the game-winning goal in a 3–2 victory over the Kings on April 7, 1977,<ref>{{citation |url=https://news.google.com/newspapers?nid=348&dat=19770408&id=v-wtAAAAIBAJ&sjid=_zIDAAAAIBAJ&pg=6819,1050224 |title=Flames break jinx, square series with Kings |work=Rome News-Tribune |date=1977-04-08 |accessdate=2014-01-21 |page=7A}}</ref> but the Flames were eliminated in the third game.<ref name="0708MG219" /> [[1975 NHL Amateur Draft|1975 draft pick]] [[Willi Plett]] emerged as a young star for the Flames. He scored 33 goals in his rookie season of 1976–77 and won the Calder Trophy.<ref name="Podnieks682">{{harvnb|Podnieks|2003|p=682}}</ref>

Seeking to improve his team's fortunes, Fletcher made several moves over the following seasons to rework the Flames roster. His goaltending tandem of Bouchard and Myre had begun to feud with each other by the [[1977–78 NHL season|1977–78 season]] as both sought more playing time. Fletcher responded by naming Bouchard his number one goaltender and trading Myre to the St. Louis Blues for three players. They made it into the playoffs again but were the only team to fall to a team with fewer points than them, the [[Detroit Red Wings]], in a best-of-three series, 2 games to 0.<ref>{{harvnb|Boer|2006|p=26}}</ref> In March 1979, Fletcher completed an eight player trade that sent franchise-leading scorer Tom Lysiak and four players to the [[Chicago Blackhawks|Chicago Black Hawks]] for three players, led by defenseman [[Phil Russell (ice hockey)|Phil Russell]].<ref>{{citation |url=https://news.google.com/newspapers?id=fgxgAAAAIBAJ&sjid=P-kFAAAAIBAJ&pg=4847,2316173 |title=Flames trade Lysiak to Chicago |work=The Miami News |date=1979-03-14 |accessdate=2012-08-29 |page=1C}}</ref> Fletcher hoped that the addition of Russell would help his team achieve playoff success.<ref>{{citation |last=Cole |first=Glenn |url=https://news.google.com/newspapers?id=xJYuAAAAIBAJ&sjid=haEFAAAAIBAJ&pg=6572%2C2394059 |title=The 'Trade' still burning issue in Atlanta |work=Montreal Gazette |date=1979-03-27 |accessdate=2012-08-29 |page=17}}</ref>

Buoyed by a franchise record ten-game winning streak in October 1978,<ref name="8YearsinAtlanta" /> the [[1978–79 NHL season|1978–79]] Flames posted the best record in their Atlanta years at 41–31–8.<ref name="0708MG105" /> [[Bob MacMillan]], acquired in the Myre deal, became the first Flame other than Lysiak to lead the team in scoring in six years and, along with [[Guy Chouinard]], was one of the first two Flames' players to score [[List of NHL players with 100-point seasons|100 points]] in one season.<ref name="0708MG105" /><ref>{{harvnb|Boer|2006|p=27}}</ref> Chouinard also became the team's first [[List of NHL players with 50-goal seasons|50-goal scorer]].<ref>{{citation |url=https://news.google.com/newspapers?id=k_YjAAAAIBAJ&sjid=Hu4DAAAAIBAJ&pg=6680,3357393 |title=Atlanta routs Rangers |work=Spokane Spokesman-Review |date=1979-04-07 |accessdate=2012-08-29 |page=21}}</ref> MacMillan won the [[Lady Byng Memorial Trophy]] that season as the NHL's most gentlemanly player.<ref name="0708MG23">{{harvnb|Hanlon|Kelso|2007|p=23}}</ref>  In the playoffs against the [[Toronto Maple Leafs]], Atlanta again failed to win a game as they lost a best of three series 2 games to 0.<ref name="0708MG219" />

Fletcher continued to alter his team's make-up throughout the [[1979–80 NHL season|1979–80]]. [[Al MacNeil]] replaced Creighton as head coach prior to the season,<ref>{{citation |url=https://news.google.com/newspapers?id=tUQNAAAAIBAJ&sjid=ym0DAAAAIBAJ&pg=7172,1037141 |title=Flames name Al MacNeil as new coach |work=Pittsburgh Post-Gazette |date=1979-06-08 |accessdate=2012-08-29 |page=12}}</ref> and the team acquired Swedish star [[Kent Nilsson]] following the demise of the WHA. Nilsson led Atlanta in scoring with 40 goals and 53 assists.<ref>{{harvnb|Boer|2006|pp=28–29}}</ref> At the [[1979 NHL Entry Draft]], Fletcher selected four players – [[Paul Reinhart]], [[Jim Peplinski]], [[Pat Riggin]] and [[Tim Hunter (ice hockey)|Tim Hunter]] – who would ultimately become regulars in the Flames line up.<ref>{{harvnb|Boer|2006|p=28}}</ref> However, while the Flames again qualified for the playoffs in 1980, they again lost in the first round, losing a best-of-five series to the [[New York Rangers]] three games to one.<ref name="0708MG219" />

===Relocation===
[[File:Dion Phaneuf 2008.JPG|thumb|right|The Calgary Flames recognize their tenure in Atlanta by using the "Flaming A"' logo to denote alternate captains, as seen here by alternate captain, [[Dion Phaneuf]].|alt=A Calgary Flames player observes his teammates who are off camera.  On his uniform is a small patch that uses Atlanta's "Flaming A" logo to denote his position as an alternate captain.]]
As the team stagnated on the ice, the Flames struggled at the gate. They peaked at an average of 14,161 fans per game in their second season, 1973–74, but fell to 12,258 three years later and then 10,500 in 1977–78.<ref name="8YearsinAtlanta" />  Concerns that low attendance could result in the relocation of the team surfaced by 1976, prompting politicians and the players themselves to purchase tickets in a bid to stabilize the franchise.<ref>{{citation |url=https://news.google.com/newspapers?id=YXE0AAAAIBAJ&sjid=jMgEAAAAIBAJ&pg=4069,4804184 |title=Flames buying own tickets |work=Florence Times |date=1976-12-22 |accessdate=2012-08-29 |page=10}}</ref> The Flames attempted to boost attendance in 1980 by signing [[Jim Craig (ice hockey)|Jim Craig]], goaltender of the [[United States men's national ice hockey team|American Olympic team]] that had won the [[Ice hockey at the 1980 Winter Olympics|Olympic gold medal]] following its "[[Miracle on Ice]]" victory over the Soviet Union.<ref>{{citation |url=https://news.google.com/newspapers?id=B2JEAAAAIBAJ&sjid=PLIMAAAAIBAJ&pg=3918,5243328 |title=Flames sign Jim Craig |work=Albany Herald |date=1980-02-28 |accessdate=2012-08-29 |page=1D}}</ref> It was not successful as attendance fell to an average of 10,024.<ref name="8YearsinAtlanta" /> Adding to the Flames' financial woes was the fact that the Omni Coliseum was one of the last major arenas in North America to be built without revenue-generating luxury suites, which led Fletcher to describe the facility as being "out-of-date when it opened".<ref name="2008ASGRetrospective" />

Cousins announced that he was seeking to sell the club following the Flames' exit from the playoffs;<ref name="CousinsSelling" /> Their final game, a 5–2 loss, was played in Atlanta on April 12, 1980.<ref>{{harvnb|Boer|2006|p=29}}</ref>  He claimed to have suffered significant financial losses on the team while low viewership hampered his ability to sign a television contract.<ref name="CousinsSelling">{{citation |url=https://news.google.com/newspapers?id=PTkyAAAAIBAJ&sjid=eKQFAAAAIBAJ&pg=2283,2770063 |title=Flames boss serious about selling franchise |work=Montreal Gazette |date=1980-04-16 |accessdate=2012-08-30 |page=59}}</ref> The Flames, estimated to have lost $12 million in its eight years, had been rumored for months to be moving to [[Calgary]], though [[Dallas]] and [[Houston]] were also mentioned as possible destinations.<ref>{{citation |last=Wood |first=Larry |url=https://news.google.com/newspapers?id=SnVkAAAAIBAJ&sjid=LH4NAAAAIBAJ&pg=5187,2420057 |title=In deep south, these Flames are barely embers |work=Calgary Herald |date=1980-01-15 |accessdate=2012-08-30 |page=C1}}</ref>

The Seaman brothers, [[Daryl Seaman|Daryl]] and [[Byron Seaman|Byron]], had made an offer of $14 million while the City of Calgary prepared to build a new arena for the team.<ref>{{citation |last=Bilych |first=George |url=https://news.google.com/newspapers?id=d3RkAAAAIBAJ&sjid=c34NAAAAIBAJ&pg=1474,3868477 |title=Seamans bailing Cousins from a sea of red ink |work=Calgary Herald |date=1980-04-16 |accessdate=2012-08-30 |page=C1}}</ref> However, Canadian businessman [[Nelson Skalbania]] emerged as a rival bidder for the team before joining the Calgary consortium. The group agreed to purchase the Flames for $16 million, at the time the highest price ever paid for a National Hockey League franchise.<ref>{{harvnb|Boer|2006|p=37}}</ref> The sale was announced on May 21, 1980, and the franchise relocated to Canada where it became the [[Calgary Flames]].<ref>{{harvnb|Hanlon|Kelso|2007|p=4}}</ref> The Flames have since used the Atlanta logo for both its [[captain (ice hockey)|alternate captains]],<ref>{{cite web|url=http://www.cbssports.com/nhl/eye-on-hockey/23298077/nhl-alltime-teams-calgary-flames|title=NHL All-time teams: Calgary Flames|first= Brian |last=Stubits |publisher=CBSSports.com|date=2013-08-24|accessdate=2015-03-02}}</ref> and the team's former affiliate that played in the [[American Hockey League]], the [[Adirondack Flames]].<ref>{{cite web|url=http://flames.nhl.com/club/news.htm?id=719518|title=Flames announce Adirondack Flames as AHL affiliate|date=2014-05-16|publisher=CalgaryFlames.com|accessdate=2015-03-02}}</ref>

The last active Atlanta Flames player in the NHL was Kent Nilsson, who played his final game in [[1994-1995 NHL season|1995]]. Several former players of the team returned to Atlanta once their careers ended.<ref>{{citation |last=Gilbertson |first=Wes |url=http://www.lfpress.com/sports/hockey/2011/06/01/18225876.html |title=Ex-Flames recall Atlanta move |work=London Free Press |date=2011-06-01 |accessdate=2012-08-30}}</ref>  Among them, Tom Lysiak operated a horse farm outside the city,<ref>{{harvnb|Podnieks|2003|p=517}}</ref> Eric Vail returned to operate a nightclub,<ref>{{harvnb|Podnieks|2003|p=877}}</ref> and Willi Plett operated a sporting theme park and golf course.<ref name="Podnieks682" />

== Season-by-season record ==
'''''Note:''' GP = Games played, W = Wins, L = Losses, T = Ties, Pts = Points, GF = Goals for, GA = Goals against, PIM = Penalties in minutes''<ref name="AtlantaLeaders" />
{| class="wikitable"
|- style="font-weight:bold; background:#ddd;" |
|Season || GP || W || L || T || Pts || GF || GA || PIM || Finish || Playoffs
|-
|[[1972–73 NHL season|1972–73]] || 78 || 25 || 38 || 15 || 65 || 191 || 239 || 852||7th, West || Did not qualify
|-bgcolor="#eeeeee"
| [[1973–74 NHL season|1973–74]] || 78 || 30 || 34 || 14 || 74 || 214 || 238 || 841||4th, West || Lost in Quarterfinals, 0–4 ([[Philadelphia Flyers|Flyers]]) 
|-
| [[1974–75 NHL season|1974–75]] || 80 || 34 || 31 || 15 || 83 || 243 || 233 || 915||4th, Patrick || Did not qualify
|-bgcolor="#eeeeee"
| [[1975–76 NHL season|1975–76]] || 80 || 35 || 33 || 12 || 82 || 262 || 237 || 928||3rd, Patrick || Lost in Preliminary Round, 0–2 ([[Los Angeles Kings|Kings]])
|-
| [[1976–77 NHL season|1976–77]] || 80 || 34 || 34 || 12 || 80 || 264 || 265 || 889||3rd, Patrick || Lost in Preliminary Round, 1–2 ([[Los Angeles Kings|Kings]])
|-bgcolor="#eeeeee"
| [[1977–78 NHL season|1977–78]] || 80 || 34 || 27 || 19 || 87 || 274 || 252 || 984||3rd, Patrick || Lost in Preliminary Round, 0–2 ([[Detroit Red Wings|Red Wings]])
|-
| [[1978–79 NHL season|1978–79]] || 80 || 41 || 31 || 8 || 90 || 327 || 280 || 1158||4th, Patrick || Lost in Preliminary Round, 0–2 ([[Toronto Maple Leafs|Maple Leafs]])
|-bgcolor="#eeeeee"
| [[1979–80 NHL season|1979–80]] || 80 || 35 || 32 || 13 || 83 || 282 || 269 || 1048||4th, Patrick || Lost in Preliminary Round, 1–3 ([[New York Rangers|Rangers]])
|- bgcolor="#dddddd"   
! Totals || 636 || 268 || 260 || 108 || 644 || 2057 || 2013 || 7615 || ||Six playoff appearances; 2–15 record
|}

==Notable personnel==

===Team captains===
* [[Keith McCreary]] 1972–1975<ref name="0708MG103">{{harvnb|Hanlon|Kelso|2007|p=103}}</ref>
* [[Pat Quinn (ice hockey)|Pat Quinn]] 1975–1977<ref name="0708MG103" />
* [[Tom Lysiak]] 1977–1979<ref name="0708MG103" />
* [[Jean Pronovost]] 1979–1980<ref name="0708MG103" />

===Award winners===
Three members of the Flames were named recipients of NHL awards during the team's tenure in Atlanta.  Eric Vail was the first, as he won the Calder Memorial Trophy as the top rookie in 1974–75 after scoring 39 goals and finishing with 60 points.<ref>{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=CMT&year=1974-75 |title=Calder Memorial Trophy winner – Eric Vail |publisher=Hockey Hall of Fame |accessdate=2015-01-22}}</ref> Paraguayan-born Willi Plett won the award two years later after scoring 33 goals and 23 assists in his first full NHL season.<ref>{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=CMT&year=1976-77 |title=Calder Memorial Trophy winner – Willi Plett |publisher=Hockey Hall of Fame |accessdate=2015-01-22}}</ref> Bob MacMillan was named the league's most gentlemanly player in 1978–79, which earned him the Lady Byng Memorial Trophy. He finished fifth overall in league scoring with 104 points while accruing only 14 [[penalty (ice hockey)|penalty minutes]] throughout the season.<ref>{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=LBT&year=1978-79 |title=Lady Byng Memorial Trophy winner – Bob MacMillan |publisher=Hockey Hall of Fame |accessdate=2015-01-22}}</ref>

Seven players represented the Flames at the [[NHL All-Star Game]].  [[Randy Manery]] became the team's first all-star when he played in the [[26th National Hockey League All-Star Game|1973]] contest. He was subsequently joined by [[Al McDonough]] ([[27th National Hockey League All-Star Game|1974]]), Tom Lysiak ([[28th National Hockey League All-Star Game|1975]], [[29th National Hockey League All-Star Game|1976]] and [[30th National Hockey League All-Star Game|1977]]), [[Curt Bennett]] (1975 and 1976), Eric Vail (1977), [[Bill Clement]] ([[31st National Hockey League All-Star Game|1978]]) and Kent Nilsson ([[32nd National Hockey League All-Star Game|1980]]).<ref>{{harvnb|Hanlon|Kelso|2007|p=22}}</ref>

===Hockey Hall of Fame===
There are three members of the Atlanta Flames organization to be enshrined in the [[Hockey Hall of Fame]]. Cliff Fletcher. A native of [[Montreal]], Fletcher began his career in hockey management as a scout for the Montreal Canadiens in 1956 and rose to the position of assistant general manager with the St. Louis Blues before being hired in 1972 as the inaugural and lone general manager of the Atlanta Flames. Fletcher remained with the organization for another 11 years following its transition to Calgary and was the architect of the franchise's lone [[Stanley Cup]] championship, in 1989. He was inducted into the Hockey Hall of Fame in 2004 as a builder.<ref>{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=b200401&page=bio |title=Cliff Fletcher biography |publisher=Hockey Hall of Fame |accessdate=2015-01-22}}</ref>

Pat Quinn played with the Atlanta Flames from 1972 to 1977 was inducted as a builder for coaching various teams around the league. The Flames first coach [[Bernie Geoffrion]] was also inducted into the player category in 1972, the same year he joined the Flames organization.

==Scoring leaders==
These are the top ten scorers for the franchise during its time in Atlanta.<ref name="AtlantaLeaders">{{harvnb|Hanlon|Kelso|2007|p=132}}</ref>

'''''Note:''' GP = Games played, G = Goals, A = Assists, Pts = Points, PIM = Penalties in minutes''
{| border="1" class="wikitable"
|-
! Player !! POS !! GP !! G !! A !! Pts !! PIM
|- style="text-align:center;"
| align="left" | [[Tom Lysiak]] || C || 445 || 155 || 276 || 431 || 329
|- style="text-align:center; background:#eee;"
| align="left" | [[Eric Vail]] || LW || 469 || 174 || 209 || 383 || 223
|- style="text-align:center;"
| align="left" | [[Guy Chouinard]] || F || 318 || 126 || 168 || 294 || 56
|- style="text-align:center; background:#eee;"
| align="left" | [[Curt Bennett]] || C || 405 || 126 || 140 || 266 || 190
|- style="text-align:center;"
| align="left" | [[Bob MacMillan]] || RW || 208 || 90 || 131 || 221 || 50
|- style="text-align:center; background:#eee;"
| align="left" | [[Rey Comeau]] || F || 468 || 88 || 126 || 214 || 153
|- style="text-align:center;"
| align="left" | [[Ken Houston (ice hockey)|Ken Houston]] || RW || 350 || 91 || 108 || 199 || 332
|- style="text-align:center; background:#eee;"
| align="left" | [[Bill Clement]] || C || 297 || 69 || 107 || 176 || 136
|- style="text-align:center;"
| align="left" | [[Willi Plett]] || RW || 296 || 91 || 83 || 174 || 738
|- style="text-align:center; background:#eee;"
| align="left" | [[Randy Manery]] || D || 377 || 30 || 142 || 172 || 242
|}

==Individual records==

===Single-season===
*Most goals: [[Guy Chouinard]], 50 (1978–79)<ref name="198081FlamesMG70">{{harvnb|Ornest|1980|p=70}}</ref>
*Most assists: [[Bob MacMillan]], 71 (1978–79)<ref name="198081FlamesMG70" />
*Most points: Bob MacMillan, 108 (1978–79)<ref name="198081FlamesMG70" />
*Most penalty minutes: [[Willi Plett]], 231 (1979–80)<ref name="198081FlamesMG71">{{harvnb|Ornest|1980|p=71}}</ref>
*Most points, defenseman: [[Paul Reinhart]], 47 (1979–80)<ref name="198081FlamesMG70" />
*Most points, rookie: [[Tom Lysiak]], 64 (1973–74)<ref name="198081FlamesMG70" />
*Most wins: [[Dan Bouchard]], 32 (1978–79)<ref name="198081FlamesMG71" />

===Career===
* Games: [[Eric Vail]], 469<ref>{{harvnb|Ornest|1980|p=73}}</ref>
* Goals: Eric Vail, 174<ref name="198081FlamesMG67">{{harvnb|Ornest|1980|p=67}}</ref>
* Assists: [[Tom Lysiak]], 276<ref name="198081FlamesMG67" />
* Points: Tom Lysiak, 431<ref name="198081FlamesMG67" />
* Penalty minutes: [[Willi Plett]], 738<ref name="198081FlamesMG67" />
* Goaltender games: [[Dan Bouchard]], 384<ref name="198081FlamesMG74">{{harvnb|Ornest|1980|p=74}}</ref>
* Goaltender wins: Dan Bouchard, 164<ref name="198081FlamesMG74" />
* Shutouts: Dan Bouchard, 20<ref name="198081FlamesMG74" />

== References ==
'''Footnotes'''
{{reflist|30em}}

{{Commons category|Atlanta Flames}}
'''General'''
*{{citation |last=Boer |first=Peter |title=The Calgary Flames: The Hottest Players & Greatest Games |year=2006 |publisher=Overtime Books |isbn=1-897277-07-5}}
*{{citation |last=Dowbiggin |first=William F. |last2=Hanson |first2=George |last3=Short |first3=John |title=Fire on Ice: The Flames |year=1982 |publisher=Executive Sport Publications |isbn=9780019035125}}
*{{citation |editor-last=Hanlon |editor-first=Peter |editor2-last=Kelso |editor2-first=Sean |title=2007–08 Calgary Flames Media Guide |publisher=Calgary Flames Hockey Club  |year=2007 |location=Calgary}}
*{{citation |editor-last=Ornest |editor-first=Leo |title=1980–81 Calgary Flames Fact Book |publisher=Calgary Flames Hockey Club |year=1980}}
*{{citation |last=Podnieks |first=Andrew |title=Players: The ultimate A–Z guide of everyone who has ever played in the NHL |publisher=Doubleday Canada |year=2003 |location=Toronto |isbn=0-385-25999-9}}

{{Atlanta Flames}}
{{Atlanta Flames seasons}}
{{NHLdefunct}}
{{NHL}}

[[Category:1972 establishments in Georgia (U.S. state)]]
[[Category:1980 disestablishments in Georgia (U.S. state)]]
[[Category:Atlanta Flames| ]]
[[Category:Defunct National Hockey League teams]]
[[Category:Ice hockey teams in Georgia (U.S. state)]]
[[Category:Sports in Atlanta]]
[[Category:Ice hockey clubs established in 1972]]
[[Category:Sports clubs disestablished in 1980]]
[[Category:National Hockey League in Atlanta|Flames]]