{{Infobox journal
| title = Animal Biology
| formernames = Archives Néerlandaises de Zoologie, Netherlands Journal of Zoology
| cover = [[File:Abcover.gif]]
| editor = M. Muller
| discipline = [[Zoology]]
| abbreviation = Anim. Biol.
| publisher = [[Brill Publishers]] on behalf of the [[Royal Dutch Zoological Society]]
| country = The Netherlands
| frequency = Quarterly
| history = 1872–present
| openaccess =
| license =
| impact = 0.617
| impact-year = 2015
| website = http://www.brill.nl/ab
| link1 = http://brill.publisher.ingentaconnect.com/content/brill/ab
| link1-name = Online access
| link2 = http://www.ingentaconnect.com/content/brill/njz
| link2-name = Online access to ''Netherlands Journal of Zoology'' (1967–2003)
| link3 = http://www.ingentaconnect.com/content/brill/anz
| link3-name = Online access to ''Archives Néerlandaises de Zoologie'' (1934–1966)
| JSTOR =
| OCLC = 53185481
| LCCN =
| CODEN =
| ISSN = 1570-7555
| eISSN = 1570-7563
}}
'''''Animal Biology''''' is a [[peer review|peer-reviewed]] [[scientific journal]] in the field of [[zoology]]. It is the official journal of the ''Koninklijke Nederlandse Dierkundige Vereniging'' (Royal Dutch Zoological Society)<ref name=KNDV>{{cite web |url=http://www.kndv.nl/ |title=KNDV homepage |format= |work= |accessdate=2009-09-20}}</ref> and published on behalf of the society by [[Brill Publishers]]. The journal was established in 1872 as the ''Archives Néerlandaises de Zoologie'' and renamed ''Netherlands Journal of Zoology'' in 1967. Since 2004 it is known under its current name.

== Abstracting and indexing ==
According to the ''[[Journal Citation Reports]]'', the journal's 2010 [[impact factor]] is 0.879<ref name="WoS">{{cite web |url = http://isiwebofknowledge.com |title = Web of Science |year = 2009 |accessdate = 2011-02-20}}</ref> and it is indexed in [[BIOSIS]], [[Elsevier BIOBASE - Current Awareness in Biological Sciences|CABS]], [[Current Contents]]/Agriculture, Biology and Environmental Sciences, [[FISHLIT]], [[GeoAbstracts]],'' ''[[Science Citation Index]],'' and ''[[Scopus]].''<ref name=AB>{{cite web |url=http://www.animal-biology.com/ |title=''Animal Biology'', Journal of the Royal Dutch Zoological Society |work= |accessdate=2009-09-20}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.brill.nl/ab}}

[[Category:Zoology journals]]
[[Category:Publications established in 1872]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Brill Publishers academic journals]]
[[Category:Academic journals associated with learned and professional societies]]