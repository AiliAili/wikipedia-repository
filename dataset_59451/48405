{{EngvarB|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{COI|date=July 2014}}
{{Infobox person
| name = Roomi Saeed Hayat
| image = CEO at work.jpg
| image_size = 250px
| caption = Roomi Saeed Hayat at IRM head office in Islamabad
| residence = [[Pakistan]]
| education = Bachelor of Engineering, Master of Science
| occupation = Trainer, entrepreneur
| notable_works = 
| boards = 
}}
'''Roomi Saeed Hayat''' is a [[social entrepreneurship|social entrepreneur]] and [[chief executive officer]] of the Institute of Rural Management. Hayat is also the founding chairperson of the Human Resource Development Network.<ref name="archives.dawn.com">[http://archives.dawn.com/2007/09/08/nat14.htm Roomi Hayat speaking about the Role of HRDN], Press Coverage in Dawn News, 8 September 2007</ref> For the last 20 years, he has been associated with the field of capacity development on institutional and rural development, and employable skills for youth.<ref name="dawn2012">[http://www.dawn.com/news/688279/profile-training-to-win Profile: Training to Win], Interview of Roomi Hayat in ''Dawn News,'' 15 January 2012</ref>

==Biography==
Hayat was born to a [[Pathan]] family in [[Kohat]], Pakistan. He attended St Mary's High School and then Pakistan Air Force Degree College, [[Peshawar]].<ref name="dawn2012" /> He completed his [[Bachelors in Engineering]] at [[Peshawar University]] and then joined the [[Pakistan Army]] for three years.<ref name="dawn2012" /> Hayat later studied for a [[Master's Degree]] in robotics from the [[New Jersey Institute of Technology]].<ref name="dawn2012" />{{better source needed|date=February 2015}}

==Career== 
After leaving the army, Hayat became joint director of training and research of the Small Industries Development Board of the Government of [[Khyber-Pakhtunkhwa]].{{cn|date=February 2015}} He joined the National Rural Support Programme in 1992.<ref>[http://csrc.org.pk/wp-content/uploads/2011/08/Pages-from-Journal-Dec2003_NoRestriction-3.pdf Quality Conscious Training Institutes], views of Roomi Hayat published in NGORC Journal 2003</ref><ref>[http://www.docstoc.com/docs/48136157/HUMAN-RESOURCES-DEVELOPMENT-SECTION ESCAP HRD course on Poverty Alleviation- based on Hayat's publications]</ref>

===Institute of Rural Management===
He set up a one-room human resource development organisation, which later became the Institute of Rural Management (IRM), and initiated vocational and technical training. In 2010, IRM was registered as a separate legal entity and underwent several strategic transformations. Hayat was appointed the first chief executive officer of the IRM.<ref>[http://www.dailytimes.com.pk/default.asp?page=2010\07\09\story_9-7-2010_pg5_2 IRM Registration with SECP]  {{webarchive |url=https://web.archive.org/web/20100714043105/http://www.dailytimes.com.pk/default.asp?page=2010\07\09\story_9-7-2010_pg5_2 |date=14 July 2010 }}</ref><ref>[http://www.secp.gov.pk/news/pdf/news_10/licence_nbfc.pdf Securities & Exchange Commission of Pakistan Grants Registration license to the NPOs]</ref> Under his leadership, IRM evolved into a substantial training institute, having trained more than 1 million people with an annual turnout of over 50,000 trainees.<ref name="archives.dawn.com"/><ref>{{cite web|url=http://www.thenews.com.pk/TodaysPrintDetail.aspx?ID=90532&Cat=6|title=IRM imparts training to one million people|date=1 February 2012|work=The News International, Pakistan|accessdate=4 February 2015}}</ref>
 
As of 2014, IRM has 15 offices across Pakistan, including six vocational and technical education centers in [[Punjab, Pakistan|Punjab]], [[Sindh]] and [[Khyber Pakhtunkhwa]]. <ref>{{cite web|url=http://www.vtec.edu.pk|title=:- Welcome to VTEC -:|work=vtec.edu.pk|accessdate=4 February 2015}}</ref>

===Human Resource Development Network===
In 1997, Hayat along with other like minded professionals co-founded the Human Resource Development Network.<ref>{{cite web|url=http://www.hrdn.net/home/founder|title=Founding Members|work=hrdn.net|accessdate=4 February 2015}}</ref><ref name="archives.dawn.com" /><ref>[http://www.dailytimes.com.pk/print.asp?page=2006\11\26\story_26-11-2006_pg11_6 “Civil society organizations key to improvement”], Roomi Hayat speaking on International conference on Human Security and Social Development, ''Daily Times,'' 26 November 2006 {{dead link|date=February 2015}}</ref> The network was intended to bridge the gap between development professionals and organisations.<ref>[http://www.docstoc.com/docs/48612868/7th-Annual-Trainers-Retreat-Swat---Pakistan-June-10---12-2005 7th Annual Trainers Retreat]  {{webarchive |url=https://web.archive.org/web/20140517121014/http://www.docstoc.com/docs/48612868/7th-Annual-Trainers-Retreat-Swat---Pakistan-June-10---12-2005 |date=17 May 2014 }}</ref> Hayat serves as the chairperson of the HRDN.<ref>{{cite web|url=http://www.hrdn.net/home/content/23|title=Chainman's Message|work=hrdn.net|accessdate=4 February 2015}}</ref>

==References==
{{reflist|2}}

{{DEFAULTSORT:Hayat, Roomi S.}}
[[Category:Living people]]
[[Category:Pashtun people]]
[[Category:People from Kohat District]]
[[Category:Pakistani social scientists]]
[[Category:University of Peshawar alumni]]