{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
{{For|the motor vehicle produced in Australia 1974–1976|Holden HJ}}

'''Henry James Holden''' (18 July 1859 – 6 March 1926), commonly known as "H. J. Holden" was an Australian businessman, a partner in Holden & Frost, which became the automobile manufacturer Holden. He was a longstanding member of the [[Kensington and Norwood Corporation]], and served as mayor for a total of nine years.

==History==
[[File:H. J. Holden (drawing).jpg|thumb|Henry James Holden, Mayor of Kensington and Norwood, South Australia]]
Henry was born in Adelaide the eldest child of [[James Alexander Holden]] (1 April 1835 – 1 June 1887) and his wife Mary Elizabeth Holden, née Phillips (9 December 1839 – 17 April 1914). He was educated at the Norwood College run by [[Thomas Caterer]], then at [[Hahndorf College]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article59935892 |title=Mayors for the Year |newspaper=[[The Register (Adelaide)]] |volume=LXXIX, |issue=21,234 |location=South Australia |date=30 November 1914 |accessdate=9 March 2016 |page=6 |via=National Library of Australia}}</ref>

In August 1885 J. A. Holden sold to Henry and to [[H. A. Frost]] his interest in the retail arm of the company,<ref name=unload>{{cite news |url=http://nla.gov.au/nla.news-article44949108 |title=Advertising |newspaper=[[South Australian Register]] |volume=L, |issue=12,092 |location=South Australia |date=15 August 1885 |accessdate=28 February 2016 |page=1 |via=National Library of Australia}}</ref> which was in financial difficulties. In November 1885 the company became '''Holden & Frost''' with the formal introduction into the partnership of  Frost, who had brought to the company additional capital.

In 1886 J. A. Holden was declared insolvent.<ref>{{cite news |url=http://nla.gov.au/nla.news-article93849558 |title=Commercial. |newspaper=[[South Australian Weekly Chronicle]] |volume=XXVIII, |issue=1,439 |location=South Australia |date=20 March 1886 |accessdate=27 February 2016 |page=3 |via=National Library of Australia}}</ref> He put the wholesale arm of the business on the open market as well as the Grenfell Street warehouse.<ref name=unload/> He put his Kensington Park house and grounds on the market for urgent sale.<ref>{{cite news |url=http://nla.gov.au/nla.news-article44948508 |title=Advertising |newspaper=[[South Australian Register]] |volume=L, |issue=12,093 |location=South Australia |date=17 August 1885 |accessdate=28 February 2016 |page=8 |via=National Library of Australia}}</ref> Under the terms of the voluntary liquidation, Holden & Frost continued to operate from the premises at 100 Grenfell Street, which was later purchased by [[Harris, Scarfe & Co.]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article132049888 |title=Motor Body Building |newspaper=[[The News (Adelaide)|The News]] |volume=VI, |issue=782 |location=South Australia |date=26 January 1926 |accessdate=27 February 2016 |page=10 (Home) |via=National Library of Australia}}</ref>

In 1899, with the advent of the Boer War, there arose a sudden demand for saddles, harness, leggings, [[Sam Browne belt]]s and so on. Holden & Frost were quick to purchase new machinery and rent additional premises in Norwood, and their business thrived.<ref name=family>{{cite web|url=http://www.hrc.org.au/images/Memories/Marc_McInnes/The%20Holden%20Family%20Retiree%20Presentation%202%20OPT.pdf|title=The Holden Family|author=Marc McInnes|accessdate=27 February 2016}} An extensive and interesting article spoiled by a few typo's.</ref>

In 1905 Henry's son [[Edward Wheewall Holden]] graduated [[B.Eng]] from Adelaide University, and was admitted to the company. He foresaw the decline in horse transport, and seeing a future for the company in motor vehicles, encouraged his father to visit the United States to observe automobile manufacturing. Edward immediately began making fabric hoods and side-curtains for motor cars, and purchased his own car in 1913.<ref>D.A. Cummings, G. Moxham ''They Built South Australia'' Published by the authors 1986 ISBN 0 9589111 0 X</ref>

During World War I, in an austerity drive, the Australian Government put an embargo on the import of motor vehicles, but left open the importation of motor chassis. This left an opening for motor body builders to supply new cars to their wealthy clients, and Holden & Frost, though slow to seize the opportunity, made the most of it.<ref>{{cite news |url=http://nla.gov.au/nla.news-article62194027 |title=Where South Australia Leads |newspaper=[[The Register (Adelaide)]] |volume=LXXXIV, |issue=22,635 |location=South Australia |date=27 May 1919 |accessdate=8 March 2016 |page=2 |via=National Library of Australia}}</ref> Around August 1917 Holden & Frost began advertising for workers for their motor body building department at Grenfell Street, and took over the business of Fred. T. Hack Limited<ref>Frederick Theodore Hack (1877–1939) was a fine batsman and grandson of [[John Barton Hack]]. Two sons were also noted cricketers.</ref> on King William Street (until 1913 Hack & Pengilly of 50–52 Flinders Street) for ₤9,000.<ref>{{cite news |url=http://nla.gov.au/nla.news-article57290391 |title=A Romance of Industry |newspaper=[[The Register (Adelaide)]] |volume=XC, |issue=26,377 |location=South Australia |date=11 July 1925 |accessdate=8 March 2016 |page=5 |via=National Library of Australia}}</ref>

On 8 May 1918 Holden Motor Body Builders Limited was founded, with H. J. Holden as managing director and E. W. Holden and A. M. Lemon<ref>Arthur Miller Lemon [[Licentiate of the Commonwealth Institute of Accountants|LCIA]] [[Australian Institute of Secretaries|AAIS]]  (c. 1890 – ) joined Holden & Frost around 1910, appointed company secretary.</ref> co-directors. A new factory was built at 376–400 King William Street South, between Halifax and Gilles Streets, and ironically in view of the later history of the Holden company, standardized on [[Dodge Brothers]] chassis.<ref>{{cite news |url=http://nla.gov.au/nla.news-article63764820 |title=Remarkable Dodge Business |newspaper=[[The Mail (Adelaide)]] |volume=8, |issue=378 |location=South Australia |date=9 August 1919 |accessdate=9 March 2016 |page=16 |via=National Library of Australia}}</ref>

His youngest son William Arthur Holden (17 December 1899 – 22 December 1929) served overseas during World War I, and on his return was brought into the company, and after five months' study in the US took charge of manufacturing. He died young, perhaps as a result of a riding accident.<ref>{{cite news |url=http://nla.gov.au/nla.news-article165618313 |title=Mr. W. A. Holden Dies, Aged 30 |newspaper=[[The Observer (Adelaide)|The Observer]] |volume=LXXXVI, |issue=4,514 |location=South Australia |date=28 December 1929 |accessdate=9 March 2016 |page=46 |via=National Library of Australia}}</ref>

Holden & Frost Limited, saddlers, continued to operate in Grenfell Street, despite fires in February 1903,<ref>{{cite news |url=http://nla.gov.au/nla.news-article59940708 |title=The Grenfell Street Fire |newspaper=[[The Register (Adelaide)]] |volume=LXVIII, |issue=17,551 |location=South Australia |date=13 February 1903 |accessdate=8 March 2016 |page=4 |via=National Library of Australia}}</ref> October 1919,<ref>{{cite news |url=http://nla.gov.au/nla.news-article89161728 |title=Fire in Grenfell Street |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |volume=LXII, |issue=3,193 |location=South Australia |date=1 November 1919 |accessdate=8 March 2016 |page=39 |via=National Library of Australia}}</ref> February 1920,<ref>{{cite news |url=http://nla.gov.au/nla.news-article213323322 |title=Motor Body Works Ablaze |newspaper=[[The Evening Journal]] |volume=LV, |issue=15155 |location=South Australia |date=21 February 1920 |accessdate=8 March 2016 |page=1  |via=National Library of Australia}}</ref> The business was purchased by Harris, Scarfe & Co. in 1923, and the Grenfell Street property became the major part of their Adelaide store.<ref>{{cite news |url=http://nla.gov.au/nla.news-article200928407 |title=Big City Deal. Passing of Holden & Frost Limited. |newspaper=[[The Evening Journal]] |volume=LVIII, |issue=16141 |location=South Australia |date=28 May 1923 |accessdate=9 March 2016 |page=1 |via=National Library of Australia}}</ref>

==Civic interests==
He was, like his father, involved with the Norwood Baptist church.<!-- and the Norwood Free School?--> He was president of the South Australian Baptist Union for 21 years<ref>{{cite news |url=http://nla.gov.au/nla.news-article89638632 |title=Obituary |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |volume=LXVIII, |issue=3,625 |location=South Australia |date=13 March 1926 |accessdate=9 March 2016 |page=59 |via=National Library of Australia}}</ref>

He was councillor with the [[City of Norwood and Kensington|Town of Norwood and Kensington]] from 1902, mayor 1904 to 1908 and alderman 1909 to 1912. He returned to the office of mayor in 1913 and retired at the end of 1916, in all nine<!--ref says eight--> years as mayor.

In March 1904 he was elected president of the Municipal Association;<ref>{{cite news |url=http://nla.gov.au/nla.news-article56487058 |title=Concerning People |newspaper=[[The Register (Adelaide)]] |volume=LXIX, |issue=17,881 |location=South Australia |date=5 March 1904 |accessdate=9 March 2016 |page=7 |via=National Library of Australia}}</ref> the last to be so elected: henceforth the Mayor of Adelaide was to be ''ex officio'', president of the Association.

He represented the municipalities on the board of the Municipal Tramways Trust from 1907 to 1919,<ref>{{cite news |url=http://nla.gov.au/nla.news-article5625095 |title=The Tramways Trust |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |volume=LXI, |issue=18,809 |location=South Australia |date=25 January 1919 |accessdate=9 March 2016 |page=9 |via=National Library of Australia}}</ref> resigning in April 1919 amid imputations of corruption.<ref>{{cite news |url=http://nla.gov.au/nla.news-article89139547 |title=The Tramways Trust |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |volume=LXI, |issue=3,164 |location=South Australia |date=12 April 1919 |accessdate=8 March 2016 |page=13 |via=National Library of Australia}}</ref>

He was president of the South Australian branch of the [[YMCA]]. for many years.

His wife was also recognised for her civic and charitable work; with the [[YWCA]] and, most notably, for the Red Cross.<ref>{{cite news |url=http://nla.gov.au/nla.news-article166325367 |title=Death of Mrs H. J. Holden |newspaper=[[The Observer (Adelaide)|The Observer]] |volume=LXXXIII, |issue=7,023 |location=South Australia |date=12 June 1926 |accessdate=9 March 2016 |page=28 |via=National Library of Australia}}</ref>

==Family==
Henry James Holden (18 July 1859 – 6 March 1926) married Mary Anne Dixon "Polly" Wheewall (16 March 1860 – 1926) on 7 April 1881, lived at "Warrinilla", 92 Osmond Tce., Norwood. Among their children were:
*Sir [[Edward Wheewall Holden]] (14 August 1885 – 17 June 1947) married Hilda May Lavis (1887 – 6 August 1867) on 18 March 1908. He was a noted industrialist, lived at "Kalymna", 28 Dequetteville Terrace, Kent Town.
:*Margaret Helen Holden (25 September 1909 – 12 October 2000) married I. Macdonald ( – )
:*Nancy Eileen Holden (12 November 1912 – 4 September 2005) married Frank C. Buttfield ( – ) on 19 February 1936. As [[Nancy Buttfield]] DBE she was a prominent Senator for South Australia.
::*son (27 March 1938 – )
::*son (28 April 1940 – )
:*John James "Jim" Holden (16 March 1919 – 30 November 2012) was RAAF pilot
*Ida Caroline M. Holden (20 July 1888 – ) married Leslie Miles<!--Leslie W. A.?--> Peacock ( – ) on 21 April 1909
*Florence Muriel Holden (4 May 1890 – 1950) married William J. Shaughnessy, lived at Victor Harbor
*Dorothy Edith Holden (19 August 1893 – ) married Dr. Reginald A(rthur) Haste ( – ) on 10 April 1919
*William Arthur Holden (17 December 1899 – 22 December 1929) married Marjorie Reeves, daughter of elocutionist Edward Reeves.

For a more extensive chart of the family see [[James Alexander Holden#Family|The Holden family]]

==Recognition==
A. A. Simpson CMG, president of the [[Royal Geographical Society of Australasia]] listed him among South Australia's ten greatest citizens.<ref>{{cite news |url=http://nla.gov.au/nla.news-article129219069 |title=Ten Best South Australian Men |newspaper=[[The News (Adelaide)|The News]] |volume=XII, |issue=1,738 |location=South Australia |date=8 February 1929 |accessdate=8 March 2016 |page=8 |via=National Library of Australia}}</ref>

== References ==
{{Reflist}}

{{DEFAULTSORT:Holden, James Alexander}}
[[Category:Mayors of places in South Australia]]
[[Category:Australian businesspeople]]
[[Category:Australian Baptists]]
[[Category:Holden]]
[[Category:1859 births]]
[[Category:1926 deaths]]