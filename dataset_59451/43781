<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=TsAGI A-4
 | image=ЦАГИ А-4 - TsAGI A-4.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Two seat [[autogyro]]
 | national origin=[[USSR]]
 | manufacturer=[[TsAGI]] (Центра́льный аэрогидродинами́ческий институ́т, (ЦАГИ), "Tsentralniy Aerogidrodinamicheskiy Institut" - Central Aero-hydrodynamic Institute)
 | designer=[[Nikolai Skrzhinskiy]]
 | first flight=6 November 1932
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=10+
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[TsAGI 2-EA]]
 | variants with their own articles=
}}
|}

The '''TsAGI A-4''', sometimes anglicised as '''CAHI A-4''' or '''ZAGI A-4''', ({{lang-ru|ЦАГИ А-4}}) was an early [[Russia]]n [[autogyro]], influenced by [[Juan de la Cierva|Cierva]] designs and delivered in small numbers to the Soviet Air Force in 1934.

==Design and development==
The early Russian [[TsAGI]] autogyros were strongly influenced by the work of [[Juan de la Cierva]] and their 1931 designed A-4 was broadly similar to the [[Avro]]-built 1929 [[Cierva C.19]], with a pylon mounted [[helicopter rotor|rotor]], forward [[tractor configuration]] engine,  small wings with [[aileron]]s and a conventional tail.<ref name=Laero/> It first flew on 6 November 1932 but it took about a year before rotor vibrational problems were solved and successful tests completed.<ref name=Russ/>

The flat sided [[fuselage]] of the A-4 was built around four tube steel [[longeron]]s with internal tube bracing and covered with a mixture of [[aircraft fabric covering|fabric]] and [[plywood]]. A {{convert|300|hp|kW|abbr=on|disp=flip|0)}} [[M-26 (KIM)]] 7-cylinder [[radial engine]] was mounted in the nose, cowled in a [[Townend ring]] and with its output shaft pointing downwards at an angle of 4.5°. Its small, [[low wing]]s were rectangular plan, wooden structures, each built around a pair of [[spar (aviation)|spar]]s and braced from above by a pair of parallel [[strut]]s, one from each spar to the upper fuselage longeron. They had 5° of [[dihedral (aircraft)|dihedral]] but their semicircular tips were inclined upwards by a further 35°. Conventional ailerons occupying almost 30% of the overall wing area provided [[Flight dynamics (fixed-wing aircraft)|roll]] control.  There were two open [[cockpit]]s in [[tandem]], the forward, passenger seat ahead of the wing [[trailing edge]] and close to the rotor axis. Dual control was fitted. At the rear the fin was long, rounded at the front but with a flat top. It mounted an un[[balanced rudder]], roughly rectangular though rounded at the rear corners, which extended to the keel.  Small like the wing, the tailplane was braced from below and had swept [[leading edge]]s; its [[elevator (aircraft)|elevator]]s were more generous, with rounded tips and a large cut-out for rudder movement.<ref name=Laero/>

The A-4's four blade rotor was mounted at the top of a three legged pylon.  The two rear legs were based on the upper fuselage longerons and the forward on the upper central fuselage. They positioned the rotor hub directly over the wing centre line and {{convert|96|mm|in|abbr=on|2}} behind the centre of gravity.  The {{convert|13|m|ftin|abbr=on|0}} rotor had blades with an [[aspect ratio]] of less than 14.  They were of mixed construction, with steel main spars, two wooden subsidiary spars and covered in a mixture of ply and fabric. Their section was the airfoil used by the Cierva C.19. The blades were wire braced from above from an extension of the rotor axis, which leaned forwards with respect to the aircraft's axis by 2°.  These wires were steel but contained rubber dampers. Hinges (double [[Cardan shaft]]s) provided both upwards and in-plane movement, the latter damped by springs; normally such deflections were about ±7°.<ref name=Laero/>

Like a fixed wing aircraft, the A-4 was controlled with ailerons and elevators connected to a [[control column]] and the rudder to pedals. To the pilot's right there was a lever which enabled the rotor to be connected to the engine via reducing gears for [[jump start (autogyros)|jump starts]] and to ease heavy landings. The drive shaft followed the single forward rotor pylon strut, enclosed within a [[aircraft fairing|streamlined fairing]].  The A-4's fixed [[landing gear|undercarriage]] had wheels on pairs of hinged V struts from the lower fuselage longerons with vertical legs, fitted with faired rubber [[shock absorber]]s, to the forward wing spar immediately below the wing strut. The tailskid also included a shock absorber.<ref name=Laero/>

==Operational history==

At least ten A-4s were delivered for military training, liaison and reconnaissance duties during 1934.<ref name=Russ/>

==Specifications (1934 design)==
{{Aircraft specs
|ref=L'Aérophile April 1935  pp.105-9<ref name=Laero/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|length m=7.22
|length note=over fuselage
|span m=6.73
|span note=

|height m=4.123
|height note=to tip of rotor pylon in flight attitude.
|wing area sqm=7.97
|wing area note=including ailerons
|airfoil=TsAGI A
|empty weight kg=1065
|empty weight note=
|gross weight kg=1365
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[M-26 (KIM)]]
|eng1 type=7-cylinder [[radial engine]]
|eng1 hp=300
|eng1 note=at 1,800 rpm
|power original=
|more power=

|prop blade number=2
|prop name=Type A-7
|prop dia m=2.65

|rot number=1
|rot dia m=13.00
|rot area sqm=132.7
|rot area note=swept; 4 blades, each with area {{convert|3.0|sqm|sqft|abbr=on|0}}.  Aerofoil Göttingen 429.
<!--
        Performance
-->
|perfhide=

|max speed kmh=170
|max speed note=at {{convert|100|m|ft|abbr=on|0}}
|cruise speed kmh=140
|cruise speed mph=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=60-65
|minimum control speed note=to maintain level flight at {{convert|100|m|ft|abbr=on|0}}
|range km=460
|range note=twice radius of action
|endurance=<!-- if range unknown -->
|ceiling m=4100
|ceiling note=practical
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=6 min to {{convert|1000|m|ft|abbr=on|0}}; 43 min to {{convert|4000|m|ft|abbr=on|0}}
|lift to drag=optimum 5.6
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=10.4
|disk loading note=
|fuel consumption kg/km=0.56
|power/mass=
|more performance=
*'''Take-off distance:''' {{convert|30-40|m|ft|abbr=on|0}} into {{convert|5|m/s|ft/s|abbr=on|0}} wind.
*'''Landing distance:''' {{convert|3-10|m|ft|abbr=on|0}}
}}

==References==
{{reflist|refs=

<ref name=Laero>{{cite magazine |last= |first= |authorlink= |coauthors= |date=April 1935 |title=Travaux soviétique sur l'autogire ZAGI A-4 |magazine=L'Aérophile|volume=1935 |issue=4 |pages=105–9|url=http://gallica.bnf.fr/ark:/12148/bpt6k65547540/f17.image }}</ref>

<ref name=Russ>{{cite book |title= The Osprey Encyclopedia of Russian Aircraft 1875-1995|last=Gunston|first=Bill|year=1995|publisher=Osprey (Reed Consumer Books Ltd)  |location=London |isbn= 1 85532 405 9|pages=66–7}}</ref>

}}
<!-- ==Further reading== -->

==External links==
*[http://aerospace.illinois.edu/m-selig/ads/afplots/goe429.gif Göttingen 429 airfoil]

<!-- Navboxes go here -->

[[Category:Autogyros]]
[[Category:Soviet aircraft 1930–1939]]