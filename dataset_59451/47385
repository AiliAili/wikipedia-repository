{{Infobox character
| name        = Dasan and Vijayan
| image       = 
| caption     = 
| first       = ''[[Nadodikkattu]]''<br>(1987)
| last        = ''[[Akkare Akkare Akkare]]''<br>(1990)
| creator     = [[Siddique–Lal]]
| portrayer   = [[Mohanlal]] (Dasan)<br>[[Sreenivasan (actor)|Sreenivasan]] (Vijayan)
| nickname    = 
| alias       = Ramadasan (Dasan)
| gender      = Male
| occupation  = [[Detective]]s
| title       = 
| religion    = [[Hinduism]]
| nationality = [[Indian people|Indian]]
}}

'''Dasan and Vijayan''' (or '''Ramadasan and Vijayan''') are two fictional character duo, the protagonists in the ''Nadodikkattu'' film series. The comical characters were created by the duo [[Siddique–Lal]] and are portrayed by [[Mohanlal]] (Dasan) and [[Sreenivasan (actor)|Sreenivasan]] (Vijayan). Dasan and Vijayan are [[detective]]s by profession working in the [[Crime Investigation Department (India)|Crime Investigation Department]] (CID) in [[Tamil Nadu Police]], India. The characters first appeared in the 1987 [[black comedy]] film ''[[Nadodikkattu]]'' and later in its sequels ''[[Pattanapravesham]]'' (1988), and ''[[Akkare Akkare Akkare]]'' (1990).<ref>{{cite news|last=K.|first=Rejimon|title=In a new guise|url=http://www.thehindu.com/thehindu/fr/2005/09/30/stories/2005093001320200.htm|accessdate=8 February 2017|work=[[The Hindu]]|date=30 September 2005}}</ref>

The first two films in the series was directed by [[Sathyan Anthikad]] and the later by [[Priyadarshan]], the trilogy was scripted by Sreenivasan. Since the release of ''Nadodikkattu'', the characters have achieved a [[cult status]] in the [[popular culture]] of [[Kerala]], along with some other characters in the film, such as Anandan Nambiar ([[Thilakan]]), Gafoorka ([[Mamukkoya]]), Pavanai ([[Captain Raju]]), Prabhakaran ([[Karamana Janardanan Nair]]) and several dialogues in the film has become [[catchphrase]]s. The characters was first introduced as an unemployed youth.

== Overview ==

The characters were created by [[Siddique–Lal]] in a story they wrote.<ref name=story/> Dasan is shown as more educated than Vijayan, and enjoys teasing Vijayan about the same. The chemistry of Mohanlal-Sreenivasan combo in the film was widely acclaimed, since then they have united for such roles in many films that followed. The series began with ''[[Nadodikkattu]]'' in 1987, directed by [[Sathyan Anthikad]], he directed its sequel ''[[Pattanapravesham]]'' in the following year, in 1990, [[Priyadarshan]] continued the series with his directorial ''[[Akkare Akkare Akkare]]''.<ref>{{cite news|last=Jayaram|first=Deepika|title=Reel friends forever|url=http://timesofindia.indiatimes.com/entertainment/malayalam/movies/news/Reel-friends-forever/articleshow/48314583.cms|accessdate=8 February 2017|work=[[The Times of India]]|date=2 August 2015}}</ref> ''Nadodikkattu'' was significant in increasing the demand for comedy films in Malayalam.<ref>{{cite news|last=George|first=Vijay|title=Siddique speaking|url=http://www.thehindu.com/features/cinema/siddique-speaking/article4605926.ece|accessdate=8 February 2017|work=[[The Hindu]]|date=11 April 2013}}</ref> About its cult following, Anthikad says "the secret of the duo still being alive in the Malayali's consciousness may be because it's a story of two unemployed youngsters told using a humorous narrative. Unemployment was one of the biggest issues and migration to greener pastures was typical of Malayali experience. This has continued to the current decade. So, the audience has no problem relating to Dasan and Vijayan,".<ref name=story>{{cite news|last=Kumar|first=M. K. Sunil|title=Blast from the past: The duo who stole our hearts|url=http://timesofindia.indiatimes.com/city/kochi/Blast-from-the-past-The-duo-who-stole-our-hearts/articleshow/11722642.cms|accessdate=8 February 2017|work=[[The Times of India]]|date=2 February 2012}}</ref>

== Film appearances ==
=== ''Nadodikkattu'' ===
In the film directed by Sathyan Anthikad, Dasan and Vijayan are two unemployed youth struggling to meet the two ends of life. Upon release, the film was widely acclaimed and was a commercial success.<ref name=story/>

=== ''Pattanapravesham'' ===
After the events of ''Nadodikkattu'', Dasan and Vijayan who are now detectives in police, are enlisted to investigate a homicide in [[Kerala]]. Both clueless in their job, gets in the middle of drug trafficking gang. A new threat follows them when Anandan Nambiar escapes from jail. The film directed by Anthikad was a critical and commercial success.<ref>{{cite news|last=Philip|first=Benson|title=Unforgettable Dectective roles in Mollywood|url=http://timesofindia.indiatimes.com/entertainment/malayalam/movies/unforgettable-dectective-roles-in-mollywood/photostory/53192862.cms|accessdate=8 February 2017|work=[[The Times of India]]|date=13 July 2016}}</ref>

=== ''Akkare Akkare Akkare'' ===
The films shows Dasan and Vijayan investigating a missing gold crown stolen from India to the [[United States]]. The film was directed by [[Priyadarshan]] and unlike previous films, this film is set in the United States and most part of the film was shot in [[Houston]], [[Texas]], and [[Los Angeles]]. Other characters were played by [[M. G. Soman]], [[Mukesh (actor)|Mukesh]], [[Maniyanpilla Raju]], [[Parvathy (actress)|Parvathy]], [[Nedumudi Venu]], [[Sukumari]], [[Jagadish]], and [[K. P. A. C. Lalitha]]. The film is noted for its dialogues and humour.<ref>{{cite news|last=Vasudevan|first=Aishwarya|title=Priyadarshan – Mohanlal: Best films of the duo|url=http://timesofindia.indiatimes.com/entertainment/malayalam/movies/Priyadarshan-Mohanlal-Best-films-of-the-duo/Priyadarshan-Mohanlal-Best-films-of-the-duo/photostory/48783904.cms|accessdate=8 February 2017|work=[[The Times of India]]|date=3 September 2015}}</ref>

== Possible return ==
In 2016, [[Vineeth Sreenivasan]] said, he is interested in bringing back the duo in his directorial and is looking for story ideas.<ref>{{cite news|title=Dasan and Vijayan making a comeback?|url=http://www.mangalam.com/en-news/detail/44782-entertainment-dasan-and-vijayan-making-a-comeback.html|accessdate=8 February 2017|work=[[Mangalam Publications]]|date=24 October 2016}}</ref>

== References ==
<!-- Inline citations added to your article will automatically display here. See https://en.wikipedia.org/wiki/WP:REFB for instructions on how to add citations. -->
{{reflist}}

== External links ==
* {{IMDb character|0035602|Dasan}}
* {{IMDb character|0035603|Vijayan}}

{{DV film series}}

[[Category:Film duos]]
[[Category:Comedy characters]]
[[Category:Fictional police detectives]]
[[Category:Fictional Indian people]]
[[Category:Fictional characters introduced in 1987]]
[[Category:Fictional duos]]
[[Category:Film duos]]
[[Category:Comedy characters]]
[[Category:Comedy film characters]]
[[Category:Fictional police detectives]]
[[Category:Fictional Indian police officers]]
[[Category:Fictional Indian people]]
[[Category:Fictional characters introduced in 1987]]
[[Category:Fictional unemployed people]]