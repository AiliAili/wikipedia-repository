{{Infobox journal
| title = Public Finance Review
| cover = [[File:Public Finance Review Journal Front Cover.jpg]]
| editor = James Alm
| discipline = [[Business Economics]]
| former_names = 
| abbreviation = Public Finance Rev.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bi-monthly
| history = 1973-present
| openaccess = 
| license = 
| impact = 
| impact-year = 2010
| website = http://www.uk.sagepub.com/journals/Journal200768?siteId=sage-uk&prodTypes=any&q=Public+Finance+Review&fs=1
| link1 = http://pfr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pfr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1091-1421
| eISSN = 
| OCLC = 38430175
| LCCN = 97657787
}}

'''''Public Finance Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers four times a year in the fields of [[business]] and [[economics]]. The journal's [[Editor-in-Chief|editor]] is [[James Alm]] ([[Tulane University]]). It has been in publication since 1973 and is currently published by [[SAGE Publications]].

== Scope ==
''Public Finance Review'' focuses on the variety of allocation, distribution, and stabilization functions within the public sector economy. The journal is a professional forum devoted to economic research, theory, and policy applications. ''Public Finance Review'' aims to publish the most up-to-date information to help policy makers, political scientists, and researchers put policies and research into action.

== Abstracting and indexing ==
''Public Finance Review'' is abstracted and indexed in the following databases:
:* [[ABI/INFORM]]
:* [[Academic Onefile]]
:* [[Business Source Complete]]
:* [[Business Source Premier]]
:* [[EconLit]]
:* [[General Onefile]]
:* [[SCOPUS]]

== External links ==
* {{Official website|1=http://pfr.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Finance journals]]
[[Category:Publications established in 1973]]