<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Trainer
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Trainer (aircraft)|Trainer]]
 | national origin=[[United Kingdom]]
 | manufacturer=Marendaz Aircraft Ltd, Barton-in-the-Clay, Bedfordshire
 | designer=Donald Marendaz?<ref name="OH1"/> 
 | first flight=November 1939
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Marendaz Trainer''' was a two-seat low-wing [[Trainer (aircraft)|training aircraft]] built in the [[UK]] just before [[World War II]].  Only one was completed.

==Development==

In 1939 the newly established Marendaz Aircraft Ltd.<ref>{{harvnb|Ord-Hume|2000|pp=530, 533}}</ref> built a two-seat, single-engined low wing cantilever [[monoplane]] called the Marendaz Trainer.<ref name="OH">{{harvnb|Ord-Hume|200|pp=407–8}}</ref>  Donald Marendaz, the company's founder, claimed to have designed it though this has been disputed, not least because Marendaz had claimed the work of others as his own before.<ref name="OH1">{{harvnb|Ord-Hume|200|pp=405, 407}}</ref> If it was not his, the true designer is unknown.

The Trainer was in most respects a conventional open cockpit two seater of its time. Its fuselage and wings were wooden structures skinned with plywood.  Only the control surfaces were fabric covered.<ref name="OH"/>  The wings were built around two box spars and consisted of a constant chord centre section, integral with the fuselage and carrying no dihedral, plus bolt-on outer panels with marked dihedral and slight taper on the trailing edge.<ref name="OH"/> The outer panels carried both ailerons and built-in leading edge slots with no moving parts.<ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1939/1939-1-%20-%201312.html ''Flight'' 23 November 1939  p.418-9]</ref>

The fuselage was flat sided, with a conventional rounded decking running forward as far as the engine cowling.  It tapered to the rear, more in plan than elevation and carried a tailplane with a sharply swept leading edge mounted on top of the fuselage and externally braced from below.  The elevators were more rounded, and well separated to allow rudder movement. The fin and horn balanced rudder together were almost triangular, with the latter rounded at tip and base.  The rear cockpit was over the trailing edge of the wing and the front cockpit at the leading edge.  The Trainer was powered by a 90&nbsp;hp (67&nbsp;kW) [[Blackburn Cirrus Minor]] four cylinder inverted in-line engine, driving a two-bladed propeller.<ref name="OH"/>

The Trainer's undercarriage was unusual<ref name="Flight"/> and possibly vulnerable.<ref name="OH"/>  Each wheel was mounted on a stub axle, independent of the other and swung on a V member formed from heavy duty square tubing, with its ends hinged to front and rear spar towards the outer end of the centre section, so the assembly could swing laterally.  Each V plus wheel was restrained by a radius arm hinged on the front spar near the fuselage, allowing a small amount of upwards and outward travel for the wheel.  The radius arms were sprung inside the fuselage.<ref name="Flight"/>  In order to get the very wide track of 93 in (2.36 m), both the V and radius arms were heavily splayed and the angle between them was much smaller than normal, a cause for some concern.<ref name="OH"/>  The tailskid was mounted without a fuselage aperture, to avoid the trapping of grass and dirt.<ref name="Flight"/>

The sole Trainer, registered ''G-AFZX''<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=reg&fullregmark=AFZX Registration documents of ''G-AFZX'']</ref>  first flew in November 1939 with R.A. Wyndham at the controls. It seems to have flown well, though it is not known if the front seat was ever occupied or ballasted.  By this time the UK was at war, so the aircraft went to the [[RAF Halton]] Squadron of the [[Air Training Corps]] in 1940 and no more was heard of it.<ref name="OH"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref={{harvnb|Ord-Hume|2000|p=408}}  These figures are the designer's estimates
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft22
|length in=4
|length note=
|span m=
|span ft=34
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=5
|height in=8
|height note=
|wing area sqm=
|wing area sqft=157
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=960
|empty weight note=
|gross weight kg=
|gross weight lb=1500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Blackburn Cirrus Minor|Blackburn Cirrus Minor I]] 
|eng1 type=4-cylinder in-line inverted air cooled piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=90<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=124
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=95
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=37<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=35
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Citations===
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6 |ref=harv}}
*{{cite magazine |last= |first= |authorlink= |coauthors= |year= |month= |title= The new Marendaz.|magazine= [[Flight International|Flight]]|volume= |issue=23 November 1939 |pages=418–9 |id= |url= http://www.flightglobal.com/pdfarchive/view/1939/1939-1-%20-%201312.html |accessdate= |quote= }}
{{refend}}
<!-- ==External links== -->

[[Category:British civil trainer aircraft 1930–1939]]