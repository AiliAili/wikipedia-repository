{{Motorsport season
|series = U.S. F2000 National Championship
|link = U.S. F2000 National Championship
|year = 2016
|races = 16
|start_date     = March 12
}}
The '''2016 U.S. F2000 National Championship''' was the seventh season&nbsp;– since its revival in 2010&nbsp;– of the [[U.S. F2000 National Championship]], an [[open-wheel car|open wheel]] [[auto racing]] series that is the first step in [[INDYCAR]]'s [[Road to Indy]] ladder, and is owned by Andersen Promotions. A 16-race schedule was announced on October 27, 2015.<ref name="schedule">http://indylights.com/news/2016-mazda-road-to-indy-schedules-announced</ref>
The National class was re-introduced, for SCCA Formula Continental cars.<ref name="NC">http://www.usf2000.com/news/national-class-reintroduced-in-usf2000</ref>

Australian [[Anthony Martin (racing driver)|Anthony Martin]] and his Cape Motorsports teammate Canadian [[Parker Thompson]] were neck and neck throughout the season. Ultimately Martin captured the championship on the back of seven wins compared to Thompson's four. Thompson finished on the podium in twelve of the sixteen races, one more than Martin. Brazilian [[Victor Franzoni]] won three races and was running at the finish of every race and finished third in points, fourteen points behind Thompson. [[Jordan Lloyd (racing driver)|Jordan Lloyd]] and [[Luo Yufeng (racing driver)|Luo Yufeng]] also captured race victories.

Cape Motorsports won the team championship by a comfortable margin.

Eric Filgueiras won the National Class championship, largely unopposed.

2016 was the final year for the [[Van Diemen]]-designed [[Formula Continental]] derived cars in Championship Class. In 2017 it will be replaced by a new bespoke design from [[Tatuus]]. The old cars will remain eligible for National Class.

==Drivers and teams==
{|
|
{| class="wikitable" style="font-size:85%;"
! Team
! {{Tooltip|No.|Car number}}
! Drivers
! Rounds
|-
|rowspan=3| [[Afterburner Autosport]]
|rowspan=2| 17
| {{flagicon|BRA}} [[Felipe Ortiz (racing driver)|Felipe Ortiz]]<ref name="Ortiz">http://www.usf2000.com/news/afterburner-autosport-confirms-felipe-ortiz-for-rookie-usf2000-campaign</ref>
| 1–2
|-
| {{flagicon|COL}} [[Christian Muñoz (racing driver)|Christian Muñoz]]
| 12–14
|-
| 18
| {{flagicon|USA}} [[Dakota Dickerson]]<ref name="Dickerson">http://motorsports.nbcsports.com/2016/02/24/dakota-dickerson-confirmed-for-afterburner-usf2000-team/</ref>
| All
|-
|rowspan=4| [[ArmsUp Motorsports]]
| 6
| {{flagicon|USA}} [[Max Hanratty]]<ref name="STP">http://usf2000.com/docs/default-source/Schedule/usf2000-entry-list.pdf?sfvrsn=0</ref>
| 1–6, 8–9
|-
| 7
| {{flagicon|USA}} [[Dale VandenBush]] '''(N)'''
| 5–6, 8–9
|-
| 9
| {{flagicon|BRA}} [[Victor Franzoni]]<ref name="STP"/>
| All
|-
| 16
| {{flagicon|USA}} [[Devin Wojcik]]
| 8–9
|-
|rowspan=4| [[Cape Motorsports]] <br/> [[Wayne Taylor Racing]]
| 2
| {{flagicon|CAN}} [[Parker Thompson]]<ref name="Thompson">http://usf2000.com/news/cape-motorsports-confirms-thompson-for-2016-usf2000-campaign</ref>
| All
|-
| 3
| {{flagicon|RUS}} [[Nikita Lastochkin]]<ref name="Lastochkin">http://www.motorsport.com/usf2000/news/lastochkin-switches-to-cape-for-sophomore-usf2000-season-66469/</ref>
| 1–6, 8–16
|-
| 4
| {{flagicon|GBR}} [[Jordan Cane]]
| 8–14
|-
| 8
| {{flagicon|AUS}} [[Anthony Martin (racing driver)|Anthony Martin]]<ref name="Martin">http://usf2000.com/news/cape-motorsports-add-anthony-martin-to-usf2000-line-up-for-2016</ref>
| All
|-
|rowspan=2|[[Chastain Motorsports]]
| 5
| {{flagicon|USA}} [[Austin McCusker]]<ref name="STP"/>
| 1–14
|-
| 77
| {{flagicon|USA}} [[Sam Chastain]]<ref name="Chastain">http://www.chastainmotorsports.com/chastain-motorsports-enters-2016-usf2000-season/</ref>
| 1–6, 8–14
|-
|rowspan=2| [[FatBoy Racing]]
| 15
| {{flagicon|USA}} [[Brendan Puderbach]]<ref name="STP2">http://www.usf2000.com/docs/default-source/Schedule/usf2000-entry-list61ccd67d339f6ec495b1ff0000570f88.pdf?sfvrsn=0</ref> '''(N)'''
| 1–2, 5–6, 8–11
|-
| 44
| {{flagicon|USA}} [[Charles Finelli]]<ref name="STP2"/> '''(N)'''
| 1–2, 5–6
|-
|rowspan=2| [[JAY Motorsports]]
| 91
| {{flagicon|AUS}} [[Luke Gabin]]<ref name="STP"/>
| All
|-
| 92
| {{flagicon|USA}} [[Cameron Das]]<ref name="BAR">http://usf2000.com/docs/default-source/Schedule/usf2000-entry-list1615d77d339f6ec495b1ff0000570f88.pdf?sfvrsn=0</ref>
| 3–6, 8–16
|-
|rowspan=2| [[JDC Motorsports]]
| 52
| {{flagicon|USA}} [[Robert Allaer]]<ref name="STP"/> '''(N)'''
| 1–2
|-
| 72
| {{flagicon|USA}} [[Tazio Ottis]]<ref name="STP"/>
| All
|-
|rowspan=3| [[John Cummiskey Racing]]
|rowspan=2| 33
| {{flagicon|NOR}} [[Ayla Ågren]]<ref name="Ågren">http://www.motorsport.com/usf2000/news/ayla-agren-joins-jcr-for-second-usf2000-season-671065/</ref>
| 1–9, 12–14
|-
| {{flagicon|NZL}} [[Michael Scott (racing driver)|Michael Scott]]
| 15–16
|-
| 34
| {{flagicon|BRA}} [[Lucas Kohl]]<ref name="Kohl">http://usf2000.com/news/lucas-kohl-signs-with-john-cummiskey-racing/</ref>
| All
|-
|rowspan=3| [[Pabst Racing Services]]
| 21
| {{flagicon|AUS}} [[Jordan Lloyd (racing driver)|Jordan Lloyd]]<ref name="Lloyd">http://usf2000.com/news/pabst-racing-signs-jordan-lloyd/</ref>
| All
|-
| 22
| {{flagicon|USA}} [[Garth Rickards]]<ref name="Rickards">http://www.motorsport.com/usf2000/news/pabst-racing-signs-garth-rickards-for-2016/</ref>
| All
|-
| 23
| {{flagicon|CHN}} [[Luo Yufeng (racing driver)|Luo Yufeng]]<ref name="Luo">http://usf2000.com/news/pabst-racing-announces-yufeng-luo-for-the-2016-season/</ref>
| All
|-
|rowspan=2| [[RJB Motorsports]]
| 19
| {{flagicon|USA}} [[Michai Stephens]]<ref name="Stephens">http://usf2000.com/news/michai-stephens-joins-rjb-motorsports-for-2016</ref>
| 1–6, 8–14
|-
| 20
| {{flagicon|USA}} [[Clint McMahan]]<ref name="McMahan">http://www.nationalspeedsportnews.com/indy/indycar-development-series/rjb-to-field-two-car-usf2000-assault/</ref>
| 1–6, 8–14
|-
|rowspan=1| [[Spencer Racing]]
| 12
| {{flagicon|USA}} [[Eric Filgueiras]]<ref name="Filguerias">http://www.motorsport.com/usf2000/news/national-class-gains-scca-pro-graduates-668975/</ref> '''(N)'''
| 1–6, 8–14
|-
|rowspan=6| [[Team Pelfrey]]
| 80
| {{flagicon|USA}} [[Robert Megennis]]<ref name="Megennis">http://usf2000.com/news/robert-megennis-steps-up-to-usf2000-with-team-pelfrey/</ref>
| All
|-
|rowspan=2| 81
| {{flagicon|GBR}} [[Jordan Cane]]<ref name="Cane">http://www.nationalspeedsportnews.com/indy/indycar-development-series/jordan-cane-graduates-to-usf2000/</ref>
| 1–6
|-
| {{flagicon|USA}} [[Kaylen Frederick]]
| 15–16
|-
|rowspan=2| 82
| {{flagicon|USA}} [[TJ Fischer]]<ref name="Fischer">http://motorsports.nbcsports.com/2016/01/07/mrti-tj-fischer-joins-team-pelfrey-usf2000-lineup/</ref>
| 1–6
|-
| {{flagicon|USA}} [[Phillippe Denes]]
| 15–16
|-
| 83
| {{flagicon|NZL}} [[James Munro (racing driver)|James Munro]]<ref name="Munro">http://usf2000.com/news/james-munro-signs-with-team-pelfrey/</ref>
| 1–4
|-
| [[Tomasi Motorsports]]
| 11
| {{flagicon|USA}} [[Robert Armington]] '''(N)'''
| 5–6, 15–16
|}
|valign="top"|
{| class="wikitable" style="font-size: 85%;"
! Icon
! Class
|-
|align=center| '''(N)'''
| National Class
|}
|}

==Race calendar and results==

{| class="wikitable" style="font-size: 85%;"
! Round|Rnd
! Circuit
! Location
! Date
! Pole position
! Fastest lap
! Most laps led
! Winning driver
! Winning team
|-
! 1
|rowspan=2| [[St. Petersburg, Florida|Streets of St. Petersburg]]
|rowspan=2| [[St. Petersburg, Florida]]
|rowspan=2| March 12
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|AUS}} Jordan Lloyd
| Pabst Racing
|-
! 2
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|CHN}} [[Luo Yufeng (racing driver)|Luo Yufeng]] 
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|CHN}} [[Luo Yufeng (racing driver)|Luo Yufeng]] 
| Pabst Racing
|-
! 3
|rowspan=2| [[Indy Grand Prix of Alabama]]
|rowspan=2| [[Birmingham, Alabama]]
| April 22
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 4
| April 23
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 5
|rowspan=2| [[Indianapolis Motor Speedway]] road course
|rowspan=2| [[Speedway, Indiana]]
| May 13
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 6
| May 14
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| Cape Motorsports with [[Wayne Taylor Racing|WTR]] 
|-
! 7
| [[Lucas Oil Raceway]]
| [[Clermont, Indiana]]
| May 27
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 8
|rowspan=2| [[Grand Prix of Road America]]
|rowspan=2| [[Elkhart Lake, Wisconsin]]
|rowspan=2| June 25
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 9
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 10
|rowspan=2| [[Grand Prix of Toronto]]
|rowspan=2| [[Toronto]], [[Canada]]
| July 16
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| ArmsUp Motorsports
|-
! 11
| July 17
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Jordan Lloyd
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|CAN}} [[Parker Thompson]]
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
|-
! 12
|rowspan=3| [[Mid-Ohio Sports Car Course]]
|rowspan=3| [[Lexington, Ohio]]
| July 29
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 13
| July 30
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 14
| July 31
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|CAN}} [[Parker Thompson]]
| {{flagicon|AUS}} Anthony Martin
| {{flagicon|AUS}} Anthony Martin
| Cape Motorsports with [[Wayne Taylor Racing|WTR]]
|-
! 15
|rowspan=2| [[Monterey Grand Prix]]
|rowspan=2| [[Salinas, California]]
| September 10
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| ArmsUp Motorsports
|-
! 16
| September 11
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| {{flagicon|BRA}} Victor Franzoni
| ArmsUp Motorsports
|}

==Championship standings==

===Drivers' Championship===
{|
|
{| class="wikitable" style="font-size:85%; text-align:center"
|- valign="top"
!valign="middle"| Pos
!valign="middle"| Driver
!colspan=2| [[Grand Prix of St. Petersburg|STP]]
!colspan=2| [[Barber Motorsports Park|BAR]]
!colspan=2| [[Indianapolis Motor Speedway|IMS]]
! [[Lucas Oil Raceway at Indianapolis|LOR]]
!colspan=2| [[Road America|ROA]]
!colspan=2| [[Honda Indy Toronto|TOR]]
!colspan=3| [[Mid-Ohio Sports Car Course|MOH]]
!colspan=2| [[Mazda Raceway Laguna Seca|LAG]]
!valign="middle"| Points
|-
!colspan=19| Championship class
|-
|-
! 1
|align="left"| {{flagicon|AUS}} [[Anthony Martin (racing driver)|Anthony Martin]]
|style="background:#DFFFDF;"| ''5''
|style="background:#CFEAFF;"| 10
|style="background:#FFDF9F;"| 3
|style="background:#DFFFDF;"| '''4'''
|style="background:#FFFFBF;"| '''1'''*
|style="background:#DFFFDF;"| 4
|style="background:#FFFFBF;"| 1
|style="background:#FFFFBF;"| '''''1'''''*
|style="background:#FFFFBF;"| '''1'''*
|style="background:#EFCFFF;"| '''16'''
|style="background:#DFDFDF;"| '''2'''
|style="background:#FFFFBF;"| '''''1'''''*
|style="background:#FFFFBF;"| '''1'''*
|style="background:#FFFFBF;"| '''1'''*
|style="background:#FFDF9F;"| 3
|style="background:#DFDFDF;"| 2
!394
|-
! 2
|align="left"| {{flagicon|CAN}} [[Parker Thompson]]
|style="background:#CFCFFF;"| 15
|style="background:#DFFFDF;"| 4
|style="background:#FFFFBF;"| '''''1'''''*
|style="background:#FFFFBF;"| ''1''*
|style="background:#DFDFDF;"| ''2''
|style="background:#FFFFBF;"| '''''1'''''*
|style="background:#DFDFDF;"| '''''2'''''*
|style="background:#DFFFDF;"| 5
|style="background:#FFDF9F;"| 3
|style="background:#FFDF9F;"| 3
|style="background:#FFFFBF;"| 1*
|style="background:#CFCFFF;"| 17
|style="background:#DFDFDF;"| ''2''
|style="background:#DFDFDF;"| ''2''
|style="background:#DFDFDF;"| 2
|style="background:#FFDF9F;"| 3
!374
|-
! 3
|align="left"| {{flagicon|BRA}} [[Victor Franzoni]]
|style="background:#CFEAFF;"| 6
|style="background:#CFCFFF;"| 12
|style="background:#DFDFDF;"| 2
|style="background:#CFEAFF;"| 9
|style="background:#FFDF9F;"| 3
|style="background:#DFDFDF;"| 2
|style="background:#FFDF9F;"| 3
|style="background:#DFFFDF;"| 4
|style="background:#DFDFDF;"| 2
|style="background:#FFFFBF;"| ''1''*
|style="background:#FFDF9F;"| 3
|style="background:#DFDFDF;"| 2
|style="background:#FFDF9F;"| 3
|style="background:#DFFFDF;"| 4
|style="background:#FFFFBF;"| '''''1'''''*
|style="background:#FFFFBF;"| '''''1'''''*
!360
|-
! 4
|align="left"| {{flagicon|AUS}} [[Jordan Lloyd (racing driver)|Jordan Lloyd]]
|style="background:#FFFFBF;"| '''1'''*
|style="background:#DFDFDF;"| '''2'''*
|style="background:#DFFFDF;"| 5
|style="background:#CFCFFF;"| 20
|style="background:#DFFFDF;"| 4
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 4
|style="background:#DFDFDF;"| 2
|style="background:#EFCFFF;"| ''21''
|style="background:#DFDFDF;"| 2
|style="background:#CFEAFF;"| ''8''
|style="background:#DFFFDF;"| 4
|style="background:#CFCFFF;"| 16
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 4
!276
|-
! 5
|align="left"| {{flagicon|AUS}} [[Luke Gabin]]
|style="background:#DFFFDF;"| 4
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 4
|style="background:#DFDFDF;"| 2
|style="background:#CFCFFF;"| 20
|style="background:#FFDF9F;"| 3
|style="background:#CFCFFF;"| 11
|style="background:#FFDF9F;"| 3
|style="background:#CFEAFF;"| 7
|style="background:#DFFFDF;"| 4
|style="background:#CFEAFF;"| 10
|style="background:#FFDF9F;"| 3
|style="background:#CFCFFF;"| 11
|style="background:#CFCFFF;"| 16
|style="background:#DFFFDF;"| 4
|style="background:#DFFFDF;"| 5
!252
|-
! 6
|align="left"| {{flagicon|USA}} [[Robert Megennis]]
|style="background:#FFDF9F;"| 3
|style="background:#CFEAFF;"| 8
|style="background:#CFCFFF;"| 12
|style="background:#FFDF9F;"| 3
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 10
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 7
|style="background:#CFCFFF;"| 11
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 5
|style="background:#CFCFFF;"| 19
|style="background:#DFFFDF;"| 4
|style="background:#CFEAFF;"| 7
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 8
!224
|-
! 7
|align="left"| {{flagicon|CHN}} [[Luo Yufeng (racing driver)|Luo Yufeng]]
|style="background:#DFDFDF;"| 2
|style="background:#FFFFBF;"| ''1''
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 11
|style="background:#DFFFDF;"| 5
|style="background:#CFEAFF;"| 6
|style="background:#DFFFDF;"| 5
|style="background:#EFCFFF;"| 24
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 7
|style="background:#EFCFFF;"| 18
|style="background:#CFCFFF;"| 17
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 7
!212
|-
! 8
|align="left"| {{flagicon|RUS}} [[Nikita Lastochkin]]
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 7
|style="background:#CFEAFF;"| 7
|style="background:#DFFFDF;"| 5
|style="background:#CFCFFF;"| 23
|style="background:#CFCFFF;"| 11
|
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 8
|style="background:#EFCFFF;"| 17
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 7
|style="background:#CFEAFF;"| 8
|style="background:#CFCFFF;"| 11
|style="background:#CFEAFF;"| 6
!183
|-
! 9
|align="left"| {{flagicon|USA}} [[Dakota Dickerson]]
|style="background:#CFEAFF;"| 8
|style="background:#EFCFFF;"| 22
|style="background:#CFCFFF;"| 17
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 11
|style="background:#CFCFFF;"| 20
|style="background:#CFEAFF;"| 9
|style="background:#CFCFFF;"| 12
|style="background:#CFEAFF;"| 10
|style="background:#CFEAFF;"| 7
|style="background:#DFFFDF;"| 4
|style="background:#DFFFDF;"| 5
|style="background:#DFFFDF;"| 5
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 10
|style="background:#CFEAFF;"| 10
!176
|-
! 10
|align="left"| {{flagicon|USA}} [[Garth Rickards]]
|style="background:#EFCFFF;"| 24
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 7
|style="background:#CFCFFF;"| 15
|style="background:#CFEAFF;"| 7
|style="background:#CFEAFF;"| 7
|style="background:#EFCFFF;"| 23
|style="background:#DFFFDF;"| 5
|style="background:#CFCFFF;"| 15
|style="background:#EFCFFF;"| 17
|style="background:#CFEAFF;"| 7
|style="background:#CFEAFF;"| 6
|style="background:#FFDF9F;"| 3
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 15
!172
|-
! 11
|align="left"| {{flagicon|NOR}} [[Ayla Ågren]]
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 13
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 7
|style="background:#CFCFFF;"| 12
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 8
|style="background:#DFFFDF;"| 4
|
|
|style="background:#CFEAFF;"| 10
|style="background:#EFCFFF;"| 21
|style="background:#CFCFFF;"| 11
|
|
!137
|-
! 12
|align="left"| {{flagicon|BRA}} [[Lucas Kohl]]
|style="background:#CFCFFF;"| 23
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 21
|style="background:#CFCFFF;"| 21
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 11
|style="background:#CFCFFF;"| 13
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 12
|style="background:#CFEAFF;"| 9
|style="background:#EFCFFF;"| 18
|style="background:#CFCFFF;"| 12
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 9
!136
|-
! 13
|align="left"| {{flagicon|GBR}} [[Jordan Cane]]
|style="background:#EFCFFF;"| 25
|style="background:#FFDF9F;"| 3
|style="background:#CFEAFF;"| 6
|style="background:#CFCFFF;"| 19
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 21
|
|style="background:#EFCFFF;"| 21
|style="background:#CFEAFF;"| 6
|style="background:#CFEAFF;"| 9
|style="background:#EFCFFF;"| 16
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 9
|style="background:#CFCFFF;"| 14
|
|
!121
|-
! 14
|align="left"| {{flagicon|USA}} [[Austin McCusker]]
|style="background:#EFCFFF;"| 20
|style="background:#FFFFFF;"| <small>DNS</small>
|style="background:#CFEAFF;"| 10
|style="background:#CFEAFF;"| 8
|style="background:#CFCFFF;"| 14
|style="background:#EFCFFF;"| 23
|style="background:#CFCFFF;"| 12
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 11
|style="background:#CFCFFF;"| 11
|style="background:#CFCFFF;"| 11
|style="background:#CFEAFF;"| 8
|style="background:#CFEAFF;"| 10
|
|
!118
|-
! 15
|align="left"| {{flagicon|USA}} [[Tazio Ottis]]
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 17
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 14
|style="background:#FFFFFF;"| <small>DNS</small>
|style="background:#CFCFFF;"| 17
|style="background:#CFCFFF;"| 17
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 16
!102
|-
! 16
|align="left"| {{flagicon|USA}} [[Cameron Das]]
|
|
|style="background:#CFCFFF;"| 20
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 17
|style="background:#EFCFFF;"| 24
|
|style="background:#CFEAFF;"| 9
|style="background:#CFCFFF;"| 14
|style="background:#CFEAFF;"| 8
|style="background:#EFCFFF;"| 15
|style="background:#FFFFFF;"| <small>DNS</small>
|style="background:#CFEAFF;"| 10
|style="background:#CFCFFF;"| 20
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 12
!83
|-
! 17
|align="left"| {{flagicon|USA}} [[Michai Stephens]]
|style="background:#CFCFFF;"| 11
|style="background:#FFFFFF;"| <small>DNS</small>
|style="background:#CFCFFF;"| 19
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 18
|style="background:#CFCFFF;"| 15
|
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 20
|style="background:#CFCFFF;"| 13
|style="background:#CFEAFF;"| 9
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 13
|
|
!82
|-
! 18
|align="left"| {{flagicon|USA}} [[Sam Chastain]]
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 18
|style="background:#CFCFFF;"| 22
|style="background:#CFCFFF;"| 12
|style="background:#CFCFFF;"| 13
|
|style="background:#EFCFFF;"| 20
|style="background:#EFCFFF;"| 23
|style="background:#FFFFFF;"| <small>DNS</small>
|
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 18
|
|
!62
|-
! 19
|align="left"| {{flagicon|USA}} [[TJ Fischer]]
|style="background:#CFCFFF;"| 22
|style="background:#CFEAFF;"| 9
|style="background:#CFCFFF;"| 13
|style="background:#CFEAFF;"| 10
|style="background:#CFEAFF;"| 9
|style="background:#CFEAFF;"| 9
|
|
|
|
|
|
|
|
|
|
!58
|-
! 20
|align="left"| {{flagicon|USA}} [[Clint McMahan]]
|style="background:#CFEAFF;"| 7
|style="background:#CFCFFF;"| 23
|style="background:#EFCFFF;"| 23
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 19
|style="background:#CFCFFF;"| 16
|
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 19
|style="background:#FFFFFF;"| <small>DNS</small>
|
|style="background:#CFCFFF;"| 13
|style="background:#EFCFFF;"| 19
|style="background:#CFCFFF;"| 17
|
|
!52
|-
! 21
|align="left"| {{flagicon|USA}} [[Max Hanratty]]
|style="background:#EFCFFF;"| 26
|style="background:#CFCFFF;"| 20
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 16
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 19
|
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 12
|
|
|
|
|
|
|
!42
|-
! 22
|align="left"| {{flagicon|USA}} [[Phillippe Denes]]
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|style="background:#CFEAFF;"| 7
|style="background:#CFCFFF;"| 11
!24
|-
! 23
|align="left"| {{flagicon|NZL}} [[James Munro (racing driver)|James Munro]]
|style="background:#CFCFFF;"| 18
|style="background:#CFCFFF;"| 21
|style="background:#CFCFFF;"| 11
|style="background:#EFCFFF;"| 23
|
|
|
|
|
|
|
|
|
|
|
|
!20
|-
! 24
|align="left"| {{flagicon|USA}} [[Kaylen Frederick]]
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|style="background:#CFCFFF;"| 13
|style="background:#CFCFFF;"| 13
!16
|-
! 25
|align="left"| {{flagicon|COL}} [[Christian Muñoz (racing driver)|Christian Muñoz]]
|
|
|
|
|
|
|
|
|
|
|
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 15
|style="background:#CFCFFF;"| 19
|
|
!15
|-
! 26
|align="left"| {{flagicon|BRA}} [[Felipe Ortiz (racing driver)|Felipe Ortiz]]
|style="background:#EFCFFF;"| 21
|style="background:#CFCFFF;"| 11
|
|
|
|
|
|
|
|
|
|
|
|
|
|
!14
|-
! 27
|align="left"| {{flagicon|USA}} [[Devin Wojcik]]
|
|
|
|
|
|
|
|style="background:#CFCFFF;"| 14
|style="background:#CFCFFF;"| 16
|
|
|
|
|
|
|
!12
|-
! 28
|align="left"| {{flagicon|NZL}} [[Michael Scott (racing driver)|Michael Scott]]
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|style="background:#EFCFFF;"| 17
|style="background:#CFCFFF;"| 14
!12
|-
!colspan=19| National class
|-
|-
! 1
|align="left"| {{flagicon|USA}} [[Eric Filgueiras]]
|style="background:#FFFFBF;"| 14
|style="background:#FFFFBF;"| 17
|style="background:#EFCFFF;"| 22
|style="background:#FFFFBF;"| 18
|style="background:#FFFFBF;"| 21
|style="background:#EFCFFF;"| 25
|
|style="background:#EFCFFF;"| 22
|style="background:#EFCFFF;"| 24
|style="background:#FFFFBF;"| 14
|style="background:#FFFFBF;"| 13
|style="background:#EFCFFF;"| 20
|style="background:#EFCFFF;"| 20
|style="background:#FFFFFF;"| <small>DNS</small>
|
|
!262
|-
! 2
|align="left"| {{flagicon|USA}} [[Brendan Puderbach]]
|style="background:#DFFFDF;"| 19
|style="background:#DFDFDF;"| 18
|
|
|style="background:#DFFFDF;"| 25
|style="background:#EFCFFF;"| 22
|
|style="background:#DFDFDF;"| 19
|style="background:#EFCFFF;"| 22
|style="background:#EFCFFF;"| 18
|
|
|
|
|
|
!111
|-
! 3
|align="left"| {{flagicon|USA}} [[Robert Armington]]
|
|
|
|
|style="background:#DFDFDF;"| 22
|style="background:#DFDFDF;"| 18
|
|
|
|
|
|
|
|
|style="background:#FFFFBF;"| 16
|style="background:#FFFFBF;"| 17
!80
|-
! 4
|align="left"| {{flagicon|USA}} [[Dale VandenBush]]
|
|
|
|
|style="background:#EFCFFF;"| 26
|style="background:#FFFFBF;"| 17
|
|style="background:#FFFFBF;"| 18
|style="background:#FFFFBF;"| 18
|
|
|
|
|
|
|
!76
|-
! 5
|align="left"| {{flagicon|USA}} [[Charles Finelli]]
|style="background:#FFDF9F;"| 17
|style="background:#EFCFFF;"| 24
|
|
|style="background:#FFDF9F;"| 24
|
|
|
|
|
|
|
|
|
|
|
!42
|-
! 6
|align="left"| {{flagicon|USA}} [[Robert Allaer]]
|style="background:#DFDFDF;"| 16
|style="background:#FFDF9F;"| 19
|
|
|
|
|
|
|
|
|
|
|
|
|
|
!33
|- style="background: #f9f9f9;" valign="top"
!valign="middle"| Pos
!valign="middle"| Driver
!colspan=2| [[Grand Prix of St. Petersburg|STP]]
!colspan=2| [[Barber Motorsports Park|BAR]]
!colspan=2| [[Indianapolis Motor Speedway|IMS]]
! [[Lucas Oil Raceway at Indianapolis|LOR]]
!colspan=2| [[Road America|ROA]]
!colspan=2| [[Honda Indy Toronto|TOR]]
!colspan=3| [[Mid-Ohio Sports Car Course|MOH]]
!colspan=2| [[Mazda Raceway Laguna Seca|LAG]]
!valign="middle"| Points
|}
|valign="top"|
{|
|
{| style="margin-right:0; font-size:85%; text-align:center;" class="wikitable"
! Color
! Result
|- style="background:#FFFFBF;"
| Gold
| Winner
|- style="background:#DFDFDF;"
| Silver
| 2nd place
|- style="background:#FFDF9F;"
| Bronze
| 3rd place
|- style="background:#DFFFDF;"
| Green
| 4th & 5th place
|- style="background:#CFEAFF;"
| Light Blue
| 6th–10th place
|- style="background:#CFCFFF;"
| Dark Blue
| Finished<br />(Outside Top 10)
|- style="background:#EFCFFF;"
| Purple
| Did not finish
|- style="background:#FFCFCF;"
| Red
| Did not qualify<br />(DNQ)
|- style="background:#DFC484;"
| Brown
| Withdrawn<br />(Wth)
|- style="background:#000000; color:white;"
| Black
| Disqualified<br />(DSQ)
|- style="background:#FFFFFF;"
| White
| Did not start<br />(DNS)
|-
| Blank
| Did not<br />participate
|}
|-
|
{| style="margin-right:0; font-size:85%; text-align:center;" class="wikitable"
|style="background:#F2F2F2;" align=center colspan=2|'''In-line notation'''
|-
|style="background:#F2F2F2;" align=center|'''Bold'''
|style="background:#F2F2F2;" align=center|[[Pole position]]<br>(1 point)
|-
|style="background:#F2F2F2;" align=center|''Italics''
|style="background:#F2F2F2;" align=center|Ran fastest race lap<br>(1 point)
|-
|style="background:#F2F2F2;" align=center|<nowiki>*</nowiki>
|style="background:#F2F2F2;" align=center|Led most race laps<br>(1 point)<br>Not awarded if more than<br>one driver leads most laps</span>
|-
|style="background:Orange;" align=center colspan=2|Rookie
|}
|}
|}

===Teams' Championship===
{|class="wikitable" style="font-size: 95%;"
!Pos
!Team
!Points
|-
! 1
|Cape Motorsports w/ [[Wayne Taylor|Wayne Taylor Racing]]
!525
|-
! 2
|Pabst Racing
!338
|-
! 3
|ArmsUp Motorsports
!271
|-
! 4
|JAY Motorsports
!186
|-
! 5
|[[Team Pelfrey]]
!180
|-
! 6
|John Cummiskey Racing
!110
|-
! 7
|[[Chastain Motorsports]]
!52
|-
! 8
|RJB Motorsports
!34
|}

== References ==

<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.usf2000.com Official website]

{{Road to Indy}}
{{USF2000}}
{{Formula Ford years}}

[[Category:U.S. F2000 National Championship]]
[[Category:Articles created via the Article Wizard]]
[[Category:2016 in American motorsport|U.S. F2000 National Championship]]