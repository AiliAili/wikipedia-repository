{{Use mdy dates|date=January 2017}}
{{For|the station actually at 21st Street & Jackson Avenue|21st Street (IND Crosstown Line)}}

{{good article}}
{{Infobox NYCS
| name = 21st Street–Queensbridge
| accessible = yes
| acc_note = 
| image = 21 St Queensbridge platform.JPG
| image_caption = View of the station facing east
| bg_color = #FF6E1A
| address = 21st Street & 41st Avenue<br/>Queens, NY 11101
| borough = [[Queens]]
| locale = [[Queensbridge Houses|Queensbridge]] and [[Long Island City]]
| lat_deg = 40.753954
| lat_dir = N
| lon_deg = 73.942451
| lon_dir = W
| coordinates_display = inline,title
| division = IND
| line = [[63rd Street Lines|IND 63rd Street Line]]
| service = 63rd IND
| connection = {{bus icon}} '''[[MTA Bus]]''': {{NYC bus link|Q66|Q69|Q100|Q102|Q103}}
| platforms = 2 [[side platform]]s
| tracks = 2
| structure = Underground
| open_date = {{start date and age|1989|10|29}}<ref name="nyt-1989-10-29"/>
| passengers = 2,884,831<ref name="2015-rider">{{cite web|url=http://web.mta.info/nyct/facts/ridership/ridership_sub_annual.htm|title=Facts and Figures: Annual Subway Ridership|publisher=Metropolitan Transportation Authority|accessdate=April 19, 2016}}</ref>
| pass_year = 2015
| pass_percent = 7.5
| rank = 181 
| wifi = yes
| wifi_custom_ref = 1
| next_north = {{NYCS next | type=local | line=IND Queens Boulevard Line | station=36th Street | service=63rd IND local}}<br/>{{NYCS next | type=express | line=IND Queens Boulevard Line | station=Jackson Heights–Roosevelt Avenue | service=63rd IND express}}
| next_south = {{NYCS next | station=Roosevelt Island | line=IND 63rd Street Line | service=63rd IND}}
| next_north_acc = {{NYCS next |line=IND Queens Boulevard Line | station=Jackson Heights–Roosevelt Avenue | service=63rd IND express}}
| next_south_acc = {{NYCS next | station=Roosevelt Island | line=IND 63rd Street Line | service=63rd IND}}
| legend = {{NYCS infobox legend|alltimes}}
}}

'''21st Street–Queensbridge''' is a [[metro station|station]] on the [[63rd Street Lines|IND 63rd Street Line]] of the [[New York City Subway]]. Located at the intersection of 21st Street and 41st Avenue in the [[Queens]] neighborhood of [[Queensbridge, Queens|Queensbridge]], it is served by the '''{{NYCS|F}}''' train at all times.

__TOC__

==History==
[[File:21 St platfotm.JPG|thumb|left|250px|View of the overpass and station architecture]]
The current 63rd Street Line was the final version of proposals for a northern [[Midtown Manhattan|midtown]] tunnel from the [[IND Queens Boulevard Line]] to the [[Second Avenue Subway|Second]] and [[IND Sixth Avenue Line|Sixth Avenue]] lines, which date back to the [[IND Second System]] of the 1920s and 1930s.<ref name="Raskin-RoutesNotTaken-2013">{{cite book|author=Joseph B. Raskin|title=The Routes Not Taken: A Trip Through New York City's Unbuilt Subway System|url=https://books.google.com/books?id=CUlGCgAAQBAJ&pg=PA307|accessdate=August 12, 2015|date=November 1, 2013|publisher=Fordham University Press|isbn=978-0-8232-5369-2}}</ref><ref name="WheelsDroveNY-2012">{{cite book|author1=Roger P. Roess|author2=Gene Sansone|title=The Wheels That Drove New York: A History of the New York City Transit System|url=https://books.google.com/books?id=qfZ0VxuLoc0C&pg=PA416|date=August 23, 2012|publisher=Springer Science & Business Media|isbn=978-3-642-30484-2|pages=416–417}}</ref><ref name=IND2ndSystem1939Map>[[:Image:1939 IND Second System.jpg|Project for Expanded Rapid Transit Facilities, New York City Transit System]], dated July 5, 1939</ref><ref name="NYTimes-63St-Nowhere-1964"/> The current plans were drawn up in the 1960s under the [[Metropolitan Transportation Authority|MTA]]'s [[Program For Action]].<ref name="int-arch" /> In the original 1960s plans, there would have been a station (in addition to or as an alternative to 21st Street–Queensbridge) located farther east at [[Northern Boulevard]], one block north of the [[Queens Plaza (IND Queens Boulevard Line)|Queens Plaza]] station of the Queens Boulevard line. There would have been a pedestrian transfer passageway between the two stations.<ref name="NYTimes-QBLBypassDelay-1976">{{cite web|last1=Burks|first1=Edward C.|title=Shortage of U.S. Funds May Delay Subway Link|url=https://query.nytimes.com/mem/archive/pdf?res=9C06E7DA1F3AE034BC4E53DFB066838D669EDE|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=June 6, 1976}}</ref><ref name="NYTimes-QBLBypassDelay2(Map)-1976">{{cite web|last1=Burks|first1=Edward C.|title=New Subway Line Delayed 5 or 6 Years|url=https://query.nytimes.com/mem/archive/pdf?res=9503E1D61431EE34BC4151DFB166838D669EDE|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=July 29, 1976}}</ref><ref name="NYTimes-63StLightEndTunnel-1976">{{cite web|last1=Burks|first1=Edward C.|title=Coming: Light at End of 63d St. Tunnel|url=https://query.nytimes.com/mem/archive/pdf?res=9F0CE6DF1E3EEF34BC4C51DFBF66838D669EDE|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=September 24, 1976}}</ref><ref name="NYTimes-NYCSvsWorld-63Archer-1976">{{cite web|last1=Burks|first1=Edward C.|title=New York Improving Subway, But Still Trails Foreign Cities|url=https://query.nytimes.com/mem/archive/pdf?res=9E00E2DB123CE334BC4F53DFBE66838D669EDE|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=August 7, 1976}}</ref><ref name="nyt-1978-05-09"/>

The station was placed at 21st Street, serving the [[Queensbridge Houses]] to the west, and commercial and industrial buildings to the east. The station was added to the plans following lobbying from the local community.<ref name="nyt-1978-05-09">{{cite news |title=Planned 40-Mile Queens Subway, Cut to 15, is Costly and Behind Time |first=Grace |last=Lichtenstein |url=https://query.nytimes.com/mem/archive/pdf?res=9D0CE1D61230E632A2575AC0A9639C946990D6CF|newspaper=The New York Times |date=May 9, 1978 |page=68 |accessdate=October 20, 2011}}</ref><ref name="NYTimes-63St-TroubledProject-1984">{{cite web|last1=Daley|first1=Suzanne|title=63D ST. SUBWAY TUNNEL: MORE SETBACKS FOR A TROUBLED PROJECT|url=https://www.nytimes.com/1984/11/01/nyregion/63d-st-subway-tunnel-more-setbacks-for-a-troubled-project.html?pagewanted=all|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=November 1, 1984}}</ref><ref name="MTAMaps-LICQnsBdg-RsvltIs-2015" /> During construction, a large amount of disturbance was created along 41st Avenue, which runs through the heart of Queensbridge.<ref name="nyt-1978-05-09"/>

This station opened on October 29, 1989,<ref>{{cite web | title= 63 St Subway Extension Opened 25 Years Ago this Week | website=mta.info |publisher=[[Metropolitan Transportation Authority]] | date=October 31, 2014 | url=http://www.mta.info/news/2014/10/31/63-st-subway-extension-opened-25-years-ago-week | accessdate=July 28, 2016}}</ref> along with the entire IND 63rd Street Line, serving as the line's northern [[terminal station|terminal]] prior to the connection with the [[IND Queens Boulevard Line]].<ref name="nyt-1989-10-29">{{cite news |title=The 'Subway to Nowhere' Now Goes Somewhere |first=Donatella |last=Lorch |url=https://www.nytimes.com/1989/10/29/nyregion/the-subway-to-nowhere-now-goes-somewhere.html |newspaper=The New York Times |date=October 29, 1989 |accessdate=September 26, 2009}}</ref><ref>{{cite news |title=V Train Begins Service Today, Giving Queens Commuters Another Option |first=Sarah |last=Kershaw |url=https://www.nytimes.com/2001/12/17/nyregion/v-train-begins-service-today-giving-queens-commuters-another-option.html |newspaper=The New York Times |date=December 17, 2001 |accessdate=October 16, 2011}}</ref> The {{NYCS|Q}} train served the station on weekdays and the {{NYCS|B}} train stopped there on the weekends; both services used the Sixth Avenue Line.<ref name="nyt-1989-10-29" /> For the first couple of months after the station opened, the [[JFK Express]] to [[John F. Kennedy International Airport|Kennedy Airport]] also served the station until it was discontinued in 1990.<ref name="NYTimes-JFKExp-2009">{{Cite web|url=http://cityroom.blogs.nytimes.com/2009/11/25/if-you-took-the-plane-to-the-train-sing-the-jingle/|title=If You Took the Train to the Plane, Sing the Jingle|last=Grynbaum|first=Michael M.|access-date=July 3, 2016|date=November 25, 2009}}</ref> The tunnel had gained notoriety as the "tunnel to nowhere" both during its planning and after its opening, with 21st Street being the line's only stop in [[Queens]].<ref name="nyt-1989-10-29"/><ref name="NYTimes-63St-Nowhere-1964">{{cite web|last1=Knowles|first1=Clayton|title=Proposed Subway Tube Assailed As ‘Nowhere‐to‐Nowhere’ Link|url=https://www.nytimes.com/1964/12/16/proposed-subway-tube-assailed-as-nowheretonowhere-link.html?_r=0|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=December 16, 1964}}</ref> The connection to the Queens Boulevard Line began construction in 1994 and was completed and opened in 2001, almost thirty years after construction of the [[63rd Street Tunnel]] began.<ref name="MTA-FLineReview-2009">{{cite web|title=Review of F Line Operations, Ridership, and Infrastructure|url=http://www.nysenate.gov/files/pdfs/flinereport_0.pdf|website=[[New York State Senate|nysenate.gov]]|publisher=[[Metropolitan Transportation Authority|MTA]] [[New York City Transit Authority]]|accessdate=July 28, 2015|date=October 7, 2009|archiveurl=https://web.archive.org/web/20100531101000/http://www.nysenate.gov/files/pdfs/flinereport_0.pdf|archivedate=May 31, 2010|deadurl=yes}}</ref><ref name="MTA-GLineReview-2013">{{cite web|title=Review of the G Line|url=http://web.mta.info/nyct/service/G_LineReview_7_10_13.pdf|website=[[Metropolitan Transportation Authority|mta.info]]|publisher=[[Metropolitan Transportation Authority]]|accessdate=August 2, 2015|date=July 10, 2013}}</ref><ref name="BklynPaper-GTrainExt-April2012">{{cite web|last1=O'Neill|first1=Natalie|title=History shows it’s not the G train ‘extension’ — it’s the G train renewal|url=http://www.brooklynpaper.com/stories/35/15/dtg_gtrainhistory_2012_04_13_bk.html|website=[[The Brooklyn Paper]]|accessdate=August 2, 2015|date=April 13, 2012}}</ref><ref>{{cite web |url=http://www.subwaynut.com/brochures/63detour.htm |title=E,F Detour in 2001, F trains via 63 St, E no trains running, take R instead |work=The Subway Nut |accessdate=October 20, 2011}}</ref><ref name="nyt 200105">{{Cite news|first=Randy |last=Kennedy |url=https://www.nytimes.com/2001/05/25/nyregion/panel-approves-new-v-train-but-shortens-g-line-to-make-room.html |title=Panel Approves New V Train but Shortens G Line to Make Room |work=[[The New York Times]] |date=May 25, 2001 |accessdate=March 20, 2010}}</ref>

== Station layout ==
{{stack|float=right|
{{Routemap
|title=Track layout
|title-bg=#{{NYCS color|F}}
|title-color=white
|style=float:right;margin-top:5px;margin-left:10px;
|legend =track
|map=
uSTR!~MFADEg\uSTR!~MFADEg ~~ ~~ ~~ {{BSsplit|to [[Jackson Heights-Roosevelt Avenue (IND Queens Boulevard Line)|Roosevelt Av]] (express)|or [[36th Street (IND Queens Boulevard Line)|36 St]] (local)}}
uSTR!~uexSTRc2\uSTR!~uexSTRc2
ueABZg+1\ueABZg+1 ~~ ~~ ~~ Stub tracks
uSTRf\uSTRg
uSTR+BSr\uSTR+BSl
uSTR+BSr\uSTR+BSl
uSTR+BSr\uSTR+BSl
uSTR+BSr\uSTR+BSl
uABZg2\uABZg3
uABZg+1\uABZg+4
uSTRf\uSTRg
uSTR!~MFADEf\uSTR!~MFADEf ~~ ~~ ~~ to [[Roosevelt Island (IND 63rd Street Line)|Roosevelt Island]]
}}
}}
{|table border=0 cellspacing=0 cellpadding=3
|style="border-bottom:solid 1px gray;border-top:solid 1px gray;" width=50 valign=top|'''G'''
|style="border-top:solid 1px gray;border-bottom:solid 1px gray;" width=100 valign=top|Street Level
|style="border-top:solid 1px gray;border-bottom:solid 1px gray;" width=500 valign=top|Exit/Entrance
|-
|valign=top|'''B1'''
|valign=top|Mezzanine
|valign=top|Fare control, station agent, MetroCard vending machines, crossover<br/>{{NYCS Platform Layout access}}
|-
|style="border-top:solid 1px gray;border-bottom:solid 1px gray;" width=50 rowspan=4 valign=top|'''B2'''
|style="border-top:solid 1px gray;border-right:solid 2px black;border-left:solid 2px black;border-bottom:solid 2px black;" colspan=2 align=center|<small>[[Side platform]], doors will open on the right {{access icon}}</small>
|-
|style="border-bottom:solid 1px gray;" width=100|<span style=color:#{{NYCS color|orange}}>'''Southbound'''</span> 
|style="border-bottom:solid 1px gray;" width=500|← {{rint|newyork|F}} toward [[Coney Island–Stillwell Avenue (New York City Subway)|Coney Island–Stillwell Avenue]] <small>([[Roosevelt Island (IND 63rd Street Line)|Roosevelt Island]])</small>
|-
|<span style=color:#{{NYCS color|orange}}>'''Northbound'''</span> 
|<span style=color:white>→</span> {{rint|newyork|F}} toward [[Jamaica–179th Street (IND Queens Boulevard Line)|Jamaica–179th Street]] <small>([[Jackson Heights–Roosevelt Avenue (IND Queens Boulevard Line)|Jackson Heights–Roosevelt Avenue]])</small> →<br/><small>(No regular service: [[36th Street (IND Queens Boulevard Line)|36th Street]])</small>
|-
|style="border-top:solid 2px black;border-right:solid 2px black;border-left:solid 2px black;border-bottom:solid 1px gray;" colspan=2 align=center|<small>[[Side platform]], doors will open on the right {{access icon}}</small>
|-
|style="border-bottom:solid 1px gray;" width=50 rowspan=2 valign=top|'''B3'''
|style="border-bottom:solid 1px gray;" width=100|'''Track 1'''
|style="border-bottom:solid 1px gray;" width=500|← '''[[Long Island Rail Road|LIRR]] [[East Side Access]]''' (under construction)
|-
|style="border-bottom:solid 1px gray;"|'''Track 2'''
|style="border-bottom:solid 1px gray;"|<span style=color:white>→</span> '''[[Long Island Rail Road|LIRR]] [[East Side Access]]''' (under construction) →
|}
[[File:21 St Queensbridge entrance vc.jpg|thumb|left|250px|Escalator entrance]]
[[File:21st Street-Queensbridge Elevator.jpg|thumb|left|250px|Elevator entrance]]

This underground station's only [[mezzanine (architecture)|mezzanine]] is at the east end of station adjacent to the Manhattan-bound platform. Access to both platforms is via an overpass above the tracks, with staircases, escalators and elevators to platform level. At this point, the station has a high ceiling.<ref name="NYCDCP-SilvercupWestTransit">{{cite web|title=Silvercup West FEIS:10.0 Transit and Pedestrians|url=http://www1.nyc.gov/assets/planning/download/pdf/applicants/env-review/silvercup_west/ch10_feis.pdf#page=6|website=[[Government of New York City|nyc.gov]]|publisher=[[New York City Department of City Planning]]|archiveurl=https://web.archive.org/web/20061013231112/http://www.nyc.gov/html/dcp/pdf/env_review/silvercup_west/ch10_feis.pdf#page=6|archivedate=October 13, 2006|deadurl=no|accessdate=August 15, 2016}}</ref> The platform walls as well as the floor are made of brick, and towards the top of the platform walls is a line of larger brown sheets, on these are the station signs at regular intervals that say "21 Street–Queensbridge." Above this is a thin black strip of metal and above this are yellow squares that take the platform walls up to the station ceiling that is made of concrete.<ref name=":0">{{Cite web|url=http://subwaynut.com/ind/21_queensbridgef/index.php|title=21 St-Queensbridge (F) - The SubwayNut|last=Cox|first=Jeremiah|website=subwaynut.com|access-date=August 15, 2016}}</ref> The two [[side platform]]s do not have yellow tactile strips on the platform edges, which is a characteristic of newly renovated and [[Americans with Disabilities Act of 1990|ADA-accessible]] New York City Subway stations. There are also no columns between the two tracks or on the platforms, except near the mezzanine and overpass.<ref name="NYTimes-NYCSvsWorld-63Archer-1976" /><ref name="NYCDCP-SilvercupWestTransit"/>

===Exits===
Outside of [[fare control]], the mezzanine leads to two street stairs at the northeast corner of 21st Street and 41st Avenue. An elevator and escalators are at the northwest corner of the same intersection.<ref name="MTAMaps-LICQnsBdg-RsvltIs-2015">{{cite web|title=MTA Neighborhood Maps: Long Island City|url=http://web.mta.info/maps/neighborhoods/qns/Q3-LIC_2015.pdf|publisher=[[Metropolitan Transportation Authority]]|accessdate=September 27, 2015|date=2015}}</ref><ref name="NYCDCP-SilvercupWestTransit"/> 

===Track layout===
Until the connection to the Queens Boulevard Line opened, this station shared the characteristic of a two side platformed terminal station with [[Flatbush Avenue–Brooklyn College (IRT Nostrand Avenue Line)|Flatbush Avenue–Brooklyn College]] on the [[IRT Nostrand Avenue Line]]. This is an inefficient terminal setup,<ref name="int-arch">{{cite web | title=Full text of "Metropolitan transportation, a program for action. Report to Nelson A. Rockefeller, Governor of New York." | website=Internet Archive | date=November 7, 1967 | url=https://archive.org/stream/metropolitantran00newy/metropolitantran00newy_djvu.txt | accessdate=October 1, 2015}}</ref> requiring passengers to know which track the next train will depart from before going to the platform level. As a terminal from 1989 to 2001, the station had tail tracks that continued eastward as far as 29th Street, ending at [[bumper block]]s.<ref name="QueensSubwayEIS-1984">{{cite book|title=Queens Subway Options Study, New York: Environmental Impact Statement|url=https://books.google.com/books?id=NaI4AQAAMAAJ&pg=PA83|accessdate=July 10, 2016|date=May 1984|publisher=[[United States Department of Transportation]], [[Metropolitan Transportation Authority]], [[Urban Mass Transit Administration]]|pages=83–}}</ref><ref name="QBL63rdLineConnector-1992" /><ref>{{cite web|title=PLAYING IN THE NEIGHBORHOOD: LONG ISLAND CITY; Tortoise Heads Into Queens|url=https://www.nytimes.com/1998/10/18/nyregion/playing-in-the-neighborhood-long-island-city-tortoise-heads-into-queens.html|publisher=[[The New York Times]]|accessdate=September 27, 2015|date=October 18, 1998}}</ref> Also, this station has "[[punch box]]es", with buttons to indicate route selection to the train dispatcher; a control tower on the west end of [[Manhattan]]-bound platform, which can be used if necessary; and a [[diamond crossover]] switch to the west which was used to turn trains.<ref name="tracks-469">{{NYCS const|trackref|469}}</ref>

====Stub tracks east of the station====
East of the station, before the line connects to the [[IND Queens Boulevard Line]], the tracks veer left while the tunnel wall goes straight, stopping around Northern Boulevard.<ref name="QueensSubwayEIS-1984" /><ref name="QBL63rdLineConnector-1992"/><ref name=MTA63rdQBLConnectorTrackMap>{{cite web|title=MTA 63rd Street Connector|url=http://nycsubway.org.s3.amazonaws.com/images/logo/63rdconn.jpg|publisher=[[Metropolitan Transportation Authority]]|accessdate=October 1, 2015|archiveurl=https://web.archive.org/web/20141030120053/http://nycsubway.org.s3.amazonaws.com/images/logo/63rdconn.jpg|deadurl=no|archivedate=October 30, 2014}}</ref><ref name="CivilEng-FinalConnection-Jul2000">{{cite journal|last1=Silano|first1=Louis G.|last2=Shanbhag|first2=Radmas|title=The Final Connection|journal=[[American Society of Civil Engineers|Civil Engineering]]|date=July 2000|volume=86|issue=7|pages=56-61}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=6aHFItRehRA&feature=related|title=Railfan Window Of An R32 F Train From 57th Street to 36th Street, Queens Part Two  (''The bellmouth for the intended super-express bypass can be seen towards the right, at the 3:09 mark into the video.'')|author=Caitsith810|publisher=Youtube|date=December 17, 2008|accessdate=October 4, 2010}}</ref><ref name="EastSideAccessEIS-2001"/> This [[Bellmouth (railroad terminology)|bellmouth]] is part of an intended [[Queens Super-Express Bypass|"super-express" bypass of the IND Queens Boulevard Line]] running along the [[Main Line (Long Island Rail Road)|mainline]] of the [[Long Island Rail Road]] between Queens Boulevard and [[Forest Hills–71st Avenue (IND Queens Boulevard Line)|Forest Hills–71st Avenue]] planned in 1968. At a proposed station at [[New York State Route 25A|Northern Boulevard]], for which the 29th Street tail tracks might have also been built, a transfer concourse to the Queens Plaza station would have allowed transfers between local, express, and bypass trains.<ref name="NYTimes-QBLBypassDelay-1976" /><ref name="NYTimes-QBLBypassDelay2(Map)-1976" /><ref name="NYTimes-63StLightEndTunnel-1976"/><ref name="NYTimes-NYCSvsWorld-63Archer-1976" /><ref name="QueensSubwayEIS-1984" /><ref name="ERA-TechTalk-NYCS-Feb2005">{{cite journal|last1=Erlitz|first1=Jeffrey|title=Tech Talk|journal=New York Division Bulletin|date=February 2005|volume=48|issue=2|pages=9-11|url=https://issuu.com/erausa/docs/2005-02-bulletin/11|accessdate=July 10, 2016|publisher=Electric Railroaders Association}}</ref><ref name="CivilEng-FinalConnection-Jul2000"/>

The current bellmouth, built along with the Queens Boulevard connection, is two levels deep with two additional stub-end subway tracks named T1A and T2A.<ref name="EastSideAccessEIS-2001"/> It is viable for future construction of the bypass or the Northern Boulevard transfer station. The original bellmouth stopped at 29th Street.<ref name="QBL63rdLineConnector-1992">{{cite book|title=Final Environmental Impact Statement for the 63rd Street Line Connection to the Queens Boulevard Line|url=https://books.google.com/books?id=n943AQAAMAAJ&pg=RA3-PT95|accessdate=July 23, 2016|year=June 1992|publisher=[[Metropolitan Transportation Authority]], [[United States Department of Transportation]], [[Federal Transit Administration]]|location=[[Queens]], [[New York City|New York]], [[New York (state)|New York]]}}</ref><ref name="EastSideAccessEIS-2001"/> The lower level of the bellmouth was excavated in 2003 for the LIRR [[East Side Access]] project, which also extended the subway stub tracks farther east towards [[Sunnyside Yard]].<ref name="EastSideAccessEIS-2001">{{cite book|title=East Side Access in New York, Queens, and Bronx Counties, New York, and Nassau and Suffolk Counties, New York: Environmental Impact Statement|url=https://books.google.com/books?id=USs3AQAAMAAJ&pg=RA1-SA2-PA12|accessdate=July 23, 2016|date=March 2001|publisher=[[Metropolitan Transportation Authority]], [[United States Department of Transportation]], [[Federal Transit Administration]]}}</ref><ref name="Ozdemir2004">{{cite journal|last1=Nasri|first1=V.|last2=Lee|first2=W.S.|last3=Rice|first3=J.|title=Comparison of the predicted behavior of the Manhattan TBM launch shaft with the observed data, East Side Access Project, New York|journal=North American Tunneling|date=2004|pages=537-544|url=https://books.google.com/books?id=JCJnZLCuOUEC&pg=PA537&lpg=PA537&dq=%22east+side+access%22+%22bellmouth%22&source=bl&ots=zuTFykrI7G&sig=vBoGCL-2iZ2PNii_DjicI4Vxr78&hl=en&sa=X&ved=0ahUKEwiO08C2-IjOAhUGpB4KHZ99BaY4ChDoAQgvMAQ#v=onepage&q=%22east%20side%20access%22%20%22bellmouth%22&f=false|accessdate=July 23, 2016|publisher=[[Taylor & Francis]]}}</ref><ref>{{cite news|last1=Reed|first1=Mary|title=Tunnel Boring Machines Core Under Big Apple’s East River|url=http://celerra.judlau.com/files%5CConstructionEquipmentGuide.pdf|accessdate=July 23, 2016|work=ConstructionEquipmentGuide.com}}</ref><ref>{{cite web|last=mtainfo|title=East Side Access – 1/24/2012 Update|url=https://www.youtube.com/watch?v=jJsp63w4lVs| publisher = [[Metropolitan Transportation Authority]] (via its [[YouTube]] channel) |accessdate= May 8, 2012}}</ref> Just above the connection sits the 29th Street Ventilation Complex, built with the connector, in the site of a former parking lot.<ref name="QBL63rdLineConnector-1992"/><ref name="MTA63rdQBLConnectorTrackMap" /><ref name="CivilEng-FinalConnection-Jul2000" /><ref name="EastSideAccessEIS-2001"/> West of the station, a second ventilation complex lies in Queensbridge Park between Vernon Boulevard and the [[East River]].<ref name="EastSideAccessEIS-2001"/>

{{-|left}}

==Ridership==
In 2015, the station had 2,884,831 boardings, making it the 181st most used station in the {{NYCS const|number|intl}}-station system.<ref name="2015-rider"/> This amounted to an average of 9,447 passengers per weekday.<ref>{{cite web | title=Facts and Figures: Weekday Ridership | website=mta.info | url=http://web.mta.info/nyct/facts/ridership/ridership_sub.htm | accessdate=April 26, 2016}}</ref>

== References ==
{{Reflist|30em}}

== External links ==

{{Commons category|21st Street – Queensbridge (IND 63rd Street Line)}}
* [https://maps.google.com/?ie=UTF8&ll=40.754003,-73.942419&spn=0,0.013765&z=17&layer=c&cbll=40.754079,-73.942348&panoid=lZkBWDDgAjtxKm6uapKhvg&cbp=12,115.92,,0,4.93 21st Street entrance from Google Maps Street View]
* [http://www.google.com/maps/@40.754509,-73.9421844,3a,75y,187.49h,90.71t/data=!3m8!1e1!3m6!1s-DtkY3xEUj_E%2FV4bZx5muo-I%2FAAAAAAAAJtI%2FvychskyvEaUdab_4xmEU6-0XAKgyEbn5wCLIB!2e4!3e11!6s%2F%2Flh4.googleusercontent.com%2F-DtkY3xEUj_E%2FV4bZx5muo-I%2FAAAAAAAAJtI%2FvychskyvEaUdab_4xmEU6-0XAKgyEbn5wCLIB%2Fw203-h100-p-k-no%2F!7i9728!8i4864!4m3!8m2!3m1!1e1!6m1!1e1 Platforms from Google Maps Street View]
* [http://www1.nyc.gov/assets/planning/download/pdf/applicants/env-review/silvercup_west/ch10_feis.pdf#page=6 Station Diagram]

{{NYCS stations navbox by service|lf=y}}
{{NYCS stations navbox by line|63st=yes}}

{{DEFAULTSORT:21st Street - Queensbridge (Ind 63rd Street Line)}}
[[Category:63rd Street Line stations]]
[[Category:Program for Action]]
[[Category:New York City Subway stations in Queens, New York]]
[[Category:New York City Subway stations located underground]]
[[Category:Railway stations opened in 1989]]
[[Category:Long Island City]]
[[Category:1989 establishments in New York]]