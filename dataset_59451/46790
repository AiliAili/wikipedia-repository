{{Use mdy dates|date=April 2016}}
{{other people}}
{{Infobox military person
|name= Edmond Butler
|birth_date= 1827
|death_date= {{Death date and age|1895|8|21|1827}}
|birth_place= [[Ireland]]
|death_place=[[Trouville-sur-Mer|Trouville, Normandy]], [[France]]
|placeofburial=[[Holy Sepulchre Cemetery (Omaha, Nebraska)|Holy Sepulchre Cemetery]], [[Omaha, Nebraska]]
|placeofburial_label= Place of burial
|image= EdmondButler.jpg
|caption= Edmond Butler
|nickname=
|allegiance= [[United States|United States of America]]<br/>[[Union (American Civil War)|Union]]
|branch= [[United States Army]]<br/>[[Union Army]]
|serviceyears=1861&ndash;1891
|rank= [[File:Union Army LTC rank insignia.png|35px]] [[Lieutenant colonel (United States)|Lieutenant Colonel]]
|commands=
|unit=[[5th Infantry Regiment (United States)|5th U.S. Infantry Regiment]]
|battles= [[American Civil War]]<br/>[[Indian Wars]]
|awards= [[File:Medal of Honor ribbon.svg|35px]] [[Medal of Honor]]
|laterwork=
}}

'''Edmund''' or '''Edmond Butler''' (March 19, or September 19, 1827 – August 21, 1895) was a [[United States Army|U.S. Army]] officer who served with the Union Army during the [[American Civil War]] and later became a prominent Indian fighter in the [[Great Plains|Northern Plains]], [[Rocky Mountains]] and [[southwest United States]] in the post-Civil War era. In 1877, he was awarded the [[List of Medal of Honor recipients for the Indian Wars|Medal of Honor]] for gallantry against the Sioux and Cheyenne at [[Battle of Wolf Mountain]].

==Biography==
Edmond Butler was born in Ireland and immigrated to the United States as a young man. Shortly after arriving in [[Brooklyn, New York]], he enlisted in the [[United States Army|U.S. Army]] and commissioned as a second lieutenant in the [[5th U.S. Infantry]] in October 1861. He was also assigned to special duty with the inspector of volunteer units in [[Kansas]] and [[Missouri]].

In 1862, he was sent to the [[New Mexico Territory]] and later assisted in the reconstruction of [[Fort Bliss, Texas|Fort Bliss]] after its recapture by the Union. Promoted to captain in 1864, he was eventually reassigned to [[Fort Wingate, New Mexico]] and, in 1865, commanded an expedition against the [[Navajo people|Navajo]]s living in [[Canyon de Chelly]]. Intercepting a Navajo raiding party under [[Manuelito Grande]], he recovered a number of sheep and other livestock taken from the neighboring [[Apache]]. After a period of 22 days, in which he had covered 720 miles, 31 Navajos were killed while another 27 were captured. He was also involved in the relocation of 3,000 Navajo to the [[Fort Sumner Reservation]] on the [[Pecos River]].

Transferred to Kansas in 1866, he spent two years there before being assigned to the [[Beecher Island]]-site in December 1868 to bring in the bodies of the soldiers killed during the [[Battle of Beecher Island]]. Despite a large Sioux presence in the area, Butler successfully removed the bodies from the site despite being "under the fire of the main body of Sioux".{{Attribution needed|date=March 2011}} However, he was unable to find the remains of Lieutenant [[Fredrick H. Beecher]] and Acting Surgeon J.H. Mooer, suggesting their bodies had been removed by the Sioux "probably in revenge for rifling Sioux graves on the Republican (earlier by camp followers)".{{Attribution needed|date=March 2011}}

During 1869, while assigned to guard the [[Fort Wallace]]–[[Denver]] stage route, Butler volunteered to join an expedition under Lieutenant Colonel [[Charles R. Woods]] against the [[Pawnee people|Pawnees]].

Returning to Kansas in October 1871, he was assigned to operations to control "organized land-leaguers"{{Attribution needed|date=March 2011}} in the southeast. Three years later, he served with Lieutenant General [[Nelson A. Miles]] during the [[Red River Campaign]] and in the [[Black Hills War]] in which he led six companies in pursuit of [[Sitting Bull]]. Although Sitting Bull and Gall escaped to Canada, he was involved in the capture of eight other Sioux chieftains and around 700 lodges.

On January 8, 1877, Butler took part in the engagement against the Sioux at Wolf Mountain. In command of Company C, he was commended for his actions during the battle for ''"conspicuous gallantry in leading his command against greatly superior numbers of hostile Indians, strongly entrenched"'' and receiving a brevet of [[Major (United States)|major]] and awarded the Medal of Honor.

Later that year, he escorted [[Chief Joseph]] and other Nez Perce to [[Fort Buford]] between October and November 1877. During the early 1880s, he was stationed at [[Fort Snelling, Minnesota]] and [[Fort Keogh|Fort Keogh, Montana]] and guarded construction parties of the [[Northern Pacific Railroad]]. He also held numerous staff positions and eventually awarded the rank of [[Lieutenant colonel (United States)|lieutenant colonel]] shortly before his retirement on March 9, 1891. He died in [[Trouville-sur-Mer|Trouville, France]] three years later and, his body being returned to the United States, later buried in [[Holy Sepulchre Cemetery (Omaha, Nebraska)|Holy Sepulchre Cemetery]] in [[Omaha, Nebraska]].

==See also==
{{Portal|Biography|United States Army|American Civil War}}
*[[List of Medal of Honor recipients]]
*[[List of Medal of Honor recipients for the Indian Wars]]

==References==
*Greene, Jerome A. ''Battles and Skirmishes of the Great Sioux War, 1876-1877: The Military View''. Norman: University of Oklahoma Press, 1993. ISBN 0-8061-2669-8
*Thrapp, Dan L. ''Encyclopedia of Frontier Biography: In Three Volumes, Volume I (A–F)''. Lincoln: University of Nebraska Press, 1988. ISBN 0-8032-9418-2
*[https://archive.org/stream/officersofarmyna00powe/officersofarmyna00powe_djvu.txt Civil War Officers]

==Further reading==
*Miles, Nelson A. ''Personal Recollections''. Chicago: Riverside Publishing Company, 1897.
*Beyer, Walter Frederick and Oscar Frederick Keydel. ''Deeds of Valor, Vol. II''. Detroit: Perrien-Keydel Company, 1907.

==External links==
*{{Find a Grave|12326|work=Claim to Fame: Medal of Honor recipients|accessdate=August 6, 2008}}
*{{Cite web |accessdate=September 27, 2010 |url=http://www.homeofheroes.com/gravesites/states/pages_af/butler_edmond.html |title=Edmond Thomas Butler, Photo of Grave site of MOH Recipient Edmond Butler}}

{{DEFAULTSORT:Butler, Edmond}}
[[Category:1827 births]]
[[Category:1895 deaths]]
[[Category:19th-century Irish people]]
[[Category:Irish emigrants to the United States (before 1923)]]
[[Category:American people of the Indian Wars]]
[[Category:United States Army Medal of Honor recipients]]
[[Category:Nez Perce War]]
[[Category:People of the Great Sioux War of 1876]]
[[Category:Union Army officers]]
[[Category:United States Army colonels]]
[[Category:Irish-born Medal of Honor recipients]]
[[Category:Irish soldiers in the United States Army]]
[[Category:American Indian Wars recipients of the Medal of Honor]]