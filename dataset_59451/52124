{{Infobox journal
| title = Journal of Vibration and Control
| cover = [[File:Journal of Vibration and Control.jpg]]
| editor = Mehdi Ahmadian, Fabio Casciati, Fabrizio Vestroni
| discipline = [[Vibration]]
| former_names =
| abbreviation = J. Vib. Control
| publisher = [[Sage Publications]]
| country = 
| frequency = 16/year
| history = 1995-present
| openaccess = 
| license = 
| website = http://www.sagepub.com/journals/Journal201401/title
| link1 = http://jvc.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jvc.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 40095739
| LCCN = 95660075
| CODEN = JVCOFX
| ISSN = 1077-5463
| eISSN = 1741-2986
}}
The '''''Journal of Vibration and Control''''' is a [[peer-reviewed]] [[scientific journal]] that covers all aspects of linear and nonlinear [[vibration]] phenomena and their control. It was established in 1995 and is published by [[Sage Publications]]. The [[editors-in-chief]] are Mehdi Ahmadian ([[Virginia Tech]]), Fabio Casciati ([[University of Pavia]]), and Fabrizio Vestroni ([[Sapienza University of Rome]]).

==Peer review fraud ==
In 2014, Sage Publications retracted 60 papers published in the journal due to a case of academic fraud, in which Peter Chen ([[National Pingtung University of Education]], Taiwan) created false accounts in order to subvert the peer review process.<ref>{{cite web |url=http://www.uk.sagepub.com/aboutus/press/2014/jul/7.htm |title=Retraction Notice |journal=Journal of Vibration and Control |publisher=SAGE |date=2014-07-09}}</ref> Ali H. Nayfeh, the editor-in-chief of the journal, retired from his academic post and resigned from his position with the journal following completion of the investigation.<ref>{{cite news |last=Barbash |first=Fred |url=http://www.washingtonpost.com/news/morning-mix/wp/2014/07/10/scholarly-journal-retracts-60-articles-smashes-peer-review-ring/ |title=Scholarly journal retracts 60 articles, smashes "peer review ring" |newspaper=[[The Washington Post]] |date=2014-02-02 |accessdate=2014-07-10}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. In 2014 the journal had its impact factor suppressed due to anomalous stacked citation patterns.<ref>{{cite web|url=http://ipscience-help.thomsonreuters.com/incitesLiveJCR/JCRGroup/titleSuppressions.html|accessdate=23 June 2015}}</ref>  In 2016, the journal is included in the Journal Citation Reports and its impact factor becomes 1.643 <ref>{{cite web|url=http://scientific.thomsonreuters.com/imgblast/JCRFullCovlist-2016.pdf}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201401/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1995]]
[[Category:Physics journals]]