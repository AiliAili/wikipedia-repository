{{Infobox company
 | name             = Little Pine
 | logo             =
 | image          = File:Little Pine restaurant.jpg
 | image_size     = 250
 | image_alt        = Exterior photo of Little Pine restaurant in Los Angeles, California
 | image_caption    = Exterior photo of Little Pine restaurant in Los Angeles, California
 | type             =  
 | genre            =
 | foundation       = November 2015
 | location_city    = [[Los Angeles]], California
 | location_country = United States
 | location         = 2870 Rowena Ave
 | coordinates      = {{Coord|34.10775|-118.26679|type:landmark_region:US-CA|display=inline,title}}
 | locations        =
 | founder          = [[Moby]]
 | area_served      = 
 | key_people       = 
 | industry         = [[Restaurant]] <br />Retail
 | products         = [[Organic food|Organic]], [[Veganism|vegan]] food
 | services         = 
 | revenue          = 
 | operating_income = 
 | net_income       = 
 | assets           = 
 | equity           = 
 | owner            = Moby
 | num_employees    = 
 |num_locations  =  1
 | parent           =
 | divisions        =
 | subsid           = 
 | slogan           =
 | homepage         = {{url|http://www.littlepinerestaurant.com}}
 | footnotes        =
 | intl             =
}}
'''Little Pine''' is a [[Veganism|vegan]] [[bistro]] located in the [[Silver Lake, Los Angeles|Silver Lake neighborhood]] of [[Los Angeles]], California.<ref name="LA Times"/><ref name="Zagat 2015"/> It was founded by electronic musician and animal rights activist [[Moby]].<ref name="LA Magazine"/> The restaurant serves [[Organic food|organic]], vegan, [[Mediterranean cuisine|Mediterranean]]-inspired dishes and has a retail section with art and books, curated by Moby himself.<ref name="Zagat 2015"/><ref name="LA Magazine"/> Moby's vision was to create a space that includes various aspects of society that he cares about. All of the restaurant's profits are donated to [[animal welfare]] organizations.

==Overview==
Little Pine opened on November 19, 2015 and is located in the Silver Lake neighborhood of Los Angeles, California.<ref name="Variety"/> Original plans for the restaurant to open in the summer of 2015 were delayed.<ref name="Rodell 2015"/> Moby opened the restaurant to create a space that encompasses various matters that he cares about, such as [[veganism]], [[organic food]], community, architecture and design.<ref name="Little Pine"/> Moby has stated that Little Pine was created to provide a "... compelling representation of veganism"<ref name="The Hollywood Reporter 2015"/> and to provide a space as "... an extension of our neighborhood and community."<ref name="Broverman 2015"/> Ann Thornton was the initial coordinating chef for the restaurant, and as of November 2015 Kristyne Starling is the head chef.<ref name="LA Magazine"/> 

The restaurant's decor has chandeliers and artistic murals created by various local artists,<ref name="Zagat 2015"/> and photographic works of nature created by [[Moby]].<ref name="LA Magazine"/><ref name="KCBS-TV-1"/> The general decorum was designed by interior designer Tatum Kendrick.<ref name="Rodell 2015"/><ref name="Broverman 2015"/> Little Pine is located in an [[Art Deco]]-style building that was built in the 1940s.<ref name="LA Magazine"/><ref name="InStyle.com 2015"/> The restaurant has 58 seats.<ref name="Kenney 2016"/> Moby does not allow his own music to be played in the restaurant,<ref name="InStyle.com 2015"/><ref name="Barrera 2016"/> and has been a vegan for 28 years.<ref name="KCBS-TV-1"/>

==Fare==
Little Pine purveys all-organic and all plant-based, vegan dishes influenced by [[Mediterranean cuisine]]<ref name="Zagat 2015"/><ref name="KCBS-TV-1"/> and influences of the cuisine, such as Italian, Spanish and French cuisines, as well as minor aspects of North African cuisine.<ref name="LA Magazine"/> Brunch dishes include French toast, pumpkin pancakes and biscuits and gravy.<ref name="Zagat 2015"/> Entrée dishes include pasta dishes such as stuffed [[Conchiglie|shells]], [[cassoulet]] and meatless meatballs, among others.<ref name="Zagat 2015"/> The cassoulet, a variety of [[casserole]] that is traditionally a meat-based dish, took months of experimentation to develop,<ref name="InStyle.com 2015"/> and is lighter compared to the traditional dish.<ref name="Barrera 2016"/> [[Organic wine]], various beers, tea and coffee are also served,<ref name="Zagat 2015"/> as well as [[Plant milk|cashew milk]].<ref name="Kenney 2016"/> Food ingredients are sourced [[Local food|locally]] whenever possible.<ref name="LA Magazine"/>

==Philanthropy==
Little Pine donates all of its profits to [[animal welfare]] organizations.<ref name="Stutz 2016"/> Donations are given to [[The Humane Society]], [[PETA]], the [[Animal Legal Defense Fund]], [[Animal Legal Defense Fund]] and [[Sea Shepherd Conservation Society|Sea Shepard]], among others.<ref name="Kenney 2016"/><ref name="Stutz 2016"/><ref name="Spin 2016"/> The donations commenced when the restaurant began to break even<ref name="Barrera 2016"/> relative to its operating expenditures.

==References==
{{reflist|30em|refs=
<ref name="LA Times">{{cite web|url=http://www.latimes.com/food/dailydish/la-dd-moby-little-pine-vegan-silver-lake-20151109-story.html|title=Moby dishes on Little Pine, his new vegan restaurant in Silver Lake|author=|date=9 November 2015|work=[[Los Angeles Times]]|accessdate=17 January 2016}}</ref>
<ref name="LA Magazine">{{cite web|url=http://www.lamag.com/digestblog/moby-talks-vegan-meatballs-and-stuffed-shells-at-little-pine/|title=Moby Talks Vegan Meatballs and Stuffed Shells at Little Pine|author=Lesley Balla|date=17 November 2015|work=[[Los Angeles (magazine)|Los Angeles Magazine]]|accessdate=17 January 2016}}</ref>
<ref name="Stutz 2016">{{cite web | last=Stutz | first=Colin | title=Moby's New Vegan Restaurant is Giving All Its Profits to Animal Welfare Groups | website=[[Billboard (magazine)|Billboard]] | date=January 5, 2016 | url=http://www.billboard.com/articles/news/dance/6835247/moby-vegan-restaurant-little-pine-profits-to-animal-welfare | accessdate=January 17, 2016}}</ref>
<ref name="Zagat 2015">{{cite web | title=Moby's Little Pine Vegan Restaurant Debuts in Silver Lake | author=Balla, Lesley | website=[[Zagat]] | date=November 20, 2015 | url=https://www.zagat.com/b/los-angeles/mobys-little-pine-vegan-restaurant-debuts-in-silver-lake | accessdate=January 17, 2016}}</ref>
<ref name="Variety">{{cite web|url=http://variety.com/2015/scene/real-estate/moby-opens-silver-lake-restaurant-little-pine-1201648866/|title=Moby on His Memoir and Music at New Silver Lake Restaurant Little Pine – Variety|author=Jasmin Rosemberg|work=[[Variety (magazine)|Variety]]|accessdate=17 January 2016}}</ref>
<ref name="Little Pine">{{cite web | url=http://www.littlepinerestaurant.com/ | title=About | publisher=Little Pine | accessdate=16 January 2016}}</ref>
<ref name="Broverman 2015">{{cite web | last=Broverman | first=Neal | title=Moby's Forthcoming Vegan Concept in Silver Lake | website=Los Angeles Magazine | date=June 23, 2015 | url=http://www.lamag.com/digestblog/the-411-on-on-mobys-forthcoming-vegan-concept-in-silver-lake/ | ref=harv | accessdate=January 17, 2016}}</ref>
<ref name="The Hollywood Reporter 2015">{{cite web | title=Moby on His Silverlake Restaurant Little Pine: | website=[[The Hollywood Reporter]] | date=October 26, 2015 | url=http://www.hollywoodreporter.com/video/moby-his-silverlake-restaurant-little-833671 | accessdate=January 17, 2016}}</ref>
<ref name="Rodell 2015">{{cite web | last=Rodell | first=Besha | title=Moby's New Vegan Restaurant in Silver Lake Is Opening Next Week | website=[[LA Weekly]] | date=November 10, 2015 | url=http://www.laweekly.com/restaurants/mobys-new-vegan-restaurant-in-silver-lake-is-opening-next-week-6260818 | accessdate=January 17, 2016}}</ref>
<ref name="InStyle.com 2015">{{cite web | author=Stern, Claire| title=Moby on His New Vegan Restaurant, Little Pine (and Why It Has a Strict "No Moby Music" Policy) | website=[[InStyle]]| date=November 19, 2015 | url=http://www.instyle.com/news/moby-little-pine-vegan-restaurant-interview | accessdate=January 17, 2016}}</ref>
<ref name="Spin 2016">{{cite web | title=Moby Will Give All Profits From His Vegan Restaurant to Animal Welfare Groups | website=[[Spin (magazine)|Spin]] | date=January 5, 2016 | url=http://www.spin.com/2016/01/moby-vegan-restaurant-animal-welfare-charity-little-pine/ | accessdate=January 17, 2016}}</ref>
<ref name="Kenney 2016">{{cite web | title=Little Pine, Big Heart: Moby’s Newest Adventure | author=Kenney, Shawna | website=[[Paste (magazine)|Paste]] | date=January 13, 2016 | url=http://www.pastemagazine.com/articles/2016/01/little-pine-big-heart-mobys-newest-adventure.html | accessdate=January 17, 2016}}</ref>
<ref name="Barrera 2016">{{cite web | last=Barrera | first=Sandra | title=Moby opens vegan restaurant in L.A., gives profits to animal charities | website=[[Los Angeles Daily News]] | date=January 12, 2016 | url=http://www.dailynews.com/arts-and-entertainment/20160112/moby-opens-vegan-restaurant-in-la-gives-profits-to-animal-charities | ref=harv | accessdate=January 17, 2016}}</ref>
<ref name="KCBS-TV-1">{{cite web | title=Music Mogul Moby Dishes On New Vegan Restaurant In Silver Lake | publisher=[[KCBS-TV|CBS Los Angeles]] | date=November 17, 2015 | url=http://losangeles.cbslocal.com/2015/11/17/music-mogul-moby-dishes-on-new-vegan-restaurant-in-silver-lake/ | accessdate=January 17, 2016}}</ref>
}}

==External links==
*{{official website|http://www.littlepinerestaurant.com}}

{{portalbar|Companies|Food|Los Angeles}}

[[Category:Organic food]]
[[Category:Restaurants in Los Angeles]]
[[Category:Vegan restaurants in the United States]]