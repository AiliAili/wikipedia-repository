{{Infobox fire department
| name                   =Hartford Fire Department  
| logo                   =
| motto                  =
<!-- Operational Area -->
| country                = {{USA}}
| subdivision_type1      = [[U.S. State|State]]
| subdivision_name1      = {{flag|Connecticut}}
| subdivision_type2      = [[U.S. City|City]]
| subdivision_name2      = [[Hartford, Connecticut|Hartford]]
<!-- Agency overview -->
| reference1         = <ref name=budget>{{cite web|title=2014 Adopted Budget|url=http://www.hartford.gov/images/budget/13-14/public_safety.pdf|website=Hartford Connecticut|accessdate=1 June 2015}}</ref>
| established        = {{Start date| 1864}}
| annual calls       =27,267 {{small|(2016)}}
| annual budget      =$30,727,968 {{small|(2014)}}
| employees          =373 {{small|(2014)}} 
| staffing           = Career
| chief              = Reginald D. Freeman
| ALSorBLS           = <!-- [[advanced life support|ALS]] and/or [[basic life support|BLS]] -->
| iaff               = 760
<!-- Facilities and equipment -->
| reference2         = <ref name=stations/>
| divisions          = 
| battalions         = 2
| stations           = 12
| engines            = 11
| trucks             = 
| quints             = 
| tillers            =
| platforms          =5
| squads             =
| rescues            =1
| ambulances         =
| tenders            =
| hazmat             =1
| usar               =
| crash              =
| wildland           =
| bulldozers         =
| airplanes          =
| helicopters        =
| fireboats          =1
| rescue boats       = 
| light and air      =
<!-- Footer -->
| website                ={{Official website|http://www.hartford.gov/fire}}
| iaffweb                ={{url|http://www.hartfordfire.org/|IAFF website}}
}} 

The '''Hartford Fire Department''' provides [[fire protection]] and first responder [[emergency medical services]] to the city of [[Hartford, Connecticut|Hartford]], [[Connecticut]].<ref name=about>{{cite web|title=About|url=http://www.hartford.gov/fire/about-hartford-fire-department|website=Hartford Fire Department|accessdate=1 June 2015}}</ref> The Hartford Fire Department is one of four [[Insurance Services Office|ISO]] Class 1-rated fire departments in [[New England]], as well as the [[New Haven Fire Department|New Haven, Connecticut Fire Department]], the [[Milford, Connecticut|Milford, Connecticut Fire Department]] and the [[Cambridge, Massachusetts|Cambridge, Massachusetts Fire Department]].

== Historic Stations ==
{{See also2|[[Engine Company 1 Fire Station|Engine Company 1]]|[[Engine Company 6 Fire Station|Engine Company 6]]|[[Engine Company 15 Fire Station|Engine Company 15]]}}
Three of Hartford's [[fire station]]s are included on the [[National Register of Historic Places]]. These include the stations for [[Engine Company 1 Fire Station|Engine 1]] and [[Engine Company 15 Fire Station|Engine 15]] which are still in use today. Additionally, the station for [[Engine Company 6 Fire Station|Engine 6]], which was disbanded in 1984, has been repurposed as a homeless shelter.<ref name=NRHP>{{cite web|url={{NRHP url|id=89000020}}|title=NRHP nomination for Engine Company 6 Fire Station|publisher=National Park Service|accessdate=2014-12-09}}</ref>

== Stations and apparatus ==
[[File:Engine Co 1 Fire Station Hartford CT.JPG|thumb|HFD Engine 1 & Ladder 6's quarters in [[South Green, Hartford|South Green]].]]
[[File:Engine Co 2 Fire Station Hartford CT.JPG|thumb|HFD Engine 2 & District Chief 2's quarters in [[Clay-Arsenal, Hartford|Clay-Arsenal]].]]
[[File:Engine Co 15 Fire Station Hartford CT.JPG|thumb|HFD Engine 15 & Ladder 2's quarters in [[Barry Square, Hartford|Barry Square]].]]
{{As of|June 2015}}, below is a complete listing of all stations and fire companies operated by the Hartford Fire Department.<ref name=stations>{{cite web|title=Fire Houses|url=http://www.hartford.gov/fire-house-locations|website=Hartford Fire Department|accessdate=1 June 2015}}</ref> Fire Headquarters is located at the Public Safety Complex at 253 High St.
{| class=wikitable style="text-align:center;"
|-
! Engine Company
! Ladder Company
! Special Unit
! Chief
! District
! Address
! Neighborhood
|-
| [[Engine Company 1 Fire Station|Engine 1]] || Ladder 6 || || || 1 || 197 Main St. || [[South Green, Hartford|South Green]]
|-
| Engine 2 || || Haz-Mat. Trailer || District 2 || 2 || 1515 Main St. || [[Clay-Arsenal, Hartford|Clay-Arsenal]]
|-
| || || Tac. Unit 1, Fireboat 1, Fire Investigation Unit || District 1 || 1 || 275 Pearl St. || [[Downtown Hartford|Downtown]]
|-
| Engine 5 || || || || 2 || 129 Sigourney St. || [[Asylum Hill, Hartford|Asylum Hill]]
|-
| Engine 7 || Ladder 3 || Engine 6 (Reserve) || || 2 || 181 Clark St. || [[Northeast, Hartford|Northeast]]
|-
| Engine 8 || || || || 1 || 721 Park St. || [[Frog Hollow, Hartford, Connecticut|Frog Hollow]]
|-
| Engine 9 || || || || 1 || 655 New Britain Ave. || [[Southwest, Hartford|Southwest]]
|-
| Engine 10 || || || || 1 || 510 Franklin Ave. || [[South End, Hartford|South End]]
|-
| Engine 11 || Ladder 5 || Rehab. Unit, Foam Trailer || || 2 || 180 Sisson Ave. || [[West End, Hartford|West End]]
|-
| Engine 14 || Ladder 4 || Decon. Trailer || || 2 || 25 Blue Hills Ave. || [[Upper Albany, Hartford|Upper Albany]] 
|-
| [[Engine Company 15 Fire Station|Engine 15]] || Ladder 2 || || || 1 || 8 Fairfield Ave. || [[Barry Square, Hartford|Barry Square]] 
|-
| Engine 16 || || || || 2 || 635 Blue Hills Ave. || [[Blue Hills, Connecticut|Blue Hills]]
|-
| || || || Car 1(Chief of Dept.), Car 2(Asst. Chief), Car 3(Asst. Chief), Car 4(Deputy Chief), Car 5(Fire Marshal), Car 6(Captain), Car 9(Public Information Officer) || 2 || 253 High St. || [[Downtown Hartford|Downtown]]
|}
===Disbanded Fire Companies===
Throughout the history of the Hartford Fire Department, there have been several fire companies that have been disbanded due to budget cuts and reorganization.
 * Engine 3 - 181 Clark St., Northeast (Quarters of Engine 7, Ladder 3) - Disbanded to form Tactical Unit 1
 * Engine 4 - 275 Pearl St., Downtown (Quarters of Tac. Unit 1, District 1)
 * Engine 6 - 34 Huyshope Ave., Charter Oak - Disbanded to form Tactical Unit 2
 * Engine 12 - 180 Sisson Ave., West End (Quarters of Engine 11, Ladder 5)
 * Engine 13 - Never Organized
 * Ladder 1 - 275 Pearl St., Downtown (Quarters of Tac. Unit 1, District 1)
 * Tactical Unit 2 - 1515 Main St., Clay Arsenal (Quarters of Engine 2, District 2)
 * District 3 - 129 Sigourney St., Asylum Hill (Quarters of Engine 5)

==References==
{{Reflist}}

== External links ==
{{commons}}
{{wikisource}}
* [http://www.hartford.gov/fire/ Official Website]
* [http://www.hartfordfire.org/ IAFF Local 760 - Hartford Firefighter's Union]
* [http://www.hfdradio.com/ Hartford Fire Department Radio]

{{Connecticut fire departments}}

[[Category:Fire departments in Connecticut]]
[[Category:Hartford, Connecticut|Fire]]