{{good article}}
{{Use mdy dates|date=May 2013}}
{{Infobox hurricane season
| Basin=SWI
| Year=1985
| Track=1984-1985 South-West Indian Ocean cyclone season summary.jpg
| First storm formed=November 9, 1984
| Last storm dissipated=April 18, 1985
| Strongest storm name=Helisaonina
| Strongest storm pressure=941
| Strongest storm winds=95
| Average wind speed=10
| Total depressions=9
| Total storms=9
| Total hurricanes=1
| Fatalities=
| Damages=
| five seasons=[[1982–83 South-West Indian Ocean cyclone season|1982–83]], [[1983–84 South-West Indian Ocean cyclone season|1983–84]], '''1984–85''', [[1985–86 South-West Indian Ocean cyclone season|1985–86]], [[1986–87 South-West Indian Ocean cyclone season|1986–87]]
| Australian season=1984–85 Australian region cyclone season
| South Pacific season=1984–85 South Pacific cyclone season
}}
The '''1984–85 South-West Indian Ocean cyclone season''' was an average cyclone season. [[Tropical cyclone]]s in this basin are monitored by the [[Regional Specialised Meteorological Centre]] in [[Réunion]]. The first storm formed in mid-November, though it was not officially named. A few days later, the first official storm of the year (Anety) formed. In December, one storm formed. During January 1985, two tropical cyclones formed towards the end of the month. Three more systems developed in a short period of time in early to mid-February. After nearly two more months of inactivity, an unusually powerful late season storm developed (Helisaonina) in mid-April, which was the strongest storm of the year. While a number of storms during the season reached [[tropical cyclone scales|severe tropical storm status]], only one of those intensified further. Even though two tropical cyclones this year made landfall, no known damage was recorded.

==Seasonal summary==
During the season, advisories were issued by [[Météo-France]]'s (MFR) meteorological office at [[Réunion]]. At the time, the MFR area of warning responsibility was from the coast of Africa to 80°&nbsp;[[Longitude|E]], and the agency primarily used the [[Dvorak technique]] to estimate the intensities of tropical cyclones.<ref>{{cite report|author=Philippe Caroff|date=June 2011|title=Operational procedures of TC satellite analysis at RSMC La Reunion|publisher=World Meteorological Organization|accessdate=May 3, 2013|url=http://www.wmo.int/pages/prog/www/tcp/documents/RSMCLaReunionforIWSATC.pdf|format=PDF|display-authors=etal}}</ref> The [[Joint Typhoon Warning Center]] (JTWC), which is a joint [[United States Navy]]&nbsp;– [[United States Air Force]] task force that issues tropical cyclone warnings for the region,<ref>{{cite web|publisher=Joint Typhoon Warning Center|title=Joint Typhoon Warning Center Mission Statement|year=2011|archivedate=July 26, 2007 |accessdate=May 3, 2013|url=https://metocph.nmci.navy.mil/jtwc/menu/JTWC_mission.html|archiveurl=https://web.archive.org/web/20070726103400/https://metocph.nmci.navy.mil/jtwc/menu/JTWC_mission.html}}</ref> also tracked a long-lived tropical storm in November in addition to the 8 storms MFR named,<ref name=JTWC /><ref name=Anety /><ref name=Bobalahy /><ref name=Celestina /><ref name=Ditra /><ref name=Esitera /><ref name=Gerimena /><ref name=Feliska /><ref name=Helisaonina /> which is comparable to the average of nine named storms per year.<ref>{{cite web|title=Cyclone Season 2001–2002|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/archives/publications/saisons_cycloniques/index20012002.html|work=RSMC La Reunion|publisher=Meteo-France|accessdate=June 4, 2013}}</ref> Following the season, the boundary for the [[tropical cyclone basins|basin]] was extended to 90° E.<ref name="SWIO bound">{{cite journal|url=http://www.bom.gov.au/amm/docs/1986/kingston.pdf|page=103|journal=Australian Meteorology Magazine|volume=34|date=August 1986|author=G. Kingston|title=The Australian Tropical Cyclone Season of 1985-86|publisher=[[Bureau of Meteorology (Australia)|Bureau of Meteorology]]|accessdate=April 29, 2013}}</ref>

==Systems==

===Tropical Storm 01S===
{{Infobox hurricane small
|Basin=SWI
|Image=01S Nov 11 1984 2322Z.png
|Track=01S 1984 track.png
|Formed=November 9
|Dissipated=November 17
|1-min winds=45
|Pressure=987
}}
According to the JTWC, a tropical depression formed on November 9 quite far from land. However, the system was never monitored by MFR. Tracking southwest throughout its lifetime, the JTWC upgraded the system into a tropical storm on November 11. Twelve hours later, the storm attained peak intensity of {{convert|50|mph|km/h|abbr=on|disp=5}}. The storm gradually weakened, and at 0000[[UTC]] on November 14, it fell to a depression. On November 17, 01S was no more.<ref name="JTWC">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 HSK0185  (1984314S08081)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1984314S08081}}</ref>
{{clear}}

===Moderate Tropical Storm Anety===
{{Infobox hurricane small
|Basin=SWI
|Image=Anety Nov 22 1984 1318Z.png
|Track=Anety 1984 track.png
|Formed=November 20
|Dissipated=November 23
|10-min winds=36
|Pressure=1003
}}
Early on November 20, the JTWC reported that a tropical depression had developed. Shortly thereafter, MFR reported that a tropical disturbance had formed. The low moved west-southwest while gradually deepening. Late on November 20, MFR upgraded the system into a [[tropical cyclone scales|moderate tropical storm]]. The storm failed to intensify further as it had moved onshore northern [[Madagascar]]. By November 21, MFR estimated that the storm weakened back into a disturbance. After emerging into the [[Mozambique Channel]], Anety reportedly re-intensified back to moderate tropical storm status. While making its closest approach to the African mainland, the storm resumed weakening while turning back southeast. On November 23, both agencies stopped monitoring the system.<ref name="Anety">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 Anety (1984325S15052)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1984325S15052}}</ref><ref name="MFR1">{{cite report|publisher=Météo-France |title=Donnes de Anety |url=http://www.meteo.fr/temps/domtom/La_Reunion/base_cyclone/nom_annee/ANETY_1984.html |accessdate=May 3, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20071214225731/http://www.meteo.fr/temps/domtom/La_Reunion/base_cyclone/nom_annee/ANETY_1984.html |archivedate=December 14, 2007 }}</ref>
{{clear}}

===Moderate Tropical Storm Bobalahy===
{{Infobox hurricane small
|Basin=SWI
|Image=Bobalahy dec 3 1984 1100Z.jpg
|Track=Bobalahy 1984 track.png
|Formed=December 2
|Dissipated=December 8
|10-min winds=44
|1-min winds=55
|Pressure=984
}}
During the morning hours of December 2, a tropical cyclone developed in extreme western portion of the [[tropical cyclone basins#Australia|Australian basin]]. After crossing the 80°E boundary that at that time separated the two basins the following day, MFR classified the system as a tropical depression. Early on December 4, the agency upgraded the system into a moderate tropical storm. Moving steadily southwest, it gradually intensified, only to turn west on December 5. That day, MFR reported that Moderate Tropical Storm Bobalahy had attained peak intensity of {{convert|45|mph|km/h|abbr=on|disp=5}}. Around that time, the JTWC estimated that Bobalahy attained peak intensity of {{convert|65|mph|km/h|abbr=on|disp=5}}. After maintaining peak intensity for a day or so, Bobalahy resumed a southwesterly path far from land while slowly weakening. On December 6, MFR downgraded the system into a tropical depression; the JTWC followed suit the next day. Now moving south-southwest, the JTWC stopped issuing advisories on the system as it had become [[extratropical cyclone|extratropical]]. However, MFR continued to monitor the remnants of the system for four more days.<ref name="Bobalahy">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 BOBALAHY  (1984337S11082)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1984337S11082}}</ref><ref name="MFR2">{{cite report|publisher=Météo-France |title=Donnes de Bobalahy |url=http://www.meteo.fr/temps/domtom/La_Reunion/base_cyclone/nom_annee/BOBALAHY_1984.html |accessdate=May 3, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20071214225731/http://www.meteo.fr/temps/domtom/La_Reunion/base_cyclone/nom_annee/BOBALAHY_1984.html |archivedate=December 14, 2007 }}</ref>
{{clear}}

===Severe Tropical Storm Celestina===
{{Infobox hurricane small
|Basin=SWI
|Image=Celestina Jan 13 1985 1235Z.png
|Track=Celestina 1985 track.png
|Formed=January 12
|Dissipated=January 20
|10-min winds=52
|1-min winds=65
|Pressure=976
}}
Well away from land, the JTWC reported that a tropical disturbance developed on January 1. Initially, the storm remained weak, but later on January 11, the JTWC noted that the system had intensified into a tropical storm. Continuing to intensify, the storm moved towards the southwest. On January 12, MFR first classified the system; within six hours, it was declared a moderate tropical storm. As Celestina made a turn towards the south-southwest, MFR estimated that Celestina attained peak intensity as a severe tropical storm at 0600 [[UTC]] on January 13. According to the JTWC, the storm briefly developed [[hurricane]]-force winds; however, Celestina began to weakened thereafter. Moving south, Celestina was situated roughly {{convert|350|mi|km|abbr=on|disp=5}} east of Madagascar. Furthermore, on January 15, Celestina briefly re-intensified while undergoing a counterclockwise loop. By January 18, Celestina resumed a weakening trend; by that night, MFR downgraded the system into a tropical depression as it was now moving south-southwest. The storm was re-upgraded into a moderate tropical storm three days later, on January 21. However, this trend was short lived and that evening, the JTWC stopped monitoring the system. MFR followed suit at 0600 UTC on January 23.<ref name="Celestina">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 CELESTINA  (1985009S11072)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985009S11072}}</ref> During its lifetime, Severe Tropical Storm Celestina brought rains to Reunion, peaking at {{convert|600|mm|in|abbr=on|disp=5}} in Trois-Bassins.<ref name=FLS1>{{cite report|title=Les systèmes dépressionnaires baptisés les plus marquants sur l'île Reunion|url=http://www.firinga.com/influences?influ=reunion&fav=AND%20favori=%27oui%27|publisher=Fringa le site|accessdate=May 5, 2013|language=fr|trans-title=The pressure systems baptized the most significant island of Reunion.}}</ref>
{{clear}}

===Severe Tropical Storm Ditra===
{{Infobox hurricane small
|Basin=SWI
|Image=Ditra Jan 29 1985 0952Z.png
|Track=Ditra 1985 track.png
|Formed=January 27
|Dissipated=January 31
|10-min winds=62
|1-min winds=70
|Pressure=966
}}
Severe Tropical Storm Ditra originated from a tropical depression that the JTWC first warned on January 26. At first, the storm moved southwest, but once it developed [[gale]]-force winds, the system turned south. At 1800 UTC on January 27, MFR started monitoring the low; early the next morning, MFR upgraded the system into a moderate tropical storm. Twelve hours later, the JTWC estimated that Ditra had intensified into winds equal to a Category 1 on [[Saffir-Simpson hurricane wind scale]] (SSHWS). During the early morning hours of January 29, Ditra intensified into a severe tropical storm as the storm briefly turned west-southwest. Shortly thereafter, the JTWC announced that Ditra had attained its peak intensity of {{convert|80|mph|km/h|abbr=on|disp=5}}. While the JTWC suggests that the storm gradually weakened during this time, MFR suggests that Ditra continued to intensify; they estimated that Ditra peaked in intensity on 0600 UTC January 30. Around this time, Ditra made its closet approach to Reunion, passing about {{convert|150|mi|km|abbr=on|disp=5}} south-southeast of the island. After maintaining this intensity for several hours, Ditra rapidly weakened as it accelerated to the southeast. It steadily weakened and late on January 31, the JTWC reportedly downgraded the system into a depression. Both agencies stopped monitoring Tropical Depression Ditra the following day.<ref name="Ditra">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 DITRA  (1985025S09073)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985025S09073}}</ref> On January 29, Dirta passed just east of [[Rodrigues]], bringing heavy rains.<ref name=FLS1/>
{{clear}}

===Moderate Tropical Storm Esitera===
{{Infobox hurricane small
|Basin=SWI
|Image=Esitera Feb 07 1985 1226Z.png
|Track=Esitera 1985 track.png
|Formed=February 3
|Dissipated=February 11
|10-min winds=44
|1-min winds=50
|Pressure=984
}}
On February 9, MFR first classified the system as a tropical depression about {{convert|400|mi|km|abbr=on|disp=5}} east of the northern tip of Madagascar. Never warned on by the JTWC, the storm moved southwest. After briefly weakening into a tropical disturbance, the storm suddenly re-intensified into a moderate tropical storm as Esitera re-curved to the southwest. On February 11, MFR stopped keeping an eye on the system.<ref name="Esitera">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 ESITERA  (1985041S17053)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985041S17053}}</ref>
{{clear}}

===Moderate Tropical Storm Gerimena===
{{Infobox hurricane small
|Basin=SWI
|Image=Gerimena Feb 21 1985 1050Z.png
|Track=Gerimena 1985 track.png
|Formed=February 11
|Dissipated=February 22
|10-min winds=44
|1-min winds=65
|Pressure=984
}}
On February 11, MFR reported that a moderate tropical storm formed over {{Convert|700|mi|km|abbr=on|disp=5}} east of Reunion. The storm erratically drifted south for the two days when the JTWC declared the system a tropical depression. Subsequently, the system turned north-northwest and slowed. Data from the MF suggests that Moderate Tropical Storm Gerimena rapidly degenerated tropical disturbance before slowly re-intensifying; however, the JTWC suggests it gradually intensified. Before turning west, the JTWC upgraded the system into a tropical storm midday on February 14. According to the JTWC, Gerimena reached a secondary peak with winds of {{convert|50|mph|km/h|abbr=on|disp=5}} before weakening a little. Meanwhile, MFR upgraded the system back to moderate tropical storm status. Slowly intensifying, Gerimena turned south. Although the JTWC suggest that Germaine briefly weakened on January 18 while turning east, data from MFR shows that Germiena did not weaken until 1800 UTC February 19. On January 20, however, both agencies agree that Gerimena started to re-intensify. The next day, the JTWC reported that the storm intensified into a hurricane even though MFR suggests that the system was just a disturbance by that time. Not long after becoming a hurricane, the JTWC remarked that Cyclone Gerimena had attained peak intensity.<ref name="Gerimena">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 GERIMENA  (1985042S18060)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985042S18060}}</ref>

Shortly thereafter, data from the JTWC suggests that Gerimena weakened as it turned southeast. Then, it turned east-southeast. On February 24, Gerimena briefly level off in intensity while turning back to south. Furthermore, the JTWC stopped keeping an eye on Gerimena at 0000 UTC on February 26 as the storm re-curved east. However, MFR continued to track Gerimena until March 4 as it fluctuated in intensity.<ref name=Gerimena />
{{clear}}

===Severe Tropical Storm Feliska===
{{Infobox hurricane small
|Basin=SWI
|Image=Feliska Feb 16 1985 1146Z.png
|Track=Feliska 1985 track.png
|Formed=February 12
|Dissipated=February 18
|10-min winds=52
|1-min winds=50
|Pressure=976
}}
On February 12, MFR first designated what would later become Feliska while it was centered north of the Mozambique Channel. The next day, MFR downgraded Feliska into a tropical depression. Hours later, the JTWC first monitored the system. Drifting east, the depression gradually intensified. MFR reported that the system regained moderate tropical storm intensity at 1800 UTC that day. Early on February 13, the JTWC upgraded Feliska into a tropical storm as it turned north. While slowing gaining strength, Feliska turned east. By 0000 UTC February 16, MFR declared that Feliska attained peak intensity. Moreover, the JTWC suggested that Feliska had peaked in intensity, with winds of {{convert|60|mph|km/h|abbr=on|disp=5}}. Thereafter, Feliska turned south and start a slow weakening trend. On February 17, Feliska briefly weakened into a tropical depression. That night Felsika was re-upgraded into a moderate tropical storm as it made [[landfall (meteorology)|landfall]] along northeastern Madagascar. At that time of landfall, the JTWC estimated that Feliksa was still a tropical storm. After moving inland, MFR stopped monitoring the system, though the JTWC kept tracking Feliska for another 24 hours as it headed southeast.<ref name="Feliska">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 FELIKSA:FELISKA  (1985043S16042)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985043S16042}}</ref>
{{clear}}

===Tropical Cyclone Helisaonina===
{{Infobox hurricane small
|Basin=SWI
|Image=Helisaonina Apr 13 1985 1010Z.png
|Track=Helisaonina 1985 track.png
|Formed=April 10
|Dissipated=April 18
|10-min winds=82
|1-min winds=110
|Pressure=941
}}
On April 10, MFR reportedly classified a low far from any land masses. Later that day, the JTWC upgraded Helisaonina into a tropical depression after turning from west to southwest. On April 11, MFR upgraded the system into a Severe Tropical Storm. At 0600 UTC the next day, the JTWC upgraded the system into a hurricane while MFR upgraded the system into [[tropical cyclone scales|tropical cyclone intensity]]. That evening, the JTWC announced that it had developed winds equivalent to Category 2 intensity. Two days after becoming a severe tropical storm, on April 13, the JTWC upgraded Helisonina to the equivalent to a Category 3 hurricane. While undergoing a counterclockwise loop, the JTWC reported that Cyclone Helisaonia had peaked in intensity with {{convert|120|mph|km/h|abbr=on|disp=5}} winds. At that time, 000 UTC April 14, MFR estimated it attained peak wind speed, with winds of {{convert|90|mph|km/h|abbr=on|disp=5}}.<ref name=Helisaonina />

After attaining peak intensity, the storm weakened rapidly as it began to move west-northwest. Later that morning, MFR downgraded the system into a severe tropical storm. By April 15, MFR downgraded the system into a tropical depression. That very day, the JTWC reported that winds of Helisaonia had fallen below hurricane-force. During the morning hours of April 17, the JTWC downgraded Helisaonina into a depression. Shortly thereafter, MFR stopped monitoring the system. After re-curving just east of Madagascar, Helisanonina dissipated according to the JTWC on April 18.<ref name="Helisaonina">{{cite report|author=Knapp, K. R.|author2=M. C. Kruk|author3=D. H. Levinson|author4=H. J. Diamond|author5=C. J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1985 HELISAONIN:HELISAONINA  (1985097S09076)|publisher=Bulletin of the American Meteorological Society|accessdate=May 3, 2013|url=http://storm5.atms.unca.edu/browse-ibtracs/browseIbtracs.php?name=v03r02-1985097S09076}}</ref> While the storm was weakening, it passed close to Rodrigues without causing any known impact.<ref name=FLS1 />
{{clear}}

==See also==
*Atlantic hurricane seasons: [[1984 Atlantic hurricane season|1984]], [[1985 Atlantic hurricane season|1985]]
*Eastern Pacific hurricane seasons: [[1984 Pacific hurricane season|1984]], [[1985 Pacific hurricane season|1985]]
*Western Pacific typhoon seasons: [[1984 Pacific typhoon season|1984]], [[1985 Pacific typhoon season|1985]]
*North Indian Ocean cyclone seasons: [[1984 North Indian Ocean cyclone season|1984]], [[1985 North Indian Ocean cyclone season|1985]]

==References==
{{reflist|2}}

{{TC Decades|Year=1980|basin=South-West Indian Ocean|type=cyclone|shem=yes}}

{{DEFAULTSORT:1984-85 South-West Indian Ocean cyclone season}}
[[Category:Southern Hemisphere tropical cyclone seasons]]
[[Category:1984–85 Southern Hemisphere tropical cyclone season]]