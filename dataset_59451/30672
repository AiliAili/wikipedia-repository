{{featured article}}
{{Automatic Taxobox
| image = Deinosuchus hatcheri - Natural History Museum of Utah - DSC07251.JPG
| image_caption = Reconstructed skeleton at the Natural History Museum of Utah
| fossil_range = [[Late Cretaceous]], {{fossil range|82|73}}
| taxon = Deinosuchus
| authority = [[William Jacob Holland|Holland]], 1909
| type_species = '''''Polyptychodon rugosus'''''
| type_species_authority = [[Ebenezer Emmons|Emmons]], 1858
| subdivision_ranks = [[Species]]
| subdivision = 
*{{extinct}}''D. rugosus'' <br><small>([[Ebenezer Emmons|Emmons]], 1858) <br>[originally ''Polyptychodon'']</small>
*{{extinct}}''D. riograndensis''<br><small>([[Edwin Harris Colbert|Colbert]] & Bird, 1954) <br>[originally ''Phobosuchus'']</small>
| synonyms =
*''[[Polydectes]]'' <small>[[Edward Drinker Cope|Cope]], 1869</small>
*''Phobosuchus'' <small>[[Franz Nopcsa von Felső-Szilvás|Nopcsa]], 1924</small>
}}

'''''Deinosuchus''''' ({{IPAc-en|ˌ|d|aɪ|n|ə|ˈ|s|juː|k|ə|s}} {{respell|DY|nə|SEW|kəs}}) is an [[extinct]] [[genus]] related to the [[alligator]] that lived 82 to 73&nbsp;million years ago ([[annum|Ma]]), during the late [[Cretaceous]] [[Geologic time scale#Terminology|period]]. The name translates as "terrible crocodile" and is derived from the [[Ancient Greek|Greek]] ''deinos'' (δεινός), "terrible", and ''soukhos'' (σοῦχος), "crocodile". The first remains were discovered in [[North Carolina]] (United States) in the 1850s; the [[genus]] was named and described in 1909. Additional fragments were discovered in the 1940s and were later incorporated into an influential, though inaccurate, skull reconstruction at the [[American Museum of Natural History]]. Knowledge of ''Deinosuchus'' remains incomplete, but better [[Skull|cranial]] material found in recent years has expanded scientific understanding of this massive predator.

Although ''Deinosuchus'' was far larger than any modern crocodile or alligator, with the largest adults measuring {{Convert|10.6|m|ft|abbr=on}} in total length, its overall appearance was fairly similar to its smaller relatives. It had large, robust teeth built for crushing, and its back was covered with thick hemispherical [[osteoderm]]s. One study indicated ''Deinosuchus'' may have lived for up to 50&nbsp;years, growing at a rate similar to that of modern crocodilians, but maintaining this growth over a much longer time.

''Deinosuchus'' fossils have been found in 10 US states, including Texas, Montana, and many along the [[East Coast of the United States|East Coast]]. Fossils have also been found in northern Mexico. It lived on both sides of the [[Western Interior Seaway]], and was an opportunistic [[apex predator]] in the coastal regions of eastern North America. ''Deinosuchus'' reached its largest size in its western habitat, but the eastern populations were far more abundant. Opinion remains divided as to whether these two populations represent separate [[species]]. ''Deinosuchus'' was probably capable of killing and eating large [[dinosaur]]s. It may have also fed upon [[sea turtle]]s, fish, and other aquatic and terrestrial prey.

==Description==

===Morphology===
[[File:Deinosuchus hatcheri.JPG|upright|thumb|left|Life restoration of ''D. rugosus'']] 
Despite its large size, the overall appearance of ''Deinosuchus'' was not considerably different from that of modern crocodilians.<ref name="schwimmer-lifetimes">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=1–16
|chapter=The Life and Times of a Giant Crocodylian
|year=2002
|isbn=0-253-34087-X
}}</ref> ''Deinosuchus'' had an [[alligator]]-like, broad snout, with a slightly bulbous tip.<ref name="schwimmer-lifetimes"/> Each [[premaxilla]] contained four teeth, with the pair nearest to the tip of the snout being significantly smaller than the other two.<ref name="schwimmer-species">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=107–135
|chapter=How Many ''Deinosuchus'' Species Existed?
|year=2002
|isbn=0-253-34087-X
}}</ref> Each [[maxilla]] (the main tooth-bearing bone in the upper jaw) contained 21 or 22&nbsp;teeth.<ref name="schwimmer-size">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=42–63
|chapter=The Size of ''Deinosuchus''
|year=2002
|isbn=0-253-34087-X
}}</ref> The tooth count for each [[dentary]] (tooth-bearing bone in the lower jaw) was at least 22.<ref name="schwimmer-species"/> All the teeth were very thick and robust; those close to the rear of the [[jaw]]s were short, rounded, and blunt.<ref name="schwimmer-prey">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=167–192
|chapter=The Prey of Giants
|year=2002
|isbn=0-253-34087-X
}}</ref> They appear to have been adapted for crushing, rather than piercing.<ref name="schwimmer-prey"/> When the mouth was closed, only the fourth tooth of the lower jaw would have been visible.<ref name="schwimmer-species"/>

Modern saltwater crocodiles ([[Saltwater crocodile|''Crocodylus porosus'']]) have the strongest recorded bite of any living animal, with a maximum force of {{convert|16,414|N|lbf|abbr=on}}.<ref name=erickson2012>{{cite journal|last=Erickson|first=G.M., Gignac, P.M., Steppan, S.J., Lappin, A.K., Vliet, K.A., Brueggen, J.D., Inouye, B.D., Kledzik, D., Webb, G.J.W.|year=2012|title=Insights into the Ecology and Evolutionary Success of Crocodilians Revealed through Bite-Force and Tooth-Pressure Experimentation|journal=PLoS ONE|volume=7|issue=3|pages=e31781|doi=10.1371/journal.pone.0031781|pmid=22431965|pmc=3303775|bibcode=2012PLoSO...731781E}}</ref> The bite force of ''Deinosuchus'' has been estimated to be {{convert|18000|N|lbf|abbr=on}}<ref name="schwimmer-lifetimes"/> to {{convert|100,000|N|lbf|abbr=on}}.<ref name=erickson2012 /> It has been argued that even the largest and strongest [[theropoda|theropod]] dinosaurs, such as ''[[Tyrannosaurus]]'', probably had bite forces inferior to that of ''Deinosuchus''.<ref name="erickson-et-al">{{cite journal
|title=The ontogeny of bite-force performance in American alligator (''Alligator mississippiensis'')
|author1=Erickson, Gregory M. |author2=Lappin, A. Kristopher |author3=Vliet, Kent A. |year=2003
|journal=[[Journal of Zoology]]
|volume=260|issue=3|pages=317–327
|doi=10.1017/S0952836903003819
|url=http://www.csupomona.edu/~aklappin/Alligator%20Bite%20Force%20Ontogeny.pdf
|format=pdf
|accessdate=2009-11-02
}}</ref>

''Deinosuchus'' had a secondary bony palate, which would have permitted it to breathe through its nostrils while the rest of the head remained submerged underwater.<ref name="cloudsley-thompson"/> The [[vertebra]]e were articulated in a procoelous manner, meaning they had a concave hollow on the front end and a convex bulge on the rear; these would have fit together to produce a [[ball and socket joint]].<ref name="colbert-bird">{{cite journal
|url=http://digitallibrary.amnh.org/dspace/bitstream/2246/2425/1/N1688.pdf
|title=A gigantic crocodile from the Upper Cretaceous beds of Texas
|author1=Colbert, Edwin H |author2=Bird, Roland T. |journal=American Museum Novitates
|volume=1688
|pages=1–22
|year=1954
|publisher=[[American Museum of Natural History]]
|format=pdf
|accessdate=2009-02-22
}}</ref><ref name="foster">{{cite book
|title=Jurassic West: The Dinosaurs of the Morrison Formation and Their World
|last=Foster|first=John
|publisher=[[Indiana University Press]]
|year=2007
|isbn=978-0-253-34870-8
|page=150
}}</ref> The secondary palate and procoelous vertebrae are advanced features also found in modern [[eusuchia]]n crocodilians.<ref name="cloudsley-thompson">{{cite book
|title=Ecology and Behaviour of Mesozoic Reptiles
|last=Cloudsley-Thompson|first=J.L.
|publisher=[[Springer Science+Business Media|Springer]]
|pages=40–41
|year=2005
|isbn=3-540-22421-1
}}</ref><ref name="rolleston-jackson">{{cite book
|title=Forms of Animal Life
|author1=Rolleston, George |author2=Jackson, William Hatchett |publisher=Oxford at the Clarendon Press
|year=1870
|page=392
}}</ref>

The [[osteoderm]]s (scutes) covering the back of ''Deinosuchus'' were unusually large, heavy, and deeply pitted; some were of a roughly hemispherical shape.<ref name="holland">{{cite journal
|title=''Deinosuchus hatcheri'', a new genus and species of crocodile from the Judith River beds of Montana
|last=Holland|first=W.J.
|journal=Annals of the Carnegie Museum
|volume=6
|year=1909
|pages=281–294
}}</ref><ref name="schwimmer-early">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=17–41
|chapter=The Early Paleontology of''Deinosuchus''
|year=2002
|isbn=0-253-34087-X
}}</ref> Deep pits and grooves on these osteoderms served as attachment points for connective tissue.<ref name="schwimmer-early"/> Together, the osteoderms and connective tissue would have served as load-bearing reinforcement to support the massive body of ''Deinosuchus'' out of water.<ref name="schwimmer-size"/><ref name="schwimmer-early"/> These deeply pitted osteoderms have been used to suggest that, despite its bulk, ''Deinosuchus'' could probably have walked on land much like modern-day crocodiles.<ref name="schwimmer-size"/><ref name="cloudsley-thompson"/>

===Size===
[[File:Large crocodyliformes.svg|thumb|The maximum size reached by ''Deinosuchus'' (yellow) is currently estimated at {{convert|10.6|m|ft|abbr=on}}. In contrast, the largest modern [[saltwater crocodile]]s rarely reach more than {{convert|6|m|ft|abbr=on}} in length.]]
The large size of ''Deinosuchus'' has generally been recognized despite the fragmentary nature of the fossils assigned to it. However, estimates of how large it really was have varied considerably over the years. The original estimate from 1954 for the type specimen of the then-named ''"Phobosuchus riograndensis"'' were based on a skull of {{convert|1.5|m|ft|abbr=on}} and a lower jaw of {{convert|1.8|m|ft|abbr=on}} long, reconstructed with similar proportions to the [[Cuban crocodile]] giving  a total estimated length of {{Convert|15|m|ft|abbr=on}}.<ref name="colbert-bird"/> However, this reconstruction is currently considered to be inaccurate.<ref name="schwimmer-size"/> Using more complete remains, it was estimated in 1999 that the size attained by specimens of ''Deinosuchus'' varied from {{Convert|8|to|10|m|ft|abbr=on}} with weights from {{convert|2.5|to|5|MT|ST|abbr=on}}.<ref name="erickson-brochu">{{cite journal
|title=How the 'terror crocodile' grew so big|author1=Erickson, Gregory M. |author2=Brochu, Christopher A. |journal=[[Nature (journal)|Nature]]|volume=398|date=March 1999|publisher=[[Nature Publishing Group]]|pages=205–206|doi=10.1038/18343|issue=6724|bibcode=1999Natur.398..205E }}</ref> This was later corroborated when it was noted that most known specimens of ''D. rugosus'' usually had skulls of about {{Convert|1|m|ft|sigfig=2|abbr=on}} with estimated total lengths of {{Convert|8|m|ft|abbr=on}} and weights of {{convert|2.3|MT|ST|abbr=on}}. A reasonably well-preserved skull specimen discovered in [[Texas]] indicated the animal's head measured about {{Convert|1.31|m|ft|abbr=on}}, and its body length was estimated at {{Convert|9.8|m|ft|abbr=on}}. However, the largest fragmentary remains of ''D. riograndensis'' were 1.5 times the size of those of the average ''D. rugosus'' and it was determined that the largest individuals of this species may have been up to {{Convert|12|m|ft|abbr=on}} in length and perhaps weighed as much as {{convert|8.5|MT|ST|abbr=on}}.<ref name="schwimmer-size"/>

A particularly large mandibular fragment from a ''D. riograndensis'' specimen was estimated to have come from an individual with a skull length of about {{convert|147.5|cm|ft|abbr=on}}. This length was used in conjunction with a regression equation relating skull length to total length in the [[American alligator]] to estimate a total length of {{convert|10.6|m|ft}} for this particular specimen.<ref name=Farlowetal2005>{{cite journal|title=Femoral dimensions and body size of Alligator mississippiensis: estimating the size of extinct mesoeucrocodylians|journal=Journal of Vertebrate Paleontology|year=2005|volume=25|issue=2|url=http://www.tandfonline.com/doi/abs/10.1671/0272-4634%282005%29025%5B0354%3AFDABSO%5D2.0.CO%3B2#.UpPCEDN3vIU|author=Farlow|doi=10.1671/0272-4634(2005)025[0354:FDABSO]2.0.CO;2|display-authors=etal|pages=354–369}}</ref> This is only slightly lower than previous estimates for the species. ''Deinosuchus'' has often been described as the largest [[Crocodyliformes|crocodyliform]] of all time. However other crocodyliformes such as ''[[Purussaurus]]'', ''[[Rhamphosuchus]]'', and ''[[Sarcosuchus]]'' may have equaled or exceeded it in size.<ref name="brochu-memoirs">{{cite journal
|doi=10.2307/3889340
|title=Phylogenetics, Taxonomy, and Historical Biogeography of Alligatoroidea
|last=Brochu
|first=Christopher A.
|journal=Society of Vertebrate Paleontology Memoir|volume=6
|date= June 14, 1999 |pages=9–100
|jstor=3889340
}}</ref>

==Paleobiology==

===Habitat===
[[Image:Deinosuchus.jpg|thumb|right|A ''Deinosuchus'' jaw fragment, exhibited at the [[North Carolina Museum of Natural Sciences]]: Fossils of this large alligatoroid have been discovered in 10 US states and northern Mexico.]]
''Deinosuchus'' was present on both sides of the [[Western Interior Seaway]].<ref name="schwimmer-localities">{{cite book
|title=King of the Crocodylians: The Paleobiology of Deinosuchus
|last=Schwimmer|first=David R.
|publisher=[[Indiana University Press]]
|pages=81–106
|chapter=''Deinosuchus'' Localities and Their Ancient Environments
|year=2002
|isbn=0-253-34087-X
}}</ref> Specimens have been found in 10 U.S. states: Utah, Montana, Wyoming, New Mexico, New Jersey, Georgia, Alabama, Mississippi, Texas, and North Carolina.<ref name="titus-et-al">{{cite journal
|title=First report of the hyper-giant Cretaceous crocodylian ''Deinosuchus'' from Utah
|author1=Titus, Alan L. |author2=Knell, Michael J. |author3=Wiersma, Jelle P. |author4=Getty, Mike A. |journal=Geological Society of America Abstracts with Programs|volume=40|issue=1
|page=58
|year=2008
|url=http://gsa.confex.com/gsa/2008CD/finalprogram/abstract_133900.htm
}}</ref> A ''Deinosuchus'' osteoderm from the [[San Carlos Formation]] was also reported in 2006, so the giant crocodilian's range may have included parts of northern [[Mexico]].<ref name="westgate-et-al">{{cite journal
|title=First occurrences of ''Deinosuchus'' in Mexico
|author1=Westgate, James |author2=Brown, R. |author3=Pittman, Jeffrey |author4=Cope, Dana |author5=Calb, Jon |journal=[[Journal of Vertebrate Paleontology]]
|volume=26|issue=Supplement to 3
|page=138A
|year=2006
|doi=10.1080/02724634.2006.10010069
}}</ref> ''Deinosuchus'' fossils are most abundant in the [[Gulf Coastal Plain]] region of Georgia, near the Alabama border.<ref name="schwimmer-localities"/> All known specimens of ''Deinosuchus'' were found in rocks dated to the [[Campanian]] [[Stage (stratigraphy)|stage]] of the [[Late Cretaceous]] [[Geologic time scale#Terminology|period]]. The oldest examples of this genus lived approximately 82 Ma, and the youngest lived around 73&nbsp;Ma.<ref>{{Cite book|title=King of the Crocodylians: The Paleobiology of Deinosuchus|last=Schwimmer|first=David R.|publisher=Indiana University Press|year=2002|isbn=0-253-34087-X|location=|pages=77-78|quote=|via=}}</ref>

The distribution of ''Deinosuchus'' specimens indicates these giant crocodilians may have preferred [[Estuary|estuarine]] environments.<ref name="schwimmer-localities"/> In the [[Aguja Formation]] of Texas, where some of the largest specimens of ''Deinosuchus'' have been found, these massive predators probably inhabited [[Brackish water|brackish-water]] [[bay]]s.<ref name="anglen-lehman">{{cite journal
|title=Habitat of the giant crocodilian ''Deinosuchus'', Aguja Formation (Upper Cretaceous), Big Bend National Park, Texas
|author1=Anglen, John J. |author2=Lehman, Thomas M. |journal=[[Journal of Vertebrate Paleontology]]
|volume=20|issue=Supplement to 3
|page=26A
|year=2000
|doi=10.1080/02724634.2000.10010765
}}</ref> Although some specimens have also been found in [[ocean|marine]] deposits, it is not clear whether ''Deinosuchus'' ventured out into the ocean (like modern-day [[Saltwater Crocodile|saltwater crocodiles]]); these remains might have been displaced after the animals died.<ref name="schwimmer-localities"/> ''Deinosuchus'' has been described as a "conspicuous" component of a purportedly distinct biome occupying the southern half of Late Cretaceous North America.<ref name="lehman">{{cite book |last=Lehman |first=T. M. |year=2001 |chapter=Late Cretaceous dinosaur provinciality |title=Mesozoic Vertebrate Life |editor1-last=Tanke |editor1-first=D. H. |editor2-last=Carpenter |editor2-first=K. |location= |publisher=Indiana University Press |pages=310–328 |isbn=0-253-33907-3 }}</ref>

===Diet===
[[File:Kritosaurus BW.jpg|thumb|''Deinosuchus'' may have preyed upon large [[ornithopod]]s. ''[[Kritosaurus]]'' lived alongside the giant crocodilian in the [[Aguja Formation]] ecosystem.<ref name="sankey">{{cite journal
 |title=Late Campanian Southern Dinosaurs, Aguja Formation, Big Bend, Texas 
 |last=Sankey 
 |first=Julia T. 
 |journal=[[Journal of Paleontology]] 
 |volume=75 
 |issue=1 
 |year=2001 
 |pages=208–215 
 |doi=10.1666/0022-3360(2001)075<0208:LCSDAF>2.0.CO;2 
 |url=http://geology.csustan.edu/julia/pdf/sankey-2001.pdf 
 |format=pdf 
 |accessdate=2009-11-02 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110719181856/http://geology.csustan.edu/julia/pdf/sankey-2001.pdf 
 |archivedate=2011-07-19 
 |df= 
}}</ref>]]
In 1954, Edwin H. Colbert and Roland T. Bird speculated that ''Deinosuchus'' "may very well have hunted and devoured some of the dinosaurs with which it was contemporaneous".<ref name="colbert-bird"/> Colbert restated this hypothesis more confidently in 1961: "Certainly this crocodile must have been a predator of dinosaurs; otherwise why would it have been so overwhelmingly gigantic? It hunted in the water where the giant theropods could not go."<ref name="colbert-dinoworld">{{cite book
|title=Dinosaurs: Their Discovery and Their World
|last=Colbert|first=Edwin H.|authorlink=Edwin Harris Colbert
|publisher=[[E. P. Dutton]]
|year=1961
|page=243
}}</ref><ref name="debus">{{cite book
|title=Dinosaur Memories
|last=Debus|first=Allen
|publisher=Authors Choice Press
|year=2002
|isbn=0-595-22988-3
|page=265
}}</ref> David R. Schwimmer proposed in 2002 that several [[hadrosaurid]] tail vertebrae found near [[Big Bend National Park]] show evidence of ''Deinosuchus'' tooth marks, strengthening the hypothesis that ''Deinosuchus'' fed on dinosaurs in at least some instances.<ref name="schwimmer-prey"/> In 2003, Christopher A. Brochu agreed that ''Deinosuchus'' "probably dined on [[ornithopod]]s from time to time."<ref name="brochu-review">{{cite journal|title=Review of ''King of the Crocodylians: The Paleobiology of Deinosuchus'' |last=Brochu|first=Christopher A.|journal=[[Palaios]]|volume=18|issue=1|pages=79–82|year=2003|doi=10.1669/0883-1351(2003)018<0080:BR>2.0.CO;2}}</ref> ''Deinosuchus'' is generally thought to have employed hunting tactics similar to those of modern crocodilians, ambushing dinosaurs and other terrestrial animals at the water's edge and then submerging them until they drowned.<ref name="lifehistory">{{cite book
|title=History of Life
|edition=3rd
|last=Cowen|first=Richard
|publisher=[[Blackwell Publishing]]
|year=2000
|isbn=0-632-04501-9
|page=263
}}</ref> A 2014 study suggested that it would have been able to perform a "[[Crocodilia#Feeding|death roll]]", like modern crocodiles.<ref>{{Cite journal | doi = 10.1080/08912963.2014.893300| title = The 'death roll' of giant fossil crocodyliforms (Crocodylomorpha: Neosuchia): Allometric and skull strength analysis| journal = Historical Biology| volume = 27| issue = 5| pages = 1| date = 2014-04-16| last1 = Blanco | first1 = R. E. | last2 = Jones | first2 = W. W. | last3 = Villamil | first3 = J. N. }}</ref>

Schwimmer and G. Dent Williams proposed in 1996 that ''Deinosuchus'' may have preyed on [[Sea turtle|marine turtles]].<ref name="schwimmer-williams">{{cite journal
|title=New specimens of ''Deinosuchus rugosus'', and further evidence of chelonivory by Late Cretaceous eusuchian crocodiles
|author1=Schwimmer, David R. |author2=Williams, G. Dent |journal=[[Journal of Vertebrate Paleontology]]
|volume=16|issue=Supplement to 3
|pages=64
|year=1996
|doi=10.1080/02724634.1996.10011371
}}</ref> ''Deinosuchus'' would probably have used the robust, flat teeth near the back of its jaws to crush the turtle shells.<ref name="schwimmer-prey"/> The "[[Pleurodira|side-necked]]" sea turtle ''[[Bothremydidae|Bothremys]]'' was especially common in the eastern habitat of ''Deinosuchus'', and several of its shells have been found with bite marks that were most likely inflicted by the giant crocodilian.<ref name="schwimmer-prey"/><ref name="schwimmer-williams"/>

Schwimmer concluded in 2002 that the feeding patterns of ''Deinosuchus'' most likely varied by geographic location; the smaller ''Deinosuchus'' specimens of eastern North America would have been opportunistic feeders in an ecological niche similar to that of the modern [[American Alligator|American alligator]]. They would have consumed marine turtles, large fish, and smaller dinosaurs.<ref name="schwimmer-size"/> The bigger, but less common, ''Deinosuchus'' that lived in Texas and Montana might have been more specialized hunters, capturing and eating large dinosaurs.<ref name="schwimmer-size"/> Schwimmer noted no theropod dinosaurs in ''Deinosuchus'''s eastern range approached its size, indicating the massive crocodilian could have been the region's [[apex predator]].<ref name="schwimmer-prey"/>

In 2016, Mark Witton took a rigorous look at the diet of ''Deinosuchus''. His results show that, although ''Deinosuchus'' did eat dinosaurs, its diet was mostly made up of sea turtles.<ref>http://markwitton-com.blogspot.com/search?updated-max=2016-03-14T05:01:00-07:00&max-results=7</ref>

===Growth rates===
[[Image:Deinosuchus scutes by Hatcher.png|thumb|The osteoderms of ''Deinosuchus'', as illustrated by W.J. Holland. They are proportionately much thicker than those of modern crocodilians]]
A 1999 study by Gregory M. Erickson and Christopher A. Brochu suggested the growth rate of ''Deinosuchus'' was comparable to that of modern crocodilians, but was maintained over a far longer time.<ref name="erickson-brochu"/> Their estimates, based on growth rings in the [[Dorsum (biology)|dorsal]] osteoderms of various specimens, indicated each ''Deinosuchus'' might have taken over 35&nbsp;years to reach full adult size, and the oldest individuals may have lived for more than 50&nbsp;years.<ref name="erickson-brochu"/> This was a completely different growth strategy than that of large dinosaurs, which reached adult size much more quickly and had shorter lifespans.<ref name="erickson-brochu"/> According to Erickson, a full-grown ''Deinosuchus'' "must have seen several generations of dinosaurs come and go".<ref name="connor">{{cite news
|title=Solved: Mystery of crocodile that feasted on dinosaurs
|work=[[The Independent]]
|publisher=[[Independent News & Media]]
|date=March 18, 1999
|last=Connor|first=Steve
}}</ref>

Schwimmer noted in 2002 that Erickson and Brochu's assumptions about growth rates are only valid if the osteodermal rings reflect annual periods, as they do in modern crocodilians.<ref name="schwimmer-size"/> According to Schwimmer, the growth ring patterns observed could have been affected by a variety of factors, including "migrations of their prey, wet-dry seasonal climate variations, or oceanic circulation and nutrient cycles".<ref name="schwimmer-size"/> If the ring cycle were <!--subjunctive--> biannual rather than annual, this might indicate ''Deinosuchus'' grew faster than modern crocodilians, and had a similar maximum lifespan.<ref name="schwimmer-size"/>

== Discovery and naming ==
[[Image:Polyptychodon tooth by Emmons.png|thumb|left|Ebenezer Emmons illustrated two fossil teeth in 1858. Most likely, they belonged to the crocodilian that would later be named ''Deinosuchus''.]]
In 1858, [[geologist]] [[Ebenezer Emmons]] described two large fossil teeth found in [[Bladen County, North Carolina]].<ref name="emmons">{{cite book
|title=Report of the North Carolina Geological Survey
|last=Emmons|first=Ebenezer|authorlink=Ebenezer Emmons
|year=1858
|publisher=Henry D. Turner
|pages=219–22
|isbn=1-4366-0488-5
}}</ref> Emmons assigned these teeth to ''[[Polyptychodon]]'', which he then believed to be "a [[genus]] of crocodilian reptiles".<ref name="emmons"/> Later discoveries showed that ''Polyptychodon'' was actually a [[pliosaur]], a type of marine reptile.<ref name="schwimmer-early"/> The teeth described by Emmons were thick, slightly curved, and covered with vertically grooved enamel; he assigned them a new [[species]] name, ''P. rugosus''.<ref name="emmons"/> Although not initially recognized as such, these teeth were probably the first ''Deinosuchus'' remains to be scientifically described.<ref name="schwimmer-early"/> Another large tooth that likely came from ''Deinosuchus'', discovered in neighboring [[Sampson County, North Carolina|Sampson County]], was named ''Polydectes biturgidus'' by [[Edward Drinker Cope]] in 1869.<ref name="schwimmer-early"/>

In 1903, at [[Willow Creek, Montana]], several fossil osteoderms were discovered "lying upon the surface of the soil" by [[John Bell Hatcher]] and T.W. Stanton.<ref name="holland"/> These osteoderms were initially attributed to the [[Ankylosauridae|ankylosaurid]] dinosaur ''[[Euoplocephalus]]''.<ref name="holland"/> Excavation at the site, carried out by W.H. Utterback, yielded further fossils, including additional osteoderms, as well as vertebrae, [[rib]]s, and a [[Pubis (bone)|pubis]].<ref name="holland"/> When these specimens were examined, it became clear that they belonged to a large crocodilian and not a dinosaur; upon learning this, Hatcher "immediately lost interest" in the material.<ref name="holland"/> After Hatcher died in 1904, his colleague [[William Jacob Holland|W.J. Holland]] studied and described the fossils.<ref name="holland"/> Holland assigned these specimens to a new genus and species, ''Deinosuchus hatcheri'', in 1909.<ref name="holland"/> ''Deinosuchus'' comes from the [[Ancient Greek|Greek]] δεινός/''deinos'', meaning "terrible", and σοῦχος/''suchos'', meaning "crocodile".<ref name="holland"/>

[[Image:1954-Colbert-Bird-Phobosuchus.png|thumb|This skull reconstruction, exhibited at the [[American Museum of Natural History]] for nearly a half-century, is probably the best known of all ''Deinosuchus'' fossils. The darker-shaded portions are actual fossil bone, while the light portions are plaster.]]
A 1940 expedition by the [[American Museum of Natural History]] yielded more fossils of giant crocodilians, this time from [[Big Bend National Park]] in [[Texas]].<ref name="colbert-bird"/> These specimens were described by Edwin H. Colbert and Roland T. Bird in 1954, under the name ''Phobosuchus riograndensis''.<ref name="colbert-bird"/> Donald Baird and [[Jack Horner (paleontologist)|Jack Horner]] later assigned the Big Bend remains to ''Deinosuchus'', which has been accepted by most modern authorities.<ref name="schwimmer-early"/><ref name="baird-horner">{{cite journal
|title=Cretaceous dinosaurs of North Carolina
|author=Baird, D.; [[Jack Horner (paleontologist)|Horner, J.]]
|journal=Brimleyana|volume=2|year=1979
|pages=1–28
}}</ref> The genus name ''Phobosuchus'', which was initially created by [[Franz Nopcsa von Felső-Szilvás|Baron Franz Nopcsa]] in 1924, has since been discarded because it contained a variety of different crocodilian species that turned out to not be closely related to each other.<ref name="schwimmer-early"/>

The American Museum of Natural History incorporated the skull and jaw fragments into a plaster restoration, modeled after the present-day [[Cuban Crocodile|Cuban crocodile]].<ref name="colbert-bird"/> Colbert and Bird stated this was a "conservative" reconstruction, since an even greater length could have been obtained if a long-skulled modern species, such as the saltwater crocodile had been used as the template.<ref name="colbert-bird"/> Because it was not then known that ''Deinosuchus'' had a broad snout, Colbert and Bird miscalculated the proportions of the skull, and the reconstruction greatly exaggerated its overall width and length.<ref name="schwimmer-early"/> Despite its inaccuracies, the reconstructed skull became the best-known specimen of ''Deinosuchus'', and brought public attention to this giant crocodilian for the first time.<ref name="schwimmer-early"/>

Numerous additional specimens of ''Deinosuchus'' were discovered over the next several decades. Most were quite fragmentary, but they expanded knowledge of the giant predator's geographic range. As noted by Chris Brochu, the osteoderms are distinctive enough that even "bone granola" can adequately confirm the presence of ''Deinosuchus''.<ref name="schwimmer-early"/><ref name="brochu-occurrences">{{cite mailing list
|mailinglist=Dinosaur Mailing List
|title=''Deinosuchus'' occurrences
|last=Brochu|first=Christopher A.
|date=1998-02-07
|accessdate=2009-01-02
|url=http://dml.cmnh.org/1998Feb/msg00202.html
}}</ref> Better cranial material was also found; by 2002, David R. Schwimmer was able to create a composite computer reconstruction of 90% of the skull.<ref name="schwimmer-lifetimes"/><ref name="schwimmer-localities"/>

==Classification and species==
[[Image:Deinosuchus at the CMNH.jpg|thumb|''Deinosuchus'' scutes and vertebra, [[Carnegie Museum of Natural History]].]]
Since the discovery of the earliest fragmentary remains that will come to be known as ''Deinosuchus'', it was found that it was a relative of [[crocodile]]s, going as far as placing it in the same [[Family (biology)|family]] ([[crocodylidae]]) in 1954, this assignment was mostly supported by dental features<ref name="colbert-bird"/> but was overturned in 1999 when the finding of new specimens from [[Texas]] and [[Georgia (US state)|Georgia]] helped place ''Deinosuchus'' in a phylogenetic analysis, finding it in a basal position within the clade [[Alligatoroidea]] along with ''[[Leidyosuchus]]''.<ref name="brochu-memoirs"/> This classification was bolstered in 2005 by the discovery of a well-preserved ''Deinosuchus'' [[Skull|brain case]] from the Blufftown Formation of Alabama, which shows some features reminiscent of those in the modern American alligator,<ref name="knight">{{cite journal|title=Anatomy of the skull and braincase of a new ''Deinosuchus rugosus'' specimen from the Blufftown Formation, Russell County, Alabama|journal=Geological Society of America Abstracts with Programs|volume=37|issue=2|year=2005 |author1=Knight, Terrell K. |author2=Schwimmer, David R. |page=12 |url=http://gsa.confex.com/gsa/2005SE/finalprogram/abstract_83311.htm}}</ref> ''Deinosuchus'' however, was not a direct ancestor of modern alligators.<ref name="schwimmer-genealogy">{{cite book|title=King of the Crocodylians: The Paleobiology of Deinosuchus|last=Schwimmer|first=David R.|publisher=[[Indiana University Press]]|pages=136–166|chapter=A Genealogy of ''Deinosuchus''|year=2002|isbn=0-253-34087-X}}</ref>

The species pertaining to ''Deinosuchus'' since the resurrection of the [[genus (biology)|generic]] name in 1979 have been traditionally recognized as ''D. rugosus'' from the eastern side of the [[Western Interior Seaway]] and the larger ''D. riograndensis'' from the western side, characterized by differences of the shape of their [[osteoderms]] and teeth, this view was not always favored by all researchers and in 2002 it was proposed that there was only one species, ''Deinosuchus rugosus'', with the differences found between the two species explained as the result of the larger size of the western morph.<ref name="schwimmer-species"/> This proposal faced support<ref name="lucas">{{cite journal|year=2006|title=The giant crocodylian ''Deinosuchus'' from the Upper Cretaceous of the San Juan Basin, New Mexico|url=http://www.nmnaturalhistory.org/science/bulletins/35/sci_bulletin35_29.pdf|format=pdf|journal=New Mexico Museum of Natural History and Science Bulletin|volume=35|pages=247|author1=Lucas, Spencer G.|author2=Sullivan, Robert M.|author3=Spielmann, Justin A.|deadurl=yes|archiveurl=https://web.archive.org/web/20090617095747/http://www.nmnaturalhistory.org:80/science/bulletins/35/sci_bulletin35_29.pdf|archivedate=2009-06-17|df=}}</ref> and criticism<ref name="westgate-et-al"/><ref name="anglen-lehman"/><ref name="brochu-review"/> and was dropped in 2010 when new fossils of specimens from both sides of the interior seaway showed that the differences between the two morphs were not just size related, teeth from large eastern specimens retained the common characteristics of other specimens of the region and osteoderms from small specimens of the western morph were no different from those of large ones, supporting the specific separation of the two morphs, now formalized as ''D. rugosus'' and ''D. riograndensis''.<ref name=Schwimmer2010>{{cite journal|last=Schwimmer|first=D.R.|title=One or two species of the giant crocodylian Deinosuchus|journal=Journal of Vertebrate Paleontology|year=2010|volume=30|issue=Supplement 2|doi=10.1080/02724634.2010.10411819|pages=1A}}</ref>

==See also==
{{portal|Paleontology}}
* ''[[Sarcosuchus]]''
* ''[[Stomatosuchus]]''

{{clear}}

== References ==
{{reflist|30em}}

{{Spoken Wikipedia|Deinosuchus_spoken_article.ogg|2010-08-27}}

{{Extinct Crocodilia|A.}}

[[Category:Late Cretaceous crocodylomorphs of North America]]
[[Category:Fossil taxa described in 1909]]
[[Category:Late Cretaceous reptiles of North America]]
[[Category:Taxa named by William Jacob Holland]]
[[Category:Campanian first appearances]]
[[Category:Campanian extinctions]]