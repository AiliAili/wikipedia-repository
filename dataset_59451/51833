{{Infobox journal
| title = Journal of Economic Geography
| cover =
| editor = Harald Bathelt, Kristian Behrens, Neil Coe, William R. Kerr
| discipline = [[Economic geography]]
| former_names =
| abbreviation = J. Econ. Geogr.
| publisher = [[Oxford University Press]]
| country =
| frequency = Bimonthly
| history = 2001-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 2.821
| impact-year = 2013
| website = http://joeg.oxfordjournals.org/
| link1 = http://joeg.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://joeg.oxfordjournals.org/archive
| link2-name = Online archive
| JSTOR =
| OCLC = 47191782
| LCCN = 2001223256
| CODEN =
| ISSN = 1468-2702
| eISSN = 1468-2710
}}
The '''''Journal of Economic Geography''''' is a bimonthly [[peer-reviewed]] [[academic journal]] published by [[Oxford University Press]] covering all aspects of [[economic geography]], including the intersection between [[economics]] and [[geography]]. The [[editors-in-chief]] are [[Harald Bathelt]] ([[University of Toronto]]), [[Kristian Behrens]] ([[Université du Québec à Montréal]]), Neil Coe ([[National University of Singapore]]), and William R. Kerr ([[Harvard Business School]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[VINITI Database RAS]]
* ''[[Referativny Zhurnal]]'' 
* [[Current Contents]]/Social and Behavioral Sciences 
* [[EconLit]] 
* [[Environmental Science and Pollution Management]] 
* [[Geographical Abstracts]]
* [[Journal of Planning Literature]]
* [[ProQuest|ProQuest Business Research Databases]] 
* [[Research Papers in Economics]]
* [[Social Science Research Network]]
* [[Social Sciences Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.821.<ref name=WoS>{{cite book |year=2014 |chapter=Journal of Economic Geography |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://joeg.oxfordjournals.org/}}

[[Category:Oxford University Press academic journals]]
[[Category:Geography journals]]
[[Category:Publications established in 2001]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Economics journals]]