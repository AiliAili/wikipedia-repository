{{Good article}}
{{Infobox University Boat Race
| name= 51st Boat Race
| winner =Oxford
| margin = 3 1/2 lengths
| winning_time= 21 minutes 39 seconds
| date= {{Start date|1894|3|17|df=y}}
| umpire =[[Frank Willan (rower)|Frank Willan]]<br>(Oxford)
| prevseason= [[The Boat Race 1893|1893]]
| nextseason= [[The Boat Race 1895|1895]]
| overall =22&ndash;28
}}
The '''51st Boat Race''' took place on 22 March 1894.  [[The Boat Race]] is an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford went into the race leading by 27&ndash;22 in the event and of the eighteen participants, half had previous Boat Race experience.  Umpired by former rower [[Frank Willan (rower)|Frank Willan]], Oxford won the race by three-and-a-half lengths in a time of 21 minutes 39 seconds, for their fifth consecutive victory in the event.  It was the largest margin of victory since the [[The Boat Race 1883|1883 race]].

==Background==
[[File:R.C.Lehmann.jpg|right|thumb|upright|[[R. C. Lehmann]] coached Oxford, despite being a [[Cantabrigian]] and former captain of [[Trinity College Boat Club|1st Trinity Boat Club]].]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the boat clubs of [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 11 September 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities, as of 2014 it is followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 5 July 2014}}</ref><ref>{{Cite book | url = https://books.google.com/books?id=o2QpA0fGyiIC&pg=PA287&lpg=PA287&dq=%22boat+race%22+%22united+kingdom%22+audience&source=bl&ots=WJsXwqiRfL&sig=5C_pRDSK839-C46kyEaZJXgjjSk&hl=en&sa=X&ei=gbElVLH2E8jW7Qat-oC4Aw&ved=0CCcQ6AEwAQ#v=onepage&q=%22boat%20race%22%20%22united%20kingdom%22%20audience&f=false | title=Gaming the World: How Sports Are Reshaping Global Politics and Culture| first = Andrei |last=Markovits|first2=Lars |last2=Rensmann| publisher = Princeton University Press| date= 6 June 2010 | isbn=978-0691137513|pages=287&ndash;288}}</ref> Oxford went into the race as reigning champions, having beaten Cambridge by one-and-a-quarter lengths in the [[The Boat Race 1893|previous year's race]], and held the overall lead, with 27 victories to Cambridge's 22 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 11 November 2014}}</ref><ref name=results>{{Cite web | url = http://theboatraces.org/results | title =Men &ndash; Results | publisher = The Boat Race Company Limited | accessdate = 27 September 2014}}</ref>

Oxford were coached by [[William Fletcher (rower)|William Fletcher]] (who rowed for Oxford in the [[The Boat Race 1890|1890]], [[The Boat Race 1891|1891]], [[The Boat Race 1892|1892]] and [[The Boat Race 1893|1893 races]]), [[R. C. Lehmann]] (former president of the [[Cambridge Union Society]] and captain of the [[Trinity College Boat Club|1st Trinity Boat Club]]; although he had rowed in the trials eights for Cambridge, he was never selected for the Blue boat)<ref>{{Cite book | title = A History of the University of Cambridge: Volume 3, 1750&ndash;1870 | first = Peter | last = Searby| publisher = [[Cambridge University Press]] | date = 6 November 1997 | isbn = 978-0521350600 | page = 664 | url = https://books.google.com/books?id=VoMPRz8nYQEC&pg=PA664}}</ref> and [[Douglas McLean (rower)|Douglas McLean]] (an Oxford Blue five times between 1883 and 1887).<ref name=coaches/>  Cambridge's coach was Charles William Moore (who represented Cambridge in the [[The Boat Race 1881|1881]], [[The Boat Race 1882|1882]], [[The Boat Race 1883|1883]] and [[The Boat Race 1884|1884 races]]).<ref name=coaches>Burnelll, pp. 110&ndash;111</ref>

The umpire for the race for the sixth year in a row was [[Frank Willan (rower)|Frank Willan]] who won the event four consecutive times, rowing for Oxford in the [[The Boat Race 1866|1866]], [[The Boat Race 1867|1867]], [[The Boat Race 1868|1868]] and [[The Boat Race 1869|1869 races]].<ref>Burnell, pp. 49, 59</ref>

==Crews==
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 3&nbsp;[[Pound (mass)|lb]] (77.3&nbsp;kg), {{convert|7|lb|kg|1}} per rower more than their opponents.  Cambridge's crew contained four rowers who had previously participated in the Boat Race, including  Lionel Arthur Edward Ollivant, Charles Thurstan Fogg-Elliiot, Robert Orme Kerrison and Trevor Gwyn Elliot Lewis.  Oxford saw five member's of the previous year's crew return, including Charles Murray Pitman and [[Hugh Benjamin Cotton]] who were rowing in their third consecutive Boat Race.<ref name=burn65>Burnell, p. 65</ref>  The Dark Blues also saw the introduction of William Burton Stewart, a [[rugby union]] [[Blue (university sport)|Blue]] who Drinkwater described as "a useful heavy-weight" who had rowed for [[Brasenose College, Oxford|Brasenose]] and [[Leander Club]] at the [[Henley Royal Regatta]].<ref name=drink99/> Eleven of the sixteen rowers were educated at [[Eton College]].<ref name=burn65/>  All of the competitors in the race were registered as British.<ref>Burnell, p. 38</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[Hugh Benjamin Cotton|H. B. Cotton]] (P) || [[Magdalen College, Oxford|Magdalen]] || 9 st 13 lb || A. H. Finch || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 0 lb
|-
| 2 ||  M. C. Pilkington || [[Magdalen College, Oxford|Magdalen]] || 12 st 4 lb || N. W. Paine || [[Trinity College, Cambridge|3rd Trinity]]  || 11 st 1 lb
|-
| 3 || W. Burton Stewart || [[Brasenose College, Oxford|Brasenose]] || 13 st 5 lb || [[Sir Charles Ross, 9th Baronet|Sir Charles Ross]] || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 8 lb
|-
| 4 || J. A. Morrison || [[New College, Oxford|New College]] || 12 st 5 lb ||  H. M. Bland || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 5 lb
|-
| 5 || E. G. Tew || [[Magdalen College, Oxford|Magdalen]] || 13 st 7 lb || L. A. E. Olliivant || [[Trinity College, Cambridge|1st Trinity]] || 13 st 5.75 lb
|-
| 6 || T. H. E. Stretch || [[New College, Oxford|New College]] || 12 st 4 lb || C. T. Fogg-Elliot (P) || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 8 lb
|-
| 7 || W. E. Crum || [[New College, Oxford|New College]] || 12 st 0 lb ||  R. O. Kerrison || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 12 lb
|-
| [[Stroke (rowing)|Stroke]] || C. M. Pitman || [[New College, Oxford|New College]] || 12 st 0 lb || T. G. E. Lewis ||[[Trinity College, Cambridge|3rd Trinity]]  || 11 st 12 lb
|-
| [[Coxswain (rowing)|Cox]] || L. Portman || [[University College, Oxford|University]] || 8 st 5 lb || F. C. Begg || [[Trinity Hall, Cambridge|Trinity Hall]] || 8 st 0 lb
|-
!colspan="7"|Source:<ref name=dodd311>Dodd, p. 311</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]], along which the race is conducted]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=burn65>Burnell, p. 65</ref>  The umpire was accompanied on his launch by the [[Duke of York]] (later to become [[George V|King George V]]).<ref name=drink99>Drinkwater, p. 99</ref>  Despite a poor tide, weather conditions were good and the race commenced at 9:12&nbsp;a.m.  [[Stroke rate|Rating]] 41 strokes per minute, Cambridge took a brief early lead but were soon overhauled by Oxford who led by half a length by the Mile Post.<ref name=drink100>Drinkwater, p. 100</ref>

Taking advantage of the bend in the river, the Dark Blues continued to pull away and were clear of Cambridge by the time the crews passed below [[Hammersmith Bridge]].  At The Doves pub, Oxford were more than a length clear, and had extended that to three lengths clear by Chiswick and five by [[Barnes Railway Bridge|Barnes Bridge]].  Relaxing down to a paddle, Oxford passed the finishing post with a three-and-a-half length advantage, in a winning time of 21 minutes 39 seconds.<ref name=drink100/>  It was Oxford's fifth consecutive win, was their largest margin of victory since the [[The Boat Race 1883|1883 race]], and took the overall record in the event to 28&ndash;22 in their favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1894}}
[[Category:1894 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1894 sports events]]