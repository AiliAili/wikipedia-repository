{{one source|date=April 2015}}

{{Infobox  website
 | name           = HAL
 | logo          = LogoHAL.PNG
 | logo_size   = 209
 | image         = 
 | description   = Archive ouverte 
 | url           = https://hal.archives-ouvertes.fr/
 | commercial    = no
 | type          = research
 | subscription  = free
 | Location_city = [[Lyon]]
 | Location_country  = France
 | collection_size =  over 1 100 000 items
 | owner  = [[Centre pour la communication scientifique directe]] (CCSD)
 | launch_date     = 2000
 | current_status   = active
}}

'''Hyper Articles en Ligne''', generally shortened to '''HAL''', is an open archive where [[author]]s can deposit scholarly documents from all [[academia|academic]] fields. It has a good position in the international web repository ranking.<ref>http://repositories.webometrics.info/en/world</ref>

HAL is run by the ''Centre pour la communication scientifique directe'',<ref>[http://ccsd.cnrs.fr CCSD]</ref> a [[France|French]] computing centre, which is part of the [[French National Centre for Scientific Research]], CNRS. Other French institutions, such as [[INRIA]], have joined the system. While it is primarily directed towards French academics, participation is not restricted to them.

Documents in HAL are uploaded either by one of the authors with the consent of the others or by an authorized person on their behalf.

HAL is a tool for direct scientific communication between academics. A text posted to HAL is normally comparable to that of a paper that an investigator might submit for publication in a [[peer review|peer-reviewed]] [[scientific journal]] or conference proceedings. A document deposited in HAL will not be subjected to any detailed scientific evaluation, but simply a rapid overview, to ensure that it falls within the category defined above.

An uploaded document does not need to have been published or even to be intended for publication: It may be posted to HAL as long as its scientific content justifies it. But should the article be published, contributors are invited to indicate the relevant bibliographic information and the [[digital object identifier]] (DOI).

HAL aims to ensure the long term preservation of a deposited document that are stored there permanently and will receive a stable web address. Thus, like any publication in a traditional scientific journal, it can be cited in other work.

The free online access to these documents provided by HAL is intended to promote the best possible dissemination of research work; the intellectual property remains that of the authors.

For physics, mathematics and other natural science topics, HAL has an automated depositing agreement with [[arXiv]]. A similar agreement exists with [[PubMed Central]].

A handful of institutions have their own entrance to HAL, called portals.  One example is the Arts and Humanities eprint repository, [[hprints]].

As an [[Open access (publishing)|open access]] repository, HAL complies with the ''[[Open Archives Initiative]]'' (OAI-PMH).

==Bibliography==
C. Berthaud, Open archive (HAL). Direct scientific communication tool, ISKO-Maghreb, 1st International symposium, Hammamet (Tunisie), 13-14 mai 2011

==References==
<references/>

== External links ==
* [http://hal.archives-ouvertes.fr/index.php?langue=en HAL's English website]

[[Category:Eprint archives]]
[[Category:Open-access archives]]
[[Category:Bibliographic databases and indexes]]