{{Infobox musical artist
| name                = Golden Bomber
| image               = Japan expo niconico 2011 (5905163915).jpg
| caption             = Golden Bomber at [[Japan Expo]] 2011
| image_size          = 250
| landscape           = yes
| background          = group_or_band
| alias               = 
| origin              = [[Japan]]
| instrument          = 
| genre               = [[Pop punk]]<br>[[power pop]]<br>[[comedy rock]]
| years_active        = 2004-present
| label               = Euclid/Zany Zap<br>[[Atlantic Records|Atlantic]] {{small|(2008 - )}}
| associated_acts     = 
| website             = [http://sound.jp/g_b/ Official site]
| current_members     =
* Shō Kiryūin
* Yutaka Kyan
* Jun Utahiroba
* Kenji Darvish
| past_members        =
* Dankichi Tenkujō
* Teppei Chimatsuri
* Niwatoriwa Susatobeni
}}

{{Nihongo|'''Golden Bomber'''|ゴールデンボンバー|Gōruden Bonbā|abbreviated as {{nihongo4|'''Kinbaku'''|金爆}}, {{nihongo4|'''Bomber'''|ボンバー|Bonbā}} or '''GB'''}} is a Japanese [[visual kei]] "[[Air guitar|air]]" [[Rock music|rock]] band formed in 2004 and signed to the independent label Euclid Agency (sublabel ''Zany Zap''). Despite not actually performing the music themselves, they have gained popularity through their theatrical "live shows".

Currently, Golden Bomber have released 12 singles and 11 albums, some of which have gained the top places in national [[Oricon]] chart.<ref name="oriconcd"/><ref name="oriconweek"/>

== History ==
The band was formed in 2004 by vocalist Shō Kiryūin and guitarist Yutaka Kyan. The band's concept was "Hyper Giga Hybrid Super Subculture Visual Rock". Teppei Chimatsuri joined as the first drummer and keyboardist; when he left later, Dankichi Tenkujō took over as drummer. Jun Utahiroba joined as bassist on April 22, 2007. On April 5, 2009, Dankichi left the band, and on April 10, 2009, Kenji Darvish joined as a drummer.

None of the members except Shō Kiryūin play instruments. The band's studio recordings are done by professional musicians, as attested to by the group members themselves. Shō Kiryūin writes the music and lyrics, and creates the arrangements with Tatsuo of Everset. In concert, the band members don't play their instruments; instead they dance and perform and act out dramas with the aid of videos. Shō Kiryūin is the only one who always sings live. The band's creative output, including songs, music videos, promotional videos and performances, contains parodies of famous bands, artists, manga and other aspects of Japanese popular culture.

Their first single, "Dakishimete Shwarz", was released on 1 May 2008, followed by four more consecutive singles.<ref name="jame-world">{{cite web |url= http://www.jame-world.com/us/artists-biography-1270-golden-bomber.html|title=Golden Bomber on JaME World}}</ref> Their first two albums, "The Golden J-POPS" and "Renai Shuukyouron" were put on sale on December 24, 2007, and their first live DVD was released that same day. In 2009 Golden Bomber had 12 consecutive one-man concerts on the 1st day of each month and performed a country-wide 4646 tour. Their most famous song, "Memeshikute", was released as single on 21 October 2009.

Through 2010 they continued putting on their one-man concerts during the Oneman Kowai, and Zenryoku Baka tours, and they had a consecutive 12 month song releases on dwango.jp.<ref name="jame-world"/> All the songs released on dwango were No.&nbsp;1 in dwango daily rankings. In 2010 the band was due to perform in [[Shanghai]], [[China]] during [[Expo 2010]], but the performance was cancelled.<ref>{{cite web |url= http://www.musicjapanplus.jp/news/1/5325/golden-bomber-performance-in-shanghai-canceled/|title=MusicJapanPlus news|accessdate = 2010-08-10}}</ref>

The popularity of the band increased significantly through 2010. By the end of the year, Golden Bomber had received major debut offers from seven companies, but rejected them all, preferring to remain a "good-for-nothing" indie band out of consideration for feelings of their fans.<ref>{{cite web |url= http://www.musicjapanplus.jp/news/1/6907/golden-bomber-making-a-major-declaration/|title= MusicJapanPlus news| accessdate = 2010-12-28}}</ref><ref>{{cite web |url= http://ameblo.jp/kiryu-in/archive5-201012.html#main|title=Kiryuuin Sho blog 27/12/2010| accessdate = 2010-12-27 | language = Japanese}}</ref>"Mata Kimi ni Bangou wo Kikenakatta" single reached No.&nbsp;4 on [[Oricon]] weekly charts<ref name="oriconcd">{{cite web |url= http://www.oricon.co.jp/prof/artist/452933/ranking/cd_single/|title=Oricon single ranking}}</ref> and was ranked No.&nbsp;3 in Oricon 2010 year top selling indies singles chart.<ref>{{cite web |url= http://www.jame-world.com/us/articles-71328-oricon-indie-top-20-2010-albums-singles.html|title=Oricon Indie Top 20 2010: Albums & Singles, JaME World| accessdate = 2011-01-01 }}</ref>  "Shimohanki Best" album was ranked No.&nbsp;3 in [[Oricon]] weekly charts.<ref name="oriconweek">{{cite web |url= http://www.oricon.co.jp/prof/artist/452933/ranking/cd_album/|title=Oricon album ranking}}</ref>

[[File:Golden Bomber Japan Expo.jpg|thumb|right|Golden Bomber performing at Japan Expo 2011.]]
In 2011 Golden Bomber continued with the "Life is All Right" spring national tour (the initial name of the tour, "Isshou Baka (Lifetime Idiot)" was changed due to the earthquake and tsunami in Japan).<ref>{{cite web|url=http://headlines.yahoo.co.jp/hl?a=20110629-00000001-exp-musi |title=Yahoo!Japan Headlines news |accessdate=2011-06-29 |language=Japanese }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> In July 2011 Golden Bomber performed at [[Japan Expo]] in Paris, France,<ref>{{cite web|url=http://www.japan-expo.com/en/invite/golden-bomber_129.htm |title=Japan Expo official site |deadurl=yes |archiveurl=https://web.archive.org/web/20110725185419/http://www.japan-expo.com/en/invite/golden-bomber_129.htm |archivedate=2011-07-25 |df= }}</ref> and in August played two live shows in Seoul, South Korea. "Yareba Dekiru Ko" national tour is scheduled for autumn 2011.<ref>{{cite web |url= http://natalie.mu/music/news/49988|title=natalie.mu news| accessdate = 2011-05-24| language = Japanese}}</ref>

On 14 and 15 January 2012 Golden Bomber performed in [[Nippon Budokan]], and on 21 January - in [[Osaka-jō Hall]].

Golden Bomber also performed for 2 days at the [[Yokohama Arena]] on 17 and 18 June 2012. On 18 June 2012 they released their first international album: "The Golden Best" in the UK, USA, France, Germany, Taiwan and Korea.<ref>{{cite web |url= http://www.jame-world.com/us/news-86175-golden-bomber-s-best-of-to-be-released-worldwide.html|title=JaME World| accessdate = 2012-04-25}}</ref>

Golden Bomber also hosts a program on [[Nico Nico Douga]]. The members have collaborated with different artists not only from music, but also from the fashion industry,<ref>{{cite web |url=http://www.tokyohive.com/2011/05/mizca-announces-new-single-rafiora/|title=tokyohive.com Mizca announces new single, "Rafiora"| accessdate = 2011-05-16 |language = Japanese}}Kiryuuin Shou as producer</ref> with different fashion brands and game producers.

== Members ==

* ([[Vocals]]): {{nihongo|Shō Kiryūin|鬼龍院 翔|Kiryūin Shō}}
** Born on June 20, 1984, in [[Tokyo]].
** Writes all music and lyrics by himself. He can play various musical instruments, including violin.<ref>{{cite web |url=https://www.youtube.com/watch?v=Wks6ShXokJE|title=Performing song on guitar, bass, drums and doing vocals|language = Japanese}}</ref>
** Created lots of parody songs of songs by famous singers and bands, imitating their melody, lyrics, singing style. For example, "Ultra Phantom" is [[Koshi Inaba]], "Tsunami no Johnny" - [[Keisuke Kuwata]] imitation.
** A host of the ''[[All Night Nippon]]'' radio program, from January 5, 2011 to present.<ref>{{cite web |url=http://www.allnightnippon.com|title=All Night Nippon official|language = Japanese}}</ref>
** Released his autobiography "I’m Golden Bomber’s vocalist, are there any questions?"<ref>{{cite web |url=http://www.jame-world.com/us/news-86540-golden-bomber-s-kiryuuin-shou-to-release-a-book.html|title=JaME World}}</ref> on June 20, 2012.
** Fan of [[Gackt]] and [[Malice Mizer]].
** Made his solo debut with the opening theme "[[Life Is Show Time|Life is SHOW TIME]]" from ''[[Kamen Rider Wizard]]''.<ref>{{cite web |url=https://www.youtube.com/watch?v=81DlbETPXFE|title=Kamen Rider Wizard opening theme}}</ref>
* ([[Guitar]]): {{nihongo|Yutaka Kyan|喜矢武 豊|Kyan Yutaka}}
** Born on March 15, 1985, in Tokyo.
** He creates all the stage properties for the shows on his own.
**Played main role in "Shi ga futari wo wakatsu made" film ("Iro no nai Ao" chapter) released in September 2012　<ref>{{cite web |url=http://www.wild-strawberry.com/ws/shigawaka|title="Shi ga futari wo wakatsu made" OHP}}</ref>
* ([[Bass guitar]]): {{nihongo|Jun Utahiroba|歌広場 淳|Utahiroba Jun}}
**Born as Takayama Jun (高山淳) on August 30, 1985, in [[Chiba, Chiba|Chiba]].
**Graduated [[Nihon University]], Faculty of Fine Arts Literature Department.
**Writes an interview column called "No Ikemen No Life" about music artists in ''Monthly TVnavi'' magazine<ref>{{cite web |url=http://www.tvnaviweb.jp/index.html|title=TVnavi|language = Japanese}}</ref>
**On July 25, 2016, Jun announced his marriage to his non-celebrity girlfriend of two years. The private ceremony took place in June 2016.<ref>{{cite web|url=http://www.kincs.jp/fashion/mintneko/ |title=Sixh. OHP |language=Japanese |deadurl=yes |archiveurl=https://web.archive.org/web/20120620215228/http://www.kincs.jp:80/fashion/mintneko/ |archivedate=2012-06-20 |df= }}</ref>
* ([[Drums]]): {{nihongo|Kenji Darvish|樽美酒 研二|Darubisshu Kenji}}
** Born on November 28, 1980, in [[Fukuoka]].
** Joined on April 10, 2009.
** Famous for doing white-black-red kabuki-like make-up
** Originally a guitarist, also did vocals in his previous band, now vocalist in Reverset.
** His blog is published as a book "The Best of Obama blog" on the 2nd of February 2012<ref>{{cite web 
|url=http://www.fusosha.co.jp/book/2012/06546.php|title=Fusosha Publishing official|language = Japanese}}</ref><ref>{{cite web |url=http://www.musicjapanplus.jp/news/1/12850/golden-bombers-darvish-kenji-releasing-blog-as-book/|title=MJP}}</ref>
** Famous for appearing in the 28th, 30th, 31st and 32nd tournaments of [[Sasuke (TV series)|Sasuke]](Ninja Warrior).

=== Past members ===
* Doramu: {{nihongo|Dankichi Tenkūjō|天空城 団吉|Tenkūjō Dankichi}}
** Born on August 13, 1984, in Hokkaido.
** Left on April 5, 2009 at Takadanobaba Club Phase live.
* Drums: {{nihongo|Teppei Chimatsuri|血祭 鉄兵|Chimatsuri Teppei}}
** Born on March 15, 1974, in Nagasaki.
** First air-drummer, also played keyboards on stage.
** Joined on December 24, 2005 at Roppongi Edge live.
** Left on March 15, 2007 at Ikebukuro Cyber live.
** He and Shō Kiryūin were originally colleagues of the same shop of Lawson.
* Doramu: {{nihongo|Riku Toriaezu|鶏和酢 里紅|Toriaezu Riku}}
** He stood in as an air-drummer and an air-keyboardist when Dankichi Tenkujo left, when Tenkujo returned he went back to being a staff member.
** He managied BAR of "Beer Bar Lemon Heart" in Tokyo. 
** Now, He is Manga-art cafe "CAFE ZENON" 's  staff, and the assistant of Japanese Manga "BAR lemon heart" .

== Music relationships ==
Different musicians took part in Golden Bomber recordings, Golden Bomber also appeared together with a number of bands.
([[Deluhi]] Leda, [[Janne Da Arc]] shuji, Ikuo, HIROKI, [[La'cryma Christi]] SHUSE, [[Kishidan]], [[Shokotan]] etc.)<ref>{{cite web |url= http://www.tokyohive.com/2010/09/v-kei-group-golden-bomber-welcome-guests-on-new-single/|title= V-kei group Golden Bomber welcome guests on new single| accessdate = 2009-09-09}} tokyohive.com</ref>

== Discography ==

=== Singles ===
{| class="wikitable" style="font-size: 95%;"
|-
!No.
!Title
!Title (Japanese)
!Date of release
!Notes
|-
|01
|Dakishimete Schwarz / Doutei ga!
|抱きしめてシュヴァルツ / 童貞が!
|1 May 2008
|
|-
|02
|Saite Saite Kirisaite / Masashi
|咲いて咲いて切り裂いて / まさし
|1 June 2008
|
|-
|03
|Gomen ne, Aishiteru / Kame Power
|ごめんね、愛してる/ 亀パワー
|1 July 2008
|
|-
|04
|Torauma Kyabajou / Motokare Korosu
|トラウマキャバ嬢 / 元カレ殺ス
|1 August 2008
|
|-
|05
|Hotel Love / Itsumo to Onaji Yoru
|ホテルラブ / いつもと同じ夜
|1 September 2008
|
|-
|06
|Blow Wind / Time Machine ga Hoshii yo
|Blow Wind / タイムマシンが欲しいよ
|17 June 2009
|Joint single with SMILY☆SPIKY. <br/>"Time Machine ga Hoshii yo" is [[5pb.]]'s game [[Skip Beat!]] ending theme<ref>{{cite web| url =http://gab.fc.yahoo.co.jp/5/117/ | title = Gaboo! SPECIAL interview| accessdate = 2009-07-07 | language = Japanese}}</ref>
|-
|07
|Memeshikute
|女々しくて
|21 October 2009
|3 types. Oricon Weekly chart No.&nbsp;77, Oricon Indies chart No.&nbsp;4
|-
|08
|Mou Bandoman ni Koi Nante Shinai
|もうバンドマンに恋なんてしない
|25 November 2009
|2 types. Oricon Weekly Chart No.&nbsp;103
|-
|09
|Mata Kimi ni Bangou wo Kikenakatta
|また君に番号を聞けなかった
|6 October 2010
|3 types. Oricon Weekly Chart No.&nbsp;4, Oricon Indies Chart No.&nbsp;1<br/>
|-
|10
|Boku Quest
|僕クエスト
|1 June 2011
|4 types. Oricon Weekly Chart No.&nbsp;5, Oricon Indies Chart No.&nbsp;1. <br/>"Boku Quest" is [[Yu-Gi-Oh! Zexal]] anime ending theme.<ref>{{cite web |url= http://www.musicjapanplus.jp/news/1/7598/golden-bomber-announce-anime-tie-up/|title=MusicJapanPlus, 2011-02-21}}</ref><ref>{{cite web |url= http://natalie.mu/music/news/47660|title=natalie.mu, 2011-04-11}}(in Japanese)</ref>
|-
|11
|Memeshikute/Nemutakute
|女々しくて/眠たくて
|24 August 2011
|3 types<br/> Oricon Weekly Chart No.&nbsp;4, Oricon Indies Chart No.&nbsp;1
|-
|12
|Yowasete Mojito
|酔わせてモヒート
|23 November 2011
|3 types<br/>Oricon Weekly Chart No.&nbsp;3
|-
|13
|[[Dance My Generation]]
|Dance My Generation
|1 January 2013
|3 types<br/>Oricon Weekly Chart No.&nbsp;1
|-
|14
|101 Kaime no Noroi
|101回目の呪い
|1 January 2014
|4 types<br/>Oricon Weekly Chart No.&nbsp;1
|-
|15
|Laura no Kizudarake
|ローラの傷だらけ
|20 August 2014
|Oricon Weekly Chart No.&nbsp;2
|-
|16
|Shinda Tsuma ni Niteiru
|死 ん だ 妻 に 似 て い る 
|29 May 2015
|4 types, sung by Kiryuin Shou,<ref>http://www.cdjapan.co.jp/product/DAKEMMD-1</ref> Kyan Yutaka,<ref>http://www.cdjapan.co.jp/product/DAKEMMD-2</ref> Utahiroba Jun,<ref>http://www.cdjapan.co.jp/product/DAKEMMD-3</ref> Darvish Kenji<ref>http://www.cdjapan.co.jp/product/DAKEMMD-4</ref>
|-
|17
|Mizushōbai o Yamete Kurenai Ka
|水商売をやめてくれないか
|27 April 2016
|
|-
|18
|[[Odoru Pompokolin]]
|おどるポンポコリン
|9 November 2016
|Opening song for the [[Chibi Maruko-chan]] anime series since 10 April 2016. The B-side is Yume Ippai, the cover of the first opening of the Chibi Maruko-chan anime series. <ref>{{cite web|title=ちびまる子ちゃん：オープニング主題歌が金爆に交代　まる子らとの"共演"も|url=http://m.mantan-web.jp/article/20160411dog00m200026000c.html|website=Mantan Web|accessdate=24 November 2016}}</ref>
|-
|19
|[[#CD ga Urenai Konna Yononakaja]]
|#CDが売れないこんな世の中じゃ
|5 April 2017
|
|-
|}

=== Albums ===
{| class="wikitable" style="font-size: 95%;"
|-
!No.
!Title
!Title (Japanese)
!Date of release
!Notes
|-
|01
|Ongaku ga bokura wo dame ni suru
|音楽が僕らを駄目にする
|24 December 2006
|Was distributed at the first lives in the venues. Out of production. Contained 7 songs, including Suppin (スッピン, "Without make-up")
|-
|-
|02
|The Golden J-POPS
|The Golden J-POPS
|24 December 2007
|Was distributed at live venues. Out of production.<ref name="ohp">{{cite web |url= http://sound.jp/g_b/ongaku.html|title=Golden Bomber official site| language = Japanese}}</ref>
|-
|03
|Ren'ai Shuukyouron
|恋愛宗教論
|24 December 2007
|Was distributed at live venues. Out of production.<ref name="ohp"/>
|-
|04
|Imitation Gold ~Kinbaku no Meikyoku Niban Shibori~
|イミテイション・ゴールド〜金爆の名曲二番搾り〜
|1 January 2009
|The parodies of hit songs by famous artists:
[[Gackt]], [[Southern All Stars]], [[CHAGE and ASKA]], [[B'z]], [[Aqua Timez]].
|-
|05
|Sorinokoshita Natsu
|剃り残した夏
|29 July 2009
|Soundtrack CD for "Sorinokoshita Natsu" film
|-
|06
|Golden Album
|ゴールデン・アルバム
|4 January 2012
|4 types<br/> Oricon weekly No.&nbsp;2
|-
|07
|The Past Masters Vol.1
|ザ・パスト・マスターズ
|24 April 2013
|-
|08
|NO MUSIC NO WEAPON
|ノーミュージック・ノーウエポン
|17 June 2015
|Oricon weekly No. 1<ref>http://www.oricon.co.jp/news/2054730/</ref>
|}

=== Best Albums ===
{| class="wikitable" style="font-size: 95%;"
|-
!No.
!Title
!Title (Japanese)
!Date of release
!Notes
|-
|01
|Golden Best ~Pressure~
|ゴールデンベスト〜Pressure〜
|6 January 2010
|Oricon Chart No.&nbsp;150
|-
|02
|Golden Best ~Brassiere~
|ゴールデンベスト〜Brassiere〜
|6 January 2010
|Oricon Chart No.&nbsp;193
|-
|03
|Golden Hour ~ Kamihanki Best 2010~
|ゴールデン・アワー〜上半期ベスト2010〜
|21 July 2010
|Oricon Daily Chart No.&nbsp;6, Oricon Weekly Chart No.&nbsp;14, Oricon Indies Chart No.&nbsp;1
|-
|04
| Golden Hour ~ Shimonanki Best 2010~
|ゴールデン・アワー〜下半期ベスト2010〜
|6 January 2011
|Oricon Daily Chart No.&nbsp;1, Oricon Weekly Chart No.&nbsp;3
|-
|05
|The Golden Best
|The Golden Best
|18 June 2012
|6 versions. Released by [[Atlantic Records]].
|}

=== PV ===
01. Memeshikute (女々しくて)

02. Mou Bandoman ni Koi Nante Shinai (もうバンドマンに恋なんてしない)

03. Mata Kimi ni Bangou wo Kikenakatta (また君に番号を聞けなかった). 3 versions

04. Boku Quest (僕クエスト). 3 versions

05. Memeshikute (女々しくて) from Memeshikute/Nemutakute single. 2 versions

06. Yowasete Mojito (酔わせてモヒート)

07. Ii Hito　（いい人）

08. Sayonara Fuyumi　（さよなら冬美）

09. Chéng lóng hěn kù (成龍很酷) (01/01/12)

10. Dance My Generation (01/01/13)

11. Gomen ne, aishiteru (ごめんね、愛してる)

=== DVD ===
{| class="wikitable" style="font-size: 90%;"
|-
!No.
!Title
!Title (Japanese)
!Date of release
|-
|01
|Gyaru Punch DE Batan Q (Out of production<ref name="ohp"/>)
|ギャルパンチDEバタンQ
|24 December 2007
|-
|02
|"Kinbaku Nengoro Tour Final Hatsu Oneman<br>"Ai wa Chikyuu wo Sukuu" ~Miwa-san 100&nbsp;km Marathon~
|"金爆ねんごろツアー ファイナル 初ワンマン
「愛は地球を巣喰う」〜美輪さん100kmマラソン〜"
|1 February 2009
|-
|03
|Sorinokoshita Natsu (Golden Bomber performance, script, direction.)
|剃り残した夏
|29 July 2009
|-
|04 - 09
|Golden Bomber 2009 Year Oneman Live DVD (January - June)
|ゴールデンボンバー 2009年ワンマンライブDVD (1月-6月)
|24 December 2009
|-
|10 - 15
|Golden Bomber 2009 Year Oneman Live DVD (July - December)
|ゴールデンボンバー 2009年ワンマンライブDVD (7月-12月)
|1 January 2010
|-
|16
|Dai-ichi Yoru Request On The Best ~Pressure night~
|第一夜　リクエスト・オン・ザ・ベスト～Pressure night～
|10 March 2010
|-
|17
|Dai-ni Yoru Request On The Best ~Brassiere night~
|第二夜　リクエスト・オン・ザ・ベスト～Brassiere night～
|10 March 2010
|-
|18
|“STYLISH WEAPON’10 ~Haru no SoyoKaze~ Omake no Ran
|“STYLISH WEAPON’10〜春のそよ風〜おまけの乱”
|27 April 2010
|-
|19
|Golden Bomber Hatsu Kyoufu no Zenkoku Oneman Tour<br>-Oneman Kowai- Tsuika Kouen(2010/6/25@Shibuya O-WEST)」
|「ゴールデンボンバー 初 恐怖の全国ワンマンツアー
-ワンマンこわい-追加公演(2010/6/25@渋谷O-WEST)」
|30 July 2010
|-
|20
|Golden Bomber　Pantsu Daisakusen Tour Final<br>(2010/9/24 @Ebisu LIQUIDROOM)
|ゴールデンボンバー　パンツ大作戦ツアーファイナル
(2010/9/24恵比寿LIQUIDROOM)
|24 November 2010
|-
|21
|Zenryoku Baka(2010/12/27@SHIBUYA-AX)
|「全力バカ」(2010/12/27@SHIBUYA-AX)
|25 February 2011
|-
|22
|SoCcer 24
|SoCcer 24
|15 March 2011
|-
|23
|”Life is all right” Tsuika Kouen (2011/5/17@TOKYO DOME CITY HALL)
|”Life is all right”追加公演(2011/5/17@TOKYO DOME CITY HALL)
|27 July 2011
|-
|24
|Golden Bomber Zepp Tour 2011 "Yareba Dekiruko" 2011.10.7 at Zepp Tokyo
|ゴールデンボンバー Zepp全通ツアー 2011 "やればできる子" 2011.10.7 at Zepp Tokyo
|21 December 2011
|-
|25
|Golden Bomber OneMan Live Tokudaigou "Isshou Baka" 2012.15.01 at [[Nippon Budokan]]
|ゴールデンボンバーワンマンライブ特大号「一生バカ」公演、<br/>1月15日（日）日本武道館
|18 March 2012
|-
|26
|Golden Bomber OneMan Live Tokudaigou "Isshou Baka" 2012.14.01 at [[Nippon Budokan]]
|ゴールデンボンバーワンマンライブ特大号「一生バカ」公演、<br/>1月14日（土）日本武道館
|16 March 2012
|-
|27
|Golden Bomber OneMan Live Tokudaigou "Isshou Baka" 2012.21.01 at [[Osaka Jo Hall]]
|ゴールデンボンバーワンマンライブ特大号「一生バカ」公演、<br/>1月21日（日）大阪城ホール
|30 March 2012
|-
|}

=== Limited Distribution ===
{| class="wikitable" style="font-size: 90%;"
|-
!No.
!Title
!Title (Japanese)
!Date of release
|-
|01
|JUSTICE Kyoto ni Tsugu Shoku no Utage album <br/>("Announcement for JUSTICE Believers... Food Feast")
1.DANDAN Kokoro Hikareteku (FIELD OF VIEW cover)<br/>2.Ai no Mama ni Wagamama ni Boku wa Kimi dake wo Kizutsukenai  ([[B'z]] cover) <br/>3.Beethoven da ne Rock'n'Roll ("21 Emon" Ending cover)<br/>4.Donna Toki Mo ([[Noriyuki Makihara]] cover)
|JUSTICE教徒に告ぐ・・蝕の宴

1.収録曲:DAN DAN 心魅かれてく(FIELD OF VIEWのカバー)

2.愛のままにわがままに僕は君だけを傷つけない([[B'z]]のカバー)

3.ベートーベンだねRock'n'Roll(21エモンのオープニングのカバー)

4.どんなときも。(槇原敬之のカバー)
|31 October 2007
|-
|02
|Datsu Indies Cover Shuu ("Indies Cover Compilation") single <br/> 1. Meteo no Hi (Idenshi Kumikae Kodomokai cover)
|「脱・インディーズカバー集」
1.メテオの日(遺伝子組換こども会のカバー)
|7 June 2008
|-
|03
|Miwa-san 100&nbsp;km Marathon Documentary
|美輪さん100kmマラソンドキュメント
|21 July 2008
|-
|04
|Miwa-san Osaka⇒Tokyo Hitch-hiking
|美輪さん大阪⇒東京横断ヒッチハイク
|22 November 2008
|-
|05
|"Legendary Creatures Really Existed!!

Chase after the [[Kappa]] who appeared in megapolis Tokyo!!!!"
|伝説の妖怪は実在した!!大都会東京に現れた河童を追え!!!!
|4 January 2009
|-
|06
|"In midwinter it's cold for Atonu"
|真冬にアトヌは寒い
|1 February 2009
|-
|07
|Golden Busters, last time prompt! Environmental pollution resulted in mutation?
At last we've seen mysterious creature, Kentaurus!

"Kentaurus makes heart beat faster" Documentary DVD
|ゴールデンバスターズ、さっそく最終回！

環境汚染が引き起こした突然変異か？！

ついに我々は謎の生物、ケンタウロスを見た！！

〜ケンタウロスドキドキドキュメントDVD〜
|1 March 2009
|-
|08
|"Show your rock soul! All 21 songs! Gold-gold extatic live!"
Special DVD "Extremey rare Heartwarming DVD"
|～Rock魂見せてやれ！ 全21曲！金金がギンギンライブ！～
[特典DVD]激レアほのぼのDVD
|5 April 2009
|-
|09
|Kokucchao!
|告っchao!
|3 May 2009
|-
|10
|Tomorrow never world
|Tomorrow never world
|3 May 2009
|-
|11
|Gi-ga-!
|ギーガー!
|3 May 2009
|-
|12
|"Total coverage documentary! Forever continuing big depression!
Surviving in city full make-up singer ~Homeless visual kei~ Yamada-san"
|密着ドキュメント!いつまでも続く大不況!都会を生き抜く
フルメイクシンガー 〜ホームレスビジュアル系〜
|3 May 2009
|-
|13
|It's not 20th, it's the 7th! Vo. Kiryuin Sho Birthday Oneman Live!
Special DVD - "Kiryuuin Sho's splendid development"
|20日じゃないよ7日だよ！Vo.鬼龍院翔バースデーワンマンライブ！　
特典DVD　鬼龍院翔の華麗なる生い立ち
|7 June 2009
|-
|14
|"Dripping sweat, losing weight, become thin" oneman live.
Special DVD - "Before becoming Miwa-san"
|汗かきゲッソリ激ヤセワンマンライブ！　
特典DVD　美輪さんができるまで
|5 July 2009
|-
|15
|Haru ga Kuru Mae ni (Yamada-san version)
|春が来る前に(夜魔堕さんVer.)
|29 July 2009
|-
|16
|"Middle of summer! Hidden in "I hate suntan" phase Kinbaku summer festival!"
Special DVD - "Gita- Solo Kyan Yutaka! ~July tour eating local products revolt~"
| 夏真っ盛り！日焼け大嫌いフェイズに隠れて金爆夏祭り！
特典DVD ギターソロ喜矢武豊！　〜7月ツアー名産品食べまくりの乱〜
|2 August 2009
|-
|17
|"Congratulations on 34 years! Yawara-chan Birthday" Oneman Live!

Kin at highest, Baku at lowest.

Special DVD - "Yawara-chan Birthday Special ~Legend of YAWARA"
|祝34歳！ヤワラチャンバースデーワンマンライブ！

「最高で金、最低でも爆」　

特典DVD 〜ヤワラチャン誕生日スペシャル〜　Legend of YAWARA
|6 September 2009
|-
|18
|"Evangelist of love, showing the ass! Miwa-san's men's ass festival"
|ケツ出せ愛の伝道師！美輪さん男尻祭り!
|4 October 2009
|-
|19
|Gachupin Challenge Series! First part!
|ガチュピン チャレンジシリーズ！第一弾！
|1 November 2009
|-
|20
|"Unexpectedly landing in the middle" trip

- GLOY Homecoming to Hokkaido Compilation-"
|ぶらり途中着陸の旅-GLOY 北海道帰郷編-
|6 December 2009
|-
|21
|Mou Bandoman Ni Koi Nante Shinai (Kyan Yutaka version)
|もうバンドマンに恋なんてしない(喜矢武 豊Ver.)
|15 December 2009
|-
|22
|Kokucchao! (Kyan Yutaka version)
|告っchao!(喜矢武 豊Ver.)
|16 December 2009
|}

=== Omnibus works ===
# Tribute to Murakami (2007) Omnibus Release
#* Featured song: Ikiteita Murakami
# Zany Zap Complex (22 July 2009) Omnibus Release
#* Featured songs: Sick Lady Tabun.../TSUNAMI no Johnny/Motokare Korosu
# NEO VOLTAGE(26 May 2010) Omnibus Release
#* Featured song：†Za・VKei-ppoi Kyoku†

=== Songs written for other artists ===
:* [[The Boss (band)|The Boss]] - "Love Days" - lyrics and music by Shō Kiryūin
:* [[Dancing Dolls]] - "Melomelo Bakkyun" from the single "Touch -A.S.A.P- / Shanghai Darling" - lyrics and music by Shō Kiryūin<ref>{{cite web |url=http://natalie.mu/music/news/75547|title=Dancing Dolls、キリショー提供曲含むデビュー盤先行配信|publisher=[[Natalie (website)|Natalie]]|date=2012-08-29|accessdate=2012-10-18}}</ref>

== Media ==

=== Drama CD ===
* Genpei Gakuen Kassen Roku (2010). Kiryūin Shō - Imai Kanehira role, Kyan Yutaka - [[Taira no Munemori]] role, Utahiroba Jun - Kisou role, Darvish Kenji - Kisou role

== References ==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

== External links ==
* {{Official website|http://sound.jp/g_b/}}
* [http://www.myspace.com/goldenbomberjp Golden Bomber] on [[Myspace]]
* [http://www.euclidagency.com/index1.html Euclid Agency Official]

{{Authority control}}

[[Category:Japanese rock music groups]]
[[Category:Visual kei musical groups]]
[[Category:Japanese pop rock music groups]]
[[Category:Atlantic Records artists]]
[[Category:2004 establishments in Japan]]
[[Category:Musical groups established in 2004]]
[[Category:Comedy rock musical groups]]