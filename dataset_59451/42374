<!-- use 1 of the following 2 infoboxes -->
{{Infobox company
| name             = Blue Yonder Aviation
| logo             = [[File:Blue Yonder Aviation Logo 1999.png|200px]]
| type             = Private company
| genre            = 
| foundation       = 
| founder          = Wayne Winters
| location_city    = [[Indus, Alberta]]
| location_country = [[Canada]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = President: Wayne Winters
| industry         = [[Aerospace]]
| products         = [[Amateur-built aircraft|Kit plane manufacturing]]
| services         = 
| market cap       = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 3 (2005)
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = [http://www.ezflyer.com/ www.ezflyer.com]
| footnotes        = 
| intl             = 
}}

[[File:Blue Yonder EZ Flyer Twin Engine 17.JPG|thumb|right|Blue Yonder President and designer Wayne Winters taxis the prototype [[Blue Yonder Twin Engine EZ Flyer|Twin Engine EZ Flyer]]]]

''' Blue Yonder Aviation ''' is a [[Canada|Canadian]] aircraft manufacturer, specializing in kit aircraft for the North American [[amateur-built aircraft]] and [[Ultralight aircraft (Canada)|ultralight]] markets.

The company website seems to have been taken down in late 2016 and the company may have gone out of business.<ref>{{cite web|url= http://web.archive.org/web/*/http://www.ezflyer.com|title=Internet Archive Wayback Machine|work=archive.org|accessdate=19 February 2017}}{{cbignore}}</ref>

==Origins==
The company was originally formed by Wayne Winters in 1986 as a flying school teaching students on a single [[Spectrum Beaver RX550]] at [[Indus/Winters Aire Park Airport|Indus/Winters Aire Park]] south of [[Calgary]], [[Alberta]].<ref name="BYAirport">Winters, Wayne. [http://www.ezflyer.com/page12BYA.html ''Airport History'', 2001.] Retrieved: 3 March 2009.</ref><ref name="COPA1">Hunt, Adam. ''Merlin Magic''. ''[[Canadian Owners and Pilots Association|COPA]] Flight'', February 2005, p. C-1.</ref>

The airport had originally been purchased in 1914 by Miltor L. Winters from the [[Canadian Pacific Railway]] for [[Canadian dollar|Cdn$]]24 per acre. In 1946 upon returning home from the [[Second World War]] Ralph C. Winters purchased the land from the older Winters. In 1970 he graded the first runway on the property. Ralph Winters son, Wayne Winters assumed operation of the airport when his father retired. The airport is home to a large community of pilots and aircraft, including a large number of ultralights.<ref name="BYAirport" />

==Present history==
In 1996, Blue Yonder purchased the rights to the [[Blue Yonder Merlin|Merlin]] from Merlin Aircraft and started manufacturing the aircraft in a converted pig barn on the property. Winters designed the open-cockpit [[Blue Yonder EZ Flyer|EZ Flyer]] in 1991 and the [[Blue Yonder Twin Engine EZ Flyer|Twin Engine EZ Flyer]] in 1999. The EZ Flyer proved successful and 30 have been completed alongside approximately 50 Merlins. Blue Yonder constructs kits or completed aircraft on a ''made-to-order'' basis. For several years US manufacturer [[Comp Air]] marketed Blue Yonder-produced Merlin kits in the USA under the name "Aero Comp Merlin", although this arrangement is no longer in effect.<ref name="COPA1" /><ref name="COPA2">Hunt, Adam. ''Merlin Magic Revisited''. ''[[Canadian Owners and Pilots Association|COPA]] Flight'', October 2005, page C-1.</ref><ref name="COPA3">Hunt, Adam. ''Pilot Report: EZ Flyer''. ''[[Canadian Owners and Pilots Association|COPA]] Flight'', May 2001, p. C-1.</ref>

In 2011 Winters completed work on a single seat, twin-engined design with an inverted [[V-tail]], inspired by the [[Ultraflight Lazair]]. The new aircraft is marketed as the [[Blue Yonder EZ Fun Flyer]].<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 46. Belvoir Publications. ISSN 0891-1851</ref>

== Aircraft ==

{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Summary of aircraft built by Blue Yonder Aircraft'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type
|-
|align=left| '''[[Blue Yonder Merlin|Merlin]]'''
|align=center| {{avyear|1986}}
|align=center| 50
|align=left| cabin monoplane
|-
|-
|align=left| '''[[Blue Yonder EZ Flyer|EZ Flyer]]'''
|align=center| {{avyear|1991}}
|align=center| 30
|align=left| open cockpit monoplane
|-
|-
|align=left| '''[[Blue Yonder EZ King Cobra|EZ King Cobra]]'''
|align=center| {{avyear|1998}}
|align=center| 1
|align=left| [[P-63 Kingcobra]] replica
|-
|-
|align=left| '''[[Blue Yonder Twin Engine EZ Flyer|Twin Engine EZ Flyer]]'''
|align=center| {{avyear|1999}}
|align=center| 1
|align=left| Twin engine observation aircraft
|-
|-
|align=left| '''[[Blue Yonder EZ Harvard|EZ Harvard]]'''
|align=center| {{avyear|2002}}
|align=center| 1
|align=left| [[AT-6 Texan|Harvard]] replica
|-
|-
|align=left| '''[[Blue Yonder EZ Fun Flyer|EZ Fun Flyer]]'''
|align=center| {{avyear|2011}}
|align=center| 1
|align=left| Inspired by the [[Ultraflight Lazair]]
|-
|}

==Merlin Manufacturers==
[[File:Macair Merlin GT C-ICQE 01.JPG|thumb|right|a 1990 model [[Blue Yonder Merlin|Merlin GT]] built by Macair Industries]]

Blue Yonder is the fourth manufacturer of the Merlin design. Companies who have built the Merlin were:<ref name="COPA1" /><ref name="Armstrong">Armstrong, Kenneth. ''Choosing Your Homebuilt - the one you will finish and fly! Second Edition''. Goleta, CA: Butterfield Press, 1993, pp. 195–201. ISBN 0-932579-26-4.</ref>

{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Merlin Manufacturers'''
|- style="background:#efefef;"
! Company
! Location
! Dates
! Ownership
|-
|align=left| '''Macair Industries'''
|align=center| [[Baldwin, Ontario]], [[Canada]]
|align=center| 1988-91
|align=left| John Burch
|-
|-
|align=left| '''Malcolm Aircraft'''
|align=center| [[Michigan]], [[United States|USA]]
|align=center| 1991-92
|align=left| John Burch
|-
|-
|align=left| '''Merlin Aircraft'''
|align=center| [[Michigan]], [[United States|USA]]
|align=center| 1993-96
|align=left| 
|-
|-
|align=left| '''Blue Yonder'''
|align=center| [[Indus, Alberta]]
|align=center| 1996–present
|align=left| Wayne Winters
|-
|}

==References==
{{Reflist}}

==External links==
{{commons category-inline|Blue Yonder Aviation}}

{{Blue Yonder Aviation}}

[[Category:Aircraft manufacturers of Canada]]
[[Category:Aviation in Canada]]