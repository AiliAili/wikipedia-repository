{{Infobox Journal
| title = Journal of Endocrinology
| italic title = force
| cover = 
| editor = Sofianos Andrikopoulos
| discipline = [[Endocrinology]]
| abbreviation = J. Endocrinol.
| publisher = [[Bioscientifica]]  on behalf of the [[Society for Endocrinology]]
| country = 
| frequency = Monthly
| history = 1939-present
| openaccess = 
| license =
| impact = 4.498
| impact-year = 2015
| website = http://joe.endocrinology-journals.org/
| link1 = http://joe.endocrinology-journals.org/content/current
| link1-name = Online access
| link2 = http://joe.endocrinology-journals.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 01754564
| LCCN = 64005925
| CODEN = JOENAK
| ISSN = 0022-0795
| eISSN = 1479-6805
}}
The '''''Journal of Endocrinology''''' is a monthly [[peer-reviewed]] [[scientific journal]] that publishes original research articles, reviews and commentaries. Its focus is on endocrine physiology and [[metabolism]], including [[hormone]] secretion, hormone action and biological effects. The journal considers [[basic science|basic]] and [[translational research|translational studies]] at the [[Organ (anatomy)|organ]] and [[organism|whole organism]] level.

''Journal of Endocrinology'' is published by [[Bioscientifica]] on behalf of the [[Society for Endocrinology]]. It is also an official journal of the [[European Society of Endocrinology]] and the [[Endocrine Society of Australia]]. The [[editor-in-chief]] is Dr Sofianos Andrikopoulos ([[University of Melbourne]]). According to the ''[[ISI Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.498 and is now the top basic science journal dedicated to endocrinology.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Endocrinology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== History ==

The ''Journal of Endocrinology'' was conceived by Sir [[Charles Dodds]] Bart FRS (the founding editor-in-chief), Sir [[Frank George Young|Frank Young]] FRS, Sir Alan Parkes FRS and [[Solly Zuckerman, Baron Zuckerman|Lord Solly Zuckerman]] OM KCB FRS in 1937. The first issue was published in 1939 (it took two years to process the papers from draft manuscript to print) and contained 45 [[scholarly paper|research articles]]. By 1946, five issues of the ''Journal of Endocrinology'' had been published.<ref name="endocrinology.org">http://www.endocrinology.org/about/history.html</ref>

In February 1946, 22 previous contributors unanimously resolved to form the [[Society for Endocrinology]] and invited all previous authors to be founding members. ''Journal of Endocrinology'' editorial board member Alan Parkes was elected as the Society’s first Chairman.<ref name="endocrinology.org"/>

From 1946, the number of issues that the journal published gradually increased. From 1953 to 1960 there were between five and seven issues published each year, and from 1961 to 1965 there were eight to nine issues. Since 1966, ''Journal of Endocrinology'' has been published monthly.<ref>http://joe.endocrinology-journals.org/content/by/year</ref>
The technological explosion of the 1970s and 1980s, exemplified by the development of [[recombinant DNA]] techniques, [[DNA sequencing]] and the invention of [[polymerase chain reaction|PCR]], resulted in an increase in research output in the areas of molecular and genetic [[endocrinology]]. In response to this, in 1988 a sister journal of the ''Journal of Endocrinology'' was founded, entitled ''[[Journal of Molecular Endocrinology]]''.<ref name="dx.doi.org">{{cite journal | doi=10.1530/JOE-12-0135 | volume=213 | title=A new era for the Journal of Endocrinology and Journal of Molecular Endocrinology | journal=Journal of Endocrinology | pages=207–208}}</ref>

In 2006, the ''Journal of Endocrinology'' was adopted as an official journal of the European Society of Endocrinology and, in 2014, of the Endocrine Society of Australia. 
<ref>{{cite journal | doi=10.1530/EJE-07-0802 | volume=158 | title=Editorial | journal=European Journal of Endocrinology | pages=1}}</ref><ref>https://www.endocrinesociety.org.au/downloads/esa_news_april2014v5Final.pdf</ref>

The regular use of [[molecular biology]] methods in work published in ''Journal of Endocrinology'', as well as its molecular-focused sister journal, often resulted in a blurred line between the subject areas covered. Consequently, in 2011 it was decided by the Publications Committee of the Society for Endocrinology that the two journals would have a single joint [[editorial board]]. This came into being at the start of 2012. While papers would still be submitted to one or other of the journals, the senior editors would have the opportunity to suggest that manuscripts be transferred between publications.<ref name="dx.doi.org"/>

'''Recent past editors-in-chief:'''
* 1975-1980: Bernard Donovan
* 1981-1984: Alf Cowie
* 1985-1992: Gavin Vinson
* 1993-1999: Alan McNeilly
* 2000-2004: Steve Hillier
* 2005-2008: Julian Davis
* 2009-2015: Adrian Clark
* 2015–present: Sofianos Andrikopoulos

== Online access ==

The ''Journal of Endocrinology'' was first published online in September 1997 in PDF format. From October 2004, the online offering was extended to include the [[HTML]] full text version of articles.
All peer-reviewed editorial and review content is free to access from publication. Research articles are under access control for the first 12 months before being made available to the public. During the first 12 months the content is accessible for those at subscribing institutions and members of the Society for Endocrinology and the European Society of Endocrinology.

The journal automatically deposits articles in [[PubMed Central]] on behalf of authors who are funded by the [[National Institutes of Health]] (NIH), for release 12 months from publication, enabling authors to comply with the [[NIH Public Access Policy]].
''Journal of Endocrinology'' is a [[hybrid open access journal]], offering a ‘gold’ [[open access]] option whereby authors can pay an [[article processing charge]] upon acceptance to have their article made freely available online immediately upon publication. These articles are automatically deposited into PubMed Central.

All journal content is included in the [[World Health Organization]]’s [[HINARI]] scheme, which offers free or reduced-price access for institutions in certain [[developing countries]] to [[health]]-related research.

The full archive of published content from 1939 to 1997 is available to purchase by institutions as part of the Society for Endocrinology Archive.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[CAB Abstracts]]
* [[Chemical Abstracts]]
* [[Current Contents]] (Life Sciences)
* [[EMBASE|EMBASE/Excerpta Medica]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Biosis]]
* [[Science Citation Index]]
* [[Academic Search Premier]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.081.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Endocrinology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== External links ==
* {{Official website|http://joe.endocrinology-journals.org/}}
* [https://www.endocrinesociety.org.au/ Endocrine Society of Australia]
* [http://www.ese-hormones.org/ European Society of Endocrinology]
* [http://www.endocrinology.org/ Society for Endocrinology]

== References ==
{{Reflist}}

[[Category:Endocrinology journals]]
[[Category:Publications established in 1939]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Bioscientifica academic journals]]
[[Category:Academic journals associated with learned and professional societies]]