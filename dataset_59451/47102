{{Use mdy dates|date=November 2016}}
{{Infobox media franchise
|title         = ''Cloudy with a Chance of Meatballs''
|image         = Cloudy with a Chance of Meatballs (film) logo.svg
|creator       = [[Judi Barrett]]
|origin        = ''[[Cloudy with a Chance of Meatballs]]'' (1978)
|owner         = [[Sony Pictures Animation]]
|films         = {{plainlist|
* {{nowrap|''[[Cloudy with a Chance of Meatballs (film)|Cloudy with a Chance of Meatballs]]''}} (2009)
* {{nowrap|''[[Cloudy with a Chance of Meatballs 2]]''}} (2013)
}}

|shorts    = {{plainlist|
* ''[[Cloudy with a Chance of Meatballs 2#Release|Super Manny]]'' (2013)
* ''[[Cloudy with a Chance of Meatballs 2#Release|Earl Scouts]]'' (2013)
* ''[[Cloudy with a Chance of Meatballs 2#Release|Steve's First Bath]]'' (2014)
* ''[[Cloudy with a Chance of Meatballs 2#Release|Attack of the 50-Foot Gummi Bear]]'' (2014)
}}
|tv     = ''[[Cloudy with a Chance of Meatballs (TV series)|Cloudy with a Chance of Meatballs]]'' (2017)
|vgs           = {{plainlist|
* ''[[Cloudy with a Chance of Meatballs (video game)|Cloudy with a Chance of Meatballs]]''
* ''[[#Video games|Cloudy with a Chance of Meatballs 2]]''
* ''[[#Video games|Foodimal Frenzy]]''
}}
}}

'''''Cloudy with a Chance of Meatballs''''' is a [[media franchise]] produced by  [[Sony Pictures Animation]] based on the [[Cloudy with a Chance of Meatballs|book of the same name]] by [[Judi Barrett]].

== Feature films ==

=== ''Cloudy with a Chance of Meatballs'' (2009) ===
{{Main article|Cloudy with a Chance of Meatballs (film)}}

Flint Lockwood invents a machine that transforms water into food called the "Flint Lockwood Diatonic Super Mutating Dynamic Food Replicator" (FLDSMDFR). Flint turns on the FLDSMDFR in his laboratory, but ends up overloading the house's electrical supply. He then decides to power the machine by hooking it up to a nearby power plant. When he turns the machine back on, it ends up rocketing through town, causing various damage, ultimately shooting up into the sky. While recovering from his failure, Flint meets Samantha "Sam" Sparks, a weather intern whose big break was foiled by Flint's actions. Their conversation is cut short when rainbow colored clouds float over the town and begin to rain cheeseburgers.

=== ''Cloudy with a Chance of Meatballs 2'' (2013) ===
{{Main article|Cloudy with a Chance of Meatballs 2}}

Upon arriving back at Swallow Falls, they notice that a jungle-like environment made of food has overgrown the island. Tim stays behind while Flint and the others investigate, finding a vast habitat of living food animals called foodimals and meet a cute strawberry nicknamed Barry. Tim, searching for sardines at his abandoned tackle shop, encounters a family of humanoid pickles and bonds with them by fishing. Chester discovers that Flint allowed his friends to join him on the mission, so he travels to the island with Barb, chagrined and determined to separate them, and he arrives just in time to save them from a Cheespider. Flint then finds his old lab and invents a device to find the FLDSMDFR. After escaping a Tacodile attack, Sam notices that the foodimal was protecting its family, and begins to suspect Chester is up to no good. Sam attempts to convince Flint to spare the foodimals, but Flint is intent on impressing Chester. Sam leaves in anger, and Flint's other companions go with her. In the jungle, Manny confirms Sam's suspicions when he reverses the Live Corp logo to reveal the "Live" as "Evil" spelled backwards. In addition, Sam proves that the foodimals mean no harm by taming a Cheespider. They also learn that the foodimals had known the truth about Live Corp before. Upon realizing Chester's intentions, the group is then ambushed by Evil Corp employees.

==Television series==
{{Main|Cloudy with a Chance of Meatballs (TV series)}}
On October 9, 2014, [[DHX Media]] announced that it will develop and produce a television series based on the film franchise, titled ''Cloudy with a Chance of Meatballs''.<ref name=DHXMTakingToTV /> The series will be traditionally animated and will consist of 26 22-minute episodes.<ref name=DHXMTakingToTV /> It will take place before the first film, showing Flint Lockwood as a high school student who dreams to become a serious scientist.<ref name=DHXMTakingToTV /> In his adventures, he will be joined by Sam Sparks, a new girl in town and the school's "wannabe" reporter, along with Flint's dad Tim, Steve the Monkey, Manny as the head of the school's audiovisual club, Earl as a school gym teacher, Brent as a baby wear model, and mayor Shelbourne.<ref name=DHXMTakingToTV /> DHX Media will handle the global television and non-US home entertainment distribution, along with worldwide merchandising rights, while Sony will distribute home entertainment in the US.<ref name=DHXMTakingToTV>{{cite press release|title=DHX MEDIA AND SONY PICTURES ANIMATION TAKING CLOUDY WITH A CHANCE OF MEATBALLS TO TELEVISION|url=http://www.dhxmedia.com/press-room/590:dhx-media-and-sony-pictures-animation-taking-cloudy-with-a-chance-of-meatballs-to-television|publisher=DHX Media|accessdate=October 10, 2014|date=October 9, 2014}}</ref> Commissioned by [[Teletoon]] in Canada, the series will air on [[Cartoon Network]] in the United States,<ref>{{cite press release|author1=Sony Pictures Animation|title=Sony Pictures Animation Announces 2017 & 2018 Slate|url=http://www.prnewswire.com/news-releases/sony-pictures-animation-announces-2017--2018-slate-300287319.html|publisher=PR Newswire|accessdate=September 25, 2016|date=June 20, 2016}}</ref> and on the [[Boomerang (TV channel)|Boomerang]] channel in other territories.<ref>{{cite press release|author1=DHX Media|title=DHX Media and Sony Pictures Animation's Cloudy With a Chance of Meatballs TV Series Goes Global|url=http://www.prnewswire.com/news-releases/dhx-media-and-sony-pictures-animations-cloudy-with-a-chance-of-meatballs-tv-series-goes-global-530629831.html|publisher=PR Newswire|accessdate=October 9, 2015|date=October 5, 2015}}</ref>

== Short films ==

=== ''Super Manny'' ===
''Super Many'' is a  short film, released in October 26, 2013.

=== ''Earl Scouts'' ===
''Earl Scouts'' is a  short film, released on  October 26, 2013.

===''Steve's First Bath''===
''Steve's First Bath'' was released on January 28, 2014.

===''Attack of the 50-Foot Gummi Bear''===
''Attack of the 50-Foot Gummi Bear'' was released on January 28, 2014.

==Cast and characters==
{| class="wikitable" width="100%" style="text-align:center"
! rowspan="2" width="16%"| Characters
! colspan="2" width="24%"| Films
! colspan="4" width="48%"| Short films
! colspan="1" width="12%"| Television series
|-
! width="12%"| ''[[Cloudy with a Chance of Meatballs (film)| Cloudy with a Chance of Meatballs]]''<br> (2009)
! width="12%"| ''[[Cloudy with a Chance of Meatballs 2]]''<br> (2013)
! width="12%"| ''Super Manny''<br> (2013)
! width="12%"| ''Earl Scouts''<br> (2013)
! width="12%"| ''Steve's First Bath''<br> (2014)
! width="12%"| ''Attack of the 50-Foot Gummi Bear''<br> (2014)
! width="12%"| ''[[Cloudy with a Chance of Meatballs (TV series)|Cloudy with a Chance of Meatballs ]]''<br> (2017-present)
|-
! Flint Lockwood
| [[Bill Hader]]<br> Max Neuwirth (young)
| Bill Hader<br> [[Bridget Hoffman]] (young)
| colspan="2" style=background-color:#D3D3D3 |
| colspan="2" | Bill Hader
| Mark Edwards
|-
! Samantha "Sam" Sparks
| colspan="3" | [[Anna Faris]]
| colspan="1" style=background-color:#D3D3D3 |
| Anna Faris
| colspan="1" style=background-color:#D3D3D3 |
| [[Katie Griffin]]
|-
! Tim Lockwood
| colspan="2" | [[James Caan]]
| colspan="4" style=background-color:#D3D3D3 |
| [[Seán Cullen]]
|-
! Steve the Monkey
| colspan="2" | [[Neil Patrick Harris]]
| colspan="2" style=background-color:#D3D3D3 |
| colspan="2" | Neil Patrick Harris
| Mark Edwards
|-
! Brent McHale
| colspan="2" | [[Andy Samberg]]
| colspan="4" style=background-color:#D3D3D3 |
| [[David Berni]]
|-
! Officer Earl Devereaux
| [[Mr. T]]
| [[Terry Crews]]
| colspan="1" style=background-color:#D3D3D3 |
| Terry Crews
| colspan="2" style=background-color:#D3D3D3 |
| [[Clé Bennett]]
|-
! Manny
| colspan="3" | [[Benjamin Bratt]]
| colspan="3" style=background-color:#D3D3D3 |
| [[Patrick McKenna]]
|-
! Calvin "Cal" Devereaux
| [[Bobb'e J. Thompson]]
| [[Khamani Griffin]]
| colspan="4" style=background-color:#D3D3D3 |
| {{N/A| ''Stock baby voices''}}
|-
! Patrick Patrickson
| colspan="2" | [[Al Roker]]
| colspan="5" style=background-color:#D3D3D3 |
|-
! Mayor Shelbourne
| [[Bruce Campbell]]
| colspan="5" style=background-color:#D3D3D3 |
| Seán Cullen
|-
! Joseph "Joe" Towne
| [[Will Forte]]
| {{N/A| ''Silent cameo''}}
| colspan="4" style=background-color:#D3D3D3 |
| (Unknown voice actor)
|-
! Fran Lockwood
| [[Lauren Graham]]
| colspan="5" style=background-color:#D3D3D3 |
| {{N/A| ''Painting''}}
|-
! Chester V
| colspan="1" style=background-color:#D3D3D3 |
| Will Forte
| colspan="5" style=background-color:#D3D3D3 |
|-
! Barb
| colspan="1" style=background-color:#D3D3D3 |
| [[Kristen Schaal]]
| colspan="5" style=background-color:#D3D3D3 |
|-
! Barry the Strawberry
| colspan="1" style=background-color:#D3D3D3 |
| [[Cody Cameron]]
| colspan="1" style=background-color:#D3D3D3 |
| Cody Cameron
| colspan="2" style=background-color:#D3D3D3 |
| {{N/A| ''Character is mute''}}
|-
! Dill Pickles
| colspan="1" style=background-color:#D3D3D3 |
| Cody Cameron
| colspan="1" style=background-color:#D3D3D3 |
| Cody Cameron
| colspan="3" style=background-color:#D3D3D3 |
|-
! Gil
| colspan="6" style=background-color:#D3D3D3 |
| Patrick McKenna
|-
! Old Rick
| colspan="6" style=background-color:#D3D3D3 |
| Seán Cullen
|}

==Reception==

===Box office performance===
Cloudy with a Chance of Meatballs is the [[List of highest-grossing animated films#Highest-grossing animated franchises and film series|17th highest-grossing animated franchise of all time.]]

{| class="wikitable" width="99%" border="1" style="text-align: center;"
! rowspan="2" | Film
! rowspan="2" | Release date
! colspan="3" | Box office gross
! colspan="2" text="wrap" | Box office ranking
! rowspan="2" | Budget
! rowspan="2" | Reference
|-
! North America
! Other territories
! Worldwide
! All time North America
! All time worldwide
|-
| ''Cloudy with a Chance of Meatballs''
| September 18, 2009
| $124,870,275 
| $118,135,851
| $243,006,126 
| #447
| #510
| $100,000,000
|<ref>{{cite web|title=Cloudy with a Chance of Meatballs|url=http://www.boxofficemojo.com/movies/?id=cloudywithachanceofmeatballs.htm|publisher=Box Office Mojo|accessdate=November 5, 2016}}</ref>
|-
| ''Cloudy with a Chance of Meatballs 2''
| September 27, 2013
| $119,793,567 
| $154,532,382 
| $274,325,949 
| #474
| #433
| $78,000,000
|<ref>{{cite web |url=http://www.boxofficemojo.com/movies/?id=cloudy2.htm|title=meatballs 2 (2013) |work=Box Office Mojo |accessdate=November 6, 2016}}</ref>
|-
! colspan="2" | Total
! ${{val|{{#expr:124870275+119793567}}|fmt=commas}}
! ${{val|{{#expr:118135851+154532382}}|fmt=commas}}
! ${{val|{{#expr:243006126+274325949}}|fmt=commas}}
!
!
! ${{val|{{#expr:100000000+78000000}}|fmt=commas}}
!
|}

===Critical response===
{| class="wikitable" width="99%" border="1" style="text-align: center;"
! Film
! Rotten Tomatoes
! Metacritic
|-
| ''Cloudy with a Chance of Meatballs ''
| 87% (139 reviews)<ref>{{cite web|title=Cloudy with a Chance of Meatballs (2009)|url=https://www.rottentomatoes.com/m/1196077-cloudy_with_a_chance_of_meatballs|publisher=Rotten Tomatoes|accessdate=November 6, 2016}}</ref>
|66 (24 reviews)<ref>{{cite web|title=Cloudy|url=http://www.metacritic.com/movie/cloudy-with-a-chance-of-meatballs|publisher=Metacritic|accessdate=November 6, 2016}}</ref>
|-
| ''Cloudy with a Chance of Meatballs 2''
| 70% (118 reviews)<ref>{{cite web |url=https://www.rottentomatoes.com/m/cloudy_with_a_chance_of_meatballs_2_2013/|title=Cloudy with a Chance of Meatballs 2 |website=Rotten Tomatoes |accessdate=November 6, 2016 }}</ref>
| 59 (31 reviews)<ref>{{cite web |url=http://www.metacritic.com/movie/cloudy-with-a-chance-of-meatballs-2 |title=Meatballs 2 |website=Metacritic |accessdate=November 6, 2016}}</ref>
|}

== References ==
{{Reflist|30em}}

== External links ==
* {{IMDb title|0844471|Cloudy with a Chance of Meatballs}}
* {{IMDb title|tt1985966|Cloudy with a Chance of Meatballs 2}}

{{Portal bar|Cartoon|2000s|Animation}}
{{Sony Pictures Animation}}

[[Category:Film series]]
[[Category:English-language films]]
[[Category:Fantasy films by series]]
[[Category:Animated film series]]
[[Category:Comedy films by series]]
[[Category:Children's film series]]
[[Category:Media franchises]]
[[Category:Sony Pictures franchises]]
[[Category:2009 introductions]]
[[Category:Cloudy with a Chance of Meatballs (franchise)| ]]

[[it:Cloudy with a Chance of Meatballs (franchise)]]