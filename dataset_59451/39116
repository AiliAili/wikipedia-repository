{{good article}}
{{Infobox cocktail
| sourcelink  = Caesar
| name        = Caesar
| image       = Caesar_Cocktail.JPG
| type        = cocktail
| flaming     = 
| vodka       = yes
| served      = on the rocks
| garnish     = stalk of [[celery]] and wedge of [[lime (fruit)|lime]]
| drinkware   = highball
| ingredients = *6 oz. Clamato Juice
*1–1½ oz. vodka
*2 dashes hot sauce
*4 dashes Worcestershire sauce
*Celery salt
*Freshly ground pepper
*Lime wedge
*1 crisp celery stalk
| prep        = Rim glass with celery salt, and a lime wedge.
}}
{{Canadian cuisine}}
A '''Caesar''' or '''Bloody Caesar''' is a [[cocktail]] created and primarily consumed in [[Canada]].  It typically contains [[vodka]], a caesar mix (a blend of tomato juice and [[clam broth]]), [[hot sauce]], and [[Worcestershire sauce]], and is served with ice in a large, [[celery salt]]-[[glass rimmer|rimmed]] glass, typically garnished with a stalk of celery and wedge of [[lime (fruit)|lime]]. What distinguishes it from a [[Bloody Mary (cocktail)|Bloody Mary]] is the inclusion of clam broth. The cocktail may also be contrasted with the [[Michelada]], which has similar flavouring ingredients but uses beer instead of vodka.

It was invented in [[Calgary]], [[Alberta]] in 1969 by restaurateur Walter Chell to celebrate the opening of a new Italian restaurant in the city.  It quickly became a popular mixed drink within Canada where over 350 million Caesars are consumed annually and it has inspired numerous variants. However, the drink remains virtually unknown elsewhere.

==Origin==
The Caesar was invented in 1969 by restaurant manager Walter Chell of the Calgary Inn (today the [[Westin Hotels|Westin Hotel]]) in [[Calgary]], [[Alberta]], [[Canada]].  He devised the cocktail after being tasked to create a signature drink for the Calgary Inn's new Italian restaurant.<ref>{{cite web |url=http://www.clamato.com/en/history.php?shead=a |title=History of Clamato |publisher=Motts LLP |accessdate=2014-01-24}}</ref>  He mixed [[vodka]] with clam and tomato juice, [[Worcestershire sauce]], and other spices,<ref name="CBC40th">{{cite news |url=http://www.cbc.ca/news/canada/calgary/story/2009/05/13/bloody-caesar-calgary-cocktail-drink-anniversary.html |title=Calgary's Bloody Caesar hailed as nation's favourite cocktail |publisher=Canadian Broadcasting Corporation |date=2009-05-13 |accessdate=2011-03-18}}</ref> creating a drink similar to a [[Bloody Mary (cocktail)|Bloody Mary]] but with a uniquely spicy flavour.<ref name="Cocktailbook">{{cite book |last=Harrington |first=Paul |last2=Moorhead |first2=Laura |title=Cocktail: The Drinks Bible for the 21st Century |publisher=Viking Penguin |year=1998 |isbn=0-670-88022-1 |pages=68–69}}</ref>

Chell said his inspiration came from Italy.  He recalled that in [[Venice]], they served [[Spaghetti alle vongole]], spaghetti with tomato sauce and clams.  He reasoned that the mixture of clams and tomato sauce would make a good drink, and mashed clams to form a "nectar" that he mixed with other ingredients.<ref name="TS25th">{{cite news |last=Naccarato |first=Michael |title=Bloody Caesar Canada's cocktail It was invented 25 years ago in Calgary and 'took off like a rocket' |work=Toronto Star |date=1994-05-11 |page=C3}}</ref>

According to Chell's granddaughter, his Italian ancestry led him to call the drink a "Caesar".<ref name="CBC40th" />  The longer name of "Bloody Caesar" is said to differentiate the drink from the Bloody Mary, but Chell said it was a regular patron at the bar who served as the inspiration.  During the three months he spent working to perfect the drink, he had customers sample it and offer feedback.  One regular customer, an Englishman, who often ordered the drink said one day "Walter, that's a damn good bloody Caesar".<ref name="TS25th" />

==Popularity==
Chell said the drink was an immediate hit with the restaurant's patrons, claiming it "took off like a rocket".<ref name="TS25th" /> Within five years of its introduction, the Caesar had become Calgary's most popular mixed drink.<ref>{{cite news |last=Haeseker |first=Fred |url=https://news.google.com/newspapers?id=q25kAAAAIBAJ&sjid=YH0NAAAAIBAJ&pg=3373,2896738 |title=Alberta drinkers take whisky first, vodka second |work=Calgary Herald |date=1974-12-31 |accessdate=2011-03-18 |page=26}}</ref> It spread throughout Western Canada, then to the east.<ref name="TS25th" />  Coinciding with its 40th anniversary, a petition was launched in 2009 in the hopes of having the Caesar named the nation's official mixed drink.<ref name="WFP40">{{cite news |last=Graveland |first=Bill |url=http://www.winnipegfreepress.com/canada/we-stand-on-guard----for-our-favourite-cocktail-44970142.html |title=We stand on guard -- for our favourite cocktail |work=Winnipeg Free Press |date=2009-05-14 |accessdate=2011-03-18 |page=A2}}</ref> In Calgary, Mayor [[Dave Bronconnier]] celebrated the drink's anniversary by declaring May 13, 2009 as Caesar Day in the city.<ref>{{cite news |last=Doody |first=Kelly |title=Page Six |work=Calgary Sun |date=2009-05-14 |page=6}}</ref>

The [[Mott's]] company was independently developing [[Clamato]], a mixture of clam and tomato juices, at the same time the Caesar was invented.  Sales of Clamato were initially slow: Mott's sold only 500 cases of Clamato in 1970,<ref name="Cocktailbook" /> but sales consistently increased after the company's distributors discovered Chell's drink.<ref name="Tribune">{{cite news |last=Lazarus |first=George |title=Clamato and vodka: 'the best bloody drink in town' |work=Chicago Tribune |date=1978-06-30 |page=E9}}</ref> By 1994, 70% of Mott's Clamato sales in Canada were made to mix Caesars, while half of all Clamato sales were made in Western Canada.<ref name="TS25th" /> Motts claims that the Caesar is the most popular mixed drink in Canada, estimating that over 350 million Caesars are consumed every year.<ref name="CBCHailCaesar">{{cite news |last=Lau |first=Andree |url=http://www.cbc.ca/consumer/foodbytes/2009/05/hail-caesar.html |title=Hail Caesar! |publisher=Canadian Broadcasting Corporation |date=2009-05-14 |accessdate=2011-03-19}}</ref>

Outside Canada, the Caesar is virtually unknown.  In the United States, it is typically only available at bars along the [[Canada–United States border]].<ref name="TS25th" /> Elsewhere, bartenders will frequently offer a Bloody Mary in its place.<ref name="CH40th">{{cite news |last=Remington |first=Robert |title=Spicy beverage still causing a stir |work=Calgary Herald |date=2009-05-13 |page=A1, A6}}</ref> In Europe, the drink can be found wherever there are higher concentrations of Canadians.<ref name="FFT40">{{cite web |last=Byrne |first=Ciara |url=http://fftimes.com/node/220614 |title=A Caesar celebration: Saucy Canadian cocktail hits the big 4-0 |work=Fort Frances Times |date=2009-03-12 |accessdate=2011-03-26}}</ref> The drink's anonymity outside Canada has come in spite of concerted marketing efforts.<ref name="Tribune" />  Producers of clam-tomato juices have speculated that their beverages have been hampered by what they describe as the "clam barrier". They have found that consumers in the United States fear that there is too much clam in the beverages.<ref>{{cite web |last=Thompson |first=Stephanie |url=http://findarticles.com/p/articles/mi_m0BDW/is_n3_v39/ai_20165077/?tag=content;col1 |title=Brand builders: juicing Clamato sales - Motts USA does marketing research to boost Clamato sales |work=Brandweek |publisher=CBS Business Network |date=1998-01-19 |accessdate=2011-03-26}}.</ref> 

While Motts Clamato continues to be synonymous with the cocktail, other producers have begun offering alternative caesar mixes. Walter Caesar (named in honor of Chell) was launched in 2013 to offer an 'all natural' alternative to [[Clamato]]. <ref>{{cite news |url=http://www.theglobeandmail.com/report-on-business/small-business/sb-growth/the-challenge/cocktail-fans-build-a-better-caesar-mix/article30649044/ |title=Cocktail Fans Build a Better Caesar... |work=Globe & Mail |date=2016-07-11 |accessdate=2016-11-18}}</ref> Walter Caesar also became the first caesar mix in Canada to be approved by [[Ocean Wise]] by using ocean-friendly clam juice from the North Atlantic. <ref>{{cite web |url=http://www.aquablog.ca/2015/02/walter-caesar-now-ocean-wise-approved/ |title=Walter Caesar Now Ocean Wise Approved|date=2016}}</ref>

The Caesar is popular as a [[hangover]] "cure",<ref>{{cite news |title=Bloody good hangover cure |work=Toronto Star |date=2004-04-10 |page=H13}}</ref> though its effectiveness has been questioned.<ref>{{cite news |last=Haggarty |first=Elizabeth |url=http://www.thestar.com/living/health/article/923846--the-two-most-effective-ingredients-to-treat-a-hangover |title=The two most effective ingredients to treat a hangover |work=Toronto Star |date=2011-01-18 |accessdate=2011-03-26}}</ref>  A study by the [[University of Toronto]] released in 1985 showed that drinking a Caesar when taking [[aspirin]] could help protect a person's stomach from the damage aspirin causes.<ref>{{cite news |url=https://news.google.com/newspapers?id=TgVFAAAAIBAJ&sjid=27oMAAAAIBAJ&pg=3886,2493050 |title=Take two drinks... |work=Windsor Star |date=1985-10-23 |accessdate=2011-03-26}}</ref>

==Preparation and variants==
Basic preparation of a Caesar follows the "one, two, three, four" rule.  The recipe calls for 1–1½ oz of vodka, two dashes of hot sauce, three dashes of salt and pepper, four dashes of Worcestershire sauce and topped with 4–6 oz of caesar mix and served with ice.<ref name="WFP40" />  The ingredients are poured into a glass rimmed with celery salt or a mixture of salt and pepper and garnished with a celery stalk and lime.<ref name="CH40th" /> The Caesar is an unusual drink in that it can be mixed in bulk and stored for a period of time before drinking.<ref name="Cocktailbook" />

Though it was not one of Chell's original ingredients, [[Tabasco sauce]] is a frequent addition,<ref name="TS25th" /> as is [[horseradish]].<ref name="Cocktailbook" />  Vodka is occasionally replaced with [[gin]], [[tequila]] or [[rum]], though the Clamato may not be substituted.<ref name="Cocktailbook" />  A variant that replaces vodka with [[beer]] is commonly called a "Red Eye",<ref name="CBCHailCaesar" /> and one without alcohol is a "Virgin Caesar".<ref>{{cite web |url=http://www.eatrightontario.ca/en/viewdocument.aspx?id=117 |title=Alcohol and nutrition |publisher=Government of Ontario |accessdate=2011-03-20}}</ref>  The Toronto Institute of Bartending operates a "Caesar School" in various locations across Canada that teaches bartenders how to mix several variants of the drink.<ref>{{cite web |url=http://www.torontobartending.com/index/caesarschool |title=Caesar School |publisher=Toronto Institute of Bartending |accessdate=2011-03-26}}</ref>

Mott's holds an annual "Best Caesar in Town" competition as part of the Prince Edward Island International Shellfish Festival.<ref>{{cite web |url=http://www.peishellfish.com/competitions-and-championships/motts-clamato-best-caesar-in-town-contest.html |title=Mott’s Clamato Best Caesar in Town Contest |publisher=Prince Edward Island International Shellfish Festival |accessdate=2011-03-20}}</ref>  Contests held across Canada to celebrate the cocktail's 40th anniversary in 2009 encouraged variants that featured the glass rimmed with [[Tim Hortons]] coffee grinds, Caesars with [[maple syrup]] and Caesars with bacon-infused vodka.<ref name="CH40th" />

==See also==
{{portal|Liquor}}
* [[List of cocktails]]

==References==
{{Commons category|Caesar (cocktail)}}
{{reflist|2}}

[[Category:Cocktails with vodka]]
[[Category:Canadian alcoholic drinks]]
[[Category:1969 introductions]]
[[Category:Canadian inventions]]
[[Category:Clam dishes]]
[[Category:Canadian cuisine]]
[[Category:Canadian drinks]]