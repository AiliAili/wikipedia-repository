{{good article}}
{{Infobox storm
|event=1947 Sydney hailstorm
|image=1947 Sydney hailstorm boat.jpg|235px|A boat at Rose Bay in water being churned by the hailstones.
|caption=A boat at [[Rose Bay, New South Wales|Rose Bay]] in water which is being churned by the hailstones.
|formed=10:00&nbsp;am, 1 January 1947<br/>Over [[Blue Mountains (Australia)|Blue Mountains]]
|dissipated=3:30&nbsp;pm, 1 January 1947<br/>East of [[Bondi, New South Wales|Bondi]], offshore
|location=[[Sydney]]
|damages=[[Pound sterling|GB£]]750,000 (est., 1947)<br/>[[Australian dollar|A$]]45&nbsp;million (est., 2007)
|show=no
}}
The '''1947 Sydney hailstorm''' was a natural disaster which struck [[Sydney]], [[Australia]], on 1 January 1947. The [[storm cell]] developed on the morning of New Year's Day, a public holiday in Australia, over the [[Blue Mountains (Australia)|Blue Mountains]], hitting the city and dissipating east of [[Bondi, New South Wales|Bondi]] in the mid-afternoon. At the time, it was the most severe storm to strike the city since recorded observations began in 1792.<ref name="WH94"/><ref name="NE23">Newman (1947), p. 23.</ref>

The high humidity, temperatures and weather patterns of Sydney increased the strength of the storm. The cost of damages from the storm were, at the time, approximately [[Pound sterling|GB£]]750,000 ([[United States dollar|US$]]3&nbsp;million); this is the equivalent of around [[Australian dollar|A$]]45&nbsp;million in modern figures.<ref name="NYT">Reuters (1947), p. 4.</ref> The [[supercell]] dropped hailstones larger than {{convert|8|cm}} in diameter,<ref name="WH98">Whitaker (2005), p. 98.</ref><ref name="MJLEE">Lee, ''et al.'' (2000), p. 579.</ref> with the most significant damage occurring in the [[Sydney central business district|central business district]] and [[Eastern Suburbs (Sydney)|eastern suburbs]] of Sydney.<ref name="WH95">Whitaker (2005), p. 95.</ref><ref name="NE26"/>

The event caused around 1000 injuries, with between 200 and 350 people requiring hospitalisation or other medical attention, predominantly caused by broken glass shards.<ref name="WH98"/><ref name="EMA07">Emergency Management Australia (2007).</ref> The majority of severe injuries reported were suffered by people on Sydney's beaches, where many were without shelter.<ref name="WH94">Whitaker (2005), p. 94.</ref> The size of the hailstones were the largest seen in Sydney for 52 years, until the [[1999 Sydney hailstorm]] caused A$1.7 billion in insured damage in becoming the costliest natural disaster in Australian history.<ref name="MJLEE"/>

==Conditions and climatology==
[[File:1947 Baro Map.JPG|thumb|right|240px|The [[Barometer|barometric]] map for 9:00&nbsp;am, from Newman's [[Bureau of Meteorology]] report of the storm.]]
During the spring and summer, conditions along the east coast of [[Australia]] are highly conducive for the formation of hailstorms. The variation of air temperature in the atmosphere; with warm and humid air close to the ground and colder air above it causes instability, and the cold upper atmosphere temperatures allow the precipitation to fall in solid form as hailstones.<ref name="WH93">Whitaker (2005), p. 93.</ref> Since records began in 1791, hailstorms in the month of January form approximately 13% of the total number of hailstorms in the [[Geography of Sydney#Urban structure|Sydney metropolitan area]], and over 15% of all events with 'large hail'.<ref name="IJC">Schuster, ''et al.'' (2005a), pp. 1641–1643.</ref>

Hailstorms have a history of significant damage in Australia. Since records on insured losses began in 1967, four hailstorms&mdash;Sydney in 1986, [[1990 Sydney hailstorm|1990]] and [[1999 Sydney hailstorm|1999]], as well as Brisbane in 1985&mdash;have featured on the top ten list of most insured damages caused by a single Australian natural disaster. Hailstorms caused more than 30% of all insured damages inflicted as a result of natural disasters in Australia during this period, and around three quarters of all hailstorm damage has occurred in [[New South Wales]].<ref name="NH1">Schuster, ''et al.'' (2005b), p. 1.</ref>

The conditions on New Year's Day, 1947 were [[Meteorology|meteorologically]] sound for the formation of a storm. The day was hot and humid, with the maximum temperature recorded during the day being {{convert|32.7|C|lk=on}} and [[humidity]] reaching 73%.<ref name="NE27">Newman (1947), p. 27.</ref> Many Sydneysiders travelled to the [[beach]]es along the coastline to benefit from the afternoon sea breeze. The general weather pattern for Sydney in summer is movement from the west to the east&mdash;from over the Blue Mountains to across the city and into the [[Tasman Sea]].<ref name="WH94" />

==Progression of the storm==
[[File:1947 Sydney hailstorm map.svg|thumb|right|240px|Path of the centre of the storm over the Sydney metropolitan region.]]
Developing from the Blue Mountains to the south-west of Sydney in the morning of 1 January 1947, the storm cell was first identified at 10:00&nbsp;am by weather observers at [[Mascot, New South Wales|Mascot]].<ref name="NE26">Newman (1947), p. 26.</ref> The formation of storms in this region is not unusual, especially given the hot and humid conditions at ground level which causes atmospheric instability. However, the [[Bureau of Meteorology]] reported that the formation of the storm was different from most others, describing how "the underpart of the [[cloud]] was [[mottle]]d and serrated or [[curtain]]ed, rather than [[wikt:mammilated|mammilated]], and looked angry black, while false [[Cirrus cloud|cirrus tufts]] were discernible at the top".<ref name="WH94"/><ref name="NE23" />

The storm cell dropped hailstones the size of billiard balls across the south-western suburbs of Sydney. It moved directly over [[Liverpool, New South Wales|Liverpool]] at 2:25&nbsp;pm, heading in a north-east direction before slowly bending its path and travelling almost due east as it passed over the southern part of the central business district.<ref name="NE26"/> "Large explosion-like sounds", presumed to be thunder by the Bureau, were heard around the [[Sydney Harbour Bridge]].<ref name="WH96">Whitaker (2005), 96.</ref> The sounds were described by the Bureau&mdash;who were based at [[Sydney Observatory|Observatory Hill]], next to the southwest pylon of the Bridge, in 1947&mdash;as a "terrific noise" akin to "several trains&nbsp;... [[Sydney Harbour Bridge#Rail|passing over <nowiki>[the Bridge]</nowiki>]]".<ref name="NE23"/>

The storm intensified as it cut through the suburbs, and eventually unleashed its full power across the eastern suburbs of Sydney. The suburbs most seriously affected were [[Surry Hills, New South Wales|Surry Hills]], south of the central district, as well as [[Bondi, New South Wales|Bondi]] and [[Rose Bay, New South Wales|Rose Bay]] in the [[Waverley, New South Wales|Waverley]] region which were struck at around 2:40&nbsp;pm.<ref name="WH95"/><ref name="EMA07"/> The hailstorm pelted beach-goers, particularly at [[Bondi Beach]], and the situation was described by a [[Second World War]] [[veteran]] as "though [he] was back in the firing line overseas".<ref name="WH96"/> The hail in the coastal regions was described as being of similar size to a [[cricket ball]].<ref name="NE26"/>

==Aftermath==
[[File:Pitt St Tram cropped.jpg|thumb|right|240px|The trams on the eastern suburbs route, from [[Central railway station, Sydney|Central station]] to [[Circular Quay]] via [[Pitt Street]], suffered damage from the hail.]]
The most damage was caused when the storm was its most intense, over the [[Eastern Suburbs (Sydney)|eastern suburbs]] of the city. According to the [[Bureau of Meteorology]], over 5000 roofs were damaged in [[Waverley, New South Wales|Waverley]] by the lumps of hail which weighed up to {{convert|1.8|kg|abbr=on}}.<ref name="MJLEE"/><ref name="EMA07"/> No official cost total exists for the amount of damage caused by the 1947 hailstorm, however a [[Reuters]] article published in ''[[New York Times]]'' on 2 January estimated preliminary damage to be worth around US$3&nbsp;million, equivalent to GB£750,000.<ref name="NYT"/> This is approximately equal to A$45&nbsp;million in modern figures, placing it well below the costliest natural disasters in Australian history; this, given the severity of the storm cell, is attributable mainly to the relative inexpensiveness of buildings and other items of the era.<ref name="WH96"/><ref name="Hunter">Hunter (1998).</ref> More definite historical accounts exist for damage caused to certain buildings. The historic skylight which runs through the centre of the main [[Central railway station, Sydney|Central railway station]] building was smashed, and the shards reportedly fell in sizes up to {{convert|26|cm2|sqin|0|abbr=on}} on around 100 waiting passengers.<ref name="WH96"/><ref name="SMH47">{{cite news |url=http://nla.gov.au/nla.news-article18019034 |title=ICE STORM LASHES CITY AND SUBURBS |newspaper=[[The Sydney Morning Herald]] |issue=34,018 |location=New South Wales, Australia |date=2 January 1947 |accessdate=11 September 2016 |page=1 |via=National Library of Australia}}<br />{{cite news |url=http://nla.gov.au/nla.news-article18019050 |title=SCENES OF DEVASTATION AFTER FREAK HAIL HIT CITY |newspaper=[[The Sydney Morning Herald]] |issue=34,018 |location=New South Wales, Australia |date=2 January 1947 |accessdate=11 September 2016 |page=1 |via=National Library of Australia}} </ref>

Convertible cars, in fashion at the time of the storm, also sustained severe damage, mainly punctures to the soft-top roofs, and [[Trams in Sydney|trams]] that ran through the eastern suburbs at the time also suffered damage.<ref name="NE26"/><ref name="WH96"/> According to veteran [[meteorologist]] [[Richard Whitaker]], "Sydney was staggered by the enormity of the incident, as there had not been even a remotely similar storm in living memory".<ref name="WH98"/> The problems were exacerbated due to a lack of building materials available for use in repair work, a result of the Second World War which had concluded only 18 months prior. This contributed to the delays which resulted in houses still covered with only temporary [[tarpaulin]]s several years later.<ref name="WH98"/>

Most of the approximately 1000 injuries were caused by the hailstones directly striking people or from flying debris, with the latter mainly from shattered windows.<ref name="WH98"/><ref name="NE24">Newman (1947), p. 24.</ref> Of these, between 200 and 350 people required [[hospital]]isation or other medical attention, however figures vary between different sources.<ref name="WH98"/><ref name="EMA07"/> The storm struck during the afternoon of a [[public holiday]]—[[New Year's Day]]—which produced hot and humid conditions, and the [[beach]]es in the eastern suburbs were significantly populated.<ref name="WH94"/> The beach-goers were exposed to the large hail when the storm cell reached the coastline, and according to the front page report in ''[[The Sydney Morning Herald]]'' the following day, "[f]or nearly three hours, [[ambulance]] wagons travelled from the eastern suburbs beaches with the injured".<ref name="SMH47"/> The {{convert|8|cm|in|1|abbr=on}} hailstones which fell during the 1947 event were not matched in Sydney for 52 years, until the [[1999 Sydney hailstorm|1999 hailstorm]], which caused A$1.7&nbsp;billion in [[Insurance|insured]] damage&mdash;the costliest [[natural disaster]] in Australian history.<ref name="MJLEE"/>

==See also==
{{Portal|Earth sciences}}
*[[Severe storms in Australia]]
*[[Emergency management]]

==Notes==
{{Reflist|2}}

==References==
*{{cite web
 | title=Sydney, NSW: Severe Hailstorm - 1 January 1947
 | url=http://www.ema.gov.au/ema/emadisasters.nsf/c85916e930b93d50ca256d050020cb1f/b33fab90463873edca256d3300057fc1?OpenDocument
 | author=Emergency Management Australia
 | author-link=Emergency Management Australia
 | date=7 August 2007
 | publisher=Australian Government - Attorney-General's Department
 | accessdate=2007-12-09
}}
*{{cite journal
 | last = Hunter
 | first = Laraine
 |date=March 1998
 | title = Perils, Postcodes & Risk Accumulation Zones
 | journal = Natural Hazards Quarterly
 | volume = 4
 | issue = 1
 |pages=1–3
}}
*{{cite journal
 | last = Lee
 | first = Lynette
 |author2= Collings, Anne
 | year = 2000
 | title = Sydney hailstorms: the health role in the recovery process
 | journal = Medical Journal of Australia
 | volume = 173
 | issue = 11–12
 |pages=579–582
| pmid=11379494
}}
*{{cite book
 | author = Newman, Barney
 | title = Phenomenal Hailstorm with Thunderstorm, Sydney 1 January 1947
 | year = 1947
 | publisher = [[Bureau of Meteorology]]
 | location = Sydney, Australia
}}
*{{cite news
 | author = Reuters
 | author-link = Reuters
 | title = Hailstones Pelt Sydney; Damage and Injuries High
 | date = 2 January 1947
 | publisher = ''[[The New York Times]]''
 | page = 4
}}
*{{cite journal
 | last = Schuster
 | first = Sandra
 | last2 = Blong
 | first2 =Russell
 | last3 = Speer 
 | first3 = Milton
 | date = 2005a
 | title = A hail climatology of the greater Sydney area and New South Wales, Australia
 | journal = International Journal of Climatology
 | volume = 25
 | issue = 12
 |pages=1633–1650
 | publisher = [[John Wiley & Sons|Wiley]]
 | location = New York City, United States
 | doi=10.1002/joc.1199
|bibcode = 2005IJCli..25.1633S }}
*{{cite web
 | title=Characteristics of the 14 April 1999 Sydney hailstorm based on ground observations, weather radar, insurance data and emergency calls
 | url=http://www.nat-hazards-earth-syst-sci.net/5/613/2005/nhess-5-613-2005.pdf
 |format=PDF| author=Schuster, Sandra
 | last2 = Blong
 | first2 =Russell
 | last3 = Leigh
 | first3 = Roy
 | last4 = McAneney
 | first4 = John
 | date=2005b
 | publisher=Natural Hazards and Earth System Sciences
 | accessdate=2007-09-08
}}
*{{cite news
 | title=Ice Storm Lashes City and Suburbs
 | author=The Sydney Morning Herald
 | author-link=The Sydney Morning Herald
 | date=2 January 1947
 | pages=1, 4–5
}}
*{{cite book
 | author = Whitaker, Richard
 | title = Australia's Natural Disasters
 | year = 2005
 | publisher = Reed New Holland
 | location = Sydney, Australia
 | isbn = 1-877069-04-3
 | pages = 93–98
 | authorlink = Richard Whitaker
}}

==Further reading==
*{{cite journal
 | author = Batt, K.
 | author2 = Casinader, T.
 | author3 = Colquhoun, J.
 | author4 = Griffiths, D.
 |date=December 1993
 | title = Severe Thunderstorms in New South Wales: Climatology and Means of Assessing the Impact of Climate Change
 | journal = Climatic Change
 | volume = 25
 | issue = 3–4
 |pages=369–388
 | publisher = Springer
 | location = The Netherlands
 | issn = 1573-1480
 | url = http://www.springerlink.com/content/w7810061v46h3t60/fulltext.pdf
 | format=PDF
 | accessdate = 2008-04-03
 | doi=10.1007/bf01098382
 }}
*{{cite journal
 | author = Keenan, T.
 | author2 = May, P.
 | author3 = Potts, R.
 |date=September 2000
 | title = Radar Characteristics of Storms in the Sydney Area
 | journal = Monthly Weather Review
 | volume = 128
 | issue = 9
 |pages=3308–3319
 | publisher = Bureau of Meteorology Research Centre
 | location = Melbourne
 | url = http://journals.ametsoc.org/doi/full/10.1175/1520-0493%282000%29128%3C3308%3ARCOSIT%3E2.0.CO%3B2
 | accessdate = 2008-04-03
 | issn = 1520-0493
 | doi=10.1175/1520-0493(2000)128<3308:rcosit>2.0.co;2|bibcode = 2000MWRv..128.3308P }}

{{coord|33|51|35|S|151|12|40|E|source:kolossus-plwiki_type:event|display=title}}

[[Category:Natural disasters in Australia]]
[[Category:1947 in Australia|Sydney hailstorm]]
[[Category:1947 meteorology|Sydney hailstorm]]
[[Category:1947 natural disasters|Sydney hailstorm]]
[[Category:History of Sydney]]
[[Category:20th century in Sydney]]