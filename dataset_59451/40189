{{Infobox Military Structure
|name = Bhadra Fort
|partof = [[Old Ahmedabad]]
|location = [[Ahmedabad]], [[India]]
|image = BhadraFort.jpg
|caption = Bhadra Fort from inside and tower clock after renovation
|type = [[Castle]] and [[City wall]]
|coordinates = {{Coord|23|1|25|N|72|34|52|E|display=inline,title}}
|code = [[Archaeological Survey of India|ASI]] Monument N-GJ-2
|built = March 4, {{Start date|1411}}<ref name="nri"/>
|builder = [[Ahmed Shah of Gujarat|Ahmad Shah I]] of [[Muzaffarid dynasty of Gujarat|Muzaffarid dynasty]]
|materials = Stone and brick
|height = 
|used = 
|demolished =
|condition = Ruined with survival of western walls and gates
|ownership = [[Archaeological Survey of India|ASI]]
|open_to_public = Yes 
|controlledby ={{plainlist|
* [[File:Gujarat Sultanate Flag.gif|23px]] [[Gujarat Sultanate]] <small>(1411-1573)</small>
* [[Mughal Empire]]  <small>(1573-1744)</small>
* [[File:Flag of the Maratha Empire.svg|23px]] [[Mughal Empire]] and [[Peshwa]] of [[Maratha Empire]] (Joint rule)<small>(1739-1744)</small>
* [[Mughal Empire]]<small>(1744-1758)</small>
* [[File:Flag of the Maratha Empire.svg|23px]] [[Peshwa]] and [[Gaekwad dynasty]] of [[Maratha Empire]] (Joint rule)<small>(1583-1818)</small>
* [[Image:Flag of the British East India Company (1707).svg|23px]] [[British East India Company]]  <small>(1818-1858)</small>
* [[File:British Raj Red Ensign.svg|23px]] [[British Raj]]  <small>(1858-1947)</small>
* [[File:Flag of India.svg|23px]] [[India]]  <small>(1947-)</small><ref name="eamc"/>}}
|garrison = 
|current_commander = 
|commanders = 
|occupants =
|battles = [[First Anglo-Maratha War]] (1779)
|events = 
}}
[[File:Bhadra Fort.JPG|thumb|Bhadra Fort Gate]]
'''Bhadra Fort''' is situated in the [[Old Ahmedabad|walled city]] area of [[Ahmedabad]], [[India]]. It was built by [[Ahmed Shah of Gujarat|Ahmad Shah I]] in 1411. With its well carved royal palaces, mosques, gates and open spaces, it was renovated in 2014 by [[Ahmedabad Municipal Corporation]] (AMC) and [[Archaeological Survey of India]] (ASI) as a cultural centre for the city.

==Etymology==
It is believed the fort adopted the name ''Bhadra'' after a temple of ''Bhadra Kali'', a form of [[Laxmi]] which was established during Maratha rule<ref name="nri">{{cite web | url=http://www.nri.gujarat.gov.in/ahmedabad-bhadra-fort.htm | title=Bhadra Fort | publisher=Government of Gujarat | work=NRI Division | date=June 25, 2010 | accessdate=January 16, 2013}}</ref><ref name=grove/><ref name="toia">{{cite news | url=http://articles.timesofindia.indiatimes.com/2011-07-06/ahmedabad/29742646_1_bhadra-fort-walled-city-jnnurm-committee | title=Strolling atop Bhadra fort | work=[[The Times of India]] | date=July 6, 2011 | agency=TNN | accessdate=January 17, 2013 | author=John, Paul | location=Ahmedabad}}</ref> but a plaque near fort tells a different story: ''The Bhadra Gate - C.A.D. 1411 - The massive fortified gate was built in or about 1411 to serve as the principal eastern entrance of the palace erected here by Sultan Ahmad Shah I (1411-1442), the founder of Ahmedabad. The palace called the Bhadra after the ancient Rajput citadel of that name at Anhilwada-Patan (Baroda State), which the first three kings of the dynasty of Gujarat Sultans had held before Ahmedabad became the capital. Three inscribed slabs on the walls connecting this gateway with two ancillary gates behind are now almost completely defaced. One of these appears to show a date of the time of Jahangir (1605-1627). ''<ref name="sdblg">{{cite web | url=http://sonalidalal.blogspot.in/2010/07/bhadra-fort-ahmedabad-india.html | title=Bhadra | date=July 22, 2010 | accessdate=January 17, 2013 | author=Dalal, Sonali}}</ref><ref name=ggj>{{cite book|title=Gazetteer of the Bombay Presidency: Ahmedabad|url=https://books.google.com/books?id=EL4IAAAAQAAJ&pg=PA275|year=1879|publisher=Government Central Press|page=275}}</ref>

==History==
[[Ahmedabad]] was named after [[Ahmed Shah of Gujarat|Ahmad Shah I]] of the [[Muzaffarid dynasty of Gujarat|Muzaffarid dynasty]] who captured Karnavati in 1411. He established [[Ahmedabad]] as the new capital of [[Gujarat Sultanate]] and built Bhadra Fort on the east bank of the [[Sabarmati river]]. It was also known as '''Arak Fort''' as described in ''Mirat-i-Ahmadi''. The foundation stone of fort was laid down at [[Manek Burj]] in 1411. Square in form, enclosing an area of about forty-three acres, and containing 162 houses, the Bhadra fort had eight gates, three large, two in the east and one in the south-west corner; three middle-sized, two in the north and one in the south; and two small, in the west.<ref name=ggj/> The area within the fort had become occupied by urban developments by 1525.<ref name="nri"/><ref name="grove">{{cite book | title=Grove Encyclopedia of Islamic Art & Architecture | publisher=Oxford University Press | author=Bloom, Jonathan | year=2009 | pages=37–39 | isbn=978-0-19-530991-1 | url=https://books.google.com/?id=un4WcfEASZwC&pg=PA37&dq=bhadra+ahmedabad+history#v=onepage&q=bhadra%20ahmedabad%20history&f=false | volume=2 | author2=Blair, Sheila}}</ref> So a second fortification was built later by [[Mahmud Begada]], the grandson of Ahmed Shah, with an outer wall 10&nbsp;km (6.2&nbsp;mi) in circumference and consisting of 12 gates, 189 bastions and over 6,000 battlements as described in ''Mirat-i-Ahmadi''.<ref name="gkm">{{cite book | title=India through the ages: history, art, culture, and religion | publisher=Sundeep Prakashan | author=G. Kuppuram | year=1988 | pages=739 | url=https://books.google.com/books?id=AvggAAAAMAAJ&dq=ahmedabad+189+bastions+and+over+6%2C000+battlements.&q=ahmedabad+189+bastions+over+6%2C000+battlements.#search_anchor | volume=2 }}</ref> Almost 60 governors ruled [[Gujarat]] during the [[Mughal Empire|Mughal period]] including the future Mughal emperors [[Jahangir]], [[Shah Jahan]] and [[Aurangzeb]].<ref name="sjtoi">{{cite news | url=http://articles.timesofindia.indiatimes.com/2012-07-29/ahmedabad/32922731_1_shah-jahan-line-of-stout-kings-cunning-craft-and-workmanship | title=No entry for Shah Jahan | work=[[The Times of India]] | date=July 29, 2012 | agency=TNN | accessdate=January 17, 2013 | author=John, Paul}}</ref><ref name="dnacd">{{cite news | url=http://www.dnaindia.com/lifestyle/report_the-story-of-how-architecture-in-gujarat-got-a-mughal-touch_1472394 | title=The story of how architecture in Gujarat got a Mughal touch… | work=[[Daily News and Analysis|DNA]] | date=Nov 26, 2010 | accessdate=January 17, 2013 | author=Desai, Hemang | location=Ahmedabad}}</ref> A seraglio was built later in the 17th century by a Mughal governor, Azam Khan, known as ''Azam Khan Sarai''.<ref name="eamc">{{cite web|url=http://www.egovamc.com/AhmCity/history.aspx |title=History |publisher=[[Ahmedabad Municipal Corporation]] |work=Official Website |accessdate=January 16, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20160223012426/http://egovamc.com/AhmCity/history.aspx |archivedate=February 23, 2016 |df= }}</ref><ref name=grove/> It was used as a ''Musafir khana'' (a resting place for travellers) during Mughal rule.<ref name="toia"/><ref name="toib">{{cite news | url=http://articles.timesofindia.indiatimes.com/2009-06-12/ahmedabad/28165182_1_bhadra-fort-walled-city-cultural-centre | title=Bhadra Fort to turn into heritage hangout! | work=[[The Times of India]] | date=June 12, 2009 | agency=TNN | accessdate=January 17, 2013 | location=Ahmedabad}}</ref>

Sarsenapati [[Umabaisaheb Khanderao Dabhade]] became the only female Commander-in-Chief in the history of the Marathas in 1732. She commanded the Maratha Army and fought a war near Ahmedabad at Bhadra Fort defeating Mughal Sardar Joravar Khan Babi.

Joint rule by [[Peshwa]] and [[Gaekwad]] of the [[Maratha Empire]] brought an end to the Mughal era in 1583. During the [[First Anglo–Maratha War]] (1775–1782), General Thomas Wyndham Goddard with 6,000 troops stormed Bhadra Fort and captured Ahmedabad on February 15, 1779. There was a garrison of 6,000 Arab and Sindhi infantry and 2,000 horses. Losses in the fight totalled 108, including two Britons. After the war, the fort was later handed back to Marathas under the [[Treaty of Salbai]].<ref name="toib"/><ref name="longman">{{cite book | url=https://books.google.com/?id=ZQXGmDs3LlMC&pg=PA446&lpg=PA446#PPA446,M1 | title=A History of the Mahrattas | publisher=Longman, Rees, Orme, Brown, and Green | author=Duff, James Grant | year=1826 | volume=2 | location=London | origyear=Oxford University }}</ref><ref name="hb">{{cite book | title=A comprehensive history of India, civil, military and social | publisher=Blackie | author=Beveridge, Henry | url=https://books.google.com/?id=hRENAAAAYAAJ&pg=PA448&lpg=PA448#v=snippet&q=ahmedabad&f=false | year=1862 | pages=456–466 | origyear=New York Public Library}}</ref>

[[Ahmedabad]] was conquered by the British in 1817.<ref name=grove/> The fort complex was used as a jail during the [[British Raj]].<ref name="toia"/>

''Azam Khan sarai'' currently houses the government offices, an [[Archaeological Survey of India|ASI]] office, a post office and the city's civil courts. It is also used for flag hoisting on Independence Day and Republic Day.<ref name="toia"/><ref name="toib"/>

==Structures==
[[File:Ahmedabad 1855.jpg|thumb|300px|Bhadra and other structures in [[old Ahmedabad]], 1879]]

===Citadel, royal square and ''Teen Darwaza''===
[[File:Ahmdedabad Teen Darwaza India 1890.jpg|Teen Darwaza visible in street scene of 1890|thumb|300px]]
Bhadra Fort housed royal palaces and the beautiful Nagina Baugh and the royal [[Ahmed Shah's Mosque]] on the west side and an open area known as ''Maidan-Shah'' on the east side. It had a fortified city wall with 14 towers, eight gates and two large openings covering an area of 43 acres. The eastern wall on the river bank can still be seen. The fort complex was used as a royal court during his reign. On the eastern side of a fort, there is a triple gateway known as ''[[Teen Darwaza]]'' which was formerly an entrance to the royal square, ''Maidan-Shah''. The road beyond ''Teen Darwaza'' leads [[Manek Chowk (Ahmedabad)|Manek Chowk]], a mercantile square. On the south side along the road, there is a congregational mosque known as [[Jama Masjid, Ahmedabad|Jami Masjid]].<ref name="nri"/><ref name="grove"/><ref name="toia" />

The citadel's architecture is Indo-sarcenic with intricately carved arches and balconies. Fine lattice work adorns windows and murals. There are some Islamic inscriptions on the arches of the fort. The palace contains royal suites, the imperial court, halls and a prison.<ref name="nri"/><ref name="grove"/><ref name="toia" />

{{quote|text= The Maidan-Shah, or the kings market, is at least 1600 feet long and half as many broad and beset all about with rows of Palm-trees and Date-trees intermixed with Citron-trees and Orange-trees, whereof there are very many in the several streets: which is not only verry pleasant to the sight, by the delightful prospect it affords, but also makes the walking among them more convenient by reason of the coolness. Besides this Maidan, there are in the city four Bazaars, or public places, where are sold all kind of merchandise.|sign=[[Johan Albrecht de Mandelslo]], German traveller; in October, 1638|source=Mandelslo's Travels In Western India, Page 22<ref name="toib"/><ref name="mandel">{{cite book | title=Mandelslo's Travels In Western India| publisher=Asian Educational Services | author=Commissariat, M. S. | year=1996 | pages=22–23 | isbn=978-81-206-0714-9 | url=https://books.google.com/books?id=PlhTMbDPykUC&q=bhadra+castle#v=snippet&q=bhadra%20castle&f=false}}</ref>}}

===Azam Khan Sarai===
[[File:Azam Khan Sarai Palace Ahmedabad 1866.jpg|thumb|250px|left|Azam Khan Sarai in 1866 when it was used as jail]]
Azam Khan, also known as Mir Muhammad Baquir was a Mughal governor. He built a palace known as ''Azam Khan Sarai'' in 1637. Its entrance, 5.49 meters high, opens onto an octagonal hall which had a low balcony made up of stone in the upper floor. It was used as a resting place for travellers in the Mughal era and as a hospital and a jail during British rule.<ref name="dnacd" /><ref name="azm">{{cite news | url=http://articles.timesofindia.indiatimes.com/2012-07-29/ahmedabad/32923374_1_mughal-era-sarkhej-roza-monument | title=Mughal icons decaying in city | work=[[The Times of India]] | date=July 29, 2012 | agency=TNN | accessdate=January 17, 2013 | author=John,  Paul | location=Ahmedabad}}</ref> There was a gibbet on the roof of Azam Khan Sarai used for hanging during the Gujarat sultanate and the British era. According to one story, it was here Ahmed Shah hanged his son-in-law who was convicted of murder.<ref name="gcdna">{{cite news | url=http://www.dnaindia.com/india/report_godhra-case-how-noose-tightened-over-ages-in-bhadra_1514934 | title=Godhra case: How noose tightened over ages in Bhadra | work=[[Daily News and Analysis]] | date=March 3, 2011 | agency=DNA | accessdate=January 17, 2013 | author=Shah, Charul}}</ref>

===Bhadra Kali Temple===
A room in north wing of ''Azam Khan Sarai'' was turned into the temple of Bhadra Kali during [[Maratha Empire|Maratha rule]].<ref name=nri/><ref name="toia"/> It has a black statue of Goddess Bhadra Kali with four hands.

;Legend
Years ago, [[Laxmi]], the Goddess of Wealth, came to the gate of Bhadra Fort to leave the city in the night. Watchman Siddique Kotwal stopped her and identified her. He asked her not to leave fort until he obtained permission from the king. He beheaded himself in order to keep Laxmi in the city. It resulted in the prosperity of the city.<ref name="kk">{{cite news | url=http://www.ahmedabadmirror.com/index.aspx?page=article&sectid=3&contentid=201102242011022403033528333ee5e43 | title=Kankaria to showcase city | work=[[Ahmedabad Mirror]] | date=February 24, 2011 | accessdate=February 22, 2013 | author=Jadav, Ruturaj}}</ref>

There is a tomb near Bhadra Gate dedicated to Siddique Kotwal and a temple to Bhadra Kali, representing Laxmi.<ref name="kk"/> A lamp in one of the hole in ''Teen Darwaza'' is lit continuously for more than six hundred years by a Muslim family is dedicated to Laxmi.<ref name="The Times 2013">{{cite news | title=Lamp of hope burns bright at historic Teen Darwaza| website=The Times of India| date=3 September 2013 | url=http://m.timesofindia.com/city/ahmedabad/Lamp-of-hope-burns-bright-at-historic-Teen-Darwaza/articleshow/22240853.cms | accessdate=11 January 2015}}</ref>

===Clock tower===
[[File:Bhadra Fort Clock tower.JPG|thumb|150px|Bhadra fort clock tower]]
The Bhadra Fort tower clock was brought from [[London]] in 1849 at cost of Rs. 8000 and installed here at the cost of £243 (Rs. 2430) in 1878 by the [[British East India Company]]. At night, it was illuminated from behind by a [[kerosene]] lamp which was replaced by an electric light in 1915. Ahmedabad's first electrical connection, it ceased to operate in the 1960s but the [[Ahmedabad Municipal Corporation|AMC]] and the [[Archaeological Survey of India|ASI]] now plan to repair it.<ref name="clock">{{cite news | url=http://articles.timesofindia.indiatimes.com/2012-11-25/ahmedabad/35347807_1_clock-tower-new-rickshaw-cycle-rickshaws | title=Pioneers' paradise | work=[[The Times of India]] | date=November 25, 2012 | agency=TNN | accessdate=January 17, 2013}}</ref><ref name="bfclock">{{cite news | url=http://lite.epaper.timesofindia.com/mobile.aspx?article=yes&pageid=1&edlabel=TOIA&mydateHid=08-06-2011&pubname=&edname=&articleid=Ar00107&format=&publabel=TOI | title=Bhadra fort clock to tick soon | work=[[The Times of India]] | date=8 June 2011 | agency=TNN | accessdate=January 17, 2013 | author=John, Paul}}</ref>

==Redevelopment==
Under Bhadra Plaza Development Project, an initiative of the [[Ahmedabad Municipal Corporation|AMC]] and the ASI, Bhadra Fort was renovated and the open space between the fort and ''Teen Darwaza'', earlier known as ''Maidan-Shah'' was restored. Landscaping was recreated based on the accounts of historical past travellers. The work started on 26 January 2012 and the renovation of open areas was completed in November 2014.<ref name="Toi1411">{{cite news|title=On road less travelled by chaos, Bhadra blooms|publisher=The Times of India|date=20 November 2014|url=http://m.timesofindia.com/city/ahmedabad/On-road-less-travelled-by-chaos-Bhadra-blooms/articleshow/45211620.cms|accessdate=2 December 2014|first=Ankur|last=Tewari}}</ref> The cost of the project is estimated Rs 115 crore.  A stretch between the fort and ''Teen Darwaza'' earlier known as ''Maidan-Shah'' was declared a pedestrian zone. The new public amenities, marble benches and kiosks for hawkers were constructed.<ref name="Times of India14">{{cite news|title=Will Bhadra plaza be picture perfect?|publisher=The Times of India|date=2 November 2014|url=http://m.timesofindia.com/city/ahmedabad/Will-Bhadra-plaza-be-picture-perfect/articleshow/45009156.cms|accessdate=2 December 2014}}</ref> There are also plans for a pedestrian bridge connecting Bhadra plaza with the [[Sabarmati Riverfront Development Project|Sabarmati riverfront]] and a multilevel car park at Lal Darwaza. The museum and galleries are planned on the first floor of the fort palace while a handicraft outlet will be housed on the ground floor. A traditional restaurant, food and ethnic markets as well as an exhibition centre are also planned.<ref name=toia /><ref name="toib"/><ref name="dga">{{cite news | url=http://deshgujarat.com/2012/01/13/amc-announces-begining-of-bhadra-fort-development-project/ | title=Bhadra plaza development project from 26 January, watch in images | work=DeshGujarat.com | date=January 13, 2012 | accessdate=January 16, 2013}}</ref><ref name="dnaa">{{cite news | url=http://www.dnaindia.com/india/report_ahmedabad-s-bhadra-fort-to-remain-shut-for-vehicles-from-jan-27_1637500 | title=Ahmedabad's Bhadra Fort to remain shut for vehicles from Jan 27 | work=[[Daily News and Analysis]] | date=January 14, 2012 | agency=DNA | accessdate=January 16, 2013 | pages=Ahmedabad}}</ref><ref name="dnab">{{cite news | url=http://www.dnaindia.com/india/report_bhadra-fort-may-get-date-palms-to-signify-history_1715281 | title=Bhadra fort may get date palms to signify history | work=[[Daily News and Analysis]] | date=July 15, 2012 | agency=DNA | accessdate=January 17, 2013 | author=Devarhubli, Chaitra | location=Ahmedabad}}</ref><ref name="dbc">{{cite news | url=http://daily.bhaskar.com/article/GUJ-AHD-from-jan-27-bhadra-will-be-closed-to-all-vehicles-2748799.html | title=From Jan 27, Bhadra will be closed to all vehicles | work=[[Daily Bhaskar]] | date=January 14, 2012 | agency=DNA | accessdate=January 17, 2013}}</ref> It is the first heritage and pedestrianization project under [[Jawaharlal Nehru National Urban Renewal Mission|JnNURM]].<ref name="hudco">{{cite news | url=http://articles.timesofindia.indiatimes.com/2013-02-21/ahmedabad/37220473_1_walled-city-bhadra-hudco | title=Bhadra revitalization project wins HUDCO award | work=[[The Times of India]] | date=February 21, 2013 | agency=TNN | accessdate=March 6, 2013}}</ref> [[Jaishankar Sundari]] hall, a performing arts venue, was renovated and reopened in 2010.<ref>{{cite news|url=http://www.ahmedabadmirror.com/index.aspx?Page=article&sectname=News%20-%20City&sectid=3&contentid=20100719201007190049535604438bedd|work =Ahmedabad Mirror |date=July 10, 2010|title =Famed Jaishankar hall will reopen in August|first=Manish|last=Mistry}}</ref>

The city civil court and sessions court were operated in buildings adjacent to Azam Khan Sarai. They were transferred to old high court building on Ashram Road. The new eight floor court building is planned after demolition of old buildings. The plan was challenged in [[Gujarat High Court]] citing protected monument laws and regulations but high court permitted the construction after presentation by authorities.<ref>{{cite news|url=http://m.timesofindia.com/city/ahmedabad/HC-gives-nod-for-demolition-of-old-court-building-at-Bhadra/articleshow/39902027.cms|title=HC gives nod for demolition of old court building at Bhadra|work=The Times of India|date=August 9, 2014}}</ref><ref>{{cite news|url=http://m.timesofindia.com/city/ahmedabad/Gujarat-high-court-stays-construction-of-new-Bhadra-court-building/articleshow/35448587.cms|title=Gujarat high court stays construction of new Bhadra court building | date=May 22, 2014|work=The Times of India}}</ref>

===Recognition===
* [[Housing and Urban Development Corporation|HUDCO]] Award for Best Practices to Improve the Living Environment 2013<ref name="hudco"/>

==Gallery==
<gallery widths="140px" heights="140px" perrow="5">
Image:Bhadra fort inscription.JPG|Bhadra Fort inscription
Image:Bhadra fort gate from inside.JPG|Bhadra fort gate from inside
Image:Chinubhai Ranchhodlal.JPG|Statue of Chinubhai Baronet in Royal Square
Image:Teen Darwaza.JPG|Roadside vendors at ''Teen Darwaza''
</gallery>

==See also==
* [[Ahmedabad]]
* [[History of Ahmedabad]]
* [[Gates of Ahmedabad]]
* [[Teen Darwaza]]
* [[Manek Chowk (Ahmedabad)|Manek Chowk]]
* [[Jama Masjid, Ahmedabad|Jami Mosque]]

==References==
{{reflist}}
* This article contains public domain text from {{cite book|title=Gazetteer of the Bombay Presidency: Ahmedabad|url=https://books.google.com/books?id=EL4IAAAAQAAJ&pg=PA275|year=1879|publisher=Government Central Press|page=275}}

==External links==
{{Commons category|Bhadra Fort}}
* [http://deshgujarat.com/2012/01/13/amc-announces-begining-of-bhadra-fort-development-project/ Bhadra Fort Development Project]
* [http://sonalidalal.blogspot.in/2010/07/bhadra-fort-ahmedabad-india.html Photo Gallery]
{{Forts in India|status=autocollapse}}
{{Ahmedabad topics|status autocollapse}}

[[Category:Buildings and structures in Ahmedabad]]
[[Category:Forts in Gujarat]]
[[Category:Tourist attractions in Ahmedabad]]