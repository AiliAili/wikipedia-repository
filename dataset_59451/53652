{{Use mdy dates|date=September 2011}}
{{Infobox officeholder
|name          = Carl McCall
|office        = [[New York State Comptroller|Comptroller of New York]]
|governor      = [[Mario Cuomo]]<br>[[George Pataki]]
|term_start    = May 7, 1993
|term_end      = January 1, 2003
|predecessor   = [[Edward Regan|Ned Regan]]
|successor     = [[Alan G. Hevesi|Alan Hevesi]]
|state_senate1 = New York
|district1     = 28th
|term_start1   = 1975
|term_end1     = 1980
|predecessor1  = [[Sidney A. von Luther|Sidney von Luther]]
|successor1    = [[Leon Bogues]]
|birth_date    = {{birth date and age|1935|10|17}}
|birth_place   = [[Boston]], [[Massachusetts]], [[United States|U.S.]]
|death_date    = 
|death_place   = 
|party         = [[Democratic Party (United States)|Democratic]]
|spouse        = Cecilia McCall {{small|(Divorced)}}<br>[[Joyce F. Brown|Joyce Brown]] {{small|(1983–present)}}
|alma_mater    = [[Dartmouth College]]<br> [[Andover Newton Theological School]]
|religion      = [[United Church of Christ]]
|allegiance    = {{flag|United States}}
|branch        = {{army|United States}}
|serviceyears     = 1959
| rank             = 
|battles       = 
}}
'''Herman Carl McCall''' (born October 17, 1935) is a former Comptroller of the [[US state]] of [[New York (state)|New York]] and was the [[United States Democratic Party|Democratic]] candidate for [[Governor of New York|state governor]] in 2002. He is an ordained minister in the [[United Church of Christ]], and currently serves on the board of directors of several corporations. He is a graduate of  [[Dartmouth College]] and was also educated at the [[University of Edinburgh]],<ref>[http://investing.businessweek.com/businessweek/research/stocks/private/person.asp?personId=4370708&privcapId=3727262&previousCapId=107534&previousTitle=Tyco%20International%20Ltd.]</ref> received a Master's of Divinity Degree from [[Andover Newton Theological School]]. He was the first African-American to be elected comptroller of New York. Since October 17, 2011 he has served as the chairman of the [[State University of New York]] Board of Trustees.<ref>{{Cite web|title = H. Carl McCall, Chairman - SUNY|url = https://www.suny.edu/about/leadership/board-of-trustees/meet-the-trustees/h-carl-mccall-chairman/|website = www.suny.edu|accessdate = 2015-06-08|first = SUNY|last = }}</ref>

==Early life and career==
McCall was born in the [[Roxbury, Boston|Roxbury]] section of Boston, the oldest of six children. His father, Herman McCall, moved to Boston from Georgia, and left the family when Carl McCall was eleven. McCall attended church with [[Edward Brooke]]. He attended [[Dartmouth College]] on private and [[ROTC]] scholarships, and graduated in 1958 with a bachelor's degree in government. During the 1960s, McCall worked as a high school teacher and a bank manager. He taught for six months at [[Jamaica Plain|Jamaica Plain High School]] on [[Sumner Hill Historic District|Sumner Hill]] in Boston, and then joined the Army. He opened a church in the [[Dorchester, Boston|Dorchester]] neighborhood. By the late 1960s McCall moved to New York City to work for church outreach. He was appointed by New York Mayor [[John Lindsay]] to head the Commission Against Poverty.

During the 1970s, McCall, backed by Harlem political power, [[Percy Sutton]], was elected to three terms as a State Senator representing Harlem and other parts of Manhattan. He was a member of the [[New York State Senate]] from 1975 to 1980, sitting in the [[181st New York State Legislature|181st]], [[182nd New York State Legislature|182nd]] and [[183rd New York State Legislature]]s. He left the Senate to accept an appointment from President [[Jimmy Carter]] as a member of the U.S. delegation to the United Nations with the rank of [[Ambassador of the United States|Ambassador]].

In 1982 he was an unsuccessful candidate for the Democratic nomination for [[Lieutenant Governor of New York]], to run on a ticket with [[Mario Cuomo]]. Governor Cuomo then appointed McCall to serve as the state's Commissioner of Human Rights (1983–84).

While serving in the private sector as a vice president for governmental relations with [[Citicorp]] (1985–93), McCall accepted an appointment to the New York City Board of Education, where he served as President of the Board from 1991 to 1993.

==State Comptroller==
In 1993, McCall was elected by the [[New York State Legislature]] to fill the unexpired term of [[Republican Party (United States)|Republican]] [[Edward Regan]] as [[New York State Comptroller|state comptroller]]. As comptroller, McCall was responsible for serving as the state's chief fiscal officer, conducting audits of state and local entities, serving as the state's bookkeeper, investing the state's funds, overseeing the state's debt issuances, and serving as the sole trustee of the state pension fund.

He was elected state comptroller in 1994, defeating conservative [[Herbert London]], and in 1998, defeating Republican Bruce Blakeman. In 1998 he announced that he would not seek election to the [[United States Senate election in New York, 2000|U.S. Senate in 2000]], helping to pave the way for the successful candidacy of [[Hillary Clinton]].

[[Al Sharpton]] was quoted as saying that "if [[David Dinkins]] has a cold in the black community, Carl McCall has [[pneumonia]]", and it has been said that McCall was a "stiff, bourgeois figure" who generally did not excite the black electorate.<ref>{{cite web|url=http://old.nationalreview.com/dreher/dreher090402.asp |title=Archived copy |accessdate=2011-06-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20100926110929/http://old.nationalreview.com:80/dreher/dreher090402.asp |archivedate=September 26, 2010 |df=mdy }}</ref>

==Campaign for Governor==
In 2002 McCall officially announced his campaign against [[Republican Party (United States)|Republican]] incumbent [[George Pataki]]. After his primary opponent, former US Housing Secretary [[Andrew Cuomo]], withdrew from the race, McCall entered the general election as the uncontested Democratic candidate, but lost to Pataki. McCall remains New York state's highest-ranking black elected official and first black major party gubernatorial candidate.<ref>[http://www.nydailynews.com/news/rep-charles-rangel-andrew-cuomo-won-dare-run-gov-paterson-article-1.457272]</ref>

McCall was the favorite of the Democratic establishment, but he faced a tough challenge from Cuomo which almost split the party.<ref>[http://edition.cnn.com/2002/ALLPOLITICS/09/03/elec02.ny.g.cuomo/index.html]</ref> Cuomo proved to be a better fundraiser, and McCall's own campaign war chest was heavily depleted in the primary battle. Although McCall himself did not make any negative attacks, his close supporter, US Congressman [[Charles B. Rangel]], stated that the McCall camp would not necessarily endorse Cuomo in the general election should the latter win. This backfired as some Italian-Americans interpreted that as racism, and many of Cuomo's supporters refused to unite behind McCall after McCall won the nomination. McCall was endorsed by Senator [[Chuck Schumer]]. While Senator [[Hillary Clinton]] did not officially take sides during the primary, she loaned a staffer and a fundraiser to McCall's campaign and she marched by McCall's side at the West Indian American Day parade in New York City, as the Clinton wanted to retain strong African-American support in case she made a president run in the future. Cuomo withdrew from the primary race after McCall moved to a double-digit lead in polls.<ref>[http://usatoday30.usatoday.com/news/nation/2002-09-03-cuomo_x.htm]</ref>

Money would prove to be a handicap in the general election, as [[Democratic National Committee|DNC]] Chairman [[Terry McAuliffe]] stated that he would not channel large sums of money to McCall's campaign unless the gap could be closed with Pataki, which McCall never managed to do. In an unusual show of support, conservative radio host [[Rush Limbaugh]] urged his listeners to donate to McCall's campaign. Limbaugh said the refusal to give the McCall campaign money was a show of racism on the part of the DNC.

Charles Rangel suggested that [[Andrew Cuomo]]'s gubernatorial run in 2010 would undo years of work that Cuomo spent rebuilding his standing in the state Democratic Party after his bruising 2002 gubernatorial primary contest against Carl McCall. Rangel said "it would be immoral for the white attorney general to challenge New York's first black governor in a primary" with the "inclination for racial polarization in a primary in the state of New York. Since we have most African-Americans registered as Democrats, and since you would be making an appeal for Democrats, it would be devastating in my opinion". [[David Paterson]], the incumbent and first African-American governor of New York, whom Rangel staunchly supported, fared poorly in polls due to several scandals and later abandoned his campaign re-election.<ref>[http://www.nydailynews.com/news/rep-charles-rangel-andrew-cuomo-won-dare-run-gov-paterson-article-1.457272]</ref><ref>[http://www.nbcnewyork.com/news/local/Cuomo-Run-Could-Spark-Racial-Divide-Rangel.html?amp=y]</ref>

===Letterhead controversy===
In October 2002, McCall released 61 letters he had written on state letterhead to heads of companies in which the state pension fund owned large blocks of stock, asking them to review enclosed resumes of his relatives and other job-seekers.

Some of the letters referred to the size of the state's ownership interest in the corporation targeted, which critics claimed amounted to a veiled threat to punish companies that didn't hire his relatives. A Quinnipiac poll released October 16 showed that two-thirds of likely voters were aware of the letters and of those more than a fifth were less likely to vote for McCall as a result.

McCall defended the letters. Although he did issue a statement regretting the "appearance" and "impression" of the letters he wrote on government stationery, he maintained that he "never sought to leverage my public position nor mix my government role with my personal and professional relationship" in the letters.<ref>[https://query.nytimes.com/gst/fullpage.html?res=9C03E0DC153AF930A25753C1A9649C8B63 COPING; Small Favors, Large Stories, Tough Questions, By ANEMONA HARTOCOLLIS, Published: October 13, 2002]</ref> McCall's daughter, Marci, was hired by Verizon, which received such a letter, but was subsequently fired for using her company credit card to pay for substantial personal expenditures. Charges of larceny against her were dropped after some reimbursement to Verizon, and she was then hired as a marketer by McCall's running mate, Dennis Mehiel.

===Results===
McCall was defeated in the election for governor by the Republican incumbent, [[George Pataki]]. McCall received 33% of the vote, a low percentage for a Democratic nominee for statewide office in a state where the Democratic Party is by far the dominant party based on voter registrations. Some observers felt that this seemingly poor showing was in part due to the revelation of the above-referenced letters; others insinuated that McCall's showing was related to racism, especially in upstate New York. However, others point out that Pataki was able to make crucial inroads into traditional areas of Democratic support, such as unions and even African-American congregations. The three-way vote-split efforts of [[Tom Golisano]], who primarily ran against Pataki on [[Independence Party of New York|his own third-party line]], also diverted much of the anti-Pataki vote away from McCall.

Other political commentators attribute McCall's defeat to the growing popularity of the [[Republican Party (United States)|Republican Party]] after the [[September 11, 2001 attacks|terrorist attacks]] of September 11, 2001, along with Governor Pataki's successful administration of the state.

==Present==
McCall was a recent member of the Board of the [[New York Stock Exchange]] (1999–2003), as well as the Apollo Theater Foundation, Inc. Currently, he is a member of the Fiscal Control Board for [[Buffalo, New York]] as well as the SUNY Board of Trustees. He also serves on the Boards of Directors for TYCO International, New Plan Realty, TAG Entertainment Corporation, Ariel Mutual Fund,<ref>[http://www.arielinvestments.com Ariel Investments]</ref> and as Chair of the New York State Public Higher Education Conference Board. He spoke at his alma mater [[Dartmouth College]]'s annual Martin Luther King, Jr. Day celebration in 2006 about modern civil rights and the legacy of Dr. King. He operates his own financial services fund called Convent Capital, LLC.

McCall is a member of [[Alpha Phi Alpha]] fraternity.<ref>[http://politicalgraveyard.com/group/alpha-phi-alpha.html Fraternity Data]</ref>

In January 2007, McCall was appointed to a panel, along with former New York State Comptroller [[Edward Regan|Ned Regan]] and former New York City Comptroller [[Harrison J. Goldin|Harrison Jay Goldin]], to interview and recommend up to five candidates to the State Legislature to replace [[Alan Hevesi]], who resigned as State Comptroller due to scandal.<ref>[https://www.nytimes.com/2007/01/28/nyregion/28comptroller.html?_r=1&oref=slogin "New York Times, Ex-Comptollers Endured Controversy Themselves," By NICHOLAS CONFESSORE, Published: January 28, 2007]</ref>

In May 2009, Convent Capital, the financial services firm run by McCall, was subpoenaed, along with other unregistered placement agents, by state Attorney General Andrew Cuomo's office as part of an inquiry into possible corruption involved in deals brokered between investment firms and the state pension fund.

McCall joined the [[State University of New York]] Board of Trustees on October 22, 2007 and was appointed chairman on October 17, 2011.<ref>http://www.suny.edu/about/leadership/board-of-trustees/meet-the-trustees/h-carl-mccall-chairman/</ref>

==Personal==
McCall is the son of Herman McCall and Caroleasa Ray. His father was a railroad porter who abandoned the family of six children; the family was supported thereafter primarily by welfare and by relatives, due to his mother's infirmity. McCall graduated from Roxbury Memorial High School in Boston. where he was president of his class, before going on to Dartmouth College. In 1983 McCall married second-wife, [[Dr. Joyce F. Brown]], former psychology professor, NYC deputy mayor in the Dinkins Administration, and current president of SUNY's [[Fashion Institute of Technology]] in New York City. They have no children. His first marriage to Cecilia McCall, mother of his daughter Marcella (Marci), ended in divorce.

==Awards==
McCall is the recipient of nine honorary degrees.{{citation needed|date=November 2014}} In 2003, he was awarded the [[Nelson Rockefeller]] Distinguished Public Service Award from the Rockefeller College of Public Affairs and Policy at the [[University of Albany]].

==References==
{{Reflist}}

==External links==
*[http://www.fiscalpolicy.org/FPIintheNews/CDBRMarch62000.htm McCall, agency spar over accountability]
*[https://web.archive.org/web/20070627052436/http://www.jrn.columbia.edu/studentwork/election/2002/governor/ Columbia]
*[http://www.visionaryproject.org/mccallcarl H. Carl McCall's oral history video excerpts] at The National Visionary Leadership Project

{{s-start}}
{{s-par|us-ny-sen}}
{{s-bef|before=[[Sidney A. von Luther|Sidney von Luther]]}}
{{s-ttl|title=Member of the [[New York State Senate|New York Senate]]<br>from the 28th district|years=1975–1980}}
{{s-aft|after=[[Leon Bogues]]}}
|-
{{s-off}}
{{s-bef|before=[[Edward Regan|Ned Regan]]}}
{{s-ttl|title=[[New York State Comptroller|Comptroller of New York]]|years=1993–2003}}
{{s-aft|after=[[Alan G. Hevesi|Alan Hevesi]]}}
|-
{{s-ppo}}
{{s-bef|before=[[Peter Vallone, Sr.|Peter Vallone]]}}
{{s-ttl|title=[[Democratic Party (United States)|Democratic]] nominee for [[Governor of New York]]|years=[[New York gubernatorial election, 2002|2002]]}}
{{s-aft|after=[[Eliot Spitzer]]}}
{{s-end}}

{{NYSComptroller}}
{{New York State Democratic Committee}}

{{DEFAULTSORT:McCall, Carl}}
[[Category:1935 births]]
[[Category:African-American state legislators in New York]]
[[Category:Alumni of the University of Edinburgh]]
[[Category:Dartmouth College alumni]]
[[Category:Living people]]
[[Category:New York gubernatorial candidates]]
[[Category:New York Democrats]]
[[Category:New York State Comptrollers]]
[[Category:New York State Senators]]
[[Category:United Church of Christ ministers]]
[[Category:United States presidential electors, 2000]]