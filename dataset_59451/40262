{{for|the athlete|Ingrid Kristiansen}}
{{Infobox person
| honorific_prefix = 
| name = Ingrid Christensen
| image = Ingrid_Christensen_died_1976.jpeg
| alt = 
| caption = Shown during her 1931 expedition
| birth_name = Ingrid Dahl
| birth_date = {{Birth date|1891|10|10|df=yes}}
| birth_place = 
| death_date = {{Death date and age|1976|06|18|1891|10|10|df=yes}}
| death_place = 
| nationality = [[Norway|Norwegian]]
| other_names = 
| occupation = 
| years_active = 
| known_for = First woman in [[Antarctica]]
| notable_works = 
| spouse = [[Lars Christensen]]
}}
'''Ingrid Christensen''' (10 October 1891 – 18 June 1976) was an early polar explorer.  She was known as the first woman to view [[Antarctica]] and land on the Antarctic mainland.<ref name=":5">{{Cite web|url=http://www.antarctica.gov.au/magazine/2011-2015/issue-23-december-2012/antarctic-arts-fellowship/the-first-woman-in-antarctica|title=The first woman in Antarctica|date=2012|website=www.antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-27}}</ref>

== Early life ==
Christensen (née Dahl) was the daughter of Alfhild Freng Dahl and wholesaler and ship owner Thor Dahl, who was at the time one of the largest merchants in [[Sandefjord]], Norway.<ref name=":0">{{Cite web|url=https://www.myheritage.no/person-7068472_114283381_114283381/ingrid-dahl|title=Obituary of Ingrid Christensen|date=1976-06-19|website=MyHeritage.com|publisher=Vestfolds Fremtid|trans-title=English translation|access-date=2016-06-07}}</ref>

The Norwegian Antarctic historian [[Hans Borgen|Hans Bogen]] described her in 1955 as: “Ingrid Dahl was exactly what in our time we call a kjekk og frisk jente (a Norwegian expression meaning a girl who could be at once one of the boys, then one of the girls, without losing her femininity or charm). She was the natural leader of the girls in her age group because of her initiative, humour and fearlessness, qualities she has preserved unwaveringly to the present day”.<ref>{{Cite web|url=http://www.worldcat.org/title/70-ar-lars-christensen-og-hans-samtid/oclc/492845180|title=70 år – Lars Christensen og hans samtid|last=Stensrud|first=Odd|website=worldcat.org|publisher=Forlagt AV Johan Grundt Tanum|location=Oslo|page=240|trans-title=70 years – Lars Christensen and his contemporaries|access-date=}}</ref> Ingrid married [[Lars Christensen]] in 1910, uniting two of Sandefjord’s most powerful ship owning families, and they had six children.<ref>{{Cite web|url=https://www.geni.com/people/Ingrid-Christensen/6000000007980783742|title=Ingrid Christensen (Dahl)|website=geni.com|access-date=2016-06-07}}</ref><ref>{{Cite book|url=https://books.google.com/books?id=fRJtB2MNdJMC|title=Encyclopedia of the Antarctic|last=Riffenburgh|first=Beau|date=2007-01-01|publisher=Taylor & Francis|isbn=9780415970242|page=234|language=en}}</ref>

== Antarctic exploration ==
Christensen made four trips to the [[Antarctic]] with her husband on the ship ''Thorshavn'' in the 1930s, becoming the first woman to see Antarctica, the first to fly over it, and—arguably—the first woman to land on the Antarctic mainland.<ref name=":5" /><ref name=":3">{{Cite journal|last=Jesse|first=Blackadder,|date=2013-01-01|title=Illuminations : casting light upon the earliest female travellers to Antarctica|url=http://researchdirect.uws.edu.au/islandora/object/uws:22583}}</ref><ref name=":4">{{Cite web|url=https://oceanwide-expeditions.com/blog/the-first-woman-and-female-scientists-in-antarctica|title=The first woman and female scientists in Antarctica|website=oceanwide-expeditions.com|access-date=2016-06-23}}</ref>

In 1931, Christensen sailed with Mathilde Wegger. The expedition sighted and named Bjerkö Head on 5 February 1931, making Christensen and Wegger the first women to see Antarctica.<ref>Chipman, E. (1986). ''Women on the ice: A history of women in the far south''. Victoria: Melbourne University Press, page 72.</ref> [[Douglas Mawson]] reported spotting two women aboard a Norwegian ship, who were probably Christensen and Wegger, during his [[British Australian and New Zealand Antarctic Research Expedition|BANZARE expedition]]. He wired back to the Australian media: "...much astonishment was excited by the dramatic appearance on their decks of two women attired in the modes of civilisation. Theirs is a unique experience, for they can make much merit of the fact that they are, perhaps, the first of their sex to visit Antarctica".<ref>{{Cite news|url=http://nla.gov.au/nla.news-article16762092|title=Women in Antarctica|last=|first=|date=1931-03-14|work=|publisher=Sydney Morning Herald|page=13|access-date=|via=}}</ref>

In 1933, Christensen sailed with Lillemor (Ingebjørg) Rachlew, who kept a diary and took photographs,<ref>Rachlew’s photographs were published in the French journal (1934, 13 January 1934). Voyage d’une femme dans L’Antarctique (Travels of a woman in the Antarctic). ''L'Illustration, 4741,'' 52–53.</ref> which appeared in Lars Christensen’s book<ref>{{Cite book|title=Such is the Antarctic|last=Christensen|first=Lars|date=1935|translator-last=Jayne|translator-first=E|website=|publisher=Hodder and Stoughton.|access-date=}}</ref> even though no landing was possible. Christensen sailed south for the third time in 1933–34 with Ingebjørg Dedichen.<ref>{{Cite book|url=http://catalogue.nla.gov.au/Record/850903|title=Women on the ice: a history of women in the far south|last=Chipman|first=Elizabeth|date=1986-01-01|publisher=Melbourne University Press|isbn=0522843247|location=Carlton, Vic|page=172}}</ref> They again did not manage a landing, though circumnavigated almost the entire continent. In 1934/35 Danish-born [[Caroline Mikkelsen]], wife of Captain Klarius Mikkelsen, sailed to Antarctica and landed on the [[Tryne Islands]] on the 20 February 1935 and was, until recently, thought to be the first woman to land on Antarctica.<ref name=":2">{{Cite book|title=Main events in the history of Antarctic exploration.|last=Bogen|first=H|publisher=Sandefjord: Norwegian Whaling Gazette|year=1957|isbn=|location=|page=90|pages=|nopp=|issn=0369-5158}}</ref> However, since Mikkelsen landed on an Antarctic island, Christensen is considered the first woman to set foot on the Antarctic mainland.<ref name=":5" /><ref name=":3" /><ref name=":4" />

In 1936–37 Christensen made her fourth and final trip south, with daughter Augusta Sofie Christensen, [[Lillemor Rachlew]], and Solveig Widerøe, the ‘four ladies’ for whom the underwater Four Ladies Bank was named during the voyage.<ref>{{Cite book|url=https://books.google.com/books?id=BOafAAAAMAAJ|title=My Last Expedition to the Antarctic, 1936–1937: With a Review of the Research Work Done on the Voyages in 1927–1937.|last=Christensen|first=Lars|date=1938-01-01|publisher=Johan Grundt Tanum|location=Oslo|page=8|language=en}}</ref><ref>{{Cite book|url=http://www.loc.gov/catdir/toc/ecip0517/2005023103.html|title=On the ice : an intimate portrait of life at McMurdo Station, Antarctica / Gretchen Legler|last=Legler|first=Gretchen|publisher=Milkweed Editions|isbn=9781571312822|location=Minneapolis, Minn|page=105}}</ref><ref>{{Cite journal|last=Herdman|first=Henry|last2=Wiseman|first2=John|last3=Ovey|first3=Cameron|date=1956-09-01|title=Proposed names of features on the deep-sea floor|url=http://www.sciencedirect.com/science/article/pii/0146631356900144|journal=Deep-Sea Research|volume=3|issue=4|pages=253–261|doi=10.1016/0146-6313(56)90014-4|pmid=|access-date=|bibcode=1956DSR.....3..253H}}</ref> Christensen flew over the mainland, becoming the first woman to see Antarctica from the air.<ref name=":2" /> On 30 January 1937, Lars Christensen's diary records that Ingrid Christensen landed at [[Scullin monolith|Scullin Monolith]], becoming the first woman to set foot on the Antarctic mainland, followed by the other three of the 'four ladies'.<ref name=":5" /><ref name=":3" /><ref name=":1">Bogen, H. (1957). ''Main events in the history of Antarctic exploration''. Sandefjord: Norwegian Whaling Gazette, page 85</ref>

== Awards and honours ==
In 1998 and 2002, polar researchers investigated Caroline Mikkelsen’s landing and concluded it was on the Tryne Islands, rather than the Antarctic mainland.<ref name=":3" /><ref>{{Cite journal|last=Norman|first=F. I.|last2=Gibson|first2=J. a. E.|last3=Burgess|first3=J. S.|date=1998-10-01|title=Klarius Mikkelsen's 1935 landing in the Vestfold Hills, East Antarctica: some fiction and some facts|url=http://journals.cambridge.org/article_S0032247400025985|journal=Polar Record|volume=34|issue=191|pages=293–304|doi=10.1017/S0032247400025985|issn=1475-3057}}</ref><ref>{{Cite journal|last=Norman|first=F.i.|last2=Gibson|first2=J.a.e.|last3=Jones|first3=R.t.|last4=Burgess|first4=J.s.|date=2002-10-01|title=Klarius Mikkelsen's landing site: some further notes on the 1935 Norwegian visit to the Vestfold Hills, East Antarctica|url=http://journals.cambridge.org/article_S0032247400018015|journal=Polar Record|volume=38|issue=207|pages=323–328|doi=10.1017/S0032247400018015|issn=1475-3057}}</ref> Other research confirmed Christensen was the first to disembark on [[Scullin monolith|Scullin Monolith]] on 30 January 1937, making her the first woman to step on the Antarctic mainland.<ref name=":3" />

=== Role in Christensen Antarctic explorations ===
Christensen played a major role in her husband’s Antarctic expeditions. Archaeologist [[Waldemar Christofer Brøgger (geologist)|Waldemar Brøgger]], wrote in the cover story of the inaugural issue of the Norwegian magazine Verden I Bilder (The World in Pictures): "In all the excursions, Lars and Ingrid Christensen have been united in the undertaking—in thick and thin, in storm and bad weather, in good weather and joys. It is almost unique in the history of exploration that two persons have thus thriven for the same goal, kept the distant target in sight and never given up before achieving it... Ingrid Christensen’s part in the whole enterprise is not the smaller, by reason of her incredibly bold, fearless personality, and it is symbolically right that it should be she who, from an aircraft threw down the Norwegian flag."<ref>As cited in Bogen, H. (1957). ''Main events in the history of Antarctic exploration''. Sandefjord: Norwegian Whaling Gazette, page 66</ref>

=== Order of St Olav ===
For her contribution to Norway’s cause in America during the war and for her public efforts, Christensen received Norway’s Knighthood, First Class, [[Order of St. Olav|Order of St Olav]], in 1946.<ref name=":0" />

== Legacy ==

=== Name given to part of Antarctica ===
[[Ingrid Christensen Coast]] in [[East Antarctica]] was discovered and named by Klarius Mikkelsen in 1935.<ref name=":0" /><ref>{{Cite book|url=http://www.loc.gov/catdir/toc/ecip0517/2005023103.html|title=On the ice: an intimate portrait of life at McMurdo Station, Antarctica|last=Legler|first=Gretchen|publisher=Milkweed Editions|isbn=9781571312822|location=Minneapolis, Minn}}</ref>

=== In fiction ===
Christensen’s four journeys to Antarctica were fictionalised in the 2013 novel ''Chasing the Light''.<ref>{{Cite book|title=Chasing the Light|last=Blackadder|first=Jesse|publisher=HarperCollins|year=2013|isbn=9780132575539|location=Sydney|pages=}}</ref>

== References ==
{{reflist|35em}}

== Further reading ==
* Christensen, L. (1937). ''My last expedition to the Antarctic''. Oslo: Johan Grundt Tanum.
* Rabot, C. (1934, 13 January 1934). Voyage d'une femme dans L'Antarctique (Travels of a woman in the Antarctic). ''L'Illustration, 4741,'' 52–53.

{{Authority control}}

{{DEFAULTSORT:Christensen, Ingrid}}
[[Category:1891 births]]
[[Category:1976 deaths]]
[[Category:People from Sandefjord]]
[[Category:Norwegian polar explorers]]
[[Category:Explorers of Antarctica]]
[[Category:Female polar explorers]]