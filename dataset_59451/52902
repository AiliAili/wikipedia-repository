{{primary sources|date=January 2016}}
{{Infobox journal
| title  = Spunti e ricerche
| cover  = [[File:Logo for Spunti e ricerche; rivista d%27italianistica.png]]
| editor = Raffaele Lampugnani, Annamaria Pagliaro, Antonio Pagliaro, Carolyn James.
| discipline = [[Italian studies]]
| language = English, Italian
| abbreviation = Spunti ric.
| publisher = Spunti e ricerche
| country = Australia
| frequency = Annually
| history = 1985–present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.spuntiericerche.com/
| link1  = http://www.spuntiericerche.com/issue/current
| link1-name = Online access
| link2  = http://www.spuntiericerche.com/issue/archive
| link2-name = Online archives
| JSTOR  =
| OCLC  = 800268615
| LCCN  =
| CODEN  =
| ISSN  = 0816-5432
| eISSN  = 2200-8942
}}
'''''Spunti e ricerche''''' is an annual [[Peer review|peer-reviewed]] [[academic journal]] that covers research in [[Italian studies]]. Individual volumes often consist of articles on a broadly defined theme, on a particular writer, or on various subjects. The [[editors-in-Chief]] are Raffaele Lampugnani ([[Monash University]]), Annamaria Pagliaro (Monash University), Antonio Pagliaro ([[La Trobe University]]), and Carolyn James (Monash University). Although other Australian journals pre-dated ''Spunti e ricerche'', such as  the now defunct ''Altro Polo'' (1978-1996), ''Spunti e ricerche'' is the oldest active academic journal in Australia specifically devoted to Italian studies.{{cn|date=January 2016}}

==History==
''Spunti e ricerche'' was the brain child of a number of tutors from the Italian Department of the [[University of Melbourne]] in 1981-1982{{cn|date=September 2012}} and the first volume appeared 1985. It was intended to foster, increase, and diversify Australian research and enquiry into Italian studies, specifically in the fields of [[Italian literature]], [[Italian politics|politics]], [[Italian language|linguistics]], [[Italian history|history]], society, [[Italian cinema|cinema]], and [[Italian art|art]], but also had the broad aim to encourage manifestations of the culture by Italians, or about Italians, in Australia.
==Abstracting and indexing==
''Spunti e ricerche'' is indexed and abstracted in:
* [[International Bibliography of Book Reviews of Scholarly Literature on the Humanities and Social Sciences]]
* [[International Bibliography of Periodical Literature]]
* [[Modern Language Association|MLA International Bibliography]]

==External links==
* {{Official|http://www.spuntiericerche.com/}}

[[Category:Publications established in 1985]]
[[Category:European studies journals]]
[[Category:Annual journals]]
[[Category:Multilingual journals]]