<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Zephyr
 | image=RAE ZephyrA.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Light sports aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=Aero Club of the Royal Aircraft Establishment
 | designer=Samuel Childs
 | first flight=3 September 1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}The '''RAE Zephyr''' was a single-seat, single-engined light [[pusher configuration]] [[biplane]] designed and built by the Aero Club of the [[Royal Aircraft Establishment|Royal Aircraft Establishment (RAE)]] for the [[Lympne light aircraft trials|1923 Lympne Motor Glider Competition]]. At a late stage the Aero Club chose to enter the more promising [[RAE Hurricane]] instead, using the Zephyr's engine, and the Zephyr itself was abandoned.

==Design and development==
The Zephyr<ref name="OrdH">{{Harvnb|Ord-Hume|2000|pages=454}}</ref><ref name="AJJ">{{Harvnb|Jackson|1960|pages=409}}</ref> was the first of three light aircraft designed and built by the Aero Club of the RAE. It was designed by Samuel Childs. As the Club originally considered it as an entrant to the [[Lympne light aircraft trials|Lympne Motor Glider Competition]] where the ''Daily Mail'' prize of £1000 for a 50-mile flight was limited to aircraft with engines of less than 750 cc capacity, it was fitted with a 600 cc Douglas flat-twin engine that produced only about 20&nbsp;hp (15&nbsp;kW).<ref>According to Jackson, the engine was a 500 cc unit and by implication not the one used in the Hurricane. Ord-Hume states the same engine was used, but puts the power at 17 hp for the Zephyr and 21.5 hp for the Hurricane</ref>

The Zephyr was a two bay biplane with wings without stagger or sweep and of constant chord with square tips.<ref name="OrdH"/><ref name="AJJ"/> It was a pusher design, reminiscent of the Royal Aircraft Factory F.E series, for example the [[Royal Aircraft Factory F.E.8|FE8]], with a full fuselage replaced with a pod or nacelle with the cockpit and the engine behind it, the empennage supported on an open frame. The Zephyr's pilot sat under the front wing with a long but downward sloping nose ahead of him. Four booms ran rearwards from the wings, two on each side converging in the vertical plane from the inner interplane struts to the tail, with rectangular bracing to stiffen them. The tailplane was mounted at the meeting of the booms and twin triangular fins, each bearing a near rectangular rudder were placed at the ends of each vertical pair of booms. The twin-wheeled main undercarriage was supplemented bu twin sprung tails skids, one under each fin.

The Zephyr flew for the first time with [[George Bulman (pilot)|Paul Bulman]] in control on 3 September 1923,<ref name="OrdH"/><ref name="AJJ"/> with just over a month before the start of the Lympne Trials on 8 October. It was certainly later than intended, for the Zephyr had been registered as a competitor in the Vauvilles (near [[Cherbourg]]) Light Plane and Glider event which ended on 26 August.<ref>[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200496.html The Vauvilles entrants ''Flight'' 16 August 1923]</ref> The first flight went well, and Bulman reported excellent handling.<ref>[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200548.html ''Flight'' 13 September 1923]</ref> However, by this time the Aero Club had designed and built the much more modern looking Hurricane, a cantilever monoplane with less air resistance and slightly lower weight, though no lightweight. The Zephyr was therefore even more underpowered than the Hurricane, and it was decided to put the Douglas engine into the latter aircraft and enter it for the trials.<ref name="OrdH"/> The wing area of the biplane was more than three times that of the Hurricane, making for such a low wing loading (2.54 b/sq. ft. or 12.4&nbsp;kg/m<sup>2</sup>) that the Zephyr would have been hard to fly except in a dead calm.<ref name="OrdH"/>  With the loss of its engine the sole Zephyr, registered<ref name="reg">[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EBGW CAA registration of ''G-EBGW'']</ref> ''G-EBGW'' was abandoned, though not destroyed until 1925.<ref name="AJJ"/><ref name="reg"/>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref={{harvnb|Ord-Hume|2000|page=454}}
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST ch3oose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=29
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=8
|height in=2
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=400
|empty weight note=
|gross weight kg=
|gross weight lb=635
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=600 cc [[Douglas (motorcycles)|Douglas]] flat twin
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=17<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=52.5
|max speed kts=
|max speed note=Ord-Hume gives range 50-55 mph
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=45
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=220
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Notes and citations===
{{reflist}}

===Cited sources===
{{commons category|RAE Zephyr}}
{{refbegin}}
*{{cite book |title= British Civil Aircraft 1919-59|last=Jackson|first=A.J.| year=1960|volume= 2|publisher=Putnam Publishing |location=London |isbn=|ref=harv}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6 |ref=harv}}
{{refend}}

<!-- ==External links== -->
{{RAE Aero Club aircraft}}

{{DEFAULTSORT:Rae Zephyr}}
[[Category:British sport aircraft 1920–1929]]