{{Orphan|date=February 2014}}

'''Juan Palarea y Blanes''', also known by his alias '''el Médico''' (December 27, 1780 - March 7, 1842) was a Spanish guerrillero commander during the Peninsular War, Medical Practitioner, and Spanish Politician.

== Early life ==

Born in the region of Murcia on December 27, 1780 to a middle-class family in the town of San Andrés. His primary education was in the Franciscan seminary of San Fulgencio, where he left school before graduating. He went on to study medicine in the city of Zaragoza, and later went to practice medicine in Villaluenga de la Sagra, in the provence of Toledo.

== Peninsular War 7 ==

In May 1808 in the castle of Águila in the town of [[Villaluenga de la Sagra|Villauenga de la Sagra]], Palarea organized a force of fourteen men to lay skirmish against forces loyal to [[Joseph Napoleon|Joseph-Napoleon Bonaparte]].

The first combat took place near the river of Guadarrama, where Palarea and his forces laid ambush on a French detachment of twenty soldiers. Serving as irregulars, El Médico's band of guerillas fought in the battles of Alhama, Las Navas del Marqués and San Martin de Valdeiglesias. Between 1808 and 1810 the guerillas fought skirmishes in Marrupe, Navalcarnero, Galapagar, Zarzuela del Monte, pinar de Trabadillo, Oño bridge, Estreño bridge, and in August 1810 the guerillas made an attack within the grounds of [[Casa de Campo]], once the royal hunting ground for Spanish monarchs and is in present day, the largest city park in Madrid. El Médico was forced to retreat in the Valle de Tiétar, and at the end of 1810 his force numbered 270 volunteers and Palarea received the rank of [[Lieutenant colonel|Teniente Coronel]].

In the Battle of the Chapel of Yuncler, Palarea and his guerillas attacked a detachment of French troops who retreated into the Chapel of Yuncler. In noticing a gust of wind flowing towards the chapel, Palerea asked the townspeople to bring sulfur and chile powder, Juliana Carrillo, a local townswomen took it upon herself to gather the supplies needed and ignited a small fire near the chapel. The smoke became unbearable for the soldiers inside and they were forced to surrender, for this action Palerea would later be considered for the [[Laureate Cross of Saint Ferdinand]].

In 1811 with the command of light horse and irregulars Palerea collaborated with the fifth regiment of "Escuadrones Francos Numantinos" in the [[Battle of Salamanca|Battle of Arapiles]]. Their main role was to disrupt the communications around the French general [[Auguste Marmont|Marmont]], providing assistance to the [[Arthur Wellesley, 1st Duke of Wellington|Earl of Wellington's]] combined forces of British and Portuguese troops. In 1812, he was present at the signing of the constitution of the 25 of September. Soon after he was named in command of his own regiment of horse titled "Regimiento de Húsares Numantinos", under the Division of Navarre. With this regiment he took command at the battle of Los Altos Sorauren in 1814.

In 1815 he married Maria de Soto Diaz and they settled in Villafranca de los Barros in Badajoz, in 1820 they moved to Madrid.

== Liberal Triennium and the Restoration of the Monarchy ==

During the years of the [[Trienio Liberal|Liberal Triennium]], Palerea was elected to the Spanish parliament and became one of the members of the liberal exiles.

During the years of the [[Ominous Decade]] (1823-1833), Palarea travelled to [[Santoña]] to establish a militia to combat Royalist forces that were being aided by the French. Palerea was defeated in a battle against the French general D'Albignal near León and Oviedo, and was again defeated by Royalist general Roselló in the [[:es:Gallegos del Campo|Gallegos del Campo]] where he and his troops were forced to retreat towards [[Zamora, Spain|Zamora]], during this retreat Palarea was captured by French general Bourque and was taken prisoner to France. Having escaped to England Palarea made intentions to restore the democratic order to Spain, until returning to Spain in 1833.

== First Carlist War ==

In 1835 under the government of Queen Maria Cristina, Palarea was named in command of the armed forces of Aragón and Mariscal de Campo, his command twice defeated Carlist general [[Ramón Cabrera y Griñó|Ramon Cabrera]]. In 1836 Palarea was promoted to Capitán General of Valencia and Murcia and second in command of Jaén and Granada.

In January 1839 Palarea was elected as a senator and on October 7, 1841 he was accused in a corruption scandal in the penal system of Cartagena. He died in Cartegena on 7 March 1842 of natural causes.

== Honors ==

The street 'Calle General Palarea" in the town of San Basilio in Murcia is named after him.

== References ==

{{reflist}}

* Molina Carrión, F. (2009). La Guerra de la Independencia en el Priorato de San Juan (1808-1814) (pgs. 178-182). Diputación de Ciudad Real. ISBN 9788477892625.
* de los Reyes, A. (2005). De San Fulgencio a Paco Rabal : 33 biografías murcianas (pgs. 137-140). Murcia: Comunidad Autónoma. Dirección General de Bellas Artes y Bienes Culturales. ISBN 84-606-3816-2.
* Rodríguez Solís, E. (1887). Los guerrilleros de 1808 : Historia popular de la Guerra de la Independencia. (pg. 719). Madrid : Fernando Cao y Domingo de Val, 1887 (facsimil 2008):Alicante : Biblioteca Virtual Miguel de Cervantes.
* Sancho-Miñano Servet, L. (1992). El General D. Juan Palarea Blanes. Oviedo: Museo del Niño. ISBN 978-84-86911-51-5.


{{DEFAULTSORT:Blanes, Juan Palarea y}}
[[Category:1780 births]]
[[Category:1842 deaths]]
[[Category:Guerrillas]]
[[Category:Murcian military personnel]]
[[Category:Spanish politicians]]
[[Category:18th-century Spanish people]]
[[Category:19th-century Spanish physicians]]