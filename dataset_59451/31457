{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Bundesarchiv DVM 10 Bild-23-61-09, Linienschiff "SMS Helgoland".jpg|300px|alt=Large gray battleship at sea. Dark smoke streams back from its three closely arranged funnels.]]
| Ship caption = SMS ''Helgoland'' {{circa|1911–1917}}
}}
{{Infobox ship class overview
| Name = ''Helgoland'' class
| Builders = 
| Operators = {{navy|German Empire}}
| Class before = {{sclass-|Nassau|battleship|4}}
| Class after = {{sclass-|Kaiser|battleship|4}}
| Subclasses = 
| Cost = 
| Built range = 1908–12
| In commission range = 1911–20
| Total ships planned = 4
| Total ships completed = 4
| Total ships lost = 0
| Total ships retired = 4
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship type = [[Battleship]]
| Ship displacement =* Designed: {{convert|22808|t|LT|abbr=on}}
* Full load: {{convert|24700|t|LT|abbr=on}}
  
| Ship length = {{convert|167.20|m|ftin|abbr=on}}
| Ship beam = {{convert|28.50|m|ftin|abbr=on}}
| Ship draft = {{convert|8.94|m|ftin|abbr=on}}
| Ship propulsion =* 3 shaft 4 cyl vertical triple expansion
* {{convert|{{convert|28000|PS|ihp|0|disp=number}}|ihp|kW|abbr=on|lk=on}}
  
| Ship speed = {{convert|20.5|kn}}
| Ship range = {{convert|5,500|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
| Ship boats = 
| Ship crew =*   42 officers
* 1071 enlisted men
  
| Ship armament =* 12 × [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} guns]]
* 14 × [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} guns]]
* 14 × [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} guns]]
*  6 × {{convert|50|cm|in|abbr=on}} torpedo tubes
  
| Ship armor =* [[Belt armor|Belt]]: {{convert|300|mm|in|abbr=on}}
* [[Deck (ship)|Decks]]: {{convert|63|mm|in|abbr=on}}
* [[Barbette]]s: 300&nbsp;mm
* [[Turret]]s: 300&nbsp;mm
  
| Ship notes = Source:{{sfn|Gröner|pp=23–24}}
}}
|}

The '''''Helgoland'' class''' was the second [[ship class|class]] of German [[dreadnought]] [[battleship]]s. Constructed from 1908 to 1912, the class comprised four ships: {{SMS|Helgoland||2}}, the [[lead ship]]; {{SMS|Oldenburg||2}}; {{SMS|Ostfriesland||2}}; and {{SMS|Thüringen||2}}. The design was a significant improvement over the previous {{sclass-|Nassau|battleship|0}} ships; they had a larger [[main battery]]—{{convert|30.5|cm|in|abbr=on}} main guns instead of the {{convert|28|cm|in|abbr=on}} weapons mounted on the earlier vessels—and an improved [[Ship#Propulsion systems|propulsion system]].{{sfn|Gardiner & Gray|p=146}} The ''Helgoland''s were easily distinguished from the preceding ''Nassau''s by the three funnels that were closely arranged, compared to the two larger funnels of the previous class. The ships retained the hexagonal main battery layout of the ''Nassau'' class.{{sfn|Gardiner & Gray|p=146}}

The ships served as a unit in the I Division, I Battle Squadron alongside the ''Nassau''-class ships in the II Division of the I Battle Squadron. They saw combat during [[World War I]], including the [[Battle of Jutland]] in the North Sea and the [[Battle of the Gulf of Riga]] in the Baltic. All four survived the war, but were not taken as part of the German fleet that was interned at [[Scapa Flow]]. When the German ships at Scapa Flow [[Scuttling of the German fleet in Scapa Flow|were scuttled]], the four ''Helgoland''s were ceded as [[war reparations]] to the victorious Allied powers in the sunken ships' stead. ''Ostfriesland'' was taken by the [[US Navy]] and expended as a target during [[Billy Mitchell (general)|Billy Mitchell]]'s air power demonstration in July 1921. ''Helgoland'' and ''Oldenburg'' were allotted to Britain and Japan respectively, and broken up in 1921. ''Thüringen'' was delivered to France in 1920, and was used as a [[target ship]] for the French navy. The ship was eventually broken up between 1923 and 1933.{{sfn|Gröner|p=25}}

== Design ==
The [[Triple Entente]] between the United Kingdom, France, and Russia had been signed in 1907. Germany had become significantly isolated—on the [[Continental Europe|Continent]], Germany was hemmed in by France in the west and Russia in the east, and the UK, with her powerful navy, was capable of blocking German access to the world shipping lanes. Admiral von Tirpitz reacted to this development with the request for newer and stronger capital ships. His thoughts on the matter were, "The aim which I had to keep in view ... for technical and organizing reasons as well as reasons of political finance was to build as steadily as possible."{{sfn|Gardiner & Gray|p=135}} His appeal came in the form of the proposed [[German Naval Laws|Second Amendment to the Naval Law]], which was passed on 27 March 1908.{{sfn|Gardiner & Gray|p=135}}

For the second class of German dreadnoughts, there was considerable debate as to what changes would be made from the first design. In May 1906, the ''Reichsmarineamt'' (RMA, Imperial Navy Office) received word that the British were building battleships equipped with {{convert|13.5|in|cm|adg=on}} guns.{{sfn|Staff (''German Battleships'')|p=33}} As a result, the General Navy Department advocated increasing the caliber of the main battery from {{convert|28|cm|in|abbr=on}} to {{convert|30.5|cm|in|abbr=on}}. Admiral von Tirpitz was reluctant to agree to this change, as he wished to avoid escalating the arms race with Britain.{{sfn|Staff (''German Battleships'')|p=34}}

Admiral von Tirpitz's hesitation at increasing the armament of the new ships was lost when it became known in early 1907 that the [[United States Navy]] was building battleships with 30.5&nbsp;cm guns. In March 1907, von Tirpitz ordered the Construction Department to prepare a design with 30.5&nbsp;cm guns and {{convert|320|mm|in|abbr=on}} thick belt armor.{{sfn|Staff (''German Battleships'')|p=34}} Some dispute remained over the arrangement of the main battery. The two {{sclass-|Minas Geraes|battleship|2}}s being built for Brazil mounted the same number of guns, but in a more efficient arrangement. Superfiring turret pairs were placed on either end of the ship, with two wing turrets amidships. Admiral von Tirpitz favored adopting this arrangement for the ''Helgoland'' class, but the Construction Department felt two superfiring turrets could be easily disabled by a single hit. As a result, the hexagonal arrangement of the preceding ''Nassau''s was retained.{{sfn|Staff (''German Battleships'')|pp=34–35}}

The Naval Law stipulated that the lifespan of large warships was to be reduced from 25&nbsp;years to 20&nbsp;years; this was done in an effort to force the [[Reichstag (German Empire)|Reichstag]] to allocate funds for additional ships. The reduction necessitated the replacement of the [[coastal defense ship]]s of the {{sclass-|Siegfried|coastal defense ship|5}} and {{SMS|Oldenburg|1884|2}} classes as well as the {{sclass-|Brandenburg|battleship|2}}s. The battleships that von Tirpitz had failed to secure in the First Amendment to the Naval Law of 1906 were now approved by the Reichstag. The Naval Law also increased the naval budget by an additional 1 billion [[German gold mark|marks]].{{sfn|Gardiner & Gray|p=135}} After the four {{sclass-|Sachsen|ironclad|2}}s had been replaced by the four ''Nassau''s, three of the ''Siegfried''-class ships—{{SMS|Siegfried||2}}, {{SMS|Beowulf||2}}, {{SMS|Frithjof||2}}—and the unique coastal defense ship {{SMS|Oldenburg|1884|2}} were the next slated to be replaced. The ''Helgoland''-class ships—SMS ''Helgoland'', SMS ''Ostfriesland'', SMS ''Thüringen'', and SMS ''Oldenburg''—were ordered under the provisional names ''[[Ersatz]] Siegfried, Ersatz Oldenburg, Ersatz Beowulf,'' and ''Ersatz Frithjof'', respectively.{{sfn|Gröner|p=24}}{{efn|name=provisional names}}

=== General characteristics ===
The ''Helgoland''-class ships were longer than their predecessors, at {{convert|167.2|m|ftin|abbr=on}} [[length overall|overall]]. The ships had a [[beam (nautical)|beam]] of {{convert|28.5|m|ftin|abbr=on}} and at full load a [[draft (ship)|draft]] of {{convert|8.94|m|ftin|abbr=on}}. The ships were significantly heavier than the ''Nassau'' class; the ''Helgoland''-class ships displaced {{convert|22808|t|LT}} at a standard load, and {{convert|24700|t|LT|-1}} at full load, nearly {{convert|4000|t|LT|-2}} more than the earlier ships.{{sfn|Gröner|pp=23–24}}{{efn|name=Nassau displacements}} The ships had 17 [[watertight compartment]]s and a double bottom for 86% of the length of the hull.{{sfn|Gröner|p=23}}

The class had greatly improved handling characteristics over the preceding ''Nassau'' class. The ''Helgoland''s were much better sea boats and did not suffer from the severe rolling that the ''Nassau''s did. The ships were responsive to the helm, and had a tight turning radius, and lost only minimal speed during swells. The ships lost up to 54% of their speed at hard rudder, and would heel up to 7°.{{sfn|Gröner|p=25}} For comparison, the earlier ''Nassau''s lost up to 70% speed and held a 12° heel with the rudder hard over.{{sfn|Gröner|p=23}}

=== Propulsion ===

The ''Helgoland''-class ships retained older triple-expansion steam engines rather than the new steam turbines in use in the British Royal Navy. This decision was based solely on cost: at the time, [[Parsons Marine Steam Turbine Company|Parsons]] held a monopoly on steam turbines and required a 1&nbsp;million [[gold mark]] royalty fee for every turbine engine.{{sfn|Staff (''German Battleships'')|p=35}} The triple-expansion engines were three-shaft, four-cylinder engines arranged in three engine rooms. Each shaft drove a four-bladed [[screw propeller]] that was {{convert|5.1|m|ftin|abbr=on}} in diameter. The engines were powered by 15 marine-type boilers with two fireboxes apiece for a total of 30. The engines were rated at {{convert|{{convert|28000|PS|ihp|0|disp=number}}|ihp|kW|abbr=on|lk=on}} with a top speed of {{convert|20.5|kn|lk=in}}. On trials, the power-plant produced up to {{convert|{{convert|35500|PS|ihp|0|disp=number}}|ihp|kW|abbr=on|lk=on}}, and a top speed of {{convert|21.3|kn}}. The ships carried {{convert|3200|t|LT|-1}} of coal, and were later modified to carry an additional {{convert|197|t|LT}} of oil that was to be sprayed on the coal to increase its burn rate.{{efn|name=coal quality}} At full fuel capacity, the ships could steam for {{convert|5,500|nmi|lk=in}} at a speed of {{convert|10|kn}}. The ships' electrical power was provided by eight turbo-generators that produced 2,000&nbsp;kW (225&nbsp;V).{{sfn|Gröner|p=24}}

=== Armament ===

[[File:SMS Helgoland bridge.PNG|thumb|''Helgoland''{{'}}s bridge and forward main battery turrets|alt=A large warship's bridge and two gun turrets]]
[[File:Nassau class main weapon.svg|thumb|Like the preceding ''Nassau'' class ''(pictured)'', the ''Helogoland''s used an unusual hexagonal arrangement for their main battery]]

Like the ''Nassau'' class which preceded it, the ''Helgoland''-class ships carried their main armament in an unusual hexagonal configuration. Twelve [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} SK L/50]]{{efn|name=gun nomenclature}} guns were emplaced in long-trunk Drh LC/1908 mountings, an improved version of the previous LC/1907 and LC/1906 mounts used in the ''Nassau'' class. The guns were arranged in pairs in six twin [[gun turret]]s, with one turret each fore and aft, and two on each flank of the ship.{{sfn|Gardiner & Gray|p=146}} The guns could initially be depressed to −8° and elevated to 13.5°, although the turrets were later modified to allow −5.5° depression and 16° of elevation.{{sfn|Gröner|p=24}} The guns fired {{convert|405|kg|lb|adj=on}} projectiles at a [[muzzle velocity]] of {{convert|855|m/s|ft/s|abbr=on}}; at 13.5°, this provided a maximum range of {{convert|18700|m|yd|abbr=on}}, and with the upgraded 16° elevation, the range was extended to {{convert|20500|m|yd|abbr=on}}.{{sfn|Gardiner & Gray|p=140}} The guns had a total of 1,020 rounds for 85 shells per gun.{{sfn|Gröner|p=24}}

The ships' secondary armament consisted of fourteen [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45 guns]], which were mounted in [[casemate]]s. The guns fired {{convert|45.3|kg|lb|adj=on}} shells at a muzzle velocity of {{convert|840|m/s|ft/s|abbr=on}}. The guns could be elevated to 19°, which provided a maximum range of {{convert|14950|m|yd}}. The ships also carried fourteen [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45 guns]], also in casemates. These guns fired a {{convert|10|kg|lb|adj=on}} projectile at {{convert|650|m/s|ft/s|abbr=on}}, and could be trained up to 25° for a maximum range of {{convert|9600|m|yd|abbr=on}}.{{sfn|Gardiner & Gray|p=140}} After 1914, two 8.8&nbsp;cm guns were removed and replaced by two 8.8&nbsp;cm Flak guns, and between 1916 and 1917, the remaining twelve 8.8&nbsp;cm casemated guns were removed.{{sfn|Gröner|p=24}} These anti-aircraft guns fired a slightly lighter {{convert|9.6|kg|lb|adj=on}} shell at {{convert|770|m/s|ft/s|abbr=on}}. They could be elevated to 45° and could hit targets {{convert|11800|m|yards|abbr=on}} away. The ''Helgoland''-class ships were further armed with six {{convert|50|cm|in|abbr=on}} submerged [[torpedo tube]]s. One tube was mounted in the bow, another in the stern, and two on each broadside, on either ends of the [[torpedo bulkhead]].{{sfn|Gardiner & Gray|p=140}}

=== Armor ===
The ''Helgoland''-class ships were equipped with [[Krupp cemented armor]], in almost the same layout as in the preceding ''Nassau''-class ships. The only major differences were slight increases in the armor protection for the main and secondary batteries, and a much thicker roof for the forward [[conning tower]]. The ships had an [[armored belt]] that was {{convert|30|cm|in|abbr=on}} thick at its strongest points, where it protected the ship's vitals, and as thin as {{convert|8|cm|in|abbr=on}} in less critical areas, such as the bow and stern. Behind the main belt was a [[torpedo bulkhead]] {{convert|3|cm|in|abbr=on}} thick. The ships' decks were armored, between {{convert|5.5|and|8|cm|in|abbr=on}} thick. The forward conning tower on each vessel had a roof that was {{convert|20|cm|in|abbr=on}} thick, and sides {{convert|40|cm|in|abbr=on}} thick. The aft conning tower was not as heavily armored, with only a {{convert|5|cm|in|abbr=on}} thick roof and {{convert|20|cm|in|abbr=on}} sides. The main battery turrets had roofs that were {{convert|10|cm|in|abbr=on}} thick, and 30&nbsp;cm<!-- converted above--> sides. The casemated secondary battery had {{convert|17|cm|in|abbr=on}} worth of armor protection, and 8&nbsp;cm<!-- converted above--> thick gun shields. The ''Helgoland''s were also fitted with [[anti-torpedo net]]s, but these were removed after 1916.{{sfn|Gröner|p=24}}

== Construction ==
[[File:Bundesarchiv Bild 146-2007-0101, Kaiserliche Werft Kiel, Linienschiff "Helgoland".jpg|thumb|''Helgoland'' in a [[Dry dock#Floating|floating dry dock]]|alt=Large ship atop a floating platform with two tall walls running along her side, and construction equipment between the walls and the ship]]

Four ships of the class were ordered, under the provisional names ''Ersatz Siegfried'' (''Helgoland''), ''Ersatz Oldenburg'' (''Ostfriesland''), ''Ersatz Beowulf'' (''Thüringen''), and ''Ersatz Frithjof'' (''Oldenburg''), as replacements for three of the [[coastal defense ship]]s of the ''Siegfried''-class, and the unique coastal defense ship SMS ''Oldenburg''. SMS ''Helgoland'' was built at [[Howaldtswerke]], [[Kiel]]. She was laid down on 24 December 1908, launched 25 August 1909, and commissioned nearly two years later on 23 August 1911.

SMS ''Ostfriesland'' was built at [[Kaiserliche Werft Wilhelmshaven]]. She was laid down 19 October 1908, launched five days after her sister ''Helgoland'', on 30 August 1909, and commissioned 1 August 1911. SMS ''Thüringen'' was built by [[AG Weser]] in [[Bremen]]. She was laid down on 7 November 1908, launched on 27 November 1909, and commissioned on 10 September 1911. SMS ''Oldenburg'', the final vessel, was built by [[Schichau-Werke|Schichau]] in [[Danzig]]; she was laid down 1 March 1909, launched 30 June 1910, and commissioned on 1 May 1912.{{sfn|Gardiner & Gray|p=146}}{{sfn|Gröner|pp=24–25}}

===Ships===
{| class="wikitable"
! align = center|Ship
! align = center|Builder
! align = center|Namesake
! align = center|Laid down
! align = center|Launched
! align = center|Commissioned
! align = center|Fate
|-
|{{SMS|Helgoland||2}}
|[[Howaldtswerke-Deutsche Werft|Howaldtswerke Werft]], [[Kiel]]
|[[Heligoland]]
|11 November 1908
|25 September 1909
|23 August 1911
|Broken up at [[Morecambe]], 1921
|-
|{{SMS|Ostfriesland||2}}
|[[Kaiserliche Werft Wilhelmshaven|Kaiserliche Werft]], [[Wilhelmshaven]]
|[[East Frisia]]
|19 October 1908
|30 September 1909
|1 August 1911
|Sunk as target off [[Cape Hatteras]], 21 July 1921
|-
|{{SMS|Thüringen||2}}
|[[AG Weser]], [[Bremen]]
|[[Thuringia]]
|2 November 1908
|27 November 1909
|1 July 1911
|Sunk as target off [[Gavres]], August 1921
|-
|{{SMS|Oldenburg||2}}
|[[Schichau-Werke]], [[Gdańsk|Danzig]]
|[[Duchy of Oldenburg]]
|1 March 1909
|30 June 1910
|1 May 1912
|Broken up at Dordrecht, 1921
|}

== History ==
The ''Helgoland''-class ships operated as a unit in the [[High Seas Fleet]]; they served as the I&nbsp;Division of the I&nbsp;Battle Squadron. The ships of the class participated in several fleet operations in the [[North Sea]], including the sortie on 31 May 1916 that resulted in the [[Battle of Jutland]]. The ships also saw limited service in the [[Baltic Sea]], primarily during the abortive [[Battle of the Gulf of Riga]] in August 1915.

=== Raid on Scarborough, Hartlepool and Whitby ===
{{main|Raid on Scarborough, Hartlepool and Whitby}}
[[File:Bundesarchiv DVM 10 Bild-23-61-55, Linienschiff "SMS Ostfriesland".jpg|thumb|An illustration of {{SMS|Ostfriesland||2}} in 1911|alt=A battleship at sea with a smaller boat alongside]]

The first major operation of the war in which the ''Helgoland''-class ships participated was the raid on Scarborough, Hartlepool and Whitby on 15–16 December 1914. The raid was primarily conducted by the battlecruisers of the [[I Scouting Group]]. The ''Helgoland''-class ships, along with the {{sclass-|Nassau|battleship|5}}, {{sclass-|Kaiser|battleship|5}}, and {{sclass-|König|battleship|4}}es steamed in distant support of [[Franz von Hipper]]'s battlecruisers. [[Friedrich von Ingenohl]], the commander of the [[High Seas Fleet]], decided to take up station approximately in the center of the North Sea, about 130&nbsp;miles east of [[Scarborough, North Yorkshire|Scarborough]].{{sfn|Tarrant|p=31}}

The [[Royal Navy]], which had recently received the German code books captured from the beached cruiser {{SMS|Magdeburg||2}}, was aware that an operation was taking place, but was not sure where the Germans would strike. Therefore, the [[Admiralty]] ordered [[David Beatty, 1st Earl Beatty|David Beatty's]] 1st Battlecruiser Squadron, the six battleships of the 2nd Battle Squadron, and several cruisers and destroyers to intercept the German battlecruisers.{{sfn|Tarrant|p=31}} However, Beatty's task force nearly ran headlong into the entire High Seas Fleet. At 6:20, Beatty's destroyer screen came into contact with the German torpedo boat ''V155''. This began a confused, 2-hour long battle between the British destroyers and the German cruiser and destroyer screen, often at very close range. At the time of the first encounter, the ''Helgoland''-class battleships were less than {{convert|10|nmi|abbr=on}} away from the six British dreadnoughts; this was well within firing range, but in the darkness, neither British nor German admirals were aware of the composition of their opponents' fleets. Admiral Ingenohl, loathe to disobey the Kaiser's order to not risk the battlefleet without his express approval, concluded that his forces were engaging the screen of the entire [[Grand Fleet]], and so, 10 minutes after the first contact, he ordered a turn to port to a southeast course. Continued attacks delayed the turn, but by 6:42, it had been carried out.{{sfn|Tarrant|p=32}} For about 40&nbsp;minutes, the two fleets were steaming on a parallel course. At 7:20, Ingenohl ordered a further turn to port, which put his ships on a course for German waters.{{sfn|Tarrant|p=33}}

Many in the German navy were furious over Ingenohl's timidity, and his reputation suffered greatly. ''[[Großadmiral]]'' [[Alfred von Tirpitz]] remarked that "On 16 December, Ingenohl had the fate of Germany in the palm of his hand. I boil with inward emotion whenever I think of it."{{sfn|Tarrant|p=35}} The captain of the battlecruiser {{SMS|Moltke||2}} was even more scathing; he stated that Ingenohl had turned away "because he was afraid of eleven British destroyers which could have been easily eliminated ... Under the present leadership we will accomplish nothing."{{sfn|Tarrant|p=35}}

The ''Helgoland''-class ships also sortied from port to support the German battlecruisers during the [[Battle of Dogger Bank (1915)|Battle of Dogger Bank]], but did not actively engage British forces.{{sfn|Hore|p=68}}

=== Battle of the Gulf of Riga ===
{{main|Battle of the Gulf of Riga}}

On 3 August 1915, several heavy units of the High Seas Fleet were transferred to the Baltic to participate in a planned foray into the [[Gulf of Riga|Riga Gulf]]. The intention was to destroy the Russian naval forces in the area, including the [[pre-dreadnought]] {{ship|Russian battleship|Slava||2}}, and to use the minelayer {{SMS|Deutschland|1914|2}} to block the entrance to Moon Sound with [[naval mine]]s. The German forces, under the command of Vice Admiral Hipper, included the four {{sclass-|Nassau|battleship|0}} and four ''Helgoland''-class battleships, the battlecruisers {{SMS|Seydlitz||2}}, {{SMS|Moltke||2}}, and {{SMS|Von der Tann||2}}, and a number of smaller craft.{{sfn|Halpern|p=196}} The four ''Helgoland''s were not committed to the actual battle, however. For the duration of the operation, the ships were stationed outside the gulf in order to prevent Russian reinforcements from disrupting the laying of minefields.{{sfn|Halpern|p=197}}

The Russians' own minefields were larger than had been expected, and so clearing them took longer than the Germans had planned. This delay was compounded by stiff resistance from the Russian navy; ''Deutschland'' was ultimately unable to lay her mines. Reports of Allied submarine activity in the area prompted the withdrawal of the German naval force on the morning of 20 August.{{sfn|Halpern|pp=197–198}} Indeed, the battlecruiser ''Moltke'' had been torpedoed by the British submarine {{HMS|E1}} the day before, though only minor damage was sustained.{{sfn|Staff (''German Battlecruisers'')|p=15}}

=== Battle of Jutland ===
{{main|Battle of Jutland}}

The ships took part in the inconclusive Battle of Jutland on 31 May&nbsp;– 1 June 1916. For the majority of the battle, the I&nbsp;Battle Squadron formed the center of the [[line of battle]], behind Rear Admiral Behncke's III&nbsp;Battle Squadron, and followed by Rear Admiral Mauve's elderly [[pre-dreadnought]]s of the II&nbsp;Battle Squadron. ''Ostfriesland'' served as the division flagship, under the command of Vice Admiral E. Schmidt.{{sfn|Tarrant|p=286}}

[[File:SMS Helgoland illustration.jpg|thumb|left|An illustration of ''Helgoland'' with her main battery trained to starboard|alt=A large gray warship steams at full speed; thick black smoke pours from its three smoke stacks.]]

The ''Helgoland''-class ships first entered direct combat at 19:20 on the first day of the battle. ''Ostfriesland'', ''Helgoland'', and ''Thüringen'' began firing on {{HMS|Warspite|03|6}}, which, along with the other {{sclass-|Queen Elizabeth|battleship|2}}s of the 5th Battle squadron, had been pursuing the German battlecruiser force.{{sfn|Tarrant|p=142}} With the exception of ''Ostfriesland'', the firing only lasted for four minutes, because the German line had been in the process of turning to the east-northeast, and the ships quickly lost sight of the British battleships. ''Thüringen'' and ''Helgoland'' only fired around 20 main battery shells before they lost sight of their target.{{sfn|Tarrant|pp=142–143}} ''Ostfriesland'', however, was able to keep visual contact until 19:45, at which point she, too, ceased firing.{{sfn|Tarrant|p=143}} At 20:15, during the third ''Gefechtskehrtwendung'',{{efn|name=battle about-turn}} ''Helgoland'' was struck by a {{convert|15|in|cm|abbr=on}} shell in the forward part of the ship. The shell tore a {{convert|20|ft|m|adj=on}} hole in the hull and rained splinters on the foremost port side 5.9&nbsp;in gun; approximately 80&nbsp;tons of water entered the ship.{{sfn|Tarrant|pp=173, 175}}

At around midnight on 1 June, the ''Helgoland''- and ''Nassau''-class ships in the center of the German line came into contact with the British 4th Destroyer Flotilla. A chaotic night battle ensued, during which {{SMS|Nassau||2}} rammed the British destroyer {{HMS|Spitfire|1912|2}}. The 4th Flotilla broke off the action temporarily to regroup, but at around 01:00, unwittingly stumbled into the German dreadnoughts a second time.{{sfn|Tarrant|p=222}} ''Oldenburg'' and ''Helgoland'' opened fire on the two leading British destroyers, but a British shell destroyed ''Oldenburg''{{'}}s forward search light. Shell fragments rained down on the bridge and wounded the ship's captain, ''Kapitän'' Hopfner, and killed his second in command, ''Kapitänleutnant'' Rabius, along with a number of other men on the bridge, including the helmsman. ''Oldenburg'' was temporarily without anyone to steer the ship; she was in danger of ramming either the ship to her rear or to her front. ''Kapitän'' Hopfner, despite his injuries, took the helm and brought the ship back into line.{{sfn|Tarrant|p=223}}

Shortly after 01:00, ''Thüringen'' and ''Nassau'' encountered the British armored cruiser {{HMS|Black Prince|1904|2}}. ''Thüringen'' opened fire first and pummeled ''Black Prince'' with a total of 27 heavy-caliber shells and 24 rounds from her secondary battery. ''Nassau'' and ''Ostfriesland'' joined in, followed by {{SMS|Friedrich der Grosse|1911|2}}.{{sfn|Tarrant|p=225}}

{{quote
| [''Black Prince''] presented a terrible and awe-inspiring spectacle as she drifted down the line blazing furiously until, after several minor detonations, she disappeared below the surface with the whole of her crew in one tremendous explosion.{{sfn|Tarrant|p=225}}
}}

By this time, the 4th Destroyer Flotilla had been largely destroyed as a fighting unit. The few remaining, heavily damaged ships had been scattered and would take no further part in the battle.{{sfn|Tarrant|pp=225–226}}

Following the return to German waters, ''Helgoland'' and ''Thüringen'', along with the {{sclass-|Nassau|battleship|2}}s ''Nassau'', {{SMS|Posen||2}}, and {{SMS|Westfalen||2}}, took up defensive positions in the [[Jadebusen|Jade]] [[roadstead]] for the night.{{sfn|Tarrant|p=263}} During the battle, the ships suffered only minor damage. ''Helgoland'' was hit by a single 15&nbsp;in shell, but sustained minimal damage. The ''Oldenburg'' was hit by a shell from a secondary battery that killed 8 and wounded 14 men. ''Ostfriesland'' and ''Thüringen'' escaped the battle unscathed, although on the return to German waters, ''Ostfriesland'' struck a [[mine (naval)|mine]] and had to be repaired in [[Wilhelmshaven]].{{sfn|Tarrant|p=263}}

=== Post-war ===

[[File:Ostfriesland bombed by Mitchells team p19.jpg|thumb|''Ostfriesland'' burns after sustaining hits during bombing tests in July 1921|alt=Aerial view of a cloud of smoke rising from a battleship at sea]]

The ships of the class saw no further significant action during the war, and were ceded to the Allies under the terms of the [[Treaty of Versailles]].{{sfn|Hore|p=68}} All four ships were stricken from the German navy on 5 November 1919. ''Helgoland'' was taken by the British and was scrapped in 1921 in [[Morecambe]]. Her bow ornament was retained and was eventually returned to Germany; it is now on display in the [[Dresden]] army museum. ''Oldenburg'' was surrendered to the Japanese, but they did not take possession of the ship. Instead, they sold the vessel to a British salvage firm that scrapped it in [[Dordrecht]] in 1921.{{sfn|Gröner|p=25}} ''Thüringen'' was taken by France; the ship was nearly scuttled by her crew while en route to [[Cherbourg]] in 1920. She was used as a target until she was beached in 1923 at [[Gavres]]. She was broken up [[in situ]], but a large portion of the hull remains off shore.{{sfn|Hore|p=68}}

''Ostfriesland'' was ceded to the [[US Navy]], and was later used as a stationary target during a [[SMS Ostfriesland#United States service|demonstration of air power]], conducted by General [[Billy Mitchell (general)|Billy Mitchell]] on 21 July 1921 off [[Cape Henry]] in Virginia. The ship sank at 12:40 after sustaining several bomb hits and near misses.{{sfn|Gröner|p=25}} However, she likely would have avoided these had she been underway, and if she had been hit, damage control teams would have kept the ship afloat.{{sfn|Hore|p=68}}

== Notes ==

'''Footnotes'''

{{notes
| notes =

{{efn
| name = provisional names
| All German ships were ordered under provisional names; new additions to the fleet were given a letter, while ships that were intended to replace older vessels were ordered as "[[Ersatz]] (ship name)." An excellent example of this practice is the {{sclass-|Derfflinger|battlecruiser|2}}s: the [[lead ship]] {{SMS|Derfflinger}} was considered a new addition to the fleet, and was ordered as "K", while her sisters {{SMS|Lützow||2}} and {{SMS|Hindenburg||2}} were ordered as ''Ersatz Kaiserin Augusta'' and ''Ersatz Hertha'', as replacements for two older ships. See: {{harvnb|Gröner|p=56}}.
}}

{{efn
| name = Nassau displacements
| The ''Nassau''-class battleships displaced 18,570&nbsp;tons at the designed weight, and 21,000&nbsp;tons at a full load. See: {{harvnb|Gröner|p=23}}.
}}

{{efn
| name = coal quality
| Because of the wartime situation, Germany had limited access to high quality coal, but was able to acquire lower-grade coal for its ships. The higher quality coal was generally reserved for the smaller craft, whose crews were less able to clean the boilers at the increased rate demanded by the low-quality coal. As a result, German capital ships were often supplied with poor coal, in the knowledge that their larger crews were better able to perform the increased maintenance. After 1915, the practice of spraying oil onto the low-quality coal was introduced, in order to increase the burn rate. See: {{harvnb|Philbin|p=56}}.
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick firing, while the L/50 denotes the length of the gun. In this case, the L/50 gun is 50 [[Caliber (artillery)|caliber]], meaning that the gun is 50 times as long as it is in diameter.
}}

{{efn
| name = battle about-turn
| This translates roughly as the "battle about-turn", and was a simultaneous 16-point turn of the entire [[High Seas Fleet]]. It had never been conducted under enemy fire before the Battle of Jutland. See: {{harvnb|Tarrant|pp=153–154}}.
}}

}}

'''Citations'''

{{reflist|25em}}

== References ==

{{portal|Battleships}}
{{commons category|Helgoland class battleship}}

* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | editor1-last=Jung
  | editor1-first=Dieter
  | editor2-last=Maass
  | editor2-first=Martin
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = Battleships of World War I
  | publisher = Southwater Books
  | location = London
  | isbn = 978-1-84476-377-1
  | oclc = 77797289
  | ref = {{sfnRef|Hore}}
  }}
* {{cite book
  | last = Philbin
  | first = Tobias R. III
  | year = 1982
  | title = Admiral Hipper: The Inconvenient Hero
  | publisher = John Benjamins Publishing Company
  | isbn = 978-90-6032-200-0
  | ref = {{sfnRef|Philbin}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2006
  | title = German Battlecruisers: 1914–1918
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-009-3
  | oclc = 64555761
  | ref = {{sfnRef|Staff (''German Battlecruisers'')}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 1
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-467-1
  | oclc = 705750106
  | ref = {{sfnRef|Staff (''German Battleships'')}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

{{Helgoland class battleship}}
{{WWIGermanShips}}

{{Featured article}}

{{DEFAULTSORT:Helgoland-class battleship}}
[[Category:Battleship classes]]
[[Category:Helgoland-class battleships| ]]
[[Category:World War I battleships of Germany|Helgoland class battleship]]