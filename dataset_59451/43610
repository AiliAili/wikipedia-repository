{|{{Infobox Aircraft Begin
  |name = D.I
  |image = SSW D.I in snow.jpg
  |caption = 
}}{{Infobox Aircraft Type
  |type = Single-seat biplane fighter
  |manufacturer = [[Siemens-Schuckert|Siemens-Schuckert Werke]]
  |designer = 
  |first flight = 1916
  |introduced = 1917
  |retired = 
  |status = 
  |primary user = ''[[Luftstreitkräfte]]''
  |more users = 
  |produced = 
  |number built = 95
  |unit cost = 
  |developed from = [[Nieuport 17]]
  |variants with their own articles = 
}}
|}

The '''Siemens-Schuckert D.I''' was a single-seat fighter built by [[Siemens-Schuckert|Siemens-Schuckert Werke]] in 1916. It was a German copy of the French [[Nieuport 17]] that was obsolete by the time it was available in numbers, so that it served mainly as an advanced trainer.

==Design and development==
The French Nieuport 17 fighter, which reached the front in March 1916, established such ascendency over existing German fighters that captured examples were supplied to several German aircraft manufacturers with a request to "study" the type.<ref name="Grey thet"/> The [[Siemens-Schuckert]] Werke produced the '''D.I''', based very closely on the Nieuport. The most important difference from the Nieuport 17 was the powerplant - instead of the [[Le Rhone 9J]] of the Nieuport (licensed and un-licensed versions of which were actually available in Germany at the time), Siemens-Schukert chose to use their own 110&nbsp;hp (82&nbsp;kW) Siemens-Halske Sh.I [[rotary engine]] - in which the cylinders, still attached to the propeller,  rotated at 900 rpm in one direction, with the crankshaft and internals rotating ''in the opposite direction'' at the same rate: producing an effective 1800 rpm. Visually, the effect of this was that in place of the Nieuport 17's circular, fully "closed" cowling the D.I had a small, close fitting, semi-circular cowling with an open bottom, to allow adequate cooling for the slow revving Siemens-Halske. This gives some photographs of the type the appearance of the earlier [[Nieuport 11]].

The wing area (14.4 m²) was a little less than the famous 15 m² of the Nieuport - the gap between the wings was reduced slightly, and the interplane struts were of steel tube, with broad wooden fairings, in place of the tape wrapped wooden struts of the original.

==Production history==
An order for 150 aircraft for the ''[[Luftstreitkräfte]]'' was placed on 25 November 1916,<ref name="Grey thet"/> but initial deliveries were slow, due to production difficulties with the complicated geared engines,<ref name="complete fighters"/> so that the type was not available for service until well into 1917, by which time many ''[[Jagdstaffeln]]'' were already equipped with the very much superior [[Albatros D.III]]. A backup order for a further 100 machines, placed on 21 March 1917, was cancelled, and only 95 were produced in total.<ref name="Grey thet"/>

Late production models were fitted with modified tailskids, and had large pointed spinners on their propellers.

==Operational History==
The S.S.W. D.I was obsolete before it was available in numbers, so that most of the examples produced were sent to the fighter training schools, although a few ''Jastas'' received one or two examples during 1917.<ref name="Grey thet"/>

The type is poorly documented - in particular no reliable details are available for its performance: the published figures are essentially those of the Nieuport 11, whereas such a close copy of the 17, with a powerplant of similar output, might have been expected to have a performance roughly equivalent to that of the original from which it was derived.

==Variants==
A single '''D.Ia''' was produced with a greater wing area - two examples of the '''D.Ib''' had a higher compression version of the Siemens-Halske Sh.I. Neither was ordered into production. Development continued through a series of '''D.II''' prototypes to the [[Siemens-Schuckert D.III]].

==Operators==
;{{flag|German Empire}}
*''[[Luftstreitkräfte]]''

==Specifications(D.I) ==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985), German Aircraft of the First World War  <ref name="Grey thet">{{cite book |last= Grey|first= Peter|author2=Thetford, Owen|title= German Aircraft of the First World War|year= 1962|publisher= Putnam|location= London|isbn= }}</ref><ref name="encycl aircraft">{{cite book |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=}}</ref>
|crew=One
|capacity=
|length main= 6.0 m
|length alt= 19 ft 8¼ in
|span main= 7.50 m
|span alt= 24 ft 7⅜ in
|height main= 2.59 m <ref name="complete fighters">{{cite book |last= Green|first= W |author2=Swanborough, G |title= The Complete Book of Fighters |edition= |year= 1994|publisher= Smithmark |isbn= 0-8317-3939-8}}</ref>
|height alt= 8 ft 5⅞ in
|area main= 14.4 m²
|area alt= 156 sq ft
|airfoil=
|empty weight main= 430 kg
|empty weight alt= 946 lb
|loaded weight main= 675 kg 
|loaded weight alt= 1,485 lb  
|useful load main=  
|useful load alt=  
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)=[[Siemens-Halske Sh.I]]
|type of prop=geared [[rotary engine]]
|number of props=1
|power main= 110 hp
|power alt= 82 kW
|power original=
   
|max speed main= 155 km/h
|max speed alt= 84 knots, 97 mph
|cruise speed main= 
|cruise speed alt= 
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 
|range alt= 
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 
|power/mass alt= 
|more performance= *'''Endurance:''' 2 h 20 min 
*'''Climb to 4,000 m (13,125 ft):''' 24 min 18 s
|armament=* One or two [[7.92x57 mm Mauser|7.92 mm]] [[LMG 08/15]] [[machine gun]]s
|avionics=

}}

==References==
{{Reflist}}

==Bibliography==
{{commons category|Siemens-Schuckert aircraft}}
*{{cite book |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=}}
*{{cite book |author1=Grey, Peter  |author2=Thetford, Owen  |lastauthoramp=yes |title= German Aircraft of the First World War (rev.edition)|year=1970|publisher= P:utnam & Company|location=London|issn=}}

<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=
|lists=
|see also=
}}
<!-- ==External links== -->
{{Siemens-Schuckert aircraft}}
{{Idflieg fighter designations}}
{{World War I Aircraft of the Central Powers}}

[[Category:German fighter aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|D.I]]