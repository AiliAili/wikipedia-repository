{{more footnotes|date=January 2014}}

{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image= Ivlia (bireme).tif
|Ship caption = ''Ivlia'' in [[Bay of Biscay]]
}}
{{Infobox ship career
|Ship country=Soviet Union/Ukraine
|Ship flag={{shipboxflag|Ukraine}}{{shipboxflag|USSR}}
|Ship name=''Ivlia''
|Ship namesake=
|Ship owner=
|Ship operator=
|Ship registry=
|Ship route=
|Ship ordered=
|Ship awarded=
|Ship builder=
|Ship original cost=
|Ship yard number=
|Ship way number=
|Ship laid down=September 1988
|Ship launched=August 1989
|Ship sponsor=
|Ship christened=
|Ship completed=
|Ship acquired=
|Ship commissioned=
|Ship recommissioned=
|Ship decommissioned=
|Ship maiden voyage= 
|Ship in service=
|Ship out of service=
|Ship renamed=
|Ship reclassified=
|Ship refit=
|Ship struck=
|Ship reinstated=
|Ship homeport=[[Odessa]] {{coord|46|28|N|30|44|E|}}
|Ship identification=
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=
|Ship status=
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Ship type=[[bireme]]
|Ship displacement=26 tonnes
|Ship tons burthen=
|Ship length={{convert|25.4|m|ftin|abbr=on}}
|Ship beam={{convert|4.6|m|ftin|abbr=on}}
|Ship height=
|Ship draught={{convert|0.8|m|ftin|abbr=on}}
|Ship depth=
|Ship hold depth=
|Ship decks=
|Ship deck clearance=
|Ship ramps=
|Ship ice class=
|Ship (equivalent) power= 88.9hp @ 9 Knots - continuous cruise, as below 31.1hp @ 2.15 knots (Oars only).
(Using Gerr Hull Ratio & Required Power)
|Ship propulsion=*large square sail, 55 sq.m
*50 oarsmen
|Ship sail plan=
|Ship speed=Maximum (with oars)  {{convert|5|knots|km/h}}
|Ship range=
|Ship endurance=
|Ship test depth=
|Ship boats=
|Ship capacity=
|Ship troops= 
|Ship complement=50 plus 3 officers (in antiquity)
|Ship crew=
|Ship time to activate=
|Ship sensors=
|Ship EW=
|Ship armament=bronze bow ram.<ref group="note">In form of wild boar's head.</ref>
|Ship armour=
|Ship armor=
|Ship aircraft=
|Ship aircraft facilities=
|Ship notes=
}}
|}

'''''Ivlia''''' ([[bireme]]) is a modern reconstruction of an [[ancient Greek]] rowing warship ([[galley]]) with oars at two levels and an important example of [[experimental archaeology]]. Between 1989 and 1994, this vessel undertook six comprehensive international historical and geographical expeditions in the footsteps of the ancient seafarers.<ref group="note">International expedition "Black Sea".</ref>

== Ship construction ==

The main sponsor of construction of the ship was the [[Black Sea Shipping Company]].<ref group="note">On all her voyages, Ivlia’s sail bore the [[acronym]] BLASCO – the logo of this shipping company.</ref> 
[[File:Ivliaplan1.jpg|left|thumb]][[File:Ivliaplan2.jpg|left|thumb]]

[[File:Ivlia2.jpg|thumb|Construction of the ship]]

The ship was constructed in 1989 at the [[Sochi]] Naval Shipyard,<ref group="note">Soviet Naval Shipyard №1.</ref> by a team under shipwright Damir S. Shkhalakhov, as well as the active participation of the future crew members. Ivlia was built from [[Durmast oak]] and [[Siberian larch]], the oars are of [[beech]]. Technical design of the project was carried out by specialists of the [[Admiral Makarov National University of Shipbuilding|Nikolayev University of Shipbuilding]].<ref>{{cite web|url=http://www.yachts.mykolayiv.com/index.html |title=Проектно-конструкторский Центр "ЯХТА" - проектирование парусно-моторных и моторных яхт |publisher=Yachts.mykolayiv.com |date= |accessdate=2014-01-03}}</ref> After processing the available scientific data (ancient illustrations on [[vases]] and [[reliefs]], written and archaeological sources) members of the Odessa Archaeological Museum, under the leadership of prof. [[PhD]] Vladimir N. Stanko, proposed the building of a bireme since in antiquity it had been the most widely used vessel in the northern Black Sea region.

[[File:Europe Ivlia En.jpg|thumb|Route of the expedition]]
[[File:Ivlia6.jpg|thumb|Ivlia’s [[bronze]] bow [[Naval ram|ram]] in form of wild [[boar]]'s head]]

== Expedition route ==

Starting from [[Odessa]] ([[Ukraine]]) in 1989, ''Ivlia'' followed the routes of the ancient mariners on the Black Sea and the Mediterranean as well as the Atlantic, covering more than 3.000 [[nautical mile]]s in six expedition seasons and visiting over 50 European ports, finally sailing up the river [[Seine]] to reach [[Paris]].<ref group="note">To celebrate the successful completion of the voyages, on behalf of the Mayor of Paris the future President of France, [[Jacques Chirac]], was received on board the Ivlia.</ref> 
The expedition's progress was widely covered by the international media.<ref group="note" name="SeeLiterature">See Literature.</ref> During the time of the voyage, hundreds of articles were published, along with dozens of TV and radio reports. The ship was regularly visited by official delegations and thousands of tourists. Ivlia also took part in international maritime festivals: «Colombo'92» ([[Genoa]], Italy), «Brest’92», «Cancal’93», «Whitbread Round the World Race '93» ([[Portsmouth]]) «Vieux Greements’94» (France). The radio constantly broadcast the expedition’s [[call sign]].<ref group="note">RB5FH/mm.</ref> Over six seasons the crew members included more than 200 people – citizens of Russia, [[Ukraine]], [[Moldova]], France, Greece and [[Georgia (country)|Georgia]].

== Scientific aspects ==


The research programme of the expedition was developed by the authors of the project, together with the staff of the Odessa Archaeological Museum and the Nikolayev University of Shipbuilding, primarily to address the following objectives:
* Clarification of written and archaeological sources of data on the design, construction technology and load capacity of ancient Greek ships;
* Practical research into the [[seaworthiness]] of antique biremes. It is worth noting the high seaworthiness of the bireme, even with tailwinds of up to 7 on the [[Beaufort scale]].<ref group="note">Even with no sails, Ivlia was able to maintain a stable course in stormy weather, solely as a result of [[windage]] from her high stern.</ref>
* Verification of [[cabotage]] routes of Hellenic sailors, as well as the possibility of galleys in antiquity making voyages on the open sea, out of sight of the coast;<ref group="note">In the scientific world, there is continuing debate about how far the routes of ancient mariners were from the coastline. Many scholars believe that ancient seagoing ships were weak, and consequently their pilots tacked close to the shores. However, a coast unlit and unequipped with [[navigation mark]]s (as it was in ancient times), posed far more perils for navigation than the open sea. To this must be added the real threat of [[pirate]] attacks, since many of the coastal peoples engaged in [[brigandage]].</ref>
* Clarifying the details of ancient [[periplus]] and verifying a range of hypotheses (from the project authors) to localise the ancient Greek settlement of the North-Western Black Sea region;
* Mastering the ancient art of navigation, control of an antique vessel by sail, methods of mooring and anchoring galleys;
The practical experience gained on Ivlia's expeditions enabled the project authors to affirm:
* The level of [[cartography|cartographical]] advancement and navigational knowledge of the ancient Greeks and the seaworthiness of their ships were far higher than is commonly believed;
* Ancient mariners were perfectly able to orient by the stars, made open sea crossings, were unafraid of sailing away from the coast, were familiar with and skilfully used the prevailing winds and [[Sea current|currents]];
* The famous [[trireme]]s, which were distinguished by their outstanding performance in combat, were much less seaworthy than the biremes: they were built for battle and used in large naval campaigns;
* Biremes were the most common type of vessel used during the [[Colonies in antiquity|great Greek colonization]] of the Mediterranean. The geographical discoveries of antiquity were made on these vessels, suited as they were to long ocean voyages. As the most seaworthy ships of the time, it is most likely that biremes were used by the [[Carthage|Carthaginian]] explorer [[Himilco]], for [[Hanno the Navigator]]'s explorations beyond the [[Pillars of Hercules]], as well as [[Pytheas|Pytheas of Massalia's]] voyage to the legendary island of [[Thule]].
In addition, the research programme conducted on board Ivlia included the participation of the Institute of Biology of the Southern Seas, the Institute of Water Transport [[Hygiene]], and other scientific centres of Ukraine. During the expedition, regular measurements were made of environmental parameters and the level of [[Marine pollution|pollution of the sea water]], assessments of the state of marine [[flora]] and [[fauna]], and a variety of medical experiments were conducted.
The data obtained during the six years of voyages are summarized in the articles and books subsequently published by the authors of the project.<ref group="note" name="SeeLiterature" />
On the whole, Ivlia’s journey around Europe was a bright page in the study of Hellenic shipbuilding and seafaring, and also contributed to the strengthening of international cultural relations.

== Notes ==

{{reflist|group="note"}}

== References ==

{{Reflist}}

== Literature ==

* Bockius, Ronald (2007). Schifffahrt und Schiffbau in der Antike, p.&nbsp;52-64. Theiss Verlag. ISBN 978-3-8062-1971-5
* [[Lionel Casson|Casson]], Lionel (1991). The Ancient Mariners, ch.8. Princeton University Press. ISBN 0-691-06836-4
* Gilles, Daniel (1992). L'Album Souvenir de la Fete Brest'92, p.&nbsp;7, 111, 236, 257. Le Chasse Maree. Armen. ISBN 2-903708-37-1 
* Mark, Samuel (2005). Homeric Seafaring, ch.4,5. Texas University Press. ISBN 1-58544-391-3
* Melnik, Igor K. (2010). Historical Maritime Sailing in Models & Reconstruktions, p.&nbsp;46-49. Kiev, Phoenix. ISBN 978-966-438-278-3
* [[John Sinclair Morrison|Morrison]], John. The Athenian Trireme, p.&nbsp;28 n.2. Cambridge University Press. ISBN 0-521-56419-0
* "[[Il Secolo XIX]]", ([[Italy]]), 23.05.1992. "In porto, dopo 3 anni d'odissea, una triremi russa", Giorgio Carrozi.
* "[[La Stampa]]", (Italy), 31.05.1992 (№147). "E'approdata a Sanremo la triremi dell'antica Grecia".
* "[[Il Tirreno]]", (Italy), 06.05.1992. " Una mostra per l’Ivlia".
* "[[Le Monde]]", ([[France]]), 19.07.1992. "Pavel, galerien d'Odessa", Annick Cojean.
* "[[Thalassa (TV series)|Revue Thalassa]]", (France), 07.1992 (№3). "Et vogue la galere" p.&nbsp;64-65.
* "Presse – Ocean (Ouest)", (France), 09.09.1994. "Ivlia se prepare pour une transatlantique", Severine Le Bourhis, p.&nbsp;15.
* "[[Le Télégramme]]", (France), 02.08.1994. "La galere antique a la conquete de l’Atlantique", Noel Pochet.
* "La Presse de la Manche", (France), 14.08.1993. " Et vogue la galere ukrainienne", Th. Motte, p.&nbsp;3-4.
* "Le Chasse Maree", (France), 07.1992 (№67). "Ivlia, la galere", p.&nbsp;16. 
* "[[Le Marin]]", (France), 21.05.1993. "Sous le vent de la galere", Cristhine Le Portal. 
* "[[Le Parisien]]", (edition Paris), 16.09.1993. "Une galere antique", Laurent Mauron. 
* "[[Libération]]", (France), 07.12.1993. "Ivlia ou l'Odyssee suspendue", Patrick Le Roux, p.&nbsp;28-29.

== External links ==
* http://www.ivlia.com/en.html 
* http://history.marin.net.ua/index.php?option=com_content&view=category&id=46:-lr&Itemid=81
* http://www.xlegio.ru/navy/ancient-ships/to-the-question-of-ancient-seafaring/ 
* http://vokrugsveta.com/index.php?option=com_content&task=view&id=478&Itemid=69
* https://www.youtube.com/watch?v=gsJt1DbbDRo
* https://www.youtube.com/watch?v=I37WQpCH474
* https://www.youtube.com/watch?v=1LGH6Y87Mpg
* https://www.youtube.com/watch?v=-TEVfO39n_I

{{coord|37|56|3.3|N|23|41|7.14|E|region:GR|display=title}}

[[Category:Galleys]]
[[Category:1989 ships]]
[[Category:Replica ships]]
[[Category:Ship types|Replica]]
[[Category:Shipbuilding]]
[[Category:Human-powered vehicles]]
[[Category:Human-powered transport]]
[[Category:History of rowing]]
[[Category:Rowing boats]]