{{Infobox journal
| title = International Review of Psychiatry
| cover = 
| discipline = [[Psychiatry]], [[psychology]]
| formernames =
| editor =  Dinesh Bhugra & Margaret Chisolm
| publisher = [[Taylor & Francis]] on behalf of the [[Institute of Psychiatry]]
| country =
| abbreviation = Int. Rev. Psychiatry
| history = 1989-present
| frequency = Bimonthly
| impact = 1.966
| impact-year = 2014
| website = http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=iirp20
| link1 = http://www.tandfonline.com/toc/iirp20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/iirp20#.VpSo3VIcEQs
| link2-name = Online archive
| ISSN = 0954-0261
| eISSN = 1369-1627
| JSTOR =
| OCLC = 226001786
| LCCN = 
| CODEN = 
}}
The '''''International Review of Psychiatry''''' is a bimonthly [[peer-reviewed]] [[medical journal]] published by [[Taylor & Francis]] on behalf of the [[Institute of Psychiatry]] ([[King's College London]]).<ref name=tfirp>{{cite web |title=Journal information |work=International Review of Psychiatry  |url=http://www.tandfonline.com/action/journalInformation?journalCode=iirp20 |publisher=[[Taylor & Francis]] |accessdate=2016-01-11}}</ref>  The [[editors-in-chief]] are [[Dinesh Bhugra]] (Institute of Psychiatry/[[Maudsley Hospital]]) and [[Margaret Chisolm]] ([[Johns Hopkins University]]). The journal was established in 1989.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Applied Social Science Index and Abstracts]]
* [[BIOSIS Previews]]
* [[Current Contents]]/Social and Behavioural Sciences
* [[Embase|Embase/EMCare]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[PsycINFO]]/Psychological Abstracts
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[Sociological Abstracts]]
* [[Studies on Women and Gender Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.966.<ref name=WoS>{{cite book |year=2015 |chapter=International Review of Psychiatry |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website}}

[[Category:Publications established in 1989]]
[[Category:Bimonthly journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Psychiatry journals]]
[[Category:English-language journals]]


{{psychiatry-journal-stub}}