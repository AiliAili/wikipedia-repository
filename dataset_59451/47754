{{Infobox artist
| bgcolour      =
| name          = Jason Eppink
| image         = 
| imagesize     =
| caption       = 
| birth_name     =
| birth_date     = {{birth year and age|1983}}
| birth_place      = [[Spring, Texas|Spring]], [[Texas]]
| death_date     =
| death_place    =
| nationality   = [[United States|American]]
| field         = [[Pranks]], [[Performance]], and [[Participatory art]]
| training      = 
| movement      =
| works         =
| patrons       =
| awards        = 
}}

'''Jason Eppink''' (born 1983) is an American curator, designer, and prankster. His projects emphasize participation, mischief, surprise, wonder, generosity, transgression, free culture, and anti-consumerism, and they are staged in public spaces and online as street art, urban interventions, and playful online services and hoaxes, frequently for non-consenting audiences.

Eppink serves as Curator of Digital Media at [[Museum of the Moving Image (New York City)|Museum of the Moving Image]] in [[New York City]]. His work at the museum revolves around participation in a variety of fields, including [[video game]]s, [[interactive art]], [[remix]], [[Graphics Interchange Format|animated GIFs]], and online communities.<ref>{{cite news | last = Gat | first = Orit | title = Artist Profile: Jason Eppink | publisher = Rhizome| date = 2012-01-17 | url = http://rhizome.org/editorial/2012/jan/17/artist-profile-jason-eppink/ | accessdate = 2013-08-20 }}</ref>
Additionally, Jason Eppink is a senior agent with prank group [[Improv Everywhere]], a member of art collective [[Flux Factory]], and a recurring character (40-Year-Old Goosey) on [[The Chris Gethard Show]].{{Citation needed|date=January 2017}}

== Notable Design/Prank Projects ==

=== Pixelator ===

In 2007, Jason Eppink created a series of boxes from foam core and diffusion gel that he placed over video billboards at the entrances to New York City subway stations, turning advertisements into abstract geometric art. Eppink published a video and diagrams to encourage others to continue and improve on the project.<ref>[http://jasoneppink.com/pixelator/ Pixelator]</ref><ref>{{cite web|last=Zjawinski|first=Sonia|title=Artist Pixelates Public Video Billboards|url=https://www.wired.com/underwire/2007/04/artist_pixelate/|work=[[Wired (magazine)|Wired]]|accessdate=21 September 2013}}</ref>

=== Astoria Scum River Bridge ===

In late 2009, frustrated with a sidewalk perpetually covered by residue from a leaking drainage pipe, Jason Eppink and frequent collaborator [[Posterchild]] constructed a footbridge from recycled wood and installed it at the site of the leak. The bridge and the press it attracted embarrassed authorities into fixing the decades-old problem.<ref>[http://jasoneppink.com/astoria-scum-river-bridge/ Astoria Scum River Bridge]</ref><ref>{{cite news|url=http://www.nypost.com/p/news/local/queens/artists_make_amtrak_dry_scum_river_Z8avq1K99uJyRey9xRwp7O|title=No Ooze is Good News|publisher=New York Post|last=Bennett|first=Chuck|date=28 January 2010|accessdate=20 August 2013}}</ref>

=== Kickbackstarter ===

In 2011, to poke fun at the increasing popularity of [[crowdfunding]], Jason Eppink created a parody [[Kickstarter]] campaign to raise money so he could fund his friends’ Kickstarter campaigns.<ref>[http://jasoneppink.com/kickbackstarter Kickbackstarter]</ref><ref>{{cite news|last=Vartanian|first=Hrag|url=http://hyperallergic.com/33463/kickstarter-art-project-goes-meta/|title=The Kickstarter Art Project Goes Meta|date=24 August 2011|publisher=Hyperallergic|accessdate=20 August 2013}}</ref>

== Notable Curatorial Projects ==

=== We Tripped El Hadji Diouf ===

In 2012, Jason Eppink curated an installation of [[animated GIFs]] by members of [[Something Awful]] in response to a [[Adobe Photoshop|Photoshop]] challenge, [http://forums.somethingawful.com/showthread.php?threadid=3387197 “What tripped El Hadji Diouf?”] Participants modified an animated GIF of the unpopular soccer player so he appeared to be tripped by a variety of sight gags and pop culture references. The 35 selected GIFs were displayed in a 50-foot-wide projection in the lobby of Museum of the Moving Image.<ref>[http://www.movingimage.us/exhibitions/2012/06/15/detail/we-tripped-el-hadji-diouf-the-story-of-a-photoshop-thread/ We Tripped el Hadji Diouf], Museum of the Moving Image</ref><ref>{{cite news|last=Miller|first=Paul|url=http://www.theverge.com/2012/5/14/3019468/GIF-exhibit-We-Tripped-El-Hadji-Diouf-museum-of-the-moving-image|title=When GIFs become art|date=14 May 2012|publisher=The Verge|accessdate=20 August 2013}}</ref> In 2015, the project appeared for the first time in the United Kingdom at the [[National Museum of Football]], as part of the exhibition Out of Play - Technology & Football.<ref>[http://www.nationalfootballmuseum.com/whats-on/event/2015/out-of-play/19-06-15/]</ref>

=== Cut Up ===

In 2013, Jason Eppink curated an exhibition of short form video remixes that primarily use popular media as source material. The show examined genres and techniques that emerged online over the last ten years along with their historical precedents, proposing that remix is an increasingly common way of participating in shared cultural conversations. Exhibition sections included [[supercut]]s, [[recut trailers]], and [[vidding]].<ref>[http://www.movingimage.us/exhibitions/2013/06/29/detail/cut-up/ Cut Up], Museum of the Moving Image</ref><ref>{{cite news|last=Halperin|first=Moze|url=http://thecreatorsproject.vice.com/blog/the-mother-of-all-supercuts-cut-up-brings-the-best-of-youtube-to-the-museum|title=The Mother Of All Supercuts: "Cut Up" Brings The Best of YouTube To The Museum|date=16 July 2013|publisher=Creators Project|accessdate=20 August 2013}}</ref>

=== The Reaction GIF: Moving Image as Gesture ===

In 2014, Jason Eppink engaged members of [[Reddit]] <ref>[http://www.reddit.com/r/gifs/comments/1yw7aa/hey_reddit_want_to_help_curate_a_museum/ Hey Reddit, want to help curate a museum exhibition about reaction gifs? (details in comments)], Reddit</ref> to identify frequently used reaction GIFs: animated GIFs posted in response to text in online forums and comment threads. Thirty-seven GIFs and their translations, provided by Redditors, were selected for the exhibition, which examined the increasingly popular use of the animated GIF as a form of non-verbal communication.<ref>[http://www.movingimage.us/exhibitions/2014/03/12/detail/the-reaction-gif-moving-image-as-gesture/ The Reaction GIF: Moving Image as Gesture], Museum of the Moving Image</ref><ref>{{cite news|authorlink=Neda Ulaby|last=Ulaby|first=Neda|url=http://www.npr.org/2014/05/09/310098121/hard-g-or-soft-the-gif-takes-its-place-as-a-modern-art-form|title=Hard ‘G’ Or Soft, The GIF Takes Its Place As A Modern Art Form|date=9 May 2014|publisher=NPR|accessdate=22 August 2014}}</ref>

=== How Cats Took Over the Internet ===

In 2015, Jason Eppink curated an exhibition that addressed the phenomenon of [[Cats and the Internet|internet cats]] as [[vernacular culture]]. The exhibition included a 20-year timeline of cats online; a quantified look at the popularity of cats and dogs on significant leisure websites; sociological, anthropological, and evolutionary explanations for the phenomenon; and an investigation into animals that perform similar roles on other national internets.<ref>[http://www.movingimage.us/exhibitions/2015/08/07/detail/how-cats-took-over-the-internet/ How Cats Took Over the Internet], Museum of the Moving Image</ref><ref>{{cite news|last=Kingson|first=Jennifer A.|url=https://www.nytimes.com/2015/08/07/arts/design/how-cats-took-over-the-internet-at-the-museum-of-the-moving-image.html|title='How Cats Took Over the Internet' at the Museum of the Moving Image|date=6 August 2015|publisher=The New York Times|accessdate=25 January 2016}}</ref>

== References ==

{{Reflist}}

== Additional References ==

*{{cite news | title = The GOOD 100 | publisher = GOOD Magazine| date = 2009-10-11 | url = http://www.good.is/posts/the-good-100-jason-eppink | accessdate = 2013-08-20 }}
*{{cite news | last = Driever | first = Juliana | title = The Big Picture: Jason Eppink | publisher = Bad At Sports | date = 2013-05-29 | url = http://badatsports.com/2013/the-big-picture-jason-eppink/ | accessdate = 2013-08-20 }}

== External links ==
* {{Official website|http://jasoneppink.com}}

{{DEFAULTSORT:Eppink, Jason}}
[[Category:Pranksters]]
[[Category:American designers]]
[[Category:American curators]]
[[Category:American artists]]
[[Category:1985 births]]
[[Category:Living people]]