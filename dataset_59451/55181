{{Infobox journal
| title         = Acadiensis
| cover         = [[File:Acadiensis cover.jpg|150px]]
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = 
| discipline    = [[Canadian studies]]<!-- or |subject= -->
| peer-reviewed =
| language      = English, French
| editors       = [[John G. Reid]], [[Janet Guildford]]
| publisher     = [[University of New Brunswick]]
| country       = Canada
| history       = 1901&ndash;1908<br>1971&ndash;present
| frequency     = Biannual
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| ISSNlabel     = 
| ISSN          = 0044-5851
| eISSN         = 
| CODEN         = 
| JSTOR         = 
| LCCN          = 92641132
| OCLC          = 316257829
| website       = http://www.acadiensis.ca/
| link1         = https://journals.lib.unb.ca/index.php/Acadiensis/issue/archive
| link1-name    = Online access (1971&ndash;present)
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
'''''Acadiensis''''' is a semi-annual [[peer-reviewed]] [[academic journal]] covering the history of [[Atlantic Canada]]. It was originally published by [[David Russell Jack]] from 1901&ndash;1908 and then resumed in 1971 (with volume numbering restarting at 1). The [[editors-in-chief]] are [[John G. Reid]] ([[Saint Mary's University (Halifax)|Saint Mary's University]]) and [[Janet Guildford]] ([[Mount Saint Vincent University]]). It is published by the [[University of New Brunswick]]. Articles can be in English or French.

== Abstracting and indexing ==
The journal is abstracted and indexed in the following [[bibliographic database]]s:<ref>http://www.lib.unb.ca/Texts/Acadiensis/bin/get.cgi?/&filename=Indexes.htm</ref><ref>http://miar.ub.edu/issn/0044-5851</ref>
{{columns-list|colwidth=30em|
*[[The Acadiensis Index]]
*[[American History and Life]]
*[[Arts and Humanities Citation Index]]
*[[Canadian Business and Current Affairs]]
*[[Canadian Index]]
*[[Canadian Periodical Index]]
*[[Current Contents]]/Arts and Humanities
*[[Educational Resources Information Center]]
*[[Historical Abstracts]]
*[[International Bibliography of Periodical Literature]]
*[[International Bibliography of Social Sciences]]
*[[International Current Awareness Services]]
*[[MLA International Bibliography]]
*[[Periodicals Index Online]]
*[[Scopus]]
}}

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.acadiensis.ca/}}
* [https://archive.org/stream/acadiensisquarte00jackuoft#page/n5/mode/2up Acadiensis Vol. 1, 1901]
* [https://archive.org/stream/acadiensisquarte00jackuoft#page/53/mode/1up Acadiensis Vol. 1, April 1901]
* [https://archive.org/stream/acadiensisquarte00jackuoft#page/125/mode/1up Acadiensis Vol. 1, July 1901]
* [https://archive.org/stream/acadiensisquarte00jackuoft#page/201/mode/1up Acadiensis Vol. 1, Oct 1901]
* [https://archive.org/stream/acadiensisquarte02jackuoft#page/n5/mode/2up Acadiensis Vol. 2 No. 1, 1902]
* [https://archive.org/stream/acadiensisquarte02jackuoft#page/73/mode/1up Acadiensis Vol. 2, No. 2, 1902]
* [https://archive.org/stream/acadiensisquarte02jackuoft#page/145/mode/1up Acadiensis Vol. 2, No. 3, 1902]
* [https://archive.org/stream/acadiensisquarte02jackuoft#page/217/mode/1up Acadiensis Vol. 2, No. 4, 1902]
*Acadiensis Vol. 3, 
*Acadiensis Vol. 4, 
* [https://archive.org/stream/acadiensisquarte05jackuoft#page/n3/mode/2up Acadiensis, Vol. 5, No. 1, 1905]
* [https://archive.org/stream/acadiensisquarte05jackuoft#page/117/mode/1up Acadiensis, Vol. 5, No. 2-3, 1905]
* [https://archive.org/stream/acadiensisquarte05jackuoft#page/357/mode/1up Acadiensis Vol. 5, No.4, 1905]
*Acadiensis Vol. 6, 1906
*Acadiensis Vol. 7, 1907
*Acadiensis Vol. 8, 1908
* [http://www.umoncton.ca/umcm-ceaac/files/umcm-ceaac/wf/wf/pdf/acad-4.pdf Acadiensis - Index re: Acadians]

[[Category:Media of Nova Scotia]]
[[Category:Media of New Brunswick]]
[[Category:Media of Prince Edward Island]]
[[Category:Media of Newfoundland and Labrador]]
[[Category:Publications established in 1901]]
[[Category:Publications disestablished in 1908]]
[[Category:Publications established in 1971]]
[[Category:Multilingual journals]]
[[Category:Biannual journals]]
[[Category:University of New Brunswick]]
[[Category:History journals]]
[[Category:1901 establishments in Canada]]
[[Category:1908 disestablishments in Canada]]
[[Category:1971 establishments in Canada]]
[[Category:English-language journals]]
[[Category:French-language journals]]
[[Category:Academic journals published by universities and colleges]]

{{history-journal-stub}}