{{about||the American musician|Jello Biafra|the West African bight known as Bight of Biafra|Bight of Bonny}}
{{Multiple issues|
{{unbalanced|date=March 2015}}
{{POV|date=March 2015}}
{{disputed|date=July 2015}}
{{refimprove|date=July 2015}}
}}
{{Use British English|date=May 2012}}
{{Use dmy dates|date=September 2016}}
{{Infobox Former Country
|native_name = Bịafra
|conventional_long_name = Republic of Biafra
|common_name = Biafra
|continent = Africa
|region =  Eastern Nigeria
|country = Nigeria
|status = [[List of historical unrecognized states|Unrecognized state]]
|era = Cold War
|year_start = 1967
|date_start = 30 May
|year_end = 1970<ref name="Ogbaa1995">{{cite book|last=Ogbaa|first=Kalu|title=Igbo|url=https://books.google.com/books?id=YBe4V7RomEsC&pg=PA49|accessdate=15 January 2014|date=1 January 1995|publisher=The Rosen Publishing Group|isbn=978-0-8239-1977-2|page=49}}</ref>
|date_end = 15 January
|event_end = Rejoins Federal Nigeria
|p1 = Nigeria
|flag_p1 = Flag of Nigeria.svg
|s1 = Nigeria
|flag_s1 = Flag of Nigeria.svg
|national_motto = "Peace, Unity, and Freedom."
|national_anthem = ''[[Land of the Rising Sun (national anthem)|Land of the Rising Sun]]''
|image_flag = Flag of Biafra.svg
|image_coat = Biafra Coat of Arms.png
|image_map = Biafra map.png
|image_map2 = Biafra_independent_state_map-en.svg
|image_map_caption = {{nowrap|'''Green''': Republic of Biafra.}} 
|image_map2_caption = Republic of Biafra in May 1967
|common_languages = {{nowrap| [[Igbo language|Igbo]]<small> (Predominant) </small><br/>[[Efik language|Efik]]{{·}}[[Annang language|Annang]]{{·}}[[Ibibio language|Ibibio]]{{·}}[[Ijaw languages|Ijaw]] <small> </small>}}
|capital = Enugu
|religion = [[Christianity]]
|largest city = Aba
|leader1 = C. Odumegwu Ojukwu
|government_type = Republic
|currency = Biafran pound
|stat_pop1 = 13500000
|stat_area1 = 77306
|stat_year1 = 1967
|footnotes = {{cite book |title=Encyclopedia of the Stateless Nations: S-Z |first=James |last=Minahan |publisher=Greenwood Publishing Group |year=2002 |isbn=0-313-32384-4 |page=762 |url=https://books.google.com/?id=K94wQ9MF2JsC&pg=PA762}}
}} 
'''Biafra''', officially the '''Republic of Biafra''', was a secessionist state in eastern [[Nigeria]] that existed from 30 May 1967 to January 1970. It took its name from the [[Bight of Biafra]], the Atlantic bay to its south. The inhabitants were mostly the [[Igbo people]] who led the secession due to economic, ethnic, cultural and religious tensions among the various peoples of Nigeria. Other ethnic groups that constituted the republic were the [[Efik people|Efik]], [[Ibibio people|Ibibio]], [[Annang]], [[Ekoi people|Ejagham]], [[Eket people|Eket]], [[Ibeno]] and the [[Ijaw people|Ijaw]] among others.
 
The secession of the Biafran region was the primary cause of the [[Nigerian Civil War]], also known as the Biafran War. The state was formally recognised by [[Gabon]], [[Haiti]], [[Ivory Coast]], [[Tanzania]] and [[Zambia]].<ref>{{Cite web|url=https://www.britannica.com/place/Biafra|title=Biafra {{!}} secessionist state, Nigeria|access-date=2016-10-01}}</ref> Other nations which did not give official recognition, but provided support and assistance to Biafra included [[Israel]], [[France]], [[Spain]], [[Portugal]], [[Norway]], [[Rhodesia]], [[Republic of South Africa|South Africa]] and the [[Vatican City]].<ref>{{cite web|url=http://www.africafederation.net/Biafra.htm|title=Biafra Free State|publisher=}}</ref><ref name="Dawodu">{{cite web|url=http://www.dawodu.com/omoigui25.htm|author=Nowa Omoigui|title=Federal Nigerian Army Blunders of the Nigerian Civil War – Part 2|accessdate=15 August 2008| archiveurl= https://web.archive.org/web/20080719145355/http://www.dawodu.com/omoigui25.htm| archivedate= 19 July 2008 | deadurl= no}}</ref>{{unreliable source?|date=February 2016}} Biafra also received aid from [[non-state actor]]s, including [[Joint Church Aid]], [[Holy Ghost Fathers]] of Ireland, [[Caritas International]], [[MarkPress]] and U.S. [[Catholic Relief Services]].<ref name="Dawodu"/>{{unreliable source?|date=February 2016}}

After two-and-a-half years of war, during which over three million Biafran civilians died from starvation caused by the total blockade of the region by the Nigerian government, Biafran forces under the motto of "No-victor, No-vanquished" surrendered to the [[Nigerian military juntas of 1966–79 and 1983–98|Nigerian Federal Military Government (FMG)]], and Biafra was reintegrated into Nigeria.<ref>{{cite news|url=http://news.bbc.co.uk/1/hi/world/africa/596712.stm|author=Barnaby Philips|title=Biafra: Thirty years on|accessdate=9 March 2011|work=BBC News|date=13 January 2000}}</ref>

==Secession==
{{Disputed section|date=November 2015}}
{{Main article|Nigerian Civil War}}

In 1960, [[Nigeria]] became independent of the [[United Kingdom]]. As with many other new African states, the borders of the country did not reflect earlier ethnic boundaries. Thus, the northern region of the country is made up of [[Islam in Nigeria|Muslim]] majority, while the southern population is predominantly [[Christianity in Nigeria|Christian]]. Following independence, Nigeria was divided primarily along ethnic lines with [[Hausa people|Hausa]] and [[Fula people|Fulani]] majority in the north, [[Yoruba people|Yoruba]] and [[Igbo people|Igbo]] majority in the south-west and south-east respectively.<ref name="BBC1">{{cite news|url=http://news.bbc.co.uk/2/hi/africa/596712.stm|title=Biafra: Thirty years on|date=13 January 2000|work=The [[BBC]]|author=Barnaby Philips | accessdate=1 January 2010}}</ref>

In January 1966, [[1966 Nigerian coup d'état|a military coup occurred]] during which 30 political leaders including Nigeria's Prime Minister, Sir [[Abubakar Tafawa Balewa]], and the Northern premier, Sir [[Ahmadu Bello]], were killed. It was alleged to be an Igbo coup because [[Nnamdi Azikiwe]], the President, of Igbo extraction, and the premier of the southeastern part of the country were not killed.<ref name="Omoigui2">{{cite web|title=OPERATION 'AURE': The Northern Military Counter-Rebellion of July 1966|url=http://www.africamasterweb.com/CounterCoup.html|author=Nowa Omoigui|work=Nigeria/Africa Masterweb}}</ref><ref name="Bozimo">{{cite web|url=http://www.nigerdeltacongress.com/farticles/festus_samuel_okotie_eboh.htm|title=Festus Samuel Okotie Eboh  (1912–1966)|author=Willy Bozimo|work=Niger Delta Congress|accessdate=17 August 2008}}</ref><ref name="onlinenigeria">{{cite web|title=1966 Coup: The last of the plotters dies|date=20 March 2007|work=OnlineNigeria.com|url=http://nm.onlinenigeria.com/templates/?a=9670&z=17}}</ref>

In July 1966 northern officers and army units staged a counter-coup. Muslim officers named a General from a small ethnic group (the Angas) in central Nigeria, General [[Yakubu Gowon|Yakubu "Jack" Gowon]], as the head of the Federal Military Government (FMG). The two coups deepened Nigeria's ethnic tensions. In September 1966, [[1966 anti-Igbo pogrom|approximately 30,000 Igbo were killed in the north]], and some Northerners were killed in backlashes in eastern cities.<ref name="onwar">{{cite web|url=http://www.onwar.com/aced/data/bravo/biafra1967.htm|date=16 December 2000|title=Biafran Secession: Nigeria 1967–1970|work=Armed Conflict Events Database}}</ref>
{{Quote box|quote=Now, therefore, I, Lieutenant-Colonel Chukwuemeka Odumegwu Ojukwu, Military Governor of Eastern Nigeria, by virtue of the authority, and pursuant to the principles, recited above, do hereby solemnly proclaim that the territory and region known as and called Eastern Nigeria together with her continental shelf and territorial waters shall henceforth be an independent sovereign state of the name and title of "The Republic of Biafra".|source=[[Chukwuemeka Odumegwu Ojukwu]]<ref>{{cite web|url=http://www.citizensfornigeria.com/index.php?option=com_content&task=view&id=52&Itemid=63|title=Ojukwu's Declaration of Biafra Speech |work=Citizens for Nigeria|accessdate=15 August 2008}}</ref>|width=420px}}Biafra as a territory existed long before the amalgamation and independence of Nigeria as a republic. [[Chukwuemeka Odumegwu Ojukwu]] in pursuit of a more agreeable arrangement for peaceful co-existence of all regions in Nigeria proposed for a confederated Nigeria.

In January 1967, the military leaders and senior police officials of each region met in [[Aburi]], Ghana and agreed on a loose confederation of regions. The Northerners were at odds with the [[Aburi Accord]]; [[Obafemi Awolowo]], the leader of the Western Region warned that if the Eastern Region seceded, the Western Region would also, which persuaded the northerners.<ref name="onwar"/>

After the federal and eastern governments failed to reconcile, on 26 May the Eastern region voted to secede from Nigeria. On 30 May, [[Chukwuemeka Odumegwu Ojukwu]], the South Eastern Region's military governor, announced the Republic of Biafra, citing the Easterners killed in the post-coup violence.<ref name="BBC1"/><ref name="onwar"/><ref>{{cite web
| title = Biafra Spotlight - Republic of Biafra is Born
| work = Library of Congress Africa Pamphlet Collection - Flickr
| accessdate = 11 May 2014
| url = https://www.flickr.com/photos/pohick2/13969882488/in/set-72157644200924229
}}</ref> The large amount of oil in the region created conflict, as oil was already becoming a major component of the Nigerian economy.<ref name="AU">{{cite web|work=American University|title=ICE Case Studies|date=November 1997|url=http://www.american.edu/ted/ice/biafra.htm}}</ref> The Eastern region was very ill-equipped for war, out-manned and out-gunned by the military of the remainder of Nigeria. Their advantages included fighting in their homeland and support of most South Easterners.<ref name="Omoigui">{{cite web|work=BBC|url=http://www.dawodu.com/omoigui24.htm|title=Nigerian Civil War file|author=Nowa Omoigui|date=3 October 2007|accessdate=27 October 2007| archiveurl= https://web.archive.org/web/20070928010636/http://www.dawodu.com/omoigui24.htm| archivedate= 28 September 2007 | deadurl= no}}</ref>

==War==
The FMG launched "police measures" to annex the Eastern Region on 6 July 1967. The FMG's initial efforts were unsuccessful;
the Biafrans successfully launched their own offensive, occupying areas in the [[Mid-Western Region, Nigeria|mid-Western Region]] in August 1967. By October 1967, the FMG had regained the land after intense fighting.<ref name="onwar"/><ref name="BBC.On This Day">{{cite news|work=BBC|title=On This Day (30 June)|url=http://news.bbc.co.uk/onthisday/hi/dates/stories/june/30/newsid_3733000/3733321.stm| date=30 June 1969 | accessdate=1 January 2010}}</ref> In September 1968, the federal army planned what Gowon described as the "final offensive". Initially the final offensive was neutralised by Biafran troops. In the latter stages, a Southern FMG offensive managed to break through the fierce resistance.<ref name="onwar"/>

During the war there were great shortages of food and medicine throughout Biafra, due largely to the Nigerian government's blockade of the region as suggested in a number of arguments by leaders of the Nigerian Government.

Anthony Enahoro stated that "there are various ways of fighting a war. You might starve your enemy into submission, or you might kill him on the battlefield." Obafemi Awolowo said, "All is fair in war, and starvation is one of the weapons of war and I don't see why we should feed our enemies in order for them to fight harder."

Many volunteer bodies organised the [[Biafran airlift]] which provided blockade-breaking relief flights into Biafra, carrying food and medicines in, and later provided means of evacuation for refugee children. On 30 June 1969, the Nigerian government banned all [[Red Cross]] aid to Biafra; two weeks later it allowed medical supplies through the front lines, but restricted food supplies.<ref name="BBC.On This Day"/> Later in October 1969, Ojukwu appealed to the United Nations to mediate a [[cease-fire]].

The federal government called for Biafra's surrender. In December, the FMG managed to cut Biafra in half, primarily by the efforts of 3 Marine Commando Division of the [[Nigerian Army]], led by then-Colonel [[Benjamin Adekunle]], popularly called "The Black Scorpion", and later by [[Olusegun Obasanjo]]. Ojukwu fled to [[Ivory Coast]], leaving his chief of staff, [[Philip Effiong]], to act as the "officer administering the government". Effiong called for a ceasefire on 12 January and submitted to the FMG.<ref name="onwar"/> By then, more than one million people had died in battle or from starvation.<ref name="NYT">{{cite news|work=New York Times|title= Few Traces of the Civil War Linger in Biafra |author=James Brooke|date=14 July 1987|accessdate=15 August 2008|url=https://query.nytimes.com/gst/fullpage.html?res=9B0DEFD71E39F937A25754C0A961948260}}
</ref><ref name="Murray">{{cite news|work=BBC|url=http://news.bbc.co.uk/2/hi/africa/6657259.stm|title=Reopening Nigeria's civil war wounds|author=Senan Murray|date=3 May 2007|accessdate=15 August 2008}}</ref> Biafra was reabsorbed into Nigeria on 15 January.

==Geography==
[[File:Biafra sat.png|thumb|left|Satellite pictures of Biafra]]

Biafra comprised over {{convert|29848|sqmi|km2}} of land,<ref name="land">{{cite book |title=Encyclopedia of the Stateless Nations: S-Z |first=James |last=Minahan |publisher=Greenwood Publishing Group |year=2002 |isbn=0-313-32384-4 |page=762 |url=https://books.google.com/?id=K94wQ9MF2JsC&pg=PA762}}</ref> with terrestrial borders shared with [[Nigeria]] to the north and west, and with [[Cameroon]] to the east. Its coast was on the [[Gulf of Guinea]] in the south.

The former country's northeast bordered the [[Benue Hills]] and mountains that lead to Cameroon. Three major rivers flow from Biafra into the Gulf of Guinea: the [[Imo River]], the [[Cross River (Nigeria)|Cross River]] and the [[Niger River]].<ref>
{{cite web
|work= Britannica
|title= Nigeria
|accessdate= 17 August 2008
|url= http://www.britannica.com/EBchecked/topic/414840/Nigeria#tab=active~checked%2Citems~checked&title=Nigeria%20--%20Britannica%20Online%20Encyclopedia
| archiveurl = https://web.archive.org/web/20080630061900/http://www.britannica.com/EBchecked/topic/414840/Nigeria
| archivedate = 30 June 2008 | deadurl= no}}
</ref>

The territory of Biafra is covered nowadays by the [[States of Nigeria|Nigerian states]] of [[Cross River State|Cross River]], [[Ebonyi State|Ebonyi]], [[Enugu State|Enugu]], [[Anambra State|Anambra]], [[Imo State|Imo]], [[Bayelsa State|Bayelsa]], [[Rivers State|Rivers]], [[Abia State|Abia]], [[Delta State|Delta]] and [[Akwa Ibom]].

==Language==
Whilst it existed, the predominant language of Biafra was [[Igbo language|Igbo]].<ref>{{cite web|url=http://ilc.igbonet.com|title=INTRODUCTION TO THE IGBO LANGUAGE
|author=Ònyémà Nwázùé|accessdate=18 August 2008| archiveurl= https://web.archive.org/web/20080818233339/http://ilc.igbonet.com/| archivedate= 18 August 2008 | deadurl= no}}</ref> Along with Igbo, there were a variety of other languages, including [[Efik language|Efik]], [[Ijaw language|Ijaw]], [[Annang language|Annang]] and [[Ibibio language|Ibibio]]. However, [[English language|English]] was the [[official language]].

==Economy==
An early institution created by the Biafran government was the Bank of Biafra, accomplished under "Decree No. 3 of 1967".<ref name="banknotes">{{cite journal|last=Symes|first=Peter|year=1997|title=The Bank Notes of Biafra|journal=International Bank Note Society Journal|volume=36|issue=4|url=http://www.pjsymes.com.au/articles/biafra.htm |accessdate=17 August 2008| archiveurl= https://web.archive.org/web/20080827224301/http://www.pjsymes.com.au/articles/biafra.htm| archivedate= 27 August 2008 | deadurl= no}}</ref> The bank carried out all central banking functions including the administration of foreign exchange and the management of the public debt of the Republic.<ref name="banknotes"/> The bank was administered by a governor and four directors; the first governor, who signed on bank notes, was [[Sylvester Ugoh]].<ref>{{cite web|last=Ivwurie |first=Dafe |url=http://allafrica.com/stories/201102280439.html |title=Nigeria: The Men Who May Be President (1) |publisher=allAfrica.com |date=25 February 2011 |accessdate=22 May 2012}}</ref> A second decree, "Decree No. 4 of 1967", modified the Banking Act of the Federal Republic of Nigeria for the Republic of Biafra.<ref name="banknotes"/>

The bank was first located in Enugu, but due to the ongoing war, the bank was relocated several times.<ref name="banknotes"/>
Biafra attempted to finance the war through foreign exchange. After Nigeria announced their currency would no longer be legal tender (to make way for a new currency), this effort increased. After the announcement, tons of Nigerian bank notes were transported in an effort to acquire foreign exchange. The currency of Biafra had been the Nigerian pound, until the Bank of Biafra started printing out its own notes, the [[Biafran pound]].<ref name="banknotes"/> The new currency went public on 28 January 1968, and the Nigerian pound was not accepted as an exchange unit.<ref name="banknotes"/> The first issue of the bank notes included only 5 shillings notes and 1 pound notes. The Bank of Nigeria exchanged only 30 pounds for an individual and 300 pounds for enterprises in the second half of 1968.<ref name="banknotes"/>

In 1969 new notes were introduced: [[pound (currency)|£]]10, £5, £1, 10[[Shilling|/-]] and 5/-.<ref name="banknotes"/>

It is estimated that a total of £115–140 million Biafran pounds were in circulation by the end of the conflict, with a population of about 14 million, approximately £10 per person.<ref name="banknotes"/>  In uncirculated condition these are very inexpensive and readily available for collectors.

==Military==
[[File:Roundel of the Biafran Air Force.svg|thumb|[[Roundel]] of the Biafran Air Force.]]

At the beginning of the war Biafra had 3,000 soldiers, but at the end of the war the soldiers totalled 30,000.<ref>{{cite web|url=http://www.canit.se/~griffon/aviation/text/biafra.htm|title=Operation Biafra Babies|accessdate=19 August 2008}}</ref> There was no official support for the Biafran Army by any other nation throughout the war, although arms were clandestinely acquired. Because of the lack of official support, the Biafrans manufactured many of their weapons locally. Europeans served in the Biafran cause; German born [[Rolf Steiner]] was a lieutenant colonel assigned to the 4th Commando Brigade and Welshman [[Taffy Williams]] served as a Major until the very end of the conflict.<ref>"The Last Adventurer" by Steiner, Rolf (Boston:, Little, Brown 1978)</ref> A special guerrilla unit, the Biafran Organization of Freedom Fighters, was established, designed to emulate the [[Viet Cong]], targeting Nigerian supply lines and forcing them to shift forces to internal security efforts.<ref>{{cite book|last=Jowett|first=Philip|title=Modern African Wars (5): The Nigerian-Biafran War 1967-70|date=2016|publisher=[[Osprey Publishing]] Press|location=Oxford|isbn=978-1472816092}}</ref>

The Biafrans managed to set up a small yet effective air force. The BAF commanders were Chude Sokey and later Godwin Ezeilo, who had trained with the Royal Canadian Air Force.<ref name="Air Enthusiast">''Air Enthusiast'' No. 65 September–October 1996 pp&nbsp;40–47 article by Vidal, Joao M. ''Texans in Biafra T-6Gs in use in the Nigerian Civil War''</ref> Early inventory included two [[B-25 Mitchell]]s, two [[B-26 Invader]]s, (one piloted by Polish World War II ace [[Jan Zumbach]], known also as John Brown), a converted [[DC-3]] and one [[De Havilland Dove|Dove]].  In 1968 the Swedish pilot [[Carl Gustaf von Rosen]] suggested the MiniCOIN project to General Ojukwu. By early 1969, Biafra had assembled five [[Malmö MFI-9|MFI-9B]]s in [[Gabon]], calling them "Biafra Babies". They were coloured green, were able to carry six 68&nbsp;mm anti-armour rockets under each wing and had simple sights. The six aeroplanes were flown by three Swedish pilots and three Biafran pilots. In September 1969, Biafra acquired four ex-Armee de l'Air North American [[North American T-6 Texan|T-6G]]s, which were flown to Biafra the following month, with another aircraft lost on the ferry flight. These aircraft flew missions until January 1970 and were flown by Portuguese ex-military pilots.<ref name="Air Enthusiast"/>

Biafra also had a small improvised navy, but it never gained the success their air force did. It was headquartered in Kidney Island, [[Port Harcourt]], and commanded by Winifred Anuku. The Biafran Navy was made up of captured craft, converted tugs, and armor-reinforced civilian vessels armed with machine guns or captured [[6-pounder gun]]s. It mainly operated in the [[Niger Delta]] and along the [[Niger River]].<ref>{{cite book|last=Jowett|first=Philip|title=Modern African Wars (5): The Nigerian-Biafran War 1967-70|date=2016|publisher=[[Osprey Publishing]] Press|location=Oxford|isbn=978-1472816092}}</ref>

==Legacy==
[[File:Starved girl.jpg|left|thumb|upright|A child suffering the effects of severe hunger and [[Kwashiorkor|malnutrition]] during the Nigerian blockade]]

The international humanitarian organisation [[Médecins Sans Frontières]] originated in response to the suffering in Biafra.<ref name=MSF>{{cite web|title=Founding of MSF|url=http://www.doctorswithoutborders.org/founding-msf|publisher=Doctors without borders|accessdate=21 December 2015}}</ref> During the crisis, French medical volunteers, in addition to Biafran health workers and hospitals, were subjected to attacks by the Nigerian army and witnessed civilians being murdered and starved by the blockading forces. French doctor [[Bernard Kouchner]] also witnessed these events, particularly the huge number of starving children, and, when he returned to France, he publicly criticised the Nigerian government and the Red Cross for their seemingly complicit behaviour. With the help of other French doctors, Kouchner put Biafra in the media spotlight and called for an international response to the situation. These doctors, led by Kouchner, concluded that a new aid organisation was needed that would ignore political/religious boundaries and prioritise the welfare of victims.<ref name="hih">Bortolotti, Dan (2004). ''Hope in Hell: Inside the World of Doctors Without Borders'', Firefly Books. ISBN 1-55297-865-6.</ref>

In their study, ''Smallpox and its Eradication'', Fenner and colleagues describe how vaccine supply shortages during the Biafra smallpox campaign led to the development of the focal vaccination technique, later adopted worldwide by the [[World Health Organization]], which led to the early and cost effective interruption of smallpox transmission in west Africa and elsewhere.<ref>http://whqlibdoc.who.int/smallpox/9241561106_chp17.pdf</ref>

On 29 May 2000, the Lagos ''[[The Guardian (Nigeria)|Guardian]]'' newspaper reported that the now ex-president [[Olusegun Obasanjo]] commuted to retirement the dismissal of all military persons who fought for the breakaway state of Biafra during Nigeria's 1967–1970 civil war. In a national broadcast, he said the decision was based on the belief that "justice must at all times be tempered with mercy".<ref>{{cite web|url=http://iys.cidi.org/humanitarian//irin/wafrica/00a/0021.html |title=Site cidi.org |publisher=Iys.cidi.org |accessdate=22 May 2012}}</ref>

In July 2006 the [[Center for World Indigenous Studies]] reported that government sanctioned killings were taking place in the southeastern city of [[Onitsha]], because of a shoot-to-kill policy directed toward Biafran loyalists, particularly members of the [[Movement for the Actualization of the Sovereign State of Biafra]] (MASSOB).<ref>{{cite web|url=http://www.cwis.org/news/index.php?newsdate=biafra1 |title=Emerging Genocide in Nigeria |publisher=Cwis.org |accessdate=22 May 2012}}</ref><ref>{{cite web|url=http://www.cwis.org/fweye/fweye-18.htm |title=Chronicles of brutality in Nigeria 2000–2006 |publisher=Cwis.org |accessdate=22 May 2012}}</ref>

In 2010, researchers from [[Karolinska Institutet]] in Sweden and [[University of Nigeria, Nsukka]], showed that Igbos born in Biafra during the years of the famine were of higher risk of suffering from obesity, hypertension and impaired glucose metabolism compared to controls born a short period after the famine had ended. The findings are in line with the developmental origin of health and disease hypothesis suggesting that malnutrition in early life is a predisposing factor for cardiovascular diseases and diabetes later in life.<ref>{{cite web|url=http://www.plosone.org/article/info:doi/10.1371/journal.pone.0013582 |title=Hypertension, Diabetes and Overweight: Looming Legacies of the Biafran Famine, PLoS ONE |publisher=Plosone.org |accessdate=22 May 2012}}</ref><ref>{{cite web|url=https://www.nytimes.com/2010/11/02/health/02global.html|title=Nigeria: Those Born During Biafra Famine Are Susceptible to Obesity, Study Finds|date=2 November 2010|work=The New York Times}}</ref>

==Movement to re-secede==
There is no central authority coordinating the Biafran re-secession campaign. The [[Movement for the Actualization of the Sovereign State of Biafra]] (MASSOB) is one of the numerous groups advocating for a separate country for the people of south-eastern Nigeria.<ref name="Murray"/> They accuse the state of marginalising the Igbo people. MASSOB says it is a peaceful group and advertises a 25-stage plan to achieve its goal peacefully.<ref>{{cite news|url=http://www.boston.com/news/world/africa/articles/2006/07/12/dream_of_free_biafra_revives_in_southeast_nigeria/|title=Dream of free Biafra revives in southeast Nigeria|author=Estelle Shirbon|date=12 July 2006|agency=Reuters}}</ref> It has two arms of government, the Biafra [[Government in Exile]] and Biafra Shadow Government.<ref>{{cite web|url=http://biafra.cwis.org/news20090413.php |title=Biafra News – 04.13.2009 |publisher=Biafra.cwis.org |accessdate=22 May 2012}}</ref>

The Nigerian government accuses MASSOB of violence; MASSOB's leader, Ralph Uwazuruike, was arrested in 2005 and was detained on treason charges. He has since been released. In 2009, MASSOB launched an unrecognized "Biafran International Passport" in response to persistent demand by some Biafran sympathizers in the diaspora.<ref>{{cite web|url=http://www.vanguardngr.com/2009/07/01/massob-launches-biafran-intl-passport-to-celebrate-10th-anniversary/comment-page-2/ |title=MASSOB launches "Biafran Int'l Passport" to celebrate 10th anniversary |publisher=Vanguardngr.com |date=1 July 2009 |accessdate=22 May 2012}}</ref> On 16 June 2015, the Supreme Council of Elders of the Indigenous People of Biafra, another pro-Biafra organization, sued the Federal Republic of Nigeria for the right to self-determination within their region as a sovereign state.<ref>{{cite web|url=http://sunnewsonline.com/new/court-determines-suit-between-nigeria-biafra-on-sept-22/ |title=Court determines suit between Nigeria, Biafra on Sept 22 |publisher= sunnewsonline.com |date=18 July 2015 |accessdate=18 August 2015}}</ref>

Another group, Indigenous People of Biafra (IPOB), led by a United Kingdom-based Biafran, [[Nnamdi Kanu]], reinvigorated the quest for Biafran realisation in 2012.  He established a [[pirate radio]] station to champion the Biafran cause, Radio Biafra, which has been broadcasting at various frequencies around the world.  The Nigerian Government, through its broadcasting regulators, the Broadcasting Organisation of Nigerian (BON) and Nigerian Communications Commission (NCC), has sought to clamp down on the UK-based station with limited success. On 17 November 2015, the Abia state police command seized an IPOB radio transmitter in [[Umuahia]].<ref>[http://www.puoreports.com/2015/11/radio-biafra-transmitters-found-in.html"Radio Biafra Transmitter's Found in Umuahia,Abia State "]</ref><ref>{{cite web|url=http://newsrescue.com/radio-biafra-container-with-massive-transmitter-found-in-nnamdi-kanus-village/|title=Radio Biafra Container With Massive Transmitter Found In Nnamdi Kanu's Village - NewsRescue.com|publisher=}}</ref> Mr. Kanu is presently being detained by the federal government.

The various groups clamouring for the restoration of the independence of Biafra have often been beset with internal wranglings that have impeded its secessionist efforts. On 19 October 2015, Chief Ralph Uwazuruike of the Movement for the Actualization of Sovereign State of Biafra (MASSOB)  disclosed that the director of Radio Biafra and leader of the Indigenous People of Biafra (IPOB), Nnamdi Kanu, does not belong to the movement and was sacked for indiscipline and for inciting violence among members.<ref>{{cite web|url=http://www.vanguardngr.com/2015/10/confusion-as-massob-disowns-radio-biafra-boss-nnamdi-kanu/|title=Confusion, as MASSOB disowns Radio Biafra boss, Nnamdi Kanu - Vanguard News|date=19 October 2015|publisher=}}</ref><ref>{{cite web|url=http://dailypost.ng/2015/10/19/arrested-radio-biafra-boss-is-not-our-member-massob-leader-uwazuruike/|title=Arrested Radio Biafra boss is not our member – MASSOB leader, Uwazuruike|date=19 October 2015|publisher=}}</ref>

There has been a renewed, intense agitation for Biafran secession. Since August 2015, protests have erupted in cities across Nigeria's south-east. Though peaceful, the protesters have been routinely attacked by the Nigerian police and army, with scores of people reportedly killed. Many others have been injured and/or arrested.<ref>{{cite news|title=Half a century after the war, angry Biafrans are agitating again|url=http://www.economist.com/news/middle-east-and-africa/21679210-half-century-after-war-angry-biafrans-are-agitating-again-go-your-own-way|accessdate=29 November 2015|work=[[The Economist]]|date=28 November 2015}}</ref> On 23 December 2015, the federal government charged Nnamdi Kanu with treasonable felony in the Federal High Court in [[Abuja]].<ref>[http://www.thisdaylive.com/articles/fg-files-fresh-treason-charges-against-nnamdi-kanu/228726/ "FG Files Fresh Treason Charges against Nnamdi Kanu"] {{webarchive |url=https://web.archive.org/web/20151224181940/http://www.thisdaylive.com/articles/fg-files-fresh-treason-charges-against-nnamdi-kanu/228726/ |date=24 December 2015 }}</ref>

According to the South-East Based Coalition of Human Rights Organizations (SBCHROs), security forces under the directive of the federal government has killed 80 members of the Indigenous People of Biafra (IPOB) and their supporters between 30 August 2015 and 9 February 2016 in a [[2015-2016 Killing of Biafran Protesters|renewed clampdown]] on the movement .<ref>{{cite web|url=http://www.vanguardngr.com/2016/03/biafra-security-forces-killed-80-ipob-members-rights-coalition/|title=Biafra will not stand, Buhari vows - Vanguard News|date=6 March 2016|publisher=}}</ref>

A report by [[Amnesty International]] also accuses the Nigerian military of killing at least 17 unarmed Biafran separatists in the city of [[Onitsha]] prior to a march on 30 May 2016 commemorating the 49th anniversary of the initial secession of Biafra.<ref>{{cite web|url=http://af.reuters.com/article/nigeriaNews/idAFL8N191428?sp=true|title=Amnesty accuses Nigerian army of killing at least 17 unarmed Biafran separatists
- News by Country
- Reuters|publisher=}}</ref>

==Meaning of "Biafra" and location==
Little is known about the literal meaning of the word Biafra. The word Biafra most likely derives from the subgroup Biafar or Biafada<ref>{{cite web|url=http://www.ethnologue.com/show_language.asp?code=bif|title=Biafada: A language of Guinea-Bissau}}</ref> of the Tenda ethnic group who reside primarily in [[Guinea-Bissau]].<ref>{{cite web|url=http://www.joshuaproject.net/peopctry.php?rog3=PU&rop3=101403|title=The Joshua Project: Biafada, Biafar of Guinea-Bissau}}</ref> [[Manuel Álvares]] (1526–1583), a Portuguese Jesuit educator, in his work ''Ethiopia Minor and a geographical account of the Province of Sierra Leone'',<ref>{{cite web|url=http://digicoll.library.wisc.edu/cgi-bin/AfricaFocus/AfricaFocus-idx?id=AfricaFocus.Alvares01|title=Africa Focus: Ethiopia Minor and a geographical account of the Province of Sierra Leone : (c. 1615): Contents|publisher=}}</ref> writes about the ''"Biafar heathen"'' in chapter 13 of the same book.<ref>{{cite web|url=http://digicoll.library.wisc.edu/cgi-bin/AfricaFocus/AfricaFocus-idx?type=article&did=AFRICAFOCUS.ALVARES01.I0019&id=AfricaFocus.Alvares01&isize=M|title=Manuel Álvares, Chapter 13: The Biafar Heathen}}</ref> The word Biafar thus appears to have been a common word in the Portuguese language back in the 16th century.

===Historical maps===
[[Early modern]] maps of Africa from the 15th–19th centuries, drawn by European [[Cartography|cartographers]] from accounts written by explorers and travellers,  reveal some information about Biafra:
#The original word used by the European travellers was not ''Biafra'' but ''Biafara'',<ref>{{cite web|url=http://catalog.afriterra.org/viewMap.cmd?number=785 |title=Map of Africa from 1669|publisher=Afriterra Foundation, The Cartographic Free Library |accessdate=14 March 2010|location=USA}}</ref><ref>{{cite web|url=http://catalog.afriterra.org/zoomMap.cmd?number=785 |title=Map of Africa from 1669 |type=zoom |publisher=Afriterra Foundation, The Cartographic Free Library|accessdate=14 March 2010|location=USA}}</ref> ''Biafar''<ref>{{cite web|url=http://www.uflib.ufl.edu/maps/ALW1663L.JPG|title=Map of West Africa from 1729|publisher=University of Florida, George A. Smathers Libraries|accessdate=14 March 2010|location=USA}}</ref> and sometimes also ''Biafares''.<ref>{{cite web|url=http://www.lib.utexas.edu/maps/historical/africa_nw_1829.jpg|title=Map of North-West Africa, 1829|publisher=University of Texas Libraries|accessdate=14 March 2010|location=USA}}</ref>[[File:Guillaume Delisle Senegambia 1707.jpg|thumbnail|right|Senegambia 1707]]
#The exact original region of Biafra is not restricted to Eastern Nigeria alone. According to the maps, the European travellers used the word ''Biafara'' to describe the entire region east of River Niger going down to the Mount Cameroon region, thus including Cameroon and a large area around Gabon. The Bight of Biafra lies in the present South-South Region of Nigeria and was renamed to Bight of Bonny by the Nigerian Government. The words ''Biafara'' and ''Biafares'' also appear on maps from the 18th century in the area around Senegal and Gambia.<ref>{{cite web|url=https://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-central/central.html|title=1730 map - L’Isle, Guillaume de, 1675-1726|date=|publisher=[[Princeton University Library]]|accessdate=16 February 2017|location=Princeton, New Jersey, USA}}</ref>[[File:Gulf of Guinea Guillaume Lavasseur de Dieppe.jpg|thumbnail|right|French map of the Gulf of Guinea from 1849]]
Maps indicating the word ''Biafara'' (sometimes also ''Biafares'' or ''Biafar'') with corresponding year:
*[http://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-continent/1584ortelius.jpg 1584]
*[http://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-continent/1644%20blaeu.jpg 1644]
*[http://www.uflib.ufl.edu/maps/AMD8173L.JPG 1660]
*[http://hitchcock.itc.virginia.edu/SlaveTrade/collection/large/blaeu01.JPG 1662]
*[http://bell.lib.umn.edu/historical/Hafrica.html 1707]
*[http://www.uflib.ufl.edu/maps/ALW1663L.JPG 1729]
*[http://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-continent/1737%20hase.jpg 1737]
*[http://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-continent/1805%20cary.jpg 1805]
*[http://etc.usf.edu/maps/pages/000/32/32.htm 1858]
*[http://www.philatelicdatabase.com/wp-content/uploads/2014/12/Map-Africa-and-Cape-Colony-1871.jpg 1871]

Maps from the 19th century indicating Biafra as the region around today's Cameroon:
*[http://www.uflib.ufl.edu/maps/MAPAFRICAL001.JPG 1843]
*[http://libweb5.princeton.edu/visual_materials/maps/websites/africa/maps-continent/1880%20andriveau.jpg 1880]
*[http://www.philatelicdatabase.com/wp-content/uploads/2008/05/africa-map-1890.jpg 1890]
*[http://www.lib.msu.edu/branches/map/africa.jsp Additional maps from the Michigan State University Map Collection]

==See also==
*[[Ambazonia]]
*[[Chimamanda Ngozi Adichie]]: ''[[Half of a Yellow Sun]]''
*[[Bruce Mayrock]]
*[[2015-2016 Killing of Biafran Protesters]]

==References==
{{reflist|30em}}

{{commons category|Biafra|<br/>Biafra|position=right}}

{{Biafra topics}}
{{coord|6|27|N|7|30|E|source:kolossus-svwiki|display=title}}

{{DEFAULTSORT:Biafra}}
[[Category:Biafra| ]]
[[Category:Former countries in Africa]]
[[Category:Nigerian Civil War]]
[[Category:Former polities of the Cold War]]
[[Category:Former republics]]
[[Category:Former unrecognized countries]]
[[Category:History of Igboland]]
[[Category:History of Nigeria]]
[[Category:History of West Africa]]
[[Category:Separatism in Nigeria]]
[[Category:States and territories established in 1967]]
[[Category:States and territories disestablished in 1970]]
[[Category:Former territorial entities in Africa]]
[[Category:Library of Congress Africa Collection related]]
[[Category:1967 establishments in Nigeria]]
[[Category:1970 disestablishments in Nigeria]]