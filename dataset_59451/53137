{{Infobox journal
| title = Work and Occupations
| cover = [[File:Work and Occupations.tif]]
| editor = Daniel B. Cornfield
| discipline = 
| former_names = 
| abbreviation = 
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1974-present
| openaccess = 
| license = 
| impact = 0.886
| impact-year = 2011
| website = http://www.uk.sagepub.com/journals/Journal200911?siteId=sage-uk&prodTypes=any&q=Work+and+Occupations&fs=1
| link1 = http://wox.sagepub.com/content/current
| link1-name = Online access
| link2 = http://wox.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0730-8884
| eISSN =
| OCLC = 473107382
| LCCN = 82642816
}}
'''''Work and Occupations''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Industrial Relations]]. The journal's [[Editor-in-Chief|editor]] is Daniel B. Cornfield ([[Vanderbilt University]]). It has been in publication since 1974 and is currently published by [[SAGE Publications]].

== Scope ==
''Work and Occupations'' aims to provide a perspective on the dynamics of the workplace and to examine international approaches to work related issues. The journal is interdisciplinary and contains scholarship in areas such as gender and race relations, immigrant and migrant workers, and violence in the workplace.

== Abstracting and indexing ==
''Work and Occupations'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 0.886, ranking it 61 out of 137 journals in the category, ‘Sociology’,<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Sociology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> and 8 out of 24 journals in the category, ‘Industrial Relations & Labor’.<ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Industrial Relations & Labor |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://wox.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]