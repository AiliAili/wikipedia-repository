{{good article}}
{{Infobox military unit
| unit_name = 56th Independent Mixed Brigade
| native_name =
| image =
| alt =
| caption =
| dates = 1944–45
| country = Japan
| countries =
| allegiance =
| branch = [[Imperial Japanese Army]]
| type =
| role =
| size = Six infantry battalions with supporting units
| command_structure =
| garrison =
| garrison_label =
| nickname =
| patron =
| motto =
| colors = <!-- or | colours = -->
| colors_label = <!-- or | colours_label = -->
| march =
| mascot =
| anniversaries =
| equipment =
| equipment_label =
| battles = [[World War II]]
*[[Borneo campaign (1945)|Borneo Campaign]]
| decorations =
| battle_honours =
| battle_honours_label =
| disbanded =
<!-- Commanders -->
| current_commander =
| commander1 =
| commander1_label =
| notable_commanders =
<!-- Insignia -->
| identification_symbol =
| identification_symbol_label =
}}
__NOTOC__
The '''56th Independent Mixed Brigade''' was an [[Imperial Japanese Army]] unit of [[World War II]]. It was raised in June 1944 to reinforce the defences of Japanese-occupied [[Borneo]], and was initially stationed in the north-east of the island. In early 1945 most of the brigade's units were ordered to move to the [[Brunei Bay]] area of west Borneo, with the brigade's personnel subsequently making a difficult march across the centre of the island.

During June and July 1945 the 56th Independent Mixed Brigade saw combat against Australian forces in the [[Borneo campaign (1945)|Borneo Campaign]]. Its units were greatly under-strength at this time due to the casualties suffered while moving across Borneo, and the elements which engaged Australian forces were rapidly defeated. The remainder of the brigade withdrew into central Borneo, and surrendered to the Australians following the end of the war.

==History==

===Formation===
During mid-1944 the Imperial Japanese Army's General Staff decided to reinforce the garrisons of the southern Philippines and Borneo ahead of an expected [[Allies of World War II|Allied]] offensive to liberate these areas.{{sfn|Shindo|2016|p=68}} As part of this effort, the [[54th Independent Mixed Brigade|54th]], [[55th Independent Mixed Brigade|55th]], 56th, [[57th Independent Mixed Brigade|57th]] and [[58th Independent Mixed Brigade|58th]] [[Independent Mixed Brigades (Imperial Japanese Army)|Independent Mixed Brigades]] were ordered to be formed for service with the [[Southern Expeditionary Army Group]] during June.{{#tag:ref|Independent mixed brigades were large units with a strength of between 4,500 and 11,000 soldiers which generally comprised three to six large independent infantry battalions and small support units. This organisation structure was designed for occupation duties and operations against poorly-equipped enemy forces, and included fewer artillery guns and support personnel than those allocated to front line combat units of a similar size. However, due to Japan's deteriorating military situation many independent mixed brigades were committed to combat against major Allied forces.{{sfn|Rottman|2005|p=18}}{{sfn|Ness|2014|pp=175–176}}|group=Note}}{{sfn|Ness|2014|p=187}} Aside from the 58th Independent Mixed Brigade, which was formed by converting an existing unit, these new brigades were to be organised in forward areas from personnel shipped from Japan.{{sfn|Ness|2014|p=187}} Upon formation, the 56th Independent Mixed Brigade was assigned to the [[Thirty-Seventh Army (Japan)|Borneo Defence Force]] (BDF).{{#tag:ref|The Borneo Defence Force was re designated the 37th Army in September 1944. The 56th Independent Mixed Brigade continued to report directly to the Army headquarters.{{sfn|Long|1963|pp=470}}|group=Note}}{{sfn|Shindo|2016|p=68}}

The process of establishing the 56th Independent Mixed Brigade took longer than expected, which delayed its deployment to Borneo. While three battalions worth of personnel departed Japan in mid-July 1944, the first did not arrive at Borneo until September. The final elements of the 56th Independent Mixed Brigade reached Borneo in mid-November.{{sfn|Shindo|2016|pp=68, 70}} The 8,000 personnel allocated to the brigade had not trained together while in Japan, and were organised into fighting units after their arrival in Borneo.{{sfn|Ness|2014|p=187}}{{sfn|Headquarters United States Army, Japan|1980|p=32}} They were also shipped to Borneo separately from their weapons and other equipment.{{sfn|Headquarters United States Army, Japan|1980|p=24}} Once its organisation was complete, the 56th Independent Mixed Brigade comprised six infantry [[battalion]]s (the 366th to 371st Independent Infantry Battalions), each with an authorised strength of 997 men, and small artillery, engineer and signal units. Like the other independent mixed brigades raised in mid-1944, the 56th had only 18 trucks and a small number of horses to transport its equipment and supplies.{{sfn|Ness|2014|pp=187–188}} The brigade was commanded by Major General [[Taijiro Akashi]].{{sfn|Dredge|1998|p=574}}

The BDF and its superior headquarters, the [[Japanese Seventh Area Army|Seventh Area Army]], initially disagreed over where the 56th Independent Mixed Brigade should be deployed. In July 1944 the BDF believed that the Allies were likely to invade northern Borneo in January 1945 or later, and wanted to station the brigade in this area. However, the Seventh Area Army judged that Allied forces were more likely to attack the [[Brunei Bay]] region in western Borneo to support a drive on [[Singapore]]. Moreover, the area army believed that it would be difficult to transfer units from northern to western Borneo due to the island's rugged terrain. The BDF eventually won the debate, and the 56th Independent Mixed Brigade's units were landed in the [[Sandakan]] area of north-east Borneo as they arrived from Japan.{{sfn|Shindo|2016|pp=69}}

Once the 56th Independent Mixed Brigade arrived in Borneo, the brigade headquarters and all the combat battalions other than the 371st Independent Infantry Battalion were concentrated at [[Tawau]]. The 371st Independent Infantry Battalion was stationed in Sandakan and placed under the command of the 41st Independent Garrison Battalion.{{sfn|Headquarters United States Army, Japan|1980|p=33–34}} As of 20 December 1944, the brigade had responsibility for defending the area around Tawau and [[Tarakan Island]].{{sfn|Headquarters United States Army, Japan|1980|p=36}}

===Redeployment===
[[File:N borneo ops 1945.jpg|thumb|alt=Map of the Brunei Bay area marked with coloured arrows and dates showing the movements of the main units involved in the Battle of North Borneo, including those described in this article|250px|A map showing the movements of the main Australian infantry units in North Borneo during June and July 1945.]]
In late 1944 the 37th Army (the former BDF) changed its views on likely Allied intentions, and concluded that the expected invasion would probably be focused on the Brunei Bay region. The Southern Expeditionary Army Group agreed with this assessment, and ordered in January 1945 that the 37th Army's forces be concentrated around Brunei Bay.{{sfn|Shindo|2016|pp=71}}{{sfn|Headquarters United States Army, Japan|1980|p=39}}  As the Allies had air superiority over Borneo and nearby waters, it was not possible to move the units in north-east Borneo by sea. Instead, they would need to march across the island.{{sfn|Pratten|2016|p=304}}

The 56th Independent Mixed Brigade, less its 369th and 370th Independent Infantry Battalions, began to march across Borneo in late January 1945. This proved difficult, with the movement through the mountainous jungle in the centre of the island being hampered by the poor state of the road network, bad weather and food shortages. Many of the brigade's soldiers fell sick during the march, and all four infantry battalions were considerably below their authorised strength by the time they arrived at Brunei Bay between April and June.{{sfn|Shindo|2016|pp=71}}{{sfn|Pratten|2016|p=304}}{{sfn|Dredge|1998|pp=574–575}}{{sfn|Long|1963|pp=495}} The 370th Independent Infantry Battalion was transferred to the direct control of the 37th Army and  remained at Tawau, and the 369th Independent Infantry Battalion was shipped to [[Banjarmasin]] in southern Borneo during March and assigned to the 22nd Special Naval Base Force.{{sfn|Dredge|1998|pp=574–575}}{{sfn|Long|1963|pp=495}}

The brigade was widely dispersed following its arrival in the Brunei Bay area. In June 1945, the brigade headquarters and the 366th and 367th Independent Infantry Battalions were stationed in [[Brunei]]. The 368th Independent Infantry Battalion was located at the inland town of [[Beaufort, Malaysia|Beaufort]], and the 371st Independent Infantry Battalion formed the main body of the island of [[Labuan]]'s garrison.{{sfn|Long|1963|pp=495}} Of the support units, the 56th Independent Mixed Brigade Artillery Unit was at Brunei and the 56th Independent Mixed Brigade Engineer and Signal Units were at Beaufort.{{sfn|Dredge|1998|p=581}} All of the brigade's heavy weapons had been left behind at Tawau, and due to the casualties incurred during the march across Borneo its combat units were not fit for battle.{{sfn|Rottman|2002|p=262}}

===Battle of North Borneo===

Two brigades of the Australian [[9th Division (Australia)|9th Division]] with large numbers of support units [[Battle of North Borneo|invaded the Brunei Bay area]] on 10 June 1945. At this time the 56th Independent Mixed Brigade was the main Japanese combat force in the region, but was in the process of withdrawing inland to defensive positions located around rice-growing areas.{{sfn|Pratten|2016|p=304}}

The 371st Independent Infantry Battalion was [[Battle of Labuan|destroyed on Labuan]] by the [[24th Brigade (Australia)|24th Brigade]] during 11 days of at times fierce fighting. The 367th Independent Infantry Battalion retreated from Brunei to the [[Trusan River]] as a formed unit, and the 366th Independent Infantry Battalion broke up into small groups as it fought a rearguard action against the Australian [[20th Brigade (Australia)|20th Brigade]]. While Beaufort was attacked by the 24th Brigade, the 368th Independent Infantry Battalion was not involved in its defence.{{sfn|Long|1963|pp=495}}{{sfn|Dredge|1998|p=575}}

During the last weeks of the war the remnants of the 366th Independent Infantry Battalion were pursued by Australian Army patrols, air attacks and groups of local [[Dayak people|Dayaks]] organised by the Australian [[Services Reconnaissance Department]].{{sfn|Long|1963|pp=495}} These attacks led to the disintegration of the unit.{{sfn|Majid|2007|p=17}} Major General Akashi led the remnants of the forces stationed at Brunei as well as a number of Japanese civilians to the inland town of [[Tenom]]; this trek took 40 days and involved traversing mountainous and trackless terrain.{{sfn|Headquarters United States Army, Japan|1980|p=43}} However, the forces involved suffered heavy casualties, with 50 percent of the 367th Battalion's soldiers being killed between 10 June and the end of the war.{{sfn|Majid|2007|p=18}} The 369th and 370th Independent Infantry Battalions did not see combat, and remained largely intact at the end of the war.{{sfn|Dredge|1998|p=575}} Following the Japanese surrender, the survivors of the 56th Independent Mixed Brigade were concentrated in locations selected by the Australian forces.{{sfn|Dredge|1998|p=598}}

==Structure==

The 56th Independent Mixed Brigade comprised the following units:<ref name=Nafziger>{{cite web|last1=Nafziger|first1=George|title=Organization of Japanese Independent Infantry Brigades and Groups 1939–1945|url=http://www.cgsc.edu/CARL/nafziger/939JXAC.PDF|website=The Nafziger Collection of Orders of Battle|publisher=Ike Skelton Combined Arms Research Library|accessdate=6 April 2015}}</ref>
*Brigade headquarters
*366th Independent Infantry Battalion
*367th Independent Infantry Battalion
*368th Independent Infantry Battalion
*369th Independent Infantry Battalion
*370th Independent Infantry Battalion
*371st Independent Infantry Battalion
*56th Independent Mixed Brigade Artillery Unit
*56th Independent Mixed Brigade Engineer Unit
*56th Independent Mixed Brigade Signal Unit

==References==

===Footnotes===
{{Reflist|group=Note}}

===Citations===
{{reflist|colwidth=30em}}

===Bibliography===
* {{cite book|editor1-last=Gin|editor1-first=Ooi Keat|title=Japanese Empire in the Tropics: Selected Documents and Reports of the Japanese Period in Sarawak Northwest Borneo 1941–1945|volume=Volume 2|date=1998|publisher=Ohio University Press|location=Athens, Ohio|isbn=0896801993|ref=harv|first=A.C.L.|last=Dredge|chapter=Order of Battle: Intelligence Bulletin No. 237, 15 June 1946|pages=572–598}}
*{{cite book|last1=Headquarters United States Army, Japan|title=War in Asia and the Pacific: Volume 6 The Southern Area (Part I)|date=1980|publisher=Garland|location=New York|isbn=082403290X|chapter=Borneo Operations (1941–1945) [Monograph No. 26]|origyear=1957|ref=harv}}
* {{cite book |last=Long |first=Gavin |authorlink=Gavin Long |editor= |title=The Final Campaigns |url=https://www.awm.gov.au/collection/RCDIG1070206/|format= |series=Australia in the War of 1939–1945|volume=Series 1 – Army. Volume VII|year=1963 |publisher=Australian War Memorial |location=Canberra|oclc=1297619|ref=harv}}
* {{cite book|last1=Majid|first1=Abdul Harun|title=Rebellion in Brunei: The 1962 Revolt, Imperialism, Confrontation and Oil|date=2007|publisher=I.B.Tauris|location=London|isbn=9781845114237|ref=harv}}
* {{cite book |last=Ness |first=Leland S. |location=Solihull, United Kingdom |publisher=Helion |year=2014 |title=Rikugun: Guide to Japanese Ground Forces, 1937–1945. Volume 1: Tactical Organization of Imperial Japanese Army & Navy Ground Forces |isbn=9781909982000 |ref=harv }}
* {{cite book|last1=Pratten|first1=Garth|authorlink=Garth Pratten|editor1-last=Dean|editor1-first=Peter J.|title=Australia 1944–45|date=2016|publisher=Cambridge University Press|location=Port Melbourne, Australia|isbn=9781107083462|chapter='Unique in the history of the AIF': Operations in British Borneo|pages=298–319|ref=harv}}
* {{cite book |title=World War II Pacific Island Guide. A Geo-Military Study |last=Rottman |first=Gordon L. |year=2002 |publisher=Greenwood Press |location=Westport, Connecticut|isbn=0-313-31395-4|ref=harv}}
* {{cite book|last1=Rottman|first1=Gordon|title=Japanese Army in World War II: South Pacific and New Guinea, 1942–43|date=2005|publisher=Osprey|location=Oxford|isbn=1841768707|ref=harv}}
* {{cite book|last1=Shindo|first1=Hiroyuki|editor1-last=Dean|editor1-first=Peter J.|title=Australia 1944–45|date=2016|publisher=Cambridge University Press|location=Port Melbourne, Australia|isbn=9781107083462|chapter=Holding on to the finish: The Japanese Army in the South and South West Pacific 1944–45|pages=51–76|ref=harv}}

{{DEFAULTSORT:56th Independent Mixed Brigade}}
[[Category:Independent Mixed Brigades (Imperial Japanese Army)]]
[[Category:Military units and formations established in 1944]]
[[Category:Military units and formations disestablished in 1945]]
[[Category:1944 establishments in Japan]]
[[Category:1945 disestablishments in Japan]]