{{other}}
"'''Brand New Love'''" is a 1986 song written by [[Lou Barlow]]. It was first released independently by Barlow under the moniker [[Sentridoh]], but has since come to be associated with [[Sebadoh]], the band Barlow formed with [[Eric Gaffney]].

==Weed Forestin' recording==
{{Infobox song
| Name    = Brand New Love
| Artist   = [[Lou Barlow|Sentridoh]]
| Album   = [[Weed Forestin']]
| Released  = 1987
| track_no  = 23
| Recorded  = 1986
| Genre   = [[Lo-fi music|Lo-fi]]
| Length   = 2:29
| Writer   = [[Lou Barlow]]
| Label   = Not on label
}}

Barlow made a four-track home recording of "Brand New Love" in 1986, during the same period that he was an active member of [[Dinosaur Jr.]]<ref>{{cite web|url=http://pitchfork.com/news/45412-lou-barlow-reissues-sentridohs-weed-forestin/ |title=Lou Barlow Reissues Sentridoh's Weed Forestin' &#124; News |publisher=Pitchfork |date=2012-02-16 |accessdate=2014-02-24}}</ref> That four-track recording closes out the album ''[[Weed Forestin']]'', which was first self-released by Lou Barlow on cassette in 1987 under the name Sentridoh. In 1990, that same recording was officially and more broadly released by [[Homestead Records]], but with the attribution changed to Sebadoh.

The ''Weed Forestin''' version of "Brand New Love" also appears on Sebadoh's compilation album released in 1990, entitled ''[[The Freed Weed]]'', also released through [[Homestead Records]].

On the compact disc release of ''The Freed Weed'', the track runs for three minutes: the last thirty seconds of the track consists of inter-cut fragments of other recordings. Fragments include a sped-up section of Dinosaur Jr.'s "The Lung", and two repeats of a section of Dinosaur Jr.'s "Throw Down", the first of which is slowed, the second of which is at regular speed.

==Sebadoh vs Helmet recording==
{{Infobox song
| Name    = Brand New Love
| Artist   = [[Sebadoh]]
| Album   = [[Sebadoh vs Helmet]]
| Released  = 1992
| track_no  = 2
| Genre   = [[Indie rock]], [[Lo-fi music|lo-fi]], [[noise pop]]
| Length   = 4:04
| Writer   = [[Lou Barlow]]
| Label   = [[Domino Recording Company|Domino]]
}}

In 1992, Sebadoh released their ''[[Sebadoh vs Helmet]]'' EP in the UK through 20/20 Recordings and [[Domino Recording Company|Domino]]. The EP featured a new studio recording of "Brand New Love" performed by the Sebadoh's then three-piece line-up of Barlow, Gaffney and [[Jason Loewenstein]].

Steve Taylor wrote that the "early lo-fi version of "Brand New Love" was taken up to mid-fi on the... ''Sebadoh vs Helmet'' mini-album".<ref>{{cite book|last=Taylor|first=Steve|title=The A to X of Alternative Music|year=2006|publisher=Continuum|isbn=0826482171|page=222|url=https://books.google.com/books?id=KPOsu8JOHO8C&dq}}</ref> At a little over four minutes, this studio version is slower and significantly longer that Barlow's original home recording. While the lyrics are essentially the same, the arrangement is more sophisticated with drums, bass guitar, distorted electric guitar and a second vocalist added to the mix. The opening riff uses the same picked notes as were used throughout the verse of the earlier version, but the pattern of the picking has changed: while the ''Weed Forestin''' version used a trochaic pattern, this one consists of two dactyls and a trochee. The most significant additional section is an intense noise-rock outro with layered guitars and a feedback-laden crescendo.

The ''Sebadoh vs Helmet'' version also appeared later the same year on Sebadoh's 1992 [[Sub Pop]] compilation album ''[[Smash Your Head on the Punk Rock]]'' as well as a number of various artist compilations, including the [[City Slang]] compilation ''Slanged!'', and the Eurostar compilation ''Going Underground Vol. II''.

==Cover versions==
* [[Superchunk]]'s 1991 release, ''[[Superchunk discography#7.22 Singles and EP.27s|The Freed Seed EP]]'', includes a cover version of "Brand New Love" as well as two other Sebadoh songs. This recording also appeared on Superchunk's compilation ''[[Tossing Seeds (Singles 89–91)]]''.
* In 1993, Superchunk released a live rendition of "Brand New Love" as the penultimate track on their VHS format release ''[[Take The Tube]]'', a video of a live concert at the University of London. "Brand New Love" was the only cover song on the set list.<ref>{{cite web|title=Superchunk, Take the Tube|url=http://www.discogs.com/Superchunk-Take-The-Tube/release/3222992|work=Discogs|accessdate=31 December 2013}}</ref>
* [[Lee Ranaldo]] of [[Sonic Youth]] released a version of the "Brand New Love" as the fifth track on his 1994 solo EP ''[[Broken Circle / Spiral Hill EP|Broken Circle / Spiral Hill]]''. The majority of the EP was recorded in a New York studio in 1994, but Ranaldo's version of "Brand New Love" was recorded in a Leeds hotel room, UK, December 1992.<ref>{{cite web|title=Lee Ranaldo, Broken Circle/Spiral Hill|url=http://www.discogs.com/Lee-Ranaldo-Broken-Circle-Spiral-Hill/release/622441|work=Discogs|accessdate=31 December 2013}}</ref>
* In 1995, Australian band [[Screamfeeder]] released a version of "Brand New Love" as a b-side to the single 'Who's Counting?/Sweet Little Oranges' from the album ''[[Fill Yourself With Music]]''. This version also appeared on the [[Spunk Records]] compilation ''Star Trackers''.
* Californian synth-rock band [[Deadsy]] included "Brand New Love" on their 2002 album ''[[Commencement (album)|Commencement]]''. A promotional video was released for the cover version.
* In 2005, [[Death Cab For Cutie]]'s ''[[The John Byrd EP]]'' concluded with a live medley that blends their track "Blacking Out The Friction" with Barlow's "Brand New Love".
* [[Ben Lee]] performed a live acoustic version of "Brand New Love" for the candid music video website shoottheplayer.com in March 2009.<ref>{{cite web|title=Ben Lee, Brand New Love|url=http://vimeo.com/4342896|publisher=Shoot The Player|accessdate=30 December 2013}}</ref>

==References==
{{Reflist}}
{{Sebadoh}}
[[Category:1986 songs]]
[[Category:Sebadoh songs]]
[[Category:Songs written by Lou Barlow]]