{{merge to|Preprint|date=December 2014|discuss=Talk:Preprint#Merge from Manuscript (publishing)}}
"'''Manuscript'''" is a broad concept in [[publishing]], that can about one or both: 
* the formatting of a ''short story manuscript'', 
* an accepted manuscript (by its merit not its format), not yet in a final format (but reviewed), published with non-final-format in ahead, as ''preprint''.

A manuscript is the work that an author submits to a publisher, [[editing|editor]], or producer for [[publishing|publication]]. Even with the advent of [[desktop publishing]], making it possible for anyone to prepare text that appears professionally [[typesetting|typeset]], many publishers still require authors to submit manuscripts within their respective guidelines.

== Manuscript format ==

Although publishers guidelines for formatting are the most critical resource for authors,<ref>{{cite book |title= Formatting and Submitting your Manuscript|edition= 3rd|last1= Sambuchino|first1= Chuck |last2= The Editors of Writer's Digest Books|year= 2009|publisher= Writer's Digest Books|location= Cincinnati, OH|isbn= 978-1-58297-571-9|pages=5, 10}}
</ref> [[style guide]]s are also key references for authors preparing manuscripts since "virtually all professional editors work closely with one of them in editing a manuscript for publication."<ref>{{cite book |title= The Pocket Idiot's Guide to Grammar and Punctuation: A Handy Reference to Resolve All Your Grammatical Problems|last= Stevenson|first=Jay|year=2005|publisher=Alpha Books|isbn=978-1-59257-393-6|page=viii}}</ref>

Manuscript formatting (also named '''standard manuscript format''') depends greatly on the type of work that is being written, as well as the individual publisher, editor or producer. Writers who intend to submit a manuscript should determine what the relevant writing standards are, and follow them. Individual publishers' standards will take precedence over [[style guide]]s.<ref>{{cite book |title= Formatting and Submitting your Manuscript|edition= 3rd|last1= Sambuchino|first1= Chuck |last2= The Editors of Writer's Digest Books|year= 2009|publisher= Writer's Digest Books|location= Cincinnati, OH|isbn= 978-1-58297-571-9|pages=10–11}}</ref>

== Preprint ==

An ordinary ''manuscript'' only becomes a "publisher's [[preprint]]" if it somehow gets distributed beyond the authors (or the occasional colleague whom they ask for advice), but the "publisher" here is a loose concept. Illustrating:

* In a [[peer review]] context: if an author prepares a manuscript on their computer and submit it to a publisher for review, and the submission is not accepted, there never was any "publisher's preprint".
* In a web context (legal/cultural authorship): if an author needs to ensure authorship, they can use a [[Disciplinary repository|repository]] to upload a version of their publication before full publication. In this context, web-publishing is not the only alternative, it is possible to use a [[legal deposit]] of the manuscript.

In both contexts, a future "final print" is planned &ndash; with [[Page layout|better layout]], [[proofreading]] and some kind of [[prepress proofing]] &ndash; that will replace the "preprinted ''manuscript''".

==References==
{{Reflist}}

[[Category:Writing]]
[[Category:Publishing]]
[[Category:Academic journal articles]]
[[Category:Academic publishing]]