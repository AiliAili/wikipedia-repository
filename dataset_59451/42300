<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Baldwin Red Devil
 | image=Baldwin Red Devil 1.jpg
 | caption=Baldwin's Red Devil, located at the [[Steven F. Udvar-Hazy Center]]
}}{{Infobox Aircraft Type
 | type=
 | national origin=United States of America
 | manufacturer=[[Thomas Scott Baldwin]]
 | designer=[[Thomas Scott Baldwin]]
 | first flight=1911
 | introduced=1911
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=6
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Baldwin Red Devil''' was a series of early [[pusher configuration]] aircraft employing steel tube construction. The aircraft were designed by [[Thomas Scott Baldwin]], an early pioneer of airships.

==Development==
[[File:Thomas Scott Baldwin (LOC).jpg|right|thumbnail|Baldwin at the wheel of the Red Devil]]
[[File:Peoli 2496405341 eb44d33c66 o.jpg|thumb|[[Cecil Peoli]] circa 1915 in front of a ''Red Devil'']]

After building several aircraft, Baldwin had C. and A. Wittemann of [[Staten Island, New York]] build an aircraft similar to the basic [[Curtiss Model D|Curtiss Pusher]]. The aircraft used fabric covered steel tubing instead of wood construction. A {{convert|60|hp}}, Hall-Scott V-8 provided engough power for {{convert|60|mi/h|km/h|abbr=on}} flights. The aircraft was named "Red Devil III", and subsequent designs would be named  "Baldwin Red Devil". Each of the Red Devil's tubing were painted a bright red, with yellow contrasting wings.<ref>{{cite web|title=Baldwin Red Devil|url=http://www.nasm.si.edu/collections/artifact.cfm?id=A19500094000|accessdate=26 October 2010}}</ref>

==Design==
The airplane was powered by water cooled Hall-Scott V-8 engine of {{convert|60|hp|kW|0|abbr=on}}, The aircraft was covered with vulcanized fabic that was tacked to the ribs rather than rib-stitched. The landing gear was made from steel tubes with hickory wood inserts. Control cables were routed through copper tubing. It had a left foot controlled throttle and a right foot controlled magneto kill switch.<ref>{{cite book|title=Aeronautics: the American magazine of aerial locomotion, Volume 8|author=Aeronautical Society of America|year=1911}}</ref>

==Operational history==
The "Red Devil III" was test flown by Baldwin over Mineola Field. The first flight resulted in a wreck into a telegraph pole, with no major injuries.<ref>{{cite book|title=Bladwind, The Luckiest Aviator|author=Arnold Kruckman}}</ref> The Baldwin School offered flying lessons in the aircraft for $500 (1911 dollars), if the student proved they had life insurance for flight. It was the student's responsibility to also find an insurance broker to provide the policy, because there were no flight training policies available at that time.<ref>{{cite book|title=Aeronautics: the American magazine of aerial locomotion, Volume 8|author=Aeronautical Society of America|year=1911}}</ref>  Baldwin previously had toured the country in Curtiss aircraft and his own designs. By the time he had produced the "Red Devil", he had more bookings then he could handle, the "Red Devil" would be advertised in several towns in close proximity, then substitute pilots and aircraft would be flown with full crowds of spectators.<ref>{{cite book|title=Talespins: a story of early aviation days|author=Edith Dodd Culver}}</ref> In one display in St. Louis he flew the "Red Devil" under the [[Eads Bridge|Eads]] and [[McKinley Bridge]]s in St.Louis.<ref>{{cite book|title=Tales and trails of Illinois|author=Stu FliegeTake}}</ref>
On October 12, 1913, [[Tony Jannus]] flew actress [[Julia Bruns]] in a Red Devil in a [[New York Times]] Derby.

==Variants==
A Red Devil is on display at the [[Smithsonian Institution]]'s [[Udvar-Hazy Center]] (UHC) of the  [[National Air and Space Museum]]. This example has a different configuration without a forward mounted elevator. The aircraft was purchased in 1950 from an established display at [[Roosevelt Field]], in [[Mineola, Long Island]], New York by [[Paul E. Garber]] along with a [[Bleriot XI]], and [[Nieuport 10]] for $2500.<ref>{{cite web|title=Roosevelt Field Museum |url=http://www.ww1aeroinc.org/files/Roosevelt_Field_204.pdf |accessdate=26 Oct 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20111008095002/http://www.ww1aeroinc.org/files/Roosevelt_Field_204.pdf |archivedate=2011-10-08 |df= }}</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Baldwin Red Devil) ==
{{Aircraft specs
|ref=Smithsonian<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=
|length ft=30
|length in=
|length note=
|span m=
|span ft=42
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=8
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=750
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Hall-Scott V-8
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=60<!-- prop engines -->

|prop blade number=1<!-- propeller aircraft -->
|prop name=Gibson
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=7<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=[[Thomas Scott Baldwin#Red Devil]]
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Baldwin Red Devil}}
{{reflist}}
<!-- ==External links== -->
<!-- Navboxes go here -->

[[Category:United States experimental aircraft 1910–1919]]
[[Category:Single-engine aircraft]]
[[Category:Baldwin aircraft|Red Devil]]