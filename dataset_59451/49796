<!--Novel article template : part of the Wikipedia:WikiProject Novels-->
{{Infobox book
| name             = '''''Sartor Resartus'''''
| image            = Sullivan carlyle.jpg
| caption          = ''Teufelsdröckh in Monmouth Street'', illustration to ''Sartor Resartus'' by [[Edmund Joseph Sullivan]].
| author           = [[Thomas Carlyle]]
| title_orig       = 
| translator       = 
| illustrator      = 
| cover_artist     = 
| country          = Great Britain
| language         = English
| series           = 
| subject          = 
| genre            = [[Comic novel|Comic Novel]], [[Sui generis]]
| publisher        = [[Fraser's Magazine]]
| pub_date         = [[1833 in literature|1833-1834]]
}}
'''''Sartor Resartus''''' (meaning 'The tailor re-tailored') is an 1836 novel by [[Thomas Carlyle]], first published as a serial in 1833–34 in ''[[Fraser's Magazine]]''. The novel purports to be a commentary on the thought and early life of a German philosopher called Diogenes Teufelsdröckh (which translates as 'god-born devil-dung'),<ref>"The full name, Diogenes Teufelsdröckh, God-born Devil's Dung, indicates the combination in one person of the half malicious Swiftian satire with the ethereal idealism of a Fichte or a Goethe. Carlyle calls attention to this twofold nature of his hero in numerous places. In the chapter on ''Reminiscences'' the editor remembers seeing in his eyes 'gleams of an ethereal or else a diabolic fire'; in the chapter on ''Characteristics'' we are told that his voice screws itself aloft 'as into the song of spirits, or else the shrill mockery of fiends,' that at times we distinguish 'gleams of an ethereal love, 'soft wailings of infinite pity,' and at others "some half invisible wrinkle of a bitter sardonic humor" so that 'you look on him almost with a shudder, as on some incarnate Mephistopheles.' His eyes again are described as sparkling with lights, which 'may indeed be reflexes of the heavenly stars, but perhaps also gleams from the region of Nether Fire'." — Johnson, William Savage (1911). "Sartor Resartus." In: [https://archive.org/stream/thomascarlylestu00johnrich#page/n9/mode/2up ''Thomas Carlyle: A Study of his Literary Apprenticeship, 1814-1831'']. New Haven: Yale University Press, pp. 113–114.</ref> author of a tome entitled "Clothes: Their Origin and Influence", but was actually a [[poioumenon]] ("product").<ref>Fowler, Alastair. ''The History of English Literature'', p. 372 [[Harvard University Press]], [[Cambridge, MA]] (1989) ISBN 0-674-39664-2</ref> Teufelsdröckh's [[Transcendentalist]] musings are mulled over by a sceptical English Reviewer (referred to as Editor) who also provides fragmentary biographical material on the philosopher. The work is, in part, a parody of [[Hegel]], and of [[German Idealism]] more generally. However, Teufelsdröckh is also a literary device with which Carlyle can express difficult truths.

==Background==

[[Archibald MacMechan]] surmised that the novel's invention had three literary sources. The first being ''[[The Tale of a Tub]]'' by [[Jonathan Swift]], whom Carlyle intensely admired in his college years, even going by the nicknames "Jonathan" and "The Dean". In that work, the three main traditions of Western Christianity are represented by a father bestowing his three children with clothes they may never alter, but proceed to do so according to fashion.{{sfn|Carlyle|1896|p=xx}} The second being Carlyle's work translating [[Goethe]], particularly ''[[Wilhelm Meister's Apprenticeship]]'', ''[[The Sorrows of Young Werther]]'', and ''[[Goethe's Faust|Faust]]'', all of which are quoted and explicitly referred to, especially in Teufelsdröckh's crisis being named "The Sorrows of Young Teufelsdröckh". The third being ''[[Tristram Shandy]]'' from which Carlyle quotes many phrases, and he referred to earlier in his letters.<ref>"My first favourite books had been ''Hudibras'' and ''Tristram Shandy''." qtd in {{harv|Carlyle|1896n|p=xx}}</ref>

[[File:Picture of Carlyle's Craigenputtock.jpg|thumb|280px|[[Craigenputtock|Craigenputtock House]].]]
Carlyle worked on an earlier novel, '''''Wotton Reinfred''''' which Macmechan refers to as "The first draft of ''Sartor''".{{sfn|Carlyle|1896|p=xxiii}} Carlyle finished seven chapters of the semi-autobiographical novel depicting a young man of deeply religious upbringing being scorned in love, and thereafter wandering. He eventually finds at least philosophical consolation in a mysterious stranger named Maurice Herbert who invites Wotton into his home and frequently discusses with him speculative philosophy. At this point the novel abruptly shifts to highly philosophical dialogue revolving mostly around [[Immanuel Kant|Kant]].{{sfn|Carlyle|1896|pp=xxiii-xxvii}} Though the unfinished novel deeply impressed Carlyle's wife [[Jane Carlyle|Jane]],{{sfn|Carlyle|1896|p=xxiv}} Carlyle never published it and its existence was forgotten until long after Carlyle's death. Macmechan suggests that the novel provoked Carlyle's frustration and scorn due to his "zeal for truth and his hatred for fiction" spoken of in his letters of the time.{{sfn|Carlyle|1896|p=xxvii}} Numerous parts of ''Wotton'' appear in the biographical section of ''Sartor Resartus'', in which Carlyle humorously sentences them to the bags containing Teufelsdröckh's autobiographical sketches, which the editor constantly complains about being overly fragmented or derivative of [[Goethe]]. Though widely and erroneously reported as having been burned by Carlyle, the unfinished novel is still extant in draft form, several passages being moved verbatim to ''Sartor Resartus'' but with their context radically changed.{{sfn|Carlyle|1896|p=xxiv}}

Carlyle had difficulty finding a publisher for the novel and began composing it as an article in October 1831 at [[Craigenputtock]].<ref>[[Fred Kaplan (biographer)|Fred Kaplan]], [http://www.oxforddnb.com/view/article/4697 "Carlyle, Thomas (1795–1881)"], ''Oxford Dictionary of National Biography'', Oxford University Press, 2004; online ed., 2008, accessed 10 Jan 2011.</ref> [[Fraser's Magazine]] serialised it in 1833-1834. The text would first appear in volume form in Boston in 1836, its publication arranged by [[Ralph Waldo Emerson]], who much admired the book and Carlyle. Emerson's savvy dealing with the overseas publishers would ensure Carlyle received high compensation that the novel did not attain in Britain.{{sfn|Carlyle|1896|p=xix}}  The first British edition would be published in London in 1838.<ref>Campbell, Ian. "Thomas Carlyle." ''Victorian Prose Writers Before 1867''. Ed. William B. Thesing. Detroit: Gale Research, 1987. Dictionary of Literary Biography Vol. 55. Literature Resource Center. Web. 10 Jan. 2011.</ref>

==Plot==

The novel takes the form of a long review by a somewhat cantankerous unnamed Editor for the English Publication ''Fraser's Magazine'' (in which the novel was first serialized without any distinction of the content as fictional) who is upon request, reviewing the fictional German book ''Clothes, Their Origin and Influence'' by the fictional philosopher Diogenes Teufelsdröckh (Professor of "Things in General" at Weissnichtwo University). The Editor is clearly flummoxed by the book, first struggling to explain the book in the context of contemporary social issues in England, some of which he knows Germany to be sharing as well, then conceding that he knows Teufelsdröckh personally, but that even this relationship does not explain the curiosities of the book's philosophy. The Editor remarks that he has sent requests back to the Teufelsdrockh's office in Germany for more biographical information hoping for further explanation, and the remainder of Book One contains summaries of Teufelsdröckh's book, including translated quotations, accompanied by the Editor's many objections, many of them buttressed by quotations from [[Johann Wolfgang von Goethe|Goethe]] and [[William Shakespeare|Shakespeare]]. The review becomes longer and longer due to the Editor's frustration at the philosophy, but desire to expose its outrageous nature. At the final chapter of Book One, the Editor has received word from the Teufelsdröckh's office in the form of several bags of paper scraps (rather esoterically organized into bags based on the  [[Zodiac#The twelve signs|signs of the Latin Zodiac]]) on which are written autobiographical fragments.

[[File:Room in Which "Sartor Resartus" Was Written.jpg|thumb|right|300px|Room in which ''Sartor Resartus'' was written.]]
At the writing of Book Two, the Editor has somewhat organized the fragments into a coherent narrative. As a boy, Teufelsdröckh was left in a basket on the doorstep of a childless couple in the German country town of Entepfuhl ("Duck-Pond"); his father a retired Sergeant of [[Frederick II of Prussia|Frederick the Great]] and his mother a very pious woman, who to Teufelsdröckh's gratitude, raises him in utmost spiritual discipline. In very flowery language, Teufelsdröckh recalls at length the values instilled in his idyllic childhood, the Editor noting most of his descriptions originating in intense spiritual pride. Teufelsdröckh eventually is recognized as being clever, and sent to Hinterschlag (slap-behind) [[Gymnasium (school)|Gymnasium]]. While there, Teufelsdröckh is intellectually stimulated, and befriended by a few of his teachers, but frequently bullied by other students. His reflections on this time of his life are ambivalent; glad for his education, but critical of that education's disregard for actual human activity and character; for both his own treatment, and his education's application to politics. While at University, Teufelsdröckh encounters the same problems, but eventually gains a small teaching post some favour and recognition from the German nobility. While interacting with these social circles, Teufelsdröckh meets a woman he calls Blumine (Goddess of Flowers; the Editor assumes this to be a pseudonym), and abandons his teaching post to pursue her. She spurns his advances for a British aristocrat named Towgood. Teufelsdröckh is thrust into a spiritual crisis, leaving the city to wander the European countryside, but even there encounters Blumine and Towgood on their honeymoon. He sinks into a deep depression, culminating in the celebrated [[The Everlasting No#The Everlasting Yea and No|Everlasting No]], disdaining all human activity. Still trying to piece together the fragments, the Editor surmises that Teufelsdröckh either fights in a war during this period, or at least intensely uses its imagery, which leads him to a "Centre of Indifference", and on reflection of all the ancient villages and forces of history around him, ultimately comes upon the affirmation of all life in "[[The Everlasting No#The Everlasting Yea and No|The Everlasting Yes]]". The Editor, in relief, promises to return to Teufelsdröckh's book, hoping with the insights of his assembled biography to glean some new insight into the philosophy.

===Characters===

'''Diogenes Teufelsdröckh:''' (Greek/German:"God-Born Devil-Dung") The Professor of "Things in General" at [[Sartor Resartus#Locales|Weissnichtwo University]], and writer of a long book of [[German idealism|German idealist philosophy]] called "Clothes, Their Origin and Influence," the review of which forms the contents of the novel. Both professor and book are fictional.

'''The Editor:''' The narrator of the novel, who in reviewing Teufelsdröckh's book, reveals much about his own tastes, as well as deep sympathy towards Teufelsdröckh, and much worry as to social issues of his day. His tone varies between conversational, condemning and even semi-Biblical prophesy. The Reviewer should not be confused with Carlyle himself, seeing as much of Teufelsdröckh's life implements Carlyle's own biography.{{sfn|Carlyle|1896|p=xxiii}}

'''Hofrath:''' Hofrath Heuschrecke (i. e. State-Councillor Grasshopper) is a loose, zigzag figure, a blind admirer of Teufelsdröckh's, an incarnation of distraction distracted, and the only one who advises the editor and encourages him in his work; a victim to timidity and preyed on by an uncomfortable sense of mere physical cold, such as the majority of the state-counsellors of the day were.<ref>{{Nuttall|title=Heuschrecke, Hofrath|inline=1}}</ref>

'''Blumine:''' A woman associated to the German nobility with whom Teufelsdröckh falls in love early in his career. Her spurning of him to marry Towgood leads Teufelsdröckh to the spiritual crisis that culminates in the Everlasting No. Their relationship is somewhat parodic of Werther's spurned love for Lotte in ''[[The Sorrows of Young Werther]]'' (including her name "Goddess of Flowers", which may simply be a pseudonym), though, as the Editor notes, Teufelsdröckh does not take as much incentive as does Werther. Critics have associated her with [[Kitty Kirkpatrick]], with whom Carlyle himself fell in love before marrying [[Jane Carlyle]].<ref name="Simon Heffer 1995, p. 48">Heffer, Simon (1995). ''Moral Desperado – A Life of Thomas Carlyle.'' London: Weidenfeld & Nicolson, p. 48.</ref><ref name="pakistanlink.com">http://www.pakistanlink.com/Opinion/2006/Jan06/06/04.HTM "East Did Meet West – 3," by Dr. Rizwana Rahim.</ref>

'''Towgood:''' The English Aristocrat who ultimately marries Blumine, throwing Teufelsdröckh into a spiritual crisis. If Blumine is indeed a fictionalization of [[Kitty Kirkpatrick]], Towgood would find his original in Captain James Winslowe Phillipps, who married Kirkpatrick in 1829.<ref name="Simon Heffer 1995, p. 48"/><ref name="pakistanlink.com"/>

===Locales===

'''Dumdrudge:''' Dumdrudge is an imaginary [[village]] where the natives drudge away and say nothing about it, as villagers all over the world contentedly do.<ref>{{Nuttall|title=Dumbdrudge|inline=1}}</ref>

''''Weissnichtwo:'''' In the book, Weissnichtwo (''weiß-nicht-wo,'' German for "don't-know-where") is an imaginary European city, viewed as the focus, and as exhibiting the operation, of all the influences for good and evil of the time, described in terms which characterised city life in the first quarter of the 19th Century; so universal appeared the spiritual forces at work in society at that time that it was impossible to say where they were and where they were not, and hence the name of the city, '''"Don't-know-where"''' (cf. Sir [[Walter Scott]]'s ''[[Kennaquhair]]'').<ref>{{Nuttall|title=Weissnichtwo|inline=1}}</ref>

==Themes and critical reception==

''Sartor Resartus'' was intended to be a new kind of book: simultaneously factual and fictional, serious and satirical, speculative and historical. It ironically commented on its own formal structure, while forcing the reader to confront the problem of where "truth" is to be found. In this respect it develops techniques used much earlier in ''[[Tristram Shandy]]'', to which it refers. The imaginary "Philosophy of Clothes" holds that meaning is to be derived from phenomena, continually shifting over history, as cultures reconstruct themselves in changing fashions, power-structures, and faith-systems. The book contains a very [[Fichte]]an conception of [[religious conversion]]: based not on the acceptance of God but on the absolute freedom of the will to reject evil, and to construct meaning. This has led some writers to see ''Sartor Resartus'' as an early [[existentialist]] text.

One of the recurring jokes is Carlyle giving humorously appropriate German names to places and people in the novel, such as the Teufelsdröckh's publisher being named Stillschweigen and co. (meaning Silence and Company) and lodgings being in Weissnichtwo (meaning Know-not-where). Teufelsdröckh's father is introduced as an earnest believer in [[The Life and Opinions of Tristram Shandy, Gentleman|Walter Shandy's]] doctrine that "there is much, nay almost all in Names."<ref>{{cite book|last=Carlyle|first=Thomas|title=Sartor Resartus|year=2008|publisher=Oxford Univ Pr|location=Oxford|isbn=9780199540372|page=67|edition=Reissued}}</ref>

[[Harold Bloom]] suggested that ''Sartor Resartus'' and [[James Joyce|James Joyce's]] 1939 novel ''[[Finnegans Wake]]'' are so thematically similar, ''Sartor Resartus'' seems to have influenced Joyce's much later novel.<ref>{{cite book|last=Bloom|first=Harold|title=The Anatomy of Influence: Literature as a Way of life|publisher=Yale University Press|location=New Haven, Conn.|isbn=0300181442|pages=112}}</ref>

According to Rodger L. Tarr, "The influence of ''Sartor Resartus'' upon American Literature is so vast, so pervasive, that it is difficult to overstate." Upon learning of Carlyle's death in 1881 [[Walt Whitman]] remarked: 'The way to test how much he has left us all were to consider, or try to consider, for the moment the array of British thought, the resultant and ensemble of the last fifty years, as existing to-day, but with Carlyle left out.  It would be like an army with no artillery.'"<ref name=Carlylexxxiii>{{cite book
  | last = Carlyle
  | first = Thomas; Introduction and Notes by Rodger L. Tarr
  | title = Sartor Resartus
  | publisher = University of California Press
  | location = Berkeley
  | year = 2000
  | page = xxxiii}}</ref>  Tarr suggests the influence of ''Sartor Resartus'' on American writers including  [[Ralph Waldo Emerson]], [[Emily Dickinson]], [[Henry David Thoreau]], [[Herman Melville]], [[Margaret Fuller]], [[Louisa May Alcott]] and [[Mark Twain]].  Both [[Nathaniel Hawthorne]] and [[Edgar Allan Poe]], however, read and objected to the book.<ref name=Carlylexxxii-xxxiii>{{cite book
  | last = Carlyle
  | first = Thomas; Introduction and Notes by Rodger L. Tarr
  | title = Sartor Resartus
  | publisher = University of California Press
  | location = Berkeley
  | year = 2000
  | page = xxxii-xxxiii}}</ref>

[[Jorge Luis Borges|Borges]] greatly admired the book, recounting that in 1916 at age 17  "[I] discovered, and was overwhelmed by, Thomas Carlyle. I read ''Sartor Resartus'', and I can recall many of its pages; I know them by heart."<ref>Jorge Luis Borges, ''This Craft of Verse'', [[Harvard University Press]], 2000. pp. 104. ''Word Music and Translation'', Lecture, Delivered February 28, 1968.</ref> Many of Borges' first characteristic and most admired works employ the same technique of intentional [[pseudepigraphy]] as Carlyle,<ref>{{cite journal|last=Toibin|first=Colm|title=Don't Abandon Me|journal=[[London Review of Books]]|date=May 11, 2006|volume=28|issue=9|pages=19–26|url=http://www.lrb.co.uk/v28/n09/colm-toibin/dont-abandon-me|accessdate=23 March 2013}}</ref>{{failed verification|date=March 2016}} such as "[[Pierre Menard, Author of the Quixote]]", "[[The Garden of Forking Paths]]" and "[[Tlön, Uqbar, Orbis Tertius]]".

==Notes==
{{reflist|30em}}

==References==
* Adams, Henry (1918). [https://archive.org/stream/educationofhenry00adamuoft#page/402/mode/2up "Chapter XXVII Teufelsdröckh 1901."] In: ''The Education of Henry Adams''. Boston and New York: Houghton Mifflin Company, pp. 403–415.
*{{cite book|ref=harv |last=Carlyle |first=Thomas |title=Sartor Resartus |year=1896 |publisher=Ginn and Company |location=Boston, Mass. |pages=xx |editor-first=Archibald |editor-last=Macmechan |chapter=Introduction}}
*{{cite book|ref=harv |last=Carlyle |first=Thomas |title=Sartor Resartus |year=1896n |publisher=Ginn and Company |location=Boston, Mass. |pages=xx |editor-first=Archibald |editor-last=Macmechan|chapter=Notes}}
* Dibble, Jerry A. (1978). ''The Pythia's Drunken Song: Thomas Carlyle's Sartor Resartus and the Style Problem in German Idealist Philosophy''. The Hague: Martinus Nijhoff.

==Further reading==
* Baker, Lee C. R. (1986). "The Open Secret of 'Sartor Resartus': Carlyle's Method of Converting His Reader," ''Studies in Philology'', Vol. 83, No. 2, pp.&nbsp;218–235.
* Barry, William Francis (1904). [http://babel.hathitrust.org/cgi/pt?id=uc2.ark:/13960/t7rn32p4z;view=1up;seq=86 "Carlyle."] In: ''Heralds of Revolt; Studies in Modern Literature and Dogma''. London: Hodder and Stoughton, pp.&nbsp;66–101.
* Deen, Leonard W. (1963). "Irrational Form in Sartor Resartus," ''Texas Studies in Literature and Language'', Vol. 5, No. 3, pp.&nbsp;438–451.
* Lamb, John B. (2010). "'Spiritual Enfranchisement': Sartor Resartus and the Politics of Bildung," ''Studies in Philology'', Vol. 107, No. 2, pp.&nbsp;259–282.
* Levine, George (1964). "'Sartor Resartus' and the Balance of Fiction," ''Victorian Studies'', Vol. 8, No. 2, pp.&nbsp;131–160.
* Maulsby, David Lee (1899). [http://babel.hathitrust.org/cgi/pt?id=uc2.ark:/13960/t2p55pj2k;view=1up;seq=5 ''The Growth of Sartor Resartus'']. Malden, Mass.: Trustees of Tufts College.
* Metzger, Lore (1961). "Sartor Resartus: A Victorian Faust," ''Comparative Literature'', Vol. 13, No. 4, pp.&nbsp;316–331.
* Moore, Carlisle (1955). "Sartor Resartus and the Problem of Carlyle's 'Conversion'," ''PMLA'', Vol. 70, No. 4, pp.&nbsp;662–681.
* Reed, Walter L. (1971). "The Pattern of Conversion in Sartor Resartus," ''ELH'', Vol. 38, No. 3, pp.&nbsp;411–431.

==External links==
{{Wikisource|Sartor Resartus}}
{{Gutenberg|no=1051|name=Sartor Resartus: the life and opinions of Herr Teufelsdrocke ++}}
*{{Cite Americana|wstitle=Sartor Resartus|year=1920|author=[[Archibald MacMechan]] |short=x}}
*{{Cite Nuttall|title=Sartor Resartus}} In addition to the article on ''Sartor Resartus'', there are many themes and ideas from ''Sartor Resartus'' cited in this work, of which the citations in the article above are only a small sample.

{{Thomas Carlyle}}

{{Authority control}}

[[Category:Existentialist novels]]
[[Category:1833 novels]]
[[Category:Works by Thomas Carlyle]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Fraser's Magazine]]
[[Category:Books with atheism-related themes]]
[[Category:Transcendentalism]]