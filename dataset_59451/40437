{{Orphan|date=October 2014}}

{{Infobox organization
| name = Furniture Bank
| logo =
| tax_id = 
| founded_date = 1998 ([[Toronto]], [[Ontario]], [[Canada]])
| founder = Sister Anne Schenck
| headquarters = 
| location = 25 Connell Court<br />Unit 1<br />[[Toronto]], [[Ontario]], [[Canada]]<br />M8Z 1E8<ref>http://www.furniturebank.org/us/contact-us/</ref>
| origins =
| key_people = 
| area_served = [[Greater Toronto Area]]
| products = 
| focus = Women and children, [[immigrant]]s, [[refugee]]s, [[homeless]]
| method =
| revenue = 
| endowment =
| num_employees =
| owner =
| Non-profit_slogan = "Furnished Homes Empowering Lives"
| homepage = {{URL|http://www.furniturebank.org}}
| type = [[Nonprofit organization|non-profit]], [[social enterprise]]
}}
'''Furniture Bank''' is a [[charitable organization]] and [[social enterprise]]<ref name=Reason>{{cite news|last1=Reason|first1=Cynthia|title=Furniture Bank offers families a fresh start|url=http://www.insidetoronto.com/news-story/1307773-furniture-bank-offers-families-a-fresh-start/|accessdate=9 October 2014|publisher=Etobicoke Guardian|date=26 July 2012}}</ref> that has been helping people in the [[Toronto|Greater Toronto Area]] establish their homes since 1998.<ref name=Benitah>{{cite news|last1=Benitah|first1=Sandie|title=Furniture Bank gives women in need a fresh start|url=http://www.cp24.com/furniture-bank-gives-women-in-need-a-fresh-start-1.778809|accessdate=9 October 2014|publisher=CP24|date=8 March 2012}}</ref><ref name="O'Connor">{{cite news|last1=O'Connor|first1=Jennifer|title=Furniture heaven for those in most need; Toronto charity offers clients donated tables, sofas and beds|url=http://search.proquest.com/docview/43909128|accessdate=9 October 2014|publisher=Toronto Star|date=25 November 2006}}</ref>  Furniture Bank accepts gently used furniture and household goods and redistributes them to people in marginalized communities.<ref name=Reason2>{{cite news|last1=Reason|first1=Cynthia|title=Furniture Bank's new location offers clients a true shopping experience|url=http://www.insidetoronto.com/news-story/2509990-furniture-bank-s-new-location-offers-clients-a-true-shopping-experience/|accessdate=9 October 2014|publisher=Etobicoke Guardian|date=20 March 2013}}</ref>  Donors can drop off furniture or use the fee-based pick-up service to make a contribution, and are offered an [[Gifts in kind|in kind]] charitable tax receipt for the value of the donated goods.<ref name=Wire>{{cite news|title=Press Release: Toronto Chair Affair Gala adds Fathead|url=http://www.wireservice.ca/index.php?module=News&func=display&sid=12380|accessdate=9 October 2014|publisher=Wire Service}}</ref><ref name=Lee>{{cite web|last1=Lee|first1=Amanda|title=There's good in the neighbourhood at the Furniture Bank in Etobicoke|url=http://eaudusoleil.empirecommunities.com/theres-good-in-the-neighbourhood-at-the-furniture-bank-in-etobicoke/|website=Eau Du Soleil|accessdate=9 October 2014}}</ref>

The organization’s focus is on helping new immigrants and refugees, abused women and children,<ref name=Benitah/> and the formerly homeless furnish their homes.<ref name=Reason/>  Furniture Bank works alongside more than 70 community agencies and shelters,<ref name="O'Connor"/> such as Streets to Homes,<ref name=Usher>{{cite news|last1=Usher|first1=Dianne|title=Furniture Bank offers a helping hand|url=http://www.torontorealestateboard.com/market_news/president_columns/pres_sun_col/2014/050214-furniture_bank.htm|accessdate=9 October 2014|agency=Toronto Real Estate Board|publisher=Toronto Sun|date=2 May 2014}}</ref> [[YMCA]] Shelters, and the [[Fred Victor Centre]],<ref name=Moorhouse>{{cite news|last1=Moorhouse|first1=Ellen|title=Do your old couch a favour|url=http://www.thestar.com/life/homes/2012/09/07/do_your_old_couch_a_favour.html|accessdate=9 October 2014|publisher=Toronto Star|date=7 September 2012}}</ref> that refer clients in need to access the services.

With 5 trucks on the road,<ref name=Wire/> Furniture Bank is able to provide furniture to more than 5,000 clients a year.<ref name=Moorhouse/><ref name=DeCarlo>{{cite web|last1=DeCarlo|first1=Sheri|title=Furniture Bank's Chair Affair helps empower families|url=http://www.torontonicity.com/2012/06/05/furniture-banks-chair-affair-helps-empower-families/|website=Torontonicity|accessdate=9 October 2014|date=5 June 2012}}</ref>  In 2012, the organization [[Landfill diversion|diverted]] 1,526 metric tons of furniture from ending up in a landfill.<ref name=Usher/>

Furniture Bank is a member of the Furniture Bank Association of North America.<ref name=Association>{{cite web|title=List of Furniture Banks|url=http://www.furniturebanks.org/list-of-furniture-banks/|website=Furniture Bank Association of North America|accessdate=9 October 2014}}</ref>
 
==History==

Sister Anne Schenck, one of the [[Sisters of St. Joseph#Toronto|Sisters of St. Joseph]] and a retired teacher and principal, established Furniture Bank in Toronto in 1998 and continues to be involved with the Board.<ref name=Benitah/><ref name=Usher/>  With help from [[Jack Layton]], she was able to secure rent-free space from the City of Toronto and established 200 Madison Avenue as Furniture Bank’s first home.<ref name=Reason/>  The organization based operations out of this warehouse until 2008 when the building was to be converted into housing, forcing Furniture Bank to move.<ref name=CBC>{{cite news|title=Furniture Bank needs to find new home|url=http://www.cbc.ca/news/canada/toronto/furniture-bank-needs-to-find-new-home-1.664067|accessdate=9 October 2014|publisher=CBC News|date=28 December 2007}}</ref>

Furniture Bank has relocated a few times, but now has a permanent home in the west end of Toronto.  The move to [[Etobicoke]] allowed the organization to meet the growing demands of the operation and transform the warehouse into a true showroom.<ref name=Lee/>  Furniture Bank has been located at 25 Connell Court since 2012.<ref name=Reason2/>

As of May 2014, Furniture Bank has served over 60,000 people in need.<ref name=Usher/>

==Programs==

===Furniture Link===

Furniture Link functions as the logistics arm of Furniture Bank by providing pick-up and delivery services to donors and clients of the organization.<ref name=Benitah/><ref name=TEF>{{cite web|title=Featured Enterprise|url=http://www.torontoenterprisefund.ca/component/k2/item/665-featured-enterprise|website=Toronto Enterprise Fund|accessdate=9 October 2014}}</ref>  This [[social enterprise]] is run much like a private company, but is driven by a social mission.  Furniture Link is supported through grants from the [[United Way of Canada|United Way's]] Toronto Enterprise Fund and the [[Ontario Trillium Foundation]].<ref name="O'Connor"/><ref name=Keung>{{cite news|last1=Keung|first1=Nicholas|title=Fund boosts unique Furniture Bank|url=http://www.thestar.com/news/gta/unitedway/2010/10/01/fund_boosts_unique_furniture_bank.html#|accessdate=9 October 2014|publisher=Toronto Star|date=1 October 2010}}</ref>  Revenue from operations are re-invested in the organization to provide funds for staffing and programming with a goal to subsidize operations and make the charity self-sustaining.<ref name=OTF>{{cite web|title=Toronto Grants Awarded |url=http://grant.otf.ca/grantlistings.aspx?year=2014&catchment=Toronto&lang=en&id=18-2014 |website=Ontario Trillium Foundation |accessdate=9 October 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140730011243/http://grant.otf.ca:80/grantlistings.aspx?year=2014&catchment=Toronto&lang=en&id=18-2014 |archivedate=30 July 2014 |df= }}</ref><ref name=Ferenc>{{cite news|last1=Ferenc|first1=Leslie|title=Stepping stones to a better life|url=http://search.proquest.com/docview/438895274|accessdate=9 October 2014|publisher=Toronto Star|date=3 October 2005}}</ref>

===Leg Up===

Furniture Bank operates an employment program called Leg Up, which provides individuals facing barriers to employment with training and work opportunities.<ref name=Lee/><ref name=UWT>{{cite web|title=Because of you, Matthew is back on his feet|url=http://www.unitedwaytoronto.com/matthew#.VDag2GddUla|website=United Way Toronto|accessdate=9 October 2014}}</ref>  The job skills program has a focus on new immigrants and at-risk youth,<ref name=Usher/> with participants working a variety of different paid placements throughout Furniture Bank’s operations.<ref name=Reason2/>

==Events==

===Chair Affair===

The signature fundraising event at Furniture Bank is the Chair Affair.<ref name=Keung/><ref name=Reason3>{{cite news|last1=Reason|first1=Cynthia|title=Designers bring creative talents to Chair Affair|url=http://www.insidetoronto.com/news-story/1311816-designers-bring-creative-talents-to-chair-affair/|accessdate=9 October 2014|publisher=Etobicoke Guardian|date=4 October 2012}}</ref>  The annual event consists of a live and silent auction on chairs selected from the Furniture Bank warehouse and decorated by Toronto designers.<ref name=Wire2>{{cite web|title=Press Release: Fashion meets compassion as the Chair Affair from Furniture Bank returns to Toronto this fall|url=http://www.wireservice.ca/index.php?module=News&func=display&sid=12315|website=Wire Service|accessdate=9 October 2014}}</ref><ref name=Marc>{{cite web|title=Marc Attends Chair Affair 2012 |url=http://marcatiyolil.com/marc-attends-chair-affair-2012/ |website=Marc & Mandy |accessdate=9 October 2014 |date=22 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20141030234115/http://marcatiyolil.com/marc-attends-chair-affair-2012/ |archivedate=30 October 2014 |df= }}</ref>  Previous designers include [[Steven Sabados]] and [[Chris Hyndman]] of [[Steven and Chris]],<ref name=DeCarlo/> and Heidi Richter and [[Paul Lafrance]] of [[Decked Out]].<ref name=Reason3/>  [[Jack Layton]],<ref name=DeCarlo/> Susan Hay,<ref name=Reason4>{{cite news|last1=Reason|first1=Cynthia|title=Chair Affair raises almost $100,000 for the Furniture Bank|url=http://www.insidetoronto.com/news-story/1376641-chair-affair-raises-almost-100-000-for-the-furniture-bank/|accessdate=9 October 2014|publisher=Etobicoke Guardian|date=4 December 2012}}</ref> and [[Olivia Chow]]<ref name=Marc/> have acted as hosts or auctioneers in the past.

==Volunteering==

Volunteers play a big role in the organization, helping to move furniture and assist clients in selecting items.  In addition to having regular volunteers, Furniture Bank often hosts corporate groups who volunteer for the day.<ref name=Moorhouse/><ref name=NewsWire>{{cite news|title=PricewaterhouseCoopers CEO and staff volunteer at Furniture Bank|url=http://search.proquest.com/docview/455579621|accessdate=9 October 2014|publisher=Canada NewsWire|date=15 September 2005}}</ref>

==See also==
*[[Furniture]]
*[[Social Enterprise]]
*[[Homelessness in Canada]]
*[[Poverty in Canada]]
*[[Charitable Organization]]
*[[Landfill diversion|Waste Diversion]]
*[[Gifts in kind]]

==References==
{{reflist}}

==External links==
*[http://www.furniturebank.org/ Furniture Bank website]

[[Category:1998 establishments in Ontario]]
[[Category:Charities based in Canada]]
[[Category:Organizations based in Toronto]]
[[Category:Etobicoke]]