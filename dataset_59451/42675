{|{{Infobox Aircraft Begin
  |name = DA20/DV20 Katana
  |logo = 
  |image = Flying Colors Precision Flight Team.jpg
  |caption = Flying Colors Precision Flight Team (DA20-A1-K100 Katana)
}}{{Infobox Aircraft Type
  |type = Two-seat light aircraft
  |manufacturer = [[Diamond Aircraft Industries|Diamond Aircraft]]
  |national origin = Austria (DV20)<br>Canada (DA20)
  |designer = 
  |first flight = 1991
  |introduction = 1992
  |retired = 
  |status = 
  |produced = 1994–present
  |number built = 
  |program cost= 
  |unit cost = $184,980 (2009 base price)<ref name = "DiamondDA20">{{cite web | url = http://www.diamondaircraft.com/build/index.php?id=20 |title = Diamond DA20C1|accessdate = 2009-09-17|last = [[Diamond Aircraft]]| year = }}</ref>
  |developed from = [[Diamond HK36 Super Dimona]]
  |variants with their own articles = 
  |developed into = [[Diamond DA40]]}}
|}
[[File:Diamond DA20 on the Ramp.jpg|thumb|right|1999 model DA20-C1 Eclipse]]
[[File:DiamondDA 20-A1KatanaC-FWNT01.jpg|thumb|right|DA20-A1 Katana]]
[[File:DiamondDA20C1InstrumnetPanel.jpg|thumb|right|DA20-C1 Eclipse instrument panel]]
[[File:Diamond-Katana-DA20-Wankel.jpg|thumb|Diamond DA20 with Diamond Engines Wankel]]
The '''Diamond DV20/DA20 Katana''' is an Austrian-designed two-seat [[tricycle gear]] [[general aviation]] light aircraft. The design was later built in Canada as the DA20 for the North American market.

==Development==
The two-seat DV20 Katana was based on the [[Diamond HK36 Super Dimona|Super Dimona]] with reduced wingspan. It was produced in Austria by HOAC (later changed its name to Diamond Aircraft). The Katana first flew in 1991 and gained certification in 1993.<ref name="flight22">{{cite journal|journal=[[Flight International]]|title=Katana gains Austrian approval|date=5 May 1993|page=22|url=https://www.flightglobal.com/pdfarchive/view/1993/1993%20-%201029.html}}</ref>  The Katana was first shown in public at the [[Paris Air Show|1993 Paris Air Show]].<ref name="canada">{{cite journal|journal=[[Flight International]]|title=Austria's HOAC plans Canadian site|date=14 July 1993|page=178|url=https://www.flightglobal.com/pdfarchive/view/1993/1993%20-%201710.html}}</ref>

The company built a factory in Canada to produce the DV20 which was designated the DA20. The first DA20 was the [[Rotax 912]] powered A1 [[Katana]] produced in [[Canada]] in 1994. It was the first Diamond aircraft available for sale in North America. Production of the Continental IO-240-B3B powered C1 Evolution and Eclipse models began in 1998, also in Canada. Production of the A1 Katana is complete but the DA20-C1 is still being constructed in 2010.<ref>{{cite web|url=http://www.diamondaircraft.com/|title=Diamond Aircraft|work=diamondaircraft.com|accessdate=30 January 2017}}</ref>

The DA20-A1 and C1 are both [[Type certificate|certified]] under [[Canadian Aviation Regulations|CAR 523]]<ref>[http://www.tc.gc.ca/aviation/applications/nico-celn/en/detail.asp?x_lang=e&aprv_num=A-191&aprv_type=TA&isu_dt_start=2005%2D08%2D23+00%3A00%3A00&id_num=1218 Transport Canada NICO website accessed 15 September 2007]</ref> in Canada and under [[Federal Aviation Regulations|FAR 23]] in the USA. The DA20 is certified in the utility category, and it is permissible to intentionally [[spin (flight)|spin]] it with flaps in the full up position.<ref>{{cite book | title = DA 20 Flight Information Manual (DA202/11 FIM) | date = 1997-02-12 | pages = 2–7 | chapter = Operating Limitations}}</ref> In 2004, Diamond received [[China|Chinese]] certification for the DA20. Both models also hold [[Joint Aviation Authorities|JAA]] certification.<ref name="DiamondPressRelease02Dec04">{{cite web|url = http://www.diamondaircraft.com/news/news-article.php?id=84&search=|title = Beijing PanAm starts commercial flight training with all glass DA40 Diamond Stars|accessdate = 3 October 2010|last = Diamond Aircraft|authorlink = |date=December 2004}}</ref>

Although the DA20 is available with instrumentation and [[avionics]] suitable for flight under [[instrument flight rules]] (IFR), its plastic airframe lacks lightning protection and thus does not qualify for IFR certification.<ref name="DiamondAirDA20">{{cite web|url = http://www.diamondaircraft.com/|title = DA20-C1 Multi-mission Machine|accessdate = 2008-05-04|last = [[Diamond Aircraft]]|authorlink = |year =}}</ref>

==Design==

The Katana is a low-wing cantilever monoplane which features control sticks (as opposed to yokes), composite construction, a [[Aircraft canopy|bubble canopy]], low-mounted wing, a single fuel tank, a [[T-tail]], and a castering nosewheel. All models have [[Composite material|composite]] airframes constructed of [[Glass-reinforced plastic|glass-]] and [[carbon-fiber reinforced plastic]].<ref name="DiamondAirDA20"/> The nose wheel of the DA20 is not linked to the rudder pedals and turns while taxiing are made with differential braking, with rudder steering becoming more effective as airspeed increases.<ref>{{cite book | title = DA 20-A1 Flight Information Manual (DA202/11 FIM) | date = 1997-02-12 | pages = 4–13 | chapter = Normal Operating Procedures}}</ref> The Rotax engine variants are approved for use with automotive gasoline.<ref>{{Cite book|url=http://www.diamondaircraft.com/wp-content/uploads/2016/01/DA202-100-Rev-6.pdf|title=DA-20-A1-100 Flight Manual|last=|first=|publisher=Diamond Aircraft|year=2000|isbn=|location=|pages=1–6}}</ref>

The Katana possesses a higher [[glide ratio]] than many of its competitors. The glide ratio of the DA20-C1 is 11:1 and the DA20-A1 is 14:1.<ref>{{cite book | title = DA 20-A1 Flight Information Manual (DA202/11 FIM) | date = 1997-02-12 | pages = 3–13 | chapter = Emergency Procedures}}</ref>

In November 2008 the company announced that it would be offering an [[Aspen Avionics]] [[glass cockpit]] [[primary flight display]] as an option on the DA20. Diamond indicated the Aspen PFD was easy to incorporate into the existing [[Flight instruments|instrument panel]] design because it mounts in a standard round instrument hole. In October 2009 the company introduced the Garmin G500 glass cockpit as an option.<ref name="AvWeb08Nov08">{{cite web|url = http://www.avweb.com/news/aopa/AOPAExpo2008_DA20WithAspenGlass_199156-1.html|title = DA20 With Aspen Glass|accessdate = 2008-11-08|last = Niles|first = Russ|authorlink = |date=November 2008}}</ref>

==Operational history==

===United States===
[[Embry-Riddle Aeronautical University]] provided the Academy Flight Screening (AFS) program for the [[United States Air Force Academy]] in DA20-C1 Falcons, which were specially ordered with slightly smaller fuel tanks to save weight and primary flight instruments on the right side of the aircraft.  Embry-Riddle operated a fleet of DA-20s at the Academy.<ref>{{cite web|url=http://www.erau.edu/er/newsmedia/newsreleases/2002/airforce.html |title=Embry-Riddle Wins Contract to Train Air Force Pilots |accessdate=2008-12-16 |deadurl=yes |archiveurl=https://web.archive.org/web/20081204110844/http://erau.edu/er/newsmedia/newsreleases/2002/airforce.html |archivedate=December 4, 2008 }}</ref> The AFS program was discontinued in 2007.

Doss Aviation, under contract for the [[United States Air Force]], currently operates a fleet of DA-20s at the [[Pueblo Memorial Airport]] in the [[Initial Flight Training]] (IFT) program.<ref>{{cite web |url=http://www.dossifs.com/index.php?option=com_content&view=article&id=64&Itemid=83 |title=Doss Initital Flight Screening (Aircraft) |accessdate=16 October 2010}}</ref> Potential USAF Pilots, Combat Systems Officers, and RPA pilots that do not have a private pilot's license must go through the Doss screening program and receive time in the DA20 before going on to their respective training programs.<ref>[http://www.dossifs.com/ Welcome to Doss IFS!] Retrieved 8 January 2012</ref>

==Variants==
[[File:G-BXPC-Katana-2.jpg|thumb|right|A DA-20A-1 in 2002]]
<div id="1"></div>
;DV20-A1 Katana
:Austrian-built development of the [[Diamond HK36 Super Dimona]] motorglider, powered by a {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912]] and certified in 1993<ref name="DiamondHist">{{cite web|url = http://www.diamondaircraft.com/why/index.php?id=1|title = DIAMOND AIRCRAFT. A History...|accessdate = 2009-04-14|last = [[Diamond Aircraft]]|authorlink = |year = 2009}}</ref>
<div id="2"></div>
;DA20-A1 Katana
:Canadian-built DV20. Powered by a {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912]] and introduced in 1995<ref name="DiamondHist" />
<div id="3"></div>
;DA20-100 Katana 100
:Factory refurbished and re-engined Katana for the European market. Powered by a {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912]]S. Introduced in 1999.<ref name="DiamondHist" />
<div id="4"></div>
;DA20-C1 Katana and Katana Eclipse
:Name used in marketing and some 1998 year model planes. The name "Katana" was actually painted on some planes. Powered by a {{convert|125|hp|kW|0|abbr=on}} [[Continental IO-240]] engine. In order to accommodate the extra 70 pounds of the IO-240, the Katana's battery was moved behind the baggage bay, to help move the empty cg aft, and the wing sweep has been changed from 1 degree aft to just 0.5 degrees back to shift the center of lift forward. Previous Katanas had simple hinged flaps — but at the higher maximum weight, more sophisticated slotted flaps were necessary to bring the stall speed to the JAR-VLA-specified 45 knots<ref name="AOPAMarsh">{{cite web|url = http://flighttraining.aopa.org/learntofly/articles/diamond0308.cfm |title = Diamond Eclipse - Delivery for the Air Force|accessdate = 2009-04-14|last = Marsh|first = Alton K.|authorlink = |year = |archiveurl = https://web.archive.org/web/20080621062759/http://flighttraining.aopa.org/learntofly/articles/diamond0308.cfm |archivedate = 2008-06-21}}</ref>
<div id="5"></div>
;DA20-C1 Evolution
:Stripped down C1, intended for flight schools as a trainer. No rear windows. Powered by a {{convert|125|hp|kW|0|abbr=on}} [[Continental IO-240]]-B engine<ref name="DiamondHist" /><ref name="AOPAMarsh" />
<div id="6"></div>
;DA20-C1 Eclipse
:Better equipped C1 for private use, with rear windows for better visibility. Powered by a {{convert|125|hp|kW|0|abbr=on}} [[Continental IO-240]]-B engine Entered production in 1999.<ref name="DiamondHist" /><ref name="AOPAMarsh" />
<div id="7"></div>
;DA20-C1 Falcon
:Military trainer version. Powered by a {{convert|125|hp|kW|0|abbr=on}} [[Continental IO-240]]-B engine. Instruments moved in front of the right seat, where the student sits. This puts the stick in the student's right hand and throttle in the left, similar to fighter aircraft. Also equipped with a smaller fuel tank. Some Embry-Riddle Falcons have been sold to private owners and flight schools after being fitted with standard instrument panels and fuel tanks.<ref name="AOPAMarsh" />

==Operators==

===Civil===
The DA20 is popular with [[flight training]] schools and is also operated by private individuals. [[Moncton Flight College]] in [[New Brunswick]], [[Canada]] operates a fleet of 35 Diamond DA20-C1 Eclipses.<ref>{{cite web|url=http://www.mfc.nb.ca/fleet.html|title=Fleet:|work=mfc.nb.ca|accessdate=30 January 2017}}</ref>

===Military===
;{{ECU}}
*[[Ecuadorian Air Force]] - 20 X DA20C1s (for delivery in 2012)<ref name="DiamondPressRel06Mar12">{{cite web|url = http://www.diamondaircraft.com/news/news-article.php?id=133|title = Ecuador Air Force accepts delivery of Diamond DA20 fleet|accessdate = 6 March 2012|last = [[Diamond Aircraft]]|date = 6 March 2012}}</ref>
;{{POL}}
*[[Polish Air Force]] - 5 X DA20 C-1 Eclipse<ref>{{cite web|url=http://lotniczapolska.pl/Diamondy-dla-Deblina,28946 |title=Diamondy dla Dęblina - Lotnicza Polska |publisher=Lotniczapolska.pl |date= |accessdate=2013-06-23}}</ref>

==Specifications (DA20-C1)==
{{Aircraft specs
|ref=DA20 webpage<ref>{{cite web
 |url=http://www.diamondaircraft.com/aircraft/da20/
 |title=Diamond Aircraft DA20 webpage
 |last=[[Diamond Aircraft]]
 |first=
 |date=
 |accessdate=4 December 2014
 |work=
 |publisher=
}}</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=
|length ft=23
|length in=6
|length note=
|span m=
|span ft=35
|span in=8
|span note=
|height m=
|height ft=7
|height in=2
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=1164
|empty weight note=
|gross weight kg=
|gross weight lb=1764
|gross weight note=
|fuel capacity={{convert|24|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental O-240|Continental IO-240-B3B]]
|eng1 type=four cylinder [[horizontally opposed]] piston [[aircraft engine]]
|eng1 kw=
|eng1 hp=125

|prop blade number=2
|prop name=[[Sensenich Propeller]] fixed pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=138
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=45
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=547
|range note=with 30 minute reserve
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=13120
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1000
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=*Garmin GNC 420W [[GPS]]/[[Airband|COM]]
*Garmin GTX 327 [[Transponder (aeronautics)|transponder]] with blind altitude encoder
*PS Engineering PM 1000 Intercom
}}

==See also==
{{aircontent
|related=
|similar aircraft=
|lists=
* [[List of military aircraft of the United States]]
|see also=
}}

==References==
{{reflist |32em}}

==External links==
{{Commons category|Diamond DA-20}}
* [http://www.diamondair.com/ Diamond Aircraft Industries]
* {{Citation | url = http://philip.greenspun.com/flying/diamond-katana | title = Diamond Katana | type = review | first = Philip | last = Greenspun}}.
* {{Citation | url = http://www.diamondaircraft.com/library/16/10/DA20-A1-AFM-inc-Rev17.pdf | title = DA-20-A1 Katana | type = flight manual | format = [[Portable document format|PDF]] | publisher = Diamond aircraft}}.
* [http://www.diamondaircraft.com/aircraft/da20/technical.php#cat-16 DA20-C1 Eclipse Flight Manual]
* [http://www.avweb.com/news/features/Used-Aircraft-Guide-Diamond-DA20222630-1.html DA20 detailed review on AVweb]

{{Diamond Aircraft}}

{{DEFAULTSORT:Diamond Da20}}
[[Category:Austrian civil trainer aircraft 1990–1999]]
[[Category:Austrian civil utility aircraft 1990–1999]]
[[Category:Canadian civil trainer aircraft 1990–1999]]
[[Category:Canadian civil utility aircraft 1990–1999]]
[[Category:Diamond aircraft|DA20]]
[[Category:Wankel-engined aircraft]]
[[Category:T-tail aircraft]]