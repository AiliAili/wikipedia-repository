'''Families Anonymous''' ('''FA''') is a [[twelve-step program]] for relatives and friends of [[wiktionary:addict|addicts]].<ref name="CASA2005">{{cite book |title=Family Matters: Substance Abuse and The American Family |chapter=Chapter V. Where to Turn for Help |publisher=The National Center on Addiction and Substance Abuse at Columbia University |date=March 2005 |pages=31–43 |url=http://www.casacolumbia.org/absolutenm/articlefiles/380-Family%20Matters.pdf |accessdate=2009-03-16}}</ref> FA was founded in 1971 by a group of parents in [[Southern California]] concerned with their children's [[substance abuse]].<ref name="HEALTHFINDER">{{cite web|title=Families Anonymous, Inc. - FA |publisher=U.S. Department of Health and Human Services |date=2006-07-06 |accessdate=2009-03-16 |url=http://www.healthfinder.gov/orgs/hr1019.htm |archivedate=2009-03-17 |archiveurl=http://www.webcitation.org/5fKYr6n4h?url=http://www.healthfinder.gov/orgs/hr1019.htm |deadurl=yes |df= }}</ref> As of 2007 there are FA meetings in more than 20 countries and about 225 regular meetings in the United States.<ref name="CASA2005"/><ref name="GLASER2007">{{cite news|last=Glaser |first=Susan |publisher=Plain Dealer |date=2007-05-24 |location=Cleveland, OH |title=Families help each other cope with drug-addicted children |archiveurl=http://www.webcitation.org/5fKafPEE7?url=http://blog.cleveland.com/earlyedition/2007/05/artslife_families_help_each_ot.html |url=http://blog.cleveland.com/earlyedition/2007/05/artslife_families_help_each_ot.html |archivedate=2009-03-17 |accessdate=2009-03-16 |deadurl=yes |df= }}</ref> A survey of FA groups in [[Lisbon, Portugal]] found members were mostly female, 45-60 years old, and mothers of substance abusing children.<ref name="FROIS2007">{{cite journal |last=Fróis |first=Catarina O |title=A reinvenção do eu através do discurso: narrativa, estigma e anonimato nas Famílias Anônimas |journal=Mana |volume=13 |issue=1 |date=April 2007 |pages=63–84 |doi=10.1590/S0104-93132007000100003 |language=Portuguese |last2=Rodgers |first2=David Allan}}</ref>

The focus of FA is on supporting members rather than changing the behavior of their friend or relative with a substance abuse problem.<ref name="HEALTHFINDER"/> [[Tough love]] is suggested as an approach to use when dealing with addicts—members do not need to rescue addicts from the consequences of problems the addicts have created, and members should be willing to offend addicts if necessary.<ref name="LAWTON1982">{{cite journal |title=Comment: Group Psychotherapy with Alcoholics: Special Techniques |journal=Journal of Studies on Alcohol |volume=43 |issue=11 |year=1982 |url=http://www.jsad.com/jsad/article/Group_Psychotherapy_with_Alcoholics_Special_Techniques/3214.html |first=Marcia J. |last=Lawton |issn=0096-882X |oclc=1261091 |pages=1276–1278 |pmid=7182687}}</ref> One study suggested the therapeutic effects of participation included a process of [[internalization]] from the stories and information shared, [[Rationalization_(psychology)|rationalization]] and freeing from [[guilt (emotion)|guilt]] regarding the behavior of the abuser, and [[Twelve Traditions|The Traditions]] protecting anonymity which allow members to reduce potential stigma acquired from membership.<ref name="FROIS2007"/>

FA's original literature includes the book ''Today a Better Way''<ref name="TODAY">{{cite book |title=Today a Better Way |location=Van Nuys, CA  |publisher=Families Anonymous |author=Families Anonymous |oclc=42889952 |year=1991}}</ref> on the principles of the FA program, a newsletter, the ''Twelve Step Rag'', as well as several pamphlets and booklets.<ref name="NHIC2006">{{cite web|title=Families Anonymous, Inc. |publisher=National Health Information center: Health Information Resource Database |date=2006-06-19 |accessdate=2009-03-16 |url=http://www.health.gov/nhic/nhicscripts/Entry.cfm?HRCode=HR1019 |archivedate=2009-01-14 |archiveurl=https://web.archive.org/web/20090114223939/http://www.health.gov/nhic/NHICScripts/Entry.cfm?HRCode=HR1019 |deadurl=yes |df= }}</ref>

==See also==
*[[Al-Anon/Alateen]]
*[[Co-Dependents Anonymous]]
*[[List of twelve-step groups]]
*[[Nar-Anon]]

==References==
{{reflist|2}}

== External links ==
* [http://www.familiasanonimas-pt.org Families Anonymous Portugal]
* [http://www.famanon.org.uk/ Families Anonymous UK] 
* [http://www.familiesanonymous.org/ Families Anonymous USA]
* {{worldcat id|id=nc-families+anonymous}} and {{worldcat id|id=nc-families+anonymous+inc|name=Families Anonymous, Inc.}}

[[Category:Addiction and substance abuse organizations]]
[[Category:Twelve-step programs]]
[[Category:Organizations established in 1971]]
[[Category:Non-profit organizations based in California]]