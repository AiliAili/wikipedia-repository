{{stack begin}}
{{Taxobox
| image = Single lycoperdon perlatum.jpg
| image_width = 234px
| image_caption = 
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Agaricaceae]]
| genus = ''[[Lycoperdon]]''
| species = '''''L. perlatum'''''
| binomial = ''Lycoperdon perlatum''
| binomial_authority = [[Pers.]] (1796)
| synonyms_ref = <ref name="urlFungorum synonymy: Lycoperdon perlatum"/><ref name="urlMycoBank: Lycoperdon perlatum"/>
| synonyms =
*''Lycoperdon gemmatum'' <small>[[August Johann Georg Karl Batsch|Batsch]] (1783)</small>
*''Lycoperdon gemmatum'' var. ''perlatum'' <small>(Pers.) [[Elias Magnus Fries|Fr.]] (1829)</small>
*''Lycoperdon bonordenii'' <small>[[George Edward Massee|Massee]] (1887)</small>
*''Lycoperdon perlatum'' var. ''bonordenii'' <small>(Massee) Perdeck (1950)</small>
}}
{{stack end}}
'''''Lycoperdon perlatum''''', popularly known as the '''common puffball''', '''warted puffball''', '''gem-studded puffball''', or the '''devil's snuff-box''', is a species of [[puffball]] fungus in the family [[Agaricaceae]]. A widespread species with a [[cosmopolitan distribution]], it is a medium-sized puffball with a round [[ascocarp (fungus)|fruit body]] tapering to a wide stalk, and dimensions of {{convert|1.5|to|6|cm|in|1|abbr=on}} wide by {{convert|3|to|7|cm|in|abbr=on}} tall. It is off-white with a top covered in short spiny bumps or "jewels", which are easily rubbed off to leave a netlike pattern on the surface. When mature it becomes brown, and a hole in the top opens to release [[spores]] in a burst when the body is compressed by touch or falling raindrops.

The puffball grows in fields, gardens, and along roadsides, as well as in grassy clearings in woods. It is [[edible mushroom|edible]] when young and the [[gleba|internal flesh]] is completely white, although care must be taken to avoid confusion with immature fruit bodies of [[poisonous mushroom|poisonous]] ''[[Amanita]]'' species. ''L.&nbsp;perlatum'' can usually be distinguished from other similar puffballs by differences in surface texture. Several chemical compounds have been isolated and identified from the fruit bodies of ''L.&nbsp;perlatum'', including [[sterol]] derivatives, [[volatility (chemistry)|volatile]] compounds that give the puffball its flavor and odor, and the unusual [[amino acid]] lycoperdic acid.  Laboratory tests indicate that [[extract]]s of the puffball have [[antimicrobial]] and [[Antifungal medication|antifungal]] activities.

==Taxonomy==
The species was first [[species description|described]] in the scientific literature in 1796 by mycologist [[Christiaan Hendrik Persoon]].<ref name="Persoon 1796"/> [[Synonym (biology)|synonyms]] include ''Lycoperdon gemmatum'' (as described by [[August Batsch]] in 1783<ref name="Batsch 1786"/>); the [[variety (botany)|variety]] ''Lycoperdon gemmatum'' var. ''perlatum'' (published by [[Elias Magnus Fries]] in 1829<ref name="Fries 1829"/>); ''Lycoperdon bonordenii'' ([[George Edward Massee]], 1887<ref name="Massee 1887"/>); and ''Lycoperdon perlatum'' var. ''bonordenii'' (A.C. Perdeck, 1950<ref name="Perdeck 1950"/>).<ref name="urlFungorum synonymy: Lycoperdon perlatum"/><ref name="urlMycoBank: Lycoperdon perlatum"/>

''L.&nbsp;perlatum'' is the [[type species]] of the genus ''Lycoperdon''. [[Molecular phylogenetics|Molecular]] analyses suggest a close [[phylogenetic]] relationship with ''[[Lycoperdon marginatum|L.&nbsp;marginatum]]''.<ref name="Larsson 2008"/>

The [[specific name (botany)|specific epithet]] ''perlatum'' is [[Latin]] for "widespread".<ref name="Schalkwijk-Barendsen 1991"/> It is [[common name|commonly]] known as the common puffball, the gem-studded puffball<ref name="Ammirati 1987"/> (or gemmed puffball<ref name="Arora 1986"/>), the warted puffball,<ref name="Schalkwijk-Barendsen 1991"/> or the devil's snuff-box;<ref name="Dickinson 1982"/> [[Samuel Frederick Gray]] called it the pearly puff-ball in his 1821 work ''A Natural Arrangement of British Plants''.<ref name="Gray 1821"/> Because some [[indigenous peoples]] believed that the spores caused blindness, the puffball has some local names such as "blindman's bellows" and "no-eyes".<ref name="Roberts 2011"/>

==Description==
{{multiple image
| align = left
| image1 = Lycoperdon perlatum 66070.jpg
| width1 = {{#expr: (200 * 500 / 750) round 0}}
| image2 = Lycoperdon perlatum 147481.jpg
| width2 = {{#expr: (200 * 2158 / 2526) round 0}}
| caption1 = The exoperidium is covered in spines and warts.
| caption2 = Fruit bodies are edible when the gleba is white and firm.
}}
The fruit body ranges in shape from pear-like with a flattened top, to nearly spherical, and reaches dimensions of {{convert|1.5|to|6|cm|in|1|abbr=on}} wide by {{convert|3|to|7|cm|in|abbr=on}} tall. It has a stem-like base. The outer surface of the fruit body (the [[peridium|exoperidium]]) is covered in short cone-shaped spines that are interspersed with granular warts. The spines, which are whitish, gray, or brown, can be easily rubbed off, and leave [[wikt:reticulate|reticulate]] pock marks or scars after they are removed.<ref name="Arora 1986"/> The base of the puffball is thick, and has internal chambers. It is initially white, but turns yellow, olive, or brownish in age.<ref name="Arora 1986"/> The reticulate pattern resulting from the rubbed-off spines is less evident on the base.<ref name="Pegler 1995"/>

In maturity, the exoperidium at the top of the puffball sloughs away, revealing a pre-formed hole ([[ostiole]]) in the endoperidium, through which the spores can escape.<ref name="Miller 1988"/> In young puffballs, the internal contents, the [[gleba]], is white and firm, but turns brown and powdery as the [[spore]]s mature.<ref name="Arora 1986"/>  The gleba contains minute chambers that are lined with [[hymenium]] (the fertile, spore-bearing tissue); the chambers collapse when the spores mature.<ref name="Miller 1988"/> Mature puffballs release their powdery spores through the ostiole when they are compressed by touch or falling raindrops. A study of the spore release mechanism in ''L.&nbsp;pyriforme'' using high-speed [[schlieren photography]] determined that raindrops of 1&nbsp;mm diameter or greater, including rain drips from nearby trees, were sufficient to cause spore discharge. The puffed spores are ejected from the ostiole at a velocity of about 100&nbsp;cm/second to form a centimeter-tall cloud one-hundredth of a second after impact. A single puff like this can release over a million spores.<ref name="Gregory 1947"/>

{{multiple image
| align = right
| image1 = Lycoperdon perlatum 64509.jpg
| width1 = {{#expr: (170 * 908 / 660) round 0}}
| image2 = Lycoperdon perlatum 74661.jpg
| width2 = {{#expr: (170 * 1024 / 795) round 0}}
| caption1 = Closeup of the ostiole. Note the pockmarks left behind from missing spines.
| caption2 = Spores are thick-walled and spherical, measuring roughly 4 &nbsp;μm in diameter.
}}
The spores are spherical, thick-walled, covered with minute spines, and measure 3.5–4.5&nbsp;[[micrometre|μm]] in diameter. The [[capillitia]] (threadlike filaments in the gleba in which spores are embedded) are yellow-brown to brownish in color, lack [[septum|septae]],<ref name="Miller 2006"/> and measure 3–7.5&nbsp;μm in diameter.<ref name="Pegler 1995"/> The [[basidium|basidia]] (spore-bearing cells) are club-shaped, four-spored, and measure 7–9 by 4–5&nbsp;μm. The basidia bear four slender [[sterigmata]] of unequal length ranging from 5–10&nbsp;μm long. The surface spines are made of chains of pseudoparenchymatous [[hypha]]e (resembling the [[parenchyma]]  of higher plants), in which the individual hyphal cells are spherical to elliptical in shape, thick-walled (up to 1&nbsp;μm), and measure 13–40 by 9–35&nbsp;μm.&nbsp;These hyphae do not have [[clamp connection]]s.<ref name="Natarajan 1987"/>

===Edibility===
{{nutritionalvalue
| name=''Lycoperdon perlatum'', dried<ref name="Colak 2009"/>
| kJ=1845.5
| protein=44.9 g
| fat=10.6 g
| carbs=42 g
| iron_mg=5.5
| copper_mg=0.5
| zinc_mg=0.5
| manganese_mg=0.6
| source_usda=1
}}
''Lycoperdon perlatum'' is considered to be a good [[edible mushroom]] when young, when the gleba is still homogeneous and white.  They have been referred to as "poor man's sweetbread" due to their texture and flavor. The fruit bodies can be eaten after slicing and frying in batter or egg and breadcrumbs,<ref name="Dickinson 1982"/> or used in soups as a substitute for [[dumpling]]s.<ref name="Kuo 2007"/> As early as 1861, Elias Fries recommended them dried and served with salt, pepper, and oil.<ref name="Laessoe 1994"/> The puffballs become inedible as they mature: the gleba becomes yellow-tinged then finally develops into a mass of powdery olive-green spores. ''L.&nbsp;perlatum'' is one of several edible species sold in markets in the Mexican states of [[Puebla]] and [[Tlaxcala]].<ref name="Lemin 2010"/><ref name="Montoya 2003"/> The fruit bodies are appealing to other animals as well: the [[northern flying squirrel]] (''Glaucomys sabrinus'') includes the puffball in their diet of non-[[truffle]] fungi,<ref name="Thysell 1997"/> while the "puffball beetle" ''[[Caenocara subglobosum]]'' uses the fruit body for shelter and breeding.<ref name="Suda 2009"/> [[Nutrition analysis|Nutritional analysis]] indicates that the puffballs are a good source of [[dietary protein|protein]], [[carbohydrate]]s, [[fat]]s, and several [[micronutrient]]s.<ref name="Colak 2009"/> The predominant [[fatty acid]]s in the puffball are [[linoleic acid]] (37% of the total fatty acids), [[oleic acid]] (24%), [[palmitic acid]] (14.5%), and [[stearic acid]] (6.4%).<ref name="Nedelcheva 2007"/>

The immature 'buttons' or 'eggs' of deadly ''[[Amanita]]'' species can be confused with puffballs. This can be avoided by slicing fruit bodies vertically and inspecting them for the internal developing structures of a mushroom. Additionally, Amanitas will generally not have "jewels" or a bumpy external surface.<ref name="Marley 2010"/>

The spores are ornamented with many sharp microscopic spines and can cause severe irritation of the lung ([[lycoperdonosis]]) when inhaled.<ref name="MMWR 1994"/><ref name="Strand 1967"/> This condition has been reported to afflict dogs that play or run where puffballs are present.<ref name="Rubensohn 2009"/><ref name="Buckeridge 2011"/>

==Similar species==
{{multiple image
| footer = ''Lycoperdon excipuliforme'' (left) and ''L.&nbsp;marginatum'' (right) are two of several lookalike puffball species.
| align = right
| image1 = Handkea excipuliformis (Scop.) Kreisel, 1989 (Pestle Puffball) (2) crop.jpg
| width1 = {{#expr: (150 * 1200 / 1256) round 0}}
| caption1 =
| image2 = 2011-07-16 Lycoperdon marginatum Vittad 200130.jpg
| width2 = {{#expr: (150 * 2271 / 3027) round 0}}
| caption2 = 
}}
There are several other puffball species with which ''L.&nbsp;perlatum'' might be confused. ''[[Lycoperdon nettyanum|L.&nbsp;nettyanum]]'', found in the [[Pacific Northwest]] region of the United States, is covered in granular patches, but these granules adhere more strongly to the surface than those of ''L.&nbsp;perlatum''.<ref name="Ramsey 1980"/> ''[[Lycoperdon pyriforme|L.&nbsp;pyriforme]]'' lacks prominent spines on the surface, and grows on rotting wood—although if growing on buried wood, it may appear to be terrestrial. The widely distributed and common ''[[Lycoperdon umbrinum|L.&nbsp;umbrinum]]'' has spines that do not leave scars when rubbed off, a gleba that varies in color from dark brown to purple-brown at maturity, and a purple-tinged base. The small and rare species ''[[Lycoperdon muscorum|L.&nbsp;muscorum]]'' grows in deep moss. ''[[Lycoperdon peckii|L.&nbsp;peckii]]'' can be distinguished from ''L.&nbsp;pyriforme'' by the lavender-tinged spines it has when young. ''[[Lycoperdon rimulatum|L.&nbsp;rimulatum]]'' has purplish spores, and an almost completely smooth exoperidium.<ref name="Arora 1986"/> ''[[Lycoperdon excipuliforme|L.&nbsp;excipuliforme]]'' is larger and grayer, and, in mature individuals, the upper portion of its fruit body breaks down completely to release its spores.<ref name="Roberts 2011"/>  In the field, ''[[Lycoperdon marginatum|L.&nbsp;marginatum]]'' is distinguished from ''L.&nbsp;perlatum'' by the way in which the spines are shed from the exoperidium in irregular sheets.<ref name="Davis 2012"/>

==Ecology and distribution==
[[File:Lycoperdon perlatum 95340.jpg|thumb|left|Fruit bodies may grow singly, scattered, in groups, or—as shown here—in clusters.]]
A [[saprobic]] species, ''Lycoperdon perlatum'' grows solitarily, scattered, or in groups or clusters on the ground. It can also grow in [[fairy ring]]s.<ref name="Dickinson 1982"/> Typical habitats include woods, grassy areas, and along roads.<ref name="Arora 1986"/> It has been reported from ''[[Pinus patula]]'' plantations in [[Tamil Nadu]], India.<ref name="Natarajan 1987"/> The puffball sometimes confuses golfers because of its resemblance to a golf ball when viewed from a distance.<ref name="Dickinson 1982"/>

A widespread species with an almost [[cosmopolitan distribution]],<ref name="Pegler 1995"/> it has been reported from Africa ([[Kenya]], [[Rwanda]],<ref name="Demoulin 1975"/> [[Tanzania]]<ref name="Maghembe 1984"/>), Asia (China,<ref name="Zhishu 1993"/> Himalayas,<ref name="Thind 1982"/> Japan,<ref name="Kasuya 2004"/> southern India<ref name="Natarajan 1987"/>), Australia,<ref name="Dickinson 1982"/> Europe,<ref name="Jordan 2004"/> New Zealand,<ref name="ChuChou 1983"/> and South America (Brazil).<ref name="Baseia 2005"/> It has been collected from [[subarctic]] areas of [[Greenland]], and [[subalpine zone|subalpine]] regions in [[Iceland]].<ref name="Jeppson 2006"/> In North America, where it is considered the most common puffball species, it ranges from [[Alaska]]<ref name="Orr 1979"/> to Mexico,<ref name="Moreno 2010"/> although it is less common in Central America.<ref name="Garner 1956"/> The species is popular on postage stamps, and has been depicted on stamps from Guinea, Paraguay, Romania, Sierra Leone, and Sweden.<ref name="Moss 1998"/>

The puffball [[bioaccumulation|bioaccumulates]] [[Heavy metal (chemistry)|heavy metal]]s present in the soil,<ref name="Ylmaz 2003"/><ref name="Falandysz 2003"/> and can be used as a [[bioindicator]] of [[soil pollution]] by heavy metals and [[selenium]].<ref name="Quinche 1990"/> In one 1977 study, samples collected from grassy areas near the side of an [[interstate highway]] in [[Connecticut]] were shown to have high concentrations of [[cadmium]] and [[lead]].<ref name="McCreight 2003"/> ''L.&nbsp;perlatum'' [[biomass]] has been shown experimentally to remove [[Mercury (element)|mercury]] ions from [[aqueous solution]]s, and is being investigated for potential use as a low-cost, [[renewable resource|renewable]], [[biosorption|biosorptive]] material in the [[Sewage treatment|treatment]] of water and [[wastewater]] containing mercury.<ref name="Sari 2012"/>

==Chemistry==
[[FIle:Lycoperdic acid.png|thumb|right|Lycoperdic acid is an amino acid known only from ''L.&nbsp;perlatum''.]]
Several [[steroid]] derivatives have been isolated and identified from fruit bodies of ''L.&nbsp;perlatum'', including (''S'')-23-hydroxylanostrol, [[ergosterol]] α-endoperoxide, ergosterol 9,11-dehydroendoperoxide and (23''E'')-lanosta-8,23-dien-3β,25-diol. The compounds [[3-Octanone|3-octanone]], [[1-octen-3-ol]], and (''Z'')-3-octen-1-ol are the predominant components of the [[Volatility (chemistry)|volatile]] chemicals that give the puffball its odor and flavor.<ref name="Szummy 2010"/> Extracts of the puffball contain relatively high levels of [[antimicrobial]] activity against the human [[pathogenic bacteria|pathogenic]] bacteria ''[[Bacillus subtilis]]'', ''[[Staphylococcus aureus]]'', ''[[Escherichia coli]]'', and ''[[Pseudomonas aeruginosa]]'', with an efficiency comparable to that of the antibiotic [[ampicillin]].<ref name="Ramesh 2010"/> These results corroborate an earlier study that additionally reported antibacterial activity against [[Salmonella enterica enterica|''Salmonella enterica'' serovar Typhimurium]], ''[[Streptococcus pyogenes]]'', and ''[[Mycobacterium smegmatis]]''.<ref name="Dulger 2005"/> Extracts of the puffball have also been reported to have [[Antifungal medication|antifungal]] activity against ''[[Candida albicans]]'', ''[[Candida tropicalis|C.&nbsp;tropicalis]]'', ''[[Aspergillus fumigatus]]'', ''[[Alternaria solani]]'', ''[[Botrytis cinerea]]'', and ''[[Verticillium dahliae]]''.<ref name="Pujol 1990"/> A 2009 study found ''L.&nbsp;perlatum'' puffballs to contain the [[natural phenol|phenolic]] compound [[cinnamic acid]] at a concentration of about 14&nbsp;milligrams per kilogram of mushroom.<ref name="Barros 2009"/> The fruit bodies contain the pigment [[melanin]].<ref name="Almendros 1987"/>

The [[amino acid]] lycoperdic acid (chemical name 3-(5(''S'')-carboxy-2-oxotetrahydrofuran-5(''S'')-yl)-2(''S'')-alanine) was isolated from the puffball, and reported in a 1978 publication.<ref name="Lamotte 1978"/> Based on the structural similarity of the new amino acid with (''S'')-[[glutamic acid]], (''S'')-(+)-lycoperdic acid is expected to have [[Receptor antagonist|antagonistic]] or [[agonistic]] activity for the [[glutamate receptor]] in the mammalian [[central nervous system]]. Methods to [[Organic synthesis|synthesize]] the compounds were reported in 1992,<ref name="Kaname 1992"/> 1995,<ref name="Yoshifuji 1995"/> and 2002.<ref name="Makino 2002"/>

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Almendros 1987">{{cite journal |vauthors=Almendros G,  Martín F, González-Vila FJ, Martínez AT |title=Melanins and lipids in ''Lycoperdon perlatum'' fruit bodies |journal=Transactions of the British Mycological Society |year=1987 |volume=89 |issue=4 |pages=533–7 |doi=10.1016/S0007-1536(87)80087-6 |url=http://www.cybertruffle.org.uk/cyberliber/59351/0089/004/0533.htm }}</ref>

<ref name="Ammirati 1987">{{cite book |vauthors=Ammirati JF, McKenny M, Stuntz DE |title=The New Savory Wild Mushroom |publisher=University of Washington Press |location=Seattle, Washington |year=1987 |page=194 |isbn=0-295-96480-4}}</ref>

<ref name="Arora 1986">{{cite book |author=Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |pages=693–4 |isbn=0-89815-169-4}}</ref>

<ref name="Barros 2009">{{cite journal |vauthors=Barros L, Dueñas M, ((Ferreira ICFR)), Baptista P, Santos-Buelga C |title=Phenolic acids determination by HPLC-DAD-ESI/MS in sixteen different Portuguese wild mushrooms species |journal=[[Food and Chemical Toxicology]] |year=2009 |volume=47 |issue=6 |pages=1076–9 |doi=10.1016/j.fct.2009.01.039 |pmid=19425182}}</ref>

<ref name="Batsch 1786">{{cite book |author=Batsch AJGK |title=Elenchus fungorum |year=1783 |publisher=apud J. J. Gebauer |location=Halle an der Saale, Germany |page=147 |language=Latin |url=http://caliban.mpiz-koeln.mpg.de/batsch/elenchus1/batsch_elenchus_1.pdf |format=PDF}}</ref>

<ref name="Baseia 2005">{{cite journal |author=Baseia UG |title=Some notes on the genera ''Bovista'' and ''Lycoperdon'' (Lycoperdaceae) in Brazil |journal=Mycotaxon |year=2005 |volume=91 |pages=81–6 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0091/0081.htm}}</ref>

<ref name="Buckeridge 2011">{{cite journal |vauthors=Buckeridge D, Torrance A, Daly M |title=Puffball mushroom toxicosis (lycoperdonosis) in a two-year-old dachshund |journal=Veterinary Record |year=2011 |volume=168 |issue=11 |page=304 |doi=10.1136/vr.c6353 |pmid=21498199}}</ref>

<ref name="ChuChou 1983">{{cite journal |vauthors=Chu-Chou M, Grace LJ |title=Hypogeous fungi associated with some forest trees in New Zealand |journal=New Zealand Journal of Botany |year=1983 |volume=21 |issue=2 |pages=183–90 |doi=10.1080/0028825X.1983.10428543}}</ref>

<ref name="Colak 2009">Nutritional values are based on [[food chemistry|chemical analysis]] of Turkish specimens, conducted by Colak and colleagues at the Department of Chemistry, [[Karadeniz Technical University]]. Source: {{cite journal |vauthors=Colak A, Faiz Ö, Sesli E|year=2009 |title=Nutritional composition of some wild edible mushrooms |journal=Türk Biyokimya Dergisi [Turkish Journal of Biochemistry] |volume=34 |issue=1 |pages=25–31 |url=http://www.turkjbiochem.com/2009/025-031.pdf |format=PDF}}</ref>

<ref name="Davis 2012">{{cite book |vauthors=Davis RM, Sommer R, Menge JA |title=Field Guide to Mushrooms of Western North America  |year=2012 |publisher=University of California Press |location=Berkeley, California |isbn=978-0-520-95360-4 |page=372 |url=https://books.google.com/books?id=obp7jddvjt4C&pg=PA372}}</ref>

<ref name="Demoulin 1975">{{cite journal |vauthors=Demoulin V, Dring DM |title=Gasteromycetes of Kivu (Zaire), Rwanda and Burundi |journal=Bulletin du Jardin botanique national de Belgique |year=1975 |volume=45 |issue=3/4 |pages=339–72 |jstor=3667488 |doi=10.2307/3667488}}</ref>

<ref name="Dickinson 1982">{{cite book |vauthors=Dickinson C, Lucas J |title=VNR Color Dictionary of Mushrooms |year=1982 |publisher=Van Nostrand Reinhold |location=New York, New York |isbn=978-0-442-21998-7 |page=29}}</ref>

<ref name="Dulger 2005">{{cite journal |author=Dulger B. |title=Antimicrobial activity of ten Lycoperdaceae |journal=Fitoterapia |year=2005 |volume=76 |issue=3–4 |pages=352–4 |doi=10.1016/j.fitote.2005.02.004 |pmid=15890468}}</ref>

<ref name="Falandysz 2003">{{cite journal |vauthors=Falandysz J, Lipka K, Kawano M, Brzostowski A, Dadej M, Jedrusiak A, Puzyn T |title=Mercury content and its bioconcentration factors in wild mushrooms at Łukta and Morag, northeastern Poland |journal=Journal of Agricultural and Food Chemistry |year=2003 |volume=51 |issue=9 |pages=2832–6 |doi=10.1021/jf026016l |pmid=12696981}}</ref>

<ref name="Fries 1829">{{cite book |author=Fries EM |title=Systema Mycologicum |volume=3 |year=1829 |page=37 |publisher=Ernesti Mauritii |location=Greifswald, Germany |language=Latin |url=https://archive.org/stream/systemamycologi00friegoog#page/n48/mode/2up}}</ref>

<ref name="Garner 1956">{{cite journal |author=Garner JHB |title=Gasteromycetes from Panama and Costa Rica |journal=Mycologia |year=1956 |volume=48 |issue=5 |pages=757–64 |jstor=3755385 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0048/005/0757.htm |doi=10.2307/3755385}}</ref>

<ref name="Gray 1821">{{cite book |title=A Natural Arrangement of British Plants |year=1821 |author=Gray SF |location=London, UK |publisher=Baldwin, Cradock and Joy |page=584 |url=https://books.google.com/books?id=KTc-AAAAcAAJ&pg=PA584}}</ref>

<ref name="Gregory 1947">{{cite journal |author=Gregory PH |title=The operation of the puff-ball mechanism of ''Lycoperdon perlatum'' by raindrops shown by ultra-high-speed Schlieren cinematography |journal=Transactions of the British Mycological Society |year=1949 |volume=32 |issue=1 |pages=11–5 |url=http://www.cybertruffle.org.uk/cyberliber/59351/0032/001/0011.htm |doi=10.1016/S0007-1536(49)80030-1}}</ref>

<ref name="Jeppson 2006">{{cite book |author=Jeppson M. |chapter=The genus ''Lycoperdon'' in Greenland and Svalbard |title=Arctic and Alpine Mycology |editor-last=Boertmann D, Knudsen H. |series=Meddelelser om Grønland Bioscience |volume=6 |year=2006 |location=Copenhagen, Denmark |publisher=Museum Tusculanum Press |page=106 |isbn=978-87-635-1277-0 |url=https://books.google.com/books?id=5OxJHNY6PGcC&pg=PA106}}</ref>

<ref name="Jordan 2004">{{cite book |author=Jordan M. |title=The Encyclopedia of Fungi of Britain and Europe |publisher=Frances Lincoln |location=London, UK |year=2004 |page=357 |isbn=0-7112-2378-5 |url=https://books.google.com/books?id=ULhwByKCyEwC&pg=PA357}}</ref>

<ref name="Kaname 1992">{{cite journal |vauthors=Kaname M, Yoshifuji S |title=1st synthesis of lycoperdic acid |journal=Tetrahedron Letters |year=1992 |volume=33 |issue=52 |pages=8103–4 |doi=10.1016/S0040-4039(00)74730-7}}</ref>

<ref name="Kasuya 2004">{{cite journal |author=Kasuya T. |title=Gasteromycetes of Chiba Prefecture, Central Honshu, Japan – I. The family Lycoperdaceae |journal=Journal of the Natural History Museum and Institute Chiba |year=2004 |volume=8 |issue=1 |pages=1–11 |issn=0915-9452}}</ref>

<ref name="Kuo 2007">{{cite book |author=Kuo M. |title=100 Edible Mushrooms |publisher=The University of Michigan Press |location=Ann Arbor, Michigan |year=2007 |page=192 |isbn=0-472-03126-0}}</ref>

<ref name="Laessoe 1994">{{cite journal |vauthors=Læssøe T, Spooner B |title=The uses of 'Gasteromycetes' |journal=Mycologist |year=1994 |volume=8 |issue=4 |pages=154–9 |doi=10.1016/S0269-915X(09)80179-1}}</ref>

<ref name="Lamotte 1978">{{cite journal |vauthors=Lamotte JL, Oleksyn B, Dupont L, Dideberg O, Campsteyn H, Vermiere M |title=The crystal and molecular structure of 3-[(5''S'')-5-carboxy-2-oxotetrahydrofur-5-yl]-(2''S'')-alanine (lycoperdic acid) |journal=Acta Crystallographica Section B |year=1978 |volume=34 |issue=12 |pages=3635–8 |doi=10.1107/S0567740878011772}}</ref>

<ref name="Larsson 2008">{{cite journal |vauthors=Larsson E, Jeppson M |title=Phylogenetic relationships among species and genera of Lycoperdaceae based on ITS and LSU sequence data from north European taxa |journal=Mycological Research |year=2008 |volume=112 |issue=1 |pages=4–22 |doi=10.1016/j.mycres.2007.10.018 |pmid=18207380}}</ref>

<ref name="Lemin 2010">{{cite journal |vauthors=Lemin M, Vasquez A, Chacon S |title=Etnomicología y comercialización de hongos en mercados de tres poblados del noreste del estado de Puebla, México |trans_title=Ethnomycology and marketing of mushrooms in markets in three villages of the northeastern state of Puebla, Mexico |journal=Brenesia |year=2010 |issue=73/74 |pages=58–63 |issn=0304-3711 |language=Spanish}}</ref>

<ref name="Maghembe 1984">{{cite journal |vauthors=Maghembe JA, Redhead JF |title=A survey of ectomycorrhizal fungi in pine plantations in Tanzania |journal=East African Agricultural and Forestry Journal |year=1984 |volume=45 |issue=3 |pages=203–6 |issn=0012-8325}}</ref>

<ref name="Makino 2002">{{cite journal |vauthors=Makino K, Shintani K, Yamatake T, Hara O, Hatano K, Hamada Y |title=Stereoselective synthesis of (''S'')-(+)-lycoperdic acid through an ''endo'' selective hydroxylation of the chiral bicyclic lactam enolate with MoOPH |journal=Tetrahedron |year=2002 |volume=58 |issue=48 |pages=9737–40 |doi=10.1016/S0040-4020(02)01254-1}}</ref>

<ref name="Marley 2010">{{cite book |author=Marley G. |title=Chanterelle Dreams, Amanita Nightmares: The Love, Lore, and Mystique of Mushrooms |publisher=Chelsea Green Publishing |location=White River Junction, Vermont |year=2010 |page=49 |isbn=1-60358-214-2 |url=https://books.google.com/books?id=KJybhI1yWucC&pg=PA49}}</ref>

<ref name="Massee 1887">{{cite journal |author=Massee GE |title=A monograph of the genus ''Lycoperdon'' (Tournef.) Fr. |journal=Journal of the Royal Microscopical Society |year=1887 |volume=5 |series=2 |pages=701–27 (see p. 713) |url=http://biodiversitylibrary.org/page/31413751}}</ref>

<ref name="McCreight 2003">{{cite journal |vauthors=McCreight JD, Schroeder DB |title=Cadmium, lead and nickel content of ''Lycoperdon perlatum'' Pers. in a roadside environment |journal=Environmental Pollution  |year=1977 |volume=13 |issue=3 |pages=265–8 |doi=10.1016/0013-9327(77)90045-3}}</ref>

<ref name="Miller 1988">{{cite book |vauthors=Miller HR, Miller OK |title=Gasteromycetes: Morphological and Developmental Features, with Keys to the Orders, Families, and Genera |publisher=Mad River Press |location=Eureka, California |year=1988 |page=40 |isbn=0-916422-74-7}}</ref>

<ref name="Miller 2006">{{cite book |vauthors=Miller HR, Miller OK |title=North American Mushrooms: A Field Guide to Edible and Inedible Fungi |publisher=Falcon Guide |location=Guilford, Connecticut |year=2006 |page=453 |isbn=0-7627-3109-5}}</ref>

<ref name="MMWR 1994">{{cite journal |vauthors=Taft TA, Cardillo RC, Letzer D, Kaufman CT, Kazmierczak JJ, Davis JP |title=Respiratory illness associated with inhalation of mushroom spores – Wisconsin, 1994 |journal=Morbidity and Mortality Weekly Report |publisher=[[Centers for Disease Control and Prevention]] |url=http://www.cdc.gov/mmwr/preview/mmwrhtml/00032029.htm |date=July 29, 1994 |volume=43 |issue=29 |pages=525–6 |accessdate=2012-07-28}}</ref>

<ref name="Montoya 2003">{{cite journal |vauthors=Montoya A, Hernández-Totomoch O, Estrada-Torres A, Kong A, Caballero J |title=Traditional knowledge about mushrooms in a Nahua community in the State of Tlaxcala, México |journal=Mycologia |year=2003 |volume=95 |issue=1 |pages=793–806 |jstor=3762007 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0095/005/0793.htm | doi = 10.2307/3762007 |pmid=21148986}}</ref>

<ref name="Moreno 2010">{{cite journal |vauthors=Moreno G, Lizárraga M, Esqueda M, Coronado ML |title=Contribution to the study of gasteroid and secotioid fungi of Chihuahua, Mexico |journal=Mycotaxon |year=2010 |volume=112 |pages=291–315 |doi=10.5248/112.291}}</ref>

<ref name="Moss 1998">{{cite journal |author=Moss MO |title=Gasteroid Basidiomycetes on postage stamps |journal=Mycologist |year=1998 |volume=12 |issue=3 |pages=104–6 |doi=10.1016/S0269-915X(98)80005-0}}</ref>

<ref name="Natarajan 1987">{{cite journal |vauthors=Natarajan K, Purushothama KB |title=On the occurrence of ''Lycoperdon perlatum'' in ''Pinus patula'' plantations in Tamil Nadu |journal=Current Science |year=1987 |volume=56 |issue=21 |pages=1117–8 |url=http://www.ias.ac.in/j_archive/currsci/56/21/1117-1118/viewpage.html |format=PDF}}</ref>

<ref name="Nedelcheva 2007">{{cite journal |vauthors=Nedelcheva D,  Antonova D,  Tsvetkova S, Marekov I, Momchilova S, Nikolova-Damyanova B,  Gyosheva M |title=TLC and GC‐MS probes into the fatty acid composition of some Lycoperdaceae mushrooms |journal=Journal of Liquid Chromatography & Related Technologies |year=2007 |volume=30 |issue=18 |pages=2717–27 |doi=10.1080/10826070701560629}}</ref>

<ref name="Orr 1979">{{cite book |vauthors=Orr DB, Orr RT |title=Mushrooms of Western North America |publisher=University of California Press |location=Berkeley, California |year=1979 |page=115 |isbn=0-520-03656-5}}</ref>

<ref name="Pegler 1995">{{cite book |vauthors=Læssøe T, Pegler DN, Spooner B |title=British Puffballs, Earthstars and Stinkhorns: An Account of the British Gasteroid Fungi |publisher=Royal Botanic Gardens |location=Kew, UK |year=1995 |page=152 |isbn=0-947643-81-8}}</ref>

<ref name="Perdeck 1950">{{cite journal |author=Perdeck AC |title=A revision of the Lycoperdaceae of the Netherlands |journal=Blumea |year=1950 |volume=6 |pages=480–516 (see p. 505)}}</ref>

<ref name="Persoon 1796">{{cite book |author=Persoon CH |title=Observationes Mycologicae |volume=1 |year=1796 |location=Leipzig, Germany |publisher=Petrum Phillippum Wolf |page=4 |language=Latin |url=http://bibdigital.rjb.csic.es/Imagenes/O_DIS_BOT(2)/DIS_BOT(2)_030.pdf |format=PDF}}</ref>

<ref name="Pujol 1990">{{cite journal |vauthors=Pujol V, Seux V, Villard J |title=Research of antifungal substances produced by higher fungi in culture |journal=Annales Pharmaceutiques Françaises |year=1990 |volume=48 |issue=1 |pages=17–22 |issn=0003-4509}}</ref>

<ref name="Quinche 1990">{{cite journal |author=Quince J-P |title=''Lycoperdon perlatum'', un champignon accumulateur de métaux lourds et de sélènium |trans_title=''Lycoperdon perlatum'' a fungus accumulating heavy metals and selenium |journal=Mycologia Helvetica |year=1990 |volume=3 |issue=4 |pages=477–86 |issn=0256-310X |language=French}}</ref>

<ref name="Ramesh 2010">{{cite journal |vauthors=Ramesh C, Pattar MG |title=Antimicrobial properties, antioxidant activity and bioactive compounds from six wild edible mushrooms of western ghats of Karnataka, India |journal=Pharmacognosy Research |year=2010 |volume=2 |issue=2 |pages=107–12 |doi=10.4103/0974-8490.62953 |pmc=3140106 |pmid=21808550}}</ref>

<ref name="Ramsey 1980">{{cite journal |author=Ramsey RW |title=''Lycoperdon nettyana'', a new puffball from western Washington State |journal=Mycotaxon |year=1980 |volume=11 |issue=1 |pages=185–8 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0011/001/0185.htm}}</ref>

<ref name="Roberts 2011">{{cite book |vauthors=Roberts P, Evans S |title=The Book of Fungi |year=2011 |publisher=University of Chicago Press |location=Chicago, Illinois |page=520 |isbn=978-0-226-72117-0}}</ref>

<ref name="Rubensohn 2009">{{cite journal |author=Rubensohn M. |title=Inhalation pneumonitis in a dog from spores of puffball mushrooms |journal=Canadian Veterinary Journal |year=2009 |volume=50 |issue=1 |page=93 |pmc=2603663 |pmid=19337622}}</ref>

<ref name="Sari 2012">{{cite journal |vauthors=Sari A, Tuzen M, Citak D |title=Equilibrium, thermodynamic and kinetic studies on biosorption of mercury from aqueous solution by macrofungus (''Lycoperdon perlatum'') biomass |journal=Separation Science and Technology |year=2012 |volume=47 |issue=8 |pages=1167–76 |doi=10.1080/01496395.2011.644615}}</ref>

<ref name="Schalkwijk-Barendsen 1991">{{cite book |author=Schalkwijk-Barendsen HME. |title=Mushrooms of Western Canada |publisher=Lone Pine Publishing |location=Edmonton, Canada |year=1991 |page=346 |isbn=0-919433-47-2}}</ref>

<ref name="Strand 1967">{{cite journal |vauthors=Strand RD, ((Neuhauser EBD)), Somberger CF |title=Lycoperdonosis |journal=New England Journal of Medicine |year=1967 |volume=277 |issue=2 |pages=89–91 |doi=10.1056/NEJM196707132770209 |pmid=6027138}}</ref>

<ref name="Suda 2009">{{cite journal |author=Suda I. |title=Metsamardikate (Coleoptera) uued liigid Eestis |trans_title=New woodland beetle species (Coleoptera) in Estonian fauna |journal=Forestry Studies/Metsanduslikud Uurimused |year=2009 |volume=50 |pages=98–114 |language=Estonian, English |issn=1406-9954 |doi=10.2478/v10132-011-0071-0}}</ref>

<ref name="Szummy 2010">{{cite journal |vauthors=Szummy A, Adamski M, Winska K, Maczka W |title=Identyfikacja związków steroidowych i olejków eterycznych z ''Lycoperdon perlatum'' |trans_title=Identification of steroid compounds and essential oils from ''Lycoperdon perlatum'' |journal=Przemysł Chemiczny |year=2010 |volume=89 |issue=4 |pages=550–3 |issn=0033-2496 |language=Polish}}</ref>

<ref name="Thind 1982">{{cite journal |vauthors=Thind KS, ((Thind IPS)) |title=Gasteromycetes of the Himalayas, India. 10 |journal=Research Bulletin of the Panjab University Science |year=1982 |volume=33 |issue=1–2 |pages=139–50 |ISSN=0555-7631}}</ref>

<ref name="Thysell 1997">{{cite journal |vauthors=Thysell DR, Villa LJ, Carey AB |title=Observations of northern flying squirrel feeding behavior: use of non-truffle food items |journal=Northwestern Naturalist |year=1997 |volume=78 |issue=3 |pages=87–92 |jstor=3536862 |doi=10.2307/3536862}}</ref>

<ref name="urlFungorum synonymy: Lycoperdon perlatum">{{cite web |title=Synonymy: ''Lycoperdon perlatum'' Pers. |url=http://www.speciesfungorum.org/Names/SynSpecies.asp?RecordID=220647 |publisher=[[Index Fungorum]]. [[CAB International]] |accessdate=2012-10-11}}</ref>

<ref name="urlMycoBank: Lycoperdon perlatum">{{cite web |title=''Lycoperdon perlatum'' Pers. 1796 |url=http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=14721&Fields=All |publisher=[[MycoBank]]. International Mycological Association |accessdate=2012-07-29}}</ref>

<ref name="Ylmaz 2003">{{cite journal |vauthors=Ylmaz F, Işloǧlu M, Merdivan M |title=Heavy metal levels in some macrofungi |journal=Turkish Journal of Botany |year=2003 |volume=27 |issue=1 |pages=45–56 |issn=1300-008X |url=http://journals.tubitak.gov.tr/botany/issues/bot-03-27-1/bot-27-1-4-0105-6.pdf |format=PDF}}</ref>

<ref name="Yoshifuji 1995">{{cite journal |vauthors=Yoshifuji S, Kaname M |title=Asymmetric synthesis of lycoperdic acid |journal=Chemical & Pharmaceutical Bulletin |year=1995 |volume=43 |issue=10 |pages=1617–20 |issn=0009-2363 |doi=10.1248/cpb.43.1617}}</ref>

<ref name="Zhishu 1993">{{cite book |vauthors=Zhishu B, Zheng G, Taihui L |title=The Macrofungus Flora of China's Guangdong Province |publisher=Columbia University Press |location=New York, New York |year=1993 |page=555 |isbn=978-962-201-556-2 |url=https://books.google.com/books?id=0cAered-vqYC&pg=PA555}}</ref>

}}

==External links==
{{commons category|Lycoperdon perlatum}}
*{{IndexFungorum|220647}}

{{featured article}}

[[Category:Agaricaceae]]
[[Category:Edible fungi]]
[[Category:Puffballs]]
[[Category:Fungi described in 1796]]
[[Category:Fungi found in fairy rings]]
[[Category:Fungi of Asia]]
[[Category:Fungi of Australia]]
[[Category:Fungi of Central America]]
[[Category:Fungi of Europe]]
[[Category:Fungi of New Zealand]]
[[Category:Fungi of North America]]
[[Category:Fungi of Oceania]]
[[Category:Fungi of South America]]
[[Category:Taxa named by Christiaan Hendrik Persoon]]