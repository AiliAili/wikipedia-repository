<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Mouse
 | image=CMouse.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Three-seat cabin monoplane
 | national origin=United Kingdom
 | manufacturer=[[Comper Aircraft Company]]
 | designer=[[Nicholas Comper]]
 | first flight=11 September 1933
 | introduced=
 | retired=1935
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Comper Mouse''' was a 1930s [[United Kingdom|British]] three-seat cabin [[monoplane]] designed by [[Nicholas Comper]], and built by the [[Comper Aircraft Company]] at [[Heston Aerodrome]] in 1933.

==Development==
The Mouse was a low-wing monoplane touring aircraft, powered by a 130&nbsp;hp (97&nbsp;kW) [[de Havilland Gipsy Major]] piston engine. Construction was mainly of fabric-covered spruce wood frames, with some plywood-covered sections. It had folding wings, retractable main landing gear and fixed tailskid. Accommodation was for the pilot and two passengers, accessible via a sliding framed canopy, plus an additional luggage locker. The first flight of the Mouse was at Heston aerodrome on 11 September 1933, piloted by Nick Comper. In February 1934, it was assessed at [[Aeroplane and Armament Experimental Establishment|A&AEE]] [[RAF Martlesham Heath|Martlesham Heath]], leading to various small design changes<ref name='Riding (1988)'>Riding (1988)</ref><ref name='Archive 2/05'/><ref name='Nick'>Nick Comper official website</ref>

==Operational history==
On 13–14 July 1934, the Mouse (registered G-ACIX) was flown by E.H. Newman in handicapped heats for the [[King's Cup Race]] at [[Hatfield Aerodrome]] in poor weather conditions. It failed to reach the final race, despite an average speed of 132.75&nbsp;mph. In an already competitive market for touring aircraft, the Mouse failed to attract sales, and only the one was completed before the company ceased trading in August 1934.<ref name='Riding (1988)'/><ref name='Lewis (1970)'>Lewis (1970)</ref>

==Specifications==
{{aerospecs
|ref=Meaden (2005)<ref name='Archive 2/05'>Meaden (2005)</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng

|crew=1 (pilot)
|capacity=2 passengers
|length m=7.65
|length ft=25
|length in=1
|span m=11.43
|span ft=37
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.05
|height ft=6
|height in=9
|wing area sqm=16
|wing area sqft=172
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=590
|empty weight lb=1300
|gross weight kg=1005
|gross weight lb=2215
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[de Havilland Gipsy Major]] piston engine
|eng1 kw=<!-- prop engines -->97
|eng1 hp=<!-- prop engines -->130
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=217
|max speed mph=135
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->209
|cruise speed mph=<!-- if max speed unknown -->130
|range km=965
|range miles=600
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|2}}

==References==
*Boughton, Terence. 1963. The Story of The British Light Aeroplane. John Murray
*{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 2|year= 1974|publisher= Putnam|location= London|isbn=0-370-10010-7 }}
*Lewis, Peter. 1970. British Racing and Record-Breaking Aircraft. Putnam ISBN 0-370-00067-6
*Meaden, Jack & Fillmore, Malcolm. (Summer 2005). The Comper Lightplanes. Air-Britain Archive (quarterly). Air-Britain. {{ISSN|0262-4923}}
*Riding, Richard T. 1987. Ultralights: The Early British Classics. Patrick Stephens ISBN 0-85059-748-X
*Riding, Richard T. June 1988. British Pre-war Lightplanes No.1: Comper Mouse. Aeroplane Monthly. IPC Media
*Riding, Richard T. March 2003. Database: Comper Swift. Aeroplane Monthly. IPC Media 
*Smith, Ron. 2002. British Built Aircraft Vol.1: Greater London ISBN 0-7524-2770-9

==External links==
{{commons category|Comper Mouse}}
*[http://www.nickcomper.co.uk Nick Comper official website]
*[http://www.caa.co.uk/docs/HistoricalMaterial/G-ACIX.pdf UK Civil Aviation Authority registration record for G-ACIX]

{{Comper aircraft}}

[[Category:British civil utility aircraft 1930–1939]]
[[Category:Comper aircraft|Mouse]]