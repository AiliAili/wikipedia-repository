'''Victor Henri Brombert'''  (born, November 11, 1923) is an American scholar of nineteenth and twentieth century literature, the  Henry Putnam University Professor at [[Princeton University]]<ref>{{cite book|authorlink1=Vineta|editor1-last=Colby|title=World Authors, 1980-1985|date=1991|publisher=H.W. Wilson Company|isbn=0824207971|page=112}}</ref>

==Biography==

Brombert was born in [[Berlin]] in 1923 into a well-to-do Russian-Jewish family that had fled Russia at the outbreak of the Revolution and settled in [[Leipzig]]. When Hitler came to power in Germany, the family left for Paris,and Brombert   received his secondary education at the [[Lycée Janson-de-Sailly]]. As the German army advanced on Paris in 1940, the family fled to the unoccupied zone under the control of the Vichy government and a year later, in 1941, escaped via Spain to the United States.

In May 1943 Brombert was drafted into the U.S. army. Due to his  fluency in French, German, and Russian he was placed  in a special unit, composed chiefly of refugees from Nazi-occupied European countries, that was trained in front-line military intelligence at [[Fort Ritchie|Camp Ritchie, Maryland]], and featured in a documentary film "[[The Ritchie Boys]]"<ref>[http://www.imdb.com/title/tt0435725/]</ref><ref>[http://www.ritchieboys.com/EN/boys_brombert.html]</ref><ref>[http://www.docurama.com/docurama/the-ritchie-boys/]</ref><ref>[http://video.tvguide.com/The+Ritchie+Boys/The+Ritchie+Boys/8150782?autoplay=true&partnerid=OVG]</ref>

In 1944 he took part in the Normandy landings with the 2nd Armored Division at [[Omaha Beach]] and also saw action with the 28th Infantry Division in the [[Battle of the Bulge]].   After the war, Brombert studied at Yale University, where he received a B.A. in 1948 and a Ph.D. in Romance Languages and Literatures in 1953. As a graduate student, he was awarded a Fulbright Fellowship (1950–51) to study in Rome, adding Italian to the languages in which he has native fluency. He is married to Beth Archer Brombert, a  translator from French and Italian, and the author of the   biographies ''Cristina: Portraits of a Princess'' and ''Édouard Manet: Rebel in a Frock Coat''. The Bromberts have two children, Lauren and Marc.

==Academic career==

On completion of his graduate studies Brombert joined the Yale Department of Romance Languages and Literatures. He was appointed Benjamin F. Barge Professor in 1968 and was chair of his Department from 1964 to 1973. In 1975 he moved to Princeton, where he had been appointed Henry Putnam University Professor and was affiliated with the Departments of Comparative Literature and Romance Languages and Literatures. At Princeton, he was also Director of Princeton's Christian Gauss Seminars in Criticism and chairman of its Council of the Humanities. He entered emeritus status in 1999 <ref>[https://www.princeton.edu/fit/people/display_person.xml?netid=brombert Princeton biography]</ref>

Brombert has been a visiting professor at many universities in the U.S. and Europe: the University of California (Berkeley), the Johns Hopkins University, Columbia University, New York University, the University of Colorado, the Scuola Normale Superiore (Pisa, Italy), the Collège de France (Paris), the University of Bologna, the University of Puerto Rico.

==Awards==

Brombert has held fellowships from the [[American Council of Learned Societies]] (1967) and from the [[John Simon Guggenheim Memorial Foundation|Guggenheim Foundation]] (1954–55; 1970). He was [[Phi Beta Kappa]] Visiting Scholar in 1986-87 and 1989–90, and a scholar-in-residence at the Rockefeller Foundation in [[Bellagio, Italy]] in 1975 and in 1990. He was elected to the [[American Academy of Arts and Sciences]] in 1974,  and to the [[American Philosophical Society]] in 1987. He holds honorary degrees from the [[University of Chicago]] (Doctor of Humane Letters, 1981) and the [[University of Toronto]] (Doctor of Laws, 1997). In 1985 he was awarded the Wilbur Cross Medal of the Yale Alumni Association for "distinguished achievements in scholarship, teaching, academic administration, and public service.” In France, he was honored with the  Médaille Vermeil de la Ville de Paris " (1985) and was made  [[Palmes Académiques|Commandeur des Palmes Académiques]]  (2008) and "[[Chevalier de la Légion d’Honneur]]'' (2009).

In 1988-89 he served as president of the [[Modern Language Association]].

==Publications==

Brombert’s work is primarily on 19th and 20th century [[French literature]], and also on the  [[history of ideas]]; the theory of literary criticism; and comparative studies of   Italian, Russian, and German narrative writers. In addition to his books, he has contributed to edited volumes and written journal articles  on French writers from Pascal to Malraux, Sartre, and Camus, and on many non-French writers:  Dostoevsky, Gogol, Tolstoy; Büchner, Max Frisch, Kafka, Thomas Mann; Giorgio Bassani, Primo Levi, Italo Svevo; J. M. Coetzee, Virginia Woolf.

Brombert is also the author of a memoir, ''Trains of Thought: Memories of a Stateless Youth'' (New York: W.W. Norton, 2002; paperback, Anchor Books, 2004) <ref>Review of ''Trains of Thought'' by [[Frank Kermode]], ''[[The Guardian]]'' [https://www.theguardian.com/books/2002/sep/14/featuresreviews.guardianreview22]</ref><ref>Review  of ''Trains of Thought''  by [[Richard Howard]] in ''[[The New York Times]]'' [https://www.nytimes.com/2002/06/30/books/engine-of-escape.html]</ref> He is   currently working on a sequel, to be entitled ''The Sabbatical Years''.
 
In the words of a reviewer in ''The Wall Street Journal'' (December 27, 2013), “Victor Brombert...has been for more than 50 years one of the glories of humanistic scholarship at Yale and Princeton. Though a generation younger than scholarly patriarchs like Erich Auerbach and Leo Spitzer, Mr. Brombert has nonetheless shown himself comparably learned and cosmopolitan in his studies...”

'''Principal Works of Literary Criticism:'''

*''The Criticism of T.S. Eliot'' (New Haven: Yale University Press, 1949)
*''Stendhal et la voie oblique'' (Paris: Presses Universitaires de France, 1954)
*''The Intellectual Hero: Studies in the French  Novel, 1880-1955'' (Philadelphia and New York: Lippincott, 1961; Chicago: University of Chicago Press, 1964).<ref>Review by René Girard in ''[[Modern Philology]]'', 61 (1963): 70-72.[http://www.jstor.org/stable/434801?seq=2#page_scan_tab_contents jstor]</ref>
*''The Novels of Flaubert: A Study of Themes and Techniques'' (Princeton: Princeton University Press, 1966)
*''Stendhal: Fiction and the Themes of Freedom'' (New York: Random House, 1968)
*''Flaubert par lui-même'' (Paris: Éditions du Seuil, 1971)
*''La prison romantique'' (Paris: Librairie José Corti, 1976). English trans. ''The Romantic Prison'' (Princeton: Princeton University Press, 1978). Awarded the Harry Levin Prize in Comparative Literature in 1978
*''Victor Hugo and the Visionary Novel'' (Cambridge, MA: Harvard University Press, 1984).<ref>[https://www.nytimes.com/1984/12/23/books/he-merged-myth-and-history.html Review in ''The New York Times'']</ref>
*''The Hidden Reader: Stendhal, Balzac, Hugo, Baudelaire, Flaubert'' (Cambridge, MA: Harvard University Press, 1988).<ref>[https://www.nytimes.com/1988/10/02/books/irony-to-the-rescue.html Review by John Sturrock in ''The New York Times'']</ref>
*''In Praise of Antiheroes. Figures and Themes in Modern European Literature, 1830-1980'' (Chicago: University of Chicago Press, 1999).<ref>[https://www.nytimes.com/books/99/05/09/reviews/990509.09suthert.html Review in ''The New York Times'']</ref>
*''Musings on Mortality. From Tolstoy to Primo Levi'' (Chicago: University of Chicago Press, 2013). Selected as the  2013 winner of the Robert Penn Warren-Cleanth Brooks award for outstanding literary criticism.<ref>Review in ''[[The Wall Street Journal]]''[https://www.wsj.com/articles/SB10001424052702304337404579211891686520678]</ref>

'''as Editor:'''

*''Stendhal: A Collection of Critical Essays'' (Englewood Cliffs, N.J.: Prentice Hall, 1962) 
*''Balzac’s “La peau de chagrin”'' (New York: Laurel Edition, 1962) 
*''The Hero in Literature: Major Essays on the Changing Concepts of Heroism from Classical Times to the Present'' (Greenwich, CT, Fawcett Publications, 1969)
*''Flaubert’s “Madame Bovary”'' (Cambridge, MA: Gallimard/Schoenhof's, 1986)

==References==
{{Reflist}}

==External links==
*[https://www.princeton.edu/fit/people/display_person.xml?netid=brombert&display=Emeritus%20Faculty  Entry on Princeton emeritus faculty list]
*[http://pressblog.uchicago.edu/2013/11/11/on-victor-bromberts-ninetieth-birthday.html  "Philosopher and Nietzsche scholar Alexander Nehamas on Brombert" University of Chicago Press]
*[https://www.princeton.edu/main/news/archive/S23/42/00M44/index.xml?section=people "On being awarded the Légion d’honneur"]
*[http://www.sinn-und-form.de/?tabelle=leseprobe&titel_id=3407 "Interview with Richard Schroetter" (Paris, September 19, 2002) in ''Sinn und Form'' (Berlin), 6 (2009):757-67]
*
* http://www.wsj.com/articles/SB10001424052702304337404579211891686520678

{{DEFAULTSORT:Brombert, Victor}}
[[Category:Living people]]
[[Category:1923 births]]
[[Category:American people of Russian-Jewish descent]]
[[Category:People from Berlin]]
[[Category:American military personnel of World War II]]
[[Category:Yale University faculty]]
[[Category:Princeton University faculty]]
[[Category:American literary critics]]