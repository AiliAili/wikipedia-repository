{{unref|date=December 2014}}
{{italic title}}
'''''Five Minutes''''' ({{lang-pt|Cinco Minutos}}) is the [[debut novel]] by [[Brazil]]ian writer [[José de Alencar]]. It was initially published under ''[[feuilleton]]'' form at the [[newspaper|journal]] ''[[Diário do Rio de Janeiro]]'' in [[1856 in literature|1856]], being so popular that it was re-released under hardcover form a few years later.

==Plot and setting==
The book is written [[epistolary novel|in the form of a letter]], addressed to "D.", the narrator's cousin. It tells the story of the aforementioned narrator's love by a woman named Carlota, whose name is only given in the final chapters of the book.

The story begins in [[Rio de Janeiro|Rio de Janeiro City]]. The narrator, a man whose name is not given throughout the entire book, is late to take his [[bus]], and misses it because of a five-minute delay (hence the book's name). Forced to take the next bus, he sits near a woman, falling in love with her at first sight, but he is not able to see her face because it is covered by a veil, and deduces she is ugly. Soon after, the woman leaves, mysteriously whispering in his ear "Non ti scordar di me" ([[Italian language|Italian]] for "Do not forget me"); mesmerized, he tries to run after the woman, but loses her from sight. After one month trying to discover who she is, he finally finds her once again in a [[theatre]], during a performance of [[Giuseppe Verdi]]'s ''[[La traviata]]''. He confesses his love for her, but she flees without saying a word, only leaving him a tear-soaked [[handkerchief]].

After many other mishaps, the man finally finds her again and confesses once more; however, she ignores him. Later on, the man receives a letter from the woman via her old mother, where she says she has been observing him secretly for a long time, and actually loves him, but they would never be able to stay together because she has an incurable disease. In the same letter, she says she has left for [[Petrópolis]] and, on the following day, would leave to Europe alongside her mother. She asks the narrator to come to her if he wants to live his love.

The narrator leaves Brazil after a brief encounter with her in the [[Villegagnon Island]], and searches for her everywhere, always finding a note from her in the places he visits. Finally finding her, they spend ten days in Europe. Nearly dying, the woman (whose name is now revealed to be Carlota) asks the narrator for a kiss, and after obtaining it, Carlota miraculously recovers from her disease. They marry and, after spending a year in Europe, return to Brazil and move to a farm in [[Minas Gerais]].

==External links==
* [http://www.bibvirt.futuro.usp.br/index.php/content/view/full/1855 Download the book] {{pt icon}}

{{Wikisourcelang|pt|Cinco Minutos}}

{{José de Alencar}}

{{DEFAULTSORT:Five Minutes (Novel)}}
[[Category:1856 novels]]
[[Category:Novels by José de Alencar]]
[[Category:Portuguese-language novels]]
[[Category:Epistolary novels]]
[[Category:Brazilian novels]]
[[Category:Debut novels]]


{{1850s-novel-stub}}
{{Brazil-lit-stub}}