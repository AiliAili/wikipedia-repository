{{Use dmy dates|date=July 2013}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Goral
 | image=GGoral.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=General purpose military
 | national origin=United Kingdom
 | manufacturer=[[Gloster Aircraft Company]]
 | designer=S.J. Waters & [[Henry Folland|H.P.Folland]]
 | first flight=8 February 1927
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from=
 | variants with their own articles=
}}
|}
The '''Gloster Goral''' was a single-engined two-seat biplane built to an [[Air Ministry]] contract for a general-purpose military aircraft in the late 1920s.  It did not win the contest and only one was built.

==Development==
In 1927, driven by conflicting pressures of an ageing, [[World War I]] aircraft stock and the continual need for economy the [[Air Ministry]] was attracted by the idea of a general-purpose, multi-tasking machine which used many components from the large stocks of [[Airco DH.9A]] accumulated ten years before.  The result was [[List of Air Ministry Specifications#1920-1929|Air Ministry specification]] 26/27, which also encouraged the use of a metal airframe for use overseas and of the abundant [[Napier Lion]] engine.  At least eight manufacturers responded and the Goral was Gloster's submission.<ref name="DNJ">{{harvnb|James|1971|pp=136–9}}</ref>

The Goral was an all-metal framed, fabric-covered biplane using DH9A wings of two-bay construction and of slight stagger, with parallel interplane struts.  There were ailerons on all wings.  The fuselage was oval in cross section and quite slim.  The wire-braced, round-tipped tailplane carried unbalanced elevators but the small fin carried a square-topped horn-balanced rudder.  The pilot sat under the wing trailing edge cutout, with the gunner behind him, his gun on a ring, mounted on the raised rear decking.  The undercarriage had a single axle strutted to the fuselage near the front spar and forward to the engine firewall.<ref name="DNJ"/><ref>''Flight'', 28 July 1927</ref>

The Goral was powered by an uncowled 425&nbsp;hp (315&nbsp;kW) [[Bristol Jupiter|Bristol Jupiter VIA ]]  radial engine rather than the suggested Lion, driving a 12&nbsp;ft (3.65 m) two-bladed fixed-pitch propeller.  The engine exhausts ran along  both sides of the lower fuselage from the Goral's long nose to below the gunner's cockpit.<ref name="DNJ"/>

The Goral was designed to allow the wings to be replaced with those from existing DH.9A stocks and the fuselage was constructed so that metal components could be replaced by wooden ones to optimise the possibility of overseas sales.  Nevertheless, these did not materialise, despite Argentinian interest as late as 1931.  At home, the Goral had been in competition for the Air Ministry contract almost immediately after its first flight in February 1927.  The competition winner was the [[Westland Wapiti]].<ref name="DNJ"/>

==The name==
[[Goral]] is the local name for a goat-antelope, native to the mountains of Northern India, ''Nemorhaedus goral''.
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aerospecs
|ref={{harvnb|James|1971|pp=139}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=two
|capacity=
|length m=9.4
|length ft=31
|length in=6
|span m=14.19
|span ft=46
|span in=7
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.3
|height ft=11
|height in=4
|wing area sqm=45.89
|wing area sqft=494
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=1,268
|empty weight lb=2,796
|gross weight kg=2,014
|gross weight lb=4,441
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Bristol Jupiter|Bristol Jupiter VIA]] 9-cylinder radial
|eng1 kw=315<!-- prop engines -->
|eng1 hp=425<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=218
|max speed mph=at 5,000 ft (1,524 m) 136
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=1,207
|range miles=750
|endurance h=at 10,000 ft (3,050 m) 6<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=6,552
|ceiling ft=21,500
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4.7
|climb rate ftmin=to 15,000 ft (4,570 m) 923
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=1× synchronised forward firing 0.303 in (7.7 mm) [[Vickers machine gun]]
|armament2=1× 0.303 in (7.7 mm) [[Lewis gun]] mounted on ring in gunner's cockpit
|armament3=provision for bomb racks carrying 2×230 lb (104 kg), 4×112 lb (50 kg) or 16×20 lb (9 kg) bombs
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Gloster Goral}}
{{Reflist}}
{{Refbegin}}
*{{Cite book |title= Gloster Aircraft since 1917|last= James|first=Derek N. |authorlink= |coauthors= |year=1971 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00084-6|url=|ref=harv |postscript= <!--None--> }}
*{{Cite magazine |last= |first= |authorlink= |coauthors= |year= |month= |title= THE GLOSTER " GORAL ".|magazine= [[Flight International|Flight]]|volume= |issue=28 July 1927 |pages=519–21 |id= |url= http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200567.html|accessdate= |quote= |ref= harv }}
{{Refend}}
{{Gloster aircraft}}

[[Category:British aircraft 1920–1929]]
[[Category:Gloster aircraft|Goral]]