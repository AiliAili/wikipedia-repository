:''For the later monoplane, see [[Bristol M.1|Bristol M.1 Monoplane Scout]]''
:''For the unrelated 1917 prototype design, see [[Bristol Scout F]]''
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{|{{Infobox Aircraft Begin
 |name=Bristol Scout
 |image=Bristol Scout.jpg
 |caption=RNAS Bristol Scout D of third production batch
}}{{Infobox Aircraft Type
 |type=single-seat scout/fighter
 |manufacturer=[[Bristol Aeroplane Company|British and Colonial Aeroplane Company]]
 |designer=[[Frank Barnwell]]
 |first flight=23 February 1914
 |introduced=
 |retired=
 |primary user=[[Royal Flying Corps]]<!--please list only one-->
 |more users=[[Royal Naval Air Service]]<br>[[Australian Flying Corps]]<!--up to three more, please separate with<br />-->  |produced=1914–1916<!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built=374<ref name="Barnes">Barnes 1964</ref>
 |unit cost=
 |variants with their own articles=
}}
|}
The '''Bristol Scout''' was a single-seat [[Rotary engine|rotary-engined]] [[biplane]] originally designed as a racing aircraft. Like similar fast, light aircraft of the period it was used by the [[RNAS]] and the [[Royal Flying Corps|RFC]] as a "[[Scout (aircraft)|scout]]", or fast reconnaissance type. It was one of the first single-seaters to be used as a [[fighter aircraft]], although it was not possible to fit it with an effective forward-firing armament until the first British [[Synchronization gear|synchronisation gears]] became available, by which time the Scout was obsolescent. Single-seat fighters continued to be called "scouts" in British usage into the early 1920s.<ref name="Barnes"/>
{{TOC limit|limit=2}}

==Background==
The Bristol Scout was designed in the second half of 1913 by [[Frank Barnwell]] and [[Harry Busteed]], Bristol's chief test pilot, who thought of building a small high-performance biplane while testing the [[Bristol-Burney seaplanes|Bristol X.3]] seaplane, a project which had been designed by a separate secret design department headed by Barnwell.  The design was initially given the works number SN.183, inherited from a cancelled design for the Italian government undertaken by [[Henri Coanda]], the half-finished fuselage of which remained in the workshops and the drawings for the aircraft bore this number.<ref>Barnes 1970, p.91</ref>

==Design and development==
[[File:Bristol Scout Prototype in March 1914.jpg|thumb|Bristol Scout prototype at the 1914 Olympia Aero Show]]
The design was an equal-span single-bay biplane with staggered parallel-chord wings with raked wingtips and [[aileron]]s fitted to the upper and lower wings, which were rigged with about half a degree of [[dihedral (aircraft)|dihedral]], making them look almost straight when viewed from the front.  The wing section was one designed by Coanda which had been used for the wings of the [[Bristol T.B.8|Bristol Coanda Biplanes]].<ref name=Flight1914/>  The rectangular-section fuselage was an orthodox wire-braced wooden structure constructed from ash and spruce, with the forward section covered with aluminium sheeting and the rear section fabric covered.<ref name=Flight1914>[http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200434.html The 80 h.p. Bristol "Scout"], [[Flight International|''Flight'']], 25 April 1914, pp.434-6</ref>  It was powered by an 80&nbsp;hp (60&nbsp;kW) [[Gnome Lambda]] [[rotary engine]] enclosed in a [[cowling]] that had no open frontal area, although the bottom was cut away to allow cooling air to get to the engine.<ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |pages=2–3}}</ref> It had a rectangular balanced [[rudder]] with no fixed [[Vertical stabilizer|fin]] and split elevators attached to a non-lifting horizontal stabiliser. The fixed horizontal tail surfaces were outlined in steel tube with wooden ribs and the elevators constructed entirely of steel tube.<ref>Rebuilding Grandad's Scout [https://bristolscout.wordpress.com/2013/12/18/18-dec-1913-wednesday/ 18 Dec 1913. Wednesday] access date 28 March 2015</ref>

[[File:Revised Bristol Scout A.jpg|thumb|left|The revised Bristol Scout A in the late spring of 1914.]]
The first flight was made at Larkhill on 23 February 1914 by Busteed and it was then exhibited at the March 1914 Aero Show at [[Olympia, London|Olympia]] in London. After more flying at Larkhill the prototype, later referred to as the '''Scout A''', was returned to the factory at [[Filton]] and fitted with larger wings, increasing the [[chord (aircraft)|chord]] by six inches (15&nbsp;cm) and the span from 22&nbsp;ft (6.71&nbsp;m) to 24&nbsp;ft 7&nbsp;in (7.49&nbsp;m). These were rigged with an increased dihedral of {{frac|1|3|4}}°.  Other changes included a larger rudder, a new open-fronted cowling with six external stiffening ribs distributed in symmetrically uneven angles around the cowl's sides (especially when seen from "nose-on")<ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |page=5}}</ref> and fabric panel-covered wheels.  It was evaluated by the British military on 14 May 1914 at [[Farnborough Airfield|Farnborough]], when, flown by Busteed, the aircraft achieved an airspeed of 97.5&nbsp;mph (157&nbsp;km/h), with a stalling speed of 40&nbsp;mph (64&nbsp;kph)<ref name= JMB1>Bruce, J. M.[http://www.flightglobal.com/pdfarchive/view/1958/1958-1-%20-%200522.html The Bristol Scout Part 1], [[Flight International]], 26 September 1958, pp. 525–8</ref> The aircraft was then entered for the 1914 [[Aerial Derby]] but did not take part because the weather on the day of the race was so poor that Bristol did not wish to risk the aircraft. By this time two more examples (works nos. 229 and 230) were under construction and the prototype was sold to [[Baron Carbery|Lord Carbery]] for £400 without its engine.  Carbery fitted it with an 80&nbsp;hp [[Le Rhône 9C]] nine-cylinder rotary and entered it in the London–Manchester race held on 20 June but damaged the aircraft when landing at [[Castle Bromwich]] and had to withdraw. After repairs, including a modification of the undercarriage to widen the [[Axle track|track]], Carbury entered it in the London–Paris–London race held on 11 July but had to ditch the aircraft in the English Channel on the return leg; while in France, only one of the two fuel tanks had been filled by mistake. Carbury managed to land alongside a ship and escaped but the aircraft was lost.<ref>Barnes 1970, pp92–3</ref>

[[File:Scout B No-648.jpg|thumb|The second Scout B produced (s/n 648)]]
Numbers 229 and 230, later designated the '''Scout B''' when Frank Barnwell retrospectively gave type numbers to early Bristol aircraft, were identical to the modified Scout A, except for having half-hoop-style underwing skids and an enlarged rudder. Completed shortly after the outbreak of war in August 1914, they were requisitioned by the War Office. Given [[Royal Flying Corps]] serial numbers 644 and 648, one was allocated to [[No. 3 Squadron RAF|No. 3 Squadron]] and the other to [[No. 5 Squadron RAF|No. 5 Squadron]] for evaluation.<ref name="Barnes"/> Number 644 was damaged beyond repair on 12 November 1914 in a crash landing.

Impressed by the performance of the aircraft, the War Office ordered twelve examples on 5 November and the Admiralty ordered a further 24 on 7 November.<ref>Barnes 1970, p. 94</ref> The production aircraft, later called the '''Scout C''', differed from their predecessors mainly in constructional detail, although the cowling was replaced by one with a small frontal opening and no stiffening ribs, the top decking in front of the cockpit had a deeper curve and the aluminium covering of the fuselage sides extended only as far as the forward centre-section struts, aft of  which the decking was plywood.<ref name= JMB1/>

==Operational history==
[[File:Lanoe Hawker's No 1611 Bristol Scout C.jpg|thumb|The actual Scout C, [[Royal Flying Corps|RFC]] serial no. 1611, flown by [[Lanoe Hawker]] on 25 July 1915 in his Victoria Cross-earning engagement. This aircraft also had the rear-location engine oil tank of the earliest Scout Cs.]]
The period of service of the Bristol Scout (1914–1916) marked the genesis of the fighter aircraft as a distinct type and many of the earliest attempts to arm British [[tractor configuration]] aircraft with forward-firing guns were tested in action using Bristol Scouts. These began with the arming of the second Scout B, RFC number 648, with two rifles, one each side, aimed outwards and forwards to clear the propeller arc.<ref name="Barnes"/><ref>{{cite book |last=Bruce |first=J. M. |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |pages=3–4}}</ref>

Two of the Royal Flying Corps' early Bristol Scout C aircraft, numbers 1609 and 1611, flown by Captain [[Lanoe Hawker]] with [[No. 6 Squadron RAF|No. 6 Squadron RFC]], were in turn armed with a [[Lewis machine gun]] on the left side of the fuselage, almost identical to the manner of the rifles tried on the second Scout B. using a mount that Hawker had designed. When Hawker downed two German aircraft and forced off a third on 25 July 1915 over [[Battle of Passchendaele|Passchendaele]] and [[Zillebeke]] he was awarded the first [[Victoria Cross]] for the actions of a British military pilot in aerial combat.<ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |pages=5, 6}}</ref>

[[File:RNAS Bristol Scout D w-twin Lewis guns.jpg|thumb|left|180px|RNAS Bristol Scout D with twin unsynchronized Lewis guns fitted to mounts enabling them to be fired obliquely outside the arc of the propeller]]
Some of the 24 initial production Scout Cs for the RNAS, were armed with one (or occasionally two) Lewis machine guns, sometimes with the Lewis gun mounted on top of the upper wing centre section in the manner of the [[Nieuport 11]];  more common was a very dubious choice of placement by some RNAS pilots, in mounting the Lewis gun on the forward fuselage of their Scout Cs, just as if it were a synchronized weapon, firing directly forward and through the propeller arc, an action likely to result in serious damage to the propeller. The type of bullet-deflecting wedges that [[Roland Garros (aviator)|Roland Garros]] had tried on his [[Morane-Saulnier Type N]] monoplane were also tried on one of the RFC's last Scout Cs, No. 5303 but since this seemed to have also required the use of the Morane Type N's immense "casserole" spinner, which almost totally blocked cooling air from reaching the engine, the deflecting-wedge method was not pursued further with Bristol Scouts.<ref name="Barnes"/><ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |pages=10, 11}}</ref><ref>Shores and Rolfe 2001, pp. 10–11.</ref>

An RNAS Scout was the first landplane to be flown from a ship, when Flt. Lt. H. F. Towler flew No. 1255 from the flying deck of the seaplane carrier [[HMS Vindex (1915)|HMS ''Vindex'']] on 3 November 1915.<ref name= JMBII>Bruce, J. M.[http://www.flightglobal.com/pdfarchive/view/1958/1958-1-%20-%200540.html The Bristol Scout Pt II] [[Flight International]] 3 October 1958, pp. 544–6</ref> As an attempted defence against German [[airship]]s, some RNAS Scout Ds were equipped with [[Ranken Dart]]s, a [[flechette]] with 1&nbsp;lb (0.45&nbsp;kg) of explosive per projectile, released from a pair of vertical cylindrical containers under the pilot's seat, each containing 24 darts.<ref name="Bruce V">Bruce, J. M. [http://www.flightglobal.com/pdfarchive/view/1958/1958-1-%20-%200698.html The Bristol Scout Part V] [[Flight International]], 31 October 1958, pp.701–11</ref>  On 2 August 1916, Flt. Lt. C. T. Freeman flew a Scout from the deck of ''Vindex'' and attacked [[List of Zeppelins#LZ 53|Zeppelin L 17]] with Ranken Darts. None of the darts did any damage to the Zeppelin, and since Freeman's aircraft could not land on the ''Vindex'' and was too far from land for a safe return, he had to [[Water landing|ditch]] his Scout D after the attack.<ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros Publications |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |page=5}}</ref>

[[File:Vickers Challenger synchroniser (Bristol Scout).jpg|thumb|Vickers-Challenger synchroniser fitted to Bristol Scout - showing the linkage between the engine and the gun.]]
In March 1916, RFC Scout C No.5313 was fitted with a [[Vickers machine gun]], synchronised to fire through the propeller by the awkward [[Synchronization gear#The Vickers-Challenger gear|Vickers-Challenger synchronising gear]], the only gear available to the RFC. Six other Scouts, late Scout Cs and early Scout Ds, were also fitted with the same combination. Types using this gear (including the [[Royal Aircraft Factory B.E.12|B.E.12]], the [[Royal Aircraft Factory R.E.8|R.E.8]] and the [[Vickers F.B.19]]) all had the gun mounted on the port side of the fuselage. The attempt to use the gear for synchronising a centrally mounted gun on the Bristol Scout failed and tests, which continued at least until May 1916, resulted in the abandonment of the idea and no Vickers-armed Bristol Scouts were used in operations.<ref name="Barnes"/>

None of the RFC or RNAS squadrons operating the Bristol Scout were exclusively equipped with this aircraft and by the end of the summer of 1916, no new Bristol Scout aircraft were being supplied to the British squadrons of either service, the early fighter squadrons in RFC service being equipped instead with the [[Airco DH.2]] single-seat [[Pusher configuration]] fighter.  A small number of Bristol Scouts were sent to the Middle East (in Egypt, [[Mesopotamia]] and [[Palestine (region)|Palestine]]) in 1916. Others served in [[Macedonia (region)|Macedonia]] and with the RNAS in the Eastern Mediterranean. The last known Bristol Scout in military service was the former RNAS Scout D No. 8978 in [[Australia]], which was based at [[Point Cook]], near [[Melbourne]], as late as October 1926.

Once the Bristol Scouts were no longer required for frontline service they were reallocated to training units, although many were retained by senior officers as personal "runabouts".

==Variants==

===Scout A===

The single prototype aircraft.

===Scout B===
Two manufactured, identical to the modified Scout A except for having half-hoop-style underwing skids and an enlarged rudder.

===Type 1 Scout C===
[[File:RNAS Bristol Scout C with Rear Oil Tank.jpg|thumb|200px|A Bristol Scout C (No.1255) in RNAS service, with added  accommodation for the engine oil tank behind the cockpit.]]
Similar to the previous Scout B. These early Scout Cs  also had their main oil tank moved to a position directly behind the pilot's shoulders, requiring a raised rear dorsal fairing immediately behind the pilot's seat to accommodate it.<ref name="Barnes"/>

Following the initial run of 36 Scout C airframes (12 for RFC, 24 for RNAS) later Scout C production batches, consisting of 50 aircraft built for the RNAS and 75 for the RFC, changed the cowl to a flat-fronted longer-depth version able to house the alternate choice of a nine-cylinder 80&nbsp;hp [[Le Rhône 9C]] rotary engine when the Gnome Lambda was not used, and moved the oil tank forward to a position in front of the pilot for better weight distribution and more reliable engine operation. The later cowl for the remaining Scout C aircraft still had the small opening of the domed unit, but often had a small cutaway made to the lower rear edge of the cowl to increase the cooling effect, and to allow any unburned fuel/oil mix to drain away.<ref name="Barnes"/> A total of some 161 Scout C airframes were produced for the British military as a whole, with the transition to the Scout D standard taking place in a gradual progression of feature changes.

===Types 2, 3, 4 and 5 Scout D===
The last, and most numerous production version, the '''Scout D''', gradually came about as the result of a series of further improvements to the Scout C design.  One of the earliest changes appeared on seventeen of the 75 naval Scout Cs with an increase in the wing dihedral angle from {{frac|1|3|4}}° to 3° and other aircraft in the 75 aircraft naval production run introduced a larger span horizontal tail surfaces and a broadened-[[chord (aeronautics)|chord]] rudder, shorter-span ailerons and a large front opening for the cowl, much like that of Scout B but made as a "one-piece" ring cowl instead.<ref>{{cite book |last=Bruce |first=J. M. |authorlink= |title=Windsock Datafile No.44, The Bristol Scouts |year=1994 |publisher=Albatros |location=Berkhamsted, Herts, UK |isbn=0-948414-59-6 |page=8}}</ref>

The newer cowl was sometimes modified with a blister on the starboard lower side for more efficient exhaust-gas scavenging, as it was meant to house the eventual choice of the more powerful, nine-cylinder 100&nbsp;hp Gnôme [[Monosoupape engine|Monosoupape]] B2 rotary engine in later production batches, to improve the Scout D's performance.  Some 210 examples of the Scout D version were produced, with 80 of these being ordered by the RNAS and the other 130 being ordered by the Royal Flying Corps.<ref name="Barnes"/><ref>[http://www.fleetairarm.com/exhibit/Bristol-Scout-D/4-6-42.aspx "Bristol Scout D."] ''Fleet Air Arm Museum.'' Retrieved: 18 October 2011.</ref>

===Other variants===
* '''S.2A''' : Two-seat fighter version of the Scout D. Two were built as advanced training aircraft.<ref name="Barnes"/>

==Operators==
;{{UK}}
* [[Royal Flying Corps]]
{{col-begin}}
{{col-2}}
** [[No. 1 Squadron RAF|No. 1 Squadron RFC]]
** [[No. 2 Squadron RAF|No. 2 Squadron RFC]]
** [[No. 3 Squadron RAF|No. 3 Squadron RFC]]
** [[No. 4 Squadron RAF|No. 4 Squadron RFC]]
** [[No. 5 Squadron RAF|No. 5 Squadron RFC]]
** [[No. 6 Squadron RAF|No. 6 Squadron RFC]]
** [[No. 7 Squadron RAF|No. 7 Squadron RFC]]
** [[No. 8 Squadron RAF|No. 8 Squadron RFC]]
** [[No. 9 Squadron RAF|No. 9 Squadron RFC]]
** [[No. 10 Squadron RAF|No. 10 Squadron RFC]]
** [[No. 11 Squadron RAF|No. 11 Squadron RFC]]
** [[No. 12 Squadron RAF|No. 12 Squadron RFC]]
** [[No. 13 Squadron RAF|No. 13 Squadron RFC]]
** [[No. 14 Squadron RAF|No. 14 Squadron RFC]]
{{col-2}}
** [[No. 15 Squadron RAF|No. 15 Squadron RFC]]
** [[No. 16 Squadron RAF|No. 16 Squadron RFC]]
** [[No. 17 Squadron RAF|No. 17 Squadron RFC]]
** [[No. 18 Squadron RAF|No. 18 Squadron RFC]]
** [[No. 21 Squadron RAF|No. 21 Squadron RFC]]
** [[No. 24 Squadron RAF|No. 24 Squadron RFC]]
** [[No. 25 Squadron RAF|No. 25 Squadron RFC]]
** [[No. 30 Squadron RAF|No. 30 Squadron RFC]]
** [[No. 36 Squadron RAF|No. 36 Squadron RFC]]
** [[No. 47 Squadron RAF|No. 47 Squadron RFC]]
** [[No. 63 Squadron RAF|No. 63 Squadron RFC]]
** [[No. 65 Squadron RAF]]
** [[No. 111 Squadron RAF|No. 111 Squadron RFC]]
{{col-end}}
* [[Royal Naval Air Service]]
;{{AUS}}
* [[Australian Flying Corps]]
** [[No. 1 Squadron RAAF|No. 1 Squadron AFC]] in [[Egypt]] and [[Palestine (region)|Palestine]].
** [[No. 6 Squadron RAAF|No. 6 (Training) Squadron AFC]] in the [[United Kingdom]].
** Central Flying School AFC at [[Point Cook, Victoria]].
;{{flag|Greece|old}}
* [[Hellenic Navy]]

==Specifications (Bristol Scout D)==
[[File:Bristol Scout C British First World War fighter biplane drawing in RNAS markings.jpg|thumb|RNAS Bristol Scout C drawing, with RNAS ring style insignia, and "behind-the-shoulders" rearwards engine oil tank location of the earliest Scout Cs.]]
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop

|ref=Bristol Aircraft since 1910<ref name="Barnes"/>

|crew=one
|capacity=
|payload main=
|payload alt=
|length main=20 ft 8 in
|length alt=6.30 m
|span main=24 ft 7 in
|span alt=7.49 m
|height main=8 ft 6 in
|height alt=2.59 m
|area main=198 ft²
|area alt=18.40 m²
|airfoil=
|empty weight main=789 lb
|empty weight alt=358 kg
|loaded weight main=1,195 lb
|loaded weight alt=542 kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=

|engine (prop)=[[Le Rhône 9C]]
|type of prop=[[rotary engine|rotary piston engine]]
|number of props=1
|power main=80 hp
|power alt=60 kW
|power original=

|max speed main=94 mph
|max speed alt=151 km/h
|cruise speed main=
|cruise speed alt=
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range main=
|range alt=
|ceiling main=16,000 ft
|ceiling alt=4,900 m
|climb rate main=18 min 30 sec to 10,000 ft
|climb rate alt=18 min 30 sec to 3,048 m
|wing loading main=6.03 lb/ft²
|wing loading alt=29.43 kg/m²
|thrust/weight=
|power/mass main=0.067 hp/lb
|power/mass alt=0.11 kW/kg
|more performance=
* '''[[Combat endurance]]:''' 2½ hours
|armament=
* 1 × [[Lewis Gun|Lewis]] or [[Vickers machine gun]]
}}

==See also==
{{aircontent|
|related=

|similar aircraft=
* [[Pemberton-Billing P.B.9]]
* [[Royal Aircraft Factory B.S.1]]
* [[Sopwith Tabloid]]
* [[Avro 511]]

|lists=
* [[List of fighter aircraft]]

|see also=
}}

==References==
{{Reflist}}

===Bibliography===
{{Refbegin}}
* Barnes, C. H. ''Bristol Aircraft since 1910.'' London: Putnam, 1964.
* Barnes, C. H. ''Bristol Aircraft since 1910.'' (second ed.) London: Putnam, 1970. ISBN 0 370 00015 3
* Bruce, J. M. ''The Bristol Scouts'' (Windsock Datafile No.44).  Berkhamsted, Herts, UK: Albatros, 1994. ISBN 0-948414-59-6.
* Shores, Christopher and  Rolfe, Mark. ''British and Empire Aces of World War 1.'' Oxford, UK: Osprey, 2001. ISBN 978-1-84176-377-4.
{{Refend}}

==External links==
{{Commons category|Bristol Scout}}
* [http://www.theaerodrome.com/aircraft/gbritain/bristol_scout.html "TheAerodrome.com's" Bristol Scout page]
* [http://www.cbrnp.com/profiles/quarter1/scout.htm The Bristol Scout By Chris Banyai-Riepl]
* [http://bristolscout.wordpress.com/ Bristol Scout: Rebuilding Granddad's Aircraft (No. 1264 of the RNAS)]

{{Bristol aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Rotary-engined aircraft]]
[[Category:Bristol Aeroplane Company aircraft|Scout]]