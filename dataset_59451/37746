{{Use dmy dates|date=July 2013}}
{{good article}}
{{Use British English|date=October 2010}}
{{Infobox governor
| name = Sir Francis Bernard, 1st Baronet
| image = FrancisBernard.png
| order = 
| office = [[List of colonial governors of New Jersey|10º Governor of the Province of New Jersey]]
| term_start = 27 January 1758
| term_end = 4 July 1760
| monarch = [[George II of Great Britain|George II]]
| predecessor = [[John Reading (New Jersey)|John Reading]]
| successor = [[Thomas Boone (governor)|Thomas Boone]]
| office2 = [[Governor of the Province of Massachusetts Bay]]
| term_start2 = 2 August 1760
| term_end2 = 1 August 1769
| monarch2 = {{unbulleted list |[[George II of Great Britain|George II]] | [[George III of Great Britain|George III]]}}
| lieutenant2 = 
| predecessor2 = [[Thomas Hutchinson (governor)|Thomas Hutchinson]] (acting)
| successor2 = [[Thomas Hutchinson (governor)|Thomas Hutchinson]] (acting)
| birth_date = baptised {{birth-date|12 July 1712}}
| birth_place = [[Brightwell-cum-Sotwell]], [[Berkshire]], England
| death_date = {{death date and age|1779|06|16|1712|07|12|df=yes}}
| death_place = [[Nether Winchendon]], [[Buckinghamshire]], England
| party = 
| spouse = 
| profession = 
| religion = 
| signature = FrancisBernardSignature.png
}}

'''Sir Francis Bernard, 1st Baronet''' (bapt. 12 July 1712 – 16 June 1779) was a British colonial administrator who served as governor of the provinces of [[Province of New Jersey|New Jersey]] and [[Province of Massachusetts Bay|Massachusetts Bay]].  His uncompromising policies and harsh tactics in Massachusetts angered the colonists and were instrumental in the building of broad-based opposition within the province to the rule of Parliament in the events leading to the [[American Revolution]].

Appointed governor of New Jersey in 1758, he oversaw the province's participation in the later years of the [[French and Indian War]], and had a generally positive relationship with its legislature.  In 1760 he was given the governorship of Massachusetts, where he had a stormy relationship with the assembly.  Early actions turned the colony's populists against him, and his responses to protests against Parliament's attempts to tax the colonies deepened divisions.  After protests against the [[Townshend Acts]] in 1768, Bernard sought British Army troops be stationed in Boston to oversee the colonists.  He was recalled after the publication of letters in which he was critical of the colony.

After returning to England, he continued to advise the British government on colonial matters, calling for hardline responses to ongoing difficulties in Massachusetts that culminated in the 1773 [[Boston Tea Party]]. He suffered a stroke in 1771 and died in 1779, leaving a large family.

==Early life==
Francis was born in [[Brightwell-cum-Sotwell]], (then in [[Berkshire]], but part of [[Oxfordshire]] since 1974), England to the Rev. Francis and Margery (Winslowe) Bernard and was christened on 12 July 1712.<ref>Higgins, p. 1:173</ref>  His father died three years later.  His mother remarried, but died herself of [[smallpox]] in 1718.<ref>Higgins, pp. 1:174–176</ref>  He was thereafter probably raised by an aunt for several years, since his stepfather was forced by a failed courtship to flee to Holland.<ref>Higgins, pp. 1:177–178</ref>  His stepfather, Anthony Alsop, returned to Berkshire a few years later, and continued to play a role in the boy's upbringing.<ref>Higgins, pp. 1:178–179</ref>  Bernard's formal education began at [[Westminster School|Westminster]] in 1725, and he then spent seven years at Oxford, where [[Christ Church, Oxford|Christ Church]] granted him a master of arts in 1736.  He [[read law]] at the [[Middle Temple]] and was [[called to the bar]] in 1737, after only four years (instead of the typical seven) of study.<ref>Nicolson (2000), p. 25</ref>   He settled in [[Lincoln, England|Lincoln]], where he practiced law and took on a variety of municipal posts.  Among his neighbors in Lincoln were the Pownalls, who had one son (John) serving in the [[Colonial Office]], and another, [[Thomas Pownall|Thomas]], who went to the North American colonies in 1753 and was appointed governor of the [[Province of Massachusetts Bay]] in 1757.<ref>Nicolson (2000), pp. 29–41</ref>

Bernard married Amelia Offley, daughter of the sheriff of [[Derbyshire]], in December 1741, and the couple raised a large family: by 1757 the couple had eight living children.<ref>Higgins, pp. 1:193–219</ref><ref>Nicolson (2000), p. 34</ref>  Because his prospects for further income to support this large family were unlikely in Lincoln, he apparently decided to seek a posting in the colonies.<ref>Higgins, pp. 1:215–217</ref>  [[John Adams]] later described Bernard as "avaricious to a most infamous degree; needy at the same time, having a numerous family to provide for."<ref>Adams, p. 33</ref>

==Governor of New Jersey==
[[File:Proclamation for a General Thanksgiving Governor Francis Bernard.jpg|thumb|left|upright|Proclamation for a General [[Thanksgiving]], issued by Governor Bernard, November 1766]]
Bernard's wife was cousin to [[William Barrington, 2nd Viscount Barrington|Lord Barrington]], who became a [[Privy Council]]lor in 1755.<ref name=BarringtonBio>{{Cite EB1911|wstitle=Barrington, William Wildman Shute, 2nd Viscount}}</ref><ref>Higgins, p. 1:215</ref>  Probably through his connections to Barrington and the Pownalls, he secured an appointment as governor of the [[Province of New Jersey]] on 27 January 1758, a post that became available upon the death of [[Jonathan Belcher]].<ref>Higgins, p. 1:220</ref><ref>Nicolson (2000), p. 41</ref>   Leaving some of his children with relatives, the couple sailed for North America with four of their children, arriving at [[Perth Amboy, New Jersey|Perth Amboy]] on 14 June.<ref>Nicolson (2000), pp. 42–45</ref>

The colonies were in the middle of the [[French and Indian War]] at the time of Bernard's arrival.  He established a good working relationship with New Jersey's assembly, and was able to convince the province to raise troops and funds for the ongoing war effort.  He signed the [[Treaty of Easton]], an agreement between New Jersey and [[Pennsylvania Colony|Pennsylvania]] on one side, and a group of Indian tribes (the [[Lenape]] being of principal concern to New Jersey) fixing boundaries between colonial and Indian lands.  This effort was important, for it reduced raiding on the frontiers and made possible the reallocation of provincial military strength to the war with [[New France]].<ref>Nicolson (2000), p. 44</ref>  It and other agreements negotiated by Bernard extinguished all of the remaining [[Indian title]]s to New Jersey.  Negotiations with the Lenape also resulted in the establishment of the first formal Indian reservation, Brotherton, near present-day [[Indian Mills, New Jersey|Indian Mills]].  This reservation was only sparsely populated, and was abandoned in 1801 when its remaining inhabitants joined the [[Stockbridge Indians]] in upstate New York.<ref>Martinelli, pp. 70–71</ref>

==Governor of Massachusetts==
[[File:JamesOtisJr by Blackburn.jpg|thumb|right|[[James Otis Jr.]], portrait by [[Joseph Blackburn (painter)|Joseph Blackburn]]; he was one of Bernard's leading opponents.]]
Through the influence of his connections in the Colonial Office, Bernard was appointed governor of the [[Province of Massachusetts Bay]] in late 1759.<ref>Nicolson (2000), p. 45</ref>  Delays in communications and slow travel were such that Bernard did not arrive in Boston until 2 August 1760. Although initially warmly welcomed, his tenure in Massachusetts was difficult.  Bernard sought to vigorously enforce the [[Navigation Acts]], in part because crown officials (including the governor and the customs officials) received shares of the proceeds from the seizure of ships that were caught violating the acts.<ref name=G25 />  The legal actions involving these seizures were heard in a jury-less [[admiralty court]] before a Crown-appointed judge, and were extremely unpopular.<ref>Galvin, pp. 24–25</ref>   Bernard also made an early opponent of [[James Otis, Jr.]] by appointing Lieutenant Governor [[Thomas Hutchinson (governor)|Thomas Hutchinson]] to be chief justice of the province's highest court, a post that had been promised by several previous governors to [[James Otis, Sr.|Otis' father]].<ref>Galvin, pp. 22–23</ref>  Upset over the snub the younger Otis resigned his post as advocate general (i.e. the Crown's representative, equivalent to a government [[prosecutor]]) before the admiralty court, and devoted himself instead to arguing (sometimes ''[[pro bono]]'') on behalf of the merchants in defense of their ships.<ref>Galvin, pp. 24–32</ref>   These early actions during Bernard's tenure drew a clear dividing line between the "popular party" (exemplified by the Otises) opposed to British colonial policy and the "court party" (exemplified by Hutchinson) who supported it.<ref name=G25>Galvin, p. 25</ref>

Bernard's difficulties were compounded when, after the death in late 1760 of [[George II of Great Britain|King George II]], it became necessary to reissue [[writs of assistance]] to customs tax collectors.  These writs, which were essentially open-ended [[search warrant]]s, were judicially controversial and so unpopular that their issuance was later explicitly disallowed by the [[United States Constitution]].  Hutchinson, who approved the writs in one of his first acts as chief justice, saw his popularity fall, and Otis, who argued the writs violated the [[Rights of Englishmen]], gained in popularity.  He was elected to the provincial legislature in May 1761, where he was well placed to continue his attacks on Bernard's policies.<ref>Galvin, pp. 28–34</ref> In the 1761 session of the assembly Otis engineered the gift of [[Mount Desert Island]] to Bernard, a partially successful stratagem to divert Bernard's attention from ongoing customs seizures.<ref>Galvin, p. 42</ref>

{{quote box|width=40% | align=right | quote=O B[ernard]! Great thy Villainy has been!<br />Schem'd to destroy our Liberty and Peace:<br />The publick Eye attentively has seen<br />Thy base Endeavours, and has watch'd our Ease
| source=— Anonymous pamphlet, 1769<ref>Walett, p. 224</ref>}}
Bernard's unpopularity continued through other tax measures, including the [[Sugar Act]] (1763) and the [[Stamp Act 1765|Stamp Act]] (1765).  While the passage of both acts occasioned protest, the response to the Stamp Act included rioting in the streets, and united many factions in the province against the governor.<ref>Galvin, pp. 74–76, 89–108</ref>  In 1767 the passage by Parliament of the [[Townshend Acts]] again raised a storm of protest in the colonies.<ref>Walett, p. 217</ref>  In Massachusetts the provincial assembly issued [[Massachusetts Circular Letter|a circular letter]], calling on the other colonies to join it in a boycott of the goods subject to the Townshend taxes.<ref name=W218>Walett, p. 218</ref>  Bernard was ordered in April 1768 by [[Wills Hill, 1st Marquess of Downshire|Lord Hillsborough]], who had recently been appointed to the newly created office of [[Secretary of State for the Colonies|Colonial Secretary]], to dissolve the assembly if it failed to retract the letter.<ref>Knollenberg, p. 56</ref>  The assembly refused, and Bernard prorogued it in July.

Maier says that his letters to London greatly influenced officials there, but they "distorted" reality. "His misguided conviction that the 'faction' had espoused violence as its primary method of opposition, for example, kept him from recognizing the radicals' peace-keeping efforts....Equally dangerous, Bernard's elaborate accounts were sometimes built on insubstantial evidence."<ref>{{cite book |author=Pauline Maier |title=From Resistance to Revolution: Colonial Radicals and the Development of American Opposition to Britain, 1765–1776 |url=https://books.google.com/books?id=-aQuIbSA19YC&pg=PA151 |year=1973 |publisher=W.W. Norton |pages=151–52}}</ref> Warden argues that Bernard was careful not to explicitly ask London for troops, but his exaggerated accounts strongly suggested they were needed. In the fall of 1767 he warned about a possible insurrection in Boston any day, and his exaggerated report of one disturbance in 1768, "certainly had given Lord Hillsboro the impression that troops were the only way to enforce obedience in the town." Warden notes that other key British officials in Boston wrote London with the "same strain of hysteria."<ref>G. B. Warden, ''Boston 1689–1776'' (1970) pp 213-14</ref> Four thousand British Army troops arrived in Boston in October 1768, further heightening tensions.  Bernard was vilified in the local press, and accused of writing letters to the ministry that mischaracterized the situation.<ref name=W218 />  Although he was challenged to release those letters he refused.  Opposition agents in London were eventually able to acquire some of his letters, which reached members of the [[Sons of Liberty]] in April 1769.<ref>Walett, p. 219</ref>  They were promptly published by the radical ''[[Boston Gazette]]'', along with deliberations of the governor's council.  One letter in particular, in which Bernard called for changes to the Massachusetts charter to increase the governor's power by increasing the council's dependence on him, was the subject of particularly harsh treatment,<ref>Walett, pp. 220–221</ref> and prompted the assembly to formally request that "he might be forever removed from the Government of the Province." He was recalled to England, and Lieutenant Governor Hutchinson became acting governor.  When Bernard left Boston on 1 August, the town held an impromptu celebration, decorated the [[Liberty Tree]], and rang church bells.<ref>Walett, p. 222</ref>

His accomplishments in Massachusetts included the design of Harvard Hall at [[Harvard University]] and the construction of a summer estate on Pond Street in [[Jamaica Plain, Massachusetts|Jamaica Plain]].<ref>{{cite web |url=http://www.jphs.org/sources/a-brief-history-of-jamaica-plain.html |title=A Brief History of Jamaica Plain |publisher=Jamaica Plain Historical Society |accessdate=2012-08-24}}</ref>

==Return to England==
[[File:Nathaniel Dance Lord North.jpg|thumb|right|British Prime Minister [[Frederick North, Lord North]] (portrait by [[Nathaniel Dance-Holland|Nathaniel Dance]]) consulted Bernard on colonial affairs.]]
Upon his return to England, he asked for and received a hearing concerning the colonial petition against his rule.  The Privy Council in February 1770 considered the petition, and after deliberation dismissed all of the charges as "groundless, vexatious, and scandalous."<ref name=H2_209>Higgins, p. 2:209</ref><ref>Nicolson (2000), p. 206</ref>  Despite this vindication, Bernard resigned as governor in 1771.  He was confirmed in the ownership of Mount Desert Island, a recognition he had been seeking since it was awarded to him in 1761.<ref name=N210>Nicolson (2000), p. 210</ref>  Although he had been promised a baronetcy and a pension of £1,000 for his service, he learned after his return that the pension had been reduced to £500 (the baronetcy, of [[Nettleham]], was awarded at crown expense).<ref name=H2_210>Higgins, pp. 2:205, 210–211</ref>  His appeals on the matter were at first rejected, but when [[Frederick North, Lord North|Lord North]] became Prime Minister in 1770, the pension was raised, but shortly after replaced by an appointment as commissioner on the Board of Revenue for Ireland, which paid the same amount.<ref>Higgins, p. 2:213</ref>

Bernard became an advisor to the North administration on matters concerning the colonies.  He generally took a harder line than his predecessor Thomas Pownall, who advocated for colonial interests in Parliament.  Proposals he made in 1771 included ideas central to the 1774 [[Massachusetts Government Act]], which severely constrained colonial political power, including a council appointed by the governor rather than one elected by the assembly.<ref name=N210 />  Bernard may also have played a role in the difficulties [[Benjamin Franklin]] had in being recognized as a colonial agent; after Franklin's credentials were refused by the colonial secretary, he encountered Bernard in an antechamber.<ref>Nicolson (2000), p. 214</ref>  Biographer Colin Nicolson observes that Bernard's presence as an advisor to the ministry "cast a shadow over virtually ever American measure regarding Massachusetts" that the ministry considered, because of Bernard's role in breaking trust between the colonists and the London government and the subsequent radicalization of Massachusetts politics.<ref>Nicolson (2000), p. 215</ref>

In 1774, when the North government was considering how to respond to the [[Boston Tea Party]], Bernard published ''Select Letters on Trade and Government'', containing proposals on how to deal with the ongoing difficulties in the colonies. He proposed to reconcile the constitutional grievances of the British and radical Americans by the possible introduction of American representatives into the Parliament of Great Britain.<ref>[https://archive.org/stream/selectlettersont00bern#page/32/mode/2up Select Letters on the Trade and Government of America: pages 33-34]</ref>  In the ''Select Letters'', which included the essay ''Principles of Law and Polity'' which he drafted in 1764, he laid out a point-by-point exposition of his viewpoints concerning imperial governance.<ref name=PLP>http://teachingamericanhistory.org/library/index.asp?document=1050</ref>  Some of his ideas were enacted, notably those enshrined in the Massachusetts Government Act; the outrage in London even sparked the sympathetic colonial advocate Thomas Pownall to propose the closure of Boston's port, which was enacted in the [[Boston Port Act]].<ref>Nicolson (2000), p. 221–223</ref>

==Decline and death==
In late 1771 Bernard was bequeathed the manor at [[Nether Winchendon]] upon the death of a cousin to whom he had been close since childhood.  Combined with other uncertainties about where various family members would reside after he received the Irish appointment, the stress of the situation led Bernard to suffer a stroke.<ref>Higgins, p. 2:233</ref>  His mobility was impaired, but he took the waters at [[Bath, Somerset|Bath]], which appear to have helped his recovery.  He applied for permission to resign the Irish post, and settled first at the Nether Winchendon manor; in 1774 his resignation was accepted and his pension restored.<ref>Higgins, p. 2:234</ref>  He was well enough in 1772 to travel to Oxford, where he received an honorary [[Doctor of Civil Law]] from his alma mater, Christ Church.<ref>Higgins, p. 2:235</ref>  Because of his health he moved later in 1772 to a smaller house in nearby [[Aylesbury]].  He died on 16 June 1779, after an epileptic seizure, at Nether Winchendon;<ref>Nicolson (2000), p. 236</ref> his grave is in St Mary's churchyard. [[Aylesbury]].<ref>{{cite web |url= http://www.findagrave.com/cgi-bin/fg.cgi?age=gr&GRid=114170412
 |title=Findagrave entry for Sir Francis Bernard|accessdate=1 January 2017}}</ref>

==Legacy==
Bernard never believed the difficulties he had in Massachusetts were personal: instead of accepting some responsibility, he blamed his problems on the policies emanating from London that he was instructed to implement.<ref>Nicolson (2000), p. 224</ref>  [[John Adams]] wrote that Bernard's "antagonistic reports" of matters in Massachusetts were instrumental in turning British government policymakers against colonial interests.<ref>Nicolson (2000), p. 231</ref> Bernard's name headed a list drawn up in Massachusetts after the [[American Revolutionary War]] broke out of "notorious conspirators against the government", and most of his property there was confiscated.<ref>Nicolson (2000), pp. 235–236</ref>  Mount Desert Island was not entirely taken; Bernard's son John, who resided in Maine during the war and sided with the victorious Americans, was able to receive Massachusetts title to half of the island.<ref name=N237>Nicolson (2000), p. 237</ref>

Upon the election of James Bowdoin to be [[Governor of Massachusetts]] in 1786, Reverend William Gordon in his sermon warned Bowdoin that he ignored the state's legislature at his peril, as Bernard had.<ref name=N237 />

Vealtown, New Jersey, a town first settled around 1715 and located in [[Bernards Township, New Jersey|Bernards Township]], was renamed [[Bernardsville]] in Bernard's honour in 1840.<ref>Lurie, p. 74</ref>  [[Bernardston, Massachusetts]] was incorporated during his Massachusetts administration and is named for him.<ref>Nason and Varney, p. 146</ref>  Bernard also named [[Berkshire County, Massachusetts]] (after his county of birth) and [[Pittsfield, Massachusetts]] (after British Prime Minister [[William Pitt the Elder|William Pitt]]).<ref>Smith, p. 132</ref>

==Notes==
{{reflist|2}}

==References==
* {{cite book |last=Adams |first=John |author2=Carey, George (ed) |title=The Political Writings of John Adams |publisher=Regnery Publishing |year=2001 |location=Washington, DC |isbn=9780895262929 |oclc=248150786}}
* {{cite book |last=Barrington |first=Viscount William Wildman |author2=Bernard, Sir Francis |title=The Barrington-Bernard Correspondence and Illustrative Matter, 1760–1770 |publisher=Harvard University Press |year=1912 |location=Cambridge, MA |url=https://books.google.com/books?id=NzN5AAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0 |oclc=2433914}}
* {{cite book |last=Galvin |first=John |title=Three Men of Boston |publisher=Thomas Y. Crowell |location=New York |year=1976 |isbn=9780690010183 |oclc=1530708}}
* {{cite book |last=Higgins |first=Sophia |title=The Bernards of Abington and Nether Winchendon: a Family History |publisher=Longmans, Green |year=1903 |location=London and New York |oclc=4277811 |url=https://books.google.com/books?id=LeHZgdEtzPcC&pg=PA173#v=onepage&f=false}} ([https://books.google.com/books?id=fQuYIGQaPtoC&printsec=frontcover Volume 2])
* {{cite book |last=Knollenberg |first=Bernhard |title=Growth of the American Revolution, 1766–1775 |location=New York |publisher=Free Press |year=1975 |isbn=0-02-917110-5}}
* {{cite book |last=Lurie |first=Maxine (ed) |title=Encyclopedia of New Jersey |location=New Brunswick, NJ |publisher=Rutgers University Press |year=2004 |isbn=9780813533254 |oclc=53131560}}
* {{cite book |last=Martinelli |first=Patricia |title=New Jersey Ghost Towns: Uncovering the Hidden Past |publisher=Stackpole Books |location=Mechanicsburg, PA |year=2012 |isbn=9780811709101 |oclc=755699329}}
* {{cite book |last=Nason |first=Elias |author2=Varney, George |title=A Gazetteer of the State of Massachusetts |publisher=B. B. Russell |year=1890 |url=https://books.google.com/books?id=eFgoAAAAYAAJ&lpg=PA146&pg=PA146#v=onepage&f=false |oclc=35077879 |location=Boston, MA}}
* {{cite book |last=Nicolson |first=Colin |title=The "Infamas Govener" Francis Bernard and the Origins of the American Revolution |publisher=Northeastern University Press |location=Boston, MA |year=2000 |isbn=978-1-55553-463-9 |oclc=59532824}}
* {{cite book |last=Smith (ed) |first=J. E. A |title=The History of Pittsfield (Berkshire County), Massachusetts, Volume 1 |publisher=Lee and Shepard |location=Boston, MA |year=1869 |url=https://books.google.com/books?id=Nlc1AAAAIAAJ&lpg=PA131&pg=PA132#v=onepage&f=false |oclc=3578710}}
* {{cite journal |last=Walett |first=Francis |title=Governor Bernard's Undoing: An Earlier Hutchinson Letters Affair |journal=The New England Quarterly |date=June 1965 |volume=38 |issue=2 |jstor=363591 |pages=217–226}}
* {{cite journal |last=Nicolson |first=Colin |title=Governor Francis Bernard, the Massachusetts Friends of Government, and the Advent of the Revolution |journal=Proceedings of the Massachusetts Historical Society |issue=Third Series, Volume 103 |year=1991 |pages=24–113 |jstor=25081034}}
* The Lost Bernardston Charter – http://blog.t3consortium.com/the-lost-bernardston-charter-of-1760/

===Primary sources===
* Nicolson, Colin, ed. ''The Papers of Francis Bernard, Governor of Colonial Massachusetts, 1760–69'', 6 vols. Colonial Society of Massachusetts and Univ. of Virginia Press, Boston: 2007-. Vols. 1 to 5 published to date.
** vol. 5: 1768–1769. (2015). xxvi, 460 pp.

{{commons category|Sir Francis Bernard, 1st Baronet}}

{{S-start}}
{{s-off}}
{{succession box
| before = [[John Reading (New Jersey)|John Reading]]<br /><small>(President Of Council)</small>
| title = [[List of colonial governors of New Jersey|Governor of the Province of New Jersey]]
| years = 1758–1760
| after = [[Thomas Boone (governor)|Thomas Boone]]
}}
{{succession box
| before = [[Thomas Hutchinson (governor)|Thomas Hutchinson]]<br /><small>(acting)</small>
| title = [[Governor of Massachusetts|Governor of the Province of Massachusetts Bay]]
| years = 2 August 1760 – 1 August 1769
| after = [[Thomas Hutchinson (governor)|Thomas Hutchinson]]<br /><small>(acting)</small>
}}
{{s-reg|gb-bt}}
{{s-new}}
{{s-ttl |title=[[Bernard baronets|Baronet]]<br />'''(of Nettleham) | years='''1769–1779}}
{{s-aft|after=[[Sir John Bernard, 2nd Baronet of Nettleham|John Bernard]]}}
{{S-end}}

{{Governors of New Jersey}}
{{Governors of Massachusetts}}

{{Authority control}}

{{DEFAULTSORT:Bernard, Francis}}
[[Category:1712 births]]
[[Category:1779 deaths]]
[[Category:Alumni of Christ Church, Oxford]]
[[Category:Baronets in the Baronetage of Great Britain]]
[[Category:Colonial governors of New Jersey]]
[[Category:Colonial governors of Massachusetts]]
[[Category:People from South Oxfordshire (district)]]
[[Category:English barristers]]
[[Category:Members of the Middle Temple]]
[[Category:People of colonial Massachusetts]]
[[Category:People admitted to the practice of law by reading law]]