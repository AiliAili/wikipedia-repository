{{Infobox journal
| italic title = force
| title = International Journal of Language & Communication Disorders
| cover = [[File:International Journal of Language & Communication Disorders.jpg|150px]]
| discipline = [[Speech and language pathology]]
| formernames = European Journal of Disorders of Communication
| abbreviation = Int. J. Lang. Commun. Disord.
| editor = Katerina Hilari, Nicola Botting
| publisher = [[Wiley-Blackwell]]
| country =
| history = 1966-present
| frequency = Bimonthly
| impact = 1.946
| impact-year = 2011
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-6984
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-6984/issues
| link2-name = Online archive
| ISSN = 1368-2822
| eISSN = 1460-6984
| LCCN = 98657724
| CODEN = IJLDFI
| OCLC = 38452377
}}
The '''''International Journal of Language & Communication Disorders''''' is a [[Peer review|peer-reviewed]] [[healthcare journal|medical journal]] that covers topics relevant to speech and language [[communication disorder|disorders]] and speech and language therapy. Article types published are research reports, reviews, discussions, and clinical fora. It is the official journal of the [[Royal College of Speech and Language Therapists]]. The journal is published by [[Wiley-Blackwell]] and [[Editor-in-chief|edited]] by Katerina Hilari and Nicola Botting ([[City University London]]). The journal was established in 1966<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-6984/homepage/ProductInformation.html|title=About the journal |publisher=[[Wiley-Blackwell]] |work=International Journal of Language & Communication Disorders }}</ref> and has a 2011 [[impact factor]] of 1.946.<ref name=WoS>{{cite book |year=2012 |chapter=International Journal of Language & Communication Disorders |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-25 |series=[[Web of Science]] |postscript=.}}</ref> The journal is available online and is published in paper format 6 times a year.

The movie ''[[The King's Speech]]'' has caused much awareness about [[stuttering]]. In response to this, a virtual issue of the journal was produced on the theme of ''The King's Speech'' and stuttering research.<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-6984/homepage/the_king_s_speech_virtual_issue.htm|title=The King's Speech and Stuttering Research|publisher=[[Wiley-Blackwell]] |work=International Journal of Language & Communication Disorders }}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291460-6984}}

{{DEFAULTSORT:International Journal of Language and Communication Disorders}}
[[Category:English-language journals]]
[[Category:Audiology journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1966]]
[[Category:Communication journals]]