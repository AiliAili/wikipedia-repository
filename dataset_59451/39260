{{Use Canadian English|date=January 2013}}
{{Use dmy dates|date=January 2013}}
A '''Canadian postal code''' is a six-character [[String (computer science)|string]] that forms part of a [[mail|postal]] [[address (geography)|address]] in [[Canada]].<ref name=canadapostglossary_postalcode>{{cite web | title = Canada Postal Guide - Addressing Guidelines | publisher = Canada Post | date=11 January 2016 | url = https://www.canadapost.ca/tools/pg/manual/PGaddress-e.asp#1449273 | accessdate=11 January 2016}}</ref> Like [[UK postcodes|British]] and [[Postal codes in the Netherlands|Dutch]] postcodes, Canada's [[postal code]]s are [[alphanumeric]]. They are in the format ''A1A 1A1'', where ''A'' is a letter and ''1'' is a digit, with a space separating the third and fourth characters.  As of September 2014, there were 855,815 postal codes<ref name=GreatData.com>{{cite web|title=Canadian Postal Code Database|url=http://greatdata.com/canada-postalcodes-lat-long|website=GreatData.com|accessdate=27 September 2014|ref=CanadaPost.ca}}</ref> using ''Forward Sortation Areas'' from A0A in [[Newfoundland (island)|Newfoundland]] to Y1A in the [[Yukon]].

[[Canada Post]] provides a free postal code look-up tool on its website,<ref>{{cite web|url=https://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf|title=Canada Post - Find a Postal Code|author=Canada Post|accessdate=11 April 2016}}
</ref> via its mobile application,<ref>{{cite web | url=http://www.canadapost.ca/cpo/mc/personal/tools/mobileapp/default.jsf | publisher=Canada Post | title=Mobile Apps}}</ref> and sells hard-copy directories and [[CD-ROM]]s. Many vendors also sell validation tools, which allow customers to properly match addresses and postal codes. Hard-copy directories can also be consulted in all post offices, and some libraries.

When writing out the postal address for a location within Canada, the postal code follows the [[Canadian postal abbreviations for provinces and territories|abbreviation for the province or territory]].

== History ==

=== City postal zones ===
Numbered postal zones were first used in [[Toronto]] in 1925.<ref>{{cite news | title=Numbers Designate New Postal Zones | newspaper=[[Toronto Star]] | date=23 July 1925 | page=3}}</ref> Mail to a Toronto address in zone 5 would be addressed in this format:

 37 Bloor Street West
 Toronto 5, Ontario<ref>[https://books.google.com/books?id=JtY9AAAAYAAJ&q=%22Toronto+5,+Ontario%22&dq=%22Toronto+5,+Ontario%22&hl=en&sa=X&ved=0ahUKEwi2nK_nwIbLAhUFxxQKHXb_BakQ6AEIQDAC ''New dimensions in curriculum development: proceedings''], Ontario Curriculum Institute, 1966, page 110</ref>

As of 1943, Toronto was divided into 14 zones, numbered from 1 to 15, except that 7 and 11 were unused, and there was a 2B zone.<ref>[https://books.google.com/books?id=uqoLAAAAMAAJ&q=%22Toronto+2B,+Ontario%22&dq=%22Toronto+2B,+Ontario%22&hl=en&sa=X&ved=0ahUKEwjJtoTHwYbLAhXEvxQKHT_CBnUQ6AEIqAEwGA ''The Corpus Almanac of Canada''], Volume 4, Corpus Publishers Services Limited, 1968, page 325</ref>

By the early 1960s, other cities in Canada had been divided into postal zones, including [[Quebec City|Quebec]], [[Montreal]], [[Ottawa]], [[Winnipeg]] and [[Vancouver]] as well as Toronto.<ref>[https://news.google.com/newspapers?id=qUIyAAAAIBAJ&sjid=LOYFAAAAIBAJ&pg=4430%2C5788916 Postal Zone Numbers Speed Big City Mail], ''[[Ottawa Citizen]]'', February 25, 1963</ref>  For example, an address in Vancouver would be addressed as:

 804 Robson Street,
 Vancouver 1, B. C<ref>[https://books.google.com/books?id=mfhAAQAAIAAJ&dq=%22Toronto+100%2C+Ontario%22&focus=searchwithinvolume&q=%22Toronto+185%2C+Ontario%22 ''The Canadian Modern Language Review''], Volume 29, Volume 4, Corpus Publishers Services Limited, 1968, page 325</ref>

In the late 1960s, however, the Post Office began implementing a three-digit zone number scheme in major cities to replace existing one and two-digit zone numbers, starting in Montreal, Toronto and Vancouver.<ref>[http://parl.canadiana.ca/view/oop.debates_HOC2801_10/448?r=0&s=1 ''House of Commons Debates''], July 2, 1969, Official Report, Volume 10, E. Cloutier, Queen's Printer and Controller of Stationery, 1969, page 10742</ref> For example, an address in Metropolitan Toronto would be addressed as:

 1253 Bay Street
 Toronto 185, Ontario<ref>[https://books.google.com/books?id=mfhAAQAAIAAJ&dq=%22Toronto+100%2C+Ontario%22&focus=searchwithinvolume&q=%22Toronto+185%2C+Ontario%22 ''The Canadian Modern Language Review''], Volume 29, Volume 4, Corpus Publishers Services Limited, 1968, page 325</ref>

Toronto's renumbering took effect 1 May 1969, accompanied by an advertising campaign under the slogan "Your number is up".<ref>[http://parl.canadiana.ca/view/oop.debates_HOC2801_10/710?r=0&s=1 ''House of Commons Debates''], July 8, 1969, Official Report, Volume 10, E. Cloutier, Queen's Printer and Controller of Stationery, 1969, page 11004</ref> However, with impending plans for a national postal code system, Postmaster General [[Eric Kierans]] announced that the Post Office would begin cancelling the new three-digit city zone system. Companies changed their mail addressing at their own expense, only to find the new zoning would prove to be short-lived.<ref>{{cite news | title=Costs of postal zone changes hit some companies second time | date=4 June 1969 | first=Terrence | last=Belford | page=B4 | newspaper=[[The Globe and Mail]] }}</ref>
<!-- TO DO: determine earliest use of postal zones (1925?); also determine if zones were used in 1961 in five cities: [[Montreal]], Toronto, Ottawa, [[Winnipeg, Manitoba|Winnipeg]], and [[Vancouver]], and later [[London, Ontario|London]], [[Ontario]]. -->

=== Planning ===
As the largest Canadian cities were growing in the 1950s and 1960s, the volumes of mail passing through the country's postal system also grew, reaching billions by the 1950s, and tens of billions by the mid-1960s. Consequently, it was becoming progressively more difficult for employees who handsorted mail to memorize and keep track of all the individual letter-carrier routes within each city.

New technology that allowed mail to be delivered faster also contributed to the pressure for these employees to properly sort the mail. Canada was one of the last Western countries to get a nationwide postal code system.<ref>
{{cite news
 |title=Cote denies conflict between ITT contract and personnel exchange with Post Office
 |last=Rolfe
 |first=John
 |page=B3
 |newspaper=[[The Globe and Mail]]
 |date=4 March 1972
 }}
</ref>

A report tabled in the House of Commons in 1969 dealt with the expected impact of "environmental change" on the Post Office operations over the following 25&nbsp;years. A key recommendation was the "establishment of a task force to determine the nature of the automation and mechanization the Post Office should adopt, which might include design of a postal code".<ref>
{{cite news
 |title=Technical advances in communications will erode Post Office work, report says
 |page=A3
 |newspaper=[[The Globe and Mail]]
 |date=6 May 1969
 }}
</ref><ref name="history1">
{{cite web
 |url=http://www.civilization.ca/cpm/chrono/ch1971ae.html
 |archiveurl=https://web.archive.org/web/20070930055242/http://www.civilization.ca/cpm/chrono/ch1971ae.html
 |archivedate=2007-09-30
 |title=A Chronology of Canadian Postal History: The Postal Code (Archived version)
 |author=[[Canadian Postal Museum]]
 |date=16 September 2001
 |accessdate=7 January 2007
 }}
</ref><!-- GA - Uncited material cloaked ----- Actual system design was carried out by a retired Canadian Army officer named William JA Groom.{{Fact|date=January 2009}} -->

=== Implementation ===
In December 1969, Communications Minister [[Eric Kierans]] announced that a six-character postal code would be introduced, superseding the three-digit zone system.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=iaIyAAAAIBAJ&sjid=-uwFAAAAIBAJ&pg=886%2C3315014
 |title=Postal coding in '70
 |page=50
 |first= 
 |last= 
 |newspaper=[[Ottawa Citizen|Saturday Citizen]]
 |date=24 December 1969
 }}</ref>
He later tabled a report in February 1970, entitled "A Canadian Public Address Postal Coding System", submitted by the firm of Samson, Belair, Simpson, Riddell Inc.<ref>
{{cite news
 |title=To speed sorting and delivery: Proposed national postal code system for Canada
 |page=5
 |url=https://news.google.com/newspapers?id=GvgvAAAAIBAJ&sjid=FkkDAAAAIBAJ&pg=2957%2C1220661
 |newspaper=''[[The Stanstead Journal]]''
 |date=26 February 1970
 }}</ref>

The introduction of the postal code with a test in [[Ottawa]] on 1 April 1971.<ref>
{{cite news
 |title=Postal code service for Canada to be inaugurated on April first
 |page=5
 |url=https://news.google.com/newspapers?id=OPgvAAAAIBAJ&sjid=FkkDAAAAIBAJ&pg=2963%2C3400343
 |newspaper=''[[The Stanstead Journal]]''
 |date=18 March 1971
 }}
</ref>
Coding of Ottawa was followed by a provincial-level rollout of the system in [[Manitoba]], and the system was gradually implemented in the rest of the country from 1972 to 1974, although the nationwide use of the code by the end of 1974 was only 38.2 per cent.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=LhQyAAAAIBAJ&sjid=2qEFAAAAIBAJ&pg=5781%2C1205627
 |title=Will 'gentle persuasion' aid postal code?
 |page=9
 |first=Guy
 |last=Demarino
 |newspaper=[[Montreal Gazette]]
 |date=7 January 1975
 }}</ref>

The introduction of such a code system allowed Canada Post to speed up easily, as well as simplify, the flow of mail in the country, with sorting machines being able to handle 26 640 objects an hour.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=yNhRAAAAIBAJ&sjid=GG8DAAAAIBAJ&pg=2430%2C3385833
 |title=New postal code for all of Canada to speed delivery and avoid errors
 |page=19
 |first= 
 |last= 
 |newspaper=''L'Avenir''
 |date=30 January 1973
 }}</ref>  However, when the automated sorting system was initially conceived, the [[Canadian Union of Postal Workers]] and other relevant unions objected to it, mainly because the wages of those who ran the new automated machines were much lower than those who had hand-sorted mail.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=XrYyAAAAIBAJ&sjid=jOwFAAAAIBAJ&pg=1584%2C2090904
 |title=Boycotts Ordered: Postmen Declare Automation War
 |page=113
 |first= 
 |last= 
 |newspaper=[[Ottawa Citizen|Saturday Citizen]]
 |date=5 June 1974
 }}</ref>

The unions ended up staging job action and public information campaigns, with the message that they did not want people and business to use postal codes on their mail.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=nog1AAAAIBAJ&sjid=9KEFAAAAIBAJ&pg=1409%2C1757737
 |title=Quick mail only without postal codes
 |page=5
 |first=Irwin
 |last=Block
 |newspaper=[[Montreal Gazette]]
 |date=7 June 1974
 }}</ref> The union declared 20 March 1975 National "Boycott the Postal Code" Day, also demanding a reduction in the work week from 40 to 30 hours.<ref>{{cite news
 |url=https://news.google.com/newspapers?id=-woyAAAAIBAJ&sjid=16EFAAAAIBAJ&pg=2367%2C534713
 |title=Postal workers vote on contract demands
 |page=5
 |first=Joan
 |last=Boaden
 |newspaper=[[Montreal Gazette]]
 |date=19 March 1975
 }}
</ref> The boycott was called off in February 1976.<ref>
{{cite news
 |url=https://news.google.com/newspapers?id=lZkuAAAAIBAJ&sjid=tqEFAAAAIBAJ&pg=947%2C277033
 |title=Postal union chiefs claim pact violated, threaten to retaliate
 |page=3
 |first=Michelle
 |last=Morisette
 |newspaper=[[Montreal Gazette]]
 |date=17 September 1976
 }}
</ref> <!-- GA - Uncited material cloaked ----- Typists also criticized the decision to make the code alphanumeric, arguing that an all-number code was far easier to type. Canada Post defended this decision by stating that the all-number system in the United States had failed as there were too few combinations.{{Fact|date=January 2009}} -->

One 1975 advertisement in the Toronto magazine ''Byliner'' generated controversy by showing a man writing a postal code on the bottom of a [[G-string|thonged]] woman with the following ditty: 
<blockquote>"We're not 'stringing' you along,<br>                                                 Use postal codes — you'll 'thing our 'thong',<br>                                                    Don't be cheeky — you've all got 'em<br>                                             Please include them on the bottom."<ref name="rodriguez">[http://parl.canadiana.ca/view/oop.debates_HOC3001_07/284?r=0&s=1 ''House of Commons Debates''], Official Report, Volume 7, E. Cloutier, Queen's Printer and Controller of Stationery, 1975, page 6826</ref></blockquote> 
The advertisement was denounced as "sexist garbage" in the [[House of Commons of Canada|House of Commons]] by [[New Democratic Party of Canada|NDP]] MP [[John Rodriguez]], prompting an apology from [[Postmaster General of Canada|Postmaster General]] [[Bryce Mackasey]].<ref name="rodriguez"></ref>

== Components of a postal code ==
[[File:Components of a Canadian postal code.png|left|140px]]

=== Forward sortation areas ===
A ''forward sortation area'' (FSA) is a geographical region in which all postal codes start with the same three characters.<ref>{{cite web | title = NDG Presort Online Training | work = NDG | publisher = [[Canada Post]]| url = http://www.canadapost.ca/business/ndg/glossary-e.asp |accessdate=23 September 2008 }}</ref> The first letter of an FSA code denotes a particular "postal district", which, outside of [[Quebec]] and [[Ontario]], corresponds to an entire [[Provinces and territories of Canada|province or territory]].

Owing to Quebec's and Ontario's large populations, those two provinces are sub-divided into three and five postal districts respectively, and each has at least one urban area so populous that it has a dedicated postal district ("''H''" for the [[Montréal (region)|Montréal region]], and "''M''" for [[Toronto]]). On the other hand, the low populations in [[Nunavut]] and the [[Northwest Territories]] (NWT) mean that even after Nunavut separated from the Northwest Territories and became its own territory in 1999, they continue to share a postal district.

The digit specifies if the FSA is urban or rural. A zero indicates a wide-area rural region, while all other digits indicate urban areas. The second letter represents a specific rural region, an entire medium-sized city, or a section of a major metropolitan area.

<div style="width:400px; float: right; text-align: center; margin-left: 1em; padding: 3px; border: 1px solid silver;">
<div style="width:400px; background:#ddd; margin-bottom:3px;">''Map of Canadian postal districts''</div>
{{Image label begin|image=Canadian postal district map (without legends).svg|width=400|float=right|caption=Map of postal districts in Canada. Click on a letter to go to a table of forward sortation areas beginning with that letter.}}
{{Image label|x=370|y=194|scale=400/400|text=[[List of A postal codes of Canada|<span title="Newfoundland and Labrador" style="font-size: large; color: black;">'''A'''</span>]]}}
{{Image label|x=371|y=274|scale=400/400|text=[[List of B postal codes of Canada|<span title="Nova Scotia" style="font-size: large; color: black;">'''B'''</span>]]}}
{{Image label|x=346|y=251|scale=400/400|text=[[List of C postal codes of Canada|<span title="Prince Edward Island" style="font-size: large; color: black;">'''C'''</span>]]}}
{{Image label|x=336|y=269|scale=400/400|text=[[List of E postal codes of Canada|<span title="New Brunswick" style="font-size: large; color: black;">'''E'''</span>]]}}
{{Image label|x=306|y=246|scale=400/400|text=[[List of G postal codes of Canada|<span title="Eastern Quebec" style="font-size: large; color: black;">'''G'''</span>]]}}
{{Image label|x=320|y=297|scale=400/400|text=[[List of H postal codes of Canada|<span title="Metropolitan Montreal" style="font-size: large; color: black;">'''H'''</span>]]}}
{{Image label|x=279|y=210|scale=400/400|text=[[List of J postal codes of Canada|<span title="Western and Northern Quebec" style="font-size: large; color: black;">'''J'''</span>]]}}
{{Image label|x=280|y=302|scale=400/400|text=[[List of K postal codes of Canada|<span title="Eastern Ontario" style="font-size: large; color: black;">'''K'''</span>]]}}
{{Image label|x=296|y=311|scale=400/400|text=[[List of L postal codes of Canada|<span title="Central Ontario" style="font-size: large; color: black;">'''L'''</span>]]}}
{{Image label|x=291|y=328|scale=400/400|text=[[List of M postal codes of Canada|<span title="Metropolitan Toronto" style="font-size: large; color: black;">'''M'''</span>]]}}
{{Image label|x=245|y=318|scale=400/400|text=[[List of N postal codes of Canada|<span title="Western Ontario" style="font-size: large; color: black;">'''N'''</span>]]}}
{{Image label|x=218|y=258|scale=400/400|text=[[List of P postal codes of Canada|<span title="Northern Ontario" style="font-size: large; color: black;">'''P'''</span>]]}}
{{Image label|x=164|y=221|scale=400/400|text=[[List of R postal codes of Canada|<span title="Manitoba" style="font-size: large; color: black;">'''R'''</span>]]}}
{{Image label|x=127|y=214|scale=400/400|text=[[List of S postal codes of Canada|<span title="Saskatchewan" style="font-size: large; color: white;">'''S'''</span>]]}}
{{Image label|x=88|y=205|scale=400/400|text=[[List of T postal codes of Canada|<span title="Alberta" style="font-size: large; color: white;">'''T'''</span>]]}}
{{Image label|x=45|y=186|scale=400/400|text=[[List of V postal codes of Canada|<span title="British Columbia" style="font-size: large; color: white;">'''V'''</span>]]}}
{{Image label|x=138|y=154|scale=400/400|text=[[List of X postal codes of Canada|<span title="Nunavut and NWT" style="font-size: large; color: white;">'''X'''</span>]]}}
{{Image label|x=37|y=120|scale=400/400|text=[[List of Y postal codes of Canada|<span title="Yukon" style="font-size: large; color: white;">'''Y'''</span>]]}}</div>
{{Image label end}}
[[List of postal codes in Canada|A directory of FSAs]] is provided, divided into separate articles by postal district. Individual FSA lists are in a tabular format, with the numbers (known as ''zones'') going across the table and the second letter going down the table.

The FSA lists specify all communities covered by each rural FSA. Medium-sized cities may have one dedicated FSA, while larger cities have more than one FSA within their limits.

For FSAs spanning more than one city, the city which is allocated the most codes in each such FSA is listed. For cities with a small number of FSAs (but more than one), the lists specify the relative location of each FSA in those cities. For cities with a large number of FSAs, applicable neighbourhoods and boroughs are specified.

=== Local delivery units ===
The last three characters denote a ''local delivery unit'' (LDU).<ref name=canadapostglossary_postalcode />  An LDU denotes a specific single address or range of addresses, which can correspond to an entire small town, a significant part of a medium-sized town, a single side of a city block in larger cities, a single large building or a portion of a very large one, a single (large) institution such as a university or a hospital, or a business that receives large volumes of mail on a regular basis.

LDUs ending in zero correspond to postal facilities, from [[post office]]s and small franchised retail postal outlets all the way up to sortation plants. In urban areas, LDUs may be specific postal carriers' routes. In rural areas where direct door-to-door delivery is not available, an LDU can describe a set of post office boxes or a [[rural route]].  LDU '''9Z9''' is used exclusively for [[Freepost|Business Reply Mail]]. In rural FSAs, the first two characters are usually assigned in alphanumerical order by the name of each community.

LDU '''9Z0''' refers to large regional distribution centre facilities, and is also used as a placeholder, appearing in some regional postmarks such as the "K0H 9Z0" which formerly appeared on purely local mail within the [[Kingston, Ontario]] area.

== Number of possible postal codes ==
Postal codes do not include the letters D, F, I, O, Q or U, and the first position also does not make use of the letters W or Z. This means the maximum number of FSAs available is 3,600. <!-- (18 1st position letters * 10 2nd position numbers * 20 3rd position letters = 3,600 FSAs) --> With 2,000 possible LDUs in each FSA, <!-- (10 4th position numbers * 20 5th position letters * 10 6th position numbers = 2,000 LDUs) --> there is a theoretical limit of 7.2 million postal codes. The practical limit is a bit lower, as Canada Post reserves some FSAs for special functions, such as for test or promotional purposes, (e.g. the H0H 0H0 for Santa Claus, see below) as well as for sorting mail bound for destinations outside Canada. The current Statistics Canada estimate of over 830,000 active postal codes<ref name="statcan">
{{cite web
 |url=http://publications.gc.ca/collections/collection_2011/statcan/92-153-G/92-153-g2011001-eng.pdf
 |title=Postal Code Conversion File (PCCF), Reference Guide
 |author=Statistics Canada
 |date=October 2010
 |pages=46
 |format=PDF
 |accessdate=26 May 2014
 }}
</ref> represents about 12% of the entire postal code "space", leaving more than ample room for expansion.

== Urbanization ==
"Urbanization" is the name Canada Post uses to refer to the process where it replaces a rural postal code (a code with a zero as its second character) with urban postal codes.<ref>
{{cite web
 |url=http://www.ontarioinsurance.com/english/pubs/bulletins/autobulletins/2006/a-02_06.asp
 |title=Bulletin - Rating Territories and Postal Code Changes by Canada Post (No.A - 02/06)
 |first=Bob
 |last=Christie
 |publisher=Financial Services Commission of Ontario
 |date=6 January 2006
 |accessdate=6 January 2007
 }}
</ref> The vacated rural postal code can then be assigned to another community or retired.  Canada Post decides when to urbanize a certain community when its population reaches a certain level, though different factors may also be involved.

For example, in early 2008, the postal code G0N 3M0 (covering [[Sainte-Catherine-de-la-Jacques-Cartier]], [[Fossambault-sur-le-Lac]] and [[Lac-Saint-Joseph, Quebec]]) was urbanized to postal codes beginning with G3N to remove ambiguities and confusions caused by similar street names.<ref>{{cite web |url=http://www.lejacquescartier.com/article-160035-Nouveaux-codes-postaux-en-fevrier-2008.html |title=Nouveaux codes postaux en février 2008 à Sainte-Catherine, Fossambault et Lac-Saint-Joseph |publisher=Médias Transcontinental |accessdate=1 December 2008 |language=fr}}</ref> Unique among province-wide districts, New Brunswick (postal district E) is completely urbanized, its rural codes having been phased out.

== Santa Claus ==
In 1974, staff at Canada Post's [[Montreal]] office were noticing a considerable number of letters addressed to [[Santa Claus]] entering the postal system, and those letters were being treated as undeliverable. Since employees handling those letters did not want the writers, mostly young children, to be disappointed at the lack of response, they started answering the letters themselves.<ref>{{cite web | work = News Releases |url=http://www.canadapost.ca/cpo/mc/aboutus/news/pr/2006/2006_jan_news_santa.jsf |title=Another million-letter year! |publisher=[[Canada Post]] |date=27 January 2006 |accessdate=27 April 2009 }}</ref>

The amount of mail sent to Santa Claus increased every [[Christmas]], up to the point that Canada Post established an official Santa Claus letter-response program in 1983. By 2011, Santa's mail was being handled with the assistance of eleven thousand volunteers, mostly current or former postal workers,<ref>{{cite web|url=https://www.canadapost.ca/cpo/mc/aboutus/news/pr/2011/2011_nov_santapo.jsf|title=News Releases|work=canadapost.ca}}</ref> at [[anycast|multiple locations]] across Canada<ref>{{cite web|url=http://www.intelligencer.ca/2011/12/14/still-time-to-write-to-santa|title=Still time to write to Santa|work=The Belleville Intelligencer}}</ref><ref>[http://www.canada.com/North+Pole+elves+ready+respond/997181/story.html North Pole elves ready to respond], ''North Shore News'', November 26, 2008</ref> devoting an average twenty-one hours to this seasonal task.

Approximately one million letters are addressed to Santa Claus each Christmas, including some originating outside of Canada, and all of them are answered in the same language in which they are written.<ref>{{cite web|url=http://www.canadapost.ca/cpo/mc/aboutus/news/pr/2005/2005_jan_news_santa.jsf |title=Over one million children write letters to Santa |author=[[Canada Post]] |date=27 January 2007 |accessdate=27 April 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20100409020858/http://www.canadapost.ca/cpo/mc/aboutus/news/pr/2005/2005_jan_news_santa.jsf |archivedate=9 April 2010 }}
</ref> Canada Post introduced a special address for mail to Santa Claus, complete with its own postal code:<ref>{{cite web|url=http://www.canadapost.ca/cpo/mc/personal/campaigns/holiday/default.jsf#santa|title=Canada Post – 2014 Holiday Season – Mailing Dates|work=canadapost.ca}}</ref>

 <tt>SANTA CLAUS</tt>
 <tt>NORTH POLE &nbsp;H0H 0H0</tt>
 <tt>CANADA</tt>

In French, Santa's name [[Père Noël]] translates as "Father Christmas", addressed as:

 <tt>PÈRE NOËL</tt>
 <tt>PÔLE NORD &nbsp;H0H 0H0</tt>
 <tt>CANADA</tt>

The postal code H0H 0H0 was chosen for this special seasonal use as it reads as "[[Ho ho ho]]".<ref name="Santa">{{cite web | title=Canada Post makes holiday connections easy! | publisher=Canada Post Media Relations | url=http://www.canadapost.ca/cpo/mc/aboutus/news/pr/2007/2007_dec_news_holiday.jsf |date=4 December 2007 |accessdate=27 April 2009 }}
</ref>

The H0- prefix is an anomaly: the 0 indicates a very small, rural village, but H is used to designate [[Montreal]], the second-largest city in Canada. As such, the H0- prefix is almost completely empty. H0M, assigned to the international [[Akwesasne]] tribal reserve on the Canada-US border, is the only other H0- postal code in active use.

In 2013, Santa was dragged into the ongoing [[Arctic sovereignty]] debate to support Canadian territorial claims extending to the [[North Pole]]. In response to attacks from [[Conservative Party of Canada|Conservative]] MP [[Paul Calandra]], parliamentary secretary to then-[[Prime Minister of Canada|Prime Minister]] [[Stephen Harper]], [[Justin Trudeau]], at the time leader of the third party [[Liberal Party of Canada|Liberals]], stated "Everyone knows that Santa Claus is Canadian. His postal code is H0H 0H0."<ref>{{cite web|url=http://www.telegraph.co.uk/news/newstopics/howaboutthat/10509980/Canada-vows-to-protect-Santa-Claus-from-Russian-troops-in-the-Arctic.html|title=Canada vows to protect Santa Claus from Russian troops in the Arctic|author=AFP|date=11 December 2013|work=''[[Daily Telegraph]]''}}</ref>

==Transition points to the Canadian Forces Postal Service==
For transition of mail from the civilian to the [[military mail|Canadian Forces Postal Service]], the postal codes of the three corresponding military post offices on Canadian soil are used. These being, depending upon the final destination.
* the Fleet Mail Offices (FMO) in Victoria, BC: V9A 7N2
* FMO in Halifax, NS: B3K 5X5
* the Canadian Forces Post Office (CFPO) in Belleville, ON: K8N 5W6<ref>[http://www.forces.gc.ca/en/write-to-the-troops/mailing-instructions.page Instructions for mailing overseas], [[Canadian Forces]]</ref>

These postal codes each represent a number of military post offices abroad, which are specified not by postal code but by CFPO or FMO number. The LDUs in this case corresponding not so much to a physical as to a virtual delivery unit since mail is not delivered locally but is forwarded to the actual delivery units at Canadian military bases and ships abroad.

:<tt>Name</tt>
:<tt>Slot #</tt>
:<tt>PO Box 5053 Stn Forces</tt>
:<tt>Belleville ON&nbsp; K8N 5W6</tt>
:<tt>CANADA</tt>

In this example, Canada Post will deliver to the CFPO at Belleville and the Canadian Forces Postal System will continue transport to the addressee at CFPO 5053 (in Geilenkirchen, Germany)<ref name="CFPO5053">{{cite web | title=CFE - CFSU(E)/CS/Post Office | url=http://www.europe.forces.gc.ca/ger-all/lm-mg/cfs-sfc/cs-sc/cspo-scpo-eng.asp |accessdate=7 January 2009 }}
</ref> by whatever means and timing the military will deem appropriate.<ref name="CFPO">
{{cite web | title=Canada Post - Canadian Forces Postal Service | url=http://www.canadapost.ca/cpo/mc/personal/productsservices/atoz/canadianforcesmail.jsf |accessdate=1 November 2014 }}
</ref>

== Alternate uses ==
Postal codes can be correlated with databased information from censuses or health registries to create a geographic profile of an area's population. For instance, postal codes have been used to compare children's risk of developing cancer<ref>{{cite web | url=http://www.statcan.ca/Daily/English/060608/d060608d.htm | title=Study: Socio-economic status and childhood cancers other than leukemia | work=The Daily | publisher=[[Statistics Canada]] | date=8 June 2006 | accessdate=3 July 2007}}</ref> and to describe a neighbourhood's entrenched poverty ("[[Vancouver]]'s [[Downtown Eastside]] is Canada's poorest postal code").<ref>[http://www.collectionscanada.gc.ca/eppp-archive/100/201/300/cdn_medical_association/cmaj/vol-159/issue-2/0139.htm The street value of prescription drugs], Amin Sajan, MD; Trevor Corneil, MD; Stefan Grzybowski, MD, MClSc, CMAJ 1998;159:139-42 </ref>

As [[electoral district (Canada)|Canadian electoral districts]] frequently follow postal code areas, citizens can identify their local elected representative using their postal code. Provincial and federal government websites offer an online "look-up" feature based on postal codes.<ref>{{cite web | url=http://www2.parl.gc.ca/parlinfo/compilations/HouseOfCommons/MemberByPostalCode.aspx?PostalCode=&Submit=Find&Language=E | title=Find your Member of Parliament using your Postal Code | publisher=Parliament of Canada | accessdate=3 July 2007}}</ref> Although A1A 1A1<ref>{{cite web |url=http://www.zipcodeworld.com/ca/A1A1A1 |title=About ZIP Code A1A 1A1 |publisher=Zipcode world |accessdate=1 December 2008 }}</ref> is sometimes displayed as a generic code for this purpose, it is actually a genuine postal code in use in the Lower [[The Battery, St. John's|Battery]], [[St. John's, Newfoundland and Labrador|St. John's Harbour]], Newfoundland.<ref>{{cite web | title = Google Maps | publisher = Google.com | url = https://maps.google.com/maps?f=q&hl=en&geocode=&q=Lower+Battery+Rd,+St+John%27s,+NL,+Canada+A1A+1A1&sll=37.0625,-95.677068&sspn=35.90509,71.71875&ie=UTF8&ll=47.571198,-52.696352&spn=0.00747,0.017509&z=16&iwloc=addr |accessdate=23 September 2008}}</ref> Another common "example" code in Canada Post materials, K1A 0B1, is the valid code for the Canada Post office building in [[Ottawa]].

== See also ==
* [[Canadian postal abbreviations for provinces and territories]]
* [[Geocoding]]
* [[List of postal codes in Canada]]

== References ==
{{Reflist|2}}

== External links ==
* [http://www.canadapost.ca/ Canada Post]
** [https://www.canadapost.ca/cpotools/mc/app/tpo/pym/targeting.jsf Precision Targeter: includes householder Counts and Maps]
** [http://www.canadapost.ca/cpo/mc/business/productsservices/nationalpresortationschematic.jsf?ecid=murl07001113&LOCALE=en National Presortation Schematic]: Includes monthly bulletin detailing postal code changes
* [http://www.canadapost.ca/cpotools/apps/fpc/personal/findByCity?execution=e4s1 Postal Code Lookup]
<!-- INCOMPLETE - Please bring this database up-to-date before adding this link; that means every active postal code in Canada needs to be in it (RESPONSE: I think its a worthy link as it has a huge portion of them and is a free database.  Let the people decide what they want.) -->
* [http://www.columbia.edu/kermit/postal-ca.html Doug Ewell's page explaining Canadian Postal Codes]
* [http://maps.library.utoronto.ca/datapub/digital/postal1925.jpg Postal Districts as of 1925]

{{Americas topic|Postal codes in}}

{{Good article}}

{{DEFAULTSORT:Canadian Postal Code}}
[[Category:1925 introductions]]
[[Category:Canada Post]]
[[Category:Postal codes in Canada|*]]
[[Category:Postal codes by country|Canada]]