{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox airport 
| name = RAF Church Lawford
| nativename = [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename-a = 
| nativename-r = 
| image = RAF Airspeed AS.10 Oxford II Brown.jpg
| image-width = 250
| caption = A Airspeed AS.10 Oxford similar to the ones that flew from the airfield
| IATA = 
| ICAO = 
| type = Military   
| owner = 
| operator = RAF   
| city-served = 
| location = Church Lawford
| elevation-f = 374
| elevation-m = 116
| coordinates = {{coord|52|21|5|N|001|20|31|W|region:GB_type:airport}}
| pushpin_map = Warwickshire
| pushpin_label = RAF Church Lawford
| pushpin_map_caption = Location in Warwickshire
| website = 
| r1-number = 16/34
| r1-length-f = 3,945 
| r1-length-m = 1,200 
| r1-surface = Concrete   
| r2-number = 08/26
| r2-length-f = 3,840   
| r2-length-m = 1,170
| r2-surface = Concrete   
| r3-number = 03/21
| r3-length-f = 3,600
| r3-length-m = 1,100 
| r3-surface = Concrete   
| stat-year = 
| stat1-header = 
| stat1-data = 
| stat2-header = 
| stat2-data = 
}}   
'''RAF Church Lawford''' is a former [[Royal Air Force]] station located {{Convert|1.5|mi|abbr=on}} south of [[Church Lawford]], [[Warwickshire]], [[England]], {{Convert|3.0|mi}} south-west of [[Rugby, Warwickshire]].

The airfield opened in April 1941<ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/church-lawford |title=Church Lawford |publisher=[[Airfields of Britain Conservation Trust]]|accessdate=22 April 2012}}</ref> and was used by the RAF for pilot training until it closed in 1955.

==Based units==

A number of [[Blind approach beacon system|Beam Approach]] units flew from the airfield like when No. 1509 Beam Approach Training Flight (BAT Flt) arrived flying [[Airspeed Oxford]]s from 6 June 1942 and No. 1533 Beam Approach Training Flight (BAT Flt) which again flew Oxfords from 27 October 1942 until April 1945.<ref name="AAFLU">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/flying_units.htm |title=Military flying units in the south west Midlands |publisher=Aviation Archaeology|accessdate=18 April 2012}}</ref>

The first unit to use the airfield was No. 2 Central Flying School flying Oxfords and [[Avro Tutor]]s from  15 June 1941 until 13 January 1942 when it was renamed No. 1 Flying Instructors School (FIS) flying Oxfords and Tutors carried on until October 1942. The unit was again renamed to [[No. 18 (Pilots) Advanced Flying Unit RAF]] ((P)AFU) flying Oxfords and [[Boulton Paul Defiant]]s on 27 October 1942 and operated until April 1945. Again the name was changed to [[No. 20 Flying Training School RAF]] (FTS) flying [[North American T-6 Texan|Harvard]]s from 3 April 1945 using [[RAF Snitterfield]] as a relief landing ground (RLG) until March 1948.<ref name="AAFLU"/>

A further two flying schools used the airfield after the end of the Second World War. The first was [[No. 20 Service Flying Training School RAF]] flying Harvards using RAF Snitterfield as a RLG until 1947, which like the wartime units at the airfield was renamed [[No. 2 Flying Training School RAF]] starting on 23 July 1947 and operating until 6 April 1948.<ref name="AAFLU"/>

===Other units===

A small number of other units was present at RAF Church Lawford during its lifetime such as [[No. 68 Maintenance Unit RAF]] which operated from 1 December 1954 until 27 March 1955 and as a sub-site of No. 68 Maintenance Unit between 27 March 1955 and 30 November 1956.<ref>{{cite web|url=http://www.rafweb.org/Stations/Stations-C.htm |title=RAF Church Lawford |publisher=RAFWEB|accessdate=18 April 2012}}</ref>

===Airfield Construction Branch===
In 1948 The [[Airfield Construction Branch RAF|Airfield Construction Branch]] moved to the airfield with the plant training school moving to [[Ryton on Dunsmore]]<ref>{{cite web|url=http://www.rafacb.org/RAFACB/Home.aspx |title=The Royal Air Force - Airfield Construction Branch. 1941 to 1966 |publisher=RAFACB|accessdate=18 April 2012}}</ref> before moving to [[Lichfield]] in 1953.<ref>{{cite web|url=http://www.raf.acb.btinternet.co.uk/aboutpag.htm |title=About Us|publisher=RAF.ACB|accessdate=18 April 2012|archiveurl=http://archive.is/XZGA|archivedate=7 September 2012}}</ref>

==Accidents and Incidents==
During life as a RAF training base accidents were not far away with a number of airmen killed during training these are just a select few:
{| class="wikitable sortable"
|-
! Date !! Incident !! Reference
|-
| 22 November 1942 ||  Oxford R6145 of 18 PAFU overshot landing and crashed at Bretford.  P/O Victor André Brayer (Jean Pierre Hinque) of the Free French air force was killed.  || <ref>http://www.aviationarchaeology.org.uk/marg/crashes1942.htm</ref>
 |-  
| 8 May 1944 || Oxford HN440 of 18 ((P)AFU) struck a tree at night while force landing following engine failure. || <ref name="AAC44">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/crashes1944.htm |title=Military aircraft crashes in the south west Midlands - 1944 |publisher=Aviation Archaeology|accessdate=18 April 2012}}</ref>
|-
| 14 October 1944  || [[Handley Page Halifax]] MZ920 of [[No. 434 Squadron RAF|434 Squadron]] crashed at Church Lawford after catching fire in the air. || <ref name ="AAC44"/>
|-
| 21 August 1946 || Harvard FT359 of No. 20 Service Flying Training School undershot landing. || <ref name="AAC46">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/crashes1946-49.htm |title=Military aircraft crashes in the south west Midlands - 1946 to 1949 |publisher=Aviation Archaeology|accessdate=18 April 2012}}</ref>
|-
| 9 January 1948 || Harvard FS725 of No. 2 Flying Training School belly landed. || <ref name="AAC46"/>
|}

==Current use==

The site of the airfield has be turned into a [[quarry]] called Ling Hall and the Lawford Heath Industrial Estate.<ref>{{cite web|url=http://www.bmhcomputers.co.uk/bmhonline/history/lawford_heath.htm  |title=Lawford Heath |publisher=BMH Online|accessdate=18 April 2012}}</ref>

==References==
{{Reflist}}

== External links==
* [http://www.rafacb.org/RAFACB/Home.aspx RAFACB.org]
{{Royal Air Force}}

{{DEFAULTSORT:Church Lawford}}
[[Category:Airports in England]]
[[Category:Royal Air Force stations in Warwickshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]