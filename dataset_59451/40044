{{multiple issues|
{{BLP sources|date=July 2013}}
{{unreliable sources|date=July 2013}}
{{Cleanup|date=January 2009}}
}}

{{Infobox philosopher
| region          = [[Western philosophy]]
| era             = [[20th-century philosophy|20th]] / [[21st-century philosophy]]
| image            = Hamid dabashi 9007.JPG
| name             = Hamid Dabashi
| birth_date       = {{birth date and age|1951|6|15}}
| birth_place      = [[Ahvaz]], [[Pahlavi dynasty|Iran]]
| nationality = Iranian
| school_tradition = [[Postcolonialism]], [[critical theory]]
| alma_mater = [[University of Tehran]]<br>[[University of Pennsylvania]]
| doctoral_advisor  = [[Philip Rieff (sociologist)|Philip Rieff]]
| main_interests   = [[Liberation theory]], [[literary theory]], [[aesthetics]], [[cultural theory]], [[sociology of culture]]
| influences       = [[Friedrich Nietzsche|Nietzsche]], [[Max Weber|Weber]], [[Martin Heidegger|Heidegger]], [[Emmanuel Levinas|Levinas]], [[Michel Foucault|Foucault]], [[Sartre]], [[Frantz Fanon|Fanon]], [[Theodor Adorno|Adorno]], [[Karl Marx|Marx]], [[Sigmund Freud|Freud]], [[Edward Said|Said]], [[Ahmad Shamlou|Shamlou]]
| notable_ideas    = Trans-Aesthetics, Radical Hermeneutics, Anti-colonial Modernity, Will to Resist Power, Dialectics of National Traumas and National Art Forms, Phantom Liberties
|}}

'''Hamid Dabashi''' ({{lang-fa|حمید دباشی}}; born 1951) is an Iranian Professor of [[Iranian peoples|Iranian]] Studies and [[Comparative Literature]] at [[Columbia University]] in [[New York City]].<ref name=HD>{{Official website|http://www.hamiddabashi.com }}</ref>

He is the author of over twenty books.<ref>[http://www.hamiddabashi.com/books.html Hamid Dabashi's Official Web Site] {{webarchive |url=https://web.archive.org/web/20070207132542/http://www.hamiddabashi.com/books.html |date=February 7, 2007 }}</ref> Among them are his ''Theology of Discontent''; several books on Iranian cinema; ''Staging a Revolution''; an edited volume, ''Dreams of a Nation: On Palestinian Cinema''; and his one-volume analysis of Iranian history ''[[Iran: A People Interrupted]].''<ref>[http://www.thenewpress.com/index.php?option=com_title&task=view_title&metaproductid=1579 Iran: A People Interrupted]</ref>

==Biography==
Born and raised in southern city of [[Ahvaz]] in [[Iran]], Dabashi--a self-professed spokesperson for [[postcolonialism]]--was educated in Iran and then in the United States, where he received a dual Ph.D. in [[sociology]] of [[culture]] and [[Islamic studies]] from the [[University of Pennsylvania]] in 1984, followed by a postdoctoral fellowship at [[Harvard University]]. He wrote his dissertation on [[Max Weber]]’s theory of [[charismatic authority]] with Freudian cultural critic [[Philip Rieff]]. He lives in New York with his wife and colleague [[Golbarg Bashi]].<ref>[http://www.hamiddabashi.com/biography.html Hamid Dabashi's Official Web Site] {{webarchive |url=https://web.archive.org/web/20070206065959/http://www.hamiddabashi.com/biography.html |date=February 6, 2007 }}</ref>

==Major works==

Hamid Dabashi’s books are [[Iran: A People Interrupted]], which traces the last two hundred year's of Iran's history including analysis of cultural trends, and political developments, up to the collapse of the reform movement and the emergence of the presidency of Mahmoud Ahmadinejad.
Dabashi argues that "Iran needs to be understood as the site of an ongoing contest between two contrasting visions of modernity, one colonial, the other anticolonial".

His book ''[[Theology of Discontent]]'', is a study of the global rise of [[Islamism]] as a form of [[liberation theology]]. His other book ''[[Close Up: Iranian Cinema, Past, Present, Future]]'' (2001) is the founding{{dubious|date=October 2012}} text on modern Iranian cinema and the phenomenon of (Iranian) national cinema as a form of cultural modernity – featured even in the [[Lonely Planet]] travel guide for [[Iran]]. In his essay "For the Last Time: Civilizations", he has also posited the binary opposition between “Islam and the West” as a major [[narrative]] strategy of raising a fictive centre for European modernity and lowering the rest of the world as [[peripheral]] to that centre.<ref>{{cite web|author=Hamid Dabashi |url=http://iss.sagepub.com/cgi/content/abstract/16/3/361 |title=For the Last Time: Civilizations - Dabashi 16 (3): 361 - International Sociology |publisher=Iss.sagepub.com |date=2001-09-01 |accessdate=2013-08-08}}</ref>

In [[Truth and Narrative]], he has deconstructed the [[essentialist]] conception of [[Islam]] projected by [[oriental studies|Orientalists]] and [[Islamists]] alike. Instead he has posited, in what he calls a “polyfocal” conception of Islam, three competing [[discourse]]s and [[institutions]] of [[authority]] – which he terms “nomocentric” (law-based), “logocentric” (reason-based) and “homocentric” (human-based) – vying for [[Power (sociology)|power]] and competing for [[wikt:Special:Search/legitimacy|legitimacy]]. The historical dynamics among these three readings of “Islam”, he concludes, constitutes the [[moral]], [[political]] and [[intellectual]] history of [[Muslims]].

Among his other work are his essays Artist without Borders (2005), Women without Headache (2005), [http://iss.sagepub.com/cgi/content/abstract/16/3/361 For the Last Time Civilization] (2001) and "The End of Islamic Ideology" (2000).<ref>[http://www.findarticles.com/p/articles/mi_m2267/is_2_67/ai_63787340 The End of Islamic Ideology] (2000)</ref>

Hamid Dabashi is also the author of numerous articles and public speeches, ranging in their subject matters from [[Islamism]], [[feminism]], globalised [[empire]] and [[ideologies]] and strategies of resistance, to visual and performing arts in a global context.

==Film and art==

Dabashi was consulted by [[Ridley Scott]] for ''[[Kingdom of Heaven (film)|Kingdom of Heaven]]'' (2005).<ref>{{cite web|url=http://www.christianitytoday.com/movies/interviews/KOHhistorians.html|title=Interviews: Kingdom of Heaven Scholars|publisher=[[Christianity Today]]|accessdate=2008-02-25|last=|first= |archiveurl = https://web.archive.org/web/20080219211329/http://www.christianitytoday.com/movies/interviews/KOHhistorians.html |archivedate = 2008-02-19}}</ref> Scott claimed his film was approved and verified by Dabashi: "I showed the film to one very important Muslim in New York, a lecturer from Columbia, and he said it was the best portrayal of Saladin he's ever seen".<ref>{{cite news|url=http://news.bbc.co.uk/1/hi/entertainment/film/4492625.stm|title=Film-maker defends Crusades epic|publisher=BBC News|accessdate=2008-02-25|last=|first= | date=April 28, 2005}}</ref>

Dabashi was the chief consultant to [[Hany Abu-Assad]]'s “[[Paradise Now]]” (2005), and [[Shirin Neshat]]’s “Women without Men” (2006).{{citation needed|date=December 2013}} Dabashi appears in [[Bavand Karim]]'s ''[[Nation of Exiles]]'' (2010), providing analysis of the [[Iranian Green Movement]].<ref>{{cite web|title=''Nation of Exiles''|url=http://topdocumentaryfilms.com/nation-of-exiles/}}</ref>

Dabashi has also served as jury member on many international art and film festivals,<ref>{{cite web|title=
Locarno International Film Festival|publisher=[[IMDB]]|url=http://www.imdb.com/Sections/Awards/Locarno_International_Film_Festival/2004}}</ref> most recently the Locarno International Festival in Switzerland. In the context of his commitment to advancing trans-national art and independent world cinema, he is the founder of Dreams of a Nation, a [[Palestinian people|Palestinian]] Film Project, dedicated to preserving and safeguarding [[Palestinian Cinema]].<ref name=HD/>  For his contributions to Iranian cinema, [[Mohsen Makhmalbaf]], the Iranian film-maker called Dabashi "a rare cultural critic".{{citation needed|date=December 2013}}

==Public commentary and criticism==

Dabashi has been a commentator on a number of political issues, often regarding the [[Middle East]], [[Columbia University]], American foreign policy, or a combination of those.

===Columbia University===
In 2004, Dabashi was involved in a dispute at [[Columbia University]] between Jewish students and pro-Palestinian professors, which included accusations of antisemitism against the professors.<ref>{{cite web|last=Senior |first=Jennifer |url=http://nymag.com/nymetro/urban/education/features/10868/ |title=Columbia University's Own Middle East War |publisher=Nymag.com |date=2005-01-17 |accessdate=2013-08-08}}</ref> According to the [[New York Times]], Dabashi was mentioned principally because of his published political viewpoints, and that he canceled a class to attend a Palestinian rally.<ref name=NYT>{{cite news|url=https://www.nytimes.com/2005/01/18/education/18columbia.html?pagewanted=2|title=Mideast Tensions Are Getting Personal on Campus at Columbia|publisher=The New York Times|accessdate=2008-02-27|last=Kleinfield|first=N. R. | date=January 18, 2005}}</ref> The New York chapter of the [[American Civil Liberties Union]] sided with the professors.<ref>{{cite web|url=http://www.nysun.com/article/6826|title=Civil Liberties Official Defends Columbia Professors - December 28, 2004 |publisher=[[The New York Sun]]|accessdate=2008-02-27|last=|first=}}</ref> An ad hoc committee formed by [[Lee C. Bollinger]], Columbia University's president, reported in March 2005 that they could not find any credible allegations of antisemitism, but did criticize the university's grievance procedures, and recommended changes. However, the committee was criticized for failing to examine an article in which Dabashi wrote that Israelis suffer from "a vulgarity of character that is bone-deep and structural to the skeletal vertebrae of its culture."<ref name="The New York Sun">{{cite web|url=http://www.nysun.com/article/11414|title=Faculty Committee Largely Clears Scholars - March 31, 2005 - |publisher=[[The New York Sun]]|accessdate=2008-02-27|last=|first=}}</ref>

In 2002, Dabashi sharply criticized Rabbi Charles Sheer (who was the university's Jewish chaplain between 1969 and 2004) after he admonished several professors for cancelling their classes to attend pro-Palestinian rallies. Dabashi wrote in the Columbia Spectator that Rabbi Sheer "has taken upon himself the task of mobilizing and spearheading a crusade of fear and intimidation against members of the Columbia faculty and students who have dared to speak against the slaughter of innocent Palestinians."<ref name="The New York Sun"/>

In September 2004, the ''[[New York Sun]]'' reported that Victor Luria, a Ph.D. student who worked in a Columbia genetics lab and who served in the Israeli military in 1998, sent an email to Dabashi sharply criticizing him for an article he wrote in Al-Ahram.<ref name=soul1/> Dabashi subsequently forwarded the email to several top Columbia officials, including [[Alan Brinkley]], claiming that he feared a "potential attack by a militant slanderer" and asked for protection from campus security. Brinkley responded that there was "nothing threatening" about Luria's email and that campus security would not be notified.<ref name=Luria>[http://www.nysun.com/new-york/professor-fearful-of-attack/5175 Professor Fearful of Attack], By Jacob Gershman, ''[[New York Sun]]'', November 22, 2004 (retrieved on December 2, 2008).</ref>

===Views on Israel===
Dabashi has described the state of Israel as "a dyslexic Biblical exegesis," "occupied Palestine," "a vicarious avocation," "a dangerous delusion," "a colonial settlement," "a Jewish apartheid state," and "a racist [[apartheid]] state"<ref name=obama1>[http://weekly.ahram.org.eg/2008/903/focus.htm Obama's Palestinian problem] by Hamid Dabashi, Al-Ahram Weekly, 26 June - 2 July 2008, Issue No. 903.</ref> In an interview with AsiaSource in June 2003, Dabashi stated that supporters of Israel "cannot see that Israel over the past 50 years as a colonial state - first with white European colonial settlers, then white American colonial settlers, now white [[Russians|Russian]] colonial settlers - amounts to nothing more than a military base for the rising predatory empire of the United States. Israel has no privilege greater or less than Pakistan or Kuwait or Saudi Arabia. These are all military bases but some of them, like Israel, are like the hardware of the American imperial imagination."<ref>{{cite news|url=http://www.asiasoc.org/news/special_reports/dabashi.cfm |title=AsiaSource Special Report - Interview with Hamid Dabashi |date=June 12, 2003 |author=Nermeen Shaikh |publisher=Asia Source }}{{dead link|date=March 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

In an interview with the Electronic Intifada in September 2002, Dabashi referred to the pro-Israel lobby as "Gestapo apparatchiks" and that "The so-called "pro-Israeli lobby" is an integral component of the imperial designs of the Bush administration for savage and predatory globalization." He also criticized "fanatic zealots from Brooklyn" who have settled on Palestinian lands.<ref name=ei1>[http://electronicintifada.net/v2/article747.shtml Campus Watch: Interview with Prof. Hamid Dabashi], The Electronic Intifada, September 30, 2002 (retrieved on November 8, 2008).</ref> Dabashi has also harshly criticized the New York Times for what he describes as a bias towards Israel, stating that the paper is "the single most nauseating propaganda paper on planet."<ref name=ohmynews1>[http://english.ohmynews.com/ArticleView/article_view.asp?menu=A11100&no=310240&rel_no=1&back_url= How Do We Sleep While Beirut Is Burning?] by Hamid Dabashi, OhmyNews, August 9, 2006.(retrieved on November 8, 2008).</ref>

In September 2004, Dabashi sharply criticized Israel in the Egyptian newspaper ''Al-Ahram'', writing that:

<blockquote>
What they call "Israel" is no mere military state. A subsumed militarism, a systemic mendacity with an ingrained violence constitutional to the very fusion of its fabric, has penetrated the deepest corners of what these people have to call their "soul."...  Half a century of systematic maiming and murdering of another people has left its deep marks on the faces of these people, the way they talk, the way they walk, the way they handle objects, the way they greet each other, the way they look at the world. There is an endemic prevarication to this machinery, a vulgarity of character that is bone-deep and structural to the skeletal vertebrae of its culture.<ref name=soul1>{{cite news|url=http://weekly.ahram.org.eg/2004/709/cu12.htm|title=For a Fistful of Dust: A Passage to Palestine|date=September 23–29, 2004|publisher=[[Al-Ahram Weekly]]|author=Hamid Dabashi}}</ref>
</blockquote>

Responding to Dabashi’s ''Al-Ahram'' essay, [[Columbia University]] President [[Lee Bollinger]] said, “I want to completely disassociate myself from those ideas. They’re outrageous things to say, in my view.”<ref>Columbia’s Own Middle East War, Jennifer Senior, Jan. 10, 2005, New York Magazine, [http://nymag.com/nymetro/urban/education/features/10868/]</ref> Jonathan Rosenblum, director of ''Jewish Media Resources'', later wrote that "Dabashi apparently subscribes to [[Lamarckism|Lamarckian genetics]] [that an organism can pass on experiences and characteristics that it acquires during its lifetime to its offspring]. Not only have the alleged actions of Israeli Jews effected changes in their very bone structure, but those changes are transmitted to subsequent generations. Dabashi’s depiction of the debased Jewish [[physiognomy]] is racism pure and simple."<ref>[http://www.jewishmediaresources.com/819/double-standards Double standards] by Jonathan Rosenblum, ''Jewish Media Resources'', March 18, 2005.</ref> In ''The Bulletin'', Herb Denenberg wrote that Dabashi's article "is not borderline racism. It’s as gross and obvious as racism can get."<ref>[http://thebulletin.us/articles/2009/01/20/herb_denenberg/doc4975a46fde30d964905359.txt Columbia Has Come To Stand For Terrorism, Genocide] By Herb Denenberg, ''The Bulletin'', January 20, 2009.</ref> Writing in ''The Nation'', Scott Sherman wrote that Dabashi's article was "troubling" because of its "sweeping characterization of an entire people--"Israeli Jews" or not—as vulgar and domineering in their very essence. The passage can easily be construed as anti-Semitic. Dabashi, at a minimum, is guilty of shrill and careless writing."<ref>[http://www.thenation.com/article/mideast-comes-columbia?page=0,1 The Mideast Comes to Columbia] by Scott Sherman, March 16, 2005.</ref>

In a sworn statement submitted to the US Commission on Civil Rights, Dabashi stated that he has not expressed, nor ever harbored, any anti-Semitic sentiments and that the 2004 ''Al-Ahram'' essay was being misconstrued.<ref>[http://www.usccr.gov/pubs/081506campusantibrief07.pdf US Commission on Civil Rights - Campus Anti-Semitism], Briefing Report, July 2006.</ref> He has also criticized pro-Israel groups in the United States, saying that the "pro-Israeli Zionist lobby in the US banked and invested heavily in infiltrating, buying, and paying for all the major and minor corridors of power."<ref name=Moralmilitary>[http://palestinechronicle.com/view_article_details.php?id=14656 The Moral and Military Meltdown of Israel] by Hamid Dabashi, The Palestine Chronicle, January 12, 2009.</ref> In the same article, Dabashi endorsed cultural and academic boycotts of Israel.<ref name=Moralmilitary/>

In a letter to the ''Columbia Spectator'', Dabashi wrote that the above passage was "not a racial characterization of a people, but a critical reflection on the body politics of state militarism" and the effects that it has on human beings. Dabashi also apologized for "any hurt that I may have inadvertently caused" due to the interpretation of the passage.<ref>[http://www.columbiaspectator.com/2005/01/27/letters-editor Dabashi Explains Comments on Militarism of Israel] by Hamid Dabashi, ''Columbia Spectator'', January 27, 2005.</ref>

In an article published January 2009, Dabashi advocated for boycott efforts targeting both individuals and institutions:
<blockquote>"The divestment campaign that has been far more successful in Western Europe needs to be reinvigorated in North America – as must the boycotting of the Israeli cultural and academic institutions… Naming names and denouncing individually every prominent Israeli intellectual who has publicly endorsed their elected officials' wide-eyed barbarism, and then categorically boycotting their universities and colleges, film festivals and cultural institutions, is the single most important act of solidarity that their counterparts can do around the world."<ref>[http://www.adl.org/main_Anti_Israel/boycott_divestment_campus_09.htm?Multi_page_sections=sHeading_2 Boycott & Divestment Efforts Proliferate on Campus], Anti-Defamation League (ADL), April 8, 2009.</ref></blockquote>

Dabashi is on the advisory board of the U.S. Campaign for the Academic & Cultural Boycott of Israel.

===Criticism of Lee Bollinger===
Following Columbia University President [[Lee Bollinger]]'s statements on Iranian President [[Mahmoud Ahmadinejad]] during Ahmadinejad's visit to Columbia in September 2007 (in which Bollinger stated that the Iranian President was a  "petty and cruel dictator" who lacked the "intellectual courage" to offer real answers on denying the [[Holocaust]]) Dabashi wrote that Bollinger's statements were "the most ridiculous clichés of the neocon propaganda machinery, wrapped in the missionary position of a white racist supremacist carrying the heavy burden of civilizing the world." Dabashi further stated that Bollinger's comments were "propaganda warfare … waged by the self-proclaimed moral authority of the United States" and that "Only Lee Bollinger's mind-numbing racism when introducing Ahmadinejad could have made the demagogue look like the innocent bystander in a self-promotional circus." In addition, Dabashi wrote that when Bollinger made these comments, "Nothing short of the devil incarnate, the Christian Fundamentalist in Bollinger thought, was sitting in front of him" and that Bollinger's "shamelessly racist" comments were "replete with racism."<ref name=NYS1>{{cite news|url=http://www2.nysun.com/new-york/columbia-professor-calls-bollinger-white/ |title=Columbia Professor Calls Bollinger White Supremacist |date=October 15, 2007 |publisher=New York Sun |author=Annie Karni }}{{dead link|date=March 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>[http://weekly.ahram.org.eg/2007/866/focus.htm Of banality and burden] by Hamid Dabashi, Al-Ahram Weekly, October 11–17, 2007, Issue No. 866. (retrieved on November 8, 2008).</ref>

Judith Jackson, a professor of epidemiology at Columbia who is the co-coordinator of the Israel advocacy group [[Scholars for Peace in the Middle East]],<ref name=SPMEAbout>[http://www.spme.net/aboutus.html "About SPME,"] online posting, official website of SPME.</ref> criticized Dabashi for his remarks, stating that  Dabashi's article was "sheer demagoguery" and that "attributing President Bollinger's remarks or behavior to racism is absurd."<ref name=NYS1/>

===Reading Lolita in Tehran and Azar Nafisi===
{{Main|Reading Lolita in Tehran}}

In 2006, Dabashi sharply criticized [[Azar Nafisi]] for her book ''[[Reading Lolita in Tehran]]'', stating that "By seeking to recycle a kaffeeklatsch version of English literature as the ideological foregrounding of American empire, Reading Lolita in Tehran is reminiscent of the most pestiferous colonial projects" and accusing her of being a "native informer and colonial agent."<ref name=ahram>{{cite web|url=http://weekly.ahram.org.eg/2006/797/special.htm |author=Hamid Dabashi |title=Native informers and the making of the American empire |publisher=Al-Ahram, # 797 |date=06-01-2006 |accessdate=09-01-2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20080110012256/http://weekly.ahram.org.eg/2006/797/special.htm |archivedate=2008-01-10 |df= }}</ref> In an interview with ''Z Magazine'', Dabashi compared Nafisi to former American soldier [[Lynndie England]], who was convicted of abusing Iraqi prisoners at Abu Ghraib."<ref name=bookclubbed/><ref name=Fulford>[http://www.canada.com/nationalpost/columnists/story.html?id=9837485b-aef9-49aa-b04d-8c73c804862e Reading Lolita at Columbia] {{webarchive|url=https://web.archive.org/web/20160303230045/http://www.canada.com/nationalpost/columnists/story.html?id=9837485b-aef9-49aa-b04d-8c73c804862e |date=2016-03-03 }} by Robert Fulford, National Post, November 6, 2006 (retrieved on October 21, 2009).</ref>

Nafisi responded to Dabashi's criticism by stating that she is not, as Dabashi claims, a neoconservative, that she opposed the Iraq war, and that she is more interested in literature than in politics. In an interview, Nafisi stated that she's never argued for an attack on Iran and that democracy, when it comes, should come from the Iranian people (and not from US military or political intervention). She added that while she is willing to engage in "serious argument...Debate that is polarized isn't worth my time." She stated that she did not respond directly to Dabashi because "You don't want to debase yourself and start calling names."<ref name=bookclubbed>[http://www.boston.com/news/globe/ideas/articles/2006/10/29/book_clubbed/?page=2 Book clubbed] by Christopher Shea, The Boston Globe, October 29, 2006 (retrieved on October 21, 2009).</ref>

== References ==
{{reflist|2}}

==External links==
{{wikiquote}}
* [http://www.hamiddabashi.com/ Official Web site]
* [http://www.columbia.edu/cu/mesaas/faculty/directory/dabashi.html Academic site at Columbia University]
* [http://www.makhmalbaf.com/persons.php?p=23&lang=2 Persian Biography], Makhmalbaf Film House.
* [http://www.hamiddabashi.com/newspaper.html Op-eds]
* Hamid Dabashi speaks in the documentary film on [[Omar Khayyam|Omar Khayyām]], ''Intoxicating Rhymes and Sobering Wine'', {{YouTube|AP6a6_C9t2k}} (48 sec).
* [http://www.legalleft.org/?page_id=137 Hamid Dabashi speaks] at [[Harvard Law School]]
*{{C-SPAN|Hamid Dabashi}}

{{Authority control}}

{{DEFAULTSORT:Dabashi, Hamid}}
[[Category:American sociologists]]
[[Category:American literary critics]]
[[Category:American art critics]]
[[Category:American film critics]]
[[Category:American historians]]
[[Category:American anti-war activists]]
[[Category:Iranian democracy activists]]
[[Category:Columbia University faculty]]
[[Category:Harvard University alumni]]
[[Category:Iranian historians]]
[[Category:Iranian expatriate academics]]
[[Category:American people of Iranian descent]]
[[Category:Iranian literary critics]]
[[Category:Iranologists]]
[[Category:Islamic studies scholars]]
[[Category:Literary historians]]
[[Category:People from Ahvaz]]
[[Category:Persian writers]]
[[Category:University of Pennsylvania alumni]]
[[Category:1951 births]]
[[Category:Living people]]
[[Category:Critical theorists]]
[[Category:American writers of Iranian descent]]