{{Infobox person
|name        = Madeline Amy Sweeney
|image       = Sweeney.madeline.jpg
|image size  =
|birth_date  = {{Birth date|1965|12|14}}
|birth_place = [[Valley Stream, New York]], [[United States]]
|death_date  = {{death date and age|2001|9|11|1965|12|14}}
|death_place = [[New York City]], [[New York (state)|New York]], [[United States]]
|death_cause = [[September 11 attacks|Terrorist engineered]] [[American 11|plane crash]]
|other_names = Amy Sweeney
|known_for   = 
|occupation  = Flight attendant
|nationality = American
|spouse= Michael Sweeney (?-2001; her death)
}}
'''Madeline Amy Sweeney''' (December 14, 1965<ref name=obit>{{cite web|url=http://www.legacy.com/obituaries/bostonglobe/obituary.aspx?pid=91987|title=Madeline Amy Sweeney Obituary|date=September 14, 2001|publisher=Boston Globe|accessdate=2013-12-10}}</ref> – September 11, 2001), known as '''Amy Sweeney''', was an American [[flight attendant]] killed on board  [[American Airlines Flight 11]] when it was [[Aircraft hijacking|hijacked]] and flown deliberately into the North Tower of the [[World Trade Center (1973–2001)|World Trade Center]] in New York City, as part of the [[September 11, 2001 attacks]].

==Flight 11==
On September 11, 2001, Sweeney was asked by [[American Airlines]] to take an extra shift because the other crew member, who was assigned to the position, was ill.<ref>{{cite web|url=http://www.nhl.com/ice/news.htm?id=588024|title=Ten years later, 9/11 still resonates in hockey|date=September 9, 2011|first=Dan|last=Rosen|publisher=NHL.com|accessdate=2011-09-10}}</ref> Normally, she would only work part-time on weekends.

On September 11, at approximately 7:15 am, before the plane had taken off, Sweeney made a [[cellular telephone]] call to her husband Michael, from the plane (which he deemed to be 'highly unusual').<ref>{{cite web|last=of Investigation|first=Federal Bureau|title=T7 B17 FBI 302s of Interest Flight 11 Fdr- Entire Contents|url=http://www.scribd.com/doc/14094215/T7-B17-FBI-302s-of-Interest-Flight-11-Fdr-Entire-Contents|publisher=Scribd Inc.|accessdate=2014-01-18}}</ref> 
She was feeling low about being at work and missing out on a chance to see their daughter, a kindergartner, off to school.<ref>{{cite web|last=Lopez|first=Steve|title=A decade later, returning to the scene of something unfathomable|url=http://articles.latimes.com/2011/sep/11/local/la-me-0911-lopez-10yearslater-20110908/2|publisher=Los Angeles Times|accessdate=2014-01-18}}</ref>

{{quote box|quote="I see water. I see buildings. I see buildings! We are flying low. We are flying very, very low. We are flying way too low. Oh my God we are flying way too low. Oh my God!" (Flight 11 crashes).|source=Sweeney's last words on the inflight call with American Airlines manager Michael Woodward.<ref>{{cite news|url=http://news.bbc.co.uk/2/hi/americas/3919613.stm|publisher=BBC News|title=Extract: 'We have some planes'|date=23 July 2004|accessdate=2009-09-11}}</ref><ref>{{cite news|url=http://911research.wtc7.net/cache/planes/attack/abc_calmbefore.html|publisher=ABC News|title=Calm as Death Drew Near for Flight 11|date=21 February 2004|accessdate=2013-09-18}}</ref>|width=31%|align=right}}

Sweeney was aged 35 when she was killed. She had been a [[flight attendant]] for twelve years. She was survived by her husband Michael and two children, Jack and Anna. They live in [[Acton, Massachusetts|Acton]], [[Massachusetts]].<ref name=obit/>

==Legacy==
[[File:12.6.11MadelineAmySweeneyPanelN-74ByLuigiNovi1.jpg|thumb|left|Sweeney’s name is located on Panel N-74 of the [[National September 11 Memorial]]’s North Pool, along with those of other passengers of Flight 11.]]

On February 11, 2002, Sweeney was commemorated in a series of new annual bravery awards initiated by the [[Government of Massachusetts]]. The annual Madeline Amy Sweeney Award for Civilian Bravery is awarded every September 11 to at least one Massachusetts resident who displayed extraordinary courage in defending or saving the lives of others.<ref>[http://www.mass.gov/?pageID=eopsutilities&L=1&sid=Eeops&U=sweeney_award_form Madeline Amy Sweeney Award for Civilian Bravery - EOPS<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20060823203633/http://www.mass.gov/?pageID=eopsutilities&L=1&sid=Eeops&U=sweeney_award_form |date=August 23, 2006 }}</ref>

The first recipients were Sweeney and her colleague [[Betty Ong]], who had also relayed information about the hijacking to personnel on the ground. Pilot [[John Ogonowski]] also received a posthumous award for being thought to have turned the cockpit radio switch on, which allowed ground control to listen to remarks being made by the hijackers. They were all residents of Massachusetts. Relatives of all three accepted the awards on their behalf.{{cn|date=March 2015}}

At the [[National 9/11 Memorial]], Sweeney is memorialized at the North Pool, on Panel N-74.<ref>{{cite web|url=http://names.911memorial.org/#lang=en_US&page=person&id=4386|title=North Pool: Panel N-74 - Madeline Amy Sweeney|publisher=[[National September 11 Memorial & Museum]]|accessdate=October 29, 2011}}</ref>

==References==
{{Reflist|2}}

{{Commons category}}

{{DEFAULTSORT:Sweeney, Madeline Amy}}
[[Category:1965 births]]
[[Category:2001 deaths]]
[[Category:Flight attendants]]
[[Category:Victims of the September 11 attacks]]
[[Category:American terrorism victims]]
[[Category:Terrorism deaths in New York]]
[[Category:People murdered in New York]]
[[Category:People from Acton, Massachusetts]]