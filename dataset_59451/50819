{{Infobox journal
| title = Calcified Tissue International
| cover = [[File:Calcified-tissue-int-153x204.jpg]]
| discipline = [[Biochemistry]]
| abbreviation = Calcif. Tissue Int.
| impact = 3.275
| impact-year = 2015
| editor = Roberto Civitelli, Stuart H. Ralston
| website = http://springerlink.metapress.com/content/100120/ 
| publisher = [[Springer Science+Business Media]]
| country = 
| history = 1967-present
| frequency = Monthly
| formernames = Calcified Tissue Research
| ISSN = 0171-967X 
| eISSN = 1432-0827 
| CODEN = CTINDZ
| LCCN = 79649796
| OCLC = 42787226
}}
'''''Calcified Tissue International''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[Springer Science+Business Media]] and first launched in 1967.<ref>{{cite web |url=http://lccn.loc.gov/79649796 |title=Calcified Tissue International|publisher=[[Library of Congress]] |work=Catalog |accessdate=2011-04-22}}</ref> From 1967 to 1978, the journal was published under the name ''Calcified Tissue Research''.<ref>{{cite web |url=http://www.springerlink.com/content/0171-967x |title=Calcified Tissue International |publisher=[[Springer Science+Business Media]] |work=SpringerLink  |accessdate=2011-04-22}}</ref> It is an official journal of the [[International Osteoporosis Foundation]].<ref>{{cite web |url=http://www.iofbonehealth.org/publications/calcified-tissue-international.html |title=Calcified Tissue International &#124; IOF Publications |publisher=International Osteoporosis Foundation |work=Homepage |accessdate=2011-04-22}}</ref> The journal is published monthly and includes original research on the structure and function of bone and other mineralized systems in living organisms, as well as reviews and special reports. 
 
The [[Editor-in-chief|co-editors]] are Roberto Civitelli and Stuart H. Ralston. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.275 <ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-07-04}}</ref>

== References ==
{{Reflist}}

==External links==
*{{Official website|http://www.springer.com/life+sciences/biochemistry+%26+biophysics/journal/223}}

[[Category:Publications established in 1967]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Biochemistry journals]]