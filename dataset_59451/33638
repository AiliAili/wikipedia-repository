{{about|the David Axelrod album|the William Blake poems it was inspired by|Songs of Innocence and of Experience|other uses|Songs of Innocence (disambiguation)}}
{{featured article}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = Song of Innocence
| Type       = studio
| Artist     = [[David Axelrod (musician)|David Axelrod]]
| Cover      = David Axelrod - Song of Innocence.jpg
| Alt        =
| Released   = October 1968
| Recorded   = 1968
| Studio     = [[Capitol Studios]] in Los Angeles
| Genre      = [[Jazz fusion]], [[baroque pop]], [[psychedelic soul|psychedelic R&B]], [[art pop]]
| Length     = {{Duration|m=26|s=35}}
| Label      = [[Capitol Records|Capitol]]
| Producer   = David Axelrod
| Last album =
| This album = '''''Song of Innocence'''''<br/>(1968)
| Next album = ''[[Songs of Experience (album)|Songs of Experience]]''<br/>(1969)
}}
'''''Song of Innocence''''' is the debut album of American composer and producer [[David Axelrod (musician)|David Axelrod]]. It was released in October 1968 by [[Capitol Records]]. Axelrod sought to capitalize on the experimental climate of popular music at the time and composed the album as a [[suite (music)|suite]]-like [[tone poem]] based on ''[[Songs of Innocence and of Experience|Songs of Innocence]]'', a 1789 illustrated collection of poems by [[William Blake]]. It was recorded at [[Capitol Studios]] in Los Angeles with an orchestra and a variety of studio musicians, including keyboardist and conductor [[Don Randi]], guitarist [[Al Casey (rock & roll guitarist)|Al Casey]], bassist [[Carol Kaye]], and drummer [[Earl Palmer]].

''Song of Innocence'' is an instrumental [[jazz fusion]] record that incorporates elements of [[classical music|classical]], [[rock music|rock]], [[funk]], [[pop music|pop]], and [[theatre music]]. It is [[arrangement|arranged]] for bass, drums, and string instruments, written in the rock [[instrumental idiom|idiom]] with [[tempo]]s centered on rock-based patterns by Palmer. Axelrod used [[contrast (music)|contrast]] in his orchestral compositions and interspersed the album's euphoric [[psychedelic soul|psychedelic R&B]] [[musical form|form]] with dramatic, harrowing arrangements to reflect the supernatural themes found in Blake's poems. The music's reverent, psychedelic overtones evoke their themes of innocence and spirituality.

Although ''Song of Innocence'' was innovative for its application of rock and jazz techniques, it was not commercially successful and confounded contemporary critics, who viewed it as an ambitious curiosity piece. In the 1990s, critics reassessed the album as a classic, while leading disc jockeys in [[hip hop music|hip hop]] and [[electronica]] rediscovered and [[sampling (music)|sampled]] the album's music. "Holy Thursday", the record's best-known song, was frequently sampled by hip hop producers. The renewed interest in Axelrod's work prompted [[Stateside Records]] to reissue ''Song of Innocence'' in 2000.

== Background ==
[[File:Blake Innocence Title.jpg|right|thumb|180px|upright|Axelrod was inspired to record an album based on ''[[Songs of Innocence and of Experience|Songs of Innocence]]'', an illustrated collection of poems by [[William Blake]].]]

In 1968, [[David Axelrod (musician)|David Axelrod]] gained national fame for his controversial [[mass (music)|mass]] composition ''[[Mass in F Minor]]'', which he wrote in a contemporary [[rock music|rock]] vein for [[the Electric Prunes]]. Axelrod, who was challenged by what he described as a "new breed of record buyer ... more sophisticated in his thinking", was one of several Los Angeles-based musical eccentrics during the late 1960s who expanded on the mid-1960s studio experiments of [[Brian Wilson]] and [[George Martin]]. After his success with the Electric Prunes, he was asked to record a similar album by [[Capitol Records]], for whom he worked as a staff producer and songwriter.<ref>{{harvnb|Anon.|1968b|p=4}}; {{harvnb|Anon.|2006|p=136}}; {{harvnb|Clayre|1969|p=1482}}</ref> Axelrod wanted to further capitalize on the experimental climate of [[popular music]] at the time and chose to adapt works by English poet [[William Blake]] on an album.{{sfn|Anon.|1968b|p=4}}

At the time, Blake musical settings were at the height of their popularity among musicians and composers.{{sfn|Fitch|1990|p=xxvi}} Numerous [[art music|serious music]] composers had set his poems to music since the 1870s, and the practice was eventually adapted in other musical fields during the 20th&nbsp;century, including popular music, [[musical theatre]], and the 1960s&nbsp;[[American folk music revival|folk]] [[instrumental idiom|idiom]].<ref>{{harvnb|Fitch|1990|pp=xxi–xxiii}}; {{harvnb|Fitch|1990|pp=xxi–xxii}}; {{harvnb|Fitch|1990|p=xxv}}</ref> Axelrod, a self-professed "Blake freak", had been fascinated by Blake's painting and poetry since his late teens and frequently read the poems as an adult.<ref>{{harvnb|Murph|2005}}; {{harvnb|Howard|2004|p=99}}; {{harvnb|Unterberger|2000}}</ref> He conceived ''Song of Innocence'' after he had bought an edition of Blake's complete poetry while working in Capitol's art department and considered the concept for a few years before ''Mass in F Minor''.<ref>{{harvnb|Monson|1968|p=B5}}; {{harvnb|Unterberger|2000}}</ref> Axelrod was not sociable with colleagues, such as record executives who could have helped him professionally, and felt that he could identify with Blake; he considered the poet "very bad at making new friends".{{sfn|Murph|2005}}

Axelrod composed ''Song of Innocence'' in one week and began recording in mid-1968.<ref>{{harvnb|Unterberger|2000}}; {{harvnb|Howard|2004|p=99}}</ref> He recorded the album at [[Capitol Studios]] in Los Angeles and enlisted his close-knit group of veteran studio musicians, including keyboardist and conductor [[Don Randi]], guitarist [[Al Casey (rock & roll guitarist)|Al Casey]], bassist [[Carol Kaye]], and drummer [[Earl Palmer]].<ref>{{harvnb|Anon.|2001}}; {{harvnb|Howard|2004|p=99}}</ref> He had worked with them when producing sessions for other recording artists.{{sfn|Anon.|1968b|p=4}} Axelrod did not play any instruments on ''Song of Innocence''; he instead wrote arrangements for his orchestra and utilized 33&nbsp;players to perform his [[Chord chart|notated charts]].<ref>{{harvnb|Anon.|1969|p=25}}; {{harvnb|Fitch|1990|p=9}}; {{harvnb|Anon.|1968b|p=4}}</ref> He had learned how to read and orchestrate complex music charts from [[jazz]] musicians during the 1950s.{{sfn|Anon.|1969|p=25}} Randi conducted the orchestra and played both piano and [[organ (music)|organ]] on the record.{{sfn|Anon.|1968b|p=4}} Axelrod preferred listening to a session from a recording booth like his contemporary [[Igor Stravinsky]]. "That way the sounds don't seem to go all over the place", he later said. "Music seems so small in a studio."{{sfn|Monson|1968|p=B5}} Axelrod originally wanted some of the album's compositions to feature a large-scale [[choir]] but was uncertain if he could find the appropriate ensemble, so he recorded an entirely instrumental album and included one Blake setting for each [[Section (music)|section]] of the [[sheet music|score]].{{sfn|Howard|2004|p=99}}

== Composition ==
{{Listen|pos = left
 |filename     = David Axelrod - Holy Thursday.ogg
 |title        = "Holy Thursday"
 |description  = "Holy Thursday" mixes [[boogaloo]] and classical elements with jazzy piano and a soaring [[refrain|chorus]] played by the string and horn sections.<ref>{{harvnb|Jurek|n.d.}}; {{harvnb|Kern|n.d.}}</ref>
}}

A [[jazz fusion]] album, ''Song of Innocence'' combines jazz elements with [[Impressionist music|impressionistic]] musical [[figure (music)|figures]] and [[hard rock]] guitar [[solo (music)|solos]].<ref>{{harvnb|Cotner|2006|p=53}}; {{harvnb|Anon.|1968b|p=4}}</ref> Its music also incorporates [[funk]], [[rock music|rock]], [[theatre music|theatre]], and [[pop music|pop]] styles.<ref>{{harvnb|Kern|n.d.}}; {{harvnb|Sonksen|2012}}</ref> Music journalists categorized the record as [[jazz-rock]], [[baroque pop]], and [[psychedelic soul|psychedelic R&B]].{{sfn|Sonksen|2012}} John Murph of ''[[JazzTimes]]'' magazine said the music could be better characterized as [[art pop]] than jazz.{{sfn|Murph|2005}} Axelrod, who had produced [[bebop]] albums before working for Capitol, asserted that jazz played a crucial role in the music: "For years, all I did was jazz. When I first got in the record business, I was so into jazz that I had never heard [[Elvis Presley]]. I still probably listen to jazz more than anything else."{{sfn|Murph|2005}}

Axelrod composed the album as a [[Symphonic poem|tone poem]] suite based on Blake's illustrated 1789 collection of poems ''[[Songs of Innocence and of Experience|Songs of Innocence]]''.{{sfn|Kresh|1969|p=115}} His compositions borrowed titles from Blake's poems, which dealt with themes such as [[Vision (spirituality)|visions]], religious iniquity, [[rite of passage]], and life experience after a person's birth and innocence.<ref>{{harvnb|Howey|Reimer|2006|p=532}}; {{harvnb|Anon.|2006|p=136}}; {{harvnb|Sonksen|2012}}</ref> Mary Campbell of ''[[The Baltimore Sun]]'' said the classical and Christian [[church music]] elements made the record sound "reverent, as if describing a biblical story".{{sfn|Campbell|1968|p=B4}} ''[[Les Inrockuptibles]]'' described it as a "[[Psyche (psychology)|psyche]]-[[liturgy|liturgical]]" work dedicated to Blake.{{sfn|Conte|2000}} According to [[AllMusic]]'s Thom Jurek, [[psychedelia]] was implicit in the record's [[musical form]] and feeling, which impelled Axelrod to "celebrate the wildness and folly of youth with celebration and verve".{{sfn|Jurek|n.d.}}

The album's music was written in the rock idiom and arranged for bass, drums, and strings.{{sfn|Kresh|1969|p=115}} As a composer, Axelrod abandoned the conventional unison approach to orchestral writing in favor of more [[contrast (music)|contrasts]] while centering his [[tempo]]s around rock-based drum patterns played mostly in [[common time]] by Palmer.<ref>{{harvnb|Campbell|1968|p=B4}}; {{harvnb|Anon.|1979|p=68}}</ref> He utilized his instrumental ensemble as a [[symphonic rock|rock orchestra]], playing melodramatic strings and pronounced, echo-laden [[breakbeat]]s.<ref>{{harvnb|Jurek|n.d.}}; {{harvnb|Bush|n.d.}}.</ref> The music was also embellished with [[electric piano]], intricate [[bassline]]s, [[Echoplex]] effects, and elements of suspense Axelrod used to reflect the supernatural themes found in Blake's poems.<ref>{{harvnb|Cotner|2006|p=53}}; {{harvnb|Sonksen|2012}}.</ref> According to David N. Howard, the album's "euphorically" upbeat psychedelic R&B form was interspersed by "dramatically sparse" and "harrowing" arrangements.{{sfn|Howard|2004|p=99}}

Axelrod and his musicians used key musical [[phrase (music)|phrases]] that are expanded upon throughout ''Song of Innocence''.{{sfn|Kern|n.d.}} He was interested in [[György Ligeti]]'s 1961 piece ''[[Atmosphères]]'' and [[Lukas Foss]]' idea of starting a piece with a [[added tone chord|sustained chord]], having musicians improvise over 100&nbsp;[[bar (music)|bars]], and ending with another chord as they finish.{{sfn|Monson|1968|p=B5}} "Urizen" opened with long sustained chords, [[sound effect]]s, [[reverberation|reverb]]ed guitar [[Stab (music)|stab]]s, and a supple bassline.<ref>{{harvnb|Kresh|1969|p=115}}; {{harvnb|Kern|n.d.}}</ref> On "Holy Thursday", the [[rhythm section]] played a slow, jazzed-out [[groove (music)|groove]] and [[blues]]y bop piano lines, as a [[big band]] [[vamp (music)|vamp]] was played by a large-scale [[string section]]. In response to their [[Swing (jazz performance style)|swing style]], the [[brass section]] and guitarists played dramatic, high-pitched overtones built around a complex [[melody]].{{sfn|Jurek|n.d.}} The middle of the album featured more traditional jazz passages and the presence of a psychedelic [[harpsichord]].{{sfn|Kern|n.d.}} "The Smile" was recorded with a rhythmic drum beat, offbeat bass, and a progressive string part.{{sfn|Weiner|Sylvester|Eells|Christgau|2008}} For the songs near the end, the musicians steadily transitioned to heady psychedelia featuring gritty guitars and disorienting organ [[lick (music)|licks]].{{sfn|Kern|n.d.}} On "The Mental Traveler", Axelrod said he tried to experiment with [[atonality]] but "chickened out".{{sfn|Monson|1968|p=B5}}

== Reception and legacy ==
''Song of Innocence'' was released in October 1968 by Capitol Records.{{sfn|Schwann|1970|p=229}} It received radio exposure on both [[AM broadcasting|AM]] and [[FM broadcasting|FM]] stations with songs such as the title track and "Holy Thursday", which became the album's best-known recording.<ref>{{harvnb|Anon.|1968b|p=4}}; {{harvnb|Jurek|n.d.}}</ref> The album was not a commercial success and only sold 75,000 copies by October 1969.<ref>{{harvnb|Howard|2004|p=99}}; {{harvnb|Anon.|1969|p=25}}</ref> In a contemporary review, ''[[Billboard (magazine)|Billboard]]'' magazine called it "an aesthetic mix of music and philosophy&nbsp;... chock full of [[mysticism]], creativity, and change", believing that Axelrod's idyllic music would be interesting enough to impact the [[record chart]]s.{{sfn|Anon.|1968a|p=30}} Writing for ''[[Gramophone (magazine)|Gramophone]]'', [[Alasdair Clayre]] said Axelrod's impressions of Blake "reveal a depth of imagination and skill warranting attention beyond the confines of pop music", proving he could compose innovatively for a large orchestra, which Clayre felt comprised the best of California's studio musicians on the album. He questioned whether the "occasional guitar gobbling" reflected Axelrod's genuine ideas or "an obligatory concession to contemporary sound" on an otherwise compelling record.{{sfn|Clayre|1969|p=1482}} The magazine's Nigel Hunter found the record's songs to be "of absorbing power and depth", but complained of the electric guitar parts.{{sfn|Hunter|1969|p=928}} Nat Freedland from ''Entertainment World'' was more critical, accusing Axelrod of "indulging himself here to little avail".{{sfn|Freedland|1969|p=45}} ''[[Stereo Review]]'' magazine's Paul Kresh panned ''Song of Innocence'' as a pretentious, inadequate album dependent on [[movie music]] tricks and outdated techniques such as forced climaxes and gaudy orchestration. He said it fell severely short of the concept Axelrod aspired to and that "only the most uneducated will be taken in by the mountains of misterioso claptrap that surround these squeaking musical mice".{{sfn|Kresh|1969|p=115}}

{{quote box|quote="''Song of Innocence'' made critics turn their heads in its day, regarding it as a visionary curiosity piece; today it's simply a great, timeless work of [[pop art]] that continues to inspire over three decades after its initial release."|source=—Thom Jurek, [[AllMusic]]{{sfn|Jurek|n.d.}}|width=20%|align=right|style=padding:8px;|border=2px}}

''Song of Innocence'' was one of many [[concept album]]s recorded as rock music was developing in various directions during the late 1960s, following in the wake of [[the Beatles]]' 1967 album ''[[Sgt. Pepper's Lonely Hearts Club Band]]''.{{sfn|Lewis|1972|pp=98–99}} It was innovative for its original application of both rock and jazz [[Musical technique|techniques]].{{sfn|Tiegel|1971|p=58}} According to music journalist Zaid Mudhaffer, the term "jazz fusion" was coined in a review of the record when it was released.{{sfn|Mudhaffer|2014}} Axelrod followed the album up in 1969 with the similarly Blake-inspired ''[[Songs of Experience (album)|Songs of Experience]]'', which adapted [[Gunther Schuller]]'s [[third stream]] concept to [[baroque music|baroque]] orchestrations and rock, pop, and [[rhythm and blues|R&B]] rhythms and melodies.{{sfn|Murph|2005}} Both albums established Axelrod as an unpredictable, challenging [[conceptual art]]ist.{{sfn|Howard|2004|p=99}} His instrumental interpretations of Blake were the first in jazz, followed in 1971 by [[Rafał Augustyn (composer)|Rafał Augustyn]]'s ''Niewinnosc'' and [[Adrian Mitchell]]'s musical ''Tyger: A Celebration of William Blake'' with composer [[Mike Westbrook]]; Westbrook later composed more Blake-inspired works, including ''The Westbrook Blake: "Bright as Fire"'' (1980).<ref>{{harvnb|Fitch|1990|p=xxv}}; {{harvnb|Fitch|1990|p=8}}; {{harvnb|Fitch|1990|pp=241–242}}</ref>

During the late 1990s, Axelrod's records were reassessed and considered innovative by critics, including ''Song of Innocence'', which was regarded as a classic.<ref>{{harvnb|Unterberger|n.d.}}; {{harvnb|Wolfson|2004}}</ref> ''[[Mojo (magazine)|Mojo]]'' cited it as "the heart of Axelrod's legacy", while ''[[NME]]'' called it "sky-kissingly high and divine", finding Axelrod versatile enough to "soar above his own pretensions",<ref>{{harvnb|Anon.|2000a}}; {{harvnb|Anon.|2006|p=136}}.</ref> Writing for AllMusic, John Bush said the album "sounded like nothing else from its era", while Thom Jurek argued that it continued to sound new upon each listen due to a lack of "cynicism and hipper-than-thou posturing" in the music.<ref>{{harvnb|Jurek|n.d.}}; {{harvnb|Bush|n.d.}}.</ref> In a four-and-a-half star retrospective review, Jurek said it was innovative in 1968 and still "withstands the test of time better than the Beatles ''Sgt. Pepper's Lonely Hearts Club Band'' album that allegedly inspired it".{{sfn|Jurek|n.d.}} Giving it a five-out-of-five score, ''[[Tiny Mix Tapes]]'' said it sounded engagingly magnificent and diverse, citing it as one of the most dynamic musical fusions and "one of the most unique and thought provoking musical efforts of the last several decades".{{sfn|Kern|n.d.}}

[[File:DJ Shadow and James Lavelle.jpg|left|thumb|''Song of Innocence'' has been sampled by various record producers since being rediscovered in the 1990s, including [[DJ Shadow]] (left).]]

Axelrod's music was also rediscovered and [[sampling (music)|sampled]] by leading [[disc jockey]]s in the 1990s including [[hip hop production|hip hop producers]].{{sfn|Keenan|2000|p=54}} When sampling in hip hop peaked during the early and mid-1990s, they searched for archived records with atmospheric beats and strings to sample. Los Angeles-based disc jockey B+ recalled finding a copy of ''Song of Innocence'' at a [[Goodwill Industries|Goodwill]] in [[Culver City]] and said it appealed to him because of its [[consonance and dissonance|dissonant]] quality, musical [[dynamics (music)|dynamics]], and string sound: "This big sound. It was like somehow [Axelrod] was summoning the future, that you can project this environment, this moment into the future."{{sfn|George|2007}} [[Electronica]] pioneers such as DJ Cam and [[DJ Shadow]] also sampled ''Song of Innocence''.{{sfn|Jurek|n.d.}} The latter producer sampled the record's choral themes and piano motifs on his influential debut album ''[[Endtroducing.....]]'' (1996).{{sfn|Anon.|2007|p=630}} "The Smile" was sampled by [[Pete Rock]] on his 1998 song "Strange Fruit" and by [[DJ Premier]] on [[Royce da 5'9"]]'s 2009 song "Shake This".<ref>{{harvnb|Fennessey|2005}}; {{harvnb|Mudhaffer|2014}}</ref> "Holy Thursday" was frequently sampled by producers, including [[The Beatnuts]] on their 1994 song "[[Hit Me with That]]", [[UNKLE]] on their 1998 song "[[Rabbit in Your Headlights]]", and [[Swizz Beatz]] on [[Lil Wayne]]'s 2008 song "Dr. Carter".<ref>{{harvnb|Goh|2006}}; {{harvnb|Anon.|2013}}; {{harvnb|Fennessey|2005}}; {{harvnb|Benbow|2008}}</ref>

The renewed interest in Axelrod's work prompted [[Stateside Records]] to [[reissue]] ''Song of Innocence'' in 2000.{{sfn|Anon.|2009}} ''[[Now (newspaper)|Now]]'' wrote that after sounding odd during the 1960s, the songs had become "a sampler's dream come true&nbsp;– who knew?"{{sfn|Anon.|1999}} [[David Keenan]] of ''[[The Wire (magazine)|The Wire]]'' attributed Axelrod's sampling legacy with producers such as DJ Premier and DJ Shadow to Palmer, "the original badass drummer [who] played on all of these tracks". He facetiously critiqued that the album's songs "may reek of stale [[joss stick]]s and [[patchouli]]-scented [[self-actualisation]], but in their very datedness they somehow sound very modern."{{sfn|Keenan|2000|p=54}} ''[[Pitchfork (website)|Pitchfork]]'' journalist Sean Fennessey felt Axelrod's first two records were "essential if only as a tour guide through early 90s hip-hop", having "literally been a rap producer's delight for years".{{sfn|Fennessey|2005}} In a 2013 list for ''[[Complex (magazine)|Complex]]'', DJ and production duo Kon and Amir named "Holy Thursday" the greatest hip hop sample of all time.{{sfn|Anon.|2013}}

== Track listing ==
All songs written, arranged, and produced by [[David Axelrod (musician)|David Axelrod]].{{sfn|Anon.|2000b}}

{{Track listing
| headline        = Side one
| title1          = Urizen
| length1         = 3:56
| title2          = Holy Thursday
| length2         = 5:30
| title3          = The Smile
| length3         = 3:25
| title4          = A Dream
| length4         = 2:26
}}

{{Track listing
| headline        = Side two
| title1          = Song of Innocence
| length1         = 4:32
| title2          = Merlin's Prophecy
| length2         = 2:43
| title3          = The Mental Traveler
| length3         = 4:03
}}

== Personnel ==
Credits are adapted from the album's liner notes.{{sfn|Anon.|2000b}}
{{columns-list|colwidth=20em|
* [[David Axelrod (musician)|David Axelrod]] – arranger, producer, vocals
* Benjamin Barrett – musician
* Arnold Belnick – musician
* [[Harry Bluestone]] – violin
* Bobby Bruce – musician
* [[Al Casey (rock & roll guitarist)|Al Casey]] – guitar
* Gary Coleman – background vocals
* Douglas Davis – musician
* Alvin Dinkin – musician
* Gene Estes – percussion
* Anne Goodman – musician
* Freddie Hill – trumpet
* Bill Hinshaw – musician
* Harry Hyams – musician
* [[Carol Kaye]] – bass
* Raphael Kramer – musician
* Richard Leith – trombone
* Arthur Maebe – horn
* Leonard Malarsky – musician
* Lew McCreary – horn
* [[Ollie Mitchell]] – trumpet
* Gareth Nuttycombe – musician
* Rob Owen – reissue co-ordination (2000)
* [[Earl Palmer]] – drums
* Joe Polito – engineer
* [[Don Randi]] – conductor, organ, piano
* The Red Room – artwork re-generation (2000)
* Nigel Reeve – reissue co-ordination (2000)
* Allen Di Rienzo – musician
* [[Howard Roberts]]  – guitar
* [[Vincent DeRosa|Vincent de Rosa]] – horn
* Nathan Ross – musician
* Henry Roth – musician
* Myron Sandler – musician
* Harold Schneier – musician
* Sid Sharp – musician
* Jack Shulman – musician
* Henry Sigismonti – [[wind instrument|wind]]
* Marshall Sosson – musician
* [[Tony Terran]] – musician
* Pete Wyant – musician
* Tibor Zelig – musician
}}

== Release history ==
{| class="wikitable sortable plainrowheaders"
|-
!Region
!Date
!Label
!Format
!Catalog
|-
|United States{{sfn|Schwann|1970|p=229}}
|October 1968
|rowspan="3"|[[Capitol Records]]
|rowspan="2"|[[stereophonic sound|stereo]] [[LP album|LP]]
|rowspan="2"|ST-2982
|-
|United Kingdom{{sfn|Anon.|n.d.}}
|1968
|-
|United States{{sfn|Fitch|1990|p=9}}
|1975
|stereo LP [[reissue]]
|ST-11362
|-
|United Kingdom{{sfn|Keenan|2000|p=54}}
|2000
|[[Stateside Records]], [[EMI]]
|[[Compact Disc|CD]] reissue
|5 21588
|}

== See also ==
{{Portal|Jazz}}
* [[William Blake in popular culture]]
{{Clear}}

== References ==
{{reflist|20em}}

== Bibliography ==
{{refbegin|30em}}
* {{cite journal|ref={{SfnRef|Anon.|1968a}}|author=Anon.|date=September 28, 1968|url=https://books.google.com/books?id=xQoEAAAAMBAJ&pg=PA30#v=onepage&q&f=false|title=Album Reviews|journal=Billboard|location=Cincinnati|volume=80|issue=39|page=30|accessdate=November 20, 2012}}
* {{cite journal|ref={{SfnRef|Anon.|1968b}}|author=Anon.|date=September 28, 1968|url=https://books.google.com/books?id=xQoEAAAAMBAJ&pg=PA4#v=onepage&q&f=false|title=Axelrod: Hunter of New Sounds|journal=[[Billboard (magazine)|Billboard]]|location=Los Angeles|volume=80|issue=39|accessdate=November 20, 2012}}
* {{cite journal|ref=harv|author=Anon.|date=October 3, 1969|journal=Entertainment World|title=Al DeLory & David Axelrod|volume=1|issue=1|location=Hollywood}}
* {{cite journal|ref=harv|author=Anon.|date=January 20, 1979|title=Talent in Action – Freddie Hubbard, Joe Sample, Joe Farrell, David Axelrod|journal=Billboard|page=68|location=Los Angeles|volume=91|issue=3|url=https://books.google.com/books?id=JCUEAAAAMBAJ&pg=PT67#v=onepage&q&f=false|accessdate=November 20, 2012}}
* {{cite news|ref=harv|author=Anon.|date=December 28, 1999|url=http://www.nowtoronto.com/music/story.cfm?content=125593|title=Top 10 Reissues|newspaper=[[Now (newspaper)|Now]]|location=Toronto|volume=20|issue=17|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6Eb0P6Ijc|archivedate=February 21, 2013|deadurl=no}}
* {{cite journal|ref={{SfnRef|Anon.|2000a}}|author=Anon.|issue=May 17|year=2000|url=http://www.nme.com/reviews/david-axelrod/2269|title=Songs of Innocence/ Songs of Experience|journal=[[NME]]|location=London|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6ETWfWxMM|archivedate=February 16, 2013|deadurl=no}}
* {{cite AV media notes|ref={{SfnRef|Anon.|2000b}}|author=Anon.|year=2000|title=Song of Innocence|others=David Axelrod|type=CD reissue booklet|publisher=[[Stateside Records]]|id=7243 5 21588 2 9|location=London}}
* {{cite journal|ref=harv|author=Anon.|year=2001|journal=[[The Fader]]|issue=7|title=David Axelrod|location=New York|url=http://www.stonesthrow.com/news/2001/09/in-the-studio-with-axelrod|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EcIRRfZk|archivedate=February 22, 2013|deadurl=no}}
* {{cite journal|ref={{SfnRef|Anon.|2006}}|author=Anon.|date=January 2006|title=Reissues|journal=[[Mojo (magazine)|Mojo]]|location=London|issue=146}}
* {{cite book|ref={{SfnRef|Anon.|2007}}|author=Anon.|title=[[The Mojo Collection]]|edition=4th|publisher=[[Canongate Books]]|year=2007|isbn=1-84767-643-X|chapter=DJ Shadow|editor1-first=Jim|editor1-last=Irvin|editor1-link=Jim Irvin|editor2-first=Colin|editor2-last=McLear}}
* {{cite journal|ref=harv|author=Anon.|issue=March 5|year=2009|url=http://www.factmag.com/staging/2009/03/05/david-axelrods-live-at-the-royal-festival-hall-on-vinyl/|title=David Axelrod's 'Live at the Royal Festival Hall' on vinyl|journal=[[Fact (UK magazine)|Fact]]|location=London|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EcGbO1wG|archivedate=February 22, 2013|deadurl=no}}
* {{cite journal|ref=harv|author=Anon.|issue=May 17|year=2013|url=http://www.complex.com/music/2013/05/kon-amir-present-the-50-greatest-hip-hop-samples-of-all-time/david-axelrod-holy-Thursday|title=1. David Axelrod "Holy Thursday" (1968) — Kon + Amir Present: The 50 Greatest Hip-Hop Samples of All Time|journal=[[Complex (magazine)|Complex]]|location=New York|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6HQcWOFsS|archivedate=June 16, 2013|deadurl=no}}
* {{cite web|ref=harv|author=Anon.|year=n.d.|url=http://www.rarerecordpriceguide.com/view-record/iGmAqA%3D%3D|title=Rare Record Price Guide|publisher=[[Metropolis International]]|accessdate=November 20, 2012}} {{subscription required}}
* {{cite news|ref=harv|last=Benbow|first=Julian|issue=June 9|year=2008|url=http://www.boston.com/ae/music/articles/2008/06/10/on_carter_iii_lil_wayne_perfects_the_art_of_the_brag/|title=On 'Carter III,' Lil' Wayne perfects the art of the brag|newspaper=[[Boston Globe]]|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6Edx5lYSl|archivedate=February 23, 2013|deadurl=no}}
* {{cite web|ref=harv|last=Bush|first=John|year=n.d.|url=http://www.allmusic.com/artist/david-axelrod-mn0000634377|title=David Axelrod – Music Biography, Credits and Discography|publisher=AllMusic|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EKyqPWDZ|archivedate=February 10, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Campbell|first=Mary|date=October 21, 1968|url=http://pqasb.pqarchiver.com/baltsun/access/1750468082.html?FMT=ABS&FMTS=ABS:AI&type=historic&date=Oct+21%2C+1968&author=&pub=The+Sun+(1837-1985)&desc=Rock+Influence&pqatl=google|title=Rock Influence|newspaper=[[The Baltimore Sun]]|accessdate=November 20, 2012}} {{subscription required}}
* {{cite journal|ref=harv|last1=Weiner|first1=Johan|last2=Sylvester|first2=Nick|last3=Eells|first3=Josh|last4=Christgau|first4=Robert|authorlink4=Robert Christgau|date=June 11, 2008|url=http://www.robertchristgau.com/xg/bl/carter-08.php|title=The Great Lil Wayne Debate: Is Tha Carter III a Classic?|journal=[[Blender (magazine)|Blender]]|location=New York|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EPlIIopp|archivedate=February 13, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Clayre|first=Alasdair|authorlink=Alasdair Clayre|date=April 1969|title=Keeping Track: A Cross Section of Recent Popular LPs, EPs and Singles|journal=[[Gramophone (magazine)|Gramophone]]|location=London|volume=46}}
* {{cite journal|ref=harv|last=Conte|first=Christophe|issue=November 30|year=2000|title=David Axelrod|journal=[[Les Inrockuptibles]]|language=French|location=Paris}}
* {{cite journal|ref=harv|last=Cotner|first=David|year=2006|title=David Cotner scopes out 7" records, 3" CDs and other odds & ends|journal=Signal to Noise|location=Burlington|issue=41}}
* {{cite web|ref=harv|last=Fennessey|first=Sean|date=September 15, 2005|url=http://pitchfork.com/reviews/albums/392-the-edge-david-axelrod-at-capitol-1966-1970/|title=David Axelrod: The Edge: David Axelrod At Capitol 1966–1970|work=[[Pitchfork (website)|Pitchfork]]|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EcEgHXol|archivedate=February 22, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Fitch|first=Donald|title=Blake Set to Music: A Bibliography of Musical Settings of the Poems and Prose of William Blake|publisher=[[University of California Press]]|year=1990|isbn=0520097343}}
* {{cite journal|ref=harv|last=Freedland|first=Nat|journal=Entertainment World|title=Music–Recording|date=October 3, 1969|volume=1|issue=1|location=Hollywood}}
* {{cite news|ref=harv|last=George|first=Lynell|issue=June 3|year=2007|url=http://articles.latimes.com/2007/jun/03/magazine/tm-axelrod22|title=Replay|newspaper=Los Angeles Times|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EV6CauCo|archivedate=February 17, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Goh|first=Daryl|issue=February 27|year=2006|url=http://ecentral.my/news/story.asp?file=/2006/2/27/music/13472560&sec=music|title=David Axelrod's The Edge of Music: An Exploration of 50 Years in Music|newspaper=[[The Star (Malaysia)|The Star]]|location=Malaysia|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6Edwx5b9g|archivedate=February 23, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Howard|first=David N.|title=Sonic Alchemy: Visionary Music Producers and Their Maverick Recordings|publisher=[[Hal Leonard Corporation]]|year=2004|isbn=0634055607}}
* {{cite book|ref={{SfnRef|Howey|Reimer|2006|p=532}}|last1=Howey|first1=Ann F.|last2=Stephen Ray|first2=Reimer|year=2006|title=A Bibliography of Modern Arthuriana (1500–2000)|publisher=[[Boydell & Brewer]]|isbn=1843840685}}
* {{cite journal|ref=harv|last=Hunter|first=Nigel|issue=December|year=1969|title=The Critics' Choice—1969|journal=Gramophone|location=London|page=928|volume=47}}
* {{cite web|ref=harv|last=Jurek|first=Thom|year=n.d.|url=http://www.allmusic.com/album/song-of-innocence-mw0000739065|title=Song of Innocence – David Axelrod : Songs, Reviews, Credits, Awards|publisher=[[AllMusic]]|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6ELOPjbHZ|archivedate=February 10, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Keenan|first=David|authorlink=David Keenan|title=Boomerang – New reissues: rated on the rebound|journal=[[The Wire (magazine)|The Wire]]|location=London|page=54|year=2000|issue=197–202}}
* {{cite web|ref=harv|author=Kern|year=n.d.|url=http://www.tinymixtapes.com/music-review/david-axelrod-songs-innocence|title=David Axelrod – Songs of Innocence|publisher=[[Tiny Mix Tapes]]|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6ELQsvqG2|archivedate=February 10, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Kresh|first=Paul|title=Stereo Tape|journal=[[Stereo Review]]|location=New York|page=115|volume=22|issue=3|date=March 1969}}
* {{cite book|ref=harv|last=Lewis|first=Roger|year=1972|title=Outlaws of America: The Underground Press and Its Context|publisher=[[Penguin Books]]|isbn=0140215867}}
* {{cite news|ref=harv|last=Monson|first=Karen|issue=December 31|year=1968|url=http://pqasb.pqarchiver.com/latimes/access/527884532.html?dids=527884532:527884532&FMT=ABS&FMTS=ABS:AI&type=historic&date=Dec+31%2C+1968&author=&pub=Los+Angeles+Times&desc=HIS+OWN+THING&pqatl=google|title=His Own Things|newspaper=[[Los Angeles Times]]|accessdate=November 20, 2012}} {{subscription required}}
* {{cite web|ref=harv|last=Mudhaffer|first=Zaid|date=January 20, 2014|url=http://www.redbullmusicacademy.com/magazine/david-axelrod-guide|title=Heavy Axe: A Guide to David Axelrod|publisher=Red Bull Music Academy|accessdate=June 23, 2014|archiveurl=http://www.webcitation.org/6QYg6Cglu|archivedate=June 23, 2014|deadurl=no}}
* {{cite journal|ref=harv|last=Murph|first=John|url=http://jazztimes.com/articles/16176-autumn-chill-41-of-the-season-s-best-albums-part-ii|issue=November|year=2005|title=Autumn Chill: 41 of the Season's Best Albums, Part II|journal=[[JazzTimes]]|location=Quincy|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6EI6Lc6MK|archivedate=February 8, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Schwann|first=William|journal=[[Schwann catalog|Schwann Monthly Guide to Stereo Records]]|year=1970|volume=22|issue=1–3|location=Cambridge|title=Long Playing Record Catalog}}
* {{cite web|ref=harv|last=Sonksen|first=Mike|date=June 15, 2012|url=http://www.kcet.org/socal/departures/landofsunshine/la-letters/songs-of-innocence-and-experience-the-tone-poems-of-david-axelrod-and-william-blake.html|title=LA Letters > Songs of Innocence and Experience: The Tone Poems of David Axelrod and William Blake|publisher=[[KCET]]|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6FaDWivqv|archivedate=April 2, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Tiegel|first=Eliot|date=July 17, 1971|url=https://books.google.com/books?id=twgEAAAAMBAJ&pg=PA58#v=onepage&q&f=false|title=Axelrod: Trend to Blend Sounds|location=Los Angeles|journal=Billboard|volume=83|issue=29|accessdate=November 20, 2012}}
* {{cite AV media notes|ref=harv|last=Unterberger|first=Richie|authorlink=Richie Unterberger|year=2000|title=[[Rock Messiah|David Axelrod's Rock Interpretation of Handel's Messiah]]|others=[[David Axelrod (musician)|David Axelrod]]|publisher=[[RCA Records]]|id=LSP-4636|location=New York|type=LP reissue liner notes}}
* {{cite web|ref=harv|last=Unterberger|first=Richie|year=n.d.|url=http://www.allmusic.com/album/heavy-axe-mw0000421998|title=Heavy Axe – David Axelrod|publisher=AllMusic|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6Dct7Gb9s|archivedate=January 12, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Wolfson|first=Richard|authorlink=Richard Wolfson (musician)|date=March 16, 2004|url=http://www.telegraph.co.uk/culture/music/3613910/The-alchemist-of-crossover.html|title=The alchemist of crossover|newspaper=[[The Daily Telegraph]]|location=London|accessdate=November 20, 2012|archiveurl=http://www.webcitation.org/6Eb3LqGD6|archivedate=February 21, 2013|deadurl=no}}
{{refend}}

== Further reading ==
* {{cite journal|last=Smith|first=RJ|date=March 2001|url=https://books.google.com/books?id=j18EAAAAMBAJ&pg=PA136#v=onepage&q&f=false|title=Groovy Again|journal=[[Los Angeles (magazine)|Los Angeles Magazine]]}}

== External links ==
* {{Official website|http://www.davidaxelrodmusic.com/}}
* {{Discogs master|67456|type=album}}
* ''[http://www.whosampled.com/sampled/David%20Axelrod/ Song of Innocence]'' at [[WhoSampled]]
{{Spoken Wikipedia|EN Song Of Innocence-Article.ogg|2015-01-06}}

[[Category:1968 debut albums]]
[[Category:Adaptations of works by William Blake]]
[[Category:Albums conducted by Don Randi]]
[[Category:Albums produced by David Axelrod (musician)]]
[[Category:Capitol Records albums]]
[[Category:Concept albums]]
[[Category:David Axelrod (musician) albums]]
[[Category:Instrumental albums]]
[[Category:Jazz fusion albums]]
[[Category:Baroque pop albums]]