{{Infobox journal
| title = Policy Sciences
| discipline = [[Public policy]]
| abbreviation = Policy Sci.
| editor = Michael Howlett
| publisher = [[Springer Science+Business Media]] on behalf of the [[Society of Policy Scientists]]
| frequency = Quarterly
| history = 1970–present
| impact = 2.457
| impact-year = 2014
| website = http://www.springer.com/social+sciences/political+science/journal/11077
| link2 = http://link.springer.com/journal/volumesAndIssues/11077
| link2-name = Online archive
| ISSN = 0032-2687
| eISSN = 1573-0891
| OCLC = 905451440
| JSTOR = 00322687
| LCCN = 79023541
}}
'''''Policy Sciences''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering issues and practices in the [[policy studies]]. It was established in 1970 and is published by [[Springer Science+Business Media]] on behalf of the [[Society of Policy Scientists]]. The [[editor-in-chief]] is Michael Howlett ([[Simon Fraser University]] and [[National University of Singapore]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-04-26}}</ref>
*[[EBSCO Information Services|EBSCO databases]]
*[[EconLit]]
*[[Geobase]]<ref name=GEOBASE>{{cite web |url=http://www.elsevier.com/online-tools/engineering-village/contentdatabase-overview |title=Content/Database Overview - GEOBASE Source List |publisher=[[Elsevier]] |work=Engineering Village |accessdate=2016-04-26}}</ref>
*[[International Bibliography of Periodical Literature]]
*[[International Bibliography of the Social Sciences]]
*[[International Political Science Abstracts]]
*[[ProQuest|ProQuest databases]]
*[[Research Papers in Economics]]
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-04-26}}</ref>
*[[Social Science Citation Index]]<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.457, ranking it fourth out of 55 journals in the category "Planning and Development",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Planning and Development |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>
third out of 46 journals in the category "Public Administration",<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Public Administration |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref> and fourth out of 95 journals in the category "Social Sciences, Interdisciplinary".<ref name=WoS3>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Social Sciences, Interdisciplinary |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==Editors-in-chief==
The following persons are or have been editor-in-chief of the journal:
{{columns-list|colwidth=30em|
*1970-1973 E.S. Quade ([[RAND Corporation]])
*1974-1977 Garry Brewer (RAND Corporation/[[University of South Carolina]]/[[Yale University]])
*1978-1980 Thomas J. Anton ([[University of Michigan]])
*1981-1983 Peter de Leon ([[Columbia University]])
*1984-1986 Ronald Brunner ([[University of Colorado Boulder]])
*1987-1990 William Ascher ([[Duke University]])
*1991-1992 Garry Brewer (Yale University)
*1992-1995 Douglas Torgerson ([[Trent University]])
*1996-1998 Patrick Larkey ([[Carnegie Mellon University]])
*1999-2001 John D. Montgomery ([[Harvard University]])
*2002-2004 Steven Brown ([[Kent State University]])
*2005-2008 Matthew Auer ([[Indiana University]])
*2009-2014 Toddi Steelman ([[North Carolina State University]]/[[University of Saskatchewan]])
*2015-present Michael Howlett (Simon Fraser University/National University of Singapore)
}}

== Harold Lasswell Prize ==
Since 1984, the association has awarded the Harold Lasswell Memorial Prize for the best article published in the current volume of the journal. The prize selection committee usually is drawn from the [[editorial board]] and Springer Science+Business Media underwrites all expenses related to the prize.

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.springer.com/social+sciences/political+science/journal/11077}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Political science journals]]
[[Category:Publications established in 1970]]
[[Category:English-language journals]]