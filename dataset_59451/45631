{{refimprove|date=January 2013}}
{{Infobox engineer
|image                = Bundesarchiv Bild 183-L18396, Kurt Tank.jpg
|image_size           =
|caption              = Prof. Dr. Dipl.-Ing. Kurt Tank, March 1941
|name                 = Kurt Waldemar Tank
|nationality          = German
|birth_date           = 24 February 1898
|birth_place          = [[Bydgoszcz|Bromberg (Bydgoszcz)]], [[Province of Posen]]
|death_date           = {{d-da|5 June 1983|24 February 1898}}
|death_place          = [[Munich]]
|education            = [[Technical University of Berlin]]
|spouse               =
|parents              =
|children             =
|discipline           =
|institutions         =
|practice_name        =
|significant_projects = [[Focke-Wulf Fw 190]]<br/>[[Focke-Wulf Ta 152]]<br/>[[Focke-Wulf Fw 200]]<br/>[[FMA IAe 33 Pulqui II|FMA IAe 33]]<br/>[[HAL HF-24]]
|significant_design   =
|significant_advance  =
|significant_awards   = Honorary Professor with chair at [[Technical University of Braunschweig]]
}}
'''Kurt Waldemar Tank''' (24 February 1898 – 5 June 1983) was a German [[Aerospace engineering|aeronautical]] [[engineer]] and test pilot who led the design department at [[Focke-Wulf]] from 1931 to 1945. He was responsible for the creation of several important [[Luftwaffe]] aircraft of [[World War II]], including the [[Focke-Wulf Fw 190|Fw 190]] [[fighter aircraft]], the [[Focke-Wulf Ta 152|Ta 152]] [[fighter aircraft|fighter]]-[[Interceptor aircraft|interceptor]] and the [[Focke-Wulf Fw 200|Fw 200 Condor]] [[airliner]].<ref>{{cite news|url=http://www.time.com/time/magazine/article/0,9171,813605,00.html
|title=Old Hands, New Directions |publisher=[[TIME]]|accessdate=2008-02-24|last=|first= | date=October 23, 1950}}</ref><ref name=target>{{cite book|last=Duffy|first=James P.|title=Target: America: Hitler's Plan to Attack the United States|year=2004|publisher=The Lyons Press|location=Guilford, CT, USA|isbn=1-59228-934-7|pages=58|url=https://books.google.com/books?id=nVdWrzRIrQ4C&pg=PA58|quote="Tank would become one of the world's leading aircraft designers and engineers."}}</ref> After the war, Tank spent two decades designing aircraft abroad, working first in Argentina and then in India, before returning to Germany in the late 1960s to work as a consultant for [[Messerschmitt-Bölkow-Blohm]] (MBB).<ref>{{cite web|last=Zukowsky|first=John|title=Kurt Tank|url=http://www.britannica.com/EBchecked/topic/1518056/Kurt-Tank|publisher=Encyclopedia Britannica|accessdate=5 August 2012}}</ref>

==Early life==
Tank was born in [[Bydgoszcz|Bromberg (Bydgoszcz)]], [[Province of Posen]]. His grandfather was a cavalry sergeant in the [[Uhlans]] and his father, Willi Tank, was a [[grenadier]] sergeant in the [[3rd Division (German Empire)|3rd Division]].<ref>{{cite book | last = Conradis | first = Heinz | authorlink = | coauthors = | title = Design for Flight | publisher = Macdonald | year = 1960 | location = | pages = 216 | url = | doi = | id = | isbn = }}</ref> When [[World War I]] broke out Tank wished to join the [[German Army (German Empire)|''Deutsches Heer'']]'s then-named  [[Luftstreitkräfte|''Fliegertruppe'']] air service, but his father insisted he instead follow the family tradition and enlist in the cavalry. He ended the war as a captain, with many decorations for bravery.

==Career==
After the war, Tank graduated from the [[Technical University of Berlin]] in 1923. A mentor from the university secured him his first job, in the design department of [[Rohrbach Metall-Flugzeugbau|Rohrbach Metallflugzeug GmbH]], where he worked on flying boats and assisted in the design of the passenger aircraft, the [[Rohrbach Ro VIII|Ro VIII ''Roland'']].

Tank moved to the famous [[World War I]] firm, [[Albatros Flugzeugwerke]], where he worked as a test pilot. The Albatros company went [[bankruptcy|bankrupt]] in 1929 and in 1931, under government pressure, was merged with [[Focke-Wulf]]. [[Ludwig Roselius]] was chairman of the supervisory board of [[Focke-Wulf]] at that time and a Swiss banking syndicate under Max Schneider provided the funds for this takeover by Sanka Brücke (a Swiss holding company for [[Kaffee HAG]]) and [[OMGUS]] documents reveal that [[Café HAG]] was run via Switzerland and Bremen.<ref>The Office of Military Government US Zone in Post-War Germany 1946-1949, declassified per Executive Order 12958, Section 3.5 NND Project Number: NND 775057 by: NND Date: 1977.</ref> Tank then started work on the design of the [[Focke-Wulf Fw 44|Fw 44]] ''Stieglitz'' (Goldfinch), a two-seat civilian biplane. It was Focke-Wulf's first commercially successful design, launched in 1934.<ref name="WarThunder">{{cite web|title=Kurt Tank Anniversary|url=https://warthunder.com/en/news/465-Kurt-Tank-Anniversary-en|website=War Thunder|accessdate=9 November 2015}}</ref> This led to burgeoning growth for the company as Hitler began to prepare the country for war.

In 1936 Tank designed the [[Focke-Wulf Fw 200]] ''Condor'' to a [[Deutsche Luft Hansa]] specification. The first flight was in July 1937 after just under one year of development with Tank at the controls. The ''Condor'' made a famous non-stop flight from [[Berlin]] to [[New York City]] in 1938, proving the concept of transatlantic air travel.<ref name=target/> The ''Condor'' would later be used as a maritime [[patrol bomber]] aircraft of some repute during the war.

===World War II===
[[File:Bundesarchiv Bild 101I-676-7975A-28, Wunstorf, Major Günther Specht und Prof. Kurt Tank.jpg|thumb|left|upright|Major [[Günther Specht]] (left) and Prof. Kurt Tank (right)]]
The [[Focke-Wulf Fw 190|Fw 190]] ''Würger'' ([[Shrike]]), first flying in 1939 and produced from 1941 to 1945, was a mainstay [[Luftwaffe]] single-seat fighter during World War II, and Tank's most-produced (over 20,000) and famous design. In January 1943, he was named honorary professor with a chair at the [[Technical University of Braunschweig]], in recognition of his work developing aircraft.<ref name="WarThunder"/>

In 1944, the ''[[Reichsluftfahrtministerium]]'' (German Air Ministry) decided that new fighter aircraft designations must include the chief designer's name. Kurt Tank's new designs were therefore given the prefix Ta.<ref name="WarThunder"/> His most notable late-war design was the [[Focke-Wulf Ta 152|Ta 152]], a continuation of the Fw 190 design.

===Postwar===
After the war Tank negotiated with the United Kingdom, the Nationalist government of China, and representatives of the Soviet Union; when the negotiations proved unsuccessful, he accepted an offer from [[Argentina]] to work at its aerotechnical institute, the [[Instituto Aerotécnico]] in [[Córdoba, Argentina|Córdoba]] under the name of Pedro Matthies. The British government decided not to offer him a contract on the grounds that they could not see how he could be integrated into a research project or design group.<ref>{{cite book | last = Rathkolb | first = Oliver | title = Revisiting the National Socialist Legacy | publisher = Aldine Transaction | year = 2004 | page = 216 | isbn = 0-7658-0596-0}}</ref>

He moved to Córdoba in late 1946, with many of his Focke-Wulf co-workers.<ref name=AirPages>{{cite web|title=Kurt Tank (1898-1983)|url=http://www.airpages.ru/eng/lw/gs_fw.shtml|website=Aviation of World War II|accessdate=9 November 2015}}</ref> One of these was [[Ronald Richter]], who intended to power airplanes with nuclear energy, to be developed in the [[Huemul Project]], which was later proven to be a fraud according to some and a visionary according to others.<ref>{{cite journal|last1=Khatchadourian|first1=Raffi|title=A Star in a Bottle|journal=New Yorker, The|date=March 3, 2014|issue=March 2014|url=http://www.newyorker.com/magazine/2014/03/03/a-star-in-a-bottle|accessdate=9 November 2015}}</ref>

The Instituto Aerotécnico later became Argentina's military aeroplane factory, the [[Lockheed Martin Aircraft Argentina|Fábrica Militar de Aviones]].  There, he designed the [[IAe Pulqui II]] based on the [[Focke-Wulf Ta 183]] design that had reached mock-up stage by the end of the war. It was a state-of-the-art design for its day, but the project was cancelled after the fall of Peron in 1959. When President [[Juan Perón]] fell from power in 1955, the ex-Focke-Wulf team dispersed with many moving to [[India]]; Tank also moved there.<ref name="AirPages"/> First he worked as Director of the [[Madras Institute of Technology]], where one of his students was [[Abdul Kalam]] (later Kalam became [[President of India]] and designed indigenous [[Satellite Launch Vehicle]](SLV) and [[Integrated Guided Missile Development Programme]]). Kurt Tank later joined [[Hindustan Aeronautics]], where he designed the [[HAL HF-24 Marut|Hindustan Marut]] fighter-bomber, the first military aircraft constructed in India. The first prototype flew in 1961; the Marut was retired from active service in 1985. Tank left Hindustan Aeronautics in 1967 and by the 1970s had returned to live in Berlin, basing himself in Germany for the rest of his life.  He worked as a consultant for [[Messerschmitt-Bölkow-Blohm|MBB]].<ref>{{Citation | contribution = Kurt Tank | contribution-url = http://www.history.com/topics/kurt-tank | title = History}}.</ref> He died in [[Munich]] in 1983.

== References ==
{{reflist}}

==External links==
*{{DNB portal|11862069X|TYP=}}

{{Kurt Tank aircraft}}

{{Authority control}}

{{DEFAULTSORT:Tank, Kurt}}
[[Category:1898 births]]
[[Category:1983 deaths]]
[[Category:Aircraft designers]]
[[Category:People from Bydgoszcz]]
[[Category:German aerospace engineers]]
[[Category:German military personnel of World War I]]
[[Category:German scientists]]
[[Category:German people of World War II]]
[[Category:German expatriates in Argentina]]
[[Category:People from the Province of Posen]]
[[Category:Technical University of Berlin alumni]]
[[Category:Braunschweig University of Technology faculty]]