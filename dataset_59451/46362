{{Infobox artist
| bgcolour      = #6495ED
| name          = Morley Baer
| image         = Morley Baer by Unk1.jpg
| imagesize     =
| caption       = Morley Baer in the early 1980s
| birth_name    = Morley Baer
| birth_date    = {{birth date|1916|4|5}}
| birth_place   = [[Toledo, Ohio]]
| death_date    = {{death date|1995|11|9}}
| death_place   = [[Monterey, California]]
| education   = [[University of Toledo]], 1934; B.A, M.A [[University of Michigan]], 1938
| spouse        = Frances Baer (neé Manney), an accomplished visual artist
| field         =  Photography - Architectural and Landscape
| training      = Mentored by [[Edward Weston]], [[Ansel Adams]], and others from the [[Group f/64]] 
| accomplishments   = Architectural Photographer; Instructor [[San Francisco Art Institute]]; Instructor,  [[UC Santa Cruz]]
| awards        = [[American Institute of Architects]] Architectural Photography Award, 1966; [[Rome Prize]] from the [[American Academy in Rome]], 1980
}}

[[File:ROAD SIDE, LOOKING NORTH - Southern Pacific Railroad Station, Burlingame Avenue and California Drive, Burlingame, San Mateo County, CA HABS CAL,41-BURL,1-15.tif|thumb|350px|[[Burlingame (Caltrain station)|Burlingame Southern Pacific Railroad Station]] trackside facade, 1971 HABS image by Baer.]]
[[File:ROAD SIDE, TOWER - Southern Pacific Railroad Station, Burlingame Avenue and California Drive, Burlingame, San Mateo County, CA HABS CAL,41-BURL,1-16.tif|thumb|350px|Burlingame Station, tower on trackside, [[Burlingame, California]], 1971 HABS image by Baer.]]
[[File:ROAD SIDE, LOOKING EAST - Southern Pacific Railroad Station, Burlingame Avenue and California Drive, Burlingame, San Mateo County, CA HABS CAL,41-BURL,1-17.tif|thumb|350px|Burlingame Station, streetside [[facade]] with tower, 1971 HABS image by Baer.]]

'''Morley Baer''' (April 5, 1916 – November 9, 1995) was an American [[photographer]] and teacher, born in [[Toledo, Ohio|Toledo]], [[Ohio]]. The former head of the photography department at the [[San Francisco Art Institute]], Baer was well known for his photographs of San Francisco's "[[Painted Ladies]]" Victorians and for photos of California's  buildings, farmlands and coastline.<ref>[http://www.sfgate.com/news/article/OBITUARY-Morley-Baer-3018930.php Obituary] at SF Gate, November 11, 1995</ref>

Baer learned basic commercial photography in Chicago but subsequently honed his skills as a [[World War II]] [[United States Navy|Navy]] combat photographer.  Returning to civilian life an accomplished professional, over the next few years he developed into “one of the foremost [[architectural photographers]] in the world,"<ref name="Baer, 1988">[[#LY|Baer, 1988]]</ref> receiving important commissions from some of the premier architects in post-war Central California.  In the early 1970s, very much influenced by a strong friendship with [[Edward Weston]], Baer began to concentrate on his personal landscape art photography.  During the last decades of the 20th century, Baer also became a sought-after instructor in various colleges and workshops teaching the art of [[landscape photography]].

[[Image:MB Photo, C. Purcell, Cambria, '93.jpg|thumb|left|200 px|"Morley Baer at his photography workshop", Cambria, CA, '93, by Christopher Purcell]]

==Early life and education==
Morley Baer was born to Clarence Theodore Baer and Blanche Evelyn Schwetzer Baer,<ref>Application for Birth Certificate copy – UCSC Archives</ref> and led an active outdoor life growing up in Toledo.  He attended the [[University of Toledo]] in 1934 and later transferred to the [[University of Michigan]], from where he graduated in 1937 with a BA in English.  He continued there in graduate school and earned an MA in Theater Arts in 1938.<ref name = "Grant">NEA Grant Application, 1960 – UCSC Archives</ref> Baer soon found a well-paying but dull job in the advertising office of the Chicago department store [[Marshall Field's]], but soon apprenticed as a low-paid menial assistant, at a greatly reduced salary, to a [[Michigan Avenue (Chicago)|Michigan Avenue]] commercial photography company.<ref name = "Lian">Letter to Lian Hurst Mann, Editor, "Architecture California", 28 Feb 1992 – UCSC Archives</ref>  He shortly was photographing in the field, and developing and printing photographs.

Along with two associates, Baer was sent on assignment to [[Colorado]] in 1939. Baer had seen an exhibition of Edward Weston’s photographs at the Katherine Kuh Galleries in January of that year, and became enamored at the sparse elegance of Weston's black-and-white prints. He resolved to meet Weston and extended his trip west to [[California]] to meet him at his studio in [[Carmel-by-the-Sea, California|Carmel-by-the-Sea]]. The two did not meet, but Baer made the most of the trip by visiting [[San Francisco]], the [[Monterey Peninsula]], and the small village of [[Carmel, California|Carmel]].

==Military photographer==
Although he returned to Chicago, he already had applied for entrance into Art Center School while in San Francisco,<ref name="Jacobs">Lou Jacobs early 1950s article on Morley and Frances Baer - UCSC archives</ref> but his plans were derailed by the onset of World War II.  In 1941 he enlisted in the Navy almost immediately after [[Attack on Pearl Harbor|Pearl Harbor]]<ref name = "Grant"/> and went through the Navy photo school at [[Pensacola, Florida|Pensacola]], [[Florida]], where he hated the stereotyped approach to making a photograph. Baer graduated, commissioned as an ensign, and was transferred to [[Norfolk, Virginia|Norfolk]] to do a series of stories on the Atlantic Theater of operations.<ref name = "Jacobs"/>

The young photographer had varied duties including public relations, aircraft recon, editorial assignments, teaching, and combat photography from aircraft and carriers.<ref name = "Grant"/>  Accompanied by a writer, Baer covered military operations in [[North Africa]], southern [[France]], [[Brazil]], and the [[Caribbean Sea]].  In 1945 he was assigned to the operational [[Naval Aviation Photographic Unit]], commanded by the eminent photographer [[Edward Steichen]].  Working under a variety of terrain, sea, and weather conditions, varying light, and new environments in physically demanding activities, and making dozens of photographs a day, Baer quickly perfected his technical and compositional photographic skills.  When discharged from the Navy in 1946 he was a complete and confident professional photographer.<ref>[[#stones|Jeffers, 2002]]</ref>

==Post-war years==
In 1945 Baer was discharged from the Navy in San Francisco and met Frances Manney, a young woman awaiting admission to [[Stanford University]] who had hired Baer as her photography tutor during his brief time in Norfolk.<ref name="Baer, 1988"/>  The two married, splurged their remaining funds and set up a commercial photography business in a small store-front studio in Carmel in 1946.<ref name="Jacobs"/>

Baer had no difficulty discovering opportunities aplenty in the booming post-war building trades.  In dire need of competent photographers to illustrate their projects, builders and architects vied to hire the Baer team. As his reputation grew he had as much work as he could handle.  His published architectural photographs from that time testify to his active professional career along with the quality of his clients among the more noted Bay Area architectural firms.<ref>[[#here|Olmstead, 1968]]</ref><ref name="Andersen, 1980">[[#CD1910|Andersen, 1980]]</ref>

Although Baer had briefly fulfilled his long-held dream of meeting Edward Weston, they had met only briefly.  But through a Weston friend sometime in 1947, Baer learned of an Ansco view camera for sale.  He had previously used an Ansco in Chicago and was very familiar with its capabilities.  Able to buy it for the then princely sum of $90,<ref name="Baer, 1988"/> it became the camera he most used for the rest of his life.  Although he had others for some assignments, the Ansco was the instrument with which he made his most memorable photographs.   It became almost an extension of his photographic seeing and visualization in his later fine art landscape work.<ref name="Baer, 2002">[[#barns|Baer, 2002]]</ref>

While Baer held Weston in considerable awe he and Frances became frequent visitors to the Weston home/studio in Carmel’s Wildcat Hill and had a close friendship until Weston’s death in 1958. Both Baers were very helpful to Weston in producing his monumental photographic Portfolios I and II. Morley worked closely with Edward’s son [[Brett Weston]] in making the prints, while Frances, as the person who did print [[spotting (photography)|spotting]], finished Weston’s prints.   Besides being helpful to Weston this association greatly benefited Baer’s career in the world of [[fine-art photography]].  Through Weston, a long-standing resident of California and the Bay Area, he met most of the prominent West Coast photographers.  The more noted among them earlier had formed [[Group f/64]] in San Francisco; its members included Weston, [[Ansel Adams]], [[Imogen Cunningham]], [[Willard Van Dyke]], and [[Henry Swift (photographer)|Henry Swift]], among others.

Baer again met up with Steichen in 1950 when he and Frances made a trip to New York.  Although Steichen carefully examined the photographic portfolio Baer showed him he was less than enthralled by its subject matter – their photographic sensitivities were vastly different.<ref name="Jeffers, 2002">[[stones|Jeffers, 2002]]</ref><ref name="Adams, 1985">[[#AA85|Adams, 1985]]</ref>

===Bay Area residency===
Finding more work opportunities in the San Francisco area in the early fifties, the Baers sold their house on Carmelo Avenue in Carmel and re-located their photographic work to [[Berkeley, California|Berkeley]].   Shortly Baer began to make a name for himself as a leading architectural photographer with portfolio work for architects and interior designers while freelancing for housing design magazines.<ref name="Jeffers, 2002"/> Ansel Adams recruited him as an instructor at the [[San Francisco Art Institute]], then under the direction of [[Minor White]].  When White left for the East Coast in 1953, Baer became Head of the Institute's Photography Department.

In 1953 the Baers moved into a 1920s era house in Greenwood Common that previously had been renovated by architect [[Rudolph Schindler (architect)|Rudolph Schindler]] for its owner, who sold it in 1951 to [[William Wurster]], who, in turn, sold it in 1953 to Baer.<ref>[[#WL|Lowell, 2009]]</ref>  Baer became very active in neighborhood affairs  Showing their lifetime appreciation for landscaping, the Baers hired [[Lawrence Halprin]]  to design their outdoor areas.

Baer rapidly became a sought-after architectural photographer for noted architects, including Craig Edwards,  the firm of [[Skidmore, Owings and Merrill]] (SOM), [[Charles Willard Moore]], and [[William Turnbull, Jr.]].<ref name="Fink, 1980">[[#AF80|Fink, 1980]]</ref>  Baer’s photographs of buildings by [[Bernard Maybeck]], [[Greene and Greene]], [[Frank Lloyd Wright]], and [[Julia Morgan]]  are considered important today in understanding American architecture and design in the first half of the 20th century.

[[Image:MB by Getta,'95.jpg|thumb|right|200 px|"Portrait of Morley Baer" '95, by Brigitte Carnochan]]

==1960s onwards==
Through the influence of [[Nathaniel A. Owings]], SOM hired Baer to photograph US consulate buildings being constructed throughout Western Europe,<ref name = "Grant"/> so the Baers and their young son moved to [[Spain]] for two years.<ref name = "Gugg">Guggenheim Grant Application, 1960 – UCSC Archives</ref> Baer managed to produce personal work by photographing out-of-the-way locations in [[Andalucia]].  The Baer family traveled the country in a VW bus in which they camped as necessary, freeing them to go where they pleased. These photographs led to Baer’s first one-man exhibition at San Francisco’s [[M. H. de Young Memorial Museum]] in 1959 and his first published portfolio.

After he returned to California, SOM hired Baer for a large photographic survey that lasted through the mid-1960s. During this time Baer also was the architectural photographer for the pioneering [[Sea Ranch, California]] in Gualala.

The earlier sojourn in Spain had whetted Baer's appetite to return.  In 1960, still at Greenwood Common, he unsuccessfully applied for a [[Guggenheim Fellowship]] to photograph there again. The Baers bought a house on Carmelo Avenue in Carmel from architect Olaf Dahlstrand that they used as a vacation home while living in Greenwood Common; Baer later photographed Dahlstrand's work.<ref>[[#KS|Seavey, 2007]]</ref>  Although he continued his successful architectural photography career in the Bay Area, operating out of Greenwood Common,  Baer continued to develop a personal interest in fine art landscape photography and began to scour the more remote regions of central California in search of landscape subjects; most photographs in his signature book, "Light Years", date from the early 1960s.

He contributed work for the 1965 [[Sierra Club]] publication "Not Man Apart", which also included entries from artists such as [[Robinson Jeffers]], [[Dorothea Lange]], and [[Beaumont Newhall]].<ref>[[#RJ02|Jeffers, 1965]]</ref> Jeffers exerted a strong influence on Baer’s thinking and artistic sensitivities.<ref name="Olmstead, 1968">[[#WS|Olmstead, 1968]]</ref>

In 1966 the [[American Institute of Architects]] gave Baer their Architectural Photography Award.

The success of ‘'Here Today'’ led to other assignments culminating in Baer's selection as the only photographer for the 1978 book ‘'Painted Ladies'’, a collection of color photographs of the more stately San Francisco [[Victorian architecture#North America|Victorian houses]], which was Baer's first major color photography project.<ref name="Pomada, 1978">[[ladies|Pomada, 1978]]</ref>

===Garrapata===
In 1965 the Baers built a new second home and studio, designed by Bay Area Modernist architect [[William Wurster]], south of Carmel near the [[Big Sur]] coast. It was located on the cliffs above [[Garrapata State Park|Garrapata Beach]], between the [[Big Sur Coast Highway]] and the ocean.  Wurster designed the two-story house with river-stone [[facade]]s, creating an organic building blending with the rocks and cliffs of Garrapata, sometimes referred to as 'The Stone House.' <ref name="Baer, 1988"/>   It commanded a striking view of Garrapata Beach and Soberanes Point with the long stretch of beach in between.  Frances never felt comfortable in Garrapata, feeling it cold, damp, and isolated and continued to live in Berkeley while Morley used the Garrapata residence as his combination home and studio. The remote coastal location brought Baer into intimate contact with the primal natural elements of wind, water, light, and rocks at the cliffs and beach.

With Garrapata as his base, Baer photographed throughout the West but most notably in Central California. He exhibited his landscape portfolios of classical black-and-white photographs, wrote or contributed to several books of photography, and began to instruct in photography workshops.   In the early 1970s, Baer joined Adams and other prominent central California fine art photographers to found the Friends of Photography in Carmel,<ref name="Adams, 1985"/> which organized yearly photographic student workshops at Carmel’s Sunset Center and at the Julia Morgan-designed [[Asilomar Conference Grounds]]. Those workshops inspired development of what became known as the West Coast style of landscape photography through the last decades of the 20th Century.<ref>Amy Conger, ''The Monterey Photographic Tradition: The Weston Years'', Monterey Peninsula Museum of Art, 1981</ref> He published two photography collections in 1973, 'Andalucia' and 'Garrapata Rock,'

Along with his increasing success as a fine art landscape photographer, Baer continued to take assignments as a commercial architectural photographer, primarily throughout the Monterey Peninsula.  Among his many architect clients in this period were: Burde & Shaw, Hugh Comstock, Gordon Drake, and Tom Elston. He was the photographer for the brochure of the exhibition, ‘'California Design 1910'’ <ref name="Andersen, 1980"/> in late 1974 at the [[Pasadena Conference Center]]; one of his landscape photographs graces the cover.

===Carmel===
In 1972, after the couple became temporarily estranged, the Baers sold the Greenwood Common house. Frances remained as an art teacher in the Bay Area. In 1979 the Garrapata house was also sold, and Baer briefly moved to a smaller home in Carmel. In 1980, Baer was awarded the [[Rome Prize]] in Design and a fellowship at the [[American Academy in Rome]],  where he mainly photographed the fountains of Rome. Baer had an exhibition of them, titled “The Fountains of Rome”, during May 1981 at the Bonnafont Gallery in San Francisco.<ref name="Baer">Baer Archives, University of California, Santa Cruz, California</ref>  He returned to his Carmel home/studio after the year in Rome, and reunited with Frances. The couple purchased a house/studio on Carmel Valley Road in 1985, where they lived until their respective deaths.

[[Image:MB, Tarpy's, C. Purcell, '94.jpg|thumb|right|200 px|"Morley Baer", Monterey, CA, '94, by Christopher Purcell]]

==Photographic philosophies and techniques==
From the beginning of their relationship, both as husband and wife and as business partners, Morley and Frances Baer worked as a team. Their separate but personal photographic visions and techniques centered around careful composition of the subject matter, complete familiarity with their equipment and materials, and dedication to the art and profession of photography. Both believed that only the view camera  – Morley with an 8x10, Frances with a 5x7 - allowed them to express their feelings about a photographic subject.<ref>[[#AA80|Adams, 1980]]</ref> Although devoted partners, the Baers were intense mutual competitors and had an arrangement for ‘artistic rights’ to a potential photograph discovered while driving the countryside — it belonged to the person on whose side of the car the subject lay.<ref name="Baer, 2002"/>

Baer's overall approach to making a photograph was centered on uniting technical capabilities of his camera and lens with emotional feelings at the moment of making the photograph.  He always would apply his 'strongest seeing,' to borrow a phrase from Edward Weston,<ref>[[#EW91|Weston, 1991]]</ref> to bring out what he saw as the dominant element in a scene.  His photographs were always finely set up, some would say “tightly”.  Although he eschewed use of the term ‘composition’,  he applied an admonition from Weston that “composition is just the strongest way of seeing”.  Indeed, along with that uncompromising admonishment, Morley held to an amplifying Weston dictum  that "Photography as a creative expression … must be seeing plus.".<ref name="Baer"/>

Morley Baer's philosophy of art photography, and indeed of life itself, is exemplified in the work of several of his photographic assistants.  Among them, linked below, are Marco Zecchin, Patrick Jablonski, Frank Long, and the late Erik Lauritzen.  Lauritzen's own work is archived at UC Santa Cruz.

===Equipment===
Motivated by a strong sense of simplicity in equipment and technique learned from Edward Weston, and reinforced by his readings of Robinson Jeffers' sparse poetry for which he had sincere admiration,<ref name="Jeffers, 2002"/> Baer reduced his photographic equipment to a compact minimum.<ref name="Olmstead, 1968"/> With time and experience, he acquired the equipment and techniques that completely suited his photographic style.  Once he had settled on a particular technique he rarely changed it.  Baer used the same Ansco 8x10S view camera on an apparently flimsy but really very sturdy wood tripod for virtually all his serious photography.  After fifty years of usage the Ansco had become almost an extension of his mind and eye; he could adjust its settings by feel alone while under his darkcloth and concentrating on his subject in the ground glass viewer.  Baer designed a special metal carrying case, with a sturdy leather-strap handle, constructed for him by a Monterey metal worker.  He replaced it only once during his career.  The case held his camera, several lenses, film holders and other paraphernalia he needed in the field.<ref name="Pomada, 1978"/> With his camera on one shoulder, and the carrying case in the opposite hand, he was perfectly laterally balanced as he strode towards the subject of his photographic interest.

[[Image:MB by DF, Early 90s.jpg|thumb|left|200 px|"Morley Baer & his Ansco Camera, early 90s, by David Fullagar]]

Precise in his lens selection and use, Baer used a wide range of lens focal lengths from a 120&nbsp;mm wide angle to a 19” (480&nbsp;mm), what he called his ‘long lens’.<ref name="Fink, 1980"/> As his favorite lens he claimed that  “It sees how I see the material.” <ref name="Baer, 2002"/> Having a lens for almost every occasion was important since Baer did mostly contact prints of his 8x10 negatives where, besides being esthetically displeasing to him, cropping was not an option.  Although owning a Navy surplus Saltzman 8x10 enlarger,<ref>John Sexton Tribute; Apogee Photo Magazine, late 1995</ref>  he rarely used it since contact prints were his preferred medium of expression – it’s now a prized possession of one of his last assistants.  He standardized his procedures to the extent possible, based on thorough testing for film exposure and development characteristics, changing  them only when necessary.

===Darkroom techniques===
In developing his 8x10 negatives, he did so by inspection during the development process. While the development was well underway, Baer briefly checked his highlight densities with a dim green safelight  and continued development until he obtained his desired densities.<ref name="Baer, 2002"/> He developed  early Isopan and later Super XX Black & White film in his variation of ABC Pyro.<ref>[[#AA95|Adams, 1995]]</ref>   For some color work, Baer used Ektachrome film developed in a commercial developer but exposed at values he worked out in extensive testing.  Although reluctant to use filters, Baer did so when necessary to make his photograph most effectively express the subject—as he described in technical entries in his several books.  He seldom changed from his favorite print developer Amidol<ref>[[#AAP|Adams, 1995]]</ref> modifying it as necessary as printing papers evolved.<ref>[[#MA79|Baer, 1979]]</ref>

With his varied Navy photography experience, his many years of study, and intimate knowledge of his equipment, materials and darkroom techniques, Baer was well-qualified to take on any photographic assignment that came along. That confidence also allowed him to concentrate on the photographic task at hand, “… to interpret and thus to fully realize the potential for maximum expression… “ in his photographs.<ref name="Baer, 1988"/>  In evaluating a potential photographic subject, Baer thought both in esthetic and organizational terms but also in the technical challenges he would face in actually making the photograph. His assistant watched Baer as he stared at a subject tree muttering, 'I am looking at the Pyro in the tree trunks [''for the negative''] and the Amidol [''for the print''] in the leaves'.<ref name="Baer, 2002"/>  He already was thinking of the technical problems he would have to solve in exposing the negative to get the tones he wanted in the final print.

==Legacy==
Morley Baer died in 1995, in [[Monterey, California]]. His photographic archive was divided between personal work and architectural work.  
Film negatives that Baer felt represented his most significant work were given to the Special Collections of the University of California, Santa Cruz . His architectural archive was given to the Architecture School at [[Stanford University]].

==References==
{{refbegin|2}}	 
* {{cite book | last = Adams | first = Ansel | authorlink= Ansel Adams| title = Ansel Adams, an Autobiography | publisher = Little, Brown | location = Boston | year = 1985 | isbn = 0-8212-1596-5 |ref =AA85}}
* {{cite book | last = Adams | first = Ansel | authorlink= Ansel Adams| title = The Camera| publisher = New York Graphic Sockety | location = Boston | year = 1980 | isbn = 0-8212-1092-0 |ref =AA80}}
* {{cite book | last = Adams | first = Ansel | authorlink= Ansel Adams| title = The Negative| publisher = Little, Brown | location = Boston | date = June 1, 1995 | isbn = 978-0-8212-2186-0 |ref =AA95}}
* {{cite book | last = Adams | first = Ansel | authorlink= Ansel Adams| title = The Print| publisher = Little, Brown | location = Boston | year = 1985 | isbn =978-0-8212-2187-7 |ref =AAP}}
* {{cite book| last1 =Andersen| first1 = Timothy |last2 = Moore | first2= Eudorah M. |last3 = Winder | first3 = Robert W. | others = Baer, Morley (photographer)| title =California Design 1910| publisher =Peregrine Smith| year= 1980| location = Santa Barbara, CA |isbn = 0-87905-055-1 |ref =CD1910 }}
* {{cite book| last =Baer| first =Morley| title =California Plain: Remembering Barns| publisher =Stanford University Press| date =June 12, 2002 | location =Palo Alto, CA | isbn=0-8047-4270-7 |ref =barns }} <ref>[http://www.sup.org/books/title/?id=1822 "California Plain: Remembering Barns" — Stanford University Press webpage]; reviews and orders.</ref>
* {{cite book  | last =Baer  | first =Morley | title =Light Years: The Photographs of Morley Baer | publisher =Photography West Graphics| year =1988  | location =Carmel, CA | isbn= 0-9616515-2-0 | ref=LY}}
* {{cite book| last = Baer| first = Morley| title = Room and Time Enough: The Land of Mary Austin| publisher = Northland Press| year = 1979| location = Flagstaff, AZ| ref =MA79| isbn =0-87358-205-5}}
* {{cite book| last1 =Baer| first1 =Morley| last2 =Wallace |first2 = David Rains  | title = The Wilder Shore| publisher = Sierra Club Books| year = 1984| location = San Francisco | isbn =0-87156-328-2 |ref =WS }}
* {{cite book| last1 = Fink| first1 = August| last2 = Baer| first2 = Morley| title = Adobes in the Sun: Portraits of a tranquil era| publisher = Chronicle Books| year = 1980| location = San Francisco| ref = AF80| isbn = 978-0-87701-193-4  }}
* {{cite book| last =Jeffers| first =Robinson| authorlink =Robinson Jeffers| editor =Brower, David Ross| title =Not Man Apart: Photographs of the Big Sur| publisher =Sierra Club Books| year = 1969| location =San Francisco| ref =RJ02| isbn =978-0-88486-005-1  }}
* {{cite book| last =Jeffers| first =Robinson| authorlink =Robinson Jeffers| title = Stones of the Sur: Poetry by Robinson Jeffers, Photographs by Morley Baer| publisher = Stanford University Press | date = June 1, 2002| location =  | isbn = 0-8047-3942-0 |ref =stones}}
* {{cite book| last =Lowell| first =Waverly B.| title = Living Modern: A Biography of Greenwood Common| publisher = William Stout Publishing| date = January 1, 2009| location = San Francisco| ref = WL| isbn=978-0-9795508-6-7  }}
* {{cite book| last1 = Olmstead| first1 =Roger| last2 = Watkins| first2 = T. W. | last3 = Baer| first3 = Morley| editor = The Junior League of San Francisco| title =Here Today: San Francisco's Architectural Heritage| publisher = Chronicle Books| year = 1968| location = San Francisco | isbn = 0-87701-125-7 | ref = here }}
* {{cite book| last1=Pomada| first1 =Elizabeth| last2 = Larsen| first2 =Michael| last3=Baer| first3 = Morley| title =Painted Ladies: San Francisco's Resplendent Victorians| publisher = E. P. Dutton| date = October 27, 1978| location = New York| isbn = 0-525-47523-0 |ref =ladies  }}
* {{cite book| last =Seavey| first =Kent| title = Carmel: A History in Architecture| publisher = Arcadia Publishing |location = Charleston, SC| date = October 17, 2007 | ref = KS| isbn =978-0-7385-4705-3  }}
* {{cite book| last =Weston| first =Edward |authorlink = Edward Weston| editor = Newhall, Nancy| title = The Daybooks of Edward Weston| publisher = Aperture| date = March 1, 1991| location = New York | ref = EW91| isbn= 978-0-89381-450-2 }}
{{refend}}

===Notes===
{{reflist|27em}}

==External links==
{{Commons category}}
* [http://www.oac.cdlib.org/findaid/ark:/13030/c8tt4pnj/ Morley Baer Photographs (Stanford University Archives)]
* [http://guides.library.ucsc.edu/speccoll/collections-overview UC Santa  Cruz photography archives]
* [http://www.photographywest.com/pages/baer_bio.html Photography West Gallery: Morley Baer] — ''Baer images by topic''.
* [http://articles.sfgate.com/1995-11-11/news/17819223_1_mr-baer-fine-art-photography-young-memorial-museum SFGate.com: Obituary]
*{{commonscat-inline|Historic American Buildings Survey of California}}
* [http://marcozecchin.com Marco Zecchin Photography]
* [http://www.oac.cdlib.org/findaid/ark:/13030/kt796nc97p/ Erik Lauritzen Photographs (Stanford University Archives)]
* [http://www.patrickjablonski.com Patrick Jablonski Photography]

{{DEFAULTSORT:Baer, Morley}}
[[Category:Architectural photographers]]
[[Category:Landscape photographers]]
[[Category:Nature photographers]]
[[Category:Photographers from California]]
[[Category:Rome Prize winners]]
[[Category:1916 births]]
[[Category:1995 deaths]]
[[Category:Artists from Toledo, Ohio]]
[[Category:People from Carmel-by-the-Sea, California]]
[[Category:University of Toledo alumni]]
[[Category:University of Michigan alumni]]
[[Category:American military personnel of World War II]]
[[Category:United States Navy personnel]]
[[Category:Heritage Documentation Programs|•]]
[[Category:20th-century American photographers]]