{{redirect|MLQ|other uses|Major League Quidditch}}
{{italic title}}
{{Infobox journal
| title         = 
| cover         = [[File:MLQ cover.gif]]
| editor        =Marshall Brown  
| discipline    =Literary History 
| peer-reviewed = 
| language      =English 
| formernames   = 
| abbreviation  =Mod. lang. q. (Seattle) 
| publisher     =Duke University Press and University of Washington
| country       =USA 
| frequency     =Quarterly  
| history       =1940-present 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       =http://mlq.dukejournals.org/ 
| link1         =http://depts.washington.edu/mlq/publications/pub_quarterlyjournal.html
| link1-name    =Online archive access  
| link2         =http://depts.washington.edu/mlq/ 
| link2-name    =U of W journal homepage 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          =1758463 
| LCCN          =43005690  
| CODEN         = 
| ISSN          =0026-7929 
| eISSN         =1527-1943 
| boxwidth      = 
}}

'''Modern Language Quarterly''' ('''MLQ'''), established in 1940, is a quarterly, [[literary history]] journal, produced (housed) at the [[University of Washington]] and published by [[Duke University Press]]. The current editor, since 1993, is Marshall Brown (University of Washington).<ref name=details>{{Cite web
  | title =MLQ details 
  | work =Details, Back issues, about, online access 
  | publisher =Duke University Press 
  | date =September 2010
  | url =http://www.dukeupress.edu/Catalog/ViewProduct.php?viewby=journal&productid=45619 
  | accessdate =2010-09-26}}</ref><ref name=homepg>{{Cite web
  | title =Homepage U of W 
  | work =brief description of journal
  | publisher =University of Washington
  | date =September 2010
  | url =http://depts.washington.edu/mlq/
  | accessdate =2010-09-26}}</ref><ref name=Editor>{{Cite web
  | title =About the editor 
  | work =Editor biography, Editorial board, Advisory board, past editors 
  | publisher =MLQ, Univ of Washington 
  | year =2005 
  | url =http://depts.washington.edu/mlq/aboutmlq/about_edinfo.html 
  | accessdate =2010-09-06}}</ref>

==Scope==
The focus of MLQ is all topics in literary history, which includes all [[genre]]s, and all [[history|time periods]]. Theory and argument are presented with a chronological [[organizational structure]].  Literary works are considered in the context of their time. The focus encompasses papers on literary change in literary practice and the profession of literature. Topical coverage includes how literary change, or literary history, relates to  [[feminism]], [[ethnic studies]], [[cultural materialism (cultural studies)|cultural materialism]], discourse analysis,  and cultural critiques. [[Literature]] as it occurs in history is seen as the demonstration and agent of change. Understanding how literature has an impact is emphasized. Publishing formats are scholarly [[essays]] and book reviews.<ref name=details/><ref name=homepg/><ref name=muse>{{Cite web
  | title =About The Journal
  | work =brief description of journal
  | publisher =Project Muse
  | date =September 2010
  | url =http://muse.jhu.edu/journals/mlq/
  | accessdate =2010-09-26}}</ref><ref name=fstpg>{{Cite web
  | title =MLQ history 
  | work =
  | publisher =University of Washington
  | date =September 2010
  | url =http://depts.washington.edu/mlq/aboutmlq/about_main.html
  | accessdate =2010-09-26}}</ref>

==Abstracting and indexing==
This journal is indexed in the following databases:<ref name=details/><ref name=mstrjnl>[http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0026-7929 Master Journal List]. Thosmson Reuters. September 2010.</ref>

:[[Thomson Reuters]]:
::[[Arts & Humanities Citation Index]]
::[[Current Contents]] / Arts & Humanities
:Academic Abstracts Fulltext Elite & Ultra
:Academic Research Library,
:[[Academic Search]] Elite & Premier
:[[Expanded Academic ASAP]],
:[[General Reference Center Gold]] & International
:Humanities and Social Sciences Index Retrospective, 1907–1984,
::Humanities Abstracts,
::Humanities Full Text,
::Humanities Index,
::Humanities Index Retrospective, 1907–1984,
::Humanities International Complete,
::Humanities International Index,
::Social Sciences Index Retrospective, 1907–1984
:[[International Bibliography of Periodical Literature]] (IBZ),
:Literary Reference Center,
:Literature Resource Center,
:Magazines for Libraries,
:[[MLA Bibliography]],
:OmniFile - Mega Edition
:Professional Development Collection (EBSCO)
:Research Library

==Staff==
The managing editor from 1943 to 1963 was [[Edward G. Cox]].

==References==
{{Portal|Literature}}
{{Reflist}}

==External links==
*[http://mlq.dukejournals.org/rss/mfc.xml Most cited full text articles]

[[Category:Publications established in 1940]]
[[Category:American literary magazines]]
[[Category:Duke University]]
[[Category:University of Washington]]
[[Category:Quarterly journals]]