[[File:SSEM Manchester museum.jpg|thumb|300px|alt=A series of seven tall metal racks filled with electronic equipment standing in front of a brick wall. Signs above each rack describe the functions carried out by the electronics they contain. Three visitors read from information stands to the left of the image.|Replica of the Small-Scale Experimental Machine (SSEM) at the [[Museum of Science and Industry (Manchester)|Museum of Science and Industry]] in [[Castlefield]], [[Manchester]]]]
The '''Manchester Small-Scale Experimental Machine''' ('''SSEM'''), nicknamed '''Baby''', was the world's first [[stored-program computer]]. It was built at the [[Victoria University of Manchester]], England, by [[Frederic Calland Williams|Frederic C. Williams]], [[Tom Kilburn]] and [[Geoff Tootill]], and ran its first program on 21 June 1948.<ref>{{citation |last=Enticknap |first=Nicholas |title=Computing's Golden Jubilee |journal=Resurrection |issue=20 |publisher=The Computer Conservation Society |date=Summer 1998 |url=http://www.cs.man.ac.uk/CCS/res/res20.htm#d |issn=0958-7403 |accessdate=19 April 2008}}</ref>

The machine was not intended to be a practical computer but was instead designed as a [[testbed]] for the [[Williams tube]], an early form of computer memory. Although considered "small and primitive" by the standards of its time, it was the first working machine to contain all the elements essential to a modern electronic computer.<ref name=EarlyComputers /> As soon as the SSEM had demonstrated the feasibility of its design, a project was initiated at the university to develop it into a more usable computer, the {{nowrap|[[Manchester Mark 1]]}}. The Mark 1 in turn quickly became the prototype for the [[Ferranti Mark 1]], the world's first commercially available general-purpose computer.<ref name=NapperMK1>{{citation |last=Napper |first=R. B. E. |title=Introduction to the Mark 1 |url=http://www.computer50.org/mark1/mark1intro.html |publisher=The University of Manchester |accessdate=4 November 2008}}</ref>

The SSEM had a 32-[[bit]] [[word (data type)|word]] length and a [[computer memory|memory]] of 32&nbsp;words. As it was designed to be the simplest possible stored-program computer, the only arithmetic operations implemented in hardware were [[subtraction]] and [[additive inverse|negation]]; other arithmetic operations were implemented in software. The first of three programs written for the machine found the highest [[Divisor#Further notions and facts|proper divisor]] of 2<sup>18</sup> (262,144), a calculation that was known would take a long time to run—and so prove the computer's reliability—by testing every integer from 2<sup>18</sup>&nbsp;−&nbsp;1 downwards, as division was implemented by repeated subtraction of the divisor. The program consisted of 17&nbsp;instructions and ran for 52&nbsp;minutes before reaching the correct answer of 131,072, after the SSEM had performed 3.5&nbsp;million operations (for an effective CPU speed of 1.1 [[instructions per second|kIPS]]).

== Background ==
{{main article|History of computing hardware}}
[[File:Maquina.png|thumb|Artistic representation of a [[Turing machine]]]]
The first design for a program-controlled computer was [[Charles Babbage]]'s [[Analytical Engine]] in the 1830s. A century later, in 1936, mathematician [[Alan Turing]] published his description of what became known as a [[Turing machine]], a theoretical concept intended to explore the limits of mechanical computation. Turing was not imagining a physical machine, but a person he called a "computer", who acted according to the instructions provided by a tape on which symbols could be read and written sequentially as the tape moved under a tape head. Turing proved that if an algorithm can be written to solve a mathematical problem, then a Turing machine can execute that algorithm.<ref>{{citation |last=Turing |first=A. M. |publication-date=1936–37 |year=1936 |title=On Computable Numbers, with an Application to the Entscheidungsproblem | periodical=Proceedings of the London Mathematical Society |series=2 |volume=42 |pages=230–265 |url=http://www.comlab.ox.ac.uk/activities/ieg/e-library/sources/tp2-ie.pdf |doi=10.1112/plms/s2-42.1.230 |accessdate=18 September 2010}}</ref>

[[Konrad Zuse]]'s [[Z3 (computer)|Z3]] was the world's first working [[computer programming|programmable]], fully automatic computer, with binary digital arithmetic logic, but it lacked the conditional branching of a Turing machine. On 12 May 1941, it was successfully presented to an audience of scientists of the ''Deutsche Versuchsanstalt für Luftfahrt'' ("German Laboratory for Aviation") in [[Berlin]].<ref>{{cite web |url=http://www2.tu-berlin.de/alumni/parTU/00dez/zuse.htm |title=Rechenhilfe für Ingenieure Konrad Zuses Idee vom ersten Computer der Welt wurde an der Technischen Hochschule geboren |archiveurl=https://web.archive.org/web/20090213222711/http://www2.tu-berlin.de/alumni/parTU/00dez/zuse.htm |archivedate=13 February 2009 |deadurl=yes |language=German |publisher=[[Technical University of Berlin]] |mode=cs2}}</ref> The Z3 stored its program on an external tape, but it was electromechanical rather than electronic. The [[Colossus computer|Colossus]] of 1943 was the first electronic computing device, but it was not a general-purpose machine.{{sfnp|Copeland|2010|pp=91–100|ps=}}

The [[ENIAC]] (1946) was the first machine that was both electronic and general purpose. It was [[Turing completeness|Turing complete]], with conditional branching, and programmable to solve a wide range of problems, but its program was held in the state of switches in patchcords, not in memory, and it could take several days to reprogram.<ref name=EarlyComputers>{{cite web |url=http://www.computer50.org/mark1/contemporary.html |title=Early Electronic Computers (1946–51) |publisher=University of Manchester |accessdate=16 November 2008 |mode=cs2}}</ref> Researchers such as Turing and Zuse investigated the idea of using the computer's memory to hold the program as well as the data it was working on,<ref>{{cite web |last=Zuse |first=Horst |url=http://www.epemag.com/zuse/default.htm |publisher=Wimborne Publishing |periodical=EPE Online |title=Konrad Zuse and the Stored Program Computer |accessdate=16 November 2008 |archiveurl=https://web.archive.org/web/20071210232351/http://www.epemag.com/zuse/default.htm |archivedate=10 December 2007 |mode=cs2}}</ref> and it was mathematician [[John von Neumann]] who wrote a widely distributed paper describing that computer architecture, still used in almost all computers.{{sfnp|Lavington|1998|p=7|ps=}}

[[File:von Neumann architecture.svg|left|thumb|Design of the [[von Neumann architecture]] (1947)]]
The construction of a von Neumann computer depended on the availability of a suitable memory device on which to store the program. During the Second World War researchers working on the problem of removing the clutter from [[radar]] signals had developed a form of [[delay line memory]], the first practical application of which was the mercury delay line,{{sfnp|Lavington|1998|p=1|ps=}} developed by [[J. Presper Eckert]]. Radar transmitters send out regular brief pulses of radio energy, the reflections from which are displayed on a CRT screen. As operators are usually interested only in moving targets, it was desirable to filter out any distracting reflections from stationary objects. The filtering was achieved by comparing each received pulse with the previous pulse, and rejecting both if they were identical, leaving a signal containing only the images of any moving objects. To store each received pulse for later comparison it was passed through a transmission line, delaying it by exactly the time between transmitted pulses.{{sfnp|Brown|1999|p=429|ps=}}

Turing joined the [[National Physical Laboratory, UK|National Physical Laboratory]] (NPL) in October 1945,{{sfnp|Lavington|1998|p=9|ps=}} by which time scientists within the [[Ministry of Supply]] had concluded that Britain needed a National Mathematical Laboratory to coordinate machine-aided computation.{{sfnp|Lavington|1980|loc=chapter 5|ps=}} A Mathematics Division was set up at the NPL, and on 19&nbsp;February 1946 Alan Turing presented a paper outlining his design for an electronic stored-program computer to be known as the [[Automatic Computing Engine]] (ACE).{{sfnp|Lavington|1980|loc=chapter 5|ps=}} This was one of several projects set up in the years following the Second World War with the aim of constructing a stored-program computer. At about the same time, [[EDVAC]] was under development at the [[University of Pennsylvania]]'s [[Moore School of Electrical Engineering]], and the [[Computer Laboratory, University of Cambridge|University of Cambridge Mathematical Laboratory]] was working on [[EDSAC]].{{sfnp|Lavington|1998|pp=8–9|ps=}}

The NPL did not have the expertise to build a machine like ACE, so they contacted [[Tommy Flowers]] at the [[General Post Office]]'s (GPO) [[Post Office Research Station|Dollis Hill Research Laboratory]]. Flowers, the designer of Colossus, the world's first programmable electronic computer, was committed elsewhere and was unable to take part in the project, although his team did build some mercury delay lines for ACE.{{sfnp|Lavington|1980|loc=chapter 5|ps=}} The [[Telecommunications Research Establishment]] (TRE) was also approached for assistance, as was [[Maurice Wilkes]] at the University of Cambridge Mathematical Laboratory.{{sfnp|Lavington|1980|loc=chapter 5|ps=}}

The government department responsible for the NPL decided that, of all the work being carried out by the TRE on its behalf, ACE was to be given the top priority.{{sfnp|Lavington|1980|loc=chapter 5|ps=}} NPL's decision led to a visit by the superintendent of the TRE's Physics Division on 22&nbsp;November 1946, accompanied by [[Frederic Calland Williams|Frederic C. Williams]] and A. M. Uttley, also from the TRE.{{sfnp|Lavington|1980|loc=chapter 5|ps=}} Williams led a TRE development group working on CRT stores for radar applications, as an alternative to delay lines.{{sfnp|Lavington|1998|p=5|ps=}} He had already accepted a professorship at the [[Victoria University of Manchester|University of Manchester]], and most of his circuit technicians were in the process of being transferred to the Department of Atomic Energy.{{sfnp|Lavington|1980|loc=chapter 5|ps=}} The TRE agreed to second a small number of technicians to work under Williams' direction at the university, and to support another small group working with Uttley at the TRE.{{sfnp|Lavington|1980|loc=chapter 5|ps=}}

== Williams–Kilburn tube ==
{{main article|Williams tube}}

Although early computers such as [[EDSAC]] made successful use of mercury [[delay line memory#Mercury delay lines|delay line memory]],<ref>{{citation |last1=Wilkes |first1=W. V. |authorlink=Maurice Wilkes |last2=Renwick |first2=W. |title=The EDSAC (Electronic delay storage automatic calculator) |journal=Mathematics of Computation |year=1950 |volume=4 |issue=30 |pages=61–65 |url=http://www.ams.org/journals/mcom/1950-04-030/S0025-5718-1950-0037589-7/ |doi=10.1090/s0025-5718-1950-0037589-7 |accessdate=21 June 2015}}</ref> the technology had several drawbacks; it was heavy, it was expensive, and it did not allow data to be accessed randomly. In addition, because data was stored as a sequence of acoustic waves propagated through a [[mercury (element)|mercury]] column, the device's temperature had to be very carefully controlled, as the velocity of sound through a medium varies with its temperature. Williams had seen an experiment at [[Bell Labs]] demonstrating the effectiveness of [[cathode ray tube]]s (CRT) as an alternative to the delay line for removing ground echoes from radar signals. While working at the TRE, shortly before he joined the University of Manchester in December 1946, he and Tom Kilburn had developed a form of electronic memory known as the [[Williams tube|Williams or Williams–Kilburn tube]] based on a standard CRT, the first [[Random-access memory|random-access]] digital storage device.<ref>{{citation |title=Early computers at Manchester University |journal=Resurrection |volume=1 |issue=4 |publisher=The Computer Conservation Society |date=Summer 1992 |url=http://www.cs.man.ac.uk/CCS/res/res04.htm#g |issn=0958-7403 |accessdate=19 April 2008}}</ref> The Manchester Small-Scale Experimental Machine (SSEM) was designed to show that the system was a practical storage device, by testing that data held within it could be read and written at the speed necessary for use in a computer.{{sfnp|Lavington|1998|pp=13, 24|ps=}}

For use in a [[binary numeral system|binary]] digital computer, the tube had to be capable of storing either one of two states at each of its memory locations, corresponding to the [[binary digit]]s (bits) 0 and&nbsp;1. It exploited the positive or negative [[electrostatic charge]] generated by displaying either a dash or a dot at any position on the CRT screen, a phenomenon known as [[secondary emission]]. A dash generated a positive charge, and a dot a negative charge, either of which could be picked up by a detector plate in front of the screen; a negative charge represented 0, and a positive charge&nbsp;1. The charge dissipated in about 0.2&nbsp;seconds, but it could be automatically refreshed from the data picked up by the detector.{{sfnp|Lavington|1998|p=12|ps=}}

The Williams tube was initially based on the CV1131, a commercially available {{convert|12|in|adj=on}} diameter CRT, but a smaller {{convert|6|in|adj=on}} tube, the CV1097, was used in the SSEM.{{sfnp|Lavington|1998|pp=12–13|ps=}}

== Development and design ==
[[File:BabyArchitecture.png|thumb|250px|right|Architectural schematic showing how the four [[cathode ray tube]]s (shown in green) were deployed]]
Following his appointment to the Chair of Electrical Engineering at Manchester University, Williams recruited his TRE colleague [[Tom Kilburn]] on secondment. By the autumn of 1947 the pair had increased the storage capacity of the Williams tube from one bit to 2,048, arranged in a 64 by 32-bit array,{{sfnp|Napper|2000|p=366|ps=}} and demonstrated that it was able to store those bits for four hours.{{sfnp|Lavington|1998|p=13|ps=}} Engineer Geoff Tootill joined the team on loan from TRE in September 1947, and remained on secondment until April 1949.{{sfnp|Lavington|1998|p=16|ps=}}

[[Max Newman]] had been appointed to the Chair of Pure Mathematics at Manchester University in 1945. During the Second World War he had worked as a cryptanalyst at [[Bletchley Park]], and had led the team which in 1943 produced the first of the Colossus code-breaking computers. Although Newman played no active role in the development of the SSEM, or any of the subsequent [[Manchester computers]], he was generally supportive and enthusiastic about the project, and arranged for the acquisition of war-surplus supplies for its construction, including [[General Post Office|GPO]] metal racks from Bletchley.{{sfnp|Lavington|1998|pp=6–7|ps=}}

By June 1948 the SSEM had been built and was working.{{sfnp|Napper|2000|p=366|ps=}} It was {{convert|17|ft}} in length, {{convert|7|ft|4|in}} tall, and weighed almost {{convert|1|LT}}. The machine contained 550&nbsp;[[Vacuum tube|valves]]—300&nbsp;[[diode]]s and 250&nbsp;[[pentode]]s—and had a power consumption of 3500&nbsp;[[watt]]s.<ref name=MMSI>{{cite web |url=http://www.msim.org.uk/media/33871703/thebaby,theworldsfirststored-programcomputer.pdf |title=The "Baby": The World's First Stored-Program Computer |publisher=Manchester Museum of Science & Industry |format=PDF |accessdate=15 November 2008 |mode=cs2}}</ref> The arithmetic unit was built using [[EF50]] pentode valves, which had been widely used during wartime.{{sfnp|Lavington|1998|p=13|ps=}} The SSEM used one Williams tube to provide 32 by 32-bit [[Word (data type)|words]] of [[random-access memory]] (RAM), a second to hold a 32-bit [[Accumulator (computing)|accumulator]] in which the intermediate results of a calculation could be stored temporarily, and a third to hold the current program [[Instruction (computer science)|instruction]] along with its [[Memory address|address]] in memory. A fourth CRT, without the storage electronics of the other three, was used as the output device, able to display the bit pattern of any selected storage tube.{{sfnp|Napper|2000|p=367|ps=}}

[[File:SSEM Manchester museum close up.jpg|left|thumb|250px|alt=Three tall racks containing electronic circuit boards|The output CRT is immediately above the input device, flanked by the monitor and control electronics.]]
Each 32-bit word of RAM could contain either a program instruction or data. In a program instruction, bits 0–12 represented the memory address of the [[operand]] to be used, and bits 13–15 specified the [[Opcode|operation]] to be executed, such as storing a number in memory; the remaining 16&nbsp;bits were unused.{{sfnp|Napper|2000|p=367|ps=}} The SSEM's [[0-operand instruction set|single operand]] architecture meant that the second operand of any operation was implicit: the accumulator or the program counter (instruction address); program instructions specified only the address of the data in memory.

A word in the computer's memory could be read, written, or refreshed, in 360&nbsp;microseconds. An instruction took four times as long to execute as accessing a word from memory, giving an instruction execution rate of about 700 per second. The main store was refreshed continuously, a process which took 20&nbsp;milliseconds to complete, as each of the SSEM's 32&nbsp;words had to be read and then refreshed in sequence.{{sfnp|Napper|2000|p=366|ps=}}

The SSEM represented negative numbers using [[two's complement]],{{sfnp|Lavington|1998|p=14|ps=}} as most computers still do. In that representation, the value of the [[most significant bit]] denotes the sign of a number; positive numbers have a zero in that position and negative numbers a one. Thus the range of numbers that could be held in each 32-bit word was −2<sup>31</sup> to +2<sup>31</sup>&nbsp;−&nbsp;1 (decimal: −2,147,483,648 to +2,147,483,647).

== Programming ==
The SSEM's three bit [[instruction set]] allowed a maximum of eight (2<sup>3</sup>) different instructions. In contrast to the modern convention, the machine's storage was arranged with the [[Endianness|least significant digits]] to the left; thus a one was represented in three bits as "100", rather than the more conventional "001".{{sfnp|Lavington|1998|p=14|ps=}}

{| class="wikitable" style="width:90%; margin:1em auto; clear:both; text-align:center;"
|+SSEM's instruction set{{sfnp|Lavington|1998|p=15|ps=}}
|-
! style="width:9%;" | Binary code
! style="width:10%;" | Original notation
! style="width:11%;" | Modern mnemonic
! style="width:70%;" | Operation
|-
| 000
| S,  Cl
| JMP S
| style="text-align:left;" | Jump to the instruction at the address obtained from the specified memory address S{{efn|name=PCInc|As the program counter was incremented at the end of the decoding process, the stored address had to be the target address -1.}}  (absolute unconditional jump)
|-
| 100
| Add S,  Cl
| JRP S
| style="text-align:left;" | Jump to the instruction at the program counter plus (+) the relative value obtained from the specified memory address S{{efn|name=PCInc}} (relative unconditional jump)
|-
| 010
| -S,  C
| LDN S
| style="text-align:left;" | Take the number from the specified memory address S, negate it, and load it into the accumulator
|-
| 110
| c,  S
| STO  S
| style="text-align:left;" | Store the number in the accumulator to the specified memory address S
|-
| 001 or<br /> 101{{efn|The  function bits were only partially decoded, to save on logic elements.{{sfnp|Lavington|1998|p=15|ps=none}}}}
| SUB S
| SUB S
| style="text-align:left;" | Subtract the number at the specified memory address S from the value in accumulator, and store the result in the accumulator
|-
| 011
| Test
| CMP
| style="text-align:left;" | Skip next instruction if the accumulator contains a negative value
|-
| 111
| Stop
| STP
| style="text-align:left;" | Stop
|- class="tfoot" style="text-align:left;"
|}

The awkward negative operations were a consequence of the SSEM's lack of hardware to perform any arithmetic operations except [[subtraction]] and [[additive inverse|negation]]. It was considered unnecessary to build an [[adder (electronics)|adder]] before testing could begin as addition can easily be implemented by subtraction,{{sfnp|Napper|2000|p=367|ps=}} i.e. ''x''+''y'' can be computed as −(−''x''−''y''). Therefore, adding two numbers together, X and Y, required four instructions:{{sfnp|Lavington|1998|p=15|ps=}}

{{Quote|<pre>LDN X // load negative X into the accumulator
SUB Y // subtract Y from the value in the accumulator
STO S // store the result at S
LDN S // load negative value at S into the accumulator</pre>}}

Programs were entered in binary form by stepping through each word of memory in turn, and using a set of 32&nbsp;switches known as the input device to set the value of each bit of each word to either 0 or 1. The SSEM had no [[punched tape|paper-tape reader or punch]].{{sfnp|Napper|2000|p=366|ps=}}

=== First programs ===
[[File:CRT memory.jpg|thumb|alt=Small cathode ray tube in a rusty metal frame|Output CRT]]
<!-- Cant find any reference to this: A division program was written, using pencil-and-paper method, operating on one bit at a time. It was used to divide 2<sup>30</sup>-1 by 31, giving the answer in about 1.5&nbsp;seconds. Then this routine was used in a program to show that 314,159,265 and 217,828,183 are [[Coprime|relatively prime]]. -->

Three programs were written for the computer. The first, consisting of 17&nbsp;instructions, was written by Kilburn, and so far as can be ascertained first ran on 21 June 1948.{{sfnp|Lavington|1998|pp=16–17|ps=}} It was designed to find the highest [[Divisor|proper factor]] of 2<sup>18</sup> (262,144) by trying every integer from 2<sup>18</sup>&nbsp;−&nbsp;1 downwards. The divisions were implemented by repeated subtractions of the divisor. The SSEM took 3.5&nbsp;million operations and 52&nbsp;minutes to produce the answer (131,072). The program used eight words of working storage in addition to its 17&nbsp;words of instructions, giving a program size of 25&nbsp;words.<ref name=TootillOriginalOriginal>{{citation |last=Tootill |first=Geoff |title=The Original Original Program |journal=Resurrection |issue=20 |publisher=The Computer Conservation Society |date=Summer 1998 |url=http://www.cs.man.ac.uk/CCS/res/res20.htm#e |issn=0958-7403 |accessdate=19 April 2008}}</ref>

Geoff Tootill wrote an amended version of the program the following month, and in mid-July Alan Turing—who had been appointed as a [[Reader (academic rank)|reader]] in the mathematics department at Manchester University in September 1948—submitted the third program, to carry out long division. Turing had by then been appointed to the nominal post of Deputy Director of the [[Computing Machine Laboratory]] at the university,{{sfnp|Lavington|1998|pp=16–17|ps=}} although the laboratory did not become a physical reality until 1951.{{sfnp|Lavington|1998|p=17|ps=}}

== Later developments ==
[[File:Kilburn Williams Plaque cropped.jpg|thumb|upright|left|A plaque in honour of Williams and Kilburn at the University of Manchester]]
Williams and Kilburn reported on the SSEM in a letter to the Journal ''[[Nature (journal)|Nature]]'', published in September 1948.<ref>{{cite journal |last1=Williams |first1=F. C. |author1-link=Frederic Calland Williams |last2=Kilburn |first2=T. |author2-link=Tom Kilburn |title=Electronic Digital Computers |journal=Nature |date=25 September 1948 |volume=162 |page=487 |url=http://www.computer50.org/kgill/mark1/natletter.html |accessdate=22 January 2009 |doi=10.1038/162487a0 |mode=cs2}}</ref> The machine's successful demonstration quickly led to the construction of a more practical computer, the {{nowrap|[[Manchester Mark 1]]}}, work on which began in August 1948. The first version was operational by April 1949,{{sfnp|Lavington|1998|p=17|ps=}} and it in turn led directly to the development of the [[Ferranti Mark 1]], the world's first commercially available general-purpose computer.<ref name=NapperMK1 />

In 1998, a working replica of the SSEM, now on display at the [[Museum of Science and Industry (Manchester)|Museum of Science and Industry in Manchester]], was built to celebrate the 50th anniversary of the running of its first program. Demonstrations of the machine in operation are held regularly at the museum.<ref>{{cite web |url=http://www.msim.org.uk/whats-on/meet-baby |title=Meet Baby |publisher=Manchester Museum of Science & Industry |accessdate=17 November 2008 |archiveurl=https://web.archive.org/web/20080626114414/http://www.msim.org.uk/whats-on/meet-baby |archivedate=26 June 2008 |deadurl=yes |mode=cs2}}</ref> In 2008, an original panoramic photograph of the entire machine was discovered at the University of Manchester. The photograph, taken on 15&nbsp;December 1948 by a research student, Alec Robinson, had been reproduced in ''[[The Illustrated London News]]'' in June 1949.<ref>{{cite news |url=http://www.telegraph.co.uk/science/science-news/3344661/Photo-of-great-grandfather-of-modern-computers-found.html |title=Photo of great grandfather of modern computers found |first=Roger |last=Highfield |newspaper=Daily Telegraph |date=17 June 2008 |accessdate=20 June 2008 |mode=cs2}}</ref>{{Clear}}

== See also ==
* [[Manchester computers]]
* [[History of computing hardware]]
* [[List of vacuum tube computers]]

== References ==
'''Notes'''
{{notelist|notes=}}

'''Citations'''
{{reflist|30em|refs=}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Brown |first=Louis |title=A Radar History of World War II: Technical and Military Imperatives |year=1999 |publisher=CRC Press |isbn=978-0-7503-0659-1}}
*{{citation |last=Copeland |first=Jack |contribution=Colossus and the Rise of the Modern Computer |editor-first=B. Jack |editor-last=Copeland |title=Colossus The Secrets of Bletchley Park's Codebreaking Computers |publisher=Oxford University Press |year=2010 |isbn=978-0-19-957814-6}}
*{{citation |last=Lavington |first=Simon |title=Early British Computers: The Story of Vintage Computers and the People who built them |year=1980 |edition=1st |publisher=Manchester University Press Society |isbn=978-0-7190-0803-0}}
*{{citation |last=Lavington |first=Simon |title=A History of Manchester Computers |year=1998 |edition=2nd |publisher=The British Computer Society |location=Swindon |isbn=978-1-902505-01-5}}
*{{citation |last=Napper |first=R. B. E. |contribution=The Manchester Mark 1 Computers |editor1-last=Rojas| editor1-first=Raúl |editor2-last=Hashagen |editor2-first=Ulf |title=The First Computers: History and Architectures |publisher=MIT Press |year=2000 |pages=356–377 |isbn=978-0-262-68137-7}}
{{refend}}

== Further reading ==
*{{citation |first=David |last=Anderson |contribution=Was the Manchester Baby conceived at Bletchley Park? |title=Alan Mathison Turing 2004: A celebration of his life and achievements |publisher=[[British Computer Society]] |date=4 June 2004 |url=http://www.bcs.org/upload/pdf/ewic_tur04_paper3.pdf |format=PDF |accessdate=16 November 2008 |ref=none}}

== External links ==
{{commons category}}
* [http://www.computer50.org/ Computer 50]&nbsp;– A website celebrating the 50th anniversary of the SSEM in 1998.
* [http://www.cs.manchester.ac.uk/Digital60/ Digital60]&nbsp;– A website celebrating the 60th anniversary of the SSEM in 2008.
* [http://www.computer50.org/mark1/new.baby.html A brief history of the Small-Scale Experimental Machine]
* [http://www.davidsharp.com/baby Manchester Baby Simulator software]
* [http://www.d60.org.uk/race.php BabyRace]&nbsp;– Run original program on a mobile phone and compare the performance with the Small-Scale Experimental Machine
* [http://news.bbc.co.uk/1/hi/technology/7465115.stm BBC article on Baby]
*[http://sounds.bl.uk/View.aspx?item=021M-C1379X0002XX-0001V0.xml Oral history interview with Geoff Tootill], one member of the team that designed and built the Manchester Small-Scale Experimental Machine, recorded for [http://www.bl.uk/historyofscience An Oral History of British Science] at the British Library.

{{featured article}}


[[Category:1940s computers]]
[[Category:Early British computers]]
[[Category:One-of-a-kind computers]]
[[Category:Vacuum tube computers]]
[[Category:Computer-related introductions in 1948]]
[[Category:History of Manchester]]
[[Category:School of Computer Science, University of Manchester]]
[[Category:History of electronic engineering]]