<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Hurricane
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Sports aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=Royal Aircraft Establishment Aero Club
 | designer=Samuel Childs
 | first flight= mid-1923
 | introduced=
 | retired=1926
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''RAE Hurricane''' was a single-seat, single-engined light [[monoplane]] designed and built by the Aero Club of the [[Royal Aircraft Establishment]] for the [[Lympne light aircraft trials|1923 Lympne Motor Glider Competition]].  It was underpowered with an unreliable engine.  Re-engined, it flew in many races, with first place in the 1926 [[Grosvenor Challenge Cup]] its greatest success.

==Design and development==
Amongst the many aircraft designed and built for the [[Lympne light aircraft trials|Lympne]] light aircraft competitions of the mid-1920s, the RAE Hurricane and the [[Cranwell CLA.2]] had one other thing in common.  Both were product of amateur groups formed within government funded aeronautical establishments, the Royal Aircraft Establishment at [[RAE Farnborough|Farnborough]] and the [[RAF Cranwell|Cranwell RAF College]].  The Hurricane was built for speed and flew in the 1923 [[Lympne light aircraft trials|Lympne Motor Glider Competition]], the CLA.2 for durability, flying in the two-seater competition the following year.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200659.html ''Flight'' 9 October 1924  p.650]</ref>

The Hurricane was a wooden framed, fabric covered shoulder wing monoplane.<ref name="OrdH">{{Harvnb|Ord-Hume|2000|pages=454–5}}</ref>  The wing was a two spar cantilever structure,<ref name="FlightA">[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200625.html ''Flight'' 11 October 1923  p.625]</ref>     made possible by the adoption of one of the novel thick Göttingen airfoil sections.<ref name="OrdH"/>  It was tapered, but almost entirely on the trailing edge and with clipped wingtips.  Seen in plan, the wing of the Hurricane looked much a late 20th-century light aircraft.<ref name="OrdH"/><ref name="FlightA"/> The competition rules required that aircraft could access suitable fields via a farm gate, and many met this condition with folding wings.  The Hurricane's designer, Samuel Childs chose instead to make its wings easily detachable after removing eight bolts per side.<ref name="OrdH"/><ref name="FlightA"/>

The fuselage of the Hurricane was in contrast very much of its time, built up on two upper longerons and a strongly curved keel, producing a structure that was narrow and almost parallel in plan and a thin triangle in cross section, deepest between engine and the wing leading edge, tapering rapidly aft.<ref name="OrdH"/><ref name="FlightA"/> The single-seat open cockpit was at mid-chord, between the spars.  The main undercarriage was a pair of fabric covered mtorcycle type wire wheels mounted at the ends of a horizontal leaf spring fixed to the keel and braced to the fuselage with a pair of V-struts,<ref name="OrdH"/> with the result that the Hurricane sat very close to the ground.  At the rear the empennage was conventional, with a broad chord fin bearing an unbalanced rudder that extended between split elevators to the keel.  Initially the tailplane leading edge was curved and merged into the curved elevator tips.<ref name="FlightA"/>

Forward of the wing, the fuselage tapered rapidly,  mostly through the upward bend of the keel. Because the 600 cc Douglas flat twin engine used in the 1923 aircraft was geared down 2:1 via a front-mounted chain drive that put the propeller shaft at the top of the motor, it was mounted low and to the rear of the nose, with cylinders exposed for cooling. The output of this engine was only 21.5&nbsp;hp (16&nbsp;kW).<ref name="OrdH"/>

Several changes were made during 1924.<ref name="OrdH"/><ref name="AJJ">{{Harvnb|Jackson|1960|pages=409–10}}</ref>  The Douglas engine was replaced with a 32&nbsp;hp (24&nbsp;kW) [[Bristol Cherub]] II flat twin.  This more powerful motor was also lighter, lacking the reduction gear of the Douglas and therefore mounted in the extreme nose with propeller and exposed cylinders at wing level.  At about the same time the empennage was modified with a squarer tailplane,<ref>This may have been done first, as the g/a diagram in Ord-Hume shows the new tailplane with old fin and the Douglas engine</ref> taller, less rounded  fin and square tipped rudder.  The leaf spring undercarriage was replaced with a lighter split axle unit carrying smaller wheels and braced with conspicuous V-struts to the wing roots.<ref name="OrdH"/><ref name="AJJ"/> There were also modifications to the top of the fuselage,<ref name="OrdH"/> including the installation of a side hinged cockpit cover with an opening just large enough to allow the pilot's head to protrude.<ref name="AJJ"/>

==Operational history==
Only one Hurricane was built, registered ''G-EBHS''.<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EBHS CAA certificate for ''G-EBHS'']</ref> Since the Lympne trials were in October, the Hurricane must have made its first light in the late summer of 1923, probably piloted by [[George Bulman (pilot)|Paul Bulman]]. At the trials, with Bulman at the controls it flew rather tail down, suggesting trim and wing incidence issues had not been sorted.<ref name="OrdH"/><ref name="FlightB">[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200637.html ''Flight'' 18 October 1923  p.637]</ref>  Compared with some other competitors it was overweight; like many others it suffered from an unreliable engine. Worst, it was slow; it had been entered specifically for the Abdullah speed prize and there had been talk of speeds near 100&nbsp;mph, but the best it could do was 58.5&nbsp;mph (94&nbsp;km/h)<ref name="FlightB"/> before retiring with a broken rocker arm.<ref>[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200640.html ''Flight'' 18 October 1923  p.640]</ref> The winner of this £500 prize was the [[Parnall Pixie|Parnall Pixie II]] at 76.1&nbsp;mph.<ref>[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200648.html ''Flight'' 18 October 1923  p.648]</ref>

As a single seater, the Hurricane did not qualify for the 1924 [[Lympne|Lympne Two Seater Light Plane Competition]], but flew in the Grosvenor Challenge Cup immediately afterwards, now powered with the Cherub engine. The extra power produced a speed of about 80&nbsp;mph, but it failed at just past the half way stage.<ref name="OrdH"/><ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200659.html ''Flight'' 9 October 1924  p.659]</ref>   Its most successful meeting was at Lympne in August 1925,  flown by F/Lt J. F. Chick where it won the light Plane Holiday Handicap, the Private Owners Race and, most prestigiously the eight lap, 100 mile (161&nbsp;km) Grosvenor Challenge Cup,<ref>[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200538.html ''Flight'' 27 August 1925  p.551]</ref> averaging 81.2&nbsp;mph (131&nbsp;km/h).<ref>[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200551.html ''Flight'' 20 August 1925  p.538]</ref>  In total, the Hurricane picked up £300 in prizes at that meeting.<ref name="AJJ"/>  It was flown by Chick in its last Grosvenor Cup in 1926; although it was faster than before (84.8&nbsp;mph or 136&nbsp;km/h), it only managed third place.<ref>[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200695.html ''Flight'' 23 August 1926  p.619]</ref>  It was broken up later that year.<ref name="AJJ"/>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Cherub powered) ==

{{Aircraft specs
|ref={{harvnb|Ord-Hume|2000|page=455}}<!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=17
|length in=8
|length note=
|span m=
|span ft=23
|span in=0
|span note=
|height m=
|height ft=4
|height in=7
|height note=
|wing area sqm=
|wing area sqft=80
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=350
|empty weight note=
|gross weight kg=
|gross weight lb=550
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Bristol Cherub]] II
|eng1 type=flat twin 
|eng1 kw=<!-- prop engines -->
|eng1 hp=32<!-- prop engines -->
|eng1 note=
|power original=
|thrust original=
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=85
|max speed kts=
|max speed note=
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=64<!-- if max speed unknown -->
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Citations and notes===
{{reflist|3}}

===Cited sources===
{{commons category|RAE aircraft}}
*{{cite book |title= British Civil Aircraft 1919-59|last=Jackson|first=A.J.| year=1960|volume= 2|publisher=Putnam Publishing |location=London |isbn=|ref=harv}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6 |ref=harv}}
{{refbegin}}
{{refend}}

<!-- ==External links== -->
{{RAE Aero Club aircraft}}

[[Category:British sport aircraft 1920–1929]]