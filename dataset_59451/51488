{{Infobox journal
| title = Human Relations
| cover = [[File:Human Relations front cover image.jpg]]
| editor = Paul Edwards 
| discipline = [[Management]]
| former_names = 
| abbreviation = Hum. Relat.
| publisher = [[Sage Publications]] in association with the [[Tavistock Institute of Human Relations]]
| country = 
| frequency = Monthly
| history = 1947–present
| openaccess = 
| license = 
| impact = 2.619
| impact-year = 2015
| website = http://www.tavinstitute.org/humanrelations
| link1 = http://hum.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hum.sagepub.com/content/by/year
| link2-name = Online archive
| link3 = http://www.sagepub.com/journals/Journal200870/title
| link3-name = Journal page at publisher website
| JSTOR = 
| OCLC = 1752393
| LCCN = 50057567
| CODEN = HUREAA
| ISSN = 0018-7267 
| eISSN = 1741-282X
}}
'''''Human Relations''''' is a monthly [[peer-reviewed]] [[academic journal]] covering research on social relationships in work-related settings. The journal is published by [[Sage Publications]] on behalf of the [[Tavistock Institute of Human Relations]] (London). The journal was established in 1947 by the Tavistock Institute and the Research Center for Group Dynamics at the [[Massachusetts Institute of Technology]].

== Abstracting and indexing == 
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of  2.619, ranking it 4th out of 93 journals in the category "Social Sciences, Interdisciplinary"<ref name=WoS1>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Social Sciences, Interdisciplinary |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref> and 37th out of 192 journals in the category "Management".<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Management |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links == 
* {{Official website|http://www.humanrelationsjournal.org}}

{{DEFAULTSORT:Human Relations (Journal)}}
[[Category:Business and management journals]]
[[Category:Human resource management journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1947]]
[[Category:English-language journals]]


{{management-journal-stub}}