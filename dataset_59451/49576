{{Infobox book |
| name          = The Cabin and Parlor; or, Slaves and Masters
| image         = Cabinandparlor.jpg
| caption = Title page from the first edition of 1852
| author        = J. Thornton Randolph (pseudonyum of [[Charles Jacobs Peterson]])
| illustrator   = 
| cover_artist  = 
| country       = United States
| language      = English
| genre         = [[Anti-Tom literature|Plantation novel]] 
| publisher     = T. B. Peterson Ltd.
| release_date  = [[1852 in literature|1852]]
| media_type    = Print ([[Hardcover]] & [[Paperback]]) & [[E-book]]
| pages         = c.300 pp (May change depending on the publisher and the size of the text)
}}

'''''The Cabin and Parlor; or, Slaves and Masters''''' is an [[1852 in literature|1852]] novel written by [[Charles Jacobs Peterson]] under the pseudonym of J. Thornton Randolph.

== Overview ==

''The Cabin and Parlor'' is an example of the pro-slavery [[Anti-Tom literature|plantation literature]] genre that emerged from the [[Southern United States]] in response to the [[Abolitionism in the United States|abolitionist]] novel ''[[Uncle Tom's Cabin]]'' by [[Harriet Beecher Stowe]], which had been published in book form in that year, and had been criticised in the [[Southern United States]] for exaggerating the workings of slaveholding.<ref>[http://www.enotes.com/uncle-toms/ ''Uncle Tom's Cabin'', eNotes]</ref>

Whereas the majority of anti-Tom novels focussed on the evils of abolitionism, Peterson instead attacks the [[Capitalism|capitalist]] attitudes of the north, as well as their use of "white slaves" (the [[working class]]es) over black slaves. This attitude would appear again in Caroline Rush's ''[[The North and the South; or, Slavery and Its Contrasts]]'', also published in 1852.<ref>[http://www.iath.virginia.edu/utc/proslav/rushhp.html ''North and South'', Anti Uncle Tom Novels, Uncle Tom's Cabin & American Culture, The University of Virginia]</ref>

== Plot summary ==

The story begins with the sudden death of a wealthy [[Virginia]] landowner, Mr. Courtenay, a kindly plantation owner who has died before being given the opportunity to pay off his debts, leaving his family in debt and facing [[Bankruptcy|destitution]].

In an effort to pay off the debts, the family sell their slaves, among whom is the kindly Uncle Peter, who takes a liking to Courtenay's daughter, Isabel, and vows to help her in any way possible in thanks for the kindness shown to him by the Courtenays. The money earned is nominal, leaving it to Isabel and her brother Horace to acquire jobs in order to pay the remaining bills and to support their ailing mother.

Isabel finds a job as a [[Teacher|schoolteacher]], whilst Horace heads to an unidentified city in the north (inferred to be [[Philadelphia]]), where he becomes a "Northern slave" (i.e. a [[clerk (position)|clerk]]) to the malevolent Mr. Sharpe, a ruthless capitalist who works Horace like as though he were a white slave.

As the Courtenays continue to struggle, Isabel eventually finds comfort in a young slaveowner named Walworth, the son of an old [[Virginia]] family, who travels back and forth between north and south. When Horace dies of exhaustion in the north, Walworth comforts him in his final hours, and delivers his final requests to his sister in the south.

Whilst travelling together, Walworth and Isabel are caught in the midst of an anti-Black riot, from which Walworth is able to save Isabel from harm. Isabel, eternally grateful, begins to have romantic feelings for Walworth, and they eventually marry. The marriage, by a twist of fate, allows Isabel to reclaim her wealth and property - including her slaves - and is finally reinstated at Courtenay Hall.

== Characters ==

*'''Isabel Courtenay''': The heroine of the novel. She is one of Courtenay's two children, who struggles to pay her father's debts following his death.
*'''Horace Courtenay''': The son of Mr. Courtenay and Isabel's brother, who travels north to acquire a job. He eventually dies in the North, having been exhausted by his overbearing employers.
*'''Walworth''': A young, [[English American]] slaveowner, whose family have deep roots in [[Virginia]]. He, with the help of Uncle Peter, helps the Courtenays to regain their wealth and property.
*'''Uncle Peter''': One of the main protagonists of the novel - a kindly slave once owned by the Courtenays who wishes to help his former masters during their difficult time in return for the kindness shown towards him.
*'''Mr. Sharpe''': A malevolent, bullying [[Capitalism|capitalist]] who employs Horace, only to treat him with contempt, literally working him to death. To an extent, Sharpe appears to fill the role of [[Uncle Tom's Cabin#Simon Legree|Simon Legree]] from ''Uncle Tom's Cabin''.
*'''Mr. Courtenay''': The resident of Courtenay Hall in [[Virginia]], who dies suddenly at the beginning of the novel, leaving his family in debt.

== Publication history ==

Peterson's novel was among the earliest examples of the plantation literature genre, having first appeared six months after ''Uncle Tom's Cabin'' appeared in book form.<ref>[http://www.iath.virginia.edu/utc/proslav/randolphhp.html ''The Cabin and Parlor'', Anti Uncle Tom Novels, Uncle Tom's Cabin & American Culture, The University of Virginia]</ref>

The publishers of Peterson's novel - T.B. Peterson Ltd. (Theophilus B. Peterson was Charles' brother) - would later go on to publish other anti-Tom novels, most notably the [[1854 in literature|1854]] novel ''[[The Planter's Northern Bride]]'' by [[Caroline Lee Hentz]].<ref>[http://www.iath.virginia.edu/utc/proslav/hentzhp.html ''The Planter's Northern Bride'', Anti Uncle Tom Novels, Uncle Tom's Cabin & American Culture, The University of Virginia]</ref>

== References ==

{{reflist}}

== External links ==
*[http://www.iath.virginia.edu/utc/proslav/randolphhp.html ''The Cabin and Parlor'' at the University of Virginia]

{{Uncle Tom's Cabin}}

{{DEFAULTSORT:Cabin And Parlor}}
[[Category:1852 novels]]
[[Category:Anti-Tom novels]]
[[Category:19th-century American novels]]
[[Category:Works published under a pseudonym]]
[[Category:Novels set in Virginia]]