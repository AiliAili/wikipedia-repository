{{Infobox Political post
|post            = Comptroller
|body            = the City of New York
|insignia        =
|insigniasize    = 140px
|insigniacaption = Seal of the City of New York
|image           =
|incumbent       = [[Scott Stringer]]
|incumbentsince  = January 1, 2014
|style           = 
|salary          = $151,500
|residence       = 
|appointer       = 
|termlength      = 4 years (renewable)
|formation       = 1801 (1898 [[City of Greater New York|consolidated City]])
|website         = [http://www.comptroller.nyc.gov New York City Office of the Comptroller website]}}

The Office of '''Comptroller of New York City''' is the [[Chief financial officer|chief fiscal officer]] and [[Chief audit executive|chief auditing officer]] of the city. The [[comptroller]] is elected, citywide, to a four-year term and can hold office for three consecutive terms.  The current comptroller is [[Democratic Party (United States)|Democrat]] [[Scott Stringer]], the former [[Borough President]] of [[Manhattan]]. Stringer was elected on November 5, 2013.

==Duties==
The comptroller is responsible for auditing the performance and finances of city agencies, making recommendations regarding proposed contracts, issuing reports on the state of the city economy, marketing and selling municipal bonds, and managing city debt.  The comptroller also "is the custodian and investment advisor to the Boards" of the five pension funds which are collectively referred to as "NYC Public Pension Funds" or "New York City pension funds".<ref name="nycpubpen_compoff">{{cite web | url=http://comptroller.nyc.gov/general-information/pension-funds-asset-allocation/ | title=NYC Public Pension Funds | accessdate=8 January 2015 | publisher=The New York City Comptroller's Office }}</ref><ref name="lopez2015" />  The funds collectively amounted to {{currency|158.7 billion}} as of September 30, 2014.<ref name="lopez2015">{{cite news | author=Lopez, Luciana | editor=Adler, Leslie | work=[[Wall Str. J.]] | date=7 January 2015 | title=New York City pension funds moved $4.9 billion out of Pimco in 2014 | via=Reuters | url=http://www.reuters.com/article/2015/01/07/us-newyork-pensions-pimco-idUSKBN0KG2DC20150107 }}</ref> The comptroller's regulations are compiled in title 44 of the ''[[New York City Rules]]''.

==History==
The office was created as an appointive office in 1801. Thirty years later, the comptroller became head of the department of finance. In 1884 the office became elective, and in 1938 the comptroller became head of a separate, independent department of the [[New York City Government|City's government]]. Until it was found unconstitutional in 1989, the comptroller served on the eight-member [[New York City Board of Estimate]], which was composed of the [[Mayor of New York City]], the comptroller and the president of the [[New York City]] Council, each of whom was elected citywide and had two votes, and the five [[Borough president]]s, each having one vote.

If vacancies should simultaneously occur in the offices of [[Mayor of New York City]] and [[New York City Public Advocate]] (formerly president of the city council or board of aldermen), the comptroller would become acting mayor. These have been the three offices elected citywide, so traditional practice has tried to balance a winning three-candidate ticket among the city's different ethnic, religious and political interests (and, more recently, between the sexes). But, while there is a delicate interaction between the campaigns for the three offices, the actual election results can sometimes differ quite markedly.

==2009 election==
{{main|New York City Comptroller election, 2009}}
The [[Democratic Party (United States)|Democratic]] nominee in the 2009 general election, [[John Liu]] won 76% of the citywide vote on Tuesday, November 3. The [[Republican Party (United States)|Republican]] nominee, Joseph Mendola, won 19.3%; the [[Conservative Party of New York|Conservative]] nominee, Stuart Avrick, 2.5%; and others 2.3%.<ref>[http://cityroom.blogs.nytimes.com/2009/09/29/liu-and-de-blasio-lead-in-early-returns/?hpw, ''The New York Times'' coverage of the 2009 NYC Comptroller race], September 29, 2009; retrieved September 26, 2013.</ref><ref>[http://manhattan.about.com/od/newsmedia/a/nycprimaryelection.htm Coverage of the 2009 NYC Comptroller race], manhattan.about.com</ref><ref>[http://elections.nytimes.com/2009/results/index.html 2009 New York City Comptroller election results], ''[[The New York Times]]'', published and retrieved November 3, 2009.</ref>

==2013 election==
{{Main|New York City Comptroller Election, 2013}}
Manhattan Borough President [[Scott Stringer]] won the September 10, 2013 Democratic primary with 52% of the vote, defeating former [[New York State Governor]], [[Eliot Spitzer]], who had been forced to resign as governor over various scandals.<ref>[http://www.reuters.com/article/2013/09/11/us-usa-politics-newyork-comptroller-idUSBRE98A05R20130911 Eliot Spitzer fails in his New York political comeback attempt], ''[[Reuters]]'', published and retrieved September 11, 2013.</ref> Former Wall Street financier [http://www.nydailynews.com/blogs/dailypolitics/2013/06/republican-wall-street-vet-john-burnett-ready-to-jump-into-race-for-nyc-comptr John Burnett] was unopposed as the Republican candidate in 2013.<ref>[http://vigilantsquirrelbrigade.blogspot.com/2013/06/meet-gop-candidate-for-nyc-comptroller.html]</ref> The Libertarian Party has nominated Hesham El-Meligy as their candidate for the office. In a prior convention, since declared invalid, they had nominated Kristin Davis, who decided not to challenge the new convention's outcome. {{cn|date=September 2013}}

==History==
===Comptrollers of the City of New York before Consolidation (1898)===
*1802&ndash;1805 [[Selah Strong]]
*1805&ndash;1806 [[Benjamin Romaind]]
*1806&ndash;1807 [[Isaac Stoutenburg]]
*1807 [[Jacob Morton]] 
*1808&ndash;1813 [[Garret N. Bleecker]]
*1813&ndash;1816 [[Thomas Mercein]]    
*1816&ndash;1831 [[Garret N. Bleecker]]
*1831&ndash;1836 [[Talman J. Waters]]
*1836&ndash;1839 [[Douw D. Williamson]]
*1839&ndash;1842 [[Alfred A. Smith]]
*1842 [[Douw D. Williamson]]
*1843&ndash;1844 [[Alfred A. Smith]]
*1844&ndash;1845 [[Douw D. Williamson]]   
*1845&ndash;1848 [[John Ewen (general)|John Ewen]]
*1848&ndash;1949 [[Talman J. Waters]]
*1849 [[John L. Lawrence]]
*1850&ndash;1853 [[Joseph R. Taylor]]
*1853&ndash;1859 [[Azariah C. Flagg]]
*1859&ndash;1863 [[Robert T. Haws]]
*1863&ndash;1867 [[Matthew T. Brennan]]
*1867&ndash;1871 [[Richard B. Connolly]]
*1871&ndash;1876 [[Andrew H. Green]]
*1876&ndash;1881 [[John Kelly (U.S. politician)|John Kelly]]
*1881&ndash;1883 [[Allan Campbell (American politician)|Allan Campbell]]
*1883&ndash;1884 [[S. Hastings Grant]]
*1884&ndash;1888 [[Edward V. Loss]] 
*1888&ndash;1894 [[Theodore W. Myers]]
*1894&ndash;1898 [[Ashbel P. Fitch]]

===Comptrollers of the City of New York since Consolidation (1898)===
*1898&ndash;1901 [[Bird Sim Coler|Bird S. Coler]]
*1902&ndash;1905 [[Edward M. Grout]]
*1906&ndash;1909 [[Herman A. Metz]]
*1910&ndash;1917 [[W. A. Pendergast]]
*1918&ndash;1925 [[Charles Lacy Craig]]
*1926&ndash;1932 [[Charles W. Berry]]
*1933 [[George McAneny]]
*1934 [[Arthur Cunningham (politician)|Arthur Cunningham]]
*1935 [[Joseph D. McGoldrick]]
*1936&ndash;1937 [[Frank J. Taylor]]
*1938&ndash;1945 [[Joseph D. McGoldrick]]
*1946&ndash;1953 [[Lazarus Joseph]]
*1954&ndash;1961 [[Lawrence E. Gerosa]]
*1962&ndash;1965 [[Abraham D. Beame]]
*1966&ndash;1969 [[Mario Procaccino]]
*1970&ndash;1973 [[Abraham D. Beame]]
*1974&ndash;1989 [[Harrison J. Goldin]]
*1990&ndash;1993 [[Elizabeth Holtzman]]
*1994&ndash;2001 [[Alan G. Hevesi]]
*2002&ndash;2009 [[Bill Thompson (New York)|William Thompson]]
*2010&ndash;2013 [[John Liu]]
*2014&ndash;Present [[Scott Stringer]]

==References==
{{Reflist|33em}}

==Sources==
*Article on "comptroller" by Noel C. Garelick in ''[[The Encyclopedia of New York City]]'', edited by [[Kenneth T. Jackson]] ([[Yale University Press]] and The [[New-York Historical Society]], [[New Haven, Connecticut]], 1995; ISBN 0-300-05536-6)
*[http://comptroller.nyc.gov/office-of-the-comptroller/nyc-comptrollers/ NYC Comptrollers (1802 – Present)]

==External links==
*[http://www.comptroller.nyc.gov/ New York City Office of the Comptroller]
* [http://rules.cityofnewyork.us/codified-rules?agency=NYCCOMP Comptroller] in the [[Rules of the City of New York]]

{{New York City Government}}
{{New York City Comptroller}}

[[Category:New York City Comptrollers| ]]
[[Category:Citywide elected offices of New York City|Comptroller]]