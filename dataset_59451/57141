{{Infobox journal
| title = German Economic Review
| discipline = [[Economics]]
| abbreviation = Ger. Econ. Rev.
| editor = [[Helmuth Cremer]], [[Joseph F. Francois]], [[Ben J. Heijdra]], [[Walter Krämer]], [[Wolfgang Leininger]], [[Christoph M. Schmidt]]
| publisher = [[John Wiley & Sons]] on behalf of the [[Verein für Socialpolitik]]
| frequency = Quarterly
| history = 2000–present
| impact = 0.543
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-0475
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-0475/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-0475/issues
| link2-name = Online archive
| ISSN = 1465-6485
| eISSN = 1468-0475
| OCLC = 231868508
| LCCN = 00235260
}}
The '''''German Economic Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] of [[economics]] published quarterly by [[John Wiley & Sons]] on behalf of the [[Verein für Socialpolitik]], of which it is an official journal. It was established in 2000. The current [[editors-in-chief]] are [[Helmuth Cremer]], [[Joseph F. Francois]], [[Ben J. Heijdra]], [[Walter Krämer]], [[Wolfgang Leininger]], and [[Christoph M. Schmidt]].

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.543, ranking it 246th out of 345 journals in the category "Economics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Economics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of economics journals]]
* [[List of political science journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-0475}}

{{DEFAULTSORT:German Economic Review}}
[[Category:Economics journals]]
[[Category:Quarterly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2000]]


{{econ-journal-stub}}