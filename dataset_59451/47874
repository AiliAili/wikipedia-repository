{{Infobox film
| name           = Feminist Stories from Women's Liberation
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Jennifer H. Lee
| producer       = 
| writer         = Jennifer H. Lee
| screenplay     = 
| story          = 
| based on       = <!-- {{based on |[title of the original work] |[writer of the original work]}} -->
| starring       = 
| narrator       = 
| music          = Fanny and Robert Wait
| cinematography = 
| editing        = 
| studio         =      <!-- or: | production companies = -->
| distributor    = [[Women Make Movies]]
| released       =    March 2013
| runtime        = 64 minutes
| country        = 
| language       = 
| budget         = 
| gross          = <!-- Please use condensed and rounded values, e.g. "£11.6 million" not "£11,586,221" -->
}}

'''''Feminist: Stories from Women's Liberation''''' (2013) is a one-hour documentary film written and directed by Jennifer Lee.<ref name="LeeAbout">{{cite web |url=http://www.feministstories.com/about-jennifer-lee/ |title=About: Jennifer Lee |work=Feminist: Stories from Women's Liberation |accessdate=2014-12-03 }}</ref>

==Synopsis==
[[Women Make Movies]] wrote: "Structured as a personal journey of rediscovery by filmmaker Jennifer Lee, this documentary brings the momentous first decade of secondwave feminism vividly to life. Its trajectory starts with the earliest stirrings in 1963 and ends with the movement’s full blossoming in 1970—from the Presidential Commission’s report on widespread discrimination against women and publication of Betty Friedan’s Feminine Mystique up through radical feminists’ takeover of the Statue of Liberty and Friedan’s calls for a women’s strike for equality.

Thirty-five diverse interviewees, including rank-and-file activists along with well-known feminists [[Betty Friedan]], [[Frances M. Beale]], [[Gloria Steinem]], [[Robin Morgan]], [[Ti-Grace Atkinson]], and others, share memories of the period as well as issues and challenges that still resonate today."<ref name="WMMsyn">{{cite web |url=http://www.wmm.com/filmcatalog/pages/c876.shtml |title=Feminist: Stories From Women’s Liberation |work=[[Women Make Movies]] |accessdate=2014-12-03 }}</ref>

==Production==
Director Jennifer Lee majored in film and women's studies at [[Hampshire College]] before working in the technical departments of [[Warner Bros.]], [[Sony]] and [[Industrial Light & Magic]].<ref>{{cite web |url=http://www.waccglobal.org/articles/gmmp-ambassador-feminist-filmmaker-jennifer-lee |title=GMMP Ambassador: feminist filmmaker Jennifer Lee |last=De Santis |first=Solange |publisher=[[World Association for Christian Communication]] |date=2014-02-21 |accessdate=2014-12-03 }}</ref>

Lee worked on the film "in bits and pieces" for nine years while working full-time.<ref name="MsMag">{{cite news |last=Derr |first=Holly L. |url=http://msmagazine.com/blog/2013/12/03/how-much-do-you-know-about-womens-lib/ |title=How Much Do You Know About "Women’s Lib"? |work=Ms. Magazine Blog |publisher=[[Ms. (magazine)]] |date=2013-12-03 |accessdate=2014-12-03 }}</ref> She combined selected archival footage with the interviews,<ref name="WMMsyn"/> including the last recorded video interview with Betty Friedan.<ref name="SILive">{{cite news |url=http://www.silive.com/news/index.ssf/2013/12/filmmaker_with_staten_island_c.html |title=Filmmaker with Staten Island connections to screen feminist documentary |work=SI Live |publisher=[[Staten Island Advance]] |date=2013-12-04 |accessdate=2014-12-03 }}</ref> The production received $12,000 support from a [[Kickstarter]] campaign,<ref name="PasaWeek">{{cite news |last=Snortland |first=Ellen |url=http://www.pasadenaweekly.com/cms/story/detail/on_the_shoulders_of_women/12478/ |title=On the shoulders of women |work=Pasadena Weekly |publisher=[[Southland Publishing]] |date=2013-10-02 |accessdate=2014-12-03 }}</ref><ref name="MercTex">{{cite news |last=Hardikar |first=Priyanka |url=http://utdmercury.com/director-shares-stories-womens-rights-struggle/ |title=Director shares stories of women’s rights struggle |work=The Mercury |location=Dallas |publisher=[[University of Texas at Dallas]] |date=2014-10-06 |accessdate=2013-09-14 }}</ref> but Lee has also stated 'My budget was 90 percent financed by me.'<ref name="Time2014">{{cite news |last=Lee |first=Jennifer |url=http://time.com/2853184/feminism-has-a-bra-burning-myth-problem/ |title=Feminism Has a Bra-Burning Myth Problem |work=[[Time (magazine)]] |date=2014-06-12 |accessdate=2014-12-03 }}</ref>

Lee has written of her experience in ''[[Time (magazine)|Time]]'':
<blockquote>'I began making my film, Feminist, in 2004 as a straightforward documentary about historical facts, but I learned so much that I finished the film a different person... I figured we don’t celebrate the Women’s Movement like we do other social movements because it was too complex—encompassing issues of class, religion, race, and language, among others... I thought it might be hard to pull out individual successes to honor. But I was wrong: As I worked on the film, I discovered many concrete successes—including ones that weren’t included in textbooks or honored in public memorials.'.<ref name="Time2014"/></blockquote>

==Release==
Lee brought her film to [[Islamabad]], Pakistan, and screened it to three universities including the [[International Islamic University, Islamabad]].<ref name="Pakistan">{{cite web |url=http://www.feministstories.com/islamabad-screening/|title=Islamabad, Pakistan screening and video series |work=Feminist: Stories from Women's Liberation |accessdate=2014-12-03 }}</ref>  It has also screened at film festivals in the United States,<ref name="WBAI">{{cite web |url=http://wbai.org/programupdates.php?programupdate=239 |title=WBAI-FM Program Highlight: Joy of Resistance Presents Feminist: Stories From Women’s Liberation |work=[[WBAI]] |accessdate=2014-12-03 }}</ref> and conferences including the 2014 [[National Organization for Women]] conference and the 20th anniversary conference of [[Veteran American Feminists]].<ref name="PasaWeek"/><ref>{{cite web|url=http://now.org/about/conference/workshops/ |title=Workshops & Feminist Flicks |publisher=[[National Organization for Women]] |date=2014-02-21 |accessdate=2014-12-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20141204071641/http://now.org/about/conference/workshops/ |archivedate=2014-12-04 |df= }}</ref> The film is distributed through ''Women Make Movies''.<ref name="WMMsyn"/>

==Reception==
The film  was awarded "Best of Festival Documentary" at the 2013 ''Los Angeles Women's International Film Festival'' and was an Official Selection of the [[Cincinnati Film Festival]].<ref name="WBAI"/><ref>{{cite news |last=Beck |first=Laura |url=http://jezebel.com/5993106/righteous-feminist-documentary-wins-award-at-film-fest |title=Righteous Feminist Documentary Wins Award at Film Fest |work=[[Jezebel (website)]] |date=2013-04-01 |accessdate=2013-09-14 }}</ref>

Ellen Snortland of the ''[[Pasadena Weekly]]'' wrote, "If I had a magic wand and could bonk you with it, I'd spirit you to a screening of a documentary I wish were mandatory viewing in schools: It's called "Feminist: Stories from Women's Liberation."<ref name="PasaWeek"/>

Kamala Lopez of ''[[The Huffington Post]]'' wrote: "Remembering and studying the women who broke the barriers before us is not just the right thing to do; we ignore their hard-won lessons at our own peril. For American women, our careless amnesia is plain poison and Lee's film is a healthy portion of the antidote". Adding that the history of [[second-wave feminism]] is "beautifully laid out in Lee's film".<ref>{{cite news |last=Lopez |first=Kamala |url=http://www.huffingtonpost.com/kamala-lopez/amnesia-feminisms-poison-_b_2968594.html |title=Amnesia: Feminism's Poison Pill |work=[[The Huffington Post]] |date=2013-04-02 |accessdate=2014-12-03 }}</ref>

The film has also been reviewed by [[Ms. (magazine)|''Ms.'' magazine]] blog,<ref name="MsMag"/> [[Psychology Today]] blog,<ref>{{cite news |last=Caplan |first=Paula J. |url=http://www.psychologytoday.com/blog/science-isnt-golden/201309/much-needed-women-and-media-conference-energizes-inspires |title=Much-needed Women and Media Conference Energizes, Inspires |work=Psychology Today Blog |publisher=[[Psychology Today]] |date=2013-12-03 |accessdate=2013-09-14 }}</ref> and by the University of Texas' ''Mercury''.<ref name="MercTex"/>

==References==
{{reflist|2}}

==External links==
* {{Official website|http://www.feministstories.com}}
* {{IMDb title|2455658}}

[[Category:2013 films]]
[[Category:American films]]
[[Category:American documentary films]]
[[Category:Documentary films about feminism]]