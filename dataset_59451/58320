{{Italic title}}
{{Infobox journal
| title = Journal of Modern Applied Statistical Methods
| discipline = [[Statistics]]
| editor = [[Shlomo Sawilowsky]]
| openaccess = Yes
| license =
| website = http://tbf.coe.wayne.edu:16080/jmasm
| publisher = JMASM, Inc.
| country = [[United States]]
| frequency = Biannually
| abbreviation = J. Mod. App. Stat. Meth.
| history = 2002&ndash;present
| impact =
| impact-year =
| ISSN = 1538-9472
| OCLC =
| RSS =
}}
The '''''Journal of Modern Applied Statistical Methods''''' is a biannual [[Peer review|peer-reviewed]] [[open access journal]].<ref>Sawilowsky, S. (2006). Journal of Modern Applied Statistical Methods. In N. Salkind (Ed.), ''Encyclopedia of Measurement and Statistics'', 2:499&ndash;500. Thousand Oaks, CA: [[SAGE Publications]].</ref> It was established in 2002 by [[Shlomo Sawilowsky]], and is currently published by the [[Wayne State University]] Library System in [[Detroit, MI]]. The ''Current Index to Statistics'' classifies it as one of over 160 core statistics journals.<ref>[http://www.statindex.org/news/CIS_core_journals.pdf ''Current Index to Statistics'' Core Journals]</ref> The journal originally appeared as a print and electronic journal through volume 8(1) in 2009, and subsequently appears as an electronic journal. It publishes peer-reviewed work pertaining to new [[statistical hypothesis testing|statistical tests]] and the comparison of existing statistical tests, [[Monte Carlo]]; [[bootstrapping|bootstrap]], [[Resampling (statistics)#Jackknife|Jackknife]], and [[Resampling (statistics)|resampling]] methods; [[non-parametric statistics|nonparametric]], robust, [[permutation]], exact, and approximate [[randomization]] methods; and statistical [[algorithm]]s, [[pseudorandom number generator]]s, and simulation techniques. The journal is indexed in the Elsevier Bibliographic Database, [[EMBASE]], [[Compendex]], Geobase, [[PsycINFO]], [[ScienceDirect]], and [[Scopus]]. It is also listed in the ''Encyclopedia of Measurement and Statistics''<ref>2006, Mahwah, NJ: Sage</ref> and Cabells.<ref>http://www.cabells.com/index.aspx</ref>

== See also ==
*[[Comparison of statistics journals]]

== References ==
<references />

== External links ==
*{{official website|http://www.jmasm.com/}}

{{Statistics journals}}

{{DEFAULTSORT:Journal Of Modern Applied Statistical Methods}}
[[Category:Statistics journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2002]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:2002 establishments in the United States]]