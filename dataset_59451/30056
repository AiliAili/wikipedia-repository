[[File:1976S Eisenhower Obverse.jpg|thumb|200px|The Eisenhower dollar, with the double date 1776–1976]]
[[File:1976 Bicentennial Quarter Rev.png|thumb|200px|Quarter Bicentennial reverse]]
[[File:1976-S 50C Clad Deep Cameo (rev).jpg|thumb|200px|Half dollar Bicentennial reverse]]
[[File:1976S Type1 Eisenhower Reverse.jpg|thumb|200px|Dollar Bicentennial reverse (Type I)]]
[[File:1976D Type2 Eisenhower Reverse.jpg|thumb|200px|Dollar Bicentennial reverse (Type II)]]

The '''United States Bicentennial coinage''' was a set of [[List of circulating currencies|circulating]] commemorative [[coin]]s, consisting of a [[Quarter (United States coin)|quarter]], [[half dollar]] and [[Dollar coin (United States)|dollar]] struck by the [[United States Mint]] in 1975 and 1976.  Regardless of when struck, each coin bears the double date 1776–1976 on the normal [[obverse and reverse|obverses]] for the [[Washington quarter]], [[Kennedy half dollar]] and [[Eisenhower dollar]].  No coins dated 1975 of any of the three denominations were minted.

Given past abuses in the system, the Mint advocated against the issuance of [[United States commemorative coin|commemorative coins]] starting in the 1950s.  Beginning in 1971, members of Congress introduced bills to authorize coins to honor the [[United States Bicentennial]], which would occur in 1976.  The Mint, through its director, [[Mary Brooks]], initially opposed such proposals, but later supported them, and Congress passed legislation requiring the temporary redesign of the [[obverse and reverse|reverse]] of the quarter, half dollar and dollar.

A nationwide competition resulted in designs of a Colonial drummer for the quarter, [[Independence Hall (United States)|Independence Hall]] for the half dollar and the [[Liberty Bell]] superimposed against the moon for the dollar.  All three coins remain common today due to the quantity struck.  Circulation pieces were in copper nickel; Congress also mandated 45,000,000 part-silver pieces be struck for collectors.  The Mint sold over half of the part-silver coins before melting the remainder after withdrawing them from sale in 1986.

== Background ==
[[Commemorative coin]]s had been struck for a number of events and anniversaries by the United States Mint since 1892.{{sfn|Breen|1988|p=582}}  Organizations would get Congress to authorize a coin and would be allowed to buy up the issue, selling it to the public at a premium.  The final issue among [[Early United States commemorative coins|these commemoratives]], half dollars honoring [[Booker T. Washington]] and [[George Washington Carver]] were struck over a number of years, and were discontinued in 1954.  Originally priced at $3.50, they were repeatedly discounted; many could not be sold at a premium and entered circulation.  The promoter of these issues, S.J. Phillips, mishandled the distribution and lost $140,000.  The negative publicity caused the [[United States Department of the Treasury|Department of the Treasury]], of which the Mint is a part, to oppose subsequent commemorative coin proposals, and until the 1970s, Congress passed none.{{sfn|Breen|1988|p=581}}

In 1966, Congress established the American Revolutionary Bicentennial Commission (ARBC) to plan and coordinate activities connected with the 1976 bicentennial of American Independence ("the Bicentennial").  In February 1970, the ARBC established a Coins and Medals Advisory Committee.{{sfn|Breen|1988|p=371}}  The committee's initial report, in July 1970, called for the production of a commemorative half dollar for the Bicentennial.{{sfn|Marotta|2001|p=502}}  In December 1970, the committee called for special designs for all denominations of US coinage for the Bicentennial; the ARBC endorsed this position the following month.  The Treasury, however, opposed the change, following its longstanding position against commemorative coins.  Several proposals for Bicentennial coins were introduced in Congress in 1971 and 1972, but did not pass.{{sfn|Breen|1988|p=371}}

Mint Director [[Mary Brooks]] had attended the Advisory Committee meetings.  At one meeting, she supported having a 1776–1976 double date on circulating coins to mark the anniversary in 1976, although accommodating two dates on the obverse would involve production difficulties.  However, in a newspaper interview she termed the idea of changing the six circulating coins (cent through dollar) "a disaster".  She felt if any Bicentennial coin was issued, it should be non-circulating, perhaps a [[half cent (United States coin)|half cent]] or a gold piece.{{sfn|Breen|1988|p=371}}{{sfn|Marotta|2001|p=502}}  Brooks believed that such a coin would not disrupt the Mint in the production of coins for circulation.{{sfn|Marotta|2001|p=502}}  During 1972, however, she retreated from that position, and by the end of the year had persuaded [[United States Secretary of the Treasury|Treasury Secretary]] [[George Shultz]] to support a Bicentennial coin bill.{{sfn|Breen|1988|p=371}}

== Authorization ==
In January 1973, [[Texas]] Representative [[Richard C. White]] introduced legislation for commemorative dollars and half dollars.  [[Oregon]] Senator [[Mark Hatfield]] also put forward a bill, calling for a $25 [[gold coin|gold piece]].  On March 2, 1973, the Treasury announced its support for Bicentennial coin legislation for design changes to the reverses of the circulating dollars and half dollars, and sent proposed legislation to Congress three days later.{{sfn|''Coin World Almanac''|1977|p=419}}  Hearings before a subcommittee in the House of Representatives were held on May 2, 1973.  Brooks testified, supporting the limited redesign in the bill, but opposing a more extensive coin redesign.  Separately from the Bicentennial matter, she asked for authority to strike US coins at the [[West Point Mint|West Point Bullion Depository]], where space was available to install older coinage presses.  Brooks deprecated the Hatfield proposal, stating that the coin would have to be .667 pure or less to avoid hoarding.{{sfn|''Coin World Almanac''|1977|p=420}}

As a result of the hearings, several additional bills were introduced, and additional hearings were held before a Senate subcommittee on June 6.  Brooks testified again, and responding to criticism that only the two least popular denominations were to be changed, indicated her support for a Bicentennial quarter as well.  On June 13, a bill, S. 1141 which provided for a circulating Bicentennial quarter, half dollar and dollar, gave permission for coins to be struck at West Point and allowed for 40% silver [[clad coinage|clad]] versions of the new coins for collectors was reported favorably by the [[United States Senate Committee on Banking, Housing, and Urban Affairs|Senate Banking Committee]].  It passed the Senate on July 13.  However, amendments authorizing US citizens to own gold, and to implement the Hatfield proposal were attached to the bill.  A similar bill passed the House of Representatives on September 12, differing from the Senate bill in lacking any provision relating to gold, and in not authorizing silver versions of the new coins.{{sfn|''Coin World Almanac''|1977|pp=421–422}}{{efn|It was made legal for US citizens to own gold by legislation enacted August 14, 1974, effective December 31, 1974. {{harvnb|''Coin World Almanac''|1977|pp=55, 127–128}}.}}

Members of the two houses met in a [[United States congressional conference committee|conference committee]] on September 19 in a session described by onlookers as "fairly hot and heavy".{{sfn|''Coin World Almanac''|1977|p=422}}  The resulting bill had no gold provisions, but authorized changes to the reverses of the quarter, half dollar and dollar for the Bicentennial.  The obverses of the three coins would not change, but would bear the double date 1776–1976.  By the terms of the statute, all coins minted to be issued after July 4, 1975<!-- this is very careful language, take care if you change it. --> and before January 1, 1977 would bear the Bicentennial dates and designs.  Congress directed the Mint to strike 45,000,000 silver clad coins (that is, 15,000,000 sets), and the Mint received the requested authority to strike coins at West Point.  Circulation quarters, half dollars and dollars would continue to be of copper nickel bonded to an internal layer of copper, that is, copper nickel clad.  The modified bill passed both houses of Congress on October 4, 1973, and the bill was signed into law by President [[Richard Nixon]] on October 18.{{sfn|''Coin World Almanac''|1977|p=422}}  Hatfield's measure, along with similar legislation from other senators, was reintroduced in 1975, but died in committee, as did legislation seeking a Bicentennial two-cent piece and a bill seeking a coin honoring [[Abigail Adams]] and [[Susan B. Anthony]].{{sfn|''Coin World Almanac''|1977|pp=49–50}}  The extra production at West Point was key to overcoming a shortage of cents in 1974, and permitted the Mint greater flexibility as it geared up to strike the Bicentennial pieces.{{sfn|Ganz|1976|p=42}}

== Competition ==
On October 23, 1973, the Department of the Treasury announced a competition for the three reverse designs.  Any US citizen could submit one drawing, or photograph of a plaster model {{convert|10|in}} in diameter.  Submissions were to include the legend "{{sm|quarter dollar}}". Treasury Secretary Shultz, advised by a panel of judges, would decide which design would be used for which denomination—the inscription would be changed to {{sm|half dollar}} and {{sm|one dollar}} for the higher denominations.  Additionally, the reverses were to bear the name of the country and the motto "{{sm|[[E Pluribus Unum|e pluribus unum]]}}", as required by law.{{sfn|Breen|1988|p=372}}{{sfn|''Coin World Almanac''|1977|p=423}}

At Director Brooks' request, the [[National Sculpture Society]] selected the five judges for the competition.  The judges were society President [[Robert Weinman]] (son of [[Adolph Weinman]], who had designed the [[Mercury dime]] and [[Walking Liberty half dollar]]), Connecticut sculptor Adlai S. Hardin, former Mint Chief Engraver [[Gilroy Roberts]], Julius Lauth of the [[Medallic Art Company]], and Elvira Clain-Stefanelli, curator in the Division of [[Numismatics]], [[Smithsonian Institution]].{{sfn|''Coin World Almanac''|1977|p=423}}

The deadline was originally December 14, 1973, but was extended to January 9, 1974 because of [[1973 oil crisis|the energy crisis]] and Christmas mail delays.  Brooks traveled more than {{convert|7000|mi}} to publicize the competition.  By the deadline, the Mint had received 15,000 inquiries and 884&nbsp;entries.  Members of the panel and any person employed by the US government as a sculptor were ineligible to enter.  The prize for each of the three winners was $5,000.  The judging was originally supposed to take place at West Point; with the delay, it took place instead at the [[Philadelphia Mint]].{{sfn|''Coin World Almanac''|1977|p=423}}{{sfn|Kelley|1974|pp=5–6}}

From the entries, the judges selected twelve semifinalist designs; the sculptor submitting each received a prize of $750.  The competitors were to place their work on plaster models, if that had not already been done, and were offered assistance in making the models.{{sfn|''Coin World Almanac''|1977|pp=423–424}}

== Preparation ==
The twelve remaining designs were released by the Treasury for public comment in early 1974.  Two of the proposed coins featured sailing ships, two featured [[Independence Hall (United States)|Independence Hall]] in Philadelphia, where the [[United States Declaration of Independence|Declaration of Independence]] was signed, and three depicted the moon or lunar spacecraft.  Another depicted the [[Liberty Bell]] superimposed on an atomic symbol.{{sfn|Bardes|1974}}  According to numismatist Michael Marotta in his 2001 article on the Bicentennial coins, "the numismatic community's reaction to the entries was predictable:  everyone complained by writing letters to the editor".{{sfn|Marotta|2001|p=503}}

From the twelve, the judges selected six finalists for review by the National Bicentennial Coin Design Competition Committee, consisting of Brooks, Representative [[Wright Patman]], Senator [[John Sparkman]], [[Commission of Fine Arts]] Secretary Charles H. Atherton and [[Eric P. Newman]], chairman of the ARBC's coins and medals advisory committee.{{sfn|''Coin World Almanac''|1977|pp=423–424}}  After receiving the committee's recommendations, Secretary Shultz selected the winners and on March 6, 1974, Brooks went on the ''[[Today (NBC program)|Today]]'' show to announce them.  Jack L. Ahr's design featuring a colonial drummer, with a torch of victory surrounded by thirteen stars (representing the original states) was selected for the quarter.  Seth Huntington's image of Independence Hall was selected for the half dollar while Dennis R. Williams' superimposition of the Liberty Bell against the moon was successful for the dollar.  Ahr owned a commercial art firm and Huntington was head artist for [[Brown and Bigelow]], a [[Minneapolis]] publishing firm.  Williams, at age 21<ref>D.R.Williams, designer</ref> the youngest person to design a US coin, was an art student who had originally created his design for a class assignment.  No change would be made to the obverses of the coins, except for the double dating.{{sfn|''Coin World Almanac''|1977|p=424}}

[[File:Scott1479.jpg|thumb|This 1973 Bicentennial stamp, like the quarter, depicts a colonial drummer.]]

Ahr was accused of copying his drummer from a 1973 stamp by the stamp's designer, William A. Smith;{{sfn|Gregory|2006|pp=52–53}} he denied it.  According to numismatic historian [[Walter Breen]], "both obviously derive from [[Archibald Willard]]'s 1876 painting ''Spirit of '76'',"{{sfn|Breen|1988|p=372}} a painting which numismatic author [[David L. Ganz]] suggests that both undoubtedly saw sometime in their lives.  Ahr, however, stated that his son had been the model for the drummer.  {{sfn|The Numismatist 2012-07}} Brooks, in a letter to Smith, stated that the design for the quarter was "sufficiently original" to impress the National Sculpture Society.{{sfn|Gregory|2006|pp=52–53}}  Weinman later deprecated the winning designs:

<blockquote>
I really don't think what we got was a great bargain.  Nothing we selected was a real winner that I'd fight to the death for.  In terms of what we had to work with, though, I think we did the best we could.{{sfn|Reiter|1979}}
</blockquote>

On April 24, 1974, the three winning designers were brought to Washington, D.C.  After a tour of the [[White House]] and meetings with the congressional committees which considered the coin bills, they went to the [[Treasury Building (Washington, D.C.)|Treasury Building]] and received their $5,000 checks from the new Treasury Secretary, [[William E. Simon]], who jokingly asked them if they wanted to invest their awards in [[savings bond]]s.{{sfn|''The Young Numismatist''|1974|pp=1–3}}

Mint Chief Engraver [[Frank Gasparro]] made minor changes to all three reverse designs.  Gasparro simplified the quarter design, altered the drum for the sake of authenticity, changed the lettering and modified the expression on the drummer's face.  He made slight changes to Independence Hall on the half dollar and altered the lettering on the dollar to facilitate the metal flow during stamping and asked the designer to straighten the bottom edge of the Liberty Bell.<ref>D.R.Williams, designer§</ref>{{sfn|''Coin World Almanac''|1977|p=424}}  Ahr later stated that he would have liked more time to finalize his design, wishing to clarify the features of the drummer's face.{{sfn|Gregory|2006|pp=52–53}}  The initials of the designer were added to the design by the Mint.{{sfn|Ganz|1976|p=64}}  All three agreed that Gasparro's changes improved their designs.{{sfn|Ganz|1976|p=59}}

== Production ==
[[File:First Bicentennial coins.png|thumb|242px|Mint Director [[Mary Brooks]] presents President [[Gerald Ford]] (center) with the first set of the Bicentennial coins, November 13, 1974 as American Revolutionary Bicentennial Administration Director [[John Warner]] looks on.]]
{{Coin  image box 1 double
| header = Type II [[Eisenhower  dollar]] (1976).
| image =  File:Eisenhower dollar.jpg
| width = 250
| footer = 40% silver version. 
| position = right
| margin = 0
}}

On August 12, 1974, the three designers were at the Philadelphia Mint, where they ceremonially operated the presses to strike the first coins bearing their designs. These prototypes were exhibited under armed guard at the [[American Numismatic Association]] convention in Florida the next day.  They differ from all other Bicentennial coins in that they were struck in silver proof without [[mint mark]]; other silver proof coins bear an "S" mint mark as struck at the San Francisco Assay Office (as the  [[San Francisco Mint]] was then known).  Coins struck at Denver bear a "D" on the obverse; pieces lacking a mint mark were struck at Philadelphia.{{sfn|Yeoman|2011|p=19}} Sets of these prototypes were presented to President [[Gerald Ford]], [[Counselor to the President]] [[Anne Armstrong]] and Director [[John Warner]] of the American Revolutionary Bicentennial Administration (the successor to the ARBC).  All other first strikes were melted, with copies not even kept for the [[National Numismatic Collection]].{{sfn|Breen|1988|p=472}}{{sfn|''Coin World Almanac''|1977|pp=424–425}}

The Mint believed that if it was required to strike 1975 quarters, half dollars and dollars, not enough could be struck before it had to begin the Bicentennial issues to prevent the 1975 pieces from becoming collector's items.  This risked coin shortages at a time when the Mint was seeking to build a surplus of quarters.  Mint officials returned to Congress to seek amending legislation.  President Ford signed a bill on December 26, 1974 that made several noncontroversial changes to law, including provisions to allow the Mint to keep striking 1974-dated pieces until it began striking the Bicentennial coins.  By terms of the amending legislation, the commemorative coins could not be issued until after July 4, 1975.{{sfn|Breen|1988|p=421}}{{sfn|''Coin World Almanac''|1977|p=51}}{{sfn|Ganz|1976|pp=66–68}}

On November 15, 1974, the Mint began taking orders for the silver clad pieces, at a price of $15 for proof sets and $9 for uncirculated, with a deadline for orders of January 31, 1975.{{sfn|''Coin World Almanac''|1977|p=424}}  Uncirculated coins are like those newly released into circulation; proof coins have a mirror finish.{{sfn|''Kiplinger's Personal Finance''|1975|p=5}}  Buyers were initially limited to five sets per person.{{sfn|Reiter|1979}}  On January 19, 1975, Brooks announced that the silver proof set price was cut to $12, and the order limit was waived.{{sfn|Marotta|2001|pp=503, 541}}  Buyers who had paid the higher price were sent refunds by check.{{sfn|Reiter|1979}}  Brooks stated that the price reduction was because of production efficiencies, the benefit of which she wished to pass along to the public.  Numismatic columnist Ed Reiter noted, though, that the reduction came amidst protests from the numismatic community that the price was too high. Coin dealer Herby Skelton suggested in 1977 that the initial high price for the sets followed by the reduction, together with the large mintage of silver sets made the public suspicious and contributed to lagging sales.{{sfn|Reiter|1977}} On August 20, 1975, the price for the uncirculated silver sets was reduced to $7 when bulk purchases of 50 or more were made.{{sfn|''Coin World Almanac''|1977|p=424}}  A bank in Taiwan ordered 250,000&nbsp;sets at this price.{{sfn|''Coin World Almanac''|1977|p=425}}

The first Bicentennial coins to be produced that were intended for the public were dollars, struck during February 1975.{{sfn|''Coin World Almanac''|1977|p=10}}  The first for collectors were struck at San Francisco on April 23, 1975.{{sfn|Marotta|2001|p=503}}  The San Francisco Assay Office struck the 45,000,000 silver coins first, producing eleven million sets in uncirculated and four million in proof, then began the base metal pieces.  Once striking began, the Mint found that the copper nickel dollar was striking indistinctly, a problem not seen with the silver pieces.  The Mint modified the dies; the most noticeable change is that the revised issue, or Type II as it came to be known, have narrower, sharper lettering on the reverse.  All silver pieces (struck only at San Francisco) are Type I; all three mints struck both Type I and Type II copper nickel pieces.  All dollars included in 1975 proof sets are Type I; all those included in 1976 proof sets are Type II.{{sfn|Wexler|Crawford|Flynn|2007|p=8}}{{sfn|Yeoman|2011|pp=174, 207, 228}}  Bicentennial coins for collectors were not delivered until after July 4, 1975.{{sfn|''Kiplinger's Personal Finance''|1975|p=5}} The Bicentennial pieces, in base metal, were included in 1975 proof sets and mint sets together with 1975-dated cents, nickels and dimes.{{sfn|Yeoman|2011|pp=337, 341}}

The new coins first entered circulation on July 7, 1975, when the half dollar was released in conjunction with ceremonies in Minneapolis, Huntington's hometown. The quarter followed in September and the dollar in October, each also with ceremonies to mark the issuance.{{sfn|''Coin World Almanac''|1977|p=425}}  The pieces were struck in numbers exceeding those needed for circulation; a Mint spokesman stated, "The theory in striking them was to have enough available so as many Americans as possible would have an opportunity to have a coinage commemoration of the Bicentennial year.  They're momentos."{{sfn|Reiter|1979}}

In 1977, the Mint returned to the old reverse designs for the quarter, half dollar and dollar.{{sfn|''Coin World Almanac''|1977|p=425}}  Sales by mid-1977 had dropped off considerably, to perhaps 300 sets a week, with one Mint official describing the sales against the massive unsold quantities as "a drop in the bucket".{{sfn|Reiter|1977}} By 1979, the Mint anticipated an eventual sellout for the silver proof set, but admitted that with massive quantities unsold, there was no realistic possibility of selling all uncirculated silver sets.{{sfn|Reiter|1979}}  On September 17, 1979, faced with [[Silver Thursday|a spike in silver prices]], Mint Director [[Stella Hackel Sims|Stella B. Hackel]] announced that the sets were being removed from sale.{{sfn|Marotta|2001|p=541}} They were returned to sale in August 1980, at increased prices of $20 in proof and $15 in uncirculated.{{sfn|AP via ''The Blade''|1980|p=3}}  In September 1981, the Mint, citing a decline in the price of silver, reduced the price of the sets to $15 in proof and $12 in uncirculated.  A limit of 100 sets per person was set on proof sales, with none on uncirculated.{{sfn|Reiter|1981}} A large number of sets were melted by the government in 1982.{{sfn|Yeoman|2011|pp=174, 207, 228}}  [[Ronald Reagan|Reagan]] administration Mint Director [[Donna Pope]] later stated,
"Sales of 1776–1976 regular-issue Bicentennial coins went on and on, seemingly forever."{{sfn|Bowers ''Encyclopedia'', Part 182}} On December 31, 1986, the remaining Bicentennial uncirculated silver sets were removed from sale.  At the time, it was announced that proof sets had already sold out when coins went off sale.{{sfn|Webster|1986}}  However, Marotta, writing in 2001, stated that when sales ceased, 400,000 proof sets and 200,000 uncirculated sets remained in inventory.{{sfn|Marotta|2001|p=542}}

Due to the large quantities struck, Bicentennial coins remain inexpensive.  A set of three silver coins contains .5381 troy ounces of the precious metal.{{sfn|Yeoman|2011|pp=174, 207, 228}}{{sfn|AP via ''The Blade''|1980|p=3}}{{sfn|Reiter|1981}}  In a 1996 statistical study, T.V. Buttrey found that about 750,000,000 of the circulation quarters, more than a third, had been hoarded and did not circulate.{{sfn|Marotta|2001|p=542}}  Coin dealer Marcel Sassola suggested in 1977 of the silver sets, "There were just too many sold, and I think it will take a long time before they have any real value.  Maybe by the Tricentennial."{{sfn|Reiter|1977}}
{{clear}}

The total coinage by striking mint is shown below:

{| class="wikitable plainrowheaders" style="text-align: right;"
! scope="col" | Circulation coins
! scope="col" | Philadelphia{{sfn|Yeoman|2011|p=228}}
! scope="col" | Denver{{sfn|Yeoman|2011|p=228}}
|-
! scope="row" | Quarters
| 809,784,016
| 860,118,839
|-
! scope="row" | Half dollars
| 234,308,000
| 287,565,248
|-
! scope="row" | Dollars (Type I)
| 4,019,000
| 21,048,710
|-
! scope="row" | Dollars (Type II)
| 113,318,000
| 82,179,164
|}

{| class="wikitable plainrowheaders" style="text-align: right;"
! scope="col" | San Francisco (sets)
! scope="col" | Copper nickel{{sfn|Yeoman|2011|pp=228, 337, 341}}
! scope="col" | Silver clad{{sfn|Yeoman|2011|pp=228, 337, 341}}
|-
! scope="row" | In 1975 proof sets (six coins, cent through dollar)
| 2,845,450
| 0
|-
! scope="row" | In 1976 proof sets (six coins, as above)
| 4,149,730
| 0
|-
! scope="row" | Actual number of silver uncirculated sets issued
| 0 ||4,908,319
|-
! scope="row" | Actual number of silver proof sets issued
| 0
| 3,998,621
|}

== References ==
'''Explanatory notes'''

{{notes}}

'''Citations'''

{{reflist|24em}}

'''Bibliography'''

* {{cite book
  | last = Breen
  | first = Walter
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York, N.Y.
  | isbn = 978-0-385-14207-6
  | ref = harv
  }}
* {{cite book
  | title = Coin World Almanac
  | publisher = Amos Press
  | location = Sidney, Ohio
  | year = 1977
  | edition = 3rd
  | asin = B004AB7C9M
  | ref = {{sfnRef|''Coin World Almanac''|1977}}
  }}
* {{cite book
  | last = Ganz
  | first = David L.
  | authorlink = David L. Ganz
  | year = 1976
  | title = 14 Bits: The Story of America's Bicentennial Coinage
  | publisher = Three Continents Press
  | location = Washington, DC
  | isbn = 978-0-914478-63-8
  | ref = harv
  }}
* {{cite book
  | last1 = Wexler
  | first1 = John
  | last2 = Crawford
  | first2 = Bill
  | last3 = Flynn
  | first3 = Kevin
  | year = 2007
  | title = The Authoritative Reference on Eisenhower Dollars
  | edition = 2nd
  | publisher = Kyle Vick
  | location = Roswell, Ga.
  | isbn = 978-0-9679655-9-8
  | ref = harv
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | year = 2011
  | title = A Guide Book of United States Coins 2012
  | edition = 65th
  | publisher = Whitman Publishing, LLC
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-3349-7
  | ref = harv
  }}

'''Other sources'''
* {{cite news
  | last = Bardes
  | first = Herbert C.
  | date = February 3, 1974
  | title = From these will come our '76 coins
  | newspaper = The New York Times
  | page = AL34
  | url = http://select.nytimes.com/gst/abstract.html?res=F10D1FF73A5A1B778DDDAA0894DA405B848BF1D3&scp=25&sq=bicentennial+coin&st=p
  | accessdate = August 4, 2011
  | ref = harv
  }} {{subscription required}}

* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 8: Silver commemoratives (and clad too), Part 182
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter08-182.aspx
  | accessdate = December 23, 2012
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 182}}
  }}
* {{cite magazine
  | last = Gregory
  | first = Barbara J.
  | date = May 2006
  | title = Drummer boy debate
  |magazine=[[Numismatist (magazine)|The Numismatist]]
  | issn = 0029-6090
  | pages = 52–53
  | ref = harv
  }}
* {{cite journal
  | last = Kelley
  | first = Paul
  | date = Spring–Summer 1974
  | title = Bicentennial coin headway
  | journal = The Young Numismatist
  | pages = 5–6
  | ref = {{sfnRef|Kelley|1974}}
  }}
* {{cite magazine
  | last = Marotta
  | first = Michael E.
  | date = May 2001
  | title = The Bicentennial coinage of 1976
  |magazine=[[Numismatist (magazine)|The Numismatist]]
  | issn = 0029-6090
  | pages = 501–503, 541–542
  | ref = harv
  }}
* {{cite journal
  | last = Reiter
  | first = Ed
  | date = August 1977
  | title = Strange fate for the Bicentennials
  | journal = CoinAge
  | pages = 86–88
  | ref = harv
  }}
* {{cite news
  | last = Reiter
  | first = Ed
  | date = July 8, 1979
  | title = Bicentennial hangover
  | newspaper = The New York Times
  | page = D38
  | url = http://select.nytimes.com/gst/abstract.html?res=F50C13FC3C5C11728DDDA10894DF405B898BF1D3&scp=5&sq=bicentennial+coin&st=p
  | accessdate = March 12, 2012
  | ref = harv
  }} {{subscription required}}
* {{cite news
  | last = Reiter
  | first = Ed
  | date = September 13, 1981
  | title = Is this really the end of the Anthony dollar?
  | newspaper = The New York Times
  | page = 52, section 2
  | url = https://www.nytimes.com/1981/09/13/arts/numismatics-is-this-really-the-end-of-the-anthony-dollar.html?scp=6&sq=bicentennial%20coin&st=nyt&pagewanted=2
  | accessdate = August 4, 2011
  | ref = harv
  }} {{subscription required}}
* {{cite news
  | last = Webster
  | first = Daniel
  | date = October 26, 1986
  | title = Gold eagle coming soon
  | agency = Knight News Service
  | newspaper = The Blade
  | page = F-8
  | location = Toledo, Ohio
  | url = https://news.google.com/newspapers?id=mQ1PAAAAIBAJ&sjid=2gIEAAAAIBAJ&dq=bicentennial%20coins%20off%20sale&pg=6972%2C5784426
  | accessdate = August 4, 2011
  | ref = harv
  }}
* {{cite news
  | newspaper = The Blade
  | agency = AP
  | date = August 1, 1980
  | title = Resumed coin sales announced
  | page = 3
  | location = Toledo, Ohio
  | url = https://news.google.com/newspapers?id=hD1PAAAAIBAJ&sjid=lAIEAAAAIBAJ&dq=bicentennial%20coin&pg=1206%2C4473997
  | accessdate = August 4, 2011
  | ref = {{sfnRef|AP via ''The Blade''|1980}}
  }}
* {{cite magazine
  | title = Your questions answered
  | date = April 1975
  |magazine= [[Kiplinger's Personal Finance]]
  | issn = 1056-697X
  | page =5
  | accessdate = August 5, 2011
  | url = https://books.google.com/books?id=6AMEAAAAMBAJ&lpg=PA6&dq=bicentennial%20coin&pg=PA6#v=onepage&q=bicentennial%20coin&f=false
  | ref = {{sfnRef|''Kiplinger's Personal Finance''|1975}}
  | author1 = Kiplinger Washington Editors
  | first1 = Inc
  }}
*{{cite journal
| title = Drummer boy debate
| journal = The Numismatist
| volume = 125
| issue = 7
| page = 27
| date = July 2012
 | ref = {{sfnRef|The Numismatist 2012-07}}
}}
* {{cite journal
  | journal = The Young Numismatist
  | date = Spring–Summer 1974
  | title = Bicentennial coin designers meet ANA officials at the White House
  | pages =1–3
  | ref = {{sfnRef|''The Young Numismatist''|1974}}
  }}

{{Coinage (United States)}}
{{US currency and coinage}}
{{Portal bar|1970s|Numismatics|United States}}

{{featured article}}

{{DEFAULTSORT:Bicentennial Coinage}}
[[Category:1975 introductions]]
[[Category:1976 in the United States]]
[[Category:Coins of the United States]]
[[Category:United States Bicentennial|Coinage]]