{{Expert-subject|Computer science|date=April 2009}}

'''Strong Key''' is a naming convention used in [[computer programming]]. There can be more than one component (e.g.: DLL) with the same naming, but with different versions. This can lead to many conflicts.

A Strong Key (also called '''SN Key''' or '''Strong Name''') is used in the [[Microsoft]] [[.NET framework]] to uniquely identify a component. This is done partly with [[Public-key cryptography]].

Strong keys or names provide security of reference from one component to another or from a root key to a component. This is not the same as [[tamper resistance]] of the file containing any given component.<ref>[http://www.codeproject.com/KB/security/NeCoder03.aspx CodeProject: Building Security Awareness in .NET Assemblies : Part 3 - Learn to break Strong Name .NET Assemblies.]</ref>  Strong names also are a countermeasure against [[dll hell]].

This key is produced by another computer program as a pair.

==References==
{{reflist}}

==External links==
*[http://www.codeproject.com/KB/security/StrongNameExplained.aspx CodeProject: Strong Names Explained]
*[http://msdn.microsoft.com/en-us/library/wd40t7ad.aspx MSDN: Strong-Named Assemblies]

{{DEFAULTSORT:Strong Key}}
[[Category:Programming constructs]]
[[Category:.NET Framework]]


{{compu-prog-stub}}