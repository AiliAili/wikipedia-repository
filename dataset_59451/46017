Active [[Motion compensator|heave compensation]] (AHC) is a technique used on lifting equipment to reduce the influence of [[Wind wave|waves]] upon offshore operations. AHC differs from [[Passive heave compensation|Passive Heave Compensation]] by having a [[control system]] that actively tries to compensate for any movement at a specific point, using power to gain accuracy.

==Principles==
The purpose of AHC is to keep a load, held by equipment on a moving vessel, motionless with regard to the seabed or another [[Marine vessel|vessel]]. Commercial offshore cranes usually use a [[Attitude_control#Motion_reference_units|Motion reference unit (MRU)]] or pre-set measurement [[position detection]] to detect the current ship displacements and rotations in all directions.<ref>{{cite web|title=Adaptive Active Heave Compensation (AHC) Technology|url=http://www.scantrol.no/scantrol-ahc-4|publisher=www.scantrol.no|accessdate=26 May 2014}}</ref> A control system, often [[Programmable logic controller|PLC]] or computer based, then calculates how the active parts of the system are to react to the movement.<ref>{{cite web|title=Active heave compensation comes of age subsea|url=http://www.oilonline.com/default.asp?id=259&nid=37173&name=Active+heave+compensation+comes+of+age+subsea|newspaper=www.oilonline.com}}</ref> The performance of an AHC system is normally limited by power, motor speed and torque, by measurement accuracy and delay, or by computing algortithms. Choice of control method, like using preset values or delayed signals, may affect performance and give large residual motions, especially with unusual waves.

State of the art AHC systems are real time systems that can calculate and compensate any displacement in a matter of milliseconds. Accuracy then depends on the forces on the system, and thus the shape of the waves more than the size of the waves.

==Application==

===Electric winch systems===
In an [[electric]] winch system, the wave movement is compensated by automatically driving the winch in the opposite direction at the same speed. The hook of the winch will thus keep its position relative to the [[seabed]]. AHC winches are used in [[Remotely operated underwater vehicle|ROV]]-systems and for lifting equipment that is to operate near or at the seabed.<ref>{{cite web|title=MacGREGOR’s ROV-handling technology expands operational weather-windows|url=http://www.siwertell.com/?id=9611|newspaper=www.siwertell.com}}</ref> Active compensation can include tension control, aiming to keep wire tension at a certain level while operating in waves. Guide-wires, used to guide a load to an underwater position, may use AHC and tension control in combination.

===Hydraulic winch systems===
[[Hydraulic]] [[Crane (machine)|cranes]] can use [[hydraulic cylinders]] to compensate, or they can utilize a hydraulic [[winch]].
<ref>{{cite web|title=Active Heave Compensation Winches|url=http://www.macartney.com/systems/launch-and-recovery/winches/active-heave-compensation-winches|newspaper=www.macartney.com}}</ref><ref>{{cite web|title=Research on an Active Heave Compensation System for Remotely Operated Vehicle|url=http://www.researchgate.net/publication/232624276_Research_on_an_Active_Heave_Compensation_System_for_Remotely_Operated_Vehicle
|newspaper=www.researchgate.net}}</ref>
Hydraulic "active boost" winches control the [[Fluid dynamics|oil flow]] from the [[pump]](s) to the winch so that the target position is reached. Hydraulic winch systems can use [[hydraulic accumulator|accumulators]] and [[passive heave compensation]] to form a semi-active system with both an active and a passive component. In such systems the active part will take over when the passive system is too slow or inaccurate to meet the target of the AHC control system. AHC cranes need to calculate the vertical displacement and/or the velocity of the crane tip position in order to actively heave compensate a load sub-sea.

A good AHC-crane is able to keep its load steady with a [[Absolute deviation|deviation]] of a few centimeters in waves up to 8m (+/-4m).

AHC cranes are typically used for [[Subsea#Oil and gas|sub-sea]] lifting operations or [[offshore construction|construction]], and special rules applies to certified heave compensating equipment.<ref>"Standard for Certification of lifting Appliances, June 2013" ''[[DNV]] Lifting Appliances''</ref>

===Cylinder compensation===
The earliest heave compensated systems came in the late 60's and were cylinder compensated systems
.<ref>{{cite web|title=Simulation of fuzzy PID Control of heave compensation system for deep-ocean mining| url=http://www.worldacademicunion.com/journal/1746-7233WJMS/wjmsvol08no01paper05.pdf|newspaper=www.worldacademicunion.com}}</ref> Such systems use [[hydraulic cylinders]] to compensate. Active heave systems pump oil into the cylinders to make them extend, or drain the oil to make them retract. Such systems may use control mechanisms based on oil-pressure rather than ship position, and can combine active and passive compensation. AHC Manipulator arms use cylinder compensation which always limits the actual working area.

US navy have used AHC <ref>{{cite web|title=Multi-Platform Active Heave Compensation System|url=http://www.navysbir.com/n09_3/N093-215.htm|newspaper=www.navysbir.com}}</ref> to create a Roll-On/Roll-Off ([[RoRo|Ro/Ro]]) system for two [[Marine vessel|vessels]] or floating platforms at sea.<ref>{{cite web|title=Active Motion-Compensation Technology for Roll-On/Roll-Off Cargo Vessel Discharge to Floating Platforms|url=http://www.navysbir.com/11_2/204.htm|newspaper=www.navysbir.com}}</ref> The system is utilizing AHC via hydraulic cylinders. This system is, according to some, currently not commercially interesting due to costs, limited use and huge amount of power.

The latest development is to compensate not only the vertical direction but also the horizontal directions,
<ref>{{cite web|title=MacGregor wins offshore industry award|url=http://www.khl.com/magazines/international-cranes-and-specialized-transport/detail/item93543/MacGregor-wins-offshore-industry-award|newspaper=www.khl.com}}</ref> making it possible to do operations on offshore windmills.<ref>{{cite web|title=Utviklet verdens mest stabile kran i Kristiansand|url=http://www.tu.no/industri/2014/04/03/utviklet-verdens-mest-stabile-kran-i-kristiansand|newspaper=www.tu.no}}</ref>

==References==
{{reflist}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Lifting equipment]]