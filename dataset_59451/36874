The name "Ofstad" or "af Awaldzstadom"  comes from '''Augvald''' ([[Old Norse]]: ''Ogvaldr'') was a semi-legendary [[Norway|Norwegian]] [[Petty kingdoms of Norway|petty king]] portrayed in the [[Legendary saga|legendary]] [[Saga|Norse sagas]]. If considered historical, reconstructed estimates based on saga information would have Augvald living some time in the 7th century AD. His kingdom was said to have been based in Jøsursheid, somewhere in the interior of south-western Norway. After a number of naval battles he succeeded in conquering the islands off the western coast of [[Rogaland]]. He subsequently moved his kingdom's seat to the north-east of [[Karmøy]], the largest of those islands and adjacent to the strategically important [[Karmsund]] strait, to a site later given the name [[Avaldsnes]], after the king. Augvald's kingdom further expanded to incorporate parts of what is today south-western [[Hordaland]].

Augvald had several daughters, including two who notably fought alongside him as so-called shield-maidens, or female warriors. He owned and worshipped a sacred cow, which he always kept with him, believing he owed his victories to the cow and the power of its milk. Augvald was killed during a battle with his rival [[Ferking]], the native king of western Karmøy, with whom his story is interlinked.

==Sources==
Norse sagas telling parts of the story of Augvald include the ''[[Óláfs saga Tryggvasonar (disambiguation)|Saga of Olaf Tryggvason]]'' (both by [[Snorri Sturluson]] in [[Heimskringla]] and by [[Oddr Snorrason]]), the ''[[Hálfs saga ok Hálfsrekka|Saga of Half & His Heroes]]'' and the ''[[Flateyjarbok]]''. He also appears in the later ''Historia rerum Norvegicarum'' and other works of the [[Iceland]]ic historian [[Thormodus Torfæus]], who lived and worked in [[Kopervik]], Karmøy.<ref name="Vea1">{{cite web|url=http://en.vikingkings.com/PortalDefault.aspx?portalID=116&activeTabID=868&parentActiveTabID=867|title=King Augvald and King Ferking|archive-date= August 2011|archiveurl=https://web.archive.org/web/20110815164857/http://en.vikingkings.com/PortalDefault.aspx?portalID=116&activeTabID=868&parentActiveTabID=867}}</ref> In addition, Augvald is mentioned in the Avaldsnes parish register, and in local Karmøy legends.<ref name="Vea2">{{cite web|url=http://en.vikingkings.com/PortalDefault.aspx?portalID=116&activeTabID=869&parentActiveTabID=867|title=The Legendary King Augvald and His Descendants|archive-date= August 2011|archiveurl=https://web.archive.org/web/20110815164920/http://en.vikingkings.com/PortalDefault.aspx?portalID=116&activeTabID=869&parentActiveTabID=867}}</ref> Scholars have been very sceptical about the reliability of the [[legendary saga]]s, which were dismissed as being of little value even in the early 20th century, before the establishment of general [[source criticism]]. The legendary sagas' value as sources has not been re-evaluated in recent times.<ref name="Opedal167">Opedal (1998) p. 167</ref>

==Name==
Augvald's name (originally ''Ogvaldr'') has been interpreted in several different ways, although usually taken to mean "he who is held in awe," derived from the West-Nordic ''agi'' meaning holy unrest and tremor. Other interpretations include "the ruler of the coast", with ''ogð'' meaning stretch of coastline, or "the ruler of the island", with ''ogn'' meaning dangerous waters/island, which would imply that the king's name might originally have been either Ogðvaldr or Ognvaldr.<ref name="Vea1"/><ref name="Vea2"/> It has also been suggested that Augvald was probably an [[epithet]] gained in adulthood rather than a given name,<ref>Hernæs (1997) p. 154</ref> and there has been speculation that Augvald was actually [[Harald Agdekonge]].<ref name="Opedal167"/><ref>Hernæs (1997) p. 155</ref> Some historians have alternatively suggested that Augvald could have been a title, possibly shared by many, rather than the name of a single individual, but the saga's account of Augvald may still have centred on one particularly notable individual.<ref name="Vea1"/><ref name="Vea2"/>

==Background==
[[File:Augvald locations.png|thumb|290px|Augvald's possible kingdom (in red) largely coincides with sites connected to the 8th-century kingdom of the monarch from the [[Storhaug (burial mound)|Storhaug]] burial mound, identified through archaeological excavations. This lends credibility to saga accounts of sites integral to a contemporary kingdom.<ref name="Vea2"/>]]
The sagas do not say when Augvald lived, but an early suggestion by Torfæus placed Augvald in the 3rd century AD.<ref name="Vea2"/> Modern estimates have been made based on two of his reported descendants, Geirmund and Håvard Heljarskinn, who are said to have [[Settlement of Iceland|settled Iceland]] as "old men" when [[Harald Fairhair]] consolidated his power in Norway. According to an estimate by Marit Synnøve Vea based on generation-cycles of 30 years (though she notes 25 years might be more accurate),<ref name="Vea2"/> Augvald would have lived in the [[Migration Period]], with slightly varied interpretations around 580–630,<ref>Hernæs (1997) pp. 153–154</ref> around 600<ref name="Vea2"/> or in the early 7th century.<ref name="Opedal169">Opedal (1998) p. 169; citing Vea</ref> Arnfrid Opedal has considered a shorter generation-cycle of 20 years, which would place Augvald 90 years later, at the end of the 7th century.<ref name="Opedal169"/>

To justify their right to rule, Norse royal families, among other things, traced their bloodlines back to divine creatures. As a member of the West-Nordic royal families, Augvald traced his ancestry back to the ancient giant [[Fornjót]] (likely another name for [[Ymir]]).<ref name="Vea2"/> According to the sagas, Augvald was the son of Rognvald, in turn the son of Rugalf, son of [[Gard Agdi]], son of [[Nór]]—a direct descendant of Fornjót.<ref name="Vea1"/><ref name="Opedal88">Opedal (1998) p. 88</ref> Augvald originally had his throne at "Roga" in "Jøsursheid". Historian [[P. A. Munch]] located the name Jøsureid in [[Kviteseid]] in western Telemark, and thus placed Augvald's kingdom in the mountains between Rogaland and [[Telemark]], concluding that the kingdom had stretched to western Telemark. Per Hernæs has on the other hand identified Jøsursheid as an old name for the moorland within [[Jøsenfjorden]] in [[Hjelmeland]], although he questions whether conditions in the area could have allowed for the rise of a great chieftain such as Augvald.<ref>Hernæs (1997) p. 147</ref>

Described as battle-hungry, Augvald carried out raids into foreign territory, gaining great wealth and honour as a result. After a series of successful naval battles, he went on to conquer the land of the ''[[Rugii|Holmrygr]]'' ("island-Rugi") people, based on the islands off the western coast of [[Rogaland]]. He banished the former chieftains from the newly conquered land, and set up his new base at the most favourable location on Karmøy, the largest island in Rogaland. The site he chose was later named Avaldsnes, after Augvald.<ref name="Vea1"/> Based on archaeological findings, it is believed that the ambitions of the increasingly powerful Norwegian chieftains of the time were influenced by the [[Merovingian dynasty|Merovingian]] [[Franks]], and especially [[Dagobert I|Dagobert]] and his empire.<ref>Hernæs (1997) p. 171</ref> As a builder of dynasties, Augvald has also been compared to [[Clovis I|Clovis]].<ref>Hernæs (1997) p. 153</ref>

Convinced that his cow was responsible for his victories, Augvald worshipped it and always kept it by his side. He also believed that the cow's milk provided special strength and vitality. According to some accounts, the cow's name was Audhumla (after [[Auðumbla]]), and it wore a golden collar around its neck.<ref name="Vea1"/>

Augvald had several daughters, including two who were female warriors, known as shield-maidens ({{lang-no|skjøldmøyer}}), and who fought alongside their father in all his battles. Augvald's (unnamed) queen also gave birth to a son, Jøsur, when Augvald was away hunting with his men.<ref name="Vea1"/> Jøsur was raised by Augvald's earl Gunnvald on [[Stord (island)|Stord]].<ref name="H148">Hernæs (1997) p. 148</ref>

==Rivalry with Ferking and death==
Augvald had set up his base in the north-eastern part of Karmøy, but the western part of the island was still ruled by another king, [[Ferking]]. The two were not enemies at first, but that changed after Augvald and his men attended a midwinter sacrificial banquet in [[Ferkingstad]]. What started as a friendly visit turned sour, and Augvald returned home with his men, leaving his daughters held captive at Ferkingstad.<ref name="Vea1"/>

Ferking apparently could not tolerate Augvald's overlordship of Karmøy,<ref>Hernæs (1997) p. 157</ref> and he went north with his army, meeting Augvald and his army at Skeie, near Avaldsnes. In the resulting battle there many fatalities, but no clear victor. The conclusive battle took place at the Field of Stava (''Stavasletta''), near Ferkingstad. Ferking and his men hid between two gorges, waiting for Augvald to appear. In the fierce battle that followed Augvald and his cow were among the many who lost their lives. When his two daughters saw that Augvald was dead, they jumped into a river and drowned.<ref name="Vea1"/><ref>Hernæs (1997) pp. 157–158</ref>

According to Snorri Sturluson, Augvald was killed by a man named Varin, although the ''Flateyjarbok'' says it was someone called Dixin. The ''Saga of Half & His Heroes'' names Augvald's killer as "Hækling's men", which possibly only refers to caped men.<ref name="Vea2"/>

==Aftermath==
[[File:Gravhauge1.jpg|thumb|One of many burial mounds in Karmøy]]
According to historical sources, Augvald and his cow were taken from the battlefield and buried at Avaldsnes. In his ''Saga of Olav Tryggvason'', Oddr Snorrason writes that [[Olaf I of Norway|Tryggvason]] excavated two mounds on Karmøy, revealing the bones of a man in one and those of a cow in the other. Local legends contradict Augvald's burial at Avaldsnes, stating instead that he was buried in Ferkingstad, on the south side of the 12th-century churchyard. A large memorial stone, still standing, was raised outside the churchyard in memory of Augvald.<ref name="Vea1"/>

In the generations following Augvald's death two royal dynasties appear to fight over the same region, the "Vikar dynasty" of [[Agder]] and Rogaland, and the "Jøsur dynasty" of Hordaland and Rogaland. If Augvald was indeed Harald Agdekonge, as suggested by some modern historians, Vikar and Jøsur could have been two warring brothers.<ref>Hernæs (1997) pp. 154–155</ref> In any event, Augvald's son Jøsur became king of Rogaland after his father's death, and reconquered parts of Hordaland.<ref name="H148"/> He was in turn succeeded by his son Hjør, who was buried in Rogaland. Hjør's son, Hjørleiv the Womanizer, was king of Hordaland and Rogaland, and mounted expeditions to [[Bjarmaland]] and Denmark. He eventually also captured a kingdom in [[Zealand]], in Denmark.<ref name="Vea2"/><ref>Hernæs (1997) pp. 151–153</ref> Hjørleiv's son Half appears as the protagonist in the ''Saga of Half & His Heroes''.<ref name="Vea2"/>

==Family tree==
The following tables show the most common rendering of the family tree attributed to Augvald in the various sagas, including his ancestors and descendants.<ref name="Vea2"/><ref name="Opedal88"/>

{| class="wikitable" style="font-size:95%; width:60%; border:1px solid #999; background:#fff; margin: 1em auto 1em auto"
!Patrilineal descent!!Descendants
|- valign="top"
|
# [[Fornjót]]
# [[Fornjót#Kári|Kári]]
# [[Fornjót#Kári|Frosti]]
# [[Snær]]
# Thorri
# [[Nór]]
# [[Gard Agdi]]
# Rugalf
# Rognvald
# '''Augvald'''
| width="75%" |{{familytree/start|style=font-size100%;line-height:100%;}}
{{familytree| | AUG |AUG='''Augvald'''|}}
{{familytree| | |!|}}
{{familytree| | JØS |JØS=Jøsur|}}
{{familytree| | |!|}}
{{familytree| | HJØ |HJØ=Hjør|}}
{{familytree| | |!|}}
{{familytree| | HJL|~|y|HIL |HJL=Hjørleiv|HIL=Hild<sup>1</sup>|}}
{{familytree| | | | | |!|}}
{{familytree| | HAL|-|^|HJO|HAL=Half|HJO=Hjørolv|}}
{{familytree| | |!|}}
{{familytree| | HJH|~|~|y|DAG|HJH=Hjør Halfson|DAG=Dagny<sup>2</sup>|}}
{{familytree| | | | | | |!|}}
{{familytree| | | DAU|y|FLE|FLE=Flein Hjørson|DAU=NN<sup>3</sup>|}}
{{familytree| | | | | |!|}}
{{familytree| | LJU|y|HJF|HJF=Hjør Fleinson|LJU=Ljufvina<sup>4</sup>|}}
{{familytree| | |,|-|^|-|.|}}
{{familytree| | GEI |GEI=Geirmund<br>Heljarskinn | |HÅV |HÅV=Håvard<br>Heljarskinn|}}
{{familytree/end}}
<sup>1</sup> Hild, daughter of Høgne of Njardø<br>
<sup>2</sup> Dagny, daughter of Hake Håmundson<br>
<sup>3</sup> Unnamed daughter of Danish king Eystein<br>
<sup>4</sup> Ljufvina, princess from Bjarmaland
|}

==References==
https://archive.org/stream/gamlepersonnavne00rygh/gamlepersonnavne00rygh_djvu.txt
{{reflist|25em}}

==Bibliography==
* {{Cite book|author=Hernæs, Per|year=1997|title=Karmøys historie – som det stiger frem. Fra istid til 1050|publisher=Karmøy kommune|isbn=978-82-7859-003-4}}
* {{Cite book|author=Opedal, Arnfrid|year=1998|title=De glemte skipsgravene: Makt og myter pa Avaldsnes|publisher=Arkeologisk museum i Stavanger|isbn=978-82-7760-043-7}}

==Further reading==
===Historical fiction===
* {{Cite book|author=Utvik, Aadne|year=1995|title= Augvalds saga og Avaldsnes|publisher=Worums Forlag|location=Haugesund|isbn=8299367018}}

{{Good article}}

{{DEFAULTSORT:Augvald}}
[[Category:Norwegian petty kings]]
[[Category:Mythological kings]]
[[Category:People in Norse mythology and legends]]
[[Category:Monarchs killed in action]]