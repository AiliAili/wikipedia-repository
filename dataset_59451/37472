{{Infobox military conflict
| conflict    = Battle of Tigranocerta
| partof      = the [[Third Mithridatic War]]
| image       = [[File:Hpa-tigranakertbattle69.gif|300px]]
| caption     = 
| date        = October 6, 69 BC
| place       = [[Tigranocerta]], [[Kingdom of Armenia (antiquity)|Armenia]]
| territory   = 
| result      = Roman victory
| status      = 
| combatants_header = 
| combatant1  = {{flagicon image|Roman Military banner.svg}} [[Roman Republic]]
| combatant2  = {{flagicon image|Artaxiad standard2.svg}} [[Kingdom of Armenia (antiquity)|Armenia]]
| commander1  = [[Lucullus]] <br/> [[Legatus]] Fannius <br> Legatus Sextilius <br/> Legatus Hadrianus
| commander2  = [[Tigranes the Great]] <br/> Taxilés <br/> Mancaeus
| strength1   = 40,000 men consisting of: <br/> 24,000 infantry <br/> 3,300 Roman and 10,000 [[Roman Gaul|Gallic]] and [[Thracians|Thracian]] cavalry <br/> [[Bithynia]]n infantry <ref name="SovArm">{{hy icon}} Manaseryan, Ruben. ''«Տիգրանակերտի ճակատամարտ Մ.Թ.Ա. 69»'' (Battle of Tigranakert, 69 BC). [[Armenian Soviet Encyclopedia]]. vol. xi. Yerevan: [[Armenian Academy of Sciences]], 1985, p. 700.</ref>
| strength2   = 80,000–100,000 men consisting of: <br/> Adiabenians, Corduenians, [[Caucasian Iberians|Iberians]], [[Medes|Medians]] <br/> 20,000–25,000 Armenians <ref name="SovArm"/>
| casualties1 = Unknown, estimated light
| casualties2 = Unknown, estimates given from 10,000 to 100,000<ref>{{cite book|authorlink=A. N. Sherwin-White|author=Sherwin-White, Adrian N.|chapter=Lucullus, Pompey, and the East|title=The Cambridge Ancient History Volume 9: The Last Age of the Roman Republic, 146-43 BC|editor1=J. A. Crook |editor2=[[Andrew Lintott]] |editor3=[[Elizabeth Rawson]] |location=Cambridge|publisher=Cambridge University Press|year=1994|isbn=0-521-25603-8|page=241}}</ref> 
| notes       = 
| campaignbox = 
}}
{{Campaignbox Mithridatic Wars}}
{{Campaignbox Third Mithridatic War}}

The '''Battle of Tigranocerta''' ({{Lang-hy|Տիգրանակերտի ճակատամարտը}}, ''Tigranakerti tchakatamartə'') was fought on October 6, 69 BC between the forces of the [[Roman Republic]] and the army of the [[Kingdom of Armenia (antiquity)|Kingdom of Armenia]] led by King [[Tigranes the Great]]. The Roman force was led by [[Consul]] [[Lucullus|Lucius Licinius Lucullus]], and Tigranes was defeated. His capital city of [[Tigranocerta]] was lost to Rome as a result.<ref name="SovArm"/>

The battle arose from the [[Third Mithridatic War]] being fought between Roman Republic and [[Mithridates VI of Pontus]], whose daughter [[Cleopatra of Pontus|Cleopatra]] was married to Tigranes. Mithridates fled to seek shelter with his son-in-law, and Rome invaded the Kingdom of Armenia. Having laid siege to Tigranocerta, the [[Roman Republic|Roman]] forces fell back behind a nearby river when the large Armenian army approached. Feigning retreat, the Romans crossed at a ford and fell on the right flank of the Armenian army. After the Romans defeated the Armenian [[cataphract]]s, the balance of Tigranes' army, which was mostly made up of raw levies and peasant troops from his extensive empire, panicked and fled, and the Romans remained in charge of the field.<ref name="SovArm"/>

==Background==
{{main|Tigranes the Great}} 
Tigranes' expansion into the [[Near East]] led to the creation of an Armenian empire that stretched almost across the entire region. With his father-in-law and ally securing the empire's western flank, Tigranes was able to conquer territories in [[Parthian Empire|Parthia]] and [[Mesopotamia]] and annex the lands of the [[Levant]]. In Syria, he began the construction of the city of Tigranocerta (also written Tigranakert), which he named after himself, and imported a multitude of peoples, including [[Arabs]], [[Greeks]], and [[Jews]], to populate it. The city soon became the king's headquarters in Syria and flourished as a great center for [[Hellenistic period|Hellenistic]] culture, complete with theaters, parks and hunting grounds.<ref>{{cite book|authorlink=George Bournoutian|author=Bournoutian, George A.|title=A Concise History of the Armenian People|location= Costa Mesa, CA|publisher=Mazda Publishers|year=2006|pages=31–32|isbn=1-56859-141-1}}</ref>

This period of Armenian hegemony in the region, however, was coming close to an end with a series of [[Roman Republic|Roman]] victories in the [[Mithridatic Wars|Roman–Mithridatic Wars]]. Friction between the two had existed for several decades, although it was during the Third Mithridatic War that the Roman armies under [[Lucullus]] made significant progress against Mithridates, forcing him to take refuge with Tigranes. Lucullus sent an ambassador named Appius Claudius to [[Antioch]] to demand that Tigranes surrender his father-in-law; should he refuse, Armenia would face war with Rome.<ref name="ShWh">Sherwin-White. "Lucullus", p. 239.</ref> Tigranes refused Appius Claudius' demands, stating that he would prepare for war against the Republic.

Lucullus was astonished upon hearing this in the year 70, and he began to prepare for an immediate invasion of Armenia.<ref name="ShWh"/> Although he had no mandate from the [[Roman Senate|Senate]] to authorize such a move, he attempted to justify his invasion by distinguishing as his enemy king Tigranes and not his subjects. In the summer of 69, he marched his troops across [[Cappodocia]] and the [[Euphrates]] river and entered the Armenian province of [[Sophene|Tsop'k']], where Tigranocerta was located.

==The siege of Tigranocerta==
[[File:Armenian Empire.png|thumb|305px|[[Tigranes the Great]]'s empire circa 80 BC.]]
Tigranes, who was residing at Tigranocerta in the summer of 69, was not only astonished by the speed of Lucullus' rapid advance into Armenia but by the fact that he had even launched such an operation in the first place. Unable to reconcile with this reality for a certain period of time, he belatedly sent a general named Mithrobarzanes with 2,000 to 3,000 cavalrymen to slow down Lucullus' advance but his forces were cut to pieces and [[rout]]ed by the 1,600 cavalry led by Sextilius, one of the [[Legatus|legates]] serving under Lucullus. Learning of Mithrobarzanes' defeat, Tigranes entrusted the defense of his namesake city to Mancaeus and left to recruit a fighting force in the [[Taurus Mountains]].<ref>[[Plutarch]]. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 25.5].</ref> Nevertheless, Lucullus' legates were able to disrupt two separate detachments coming to the aid of Tigranes and even located and engaged the king's forces in a canyon in the Taurus. Lucullus, nevertheless, chose not to pursue Tigranes while he had an unimpeded path towards Tigranocerta; he advanced and began to lay siege to it.<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 26.1].</ref>

Tigranocerta was still an unfinished city when Lucullus laid siege to it in the late summer of 69. The city was heavily fortified and  according to the Greek historian [[Appian]], had thick and towering walls that stood 25 meters high, providing a formidable defense against a prolonged siege.<ref>[[Appian]]. ''The Mithrdatic Wars'', [http://www.livius.org/ap-ark/appian/appian_mithridatic_17.html#%A784 12.84].</ref> The [[Roman siege engines]] that were employed at Tigranocerta were effectively repelled by the defenders by the use of [[naphtha]], making  Tigranocerta, according to one scholar, the site of "perhaps the world's first use of [[chemical warfare]]."<ref>{{cite book|authorlink=Warwick Ball|author=Ball, Warwick|title=Rome in the East: The Transformation of an Empire|location= London|publisher=RoutledgeCurzon|year=2001|pages=11, 452n.|isbn=0-415-24357-2}}</ref>

However, the loyalty of the city's population was untested: since Tigranes had forcibly removed many of its inhabitants from their native lands and brought them to Tigranocerta, their allegiance to the king was cast into doubt. They soon proved their unreliability: when Tigranes and his army appeared on a hill overlooking the city, the inhabitants "greeted his [Lucullus] appearance with shouts and din, and standing on the walls, threateningly pointed out the Armenians to the Romans."<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 27.1].</ref>

==Forces==

Appian claims that Lucullus had embarked from Rome with only a single [[Roman legion|legion]]; upon entering Anatolia to make war against Mithridates, he added four more legions to his army. The overall size of this force consisted of 30,000 infantry and 1,600 cavalry.<ref>Appian. ''The Mithrdatic Wars'', [http://www.livius.org/ap-ark/appian/appian_mithridatic_15.html#%A772 12.72].</ref> Following Mithridates' retreat to Armenia, Appian estimates Lucullus' invading force to be only two legions and 500 horsemen,<ref name="12.84">Appian. ''The Mithrdatic Wars'', [http://www.livius.org/ap-ark/appian/appian_mithridatic_17.html#%A784 12.84].</ref> although it is highly improbable that he would have undertaken the invasion of Armenia with such a small army.<ref name="Luke">Ueda-Sarson, Luke. [http://www.ne.jp/asahi/luke/ueda-sarson/Tigranocerta.html Tigranocerta: 69 BC]. June 20, 2004. Accessed June 12, 2008.</ref> Historian [[A. N. Sherwin-White|Adrian Sherwin-White]] places the size of Lucullus' force to 12,000 seasoned [[legionaries]] (composed of three legions), and 4,000 provincial cavalry and light infantry.<ref>Sherwin-White. "Lucullus", p. 240.</ref> The Roman army was further bolstered by several thousand allied [[Roman Gaul|Gallic]], [[Thracians|Thracian]], and [[Bithynian]] infantry and cavalry.

Tigranes' army clearly held a numerical superiority over that of Lucullus'.<ref>{{cite book|author1=Cowan, Ross  |author2=Adam Hook |lastauthoramp=yes |title=Roman Battle Tactics 109BC-AD313|location=University Park, Il.|publisher=Osprey Publishing|year=2007|isbn=1-84603-184-2|page=41}}</ref> According to Appian, it numbered 250,000 infantry and 50,000 cavalry.<ref>Appian. ''The Mithrdatic Wars'', [http://www.livius.org/ap-ark/appian/appian_mithridatic_17.html#%A785 12.85].</ref> Many scholars, however, doubt these figures accurately reflect the true number of Tigranes' army and believe they are highly inflated.<ref name="Luke"/><ref name="Hook. p. 41">Cowan and Hook. ''Roman Battle Tactics'', p. 41.</ref> Some historians, most notably [[Plutarch]], wrote that Tigranes considered Lucullus' army far too small, and upon seeing it, is quoted to have said that "If they come as ambassadors, they are too many; if they are soldiers, too few,"<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 27.4].</ref> although some have expressed doubt on the veracity of this quote.<ref>[[Vahan Kurkjian|Kurkjian, Vahan]] (1958). ''[http://penelope.uchicago.edu/Thayer/E/Gazetteer/Places/Asia/Armenia/_Texts/KURARM/13*.html#before_Tigranocerta A History of Armenia]''. New York: Armenian General Benevolent Union of America, p. 80.</ref><ref>{{cite book|author=Chahin, Mack|title=The Kingdom of Armenia|location=London|publisher=RoutledgeCurzon|year=2001|isbn=0-7007-1452-9|page=201}}</ref> Tigranes also possessed several thousand [[cataphract]]s, formidable [[heavy cavalry|heavily armored cavalry]] that were clad in [[mail armor]] and armed with [[lances]], [[spear]]s or [[Bow (weapon)|bow]]s.<ref>{{cite book
|author=Wilcox, Peter
|title=Rome's Enemies (3): Parthians and Sassanid Persians
|location=University Park, Il.
|publisher=Osprey Publishing
|year=1986
|pages=42–44
|isbn=0-85045-688-6}}</ref>

==Disposition and engagement==
[[File:Lukullus wiki.png|thumb|left|150px|The commander of the Roman Senate army, [[Lucullus|Lucius Licinius Lucullus]].]]
The two armies converged toward the Batman-Su river slightly to the southwest to Tigranocerta.<ref name="loc">The precise location of the battle has remained a source of dispute. Appian and Plutarch remark that the battlefield was close enough for the garrison commander of Tigranocerta to make out the figures of the armies; however, according to [[T. Rice Holmes]], the site of the battlefield was roughly sixteen miles to the southwest of the city. Holmes restricts the location of the battle to three contemporary sites: Arzen, Farkin, and Tell Ermen. See T. Rice Holmes, "[http://www.jstor.org/stable/295584 Tigranocerta]." ''Journal of Roman Studies'', vol. vii, 1917, pp. 136-138.</ref>

Tigranes' army was positioned on the east bank of the river while Lucullus, who had left a rear guard to continue the siege of the city, met the Armenian army on the river's west bank. The Armenian army was formed of three sections.<ref name="Luke"/> Two of Tigranes' vassal kings led the left and right flanks, while Tigranes led his cataphracts in the center. The rest of his army stood in front of a hill, a position Lucullus soon exploited.

Roman troops at first attempted to dissuade Lucullus from engaging in battle since October 6 marked the day of the disastrous [[battle of Arausio]], where the general [[Quintus Servilius Caepio]] and his Roman army were delivered a crushing defeat by the [[Germanic peoples|Germanic]] [[Cimbri]] and [[Teuton]] tribes. Ignoring his troops' superstitious beliefs, Lucullus is said to have responded, "Verily, I will make this day, too, a lucky one for the Romans."<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 27.7].</ref>

Cowan and Hook suggest that Lucullus would have deployed the Romans in a ''[[Roman infantry tactics|simplex acies]]'', that is to say a single line, so making the frontage of the army as wide as possible as a counter to the cavalry.<ref name="Hook. p. 41"/> He took several of his troops downriver, where the river was the easiest to ford, and at one moment, Tigranes believed that this move meant Lucullus was withdrawing from the battlefield.<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 27.5].

One of Mithirdates' commanders, Taxilés, who accompanied Tigranes, informed the Armenian king that, "When these men are merely on the march, they do not put on gleaming armour, nor have their shields polished and helmets uncovered as they have now, taken the leather covers from their armour. No, this splendor means they are going to fight, and are now advancing on their enemies." Quoted in Cowan and Hook. ''Roman Battle Tactics'', p. 42.</ref>

Lucullus had initially decided to make a ''running charge'' with his infantry, a Roman military tactic that minimized the amount of time an enemy could utilize its archers and sling infantry prior to [[close combat]] engagement.<ref>Cowan and Hook. ''Roman Battle Tactics'', p. 42.</ref> However, he decided against this at the last moment when he realized that the Armenian cataphracts posed the greatest threat to his men, ordering instead a diversionary attack with his Gallic and Thracian cavalry against the cataphracts.

With the cataphracts' attention fixated elsewhere, Lucullus formed two cohorts into [[Maniple (military unit)|maniples]] and then ordered them to [[Ford (crossing)|ford]] across the river.<ref>Rice. "Tigranocerta", p. 136.</ref> His objective was to [[Flanking maneuver|outflank]] Tigranes' cataphracts by circling counterclockwise around the hill and attacking them from the rear.<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 28.1-3].</ref>

Lucullus personally led the charge on foot and upon reaching the top of the hill, he yelled to his soldiers in an effort to buoy their morale: "The day is ours, the day is ours, my fellow soldiers!"<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 28.3].</ref> With this, he gave special instructions to the cohorts to attack the horses' legs and thighs, since these were the only areas of the cataphracts which were not armored.<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 28.4].</ref><ref name="Sherwin-White. Lucullus, p. 241">Sherwin-White. "Lucullus", p. 241.</ref> Lucullus charged downwards along with his cohorts and his orders soon proved fatal: the lumbering cataphracts were caught by surprise and, in their attempts to break free from their attackers, careened into the ranks of their own men as the lines began to collapse.<ref name="12.84"/><ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 28.4-5].</ref>

The infantry, which was also made up of many non-Armenians, began to break rank and confusion spread to the rest of the body of Tigranes' army. While the great king himself took to flight with his baggage train northwards, the entire line of his army gave way. The casualties reported for Tigranes' army are immense, with estimates given from 10,000 to as many as 100,000 men.<ref name="Sherwin-White. Lucullus, p. 241"/>

Plutarch says that on the Roman side, "only a hundred were wounded, and only five killed,"<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 28.6].</ref> although such low figures are highly unrealistic.<ref>Chahin. ''Kingdom of Armenia'', p. 201.</ref>  Cowan and Hook, while considering these losses ridiculous, think it's clear that the battle was won with disproportionate losses.<ref>Cowan and Hook. ''Roman Battle Tactics'', p. 43.</ref>

==Aftermath and legacy==
With no army left to defend Tigranocerta, and a foreign populace that gleefully opened the gates to the Romans, Lucullus' army began the wholesale looting and plunder of the city.<ref>Plutarch. ''Life of Lucullus'', [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Plutarch/Lives/Lucullus*.html 29.3].</ref> The city was literally deconstructed piece by piece. The king's treasury, estimated to be worth 8,000 [[attic talent|talents]],<ref>Kurkjian. ''History of Armenia'', p. 81.</ref> was looted and each soldier in the army was awarded 800 [[drachma]]. The battle also resulted in severe territorial losses: most of the lands in Tigranes' empire to the south of the Taurus fell under the sway of Rome.

Despite the heavy losses Tigranes suffered, the battle was not decisive in ending the war. In retreating northwards, Tigranes and Mithridates were able to elude Lucullus' forces, though losing again against the Romans during the [[battle of Artashat]].<ref>{{hy icon}} Kurdoghlian, Mihran (1994). ''Badmoutioun Hayots, Volume I'' (''History of the Armenians''). Athens, Greece: Hradaragoutioun Azkayin Oussoumnagan Khorhourti, pp. 67-76.</ref> In 68, Lucullus' forces' began to mutiny, longing to return home, and he withdrew his forces from Armenia the following year.<ref>Sherwin-White. "Lucullus", p. 243.</ref>

The battle is highlighted by many historians specifically because Lucullus overcame the numerical odds facing his army.<ref name="Luke"/> The Italian philosopher [[Niccolò Machiavelli]] remarked upon the battle in his book, ''[[The Art of War (Machiavelli)|The Art of War]]'', where he critiqued Tigranes' heavy reliance on his cavalry over his infantry.<ref>{{cite book|authorlink=Niccolò Machiavelli|author=Machiavelli, Niccolò|title=The Art of War|location=Chicago|publisher=University Of Chicago Press|year=2005|isbn=0-226-50046-2|page=40}}</ref>

==References==
{{reflist|2}}

==Further reading==
*{{cite book|author=Armen, Herant K.|title=Tigranes the Great: A Biography|location=Detroit|publisher=Avondale|year=1940}}
*Manandyan, Hakob. ''Tigranes II and Rome: A New Interpretation Based on Primary Sources''. Trans. [[George Bournoutian]]. Costa Mesa, CA: Mazda Publishers, 2007.
*{{hy icon}} Manaseryan, Ruben. ''Տիգրան Մեծ՝ Հայկական Պայքարը Հռոմի և Պարթևաստանի Դեմ,  մ.թ.ա. 94-64 թթ.'' (''Tigran the Great: The Armenian Struggle Against Rome and Parthia, 94-64 B.C.''). Yerevan: Lusakan Publishing, 2007.

==External links==
*[http://www.armenica.org/cgi-bin/history/en/getHistory.cgi?4=1=28 The Battle of Tigranakert (October 6, 69 BC.)]
*[http://www.ne.jp/asahi/luke/ueda-sarson/Tigranocerta.html Battle of Tigranocerta]

{{good article}}

{{coord missing}}

[[Category:Battles involving the Roman Republic|Tigranocerta 069]]
[[Category:Battles involving the Kingdom of Armenia|Tigranocerta 69 BC]]
[[Category:60s BC conflicts|Tigranocerta]]
[[Category:Mithridatic Wars|Tigranocerta]]
[[Category:69 BC]]