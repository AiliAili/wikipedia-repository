{{Infobox currency
| currency_name_in_local = {{native name|ps|افغانۍ}}; ([[Dari language|Dari]])افغانی
| image_1 =
| image_title_1 = Afghani paper money
| iso_code = AFN
| using_countries = {{AFG}} (alongside the [[United States dollar|US dollar]])
| Unofficial_Users = Pakistan and Uzbekistan| inflation_rate = 13.8%
| inflation_source_date = ''[https://www.cia.gov/library/publications/the-world-factbook/fields/2092.html The World Factbook]'', 2011 est.
| subunit_ratio_1 = {{Frac|100}}
| subunit_name_1 = pul
| symbol ='''؋''' (U+060B) or '''Af''' (sing.) or '''Afs'''<ref name="DAB">Da Afghanistan Bank. "[http://www.centralbank.gov.af/pdf/CapitalNotesIssuanceAndAuctionRegulation.pdf Capital Notes Issuance and Auction]." Accessed 26 Feb 2011.</ref>
| used_coins = 1 Af,<ref name="DAB"/> 2, 5 Afs
| used_banknotes = 1 Af, 2, 5, 10, 20, 50, 100, 500, 1000 Afs
| issuing_authority = [[Da Afghanistan Bank]]
| issuing_authority_website = {{URL|www.centralbank.gov.af}}
}}

The '''afghani''' ([[currency sign|sign]]: '''Afs''';<ref name="DAB"/> [[Pashto language|Pashto]]: '''{{lang|ps|افغانۍ}}'''; [[Dari (Eastern Persian)|Dari]] '''{{lang|fa|افغانی}}'''; [[ISO 4217|code]]: '''AFN''') is the [[currency]] of [[Afghanistan]]. It is nominally subdivided into 100 ''[[Pul (coin)|pul]]'' (پول), although there are no pul coins currently in circulation.

==Original afghani (1925-2003)==
The first afghani ([[ISO 4217]] code: AFA) was introduced in 1925. In addition to being subdivided into 100 pul, 20 afghani were equal to one ''amani''. The rate of conversion from the rupee is sometimes quoted as 1 afghani = 1 rupee 6 paisa,<ref>{{numis cite TMMH|region=asia|accessdate=2007-08-27}}</ref> based on the silver contents of the last rupee coins and the first Afghani coins. The Afghani initially contained 9 grams of silver.<ref>{{numis cite SCWC|date=1991}}</ref>

Except during [[World War I]] Afghanistan's foreign exchange rate has been freely determined by market forces.<ref name="fry1">Fry, Maxwell J. (1976) "A Monetary Approach to Afghanistan's Flexible Exchange Rate", ''Journal of Money, Credit and Banking,'' Vol. 8 (2): 219-225</ref> However, for some periods, a dual exchange rate regime existed in Afghanistan: an official exchange rate which was fixed by the Afghan [[Central Bank]], and a [[free market]] exchange rate which was determined by the supply and demand forces in Kabul's money [[bazaar]] called ''Saraye Shahzada''.<ref name="fry2">Fry, Maxwell J. (1974) "The Afghan Economy: Money, Finance, and the Critical Constraints to Economic Development", Brill Publications, Leiden, Holland</ref> For example, in order to avoid the seasonal fluctuations in the exchange rate, a fixed exchange rate was adopted in 1935 by the [[Banke Millie Afghan|Bank-e Milli]], which was then responsible for the country's exchange rate system and official reserves.<ref name="fry2" /> Bank-e Milli agreed to exchange afghanis at 4 Afs against 1 [[Indian rupee]] in 1935. After the establishment of [[Da Afghanistan Bank]] as the [[Central Bank]] of Afghanistan, such a preferential official fixed exchange rate continued to be practiced. Although Da Afghanistan Bank tried to keep its official rate close to the Saraye Shahzada's exchange rate, the gap between the official and free-market exchange rates widened in the 1980s and during the civil war.

Since 2002, Da Afghanistan Bank has adopted a [[floating exchange rate]] regime and has let the exchange rate to be determined freely by market forces.

The Afghani exchange rate [[wikt:vis-à-vis|vis-à-vis]] the [[United States dollar|U.S. dollar]] since 1950 has been shown in the following table:

{|class="wikitable"
!colspan="3"|Afghani exchange rate (LCU in USD)<ref>Sources: Fry, Maxwell (1976); and "World Development Indicators" database of the World Bank</ref>
|-
!Date!!Free-market exchange rate!!Official exchange rate
|-
|1950||39.0||
|-
|1960||40.8||17.7
|-
|1970||84.8||39.9
|-
|1980|| ||39.2
|-
|2003||49.0||49.0
|-
|2010||45.2||45.2
|}

Prior to the [[War in Afghanistan (2001–present)|U.S. invasion of Afghanistan]], warlords, political parties, foreign powers and forgers each made their own Afghani banknotes, with no regard to standardization or honouring serial numbers. In December 1996, shortly after the [[Taliban]] took control of Afghanistan's institutions, [[Ehsanullah Ehsan (banker)|Ehsanullah Ehsan]], the chairman of the [[Taliban]]'s Central Bank, declared most Afghani notes in circulation to be worthless (approximately 100 [[Orders of magnitude (numbers)#1012|trillion]] Afghani) and cancelled the contract with the [[Russia]]n firm that had been printing the currency since 1992. Ehsan accused the firm of sending new shipments of Afghani notes to ousted president [[Burhanuddin Rabbani]] in northern [[Takhar province]]. The exchange rate at the time of Ehsan's announcement was 21,000 Afghani to the [[U.S. dollar]] The [[Afghan Northern Alliance|Northern Alliance]] then had banknotes produced in [[Russia]] which were sold on the markets of [[Kabul]] at half their value.{{Citation needed|date=April 2011}}

In April, 2000, the afghani traded at 6400 AFA per USD. By 2002, the afghani was valued at 43,000 AFA per [[United States dollar|USD]]. By 2009, the afghani was valued at 45 AFA per USD

===Coins===
In 1925, bronze and brass 2, 5 and 10 pul, [[Billon (alloy)|billon]] 20 pul, silver {{Frac|2}} and 1 afghani, and gold {{Frac|2}} and 1 amani coins were introduced, followed by silver {{Frac|2|1|2}} afghani and gold {{Frac|2|1|2}} amani in 1926.{{Citation needed|date=April 2013}} In 1930, bronze and brass 1 and 25 pul were added, along with bronze 3 pul and cupro-nickel 10 and 20 pul in 1937.{{Citation needed|date=April 2013}}

In 1952, aluminium 25 pul and nickel-clad steel 50 pul were introduced, followed by aluminium 2 and 5 afghani in 1958. In 1961 nickel-clad steel 1, 2 and 5 afghani were minted; the 1 and 2 afghani coins show years of [[Solar Hijri calendar|SH]] 1340 and the 5 Afghani coin shows the year [[Islamic calendar|AH]] 1381.<ref>{{cite web|title=Afghanistan coins|url=http://en.numista.com/catalogue/afghanistan-12.html?o=y|work=Numista|accessdate=17 April 2013}}</ref>  In 1973, the Republic of Afghanistan issued brass-clad-steel 25 pul, copper-clad steel 50 pul and cupro-nickel-clad steel 5 afghani coins.{{Citation needed|date=April 2013}} These were followed, between 1978 and 1980, by issues of the [[Democratic Republic of Afghanistan]] consisting of aluminium-bronze 25 and 50 pul and cupro-nickel 1, 2 and 5 afghani.{{Citation needed|date=April 2013}} A number of commemorative coins were also issued by the [[Islamic State of Afghanistan]] between 1995 and 2001.

===Banknotes===
Between 1925 and 1928, Treasury notes were introduced in denominations of 5, 10 and 50 afghanis. In 1936, 2, 20 and 100 afghani notes were added. The Bank of Afghanistan took over paper money production in 1939, issuing notes for 2, 5, 10, 20, 50, 100, 500 and 1000 afghanis. The 2 and 5 afghani notes were replaced by coins in 1958. In 1993, 5000 and 10,000 afghani notes were introduced.

[[File:100 Afghanis (1963 - top).jpg|thumb|100 afghanis banknote of [[King Zahir Shah]]'s reign (Front)]]
[[File:100 Afghanis (1963 - back).jpg|thumb|100 afghanis banknote of [[king Zahir Shah]]'s reign (Back)]]
"A 'pothole [[cave]]' or 'mouth of a shaft' (or [[Pit cave]]) is said to be visible on the 10,000 afghanis banknote as a limited dark area in the hillside above the ancient 'pol' or gateway at the ruins near [[Lashkar Gah]]. This is possibly the entrance to one of the man-made undergrounds at Qal'a-i-Bost."<ref>Gebauer, Herbert Daniel. 2004. Resources on the Speleology of Afghanistan. Berliner Hoehlenkundliche Berichte. Band 14. {{ISSN|1617-8572}}. Page 81.</ref>

==New Afghani (2002–present)==

Between October 7, 2002, and January 2, 2003, a new afghani was introduced with the [[ISO 4217]] code AFN. No subdivisions have been issued. It replaced the previous afghani at two distinct rates. Issues of the government of President [[Burhanuddin Rabbani]] were replaced at a rate of 1000 to the new afghani, whilst the issues of [[Abdul Rashid Dostum]] (the Northern Alliance) were replaced at a rate of 2000 to the new afghani, The new afghani was valued at 43 afghani to the U.S. dollar. Prior to the reissue, there were more than 15 [[Orders of magnitude (numbers)#1012|trillion]] afghani in circulation after unrestrained printing under Taliban rule and during wars and occupation.{{Citation needed|date=November 2008}}

This was in preparation for October 8 when all prices in the Afghan marketplace were to be specified in afghani.

After depreciating during the last quarter of 2003/04, the afghani has been appreciating steadily, gaining 8 percent against the U.S. dollar between end-March 2004 and end-July 2004. This appreciation, at a time of increasing inflation, appears to reflect a greater willingness by the population to use the afghani as a medium of exchange and as a store of value. This trend appears to be attributable to the relative stability of the exchange rate since the introduction of the new currency, administrative measures aimed at promoting its use, such as the requirement that shopkeepers must price goods in afghani. Donors are increasingly making payments in afghani instead of U.S. dollars and this appears to be widely accepted.

===Coins===
In 2005, coins were introduced in denominations of 1, 2 and 5 afghanis <ref>{{cite web | author=Blogspot | date=2006-11-15 | url=http://worldcoinnews.blogspot.com/2006/11/afghanistan-2005.html | title=Update - Afghanistan. | accessdate=2006-11-20}} {{Dead link|date=September 2010|bot=H3llBot}}</ref>

<gallery>
Image:Afghani_Coins.JPG|Afghani Coins
</gallery>

===Banknotes===
On 7 October 2002, banknotes were introduced in denominations of 1, 2, 5, 10, 20, 50, 100, 500, and 1000 afghanis. The 1, 2 and 5 afghani notes were replaced by coins in 2005. In 2004 and 2008, the security features on several denominations were improved.

{|class="wikitable" style="font-size: 90%"
!colspan="9"| 2002 Series [http://www.centralbank.gov.af/pdf/Banknotes%20Specifications%20and%20Images.pdf]
|-
!colspan="2"| Image !!rowspan="2"| Value !!rowspan="2"| Dimensions !!rowspan="2"| Main Colour !!colspan="2"| Description !!colspan="2"| Date of
|-
! Obverse !! Reverse !! Obverse !! Reverse !! printing !! issue
|-
| 
| 
| 1 afghani
|rowspan="3"| 131 × 55&nbsp;mm
| Pink
|rowspan="3"| Seal of [[Da Afghanistan Bank]] with [[Eucratides I]]-era coin.
| Mosque in [[Mazari Sharif]]
|rowspan="9"| 2002<br/>([[Islamic calendar|AH]] 1381)
|rowspan="9"| October 7, 2002
|-
| 
| 
| 2 afghanis
| Blue
| [[Paghman Gardens]]
|-
| 
| 
| 5 afghanis
| Brown
| [[Bala Hissar, Kabul|Bala Hissar]]
|-
| 
| 
| 10 afghanis
| 136 × 56&nbsp;mm
| Yellow green
| [[Ahmed Shah Durrani]] mausoleum, Kandahar
| [[Paghman Gardens]]
|-
| 
| 
| 20 afghanis
| 140 × 58&nbsp;mm
| Brown
| [[Mahmud of Ghazni]]'s Tomb
| [[Arg (Kabul)|Arg]] King's Palace
|-
| 
| 
| 50 afghanis
| 144 × 60&nbsp;mm
| Dark green
| Shah Do Shamira Mosque
| Salang Pass
|-
| 
| 
| 100 afghanis
| 148 × 62&nbsp;mm
| Violet
| Pul e Khishti Mosque
| [[Arch of Bost]]
|-
| 
| 
| 500 afghanis
| 152 × 64&nbsp;mm
| Blue
| Khwaja Abdullah Ansari Mosque in Herat
| [[Kandahar Airport]] tower
|-
| 
| 
| 1000 afghanis
| 156 × 66&nbsp;mm
| Orange
| Mazar Sharif [[Shrine of Hazrat Ali]]
| Tomb of [[Ahmad Shah Durrani]] Baba
|-
|colspan="9"|{{Standard banknote table notice|standard_scale=Y|BrE=Y}}
|}
{{Exchange Rate|AFN}}

==See also==
* [[Economy of Afghanistan]]
* [[Rupee]]
* [[Afghan rupee]]

==References==
{{Reflist|30em}}

==Further reading==
*{{numis cite SCWPM|date=1994}}

==External links==
*[http://numismondo.com/pm/afg Afghanistan's paper money]
*[http://www.afghanistan-photos.com/crbst_34.html Ancient banknotes of Afghanistan]
*[http://www.mof.gov.af/ Ministry of Finance, Afghanistan]
*[http://convertmycurrency.com/currency-afghan-afghani.html Afghan Afghani Exchange Rates]

{{Currencies of Asia}}

[[Category:Currencies of Afghanistan]]
[[Category:1925 introductions]]