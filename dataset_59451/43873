{{Infobox company
| name             = Wag-Aero Group
| logo             = [[File:Wag-Aero Logo 2015.png]]
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = 1960s
| founder          = Dick and Bobbie Wagner
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| hq_location = 
| hq_location_city = [[Lyons, Wisconsin]]
| hq_location_country = [[United States]]
| area_served      = 
| key_people       = 
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]] and [[aircraft part]]s
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = Bill Read and Mary Myers
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|www.wagaero.com}}
| footnotes        = 
| intl             = 
}}
[[File:Wagaero Cuby AN0995379.jpg|thumb|right|[[Wag-Aero CUBy]] on floats]]
[[File:Wag-Aero SPORTSMAN 2+2 C-GMTL 01.JPG|thumb|right|[[Wag-Aero Sportsman 2+2]]]]
[[File:Wagaero WAGABOND amateur-built C-GCXB 03.JPG|thumb|right|[[Wag-Aero Wag-a-Bond]]]]
The '''Wag-Aero Group''' is an [[United States|American]] [[aircraft manufacturer]], that was founded by Dick and Bobbie Wagner in the 1960s and is based in [[Lyons, Wisconsin]]. The company specializes in the design and manufacture of [[kit aircraft]] and [[aircraft part]]s for [[Homebuilt aircraft|amateur construction]].<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', pages 289-290, 367-374, 381-396, 401 and 407. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="About">{{cite web|url = http://www.wagaero.com/about.html|title = A Brief History|accessdate = 12 March 2015|last = Wag-Aero Group|date = }}</ref><ref name="Plane and Pilot">Plane and Pilot: ''1978 Aircraft Directory'', page 159. Werner & Werner Corp, Santa Monica CA, 1977. ISBN 0-918312-00-0</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 126. WDLA UK, Lancaster UK, 2011. {{ISSN|1368-485X}}</ref><ref name="KitplanesDec1998">Downey, Julia: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 75-76. Primedia Publications. {{ISSN|0891-1851}}</ref>

The company is owned by Bill Read and Mary Myers.<ref name="GAN">{{cite news|url = http://generalaviationnews.com/2005/08/05/wag-aero-buys-safe-air-repair/|title = Wag-Aero buys Safe Air Repair|accessdate = 12 March 2015|last = GAN Staff|date = 5 August 2005| work = General Aviation News}}</ref>

==Divisions==
The Wag-Aero Group of companies includes several aerospace divisions from acquisitions, including:<ref name="About"/><ref name="GAN"/>
* Aero Fabricators
* Ground Support Manufacturing, Inc
* [[Leading Edge Air Foils]]
* Safe Air Repair
* Viking Aero

==History==
Wag-Aero was started by Dick and Bobbie Wagner in the 1960s and run from the basement of their home in Lyons, Wisconsin. In 1971 the company moved into permanent quarters and an aerodrome was constructed to accommodate fly-in customers, now known as Wag-Aero Airport (WI92).<ref name="About"/><ref name="airport">{{cite web|url = http://www.airnav.com/airport/WI92|title = Wag-Aero Airport, Lyons, Wisconsin, USA|accessdate = 12 March 2015|last = .airnav.com|date = 5 March 2015}}</ref>

The company formed a subsidiary, Aero Fabricators, in the 1970s to make welded and sheet metal aircraft parts, plus seat belts.<ref name="About"/>

On 1 September 1995 the Wagners sold the group of companies to Bill Read and Mary Myers. In 1997 they purchased Viking Aero and in April 2002 acquired Ground Support Manufacturing, Inc. In 2005 they bought Safe Air Repair, a parts manufacturer. Ultralight parts supplier [[Leading Edge Air Foils]] (LEAF) was also added to the group.<ref name="About"/><ref name="GAN"/>

==Products==
In addition to aircraft parts, the company supplies parts and kits to construct several light aircraft designs, all based on 1930s and 1940s [[Piper Aircraft]] models.<ref name="Aerocrafter"/><ref name="Plane and Pilot"/><ref name="WDLA11"/><ref name="KitplanesDec1998"/><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2011 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 76. Belvoir Publications. {{ISSN|0891-1851}}</ref>

The [[Wag-Aero CUBy]], a [[Piper J-3]] replica, was designed by Dick Wagner and first flew on 12 March 1975. It is offered in several different models and variants including the Acro Trainer, Observer, Sport Trainer and Super Sport.<ref name="Aerocrafter"/><ref name="Plane and Pilot"/><ref name="WDLA11"/><ref name="KitplanesDec1998"/><ref name="KitplanesDec2011"/>

The [[Wag-Aero Wag-a-Bond]] was also designed by Dick Wagner as a [[Piper PA-17 Vagabond]] replica and first flew on 9 June 1978.<ref name="Aerocrafter"/><ref name="WDLA11"/><ref name="KitplanesDec1998"/><ref name="KitplanesDec2011"/>

The [[Wag-Aero Sportsman 2+2]], also known as the [[Wag-Aero CHUBy CUBy]], was designed by Dick Wagner and Tom Iverson as a [[Piper PA-14 Family Cruiser]] replica and first flew on 8 May 1982.<ref name="Aerocrafter"/><ref name="KitplanesDec1998"/><ref name="KitplanesDec2011"/>

== Aircraft ==

{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| Summary of aircraft built by Wag-Aero
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[Wag-Aero CUBy]]'''
|align=center| 12 March 1975
|align=center| 
|align=left| Two seat [[Piper J-3]] replica
|-
|align=left| '''[[Wag-Aero Wag-a-Bond]]'''
|align=center| 9 June 1978
|align=center| 
|align=left| Two seat [[Piper PA-17]] replica
|-
|align=left| '''[[Wag-Aero CHUBy CUBy]]'''
|align=center| 8 May 1982
|align=center| 
|align=left| Four seat [[Piper PA-14]] replica
|-
|}

==References==
{{Reflist|30em}}

==External links==
{{Commons category}}
*{{Official website|http://www.wagaero.com/}}
{{Wag-Aero aircraft}}
{{LEAF aircraft}}

[[Category:Aircraft manufacturers of the United States]]
[[Category:Ultralight aircraft]]
[[Category:Homebuilt aircraft]]
[[Category:Companies based in Wisconsin]]
[[Category:Walworth County, Wisconsin]]