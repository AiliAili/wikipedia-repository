__NOTOC__
{|{{Infobox Aircraft Begin
  |name = Siemens-Schuckert D.III
  |image = SSWD3.jpg  
  |caption = 
}}{{Infobox Aircraft Type
  |type = [[fighter aircraft|Fighter]]
  |manufacturer = [[Siemens-Schuckert|Siemens-Schukert Werke]]
  |designer = 
  |first flight = October [[1917 in aviation|1917]] 
  |introduced = April [[1918 in aviation|1918]]
  |retired = 
  |status = 
  |primary user = ''[[Luftstreitkräfte]]''
  |more users = 
  |produced = 80
  |number built = 
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}

The '''Siemens-Schuckert D.III''' was a [[Germany|German]] single-seat fighter built by [[Siemens-Schuckert]] Werke. The '''D.III''' was a development of the earlier Siemens-Schuckert D.IIc prototype.<ref name="Green p530">Green and Swanborough 1994, p. 530.</ref> The D.III was an (nearly) equal-span [[biplane]] powered by a 160&nbsp;hp (119&nbsp;kW) [[Siemens-Halske Sh.III]] bi-rotary engine. ''Idflieg'' placed an order for 20 aircraft in December 1917, followed by a second order of 30 aircraft in February 1918.<ref name="Gray p213">Gray and Thetford 1962, p. 213.</ref>

==Operational history==
Approximately 41 D.IIIs were delivered to frontline units between April and May 1918.<ref name="Gray p213"/> Most aircraft were supplied to ''Jagdgeschwader'' II,<ref name="VanWyngarden p43">VanWyngarden 2005, p. 43.</ref> whose pilots were enthusiastic about the new aircraft's handling and rate of climb. After only seven to 10 hours of service, however, the Sh.III engines started showing serious problems with overheating and piston seizure.<ref name="Gray p214">Gray and Thetford 1962, p. 214.</ref> The problem was later traced to the ''Voltol'' mineral oil that was used to replace the now-scarce [[castor oil]].<ref name="VanWyngarden p43"/> Furthermore, the close-fitting engine cowling provided inadequate cooling to the engine.<ref name="Gray p215">Gray and Thetford 1962, p. 215.</ref>

In late May 1918, ''Jagdgeschwader'' II replaced its D.IIIs with the older [[Fokker Dr.I]].<ref name="VanWyngarden p44">VanWyngarden 2005, p. 44.</ref> The remaining D.III aircraft were returned to the Siemens-Schuckert factory, where they were retrofitted with new Sh.IIIa engines, an enlarged rudder, and cutaway cowlings that provided improved airflow.<ref name="Green p530"/> A further 30 new production D.IIIs incorporated these modifications. Total production amounted to 80 aircraft.<ref name="Green p530"/>

In July 1918, the D.III returned to active service as an interceptor with home defense squadrons.<ref name="Gray p214"/> By this time, the D.III had been replaced in production by the [[Siemens-Schuckert D.IV]].

==Operators==
;{{flag|German Empire}}
*''[[Luftstreitkräfte]]''
;{{SUI}}
*[[Swiss Air Force]]

==Specifications==
[[File:Siemens-Schukert D.III dwg.jpg|thumb|Siemens-Schukert D.III drawing]]
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|crew=One
|capacity=
|length main= 18 ft 8½ in
|length alt= 5.70 m
|span main= 27 ft 7¾ in
|span alt= 8.43 m
|height main=  9 ft 2¼ in
|height alt= 2.80 m
|area main= 203.44 ft²
|area alt= 18.90 m²
|airfoil=
|empty weight main= 1,177 lb
|empty weight alt= 534 kg
|loaded weight main=  
|loaded weight alt=   
|useful load main=  
|useful load alt=  
|max takeoff weight main= 1,598 lb 
|max takeoff weight alt= 725 kg
|more general=
|engine (prop)=[[Siemens-Halske Sh.III]]
|type of prop=bi-rotary
|number of props=1
|power main= 160 hp
|power alt= 119 kW
|power original=
   
|max speed main= 112 mph 
|max speed alt= 180 km/h
|cruise speed main= 
|cruise speed alt= 
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 
|range alt= 
|ceiling main= 26,245 ft
|ceiling alt= 8000 m
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 
|power/mass alt= 
|more performance= 
|armament=
*2× 7.92 mm (0.31 in) [[LMG 08/15]] machine-guns
|avionics=

}}

==References==
{{commons category|Siemens-Schuckert D.III}}

===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
* Gray, Peter and Owen Thetford. ''German Aircraft of the First World War''. London: Putnam, 1962. ISBN 0-933852-71-1.
* Green, William and Gordon Swanborough. ''The Complete Book of Fighters.'' London: Salamander Books, 1994. ISBN 0-8317-3939-8. 
* VanWyngarden, Greg. ''Jagdgeschwader Nr II Geschwader 'Berthold' (Aviation Elite Units No. 19).'' Oxford: Osprey Publishing, 2005. ISBN 1-84176-727-1.
{{refend}}

<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=
|lists=
|see also=
}}
<!-- ==External links== -->
{{Siemens-Schuckert aircraft}}
{{Idflieg fighter designations}}
{{World War I Aircraft of the Central Powers}}

[[Category:German fighter aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|D.III]]
[[Category:Sesquiplanes]]