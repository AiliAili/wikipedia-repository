{{italic title}}
{{Infobox journal
| title         = Journal of Combinatorial Theory
| cover         = 
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = 
| discipline    = Mathematics
| peer-reviewed = 
| language      = English
| editor        = <!-- or |editors= -->
| publisher     = [[Elsevier]]
| country       = 
| history       = 1966-present
| frequency     = Monthly
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
 | ISSNlabel = Series A
 | ISSN = 0097-3165
 | eISSN = 
 | ISSN2label = Series B
 | ISSN2 = 0095-8956
 | eISSN2 = 
| CODEN         =
| JSTOR         = 
| LCCN          = 
| OCLC          =
| website       =
| link1         = http://www.journals.elsevier.com/journal-of-combinatorial-theory-series-a/
| link1-name    = Series A website
| link2         = http://www.journals.elsevier.com/journal-of-combinatorial-theory-series-b/
| link2-name    = Series B website
| boxwidth      = 
}}
The '''''Journal of Combinatorial Theory''''', '''Series A'''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/622862/description Journal of Combinatorial Theory, Series A - Elsevier<!-- Bot generated title -->]</ref> and '''Series B''',<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/622863/description Journal of Combinatorial Theory, Series B - Elsevier<!-- Bot generated title -->]</ref> are [[mathematical journal]]s specializing in [[combinatorics]] and related areas. They are  published by [[Elsevier]]. ''Series A'' is concerned primarily with [[Mathematical structure|structure]]s, [[block design|design]]s, and applications of combinatorics. ''Series B'' is concerned primarily with [[graph theory|graph]] and [[matroid theory]]. The two series are two of the leading journals in the field and are widely known as "JCTA" and "JCTB".{{Citation needed|date=February 2010}}

The journal was founded in 1966 by [[Frank Harary]] and [[Gian-Carlo Rota]].<ref name=sigact>They are acknowledged on the journals' title pages and Web sites.  See [http://www.elsevier.com/wps/find/journaleditorialboard.cws_home/622862/editorialboard Editorial board of JCTA]; [http://www.elsevier.com/wps/find/journaleditorialboard.cws_home/622863/editorialboard Editorial board of JCTB].</ref> Originally there was only one journal, which was split into two parts in 1971 as the field grew rapidly.

==Influential articles==
Influential articles that appeared in the journal include [[Gyula O. H. Katona|Katona]]'s elegant proof of the [[Erdős–Ko–Rado theorem]] and a series of papers spanning over 500 pages, appearing from 1983 to 2004, by [[Neil Robertson (mathematician)|Neil Robertson]] and [[Paul Seymour (mathematician)|Paul D. Seymour]] on the topic of [[minor (graph theory)|graph minors]], which together constitute the proof of the [[graph minor theorem]].

A selection of a few influential articles is listed below.

<!--Don't change the title of the following paper!  mis-spelling "Erdös" is how it appeared.-->
* {{cite journal
  | first = G.O.H.
  | last = Katona
  | authorlink = Gyula O. H. Katona
  | title = A simple proof of the Erdös-Chao Ko-Rado theorem
  | journal = Journal of Combinatorial Theory, Series B
  | volume = 13
  | year = 1972
  | issue = 2
  | pages = 183–184
  | doi = 10.1016/0095-8956(72)90054-8
}}
*{{cite journal
| first=Neil
| last=Robertson
|author2=P.D. Seymour
| title=Graph Minors. I. Excluding a forest
| journal=Journal of Combinatorial Theory, Series B
| volume=35
| issue=1
| year=1983
| pages=39–61
| doi=10.1016/0095-8956(83)90079-5
}}
*{{cite journal
| first=Neil
| last=Robertson
|author2=P.D. Seymour
| title=Graph Minors. XX. Wagner's conjecture
| journal=Journal of Combinatorial Theory, Series B
| volume=92
| issue=2
| year=2004
| pages=325–357
| doi=10.1016/j.jctb.2004.08.001
}}

==References==
{{reflist}}

[[Category:Mathematics journals]]
[[Category:Publications established in 1966]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]