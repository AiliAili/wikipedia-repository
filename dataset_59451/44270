{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox military structure
|name = RAF Calshot
|ensign=[[File:Ensign of the Royal Air Force.svg|90px]]
|native_name =
|partof =
|location = Located near [[Calshot]] in [[Hampshire]]
|image =RAF_Calshot.jpg
|caption =Aerial view, late 1920s.
|map_type = Hampshire
|coordinates = {{coord|50.8194|-1.3083|type:landmark_region:GB-HAM|display=inline}}
|map_size =
|map_alt =
|map_caption = Shown within Hampshire
|type = Seaplane and Flying boat station <br> [[Royal Air Force station]]
|code =
|built = {{Start date|df=yes|1913}}
|builder =
|materials =
|height =
|used = April 1918-April 1961
|demolished =
|condition =
|ownership = [[Ministry of Defence (United Kingdom)|Ministry of Defence]]
|open_to_public =
|controlledby = {{air force|United Kingdom}} (1918-61)
|garrison =
|current_commander =
|commanders =
|occupants =
|battles =[[First World War]]<br>[[Second World War]]
|events =
|image2 =
|caption2 =
}}
'''Royal Air Force Calshot''' or more simply '''RAF Calshot''' was initially a [[seaplane]] and [[flying boat]] station, and latterly a [[Royal Air Force]] marine craft maintenance and training unit. It was located at the end of [[Calshot Spit]] in [[Southampton Water]], [[Hampshire]], [[England]], at {{gbmapping|SU487024}}. It was the main [[seaplane]]/[[flying boat]] development and training unit in the [[United Kingdom|UK]], with the landing area sheltered by the mainland, to the west, north and east, and the [[Isle of Wight]], a few miles away to the south on the other side of the [[Solent]], where seaplanes and flying boats were mass-produced by [[Saunders-Roe]]. It closed in 1961. Much of the former base has been preserved, with most of the site now being occupied by the [[RNLI]].

==Origins==
The station was originally established on 29 March 1913 by the [[Royal Flying Corps]] (RFC),<ref name="AoA" >{{cite web
  |url=http://www.rafweb.org/Stations/Stations-C.htm 
  |title=RAF Calshot - Air of Authority, A History of RAF Organisation
}}</ref> as the [[RNAS Calshot|Calshot Naval Air Station]], for the purpose of testing seaplanes for the RFC Naval wing.<ref>{{cite web
  |url=http://daveg4otu.tripod.com/airfields/cal.html 
  |title=Hampshire Airfields 
  |publisher=daveg4otu.tripod.com
}}</ref>  The station was taken over by the [[Royal Naval Air Service]] (RNAS) during the [[World War I|First World War]]<ref>[http://www.fleetairarmarchive.net/History/Index.htm Naval Aviation History & FAA Origins - Fleet Air Arm Archive.]</ref> and finally became a [[Royal Air Force]] station on the merger of the RNAS and the RFC on 1 April 1918.

==1918 to 1939==
[[File:RAF personnel at Calshot in 1936.jpg|thumb|200px|RAF personnel on the beach at Calshot, 1936.]]
[[File:Calshot officer's mess-1.jpg|thumb|200px|Houston House, formerly the officer's mess at RAF Calshot.]]
[[File:Calshot plaque.jpg|thumb|200px|Plaque commemorating Lady Houston's funding for the 1931 RAF Schneider Trophy team.]]

On 8 August 1918 'A' & 'B' Boat Seaplane Training Flights was formed here within 210 Training Depot Station before being disbanded during 1919.{{sfn|Lake|1999|p=9}}{{sfn|Lake|1999|p=35}}

On the formation of the RAF, Calshot retained the two RNAS flights of [[Felixstowe F.2|Felixstowe]] flying boats and the flight of seaplanes, and for a short while the station became the headquarters of [[No. 10 Group RAF]]. After the war, Calshot continued its crew training and aircraft development role, and became home to the ''RAF School of Naval Co-operation and Aerial Navigation''. The station was officially renamed as RAF Calshot on 5 February 1922.<ref name="AoA" />
 
During this time, Calshot also housed a [[reconnaissance]] flight (with Felixstowe flying boats),<ref>{{cite web
  |url=http://www.winkton.net/fonfa%20pages/THE%20AIRFIELDS.htm 
  |title=A Brief History of the New Forest Airfields
  |publisher=Friends of the New Forest Airfields
}}</ref> and a large part of the unit was designated for the training of motorboat crews and other marine craft; the boats mainly used for towing disabled aircraft, collecting bombs from practice runs, and for towing gunnery target boats.<ref name="ASR" >{{cite web
  |url=http://www.raf.mod.uk/history_old/sar601.html 
  |title=The origins of Air/Sea Rescue
  |publisher=Royal Air Force History
}}</ref>

On 24 March 1924, three serving members of the RAF took off from Calshot in a [[Vickers Viking|Vickers Vulture]] amphibious biplane on a planned round-the-world flight. The flight was beset by bad weather and several mishaps, including a crash in [[Sittwe|Akyab]] Harbour, [[Burma]] (requiring a replacement aircraft); and eventually a forced sea landing near [[Bering Island]] ended the attempt.<ref>{{cite web
  |url=http://www.wingnet.org/rtw/rtw001c.htm 
  |title=MacLaren/Plenderieith Round-the-World Flight Attempt
  |publisher=WingNet
}}</ref>

Calshot also became home for the ''[[High Speed Flight RAF|High Speed Flight]]'' as it prepared for the [[Schneider Trophy]] competitions in 1927, 1929 and finally 1931. The 1927 competition was held in [[Venice]] and won for Britain by the Calshot team in a [[Supermarine S.5]], giving Britain the right to host the race in 1929. Calshot was chosen as the venue and an updated S.5 aircraft covered the seven laps at an average speed of 328.63&nbsp;mph to take the prize.<ref name="SLife" >{{cite web
  |url=http://www.southernlife.org.uk/calshot.htm 
  |title=The history of the villages of Hampshire, England, and surrounding counties 
  |publisher=Southern Life (UK)
}}</ref>  Two years later, again at Calshot, a [[Supermarine S.6B]] won the race for Britain for the third time in a row to retain the Trophy outright.<ref name="forest" >{{cite web
  |url=http://website.lineone.net/~forestguide/History/AirfieldsFrameSet.htm 
  |title=The New Forest Guide - Airfields of The New Forest
  |author=Jon Honeysett
}}</ref>

"Aircraftsman Shaw", also known as [[T. E. Lawrence]], or Lawrence of Arabia, was detached to Calshot to help with the 1929 Schneider races. He was seconded to the nearby [[British Power Boat Company]] factory at [[Hythe, Hampshire|Hythe]], and worked closely with the owner, [[Hubert Scott-Paine]], in the development of the 200 Class Seaplane Tender, a {{frac|37|1|2}} foot vessel capable of 27 knots. Deliveries commenced in 1932, and at the time it was claimed to be the fastest craft of their size in the world.<ref name=ASR />

Through the 1930s, Calshot continued its development and training role, and included:<ref name="AoA" />
* Navigation School (until 6 January 1936),
* [[No. 201 Squadron RAF|201 Sqn]] - re-formed on 1 January 1929 with [[Supermarine Southampton]] flying boats, and later with [[Saro London]]s,<ref>{{cite web
  |url=http://www.raf.mod.uk/structure/201squadron.cfm 
  |title=RAF Structure &gt; Squadrons &gt; Maritime Patrol and Search and Rescue &gt; 201 Squadron
}}</ref>
* Seaplane Training Sqn (STC) - re-formed on 1 October 1931,
* [[No. 240 Squadron RAF|240 Sqn]] - re-formed on 30 March 1937 from C Flight of the STC, initially equipped with [[Supermarine Scapa]]s, then [[Short Singapore]]s, and (by July 1939) with [[Saro London]]s,<ref name="240sqn" >{{cite web
  |url=http://www.raf.mod.uk/history_old/h240.html 
  |title=History of No. 240 Squadron - Royal Air Force History
}}</ref>
* Flying Boat Training Sqn - re-formed on 2 January 1939.

==Second World War==
[[File:Calshot Red Plaque-1.JPG|thumb|upright|Transport Heritage Site plaque on the former Sunderland flying boat hangar, now used to house climbing walls and a [[Calshot Activities Centre|velodrome]].]]
Just before the outbreak of [[World War II]], the two operational squadrons left Calshot: 201 Sqn to [[Pembroke Dock]]<ref name=SLife /> and 240 Sqn to [[Invergordon]].<ref name="240sqn" /> Later, in June 1940, the Flying Boat Training Squadron moved away to [[Stranraer]]<ref name="AoA" /> and Calshot became primarily responsible for the repair, maintenance and modification of RAF flying boats, concentrating on the maintenance of [[Short Sunderland]]s. The station also continued to provide marine craft maintenance, and to train boat crews.

Calshot sent five seaplane tenders to help in the [[Dunkirk evacuation|evacuation at Dunkirk]] on 31 May 1940 and three of these tenders carried 500 men to safety, with one of them making a successful second voyage.<ref name="SLife" /> Also in 1940, a small number of ex [[Norway|Norwegian]] [[Heinkel He 115]] flying boats arrived - later to be used for covert operations, landing and collecting agents from occupied Europe.<ref name="forest" />

The station housed several air-see rescue (ASR) units from 1942 onwards. These were used in the [[D-Day]] landings, proving quick support for downed aircraft.<ref name="AoA" />

==1946 to 1961==
Operational squadrons returned to Calshot in March 1946, when No 201 Squadron arrived, to be followed a month later by [[No. 230 Squadron RAF|No 230 Squadron]], both equipped with Sunderlands.<ref name="AoA" /> During the [[Berlin Blockade|Berlin crisis of 1948]], all the Sunderland aircraft from Calshot were called into action and flew over 1,000 sorties to the [[Berlin]] lakes from [[Hamburg]] carrying food into the city and evacuating sick children.<ref name="HCC" >{{cite web
  |url=http://www.hants.gov.uk/scrmxn/c27723.html 
  |title=Report on Calshot Heritage and Visitor Centre
  |date=18 June 1998
  |publisher=Hampshire County Council
}}</ref> On their return from Berlin, both squadrons were transferred to Pembroke.<ref name="SLife" />

After this, although some flying continued, the station was primarily a marine craft station, providing Air Sea Rescue, target-towing and range safety launches, and also included No 238 Maintenance Unit, responsible for the servicing of all RAF marine craft.

A notable event at RAF Calshot was the award of a [[George Medal]] to Corporal Peter (Andy) Anderson 3059642. In February 1950, whilst ferrying crew to their Sunderland flying boat as  Duty Coxswain, an aircraft he had earlier supplied with technicians, burst into flames. This was due to float flares coming into contact with water, as the plane had partially sunk during a storm. Cpl Anderson transferred his remaining aircraft crews and, on passing a fireboat, ordered the burning plane to be sprayed with foam whilst he rescued the two fitters, who were in the burning water holding onto a propeller blade. With his own seaplane tender now on fire, he dropped them off at the jetty to be treated for their injuries. Cpl Anderson was awarded his George Medal at Buckingham Palace on 14 November 1951 by the [[Prince Henry, Duke of Gloucester|Duke of Gloucester]], King George VI being ill at the time.

Two unfinished [[Saunders-Roe Princess]] flying boats, designed to carry 100 passengers and with a range of 5000 miles, were cocooned at RAF Calshot, near the island hangar during the 1950s and 1960s, before being scrapped.<ref name=SLife /> The third one was cocooned at [[Cowes]].

On 1 April 1961, the RAF base finally closed, the marine craft work being transferred to '''[[RAF Mount Batten]]'''.

==Post-RAF==
[[File:Calshot Lifeboat-2.JPG|right|thumb|200px|RNLI Tyne class lifeboat at Calshot Spit mooring]]
In 1964, [[Hampshire County Council]] negotiated a lease with the [[Crown Estate]]s to use the site as an Activities Centre. The first course - a sailing one for Hampshire children - commenced on 18 May 1964.<ref name=HCC />

On 25 July 1970 the [[RNLI]] sent the [[Keith Nelson type lifeboat]] {{RNLB|Ernest William and Elizabeth Ellen Hyde|ON 1017}} to Calshot for evaluation, and in 1971 a permanent [[Calshot Lifeboat Station|station]] was established.<ref name="RNLI" >{{cite web
  |url=http://rnli.org.uk/rnli_near_you/east/stations/CalshotHants/history 
  |title=Calshot History
  |publisher=[[RNLI]]
}}</ref> As of 2010, an RNLI {{Lbc|Tyne}} lifeboat, {{RNLB|Alexander Coutanche|ON 1157}}, was stationed afloat at moorings. In 2012, RNLB ''Alexander Coutanche'' was withdrawn from the station and it became an inshore lifeboat station.
[[File:Calshot Spit showing redundent officers mess and hanger from RAF Calshot.jpg|thumb|200px|View of buildings from RAF Calshot on Calshot Spit, Hampshire]]

In 1981, a [[Short Sandringham]] flying boat, called "The Southern Cross", landed at Calshot after an epic journey across the Atlantic Ocean. This plane is now housed at the [[Solent Sky]] aviation museum in [[Southampton]]. The last flying Sunderland, "Excalibur", came ashore at Calshot in 1984. After an extensive refit, it was flown to [[Florida]] in 1994 to join the extensive collection of [[Kermit Weeks]] at [[Fantasy of Flight]].<ref name="SLife" />

The site is now used as a base for the RNLI and for [[HM Coastguard]], as well as for the Calshot Activities Centre, which runs courses in a number of water and land-based activities, and uses the original [[hangar]]s to house indoor climbing walls, [[artificial ski slopes]] and a [[cycling|cycle track]].

==Trivia==
* Late in life, [[Henry Royce]] signed the visitors' book at RAF Calshot as "F. H. Royce - Mechanic"<ref>{{cite web
  |url=http://www.royce.org.au/fhr_bio.html 
  |title=Sir Henry Royce, Bart., O.B.E. (1863–1933) biography
}}</ref>

==See also==
* [[List of seaplanes and flying boats#United Kingdom|List of seaplanes and flying boats - United Kingdom]]
* [[Seaplane bases in the United Kingdom]]

==References==
===Citations===
{{Reflist}}
===Bibliography===
*{{cite book |last1=Lake |first1=A |title= Flying units of the RAF |year=1999 |publisher= Airlife |location= [[Shrewsbury]] |isbn= 1-84037-086-6 |ref= {{harvid|Lake|1999}} }}

==External links==
* {{cite web
  |url=http://www.bbc.co.uk/ww2peopleswar/stories/13/a4052413.shtml 
  |title=Memories of RAF Calshot - WW2 People's War
  |publisher=BBC
}}
{{Royal Air Force}}

{{DEFAULTSORT:Calshot}}
[[Category:Royal Air Force stations in Hampshire]]
[[Category:Military units and formations established in 1913]]
[[Category:Seaplane bases in the United Kingdom]]
[[Category:Seaplane bases in England]]
[[Category:Military units and formations disestablished in 1961]]
[[Category:1913 establishments in the United Kingdom]]