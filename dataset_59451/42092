<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Starduster
 | image=Stolp Starduster N163C LGB 1971 edited-2.jpg
 | caption=Stolp-Adams Starduster, amateur-built in 1966, at [[Long Beach Airport]], California, in 1971 
}}{{Infobox Aircraft Type
 | type=Single seat sport aircraft
 | national origin=United States
 | manufacturer=[[Stolp Aircraft]]<br />[[Aircraft Spruce and Specialty]]
 | designer=Louis A Stolp and George M Adams
 | first flight=November 1957
 | introduced=
 | retired=
 | status=Plans available (2012)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost=approximately $3600 to build in 1971<ref name=cost/>
 | developed from= 
 | variants with their own articles=[[Stolp Starduster Too]]
}}
|}

The '''Stolp-Adams SA-100 Starduster''' is a U.S. single seat sport [[biplane]] designed to be [[Homebuilt aircraft|built from plans]] supplied by [[Aircraft Spruce and Specialty]]. Though the first flight was in 1957, Stardusters continue to be built and flown.<ref name="WDLA11"/>

==Design and development==
The SA-100 Starduster was designed by Louis A Stolp and George M Adams as a light sports aircraft for [[home-built aircraft|homebuilding]] from plans.  It is a [[wing configuration#Number and position of planes|single bay biplane]] with [[Aircraft fabric covering|fabric covered]], wooden framed [[Stagger (aviation)|staggered]] wings, each pair braced by a single, wide chord [[interplane strut]] aided by [[flying wires]].  A total of eight centre section [[strut]]s join the upper wing to the fuselage, basically two pairs in N-form but with the forward strut doubled. The lower wing is unswept and has 1.5° of [[Dihedral (aircraft)|dihedral]]; the upper wing has 6° of sweep on its leading edge, no dihedral and a greater span.  There are [[ailerons]] on the lower wings only, but no flaps.<ref name=JAWA66/>
  
The [[fuselage]] and tail unit have a fabric covered steel tube structure, with the open cockpit positioned just behind the swept upper wing trailing edge which has a rounded cut-out for upward visibility.  There is a long and prominent faired headrest behind the cockpit, on top of the curved upper fuselage surface.  The Starduster has a conventional tail unit, with a wire braced [[tailplane]] and straight tapered, round topped [[fin]] and [[rudder]], the latter extending to the keel between split [[elevator (aircraft)|elevators]].  Both rudder and elevators are [[balanced rudder|horn balanced]].<ref name=JAWA66/>

The Starduster has a recommended power range of {{convert|125|to|160|hp|kW|0|abbr=on}} and is usually powered by a four-cylinder, horizontally opposed, 125&nbsp;hp (93&nbsp;kW) [[Lycoming O-290|Lycoming O-290-D-1]],<ref name=JAWA66/> though more powerful engines of up to 200&nbsp;hp (150&nbsp;kW) have been fitted.<ref name=Spruce1/>  It has a [[conventional gear|conventional tailwheel undercarriage]].  The mainwheels are mounted on V-struts hinged from the lower fuselage longeron, with rubber shock absorbers on diagonal extension struts between wheel and a short, central, under fuselage V-form mounting bracket.  The main legs are often partially or completely faired and the wheels enclosed in [[spats (aircraft)|spats]].<ref name="WDLA11" /><ref name=JAWA66/>

==Operational history==

Starduster plans remain available more than 50 years after the first flight and homebuilding building continues.<ref name=Spruce2/> A Starduster register<ref name=Spruce3/> currently shows 27 SA-100 Stardusters and 3 SA-101 Super Stardusters built and building.  The FAA register shows 64 SA-100s and 1 SA-101, though not all are assigned and some further Stardusters appear without a type number.<ref name=FAA/>

==Variants==
;SA-100 Starduster
:Original version.

;SA-101 Stolp Super Starduster
:Larger and more powerful - uses the longer wings of the [[Stolp Starduster Too|CA300 Starduster Too]], which have a symmetric M6 [[airfoil]] and no dihedral, together with a 180&nbsp;hp (134&nbsp;kW) [[Lycoming O-360|Lycoming I0-360-A1A]] engine to produce a maximum speed of over 170&nbsp;mph (275&nbsp;km/h).<ref name=Spruce4/>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (SA-100)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1966/7, p.331,<ref name=JAWA66/> Aerofiles<ref name=aeroF/><!-- for giving the reference for the data -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=1
|length m=
|length ft=16
|length in=6
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=19
|upper span in=0
|upper span note=
|lower span m=
|lower span ft=18
|lower span in=0
|lower span note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=6
|height in=0
|height note=
|wing area sqm=
|wing area sqft=110.0
|wing area note=
|aspect ratio=6.33<!-- give where relevant eg sailplanes  -->
|airfoil=NACA 4412
|empty weight kg=
|empty weight lb=700
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=1080
|max takeoff weight note=
|fuel capacity=20 Imp gal (24 US gal, 91 L)
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming O-290|Lycoming O-290-D-1]]
|eng1 type=4-cylinder horizontally opposed air cooled
|eng1 kw=<!-- prop engines -->
|eng1 hp=125<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=

|prop blade number=2<!-- propeller aircraft -->
|prop name=[[Sensenich Propeller|Sensenich]] M74DM61 fixed pitch
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=148
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=130
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=55<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=400
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=
|thrust/weight=

|more performance=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[D'Apuzzo Senior Aero Sport]]
*[[Shober Willie II]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|Stolp Starduster}}
{{reflist|refs=

<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 121. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

<ref name=JAWA66>{{cite book |title= Jane's All the World's Aircraft 1966-67|last= Taylor |first= John W R |coauthors= |edition= |year=1966|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page= }}</ref>

<ref name=cost>{{cite journal|magazine=Air Trails|date=Winter 1971|title=The true cost of building your own plane|author=Leo J. Kohn|page=63}}</ref>

<ref name=aeroF>{{cite web |url=http://www.aerofiles.com/aircraft.html |title=Aerofiles-Stolp |author= |date= |work= |publisher= |accessdate=2011-05-11}}</ref>

<ref name=Spruce1>{{cite web |url=http://starduster.aircraftspruce.com/SA100.html |title= Starduster One SA100|author= |date= |work= |publisher= |accessdate=2011-05-12}}</ref>

<ref name=Spruce2>{{cite web |url=http://www.aircraftspruce.com/menus/kits/starduster.html |title= Kits - Starduster |author= |date= |work= |publisher= |accessdate=2011-05-12}}</ref>

<ref name=Spruce3>{{cite web |url=http://starduster.aircraftspruce.com/registry.html |title= Starduster registry |author= |date= |work= |publisher= |accessdate=2011-05-12}}</ref>

<ref name=Spruce4>{{cite web |url=http://www.aircraftspruce.com/catalog/kitspages/sa101.php |title= SA101 Super Starduster |author= |date= |work= |publisher= |accessdate=2011-05-12}}</ref>

<ref name=FAA>{{cite web |url=http://registry.faa.gov/aircraftinquiry/ |title= FAA register |author= |date= |work= |publisher= |accessdate=2011-05-12}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->
{{Stolp aircraft}}

[[Category:Stolp aircraft|Starduster]]
[[Category:United States sport aircraft 1950–1959]]