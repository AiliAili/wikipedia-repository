<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= DA52 and DA62
 | image=Diamond DA52 prototype maiden flight.png
 | caption=Diamond DA52 prototype on its maiden flight, 3 April 2012, Wiener Neustadt, Austria
}}{{Infobox Aircraft Type
 | type=Twin engine [[light aircraft]]
 | national origin=[[Austria]]
 | manufacturer=[[Diamond Aircraft Industries]]
 | designer=
 | first flight=3 April 2012
 | introduced=October 2015
 | retired=
 | status= In production (DA62)<ref>{{cite web|url=http://www.diamond-air.at/single-engine-aircraft.html|title=Single Engine Aircraft|author=[[Diamond Aircraft Industries]]|work=diamond-air.at|accessdate=7 November 2015}}</ref>
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 2015-
 | number built=25+
 | program cost= <!--Total program cost-->
 | unit cost= €960,000 (US$1.07M), (lower gross weight version)/US$1.25M (increased gross weight version) (2015)<ref name=DiamondReadies/> <br/>US$1.3M (2016, increased gross weight version)<ref name="avweb.com">{{cite web|url=http://www.avweb.com/avwebflash/news/Aero-Diamond-Says-Strong-Demand-for-DA62-226118-1.html|title=Aero: Diamond Says Strong Demand For DA62|work=avweb.com|accessdate=25 April 2016}}</ref>
 | developed from= [[Diamond DA50]]
 | variants with their own articles=
}}
|}
The '''Diamond DA62''' is a five to seven seat, twin-engined [[light aircraft]] produced by [[Diamond Aircraft Industries]] and first announced in March 2012.<ref name="Bertorelli17Mar12">{{cite news|url = http://www.avweb.com/avwebflash/news/DiamondAircraft_AeroFriedrichshafen_DA52_DA42V1_206336-1.html|title = At Aero, More New Stuff From Diamond |accessdate = 14 March 2012|last = Bertorelli|first = Paul|date = 17 March 2012| work = AVweb}}</ref><ref name="Diamond03Apr12">{{cite web|url = http://www.diamond-air.at/news_detail+M5bb6311be29.html|title = Erstflug der neuen DA52 von Diamond Aircraft|accessdate = 5 April 2012|last = [[Diamond Aircraft]]|date = 3 April 2012}}</ref><ref name="Pew20Apr12">{{cite news|url = http://www.avweb.com/avwebflash/news/da52_fligGht_stats_fly_by_wire_control_206566-1.html|title = Diamond Shares DA52 Maiden Flight Stats|accessdate = 23 April 2012|last = Pew|first = Glenn|date = 20 April 2012| work = AVweb}}</ref>

The prototype, designated as the DA52, first flew on 3 April 2012 after six months of development.<ref name="Diamond03Apr12" /><ref name="Bertorelli103Apr12">{{cite news|url = http://www.avweb.com/avwebflash/news/DiamondFliesItsNewDA52_206462-1.html|title = Diamond Flies Its New DA52 |accessdate = 5 April 2012|last = Bertorelli|first = Paul|date = 3 April 2012| work = AVweb}}</ref> In June 2014 it was announced the production aircraft would be designated the DA62.<ref>{{cite web|url=http://www.flightglobal.com/news/articles/diamond39s-top-of-the-range-da52-becomes-da62-400121/|title=Diamond's top-of-the-range DA52 becomes DA62|author=Reed Business Information Limited|work=flightglobal.com|accessdate=27 April 2015}}</ref><ref name="Staff014Jun14">{{cite news|url = http://www.avweb.com/avwebflash/news/Diamond-Renames-the-DA52-the-DA62222112-1.html|title = Diamond Renames The DA52 The DA62|accessdate = 6 June 2014|last = AVweb Staff|date = 4 June 2014| work = AVWeb}}</ref>

==Design and development==
The DA62 development team is headed by Diamond managing director Manfred Zipper. It is based upon the [[fuselage]] of the single-engine [[Diamond DA50]], but with two [[Austro AE300]] diesel engines burning [[Jet-A]] fuel. Company CEO Christian Dries indicated that the engines may be replaced with [[turboprop]]s.<ref name="Bertorelli17Mar12" /><ref name="Bertorelli103Apr12" />

In flying the prototype from Diamond's [[Wiener Neustadt]] plant to the 2012 [[AERO Friedrichshafen]] aviation trade show, the aircraft achieved 16.6 mpg fuel efficiency, the result of improvements in cooling drag and aerodynamic drag made during its development.<ref name="Bertorelli18Apr12">{{cite news|url = http://www.avweb.com/avwebbiz/news/Diamond_DA52_Centerpiece_206542-1.html|title = Diamond's DA52 A Centerpiece|accessdate = 19 April 2012|last = Bertorelli|first = Paul|date = 18 April 2012| work = AVweb}}</ref>

The company originally intended to have the aircraft available for sale in July 2013 and expected to offer [[fly-by-wire]] controls as an option by 2014, but development was delayed and those dates were not met.<ref name="Diamond03Apr12" /><ref name="Pew20Apr12" /> The DA62 was [[European Aviation Safety Agency]] (EASA)-certified on 16 April 2015.<ref name=avw2015-04>{{cite web |first=Mary |last=Grady |url=http://www.avweb.com/avwebflash/news/Diamond-DA62-Twin-Now-EASA-Certified-223874-1.html |title=Diamond DA62 Twin Now EASA-Certified |work=avweb.com|agency=Aviation Publishing Group |date=16 April 2015 |accessdate=17 April 2015 }}</ref><ref>{{cite web|url=http://www.diamond-air.at/media-center/press-releases/news/article/diamond-da62-receives-easa-certification.html|title=Diamond Aircraft DA62 receives EASA Certification|work=[[Diamond Aircraft]]|accessdate=27 April 2015}}</ref> By September 2015, the company was preparing to deliver the first production DA62s to customers the following month and was manufacturing the first aircraft destined for the US market &ndash; the tenth DA52/DA62 to be built and the third production aircraft &ndash; for an appearance at that year's [[National Business Aviation Association]] Convention in November.<ref name=DiamondReadies>{{cite web|last=Sarsfield |first=Kate |url=https://www.flightglobal.com/news/articles/diamond-readies-first-da62-piston-twins-for-delivery-417057/ |title=Diamond readies first DA62 piston twins for delivery |website=FlightGlobal |publisher=Reed Business Information |date=22 September 2015 |access-date=3 August 2016}}</ref> American [[Federal Aviation Administration]] (FAA) certification was received on 23 February 2016<ref name=":0">{{Cite web
| url = http://www.flyingmag.com/new-diamond-twin-snags-faa-certification
| title = New Diamond Twin Snags FAA Certification
| website = Flying Magazine
| access-date = 2016-02-26
}}</ref> <ref>"Diamond DA62 FAA Certified!" ''Diamond Aircraft Industries'' Retrieved 2016-2-26</ref>  The FAA certification came ten months after EASA certification.<ref name=":0" /> At the 2016 AERO Friedrichshafen show, Diamond's CEO Christian Dries reported that production would be increased to 60-62 aircraft a year to meet strong demand.<ref name="avweb.com"/>

The aircraft is available in two weight versions. The "European" version has five seats and a [[maximum takeoff weight]] (MTOW) of {{convert|1999|kg|0}}, the "US" version has seven seats and a MTOW of {{convert|2300|kg|0}}.<ref name=WeFly>{{Cite web|last=Pope |first=Stephen |url=http://www.flyingmag.com/we-fly-diamond-da62 |title=We Fly: Diamond DA62 |website=Flying Magazine |date=3 December 2015 |access-date=3 August 2016}}</ref><ref>{{cite web|url=https://www.easa.europa.eu/system/files/dfu/EASA-TCDS_DA42_A005%20issue%2031.pdf |title=EASA Type Certificate Data Sheet No. EASA.A.005: {{sic|nolink=y|Diamond DA 42 and variant}}; For models: {{sic|nolink=y|DA 42, DA 42 M, DA 42 NG, DA 42 M-NG, DA 62}}; Issue 31 |date=1 July 2016 |access-date=3 August 2016}}</ref> The lower MTOW of the "European" version is to allow operators to avoid higher weight-based [[air traffic control]] user charges.<ref name=DiamondReadies/> The third row of seating and increased MTOW of the "US" version are available as factory options at extra cost.<ref name=WeFly/> At the 2016 AERO Friedrichshafen, Christian Dries said a special version with an additional baggage belly pod was under consideration for the [[air charter]] market.<ref name="avweb.com"/>

==Variants==
;DA52
:Prototype, two built.
;DA62
:Five-seven seat production variant with an extra third window and larger horizontal stabilizer.<ref name="Staff014Jun14" /><ref>{{cite web|url=http://www.flyingmag.com/aircraft/pistons/five-seat-diamond-da-62-twin-revealed|title=Five-Seat Diamond DA-62 Twin Revealed|date=1 March 2015}}</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (DA62) ==
{{Aircraft specs
|ref=Air International<ref name="ai0815 p72">Unwin 2015, p. 72.</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=up to six passengers
|length m=9.19
|length ft=
|length in=
|length note=
|span m=14.7
|span ft=
|span in=
|span note=
|height m=2.28
|height ft=
|height in=
|height note=
|wing area sqm=17.10
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=1570
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=2300
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|326|L}}
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Austro AE300]]
|eng1 type=diesel [[aircraft engine]]s
|eng1 kw=134<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=
|never exceed speed kmh=379
|never exceed speed note=
|max speed kmh=367
|max speed mph=
|max speed kts=
|max speed note=([[True airspeed|True Air Speed]] (TAS)<ref name="DA62spec">{{cite web|title=DA62 - More of the Best|publisher=Diamond Aircraft|accessdate=25 July 2015|url=http://www.diamond-air.at/twin-engine-aircraft/da62.html}}</ref>
|cruise speed kmh=325
|cruise speed mph=
|cruise speed kts=
|cruise speed note=(TAS)
|stall speed kmh=125<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=2380
|range miles=
|range nmi=
|range note=<ref name="DA62spec"/>
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=20000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1200
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

*{{cite magazine|last=Unwin|first=Dave|title=An Austrian Gem|magazine=[[Air International]]|date=August 2015|volume=89|issue=2|pages=68–73|issn=0306-5634}}

==External links==
*{{Official website|http://www.diamond-air.at/da52-vii.html}}
*[http://www.diamond-air.at/2614+M52087573ab0.html Official company first flight video]
*[https://www.youtube.com/watch?v=JR4o2Qr_SZM AVWeb video about DA52 development shot at Aero 2012]
*[https://www.youtube.com/watch?v=hksiq2Hw4d0 AVWeb video about the DA52 from May 2013]
*[https://www.youtube.com/watch?v=kmz67HAyvbs AVWeb video about the DA62 from NBAA in November 2015]
*[http://www.flyingmag.com/we-fly-diamond-da62 We Fly: Diamond DA62] (Pilot Report)

{{Diamond Aircraft}}

[[Category:Austrian civil utility aircraft 2010–2019]]
[[Category:Diamond aircraft|DA52]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:T-tail aircraft]]