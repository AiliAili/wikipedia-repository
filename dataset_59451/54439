{{Use Australian English|date=September 2016}}
{{Use dmy dates|date=December 2014}}
{{Infobox Politician
| honorific-prefix =
| name             = Dr Andrew Southcott
| honorific-suffix = 
| image            = Andrew_Southcott.jpg
| constituency_MP  = [[Division of Boothby|Boothby]] 
| parliament       = Australian
| term_start       = 2 March 1996
| term_end         = 9 May 2016
| predecessor      = [[Steele Hall]]
| successor        = [[Nicolle Flint]]
| majority         =
| birth_date       = {{birth date and age|df=yes|1967|10|15}}
| birth_place      = Panorama
| death_date       =
| death_place      =
| party            = [[Liberal Party of Australia]]
| relations        =
| spouse           = Kate Southcott (née Simpson)
| children         = 2
| residence        =
| occupation       = Medical practitioner
| alma_mater       = [[Flinders University]]<br />[[University of Adelaide]]
| religion         = [[Anglican]]
| signature        =
| website          = [http://www.andrewsouthcott.com.au/ AndrewSouthcott.com.au]
| footnotes        =
}}

'''Andrew John Southcott''' (born 15 October 1967) is an Australian politician and medical practitioner. He was the [[Liberal Party of Australia|Liberal]] member for the [[Australian House of Representatives|House of Representatives]] seat of [[Division of Boothby|Boothby]] from the [[Australian federal election, 1996|1996 election]] until he stood down at the [[Australian federal election, 2016|2016 election]].

==Early life==
Southcott was born in [[Panorama, South Australia]], and attended Paringa Park Primary School, and [[St Peter's College, Adelaide|St Peter's College]].

He studied medicine at [[University of Adelaide]] and was a medical practitioner before entering politics, serving as an intern and surgical trainee at the [[Royal Adelaide Hospital]] and as a surgical registrar at the [[Flinders Medical Centre]] and [[Repatriation General Hospital, Daw Park]]. While in office he has completed a [[Bachelor of Economics]] at [[Flinders University]] and a [[Masters of Business Administration]] at the [[University of Adelaide]].<ref>http://www.andrewsouthcott.com/Pages/AboutAndrew/aboutAndrewSouthcott.aspx</ref>

==Political career==
Southcott joined the party in 1989 while serving as president of the Adelaide Medical Students Society. He defeated Senate leader and future Defence Minister [[Robert Hill (Australian politician)|Robert Hill]], a moderate, in a pre-selection battle for the seat of [[Division of Boothby|Boothby]] in 1994. Southcott is from the conservative faction of the [[Liberal Party of Australia]].<ref>{{cite journal |last= Parkin |first= Andrew |date=June 1998 |title= Australian Political Chronicle: July–December 1997 |journal= Australian Journal of Politics and History| volume = 44| issue =2|pages= 286–287 |issn=0004-9522 |doi=10.1111/1467-8497.00019}}</ref>

Southcott was elected to the [[Australian House of Representatives|House of Representatives]] seat of [[Division of Boothby|Boothby]] at the [[Australian federal election, 1996|1996 election]] and remained a [[backbencher]] throughout the [[Howard Government]].

{|class=wikitable
|-align=right
| Election in [[Division of Boothby|Boothby]] || [[Australian federal election, 1996|1996]]|| [[Australian federal election, 1998|1998]]|| [[Australian federal election, 2001|2001]]|| [[Australian federal election, 2004|2004]]|| [[Australian federal election, 2007|2007]]|| [[Australian federal election, 2010|2010]]|| [[Australian federal election, 2013|2013]]
|-align=right
| [[First-preference votes|First preference]] % ||53.4||48.2||47.9||50.6||46.3||44.8||51.1
|-align=right
| [[Two-party-preferred vote|Two-party-preferred]] % ||bgcolor="#61c3ff"|'''61.6'''||bgcolor="#80d8f9"|'''57.5'''||bgcolor="#80d8f9"|'''57.4'''||bgcolor="#a6e7ff"|'''55.4'''||bgcolor="#a6e7ff"|'''52.9'''||bgcolor="#a6e7ff"|'''50.8'''||bgcolor="#80d8f9"|'''57.7'''
|}

At the [[Australian federal election, 2004|2004 election]], despite a solid national two-party swing and vote to the Liberals, [[Division of Boothby|Boothby]] became a marginal Liberal seat for the first time in over half a century, with [[Australian Labor Party|Labor]]'s [[Chloë Fox]] reducing the Liberals to a marginal 5.4 percent two-party margin. Labor's [[Nicole Cornes]] reduced the Liberals to a marginal 2.9 percent two-party margin at the [[Australian federal election, 2007|2007 election]], while at the [[Australian federal election, 2010|2010 election]] Labor's [[Annabel Digance]] reduced the Liberals to just a 0.75 percent two-party margin (638 votes), which put Boothby ahead of neighbouring [[Division of Sturt|Sturt]] as the most marginal seat in South Australia. However, Boothby became a fairly safe Liberal seat again at the [[Australian federal election, 2013|2013 election]].

Southcott was a supporter of [[Republicanism in Australia]].<ref>[http://www.abc.net.au/4corners/stories/s72500.htm Four Corners - 10/27/1997: Battle Royal. Australian Broadcasting Corp<!-- Bot generated title -->]</ref>

Southcott was chair of the Parliament's Joint Standing Committee on Treaties from 2003 until 2007.<ref>[http://www.theage.com.au/news/national/indonesian-threat-to-block-envoy/2005/07/16/1121455936361.html Indonesian threat to block envoy - National - theage.com.au<!-- Bot generated title -->]</ref>

Following the Coalition's defeat at the [[Australian federal election, 2007|2007 election]], Southcott was appointed by party leader [[Brendan Nelson]] to the Outer Shadow Ministry as Shadow Minister for Employment Participation, and Apprenticeships and Training.

Resulting from the ascendancy of [[Malcolm Turnbull]] at the [[Liberal Party of Australia leadership spill, 2008|2008 Liberal leadership ballot]], Southcott became Shadow Minister for Employment Participation, Training and Sport.<ref>Coalition Shadow Ministry as at 22 September 2008, http://www.liberal.org.au/documents/CoaShadMinList.pdf</ref>

After the ascendancy of [[Tony Abbott]] at the [[Liberal Party of Australia leadership spill, 2009|2009 Liberal leadership ballot]], Southcott was dropped from the Outer Shadow Ministry and demoted to Shadow Parliamentary Secretary for Regional Health Services, Health and Wellbeing.<ref>{{cite web|url=http://www.aph.gov.au/house/members/biography.asp?id%3DTK6 |title=Archived copy |accessdate=2010-08-12 |deadurl=yes |archiveurl=https://web.archive.org/web/20100708003136/http://www.aph.gov.au/house/members/biography.asp?id=TK6 |archivedate=8 July 2010 |df=dmy }}</ref> Following the [[Australian federal election, 2010|2010 election]], Southcott was appointed Shadow Parliamentary Secretary for Primary Healthcare.<ref>http://www.aph.gov.au/Library/parl/43/Shadow/index.htm</ref>

Resulting from the [[Australian federal election, 2013|2013 election]] where the [[Abbott Government]] took office, Southcott was dropped and not included in the incoming Abbott ministry, returning to the [[backbencher|backbench]] where he remained.

Following the resignation of [[Bronwyn Bishop]], Southcott ran for the position of [[Speaker of the Australian House of Representatives]] in August 2015 but was unsuccessful.<ref>[http://www.abc.net.au/news/2015-08-08/speakership-candidates-profiles/6682002 Meet the candidates vying for the big chair: ABC 8 August 2015]</ref><ref>[http://www.afr.com/news/politics/tony-smith-replaces-bronwyn-bishop-as-speaker-20150809-giv9se Tony Smith replaces Bronwyn Bishop as Speaker: Financial Review 10 August 2015]</ref>

===Parliamentary resignation===
On 4 September 2015, Southcott announced his parliamentary retirement as of the [[Australian federal election, 2016|2016 election]], and would resume his medical career.<ref name=southcott>{{cite news|title=Liberal MP Andrew Southcott to stand down at next election|url=https://www.theguardian.com/australia-news/2015/sep/04/liberal-mp-andrew-southcott-to-stand-down-at-nextt-election|accessdate=4 September 2015|work=The Guardian|date=4 September 2015}}</ref> Southcott said his decision not to re-contest [[Division of Boothby|Boothby]] was "completely unrelated" to being unsuccessful in running for the Speakership.<ref>[http://www.abc.net.au/news/2015-09-04/southcott-not-standing-at-next-federal-election-boothby/6748814 Veteran South Australian MP Andrew Southcott won't stand at next election: ABC 4 September 2015]</ref> The Liberals pre-selected doctoral student and newspaper columnist [[Nicolle Flint]] as their candidate in Boothby at the [[Australian federal election, 2016|2016 election]].<ref>[http://www.abc.net.au/news/2015-11-01/liberals-announce-nicolle-flint-as-its-boothby-candidate/6902666 Liberals announce Nicolle Flint as Boothby candidate in SA to replace veteran Andrew Southcott: ABC 1 November 2015]</ref>

==References==
{{reflist|30em}}

==External links==
*[http://www.andrewsouthcott.com/ Personal website]
*[http://www.aph.gov.au/A_Southcott_MP Parliamentary Biography]

{{s-start}}
{{s-par|au}}
{{s-bef|before=[[Steele Hall]]}}
{{s-ttl|title=[[Division of Boothby|Member for Boothby]]|years=1996–2016}}
{{s-aft|after=[[Nicolle Flint]]}}
{{s-end}}

{{DEFAULTSORT:Southcott, Andrew}}
[[Category:People from Adelaide]]
[[Category:1967 births]]
[[Category:Living people]]
[[Category:Liberal Party of Australia members of the Parliament of Australia]]
[[Category:Members of the Australian House of Representatives]]
[[Category:Members of the Australian House of Representatives for Boothby]]
[[Category:University of Adelaide Medical School alumni]]
[[Category:People educated at St Peter's College, Adelaide]]
[[Category:Flinders University alumni]]
[[Category:21st-century Australian politicians]]
[[Category:20th-century Australian politicians]]