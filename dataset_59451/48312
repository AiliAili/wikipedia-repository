{{COI|date=April 2015}}
{{Taxobox 
| status = LC
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN | id = 191208 | taxon =''Brevoortia patronus''| assessor =NatureServe | assessment_year =2015 | version =4.1 | criteria-version =4.1 | accessdate =February 25, 2016 }}</ref>
| image = B.patronus.JPG
| image_width = 240px
| image_caption = Gulf menhaden (''Brevoortia patronus''), captured in Galveston Bay, TX
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Actinopterygii]]
| ordo = [[Clupeiformes]]
| familia  = [[Clupeidae]]
| subfamilia = [[Alosinae]]
| genus = [[Brevoortia]]
| species = '''''B. patronus'''''
| binomial = ''Brevoortia patronus''
| binomial_authority = [[George Brown Goode|Goode]], 1878
}}
The '''Gulf menhaden''' (''Brevoortia patronus'') is a small marine [[filter feeding|filter-feeding]] fish belonging to the family [[Clupeidae]].  The range of Gulf menhaden encompasses the entirety of the [[Gulf of Mexico]] nearshore waters, with the exception of the extreme eastern [[Yucatan]] and western [[Cuba]].<ref name="FAO">FAO 2002. The living marine resources of the western central Atlantic. ASIH special publication No. 5, Kent E. Carpenter, ed.  ISSN 1020-6868.</ref>  Evidence from morphology <ref name="Dahlberg">Dahlberg, M.D.  1970.  Atlantic and Gulf of Mexico menhadens, genus Brevoortia (Pisces:Clupeidae).  Bulletin of the Florida State Museum, Biological Sciences 15:91-162.</ref> and DNA analyses <ref>Anderson, J.D.  2007.  Systematics of the North American menhadens: molecular evolutionary reconstructions in the genus Brevoortia (Clupeiformes: Clupeidae).  Fishery Bulletin 205:368-378.</ref> suggest that the Gulf menhaden is the Gulf of Mexico complement to the Atlantic menhaden (''Brevoortia tyrannus'').  Both species support large commercial reduction fisheries,<ref>Vaughan, D.S. and C. Strobeck. 1998. Assessment and management of Atlantic and Gulf menhaden stocks. Marine Fishery Review 53, 47-55.</ref> with Gulf menhaden supporting the second largest fishery, by weight, in the United States.<ref name="Pritchard">Pritchard, E.S. 2005. Fisheries of the United States 2004. Silver Spring, MD: National Marine Fisheries Service, Office of Science and Technology, pp. 1-19.</ref>

== Range and distribution ==
The Gulf menhaden occurs throughout the Gulf of Mexico, but its distribution is patchy.  The center of distribution of the species appears to be the northwest/northcentral Gulf,<ref name="FAO"/> particularly in [[Louisiana]] and [[Texas]] where populations are very large and numerous.  In the southern Gulf of Mexico the range of Gulf menhaden overlaps that of the closely related finescale menhaden (''Brevoortia gunteri''),<ref>Anderson, J.D. and D.L. McDonald. 2007. Morphological and genetic investigations of two western Gulf of Mexico menhadens (Brevoortia spp.). Journal of Fish Biology 70a:139-147.</ref> and there is evidence for resource partitioning (a process whereby closely related species occurring in close proximity results in subtle differences in ecological niches) between these species.<ref name="Castillo">Castillo-Revera, M., A. Kobelkowsky and V. Zamayoa. 1996. Food resource partitioning and trophic morphology of Brevoortia gunteri and B. patronus. Journal of Fish Biology 49:1102-1111.</ref>  In the eastern Gulf, the range of Gulf menhaden overlaps that of the [[yellowfin menhaden]] (''Brevoortia smithi''), and hybridization between these species has been demonstrated using morphological <ref name="Dahlberg"/> and DNA evidence.<ref name="AndersonAndKarel">Anderson, J.D. and W.J. Karel. 2007. Genetic evidence for asymmetric hybridization between menhadens (Brevoortia spp.) from peninsular Florida. Journal of Fish Biology 71b:235-249.</ref>  Gulf menhaden also may have a presence on the southern Atlantic coast of Florida,<ref name="AndersonAndKarel"/><ref>Reintjes, J.W. 1959. Continuous distribution of menhaden along the south Atlantic and Gulf coasts of the United States. Proceedings of the Gulf and Caribbean Fisheries Institute 12, 31-35.</ref> although this finding is based primarily upon DNA evidence.

== General biology ==
Gulf menhaden are commonly 8 inches in length but can reach 12 inches. Gulf menhaden are a dull silver with a greenish back. Like their Atlantic counterpart, Gulf menhaden have a prominent black spot found behind the gill cover followed by a row of smaller spots <ref>[http://www.seagrantfish.lsu.edu/biological/misc/gulfmenhaden.htm Louisiana Fisheries]</ref>

===Diet===
Gulf menhaden are filter feeders, meaning that they collect food by filtering water through modifications of the branchial apparatus (gill or [[branchial arch]]es and [[gill raker]]s). Like [[Atlantic menhaden]], Gulf menhaden’s diet depends on the size of their gill rakers, which change as menhaden age. When the rakers are smaller, which generally correspond to when they are under the age of 1, Gulf menhaden feed primarily on [[phytoplankton]]. As they age and their gill rakers grow larger, menhaden shift their diet to primarily consume [[zooplankton]].<ref name="menhaden.gsmfc.org">[http://menhaden.gsmfc.org/2010%20FAQ.shtm “Menhaden Facts.” Gulf States Marine Fisheries Commission.]</ref>

===Life cycle===

Spawning occurs offshore in winter (October–March).<ref name="FAO"/>  Eggs and larvae are [[pelagic]] and are carried into estuarine nursery areas via prevailing currents.  As a result, migration at this stage can be lengthy, and populations of Gulf menhaden throughout the Gulf of Mexico are generally thought to comprise a single genetic stock.<ref>Anderson, J.D.  2006.  Conservation Genetics of Gulf Menhaden (Brevoortia patronus): Implications for the Management of a Critical Forage Component for Texas Coastal Gamefish Ecology.  Federal Aid in Sportfish Restoration Act Technical Series, F-144-R.</ref>

== Commercial fishery ==
The Gulf menhaden fishery is one of the largest in the United States. In 2013, the fishery supported four of the nation’s top ten ports by volume of landings.<ref>[http://www.st.nmfs.noaa.gov/Assets/commercial/fus/fus13/FUS2013.pdf National Marine Fisheries Service Office of Science and Technology, “Fisheries of the United States 2013,” September 2014]</ref>  Gulf menhaden are harvested primarily for fish meal and fish oil based products. A much smaller number of menhaden are caught for use as bait. In addition to being one of the largest fisheries in the US, the Gulf menhaden fishery has also been recognized internationally for its sustainability.

The fishery’s sustainability has also been certified by independent organizations. [[Friend of the Sea]], an international seafood sustainability certification program, has recognized both the [[Atlantic menhaden]] and Gulf menhaden fisheries as sustainable.<ref>[http://www.friendofthesea.org/fisheries.asp?ID=124 Friend of the Sea, “Fisheries USA – Purse seine – Gulf menhaden, Atlantic menhaden – FAO Western Atlantic Ocean”]</ref> This is both due to the healthy status of the stock as well as the fishery’s low levels of bycatch, which it achieves with the use of purse seine nets.<ref>[http://www.friendofthesea.org/fisheries.asp?ID=26 Friend of the Sea, “Omega Protein’s Purse Seine Menhaden fleet”]</ref>

==Management==
Gulf menhaden are managed by an interstate compact called the [[Gulf States Marine Fisheries Commission]] (GSMFC). According to the most recent 2013 stock assessment by the GSMFC, Gulf menhaden are “neither overfished nor experiencing overfishing.”<ref>[http://www.gsmfc.org/pubs/SEDAR32A%20PRESS%20RELEASE.pdf Gulf States Marine Fisheries Commission, “Stock Assessment Report for Gulf of Mexico Gulf Menhaden (SEDAR32A) and Adoption of Reference Points,” November 14, 2013]</ref> According to the GSMFC, “the Gulf menhaden fishery is probably the most closely monitored and managed fishery in the Gulf of Mexico.”<ref name="menhaden.gsmfc.org"/> An example of the fishery’s monitoring is the Gulf of Mexico purse seine fishery’s participation in NOAA’s Southeast Fisheries Observer Program since 2011.<ref>[NOAA, “National Observer Program, FY 2011 Annual Report”]</ref> Specifically, the Menhaden Advisory Committee (MAC) is the GSMFC subcommittee that oversees menhaden management.

==Dead zones==
According to the GSMFC, “Menhaden do not have the capacity to reduce unwanted phytoplankton blooms that arise from manmade sources, primarily because they eat mostly zooplankton. In addition, menhaden excrete large amounts of ammonia (a nitrogenous product), contributing to an already high nitrogen load.”<ref name="menhaden.gsmfc.org"/> In addition, the commercial menhaden fishery only targets adult menhaden, which consume zooplankton, not juvenile menhaden, which do consume phytoplankton  <ref>[http://www.sefsc.noaa.gov/sedar/download/Brief%20History%20of%20the%20Gulf%20Menhaden%20Fishery.pdf?id=DOCUMENT NOAA, “Brief History of the Gulf Menhaden Purse-Seine Fishery”]</ref>

== Sources ==
<references/>

== External links ==
*[http://menhaden.gsmfc.org/ Gulf States Marine Fisheries Commission], menhaden page

[[Category:Alosinae]]