<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name=VS-300
 |image=Sikorsky vs-300.jpg
 |caption=[[Igor Sikorsky]] in the VS-300, at the end of 1941
}}{{Infobox aircraft type
 |type=[[Experimental aircraft|Experimental]]
 |manufacturer=[[Vought-Sikorsky]]
 |designer=[[Igor Sikorsky]]
 |first flight=14 September 1939 <ref name= "Chiles"/>
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=
 |unit cost=
 |variants with their own articles=[[Sikorsky R-4]]
}}
|}

The '''Vought-Sikorsky VS-300''' (or '''S-46''') was a single-engine [[helicopter]] designed by [[Igor Sikorsky]]. It had a single three-blade rotor originally powered by a 75 [[horsepower]] (56&nbsp;[[Watt#Kilowatt|kW]]) engine. The first "free" flight of the VS-300 was on 13 May 1940.<ref name="munson">Munson 1968, p. 111.</ref> The VS-300 was the first successful single lifting rotor helicopter in the United States and the first successful helicopter to use a single vertical-plane [[tail rotor]] configuration for antitorque. With [[Floats (nautical)|floats]] attached, it became the first practical [[amphibious helicopter]].

==Design and development==
Igor Sikorsky's quest for a practical helicopter began in 1938, when as the Engineering Manager of the Vought-Sikorsky Division of United Aircraft Corporation, he was able to convince the directors of United Aircraft that his years of study and research into rotary-wing flight problems would lead to a breakthrough.  His first experimental machine, the VS-300, was test flown by Sikorsky on 14 September 1939, [[tether]]ed by cables.<ref>{{cite journal|magazine=AOPA Pilot|date=September 2014|title=75 Years Ago|page=28}}</ref> In developing the concept of rotary-wing flight, Sikorsky was the first to introduce a single engine to power both the main and tail rotor systems. The only previous successful attempt at a single-lift rotor helicopter, the Yuriev-Cheremukhin [[TsAGI]]-1EA in 1931 in the Soviet Union, used a pair of uprated, Russian-built [[Gnome Monosoupape]] rotary engines of 120&nbsp;hp each for its power.<ref>[https://www.youtube.com/watch?v=rx565dqF-5M video].</ref><ref>Savine, Alexandre. [http://www.ctrl-c.liu.se/misc/ram/1-ea.html "TsAGI 1-EA."] ''ctrl-c.liu.se,'' 24 March 1997. Retrieved 12 December 2010.</ref> For later flights of his VS-300, Sikorsky also added a vertical [[airfoil]] surface to the end of the tail to assist anti-[[torque]] but this was later removed when it proved to be ineffective.<ref name="Helicopter World"/>

The cyclic control was found to be difficult to perfect, and led to Sikorsky locking the cyclic and adding two smaller vertical-axis lifting rotors to either side aft of the tailboom.<ref>Dorr 2005, p. 32.</ref> By varying pitch of these rotors simultaneously, fore and aft control was provided.  Roll control was provided by differential pitching of the blades.  In this configuration, it was found that the VS-300 could not fly forward easily and Sikorsky joked about turning the pilot's seat around.<ref name="Helicopter World">[http://www.sections.asme.org/Fairfield/Sikorsky%20VS-300%20Helicopter.pdf "A National Historic Engineering Landmark: VS-300 Helicopter (1939)."] Crystal City, Virginia: ''The American Society of Mechanical Engineers, American Helicopter Society,'' 17 May 1984.</ref>

==Operational history==
Sikorsky fitted utility floats (also called pontoons) to the VS-300 and performed a water landing and takeoff on 17 April 1941, making it the first practical [[amphibious helicopter]].<ref name=SikorskyTimeline>[http://www.sikorsky.com/vgn-ext-templating-SIK/v/index.jsp?vgnextoid=208ae39d40a78110VgnVCM1000001382000aRCRD "Timeline."] ''Sikorsky.com.'' Retrieved: 22 September 2009.</ref> On 6 May 1941, the VS-300 beat the world endurance record held by the [[Focke-Wulf Fw 61]], by staying aloft for one hour 32 minutes and 26.1 seconds.<ref name="munson"/>

The final variant of the VS-300 was powered by a 150&nbsp;hp [[Franklin Engine Company|Franklin]] engine.<ref>Munson 1985, p. 51.</ref> The VS-300 was one of the first helicopters capable of carrying cargo.  The VS-300 was modified over a two-year period, including removal of the two vertical tail rotors, until 1941 when a new cyclic control system gave it much improved flight behavior.<ref name= "Chiles">Chiles 2008, p. 104.</ref>

==Survivor==
In 1943, the VS-300 was retired to the [[The Henry Ford|Henry Ford Museum]] in Dearborn, Michigan.  It has been on display there ever since, except for a trip back to the Sikorsky Aircraft plant for restoration in 1985.{{citation needed|date=August 2014}}

==Specifications (VS-300)==
{{Aircraft specs
|ref=<ref name= "Chiles"/>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=28
|length in=0
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=10
|height in=0
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=1150
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Franklin 4AC-199-E]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=90
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=at 2,500 rpm
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|rot number=1
|rot dia m=
|rot dia ft=30
|rot dia in=0
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=50
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=75
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance= 1 hour 30 minutes 
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=
* [[Sikorsky R-4]]
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
;Notes
{{Reflist}}
;Bibliography
{{Refbegin}}
* Chiles, James R. ''The God Machine: From Boomerangs to Black Hawks: The Story of the Helicopter.'' New York: Bantam, 2008. ISBN 978-0-553-38352-2.
* Dorr, Robert F. ''Chopper: A History of America Military Helicopter Operations from WWII to the War on Terror.'' New York: Penguin Books, 2005. ISBN 0-425-20273-9.
* Munson, Kenneth. ''Helicopters and Other Rotorcraft Since 1907''. London: Blandford, 1968. ISBN 0-7137-0610-4.
* Munson, Kenneth. ''US Warbirds, From World War 1 to Vietnam''. New York: New Orchard, 1985. ISBN 978-1-85079-029-7.
* Sikorsky, I. I. [http://www.flightglobal.com/pdfarchive/view/1942/1942%20-%201857.html "Development of the VS-300 Helicopter (A paper read at the Rotating Wing Aircraft Session of the Tenth Annual Meeting of the Institute of Aeronautical Sciences by Sikorsky, I. I, Engineering Manager, Vought-Sikorsky Division of the United Aircraft Corporation)."] ''Flight,'' 3 September 1942.
{{Refend}}

==External links==
{{commons category|Vought-Sikorsky VS-300}}
*[https://books.google.com/books?id=vdkDAAAAMBAJ&pg=PA380&dq=defiant&hl=en&ei=Aud5TOmcGoqonQehs9mWCw&sa=X&oi=book_result&ct=result&resnum=10&ved=0CFUQ6AEwCTgK#v=onepage&q=defiant&f=true "Wingless Helicopter Flies Straight Up", ''[[Popular Mechanics]],'' September 1940 article showing Sikorsky flying his first helicopter]
*[http://www.hfmgv.org/exhibits/heroes/inventors/sikorsky.asp Heroes of the Sky: VS300 exhibit at the [[Henry Ford Museum]]]

{{Sikorsky Aircraft}}

{{Authority control}}
{{DEFAULTSORT:Vought-Sikorsky Vs-300}}
[[Category:United States experimental aircraft 1940–1949]]
[[Category:Amphibious helicopters]]
[[Category:Sikorsky aircraft|VS-300]]
[[Category:United States helicopters 1940–1949]]
[[Category:Single-engined piston helicopters]]
[[Category:Individual aircraft]]