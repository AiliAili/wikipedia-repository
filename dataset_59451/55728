{{Other uses|Ashgate (disambiguation)}}
{{Infobox publisher
| image = 
| parent = [[Informa]] ([[Taylor & Francis]])
| status = 
| founded = 1967
| founder = 
| successor = 
| country = United Kingdom
| headquarters = [[Farnham]]
| distribution = 
| keypeople = 
| publications = [[Book]]s, [[academic journal]]s
| topics = 
| genre = 
| imprints = Gower Publishing, Lund Humphries
| revenue = 
| numemployees = 
| nasdaq = 
| url = {{URL|http://www.ashgate.com}}
}}
'''Ashgate Publishing''' was an academic book and journal publisher based in [[Farnham]] ([[Surrey]], United Kingdom).<ref>{{cite web |url=http://www.booknews.co.uk/Publishers/Publisher19.html |title=Ashgate Publishing Ltd |publisher=Booknews.co.uk |accessdate=2012-07-28}}</ref> It was established in 1967 and specialised in the [[social sciences]], [[arts]], [[humanities]] and professional practice. It had an American office in [[Burlington, Vermont]],<ref>{{cite web |url=http://www.kellysearch.com/us-company-900741560.html |title=Ashgate Publishing Company - Ashgate Publishing Company - Burlington - Vermont (VT) - USA |publisher=Kellysearch.com |date=2012-07-06 |accessdate=2012-07-28}}</ref> and another British office in [[London]]. It is now a subsidiary of [[Informa]] ([[Taylor & Francis]]).

The company had two imprints: Gower Publishing published professional business and management titles,<ref>{{cite web |url=http://www.gowerpublishing.com/ |title=Gower Publishing |publisher=Gowerpublishing.com |accessdate=2012-07-28}}</ref> and Lund Humphries – established over 60 years ago – publishes illustrated art books, particularly in the field of modern [[British art]].<ref>{{cite web |url=http://www.lundhumphries.com/ |title=Lund Humphries |publisher=Lund Humphries |accessdate=2012-07-28}}</ref> In March 2015, Gower unveiled GpmFirst,<ref>{{cite web |url=http://www.gpmfirst.com/|title=GpmFirst |work=GpmFirst}}</ref> a web-based community of practice allowing subscribers access to more than 120 project management titles, as well as discussions and articles relevant to business and project management.

In July 2015, it was announced that Ashgate had been sold to [[Informa]]<ref>{{cite web |url=https://www.ashgate.com/default.aspx?page=5616 |title=Ashgate Acquired by Informa (Taylor & Francis) |work=ashgate.com}}</ref> for a reported £20M.<ref>{{cite web |url=http://www.thebookseller.com/news/informa-pays-20m-ashgate-publishing-308308 |title=Informa pays £20m for Ashgate Publishing |work=[[The Bookseller]]}}</ref> It was announced in December 2015 that Lund Humphries has been relaunched as an independent publisher.<ref>{{cite web|url=http://www.thebookseller.com/news/lund-humphries-relauches-indie-publisher-317843 |title=Lund Humphries relaunches as indie publisher |work=[[The Bookseller]]}}</ref> By February 2016, the independent imprints of Ashgate became part of the [[Routledge]] imprint.

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.ashgate.com}}

{{Informa}}

[[Category:Book publishing companies of the United Kingdom]]
[[Category:Book publishing companies of the United States]]
[[Category:Educational book publishing companies]]
[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1967]]
[[Category:1967 establishments in the United Kingdom]]


{{publisher-stub}}