__NOTOC__{{Infobox military person
|name=Johannes Naumann
|image=
|caption=
|birth_date=11 October 1917
|death_date={{death date and age|2010|3|22|1917|10|11|df=yes}}
|birth_place=
|death_place=
|nickname=
|allegiance={{flag|Nazi Germany}} <br />{{flag|West Germany}}
|branch=[[Luftwaffe]]<br/>[[German Air Force]]
|serviceyears=
|rank=[[Major (Germany)|Major]] ([[Wehrmacht]])<br />[[Oberst]] ([[Bundeswehr]])
|unit=
|commands=II./[[JG 26]], II./[[JG 6]], III./[[JG 7]]
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Johannes Naumann''' (11 October 1917 – 22 March 2010) is a pilot in the [[Luftwaffe]] of [[Nazi Germany]] during [[World War II]]. A [[fighter ace]], he was a recipient of the [[Knight's Cross of the Iron Cross]]. Naumann was credited with 34 aerial victories claimed in roughly 350 combat missions during World War II.<ref>Obermaier 1989, p. 172.</ref> Naumann credited his father with inspiring him to become a fighter pilot.  Naumann went on to fly both the Me 109 and the Fw 190 in combat during World War II, and while he also trained on the Me 262, he never flew the jet operationally.<ref>{{cite web|last=Holland|first=James|title=JOHANNES NAUMANN Interview|url=http://www.griffonmerlin.com/ww2_interviews/johannes-naumann-german/|publisher=Griffon Merlin|accessdate=16 August 2013|date=May 2008}}</ref>

==Awards==
* [[Iron Cross]] (1939) 2nd and 1st Class
* [[Ehrenpokal der Luftwaffe]] on 25 June 1943 as ''[[Oberleutnant]]'' and pilot<ref>Patzwall 2008, p. 152.</ref>
* [[German Cross]] in Gold on 31 August 1943 as ''[[Hauptmann]]'' in the 6./Jagdgeschwader 26<ref>Patzwall & Scherzer 2001, p. 326.</ref>
* [[Knight's Cross of the Iron Cross]]  on 9 November 1944 as ''Hauptmann'' and ''[[Gruppenkommandeur]] '' of the II./Jagdgeschwader 6<ref>Fellgiebel 2000, p. 321.</ref><ref>Scherzer 2007, p. 563.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst 
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |last2=Scherzer
  |first2=Veit 
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit 
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
{{Refend}}

{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Naumann, Johannes}}
[[Category:1917 births]]
[[Category:2010 deaths]]
[[Category:People from Dresden]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:People from the Kingdom of Saxony]]


{{germany-airforce-bio-stub}}