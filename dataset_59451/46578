{{Use mdy dates|date=January 2015}}
'''Win Blevins''' (born October 21, 1938)<ref>{{cite web|url=http://www.encyclopedia.com/article-1G2-2699000022/blevins-winfred-1938.html |title=Blevins, Winfred 1938– - Contemporary Authors, New Revision Series |publisher=Encyclopedia.com |date= |accessdate=2013-11-25}}</ref> is a New York Times Bestselling<ref>{{cite web|url=https://www.nytimes.com/best-sellers-books/2016-03-20/e-book-nonfiction/list.html}}</ref> American [[author]] of historical [[fiction]], narrative non-fiction, historical fantasy, and [[non-fiction]] books, as well as short stories, novellas, articles, reviews, and screenplays. He has written many books about the western mountain trappers,<ref>{{cite web|url=http://muzzleloadermagazine.com/Book%20Reviews/2004/BookReview_SO04_A.htm |accessdate=July 25, 2012 }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref> and is known for his "mastery of western lore."<ref>{{cite web|url=http://www.publishersweekly.com/978-0-7653-0577-0 |title=Fiction Book Review: A Long and Winding Road by Win Blevins, Author . Forge $25.95 (352p) ISBN 978-0-7653-0577-0 |publisher=Publishersweekly.com |date=2007-09-03 |accessdate=2013-11-25}}</ref> His notable works include Stone Song, So Wild a Dream, and Dictionary of the American West. According to WorldCat, the Dictionary of the American West is held in 728 libraries.<ref>{{cite web|url=http://www.worldcat.org/title/dictionary-of-the-american-west/oclc/25316030&referer=brief_results |title=Dictionary of the American West (Book, 1992) |publisher=[WorldCat.org] |date=2012-03-11 |accessdate=2013-11-25}}</ref> Blevins has won numerous awards, including being named winner of the [[Owen Wister Award|Owen Wister Award for Lifetime Achievement]] in writing literature of the West,<ref name="prnewswire.com">http://www.prnewswire.com/news-releases/win-blevins-to-receive-2015-owen-wister-award-300021657.html</ref> being selected for the Western Writers Hall of Fame,<ref name="prnewswire.com"/> being twice named 'Writer of the Year' by [http://www.wordcraftcircle.org/ Wordcraft Circle of Native Writers],<ref name="ipl"/> and winning two [[Spur Award]]s for Novel of the West.<ref>{{cite web|url=http://ascplpop.akronlibrary.org/favorite-fiction-booklists/westerns/spur-award-winning-westerns/ |title=Spur Award-Winning Westerns » Pop Culture@Ascpl |publisher=Ascplpop.akronlibrary.org |date= |accessdate=2013-11-25}}</ref><ref>{{cite web|url=http://westernwriters.org/spur-awards/#a1995 |title=Spur Awards « Western Writers of America |publisher=Westernwriters.org |date= |accessdate=2013-11-25}}</ref>

[[File:Winblevins.gif|thumb|Win Blevins]]

==Early life and education==
Blevins, of [[Cherokee]] and Welsh-Irish descent, is a native of [[Little Rock, Arkansas]]. After attending school in [[St. Louis, Missouri]], he moved to New York, where he received a master's degree from [[Columbia University]], graduating with honors, and continued to California, where he attended the Music Conservatory of the [[University of Southern California]].<ref name="ipl">{{cite web|url=http://www.ipl.org/div/natam/bin/browse.pl/A753 |title=Win Blevins on Native American Authors |publisher=Ipl.org |date= |accessdate=2013-11-25}}</ref>

==Journalism and writing career==
Win Blevins started his writing career as a music and drama reviewer for the Los Angeles Times. He then became the entertainment editor and principal theater and movie critic of the [[Los Angeles Herald-Examiner|Hearst]] newspaper in Los Angeles.<ref>"Blevins, Win 1938–." Contemporary Authors, New Revision Series. Ed. Amy Elisabeth Fuller. Vol. 188. Detroit: Gale, 2009. 59-61. Gale Virtual Reference Library. Web. June 29, 2012.</ref> His first book was published in 1973 and since then he has made a living as a free-lance writer. He has written articles for magazines,<ref>{{cite web|url=http://www.designbuildbluff.org/sites/default/files/files/about/media_2011_native_peoples_201111.pdf |title=Native Peoples |publisher=Designbuildbluff.org |accessdate=2013-11-25}}</ref><ref>{{cite web|url=http://www.ohioswallow.com/book/Frank+Waters |title=Frank Waters: Man and Mystic - Ohio University Press & Swallow Press |publisher=Ohioswallow.com |date= |accessdate=2013-11-25}}</ref> essays,<ref>{{cite web|url=http://www.gazette.com/articles/publisher-26798-through-poetry.html |title=Hot Off The Press |publisher=Gazette.com |date=2007-09-02 |accessdate=2013-11-25}}</ref> published thirty-one books, one a dictionary, several travel guides to the West, and the rest novels, including fantasy, historical fiction and modern works of the West such as his contemporaries [[Rudolfo Anaya]], [[John Nichols (writer)|John Nichols]], [[Scott Momaday]], Max Evans and [[Barbara Kingsolver]] write. For fifteen years he was an editor at [[Macmillan Publishers|Macmillan Publishing]]. From 2010 - 2012, Win spent two years as [[Gaylord College of Journalism and Mass Communication|Gaylord Family Visitor Professor of Professional Writing]] at the University of Oklahoma.<ref>[http://www.ou.edu/content/gaylord/home/main/faculty_staff/WinBlevins.html ]{{dead link|date=November 2013}}</ref> He has also written seven screenplays.

==Books==
Most of Win Blevins' books were originally published as hardbacks, and were subsequently made available as mass-market paperbacks, trade paperbacks, book club editions, foreign editions, audio books, and e-books. Almost all are still in print.
* ''Give Your Heart to the Hawks'', Nash Publishing, 1973. Narrative non-fiction.
* ''Charbonneau: Man of Two Dreams'', Nash Publishing, 1975. Historical fiction.
* ''The Misadventures of Silk and Shakespeare'', Jameson Books, 1985. Historical fiction.
* ''The Yellowstone'', Bantam, 1988 (Rivers West series), Bantam Books. Historical fiction.
* ''Roadside History of Yellowstone Park'', 1989, Mountain Press Publishing Company. Guide Book.
* ''Powder River'', Bantam, 1990 (Rivers West series). Historical fiction.
* ''The Snake River'', Bantam, 1992 (Rivers West series). Historical fiction.
* ''The High Missouri'', Bantam, 1994 (River West series). Historical fiction.
* ''Dictionary of the American West'', 1993, Facts on File.
* ''History from the Highways'': Wyoming, with Thomas Schmidt, 1993, Pruett Press. Guide Book.
* ''Stone Song: A Novel of the Life of Crazy Horse'', TOR-Forge Books, 1995. Historical fiction. According to WorldCat, the book is held in 774 libraries<ref>{{cite web|url=http://www.worldcat.org/title/stone-song-a-novel-of-the-life-of-crazy-horse/oclc/32051142&referer=brief_results |title=Stone song : a novel of the life of Crazy Horse (Book, 1995) |publisher=[WorldCat.org] |date= |accessdate=2013-11-25}}</ref>
* ''The Rock Child'', TOR-Forge Books, 1998. Historical fiction.
* ''RavenShadow'', TOR-Forge Books, 1999. Historical fantasy.
* ''Dictionary of the American West'', expanded edition, Sasquatch Books, 2001. Expanded to include the Pacific Northwest, especially Alaska.
* [http://www.worldcat.org/title/moonlight-water/oclc/879582555&referer=brief_results ''Moonlight Water''], written by Win Blevins and Meredith Blevins, TOR-Forge Books, 2015.
* ''The Darkness Rolling'', written by Win Blevins and Meredith Blevins, TOR-Forge Books, 2015.

===The Rendezvous Series===
* ''So Wild a Dream'', TOR-Forge Books, 2003. Historical fiction. According to WorldCat, the book is held in 1094 libraries, his most widely held book.<ref>{{cite web|url=http://www.worldcat.org/oclc/052208376 |title=So wild a dream (Book, 2003) |publisher=[WorldCat.org] |date= |accessdate=2013-11-25}}</ref>
* ''Beauty for Ashes'', TOR-Forge Books, 2004. Historical fiction.
* ''Dancing with the Golden Bear'', TOR-Forge, 2005. Historical fiction.
* ''Heaven Is a Long Way Off'', TOR-Forge Books, 2006. Historical fiction.
* ''A Long and Winding Road'', TOR-Forge Books, 2007. Historical fiction.
* ''Dreams Beneath Your Feet'', TOR-Forge Books, 2008. Historical fiction.

===Natural History===
* ''Buffalo, Rio Nuevo'', 2005 (Looks West series). Natural history.

===Cherokee Pre-History Fantasy===
* ''Zadayi Red'', TOR-Forge Books, 2009, published under the pen name Caleb Fox. Historical fantasy.  Reissued as 'The Chosen One' by Meredith & Win Blevins 
* ''Shadows in the Cave'', TOR-Forge Books, 2010, TOR-Forge Books, published under the pen name Caleb Fox. Historical fantasy. Reissued with authors Meredith & Win Blevins

===Yazzie Goldman===
* ''Stealing Fire'', TOR-Forge, 2016

==As General Editor==
Blevins also created, edited, and co-published the series ''Classics of the Fur Trade''.
* ''The River of the West: The Adventures of Joe Meek, volume one—The Mountain Years'', by Frances Fuller Victor, Mountain Press Publishing Company, 1983. Autobiography.
* ''Journal of a Mountain Man, by James Clyman'', Mountain Press Publishing Company, 1984. Journal.
* ''The River of the West: The Adventures of Joe Meek, volume two—The Oregon Years'', by Frances Fuller Victor, Mountain Press Publishing Company, 1985. Autobiography.
* ''Edward Warren'', by Sir William Drummond Stewart, Mountain Press Publishing Company, 1986. Fiction.
* ''The Personal Narrative of James O. Pattie'', by James Ohio Pattie, Mountain Press Publishing Company, 1988. Memoir.
* ''The Long Rifle'', by Steward Edward White, Mountain Press Publishing Company, 1990. Historical fiction.

==Awards==
* Owen Wister Award, 2015, for lifetime achievement in writing literature of the West.<ref name="prnewswire.com"/>
* Selection to the Western Writers Hall of Fame, displayed in the Buffalo Bill Museum, Cody,Wyoming, 2015.<ref name="prnewswire.com"/>
* So Wild a Dream- Spur award, 2004.<ref>{{cite web|url=http://westernwriters.org/spur-awards/#a2004 |title=Spur Awards « Western Writers of America |publisher=Westernwriters.org |date= |accessdate=2013-11-25}}</ref>
* Stone Song –Spur Award, Mountains and Plains Booksellers award for best fiction of 1995.<ref name="ipl" /><ref>{{cite web|url=http://www.selectpeaks.com/images/awards.pdf |title=Regional Book Award Winners |publisher=Selectpeaks.com |accessdate=2013-11-25}}</ref>
* Dictionary of the American West – Wordcraft Circle of Native Writers and Storytellers (Writer of the Year) 2002.<ref name="ipl" />
* Heaven Is a Long Way Off - Wordcraft Circle of Native Writers and Storytellers (Writer of the Year) 2006-2007.<ref name="ipl" />

==Pseudonyms==
Win has published two novels and an article in True West Magazine<ref>{{cite web|url=http://www.truewestmagazine.com/jcontent/true-westerners/true-westerners/what-history-taught-me/2921-caleb-fox-author |title=Caleb Fox, Author |publisher=Truewestmagazine.com |date=2009-11-03 |accessdate=2013-11-25}}</ref> under the pen name, Caleb Fox.

==Personal life==

Blevins has five children and a growing number of grandchildren. He lives with his wife, the novelist Meredith Blevins, among the Navajos in [[San Juan County, Utah]]. Win has been a river runner and has climbed mountains on three continents. His greatest loves are his family, music, and the untamed places of the West. He considers writing for a living to be a great blessing.

==References==
{{Reflist}}

{{DEFAULTSORT:Blevins, Winfred}}
[[Category:1938 births]]
[[Category:Living people]]
[[Category:American male novelists]]
[[Category:20th-century American novelists]]
[[Category:21st-century American novelists]]
[[Category:Native American writers]]