{{For|usage of airport before 1971|Perrin Air Force Base}}
{{Infobox airport
| name         = North Texas Regional Airport
| nativename   = Perrin Field
| image        = North Texas Regional Airport - Texas.jpg
| image-width  = 250
| caption      = [[USGS]] 1999 [[orthophoto]]
| IATA         = PNX <!--not GYI-->
| ICAO         = KGYI
| FAA          = GYI
| type         = Public
| owner        = [[Grayson County, Texas]]
| operator     =
| city-served  = [[Sherman, Texas|Sherman]] / [[Denison, Texas|Denison]]
| location     = <!--if different than above-->
| elevation-f  = 749
| website      =
| coordinates  = {{coord|33|42|51|N|096|40|25|W|region:US-TX_scale:10000|display=inline,title}}
| pushpin_map            = USA Texas
| pushpin_mapsize        = 250
| pushpin_map_caption    = Location in Texas
| pushpin_label          = '''KGYI'''
| pushpin_label_position = bottom
| r1-number    = 13/31
| r1-length-f  = 2,277
| r1-surface   = Asphalt
| r2-number    = 17L/35R
| r2-length-f  = 9,000
| r2-surface   = Asphalt/Concrete
| stat-year    = 2007
| stat1-header = Aircraft operations
| stat1-data   = 53,300
| stat2-header = Based aircraft
| stat2-data   = 169
| footnotes    = Source: [[Federal Aviation Administration|FAA]]<ref name="FAA">{{FAA-airport|ID=GYI|use=PU|own=PU|site=24780.*A}}. Federal Aviation Administration. Effective July 2, 2009.</ref> and airport website<ref name="Airport">[http://www.northtexasregionalairport.com/ North Texas Regional Airport], official site</ref>
}}

'''North Texas Regional Airport''' / '''Perrin Field'''<ref name="Airport" /> {{airport codes|PNX|KGYI|GYI}} is a county owned [[airport]] in [[Grayson County, Texas]] between [[Sherman, Texas|Sherman]] and [[Denison, Texas|Denison]].<ref name="FAA" /> Formerly Grayson County Airport, the airport was renamed in November 2007.<ref name="Airport" /> Several buildings are occupied by businesses, Grayson County government agencies, and [[Grayson County College]].

Most U.S. airports use the same three-letter [[location identifier]] for the [[Federal Aviation Administration|FAA]] and [[International Air Transport Association|IATA]], but this airport is GYI to the FAA and PNX to the IATA (which assigned GYI to [[Gisenyi Airport]] in Gisenyi, [[Rwanda]]).

The northern extension of [[Texas State Highway 289|State Highway 289]] passes the airport on the west side.

== History ==
{{For|usage of airport before 1971|Perrin Air Force Base}}
The airport is on the site of '''Perrin Air Force Base''', which was built in 1941 and closed in 1971.<ref>[http://www.northtexasregionalairport.com/content/?page=18 North Texas Regional Airport: History]</ref> Since the closure, a group of local citizens have held the memory of Perrin together, hosting nine Perrin Field reunions since the early 1980s. The Perrin AFB Research Foundation was established in 1998.  Today, in addition to serving as a general aviation airport, several businesses, as well as a juvenile detention center/boot-camp and adult probation center are built upon former barracks and nearby areas.  There is a small museum dedicated to the former Perrin Air Force Base at the airport and [[Grayson County College]] uses several buildings.  The college also operates the former base golf course.
[[File:North Texas Regional Airport.jpg|thumb|left|135px|Front entrance sign at the North Texas Regional Airport]]
After seeing the fighters take off from here as a young man, aviation expert [[Chesley Sullenberger]] (best known as the pilot of [[US Airways Flight 1549]]) became interested in flying.<ref name="nyt">{{Cite news|last=Rivera|first=Ray|title=A Pilot Becomes a Hero Years in the Making|newspaper=The New York Times|date=January 16, 2009|url=https://www.nytimes.com/2009/01/17/nyregion/17pilot.html?pagewanted=2&_r=1&hp|postscript=<!--None-->}}</ref><ref name="aarp">{{cite web|last=Kaufmann|first=Carol|title=Hudson River Hero|work=AARP Today|publisher=American Association of Retired Persons|date=January 16, 2009|url=http://bulletin.aarp.org/yourworld/gettingaround/articles/hudson_river_hero.html|accessdate=March 16, 2009}}</ref>

== Facilities==


The airport covers {{convert|1,410|acre|ha|lk=on}} at an [[elevation]] of 749 feet (228 m). It has two [[runway]]s: 17L/35R is 9,000 by 150 feet (2,743 x 46 m) asphalt/concrete; 13/31 is 2,277 by 60 feet (694 x 18 m) asphalt.<ref name="FAA" />

It had three runways, but one 8,000'(2,438m) runway is now a [[taxiway]]. The airport has a Category I [[instrument landing system]] (ILS) to Runway 17L.  The former USAF control tower resumed operations in mid-2008.

In the year ending April 30, 2007 the airport had 53,300 aircraft operations, average 146 per day: 98% [[general aviation]], 2% military and <1% [[air taxi]]. 169 aircraft were then based at the airport: 73% single-engine, 11% multi-engine, 11% jet, 4% [[helicopter]] and 1% [[ultralight]].<ref name="FAA" />

==References==
<references/>

==External links==
{{Portal|United States Air Force|Military of the United States}}
* [http://www.perrinfield.org/ Perrin Field Historical Society and Museum]
* [http://ftp.dot.state.tx.us/pub/txdot-info/avn/airport_directory/gyi.pdf Sherman/Denison, North Texas Rgnl / Perrin Field (GYI)] at [[Texas DOT]] Airport Directory
* {{FAA-procedures|GYI}}
* {{US-airport-ga|GYI|PNX}}

[[Category:Airports in Texas]]
[[Category:Buildings and structures in Grayson County, Texas]]
[[Category:Transportation in Grayson County, Texas]]