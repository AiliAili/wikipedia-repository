{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->

 | Name = Beautiful Heartache
 | Type = studio
 | Artist = [[Oksana Grigorieva]]
 | Cover = Beautiful Heartache.jpg
 | Alt = 
 | Released = November 2, 2009
 | Recorded = 2009
 | Genre = [[Pop music|Pop]], [[classical music|classical]], [[traditional pop]]
 | Length = 45:21
 | Label = [[Icon Entertainment]]
 | Producer = [[Oksana Grigorieva]], [[Mel Gibson]]
 | Last album  = ''[[Awake (Josh Groban album)|Awake]]'', "Un Dia Llegara" (2006)
 | This album  = '' Beautiful Heartache'' (2009)
 | Next album  =
}}
'''''Beautiful Heartache''''' is a 2009 [[music]] [[album]] by [[Russians|Russian]] [[pianist]] and [[singer-songwriter]], [[Oksana Grigorieva]], with [[Mel Gibson]] as [[executive producer]]. Born in [[Saransk]], [[Russia]], Grigorieva studied music in [[Moscow]] and [[Kazan]], and at the [[Royal College of Music]] in [[London]]. She later moved to the U.S. and [[patent]]ed a technique of instructing music notation to children. Grigorieva gained attention as a songwriter in 2006 after a song that she wrote, "Un Dia Llegara", became popular on the [[Josh Groban]] album ''[[Awake (Josh Groban album)|Awake]]''.

Grigorieva signed with Gibson's music label, and the two began production on her first music album. She composed all songs on the album. Gibson served as co-writer on the song, "Say My Name," and recorded four [[music video]]s for the album's release. The album was positively received by the media, including ''[[ABC News]]'' and ''[[Reuters]]''. ''IndieLondon'' gave the album a rating of 4 out of 5.

== Musician ==
{{main|Oksana Grigorieva}}
Grigorieva was born in [[Saransk]], [[Russia]].<ref name="machell" />  Her parents were music professors.<ref name="machell" />  She grew up in [[Ukraine]],<ref name="palmer" /> and at the age of 15 moved to [[Moscow]] to attend university in order to learn [[piano]].<ref name="machell" /><ref name="palmer" />  She said of her experience of learning music as a child, "Diplomas were everything. It wasn't unusual for students to play ten hours a day. Our skin would start to crack. We'd literally have bleeding fingers."<ref name="machell" /> Grigorieva finished [[conservatoire]] studies in [[Kazan]],<ref name="machell" /> and moved to [[London]] where she continued her studies and taught music to others.<ref name="machell" /> After studying at the [[Royal College of Music]],<ref name="palmer" /> she moved to the [[United States]] and spent time living in [[New York City]] and [[Los Angeles, California]].<ref name="machell" /> She taught music in the U.S., and [[patent]]ed a technique of instructing music notation to children.<ref name="machell" /> Grigorieva composed and performed music, and produced works for theatre and advertisements.<ref name="machell" /> A 2006 song she wrote, "Un Dia Llegara", became popular on the [[Josh Groban]] album, ''[[Awake (Josh Groban album)|Awake]]''; this garnered her recognition as a songwriter.<ref>{{cite news | last = Serpe | first = Gina | title = Five Things You Need to Know About Oksana Grigorieva| work = E! Online | publisher = www.eonline.com | date = July 15, 2010 | url = http://www.eonline.com/uberblog/detail.jsp?contentId=190543 | accessdate = 2010-07-17}}</ref><ref name="zeidler">{{cite news | last = Zeidler | first = Sue | title = Gibson girlfriend Oksana Grigorieva unveiled | work = [[Reuters]] | publisher = in.reuters.com | date = August 11, 2009 | url = http://in.reuters.com/article/idINIndia-41669720090811 | accessdate = 2010-07-17}}</ref><ref name="wennfact">{{cite news | title = Fascinating fact | work = [[World Entertainment News Network]] | location =England | publisher = Comtex | date = August 11, 2009}}</ref>

== Production ==
Grigorieva had signed with Gibson's music label prior to the album's release.<ref name="aap">{{cite news| last =[[Australian Associated Press]] | title =Gibson a father again  | work = [[The Australian]] |location=Australia | publisher = Nationwide News Pty Limited | page = 010 | date = November 3, 2009 }}</ref> The music project was made by the film production company [[Icon Entertainment]], owned by Gibson.<ref name="machell">{{cite news| last = Machell | first = Ben   | title =  From Russia, with love – music Oksana Grigorieva is more than merely Mel Gibson's paramour, says Ben Machell | work = [[The Times]] | location = London, England | publisher = Times Newspapers Limited | pages = 6, 7 | date = October 24, 2009  }}</ref> She wrote all of the songs on the album,<ref>{{cite news| last = The McClatchy Company | title = Mel and pregnant girlfriend deliver a song  | work = [[The Kansas City Star]] | location =Missouri  | page = D6 | date = June 16, 2009  }}</ref> and received a credit as producer with Gibson listed as [[executive producer]].<ref name="machell" /> Grigorieva explained the choice of title for the album, in an interview with ''IndieLondon'', "Well, one of the tracks on the album is called ''Beautiful Heartache'' ... everybody has a heartbreak at some point in their lives. It’s how you deal with it that’s important. So, I guess I was drawn to that notion for the album title."<ref>{{cite news|first=Rob |last=Carnevale | title =  Oksana Grigorieva – The IndieLondon interview | work =IndieLondon  | publisher =  www.indielondon.co.uk | year = 2009 | url =http://www.indielondon.co.uk/Music-Review/oksana-grigorieva-the-indielondon-interview  | accessdate = 2010-07-30 }}</ref>

In an interview with ''[[The Times]]'', she commented, "I'm very lucky they offered me the chance to follow my dreams, but also to allow me to produce my own album, which is hardly ever offered to first-time artists. He believes in me that much. I'm really blessed ... It's the most incredible gift that Mr Gibson could have given me."<ref name="machell" /> She acknowledged to the ''[[Herald Sun]]'' that it was helpful to have Gibson's production company assist with her music career, "If you think about it, every talent needs a serious push and help from somebody who is stronger, because it's pretty much impossible for anybody to succeed in this industry. So I've become very lucky, and I'm very grateful. You don't control the situations or the people you meet. I did not plan this. I'm just doing what I've always been doing. It's not like I've changed my goals at all."<ref name="lucky">{{cite news| last = Byrne | first = Fiona |author2=Alice Coster |author3=Katherine Firkin  | title = Mel's lover a 'lucky' woman | work = [[Herald Sun]] | location =Melbourne, Australia  | publisher =  Nationwide News Pty Limited| page =018  | date = August 13, 2009  }}</ref>

Gibson contributed lyrics to multiple tracks on the album.<ref name="palmer">{{cite news| last = Palmer | first =Alun | title = Mel of a girl | work =[[The Daily Mirror]]|location=London, England | date =October 23, 2009   }}</ref> The track, "Say My Name", was co-written by Grigorieva and Gibson.<ref name="mustbedeaf">{{cite news| last =Byrne  | first = Fiona |author2=Alice Coster |author3=Katherine Firkin | title = Music critics say Mel must be deaf | work = [[Herald Sun]] | location =Melbourne, Australia  | publisher = Nationwide News Pty Limited | page = 018| date =June 17, 2009  }}</ref> Grigorieva described "Say My Name", stating, "It is naked, intimate, raw emotion. It is the first and one of the simplest songs on the album, but also one of the most intimate."<ref name="mustbedeaf" /> She characterized it as, "little more than piano and voice as if it is a cry from the soul".<ref>{{cite news| last = Gutierrez | first =Lisa   | title = Mel Gibson and pregnant girlfriend's new arrival – a song | work =[[The Kansas City Star]]  | location =Missouri  | date = June 15, 2009  }}</ref> Gibson provided guest vocals for two tracks on the album.<ref>{{cite news| last =Hurt | first = Jessica |author2=Anna Vlach|author3=Helene Sobolewski  | title =Mel's new role . . . rock star  | work = The Advertiser | location = Adelaide, Australia | publisher = Nationwide News Pty Limited | page = 022 | date =  September 29, 2009  }}</ref><ref>{{cite news| last =Moodie  | first = Clemmie |author2=Danielle Lawler  | title = Absolute hell Mel | work = [[The Daily Mirror]] | location =London, England | date = August 14, 2009 }}</ref> Grigorieva stated of Gibson's singing on the album, "Mel is an incredible artist. He has a great voice. He is a visionary. He enabled me to produce this, for my music to live."<ref name="byrne">{{cite news| last =Byrne  | first =Fiona |author2=Alice Coster |author3=Katherine Firkin | title = Mel croons for his love | work =  [[Herald Sun]] | location =Melbourne, Australia  | publisher = Nationwide News Pty Limited | page =026  | date = August 18, 2009  }}</ref>

Gibson served as director of four [[music video]]s intended to accompany the release of the album.<ref name="machell" /><ref name="zeidler" /> The music videos were filmed over a seven-day period in [[Mexico]].<ref name="palmer" /> Grigorieva stated of working with Gibson as a music video director, "I kept asking him, 'Please, give me directions!' I said, 'You can yell at me if you want to'. But he said, 'No, you're doing just fine. You're a good actor'. He was very much into his own world, painting pictures with the camera."<ref name="machell" /> In describing the music videos, she observed, "They're like dramatic miniature films a whirlpool of different, exciting, bright images."<ref>{{cite news| last =  Fillo | first = Mary Ellen | title = From Oscar-Winning Movies To Music Videos  | work =  [[The Hartford Courant]] | location =  Connecticut | publisher = The Hartford Courant Co. | page = C2 | date = August 13, 2009  }}</ref> Grigorieva toured to promote her album, during October 2009.<ref>{{cite news| last = Byrne | first =  Fiona |author2=Alice Coster|author3=Katherine Firkin | title =  Beautiful music for bub| work = [[Herald Sun]] | location = Melbourne, Australia | publisher =Nationwide News Pty Limited  | page = 021 | date = October 28, 2009 }}</ref> The first single from the album, "Say My Name", was released in June 2009 on the website People.com as a listening exclusive.<ref>{{cite news | title = Gibson's girl releases first single | work = [[World Entertainment News Network]] | location = England | publisher = Comtex | date = June 15, 2009  }}</ref> ''Beautiful Heartache'' was released on November 2, 2009.<ref name="machell" /> It was made available in 2009 on [[iTunes]],<ref name="lucky" /> and via download on the musician's official website, at www.oksana.fm.<ref name="mustbedeaf" />

== Contents ==
''Beautiful Heartache'' was marketed as "grown-up, piano-led pop, heavy on classical motifs".<ref name="machell" /> The first single released for the album was titled, "Say My Name".<ref name="sharp">{{cite news| last = Sharp | first = Annette |author2=Marcus Casey|author3=Joel Christie|author4=Jen Melocco | title = Mad about Mel  | work =Daily Telegraph  | location =Sydney, Australia  | publisher = Nationwide News Pty Limited | page = 016 | date = July 14, 2009 }}</ref> A music video for the second single released from the album featured Grigorieva on the piano, as well as portraying an assistant to a knife-thrower.<ref>{{cite news| title = Mel directs girlfriend's song video | work =  [[The Daily Mirror]] | location =  London, England | date = July 15, 2009  }}</ref> The album includes a title track, "Beautiful Heartache", with lyrics including: "I love the way you wear your skin."<ref name="sharp" /> The music video for the track, "Beautiful Heartache", focused on a [[tango music|tango]] theme.<ref name="sharp" /> The song "Angel" was named for her son with Timothy Dalton, Alexander.<ref>{{cite news | author = Daily Mail Reporter | title = Mel Gibson's pregnant girlfriend shows her daring side as a knife-thrower's assistant in video he directed | work = [[The Daily Mail]] | publisher = www.dailymail.co.uk | date = July 13, 2009 | url = http://www.dailymail.co.uk/tvshowbiz/article-1199381/Mel-Gibsons-pregnant-girlfriend-Oksana-Grigorieva-shows-daring-video-directed.html | accessdate = 2010-07-10 }}</ref>

== Reception ==
{{Album ratings
|rev1 = ''IndieLondon''
|rev1score = {{Rating|4|5}}<ref name="foley" />
}}<!-- Automatically generated by DASHBot-->
''[[ABC News]]'' reported that ''Beautiful Heartache'' garnered the singer "rave reviews" from music critics.<ref name="breaksonto">{{cite news | last = | first = | title = Oksana Grigorieva Breaks Onto the Music Scene | work = [[ABC News]] | language = | publisher = [[American Broadcasting Company|ABC]] | page = | date = August 13, 2009 | url = http://abcnews.go.com/video/playerIndex?id=8318175 | accessdate = 2010-07-10}}</ref> ''[[Reuters]]'' commented of the album, "'Beautiful Heartache' features a collection of wistful love songs, blending shimmering string arrangements with pop and jazz-influenced arrangements that showcase Grigorieva's soulful voice."<ref name="zeidler" /> In a review of the album, Jack Foley of ''IndieLondon'' gave it a rating of 4 out of 5, and observed, "throughout, she displays a keen ear for melody, for honest emotional simplicity and classic values. She’s well worth taking the time to check out".<ref name="foley">{{cite news | last = Foley | first = Jack | title = Oksana Grigorieva&nbsp;— Beautiful Heartache | work = IndieLondon | publisher = www.indielondon.co.uk | year =2010 | url = http://www.indielondon.co.uk/Music-Review/oksana-grigorieva-beautiful-heartache | accessdate = 2010-07-10}}</ref> The track from the album co-written with Mel Gibson, "Say My Name", was not generally well received by music critics,<ref name="sharp" /><ref>{{cite news| last = Byrne | first =  Fiona |author2=Alice Coster|author3=Katherine Firkin  | title = Will there be more music heartache for Oksana? | work =[[Herald Sun]]  | location = Melbourne, Australia | publisher = Nationwide News Pty Limited | page =016  | date =July 14, 2009  }}</ref> and received negative comments from listeners at the website ''[[E! Online]]''.<ref name="mustbedeaf" />

== Track listing ==
{{tracklist
| collapsed       = 
| headline        = Beautiful Heartache
| extra_column    = 
| total_length    = 45:21
| all_writing     = 
| all_lyrics      = [[Oksana Grigorieva]];
| all_music       = Oksana Grigorieva; "Say My Name" co-written with [[Mel Gibson]]
| writing_credits = 
| lyrics_credits  = 
| music_credits   = 
| title1          = Say My Name
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 3:48
| title2          = Flying Upside Down
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         =3:48
| title3          = When It’s Over
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         =4:07
| title4          = Evening With Daddy
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 4:46
| title5          = Angel
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 3:53
| title6          = Beautiful Heartache
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 3:53
| title7          = Back From Russia
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 4:42
| title8          = What Kind of Love Is This?
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 4:18
| title9          = Ain’t Right
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 4:22
| title10          = Love Goes On
| note10           = 
| writer10         = 
| lyrics10         = 
| music10          = 
| extra10          = 
| length10         = 4:45
| title11          = Dark Eyes (Ochi Chernye)
| note11           = 
| writer11         = 
| lyrics11         = 
| music11          = 
| extra11          = 
| length11         = 2:59

}}

== Personnel ==
;Composition
* [[Oksana Grigorieva]] – writing, composition
* [[Mel Gibson]] – co-writer, "Say My Name"
;Accompaniment
* [[Matt Chamberlain]] – drums
* Paul Bushnell – bass
* [[Lyle Workman]] – guitar
* Greg Suran – guitar
* [[Jamie Muhoberac]] – keyboard
* [[David Campbell (composer)|David Campbell]] – string arrangements and conducting

== See also ==

* [[Awake (Josh Groban album)]]
* [[List of classical pianists]]
* [[List of Russian people]]

== References ==
{{Reflist|2}}

== Further reading ==
* {{cite news| last = Machell | first = Ben   | title =  From Russia, with love – music Oksana Grigorieva is more than merely Mel Gibson's paramour, says Ben Machell | work = [[The Times]] | location = London, England | publisher = Times Newspapers Limited | pages = 6, 7 | date = October 24, 2009  }}
* {{cite news | last = Zeidler | first = Sue | title = Gibson girlfriend Oksana Grigorieva unveiled | work = [[Reuters]] | publisher = in.reuters.com | date = August 11, 2009 | url = http://in.reuters.com/article/idINIndia-41669720090811 | accessdate = 2010-07-17}}
* {{cite news| last =  LaPorte| first = Nicole | title = The Truth About Mel's Ex | work = Daily Beast | publisher =www.thedailybeast.com | date =June 28, 2010  | url = http://www.thedailybeast.com/blogs-and-stories/2010-06-28/mel-gibsons-ugly-split-with-oksana-grigorieva | accessdate =2010-07-30  }}

== External links ==
* [http://webcache.googleusercontent.com/search?q=cache:http://www.oksana.fm/ Oksana.fm]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, archive of website by [[Google]]
* [http://www.last.fm/music/Oksana+Grigorieva/+wiki Oksana Grigorieva], bio profile at ''Last.fm''

{{Portal bar|Arts|Music|Media|Russia|United States|California}}

{{Good article}}

[[Category:2009 albums]]