{{Otheruses|Journal of Insect Science (disambiguation){{!}}Journal of Insect Science}}
{{Infobox journal
| title = Journal of Insect Science
| formernames = 
| cover = 
| founded = Henry Hagedorn
| editor = Phyllis Weintraub
| discipline = [[Entomology]]
| abbreviation = 
| publisher = [[Oxford University Press]]
| country = 
| frequency = 
| history = 2001-present
| openaccess = Yes
| license = 
| impact = 0.921
| impact-year = 2013
| website = http://jinsectscience.oxfordjournals.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR =
| OCLC = 46820266
| LCCN = 
| CODEN =
| ISSN = 
| eISSN = 1536-2442
}}
'''''Journal of Insect Science''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] covering [[entomology]]. It was established in 2001 with support from the [[University of Arizona]]<ref name="oxfordjournals">{{cite web|url=http://jinsectscience.oxfordjournals.org/about|title=About &#124; Journal of Insect Science|publisher=jinsectscience.oxfordjournals.org|accessdate=2015-04-30}}</ref> by [[Henry Hagedorn]]. In 2014, after the death of [[Henry Hagedorn]], the [[Entomological Society of America]] assumed responsibility for the journal, and it switched to publishing with [[Oxford University Press]].<ref name="entsoc">{{cite web|url=http://www.entsoc.org/press-releases/oxford-university-press-publish-esa-journals|title=Oxford University Press to Publish ESA Journals &#124; Entomological Society of America (ESA)|publisher=entsoc.org|accessdate=2015-04-30}}</ref> The [[editors-in-chief|editor-in-chief]] is Phyllis Weintraub.

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-04-30}}</ref>
* [[Science Citation Index Expanded]]
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences
* [[The Zoological Record]]
* [[BIOSIS Previews]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.921.<ref name=WoS>{{cite book |year=2014 |chapter=Journal of Insect Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://jinsectscience.oxfordjournals.org/}}

[[Category:Publications established in 2001]]
[[Category:Entomology journals and magazines]]
[[Category:English-language journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Academic journals associated with learned and professional societies of the United States]]


{{biology-journal-stub}}