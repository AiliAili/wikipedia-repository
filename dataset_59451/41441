{{Use mdy dates|date=January 2017}}
{{Redirect|Triple Lindy|the dive|Back to School}}
[[File:Lindy hop.jpg|thumb|Dancing the Lindy hop at the [[Sacramento Jazz Jubilee]], [[Sacramento, California]], US (2006)]]
[[File:Lindyhop Oudedijk.webm|thumb|Lindy Hop Dance, 2013]]
The '''Lindy hop''' is an American dance that evolved in [[Harlem]], [[New York City]], in the 1920s and 1930s and originally evolved with the [[jazz]] music of that time. It was very popular during the [[Swing music|Swing era]] of the late 1930s and early 1940s.  Lindy was a fusion of many dances that preceded it or were popular during its development but is mainly based on [[jazz dance|jazz]], [[tap dance|tap]], [[breakaway (dance)|breakaway]], and [[Charleston (dance)|Charleston]]. It is frequently described as a [[jazz dance]] and is a member of the [[swing (dance)|swing dance]] family.

In its development, the Lindy hop combined elements of both partnered and solo dancing by using the movements and improvisation of [[African-American dance|Black dances]] along with the formal eight-count structure of European [[partner dance]]s – most clearly illustrated in the Lindy's basic step, the [[swingout]]. In this step's [[open position]], each dancer is generally connected hand-to-hand; in its [[closed position]], leads and follows are connected as though in an embrace on one side and holding hands on the other.

There was renewed interest in the dance in the 1980s from American, Swedish, and British dancers and the Lindy hop is now represented by dancers and loosely affiliated grass-roots organizations in North America, South America, Europe, Asia, and Oceania.

Lindy hop is sometimes referred to as a [[street dance]], referring to its improvisational and social nature.  In 1932, twelve-year-old [[Norma Miller]] did the Lindy hop outside the Savoy Ballroom with her friends for tips.<ref>{{Cite book|title=Swingin' at the Savoy: The Memoir of a Jazz Dancer|last=Miller|first=Norma|publisher=Candlewick Press|year=1996|isbn=0-7636-2244-3|location=Cambridge, Massachusetts|pages=11}}</ref>  In 1935, 15,000 people danced on Bradhurst Avenue for the second of a dance series held by the Parks Department. Between 147th and 148th street, Harlem "threw itself into the Lindy hop with abandon" as Sugar Hill residents watched from the bluffs along Edgecombe Avenue .<ref>{{Cite news|url=|title=15,000 DANCE IN STREET.|last=|first=|date=July 17, 1935|work=The New York Times|access-date=|archive-url=|archive-date=|dead-url=}}</ref> 

==History==
{{main|History of Lindy Hop}}
{{Contradicts other|date=January 2009|1=History of Lindy Hop}}

===Swing era (1920s–1940s)===
The Lindy hop was born in [[African-American]] communities in [[Harlem]], [[New York (state)|New York]], in the [[United States]], from about 1927 into the early 1930s. It originated from four possible sources, or some combination thereof: the [[Breakaway (dance)|breakaway]], the [[Charleston (dance)|Charleston]], the [[Texas Tommy (dance)|Texas Tommy]], and the hop.<ref name=Stearns>{{cite book | last = Stearns | first = Marshall and Jean | title = Jazz Dance: The Story of American Vernacular Dance | year = 1968 | publisher = Macmillan | location = New York|pages=128–129, 315–316, 322–326, 330}}</ref>

A recorded source of the dance's name is famed aviator [[Charles Lindbergh]], nicknamed "Lucky Lindy", who "hopped the Atlantic" in 1927.<ref>LINDBERGH ARRIVES AFTER RECORD HOPS, The New York Times, Front Page,May 13, 1927</ref><ref>HONDURANS HAIL LINDBERGH IN CONGRESS AND PALACE
HOPS FOR NICARAGUA TODAY, The New York Times, January 5, 1928</ref><ref>LINDBERGH REACHES GOAL AT HAVANA; SPOKESMEN OF PAN AMERICAN NATIONS AND 100,000 CUBANS WELCOME FLIER; WILD ACCLAIM AT LANDING Troops Barely Hold Back Great Crowd as Flier Taxis In. MET BY HUGHES AT FIELD Procession to City an Ovation-- President Machado Formally Greets Aviator at Palace. CONFERENCE HONORS HIM Pan-American Session Adjourns for Day as Lindbergh Comes by 800-Mile Hop From Haiti. Hughes Welcomes Him at Field. HOPS OFF IN HAITIAN DAWN, The New York Times, February 9, 1928</ref> After Lindbergh's solo non-stop flight from New York to Paris in 1927 he became incredibly popular <ref>{{cite web|title=Charles Augustus Lindbergh Jr.|url=http://www.biography.com/people/charles-lindbergh-9382609#first-solo-transatlantic-flight|website=Biography.com|publisher=The Biography.com website|accessdate=August 12, 2014}}</ref> and many people named businesses and other things after him. Te Roy Williams and His Orchestra recorded the song "Lindbergh Hop," written by Ted Nixon and Elmer Snowden, on May 25, 1927.<ref>Bastin, Bruce, The Melody Man: Joe Davis and the New York Music Scene, 1916-1978, University Press of Mississippi, 2012</ref><ref>Jazz Odyssey 'The Sound Of Harlem' Volume III Original 1964 3XLp Vinyl Box Set Columbia Records C3L 33 Mono Jazz Archive Series Various Artist in High Fidelity Sound with 40-Page Booklet, produced by Frank Driggs</ref> The Memphis Jug Band on September 13, 1928 recorded "Lindberg Hop- Overseas stomp," written by Jab Jones and [[Will Shade]].<ref>http://adp.library.ucsb.edu/index.php/matrix/detail/800020937/BVE-47008-Lindberg_hop retrieved 6/20/16</ref>

Just 10 days after Lindbergh landed at Le Bourget, a 'Lindy Hop' dance was described  by  columnist Gilbert Swan (Reno Gazette, May 31, 1927). He wrote, "Obviously the first dance named for the Lindbergh flight was the 'Lindy Hop'...Like all trick dances, they will be done in a few theatres and dance halls, where experts appear, and that will be that". Later that year, on Sep 14,  the Woodland Daily Democrat reported Catherine B Sullivan describing the  'Lindy Hop' as having been placed  3rd in the Dancing Masters of America, New Dances competition, behind the Kikajou, and the Dixie Step. ( 'Lindy Hop' also described  in reports as the 'Lindbergh Glide'). The journalist reports that  Miss Johnson showed a very fast  little step, with hops and a kick, whilst holding the arms out , like the Spirit of St Louis. The foot work is described  as "dum-de-dum, dum-de-dum, dum-de-dum".

According to Ethel Williams, the Lindy hop was similar to the dance known as the Texas Tommy in New York in 1913. The basic steps in the Texas Tommy were followed by a breakaway identical to that found in the Lindy. Savoy dancer [["Shorty" George Snowden]] stated that "We used to call the basic step the Hop long before [[Charles Lindbergh|Lindbergh]] did ''his'' hop across the Atlantic. It had been around a long time and some people began to call it the Lindbergh Hop after 1927, although it didn't last. Then, during the marathon at [[Manhattan Casino (New York City)]], I got tired of the same old steps and cut loose with a breakaway..."<ref name=Stearns/> According to Snowden, Fox Movietone News covered the marathon and took a close-up of Shorty's feet. As told to [[Marshall Stears|Marshall]] and Jean Stearns, he was asked "What are you doing with your feet," and replied, "The Lindy". The date was June 17, 1928.<ref name=Stearns/>

The first generation of Lindy hop is popularly associated with dancers such as [["Shorty" George Snowden]], his partner Big Bea, and Leroy Stretch Jones and Little Bea. "Shorty" George and Big Bea regularly won contests at the [[Savoy Ballroom]]. Their dancing accentuated the difference in size with Big Bea towering over Shorty. These dancers specialized in so-called floor steps.<ref>
{{cite web | url = http://www.savoystyle.com/shorty_george.html | title = Lindy Hop Biographies: Shorty George Snowden | accessdate = August 8, 2007 | year = 2006 | publisher = Judy Pritchett with Frankie Manning }}</ref><ref name="tbtiad">{{cite book|title=The Black Tradition in American Dance|author=[[Richard A. Long]]|publisher=Rizzoli International Publication, Inc.|year=1989|page=33|isbn=0-8478-1092-5}}</ref>

As white people began going to Harlem to watch black dancers, according to [[Langston Hughes]]: "The lindy-hoppers at the Savoy even began to practice acrobatic routines, and to do absurd things for the entertainment of the whites, that probably never would have entered their heads to attempt for their own effortless amusement. Some of the lindy-hoppers had cards printed with their names on them and became dance professors teaching the tourists. Then Harlem nights became show nights for the Nordics."<ref>{{cite book|title=The Big Sea|author=Langston Hughes|publisher= New York: Hill and Wang|year=1940}} – cited in {{cite book|title=Black Dance in the United States from 1916 to 1970|author=Lynne Fauley Emery|publisher=National Press Books|year=1972|isbn=0-87484-203-4}}</ref>

Charles Buchanan, manager of the Savoy, paid dancers such as Shorty Snowden to "perform" for his clientele.<ref name="steppin">{{cite book |title=Steppin' on the Blues |author=Jacqui Malone |publisher=University of Illinois Press |year=1996 |pages=101–103 |isbn=0-252-02211-4 }}</ref> According to Snowden, "When he finally offered to pay us, we went up and had a ball. All we wanted to do was dance anyway."<ref name=Stearns/> When "Air steps" or "aerials" such as the Hip to Hip, Side Flip, and Over the Back (the names describe the motion of the follow in the air) began to appear in 1936, the old guard of dancers such as Leon James, Leroy Jones, and Shorty Snowden disapproved of the new moves.<ref name=Stearns/>

Younger dancers fresh out of high school (Al Minns, Joe Daniels, Russell Williams, and Pepsi Bethel) worked out the Back Flip, "Over the head", and "the Snatch".<ref name=Stearns/><ref name="tbtiad"/>

[[Frankie Manning]] was part of a new generation of Lindy hoppers, and is the most celebrated Lindy hopper in history. [[Al Minns]] and [[Pepsi Bethel]], [[Al & Leon|Leon James]], and [[Norma Miller]] are also featured prominently in contemporary histories of Lindy hop. Some sources credit Manning, working with his partner Freida Washington, for inventing the ground-breaking "Air Step" or "[[aerial (dance move)|aerial]]" in 1935. One source credits Al Minns and Pepsi Bethel as among those who refined the air step.<ref name="tbtiad"/> An Air Step is a dance move in which at least one of the partners' two feet leave the ground in a dramatic, acrobatic style. Most importantly, it is done in time with the music. Air steps are now widely associated with the characterization of lindy hop, despite being generally reserved for competition or performance dancing, and not generally being executed on any social dance floor.

Lindy hop entered mainstream American culture in the 1930s, gaining popularity through multiple sources. Dance troupes, including the [[Whitey's Lindy Hoppers]] (also known as the [[ Congaroos|Harlem Congaroos]]), Hot Chocolates and Big Apple Dancers exhibited the Lindy hop.  Hollywood films, such as [[Hellzapoppin' (film)|Hellzapoppin']] and [[A Day at the Races (film)|A Day at the Races]] began featuring the Lindy hop in dance sequences. Dance studios such as those of [[Arthur Murray]] and [[Irene and Vernon Castle]] began teaching Lindy hop. By the early 1940s the dance was known as "New Yorker" on the West Coast.<ref>"Murray in a Hurry Monday", ''Time'', October 5, 1942.</ref>

Lindy hop moved off-shore in the 1930s and 40s, again in films and news reels, but also with American troops stationed overseas, particularly in the [[United Kingdom]], [[Canada]], [[Australia]], [[New Zealand]], and other [[Allies of World War II|Allied]] nations. Although Lindy hop and jazz were banned in [[Nazi Germany]], both were popular in other European countries during this period.{{Citation needed|date=September 2008}}

In 1944, due to continued involvement in [[World War II]], the United States levied a 30 percent federal [[Excise|excise tax]] against "dancing" [[nightclub]]s. Although the tax was later reduced to 20 percent, "No Dancing Allowed" signs went up all over the country.<ref>{{cite book|title=Stomping the Blues|author=Albert Murray|publisher=Da Capo Press|year=2000|pages=109–110|isbn=0-252-02211-4}}</ref>

===Post-swing era (1950s–1960s)===
Arthur Murray's 1954 edition of ''How to Become a Good Dancer'' included four pages of instruction for swing, covering the basic Lindy step, the double Lindy hop, the triple Lindy hop, the sugar foot walk, and the tuck-in turn.<ref>''How to Become a Good Dancer''; by Arthur Murray. 1954. Simon and Schuster. Table of contents and pages 48–52; no ISBN</ref> A chapter is devoted to Lindy hop in the 1953 and 1958 editions of ''Dancing Made Easy''.<ref>Betty White, ''Dancing Made Easy''. David McKay Company, Inc., p. 177. {{LCCN|53-11379}}</ref>

The 1962 ''Ballroom Dancebook for Teachers'' included an entire chapter on "Lindy".<ref name=DanceBook>Betty White. ''Ballroom DanceBook for Teachers'', David McKay Company, 1962, pp. 131–144. LCCN 62-18465</ref>

According to the book ''Social Dance'' (copyrighted in 1969), by 1960 the Lindy hop was known as "swing".<ref>''Social Dance''. John G. Youmans. Goodyear Publishing Company, 1969, p. 25. LCCN 69-17984.</ref>

===Revival (1980s and 1990s)===
Sandra Cameron and Larry Schulz of the Cameron Dance Center Inc in New York were instrumental in bringing Al Minns and Frankie Manning back into teaching Lindy hop at their dance center.<ref name="Version 1 1998. page 5">Craig R Hutchinson. Swing Dancer: Version 1.10, a swing dancer's manual. December 1998. Potomac Swing Dance Club, Inc., pp. 5.1–5</ref> Minns joined the dance center and began a swing program there in 1981. Frankie Manning joined the Center in 1985.<ref>[http://www.sandracameron.com/programs/swing/swing_staff.html Frankie Manning], Staff, Sandra Cameron Swing.</ref>

Al Minns' early students formed the basis for the New York Swing Dance Society, established in 1985.<ref name="Version 1 1998. page 5"/>

In the 1980s, American and European dancers from California, New York, London and Sweden (such as [[Sylvia Sykes]], [[Erin Stevens]], [[Steven Mitchell (dance)|Steven Mitchell]], Terry Monaghan and Warren Heyes who formed London's [http://www.jivinglindyhoppers.com/ ''Jiving Lindy Hoppers''] performance troupe, and Stockholm's [[Rhythm Hot Shots]] / Harlem Hot Shots) went about "reviving" Lindy hop using archival films such as ''[[Hellzapoppin' (film)|Hellzapoppin']]'' and ''[[A Day at the Races (film)|A Day at the Races]]'' and by contacting dancers such as [[Frankie Manning]], [[Al Minns]], and [[Norma Miller]]. In the mid-to-late 1990s the popularity of neo swing music of the [[swing revival]] stimulated mainstream interest in the dance. The dance was propelled to wide visibility after it was featured in movies such as ''[[Swing Kids (film)|Swing Kids]]'' in 1993 and in the "Kakhis Swing" television commercials for [[Gap (clothing retailer)|GAP]] in 1998.<ref>"[http://www.washingtonpost.com/wp-srv/style/music/features/swing1026.htm Back in Swing]". ''The Washington Post''. Richard Harrington. October 26, 1998. Retrieved October 31, 2013.</ref> The popularity led to the founding of local Lindy hop dance communities in many cities.{{Citation needed|date=September 2008}}

In 1999, ''[[Swing!]]'' opened up on Broadway, featuring world-class Lindy hoppers Jenny Thomas and [[Ryan Francois]], Latin swing dancer Maria Torres and her partner Carlos Sierra-Lopez, country swing stars Robert Royston and Laureen Baldovi, and west coast swing couple Beverly Durand and Aldrin Gonzales. Carol Bentley, Scott Fowler, Caitlin Carter, Edger Godineaux, Geralyn Del Corso, and Keith Lamelle Thomas were also featured in various swing-related dance pieces in the [[Tony Award|Tony-nominated]] show during its run at the [[St. James Theatre]]. The show closed in January 2001, yet continues to be set in regional and international cities around the world.

==Current status==
{{main|Lindy Hop today}}
[[File:Lindy Hop at Washington, DC's DuPont Circle.jpg|thumb|Lindy hop dancers at DuPont Circle, Washington DC on a Saturday afternoon]]

There are thriving communities throughout the world, and Lindy hop can today be found in almost every large westernized city.

The small village of [[Herräng]] in Sweden (north of [[Stockholm]]) has unofficially become the international mecca of Lindy hop thanks to the annual [[Herräng Dance Camp]] formerly run by the Rhythm Hot Shots then passed on to new owners and operational team, with an attendance from around 40 countries.{{Citation needed|date=September 2008}} Lindy hop tends to be concentrated in small local scenes in cities in each of these countries, although regional, national, and international dance events bring dancers from many of these scenes together. Local swing dance communities in each city and country feature different local cultures. The  concept of a [[Lindy exchange]], a gathering of Lindy hop dancers in one city for several days to dance with visitors and locals, enables different communities to share their ideas with others.

Lindy hop today is danced as a [[social dance]], as a [[competitive dance]], as a [[performance dance]], and in classes, workshops, and camps. In each, partners may dance alone or together, with [[improvisation]] a central part of social dancing and many performance and competition pieces.

==In popular culture==
Lindy hop has been featured in popular media since its inception. Variants include the Double Lindy<ref name="bennett2006">Bennett, John Price and Pamela Coughenour Riemer (2006). ''Rhythmic Activities and Dance.'' Human Kinetics, ISBN 978-0-7360-5148-4</ref> and Triple Lindy.<ref name="wright2003">Wright, Judy Patterson 2003). ''Social Dance: steps to success.'' Human Kinetics, ISBN 978-0-7360-4505-6</ref>

It featured in several [[music video]]s, including [[Marilyn Manson]]'s "[[Mobscene]]", the 2002 music video to [[Elvis Presley]] vs. JXL remix of "[[A Little Less Conversation]]", the 2007 music video to [[Christina Aguilera]]'s song "[[Candyman (Christina Aguilera song)|Candyman]]",{{Citation needed|date=September 2008}} the 2008 video release from [[Millencolin]]; Detox and the music videos to [[Movits!]]'s songs "Fel Del Av Gården" and "Sammy Davis Jr.". It also featured in the 2013 music video for [[Capital Cities (band)|Capital Cities']] song "[[Safe and Sound (Capital Cities song)|Safe and Sound]]".

The [[Harlem]] Lindy hop dance club and [[zoot suit]] culture forms a colourful backdrop in the early part of [[Spike Lee]]'s film ''[[Malcolm X (1992 film)|Malcolm X]]'', starring [[Denzel Washington]] as [[Malcolm X]]. Lee appears as musician who is Malcolm's friend "Shorty".<ref>[http://www.imdb.com/title/tt0104797/ Malcolm X (1992) - IMDb<!-- Bot generated title -->]</ref>

In the ''[[The Simpsons|Simpsons]]'' episode "[[Homer vs. Dignity]]", which originally aired in 2000, [[Mr. Burns]] pranks the Springfield Zoo by having [[Homer Simpson|Homer]] disguise himself as a panda and dance the Lindy Hop for the zoo attendees.

Lindy Hop is featured in the 2007 [[Gmail]]: Behind The Scenes collaborative video by Google, viewed by more than 5 million people.<ref>[https://www.youtube.com/watch?v=qKAInP_tmHk Gmail: Behind The Scenes Final Cut- Youtube]</ref> In it, Montrealers Ann Mony and Alain Wong perform the basic swing out pattern while holding the Gmail envelope.

In the 2009 ''[[Strictly Come Dancing]]'' final the Lindy hop was performed by the two remaining contestants. In the [[Dancing with the Stars (U.S. season 8)|eighth season]] of the US version of ''[[Dancing with the Stars]]'', it was added to the list of dances along with the [[Argentine Tango]].

Lindy hop features in the 2010 Taiwanese film ''[[Au Revoir Taipei]]'', in which [[Amber Kuo]]'s character goes to dance classes at night. One of these classes is seen at the end of the film.

[[Stephen King]] makes the Lindy hop an important element in his novel ''[[11/22/63]]''.

==See also==
* [[Hand dancing]]
* [[Hollywood-style Lindy Hop]]
* [[Jitterbug]]
* [[List of lindy hop moves]]
* [[Savoy-style Lindy Hop]]
* [[Swing Camps]]

==References==
{{reflist|30em}}

==Further reading==
* Batchelor, Christian, ''This Thing Called Swing''. Christian Batchelor Books, 1997, ISBN 0-9530631-0-0
* DeFrantz, Thomas. ''Dancing Many Drums: Excavations in African American Dance''. Wisconsin: University of Wisconsin Press, 2001.
* Emery, Lynne Fauley. ''Black Dance in the United States from 1619 to 1970''. California: National Press Books, 1972.
* Friedland, LeeEllen. "Social Commentary in African-American Movement Performance." In Brenda Farnell (ed.), ''Human Action Signs in Cultural Context: The Visible and the Invisible in Movement and Dance''. London: Scarecrow Press, 1995. 136 – 57.
* Gottschild, Brenda Dixon. ''Digging the Africanist Presence in American Performance''. Connecticut and London: Greenwood Press, 1996.
* Hancock, Black Hawk. ''American Allegory: Lindy Hop and the Racial Imagination''. Chicago: University of Chicago Press, 2013.
* Hazzard-Gordon, Katrina. ''Jookin': The Rise of Social Dance Formations in African-American Culture''. Philadelphia: Temple University Press, 1990.
* Jackson, Jonathan David. "Improvisation in African-American Vernacular Dancing." ''Dance Research Journal'' 33.2 (2001/2002): 40 – 53.
* Malone, Jacqui. ''Steppin' on the Blues: The Visible Rhythms of African American Dance''. Urbana and Chicago: University of Illinois Press, 1996.
* {{cite book | last = Manning | first = Frankie | authorlink = Frankie_Manning |author2=Cynthia R. Millman  | title = Frankie Manning: Ambassador of Lindy Hop | year = 2007 | publisher = Temple University Press | location = Philadelphia, Pennsylvania | isbn = 1-59213-563-3 | url = http://www.frankiemanning.com}}
* Szwed, John F., and Morton Marks. "The Afro-American Transformation of European Set Dances and Dance Suites." ''Dance Research Journal'' 20.1 (1988): 29 – 36.
*Spring, Howard. "Swing and the Lindy Hop: Dance, Venue, Media, and Tradition". ''American Music'', Vol. 15, No. 2 (Summer 1997), pp.&nbsp;183–207.
* Thomas, Amy. ''Infinity Dance: The Move That Never Ends''. California: National Press Books, 2006.

==External links==
{{Wikibooks|Swing Dancing|Lindy Hop}}
{{commons|Lindy Hop|Lindy Hop}}
;Lindy hop history:
*[http://www.savoystyle.com/ "Savoystyle Archives of early Lindy Hop"], resource for Lindy hop history
*[http://www.dancehistory.org/ ''Dancehistory.org''], resource for jazz dance history
*[http://www.kcdance.com/catscorner/LindyHopTerms.htm "Lindy Hop Terms"]
*[http://www.jitterbuzz.com/cameron.html  "History of the Swing Revival in DC"]
*[http://www.jitterbuzz.com/decfur.html  "Lindy Hop and Retro Lifestyle"]

;Lindy hop dancing today:
<!-- please do NOT add all the local Lindy hop websites you know here, this gets out of hands otherwise! -->
*[http://www.thelindyhop.com/ "The Lindy Hop"], resource listing of many Lindy hop links including teachers, communities, and forums.
*[http://www.yehoodi.com/ ''Yehoodi'': "The website for the hardcore hep-cat swinger!"], international discussion forum for all things Lindy hop.
*[http://www.iamlindyhop.com/ "I Am Lindy Hop"], stories from modern day Lindy hoppers on Lindy hop today
<!-- remember: this is not a web directory like DMOZ, but an encyclopedia! -->
{{African-American dance}}
{{Dance}}
{{Street dance}}

[[Category:Dancesport]]
[[Category:Competitive dance]]
[[Category:Swing dances]]
[[Category:Lindy Hop| ]]
[[Category:Dance terminology]]
[[Category:Articles containing video clips]]