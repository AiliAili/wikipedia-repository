{{one source|date=April 2015}}
{{third-party|date=April 2015}}
<!-- Deleted image removed: [[File:ASN logo1.png|right|100px|]] -->
The '''American Society for Neurochemistry''' (ASN) is a [[Learned society|professional society]] for [[neurochemist]]s and [[neuroscientist]]s from North, Central, and South America and the Caribbean, whose research concerns the role and interactions of small molecules ([[proteins]], [[peptides]], [[nucleic acid]]s, [[lipids]], [[sugars]]) in the development, growth, function, and pathology of the [[nervous system]].

== History ==
The ASN was incorporated August 5, 1969,<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/articles.htm |title=Articles of Incorporation of the American Society for Neurochemistry |publisher=American Society for Neurochemistry |work=Homepage}}</ref> and is guided by a set of [[bylaws]]<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/bylaws.htm |title=Bylaws of the American Society for Neurochemistry |publisher=American Society for Neurochemistry |work=Homepage}}</ref> and standing rules<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/rules.htm |title=ASN Rules and Guidelines |work=Homepage |publisher=American Society for Neurochemistry}}</ref> that incorporate amendments to the bylaws. The first president was [[Donald B. Tower]], [[Jordi Folch Pi]] was the second president. The current president (2013–2015) is Etty Benveniste ([[University of Alabama at Birmingham]]) and the current president elect is Babette Fuss ([[Richmond, Virginia]]). Elections are held ever 2 years to elect the next president, treasurer, secretary, and council members.

== Organization ==
The officers and council members<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/council.htm |title=Officers and Council |work=Homepage |publisher=American Society for Neurochemistry}}</ref> are assisted by a number of standing committees.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/committees.htm |title=Committees |work=Homepage |publisher=American Society for Neurochemistry}}</ref> An informal history of the ASN has been put together by Claude Baxter.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/History/Contents.htm |title=History |last=Baxter |first=Claude |work=Homepage |publisher=American Society for Neurochemistry}}</ref>

== Annual meeting ==
Annual meetings include plenary lectures, symposia, colloquia and workshops over the course of 4 days.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/PastMeetings.htm |title=Past ASN Meetings |work=Homepage |publisher=American Society for Neurochemistry}}</ref> The first such meeting was held March 16–18, 1970 in [[Albuquerque, NM]].

Every 6 years, the ASN helps organize a joint meeting with the [[International Society for Neurochemistry]].

== Publications ==
The ASN publishes the textbook ''Basic Neurochemistry'' for use by undergraduate, graduate, and post graduate students and instructors. The founding editor was George Siegel, and the current chief editor is Scott Brady. It is currently in its 8th edition, which was published in December 2011.

The ASN launched an [[open access]] online journal, ''[[ASN Neuro]]'', at the 40th annual meeting which took place in March 2009. It was initially published by [[Portland Press]], and taken over by [[Sage Publications]] in 2014. ''ASN Neuro'' furthers the ASN missions to advance, promote, support, encourage and facilitate communication among cellular and molecular neuroscientists.

== Awards ==
The Jordi Folch-Pi Award is given to an outstanding young investigator who has demonstrated a high level of research competence and originality, who has significantly advanced our knowledge of neurochemistry and who shows a high degree of potential for future accomplishments.<ref>{{cite web |url=http://www.asneurochem.org/ASNarchive/History/PastAwardeesoftheJordiFolch-PiAward.htm |title=Jordi Folch-Pi Award |work=Homepage |publisher=American Society for Neurochemistry}}</ref>

The Marian Kies Memorial Award is given to a junior scientist for outstanding research conducted during graduate training.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/History/PastAwardeesoftheMarionKiesAward.htm |title=Marian Kies Award |work=Homepage |publisher=American Society for Neurochemistry}}</ref>

The Bernard Haber Award recognizes an individual whose leadership skills have fostered collaborations among the world's neuroscientists.

The ASN offers Young Investigator Educational Enhancement awards to graduate students in their last year of studies and who reside in the Americas to travel to the annual meeting.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/YoungTravelAward.htm |title=YIEE Travel Awards |work=Homepage |publisher=American Society for Neurochemistry}}</ref> Beginning at the 2010 meeting, one YIEE awardee is selected to receive the ''ASN Neuro'' travel award.

ASN also offers Young Latin American Award scholarships for young neuroscientists from [[Latin American]] countries. These awards subsidize travel expenses to attend the Annual Meeting, along with a short visit to an established neuroscience laboratory in North America.<ref>{{cite web |url=http://asneurochem.org/ASNarchive/library/YoungLatinAward.htm |title=Latin American Award |work=Homepage |publisher=American Society for Neurochemistry}}</ref>

== See also ==
* [[Society for Neuroscience]]
* [[European Society for Neurochemistry]]
* ''[[Journal of Neurochemistry]]''

== References ==
<references />

== External links ==
* {{Official website|http://asneurochem.org/}}

{{DEFAULTSORT:American Society For Neurochemistry}}
[[Category:Neuroscience organizations]]
[[Category:Learned societies of the United States]]
[[Category:1969 establishments in the United States]]
[[Category:Organizations established in 1969]]