{{Infobox artist
| bgcolour      =
| name          = Mat Rappaport
| image         = 24 r14w2 Web.jpg
| caption       = sample image from the ''resort'' photo archive
| birth_name     =
| birth_date     = {{birth-date|1971}}
| birth_place    = [[Boston, Massachusetts]]
| death_date     =
| death_place    =
| nationality    = [[United States|American]]
| field          = [[New media art]], [[Video art]], [[Photography]]
| training       = [[University of Notre Dame]], [[Tufts University]], [[School of the Museum of Fine Arts, Boston]]
| movement       =
| works          =
| patrons        =  
| awards         =  Propeller Grant (2011), Howard Fellowship (2006), Mary L. Nohl Fellowship for Individual Artists Award (2005)
| website        = {{URL|http://www.meme01.com/}}
}}

'''Mat Rappaport''' (born 1971) is an internationally exhibited [[new media]] and installation artist, curator, and educator.  He is currently an associate professor at [[Columbia College Chicago]],<ref>faculty bio http://www.colum.edu/Academics/Interarts/interdisciplinary-arts/faculty.php</ref> a board member of the New Media Caucus,<ref>listing of officers http://www.newmediacaucus.org/wp/organization/</ref> and a founding member of v1b3 (Video in the Built Environment).<ref>About Page v1b3 http://v1b3.com/?page_id=21</ref> He currently lives and works in [[Chicago]].

== Education ==

Rappaport earned a BFA from the [[School of the Museum of Fine Arts, Boston]] and [[Tufts University]].<ref>alumni listing SMFA http://www.smfa.edu/alumni-web-sites/</ref>  While an undergraduate Rappaport spend a year studying at [[Hebrew University]] and the [[Bezalel Academy of Art and Design]] in Jerusalem, Israel. Upon returning to the United States, he was an intern in Objects Conservation at the Museum of Fine Arts Boston.  Rappaport received his MFA in Studio Art from the  [[University of Notre Dame]].

== Work ==

Rappaport's projects include video installation, public art interventions, mobile media performance, and interactive installation dealing primarily with the mutability of architecture and media representation as sites of meaning. His creative works and published writing explore public art and media.<ref>Rappaport, Mat. ''States of Distraction. Media Art Strategies Within Public Conditions''. A. Aneesh (Editor), Lane Hall (Editor), Patrice Petro (Editor). Making New Worlds in Media, Art, and Social Practices. Rutgers University Press, 2011. http://rutgerspress.rutgers.edu/(S(svrtpb550lvmxjm2tv0xnjic))/catalog/productinfo.aspx?id=4104&AspxAutoDetectCookieSupport=1</ref> Specific works include ''a game we play'' an [[augmented reality]] application to impose a video game atmosphere onto the Chicago Financial District.  The project was funded by a Propeller Fund grant from threewalls in Chicago.,<ref>Announcement of 2011 Propeller fun winners http://www.propellerfund.org/news/the-2011-propeller-fund-awardees</ref> touristic intents, a multi-modal documentary art project that explores the use of architecture and tourism as a projection of political ideology and [[propaganda]].,<ref>InterArts & CBPA Newsfeed, Dec. 20, 2012 http://blogs.colum.edu/interarts-cbpa/2012/12/20/shaping-views-of-history-through-documentary-film-and-installation/</ref> and ''current'', a site-specific video and sound installation, made for showing over the Delaware River, between New York and Pennsylvania.  A flashlight is programmed to flash morse code over the river at a video screen on the opposite side.  The code spells out a portion of the Constitution about interstate trade. Earlier  works, such as ''way'', a single channel video, made in 2005 has been critically reviewed.<ref>Schumacher, Mary Louise. ''Collective Vision''. Milwaukee Journal Sentinel May 25, 2007.</ref>  His work has been featured in a wide range of exhibition venues, including the Under Surveillance exhibition at the [[John Michael Kohler Arts Center]], along with other well-known artists such as [[Trevor Paglen]] and William Betts, who deal with surveillance in the contemporary world.<ref>McNerney, Regan Golden. Essays, Interviews & Reviews Citation of "A Long Look from the Seat of Power: A review of Under Surveillance at the Kohler Arts Center," (Trevor Paglen, William Betts) Susceptible to Images, August 2008. http://drawnlots.com/newWritings.html</ref>

== Awards ==
'''2011 Propeller Grant'''<ref>''News The 2011 Propeller Fund Awardees'', Chicago, IL http://www.propellerfund.org/news/the-2011-propeller-fund-awardees</ref><br>
Propeller Fund, Chicago, IL<br>
'''2006 Howard Fellowship'''<ref>''Recipients of Howard Fellowships since 1999'', Howard Foundation Brown University, Providence, RI http://brown.edu/Divisions/Graduate_School/Howard_Foundation/Previous.html</ref><br>
Howard Foundation, Brown University, Providence, RI<br>
'''Center for 21st Century Studies Fellowship'''<ref>''Center fellows Since 1974'', Center for 21st Century Studies, Milwaukee, WI http://www4.uwm.edu/c21/pages/research/fellowships/oldfellows.html</ref>

== References ==

{{Reflist}}
<!-- Be sure to cite all of your sources in <ref>...</ref> tags and they will automatically display when you hit save. The more reliable sources added the better! See [[Wikipedia:REFB]] for more information-->

== External links ==
* [http://www.meme01.com/ Studio of Mat Rappaport]
* [http://www.v1b3.com/ v1b3 (video in the built environment)]

{{DEFAULTSORT:Rappaport, Mat}}
[[Category:1971 births]]
[[Category:Living people]]