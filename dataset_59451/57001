{{Infobox journal
| title = Fishery Bulletin
| cover = [[File:Fishery Bulletin.jpg]]
| editor = Richard D. Brodeur
| discipline = [[Zoology]]
| former_names = Bulletin of the United States Fish Commission, Bulletin of the Bureau of Fisheries, Bulletin of the United States Fish Commission, Fishery Bulletin of the Fish and Wildlife Service
| abbreviation = Fish. Bull.
| publisher = [[National Oceanic and Atmospheric Administration]]
| country = United States
| frequency = Quarterly
| history = 1881–present
| openaccess = Yes
| license = public domain<ref name="noaa.gov"/><!--"Although the contents of the Fishery Bulletin have not been copyrighted and may be reprinted entirely, reference to source is appreciated."-->
| impact = 1.135
| impact-year = 2012
| website = http://fishbull.noaa.gov/index.html
| link1 = http://fishbull.noaa.gov/fcontent.htm
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 01783998
| LCCN = 72625459
| CODEN = FSYBAY
| ISSN = 0090-0656
| eISSN = 
}}
The '''''Fishery Bulletin''''' is a quarterly [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[National Oceanic and Atmospheric Administration]].

It was established in 1881 and was until 1903 published as the ''Bulletin of the United States Fish Commission'' by the [[United States Fish Commission]]. The journal then went through a number of changes in its name: ''Bulletin of the Bureau of Fisheries'' (1904–1911), ''Bulletin of the United States Fish Commission'' (1912–1940), ''Fishery Bulletin of the Fish and Wildlife Service'' (1941–1970), and finally from 1971, ''Fishery Bulletin''.<ref name="noaa.gov">{{cite web |url=http://docs.lib.noaa.gov/rescue/Fish_Commission_Bulletins/data_rescue_fish_commission_bulletins.html |title=Bulletin of the United States Fish Commission |author=NOAA Central Library |date=2009 |accessdate=19 May 2012}}</ref> All content has been scanned and is available through the journal's page or the site maintained by the [[NOAA Central library]].

==References==
{{reflist}}

==External links==
{{commons category|Images from Fishery Bulletin}}
* {{Official|http://fishbull.noaa.gov/index.html}}
* [http://docs.lib.noaa.gov/rescue/Fish_Commission_Bulletins/data_rescue_fish_commission_bulletins.html ''Bulletin of the United States Fish Commission''] [[NOAA Central library]] (on-line access to volumes until 1998)

[[Category:Ichthyology journals]]
[[Category:English-language journals]]
[[Category:National Oceanic and Atmospheric Administration]]
[[Category:Publications established in 1881]]
[[Category:Quarterly journals]]
[[Category:Academic journals published by the United States government]]
[[Category:1881 establishments in the United States]]