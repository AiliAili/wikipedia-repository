{{About|the 1977 sitcom|other uses|All That Glitters (disambiguation){{!}}All That Glitters}}
{{Infobox television
| show_name            = All That Glitters 
| image                = Atgpromo.jpg
| caption              = Screen shot from promotional ad for the series, <br>from [[Chicago]] station [[WFLD]]
| show_name_2          = 
| genre                = [[Sitcom]]<br>Soap opera parody
| creator              = [[Norman Lear]]
| writer               = 
| director             = [[James Frawley]]<br>[[Herbert Kenwith]]
| starring             = 
| narrated             = 
| theme_music_composer = [[Alan and Marilyn Bergman]] 
| opentheme            = "Genesis Revisited" performed by [[Kenny Rankin]]
| composer             = Ray Brown<br>Bobby Knight<br>[[Shelly Manne]]
| country              = United States
| language             = English
| num_seasons          = 1
| num_episodes         = 65
| list_episodes        = 
| executive_producer   = Norman Lear
| producer             = Norman Lear<br>Stephanie Sills
| editor               = 
| cinematography       = 
| camera               = [[Multiple-camera setup|Multi-camera]] 
| runtime              = 25 mins. 
| company              = Norman Lear/Tandem Productions
| distributor          = [[Sony Pictures Television]]
| channel              = [[Television syndication|Syndicated]]
| picture_format       = 
| audio_format         = [[Monaural]] 
| first_aired          = {{start date|1977|04|18}}
| last_aired           = {{end date|1977|07|15}}
}}

'''''All That Glitters''''' is an American [[sitcom]] by producer [[Norman Lear]]. It consisted of 65 episodes and aired between April 18 and July 15, 1977 in [[broadcast syndication]]. The show, a spoof of the [[soap opera]] format, depicted the trials and tribulations of a group of executives at the Globatron corporation. The twist of the series was that it was set within a world of complete role-reversal: Women were the "stronger sex," the executives and breadwinners, while the "weaker sex" – the men – were the secretaries or stay-at-home [[househusband]]s. Men were often treated as sex objects.

The series features [[Eileen Brennan]], [[Greg Evigan]], [[Lois Nettleton]], [[Gary Sandy]], [[Tim Thomerson]] and [[Jessica Walter]]. Comic actor and cartoon voice artist [[Chuck McCann]] was also a regular. [[Linda Gray]] played [[transgender]]<ref name=time>{{cite news|last=Clarke|first=Gerald|title=Eve's Rib and Adam's Yawn|work=TIME|date=1977-04-25|url=http://www.time.com/time/magazine/article/0,9171,918874,00.html|accessdate=2008-11-25| archiveurl= https://web.archive.org/web/20081215121022/http://www.time.com/time/magazine/article/0,9171,918874,00.html| archivedate= 15 December 2008 <!--DASHBot-->| deadurl= no}}</ref> fashion model Linda Murkland, the first transgender series regular on American television.<ref>Stein, p. 177</ref>

Before and after its premiere, ''All That Glitters'' was negatively received and the series lasted just 13 weeks.

==Production==
''All That Glitters'' was series creator Norman Lear's attempt to duplicate his success with the syndicated soap opera spoof ''[[Mary Hartman, Mary Hartman]]''. Lear described the premise simply: "God created Eve first, took out her rib and gave her a companion so she wouldn't be lonely."<ref name = time /> Lear came up with the idea on a trip to [[Washington, D.C.]]: <blockquote>"I had visited the Institute of Policy Studies, and I just loved the whole thing. And I thought there was a series in it—a five-times-a-week series: I went to bed thinking about that, and I woke up the next morning thinking what would happen if the male-female equation were changed? What would happen if the women had all the power and all the advantage, and the men had what the women normally would have?”<ref name =newtimes /></blockquote>

The world of ''All That Glitters'' had always been female-dominated but Lear also used the series to comment on changing sex roles in the United States in the 1970s.<ref>{{cite news|last=Sharbutt|first=Jay|title=Women dominate new show in pattern of 'Mary Hartman'|work=Chillicothe Constitution-Tribune|page=13|publisher=Associated Press|date=1976-10-29}}</ref>

Former [[Major League Baseball]] player [[Wes Parker]] almost literally walked into his role. He was doing [[play-by-play]] reporting for a [[Los Angeles]] television station owned by Lear's partner, [[Jerry Perenchio]]. "Lear casually asked if I'd be interested in the part. I said yes, but knew it was out of the question, because in real life things don't happen that way. Nobody walks in and gets on a Norman Lear show. I read for the part, got it and didn't sleep at all that night."<ref>{{cite news|last=Leggett|first=William|title=Gold Glover Snags A Big Role|work=Sports Illustrated|date=1977-05-09|url=http://vault.sportsillustrated.cnn.com/vault/article/magazine/MAG1092385/index.htm|accessdate=2008-12-01}}</ref> [[Linda Gray]] was somewhat non-plussed upon being offered the role of transgender Linda Murkland. "I remember meeting Norman and him saying, 'You'll be perfect for the role.' I didn't know whether to take that as a compliment or what."<ref>{{cite news|last=Stanford|first=Peter|title=How do I look? Actress Linda Gray doesn't mind appearing nude on stage|work=The Independent|date=2001-10-20|url=http://findarticles.com/p/articles/mi_qn4158/is_20011020/ai_n14436495|accessdate=2008-12-01}} {{Dead link|date=September 2010|bot=H3llBot}}</ref> To prepare for her role, Linda Gray asked Lear to arrange for her to meet with a transgender woman. Gray met with her for several hours prior to the beginning of filming and on a couple of occasions during production.<ref>{{cite news|last=Bocchini|first=Paul|title=Scene-stealer Linda Gray|work=FAB National magazine #16|pages=56–57, 76|year=1999}}</ref> [[Lois Nettleton]] reportedly based her characterization of Christina Stockwood on [[Clark Gable]].<ref name = time /> Production started in early March 1977<ref>{{cite news|last=Brown|first=Les|title=Norman Lear plans summer replacement for Mary Hartman, Mary Hartman TV...|work=New York Times Abstracts|page=63, col. 1|date=1977-03-08|publisher=New York Times Company}}</ref> with director Herbert Kenwith.<ref name="NYT">{{cite web|title=All That Glitters - More Credits For 'All That Glitters'|url=http://tv.nytimes.com/show/153259/All-That-Glitters/credits|accessdate=2008-12-02|work=New York Times|publisher=New York Times Company}}</ref>

In test screenings prior to its premiere, reaction to the show was sharply divided. According to executive producer [[Stephanie Sills]], the strongest negative reaction came from male executives. "They didn't mind being portrayed by women. It was simply that they detest the way we depicted them."<ref name = time /> [[Feminism|Feminists]] were uncertain how to react to the series, with some being concerned that audiences would not perceive the show as satire but as an attempt to represent how a female-dominated society would actually operate.<ref name = time /> Lear marketed the program through his company, TAT Syndication.<ref>Television/radio Age, p. 38</ref> The series ran five nights a week.

==Cast==
* [[Barbara Baxley]] – L.W. Carruthers, President of Globatron
* [[Eileen Brennan]] – Ma Packer
* [[Vanessa Brown]] – Peggy Horner, Globatron executive
* [[Anita Gillette]] – Nancy Langston, Globatron executive
* [[Linda Gray]] – Linda Murkland, model
* [[Jim Greenleaf]] – Jeremy Stockwood, Christina's son
* [[David Haskell]] – Michael McFarland, Andrea's boyfriend
* [[Chuck McCann]] – Bert Stockwood, Christina's husband
* [[Lois Nettleton]] – Christina Stockwood, Globatron executive
* [[Wes Parker]] – Glenn Langston, Nancy's husband
* [[Gary Sandy]] – Dan Kincaid, Globatron secretary
* [[Louise Shaffer]] – Andrea Martin, lawyer
* [[Marte Boyle Slout]] – Grace Smith, Globatron executive
* [[Tim Thomerson]] – Sonny Packer
* [[Jessica Walter]] – Joan Hamlyn, agent<ref>{{cite web|title=All That Glitters - Full Acting Credits|url=http://tv.nytimes.com/show/153259/All-That-Glitters/cast|accessdate=2008-12-02|work=New York Times|publisher=New York Times Company}}</ref>

==Critical reaction==
''All That Glitters'' debuted the week of April 18, 1977 on about 40 stations in late-night syndication.<ref name=time /> It was poorly critically received, with one reviewer going so far as to call the show's theme song "blasphemous" for suggesting that God was female and created Eve first.<ref>{{cite news|last=Fulkerson|first=Perry|title=All That Glitters Tarnished|work=The Evening Independent|page=46|date=1977-04-16|url=https://news.google.com/newspapers?id=mf8LAAAAIBAJ&sjid=VVgDAAAAIBAJ&pg=6692,398123&dq=all-that-glitters+blasphemous|accessdate=2008-11-27}}</ref> [[Time (magazine)|''Time'' magazine]] sharply criticized the series, calling it "embarrassingly amateurish", with "flaccid" and "wearying" jokes, flat writing, "mediocre" acting and "aimless" direction.<ref name = time /> The ''[[Wall Street Journal]]'' concurred, saying that while the series' role-reversal premise may have been adequate for a play or film, it was too limiting to serve as the basis for a continuing series. These limitations showed up most clearly, the ''Journal'' says, in the lead performances. Although praising the performers themselves as talented, they are cited for being "unable to infuse much life into their roles".<ref name = wsj>{{cite news|last=Adler|first=Richard P.|title='All that glitters' premise limiting|work=The Pocono Record|page=25|date=1977-06-11|publisher=Wall Street Journal|accessdate=2008-11-27}}</ref> The ''Journal'' pegs the fundamental problem with ''All That Glitters'' as that "its characters are not people at all, merely composites of the least attractive characteristics of each sex. The satire focuses not on the way real, recognizable people behave, but on stereotypes and cliches about masculine and feminine attitudes. Even when stood on their heads, they still remain stereotypes and cliches."<ref name = wsj />

''[[New Times Magazine]]'' was much more receptive to the series. Although labeling it "unquestionably the weirdest [show] that Lear has ever produced",<ref name=newtimes>{{cite news|last=Nadel|first=Gerry|title=All in his Family|work=New Times Magazine|date=1977-07-08|url=http://normanlear.com/backstory_press_4.html|accessdate=2008-11-27| archiveurl= https://web.archive.org/web/20090105202540/http://www.normanlear.com/backstory_press_4.html| archivedate= 5 January 2009 <!--DASHBot-->| deadurl= no}}</ref> ''New Times'' found that the series was not "a satire of mannerisms but of attitudes".<ref name = newtimes /> ''All That Glitters'' required that viewers [[Close reading|watch closely]] to pick up on the subtleties and nuances, "not so much for what the show says, but for the ''way'' that it's said".<ref name = newtimes />

''All That Glitters'', after initially capturing 20% of viewers in major markets in its opening weeks, had lost about half of that audience mid-way through its run.<ref name = newtimes /> The series was cancelled after 13 weeks, last airing on July 15, 1977. Although the show was panned, it and Lear, along with ''Mary Hartman, Mary Hartman'', are credited with expanding the subject matter that television producers were able to explore with lessened fear of antagonizing sponsors or viewers.<ref>Hilliard, p. 43</ref>

In the years since the series, it has garnered something of a positive reputation, with one critic listing it and other Lear efforts as "imaginative shows that contained some of the most striking satires of television and American society ever broadcast".<ref>Kellner, p. 59</ref>

While the show itself was unsuccessful, it did spawn a hit song. "[[You Don't Bring Me Flowers (song)|You Don't Bring Me Flowers]]", which had been written with the intention of its being the theme song, was recorded by [[Neil Diamond]] and [[Barbra Streisand]] and made it to #1 on the [[Billboard Hot 100|''Billboard'' Hot 100]]. By the time the show made it to air, another song had been chosen as the theme.<ref>{{cite web|title=Alan and Marilyn Bergman on Songwriting: Part 1|url=http://www.ascap.com/musicbiz/bergman-part1.html|date=November 1996|publisher=American Society of Composers and Publishers|accessdate=2008-12-01| archiveurl= https://web.archive.org/web/20081212095613/http://www.ascap.com/musicbiz/bergman-part1.html| archivedate= 12 December 2008 <!--DASHBot-->| deadurl= no}}</ref> The replacement, "Genesis Revisited", was later described by the ''[[New York Times]]'' as "sparkl[ing] with witty rhymes and a punchy good humor".<ref>{{cite news|last=Holden|first=Stephen|title=POP REVIEW; Retro Retrospective of Movie Songs|work=New York Times|date=1995-01-24|url=https://query.nytimes.com/gst/fullpage.html?res=990CE7DE173FF937A15752C0A963958260l|accessdate=2008-11-27}}</ref> The song was performed by [[Kenny Rankin]]. The lyrics for both songs were written by [[Alan and Marilyn Bergman]] (the music for "Genesis Revisited" is credited solely to Alan Bergman).<ref name="NYT"/>

==References==
{{reflist}}

===Bibliography===
* {{cite book|last=Hilliard|first=Robert L.|title=Media, Education, and America's Counter-culture Revolution: Lost and Found Opportunities for Media Impact on Education, Gender, Race, and the Arts|year=2001|publisher=Ablex|location=Westport, Connecticut|isbn=1-56750-512-0|oclc=43370194}}<!--|accessdate=2008-12-02-->
* {{cite book|last=Kellner|first=Douglas|authorlink=Douglas Kellner|title=Television and the Crisis of Democracy|year=1990|publisher=Westview Press|location=Boulder, Colorado|isbn=0-8133-0549-7|oclc=21876111}}<!--|accessdate=2008-12-02-->
* {{cite book|last=Stein|first=Marc|title=Encyclopedia of Lesbian, Gay, Bisexual, and Transgender History in America|volume=3|year=2004|publisher=Charles Scribner's Sons/Thomas/Gale|location=New York|isbn=0-684-31264-6|oclc=52819577}}<!--|accessdate=2008-12-02-->
* ''Television/radio Age'' (1976). New York, Television Editorial Corp. ISSN 0040-277X. OCLC 2246124.

==External links==
*{{IMDb title|id=0075658|title=All That Glitters}}

{{Norman Lear Shows}}
{{good article}}

{{Portal bar|1970s}}

{{DEFAULTSORT:All That Glitters (Tv Series)}}
[[Category:1977 American television series debuts]]
[[Category:1977 American television series endings]]
[[Category:1970s American comedy television series]]
[[Category:1970s American television series]]
[[Category:American LGBT-related television shows]]
[[Category:American television sitcoms]]
[[Category:American television soap operas]]
[[Category:English-language television programming]]
[[Category:First-run syndicated television programs in the United States]]
[[Category:Television series by Sony Pictures Television]]
[[Category:Television series created by Norman Lear]]
[[Category:Television shows set in Washington, D.C.]]
[[Category:Transgender-related television programs]]