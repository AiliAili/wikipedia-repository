<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Model 9 Orion
  |image =VarneyOrion.jpg
  |caption = "North Wind" - one of six Orions owned by [[Varney Speed Lines]] of [[Burbank, California|Burbank]], [[California]]
}}{{Infobox Aircraft Type
  |type = Airliner
  |manufacturer = [[Lockheed Corporation|Lockheed Aircraft Limited]]
  |designer = Richard A. von Hake
  |first flight = [[1931 in aviation|1931]]
  |introduced = 1931
  |retired =
  |status =
  |primary user =  
  |more users = 
  |produced = 
  |number built = 35 (36?)
  |unit cost = US$25,000
  |variants with their own articles = 
}}
|}

The '''Lockheed Model 9 Orion''' is a single-engined [[passenger aircraft]] built in 1931 for commercial airlines. It was the first [[airliner]] to have retractable landing gear and was faster than any [[military aircraft]] of that time. Designed by Richard A. von Hake, it was the last wooden monoplane design produced by the [[Lockheed Aircraft Corporation]].

==Design==
The Orion was the last design using many identical elements from the Lockheed designs preceding it. It primarily used all the elements of the [[Lockheed Altair|Altair]], but included a forward top [[cockpit]] similar to the [[Lockheed Vega|Vega]], plus the [[NACA cowling]] introduced in the [[Lockheed Air Express|Air Express]].<ref name="eden">Eden, and Moeng, eds. 2002, p. 899.</ref> Lockheed used the same basic fuselage mold and wing for all these wooden designs (the [[Lockheed Explorer|Explorer]] wing was unique), hence the close similarities between them. The Orion featured an enclosed cabin with seating for six [[passenger]]s. The Orion received its [[Type certificate|Approved Type Certificate]] on May 6, 1931.<ref name="Francillon">Francillon, 1987.</ref>

Gerard F. Vultee was Lockheed's chief engineer in 1928 through 1931 and was involved in the designs of all the Lockheed variants of that time and specifically designed Charles Lindbergh’s [[Lockheed Sirius|Sirius]].<ref>Vultee, Gerald. [http://98.230.172.128:8080/aero/Vega%20Fuse.pdf  Fabrication of the Lockheed "Vega" Airplace-Fuselage]. Los Angeles Aeronautic Meeting Paper, November 1928.</ref>

==Operational history==
Although designed with the passenger market in mind, its speed made it a natural for air races. The first [[Bendix Trophy|Bendix race]] of 1931 had a showing of two Orions, three Altairs and one Vega in a race that had only nine aircraft competing. On 11 July 1935, [[Laura Ingalls (aviator)|Laura H. Ingalls]] flew a Lockheed Orion, powered by a [[Pratt & Whitney Wasp]] engine, from [[Floyd Bennett Field]] to [[Burbank, California]], establishing an East-West record for women. Two months later she flew it back to set a West-East record.<ref name="Francillon"/>

The first Orion entered service with [[Bowen Air Lines]] at [[Fort Worth, Texas]], in May 1931.<ref name="eden"/> [[Northwest Airlines|Northwest Airways]], later renamed to Northwest Airlines, operated the plane from 1933 to 1935.<ref>https://www.deltamuseum.org/exhibits/delta-history/family-tree/northwest-airlines</ref>[[American Airways]], itself also renamed to [[American Airlines]] in 1934, operated several 9D Orions. Many safe miles were flown in airline service and the headlines won by a few expert speed pilots proved the advanced design and reliability of the Orion. Those that went into airline use as a passenger transport had their lifespan limited, however. In 1934 the [[Civil Aeronautics Authority]] issued a ruling prohibiting further use of single-engined passenger aircraft from operating on all major networks. It also became mandatory to have a [[copilot]] and therefore a two-seat cockpit arrangement on all such flights. The requirements of the ruling brought an end to the "Orion" as a passenger-carrying airlines airplane. They were then used for [[cargo]] or [[mail]] carrying or sold for private use and charter. Because the aircraft had a complicated wood construction and needed to be sent back to Lockheed in Burbank California to be repaired, they were often disposed of after any type of significant accident. At least 12 of the used "Orions" were purchased for service in the [[Spanish Civil War]] and destroyed in use.<ref name="Francillon"/>

In 1935 a single Model 9 Orion was modified by Lockheed as a news camera plane for the [[Detroit News]].  To work in the role, a pod was built into the front leading edge of the right wing about eight feet out from the fuselage.  This pod had a glass dome on the front and mounted a camera.  To aim the camera the pilot was provided with a primitive grid similar to a gunsight on his windshield.<ref>[https://books.google.com/books?id=wt8DAAAAMBAJ&pg=PA513&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=zHM2Tpf-IeuFsgLc_MH3Cg&sa=X&oi=book_result&ct=result&resnum=2&ved=0CCwQ6AEwATgK#v=onepage&q=Popular%20Science%201935%20plane%20%22Popular%20Mechanics%22&f=true "Flying Camera Aimed Like a Machine Gun"] ''Popular Mechanics'', April 1935</ref>

The '''Orion Explorer'''  was a modified '''9E'''. It had a damaged wing replaced with the wing of the Explorer 7 after a crash, and was fitted with a 600&nbsp;hp (482&nbsp;kW) [[Pratt & Whitney Wasp]] S3H1 engine. Fixed landing gear and later floats were also fitted. It was used by [[Wiley Post]] and [[Will Rogers]] for a round-the-world flight attempt, but both men died when the aircraft crashed in [[Alaska]] on 15 August 1935.<ref name="eden"/>

==Variants==
[[File:Lockheed Model 9 Orion Swissair.jpg|thumb|right|A Swissair Orion undergoes flight tests]]
[[File:Lockheed UC-85 Orion.jpg|thumb|right|The UC-85 in 1943]]
;Orion 9: 14 built, 410&nbsp;hp (306&nbsp;kW) [[Pratt & Whitney Wasp]] A or 420&nbsp;hp (313&nbsp;kW) [[Pratt & Whitney Wasp]] C<ref name="eden"/>
;Orion 9A Special: one aircraft with 450&nbsp;hp (336&nbsp;kW) [[Pratt & Whitney Wasp]] SC engine<ref name="eden"/>
;Orion 9B: two aircraft supplied to [[Swissair]], 575&nbsp;hp (429&nbsp;kW) [[Wright R-1820]]-E engine<ref name="eden"/>
;Orion 9C: redesignated Altair DL-2A<ref name="eden"/>
;Orion 9D: 13 built<ref name="eden"/>
;Orion 9E: three aircraft with 450&nbsp;hp (336&nbsp;kW) [[Pratt & Whitney Wasp]] SC-1 engine<ref name="eden"/>
;Orion 9F: one executive aircraft with a 645&nbsp;hp (481&nbsp;kW) Wright R-1820-F2 engine<ref name="eden"/>
;Orion 9F-1: one executive aircraft with a 650&nbsp;hp (485&nbsp;kW) Wright SR-1820-F2 engine<ref name="eden"/>
;UC-85: one '''Orion 9D''' to [[USAAF]] in June 1942<ref name="eden"/>
;Orion-Explorer: modified '''Orion 9E''', 600&nbsp;hp (482&nbsp;kW) [[Pratt & Whitney Wasp]] S3H1 engine<ref name="eden"/>

==Survivors==
[[File:Lockheed Model 9 Orion NACA image 6993.jpg|thumb|right|The Shellightning in October 1932]]
[[File:Lockheed Orion 9C.JPG|thumb|The only existing Orion, [[Swiss Transport Museum]], Lucerne]]

In all, Lockheed built a total of 35 Orions costing $25,000 each new. It is not known if any survived past the 1940s except the one that survives to the present day. This lone remaining Orion was originally built as an experimental Altair with a metal fuselage. This Altair (built in 1931) was damaged in a [[belly landing]] accident in Columbus, Ohio, in 1933. It was returned to Lockheed where it was converted in 1934 to an Orion 9C configuration by the original designer of the Orion, Richard A. von Hake, and others who worked for free during a slow period when the Lockheed factory was going into bankruptcy.

A valid argument has been raised that since the fuselage, wing and tail of both planes are identical, and that it was also rebuilt by the original designer at the Lockheed plant, it may be considered an actual Orion (#36) instead of a modified Altair. In any case, it was sold to Shell Aviation Corp., painted yellow-orange and red and named ''"Shellightning."'' It was used by Shell's aviation manager, [[James H. Doolittle]], on cross-county and exhibition flights. Jimmy Doolittle made hundreds of trips in this Lockheed, and the aircraft  was very much in evidence at air shows, airport dedications, and business meetings across the territories of all three Shell companies in the United States.

In 1936, "Shellightning" was again involved in an accident, in St. Louis, and was stored there. Two years later, [[Paul Mantz]] caught the racing bug in addition to his aeronautical movie work. He bought the damaged "Shellightning" and had it rebuilt at [[Parks College of Engineering, Aviation and Technology|Parks Air College]] in St. Louis, Missouri with a more powerful Wright Cyclone engine and some streamlining to add to its speed. It was repainted red with white trim and Mantz flew the plane in the Bendix Races in 1938 and 1939, coming in third both times. In 1943, he sold the plane and it went through a series of owners until Mantz bought it back in 1955. He retained ownership until selling it to TallMantz Aviation, Inc. in 1962.

In 1964, the plane was sitting out in the open on the flightline at Orange County Airport, now John Wayne Airport, in blue-and-white American Airways trim. Some time in the 1960s it was purchased by [[Swissair|Swiss Air]] and rebuilt to flying status by the famous "Fokker" restoration team and is on display at the [[Swiss Transport Museum]] in [[Lucerne]], [[Switzerland]] in the livery of the original Swiss Air Orion.<ref>[http://www.luftfahrtmuseum.com/htmi/ite/l9.htm]</ref>

==Operators==
;{{MEX}}
* [[Lineas Aereas Occidentales]]

;{{flag|Switzerland}}
* [[Swissair]]

;{{flag|Spain|1931}}
* [[Spanish Republican Air Force]] from [[LAPE]]

;{{USA}}
* [[Alaska Star Airlines]]
* [[American Airlines|American Airways]]
* Air Express
* Bowen Air Lines
* [[The Detroit News|Detroit News]]
* [[Hal Roach Studios]]
* [[Northwest Airlines|Northwest Airways]]
* [[Paul Mantz]] 
* [[Trans World Airlines|Transcontinental and Western Air/TWA]]
* [[United States Army Air Forces]]
* [[Shell Oil]]
* [[Varney Speed Lines]]
* Wyoming Air Service

==Specifications (Orion 9D)==
{{Aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=''The Complete Encyclopedia of World Aircraft''<ref name="eden"/>
|crew=one, pilot
|capacity=
|payload main=
|payload alt=
|length main= 28 ft 4 in
|length alt= 8.64 m
|span main= 42 ft 9.25 in
|span alt= 13.04 m
|height main= 9 ft 8 in
|height alt= 2.95 m
|area main= 294.1 ft²
|area alt= 27.32 m²
|airfoil=
|empty weight main= 3,640 lb
|empty weight alt= 1,651 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 5,200 lb
|max takeoff weight alt= 2,359 kg
|more general=
|engine (prop)= [[Pratt & Whitney Wasp]] S1D1
|type of prop=
|number of props=1
|power main= 550 hp
|power alt= 410 kW 
|power original=
|max speed main=192 kn at sea level 
|max speed alt= 220 mph, 354 km/h
|cruise speed main= 178 kn
|cruise speed alt= 205 mph, 330 km/h
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 652 nmi
|range alt= 750 mi, 1,159 km 
|ceiling main= 22,000 ft 
|ceiling alt= 6,705 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{Aircontent|
|sequence=
|similar aircraft=
*[[Heinkel He 70]]
|related=
* [[Lockheed Air Express]]
* [[Lockheed Explorer]]
* [[Lockheed Model 8 Sirius]]
* [[Lockheed Vega]]
|lists=
* [[List of aircraft of the Spanish Republican Air Force]]
* [[List of aircraft of World War II]]
* [[List of Lockheed aircraft]]
}}

==References==
;Notes
{{Reflist}}
;Bibliography
* Francillon, René J, ''Lockheed Aircraft since 1913''. Naval Institute Press: Annapolis, 1987.
* Eden, Paul and Moeng, Soph,  eds. ''The Complete Encyclopedia of World Aircraft''. London: Amber Books Ltd., 2002. ISBN 0-7607-3432-1.

==External links==
{{commons category|Lockheed Model 9 Orion}}
* [http://www.pionnair-ge.com/spip1/spip.php?article118 Lockheed Orion on Swissair flights] {{fr}}

{{USAF transports}}
{{Lockheed aircraft}}

[[Category:Lockheed aircraft|Orion]]
[[Category:United States airliners 1930–1939]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]