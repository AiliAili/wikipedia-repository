{{Orphan|date=February 2014}}
{{Infobox journal
| title = Hand Therapy
| editor = Christina Jerosch-Herold
| discipline = [[Physical medicine and rehabilitation]]
| former_names = The British Journal of Hand Therapy
| abbreviation = hand ther.
| publisher = [[SAGE Publications]] on behalf of the British Association of Hand Therapists and the European Federation of Societies for Hand Therapy
| country =
| frequency = Quarterly
| history = 1991-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://hth.sagepub.com/
| link1 = http://hth.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hth.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1758-9983
| eISSN = 1758-9991
| OCLC = 570804106
| LCCN =
}}

'''''Hand Therapy''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers four times a year in the field of [[Physical medicine and rehabilitation]]. The journal's [[Editor-in-Chief]] is [[Christina Jerosch-Herold]] ([[University of East Anglia]], Norwich). It has been in publication since 1991 and is currently published by [[SAGE Publications]] in association with the [[British Association of Hand Therapists]] and the [[European Federation of Societies for Hand Therapy]].

== Scope ==
''Hand Therapy'', published quarterly, publishes review articles, research articles, audits and case reports on the theory and practice of [[physiotherapy]] and [[occupational therapy]] for the hand. It also includes related fields of interest to hand therapists and surgeons, occupational therapists, physiotherapists and other hand specialists.

== Abstracting and indexing ==
''Hand Therapy'' is abstracted and indexed in the following databases:
:*[[Scopus]]<ref>{{cite web|title=Scopus entry for Hand Therapy|url=http://www.scopus.com/source/sourceInfo.url?sourceId=21100202930&origin=resultslist|publisher=Scopus|accessdate=13 November 2013}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://hth.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Rehabilitation medicine journals]]
[[Category:Publications established in 1991]]
[[Category:Quarterly journals]]


{{med-journal-stub}}