{{Orphan|date=May 2013}}

{{Infobox person
| honorific_prefix          =
| name                      = Peter Allen <!-- include middle initial, if not specified in birth_name -->
| honorific_suffix          = 
| image_size                = 
| alt                       = 
| caption                   = 
| birth_name                = 
| birth_date                = {{Birth date|1921|12|13}}
| birth_place               = [[Philadelphia, Pennsylvania]], United States
| death_date                ={{nowrap| {{death date and age|2014|11|17|1921|12|13}} }}
| death_place               = Oakville, Ontario, Canada
| monuments                 = 
| residence                 = [[Oakville, Ontario]]
| nationality               = Canadian
| other_names               = 
| ethnicity                 = Anglosaxon
| citizenship               = [[Canadians|Canadian]]
| alma_mater                = [[University of Toronto]], 1946
| occupation                = [[Surgeon]]
| years_active              = 1954 to 2006
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = first open heart operation in British Columbia
| awards                    = McLaughlin Fellowship and Nuffield Travelling Fellowship
| signature                 = 
| signature_alt             = 
| signature_size            = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
}}
'''Peter Allen''' (December 13, 1921 – November 17, 2014) was a Canadian surgeon who played a leading role in improving cardiac surgery techniques. Along with Dr. Philip Ashmore, Dr. W.G. (Bill) Trapp and Dr. Ross Robertson, he performed the first [[Cardiac surgery|Open Heart Surgery]] in [[British Columbia]] on 29 October 1957 at [[Vancouver General Hospital]], by closing an [[Atrial septal defect|Atrial Septal Defect (ASD)]] in 9 year old John Evans, using [[Cardiopulmonary bypass|Cardiopulmonary Bypass (CPB)]].<ref name="Surgical Times">The Surgical Times, University of British Columbia, Department of Surgery http://www.surgery.ubc.ca/files/times/TimesW07.pdf</ref><ref>Proust Physicians Questionnaire - Dr. Peter Allen http://www.bcmj.org/proust-physicians/proust-questionnaire-peter-allen-md</ref>

==  Background  ==

Allen grew up in [[Vancouver, British Columbia]]. In 1946, he obtained a MD from the [[University of Toronto]].  In 1953, he received a Fellowship in [[general surgery]] from the [[Royal College of Physicians and Surgeons of Canada]] and, in 1964, a Fellowship in [[thoracic surgery]] from the same institution.

His post graduate training included one year of [[internal medicine]] at the [[University of Amsterdam]], the [[Netherlands]] followed by 4 1/2 years at the [[University of Toronto]] in the general surgery program at the [[Hospital for Sick Children]] and the [[Toronto General Hospital]].<ref>Dr. William E. Gallie, The Canadian Medical Hall of Fame http://www.cdnmedhall.org/dr-william-e-gallie</ref>

Returning to Vancouver, between early 1954 to mid-1956, he developed a practice in general surgery. During that period he held a research position at the British Columbia Research Institute under the direction of Dr. Kenneth Evelyn. The project tested the feasibility of replacing a diseased [[abdominal aorta]] with a mesh supported [[Vena cavae|vena cava]] autograft<ref name="Surgical Times"/> In mid 1956 he began a year of training in [[Cardiac surgery]] with [[C. Walton Lillehei]], the originator of [[Open-heart surgery]], at the [[University of Minnesota]] in [[Minneapolis]].  In early 1957, he participated in the operation where the world’s first artificial cardiac pacemaker was employed by Lillehei.

==Career==
In mid 1957 Allen returned to Vancouver and formed a cardiac team with surgeon, Philip Ashmore, anaesthetist  William Dodds and paediatrician  Morris Young. On October 29, 1957, at the Vancouver General Hospital, the team successfully performed the first open heart operation in British Columbia.
He also performed the first Coronary Artery Bypass Graft surgery in Cardiff, Wales

In 1977 to 1980 Allen was invited to establish a cardiac surgical certre at the [[Kasturba General Hospital]], Bhopal, India. The Indian surgeon, Dr. Raj Bisaryia, who had trained in Vancouver continued the program between Dr. Allen’s visits.

In 1987 and 1989 Allen was the visiting cardiac surgeon at the Ibn-al;Bitar Hospital in Baghdad, Iraq. The Hospital was built and staffed by the Irish construction company, PARC. The doctors, nurses and technicians were Irish, the only Iraqis were the patients and interpreters. Many patients lived in remote areas  with minimal access to medical care. Consequently, most of the cardiac valve replacements used bioprosthetic {porcine tissue ]that required no anticoagulant therapy post operative, compared to the mechanical artificial valve that did require anticoagulents.

1990 to 1999 Allen was a chairman of medical review panels of the British Columbia Worker’s Compensation Board; he   chaired 275 Panels.  From 1972 to 1988 he  established the annual Peter Allen Surgical Essay Award, open to senior cardio-thoracic trainees in the United Kingdom.

In 1993, after 36 years of surgical practice, he retired as an Emeritus Clinical Professor of Surgery at  the University of British Columbia Faculty of Medicine. In 1995 he  moved to [[Oakville, Ontario]] and joined the Medical Clinic of Dr. Sapra and Dr. Khanna as a consultant in peripheral vascular disease, until retirement in 2006. He died on November 17, 2014.<ref>{{cite news|title=Dr. Peter ALLEN obituary|url=http://www.legacy.com/obituaries/urbanacitizen/obituary.aspx?n=peter-allen&pid=173315075|accessdate=4 December 2014|publisher=Vancouver Sun|date=29 November 2014}}</ref>

==  Honours and Awards  ==

1956 - Annual research prize of the British Columbia Surgical Society for work on the diseased abdominal aorta.

1957 - McLaughlin Fellowship  to study cardiac surgery under Dr. Lillehei in Minneapolis.

1964 - Nuffield Travelling Fellowship to further study cardiac surgery at the Brompton Hospital in London and the Sulley Chest Hospital in Cardiff, Wales.

1972 - Invitation to address the Society of Thoracic and Cardiac Surgeons of Great Britain and Ireland.

1978 - Vancouver  Medical Association annual Osler Address.

1980 - Invitation to join the Pete’s Club composed of United Kingdom and Western European cardiac surgrons. The meetings were unique in that only surgical failures and problem cases were discussed. He held the final meeting of the Pete’s Club at the Vancouver General Hospital in 1989.

1998–present  Member, Board of Directors, Ian Anderson House, Cancer Hospice, Oakville,ON

2011–present  Joined the Grants Committee of the Oakville Community Foundation, a philanthropic  group, Oakville,ON.

==References==
{{reflist}}

*The Scarlet Pimpernel in Artificial Heart Valves - Peter Allen, M.D., British Columbia Medical Journal 31;7, Page 395 (1989)
*Open Cardiac Surgery in British Columbia - Peter Allen M.D., British Columbia Medical Journal 25:9, Page 444 (1983)
*Universal Cardiac Valve Holder - J.S. Masterson M.D.; Robert MiyigashimaM.D.; Peter Allen M.D., Annals of Thoracic Surgery 23, Page 376 (1977) [http://www.sciencedirect.com/science/article/pii/S0003497510641471 Click to view link]
*Strut Fracture and Disc Embolization of a Björk-Shiley Mitral Valve Prosthesis: Localization of Embolized Disc by Computerized Axial Tomography - Alberto J. Larrieu, M.D., Eduardo Puglia, M.D., Peter Allen, M.D. - Annals of Thoriacic Surgery 34:2, Page 192 (1982) [http://www.sciencedirect.com/science/article/pii/S0003497510608831 Click to view link]
*Spontaneous Massive Haemothorax in von Recklinghausen's Disease - A.J. Larrieu M.D., S.A, Hashimoto M.D., Peter Allen, M.D. - Thorax 37, Page 151 (1982)
*Results of Demand Pacing in Cardiac Arrhythmias - Peter Allen M.D., Eve Rotem M.D. - Annals of Thoracic Surgery, 8:2, Page 146 (1969)
*Use of Induced Cardiac Arrest in Open Heart Surgery - Peter Allen M.D., C Walton Lillehei M.D. - Minnesota Medicine 40,  Page 672 (1957)
*The Possibility of Myocardial Revascularization by Creation of a Left Ventriculocoronary Artery Fistula - Ian Munro M.D. Peter Allen M.D. - Journal of Thoracic and Cardiovascular Surgery 5 8:1, Page 25 (1969)
*The Cutting Edge by Frank Porter Patterson MD, Chapter 5 page 128
*J.C. Boileau Grant by Clayton L.N. Robinson MD, Page 11
*The Key To The Door - A Cardiac Surgery Anthology by C. Walton Lillehei, Page 187
*1957 to 1982 Open Cardiac Surgery in British Columbia" - In the British Columbia Medical Journal, Vol 25 #9, page 444 - September 1983.
*"Surgical Management of Post Infarction Ventricular Septal Defects". In the Journal of Thoracic and Cardio Vascular Surgery #51 page 346 (1966).
*"How a Lack of so Little Killed so Many" - In the News of the British Columbia Medical Association page 14, October to November 1991
*VGN Nursing School Alumnae - [http://vghnursingschoolalumnae.com/blog/2012/3/19/first-open-heart-surgery-in-bc-at-vgh-october-29-1957.html Read article about First Open Heart Surgery in British Columbia at VGH on October 29, 1957]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Allen, Peter}}
[[Category:1921 births]]
[[Category:2014 deaths]]
[[Category:Physicians from Philadelphia]]
[[Category:University of Amsterdam alumni]]
[[Category:University of Toronto alumni]]
[[Category:Canadian surgeons]]
[[Category:People from Vancouver]]