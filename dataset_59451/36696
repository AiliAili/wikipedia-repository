{{good article}}
{{Infobox NRHP
| name = Art's Auto
| nrhp_type = 
| image = One Lonsdale Avenue, Pawtucket RI.jpg
| caption = Art's Auto
| location = [[Pawtucket, Rhode Island]]
| coordinates = {{coord|41|52|7|N|71|23|56|W|display=inline,title}}
| locmapin = Rhode Island#USA
| area =
| built = 1927
| architect =
| architecture =
| added = December 15, 1978
| refnum = 78000071 <ref name="nris">{{NRISref|2007a}}</ref>
}}

'''Art's Auto''' is a historic former service station at 5–7 Lonsdale Avenue in [[Pawtucket, Rhode Island]].  It is a single-story brick structure with a flat roof and a series of towers capped by pointed roofs. It was built as an automotive service station in 1927–28 for Arthur Normand at a time when gasoline producers competed, in part, by the shape and style of their service stations. This station is one of two stations known to survive from this period in the state. Its front facade has a dramatic presentation, with square towers topped by pyramidal roofs at the corners, and a projecting round bay in the center topped by a conical roof, with windows arrayed around the bay and on its flanks. The building is currently used as an office for Anchor Financial. Art's Auto was listed on the [[National Register of Historic Places]] in 1978.

== Design ==
Art's Auto was constructed for Arthur Normand in 1927–28 on a triangular lot intersected by Main Street and Lonsdale Avenue ([[Rhode Island Route 122]]). Also intersecting are Thurston Street and Randall Street, forming a busy intersection in [[Pawtucket, Rhode Island]]. 

The service station is a single-story brick structure measuring {{convert|32|ft|m}} by {{convert|40|ft|m}}. The interior and exterior of the building are divided into two distinct sections, with the original office and sales display area measuring {{convert|14|ft|m}} deep and the rear area being {{convert|26|ft|m}} deep.<ref name=NRHP /> The front division's corners are marked by four square turret-like piers with pyramidal roofs, each capped with a single over-scaled ball finial. Projecting from the center of the southwest facade is a circular tower with a ten-sided conical roof and a large ball finial. The front of the building has large display windows and a doorway on each side, comprising the majority of the facade which is topped by a false [[mansard roof]]. The rear division is devoid of architectural ornamentation, consisting of a flat metal roof to house the garage of the service station. An overhead garage door allows access for vehicles into the concrete floor service area.<ref name=NRHP /> 

In 1978, the building likely used [[asbestos shingle]]s for the front roofing, though they may have been removed by later renovations.<ref name=NRHP /><ref name=gas />

== Use ==
Art's Auto was originally used as a gas station, but it had been abandoned and was slated for demolition after it was acquired by the Pawtucket Redevelopment Agency. In 1978, the agency recognized the building's historical nature and decided to preserve the property.<ref name=NRHP /> At the time of its listing on the historic register, the property was in a run-down state, while the Pawtucket Redevelopment Agency was advertising it as "the most interesting and best preserved early 20th century service station in Rhode Island".<ref name=NRHP>{{cite web|url=http://www.preservation.ri.gov/pdfs_zips_downloads/national_pdfs/pawtucket/pawt_lonsdale-avenue-5_arts-auto-supply.pdf|title=NRHP nomination for Art's Auto|publisher=Rhode Island Preservation|accessdate=2014-11-13}}</ref><ref>{{cite book | url=https://books.google.com/books?id=xEpWAAAAMAAJ&q=%22Art%27s+Auto%22+Pawtucket&dq=%22Art%27s+Auto%22+Pawtucket&hl=en&sa=X&ei=rSCCVMn8LM2NyAS-44HQBw&ved=0CD4Q6AEwAw | title=Cars and Parts Volume 22 | publisher=Amos Press | year=1979 | pages=59}}</ref> <!---hiding the following sentence because it is a non-sequitur: This local attempt to revitalize and preserve historic sites was not unique because more than 4,000 such organizations existed nationally by 1977 with the goal to re-use or save such sites from destruction.<ref>{{cite book | url=https://books.google.com/books?id=CyQF6pKZ61YC&pg=PA562&dq=Art%27s+Auto+Pawtucket&hl=en&sa=X&ei=eCCCVKTEO4WkyQSWxoGwBg&ved=0CDAQ6AEwAA#v=onepage&q=Art%27s%20Auto%20Pawtucket&f=false | title=Mystic Chords of Memory: The Transformation of Tradition in American Culture | publisher=Knopf Doubleday Publishing Group | author=Kammen, Michael | year=2011 | pages=562}}</ref> ---> 

The vacant property was later purchased for use by a lawyer and then by an auto dealer before being purchased by Joseph Baptista in 2000. Baptista had to survey the ground to ensure that the gas tanks had been removed. Since 2000, Baptista's mortgage finance company Anchor Financial has operated out of the location and the building has continued to retain its historic features.<ref name=gas>{{cite web | url=http://www.providencejournal.com/cars/content/20140201-two-former-gas-stations-in-pawtucket-stand-out-for-their-architectural-details.ece | title=Two former gas stations in Pawtucket stand out for their architectural details | publisher=Providence Journal | date=February 1, 2014 | accessdate=5 December 2014 | author=Elsworth, Peter}}</ref>

== Significance ==
Art's Auto is historically and architecturally significant as a type of novelty gas station that was constructed during the phase in which America became highly mobile and yet had not designed specific forms and marketing for automobile service stations. The building's design was intended to capture the attention of motorists and capitalize on the positive imagery of the design.<ref name=NRHP /> Pawtucket's Art's Auto and [[Gilbane's Service Center building]]s are both rare surviving examples of a dedicated service station displaying whimsical designs in what has been described as the second generation service stations.<ref name=gas /> The design of both buildings represents the antithesis of standardization, opposed to the modern concept of promoting an "image of familiarity and reliability through uniformity".<ref name=NRHP /> The building was listed on the [[National Register of Historic Places]] in 1978.<ref name="nris"/>

==See also==
*[[National Register of Historic Places listings in Pawtucket, Rhode Island]]

==References==
{{Reflist}}

{{National Register of Historic Places}}

[[Category:Transportation buildings and structures on the National Register of Historic Places in Rhode Island]]
[[Category:Buildings and structures in Pawtucket, Rhode Island]]
[[Category:Gas stations on the National Register of Historic Places]]
[[Category:National Register of Historic Places in Pawtucket, Rhode Island]]