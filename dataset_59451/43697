<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R.V
 | image=Stahlwerk-Mark R.V.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Small airliner
 | national origin=[[Germany]]
 | manufacturer=[[Stahlwerk-Mark]]
 | designer=Rieseler brothers
 | first flight=1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Stahlwerk-Mark R.V''', sometimes known as the '''Stahlwerk-Mark R.V/23''' to indicate its year of production, was a small [[Germany|German]] airliner able to carry three passengers.  Two were built.

==Design and development==
[[File:Stahlwerk-Mark R.V side.png|thumb|right]]

The R.V was the Rieseler brother's first multiple passenger aircraft, a single engine [[parasol wing]] cabin design.  The wing was in two parts, each with two wooden [[spar (aviation)|spar]]s and [[rib (aircraft)|rib]]s.  Though the wings had quite thick, high lift  [[airfoil]] sections, they were not [[cantilever]] structures and wing loads were transmitted to the fuselage via N form struts from the lower [[fuselage]] [[longeron]]s to the wing spars, their outer, parallel members enclosed in [[aircraft fairing|fairings]].  A shallow steel tube [[cabane strut|cabane]] connected and supported the wings over the fuselage.  They were fitted with high [[aspect ratio (wing)|aspect ratio]] [[ailerons|overhung balanced ailerons]] and had a deep cut-out in the [[trailing edge]] over the [[cockpit]] to enhance the pilot's forward and upward vision.<ref name=Flight/>

The clean [[fuselage]] of the R.V was an internally wire braced, welded steel tube structure of rectangular cross section. The {{convert|100|hp|kW|abbr=on|0}} [[Mercedes D.I]] water-cooled six cylinder inline engine was mounted behind a rectangular nose [[radiator (engine cooling)|radiator]] in an aluminium covered forward fuselage region; fuel was gravity fed from a wing tank. Aft, the fuselage was [[aircraft fabric covering|fabric covered]]. The pilot had an open cockpit under the wing trailing edge, with one passenger next to him. Two other passengers could sit, though not stand, side by side within the fuselage forward of the pilot in a cabin lit by windows, one in the small, starboard side door, and heated by hot air from the engine. Their luggage was placed in a small compartment behind the pilot. The [[empennage]] was a steel tube structure, fabric covered.  The braced tailplane carried separate [[elevator (aircraft)|elevator]]s which, like the [[rudder]], were [[balanced rudder|balanced]].  The R.V had a simple, fixed [[conventional undercarriage]] with steel tube V struts and mainwheels on a rubber sprung single axle, together with a tailskid.<ref name=Flight/>

The R.V was flying before mid-January 1924. There were plans to accommodate all three passengers in the cabin.<ref name=Flight/> By February, their test pilot Raab had expressed strong concerns about the pilot's position, particularly the lack of visibility and his exposure to wind and pitching motions, suggesting that the relative positions of cockpit and cabin needed altering. This advice was heeded and in the second aircraft the cockpit had been moved forward, the cut-out in the trailing edge removed and the cabin placed under the trailing edge. In addition, the ailerons were no longer overhung and the vertical tail was broader and less high.  This machine had a Daimler engine.<ref name=HAv/>
<!-- ==Operational history== -->
<!-- ==Variants== -->

==Specifications==
[[File:Stahlwerk-Merk R.V 3view.png|thumb|right]]
{{Aircraft specs
|ref=The Stahlwerk-Mark R.V/23 Commercial Monoplane<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=Three passengers
|length m=7.9
|length note=
|span m=14.25
|span note=
|height m=3.35
|height ft=
|height in=
|height note=<ref name=HAv/>
|wing area sqm=27
|wing area note=
|aspect ratio=7.52
|airfoil=
|empty weight kg=800
|empty weight note=
|gross weight kg=1280
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Mercedes D.I]]
|eng1 type=6-cylinder water-cooled inline
|eng1 hp=100
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=150
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=370
|range note=
|endurance=4 hr at full speed
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=8 min to 1,000 m (3,280 ft)
|lift to drag=
|wing loading lb/sqft=9.7
|wing loading note=

|power/mass= 58 W/kg (0.035 hp/lb) 
|thrust/weight=

|more performance=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |coauthors= |date=17 January 1924  |title= The Stahlwerk-Mark R.V/23 Commercial Monoplane|magazine= [[Flight International|Flight]]|volume=XVI |issue=3 |pages=31–2 |url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200031.html }}</ref>

<ref name=HAv>{{cite web |url=http://www.histaviation.com/Stahlwerk_Mark_R_Va.html|title=Stahlwerk-Mark R.V|accessdate=9 August 2013}}</ref>

}}

[[Category:Parasol-wing aircraft]]
[[Category:German civil aircraft 1920–1929]]