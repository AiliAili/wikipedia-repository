<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=Pixie
 | image=PixieB.jpg
 | caption=Pixie III ''G-EBJG'' in monoplane and biplane configurations
}}{{Infobox Aircraft Type
 | type=Light aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=[[Parnall Aircraft|George Parnall & Co.]]
 | designer=Harold Bolas
 | first flight=13 September 1923
 | introduced=
 | retired=1939
 | status=
 | primary user=
 | number built=3
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Parnall Pixie''' was a low powered [[United Kingdom|British]] single-seat [[monoplane]] light aircraft originally designed to compete in the [[Lympne light aircraft trials|Lympne, UK]] trials for [[motor-glider]]s in 1923, where it was flown successfully by [[Norman Macmillan (RAF officer)|Norman Macmillan]]. It had two sets of wings, one for cross-country flights and the other for speed; it later appeared as a [[biplane]] which could be converted into a monoplane.

==Design and development==
Though only three Parnall Pixies were built, they appeared with a remarkable variety of wings; a normal span monoplane (Pixie I); a short span monoplane (Pixie II); a biplane readily convertible to a monoplane (Pixie IIIA); and a non-convertible monoplane version of the latter, with a greater span than the Pixie I.<ref name="AJJ">{{Harvnb|Jackson|1960|pages=399–400.}}</ref>  The first Pixie was designed to compete in the [[Lympne light aircraft trials|Lympne Light Aeroplane Trials]] of 1923, organised by the Royal Aero Club for what they described as single-seat motor-gliders. The intention was to develop economical private aviation, so the engine size was limited to 750 cc with immediate consequences for aircraft size and weight. Various sponsors provided attractive prizes, particularly the total of £1500 jointly from the Duke of Sutherland and the Daily Mail. The event took place from 8–13 October 1923. There were many entrants from the British aviation industry, including the [[de Havilland Humming Bird]], [[Gloster Gannet]] and [[Vickers Viget]].  The Pixie I first flew, at [[Filton]] on 13 September 1923, in good time for the competition.<ref name="AJJ"/>

The sole Pixie I & II, registered ''G-EBKM'', was the same aircraft apart from the wings and engine.<ref name="AJJ"/><ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200653.html "Pixie."] ''Flight'', 25 October 1923, pp. 653-654.</ref>  The common fuselage was built around four spruce longerons, stiffened by diagonals or plywood sheet, though most of the fuselage was fabric covered; it had with a rounded decking.<ref name="Flight"/>  The single cockpit was at mid wing chord.  The engine was mounted on steel tubes fixed to the ends of the longerons, with a firewall between engine and pilot.<ref name="Flight"/>  There was a triangular tailplane bearing a single piece elevator, with hinge just at the end of the fuselage where there was also a small tail skid.  The fin was also triangular, though its trailing edge, carrying the rudder hinge, leant slightly forward, helping the large and almost semi-circular rudder to clear the elevator.  The main undercarriage was unusual and looked rather vulnerable.  Two steel tubes formed an inverted V, joined at the top to the upper fuselage internally and emerging below to meet a cross-axle at points not much further apart than the width of the fuselage.  The axle was more than twice this width, giving a wide track arrangement which was sprung only by deflection of the V struts.<ref name="Flight"/>

Both pairs of wings had similar planforms and the same centre section chord.  The leading edges were straight apart from at the tips, as were the centre section trailing edges.  Outboard the trailing edges, fully occupied with ailerons swept forward, more sharply on the short span wing.<ref name="Flight"/>  The ailerons were of the differential kind, a recent invention, with less downward movement than upward. Both wing sets were built up around two spruce spars; the only novelty was that the rear spar was not straight but came in two joined sections, the outer part swept forward to meet the forward spar at the wingtip.  The wings were hinged to the lower longerons and braced by a pair of streamlined steel tubes from spars to upper longerons.<ref name="Flight"/>

The wings of the Pixie I, long span and designed for fuel economy had a span of 28&nbsp;ft 6 in and an area of {{convert|100|sqft|m2|abbr=on}}, whereas those of the Pixie II, designed for speed had a span of 17&nbsp;ft 10 in (5.44 m) and an area of 60 sq ft (5.57 m<sup>2</sup>).<ref name="Flight"/>  The engines were chosen to match the same purposes.  For economy the Pixie I had a 500 cc Douglas horizontal twin and the Pixie II a more powerful 750 cc (the competition limit) engine of the same make and configuration.<ref name="Flight"/> In 1924 it was flying with a 696 cc [[Blackburne Tomtit]] engine.<ref name="AJJ"/>   These engines were mounted low on the nose, driving a two-bladed propeller on a shaft above it via a chain reduction gear of ratio 2.5:1.<ref name="Flight"/>

The rules of the 1924 Lympne trials were revised to allow more practical aircraft, rather than motor-gliders.  Parnall therefore built a two-seat version, the Pixie IIIA with its fuselage extended by 3&nbsp;ft 2 in (965&nbsp;mm) to allow a second cockpit, one now above each wing spar and the wing span extended to 32&nbsp;ft 5 in.  A demountable upper wing, attached by N form interplane struts and several cabane struts and of markedly smaller span and deep central chord<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200652.html "Pixie."] ''Flight'', 9 October 1924, pp.652–653.</ref> allowed the IIIA to be flown either as monoplane or biplane. Its undercarriage legs now had rubber compression shock absorbers.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200655.html "Pixie."] ''Flight'', 9 October 1924, p. 655.</ref>    Two were built, one with a 32&nbsp;hp [[Bristol Cherub|Bristol Cherub III]] engine and the other with a 35&nbsp;hp [[Blackburne Thrush]].  After the trials and in the following year, the Thrush was replaced with a 1,100 cc [[Anzani]].  In 1926, both IIIAs were converted to permanent monoplane configuration as Pixie IIIs, both now with the Bristol engine.

==Operational history==
The Pixie did well at Lympne, winning the £500 speed prize at 76.1&nbsp;mph (122 k/h) as the Pixie II.<ref name="AJJ"/>   The Pixie II also won the Wakefield prize at 81&nbsp;mph (130&nbsp;km/h) at Hendon, later that October.<ref name="AJJ"/>  It flew mostly as the Pixie II thereafter, appearing at Lympne races in 1924 and, with an enlarged rudder, in 1925.  It also flew in the RAF display of 1924.  After a period of inactivity from about 1925 to 1937, it was in the air again until it crashed in April 1939.<ref name="AJJ"/>

Neither Pixie IIIA, competing as biplanes, made as much impact at Lympne 1924, with the Cherub engined machine retiring early,<ref name="AJJ"/> though it flew later in the week as a monoplane.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200650.html "Pixie".] ''Flight'', 9 October 1924, pp. 650–659.</ref>   The other, ''G-EBKK'' flying as no.19 and flown by W. Douglas completed all tasks but the two sets of five laps required by the high speed tests.  Only two aircraft, the [[Bristol Brownie]] and the [[Beardmore Wee Bee]], the overall winner, did complete these high speed tests.  It was the top-scorer in the low speed test.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200659.html "Pixie."] ''Flight'', 9 October 1924,  p.659.</ref>   At the end of the Lympne trials, no.19 went on to compete in the [[Grosvenor Cup]] handicap, where it finished fifth. Converted to Pixie III standard they both flew on into the 1930s, one (''G-EBJG'') surviving [[World War II]] in store but not flying afterwards.<ref name="AJJ"/>   Its remains are in deep store in the [[Midland Air Museum]], [[Coventry]].

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Pixie II) ==
[[File:PixieA.jpg|thumb|right|Pixie II]]
{{aerospecs
|ref=''Flight'' 25 October 1923, pp.653-4<!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=1
|capacity=
|length m=5.47
|length ft=18
|length in=0
|span m=5.44
|span ft=17
|span in=10
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=5.57
|wing area sqft=60
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=127
|empty weight lb=279
|gross weight kg=209
|gross weight lb=460
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=750 cc [[Douglas (motorcycles)|Douglas]] flat twin, air cooled
|eng1 kw=?<!-- prop engines -->
|eng1 hp=?<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=130
|max speed mph=<ref name="AJJ"/> 81
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Parnall Pixie}}

===Notes===
{{reflist}}

===Bibliography===
{{Refbegin}}
*{{cite book |title= British Civil Aircraft 1919–1959|last=Jackson|first=A.J.| year=|volume= 2|publisher=Putnam Publishing, 1960 |location=London |isbn=|ref=harv}}
{{Refend}}

<!-- ==External links== -->
{{Parnall aircraft}}

[[Category:British sport aircraft 1920–1929]]
[[Category:Parnall aircraft|Pixie]]