{{Infobox military person
|name=Helmut Eberspächer
|birth_date=18 November 1915
|death_date={{death date and age|2011|6|19|1915|11|18|df=y}}
|image=Helmut Eberspächer.jpg
|caption=
|birth_place= [[Tübingen]]
|death_place= [[Esslingen am Neckar]]
|nickname=
|allegiance={{flag|Nazi Germany}}
|branch=[[German Army (Wehrmacht)|Army]]<br/>[[Luftwaffe]]
|serviceyears=1934–39<br/>1939–45
|rank=''[[Hauptmann]]'' (Captain)
|unit=[[Lehrgeschwader 2|LG 2]]<br/>[[SKG 10]]<br/>[[Kampfgeschwader 51|KG 51]]<br/>[[NSGr. 20]]
|commands=
|battles=[[World War II]]
*[[Defense of the Reich]]
|awards=[[Knight's Cross of the Iron Cross]]<br />[[Order of Merit of Baden-Württemberg]]
|laterwork=Businessperson and chairman of [[Eberspächer]] GmbH & Co. KG}}

'''Helmut Eberspächer''' (18 November 1915 – 19 June 2011) was a German [[fighter ace]] in the [[Luftwaffe]] and recipient of the [[Knight's Cross of the Iron Cross]] during [[World War II]]. The Knight's Cross was awarded to recognise extreme battlefield bravery or outstanding military leadership. Eberspächer was credited with 7 victories, each resulting in the destruction of an enemy aircraft.

In 1950, he and his cousin Walter took over the family business Eberspächer. He ran the business as chairman until 1988, and remained as an honorary chairman until 2005. He also served on the board of the [[Confederation of German Employers' Associations]] (BDA) and the [[Verband der Automobilindustrie|German Automotive Industry Association]]. Under his leadership, Eberspächer became one of the leading automobile industry suppliers in Germany, with approximately 5,600 employees worldwide and 1.9 billion [[Euro]]s in sales in 2010.<ref name="Eberspächer">{{cite web|work=Eberspächer |title=Helmut Eberspächer: a socio-politically committed entrepreneur |url=http://www.eberspaecher.com/nc/ca_fr/press/press-releases/individual-view/article/helmut-eberspaecher-ein-gesellschaftspolitisch-engagierter-unternehmer/8.html|accessdate=16 March 2013}}</ref> In 1989 he was fined for [[tax evasion]].

==Early life and military career==
Eberspächer was born on 18 November 1915 in [[Tübingen]], at that time part of the [[Kingdom of Württemberg]], a [[federated state]] of the [[German Empire]]. In 1934 he volunteered for military service in the [[Reichswehr]], and served in an armoured reconnaissance battalion. In parallel to his military service he studied [[mechanical engineering]] at the [[University of Stuttgart]]. Following his graduation in 1939, he transferred to the Luftwaffe. From July 1940 to January 1943 he served with the 7.(''Fernaufklärungs''-) ''[[Organization of the Luftwaffe (1933–1945)#Staffel|Staffel]]'' of [[Lehrgeschwader 2]] (7th Long Range Reconnaissance Squadron of the 2nd Demonstration Wing) on both [[Western Front (World War II)|Western]] and [[Eastern Front (World War II)|Eastern Fronts]].<ref group="Note">For an explanation of ''Luftwaffe'' unit designations see [[Organization of the Luftwaffe (1933–1945)|Organisation of the Luftwaffe during World War II]].</ref> He then converted to a [[ground attack]] role, and from March 1943 served with [[Schnellkampfgeschwader 10]] (SKG 10—10th Fast Bomber Wing), flying fighter-bomber missions over southern England and anti-shipping operations in the English Channel. He was promoted to the rank of ''[[Hauptmann]]'' (captain) on 1 May 1944 and appointed as ''[[Staffelkapitän]]'' (leader of the squadron) in [[Kurt Dahlmann]]'s I./SKG 10 (1st Group).<ref name="Obermaier p105">Obermaier 1989, p. 105.</ref>

Early on [[D-Day]] 6 June 1944, Hauptmann Eberspächer was ordered to lead four [[Focke-Wulf Fw 190]]s of 3/SKG 10 (3rd Squadron of SKG 10) over [[Normandy]]. Two hours later, the flight landed at [[Evreux]] having achieved the first Luftwaffe aerial victories of D-Day. At 05:01 they had intercepted a formation of [[Royal Air Force]] [[Avro Lancaster]]s, and in the next three minutes, four were shot down. The first fell over [[Isigny-sur-Mer]] and the others near [[Carentan]]. Three were claimed as downed by Eberspächer, one of them being Lancaster ND739 of [[No. 97 Squadron RAF]], flown by the squadron's commanding officer, W/C Jimmie Carter. His seven-man crew had earned four [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]es and three [[Distinguished Flying Medal]]s. The wreckage of ND739 was located and excavated in 2012. An assortment of items were recovered including the wedding ring belonging to crew member Albert Chambers.<ref>{{cite news|work=DailyMail |title=For the love of Vera: D-Day Lancaster bomber crew identified 68 years on by poignant inscription on dead airman's ring |url=http://www.dailymail.co.uk/news/article-2211250/Wreckage-Lancaster-bomber-crashed-D-Day-killing-crew-identified.html#ixzz2NeLRUuC6|accessdate=16 March 2013|location=London|date=1 October 2012}}</ref><ref>Zaloga 2011, pp. 28–29.</ref>

Eberspächer was awarded the [[Knight's Cross of the Iron Cross|Knight's Cross]] on 24 January 1945, after 170 ''Jabo'' (German abbreviation for ''Jagdbomber''—[[fighter-bomber]]) missions over the [[Western Front (World War II)|Western Front]]. He flew sorties during the [[Battle of the Bulge]] and against the [[Ludendorff Bridge|Remagen bridge]] and the established US Army bridgehead. His unit, I/SKG 10 (1st Group of SKG 10), was assigned to [[Kampfgeschwader 51]] as III. ''[[Organization of the Luftwaffe (1933–1945)#Gruppe|Gruppe]]'' (3rd Group) on 30 June 1944. His squadron was then redesignated as the 3. Staffel/''Nachtschlachtgruppe'' 20 [[Harassing fire#World War II|night harassment]] attack squadron in October 1944. He survived the war and was credited with a total of seven aerial victories, including three at night.{{sfn|Brütting|1992|p=190}}

==Later life and business career==
[[File:Jakob Eberspächer.jpg|thumb|right|Jakob Eberspächer, grandfather of Helmut and founder of the "Glasdachfabrik-Eberspächer".]]
After the war, Eberspächer joined the family business, which had been founded by his grandfather, Jakob Eberspächer, in 1865 as a plumber's workshop which was known as "Glasdachfabrik-Eberspächer" (Eberspächer Roof-Glazing). He succeeded [[Hanns-Martin Schleyer]] in 1978 as president of the ''Landesvereinigung Baden-Württembergischer Arbeitgeberverbände'' (State Federation of Baden-Württemberg Employers' Associations) after Schleyer was kidnapped and murdered by the [[Red Army Faction]] on 18 October 1977. Eberspächer also supported the [[Internationale Bachakademie Stuttgart]].<ref name="Eberspächer"/><ref>{{cite web|work=Internationale Bachakademie Stuttgart|title=Patrons|url=http://www.bachakademie.de/eng/836.html|accessdate=24 March 2013}}</ref>

In July 1989, in the aftermath of the [[Flick affair]], Eberspächer was fined 140,000 [[Deutsche Mark|DM]] for [[tax evasion]]. He had transferred and donated more than 300,000 DM into the accounts of the political parties [[Christian Democratic Union (Germany)|Christian Democratic Union]] and [[Free Democratic Party (Germany)|Free Democratic Party]] via the "''Gesellschaft zur Förderung der Wirtschaft Baden-Württemberg''" (the Society for the Advancement of Economics in Baden-Württemberg) between 1972 and 1981.<ref>{{Cite news|journal=Der Spiegel |volume=29|year=1989 |title=Kleines Licht|url=http://www.spiegel.de/spiegel/print/d-13494402.html| language=German|accessdate=16 March 2013}}</ref><ref>{{Cite news|journal=Der Spiegel |volume= 17|year=1989 |title=Mehr Steine als Brot|url=http://www.spiegel.de/spiegel/print/d-13495402.html| language=German|accessdate=16 March 2013}}</ref>

He died on 19 June 2011 in [[Esslingen am Neckar]].<ref name="Eberspächer"/> The Eberspächer family are heirs of his business and are ranked 264th of the 500 richest Germans in 2013, with net assets of 450 Million Euros, an increase of 100 Million Euros over the year 2012.<ref>{{cite web|last=Güneri |first=Emel |date=1 February 2013 |work=
der WISSENSCHAFTLICHE KARRIERE-BLOG! |title=die REICHSTEN DEUTSCHEN 2013 – Teil 3|url=http://karriere-mit-stil.trust-wi.de/2013/02/die-reichsten-deutschen-2013-teil-3/| language=German|accessdate=20 March 2013}}</ref>

==Awards==
* [[Aviator badge|Flugzeugführerabzeichen]]
* [[Iron Cross]] (1939)
** 2nd Class (5 May 1941)<ref name="World War 2 Awards">{{cite web|work=World War 2 Awards.com |title=Helmut Eberspächer |url=http://www.ww2awards.com/person/22676 | accessdate = 16 March 2013}}</ref>
** 1st Class (5 August 1941)<ref name="World War 2 Awards"/>
* [[Ehrenpokal der Luftwaffe]] on 17 April 1944 as ''[[Oberleutnant]]'' and pilot{{sfn|Patzwall|2008|p=70}}{{refn|According to Obermaier on 20 March 1944.<ref name="Obermaier p105"/>|group="Note"}}
* [[Front Flying Clasp of the Luftwaffe]] for Combat Pilots (7 October 1944)
* [[German Cross]] in Gold on 1 January 1945 as ''[[Hauptmann]]'' in the I./Schnellkampfgeschwader 10{{sfn|Patzwall|Scherzer|2001|p=95}}
* [[Knight's Cross of the Iron Cross]] on 26 January 1945 as ''Hauptmann'' and ''Staffelkapitän'' of the 3./Nachtschlacht-Gruppe 20{{sfn|Fellgiebel|2000|p=168}}{{sfn|Scherzer|2007|p=285}}
* [[Order of Merit of Baden-Württemberg]] (1979) presented on 21 April 1979 at [[Ludwigsburg Palace]]<ref>{{cite web|work=Staatsministerium Baden-Württemberg |title=Verdienstorden des Landes Baden-Württemberg—Liste der Ordensträger 1975 – 2012 |url=http://www.stm.baden-wuerttemberg.de/fm7/2126/120428_Verdienstorden_B-W_Liste_Ordenstr%E4ger_1975-2012.pdf#page=12&zoom=auto,0,842 |format=PDF|language=German |page=12 |accessdate=19 March 2013}}</ref>

==Notes==
{{reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Brütting
  |first=Georg
  |year=1992
  |origyear=1976
  |edition=7th
  |title=Das waren die deutschen Stuka-Asse 1939 – 1945
  |trans_title=These were the German Stuka Aces 1939 – 1945
  |language=German
  |location=Stuttgart, Germany
  |publisher=Motorbuch
  |isbn=978-3-87943-433-6
  |ref=harv
}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
  |ref=harv
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Zaloga
  |first=Steven J.
  |authorlink=Steven Zaloga
  |year=2011
  |title=The Most Daring Raid of World War II: D-Day—Pointe-Du-Hoc
  |location= New York
  |publisher=The Rosen Publishing Group
  |isbn=978-1-4488-1867-9
}}
{{Refend}}

==External links==
*{{Cite news|journal=Der Spiegel |volume=38|year=1985 |title=Parteispenden: Mal die Matratze hochheben|url=http://www.spiegel.de/spiegel/print/d-13515387.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume= 48|year=1987 |title=Weit über das Maß|url=http://www.spiegel.de/spiegel/print/d-13526772.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume=17|year=1989 |title=Mehr Steine als Brot|url=http://www.spiegel.de/spiegel/print/d-13495402.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume=23|year=1989 |title=Schelle um den Hals|url=http://www.spiegel.de/spiegel/print/d-13492915.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume=29|year=1989 |title=Kleines Licht|url=http://www.spiegel.de/spiegel/print/d-13494402.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume=30|year=1989 |title=Tätige Spender-Reue|url=http://www.spiegel.de/spiegel/print/d-13495055.html| language=German|accessdate=16 March 2013}}
*{{Cite news|journal=Der Spiegel |volume=14|year=1990 |title=So dappig|url=http://www.spiegel.de/spiegel/print/d-13497366.html| language=German|accessdate=16 March 2013}}

{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Cars
| portal4=Military of Germany
| portal5=World War II
}}

{{DEFAULTSORT:Eberspacher, Helmut}}
[[Category:1915 births]]
[[Category:2011 deaths]]
[[Category:People from Tübingen]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:University of Stuttgart alumni]]
[[Category:German businesspeople]]
[[Category:German people convicted of tax crimes]]
[[Category:Recipients of the Order of Merit of Baden-Württemberg]]
[[Category:People from the Kingdom of Württemberg]]