{{Infobox coin
| Denomination         = Jefferson nickel
| Country              = United States
| Value                = 5
| Unit                 = cents (0.05  [[United States dollar|US dollars]])
| Mass                 = [[Shield nickel#use of Ni|5.000]]
| Diameter             = 21.21
| Edge                 = Plain
| Composition =
  {{plainlist |
* 75% [[copper]]
* 25% [[nickel]]
  }}
"Wartime Nickels" <br /> (mid-1942 to 1945)<!--
-->{{plainlist |
* 56% [[copper]]
* 35% [[silver]]
* 9% [[manganese]]
   }}
| Years of Minting     = 1938 – present
| Obverse              = US Nickel 2013 Obv.png
| Obverse Design       = [[Thomas Jefferson]]
| Obverse Designer     = [[Jamie Franki]]
| Obverse Design Date  = 2006–present
| Obverse2             = NickelObverses.jpg
| Obverse2 Design Date = 1938–2004 (left) and 2005 (right)
| Reverse              = US Nickel 2013 Rev.png
| Reverse Design       = [[Monticello]]
| Reverse Designer     = [[Felix Schlag]]
| Reverse Design Date  = 1938–2003 and 2006–present.  Struck without initials "FS" prior to 2006.
| Reverse2             = NickelReverses.jpg
| Reverse2 Design Date = upper two designs struck in 2004; lower two in 2005
| Mint marks          = [[Denver Mint|D]], [[San Francisco Mint|S]], [[Philadelphia Mint|P]].  Located from 1938 to 1964 to the right of Monticello, except for "wartime nickels" which have a large mint mark above Monticello.  No mint marks used from 1965 to 1967.  From 1968 to 2004, slightly clockwise from the last digit of the date.  In 2005, under "Liberty".  Since 2006, under the date. Philadelphia Mint specimens before 1980 lack mint mark, except for wartime nickels, which have a P for Philadelphia if struck there.
}}

The '''Jefferson nickel''' has been the [[nickel (United States coin)|five-cent coin]] struck by the [[United States Mint]] since 1938, when it replaced the [[Buffalo nickel]].  From 1938 until 2004, the copper-nickel coin's [[Obverse and reverse|obverse]] featured a profile depiction of [[Founding Fathers of the United States|founding father]] and third U.S. President [[Thomas Jefferson]] by artist [[Felix Schlag]]; the obverse design used in 2005 was also in profile, though by Joe Fitzgerald. Since 2006 Jefferson's portrayal, newly designed by Jamie Franki, faces forward. The coin's reverse is still the Schlag original, although in 2004 and 2005 the piece bore commemorative designs.

First struck in 1913, the [[Buffalo nickel]] had long been difficult to coin, and after it completed the 25-year term during which it could only be replaced by Congress, the Mint moved quickly to replace it with a new design.  The Mint conducted a design competition in early 1938, requiring that Jefferson be depicted on the obverse, and Jefferson's house [[Monticello]] on the reverse.  Schlag won the competition, but was required to submit an entirely new reverse and make other changes before the new piece went into production in October 1938.

As nickel was a strategic war material during World War II, nickels coined from 1942 to 1945 were struck in a copper-silver-manganese alloy which would not require adjustment to vending machines. They bear a large [[mint mark]] above the depiction of Monticello on the reverse.  In 2004 and 2005, the nickel saw new designs as part of the [[Westward Journey nickel series]], and since 2006 has borne Schlag's reverse and Franki's obverse.

== Inception ==

[[File:Inspecting nickel designs crop.jpg|thumb|left|[[Nellie Tayloe Ross]] (right), Director of the Mint, and [[Edward Bruce (New Deal)|Edward Bruce]], Director of the [[Section of Painting and Sculpture]], inspect candidates for the design of the new Jefferson nickel, April 1938]]

The design for the Buffalo nickel is well regarded today, and has appeared both on a commemorative silver dollar and [[Gold Buffalo|a bullion coin]].  However, during the time it was struck (1913–1938), it was less well liked, especially by Mint authorities, whose attempts to bring out the full design increased an already high rate of die breakage.  By 1938, it had been struck for 25&nbsp;years, thus becoming eligible to be replaced by action of the [[United States Secretary of the Treasury|Secretary of the Treasury]]  rather than by Congress.  The Mint, which is part of the [[United States Department of the Treasury|Department of the Treasury]],  moved quickly and without public protest to replace the coin.{{sfn|Bowers|2007|p=127}}

In late January 1938, the Mint announced an open competition for the new nickel design, with the winner to receive a prize of $1,000.  The deadline for submissions was April 15; Mint Director [[Nellie Tayloe Ross]] and three sculptors were to be the judges.  That year saw the bicentennial of the birth of the third U.S. President, [[Thomas Jefferson]]; competitors were to place a portrait of Jefferson on the obverse, and a depiction of his house, [[Monticello]], on the reverse.{{sfn|Bowers|2007|pp=127–128}}

By mid-March, few entries had been received.  This seeming lack of response proved to be misleading, as many artists planned on entering the contest and would submit designs near the deadline.  On April 20, the judges viewed 390 entries; four days later, [[Felix Schlag]] was announced as the winner.{{sfn|Bowers|2007|p=129}}  Schlag had been born in [[German Empire|Germany]] and had come to the United States only nine years previously.{{sfn|Taxay|1983|p=369}}  Either through a misunderstanding or an oversight, Schlag did not include his initials in the design; they would not be added until 1966.<ref name="Bardes 1966-07-24" />  The bust of Jefferson on the obverse closely resembles his bust by sculptor [[Jean-Antoine Houdon]], which is to be found in [[Museum of Fine Arts, Boston|Boston's Museum of Fine Arts]].{{sfn|Vermeule|1971|pp=205–206}}

In early May, it was reported that the Mint required some changes to Schlag's design prior to coining.  Schlag's original design showed a three-quarters view of Monticello, including a tree.  Officials disliked the lettering Schlag had used, a more modernistic style than that used on the eventual coin. The tree was another source of official displeasure; officials decided it was a palm tree and incorrectly believed Jefferson could not have been growing such a thing.  A formal request for changes was sent to Schlag in late May.  The sculptor was busy with other projects and did not work on the nickel until mid-June.  When he did, he changed the reverse to a plain view, or head-on perspective, of Monticello.{{sfn|Bowers|2007|pp=129–131}}  Art historian [[Cornelius Vermeule]] described the change:

<blockquote>
Official taste eliminated this interesting, even exciting, view, and substituted the mausoleum of Roman profile and blurred forms that masquerades as the building on the finished coin.  On the trial reverse the name "Monticello" seemed scarcely necessary and was therefore, logically, omitted.  On the coin as issued it seems essential lest one think the building portrayed is the vault at Fort Knox, a state archives building, or a public library somewhere.{{sfn|Vermeule|1971|p=207}}
</blockquote>

The designs were submitted to the [[Commission of Fine Arts]] for their recommendation in mid-July; the version submitted included the new version of Monticello but may not have included the revised lettering.  The Commission approved the designs.  However, Commission chairman Charles Moore asked that the positions of the mottos on the reverse be switched, with the country name at the top; this was not done.  After the Fine Arts Commission recommendation, the [[United States Secretary of the Treasury|Secretary of the Treasury]], [[Henry Morgenthau, Jr.|Henry Morgenthau]], approved the design.{{sfn|Bowers|2007|pp=129–131}}

On August 21, the Anderson (Indiana) ''Herald'' noted:

<blockquote>
[T]he Federal Fine Arts Commission&nbsp;... didn't like the view of Thomas Jefferson's home, Monticello, so they required the artist to do another picture of the front of the house.<ref>''Sic''; the view is actually the rear elevation, not the front</ref>  They did not like the lettering on the coin.  It wasn't in keeping, but they forgot to say what it wasn't in keeping with&nbsp;... There is no more reason for imitating the Romans in this respect [by using Roman-style lettering on the coin] than there would be for modeling our automobiles after the chariot of [[Ben-Hur: A Tale of the Christ|Ben Hur's]] day.{{sfn|Taxay|1983|p=369}}
</blockquote>

== Production ==
{{See also|United States nickel mintage quantities#Jefferson Head nickels (1938–present)}}

=== 1938–1945: Early minting; World War II changes ===<!-- This section is linked from [[Manganese]] -->
Production of the Jefferson nickel began at all three mints ([[Philadelphia Mint|Philadelphia]], [[Denver Mint|Denver]], and [[San Francisco Mint|San Francisco]]), on October 3, 1938.  By mid-November, some twelve million had been coined, and they were officially released into circulation on November 15; more than thirty million would be struck in 1938.{{sfn|Bowers|2007|pp=141–143}}  According to contemporary accounts, the Jefferson nickel was initially hoarded, and it was not until 1940 that it was commonly seen in circulation.{{sfn|Lange|2006|p=167}}

In 1939, the Mint recut the hub for the nickel, sharpening the steps on Monticello, which had been fuzzy in initial strikings.  Since then, a test for whether a nickel is particularly well struck has been whether all six steps appear clearly, with "full step" nickels more collectable.{{sfn|Bowers|2007|pp=143–144}}

[[File:1945-P-Jefferson-War-Nickel-Reverse.JPG|thumb|left|During [[World War II]], the mint mark of the part-silver "war nickels" appeared above the image of [[Monticello]]]]

With the entry of the United States into [[World War II]], nickel became a critical war material, and the Mint sought to reduce its use of the metal.  On March 27, 1942, Congress authorized a nickel made of 50%&nbsp;copper and 50%&nbsp;silver, but gave the Mint the authority to vary the proportions, or add other metals, in the public interest.  The Mint's greatest concern was in finding an alloy which would use no nickel, but still satisfy counterfeit detectors in vending machines.  An alloy of 56%&nbsp;copper, 35%&nbsp;silver and 9%&nbsp;manganese proved suitable, and this alloy began to be coined into nickels from October 1942.  In the hopes of making them easy to sort out and withdraw after the war, the Mint struck all "war nickels" with a large mint mark appearing above Monticello.  The mint mark P for Philadelphia was the first time that mint's mark had appeared on a US coin.  The prewar composition and smaller mint mark (or no mint mark for Philadelphia) were resumed in 1946.  In a 2000 article in ''[[The Numismatist]]'', Mark A. Benvenuto suggested that the amount of nickel saved by the switch was not significant to the war effort, but that the war nickel served as a ubiquitous reminder of the sacrifices that needed to be made for victory.{{sfn|Bowers|2007|pp=146–148}}

Within the war nickel series collectors recognize two additions, one official, the other counterfeit.  Some 1943-P nickels are overdated.  Here a die for the previous year was reused, allowing a "2" to be visible under the "3".<ref>[http://www.pcgscoinfacts.com/Coin/Detail/84019 1943/2-P 5C, FS (Regular Strike)]</ref>  In addition, a number of 1944 nickels are known without the large "P" mintmark.  These were produced in 1954 by Francis LeRoy Henning, who also made counterfeit nickels with at least four other dates.<ref>[http://www.numismaticenquirer.com/TNE/Henning%20Counterfeit%20Nickel.html Henning Counterfeit Nickel]</ref>

=== 1946–2003: Later production of original designs ===
When it became known that the Denver Mint had struck only 2,630,030&nbsp;nickels in 1950, the coins (catalogued as 1950-D) began to be widely hoarded.  Speculation in them increased in the early 1960s, but prices decreased sharply in 1964. Because they were so widely pulled from circulation, the 1950-D is readily available today.  A number of reverse dies with an S mint mark, intended for the San Francisco Mint, were created in 1955; they were not used as that mint struck no nickels that year and subsequently closed, and the unused dies were sent for use at Denver, where the S mint mark was overpunched with a D.{{sfn|Bowers|2007|p=149}}  1949 and 1954 are other years where one mintmark was punched over another.

[[Proof coin]]s, struck at Philadelphia, had been minted for sale to collectors in 1938 and continued through 1942.{{sfn|Bowers|2007|p=143}}  In the latter year proofs were struck in both the regular and "war nickel" compositions, after which they were discontinued.  Sales of proof coins began again in 1950 and continued until 1964, when their striking was discontinued during the coin shortage.  In 1966 a small change was made to the design to add the initials of the designer (FS) to the obverse, underneath Jefferson's portrait.  In commemoration of that change, two proof 1966 nickels with the initials were struck and presented to him.  Special mint sets, of lower quality than proof coins, were struck from 1965 to 1967.  Proof coin sales resumed in 1968, with coins struck at the reopened San Francisco facility.  Coins struck at any mint between 1965 and 1967 lack mint marks. Beginning in 1968, mint marks were again used, but were moved to the lower part of the obverse, to the right of Jefferson's bust.{{sfn|Bowers|2007|pp=259–260}}  From 1971, no nickels were struck for circulation in San Francisco—the 1971-S was the first nickel struck in proof only since 1878.{{sfn|Bowers|2007|p=222}}  In both 1994 and 1997 matte proof nickels, with distinctive grainy surfaces, were struck in small numbers at the Philadelphia mint for inclusion in [[commemorative coin]] sets.<ref>[http://news.coinupdate.com/the-matte-finish-jefferson-nickels-1685/ The 1994 & 1997 Matte Finish Jefferson Nickels]</ref>

During the late twentieth century the Mint repeatedly modified the design.  In 1982, the steps were sharpened in that year's redesign.  The 1987 modification saw the sharpening of Jefferson's hair and the details of Monticello—since 1987, well-struck nickels with six full steps on the reverse have been relatively common.  In 1993, Jefferson's hair was again sharpened.{{sfn|Bowers|2007|pp=150–151}}

=== 2003–present: Westward Journey nickel series; redesign of obverse ===
[[File:Jefferson medal Reich reverse.jpg|thumb|upright|This reverse of the [[Indian Peace Medal]] struck for Jefferson served as the basis of one of the Western Journey designs]]
In June 2002, Mint officials were interested in redesigning the nickel in honor of the upcoming bicentennial of the [[Lewis and Clark Expedition]].  They contacted the office of Representative [[Eric Cantor]] ([[Republican Party (United States)|Republican]]-[[Virginia]]).  Cantor had concerns about moving Monticello, located in his home state, off the nickel, and sponsored legislation which would allow the Mint to strike different designs in 2003, 2004, and 2005, and again depict Monticello beginning in 2006.<ref name="usatod 2002-04-23" />  The resultant act, the "American 5-Cent Coin Design Continuity Act of 2003", was signed into law on April 23, 2003.  Under its terms, the Treasury Secretary could vary the nickel's designs in honor of the 200th anniversary of the Expedition and of the [[Louisiana Purchase]], but the nickel would again feature Jefferson and Monticello beginning in 2006.<ref name="usmint new nickels" /> Under Cantor's legislation, every future five-cent coin will feature Jefferson and Monticello.<ref name="cornell T31S5112" />

In November 2003, the Mint announced the first two reverse designs, to be struck with Schlag's obverse in 2004.<ref name="Anderson 2003-11-06" /> The first, designed by United States Mint sculptor-engraver Norman E. Nemeth, depicts an adaptation of the [[Indian Peace Medal]]s struck for Jefferson. The second, by Mint sculptor-engraver Alfred Maletsky, depicts a [[keelboat]] like that used by the Expedition.<ref name="usmint 2004 westward nickels" />

[[File:Monticello 2010-10-29.jpg|thumb|left|Monticello returned to the reverse of the Jefferson nickel in 2006]]
The 2005 nickels presented a new image of the former President, designed by Joe Fitzgerald based on Houdon's bust of Jefferson.<ref name="Clark" />  The word "Liberty" was taken from Jefferson's handwritten draft for the [[United States Declaration of Independence|Declaration of Independence]], though to achieve a capital L, Fitzgerald had to obtain one from other documents written by Jefferson.<ref name="Frazier 2005-05-05" />  The reverse for the first half of the year depicted an [[American bison]], recalling the Buffalo nickel and designed by Jamie Franki.  The reverse for the second half showed a coastline and the words "Ocean in view! O!  The Joy!", from a journal entry by [[William Clark (explorer)|William Clark]], co-leader of the Expedition.<ref name="Clark" />  Clark had actually written the word as "ocian", but the Mint modernized the spelling.<ref name="Frazier 2005-05-05" />

The obverse design for the nickel debuting in 2006 was designed by Franki.  It depicts a forward-facing Jefferson based on an 1800 study by [[Rembrandt Peale]], and includes "Liberty" in Jefferson's script.  According to Acting Mint Director David Lebryk, "The image of a forward-facing Jefferson is a fitting tribute to [his] vision."<ref name="BBC 2005-10-06" />  The reverse beginning in 2006 was again Schlag's Monticello design, but newly sharpened by Mint engravers.<ref name="usmint 2006 westward nickels" />  As Schlag's obverse design, on which his initials were placed in 1966, is no longer used, his initials were placed on the reverse to the right of Monticello.<ref name="collectorsweekly Jeff's nickels" />

In 2009, a total of only 86,640,000&nbsp;nickels were struck for circulation.<ref name="usmint 2009 coin production" />  The figure increased in 2010 to 490,560,000.<ref name="usmint 2010 coin production" />  The unusually low 2009 figures were caused by a lack of demand for coins in commerce due to poor economic conditions.<ref name="Unser 2010-01-20" /> 
{{clear}}

== References ==

{{reflist
| colwidth = 20em
| refs =

<ref name="Bardes 1966-07-24">
Bardes, Herbert C. [https://select.nytimes.com/gst/abstract.html?res=FB0C11FC3459117B93C6AB178CD85F428685F9&scp=15&sq=Jefferson%20nickel&st=cse Nickel designer gains his place]. ''[[The New York Times]]'', July 24, 1966, p. 85. Retrieved on April 7, 2011. Fee for article.
</ref>

<ref name="usatod 2002-04-23">
[http://www.usatoday.com/news/washington/2002-07-23-nickel_x.htm Va. legislators want to keep their nickel back]. AP via ''USA TODAY'', July 23, 2002. Retrieved on April 7, 2011.
</ref>

<ref name="usmint new nickels">
[http://www.usmint.gov/pressroom/index.cfm?action=press_release&ID=433 Nation to get newly-designed nickels]. United States Mint, April 24, 2003. Retrieved on April 7, 2011.
</ref>

<ref name="cornell T31S5112">
[http://www.law.cornell.edu/uscode/uscode31/usc_sec_31_00005112----000-notes.html U.S. Code, Title 31, Section 5112]. Cornell University Law School. Retrieved on April 20, 2011.
</ref>

<ref name="Anderson 2003-11-06">
Anderson, Gordon T. [http://money.cnn.com/2003/11/06/news/economy/nickel/ U.S. to get two new nickels]. CNN Money, November 6, 2003. Retrieved on April 7, 2011.
</ref>

<ref name="usmint 2004 westward nickels">
[http://www.usmint.gov/mint_programs/index.cfm?action=NickelSeries2004 The 2004 Westward Journey nickel series designs]. United States Mint. Retrieved on April 7, 2011.
</ref>

<ref name="Clark">
[http://www.usmint.gov/mint_programs/index.cfm?action=nickel_series The 2005 Westward Journey nickel series designs]. United States Mint. Retrieved on April 7, 2011.
</ref>

<ref name="Frazier 2005-05-05">
Frazier, Joseph. [https://news.google.com/newspapers?id=-19WAAAAIBAJ&sjid=efADAAAAIBAJ&pg=5417,747519&dq=clark+ocian+view+nickel&hl=en New nickel recalls historic moment]. AP via ''The Register-Guard'' (Eugene, Ore.), August 5, 2005, p. C7. Retrieved on April 7, 2011.
</ref>

<ref name="BBC 2005-10-06">
[http://news.bbc.co.uk/2/hi/business/4315018.stm US unveils forward-looking nickel]. BBC, October 6, 2005. Retrieved on April 8, 2011.
</ref>

<ref name="usmint 2006 westward nickels">
[http://www.usmint.gov/mint_programs/nickel/?action=returnToMonticello The 2006 Westward Journey nickel series designs]. United States Mint. Retrieved on April 8, 2011.
</ref>

<ref name="collectorsweekly Jeff's nickels">
[http://www.collectorsweekly.com/us-coins/jefferson-nickels Jefferson nickels]. ''[[Collectors Weekly]]''. Retrieved on April 12, 2011.
</ref>

<ref name="usmint 2009 coin production">
[http://www.usmint.gov/about_the_mint/coin_production/index.cfm?action=production_figures&allCoinsYear=2009#starthere 2009 coin production]. [[United States Mint]]. Retrieved on April 20, 2011.
</ref>

<ref name="usmint 2010 coin production">
[http://www.usmint.gov/about_the_mint/coin_production/index.cfm?action=production_figures&allCoinsYear=2010#starthere 2010 coin production]. [[United States Mint]]. Retrieved on April 20, 2011.
</ref>

<ref name="Unser 2010-01-20">
Unser, Darrin Lee. [http://www.coinnews.net/2010/01/20/2009-us-coin-mintages-plummeted-as-mint-cut-production/ US coin mintages plummeted as Mint cut production]. ''Coin News'', January 20, 2010. Retrieved on April 20, 2011.
</ref>

}}

== Bibliography ==

* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 2007
  | title = A Guide Book of Buffalo and Jefferson Nickels
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | url = https://whitman.com/Inventory/Detail/Guide-Book-of-Buffalo-and-Jefferson-Nickels
  | isbn = 978-0-7948-2008-4
  | ref = harv
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and Its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | url = https://whitman.com/Inventory/Detail/History-of-the-United-States-Mint-and-Its-Coinage
  | isbn = 978-0-7948-1972-9
  | ref = harv
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | year = 1983
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York, N.Y.
  | edition = reprint of 1966
  | isbn = 978-0-915262-68-7
  | ref = harv
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, Mass.
  | isbn = 978-0-674-62840-3
  | ref = harv
  }}

{{Nickels}}
{{Coinage (United States coin)}}
{{Thomas Jefferson}}
{{Featured article}}

[[Category:1938 introductions]]
[[Category:Five-cent coins of the United States]]
[[Category:Thomas Jefferson]]