The '''American Society for Photobiology''' (ASP) is a [[scientific society]] for the promotion of research in [[photobiology]], integration of different photobiology disciplines, dissemination of photobiology knowledge, and provides information on photobiological aspects of national and international issues.

==History==
The society was formed in 1972 and held its inaugural meeting on Sunday June 10, 1973 in [[Sarasota]], [[Florida]] under the presidency of founder [[Kendric C. Smith]].<ref>{{cite journal
|url=http://www.pol-us.net/ASP_Home/1973_Doc_of_pictures_and_news_clippings.pdf
|title=News Clippings and Photos, 1973
|format=PDF
|year=1973
|accessdate=12 December 2010
| archiveurl= https://web.archive.org/web/20110105115119/http://www.pol-us.net/ASP_Home/1973_Doc_of_pictures_and_news_clippings.pdf| archivedate= 5 January 2011 <!--DASHBot-->| deadurl= no}}</ref>

==Activities==

===Publications===
The society publishes ''[[Photochemistry and Photobiology]]'' as its official journal. Other publications include the free online textbook, ''Photobiological Sciences Online''.<ref>[http://photobiology.info/ ''Photobiological Sciences Online'']</ref>

===Meetings===
The society met annually from its formation until 2004 and has met biennially since then.<ref>{{cite web|url=http://www.photobiology.org/asp.php?id=1|title=ASP Meetings|accessdate=5 January 2013}}</ref>

===Awards===
The society awards a number of prizes at its meetings including the ASP Research Award, the ASP New Investigator Award, the ASP Photon Award and the ASP Lifetime Achievement Award. Recipients of the Lifetime Achievement Award include [[John Woodland Hastings]], [[Pill-Soon Song]], and [[Margaret Kripke]].

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.photobiology.org/}}

{{DEFAULTSORT:American Society for Photobiology}}
[[Category:Scientific societies]]
[[Category:Organizations established in 1972]]
[[Category:Science and technology in the United States]]