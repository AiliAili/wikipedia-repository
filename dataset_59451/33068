{{featured article}}
{{Use dmy dates|date=December 2016}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Posen NH 46831.jpg|300px]]
| Ship caption = 
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship namesake = [[Province of Posen]]{{sfn|Staff|p=32}}
| Ship builder = [[Germaniawerft]], Kiel
| Ship original cost = 
| Ship laid down = 11 June 1907
| Ship launched = 13 December 1908
| Ship commissioned = 31 May 1910
| Ship fate = Scrapped 1922
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Nassau|battleship}}
| Ship displacement =* Designed: {{convert|18,873|t|LT|abbr=on}}
* Full load: {{convert|20,535|t|LT|abbr=on|-1}}
  
| Ship length = {{convert|146.1|m|ftin|abbr=on|1}}
| Ship beam = {{convert|26.9|m|ftin|abbr=on|1}}
| Ship draft = {{convert|8.9|m|ftin|abbr=on|1}}
| Ship propulsion =* 3-shaft vertical triple expansion
* {{convert|22000|PS|ihp kW|lk=on|abbr=on|-1}}
  
| Ship speed =* Designed: {{convert|19|kn|lk=in}}
* Maximum: {{convert|20|kn}}
  
| Ship range = At {{convert|12|kn}}: {{convert|8,300|nmi|lk=in|abbr=on}}
| Ship complement =* Standard: 40&nbsp;officers, 968&nbsp;men
* Squadron flagship: 53&nbsp;officers, 1,034&nbsp;men
* 2nd command flagship: 42&nbsp;officers, 991&nbsp;men{{efn|name=2CF}}
  
| Ship armament =* 12 × {{convert|28|cm|in|abbr=on|1}} SK L/45 guns
* 12 × {{convert|15|cm|in|abbr=on|1}} SK L/45 guns
* 16 × {{convert|8.8|cm|in|abbr=on|1}} SK L/45 guns
*  5 × {{convert|45|cm|in|abbr=on|1}} torpedo tubes
  
| Ship armor =* Belt: {{convert|300|mm|in|abbr=on|1}}
* Turrets: {{convert|280|mm|in|abbr=on|1}}
* Deck: {{convert|80|mm|in|abbr=on|1}}
* Conning Tower: {{convert|400|mm|in|abbr=on|1}}
  
}}
|}

'''SMS ''Posen'''''{{efn|name=SMS}} was one of four [[battleship]]s in the {{sclass-|Nassau|battleship|4}}, the first [[dreadnought]]s built for the [[German Imperial Navy]] (''Kaiserliche Marine''). The ship was [[laid down]] at the [[Germaniawerft]] shipyard in [[Kiel]] on 11 June 1907, launched on 13 December 1908, and commissioned into the [[High Seas Fleet]] on 31 May 1910. She was equipped with a main battery of twelve {{convert|28|cm|in|abbr=on}} guns in six twin [[Gun turret|turrets]] in an unusual hexagonal arrangement.

The ship served with her three sister ships for the majority of World War I. She saw extensive service in the [[North Sea]], where she took part in several fleet sorties. These culminated in the [[Battle of Jutland]] on 31&nbsp;May&nbsp;– 1&nbsp;June 1916, where ''Posen'' was heavily engaged in night-fighting against British light forces. In the confusion, the ship accidentally rammed the light cruiser {{SMS|Elbing}}, which suffered serious damage and was [[scuttling|scuttled]] later in the night.

The ship also conducted several deployments to the [[Baltic Sea]] against the [[Russian Navy]]. In the first of these, ''Posen'' supported a German naval assault in the [[Battle of the Gulf of Riga]]. The ship was sent back to the Baltic in 1918 to support the [[White Guard (Finland)|White Finns]] in the [[Finnish Civil War]]. At the end of the war, ''Posen'' remained in Germany while the majority of the fleet was interned in [[Scapa Flow]]. In 1919, following the [[scuttling of the German fleet in Scapa Flow]], she was ceded to the British as a replacement for the ships that had been sunk. She was then sent to [[ship breaking|ship-breakers]] in the Netherlands and scrapped in 1922.

== Description ==
{{main|Nassau-class battleship}}

[[File:Nassau class main weapon.svg|thumb|left|Line drawing of a ''Nassau''-class battleship showing the disposition of the main battery]]

''Posen'' was {{convert|146.1|m|ftin|abbr=on|1}} long, {{convert|26.9|m|ftin|abbr=on|1}} wide, and had a draft of {{convert|8.9|m|ftin|abbr=on|1}}. She displaced {{convert|18,873|t|LT|abbr=on}} with a standard load, and {{convert|20,535|t|LT|abbr=on}} fully laden.{{efn|name=displacement}} The ship had a crew of 40&nbsp;officers and 968&nbsp;enlisted men. ''Posen'' retained three-shafted [[Steam engine#Multiple expansion engines|triple expansion engines]] with coal-fired boilers instead of more advanced [[turbine]] engines. Her propulsion system was rated at {{convert|28000|PS|ihp kW|lk=on|abbr=on|0}} and provided a top speed of {{convert|20|kn|lk=in}}. She had a cruising radius of {{convert|8300|nmi|lk=in}} at a speed of {{convert|12|kn}}.{{sfn|Gröner|p=23}} This type of machinery was chosen at the request of both Admiral [[Alfred von Tirpitz]] and the Navy's construction department; the latter stated in 1905 that the "use of turbines in heavy warships does not recommend itself."{{sfn|Herwig|pp=59–60}} This decision was based solely on cost: at the time, [[Parsons Marine Steam Turbine Company|Parsons]] held a monopoly on steam turbines and required a 1&nbsp;million [[gold mark]] royalty fee for every turbine engine. German firms were not ready to begin production of turbines on a large scale until 1910.{{sfn|Staff|pp=23, 35}}

''Posen'' carried twelve [[28 cm SK L/45 gun|{{convert|28|cm|in|abbr=on|1}} SK L/45]]{{efn|name=gun nomenclature}} guns in an unusual hexagonal configuration. Her secondary armament consisted of twelve [[15 cm SK L/45|{{convert|15|cm|in|abbr=on|1}} SK L/45 guns]] and sixteen [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on|1}} SK L/45 guns]], all of which were mounted in [[casemate]]s.{{sfn|Gröner|p=23}} Later in her career, two of the 8.8&nbsp;cm guns were replaced with high-angle Flak mountings of the same caliber for defense against aircraft.{{sfn|Hildebrand Röhr & Steinmetz|p=239}} The ship was also armed with six {{convert|45|cm|in|abbr=on|1}} submerged [[torpedo tube]]s. One tube was mounted in the bow, another in the stern, and two on each broadside, on either ends of the [[torpedo bulkhead]].{{sfn|Gardiner & Gray|p=140}} The ship's [[belt armor]] was {{convert|300|mm|abbr=on|1}} thick in the central portion of the hull, and the armored deck was {{convert|80|mm|abbr=on|1}} thick. The main battery turrets had {{convert|280|mm|abbr=on|1}} thick sides, and the [[conning tower]] was protected with {{convert|400|mm|abbr=on|1}} of armor plating.{{sfn|Gröner|p=23}}

== Service history ==
[[File:First and second battleship squadrons and small cruiser of the - NARA - 533188-2 restored.jpg|thumb|''Posen'' and the rest of the [[I Battle Squadron]] in [[Kiel]] before the war]]

''Posen'' was ordered under the provisional name ''Ersatz Baden'', as a replacement for the {{SMS|Baden|1880|2}}, one of the elderly {{sclass-|Sachsen|ironclad|2}}s.{{sfn|Gröner|p=23}} She was laid down on 11 June 1907 at the [[Germaniawerft]] shipyard in [[Kiel]].{{sfn|Staff|p=20}} As with her sister {{SMS|Nassau||2}}, construction proceeded under absolute secrecy; detachments of soldiers guarded the shipyard and also guarded contractors that supplied building materials, such as [[Krupp]].{{sfn|Hough|p=26}} The ship was launched a year and a half later, on 12 December 1908.{{sfn|Staff|p=20}} Wilhelm August Hans von Waldow-Reitzenstein gave a speech at her launching, and ''Posen'' was christened by Johanna von Radolin, the wife of Hugo Fürst von Radolin, a German diplomat who hailed from the ship's namesake province.{{sfn|Hildebrand Röhr & Steinmetz|p=239}} Initial trials were conducted through April 1910, followed by final [[fitting-out]] in May. The ship was commissioned into the fleet on 31 May. [[Sea trial]]s were conducted afterward and completed by 27 August.{{sfn|Staff|p=32}} In total, her construction cost the German government 36,920,000&nbsp;[[German gold mark|marks]].{{sfn|Gröner|p=23}}

After completing her trials in August 1910, ''Posen'' left Kiel for [[Wilhelmshaven]], where she arrived on 7 September. As the German Imperial Navy had chronic shortages of trained sailors,{{sfn|Herwig|p=90}} many of the crew were then assigned to other ships.{{sfn|Staff|p=32}}{{efn|name=personnel shortages}} These crewmembers were replaced with personnel from the old [[pre-dreadnought]] {{SMS|Wittelsbach||2}}, which was decommissioned on 20 September. After their commissioning, all four ''Nassau''-class ships served as a unit, the II Division of I&nbsp;Battle Squadron, with ''Posen'' as the [[flagship]].{{sfn|Staff|p=32}}

''Posen'' participated in several training exercises with the rest of the fleet before the outbreak of war.{{sfn|Staff|p=32}} In late 1910 the fleet conducted a training cruise into the [[Baltic Sea]]. The following year the fleet conducted maneuvers in May; the annual summer cruise to Norway followed in July. The fleet participated in another round of fleet exercises in the Baltic in September, followed by another set at the end of the year. The next year followed a similar pattern, though the summer cruise to Norway was interrupted by the [[Agadir crisis]]; as a result, the summer cruise only went into the Baltic. The September exercises were conducted off [[Helgoland]] in the [[North Sea]]; another winter cruise into the Baltic followed at the end of the year.{{sfn|Staff|p=8}} The training schedule returned to normal for 1913 and 1914, and the summer cruises again went to Norway. For the 1914 cruise, the fleet departed for Norwegian waters on 14 July, some two weeks after the [[assassination of Archduke Franz Ferdinand]] in [[Sarajevo]]. The probability of war cut the cruise short; ''Posen'' and the rest of the fleet were back in Wilhelmshaven by 29 July.{{sfn|Staff|p=11}}

=== World War I ===
At midnight on 4 August, the United Kingdom declared war on Germany.{{sfn|Herwig|p=144}} ''Posen'' and the rest of the fleet conducted several advances into the North Sea to support Rear Admiral [[Franz von Hipper]]'s [[I Scouting Group]] [[battlecruiser]]s.{{sfn|Staff|p=32}} The battlecruisers raided British coastal towns in an attempt to lure out portions of the [[Grand Fleet]] where they could be destroyed by the High Seas Fleet.{{sfn|Herwig|pp=149–150}} The first such operation was the [[raid on Scarborough, Hartlepool and Whitby]] on 15–16 December 1914.{{sfn|Tarrant|p=31}} On the evening of 15 December, the German battle fleet of 12 dreadnoughts—including ''Posen'' and her three sisters—and eight pre-dreadnoughts came to within {{convert|10|nmi|abbr=on}} of an isolated squadron of six British battleships. Skirmishes between the rival [[destroyer]] screens in the darkness convinced the German fleet commander, Admiral [[Friedrich von Ingenohl]], that he was faced with the Grand Fleet, now deployed in its battle formation. Under orders from [[Kaiser Wilhelm II]] to avoid risking the fleet unnecessarily, von Ingenohl broke off the engagement and turned the battlefleet back toward Germany.{{sfn|Tarrant|pp=31–33}}

==== Battle of the Gulf of Riga ====
{{main|Battle of the Gulf of Riga}}

In August, a special unit from the German fleet attempted to clear the Russian-held [[Gulf of Riga]] in order to assist the German Army, which was planning an assault on [[Riga]]. To do so, the German planners intended to drive off or destroy the Russian naval forces in the Gulf, which included the pre-dreadnought battleship {{ship|Russian battleship|Slava||2}} and some smaller [[gunboat]]s and destroyers. The German battle fleet was accompanied by several mine-warfare vessels. These ships were tasked with clearing Russian minefields and laying a series of their own minefields in the northern entrance to the gulf, to prevent Russian naval reinforcements from reaching the area. The assembled German flotilla included ''Posen'' and her three sister ships, the four {{sclass-|Helgoland|battleship|2}}s, the battlecruisers {{SMS|Von der Tann||2}}, {{SMS|Moltke||2}}, and {{SMS|Seydlitz||2}}, and several pre-dreadnoughts, operating under the command of von Hipper, now a Vice Admiral. The eight battleships were to provide cover for the forces engaging the Russian flotilla. The first attempt on 8 August was unsuccessful, as it had taken too long to clear the Russian minefields to allow the minelayer {{SMS|Deutschland|1914|2}} to lay a minefield of her own.{{sfn|Halpern|pp=196–197}}

On 16 August 1915, ''Posen'' and ''Nassau'' led a second attempt to breach the defenses of the gulf, with ''Posen'' as Admiral Schmidt's flagship.{{sfn|Staff|p=32}} The two dreadnoughts were accompanied by 4&nbsp;light cruisers and 31&nbsp;torpedo boats. On the first day of the assault the Germans broke through the Russian forces, but two German light craft—the minesweeper {{SMS|T46||2}} and the destroyer {{SMS|V99||2}}—were sunk.{{sfn|Halpern|p=197}} ''Posen'' and ''Nassau'' engaged a pair of Russian gunboats, {{ship|Russian gunboat|Sivuch||2}} and {{ship|Russian gunboat|Korietz||2}}. ''Sivuch'' was sunk that day and ''Korietz'' was severely damaged; the ship managed to limp away but had to be scuttled the following day.{{sfn|Staff|p=32}} On the 17th, ''Posen'' and ''Nassau'' engaged ''Slava'' at long range; they scored three hits on the Russian ship and forced her to return to port. By 19 August, the Russian minefields had been cleared and the flotilla entered the Gulf. Reports of Allied submarines in the area prompted the Germans to call off the operation the following day.{{sfn|Halpern|pp=197–198}} Admiral Hipper later remarked, "To keep valuable ships for a considerable time in a limited area in which enemy submarines were increasingly active, with the corresponding risk of damage and loss, was to indulge in a gamble out of all proportion to the advantage to be derived from the occupation of the Gulf ''before'' the capture of Riga from the land side." In fact, the battlecruiser ''Moltke'' had been torpedoed that morning.{{sfn|Halpern|p=198}} On 21 August, Schmidt had his flag hauled down from ''Posen'' and disbanded the special unit.{{sfn|Staff|p=32}}

==== Return to the North Sea ====
[[File:SMS Westfalen LOC 25466u.jpg|thumb|SMS ''Westfalen''|alt=A large warship steams at low speed; gray smoke drifts from the two smoke stacks]]

By the end of August ''Posen'' and the rest of the High Seas Fleet had returned to their anchorages in the North Sea. The next operation conducted was a sweep into the North Sea on 11–12 September, though it ended without any action. Another fleet sortie followed on 23–24 October without encountering any British forces. On 4 March 1916, ''Posen'', ''Nassau'', ''Westfalen'', and ''Von der Tann'' steamed out to the Amrumbank to receive the [[auxiliary cruiser]] {{SMS|Möwe||2}}, which was returning from a raiding mission.{{sfn|Staff|p=33}}

Another uneventful advance into the North Sea took place on 21–22 April. A bombardment mission followed two days later; ''Posen'' joined the battleship support for Hipper's battlecruisers while they [[Bombardment of Yarmouth and Lowestoft|attacked Yarmouth and Lowestoft]] on 24–25 April.{{sfn|Staff|p=33}} During this operation, the battlecruiser ''Seydlitz'' was damaged by a British mine and had to return to port prematurely. Due to the poor visibility, the operation was soon called off, leaving the British fleet no time to intercept the raiders.{{sfn|Tarrant|pp=52–54}}

==== Battle of Jutland ====
{{main|Battle of Jutland}}
[[File:Map of the Battle of Jutland, 1916.svg|thumb|350px|Maps showing the maneuvers of the British (blue) and German (red) fleets on 31 May&nbsp;– 1 June 1916]]

Admiral [[Reinhard Scheer]], who had succeeded Admirals von Ingenohl and [[Hugo von Pohl]] as the fleet commander, immediately planned another attack on the British coast. The damage to ''Seydlitz'' and [[Condenser (heat transfer)|condenser]] trouble on several of the III Battle Squadron dreadnoughts delayed the plan until the end of May.{{sfn|Tarrant|pp=56–58}} The German battlefleet departed the [[Jade Bight|Jade]] at 03:30 on 31 May.{{sfn|Tarrant|p=62}} ''Posen'' was assigned to the II Division of the I Battle Squadron as the flagship of Rear Admiral W. Engelhardt. ''Posen'' was the first ship in the division, ahead of her three sisters. The II Division was the last unit of dreadnoughts in the fleet; they were followed by only the elderly pre-dreadnoughts of the II Battle Squadron.{{sfn|Tarrant|p=286}}

Between 17:48 and 17:52, ''Posen'' and ten other German battleships engaged the British 2nd Light Cruiser Squadron, though the range and poor visibility prevented effective fire.{{sfn|Campbell|p=54}} Shortly thereafter, two British destroyers—{{HMS|Nomad||2}} and {{HMS|Nestor|1915|2}}—came under intense fire from the German line. ''Posen'' fired at ''Nestor'' with both her main battery and secondary guns. At 18:35, ''Nestor'' exploded and sank under the combined fire of eight battleships.{{sfn|Campbell|p=101}} By 20:15, the German fleet had faced the Grand Fleet for a second time and was forced to turn away; in doing so, the order of the German line was reversed. ''Posen'' was now the fourth ship in the line, astern of her three sisters.{{sfn|Tarrant|p=172}}

At around 21:20, ''Posen'' and her sister ships were engaged by the battlecruisers of the [[3rd Battlecruiser Squadron (United Kingdom)|3rd Battlecruiser Squadron]]. ''Posen'' was the only ship on the I Battle Squadron to be able to make out a target, which turned out to be the battlecruisers {{HMS|Princess Royal|1911|6}} and {{HMS|Indomitable|1907|2}}. ''Posen'' opened fire at 21:28 at a range of {{convert|10000|m|yd|abbr=on}}; she scored one hit on ''Princess Royal'' at 21:32 and straddled ''Indomitable'' several times, surrounding her with a salvo of shells, before ceasing fire at 21:35.{{sfn|Campbell|p=254}}

At about 00:30, the leading units of the German line encountered British destroyers and cruisers. A violent firefight at close range ensued; the leading German battleships, including ''Posen'', opened fire on several British warships. In the confusion, the light cruiser {{SMS|Elbing||2}} passed through the German line directly in front of ''Posen'' and was rammed. ''Posen'' was undamaged, but both of ''Elbing''{{'}}s engine rooms were flooded and the ship came to a halt.{{sfn|Campbell|p=286}} Two and a half hours later, ''Elbing'' spotted several approaching British destroyers, and her captain gave the order to scuttle the ship.{{sfn|Campbell|p=295}}

Shortly before 01:00, the German line engaged a flotilla of British destroyers. ''Posen'' spotted the destroyers {{HMS|Fortune|1913|2}}, {{HMS|Porpoise|1913|2}}, and {{HMS|Garland|1913|2}} at very close range; she opened fire on the first two ships at ranges between {{convert|800|and|1600|m|yd|abbr=on}}, seriously damaging ''Porpoise''. ''Fortune'' quickly sank under fire from ''Posen'' and several other battleships, but not before firing two torpedoes which ''Posen'' had to evade.{{sfn|Campbell|p=289}} At 01:25, ''Westfalen'' illuminated the destroyer {{HMS|Ardent|1913|2}} and opened fire; ''Posen'' joined her shortly thereafter and reported several hits at ranges of {{convert|1000|to|1200|m|yd|abbr=on}}.{{sfn|Campbell|p=290}}

Despite the ferocity of the night fighting, the High Seas Fleet punched through the British destroyer forces and reached [[Horns Reef]] by 04:00 on 1&nbsp;June.{{sfn|Tarrant|pp=246–247}} The German fleet reached Wilhelmshaven a few hours later, where ''Posen'' and several other battleships from the I Battle Squadron took up defensive positions in the outer [[roadstead]].{{sfn|Tarrant|p=263}} Over the course of the battle, the ship had fired fifty-three 28&nbsp;cm shells, sixty-four 15&nbsp;cm rounds, and thirty-two 8.8&nbsp;cm shells.{{sfn|Tarrant|p=292}} The ship and her crew emerged from the battle completely unscathed by enemy fire.{{sfn|Staff|p=33}}

Beginning in June 1917, [[Wilhelm von Krosigk]] served as the ship's commanding officer; he held this position until the end of the war in November 1918.{{sfn|Hildebrand Röhr & Steinmetz|p=239}}

==== Expedition to Finland ====
[[File:SMS Nassau illustration.jpg|alt=A large gray warship steams through calm seas; thick black smoke pours from two tall smoke stacks|thumb|A recognition drawing of a ''Nassau''-class battleship]]
In February 1918, the German navy decided to send an expedition to Finland to support German army units to be deployed there. The Finns were engaged in a civil war; the [[White Guard (Finland)|White Finns]] sought a conservative government free from the influence of the newly created [[Soviet Union]], while the [[Red Guards (Finland)|Red Guards]] preferred Soviet-style communism. On 23 February, two of ''Posen''{{'}}s sister ships—''Westfalen'' and ''Rheinland''—were assigned as the core of the ''Sonderverband Ostsee'' (Special Unit Baltic Sea).{{sfn|Ganz|pp=85–86}} The two ships embarked the 14th [[Jäger (military)|Jäger]] Battalion. They departed for the [[Åland Islands]] on the following morning. Åland was to be a forward operating base, from which the port of [[Hanko]] would be secured. From Hanko, the German expedition would assault the capital of [[Helsingfors]]. The task force reached the Åland Islands on 5 March, where they encountered the Swedish [[coastal defense ship]]s {{HSwMS|Sverige}}, {{HSwMS|Thor|1898|2}}, and {{HSwMS|Oscar II||2}}. Negotiations ensued, which resulted in the landing of the German troops on Åland on 7 March; ''Westfalen'' then returned to [[Danzig]], where ''Posen'' was stationed.{{sfn|Staff|p=27}}

On 31 March ''Posen'' and ''Westfalen'' left Danzig; the ships arrived at [[Russarö]], which was the outer defense for Hanko, by 3 April. The German army quickly took the port. The task force then proceeded to Helsingfors; on 11 April the ship passed into the harbor at Helsingfors and landed the soldiers. This included a detachment from the ship which was landed two days later on 13 April. During the operation, ''Posen''{{'}}s crew suffered four men killed and twelve wounded. From 18 to 20 April, ''Posen'' assisted with the efforts to free ''Rheinland'', which had been grounded. Two days later, ''Posen'' struck a sunken wreck in Helsingfors harbor, which caused minor damage. On 30 April the ship was detached from the ''Sonderverband Ostsee''. The ship returned to Germany, reaching Kiel by 3 May, where she entered drydock. Repair work lasted until 5 May.{{sfn|Staff|p=33}}

==== Later actions in the North Sea ====
On 11 August 1918, ''Posen'', ''Westfalen'', {{SMS|Kaiser|1911|2}}, and {{SMS|Kaiserin||2}} sortied from Wilhelmshaven to support torpedo boats on patrol off [[Terschelling]].{{sfn|Staff|p=27}} On 2 October, ''Posen'' moved out into the outer roadsteads of the Jade to provide cover for the returning [[U-boat]]s of the [[Flanders Flotilla]]. ''Posen'' was to have taken part in the [[Naval order of 24 October 1918|last fleet operation]] of the war,{{sfn|Staff|p=33}} planned for 30 October. The operation was intended to inflict as much damage as possible on the British navy, in order to retain a better bargaining position for Germany, whatever the cost to the fleet. War-weary sailors [[Wilhelmshaven mutiny|mutinied]], which led to the operation being canceled. In an attempt to suppress the spread of mutinous sentiments, Admiral Hipper ordered the fleet dispersed.{{sfn|Tarrant|pp=280–281}} ''Posen'' and the other ships of the I Battle Squadron were sent out into the roadstead on 3 November, then returned to Wilhelmshaven on 6 November.{{sfn|Staff|p=33}}

=== Fate ===
On 11 November 1918, the [[Armistice with Germany|Armistice]] took effect; according to its terms, eleven battleships and five battlecruisers were to be interned in [[Scapa Flow]] for the duration of negotiations for the peace treaty.{{sfn|Gardiner & Gray|p=139}} ''Posen'' was not among the ships interned, and she was instead decommissioned on 16 December. The ships in Scapa Flow were [[scuttling of the German fleet in Scapa Flow|scuttled by their crews on 21 June 1919]] to prevent them from being seized by the Allies. As a result, ''Posen'' and the other battleships that remained in Germany were seized as replacements for the ships that had been lost. On 5 November, ''Posen'' was stricken from the German navy list to be handed over to Great Britain. The ship was transferred on 13 May 1920; on 1 November 1920, she was driven ashore at [[Hawkcraig]], [[Fife]], [[Scotland]].<ref>{{Cite newspaper The Times |articlename=Casualty reports |day_of_week=Tuesday |date=2 November  1920 |page_number=18 |issue=42557 |column=D }}</ref> The British subsequently sold ''Posen'' to ship-breakers in the [[Netherlands]].{{sfn|Staff|p=33}} She was broken up in [[Dordrecht]] in 1922.{{sfn|Gröner|p=24}}

== Notes ==

'''Footnotes'''

{{notes
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''", or "His Majesty's Ship".
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick firing, while "L/45" provides the length of the gun in terms of the diameter of the barrel. In this case, the L/45 gun is 45 [[caliber (artillery)|calibers]], which means that the gun is 45&nbsp;times as long as its diameter. See: {{harvnb|Grießmer|p=177}}.
}}

{{efn
| name = personnel shortages
| Of the problem, Admiral Oldekop stated in November 1912: "There is almost no more personnel on land. Garrison watch duty can no longer be conducted according to regulations...The Navy units [in Kiel] are equally exhausted as in Wilhelmshaven. Complaints concerning lack of personnel on land are the same." See: {{harvnb|Herwig|p=90}}.
}}

{{efn
| name = displacement
| According to Erich Gröner, this figure is calculated as "25 to 50 percent full load...and [was] used in the German navy since 1882 as a basis for performance and speed calculations." See: {{harvnb|Gröner|p=ix}}
}}

{{efn
| name = 2CF
| Each eight-ship battle squadron had a flagship that also functioned as the flagship of the first four-ship division; a second command flagship served as the leader of the second division in each squadron. At the Battle of Jutland, ''Posen'' served as the second command flagship, flying the flag of Rear Admiral Walter Engelhardt. See: {{harvnb|Tarrant|p=286}}
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==

{{portal|Battleships}}

* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell}}
  }}
* {{cite journal
  | last = Ganz
  | first = A. Harding
  |date=April 1980
  | title = The German Expedition to Finland, 1918
  | journal = Military Affairs
  | volume = 44
  | issue = 2
  | pages = 84–91
  | publisher = Society for Military History
  | location = Lexington, VA
  | doi = 10.2307/1986604
  | ref = {{sfnRef|Ganz}}
  | jstor = 1986604
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Grießmer
  | first = Axel
  | year = 1999
  | language = German
  | title = Die Linienschiffe der Kaiserlichen Marine
  | publisher = Bernard & Graefe Verlag
  | location = Bonn
  | isbn = 978-3-7637-5985-9
  | oclc = 
  | ref = {{sfnRef|Grießmer}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last1 = Hildebrand
  | first1 = Hans H.
  | last2 = Röhr
  | first2 = Albert
  | last3 = Steinmetz
  | first3 = Hans-Otto
  | year = 1993
  | title = Die Deutschen Kriegsschiffe
  | volume = 6
  | location = [[Ratingen]]
  | publisher = Mundus Verlag
  | isbn = 978-3-8364-9743-5
  | oclc = 
  |asin=B003VHSRKE |asin-tld=de
  | ref = {{sfnRef|Hildebrand Röhr & Steinmetz}}
  }}
* {{cite book
  | last = Hough
  | first = Richard
  | authorlink = Richard Hough
  | year = 2003
  | title = Dreadnought: A History of the Modern Battleship
  | publisher = Periscope Publishing
  | location = Penzance, Cornwall, UK
  | isbn = 978-1-904381-11-2
  | ref = {{sfnRef|Hough}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 1
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-467-1
  | oclc = 705750106
  | ref = {{sfnRef|Staff}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

{{Nassau class battleship}}

{{DEFAULTSORT:Posen}}
[[Category:Nassau-class battleships]]
[[Category:Ships built in Kiel]]
[[Category:1908 ships]]
[[Category:World War I battleships of Germany]]
[[Category:Maritime incidents in 1920]]
[[Category:Shipwrecks of Scotland]]