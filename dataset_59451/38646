{{Infobox song
| Name       = Bravado
| Border     = yes
| Artist     = [[Lorde]]
| Album      = [[The Love Club EP]] 
| Type       = [[Promotional recording|Promotional single]]
| Released   = 6 September 2013<ref name="itunesde">{{cite web|url=https://itunes.apple.com/de/album/bravado-single/id689042224|title=Bravado &ndash; Single von Lorde|language=German|publisher=[[iTunes Store]] (DE). [[Apple Inc]]|accessdate=2014-01-12}}</ref>
| Format     = [[Music download|Digital download]]
| Recorded   = {{nowrap|At [[Golden Age Studios]]}}, {{small| {{nowrap|[[Morningside, Auckland]]}}, {{nowrap|[[New Zealand]]}}}}
| Length     = 3:41
| Genre      = {{hlist|[[Chamber pop]]|[[electropop]]}}
| Label      = [[Universal Music Group|Universal]]
| Writer     = {{hlist|[[Lorde|Ella Yelich-O'Connor]]|[[Joel Little]]}}
| Producer   = Joel Little
}}
"'''Bravado'''" is a song by New Zealand singer [[Lorde]], originally included on her debut [[Extended play|EP]] ''[[The Love Club EP]]''. It was later featured on her ''[[Tennis Court (song)|Tennis Court EP]]'' and the extended version of her debut album ''[[Pure Heroine]]'' (2013). The song was written by Lorde and [[Joel Little]] and was produced by the latter. The track was released as a single on 6 September 2013, via [[iTunes Store]]s, in a number of European countries and India. Characterised as a [[chamber pop]] and [[electropop]] song, "Bravado" addresses Lorde's introverted nature and the need to feign confidence in the music industry. The single was well received by [[music journalism|music critics]] and peaked at number five on the New Zealand Artist Singles chart.

==Background and composition==
{{listen
 | filename    = Bravado Lorde.ogg
 | title       = "Bravado"
 | description = A [[chamber pop]] and [[electropop]] song, "Bravado" describes Lorde's guise of confidence
 | pos         = left
}}
"Bravado" was written by Lorde (credited under her birth-name Ella Yelich-O'Connor) and [[Joel Little]], while production for the song was handled by the latter.<ref name="Notes">{{cite web|url=http://www.allmusic.com/album/the-love-club-mw0002512787|title=The Love Club &ndash; Lorde|publisher=[[AllMusic]]. [[All Media Network]]|accessdate=2014-01-16}}</ref> The track was written and recorded in 2012 at Little's Golden Age Studios in [[Auckland]].<ref>{{cite journal|url=http://www.nzmusician.co.nz/index.php/ps_pagename/article/pi_articleid/3521/pi_page/2 |title=Joel Little &ndash; Rings Of The Lorde (page 2) |first=Richard |last=Thorne |journal=NZ Musician |date=October–November 2013 |volume=17 |issue=9 |page=2 |accessdate=1 June 2014 |archiveurl=http://www.webcitation.org/6Pzr66ZNT?url=http%3A%2F%2Fwww.nzmusician.co.nz%2Findex.php%2Fps_pagename%2Farticle%2Fpi_articleid%2F3521 |archivedate=1 June 2014 |deadurl=yes |df= }}</ref><ref>{{cite web|url=http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=11126667 |title=Joel Little: Doing the Lorde's work |first=Lydia |last=Jenkin |work=[[The New Zealand Herald]]|publisher=[[APN News & Media]]|date=2013-09-19|accessdate=2013-12-30}}</ref> Within a week, Lorde had finished recording "Bravado" alongside "[[Royals (song)|Royals]]" and "Biting Down" during a school break.<ref>{{cite web|url=https://www.theguardian.com/music/2014/apr/11/joel-little-lorde|title=The secrets of Lorde's right-hand man, Joel Little|work=[[The Guardian]]|publisher=[[Guardian News and Media]]|first=Huw|last=Oliver|date=11 April 2014|accessdate=2 June 2014}}</ref> Chris Schulz, from ''[[The New Zealand Herald]]'', described "Bravado" as a [[chamber pop]] piece,<ref name="NZHerald">{{cite web|url=http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=10872495 |title=Album review: Lorde, The Love Club EP |first=Chris |last=Schulz|work=The New Zealand Herald|publisher=APN News & Media|date=2013-03-21 |accessdate=2014-01-02}}</ref> while ''[[The Village Voice]]''{{'}}s Brittany Spanos called it an [[electropop]] song.<ref name="VillageVoice">{{cite web|url=http://blogs.villagevoice.com/music/2013/10/lorde_-_webster.php|title=Lorde &ndash; Webster Hall|work=[[The Village Voice]]|publisher=[[Village Voice Media]]|date=2013-10-01|accessdate=2014-08-06|first=Brittany|last=Spanos}}</ref> Running for a duration of {{duration|m=3|s=41}} (three minutes and 41 seconds),<ref name="Notes"/> the track is composed on [[electronic music|electronic]] [[beat (music)|beats]] in the [[key (music)|key]] of [[B minor]] and plays in [[common time]] at a moderate [[tempo]] of 88 [[beats per minute]].<ref name = "musicnotes"/><ref>{{cite news|title=Spinning Around|first=Nick|last=Mason|work=[[mX (newspaper)|mX]]|publisher=[[News Corp Australia]]|date=11 April 2013|page=17}}</ref> Lorde's vocal range on the song spans from [[F (musical note)|F{{music|sharp}}<sub>3</sub>]] to [[D (musical note)|D<sub>5</sub>]].<ref name = "musicnotes">{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdFPE.asp?ppn=mn0126438&ref=google|title=Lorde "Bravado" Sheet Music|publisher=Musicnotes.com. [[EMI Music Publishing]]|accessdate=2014-01-13}}</ref> The lyrics of "Bravado" address the idea "of false confidence delivering real confidence" and of Lorde stepping into a line of work where, she said, "everyone would be watching me, and everyone would want to talk to me and confront me".<ref name = "SpotifyBlog">{{cite web|last=Katz|first=Candance|date=2013-05-29|url=http://news.spotify.com/us/2013/05/29/our-interview-with-lorde/|title=Our Interview with Lorde|publisher=[[Spotify]]. Spotify Ltd|accessdate=2014-01-12}}</ref> Lorde cited the line "Me found bravery in my bravado" from [[Kanye West]]'s song "[[Dark Fantasy (song)|Dark Fantasy]]" as an influence.<ref>{{cite news|url=http://www.mtv.co.uk/lorde/news/lorde-i-get-paralyzingly-nervous |title=Lorde: "I Get Paralyzingly Nervous" |publisher=[[MTV UK]]. [[MTV Networks]]|date=2014-01-15 |accessdate=2014-02-01}}</ref> Simon Collins, of ''[[The West Australian]]'', called "Bravado" a particularly prescient song, preempting Lorde's rise to prominence.<ref>{{cite news|title=She's so good it's sick|work=[[The West Australian]]|publisher=[[Seven West Media Limited]]|date=7 July 2014|page=2|first=Simon|last=Collins}}</ref> At a point of the song, Lorde confesses "to a battle between shyness and show-business aspirations":
<poem>
:I learned not to want
:The quiet of the room with no one around to find me out
:I want the applause, the approval, the things that make me go.<ref name="TheNYTimes">{{cite web|url=https://www.nytimes.com/2013/10/02/arts/music/shes-16-but-not-thinking-of-sweet.html?_r=0|title=She's 16, but Not Thinking of Sweet|work=[[The New York Times]]|publisher=[[The New York Times Company]]|date=2013-10-02|first=Jon|last=Pareles|accessdate=2014-08-06|authorlink=Jon Pareles}}</ref></poem>

==Promotion and reception==
On 6 September 2013, "Bravado" was released as a [[music download|digital download]] single on [[iTunes Store]]s in India and various European countries.<ref name="itunesde"/> Lorde held a concert at [[(Le) Poisson Rouge|Le Poisson Rouge]] in New York, which was her first U.S. live show, and performed the song among other tracks from ''The Love Club EP''.<ref name="USshow">{{cite web|url=http://www.billboard.com/articles/columns/pop-shop/5638325/lorde-hypnotizes-at-first-us-show-live-review|title=Lorde Hypnotizes At First U.S. Show: Live Review|first=Jason|last=Lipshutz|work=Billboard|publisher=Nielsen Business Media|date=2013-08-07|accessdate=2014-01-16}}</ref> On 3 October 2013, the singer held a concert at the Warsaw Venue in Brooklyn and performed the song among other tracks from the album.<ref>{{cite web|url=http://www.complex.com/music/2013/10/lorde-concert-review-brooklyn-warsaw|title=Live Review: Lorde Brings Brooklyn More Than a Chart-Topper|first=Foster|last=Kamer|date=2013-10-04|work=[[Complex (magazine)|Complex]]|publisher=Complex Media|accessdate=2014-01-02}}</ref> In November 2013, Lorde performed the song on the ''[[Late Show with David Letterman]]'' to promote ''The Love Club EP'' and ''Pure Heroine''. Throughout the show, she was only backed by a drummer and a keyboard player.<ref name="RS">{{cite web|url=http://www.rollingstone.com/music/videos/lorde-is-blissful-during-six-song-live-on-letterman-set-20131113|title=Lorde Is Blissful During Six-Song 'Live on Letterman' Set|first=Kory|last=Grow|work=[[Rolling Stone]]|publisher=[[Wenner Media]]|date=2013-11-13|accessdate=2014-01-09}}</ref> "Bravado" was additionally performed during Lorde's debut concert tour throughout 2013 and 2014.<ref>{{cite web|url=http://mix965houston.cbslocal.com/2014/03/06/lorde-concert-set-list-and-pictures/|title=Lorde Concert Set List And Pictures|publisher=CBSLocal. [[CBS Radio]]|date=6 March 2014|accessdate=3 December 2014}}</ref> In 2014, she performed the track at Silo Park, Auckland on 29 January 2014 as part of her make-up show for the 2014 [[Laneway Festival]],<ref>{{cite web|url=http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=11193929|title=Concert review: Lorde, Silo Park, Auckland|work=The New Zealand Herald|publisher=APN News & Media|date=30 January 2014|first=Chris|last=Schulz|accessdate=3 December 2014}}</ref> and at the Brazil edition of the [[Lollapalooza]].<ref>{{cite web|url=http://au.ibtimes.com/articles/546938/20140407/lorde-40000-fans-brazil.htm#.VH7t-WHQjOE|title=Lorde Conquers the Brazillian Stage, Rocks 40,000 fan at the Lollapalooza in Sao Paolo|publisher=[[International Business Times]]|first=Tarun|last= Mazumdar|date=7 April 2014|accessdate=3 December 2014}}</ref>

"Bravado" was well received by [[music journalism|music critics]]. Chris Schulz from ''[[The New Zealand Herald]]'' picked the song and "Million Dollar Bills" as the two best tracks from ''The Love Club EP''.<ref name = "NZHerald"/> Jason Lipshutz, writing for ''[[Billboard (magazine)|Billboard]]'', described the track as "delightfully spooky".<ref name="USshow"/> In a review published in ''[[The Dominion Post (Wellington)|The Dominion Post]]'', Tom Cardy compared the track's style to that of works by [[Florence Welch]] and [[Marina Diamandis]], labelling it a "sharp, refreshing and smart" song.<ref>{{cite news|title=Today's album|first=Tom|last=Cardy|date=12 April 2013|work=[[The Dominion Post (Wellington)|The Dominion Post]]|publisher=[[Fairfax Media]]|page=10}}</ref> The song peaked at number five on the [[Recorded Music NZ|New Zealand National Singles]] and number twenty-nine on the US [[Hot Rock Songs]] chart.<ref name="NZchart"/><ref name="UShardrock"/>

==Weekly charts==
{|class="wikitable sortable plainrowheaders"
|-
!Chart (2013)
!Peak<br>position
|-
!scope="row"|New Zealand Artist Singles ([[Recorded Music NZ]])<ref name = "NZchart">{{cite web|url=http://nztop40.co.nz/chart/nzsingles?chart=2152|title=Top 20 New Zealand Singles Chart|publisher=[[Recorded Music NZ]]|accessdate=2014-01-13}}</ref>
|align=center|5
|-
!scope="row"{{singlechart|Billboardrocksongs|29|artist=Lorde|accessdate=2014-01-12|refname="UShardrock"}}
|}

==Release history==
{{Wikipedia books|The Love Club EP|''The Love Club EP''}}
{|class="wikitable plainrowheaders"
|-
! scope="col"| Country
!scope="col"| Date
!scope="col"| Format
!scope="col"| Label
|-
!scope="row"|Austria<ref name = "itunesat">{{cite web|url=https://itunes.apple.com/at/album/bravado-single/id689042224|title=Bravado &ndash; Single von Lorde|language=German|publisher=iTunes Store (AT). Apple Inc|accessdate=2014-01-12}}</ref>
|rowspan="9"|6 September 2013
|rowspan="9"|[[Music download|Digital download]]
|rowspan="9"|[[Universal Music Group|Universal]]
|-
!scope="row"|Belgium<ref name = "itunesbe">{{cite web|url=https://itunes.apple.com/be/album/bravado-single/id689042224|title=Bravado – Single by Lorde|publisher=iTunes Store (BE). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Finland<ref name = "itunesfi">{{cite web|url=https://itunes.apple.com/fi/album/bravado-single/id689042224|title=Bravado – Single by Lorde|publisher=iTunes Store (FI). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Germany<ref name="itunesde"/>
|-
!scope="row"|India<ref name = "itunesin">{{cite web|url=https://itunes.apple.com/in/album/bravado-single/id689042224|title=Bravado – Single by Lorde|publisher=iTunes Store (IN). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Luxembourg<ref name = "ituneslu">{{cite web|url=https://itunes.apple.com/lu/album/bravado-single/id689042224|title=Bravado – Single by Lorde|publisher=iTunes Store (LU). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Netherlands<ref name = "itunesnl">{{cite web|url=https://itunes.apple.com/nl/album/bravado-single/id689042224|title=Bravado – Single by Lorde|language=Dutch|publisher=iTunes Store (NL). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Portugal<ref name = "itunespt">{{cite web|url=https://itunes.apple.com/pt/album/bravado-single/id689042224|title=Bravado – Single by Lorde|publisher=iTunes Store (PT). Apple Inc|accessdate=2014-01-12}}</ref>
|-
!scope="row"|Switzerland<ref name = "itunesch">{{cite web|url=https://itunes.apple.com/ch/album/bravado-single/id689042224|title=Bravado &ndash; Single von Lorde|language=German|publisher=iTunes Store (CH). Apple Inc|accessdate=2014-01-12}}</ref>
|}

==References==
{{reflist|2}}

==External links==
*{{MetroLyrics song|lorde|bravado}}
*{{YouTube|xV_xHBdLFSk|Lorde performing "Bravado" on Late Show with David Letterman}}

{{Lorde}}
{{Good article}}

[[Category:2013 songs]]
[[Category:Lorde songs]]
[[Category:Songs written by Joel Little]]
[[Category:Songs written by Lorde]]