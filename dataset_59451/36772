{{good article}}
{{for|the play|Robert Kemp (playwright)}}
{{Infobox television episode
| title        = The Asset
| series       = [[Agents of S.H.I.E.L.D.]]
| image        = [[File:Gravitonium.png|275px]]
| alt          = The fictional fluid-like metal Gravitonium floats in a glass container
| caption      = Gravitonium, as seen in the episode's end tag, with visual effects by FuseFX.
| season       = 1
| episode      = 3
| director     = [[Milan Cheylov]]
| writer       = {{plainlist|
*[[Jed Whedon]]
*[[Maurissa Tancharoen]]
}}
| producer     = {{Plainlist|
* Jed Whedon
* Maurissa Tancharoen
* [[Jeffrey Bell]]
}}
| photographer = Jeff Mygatt
| editor       = Joshua Charson
| airdate      = {{Start date|2013|10|08}}
| length       = 44 minutes
| guests       = 
* [[David Conrad]] as Ian Quinn
* [[Ian Hart]] as [[Graviton (comics)|Franklin Hall]]
| prev         = [[0-8-4]]
| next         = [[Eye Spy (Agents of S.H.I.E.L.D.)|Eye Spy]]
| episode_list = [[List of Agents of S.H.I.E.L.D. episodes|List of ''Agents of S.H.I.E.L.D.'' episodes]]
}}
"'''The Asset'''" is the third episode of the [[Agents of S.H.I.E.L.D. (season 1)|first season]] of the American television series ''[[Agents of S.H.I.E.L.D.]]'', based on the [[Marvel Comics]] organization [[S.H.I.E.L.D.]] (Strategic Homeland Intervention, Enforcement and Logistics Division), revolving around the character of [[Phil Coulson]] and his team of S.H.I.E.L.D. agents as they investigate the kidnapping of Dr. [[Graviton (comics)|Franklin Hall]]. It is set in the [[Marvel Cinematic Universe]] (MCU), sharing continuity with the [[List of Marvel Cinematic Universe films|films of the franchise]]. The episode was written by [[Jed Whedon]] and [[Maurissa Tancharoen]], and directed by [[Milan Cheylov]].

[[Clark Gregg]] reprises his role as Coulson from the film series, and is joined by series regulars [[Ming-Na Wen]], [[Brett Dalton]], [[Chloe Bennet]], [[Iain De Caestecker]], and [[Elizabeth Henstridge]]. The episode adapts the character of Hall from the comics, portrayed by guest star [[Ian Hart]], and introduces his gravitational technology based on the fictional element gravitonium, which was created by visual effects vendor FuseFX. The episode also introduces [[David Conrad]] as recurring villain Ian Quinn, with his own villainous musical theme; a variation of this theme is played on ethnic instruments to support the episode's Malta setting.

"The Asset" originally aired on [[American Broadcasting Company|ABC]] on October 8, 2013, and was watched by 12.01 million viewers within a week of its release according to [[Nielsen ratings|Nielsen Media Research]]. The episode received a mostly positive critical response, with the introduction of Hart as Hall oft praised, but the development of the main cast seen to be lacking.

==Plot==
Carrying a S.H.I.E.L.D. 'asset' between classified bases, a convoy is attacked by a seemingly invisible force, with the vehicles being hurled impossibly into the air. Soldiers break into the main transport and find the asset, Dr. [[Graviton (comics)|Franklin Hall]]. Aboard the Bus, the aerial headquarters for S.H.I.E.L.D. agent [[Phil Coulson]] and his team, Agent Grant Ward is struggling to supervise the training of the unmotivated civilian-recruit [[Daisy Johnson|Skye]]. Learning of Hall's kidnapping, Coulson and his team investigate the convoy wreckage. Agents Leo Fitz and Jemma Simmons discover a device, fueled by the rare element gravitonium, that alters gravity fields. The team tracks down the former owner of an [[excavator]] used by the soldiers in the attack, and trace the gold bars he was paid with back to Ian Quinn, a wealthy industrialist/philanthropist.

Quinn holds an announcement of a large deposit of gravitonium in his possession, in his [[Malta]] mansion where S.H.I.E.L.D. has no jurisdiction. Skye uses her [[hacktivist]] background to gain entry to the announcement, and disables Quinn's outer defenses. Coulson and Ward are able to sneak into Quinn's mansion, where they find Hall free and well, and working on a large gravitonium generator that would allow Quinn to control the world's gravity. Hall reveals that he was working with Quinn all along, the two having attended college together where they had first designed the generator. However, Hall realized that he couldn't allow anyone to gain control of the generator's power, and so plans to let it destroy itself and Quinn's mansion. As this would kill the thousands of innocent people on the island, Coulson lets Hall fall into the gravitonium which catalyzes an anti-reaction to turn off the machine, apparently killing Hall in the process. 

Quinn escapes custody while S.H.I.E.L.D. takes possession of the gravitonium. Skye finds the motivation to commit to her training, and Agent Melinda May, who had previously avoided combat operations since retiring, decides she would rather be fully committed than watching helplessly from the Bus. In an end tag, Hall is still alive within the gravitonium, which is sealed in an unmarked vault by S.H.I.E.L.D.

==Production==
===Development and casting===
{{further|List of Agents of S.H.I.E.L.D. characters}}
In September 2013, Marvel revealed that the third episode would be titled "The Asset", and would be written by executive producers [[Jed Whedon]] and [[Maurissa Tancharoen]], with [[Milan Cheylov]] directing. Main cast members [[Clark Gregg]], [[Ming-Na Wen]], [[Brett Dalton]], [[Chloe Bennet]], [[Iain De Caestecker]], and [[Elizabeth Henstridge]] star as [[Phil Coulson]], Melinda May, Grant Ward, [[Daisy Johnson|Skye]], Leo Fitz, and Jemma Simmons, respectively. The guest cast for the episode includes David Conrad as Ian Quinn and Ian Hart as [[Graviton (comics)|Franklin Hall]].<ref name="Declassifying" />

Talking about bringing the character of Franklin Hall from the comics to the series, executive producer [[Jeffrey Bell]] said that the writers had looked through the comics for a character to fill the episode's role, and felt that Hall had an interesting history. Subsequently, changes were made to fit the comics character into the existing role for the series,<ref name="Bell" /> and he ultimately appears in an [[origin story]]-type role rather than as the fully formed supervillain from the comics. As a hint at his potential to become the villain "Graviton" in the future, the series changed his original scientific interests from [[teleportation|matter transportation]] to the study of "gravitonium", a substance created for the episode. The episode's ending, which sees Hall fall into some gravitonium, indicates that this is how he gains his Graviton abilities in the series.<ref name="GravitonChanges" />

===Visual effects===
The visual effects for the episode were completed by FuseFX. For effects shots involving the substance gravitonium, visual effects supervisor Kevin Lingenfelser explained that they were divided into two categories: shots where the gravitonium is "‘neutral’ or ‘ball like’", which were animated to make the element act like a fluid; and more aggressive shots where the gravitonium envelopes Hall, which mimicked the effects of gravity while Hall was being sucked in, with "more sentient and deliberate motion" animated around that.<ref name="effects" /> For the opening sequence, the effects team completely replaced the S.H.I.E.L.D. vehicles with computer generated models so as to depict them defying gravity and being destroyed.<ref name="vfx" />

===Music===
For "The Asset", composer [[Bear McCreary]] wrote a theme for Ian Quinn. A "bouncy and energetic" version of this was performed on a [[bouzouki]] by guitarist Ed Trybek to evoke the music of Malta, while a simplified orchestral version is used as the primary Quinn theme. McCreary stated that he does not "use it a lot, but it counts when I do. When he catches Skye in the hallways and steps forward menacingly, the low strings and woodwinds sneak in on this theme and underscore how dangerous he is."<ref name="music" />

==Release==
===Broadcast===
"The Asset" was first aired in the United States on ABC on October 8, 2013.<ref name="ratings" /> It was aired alongside the US broadcast in Canada on [[CTV Television Network|CTV]],<ref name="canada" /> while it was first aired in the United Kingdom on [[Channel 4]] on October 11, 2013.<ref name="uk" /> It premiered on the [[Seven Network]] in Australia on October 9, 2013.<ref name="australia"/>

===Home media===
The episode, along with the rest of ''Agents of S.H.I.E.L.D.''{{'}}s first season, was released on [[Blu-ray]] and [[DVD]] on September 9, 2014. Bonus features include behind-the-scenes featurettes, audio commentary, deleted scenes, and a blooper reel.<ref name="HomeMedia" /> On November 20, 2014, the episode became available for streaming on [[Netflix]].<ref name="Netflix" />

==Reception==
===Ratings===
In the United States the episode received a 2.9/9 percent share among adults between the ages of 18 and 49, meaning that it was seen by 2.9 percent of all households, and 9 percent of all of those watching television at the time of the broadcast. It was watched by 7.87 million viewers.<ref name="ratings" /> The Canadian broadcast gained 1.91 million viewers, the second highest for that day and the seventh highest for the week.<ref name="canada" /> The [[United Kingdom]] premiere had 2.37 million viewers<ref name="uk" /> and in [[Australia]], the premiere had 1.9 million viewers, including 0.9 million [[Time shifting|timeshifted]] viewers.<ref name="australia" /> Within a week of its release, the episode was watched by 12.01 million U.S. viewers,<ref name="DVR" /> above the season average of 8.31.<ref name="AvgRatings" />

=== Critical response ===
Eric Goldman of [[IGN]] scored the episode 7.7 out of 10, praising the plot and the introduction of Hall/Graviton, but criticizing the amount of humor and MCU references.<ref name="IGN" /> David Sims of ''[[The A.V. Club]]'' scored the episode a 'B', calling it "the first episode to show some potential for originality around the corner". He praised the character development, specifically for Coulson and Skye, and the introduction of Hall/Graviton, but criticized Quinn as "barely a step above a generic ''[[Miami Vice]]'' villain and whose motives would be totally uninteresting if they were ever made clear to us". He also found Dalton to be "the latest in a line of dull [[Joss Whedon|Whedon]] hunks with just a glimmer of personality."<ref name="AVClub" /> ''[[The Guardian]]''{{'}}s Graeme Virtue felt that "If ''Agents of S.H.I.E.L.D.'' hasn't been levitating your boat so far, this breezy episode probably didn't do that much to change your mind. If you're not in the mood, the endless quipping can seem exhausting, but at least there was some incremental character development." He had especial praise for the introduction of Hall/Graviton, and felt that Hart's performance as the character topped [[Samuel L. Jackson]]'s cameo from the [[0-8-4|previous episode]].<ref name="Guardian" />

Dan Casey at [[Nerdist Industries|Nerdist]] found the adage "third time is the charm" to apply to the episode, feeling that "Rather than trying to figure out where it fits within Marvel’s grander on-screen universe, ''S.H.I.E.L.D.'' is focusing its energy on developing the eponymous agents and giving us more backstory", and though he felt that "The show still needs to figure out its balance of seriousness and humor", he concluded that the "show just keeps getting better."<ref name="Nerdist" /> James Hunt at ''Den of Geek'' felt that "There's a distinct feeling of treading water", finding the episodic plot to be "fairly by-the-numbers for a show that's supposed to be about the fantastic", and he was disappointed in "Graviton's non-appearance appearance", referring to the lack of Hall's comics' alter-ego.<ref name="DenofGeek" /> Marc Bernardin of ''[[The Hollywood Reporter]]'' praised the episode's opening sequence, but criticized the character Skye and the focus the episode put on her rather than Coulson. He also spoke unfavorably of Quinn, noting that Hall would likely return as a villain himself in the future, but "until then, ''S.H.I.E.L.D.'' needs to up its adversarial game".<ref name="Hollywood" /> [[Jim Steranko]], known for his work on ''[[Nick Fury|Nick Fury, Agent of S.H.I.E.L.D.]]'', felt "the plot’s twists and turns clicked, even though it’s kind of embarrassing when the commercials are more engrossing than the show."<ref name="Hollywood" />

==References==
{{reflist|30em|refs=

<!-- CASTING -->

<ref name="Declassifying">{{cite news |url=http://marvel.com/news/tv/2013/9/19/21197/declassifying_marvels_agents_of_shield_ep_103_the_asset |title=Declassifying Marvel's Agents of S.H.I.E.L.D. Ep. 103: The Asset |publisher=[[Marvel Comics|Marvel.com]] |date=September 23, 2013 |accessdate=September 3, 2014 |archivedate=September 3, 2014 |archiveurl=http://www.webcitation.org/6SJEC0W2y |deadurl=no}}</ref> 

<ref name="Bell">{{cite news |url=http://www.comicbookresources.com/?page=article&id=48325 |last=Ching |first=Albert |title="Agents of S.H.I.E.L.D." EP Talks Ratings, Nick Fury Cameo |publisher=Comic Book Resources |date=October 7, 2013 |accessdate=September 2, 2014 |archivedate=September 2, 2014 |archiveurl=http://www.webcitation.org/6SHSkrnIn |deadurl=no}}</ref>

<ref name="GravitonChanges">{{cite news |url=http://screenrant.com/agents-of-shield-supevillains-compared-to-comic-books/?view=all |title=Agents of S.H.I.E.L.D.: How The Show’s Supervillains Compare To The Comics |last=Scheidler |first=Bryan |publisher=ScreenRant |date=April 10, 2016 |accessdate=January 14, 2017 |archivedate=January 14, 2017 |archiveurl=http://www.webcitation.org/6nUh0sU9p |deadurl=no}}</ref> 

<!-- VISUAL EFFECTS -->

<ref name="effects">{{cite news |url=http://www.fxguide.com/featured/vfx-in-tv-a-to-v-agents-of-s-h-i-e-l-d-to-vikings-and-more/ |last=Failes |first=Ian |title=VFX in TV, A to V: Agents of S.H.I.E.L.D to Vikings & More |date=July 8, 2014 |accessdate=September 3, 2014 |archivedate=September 3, 2014 |archiveurl=http://www.webcitation.org/6SJJKxWJd |deadurl=no}}</ref>

<ref name="vfx">{{cite news |url=http://www.markkolpack.com/Visual_Effects_Reels.html |last=Kolpack|first=Mark|title=Visual Effects Reels|accessdate=September 14, 2014}}</ref>

<!-- MUSIC -->

<ref name="music">{{cite news |url=http://www.bearmccreary.com/#blog/blog/agents-of-s-h-i-e-l-d-the-asset/ |last=McCreary |first=Bear |title=Agents of S.H.I.E.L.D. – The Asset |date=October 8, 2013 |accessdate=September 3, 2014 |archivedate=September 3, 2014 |archiveurl=http://www.webcitation.org/6SJI47dXQ |deadurl=no}}</ref>

<!-- RELEASE -->

<ref name="HomeMedia">{{cite web|url=http://www.ign.com/articles/2014/05/30/marvels-agents-of-shield-blu-ray-and-dvd-details|title=Marvel's Agents of SHIELD Blu-ray And DVD Details|last=Fowler|first=Matt|publisher=[[IGN]]|date=May 30, 2014|accessdate=May 30, 2014|archiveurl=http://www.webcitation.org/6PxznP2hp|archivedate=May 30, 2014|deadurl=no}}</ref>

<ref name="Netflix">{{cite web|url=http://decider.com/2014/11/11/marvels-agents-of-shield-netflix/|title=Exclusive: ‘Marvel’s Agents Of S.H.I.E.L.D.’ Is Coming To Netflix November 20!|last=O'Keefe|first=Meghan|publisher=Decider|date=November 11, 2014|accessdate=November 16, 2014|archiveurl=http://www.webcitation.org/6U6shPELO|archivedate=November 16, 2014|deadurl=no}}</ref>

<!-- RATINGS -->

<ref name="ratings">{{cite web|url=http://tvbythenumbers.zap2it.com/2013/10/09/tuesday-final-ratings-marvels-agents-of-s-h-i-e-l-d-ncis-chicago-fire-the-goldbergs-adjusted-down/207852|title=Tuesday Final Ratings: 'Marvel's Agents of S.H.I.E.L.D.', 'NCIS' & 'The Voice' Adjusted Up; 'Chicago Fire', 'The Goldbergs' Adjusted Down|last=Kondolojy |first=Amanda|work=TV by the Numbers|date=October 9, 2013|accessdate=September 4, 2014|archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SObmbKqF |deadurl=no}}</ref>

<ref name="canada">{{cite web|url=https://www.bbm.ca/_documents/top_30_tv_programs_english/2013-14/2013-14_10_07_TV_ME_NationalTop30.pdf|title=Top Programs – Total Canada (English) October 7 - October 13, 2013|work=bbm.ca|accessdate=September 4, 2014|archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKa3Xj3l |deadurl=no}}</ref>

<ref name="uk">{{cite web |url=http://www.barb.co.uk/whats-new/weekly-top-30 |title=Top 30 Programmes |publisher=BARB |accessdate=September 4, 2013|archivedate=September 1, 2014 |archiveurl=http://www.webcitation.org/6SG3Fv74H |deadurl=no}}</ref>

<ref name="australia">{{cite web|url=http://www.tvtonight.com.au/2013/10/timeshifted-wednesday-9-october-2013.html|title=Timeshifted: Wednesday 9 October 2013|work=tvtonight.com.au|date=October 9, 2013|accessdate=September 4, 2014|archivedate= |archiveurl= |deadurl=no}}</ref>

<ref name="DVR">{{cite web|last=Kondolojy|first=Amanda|title=Updated Live+7 DVR Ratings: 'The Big Bang Theory' Tops Adults 18-49 Ratings Increase, 'Elementary' Earns Biggest Percentage Gain,'The Blacklist' Grows Most in Total Viewers in Week 3|url=http://tvbythenumbers.zap2it.com/2013/10/27/updated-live7-dvr-ratings-the-big-bang-theory-tops-adults-18-49-ratings-increase-elementary-earns-biggest-percentage-increase-in-week-3/211637/|work=[[TV by the Numbers]]|publisher=[[Zap2it]]|date=October 27, 2013|accessdate=October 27, 2013}}</ref>

<ref name="AvgRatings">{{cite web|url=http://www.deadline.com/2014/05/tv-season-series-rankings-2013-full-list-2|title=Full 2013–2014 TV Season Series Rankings|publisher=[[Deadline.com]]|date=May 22, 2014|accessdate=May 25, 2014|archiveurl=http://www.webcitation.org/6PpJWyOpl|archivedate=May 25, 2014|deadurl=no}}</ref>

<!-- REVEIEWS -->

<ref name="IGN">{{cite news |url=http://ign.com/articles/2013/10/09/marvels-agents-of-shield-the-asset-review |last=Goldman |first=Eric |title=Marvel's Agents of SHIELD: "The Asset" Review |publisher=IGN |date=October 8, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKb5wd5s |deadurl=no}}</ref>

<ref name="AVClub">{{cite news |url=http://www.avclub.com/tvclub/marvels-agents-of-shield-the-asset-103568 |last=Sims |first=David |title=Marvel's Agents of S.H.I.E.L.D.: "The Asset" |work=The A.V. Club |date=October 8, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKbdWNZe |deadurl=no}}</ref>

<ref name="DenofGeek">{{cite news |url=http://www.denofgeek.com/tv/marvels-agents-of-shield/27635/marvels-agents-of-shield-episode-3-review-the-asset |last=Hunt |first=James |title=Marvel's Agents Of S.H.I.E.L.D. episode 3 review: The Asset |publisher=Den of Geek |date=October 10, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKc6DdN9 |deadurl=no}}</ref>

<ref name="Hollywood">{{cite news |url=http://www.hollywoodreporter.com/live-feed/agents-shield-recap-5-things-645587 |last=Bernardin |first=Marc |title='Agents of SHIELD' Recap: 5 Things We Learned From 'The Asset' |work=The Hollywood Reporter |date=October 9, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKcpdnj7 |deadurl=no}}</ref>

<ref name="Guardian">{{cite news |url=https://www.theguardian.com/tv-and-radio/tvandradioblog/2013/oct/11/marvels-agents-shield-recap-the-asset |last=Virtue |first=Graeme |title=Agents of SHIELD recap: season one, episode three – The Asset |work=The Guardian |date=October 11, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKeDH5rl |deadurl=no}}</ref>

<ref name="Nerdist">{{cite news |url=http://www.nerdist.com/2013/10/marvels-agents-of-s-h-i-e-l-d-recap-the-asset/ |last=Casey |first=Dan |title=Marvel's Agents of S.H.I.E.L.D. Recap: The Asset |publisher=Nerdist |date=October 9, 2013 |accessdate=September 4, 2014 |archivedate=September 4, 2014 |archiveurl=http://www.webcitation.org/6SKfWD5eX |deadurl=no}}</ref>

}}

==External links==
*[http://abc.go.com/shows/marvels-agents-of-shield/episode-guide/season-01/103-the-asset "The Asset"] at [[American Broadcasting Company|ABC]]
*{{IMDb episode|3130186|The Asset}}
*{{Tv.com episode|marvels-agents-of-shield/the-asset-2891926|The Asset}}

{{Agents of S.H.I.E.L.D. episodes}}
{{portal bar|Marvel Cinematic Universe}}
{{DEFAULTSORT:Asset, The}}

[[Category:Agents of S.H.I.E.L.D. (season 1) episodes]]
[[Category:2013 American television episodes]]