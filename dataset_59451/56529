'''DAREnet''' (2003 - 2007) stands for '''Digital Academic Repositories''' and is an initiative by the Dutch organisation Surf. The DARE programme is a joint initiative by the [[List of universities in the Netherlands|Dutch universities]] and the [[National Library of the Netherlands]], the [[Royal Netherlands Academy of Arts and Sciences]] (KNAW) and the [[Nederlandse Organisatie voor Wetenschappelijk Onderzoek]] (NWO) with the aim to store the results of all Dutch research in a network of so-called repositories, thus facilitating access to them. DAREnet is now integrated into the portal www.narcis.nl.

== "Cream of Science" initiative ==
In May 2005 DARE started the ''Cream of Science'' [[Open access (publishing)|open access]] project with an online library of 25.000 publications from 200 prominent Dutch scientists. Because some publishers have more stringent copyright restrictions (notably [[Reed Elsevier]]) than others, free online access is limited to approximately 60% of the library content. The main disadvantage of the database is that free online accessibility is not listed for each publication and will only become apparent when a click-through is attempted. In the final stage of the DARE programme an ambitious project called ''HunDAREd thousand'' was started. The aim was to upload 100,000 full-text objects into DAREnet within a year. The focus of this project was on the doctoral theses of young scholars. The aim of this "Promise of Science" project was to set up a national doctoral e-thesis gateway and populate it with 10,000 full-text e-theses before the end of 2006. The total annual production of doctoral theses in the Netherlands is around 2,500. Promise of Science was launched in September 2006 and in January 2007 DAREnet also achieved DARE’s final goal, the HunDAREd thousand project. At the time DAREnet contained a total of 103,429 objects.

== Organisational structure ==
The development of NARCIS started as a cooperation project of KNAW Research Information, NWO, VSNU and METIS, as part of the development of services within the DARE programme of SURFfoundation. This project resulted in the NARCIS portal, in which the DAREnet service was incorporated in January 2007. In 2011 NARCIS became a service of the institute Data Archiving and Networked Services (DANS), an institute of KNAW and NWO. 

== Content ==
NARCIS provides access to scientific information, including (open access) publications from the repositories of all the Dutch universities, KNAW, NWO and a number of research institutes, datasets from some data archives as well as descriptions of research projects, researchers and research institutes.

== External links ==
* [http://www.narcis.nl/ NARCIS]
* [http://www.dans.knaw.nl/ Data Archiving and Networked Services (DANS)]
* [http://www.surf.nl/ SURF]
* [http://www.nwo.nl/ NWO]
* [http://www.knaw.nl/ KNAW]

[[Category:Bibliographic databases and indexes]]
[[Category:Open access projects]]

[[nl:Digital Academic Repository]]