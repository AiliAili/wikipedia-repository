{{Use British English|date=October 2013}}
{{Use dmy dates|date=July 2013}}
{{Good article}}
{{Infobox Governor
|honorific-prefix = 
|name = Sir Gilbert Thomas Carter
|honorific-suffix = {{postnominals|country=GBR|size=100%|KCMG}}
|image = Sir Gilbert Thomas Gilbert-Carter by Alexander Bassano 1893 NPG x6206 detail.jpg <!-- http://www.npg.org.uk/collections/search/use-this-image.php?mkey=mw167156 -->
|alt =
|caption = Portrait of Sir Gilbert by [[Alexander Bassano]], 1893
|birth_date = {{Birth date|1848|01|14|df=yes}}
|birth_place = London, United Kingdom
|death_date = {{Death date and age|1927|01|18|1848|01|14|df=yes}}
|death_place = [[Barbados]]
<!--Military service-->
|spouse= 
* Susan Laura Hocker 1874–1895
* Gertrude Codman Parker 1903–1927
|allegiance = {{Flag|United Kingdom}}
|branch = {{navy|United Kingdom}}
|serviceyears = 1864–1875
|rank = Assistant Paymaster<ref name="lg-20aug1875"/>
|unit =
|commands =
|battles = [[Anglo-Ashanti Wars#Third Anglo-Ashanti War|Third Anglo-Ashanto War]]
|awards = [[Knight Commander of the Order of St Michael and St George]]<br />[[Companion of the Order of St Michael and St George]]
|military_blank1 = Joined Navy<br />(Assistant Clerk)
|military_data1 = 14 December 1864<ref name="ServiceRecord1"/>
|military_blank2 = Promoted to Clerk
|military_data2 = 4 July 1866<ref name="ServiceRecord1"/>
|military_blank3 = Placed on Navy List<br />(Promoted to Assistant Paymaster)
|military_data3 = 1 December 1869<ref name="ServiceRecord1"/>
|military_blank4 = Placed on Retired List
|military_data4 = 16 August 1875<ref name="ServiceRecord1"/>
}}
[[File:Gilbert Thomas Carter and wife in 1916.jpg|thumb|Gilbert Thomas Carter, Gertrude Codman Parker and John Codman Carter, 1916]]
'''Sir Gilbert Thomas Carter''' ('''Sir Thomas Gilbert-Carter''') {{postnominals|country=GBR|KCMG}} (14 January 1848&nbsp;– 18 January 1927)<ref name="TimesObit"/> was an administrative officer in the [[Royal Navy]] and a colonial official for the [[British Empire]].

Starting as a Collector of [[HM Customs and Excise|Customs]] for the [[Gold Coast (region)|Gold Coast]], he then became a [[Treasurer#Government|Treasurer]] of the Gold Coast and [[the Gambia]]. Moving on to colonial administration, he started as the [[Administrator of the Government|Administrator]] for the Gambia, where he dealt with the aggression of the native king of Gambia.

His next post was as [[Governor#British Empire and Commonwealth of Nations|Governor]] for the [[Lagos Colony]] where he negotiated treaties with the local chiefs which protected Christian missionaries and ending human sacrifies. He later served as the Governor for [[The Bahamas]] and [[Barbados]] and finally as the Governor for [[Trinidad and Tobago]].

==Early life and Naval career==
Carter was born in Topsham, Devon in 1848.<ref>"Sir Gilbert Thomas Gilbert-Carter (1848–1927)" by Bob Maddocks in ''Cameo'', Journal of the [[West Africa Study Circle]], Vol. 13, No. 2, June 2012, p. 117.</ref> He was the only son of [[Commander#Royal Navy|Commander]] Thomas Gilbert Carter ([[Royal Navy|R.N.]]).<ref name="TimesObit"/> He was educated at the [[Royal Hospital School]] in [[Greenwich]].<ref name="TimesObit"/> Carter joined the Royal Navy in 1864, serving as an Assistant Clerk on the {{HMS|Frederick William||6}}, being transferred in 1866 to {{HMS|Malacca|1853|6}}.<ref name="ServiceRecord1"/> On 5 July 1866, Carter was promoted to Clerk, while still serving on HMS ''Malacca''.<ref name="ServiceRecord1"/> Between 1867 and 1869, he served on a variety of ships as a clerk, until 1 December 1869 (while serving on {{HMS|Pembroke|1812|6}}), when he was promoted to Assistant Paymaster (being added to the [[Navy List]]).<ref name="navy-list-20dec1880"/>

Following a posting to {{HMS|Royal Adelaide|1828|6}} for the first 9 months of 1870, Carter's final posting was to the Colonial steamer ''Sherbro'' from August 1870.<ref name="ServiceRecord1"/> During his time on the ''Sherbro'', he was involved with the [[Anglo-Ashanti Wars#Third Anglo-Ashanti War|Third Anglo–Ashanto War]] on the [[Gold Coast (region)|Gold Coast]].<ref name="TimesObit"/> When [[Elmina]] was sold to the British by the Dutch Government, he was a [[Commissioner#British and Commonwealth overseas possessions|commissioner]], responsible for valuing the stores and ordnance left behind by the Dutch.<ref name="TimesObit"/> He married Susan Laura Hocker, daughter of Lieutenant-Colonel Edward Hocker, in 1874<ref name="Ancestry-Marriage"/> (later having 3 sons and 2 daughters&nbsp;— his second son [[Humphrey Gilbert-Carter|Humphrey]] 1884–1969 was the first Director of the [[Cambridge University Botanic Garden]]). Lady Carter died in 1895{{sfn|ILN|1895}}<ref name="TimesObit"/><ref name="np-apr2004"/> He retired from the Navy on 21 July 1875.<ref name="ServiceRecord1"/>

==Leeward Islands, the Gold Coast and the Gambia==
Carter became the private secretary to Sir George Berkeley, Governor of the [[Leeward Islands]], in 1875.<ref name="TimesObit"/> In August 1879, he was appointed Collector of [[HM Customs and Excise|Customs]] and [[Treasurer#Government|Treasurer]] of the Gold Coast, an appointment he kept until 1882.<ref name="TimesObit"/><ref name="lg-11jul1879"/> From 1882 until December 1888, Carter administered the Settlement on [[the Gambia]]<ref name="lg-04dec1888"/> as a Treasurer and Postmaster.<ref name="TimesObit"/> From 1886, he was acting [[List of colonial heads of the Gambia|Administrator]] of the Colony of the Gambia, and on 1 December 1888 he was appointed Administrator on that colony's separation from Sierra Leone.<ref name="TimesObit"/><ref name="lg-04dec1888"/> While working in the Gambia, he was appointed [[Companion of the Order of St Michael and St George]] (CMG) on 1 January 1890.<ref name="lg-01jan1890"/>

In 1891, the native King of Gambia had been organising abusive acts towards the British colonists.<ref name="nyt-23apr1891"/> Carter (from his official residence in [[Banjul|Bathurst]]) sent an envoy with a message that if the abuses continued, "he might expect a visit of a disciplinary nature from the marine forces of the Queen of England."<ref name="nyt-23apr1891"/> The King sent the envoy back mutilated, with a message: "This is the King's answer."<ref name="nyt-23apr1891"/> In response, Carter sent three British [[Gunboat#Steam era|gunboats]] to avenge the outrage upon the envoy.<ref name="nyt-23apr1891"/>

==Lagos (Nigeria)==
Carter was appointed Governor and Commander-in-Chief of the [[History of Lagos#Colonial-era|Colony of Lagos]] on 3 February 1891.<ref name="lg-03feb1891"/> Carter ordered an attack on the [[Ijebu]] "in the interest of civilization" in 1892.<ref name="falola-99"/> Afterwards, he continued to justify this attack as a war to end slavery and promote civilization.<ref name="falola-99"/>

Carter travelled to various parts of [[Yoruba people|Yorubaland]], accompanied by soldiers, in an attempt to demonstrate the might of the British.<ref name="falola-99"/> Carter was not well received at [[Oyo, Nigeria|Oyo]], and the [[Egba people|Egba]] chiefs advised him not to interfere with slavery, while the [[Ibadan]] chiefs said they were afraid that their slaves would "assert their freedom by running to the [[Resident (title)|Resident]]"&nbsp;– and they refused to sign a treaty with Carter that would impose a Resident on the city.<ref name="falola-99"/>

However, in January 1893 the Egba chiefs signed a Treaty of Independence with the British Government.<ref name="egbayewa-ordinances"/> It was agreed that freedom of trade between the Egba Nation and Lagos was to be guaranteed by the British Government,<ref name="appleton-1894"/> in return for which no road would be closed without the approval of the Governor.<ref name="egbayewa-ordinances"/> They further agreed that complete protection and "every assistance and encouragement" would be afforded to all Christian ministers.<ref name="egbayewa-ordinances"/> The Crown agreed that "no annexation on any portion of Egba Nation shall be made by her Majesty's Government without the consent of the lawful authorities of the nation, no aggressive action shall be taken against the said nation and its independence shall be fully recognized."<ref name="egbayewa-ordinances"/> The Egba chiefs further promised to abolish human sacrifices.<ref name="egbayewa-ordinances"/>

He was promoted [[Knight Commander of the Order of St Michael and St George]] (KCMG) on 3 June 1893,<ref name="lg-03jun1893"/> "in recognition of his services in conducting a mission to the Yoruba country which resulted in the negotiation of important treaties and brought to an end a long-standing war."<ref name="TimesObit"/>

Carter was given [[Ife]] works of art in 1896 by the recently crowned King of Ife, Adelekan, in the hopes that a decision in his favour would be made about the resettlement of [[Modakeke]] residents outside the city. These works (including three known as the ''Ife marbles''), were sent by Carter to Europe.<ref name="ab-sep1985"/>

While serving in Lagos, Lady Carter, his first wife, died on January 13th 1895, shortly after arriving in Lagos.{{sfn|ILN|1895}}

==Later life, retirement and death==
[[File:Barbados Ship 1905 issue.JPG|right|thumb|The 1905 "Olive Blossom" stamp of Barbados, designed by Gilbert's second wife, Gertrude.]]
Carter was transferred to Bahamas as Governor and Commander-in-Chief in 1898,<ref name="lg-21dec1897"/> and after a temporary transfer to Trinidad,<ref name="TimesObit"/> in July 1904 he was transferred to the Barbados as Governor and Commander-in-Chief.<ref name="lg-29jul1904"/>

Carter met an American artist (see Stamp) from Boston, [[Gertrude Codman Parker]] (6 February 1875 - 12th November 1953, Boston{{sfn|Stamp Art|2014}}<ref name="burke-1975"/>), in the spring of 1903 when she was travelling in the Bahamas with her parents, [[Francis Vose Parker]] and his wife.<ref name="nyt-26apr1903"/> She became his second wife on 25 August 1903, when they were married in the Church of the Advent in [[Boston]] and had a son, John Codman Carter.{{sfn|Stamp Art|2014}}<ref name="nyt-26apr1903"/>

Carter continued working in senior colonial positions&nbsp;— being appointed the [[Governor of Barbados#Governors of Barbados (1885–1966)|Governor]] and Commander-in-Chief of the Island of [[Barbados]] and its Dependencies<ref name="lg-29jul1904"/> in 1904, and as [[Administrator of the Government]] of the Colony of [[Trinidad and Tobago]], and its Dependencies in the absence of the Governor from 1907 until 1910,<ref name="lg-03may1907"/> whereupon he retired.<ref name="TimesObit"/>

In 1919, he changed his surname to ''Gilbert-Carter''.<ref name="TimesObit"/> In the early 1920s, he moved back to Barbados, and lived at [[Ilaro Court]], which had been designed and built by Lady Gilbert Carter. He died there on 18 January 1927.<ref name="TimesObit"/> When his will was [[probate]]d on 22 March of that year, the total value of his effects was £6859 9s 11d.<ref name="Probate"/>

==References==
{{reflist|2|refs=
<ref name="navy-list-20dec1880">{{cite book
 |author=The [[Admiralty]]
 |title=The Navy List, corrected to the 20th&nbsp;December 1880
 |publisher=[[Eyre & Spottiswoode]]
 |location=London, England
 |year=1881
 |page=417
 |url=https://books.google.com/books?id=x-wNAAAAQAAJ&cd=2&dq=%22Gilbert+Thomas+Carter%22+%22The+Navy+List%22&q=%22Gilbert+Thomas+Carter%22}}</ref>
<ref name="ab-sep1985">{{cite journal
 |last=Preston Blier
 |first=Suzanne
 |date=September 1985
 |title=Kings, Crowns, and Rights of Succession: Obalufon Arts at Ife and Other Yoruba Centers
 |journal=The Art Bulletin
 |publisher=[[College Art Association]]
 |location=New York, USA
 |volume=67
 |issue=3
 |jstor=3050958
 |pages=383–401
 |doi=10.2307/3050958}}</ref>
<ref name="falola-99">{{cite book
 |last=Falola
 |first=Toyin
 |title=Slavery and colonial rule in Africa
 |editor=Miers, Suzanne |editor2=Klein, Martin A
 |publisher=[[Routledge]]
 |location=Abingdon, Oxfordshire, England
 |year=1999
 |series=Studies in slave and post-slave societies and cultures
 |volume=8
 |pages=234–235
 |chapter=The End of Slavery among the Yoruba
 |isbn=978-0-7146-4884-2
 |url=https://books.google.com/books?id=McuFp5az4o8C&pg=PA234&dq=%22Gilbert+Thomas+Carter%22&cd=1#v=onepage&q=%22Gilbert%20Thomas%20Carter%22
 |accessdate=15 December 2009}}</ref>
<ref name="appleton-1894">{{cite book
 |title=Appletons' Annual Cyclopaedia and Register of Important Events
 |publisher=[[D. Appleton & Company]]
 |location=New York, USA
 |year=1894
 |volume=18
 |page=367
 |url=https://books.google.com/books?id=WEgoAAAAMAAJ&cd=19&dq=%22Gilbert+Thomas+Carter%22&q=
 |accessdate=15 December 2009}}</ref>
<ref name="egbayewa-ordinances">{{cite book
 |url=https://books.google.com/books?id=EWUSAAAAYAAJ&pg=PA1082&dq=egba+gilbert+thomas+carter+1893&hl=en&sa=X&ei=3CHhT4DCJcqe2gWAronXCw&ved=0CDoQ6AEwAQ#v=onepage&q=egba%20gilbert%20thomas%20carter%201893&f=false
 |title=Ordinances, and orders and rules thereunder, in force in the colony of Lagos, on April 30th, 1901
 |volume=2
 |page=1082
 |publisher=Stevens and Sons Limited
 |location=London, England
 |year=1902
 |accessdate=20 June 2012}}</ref>
<ref name="nyt-23apr1891">{{cite news
 |url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9905E0DC163AE533A25750C2A9629C94609ED7CF
 |title=Imprudent King of Gambia.; British Gunboats to Avenge an Outrage on an Envoy
 |date=23 April 1891
 |work=[[The New York Times]]
 |publisher=[[The New York Times Company]]
 |page=1
 |accessdate=15 December 2009
 |location=New York, USA
| format=PDF}}</ref>
<!-- please note that prior to 1889, the London Gazette was not published by HMSO -->
<ref name="lg-20aug1875">{{London Gazette
 |issue=24238
 |startpage=4187
 |date=20 August 1875
 |accessdate=15 December 2009}}</ref>
<ref name="lg-11jul1879">{{London Gazette
 |issue=24742
 |startpage=4404
 |date=11 July 1879
 |accessdate=15 December 2009
}}</ref>
<ref name="lg-04dec1888">{{London Gazette
 |issue=25880
 |startpage=6940
 |date=4 December 1888
 |accessdate=15 December 2009
}}</ref>
<ref name="lg-01jan1890">{{London Gazette
 |issue=26008
 |startpage=2
 |date=1 January 1890
 |accessdate=15 December 2009
}}</ref>
<ref name="lg-03feb1891">{{London Gazette
 |issue=26131
 |startpage=615
 |date=3 February 1891
 |accessdate=15 December 2009
}}</ref>
<ref name="lg-03jun1893">{{London Gazette
 |issue=26409
 |startpage=3253
 |date=3 June 1893
 |accessdate=15 December 2009}}</ref>
<ref name="lg-21dec1897">{{London Gazette
 |issue=26921
 |startpage=7646
 |date=21 December 1897
 |accessdate=15 December 2009}}</ref>
<ref name="lg-29jul1904">{{London Gazette
 |issue=27700
 |startpage=4908
 |date=29 July 1904
 |accessdate=15 December 2009}}</ref>
<ref name="lg-03may1907">{{London Gazette
 |issue=28018
 |startpage=2991
 |date=3 May 1907
 |accessdate=15 December 2009}}</ref>
<ref name="burke-1975">{{cite book
 |last=Burke
 |first=Arthur Meredyth
 |title=The prominent families of the United States of America
 |publisher=Heraldic Publishing Co
 |location=New York, USA
 |year=1975
 |page=406
 |url=https://books.google.com/books?id=MUxlAAAAMAAJ&q=%22Gilbert+Thomas+Carter%22&dq=%22Gilbert+Thomas+Carter%22&cd=26
 |accessdate=15 December 2009}}</ref>
<ref name="nyt-26apr1903">{{cite news
 |url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9406EED61739E433A25755C2A96E9C946297D6CF
 |title=Marries an American Girl
 |date=26 April 1903
 |work=Special to [[The New York Times]]
 |publisher=[[The New York Times Company]]
 |page=7
| accessdate=15 December 2009
 |location=New York, USA
| format=PDF}}</ref>
<ref name="TimesObit">{{Cite newspaper The Times
 |articlename=Sir G. T. Gilbert-Carter
 |section=Obituaries
 |day_of_week=Wednesday
 |date=19 January 1927
 |page_number=9
 |issue=44483
 |column=B
}}</ref>
<ref name="np-apr2004">{{cite journal
 |last=Raven
 |first=John A.
 |date=April 2004
 |title=Building Botany in Cambridge
 |journal=[[New Phytologist]]|publisher=[[Wiley-Blackwell|Blackwell Publishing]] on behalf of the New Phytologist Trust
 |volume=162
 |issue=1
 |pages=7–8
 |issn=0028-646X
 |oclc=1759937
 |location=Lancaster, Lancashire, England
 |jstor=1514472|doi=10.1111/j.1469-8137.2004.01040.x}}</ref>
<ref name="ServiceRecord1">{{cite web
 |url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7901759
 |title=RN Officer's Service Records—Image details—Carter, Gilbert Thomas—Officers' Service Records (Series III)—Paymasters
 |work=DocumentsOnline
 |publisher=[[The National Archives (United Kingdom)|The National Archives]]
 |location=Kew, London, England
 |format=fee normally required to view full pdf of original service record
 |accessdate=17 December 2009
}}</ref>
<ref name="Probate">{{cite web
 |url=http://search.ancestry.co.uk/iexec?htx=View&r=5538&dbid=1904&iid=31874_222549-00571&fn=Gilbert+Thomas&ln=Carter&st=r&ssrc=&pid=1907823
 |title=England & Wales, National Probate Calendar (Index of Wills and Administrations), 1858–1966
 |publisher=[[Ancestry.com]]
 |location=Provo, Utah, USA
 |subscription=yes
 |accessdate=17 June 2012
}}</ref>
<ref name="Ancestry-Marriage">{{cite web
 |url=http://search.ancestry.co.uk/cgi-bin/sse.dll?rank=0&db=freebmdmarriage&ti=5538&f6=101&f7=1874&f9=4&f5=5b
 |title=All England & Wales, FreeBMD Marriage Index 1837–1915 Results
 |publisher=[[Ancestry.com]]
 |location=Provo, Utah, USA
 |subscription=yes
 |accessdate=17 June 2012
}}</ref>
}}

==Bibliography==
{{refbegin}}
* {{cite book|last=Carter|first=Gilbert Thomas|title=The Colony of Lagos|publisher=Royal Colonial Institute|location=London, England|year=1987}}
* {{cite book|title=Historical dictionary of the Gambia|editor1=Hughes, Arnold |editor2=Gailey, Harry A |publisher=[[Rowman & Littlefield|Scarecrow Press]]|location=[[Lanham, Maryland]], USA|year=1999|edition=3|series=African historical dictionaries|volume=79|isbn=978-0-8108-3660-0}}
* {{cite web|last1=ILN|authorlink = Illustrated London News|title=The Arrival And Subsequent Death of Lady Carter At Lagos|url=http://kirbyhistories.blogspot.ca/2014/01/the-arrival-and-subsequent-death-of.html|website=Illustrated London News|accessdate=16 December 2016|date=19 January 1895|ref=harv}}
* {{cite web|title=Lady Carter|url=http://artinstamps.blogspot.ca/2014/11/lady-carter.html|website=Stamp Art|accessdate=16 December 2016|date=16 November 2014|ref={{harvid|Stamp Art|2014}}}}
{{refend}}

==External links==
* [http://heraldry-online.org.uk/carter/carter-arms.htm Carter Coat of Arms]

{{s-start}}
{{s-gov}}
{{succession box|title=[[List of colonial heads of The Gambia|Administrator of the Gambia]]
| before=Sir [[James Shaw Hay]] {{nobold|(1886–1888)}}
| after=Sir [[Robert Baxter Llewelyn]] {{nobold|(1891–1900)}}
| years=1888–1891
}}
{{succession box|title=Governor and Commander-in-Chief of the Colony of Lagos (Nigeria)
| before=[[Cornelius Alfred Moloney]] {{nobold|(1886–1891)}}
| after=[[Henry Edward McCallum]] {{nobold|(1897–1899)}}
| years=1891–1897
}}
{{succession box|title=[[Governor of the Bahamas#Governors of the Bahamas (1717–1969)|Governor and Commander-in-Chief of the Bahama Islands]]
| before=Sir [[William Frederick Haynes Smith]] {{nobold|(1895–1898)}}
| after=Sir [[William Grey-Wilson]] {{nobold|(1904–1912)}}
| years=1898–1904
}}
{{succession box|title=[[List of Governors of Barbados#Governors of Barbados (1885–1966)|Governor and Commander-in-Chief of the Island of Barbados and its Dependencies]]
| before=Sir [[Frederick Mitchell Hodgson]] {{nobold|(1900–1904)}}
| after=Sir [[Leslie Probyn]] {{nobold|(1911–1918)}}
| years=1904–1911
}}
{{s-end}}

{{commons}}

{{DEFAULTSORT:Carter, Gilbert Thomas}}
[[Category:British governors of the Bahamas]]
[[Category:Governors of Barbados]]
[[Category:Governors of the Gambia]]
[[Category:History of Ghana]]
[[Category:British military personnel of the Third Anglo-Ashanti War]]
[[Category:Royal Navy officers]]
[[Category:1848 births]]
[[Category:1927 deaths]]
[[Category:Knights Commander of the Order of St Michael and St George]]
[[Category:Governors of the Lagos Colony]]