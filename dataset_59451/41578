{{Other uses|Battle of Algiers (disambiguation){{!}}Battle of Algiers}}
{{Infobox military conflict
|conflict=Battle of Algiers
|partof=the [[Algerian War]]
|image= [[File:Casbah-cache-Ali-lapointe.jpg|300px]]
|caption= The remains of a house in the [[Casbah of Algiers]] destroyed in the explosion that killed [[Ali la Pointe]]
|date= 30 September 1956 – 24 September 1957<ref name="Horne571">{{harvnb|Horne|p=571}}</ref>
|place= [[Algiers]], [[French Algeria]]
|result= Tactical French victory <ref>International Encyclopedia of Military History - edited by James C. Bradford, p49 [https://books.google.fr/books?id=ELDlCAAAQBAJ&pg=PT79&dq=battle+algiers+tactical+fln+victory&hl=en&sa=X&ved=0CBsQ6AEwAGoVChMIyYjyp6bwxwIVRzwUCh2_JAd0#v=onepage&q=battle%20algiers%20tactical%20fln%20victory&f=false]</ref><ref>Screening Torture: Media Representations of the State of Terror - Michael Flynn, Fabiola, p223 [https://books.google.fr/books?id=twGAFrlmorgC&pg=PA223&dq=battle+algiers+tactical+fln+victory&hl=en&sa=X&ved=0CCAQ6AEwAWoVChMIyYjyp6bwxwIVRzwUCh2_JAd0#v=onepage&q=battle%20algiers%20tactical%20fln%20victory&f=false]</ref> but strategic defeat.<ref>The New Counter-insurgency Era in Critical Perspective - By Celeste Ward Gventer, David Martin Jones, M.L.R Smith, p 183 [https://books.google.fr/books?id=ETDFAgAAQBAJ&pg=PA183&lpg=PA183&dq=But+by+October+1957,+the+French+army+had+transformed+a+tactical+victory+over+the+FLN+into+a+strategic+defeat.&source=bl&ots=BKRA4OV6dH&sig=6ORrWpfc1V8ErG1pcyntdt0PYT4&hl=en&sa=X&ved=0CBsQ6AEwAGoVChMIpvDm2KjwxwIVzNcUCh2jmwOK#v=onepage&q=But%20by%20October%201957%2C%20the%20French%20army%20had%20transformed%20a%20tactical%20victory%20over%20the%20FLN%20into%20a%20strategic%20defeat.&f=false]</ref>
|combatant1={{flagicon|Algeria|1958}} [[National Liberation Front (Algeria)|FLN]]
|combatant2={{flagicon|France}} [[French Fourth Republic|France]]
|commander1=[[Larbi Ben M'hidi]]{{executed}}<br>[[Saadi Yacef]]{{POW}}<br> [[Ali La Pointe]]{{KIA}}
|commander2=[[Jacques Massu]] <br> [[Yves Godard (French officer)|Yves Godard]] <br> [[Marcel Bigeard]]<br> [[Paul Aussaresses]]
|strength1=24,000 FLN members
|strength2=8,000 soldiers<br>1,500 policemen<ref name="Horne188">{{harvnb|Horne|p=188}}.</ref>
|casualties1=1,000-3,000 dead or missing
|casualties2=300 dead <br> 900 wounded
|notes=
}}
{{Campaignbox Algerian War}}

The '''Battle of Algiers''' was a campaign of urban [[guerrilla warfare]] carried out by the [[National Liberation Front (Algeria)|National Liberation Front]] (FLN) against the [[French Algeria]]n authorities from late 1956 to late 1957. The conflict began as a series of terrorist attacks by the FLN against the French forces and civilians in [[Algiers]], reprisals followed and the violence escalated leading the French Governor-General to deploy the [[French Army]] in Algiers to suppress the FLN. Civilian authorities left all prerogatives to General [[Jacques Massu]] who, operating outside legal frameworks between January and September 1957, successfully eliminated the FLN from Algiers. The use of [[torture]], [[forced disappearance]]s and illegal executions by the French later caused controversy in France.

==Background==
In March 1955, [[Rabah Bitat]], head of the FLN in Algiers, was arrested by the French. [[Abane Ramdane]], recently freed from prison, was sent from [[Kabylie]] to take the political direction of the city in hand. In a short time, Ramdane managed to revive the FLN in Algiers. On August 20, 1955, [[Battle of Philippeville|violence broke out]] around Philippeville, drastically escalating the conflict.

In 1956, the "Algerian question" was to be debated at the [[United Nations]]. During the summer of 1956, secret negotiations between the French and Algerian separatists took place in [[Belgrade]] and [[Rome]].

The ''Pied-noirs'' began to organise themselves into a paramilitary group under [[André Achiary]], a former officer of the [[Service de Documentation Extérieure et de Contre-Espionnage|SDECE]] and under-prefect of [[Constantinois]] at the time of [[Sétif massacre]].<ref name=Robin95>Marie-Monique Robin, ''Escadrons de la mort, l'école française'', 2008, p.95</ref>

==First Phase==
[[File:Bataille d'Alger.jpg|thumb|180px|Map of Algiers showing: Muslim quarters (green), European quarters (orange) and attacks by the FLN and counterattacks]]
On 19 June 1956 two FLN prisoners were executed by [[guillotine]] at the Barberousse Prison. Abane Ramdane ordered immediate reprisals against the French and [[Yacef Saâdi]], who had assumed command in Algiers following Bitat's arrest was ordered to "shoot down any European, from 18 to 54. No women, no children, no elder." A series of random attacks in the city followed with 49 civilians shot by the FLN between 21 and 24 June.<ref name="Horne1834">{{harvnb|Horne|pp=183–4}}</ref>

On the night of 10 August 1956, helped by members of [[Robert Martel]]'s ''[[Union française nord-africaine]]'', Achiary planted a bomb at Thèbes Road in the [[Casbah of Algiers|Casbah]] targeted at the FLN responsible for the June shootings, the explosion killed 73 Muslims.<ref name="Horne184"/>

In September at the [[Soummam conference]], the FLN adopted a policy of indiscriminate terrorism and Ben M'Hidi and Yacef Saâdi were ordered to prepare for an offensive.<ref name="Horne184">{{harvnb|Horne|1977|p=184}}</ref>

Peace talks broke down and [[Guy Mollet]]'s government put an end to the policy of negotiations. [[Larbi Ben M'Hidi]] decided to extend terrorist actions to the European city as to touch more urban populations, Arab bourgeoisie in particular, and use Algiers to advertise his cause in metropolitan France and in the International community.

Yacef Saâdi proceeded to establish an organisation based within the Casbah. On the evening of 30 September 1956, a trio of female FLN militants recruited by Yacef Saâdi, [[Djamila Bouhired]], [[Zohra Drif]] and Samia Lakhdari, carried out the first series of bomb attacks on three civilian targets in European Algiers. The bombs at the Milk Bar on Place Bugeaud and the Cafeteria on Rue Michelet killed 3 and injured 50, while the bomb at the [[Air France]] terminus failed to explode due to a faulty timer.<ref name="Horne186">{{harvnb|Horne|1977|p=186}}</ref>

On 22 October 1956, a Moroccan DC-3 plane ferrying the foreign affairs personnel of the FLN from Rabat to Tunis for a conference with President [[Bourguiba]] and the Sultan of Morocco was re-routed to Algiers. [[Hocine Aït Ahmed]], [[Ahmed Ben Bella]], [[Mohammed Boudiaf]], [[Mohamed Khider]] and [[Mostefa Lacheraf]] were arrested.

In December, General [[Raoul Salan]] was promoted to commander in chief of the army of Algeria. Salan was adept in the theory of [[counter-insurgency]]; he chose veterans of the [[First Indochina War]] as his lieutenants, most notably General [[André Dulac]], Colonel Goussault (psychological operations), General [[Robert Allard]], and Lieutenant-Colonel [[Roger Trinquier]]. On the FLN side, a decision was made in late 1956 to embark upon a sustained campaign of urban terrorism designed to show the authority of the French state did not extent to Algiers, Algeria's largest city.<ref name="Evans, Martin pages 201-201">Evans, Martin ''Algeria: France's Undeclared War'', Oxford: Oxford University Press, 2012 pages 201-201.</ref> Abane Ramdane believed that such a campaign would be the "Algerian Dien Bien Phu" that would force the French out of Algeria.<ref name="Evans, Martin pages 201-201"/> It was decided to deliberately target ''pied-noir'' citizens as a way of breaking French power as one FLN directive put it: "A bomb causing the death of ten people and wounding fifty others is the equivalent on the psychological level to the loss of a French battalion."<ref>Evans, Martin ''Algeria: France's Undeclared War'', Oxford: Oxford University Press, 2012 page 202.</ref>

On 28 December 1956, on the orders of Ben M'hidi, [[Ali La Pointe]] assassinated the Mayor of [[Boufarik]] and President of the Federation of Mayors of Algeria, Amedee Froger outside his house on Rue Michelet. The following day, a bomb exploded in the cemetery where Froger was to be buried; enraged European civilians responded by carrying out random revenge attacks (''ratonnade''), killing four Muslims and injuring 50.<ref name="Horne187">{{harvnb|Horne|p=187}}</ref>

===The Army takes over===
On 7 January 1957, Governor-General [[Robert Lacoste]] summoned General Salan and General [[Jacques Massu|Massu]] commander of the [[10th Parachute Division (France)|10th Parachute Division]] (10e DP) and explained that, as the Algiers police force was incapable of dealing with the FLN and controlling the ''Pied-noirs'', Massu was to be granted full responsibility for the maintenance of order in Algiers.<ref name="Horne188"/>

The 5,000 man strong 10e DP had just returned from the [[Suez Crisis|Suez campaign]]. An elite unit it was officered by many veterans of the Indochina War, including Colonels [[Marcel Bigeard]], [[Roger Trinquier]], [[Albert Fossey Francis|Fossey-François]] and [[Yves Godard]] (chief of staff), all of whom were experienced in counter-insurgency and revolutionary warfare and determined to avoid another defeat.

In addition to the 10e DP, Massu's forces included:
*Police (1,100 men)
*the [[Direction de la surveillance du territoire|DST]] (domestic intelligence agency)
*the [[Service de Documentation Extérieure et de Contre-Espionnage|SDECE]] (external intelligence agency)
*the [[11e régiment parachutiste de choc|11th Parachute Choc Regiment]] (11e Choc) (1000 men)
*the 9th [[Zouave]] Regiment (based in the Casbah)
*the 5th [[Chasseurs d'Afrique]] Regiment (350 armored cavalry troops)
*the 25th Dragoon Regiment (400 men)
*two Intervention and Reconnaissance detachments (650 men)
*55 gendarmes
*the [[Compagnies républicaines de sécurité]] (920 men)
*the [[Unités territoriales]] (1,500 men), mostly composed of [[Pied-Noirs]] and led by Colonel [[Jean-Robert Thomazo]]

Prefect [[Serge Baret]] signed a delegation of powers to General Massu, stipulating that:

{{quote|text=over the territory of the Algier department, responsibility for riot control is transferred, from the publication date of this decree, to the military authority that shall exercise police powers normally devoted to civilian authorities.<ref name=Robin95>Marie-Monique Robin, ''Escadrons de la mort, l'école française'', 2008, p.95</ref>}}

Massu was charged to:

{{quote|text=institute zones where stay is regulated or forbidden; to place any person whose activity would prove dangerous to public security and order under house arrest, under surveillance or not; to regulate public meetings, shows, bars; to order declaration of weapons, ammunition and explosives, and order their surrendering or seek and confiscate them; the order and authorise perquisitions of homes by day or night; to decide of penalties imposed as reparations of damage to public and private property to anyone found to have helped the rebellion in any way.<ref name=Robin95/>}} There were no written orders from the government of Guy Mollet to use torture or engage in extrajudicial executions, but numerous Army officers have stated that they received verbal permission to use whatever means necessary to break the FLN including the use of torture and extrajudicial killings.<ref>Evans, Martin ''Algeria: France's Undeclared War'', Oxford: Oxford University Press, 2012 pages 205-206.</ref>

===Deployment and response===
The 10e DP deployed into Algiers the following week. The city had been divided into squares under a system known as ''quadrillage'' with each allotted to a Regimental command. The troops cordoned off each section, established checkpoints and conducted house-to-house searches throughout their areas of responsibility. A unit of the 11e Choc raided the [[Sûreté]] headquarteres and took away all files on FLN suspects and then proceeded to conduct mass arrests.<ref name="Horne190">{{harvnb|Horne|p=190}}</ref>

On the afternoon of Saturday 26 January, female FLN operatives again planted bombs in European Algiers, the targets were the Otomatic on Rue Michelet, the Cafeteria and the Coq-Hardi brasserie. The explosions killed 4 and wounded 50 and a Muslim was killed by ''Pied-Noirs'' in retaliation.<ref name="Horne192">{{harvnb|Horne|p=192}}</ref>

In late January the FLN called an 8-day general strike across Algeria commencing on Monday 28 January. The strike appeared to be a success with most Muslim shops remaining shuttered, workers failed to turn up and children didn't attend school. However Massu soon deployed his troops and used armored cars to pull the steel shutters off shops while army trucks rounded up workers and schoolchildren and forced them to attend their jobs and studies. Within a few days the strike had been broken.<ref name="Horne1902">{{harvnb|Horne|pp=190–2}}</ref> The bombings however continued and in mid-February female FLN operatives planted bombs at the Municipal Stadium and the [[El-Biar]] Stadium in Algiers killing 10 and injuring 45.<ref name="Horne192"/> After visiting Algiers, a clearly shocked defense minister Maurice Bourgès-Maunoury told General Massu after the bombings: "We must finish these people off!".<ref>Evans, Martin ''Algeria: France's Undeclared War'', Oxford: Oxford University Press, 2012 page 207.</ref>

===Battle of the Casbah===
The Casbah fell under the control of Colonel Bigeard and his [[3rd Marine Infantry Parachute Regiment|3rd Colonial Parachute Regiment]] (3e RPC).<ref name="Horne190"/>

While females had not previously been searched in Algiers; following the Coq Hardi explosion one of the waiters identified the bomber as a woman. Accordingly, female suspects were subsequently  searched by metal detectors or physically, limiting the ability of the FLN to continue the bombing campaign from the Casbah. In February Bigeard's troops captured Yacef's bomb transporter, who under extreme interrogation gave the address of the bomb factory at 5 Impasse de la Grenade. On 19 February the 3e RPC raided the bomb factory finding 87 bombs, 70&nbsp;kg of explosives, detonators and other material, Yacef's bomb-making organisation within the Casbah had been destroyed.<ref name="Horne193">{{harvnb|Horne|p=193}}</ref>

==Intelligence, torture and summary executions==
{{Further|Torture during the Algerian War}}
Meanwhile, Colonel Godard had been mapping out the operational structure of the FLN in Algiers with his ''organigramme'', each arrest and interrogation revealed new organisational cells.<ref name="Horne194">{{harvnb|Horne|p=194}}</ref>

Colonel Trinquier operated an intelligence gathering network throughout the city called the Dispositif de Protection Urbaine (DPU) which divided Algiers into sectors, sub-sectors, blocks and buildings each individually numbered. For each block a trusted Muslim French Army veteran was appointed as the block-warden responsible for reporting all suspicious activities in their block. Many of these ''responsables'' would be assassinated by the FLN.<ref name="Horne198">{{harvnb|Horne|p=198}}</ref>

Edward Behr estimated that 30-40% of the male population of the Casbah was arrested at some point during the battle. These arrests generally took place at night so that any names revealed under interrogation could be picked up before the curfew lifted in the morning. The suspects would then be handed over to the Détachement Operationnel de Protection (DOP) for interrogation after which they would either be released or passed to a ''centre d'hebergement'' for further interrogation.<ref name="Horne199">{{harvnb|Horne|p=199}}</ref>

During the battle the use of torture by the French security forces became institutionalised, the techniques ranging from beatings, electroshock (the ''gegene''), [[Waterboarding]], sexual assault and rape.<ref name="Horne198200">{{harvnb|Horne|pp=198–200}}</ref> The use of torture was not restricted to Muslims, French FLN sympathisers were also picked up and subjected to it. [[Maurice Audin]] a Communist university professor was arrested by the paras on 11 June on suspicion of harboring and aiding FLN operatives, [[Henri Alleg]] the Communist editor of ''[[Alger républicain]]'' was arrested by the Paras at Audin's apartment the following day and was told by Audin that he had been tortured, Audin was never seen again and it is believed that he either died while being interrogated or was summarily executed.<ref name="Horne2023">{{harvnb|Horne|pp=202–3}}</ref> Alleg was subjected to the ''gegene'' and waterboarding, following his release he wrote his book [[La Question]] published in 1958 which detailed his final meeting with Audin and his own experience of torture.<ref name="Horne2002">{{harvnb|Horne|pp=200–2}}</ref>

General Massu appointed Major [[Paul Aussaresses]] to run a special interrogation unit<ref name="Aussaresses7592">{{harvnb|Aussaresses|pp=75–92}}</ref> based at the Villa des Tourelles in the Mustapha District of Algiers.<ref name="Aussaresses119">{{harvnb|Aussaresses|p=119}}</ref> High-value suspects and suspects with information relating to matters outside a para regiment's territorial sector would be passed over to Colonel Aussaresses' unit, where "torture was used as a matter of course".<ref name="Aussaresses1201">{{harvnb|Aussaresses|pp=120–1}}</ref> Following interrogation the vast majority of suspects were sent to camps, while thoese deemed too dangerous were driven to a remote location outside of Algiers where they were machine-gunned and buried. At the end of each night Aussaresses would write a report to Generals Salan and Massu and Governor-General Lacoste.<ref name="Aussaresses1212">{{harvnb|Aussaresses|pp=121–2}}</ref>

On 9 February, paratroopers of the [[2nd Parachute Chasseur Regiment]] (2e RCP) arrested a prominent young lawyer and FLN sympathiser [[Ali Boumendjel]]. After attempting suicide Boumendjel volunteered everything he knew, including his involvement in the murder of a European family.<ref name="Aussaresses145">{{harvnb|Aussaresses|p=145}}</ref>

On 25 February Colonel Trinqier's intelligence sources located Ben M'hidi who was captured in his pyjamas by Paras at Rue Claude-Debussy. On 6 March it was announced that Ben M'hidi had committed suicide by hanging himself with his shirt.<ref name="Horne1945">{{harvnb|Horne|pp=194–5}}</ref> Bigeard had spent several days meeting with Ben M'hidi after his capture, hoping to use Ben M'hidi's rivalry with Ben Bella to undermine the FLN. Judge Bérard had suggested to Major Aussaresses that Ben M'hidi should be poisoned with cyanide in an apparent suicide and later in a meeting of General Massu, Colonel Trinquier and Aussaresses it was decided that Ben M'hidi should not stand trial due to the reprisals that would follow his execution and so Major Aussaresses and men of the [[1st Parachute Chasseur Regiment]] (1e RCP) removed him from Bigeard's custody and drove him to a farm outside Algiers where they faked his suicide by hanging.<ref name="Aussaresses13444">{{harvnb|Aussaresses|pp=134–44}}</ref>

On 23 March following a meeting between Massu, Trinquier, Fossy-Francois and Aussaresses to discuss what was to be done with Ali Boumendjel, Aussaresses went to the prison where Boumendjel was being held and ordered that he be transferred to another building, in the process he was thrown from a 6th floor skybridge to his death.<ref name="Aussaresses1478">{{harvnb|Aussaresses|pp=147–8}}</ref>

Major Aussaresses was unapologetic regarding the actions he had undertaken during the battle, he said that "The justice system would have been paralyzed had it not been for our initiative. Many terrorists would have been freed and given the opportunity of launching other attacks..The judicial system was not suited for such drastic conditions... Summary executions were therefore an inseparable part of the tasks associated with keeping law and order."<ref name="Aussaresses128">{{harvnb|Aussaresses|p=128}}</ref>

==Interlude and reorganisation==
By late March 1957 the FLN organisation within Algiers had been completely broken, with most of the FLN leadership killed or underground and no bombs went off in Algiers. The 10e DP were withdrawn from the city and redeployed to engage the FLN in the Kabylia. However Yacef set about rebuilding his organisation within Algiers.

In April one of Yacef's collaborators, Djamila Bouhired was arrested by a French patrol, Yacef following her and disguised as a woman attempted to shoot her, but only succeeded in wounding her and Yacef fled back into the Casbah.<ref name="Horne211">{{harvnb|Horne|p=211}}</ref>

==Second phase==
In early May two paratroopers were shot in the street by the FLN, their comrades led by one of Trinquier's informers attacked a bath-house which was believed to be an FLN hideout, killing almost 80 Muslims.<ref name="Horne2089">{{harvnb|Horne|pp=208–9}}</ref>

On 3 June Yacef's forces planted bombs in street lamps at bus stops in the centre of Algiers, the explosions killed eight and wounded 90, a mix of French and Muslims. On 9 June a bomb exploded at the Casino on the outskirts of Algiers killing nine and injuring 85. Following the burial of the dead from the casino, the ''Pied-Noirs'' started a ''ratonnade'' that resulted in five Muslims dead and more than 50 injured. As a result of this upturn in violence the 10e DP was again deployed to Algiers.<ref name="Horne20911">{{harvnb|Horne|pp=209–11}}</ref>

In July informal negotiations took place between Yacef and [[Germaine Tillion]] to try to agree a deal whereby attacks on civilians would stop in return for the French ceasing to guillotine members of the FLN. During this period a number of FLN bombs were planted but with no civilian casualties.<ref name="Horne2126">{{harvnb|Horne|pp=212–6}}</ref>

===FLN defeated===
On 26 August following intelligence gained by Colonel Godard's operatives, the 3e RPC raided a house in the Impasse Saint-Vincent where Yacef's new bomb-maker and deputy were believed to be hiding. After suffering several casualties trying to capture the two alive, both men were eventually killed.<ref name="Horne212">{{harvnb|Horne|p=212}}</ref>

On 23 September a courier for Yacef was arrested by Godard's men. At 5am on 24 September the 1e REP commanded by Colonel [[Pierre Jeanpierre]] sealed off Rue Caton and raided Yacef's hideout at No. 3. Yacef and Zohra Drif hid in a wall cavity, but this was soon located by the French troops. Yacef threw a grenade at the French troops but they were eager to take him alive and he and Zohra Drif eventually surrendered. Across the street at No 4, Ali La Pointe escaped the French cordon and went to another safe-house in the Casbah.<ref name="Horne217">{{harvnb|Horne|p=217}}</ref>

On the evening of 8 October the 1e REP surrounded Ali La Pointe's hideout at 5 Rue de s Abderames. The paratroops laid charges to blow away the false partition behind which Ali and his comrades were hiding, unfortunately the explosion detonated a store of bombs destroying the house and several neighbouring buildings, killing Ali, his two comrades and 17 other Muslims in neighbouring houses.<ref name="Horne218">{{harvnb|Horne|p=218}}</ref>

The capture of Yacef and the death of Ali la Pointe marked the defeat of the FLN in the city and the end of the Battle of Algiers.

==Aftermath==
The battle was the first clearly definable French victory of the war. The Paras and their commanders enjoyed immense popularity with the ''Pied-noirs'' and this sense of exuberance and strength would reach its zenith during the [[May 1958 crisis]].<ref name="Horne218">{{harvnb|Horne|p=218}}</ref>

The FLN losses are impossible to determine accurately. In addition to the publicised FLN deaths there were many who simply disappeared. Paul Teitgen, general secretary of the Prefecture of Algiers who resigned in March 1957 (but was kept in his post by Governor-General Lacoste until October 1957) over the use of torture by French forces calculated that over 24,000 Muslims had been arrested during the battle and by subtracting those released or still in captivity estimated that as many as 3000 were missing.<ref name="Aussaresses166">{{harvnb|Aussaresses|p=166}}</ref>

As details of the use of torture and summary executions became public in the years following the battle and the end of the Algerian War, the French victory and the reputations of many of the commanders became tainted by the methods used in the battle.<ref name="Horne2047">{{harvnb|Horne|pp=204–7}}</ref>

==In popular culture==
* ''[[Lost Command]]'' (1966) by [[Mark Robson]]
* ''[[The Battle of Algiers (film)|The Battle of Algiers]]'' (1966) by [[Gillo Pontecorvo]]
* ''La Question'' (1977) by Laurent Heynemann

==See also==
* [[Torture during the Algerian War]]

==References==
{{reflist}}

===Bibliography===
* {{citation
| last =Alleg
| first =Henri
| author-link =
| publication-date = 2008 
| year = 1958
| title = La Question
| publisher = 
| isbn = 2-7073-0175-2
}}
* {{citation
| last = Aussaresses
| first = General Paul
| author-link =
| publication-date = 2010
| year = 2010
| title = The Battle of the Casbah: Terrorism and Counter-Terrorism in Algeria 1955-1957
| publisher = Enigma Books
| isbn = 978-1-929631-30-8
}}
* {{citation
| last = Courrière
| first = Yves
| author-link =
| publication-date =  
| year = 1969
| title = La guerre d'Algérie, tome 1 (Les fils de la Toussaint)
| publisher = Fayard
| isbn = 2213611181
}}
* {{citation
| last = Horne
| first = Alistair
| author-link =
| publication-date = 2006
| year = 1977
| title = A Savage War of Peace: Algeria 1954-1962
| publisher = New York Review
| isbn = 978-1-59017-218-6
}}
* {{citation
| last = Morgan
| first = Ted
| author-link =
| publication-date = 2006 
| year = 2005
| title = My Battle of Algiers
| publisher = Smithsonian Books
| isbn = 978-0-06-120576-7
}}
* {{citation
| last =Pellissier
| first = Pierre
| author-link =
| publication-date = 
| year = 2002
| title = La Bataille d'Alger
| publisher = Académique Perrin
| isbn = 2-2620-1865-0
}}
* {{citation
| last =Stora
| first = Benjamin
| author-link =
| publication-date = 
| year = 1993
| title = Histoire de la Guerre d'Algérie, 1954-1962
| publisher = la Découverte
| isbn = 270714293X
}}

==External links==
* [http://www.ina.fr/archivespourtous/index.php?full=bataille+alger&action=ft&x=0&y=0&cs_page=0&cs_order=3 http://www.ina.fr/], Video archives of the [[Institut national de l'audiovisuel|INA]] on the Battle of Algiers
* [http://www.rand.org/pubs/monographs/2006/RAND_MG478-1.pdf Pacification in Algeria], RAND
* {{Internet Archive film clip|id=1957-06-03_British_H-Bomb|description=British H-Bomb Fired As Debate On Atom Test Ban Rages, 1957/06/03 (1957)}}

{{coord|36.7761|N|3.0600|E|source:wikidata|display=title}}

{{DEFAULTSORT:Algiers (1956-7), Battle of}}
[[Category:Conflicts in 1957]]
[[Category:Algerian War]]
[[Category:Battles of the Algerian War|Algiers 1957]]
[[Category:Battles involving France|Algiers 1957]]
[[Category:Battles involving Algeria|Algiers 1957]]
[[Category:Guerrilla wars]]
[[Category:History of Algiers]]
[[Category:Urban warfare]]
[[Category:1956 in France]]
[[Category:1957 in France]]
[[Category:1956 in Algeria]]
[[Category:1957 in Algeria]]
[[Category:20th century in Algiers]]