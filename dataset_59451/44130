{{Infobox airport
| name         = Bergen Airport, Hjellestad
| nativename   = 
| image        =
| image-width  = 
| caption      =
| IATA         =
| ICAO         =
| type         = Public
| owner        =
| operator     =
| city-served  = [[Bergen]], [[Norway]]
| location     = [[Hjellestad]], [[Fana (municipality)|Fana]]
| metric-elev  = yes
| elevation-f  = 0
| elevation-m  = 0
| coordinates  = {{coord|60.2559|N|005.2399|E|region:NO|display=inline,title}}
| pushpin_relief         = yes
| pushpin_map            = Norway
| pushpin_mapsize        = 300
| pushpin_label          = Hjellestad
| pushpin_label_position = right
| pushpin_map_caption    = 
| metric-rwy   = yes
| r1-number    =
| r1-length-m  =
| r1-length-f  = 
| r1-surface   = Water
}}

'''Bergen Airport, Hjellestad''' ({{lang-no|Bergen sjølufthavn, Hjellestad}}) was a [[water airport]] serving [[Bergen]], [[Norway]] from 1948 through 1951. Located at [[Hjellestad]], then in the municipality of [[Fana (municipality)|Fana]], the [[airport]] was variously used by [[Norwegian Air Lines]] (DNL), [[Scandinavian Airlines System]] (SAS), [[Widerøe]] and [[West Norway Airlines]] for [[seaplane]] routes eastwards to [[Oslo]], southwards to [[Haugesund]] and [[Stavanger]], and northwards to [[Ålesund]], [[Kristiansund]] and [[Trondheim]].

[[Bergen Airport, Sandviken]] proved too small for DNL's routes with the introduction of the [[Short Sandringham]], most due to ship traffic in the area. The airport at Hjellestad opened on 23 August 1948. It consisted of a small cabin used as a [[control tower]], a small terminal building. Smaller aircraft could dock at a floating wharf, while the larger Sandringhams anchored at a [[buoy]]. The airport was closed in 1951, after DNL closed its Oslo routes and traffic moved back to Sandviken.

==History==
Bergen Airport, Sandviken was the first water airport to serve the city, with services commencing in 1934. Although it was close to the city center, this also meant that the airport's runway took up central parts of [[Byfjorden (Hordaland)|Byfjorden]]. With increased use of aircraft, they became a safety hazard on the fjord. The operations were distributed, as the [[air traffic control]] was located at [[Herdla Airport]] on [[Askøy]].<ref name=aarsand>{{cite book |last=Aarsand |first=Knut |last2=Bakka |first2=Dag |chapter=Hjellestad sjøflyhavn 1948–1952 |work=Årsskrift 2004–2005 |publisher=Fana Historielag |issn=0809-5256 |pages=38–42 |language=Norwegian}}</ref> Herdla was considered, but its runway was too rudimentary and the airport was far from the city. Although closer, [[Bergen Naval Air Station]] on [[Flatøy]] also needed to be reached with a ferry.<ref name=gynnild>{{cite web |url=http://avinor.moses.no/index.php?seks_id=135&element=kapittel&k=2 |title=Flyplassenes og flytrafikkens historie |work=Kulturminner på norske lufthavner – Landsverneplan for Avinor |publisher=[[Avinor]] |last=Gynnild |first=Olav |year=2009 |language=Norwegian |accessdate=25 January 2012 |archivedate=25 January 2012 |archiveurl=http://www.webcitation.org/6DwQ1MXLE |deadurl=no}}</ref>

The authorities therefore decided to establish a water airport at Hjellestad. This would allow air traffic control and terminal to be co-located and ensured that traffic could continue to increase. The airport was scheduled for completion for 1 June 1948, in time for the summer season operations, but it did not open until 23 August.<ref name=aarsand />

The municipality had ambitions to make Hjellestad the largest water airport in the country. The plans included building a causeway to the island of Dronningen, which would have cost several million [[Norwegian krone]]. The rationale was that Bergen was all of cities of similar size in Norway had a land airport. By 1950 the expansion plans were shelved to allow the funding to be used to build a land airport instead.<ref>Østerbø:29</ref>

The busiest route the service to [[Oslo Airport, Fornebu]]. It was flown by Norwegian Air Lines using a 37-passenger [[Short Sandringham]]. Flight time was seventy minutes. From 1950 the route passed to [[Scandinavian Airlines]], who transported 3,504 passengers to Oslo that year. The Sandringhams continued northwards via [[Ålesund Airport, Sørneset]] and [[Kristiansund]] to [[Trondheim Airport, Værnes]], and onwards northwards. Southwards the service flew via [[Haugesund Airport, Storesundsskjær]] to [[Stavanger Airport, Sola]]. This was flown using the smaller [[Junkers Ju 52]] with capacity for seventeen passengers.<ref name=aarsand />

The route operators shifted in 1950. The route to Trondheim was taken over by [[West Norway Airlines]], which used a [[Short Sealand]]. The southbound route to Stavanger was taken over by [[Widerøe]], using a [[Noorduyn Norseman]]. SAS continued flying to Oslo for another two seasons. Ahead of the 1952 season SAS wanted to retire its Sandringham fleet. It therefore terminated the lease of the airport from the end of the 1951 season. Instead they flew passengers to Stavanger by land planes and onwards with West Norway Airlines to Sandviken, who took over the route from Widerøe. The terminal building and docks were demolished in the late 1950s and all that remains today is the café and the flag pole.<ref name=aarsand />

==Facilities==
The airport was situated at Hjellestad, then part of the municipality of [[Fana (municipality)|Fana]], today in Bergen.<ref name=aarsand /> Ownership of the airport was however under that of Bergen Municipality<ref name=gynnild /> and based on leases paid for by DNL.<ref name=aarsand />

Smaller aircraft, such as the Ju 52 and Sealands, docked as a floating dock at the quay. The Sandringhams were too large for this, and docked at a buoy in the bay. The airport operated a boat which ferried passengers and cargo to and from the aircraft. There was a small terminal building on the quay, which was run by DNL. The airlines also organized bus transport to the city center.<ref name=aarsand />

The control tower was located in a cabin which was placed on a hill shelf overlooking the quay at Hjellestad. A radio mast was erected at Sandkleivane and a windsock installed don Kuholmen. [[Non-directional beacon]]s were installed at Espeland and Bjelkarøy.<ref name=aarsand /> Ship traffic near Hjellestad had to be alerted in case of incoming flights; this was announced through the hoisting of a black and yellow ball on the flag pole at the control tower.<ref name=aarsand />

==References==
{{reflist}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

[[Category:Water aerodromes in Norway]]
[[Category:Defunct airports in Norway]]
[[Category:Airports in Hordaland]]
[[Category:Buildings and structures in Bergen]]
[[Category:1948 establishments in Norway]]
[[Category:1951 disestablishments in Norway]]
[[Category:Airports established in 1948]]