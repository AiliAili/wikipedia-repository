{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox person
| name                      = Dallas Dempster
| birth_name                =
| birth_date                = {{Birth date and age|df=yes|1941|08|27}}<ref name="west-birth">{{cite news | url=http://trove.nla.gov.au/ndp/del/article/47158567 | title=Births | work=The West Australian | date=29 August 1941 | accessdate=24 December 2012}}</ref>
| birth_place               = [[Nedlands, Western Australia]]
| residence                 = 
| nationality               = Australian
| education                 = [[Hale School]]
| occupation                = 
| years_active              = 
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = 
| salary                    = 
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| title                     =  
| term                      = 
| party                     = 
| boards                    =
| spouse                    = Christine<ref name="afr-wife">{{cite news | title=Bankrupt Dempster pays $250,000 instalment | work=The Australian Financial Review | date=28 May 1999 | accessdate=31 January 2013 | author=Drummond, Mark | quote=...relied on his wife, Mrs Christine Dempster...}}</ref>
| children                  = Dallas,<ref name="stm-son">{{cite news | title=heir and there | work=STM Magazine | date=11 November 2007 | accessdate=31 January 2013 | author=Quartermaine, Braden | quote=Dallas Jr, the eldest son of property developer and Burswood casino founder Dallas Dempster}}</ref> Damien, Nina
| awards                    = 
}}
'''Dallas Reginald Dempster''' (born 27 August 1941) is an Australian businessman notable for original development of Perth's Burswood Resort and Casino (now [[Crown Perth]]) and the proposed [[Kwinana Petrochemical Plant]], both of which were among the Western Australian government transactions examined by the 1990–92 [[WA Inc|WA Inc Royal Commission]]. In November 2013 The ''[[The West Australian|West Australian]]'' newspaper named Dempster as one of Western Australia's 100 most influential business leaders (1829–2013).<ref name="Most Influential">{{cite news | title=100 Most Influential – The business leaders who shaped WA 1829 – 2013 | work=The West Australian | date=29 November 2013 | accessdate=29 November 2013 |author1=Harvey, Ben  |author2=Hatch, Daniel}}</ref>

==Early life, education and early career==
He was born in [[Nedlands, Western Australia]] in 1941 to Dallas Athol Dick and Eileen Olga Dempster. He was educated at [[Hale School]].<ref name=WAY79>Sacks M. A. (ed) (1980) ''The WAY 79 Who is Who: synoptic biographies of Western Australians''  Crawley Publishers, Nedlands, W.A. ISBN 0-949848-00-X, p.94</ref>

===Property development===
In collaboration with WA ecologist and wetlands expert Tom Riggert, Dempster developed Floreat Waters Estate at [[Herdsman Lake]], creating permanent lakes by dropping swamp levels below surrounding land, for which he received the Duke of Edinburgh Environment Award.<ref name=ST101289>{{cite news|newspaper=Sunday Times|title=Million-dollar gambler|page=7|date=10 December 1989}}</ref>

==Major projects==

===Burswood casino===
Dempster was a principal in the establishment and early development of Western Australia's only casino, Burswood Resort and Casino (now named [[Crown Perth]]), in partnership with [[Genting Group|Genting Berhad]] of Malaysia which had previous experience in casino development and associated tourism.<ref>Parliament of New South Wales [http://www.parliament.nsw.gov.au/prod/parlment/publications.nsf/0/09A6CB6295F90825CA256ECF00096E34/$File/16-99.pdf Gaming Commissions, Internet Gambling and Responsible Gambling], Retrieved on 6 July 2012.</ref>

The original resort complex included a [[Hotel rating|five-star hotel]], casino, recreation facilities with 18-hole golf course, tennis courts, and swimming pools on a site fronting Perth's Swan River, 3&nbsp;km from the city centre. The casino, the third largest in the world,<ref>{{cite news |author = Staff writer |title = Australia's Gateway to the Orient – Perth |work = [[Australian Financial Review]] |publisher = John Fairfax Holdings Ltd |page = 5 |date= 23 March 1987 |accessdate = 22 September 2006 }}</ref> opened for business in December 1985, with other facilities progressively launched over the following two years. The adjacent Burswood Park was planned to enhance the approach to Perth from the airport.<ref>Burswood Park Board [http://www.burswoodpark.wa.gov.au/web/About+Us Burswood Park on the Stunning Swan River], Retrieved on 6 July 2012.</ref> Burswood Casino and Resort committed to providing the revenue to maintain and improve the park indefinitely.<ref>Burswood Park Board [http://www.burswoodpark.wa.gov.au/files/reports/BPB_AR_2011_.pdf Burswood Park Board Annual Report 2011], Retrieved on 6 July 2012.</ref>

Rival bidders accused Dempster of improper dealings with state premier [[Brian Burke (Australian politician)|Brian Burke]] prior to the awarding of the licence,<ref name="BigRoll">{{cite news |first = Anthony |last = Dennis |title = After a troubled start, WA's $300M casino is on a big roll |work = [[Sydney Morning Herald]] |publisher = John Fairfax Group Pty Ltd |page = 11 |date= 9 May 1987 |accessdate = 22 September 2006 }}</ref> but after a thorough investigation of these accusations, the WA Inc Royal Commission stated that there was no evidence of impropriety.  However it noted that following the awarding of the licence, Mr Dempster, a [[Liberal Party of Australia|Liberal Party]] member, had begun making large donations to the ruling [[Australian Labor Party|Labor Party]] cause.<ref name=RoycomV2>State Law Publisher of Western Australia [http://www.slp.wa.gov.au/publications/publications.nsf/DocByAgency/276D079050C3CC4448256984002033D2/$file/volume2.pdf Findings of WA Inc Royal Commission], Vol. 2, Ch. 8. Retrieved on 2 February 2013. Quotation: It is understandable that others should feel aggrieved by the seemingly irresistible advance of the Dempster machine after the closing date and wonder how this "inside running" could have been achieved without some impropriety somewhere. But the fact remains that despite as thorough an investigation as has been open to the Commission, no impropriety in that regard has been established."</ref>

===Petrochemical Industry Company===
{{main|Kwinana Petrochemical Plant}}
In September 1986 Petrochemical Industries Company Limited (PICL), a joint venture involving companies controlled by Dempster and [[Laurie Connell]],<ref name=RoycomV4>State Law Publisher of Western Australia [http://www.slp.wa.gov.au/publications/publications.nsf/DocByAgency/63BAC21E6BA5A5364825698400207E92/$file/volume4.pdf Findings of WA Inc Royal Commission] Vol. 4, Ch.18. Retrieved on 9 November 2012.</ref>{{rp|p 9}} proposed the development of a major [[petrochemical]] project at [[Kwinana, Western Australia|Kwinana]], south of [[Fremantle]], and on 2 November 1987 heads of agreement were signed by the government-owned [[State Energy Commission of Western Australia]] and PICL for the former to supply the latter with natural gas from the state's [[North West Shelf]] at a commercially realistic price linked to the [[Consumer price index|CPI]]. By December 1987, though, it was becoming apparent that Connell's merchant bank Rothwells was no longer a viable financial institution, and Dempster had been unable to secure 100% debt finance for the project.<ref name=RoycomPt2>State Law Publisher of Western Australia [http://www.slp.wa.gov.au/publications/publications.nsf/DocByAgency/EB7A73F79B8C4FCA482569850012E10E/$file/report2.pdf Findings of WA Inc Royal Commission] Part II</ref>{{rp|p 22}}<ref name=RoycomV5>State Law Publisher of Western Australia [http://www.slp.wa.gov.au/publications/publications.nsf/DocByAgency/37324B79C41BD728482569840020A568/$file/volume5.pdf Findings of WA Inc Royal Commission] Vol. 5, Ch. 21</ref>{{rp|p 166}}

Due to outstanding loans to Connell and Connell-associated entities, Rothwells was short $300 million or more needed to restore its solvency and produce an acceptable balance sheet, and without these funds Rothwells would have to cease trading on its reporting date, 31 July 1988. To deal with Rothwells's impending crisis, a mid-1988 meeting involving representatives of the state government, Rothwells and Bond Corporation agreed that the government would give a discounted energy price and other guarantees to PICL so as to boost the company's value to $400 million, and Bond Corporation and the government would then purchase PICL for that price – $350 million to be paid to Connell so that he could repay $300 million of his debt to Rothwells and add $50 million of liquidity, and $50 million to Dempster for his share.<ref name=RoycomV5/>{{rp|pp 168–70}}

Some evidence to the [[WA Inc]] Royal Commission from government officers conflicted with Dempster's evidence, and he was found to have made a series of deliberate false statements regarding the project's viability.<ref name=RoycomV5/>{{rp|p 167}}

==References==
{{Reflist}}

{{WA Inc}}

{{DEFAULTSORT:Dempster, Dallas}}
[[Category:1941 births]]
[[Category:Living people]]
[[Category:Australian businesspeople]]