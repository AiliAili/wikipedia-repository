{{Infobox Constellation
| abbreviation = TrA
| name = Triangulum Australe
| genitive = Trianguli Australis
| pronounce = {{IPAc-en|t|r|aɪ|ˈ|æ|ŋ|ɡ|jᵿ|l|əm|_|ɒ|s|ˈ|t|r|eɪ|l|iː}},<br>genitive {{IPAc-en|t|r|aɪ|ˈ|æ|ŋ|ɡ|jᵿ|l|aɪ|_|ɒ|ˈ|s|t|r|eɪ|l|ᵻ|s}}
| symbolism = the Southern [[Triangle]]
| RA =  {{RA|14|56.4}} to {{RA|17|13.5}}{{sfn|IAU Constellation Boundary}}
| dec= −60.26° to −70.51°{{sfn|IAU Constellation Boundary}}
| family = [[Hercules Family|Hercules]]
| quadrant = SQ3
| areatotal = 110
| arearank = 83rd
| numbermainstars = 3
| numberbfstars = 10
| numberstarsplanets = 1
| numberbrightstars = 3
| numbernearbystars = 0
| brighteststarname = [[Alpha Trianguli Australis|α TrA]] (Atria)
| starmagnitude = 1.91
| neareststarname = [[Zeta Trianguli Australis|ζ TrA]]
| stardistancely = 39.48
| stardistancepc = 12.11
| numbermessierobjects = None
| meteorshowers = None
| bordering = [[Norma (constellation)|Norma]]<br />[[Ara (constellation)|Ara]]<br />[[Circinus]]<br />[[Apus]]
| latmax = [[25th parallel north|25]]
| latmin = [[South Pole|90]]
| month = July
| notes=
}}
'''Triangulum Australe''' is a small [[constellation]] in the far [[Southern Celestial Hemisphere]]. Its name is [[Latin]] for "the southern triangle", which distinguishes it from [[Triangulum]] in the northern sky and is derived from the almost [[equilateral triangle|equilateral pattern]] of its three brightest [[star]]s. It was first depicted on a celestial globe as Triangulus Antarcticus by [[Petrus Plancius]] in 1589, and later with more accuracy and its current name by [[Johann Bayer]] in his 1603 ''[[Uranometria]]''. The French explorer and astronomer [[Nicolas Louis de Lacaille]] charted and gave the brighter stars their [[Bayer designation]]s in 1756.

[[Alpha Trianguli Australis]], known as Atria, is a [[apparent magnitude|second-magnitude]] orange [[bright giant|giant]] and the brightest star in the constellation, as well as the 42nd-brightest star in the night sky. Completing the triangle are the two [[A-type main sequence star|white main sequence]] stars [[Beta Trianguli Australis|Beta]] and [[Gamma Trianguli Australis]]. Although the constellation lies in the [[Milky Way]] and contains many stars, [[deep-sky object]]s are not prominent. Notable features include the [[open cluster]] [[NGC 6025]] and [[planetary nebula]] [[NGC 5979]].

==History==
Italian navigator [[Amerigo Vespucci]] explored the New World at the beginning of the 16th century. He learnt to recognize the stars in the southern hemisphere and made a catalogue for his patron king [[Manuel I of Portugal]], which is now lost. As well as the catalogue, Vespucci wrote descriptions of the southern stars, including a triangle which may be either Triangulum Australe or [[Apus]]. This was sent to his patron in Florence, [[Lorenzo di Pierfrancesco de' Medici]], and published as ''Mundus Novus'' in 1504.{{sfn|Kanas|2007|pp=118–19}} The first depiction of the constellation was provided in 1589 by Flemish astronomer and clergyman [[Petrus Plancius]] on a {{frac|32|1|2}}-cm diameter [[globe#Celestial|celestial globe]] published in Amsterdam by Dutch cartographer [[Jacob Floris van Langren]],{{sfn|Ridpath Star Tales – Triangulum Australe}}<!-- cites previous three sentences--> where it was called Triangulus Antarcticus and incorrectly portrayed to the south of [[Argo Navis]]. His student [[Pieter Dirkszoon Keyser|Petrus Keyzer]], along with Dutch explorer [[Frederick de Houtman]], coined the name Den Zuyden Trianghel.{{sfn|Wagman|2003|pp=303–04}} Triangulum Australe was more accurately depicted in [[Johann Bayer]]'s celestial atlas ''[[Uranometria]]'' in 1603, where it was also given its current name.{{sfn|Moore|Tirion|1997|p=120}}

[[Nicolas Louis de Lacaille]] portrayed the constellations of [[Norma (constellation)|Norma]], [[Circinus (constellation)|Circinus]] and Triangulum Australe as a set square and ruler, a compass, and a surveyor's level respectively in a set of draughtsman's instruments in his 1756 map of the southern stars.{{sfn|Ridpath Star Tales – Lacaille}} Also depicting it as a surveyor's level, German [[Johann Bode]] gave it the alternate name of Libella in his ''Uranographia''.{{sfn|Ridpath Star Tales – Triangulum Australe}}

German poet and author [[Philipp von Zesen|Philippus Caesius]] saw the three main stars as representing the [[Patriarchs (Bible)|Three Patriarchs]], Abraham, Isaac and Jacob (with Atria as Abraham).{{sfn|Motz|Nathanson|1991|p=387}} The [[Wardaman people]] of the Northern Territory in Australia perceived the stars of Triangulum Australe as the tail of the [[Rainbow Serpent]], which stretched out from near [[Crux]] across to [[Scorpius]]. Overhead in October, the Rainbow Serpent "gives Lightning a nudge" to bring on the wet season rains in November.{{sfn|Harney|Cairns|2004|pp=156–57}}

==Characteristics==
Triangulum Australe is a small constellation bordered by Norma to the north, Circinus to the west, Apus to the south and [[Ara (constellation)|Ara]] to the east. It lies near the Pointers ([[Alpha Centauri|Alpha]] and [[Beta Centauri]]), with only Circinus in between.{{sfn|Moore|2005|p=116}} The constellation is located within the [[Milky Way]], and hence has many stars.{{sfn|Inglis|2004|p=119}} A roughly equilateral triangle, it is easily identifiable. Triangulum Australe lies too far south in the celestial southern hemisphere to be visible from Europe,{{sfn|Moore|2005|p=116}} yet is [[Circumpolar star|circumpolar]] from most of the southern hemisphere.{{sfn|Inglis|2004|p=119}} The three-letter abbreviation for the constellation, as adopted by the [[International Astronomical Union]] in 1922, is "TrA".{{sfn|Russell|1922|p=469}} The official constellation boundaries, as set by [[Eugène Joseph Delporte|Eugène Delporte]] in 1930, are defined by a polygon of 18 segments. In the [[equatorial coordinate system]], the [[right ascension]] coordinates of these borders lie between {{RA|14|56.4}} and {{RA|17|13.5}}, while the [[declination]] coordinates are between −60.26° and −70.51°.{{sfn|IAU Constellation Boundary}} Triangulum Australe [[Culmination|culminates]] each year at 9 p.m. on 23 August.{{sfn|James|2011}}

==Notable features==
{{see also|List of stars in Triangulum Australe}}

===Bright stars===
In defining the constellation, Lacaille gave twelve stars [[Bayer designation]]s of Alpha through to Lambda, with two close stars called Eta (one now known by its [[Star catalogue#HD/HDE|Henry Draper catalogue]] number), while Lambda was later dropped due to its dimness.{{sfn|Wagman|2003|pp=303–04}} The three brightest stars, Alpha, Beta and Gamma, make up the triangle. Readily identified by its orange hue,{{sfn|Moore|2005|p=116}} [[Alpha Trianguli Australis]] is a [[bright giant]] star of [[stellar classification|spectral type]] K2&nbsp;IIb-IIIa with an [[apparent magnitude]] of +1.91 that is the 42nd-brightest star in the night sky.{{sfn|Schaaf|2008|pp=263–65}} It lies {{Convert|424|ly|pc|abbr=off|lk=on}} away and has an [[absolute magnitude]] of −3.68 and is 5,500 times more luminous than our [[Sun]].{{sfn|Ayres|Brown|Harper|2007}} With a diameter 130 times that of our Sun, it would almost reach the orbit of Venus if placed at the centre of the Solar System. The proper name Atria is a contraction of its Bayer designation.{{sfn|Kaler, Atria}} [[Beta Trianguli Australis]] is a [[double star]], the primary being a  [[F-type main-sequence star]] with a stellar classification of F1V,{{sfn|Gray et al.|2006}} and an apparent magnitude of 2.85.{{sfn|Nicolet|1978}} Lying only {{Convert|40|ly|pc|abbr=off|lk=off}} away, it has an absolute magnitude of 2.38.{{sfn|Schaaf|2008|pp=263–65}} Its companion, almost 3 [[Minute and second of arc#Symbols and abbreviations|arcminutes]] away, is a 13th-magnitude star which may or may not be in orbit around Beta.{{sfn|Kaler, Beta TrA}} The remaining member of the triangle is [[Gamma Trianguli Australis]] with an apparent magnitude of 2.87.{{sfn|Wielen et al.|1999}} It is an [[A-type main sequence star]] of spectral class A1&nbsp;V, which lies {{Convert|180|ly|pc|abbr=off|lk=off}} away.{{sfn|Schaaf|2008|pp=263–65}}

Located outside the triangle near Beta, [[Delta Trianguli Australis]] is the fourth-brightest star at apparent magnitude +3.8.{{sfn|Moore|2005|p=116}} It is a yellow giant of spectral type G2Ib-II and lies {{Convert|606|ly|pc|abbr=off|lk=on}} away.{{sfn|SIMBAD Delta Trianguli Australis}} A [[binary star]], it has a 12th-magnitude companion star separated by 30 [[Minute and second of arc#Symbols and abbreviations|arcseconds]] from the primary.{{sfn|SIMBAD CPD-63 3854B}} Lying halfway between Beta and Gamma, [[Epsilon Trianguli Australis]] is another double star. The primary component, Epsilon Trianguli Australis A, is an orange [[stellar classification|K-type]] sub-[[giant star|giant]] of spectral type K1.5III with an apparent magnitude of +4.11.{{sfn|SIMBAD Epsilon Trianguli Australis}} The companion, Epsilon Trianguli Australis B (or HD 138510), is a white main sequence star of spectral type A9IV/V which has an apparent magnitude of +9.32,{{sfn|SIMBAD HD 138510}} [[Zeta Trianguli Australis]] appears as a star of apparent magnitude +4.91 and spectral class F9V, but is actually a [[spectroscopic binary]] with a near companion, probably a [[red dwarf]]. The pair orbit each other once every 13 days.{{sfn|Skuljan|2004}} A young star, its proper motion indicates it is a member of the [[Ursa Major moving group]].{{sfn|Croswell|2005}} [[Iota Trianguli Australis]] shows itself to be a multiple star system composed of a yellow and a white star when seen though a 7.5&nbsp;cm telescope.{{sfn|Hartung|1984|p=214}} The brighter star has a spectral type of F4IV and is a spectroscopic binary whose components are two yellow-white stars which orbit each other every 39.88 days. The primary is a [[Gamma Doradus variable]], pulsating over a period of 1.45 days.{{sfn|De Cat et al.|2009}} The fainter star is not associated with the system, hence the system is an [[Double star|optical double]].{{sfn|Hartung|1984|p=214}} [[HD 147018]] is a Sun-like star of apparent magnitude 8.3 and spectral type G9V,{{sfn|SIMBAD LTT 6522}} which was found to have two exoplanets, [[HD 147018 b]] and [[HD 147018 c]], in 2009.{{sfn|Ségransan et al. |2010}}

Of apparent magnitude 5.11, the yellow bright giant [[Kappa Trianguli Australis]] of spectral type G5IIa lies around {{Convert|1207|ly|pc|abbr=off|lk=off}} distant from the Solar System.{{sfn|SIMBAD Kappa Trianguli Australis}} [[Eta Trianguli Australis]] (or Eta1 Trianguli Australis) is a [[Be star]] of spectral type B7IVe which is {{Convert|661|ly|pc|abbr=off|lk=off}} from Earth, with an apparent magnitude of 5.89.{{sfn|SIMBAD HR 6172}} Lacaille named a close-by star as Eta as well, which was inconsistently followed by [[Francis Baily]], who used the name for the brighter or both stars in two different publications. Despite their faintness, [[Benjamin Apthorp Gould|Benjamin Gould]] upheld their Bayer designation as they were closer than 25 degrees to the south celestial pole. The second Eta is now designated as HD 150550.{{sfn|Wagman|2003|pp=303–04}} It is a variable star of average magnitude 6.53 and spectral type A1III.{{sfn|SIMBAD HD 150550}}

===Variable stars===
Triangulum Australe contains several [[cepheid variable]]s, all of which are too faint to be seen with the naked eye:{{sfn|Klepešta|Rükl|1974|pp=248–49}} [[R Trianguli Australis]] ranges from apparent magnitude 6.4 to 6.9 over a period of 3.389 days, [[S Trianguli Australis]] varies from magnitude 6.1 to 6.8 over 6.323 days, and [[U Trianguli Australis]]' brightness changes from 7.5 to 8.3 over 2.568 days.{{sfn|Inglis|2004|p=119}} All three are yellow-white giants of spectral type F7Ib/II,{{sfn|SIMBAD R Trianguli Australis}} F8II,{{sfn|SIMBAD S Trianguli Australis}} and F8Ib/II respectively.{{sfn|SIMBAD U Trianguli Australis}} [[RT Trianguli Australis]] is an unusual cepheid variable which shows strong [[absorption band]]s in [[molecule|molecular]] fragments of [[Diatomic carbon|C<sub>2</sub>]], [[methylidyne radical|'''⫶'''CH]] and [[cyano radical|'''⋅'''CN]], and has been classified as a carbon cepheid of spectral type R. It varies between magnitudes 9.2 and 9.97 over 1.95 days.{{sfn|Wallerstein et al. 2000}} Lying nearby Gamma, [[X Trianguli Australis]] is a variable [[carbon star]] with an average magnitude of 5.63.{{sfn|SIMBAD X Trianguli Australis}} It has two periods of around 385 and 455 days, and is of spectral type C5, 5(Nb).{{sfn|Tabur|Bedding|2009}}

[[EK Trianguli Australis]], a [[dwarf nova]] of the SU Ursae Majoris type, was first noticed in 1978 and officially described in 1980.{{sfn|Hassall|1985}} It consists of a [[white dwarf]] and a donor star which orbit each other every 1.5 hours.{{sfn|Mennickent|Arenas|1998}} The white dwarf sucks matter from the other star onto an [[accretion disc]] and periodically erupts, reaching magnitude 11.2 in superoutbursts, 12.1 in normal outbursts and remaining at magnitude 16.7 when quiet.{{sfn|Gänsicke et al.|1997}} [[Nova Trianguli Australis 2008]] was a slow nova which peaked at magnitude 8.4 in April 2008, before fading to magnitude 12.4 by September of that year.{{sfn|Bianciardi|2009}}

===Deep-sky objects===
[[File:Hubble Interacting Galaxy ESO 69-6 (2008-04-24).jpg|right|thumb|[[ESO 69-6]], two merging galaxies with prominent long tails, photographed by the [[Hubble Space Telescope]]]]
Triangulum Australe has few [[deep-sky object]]s—one open cluster and a few planetary nebulae and faint galaxies.{{sfn|Inglis|2004|p=119}} [[NGC 6025]] is an [[open cluster]] with about 30 stars ranging from 7th to 9th magnitude.{{sfn|Inglis|2004|p=119}} Located 3 degrees north and 1 east of Beta Trianguli Australis, it lies about {{Convert|2500|ly|pc|abbr=off|lk=off|sigfig=2}} away and is about {{Convert|11|ly|pc|abbr=off|lk=off|sigfig=2}}  in diameter. Its brightest star is MQ Trianguli Australis at apparent magnitude 7.1.{{sfn|Mobberley |2009|p=198}} [[NGC 5979]], a [[planetary nebula]] of apparent magnitude 12.3, has a blue-green hue at higher magnifications, while Henize 2-138 is a smaller planetary nebula of magnitude 11.0.{{sfn|Griffiths|2012|p=262}} [[NGC 5938]] is a remote [[spiral galaxy]] around 300 million light-years (90 megaparsecs) away. It is located 5 degrees south of Epsilon Trianguli Australis.{{sfn|Polakis|2001}} [[ESO 69-6]] is a pair of merging galaxies located about 600 million light-years (185 megaparsecs) away. Their contents have been dragged out in long tails by the interaction.{{sfn|Cosmic Collisions Galore!}}

==References==
'''Citations'''
{{reflist|30em}}

'''Sources'''
* {{citation
  | last1 = Ayres
  | first1 = Thomas R.
  | last2 = Brown
  | first2 = Alexander
  | last3 = Harper
  | first3 = Graham M.
  | title = α TrA Junior
  | journal = The Astrophysical Journal
  | volume = 658
  | issue = 2
  | pages = L107–L110
  |date=April 2007
  | doi = 10.1086/514818
  | bibcode = 2007ApJ...658L.107A
  | ref = harv
  }}
* {{citation
  | last = Bianciardi
  | first = G.
  | journal = Astronomia, La rivista dell' Unione Astrofili Italiani
  | title=Nova Trianguli Australis 2008
  |issn= 0392-2308
  |issue=2
  |pages= 11–12
  |date= 2009
  |language=Italian
  |bibcode=2009AsUAI...2...11B
  | ref = harv
}}
* {{citation
  | title = Descendants of the Dipper
  | last = Croswell
  | first = Ken
  | journal = Astronomy
  | issn = 0091-6358
  | volume = 33
  | issue = 3
  | pages = 40–45
  | date = 2005
  | url = http://www.kencroswell.com/DescendantsOfTheDipper.html
  | ref = harv}}
* {{citation
  | last1 = De Cat
  | first1 = P.
  | last2 = Wright
  | first2 = D. J.
  | last3 = Pollard
  | first3 =  K. R.
  | last4 =  Maisonneuve
  | first4 = F.
  | last5 = Kilmartin
  | first5 = P. M.
  | date = 2009
  | title = Is HD147787 a double-lined binary with two pulsating components? Preliminary results from a spectroscopic multi-site campaign
  | journal = AIP Conference Proceedings
  | volume = 1170
  | pages = 483–85
  | url = http://scitation.aip.org/getpdf/servlet/GetPDFServlet?filetype=pdf&id=APCPCS001170000001000483000001&idtype=cvips&doi=10.1063/1.3246549&prog=normal&bypassSSO=1
  | doi = 10.1063/1.3246549
  | ref = {{sfnRef|De Cat et al.|2009}}
  }}
* {{citation
  | last1 = Gänsicke
  | first1 = B. T.
  | last2 = Beuermann
  | first2 = K.
  | last3 = Thomas
  | first3 =  H.-C.
  | title=EK TrA, a close relative of VW HYI
  | journal  =[[Monthly Notices of the Royal Astronomical Society]]
  | volume = 289
  |issue=2
  |pages= 388–92
  |date=1997
   | bibcode=1997MNRAS.289..388G
  |url=http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1997MNRAS.289..388G&amp;data_type=PDF_HIGH&amp;whole_paper=YES&amp;type=PRINTER&amp;filetype=.pdf
  | ref = {{sfnRef|Gänsicke et al.|1997}}
  | doi=10.1093/mnras/289.2.388
}}
* {{citation
  | last1 = Gray
  | first1 = R. O.
  | last2 = Corbally
  | first2 = C. J.
  | last3 = Garrison
  | first3 = R. F.
  | last4 = McFadden
  | first4 = M. T.
  | last5 = Bubar
  | first5 = E. J.
  | last6 = McGahee
  | first6 = C. E.
  | last7 = O'Donoghue
  | first7 = A. A.
  | last8 = Knox
  | first8 = E. R.
  | title = Contributions to the Nearby Stars (NStars) Project: spectroscopy of stars earlier than M0 within 40 pc-The Southern Sample
  | journal = [[The Astronomical Journal]]
  | volume = 132
  | issue = 1
  | pages = 161–70
  | date = 2006
  | doi = 10.1086/504637
  | bibcode = 2006AJ....132..161G
  | arxiv = astro-ph/0603770
  | ref = {{sfnRef|Gray et al.|2006}}
  }}
* {{citation
  |last=Griffiths
  |first=Martin
  |title=Planetary Nebulae and How to Observe Them
  |publisher=Springer
  |location = New York, New York
  |date=2012
  |isbn=1-4614-1781-3
  |url=https://books.google.com/books?id=Q2aURFWBI8EC&pg=PA262&dq=Triangulum+Australe&hl=en&sa=X&ei=qhPMT7CsFamXiQfvx-DGBg&ved=0CD8Q6AEwAg#v=onepage&q=Triangulum%20Australe&f=false
  | ref = harv
}}
* {{citation
  | last1 = Harney
  | first1 = Bill Yidumduma
  | last2 = Cairns
  | first2 = Hugh C.
  | title = Dark Sparklers
  | publisher = Hugh C. Cairns
  | location = Merimbula, New South Wales
  | date = 2004
  | origyear = 2003
  | edition = Revised
  | isbn = 0-9750908-0-1
  | ref = harv
  }}
* {{citation
  | last = Hartung
  | first = Ernst Johannes
  | title = Astronomical Objects for Southern Telescopes, with an Addendum for Northern Observatories: A Handbook for Amateur Observers
  | publisher = CUP Archive
  |location= Cambridge, United Kingdom
  | date = 1984
  | isbn = 0-521-31887-4
  | ref = harv
  }}
* {{citation
  | last = Hassall
  |first = B.J.M.
  | title=A superoutburst of the dwarf nova EK Trianguli Australis
  |journal=Monthly Notices of the Royal Astronomical Society
  |issn=0035-8711
  |volume= 216
  |date= 1985
  |pages= 335–52
  |bibcode=1985MNRAS.216..335H
  |url=http://adsabs.harvard.edu/full/1985MNRAS.216..335H
  | ref = harv
  | doi=10.1093/mnras/216.2.335
}}
* {{citation
 |last=Inglis
 |first=Mike
 |title=Astronomy of the Milky Way: Observer's Guide to the Southern Sky
 |publisher=Springer
 |location = New York, New York
 |date=2004
 |isbn=1-85233-742-7
 |url=https://books.google.com/books?id=1r0qvMjSCGAC&pg=SA3-PA48&dq=Triangulum+Australe&hl=en&sa=X&ei=LxrMT4msK86uiQfynITsBg&ved=0CFkQ6AEwBg#v=onepage&q=Triangulum%20Australe&f=false
  | ref = harv
}}
* {{citation
  | last = Kanas
  | first = Nick 
  | title = Star Maps: History, Artistry, and Cartography
  | date = 2007
  | isbn = 0-387-71668-8
  | publisher = Praxis Publishing
  | location=Chichester, United Kingdom
  | ref = harv
  }}
* {{citation
  |last1=Klepešta
  |first1=Josef
  |last2=Rükl
  |first2=Antonín
  |title=Constellations
  |publisher=Hamlyn
  |location=Feltham, United Kingdom
  |date=1974
  |origyear=1969
  |isbn=0-600-00893-2
  |ref = harv
}}
* {{citation
  | last1 = Mennickent
  | first1 = Ronald E.
  | last2 = Arenas
  | first2 = Jose
  |date=1998
  |title= The Orbital Period of the SU Ursae Majoris Star EK Trianguli Australis and Evidence for Ring-Like Accretion Disks in Long-Supercycle Length SU Ursae Majoris Stars
  |journal= Publications of the Astronomical Society of Japan
  |volume=50
  |pages=333–42
  |url= http://articles.adsabs.harvard.edu/full/1998PASJ...50..333M
  | ref = harv
|bibcode = 1998PASJ...50..333M |doi = 10.1093/pasj/50.3.333 }}
* {{citation
  |last=Mobberley
  |first=Martin
  |authorlink=Martin Mobberley
  |title=The Caldwell Objects and How to Observe Them
  |publisher=Springer
  |location = New York, New York
  |date=2009
  |isbn=1-4419-0325-9
 |url=https://books.google.com/books?id=amPrWoOWgHcC&pg=PA198&dq=Triangulum+Australe&hl=en&sa=X&ei=LxrMT4msK86uiQfynITsBg&ved=0CE0Q6AEwBA#v=onepage&q=Triangulum%20Australe&f=false
  | ref = harv
}}
* {{citation
  | last = Moore
  | first = Patrick
  | authorlink = Patrick Moore
  | title = The Observer's Year: 366 Nights of the Universe
  | publisher = Springer
  |location = New York, New York
  | date = 2005
  | isbn=1-85233-884-9
  | ref = harv
  }}
* {{citation
  | last1 = Moore
  | first1 = Patrick
  | authorlink1 = Patrick Moore
  | last2 = Tirion
  | first2 = Wil
  | date = 1997
  | title = Cambridge Guide to Stars and Planets
  | publisher = Cambridge University Press
  | location = Cambridge, United Kingdom
  | isbn = 978-0-521-58582-8
  | ref = harv
  }}
* {{citation
  | last1 = Motz
  | first1 = Lloyd
  | last2 = Nathanson
  | first2 = Carol
  | title = The Constellations: An Enthusiast's Guide to the Night Sky
  | publisher = Aurum Press
  | location = London, United Kingdom
  | date = 1991
  | isbn = 1-85410-088-2
  | ref = harv
  }}
* {{citation
  | last = Nicolet
  | first = B.
  | date = 1978
  | title = Photoelectric Photometric Catalogue of Homogeneous Measurements in the UBV System
  | journal = Astronomy and Astrophysics Supplement Series
  | volume = 34 
  | pages = 1–49
  | bibcode = 1978A&AS...34....1N
  | ref = harv
  }}
* {{citation
  | last = Russell
  | first = Henry Norris
  | authorlink = Henry Norris Russell
  | title = The New International Symbols for the Constellations
  | journal = [[Popular Astronomy (US magazine)|Popular Astronomy]]
  | volume = 30
  | pages = 469–71
  | bibcode = 1922PA.....30..469R
  | date = 1922
  | ref = harv
  }}
* {{citation
  | first = Fred
  | last = Schaaf
  | date = 2008
  | title = The Brightest Stars: Discovering the Universe Through the Sky's Most Brilliant Stars
  | publisher = John Wiley and Sons
  | location = Hoboken, New Jersey
  | isbn = 0-471-70410-5
  | ref = harv
  }}
* {{citation
  | ref = {{sfnRef|Ségransan et al. |2010}}
  | author  = Ségransan, D.
  | author2  = Udry, S.
  | author3  = Mayor, M.
  | author4  = Naef, D.
  | author5  = Pepe, F.
  | author6  = Queloz, D.
  | author7  = Santos, N.C.
  | author8  = Demory, B.-O.
  | author9  = Figueira, P.
  | author10  = Gillon, M.
  | author11  = Marmier, M.
  | author12  = Mégevand, D.
  | author13  = Sosnowska, D.
  | author14  = Tamuz, O.
  | author15  =  Triaud, A. H. M. J.
  | title = The CORALIE Survey for Southern Extrasolar Planets: XVI. Discovery of a Planetary System around HD 147018 and of Two Long Period and Massive Planets Orbiting HD 171238 and HD 204313
  | date = February 2010
  | journal = Astronomy and Astrophysics
  | volume = 511
  | page = A45
  | url = http://www.aanda.org/index.php?option=com_article&access=standard&Itemid=129&url=/articles/aa/full_html/2010/03/aa12136-09/aa12136-09.html
  | doi=10.1051/0004-6361/200912136
  |arxiv = 0908.1479 |bibcode = 2010A&A...511A..45S }}
* {{citation
  | last=Skuljan
  | first=Jovan
  | date=2004
  | title=Accurate Orbital Parameters for the Bright Southern Spectroscopic Binary ζ Trianguli Australis – an Interesting Case of a Near-circular Orbit
  | journal=Monthly Notices of the Royal Astronomical Society
  | volume=352
  | page=975
  | bibcode = 2004MNRAS.352..975S
  | doi = 10.1111/j.1365-2966.2004.07988.x
  | ref = harv
  }}
* {{citation
  | title = Long-term Photometry and Periods for 261 Nearby Pulsating M Giants
  | last1 = Tabur
  | first1 = V.
  | last2 = Bedding
  | first2 = T. R.
  | date = 2009
  | journal = Monthly Notices of the Royal Astronomical Society
  | volume = 400
  | issue = 4
  | pages = 1945–61
  | doi = 10.1111/j.1365-2966.2009.15588.x
  | ref = harv
  |arxiv = 0908.3228 |bibcode = 2009MNRAS.400.1945T }}
* {{citation
  | last = Wagman
  | first = Morton
  | date = 2003
  | title = Lost Stars: Lost, Missing and Troublesome Stars from the Catalogues of Johannes Bayer, Nicholas Louis de Lacaille, John Flamsteed, and Sundry Others
  | publisher = The McDonald & Woodward Publishing Company
  | location = Blacksburg, Virginia
  | isbn = 978-0-939923-78-6
  | ref = harv
  }}
* {{citation
  | ref = {{sfnRef|Wallerstein et al. 2000}}
  |title=The Carbon Cepheid RT Trianguli Australis: Additional Evidence of Triple-α and CNO Cycling
  |last1=Wallerstein
  |first1=George
  |last2=Matt
  |first2=Sean
  |last3=Gonzalez
  |first3=Guillermo
  |journal=Monthly Notices of the Royal Astronomical Society
  |volume= 311
  |issue=2
  |pages=414–22
  |bibcode=2000MNRAS.311..414W
  |url=http://adsabs.harvard.edu/full/2000MNRAS.311..414W
  |date=2000
  |doi = 10.1046/j.1365-8711.2000.03064.x
  }}
* {{citation
  | title = Sixth Catalogue of Fundamental Stars (FK6). Part I. Basic Fundamental Stars with Direct Solutions
  | last1 = Wielen
  | first1 = R.
  | last2 = Schwan
  | first2 = H.
  | last3 = Dettbarn
  | first3 = C.
  | last4 = Lenhardt
  | first4 = H.
  | last5 = Jahreiß
  | first5 = H.
  | last6 = Jährling
  | first6 = R.
  | publisher = Astronomisches Rechen-Institut Heidelberg
  | issue = 35
  | date = 1999
  | bibcode = 1999VeARI..35....1W
  | ref = {{sfnRef|Wielen et al.|1999}}
  }}
{{refend}}

'''Online sources'''
{{refbegin|30em}}
* {{citation
  | title = Cosmic Collisions Galore!
  |url=http://hubblesite.org/newscenter/archive/releases/2008/16/image/as/|author=Office of Public Outreach
  |date=24 April 2008
  |work=HubbleSite
  |publisher=Space Telescope Science Institute (STScI)
  |accessdate=17 October 2012
  |location=Baltimore, Maryland
  | ref = {{sfnRef|Cosmic Collisions Galore!}}
}}
* {{citation
  | title = Triangulum Australe, Constellation Boundary
  | work = The Constellations
  | publisher = International Astronomical Union
  | url = http://www.iau.org/public/constellations/#tra
  | accessdate = 7 June 2012
  | ref = {{sfnRef|IAU Constellation Boundary}}
  }}
* {{citation
  | url = http://www.southastrodel.com/Page20502.htm
  | title='The '"Constellations : Part 2 Culmination Times"'
  | work = Southern Astronomical Delights
  | date = 7 February 2011
  | accessdate = 10 September 2012
  | last = James
  | first = Andrew
  | location = Sydney, New South Wales
  | ref = harv
  }}
* {{citation
  | url = http://stars.astro.illinois.edu/sow/atria.html
  | title = Atria (Alpha Trianguli Australis)
  | last = Kaler
  | first = Jim
  | work = Stars
  | publisher = University of Illinois
  | accessdate = 17 October 2012
  | ref = {{sfnRef|Kaler, Atria}}
  }}
* {{citation
  | url = http://stars.astro.illinois.edu/sow/betatra.html
  | title = Beta Trianguli Australis
  | last = Kaler
  | first = Jim
  | work = Stars
  | publisher = University of Illinois
  | accessdate = 17 October 2012
  | date = 10 August 2007
  | ref = {{sfnRef|Kaler, Beta TrA}}
  }}
* {{citation
  | url =  http://www.ianridpath.com/startales/lacaillenormaetc.htm
  | last = Polakis
  | first = Tom
  | title= Ara, Triangulum and Apus:  A spectacular Myriad of Deep-sky Objects Fills this Southern Trio
  |journal=Astronomy
  |volume=29
  | issue=7
  |pages=80–84
  | date= 2001
  |issn=0091-6358
  | ref = harv
}}
* {{citation
  | url =  http://www.ianridpath.com/startales/lacaillenormaetc.htm
  | last = Ridpath
  | first = Ian 
  | title = Lacaille's Grouping of Norma, Circinus, and Triangulum Australe
  | accessdate = 7 June 2012
  | work = Star Tales
  | ref = {{sfnRef|Ridpath Star Tales – Lacaille}}
  }}
* {{citation
  | url =  http://www.ianridpath.com/startales/triangulumaustrale.htm
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | title = Triangulum Australe
  | accessdate= 7 June 2012
  | work= Star Tales
  | ref = {{sfnRef|Ridpath Star Tales – Triangulum Australe}}
  }}
* {{citation
  |url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Delta+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = Delta Trianguli Australis 
  | work = SIMBAD Astronomical Database 
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 7 June 2012
  | ref = {{sfnRef|SIMBAD Delta Trianguli Australis}}
  }}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%403413226&Name=CPD-63%20%203854B&submit=submit
  | title = CPD-63 3854B 
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 4 October 2012
  | ref = {{sfnRef|SIMBAD CPD-63 3854B}}
  }}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Epsilon+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | title = Epsilon Trianguli Australis 
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 7 June 2012
  | ref = {{sfnRef|SIMBAD Epsilon Trianguli Australis}}
  }}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%403381154&Name=HD%20138510&submit=submit
  | title = HD 138510 
  | work = SIMBAD Astronomical Database 
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 7 June 2012
  | ref = {{sfnRef|SIMBAD HD 138510}}
  }}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+150550&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | title = HD 150550 – Pulsating variable Star
  | work = SIMBAD Astronomical Database 
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 4 October 2012
  | ref = {{sfnRef|SIMBAD HD 150550}}
  }}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=16+41+23.10750-68+17+46.0378&CooFrame=ICRS&CooEqui=2000.0&CooEpoch=J2000&Radius.unit=arcmin&submit=query+around&Radius=2
  | title =  HR 6172 – Be Star
  | work = SIMBAD Astronomical Database 
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 4 October 2012
  | ref = {{sfnRef|SIMBAD HR 6172}}
  }}
* {{citation
  |url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Kappa+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = Kappa Trianguli Australis
  |work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  |accessdate = 4 October 2012
  | ref = {{sfnRef|SIMBAD Kappa Trianguli Australis}}
}}
* {{citation
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+147018 
  | title = LTT 6522 – High proper-motion Star 
  | work = SIMBAD Astronomical Database 
  | publisher = Centre de Données astronomiques de Strasbourg
  | accessdate = 8 September 2012
  | ref = {{sfnRef|SIMBAD LTT 6522}}
  }}
* {{citation
  |url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=R+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = R Trianguli Australis – Classical Cepheid (delta Cep type)
  |work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  |accessdate = 2 September 2012
  | ref = {{sfnRef|SIMBAD R Trianguli Australis}}
}}
* {{citation
  |url =  http://simbad.u-strasbg.fr/simbad/sim-id?Ident=S+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = S Trianguli Australis – Classical Cepheid (delta Cep type)
  |work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  |accessdate = 2 September 2012
  | ref = {{sfnRef|SIMBAD S Trianguli Australis}}
}}
* {{citation
  |url =  http://simbad.u-strasbg.fr/simbad/sim-id?Ident=U+Trianguli+Australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = U Trianguli Australis – Cepheid variable Star
  |work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  |accessdate = 2 September 2012
  | ref = {{sfnRef|SIMBAD U Trianguli Australis}}
}}
* {{citation
  |url =  http://simbad.u-strasbg.fr/simbad/sim-id?Ident=X+trianguli+australis&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  |title = X Trianguli Australis – Carbon Star
  |work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  |accessdate = 9 September 2012
  | ref = {{sfnRef|SIMBAD X Trianguli Australis}}
}}
{{refend}}

== External links ==
{{Commons+cat|Triangulum Australe|Triangulum Australe}}
* [http://www.allthesky.com/constellations/norma/ The Deep Photographic Guide to the Constellations: Triangulum Australe]
* [http://www.starrynightphotos.com/constellations/triang_austral.htm Starry Night Photography: Triangulum Australe]
* [http://www.constellation-guide.com/constellation-list/triangulum-australe-constellation/ Triangulum Australe at Constellation Guide]

{{Stars of Triangulum Australe}}
{{navconstel}}
{{Sky|16|00|00|-|65|00|00|10}}

{{featured article}}

[[Category:Constellations listed by Petrus Plancius]]
[[Category:Southern constellations]]
[[Category:Triangulum Australe| ]]
[[Category:Dutch constellations]]