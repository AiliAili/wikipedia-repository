{{Infobox journal
| title = Hastings West-Northwest Journal of Environmental Law and Policy
| cover = [[File:West-Northwest Journal Cover.jpg|120px]]
| abbreviation = Hastings West-Northwest J. Environ. Law Policy
| discipline = [[Environmental law]]
| editors = Kevin Armonio
| publisher = [[University of California, Hastings College of the Law]]
| history = 1994-present
| frequency = Biannually
| website = http://journals.uchastings.edu/journals/websites/west-northwest/
| impact = 
| link1-name =
| link2 =
| ISSN = 1080-0735
| eISSN = 
| OCLC = 818986639
| LCCN = 96657139
| JSTOR = 
}}
The '''''Hastings West-Northwest Journal of Environmental Law and Policy''''' (''[[Bluebook]]'' abbreviation: ''Hastings W.-N.W. J. Envtl. L. & Pol'y''), also known by its abbreviated title, '''''West-Northwest''''', is a student-run [[law review]] published at the [[University of California, Hastings College of the Law]]. The journal primarily covers [[environmental law|environmental law and policy]] and related subjects with a regional focus in [[California]], [[Hawai'i]], [[Alaska]], and the [[Pacific Northwest]].<ref name = WNW>''Journal Information'', 21 {{smallcaps|Hastings W.-Nw. J. Envt'l L. & Pol'y}} vii (2015).</ref> 

== History and overview ==
''West-Northwest'' was established in 1994 as a regional journal "that would contribute to an integrated understanding of environmental and natural resources policy."<ref name = Gray>Brian E. Gray, ''Dedication'', 14 {{smallcaps|Hastings W.-Nw. J. Envt'l L. & Pol'y}} 29 (2008).</ref> The founding editors had the goal of establishing an "interdisciplinary journal" that would feature "articles by legal scholars, practicing lawyers, biologists, economists, engineers, historians, hydrologists, land and resource managers, and other professionals."<ref name = Gray/> It was the first regional environmental law journal to focus specifically on issues involving [[California]] and the [[Pacific Northwest]].<ref>{{smallcaps|Philippine Laws and Jurisprudence Databank}}, [http://www.lawphil.net/legalink/zines.html/ Online Legal Publications: Hastings West-Northwest Journal of Environmental Law and Policy]; see also ''Introduction'', 3 {{smallcaps|Hastings W.-Nw. J. Envt'l L. & Pol'y}} xi (1995-1996).</ref> 

Although the journal primarily covers environmental law and policy, it also includes complimentary fields, including [[economics]], [[anthropology]], [[regional planning]], [[engineering]], [[biology]], and [[Earth sciences]].<ref name = WNW/> The journal also features photography and other artwork relating to nature and the environment in California, Hawai'i, Alaska, and the Pacific Northwest.<ref>Brian King & Michael Linder, ''Introduction'', 20 {{smallcaps|Hastings W.-Nw. J. Envt'l L. & Pol'y}} iii (2014).</ref> Occasionally, ''West-Northwest'' will publish works of [[fiction]], [[poetry]], and other [[non-fiction]]. In 2008, the journal published an anthology of its "most important" articles to honor [[Joseph Sax]]'s award of the 2007 [[Blue Planet Prize]].<ref>Matthew Visick, ''Introduction'', 14 {{smallcaps|Hastings W.-Nw. J. Envt'l L. & Pol'y}} ii (2008).</ref> The journal has also been a sponsor of the annual California Water Law Symposium.<ref>{{smallcaps|California Water Law Symposium}}, [http://www.waterlawsymposium.com/seminars/ Past Symposia].</ref>

== Impact ==
[[Washington and Lee University]]'s 2008-2015 Law Journal Rankings places ''West-Northwest'' among the top thirty law journals that specialize in environmental, natural resources, and land use law.<ref>[http://lawlib.wlu.edu/LJ/index.aspx "Law Journals: Submission and Ranking, 2008-2015,"] Washington & Lee University (Accessed September 25, 2016).</ref> Articles appearing in the journal have been cited by many [[state supreme courts]] in published decisions.<ref>See ''Cedar River Water and Sewer Dist. v. King County'', 178 Wash.2d 763 (2013); ''Boston Gas Co. v. Century Indem. Co.'', 454 Mass. 337 (2009); ''EnergyNorth Natural Gas, Inc. v. Certain Underwriters at Lloyd's'', 156 N.H. 333 (2007); ''High Plains A & M, LLC v. Southeastern Colorado Water Conservancy Dist.'', 120 P.3d 710 (Colo. 2005); ''Fletcher Hill, Inc. v. Crosbie'', 178 Vt. 77 (2005); ''Spaulding Composites Co., Inc. v. Aetna Cas. and Sur. Co.'', 176 N.J. 25 (2003).</ref> Articles also appear in many legal treatises, including ''[[American Jurisprudence]]'',<ref>See, e.g., American Jurisprudence, Chapter 212, Public Lands, 15A {{smallcaps|Am. Jur. Legal Forms}} 2d Ch. 212.</ref> [[American Law Reports]],<ref>Kurtis A. Kemper, ''Delisting of Species Protected Under Endangered Species Act'', 54 {{smallcaps|A.L.R. Fed. 2d}} 607 (2011).</ref> and [[Westlaw]] practice guides.<ref>''Insurance Law Reports - Personal and Commercial Liability Archive Cases (2002 to 2011): Commercial General Liability Insurance-Pollution Cases'', CCH-INSLRPCL P 7657 (C.C.H.) (2003).</ref>

== Alternate title ==
* The Spring 1994 edition of ''West-Northwest'' (Vol. 1, No. 1) was titled ''West-Northwest Journal of Environmental Law, Policy, Thought.''<ref>{{smallcaps|Library of Congress Online Catalogue}}, [http://lccn.loc.gov/96657139/ Hastings West-Northwest Journal of Environmental Law and Policy: Other Title].</ref>

== Abstracting and indexing ==
The journal is abstracted or indexed in [[HeinOnline]], [[LexisNexis]], [[Westlaw]],<ref name=Finder>{{smallcaps|Washington and Lee University Law Library}}, [http://lawlib.wlu.edu/resolver.aspx?title=Hastings%20West-Northwest%20Journal%20of%20Environmental%20Law%20and%20Policy&issn=1080-0735/Journal Finder: Hastings West-Northwest Journal of Environmental Law and Policy].</ref> and the [[University of Washington]]'s Current Index to Legal Periodicals.<ref>{{smallcaps|University of Washington Gallagher Law Library}}, [https://lib.law.washington.edu/cilp/period.html Periodicals Indexed in CLIP].</ref> Tables of contents are also available through [[Ingenta]].<ref name=Finder/> As of July 2015, the [[University of California, Hastings College of the Law]] plans to make available full-text reprints of articles at the university's online scholarship repository.<ref>{{smallcaps|University of California, Hastings College of the Law}}, [http://repository.uchastings.edu/journals.html Scholarship Repository: Law Journals] (Accessed July 23, 2015).</ref>

== See also ==
* [[List of law journals]]
* [[List of environmental law journals]]

==References==
{{cbignore}}
{{reflist|30em}}

==External links==
* {{Official website|http://journals.uchastings.edu/journals/websites/west-northwest/}}

{{DEFAULTSORT:Hastings West-Northwest Journal of Environmental Law and Policy}}
[[Category:American law journals]]
[[Category:University of California, Hastings College of the Law]]
[[Category:Publications established in 1994]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Environmental law journals]]
[[Category:Law journals edited by students]]