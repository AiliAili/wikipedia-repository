[[File:Chris Candido Memorial Show screenshot.jpg|thumb|200px|DVD title screen for the 2005 Chris Candido Memorial Show]]
The '''Chris Candido Memorial Show''' was an annual [[professional wrestling]] [[List of professional wrestling memorial shows|memorial event]] produced by the USA Xtreme Wrestling (UXW) [[professional wrestling promotion|promotion]] and held between 2005 and 2006.<ref name="OWW">{{cite web |url=http://www.onlineworldofwrestling.com/results/usa-pro/ |title=USA Pro Wrestling |author= |year=2006 |work=Results |publisher=OnlineWorldofWrestling.com |accessdate= |archiveurl= |archivedate=}}</ref> The show was held in memory of [[Chris Candido]], who died from a [[Thrombus|blood clot]] due to complications from ankle surgery in [[Matawan, New Jersey]] on April 28, 2005, with the proceeds and merchandise sales being donated to Candido's late [[common-law marriage|common-law wife]] and [[manager (professional wrestling)|manager]] [[Tammy Lynn Sytch]] and the Chris Candido Scholarship Fund.<ref name="Dawgs">{{cite web|url=http://www.gumgod.com/matt_candido.htm|title=Chris Candido Memorial Show|author=Dawgs, Matt|year=2005|publisher=GumGod.com}}</ref>

Though not the first such event, the show was arguably the biggest gathering of former alumni of [[Extreme Championship Wrestling]] prior to the [[Hardcore Homecoming]] shows which started that same year. It was also the third major memorial show held for a former ECW star, the second and last one held by UXW, and one of four held for Chris Candido: the [[Chris Candido Memorial Tag Team Tournament]] (2005), the Chris Candido Cup Tag Team Tournament (2007-2008) and the Chris Candido Memorial J-Cup (2009). A second Candido Memorial Show was held by the National Wrestling Superstars, another independent promotion Candido wrestled for, during the same time.<ref>{{cite news |title=Memorial show planned for wrestler Candido |author=Rosenberg, Michelle |newspaper=Independent |date=June 2, 2005 |url=http://independent.our-hometown.com/news/2005-06-02/Sports/030.html }}</ref> NWS promoter Joe Panzarino acquired the rights to promote the [[Jersey J-Cup|J-Cup Tournament]] that same year which was subsequently renamed the [[Jersey J-Cup#2005|Chris Candido Memorial J-Cup]].

The first show was later released on DVD by two companies, [[RF Video]] and TCTapes.net; Rob Feinstein and Doug Gentry both made public appearances for the event while RF Video provided additional coverage backstage and conducted interviews with wrestlers and other on-screen personalities in attendance including Candido's younger brother [[Johnny Candido|Johnny]], [[The Sandman (wrestler)|The Sandman]], [[Mick Foley]], [[Raven (wrestler)|Raven]], [[Balls Mahoney]] (who also performed as a [[color commentator]]), [[Axl Rotten]], [[Tony DeVito|DeVito]], [[Steve Robinson (wrestler)|Corporal Robinson]], Ref Hansen, Stephen DeAngelis, [[Kid Kash]], [[Stephanie Finochio|Trinity]], [[Bill Apter]] and others discussing their memories of Candido.<ref name="RF Video">{{cite video | people=USA Pro Wrestling (Producer) | date=May 21, 2005 | url=http://www.rfvideo.com/uxwchriscandidomemorial52105.aspx | title=UXW Chris Candido Memorial 5/21/05 | medium=DVD | location=[[Bethpage, New York]] | publisher=[[RF Video]]}}</ref> Sean "The Mic" McCaffery, editor-in-chief of DeclarationofIndependents.net, also provided color commentary for TCTapes.net.<ref name="Dawgs"/><ref name="Kent">{{cite web |url=http://www.411mania.com/wrestling/video_reviews/33501/VIOLENT-PANDA-Wrestling-Review:-UXW-Chris-Candido-Memorial-Show.htm |title=VIOLENT PANDA Wrestling Review: UXW Chris Candido Memorial Show  |author=Kent, Peter |date=August 29, 2005 |work=Video Reviews |publisher=411mania.com |accessdate= |archiveurl= |archivedate=}}</ref> The show was also promoted by the Don Tony And Kevin Castle Show [[internet radio|internet radio show]] by offering those who ordered tickets via Anthony 'Don Tony' DeBlasi and Wrestling-News.com to have their photo be taken with [[Daffney]] and be the first fans let into the building before the general public.<ref name="DTKC">{{cite video | people=The Don Tony & Kevin Castle Show (Producer) | date=May 15, 2006 | url=http://traffic.libsyn.com/prowrestlingwebcast/MinRep5152006.mp3 | title=Minority Report Webcast 5/15/06 (Wrestling-News.com) | medium=[[Internet radio]] | location= | publisher=ProWrestlingWebcast.com}}</ref>

While the promotion was praised for its tribute to Candido, who had been a mainstay in USA Xtreme Wrestling since 2001, the memorial shows themselves received mixed reviews from critics. Some of the criticism was due to the length of the shows, which ran roughly four hours, and the last-minute substitutions of local wrestlers in place of bigger stars.<ref name="Kent"/> This was primarily due to promoter Frank Goodman dropping his policy prohibiting USA Pro wrestlers wrestling "double shots", in which wrestlers worked on more than one show a night, causing many of the bigger stars to arrive late or miss the show entirely. [[D'Lo Brown|D-Lo Brown]], for example, wrestled on two other shows on the night of the Candido's memorial. Several other promotions were running shows on the same night including the New York State Wrestling Federation (NYSWF) in [[Yonkers, New York]], the [[CyberSpace Wrestling Federation]] (CSWF) in [[Wayne, New Jersey]], and smaller organizations in Pennsylvania. This, in addition to the heavy inbound traffic to [[Long Island]], resulted in more than half the regular roster being absent a half-hour before showtime. It also meant last-minute changes to the card, substituting opponents with local wrestlers or dropping other matches altogether,<ref name="Dawgs"/> as [[Crowbar (wrestler)|Crowbar]], "Lowlife" Louie Ramos, [[Mana the Polynesian Warrior]], [[Billy Reil]], [[Sonny Siaki]], [[Sabu (wrestler)|Sabu]], and [[Xavier (wrestler)|Xavier]] were all unable to make the show.<ref name="Dawgs"/><ref name="IPW">{{cite web|url=http://wrestling.insidepulse.com/2005/05/16/37590/ |title=InsidePulse Indy Report 5.16.5: News, Results, TU-SAT Shows |author= |date=May 16, 2005 |work=Inside Pulse Wrestling |publisher=InsidePulse.com |accessdate= |deadurl=yes |archiveurl=https://web.archive.org/web/20120316153121/http://wrestling.insidepulse.com/2005/05/16/37590/ |archivedate=2012-03-16 |df= }}</ref> Similarly, Axl Rotten, Sabu and [[Big Van Vader]] were scheduled to be on the second show but failed to appear<ref name="WrestlingFigs">{{cite web |url=http://www.wrestlingfigs.com/inner2.php?id=2275&page_id=1 |title=UXW Presents 2nd Annual Candido Memorial Show 5/20 |author= |date=April 5, 2006 |work= |publisher=WrestlingFigs.com |accessdate= |archiveurl= |archivedate=}}</ref><ref name="Zevon">Zevon, Mike. "UXW's 2nd Annual Chris Candido Memorial Show Preview." DeclarationofIndependents.net. N.p., 2006. Web. 24 Feb. 2011. <www.declarationofindependents.net/doi/pages/reviews/mikezevon_reviews/chriscandido2.html></ref> as did UXW [[disc jockey|sound technician]] Pat Savino, whose wife had gone into [[child labor]] earlier that night, meaning wrestlers were not provided with [[music in professional wrestling|entrance music]].<ref name="Lynch">Lynch, John. "UXW's Second Annual Chris Candido Memorial Show May 20, 2006 Review." DeclarationofIndependents.net. N.p., 2006. Web. 24 Feb. 2011. <www.declarationofindependents.net/doi/pages/reviews/johnny5lcqueenz_reviews/generaladmission/uxw052006.html></ref>

Among the reviews, GumGod.com's Matt Dawgs called the first event "one of the best UXW shows ever assembled"<ref name="Dawgs"/> while Peter Kent of 411mania.com referred to the show as "torture" and gave the show a rating of 1 out of 5.<ref name="Kent"/> In his review of the second event, John Lynch of DeclarationofIndependents.net wrote the event "seemingly marked the end of one era in UXW and the beginning of another" as many of the promotion's former ECW stars were likely to leave the promotion to join [[World Wrestling Entertainment]]'s newly formed [[ECW (WWE)|ECW brand]].<ref name="Lynch"/>

==Show results==

===First Annual Chris Candido Memorial Show (2005)===
'''May 21, 2005 in [[Old Bethpage, New York]] (Skate Safe America)'''
{{Pro Wrestling results table
|results=
|times=

|match1=[[Johnny Candido]] (with [[Tammy Lynn Sytch]]) won the first-annual Candido Cup Battle Royal last eliminating [[Balls Mahoney]]<ref group=Note>The final two participants were required to win either by pin or submission. The other participants included were: [[Janel Horton|Alere Little Feather]], Big Jay, Blade Michaels, Brolly, [[Christopher Street Connection|Buff-E]], Candy, Danny Demanto, [[Tony DeVito|DeVito]], Eddie Guapo, Equalizer, Greatness, Heavy Metal, Jimmy Hustler, Johnny Bravado, Johnny TNT, Ken Sweeney, [[Christopher Street Connection|Mace Mendoza]], Matt Lariat, Mercenary, Mike Campbell, Ron Sampson, Sean Sanchez, Slugger, The New Dynamite Kid, and Tony Lo.</ref>
|stip1=26-man Candido Cup Battle Royal
|time1=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch">{{cite web |url=http://www.cagematch.net/?id=1&nr=7590 |title=UXW Chris Candido Memorial Show |author=Kreikenbohm, Philip |date= |work=Events |publisher=CageMatch.net |language=de |accessdate= |archiveurl= |archivedate=}}</ref><ref name="Tees">{{cite web |url=http://www.kocosports.com/absolutenm/anmviewer.asp?a=4329 |title=UXW "Chris Candido Memorial Show" Results (5/21) |author=Tees, David |date=May 22, 2005 |work= |publisher=KocoSports.com |accessdate= |archiveurl= |archivedate=}}</ref>

|match2=Poppalishus (with Matt Da Angry Young Man) defeated Oman Tortuga
|stip2=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time2=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match3=[[Monty Sopp|Billy Gunn]] defeated [[Peter Polaco|Justin Credible]]
|stip3=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time3=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match4=[[Axl Rotten]] defeated [[Steve Robinson (wrestler)|Corporal Robinson]]
|stip4=[[Hardcore wrestling|Hardcore match]]
|time4=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match5=[[Mike Kruel]] defeated [[Andy Douglas]]
|stip5=[[Professional wrestling match types#Variations of singles matches|"Candido Protege" match]]  for the vacant UXW United States Championship; Candido had been the reigning champion at the time of his death and both men were later handpicked by Tammy Lynn Sytch, having been both mentored by Candido, to wrestle for the title.
|time5=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/><ref>{{cite web|url=http://www.pwtorch.com/ChampionsListing.html |title=2006 List Of Champs And Title Changes: Up-To-Date List of 2006 Title Changes and Holders Worldwide |author=Lipinski, Keith |date=March 8, 2005 |work=Annual Title Change Listings |publisher=[[Pro Wrestling Torch Newsletter]] |accessdate= |deadurl=yes |archiveurl=https://web.archive.org/web/20100505003713/http://www.pwtorch.com:80/ChampionsListing.html |archivedate=2010-05-05 |df= }}</ref>

|match6=[[Elix Skipper]] (c) defeated [[Kid Kash]]
|stip6=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the UXW X-Treme Championship
|time6=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match7=[[Veronica Stevens|Simply Luscious]] (with Johnny Diamond) defeated [[Janel Horton|Alere Little Feather]]
|stip7=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time7=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match8=The Greatest Masked Wrestlers Of All Time (Masked Maniac and [[Shark Boy]]) defeated [[Christopher Street Connection|The Christopher Street Connection]] (Buff E and Mace Mendoza)
|stip8=[[Tag team#Tag team match rules|Tag Team match]]
|time8=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match9=[[CM Punk]] defeated [[Ian Rotten]]
|stip9=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time9=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match10=Knight Life ([[Mike Tobin]] and [[Trent Acid]]) (with Justin Credible) defeated The Solution (Papadon and Havok) (c) (with John Shane)
|stip10=[[Tag team#Tag team match rules|Tag Team match]] for the UXW Tag Team Championship; As per the pre-match stipulation, The Solution was forced to break-up and face each other at the next show.
|time10=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match11=[[Balls Mahoney]] (c) defeated [[D'Lo Brown|D-Lo Brown]]
|stip11=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the UXW Heavyweight Championship
|time11=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

|match12=[[The Sandman (wrestler)|The Sandman]] defeated [[Raven (wrestler)|Raven]]
|stip12=[[Professional wrestling match types#Hardcore-based variations|Raven's Rules match]] with [[Professional wrestling match types#Special referee|special guest referee]] [[Mick Foley]]
|time12=<ref name="OWW"/><ref name="Dawgs"/><ref name="RF Video"/><ref name="Kent"/><ref name="CageMatch"/><ref name="Tees"/>

}}

<references group=Note/>

===Second Annual Chris Candido Memorial Show (2006)===
'''May 20, 2006 in [[Old Bethpage, New York]] (Skate Safe America)'''
{{Pro Wrestling results table
|results=
|times=

|match1=Malta The Destroyer defeated Xena
|stip1=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time1=<ref name="OWW"/><ref name="Lynch"/><ref name="CageMatch2">{{cite web |url=http://www.cagematch.net/?id=1&nr=7056 |title=UXW Second Annual Chris Candido Memorial Show |author=Kreikenbohm, Philip |date= |work=Events |publisher=CageMatch.net |language=de |accessdate= |archiveurl= |archivedate=}}</ref>

|match2=Bison Bravado (c) defeated [[Mike Tobin]]
|stip2=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the UXW New York State "Philly" Championship with [[Professional wrestling match types#Special referee|special guest referee]] Tony Lo
|time2=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match3=[[Xavier (wrestler)|Xavier]] defeated [[Elix Skipper]]
|stip3=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time3=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match4=[[Christopher Street Connection|The Christopher Street Connection]] (Buff-E and Mace Mendoza) defeated Wacky Wayne and Ron Sampson
|stip4=[[Tag team#Tag team match rules|Tag Team match]]
|time4=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match5=[[Steve Mack|Monsta Mack]] and Havok defeated Christopher Street Connection (Buff-E and Mace Mendoza)
|stip5=[[Tag team#Tag team match rules|Tag Team match]] for the UXW Tag Team Championship
|time5=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match6=Mr. Big defeated Johnny Diamond (with Diamond Vixxxen)
|stip6=[[Professional wrestling match types#Tuxedo match|Tuxedo match]]
|time6=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match7=Little Greatness defeated Dan and Brandon Resto
|stip7=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time7=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match8=Ghanda Rhea Akbar (with Ali Baba) won the second-annual Chris Candido Memorial Battle Royal by eliminating [[Johnny Candido]]<ref group=Note>The final two participants were required to win either by pin or submission. The other participants included were: [[Billy Reil]], Team Puerto Rico, Wacky Wayne, [[Christopher Street Connection|Buff-E and Mace Mendoza]], Ron Sampson, Tony Lo, [[Mike Tobin]], Bison Bravado, Greatness, [[Monsta Mack]], Havok, and Alex Armani among others.</ref>
|stip8=Chris Candido Memorial Battle Royal
|time8=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match9=Louie Ramos defeated High Life Louie (with Anthony 'Don Tony' DeBlasi)
|stip9=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time9=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match10=The New Dynamite Kid defeated Masked Maniac and Ken Sweeney (with Mr. Big)
|stip10=[[Professional wrestling match types#Basic elimination matches|Three-Way Dance]]
|time10=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match11=[[Crowbar (wrestler)|Crowbar]] (with [[Daffney]]) defeated Sinister X
|stip11=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time11=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match12=[[King Kong Bundy]] (with [[Kenny Casanova]]) defeated [[Jake Roberts|Jake "The Snake" Roberts]]
|stip12=[[Professional wrestling match types#Variations of singles matches|Legends match]]
|time12=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match13=[[Trent Acid]] defeated [[Balls Mahoney]]
|stip13=[[Championship unification|Title Unification match]] for the UXW United States Championship and the UXW X-Treme Championship
|time13=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match14=[[Mike Kruel]] defeated [[Chad Wicks|Billy Kryptonite]] (with Mr. Big)
|stip14=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time14=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>

|match15=[[Trent Acid]] defeated [[Raven (wrestler)|Raven]], [[Nick Berk]], [[Johnny Candido]], [[The Sandman (wrestler)|The Sandman]] and [[Steve Corino]]
|stip15=[[Professional wrestling match types#Basic elimination matches|8-Ball Challenge]] [[Championship unification|Title Unification match]] for the UXW X-Treme Championship and the UXW Heavyweight Championship
|time15=<ref name="OWW"/><ref name="Zevon"/><ref name="Lynch"/><ref name="CageMatch2"/>
}}

<references group=Note/>

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20050908060514/http://www.usaprowrestling.net/videos.htm Chris Candido Memorial Show at USAProWrestling.net]

[[Category:Professional wrestling memorial shows]]
[[Category:2005 in professional wrestling]]
[[Category:2006 in professional wrestling]]