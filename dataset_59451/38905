{{redirect|Stendec}}
{{EngvarB|date=July 2014}}
{{DISPLAYTITLE:1947 BSAA Avro Lancastrian ''Star Dust'' accident}}
{{good article}}
{{Infobox aircraft occurrence
|occurrence_type = Accident
|name = BSAA ''Star Dust'' accident
|image = Avro Avro 691 Lancastrian 3 G-AGWH cn 1280 'Stardust' BSAA (British South American Airways) (15215624954).jpg
|caption = [[British South American Airways|BSAA]] [[Avro Lancastrian|Lancastrian 3]] G-AGWH painted as ''Stardust''
|date = 2 August 1947
|type = [[Controlled flight into terrain]] due to severe weather conditions<ref name="Crash pilot cleared" /><ref name="Pilot Cleared" />
|site = [[Tupungato|Mount Tupungato]], Argentina
|coordinates = {{coord|33|22|15|S|69|45|40|W|type:event_source:kolossus-eswiki|display=inline,title}}
|passengers = 6
|crew = 5
|fatalities = 11 (all)
|aircraft_type = [[Avro Lancastrian]]
|operator = [[British South American Airways]]
|tail_number = G-AGWH
|origin = {{nowrap|[[Morón Airport and Air Base|Morón Airport and Air Base (MOR/SADM)]]<ref>Rayner (2002), p. 112.</ref><br/>[[Buenos Aires, Argentina]]}}
|destination = {{nowrap|Los Cerrillos Airport (ULC/SCTI)<ref name="Vanishings TV"/><br/>[[Santiago, Chile]]}}
}}

'''''Star Dust''''' ([[United Kingdom aircraft registration|registration]] G-AGWH) was a [[British South American Airways]] (BSAA) [[Avro Lancastrian]] airliner which crashed into Mount [[Tupungato]] in the [[Argentina|Argentine]] [[Andes]] on 2 August 1947, during a flight from [[Buenos Aires]] to [[Santiago, Chile]]. A comprehensive search of a wide area (including what is now known to have been the crash site) was fruitless, and the fate of the aircraft and occupants remained unknown for over 50 years. An investigation in 2000 after wreckage of G-AGWH had been found determined the crash was caused by weather-related factors,<ref name="Crash pilot cleared">{{cite news | url=https://www.theguardian.com/uk/2000/jul/08/2 | title=Crash pilot cleared 50 years on | newspaper=[[The Guardian]] | date=7 July 2000 | accessdate=28 September 2011 }}</ref><ref name="Pilot Cleared">{{cite news | url=http://www.thefreelibrary.com/Pilot+finally+cleared+over+mystery+of+1947+mountain+plane+disaster.-a063254928 | title=Pilot finally cleared over mystery of 1947 mountain plane disaster | newspaper=[[The Birmingham Post]] | date=8 July 2000 | accessdate=18 August 2011 }}</ref> but until then speculation had included theories of international intrigue, intercorporate sabotage and even [[alien abduction|abduction by aliens]].

In the late 1990s, pieces of wreckage from the missing aircraft began to emerge from the [[glacial ice]].  It is now assumed that the crew became confused as to their exact location while flying at high altitudes through the (then poorly understood) [[jet stream]]. Mistakenly believing they had already cleared the mountain tops, they started their descent when they were in fact still behind cloud-covered peaks, and ''Star Dust'' crashed into Mount Tupungato, killing all aboard and burying itself in snow and ice.

The last word in ''Star Dust's'' final [[Morse code]] transmission to Santiago airport, "STENDEC", was received by the airport control tower four minutes before its planned landing and repeated twice; it has never been satisfactorily explained.

== Background ==
The aircraft, an [[Avro Lancastrian|Avro 691 Lancastrian 3]], was built as constructor's number 1280 for the [[Ministry of Supply]] to carry 13 passengers, and first flew on 27 November 1945. Its civil [[certificate of airworthiness]] (CofA) number 7282 was issued on 1 January 1946. It was delivered to BSAA on 12 January 1946, was [[Aircraft registration|registered]] on 16 January as G-AGWH and given the individual aircraft name "''Star Dust''".<ref>http://www.flywiththestars.co.uk/Airline/Fleet/fleet.htm</ref>

''Star Dust'' carried six passengers and a crew of five on its final flight. The captain, Reginald Cook, was an experienced [[Royal Air Force]] pilot with combat experience during [[World War II]]—as were his first officer, Norman Hilton Cook, and second officer, Donald Checklin. Reginald Cook had been awarded the [[Distinguished Service Order]] (DSO) and the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC). The radio operator, Dennis Harmer, also had a record of wartime as well as civilian service.  The crew also included Iris Evans, a [[flight attendant]] or "Stargirl", who had previously served in the [[Women's Royal Naval Service]] ("Wrens").<ref name="BBC News Lost Plane Found">{{cite web|url=http://news.bbc.co.uk/2/hi/uk_news/618829.stm | title=Lost plane found in Andes | publisher=BBC News | date=26 January 2000 | accessdate=18 August 2011}}</ref>

The passengers were Casis Said Atalah, a [[Palestinian people|Palestinian]] returning home to Chile from a visit to his dying mother; Jack Gooderham and Harald Pagh, businessmen; Peter Young, an agent for [[Dunlop Rubber|Dunlop]]; Paul Simpson, a British civil servant; and Marta Limpert, a Chilean resident of German origin who had been stranded in Germany during the war along with her husband. Atalah is said to have had a diamond with him (stitched into the lining of his suit), Limpert was bringing her dead husband's ashes with her, and Simpson was functioning as a [[King's Messenger]] with diplomatic documents destined for the British embassy in Santiago.<ref name="Vanished Transcript">{{cite web |url=http://www.bbc.co.uk/science/horizon/2000/vanished_transcript.shtml |title=Vanished: The Plane That Disappeared |publisher=BBC | date=2 November 2000 |accessdate=18 August 2011}}</ref>

''Star Dust's'' last flight was the final leg of BSAA Flight CS59, which had started in London on an [[Avro York]] named ''Star Mist'' on 29 July 1947, landing in Buenos Aires on 1 August.<ref>Rayner (2002), pp. 119–122.</ref> Marta Limpert was the only one of the six passengers known for certain to have initially boarded ''Star Mist'' in London<ref>Rayner (2002), p. 119.</ref> before changing aircraft in Buenos Aires to continue on to Santiago with the other passengers.<ref name="Vanished Report">{{cite web| url=http://www.pbs.org/wgbh/nova/vanished/sten_report.html | title=Vanished: 1947 Official Accident Report | publisher=PBS | accessdate=18 August 2011 }}</ref>

== Disappearance ==
[[File:Stgo Abril.jpg|thumb|right|The Andes above modern-day Santiago]]
The flight left Buenos Aires at 1.46 PM on 2 August<ref>Rayner (2002), p. 124.</ref> and was apparently uneventful until the radio operator (Harmer) sent a routine message in [[Morse code]] to the airport in Santiago at 5.41 PM, announcing an expected arrival of 5.45 PM.<ref name="Vanished STENDEC" /> However, ''Star Dust'' never arrived, no more radio transmissions were received by the airport, and intensive efforts by both Chilean and Argentine search teams, as well as by other BSAA pilots, failed to uncover any trace of the aircraft or of the people on board.<ref name="Vanishings TV">{{cite episode | title=Stardust Lost in the Andes | series=Vanishings! | network=[[History International]] | airdate=27 September 2003}}</ref> The head of BSAA, [[Air Vice Marshal]] [[Don Bennett]], personally directed an unsuccessful five-day search.<ref name=Bermuda_75>{{cite book | title=Can Anyone See Bermuda? Memories of an Airline Pilot (1941–1976) | author=Jackson, Archie | year=1997 | publisher=Cirrus Associates | location=[[Gillingham, Dorset]] | isbn=0-9515598-5-0 | page=75}}</ref>

A report by an [[amateur radio]] operator who claimed to have received a faint [[SOS]] signal from ''Star Dust'' initially raised hopes that there might have been survivors,<ref name="Vanishings TV" /> but all subsequent attempts over the years to find the vanished flight failed. In the absence of any hard evidence, numerous theories arose —including rumours of sabotage (compounded by the later disappearance of two other aircraft also belonging to British South American Airways);<ref name="Vanished Transcript" /> speculation that the flight might have been blown up to destroy diplomatic documents being carried by passenger Paul Simpson;<ref name="Vanished Transcript" /> or even the suggestion that ''Star Dust'' might have been taken or destroyed by a [[UFO]] (an idea fuelled by unresolved questions about the flight's final Morse code message).<ref name="Vanished STENDEC" />

== Discovery of wreckage and reconstruction of the crash ==
[[File:Stardust Wheel Wreckage.png|thumb|right|A main wheel from the ''Star Dust'', found amidst the wreckage in 2000]]
In 1998, two Argentine mountaineers climbing [[Tupungato|Mount Tupungato]]—about {{convert|60|mi|abbr=on}} west-southwest of [[Mendoza, Argentina|Mendoza City]], and about {{convert|50|mi|abbr=on}} east of Santiago—found the wreckage of a [[Rolls-Royce Merlin]] aircraft engine, along with twisted pieces of metal and shreds of clothing, in the Tupungato Glacier at an elevation of {{convert|15000|ft|abbr=on}}.<ref name="Vanishings TV" />

In 2000, an [[Argentine Army]] expedition found additional wreckage—including a [[propeller]] and wheels (one of which had an intact and inflated tyre)—and noted that the wreckage was well localised, a fact which pointed to a head-on impact with the ground, and which also ruled out a mid-air explosion.<ref>Rayner (2002), p. 212.</ref> Human remains were also recovered, including three torsos, a foot in an ankle boot and a manicured hand. By 2002, the bodies of five of the eight British victims had been identified through [[DNA]] testing.<ref name="Bowcott">{{cite news|url=https://www.theguardian.com/uk/2002/sep/06/owenbowcott1|title=DNA clues reveal 55-year-old secrets behind crash of the Star Dust|newspaper=[[The Guardian]]|date=6 September 2002 | accessdate=18 August 2011}}</ref>

A recovered propeller showed that the engine had been running at near-cruising speed at the time of the impact. Additionally, the condition of the wheels proved that the undercarriage was still retracted, suggesting [[controlled flight into terrain]] rather than an attempted emergency landing.<ref>Rayner (2002), p. 213.</ref> During the final portion of ''Star Dust's'' flight, heavy clouds would have blocked visibility of the ground. It has therefore been suggested that, in the absence of visual sightings of the ground due to the clouds, a large navigational error could have been made as the aircraft flew through the [[jet stream]]—a phenomenon not well understood in 1947, in which high-altitude winds can blow at high speed in directions different from those of winds observed at ground level.<ref>Rayner (2002), p. 214.</ref> If the airliner, which had to cross the Andes mountain range at {{convert|24000|ft|abbr=on}}, had entered the jet-stream zone—which in this area normally blows from the west and south-west, resulting in the aircraft encountering a headwind—this would have significantly decreased the aircraft's [[ground speed]].

Mistakenly assuming their ground speed to be faster than it really was, the crew may have [[dead reckoning|deduced]] that they had already safely crossed the Andes, and so commenced their descent to Santiago, whereas in fact they were still a considerable distance to the east-north-east and were approaching the cloud-shrouded Tupungato Glacier at high speed.<ref name="Vanished Transcript" /> Some BSAA pilots, however, have expressed scepticism at this theory; convinced that Cook would not have started his descent without a positive indication that he had crossed the mountains, they have suggested that strong winds may have brought down the craft in some other way.<ref>Rayner (2002), pp. 215–216.</ref> One of the pilots recalled that "we had all been warned not to enter cloud over the mountains as the turbulence and icing posed too great a threat."<ref name=Bermuda_75 />

A similar set of events to those that doomed ''Star Dust'' also caused the crash of [[1972 Andes flight disaster|Uruguayan Air Force Flight 571]] in 1972 (depicted in the film ''[[Alive (1993 film)|Alive]]''), though there were survivors from that crash because it involved a glancing blow to a mountainside rather than a head-on collision.<ref name="Alive">{{cite web | url=http://www.history.com/topics/alive | title=I Am Alive: The Crash of Uruguayan Air Force Flight 571 | publisher=History.com | accessdate=18 August 2011 }}</ref>

''Star Dust'' is likely to have flown into a nearly vertical snow field near the top of the glacier, causing an avalanche that buried the wreckage within seconds and concealed it from searchers. As the compressed snow turned to ice, the wreckage would have been incorporated into the body of the glacier, with fragments emerging many years later and much farther down the mountain. Between 1998 and 2000, about ten per cent of the wreckage emerged from the glacier, prompting several re-examinations of the accident. More debris is expected to emerge in future, not only as a result of normal [[Glacier#Motion|glacial motion]], but also as the glacier [[Retreat of glaciers since 1850|melts]].<ref name="Vanished Transcript" />

A 2000 [[Argentine Air Force]] investigation cleared Captain Cook of any blame, concluding that the crash had resulted from "a heavy snowstorm" and "very cloudy weather", as a result of which the crew "were unable to correct their positioning".<ref name="Crash pilot cleared"/><ref name="Pilot Cleared"/>

== STENDEC ==

The last Morse code message sent by ''Star Dust'' was "ETA SANTIAGO 17.45 HRS STENDEC".<ref name="Vanished STENDEC" /> The [[Chilean Air Force]] radio operator at the Santiago airport described this transmission as coming in "loud and clear" but very fast; as he did not recognise the last word, he requested clarification and heard "STENDEC" repeated twice in succession before contact with the aircraft was lost.<ref name="Ministry of Civil Aviation">[http://wiki.gark.net/images/0/06/Star_Dust_Report.pdf Ministry of Civil Aviation, "Report on the accident to Lancastrian III G-AGWH which occurred on 2nd August 1947 in the Andes Mountains South America", Accidents Investigation Branch Report No. C.A. 106. London: His Majesty's Stationery Office 1948]</ref><ref>Rayner (2002), p. 125.</ref> This word has not been definitively explained and has given rise to much speculation—including suggestions (made before the wreckage was finally discovered) that the aircraft and those aboard could have been the victims of a [[UFO]] encounter.<ref name="Vanished STENDEC">{{cite web | url=http://www.bbc.co.uk/science/horizon/2000/vanished_stendec.shtml | title='STENDEC' – Stardust's final mysterious message | publisher=BBC | date=2 November 2000 | accessdate=18 August 2011 }}</ref>

The staff of the BBC television series ''[[Horizon (BBC TV series)|Horizon]]''—which presented an episode in 2000 on the ''Star Dust'' disappearance—received hundreds of messages from viewers proposing explanations of ''STENDEC''.  These included suggestions that the radio operator, possibly suffering from [[Hypoxia (medical)|hypoxia]], had scrambled the word ''DESCENT'' (of which ''STENDEC'' is an [[anagram]]); that ''STENDEC'' may have been the initials of some obscure phrase; or that the airport radio operator had misheard the Morse code transmission despite it reportedly having been repeated multiple times.  The ''Horizon'' staff concluded that, with the possible exception of some misunderstanding based on Morse code, none of these proposed solutions was plausible.<ref name="Vanished STENDEC" /> In the absence of new clues, the meaning of ''STENDEC'' is likely to remain a mystery.<ref name="Vanished Transcript" /><ref>Rayner (2002), pp. 226–229.</ref>

== See also ==
*[[BSAA Star Tiger disappearance|BSAA ''Star Tiger'' disappearance]]
*[[BSAA Star Ariel disappearance|BSAA ''Star Ariel'' disappearance]]
*[[LAN Chile Flight 621]]

== Notes ==
{{reflist|30em}}

== References ==
{{cite book | author=Rayner, Jay | title=Star Dust Falling: The Story of the Plane that Vanished | year=2002 | publisher=Doubleday | isbn=0-385-60226-X }}

== External links ==
* [http://www.bbc.co.uk/science/horizon/2000/vanished.shtml BBC ''Horizon'' programme on the ''Star Dust'' accident]
* [http://www.pbs.org/wgbh/nova/vanished/ PBS NOVA programme] (US version of the ''Horizon'' programme)
* [http://www.summitpost.org/mountains/photo_link.pl?photo_id=66086&object_id=2365&type=mountain&mountain_id=2365&route_id= Aerial photo of the Tupungato area]
* {{ASN accident|id=19470802-0}}
* [http://wiki.gark.net/images/0/06/Star_Dust_Report.pdf Ministry of Civil Aviation official report on the accident, 1948]
* [http://www.flightglobal.com/pdfarchive/view/1946/1946%20-%201594.html "Over the Andes"] a 1946 ''Flight'' article on the BSAA route

{{Aviation accidents and incidents in 1947}}

{{Use dmy dates|date=July 2014}}

{{DEFAULTSORT:BSAA Avro Lancastrian Star Dust accident}}
[[Category:Airliner accidents and incidents involving controlled flight into terrain]]
[[Category:Aviation accidents and incidents in Argentina]]
[[Category:Aviation accidents and incidents in 1947]]
[[Category:British South American Airways accidents and incidents]]