{{Infobox journal
| title= Journal of Counseling & Development
| cover = [[File:Journal of Counseling & Development cover.gif]]
| editor = Spencer G. Niles
| discipline = [[Psychotherapy]], [[counseling]], [[applied psychology]]
| abbreviation = J. Couns. Dev.
| formernames = National Vocational Guidance Bulletin, Vocational Guidance Magazine, Occupations: The Vocational Guidance Journal, The Personnel and Guidance Journal
| publisher = [[Wiley-Blackwell]] on behalf of the [[American Counseling Association]]
| country = United States
| frequency = Quarterly
| history = 1921-present
| impact = 0.622
| impact-year = 2011
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1556-6678
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1556-6678/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1556-6678/issues
| link2-name = Online archive
| ISSN = 0748-9633
| eISSN = 1556-6676
| LCCN = 85647714
| OCLC = 11046863
}}
The '''''Journal of Counseling & Development''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published quarterly by [[Wiley-Blackwell]] on behalf of the [[American Counseling Association]]. The journal was established in 1921 as the ''National Vocational Guidance Bulletin''. In 1924 the titled changed to ''Vocational Guidance Magazine'' and, again, to ''Occupations: The Vocational Guidance Journal''. In 1952 the titled became ''The Personnel and Guidance Journal'' and in 1984 the journal adapted the current name, ''Journal of Counseling & Development''. The current [[editor-in-chief]] is [[Spencer G. Niles]] ([[Pennsylvania State University]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.622, ranking it 60th out of 72 journals in the category "Psychology, Applied".<ref name=WoS>{{cite book |year=2012 |chapter=Journal of Counseling & Development |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2012-08-02 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1556-6678}}

{{DEFAULTSORT:Journal of Counseling and Development}}
[[Category:Wiley-Blackwell academic journals]]
[[Category:Quarterly journals]]
[[Category:Psychotherapy journals]]
[[Category:Publications established in 1921]]
[[Category:English-language journals]]