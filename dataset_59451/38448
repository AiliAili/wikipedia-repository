{{Use dmy dates|date=May 2015}}
{{Use Australian English|date=May 2015}}
{{Infobox artist
| image = <!-- Only freely-licensed images may be used to depict living people.  See [[WP:NONFREE]].  -->
| name          = Susie Bootja Bootja Napaltjarri 
| caption       = 
| birth_name    = 
| birth_date    = {{birth-date|c.1935}}
| birth_place   = Kurtal, south-west of [[Balgo, Western Australia]]
| death_date    = 16 January 2003
| death_place   = 
| nationality   = Australian
| field         = Painting
| training      = 
| movement      = 
| works         = 
| patrons       = 
| awards        = 
}}

'''Susie Bootja Bootja Napaltjarri''' (also referred to as '''Susie Bootja Bootja Napangardi''',<ref name="SuzieAAPN"/> '''Napangarti''',<ref name="Watson"/> or '''Napangati''')<ref name="Johnson68"/> (c. 1935 – 16 January 2003) was an [[Contemporary Indigenous Australian art|Indigenous artist]] from [[Australia|Australia's]] [[Western Desert cultural bloc|Western Desert]] region. Born south-west of [[Balgo, Western Australia]], in the 1950s Susie Bootja Bootja married artist Mick Gill Tjakamarra, with whom she had a son, Matthew Gill Tjupurrula (also an artist).

Susie Bootja Bootja's painting career followed the establishment of Warlayirti Artists, an [[List of Australian Indigenous art movements and cooperatives|Indigenous art centre]] at Balgo. One of the area's strongest artists, her work was characterised by an expressive style, and has been acquired by major Australian galleries, including the [[Art Gallery of New South Wales]] and [[National Gallery of Victoria]]. She died in 2003.

==Life==
Susie Bootja Bootja was born circa 1935{{#tag:ref|Johnson's reference work gives a date of 1932<ref name="Johnson68">{{cite book|last=Johnson|first=Vivien|title=Aboriginal Artists of the Western Desert: A Biographical Dictionary|publisher=Craftsman House|location=Roseville East, NSW|page=68|year=1994|ISBN=976-8097-81-7}}</ref> the National Gallery of Victoria suggests 1935,<ref name="SusieNGV">{{cite web|url=http://www.ngv.vic.gov.au/collection/pub/artistItemListing?artistID=5928|title=Susie Bootja Bootja Napaltjarri|publisher=National Gallery of Victoria|accessdate=26 September 2009}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> while the ''Oxford Companion to Aboriginal art and culture'' suggests 1936.<ref name="Watson">{{cite book|last=Watson|first=Christine|title=The Oxford Companion to Aboriginal art and culture|editor=Sylvia Kleinert and Margot Neale|publisher=Oxford University Press|location=Oxford|year=2000|page=593|chapter=Gill Tjakamarra, Mick|ISBN=0-19-550649-9 }}</ref>|group=notes}} near Kurtal, or Helena Spring, south-west of [[Balgo, Western Australia]];<ref name="Watson"/> the country is called Kaningarra in her own language, a name that appears as the title of one of her art works.<ref name="AGNSWSusie"/> The ambiguity around the year of birth is in part because Indigenous Australians operate using a different conception of time, often estimating dates through comparisons with the occurrence of other events.<ref name="BirnbergIntro">{{cite book|last=Birnberg|first=Margo|first2=Janusz|last2=Kreczmanski|title=Aboriginal Artist Dictionary of Biographies: Australian Western, Central Desert and Kimberley Region|publisher=J.B. Publishing|pages=10–12|location=Marleston, South Australia|year=2004|isbn=1-876622-47-4}}</ref> While sources vary as to Susie's [[Australian Aboriginal kinship|skin name]] (some indicating Napangarti, others Napaltjarri), the similar birth dates, locations, and work history indicate that all are referring to the one individual.

'[[Napaljarri (skin name)|Napaljarri]]' (in Warlpiri) or 'Napaltjarri' (in Western Desert dialects) is a skin name, one of sixteen used to denote the subsections or subgroups in the [[kinship system]] of central Australian Indigenous people. These names define kinship relationships that influence preferred marriage partners and may be associated with particular totems. Although they may be used as terms of address, they are not surnames in the sense used by Europeans.<ref name="CLC">{{cite web|url=http://www.clc.org.au/People_Culture/kinship/kinship.html|title=Kinship and skin names|work=People and culture|publisher=Central Land Council|accessdate=23 October 2009|archiveurl=http://www.webcitation.org/query?url=http%3A%2F%2Fwww.clc.org.au%2FPeople_Culture%2Fkinship%2Fkinship.html&date=2010-10-11|archivedate=12 October 2010}}</ref><ref name="de Brabander">{{cite book|last=De Brabander|first=Dallas|title=[[Encyclopaedia of Aboriginal Australia]]|editor=David Horton|publisher=Aboriginal Studies Press for the Australian Institute of Aboriginal and Torres Strait Islander Studies|location=Canberra|year=1994|volume=2|page=977|chapter=Sections|isbn=978-0-85575-234-7}}</ref> Thus 'Susie Bootja Bootja' is the element of the artist's name that is specifically hers.

Susie Bootja Bootja was of the Kukatja language group. She married artist Mick Gill Tjakamarra at Old Balgo in the 1950s, and they had a son, Matthew Gill Tjupurrula (born 1960), who also became an artist.<ref name="Watson"/> Susie Bootja Bootja died on 16 January 2003.<ref name="SusieNGV"/>

==Art==
[[File:Balgo from the air.jpg|right|thumb|Alt= an aerial view of a small settlement, showing about seventy rooftops, red dirt roads and a dirt oval, with dry scrubland receding into the distance|Balgo, Western Australia, where Susie Bootja Bootja lived and worked]]

===Background===
Contemporary Indigenous art of the western desert began when Indigenous men at [[Papunya]] began painting in 1971, assisted by teacher [[Geoffrey Bardon]].<ref name="Bardon">{{cite book|last=Bardon|first=Geoffrey|first2=James|last2=Bardon|title=Papunya – A place made after the story: The beginnings of the Western Desert painting movement|publisher=Miegunyah Press|location=University of Melbourne|year=2006|ISBN=978-0-522-85434-3 }}</ref> Their work, which used acrylic paints to create designs representing body painting and ground sculptures, rapidly spread across Indigenous communities of central Australia, particularly following the commencement of a government-sanctioned art program in central Australia in 1983.<ref name="Dussart06">{{cite journal|last=Dussart|first=Francoise|year=2006|title=Canvassing identities: reflecting on the acrylic art movement in an Australian Aboriginal settlement|journal=Aboriginal History|volume=30|pages=156–168}}</ref> By the 1980s and 1990s, such work was being exhibited internationally.<ref name="Morphy99">{{cite book|last=Morphy|first=Howard|title=Aboriginal Art|publisher=Phaidon|location=London|year=1999|pages=261–316|ISBN=0-7148-3752-0}}</ref> The first artists, including all of the founders of the [[Papunya Tula]] artists' company, had been men, and there was resistance amongst the Pintupi men of central Australia to women painting.<ref name="Strocchi06">{{cite journal|last=Strocchi|first=Marina|year=2006|title=Minyma Tjukurrpa: Kintore / Haasts Bluff Canvas Project: Dancing women to famous painters|journal=Artlink|volume=26|issue=4}}</ref> However, there was also a desire amongst many of the women to participate, and in the 1990s large numbers of them began to create paintings. In the western desert communities such as [[Kintore, Northern Territory|Kintore]], [[Yuendumu, Northern Territory|Yuendumu]], Balgo, and on the [[outstation movement|outstations]], people were beginning to create art works expressly for exhibition and sale.<ref name="Morphy99"/> [[List of Australian Indigenous art movements and cooperatives|Art centres]] were important to this widespread creation of art works.<ref name="Wright">{{cite book|last=Wright|first=Felicity |first2=Frances|last2=Morphy|title=The Art & Craft Centre Story|publisher=Aboriginal and Torres Strait Islander Commission|location=Canberra|date=1999–2000|volume=1|isbn=0-642-70453-8}}</ref>

===Career===
The Balgo community did not establish an art centre for more than ten years after their colleagues at Papunya, with artistic activities commencing when an adult education centre was opened in 1981.<ref name="JohnsonD">{{cite book|last=Johnson|first=Vivien|title=Aboriginal Artists of the Western Desert: A Biographical Dictionary|chapter=Domino effects: the spread of Western Desert art in the '80s|publisher=Craftsman House|location=Roseville East, NSW|pages=13–49|year=1994|ISBN=976-8097-81-7}}</ref> However once Warlayirti Artists was set up, the community went on to become one of Australia's most successful Indigenous art centres.<ref name="AGNSWSusie"/><ref name="Senate11">{{cite book|last=Australia. Senate. Standing Committee on Environment, Communications, Information Technology and the Arts |title=Indigenous Art: Securing the Future: Australia’s Indigenous visual arts and craft sector |publisher=Parliament of Australia |location=Canberra |date=June 2007 |pages=11 |isbn=978-0-642-71788-7 |url=http://www.aph.gov.au/senate/committee/ecita_ctte/completed_inquiries/2004-07/indigenous_arts/report/report.pdf |deadurl=yes |archiveurl=https://web.archive.org/web/20110415222218/http://www.aph.gov.au/Senate/committee/ecita_ctte/completed_inquiries/2004-07/indigenous_arts/report/report.pdf |archivedate=15 April 2011 }}</ref> Painting at the centre is a sociable, communal activity,<ref name="JohnsonD"/> and Susie Bootja Bootja would reguarlly collaborate with other painters, including her husband.<ref name="Johnson68"/>

Susie Bootja Bootja was represented by Warlayirti artists at Balgo,<ref>{{cite web|url=http://www.balgoart.org.au/art_centre/full_list_of_artists.htm|title=Full list of artists|publisher=Warlayirti Artists|accessdate=10 September 2009}}</ref> where she was living and working in the 1990s.<ref name="Johnson68"/> She was one of the strongest painters at Balgo.<ref name="JohnsonD"/> The work of Balgo artists such as Susie Bootja Bootja, and her fellow artists including Sunfly Tjampitjin and Wimmitji Tjapangarti, are characterised by an expressive style, involving "linked dotting and blurred forms and edges".<ref name="Desert art217">{{cite book|last=Johnson|first=Vivien|title=The Oxford Companion to Aboriginal Art and Culture|editor=Sylvia Kleinert and Margo Neale|publisher=Oxford University Press|location=Melbourne|year=2000|page=217|chapter=Desert art|ISBN=0-19-550649-9 }}</ref>

Works by Susie Bootja Bootja are held by the Art Gallery of New South Wales,<ref name="AGNSWSusie">{{cite web|url=http://collection.artgallery.nsw.gov.au/collection/results.do?id=154762&db=object&keyword-0=Susie+Bootja+Bootja+Napaltjarri&field-0=simpleSearchObject&view=detail&searchMode=simple|title=Susie Bootja Bootja Napaltjarri – Kaningarra|year=2003|work=Aboriginal and Torres Strait Islander Art > Paintings|publisher=Art Gallery of New South Wales|accessdate=25 November 2009}}</ref> the National Gallery of Victoria,<ref name="SusieNGV"/> and the [[Flinders University]] Art Museum Collection.<ref name="SuzieAAPN">{{cite web|url=http://www.aboriginalartprints.com.au/indigenous_artists_details.php?artist_id=12|title=Suzie Bootja Bootja (Napangarti)|publisher=Australian Art Print Network|accessdate=26 September 2009}}</ref> She is also represented in major private collections, such as Nangara (also known as the Ebes Collection),<ref name="Nangara">{{cite web|url=http://www.nangara.com/collection/artists.htm|title=The artists|publisher=Nangara: the Australian Aboriginal art exhibition|accessdate=2 July 2009}}</ref> the Holmes à Court Collection and the Morven Estate.<ref name="SuzieAAPN"/> Works by both Susie Bootja Bootja and her husband were included in a 1991 exhibition 'Yapa: Peintres Aborigenes de Balgo et Lajamanu' in Paris, and in 'Daughters of the Dreaming' at the [[Art Gallery of Western Australia]] in 1997.<ref name="Watson"/> Her paintings feature in Christine Watson's 2003 book, ''Piercing the Ground: Balgo Women's Image Making and Relationship to Country''.<ref name="Watson"/>

Susie Bootja Bootja helped choose the site for, and participated in, a major ceremony for a 1993 [[Australian Broadcasting Corporation]] documentary film, ''Milli Milli''. The ceremony, called Wati Kutjarra (Two men) Dreaming, was performed with others including fellow artist [[Peggy Rockman Napaljarri]].<ref>{{cite journal|last=Glowczewski|first=Barbara|year=2004|title=Piercing the Ground: Balgo Women's Image Making and Relationship to Country [by Watson, Christine (2003)]: (review article)|journal=Australian Aboriginal Studies|issue=2|page=105}}</ref>

==Collections==
*[[Art Gallery of New South Wales]]<ref name="AGNSWSusie"/>
*[[Flinders University Art Museum]] Collection
*[[National Gallery of Victoria]]
* [[Kluge-Ruhe Aboriginal Art Collection]], [[University of Virginia]]
*[[Janet Holmes à Court|Holmes à Court collection]]<ref name="Johnson68"/>

==Notes==
<references group="notes"/>

==References==
{{reflist|colwidth=30em}}

==External links==
{{Central and Western Desert artists}}
{{Good article}}

{{DEFAULTSORT:Bootja Napaltjarri, Susie Bootja}}
[[Category:1935 births]]
[[Category:2003 deaths]]
[[Category:Australian Aboriginal artists]]
[[Category:Australian women painters]]
[[Category:Australian painters]]
[[Category:20th-century Australian painters]]
[[Category:20th-century women artists]]