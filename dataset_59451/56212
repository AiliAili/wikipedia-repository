{{no ref|date=July 2015}}
{{italic title}}
{{Infobox Journal
| title        = Children's Literature
| cover        = [[Image:Childrens literature.gif]]
| editor       = Michelle Ann Abate, Julie Pfeiffer
| discipline   = [[Children's literature]]
| language     = English
| abbreviation = 
| publisher    = [[Johns Hopkins University Press]]
| country      = United States
| frequency    = Annually
| history      = 1972-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = https://web.archive.org/web/20110702180457/http://chla.wikispaces.com:80/Childrens+Literature
| link1        = http://muse.jhu.edu/journals/childrens_literature/
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 51540302
| LCCN         = 
| CODEN        = 
| ISSN         = 0092-8208  
| eISSN        = 1543-3374
}}
'''''Children’s Literature''''' is an [[academic journal]] and annual publication of the [[Modern Language Association]] and the [[Children’s Literature Association]] Division on Children's Literature. The journal was founded in 1972 by Francelia Butler and promotes a scholarly approach to the study of [[Children's literature|children’s literature]] by printing theoretical articles and essays, as well as book reviews. The publication is currently edited by Amanda Cockrell, of [[Hollins University]] in Roanoke, Virginia. The current editor in chief is R. H. W. Dillard.

''Children's Literature'' is published annually in May by the [[Johns Hopkins University Press]]. Each issue has an average length of 300 pages.

==See also==
{{Portal|Children's literature}}
*[[Children's literature criticism|Children’s literature criticism]]
*[[Children's literature periodicals|Children’s literature periodicals]]

== External links ==
* {{Official website|https://web.archive.org/web/20110702180457/http://chla.wikispaces.com:80/Childrens+Literature}}
*[http://www.hollins.edu/grad/childlit/journal.htm ''Children’s Literature'' on the Hollins University website]
*[http://www.press.jhu.edu/journals/childrens_literature/ ''Children’s Literature'' on the JHU Press website]
*[http://muse.jhu.edu/journals/childrens_literature/ ''Children’s Literature'' ] at [[Project MUSE]]

[[Category:Children's literature criticism]]
[[Category:American literary magazines]]
[[Category:Publications established in 1972]]
[[Category:Annual journals]]
[[Category:English-language journals]]
[[Category:Johns Hopkins University Press academic journals]]