{{Infobox journal
| title = Criminal Justice And Behavior
| cover = [[File:Criminal Justice and Behavior.tif]]
| editor = Curt R. Bartol 
| discipline = [[Criminology]]
| former_names = 
| abbreviation = Crim. Justice Behav.
| publisher = [[SAGE Publications]]
| country =
| frequency = Monthly
| history = 1973-present
| openaccess = 
| license = 
| impact = 1.708
| impact-year = 2011
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200960
| link1 = http://cjb.sagepub.com/content/current
| link1-name = Online access
| link2 = http://cjb.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 1793415
| LCCN = 74643412 
| CODEN = CJBHAB
| ISSN = 0093-8548
| eISSN =  1552-3594 
}}

'''''Criminal Justice and Behavior ''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers research in the fields of [[psychology]] and [[criminology]]. The [[editor-in-chief]] is Curt R. Bartol. It was established in 1974 and is currently published by [[SAGE Publications]] in association with the [[American Association for Correctional and Forensic Psychologists]] and the [[International Association for Correctional and Forensic Psychology]].

== Abstracting and indexing ==
''Criminal Justice and Behavior '' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 1.708, ranking it 9 out of 50 journals in the category "Criminology & Penology"<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Criminology & Penology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> and 43 out of 109 journals in the category "Psychology, Clinical".<ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Psychology, Clinical |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://cjb.sagepub.com/}}
* [http://www.ia4cfp.org/ International Association for Correctional and Forensic Psychology]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Criminal justice journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1973]]