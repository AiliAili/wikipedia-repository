<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = An-30
 |image = File:An-30 - RA-26226.jpg
 |caption = 
}}{{Infobox Aircraft Type
 |type = [[Cartography|Aerial cartography Aircraft]] and [[reconnaissance aircraft|reconnaissance]] and [[military transport aircraft|transport]]
 |manufacturer = [[Antonov]]
 |designer = [[Beriev]]
 |first flight = 21 August 1967<ref name='prod'>''The prototype aircraft (a converted An-24 designated An-24FK) first flew on 21 August 1967. The first production An-30 first flew in 1974</ref><ref name='p75'>Gordon, Komissarov and Komissarov 2003, p. 75.</ref>
 |introduced = July 1968
 |retired =
 |produced = [[1971 in aviation|1971]]–[[1980 in aviation|1980]]<ref name='p75' />
 |number built = 123<ref name='p75' />
 |status = Limited service
 |unit cost =
 |primary user = [[Ukrainian Air Force]]
 |more users = [[Bulgarian Air Force]]<br />[[Romanian Air Force]]
 |developed from = [[Antonov An-24]]
 |variants with their own articles = 
}}
|}

The '''Antonov An-30''' ([[NATO reporting name]]: '''Clank'''), is a development of the [[Antonov An-24|An-24]] designed for aerial [[cartography]].

==Development==

The first [[aerial survey]] version of the [[Antonov An-24]] was designed by the [[Beriev]] [[OKB]] and designated '''An-24FK'''. The FK stood for ''fotokartograficheskiy'' (photo mapping).<ref name ='p73'>Gordon, Komissarov and Komissarov 2003, p. 73.</ref> The prototype was converted from a production An-24A at Beriev's No. 49 construction shop during 1966. The An-24FK made its first flight on 21 August 1967, with state acceptance trials being completed in 1970 and civil certification completed in 1974. Redesignated '''An-30''', production began in 1971 at the [[Antonov]].<ref name='p75' />  123 production An-30s were manufactured between 1971 and 1980<ref name ="gunston">{{Cite book|author=Gunston, Bill|authorlink =Bill_Gunston|title=The Osprey Encyclopedia of Russian Aircraft from 1875 – 1995|publisher=Osprey Aerospace|year=1995|isbn= 1-85532-405-9}}</ref> in Kiev in 2 main versions.

===Total production===
{| class="wikitable" style="text-align: right; font-size: 96%;"
|-
!Total Production<ref name ='>{{cite web|url=http://russianplanes.net/planelist/Antonov/An-30|title=✈ russianplanes.net ✈ наша авиация|publisher=}}</ref>{{vs|date=November 2015}}<ref>{{cite web|title=Antonov An -30 Aerial Car Aerial Cartography Aircraft - Air Force Technology|url=http://www.airforce-technology.com/projects/antonov-an30/|website=Air Force Technology|publisher=Kable Intelligence LTD|accessdate=9 November 2015}}{{Unreliable source?|reason=domain on WP:BLACKLIST|date=August 2016}}</ref>!! 1979 !! 1978 !! 1977 !! 1976 !! 1975 !! 1974 !! 1973 
|-
||'''124'''||8||13||27||24||27||17||8
|}

==Design==
[[File:Lukaviatrans Antonov An-30.jpg|thumb|right|[[Lukaviatrans]] An-30A]]

The Antonov An-30 is a derivative of the An-24 fitted with an entirely new fuselage forward of frame 11. The fuselage nose is extensively glazed, reminiscent of the [[Boeing B-29]]. Housed within the new nose section is the navigator and precise navigational equipment, including an optical sight for ensuring accuracy of aerial photography.<ref name='p73' /> To enable accurate and repeatable survey flights, standard equipment for the An-30 included computer flight path control technology.<ref name=obs>{{Cite book |author= Green, W |title= The Observer's Book of Aircraft (25th ed.) |publisher= Frederick Warne & Co |year= 1976 |isbn= 0-7232-1553-7}}</ref> This additional equipment replaced the radar. The positioning of the new navigational equipment required the flightdeck to be raised by 41&nbsp;cm in comparison to the An-24,<ref name='p73' /> giving the aircraft its other main feature, a hump containing the cockpit, similar to the [[Boeing 747]].

The radio operator and flight engineer sat in the first cabin aft of and below the flightdeck. The mission equipment was located further aft, in a cabin featuring five camera windows in the floor. Each camera window could be closed with covers to protect the glass panels. The covers were located in special fairings protruding from the fuselage underside. In the normal aerial photography role, four or five cameras were carried aboard. Three cameras were mounted vertically, intended for mapping purposes. The remaining two cameras were pointed at an angle of 28° on each side of the aircraft, for oblique photography. The same fuselage compartment contained workstations for two camera operators and a crew rest area.<ref name ='p74'>Gordon, Komissarov and Komissarov 2003, p. 74.</ref>

The aircraft's cameras could be used between 2,000 and 7,000 m (6,500 and 23,000&nbsp;ft) and the scale of the resultant photographs was between 1:200,000 and 1:15,000,000.<ref name='p75' /> The aircraft was supplied with four or five cameras.

The An-30 was powered by two [[Ivchenko AI-24|Ivchenko AI-24VT]] turboprops with a take-off rating of 2,820 ehp.<ref name='p75' />

==Operational history==
[[File:Ukrainian an 30.jpg|thumb|right|Ukrainian An-30 [[Ukrainian Air Force]]]]
As well as its principal use as a survey aircraft, it has also been used by Bulgaria, Czech Republic, Romania, Russia and Ukraine to carry out surveillance under the [[Treaty on Open Skies|Open Skies Treaty]].

The An-30 has also been used as a [[weather control]] aircraft as the '''An-30M'''. Some have been fitted with frozen tanks of [[carbon dioxide]] to be ejected into the sky to form artificial [[rain]] [[cloud]]s. These An-30s have also been put to use to avoid crop-damaging [[hail]]storms and also to maintain good weather for, as examples, new airplane maiden flights, important parades like the 1st of May and the 850th anniversary of [[Moscow]] in September 1997.<ref name = "brasseys">{{cite book | editor = M J H Taylor | title = Brassey's World Aircraft & Systems Directory 1999/2000 Edition | year = 1999 | publisher = Brassey's | isbn = 1-85753-245-7 }}</ref>

Between 1971 and 1980 a total of 115 aircraft were built and 23 were sold abroad to Afghanistan, Bulgaria, China, Cuba, Czechoslovakia, Mongolia and Vietnam.

An-30s completely mapped Afghanistan in 1982, with [[List of Soviet aircraft losses in the Soviet war in Afghanistan|one shot down]] by a [[MANPADS]] during  an aerial photography flight in the Kabul area south of the Panjshir Valley on 11 March 1985. 
Cuban An-30s saw active service in Angola in 1987.

On 22 April 2014, a Ukrainian An-30 was hit by small arms fire while on a surveillance mission over the town of Sloviansk in Eastern Ukraine, which was being held by pro-Russian separatists. The plane landed safely with minor damage.<ref>{{cite web|url=http://www.bbc.com/news/world-europe-27106630|title=Ukraine crisis: Biden says Russia must 'start acting' - BBC News|publisher=}}</ref> On 6 June 2014, a Ukrainian An-30B was shot down near the city of [[Sloviansk|Slavyansk]] in eastern Ukraine, reportedly by a [[MANPADS]] fired by local separatists.<ref>{{cite web|url=http://tvzvezda.ru/news/vstrane_i_mire/content/201406061920-pjzk.htm|title=Самолет-разведчик сбили над Славянском ополченцы - Телеканал «Звезда»|publisher=}}</ref>

== Accidents ==
On 23 May 2012 Russian Open Skies Antonov-30 caught fire during an emergency landing at an airport outside the Czech city of Caslav. According to unconfirmed reports, the accident occurred because the crew was unable to extend the landing gear. Seven passengers were injured, out of 14 Russian and 9 Czech citizens on board.<ref>{{cite web|url=http://english.ruvr.ru/2012_05_23/75706568/|title=Another Russian plane tragedy|publisher=}}</ref>

== Variants ==
;An-24FK
:The sole prototype converted from an An-24B with a navigators station in an extensively glazed nose and elevated cockpit to give clearance for mission equipment.

;An-30A
:Version designed for civilian aviation. 65 were delivered to the Soviet Ministry of Civil Aviation, 6 to other Soviet civil organisations. 18 An-30As were built for export, 7 of which were delivered to [[China]].<ref name ='p77'>Gordon, Komissarov and Komissarov 2003, p. 77.</ref>

;An-30B
:Version designed for the Soviet Air Force. 26 built.  Main differences from An-30A was the avionics fit. Most An-30Bs were retro-fitted with chaff/flare dispensers.<ref name ='p80'>Gordon, Komissarov and Komissarov 2003, p. 80.</ref>

;An-30D ''Sibiryak''
:Long range version of the An-30A with increased fuel capacity, developed in 1990. 5 aircraft were converted to An-30Ds. All were based at [[Myachkovo]] airfield near [[Moscow]]. This variant was used for ice monitoring, fisheries monitoring and as a transport aircraft. It had improved communication equipment, including a data-link system. Rescue equipment was also carried on board.<ref name ='p83'>Gordon, Komissarov and Komissarov 2003, p. 83.</ref>

;An-30FG
:[[Czech Republic|Czech]] designation for the single [[Czech Air Force]] An-30, after being retro-fitted with a western weather radar.<ref name='p83' />

;An-30M ''Meteozashchita''
:Version equipped for weather research. It can spray dry ice into the atmosphere for weather control duties. The dry ice was stored in 8 containers per 130 kg instead of the photographic equipment.

;An-30R
:A production An-30 CCCP-30055/RA-30055(c/n1101) converted to a NBC reconnaissance aircraft with air sampling pods under the forward fuselage and other sensors for monitoring nuclear, biological and chemical warfare by-products. A second example, 30080, was acquired by the VVS, differing in having only one sampling pod on the port pylon and provision for dropping large flare bombs from the starboard pylon. An-30R RA-30055 was used for monitoring the plume from the [[Chernobyl disaster|Chernobyl]] No.4 nuclear reactor fire and became permanently radio-active in the process, being withdrawn from use immediately afterwards.

== Operators ==
{{Acopmap|An-30|Military An-30 operators}}
[[File:Antonov An-30.JPG|thumb|[[Bulgarian Air Force]] An-30]]
[[File:An30.jpg|thumb|An-30 aircraft]]

=== Military operators ===
;{{BUL}}
*[[Bulgarian Air Force]] <ref>{{cite web|url=http://pan.bg/view_article-4-30161-V-DOLNA-MITROPOLIQ-POLETI-POLETI-POLETI.html|title= Flying Flying  - Scramble|work=scramble.nl|accessdate=19 May 2015}}</ref><ref>{{cite web|url=http://www.pan.bg/view_article-1-335712-Uspeshno-oblitane-na-An-30-za-udylzhavane-na-resursa.html|title=Успешно облитане на Ан-30 за удължаване на ресурса -  Авиация - Pan.bg|publisher=}}</ref> 
;{{ROM}}
*[[Romanian Air Force]] – 3 operated from 1976; currently 2<ref name="Romanian AFE">{{cite web|title=Romanian Armed Forces Equipment|url=http://www.armedforces.co.uk/Europeandefence/edcountries/countryromania.htm|work=European Defense Inventory|publisher=Armed Forces.co.uk|accessdate=22 April 2014}}</ref>
;{{RUS}}
*[[Russian Air Force]]
;{{UKR}}
*[[Ukrainian Air Force]]

==== Former Military operators ====
;{{AFG}}
*[[Afghan Air Force]] received an An-30 in 1985.
;{{CUB}}
*[[Cuban Air Force]]
;{{CZE}}
*[[Czech Air Force]] retired their An-30 in 2003.
;{{CZS}}
*[[Czechoslovakian Air Force]]
;{{MNG}}
*[[Mongolian Air Force]]
;{{PRC}}
*[[People's Liberation Army Air Force]]
;{{USSR}}
*[[Soviet Air Force]]
;{{VIE}}
*[[Vietnam People's Air Force]]

=== Civil operators ===
;{{Flagu|Congo}}
*[[Aero-Fret]]

==== Former civil operators ====
;{{PRC}}
*[[Civil Aviation Administration of China]]
;{{MGL}}
*[[MIAT Mongolian Airlines]]
;{{RUS}}
*[[Moskovia Airlines]]
*[[Lukiaviatrans]]
*[[Myachkovo Air Services]] 
*[[Novosibirsk Air Enterprise]] 
*[[Polet Airlines]] 
*[[Practical Geodinamics Center]] 
;{{UKR}}
*[[ARP 410 Airlines]] 
*[[Ukraine National Airlines]] 
;{{VIE}}
*[[Vietnam Air Service Company]]
;{{SUD}}

==Specifications (An-30)==
{{Aircraft specifications

<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->

<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). 
If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")" and start a new, fully formatted line beginning with * -->
|ref=Jane's All The World's Aircraft 1988–89<ref name="janes">{{cite book
 | editor = J W R Taylor
 | title = Jane's All The World's Aircraft,1988–89
 | year = 1988
 | publisher = Jane's Information Group
 | isbn =0-7106-0867-5
 }}</ref>
|crew=7
|capacity=
|length main= 24.26 m
|length alt= 79 ft 7 in
|span main= 29.20 m
|span alt= 95 ft 9½ in
|height main= 8.32 m
|height alt= 27 ft 3½ in
|area main= 75 m²
|area alt= 807 ft²
|aspect ratio=11.4:1
|airfoil=
|empty weight main= 15,590 kg
|empty weight alt= 34,370 lb
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt=  
|max takeoff weight main= 23,000 kg
|max takeoff weight alt= 50,706 lb
|more general=
|engine (prop)= [[Ivchenko AI-24|ZMKB Progress AI-24T]] 
|type of prop= turboprops
|number of props=2
|power main= 2,103 kW
|power alt= 2,803 ehp
|power original=
|max speed main= 540 km/h
|max speed alt= 291 knots, 335 mph
|cruise speed main= 430 km/h
|cruise speed alt= 232 knots,  267 mph
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 2,630 km
|range alt= 1,420 nm,  1,634 mi
|more range= (with no reserves)
|ceiling main= 8,300 m
|ceiling alt= 27,230 ft
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt= 
|thrust/weight=
|power/mass main= 
|power/mass alt= 
|more performance=
|armament=
|avionics= 5 positions for large cameras.  Other survey equipment can be fitted.
}}

== See also ==
{{Aircontent
|related=
* [[Antonov An-24]]
|similar aircraft=
* [[Aeritalia G.222]]
|sequence=
|lists=
* [[List of military aircraft of the Soviet Union and the CIS]]
|see also=

}}

== References ==
* [http://english.pravda.ru/photo/report/An_30-3227 Photo gallery An-30 aircraft at Pravda.Ru]

=== Notes ===
{{reflist}}
http://russianplanes.net/planelist/Antonov/An-30

=== Bibliography ===
{{refbegin}}
* Yefim Gordon, Dmitriy Komissarov and Sergey Komissarov (2003) ''Antonov's Turboprop Twins''. Hinckley, UK: Midland Publishing. ISBN 1-85780-153-9
{{refend}}

== External links ==
{{commons category|Antonov An-30}}
* [http://www.airliners.net/search/photo.search?aircraft_genericsearch=Antonov%20An-30&distinct_entry=true Pictures of An-30]

{{Antonov aircraft}}

[[Category:Antonov aircraft|An-030]]
[[Category:Soviet special-purpose aircraft 1960–1969]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Turboprop aircraft]]