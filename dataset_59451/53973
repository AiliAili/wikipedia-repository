{{Coord|-35.052922|138.618602|display=title}}
{{Refimprove|date=October 2008}}
{{Infobox school
| name        = Aberfoyle Park High School
| image       = [[File:Aberfoyle Park High School Logo.png|170px]]
| motto       = ''Technology Focus School''
| established = 1984
| type        = [[Public school (government funded)|Public]]
| principal   = Marion Coady <ref>http://intra.aphs.sa.edu.au/node/14</ref>
| city        = [[Aberfoyle Park]]
| state       = [[South Australia]]
| country     = [[Australia]]
| nickname  = The Hub
| campus      = Urban
| enrolment   = 1130<ref name="intra.aphs.sa.edu.au">[http://intra.aphs.sa.edu.au/node/13 APHS Context Statement 2008]</ref>
| colours     = Maroon, Black and White (Yr 8–11) {{color box|maroon}} {{color box|black}} {{color box|white}}<br>White and Black (Yr 12) {{color box|white}} {{color box|black}}
| homepage    = http://intra.aphs.sa.edu.au/
}}

'''Aberfoyle Park High School''' is one of the largest [[state school|public secondary schools]] in [[South Australia]]. Its facilities include the Community Library, Performing Arts Centre, Gymnasium, and the Information and Communication Technology facilities. The school focuses on extending gifted students through the Ignite (formerly SHIP) programme.

Aberfoyle Park High School has become a centre for international students, resulting in strong relationships with a number of schools in other countries.

==History==
Aberfoyle Park High School was built in 1983 and opened in 1984 to service the growing suburbs of [[Aberfoyle Park, South Australia|Aberfoyle Park]], [[Happy Valley, South Australia|Happy Valley]], [[Flagstaff Hill, South Australia|Flagstaff Hill]] and other surrounding suburbs. An extra two buildings were completed in 1984 for Science and English / Computing studies at the school. In 1986, [[Elizabeth II]] visited the school inspecting the facilities and meeting the students. In 1987 a new Computing Facility was opened providing computers for the students. The school grew throughout the 1990s to a peak of over 1500 students and in the late 1990s the school expanded to include the former primary school site, south-east of the original site. 

Aberfoyle Park High School celebrated its 20th Anniversary in 2004.

==Partnerships==

In 2008, Aberfoyle Park High School entered into a partnership with [[defense contractor]], [[Raytheon| Raytheon Australia]] to provide $450,000 funding over the next 3 years for purchase of new computers for [[Ignite (youth program)|IGNITE]], Science and Maths students at the school. The partnership was launched at the school in November 2008 by the [[Premier of South Australia|Premier]] [[Mike Rann]] and Raytheon Australia managing director, Ron Fisher.<ref>http://hills-and-valley-messenger.whereilive.com.au/news/story/future-engineers/</ref>

==Notable alumni==
'''Sport'''
* [[Adam Cooney]] – former professional [[Australian rules football]]er for the [[Western Bulldogs]] (2004–2014), {{AFL Ess}} (2015–2016) and [[2008 Brownlow Medal]]list.<ref>{{cite web|url=http://www.afl.com.au/Season2007/News/NewsArticle/tabid/208/Default.aspx?newsId%3D35045 |title=Archived copy |accessdate=2008-11-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20110524201751/http://www.afl.com.au/Season2007/News/NewsArticle/tabid/208/Default.aspx?newsId=35045 |archivedate=2011-05-24 |df= }}</ref>
* [[Nathan Eagleton]] – former professional Australian rules footballer for {{AFL PA}} (1997–1999) and Western Bulldogs (2000–2010).
* Ben Hale - Australian [[Korfball]] team representative including 2007 World Championships.
* Brad Marks - Australian [[Korfball]] team representative including 2007 World Championships.
* [[Ben Marsh]] – former Australian rules footballer for {{AFL Ade}} (1998–2003) and {{AFL Ric}} (2004).
* [[Brenton Sanderson]] – former coach of Adelaide (2012–2014) and former professional Australian rules footballer for Adelaide (1992–1993), {{AFL Col}} (1994) and {{AFL Gee}} (1995–2005).
* [[Beau Waters]] – former professional Australian rules footballer for {{AFL WC}} (2004–2015).
* [[Caleb Daniel]] – Australian rules footballer for the Western Bulldogs (2015–present).
'''Music'''
* [[Andy Strachan]] – Drummer for [[The Living End]], multiple ARIA Award winner (2002–present).
* Michael Green – Multi-instrumentalist, vocalist [[The Audreys]], multiple ARIA Award winner (2003–2010).
* Kynan Johns - Conductor, currently Director of Orchetras at [[Rutgers University]]

==References==
{{Reflist}}

==External links==
*[http://intra.aphs.sa.edu.au Aberfoyle Park High School Website]

[[Category:High schools in South Australia]]
[[Category:Public schools in South Australia]]
[[Category:Special interest high schools in South Australia]]
[[Category:School buildings completed in 1984]]