{{Infobox journal
| title = Ledger
| cover = 
| editor = Christopher E. Wilmer
Peter R. Rizun
| discipline = [[Cryptocurrency|Cryptocurrencies]]
| abbreviation = Ledger
| publisher =[[D-Scribe Digital Publishing]] (cosponsored by the [[University of Pittsburgh Press]])
| country = United States
| frequency = Biannual
| history = 2016-present
| openaccess = Yes
| license =
| website = http://www.ledgerjournal.org/ojs/index.php/ledger/index
| link1 = http://www.ledgerjournal.org/ojs/index.php/ledger/issue/current
| link1-name = Current issue
| link2 = http://www.ledgerjournal.org/ojs/index.php/ledger/issue/archive
| link2-name = Online archive
| OCLC = 910895894
| eISSN = 2379-5980
}}
'''''Ledger''''' is the first [[peer-reviewed]] [[academic journal]] covering research on all aspects of [[cryptocurrency|cryptocurrencies]], such as [[bitcoin]], and [[Blockchain (database)|blockchain technology]],<ref name="Extance2015">{{cite journal |last1=Extance |first1=Andy |title=The future of cryptocurrencies: Bitcoin and beyond |journal=[[Nature (journal)|Nature]] |volume=526 |issue=7571 |year=2015 |pages=21–23 |doi=10.1038/526021a}}</ref> including [[mathematics]], [[computer science]], [[engineering]], [[law]], [[economics]] and [[philosophy]].<ref name="Hertig2015"/>

==Overview==
''Ledger'' is an [[open access]] journal published by the [[Hillman Library#University Library System|University Library System]] (ULS) of the [[University of Pittsburgh]] and is cosponsored by the [[University of Pittsburgh Press]].<ref>{{cite web|last1=Dotson|first1=Kyt|title=The University of Pittsburgh is now home to Ledger: the first Bitcoin-only academic journal|url=http://siliconangle.com/blog/2015/09/15/the-university-of-pittsburgh-is-now-home-to-ledger-the-first-bitcoin-only-academic-journal/|website=siliconangle.com|accessdate=12 January 2017|archiveurl=https://web.archive.org/web/20170112102850/http://siliconangle.com/blog/2015/09/15/the-university-of-pittsburgh-is-now-home-to-ledger-the-first-bitcoin-only-academic-journal/|archivedate=12 January 2017|date=16 September 2015}}</ref> The journal expects to release full-publications twice a year and additional articles throughout.<ref name="Castillo2016"/> Individuals such as [[Michael Kumhof]], Senior Research Advisor at the [[Bank of England]],<ref>{{cite web|last1=Palmer|first1=Daniel|title=Bank of England Economist Joins Ledger Journal Editorial Board|url=http://www.coindesk.com/bank-england-economist-editorial-ledger/|website=coindesk.com|publisher=Coindesk|accessdate=11 January 2017|archiveurl=https://web.archive.org/web/20170111132829/http://www.coindesk.com/bank-england-economist-editorial-ledger/|archivedate=11 January 2017|date=2 February 2016}}</ref> and [[Vitalik Buterin]], co-founder of [[Ethereum]], as well as academics from a variety of institutions including [[Oxford university|Oxford]], [[Stanford University|Stanford]] and [[MIT]] currently make up the editorial team.<ref>{{cite web|title=Editorial Team|url=http://ledger.pitt.edu/ojs/index.php/ledger/about/editorialTeam|website=ledger.pitt.edu|publisher=Ledger|archiveurl=https://web.archive.org/web/20170110175224/http://www.ledgerjournal.org/ojs/index.php/ledger/about/editorialTeam|archivedate=10 January 2017}}</ref>

The idea for ''Ledger'' was born out of a discussion between managing editors Peter R. Rizun and Christopher E. Wilmer on the bitcoin forum bitcointalk.org. A call for papers was issued on the 15th September 2015 with the deadline set at 31st December 2015 but following delays in formalising the review process the inaugural issue was not published until December 2016.<ref>{{cite web|last1=Prisco|first1=Giulio|title=First Peer-Reviewed Academic Bitcoin Journal, Ledger, Launches and Issues Call for Papers|url=https://bitcoinmagazine.com/articles/first-peer-reviewed-academic-bitcoin-journal-ledger-launches-issues-call-papers-1442439097|website=bitcoinmagazine.com|accessdate=10 January 2017|archiveurl=https://web.archive.org/web/20170110171453/https://bitcoinmagazine.com/articles/first-peer-reviewed-academic-bitcoin-journal-ledger-launches-issues-call-papers-1442439097|archivedate=10 January 2017|date=10 September 2015}}</ref><ref name="Hertig2015">{{cite web|last1=Hertig|first1=Alyssa|title=Introducing Ledger, the First Bitcoin-Only Academic Journal|url=http://motherboard.vice.com/read/introducing-ledger-the-first-bitcoin-only-academic-journal|website=motherboard.vice.com|publisher=Vice Media|accessdate=10 January 2017|archiveurl=https://web.archive.org/web/20170110172807/http://motherboard.vice.com/read/introducing-ledger-the-first-bitcoin-only-academic-journal|archivedate=10 January 2017|date=15 September 2015}}</ref><ref name="Castillo2016">{{cite web|last1= del Castillo|first1=Michael|title=Ledger Publishes First Volume of Peer-Reviewed Blockchain Research|url=http://www.coindesk.com/ledger-first-volume-blockchain-research/|website=coindesk.com|accessdate=10 January 2017|archiveurl=https://web.archive.org/web/20170110165805/http://www.coindesk.com/ledger-first-volume-blockchain-research/|archivedate=10 January 2017|date=22 December 2016}}</ref><ref>{{cite web|last1=Menezes|first1=Nuno|title=LEDGER – A new Journal for Cryptocurrency Papers|url=http://www.newsbtc.com/2015/11/17/ledger-new-journal-digital-currency-papers/|website=newsbtc.com|accessdate=12 January 2017|archiveurl=https://web.archive.org/web/20170112115251/http://www.newsbtc.com/2015/11/17/ledger-new-journal-digital-currency-papers/|archivedate=12 January 2017|date=17 November 2015}}</ref><ref>{{cite web|last1=Dollentas|first1=Nigel|title=Ledger: First Scholarly Bitcoin Journal Invites Authors|url=http://bitcoinist.com/ledger-first-scholarly-bitcoin-journal-invites-authors/|website=bitcoinist.com|accessdate=12 January 2017|archiveurl=https://web.archive.org/web/20170112120932/http://bitcoinist.com/ledger-first-scholarly-bitcoin-journal-invites-authors/|archivedate=12 January 2017|date=24 September 2015}}</ref>

The journal encourages authors to [[Digital signature|digitally sign]] a [[Hash function|file hash]] of submitted papers, which will then be [[Trusted timestamping|timestamped]] into the bitcoin [[Blockchain (database)|blockchain]]. Authors are also asked to include a personal bitcoin address in the first page of their papers.<ref>{{cite journal|last1=Rizun|first1=Peter R.|last2=Wilmer|first2=Christopher E.|last3=Burley|first3=Richard Ford|last4=Miller|first4=Andrew|title=How to Write and Format an Article for Ledger|journal=Ledger|date=2015|doi=10.5195/LEDGER.2015.1|url=http://ledger.pitt.edu/ojs/public/journals/1/AuthorGuide.pdf|accessdate=11 January 2017|issn=2379-5980|oclc=910895894}}</ref><ref>{{cite web|last1=Nm|first1=Gautham|title=Ledger, Crypto Only Journal for the Academia|url=http://www.newsbtc.com/2015/09/20/ledger-crypto-only-journal-for-the-academia/|website=newsbtc.com|accessdate=12 January 2017|archiveurl=https://web.archive.org/web/20170112114359/http://www.newsbtc.com/2015/09/20/ledger-crypto-only-journal-for-the-academia/|archivedate=12 January 2017|date=20 September 2015}}</ref>

==See also==
<!-- Please respect alphabetical order -->
{{columns-list|colwidth=30em|
* [[Alternative currency]]
* [[Decentralized autonomous organization]]
* [[Digital gold currency]]
* [[Private currency]]
* [[World currency]]
}}

==References==
{{Reflist|30em}}

==External links==
*{{Official website |http://www.ledgerjournal.org/ojs/index.php/ledger/index}}
{{Cryptocurrencies}}
{{Bitcoin}}
{{Portal bar|Cryptography|Economics|Internet|Numismatics}}


[[Category:Academic journals published by universities and colleges]]
[[Category:Biannual journals]]
[[Category:Computer science journals]]
[[Category:Creative Commons-licensed journals]]
[[Category:Cryptography journals]]
[[Category:English-language journals]]
[[Category:Finance journals]]
[[Category:Multidisciplinary academic journals]]
[[Category:Cryptocurrencies]]
[[Category:Financial technology]]
[[Category:Publications established in 2016]]