{{Infobox journal
| cover = [[File:Earth and Planetary Science Letters.gif]]
| editor = T. Elliott, T.M. Harrison, G.M. Henderson, J. Lynch-Stieglitz, B. Marty, Y. Ricard, P. Shearer, C. Sotin, T. Spohn, L. Stixrude| discipline = [[Planetary science]]
| abbreviation = Earth Planet. Sci. Lett.
| publisher = [[Elsevier]]
| country = 
| frequency = 48/year
| history = 1966-present
| openaccess = [[Hybrid open access|Hybrid]]
| license = 
| impact = 4.724
| impact-year = 2013
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/503328/description
| link1 = http://www.sciencedirect.com/science/journal/0012821X
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 1567193
| LCCN = 66009932 
| CODEN = EPSLA2
| ISSN = 0012-821X
| eISSN = 1385-013X
}}
'''''Earth and Planetary Science Letters''''' is a weekly [[peer review|peer-reviewed]] [[scientific journal]] covering research on physical, chemical and mechanical processes of the Earth and other planets, including [[Extrasolar planet|extrasolar]] ones. Topics covered range from deep planetary interiors to atmospheres. The journal was established in 1966 and is published by [[Elsevier]]. The [[editor-in-chief|editors]] are T. Elliott ([[University of Bristol]]), T.M. Harrison ([[University of California, Los Angeles]]), G.M. Henderson ([[University of Oxford]]), J. Lynch-Stieglitz ([[Georgia Institute of Technology]]), B. Marty ([[École Nationale Supérieure de Géologie]]), Y. Ricard ([[Université Claude Bernard]]), P. Shearer ([[University of California, San Diego]]), C. Sotin ([[California Institute of Technology]]), T. Spohn ([[German Aerospace Center]]), and L. Stixrude ([[University College London]]).<ref>{{cite web |url=http://www.journals.elsevier.com/earth-and-planetary-science-letters/editorial-board/ |title=Editorial Board |publisher=Elsevier |work=Earth and Planetary Science Letters |accessdate=2012-12-23}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Bibliography and Index of Geology]]
*[[Bulletin Signalétique]]
*[[Chemical Abstracts Service]]
*[[Current Contents]]
*[[GEOBASE]]
*[[Inspec]]
*[[Meteorological & Geoastrophysical Abstracts]]
*[[Mineralogical Abstracts]]
*[[PASCAL (INIST database)|PASCAL]]
*[[Physikalische Berichte]]
*[[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 4.724.<ref name=WoS>{{cite book |year=2014 |chapter=Earth and Planetary Science Letters |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/503328/description}}

[[Category:Publications established in 1966]]
[[Category:Planetary science journals]]
[[Category:Earth and atmospheric sciences journals]]
[[Category:Elsevier academic journals]]
[[Category:Space science journals]]
[[Category:Geophysics journals]]
[[Category:Meteoritics publications]]