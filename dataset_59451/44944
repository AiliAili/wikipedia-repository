{{Redirect|Reverb}}
{{Refimprove|date=December 2008}}   
 
{{listen|
filename=Reverberation_effect.ogg|
title=Short sample of reverberation effect|
description=Clean signal, followed by different versions of reverberation (with longer and longer decay times).
}}
'''Reverberation''', in [[psychoacoustics]] and [[acoustics]], is the persistence of [[sound]] after a sound is produced.<ref>{{cite book|last=Valente|first=Michael|author2=Holly Hosford-Dunn |author3=Ross J. Roeser |title=Audiology|publisher=Thieme|date=2008|pages=425–426|isbn=978-1-58890-520-8}}</ref> A reverberation, or '''reverb''', is created when a sound or signal is reflected causing a large number of reflections to build up and then decay as the sound is absorbed by the surfaces of objects in the space – which could include furniture, people, and air.<ref>{{cite book|last=Lloyd|first=Llewelyn Southworth|title=Music and Sound|publisher=Ayer Publishing|date=1970|pages=169|isbn=978-0-8369-5188-2}}</ref> This is most noticeable when the sound source stops but the [[reflection (physics)|reflections]] continue, decreasing in [[amplitude]], until they reach zero amplitude.

Reverberation is frequency dependent: the length of the decay, or reverberation time, receives special consideration in the architectural design of spaces which need to have specific reverberation times to achieve optimum performance for their intended activity.<ref>{{cite book|last=Roth|first=Leland M.|title=Understanding Architecture|publisher=Westview Press|date=2007|pages=104–105|isbn=978-0-8133-9045-1}}</ref> In comparison to a distinct echo that is a minimum of 50 to 100&nbsp;[[millisecond|ms]] after the initial sound, reverberation is the occurrence of reflections that arrive in less than approximately 50 ms. As time passes, the amplitude of the reflections is reduced until it is reduced to zero. Reverberation is not limited to indoor spaces as it exists in forests and other outdoor environments where reflection exists.

Reverberation occurs naturally when a person sings, talks or plays an instrument acoustically in a hall or performance space with sound-reflective surfaces.<ref>{{cite book|last1=Davis|first1=Gary|title=The sound reinforcement handbook|date=1987|publisher=Hal Leonard|location=Milwaukee, WI|isbn=9780881889000|page=259|edition=2nd|url=https://books.google.com/books?id=d7ft6F8ZUdcC&pg=PA259|accessdate=February 12, 2016}}</ref> The sound of reverberation is often electronically added to the [[singing|vocals]] of singers in [[sound reinforcement system|live sound systems]] and [[sound recording]]s by using [[effects unit]]s or [[digital delay]] effects.

== {{visible anchor | Reverberation time}} ==
[[File:Reverberation time diagram.svg|thumb|Sound level in a reverberant cavity excited by a pulse, as a function of time (very simplified diagram).]]
The time it takes for a signal to drop by 60&nbsp;dB is the reverberation time.

''RT''<sub>60</sub> is the time required for reflections of a direct sound to decay 60&nbsp;[[Decibel|dB]]. Reverberation time is frequently stated as a single value, if measured as a wide band signal (20&nbsp;Hz to 20&nbsp;kHz), however, being frequency dependent, it can be more precisely described in terms of frequency bands (one octave, 1/3 octave, 1/6 octave, etc.). Being frequency dependent, the reverberation time measured in narrow bands will differ depending on the frequency band being measured. For precision, it is important to know what ranges of frequencies are being described by a reverberation time measurement.

In the late 19th century, [[Wallace Clement Sabine]] started experiments at Harvard University to investigate the impact of absorption on the reverberation time. Using a portable wind chest and organ pipes as a sound source, a [[stopwatch]] and his ears, he measured the time from interruption of the source to inaudibility (a difference of roughly 60&nbsp;dB). He found that the reverberation time is proportional to room dimensions and inversely proportional to the amount of absorption present.

The optimum reverberation time for a space in which music is played depends on the type of music that is to be played in the space. Rooms used for speech typically need a shorter reverberation time so that speech can be understood more clearly.  If the reflected sound from one [[syllable]] is still heard when the next syllable is spoken, it may be difficult to understand what was said.<ref>{{cite web|url=http://www.mcsquared.com/y-reverb.htm|title=So why does reverberation affect speech intelligibility?|publisher=MC Squared System Design Group, Inc|accessdate=2008-12-04}}</ref> "Cat", "Cab", and "Cap" may all sound very similar. If on the other hand the reverberation time is too short, [[tonal balance]] and loudness may suffer. Reverberation effects are often used in [[recording studio|studios]] to add depth to sounds. Reverberation changes the perceived spectral structure of a sound, but does not alter the pitch.

Basic factors that affect a room's reverberation time include the size and shape of the enclosure as well as the materials used in the construction of the room. Every object placed within the enclosure can also affect this reverberation time, including people and their belongings.

=== Sabine equation ===
[[Wallace_Clement_Sabine|Sabine]]'s reverberation equation was developed in the late 1890s in an [[empirical]] fashion. He established a relationship between the ''RT''<sub>60</sub> of a room, its volume, and its total absorption (in [[sabins]]). This is given by the equation:

:<math>RT_{60} = \frac{24 \ln 10^1}{c_{20}} \frac{V}{Sa} \approx 0.1611\,\mathrm{s}\mathrm{m}^{-1} \frac{V}{Sa}</math>.

where <math>c_{20}</math> is the speed of sound in the room (for 20 degrees Celsius), <math>V</math> is the volume of the room in m³, <math>S</math> total surface area of room in m², <math>a</math> is the average absorption coefficient of room surfaces, and the product <math>Sa</math> is the total absorption in sabins.

The total absorption in sabins (and hence reverberation time) generally changes depending on frequency (which is defined by the [[acoustics|acoustic properties]] of the space). The equation does not take into account room shape or losses from the sound travelling through the air (important in larger spaces). Most rooms absorb less sound energy in the lower frequency ranges resulting in longer reverb times at lower frequencies.

Sabine concluded that the reverberation time depends upon the reflectivity of sound from various surfaces available inside the hall. If the reflection is coherent, the reverberation time of the hall will be longer; the sound will take more time to die out.

The reverberation time ''RT''<sub>60</sub> and the [[volume]] ''V'' of the room have great influence on the [[critical distance]] ''d''<sub>c</sub> (conditional equation):
:<math>
d_\mathrm{c} \approx 0{.}057 \cdot \sqrt \frac{V}{RT_{60}}
</math>

where critical distance <math>d_c</math> is measured in meters, volume <math>V</math> is measured in m³, and reverberation time <math>RT_{60}</math> is measured in [[second]]s.

=== Absorption Coefficient ===
The absorption coefficient of a material is a number between 0 and 1 which indicates the proportion of sound which is absorbed by the surface compared to the proportion which is reflected back into the room. A large, fully open window  would offer no reflection as any sound reaching it would pass straight out and no sound would be reflected. This would have an absorption coefficient of 1. Conversely, a thick, smooth painted concrete ceiling would be the acoustic equivalent of a mirror, and would have an absorption coefficient very close to 0.

Sound absorption coefficients of common materials used in buildings are presented in this [http://logacustica.com/en/coeficientes-de-absorcao-sonora-sound-absorption-coefficients/ table].

=== Measurement of reverberation time ===
[[File:RT60 measurement.jpg|thumb|280px|right|Automatically determining T20 value - 5dB trigger - 20dB measurement - 10dB headroom to noise floor.]]
Historically reverberation time could only be measured using a level recorder (a plotting device which graphs the noise level against time on a ribbon of moving paper). A loud noise is produced, and as the sound dies away the trace on the level recorder will show a distinct slope. Analysis of this slope reveals the measured reverberation time. Some modern digital [[sound level meter]]s can carry out this analysis automatically.<ref>[http://www.nti-audio.com/en/functions/reverberation-time-rt60.aspx Measuring Reverberation Time RT60]</ref>

Several methods exist for measuring reverb time.  An impulse can be measured by creating a sufficiently loud noise (which must have a defined cut off point). [[Impulse noise (audio)|Impulse noise]] sources such as a [[Blank (cartridge)|blank]] pistol shot or balloon burst may be used to measure the impulse response of a room.

Alternatively, a [[Colors of noise|random noise signal]] such as [[pink noise]] or [[white noise]] may be generated through a loudspeaker, and then turned off. This is known as the interrupted method, and the measured result is known as the interrupted response.

A two port measurement system can also be used to measure noise introduced into a space and compare it to what is subsequently measured in the space.  Consider sound reproduced by a loudspeaker into a room.  A recording of the sound in the room can be made and compared to what was sent to the loudspeaker.  The two signals can be compared mathematically.  This two port measurement system utilizes a [[Fourier transform]] to mathematically derive the impulse response of the room.  From the impulse response, the reverberation time can be calculated.  Using a two port system allows reverberation time to be measured with signals other than loud impulses.  Music or recordings of other sound can be used.  This allows measurements to be taken in a room after the audience is present.

Reverberation time is usually stated as a decay time and is measured in seconds.  There may or may not be any statement of the frequency band used in the measurement.  Decay time is the time it takes the signal to diminish 60&nbsp;dB below the original sound. It is often difficult to inject enough sound into the room to measure a decay of 60&nbsp;dB, particularly at lower frequencies. If the decay is linear, it is sufficient to measure a drop of 20&nbsp;dB and multiply the time by 3, or a drop of 30&nbsp;dB and multiply the time by 2. These are the so-called T20 and T30 measurement methods.

The concept of Reverberation Time implicitly supposes that the decay rate of the sound is exponential, so that the sound level diminishes regularly, at a rate of so many dB per second. It is not often the case in real rooms, depending on the disposition of reflective, dispersive and absorbing surfaces. Moreover, successive measurement of the sound level often yields very different results, as differences in phase in the exciting sound build up in notably different sound waves. In 1965, [[Manfred R. Schroeder]] published "A new method of Measuring Reverberation Time" in the ''Journal of the Acoustical Society of America''. He proposed to measure, not the power of the sound, but the energy, by integrating it. This made it possible to show the variation in the rate of decay, and to free acousticians from the necessity of averaging many measurements.

== Creating reverberation effects ==
A performer or a producer of live or recorded music often induces reverberation in a work. Several systems have been developed to produce or to simulate reverberation.

===Chamber reverberators===
The first reverb effects created for recordings used a real physical space as a natural [[echo chamber]]. A [[loudspeaker]] would play the sound, and then a [[microphone]] would pick it up again, including the effects of reverb. Although this is still a common technique, it requires a dedicated soundproofed room, and varying the reverb time is difficult.

===Plate reverberators===<!--[[Plate reverb]] redirects here-->
A plate reverb system uses an electromechanical [[transducer]], similar to the driver in a loudspeaker, to create vibrations in a large plate of [[sheet metal]]. The plate’s motion is picked up by one or more [[contact microphone]]s whose output is an audio signal which may be added to the original "dry" signal. In the late 1950s, [[Elektro-Mess-Technik]] (EMT) introduced the EMT 140;<ref>{{cite book |url=https://books.google.com/books?id=00m1SlorUcIC&pg=PA233 |page=233 |title=Handbook of Recording Engineering |last=Eargle |first=John M. |authorlink=John M. Eargle |publisher=Birkhäuser |year=2005 |isbn=0-387-28470-2 |edition=4}}</ref> a {{convert|600|lb|adj=on}} model popular in recording studios, contributing to many hit records such as [[The Beatles|Beatles]] and [[Pink Floyd]] albums recorded at [[Abbey Road Studios]] in the 1960s, and others recorded by [[Bill Porter (sound engineer)|Bill Porter]] in Nashville's [[RCA Studio B]].{{citation needed|date=December 2012}} Early units had one pickup for mono output, later models featured two pickups for stereo use. The reverb time can be adjusted by a damping pad, made from framed acoustic tiles. The closer the damping pad, the shorter the reverb time. However, the pad never touches the plate. Some units also featured a remote control.

===Spring reverberators===
{{redirect|Spring reverb|the album by [[The Big Wu]]|Spring Reverb (album) }}
[[Image:Reverb-1.jpg|thumb|Folded line reverberation device.]]
[[Image:Reverb-2.jpg|thumb|The folded coil spring is visible underside of the reverberation device.]]
A spring reverb system uses a transducer at one end of a spring and a pickup at the other, similar to those used in plate reverbs, to create and capture vibrations within a metal [[spring (device)|spring]].  [[Laurens Hammond]] was granted a patent on a spring-based mechanical reverberation system in 1939.<ref>Laurens Hammond, Electrical Musical Instrument, [http://www.google.com/patents?id=6ZZQAAAAEBAJ U.S. Patent 2,230,836], granted Feb. 4, 1941.</ref> [[Guitar amplifier]]s frequently incorporate spring reverbs due to their compact construction and low cost. Spring reverberators were once widely used in semi-professional recording due to their modest cost and small size.

Many musicians have made use of spring reverb units by rocking them back and forth, creating a thundering, crashing sound caused by the springs colliding with each other. The [[Hammond Organ]] included a built-in spring reverberator, making this a popular effect when used in a rock band.

===Digital reverberators===
Digital reverberators use various [[Digital signal processing|signal processing]] algorithms in order to create the reverb effect. Since reverberation is essentially caused by a very large number of echoes, simple reverberation algorithms use several feedback [[Delay (audio effect)|delay circuits]] to create a large, decaying series of echoes. More advanced digital reverb generators can simulate the time and frequency domain response of a specific room (using room dimensions, absorption and other properties). In a music hall, the direct sound always arrives at the listener's ear first because it follows the shortest path. Shortly after the direct sound, the reverberant sound arrives. The time between the two is called the "pre-delay."

Reverberation, or informally, "reverb," is a standard audio effect used universally in [[digital audio workstations]] (DAWs) and [[Virtual Studio Technology|VST plug-ins]].

==== Convolution reverb ====
{{main article|Convolution reverb}}
Convolution reverb is a process used for digitally simulating reverberation. It uses the mathematical [[convolution]] operation, a pre-recorded audio sample of the [[impulse response]] of the space being modeled, and the sound to be echoed, to produce the effect. The impulse-response recording is first stored in a [[digital signal processor|digital signal-processing]] system. This is then [[convolution|convolved]] with the incoming audio signal to be processed.

== See also ==
*[[Acoustic resonance]]
*[[Audio mixing (recorded music)|Audio mixing]]
*[[Exponential decay]]
*[[Reverberation room]]

==References==
{{Reflist}}

==External links==
*[http://hyperphysics.phy-astr.gsu.edu/hbase/acoustic/reverb.html#c1 Reverberation] - Hyperphysics
*[http://www.ind.rwth-aachen.de/air/# A database of measured room impulse responses to generate realistic reverberation effects]
*[http://www.amplifiedparts.com/tech_corner/spring_reverb_tanks_explained_and_compared# Spring Reverb Tanks Explained and Compared]
*[http://sound.whsites.net/articles/reverb.htm# Care and Feeding of Spring Reverb Tanks]
*[http://www.energy-control-system.com/index.php?option=com_remository&Itemid=57&func=fileinfo&id=16&lang=en# Download possibility for a calculation software to Reverberation Time]
*[http://www.sengpielaudio.com/calculator-RT60.htm Calculation of the reverberation time after Sabine − RT60 decay]

{{Music production}}
{{Music technology}}
{{Acoustics}}

[[Category:Acoustics]]
[[Category:Audio effects]]