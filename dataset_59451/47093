{{Infobox company
 | name     = Cleveland Diesel Engine Division 
 | logo     = 
 | type     = [[Subsidiary]] of [[General Motors]]
 | predecessor = [[Winton Motor Carriage Company#Sale to General Motors|Winton Engine Corporation]]
 | defunct          = 1962
 | fate             = Folded into [[Electro-Motive Diesel|Electro-motive Diesel Division]]
 | slogan   = 
 | foundation       = 1938 
 | location_city    = [[Cleveland]], [[Ohio]]
 | location_country = [[United States]]
 | key_people       = [[Charles Kettering]] and [[Alexander Winton]]
 | num_employees    = 5,000 during World War II
 | industry         = [[Marine propulsion#Reciprocating diesel engines|Marine Diesel Engines]]
 | products         = [[Diesel engines]]
 | parent           = [[General Motors]]
 | revenue          =  
 | net_income       =  
 | homepage         = 
}}

The '''Cleveland Diesel Engine Division''' of General Motors (GM) was a leading research, design and production facility of diesel engines from the 1930s to the 1960s that was based in [[Cleveland, Ohio]]. The Cleveland Diesel Engine Division designed several 2 stroke diesel engines for [[submarines]], [[tugboats]], [[destroyer escorts]], {{sclass-|Patapsco|gasoline tanker|1}}s and other marine applications. Emergency generator sets were also built around the Cleveland Diesel and were installed in many US warships. The division was created in 1937 from the GM acquired Winton Engine Company and was folded into the [[Electro-Motive Diesel|Electro-Motive Diesel Division of General Motors]] in 1962. The engines continue in use today on older tugs.

==History==
Cleveland Diesel traces it roots to the Winton Gas Engine and Manufacturing Company which was formed by [[Alexander Winton]] an early Cleveland automobile designer in November 1912 to produce marine engines in Cleveland, Ohio. Winton chose a diesel engine design from Europe and produced the first American diesel in 1913. Renamed the Winton Engine Works in 1916 it would manufacture marine diesel engines and eventually locomotive engines. It was renamed again as the [[Winton Motor Carriage Company|Winton Engine Company]]. George W. Codrington replaced Winton as the president in 1928. [[General Motors]] purchased the company in 1930 and renamed it the Winton Engine Corporation on June 20, 1930. GM changed the name in 1938 to the Cleveland Diesel Engine Division of General Motors Corporation.<ref name="ech.case.edu">https://ech.case.edu/cgi/article.pl?id=CDEDOGMC</ref><ref>https://ech.case.edu/cgi/article.pl?id=WA3</ref>

===Winton Engine Corporation===

The '''Winton Engine Corporation''' produced the first practical [[two-stroke engine|two-stroke]] [[diesel engine]]s in the 400 to 1,200&nbsp;hp (300 to 900&nbsp;kW) range. They powered early [[Electro-Motive Diesel|Electro-Motive Corporation]] (another GM subsidiary) [[diesel locomotive]]s and [[U.S. Navy]] [[submarine]]s. In 1934 an 8-cylinder, 600-horsepower (447&nbsp;kW), 8-201A diesel engine powered the first American diesel-powered train the [[Pioneer Zephyr|Burlington ''Zephyr'']] streamliner passenger train. The locomotive diesel manufacturing side of Winton became part of the Electro-Motive Corporation (later the Electro-Motive Division of General Motors) in 1935. The 2-stroke Winton Model 201 series engines featured [[Two-stroke engine#Uniflow-scavenged|Uniflow scavenging]] with intake ports in the cylinder walls and exhaust valves in the cylinder heads which would carry over to later Cleveland Diesel designs. They were a 60 degree opposed V type engine with [[Diesel engine#Mechanical and electronic injection|mechanical injection]]. They were produced for rail use until late 1938 when the new lighter-weight diesel engine with more speed and flexibility had been developed and introduced as the EMC designed Model 567 series. EMD's successor is still producing locomotive engines based on the Model 567 design.<ref name=TugCleveland>{{cite web|last=|first=|title=Cleveland Diesel Model 278A |url=http://www.tugboatenthusiastsociety.org/pages/tugmach-diesel-historic-CDED-278A.htm|work=Old Marine Engines|publisher= Tugboat Enthusiasts Society of the Americas |accessdate=2013-03-03}}</ref><ref name=Pinkepank(73)>{{cite book|last=Pinkpank|first=Jerry A|title=The Second Diesel Spotter’s Guide|year=1973|publisher=Kalmbach Books|lccn=66-22894|isbn=|pages=25–26}}</ref><ref name="subvetpaul.com">http://www.subvetpaul.com/Engines.htm</ref>

"[[Charles Kettering|Charles F. ("Boss") Kettering]] was the head of the GM Research Laboratories in the 1930s. Boss Kettering was also the principal developer of the Detroit Diesel 71 series engine, the Electro-Motive 567 series diesel, and the Cleveland Diesel 268 and 268A engines." The Cleveland Diesel Model 248 series resulted from a series of design changes of the Winton 201A engine led by Kettering. Further changes and increases in cylinder displacements resulted in the Model 278 during [[World War II]].<ref name="tugboatenthusiastsociety.org">http://www.tugboatenthusiastsociety.org/pages/tugmach-diesel-historic-CDED-278A.htm</ref>

===Cleveland Diesel Engine Division of General Motors===

By 1936 Winton was producing only diesel engines for marine, U.S. Navy, and stationary applications. GM reorganized the company in 1937 as the Cleveland Diesel Engine Division of General Motors. Cleveland diesel engines were used widely by the U.S. Navy in World War II, powering submarines, destroyer escorts, and numerous auxiliaries. They were also installed as generator sets and as auxiliary drives in many US warships built during the 1930s through the 1960s.<ref name=TugCleveland /><ref name="Silverstone(66)">{{cite book |last=Silverstone|first=Paul H|year=1966|title=U.S. Warships of World War II|publisher=Doubleday and Company|pages=164–167|isbn=}}</ref><ref name="Jane’s(98)">{{cite book|last=|first=|year=1998|title=Jane’s Fighting Ships of World War II|publisher=Crescent Books (Random House)|pages=288, 290–291|isbn=0517-67963-9}}</ref><ref name="NavSource SS-285">{{cite web|url=http://www.navsource.org/archives/08/08285a.htm|title=NavSource USS ''Balabo'' SS-285|work=Photographic History of the U.S. Navy|publisher=NavSource Naval History|accessdate=2013-03-03}}</ref><ref name="NavSource DE 99">{{cite web|url=http://www.navsource.org/archives/06/099.htm|title=NavSource USS ''Cannon'' DE 99|work=Photographic History of the U.S. Navy|publisher=NavSource Naval History|accessdate=2013-03-04}}</ref><ref name="NavSource ATA 121">{{cite web|url=http://www.navsource.org/archives/09/38/38121.htm|title=NavSource USS ''Sotoyomo'' ATA 121|work=Photographic History of the U.S. Navy|publisher=NavSource Naval History|accessdate=2013-03-04}}</ref>

98% of Cleveland Diesel's business was government by 1939. The plant was expanded in 1941 producing an estimated 70% of the diesel engines used in U.S. Navy's submarines during World War II. Employment rose to 5,000 during World War II but by 1947 dropped to 1,000. Production expanded again in the 1950s during the [[Cold War]] and Cleveland Diesel acquired the plants at 2160 W. 106th St. and 8200 Clinton Road in Cleveland, Ohio. The development and production of [[nuclear powered submarine]]s in the U.S. Navy during the 1950s reduced the need for Cleveland Diesels and in 1962 General Motors closed the Cleveland plant combining the Cleveland Diesel Division with the Electro-Motive Division in [[LaGrange, Illinois]].<ref name="ech.case.edu"/>

==Cleveland Diesel design==
The Winton Engine Companies Model 201A proved to be unreliable and Kettering redesigned the 201A into the improved Model 248. The Model 16-201A diesels in early fleet submarines such as the [[United States Porpoise-class submarine|''Porpoise'' class]] were eventually replaced with the Cleveland diesel 12-278A during World War II.<ref>Alden, John D., Commander, USN (retired). ''The Fleet Submarine in the U.S. Navy'' (Annapolis, Maryland: Naval Institute Press, 1979), p. 58.</ref>

The Model 248 was offered in 8, 12 and 16-cylinder V type layouts. The [[mechanical fuel injection]] two-stroke Uniflow-scavenged engines employ a gear driven [[Roots blower]] on the front of the engine which provided aspiration for the cylinders. It is a medium speed marine diesel designed to operate under 800&nbsp;rpm and produced full power at 756&nbsp;rpm.<ref>https://books.google.com/books?id=TXO9y6lz8csC&pg=PA34&lpg=PA34&dq=Cleveland+16-248&source=bl&ots=tP1SaNsWJ_&sig=EF4otn9gjzntYheQ0X7YvD7I0kU&hl=en&sa=X&ved=0ahUKEwimhqLG2t_LAhVN62MKHRwiDI8Q6AEINTAE#v=onepage&q=Cleveland%2016-248&f=false</ref>

The Cleveland Diesel Model 248 cylinder displacement was increased in the Model 248A and further redesign for production simplification resulted in the Model 278 with aluminum pistons. In early World War Two it was redesigned again for increased cylinder displacement and horsepower resulting in the Model 278A with steel pistons. The Model 278 engine was built in 6, 8, 12, and 16-cylinder series. The Cleveland Diesel Division product line produced it through the late 1950s.<ref name="tugboatenthusiastsociety.org"/><ref name="subvetpaul.com"/>

==Cleveland diesel-powered fleet submarines==
[[File:General Motors Model 16-248 V16 diesel engine.jpg|thumb|left|General Motors Cleveland Diesel Engine Division Model 16-248 V16 diesel engine]]
The Cleveland Diesel Engine Division of General Motors built the majority of submarine engines during World War II. The Model 16-248 and 16-278A were installed in many of the {{sclass-|Salmon|submarine|5}}, {{sclass-|Sargo|submarine|5}}, {{sclass-|Tambor|submarine|5}}, {{sclass-|Gato|submarine|5}}, and {{sclass-|Balao|submarine|4}}es of [[diesel electric]] submarines built in World War II and they continued to operate in US service until the 1980s and in foreign service until the 2000s. Two models of the Cleveland diesels were used as main engines in World War II era [[Fleet submarine|fleet type submarines]], the Model 16-248 and Model 16-278A. The 16-248 was installed in Cleveland diesel equipped submarines until the Model 16-278 was introduced. Cleveland diesel installations since early World War II were Model 16-278A engines.

They are of a 16-cylinder V-type engine with two banks of eight cylinders each The engines operates on the 2-stroke cycle principle, are air started, and are rated at 1600&nbsp;bhp at 756&nbsp;rpm on the 16-248 and 750&nbsp;rpm on the 16-278A. The size of the bore and stroke of the 16-248 engine is {{frac|8|1|2}} inches and {{frac|10|1|2}} inches respectively as compared to {{frac|8|3|4}} inches and {{frac|10|1|2}} inches for Model 16-278A. Both models were connected to electric generators that provided DC power to the electric drive that turned the shafts.

The Cleveland Diesel Model 268 inline diesel was used as an auxiliary engine in many fleet submarines and as emergency backup generators on larger warships. They also found use in commercial applications. The 8-cylinder, in-line, 2-cycle, air starting engine, rated at 300 kW generator output at 1200 rpm. The size of the bore and stroke is {{frac|6|3|8}} inches and 7 inches respectively.<ref>http://maritime.org/doc/fleetsub/diesel/chap3.htm</ref> The small displacement [[United States Barracuda-class submarine (1951)|''Barracuda'' class]] used three 8-268A Cleveland diesels which developed 1050&nbsp;shp.

==See also==
* [[Diesel Engine]]
* [[Fairbanks-Morse]]
* [[Hooven-Owens-Rentschler]]
* [[Electro-Motive Diesel]]

==References==
{{reflist|30em}}

* The Fleet Type Submarine Main Propulsion Diesels Manual, U.S. Navy

==External links==
*http://maritime.org/doc/fleetsub/diesel/chap3.htm
*http://www.ss563.org/t-class.html
*http://www.tugboatenthusiastsociety.org/pages/tugmach-diesel-historic-CDED-278A.htm
*https://ech.case.edu/cgi/article.pl?id=CDEDOGMC

[[Category:Motor vehicle manufacturers based in Ohio]]
[[Category:General Motors marques]]