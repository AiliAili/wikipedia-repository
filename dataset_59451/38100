{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:SMS Pfeil 1899.jpg|300px]]
|Ship caption=''Pfeil'' in 1899
}}
{{Infobox ship career
|Hide header=
|Ship country= German Empire
|Ship flag= {{Shipboxflag|German Empire|naval}}
|Ship name= SMS ''Pfeil''
|Ship builder=[[Kaiserliche Werft Wilhelmshaven]]
|Ship laid down=1881
|Ship launched=16 September 1882
|Ship commissioned=25 November 1884
|Ship struck=16 February 1922
|Ship fate=Scrapped, 1922
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Blitz|aviso}}
|Ship displacement= {{Convert|1486|MT|0|lk=on}} 
|Ship length= {{Convert|78.43|m|ftin|abbr=on}} [[Length overall|o/a]]
|Ship beam= {{Convert|9.9|m|ftin|abbr=on}}
|Ship draft=  {{Convert|4.07|m|ftin|abbr=on}}
|Ship propulsion=2 × 2-cylinder [[double expansion engine]]s, 2 shafts
|Ship speed= {{Convert|15.7|kn|lk=in}}
|Ship range= {{Convert|2440|nmi|abbr=on}} at {{Convert|9|kn|abbr=on}} 
|Ship complement=* 7 officers
*127 enlisted men

|Ship armament=* 1 × {{Convert|12.5|cm|in|abbr=on}} K L/23 gun
* 4 × {{Convert|8.7|cm|in|abbr=on}} K L/23 guns
* 1 × {{convert|35|cm|abbr=on}} [[torpedo tube]]

|Ship notes=
}}
|}

'''SMS ''Pfeil''''' was an [[aviso]] of the [[Imperial German Navy]], the second and final member of the {{sclass-|Blitz|aviso|4}}. She had one sister ship, {{SMS|Blitz}}. ''Pfeil'' was laid down at the [[Kaiserliche Werft Wilhelmshaven|''Kaiserliche Werft'']] shipyard in [[Wilhelmshaven]] in 1881, launched in September 1882, and commissioned in November 1884. Throughout her career, ''Pfeil'' served in various units in the German fleet and participated in numerous training exercises. In 1889&ndash;90, she was sent to [[German East Africa]] to assist in the suppression of a colonial revolt. In 1894&ndash;99, she was used as a fishery protection ship, and after 1904, she was used as a tender for the battle fleet. She served in this capacity until 1919. ''Pfeil'' was stricken from the [[naval register]] in February 1922 and subsequently broken up for scrap.

==Design==
[[File:Kleiner Kreuzer SMS Blitz (1882).png|thumb|left|Illustration of the ''Blitz'' class]]

{{main|Blitz-class aviso}}

''Pfeil'' was {{convert|78.43|m|sp=us}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|9.9|m|abbr=on}} and a maximum [[draft (hull)|draft]] of {{convert|4.07|m|abbr=on}} forward. She displaced {{convert|1486|MT}} at full combat load. Her propulsion system consisted of two horizontal 2-cylinder [[double expansion engine]]s. Steam for the engines was provided by eight coal-fired cylindrical boilers. The ship's propulsion system provided a top speed of {{convert|15.6|kn|abbr=on}} and a range of approximately {{convert|2440|nmi|lk=in}} at {{convert|9|kn|abbr=on}}. ''Pfeil'' had a crew of 7&nbsp;officers and 127&nbsp;enlisted men.<ref name=G91>Gröner, p. 91</ref>

As built, the ship was armed with one [[12.5 cm K L/23 gun|{{convert|12.5|cm|abbr=on}} K L/23]] gun placed in a pivot mount. The gun was supplied with 100 rounds of ammunition. The ship was also equipped with four [[8.7 cm K L/23 gun|8.7&nbsp;cm K L/23]] guns in single mounts. ''Pfeil'' also carried one {{convert|35|cm|abbr=on}} [[torpedo tube]] mounted in the bow. In 1891&ndash;92, the ship was rearmed with six [[8.8 cm SK L/30 gun]]s in single mounts and three 35&nbsp;cm torpedo tubes, one in the bow and one on each [[broadside]], all submerged in the hull.<ref name=G91/>

==Service history==
[[File:SMS Pfeil 1914.jpg|thumb|left|''Pfeil'' at sea c. 1914]]
''Pfeil'' was laid down at the [[Kaiserliche Werft Wilhelmshaven|''Kaiserliche Werft'']] shipyard in [[Wilhelmshaven]] in 1881. She was launched on 16 September 1882, and commissioned into the German fleet on 25 November 1884.<ref>Gardiner, p. 256</ref> From her commissioning, she was employed as a tender for the fleet.<ref name=G91/> ''Pfeil'' participated in the annual maneuvers in the summer of 1885. She was assigned to the unarmored division, along with the old sailing [[corvette]]s {{SMS|Stein||2}}, {{SMS|Olga||2}}, and {{SMS|Sophie||2}}. Independent division exercises took place in June, and in August, the divisions, which also included an armored division composed of several [[ironclad warship|ironclads]] and a torpedo boat division led by ''Pfeil''{{'}}s [[sister ship]] {{SMS|Blitz||2}}, joined for combined fleet maneuvers that lasted until September.<ref>Sondhaus, p. 162</ref>

In 1889, ''Pfeil'' and the [[unprotected cruiser]] {{SMS|Schwalbe||2}} were sent to [[German East Africa]] to participate in the suppression of local resistance to German colonial rule. By November 1890, the rebellion had been defeated and German imperial rule returned. ''Pfeil'' was the first steel-hulled, unrigged warship ever sent by Germany to its African colonies.<ref>Sondhaus, p. 203</ref> In 1891, ''Pfeil'' served in the Training Squadron as a torpedo boat flotilla leader, along with ''Blitz'' and the aviso {{SMS|Jagd||2}}. During the annual summer exercises, the Training Squadron served as the hostile fleet, and conducted a mock attack on the harbor at [[Kiel]].<ref>''Notes on the Year's Naval Progress'', p. 257</ref> In early 1894, ''Pfeil'' was assigned to the I Division of the Maneuver Squadron as the dispatch vessel for the four {{sclass-|Sachsen|ironclad|1}}s.<ref>Cleveland, p. 663</ref> Later that year, the ship was used for fishery protection duties.<ref name=G91/> She participated in the summer fleet exercises in 1897, where she acted in the simulated hostile fleet.<ref>Garbett, p. 1559</ref>

Her service as a fishery protection ship lasted until 1899, when she returned to service with the fleet. She was withdrawn from active service in 1904, when she resumed her old tender duties.<ref name=G91/> In 1912, ''Pfeil'' was assigned as the tender for the [[pre-dreadnought battleship]]s of the [[II Battle Squadron]].<ref>Courtney, p. 795</ref> While in the II Squadron, she wore a yellow identification band on her funnel, to distinguish her from her sister ''Blitz'', which wore a white identification stripe.<ref>''Journal of the American Society of Naval Engineers'', p. 1053</ref> She remained in her tender duties through 1919. She was stricken from the [[naval register]] on 16 February 1922 and broken up for scrap at Wilhelmshaven.<ref name=G91/>

==Notes==
{{reflist|20em}}

==References==
* {{cite journal|editor-last=Cleveland|editor-first=H. F.|title=Naval and Military Notes|journal=Journal of the Royal United Service Institution|date=June 1894|volume=XXXVIII|publisher=Harrison and Sons|location=London|oclc=494356158|pages=657&ndash;672}}
* {{cite journal|editor-last=Courtney|editor-first=W. L.|journal=The Fortnightly Review|year=1912|volume=XCI|publisher=Chapman and Hall, Ltd.|location=London|oclc=1781612}}
* {{cite journal|editor-last=Garbett|title=The Autumn Manoevres of the German Fleet, 1897|editor-first=H.|journal=Journal of the Royal United Service Institution|date=December 1897|volume=XLI|publisher=J. J. Keliher & Co.|location=London|oclc=494356158|pages=1557&ndash;1561}}
* {{cite book |editor-last=Gardiner|editor-first=Robert|title=Conway's All the World's Fighting Ships: 1860–1905|year=1979|location=London|publisher=Conway Maritime Press|isbn=0-85177-133-5}}
*{{Cite book |last=Gröner|first=Erich|title=German Warships: 1815–1945|year=1990|publisher=Naval Institute Press|isbn=0-87021-790-9|location=Annapolis, MD}}
* {{cite journal|journal=Journal of the American Society of Naval Engineers|date=August 1909|volume=XXI|number=3|publisher=R. Beresford|location=Washington, DC|oclc=3227025}}
*{{Cite journal|journal=Notes on the Year's Naval Progress|publisher=Office of Naval Intelligence|date=July 1892|location=Washington, DC|oclc=6954233}}
*{{Cite book |last=Sondhaus|first=Lawrence|title=Preparing for Weltpolitik: German Sea Power Before the Tirpitz Era|year=1997|publisher=Annapolis|location=Naval Institute Press|isbn=1-55750-745-7}}

{{Blitz class aviso}}

{{DEFAULTSORT:Blitz}}
[[Category:Avisos of the Imperial German Navy]]