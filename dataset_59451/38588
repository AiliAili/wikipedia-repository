{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{{Infobox locomotive
|powertype       = Steam
|name            = BR Standard Class 6<ref name=Clarke>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', pp.80&ndash;87</ref> 
|image           = 72003 Clan Fraser in 1958.jpg
|caption         = 72003 ''Clan Fraser'' at Polmadie, Glasgow on 31 August 1958
|designer        = [[Robert Riddles]] at BR Derby
|builder         = BR [[Crewe Works]]
|builddate       = December 1951 – March 1952
|totalproduction = 10
|whytetype       = 4-6-2 (''Pacific'')
|uicclass        = 2′C1′h2
|gauge           = {{Track gauge|uksg|allk=on}}
|leadingdiameter = {{convert|3|ft|4|in|mm|0|abbr=on}}
|driverdiameter  = {{convert|6|ft|2|in|mm|0|abbr=on}}
|trailingdiameter= {{convert|3|ft|3+1/2|in|mm|0|abbr=on}}
|length          = {{convert|68|ft|9|in|m|2|abbr=on}}
|width           = {{convert|8|ft|8+3/4|in|m|2|abbr=on}}
|height          = {{convert|13|ft|0+1/2|in|m|2|abbr=on}}
|axleload        = {{convert|19.00|LT|t ST|abbr=on|lk=on}}
|weightondrivers = {{convert|56.90|LT|t ST|abbr=on}}
|locoweight      = {{convert|88.50|LT|t ST|abbr=on}}
|tenderweight    = {{convert|14.15|LT|t ST|abbr=on}}
|tendertype      = BR1
|fueltype        = [[Coal]]
|fuelcap         = {{convert|7.00|LT|t ST|abbr=on}}
|watercap        = {{convert|4250|impgal|abbr=on}}
|boiler          = BR1
|boilerpressure  = {{convert|225|psi|kPa|abbr=on}} 
|firearea        = {{convert|36|sqft|m2|abbr=on}}
|fireboxarea     = {{convert|195|sqft|m2|abbr=on}}
|tubesandflues   = {{convert|1878|sqft|m2|abbr=on}}
|superheaterarea = {{convert|592|sqft|m2|abbr=on}}
|cylindercount   = Two, outside
|cylindersize    = {{convert|19.5|x|28|in|mm|0|abbr=on}}
|tractiveeffort  = {{convert|27520|lbf|kN|1|abbr=on}}
|factorofadhesion = 4.63
|operator        = [[British Railways]]
|locale          = [[Scottish Region of British Railways|Scottish Region]], [[London Midland Region of British Railways|London Midland Region]]
|powerclass      = 6P5F
|fleetnumbers    = 72000–72009
|axleloadclass   = [[Route Availability]] 8
|withdrawndate   = December 1962 – June 1966
|disposition     = All original locomotives [[scrap]]ped, 72010 Hengist under construction.
}}

The '''Standard class 6''', otherwise known as the ''' ''Clan'' Class''', was a class of [[4-6-2|4-6-2 ''Pacific'']] [[Tender (railroad car)|tender]] [[steam locomotive]] designed by [[Robert Riddles]] for use by [[British Railways]]. Ten locomotives were constructed between 1951 and 1952, with a further 15 planned for construction. However, due to acute steel shortages in Britain, the order was continually postponed until it was finally cancelled on the publication of the [[1955 Modernisation Plan]] for the re-equipment of British Railways.

The ''Clan'' Class was based upon the [[BR Standard Class 7|''Britannia'' Class]] design, incorporating a smaller [[boiler]] and various weight-saving measures to increase the [[route availability]] of a ''Pacific''-type locomotive for its intended area of operations, the west of [[Scotland]]. The ''Clan'' Class received a mixed reception from crews, with those regularly operating the locomotives giving favourable reports as regards performance.{{sfn|Atkins|1992|p=10}} However, trials in other areas of the British Railways network returned negative feedback, a common complaint being that difficulty in steaming the locomotive made it hard to adhere to timetables. Reports exist that suggest a degree of the disappointment with these locomotives was attributable to their being allocated to Class 7 work where they were only a Class 6 in reality; a problem put down to their very similar appearance to the BR Standard Class 7.

Some of the ''Clan'' Class locomotives took their names from the [[Highland Railway Clan Class]] which was being withdrawn from service at the time, indicating further their intended area of operations.<ref name=Cox1 />  The class was ultimately deemed a failure by British Railways, and the last was withdrawn in 1966. None survived into preservation, although a project to build the next locomotive in line, number 72010 ''Hengist'', is progressing. The frame plates are presently at Riley & Son Ltd. at Bury awaiting further parts to commence assembly of the frame structure.

==Background==
Under the initial scheme for the creation of a series of British Railways standard locomotives, larger passenger and mixed traffic types were intended to be of the 4-6-2 ''Pacific'' wheel arrangement, the main advantage of which was that it could be fitted with a wide [[Firebox (steam engine)|firebox]] capable of burning a range of coal types (and qualities).{{sfn|Clarke|2006|p=85}} The ''Pacifics'' were originally intended to be produced in four power groups: 8, 7, 6, and 5, according to the system of power ratings inherited from the [[London, Midland and Scottish Railway|L.M.S.]] constituent company. Power groups 7, 6 & 5 were to be for mixed traffic (MT) service. The whole standardisation programme was launched with the building of the [[BR Standard Class 7|7MT Britannia]] design in 1951; in the event, the 5MT proposal was dropped in favour of an updated version of the highly successful [[LMS Stanier Class 5 4-6-0|Stanier mixed traffic 4-6-0]].<ref name=Haresnape1>Haresnape, Brian: ''Loco profile no. 12: BR Britannias'', p. 270</ref> It was further appreciated that a ''Pacific'' of 6MT power could be built with a high enough route availability to fulfil all remaining requirements; this had been amply demonstrated by [[Oliver Bulleid]], [[Chief Mechanical Engineer]] of the [[Southern Railway (Great Britain)|Southern Railway]], who had developed a lighter version of his large 3-cylinder [[SR Merchant Navy Class|''Merchant Navy'' Class]] in 1945. H.A.V. Bulleid, Oliver Bulleid's son, advocated that the resultant [[SR West Country and Battle of Britain Classes|''Light Pacifics'']] had "almost 90%" route availability on the Southern railway network.<ref>Bulleid, H. A. V.: ''Bulleid of the Southern'', p. 68</ref> The advantages of such a locomotive for use on some of the heavily restricted main lines in Scotland, such as the [[Dumfries]] to [[Stranraer]] line, had been further demonstrated by the remarkable performance of ''Light Pacific'' number 34004 ''Yeovil'' on the ex [[Highland Railway]] line to [[Inverness]] during the British Railways [[1948 Locomotive Exchange Trials]]. During these trials the locomotive showed that a ''Light Pacific'' had the potential to revolutionise the timetable over this difficult trunk route. As the general policy of the Railway Executive was to eliminate as far as possible the perceived complication of multi-[[Cylinder (locomotive)|cylinder]] locomotives, an equivalent 2-cylinder ''Pacific'' was produced by mounting a smaller and lighter boiler on the standard 7MT chassis.<ref name=Haresnape2>Haresnape, Brian: ''Loco profile no. 12: BR Britannias'', p. 271</ref>

==Design details==
[[File:Chester General, unusual locomotive by Chester 3A Box - geograph.org.uk - 2242403.jpg|thumb|No. 72005 'Clan Macgregor' at Chester General, 29 August 1964]]
The arrangement consisted of a modified Standard Class 7 boiler, with smaller steel cylinders and other modifications to save weight and hence increase route availability.<ref name=Cox />  The wider firebox, designed for use with cheaper imported coal, was also utilised to spread its weight evenly over the [[axles]], whilst the standard [[smokebox]] completed the boiler, which at 225&nbsp;lbf/in² was rated at a lower working pressure than that of the ''Britannias''.<ref name=Clarke3 />  A single chimney was incorporated into the design, although this was to create problems later on, due to its small diameter, which reduced the choke area that allowed the fierce exhaust blast to escape, reducing the overall efficiency of the locomotive. Similarities with the ''Britannias'' rested with the frames, tenders and running gear, allowing easy standardisation of parts common with other classes.<ref name=Clarke3 />  The design was fitted with a standard set of two [[Walschaerts valve gear]] systems, and all members of the class were equipped with 4,200 gallon BR 1 tenders.<ref name=Clarke3 />

Following experience of occasional cracks appearing in the frame plates near the spring brackets, had the second batch of Class 6 Standard Pacifics been built, the chassis would have been rearranged to be similar to that used on the solitary Class 8 Pacific. This would have resulted in the locomotive riding on three cast steel Combined Frame Stretcher & Spring Brackets carrying the 10 front-most spring brackets and lengthened spring brackets behind the rear driven axle. These Combined Frame Stretcher & Spring Brackets are often referred to as "sub-frames". (Perhaps remarkably, the rearmost spring brackets were not to be integrated into a single cast combined sub-frame/pony truck pivot stretcher. The pony truck pivot stretcher being a separate fabrication).<ref>Confirmed on various original BR drawings including SL/DE/21642, SL/DE/21631 and SL/DE/22042 sourced from the [[National Railway Museum|NRM]]</ref>

Unlike the smaller BR Standards the exhaust steam manifold within the smokebox saddle (along with the BR Standard Class 7 engines) was a steel casting<ref>Confirmed on original BR drawing SL/DE/19620 sourced from the NRM</ref> that was welded into the saddle. Other original drawings <ref>Including BR drawing SL/SW/616 sourced from the NRM</ref> confirm the exhaust steam manifold was a steel fabrication in the smaller BR standards.

==Construction history==
Designed at the drawing offices of [[Derby Works]], the new class was constructed at British Railways' [[Crewe Works]] between 1951 and 1952.<ref name=Cox>Cox, E. S.: ''British Railways Standard Locomotives'', p. 61</ref>  The initial order was for 25 locomotives, but such was the immediacy of demand regarding a smaller version of the ''Britannias'' that a batch of 10 was rushed through construction before teething problems had been ironed out at the British Railways testing station at [[Rugby, Warwickshire|Rugby]].<ref name=Clarke3>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', p. 80</ref> No more were constructed due to the steel shortages of the 1950s, and onset of the British Railways [[1955 Modernisation Plan|Modernisation Plan]] from 1954.

===Initial modifications===
After initial running-in, E.S.Cox was quoted as discerning a distinct "woolliness" in their steaming, and although they missed their appointment at the Rugby Testing Station due to late completion, some modifications were carried out, most notably to the diameter of the [[blastpipe]], resulting in better steaming and increased power.<ref name=Cox2>Cox, E. S.: ''British Railways Standard Locomotives'', p. 64</ref>  Initially, the return cranks on the main driving wheels were of [[LNER]] block-type as seen on [[Arthur Peppercorn]]'s [[LNER Peppercorn Class A1|A1s]] and [[LNER Peppercorn Class A2|A2s]], but this was changed to the simpler [[London, Midland and Scottish Railway|LMS]] four-stud fitting. With time, enough information was gathered from operational feedback from crews to allow modifications to be applied to the further 15 locomotives on order in the second batch, had they been built.<ref name=Cox2 />

===Naming the locomotives===
The choice of locomotive names came from engineer and future railway historian Ernest Stewart Cox's desire to replicate the near extinct Ex-[[Highland Railway]] 4-6-0 ''Clan'' Class, therefore representing Scotland in the new organisation.<ref name=Cox1>Cox, E. S.: ''British Railways Standard Locomotives'', p. 62</ref>  The first of the class, No. 72000 ''Clan Buchanan'', was treated to a special ceremony at [[Glasgow Central station]] on 15 January 1952 at which the [[Lord Provost]] unveiled its nameplates. Five of these names had previously been used on Highland Railway locomotives.
The first five of the planned second batch of 15 locomotives were intended for use on BR's [[Southern Region of British Railways|Southern Region]]; these were allocated names ''Hengist'', ''Horsa'', ''Canute'', ''Wildfire'' and ''Firebrand'', which had all been previously used on locomotives in southern England. The following ten were to be allocated to Scotland and were allocated further "Clan" names, all of which were new.<ref>{{harvnb|Gilbert|Chancellor|1994|pp=151,153}}</ref>

==Operational details==
The ''Clan Class'' had a mixed reception when first introduced to British Railways locomotive crews because there were only 10 locomotives in a class that was mostly confined to the North West of the railway network. This was due to the fact the low number of class members prevented effective training of locomotive crews throughout the nationalised network, and a degree of partisanship amongst crews towards newer locomotives further ensured this.{{sfn|Atkins|1992|pp=6–7}}  The entire class was also based predominantly at two depots throughout their working lives, these being [[Glasgow]] [[Polmadie]] and [[Carlisle, Cumbria|Carlisle]] Kingmoor, compounding their restricted circulation. However, factors such as these meant that they spent most of their short careers out of the limelight that the ''Britannias'' had, resulting in a relatively camera shy class of locomotive. Crews that used them on regular duties displayed their liking for the locomotives, and as such, could produce good work.{{sfn|Atkins|1992|p=10}}  However, the predominant number of crews who were unfamiliar with the ''Clans'' found them difficult to handle, leading to an undeservedly bad reputation.

The poor steaming characteristics of the class had been the result of rushed production, which was another factor that led to the bad reputation of the ''Clan'' Class.<ref name=Farr2>Farr, Keith: ' 'Clans' Highland and Lowland', pp. 722&ndash;723</ref>  Furthermore, they suffered from complaints regarding a lack of pulling power, although this can be attributed to indifferent handling and firing techniques, which certainly did not help the situation.<ref name=Farr2 />  However, had the [[1955 Modernisation Plan|Modernisation Plan]] been delayed, and the correct amount of investment made for undertaking the relevant modifications, such as streamlining of the steam passages and increased diameter [[blastpipe]] in a double-chimney layout, the ''Clans'' would have been free-steaming workhorses worthy of complementing the 'Britannias'.<ref name=Farr3>Farr, Keith: ' 'Clans' Highland and Lowland', p. 718</ref>  Without modification, they were still capable machines when handled properly, as various feats testifying this included regular ascents of [[Shap]] and [[Beattock]] with 14&nbsp;[[carriages]] without the assistance of a [[banking locomotive]].{{sfn|Atkins|1992|p=9–10}}  Other arduous duties that the class frequently undertook were the regular turns on the [[Settle-Carlisle Railway|Settle to Carlisle]] route, which has some of the steepest gradients and harshest working conditions of any British mainline.{{sfn|Atkins|1992|p=10}}  The Midland region was always short of top-link motive power and the ''Clan'' Class proved to be a very welcome addition to the fleet.

The engines also performed on Glasgow–[[Crewe]], [[Manchester]] and [[Liverpool]] services, [[Edinburgh]]–[[Leeds]] services, Carlisle–[[Bradford]] services, and finally the [[Stranraer]] Boat Train workings.<ref name=Barnes4>Barnes, Robin: 'Salute to the Scottish standard - Part 2', pp. 286&ndash;287</ref>  As more crews got used to them, the class could be found far from home territory at destinations as diverse as [[Aberdeen]], [[Inverness]], [[Port Talbot]], [[Newcastle upon Tyne]], [[Bristol]], and even [[London]].<ref name=Barnes4 />  ''Clan'' number 72001 to this day remains the only Pacific locomotive to have worked over the [[West Highland Line]], the result of a successful trial held in early 1956 to ascertain whether a Pacific type could traverse this steeply graded line.<ref name=Farr>Farr, Keith: ' 'Clans' Highland and Lowland', pp. 714&ndash;723</ref>  Having passed that test, a tribute to the versatility of the class, ''Clan Cameron'' was allowed to work special trains for the [[Clan Cameron]] gathering that took place in June of that year.<ref name=Farr1>Farr, Keith: ' 'Clans' Highland and Lowland', pp. 719&ndash;720</ref>

In August 1958, number 72009 was tested on the [[Eastern Region of British Railways|Eastern Region]], being based at [[Stratford station|Stratford]] MPD, though a preference for the ''Britannias'' meant that this sojourn was short-lived, lasting only a month. The locomotive was utilised on services from London [[Liverpool Street station|Liverpool Street]] to [[Norwich]], [[Clacton]], and [[Harwich]].<ref name=Review>'Experimental use of the ''Clan'' Class on the Liverpool Street-Clacton service', p. 218</ref>  At first they were mistakenly allocated Class 7 duties, in which the ''Clans'', although capable, were not able to keep to their allotted timings.<ref name=Review />  This was part of the trials for the West Highland Line services mentioned earlier, but the locomotive was rejected for such duties on the grounds that they were "no better than a good [[LNER Thompson Class B1|B1]]".<ref name=Review />  The result of these trials was that as both Standard Class 7 and 8 locomotives were moved north in 1961 after [[dieselisation]] started in earnest, the ''Clans'' were downgraded to secondary work. Maintenance was initially undertaken at Crewe Works, but responsibility was transferred to [[Cowlairs]] Works in the spring of 1958.<ref name=Barnes>Barnes, Robin: 'Salute to the Scottish Standard - Part 1', pp. 235&ndash;241</ref>  More varied work was allocated to them as their reliability improved, including working portions of the ''Thames-Clyde Express'' and the ''Queen of Scots Pullman''. They also deputised for the many failed [[diesel locomotives]] that plagued the network at the time, and were extensively used on [[freight]] workings.

Most Scottish and Midland region crews that used them regularly took to the class, and found that if used properly, running times were kept with ease.<ref name=Barnes />  These crews rated them the most sure-footed of any ''Pacifics'' available on the Midland Region, though other crews who tested them claimed that the ''Clans'' were prone to [[Locomotive wheelslip|slipping]], though this was the case with most ''Pacific'' designs.<ref name=Barnes1>Barnes, Robin: 'Salute to the Scottish Standard - Part 2', pp. 288&ndash;290</ref>  Despite the various successes of the ''Clans'', the class was generally regarded as a failure, even with overall performance being just short of Riddles' aims.<ref name=Barnes2>Barnes, Robin: 'Salute to the Scottish Standard - Part 1', pp.236&ndash;237</ref>  However, the premise of all British Railways Standard designs was for a hard working, easily maintained, economical, highly available, and all-purpose locomotive. In these respects, the ''Clans'' were highly successful.<ref name=Barnes3>Barnes, Robin: 'Salute to the Scottish Standard - Part 2', p. 290</ref>

===Proposed second batch and withdrawal===
Prior to the publishing of the [[1955 Modernisation Plan|Modernisation Plan]] advocating the change-over to diesel traction, there was a proposal to construct a second batch of the ''Clan'' Class, which was accepted as Crewe Works Order Lot 242. This authorised the construction of a further batch of fifteen ''Clans'' that included modifications to the original design.<ref name=Southern />  Originally scheduled for 1952 with frames constructed for 72010 ''Hengist'', acute steel shortages meant that the order was continually rescheduled until the publication of the British Railways Modernisation Plan finally halted the project. The initial name allocations for the new batch would seem to suggest several operating the [[Kent]] coast trains, hauling the [[Golden Arrow (train)|Golden Arrow]] and other expresses, so that some of this batch would have been allocated to the Southern Region.<ref name=Southern>'"CLANS" for the Southern' (Trains Illustrated), p. 406</ref>

The first locomotives to be withdrawn from service were the Polmadie locos 72000-72004 en masse in December 1962, where after being moved first to Glasgow Parkhead and stored, they were eventually moved to [[Darlington]] for scrapping in 1964.<ref name=Clarke7 />  Of the Kingmoor allocation, the first, number 72005, was withdrawn in April 1965, whilst the final loco was 72008 on 21 May 1966 from Carlisle Kingmoor shed.<ref name=Clarke7 />  When No 72008 ''Clan MacLeod'' was finally scrapped in August 1966, it rendered the class extinct. Though this locomotive served British Railways for only fourteen years and three months, it was the longest serving ''Clan''.<ref name=Clarke7>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', p.84</ref>

==Livery and numbering==

The livery of the ''Clans'' was a continuation of the standard British Railways [[Brunswick green]] applied to express passenger locomotives after [[nationalisation]], lined in orange and black. The class was given the [[power classification]] 6P.<ref name=Clarke1>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', pp. 82&ndash;83</ref>  Following on from the ''Britannias'', the ''Clans'' were numbered under the British Railways standard numbering system in the 72xxx series.<ref name=Clarke2>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', p. 82</ref> The locomotives were numbered between 72000 and 72009, and featured brass nameplates with a black background, located on the smoke deflectors, though towards the end of their working lives, some nameplates were painted with a red background.<ref name=Clarke84>Clarke, David: ''Riddles Class 6/7 Standard Pacifics'', p. 84</ref>

==Preservation==

None of the original engines survived into preservation, however progress is being made by a Registered Charity (No. 1062320), the Standard Steam Locomotive Company, on constructing a new locomotive that would have been the first of the uncompleted batch of 15, number 72010 ''Hengist''. As 999 [[Steam locomotives of British Railways#BR standard classes|BR standard steam locomotives]] were built in the years up to 1960 the builders consider this is the 1000th locomotive build to be commenced to a British Railways standard design.<ref name=Thomas>{{harvnb|Thomas|2006|p=20}}</ref> The Standard Steam Locomotive Company believes that significant costs will be avoided as many of the required cast parts can be made from patterns held by fellow members of the [[BRSLOG|British Railways Standard Locomotive Owners Group (BRSLOG)]].

At present the group are concentrating on amassing the many frame components at their Midlands storage facility prior to assembly before relocation to the [[Great Central Railway (heritage railway)|Great Central Railway]] in Leicestershire. Due to an issue with the Frame plates and horn guides being out of true a new set have been manufactured and delivered to Ian Riley & Son (Bury) Ltd (24/4/2014). The remaining components are in a queue with an aim to start frame assembly in late 2014 or 2015.<ref>{{Cite web|url=https://www.theclanproject.org/|title=THE 'CLAN' PROJECT - Home|website=www.theclanproject.org|access-date=2016-10-14}}</ref>

== See also ==
* [[List of BR 'Clan' Class locomotives]]

== Footnotes ==
{{reflist|colwidth=35em}}

== References ==
{{refbegin}}
* {{cite journal |last= |first= |year=1952 |title=A New Light Pacific |journal=Trains Illustrated |page=5 |ref=harv}}
* {{cite journal |last=Atkins |first=Philip |year=1992 |title=The enigma of the BR ‘Clans’ |journal=Steam World |page=65 |ref=harv }}
* {{cite journal |last=Barnes |first=Robin |year=1996 |title=Salute to the Scottish Standard – Part 1 |journal=Backtrack |volume=10 |issue=5 |ref=harv}} 
* {{cite journal |last=Barnes |first=Robin |year=1996 |title=Salute to the Scottish Standard – Part 2 |journal=Backtrack |volume=10 |issue=6 |ref=harv}}
* {{Bradley-StdSteamBR}}
* {{cite journal |last= |first= |year=1954 |title="CLANS" for the Southern |journal=Trains Illustrated |page=7 |ref=harv}}
* {{cite book |last=Clarke |first=David |year=2006 |title=Riddles Class 6/7 Standard Pacifics (Locomotives in Detail volume 5) |location= Hinckley |publisher=Ian Allan |isbn=0-7110-3177-0 |ref=harv}}
* {{cite book |last=Cox |first=E. S. |year=1966 |title=British Railways Standard Locomotives| location=London |publisher=Ian Allan |ref=harv}}
* {{cite journal |last= |first= |year=1958 |title=Experimental use of the Clan class on the Liverpool Street-Clacton service |journal=[[Locomotive, Railway Carriage & Wagon Review]]'' |page=64| ref=harv}}
* {{cite journal |last=Farr |first=Keith: |date=2001 |title=‘Clans’ Highland and Lowland  |journal=Backtrack |page=15 |ref=harv }}
*{{cite book |last1=Gilbert |first1=P.T. |last2=Chancellor |first2=P.J. |year=1994 |editor-last=Taylor |editor-first=R.K. |title=A Detailed History of British Railways Standard Steam Locomotives – Volume One: Background to Standardisation and the Pacific Classes |location=Lincoln |publisher=[[Railway Correspondence and Travel Society|RCTS]] |isbn=0-901115-81-9 |ref=harv }}
* {{cite book| last=Haresnape |first=Brian |year= |title=Loco profile no. 12: BR Britannias |location=Windsor |publisher=Profile Publications |ref=harv}}
* {{cite book|last=Rogers |first=Colonel H. C. B. |year= |title=Transition from Steam |location=London |publisher=Ian Allan |isbn=0-7110-1014-5 |ref=harv}}
* {{cite journal |last=Thomas |first=Cliff |date=December 2006 |editor-first=Nick |editor-last=Pigott |editor-link=Nick Pigott |title=The 'Lazarus' locomotives - number 1: BR 'Clan' Pacific no. 72010 HENGIST |journal=[[The Railway Magazine]] |volume=152 |issue=1268 |publisher=IPC Media |location=London |issn= |ref=harv }}
{{refend}}

== External links ==
{{commons category|BR Standard Class 6|<br />BR Standard Class 6}}
* [http://www.theclanproject.org/index.html 72010 ''Hengist'' Official Website]
* [http://www.railuk.co.uk/steam/getsteamclass.php?item=CLAN Railuk database]
* http://www.tower-models.com/towermodels/ogauge/djh/k308/
* [http://www.swanagerailway.co.uk/news41.htm 1000th BR Standard steam locomotive to be built at Swanage]

{{BRstds}}

{{good article}}

[[Category:British Railways standard classes|6]]
[[Category:4-6-2 locomotives]]
[[Category:Railway locomotives introduced in 1951]]
[[Category:Scrapped locomotives]]