'''Adelyn Breeskin''' (1896–1986) was an American curator, museum director, and art historian known for her longtime leadership of the [[Baltimore Museum of Art]] and [[Mary Cassatt]] scholarship.

==Biography==

Adelyn Breeskin was born in 1896 in Baltimore to Alfred Robert Louis Dohme and Emmie Blumner (Dohme).<ref>Alice Steinbach, "Adelyn D. Breeskin dies on trip to Italy; was head of Baltimore Museum of Art" 25 Jul. 1986''The Sun,''1A</ref> In 1918, she received her bachelor's degree from the Boston School of Fine Arts, Crafts, and Decorative Design. Breeskin then served as an assistant in the print department of the [[Metropolitan Museum of Art]] with Kathryn B. Child under the curator of prints, William Mills Ivins. In 1920, she married the violinist [[Elias Breeskin]] (1895–1969), but the couple divorced in 1930.<ref name="dictionaryofarthistorians">[http://www.dictionaryofarthistorians.org/breeskina.htm "Breeskin, Adelyn Dohme, née Dohme" ''Dictionary of Art Historians'']</ref>
Breeskin moved to Baltimore to work as a curator in the [[Baltimore Museum of Art]] and was promoted to director of the museum in 1942. As the director of the Baltimore Museum of Art, Breeskin established a works on paper collection, supervised an exhibit of [[John Russell Pope]], mounted the show “Abstract Expressionism,” oversaw expansion to the institution, acted as commissioner for the American Contingent of 30th Venice Biennale, and negotiated the donation of Etta and Claribel Cone [[Cone Collection]].<ref>Avis Berman, "Adelyn Breskin: A Perseverance of Vision" 30 Oct. 1977 ''The Sun,''D1</ref>

In 1962, Breeskin left the Baltimore Museum of Art to become the head of Washington Gallery of Modern Art. Breeskin currated shows such as “Roots of Abstract Art in America” in 1965, but she resigned two years later. From 1960 to 1974, she served as the curator of contemporary painting and sculpture at the National Collection of Fine Arts later called the [[National Museum of American Art]] where she featured artists such as [[Mary Cassatt]], [[Milton Avery]], H. Lyman Sayen, [[William J. Johnson]], and [[Bob Thompson (painter)]].<ref name="dictionaryofarthistorians" />

===Mary Cassatt===
Beginning her research in the 1940s, Breeskin authored two catalogue raisonnés on Cassatt’s paintings, oils, and pastels and Cassatt’s watercolors and drawings. She acted as curator of the Cassatt retrospective exhibition at the National Gallery of Art in 1970 though she was simultaneously working on the Sayn and Johnson shows. Several hours a week, Breeskin devoted time to adding information to her catalogues and answering numerous questions from collectors from around the world.<ref>Jan Keene Muhlert, "Adelyn Dohme Breeskin: A Tribute." 11:1 (Spring, 1997) Chicago: The University of Chicago Press on behalf of the Smithsonian American Art Museum, 94-96.</ref>

=== Awards ===
1956 President of the Association of Art Museum Directors, 1976 Katherine Caffey Award for Distinguished Accomplishment in the Museum Profession, 1984 Saluted by Women’s caucus of the [[American Alliance of Museums|American Association of Museums]] as “an exemplary model whose distinguished career is permanently etched in the fertile ground of American art history.”, 1985 [[Smithsonian Institution]] presented her their highest award, the Gold Medal for Exceptional Service

== See also ==
* [[Women in the art history field]]

== References ==
{{Reflist}}

==External links==
*"Adelyn Breeskin's farewell dinner (sound recording), 1962 Apr. 16," Archives of American Art [http://www.aaa.si.edu/collections/adelyn-breeskins-farewell-dinner-sound-recording-10412]
*"Oral history interview with Adelyn Dohme Breeskin," 1974 June 27" Archives of American Art [http://www.aaa.si.edu/collections/interviews/oral-history-interview-adelyn-dohme-breeskin-13285]
*"Adelyn Dohme Breeskin papers, 1934–1978," Archives of American Art [http://www.aaa.si.edu/collections/adelyn-dohme-breeskin-papers-8422]
*Alan Wurtzburger, "Alan and Janet Wurtzburger Papers 1955–1969" Baltimore: Baltimore Museum of Art [http://www.worldcat.org/oclc/707527627]
*Baltimore Museum of Art, "Cone Collection Records" [http://www.worldcat.org/oclc/646346747]

{{DEFAULTSORT:Breeskin, Adelyn}}
[[Category:American art historians]]
[[Category:People from Baltimore]]
[[Category:Museum directors]]
[[Category:American curators]]
[[Category:1896 births]]
[[Category:1986 deaths]]
[[Category:American women historians]]
[[Category:Women art historians]]