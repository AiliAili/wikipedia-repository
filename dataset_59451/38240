{{Good article}}
{{Use British English|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{Infobox University Boat Race
| name= 66th Boat Race
| winner = Oxford
| margin = 3 and 1/2 lengths
| winning_time= 19 minutes 50 seconds
| overall = 30–35
| umpire =  [[Frederick I. Pitman]]<br>(Cambridge)
| date= {{Start date|1909|4|3|df=y}}
| prevseason= [[The Boat Race 1908|1908]]
| nextseason= [[The Boat Race 1910|1910]]
}}

The '''66th Boat Race''' took place on 3 April 1909.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge were reigning champions, having won the [[The Boat Race 1908|previous year's race]], while Oxford's heavier crew contained three Olympic gold medallists.  In a race umpired by [[Frederick I. Pitman]], Oxford won by three-and-a-half lengths in a time of 19 minutes 50 seconds.  It was their first win in four races and took the overall record to 35&ndash;30 in their favour.

==Background==
[[File:Muttlebury SD Vanity Fair 1890-03-22.jpg|right|thumb|Former [[Cambridge University Boat Club]] rower [[Stanley Muttlebury]] coached the Light Blues.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2015, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1908|1908 race]] by two-and-a-half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 20 August 2014}}</ref> while Oxford led overall with 34 victories to Cambridge's 30 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Oxford's coaches were G. C. Bourne who had rowed for the university in the [[The Boat Race 1882|1882]] and [[The Boat Race 1883|1883 races]], Francis Escombe (for the sixth consecutive year), [[Harcourt Gilbey Gold]] (Dark Blue president for the [[The Boat Race 1900|1900 race]] and four-time Blue), W. F. C. Holland who had rowed for Oxford four times between 1887 and 1890, and [[Felix Warre]] (who had rowed in 1898 and 1899).  Cambridge were coached by [[Stanley Muttlebury]], five-time Blue between 1886 and 1890, and David Alexander Wauchope (who had rowed in the [[The Boat Race 1895|1895 race]]).<ref>Burnell, pp. 110&ndash;111</ref>   For the sixth year the umpire was old [[Eton College|Etonian]] [[Frederick I. Pitman]] who rowed for Cambridge in the [[The Boat Race 1884|1884]], [[The Boat Race 1885|1885]] and [[The Boat Race 1886|1886 races]].<ref>Burnell, pp. 49, 108</ref>

Former rower and author George Drinkwater noted that Oxford had "a considerable wealth of material" at their disposal, while Cambridge had "very good [[The Boat Race#Build-up|Trial Eights]]".<ref name=drink123124>Drinkwater, pp. 123&ndash;124</ref>  He went on to describe Oxford as "a very rough crew" upon arrival at Putney.<ref name=drink123>Drinkwater, p. 123</ref>  Conversely, Cambridge "rapidly got together" and despite a late replacement at [[Bow (rowing)|bow]], Cambridge were favourites to win the race.<ref name=drink123/>

==Crews==
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 8.25&nbsp;[[Pound (mass)|lb]] (79.8&nbsp;kg), {{convert|3.25|lb|kg|1}} per rower more than their opponents.  Cambridge's crew contained three rowers with Boat Race experience, including H. E. Kitching, [[Edward Gordon Williams|Edward Williams]] and [[Douglas Stuart (rower)|Douglas Stuart]].  Oxford saw six crew members return to the boat, including [[Alister Kirby]] and [[Albert Gladstone]], both of whom were making their fourth consecutive appearances.  Six occupants of the Dark Blue boat were educated at Eton College.<ref name=burn69>Burnell, p. 69</ref> Oxford's number three, Australian [[Collier Cudmore]], was the only non-British participant registered in the race.<ref>Burnell, p. 39</ref>  Three of the Oxford crew who were studying at [[Magdalen College, Oxford|Magdalen College]], [[Duncan Mackinnon]], [[James Angus Gillan]] and Cudmore, were gold medallists in the [[Rowing at the 1908 Summer Olympics – Men's coxless four|men's coxless four]] at the [[1908 Summer Olympics]].<ref>Burnell, p. 13</ref><ref name=coxless08>{{Cite web | url = http://www.sports-reference.com/olympics/summer/1908/ROW/mens-coxless-fours.html | title = Rowing at the 1908 London Summer Games: Men's Coxless Fours| publisher = [[Sports Reference]] | accessdate = 20 January 2015}}</ref>  The Dark Blues' number two, [[Harold Barker]], and Cambridge's number three, [[Gordon Thomson (rower)]],  won silver in the same event.<ref name=coxless08/>  Thomson also won gold in the [[Rowing at the 1908 Summer Olympics – Men's coxless pair|coxless pair]].<ref> {{cite book | last = Cook | first = Theodore Andrea | year = 1908 | title = The Fourth Olympiad, Being the Official Report | publisher = British Olympic Association | location = London}}</ref> Cambridge's number five, [[Edward Gordon Williams]], won a bronze medal in the [[Rowing at the 1908 Summer Olympics – Men's eight|men's eight]] in the same Olympiad.<ref>{{Cite web | url = http://www.sports-reference.com/olympics/athletes/wi/edward-williams-1.html | title = Edward Williams | publisher = [[Sports Reference]] | accessdate = 20 January 2015}}</ref>  Six of the Oxford crew were studying at Magdalen College, while five of Cambridge's were matriculated at [[Trinity Hall, Cambridge|Trinity Hall]].<ref name=burn69/>
{{multiple image
| align = right
| direction = horizontal
| width1 = 170
| image1 =Mackinnon Duncan The World 1910.jpg
| width2 = 158
| image2 =Stuart DCR Vanity Fair 1907-03-13.jpg
| footer =[[Duncan Mackinnon]] ''(left)'' rowed at number five for Oxford while [[Douglas Stuart (rower)|Douglas Stuart]] [[Stroke (rowing)|stroked]] the Cambridge boat.
}}
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[Albert Gladstone|A. C. Gladstone]] || [[Christ Church, Oxford|Christ Church]] || 11&nbsp;st 6.5&nbsp;lb || R. W. M. Arbuthnot || [[Trinity College, Cambridge|3rd Trinity]] || 10&nbsp;st 2&nbsp;lb
|-
| 2 || [[Harold Barker|H. R. Barker]] ||  [[Christ Church, Oxford|Christ Church]] || 12&nbsp;st 5&nbsp;lb || H. E. Swanston || [[Jesus College, Cambridge|Jesus]] || 12&nbsp;st 4&nbsp;lb
|-
| 3 ||  [[Collier Cudmore|C. R. Cudmore]] || [[Magdalen College, Oxford|Magdalen]] || 12&nbsp;st 4&nbsp;lb || [[Gordon Thomson (rower)|G. L. Thomson]] || [[Trinity Hall, Cambridge|Trinity Hall]] || 12&nbsp;st 6&nbsp;lb
|-
| 4 || [[Stanley Garton|A. S. Garton]] ||  [[Magdalen College, Oxford|Magdalen]] || 13&nbsp;st 8.5&nbsp;lb || H. E. Kitching || [[Trinity Hall, Cambridge|Trinity Hall]] || 12&nbsp;st 12&nbsp;lb
|-
| 5 || [[Duncan Mackinnon|D. Mackinnon]] ||  [[Magdalen College, Oxford|Magdalen]] || 13&nbsp;st 3.5&nbsp;lb || [[Edward Gordon Williams|E. G. Williams]] || [[Trinity College, Cambridge|3rd Trinity]] || 13&nbsp;st 0&nbsp;lb
|-
| 6 || [[James Angus Gillan|J. A. Gillan]] ||  [[Magdalen College, Oxford|Magdalen]] || 13&nbsp;st 1&nbsp;lb || J. B. Rosher || [[Trinity College, Cambridge|1st Trinity]] || 14&nbsp;st 0&nbsp;lb
|-
| 7 ||  [[Alister Kirby|A. G. Kirby]] (P) ||  [[Magdalen College, Oxford|Magdalen]] || 13&nbsp;st 10.5&nbsp;lb || E. S. Hornidge || [[Trinity Hall, Cambridge|Trinity Hall]] || 13&nbsp;st 0&nbsp;lb
|-
| [[Stroke (rowing)|Stroke]] || [[Robert Bourne (politician)|R. C. Bourne]] || [[Christ Church, Oxford|Christ Church]] || 10&nbsp;st 13&nbsp;lb || [[Douglas Stuart (rower)|D. C. R. Stuart]] (P) || [[Trinity Hall, Cambridge|Trinity Hall]] || 11&nbsp;st 2&nbsp;lb
|-
| [[Coxswain (rowing)|Cox]] || A. W. F. Donkin || [[Magdalen College, Oxford|Magdalen]] || 8&nbsp;st 8&nbsp;lb || G. D. Compston || [[Trinity Hall, Cambridge|Trinity Hall]] || 8&nbsp;st 10&nbsp;lb
|-
!colspan="7"|Source:<ref name=burn69>Burnell, p. 69</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the toss and elected to start from the Surrey station, handing the Middlesex side of the river to Oxford.<ref name=ross228/>  Drinkwater described the conditions as "perfect" and umpire Pitman started the race at 12:38&nbsp;p.m.  Both crews made strong starts but Oxford edged ahead and held a third of a length lead after a minute which they extended to a half-length after two minutes.<ref name=drink123124/>  The Cambridge stroke Stuart kept the Light Blue [[stroke rate]] high round the bend to the Mile Post and his counterpart Bourne allowed them to draw level by [[Harrods Furniture Depository]].<ref name=drink124>Drinkwater, p. 124</ref>

As the crews passed below [[Hammersmith Bridge]], Cambridge held a slender lead.  Despite a number of spurts from Oxford, the Light Blues held on to the lead, but relinquished it briefly before Stuart spurted once again as the crews passed into Corney Reach to re-take the lead.  Oxford pushed on {{convert|300|yd|m}} before [[Barnes Railway Bridge|Barnes Bridge]] and left Cambridge behind "as if standing".<ref name=drink124/>  Clear at Barnes Bridge, Oxford extended their lead with every stroke and won by three and a half lengths in a time of 19 minutes 50 seconds.<ref name=ross228>Ross, p. 228</ref>  It was Oxford's first victory in four years and took the overall record in the event to 35&ndash;30 in their favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-9500638-7-4 | publisher = Precision Press}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}
*{{Cite book | title = The Boat Race | first = Gordon |last = Ross | authorlink=Gordon Ross (writer)|publisher = The Sportmans Book Club| year = 1956| isbn=978-0-333-42102-4}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1909}}
[[Category:1909 in English sport]]
[[Category:The Boat Race]]
[[Category:April 1909 sports events]]