{{About|a media file format|ISO in reference to the ISO image file format|ISO image|other uses|ISO (disambiguation)}}
{{Infobox file format
| name = ISO base media file format (MPEG-4 Part 12)
| icon = 
| logo =
| extension = 
| mime = 
| type code = 
| uniform type = 
| magic = 
| owner = [[International Organization for Standardization|ISO]]
| genre = [[Media container]]
| container for = Audio, video, text, data
| contained by = 
| extended from = [[.MOV|QuickTime .mov]]
| extended to = [[MP4]], [[3GP]], 3G2, [[JPEG 2000#Motion JPEG 2000|.mj2]], .dvb, .dcf, .m21
| standard = ISO/IEC 14496-12, ISO/IEC 15444-12
}}

'''ISO base media file format''' ([[International Organization for Standardization|ISO]]/[[International Electrotechnical Commission|IEC]] 14496-12 - MPEG-4 Part 12) defines a general structure for time-based [[multimedia]] files such as video and audio. <ref name="iso-mpeg4part12">{{cite paper
  | author = [[International Organization for Standardization|ISO]]
  | title = Information technology -- Coding of audio-visual objects -- Part 12: ISO base media file format; ISO/IEC 14496-12:2008
  | publisher = International Organization for Standardization
  | year = 2008
  | url = http://standards.iso.org/ittf/PubliclyAvailableStandards/c051533_ISO_IEC_14496-12_2008.zip
  | format = PDF
  | pages = 88, 94
  | accessdate = 2009-07-29 }}</ref><ref name="ISO-14496-12">{{cite web
 | url = http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=38539
 | author = ISO
 | title = Information technology -- Coding of audio-visual objects -- Part 12: ISO base media file format; ISO/IEC 14496-12:2004
 | year = 2004
 | accessdate = 2009-07-29 }}</ref>
The identical text is published as ISO/IEC 15444-12 (JPEG 2000, Part 12).<ref name="ISO-15444-12">{{cite web
 | url = http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=38612
 | author = ISO
 | title = Information technology -- JPEG 2000 image coding system -- Part 12: ISO base media file format; ISO/IEC 15444-12:2004
 | year = 2004
 | accessdate = 2009-07-29 }}</ref>

It is designed as a flexible, extensible format that facilitates interchange, management, editing and presentation of the media. The presentation may be local, or via a network or other stream delivery mechanism. The file format is designed to be independent of any particular network protocol while enabling support for them in general.<ref name="ISO-14496-12" /> It is used as the basis for other media [[file format]]s (e.g. [[container format (digital)|container format]]s [[MP4]] and [[3GP]]).

== History ==
ISO base media file format is directly based on [[Apple Computer|Apple]]’s [[QuickTime File Format|QuickTime container format]].<ref name="mp4ra-org-references">{{cite web
 | url = http://www.mp4ra.org/specs.html
 | author = mp4ra.org&nbsp;— MP4 Registration authority
 | title = References, MPEG-4 Registration authority
 | year = 
 | accessdate = 2009-07-29
}}</ref><ref name="mpeg4part12">{{cite paper
  | author = [[International Organization for Standardization|ISO]]
  | title =  ISO Base Media File Format white paper - Proposal
  | publisher = archive.org
  | date = April 2006
  | url = http://www.chiariglione.org/mpeg/technologies/mp04-ff/index.htm
   | accessdate = 2009-12-26 |archiveurl = https://web.archive.org/web/20080714101745/http://www.chiariglione.org/mpeg/technologies/mp04-ff/index.htm |archivedate = 2008-07-14}}</ref><ref name="mpeg4part12--old">{{cite paper
  | author = [[International Organization for Standardization|ISO]]
  | title =  MPEG-4 File Formats white paper - Proposal
  | publisher = archive.org
  | date = October 2005
  | url = http://www.chiariglione.org/mpeg/technologies/mp04-ff/index.htm
   | accessdate = 2009-12-26 |archiveurl = https://web.archive.org/web/20080115035235/http://www.chiariglione.org/mpeg/technologies/mp04-ff/index.htm |archivedate = 2008-01-15}}</ref><ref name="mpeg4part12--new">{{cite paper
  | author = [[International Organization for Standardization|ISO]]
  | title =  ISO Base Media File Format white paper - Proposal
  | publisher = chiariglione.org
  | date = October 2009
  | url = http://mpeg.chiariglione.org/technologies/mpeg-4/mp04-ff/index.htm
  | accessdate = 2009-12-26 }}</ref><ref name="digitalpreservation-mpeg4part12">{{cite web
  | url = http://www.digitalpreservation.gov/formats/fdd/fdd000079.shtml
  | author = Library of Congress
  | title =  ISO Base Media File Format
  | date = 2005-06-27
  | accessdate = 2009-07-29 }}</ref> It was developed by [[MPEG]] ([[ISO/IEC JTC1]]/SC29/WG11). The first MP4 file format specification was created on the basis of the QuickTime format specification published in 2001.<ref name="quicktime-format-2001">{{cite web
 | url = http://developer.apple.com/standards/classicquicktime.html
 | author = Apple Inc.
 | title = Classic Version of the QuickTime File Format Specification
 | year = 2001
 | accessdate = 2009-07-29
}}</ref> The MP4 file format known as "version 1" was published in 2001 as ISO/IEC 14496-1:2001, as revision of the MPEG-4 Part 1: Systems.<ref name="digitalpreservation-ISO-14496-1-2001">{{cite web
 | url = http://www.digitalpreservation.gov/formats/fdd/fdd000037.shtml
 | author = Library of Congress
 | title = MPEG-4 File Format, Version 1
 | year = 2001
 | accessdate = 2009-07-29
}}</ref><ref name="rfc4337">{{cite web
 | url = http://tools.ietf.org/html/rfc4337#page-4
 | author = Network Working Group
 | title = MIME Type Registration for MPEG-4
 | year = 2006
 | accessdate = 2009-07-29
}}</ref><ref name="ISO-14496-1">{{cite web
 | url = http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=34903
 | author = International Organization for Standardization
 | title = MPEG-4 Part 1: Systems; ISO/IEC 14496-1:2001
 | year = 2001
 | accessdate = 2009-07-29
}}</ref> In 2003, the first version of MP4 file format was revised and replaced by MPEG-4 Part 14: MP4 file format (ISO/IEC 14496-14:2003), commonly known as MPEG-4 file format "version 2".<ref name="digitalpreservation-ISO-14496-14-2003">{{cite web
 | url = http://www.digitalpreservation.gov/formats/fdd/fdd000155.shtml
 | author = Library of Congress
 | title = MPEG-4 File Format, Version 2
 | year = 2003
 | accessdate = 2009-07-29
}}</ref> The MP4 file format was generalized into the ISO Base Media File format (ISO/IEC 14496-12:2004 or ISO/IEC 15444-12:2004), which defines a general structure for time-based media files. It is used as the basis for other file formats in the family such as MP4, 3GP, [[JPEG 2000#Motion JPEG 2000|Motion JPEG 2000]]).<ref name="mp4ra-org-references" />

{| class="wikitable sortable"
|+MPEG-4 Part 12 / JPEG 2000 Part 12 editions<ref name="mpeg-standards">{{cite web|url=http://mpeg.chiariglione.org/standards.htm |title=MPEG standards - Full list of standards developed or under development |author=MPEG |publisher=chiariglione.org |accessdate=2009-10-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20100420192552/http://mpeg.chiariglione.org/standards.htm |archivedate=2010-04-20 |df= }}</ref>
|-
! Edition
! Release date
! Latest amendment
! Standard
! Description
|-
| First edition
| 2004<ref name="ISO-14496-12" />
| 
| ISO/IEC 14496-12:2004, ISO/IEC 15444-12:2004
|
|-
| Second edition
| 2005<ref>{{cite web | url=http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=41828 | title=ISO/IEC 14496-12:2005 - Information technology -- Coding of audio-visual objects -- Part 12: ISO base media file format | author=ISO | publisher=ISO | accessdate=2009-10-31}}</ref><ref>{{cite web | url=http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=41827 | title=ISO/IEC 15444-12:2005 - Information technology -- JPEG 2000 image coding system -- Part 12: ISO base media file format | author=ISO | publisher=ISO | accessdate=2009-10-31}}</ref>
| 2008
| ISO/IEC 14496-12:2005, ISO/IEC 15444-12:2005
| 
|-
| Third edition
| 2008<ref name="iso-mpeg4part12" /><ref>{{cite web | url=http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=51533 | title=ISO/IEC 14496-12:2008 - Information technology -- Coding of audio-visual objects -- Part 12: ISO base media file format | author=ISO | publisher=ISO | accessdate=2009-10-31}}</ref><ref>{{cite web | url=http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=51537 | title=ISO/IEC 15444-12:2008 - Information technology -- JPEG 2000 image coding system -- Part 12: ISO base media file format | author=ISO | publisher=ISO | accessdate=2009-10-31}}</ref>
| 2009<ref>{{cite web |url=http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=52356 |title=ISO/IEC 14496-12:2008/Amd 1:2009 - General improvements including hint tracks, metadata support and sample groups |author=ISO |accessdate=2009-12-30}}</ref> (next expected in 2013<ref name="mpeg-standards" />)
| ISO/IEC 14496-12:2008, ISO/IEC 15444-12:2008
| 
|-
| Fourth edition
| 2012<ref>{{cite web |url=http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=61988 |title=ISO/IEC DIS 14496-12 Information technology -- Coding of audio-visual objects -- Part 12: ISO base media file format |author=ISO |date=2012-04-25 |accessdate=2012-04-28}}</ref>
| 
| ISO/IEC 14496-12:2012, ISO/IEC 15444-12:2012
| 
|}

== Extensions ==
[[File:Relations between ISO MP4 3GPP and 3GPP2 file format.svg|250px|thumb|right|Relations between ISO Base Media File Format, MP4 File Format, 3GPP file format and 3GPP2 file format. Based on the 3GPP2 technical specification published on 18 May 2007.<ref name="3gpp2-vs-3gpp" />]]
The ISO base media file format is designed as extensible file format. List of all registered extensions for ISO Base Media File Format is published on the official registration authority website [http://www.mp4ra.org www.mp4ra.org]. The registration authority for code-points (identifier values) in "MP4 Family" files is Apple Inc. and it is named in Annex D (informative) in MPEG-4 Part 12.<ref name="mpeg4part12" /> Codec designers should register the codes they invent, but the registration is not mandatory<ref name="ftyp-mp4ra">{{cite web
 | url = http://www.ftyps.com/mp4reg.html
 | author = Steven Greenberg
 | title = Registration of ftyp's
 | year = 2009
 | accessdate = 2009-07-29
}}</ref> and some of invented and used code-points are not registered.<ref name="ftyp">{{cite web
 | url = http://www.ftyps.com/
 | author = Steven Greenberg
 | title = Complete List of all known MP4 / QuickTime 'ftyp' designations
 | year = 2009
 | accessdate = 2009-07-29
}}</ref> When someone is creating a new specification derived from the ISO base media file format, all the existing specifications should be used both as examples and a source of definitions and technology. If an existing specification already covers how a particular media type is stored in the file format (e.g. MPEG-4 audio or video in MP4), that definition should be used and a new one should not be invented.<ref name="mpeg4part12" />

[[Moving Picture Experts Group|MPEG]] has standardized a number of specifications extending the ISO base media file format: The MP4 file format (ISO/IEC 14496-14) defined some extensions over ISO base media file format to support [[MPEG-4]] visual/audio codecs and various MPEG-4 Systems features such as object descriptors and scene descriptions. The [[MPEG-4 Part 3]] (MPEG-4 Audio) standard also defined storage of some audio compression formats. Storage of MPEG-1/2 Audio ([[MP3]], [[MP2 (format)|MP2]], [[MP1]]) in the ISO base media file format was defined in ISO/IEC 14496-3:2001/Amd 3:2005.<ref name="ISO-14496-3-mpeg1-2">{{cite web
 | url = http://www.iso.org/iso/catalogue_detail.htm?csnumber=39584
 | author = ISO
 | title = MPEG-1/2 audio in MPEG-4, ISO/IEC 14496-3:2001/Amd 3:2005
 | year = 2005
 | accessdate = 2009-10-12
}}</ref> The Advanced Video Coding (AVC) file format (ISO/IEC 14496-15) defined support for [[H.264/MPEG-4 AVC]] video compression.<ref name="ISO-14496-15">{{cite web
 | url = http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=38573
 | author = International Organization for Standardization
 | title = Information technology -- Coding of audio-visual objects -- Part 15: Advanced Video Coding (AVC) file format; ISO/IEC 14496-15:2004
 | year = 2004
 | accessdate = 2009-07-29
}}</ref> The [[High Efficiency Image File Format]] (HEIF) is an image container format using the ISO base media file format as the basis. While HEIF can be used with any image compression format, it specifically includes the support for [[High Efficiency Video Coding|HEVC]] intra-coded images and HEVC-coded image sequences taking advantage of inter-picture prediction. 

Some of the above-mentioned MPEG standard extensions are used by other formats based on ISO base media file format (e.g. 3GP).<ref name="3gpp2-vs-3gpp">{{cite paper
  | author = 3GPP2
  | title = 3GPP2 C.S0050-B Version 1.0, 3GPP2 File Formats for Multimedia Services
  | publisher = 3GPP2
  | date = 18 May 2007
  | url = http://www.3gpp2.org/Public_html/specs/C.S0050-B_v1.0_070521.pdf
  | format = PDF
  | pages = 67, 68
  | accessdate = 2009-07-29}}</ref>  The 3GPP file format (.3gp) specification also defined extensions to support [[H.263]] video, [[AMR-NB]], [[AMR-WB]], [[AMR-WB+]] audio and [[3GPP Timed Text]] in files based on the ISO base media file format.<ref name="3gp-specification">ETSI 3GPP [http://www.3gpp.org/ftp/Specs/html-info/26244.htm 3GPP TS 26.244; Transparent end-to-end packet switched streaming service (PSS); 3GPP file format (3GP)] Retrieved on 2009-07-29.</ref> The 3GPP2 file format (.3g2) defined extensions for usage of [[EVRC]], [[Selectable Mode Vocoder|SMV]] or 13K ([[QCELP]]) voice compression formats.<ref name="3gpp2-vs-3gpp" /> The [[JPEG 2000]] specification (ISO/IEC 15444-3) defined usage of [[Motion JPEG 2000]] video compression and uncompressed audio ([[Pulse-code modulation|PCM]]) in ISO base media file format (.mj2). The "DVB File Format" (.dvb) defined by DVB Project allowed storage of [[Digital Video Broadcasting|DVB]] services in the ISO base media file format. It allows the storage of audio, video and other content in any of three main ways: encapsulated in a [[MPEG transport stream]], stored as a reception hint track; encapsulated in an [[Real-time transport protocol|RTP]] stream, stored as a reception hint track or stored directly as media tracks.<ref>ETSI (2008-11-18) [http://webapp.etsi.org/workprogram/Report_WorkItem.asp?WKI_ID=28812 ETSI TS 102 833 V1.1.1 (2008-11); Digital Video Broadcasting (DVB); File Format Specification for the Storage and Playback of DVB Services], Retrieved on 2009-08-07</ref><ref>DVB Project (June 2008) [http://www.dvb.org/technology/standards/a121.tm3904r3.ff0020r12.DVB_File_Format_Specification.pdf DVB BlueBook A121 - File Format Specification for the Storage and Playback of DVB Services], Retrieved on 2009-08-07</ref> The [[MPEG-21]] File Format (.m21, .mp21) defined the storage of an MPEG-21 [[Digital Item]] in ISO base media file format, with some or all of its ancillary data (such as movies, images or other non-XML data) within the same file.<ref>ISO (April 2006) [http://mpeg.chiariglione.org/technologies/mpeg-21/mp21-ff/index.htm MPEG-21 File Format white paper - Proposal], chiariglione.org, Retrieved on 2009-08-14</ref><ref>{{cite web |url=http://www.chiariglione.org/mpeg/technologies/mp21-ff/index.htm  |author=ISO |publisher=archive.org |title=MPEG-21 File Format white paper - Proposal |year=2006–04 |accessdate=2009-12-28 |archiveurl = https://web.archive.org/web/20080117150836/http://www.chiariglione.org/mpeg/technologies/mp21-ff/index.htm |archivedate = 2008-01-17}}</ref> The [[OMA DRM]] Content Format (.dcf) specification from [[Open Mobile Alliance]] defined the content format for [[Digital rights management|DRM]] protected encrypted media objects and associated metadata.<ref>Open Mobile Alliance (2008-07-23) [http://www.openmobilealliance.org/Technical/release_program/docs/CopyrightClick.aspx?pck=DRM&file=V2_0_2-20080723-A/OMA-TS-DRM_DCF-V2_0_2-20080723-A.pdf DRM Content Format 2.0.2], Retrieved on 2009-08-14</ref><ref>Open Mobile Alliance (2008-10-14) [http://www.openmobilealliance.org/Technical/release_program/docs/copyrightclick.aspx?pck=DRM&file=V2_1-20081106-A/OMA-TS-DRM_DCF-V2_1-20081014-A.pdf DRM Content Format 2.1], Retrieved on 2009-08-14</ref> There are also other extensions, such as [[Internet Streaming Media Alliance|ISMA]] [[ISMACryp]] specification for encrypted/protected audio and video,<ref>[[Internet Streaming Media Alliance|ISMA]] (2007-11-15) [http://www.isma.tv/specs/ISMA_E&Aspec2.0.pdf ISMA Encryption and Authentication, Version 2.0] {{webarchive|url=https://web.archive.org/web/20130605224839/http://www.isma.tv/specs/ISMA_E%26Aspec2.0.pdf |date=2013-06-05 }}, Retrieved on 2009-08-14</ref><ref>[[Internet Streaming Media Alliance|ISMA]] (2009) [http://www.isma.tv/spec-request.html ISMA Technical Specifications] {{webarchive|url=https://web.archive.org/web/20090910034756/http://www.isma.tv/spec-request.html |date=2009-09-10 }}, Retrieved on 2009-08-14</ref> [[G.719]] audio compression specification,<ref>ITU-T (2009) [http://www.itu.int/rec/T-REC-G.719/en ITU-T Recommendation G.719] Retrieved on 2009-08-14</ref> [[Dolby AC-3|AC3]] and [[E-AC-3]] audio compression,<ref>ETSI (2008-08-20) [http://webapp.etsi.org/workprogram/Report_WorkItem.asp?WKI_ID=28541 TS 102 366 - Digital Audio Compression (AC-3, Enhanced AC-3) Standard], Retrieved on 2009-08-14</ref> [[DTS audio]] compression,<ref>ETSI (2002-12-18) [http://webapp.etsi.org/workprogram/Report_WorkItem.asp?WKI_ID=17715 TS 102 114 - DTS Coherent Acoustics; Core and Extensions - DTS specification], Retrieved on 2009-08-14</ref> [[Dirac (codec)|Dirac]] video compression,<ref>Dirac Video Compression (2008-07-17) [http://www.diracvideo.org/node/17 ISOM/MP4 registrations complete], Retrieved on 2009-08-08</ref><ref>BBC (2008-07-16) [http://www.diracvideo.org/download/mapping-specs/dirac-mapping-isom-latest.pdf Encapsulation of Dirac in ISO Base Media file format derivatives], Retrieved on 2009-08-08</ref> [[VC-1]] video compression specification and others, which are named on the MP4 Registration authority's website.<ref>mp4ra.org&nbsp;— MP4 Registration authority [http://www.mp4ra.org/codecs.html MP4 Registration Authority - Codecs], Retrieved on 2009-07-29</ref>

There are some extensions over ISO base media file format, which were not registered by the MP4 Registration authority. Adobe Systems introduced in 2007 new F4V file format for [[Flash Video]] and declared that it is based on the ISO base media file format. The F4V file format was not registered by the MP4 registration authority, but the F4V technical specification is publicly available. This format can contain H.264 video compression and MP3 or AAC audio compression. In addition, F4V file format can contain data corresponding to the [[Action Message Format|ActionScript Message Format]] and still frame of video data using image formats [[GIF]], JPEG and PNG.<ref name="ftyp" /><ref name="f4v">{{cite paper|author=Adobe Systems Incorporated |title=Video File Format Specification, Version 10 |publisher=Adobe Systems Incorporated |date=18 May 2007 |url=https://www.adobe.com/devnet/flv/pdf/video_file_format_spec_v10.pdf |format=PDF |pages=17–44 |accessdate=2009-08-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20090206025343/http://www.adobe.com/devnet/flv/pdf/video_file_format_spec_v10.pdf |archivedate=February 6, 2009 }}</ref><ref name="kaourantin">{{cite paper  | publisher = Kaourantin.net  | date = October 31, 2007  | author = Tinic Uro | url = http://www.kaourantin.net/2007/10/new-file-extensions-and-mime-types.html | title = New File Extensions and MIME Types  | accessdate =  2009-08-03}}</ref>
Microsoft Corporation announced in 2009 a file format based on the ISO base media file format&nbsp;— ISMV (Smooth Streaming format), also known as Protected Interoperable File Format (PIFF). As announced, this format can for example contain VC-1, [[Windows Media Audio|WMA]], H.264 and AAC compression formats.<ref name="ms">{{cite paper
  | author = Microsoft Corporation
  | title = IIS Smooth Streaming Technical Overview
  | publisher = Microsoft Corporation
  | date = March 2009
  | url = http://download.microsoft.com/download/4/2/4/4247C3AA-7105-4764-A8F9-321CB6C765EB/IIS_Smooth_Streaming_Technical_Overview.pdf
  | format = PDF
  | pages = 11–16
  | accessdate = 2009-08-04}}</ref> Microsoft published a Protected Interoperable File Format (PIFF) specification in 2010. It defined another usage of multiple encryption and DRM systems in a single file container.<ref>{{cite paper
  | author = Microsoft
  | title = The portable interoperable file format (PIFF), Version 10
  | publisher = Microsoft
  | date = 9 Mar 2010
  | url = http://go.microsoft.com/?linkid=9682897
  | format = PDF
  | pages = 1–32
  | accessdate = 2010-08-03}}</ref><ref>{{cite web |url=http://learn.iis.net/page.aspx/941/smooth-streaming-primer#about_piff |title=About the Protected Interoperable File Format (PIFF) |author=John Deutscher |date=2010-08-16}}</ref> PIFF brand was registered by the MP4 registration authority in 2010. Some extensions used by this format (e.g. for WMA support) were not registered. Usage of WMA compression format in ISO base media file format was not publicly documented so it’s possible that they may be unsupported by some platforms.<ref>{{cite web
 | url = http://alexzambelli.com/blog/smooth-streaming-faq/
 | author = Alex Zambelli
 | title = Smooth Streaming FAQ
 | year = 2009
 | accessdate = 2009-08-04
}}</ref>

== Technical details ==

ISO base media file format contains the timing, structure, and media information for timed sequences of media data, such as audio-visual presentations. The file structure is object-oriented. A file can be decomposed into basic objects very simply and the structure of the objects is implied from their type.

Files conforming to the ISO base media file format are formed as a series of objects, called "boxes". All data is contained in boxes and there is no other data within the file. This includes any initial signature required by the specific file format. The "box" is an object-oriented building block defined by a unique type identifier and length. It was called "atom" in some specifications (e.g. the first definition of MP4 file format).<ref name="iso-mpeg4part12" />

A presentation (motion sequence) may be contained in several files. All timing and framing (position and size) information must be in the ISO base media file and the ancillary files may essentially use any format. They must be only capable of description by the metadata defined in ISO base media file format.<ref name="iso-mpeg4part12" />

=== File Type Box ===
In order to identify the specifications to which a file based on ISO base media file format complies, brands are used as identifiers in the file format. They are set in a box named File Type Box ('ftyp'), which must be placed in the beginning of the file. It is somewhat analogous to the so-called [[fourcc]] code, used for a similar purpose for media embedded in [[Audio Video Interleave|AVI]] container format.<ref name="ftyp-def">{{cite web
 | url = http://www.ftyps.com/what.html
 | author = Steven Greenberg
 | title = What is an "ftyp", anyway?
 | year = 2009
 | accessdate = 2009-07-30
}}</ref>  A brand might indicate the type of encoding used, how the data of each encoding is stored, constraints and extensions that are applied to the file, the compatibility, or the intended usage of the file. Brands are a printable four-character codes. A File Type Box contains two kinds of brands. One is "major_brand" which identifies the specification of the best use for the file. It is followed by "minor_version", an informative 4 bytes integer for the minor version of the major brand. The second kind of brand is "compatible_brands", which identifies multiple specifications to which the file complies. All files shall contain a File Type Box, but for compatibility reasons with an earlier version of the specification, files may be conformant to ISO base media file format and not contain a File Type Box. In that case they should be read as if they contained an ''ftyp'' with major and compatible brand "mp41" (MP4 v1 - ISO 14496-1, Chapter 13).<ref name="iso-mpeg4part12" /> Many in-use brands (ftyps) are not registered and can be found on some webpages.<ref name="ftyp" />

A multimedia file structured upon ISO base media file format may be compatible with more than one concrete specification, and it is therefore not always possible to speak of a single "type" or "brand" for the file. In this regard, the utility of the [[Multipurpose Internet Mail Extension]] type and file name extension is somewhat reduced. In spite of that, when a derived specification is written, a new file extension will be used, a new MIME type and a new Macintosh file type.<ref name="iso-mpeg4part12" />

=== Streaming ===
The ISO base media file format supports [[streaming media|streaming]] of media data over a network as well as local playback. A file that supports streaming includes information about the data units to stream (how to serve the [[elementary stream]] data in the file over streaming protocols). This information is placed in additional tracks of the file called "hint" tracks. Separate "hint" tracks for different protocols may be included within the same file. The media will play over all such protocols without making any additional copies or versions of the media data. Existing media can be easily made streamable for other specific protocols by the addition of appropriate hint tracks. The media data itself need not be reformatted in any way. The streams sent by the servers under the direction of the hint tracks, need contain no trace of file-specific information. When the presentation is played back locally (not streamed), the hint tracks may be ignored. Hint tracks may be created by an authoring tool, or may be added to an existing file (presentation) by a hinting tool.<ref name="iso-mpeg4part12" /> In media authored for progressive download  the '''moov''' box, which contains the index of frames should precede the movie data '''mdat''' box.<ref name="From Google I/O 2009 - Mastering the Android Media Framework">{{YouTube|-0UmSQeWsJc|From Google I/O 2009 - Mastering the Android Media Framework}}</ref>

== References ==

{{reflist|colwidth=30em}}

== External links ==

* RFC 4281 - The Codecs Parameter for "Bucket" Media Types
* {{cite web|url=http://www.iso.org/iso/home/store/catalogue_ics/catalogue_detail_ics.htm?csnumber=61988 |title=ISO/IEC 14496-12:2012 - Information technology&nbsp;— Coding of audio-visual objects&nbsp;— Part 12: ISO base media file format |publisher=Iso.org |date=2012-09-15 |accessdate=2009-10-18}}
* {{cite web|url=http://standards.iso.org/ittf/PubliclyAvailableStandards/c051533_ISO_IEC_14496-12_2008.zip |title=ISO&nbsp;— Licence Agreement for freely available standards |publisher=Standards.iso.org |date=2006-09-29 |accessdate=2009-10-18}}
* {{cite web|url=http://www.mp4ra.org/codecs.html |title=The 'MP4' Registration Authority |publisher=Mp4ra.org |date= |accessdate=2009-10-18}}
* {{cite web|url=http://www.3gpp.org/ftp/Specs/html-info/26244.htm |title=3GPP specification: 26.244 |publisher=3gpp.org |date= |accessdate=2009-10-18}}
* {{cite web|url=http://www.3gpp2.org/Public_html/specs/C.S0050-B_v1.0_070521.pdf |title=File Format for Multimedia Services |format=PDF |date= |accessdate=2009-10-18}}
* {{cite web|url=http://www.ftyps.com/ |title=Complete List of all known MP4/QT 'ftyp' designations |publisher=Ftyps.com |date= |accessdate=2009-10-18}}
* {{cite web|url=http://mpeg.chiariglione.org/technologies/mpeg-4/mp04-ff/index.htm |title=ISO Base Media File Format white paper - Proposal |publisher=Chiariglione.org |date= |accessdate=2009-12-26}}

{{Compression formats}}
{{MPEG}}

[[Category:Container formats]]
[[Category:MPEG]]