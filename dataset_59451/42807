<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name = IA-63 Pampa
 |image = File:Argentina Air Force FMA IA-63 Pampa Lofting-2.jpg
 |caption = IA-63 in [[Mendoza, Argentina]]
 |alt =
}}{{Infobox aircraft type
 |type = [[Trainer aircraft]]
 |national origin = [[Argentina]]
 |manufacturer = [[Fábrica Argentina de Aviones|FAdeA]]
 |designer =
 |first flight = 6 October 1984
 |introduced = April 1988
 |retired =
 |status = In service
 |primary user = [[Argentine Air Force]]
 |more users =
 |produced = 1984-present
 |number built = 27
 |program cost =
 |unit cost =
 |developed from =
 |variants with their own articles =
}}
|}

The '''IA-63 Pampa''' is an advanced [[trainer aircraft]] with combat capability, produced in [[Argentina]] by [[Fabrica Militar de Aviones]] (FMA) with assistance from [[Dornier Flugzeugwerke|Dornier]] of [[Germany]].

==Design and development==

Preliminary design studies for a replacement for the [[Morane-Saulnier MS-760]] of the [[Argentine Air Force]] started at the [[Fábrica Militar de Aviones]] (FMA) in 1978, with these studies resulting in selection of a proposal powered by a single [[Garrett TFE731]] [[turbofan]] with high, unswept wings. At the same time the FMA signed a partnership agreement with Dornier to develop the new aircraft.<ref name="Flores p59-60">Flores 1987, pp. 59—60.</ref>

Although influenced by the [[Dassault/Dornier Alpha Jet]] design, the Pampa differs in being a smaller aircraft, it is also single-engined and has straight [[Supercritical airfoil|supercritical]] wings rather than the swept ones of the Alpha Jet. It is constructed mainly of [[aluminium]] alloy, with [[carbon-fibre]] used for components such as the air intakes. The crew of two sit in [[tandem]] under a single-piece clamshell canopy.<ref name="Flores p64">Flores 1987, p.64.</ref>  The [[avionics]] systems are also simpler than the Franco-German aircraft, which has an important secondary combat role. The Pampa prototype first flew on 6 October 1984.<ref name="Flores p66">Flores 1987, p.66.</ref>

==Variants==
;IA 63
Production of the initial series has been delayed and hampered by the state of the Argentinian economy, and as a result only 18 production aircraft have been built in the first batch (1988–90) and six in the second batch (2006–07) for the [[Argentine Air Force]]. First deliveries occurred in April 1988.<ref  name="Brasseys 99 p1">Taylor 1999, p.1.</ref> The 18 aircraft are in service, all modernized, with the ''IV Brigada Aérea'' ({{lang-en|IV Air Brigade}}) at [[Mendoza, Argentina|Mendoza]] for the advanced training role of Argentine pilots.<ref>{{cite news|title=Argentina to buy 40 more Pampas |url=http://www.flightglobal.com/articles/2011/09/21/362327/argentina-to-buy-40-more-pampas.html |agency=[[Flight International]] |work=[[Flightglobal.com]] |date={{date|2011-9-21}} |quote=Split between two Mendoza-based units, the 12 surviving Pampas from an original 16-aircraft batch have all been upgraded to near Series II standard. They are expected to be rotated through FAdeA's installations for re-engining, along with six newly-built IA-63s delivered between 2004 and 2008. |archiveurl=http://www.webcitation.org/66aIg2Z9B?url=http://www.flightglobal.com/news/articles/argentina-to-buy-40-more-pampas-362327/ |archivedate=31 March 2012 |deadurl=yes |df=dmy }}</ref>

;Vought Pampa 2000
In the 1990s, LTV/[[Vought]] selected the IA 63 as the basis for the Pampa 2000, which Vought entered into the [[Joint Primary Aircraft Training System]] competition for the [[United States Air Force]].<ref name="jawa92 p2">Lambert 1992, p. 2.</ref>  The Pampa 2000 lost to the Beechcraft/Raytheon entry which became the [[T-6 Texan II]].<ref>{{cite magazine|last=Warwick|first=Graham|title= Turboprop Triumph |work=[[Flight International]] |date=21–27 February 1996| page= 23 |volume = 149|issue= 4511|url= https://www.flightglobal.com/pdfarchive/view/1996/1996%20-%200427.html}}</ref>

;AT-63 Pampa "Phase 2"
With the acquisition of FMA by [[Lockheed Martin]] in 1995,<ref name="AFT">http://www.airforce-technology.com/projects/fma-ia--63-pampa-advance-jet-trainerlight-attack-aircraft/</ref> the Pampa was upgraded with a new engine and a more advanced avionics package compatible with the [[A-4AR Fightinghawk|A-4AR]] and weapons system. This new project was called AT-63 Pampa "Phase 2"(for attack trainer) and was marketed by Lockheed Martin. The only client is the Argentine Air Force.{{citation needed|date=January 2016}}

;IA-63 Pampa GT "Phase 3"
A third batch of forty new units of the redesigned Pampa was announced by FADEA on 10 October 2013.<ref>{{cite news|title= El Gobierno destinó más de u$s180 millones a una fábrica aeronáutica que aún no construyó un solo avión|first1= Juan|last= Graña|newspaper= infobae.com|date= {{Date|2014-10-12}}|url= http://www.infobae.com/2014/08/12/1587028-el-gobierno-destino-mas-us180-millones-una-fabrica-aeronautica-que-aun-no-construyo-un-solo-avion}}&nbsp;{{webarchive |url=https://web.archive.org/web/20150316224807/http://www.infobae.com/2014/08/12/1587028-el-gobierno-destino-mas-us180-millones-una-fabrica-aeronautica-que-aun-no-construyo-un-solo-avion |date=16 March 2015 }}</ref> After a long delay, finally in 2015 a prototype of the third version of the IA-63 Pampa was introduced to the press. However, rampant inflation and a severe recession have made impossible for the Argentine government to finance the manufacturing of any of the promised planes. <ref>http://zona-militar.com/2015/09/22/numeros-imaginarios-y-numeros-reales-la-teoria-del-ia-63-pampa/</ref>

==Specifications (IA 63) ==
[[File:Argentine IA-63 Pampa.JPEG|thumb|right|300px|IA-63 armament]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
|ref=Jane's All The World's Aircraft 1988–89 <ref name="Janes 88 p3-4">Taylor 1988, pp. 3—4.</ref> and Air Force Technology<ref name="AFT"/>
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew= two
|capacity=
|length main= 10.93 m
|length alt= 35 ft 10¼ in
|span main= 9.69 m
|span alt= 31 ft 9¼ in
|height main= 4.29 m 
|height alt= 14 ft 1 in
|area main=15.63 m<sup>2</sup>
|area alt= 168.2 ft<sup>2</sup>
|airfoil=Dornier DoA-7
|empty weight main= 2,821 kg
|empty weight alt=  6,219 lb
|loaded weight main= 3,500 kg
|loaded weight alt= 7,716 lb
|useful load main=
|useful load alt=
|max takeoff weight main=5,000 kg 
|max takeoff weight alt= 11,023 lb
|more general=
|engine (jet)= [[Honeywell TFE731|Garrett TFE731-2-2N]]
|type of jet= [[turbofan]]
|number of jets= 1
|thrust main= 15.57 kN
|thrust alt= 3,500 lbf
|thrust original=
|afterburning thrust main=
|afterburning thrust alt=
|max speed main=819 km/h 
|max speed alt=442 knots, 509 mph
|max speed more=at 7,000 m (22,965 ft)
|cruise speed main= 747 km/h
|cruise speed alt=403 knots, 464 mph
|cruise speed more=at 4,000 m (13,125 ft)
|never exceed speed main=Mach 0.81
|never exceed speed alt=at 9,500 m (31,170 ft)
|stall speed main=
|stall speed alt=
|range main= 1,500 km
|range alt=809 [[nautical mile|nmi]], 932 mi
|ceiling main=12,900 m 
|ceiling alt= 42,325 ft
|climb rate main=30.2 m/s
|climb rate alt=5,950 ft/min
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|guns=1× 30 mm (1.18 in) [[DEFA cannon|Defa-Giat 554]]
|hardpoints= 5
|hardpoint capacity=<br />
**400 kg (880 lb) on inboard underwing pylons
**250 kg (550 lb) on fuselage centerline and outer wing pylons 
| missiles=<br/>
** LAU-32/51 rocket pods
** LAU-10 rocket pods
| bombs=<br/>
** Mk81/ Mk82 cluster bombs
** CBLS 200 practice bombs
|avionics=
}}

==See also==
{{Portal|Aviation}}
{{aircontent|
|related=
* [[Dassault/Dornier Alpha Jet]]
|similar aircraft=
* [[Aermacchi S-211]]
* [[Aero L-39 Albatros]]
* [[BAE Hawk]]
* [[CASA C-101]]
* [[IAR-99]]
* [[PZL I-22 Iryda]]
* [[Soko G-4 Super Galeb]]
}}

==References==
;Notes
{{Reflist|30em}}

;Bibliography
* Flores, Jackson, (Jr). "The Pampa...A Tutor with a Teutonic Flavour". ''Air International'', February 1987, Vol 32 No. 2. Bromley, UK:Fine Scroll. ISSN 0306-5634. pp.&nbsp;59–66, 90.
* Lambert, Mark (ed.) ''Jane's All The World's Aircraft 1992–93''. Coulsdon, UK: Jane's Defence Data, 1992. ISBN 0-7106-0987-6.
* Taylor, John W.R. ''Jane's All The World's Aircraft 1988–89''. Coulsdon, UK:Jane's Defence Data, 1988. ISBN 0-7106-0867-5.
* Taylor, Michael J.H. ''Brassey's World Aircraft & Systems Directory 1999/2000''. London:Brassey's, 1999. ISBN 1-85753-245-7.

==External links==
{{Commons|FMA IA 63 Pampa}}
* [https://web.archive.org/web/20080101032733/http://www.lockheedmartin.com/products/AT63Pampa/index.html AT-63 Official page] at Lockheed Martin website (archived 2008-01-01)
* [http://www.globalsecurity.org/military/world/argentina/pampa-specs.htm  Global Security.org page]
* [http://www.dintel-gid.com.ar/2010/mayo/fidae_2010/articulo70.html Dintel GID Plane specifications]
* [http://www.lanacion.com.ar/1884953-el-avion-pampa-iii-realizo-con-exito-su-primer-vuelo-de-ensayo News on first flight of Pampa III - La Nacion newspaper, 31 March 2016] (accessed 2016-12-11)
{{FMA aircraft}}
{{JPATS}}
{{use dmy dates|date=March 2015}}

[[Category:Argentine military trainer aircraft 1980–1989]]
[[Category:FMA aircraft]]