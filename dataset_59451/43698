<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=H-101 Salto
|image=H101 Salto.jpg
|caption=H101 Salto
}}{{Infobox Aircraft Type
|type=Aerobatic sailplane
|national origin=[[West Germany]]
|manufacturer=[[Start + Flug]]
|designer=[[Ursula Hänle]]
|first flight=6 March {{avyear|1970}}
|introduced=
|number built=72 by 1996
|developed from=[[Glasflügel H-201]]}}|}

The '''H-101 ''Salto''''' ({{lang-en|Loop}}) is an [[aerobatic]] [[Glider (sailplane)|glider]] of glass composite construction, developed in Germany in the 1970s. Based on the [[Glasflügel H-201|Standard Libelle H-201]], it was designed by [[Ursula Hänle]],<ref name="Coates">Coates 1978, p. 101.</ref> widow of [[Eugen Hänle]], former Director of Glasflügel. It was first produced by Start + Flug GmbH Saulgau.<ref name=Coates>{{cite book |title=Jane's World Sailplanes & Motor Gliders|last=Coates |first=Andrew  |year=1980 |publisher=Jane's |location=London |isbn=0-7106-0017-8 |edition=2nd|page=111}}</ref>

==Design and development==
The H-101 differs from the Libelle in having a V-tail, showing its ancestry to the V-tailed Hütter H-30 GFK.<ref name="Coates"/> Four flush-fitting [[Air brake (aircraft)|air brakes]] were fitted to the trailing edges of the wings, replacing the more conventionally sited air brakes of the Standard Libelle. The Salto's air brakes are hinged at their midpoints so that half the surface projects above the wing and half below.<ref name=Coates/>

The Salto prototype first flew on 6 March 1970, and 67 had been delivered by early 1977, when production at Start + Flug GmbH Saulgau ceased. Five more Saltos were built from 1993 to 1996 by the German company "LTB Frank & Waldenberger", bringing total output of Salto gliders to 72.

The Salto was again made available in the late 1980s by [[Doktor Fiberglas]], set up by Ursula Hänle at [[Westerburg]] in [[West Germany]] as the Hänle H 101 Salto, available in utility and aerobatic versions, with the Utility version available with either short or long-span wings.<ref name=JAWA88-89/>

==Specifications (H 101 Aerobatic)==
{{Aircraft specs
|ref=Jane's World Sailplanes & Motor Gliders,<ref name=Coates/><ref name=JAWA88-89>{{cite book |title=Jane's All the World's Aircraft 1988-89 |year=1988 |publisher=Jane's Information Group |location=London |isbn=0-7106-0867-5 |editor=John W.R. Taylor|pages=626}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|crew=1
|length m=5.7
|span m=13.3
|span note=''''A'''' version</li>
::::{{convert|15.5|m|ft|abbr=on}} ''''U'''' version
|height m=0.88
|wing area sqm=8.58
|wing area note=A version</li>
::::{{convert|9.1|m²|sqft|abbr=on}} ''''U'''' version
|aspect ratio=20.6 'A' version</li>
::::{{convert|15.5|m|ft|abbr=on}} ''''U'''' version
|airfoil=
|empty weight kg=182
|empty weight note=''''A'''' version</li>
::::{{convert|187|kg|lb|abbr=on}} ''''U'''' version
|max takeoff weight kg=280
|max takeoff weight note=''''A'''' version</li>
::::{{convert|310|kg|lb|abbr=on}} ''''U'''' version
|more general=
<!--
        Performance
-->
|stall speed kmh=70
|stall speed note=''''A'''' version</li>
::::{{convert|62|kph|kn mph|abbr=on}} ''''U'''' version
|never exceed speed kmh=280
|never exceed speed note=''''A'''' version</li>
::::{{convert|250|kph|kn mph|abbr=on}} ''''U'''' version</li>
::::{{convert|160|kph|kn mph|abbr=on}} on aero-tow</li>
::::{{convert|130|kph|kn mph|abbr=on}} on winch launch</li>
|g limits=+7 -4.9 ''''A'''' version
|roll rate=<!-- aerobatic -->
|glide ratio=34.5 ''''A'''' version at {{convert|94|kph|kn mph|abbr=on}}</li>
::::37 'U' version at {{convert|94|kph|kn mph|abbr=on}}
|sink rate ms=0.6
|sink rate note=''''A'''' version at {{convert|72|kph|kn mph|abbr=on}} at {{convert|250|kg|lb|abbr=on}}</li>
::::{{convert|0.55|m/s|ft/min|abbr=on}} ''''U'''' version at {{convert|72|kph|kn mph|abbr=on}} at {{convert|250|kg|lb|abbr=on}}
|lift to drag=
|wing loading kg/m2=32.6
|wing loading note=''''A'''' version</li>
::::36.1 kg/m² (7.4 lb/sq ft) ''''U'''' version {{convert|13.3|m|ft|abbr=on}} wings</li>
::::34 kg/m² (6.97 lb/sq ft) ''''U'''' version {{convert|15.5|m|ft|abbr=on}} wings
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=
*[[Celair GA-1 Celstar]]
*[[Pilatus PC-11|Pilatus PC-11 AF]]
*[[PZL Bielsko SZD-59|SZD-59 Acro]]
<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of gliders]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Further reading===
* {{cite book |last=Simons |first=Martin |title=Sailplanes 1965-2000 |year=2005 |publisher=EQIP Werbung und Verlag G.m.b.H. |location=Königswinter |isbn=978-3-9808838-1-8 |edition=2nd revised}}
*{{cite book|last1=Woollard|first1=Mike|last2= Mallinson|first2=Peter|title=The Handbook of Glider Aerobatics.|date=1999|publisher=Airlife Pub.|location=Shrewsbury [England]|isbn=978-1840371109}}
*{{cite web| title=EASA.SAS.A.028|url=http://easa.europa.eu/system/files/dfu/EASA.SAS.A.028_Haenle_H101_Salto_issue01.pdf| website=easa.europa.eu |accessdate=19 January 2015|format=pdf}}

==External links==
{{Commons category|H101 Salto}}
*http://www.frankundwaldenberger.de/index.php?firmengeschichte (History of the Frank & Waldenberger company)
*http://www.easa.europa.eu/ws_prod/c/doc/SAS/A.028/EASA.SAS.A.028_Haenle_H101_Salto_issue01.pdf

{{Glasflügel aircraft}}

{{DEFAULTSORT:Glasflugel H-101}}
[[Category:Glider aircraft]]
[[Category:German sailplanes 1970–1979]]
[[Category:Glasflügel aircraft]]
[[Category:V-tail aircraft]]