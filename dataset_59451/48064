'''Ganneious''' is former [[Iroquois]] village, settled by the [[Oneida people|Oneida]], located on the North Shore of [[Lake Ontario]] near the present site of [[Napanee]], [[Ontario]], [[Canada]].:<ref>{{cite web|title=G - Canadian Indian Villages, Towns and Settlements|url=http://www.accessgenealogy.com/native/g-canadian-indian-villages-towns-and-settlements.htm|publisher=Access Genealogy: A Free Genealogy Resource|accessdate=4 February 2014}}</ref><ref name="Hodge2003">{{cite book|author=Frederick Webb Hodge|title=Handbook of American Indians North of Mexico Volume 1/4 A-Z|url=https://books.google.com/books?id=Fp6eAwAAQBAJ&pg=PA487|date=1 July 2003|publisher=Digital Scanning Inc|isbn=978-1-58218-748-8|pages=487–}}</ref>

Ganneious was settled temporarily as part of a mid 17th century push by the Iroquois Confederacy north, from their traditional homeland in New York state.<ref name="Karcich2013">{{cite book|author=Grant Karcich|title=Scugog Carrying Place: A Frontier Pathway|url=https://books.google.com/books?id=JqanejiJ994C&pg=PT36|date=30 March 2013|publisher=Dundurn|isbn=978-1-4597-0752-8|pages=36–}}</ref> The village was one of seven northern bases for the Iroquois from which to hunt beaver and other fur bearers and to control the flow of furs from the north and west to the markets at [[Albany, New York|Albany]]. The village was located on or near the fertile and productive soils of the Hay Bay area, near Fredericksburg and Cataraqui. The exact location of the village has not been determined.<ref name="Parkman1927">{{cite book|author=Francis Parkman|title=Count Frontenac and New France Under Louis XIV|url=https://books.google.com/books?id=y0nEVAYGxjYC&pg=PT133|year=1927|publisher=Library of Alexandria|isbn=978-1-4655-2326-6|pages=133–}}</ref><ref name="FernowO'Callaghan1855">{{cite book|author1=Berthold Fernow|author2=Edmund Bailey O'Callaghan|title=Documents Relating to the Colonial History of the State of New York|url=https://books.google.com/books?id=x2ZBAQAAMAAJ&pg=PA362|year=1855|publisher=Weed, Parsons|pages=362–}}</ref>

In 1673, the French built [[Fort Frontenac]], which is located in modern day [[Kingston, Ontario|Kingston]], Ontario and approximately 40 kilometres from Ganneious. The establishment of the fort had a significant impact on Ganneious. French missionaries made several attempts to encourage the population in Ganneious to resettle closer to the Fort, in order to [[Christianization|Christianize]], [[Europeanisation|Europeanize]] and encourage them to learn trades and farm. In 1675, [[René-Robert Cavelier, Sieur de La Salle]] and [[Louis Hennepin|Father Louis Hennepin]] undertook a journey to Ganneious to convince the Oneida settled there to relocate closer to Fort Frontenac. One account from [[Hennepin's|Father Louis Hennepin]]  suggests that he was successful and able to convince some people from Ganneious to move and settle around the fort:

"While the brink of the lake was frozen, I walk'd upon the ice to an Iroquois village called Ganneious, near to Kente (Quinte), about nine leagues from the fort - in the company of Sieur de La Salle above mentioned. These savages presented us with the flesh of elks and porcupine, which we fed upon. After having discours'd them some time, we returned bringing with us a considerable number of the natives, in order to form a little village of about forty cottages to be inhabited by them lying betwixt the fort and our House of Mission" (Thwaites 1903:47-48).<ref>{{cite journal|last=Adams|first=Nick|title=Iroquois Settlement at Fort Frontenac in the Seventeenth and Early Eighteenth Centuries|journal=Ontario Archaeology|date=1986|issue=46|pages=9–10|url=http://www.ontarioarchaeology.on.ca/publications/pdf/oa46-1-adams.pdf|accessdate=4 February 2014}}</ref>

In June 1687, under pressure from King Louis XIV to capture `prisoners of war for his galleys'  the residence of Ganneious were rounded up and held as captives by [[Jacques-René de Brisay de Denonville, Marquis de Denonville|Jacques Rene de Brisay de Donneville]]. Donneville departed Montreal in June 1687 with 2,000 men. The troops took 200 prisoners from Kente and Ganneious and destroyed both villages.<ref>{{cite book|last=Barr|first=Daniel P.|title=Unconquered: The Iroquois League at War in Colonial America|date=2006|publisher=Praeger Publishers|location=Westport CT|page=87|url=https://books.google.ca/books?id=vi1ROx0PmI4C&pg=PA87&dq=Ganneious&hl=en&sa=X&ei=PT3xUtHzGaa6yQGywYDYDQ&ved=0CEoQ6AEwBDgK#v=onepage&q=Ganneious&f=false}}</ref>  After 1687, all seven Iroquois Villages on the northern shore of Lake Ontario were abandoned.

== Iroquois Villages on the North Shore of Lake Ontario ==

By the late 1660s various Five Nation Iroquois had established seven villages along the shores of Lake Ontario where trails led off into the interior. In addition to Ganneious, the following settlements have been identified by Historian Percy James Robinson<ref>{{cite web|last=Marcel|first=C.M.W.|title=Iroquois origins of modern Toronto|url=http://www.counterweights.ca/2006/08/iroquois/|publisher=counterweights|accessdate=4 February 2014}}</ref>

* Kente - on the [[Bay of Quinte]]
* Kentsio - on [[Rice Lake (Ontario)|Rice Lake]]
* Ganaraske - on the site of present day [[Port Hope, Ontario|Port Hope]]
* [[Bead Hill archaeological site|Ganatsekwyagon]] - at the mouth of the [[Rouge River (Ontario)|Rouge River]]
* [[Teiaiagon]] - at the mouth of the [[Humber River (Ontario)|Humber River]]
* Quinaouatoua (or Tinawatawa) - Near modern day [[Hamilton, Ontario|Hamilton]]

== Fiction ==

Author Gerald Richardson Brown has written a work of historical fiction, ''Road to Ganneious'', which is set in Hay Bay during the 17th and 18th century.

==References==
{{reflist}}

{{Iroquois settlement of the northern shores of Lake Ontario}}

[[Category:Iroquois populated places]]
[[Category:Oneida| ]]
[[Category:First Nations in Ontario]]