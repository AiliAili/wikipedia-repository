{{Use dmy dates|date=June 2013}}
{{Infobox Military Structure
|name=Camp Zama
|location=[[Zama, Kanagawa|Zama]] and [[Sagamihara|Sagamihara, Japan]]
|image= [[File:CampZamaSignalTower.jpg|200px]]
|caption= Two U.S. airmen work atop a signal tower at Camp Zama in June 2002.
|type=Military installation
|built=
|materials=
|used=
|builder=  [[Imperial Japanese Army]]
|ownership= USA, with authority from Japan
|controlledby= [[File:United States Department of the Army Seal.svg|25px]]&nbsp; [[United States Army]]
|garrison=
* United States Army Japan/I Corps (Forward)
* U.S. Army Garrison - Japan
* 441st Military Intelligence Battalion
* Japan Engineer District
* 78th Signal Battalion
* Central Readiness Force, Japan Ground Self-Defense Force
* 4th Engineer Group, Japan Ground Self-Defense Force
* and others
|commanders=
|occupants= [[File:United States Army, Japan - Shoulder sleeve insignia.svg|25px]]&nbsp; [[United States Army, Japan]]<br/>[[File:Flag of JSDF.svg|25px]]&nbsp; [[Japan Ground Self-Defense Force]]
|battles=
}}
{{Infobox airport
| name = Camp Zama Kastner Army Airfield
| nativename = Zama/Kastner Heliport
| nativename-a =
| nativename-r =
| image =
| IATA =
| ICAO = RJTR
| type = Military
| operator = [[United States Army|US Army]]
| city-served =
| location = Camp Zama
| elevation-f = 367
| coordinates = {{coord|35|30|49|N|139|23|37|E|region:JP|display=inline,title}}
| pushpin_map = Japan
| pushpin_label = RJTR
| pushpin_map_caption = Location in Japan
| website = [http://www.usarj.army.mil/ www.usarj.army.mil]
| metric-rwy = yes
| h1-number = 02
| h1-length-f = 1,499&nbsp;×&nbsp;52
| h1-length-m = 457&nbsp;×&nbsp;16
| h1-surface = [[Asphalt]]
| h2-number = 20
| h2-length-f = 1,499&nbsp;×&nbsp;52
| h2-length-m = 457&nbsp;×&nbsp;16
| h2-surface = Asphalt
| footnotes = Source: Japanese [[Aeronautical Information Publication|AIP]] at [[Aeronautical Information Service|AIS Japan]]<ref name="AIP">[http://arquivo.pt/wayback/20160517110850/https://aisjapan.mlit.go.jp/ AIS Japan]</ref>
}}

{{Nihongo|'''Camp Zama'''|キャンプ座間|}} is a [[United States Army]] post located in the cities of [[Zama, Kanagawa|Zama]] and [[Sagamihara, Kanagawa|Sagamihara]], in [[Kanagawa Prefecture]], Japan, about {{convert|40|km|abbr=on}} southwest of Tokyo.

Camp Zama is home to the [[U.S. Army Japan]] (USARJ)/[[I Corps (United States)|I Corps (Forward)]],<ref>{{cite web |url=http://www.stripes.com/article.asp?section=104&article=55619 |title=I Corps setting up shop at Camp Zama |author=Vince Little |date=19 June 2007 |publisher=[[Stars and Stripes (newspaper)]] |accessdate=17 November 2009 }}</ref> the [[U.S. Army Aviation Battalion Japan]] "Ninjas" <!--Army, not Air Force; see talk-->, the [[500th Military Intelligence Brigade (United States)|441st Military Intelligence Brigade]], the Japan Engineer District ([[U.S. Army Corps of Engineers]]), the [[78th Signal Battalion]] and the [[Central Readiness Force]] and 4th Engineer Group of the [[Japan Ground Self-Defense Force]].

==The Camp==
Camp Zama is close to the [[Sagami River]] near the foothills of the [[Tanzawa]] Mountain Range, Kanagawa Prefecture. The installation falls in the Zama City limits while the two housing areas, Camp Zama and [[Sagamihara Housing Area|Sagamihara Family Housing Area]] (SFHA), are located in the adjacent [[Sagamihara City]]. Once considered rural, this area has transformed into an urban area. New housing developments and communities along with shopping centers have increased the population and made traffic extremely congested. Traveling from Tokyo and outlying U.S. military installations to Camp Zama averages from 1.5 to 3 hours depending on the time of day. However traveling from other parts of Kanagawa was made easier with the opening of the nearby Sagamihara/Aikawa Interchange  which connects with the [[Ken-Ō Expressway]] in May 2012. The recommended method to travel to Camp Zama during times of peak road traffic is via the extremely reliable local [[public transportation]] train system. The closest train station to Camp Zama is the [[Odakyū Electric Railway|Odakyū Line's]] [[Sōbudai-mae Station]].

==History==
Camp Zama is located on the former site of the [[Imperial Japanese Army]] Academy, which was named "Sōbudai" ({{lang-ja|相武台}}) by [[Hirohito|Emperor Showa]]. Route 51 is the road to Camp Zama that was specifically built in order for the Emperor to travel to review the graduating classes from [[Machida Station (Odakyu)|Machida Station]]. The Emperor Showa visited Camp Zama in 1937. Camp Zama also houses an emergency shelter for the Emperor, and to this day, it has been maintained by the U.S. Army Garrison Japan. The Camp Zama theater workshop is one of the few remaining buildings from the pre-occupation era. It is a large hall that was used for ceremonies by the Imperial Japanese Army.  Additionally, the former recreation center still stands currently used by the Camp Zama Tours and Travel Office and Boys Scouts, along with others.

In November 1984, [[Mother Teresa]] of [[Kolkata|Calcutta]] visited Camp Zama and spoke to an audience of 1,200 people.<ref> [https://www.facebook.com/USARJ/photos/a.10150104681845728.276750.105103590727/10153050420715728/?type=1&theater U.S. Army in Japan Facebook page November 10, 2015] Retrieved August 16, 2016 </ref><ref> [http://www.usarj.army.mil/about/history/camp_zama_1980s_201602.pdf U.S. Army in Japan - CAMP ZAMA THROUGH THE YEARS....] Retrieved August 16, 2016 </ref>

The camp has been attacked several times by terrorists. First when a bomb was exploded outside the camp in 2002 by the "Revolutionary Army".<ref> [http://www.japantimes.co.jp/news/2002/11/20/national/mortar-attack-believed-behind-camp-zama-blasts/ Mortar attack believed behind Camp Zama blasts November 20, 2002] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref> There was a further attack in 2007, which was speculated to be an Al-Qaida attack<ref> [http://www.japantimes.co.jp/news/2007/02/14/national/camp-zama-blasts-may-be-al-qaida-work-abc/ Camp Zama blasts may be al-Qaida work: ABC February 12, 2007] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref> but responsibility was claimed by so-called "Revolutionary Army" responsible for the 2002 attack.<ref>{{cite web|url=http://www.stripes.com/news/group-claims-responsibility-for-camp-zama-explosions-1.60500|title=Group claims responsibility for Camp Zama explosions|accessdate=2012-02-08|date=19 February 2007|author=Hana Kusumoto and Vince Little|publisher=Stars and Stripes}}</ref> There was another attempted attack in May 2015.<ref>[http://www.japantimes.co.jp/news/2015/04/28/national/camp-zama-apparently-targeted-explosive-projectiles/ Police investigate possible projectile attack on U.S. Army’s Camp Zama April 28, 2015] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref><ref> [http://www.stripes.com/news/pacific/japan/report-police-investigating-explosions-heard-near-camp-zama-1.342790 Report: Police investigating explosions heard near Camp Zama April 27, 2015] ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'' Retrieved August 16, 2016 </ref>

In 2004 [[Charles Robert Jenkins|Charles Jenkins]], a U.S. Army sergeant who had deserted to North Korea in 1965, turned himself into Camp Zama. He was sentenced to a 30-day jail sentence and given a dishonourable discharge.<ref> Takahara, Kanako [http://www.japantimes.co.jp/2004/09/02/announcements/accused-u-s-deserter-jenkins-to-report-soon-to-camp-zama/ Accused U.S. deserter Jenkins to report soon to Camp Zama September 2, 2004] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref><ref> [http://www.japantimes.co.jp/news/2004/11/04/national/timeline-of-jenkins-saga/ Timeline of Jenkins saga November 4, 2004] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref><ref> [http://www.japantimes.co.jp/news/2004/11/04/national/jenkins-gets-30-days-in-jail-dishonorable-discharge/ Jenkins gets 30 days in jail, dishonourable discharge November 4, 2004] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref> He later gained permanent residency in Japan to live with his Japanese wife and family.<ref> [http://www.japantimes.co.jp/news/2008/07/12/national/jenkins-status-of-residency-now-permanent/ Jenkins’ status of residency now permanent July 12, 2008] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref>

In 2005 a live [[anthrax]] sample was sent to the base in error. It was destroyed in 2009.<ref> [http://www.japantimes.co.jp/news/2015/06/13/national/pentagon-sent-live-anthrax-to-japan-in-2005/ U.S. accidentally sent live anthrax to Camp Zama in 2005 June 13, 2015] ''[[Japan Times]]'' Retrieved August 17, 2016 </ref>

In March 2007, [[Michael Jackson]] visited the camp to greet 3,000 plus U.S. troops and their families. Jackson was flown in on a [[Sikorsky UH-60 Black Hawk|Black Hawk helicopter]] from Hardy Barracks in Tokyo and addressed the frenzied crowd at the base's Yano Fitness Center gymnasium:

“It’s an honor and a privilege to be here,” Jackson told the spectators. “You people are among the most special in the world because you haven chosen a life of service. Because of you here today and others who have given their lives, we can enjoy our freedom at home. I thank you from the bottom of my heart, and I love you."

Col. Robert M. Waltemeyer, Commander U.S. Army Garrison Japan, presented Jackson with a Certificate of Appreciation for his devotion to U.S. Military troops and their families.<ref>{{cite web|url=http://webcache.googleusercontent.com/search?q=cache:oE9ASCivi70J:www.examiner.com/article/michael-jackson-was-long-time-u-s-military-supporter&hl=en&gl=uk&strip=1 |archive-url=https://archive.is/20130807040007/http://webcache.googleusercontent.com/search?q=cache:oE9ASCivi70J:www.examiner.com/article/michael-jackson-was-long-time-u-s-military-supporter&hl=en&gl=uk&strip=1 |dead-url=yes |archive-date=7 August 2013 |last=Halaby |first=Valerie |title=Michael Jackson was long-time U.S. Military supporter |publisher=Clarity Digital Group LLC |accessdate=7 August 2013 }}</ref>

In December 2007 headquarters for the [[I Corps (United States)|1st Corps]] was opened at Camp Zama.<ref> [http://www.japantimes.co.jp/news/2007/12/20/national/u-s-army-1st-corps-hqs-in-zama/ U.S. Army 1st Corps HQs in Zama December 20, 2007] Retrieved August 16, 2007</ref>

Personnel from the base assisted with [[Operation Tomodachi]] following and during the March [[2011 Tōhoku earthquake and tsunami]] and [[Fukushima I nuclear accidents]]. During the crisis, around 300 American family members voluntarily departed the base for locations outside Japan.<ref>Reed, Charlie, "[http://www.stripes.com/news/pacific/japan/military-wraps-up-first-round-of-departures-from-japan-1.138869 Military wraps up first round of departures from Japan]", ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'', 25 March 2011, retrieved 28 March 2011.</ref>

In 2013 a handgun went missing at the base, and was reported to police.<ref> [http://www.japantimes.co.jp/news/2013/03/30/national/police-are-alerted-after-handgun-disappears-off-desk-at-camp-zama/ Police are alerted after handgun disappears off desk at Camp Zama March 30, 2013] ''[[Japan Times]]'' Retrieved August 16, 2016 </ref>

== Education ==
The [[United States Department of Defense]] operates several public schools in the base.
* Arnn Elementary School
* Zama Middle School
* Zama American High School

Higher educational opportunities for those in the military and working for the Department of Defense, as well as for family members at Camp Zama are available through several contracted academic institutions. For example:
* [[University of Maryland University College]]<ref> [http://www.asia.umuc.edu UMUC Asia] Retrieved August 17, 2016</ref>
* [[Central Texas College]]

===Arnn Elementary School ===
The Sagamihara Elementary School opened in September 1951 with 300 students, ten teachers, and a principal. It started in a building purchased from the Japanese Government. This original building was destroyed by fire in 1976. Three temporary buildings were constructed in the summer of 1976 on the community play area across the street from the original school site. Later in 1978 three new buildings were completed on the original site and the campus was completed in 1983. These buildings served as the school until the new school replacement project was completed in May 2003. Fall 2003, the new John O. Arnn ES opened.<ref> [http://www.dodea.edu/ArnnES/about.cfm John O. Arnn Elementary School - About Our School] Retrieved August 17, 2016 </ref>

* School motto: "Creating lifelong learners"
* School mascot: The Knight
* School colors: Blue and green

===Zama American High School===
[[File:Zama American High School-2008.jpg|thumb|Zama American High School, 2008.]]
The Zama American High School, also known as ZAHS, first opened in 1959. It was opened to, and continues to serve, American [[Dependent (law)|dependents]] of U.S. Military and civilian employees stationed in the area, as well as U.S. Contractors. It was built at the bottom of "General's Hill" on the north side of Camp Zama and remained there until 1968. In 1968, the school Principal, Mr. Richard A. Pemble, had the high school and Jr. High 'switched', and the high school then occupied two wooden army barracks close to the main gate. The [[barracks]] were the original [[Imperial Japanese Army]] buildings used to house Japanese Imperial army officer candidates during [[World War II]], and subsequently house U.S. troops during the occupation.

In 1980, a new high school was built on the hill near the original site, and the historical barracks were subsequently torn down. The high school still serves the American School community for the U.S. dependents in the Camp Zama, Sagamihara, [[Atsugi Naval Air Facility]] and surrounding  areas.

In 1987, the school split into Zama American Middle School and Zama American High School.

ZAHS has an active alumni association and biyearly reunions that draw members from all over the globe.

Zama American High School celebrated its 50th graduating class anniversary in June, 2009.
In June 2012, the school was placed on accreditation probation by accreditation agency [[AdvancED]].  AdvancED's report cited an "obstructive and negative climate perpetuated by an intimidating, manipulative minority of staff members at the school" as the main source of problems with the learning environment at the school.  In fear of losing its accreditation, school staff had until April 2013 to correct the problem.<ref>Slavin, Erik, "[http://www.stripes.com/news/zama-american-at-risk-of-losing-school-accreditation-after-scathing-independent-report-1.179706 Zama American at risk of losing school accreditation after scathing independent report]", ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'', 6 June 2012</ref>  In response, in April 2012, DoDEA called former Zama High School teacher Bruce Derr out of retirement to serve as principal and turn things around.<ref>Hoff, Charly, "[http://www.dodea.edu/Pacific/newsroom/pressreleases/05112012.cfm Former Zama American High School teacher returns as new Principal]", ''[DoDEA Pacific Press Release'', 11 May 2012</ref> In August 2012 [[Department of Defense Education Activity|DoDEA]] transferred union representative Brian Chance, identified as one of those reportedly contributing to the conflicts between faculty and administrators at the school, to Germany.  One teacher was fired.  Six other teachers were also transferred or elected to retire in lieu of accepting a transfer.  The school met the deadline and is again fully accredited.<ref>Slavin, Erik, "[http://www.stripes.com/news/with-its-accreditation-status-on-probation-zama-eyes-fresh-start-1.187030 With its accreditation status on probation, Zama eyes fresh start]", ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'', 26 August 2012</ref>

In 2012 the DoDEA boss agreed that Zama High School was failing and believed it should receive a D-.  According to the 2012 Report of the Quality Assurance Review Team's report SAT scores and other data is not easily accessible to the parents and the public.<ref> [http://www.stripes.com/news/dodea-boss-f-grade-for-zama-too-harsh-it-s-a-d-1.179863 DODEA boss: ‘F’ grade for Zama too harsh; it’s a ‘D-' June 8, 2012] ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'' Retrieved August 17, 2016</ref>

==Zama American High School SAT Test Scores==
 
In 2013, Zama American High School students scored an average of 1339 points, obtaining a 442 in critical reading, 465 in math, and 432 in writing,<ref> {{cite web|url=http://www.dodea.edu/Pacific/Japan/CampZama/ZamaAmericanHS/upload/zahsdatawall-2.pdf |title=Archived copy |accessdate=2016-03-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20160820015723/http://www.dodea.edu/Pacific/Japan/CampZama/ZamaAmericanHS/upload/zahsdatawall-2.pdf |archivedate=20 August 2016 |df=dmy }} </ref>  ranking well below most inner city schools.

{| class="wikitable"
|-
! Year !! Reading !! Writing !! Math !! Combined
|-
| 2013 || 442 || 432 || 465 || 1339
|-
| 2012 || 492 || 478 || 500 || 1470
|-
| 2011 || 478 || 470 || 466 || 1414
|-
| 2010 || 503 || 513 || 497 || 1513
|-
| 2009 || 504 || 496 || 481 || 1481
|}

==References==
{{Reflist}}

==External links==
*[http://www.zamaalum.net/ Zama Alumni Association Website]
*[http://www.zama-hs.pac.dodea.edu/ Zama American High School Official Website (public)]
*[http://www.usarj.army.mil/ USARJ and I Corps (Forward)] Web site
*[http://www.globalsecurity.org/military/facility/camp-zama.htm http://www.globalsecurity.org/military/facility/camp-zama.htm]
*[http://www.zamaalumni.com Site dedicated to alumni of Camp Zama American High School. Pictures, artifacts, history, memorabilia, etc.]
*[http://www.zamayearbooks.com Yearbooks on-line: Zama American High School yearbooks online - all yearbooks from the first year the school opened in 1959 to 2000.]
*[http://mywebpages.comcast.net/jgbarber65/ The Official 1956th Communications Group/374th Communications Squadron, Operation Location - C Alumni Website.]
*[http://www.globalsecurity.org/military/facility/camp-zama.htm]

{{Japanese airports}}

[[Category:Military bases of the United States in Japan]]
[[Category:United States Army posts|Zama]]
[[Category:Transport in Kanagawa Prefecture]]
[[Category:United States military in Japan]]
[[Category:Heliports in Japan]]
[[Category:Buildings and structures in Sagamihara]]
[[Category:Buildings and structures in Kanagawa Prefecture]]