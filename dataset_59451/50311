{{About|the mobile network operator in the Republic of Congo|Warid Telecom International|Warid Telecom|}}
{{Infobox company
|  name   = Warid Congo SA
|  type   = Telecommunication operator
|  logo   = [[File:Warid Congo.gif]]
|  logo_caption = passer à l'action <br> ''Take action''
|  foundation     = 2008
|  location       = [[Brazzaville]], [[Republic of the Congo]]
|  key_people     = Michel Elame (CEO)
|  location_city    = 
|  location_country = 
|  area_served      =  Brazzaville <br> Pointe – Noire <br>  Dolisie <br> Kinkala <br> N’Kayi <br> Madingou <br> Mindouli <br> Loudima <br> Gamboma <br> Ollombo <br> Oyo <br> Ewo <br> Mengo <br> Yamba <br> Ouesso <br> Ngombe <br> Pokola<ref>http://waridtel.cg/coverage.php</ref>
|  industry       = [[Telecommunication]]
| products = Mobile services <br> Data services <br> Blackberry solution <br> Mobile banking
|  homepage       = [http://www.waridtel.cg www.waridtel.cg]
}}

'''Warid Congo SA''' is a GSM based mobile operator in the [[Republic of the Congo]]. It launched commercial operations in March 14, 2008.<ref>http://www.g2mi.com/company_description.php?id=2531</ref>

Warid Congo is a joint venture between Warid Telecom International and the government of the [[Republic of the Congo|Congo]]. At present, Warid is the country's third largest cellular operator having a subscriber base of 1 million with a market share of 17.7%.<ref>http://www.thehindu.com/business/Industry/bharti-airtel-to-buy-warids-congo-operations/article5317439.ece</ref>

==History==
[[Warid Telecom International]] acquired a [[GSM]] based mobile license on January 2008 less than a week after the commercial launch of Warid Uganda.<ref>http://allafrica.com/stories/200801171031.html</ref> The company launched commercial service in 11 cities and towns in first phase.

==Network==
Warid Congo currently uses GSM based GPRS (2.5G) and Edge (2.75G) technologies.
{| class="wikitable sortable"
|+Frequencies used by Warid network
|-
! Frequency !! Protocol !! Class  
|-
| <center>900&nbsp;MHz<center> || <center>[[GSM]]/[[General Packet Radio Service|GPRS]]/[[Enhanced Data Rates for GSM Evolution|EDGE]]<center> || <center>2G, 2.5G, 2.75G<center>
|}

===National numbering plan===
Warid uses the following numbering scheme:

+242 04 X<sub>1</sub>X<sub>2</sub>X<sub>3</sub>X<sub>4</sub>X<sub>5</sub>X<sub>6</sub>X<sub>7</sub>

Where, 242 is the [[International Subscriber Dialling|ISD]] code for the [[Republic of Congo]] and is required when dialling outside the country, 04 is the national destination code for Warid allocated by [[Agence de Régulation des Postes et des Communications Electroniques, Brazzaville]]. The subscriber number along with national destination code constitute the national significant number and is nine digit long.<ref>http://www.itu.int/oth/T020200002E/en</ref>

==Products and services==
Warid Congo offers [[Prepaid mobile phone|pay-as-you-go]] plans. This prepaid service comes with 64K SIM card. Subscribes can call up any local, national, and international telephone numbers. Such plans include Warid basic prepaid offer, Warid Welcome, Baninga 
friends and family service, and Warid Xpress.

In addition to sending and receiving [[Short Message Service|SMS]] service, voicemail, voice based messaging service SMS Koza, end call notification on call completion, IVR, and USSD recharge are included in the prepaid base Warid connection. Other value added services such as mobile internet and waiting tone under the brand of WaridNet and WaridMusik can also be availed.

==Mobile banking==
In February 2012, Warid Congo announced the launch of Warid Mobicash over its network. Mobicash service facilitates customers to have money transaction even more secure. Moreover, payments can be transferred through the mobile phones and this service is available on all models of mobile phones and carriers. This is simple service, easy to use and open to everyone with no complications to download software and no restrictions for registration.<ref>http://www.prlog.org/11800121-warid-congo-launches-warid-mobicash.html</ref><ref>http://www.itnewsafrica.com/2012/02/warid-congo-launches-mobicash/</ref>

==References==
{{reflist}}

==External links==
* {{Official website|waridtel.cg}}
* {{facebook|117245778317438}}

{{Warid}}

[[Category:Telecommunications companies of Africa]]
[[Category:2008 introductions]]
[[Category:Republic of the Congo]]
[[Category:Communications in the Republic of the Congo]]
[[Category:Economy of the Republic of the Congo]]
[[Category:Companies of the Republic of the Congo]]
[[Category:Industry in the Republic of the Congo]]
[[Category:Lists of companies of the Republic of the Congo]]
[[Category:Telecommunications in the Republic of the Congo]]
[[Category:Warid Telecom]]