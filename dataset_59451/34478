{{Distinguish|1st Guards Brigade (Croatia)}}
{{good article}}
{{Infobox military unit
|unit_name=1st Croatian Guards Corps
|image=Dvadeseta obljetnica formiranja OSRH zastava 1 H G Zbor 270511 1341.jpg
|caption=Corps flag, bearing corps [[shoulder sleeve insignia]]
|dates=1994–2000
|country= [[Croatia]]
|allegiance=
|branch=
|type= [[Special forces]]
|role=
|size= 2,500
|command_structure=
|garrison=[[Zagreb]]
|garrison_label=
|equipment=
|equipment_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|battles=[[Croatian War of Independence]]:
* [[Operation Winter '94]]
* [[Operation Flash]]
* [[Operation Leap 2]]
* [[Operation Summer '95]]
* [[Operation Storm]]
[[Bosnian War]]
* [[Operation Mistral 2]]
* [[Operation Southern Move]]
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment=
|colonel_of_the_regiment_label=
|notable_commanders=[[Mile Ćuk]]<br>[[Miljenko Filipović]]
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
}}
The '''1st Croatian Guards Corps''' ({{lang-hr|1. hrvatski gardijski zbor}}) was a special [[formation (military)|formation]] of the [[Croatian Army]] (''Hrvatska vojska'' – HV) directly subordinated to the [[Ministry of Defence (Croatia)|Ministry of Defence]] rather than the [[General Staff of the Armed Forces of the Republic of Croatia]] and reporting directly to the [[President of Croatia]]. The corps was established in 1994 by the amalgamation of various HV [[special forces]]. The 2,500-strong unit was organised into the '''1st Croatian Guards Brigade''' (''1. hrvatski gardijski zdrug'' – HGZ), a multi-purpose special forces combat unit, and four battalions tasked with ensuring the security of the President of Croatia and carrying out ceremonial duties. The HGZ took part in a number of military operations during the [[Croatian War of Independence]] and the [[Bosnian War]]. It was disbanded in 2000, when its components were amalgamated with other HV units to form the [[Special Operations Battalion (Croatia)|Special Operations Battalion]], the 305th Military Intelligence Battalion, and the [[Honor Guard Battalion (Croatia)|Honour Guard Battalion]].

==Establishment==
[[File:Croatian detachment Bastille Day 2013 Paris t105017.jpg|thumb|right|[[Honor Guard Battalion (Croatia)|Honour Guard Battalion]] troops wearing [[dress uniforms]] first used by the 1st Guards Honour Battalion]]
On 25 February 1994, the [[special forces]] of the [[Croatian Army]] (''Hrvatska vojska'' – HV) were reorganised when all HV's special forces units were combined to form the 1st Croatian Guards Corps (''1. hrvatski gardijski zbor'').{{sfn|Žabec|27 November 2010}} In April, personnel of the 8th [[Military Police]] Light Assault Brigade (itself only established in September 1993) were also transferred to the corps,{{sfn|Thomas|Mikulan|2006|p=27}} ultimately making the corps 2,500-strong. In addition to special forces operations, the corps was also tasked with providing security for the [[President of Croatia]].{{sfn|Žabec|27 November 2010}} The corps was organised into four specialised battalions and the 1st Croatian Guards Brigade (''1. hrvatski gardijski zdrug'' – HGZ), a multi-purpose special forces combat unit.{{sfn|Bilandžić|Milković|2009|loc=note 20}} The HGZ was considered the elite unit of the HV.{{sfn|Bilandžić|Milković|2009|p=49}} The entire corps was directly subordinated to the [[Ministry of Defence (Croatia)|Ministry of Defence]] rather than the [[General Staff of the Armed Forces of the Republic of Croatia]],{{sfn|CIA|2002|p=447}} and reported directly to the president.{{sfn|Thomas|Mikulan|2006|p=25}} It was commanded by [[Major General]] [[Mile Ćuk]]. Ćuk and his deputy were based in the [[Presidential Palace, Zagreb|Presidential Palace]], while the bulk of the corps was based in nearby [[Tuškanac]] barracks.{{sfn|Žabec|27 November 2010}} The HGZ was capable of fielding up to 300 troops in combat.{{sfn|Index.hr|16 April 2011}} It was commanded by [[Colonel]] (later Major General) [[Miljenko Filipović]], who had previously commanded the [[Zrinski Battalion]]—one of the special forces units amalgamated into the corps.{{sfn|Biluš|14 September 2004}}

{| class="plainrowheaders wikitable" style="background:none; cellspacing=2px; text-align:left; font-size:90%;"
|+Croatian Guards Corps order of battle{{sfn|Bilandžić|Milković|2009|loc=note 20}}{{sfn|CIA|2002|p=447}}
|- style="font-size:100%; text-align:right;"
! Unit !! Purpose
|-
| 1st Croatian Guards Brigade || Multi-purpose special forces unit; the brigade included a parachute battalion, an armoured-mechanised battalion, an [[electronic warfare]] battalion and a mixed aviation aquadron.
|-
| 1st Guards Honour Battalion || [[Honour guard]] unit, used in ceremonial occasions, wearing [[dress uniform]]s
|-
| 2nd Guards Assault Battalion || Presidential guard unit, wearing [[battledress]]
|-
| 3rd Guards Naval Honour Assault Battalion || Presidential guard and honour guard unit, headquartered on [[Brijuni]] Islands
|-
| 4th Guards Special Purpose Battalion || Plain clothes bodyguards; The battalion included a motorcycle honour guards platoon.
|}

==Combat service==
The HGZ took part in several battles of the [[Croatian War of Independence]] and the [[Bosnian War]]. In late November and December 1994, it participated in [[Operation Winter '94]], the joint offensive of the HV and the [[Croatian Defence Council]] (''Hrvatsko vijeće obrane'' – HVO) which pushed the [[Army of Republika Srpska]] (''Vojska Republike Srpske'' – VRS) from the western parts of the [[Livanjsko field]] in Bosnia and Herzegovina.{{sfn|Ratković|November 2011}} Elements of the HGZ also saw action along the [[Novska]]–[[Okučani]] axis of advance in the HV's [[Operation Flash]] offensive that took place in western [[Slavonia]] in early May 1995.{{sfn|CIA|2002|loc=p. 399, note 73}} By this time, the HGZ also commanded its own artillery and [[Mil Mi-24]] [[Gunship|helicopter gunships]], in addition to [[Mil Mi-8]] [[transport helicopter]]s.{{sfn|Thomas|Mikulan|2006|p=25}} The HGZ redeployed west of [[Livno]] once again in early June to take part in [[Operation Leap 2]], extending the [[Salients, re-entrants and pockets|salient]] that had been created in late 1994 west towards [[Bosansko Grahovo]] and [[Glamoč]].{{sfn|Marijan|2010|p=57}} The unit participated in the capture of those towns in late July 1995 during [[Operation Summer '95]].{{sfn|CIA|2002|p=365}}

In preparation for [[Operation Storm]], the HV [[4th Guards Brigade (Croatia)|4th Guards]] and [[7th Guards Brigade (Croatia)|7th Guards Brigades]] were pulled back from positions facing the VRS that had been established during Operation Summer '95, and were reoriented south towards the [[Army of the Republic of Serb Krajina]] (ARSK). The ARSK was protecting the northern approaches to [[Knin]] − the capital of the unrecognised [[Republic of Serbian Krajina]] − which Croatia claimed as part of its own territory. As the two brigades turned over the positions north and west of Bosansko Grahovo to the HV 81st Guards Battalion, the HGZ was deployed to the rear of the battalion, tasked with intervening in case of any VRS attack towards Bosansko Grahovo.{{sfn|Marijan|2010|pp=79–80}} On the second day of the operation, 6 August, after Knin was captured by the HV, the HGZ was airlifted from the Livanjsko field to the village of Rovanjska north of [[Zadar]].{{sfn|Marijan|2010|loc=note 198}} They then linked up with the 2nd Battalion of the [[9th Guards Brigade (Croatia)|9th Guards Brigade]] and advanced east to capture the villages of Muškovac and [[Kaštel Žegarski]].{{sfn|Marijan|2010|pp=86–87}} On 8 August, the HGZ participated in an operation against the last significant ARSK pocket in the area of [[Donji Lapac]] and [[Srb]], alongside the three guards brigades and [[Croatian special police order of battle in 1991–95|special police forces]].{{sfn|CIA|2002|p=374}}

In September 1995, the HGZ took part in [[Operation Mistral 2]], which extended HV and HVO control in western Bosnia and Herzegovina and captured the towns of [[Jajce]], [[Šipovo]] and [[Drvar]], moving the confrontation line north towards the [[Bosnian Serb]] capital of [[Banja Luka]].{{sfn|CIA|2002|p=381}} In October, the HGZ also participated in [[Operation Southern Move]], which captured the town of [[Mrkonjić Grad]], and reached the southern slopes of Mount [[Manjača]], {{convert|25|km|abbr=off}} south of Banja Luka.{{sfn|CIA|2002|pp=390–391}}

==Reorganisation==
The corps was disbanded in 2000 and its constituents reorganised.{{sfn|Bilandžić|Milković|2009|p=50}} A part of the HGZ was amalgamated with the Special Combat Skills Centre Šepurine to form the [[Special Operations Battalion (Croatia)|Special Operations Battalion]].{{sfn|MORH|29 April 2013}} The remainder of the brigade was amalgamated with the Reconnaissance-Sabotage Company based in [[Pula]], the 350th Sabotage Detachment, the 280th Unmanned Aerial Vehicle Platoon, and the 275th Electronic Warfare Company to form the 305th Military Intelligence Battalion.{{sfn|Vlahović|October 2013}} The elements of the corps which were tasked with security of the President of Croatia and ceremonial duties were reformed and the [[Honor Guard Battalion (Croatia)|Honour Guard Battalion]] was established in their place.{{sfn|Žabec|27 November 2010}}

==Footnotes==
{{reflist|20em}}

==References==
;Books
{{refbegin|60em}}
*{{cite book|ref={{harvid|CIA|2002}}|url=https://books.google.com/books?id=it1IAQAAIAAJ|title=Balkan Battlegrounds: A Military History of the Yugoslav Conflict, 1990–1995|publisher=Central Intelligence Agency|author=[[Central Intelligence Agency]], Office of Russian and European Analysis|year=2002|isbn=978-0-16-066472-4|location=Washington, D.C.}}
*{{cite book|ref=harv|url=http://centardomovinskograta.hr/pdf/izdanja2/1-400_engleski_oluja_final_25_05_10-opt.pdf|format=PDF|first=Davor|last=Marijan|title=Storm|publisher=Croatian Homeland War Memorial & Documentation Centre|location=Zagreb, Croatia|isbn=978-953-7439-25-5|year=2010}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=G5Px01NrM7QC|title=The Yugoslav Wars (1): Slovenia & Croatia 1991–95|first1=Nigel|last1=Thomas|first2=Krunislav|last2=Mikulan|publisher=[[Osprey Publishing]]|location=Oxford, England|year=2006|isbn=978-1-84176-963-9}}
{{refend}}

;Scientific journal articles
{{refbegin|60em}}
*{{cite journal|ref=harv|journal=Polemos: Journal of Interdisciplinary Research on War and Peace|publisher=Croatian Sociological Association and Jesenski & Turk Publishing House|issn=1331-5595|url=http://hrcak.srce.hr/47720?lang=en|title=Specijalne vojno-policijske protuterorističke postrojbe: Hrvatska i svijet|language=Croatian|trans_title=Special Military and Police Counter-Terrorist Units: Croatia and the World|volume=12|issue=24|date=December 2009|first1=Mirko|last1=Bilandžić|first2=Stjepan|last2=Milković|pages=33–60}}
{{refend}}

;News reports
{{refbegin|60em}}
*{{cite news|ref={{harvid|Biluš|14 September 2004}}|newspaper=[[Nacional (weekly)]]|title=Tajna snimka tereti generala Filipovića|trans_title=Secret Recording Points to General Filipović|url=http://www.nacional.hr/clanak/13957/tajna-snimka-tereti-generala-filipovica|first=Marina|last=Biluš|date=14 September 2004|issue=461|language=Croatian}}
*{{cite web|ref={{harvid|Index.hr|16 April 2011}}|date=16 April 2011|url=http://www.index.hr/vijesti/clanak/procitajte-brijunske-transkripte-glavni-dokaz-haskog-suda-/547318.aspx?mobile=false|publisher=[[Index.hr]]|title=Pročitajte Brijunske transkripte, glavni dokaz Haškog suda|language=Croatian|trans_title=Read Brijuni Transcripts, Main Evidence of the Hague Court}}
*{{cite news|ref={{harvid|Ratković|November 2011}} |newspaper=Sinjske novine |date=November 2011 |url=http://www.sinjske-novine.kus-sinj.hr/pdf/kus937079465.pdf |format=PDF |title=Obilježena 17. godišnjica vojne akcije "Zima '94." |trans_title=The 17th anniversary of military Operation Winter '94 marked |first=Filip |last=Ratković |archivedate=30 November 2013 |archiveurl=http://www.webcitation.org/6LWAduyk8?url=http%3A%2F%2Fwww.sinjske-novine.kus-sinj.hr%2Fpdf%2Fkus937079465.pdf |deadurl=no |df= }}
*{{cite news|ref={{harvid|Žabec|27 November 2010}}|date=27 November 2010|first=Krešimir|last=Žabec|url=http://www.jutarnji.hr/predsjednik-ivo-josipovic-iz-bojne-izbacio-sve-umijesane-u--sumnjive-poslove-/907146/|language=Croatian|title=Predsjednik Josipović iz bojne izbacio sve umiješane u "sumnjive poslove"|trans_title=President Josipović Discharged Everyone Involved in "Suspicious Activities" from the Battalion|newspaper=[[Jutarnji list]]}}
{{refend}}

;Other sources
{{refbegin|60em}}
*{{cite web|ref={{harvid|MORH|29 April 2013}} |date=29 April 2013 |url=http://www.morh.hr/hr/prvi-za-hrvatsku-13/bojna-za-specijalna-djelovanja.html |language=Croatian |publisher=[[Ministry of Defence (Croatia)]] |title=Bojna za specijalna djelovanja |trans_title=Special Operations Battalion |archiveurl=http://www.webcitation.org/6QijNTXdF?url=http%3A%2F%2Fwww.morh.hr%2Fhr%2Fprvi-za-hrvatsku-13%2Fbojna-za-specijalna-djelovanja.html |deadurl=no |archivedate=30 June 2014 |df= }}
*{{cite journal|ref={{harvid|Vlahović|October 2013}}|date=October 2013|first=Domagoj|last=Vlahović|title=Spremni za buduće izazove|language=Croatian|trans_title=Ready for Future Challenges|url=http://www.hrvatski-vojnik.hr/hrvatski-vojnik/4332013/vob.asp|publisher=Ministry of Defence (Croatia)|journal=[[Hrvatski vojnik]]|issue=433|issn=1333-9036}}
{{refend}}

==Further reading==
*{{cite book|title=1. hrvatski gardijski zdrug|language=Croatian|trans_title=1st Croatian Guards Brigade|isbn=978-953-193-124-3|year=2011|publisher=Ministry of Defence (Croatia)|location=[[Zagreb]], Croatia|editor-first=Josip|editor-last=Lucić|editor-link=Josip Lucić|url=http://katalog.nsk.hr/F/DN23F2FV4JYFMT8MYGFK1UPMUKVIA5B1UY51BF9XS65DSK6629-11911?func=full-set-set&set_number=002235&set_entry=000003&format=999}}

[[Category:Military units and formations established in 1994]]
[[Category:Military units and formations disestablished in 2000]]
[[Category:Military units and formations of the Croatian War of Independence]]
[[Category:Special forces]]
[[Category:1994 establishments in Croatia]]