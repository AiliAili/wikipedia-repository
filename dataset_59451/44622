{{About|the district of the [[Republic of Abkhazia]]|the district of the [[Government of the Autonomous Republic of Abkhazia|Autonomous Republic of Abkhazia]]|Gali Municipality}}
{{Infobox district

| name                    = Gali District
| native_name             = {{lang|ka|გალის რაიონი}}<br>{{lang|ab|Гал араион}}<br />{{lang|ru|Гальский район}}
| type                    = [[Districts of Abkhazia|District]]
| image_skyline           =
| image_alt               =
| image_caption           =
| image_map               = Abkhazia_Gali_map.svg
| map_alt                 =
| map_caption             = Location of Gali district in Abkhazia
| latd  =  |latm  =  |lats  =  |latNS  =
| longd =  |longm =  |longs =  |longEW =
| coordinates_type        = region:GE-AB_type:adm1st
| coordinates_display     = inline,title
| coordinates_footnotes   =
| subdivision_type        = [[List of sovereign states|Country]]
| subdivision_name        = [[Georgia (country)|Georgia]]
| subdivision_type1       = [[List of sovereign states#Other states|De Facto state]]
| subdivision_name1       = [[Abkhazia]]<ref>{{Abkhazia-note}}</ref>
| established_title       =
| established_date        =
| seat_type               = [[Capital (political)|Capital]]
| seat                    = [[Gali (town)|Gali]]
| government_footnotes    =
| leader_party            =
| leader_title            = Governor
| leader_name             = ''[[Temur Nadaraia]]'' {{small|Acting}}
| unit_pref               = Metric<!-- or US or UK -->
| area_footnotes          =
| area_total_km2          =
| area_note               =
| elevation_footnotes     =
| elevation_m             =
| population_footnotes    =
| population_total        = 29287
| population_as_of        = 2003
| population_density_km2  = auto
| population_demonym      =
| population_note         =
| timezone1               = [[Moscow Time|MSK]]
| utc_offset1             = +3
| postal_code_type        =
| postal_code             =
| area_code_type          =
| area_code               =
| website                 = <!-- [http://www.example.com example.com] -->
| footnotes               = Area differs from the administrative division of Georgia<ref>Gali district according to the administrative division of Georgia has an area of 1,003&nbsp;km² and is somewhat bigger than the de facto Gali district</ref>
}}

[[File:United Nation Abkhazia small.PNG|thumb|300px|right|Gali district was largely in the UN security zone]]

'''Gali district''' is a district of [[Republic of Abkhazia|Abkhazia]]. Its capital is [[Gali (town)|Gali]], the town by the same name. The district is smaller than the eponymous one in the de jure [[Administrative divisions of Georgia (country)|subdivision of Georgia]], as some of its former territory is now part of [[Tkvarcheli District]], formed by de facto Abkhaz authorities in 1995.

Gali district was populated almost entirely by [[Mingrelians]], a [[Georgians|Georgian]] regional subdivision, in the pre-war Abkhazia. The majority of Georgians fled the district following the inter-ethnic clashes in 1993–1994 and again in 1998. From 40,000 to 60,000 refugees have returned to Gali district since 1998, including persons commuting daily across the ceasefire line and those migrating seasonally in accordance with agricultural cycles. Gali district is now the only district of Abkhazia with ethnic Georgians constituting clear majority.

The population of the district was 29,287 according to the 2003 census conducted in Abkhazia but that figure is questioned by many international observers;<ref name=census>[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html 2003 Census results] {{ru icon}}</ref> it was estimated at 45 thousand in 2006 although Abkhazian authorities contested this number claiming that at least 65,000 refugees had returned.<ref name="Today">[http://www.crisisgroup.org/en/regions/europe/caucasus/georgia/176-abkhazia-today.aspx Abkhazia Today.] ''The [[International Crisis Group]] [[Europe]] Report N°176, 15 September 2006'', page 11. Retrieved on May 27, 2007. ''Free registration needed to view full report''</ref>

Together with the [[Kodori Valley]], Gali district is one of the two real troublespots while the situation is relatively peaceful in the rest of Abkhazia. It was a battlefield of the 1998 escalation of the [[Georgian-Abkhaz conflict]].

==History==
Historically, the present-day Gali district constituted the borderland between the principalities of [[Principality of Abkhazia|Abkhazia]] and [[Samegrelo]], the two breakaway feudal domains of the Kingdom of Georgia, and frequently changed its masters as the borders of these princedoms fluctuated during the dynastic feuds between the [[Shervashidze]] and [[Dadiani]] clans. In the second half of the 17th century, the Shervashidze princes of Abkhazia succeeded in conquering the territories up the [[Inguri River]] including the Gali district. With the dissolution of the Principality of Abkhazia c. 1700, the district between the Galidzga to the Inguri came to be ruled by a branch of the Shervashidze family whose one member, Murzakan, gave to his new fiefdom the name "Samurzakano" (i.e., "land of Murzakan"). The upper class of this new principality was composed chiefly of the Georgian-speaking Abkhaz nobility, whereas the peasants were of both Abkhaz and Georgian (Mingrelian) background. In contrast to the rest of Abkhazia largely [[Islamization|Islamized]] under the [[Ottoman Empire]], Samurzakano remained adherent to the [[Georgian Orthodox Church|Georgian Orthodox Christianity]]. Eventually the princedom came under the Mingrelian possession, but retained a degree of autonomy even after the expanding [[Imperial Russia]] established its protectorate over Mingrelia in 1804. In 1840, however, Russia outrightly annexed Samurzakano. In the 1860s–70s, the Abkhaz revolts and the systematic persecution of Muslim population at the hands of Russian authorities forced many Abkhaz to become [[Muhajir (Caucasus)|Muhajirs]] to the Ottoman possessions. The Orthodox Christian population of Samurzakano remained relatively unaffected though. The decline in Abkhaz population left Mingrelian a dominant culture in the area. Furthermore, some Georgian peasants from the left bank of the Inguri River also emigrated to the right bank into Abkhazia where weaker practices of serfdom prevailed.

[[File:Abkhazia & Samurzakan.jpg|thumb|450px|left|Samurzakan in 1899]]
The rate of intermarriages between the Georgians and Abkhaz was high that resulted in the mixed heritage of the district's population and the introduction of the special category "Samurzakanians" in the 1897 Imperial Russian census. This group was made up chiefly by the Mingrelians with a minority of Abkhaz. 38% and 35% of the district's population identified themselves as [[Mingrelian people|Mingrelian]] and Georgian respectively during the first Soviet census of 1926, with another 26% identifying themselves as Abkhaz. The latter group in Gali district declined greatly so that they formed only a few percent of its population. As the Soviet censuses carried out after 1926 didn't distinguish Georgians and Mingrelians as separate nationalities they together constituted the overwhelming majority in the district.<ref name=census/>

The subsequent history of Samurzakano/Gali basically follows that of modern Abkhazia, which became an autonomous entity within the [[Democratic Republic of Georgia]] (1918–1921), then the [[Abkhazian SSR]] associated with the [[Georgian SSR]] (1921–1931) and finally the [[Abkhaz ASSR]] within Georgia. Under [[Joseph Stalin]], from 1944/5 to 1953, Abkhaz schools were closed and cultural institutions were suppressed which is sometimes seen as an attempt at the assimilation of Abkhaz into Georgians.

At the height of the [[War in Abkhazia (1992–93)|secessionist war in Abkhazia]] in 1993, the Georgians of Gali were subjected to systematic [[Ethnic cleansing of Georgians in Abkhazia|ethnic cleansing]] at the hands of the Abkhaz militias and their allies from Russia's [[North Caucasus|North Caucasian]] republics. By November 1993, most of the Gali district had been controlled by the secessionist with the exception of a few isolated enclaves evacuated by the Georgian forces according to the Russian-brokered ceasefire accord in 1994. Since then, some 40,000–60,000 Georgians have sporadically returned to the Gali district, but the process was significantly thwarted by the outbreak of hostilities in May 1998. In response to the attempt by the Georgian guerrillas to recover the area, the Abkhaz forces launched a large sweep into the district. Despite the criticism from opposition, [[Eduard Shevardnadze]], [[President of Georgia]], refused to deploy troops against Abkhazia. A ceasefire was negotiated on May 20, 1998. The hostilities resulted in hundreds of casualties on both sides and displaced an additional 30,000 – 40,000 Georgians. Although many families soon came back, the clashes left some 1,500 homes and infrastructure, including some that had been recently rehabilitated with international funding, in ruins. The United Nations Joint Assessment Mission to the Gali District confirmed, in 2000, that homes and infrastructure were deliberately burned and looted during the Abkhaz offensive.<ref name="Today"/>

==Demographics==
According to 2003 census, the population of the district included:<ref name=census/>
* Georgians (98.7%)
* Russians (0.5%)
* Abkhaz (0.4%)
* Armenians (0.1%)
* Greeks (0.1%)

== Human rights ==
In the Georgian-populated areas in Gali district, where local authorities are almost exclusively made up of ethnic Abkhaz, the human rights situation remains precarious. The United Nations and other international organizations have been fruitlessly urging the Abkhaz de facto authorities "to refrain from adopting measures incompatible with the right to return and with international human rights standards, such as discriminatory legislation… [and] to cooperate in the establishment of a permanent international human rights office in Gali and to admit United Nations civilian police without further delay."<ref>[http://www.brook.edu/fp/projects/idp/200603_rpt_Georgia.pdf Report of the Representative of the Secretary-General on the human rights of internally displaced persons – Mission to Georgia] {{webarchive |url=https://web.archive.org/web/20061223084834/http://www.brook.edu/fp/projects/idp/200603_rpt_Georgia.pdf |date=December 23, 2006 }}. United Nations: 2006.</ref>

The security situation in the district improved since February, 2006 and was generally calm in 2006.<ref name="Today"/><ref>[http://daccessdds.un.org/doc/UNDOC/GEN/N06/401/49/PDF/N0640149.pdf Report of the Secretary-General on the situation in Abkhazia, Georgia, 26 June 2006]  {{webarchive |url=https://web.archive.org/web/20081007184716/http://daccessdds.un.org/doc/UNDOC/GEN/N06/401/49/PDF/N0640149.pdf |date=October 7, 2008 }}</ref> However, both Abkhaz and Georgian criminal networks continue to harass the locals. Georgia regularly criticizes the Abkhaz authorities and the Russian peacekeeping forces for failing to assure the local population's safety and prevent human rights abuses.<ref name="Today"/> It proposed, on February 3, 2003, to create a joint Georgian-Abkhaz administration in Gali, but the Abkhaz side rejected the proposal.<ref>[http://www.civil.ge/eng/detail.php?id=5924 Timeline 2003,] ''Civil Georgia''. Retrieved April 30, 2008.  {{webarchive |url=https://web.archive.org/web/20071118053631/http://www.civil.ge/eng/detail.php?id=5924 |date=November 18, 2007 }}</ref> The [[UNOMIG]] and Georgian side have proposed the deployment of a U.N. civilian police mission on both sides of the ceasefire line. Since 2003 U.N. police
have been present on the Georgian-controlled side, but de facto Abkhaz authorities oppose their deployment in Gali, because it would undermine their own political authority.<ref name="Today"/>

Georgia has also reported several cases of forcible recruitment of Georgian returnees into the [[Military of Abkhazia|Abkhaz military]], but the Sukhumi-based authorities categorically deny this, claiming that all citizens are obliged to serve, but no
one is forcibly recruited.<ref name="Today"/> Threats made by Georgian security services (aimed at the prevention of the participation of the district's residents in the elections and against the inhabitants of the district working in its administration) were also reported.<ref>[[International Crisis Group]], [http://www.crisisgroup.org/en/regions/europe/caucasus/georgia/193-georgia-and-russia-clashing-over-abkhazia.aspx GEORGIA AND RUSSIA: CLASHING OVER ABKHAZIA], 05.06.08</ref>

After the [[2008 South Ossetia war|Russian-Georgian war]] of August 2008, the situation in Gali significantly deteriorated with the Russian and Abkhaz military buildup and increased pressure on local Georgians from the Abkhaz authorities.<ref>[http://www.rferl.org/Content/Abkhaz_Incident_Opens_Up_New_Vista_In_Georgia_Conflict/1504128.html Abkhaz Incident Opens Up New Vista In Georgia Conflict]. [[Radio Free Europe/Radio Liberty]]. March 4, 2009</ref> In April 2009, the OSCE High Commissioner on National Minorities called om "the de facto authorities to put an end to the pressure being exercised on the Georgian population in the Gali District through the limitation of their education rights, compulsory "[[passportization]]", forced conscription into the Abkhaz military forces and restrictions on their freedom of movement."<ref>[http://www.osce.org/item/37226.html OSCE High Commissioner on National Minorities deeply concerned by recent developments in Abkhazia]. OSCE Press Release. 14 April 2009 {{dead link|date=September 2011}}</ref>

On July 31, 2009, the breakaway region's Parliament passed amendment to the law making ethnic Georgians living in Gali district eligible for the Abkhaz citizenship. The move triggered wave of protest among opposition groups forcing the Parliament to revoke its decision on August 6.<ref>[http://www.civil.ge/eng/article.php?id=21375 Head of Abkhaz NSC Resigns]. [[Civil Georgia]]. 18 August 2009</ref>

==Administration==
On 11 February 1991, the Presidium of the Supreme Soviet of Georgia dismissed [[Rabo Shonia]] as District Soviet Chairman and appointed [[Edisher Janjulia]] as Prefect. The Supreme Soviet and the Soviet of Ministers of the [[Abkhazian ASSR]] subsequently condemned this decision as a violation of their authority and appealed to the Georgian Supreme Soviet to reconsider.<ref name=rrc1824>{{cite news|title=ЗАЯВЛЕНИЕ ПРЕЗИДИУМА ВЕРХОВНОГО СОВЕТА АБХАЗСКОЙ АССР И СОВЕТА МИНИСТРОВ АБХАЗСКОЙ АССР|url=http://www.rrc.ge/law/nGancxAfx_07_02_1991_R.htm?lawid=1824&lng_3=ru|accessdate=21 July 2012|date=February 1991}}</ref>

[[Ruslan Kishmaria]] was reappointed as Administration Head on 10 May 2001 following the March 2001 local elections.<ref name="press2001-92">{{cite news|title=Выпуск № 92|url=http://abkhazia.narod.ru/gb/1579|accessdate=24 April 2016|agency=[[Apsnypress]]|date=10 May 2001}}</ref>

After the [[Abkhazian local elections, 2004|March 2004 Assembly elections]], [[Yuri Kvekveskiri]] was appointed Administration Head, while Kishmaria became Chairman of the Assembly.<ref name=uzel52852>{{cite news|last=Kuchuberia|first=Anzhela|title=Назначен новый глава Гальского района Абхазии|url=http://www.kavkaz-uzel.ru/articles/52852/|accessdate=14 January 2012|newspaper=[[Caucasian Knot]]|date=25 March 2004}}</ref> Kvekveskiri did not participate in the [[Abkhazian local elections, 2007|March 2007 elections]], but remained acting Administration Head until [[Beslan Arshba]] was appointed as his successor on 23 May 2007.<ref name=regnum832231>since 23.05.2007; Regnum.ru [http://regnum.ru/news/abxazia/832231.html Назначен новый глава Гальского района Абхазии], 23.05.2007</ref>

On 2 June 2014, following the [[2014 Abkhazian political crisis]], acting President [[Valeri Bganba]] dismissed Beslan Arshba, as had been demanded by protesters, and appointed his Deputy [[Vakhtang Maan]] as acting District Head.<ref name=apress12143>{{cite news|title=Тимур Гогуа и Беслан Аршба освобождены от должностей глав Администраций Ткуарчалского и Галского районов.|url=http://apsnypress.info/news/12143.html|accessdate=8 June 2014|agency=Apsnypress|date=2 June 2014}}</ref> After the election of [[Raul Khajimba]] as President, he on 11 November appointed [[Temur Nadaraia]] as Acting Head in Maan's stead.<ref name=apress13463>{{cite news|title=Надарая Темур Хухутович назначен исполняющим обязанности главы администрации Галского района|url=http://apsnypress.info/news/13463.html|accessdate=14 November 2014|agency=Apsnypress|date=11 November 2014}}</ref>

===List of District Governors===

{| class="wikitable" width=75% cellpadding="2"
|-style="background-color:#E9E9E9; font-weight:bold" align=left
| #
| width=240|Name
| width=200|From
|
| width=200|Until
|
| width=200|President
| width=200|Comments
|-
|colspan=8 align=center|'''Chairmen of the District Soviet:'''
|-
|
|[[Vakhtang Kolbaia]]
|1984
|
|1990
|
|
|
|-
|
|[[Rabo Shonia]]
|?
|
|11 February 1991
|<ref name=rrc1824/>
|
|
|-
|colspan=8 align=center|'''Prefects:'''
|- 
|
|[[Edisher Janjulia]]
|11 February 1991
|<ref name=rrc1824/>
|?
|
|
|
|-
|colspan=8 align=center|'''Heads of the District Administration:'''
|-
|
|[[Vazha Zarandia]]
|December 1993
|<ref name=lakoba>{{cite web|last=Lakoba|first=Stanislav|authorlink=Stanislav Lakoba|title=Кто есть кто в Абхазии|url=http://apsnyteka.narod2.ru/l/abhaziya_posle_dvuh_imperii_xix-xxi_vv/kto_est_kto_v_abhazii_ukazatel_imen/index.html|accessdate=20 January 2012}}</ref>
|April 1994
|<ref name=lakoba/>
|
|
|-
|rowspan=2|
|rowspan=2|[[Ruslan Kishmaria]]
|May 1994
|<ref name=uzel62034>{{cite news|title=Кишмария Руслан Ражденович|url=http://www.kavkaz-uzel.ru/articles/62034/|accessdate=14 January 2012|newspaper=[[Caucasian Knot]]|date=24 September}}</ref>
|26 November 1994
|
|
|
|-
|26 November 1994
|
|1997
|<ref name=uzel62034/>
|rowspan=4|[[Vladislav Ardzinba]] 
|
|-
|
|[[Valeri Lomia]]
|1997
|
|1998
|
|
|-
|
|[[Ruslan Kishmaria]]
|1998
|<ref name=uzel62034/>
|March 2004
|
|
|-
|rowspan=2|
|rowspan=2|[[Yuri Kvekveskiri]] 
|March 2004
|<ref name=uzel52852/>
|2005
|
|
|-
|2005
|
|23 May 2007
|<ref name=regnum832231/>
|rowspan=2|[[Sergei Bagapsh]]
|
|-
|rowspan=2|
|rowspan=2|[[Beslan Arshba]]
|23 May 2007
|<ref name=regnum832231/>
|26 September 2011
|
|
|-
|26 September 2011
|
|2 June 2014
|<ref name=apress12143/>
|[[Alexander Ankvab]]
|
|-
|
|''[[Vakhtang Maan]]''
|2 June 2014
|<ref name=apress12143/>
|11 November 2014
|<ref name=apress13463/>
|''[[Valeri Bganba]]''
|Acting
|-
|
|''[[Temur Nadaraia]]''
|11 November 2014
|<ref name=apress13463/>

|''Present''
|
|[[Raul Khajimba]]
|Acting
|}

== Economy ==
{{externalimage |align = right |width = 200px |image1 = [http://www.panoramio.com/photo/9547685 Gali district countryside]<ref>[http://www.panoramio.com/user/433022/tags/G%C3%A9orgie&comments_page=1&photos_page=1 Photos by Patrick Perrier], 26.10.2008</ref>
|image2= [http://www.geotimes.ge/uploads_script/news/e090d2bd3e21ca3.jpg Street in Gali district]<ref>[http://www.geotimes.ge/index.php?m=home&newsid=10582 Geotimes] 4.1.2009</ref>}}
The Gali district is a rich agricultural area for tea, citrus, hazelnuts and vegetables. The [[Inguri Dam|Inguri]] [[hydroelectric station]], a major supplier to Abkhazia and part of Georgia proper, is located on the Abkhaz-Georgian ceasefire line and is operated jointly.

Gali's residents are allowed to relatively freely cross into the neighboring Georgian districts, but must pay 50 [[Russian rouble]]s. Customs fees are also levied upon any goods they are carrying into/out of Georgia proper.<ref name="Today"/>

The district's 2006 budget was 7.5 million Russian roubles ($300,000) but 30 per cent is [[tax revenue]] forwarded to Sukhumi. The remaining 70 per cent is spent on salaries, pensions and administration expenses. However, the district's budget does not include allocations from the central budget, which tend to be [[ad hoc]], dependent on particular needs.<ref name="Today"/> The infrastructure remains in the state of collapse and, in spite of the limited international humanitarian aid, the majority of returnees continue to live in damaged houses or temporary shelters.<ref>[http://www.internal-displacement.org/8025708F004CE90B/(httpDocuments)/CB7F31BB6E4E78EDC12571CC005ADC63/$file/Final+Housing+Assesment+Gali+feb+13+2006.doc Housing Assessment. Gali District] ([http://www.internal-displacement.org/8025708F004CE90B/httpCountry_Documents?ReadForm&country=Georgia&count=10000 library]). Danish Refuge Council/[[ECHO (European Commission)|ECHO]]. February 1, 2006. Retrieved July 12, 2007.</ref>

== References ==
{{Reflist}}

{{Administrative divisions of Abkhazia}}
{{Districts of Georgia}}

{{coord|42|34|38|N|41|39|06|E|region:GE_type:adm2nd_source:kolossus-huwiki|display=title}}

[[Category:Districts of Abkhazia]]
[[Category:Gali District, Abkhazia| ]]