{{Infobox Journal
| title        = DNA and Cell Biology
| cover        = [[File:DNA and Cell Biology Journal Cover.jpg]]
| editor       = [[Jo Handelsman]]
| discipline   = [[Molecular biology]]
| peer-reviewed=
| language     = [[English language|English]]
| former_names = DNA
| abbreviation = DNA Cell Biol.
| publisher    = [[Mary Ann Liebert, Inc.|Mary Ann Liebert]]
| country      = [[United States]]
| frequency    = 12/year
| history      = 1981–present
| openaccess   =
| impact       = 2.344 
| impact-year  = 2012
| website      = http://www.liebertpub.com/DNA
| link1        = http://online.liebertpub.com/loi/DNA
| link1-name   = Online Access
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         = 19827838
| LCCN         =
| CODEN        =
| ISSN         = 1044-5498
| eISSN        = 1557-7430
| boxwidth     =
}}

'''''DNA and Cell Biology''''' is a scientific journal published by [[Mary Ann Liebert, Inc.]], and covers topics related to DNA and cell biology, such as:

*[[Gene structure]], organization and [[gene expression|expression]]
*[[Molecular medicine]], [[virology]] and [[immunology]]
*[[Comparative biology]] and [[biochemistry]]<ref name="Research Gate">[https://www.researchgate.net/journal/1044-5498_DNA_and_Cell_Biology Research Gate]</ref><ref>[http://www.liebertpub.com/products/product.aspx?pid=13 Publisher Website]</ref>
Articles produced with [[NIH funding]] appear in [[PubMed Central]] a year after publication, starting with volume 27 (2008).<ref>{{Cite web|url=http://www.ncbi.nlm.nih.gov/pmc/journals/?filter=t2&titles=all&search=journals|title=PMC Journal List [C&mdash;H]|website=PubMed Central|publisher=NIH|at=DNA and Cell Biology|access-date=2016-06-12}}</ref>

==Indexing==
''DNA and Cell Biology'' is indexed in:
{{columns-list|2|
*[[Biochemistry & Biophysics Citation Index]]
*[[Biological Abstracts]]
*[[BIOSIS Previews]]
*[[Biotechnology Citation Index]]
*[[CAB Abstracts]]
*[[Current Contents]]/Life Sciences
*[[EMBASE]]/[[Excerpta Medica]]
*[[EMBiology]]
*[[Journal Citation Reports]]/Science Edition
*[[MEDLINE]]
*[[Science Citation Index]]
*[[Science Citation Index Expanded]]
*[[Scopus]]
}}

==References==
{{reflist}}

[[Category:Biology journals]]
[[Category:Publications established in 1981]]
[[Category:Mary Ann Liebert academic journals]]