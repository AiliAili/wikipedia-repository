{{For|the earlier highway signed as Route 243|California State Route 243 (1964–1968)}}
{{Good article}}
{{Infobox road
| state = CA
| type = SR
| route = 243
| alternate_name = Banning-Idyllwild Panoramic Highway
| section = 543
| maint = [[Caltrans]]
|map_notes=SR 243 highlighted in red
| length_mi = 29.625
| length_round = 3
| length_ref = <ref name="trucklist" />
| direction_a = South
| terminus_a = {{jct|state=CA|SR|74}} near [[Mountain Center, California|Mountain Center]]
| direction_b = North
| terminus_b = {{jct|state=CA|I|10}} in [[Banning, California|Banning]]
| previous_type = SR
| previous_route = 242
| next_type = SR
| next_route = 244
|counties=[[Riverside County, California|Riverside]]
}}
'''State Route 243''' ('''SR 243'''), or the '''Banning-Idyllwild Panoramic Highway''', is a 30-mile (50 kilometer) two-lane [[highway]] that runs from [[Banning, California]] (in the north) to [[Idyllwild, California]] (in the south) in [[Riverside County, California]]. The highway is a connector between [[Interstate 10 (California)|Interstate 10]] (I-10) and [[California State Route 74|SR 74]]. Along its route, it provides access to the  [[San Bernardino National Forest]]. A road from Banning to Idyllwild was planned around the turn of the twentieth century, and was open by 1910. The road was added to the state highway system in 1970.

==Route description==
SR 243 begins at [[SR 74 (CA)|SR 74]] in the [[San Jacinto Wilderness]] near [[Mountain Center, California|Mountain Center]], [[Riverside County, California|Riverside County]] as Idyllwild Road. The highway traverses north along a winding road through the community of [[Idyllwild, California|Idyllwild]]. SR 243 makes a left turn at the intersection with Circle Drive and continues through [[Pine Cove, California|Pine Cove]]. The road continues through the forest past [[Mount San Jacinto State Park]] through [[Twin Pines, California|Twin Pines]] and the [[Morongo Indian Reservation]] before making a few switchbacks and descending en route to the city of [[Banning, California|Banning]] as the Banning Idyllwild Panoramic Highway. The highway continues as San Gorgonio Avenue into the city before making a left onto Lincoln Street and a right onto 8th Street and terminating at a [[diamond interchange]] with [[I-10 (CA)|I-10]].<ref name="tgriv">{{Cite map| publisher=Thomas Brothers| title=Riverside County Street Atlas| year=2008 |pages=389, 722, 782, 814, 844}}</ref><ref name="cagaze">{{cite map|publisher=DeLorme|title=California Atlas and Gazetteer|year=2011|pages=142–143}}</ref>

SR 243 is part of the [[National Highway System (United States)|National Highway System]],<ref name=fhwa-nhs>{{FHWA NHS map|region=californiasouth}}</ref> a network of highways that are essential to the country's economy, defense, and mobility.<ref name=NHS-FHWA>{{FHWA NHS}}</ref> SR&nbsp;243 is eligible for the [[State Scenic Highway System (California)|State Scenic Highway System]],<ref name="scenic">{{CA scenic}}</ref> and is officially designated as a scenic highway by the [[California Department of Transportation]] (Caltrans),<ref name="caltransscenic">{{Caltrans scenic}}</ref> meaning that it is a substantial section of highway passing through a "memorable landscape" with no "visual intrusions", where the potential designation has gained popular favor with the community.<ref>{{Caltrans scenic info}}</ref> In 2007, it was named the '''Esperanza Firefighters Memorial Highway''' in honor of five firefighters who died while fighting the [[Esperanza Fire]] in October 2006.<ref>{{CA Named Freeways | pages=75, 217}}</ref> In 2013, SR&nbsp;243 had an [[annual average daily traffic]] (AADT) of 1,650 between Marion Ridge Drive in Idyllwild and San Gorgonio Avenue in Banning, and 6,500 at the northern terminus in Banning, the latter of which was the highest AADT for the highway.<ref name=traffic>{{Caltrans traffic|year=2013|start=221|end=275}}</ref>

==History==
A road from Banning to Idyllwild was under construction in 1904, and {{convert|12|mi|km}} of the road was open by August, with an additional {{convert|6|mi|km|spell=in}} of the road planned.<ref>{{cite news | title=Riverside.: Scenic Road to Idyllwild | work=Los Angeles Times | date=August 29, 1904 | author=Staff | page=16}}</ref> Another {{convert|4|mi|km|spell=in}} were commissioned in 1908.<ref>{{cite news | title=Banning | work=Los Angeles Times | date=July 15, 1908 | author=Staff | page=II9}}</ref> The [[oiled (road)|oiled]] road was completed by September 1910, and provided a view of [[Lake Elsinore]] and the [[Colorado Desert]], and it was expected to help with transporting lumber and stopping fires; because of this, the federal government provided $2,000 for the construction.<ref>{{cite news | title=Scenic Road is Useful Also | work=Los Angeles Times | date=September 11, 1910 | author=Staff | page=II5}}</ref> The road became a part of the [[Forest Highway|forest highway system]] in 1927.<ref>{{cite news | title=Idyllwild Road is Now Part of National System | work=Los Angeles Times | date=May 17, 1927 | author=Staff | page=12}}</ref>

A new "high-gear" road from Banning to Idyllwild was under way by 1935,<ref>{{cite news | title=San Jacinto Season Open | work=Los Angeles Times | date=January 27, 1935 | author=Staff | page=F2}}</ref> and two years later, the ''[[Los Angeles Times]]'' considered the road to be "high-gear".<ref>{{cite news | title=Mt. San Jacinto State Park to be Dedicated Next Saturday | work=Los Angeles Times | date=June 13, 1937 | author=Rogers, Lynn | page=F1}}</ref> Efforts to pave the road were under way in 1950.<ref>{{cite news | title=Banning-Idyllwild Road Paving Starts Monday | work=Los Angeles Times | date=July 14, 1950 | author=Staff | page=14}}</ref> The road from Banning through Idyllwild to SR 74 was known as [[CR R1 (CA)|County Route R1]] (CR R1) by 1966<!--check date-->.<ref>{{cite map|year=1966|title=California|publisher=California Division of Highways|url=http://www.cosmos-monitor.com/ca/map1966/se-corner.html}}</ref> By 1969, plans were in place to add the Banning to Idyllwild to Mountain Center road as a state highway; earlier, State Senator Nelson Dilworth proposed legislation to require the road to be added to the state highway system if [[SR 195 (CA)|SR 195]] was removed, as the two were of roughly the same length, but the latter remained in the system.<ref>{{cite news | url=https://www.newspapers.com/clip/2655894/the_san_bernardino_county_sun/ | title=Road in San Jacinto Mts. May Become State Highway | work=San Bernardino County Sun | date=July 13, 1969 | accessdate=June 20, 2015 | page=18 | via=Newspapers.com}} {{open access}}</ref> SR 243 was added to the state highway system in 1970.<ref>{{cite CAstat|year=1970|ch=1473}}</ref> The Division of Highways suggested deleting the highway in 1971.<ref>{{cite news | url=https://www.newspapers.com/clip/2655946/the_san_bernardino_county_sun/ | title=State Officials Unveil Their Proposal For Classifying Roads By Function | work=San Bernardino County Sun | date=September 17, 1971 | accessdate=June 20, 2015 | author=Long, Ken | page=16 | via=Newspapers.com}} {{open access}}</ref> In 1998, Caltrans had no plans to improve the route through 2015.<ref>{{cite web | url=http://dot.ca.gov/hq/tpp/corridor-mobility/D8_docs/TCRs/sr-243.pdf | title=Route Concept Report – State Route 243 | publisher=California Department of Transportation | date=March 1998 | accessdate=February 15, 2015 | author=Staff}}</ref>

==Major intersections==
{{CAinttop|post-1964=yes|county=Riverside|post_ref=<br><ref name=trucklist /><ref name=traffic /><ref name=bridgelog>{{Caltrans bridgelog}}</ref>
}}
{{CAint
|location=Mountain Center
|postmile=0.00
|road={{Jct|state=CA|SR|74|city1=Lake Hemet|city2=Palm Desert|city3=Hemet}}
|notes=South end of SR 243
}}
{{CAint
|location=Banning
|lspan=2
|postmile=29.66
|road={{Jct|state=CA|I|10|city1=Los Angeles|city2=Indio}}
|notes=Interchange; north end of SR 243; I-10 exit 100
}}
{{CAint
|postmile=29.66
|road=8th Street
|notes=Continuation beyond I-10
}}
{{Jctbtm}}

==See also==
*{{portal-inline|California Roads}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
{{commonscat}}
*[http://www.aaroads.com/california/ca-243.html California @ AARoads.com - State Route 243]
*[http://www.dot.ca.gov/hq/roadinfo/sr243 Caltrans: Route 243 highway conditions]
*[http://www.cahighways.org/241-248.html#243 California Highways: SR 243]

[[Category:State highways in California|243]]
[[Category:Roads in Riverside County, California|State Route 243]]
[[Category:Scenic highways in California|243]]
[[Category:Banning, California]]