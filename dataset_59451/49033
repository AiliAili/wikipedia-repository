'''Keys and Dowdeswell''' was a major international architecture firm operating out of Shanghai, and designing buildings throughout China and South East Asia in the 1920s, 30s, and 40s. They designed some of the most prominent buildings in Kuala Lumpur and Singapore, bringing an international standard of luxury to the Asian hospitality market. Major P. Hubert Keys and Frank Dowdeswell were British architects who relocated to Shanghai, China.<ref>{{cite news|title=Legacy|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=straitstimes20000606-1.2.41.20&sessionid=1c545d4a856d47acade9e23d03511741&keyword=keys+and+dowdeswell&token=dowdeswell%2Cand%2Ckeys|publisher=''The Straits Times''|date=6 June 2000}}</ref> They were originally appointed by the Straits Government to design the general post office  in Singapore, June 1, 1927.<ref>{{cite news|title=New Firm of Architects|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=straitstimes19270520-1.2.48&sessionid=0fcc5414314b4c15b58b6060e8627b36&keyword=keys+and+dowdeswell&token=dowdeswell%2Cand%2Ckeys|publisher=''The Straits Times''|date=20 May 1927}}</ref> They were designated A.R.I.B.A (Associate of the Royal Institute of British Architects).<ref>{{cite news|title=Tenders|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=straitstimes19310810-1.2.5.6&sessionid=2f49b147b5714377a19769dfc60675f9&keyword=mercantile+bank+keys+and+dowdeswell&token=dowdeswell%2cand%2ckeys%2cbank%2cmercantile|publisher=The Straits Times|date=10 August 1931}}</ref> They designed in a historicist tradition, with many of their buildings in the [[classical style]], although their design approach changed over the years, and some of their buildings are classified as [[Art Deco]], and a few are Modernist.

==Design==

Major Keys & Dowdeswell were distinguished designers of classical façades and grand interior spaces. They followed the 20th-century Beaux-Arts tradition of classicism, and their works can be divided into three significant categories.<ref>{{cite book|title=Re-shaping Cities : How Global Mobility Transforms Architecture and Urban Form|url=https://books.google.com/books?id=EzKMAgAAQBAJ&pg=PA140&lpg=PA140&dq=p.h+keys+architect&source=bl&ots=DoiBPiL3Ru&sig=I5Q1cSgSY15eDJ1nAznFG5EtKIk&hl=en&sa=X&ei=489UVPSTC8PVuQSO2IKwAg&ved=0CC4Q6AEwAw#v=onepage&q=p.h%20keys%20architect&f=false|accessdate=|date=1 December 2014}}</ref>

===Governmental===

Singapore’s colonial government was in the process of improving the health care due to overcrowding and scarcity of medical help. Therefore, the Governor commissioned Keys and Dowdeswell to design the Singapore General Hospital. This project also reflected the authority and power of the time.

===Monumental===

This was also an approach that aimed ‘’bigness’’. Their buildings had a simple dignity and avoided superfluous features in order to have the image of grandeur. Their designs often contained a grand staircase and monumental clock towers.

===Ornamental===

Their version of classicism was far evolved from an initial study of Roman and Greek architecture. They used Doric and Ionic columns and sculpted decorations on their facades.

==Awards==

In 1920,  Keys and Dowdeswell won the Fullerton Building project through an [[architectural design competition]] which resulted in a building which is now one of the landmarks in Singapore, [[The Fullerton Hotel Singapore]].

In 1929, Keys and Dowdeswell won the design competition for the [[Perak Turf Club]],<ref>{{cite news|title=Perak Turf Club's New Grandstand|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=singfreepressb19291003-1.2.12&sessionid=0c42c6179cab42039ffb8ecc05ff3993&keyword=perak+turf+club+keys+and+dowdeswell&token=dowdeswell%2cand%2ckeys%2cclub%2cturf%2cperak|publisher=The Singapore Free Press and Mercantile Advertiser|date= 3 October 1929}}</ref> which is a horse race course in [[Ipoh]]. 
This was an achievement that significantly marks an appearance of modern architecture in Malaya before World War II.

==Buildings==

{| class="wikitable"
|-
! BUILDING IN MALAYSIA !! YEAR !! BUILDING IN SINGAPORE
|-
| -|| 1921|| King Edward VII College Of Medicine; College Road
|-
| -|| 1926 || Singapore General Hospital; Third Hospital Ave
|-
| -|| 1928 || Fullerton Hotel; Downtown Core
|-
| Perak Turf Club; Ipoh, Perak, Malaysia || 1929 || KPM Building; Robinson Road
|-
|-
| - || 1929 || Capitol Theatre, Downtown Core
|-
| Mercantile Bank Of India; Ipoh, Perak || 1931 || -
|-
| Majestic Hotel; Kuala Lumpur || 1932 || China Building; Chulia Street
|-
| Lam Looking Building; Ipoh, Perak || 1933||Capitol Building; Downtown Core
|-
|}

===Hotel Majestic===

===KPM Building===

KPM Building was a Classical building with some modern touches that Keys and Dowdeswell designed for [[Koninklijke Paketvaart-Maatschappij]] or known as Royal Packet Navigation Company of Batavia; it lies at the junction of Robinson Road and Finlayson Green Road in Singapore. It was constructed entirely with reinforced concrete while the exterior are left with masonry finishes which is an imitation of granite. This six storey building was occupied with KPM offices in the ground floor while the other floors were let to various commercial building.<ref>{{cite news|title=Singapore's New Building|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=straitstimes19291009-1.2.57&sessionid=7d40c297779f4330bd47b85d43399f71&keyword=finlayson+green+keys+and+dowdeswell&token=dowdeswell%2cand%2ckeys%2cgreen%2cfinlayson|publisher=The Straits Times|date=9 October 1929}}</ref>

The building has several interesting features and innovative elements that are brought into the design which includes the basement that functioning as a parking space, the first use of such a function in Singapore. Another innovation were the lifts that were installed at three entrances of the building. The KPM office flooring are lined with marble tiles and there is a mezzanine floor above it for the accommodation of the staff to separate them from the public<ref>{{cite news|title=The K.P.M. Building|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article.aspx?articleid=singfreepressb19301107-1.2.125&sessionid=480562f84cde4f6bacb2a3be2d42c305&keyword=the+new+kpm+building+keys+and+dowdeswell&token=dowdeswell%2cand%2ckeys%2cbuilding%2ckpm%2cnew%2cthe|publisher=The Singapore Free Press and Mercantile Advertiser|date=7 November 1930}}</ref>

==Controversy==

In early 1930, the firm had another project in Ipoh, the Lam Looking Building. The project was taken by a Danish architect, Berthel M. Iverson who was the acting manager. The Board of Architects had found Keys and Dowdeswell guilty of professional misconduct and struck them off the register. They had to leave Singapore in early 30s in disgrace. Iverson took over the project while it was under construction. Due to stylistic differences - Iversen became one of Malaya's most prominent modernists - he decided to withdraw himself from the firm and started his own firm.<ref>{{cite web|title=The Construction And Subsequent Use Of The Lam Looking Building - Part 1.|url=http://db.ipohworld.org/view/id/6227|website=http://db.ipohworld.org/}}</ref> It was in the employ of Keys and Dowdeswell that Iversen met his future business partner, the Dutch architect, also a modernist, S.S. van Sitteren.<ref>{{cite journal|title=untitled|journal=''The Singapore Free Press and Mercantile Advertiser''|date=4 June 1930|page=6}}</ref>

==References==
{{reflist}}

===Bibliography===

{{refbegin}}
*http://eresources.nlb.gov.sg/newspapers/Digitised/SearchResults.aspx?keyword=keys%20and%20dowdeswell
*http://eresources.nlb.gov.sg/infopedia/articles/SIP_1087_2011-01-21.html
*http://www.ipohworld.org/
*http://www.ytlhotels.com/publications/ytl-life/YTL_Life_19.pdf
*http://www.yoursingapore.com/see-do-singapore/architecture/historical/capitol-building-singapore.html
{{refend}}

==Further reading==

*Michael Guggenheim, Ola Söderström.''Re-shaping Cities: How Global Mobility Transforms Architecture and Urban Form''.New York:Routledge, 2010. ISB N 0-203-86407-7

==External links==
* [http://www.pam.org.my Malaysian Institute of Architects]
* [http://www.lam.gov.my Board of Architects Malaysia]



[[Category:Architecture firms of China]]