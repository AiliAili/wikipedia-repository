{{Infobox earthquake
 | name        = 1980 Eureka earthquake
 | map2        = {{Location map+ | USA West
  |AlternativeMap = USA Region West landcover location map.jpg
  |places =
{{Location map~|USA West|lat=44.93|long=-123.03|label=Salem|mark=Green pog.svg}}
{{Location map~|USA West|lat=43.22|long=-123.36|label=Roseburg|mark=Green pog.svg}}
{{Location map~|USA West|lat=39.47|long=-118.78|label=Fallon|mark=Green pog.svg}}
{{Location map~|USA West|lat=37.43|long=-122.14|label=Palo Alto|mark=Green pog.svg}}
{{Location map~|USA West|lat=40.80|long=-124.16|label=Eureka|mark=Green pog.svg}}
{{Location map~|USA West|lat=41.13|long=-124.44|mark=Bullseye1.png|marksize=40}}
  | relief = yes
  | width = 250
  | float = right
  | caption = }}
 | date        = {{Start date|1980|11|08}}
 | time        = {{tooltip|10:27:34 UTC|02:27:34 local}} <ref name=ISC-GEM/>
 | magnitude   = 7.3 [[Moment magnitude scale|M<sub>w</sub>]] <ref name=ISC-GEM/>
 | depth       = {{convert|10|km|abbr=on|order=flip}} <ref name=ISC-GEM/>
 | location    = {{coord|41.13|-124.44|region:US_type:event|display=inline,title}} <ref name=ISC-GEM/>
 | type        = [[Fault (geology)#Strike-slip faults|Strike-slip]] <ref name=PAGER-CAT/>
 | affected    = [[North Coast (California)]] <br> United States
 | damage     = $2–2.75 million <ref name=PAGER-CAT/><ref name=Stover/>
 | intensity   = [[Mercalli intensity scale|VII (''Very strong'')]] <ref name=Stover/>
 | pga         = .15–.25''[[Peak ground acceleration|g]]'' <ref name=nrc/>
 | landslide   = Yes
 | casualties  = Six injured <ref name=Stover/>
}}

The '''1980 Eureka earthquake''' (also known as the '''Gorda Basin earthquake''') occurred on November 8 at {{tooltip|02:27:34 local time|10:27:34 UTC}} along the northern coastal area of [[California]] in the [[United States]]. With a [[moment magnitude scale|moment magnitude]] of 7.3 and a maximum [[Mercalli intensity scale|Mercalli intensity]] of VII (''Very strong''), this [[Fault (geology)#Strike-slip faults|strike-slip]] earthquake was the largest to occur in California in 28 years. Although damage was considered light, several loss estimates equaled or exceeded $2 million, and six injuries resulted when two vehicles came down with the partial collapse of a highway overpass on [[U.S. Route 101 in California|US 101]] in [[Fields Landing, California|Fields Landing]]. The north coast of California experiences frequent [[Plate tectonics|plate boundary]] earthquakes near the [[Mendocino Triple Junction]] and [[intraplate earthquake|intraplate event]]s also occur within the [[Gorda Plate]].

Due to the regional [[seismic risk]], the nuclear portion of the [[Humboldt Bay Nuclear Power Plant]] was shut down in the 1970s. No substantial damage occurred to the two [[Fossil-fuel power station|fossil-fuel]] units that were still operational at the facility. Several types of sensors were installed at the site to capture strong motion data in this seismically-active area, but the majority of records from the event were considered unreliable due to faulty equipment or inadequate maintenance. Only one piece of equipment at the facility provided data by which an estimate of the [[peak ground acceleration]] could be made.

==Tectonic setting==
{{See also|Convergent boundary|Mid-ocean ridge}}

Near [[Cape Mendocino]], the Mendocino Triple Junction is an area of active seismicity where three tectonic plates come together. The [[Mendocino Fracture Zone]] (also known as the Mendocino Fault east of the [[Gorda Ridge]]) is a [[transform fault]] that separates the [[Pacific Plate|Pacific]] and [[Gorda Plate]]s. To the south, the relative motion between the Pacific Plate and [[North American Plate]] is accommodated by the [[San Andreas Fault]], and to the north, the Gorda Plate is converging with the North American Plate at the [[Cascadia Subduction Zone]]. Earthquakes within the Gorda Plate are the result of north-south compression at the Mendocino Fault.<ref name=Bakun>{{citation|title=Seismicity of California's North Coast|url=http://bssa.geoscienceworld.org/content/90/4/797.abstract|first=W. H.|last=Bakun|year=2000|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=90|number=4|pages=797, 798}}</ref>

==Earthquake==
[[File:USGS Shakemap - 1980 Eureka earthquake.jpg|thumb|USGS ShakeMap for the event]]
{{See also|Strike-slip tectonics}}

The [[Sinistral and dextral|left-lateral]] strike-slip earthquake was the largest to occur in California since the [[1952 Kern County earthquake]]. The mainshock (which was described as a multiple-rupture, with four subevents in the initial 80 seconds) and its aftershocks occurred on a northeast-trending [[Fault (geology)|fault]] that extended from near the Mendocino Fault to a point northwest of [[Eureka, California|Eureka]]. Movement along the fault is due to a north-south compressional regime and the resulting [[intraplate deformation]] of the Gorda Plate. Previous events in this area were the January 1922  7.3 M<sub>s</sub> and the January 1923 7.2 M<sub>s</sub> shocks.<ref>{{citation|title=Long-period mechanism of the 8 November 1980 Eureka, California, earthquake|url=http://www.bssaonline.org/content/72/2/439.abstract|first=T.|last=Lay|first2=J. W.|last2=Given|first3=H.|last3=Kanamori|authorlink3=Hiroo Kanamori|year=1982|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=72|number=2|pages=439, 440, 455, 456}}</ref>

The earthquake occurred offshore (to the northwest of Eureka, about {{convert|60|km|abbr=on|order=flip}} west of Patrick's Point) and had a maximum Mercalli intensity of VII (''Very strong''). Some people reported intense shaking that lasted for 15 to 30 seconds; tremors were felt as far south as the [[San Francisco Bay Area]] and as far north as [[Salem, Oregon]]. In the epicentral area, items were knocked off shelves and furniture was displaced, but damage was considered light (the smaller 5.2 M<sub>L</sub> event that occurred in June 1975 caused more damage).<ref name=Lajoie>{{citation|title=Investigations of the 8 November 1980 earthquake in Humboldt County, California|url=https://pubs.er.usgs.gov/publication/ofr81397|first=K.|last=Lajoie|first2=D.|last2=Keefer|year=1981|publisher=[[United States Geological Survey]]|series=Open-File Report 81-397|pp=1, 4, 8, 9, 12–16}}</ref>

===Damage===
While most of the damage from the Gorda Basin earthquake was considered light, there were some exceptions. Mercalli intensities in the immediate area were judged to be in the range of V (''Moderate'') to VII. In Eureka, intensity VI (''Strong'') effects included broken windows and dishes, fallen chimneys, and merchandise that fell from store shelves. Intensity VII effects affected the Fields Landing, King Salmon, Loleta, and Big Lagoon areas, and included surface cracks on the ground, [[soil liquefaction]], small landslides and [[rockfall]]s, and numerous [[Slump (geology)|slumps]] along the [[Eel River (California)|Eel River]]. Similar effects occurred along the Old Coast Highway near [[Trinidad, California|Trinidad]] and [[Moonstone, California|Moonstone]], where the roadway was reduced to one lane of travel in some areas. Several homes were knocked off their foundations and a highway overpass collapsed in Fields Landing. [[Seismic wave]] amplification, poor design, or inadequate construction style may have contributed to losses there.<ref name=Lajoie/><ref name=cg>{{citation|title=Gorda Basin Earthquake, Northwestern California|url=http://www.johnmartin.com/earthquakes/eqpapers/00000050.htm|first=R. T.|last=Kilbourne|first2=G. J.|last2=Saucedo|year=1981|journal=California Geology|publisher=[[California Geological Survey|California Division of Mines and Geology]]|volume=34|number=3|pages=53–57}}</ref>

====Tompkins Hill Road overpass====
{{see also|List of bridge failures}}

The Tompkins Hill Road overpass is situated just south of Fields Landing and was built in the late 1960s. It suffered slight damage during the 1975 earthquake and was due for a retrofit in 1981. The overpass was constructed with cement [[abutment]]s on earthen ramps on either end and a series of concrete support columns in the middle. Eight {{convert|60|ft|abbr=on|sigfig=1}} reinforced concrete spans accommodated northbound and southbound lanes of traffic, with no [[Anchor bolt|anchors]] connecting the spans with each other or to the abutments. At the time of the shock, two of the southbound spans came off their support (a {{convert|15|cm|abbr=on|order=flip|sigfig=1}} ledge) and six people were injured when a [[Volkswagen Beetle]] and a small [[pickup truck]] plummeted off the bridge.<ref name=Lajoie/>

{| style="float: right;" border="3" class="wikitable"
|-
| colspan="2" style="text-align: center;" | Selected Mercalli intensities
|-
! [[Mercalli intensity scale|MMI]] !! Locations
|-
| VII (''Very strong'')
| [[Fields Landing, California|Fields Landing, CA]]
|-
| VI (''Strong'')
| [[Eureka, California|Eureka, CA]], [[Arcata, California|Arcata, CA]]
|-
| V (''Moderate'')
| [[Crescent City, California|Crescent City, CA]], [[Scotia, California|Scotia, CA]]
|-
| IV (''Light'') 
| [[Fallon, Nevada|Fallon, NV]], [[Roseburg, Oregon|Roseburg, OR]]
|-
| III (''Weak'')
| [[Salem, Oregon|Salem, OR]], [[Palo Alto, California|Palo Alto, CA]]
|-
| colspan="3" style="text-align: center;" | <small><span class="plainlinks">[http://www.ngdc.noaa.gov/hazard/intintro.shtml Earthquake Intensity Database, National Geophysical Data Center]</span></small>
|}

====Humboldt Bay Power Plant====
{{Main article|Humboldt Bay Nuclear Power Plant}}

The Humboldt Bay Power Plant is located about {{convert|3|mi|sp=us}} north of the collapsed highway overpass and operated [[Fossil-fuel power station|fossil-fuel]] and nuclear units in the 1960s and 1970s. The nuclear unit was cancelled in 1976 because of seismic safety concerns, but the two fossil-fuel units remained in operation. The [[Nuclear Regulatory Commission]] performed a post-event survey of the facility's systems in late 1980, but inspectors found only minor effects to the plant's structure, piping, tanks, and other mechanical equipment. It was found that the two units were automatically shut down at the time of the shock for various protective concerns and that there were minor cracks in masonry and concrete, sheared bolts, pipe leaks, and slight movement of water tanks. The deformation of a reinforced masonry wall that resulted in a variable-width gap was the only structural issue.<ref name=nrc>{{citation|title=Reconnaissance report : effects of November 8, 1980 earthquake on Humboldt Bay Power Plant and Eureka, California area|url=http://nisee.berkeley.edu/elibrary/Text/S22158|first=K. S|last=Herring|first2=V.|last2=Rooney|first3=N. C.|last3=Chokshi|year=1981|publisher=[[Nuclear Regulatory Commission|U.S. Nuclear Regulatory Commission]]|pages=1–8}}</ref>

===Strong motion===
{{see also|Strong ground motion}}

[[Pacific Gas and Electric Company|Pacific Gas and Electric]] initially reported that [[peak ground acceleration]]s in the range of .16–.4''[[Peak ground acceleration|g]]'' were recorded on the floor of the refueling building on the plant's strong motion instruments ([[accelerometer]]s). Low voltage from a faulty power supply left the instruments in a condition that was functional, but the records were not considered reliable. Three TERA Technology film recorders were also in use as a backup system, but these instruments also did not produce any usable records, because lack of maintenance had allowed dirt and grit to get inside. Only records from one instrument (an Engdhal peak shock recorder) was believed to be operating correctly and, with a close examination of the energy dispersed at various frequencies, an estimate of .15–.25''[[Peak ground acceleration|g]]'' was given for the event at that location.<ref name=nrc/>

==References==
{{reflist|30em|refs=
<ref name=ISC-GEM>{{citation|title=ISC-GEM Global Instrumental Earthquake Catalogue (1900–2009)|url=http://www.isc.ac.uk/iscgem/index.php|author=ISC|year=2015|publisher=[[International Seismological Centre]]|series=Version 2.0}}</ref>
<ref name=PAGER-CAT>{{citation|title=PAGER-CAT Earthquake Catalog|url=ftp://hazards.cr.usgs.gov/web/data/pager/catalogs/|author=USGS|date=September 4, 2009|publisher=[[United States Geological Survey]]|series=Version 2008_06.1}}</ref> 
<ref name=Stover>{{citation|last1=Stover|first1=C. W.|last2=Coffman|first2=J. L.|title=Seismicity of the United States, 1568–1989 (Revised) – U.S. Geological Survey Professional Paper 1527|url=https://books.google.com/books?id=bY0KAQAAIAAJ&printsec=frontcover&dq=seismicity+of+the+united+states&hl=en&sa=X&ei=wOqeUIjiI4XBiwKVvIHYDg&ved=0CDcQ6AEwAQ#v=onepage&q&f=false|year=1993|publisher=[[United States Government Printing Office]]|pages=95, 168}}</ref>
}}

==External links==
* [http://earthquake.usgs.gov/earthquakes/eventpage/usp0001aq1#general_summary M7.2 - offshore Northern California] – [[United States Geological Survey]]
* [http://www.times-standard.com/20101108/when-the-bridge-fell-father-looks-back-on-30th-anniversary-of-quake-that-claimed-overpass When the bridge fell: Father looks back on 30th anniversary of quake that claimed overpass] – ''[[Times-Standard]]''
* [https://www.nytimes.com/2010/11/19/us/19bcjames.html?_r=0 A Quake, a Collapse and No Responsibility] – ''[[The New York Times]]''
* [http://www.nrc.gov/reading-rm/doc-collections/gen-comm/circulars/1981/cr81003.html IE Circular No. 81-03, Inoperable Seismic Monitoring Instrumentation] – [[Nuclear Regulatory Commission|U.S. Nuclear Regulatory Commission]]

{{Earthquakes in 1980}}
{{Earthquakes in California}}
{{Earthquakes in the United States}}
{{Good article}}

[[Category:1980 earthquakes]]
[[Category:Earthquakes in California]]
[[Category:History of Humboldt County, California]]