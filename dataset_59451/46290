{{Infobox station
| name = Astrid
| image =Metro Astrid Ligne 8.JPG
| country = Belgium
| opened = {{Start date|1996|04|01|df=y}} 
| structure = underground
| operator = [[De Lijn]]
| levels  = 3
| coordinates = {{coord|51|13|08|N|04|25|17|E|display=inline}}
| services = {{s-rail|title= De Lijn}}
{{s-line|system=Antwerp tram|line=2|previous=Elisabeth|next=Diamant}}
{{s-line|system=Antwerp tram|line=3|previous=Opera|next=Elisabeth}}
{{s-line|system=Antwerp tram|line=5|previous=Opera|next=Elisabeth}}
{{s-line|system=Antwerp tram|line=6|previous=Elisabeth|next=Diamant}}
{{s-line|system=Antwerp tram|line=8|next=Zegel}}
<!--Future: {{s-line|system=Antwerp tram|line=10|previous=Opera|next=Zegel}}-->
| map_type = Antwerp premetro

}}
'''Astrid''' is a station in the [[Antwerp Pre-metro|Antwerp premetro]] network that was opened on April 1, 1996. The station lies directly under the Koningin Astridplein on the Gemeentestraat side. The station is one of the two premetro stations in [[Antwerp]] servicing passengers from [[Antwerp Central Station]], the other being [[Diamant (Antwerp premetro station)|Diamant]], which lies to the south of Astrid.

== Location ==
Astrid station lies under the northern part of the Astridplein square. Adjacent to the square lie the [[Radisson Blu]] hotel, the entrance gate to the Antwerp [[Chinatown]] and the [[Aquatopia]] aquarium on the northern side, the [[Koningin Elisabethzaal]] and [[Antwerp Zoo]] on the eastern side and [[Antwerp Central Station]] on the southern side. Also, the Rooseveldtplaats bus station, serving a large  portion of all bus traffic in the city, lies some 200 meters to the west of Astrid station.

== Layout ==
The -1 level of the station contains the entrance hall and two platforms lying in an east-west orientation. The platforms were opened on April 18, 2015, along with the opening of the [[Reuzenpijp]] tunnel and the introduction of [[Tram route 8 (Antwerp)|tram route 8]]. At present, they are serviced only by route 8, having its terminus in Astrid station. Trams coming from [[Wommelgem]] and [[Borgerhout]] arrive at the northern platform, where passengers can get out. The trams then continue westward toward the Rooseveldtplaats underground turning loop, and drive back to Astrid station, allowing departing passengers to enter the tram on the southern platform.<ref>https://www.delijn.be/nl/haltes/halte//103109 Retrieved 24/06/2016</ref>

The -2 level contains two more halls, giving access to the -3 and -4 levels and allowing passengers to cross the tracks at the -1 level.

The levels -3 and -4 contain two north-south oriented platforms serviced by routes [[Tram route 2 (Antwerp)|2]], [[Tram route 3 (Antwerp)|3]], [[Tram route 5 (Antwerp)|5]] and [[Tram route 6 (Antwerp)|6]]. The platform on the -3 level is serviced by trams going south to Diamant, or west to Opera, using the central railway triangle under Antwerp Central Station. The -4 level is used by trams going north toward [[Deurne, Belgium|Deurne]], [[Wijnegem]], [[Merksem]] and [[Luchtbal]].<ref>https://www.delijn.be/nl/haltes/halte//104717 Retrieved 24/06/2016</ref>

Since December 2006, station is also connected to the newly built underground car park under the Astridplein. Using a walkway running through the car park, passengers can also reach the Antwerp central station and Diamant premetro station without having to come above ground.<ref>http://www.q-park.be/nl/over-q-park/nieuws/articletype/articleview/articleid/10/qpark-opent-parking-onder-het-koningin-astridplein-in-antwerpen Retrieved 24/06/2016</ref>

== Extension ==

From its opening in 1996 until the opening of route 8 and the [[Reuzenpijp]] tunnel on April 18, 2015, only the platforms on the -3 and -4 levels were in use. The two platforms on the -1 level, built between 1977 and 1981, were hidden behind wooden panels and inaccessible for passengers. In March 2013, works started to refurbish the station in a more modern style, and to open the east-west platforms as a part of the LIVAN-project, which included the opening of the eastern premetro axis. Since April 18, 2015, when the premetro tunnel was officially put into service, the platforms at the -1 level are serviced by route 8.<ref>http://www.gva.be/cnt/aid1531358/vernieuwd-station-astrid-open Retrieved 24/06/2016</ref>

It is planned that tram route 10, which now has its stop above ground, will join route 8 in the premetro tunnel in 2018, after the opening of the [[Foorplein (Antwerp premetro station)|Foorplein]] branch of the Reuzenpijp.<ref>https://www.delijn.be/nl/overdelijn/nieuws/bericht/10735_in_een_kwartier_van_wijnegem_tot_hartje_antwerpen Retrieved 24/06/2016</ref> After the opening of the metro entrance at the Leien and the opening of the -3 level of [[Opera (Antwerp premetro station)|Opera station]], trams using the eastern premetro tunnel will no longer have to have their terminus at Astrid station. <ref>http://www.noorderlijn.be/premetro-opera Retrieved 24/06/2016</ref>


== References ==

{{Reflist}}

== External links ==
* [https://www.delijn.be www.delijn.be], the operator of all public city transport in Antwerp and Flanders.

{{Antwerp Premetro navbox}}
[[Category:Articles created via the Article Wizard]]
[[Category:Public transport in Antwerp]]