{{Good article}}
{{Infobox royalty
| name =Andrew III
| full name =
| image          = Andrew III (Chronica Hungarorum).jpg
| succession    =  [[King of Hungary]] and [[King of Croatia|Croatia]]
| reign         =  1290–1301
| coronation    =  23 July 1290
| cor-type = hungary
| predecessor   =  [[Ladislas IV of Hungary|Ladislaus IV]]
| successor     =  [[Wenceslaus III of Bohemia|Wenceslaus]]
| spouse         =  [[Fenenna of Kuyavia]]<br />[[Agnes of Austria (1281-1364)|Agnes of Austria]]
| issue          =  [[Elizabeth of Töss]]
| house = [[Árpád dynasty|Árpád]]
| house-type=Dynasty
| father         = [[Stephen the Posthumous]]
| mother         =  [[Tomasina Morosini]]
| birth_date  = {{circa}} 1265
| birth_place =
| death_date  = 14 January {{Death year and age|1301|1265}}
| death_place = [[Buda]]
| place of burial= Greyfriars' Church, [[Buda]]
| religion =[[Catholic Church|Roman Catholic]]
|}}
'''Andrew III the Venetian''' ({{lang-hu|III. Velencei András}}, {{lang-hr|Andrija III. Mlečanin}}, {{lang-sk|Ondrej III.}}; {{circa}} 1265{{spaced ndash}}14 January 1301) was [[King of Hungary]] and [[King of Croatia|Croatia]] between 1290 and 1301. His father, [[Stephen the Posthumous]], was the posthumous son of [[Andrew II of Hungary]] although Stephen's brothers considered him a bastard. Andrew grew up in [[Venice]], and first arrived in Hungary upon the invitation of a rebellious baron, [[Ivan Kőszegi]], in 1278. Kőszegi tried to play Andrew off against [[Ladislaus IV of Hungary]], but the conspiracy collapsed and Andrew returned to Venice.

Being the last male member of the [[House of Árpád]], Andrew was elected king after the death of King Ladislaus IV in 1290. He was the first Hungarian monarch to issue a coronation diploma confirming the privileges of the noblemen and the clergy. At least three pretenders—[[Albert I of Germany|Albert of Austria]], [[Maria of Hungary, Queen of Naples|Mary of Hungary]], and an adventurer—challenged his claim to the throne. Andrew expelled the adventurer from Hungary and forced Albert of Austria to conclude a peace within a year, but Mary of Hungary and her descendants did not renounce their claim. The Hungarian bishops and Andrew's maternal family from Venice were his principal supporters, but the leading Croatian and [[Slavonia]]n lords were opposed to his rule.

Hungary was in a state of constant anarchy during Andrew's reign. The [[Kőszegi family|Kőszegis]], the [[Csák (genus)|Csáks]], and other powerful families autonomously governed their domains, rising up nearly every year in open rebellion against Andrew. With Andrew's death, the House of Árpád became extinct. A civil war ensued which lasted for more than two decades and ended with the victory of Mary of Hungary's grandson, [[Charles Robert]].

==Childhood ({{circa}} 1265–1278)==

Andrew was the son of [[Stephen the Posthumous]], the self-styled [[Duke of Slavonia]], and his second wife, [[Tomasina Morosini]].{{sfn|Kristó|Makk|1996|p=282, Appendix 4}}{{sfn|Zsoldos|2003|p=282}} Andrew's father was born to [[Beatrice d'Este, Queen of Hungary|Beatrice D'Este]], the third wife of [[Andrew II of Hungary]], after the king's death.{{sfn|Kristó|Makk|1996|p=282}} However, Andrew II's two elder sons, [[Béla IV of Hungary]] and [[Coloman of Galicia-Lodomeria|Coloman of Halych]], accused Beatrice D'Este of adultery and refused to acknowledge Stephen the Posthumous as their legitimate brother.{{sfn|Zsoldos|2003|p=123}} Andrew's mother, Tomasina Morosini, was the daughter of wealthy Venetian patrician Michele Morosini.{{sfn|Zsoldos|2003|pp=124–125}}

The exact date of Andrew's birth is unknown.{{sfn|Zsoldos|2003|p=124}} According to historians Tibor Almási, Gyula Kristó, and Attila Zsoldos, he was born in about 1265.{{sfn|Almási|2012|p=100}}{{sfn|Kristó|Makk|1996|p=283}}{{sfn|Zsoldos|2003|p=124}} Stephen the Posthumous nominated his wife's two kinsmen, including her brother Alberto Morosini, as Andrew's guardians before his death in 1272.{{sfn|Zsoldos|2003|p=125}}

==Pretender (1278–1290)==

[[File:Andrew III of Hungary.jpg|thumb|right|190px|alt=A young blonde man on a horse, accompanied by two older men|Andrew is brought to Hungary from [[Venice]] to fight against King [[Ladislaus the Cuman]] (from the ''[[Illuminated Chronicle]]'')]]

Andrew came to Hungary for the first time in 1278 at the invitation of a powerful lord, [[Ivan Kőszegi]].{{sfn|Kristó|Makk|1996|p=283}}{{sfn|Érszegi|Solymosi|1981|p=173}} Kőszegi wanted to play Andrew off against [[Ladislaus IV of Hungary]].{{sfn|Kristó|Makk|1996|p=283}} Andrew, who was the only male member of the [[Árpád dynasty|royal family]] besides the king, adopted the title of "Duke of [[Slavonia]], [[Dalmatia]] and [[Kingdom of Croatia (1102–1526)|Croatia]]" and marched as far as [[Lake Balaton]].{{sfn|Kristó|Makk|1996|p=282}} Andrew achieved nothing, however, and went back to Venice in autumn.{{sfn|Kristó|Makk|1996|p=282}}{{sfn|Érszegi|Solymosi|1981|p=173}}

Andrew returned to Hungary at the beginning of 1290.{{sfn|Kristó|Makk|1996|p=282}} On this occasion, [[Lodomer, Archbishop of Esztergom]], also urged him to come, since the archbishop wanted to dethrone the excommunicated Ladislaus&nbsp;IV with the assistance of Ivan Kőszegi.{{sfn|Érszegi|Solymosi|1981|p=173}} Before Andrew was successful, [[Arnold III Hahót|Arnold Hahót]], an enemy of the Kőszegis, invited him to the fort of [[Štrigova]] and captured him.{{sfn|Érszegi|Solymosi|1981|p=173}}{{sfn|Zsoldos|2003|p=135}} Hahót sent Andrew to Vienna, where [[Albert I of Germany|Albert I]], [[Duke of Austria]], held him in captivity.{{sfn|Érszegi|Solymosi|1981|p=173}}{{sfn|Almási|2012|p=100}}

Three Cuman assassins murdered Ladislaus&nbsp;IV on 10&nbsp;July 1290,{{sfn|Berend|Urbańczyk|Wiszewski|2013|p=473}}{{sfn|Érszegi|Solymosi|1981|p=181}} and Archbishop Lodomer subsequently dispatched two monks to Vienna to inform Andrew of the king's death.{{sfn|Zsoldos|2003|p=136}} With the monks' assistance, Andrew left his prison in disguise and hastened to Hungary.{{sfn|Zsoldos|2003|p=136}}

==Reign==

===Coronation and pretenders (1290–1293)===

Upon Andrew's arrival, his opponents tried to bribe Theodor, Prior of the [[Székesfehérvár Chapter]], not to hand over the [[Holy Crown of Hungary]] to the soon-to-be-king, but the prior refused them.{{sfn|Zsoldos|2003|p=136}} Archbishop Lodomer crowned Andrew king in [[Székesfehérvár]] on 23&nbsp;July.{{sfn|Engel|2001|p=110}}{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=34}} The lords and prelates swore loyalty to Andrew only after he issued a charter promising the restoration of internal peace and respect for the privileges of the nobility and the clergymen.{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=23}}{{sfn|Érszegi|Solymosi|1981|p=181}}{{sfn|Kontler|1999|p=84}} He then appointed the most powerful noblemen, who had for years administered their domains independently of the monarch, to the highest offices.{{sfn|Zsoldos|2003|pp=147–148}} [[Amadeus Aba]], who dominated the northeastern parts of the kingdom, was made [[Palatine of Hungary|palatine]], Ivan Kőszegi, the lord of the western parts of Transdanubia, became [[master of the treasury]], and [[Roland Borsa]] remained the [[voivode of Transylvania]].{{sfn|Kontler|1999|p=84}}{{sfn|Zsoldos|2003|pp=143–144, 147–148}} Andrew held a [[Diet of Hungary|diet]] before 1&nbsp;September.{{sfn|Érszegi|Solymosi|1981|p=181}} To put an end to anarchy, the "prelates, barons and noblemen" ordered the destruction of castles which had been erected without royal permission and the restoration of estates that had been unlawfully seized to their rightful owners.{{sfn|Engel|2001|p=110}} Andrew promised that he would hold a diet each year during his reign.{{sfn|Engel|2001|p=110}}

[[File:Ondra3 pecet.jpg|thumb|left|190px|alt=A crowned young man sitting on a throne|Andrew's royal seal]]

There were several other challengers to Andrew's claim to the throne. [[Rudolf I of Germany]] claimed that Hungary [[escheat]]ed to him after Ladislaus&nbsp;IV's childless death, because Ladislaus&nbsp;IV's grandfather, [[Béla IV of Hungary]], had sworn fidelity to [[Frederick II, Holy Roman Emperor]] during the [[Mongol invasion of Europe|Mongol invasion of Hungary]].{{sfn|Zsoldos|2003|p=134}} Although [[Pope Innocent IV]] had years before freed Béla&nbsp;IV of his oath, Rudolf&nbsp;I of Germany attempted to bestow Hungary on his own son, Albert of Austria, on 31&nbsp;August.{{sfn|Zsoldos|2003|p=134}}{{sfn|Érszegi|Solymosi|1981|p=181}} The self-declared "[[Andrew, Duke of Slavonia]]"—an adventurer who claimed to be identical to Ladislaus&nbsp;IV's dead younger brother—also challenged King Andrew's right to the crown and stormed into Hungary from [[Poland]].{{sfn|Érszegi|Solymosi|1981|p=181}}{{sfn|Zsoldos|2003|p=163}} He was shortly thereafter forced to return to Poland, where he was murdered.{{sfn|Érszegi|Solymosi|1981|p=181}}{{sfn|Zsoldos|2003|p=134}}

Andrew married [[Fenenna of Kuyavia|Fenenna]], the daughter of [[Ziemomysł of Kuyavia]], before the end of 1290.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Kristó|Makk|1996|p=286, Appendix 4}} Andrew then held a general assembly for the barons and the noblemen of five [[County (Kingdom of Hungary)|counties]] to the east of the river [[Tisza]]—[[Bihar County|Bihar]], [[Kraszna County|Kraszna]], [[Szabolcs County|Szabolcs]], [[Szatmár County|Szatmár]], and [[Szolnok County|Szolnok]]—at [[Nagyvárad]] (now Oradea in Romania) in early 1291.{{sfn|Érszegi|Solymosi|1981|p=182}} The assembly outlawed [[Stephen Balogsemjén]], a staunch supporter of the late Ladislaus&nbsp;IV, for major trespass.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Zsoldos|2003|pp=164–165}} From the assembly, Andrew went to [[Gyulafehérvár]] (now Alba Iulia in Romania).{{sfn|Zsoldos|2003|pp=164–165}} Here he issued the decrees of his 1290 diet at the assembly of the local noblemen, [[Transylvanian Saxons|Saxons]], [[Székelys]] and [[Romanians]], in February or March.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Sălăgean|2005|p=241}} Around the same time, Andrew dismissed Amadeus Aba and made Ivan Kőszegi palatine.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Zsoldos|2003|p=169}}

Ladislaus IV's sister [[Maria of Hungary, Queen of Naples|Mary]], wife of [[Charles II of Naples]], announced her claim to the throne in April 1291.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Magaš|2007|p=59}} The [[Babonići]], [[Frankopans]], [[Šubići]], and other leading Croatian and Slavonian noble families accepted her as the lawful monarch.{{sfn|Magaš|2007|p=59}}{{sfn|Fine|1994|p=207}} Andrew's main concern, however, was [[Albert I of Germany|Albert of Austria]]'s claim.{{sfn|Zsoldos|2003|pp=170–171}} He invaded Austria, forcing Albert to withdraw his garrisons from the towns and fortresses—including [[Pressburg]] (now Bratislava in Slovakia) and [[Sopron]]—that he had captured years before, many of which were held by the Kőszegis before their conquest.{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=34}}{{sfn|Érszegi|Solymosi|1981|p=182}} The [[Peace of Hainburg]], which concluded the war, was signed on 26&nbsp;August, and three days later Andrew and Albert of Austria confirmed it at their meeting in [[Köpcsény]] (now Kopčany in Slovakia).{{sfn|Érszegi|Solymosi|1981|p=182}} The peace treaty prescribed the destruction of the fortresses that Albert of Austria had seized from the Kőszegis.{{sfn|Zsoldos|2003|p=173}} The Kőszegis rose up in open rebellion against Andrew in spring 1292, acknowledging Mary's son, [[Charles Martel of Anjou|Charles Martel]], as King of Hungary.{{sfn|Érszegi|Solymosi|1981|p=182}}{{sfn|Zsoldos|2003|p=177}} The royal troops subdued the rebellion by July, but the Kőszegis captured and imprisoned Andrew during his journey to Slavonia in August.{{sfn|Érszegi|Solymosi|1981|p=183}}{{sfn|Zsoldos|2003|p=183}} Andrew was liberated within four months, after his supporters sent their relatives as hostages to the Kőszegis.{{sfn|Érszegi|Solymosi|1981|p=183}}{{sfn|Zsoldos|2003|p=183}}

===Rebellions and attempts to consolidate (1293–1298)===

Upon Andrew's request, his mother, Tomasina, moved to Hungary in 1293.{{sfn|Érszegi|Solymosi|1981|p=183}}{{sfn|Zsoldos|2003|p=183}} Andrew appointed her to administer Croatia, Dalmatia, and Slavonia.{{sfn|Érszegi|Solymosi|1981|p=183}}{{sfn|Zsoldos|2003|p=183}} Due to her activities, the Babonići, Šubići, and the Dalmatian towns, acknowledged Andrew's rule.{{sfn|Fine|1994|pp=207–208}} Andrew visited the northern parts of Hungary and ordered the revision of former land grants in February.{{sfn|Zsoldos|2003|p=187}} After his return to [[Buda]], he again made Amadeus Aba palatine.{{sfn|Zsoldos|2003|p=187}} In August, Andrew arranged a marriage between his niece, Constance Morosini, and [[Vladislav, King of Syrmia|Vladislav]], son of [[Stefan Dragutin of Serbia]], who had earlier acknowledged Charles Martel's claim to Hungary.{{sfn|Érszegi|Solymosi|1981|p=183}}{{sfn|Zsoldos|2003|p=187}}

[[File:Denar of Andrew III.jpg|thumb|left|190px|alt=A crowned young man sitting on a throne|Andrew's denarius]]

Roland Borsa besieged and captured [[Benedict, Bishop of Várad]]'s fortress at [[Finiș|Fenes]] (now Finiș in Romania) on 23&nbsp;May 1294.{{sfn|Sălăgean|2005|p=241}}{{sfn|Érszegi|Solymosi|1981|p=184}} Andrew held a general assembly and outlawed Borsa. {{sfn|Zsoldos|2003|p=188}} According to historian Attila Zsoldos, he made Nicholas Kőszegi palatine on this occasion.{{sfn|Zsoldos|2003|p=188}} Andrew laid siege to Borsa's fort at [[Livada, Satu Mare|Adorján]] (now Adrian in Romania).{{sfn|Zsoldos|2003|p=188}} The siege lasted three months before the fort fell to Andrew in October.{{sfn|Sălăgean|2005|p=241}} Andrew replaced Roland Borsa with [[Ladislaus III Kán]] as voivode of Transylvania, but the former preserved all his domains in the lands east of the Tisza.{{sfn|Sălăgean|2005|p=241}}{{sfn|Zsoldos|2003|p=188}}

The Croatian lord [[Paul I Šubić of Bribir|Paul Šubić]] again turned against Andrew and joined the camp of Charles Martel in early 1295, but Charles died in August.{{sfn|Fine|1994|p=208}}{{sfn|Zsoldos|2003|p=198}} Within two months, the Babonići also rebelled against Andrew.{{sfn|Zsoldos|2003|p=198}} Early the next year, the recently widowed King Andrew visited Vienna and arranged a marriage with Duke Albert's daughter [[Agnes of Austria (1281–1364)|Agnes]].{{sfn|Zsoldos|2003|p=198}} The Kőszegis soon rose up in open rebellion.{{sfn|Zsoldos|2003|p=198}} Andrew declared war on the rebels, and Archbishop Lodomer excommunicated them.{{sfn|Érszegi|Solymosi|1981|p=184}} Andrew and Albert jointly seized the Kőszegis' main fort at [[Kőszeg]] in October, but could not subdue them.{{sfn|Érszegi|Solymosi|1981|p=184}}{{sfn|Zsoldos|2003|p=198}} Andrew's mother seems to have died at the end of the year because references to her activities disappear from the contemporaneous documents.{{sfn|Zsoldos|2003|p=198}}

[[Matthew III Csák]], whom Andrew had made palatine in 1296, turned against Andrew at the end of 1297.{{sfn|Engel|2001|p=110}}{{sfn|Zsoldos|2003|p=203}} Andrew's staunch supporter, Archbishop Lodomer, died around the same time.{{sfn|Zsoldos|2003|p=204}} In early February 1298, Andrew visited Albert of Austria in Vienna and promised to support him against [[Adolf, King of Germany|Adolf of Nassau]], [[King of Germany]].{{sfn|Érszegi|Solymosi|1981|p=184}} Andrew sent an auxiliary troop, and Albert of Austria routed King Adolf in the [[Battle of Göllheim]] on 2&nbsp;July.{{sfn|Érszegi|Solymosi|1981|p=184}}

===Last years (1298–1301)===

[[File:Oligarchs in the Kingdom of Hungary.png|thumb |right |alt=A dozen provinces depicted in a map |The provinces ruled by the "[[Oligarch (Kingdom of Hungary)|oligarchs]]" (powerful lords) in the early 14th century]]

Andrew held an assembly of the prelates, noblemen, Saxons, Székelys, and Cumans in [[Pest, Hungary|Pest]] in the summer of 1298.{{sfn|Érszegi|Solymosi|1981|p=185}}{{sfn|Sălăgean|2005|pp=242–241}} The preamble to the decrees that were passed at the diet mentioned "the laxity of the lord king".{{sfn|Engel|2001|p=110}}{{sfn|Zsoldos|2003|p=206}} The decrees authorized Andrew to destroy forts built without permission and ordered the punishment of those who had seized landed property with force, but also threatened Andrew with excommunication if he did not apply the decrees.{{sfn|Zsoldos|2003|p=207}} At the gathering, he appointed his uncle, Albertino Morosini, Duke of Slavonia.{{sfn|Zsoldos|2003|p=206}} After the close of the diet, Andrew entered into a formal alliance with five influential noblemen, including Amadeus Aba, who stated that they were willing to support him against the Pope and the bishops.{{sfn|Érszegi|Solymosi|1981|p=186}}{{sfn|Zsoldos|2003|pp=211, 213}} [[Gregory Bicskei]], the Archbishop-elect and [[Apostolic Administrator]] of Esztergom, forbade the prelates to participate at a new diet which was held in 1299.{{sfn|Érszegi|Solymosi|1981|p=186}}{{sfn|Zsoldos|2003|p=214}} The prelates ignored the archbishop's order and Andrew deprived him of [[Esztergom County]].{{sfn|Érszegi|Solymosi|1981|pp=186–187}}{{sfn|Zsoldos|2003|p=214}}

A group of powerful lords—including the Šubići, Kőszegis and Csáks—urged Charles&nbsp;IΙ of Naples to send his grandson, the 12-year-old [[Charles Robert]], to Hungary in order to become king.{{sfn|Zsoldos|2003|pp=218–219}} The young Charles Robert disembarked in Split in August 1300.{{sfn|Fine|1994|p=208}} Most Croatian and Slavonian lords and all Dalmatian towns but [[Trogir]] recognized him as king before he marched to Zagreb.{{sfn|Fine|1994|pp=208–209}} The Kőszegis and Matthew Csák, however, were shortly reconciled with Andrew, preventing Charles' success.{{sfn|Zsoldos|2003|p=220}} Andrew's envoy to the Holy See noted that [[Pope Boniface VIII]] did not support Charles Robert's adventure, either.{{sfn|Zsoldos|2003|p=220}} Andrew, who had been in poor health for a while, was planning to capture his opponent, but he died in [[Buda Castle]] on 14&nbsp; January 1301.{{sfn|Zsoldos|2003|pp=220–221}}{{sfn|Érszegi|Solymosi|1981|p=187}} According to historians Attila Zsoldos and Gyula Kristó, the contemporaneous gossip suggesting that Andrew was poisoned cannot be proved.{{sfn|Zsoldos|2003|p=221}}{{sfn|Kristó|Makk|1996|pp=287–288}}

Andrew was buried in the Franciscan church in [[Buda]].{{sfn|Zsoldos|2003|p=221}} Years later, Palatine [[Stephen Ákos]] referred to Andrew as the "last golden branch" of the tree of [[Stephen I of Hungary|King Saint Stephen]]'s family, because with Andrew's death the [[House of Árpád]], the first royal dynasty of Hungary, ended.{{sfn|Engel|2001|p=124}}{{sfn|Kristó|Makk|1996|p=288}} A civil war between various claimants to the throne—Charles Robert, [[Wenceslaus III of Bohemia|Wenceslaus of Bohemia]], and [[Otto III, Duke of Bavaria|Otto of Bavaria]]—followed Andrew's death and lasted for seven years.{{sfn|Kontler|1999|p=84}}{{sfn|Engel|2001|pp=128–130}} The civil war ended with Charles Robert's victory, but he was forced to continue fighting against the Kőszegis, the [[Aba (genus)|Abas]], Matthew Csák, and other powerful lords up to the early 1320s.{{sfn|Kontler|1999|pp=87–88}}{{sfn|Engel|2001|pp=130–134}}

==Family==

{{ahnentafel top|width=100%|Ancestors of Andrew III of Hungary{{sfn|Kristó|Makk|1996|p=282, Appendices 3–4}}{{sfn|Zsoldos|2003|pp=121–125}}{{sfn|Runciman|1989|p=345, Appendix III}}{{sfn|Chiappini|2001|pp=31–32, 45}}}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Andrew III of Hungary'''

|2= 2. [[Stephen the Posthumous]]
|3= 3. [[Tomasina Morosini]]

|4= 4. [[Andrew II of Hungary]]
|5= 5. [[Beatrice d'Este, Queen of Hungary|Beatrice d'Este]]
|6= 6. Michele Morosini
|7=

|8= 8. [[Béla III of Hungary]]
|9= 9. [[Agnes of Antioch]]
|10= 10. Aldobrandino d'Este
|11=
|12=
|13=
|14=
|15=

|16= 16. [[Géza II of Hungary]]
|17= 17. [[Euphrosyne of Kiev]]
|18= 18. [[Raynald of Châtillon]]
|19= 19. [[Constance of Antioch]]
|20= 20. [[Azzo VI of Este|Azzo VI d'Este]]
|21= 21. (Sofia) Aldobrandini
|22= 
|23= 
|24= 
|25=
|26=
|27=
|28=
|29=
|30=
|31=
}}</center>
{{ahnentafel bottom}}

Andrew's first wife, Fenenna of Kuyavia, gave birth to a daughter, [[Elizabeth of Töss|Elizabeth]], in 1291 or 1292, but died in 1295.{{sfn|Kristó|Makk|1996|pp=286–287, Appendix 4}} Elizabeth became engaged to Wenceslaus, the heir to [[Wenceslaus II of Bohemia]], in 1298, but the betrothal was broken in 1305.{{sfn|Kristó|Makk|1996|p=287}}{{sfn|Klaniczay|2002|p=207}} She joined the [[Dominican Order|Dominican]] convent at Töss where she died a nun on 5&nbsp;May 1338.{{sfn|Kristó|Makk|1996|p=287, Appendix 4}}{{sfn|Klaniczay|2002|p=208}} She is now venerated as Blessed Elizabeth of Töss.{{sfn|Klaniczay|2002|pp=207–208}} Andrew's second wife, [[Agnes of Austria (1281–1364)|Agnes of Austria]], was born in 1280.{{sfn|Kristó|Makk|1996|p=286}} She survived her husband, but did not marry again; she died in the [[Königsfelden Monastery]] of the [[Poor Clares]] in 1364.{{sfn|Klaniczay|2002|p=208}}{{sfn|Kristó|Makk|1996|p=287, Appendix 4}}

==References==
{{Reflist|3}}

==Sources==
{{Refbegin}}
* {{cite book |last=Almási |first=Tibor |editor1-last=Gujdár |editor1-first=Noémi |editor2-last=Szatmáry |editor2-first=Nóra |title=Magyar királyok nagykönyve: Uralkodóink, kormányzóink és az erdélyi fejedelmek életének és tetteinek képes története ''[Encyclopedia of the Kings of Hungary: An Illustrated History of the Life and Deeds of Our Monarchs, Regents and the Princes of Transylvania]'' |publisher=Reader's Digest |year=2012 |pages=100–101 |chapter=III. András |isbn=978-963-289-214-6 |ref=harv|language=hu}}
* {{cite book |last1=Bartl |first1=Július |last2=Čičaj |first2=Viliam |last3=Kohútova |first3=Mária |last4=Letz |first4=Róbert |last5=Segeš |first5=Vladimír |last6=Škvarna |first6=Dušan |year=2002|title=Slovak History: Chronology & Lexicon |publisher= Bolchazy-Carducci Publishers, Slovenské Pedegogické Nakladatel'stvo |isbn=0-86516-444-4|ref=harv}}
* {{cite book |last1=Berend |first1=Nora |last2=Urbańczyk |first2=Przemysław |last3=Wiszewski |first3=Przemysław |year=2013 |title=Central Europe in the High Middle Ages: Bohemia, Hungary and Poland, c. 900-c. 1300 |publisher= Cambridge University Press |isbn=978-0-521-78156-5 |ref=harv}}
* {{cite book |last=Chiappini |first=Luciano |year=2001 |title=Gli Estensi: Mille anni di storia ''[The Este: A Thousand Years of History]'' |publisher= Corbo Editore |isbn=88-8269-029-6|ref=harv|language=it}}
* {{cite book |last=Engel |first=Pál |year=2001 |title=The Realm of St Stephen: A History of Medieval Hungary, 895–1526 |publisher= I.B. Tauris Publishers |isbn=1-86064-061-3|ref=harv}}
* {{cite book |last1=Érszegi |first1=Géza |last2=Solymosi |first2=László |editor-last=Solymosi |editor-first=László | title=Magyarország történeti kronológiája, I: a kezdetektől 1526-ig ''[Historical Chronology of Hungary, Volume I: From the Beginning to 1526]'' |publisher=Akadémiai Kiadó |year=1981 |pages=79–187 |chapter=Az Árpádok királysága, 1000–1301 [The Monarchy of the Árpáds, 1000–1301] |isbn=963-05-2661-1|ref=harv|language=hu}}
* {{cite book |last=Fine |first=John V. A |authorlink=John Van Antwerp Fine, Jr. |year=1994  |title=The Late Medieval Balkans: A Critical Survey from the Late Twelfth&nbsp;Century to the Ottoman Conquest |publisher= The University of Michigan Press |isbn=0-472-08260-4|ref=harv}}
* {{cite book |last=Kontler |first=László |year= 1999  |title=Millennium in Central Europe: A History of Hungary |publisher= Atlantisz Publishing House |isbn=963-9165-37-9|ref=harv}}
* {{cite book |last=Klaniczay |first=Gábor |year=2002 |title=Holy Rulers and Blessed Princes: Dynastic Cults in Medieval Central Europe |publisher= Cambridge University Press |isbn=0-521-42018-0|ref=harv}}
* {{Cite book |last1=Kristó |first1=Gyula |last2=Makk |first2=Ferenc |year=1996 |title=Az Árpád-ház uralkodói ''[Rulers of the House of Árpád]''|publisher=I.P.C. Könyvek | isbn=963-7930-97-3|ref=harv|language=hu}}
* {{cite book |last=Magaš |first=Branka |year=2007 |title=Croatia Through History |publisher=SAQI |isbn=978-0-86356-775-9|ref=harv}}
* {{cite book |last=Runciman |first=Steven |author-link = Steven Runciman |year=1989 |title=A History of the Crusades, Volume II: The Kingdom of Jerusalem and the Frankish East 1100–1187 | publisher=Cambridge University Press |isbn=0-521-06162-8 |ref=harv}}
* {{cite book |last=Sălăgean |first=Tudor |editor1-last=Pop |editor1-first=Ioan-Aurel |editor2-last=Nägler |editor2-first=Thomas |title=The History of Transylvania, Vol. I. (Until 1541) |publisher=Romanian Cultural Institute (Center for Transylvanian Studies) |year=2005 |pages=233–246 |chapter=''Regnum Transilvanum''. The assertion of the Congregational Regime |isbn=973-7784-00-6|ref=harv}}
* {{cite book |last=Zsoldos |first=Attila |editor1-last=Szovák |editor1-first=Kornél |editor2-last=Szentpéteri |editor2-first=József |editor3-last=Szakács |editor3-first=Margit | title=Szent István és III. András ''[Saint Stephen and Andrew III]'' |publisher=Kossuth Kiadó |year=2003 |pages=119–227 |chapter=III. András |isbn=963-09-4461-8|ref=harv|language=hu}}
{{Refend}}

{{Commons category|Andrew III of Hungary}}

{{s-start}}
{{s-hou|[[House of Árpád]]||{{circa}} 1265|14 January|1301}}
{{s-reg}}
{{s-bef|before=[[Ladislaus IV of Hungary|Ladislaus IV]]}}
{{s-ttl |title=[[King of Hungary]] and [[King of Croatia|Croatia]] |years=1290–1301}}
{{s-aft|after=[[Wenceslas III of Bohemia|Wenceslaus]]}}
{{s-end}}
{{Hungarian kings}}
{{Croatian kings}}

{{Authority control}}

{{DEFAULTSORT:Andrew 03 Of Hungary}}
[[Category:House of Árpád]]
[[Category:Kings of Hungary]]
[[Category:Kings of Croatia]]
[[Category:1265 births]]
[[Category:1301 deaths]]
[[Category:People from Venice]]
[[Category:13th-century monarchs in Europe]]
[[Category:13th-century Hungarian people]] 
[[Category:14th-century monarchs in Europe]]
[[Category:14th-century Hungarian people]]