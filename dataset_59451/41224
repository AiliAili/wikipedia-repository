{{for|the subsequent government of the same name|Republic of Afghanistan (1987–1992)}}
{{for|the current government of Afghanistan|Islamic Republic of Afghanistan}}
{{Infobox former country
|conventional_long_name = Republic of Afghanistan
|native_name = {{unbulleted list|{{lang|prs|جمهوری افغانستان}}|''{{transl|prs|Jomhūrī-ye Afġānestān}}''|{{lang|ps|د افغانستان جمهوریت}}|''{{transl|ps|Dǝ Afġānistān Jumhūriyat}}''}}
|common_name = Afghanistan
|image_map = LocationAfghanistan (with Soviet borders).svg
|continent = Asia
|region = South Asia
|region2 = Central Asia
|era = Cold War
|status = 
|government_type = [[One-party state|One-party]] [[presidential republic]]
|national_motto = 
|national_anthem = ''[[Afghan National Anthem#Anthem used from 1973 to 1978|Millī Surūd]]''<br/><big>ملی سرود</big><br/><small>Afghan National Anthem</small>
|leader_title = [[President of Afghanistan|President]]
|leader1 = Mohammed Daoud Khan
|year_leader1 = 1973&ndash;1978
|legislature = [[Loya Jirga]]
|event_start = [[Coup d'état]]
|year_start = 1973
|date_start = 17 July
|event_end = [[Saur Revolution]]
|year_end = 1978
|date_end = 30 April
|p1 = Kingdom of Afghanistan
|flag_p1 = Flag of Afghanistan 1930.svg
|s1 = Democratic Republic of Afghanistan
|flag_s1 = Flag of Afghanistan (1978).svg
|image_flag = Flag of Afghanistan 1974.svg
|flag_type = Flag (1974-1978)
|image_coat = Emblem of Afghanistan (1974-1978)Gold.png
|capital = Kabul
|common_languages = [[Pashto language|Pashto]]<br/>[[Persian language|Persian]]
|religion = Sunni Islam
|currency = [[Afghan afghani|Afghani]]
|stat_year1 = 1978
|stat_area1 = 647500
|stat_pop1 = 13199600
}}
{{History of Afghanistan}}

The '''Republic of Afghanistan''' ({{lang-prs|جمهوری افغانستان}}, ''{{transl|fa|Jǝmhūri Afġānistān}}''; {{lang-ps|د افغانستان جمهوریت}}, ''{{transl|ps|Dǝ Afġānistān Jumhūriyat}}'') was the name of the first [[republic]] of [[Afghanistan]], created in 1973 after [[Mohammed Daoud Khan]] deposed his cousin, [[King of Afghanistan|King]] [[Mohammad Zahir Shah]], in a non-violent [[Coup d'état|coup]]. Daoud was known for his [[progressive politics]] and attempts to [[Modernization|modernise]] the country with help from both the [[Soviet Union]] and the [[United States]], among others.<ref name=Iranica>{{cite encyclopedia |last=Rubin |first=Barnett |editor=[[Ehsan Yarshater]] |encyclopedia=[[Encyclopædia Iranica]] |title=DĀWŪD <u>KH</U>AN |url= http://www.iranica.com/newsite/articles/v7f2/v7f246.html |accessdate=January 2008 |edition=Online |publisher=[[Columbia University]] |location=United States }}</ref>

In 1978, a military coup known as the [[Saur Revolution]] took place, instigated by the communist [[People's Democratic Party of Afghanistan]], in which Daoud and his family were killed. The "Daoud Republic" was subsequently succeeded by the Soviet-allied [[Democratic Republic of Afghanistan]].<ref name="daoud">{{cite web|author=|url=http://countrystudies.us/afghanistan/28.htm |title=Daoud's Republic, July 1973 - April 1978 |publisher=''Country Studies'' | language=| accessdate=2009-03-15}}</ref>

The current official name of [[Afghanistan]] is ''Islamic Republic of Afghanistan''.

==History==

===Formation===
In July 1973, while [[Mohammed Zahir Shah]] was in [[Italy]] undergoing eye surgery as well as therapy for [[lumbago]], his cousin and former [[Prime Minister of Afghanistan|Prime Minister]] [[Mohammed Daoud Khan]] staged a [[coup d'état]] and established a [[republican government]]. Daoud Khan had been forced to resign as prime minister by Zahir Shah a decade earlier.<ref name = tntimes/> The king abdicated the following month rather than risk an all-out civil war.<ref name="tntimes">{{cite web|author=Barry Bearak|url=https://www.nytimes.com/2007/07/23/world/asia/23cnd-shah.html |title=Former King of Afghanistan Dies at 92 |publisher=''The New York Times'' | date=23 July 2007 | accessdate=2009-03-19}}</ref>

===Political reforms===
The same year, former [[Prime Minister of Afghanistan]] [[Mohammad Hashim Maiwandwal]] was accused of plotting a coup, though it is unclear if the plan was actually targeting the new republican government or the by then abolished monarchy. Maiwandwal was arrested and allegedly committed suicide in jail before his trial, but widespread belief says he was tortured to death.<ref name="daoud"/>

After seizing power, President [[Mohammed Daoud Khan]] established his own [[political party]], the [[List of political parties in Afghanistan#Former parties|National Revolutionary Party]]. This party became the sole focus of political activity in the country. The [[Loya jirga]] approved a new constitution establishing a presidential [[one-party state]] in January 1977, with political opponents being violently persecuted.<ref name="daoud"/>

===Rise of communism===
{{main|People's Democratic Party of Afghanistan}}
During Daoud's presidency, relations with especially the [[Soviet Union]] eventually deteriorated. They saw his shift to a more Western-friendly leadership as dangerous, including criticism of Cuba's membership in the [[Non-aligned Movement]] and the expulsion of Soviet military and economic advisers. The suppression of political opposition furthermore turned the Soviet-backed [[People's Democratic Party of Afghanistan|People's Democratic Party (PDPA)]], an important ally in the 1973 coup against the king, against him.<ref name="daoud"/>

In 1976, Daoud established a seven-year [[economic plan]] for the country. He started military training programs with [[India]] and commenced [[economic development]] talks with [[Pahlavi dynasty|Imperial Iran]]. Daoud also turned his attention to oil rich [[Middle East]]ern nations such as [[Saudi Arabia]], [[Iraq]] and [[Kuwait]] among others for financial assistance.<ref name="daoud"/>

Daoud had however achieved little of what he had set out to accomplish in 1978. The [[Afghan economy]] hadn't made any real progress and the Afghan standard of living had not risen. Daoud had also garnered much criticism for his single party constitution in 1977 which alienated him from his political supporters. By this time, the two main factions of the PDPA, previously locked in a power struggle, had reached a fragile agreement for reconciliation. Communist-sympathizing army officials were by then already planning a move against the government. According to [[Hafizullah Amin]], who became Afghan head of state in 1979, the PDPA had started plotting the coup in 1976, two years before it materialized.<ref name="daoud"/>

===Saur Revolution===
{{main|Saur Revolution}}
[[File:Day after Saur revolution.JPG|thumb|Day after Saur revolution in [[Kabul]].]]
The PDPA seized power in a [[military coup]] in 1978 which is best known as the [[Saur Revolution]].<ref name="bbc">{{cite web|author=|url=http://news.bbc.co.uk/2/hi/south_asia/83854.stm |title=World: Analysis Afghanistan: 20 years of bloodshed |publisher=''BBC News'' | language=| accessdate=2009-03-15 | date=1998-04-26}}</ref> On April 27, troops from the [[military base]] at [[Kabul International Airport]] started to move towards the center of the capital. It took only 24 hours to consolidate power, with the rapid push including an air raid on the [[Arg (Kabul)|presidential palace]] and insurgent army units quickly seizing critical institutions and communication lines. Daoud and most of his family were [[executed]] the following day.<ref name="Garthoff, Raymond L. 1994. p. 986">Garthoff, Raymond L. ''Détente and Confrontation.'' Washington D.C.: The Brookings Institution, 1994. p. 986.</ref>

[[Nur Muhammad Taraki]], [[General Secretary]] of the PDPA, was proclaimed Chairman of the Presidium of the [[Revolutionary Council (Afghanistan)|Revolutionary Council]] and effectively succeeded Mohammed Daoud Khan as head of state. He simultaneously became head of government of the newly established [[Democratic Republic of Afghanistan]].<ref name="Garthoff, Raymond L. 1994. p. 986"/>

== References ==
{{Reflist}}

== External links ==
*[http://countrystudies.us/afghanistan/28.htm Daoud's Republic of Afghanistan]

{{Heads of state of Afghanistan since 1919}}

{{coord missing|Afghanistan}}

{{DEFAULTSORT:Afghanistan, Republic of}}
[[Category:Former countries in the Middle East]]
[[Category:Former countries in South Asia]]
[[Category:Former countries in Central Asia]]
[[Category:States and territories established in 1973]]
[[Category:1978 disestablishments in Asia]]
[[Category:1970s in Afghanistan]]
[[Category:1973 establishments in Asia]]