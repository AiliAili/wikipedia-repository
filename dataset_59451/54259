{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
|name      = Gould Creek
|city      = Adelaide
|state     = sa
|image     =
|lga       = City of Tea Tree Gully
|lga2      = City of Playford
|postcode  = 5114
|fedgov    = [[Division of Makin|Makin]]
|stategov  = [[Electoral district of Little Para|Little Para]]
|dist1     = 8 |location1 = [[Elizabeth, South Australia|Elizabeth]]
|near-nw   =
|near-n    = [[One Tree Hill, South Australia|One Tree Hill]]
|near-ne   =
|near-w    = [[Hillbank, South Australia|Hillbank]]
|near-e    = Sampson Flat
|near-sw   = [[Greenwith, South Australia|Greenwith]]
|near-s    = [[Yatala Vale, South Australia|Yatala Vale]]
|near-se   = [[Upper Hermitage, South Australia|Upper Hermitage]]
}}

'''Gould Creek''' is an outer northeastern rural suburb of [[Adelaide]], [[South Australia]].  Gould Creek is located in the [[City of Tea Tree Gully]] and [[City of Playford]] local government areas, and is adjacent to [[Greenwith, South Australia|Greenwith]], [[Salisbury Heights, South Australia|Salisbury Heights]] and [[Hillbank, South Australia|Hillbank]], as well as the rural districts of [[Yatala Vale, South Australia|Yatala Vale]] and [[Upper Hermitage, South Australia|Upper Hermitage]] and the town of [[One Tree Hill, South Australia|One Tree Hill]].

==History==

=== European settlement ===

Gould Creek is a creek with several fresh water springs which historically flowed into the [[Little Para River]]. 

The first European settlers were James Fisher (eldest son of [[James Hurtle Fisher|J. H. Fisher]]) and [[Frederick Henry Handcock|Fred Handcock]]. The bachelor partners, aged twenty-three, first visited this locality in December 1837 when they accompanied Colonel [[William Light]] on his exploration to discover the [[Barossa Valley]].<ref>Elder, David (ed.) : William Light's Brief Journal and Australian Diaries, (Wakefield Press, 1984), ISBN 0 949268 01 1.</ref> In early 1838 they pioneered a pastoral run at the junction of [[Little Para River]] and Gould Creek. Their squatters’ homestead, with mud chimney and thatched reed roof, known as Fisher and Handcock’s Station, was sketched by [[William Light]] and was one of the earliest to be built outside Adelaide. <ref>Original sketch held by Art Gallery of South Australia.</ref>

Following surveys and closer settlement this became the site of Cumberland Farm, owned by pioneer Reuben Richardson (after whom a street in neighbouring [[Greenwith, South Australia|Greenwith]] has now been named). Agriculture played a key role in Gould Creek's early history, but from the early 1960s onward, Gould Creek played a critical role in Adelaide's utilities.<ref>[http://www.ttghistoricalsociety.org/wellsandwaterways.html  Tea Tree Gully Historical Society - Wells and Waterways] Beryl Gould (ed.). Retrieved 23 March 2013.</ref>

In the early 1960s, the 275kV Para substation was built to transmit power from the large, gas-fired [[Torrens Island Power Station]], which came online in 1967. The substation was monitored and controlled by an innovative electronic remote supervisory system, which was commissioned in 1968.<ref>[http://www.electranet.com.au/pathways/chapter2.pdf Electranet - Pathways] - Context and construction of Para substation.</ref>

The Little Para Reservoir was built between 1974 and 1977 and commissioned in 1979, functioning largely as a balancing storage for [[Murray River]] water, and also serving a flood mitigation role. The reservoir has a capacity of 20,800 megalitres and almost 300,000 cubic metres of rock was used in the reservoir's construction. The Reservoir has two lookouts and public toilets and was open to the public until June 2008.<ref> [http://www.sawater.com.au/SAWater/Education/OurWaterSystems/Water+Storage+(Reservoirs).htm SA Water - Little Para Reservoir]</ref>

==Geography==
The boundaries of Gould Creek are the end of the suburb of Hillbank to the west, the Little Para River to the south, the town of One Tree Hill to the north and One Tree Hill Road and Hannaford Hump Road to the east.<ref>UBD Adelaide directory (1999; 47th ed.) Universal Press. ISBN 0-7319-1033-8</ref> Gould Creek is north of the [[Golden Grove, South Australia|Golden Grove]] conurbation and east of [[Elizabeth, South Australia|Elizabeth]].

==Transport==
The area is not serviced by Adelaide public transport.

== References ==
{{reflist}}

{{Coord|-34.749|138.721|format=dms|type:city_region:AU-SA|display=title}}
{{City of Tea Tree Gully suburbs}}
{{City of Playford suburbs}}

[[Category:Suburbs of Adelaide]]