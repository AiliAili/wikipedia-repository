{{infobox television 
  | show_name          = E! True Hollywood Story
  | image              =
  | caption            =
  | runtime            = 45 - 90 Minutes
  | creator            =
  | developer          = Ilya Reznikov
  | executive_producer = Suzanne Ross
  | starring           = various
  | narrated           = [[Don Morrow]]<br>[[Neil Ross]]<br>[[Kieran Mulroney]]<br>[[Michael Bell (actor)|Michael Bell]]<br>[[Chip Bolcik]]<br>[[Phil Crowley]]<br>Ivan Allen<br>[[Beng Spies]]<br>Angela Bishop (only in Australia)<br>Nick Omana<br>Pete Brooks
  | country            = United States
  | network            = [[E!]]
  | first_aired        = 1996
  | last_aired         = present
  | num_seasons        = 17
  | num_episodes       = 501 
  | list_episodes      =
  | website            = http://www.eonline.com/on/shows/ths
  |}}

'''''E! True Hollywood Story''''' is an American [[documentary film|documentary]] series on [[E!]] that deals with famous [[Hollywood]] celebrities, movies, TV shows, and well-known public figures. Among the topics covered on the program include salacious re-tellings of Hollywood secrets, show-biz scandals, celebrity murders and mysteries, porn-star biographies, and "where-are-they-now?" investigations of former child stars. It frequently features in-depth interviews, actual courtroom footage, and dramatic reenactments.  When aired on the E! network, episodes are usually updated to reflect the current life or status of the subject.

==Overview==
''E! True Hollywood Story'' originally started as a series of specials beginning on March 29, 1996, but evolved into a weekly biographical documentary series. The regular run as a series began in October 1996. The first ''True Hollywood Story'' focused on the murder of [[Rebecca Schaeffer]].

Episodes are either one or two hours long, depending on the topic being covered. There have been more than 500 ''True Hollywood Stories.''

The series was nominated for [[Emmy Award]]s in 2001, 2002, and 2003, and [[Prism Awards]] in 2009.

On Wednesday, May 27, 2009, at 10pm Pacific time, the 500th ''THS'' episode premiered. It was a top 10 countdown of the greatest stories ever told.

==List of E! True Hollywood Stories==
{{inc-tv}}

===Adult industry entertainers===
* [[John Holmes (actor)|John Holmes]]
* [[Jenna Jameson]]
* [[Traci Lords]]
* [[Linda Lovelace]]
* [[Ginger Lynn]]
* [[Savannah (actress)|Savannah]]
* [[Asia Carrera]]
* [[Bob Guccione]]

===Athletes===
* [[Tai Babilonia]] and [[Randy Gardner (figure skater)|Randy Gardner]]
* [[Oksana Baiul]]
* [[Kobe Bryant]]
* [[Anna Kournikova]]
* [[Chris Benoit]]
* [[Dave Draper]]
* [[Rudy Galindo]]
* [[Tonya Harding]]
* [[Scott Hamilton (figure skater)|Scott Hamilton]]
* [[Hulk Hogan]]
* [[O. J. Simpson]]
* [[Tim Tebow]] 
* [[Serena Williams|Serena]] and [[Venus Williams]] 
* [[Tiger Woods]]

===Comedians===
* [[John Belushi]]
* [[Lenny Bruce]]
* [[Brett Butler (actress)|Brett Butler]]
* [[Cheech & Chong]]
* [[Ray Combs]]
* [[Ellen DeGeneres]]
* [[Andrew Dice Clay]]
* [[Andy Dick]]
* [[Phyllis Diller]]
* [[Chris Farley]]
* [[Redd Foxx]]
* [[Gallagher (comedian)|Gallagher]]
* [[Kathy Griffin]]
* [[Phil Hartman]]
* [[Andy Kaufman]]
* [[Sam Kinison]]
* [[Martin Lawrence]]
* [[Joe Piscopo]]
* [[Paula Poundstone]]
* [[Richard Pryor]]
* [[Gilda Radner]]
* [[Joan Rivers]] (A parody on her career, April 1, 2001)<ref>[http://www.imdb.com/title/tt0291280/fullcredits ''Joan Rivers'' parody episode, April 1, 2001 at IMDB]</ref>
* [[Jerry Lewis]]

===Couples and families===
* [[Baldwin brothers (disambiguation)|The Baldwin Brothers]]
* [[Darva Conger]] & [[Rick Rockwell]] 
* The Douglas Dynasty ([[Kirk Douglas]] and [[Michael Douglas]])
* [[Goldie Hawn]] and [[Kate Hudson]]
* [[Paris Hilton|Paris]] and [[Nicky Hilton Rothschild|Nicky Hilton]]
* [[Whitney Houston]] and [[Bobby Brown]]
* [[Ice-T]] and [[Coco Austin|Coco]]
* [[Mick Jagger]] and [[Jerry Hall]]
* [[The Judds]] ([[Naomi Judd|Naomi]], [[Wynonna Judd|Wynonna]], and [[Ashley Judd]])
* [[The Kardashians]] ([[Robert Kardashian|Robert, Sr]]; [[Kourtney Kardashian|Kourtney]]; [[Kim Kardashian|Kimberley]]; [[Khloé Kardashian|Khloé]]; [[Rob Kardashian|Robert, Jr.]]; [[Bruce Jenner]]; [[Kris Jenner]];  [[Kendall Jenner]] and [[Kylie Jenner]])
* [[Kennedy family|The Kennedys]]
* [[Catherine, Duchess of Cambridge|Kate]] and [[Pippa Middleton]]
* The O'Neals ([[Tatum O'Neal|Tatum]], [[Ryan O'Neal|Ryan]], and [[Griffin O'Neal]])
* [[The Osbournes]] ([[Ozzy Osbourne|Ozzy]], [[Sharon Osbourne|Sharon]], [[Kelly Osbourne|Kelly]], and [[Jack Osbourne]])
* [[Sean Penn]] and [[Madonna (entertainer)|Madonna]]
* [[Posh and Becks]]
* [[Lionel Richie|Lionel]] and [[Nicole Richie]]
* [[Joan Rivers|Joan]] and [[Melissa Rivers]]
* [[Brett Salisbury]] and [[Sean Salisbury]]
* Jessica, Ashlee, and the Simpson Family ([[Jessica Simpson|Jessica]], [[Ashlee Simpson|Ashlee]], [[Joe Simpson (manager)|Joe]], and Tina Simpson)
* [[Charlie Sheen]] and [[Denise Richards]]
* [[Britney Spears]] and [[Kevin Federline]]
* [[Rod Stewart|Rod]] and [[Kimberly Stewart]]
* [[Arnold Schwarzenegger]] and [[Maria Shriver]]
* [[Steven Tyler|Steven]] and [[Liv Tyler]]
* [[Luke Wilson|Luke]] and [[Owen Wilson]]
* [[Kevin Jonas]] and [[Danielle Deleasa]]

===Criminals===
* [[Margaret Rudin]]
* [[Michael Alig]]
* [[Death of Caylee Anthony|Casey Anthony]] 
* [[Christian Brando]]
* [[Amy Fisher]]
* [[Heidi Fleiss]]
* [[Mary Kay Letourneau]]
* [[Claudine Longet]] 
* [[Charles Manson]]
* [[Lyle and Erik Menendez|The Menendez Brothers]]
* [[Andrea Yates]]

===Directors / producers===
* [[Al Adamson]]
* [[Bob Fosse]]
* [[Joe Francis]]
* [[Alfred Hitchcock]]
* [[Russ Meyer]]
* [[Julia Phillips]]
* [[Roman Polanski]]

===Entertainers and actors===
{{div col|cols=2}}
* [[Paula Abdul]]
* [[Loni Anderson]]
* [[Jennifer Aniston]]
* [[Drew Barrymore]]
* [[Mischa Barton]] and [[Kristin Cavallari]] 
* [[Justine Bateman]]
* [[Kim Basinger]]
* [[Halle Berry]]
* [[Valerie Bertinelli]]
* [[Bill Bixby]]
* [[Robert Blake (actor)|Robert Blake]]
* [[Linda Blair]]
* [[Sonny Bono]]
* [[Linda Boreman]] also known as [[Linda Lovelace]]
* [[Lara Flynn Boyle]]
* [[Marlon Brando]]
* [[Brat Pack (actors)|The Brat Pack]]
* [[Jim J. Bullock]]
* [[Gary Busey]]
* [[Amanda Bynes]]
* [[John Candy]]
* [[Peter Lawford]]
* [[Lynda Carter]]
* [[Jeanne Carmen]]
* [[Lauren Chapin]]
* [[Duane Chapman|Duane "Dog" Chapman]]
* [[Chippendales]]
* [[Ray Combs]]
* [[Simon Cowell]]
* [[Courteney Cox]]
* [[Bob Crane]]
* [[Macaulay Culkin]] ("Macaulay Culkin: A Child's Rise, A Family's Fall")
* [[Miley Cyrus]]
* [[Patrick Dempsey]]
* [[James Dean]]
* [[Johnny Depp]]
* [[Bo Derek]]
* [[Pete Duel]]
* [[Phyllis Diller]]
* [[Kara DioGuardi]]
* [[Judy Holliday]]
* [[Divine (performer)|Divine]]
* [[Shannen Doherty]]
* [[Robert Downey Jr.]]
* [[Dominique Dunne]]
* [[Zac Efron]]
* [[Chris Farley]]
* [[Colin Farrell]]
* [[Farrah Fawcett]]
* [[Corey Feldman]]
* [[Jane Fonda]]
* [[Jodie Foster]]
* [[Michael J. Fox]]
* [[Redd Foxx]]
* [[Bethenny Frankel]]
* [[Judy Garland]]
* [[Richard Gere]]
* [[Mel Gibson]]
* [[Selena Gomez]]
* [[Kate Gosselin]]
* [[Melanie Griffith]]
* [[Corey Haim]]
* [[Daryl Hannah]]
* [[Mariska Hargitay]]
* [[Melissa Joan Hart]]
* [[Phil Hartman]]
* [[Rodney Harvey]] 
* [[David Hasselhoff]]
* [[Joey Heatherton]]
* [[Paul Reubens]]/"[[Pee-wee Herman]]"
* [[Dennis Hopper]]
* [[Kate Hudson]]
* [[Rock Hudson]]
* [[Elizabeth Hurley]]
* [[Angelina Jolie]]
* [[Anissa Jones]] 
* [[Christopher Jones (actor)|Christopher Jones]]
* [[Margot Kidder]]
* [[Nicole Kidman]]
* [[Tawny Kitaen]]
* [[Sid and Marty Krofft]]
* [[Michael Landon]]
* [[Queen Latifah]]
* [[Heath Ledger]]
* [[Brandon Lee]]
* [[Emmanuel Lewis]]
* [[Jerry Lewis]]
* [[Heather Locklear]]
* [[Lindsay Lohan]]
* [[Eva Longoria]]
* [[Jennifer Lopez]]
* [[Mario Lopez]]
* [[Demi Lovato]]
* [[Howie Mandel]]
* [[Kristy McNichol]]
* [[Steve McQueen]]
* [[Tammy Faye Messner]]
* [[Marilyn Monroe]]
* [[Demi Moore]]
* [[Dudley Moore]]
* [[Mary Tyler Moore]]
* [[Mr. T]]
* [[Jack Nicholson]]
* [[Brigitte Nielsen]]
* [[Nick Nolte]]
* [[Jay North]]
* [[Rosie O'Donnell]]
* [[Jennifer O'Neill]]
* [[Heather O'Rourke]]
* [[Ty Pennington]]
* [[Anthony Perkins]]
* [[Phil McGraw|Dr. Phil]]
* [[Mackenzie Phillips]]
* [[River Phoenix]]
* [[Dana Plato]]
* [[Freddie Prinze]]
* [[Rat Pack|The Rat Pack]]
* [[Rachael Ray]]
* [[Keanu Reeves]]
* [[Tara Reid]]
* [[Burt Reynolds]]
* [[Natasha Richardson]]
* [[Kelly Ripa]]
* [[John Ritter]]
* [[Mickey Rourke]]
* [[Meg Ryan]]
* [[Winona Ryder]]
* [[Rebecca Schaeffer]] ("Dark Obsession: The Rebecca Schaeffer Story")
* [[Scott Schwartz]]
* [[Steven Seagal]]
* [[Rod Serling]]
* [[William Shatner]]
* [[Charlie Sheen]]
* [[Cybill Shepherd]]
* [[Richard Simmons]]
* [[Will Smith]]
* [[Snooki]]
* [[Suzanne Somers]]
* [[Tori Spelling]]
* [[James Stacy]]
* [[Sylvester Stallone]]
* [[John Stamos]]
* [[Sharon Stone]]
* [[David Strickland]]
* [[Hilary Swank]]
* [[Patrick Swayze]]
* [[Sharon Tate]] 
* [[Elizabeth Taylor]]
* [[Charlize Theron]]
* [[Uma Thurman]]
* [[Jean-Claude Van Damme]]
* [[Jim Varney]] 
* [[Hervé Villechaize]]
* [[Jan-Michael Vincent]]
* [[Andy Warhol]]
* [[Mark Wahlberg]]
* [[Adam West]] 
* [[Montel Williams]]
* [[Vanessa Williams]]
* [[Oprah Winfrey]]
* [[Natalie Wood]]
* Young Hollywood: A to Zac
* [[Sean Young]]
* [[Pia Zadora]]
* [[Renée Zellweger]]
{{Div col end}}

===Fashion designers===
* [[Gianni Versace]]
* [[Halston]]
* [[Erin Hughes]]

===Fashion models (supermodels)===
* [[Tyra Banks]]
* [[Christie Brinkley]]
* [[Naomi Campbell]]
* [[Gia Carangi]]
* [[Janice Dickinson]]
* [[Fabio Lanzoni|Fabio]]
* [[Jerry Hall]]
* [[Margaux Hemingway]]
* [[Lauren Hutton]]
* [[Heidi Klum]]
* [[Kate Moss]]
* [[Carré Otis]]
* [[Kimora Lee Simmons]]
* [[Brooke Shields]]
* [[Supermodel|Supermodels]]: Beyond Skin Deep
* [[Niki Taylor]]

===Game shows===
* ''[[Family Feud]]''
* ''[[The Gong Show]]''
* ''[[Hollywood Squares]]''
* ''[[The Price Is Right (U.S. game show)|The Price Is Right]]''
* ''[[Wheel of Fortune (U.S. game show)|Wheel of Fortune]]''

===Movies===
* ''[[American Pie (film)|American Pie]]'' ("American Pie Uncovered")
* ''[[Billy Jack]]''
* [[Bond girl|Bond Girls]]
* ''[[Clueless (film)|Clueless]]''
* ''[[The Cotton Club (film)|The Cotton Club]]''
* ''[[Diner (film)|Diner]]''
* ''[[Dirty Dancing]]''
* ''[[The Exorcist (film)|The Exorcist]]''
* ''[[Fast Times at Ridgemont High]]''
* ''[[Flashdance]]''
* ''[[Jaws (film)|Jaws]]''
* ''[[Mean Girls]]''
* ''[[Our Gang|Our Gang/The Little Rascals]]'' ("Curse of The Little Rascals")
* ''[[Poltergeist (1982 film)|Poltergeist]]'' ("Curse of the Poltergeist")
* ''[[Scarface (1983 film)|Scarface]]''
* ''[[Scream (1996 film)|Scream]]''
* ''[[Sixteen Candles]]''
* ''[[The Terminator]]''
* ''[[The Texas Chainsaw Massacre (franchise)|The Texas Chainsaw Massacre]]''
* ''[[Tropic Thunder]]''
* ''[[Twilight Zone: The Movie]]'' ("The Twilight Zone Movie Trial")

===Musicians===
{{div col|2}}
* [[Aaliyah]]
* [[Bob Marley]]
* [[Paula Abdul]]
* [[Cher]]
* [[Christina Aguilera]]
* [[Clay Aiken]]
* [[The Beach Boys]]
* [[Brandy Norwood|Brandy]]
* [[Mariah Carey]]
* [[Karen Carpenter]]
* [[Johnny Cash]]
* [[Sean Combs]]
* [[Sheryl Crow]]
* [[Sammy Davis Jr.]]
* [[Doris Day]]
* [[Celine Dion]]
* Country Divas ([[Shania Twain]], [[Shelby Lynne]], [[Faith Hill]], [[Dixie Chicks]], [[Trisha Yearwood]] & [[Gretchen Wilson]])
* [[Miley Cyrus]]
* [[John Denver]]
* [[Cass Elliot|Mama Cass Elliot]]
* [[Missy Elliott]]
* [[Eminem]]
* [[Judy Garland]]
* [[Marvin Gaye]]
* [[Taylor Swift]]
* [[Selena Gomez]]
* [[Michael Hutchence]]
* [[Janet Jackson]]
* [[La Toya Jackson]]
* [[Michael Jackson]]
* Hip Hop Wives
* [[Jonas Brothers]]
* [[Janis Joplin]]
* [[Adam Lambert]]
* [[Demi Lovato]]
* [[Justin Bieber]]
* [[Liberace]]
* [[Little Richard]]
* [[Tommy Lee]]
* [[John Lennon]]
* [[Jennifer Lopez]]
* [[Courtney Love]]
* [[LL Cool J]]
* [[Dean Martin]]
* [[George Michael]]
* [[Liza Minnelli]]
* [[The Monkees]]
* [[New Kids on the Block]]
* [[*NSYNC]]
* [[Wayne Newton]]
* [[Olivia Newton-John]]
* [[Dolly Parton]]
* [[Katy Perry]]
* [[Pink (singer)|Pink]]
* [[Elvis Presley]] 
* [[Lisa Marie Presley]]
* [[The Pussycat Dolls]]
* [[Diana Ross]]
* [[Selena]] (A recreation of her 1995 murder trial)
* [[Frank Sinatra]]
* [[Snoop Dogg]]
* [[Britney Spears]] ("Britney Spears: Fall from Grace" and '"Britney Spears: The Price of Fame")
* [[Nicole Scherzinger]]
* [[Spice Girls]]
* [[Tiffany (American singer)|Tiffany]]
* [[Timbaland]]
* [[Tiny Tim (musician)|Tiny Tim]]
* [[Tanya Tucker]]
* [[Usher (entertainer)|Usher]]
* [[Village People]]
* [[Amy Winehouse]]
{{div col end}}

===News anchors / reporters / journalists===
* [[James Bacon (author)|James Bacon]]
* [[Rona Barrett]]
* [[Katie Couric]]
* [[Star Jones]]
* [[Jane Pauley]]
* [[Giuliana Rancic]]
* [[Geraldo Rivera]]

===Playboy===
* [[Pamela Anderson]]
* [[Rebekka Armstrong]]
* [[Shane and Sia Barbi|The Barbi Twins]]
* [[Carmen Electra]]
* [[Hugh Hefner]]
* [[Jessica Hahn]]
* [[Claudia Jennings]]
* [[Holly Madison]]
* [[Jenny McCarthy]]
* [[Bettie Page]]
* [[Anna Nicole Smith]]
* [[Dorothy Stratten]]
* [[Shannon Tweed]]
* [[Kendra Wilkinson]]

===Presidents===
* [[Bill Clinton]]
* [[John F. Kennedy]]
* [[Donald Trump]]

===Royalty / billionaires===
* [[Diana, Princess of Wales|Diana: Legacy of a Princess]]
* [[Sarah, Duchess of York|Sarah: Duchess of York]]
* [[Grace Kelly]], [[Caroline, Princess of Hanover|Princess Caroline]], and [[Princess Stéphanie of Monaco|Princess Stéphanie]]
* [[Donald Trump]]
* First Daughters (Daughters of [[United States presidents]])
* Society Girls (Daughters of high society's wealthiest figures)
* Trust Fund Babies (Daughters of rich and famous parents, such as [[Paris Hilton]])
* Young Royals (Seven European royals)

===Television / reality shows===
{{div col|2}}
* ''[[24 (TV series)|24]]''
* ''[[All in the Family]]''
* ''[[American Idol]]''
* [[American Idol|American Idol: Girls Rule]] ([[Kelly Clarkson]], [[Carrie Underwood]], and [[Fantasia Barrino]])
* ''[[America's Next Top Model]]''
* ''[[The Andy Griffith Show]]'' ("Andy of Mayberry")
* ''[[The Apprentice (U.S. TV series)|The Apprentice]]''
* ''[[The Bachelor (US TV series)|The Bachelor]]''
* '' Bad Girls of Reality TV ([[Omarosa Manigault-Stallworth|Omarosa]], [[Trishelle Cannatella]] and [[Alison Irwin]])
* ''[[Batman (TV series)|Batman]]''
* ''[[Baywatch]]''
* ''[[The Beverly Hillbillies]]''
* ''[[Beverly Hills, 90210]]''
* ''[[Bewitched]]''
* ''[[Big Brother (US)|Big Brother]]''
* ''[[Blossom (TV series)|Blossom]]''
* ''[[The Brady Bunch]]''
* ''[[Charles in Charge]]''
* ''[[Cheers]]''
* ''[[CHiPs]]''
* ''[[Clueless (TV series)|Clueless]]''
* ''[[The Cosby Show]]'' ("The Cosby Kids")
* ''[[Dallas (1978 TV series)|Dallas]]''
* ''[[Dancing with the Stars (US TV series)|Dancing with the Stars]]''
* ''[[Dawson's Creek]]'' ("The Kids of Dawson's Creek")
* ''[[Desperate Housewives]]''
* ''[[Diff'rent Strokes]]''
*''[[Duck Dynasty]]''
* ''[[The Dukes of Hazzard]]''
* ''[[Dynasty (TV series)|Dynasty]]''
* ''[[Dog The Bounty Hunter]]''
* ''[[Eight is Enough]]''
* ''[[The Facts of Life (TV series)|The Facts of Life]]'' ("Facts of Life Girls")
* ''[[Friends]]''
* ''[[Full House]]''
* ''[[Gimme a Break!]]''
* ''[[Gilligan's Island]]''
* ''[[Good Times]]''
* ''[[Growing Pains]]''
* ''[[Home Improvement (TV series)|Home Improvement]]''
* ''[[I Dream of Jeannie]]''
* ''[[I Love Lucy]]''
* ''[[The Jenny Jones Show]]'' (Including the murder of [[Scott Amedure]] by Jonathan Schmitz after their appearance on the show, which never aired)
* ''[[L.A. Law]]''
* ''[[The Little Rascals]]''
* ''[[Magnum, P.I.]]''
* ''[[Married... with Children]]''
* ''[[Melrose Place]]''
* ''[[Miami Vice]]''
* ''[[Modern Family]]''
* ''[[The Mickey Mouse Club]]'' (All three versions, 1955–58, 1977–79, and 1989–96)
* ''[[The Mod Squad]]''
* ''[[The Partridge Family]]''
* ''[[Punky Brewster]]'' (and its star [[Soleil Moon Frye]])
* ''[[The Real World]]''
* Reality Ex-wives ([[Adrianne Curry]], [[LuAnn de Lesseps]], [[Linda Bollea]], [[Courtney Stodden]], and [[Basketball Wives|Jennifer Williams]])
* ''[[Roseanne (TV series)|Roseanne]]''
* ''[[Saved by the Bell]]''
* ''[[Seinfeld]]''
* ''[[Sex and the City]]''
* ''[[The Sopranos]]''
* ''[[Survivor (US TV series)|Survivor]]''
* ''[[Three's Company]]''
* ''[[That '70s Show]]''
* ''[[The View (U.S. TV series)|The View]]''
* ''[[Who's the Boss?]]
* ''[[Welcome Back Kotter]]''
* ''[[The Young and the Restless]]''
{{div col end}}

===THS investigates===
{{div col|2}}
* Bridal to [[Homicide|Homicidal]]
* [[Chandra Levy]] and the Capitol Murders
* [[Cheerleading]]
* [[Cult (religious practice)|Cult]]s, [[Religion]] and [[Mind Control]]
* Curse of the [[Lottery]]
* Curse of the Lottery 2
* [[Dating]] Nightmares
* Deadly Waters
* [[Dieting|Diet]] Fads
* [[Hazing]]
* Hot for Student
* Husbands Who Kill
* Inside the Mind of a [[Serial Killer]]
* [[Kidnapping]]
* Love Behind Bars
* Murder and the Media Machine
* [[Online]] Nightmares
* Original Night Stalker
* [[Paparazzi]]
* [[Plastic Surgery]] Nightmares
* [[Prom]] Nightmares
* Rich Kids Who Kill
* Teen Pregnancy Nightmares
* Teenage Trafficking
* The Real-Life CSI
* [[Spring Break]] Nightmares
* [[Stalking]]
* [[Starving]] for Perfection
* Women Who Kill
{{div col end}}

===Venues===
* [[The Comedy Store]]
* [[Studio 54]]

===Other===
{{div col|2}}
* [[Criss Angel]]
* Baseball Wives
* [[The Beatles|The Beatle]] Wives
* [[Beauty pageant|Beauty Pageants]]
* Biggest Scandals Ever
* [[Bond girl|The Bond Girls]]
*[[Paula Deen]]
* [[John DeLorean]]
* [[Pamela Des Barres]]
* [[Female Body Builders]]
* [[First Daughters]]
* [[Larry Flynt]]
* [[Football Wives]]
* [[Amber Frey]]
* [[Wally George]]
* [[Elizabeth Glaser]]
* [[Patty Hearst]]
* Hollywood Ex-Wives
* [[Jacqueline Kennedy Onassis]]
* [[John F. Kennedy, Jr.]] ("The Last Days of John F. Kennedy, Jr.")
* [[Evel Knievel]]
* [[Grady Stiles|Lobster Boy]]
* [[Heather Mills]]
* [[Mitchell brothers|Jim and Artie Mitchell]]
* [[Sarah Palin]]
* [[Laci Peterson]]
* [[JonBenét Ramsey]]
* Rapper Wives
* Rock Star Wives
* Rock Wives Part 2
* [[Siegfried & Roy]]
* [[Laura Schlessinger]]
* [[Richard Simmons]]
* [[Linda Sobek]]
* [[Martha Stewart]]
* [[Sunset Strip|The Sunset Strip]]
* [[Jacqueline Susann]]
* [[Heidi von Beltz]]
* [[John Walsh (television host)|John Walsh]] ("America's Crime Fighter: John Walsh")
* [[Jill Ann Weatherwax]] ("The Murder of Miss Hollywood")
{{div col end}}

==See also==
* [[Insider]]

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.eonline.com/on/shows/ths/ }}
* {{IMDb title|id=0463399}}
* {{tv.com show|e-true-hollywood-story}}
* {{epguides|id=ETrueHollywoodStory}}

{{E!}}

[[Category:1996 American television series debuts]]
[[Category:1990s American television series]]
[[Category:2000s American television series]]
[[Category:2010s American television series]]
[[Category:American documentary television series]]
[[Category:Television series featuring reenactments]]
[[Category:E! network shows]]
[[Category:English-language television programming]]
[[Category:Entertainment news shows]]