{{Infobox journal
| cover = [[Image:JRS new cover.gif]]
| discipline = [[Spectroscopy]]
| abbreviation = J. Raman Spectrosc.
| editor = Laurence A. Nafie
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Monthly
| history = 1973-present
| impact = 3.137
| impact-year = 2010
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4555
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4555/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4555/issues
| link2-name = Online archive
| ISSN = 0377-0486
| eISSN = 1097-4555
| CODEN = JRSPAF
}}
The '''''Journal of Raman Spectroscopy''''' is a monthly [[peer-review]]ed [[scientific journal]] covering all aspects of [[Raman spectroscopy]], including Higher Order Processes, and [[Brillouin scattering|Brillouin]] and [[Rayleigh scattering]]. It was established in 1973 and is published by [[John Wiley & Sons]]. The current [[editor-in-chief]] is [[Laurence A.Nafie]] ([[Syracuse University]]).<ref name="onlinelibrary">{{cite web|url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4555/homepage/EditorialBoard.html |title=Journal of Raman Spectroscopy - Editorial Board - Wiley Online Library |publisher=Onlinelibrary.wiley.com |date= |accessdate=2012-01-30}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name="interscience">{{cite web|url=http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291097-4555/homepage/ProductInformation.html |title=Journal of Raman Spectroscopy - Overview - Wiley Online Library |publisher=Onlinelibrary.wiley.com |date= |accessdate=2012-01-30}}</ref>
{{columns-list|colwidth=30em|
* [[Advanced Polymers Abstracts]]
* [[Cambridge Scientific Abstracts]]
* [[Ceramic Abstracts]]/[[World Ceramics Abstracts]]
* [[Chemical Abstracts Service]]
* [[Chemistry Citation Index]]
* [[ChemWeb]]
* [[Civil Engineering Abstracts]]
* [[Computer & Information Systems Abstracts]]
* [[Computer Information & Technology Abstracts]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Engineered Materials Abstracts]]
* [[Inspec]]
* [[International Aerospace Abstracts & Database]]
* [[Materials Business File]]
* [[Materials Information]]
* [[Mechanical & Transportation Engineering Abstracts]]
* [[METADEX]]
* [[Earthquake Engineering Abstracts]]
* [[Science Citation Index]]
* [[Scopus]]
* [[Technology Research Database]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 3.137, ranking it 9th out of 40 journals in the category "Spectroscopy".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Spectroscopy |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-01-30 |series=Web of Science |postscript=.}}</ref>

== Notable papers ==
The most cited papers published by the ''Journal of Raman Spectroscopy'' are:<ref name="cites">{{cite book |year=2012 |title=[[Web of Science]] |publisher=[[Thomson Reuters]] |postscript=.}}</ref>
* Normal mode determination in crystals', Volume 10, Issue 1, January 1981, Pages: 253–290, D. L. Rousseau, R. P. Bauman and S. P. S. Porto.
* Research Article: 'Raman microspectroscopy of some iron oxides and oxyhydroxides', Volume 28, Issue 11, November 1997, Pages: 873–878, D. L. A. de Faria, S. Venâncio Silva and M. T. de Oliveira.
* Article: 'Raman spectrum of anatase, TiO2', Volume 7, Issue 6, June 1978, Pages: 321-324, Toshiaki Ohsaka, Fujio Izumi and Yoshinori Fujiki.

== References ==
{{Reflist|35em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4555}}

[[Category:Chemistry journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1973]]
[[Category:Monthly journals]]