{{Taxobox
| name = Australian blacktip shark
| image = Carcharhinus tilstoni csiro-nfc.jpg
| status = LC 
| status_system = iucn3.1
| status_ref = <ref name="iucn"/>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Chondrichthyes]]
| subclassis = [[Elasmobranchii]]
| superordo = [[Shark|Selachimorpha]]
| ordo = [[Carcharhiniformes]]
| familia = [[Requiem shark|Carcharhinidae]]
| genus = ''[[Carcharhinus]]''
| species = '''''C. tilstoni'''''
| binomial = ''Carcharhinus tilstoni''
| binomial_authority = ([[Gilbert Percy Whitley|Whitley]], 1950)
| range_map = Carcharhinus tilsoni distmap.png
| range_map_caption =Range of the Australian blacktip shark<ref name="boomer et al"/>
| synonyms = ''Galeolamna pleurotaenia tilstoni'' <small>Whitley, 1950</small>
}}

The '''Australian blacktip shark''' (''Carcharhinus tilstoni'') is a [[species]] of [[requiem shark]], in the [[family (biology)|family]] Carcharhinidae, [[endemism|endemic]] to northern and eastern [[Australia]]. Favoring the upper and middle parts of the [[water column]], it can be found from the [[intertidal zone]] to a depth of {{convert|50|m|ft|abbr=on}}. Appearance-wise this species is virtually identical to the [[common blacktip shark]] (''C. limbatus''), from which it can be reliably distinguished only by its lower [[vertebra]] number and by [[genetic marker]]s. Generally reaching {{convert|1.5|-|1.8|m|ft|abbr=on}} in length, it is a fairly stout-bodied, bronze-colored shark with a long snout and black-tipped fins.

Primarily [[piscivore|piscivorous]], the Australian blacktip shark forms large groups of similar size and sex that tend to remain within a local area. It exhibits [[vivipary]], meaning that the unborn young are provisioned through a [[placenta]]l connection. There is a well-defined annual reproductive cycle with [[mating]] occurring in February and March. Females bear one to six pups around January of the following year, after a 10-month [[gestation period]]. The Australian blacktip shark is among the sharks most commonly caught by northern Australian [[commercial fishing|commercial fisheries]]. It is mainly valued for its [[shark meat|meat]], which is sold as "[[flake (fish)|flake]]". This species was an important catch of a [[Taiwan]]ese [[gillnet]] fishery that operated from 1974 to 1986, and of the Australian Northern Shark Fishery that continues to the present day. As current fishing levels are not thought to threaten this shark's population, the [[International Union for Conservation of Nature]] (IUCN) has assessed it as [[Least Concern]].

==Taxonomy and phylogeny==
The Australian blacktip shark was described by Australian [[ichthyologist]] [[Gilbert Percy Whitley]] in a 1950 issue of the [[scientific journal]] ''Western Australian Naturalist''. He named it ''Galeolamna pleurotaenia tilstoni'' in honor of Richard Tilston, assistant surgeon at [[Port Essington]], [[Northern Territory]]. The [[type specimen]] is a {{convert|1.5|m|ft|abbr=on}} long female caught from Van Cloon Reef in [[Joseph Bonaparte Gulf]].<ref name="whitley"/> Later authors have regarded ''Galeolamna'' as a [[synonym (taxonomy)|synonym]] of the genus ''[[Carcharhinus]]''. This shark may also be referred to as blacktip whaler, Tilston's whaler shark, and Whitley's blacktip shark.<ref name="fishbase"/>

After Whitley's initial description, ''C. tilstoni'' was generally regarded as synonymous with ''C. limbatus'', the [[common blacktip shark]]. In the 1980s, additional [[morphology (biology)|morphological]] and [[biological life cycle|life history]] data again favored the recognition of ''C. tilstoni'' as a separate species, which was eventually confirmed by [[allozyme]] studies performed by Shane Lavery and James Shaklee.<ref name="lavery and shaklee2"/> Several [[molecular phylogenetic]] studies based on [[allozyme]]s, [[mitochondrial DNA]], and [[nuclear DNA]] have found that the Australian and common blacktip sharks form a closely related [[clade]] with the [[graceful shark]] (''C. amblyrhynchoides'') and the [[smoothtooth blacktip shark]] (''C. leiodon'').<ref name="lavery"/><ref name="moore et al"/><ref name="velez-zuazoa and agnarsson"/> The interrelationships between them have not been fully resolved, but available data suggest that ''C. tilstoni'' and ''C. limbatus'' are not the most closely related species within the clade despite their similarity.<ref name="moore et al"/><ref name="voigt and weber"/>

==Description==
[[File:Carcharhinus limbatus - Caraibische zwartpunthaai.jpg|thumb|left|The common blacktip shark (pictured) is nearly identical in appearance to the Australian blacktip shark.]]
Physically, the Australian blacktip shark can only reliably be distinguished from the common blacktip shark by the number of [[vertebra]]e (174–182 total, 84–91 before the tail in ''C. tilstoni'', 182–203 total, 94–102 before the tail in ''C. limbatus''). It has a moderately robust, spindle-shaped body and a long, pointed snout. The anterior rims of the nostrils are slightly enlarged into low triangular flaps. The large, circular eyes are equipped with [[nictitating membrane]]s. The furrows at the corners of the mouth are barely evident. There are 32–35 upper and 29–31 lower tooth rows; each upper tooth has a slender, upright cusp and fine serrations that become coarser near the base, while the lower teeth are narrower and more finely serrated. The five pairs of [[gill slit]]s are long.<ref name="voigt and weber"/><ref name="last and stevens"/>

The long and narrow [[pectoral fin]]s are falcate (sickle-shaped) with pointed tips. The large first [[dorsal fin]] is also falcate and originates over or slightly behind the rear of the pectoral fin bases. The second dorsal fin is moderately tall and positioned about opposite the [[anal fin]]. There is no midline ridge between the dorsal fins. A crescent-shaped notch is present on the [[caudal peduncle]] just before the upper [[caudal fin]] origin. The caudal fin is asymmetrical, with a strong lower lobe and a longer upper lobe with a ventral notch near the tip. The roughly diamond-shaped [[dermal denticle]]s are placed closely together and slightly overlapping; each bears five to seven (three in juveniles) horizontal ridges leading to marginal teeth. The Australian blacktip shark is bronze above (gray after death) and whitish below, with a pale stripe on the flanks. Some individuals have black tips on all fins, while others have unmarked [[pelvic fin|pelvic]] and anal fins. It typically reaches {{convert|1.5|-|1.8|m|ft|abbr=on}} long; the maximum length and weight on record are {{convert|2.0|m|ft|abbr=on}} and {{convert|52|kg|lb|abbr=on}}.<ref name="voigt and weber"/><ref name="last and stevens"/><ref name="compagno and niem"/>

==Distribution and habitat==
Inhabiting the [[continental shelf]], the Australian blacktip shark is found from [[Thevenard Island]] in [[Western Australia]] to [[Sydney]] in [[New South Wales]].<ref name="boomer et al"/><ref name="last and stevens"/> Within its range, it co-occurs with the common blacktip shark; the ratio between ''C. limbatus'' and ''C. tilstoni'' was once thought to be 1:300, but recent genetic studies have found it to be closer to 50:50.<ref name="ovenden et al"/> This species has been reported from the [[intertidal zone]] to a depth of {{convert|150|m|ft|abbr=on}}; larger sharks tend to occur in deeper water.<ref name="iucn"/> Though it occupies the entire [[water column]], it is most common close to the surface or in midwater.<ref name="last and stevens"/> Genetic data suggest Australian blacktip sharks across northern Australia are all members of a single population.<ref name="lavery and shaklee"/> Tagging studies have found this species mostly moves only short distances along the coastline and rarely enters offshore waters. However, some individuals have been recorded traveling longer distances, up to {{convert|1348|km|mi|abbr=on}}.<ref name="stevens et al"/><ref name="stevens"/>

==Biology and ecology==
[[File:Thton u0.gif|thumb|The longtail tuna is known to be preyed upon by the Australian blacktip shark.]]
The Australian blacktip shark is known to form large groups, segregated by size and sex.<ref name="voigt and weber"/> It preys mainly on [[teleost|teleost fishes]], including [[ponyfish]]es, [[Terapontidae|grunters]], [[Scombridae|tunas]], and [[Clupeidae|herring]]. [[Cephalopod]]s are a secondary food source, which are particularly important around April.<ref name="salini et al"/><ref name="simpfendorfer and milward"/> This species also occasionally consumes smaller sharks, including [[snaggletooth shark]]s (''Hemipristis pristis''), [[spottail shark]]s (''C. sorrah''), and [[Rhizoprionodon|sharpnose sharks]] (''Rhizoprionodon'').<ref name="voigt and weber"/> Their diets shift with age: smaller individuals feed on proportionately more [[demersal fish|bottom-dwelling fish]], while larger individuals feed on proportionately more midwater fish and cephalopods.<ref name="stevens and wiley"/> Documented [[parasite]]s of this shark include the [[tapeworm]]s ''[[Fossobothrium perplexum]]''<ref name="schaeffner and beveridge"/> and ''[[Platybothrium]]'' sp.,<ref name="healy"/> and the [[copepod]] ''[[Perissopus dentatus]]''.<ref name="simpfendorfer"/>

Like other requiem sharks, the Australian blacktip shark is [[viviparous]]: once the [[embryo]]s exhaust their [[yolk]] supply, the depleted [[yolk sac]] develops into a [[placenta]]l connection through which the mother provides sustenance. Females bear litters of one to six pups (average three) every year. [[Mating]] takes place in February and March, with the females storing the [[sperm]] until [[ovulation]] in March and April. After a [[gestation]] period of 10 months, the young are born around January of the following year.<ref name="stevens and wiley"/> Near-term females move into shallow, coastal nurseries, such as [[Cleveland Bay (Queensland)|Cleveland Bay]] in northern Queensland, to give birth.<ref name="simpfendorfer and milward"/> Such nursery areas appear to be widespread, as newborns have been recorded throughout the range of the species.<ref name="stevens"/> The newborns are relatively large, measuring about {{convert|60|cm|in|abbr=on}} long. They grow rapidly in their first year, increasing in length by an average of {{convert|17|cm|in|abbr=on}}. The growth rate subsequently slows, averaging {{convert|8|-|10|cm|in|abbr=on}} per year by age five. Both sexes attain [[sexual maturity]] at three to four years of age, at lengths of roughly {{convert|1.1|and|1.2|m|ft|abbr=on}} for males and females, respectively. The [[maximum lifespan]] is estimated to be 20 years.<ref name="stevens and wiley"/><ref name="davenport and stevens"/>

[[Hybrid (biology)|Hybrid]]s between the Australian blacktip shark and the common blacktip shark, comprising both [[F1 hybrid|F1]] and [[backcrossing|backcrossed]] individuals, have been discovered all along the eastern coast of Australia. Despite the widespread incidence of hybridization, there is no evidence that the two parental lineages are merging and the mechanisms preventing [[introgression]] are unclear. This is the first confirmed case of hybridization among [[cartilaginous fish]]es.<ref name="morgan et al"/>

==Human interactions==
Along with the spottail shark, the Australian blacktip shark is one of the most economically important sharks off northern Australia. It was historically fished for its meat and [[shark fin soup|fins]] by a [[Taiwan]]ese [[gillnet]] [[fishery]], which began operating off northern Australia in 1974. The annual catch initially averaged around 25,000 tons (live weight), with approximately 70% consisting of Australian blacktip sharks, spottail sharks, and [[Thunnus tonggol|longtail tuna]] (''Thunnus tonggol''). Following the establishment of the Australian Fishing Zone (AFZ) in November 1979, the Australian government progressively excluded Taiwanese gillnet vessels from large portions of Australian waters and imposed [[individual fishing quota|fishing quota]]s. The fishery eventually became uneconomical when Australia restricted gillnet lengths to {{convert|2.5|km|mi|abbr=on}} in May 1986, and Taiwanese vessels ceased fishing in Australian waters that year.<ref name="stevens"/> At end of the fishery, Australian blacktip shark numbers were estimated to have been diminished by around 50%. Given its relatively high reproductive rate, its population has likely since recovered.<ref name="iucn"/>

Since 1980, the Australian blacktip shark has also been targeted by Australian commercial gillnetters and [[longline fishing|longliner]]s in the Northern Shark Fishery. It is additionally [[bycatch|caught incidentally]] by other [[commercial fishing|commercial fisheries]] targeting [[bony fish]]es or [[prawn]]s.<ref name="stevens"/> The contemporary northern Australian shark catch is estimated to be between 100 and 900 tons (live weight) annually, of which most are Australian blacktip and spottail sharks.<ref name="iucn"/> The meat of this species is sold in Australia as "[[flake (fish)|flake]]", though it may contain high concentrations of [[mercury (element)|mercury]]. The fins are exported to Asia, while the [[shark cartilage|cartilage]], [[shark liver oil|liver oil]], and [[shagreen|skin]] may also be used.<ref name="last and stevens"/><ref name="stevens"/> Since fishing pressure on the Australian blacktip shark is now much lower than historical levels, the [[International Union for Conservation of Nature]] (IUCN) has listed it under [[Least Concern]].<ref name="iucn"/> However, of potential concern are [[Indonesia]]n fishers in the [[Arafura Sea]], who are increasingly trespassing into the AFZ.<ref name="last and stevens"/><ref name="stevens"/>

==References==
{{reflist|colwidth=30em|refs=

<ref name="boomer et al">{{cite journal |author1=Boomer, J.J. |author2=Peddemors, V |author3=Stow, A.J. |date=2010 |title=Genetic data show that ''Carcharhinus tilstoni'' is not confined to the tropics, highlighting the importance of a multifaceted approach to species identification |journal=Journal of Fish Biology |volume=77 |pages=1165–1172 |url=http://onlinelibrary.wiley.com/doi/10.1111/j.1095-8649.2010.02770.x/full |doi=10.1111/j.1095-8649.2010.02770.x |issue=5 |pmid=21039498}}</ref>

<ref name="compagno and niem">{{cite book |author1=Compagno, L.J.V. |author2=Niem, V.H. |year=1998 |chapter=Carcharhinidae: Requiem sharks |pages=1312–1360 |editor1=Carpenter, K.E. |editor2=Viem, V.H. |title=FAO Identification Guide for Fishery Purposes: The Living Marine Resources of the Western Central Pacific |publisher=Food and Agricultural Organization of the United Nations |isbn=92-5-104052-4}}</ref>

<ref name="davenport and stevens">{{cite journal |title=Age and growth of two commercially imported sharks (''Carcharhinus tilstoni'' and ''C. sorrah'') from Northern Australia |author1=Davenport, S. |author2=Stevens, J.D. |journal=Australian Journal of Marine and Freshwater Research |volume=39 |issue=4 |pages=417–433 |date=1988 |url=http://www.publish.csiro.au/paper/MF9880417 |doi=10.1071/MF9880417}}</ref>

<ref name="fishbase">Froese, R.; Pauly, S.; eds. (2011). [http://fishbase.org/summary/Carcharhinus-tilstoni.html "Carcharhinus tilstoni"] in FishBase. December 2011 version.</ref>

<ref name="healy">{{cite journal |author=Healy, C. |title=A revision of ''Platybothrium'' Linton, 1890 (Tetraphyllidea: Onchobothriidae), with a phylogenetic analysis and comments on host-parasite associations |journal=Systematic Parasitology |date=October 1, 2003 |pages=85–139 |volume=56 |issue=2 |url=http://www.eeb.utoronto.ca/system/files/publication_healy_2003.pdf |doi=10.1023/A:1026135528505 |pmid=14574090}}</ref>

<ref name="iucn">{{IUCN |assessor=Pillans, R. |assessor2=Stevens, J. |title=''Carcharhinus tilstoni'' |year=2003 |id=41739 |version=2011.2}}</ref>

<ref name="last and stevens">{{cite book |author1=Last, P.R. |author2=Stevens, J.D. |year=2009 |title=Sharks and Rays of Australia |edition=second |publisher=Harvard University Press |pages=271–272 |isbn=0-674-03411-2}}</ref>

<ref name="lavery">{{cite journal |title=Electrophoretic analysis of phylogenetic relationships among Australian carcharhinid sharks |author=Lavery, S. |journal=Australian Journal of Marine and Freshwater Research |volume=43 |issue=1 |pages=97–108 |date=1992 |url=http://www.publish.csiro.au/?paper=MF9920097 |doi=10.1071/MF9920097}}</ref>

<ref name="lavery and shaklee">{{cite journal |title=Population genetics of two tropical sharks, ''Carcharhinus tilstoni'' and ''C. sorrah'', in Northern Australia |author1=Lavery, S. |author2=Shaklee, J.B. |journal=Australian Journal of Marine and Freshwater Research |volume=40 |issue=5 |pages=541–557 |date=1989 |url=http://www.publish.csiro.au/paper/MF9890541 |doi=10.1071/MF9890541}}</ref>

<ref name="lavery and shaklee2">{{cite journal |author1=Lavery, S. |author2=Shaklee, J.B. |journal=Marine Biology |volume=108 |issue=1 |pages=1–4 |title=Genetic evidence for separation of two sharks, ''Carcharhinus limbatus'' and ''C. tilstoni'', from Northern Australia |date=1991 |url=http://www.springerlink.com/index/M660726T2201628W.pdf |doi=10.1007/BF01313464}}</ref>

<ref name="moore et al">{{cite journal |title=Rediscovery and redescription of the smoothtooth blacktip shark, ''Carcharhinus leiodon'' (Carcharhinidae), from Kuwait, with notes on its possible conservation status |author1=Moore, A.B.M. |author2=White, W.T. |author3=Ward, R.D. |author4=Naylor, G.J.P. |author5=Peirce, R. |journal=Marine and Freshwater Research |date=2011 |volume=62 |pages=528–539 |url=http://www.publish.csiro.au/paper/MF10159 |doi=10.1071/MF10159 |issue=6}}</ref>

<ref name="morgan et al">{{cite journal |title=Detection of interspecies hybridisation in Chondrichthyes: hybrids and hybrid offspring between Australian (''Carcharhinus tilstoni'') and common (''C. limbatus'') blacktip shark found in an Australian fishery |author1=Morgan, J.A.T. |author2=Harry, A.V. |author3=Welch, D.J. |author4=Street, R. |author5=White, J. |author6=Geraghty, P.T. |author7=Macbeth, W.G. |author8=Tobin, A. |author9=Simpfendorfer, C.A. |author10=Ovenden, J.R. |journal=Conservation Genetics |date=2012 |volume=13 |issue=2 |pages=455–463 |url=http://nctc.fws.gov/CSP/Resources/conservation_science_web_series/information_page/ovenden/morgan_et_al_2011_hybrids.pdf |doi=10.1007/s10592-011-0298-6}}</ref>

<ref name="ovenden et al">{{cite journal |title=Towards better management of Australia's shark fishery: genetic analyses reveal unexpected ratios of cryptic blacktip species ''Carcharhinus tilstoni'' and ''C. limbatus'' |author1=Ovenden J.R. |author2=Morgan J.A.T. |author3=Kashiwagi T. |author4=Broderick, D. |author5=Salini, J. |journal=Marine and Freshwater Research |volume=61  |issue=2 |pages=253–262  |date=2010 |url=http://www.publish.csiro.au/?paper=MF09151 |doi=10.1071/MF09151}}</ref>

<ref name="salini et al">{{cite journal |title=Diets of sharks from estuaries and adjacent waters of the North-eastern Gulf of Carpentaria, Australia |author1=Salini, J.P. |author2=Blaber, S.J.M. |author3=Brewer, D.T. |journal=Australian Journal of Marine and Freshwater Research |volume=43 |issue=1 |pages=87–96 |date=1992 |url=http://www.publish.csiro.au/paper/MF9920087 |doi=10.1071/MF9920087}}</ref>

<ref name="schaeffner and beveridge">{{cite journal |author1=Schaeffner, B.C. |author2=Beveridge, I. |year=2013 |title=''Pristiorhynchus palmi'' n. g., n. sp. (Cestoda: Trypanorhyncha) from sawfishes (Pristidae) off Australia, with redescriptions and new records of six species of the Otobothrioidea Dollfus, 1942 |journal=Systematic Parasitology |volume=84 |issue=2 |pages=97–121 |doi=10.1007/s11230-012-9391-6 |pmid=23299749}}</ref>

<ref name="simpfendorfer">{{cite journal |author=Simpfendorfer, C.A. |year=1993 |title=Pandarid copepods parasitic on sharks from north Queensland waters |journal=Memoirs of the Queensland Museum |volume=33 |issue=1 |page=290}}</ref>

<ref name="simpfendorfer and milward">{{cite journal |author1=Simpfendorfer, C.A. |author2=Milward, N.E. |title=Utilisation of a tropical bay as a nursery area by sharks of the families Carcharhinidae and Sphyrnidae |journal=Environmental Biology of Fishes |date=August 1, 1993 |pages=337–345 |volume=37 |issue=4 |url=http://www.springerlink.com/index/J826208X46253058.pdf |doi=10.1007/BF00005200}}</ref>

<ref name="stevens">{{cite book |chapter=Management of shark fisheries in northern Australia |author=Stevens, J.D. |editor=Shotton, R. |title=Case studies of the management of elasmobranch fisheries 1 |publisher=Food and Agricultural Organization of the United Nations |year=1999 |isbn=92-5-104291-8 |chapterurl=http://www.fao.org/DOCREP/003/X2097E/X2097E20.htm}}</ref>

<ref name="stevens et al">{{cite journal |author1=Stevens, J.D. |author2=West, G.J. |author3=Mcloughlin, K.J. |date=2000 |title=Movement, recapture patterns, and factors affecting the return rate of carcharhinid and other sharks tagged off Northern Australia |journal=Marine and Freshwater Research |volume=51 |pages=127–141 |url=http://www.publish.csiro.au/?paper=MF98158 |doi=10.1071/MF98158 |issue=2}}</ref>

<ref name="stevens and wiley">{{cite journal |title=Biology of two commercially important carcharhinid sharks from northern Australia |author1=Stevens, J.D. |author2=Wiley, P.D. |journal=Australian Journal of Marine and Freshwater Research |volume=37 |issue=6 |pages=671–688 |date=1986 |url=http://www.publish.csiro.au/paper/MF9860671 |doi=10.1071/MF9860671}}</ref>

<ref name="velez-zuazoa and agnarsson">{{cite journal |author1=Vélez-Zuazoa, X. |author2=Agnarsson, I. |title=Shark tales: A molecular species-level phylogeny of sharks (Selachimorpha, Chondrichthyes) |journal=Molecular Phylogenetics and Evolution |volume=58 |issue=2 |date=February 2011 |pages=207–217 |url=http://www.mendeley.com/research/shark-tales-molecular-species-level-phylogeny-sharks-selachimorpha-chondrichthyes/ |doi=10.1016/j.ympev.2010.11.018 |pmid=21129490}}</ref>

<ref name="voigt and weber">{{cite book |author1=Voigt, M. |author2=Weber, D. |title=Field Guide for Sharks of the Genus ''Carcharhinus'' |publisher=Verlag Dr. Friedrich Pfeil |year=2011 |isbn=978-3-89937-132-1 |pages=99–101}}</ref>

<ref name="whitley">{{cite journal |author=Whitley, G. P. |date=July 7, 1950 |title=A new shark from north-western Australia |journal=Western Australian Naturalist |volume=2 |issue=5 |pages=100–105}}</ref>

}}

==External links==
* [http://fishbase.org/summary/Carcharhinus-tilstoni.html "''Carcharhinus tilstoni'', Australian blacktip shark" at FishBase]
* [http://www.iucnredlist.org/apps/redlist/details/41739/0 "''Carcharhinus tilstoni'' (Australian Blacktip Shark, Blacktip Whaler)" at IUCN Red List]
* [http://shark-references.com/species/view/Carcharhinus-tilstoni "Species description of ''Carcharhinus tilstoni''" at Shark-References.com]

{{Carcharhinidae}}

{{Good article}}
{{taxonbar}}

[[Category:Carcharhinidae]]
[[Category:Animals described in 1950]]
[[Category:Fish of Australia]]
[[Category:Marine edible fish]]
[[Category:Endemic fauna of Australia]]