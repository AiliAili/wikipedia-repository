{{Infobox journal
| title = Open Physics
| formernames = Central European Journal of Physics
| discipline = [[Physics]]
| abbreviation = Open Phys.
| publisher = [[Walter de Gruyter]]
| frequency = Monthly
| history = 2003-present
| impact = 0.948
| impact-year = 2015
| openaccess= Yes
| license = [[Creative Commons-BY-NC-ND]]
| website = http://www.openphysics.com/
| link1 = http://www.springer.com/physics/journal/11534
| link1-name = Journal homepage at Springer
| link2 = http://www.facebook.com/CEJPhysics
| link2-name = Journal fanpage
| eISSN = 2391-5471
| ISSN2label = '' Central European Journal of Physics'':
| ISSN2 = 1895-1082
| eISSN2 = 1644-3608
}}
'''''Open Physics''''' is a monthly [[peer-reviewed]] [[open access]] [[scientific journal]] covering all aspects of [[physics]]. It is published by [[De Gruyter Open]] and the [[editor-in-chief]] Jonathan L. Feng ([[University of California, Irvine]]). Occasionally, the journal publishes special issues on a specific topic.

==History==
The journal was established in 2003 as the ''Central European Journal of Physics''. It was co-published by [[Versita]] and [[Springer Science+Business Media]]. The founding editor-in-chief was Janos Lendvai ([[Eotvos Lorand University]]), who was succeeded in 2004 by [[Vladimir E. Zakharov]] ([[University of Arizona]] and [[Lebedev Physical Institute]]) and in 2011 by Feng.

In 2014 the journal was moved to the De Gruyter Open imprint. It obtained its current name in 2015 and simultaneously became [[open access]].

== Abstracting and indexing ==
The journals is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]] 
* [[Advanced Polymers Abstracts]] 
* [[Aluminium Industry Abstracts]] 
* [[Astrophysics Data System]]
* [[Ceramic Abstracts]]/[[World Ceramics Abstracts]] 
* [[Chemical Abstracts Service]]
* [[Computer and Information Systems Abstracts]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences, 
* [[Earthquake Engineering Abstracts]] 
* [[Inspec]]
* [[METADEX]]
* [[Mechanical & Transportation Engineering Abstracts]] 
* [[Scopus]]
* [[Science Citation Index Expanded]]
* [[Solid States and Superconductivity Abstracts]]
* [[GeoRef]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.948.<ref name=WoS>{{cite book |year=2016 |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.openphysics.com/}}

[[Category:Physics journals]]
[[Category:Publications established in 2003]]
[[Category:English-language journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Walter de Gruyter academic journals]]