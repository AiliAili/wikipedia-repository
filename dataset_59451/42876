{{For|the Louisiana politician|Gordon Dove (politician)}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Gove
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Single seat sports [[ultralight aircraft]]
 | national origin=[[United Kingdom]]
 | manufacturer=Premier Aircraft Construction Ltd
 | designer=S.C. Buszard
 | first flight=3 March 1937
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Gordon Dove''' was a [[United Kingdom|British]] low powered, low wing single seat [[monoplane]], built for the ultralight sports market in competition with aircraft like the Belgian designed [[Tipsy S]].  There was little enthusiasm for such machines in England in the late 1930s and only three were constructed.

==Design and development==

In 1935 Mervyn Chadwick and Raymond Gordon had come together to develop the then popular [[Flying Flea]] or other low powered aircraft suited to a predicted market in low cost, single seat flying. The appearance in England of the single seat Tipsy S in the following summer suggested that a low winged cantilever monoplane might be more popular and safer, so they set out to produce an aircraft along the same lines.  This became the Gordon Dove, built by Chadwick and Gordon's Premier Aircraft Constructions Ltd, formed in November 1936.  Though some commentators have described this as "plagiarism" from the Tipsy, it was significantly different from its inspiration and easily distinguished when seen in the air, in plan view.<ref name="OH">{{harvnb|Odr-Hume|2000|pp=366–7}}</ref>

The wings of the Dove were strongly tapered to a narrow tip, though the straight leading edge was only slightly swept.   Wide span ailerons covered much of the straight, forward swept, trailing edge. The wing was built around a main wooden box spar transversely braced to a subsidiary rear spar and plywood covered from the latter forward. Behind this rear spar the wing was fabric covered.<ref name="JAWA38">{{harvnb|Bridgman|1972|pp=43c}}</ref>  The outer panels were readily detachable for transport. The wide track undercarriage had a single vertical long travel leg on each side to the main spar, with a small bracing strut forward.<ref name="OH"/> The first two of the three Doves built had their wheels enclosed in spats, with unfaired legs, but the third used trouser fairings.<ref name="OH"/><ref name="JAWA38"/>

The fuselage was a rectangular plywood covered structure, slender in plan.  A 28&nbsp;hp (21&nbsp;kW) Aero Engines Sprite, an air-cooled  flat twin engine, was mounted on steel bearers and well streamlined. The single cockpit was over the wing, with a baggage compartment aft.  The Dove was offered with an enclosed Coupé cockpit cover.  The tail unit was built of fabric covered wood.  Both horizontal and vertical surfaces were heavily revised for the third aircraft, though on all three, all control surfaces were [[horn balanced]].<ref name="JAWA38"/>  The early machines had tailplanes which were strut braced to the top of the fin, with a straight leading edge of modest sweep and carrying generous, curved elevators.  The fin was sharply triangular, but the rudder extended the leading edge line, was curved at the rear and ended above the elevators.  On the third Dove the bracing was abandoned, the leading edge sweep increased and the elevators decreased in chord and made straight edged.  Its vertical surfaces were altered to produce a more conventional shape, the rudder projecting less above the fin but continuing downwards between the elevators to the fuselage bottom.<ref name="OH"/><ref name="JAWA38"/>

The Dove ''G-AETU'' first flew on 3 March 1937 from Maylands Aerodrome, [[Romford]], with C. Oscroft at the controls. It was rapidly advertised for sale at £225, a price that included a flying course to pilot's licence standard at the Romford Flying Club (RFC), an organisation in which Chadwick and Gordon had financial interests.  Alternatively, a kit of parts was offered at £165, though no kits were sold.<ref name="OH"/>  The first aircraft was sold to RFC and a production line of seven aircraft was planned, though only two were built. Premier Aircraft Constructions went into Receivership in August 1938.  The Dove's failure to sell was at least partly due to the lack of interest in England in single seat sports aircraft, particularly underpowered ones, an attitude that also led to the rapid abandonment of the production under licence of the Tipsy S. which had inspired the Dove.<ref>{{harvnb|Ord-Hume|2000|pp=486}}</ref>  The second Dove was scrapped in May 1939 after being offered for sale complete at £90<ref>[http://www.flightglobal.com/pdfarchive/view/1939/1939%20-%201366.html ''Flight'' 4 May 1939  p.37]</ref> and the first was destroyed in a fire in February 1940.<ref name="OH"/>  The third aircraft was damaged in a forced landing in September 1937, after just two weeks of flight and was kept in store awaiting repair<ref name="OH"/> whilst efforts were made to sell it as found.<ref>[http://www.flightglobal.com/pdfarchive/view/1939/1939%20-%200728.html ''Flight'' 9 March 1939  p.48]</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (first two aircraft)==
{{Aircraft specs
|ref={{harvnb|Bridgman|1972|pp=43c}}
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=18
|length in=3
|length note=
|span m=
|span ft=27
|span in=3
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=5
|height in=8
|height note=
|wing area sqm=
|wing area sqft=112
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=382
|empty weight note=
|gross weight kg=600
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name= Aero Engines Sprite
|eng1 type=air cooled flat twin
|eng1 kw=<!-- prop engines -->
|eng1 hp=28<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=95
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=85
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=400
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=800
|climb rate note=initial
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=5.4
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Citations===
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6 |ref=harv}}
*{{cite book |title= Jane's All the World's Aircraft 1938|last= Grey |first= C.G. |coauthors= |edition= |year=1972|publisher= David & Charles|location= London|isbn=0-7153-5734-4|ref=harv }}
{{refend}}

<!-- ==External links== -->

[[Category:British sport aircraft 1930–1939]]