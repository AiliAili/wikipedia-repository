{{advert|date=February 2016}}

{{Infobox company 
  |name           = Drybar 
  |logo = Drybar logo.jpg
|foundation     = [[Brentwood, California]] (2008)
  |locations      = 69 <ref name=about>{{cite web|title=About Us|url=https://www.thedrybar.com/about-us/|website=thedrybar.com|accessdate=1 November 2016}}</ref>
  |founders        = Alli Webb, Michael Landau, Cameron Webb <!-- we have no biogs -->
  |homepage        = {{URL|thedrybar.com}} 
  }}

'''Drybar''' is a [[California]]-based chain of salons that solely provide hair styling service, also known as "blowouts", founded in 2008<ref name="Forbes">{{cite news|last1=Meghan|first1=Casserly|title=Drybar: How One Woman And A Hair Dryer Became A $20 Million Operation|url=http://www.forbes.com/sites/meghancasserly/2012/11/01/drybar-how-one-woman-and-a-hair-dryer-became-a-20-million-operation/|work=Forbes|date=2012-11-11}}</ref>

== History ==

In 2008, Alli Webb began a side business called Straight At Home.<ref>{{cite web|title=About Us|url=http://www.thedrybar.com/about-us/|website=Drybar}}</ref> She quickly outgrew her one-woman operation and realized there was a huge hole in the market for just blowouts.<ref name="eonline">{{cite news|last1=Chan|first1=Jennifer|title=Trendsetters at Work: Drybar Founder Alli Webb|url=http://www.eonline.com/news/441320/trendsetters-at-work-drybar-s-founder-alli-webb|work=E Online|date=2013-07-22}}</ref> Along with her brother Michael Landau, former Vice President of Brand Marketing at [[Yahoo!]], and her husband, Cameron Webb, former Creative Director at the pretigious ad agency Secret Weapon Marketing,<ref name="USA Today">{{cite news|last1=Petrecca|first1=Laura|title=No Haircuts or color: Blowdry bars are a booming business|url=http://usatoday30.usatoday.com/money/economy/story/2012-08-12/drybar-blowdry-bar/56939214/1|work=USA Today|date=2012-08-13}}</ref> Alli co-founded what would become Drybar with a salon in Brentwood, California in 2010. The following year, along with the help of friends, they were able to raise $2.5 million in order to expand the business.<ref name="Huffington Post">{{cite news|last1=Bronner|first1=Sasha|title=Drybar’s Alli Webb Talks Borrowing Money and How to Make a Blowout Last: My LA|url=http://www.huffingtonpost.com/2013/06/19/drybar-alli-webb-borrowing-money_n_3460941.html|work=Huffington Post|date=2013-06-19}}</ref>  Once their business had been expanded, Drybar looked to add to their investors as well as their board of trustees.  After an extensive search, they added Castanea Partners, a [[Boston]]-based private equity firm, to their list of investors. Additionally, [[Paul Pressler]], former [[Chief executive officer|CEO]] of [[Gap Inc.|GAP]] and President of [[The Walt Disney Company|Disney]], became an investor and Board member. [[Janet Gurwitch]], the founder and former CEO of [[Laura Mercier Cosmetics]], also became an investor and Board member at Drybar.<ref>{{cite web|title=Drybar Announces New Investment- Appointment of Paul Pressler and Janet Gurwitch to Board|url=http://www.prnewswire.com/news-releases/drybar-announces-new-investment---appointment-of-paul-pressler-and-janet-gurwitch-to-board-137748663.html|website=PR Newswire|date=2012-01-20}}</ref>

Drybar's motto is "No cuts. No color. Just blowouts."<ref name="USA Today" />

Everything at Drybar is designed with the “bar vernacular” in mind. The cashiers are aptly called “bartenders” and hairstyles are named after cocktails such as the [[Cosmopolitan (cocktail)|Cosmo]], [[Mai Tai]], or [[Manhattan (cocktail)|Manhattan]].<ref name="Huffington Post" /> The idea behind this came from Webb who believed that “women [should want] to come in and have fun” and "what's more fun than going to your local bar?"<ref name="USA Today" /> Webb’s vision is present in the designs brought to life by Josh Heitler. Instead of the typical salon set up, clients at Drybar “sit facing a U-shaped or single-stretch bar, with their backs to the mirrors,” which brings to mind sitting at a bar rather than being at a salon.<ref name="Forbes" />

Aesthetically, all Drybars look roughly the same, due mainly in part to Josh Heitler.<ref name="Huffington Post" /> Heitler, the principal of a boutique architectural firm and now partner in the company,<ref name="Huffington Post" /> came up with the design elements and look of Drybar.<ref name="Forbes" /> The reasoning behind the name Drybar, is evident in the design.

== Locations ==

Drybar revenue grew from $1.5 million in 2010 to a revenue of  $19 million in 2012 to $39 million in 2013.<ref name="beast">{{cite news|last1=Meany|first1=Kelsey|title=Blow Dry Bars Are a Thriving Industry Disrupting the Salon Business|url=http://www.thedailybeast.com/articles/2013/07/13/blow-dry-bars-are-a-thriving-industry-disrupting-the-salon-business.html|work=The Daily Beast: Business|date=2013-07-13}}</ref> As of January 2016, Drybar has 66<ref name="about" /> locations in 11 states, [[Washington, D.C.|Washington DC]] and [[Vancouver]], [[British Columbia]]<ref name="eonline" /><ref>{{cite web|title=Find a Drybar Near You|url=http://www.thedrybar.com/locations/|website=Drybar}}</ref>

== Products ==

Webb, a longtime professional stylist, spent several years developing a complete line of products. This came about “after trying nearly every existing product line." Webb just couldn't find what she was looking for to meet their very specific styling needs, so she “partnered with some of the best labs in the world to create exactly what [was] needed at Drybar."<ref name="beast" />

After being frustrated with the product and tool choices available, Webb (along with the help of Board member and investor Janet Gurwitch) developed a line specifically for Drybar and blowouts. In 2013, after testing the line in about 70 [[Sephora]] locations, Drybar then went ahead with 300+ Sephora shops and [[QVC]] to launch their line of products.<ref>{{cite news|author1=Staff|title=Drybar Launches Product Line, Names New CFO|url=http://www.bizjournals.com/newyork/news/2013/01/03/drybar-launches-product-line-names.html?page=all|work=New York Business Journal|date=2013-01-03}}</ref>  These products range from brushes to a blow dryer to hair accessories to various spritzes, sprays, and creams.<ref>{{cite web|title=Crafted for the Perfect Blowout|url=http://shop.thedrybar.com/|website=Drybar}}</ref>  These products are for home usage in order to lengthen the life of a Drybar blowout or for customers to achieve the look on their own. Buttercup, the official blow dryer used at Drybar, is the Drybar mascot and is sold for at-home use.<ref>{{cite web|title=Buttercup|url=http://shop.thedrybar.com/products/buttercup-blowdryer|website=Drybar}}</ref>

== Milestones ==
One of the core components to the “"Drybar Experience" is the ability for the salons to serve a glass of [[champagne]] or [[wine]] to their clients as they are receiving a service. Due to alcohol licensing laws, certain states and locations don’t allow for this. In October 2012,<ref name="USA Today" /> a Drybar location was opened in [[Bethesda, Maryland]]. When the regional manager, Courtney Barfield, realized this, both she and Drybar began working with District 16 Delegate [[Ariana Kelly]] to push legislation in 2014's General Assembly to allow for [[Montgomery County, Maryland|Montgomery County]] hair salons to provide complimentary wine, beer and champagne.<ref>{{cite news|last1=Kraut|first1=Aaron|title=Drybar Bethesda Celebrates New Alcohol Law|url=http://www.bethesdanow.com/2014/07/01/drybar-bethesda-celebrates-new-alcohol-law/|work=Bethesda Now|date=2014-07-01}}</ref>

On May 5, 2014, Governor [[Martin O'Malley]] signed the so-called Drybar Bill and the law was enacted on July 1, 2014.<ref name="beast" />

Governor O'Malley declared July 1 "Buttercup Day" in reference to Drybar's yellow blow dryer mascot.<ref name="beast" />

On September 4, 2011, Drybar was featured on an episode of the hit [[HBO]] show ''[[Entourage (U.S. TV series)|Entourage]]''. In the episode, studio head [[Dan Gordon (screenwriter)|Dana Gordon]] stops by the blow dry bar, which is a favorite of celebrities such as [[Emma Roberts]], [[Zooey Deschanel]], [[Renée Zellweger]], [[Cindy Crawford]] and [[Perrey Reeves|Perry Reeves]].<ref>{{cite news|last1=Hogan|first1=Kate|title=Entourage' Books a Stylish Guest Star: Drybar Blow Dry Shop|url=http://stylenews.peoplestylewatch.com/2011/09/02/entourage-season-8-cameo-drybar/|work=StyleWatch|date=2011-09-02}}</ref>

On December 7, 2012, Webb was featured on ''[[Katie (talk show)|Katie]]'' and was interviewed by [[Katie Couric]] about Drybar and how it came to be.<ref>{{cite episode|title=Interview with Alli Webb|network=ABC|series=Katie|date=2012-12-07}}</ref>

On January 9, 2017, Webb was interviewed on [[How I Built This]], and was interviewed by [[Guy Raz]].<ref>{{Cite news|url=http://www.npr.org/podcasts/510313/how-i-built-this|title=How I Built This|last=|first=|date=|work=|newspaper=[[NPR]]|access-date=2017-01-09|via=}}</ref>

==References==
{{reflist|25em}}

==External links==
* {{official website |thedrybar.com}}

[[Category:Beauty]]
[[Category:2009 establishments in California]]