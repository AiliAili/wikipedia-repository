:''This article is about artificial seismic sources. For natural seismic sources, see [[earthquake]], [[volcano]], and related articles.''
[[Image:Air gun hg.jpg|thumb|An air gun seismic source (30 litre)]]
A '''seismic source''' is a device that generates controlled [[seismic]] [[energy]] used to perform both [[reflection seismology|reflection]] and [[seismic refraction|refraction]] seismic surveys. A seismic source can be simple, such as [[dynamite]], or it can use more sophisticated technology, such as a specialized [[air gun]]. Seismic sources can provide single pulses or continuous sweeps of energy, generating [[seismic wave]]s, which travel through a [[Medium (optics)|medium]] such as [[water]] or layers of [[rock (geology)|rock]]s. Some of the waves then [[Reflection (physics)|reflect]] and [[Refraction|refract]] and are recorded by receivers, such as [[geophone]]s or [[hydrophone]]s.<ref>R.E. Sheriff (2002) p160 & p 182</ref>

Seismic sources may be used to investigate shallow subsoil structure, for engineering site characterization, or to study deeper structures, either in the search for petroleum and mineral deposits, or to map subsurface faults or for other scientific investigations. The returning signals from the sources are detected by seismic sensors ([[geophone]]s or [[hydrophone]]s) in known locations relative to the position of the source. The recorded signals are then subjected to specialist processing and interpretation to yield comprehensible information about the subsurface.<ref>R.E. Sheriff (2002) p312</ref>

==Source model==
A seismic source signal has the following characteristics:
# Generates an [[impulse function|impulse]] signal
# Band-limited
# The generated waves are time-varying
The generalized equation that shows all above properties is:
:<math>s(t)=\beta e^{-\alpha t^2} \sin(2 \pi f_{max} t)</math>
where <math>f_{max}</math> is the maximum frequency component of the generated waveform.<ref>[http://www.ipp.mpg.de/de/for/bereiche/stellarator/Comp_sci/CompScience/csep/csep1.phy.ornl.gov/sw/sw.html Seismic Wave Propagation Modeling and Inversion, Phil Bording]</ref>

==Types of sources==

===Hammer===
The most basic seismic source is a [[sledge hammer]], either striking the ground directly, or more commonly striking a metal plate on the ground, known as hammer and plate. Useful for seismic [[refractio]]n surveys down to about 20 m below surface.

===Explosives===
[[Explosives]], such as [[dynamite]], can be used as crude but effective sources of seismic energy. For instance, [[hexanitrostilbene]] was the main explosive fill in the ''thumper'' [[Mortar (weapon)|mortar round]] canisters used as part of the [[Apollo Lunar Surface Experiments Package|Apollo Lunar Active Seismic Experiments]].<ref>[http://www.lpi.usra.edu/lunar/documents/NASA%20RP-1036.pdf NASA reference publication]</ref> Generally, the explosive charges are placed between {{convert|20|and|250|ft|0|disp=flip}} below ground, in a hole that is drilled with dedicated drilling equipment for this purpose.  This type of seismic drilling is often referred to as "Shot Hole Drilling". A common drill rig used for "Shot Hole Drilling" is the ARDCO C-1000 drill mounted on an ARDCO K 4X4 buggy.  These drill rigs often use water or air to assist the drilling.

===Air gun===
An '''air gun''' is used for marine [[Reflection seismology|reflection]] and refraction surveys. It consists of one or more [[pneumatic]] chambers that are pressurized with compressed air at pressures from {{convert|2000|to|3000|psi|MPa|disp=flip|abbr=on}}. Air guns are submerged below the water surface, and towed behind a ship. When an air gun is fired, a solenoid is triggered, which releases air into a fire chamber which in turn causes a piston to move, thereby allowing the air to escape the main chamber and producing a pulse of [[Underwater acoustics|acoustic]] energy.<ref>R.E. Sheriff (2002) p6-8</ref> Air gun arrays may consist of up to 48 individual air guns with different size chambers, fired in concert, the aim being to create the optimum initial shock wave followed by the minimum reverberation of the air bubble(s).

Air guns are made from  the highest grades of corrosion resistant stainless steel. Large chambers (i.e., greater than 1.15&nbsp;L or 70&nbsp;cu&nbsp;in) tend to give low frequency signals, and the small chambers (less than 70 cubic inches) give higher frequency signals.

===Plasma sound source===
[[Image:PSS in swimmingpool 01.jpg|thumb|right|Plasma sound source fired in small swimming pool]]
A '''plasma sound source''' (PSS), otherwise called a '''spark gap sound source''', or simply a '''sparker''', is a means of making a very low frequency [[sonar]] pulse underwater. For each firing, electric charge is stored in a large high-voltage bank of [[capacitor]]s, and then released in an arc across electrodes in the water. The underwater spark discharge produces a high-pressure plasma and vapor bubble, which expands [[cavitation|and collapses]], making a loud sound.<ref>R.E. Sheriff (2002) p328</ref> Most of the sound produced is between 20 and 200&nbsp;Hz, useful for both [[reflection seismology|seismic]] and [[sonar]] applications.

There are also plans to use PSS as a [[Anti-frogman techniques#Audible sound|non-lethal weapon against submerged divers]]{{Citation needed|date=June 2015}}.

===Thumper truck===
[[File:Thumper trucks, Noble Energy.jpg|thumb|Thumper trucks, [[Noble Energy]], northern Nevada 2012.]]
In 1953, the weight dropping Thumper technique was introduced as an alternative to dynamite sources.
[[File:Vibroseis.jpg|thumb|Vibroseis]]
[[File:Vibroseis 2.jpg|thumb|Vibroseis 2]]
A '''thumper truck''' (or weight-drop) truck is a vehicle-mounted ground impact system which can be used to provide a seismic source. A heavy weight is raised by a hoist at the back of the truck and dropped, generally about three meters, to impact (or "thump") the ground.<ref>R.E. Sheriff (2002) p357</ref> To augment the signal, the weight may be dropped more than once at the same spot, the signal may also be increased by thumping at several nearby places in an array whose dimensions may be chosen to enhance the seismic signal by spatial filtering.

More advanced Thumpers use a technology called "'''Accelerated Weight Drop'''" (AWD), where a high pressure gas (min {{convert|1000|psi|MPa|abbr=on||disp=flip}}) is used to accelerate a heavy weight Hammer (5,000&nbsp;kg) to hit a base plate coupled to the ground from a distance of 2 to 3&nbsp;m. Several thumps are stacked to enhance signal to noise ratio. AWD allows both more energy and more control of the source than gravitational weight-drop, providing better depth penetration, control of signal frequency content.

Thumping may be less damaging to the environment than firing explosives in shot-holes{{Citation needed|date=June 2015}}, though a heavily thumped seismic line with transverse ridges every few meters might create long-lasting disturbance of the soil. An advantage of the thumper (later shared with Vibroseis), especially in politically unstable areas, is that no explosives are required.

===Electromagnetic Pulse Energy Source (Non-Explosive)===

EMP sources based on the electrodynamic and electromagnetic principles.

===Seismic vibrator===

A [[Seismic vibrator]] propagates energy signals into the [[Earth]] over an extended period of time as opposed to the near instantaneous energy provided by impulsive sources. The data recorded in this way must be [[cross-correlation|''correlated'']] to convert the extended source signal into an impulse. The source signal using this method was originally generated by a servo-controlled hydraulic vibrator or ''shaker unit'' mounted on a mobile base unit, but [[electromechanics|electro-mechanical]] versions have also been developed.

The "Vibroseis" exploration technique was developed by the [[Conoco Inc.|Continental Oil Company (Conoco)]] during the 1950s and was a trademark until the company's [[patent]] lapsed.

===Boomer sources===
Boomer sound sources are used for shallow water seismic surveys, mostly for engineering survey applications. Boomers are towed in a floating sled behind a survey vessel. Similar to the plasma source, a boomer source stores energy in capacitors, but it discharges through a flat spiral coil instead of generating a spark. A copper plate adjacent to the coil flexes away from the coil as the capacitors are discharged. This flexing is transmitted into the water as the seismic pulse.<ref>Sheriff R. E., 1991, Encyclopedic Dictionary of Exploration Geophysics, Society of Exploration Geophysicists, Tulsa, 376p</ref>

Originally the storage capacitors were placed in a steel container (the '''bang box''') on the survey vessel. The high voltages used, typically 3,000 V, required heavy cables and strong safety containers. Recently, low voltage boomers have become available.<ref>Jopling J. M., Forster P. D., Holland D. C. and Hale R. E., 2004, Low Voltage Seismic Sound Source, US Patent No 6771565</ref> These use capacitors on the towed sled, allowing efficient energy recovery, lower voltage power supplies and lighter cables. The low voltage systems are generally easier to deploy and have fewer safety concerns.

===Noise sources===
Correlation-based processing techniques also enable seismologists to image the interior of the Earth at multiple scales using natural (e.g., the oceanic microseism) or artificial (e.g., urban) background noise as a seismic source.<ref>R.E. Sheriff (2002) p295</ref>  For example, under ideal conditions of uniform seismic illumination, the correlation of the noise signals between two seismographs provides an estimate of the bidirectional seismic [[impulse response]].

==See also==
*[[Reflection seismology]]
*[[seismic refraction]]

==References==
<references/>

==Bibliography==
* Crawford, J. M., Doty, W. E. N. and Lee, M. R., 1960, Continuous signal seismograph: Geophysics, Society of Exploration Geophysicists, 25, 95-105.
* Robert E. Sheriff, Encyclopedic Dictionary of Applied Geophysics (Geophysical References No. 13) 4th Edition, 2002, 429 pag. ISBN 978-1560801184.
*Snieder, R., 2004, Extracting the Green's function from the correlation of coda waves: A derivation based on stationary phase, Phys. Rev. E., 69, 4, 046610.
* Seismic Wave Propagation Modeling and Inversion, Phil Bording [http://www.ipp.mpg.de/de/for/bereiche/stellarator/Comp_sci/CompScience/csep/csep1.phy.ornl.gov/sw/sw.html]
* Derivation of Seismic wave equation can be found here. [http://www.ees.nmt.edu/Geop/Classes/GEOP523/Docs/waveeq.pdf]

==External links==
*[http://www.naturalgas.org/naturalgas/exploration.asp Photos of Thumper trucks in action]
*[http://arctic.fws.gov/seismic.htm Arctic Refuge thumper trails]
*[http://www.sierraclub.org/ut/careforutah/p/DSCN4683.html Utah thumper trails]
*[http://www.spawar.navy.mil/sti/publications/pubs/td/3138/td3138cond.pdf Non-Lethal Swimmer Neutralization Study by The University of Texas, May 2002] page 42
*[http://www.vibroseis.com/ Vibroseis, Omnilaw International, a Texas Corporation]
*[https://www.youtube.com/watch?v=lRsAO7Y2k60&list=PL369AB2240FF8D16B&index=1 Illustration of Vibroseis in 3D land seismic acquisition]

<br>
{{petroleum industry}}

{{DEFAULTSORT:Seismic Source}}
[[Category:Sonar]]
[[Category:Seismology measurement]]