{{about|the airport in South Australia| the associated suburb |Adelaide Airport, South Australia}}
{{Use Australian English|date=May 2014}}
{{Use dmy dates|date=May 2014}}
{{Infobox airport
| name = Adelaide Airport
| image = Adelaide Airport logo.svg
| image-width = 200
| image2 = Adelaideterminal.jpg
|caption2 = Adelaide Airport T1, Qantas Check in Desks
| image2-width = 250
| IATA = ADL
| ICAO = YPAD
| type = Public
| owner =
| operator = Adelaide Airport Limited
| city-served = [[Adelaide]]
| location = [[Adelaide Airport, South Australia|Adelaide Airport]], [[South Australia]]
| hub = [[Alliance Airlines]] <br/> [[Qantas]] <br/>[[Regional Express Airlines]] <br/>[[Sharp Airlines]] <br/> [[Cobham Aviation Services Australia|Cobham]]
| focus_city = [[Virgin Australia]] <br/>[[Jetstar Airways]]
| elevation-f = 20
| coordinates = {{coord|34|56|42|S|138|31|50|E|region:AU-SA|display=inline,title}}
| pushpin_map = Australia Greater Adelaide#South Australia#Australia
| pushpin_label = ADL
| website = [http://www.adelaideairport.com.au/ adelaideairport.com.au]
| metric-rwy = Yes
| r1-number = 05/23
| r1-length-m = 3,100
| r1-surface = [[Asphalt]]
| r2-number = 12/30
| r2-length-m = 1,652
| r2-surface = [[Asphalt]]
| stat-year = 
| stat1-header = PassengersYE 2016
| stat1-data = 8,007,000<ref>https://www.facebook.com/AdelaideAirport/posts/1419898551374184:0</ref>
| stat2-header = Movements 2015
| stat2-data = 78,072
| stat3-header = Freight (Tonnes) 2015
| stat3-data = 20 822
| footnotes = Sources: [[Department of Infrastructure and Regional Development|BITRE]] <ref name="airportdata">{{cite web|url=http://bitre.gov.au/publications/ongoing/files/WebAirport_FY_1986-2015.xls |title=Airport Traffic Data 1985–86 to 2014–15  |publisher=BITRE |date=March 2016 |accessdate=15 March 2016}}</ref>
}}

'''Adelaide Airport''' {{Airport codes|ADL|YPAD}} is the principal airport of [[Adelaide]], South Australia and the [[List of the busiest airports in Australia|fifth busiest airport in Australia]], servicing just over eight million passengers in the calendar year ending 31 December 2016.<ref name="Adelaide Airport">{{cite web|url=http://www.adelaideairport.com.au/corporate/wp-content/uploads/2014/11/nr-8-million-pax-01.17-v2.pdf|title=Adelaide Airport reaches 8 million passengers in 2016|last=|first=|date=|website=|publisher=|format=pdf|deadurl=no|accessdate=17 January 2017}}</ref> Located adjacent to [[West Beach, South Australia|West Beach]], it is approximately {{convert|6|km|abbr=on}} west of the [[Adelaide city centre|city-centre]]. It has been operated privately by Adelaide Airport Limited under a long-term lease from the [[Government of Australia|Commonwealth Government]] since 29 May 1998.<ref name=bitre72>{{cite web | title=Air passengermovements through capital city airports to 2025–26 | work=Working Paper 72 | year=2008 | publisher=[[Bureau of Infrastructure, Transport and Regional Economics]] | location=Canberra | format=PDF | url=http://www.bitre.gov.au/publications/2008/files/wp_072.pdf | accessdate=16 May 2012}}</ref>{{rp|p 25}}

First established in 1955, a new dual international/domestic [[Airport terminal|terminal]] was opened in 2005 which has received numerous awards, including being named the world's second-best [[international airport]] (5–15&nbsp;million passengers) in 2006.<ref>{{cite web|title=Adelaide Airport: T1 |publisher=Adelaide Airport Limited |url=http://www.aal.com.au/lib/pdf/mf55.pdf |accessdate=1 November 2008 |archiveurl=https://web.archive.org/web/20080719195317/http://www.aal.com.au/lib/pdf/mf55.pdf |archivedate=19 July 2008 |deadurl=no |df=dmy }}</ref> Also, it has been named Australia's best capital city airport in 2006, 2009 and 2011.<ref>{{cite web|url=http://www.adelaideairport.com.au/assets/pdfs/media-releases/nr%20-%20airport%20award%2016%2011.pdf |title=Adelaide names Australia's best airport again}}</ref>

Over the 2016 calendar year, Adelaide Airport experienced passenger growth of 5.9% internationally and 2.1% for domestic and regional passengers.<ref name="Adelaide Airport"/>

==History==
The first Adelaide airport was an [[Aerodrome#Australian and Canadian usage|aerodrome]] constructed in 1921 on {{convert|24|ha|abbr=on}} of land in [[Hendon, South Australia|Hendon]]. The small facility allowed for a mail service between Adelaide and Sydney. To meet the substantial growth in aviation, [[Parafield Airport]] was developed in 1927. The demand on aviation outgrew Parafield and the current site of Adelaide Airport was selected at West Torrens (now West Beach) in January 1946.<ref>{{cite web|url=http://trove.nla.gov.au/newspaper/article/48688344/2628313|title=West Beach Airport Plan Approved|publisher=''The Advertiser'' 26 January 1946 page 1}}</ref> An alternative site at Port Adelaide, including a seaplane facility, was considered inferior and too far from the [[Central business district|C.B.D.]]<ref>{{cite web|url=http://trove.nla.gov.au/newspaper/article/43499423/2619483|title=Airport For Adelaide|publisher=''The Advertiser'' 27 June 1945 page 7}}</ref> Construction began and flights commenced in 1954. Parafield Airport was turned into a private and military aviation facility.

[[File:Adelaide Airport Tarmac 1967.jpg|thumb|left|Passengers boarding from the tarmac in December 1967; this continued for domestic passengers until 2006.]]
An annexe to one of the large [[hangar]]s at the airport served as a passenger terminal until the Commonwealth Government provided funds for the construction of a temporary building.<ref>{{cite web|title=History: 1927–2005 |publisher=Adelaide Airport Limited |url=http://www.aal.com.au/corporate/history.aspx |accessdate=14 October 2006 |archiveurl=https://web.archive.org/web/20061003162004/http://www.aal.com.au/corporate/history.aspx |archivedate=3 October 2006 |deadurl=no |df=dmy }}</ref> International services became regular from 1982 upon the construction of an international terminal. A new dual-use $260&nbsp;million facility replaced both the original 'temporary' domestic and international terminals in 2005.

In October 2006, the new terminal was named the Capital City Airport of the Year at the Australian Aviation Industry Awards in [[Cairns]].<ref>{{cite web|url=http://en.carnoc.com/list/1/1227.html |title=China Aviation News:Adelaide Airport Rated No. 1 in Australia |publisher=En.carnoc.com |date=18 October 2006|accessdate=30 May 2011| archiveurl= https://web.archive.org/web/20110708121305/http://en.carnoc.com/list/1/1227.html| archivedate= 8 July 2011| deadurl= no}}</ref> In March 2007, Adelaide Airport was rated the world's second best airport in the 5–15&nbsp;million passengers category at the [[Airports Council International]] (ACI) 2006 awards in [[Dubai]].<ref>{{cite web|url=http://en.carnoc.com/list/2/2228.html |title=Adelaide Airport Wins International Praise |publisher=En.carnoc.com |date=13 March 2007|accessdate=30 May 2011| archiveurl= https://web.archive.org/web/20110708121310/http://en.carnoc.com/list/2/2228.html| archivedate= 8 July 2011| deadurl= no}}</ref>

Plans were announced for an expansion of the terminal in July 2007, including more [[Jet bridge|aerobridges]] and demolition of the old International Terminal.<ref>{{cite news | url=http://www.news.com.au/adelaidenow/story/0,22606,22067819-5006301,00.html | accessdate=13 July 2007| title=Adelaide Airport boost | first=Stuart | last=Innes | date=12 July 2007| work=The Advertiser}}</ref>

On 5 August 2008 Tiger Airways Australia confirmed that Adelaide Airport would become the airline's second hub which would base two of the airline's [[Airbus A320]]s by early 2009.<ref name="tigerhub">{{cite news|title=Tiger sets up second home in Adelaide |work=Fairfax Digital |publisher=The Age |date=5 August 2008 |url=http://news.theage.com.au/national/tiger-sets-up-second-home-in-adelaide-20080805-3q9s.html |accessdate=5 August 2008 |location=Melbourne |deadurl=yes |archiveurl=https://web.archive.org/web/20090515020444/http://news.theage.com.au:80/national/tiger-sets-up-second-home-in-adelaide-20080805-3q9s.html |archivedate=15 May 2009 |df=dmy }}</ref> On 29 October 2009 Tiger announced it would be housing its third A320 at Adelaide Airport from early 2010.<ref>{{cite news| url=http://www.news.com.au/adelaidenow/story/0,22606,26277005-2682,00.html?from=public_rss | title=Tiger Airways base in Adelaide to grow by 50 per cent | publisher=News Limited | first=Stuart | last=Innes | date=29 October 2009| work=The Advertiser}}</ref> Tiger Airways later shut down its operations from Adelaide only to recommence them in 2013.<ref>{{cite news| url=http://news.smh.com.au/breaking-news-national/tiger-airways-future-aust-look-under-wraps-20110822-1j623.html | title=Tiger Airways future Aust look under wraps | work=The Sydney Morning Herald | date=22 August 2011}}</ref>

The airport encountered major problems during the [[2011 Puyehue eruption|eruption of Puyehue volcano in Chile]], the ash cloud caused flights to be cancelled nationwide, with over 40,000 passengers being left stranded in Adelaide.

==Previous terminals==
The original international terminal had only one gate with limited space for passengers. Check in desks were small and waiting space was limited. It was partially demolished{{when|date=April 2015}} to make the area more secure and allow aircraft to park on the other side of the terminal. The old domestic terminal was closed shortly after the new terminal was opened to flights and was demolished not long after. A new control tower was built west of the current terminal with the old control tower maintained for additional operations.

==Present terminal building==
[[File:A380 VH-OQA Adelaide.jpg|thumb|A large crowd watches Qantas A380 VH-OQA visit Adelaide, 27 September 2008]]
[[File:Adelaide Airport Terminal One Interior.JPG|thumb|Main concourse terminal one, 2006]]
The airport was redeveloped in 2005 at a cost of $260&nbsp;million. The redevelopment was managed by builders [[Hansen Yuncken]]. Before the redevelopment, the old airport terminal was criticised for its limited capacity and lack of aerobridges. {{Citation needed|date=November 2015}}

Proposals were developed for an upgraded terminal of world standard. The final proposal, released in 1997, called for a large, unified terminal in which both domestic and international flights would use the same terminal. A combination of factors, the most notable of which was the collapse of [[Ansett Australia]], then a [[duopoly]] domestic carrier with [[Qantas]], and the resultant loss of funds for its share of the construction cost, saw the new terminal plans shelved until an agreement was reached in 2002. {{Citation needed|date=November 2015}}

The new terminal was opened on 7 October 2005 by the Prime Minister [[John Howard]] and South Australian Premier [[Mike Rann]]. However, Adelaide Airport Limited announced soon afterward that only international flights would use the new facility immediately due to problems with the [[Aviation fuel|fuel]] pumps and underground pipes. These problems related initially to the anti-rusting agent applied to the insides of the fuel pumps, then to construction debris in the pipes. Although international and regional (from December 2005) aircraft were refuelled via tankers, a lack of space and safety concerns prevented this action for domestic jet aircraft, which instead continued operations at the old terminal. The re-fueling system was cleared of all debris and the new terminal was used for all flights from 17 February 2006.<ref>{{cite news |title=Passengers urged to be patient as new SA terminal opens |url=http://www.abc.net.au/news/newsitems/200602/s1572055.htm |publisher=ABC News |location=Australia |date=17 February 2006| accessdate=14 October 2006}}</ref> The new airport terminal is approximately {{convert|850|m|abbr=on}} end to end and is capable of handling 27 aircraft, including an [[Airbus A380]], simultaneously and processing 3,000 passengers per hour. It includes high-amenity public and airline lounges, 14 glass-sided aerobridges, 42 common user [[Airport check-in|check-in]] desks and 34 shop fronts. Free [[Wireless Internet Protocol|wireless Internet]] is also provided throughout the terminal by [[Internode Systems]], a first for an Australian airport.<ref>{{cite web |url=http://www.crn.com.au/story.aspx?CIID=25706 |title=Weaving wireless magic |accessdate=14 October 2006|author=Denise Murray |date=31 October 2005|publisher=[[CRN Australia|CRN]]| archiveurl= https://web.archive.org/web/20061010065737/http://www.crn.com.au/story.aspx?CIID=25706| archivedate= 10 October 2006| deadurl= no}}</ref>

The first Qantas A380, VH-OQA "[[Nancy Bird Walton]]", landed at the airport on 27 September 2008, Several thousand spectators gathered to catch a glimpse of the giant aircraft. This was a 25-minute stopover before it flew on to Melbourne. This was one of several visits the airliner made as part of a pilot training and testing program.

In July 2013, Adelaide Airport became the first Australian airport and second airport worldwide to have [[Google Street View]] technology, allowing passengers to explore the arrival and departure sections of the airport before travel.<ref>{{cite press release |url=http://www.adelaideairport.com.au/assets/pdfs/media-releases/nr%20-%20Google%20Street%20View%2007%2013%20web%20(2).pdf |title=Google Street View Technology First for Adelaide Airport |date=19 July 2013 |publisher=Adelaide Airport Limited |accessdate=10 April 2015}}</ref>

==Recent development==
As of 2011 a series of developments are either underway, approved or proposed for Adelaide Airport. In February 2011 a A$100&nbsp;million building program was launched as part of a five-year master plan. The developments which have been made public (whether part of the building plan or not) are listed below:

* New airport road network to improve traffic flow (completed)
* New [[multi-storey car park]] – increasing parking spaces from 800 to 1,650 (completed August 2012)<ref>{{cite web |url=http://www.adelaideairport.com.au/air-travel/to-and-from/parking |title=Parking |publisher=Adelaide Airport Limited |accessdate=10 April 2015}}</ref>
* New passenger terminal plaza frontage (completed March 2013)
* Walkway bridge connecting new car park and existing terminal building (completed March 2013)
* Terminal concourse extension
* Three new aerobridges
* Terminal commercial projects and passenger facilities
* Relocation of regional carrier Rex
* Relocation of old transportable charter aircraft operators' terminal
* New control tower, twice the height of the old tower, expected to cost A$16.9&nbsp;million (opened early 2012)
* Addition of Emirates airlines, Qatar Airways and China Southern Airlines to the list of airlines serving the airport.
* Adelaide Airport Hotel ({{Convert|37|m|abbr=on}} tall, nine levels)
* New airside cargo facility (1500sqm)[http://www.tandlnews.com.au/2017/03/01/article/big-new-cargo-facility-opens-adelaide-airport/]

==Airlines and destinations==

===Passenger===
[[File:The Adelaide Airport at night.jpg|thumb|The tarmac of the regional Gate 50]]
{{Airport destination list
| 3rdcoltitle = Refs.
| [[Air New Zealand]] | [[Auckland Airport|Auckland]]|
| [[Alliance Airlines]] | [[Ballera Airport|Ballera]],<ref name="alliance1">{{cite news| url=http://australianaviation.com.au/2015/03/avalon-confident-of-a-future-even-if-jetstar-flights-end/ | work=Australian Aviation | title=Alliance wins new Santos contract}}</ref><ref name="alliance2">{{cite news| url=http://www.adelaidenow.com.au/business/cobham-loses-cooper-basin-fifo-contract/news-story/56c6914d64f5675fdea801f12b78941f | work=The Advertiser | title=Cobham loses Cooper Basin FIFO contract}}</ref> [[Moomba Airport|Moomba]],<ref name="alliance1"/><ref name="alliance2"/> [[Olympic Dam Airport|Olympic Dam]] <ref>{{cite news| url=http://www.ch-aviation.com/portal/news/42315-australias-alliance-air-ends-qantaslink-tie-up | work=ch-aviation | title=Australia's Alliance Air ends QantasLink tie-up}}</ref> <br/> '''Mining Charter''': [[Coober Pedy Airport|Coober Pedy]], [[Port Augusta Airport|Port Augusta]], [[Prominent Hill Airport|Prominent Hill Mine]]|
| [[Cathay Pacific]] | [[Hong Kong International Airport|Hong Kong]]|
| [[China Southern Airlines]] | [[Guangzhou Baiyun International Airport|Guangzhou]]<ref>http://www.traveldailymedia.com/239744/china-southern-to-start-flying-to-adelaide/</ref>|
| [[Emirates (airline)|Emirates]] | [[Dubai International Airport|Dubai–International]]|
| [[Fiji Airways]] | [[Nadi International Airport|Nadi]] (begins 30 June 2017)<ref>{{cite web |url=https://www.fijiairways.com/about-fiji-airways/media-centre/bula-adelaide/ |title=Bula Adelaide! Fiji Airways to operate flights out of its newest Australian port, Adelaide Airport. |date=21 December 2016 |publisher=[[Fiji Airways]] |accessdate=21 December 2016}}</ref>|
| [[Jetstar Airways]] | [[Avalon Airport|Avalon]],<ref>{{cite news| url=http://www.geelongadvertiser.com.au/news/geelong/jetstar-includes-hobart-adelaide-on-avalon-flights-radar/news-story/15e68954f7385e418aa518665580c62c | work=Geelong Advertiser | title=Jetstar includes Hobart, Adelaide on Avalon flights radar}}</ref> [[Brisbane Airport|Brisbane]], [[Cairns Airport|Cairns]], [[Darwin International Airport|Darwin]], [[Ngurah Rai International Airport|Denpasar]], [[Gold Coast Airport|Gold Coast]], [[Melbourne Airport|Melbourne]], [[Perth Airport|Perth]],  [[Sunshine Coast Airport|Sunshine Coast]],<ref>http://australianaviation.com.au/2016/05/jetstar-announces-sunshine-coast-adelaide-flights/comment-page-1/#comment-41387</ref> [[Sydney Airport|Sydney]]|
| [[Malaysia Airlines]] | [[Kuala Lumpur International Airport|Kuala Lumpur–International]]|
| [[Pel-Air]] | '''Mining Charter''': [[Jacinth Ambrosia Airport|Jacinth-Ambrosia Mine]]|
| [[Qantas]] | [[Alice Springs Airport|Alice Springs]], [[Brisbane Airport|Brisbane]], [[Canberra International Airport|Canberra]], [[Darwin International Airport|Darwin]], [[Melbourne Airport|Melbourne]], [[Perth Airport|Perth]], [[Sydney Airport|Sydney]]|
| [[QantasLink]]<br>operated by [[Cobham Aviation Services Australia]] | [[Brisbane Airport|Brisbane]],<ref>{{cite news| url=http://australianaviation.com.au/2016/07/qantas-expands-717-domestic-network/ | work=Australian Aviation | title=Qantas expands 717 domestic network}}</ref> [[Sydney Airport|Sydney]], [[Perth Airport|Perth]]|
| [[QantasLink]]<br>operated by [[Eastern Australia Airlines]] | [[Port Lincoln Airport|Port Lincoln]], [[Whyalla Airport|Whyalla]]|
| [[Qatar Airways]]| [[Hamad International Airport|Doha]]<ref>{{cite news| url=http://www.qatarairways.com/au/en/press-release.page?pr_id=pressrelease_131015_adelaide | work=Qatar Airways | title=QATAR AIRWAYS ANNOUNCES THE LAUNCH OF ANOTHER EXCITING AUSTRALIAN DESTINATION – ADELAIDE}}</ref>|
| [[Regional Express Airlines]] | [[Broken Hill Airport|Broken Hill]], [[Ceduna Airport|Ceduna]], [[Coober Pedy Airport|Coober Pedy]], [[Kingscote Airport|Kingscote]], [[Mildura Airport|Mildura]], [[Mount Gambier Airport|Mount Gambier]], [[Port Lincoln Airport|Port Lincoln]], [[Whyalla Airport|Whyalla]]|
| [[Rossair (Australia)|Rossair]] | '''Mining Charter:''' [[Ballera Airport|Ballera]], [[Challenger Mine|Challenger]], [[Moomba Airport|Moomba]]|
| [[Sharp Airlines]] | [[Port Augusta Airport|Port Augusta]] <br> '''Mining Charter:''' [[Beverley Uranium Mine]], [[Honeymoon Uranium Mine]], [[Leigh Creek Airport|Leigh Creek]], [[Moomba Airport|Moomba]], [[Prominent Hill Airport|Prominent Hill Mine]]|
| [[Singapore Airlines]] | [[Singapore Changi Airport|Singapore]]|
| [[Tigerair Australia]] | [[Brisbane Airport|Brisbane]],  [[Melbourne Airport|Melbourne]], [[Sydney Airport|Sydney]]|
| [[Virgin Australia]] | [[Alice Springs Airport|Alice Springs]],<ref>{{cite news| url=http://australianaviation.com.au/2014/11/virgin-to-fly-adelaide-alice-springs-from-march-2015/ | work=Australian Aviation | title=Virgin to fly Adelaide-Alice Springs from March 2015}}</ref> [[Brisbane Airport|Brisbane]], [[Canberra International Airport|Canberra]], [[Gold Coast Airport|Gold Coast]], [[Melbourne Airport|Melbourne]], [[Perth Airport|Perth]], [[Sydney Airport|Sydney]]|
}}

===Cargo===
{{Airport-dest-list
| [[Atlas Air]]<br>operated by [[Emirates Sky Cargo]] | [[Dubai International Airport|Dubai–International]]
| [[Australian air Express]]<br>operated by [[Cobham Aviation Services|Cobham]] | [[Melbourne Airport|Melbourne]], [[Sydney Airport|Sydney]]
| [[MASkargo]] | [[Kuala Lumpur International Airport|Kuala Lumpur–International]], [[Sydney Airport|Sydney]]
| [[Qantas Freight]] | [[Sydney Airport|Sydney]], [[Singapore Changi Airport|Singapore]]
| [[Singapore Airlines Cargo]] | [[Singapore Changi Airport|Singapore]]
| [[Toll Priority]]<br>operated by [[Pel-Air]] and [[Toll Aviation]] | [[Melbourne Airport|Melbourne]], [[Perth Airport|Perth]], [[Sydney Airport|Sydney]], [[Canberra International Airport|Canberra]]}}

==Traffic and statistics==

===Domestic===
{| class="wikitable sortable" width= align=
|+ Busiest domestic/regional routes out of Adelaide Airport
|- align="center" style="background:Gainsboro;"
|rowspan="2" |
|rowspan="2" | '''Airport'''
|colspan="4" | '''Passengers'''
|- align="center" style="background:Gainsboro;"
|<small>'''Year Ending'''</small><br/>'''2016'''<ref name="DAA">{{cite web|url=https://bitre.gov.au/publications/ongoing/files/domestic_airline_activity_2016a.pdf|title=Domestic aviation activity - Annual 2016|last=|first=|date=March 2016|website=|publisher=[[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics]] (BITRE)|archive-url=|archive-date=|dead-url=|accessdate=27 March 2016}} Refers to "Regular Public Transport (RPT) operations only"</ref>
|align="center" | '''<small>% Change</small>'''
|<small>'''July'''</small><br/>'''2016'''<ref name="DAA2">{{cite web | url = http://bitre.gov.au/publications/ongoing/files/Domestic_aviation_%20Jul_2016.pdf | title = Domestic aviation activity - July 2016 | publisher = [[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics]] (BITRE) | date = July 2016 | accessdate = 30 September 2016}} Refers to "Regular Public Transport (RPT) operations only"</ref>
|align="center" | '''<small>% Change</small>'''
|-
|align="center" | '''1'''||{{Sort|04|{{flagicon|Victoria}} [[Melbourne Airport|Melbourne]]}}||align="right"|2,393,636|| align="center" |{{increase}} 3.6|| align="right" |215,400||align="center"|{{increase}} {{Sort|01|6.4}}
|-
|align="center" | '''2'''||{{Sort|07|{{flagicon|New South Wales}} [[Sydney Airport|Sydney]]}}||align="right"|1,871,990|| align="center" |{{increase}} 2.2|| align="right" |153,700||align="center"|{{decrease}} {{Sort|03|1.7}}
|-
|align="center" | '''3'''||{{Sort|01|{{flagicon|Queensland}} [[Brisbane Airport|Brisbane]]}}||align="right"|830,335|| align="center" |{{increase}} 4.7|| align="right" |76,200||align="center"|{{increase}} {{Sort|02|7.2}}
|-
|align="center" | '''4'''||{{Sort|05|{{flagicon|Western Australia}} [[Perth Airport|Perth]]}}||align="right"|617,103|| align="center" |{{increase}} 1.0|| align="right" |50,400||align="center"|{{decrease}} {{Sort|04|5.1}}
|-
|'''5'''||{{Sort|03|{{flagicon|Queensland}} [[Gold Coast Airport|Gold Coast]]}}||align="right"|221,536|| align="center" |{{increase}} 1.0|| align="right" |20,200||align="center"|{{increase}} {{Sort|06|1.2}}
|-
|'''6'''||{{Sort|06|{{flagicon|South Australia}} [[Port Lincoln Airport|Port Lincoln]]}}||align="right"|178,895|| align="center" |{{decrease}} 3.7|| align="right" |14,000||align="center"|{{decrease}} {{Sort|07|4.4}}
|-
|'''7'''||{{Sort|02|{{flagicon|Australian Capital Territory}} [[Canberra International Airport|Canberra]]}}||align="right"|174,046|| align="center" |{{increase}} 2.4 || align="right" |13,200|| align="center" |{{decrease}} {{Sort|05|6.5}}
|-
|'''8'''||{{Sort|02|{{flagicon|Northern Territory}} [[Alice Springs Airport|Alice Springs]]{{Ref|1|1}}}}||align="right"|120,714|| align="center" |''n/a''
|align="right"|11,700||align="center"|{{decrease}} {{Sort|08|6.1}}
|}

<small>
;Notes:
*{{note|1|1}} Alice Springs only included from April 2015.
</small>

===International===
{| class="wikitable sortable" width= align=
|+ Busiest International routes out of Adelaide Airport
|- align="center" style="background:Gainsboro;"
|rowspan="2" |
|rowspan="2" | '''Airport'''
|colspan="4" | '''Passengers'''
|- align="center" style="background:Gainsboro;"
|<small>'''Year Ending'''</small><br/>'''2015/16'''<ref name="DAA3">{{cite web | url = http://bitre.gov.au/publications/ongoing/files/International_airline_activity_CityPairs_2009toCurrent_1606.xls | title = City pairs data—passengers, freight and mail—2009 to current  | publisher = [[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics]] (BITRE) | date = September 2016 | accessdate = 30 September 2016}} Refers to "Regular Public Transport (RPT) operations only"</ref>'''
|align="center" | '''<small>% Change</small>
|'''June'''<br/>'''2016'''<ref name="DAA4">{{cite web|url=http://bitre.gov.au/publications/ongoing/files/International_airline_activity_1606.pdf|title=International airline activity - June 2016|date=July 2016|publisher=[[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics]] (BITRE)|accessdate=30 September 2016}} Refers to "Regular Public Transport (RPT) operations only"</ref>
|align="center" | '''<small>% Change</small>'''
|-
|align="center" | '''1'''||{{Sort|04|{{flagicon|United Arab Emirates}} [[Dubai International Airport|Dubai]]}}||align="right"|202,502||align="center"|{{decrease}} {{Sort|01|3.1}}||align="right"|14,451||align="center"|{{decrease}} {{Sort|05|21.8}}
|-
|align="center" | '''2'''||{{Sort|07|{{flagicon|Singapore}} [[Singapore Changi Airport|Singapore]]}}||align="right"|199,632||align="center"|{{decrease}} {{Sort|03|2.9}}||align="right"|14,796||align="center"|{{increase}} {{Sort|04|3.3}}
|-
|align="center" | '''3'''||{{Sort|05|{{flagicon|Indonesia}} [[Ngurah Rai International Airport|Denpasar]] ''(Bali)''}}||align="right"|160,202||align="center"|{{increase}} {{Sort|04|6.6}}||align="right"|16,169|| align="center" |{{increase}} {{Sort|01|10.3}}
|-
|align="center" | '''4'''||{{Sort|03|{{flagicon|Hong Kong}} [[Hong Kong International Airport|Hong Kong]]}}||align="right"|104,696||align="center"|{{increase}} {{Sort|06|16.0}}||align="right"|8,124||align="center"|{{increase}} {{Sort|03|9.9}}
|-
|align="center" | '''5'''||{{Sort|01|{{flagicon|Malaysia}} [[Kuala Lumpur International Airport|Kuala Lumpur–International]]}}||align="right"|98,295||align="center"|{{decrease}} {{Sort|02|50.9}}||align="right"|6,202||align="center"|{{decrease}} {{Sort|06|45.8}}
|-
|align="center" | '''6'''||{{Sort|06|{{flagicon|New Zealand}} [[Auckland Airport|Auckland]]}}||align="right"|76,243||align="center"|{{decrease}} {{Sort|07|2.5}}||align="right"|4,258||align="center"|{{increase}} {{Sort|02|0.1}}
|-
|align="center" | '''7'''||{{Sort|08|{{flagicon|Qatar}} [[Hamad International Airport|Doha]]}}||align="right"|15,632||align="center"|''new''||align="right"|9,079||align="center"|''new''
|}

===Annual Passengers===
{| class="wikitable sortable" width= align=
|+ '''Annual passenger statistics
! Year || Passenger movements
|-
! 2001–02
| 4,180,000
|-
! 2002–03
| 4,358,000
|-
! 2003–04
| 4,897,000
|-
! 2004–05
| 5,371,000
|-
! 2005–06
| 5,776,000
|-
! 2006–07
| 6,192,000
|-
! 2007–08
| 6,635,000
|-
! 2008–09
| 6,799,000
|-
! 2009–10
| 7,030,000
|-
! 2010–11
|  7,297,000
|-
! 2011-12
| 6,968,000
|-
! 2012-13
| 7,300,000
|-
! 2013-14
| 7,696,000
|-
!2014-15
|7,670,000
|-
!2015-16
|7,777,747
|- style="background:lightgrey;"
! 2020-21
| 9,856,000
|- style="background:lightgrey;"
! 2025-26
| 11,552,000
|- style="background:lightgrey;"
! 2030–31
| 13,537,000
|}

===Cargo===
{| class="wikitable sortable" width= align=
|+ Busiest international freight routes into and out of Adelaide Airport<br>(YE June 2011)<ref name="IAA">{{cite web | url = http://www.bitre.gov.au/publications/ongoing/files/International_airline_activity_CY11.pdf | title = Australian International Airline Activity 2011 | publisher = [[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics]] (BITRE) | date = June 2012 | accessdate = 10 July 2012}} Refers to "Regular Public Transport (RPT) operations only"</ref>
|- style="background:lightgrey;"
! Rank|| Airport || Tonnes || % Change
|-
|1|| {{Sort|04|Singapore, [[Singapore Changi Airport|Singapore]]}}||align="right"|10,995.7||{{decrease}}{{Sort|03|10.8}}
|-
|2|| {{Sort|21|Hong Kong, [[Hong Kong International Airport|Hong Kong]]}}||align="right"|3,413.2||{{decrease}}{{Sort|02|8.8}}
|-
|3|| {{Sort|03|Malaysia, [[Kuala Lumpur International Airport|Kuala Lumpur–International]]}}||align="right"|2,984.4||{{increase}}{{Sort|01|1.9}}
|-
|4|| {{Sort|01|New Zealand, [[Auckland Airport|Auckland]]}}||align="right"|449.4||{{decrease}}{{sort|04|11.8}}
|}

==Ground transport==
[[Adelaide Metro]] operates frequent [[Buses in Adelaide#JetBus services|JetBus]] buses connecting the airport to the [[Adelaide city centre|Central Business District]] and [[Glenelg, South Australia|Glenelg]]. Routes J1, J1X and J3 operate to the City every 15mins. Route J1 also operates to Harbour Town Shopping Centre and Routes J1 and J3 continue to Glenelg. Routes J7 and J8 operate to [[West Lakes, South Australia|West Lakes]] and [[Marion, South Australia|Marion]].<ref>{{cite web|title=Adelaide Metro - Stop 17327|url=http://www.adelaidemetro.com.au/stops/view/17327|publisher=Adelaide Metro|accessdate=1 August 2014}}</ref> The AAL's latest airport master plan proposes a light rail service. Historically airlines provided connecting buses to the Central Business District, after which a private bus service provided a service until 2013.

==See also==
{{portal|Aviation|South Australia}}
* [[List of airports in South Australia]]
* [[Transport in Adelaide]]
* [[Transportation in Australia]]

==References==
{{Reflist|30em}}

==External links==
{{Commonscat-inline}}
* [http://www.aal.com.au/ Adelaide Airport Limited]
* [http://www.adelaideairport.com.au/travellers-guide/webcam/ Adelaide Airport webcam, updated every 60 seconds. The camera is looking north east from Gate 26]
* [https://www.youtube.com/watch?v=9km-wMzqIow Video of Qantas A380's first visit to Adelaide Airport]

{{City of West Torrens suburbs}}
{{Airports in Australia}}
{{Airports in South Australia}}

[[Category:1955 establishments in Australia]]
[[Category:Airports in South Australia]]
[[Category:Transport in Adelaide]]
[[Category:Airports established in 1955]]