{{EngvarB|date=May 2014}}
{{good article}}
{{Use dmy dates|date=April 2011}}
{{Infobox banknote
| country             = [[Eurozone]] and [[Institutions of the European Union|Institutions]]
| denomination        = Twenty euro
| value               = 20
| unit                = euro
| colour              = Blue
| width_mm            = 133
| height_mm           = 72
| security_features   = First series: [[Security hologram|hologram]] stripe with [[perforation]]s, reflective glossy stripe, [[EURion constellation]], [[watermark]]s, [[microprinting]], [[Invisible ink#Inks visible under ultraviolet light|ultraviolet ink]], raised printing, [[security thread]], matted surface, see-through number, [[barcode]]s and [[serial number]]<ref>{{cite web|url=http://www.ecb.int/euro/html/eurocoins.en.html |title=ECB: Security Features|publisher=European Central Bank|work=European Central Bank|year=2008|accessdate=13 October 2011}}</ref><br />
Europa series: portrait watermark, portrait hologram, portrait window, emerald number<ref name="Security features Europa">{{cite web|url=http://www.ecb.europa.eu/euro/banknotes/security/html/index.en.html|title=ECB: Security features|accessdate=9 July 2015|work=European Central Bank|publisher=ecb.int}}</ref>
| paper_type          = 100% pure cotton [[fibre]]<ref>{{cite web|url=http://www.ecb.int/euro/banknotes/security/feel/html/index.en.html |title=ECB: Feel|accessdate=9 October 2011|work=European Central Bank|publisher=European Central Bank|year=2002}}</ref>
| years_of_printing   = 1999 - 2013 (1st series)<ref name="Introduction" /><br>Since 2014 (Europa series)<ref name="Introduction"/>
| obverse             = The Europa series 20 € obverse side.jpg
| obverse_design      = Window in [[Gothic architecture]]<ref name="Banknotes index"/>
| obverse_designer    = [[Robert Kalina]]<ref name="Design">{{cite web|url=http://www.ecb.int/euro/banknotes/html/design.en.html |title=ECB: Banknotes design|publisher=ECB|work=ECB|accessdate=13 October 2011|date=February 1996}}</ref>
| obverse_design_date = 24 February 2015<ref name="Design"/> 
| reverse             =The Europa series 20 €  reverse side.jpg
| reverse_design      = Bridge in [[Gothic architecture]] and map of Europe<ref name="Banknotes index"/>
| reverse_designer    = Robert Kalina<ref name="Design"/>
| reverse_design_date = 24 February 2015<ref name="Design"/>
}}

The '''twenty [[euro]] note (€20)''' is the third-lowest value [[euro banknote]] and has been used since the introduction of the euro (in its cash form) in 2002.<ref>{{cite news|url=http://pqasb.pqarchiver.com/smgpubs/access/97637858.html?dids=97637858:97637858&FMT=ABS&FMTS=ABS:FT&type=current&date=Jan+01%2C+2002&author=Alf+Young%3B+on+Tuesday&pub=The+Herald&desc=Witnessing+a+milestone+in+European+history&pqatl=google |title=Witnessing a milestone in European history|publisher=Back Issue|work=The Herald|date=1 January 2002|accessdate=23 October 2011}}</ref> 
The note is used in the 23 countries which have it as their sole currency (with 22 legally adopting it); with a population of about 332&nbsp;million.<ref>
* {{cite web
 |url=http://www.eurocoins.co.uk/andorra.html 
 |title=Andorran Euro Coins 
 |year=2003 
 |accessdate=15 October 2011 
 |publisher=Eurocoins.co.uk 
 |work=Eurocoins.co.uk 
}}
* {{cite web
 |url=http://www.unmikonline.org/regulations/admdirect/1999/089%20Final%20%20ADE%201999-02.htm 
 |title=By UNMIK administration direction 1999/2 
 |publisher=Unmikonline.org 
 |date=4 October 1999 
 |accessdate=30 May 2010 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110607234444/http://www.unmikonline.org/regulations/admdirect/1999/089%20Final%20%20ADE%201999-02.htm 
 |archivedate=7 June 2011 
 |df=dmy 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2002:142:0059:0073:EN:PDF 
 |title=By monetary agreement between France (acting for the EC) and Monaco 
 |date=31 May 2002 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:C:2001:209:0001:0004:EN:PDF 
 |title=By monetary agreement between Italy (acting for the EC) and San Marino 
 |date=27 July 2001 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:C:2001:299:0001:0004:EN:PDF 
 |title=By monetary agreement between Italy (acting for the EC) and Vatican City 
 |date=25 October 2001 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://www.ecb.europa.eu/euro/intro/html/map.en.html 
 |title=ECB: Map of euro area 1999 – 2011 
 |date=1 January 2011 
 |accessdate=27 October 2011 
 |work=ECB 
 |publisher=ecb.int 
}}
* {{cite web
 |url=http://epp.eurostat.ec.europa.eu/tgm/table.do?tab=table&language=en&pcode=tps00001&tableSelection=1&footnotes=yes&labeling=labels&plugin=1 
 |title=Total population as of 1 January 
 |publisher=Epp.eurostat.ec.europa.eu 
 |date=2011-03-11 
 |accessdate=2011-07-17 
 |archiveurl=https://web.archive.org/web/20110720161751/http://epp.eurostat.ec.europa.eu/tgm/table.do?tab=table&language=en&pcode=tps00001&tableSelection=1&footnotes=yes&labeling=labels&plugin=1 
 |archivedate=20 July 2011 
 |deadurl=no 
 |df=dmy 
}}
</ref>

It is the third-smallest note, measuring 133 x 72&nbsp;mm with a blue colour scheme.<ref name="Banknotes index">{{cite web|url=http://www.ecb.int/euro/banknotes/html/index.en.html |title=ECB: Banknotes|publisher=European Central Bank|work=European Central Bank|year=2002|accessdate=13 October 2011}}</ref> The twenty euro banknotes depict bridges and arches/doorways in [[Gothic architecture]] (between the 13th and 14th century CE).

The twenty euro note contains several complex security features such as watermarks, invisible ink, holograms and microprinting that document its authenticity. In October 2011, there were approximately 2,755,346,800 twenty euro banknotes in circulation around the [[eurozone]].

The full design of the Europa series 20 euro banknote was revealed on 24 February 2015<ref name="ecb.europa.eu">[http://www.ecb.europa.eu/press/pr/date/2015/html/pr150224.en.html PRESS RELEASE 24 February 2015 - New €20 banknote unveiled in Frankfurt today]</ref><ref>http://www.ecb.europa.eu/press/pr/date/2014/html/pr141219.en.html</ref> and  launched on 25 November 2015.<ref name="ecb.europa.eu"/>

== History ==
{{main article|History of the euro}}
The euro was founded on 1 January 1999, when it became the currency of over 300 million people in Europe.<ref name="Introduction">{{cite web|url=http://www.ecb.int/euro/intro/html/index.en.html |title=ECB: Introduction|publisher=ECB|work=ECB| accessdate= 21 October 2011 <!--Added by DASHBot-->}}</ref> For the first three years of its existence it was an invisible currency, only used in accountancy. Euro cash was not introduced until 1 January 2002, when it replaced the national banknotes and coins of the countries in eurozone 12, such as the [[Belgian franc]] and the [[Greek drachma]].<ref name="Introduction" />

[[Slovenia]] joined the [[Eurozone]] in 2007,<ref>{{cite web|url=http://ec.europa.eu/economy_finance/focuson/focuson9120_en.htm |title=Slovenia joins the euro area - European Commission |publisher=European Commission |date=16 June 2011 |accessdate=6 August 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130911232930/http://ec.europa.eu:80/economy_finance/focuson/focuson9120_en.htm |archivedate=11 September 2013 |df=dmy }}</ref> [[Cyprus]] and [[Malta]] in 2008,<ref>{{cite news | url=http://news.bbc.co.uk/2/hi/europe/7165622.stm | title=Cyprus and Malta adopt the euro - BBC NEWS | work=BBC News | date=1 January 2008 | agency=British Broadcasting Corporation | accessdate=6 August 2013}}</ref> [[Slovakia]] in 2009,<ref>{{cite news | url=http://www.businessweek.com/stories/2008-12-31/slovakia-joins-decade-old-euro-zonebusinessweek-business-news-stock-market-and-financial-advice | title=Slovakia Joins Decade-Old Euro Zone - Businessweek | work=Bloomberg Businessweek | date=31 December 2008 | agency=Bloomberg | accessdate=6 August 2013 | author=Kubosova, Lucia}}</ref> [[Estonia]] in 2011,<ref>{{cite news | url=http://www.rte.ie/news/2010/0713/133287-euro/ | title=Estonia to join euro zone in 2011 | work=RTÉ News | date=13 July 2010 | agency=Radió Teilifís Éireann | accessdate=6 August 2013}}</ref> [[Latvia]] in 2014,<ref name="Latvia Eurozone">{{cite web | url=https://www.wsj.com/article/SB10001424127887324507404578595440293862344.html | title=Latvia Gets Green Light to Join Euro Zone -WSJ.com | publisher=Wall Street Journal | work=Wall Street Journal | date=9 July 2013 | accessdate=31 July 2013 | author=Van Tartwijk, Maarten | author2=Kaza, Juris}}</ref> and [[Lithuania]] in 2015.
[[File:EUR 20 obverse (2002 issue).jpg|thumb|20 € (2002 issue) obverse side.]]
[[File: EUR 20 reverse (2002 issue).jpg|thumb|20 € (2002 issue) reverse side.]]

=== The changeover period ===
The changeover period during which the former currencies' notes and coins were exchanged for those of the euro lasted about two months, going from 1 January 2002 until 28 February 2002. The official date on which the national currencies ceased to be legal tender varied from member state to member state.<ref name="Introduction"/> The earliest date was in Germany, where the mark officially ceased to be legal tender on 31 December 2001, though the exchange period lasted for two months more. Even after the old currencies ceased to be legal tender, they continued to be accepted by national central banks for periods ranging from ten years to forever.<ref name="Introduction" /><ref>{{cite web | url=http://www.centralbank.ie/about-us/Documents/PRESSKIT%20-%2010th%20anniversary%20of%20the%20euro.pdf | title=Press kit - tenth anniversary of the euro banknotes and coins | publisher=Central Bank of Ireland | work=ECB | year=2011 | accessdate=21 August 2012}}</ref>

=== Changes ===
Notes printed before November 2003 bear the signature of the first [[President of the European Central Bank|president]] of the [[European Central Bank]], [[Wim Duisenberg]], who was replaced on 1 November 2003 by [[Jean-Claude Trichet]], whose signature appears on issues from November 2003 to March 2012. Notes issued after March 2012 bear the signature of the third president of the European Central Bank, incumbent [[Mario Draghi]].<ref name="Banknotes index" />

Until now there has been only one complete series of euro notes; however a new series, similar to the current one, is being released.<ref name="Euro series 2">{{cite web | url=http://www.ecb.int/pub/pdf/other/p041-048_mb200508en.pdf | title=ECB Monthly bulletin- August 2005 - THE EURO BANKNOTES: DEVELOPMENTS AND FUTURE CHALLENGES | publisher=ecb.int | work=ECB | date=August 2005 | accessdate=21 August 2012 | page=43}}</ref> The European Central Bank will, in due time, announce when banknotes from the first series lose legal tender status.<ref name="Euro series 2" />

As of June 2012, current issues do not reflect the expansion of the European Union to 27 member states as [[Cyprus]] is not depicted on current notes as the map does not extend far enough east and [[Malta]] is also missing as it does not meet the current series' minimum size for depiction.<ref name="ECBdesign">{{cite web| url = http://www.ecb.europa.eu/euro/banknotes/html/index.en.html#main| title = The Euro: Banknotes: Design elements| accessdate = 2009-07-05| author = European Central Bank| authorlink = European Central Bank| quote = The banknotes show a geographical representation of Europe. It excludes islands of less than 400 square kilometres because high-volume offset printing does not permit the accurate reproduction of small design elements.}}</ref> Since the European Central Bank plans to redesign the notes every seven or eight years after each issue, a second series of banknotes is already in preparation. New production and anti-counterfeiting techniques will be employed on the new notes, but the design will be of the same theme and colours identical of the current series; bridges and arches. However, they would still be recognisable as a new series.<ref>[http://www.dnb.nl/dnb/home?lang=en&id=tcm:47-150696-64 The life cycle of a banknote] {{webarchive |url=https://web.archive.org/web/20070926234241/http://www.dnb.nl/dnb/home?lang=en&id=tcm:47-150696-64 |date=26 September 2007 }}, [[De Nederlandsche Bank]]. Accessed 2007-08-17.</ref>

==Design==
{{multiple image
<!-- Essential parameters -->
| align     = right
| direction = vertical
| width     = 150
| header    = 20 euro banknote under fluorescent light (UV-A)

<!-- Image 1 -->
| image1   = 020euro-uv.JPG
| width1   = 135
| alt1     = 20 euro note under UV light (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = 020euro-uv2.JPG
| width2   = 135
| alt2     = 20 euro note under UV light (Reverse)
| caption2 = Reverse
}}
The twenty euro note is the third smallest euro note at {{convert|133|mm}} × {{convert|72|mm}} with a blue colour scheme.<ref name="Banknotes index"/> All bank notes depict bridges and arches/doorways in a different historical European style; the twenty euro note shows the [[Gothic architecture|gothic era]] (between the 13th and 14th century CE).<ref name="Security">{{cite web|url=http://www.ecb.int/euro/banknotes/security/html/index.en.html |title=ECB: Security Features|publisher=ECB|work=ECB}}</ref> Although [[Robert Kalina]]'s original designs were intended to show real monuments, for political reasons the bridge and art are merely hypothetical examples of the architectural era.<ref>{{cite news| url=http://news.bbc.co.uk/hi/english/static/slideshow/money_talks/slide2.stm |title=Money talks - the new Euro cash|date=December 1996|accessdate=13 October 2011|work=BBC News|publisher=BBC News}}</ref>

Like all euro notes, it contains the denomination, the [[Flag of Europe|EU flag]], the signature of the president of the [[European Central Bank|ECB]]<ref name="Banknotes index"/> and the initials of said bank in different [[Languages of the European Union|EU languages]], a depiction of EU territories overseas, the stars from the EU flag and thirteen security features as listed below.<ref name="Banknotes index"/>

The ECB released a game on 5 February 2015 to discover some of the new security features embedded in the new €20 note.<ref>http://www.new-euro-banknotes.eu/</ref> The most significant new anti-counterfeit measure is a transparent window, containing a hologram which shows a portrait of Europa and the number 20.<ref>http://game-20.new-euro-banknotes.eu/?slang=EN</ref> The Europa series design of the 20 euro note was officially revealed on 24 February 2015.<ref>[https://www.ecb.europa.eu/press/pr/date/2015/html/pr150224.en.html New €20 banknote unveiled in Frankfurt today]</ref>

=== Security features (First Series)===
[[File:Watermarks 20 Euro.jpg|thumb|The watermark on the 20 euro note]]
As a lower value note, the security features of the twenty euro note are not as high as the other denominations; however, it is protected by: 
* A [[hologram]],<ref name="Interactive security features">{{cite web|url=http://www.ecb.int/euro/html/security_features.en.html |archive-url=https://web.archive.org/web/20090409044753/http://www.ecb.int:80/euro/html/security_features.en.html |dead-url=yes |archive-date=9 April 2009 |title=ECB: Security Features|accessdate=22 October 2011|work=European Central Bank|publisher=ecb.int|year=2002 |format=[[Adobe Flash]]}}</ref> tilt the note and one should see the hologram image change between the value and a window or doorway, but in the background, one should see rainbow-coloured concentric circles of micro-letters moving from the centre to the edges of the patch.<ref name="Titling">{{cite web|url=http://www.ecb.int/euro/banknotes/security/tilt/html/index.en.html |title=ECB:Tilt|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* The [[EURion constellation]].
* A special printing processes give the euro notes their unique feel.<ref name="Interactive security features" />
* A glossy stripe,<ref name="Interactive security features" /> tilt the note and a glossy stripe showing the value numeral and the euro symbol will appear.
* [[Watermark]]s,<ref name="Interactive security features" /> it appears when the banknote is against the light.
* Raised printing,<ref name="Interactive security features" /> special methods of printing makes the ink feel raised or thicker in the main image, the lettering and the value numerals on the front of the banknotes. To feel the raised print, run your finger over it or scratch it gently with your fingernail.<ref name="Feel">{{cite web|url=http://www.ecb.int/euro/banknotes/security/feel/html/index.en.html |title=ECB: Feel|work=ECB|publisher=ecb.int|date=1 January 2011|accessdate=22 October 2011}}</ref>
* [[Invisible ink#Inks visible under ultraviolet light|Ultraviolet ink]],<ref name="Interactive security features" /> Under ultraviolet light, the paper itself should not glow, fibres embedded in the paper should appear, and should be coloured red, blue and green, the European Union flag looks green and has orange stars, the ECB President signature turns green, the large stars and small circles on the front glow and the European map, a bridge and the value numeral on the back appear in yellow.<ref name="Additional Features">{{cite web|url=http://www.ecb.int/euro/banknotes/security/additional/html/index.en.html |title=ECB: Additional features|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* Microprinting,<ref name="Interactive security features" /> On numerous areas of the banknotes you can see microprinting, for example, inside the "ΕΥΡΩ" (EURO in Greek characters) on the front. You will need a magnifying glass to see it. The tiny text is sharp, and not blurred.<ref name="Additional Features" />
* A security thread,<ref name="Interactive security features" /> The security thread is embedded in the banknote paper. Hold the banknote against the light - the thread will appear as a dark stripe. The word "EURO" and the value can be seen in tiny letters on the stripe.<ref name="Look">{{cite web|url=http://www.ecb.int/euro/banknotes/security/look/html/index.en.html |title=ECB: Look|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* Perforations,<ref name="Interactive security features" /> Hold the banknote against the light. You should see perforations in the hologram which will form the € symbol. You should also see small numbers showing the value.<ref name="Look" />
* A matted surface,<ref name="Interactive security features" /> the note paper is made out of pure cotton, which feels crisp and firm, but not limp or waxy.<ref name="Feel" />
* Barcodes.<ref name="Interactive security features" />
* A serial number.<ref name="Interactive security features" />

=== Security Features (Europa Series) ===
* '''Watermark''': When the note is held under a normal light source, a portrait of Europa and an electrotype denomination appear on either side.
* '''Portrait Window''': When the note is held against the light, the window in the hologram becomes transparent and reveals a portrait of Europa, which is visible on both sides of the note.
* '''Portrait Hologram''': When the note is tilted, the hologram - the silver-coloured stripe on the right of the note - reveals a portrait of Europa as well as the "€" symbol, the main image and the value of the banknote.
* '''Emerald Number''': When the note is tilted, the number "20" on the bottom left corner of the note displays an effect of the light that moves up and down. The number "20" also changes colour from emerald green to deep blue.
* '''Security Thread''': When the note is held to the light, the security thread appears as a dark line. The "€" symbol and the value of the note can be seen in tiny white lettering in the stripe.
* '''Microprinting''': Some areas of the banknote feature a series of tiny letters. The microprinting can be read with a magnifying glass. The letters are sharp, not blurred.

== Circulation ==

The European Central Bank is closely monitoring the circulation and stock of the euro coins and banknotes. It is a task of the Eurosystem to ensure an efficient and smooth supply of euro notes and to maintain their integrity throughout the euro area.<ref name="Circulation" />

On January 2017, there are {{formatnum:3415829019}} €20 banknotes in circulation around the Eurozone.<ref name="Circulation">{{cite web|url=http://www.ecb.int/stats/euro/circulation/html/index.en.html |title=ECB: Circulation|work=ECB|publisher=European Central Bank}}</ref> That is €{{formatnum:68316580380}} worth of €20 banknotes.

This is a net number, i.e. the number of banknotes issued by the Eurosystem central banks, without further distinction as to who is holding the currency issued, thus also including the stocks held by credit institutions.

Besides the date of the introduction of the first set to January 2002, the publication of figures is more significant through the maximum number of banknotes raised each year. The number is higher the end of the year.

The figures are as follows (March 14, 2017) :

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="40%"
|-align="center"
!scope="row"| Key date
| Banknotes 
| Amount
|-align="center"
!scope="row"| January 2002
| {{formatnum:1961761089}}
| {{formatnum:39235221780}}
|-align="center"
!scope="row"| December 2002
| {{formatnum:1974764476}}
| {{formatnum:39495289520}}
|-align="center"
!scope="row"| December 2003
| {{formatnum:2053751069}}
| {{formatnum:41075021380}}
|-align="center"
!scope="row"| December 2004
| {{formatnum:2079431718}}
| {{formatnum:41588634360}}
|-align="center"
!scope="row"| December 2005
| {{formatnum:2159677359}}
| {{formatnum:43193547180}}
|-align="center"
!scope="row"| December 2006
| {{formatnum:2336568793}}
| {{formatnum:46731375860}}
|-align="center"
!scope="row"| December 2007
| {{formatnum:2467676850}}
| {{formatnum:49353537000}}
|-align="center"
!scope="row"| December 2008
| {{formatnum:2617914839}}
| {{formatnum:52358296780}}
|-align="center"
!scope="row"| December 2009
| {{formatnum:2690208898}}
| {{formatnum:53804177960}}
|-align="center"
!scope="row"| December 2010
| {{formatnum:2751808438}}
| {{formatnum:55036168760}}
|-align="center"
!scope="row"| December 2011
| {{formatnum:2853452345}}
| {{formatnum:57069046900}}
|-align="center"
!scope="row"| December 2012
| {{formatnum:2988384283}}
| {{formatnum:59767685660}}
|-align="center"
!scope="row"| December 2013
| {{formatnum:3088833405}}
| {{formatnum:61776668100}}
|-align="center"
!scope="row"| December 2014
| {{formatnum:3233284025}}
| {{formatnum:64665680500}}
|}

On November 2015, a new 'Europe' series was issued. The first series of notes were issued in conjunction with those for a few weeks in the series 'Europe' until existing stocks are exhausted, then gradually withdrawn from circulation. Both series thus run parallel but the proportion tends inevitably to a sharp decrease in the first series.

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="80%"
|-align="center"
!scope="row"| Key date
| Banknotes 
| Amount
| Series '1' remainder
| Amount
| Proportion
|-align="center"
!scope="row"| December 2015
| {{formatnum:3439563088}}
| {{formatnum:68791261760}}
| {{formatnum:2814523557}}
| {{formatnum:56290471140}}
| 81,8%
|-align="center"
!scope="row"| December 2016
| {{formatnum:3590492061}}
| {{formatnum:71809841220}}
| {{formatnum:1336184040}}
| {{formatnum:26723680800}}
| 37,2 %
|}

The latest figures provided by the ECB are the following :

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="80%"
|-align="center"
!scope="row"| Key date
| Banknotes 
| Amount
| Series '1' remainder
| Amount
| Proportion
|-align="center"
!scope="row"| February 2017
| {{formatnum:3424751882}}
| {{formatnum:68495037640}}
| {{formatnum:1240775345}}
| {{formatnum:24815506900}}
| 36,2 %
|}

== Legal information ==
Legally, both the European Central Bank and the central banks of the [[eurozone]] countries have the right to issue the seven different euro banknotes. In practice, only the national central banks of the zone physically issue and withdraw euro banknotes. The European Central Bank does not have a cash office and is not involved in any cash operations.<ref name="Introduction" />

== Tracking ==
There are several communities of people at European level, most of which is [[EuroBillTracker]],<ref name="EuroBillTracker About">{{cite web|url=http://en.eurobilltracker.com/about/ |title=EuroBillTracker - About this site|date=1 January 2002|accessdate=21 October 2011|work=Philippe Girolami, Anssi Johansson, Marko Schilde|publisher=EuroBillTracker|archiveurl=http://www.webcitation.org/query?id=1372296306684953|archivedate=2013-06-27|deadurl=no}}</ref> that, as a hobby, it keeps track of the euro banknotes that pass through their hands, to keep track and know where they travel or have travelled.<ref name="EuroBillTracker About"/> The aim is to record as many notes as possible to know details about its spread, like from where and to where they travel in general, follow it up, like where a ticket has been seen in particular, and generate statistics and rankings, for example, in which countries there are more tickets.<ref name="EuroBillTracker About"/> EuroBillTracker has registered over 155 million notes as of May 2016,<ref name="EuroBilltracker Statistics">{{cite web |url=http://en.eurobilltracker.com/stats/ |title=EuroBillTracker - Statistics|date=1 January 2002|accessdate=21 October 2011|work=Philippe Girolami, Anssi Johansson, Marko Schilde |publisher=EuroBillTracker}}</ref> worth more than €2.897 billion.<ref name="EuroBilltracker Statistics"/>

== References ==
{{reflist|2}}

== External links ==
* {{Commonscat-inline|20 euro banknotes}}

{{Euro topics}}

[[Category:Euro banknotes]]
[[Category:Twenty-base-unit banknotes]]