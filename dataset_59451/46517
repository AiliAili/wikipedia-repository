{{Orphan|date=November 2016}}

'''Barbara Metselaar Berthold''' is a German photographer and filmmaker.

== Biography ==
Berthold studied from 1969 to 1971 at the [[Friedrich-Schiller-University Jena]] social psychology. In 1971 she began studying photography at the Academy of Visual Arts Leipzig, completing her degree in 1976 with a diploma. Then she went to Berlin, where she worked as a freelance photographer. In 1984 she married the Dutchman Kees Metselaar and traveled out of the GDR. In the years 1985 to 1989 she was a lecturer and artistic staff including work at the University of Arts in Berlin. Then she focused on the production of documentaries and video essays and worked from 1991 for various TV productions. In 1996 signed the International Festival for Documentary and Animated Film in Leipzig her film "We were so happy to have been heroes" with the Silver Dove. [1] 1998/99 was Barbara Merselaar Berthold and a lecturer in photography at the School of Film TV Babelsberg held. Their documentary projects have been supported by grants from the film enhance the country Mecklenburg-Vorpommern and cultural promotion in the Free State of Saxony. In 2010 she received the award for artistic photography of artists program of the Berlin Senate.
She lives in Berlin and the Uckermark.

== Exhibitions ==

* Fotowerkstatt Kreuzberg, Berlin 1985
* ''Stadtstand II'', [[Installation (Kunst)|Installation]] mit [[Hans-Hendrik Grimmling]], Lutz Friedel und Martin Steyer im [[Bethanien (Berlin)#K.C3.BCnstlerhaus Bethanien|Künstlerhaus Bethanien]], Berlin, 1988
* ''DDR-Frauen fotografieren'' , [[Haus am Lützowplatz]] Berlin und im [[Ludwig Galerie Schloss Oberhausen|Museum Ludwig]], Oberhausen 1990, mit Katalog: Gabriele Muschter, (Hrsg.): ''DDR Frauen fotografieren. Lexikon und Anthologie'', ex pose verlag, Berlin 1990
* ''Citylights'', Alter Wiehrebahnhof Freiburg 1991
* ''Changeant'', intermediales Projekt mit M.-A. Bahra, Anna Werkmeister und Heike Willingham, [[Waschhaus Potsdam]] 1994
* ''In den großen Städten'', [[Galerie argus Fotokunst]], Berlin 1996
* ''Cut/Kire/Schnitt'', Galerie Schwarzes Kloster Freiburg, 1998
* ''Frozen Margaritas'', fotografische Installation in der [[Prater (Berlin)#Galerie im Prater|Galerie am Prater]], Berlin 1999
* ''Deutsche Tänze'', [[Kunstmuseum Dieselkraftwerk Cottbus|Museum für zeitgenössische Kunst Cottbus]], und im Haus am Kleistpark, Berlin-Schöneberg 1999
* ''Positions-Attidudes-Actions'', Foto Biennale Rotterdam (Beteiligung), 2000
* ''Within and beyond the wall (Nine German photographers)'' Harbourfront Gallery Toronto, 2004
* ''Fifteen years after the fall of the wall'' (Beteiligung), Central Library Gallery [[San Antonio]], 2004
* ''Utopie und Wirklichkeit'', Forum für Fotografie Köln und [[Willy-Brandt-Haus]] Berlin (Beteiligung), 2004
* ''Behind Walls'', Noorderlicht Photofestival (Beteiligung), [[Fries Museum]], Leeuwarden, 2008
* ''Art of two Germanys'', [[Los Angeles County Museum of Art]] (Beteiligung), 2009
* ''Suche, Seele, Suche - zwischen zwei Welten'', Galerie argus Fotokunst, Berlin 2010
* ''Filetstücke - Vexierbilder Berlin Mitte'', [[Ephraim-Palais]], Berlin 2010
* ''4. Fotofestival'', [https://web.archive.org/web/20111205170900/http://www.fotofestival.info:80/de/vita/?id=18 Mannheim Ludwigshafen Heidelberg 2011]

== Works in public collections ==

Ihre Arbeiten sind unter anderen in folgenden Sammlungen enthalten:
* [[Los Angeles County Museum of Art]] (LACMA), Los Angeles
* [[Berlinische Galerie]], Berlin
* [[Märkisches Museum (Berlin)|Märkisches Museum]], Berlin
* [[Moritzburg (Halle)#Sammlung Fotografie|Fotografischen Sammlung]] des [[Stiftung Moritzburg#Stiftung Moritzburg .2F Kunstmuseum des Landes Sachsen-Anhalt|Kunstmuseum des Landes Sachsen-Anhalt]] auf der [[Moritzburg (Halle)|Moritzburg in Halle]]
* [[Kunstmuseum Dieselkraftwerk Cottbus|Museum für zeitgenössische Kunst Cottbus]]

== Films ==

* ''Staatsbeben'', Materialsammlung, (Regie, Kamera, Schnitt), 1989
* ''Sonst knallts - die Klappe'' und ''Das Haus'', künstlerische Videos, 1989/90
* ''Tell me the tales'' (Kamera), [[Deutscher Fernsehfunk]] 1991
* ''Seilfahrten ... relativ extrem'' (Regiekamera, Schnitt), [[ZDF]] [[Kleines Fernsehspiel]], 1993/94
* ''Wir wären so gerne Helden gewesen'' (Buch, Regie, Kamera, Schnitt) ZDF Kleines Fernsehspiel, 1995/96
* ''Frozen Margaritas'' (Buch, Regie, Kamera, Schnitt, Produktion), 1998/99
* ''Insel - Gespräche vor blühender Landschaft'' (Buch, Regie, Kamera, Schnitt), 2005
* ''Audienzen - Strategien der Selbstbehauptung'' (Buch, Regie, Kamera, Schnitt), mit [[Tina Bara]], 2006/07

== Publications ==

* ''Kratzen am Beton - 68er in der DDR?''. Verlag Vopelius, Jena 2008, ISBN 978-3939718185.
* ''Albatros. Vom Abheben'', [[Lukas Verlag]], Berlin 2010, ISBN 978-3-86732-094-8

== References ==
{{reflist}}
*http://www.argus-fotokunst.de/de/info/berthold.html

== External links ==
* {{DNB-Portal|Barbara+Metselaar+Berthold}}
* {{IMDb name|1257917}}
* [http://www.argus-fotokunst.de/de/info/berthold.html Biographie Barbara Metselaar Berthold (Galerie argus fotokunst)]
* [http://www.noorderlicht.com/eng/fest08/behindwalls/metselaar/index.html Noorderlicht Photofestival 2008]
* [http://www.berlinonline.de/berliner-zeitung/archiv/.bin/dump.fcgi/2010/1116/kunst/0005/index.html Bilder, die sie nicht mehr sehen wollte] – Rezension der Ausstellung im Ephraim-Palais von Irmgard Berner in der Berliner Zeitung, 16. November 2010

{{Authority control}}

{{DEFAULTSORT:Berthold, Barbara Metselaar}}
[[Category:1951 births]]
[[Category:Living people]]
[[Category:German photographers]]