<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=GR 912 & Sport 2000
 | image=File:Australian LightWing GR 912.jpg
 | caption=GR 912 model
}}{{Infobox Aircraft Type
 | type=[[Light-sport aircraft]]
 | national origin=[[Australia]]
 | manufacturer=[[Australian Lightwing]]
 | designer=
 | first flight=
 | introduced=1986
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= 
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Australian Lightwing GR 912''' and '''Sport 2000''' are a family of [[Australia]]n [[light-sport aircraft]], designed and produced by [[Australian Lightwing]] and introduced in 1986. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 30. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 31. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
The aircraft series feature a [[strut-braced]] [[high-wing]], a two-seats-in-[[side-by-side configuration]] enclosed cockpit, fixed [[tricycle landing gear]] or [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="WDLA11" /><ref name="WDLA15"/>

The aircraft is made with a welded steel [[fuselage]] covered in a combination of [[fibreglass]] and [[Aircraft dope|doped]] [[aircraft fabric]]. Its {{convert|9.50|m|ft|1|abbr=on}} span wing is made with an [[aluminum]] frame and partially covered in aluminum sheet and doped fabric. Standard engines available are the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] [[two-stroke]], the {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912UL]], the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]] [[four-stroke]] powerplant and automotive conversions. The cockpit width is {{convert|106|cm|in|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/>
<!-- ==Operational history== -->

==Variants==
[[File:25-0080 Australian LightWing GR-532 (9269900681).jpg|thumb|right|Australian LightWing GR 532]]
;GR 532
:Initial version with [[Rotax 532]] powerplant
;GR 582
:Version with [[Rotax 582]] powerplant<ref name="WDLA11" />
;GR 912
:Tail wheel-equipped version<ref name="WDLA11" />
;Sport 2000
:Nose-wheel equipped version<ref name="WDLA11" />
<!-- ==Aircraft on display== -->

==Specifications (GR 912) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=9.50
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=600
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|60|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912UL]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=60<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=160
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=3.2 hours
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Australian LightWing}}
*{{Official website|http://www.lightwing.com.au/sport.html}}
{{Australian Lightwing aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Australian Lightwing aircraft]]