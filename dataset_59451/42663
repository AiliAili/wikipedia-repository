<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=1910 Monoplane
 | image=Deperdussin 1910 at Shuttleworth uncovered 2013.JPG
 | caption=Deperdussin monoplane belonging to the [[Shuttleworth Collection]].  
}}{{Infobox Aircraft Type
 | type=Sports aircraft
 | national origin=[[France]]
 | manufacturer=[[Société Pour L'Aviation et ses Dérivés|Aéroplanes Deperdussin]]
 | designer=[[Louis Béchereau]]
 | first flight=[[1910 in aviation|1910]]
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= 
 | produced=
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''1910 Deperdussin monoplane''' was the first aircraft to be built in significant quantities by [[Société Pour L'Aviation et ses Dérivés|Aéroplanes Deperdussin]]. The type was produced in a number of variants which were flown successfully in [[Air racing|air races]] and gained several records during 1911, and was used by the [[Australia]]n [[Central Flying School RAAF|Central Flying School]] at [[Point Cook, Victoria]]. Several have survived, including an airworthy example in the [[Shuttleworth Collection]] in England.

==Background==
Aéroplanes Deperdussin was established in 1909 by the silk broker [[Armand Deperdussin]] with [[Louis Béchereau]] acting as the technical director. The first product of their aircraft works at [[Laon]] was a [[canard (aeronautics)|canard]] configuration design, which was not a success. The 1910 monoplane was their first successful design.  The prototype, which was first flown by [[George Busson]] at [[Issy-les-Moulineaux]] in October 1910,<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200893.html|title= Doings at Issy|journal=[[Flight International|''Flight'']]|date= 29 October 1910|pages= 891–2}}</ref> possibly powered by a water-cooled inline 4-cylinder [[Clerget]] engine.

==Design and development==
[[File:Deperdussin NTM.jpg|thumb|right|Deperdussin Type A, Norsk Teknisk Museum.]]
[[File:Deperdussin B controls.png|thumb|right|Controls of the Deperdussin monoplane]]
The 1910 Deperdussin monoplane was a [[tractor configuration]] mid-wing monoplane, with a very slender [[fuselage]] formed by a shallow fabric-covered wire-braced wooden box-girder, the [[longerons]] curving in to a vertical knife-edge at the back.  The depth of the front section of the fuselage was increased by a shallow shell of wood [[Wood veneer|veneer]] built over curved formers.  Due to the extremely shallow fuselage, the pilot was almost completely exposed, sitting on rather than in the fuselage.<ref name = Flight713>{{cite journal|journal=[[Flight International|Flight]]|title=The Deperdussin Monoplane|date=19 August 1911   |page=713|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200711.html}}</ref>

The wings were mounted below the upper [[longeron]]s and were slightly tapered.  Lateral control was by [[wing warping]]. In earlier aircraft the warping and bracing wires were attached to a pair of vertical [[kingpost]]s just aft of the leading edge of the wing: in later aircraft these were braced by diagonal struts leading back to the fuselage longerons.  The [[empennage]] of early examples consisted of an elongated triangular vertical fin with a cutout to allow [[elevator (aircraft)|elevator]] travel with a rectangular [[rudder]] hinged to the trailing edge, and a similarly elongated triangular [[tailplane]] with an elevator.<ref name= Flight1050>{{cite journal|journal=[[Flight International|Flight]]|title=Silhouettes from the Paris Show: The Deperdussin Monoplane |date= 24 December 1910|page= 1050 |url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%201053.html}}</ref>

In later examples, such as that in the Shuttleworth Collection, the horizontal tail surface was shorter, the leading edges being swept at about 45°.  A distinctive feature of the type was that the trailing edge of the rudder and elevator was braced by wires leading to the control horns.  The controls consisted of a wheel mounted on an inverted U-shaped yoke, the uprights of which were outside the fuselage structure. Fore and aft movement of the entire yoke operated the elevator and the wheel operated the wing warping.  The rudder was controlled by pedals.  The undercarriage consisted of a pair of [[trapezoid]]al frames, each braced by a diagonal member extended forwards to form a short upcurved skid to protect the propeller in the case of nose-overs, with a pair of wheels carried on a sprung cross axle between the two frames.<ref name = Flight715>{{cite journal|journal=[[Flight International|Flight]]|title=The Deperdussin Monoplane|date=19 August 1911 |page=715|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200713.html}}</ref>

==Variants and nomenclature==
The example shown at the 1910 Paris Aero Salon had a length and wingspan of 9&nbsp;m (29&nbsp;ft 6&nbsp;in), a wing area of 15&nbsp;m<sup>3</sup> (161&nbsp;ft<sup>3</sup>) and was powered by a 4 cylinder water-cooled Clerget engine driving a six-bladed propeller.<ref name= Flight1050/>

Although Deperdussin did produce a catalogue in 1911 which lists Types A, B, C, D and E,<ref>{{cite web|url=http://www.marelibri.com/t/main/3307204-deperdussin/books/AUTHOR_AZ/0|publisher=Marelibri|title=Deperdussin|accessdate=22 December 2013}}</ref> this system of nomenclature was not generally used at the time: an example of contemporary nomenclature is provided by the report in ''[[Flight International|Flight]]'' on the 1911 Paris Aero Salon. Deperdussin exhibited four aircraft: these are described as the "School Type", the "Military single seater", the "Military two-seater" and the "Military three-seater" .<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%201103.html|title= A Tabular Description of the Aeroplanes Exhibited at the Third Paris Aero Salon|journal=[[Flight International|''Flight'']]|date= 23 December 1911|page=1111}}</ref> The "School Type" was powered by a {{convert|25|hp|kW||abbr=on}} [[Anzani 3-cylinder fan engines|Anzani]] three-cylinder semi-radial engine, was 7.45&nbsp;m long with a wingspan of 8.5&nbsp;m. The "Military single seater" is powered by a {{convert|50|hp|kW|abbr=on}} [[Gnome Omega]] and is slightly longer at 7.5&nbsp;m. The "Military two-seater" was powered by a {{convert|70|hp|kW|abbr=on}} [[Gnome Gamma]] and was 8&nbsp;m (26&nbsp;ft 3&nbsp;in) long with a wingspan of 10&nbsp;m.(29&nbsp;ft 10&nbsp;in). The three-seater is described as  being a different type. 
<!--Old Warden ac:Height: 7ft 6in Length: 24ft 10in Wingspan: 28ft 9in Engine: one 35hp Anzani 3 cylinder 'Y' type-->

Neither the [[Shuttleworth Collection]] nor the [[RAAF Museum]] use any type designation for the aircraft in their collections.

==Operational history==
*On February 13 Busson set a new speed record for an aircraft carrying a passenger at Reims, flying {{convert|100|km|mi|abbr=on}} in 1&nbsp;h 1&nbsp;min 32&nbsp;s, a speed of {{convert|98.74|kph|mph|abbr=on}}, bettering the previous record over the distance by more than 15 minutes.  The aircraft was described as a "Military type 2/3 seater" <ref>{{cite journal|journal=[[l'Aérophile]]|language=French|url=http://gallica.bnf.fr/ark:/12148/bpt6k65523525/f108.image|title=Les Records du Mode avec Passenger |date=1 March 1911|page=102}}</ref>
*On 10 March Busson set two more records over a {{convert|2.5|km|mi|abbr=on}}  circuit, carrying 3 passengers {{convert|50|km|mi|abbr=on}} in 31&nbsp;min 31.2&nbsp;s, a speed of over {{convert|95|kph|mph|abbr=on}} and four passengers {{convert|25|km|mi|abbr=on}} in 17&nbsp;min 28.2&nbsp;s.<ref>{{cite journal|journal=[[l'Aérophile]]|language=French|url=http://gallica.bnf.fr/ark:/12148/bpt6k65523525/f139.image|title=Les Records avec 3 et 4 Passengers|date=15 March 1911|page=133}}</ref> 
*Rene Vidart won third place in the 1911 [[Circuit of Europe]].<ref>{{Cite newspaper The Times |articlename=Finish Of Aviation Circuit|section=News |day_of_week=Saturday |date=8 July 1911 |page_number= 12|issue= 39632|column=D}}</ref>  Seven Deperdussins were entered in the race. 
*[[James Valentine (aviator)|James Valentine]] won third place in the [[Daily Mail Circuit of Britain Air Race|''Daily Mail'' 1911 Circuit of Britain race]].<ref>{{cite journal|journal=[[Flight International|Flight]]|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200659.html|title= Table of Competitors' Times, Distances &c|date= 26 July 1911|page=}}</ref> Although Valentine did not succeed in completing the course inside the stipulated time limit, the simple fact of his finishing the course provided good publicity for Deperdussin. 
*Second and third places in the Belgian National Circuit race, flown by Lanser and Hessel<ref>{{cite journal|journal=[[Flight International|Flight]]|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200762.html|title= The Belgian National Circuit|date= 2 September 1911|page=764}}</ref>

Deperdussin, like most contemporary French aircraft manufacturers, also ran their own flying schools, at [[Étampes]], Pau, and [[Betheny]] in France and at [[Brooklands]] in England, and many early aviators learned to fly on Deperdussin aircraft.

Two examples were bought by the Australian government and formed part of the equipment of the Central Flying School at Point Cook, together with two [[Royal Aircraft Factory BE 2]]as and a [[Bristol Boxkite]]<ref>{{cite web|url=http://www.environment.gov.au/heritage/places/national/point-cook/index.html|publisher=Australian Government  |title=National Heritage Places - Point Cook Air Base|accessdate=9 October 2013}}</ref>

==Survivors==
* [[Shuttleworth Collection]], England.[http://www.shuttleworth.org/shuttleworth-collection/aircraft-details.asp?ID=2 T1910 Deperdussin] An airworthy Anzani-engined example.
* [[Norsk Teknisk Museum]], Oslo
* [[RAAF Museum]], Point Cook, Australia

==Specifications==

{{Aircraft specs
|ref=[[Flight International|''Flight'']]: 19 August 1911, p.714.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200712.html The Deperdussin Monoplane]</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=23
|length in=9
|length note=
|span m=
|span ft=28
|span in=9
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=150
|wing area note=
|airfoil=
|empty weight kg=280
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name= [[Anzani 3-cyl. Fan 40-45 hp]]
|eng1 type=3 cylinder air-cooled semi-radial
|eng1 kw=37<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=8
|prop dia in=0
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=90
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=
|range miles=
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
}}

==Notes==
{{reflist|30em}}

==References==
{{Commons category|Deperdussin 1910 monoplane}}
{{refbegin}}
*Hallion, Richard P. ''Taking Flight'' New York, Oxford University Press, 2003 ISBN 0 19 516035 5 
*Opdycke, Leonard E. ''French Aeroplanes Before the Great War'' Atglen, PA: Schiffer, 1999 ISBN 0 7643 0752 5

{{refend}}

[[Category:French sport aircraft 1910–1919]]
[[Category:French military aircraft 1900–1909]]
[[Category:Propeller aircraft]]
[[Category:Single-engine aircraft]]