<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=Neiva B Monitor
|image=File:Neiva b monitor.jpg
|caption=
}}{{Infobox Aircraft Type
|type=two-seater training sailplane
|national origin=Brazil
|manufacturer=[[Indústria Aeronáutica Neiva]]
|designer=Jose Carlos de Barros Neiva
|first flight=1945
|introduced=1950
|number built= 21+
|developed from=[[Grunau Baby]]}}|}

The '''Neiva B Monitor''', also designated '''B-2''',<ref name=JAWA56>{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|pages=}}</ref> is a [[Brazil]]ian tandem two-seat [[glider aircraft]] designed and manufactured by [[Indústria Aeronáutica Neiva]] between 1945 and 1955 for primary training and general flying.

==Design and development==
The B Monitor was designed by [[Jose Carlos de Barros Neiva]] after the end of [[World War II]]. At that time [[Brazil]] was conducting the "''Campanha Nacional de Aviação''" (National Aviation Campaign), a nationwide program focused on re-equipping and incentivizing [[Aero club]]s and the aeronautical industry. The lack of good performance gliders with good training qualities to replace the ageing and obsolete sailplane training fleet, composed in its majority by German types of the 1930s, was acute.
Jose Carlos de Barros Neiva designed then a glider which conserved the best characteristics of the [[Grunau Baby]], widely used in [[Brazil]] at the time. The first prototype, registered ''PP-PCB'' had its maiden flight in 1945, and the [[type certificate]] was given in 1950.
The “Campanha Nacional de Aviação” bought the first prototype and other 20 aircraft for the aero clubs. A modified version called B-Monitor Modificado was built with a different nose and different materials in the mid-1950s<ref>{{Cite book|title=Aircraft Building: A Brazilian Heritage|last=Andrade|first=Roberto Pereira de|publisher=|year=2008|isbn=85-89357-03-01|location=Sao Paulo|pages=181}}</ref>.

===Construction===
The Monitor is largely a wooden aircraft. Its [[Wing configuration#number placement of mainplanes|high wings]] are built around a single box spar, with a torsion resisting, plywood covered leading edge. Behind the spar the wing is fabric covered. Dihedral is just 0.5°.  The wings are braced to the lower fuselage with a pair of single [[lift strut]]s. The [[ailerons]] are wood framed and fabric covered; [[Spoiler (aeronautics)|spoilers]] are wooden.<ref name=JAWA56/>

The fuselage is mainly built with wood, a semi-monocoque construction with steel tubes for wing attachments. The two seat cockipt has dual controls. The [[empennage]] is conventional, with a [[braced]] tailplane mounted on top of the fuselage. The landing gear is a forward skid with elastic dampers and a single, fixed wheel aft of the centre of gravity.<ref name=JAWA56/>

==Variants==

===Neiva B Modificado===
In 1959, Neiva produced a version called B-Monitor Modificado with different nose, and cockpit. The fuselage was built with welded steel tubes, but conserving the same wing and tailplane. A single unit was produced (Serial number CTA-02 A-223<ref>{{Cite web|url=http://www2.anac.gov.br/certificacao/Produtos/Espec/EP-4601-01p.pdf|title=Neiva B-Modificado Serial Number|last=|first=|date=|website=ANAC|archive-url=|archive-date=|dead-url=|access-date=}}</ref>), for evaluation at the [[Department of Aerospace Science and Technology]] of the Brazilian Air Force, and was later donated to CVV-CTA Aeroclub<ref>{{Cite book|title=Aircraft Building: A Brazilian Heritage|last=Andrade|first=Roberto Pereira de|publisher=|year=2008|isbn=85-89357-03-1|location=Sao Paulo|pages=181}}</ref>.

[[File:Neiva-bmonitor.jpg|left|thumb|The only surviving Neiva-B modificado at CVV-CTA in 2009 ]]

==Operational history==
The B-Monitor were used in many aero clubs and have trained hundreds of pilots. Many Brazilian soaring records were established in the type and it was used as a primary trainer in some of the largest Brazilian aero clubs, including CVV-CTA in São José dos Campos/Ipuã, Rio Claro, Osório, and Tatuí.

==Specifications==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1956/7<ref name=JAWA56/>  The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=7.1
|length ft=
|length in=
|length note=
|span m=15.86
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=1.13
|height ft=
|height in=
|height note=
|wing area sqm=18.4
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=13.67
|airfoil=Göttingen 535 - [[NACA airfoil|NACA 0009]]
|empty weight kg=215
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=375
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=52
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=145
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+5.33 -3.31 at {{convert|145|km/h|mph kn|abbr=on|1}}
|roll rate=<!-- aerobatic -->
|glide ratio=18 at {{convert|67|km/h|mph kn|abbr=on|1}}
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=0.78
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=at {{convert|55|km/h|mph kn|abbr=on|1}}
|lift to drag=
|wing loading kg/m2=20.3
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

}}

==Notes==
{{reflist}}

==References==
*{{Cite book|last=Pereira de Andrade|first=Roberto |title=A Construção Aeronáutica no Brasil 1910/1976|publisher=Editora Brasiliense|location=São Paulo|year=1976}}
*{{Cite book|last=Pereira de Andrade|first=Roberto|title=Enciclopédia de Aviões Brasileiros|publisher=Editora Globo|location=São Paulo|year=1997|isbn=85-250-2137-7}}
*{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|pages=}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}
*{{Cite web|url=http://www2.anac.gov.br/certificacao/Produtos/Espec/EP-4601-01p.pdf|title=Neiva B Type Certificate|last=|first=|date=|website=ANAC|archive-url=http://www2.anac.gov.br/certificacao/Produtos/Espec/EP-4601-01p.pdf|archive-date=September 2011|access-date=2 April 2017 }}

==External links==
{{commons category|Neiva B Monitor}}
*''[http://www.aeroneiva.com.br Neiva’s official site]''

[[Category:Neiva aircraft|B Monitor]]
[[Category:Brazilian sailplanes 1940–1949]]
[[Category:Glider aircraft]]
[[Category:High-wing aircraft]]