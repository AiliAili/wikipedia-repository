{{Orphan|date=October 2013}}

{{Infobox building
| name                = Lottie Moon House
| native_name         =
| image               = 
| image_alt           = 
| caption             = Main façade of the Lottie Moon House - 1938
{{cite book
  | last = 
  | first = 
  | authorlink = 
  | title = Lottie Moon House - FS
  | publisher = Smith History Library 
  | series = 
  | volume = 
  | edition = 
  | year = 1938
  | location = 
  | pages = 
  | language = 
  | url = 
  | doi = 
  | id = 
  | isbn =
  | mr = 
  | zbl = 
  | jfm =}}
| former_names        = 
| alternate_names     =
| map_type            =
| map_alt             = 
| map_caption         = 
| building_type       = 
| architectural_style = Nineteenth Century
| structural_system   = 
| cost                = 
| location            = [[Oxford]], [[Ohio]]
| client              = 
| owner               = [[Miami University]]
| current_tenants     = 
| landlord            = 
| location_country    = [[United States]] 
| coordinates         = {{coord|39|30|38.45|N|84|44|16.60|W|display=inline}}
| altitude            = 950.174 feet
| start_date          = 
| completion_date     = 1831
| inauguration_date   = 
| demolition_date     = 
| height              = 
| diameter            = 
| other_dimensions    = 60 Feet from Road
| floor_count         = 2
| floor_area          = 
| main_contractor     = 
| architect           = 
| structural_engineer = 
| services_engineer   = 
| civil_engineer      = 
| other_designers     = 
| quantity_surveyor   = 
| awards              = 
| url                 = 
| references          =}}
[[File:Lottie Moon House - Present.jpg|thumb|Main façade of the Lottie Moon House - 2011]]

The '''Lottie Moon House''', located at 220 East High Street in [[Oxford, Ohio]], was completed in 1831. It was not named Lottie Moon House, but rather adopted the name after being inhabited by the Moon family in 1839. The nineteenth-century [[architecture]] is a simple design enhanced by a one-story wooden [[porch]] that spans most of the entire front of the house. The front of the house is symmetrical which adds to its simple beauty. 
<ref>{{cite book 
  | last1 = Smith | first1 = Ophia
  | first2 = Lucy | last2 = Curry
  | title = Ohio History Inventory - Lottie Moon House
  | location = Oxford 
  | publisher = Smith Library}}</ref>

The Lottie Moon House is part of the historic [[neighborhood]] that surrounds [[Miami University]]. The house was built as a residence, but after Lottie Moon became famous for being a [[Confederate States of America|Confederate]] spy during the [[American Civil War]], the house  was deemed of historic importance, and it was donated to Miami University to be a part of their campus.

==Lottie Moon==
Cynthia Charlotte Moon, known as Lottie, was a young girl when her family moved into the residency at 220 East High Street. Lottie became famous as a Confederate [[spy]]. She rode the battle lines in the South in President [[Abraham Lincoln]]'s personal [[carriage]]. Lottie was disguised as "Lady Hull", a [[rheumatic]] English invalid. She pretended to be asleep while President Lincoln and [[Secretary of War]], [[Edwin M. Stanton]], discussed upcoming strategies.
<ref>{{cite web
  | title = The Moon Sister Spies
  | work = Confederate Sister Act
  | publisher = AUG LINK Communications Inc. 
  | year = 2010
  | url = http://userpages.aug.com/captbarb/moon.html
  | accessdate =1 Nov 2011}}</ref>{{failed verification|date=February 2012}}

The two [[Union (American Civil War)|Union]] men believed that they were taking Lady Hull to the South for a [[hot springs|warm springs]] treatment for her sickness. Stanton offered $10,000 for the capture of Lottie Moon after realizing Lady Hull was Lottie Moon.
<ref>{{cite book
  | last = Shriver
  | first = Phillip
  | title = History of Women at Miami
  | publisher = Women's History Month
  | date = 5 Mar 1996
  | location = Shriver Center}}</ref>

==Past residents==
The Moon family was the first and most famous family to reside at 220 East High Street. Robert Moon, Lottie's father, moved his family into the house in 1839, and they lived as a family in the house until 1849. In 1849, Robert Moon offered the house as a gift to Lottie upon the announcement of her [[engagement]] to James Clark. During the Civil War, Lottie moved to the South to be closer to the front lines and better able to relay messages.<ref>{{Citation
  | contribution = Butler Country Reappraisement Records 
  | year = 1846}}</ref> Robert and his family continued to live in the house until his death in 1856.

Dr. John Hall, president of Miami University from 1855 to 1866, moved into the house with his family. They lived in this house until his [[resignation]] from presidency.<ref>{{Citation
  | contribution = Butler Country Reappraisement Records 
  | year = 1859}}</ref> In 1870, Milo Sawyer, a retired oil cloth [[manufacturer]] moved into the house with his wife, Fannie, and their two children. He was a university [[professor]] and resided in the house for two years.

Sutton C. Richey bought the house from Milo Sawyer in 1873. Richey was a [[druggist]]. His wife, Rella, and their three children lived in the house. Richey had a practice in the McCullough Building located at 20 East High Street from 1859 until 1891. When Mr. Richey died, Rella lived in the house with their daughter, Jennie, until Rella's son, Samuel Webster Richey, took the house for himself. Samuel passed it on to his son, Sheffield Clay Richey, who donated the house to Miami University in 1988 with his sons,<ref>{{cite journal
  | title = Samuel W. Richey Gravesite Rites Set
  | journal = Oxford Press
  | issue = 2
  | date = 7 June 1973}}</ref> Sheffield Jr. and Tom Richey, representing him. The Lottie Moon House was recognized as a piece of historic property and accepted by [[Provost (education)|Provost]] E. Fred Carlisle. At the donation ceremony Provost Carlisle announced that "Miami University values its Oxford heritage very much. This house demonstrates the richness of that heritage."<ref>{{cite journal
  | title = Richeys Give Miami University Famed "Lottie Moon" Residence
  | journal = The Oxford Press
  | issue = 2
  | date = 18 Feb 1988}}</ref>

==Details of Lottie Moon House==
The house was originally a residence, but now is used as a [[historic site]] for the City of Oxford, Ohio. There is a partial [[basement]]. The [[Foundation (engineering)|foundation]] is stone block, and the wall is brick bearing. The exterior material brick is [[American bond]], and the [[roof]] type is [[truncation (geometry)|truncated]] hip with [[asphalt]] shingles. 
<ref>{{cite book 
  | last1 = Smith | first1 = Ophia
  | first2 = Lucy | last2 = Curry
  | title = Ohio History Inventory - Lottie Moon House
  | location = Oxford 
  | publisher = Smith Library}}</ref>

The wooden porch on the front of the house is single story and spans the entire front of the house. The [[window]]s are six feet by six feet. There are three on the second story, and two surrounding the front door on the first story.

The house faces the Miami University campus. There is a fence around the premises with a gate topped with a lion's head that was made by Sutton C. Richey, a former owner. 
<ref>{{cite book
  |author=Smith Library Staff
  | title = Walking Tour of Oxford's University History District
  | publisher = Smith Library 
  | year = 2008
  | location = Oxford}}</ref>

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

[[Category:Miami University]]
[[Category:Houses completed in 1831]]