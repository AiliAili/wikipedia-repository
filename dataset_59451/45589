{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=Stanisław Skalski
|honorific_suffix = {{post-nominals|post-noms=DSO DFC**}}
|image= Stanislaw Skalski in color.jpg
|rank=[[Generał brygady]]
|birth_date={{Birth date|df=yes|1915|11|27}}
|birth_place=[[Kodyma]], [[Russian Empire]]
|death_date={{death date and age|df=yes|2004|11|12|1915|11|27}}
|death_place=[[Warsaw]], [[Poland]]
|allegiance=[[Poland]]
|serviceyears=1938–1945, 1956–1972
|commands=Wing Commander in [[No. 316 Polish Fighter Squadron|316 PAF]], [[commanding officer|CO]] of [[No. 317 Polish Fighter Squadron|317 PAF]], [[Polish Fighting Team|PFT]], [[No. 601 Squadron RAF|601 RAF]], 131st Fighter Wing, 133rd Fighter Wing
|battles=[[Invasion of Poland (1939)|Polish Defensive War]], [[World War II]]
|awards=[[Virtuti Militari]]; [[Polonia Restituta]]; [[Cross of Valour (Poland)|Cross of Valour]]; [[Distinguished Service Order]] (UK); [[Distinguished Flying Cross (UK)]]
}}

'''Stanisław Skalski''' {{post-nominals|country=GBR-cats|DSO|DFC**}}  (27 November 1915 – 12 November 2004) was a [[Poland|Polish]] [[fighter ace]] of the [[Polish Air Force]] in [[World War II]], later rising to the rank of [[Brigadier General]]. Skalski was the top Polish fighter ace of the war and the first Allied fighter ace of the war, credited, according to official lists, with 18 <small><sup>11</sup>/<sub>12</sub></small> victories and two probable. Some sources, including Skalski himself, give a number of 22 <small><sup>11</sup>/<sub>12</sub></small> victories.

==Biography==
[[File:Stanislaw Skalski 1.jpg|thumb|left|Stanisław Skalski]]

Stanisław Skalski was born on 27 November 1915 in [[Kodyma]] in [[Podolia Governorate]], [[Russian Empire]] (now in [[Ukraine]]). After completing Pilot Training School in 1938 Skalski was ordered to the 142nd Fighter Squadron in Toruń (142 eskadra "Toruńska"). On 1 September 1939 he attacked a German [[Henschel Hs 126]] reconnaissance aircraft, which was eventually shot down by [[Marian Pisarek]]. Skalski then landed next to it, helped to bandage wounded crew members and arranged for them to be taken to a military hospital.  By 16 September Skalski reached "ace" status, claiming a total of six German aircraft and making him the first Allied air ace of World War II.

His claims consisted of one [[Junkers Ju 86]], two [[Dornier Do 17]], one [[Junkers Ju 87]], two Hs 126s and one Hs 126 shared (official list credits him with four aircraft: two Do 17s, one Hs 126, one Ju 87 and one Hs 126 shared).<ref>Note: In this context "Ace" means shooting down five or more enemy aircraft.</ref> Soon after he fled the country with other Polish pilots to [[Romania]], and from there via [[Beirut]] to [[France]] and after went on to fight with the [[Royal Air Force]] in the [[Battle of Britain]].

In August 1940 [[Pilot Officer]] Skalski joined [[No. 501 Squadron RAF|501 Squadron]]. From 30 August to 2 September 1940 he shot down a He 111 bomber and three [[Messerschmitt Bf 109]]s. On 5 September Skalski himself was shot down.<ref>According to some sources, he shot down a He 111 bomber and two Bf 109s in that flight, but there is no firm evidence, these victories were not acknowledged officially, and several other pilots also claimed these Bf 109s.</ref> Skalski bailed out with severe burns that hospitalized him for six weeks. He returned to his unit in late October 1940. During the [[Battle of Britain]], he was credited with four planes shot down and one shared.

In March 1941 he was assigned to the Polish [[No. 306 Polish Fighter Squadron|306 (Polish) Squadron]],<ref>Note: 306 Dywizjon Myśliwski "Toruński"; the unit insignia was derived from that of Skalski's original unit, 142 eskadry</ref> flying in ''Circus'' sorties<ref>Circus missions aimed to draw out enemy fighters in response to a perceived bombing attack</ref> over France. On 1 March 1942, he became a flight commander in [[No. 316 Polish Fighter Squadron|316 (Polish) Squadron]]. On 29 April 1942 [[Flight Lieutenant]] Skalski was made Commanding Officer of the [[No. 317 Polish Fighter Squadron|317 (Polish) Squadron]] for five months. From November 1942 he was an instructor with No. 58 Operation Training Unit.

In October 1942 he was given command of the [[Polish Fighting Team]] (PFT), or so called "Cyrk Skalskiego" (Skalski's Circus) - a Special Flight consisting of fifteen experienced Polish fighter pilot volunteers. The Poles arrived at Bu Grara airfield, west of [[Tripoli]] in March 1943 and attached to [[No. 145 Squadron RAF|145 Squadron]]. The PFT took part in actions in [[Tripolitania]] and in [[Sicily]]. On 6 May 1943 the "Skalski Circus" fought its last combat. The unit has been disbanded after the conclusion of the North African campaign.

During its two months on operations, the Polish pilots had claimed a total of 26 German and Italian aircraft shot down. Flight Lieutenant Skalski scored four aircraft, and Pilot Officer [[Eugeniusz Horbaczewski]] claimed five confirmed victories.

[[File:Skalski.JPG|thumb|left|[[Wing Commander (rank)|Wg Cdr]] Skalski with [[Air Marshal|Air Mshl]] [[Arthur Coningham (RAF officer)|Coningham]] (pictured left) and General [[Kazimierz Sosnkowski]] (pictured right).]]

Skalski then became commander of [[No. 601 Squadron RAF|601 (County of London) Squadron]] the first Pole to command an RAF Squadron. He then took part in the [[invasion of Sicily]] and [[invasion of Italy]]. From December 1943 to April 1944 [[Wing Commander (rank)|Wing Commander]] Skalski commanded No. 131 Polish Fighter Wing. On 4 April 1944 he was appointed commander of No. 133 Polish Fighter Wing flying the Mustang Mk III. On 24 June 1944 Skalski scored two air victories over [[Rouen]].

He left for a tour of duty in the USA in September 1944, returning in February 1945 to a staff position at No. 11 Group.

After the war he returned to Poland in 1947 and joined the [[Air Force of the Polish Army]]. In 1948 however he was arrested under the false charge of espionage. Sentenced to death, he spent three years awaiting the execution, after which his sentence was changed to [[life imprisonment]] in [[Wronki Prison]]. 
[[File:Skalski Mustang III.jpg|thumb|A Mustang flown by [[Wing Commander (rank)|Wing Commander]] Stanisław Skalski, C/O of 133(Polish) Fighter Wing, [[Coolham]], June 1944.]]

After the end of [[Stalinism]] in Poland, in 1956 he was released, rehabilitated, and allowed to join the military. He served at various posts in the Headquarters of the Polish Air Forces. He wrote memoires of the 1939 campaign ''Czarne krzyże nad Polską'' ("Black crosses over Poland", 1957). On 20 May 1968 he was nominated the secretary general of the [[Aeroklub Polski]] and on 10 April 1972 he retired. On 15 September 1988 he was promoted to the rank of [[Generał brygady|Brigadier General]]. In 1990 he met with the German pilot he had rescued on the first day of the war.

Stanisław Skalski died in Warsaw on 12 November 2004.

==Awards==
[[File:POL Virtuti Militari Złoty BAR.svg|60px]] [[Virtuti Militari]], Golden Cross<br>
[[File:Virtuti Militari Ribbon.png|60px]] [[Virtuti Militari]], Silver Cross <br> 
[[File:POL Krzyż Walecznych (1940) 4r BAR.PNG|60px]] [[Cross of Valour (Poland)]], four times<br> 
[[File:POL Polonia Restituta Kawalerski BAR.svg|60px]] [[Order of Polonia Restituta]], Knight's Cross<br> 
[[File:POL Order Krzyża Grunwaldu 3 Klasy BAR.svg|60px]] [[Order of the Cross of Grunwald]], 3rd class<br>
[[File:Dso-ribbon.png|60px]] [[Distinguished Service Order]]<br>
[[File:UK DFC w 2bars BAR.svg|60px]] 
[[Distinguished Flying Cross (United Kingdom)]] and two [[medal bar|bars]]<br> 
[[File:39-45 Star w BoB clasp BAR.svg|60px]] [[1939-1945 Star]] with [[Battle of Britain]] clasp <br>
[[File:Italy Star BAR.svg|60px]] [[Italy Star]]

[[File:Stanislaw Skalski monument.jpg|right|thumb|Stanisław Skalski's monument, [[Warsaw]] ([[Poland]]), 30 July 2006]]
[[File:Stanislaw Skalski auf MiG-29.jpg|right|thumb|Stanislaw Skalski on the tailfin of a Polish [[Mikoyan MiG-29]] (2016)]]

==References==

===Notes===

{{Reflist}}

===Bibliography===

{{refbegin}}
* Cynk, Jerzy Bogdam. ''Polskie lotnictwo myśliwskie w boju wrześniowym'' (in Polish). Gdańsk, Poland: AJ-Press, 2000.
* Cynk, Jerzy Bogdam. ''Polskie Siły Powietrzne w wojnie tom 1: 1939-43 (Polish Air Force in War pt. 1: 1939-43)'' (in Polish). Gdańsk, Poland: AJ-Press, 2001. <br>(Updated and revised edition of ''The Polish Air Force at War: The Official History, Vol.2 1939-1943''. Atglen, PA: Schiffer Books, 1998. ISBN 0-7643-0559-X.)
* Cynk, Jerzy Bogdam. ''Polskie Siły Powietrzne w wojnie tom 2: 1943-45 (Polish Air Force in War pt. 2: 1943-45)'' (In Polish). Gdańsk, Poland: AJ-Press, 2002. <br>(Updated and revised edition of ''The Polish Air Force at War: The Official History, Vol.2 1943-1945''. Atglen, PA: Schiffer Books, 1998. ISBN 0-7643-0560-3.)
* Grabowski, Franciszek. ''Gen. bryg. pil. Stanisław Skalski''. in: "Militaria i Fakty" 2/2005 (Polish)
* Grabowski, Franciszek. ''Stanisław Skalski''. Sandomierz, Poland/Redbourn, UK: Mushroom Model Publications, 2007. ISBN 83-89450-11-9.
* Gretzyngier, Robert. ''Poles in Defence of Britain: A Day-by-day Chronology of Polish Day and Night Fighter Pilot Operations - July 1940 - June 1941''. London: Grub Street, 2005. ISBN 1-904943-05-5.
* Ochabska, Katarzyna. ''Stanisław Skalski''. Gliwice, 2007.
* Skalski, Stanisław. ''Czarne krzyże nad Polską'' (in Polish). Warszawa, Poland, 1957 (New edition: De Facto, 2006).
{{refend}}

===Further reading===

{{refbegin}}
* Cynk, Jerzy Bogdam. ''History Of The Polish Air Force 1918-1968''. UK: Osprey Publications, 1972.
* Koniarek, Dr. Jan. ''Polish Air Force 1939-1945''. Carrollton, TX: Squadron/Signal Publications, Inc.,1994. ISBN 0-89747-324-8.
* Kornicki, Franciszek. ''Polish Air Force- Chronicle of Main Events''. UK: Polish Air Force Association of Great Britain, 1993.
* Lisiewicz, Mieczyslaw (Translated from the Polish by Ann Maitland-Chuwen). ''Destiny can wait - The Polish Air Force in the Second World War''. London: Heinemann, 1949.
* Zamoyski, Adam. ''The Forgotten Few: The Polish Air Force in The Second World War''. UK: Leo Cooper Ltd., 2004. ISBN 1-84415-090-9.
{{refend}}

==External links==
{{commons category}}
* Krajewski, Wojciech. [http://www.muzeumwp.pl/download.php?FID=40&name=Genera%C5%82%20brygady%20pilot%20Stanis%C5%82aw%20Skalski,%20as%20polskiego%20lotnictwa.pdf&mine=application/pdf "Generał brygady pilot Stanisław Skalski as polskiego lotnictwa"]

{{Authority control}}

{{DEFAULTSORT:Skalski, Stanislaw}}
[[Category:1915 births]]
[[Category:2004 deaths]]
[[Category:People from Kodyma Raion]]
[[Category:People from Podolia Governorate]]
[[Category:The Few]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Polish People's Army generals]]
[[Category:Polish World War II flying aces]]
[[Category:Knights of the Order of Polonia Restituta]]
[[Category:Gold Crosses of the Virtuti Militari]]
[[Category:Recipients of the Distinguished Flying Cross and two Bars (United Kingdom)]]
[[Category:Recipients of the Order of the Cross of Grunwald, 3rd class]]
[[Category:Recipients of the Cross of Valour (Poland)]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force pilots of World War II]]