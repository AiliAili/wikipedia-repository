'''Women For Sobriety, Inc.''' ('''WFS'''), is a [[non-profit]] [[secular]] [[addiction recovery groups|addiction recovery group]] for  women with addiction problems. WFS was created by [[sociologist]] Jean Kirkpatrick in 1976 as an alternative to [[Twelve-step program|twelve-step]] addiction recovery groups like [[Alcoholics Anonymous]] (AA). As of 1998 there were more than 200 WFS groups worldwide.<ref name="TONIGAN1998"/> Only women are allowed to attend the organization's meetings as the groups focus specifically on women's issues. WFS is not a [[Radical feminism|radical feminist]], anti-male, or anti-AA organization.<ref name="KASKUTAS1996ROAD"/>

==History==
Jean Kirkpatrick attended AA meetings for three years and was unable to maintain sobriety. The methods of what is now the "New Life" program of Women For Sobriety empowered Kirkpatrick to quit drinking. Discovered through [[trial and error]], the New Life methods are based largely on the writings of [[Ralph Waldo Emerson]] (particularly his essay, ''[[Self-Reliance]]'') and the [[New Thought|Unity Movement of New Thought]] in addition to Kirkpatrick's personal experience, knowledge of [[sociology]], and experience in AA.<ref name="KASKUTAS1996ROAD">{{cite journal |title=A Road Less Traveled: Choosing the "Women for Sobriety" Program |last=Kaskutas |first=Lee A. |journal=Journal of Drug Issues |volume=26 |issue=1 |year=1996 |pages=77–94 |url=http://jod.sagepub.com/content/26/1/77.short}}</ref> In her design, as in AA, WFS encourages the open and hosting sharing but focuses on improving self-esteem and reducing guilt rather than admitting powerlessness. While Kirkpatrick's program stresses spirituality as the "fundamental object of life" the solution to alcoholism is described as being within the mind of the female alcoholic, not requiring a [[Higher Power]]. Also like AA, Kirkpatrick's program encourages complete abstinence from alcohol, rather than [[harm reduction]].<ref name="KASKUTAS1996PATHWAYS">{{cite journal |author=Kaskutas, Lee Ann |title=Pathways to Self-Help Among Women for Sobriety |journal=The American Journal of Drug and Alcohol Abuse |date=1 May 1996|pages=259–280 |volume=22 |issue=2 |doi=10.3109/00952999609001658 |url=http://www.tandfonline.com/doi/abs/10.3109/00952999609001658 |pmid=8727059}}</ref>

==Program==
The program is built on thirteen [[Affirmations (New Age)|affirmations]] encouraging members to change their [[self-image]] and [[world view]].<ref name="KASKUTAS1996PATHWAYS"/> As is practiced in [[SMART Recovery]], WFS members avoid labeling themselves as alcoholics and addicts and instead refer to themselves as competent women during meeting introductions.<ref name="CHIAUZZI">{{cite book |last=Chiauzzi |first=Emil J. |title=Time Effective Treatment with CE Test: A Manual for Substance Abuse Professionals |chapter=Chapter 2: Principles of Time-Effective Substance Abuse Treatment and Program Development |pages=9–27 |publisher=Hazelden |isbn=1-59285-046-4 |year=2003}}</ref> Philosophically, these ideas are close to [[modernity]], emphasizing [[self-control]] and [[rationality]].<ref name="HUMPHREYS1995">{{cite journal |last=Humphreys |first=Keith |author2=Kaskutas, Lee Ann  |title=World Views of Alcoholics Anonymous, Women for Sobriety, and Adult Children of Alcoholics/Al-Anon Mutual Help Groups |journal=Addiction Research & Theory |volume=3 |issue=3 |pages=231–243 |doi=10.3109/16066359509005240 |year=1995}}</ref> As described in WFS literature, the fundamental problem of females with alcohol dependence is low [[self-esteem]], a condition that is culturally reinforced in women more than in men, necessitating a qualitatively different treatment for women. In WFS members focus on responsibility rather than powerlessness, on self-esteem rather than [[humility]] and on thinking rather than [[Surrender (spirituality and psychology)|surrender]]. Like AA, WFS encourages meditation and spirituality, although sobriety is not viewed as dependent on a [[Higher Power]]. To increase self-esteem, WFS encourages [[Positive mental attitude|positive thinking]] and discourages negative thinking (a cause of low self-esteem).<ref name="KASKUTAS1996ROAD"/>

In WFS language, "faulty thinking" causes destructive behavior, consequently WFS teaches its members that they have the power to change their thinking to change their actions. The WFS approach, in this sense, is similar to [[cognitive behavioral therapy]]. Newcomers are encouraged to take pride in their accomplishments, no matter how small—even in an hour of sobriety. Similarly, members learn to beware of negative thoughts as they arise.<ref name="KASKUTAS1996ROAD"/> There are also elements of applied self-in-relation theory (the theory that a woman's sense of definition and value is strongly tied to their relationships with others); women are encouraged to build new, healthy relationships inside and outside of meetings.<ref name="Manhal-Baugus1998">{{cite journal |title=The Self-in-Relation Theory and Women for Sobriety: Female-Specific Theory and Mutual Help Group for Chemically Dependent Women |first=Monique |last=Manhal-Baugus |journal=[[Journal of Addictions and Offender Counseling]] |volume=18 |issue=2 |date=April 1998 |pages=78–85 |url=http://eric.ed.gov/?id=EJ569861}}</ref>

=== Affirmations ===
The thirteen affirmations represent six levels of growth in which members accept the physical nature of alcoholism (affirmation one), remove negativity (affirmations two, four and nine), learn to think better of themselves (affirmations five and twelve), change their attitudes (affirmations three, six and eleven), improve their relationships (affirmations seven and ten), and change their life's priorities (affirmations eight and thirteen).<ref name="KASKUTAS1996ROAD"/>
 
# I have a life-threatening problem that once had me.
# Negative thoughts destroy only myself.
# Happiness is a habit I will develop.
# Problems bother me only to the degree I permit them to.
# I am what I think.
# Life can be ordinary, or it can be great.
# Love can change the course of my world.
# The fundamental object of life is emotional and spiritual growth.
# The past is gone forever.
# All love given returns.
# Enthusiasm is my daily exercise.
# I am a competent woman and have much to give life.
# I am responsible for myself and my actions.

The First, Second, Tenth, Twelfth, and Thirteenth Affirmations were changed at some point in the 1990s{{Citation needed|date=September 2009}} these originally appeared, respectively, as: I have a drinking problem that once had me, Negative emotions destroy only myself, All love given returns twofold, I am a competent woman and have much to give to others, I am responsible for myself and my sisters.<ref name="KASKUTAS1996ROAD"/>

===Meetings===
Meetings range in size from two to twenty members, the ideal group size is between six and ten women. The room is arranged so that all the women are sitting in a circle. The meeting opens with a reading of the thirteen affirmations and the WFS purpose. The opening is followed by discussion among members based on a topic from WFS literature (e.g. acceptance, stress, compulsions, procrastination or one of the thirteen affirmations).<ref name="TONIGAN1998">{{cite book |title=Treating Addictive Behaviors |chapter=Mutual-Help Groups: Research and Clinical Implications |editor=Miller, William R. |editor2=Heather, Nick |publisher = [[Springer Science+Business Media|Springer]] |year=1998 |isbn=0-306-45852-7 |last=Tonigan |first=J. Scott |author2=Toscova, Radka T.  |pages=285–298}}</ref><ref name="KASKUTAS1996PATHWAYS"/><ref name="HUMPHREYS1995"/> Following a five- to ten-minute break, members begin the second part of the meeting. During the second part, members discuss what happened the previous week, each member is given a chance to speak and is encouraged to include at least one positive behavior or event.<ref name="KASKUTAS1996PATHWAYS"/><ref name="HUMPHREYS1995"/> Discussing previous drinking experiences, "drunkalogs," is discouraged as members are asked to keep their sharing positive. Cross-talk, responding directly to the speaker, is allowed.<ref name="KASKUTAS1996PATHWAYS"/> At the meeting closing members hold hands and recite the WFS motto, "We are capable and competent, caring and compassionate, always willing to help another, bonded together in overcoming our addictions."<ref name="TONIGAN1998"/>

A telephone list is distributed and members are allowed to call each other throughout the week. If someone has been hospitalized or has returned to drinking, other members will call her to offer their support.<ref name="KASKUTAS1996PATHWAYS"/>

===Moderators===
Meetings are run by moderators with at least one year of continuous sobriety who are familiar with the WFS program.<ref name="TONIGAN1998"/><ref name="KASKUTAS1996PATHWAYS"/> Moderators must be certified by WFS headquarters in [[Quakertown, Pennsylvania]].<ref name="HUMPHREYS1995"/>

== Demographics ==
During the winter of 1991 Lee Ann Kaskutas conducted a survey of all WFS members, she sent surveys to each active WFS group at the time and achieved 73% [[response rate (survey)|response rate]].<ref name="KASKUTAS1996ESTEEM">{{cite journal |author=Kaskutas, Lee Ann |title=Predictors of Self Esteem Among Members of Women for Sobriety |journal= Addiction Research & Theory |volume=4 |issue=3 |year=1996 |pages=273–281 |doi=10.3109/16066359609005572}}</ref> The information in this section is based on her analysis of the survey results.

The average WFS member is 46 years old, [[White people|white]], has been sober for 4.5 years, and is married with 1.8 children. About one-third of WFS members are [[Protestant]], another third are [[Catholic]], about one-fifth do not have a religious affiliation. Two-thirds have attended college and more than half are employed with an average individual income of $23,700 per year (an average household income of $51,800 per year). Half of WFS members have been sober for less than two years and in WFS for a year or less.<ref name="KASKUTAS1996ESTEEM"/> Women who took [[disulfiram|Antabuse]] were more likely to relapse than those who did not.<ref name="KASKUTAS1996PATHWAYS"/> Most members (40%) were self-referred, others were referred to WFS by a counselor or treatment program.<ref name="KASKUTAS1996ESTEEM"/> The vast majority of WFS members had received professional help at some point (89%), most frequently this was individual therapy and least frequently group therapy.<ref name="KASKUTAS1996PATHWAYS"/>

=== Correlates of self-esteem ===
Length of sobriety was correlated positively with membership in WFS. Half, however, of WFS members had been sober before joining WFS. Controlling for length of sobriety, length of time in WFS was positively correlated with self-esteem, as measured by the [[Rosenberg self-esteem scale]]. Three other independent variables were correlated with self-esteem: belief in the First Affirmation, frequent use of the Affirmations and disbelief in [[Twelve-step program#Twelve Steps|AA's First Step]].<ref name="KASKUTAS1996ESTEEM"/>

=== Turning points ===
A turning point represents an event or state that made WFS members realize they needed to do something about their drinking. The survey of WFS members found there were eight general categories of turning points: physical signs of alcoholism, emotional problems, general life problems, loss of control over drinking, being confronted about their drinking, problems related to driving, exposure to others' drinking problems and problems related to work. On average, the turning point occurred for WFS members at age 39, it took WFS members four years to achieve sobriety after their turning point and five years to become a member in WFS. Women who had a turning point entailing a realization that their life was out of control achieved sobriety in less than average time (two years). Similarly, women who had felt suicidal or attempted suicide achieved sobriety in one year. The five factors most frequently cited for attending WFS were: self-motivation, seeing information about WFS in a newspaper, WFS literature, hearing information from friends, and getting information from a counseling agency.<ref name="KASKUTAS1996PATHWAYS"/>

=== Dual membership ===
Nearly all, ninety percent, of WFS members have experience with AA and about one-third also regularly attend AA meetings (and have for an average of five years). Non-AA attending WFS members are more likely to believe that maintaining sobriety is a matter of hard work having little to do with God's intervention, while those attending AA attribute their sobriety to their spiritual program. Ninety-two percent of WFS members, however, believe their state of mind is the most important factor in maintaining sobriety.<ref name="KASKUTAS1996ESTEEM"/><ref name="KASKUTAS1992">{{cite journal |last=Kaskutas |first=Lee Ann |title=Beliefs on the source of sobriety: Interactions of membership in Women for Sobriety and Alcoholic Anonymous |journal=Contemporary Drug Problems |date=Winter 1992 |volume=63 |pages=638–648 |url=http://heinonline.org/HOL/Page?handle=hein.journals/condp19&g_sent=1&collection=journals&id=645}}</ref>

Although length of time in WFS correlated positively with self-esteem, length of time in AA did not emerge as a significant predictor of self-esteem.<ref name="KASKUTAS1996ESTEEM"/> Relapse was less common among women who attended both AA and WFS. The largest proportion of WFS members to achieve sobriety in a year following their turning point were those who attended AA in addition to seeking professional help compared to those who just attended AA or sought professional help.<ref name="KASKUTAS1996PATHWAYS"/> WFS members who attended AA reported they did so primarily as insurance against relapse (28%), its availability (25%), for sharing (31%) and support (27%). WFS members who did not attend AA mentioned feeling as though they never fit in at AA (20%), found AA too negative (18%), disliked drunkalogs (14%) disliked AA's focus on the past (14%), and felt that AA was geared more to men's needs (15%).<ref name="KASKUTAS1994">{{cite journal |title=What do women get out of self-help? their reasons for attending women for sobriety and alcoholics anonymous |last=Kaskutas |journal=Journal of Substance Abuse Treatment |volume=11 |issue=3 |date=June 1994 |pages=185–195 |doi=10.1016/0740-5472(94)90075-2 |first=Lee Ann |pmid=8072046}}</ref>

=== Attrition and prevalence ===
WFS and AA have similar drop-out rates of new members; in about four months about half of new WFS members drop-out.<ref name="KASKUTAS1992THESIS">{{cite book |title=An Analysis of "Women For Sobriety" |author=Kaskutas, Lee Ann |date=November 1992 |publisher=University of California |location=Berkeley, CA |oclc=28006988}} cited in {{cite journal |author=Kaskutas, Lee Ann |title=Predictors of Self Esteem Among Members of Women for Sobriety |journal= Addiction Research & Theory |volume=4 |issue=3 |year=1996 |pages=273–281 |doi=10.3109/16066359609005572}}</ref> WFS discourages lifetime membership and reliance on meetings to maintain sobriety. In this way, the size and number of WFS meetings remain static, but does not necessarily reflect a decline in the group's popularity. Forming a WFS meeting for some may be prohibitively difficult, in addition to obtaining certification and a year of sobriety, the leader has an ongoing responsibility for the meeting.<ref name="KASKUTAS1996ROAD"/>

== Criticism ==
The thirteen affirmations may be difficult for women to implement in day-to-day living, depending on their situations. For instance, women with limited financial resources many find it difficult to accept that "problems bother her only to the extent she allows them." Depressed women many find it difficult to make enthusiasm a daily exercise, or accept happiness as a habit to develop. Similarly positive thinking affirmations have been interpreted as asking women to deny their real feelings and inhibiting recovery. The applicability of the affirmations to female alcoholic's lives may limit the appeal of the WFS program.<ref name="KASKUTAS1996PATHWAYS"/>

Kirkpatrick has stated the affirmations were derived from observing her thoughts as she felt good enough to stop drinking. The intention of the affirmations is [[behavior modification]], asking WFS members not to dwell on past problems is intended to prevent them from drinking. The purpose is not to deny the past, but not to indulge it as this is likely to cause negative thinking. By practicing affirmations and positive thinking, it is believed WFS members slowly change their habits and their thoughts become reality.<ref name="KASKUTAS1996PATHWAYS"/>

==Literature==
WFS sells several dozen books on their website, and several dozen more booklets, CDs, and related materials<ref name="WFSCATALOG">{{cite journal |title=Welcome to Women For Sobriety, Inc. Catalog |author=Women For Sobriety |date=2008-03-25 |accessdate=2008-11-12 |url=http://www.wfscatalog.org}}</ref> and regularly publish a journal ''Sobering Thoughts''.<ref name="SOBERING">{{cite journal |title=Sobering Thoughts: A Publication of Women For Sobriety, Inc |publisher=Women For Sobriety, Inc |location=Quakertown, PA |issn=1071-4111 |oclc=25244472}}</ref> Four books by Kirkpatrick, however, are used principally in the WFS program, the most important of which is ''Turnabout''.<ref name="TONIGAN1998"/>

* {{cite book | last=Kirkpatrick |first=Jean |title=Turnabout: New Help for the Woman Alcoholic |isbn=1-56980-146-0 |oclc=42904483 |publisher=Barricade Books |location=New York |year=1999}}
* {{cite book |last=Kirkpatrick |first=Jean |title=Goodbye Hangovers, Hello Life |oclc=12550360 |isbn=0-689-11576-8|publisher=Atheneum |year=1986 |location=New York}}
* {{cite book |last=Kirkpatrick |first=Jean |title=On the Road to Sell Recovery |oclc=30498159 |isbn= |publisher=Women for Sobriety |year=1990 |location=Quakertown, PA}}
* {{cite book |last=Kirkpatrick |first=Jean |title=A Fresh Start |oclc=8708189 |isbn=0-8403-2463-4 |year=1981 |publisher=Kendall/Hunt Publish Company |location=Dubuque, Iowa}}

== Further reading ==
* {{cite thesis |title=Women For Sobriety: A Case Study |author=Kristie, Janet M. |year=1986 |publisher=California State University |location=Sacramento, CA |degree=M.S.W. |oclc=14773534}}
* {{cite thesis |title=An Analysis of "Women For Sobriety" |author=Kaskutas, Lee Ann |date=November 1992 |publisher=University of California |location=Berkeley, CA |degree=Dr. of Public Health |oclc=28006988}}
* {{cite book |title=The Effect of Relational Empowerment Within The Therapeutic Self-help Group of Women For Sobriety: Expanding Treatment Options For Women in Recovery: A Project Based Upon an Independent Investigation |author=Mulqueen, Kelly Marie |year=1992 |publisher=Smith College School for Social Work |location=Northampton, MA |oclc=26937414}}

== See also ==
{{Col-begin}}
{{Col-2}} 
*[[Addiction recovery groups]]
*[[Alcoholism]]
*[[Cognitive Behavior Therapy]]
*[[Drug addiction]]
{{Col-2}}
*[[LifeRing Secular Recovery]]
*[[Rational Recovery]]
*[[Secular Organizations for Sobriety]]
*[[SMART Recovery]]
{{Col-end}}

== References ==
{{Reflist|2}}

== External links ==
* [http://www.womenforsobriety.org/ Official website of Women for Sobriety]
* {{worldcat id|id=nc-women+for+sobriety}} and {{worldcat id|id=nc-women+for+sobriety+inc|name=Women For Sobriety, Inc}}

[[Category:Positive mental attitude]]
[[Category:Addiction and substance abuse organizations]]
[[Category:Organizations based in Pennsylvania]]
[[Category:Non-profit organizations based in Pennsylvania]]
[[Category:Temperance organizations in the United States]]
[[Category:Organizations established in 1976]]
[[Category:Support groups]]
[[Category:Personal development]]
[[Category:Psychosocial rehabilitation]]
[[Category:Self care]]
[[Category:Alcohol abuse]]
[[Category:Drug rehabilitation]]
[[Category:Women's clubs in the United States]]
[[Category:Women in Pennsylvania]]