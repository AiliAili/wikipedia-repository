{{good article}}
{{Infobox NCAA football yearly game
 |Game Name=Liberty Bowl
 |Date Game Played=December 14
 |Year Game Played=1968
 |Football Season=1968
|Image=[[Image:OldLiberty Bowl logo.jpg|150px]]
|Stadium=[[Memphis Memorial Stadium]]
|City=[[Memphis, Tennessee]]
|Visitor Name Short=Ole Miss
|Visitor Nickname=Rebels
|Home Name Short=Virginia Tech
|Home Nickname=Hokies
|Visitor Record=6&ndash;3&ndash;1
|Home Record=7&ndash;3
|Visitor Coach=[[Johnny Vaught|John Vaught]]
|Home Coach=[[Jerry Claiborne]]
|Visitor1=0
|Visitor2=14
|Visitor3=7
|Visitor4=13
|Home1=17
|Home2=0
|Home3=0
|Home4=0
|Visitor AP=
|Visitor Coaches=
|Visitor BCS=
|Home AP=
|Home Coaches=
|Home BCS=
|MVP=[[Running Back|RB]] [[Steve Hindman]] (Ole Miss)
|Odds=
|Anthem=
|Referee=
|Halftime=
|Attendance=46,206
|US Network=[[American Broadcasting Company|ABC]]
|US Announcers=
|Ratings=
|Intl Network=
|Intl Announcers=
}}

The '''1968 [[Liberty Bowl]]''' was a post-season American [[college football]] [[bowl game]] between the [[Virginia Tech Hokies football|Virginia Tech Hokies]] and the [[Ole Miss Rebels football|Ole Miss Rebels]] from the [[University of Mississippi]] at [[Memphis Memorial Stadium]] in [[Memphis, Tennessee]] on December 14, 1968. The game was the final contest of the [[1968 college football season]] for both teams, and ended in a 34-17 victory for Mississippi.

Two&nbsp;years after a previous trip to the Liberty Bowl, Virginia Tech was again asked to travel to Memphis to play in a post-season bowl game. This time, the opponent was [[Ole Miss Rebels football|Mississippi]], which had amassed a 6&ndash;3&ndash;1 record during the regular season. The Hokies came into the game with a 7&ndash;3 record that included a loss to Tech's previous Liberty Bowl opponent, Miami.

The 1968 Liberty Bowl kicked off on December 14, 1968. As in the Hokies' previous appearance in the Liberty Bowl, Virginia Tech got off to a fast start. On the game's second play, Tech ran 58&nbsp;yards for a touchdown, courtesy of a trick play. After Mississippi fumbled, Tech recovered and scored another quick touchdown. At the end of the first quarter, Tech added a field goal to the two touchdowns it had already earned, making the score 17&ndash;0 at the end of one quarter. From that point onward, however, almost nothing would go in Virginia Tech's favor. Tech attempted an [[onside kick]] following the field goal, but were unable to successfully recover the ball. With good field position following the kick, Mississippi quarterback Archie Manning orchestrated a 49-yard drive for the Rebels' first points of the game.

Mississippi scored another touchdown before halftime, and the Hokies clung to a 17&ndash;14 lead at the beginning of the second half. That three-point lead quickly evaporated, however, as 21&nbsp;seconds into the third quarter, Mississippi's Steve Hindman ran for 79&nbsp;yards and a touchdown to give Mississippi a 21&ndash;17 lead. Ole Miss added 13 more points before the game ended and earned the victory, 34&ndash;17.

== Pregame buildup ==

=== Virginia Tech ===

The Virginia Tech Hokies, led by head coach Jerry Claibourne,<ref name="coaches"/> amassed a 7&ndash;3 record during the regular season prior to the Liberty Bowl.<ref>[http://www.hokiesports.com/football/pastschedules.html?season=1968 Virginia Tech Football Past Schedules: 1968-1969] Virginia Tech Athletics Department, Hokiesports.com. Accessed February 20, 2008.</ref> The Hokies came into the 1968 season with high expectations. From 1963 to 1967, Virginia Tech was the 12th winningest major college football program, recording 36 wins, 13 losses, and one tie—putting the Hokies just behind Notre Dame in winning percentage.<ref>Lazenby, pgs. 106-107.</ref> During the regular season, [[linebacker]] [[Mike Widger]] emerged as a major threat on defense for Tech. In the Hokies' win over No. 18 [[Florida State Seminoles football|Florida State]] in 1968, Widger intercepted two passes. Shortly after Liberty Bowl committee chairman Bud Dudley selected the Hokies to play in the Liberty Bowl, Widger was named a first-team [[Associated Press]] [[All-America]]n, marking him as one of the best players at his position in the country.<ref name="Leg"/>

=== Mississippi ===

The Ole Miss Rebels, led by head coach John Vaught,<ref name="coaches">[http://www.libertybowl.org/navigation-content-coaches Coaches] Libertybowl.org, Accessed July 9, 2008.</ref> earned a regular-season record of 6&ndash;3&ndash;1 prior to the Liberty Bowl.<ref>[http://www.olemisssports.com//pdf7/83758.pdf?SPSID=82783&SPID=737&DB_OEM_ID=2600 2007 Ole Miss Media Guide] Mississippi Athletics Department, olemisssports.com. Page 179. Accessed February 21, 2008.</ref> Vaught remains the all-time leader in wins at Ole Miss, and at the time of Mississippi's selection for the 1968 Liberty Bowl, he had already led the Rebels to three national championships and six [[Southeastern Conference]] championships, the most recent of each coming in 1962.<ref>[http://www.olemisssports.com/ViewArticle.dbml?&&DB_OEM_ID=2600&ATCLID=622354&SPID=737&SPSID=57052 Remembering Coach Vaught: The Legend ... The Coach ... The Man] Mississippi Athletics Department, OleMisssports.com. October 2, 2006. Accessed July 10, 2008.</ref> On the field, Ole Miss featured star quarterback Archie Manning, who despite being just a sophomore, was already making a name for himself and would later go on to set records for passing yardage and passing touchdowns for Ole Miss, en route to becoming one of the greatest quarterbacks in Mississippi history.<ref>[http://www.olemisssports.com/ViewArticle.dbml?&DB_OEM_ID=2600&ATCLID=883744&SPID=737&SPSID=87067 History of Rebel Football] "Archie Manning," Mississippi Athletics Department, OleMisssports.com. Accessed July 10, 2008.</ref>

== Game summary ==

The 1968 Liberty bowl kicked off on a cold and blustery day in front of 46,206 fans at [[Memphis Memorial Stadium]] in [[Memphis, Tennessee|Memphis]], [[Tennessee]] on December 14, 1968. The record crowd (it was the largest in the 10-year history of the bowl to that point) consumed 20,000 [[hot dogs]]—so many that the stadium ran out by the third quarter.<ref name="Leg">Lazenby, p. 109.</ref>

In the first quarter of the game, it appeared that Virginia Tech would run away with an overwhelming victory. Tech received the ball to begin the game, and on the game's second play, Virginia Tech's Ken Edwards ran 58 yards on a trick play for the game's first [[touchdown]].<ref name="1968 Liberty">[http://www.hokiesports.com/football/bowls/1968Liberty.html 1968 Liberty Bowl] Virginia Tech Athletics Department, Hokiesports.com. Accessed February 21, 2008.</ref> Mississippi [[fumble]]d the ball on its first offensive play and Virginia Tech recovered the turnover. Three plays later, Tech scored another touchdown on a seven-yard run by Terry Smoot. After stopping the Rebels with their defense, Tech appeared to have another chance to score when the Hokies' Ron Davidson returned an Ole Miss punt to the Rebels' 42-yard line. Despite the excellent field position, Tech was unable to score after Hokie quarterback Al Kinkaid was [[quarterback sack|sacked]] for a 19-yard loss.<ref name="HG"/> Despite the setback, Tech was able to keep Ole Miss from scoring in the first quarter and tacked on a 29-yard field goal by kicker Jack Simcsak before time came to an end in the quarter. At the end of the quarter, Tech led 17&ndash;0.<ref name="1968 Liberty"/>

In the second quarter, Mississippi came storming back. Following the field goal, Tech head coach Jerry Claiborne ordered an [[onside kick]] in an effort to gain another chance on offense and potentially build an insurmountable lead.<ref name="Leg"/> Onside kicks, unlike an ordinary free kick, can be recovered by the kicking team—but only after the ball has traveled 10 yards. If the kicking team touches the ball before it has traveled 10 yards, the receiving team takes possession at the place the ball was touched.<ref>[http://www.ncaa.org/library/rules/2007/2007_football_rules.pdf 2007 NCAA Football rulebook] The NCAA, NCAA.org. Accessed July 9, 2008.</ref> Unfortunately for Virginia Tech, Simsak's onside kick—though it surprised Ole Miss—did not travel the requisite 10 yards. Mississippi took over at the Virginia Tech 49-yard line, and many of the Ole Miss players were angered by what they considered to be an insult.<ref name="Leg"/>

Mississippi quarterback Archie Manning used the good field position and his inspired offense proficiently following the kick, driving the Rebels down the field and connecting with Hank Shows on a 21-yard touchdown pass for Mississippi's first points just 30 seconds into the second quarter. By [[halftime]], Manning had connected on another touchdown pass, this one a 23-yard strike to Leon Felts. Tech still held a 17&ndash;14 lead, but Mississippi had the momentum and would receive the ball to begin the second half.<ref name="1968 Liberty"/>

Ole Miss wasted no time in scoring, as Steve Hindman turned the first play of the second half into a 79-yard for a touchdown.<ref name="HG">Tandler, p. 112</ref> The Rebels now held a 21&ndash;17 lead, and Virginia Tech never threatened afterward.<ref>[http://www.olemisssports.com//pdf7/83758.pdf?SPSID=82783&SPID=737&DB_OEM_ID=2600 2007 Ole Miss Media Guide] Mississippi Athletics Department, olemisssports.com. Page 169. Accessed February 21, 2008.</ref> Ole Miss defender Bob Bailey [[interception|intercepted]] a Virginia Tech pass at the beginning of the fourth quarter and returned it 70 yards for another Mississippi touchdown. The Rebels also added a pair of field goals to their score, and rolled to a 34&ndash;17 victory over Virginia Tech.<ref name="1968 Liberty"/>

== Statistical summary ==

<div style="margin-left: 10px; margin-bottom: 10px; float: right;">
{| class="wikitable"
|+ '''Statistical Comparison'''<ref name="1968 Liberty"/>
!  !! UM !! VT
|-
| '''1st Downs''' || 7 || 14
|-
| '''Total Yards''' || 326 || 332
|-
| '''Passing Yards''' || 141 || 2
|-
| '''Rushing Yards''' || 185 || 330
|-
| '''Penalties''' || 4-30 || 12-120
|-
| '''Turnovers''' || 3 || 5
|}
</div>

Mississippi's Steve Hindman was named the game's [[most valuable player]]<ref>[http://www.libertybowl.org/navigation-content-mvp Most Valuable Players] Libertybowl.org, Accessed July 9, 2008.</ref> after he rushed the ball for 121 yards and a touchdown in addition to catching three passes for 32 yards.<ref name="1968 Liberty"/> Ole Miss quarterback Archie Manning finished the game with 141 passing yards and two touchdowns.<ref name="Leg"/> Virginia Tech was led on offense by Ken Edwards, who finished with 119 rushing yards and a touchdown, and Terry Smoot, who finished with 91 rushing yards and a touchdown.<ref name="1968 Liberty"/> Tech turned the ball over five times, committing three fumbles and throwing two interceptions to the Mississippi defense.<ref name="Leg"/> Despite Tech's relative offensive success on the ground, the Hokies were completely inept through the air, completing just one pass for two net passing yards. Both marks remain Liberty Bowl records for the fewest passes and fewest passing yards gained by a team.<ref>[http://www.libertybowl.org/navigation-content-teamrecords Team Records] Libertybowl.org, Accessed July 9, 2008.</ref>

Although the Hokies had success on defense during the first quarter, Mississippi dominated the Hokies defensively for the rest of the game. Such was the defensive dominance of Mississippi in the last three quarters of the game that after Tech's field goal in the first quarter, the Hokies had 11 offensive possessions and crossed into Ole Miss territory just once.<ref name="HG"/>

In addition to Steve Hindman's selection as the game's MVP, Mississippi had two other players recognized for their achievements in the game: Robert Bailey was named the game's most outstanding [[defensive back]] and [[offensive tackle]] Worthy McClure was named the game's most outstanding [[offensive line]]man.<ref>[http://www.olemisssports.com/ViewArticle.dbml?SPSID=87067&SPID=737&DB_OEM_ID=2600&ATCLID=1134765 Rebel Football Bowl History] "Outstanding Player Awards," Mississippi Athletic Department, OleMissSports.com. Accessed July 10, 2008.</ref>

== See also ==

*[[Glossary of American football]]

== Notes ==
{{reflist|2}}

== References ==

* Lazenby, Roland. Legends: A Pictoral History of Virginia Tech Football. Taylor, Full Court Press (1986) ISBN 978-0-913767-11-5
* Tandler, Rich. Hokie Games: Virginia Tech Football Game by Game 1945-2006. Game by Game Sports Media (September 15, 2007) ISBN 978-0-9723845-2-0

{{Liberty Bowl navbox}}
{{Ole Miss Rebels bowl game navbox}}
{{Virginia Tech Hokies bowl game navbox}}

[[Category:Liberty Bowl]]
[[Category:Ole Miss Rebels football bowl games]]
[[Category:Virginia Tech Hokies football bowl games]]
[[Category:1968–69 NCAA football bowl games|Liberty Bowl]]
[[Category:1968 in Tennessee]]