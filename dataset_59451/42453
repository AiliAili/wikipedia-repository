<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Breguet G.11E
 | image=Breguet G111 Musee de l'Espace P1020331.JPG
 | caption=Breguet G.111 in [[Museum of Air and Space]]
}}{{Infobox Aircraft Type
 | type=Experimental twin rotor [[helicopter]]
 | national origin=[[France]]
 | manufacturer=[[Breguet Aviation|Breguet]]
 | designer=[[Louis Charles Breguet]]
 | first flight=21 May 1949
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Breguet G.11E''' was a [[France|French]] passenger [[coaxial rotor]] [[helicopter]] flown soon after [[World War II]]. Only one was built, development ceasing when funding ran out.

==Design and development==
Louis Breguet designed his first helicopter, the [[Breguet-Richet Gyroplane]], in 1908 but his 1935 [[Gyroplane Laboratoire]] was much more successful. It had no [[tail rotor]] but instead had co-axial contra-rotating [[helicopter rotor|rotors]]. After [[World War II]] Breguet was approached by the ''Société Francaises du Gyroplane'' (SFG, {{lang-en|French Gyroplane Society}}) for a helicopter capable of carrying several passengers.  Breguet developed his wartime studies of a project named the '''G.34''' into the two-passenger Breguet G.11E, otherwise known as the '''Société Francaises du Gyroplane G.11E'''.<ref name=GaillardI/>

Though a much larger aircraft, the G.11E used the same coaxial, three blade twin rotor layout as on the Gyroplane Laboratoire. It was initially powered by a fan cooled {{convert|240|hp|kW|abbr=on|0|disp=flip}} [[Potez 9E]] nine cylinder [[radial engine]] mounted amidships, under the concentric rotor shafts.  There was 6.5:1 speed reduction gearing between the engine and the rotor drive.<ref name=GaillardI/><ref name=Flight/> The rotors are built around tapered tube [[spar (aviation)|spar]]s, which carry [[rib (aircraft)|rib]]s and are [[Dural]] clad at the [[leading edge]]s and with alloy over [[plywood|3-ply]] elsewhere. They are mounted on [[helicopter rotor|flapping hinges]] and have [[helicopter rotor|drag hinge]] dampers. The control column alters [[cyclic pitch]] via a pair of [[swashplate (aeronautics)|swashplate]]s and pedals make [[torque]] corrections and control [[Flight dynamics|yaw]] by changing the relative [[collective pitch]] of the two rotors. A mechanical [[inertial]] [[governor]] limited rotor accelerations; the pilot could increase the collective pitch over that set by the governor but not below it, emergencies apart.<ref name=Flight/>

The G.11E's [[fuselage]] has a tapered, oval section. The forward part is a light alloy [[monocoque]] containing the well glazed [[cockpit]], accessed by two sliding doors. The rear fuselage is a steel tube structure, covered in [[aircraft fabric covering|fabric]], bearing a tall [[T-tail]] with a moving, one-piece [[tailplane]] which corrected the cyclic pitch via the control column to prevent once per revolution [[Flight dynamics|pitch]] oscillations. A wide track undercarriage has main wheels mounted on horizontal V-struts from the fuselage bottom and with a single bracing strut to the mid-fuselage on each side.<ref name=GaillardI/><ref name=Flight/>

The first flight was made on 21 May 1949 but tests showed that the G.11E was underpowered, so a decision was made to replace the Potez engine with a bigger nine-cylinder radial, a {{convert|450|hp|kW|abbr=on|0|disp=flip}} [[Pratt & Whitney Wasp Junior]]. The type name was changed to '''G.111''' and some re-design accompanied the power increase; the rotor diameter was increased by {{convert|1.00|m|ftin|abbr=on|0}} and the fuselage lengthened by {{convert|480|mm|ftin|abbr=on|0}} to include two more seats so that four passengers could be carried.  Empty and maximum weights increased to {{convert|1476|kg|lb|abbr=on|0}} and {{convert|1476|kg|lb|abbr=on|0}} respectively.<ref name=GaillardI/>

The G.111 began flight tests in 1951 but these were not completed as SFG were declared bankrupt the following year.

==Variants==
;G.11E:Two passengers, Potez 9E engine.
;G.111: Four passengers, G.11E re-engined with Wasp Junior, enlarged.

==Specifications (G.11E) ==
{{Aircraft specs
|ref=''Gaillard (1991) p.91''.<ref name=GaillardI/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One pilot
|capacity=Two passengers 
|length m=9.20
|length note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=4.05
|height note=
|empty weight kg=850
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=1300
|fuel capacity={{convert|120|l|Impgal USgal|abbr=on|1}}<ref name=Flight/>
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Potez 9E]]
|eng1 type=fan cooled<ref name=Flight/> 9-cylinder [[radial engine|radial]]
|eng1 hp=240
|eng1 note=<ref name=GaillardI/><ref name=Flight/>
|power original=
|more power=

|rot number=2
|rot dia m=8.60
|rot area sqm=58.0
|rot area note=each. 3 blade rotors.
<!--
        Performance
-->
|perfhide=

|max speed kmh=240
|max speed note=
|cruise speed kmh=185
|cruise speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=4000
|g limits=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
}}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft or engine: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of rotorcraft]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Commons category|Breguet G-111}}
{{reflist|refs=

<ref name=GaillardI>{{cite book |title=Les Avions Francais de 1944 à 1964|last=Gaillard|first=Pierre|year=1990|publisher=Éditions EPA|location=Paris|isbn=2 85120 350 9|page=91}}</ref>

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |coauthors= |date=9 March 1950 |title= Helicopter development in France:Breguet G11E|magazine= [[Flight International|Flight]]|volume=LVII |issue=2150 |page=309|url= http://www.flightglobal.com/pdfarchive/view/1950/1950%20-%200469.html  }}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->
{{Breguet aircraft}}

[[Category:Coaxial rotor helicopters]]
[[Category:French helicopters 1940–1949]]
[[Category:Breguet aircraft|G.11E]]