{{italic title}}
[[File:Country of the Pointed Firs cover 1896.jpg|thumb|''The Country of the Pointed Firs'' by Sarah Orne Jewett, 1896]]
'''''The Country of the Pointed Firs''''' is an 1896 short story sequence by [[Sarah Orne Jewett]] which is considered by some [[literary criticism|literary critics]] to be her finest work. [[Henry James]] described it as her "beautiful little quantum of achievement." [[Ursula K. Le Guin]] praises its "quietly powerful rhythms."<ref>Ursula Le Guin. ''[[Steering the Craft]]'', p. 16 (First Mariner Books, 2015)</ref> Because it is loosely structured, many critics view the book not as a novel, but a series of sketches; however, its structure is unified through both [[setting (fiction)|setting]] and [[theme (literature)|theme]]. The novel can be read as a study of the effects of [[isolation (psychology)|isolation]] and hardship experienced by the inhabitants of the decaying [[fishing]] [[village]]s along the [[Maine]] coast.

Sarah Orne Jewett, who wrote the book when she was 47, was largely responsible for popularizing the [[American literary regionalism|regionalism]] genre with her sketches of the fictional Maine fishing village of Dunnet Landing.  Like Jewett, the narrator is a woman, a writer, unattached, genteel in demeanor, intermittently feisty and zealously protective of her time to write.  The narrator removes herself from her landlady's company and writes in an empty schoolhouse, but she also continues to spend a great deal of time with Mrs. Todd, befriending her hostess and her hostess's family and friends.

==Publication==
The Country of the Pointed Firs was serialized in the January, March, July, and September 1896 issues of [[The Atlantic Monthly]]. Sarah Orne Jewett subsequently expanded and revised the text and added titles for the chapters. The novel was then published in book form in Boston and New York by Houghton, Mifflin and Company in November 1896.

==Plot==

The narrator, a Bostonian, returns after a brief visit a few summers prior, to the small coastal town of Dunnet, Maine, in order to finish writing her book. Upon arriving she settles in with Almira Todd, a widow in her sixties and the local apothecary and herbalist. The narrator occasionally assists Mrs. Todd with her frequent callers, but this distracts her from her writing and she seeks a room of her own.

Renting an empty schoolhouse with a broad view of Dunnet Landing, the narrator can apparently concentrate on her writing, although Jewett does not use the schoolhouse to show the narrator at work but rather in meditation and receiving company.  The schoolhouse is one of many locations in the novel which Jewett elevates to mythic significance and for the narrator the location is a center of writerly consciousness from which she makes journeys out and to which others make journeys in, aware of the force of the narrator's presence, out of curiosity, and out of respect for Almira Todd.

After a funeral, Captain Littlepage, an 80-year-old retired sailor, comes to the schoolhouse to visit the narrator because he knows Mrs. Todd. He tells a story about his time on the sea and she is noticeably bored so he begins to leave. She sees that she has offended him with her display of boredom, so she covers her tracks by asking him to tell her more of his story. The Captain's story cannot compare to the stories that Mrs. Todd, Mrs. Todd's brother and mother, and residents of Dunnet tell of their lives in Dunnet. The narrator's friendship with Mrs. Todd strengthens over the course of the summer, and the narrator's appreciation of the Maine coastal town increases each day.

==Characters==

'''Mrs. Almira (Almiry) Todd'''

Initial appearance: Chapter 2; "Mrs. Todd" p.&nbsp;6 – first talks to the narrator
"Well, dear, I took great advantage o' your bein' here. I ain't had such a season for years, but I have never had nobody I could so trust."

Final appearance: Chapter 24; "The Backward View" p.&nbsp;128–129 –  when the narrator is leaving
"I lost sight of her as she slowly crossed an open space on one of the higher points of land, and disappeared again behind a dark clump of juniper and the pointed firs.

'''Captain Littlepage'''

Initial appearance: Chapter 4; "At the Schoolhouse Window" p.&nbsp;12 – in the midst of a funeral procession
"...I recognized the one strange and unrelated person in all the company, an old man who had always been mysterious to me."

Final appearance: Chapter 10 "The Great Expedition" p.&nbsp;89 – sitting behind his closed window
"There was a patient look on the old man's face, as if the world were a great mistake and he had nobody with whom to speak his own language or find companionship."

'''Mrs. Blackett'''

Initial appearance: Chapter 8 "Green Island" p.&nbsp;35 – on land
"I looked and could see a tiny flutter in the doorway, but a quicker signal had made its way from the heart on shore to the heart on sea."

'''Abby Martin'''<br>
Abby Martin is the title character of the section titled ''The Queen's Twin'', a Maine woman who is convinced that she and Queen Victoria are twins.<ref>Nineteenth-century Literature −2005  Volume 60 p 374 ""The Queen's Twin" is the title of Jewett's most unusual section in her masterpiece, The Country of the Pointed Firs, and the moniker refers to Mis' Abby Martin, a Maine woman who is convinced that she and Queen Victoria are twins, despite ..."</ref><ref>Valerie Rohy ''Anachronism and Its Others: Sexuality, Race, Temporality'' 2009 -p 58  "“The Queen's Twin” is thus a meditation on various forms of anachronism, in which Abby's feeling for the queen appears as a form of national nostalgia, a fall back to the United States' English affiliation, and a sign of sexual regression to an ..."</ref><ref>Charmion Gustke '' Recognition, Resistance and Empire in the Frontier Fiction''  2008 p.49 "Examining the ways in which ideals of empire are employed to camouflage the struggles of poverty and the isolation of rural life, “The Queen's Twin” is the story of the narrator's introduction to Abby Martin. "</ref>

==References==
{{reflist}}

==External links==
{{Gutenberg|no=367|name=The Country of the Pointed Firs}}
*[http://www.broadviewpress.com/product.php?productid=975&cat=0&page=1 Scholarly Edition published November 15, 2009] by [[Broadview Press]]
* {{librivox book | title=The Country of the Pointed Firs | author=Sarah Orne JEWETT}}

{{DEFAULTSORT:Country Of The Pointed Firs, The}}
[[Category:1896 short story collections|Country]]
[[Category:19th-century American novels|Country]]
[[Category:Maine in fiction|Country]]
[[Category:American short story collections|Country]]
[[Category:Short story collections by Sarah Orne Jewett|Country]]