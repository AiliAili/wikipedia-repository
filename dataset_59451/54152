{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox officeholder
|name        = John Colton
|image       = John colton.jpg
|order       = 1st [[Leader of the Opposition (South Australia)|Leader of the Opposition]] (SA)
|term_start  = 1884
|term_end    = 1884
|successor   = [[John Cox Bray]]
|birth_date  = {{birth date|1823|9|23|df=y}}
|death_date  = {{death date and age|1902|2|6|1823|9|23|df=y}}
|religion = Christian (Wesleyan Methodist)
|resting_place = [[West Terrace Cemetery]]<ref name="funeral">{{cite news |url=http://nla.gov.au/nla.news-article87818256 |title=THE LATE SIR JOHN COLTON. |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |location=Adelaide |date=15 February 1902 |accessdate=9 January 2016 |page=33 |via=National Library of Australia}}</ref>
|}}

'''Sir John Colton''' [[Order of St Michael and St George|KCMG]] (23 September 1823 – 6 February 1902) was an Australian politician, [[Premier of South Australia]] and philanthropist.<ref>
{{Australian Dictionary of Biography
 |first=S. R. |last=Parr
 |title=Colton, Sir John (1823–1902)
 |id2=colton-sir-john-3247
 |accessdate=20 January 2014
}}</ref>

==Life==
Colton, the son of William Colton, a farmer, was born in [[Devon]]shire, England. He arrived in [[South Australia]] in 1839 with his parents, who went on the land. Colton, however, found work in [[Adelaide]], and at the age of 19, began business for himself as a saddler. He was shrewd, honest and hard-working, and his small shop eventually developed into a large and prosperous wholesale ironmongery and saddlery business. He gave £100 to start the work on the [[Pilgrim Uniting Church|Pirie Street Wesleyan Church]]<ref>{{cite news |url=http://nla.gov.au/nla.news-article44917395 |title=Odd Aspects Of City Church's Centenary. |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=8 July 1950 |accessdate=9 January 2016 |page=2 |via=National Library of Australia}}</ref> where he was an active member for over 50 years.<ref name="funeral"/> In 1859 Colton was elected a member of the [[Adelaide City Council]], and on 17 November 1862 was returned to the [[South Australian House of Assembly]] for [[Electoral district of Noarlunga|Noarlunga]],<ref>
{{cite SA-parl
 |pid=3631
 |name=John Colton
}}</ref> at the head of the poll.

On 3 November 1868 he became commissioner of public works in the [[Henry Strangways|Strangways]]  ministry, but when this cabinet was reconstructed in May 1870 he was omitted. He was [[Mayor of Adelaide]] 1874-5, and on 3 June 1875 joined the second [[James Boucaut|Boucaut]] ministry as [[Treasurer of South Australia]], but he resigned in March 1876. On 6 June he formed his first ministry as premier and commissioner of public works. His ministry lasted until 26 October 1877, when it resigned after a constitutional struggle with the upper house, which had not been consulted about the new parliamentary buildings. The government, however, had succeeded in passing a liberalized crown lands consolidation bill, and a forward policy of public works in connexion with railways and water supply had been carried out.

Colton might have been premier again in June 1881, but stood aside in favour of [[John Cox Bray|Bray]]. On 16 June 1884 he became premier and chief secretary in his second ministry, which in the following twelve months passed some very useful legislation, including a public health act, an agricultural crown land act, a pastoral land act, a vermin destruction act and a land and income tax act. The ministry was defeated on 16 June 1885. Seldom had a ministry done so much in so short a time, but Colton was prostrated by overwork and was compelled to live in retirement for some months. On his return to parliament he attempted to lead the opposition, but an attack of paralysis finished his political career and he resigned from parliament in January 1887.

Colton paid a visit to England and regained some of his health. Henceforth, he gave much of his time to philanthropic work. It was said of him that no society or charitable institution ever appealed to him in vain for either financial or personal assistance, if they could show that their aims were worthy. He took a great interest in [[Prince Alfred College]], and was its Treasurer for many years, and was for a time chairman of the board of management of the Adelaide hospital. He was a great advocate for temperance and retained his interest in the [[Methodist]] Church throughout his life.

He was created {{post-nominals|country=GBR|size=100%|KCMG}} on 1 January 1891.
He died in [[Adelaide]] on 6 February 1902.<ref name="dnb">{{cite DNB12|wstitle=Colton, John |first=Chewton|last= Atchley}}</ref>

==Family==
On 4 December 1844, he married Mary, daughter of Samuel Cutting of London.
He was survived by four sons and a daughter.<ref name="dnb"/>

==References==
{{Reflist}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[Philip Santo]]}}
{{s-ttl|title=[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]]|years=1868{{spaced ndash}}1870}}
{{s-aft|after=[[Friedrich Krichauff]]}}
{{s-bef | before = [[William Dixon Allot]]}}
{{s-ttl | title = [[List of mayors and lord mayors of Adelaide|Mayor of the Corporation of Adelaide]] | years =1874{{spaced ndash}}1875}}
{{s-aft|after=[[Caleb Peacock]]}}
|-
{{s-bef | rows=2 | before = [[James Boucaut]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1876{{spaced ndash}}1877}}
{{s-aft|after=[[James Boucaut]]}}
|-
{{s-ttl|title=[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]]|years=1876{{spaced ndash}}1877}}
{{s-aft|after=[[George Charles Hawker|George Hawker]]}}
|-
{{s-new}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition of South Australia]]|years=1884}}
{{s-aft|after=[[John Cox Bray|John Bray]]}}
|-
{{s-bef | before =[[John Cox Bray|John Bray]] }}
{{s-ttl | title = [[Premier of South Australia]] | years =1884{{spaced ndash}}1885}}
{{s-aft|after=[[John Downer]]}}
|-
{{s-bef | before =[[James Garden Ramsay|James Ramsay]]}}
{{s-ttl|title=[[Chief Secretary of South Australia]]|years=1884{{spaced ndash}}1885}}
{{s-aft|after=[[John Brodie Spence|John Spence]]}}
|-
{{s-par|au-sa-la}}
{{s-bef|before=[[Alexander Anderson (Australian politician)|Alexander Anderson]]}}
{{s-ttl|title=[[Member of parliament#Australia|Member of Parliament]] for [[Electoral district of Noarlunga|Noarlunga]]|years=1862&nbsp;– 1870 | alongside=Charles Hewett, [[John Carr (Australian politician)|John Carr]]}}
{{s-aft|after=[[James Stewart (South Australian politician)|James Stewart]]}}
|-
{{s-bef|before=[[Charles Myles]]}}
{{s-ttl|title=[[Member of parliament#Australia|Member of Parliament]] for [[Electoral district of Noarlunga|Noarlunga]]|years=1875&nbsp;– 1878 | alongside=[[John Carr (Australian politician)|John Carr]]}}
{{s-aft|after=[[Thomas Atkinson (Australian politician)|Thomas Atkinson]]}}
|-
{{s-bef|before=[[John Carr (Australian politician)|John Carr]]}}
{{s-ttl|title=[[Member of parliament#Australia|Member of Parliament]] for [[Electoral district of Noarlunga|Noarlunga]]|years=1880&nbsp;– 1887 | alongside=[[Thomas Atkinson (Australian politician)|Thomas Atkinson]]}}
{{s-aft|after=[[Charles Dashwood (judge)|Charles Dashwood]]}}
{{end}}

{{Premiers of South Australia}}

{{DEFAULTSORT:Colton, John}}
[[Category:1823 births]]
[[Category:1902 deaths]]
[[Category:People from Devon]]
[[Category:English emigrants to Australia]]
[[Category:Mayors and Lord Mayors of Adelaide]]
[[Category:Premiers of South Australia]]
[[Category:Australian Methodists]]
[[Category:Australian Knights Commander of the Order of St Michael and St George]]
[[Category:Members of the South Australian House of Assembly]]
[[Category:Leaders of the Opposition in South Australia]]
[[Category:Treasurers of South Australia]]
[[Category:Burials at West Terrace Cemetery]]
[[Category:19th-century Australian politicians]]