<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= B6N 
 |image= File:B6N in flight.jpg{{!}}border
 |caption= B6N2 in flight
}}{{Infobox Aircraft Type
 |type= [[Torpedo bomber]]
 |national origin = [[Japan]]
 |manufacturer= [[Nakajima Aircraft Company]] 
 |designer=
 |first flight= 14 March 1941
 |introduced=August 1943
 |retired= 1945
 |status=
 |primary user= [[Imperial Japanese Navy]]
 |more users=
 |number built=1,268
 |unit cost=
 |variants with their own articles=
}}
|}

The '''Nakajima B6N ''Tenzan''''' ([[Japanese language|Japanese]]: '''中島 B6N 天山'''—"Heavenly Mountain", [[World War II Allied names for Japanese aircraft|Allied reporting name]]: "'''Jill'''") was the [[Imperial Japanese Navy]]'s standard [[Aircraft carrier|carrier]]-borne [[torpedo bomber]] during the final years of [[World War II]] and the successor to the [[Nakajima B5N|B5N "Kate"]]. Due to its protracted development, a shortage of experienced pilots and the [[United States Navy]]'s achievement of air superiority by the time of its introduction, the B6N was never able to fully demonstrate its combat potential.

==Design and development==
[[File:B6N2 mec turning prop.jpg|thumb|right|A B6N2 before starting the engine.]]
The B5N carrier torpedo-bomber's weaknesses had shown themselves early in the [[Second Sino-Japanese War]] and, as well as updating that aircraft, the Imperial Japanese Navy began seeking a faster longer-ranged replacement. In December 1939 it issued a specification to Nakajima for a Navy Experimental ''14-Shi'' Carrier Attack Aircraft capable of carrying the same external weapons load as the B5N. The new plane was to carry a crew of three (pilot, navigator/bombardier and radio operator/gunner) and be of low wing, cantilevered, all-metal construction (though control surfaces were fabric-covered). Further requirements included a top speed of {{convert|250|kn|km/h mph}}, a cruising speed of {{convert|200|kn|km/h mph}} and a range of {{convert|1000|nmi|abbr=on}} with an {{convert|800|kg|abbr=on}} bomb load or {{convert|2072|nmi|abbr=on}} without external armament.<ref name="Wieliczko2003p4">Wieliczko 2003, p. 4.</ref>

The Navy had requested installation of the proven [[Mitsubishi Kasei|Mitsubishi ''Kasei'']] engine as the B6N's powerplant but Engineer Kenichi Matsumara insisted on using Nakajima's new {{convert|1870|hp|abbr=on}} [[Nakajima Mamoru|''Mamori'']] 11 14-cylinder air-cooled radial due to its lower fuel consumption and greater adaptability. This became an unfortunate choice as the ''Mamori'' engine was plagued with mechanical defects and never achieved its expected power rating.<ref name="Francillon1979p429">Francillon 1979, p. 429.</ref>

Constrained by the standard-sized aircraft elevators then in use on most Japanese carriers, designer Matsumara was obliged to use a wing similar in span and area as that of the B5N and to limit the aircraft's overall length to {{convert|11|m|ft|abbr=on}}. This latter restriction accounted for the B6N's distinctive swept-forward tail fin and rudder.[http://www.j-aircraft.com/walk/john_ferguson/jill1stbrd.jpg] The outer wing panels folded upward hydraulically, reducing the B6N's overall span from {{convert|14.9|m|ft|abbr=on}} to approximately {{convert|6.3|m|ft|abbr=on}} for minimal carrier stowage. In order to lessen increased wingloading due to the heavier powerplant, [[Fowler flaps]] were installed which could be extended beyond the wing's trailing edge. These were normally lowered to an angle of 20 degrees during take-off and 38 degrees when landing. Despite the use of these flaps, however, the B6N had a much higher stall speed than its predecessor.<ref name="Wieliczko2003p4">Wieliczko 2003, p. 4.</ref>

The prototype B6N1 made its maiden flight on 14 March 1941. Following continued testing, however, several problems became evident. In particular, the aircraft exhibited an alarming tendency to roll while in flight, the cause of which was traced to the extreme torque developed by the four-bladed propeller. To compensate, the aircraft's tail fin was thinned down and moved 2 degrees ten minutes to port. This modification greatly improved the plane's handling characteristics.<ref name="Francillon1979p431">Francillon 1979, p. 431.</ref>

The B6N1's ''Mamori'' 11 engine was found prone to severe vibrations and overheating at certain speeds and was at first judged too unreliable (an important consideration given that the plane was expected to fly long distances over open water). Following a series of modifications, though, the engine's performance was finally deemed promising enough that carrier acceptance trials were begun at the end of 1942. Subsequent test flights conducted aboard the carriers ''Ryuho'' and ''Zuikaku'' indicated the need to strengthen the tail hook mounting on the plane's fuselage. Some attempts were also made to use RATOG (rocket-assisted take-off gear) units on several B6N1s in order to qualify the aircraft for use on smaller carriers but the results were unsatisfactory.<ref name="Francillon1979p430-31">Francillon 1979, p. 430-31.</ref>

The B6N1 was officially approved for production status in early 1943 and given the designation Navy Carrier Attack Aircraft ''Tenzan'' Model 11. Modifications based on testing of the initial prototypes included: the addition of a flexible [[Type 92 machine gun]] in a ventral tunnel at the rear of the cockpit (in addition to the standard rear-firing Type 92), and a 7.7mm [[Type 97 aircraft machine gun|Type 97 machine-gun]] to the port wing (the latter was eventually deleted after the seventieth production aircraft); angling the torpedo mounting rack 2 degrees downward and adding torpedo stabilization plates to prevent the torpedo from bouncing during low-altitude release; strengthening of the main landing gear.<ref name="Wieliczko2003p5-6">Wieliczko 2003, p. 5-6.</ref> A proposal by the designers to replace the B6N1's unprotected fuel tanks with self-sealing ones would have resulted in a 30% drop in fuel capacity, a loss in range the Navy decided was unacceptable.<ref name="Wieliczko2003p7">Wieliczko 2003, p. 7.</ref>

After only 133 B6N1s had been produced by July 1943, the Japanese Ministry of Munitions ordered Nakajima to halt manufacture of the ''Mamori'' 11 engine in order that the Navy reduce the number of different engines then in use. Pending availability of the 18-cylinder [[Nakajima Homare|Nakajima ''Homare'']] engine, Nakajima was asked to substitute the {{convert|1850|hp|abbr=on}} [[Mitsubishi Kasei|Mitsubishi MK4T ''Kasei'' 25]] engine on the B6N1 airframe, the very engine the Navy had originally requested them to use. As the ''Mamori'' 11 and ''Kasei'' 25 were similar in size, installation was relatively straightforward, requiring only that the nose be extended to maintain the aircraft's center of gravity and minor alterations to the oil cooler and air intakes on the engine cowling. A smaller {{convert|3.4|m|ft|abbr=on}} diameter four-bladed propeller and shorter spinner were also installed at this time, resulting in a small weight-savings, and the retractable tailwheel was fixed permanently in the down position. Finally, the single exhaust stacks on either side of the engine cowling were replaced with multiple smaller stubs to reduce glare at night and to supply a minor amount of forward thrust. The resulting modification was designated Navy Carrier Attack Aircraft ''Tenzan'' Model 12 or B6N2.<ref name="Wieliczko2003p7-8">Wieliczko 2003, p. 7-8.</ref>

Starting in the fall of 1943, one of every three B6N2s manufactured was equipped with 3-Shiki Type 3 air-to-surface radar for detecting enemy ships. Yagi antennas were installed along the wing leading edges and also protruded from the sides of the rear fuselage.<ref name="Wieliczko2003p9">Wieliczko 2003, p. 9.</ref>

A final version of the aircraft, designated B6N3 Model 13, was planned for land-based use as, by this point in the war, all of Japan's large carriers had been sunk and those few smaller ones remaining lacked catapults for launching heavier carrier-borne aircraft like the B6N. Changes included installation of a ''Kasei'' Model 25c engine, a more streamlined engine cowling and crew canopy, strengthening of the main landing gear, a retractable tail wheel and removal of the tail hook. Two B6N3 prototypes were completed but Japan surrendered before this variant could be put into production.<ref name="Wieliczko2003p9">Wieliczko 2003, p. 9.</ref>

By war's end in August 1945, Nakajima had completed a total of 1,268 B6Ns (almost all of them B6N2s) at its plants in Okawa in the Gumma district and at Aichi in the Handa district. Production never exceeded more than 90 planes per month.<ref name="Wieliczko2003p9">Wieliczko 2003, p. 9.</ref>

==Operational history==
[[File:B6N Yorktown.jpg|thumb|A B6N explodes after direct hit by 5-inch shell from [[USS Yorktown (CV-10)|USS Yorktown]] as it attempts an unsuccessful attack on the carrier off [[Kwajalein]] on 4 December 1943]]
The B6N ''Tenzan'' began reaching front-line units in August 1943 but only in small numbers. The intent was to gradually replace all of the older B5N ''Kate'' torpedo planes then operating aboard the carriers of the Third Fleet at Truk Atoll in the Caroline Islands. However, the B6Ns were prematurely committed to battle when increased Allied naval activity in the Solomon Islands indicated a likely invasion at Bougainville. In response to this threat, the IJN initiated Operation ''Ro''. This involved reinforcing land-based air units at Rabaul with 173 carrier aircraft from First Carrier Division (''Zuikaku'', ''Shokaku'' and  ''Zuiho''), including forty B6Ns. These aircraft were flown from Truk to Rabaul between 28 October and 1 November. On 5 November fourteen B6N1s, escorted by four Zero fighters, were sent to attack American shipping anchored off Bougainville. Four B6N1s were lost and no actual hits were scored, yet returning Japanese pilots claimed to have sunk one large and one medium carrier, two large cruisers and two other cruisers or large destroyers. This was considered an unusually auspicious debut for the new warplane.<ref name="Wieliczko2003p10-11">Wieliczko 2003, p. 10-11.</ref>

Additional attacks mounted on 8 November and 11 November yielded less sanguine results and suffered heavy losses in return. Only 52 of the original 173 planes from First Carrier Division made it back to Truk on 13 November, among them just six B6N1 ''Tenzan'''s out of the forty initially committed.<ref name="Wieliczko2003p11">Wieliczko 2003, p. 11.</ref>

On 19 June 1944, the B6N made its carrier-borne combat debut at The [[Battle of the Philippine Sea]], operating in an environment where the U.S. Navy had virtually complete air superiority. Subsequently, it failed to inflict any significant damage whatsoever whilst taking heavy losses from the U.S. Navy's new [[Grumman F6F Hellcat|F6F Hellcat]] fighter.

By this point, small improvements in the B6N's performance were amongst the least of the Japanese Navy's problems. When the new model became available in mid-1944, Japan had already lost most of its large carriers, and was becoming desperately short of experienced pilots. The vast majority of B6N2 operations therefore took place from land bases and failed to achieve any major successes. The planes were extensively used in the [[Battle of Okinawa]] where they were also used for [[kamikaze]] missions for the first time.

==Variants==
<ref name="TMM_P72">The Maru Mechanic (1981), p. 72&ndash;74</ref>
[[File:B6N2 in formation.jpg|thumb|right|Nakajima B6N2 "Tenzan" as 752nd [[Kōkūtai]] flying in formation (note aircraft numbers on [[hinomaru]]).]]
[[File:B6N2s prior to take off.jpg|thumb|right|Nakajima B6N2 "Tenzan" unit before take-off.]]

*B6N1 : Prototypes - Engine [[Nakajima Mamoru|Nakajima NK7A Mamori]] 11 of 1,394&nbsp;kW (1,870&nbsp;hp), four-blade propeller. Two examples built.
*B6N1 Tenzan Navy Carrier Based-Attack Bomber, Model 11: First series model. 133 built (work number 1&ndash;133).
*B6N2 Model 12: Main production model, featured [[Mitsubishi Kasei|Mitsubishi MK4T Kasei 25]] of 1,380&nbsp;kW (1,850&nbsp;hp). 1,131 built as B6N2/B6N2a (work number 134&ndash;750, 753&ndash;1,266).
*B6N2a Model 12A: Revised tail armament. 7.7&nbsp;mm (.303&nbsp;in) [[Type 92 machine gun]], replaced with one 13&nbsp;mm [[Type 2 machine gun]].
*B6N3 Model 13 Prototypes: Engine Mitsubishi MK4T-C Kasei 25c of 1,380&nbsp;kW (1,850&nbsp;hp). Modified landing gear for operating from land bases; two built (work number 751&ndash;752).
*''Total Production'' (all versions): 1,268 examples.

==Survivors==
Today only one B6N remains in existence and it is stored at the [[National Air and Space Museum]] in [[Washington, D.C.]]. It consists of the fuselage and its engine/propeller (separate) and a [[vertical stabilizer]]. The location of the horizontal surfaces is unconfirmed; however, as the aircraft was intact at one time, it is possible that the wings are stored separately.<ref>[http://www.preservedaxisaircraft.com/ B6N survivor]</ref>

[[File:B6N Taic.jpg|right|thumb|A B6N2 at NAS Anacosta is tested by US Navy personnel of the TAIC (Technical Air Intelligence Center) after the war.]]

== Operators ==
;{{JPN}}
* [[Imperial Japanese Navy]]<ref name="TMM_P72" />
**Aircraft carrier
***''[[Japanese aircraft carrier Shōkaku|Shōkaku]]''
***''[[Japanese aircraft carrier Zuikaku|Zuikaku]]''
***''[[Japanese aircraft carrier Taihō|Taihō]]''
***''[[Japanese aircraft carrier Jun'yō|Jun'yō]]''
***''[[Japanese aircraft carrier Hiyō|Hiyō]]''
***''[[Japanese aircraft carrier Ryūhō|Ryūhō]]''
***''[[Japanese aircraft carrier Chitose|Chitose]]''
***''[[Japanese aircraft carrier Chiyoda|Chiyoda]]''
***''[[Japanese aircraft carrier Zuihō|Zuihō]]''
**Naval Air Group
***Himeji [[Kōkūtai]]
***Hyakurihara Kōkūtai
***Kushira Kōkūtai
***Sunosaki Kōkūtai
***Suzuka Kōkūtai
***Taiwan Kōkūtai
***Tateyama Kōkūtai
***Taura Kōkūtai
***Usa Kōkūtai
***Yokosuka Kōkūtai
***131st Kōkūtai
***210th Kōkūtai
***331st Kōkūtai
***501st Kōkūtai
***531st Kōkūtai
***551st Kōkūtai
***553rd Kōkūtai
***582nd Kōkūtai
***601st Kōkūtai
***634th Kōkūtai
***652nd Kōkūtai
***653rd Kōkūtai
***701st Kōkūtai
***705th Kōkūtai
***752nd Kōkūtai
***761st Kōkūtai
***762nd Kōkūtai
***765th Kōkūtai
***901st Kōkūtai
***903rd Kōkūtai
***931st Kōkūtai
***951st Kōkūtai
***1001st Kōkūtai
**Aerial Squadron
***Attack 251st [[Hikōtai]]
***Attack 252nd Hikōtai
***Attack 253rd Hikōtai
***Attack 254th Hikōtai
***Attack 256th Hikōtai
***Attack 262nd Hikōtai
***Attack 263rd Hikōtai
**[[Kamikaze]]
***Kikusui-Tenzan group
***Kikusui-Ten'ō group
***Kikusui-Raiō group
***Mitate group No. 2
***Mitate group No. 3
***Kiichi group
* {{Citation needed span|text=[[Imperial Japanese Army]]|date=May 2012}}<!---Please describe the air unit name. The exchange with Army Type-1 fighter "OSCAR" was not realized in 1944.--->

==Specifications (Nakajima B6N2)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Japanese Aircraft of the Pacific War<ref name="Frnc pac 70 p433">Francillon 1970, p. 433.</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |ref=, |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Airtemp]]. -->
|crew=3
|length main=10.87 m
|length alt=35 ft 7¾ in
|span main=14.89 m
|span alt=48 ft 10⅜ in
|height main=3.80 m
|height alt=12 ft 5⅝ in
|area main=37.2 m²
|area alt=400 ft²
|empty weight main=3,010 kg
|empty weight alt=6,636 lb
|loaded weight main=5,200 kg
|loaded weight alt=11,460 lb
|max takeoff weight main=5,650 kg
|max takeoff weight alt=12,460 lb
|more general=
|engine (prop)=[[Mitsubishi Kasei]] 25
|type of prop=14-cylinder air-cooled [[radial engine]]
|number of props=1
|power main=1,380 kW
|power alt=1,850 hp
|power more=(take-off power)
|max speed main=481 km/h
|max speed alt=260 knots, 299 mph
|max speed more=at 4,900 m (16,075 ft)
|cruise speed main=333 km/h
|cruise speed alt=180 knots, 207 mph
|cruise speed more=at 4,000 m (13,125 ft)
|range main=3,046 km
|range alt=1,644 nmi,1,892 mi
|ceiling main=9,040 m
|ceiling alt=29,660 ft
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|power/mass main= 
|power/mass alt= 
|more performance=*'''Climb to 5,000 m (16,400 ft):''' 10 min 24 sec
|guns=1 × 7.7 mm (0.303 in) [[Type 92 machine gun]] in rear cockpit and 1 × 7.7 mm (0.303 in) Type 92 firing through ventral tunnel
|bombs=1 x 800kg torpedo ''or'' 800 kg (1,760 lb) of bombs (1 x 800kg or 500kg, or 2 x 250kg)
}}

==See also==
{{aircontent|
|related=
* [[Nakajima B5N]]
|similar aircraft=
* [[Aichi B7A Ryusei]]
* [[Curtiss SB2C Helldiver]]
* [[Fairey Barracuda]]
* [[Grumman TBF Avenger]]
* [[Junkers Ju 87|Junkers Ju 87D]]
* [[Yokosuka D4Y Suisei]]
|lists=
* [[List of aircraft of World War II]]
* [[List of military aircraft of Japan]]
|see also=
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
*Francillon, René J. ''Imperial Japanese Navy Bombers of World War Two''. Windsor, Berkshire, UK: Hylton Lacy Publishers Ltd., 1969. ISBN 0-85064-022-9.
*Francillon, R. J. ''Japanese Aircraft of the Pacific War''. London: Putnam, 1970. ISBN 0-370-00033-1.
*Francillon, René J. ''Japanese Aircraft of the Pacific War''. London: Putnam & Company Ltd., 1979. ISBN 0-370-30251-6.
*Francillon, René J. ''Japanese Carrier Air groups 1941-45''. London: Osprey Publishing Ltd., 1979. ISBN 0-85045-295-3.
* Gunston, Bill. ''Military Aviation Library World War II: Japanese & Italian Aircraft''. Salamander Books Ltd., 1985. ISBN 0-89009-899-9.
* Mondey, David. ''Concise Guide to Axis Aircraft of World War II''. Temple Press, 1984. ISBN 0-600-35027-4.
*Thorpe, Donald W. ''Japanese Naval Air Force Camouflage and Markings World War II''. Fallbrook, California; Aero Publishers Inc., 1977. ISBN 0-8168-6587-6. (pbk.) ISBN 0-8168-6583-3. (hc.)
*Tillman, Barrett. ''Clash of the Carriers''. New American Library, 2005. ISBN 0-451-21670-9
*Wieliczko, Leszek A. and Argyropoulos, Peter. (transl.) ''Nakajima B6N "Tenzan" (Famous Airplanes 3)'' (Bi-lingual Polish/English). Lublin, Poland: Kagero, 2003. ISBN 83-89088-36-3.
*The Maru Mechanic No. 30 ''Nakajima carrier torpedo bomber "Tenzan" B6N'', Ushio Shobō (Japan), September 1981
{{refend}}

==External links==
{{Commons|Nakajima B6N}}
* [http://www.ne.jp/asahi/airplane/museum/cl-pln6/Tenzan.html Asahi museum prints]

{{Nakajima aircraft}}
{{Japanese Navy Torpedo Bombers}}
{{Allied reporting names}}

[[Category:Carrier-based aircraft]]
[[Category:Japanese bomber aircraft 1940–1949|B06N, Nakajima]]
[[Category:World War II Japanese torpedo bombers|B6N]]
[[Category:Nakajima aircraft|B06N]]