{{Italic title}}
'''''Anuario Filosófico''''' is a triannual [[peer-reviewed]] [[academic journal]] of [[philosophy]]. It was established in 1968 and is published by the Philosophy Department of the [[University of Navarra]] in [[Spanish language|Spanish]]. The [[editor-in-chief]] is Montserrat Herrero of the same University.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[L'Année Philologique]]
* [[Arts & Humanities Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-10-05}}</ref>
* [[FRANCIS]]
* [[International Bibliography of Periodical Literature in the Humanities and Social Sciences]]
* [[Linguistics and Language Behavior Abstracts]]
* [[Philosopher's Index]]
* [[Philosophy Research Index]]
* [[Revue d'Histoire Ecclesiastique]]
* [[Scopus]]<ref name=Scopus>{{cite web|url=http://cdn.elsevier.com/assets/excel_doc/0003/148548/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work= |accessdate=2014-10-05 |deadurl=yes |archiveurl=https://web.archive.org/web/20131202041814/http://cdn.elsevier.com:80/assets/excel_doc/0003/148548/title_list.xlsx |archivedate=2013-12-02 |df= }}</ref>
}}

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.unav.es/publicaciones/anuariofilosofico/contenidos/eng.htm}}
* {{ISSN|0066-5215}}

{{DEFAULTSORT:Anuario Filosofico}}
[[Category:1968 establishments in Spain]]
[[Category:Philosophy journals]]
[[Category:Spanish-language journals]]
[[Category:Publications established in 1968]]
[[Category:University of Navarra]]
[[Category:Triannual journals]]