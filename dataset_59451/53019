'''''The Society for the Study of the Multi-Ethnic Literature of the United States''''' (''MELUS'') is a [[learned society|scholarly society]] established in 1974. Together with the [[University of Connecticut]], MELUS publishes a quarterly [[academic journal]], ''MELUS''. The aim of the Society is "to expand the definition of [[American literature]] through the study and teaching of [[Latino]] American, [[Native Americans in the United States|Native American]], [[African-American]], [[Asian American|Asian and Pacific American]], and ethnically specific Euro-American literary works, their authors, and their cultural contexts".<ref name=UConn>{{cite web|url=http://english.uconn.edu/affiliated_programs/melus_docs/melus.html |title=Department of English &#124; UConn |publisher=English.uconn.edu |date= |accessdate=2012-08-03}}</ref>

== Founding ==
The society was formed in response to the perceived practice at the [[Modern Language Association]]'s annual conference American Literature section of discussing only works by white men. The society was founded at the following year's conference and within a few months had almost 100 members. At the conference the following year (1974), society members formally proclaimed their demand, "We must expand the canon of American literature!" At this time, the society's goals included the recovery of lost works by minority authors, the compilation of bibliographies of minority literature, and  the enlisting of the aid of ethnic studies scholars in all fields, as well as publishing book reviews, connecting scholars, and printing abstracts on ethnic studies dissertations.<ref>{{cite journal |last=Newman |first=Katherine |title=MELUS Invented: The Rest Is History |journal=MELUS |date=Winter 1989–1990  |volume=16 |issue=4 |pages=99–113 |jstor=467104 |doi=10.2307/467104}}</ref>

== Present ==
The Society organizes sessions at the conventions of such scholarly organizations as the Modern Language Association and its Regionals, [[College English Association]], [[National Women's Studies Association]], [[American Studies Association]], [[American Literature Association]], and [[Popular Culture Association]]".<ref name=UConn/> The current president is [[Wenying Xu]] ([[Florida Atlantic University]]).<ref name=home>{{cite web|url=http://webspace.ship.edu/kmlong/melus/ |title=MELUS The Society for the Study of the Multi-Ethnic Literature of the United States |publisher=Webspace.ship.edu |date= |accessdate=2012-08-03}}</ref>

== Journal ==
{{Infobox journal
| title = MELUS
| cover =
| editor = [[Martha J. Cutter]]
| discipline = [[Literature]], [[ethnic studies]]
| abbreviation =
| publisher = [[University of Connecticut]] on behalf of The Society for the Study of the Multi-Ethnic Literature of the United States
| country = United States
| frequency = Quarterly
| history = 1974-present
| openaccess =
| impact =
| impact-year =
| website = http://www.melus.org/journal/
| link1 = http://english.uconn.edu/affiliated_programs/melus_docs/melus.html
| link1-name = Journal page at website of Dept. of English, University of Connecticut
| link2 =
| link2-name =
| JSTOR = 0163755X
| OCLC = 50709793
| LCCN = 79642925
| CODEN =
| ISSN = 0163-755X
| eISSN = 1946-3170
}}
''MELUS'' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]], covering [[Multiculturalism|multicultural]] [[Literary criticism|literary studies]]. Most issues are thematically organized. The founding [[editor-in-chief]] was [[Katherine D. Newman]], who was succeeded by [[Joseph T. Skerrett]], Jr., then by [[Veronica Makowsky]], and, most recently, by [[Martha J. Cutter]].<ref name=black>{{cite journal |title=The Bodies of Black Folk |journal=MELUS |date=Winter 2010 |volume=35 |issue=4 |pages=5–199 |jstor=25759553 |doi=10.1093/melus/35.4.5}}</ref> The journal is supported by dues of Society members, library subscriptions, funds from patrons, and by the [[University of Connecticut]].

=== Abstracting and indexing ===
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Modern Language Association|MLA Bibliography]]
* [[Arts and Humanities Citation Index]]
* [[American Humanities Index]]
* [[International Bibliography of Periodical Literature]]
* [[International Bibliography of Book Reviews]]
}}

== Annual conference ==
Since 1987, the society has sponsored themed conferences in various locations around the United States. These conferences feature "panels, workshops and round tables on all aspects of the multi-ethnic literatures of the United States".<ref name=black />

==References==
{{Portal|Literature}}
{{Reflist}}

==External links==
* {{Official|http://www.melus.org/journal/}}

{{DEFAULTSORT:Multi-Ethnic Literature Of The United States}}
[[Category:Learned societies of the United States]]
[[Category:Literary societies]]
[[Category:American studies]]
[[Category:American literary magazines]]
[[Category:Publications established in 1974]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:University of Connecticut]]