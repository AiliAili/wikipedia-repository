{{good article}}
{{Correct title|title=Bachata #1's, Vol. 3|reason=hash}}
{{Infobox album <!-- See Wikipedia:WikiProject Albums -->
| Name        = Bachata #1's, Vol. 3
| Type        = compilation
| Cover       = Bachata -1's, Vol. 3.jpg
| Caption     = 
| Artist      = Various Artists
| Released    = March 30, 2010
| Recorded    = 2006-2010
| Genre       = [[Bachata (music)|Bachata]]
| Length      = {{duration|m=44|s=20}}
| Language    = Spanish
| Label       = [[Machete Music]]
| Producer    = [[Anthony Santos]], [[Lenny Santos]], [[Monserrate & DJ Urba]], [[Ivy Queen]], Los Magnificos, Polo Parra, Juan Luis Guzman, Irving Menendez
| Last album  = ''[[Bachata Number 1's, Vol. 2|Bachata #1's, Vol. 2]]''<br>(2008)
| This album  = '''''Bachata #1's, Vol. 3'''''<br>(2010)
| Next album  = ''Bachata #1's, Vol. 4''<br>(2011)
}}

'''''Bachata #1's, Vol. 3''''' is a [[compilation album]] released by [[Machete Music]] on March 30, 2010. The album includes tracks recorded by several artists from the [[bachata (music)|bachata]] genre, such as [[Aventura (band)|Aventura]], [[Héctor Acosta (singer)|Héctor Acosta]], [[Xtreme (group)|Xtreme]], [[Ivy Queen]], Grupo Rush, [[Andy Andy]], [[Carlos & Alejandra]], and Marcy Place. It also features select bachata versions of songs by [[reggaeton|reggaetón]] and [[Latin pop]] artists including [[R.K.M & Ken-Y]], [[Alejandro Fernández]], [[Luis Fonsi]], and [[Cristian Castro]].
	
Upon release, the album peaked at number forty-one on the [[Latin Albums|''Billboard'' Latin Albums]] chart and number four on the [[Tropical Albums|''Billboard'' Tropical Albums]] chart. It became the twentieth best-selling Tropical Album of 2010. Several songs included on the album were released as singles from their respective parent albums including the opening "[[El Perdedor (Aventura song)|El Perdedor]]" by Aventura, "[[No Me Doy Por Vencido]]" by Luis Fonsi, and "[[Dime (Ivy Queen song)|Dime]]" by Ivy Queen. The fourth volume in the ''Bachata #1's'' series was released in 2011.

==Background and repertoire==
[[File:Anthony Romeo Santos.jpg|thumbnail|left|Lead singer of [[Aventura (band)|Aventura]], [[Romeo Santos]] ''(pictured)'' wrote "[[El Perdedor (Aventura song)|El Perdedor]]".]]
The ''Bachata #1's'' series are several compilation albums of various artists centered on the genre of [[bachata (music)|bachata]].<ref>{{cite web|url=http://www.terra.com.mx/musica/articulo/729961/Luis+Fonsi+alcanza+el+primer+lugar+con+su+disco.htm |archive-url=http://archive.is/20130615201706/http://www.terra.com.mx/musica/articulo/729961/Luis+Fonsi+alcanza+el+primer+lugar+con+su+disco.htm |dead-url=yes |archive-date=2013-06-15 |title=Luis Fonsi alcanza el primer lugar con su disco |work=[[Terra Networks]] |publisher=[[Telefónica]] |date=2009-11-22 |accessdate=2013-05-09 }}</ref><ref>{{cite web|url=http://www.terra.com/musica/noticias/marco_antonio_solis_sigue_en_primer_lugar_con_el_album_quotno_molestarquot/oci449475 |archive-url=http://archive.is/20130615235828/http://www.terra.com/musica/noticias/marco_antonio_solis_sigue_en_primer_lugar_con_el_album_quotno_molestarquot/oci449475 |dead-url=yes |archive-date=2013-06-15 |title=Marco Antonio Solís sigue en primer lugar con el álbum "No molestar |work=[[Terra Networks]] |publisher=[[Telefónica]] |date=2008-10-30 |accessdate=2013-05-09 }}</ref> The first two in the series were released in 2008 and 2009, respectively. ''Bachata #1's, Vol. 3'' was released by [[Machete Music]] digitally and physically on March 30, 2010 in the United States.<ref>{{cite web|url=http://www.allmusic.com/album/bachata-1s-vol-3-mw0001966591/releases|title=Bachata #1's, Vol. 3 - Various Artist: Releases: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> "[[El Perdedor (Aventura song)|El Perdedor]]", written by [[Romeo Santos|Anthony "Romeo" Santos]] and performed by [[Aventura (band)|Aventura]], was released as the third and final single from the group's second live album, ''[[K.O.B. Live]]'' (2006). The song peaked at number five on the [[Latin Songs|''Billboard'' Latin Songs]] chart, topping both the [[Tropical Songs|''Billboard'' Tropical Songs]] and [[Latin Rhythm Airplay|''Billboard'' Latin Rhythm Songs]] charts.<ref>{{cite web|url=http://www.allmusic.com/album/kob-live-mw0000571400/awards|title=K.O.B - Aventura: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> The bachata-infused-R&B number<ref>{{cite web|url=http://www.informador.com.mx/entretenimiento/2011/302420/6/aventura-continua-contagiando-al-mundo-de-su-bachata.htm|title=Aventura continúa contagiando al mundo de su bachata|language=Spanish|work=El Informador|publisher=Unión Editorialista, S.A. de C.V|date=2010-06-25|accessdate=2013-05-08}}</ref> has been named one of their biggest hits along with "[[Los Infieles]]", "[[Un Beso]]", and "[[Mi Corazoncito]]" among others.<ref>{{cite web|url=http://www.estereofonica.com/aventura-anuncia-su-tour-por-colombia-en-agosto/ |title=Aventura anuncia su tour por Colombia en agosto |date=2009-06-11 |language=Spanish |work=Estereofónica |publisher=Estereofónica, LLC |accessdate=2013-05-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20130612012223/http://www.estereofonica.com:80/aventura-anuncia-su-tour-por-colombia-en-agosto/ |archivedate=2013-06-12 |df= }}</ref><ref>{{cite web|url=http://archivo.elheraldo.hn/Ediciones/2011/06/20/Noticias/Aventura-y-una-coleccion-de-lujo-con-sus-temas-mas-sonados |title=Aventura y una colección de lujo con sus temas más sonados |language=Spanish |work=El Heraldo |publisher=PUBLYNSA S.A. |date=2011-06-19 |accessdate=2013-05-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20130921060726/http://archivo.elheraldo.hn/Ediciones/2011/06/20/Noticias/Aventura-y-una-coleccion-de-lujo-con-sus-temas-mas-sonados |archivedate=2013-09-21 |df= }}</ref>

The second track, "[[Se Me Va la Voz|Se Me Va La Voz]]" by Mexican singer [[Alejandro Fernández]] was released as the [[lead single]] from his fourteenth studio album ''[[Dos Mundos (Alejandro Fernández album)|Dos Mundos: Evolución]]'' (2009). It was named the highlight of the album by Jason Birchmeier of [[Allmusic]].<ref>{{cite web|url=http://www.allmusic.com/album/dos-mundos-evoluci%C3%B3n-mw0001338042|title=Dos Mundos: Evolución - Alejandro Fernández: Songs, Reviews, Credits, Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> It topped the ''Billboard'' Latin Songs,<ref>{{cite web|url=http://www.billboard.com/artist/278390/alejandro-fern-ndez/chart?f=363|title=Alejandro Fernández - Chart History: Latin Songs|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> ''Billboard'' Latin Pop Songs,<ref>{{cite web|url=http://www.billboard.com/artist/278390/alejandro-fern-ndez/chart?f=372|title=Alejandro Fernández - Chart History: Latin Pop Songs|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> and ''Billboard'' Tropical Songs charts.<ref>{{cite web|url=http://www.billboard.com/artist/278390/alejandro-fern-ndez/chart?f=340|title=Alejandro Fernández - Chart History: Tropical Songs|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> The bachata version, which is included on this release, features Héctor Acosta "El Torito".<ref>{{cite web|url=http://www.elnacional.com.do/que-pasa/2009/10/22/29762/aaaa |title=Héctor Acosta feliz con "Simplemente El Torito |work=El Nacional |publisher=Periódico El Nacional |date=2009-10-22 |accessdate=2013-05-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20130921060427/http://www.elnacional.com.do/que-pasa/2009/10/22/29762/aaaa |archivedate=2013-09-21 |df= }}</ref> "[[No Me Doy Por Vencido]]" is originally from Puerto Rican singer [[Luis Fonsi]]'s seventh studio album ''[[Palabras del Silencio|Palabras Del Sliencio]]'' (2008), which includes "unexpected musical styles" including the ranchera accents that can be found on "No Me Doy Por Vencido" and its "breathtaking chorus".<ref>{{cite web|url=http://www.allmusic.com/album/palabras-del-silencio-mw0000796480|title=Palabras del Silencio - Luis Fonsi: Songs, Reviews, Credits, Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> It topped the ''Billboard'' Latin Songs, Tropical Songs, and Latin Pop Songs charts.<ref>{{cite web|url=http://www.allmusic.com/album/palabras-del-silencio-mw0000796480/awards|title=Palabras del Silencio - Luis Fonsi: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> [[Xtreme (group)|Xtreme]] performs "Lloro y Lloro" from their second studio album ''Chapter Dos'' (2008). It, along with the album, takes influence of contemporary hip-hop and R&B.<ref>{{cite web|url=http://www.allmusic.com/album/chapter-dos-mw0000803240|title=Chapter Dos - Xtreme: Songs, Reviews, Credits, Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> "[[Dime (Ivy Queen song)|Dime]]" was written by Puerto Rican singer Ivy Queen along with the producers [[Monserrate & DJ Urba]].<ref>{{cite web|url=http://repertoire.bmi.com/title.asp?blnWriter=True&blnPublisher=True&blnArtist=True&keyID=9903128&ShowNbr=0&ShowSeqNbr=0&querytype=WorkID|title=BMI - Repertoire: Dime (Legal Title)|work=[[Broadcast Music, Inc.|BMI]]|publisher=[[Broadcast Music, Inc.]]|accessdate=2013-05-09}}</ref> It was serviced to radio as the lead single from Queen's first live album, ''[[Ivy Queen 2008 World Tour LIVE!]]'' (2008) in both [[bachaton]] and bachata versions.<ref>{{cite web|url=https://books.google.com/books?id=jhQEAAAAMBAJ&pg=PA21&dq=Ivy+Queen+2008+World+Tour&hl=en&sa=X&ei=miSFUe7ICcnX0gGSyIGQCA&ved=0CEQQ6AEwAg#v=onepage&q=Ivy%20Queen%202008%20World%20Tour&f=false|title=Urban Jungle: Reggaetón Stars Expand Reach On New Albums|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|author=Ben-Yehuda, Ayala|date=2008-07-12|volume=120|issue=28|page=21|issn=0006-2510|accessdate=2013-05-09}}</ref> It peaked at number eight on the ''Billboard'' Latin Songs chart,<ref>{{cite web|url={{BillboardURLbyName|artist=ivy queen|chart=Latin Songs C}}|title=Ivy Queen - Chart History: Latin Songs|work=Billboard|publisher=Prometheus Global Media|accessdate=2012-10-30}}</ref> number four on the ''Billboard'' Tropical Songs chart,<ref>{{cite web|url=http://www.terra.com.mx/pms/core/inicio/cl/p/terramx/?s=1839&id_original=758771|title=Ricardo Arjona en primer lugar de Billboard con su disco "5to Piso|author=Saint, Cynthia|date=2008-11-27|work=[[Terra Networks]]|publisher=[[Telefónica]]|accessdate=2013-05-09}}</ref> and number one on the ''Billboard'' Latin Rhythm Songs chart.<ref>{{cite web|url={{BillboardURLbyName|artist=ivy queen|chart=Latin Rhythm Airplay}}|title=Ivy Queen - Chart History: Latin Rhythm Airplay|work=Billboard|publisher=Prometheus Global Media|accessdate=2013-05-09}}</ref> It spent seven consecutive weeks atop the chart.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2008-10-11/latin-rhythm-airplay|title=Latin Rhythm Airplay: October 11, 2008 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref>

Héctor Acosta's "Sin Perdon" originates from his second studio album ''Mitad/Mitad'' (2008).<ref>{{cite web|url=http://www.allmusic.com/album/mitad-mitad-mw0000784044|title=Mitad/Mitad - Héctor Acosta: Songs, Reviews, Credits: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> It reached twenty-six on the ''Billboard'' Latin Songs chart, however topped the ''Billboard'' Tropical Songs chart.<ref>{{cite web|url=http://www.allmusic.com/album/mitad-mitad-mw0000784044/awards|title=Mitad/Mitad - Héctor Acosta: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> "Jasmine" is performed Dominican bachata group Grupo Rush's, which is composed of Damian, Migz, Lenny, and Khriz. It is a single from their debut album ''We On Fire'' (2009). The song became a top ten hit on the ''Billboard'' Tropical Songs chart.<ref>{{Cite web|url=http://www.allmusic.com/artist/grupo-rush-mn0001022056|title=Grupo Rush - Music Biography, Credits and Discography: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> Dominican Bachata singer [[Andy Andy]] performs "Agua Pasada" which is from his seventh studio album ''Placer y Castigo'' (2009).<ref>{{cite web|url=http://elnuevodiario.com.do/app/article.aspx?id=151104|title=Andy Andy estrena casa disquera|work=El Nuevo Diario|publisher=El Nuevo Diario, LLC|date=2009-09-05|accessdate=2013-05-09}}</ref> It reached number eleven on the ''Billboard'' Tropical Songs chart.<ref>{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=EN&s_site=miami&p_multi=EN&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=12A211CEB62B91A8&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=El Grupo Aventura ya tiene Sucesores|work=El Nuevo Herald|publisher=Miami Herald Media Corporation|date=2009-08-13|accessdate=2013-05-09}}</ref> Puerto Rican reggaetón duo [[R.K.M & Ken-Y]] recorded "Vicio Del Pecado" in bachata in collaboration with Héctor Acosta, who makes his third appearance on this release.<ref>{{cite web|url=http://www.elnacional.com.do/que-pasa/2009/1/29/6064/El-Torito-Rakim-y-Ken-y-graban-video-musical-en-Santo-Domingo |title=El Torito, Rakim y Ken y graban video musical en Santo Domingo |work=El Nacional |publisher=Periódico El Nacional |date=2009-01-29 |accessdate=2013-05-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20090812033643/http://www.elnacional.com.do:80/que-pasa/2009/1/29/6064/El-Torito-Rakim-y-Ken-y-graban-video-musical-en-Santo-Domingo |archivedate=2009-08-12 |df= }}</ref> It was previously featured on the duo's second studio effort, ''[[The Royalty: La Realeza]]'' (2008).<ref>{{cite web|url=http://itunes.apple.com/us/album/royalty-la-realeza/id290672788|title=iTunes - Music - The Royalty/La Realeza by R.K.M & Ken-Y|work=[[iTunes Store]]|publisher=[[Apple Inc.]]|accessdate=2013-05-08}}</ref><ref>{{cite web|url=http://www.amazon.com/The-Royalty-La-Realeza/dp/B001F5U1BW/ref=tmm_other_meta_binding_title_0|title=Amazon.com: The Royalty/La Realeza: R.K.M & Ken-Y: Official Music|work=''[[Amazon.com]]''|publisher=[[Amazon.com|Amazon.com, Inc.]]|accessdate=2013-05-08}}</ref> [[Carlos & Alejandra]]'s "Cuanto Duele" appears on their debut album ''La Introduccion'' (2009) and the tenth track on this release. It reached number six on the ''Billboard'' Tropical Songs chart.<ref>{{cite web|url=http://www.allmusic.com/album/la-introduccion-mw0000816875/awards|title=La Introduccion - Carlos y Alejandra: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> "[[El Culpable Soy Yo]]" by [[Cristian Castro]] is from the [[El Culpable Soy Yo|album of the same name]].<ref>{{cite web|url=http://archivo.univision.com/content/content.jhtml?cid=1906014 |title=Cristian es el culpable: Escucha su sencillo y sé su cómplice |work=[[Univision]] |publisher=[[Univision|Univision Communications]] |date=2010-02-12 |accessdate=2013-05-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20130921060815/http://archivo.univision.com/content/content.jhtml?cid=1906014 |archivedate=2013-09-21 |df= }}</ref><ref>{{cite web|url=http://www.terra.com.mx/musica/articulo/817318/Cristian+Castro+se+declara+culpable+ante+100+millones+de+personas.htm|title=Cristian Castro se declara culpable ante 100 millones de personas|work=[[Terra Networks]]|publisher=[[Telefónica]]|date=2009-04-22|accessdate=2013-05-09}}</ref> It reached the top ten of both the ''Billboard'' Latin Songs and Tropical Songs chart.<ref>{{cite web|url=http://www.allmusic.com/album/el-culpable-soy-yo-mw0000817107/awards|title=El Culpable Soy Yo - Cristian Castro: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref> "Dame Un Chance (Nena)" is performed by urban bachata group Marcy Place from their debut album ''B From Marcy Place'' (2008).<ref>{{cite web|url=http://www.terra.com.mx/articulo.aspx?articuloId=765059&ref=1 |title=El grupo de bachata urbana Marcy Place debuta con el apoyo de Don Omar |work=[[Terra Networks]] |publisher=[[Telefónica]] |date=2008-12-17 |accessdate=2013-05-09 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{cite web|url=http://noticias.terra.com/noticias/Grupo_de_bachata_Marcy_Pace_continua_gira_de_presentaciones/act1832095|title=Grupo de bachata Marcy Pace continúa gira de presentaciones|work=[[Terra Networks]]|publisher=[[Telefónica]]|date=2009-06-29|accessdate=2013-05-09}}</ref> It peaked at number twenty-three on the ''Billboard'' Tropical Songs chart.<ref>{{Cite web|url=http://www.allmusic.com/album/b-from-marcy-place-mw0000803976/awards|title=B from Marcy Place - Marcy Place: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref>

==Reception and commercial performance==
Upon release, the album debuted at number forty-five on the [[Top Latin Albums|''Billboard'' Top Latin Albums]] chart for the week of April 17, 2010.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-04-17/latin-albums|title=Top Latin Albums: April 17, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> Sales increased slightly and the album rose two positions to number forty-one for the week of April 24, 2010<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-04-24/latin-albums|title=Top Latin Albums: April 24, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> It spent an additional week at that position before falling to number fifty-six, two weeks later on the issue date May 15, 2010.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-05-01/latin-albums|title=Top Latin Albums: May 01, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref><ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-05-15/latin-albums|title=Top Latin Albums: May 15, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> In its sixth week, the album again fell to number seventy.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-05-22/latin-albums|title=Top Latin Albums: May 22, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> The recording ended its reign on the chart a week later at number seventy-four for the issue dated May 29, 2010.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-05-29/latin-albums|title=Top Latin Albums: May 29, 2010|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref>

On the [[Tropical Albums|''Billboard'' Tropical Albums]] chart, the album debuted at number five behind Aventura's ''[[The Last (album)|The Last]]'', [[El Gran Combo de Puerto Rico]]'s ''[[Sin Salsa No Hay Paraiso]]'', [[Prince Royce]]'s [[Prince Royce (album)|debut studio album]] and ''[[Ciclos (Luis Enrique album)|Ciclos]]'' by [[Luis Enrique (singer)|Luis Enrique]] for the week of April 17, 2010.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-04-17/latin-tropical-albums|title=Tropical Albums: April 17, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> In its second week, it rose to number four replacing ''Ciclos''.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-04-24/latin-tropical-albums|title=Tropical Albums: April 24, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> It remained at the number four spot for four weeks.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-05-15/latin-tropical-albums|title=Tropical Albums: May 15, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> It spent nine weeks within the top ten of the chart.<ref>{{cite web|url=http://www.billboard.com/biz/charts/2010-06-19/latin-tropical-albums|title=Tropical Albums: June 19, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref> ''Bachata #1's, Vol. 3'' became the twentieth best-selling Tropical Album of 2010.<ref>{{cite web|url=http://www.billboard.com/artist/277740/various-artists/chart?f=459|title=Various Artist - Chart History: Tropical Albums (Year-End)|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-08}}</ref> Five of the songs included on this release are number-one singles, these include "El Perdedor", "Se Me Va La Voz", "No Me Doy Por Vencido", "Dime", and "Sin Perdon".<ref name="Liner">''Bachata #1's, Vol. 3'' (CD liner notes). [[Machete Music|Machete]]. 2010.</ref>

==Track listing==
*Track listing adapted from the album liner notes.<ref name="Liner"/>
{{tracklist
| writing_credits     = yes
| extra_column    = Performer(s)
| title1          = [[El Perdedor (Aventura song)|El Perdedor]]
| extra1           = [[Aventura (band)|Aventura]]
| writer1         = [[Romeo Santos|Anthony Santos]]
| length1         = 3:35
| title2          = [[Se Me Va la Voz|Se Me Va La Voz]]
| note2           = Bachata Version
| extra2           = [[Alejandro Fernández]] featuring [[Héctor Acosta (singer)|Héctor Acosta]]
| writer2         = Roy Tabare
| length2         = 3:16
| title3          = [[No Me Doy Por Vencido]]
| extra3           = [[Luis Fonsi]]
| note3           = Bachata Version
| writer3         = Claudia Brant, Luis Fonsi 
| length3         = 3:49
| title4          = Lloro y Lloro
| extra4           = [[Xtreme (group)|Xtreme]]
| writer4         = Steven Tejada
| length4         = 3:08
| title5          = [[Dime (Ivy Queen song)|Dime]]
| extra5          = [[Ivy Queen]]
| writer5         = [[Ivy Queen|Martha Pesante]], [[Monserrate & DJ Urba|Urbani Cedeño]], [[Monserrate & DJ Urba|Michael Monserrate]]
| length5         = 3:42
| title6          = Sin Perdon
| extra6           = Héctor Acosta
| writer6         = [[Jorge Celedón]]
| length6         = 4:06
| title7          = Jasmine
| extra7          = Grupo Rush
| writer7           = Edwin Garcia, Gabriel Gomez, Miguel Angel Perez 
| length7         = 3:23
| title8          = Agua Pasada
| extra8           = [[Andy Andy]]
| writer8         = Osmany Espinosa
| length8         = 3:36
| title9          = Vicio Del Pecado
| extra9           = [[R.K.M & Ken-Y]] featuring Héctor Acosta
| writer9         =  Juan Luis Guzman, [[R.K.M & Ken-Y|Jose Nieves]], [[Pina Records|Rafael Pina]], [[R.K.M & Ken-Y|Kenny Vazquez]]
| length9         = 3:57
| title10          = Cuanto Duele
| extra10           = [[Carlos & Alejandra]]
| writer10         = Carlos Vargas
| length10         = 3:32
| title11          = [[El Culpable Soy Yo]]
| note11           = Bachata Version
| extra11          = [[Cristian Castro]] featuring Carlos & Alejandra
| writer11         = Gloria Espana
| length11        = 4:04
| title12         = Dame Un Chance (Nena)
| extra12         = Marcy Place
| writer12        = Juan Carlos Cabrera
| length12        = 3:56
| total_length      = 44:20
}}

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!scope="col"|Chart (2009)
!scope="col"|Peak <br> Position
|-
!scope="row"|US [[Top Latin Albums]] (''[[Billboard (magazine)|Billboard]]'')<ref name="Allmusic">{{cite web|url=http://www.allmusic.com/album/bachata-1s-vol-3-mw0001966591/awards|title=Bachata #1's, Vol. 3 - Various Artist: Awards: Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2013-05-09}}</ref>
| style="text-align:center;"|41
|-
!scope="row"|US [[Tropical Albums]] (''[[Billboard (magazine)|Billboard]]'')<ref name="Allmusic"/>
| style="text-align:center;"|4
|-
|}
{{col-2}}

===Yearly charts===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!scope="col"|Chart (2009)
!scope="col"|Peak <br> Position
|-
|US [[Tropical Albums]] (''[[Billboard (magazine)|Billboard]]'')<ref>{{cite web|url=http://www.billboard.com/biz/charts/year-end/2010/tropical-albums|title=Tropical Albums: December 31, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2013-05-09}}</ref>
| style="text-align:center;"|20
|-
|}
{{col-end}}

==Release history==
{| class="wikitable"
! Region
! Date
! Format
! Label
|-
| Canada<ref>{{cite web|url=http://www.amazon.ca/Vol-3-Bachata-1s-Bachata/dp/B00389JE2K/ref=sr_1_1?ie=UTF8&qid=1368458313&sr=8-1&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.ca: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]] (Canada)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
| rowspan="7"| March 30, 2010
| rowspan="4"| [[Import]]
| rowspan="7"| Machete Music
|-
| Germany<ref>{{cite web|url=http://www.amazon.de/Bachata-Vol-3-Various-Machete-Music/dp/B00389JE2K/ref=sr_1_3?ie=UTF8&qid=1368458744&sr=8-3&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.de: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]] (Germany)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
|-
| France<ref>{{cite web|url=http://www.amazon.fr/Bachata-1s-Vol-Various-Artists/dp/B00389JE2K/ref=sr_1_1?ie=UTF8&qid=1368459404&sr=8-1&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.fr: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]] (France)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
|-
| Italy<ref>{{cite web|url=http://www.amazon.it/Bachata-Vol-3-Edizione-Various-Machete/dp/B00389JE2K/ref=sr_1_1?ie=UTF8&qid=1368458908&sr=8-1&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.it: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]] (Italy)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
|-
| Spain<ref>{{cite web|url=http://www.amazon.es/Vol-3-Bachata-1s-Various/dp/B00389JE2K/ref=sr_1_1?ie=UTF8&qid=1368459048&sr=8-1&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.es: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]] (Spain)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
| rowspan="2"| [[Compact disc|CD]], [[Import]]
|-
| United Kingdom<ref>{{cite web|url=http://www.amazon.co.uk/Bachata-1s-Vol-Various-Artists/dp/B00389JE2K/ref=sr_1_1?ie=UTF8&qid=1368459152&sr=8-1&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.com.uk: Bachata #1's Vol. 3: Music|work=''[[Amazon.com]]'' ''(United Kingdom)''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
|-
| United States<ref>{{cite web|url=http://www.amazon.com/Bachata-1s-Vol-Various-Artists/dp/B00389JE2K/ref=sr_1_3?ie=UTF8&qid=1368458147&sr=8-3&keywords=Bachata+%231%27s%2C+Vol.+3|title=Amazon.com: Bachata #1's Vol. 3: Various Artist: Music|work=''[[Amazon.com]]''|publisher=[[Amazon.com|Amazon.com, Inc]]|accessdate=2013-05-13}}</ref>
| rowspan="1"| [[Compact disc|CD]], [[Music download|Digital download]]
|-
|}

==References==
{{Reflist|2}}

==External links==
*[https://www.youtube.com/watch?v=LgGtsbqN7qI Universal Music Latin Entertainment Television Promotion] at [[YouTube]].

[[Category:2010 compilation albums]]
[[Category:Bachata compilation albums]]
[[Category:Machete Music compilation albums]]
[[Category:Albums produced by Ivy Queen]]
[[Category:Spanish-language compilation albums]]