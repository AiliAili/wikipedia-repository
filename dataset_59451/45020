{{For|other uses of Metrocard|MetroCard (disambiguation)}}
{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
[[File:Concession Adelaide Metrocard.png|right|thumb|240px|A Concession Metrocard issued in 2012.]]

A '''metrocard''' is a [[contactless smartcard]] ticketing system using [[MIFARE]]-Technology and being rolled out across the [[Adelaide Metro]] bus, train and tram system. It is similar to the [[Octopus Card]] and [[Oyster Card]] used in Hong Kong and London respectively. The contract for the system was tendered out, and in 2010 was awarded to [[Affiliated Computer Services]] using technology used in, among others, [[Montreal, Canada]], [[Houston, Texas]] and [[Toulouse, France]].

It was originally scheduled to become fully operative in early 2013, however due to overwhelmingly positive response from trial users it was launched on 3 November 2012.<ref name=adv-metcard-announce>{{cite news|last=Kelton|first=Sam|title=Public transport Metrocard ticketing up and running on SA buses, trams and trains by November|url=http://www.adelaidenow.com.au/public-transport-metrocard-ticketing-up-and-running-on-sa-buses-trams-and-trains-by-november/story-e6frea6u-1226477226639|accessdate=17 November 2012|newspaper=The Advertiser|date=19 September 2012}}</ref>

== History ==
The original ticketing system was based on the [[Crouzet]] system and was provided by [[Affiliated Computer Services]]. It was introduced in 1987 and was intended to be used for only a few years before being replaced. Replacement plans soon fell through and the original equipment remained in use until 2012.<ref name=newconn-dtei>{{cite web|title=New Connections - Winter 2010|url=http://www.dtei.sa.gov.au/__data/assets/pdf_file/0019/47701/New_Connections_Winter_2010_lowres.pdf|publisher=Department of Transport, Energy and Infrastructure}}</ref>  In 2010, it was announced that Affiliated Computer Services had been awarded the contract to replace the aging Crouzet system with a new [[contactless smartcard]] system, which would also allow the current [[Edmondson ticket|Edmondson]]-sized magnetic stripe tickets to remain in use.

As a result of the new system, multi-trip tickets are no longer sold as Crouzet tickets. Metrocards have replaced them, and Seniors tickets have been replaced by the new Seniors Card. Single and day-trip tickets continue to be sold as Edmondson-sized magnetic stripe Metrotickets. Passengers could trade their existing Multitrip Metrotickets in for Metrocard trips. At the end of December 2014, validators no longer accepted Multitrip tickets, however users could continue to convert them to Metrocard Trips until February 28, 2015.<ref>[https://au.news.yahoo.com/a/25664806/multitrip-tickets-for-adelaide-public-transport-to-be-invalid-soon/?source=wan Multitrip tickets for Adelaide public transport to be invalid soon] Seven news 2 December 2014</ref>

In 2013, it was announced that Metrocards would be able to be used to access secure bike cages, at railway stations fitted with the necessary equipment, for an annual fee.<ref name=admetbikes>{{cite web|title=Adelaide Metro - Bikes|url=http://www.adelaidemetro.com.au/Using-Adelaide-Metro/Bikes|work=Adelaide Metro|publisher=Government of South Australia|accessdate=29 March 2013}}</ref> It was also announced that, as part of the Tea Tree Plaza Interchange Park and Ride upgrade, Metrocard users would be able to use their Metrocard to pay for parking.<ref name=ttp-pnr>{{cite web|title=Safe and secure parking for O-Bahn commuters at Tea Tree Plaza|url=http://www.infrastructure.sa.gov.au/__data/assets/pdf_file/0006/92814/ttp_upgrade.pdf|work=Infrastructure SA|publisher=Government of South Australia|accessdate=29 March 2013}}</ref>

== Equipment ==

=== Validators ===
There are two kinds of validators: dual-purpose and Metrocard only. Dual-purpose validators have been installed on all buses, trains and trams and will accept both Metrocards and Crouzet tickets. Metrocard only validators only accept Metrocards and have been installed alongside the dual purpose validators on the [[3000 class railcar]]s and the [[Trams in Adelaide#Citadis 302 .2F Type 200 .2F Type J|Citadis Trams]]. Both feature indicator lights and an LCD display to display information (such as ticket type and errors) to users.

=== Vending Machines ===
[[File:Adelaide Metrocard Vending Machine.jpg|right|thumb|240px|A Metrocard vending machine located on a [[3000 class railcar]].]]

Located on every railcar and train is a vending machine used for the purchase of singletrip and day trip tickets. The vending machines can also recharge Metrocards. The machines accept coins and card payments, however they do not accept notes, which was a common criticism of the previous generation ticket vending machines.

The ticket vending machines have a touchscreen display for passengers to choose the appropriate fare and payment method, and upon payment the machine dispenses a validated ticket for the passenger. Passengers paying using a debit or credit card interact with the [[PIN pad]] located towards the centre to process payment.

A similar version of the vending machines is located at several of the city tram stops, the Adelaide Metro InfoCentres, and some major interchanges that will only recharge Metrocards and accept [[EFTPOS]] payments.

=== Metrocard Vending Machine ([[Ticket Vending Machine|TVM]]) ===

These machines are located in the main passenger concourse at [[Adelaide railway station]] and in the new public bus stop shelter at [[Adelaide Airport]]. They differ to other ticket vending machines as they dispense  both Metrocards and [http://www.adelaidemetro.com.au/Tickets/Metrotickets Metrotickets] as well as recharge Metrocards.

The machines have a larger touchscreen, similar to the other vending machines, which allow for selection of different ticket types and payment methods.

The payment methods accepted by this machine are coins (except 5c pieces), [[EFTPOS]], but also notes (except $50 and $100 notes).

==See also==

* [[Electronic money]]
* [[List of smart cards]]
* [[Oyster card]]
* [[Octopus card]]
* [[myki]]
* [[Opal card]]

== References ==
{{reflist}}

[[Category:Transport in Adelaide]]
[[Category:Fare collection systems in Australia]]