The '''Australasian College of Health Informatics''' is the [[professional body]] for [[health informatics]] in the [[Asia-Pacific]] region.

It consists of credentialed fellows and members as well as associate and student members. The College is an academic institutional member<ref>{{cite web |url=http://www.imia-medinfo.org/new2/academic |title=IMIA Academic Institutional Members |publisher=International Medical Informatics Association |work= |accessdate=2011-07-13}}</ref> of the [[International Medical Informatics Association]] and a full member of the [[Australian Council of Professions]].<ref>{{cite web |url=http://www.professions.com.au/about-us/members |title=Membership list |publisher=Australian Council of Professions |work= |accessdate=2011-06-13}}</ref> It was founded in 2002 and regularly provides comment and input to papers, proposals and legislative drafts in the region.<ref>{{cite web |url=http://www.ACHI.org.au/Documents.htm |title=Documents published by the Australasian College of Health Informatics |publisher=Australasian College of Health Informatics |work= |accessdate=2011-06-13}}</ref>

The college sees its functions as:
* standards setting for [[education]] and professional practice in health informatics
* support of health informatics initiatives
* facilitation of [[collaboration]]
* community [[mentoring]] 

The College sponsors the ''electronic Journal of Health Informatics''.<ref>{{cite web |url=http://www.ejhi.net/ojs/index.php/ejhi/about/journalSponsorship |title=Journal Sponsorship |format= |work=electronic Journal of Health Informatics |accessdate=2011-06-13| archiveurl= https://web.archive.org/web/20110723113638/http://www.ejhi.net/ojs/index.php/ejhi/about/journalSponsorship| archivedate= 23 July 2011 <!--DASHBot-->| deadurl= no}}</ref> It has also supported the [[Australian Health Informatics Education Council]] since its founding in 2009.<ref>{{cite web |url=http://www.AHIEC.org.au |title=AHIEC |publisher=Australian Health Informatics Education Council |work= |accessdate=2011-06-13}}</ref>

The College has Memoranda of Understanding with the [[Australian Library and Information Association]], the ''Health Information Management Association of Australia'' and the [[Health Informatics Society of Australia]].



==References==
{{Reflist}}

==External links==
* {{Official|http://www.ACHI.org.au}}
* {{Twitter}}
*[http://www.eJHI.net www.eJHI.org]  e-Journal of Health Informatics - an Open Access Journal
 

{{Health informatics}}

[[Category:Health informatics and eHealth associations]]
[[Category:Professional associations based in Australia]]
[[Category:2002 establishments in Australia]]