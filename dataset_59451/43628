<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Antilope
 | image=SIPA Antilope F-BJSS LBG 19.06.65 edited-2.jpg
 | caption=The sole SIPA Antilope exhibited at the [[Paris Air Show]] at Le Bourget in 1965 
}}{{Infobox Aircraft Type
 | type=4/5 seat turboprop light aircraft
 | national origin=[[France]]
 | manufacturer=[[Société Industrielle Pour l’Aéronautique|SIPA]]
 | designer=
 | first flight=7 November 1962
 | introduced=
 | retired=
 | status=stored in a private museum
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''SIPA S.251 Antilope''' was a low-wing [[monoplane]], seating four or five and powered by a single [[turboprop engine]], developed in France in the early 1960s. It set a number of class records but was not put into production.

==Design and development==

The Antilope was one of the first turboprop powered light aircraft.  Apart from its engine, it was a conventional all-metal low-wing machine.  The cantilever wing was built around two spars and was a semi-monocoque structure, carrying unslotted ailerons and electrically powered, single slot [[Fowler flaps]]. The fuselage was also of semi-monocoque construction.  The tail unit included a variable incidence tailplane and a rudder with a [[trim tab]].<ref name="JAWA"/>

It had an electrically actuated tricycle undercarriage, the main wheels retracting inwards into the wings.  The cabin had seats for four or five, two at the front and a bench seat behind. In a proposed air ambulance configuration, the Antilope would have carried two stretchers and a medic.  Access to the cabin was via a large rear hinged door on the starboard side.<ref name="JAWA"/>

The Antilope was powered by a 665&nbsp;hp (495&nbsp;kW) [[Turbomeca Astazou|Turbomeca Astazou X]] driving a 3-bladed propeller, on a long spinner, well ahead of the surrounding air intake.<ref name="JAWA"/>

It first flew on 7 November 1962 and gained certification in April 1964. That autumn, P. Bonneau set six international Class C1c (1000 – 1750&nbsp;kg) records with it, achieving for example a speed of 432.9&nbsp;km/h (267&nbsp;mph) over a 3&nbsp;km course and reaching an altitude of 10,420 m (34,186&nbsp;ft).  Early in 1965 it flew with a four-bladed propeller and improved on one of its own records.  A three-blade propellor was re-installed and the aircraft was exhibited at the 1965 [[Paris Air Show]] wearing registration ''F-BJSS''. By mid 1966 development had been completed without a decision to commence production.<ref name="JAWA"/>  The production version would have been known as the '''SIPA S.2510 Antilope''' but none were built; the prototype (''F-WJSS'') carried the designation S.251 on its fin.<ref name="AvFr"/>

The sole Antilope is undergoing restoration in a private museum, owned by the ''Association Antilope'', at Montpelier-Mediterranee Airport, in southern France.<ref name="Ogden"/>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (S.2510)==
{{Aircraft specs
|ref=<ref name="JAWA"/> (projected production variant S.2510, estimated at maximum takeoff weight) 
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=4/5
|length m=9.015
|length ft=
|length in=
|length note=
|span m=11.11
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=2.60
|height ft=
|height in=
|height note=
|wing area sqm=16.21
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=NACA 23,015 root, 4411 tip
|empty weight kg=990
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=1900
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Turbomeca Astazou|Turbomeca Astazou X]]
|eng1 type=turboprop
|eng1 kw=<!-- prop engines -->
|eng1 hp=665<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=3<!-- propeller aircraft -->
|prop name=Ratier-Figeac FH 76
|prop dia m=1.92<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=450
|max speed mph=
|max speed kts=
|max speed note= at 6,100 m (20,000 ft)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=380
|cruise speed mph=
|cruise speed kts=
|cruise speed note=70% power, at 6,100 m (20,000 ft)
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=2000
|range miles=
|range nmi=
|range note=typcal max
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=1100
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=13.5
|climb rate ftmin=
|climb rate note=at sea level
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{Commons category|SIPA Antilope}}
{{reflist|refs=
<ref name="JAWA">{{cite book |title= Jane's All the World's Aircraft 1966-67|last= Taylor |first= John W R |coauthors= |pages=56–7|edition= |year=1966|publisher= Sampson Low, Marston &Co. Ltd|location= London|isbn=|ref=harv }}</ref>
<ref name="Ogden">{{cite book|title=Aviation Museums and Collections of Mainland Europe|last=Ogden|first=Bob|pages=121–122|year=2006|isbn=0-85130-375-7}}</ref>
<ref name="AvFr">{{cite web |url=http://www.aviafrance.com/aviafrance1.php?ID=967&ID_CONSTRUCTEUR=1140|title=S.I.P.A. S-2510 'Antilope' |author= |date= |work= |publisher= |accessdate=2010-02-27}}</ref>
}}
{{refbegin}}
<!-- insert the reference sources here -->
{{refend}}
==Further reading==
* ''Coleman, Herbert J.'' [https://archive.org/stream/Aviation_Week_1963-06-24#page/n48/mode/1up SIPA Seeks Military Orders for Antilope]. // ''Aviation Week & Space Technology'', June 24, 1963, v. 78, no. 25, pp. 96-99.

<!-- ==External links== -->

{{SIPA aircraft}}

[[Category:French civil utility aircraft 1960–1969]]
[[Category:SIPA aircraft|S0251]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Turboprop aircraft]]