{{About||the battleship|Brazilian battleship Minas Geraes|this ship's earlier operation in the British and Australian navies|HMS Vengeance (R71)}}
{{Use dmy dates|date=December 2016}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:NAe Minas Gerais (A-11).jpg|300px|''Minas Gerais'']]
|Ship caption=''Minas Gerais''
}}
{{Infobox ship career
|Ship country=Brazil
|Ship flag={{shipboxflag|Brazil|naval}}
|Ship name=''Minas Gerais''
|Ship namesake=[[Minas Gerais|State of Minas Gerais]]
|Ship builder=Swan Hunter
|Ship laid down=16 November 1942 as {{HMS|Vengeance|R71}}
|Ship launched=23 February 1944
|Ship completed=15 January 1945
|Ship acquired=14 December 1956
}}
{{Infobox ship career
|Hide header=yes
|Ship builder=Verolme Dock, Rotterdam (reconstruction)
|Ship original cost=US$27,000,000
|Ship commissioned=6 December 1960
|Ship decommissioned=16 October 2001
|Ship fate=Sold for scrap
}}
{{Infobox ship characteristics
|Header caption=(Brazil service)
|Ship class=Modified {{sclass-|Colossus|aircraft carrier}}
|Ship displacement=*15,890 tons standard
*17,500 tons normal
*19,890 tons full load
|Ship length=*{{convert|630|ft|m|abbr=on}} [[Length between perpendiculars|between perpendiculars]]
*{{convert|695|ft|m|abbr=on}} [[Length overall|overall]]
|Ship beam={{convert|80|ft|m|abbr=on}}
|Ship draught={{convert|24.5|ft|m|abbr=on}}
|Ship propulsion=
|Ship speed={{convert|25|kn}} at 120 revolutions
|Ship range=*{{convert|12000|nmi}} at {{convert|14|kn}}
*{{convert|6200|nmi}} at {{convert|23|kn}}
|Ship endurance=
|Ship boats=
|Ship complement=1,000 + 300 air group
|Ship sensors=*'''Air search:''' Lockheed SPS-40B; E/F band
*'''Surface search:''' Plessey AWS 4; E/F band
*'''Navigation:''' Signaal ZW06; I band<!-- double-a spelling is correct -->
*'''Fire control:''' 2 × SPG-34; I/J band
*'''CCA:''' Scanter Mil-Par; I band
|Ship EW=
|Ship armament=*10 × [[Bofors 40 mm gun|Bofors 40 mm anti-aircraft guns]] (2 quad, 1 twin)
*2 × 47 mm [[Salute#Heavy arms: gun salutes|saluting guns]]
|Ship aircraft=21
|Ship aircraft facilities=
|Ship notes=Taken from:<ref name=Janes96>Sharpe (ed.), ''Jane's Fighting Ships, 1996-1997'', p. 55</ref> 
}}
|}

'''NAeL ''Minas Gerais''''' (pennant number '''A 11''') was a {{sclass/core|Colossus|aircraft carrier||||format=sclass-}} operated by the ''[[Marinha do Brasil]]'' (MB, Brazilian Navy) from 1960 until 2001. The ship was laid down for the [[Royal Navy]] during [[World War II]] as {{ship|HMS|Vengeance|R71|6}}, but was completed only shortly before the war's end, and did not see combat. After stints as a training vessel and Arctic research ship, the carrier was loaned to the [[Royal Australian Navy]] from 1952 to 1955. She was returned to the British, who sold her in 1956 to Brazil.

The ship underwent a four-year conversion in the Netherlands to make her capable of operating heavier naval aircraft. She was commissioned into the MB as ''Minas Gerais'' (named after the state of [[Minas Gerais]]) in 1960; the first purchased by a [[Latin America]]n nation, but the second to enter service, behind the Argentinian [[ARA&#32;Independencia&#32;(V-1)|ARA&nbsp;''Independencia'']]. Between 1987 and 1996, the carrier was unable to operate fixed-wing aircraft because of a defective [[aircraft catapult|catapult]], and was retasked as a helicopter carrier and [[amphibious assault ship]].

''Minas Gerais'' remained in service until 2001, when she was replaced by NAe {{ship|Brazilian aircraft carrier|São Paulo|A12|2}}. At the time of her decommissioning, she was the oldest operational aircraft carrier in the world, and the last operational unit of the World War II [[1942 Design Light Fleet Carrier|Light Fleet design]]. Despite attempts to preserve the carrier as a [[museum ship]], and after several failed attempts to auction the ship off (including a listing on [[eBay]]), ''Minas Gerais'' was sold for scrap in 2004 and taken to [[Alang, India|Alang]] for [[Ship breaking|breaking up]].

==Design and construction==
{{main|1942 Design Light Fleet Carrier}}
The carrier was constructed by [[Swan Hunter]] for the [[Royal Navy]] (RN) as HMS ''Vengeance''.<ref name=Janes68>Blackman (ed.), ''Jane's Fighting Ships (1968-69)'', p. 23</ref> She was laid down on 16 November 1942, and launched on 23 February 1944.<ref name=Janes68/> Construction was completed on 15 January 1945, and ''Vengeance'' was commissioned into the RN.<ref name=Janes68/>

The ''Colossus''-class carriers were intended to be 'disposable warships': they were to be operated during World War II and scrapped at the end of hostilities or within three years of entering service.<ref name="Hobbs p. 217">Hobbs, in ''The Navy and the Nation'', p. 217</ref> Despite this plan, the ship had a service life of over 55 years across three navies.

==Early operational history==
{{Main|HMS Vengeance (R71)}}
After commissioning, ''Vengeance'' was assigned to the 11th Aircraft Carrier Squadron, which was attached to the [[British Pacific Fleet]].<ref name=Cass181>Cassells, ''The Capital Ships'', p. 181</ref> The carrier arrived in Sydney, Australia in July 1945, but remained in the harbour for refits until the end of World War II.<ref name=Cass181/> ''Vengeance'' operated in the waters of Asia until mid-1946, when she returned to England and was reassigned as a training vessel.<ref name=Cass181/> In late 1948, ''Vengeance'' was converted for Arctic conditions, and from February 1949 to March 1949, she operated in Arctic waters as part of an experimental cruise to determine how well ships, aircraft, and personnel functioned in extreme cold.<ref name=Cass181/><ref>Till, ''Holding the Bridge in Troubled Times'', p. 317</ref>

[[File:HMAS Vengeance (AWM 044574).jpg|thumb|upright|left|''Vengeance'' during her Australian service]]
From late 1952 until mid-1955, ''Vengeance'' was loaned to the [[Royal Australian Navy]], to serve in place of the under-construction {{HMAS|Melbourne|R21|6}}.<ref name=Donohue94>Donohue, ''From Empire Defence to the Long Haul'', p. 94</ref><ref name=ANAM120>ANAM, ''Flying Stations'', p. 120</ref>

==Purchase and modernisation==
During the leadup to the [[Brazilian presidential election, 1955|1955 presidential election]], [[Juscelino Kubitschek de Oliveira]], governor of the state of [[Minas Gerais]] promised Brazilian admirals to acquire an aircraft carrier for the ''[[Marinha do Brasil]]'' (MB, Brazilian Navy).<ref name=Gordon40>Gordon, ''Brazil's Second Chance'', p. 40</ref> Kubitschek later claimed that this was to avoid a naval rebellion during his inauguration at the start of 1956, and despite believing in the "military uselessness" of the second-hand warship,<ref name=Gordon40/> the sale of ''Vengeance'' to Brazil for US$9 million went through on 14 December 1956.<ref name=Janes68/>

From mid-1957 until December 1960, the carrier underwent a massive refit and reconstruction in the Netherlands.<ref name=Janes68/> The work was carried out by Verolme Dock in Rotterdam, and cost US$27 million.<ref name=Janes68/> The most visible change was the installation of an 8.5-degree [[angled flight deck]].<ref name=Ireland245>Ireland, ''Aircraft Carriers of the World'', p. 245</ref> The size of the angle required that an accessway be built around the starboard side of the island superstructure; the weight acting as a counterbalance for the flight deck's portside overhang.<ref name=Ireland245/> Combined with the fitting of a more powerful [[steam catapult]], stronger [[arresting gear]], reinforced hangar elevators, and a [[mirror landing aid]], these modifications permitted the operation of jet aircraft up to {{convert|20000|lb}} in weight.<ref name=Ireland245/><ref name=BisChant82>Bishop & Chant, ''Aircraft carriers'', p. 82</ref> The carrier's island superstructure was replaced, and a lattice mast was fitted to support the new fire control system and radar suite.<ref name=Janes68/><ref name=Ireland245/> The ship's boiler capacity was increased, and internal electricity was converted to AC through the installation of four turbo generators and one diesel generator.<ref name=Janes68/>

The carrier was commissioned into the ''Marinha do Brasil'' as NAeL ''Minas Gerais'' (named for Kubitschek's home state) on 6 December 1960.<ref name=Janes68/> She departed Rotterdam for Rio de Janeiro on 13 January 1961.<ref name=Janes68/> The duration of the refit meant that while the carrier was the first purchased by a [[Latin America]]n nation, she was the second to enter service, after another ''Colossus''-class carrier entered service with the Argentine Navy as ARA ''Independencia'' in July 1959.<ref name=English56>English, ''Focus on Latin American Navies'', p. 56</ref>

===Weapons and systems===
During the ship's modernisation refit to become ''Minas Gerais'', her armament was altered to consist of ten [[Bofors 40 mm gun|Bofors 40 mm anti-aircraft guns]] (2 quad mountings and 1 twin mounting), and two 47&nbsp;mm [[Salute#Heavy arms: gun salutes|saluting guns]].<ref name=Janes96/> In 1994, the Bofors were removed and replaced with two twin surface-to-air launchers for [[Mistral missile]]s.<ref name=Janes96/> If additional armament was required, the Bofors could be reinstalled.<ref name=Janes96/>

By 1996, ''Minas Gerais'' was fitted with the following radars: a Lockheed SPS-40B for air search, a Plessey AWS 4 for surface search, a Signaal ZW06<!-- double-a spelling is correct --> for navigation, two SPG-34 for fire control, and a Scanter Mil-Par for Carrier-Controlled Approach.<ref name=Janes96/> The two search radars operate on [[Radio waves#EU, NATO, US ECM frequency designations|NATO frequency bands]] designated E and F, the navigation and CCA radars operate on the NATO I band, and the fire control radars on both the I and J bands.<ref name=Janes96/>

[[File:Minas Gerais DN-ST-90-01334.jpg|thumb|left|''Minas Gerais'' about to launch an S-2 Tracker]]
As of the mid-1990s, the carrier's air group consisted of six [[S-2 Tracker|S-2E Trackers]], four to six [[SH-3 Sea King|ASH-3D Sea Kings]], two [[Eurocopter AS355|AS-355 Ecureuils]], and three [[Eurocopter Super Puma|A-332 Super Pumas]].<ref name=Janes96/> In 1999, the MB acquired 20 [[A-4 Skyhawk|A-4KU Skyhawk]]s and 3 TA-4KU trainer aircraft from the [[Kuwait Air Force]] for $US70 million.<ref name=Corless/> ''Minas Gerais'' began operating these aircraft in late 2000.<ref>http://www.chilecompany.com/chilecomp11.htm Kuwaiti Air Force Sale</ref>

==Brazilian operational history==
In 1965, President [[Humberto de Alencar Castelo Branco]] permitted the operation of helicopters to the Navy, fixed-wing aircraft were to remain the responsibility of the ''[[Força Aérea Brasileira]]'' (FAB, Brazilian Air Force).<ref name=Signals1998>Jane's Navy International, ''Carrier Aviation - Skyhawks set to land on Brazilian carrier'', p. 6</ref> As a result, ''Minas Gerais'' was required to embark two air groups: the Navy operated helicopters while the Air Force operated [[S-2 Tracker]] aircraft.<ref name=English1996/> Consequently, the ship spent most of her Brazilian career operating as an anti-submarine warfare carrier.<ref name=Corless>Corless, ''The Brazilian Navy blazes a trail in the South Atlantic''</ref>

[[File:Minas Gerais DN-ST-90-01327.jpg|thumb|right|''Minas Gerais'' underway in 1984]]
From 1976 to 1981, the ship underwent a major refit.<ref name=BisChant82/> Work included the installation of a datalink to improve co-operation with {{sclass-|Niteroi|frigate}}s, updates to the radar suite, and other work to extend the carrier's life expectancy into the 1990s.<ref name=BisChant82/> In 1986, engine problems, combined with the inability for Argentina to fund a required modernisation of {{ship|ARA|Veinticinco de Mayo|V-2|6}}, saw the ''Colossus''-class carrier confined to port, making ''Minas Gerais'' the only active aircraft carrier in the South American region.<ref name=English1996>English, ''Latin American Navies still treading water''</ref> In December 1987, ''Minas Gerais'' herself was laid up after participating in Operation Dragon XXIII because of problems with her aircraft catapult.<ref name=FAAA>Fleet Air Arm Archive, ''History of the Colossus class carrier Minas Gerais''</ref> Although unable to operate as an aircraft carrier, the ship saw use over the following years in training exercises as an [[amphibious assault ship]]; using an air group of [[Eurocopter AS532 Cougar]] and [[Eurocopter AS350 Squirrel]] helicopters to transport [[Brazilian Marine Corps|Marines]] ashore.<ref name=FAAA/>

''Minas Gerais'' underwent a modernisation refit from July 1991 to October 1993.<ref name=Janes96/> This included the overhaul of the boilers and engines, integration of a SICONTA command system, two new navigation radars and Scanter-MIL landing radar were installed, and preparation for the installation of Simbad launchers for [[Mistral (missile)|Mistral surface-to-air missiles]].<ref name=Janes96/><ref name=ScottStarr>Scott & Starr, ''Carrier aviation at the crossroads''</ref> The launchers themselves were installed in 1994, with the ten Bofors removed at the same time.<ref name=Janes96/> The Bofors could be reinstalled to complement the ship's armament if needed.<ref name=Janes96/> In November 1993, the carrier conducted a joint exercise with the [[Argentine Navy]], where Argentine pilots flying [[Dassault-Breguet Super Étendard]] aircraft performed 177 [[touch-and-go landing]]s, in order to maintain their carrier landing qualifications while ''Veinticinco de Mayo'' was out of service.<ref name=ScottStarr/>

[[File:Minas Gerais DN-SN-97-01755.jpg|thumb|left|''Minas Gerais'' at anchor in Rio de Janeiro in 1995]]
During 1995 and 1996, the ability to operate fixed-wing aircraft was restored to ''Minas Gerais'', after the catapult from the decommissioned ''Veinticinco de Mayo'' was acquired and installed.<ref name=FAAA/> In 1997, ''Minas Gerais'' was loaned an A-4Q airframe by the Argentine ''Aviación Naval'' (Naval Aviation) for deck-handling and interface trials.<ref name=Signals1998/> This was in lead-up to the 1999 acquisition of 20 [[A-4 Skyhawk|A-4KU Skyhawk]]s and three TA-4KU trainer aircraft from the [[Kuwait Air Force]] for US$70 million.<ref name=Corless/> This was the first time since the carrier's commissioning that ''Forca Aeronaval da Marinha'' (Brazilian Navy Aviation) had been permitted to own and operate fixed-wing aircraft.<ref name=Corless/> The 23 aircraft were formed as the First Intercept and Attack Squadron, had all entered service by early 2000, and began carrier operations in late October 2000.<ref name=Corless/><ref name=Signals2000>Jane's International Defence Review ''Brazil - Skyhawks begin flights from carrier''</ref> In order to operate the new fighters, ''Minas Gerais'' underwent a major refit at the Arsenal de Marinha do Rio de Janeiro.<ref name=Corless/> The main purpose of this refit was to upgrade the catapult to launch Skyhawks.<ref name=Corless/>

==Replacement and decommissioning==
The replacement of ''Minas Gerais'' was first suggested in the early 1980s, as part of planned 15-year naval expansion program.<ref name=Anthony110/> Two different carrier designs were proposed.<ref name=Anthony110>Anthony, ''The Naval Arms Trade'', p. 110</ref> The first was for a 40,000 ton ship equipped with up to forty aircraft, including naval fighters.<ref name=Corless/><ref name=Anthony110/> To complement this proposal, a plan to expand the Brazilian fleet air arm by acquiring second-hand [[A-4 Skyhawk]]s from Kuwait or Israel was submitted.<ref name=Anthony111>Anthony, ''The Naval Arms Trade'', p. 111</ref> For this to happen, the 1965 ruling that prevented the MB from operating fixed-wing aircraft had to be overturned; a decision the FAB opposed.<ref name=Anthony110/> To make the purchase worthwhile, ''Minas Gerais'' would have to operate the aircraft until the replacement carrier entered service, which in turn required the installation of a modified steam catapult and arresting gear.<ref name=Anthony111/> As the ship had just emerged from a modernisation refit, this was an expensive proposition, and the Skyhawk acquisition plan was cancelled in October 1984.<ref name=Anthony111/> The Skyhawk plan was successfully revisited in the late 1990s. The second proposal was for a 25,000 ton helicopter carrier built to commercial standards.<ref name=Corless/><ref name=Anthony110/> Later interpretations of this proposal suggested a ship similar to the Royal Navy's {{HMS|Ocean|L12|6}}.<ref name=Corless/>

Replacement of ''Minas Gerais'' was under serious consideration by 1999; despite numerous refits and life-extending upgrades, the MB predicted that the carrier would require replacement before 2010.<ref name=Corless/> As well as the two proposals, consideration was given to acquiring a second-hand carrier, such as the French Navy's {{ship|French aircraft carrier|Foch|R99|2}}.<ref name=Corless/> One of the main issues in considering the replacement was the MB's significant investment in fixed-wing aviation in the late 1990s; a carrier capable of operating the recently acquired Skyhawks would be more expensive to acquire and operate than a [[STOVL]] or helicopter carrier, but the cheaper concepts would require the refactoring of Brazilian naval aviation.<ref name=Corless/> In the end, ''Foch'' was acquired, renamed NAe ''São Paulo'', and slated to commission into the ''Marinha do Brasil'' in April 2001.<ref name=Signals2000/>She was downclassed as a helicopter support ship in 2001 before her decommissioning. <ref>http://www.fleetairarmarchive.net/vengeance/history_bn.html</ref>

''Minas Gerais'' was decommissioned on 16 October 2001: the second last of the World War II-era light aircraft carriers to leave service, with the other being the 1959-commissioned {{INS|Viraat|R22|6}} (former {{HMS|Hermes|R12|6}}) which was ordered during the war but not completed and is the oldest aircraft carrier still in service in the world until it is decommissioned in 2016. <ref name="Hobbs p. 217"/><ref name=NNCampaign>Navy News (Australia), ''Campaign to save the Vengeance''</ref> At the time of her decommissioning, she was the second oldest active aircraft carrier in the world (a title passed on to the 1961-commissioned {{USS|Kitty Hawk|CV-63|6}})  Also at the decommissioning ceremony was 87 year old Admiral Hélio Leôncio Martins who was the first commander of the aircraft carrier. Also there was crew who had served on the Minas Gerais at different times <ref>http://www.fleetairarmarchive.net/vengeance/history_bn.html</ref> The carrier was marked for sale in 2002, and was sought after by British naval associations for return to the United Kingdom and preservation as a [[museum ship]], although they were unable to raise the required money.<ref name=telerace>Syal & Lashmar, ''Race to save historic ship from scrap heap''</ref><ref name=Parry/> Just before Christmas 2003, the carrier was listed for sale on auction website [[eBay]] by a user claiming to be a [[shipbroker]] representing the owner.<ref name=teleebay>Tweedie, ''For internet sale: aircraft carrier, only three owners''</ref> Bidding reached £4 million before the auction was removed from the website under rules preventing the sale of military ordnance.<ref name=teleebay/> An auction in Rio de Janeiro in February 2004 also failed to sell the ship.<ref name=Parry/> Sometime between February and July 2004, the carrier was towed to the [[ship breaking]] yards at [[Alang, India]] for dismantling.<ref name=Parry>Parry, ''Sad end to symbol of city's liberation''</ref>

== See also ==

* {{ship|Brazilian aircraft carrier|São Paulo|A12}}

==Citations==
{{Reflist|30em}}

==References==
;Books
* {{cite book|last=Anthony |first=Ian |title=The Naval Arms Trade |publisher=Oxford University Press |location=Oxford |year=1990 |series=Strategic issue papers |isbn=0-19-829137-X |oclc=59184818 |url=https://books.google.com/books?id=UMIUHvdi1gEC |format=Google Books |accessdate=13 November 2008}}
* {{cite book |author=Australian Naval Aviation Museum (ANAM) |title=Flying Stations: a story of Australian naval aviation |year=1998 |publisher=Allen & Unwin |location=St Leonards, NSW |isbn=1-86448-846-8 |oclc=39290180}}
*{{cite book |author=Blackman, Raymond (ed.) |title=Jane's Fighting Ships, 1968-69 |edition=71st |year=1968 |publisher=Jane's Publishing Company |location=London |oclc=123786869}}
*{{cite book |last=Bishop |first=Chris |author2=Chant, Christopher |title=Aircraft carriers: the world's greatest naval vessels and their aircraft |url=https://books.google.com/books?id=PY8CvlKC7kgC |format=Google Books |accessdate=13 November 2008 |year=2004 |publisher=Zenith |location=Grand Rapids, MI |isbn=0-7603-2005-5 |oclc=56646560}}
*{{cite book |last=Cassells |first=Vic |title=The Capital Ships: their battles and their badges |year=2000 |publisher=Simon & Schuster |location=East Roseville, NSW |isbn=0-7318-0941-6 |oclc=48761594}}
*{{cite book |last=Donohue |first=Hector |title=From Empire Defence to the Long Haul: post-war defence policy and its impact on naval force structure planning 1945-1955 |series=Papers in Australian Maritime Affairs (No. 1) |date=October 1996 |publisher=Sea Power Centre |location=Canberra |isbn=0-642-25907-0 |issn=1327-5658 |oclc=36817771}}
* {{cite book |last=Gordon |first=Lincoln |title=Brazil's Second Chance: En Route Toward the First World |url=https://books.google.com/books?id=j-0Q0ZeYMQcC |format=Google Books |accessdate=13 November 2008 |year=2001 |publisher=Brookings Institution Press |location=Washington, D.C. |isbn=0-8157-0032-6 |oclc=45806362}}
*{{cite book |last=Hobbs |first=David |editor=Stevens, David |editor2=Reeve, John |title=The Navy and the Nation: the influence of the Navy on modern Australia |year=2005 |publisher=Allen & Unwin |location=Corws Nest, NSW |isbn=1-74114-200-8 |oclc=67872922 |chapter=HMAS Sydney (III): a symbol of Australia's growing maritime capability}}
* {{cite book|last=Ireland |first=Bernard |title=The Illustrated Guide to Aircraft Carriers of the World |publisher=Anness Publishing |location=London |year=2008 |origyear=2005 |isbn=978-1-84477-747-1 |oclc=156616762}}
* {{cite book |title=The Naval Institute Guide to the Ships and Aircraft of the U.S. Fleet |first=Norman |last=Polmar |year=2001 |origyear=1993 |url=https://books.google.com/books?id=VO8BAdZJ7SsC |format=Google Books |accessdate=13 November 2008 |edition=17th |publisher=Naval Institute Press |location=Annapolis, Md |isbn=1-55750-656-6 |oclc=249124965 |page=107}}
* {{cite book |last=Sharpe |first=Richard (ed.) |title=Jane's Fighting Ships, 1996-97 |publisher=Jane's Information Group |location=Surrey |date=March 1996 |edition=99th |isbn=0-7106-1355-5 |oclc=34998928}}

;Journal articles
* {{cite journal |last=Corless |first=Josh |date=1 June 1999 |title=The Brazilian Navy blazes a trail in the South Atlantic |journal=Jane's Navy International |publisher=Jane's Information Group |volume=104 |issue=006}}
* {{cite journal |last=English |first=Adrian J. |year=2002 |title=Focus on Latin American Navies |journal=Naval Forces |publisher=Bonn Mönch |volume=23 |issue=6 |pages=53–64 |issn=0722-8880}}
* {{cite journal |last=English |first=Adrian J. |date=1 May 1996 |title=Latin American Navies still treading water |journal=Jane's Navy International |publisher=Jane's Information Group |volume=101 |issue=003}}
* {{cite journal |date=24 October 2000 |title=Brazil - Skyhawks begin flights from carrier |journal=Jane's International Defence Review |publisher=Jane's Information Group}}
* {{cite journal |date=1 January 1998 |title=Carrier Aviation - Skyhawks set to land on Brazilian carrier |journal=Jane's Navy International |publisher=Jane's Information Group |volume=103 |issue=001 |page=6}}
* {{cite journal |last=Scott |first=Richard |author2=Starr, Barbara |date=1 March 1999 |title=Carrier aviation at the crossroads |journal=Jane's Navy International |publisher=Jane's Information Group |volume=100 |issue=002}}
* {{cite journal |last=Till |first=Geoffrey |date=April 2005 |title=Holding the Bridge in Troubled Times: The Cold War and the Navies of Europe |journal=The Journal of Strategic Studies |publisher=Routledge |volume=28 |issue=2 |pages=309–337 |issn=0140-2390 |doi=10.1080/01402390500088379}}

;Newspaper articles and websites
* {{cite news |url=http://www.defence.gov.au/news/navynews/editions/02_04_02/story10.htm |title=Campaign to save the Vengeance |date=4 February 2001 |accessdate=8 August 2008 |work=Navy News |publisher=Royal Australian Navy}}
*{{cite web|url=http://www.fleetairarmarchive.net/vengeance/History_BN.html|title=History of the Colossus class carrier Minas Gerais |publisher=Fleet Air Arm Archive |accessdate=11 December 2011}}
* {{cite news |author=Syal, Rajeev |author2=Lashmar, Paul |title=Race to save historic ship from scrap heap |url=http://www.telegraph.co.uk/news/uknews/1397428/Race-to-save-historic-ship-from-scrap-heap.html |work=The Daily Telegraph |location=London |id= |date=16 June 2002 |accessdate=11 July 2008}}
* {{cite news |author=Tweedie, Neil |title=For internet sale: aircraft carrier, only three owners |url=http://www.telegraph.co.uk/news/worldnews/northamerica/usa/1451338/For-internet-sale-aircraft-carrier%2C-only-three-owners.html |work=The Daily Telegraph |location=London |id= |date=10 January 2004 |accessdate=11 July 2008}}

==External links==
{{Commons category|NAeL Minas Gerais}}
* [http://www.hms-vengeance.co.uk/farewell.htm HMS Vengeance website - photos of transport and breakup]
* [http://www.fleetairarmarchive.net/vengeance/Index.html Friends of HMS Vengeance Campaign]
* [http://www.naval.com.br/historia/a_compra/vengeance.htm The 'Vengeance' acquisition (Portuguese version only)]
* [http://www.naval.com.br/biblio/NAeL_Minas/dossie01.htm NAeL ''Minas Gerais'' - Poder Naval OnLine (Portuguese version only)]

{{1942 design aircraft carrier}}

{{DEFAULTSORT:Minas Gerais}}
[[Category:Colossus-class aircraft carriers of the Brazilian Navy]]
[[Category:Cold War aircraft carriers of Brazil]]
[[Category:1944 ships]]
[[Category:EBay listings]]
[[Category:Ships built by Swan Hunter]]