{{Use dmy dates|date=April 2014}}
{{Infobox video game
| image         = Ancient Trader logo.png
| developer     = 4Kids Games
| publisher     = 4Kids Games
| engine        = [[Microsoft XNA]]
| platforms     = [[iOS]], [[Microsoft Windows]], [[Xbox 360]]
| released      = '''Microsoft Windows, Xbox Live'''{{Video game release|WW|27 June 2010}}'''iOS'''{{Video game release|WW|17 December 2010}}
| genre         = [[Turn-based strategy game|Turn-based strategy]]
| modes         = [[Single-player video game|Single-player]], [[Multiplayer video game|multiplayer]]
| designer      = Peter Levius
| composer      = Milan Malik
}}

'''''Ancient Trader''''' is a [[Turn-based strategy game|turn-based strategy]] video game developed by the [[Slovakia]]n [[video game studio|studio]] 4Kids Games. It was released in 2010 for [[Microsoft Windows]], [[Xbox Live]] on [[Xbox 360]], and [[iOS]]. The player controls a ship, allowing them to explore the world and engage in [[trading]] while seeking three artifacts that will help to defeat the game's main antagonist, a sea creature called the Ancient Guardian.

The game design was influenced by [[board game]]s and the video games ''[[Elite (video game)|Elite]]'' and ''[[Advance Wars]]''. The game was developed using [[Microsoft XNA]], and took one year to complete with a team of six people. Lead designer Peter Levius worked with the artist Petr Vcelka on the game's [[graphic design]], and with Milan Malik on the game's score. ''Ancient Trader'' received a positive response from critics, who praised the art design and overall gameplay. A sequel, ''Fortune Winds: Ancient Trader'', was developed by Legendo Entertainment and released in 2012 for Microsoft Windows.

==Gameplay==
''Ancient Trader'' is a [[Turn-based strategy game|turn-based strategy]] video game played from a [[2D computer graphics|two-dimensional perspective]]. The player controls a ship and seeks out three artifacts to defeat the game's [[Boss (video gaming)#Final_boss|final boss]], a sea creature called the Ancient Guardian.<ref name="euronet"/> The player explores the map to gather items, including tea, spices, and fruit, all of which can be found in wrecks.<ref name=fidgit/> These can be exchanged for gold, which can be used to buy upgrades for the ship.<ref name=IGN-aug2010/> The commodities are stored in the cargo hold, the capacity of which can be upgraded with gold.<ref name=fidgit/>

[[File:Ancient Trader gameplay elements.png|thumb|right|''Ancient Trader''{{'s}} gameplay is split in two parts. The first, turn-based strategy mode (''above'') is experienced while playing most of the game. The second, a card minigame (''below''), is triggered with a battle between the player and a non-playable character.]]
At the beginning of the game, the entire map is obscured until explored by the player. The map consists of a [[landlocked]] sea with several islands, wreckage, sea creatures, enemy ships, ports, and whirlpools. The player can sometimes encounter [[message in a bottle|message bottles]] that clear away [[fog of war#Simulations and games|fog of war]] on the map to reveal hidden ports.<ref name="euronet"/>Sea creatures and enemy ships appear randomly and challenge the player for gold or commodities in a card [[minigame]].<ref name=IGN-aug2010/> Ports are trading and safe zones where the player can sell commodities, buy upgrades for the ship, and take on [[quest (video gaming)|sidequests]].<ref name="euronet"/><ref name=Indiegamerev/> Whirlpools teleport the player's ship to a random location on the map.

The ship can be moved horizontally and vertically, but not diagonally. The player makes a set number of [[game mechanics#movement|steps]] each turn, after which the [[Artificial intelligence (video games)|artificial intelligence]] does the same for [[non-playable character]]s (NPCs). If the player's ship is not docked at a port at the end of a turn, the player can be attacked by an enemy ship or a sea creature. If they are attacked, a card minigame is triggered to decide if the player loses gold to a rival ship or cargo to a sea creature.<ref name="euronet"/> The player and NPC draw coloured and numbered cards; the highest-numbered card wins each turn, unless presented against a powerful color. The strongest hue receives an attack bonus.<ref name="euronet"/> The player's following turn starts after the minigame is completed.<ref name="euronet"/>

After sufficiently upgrading the ship and exploring the map, the player is allowed to buy the three powerful artifacts, which then [[Spawning (video gaming)|spawn]] at random ports. Defeating the Ancient Guardian in a card minigame awards the player additional loot and previously unavailable upgrades.<ref name=Indiegamerev/> ''Ancient Trader'' does not include a [[Game save|saving feature]], which means that all progress is lost if the game is closed.<ref name="euronet" /> An [[online multiplayer]] mode allows for several players to simultaneously play the same map, chase the artifacts, and defeat the Guardian. The prices of the artifacts increase as other players buy them.<ref name="fidgit" /> The players can check the wealth and artifacts gained by other players.<ref name="euronet" /> The game also includes several alternative [[game modes]], which focus on reaching a set total wealth or cash tally before other players. The multiplayer does not include an online scoreboard.<ref name="euronet" />

==Development==
''Ancient Trader'' is the first video game developed by the [[Slovakia]]n studio 4Kids Games.<ref name=xboxhornet/> The development team consisted of the lead designer Peter Levius, the artist Petr Vcelka, the game designer Miroslav Petrasko, and the programmer Andrej Vakrcka.<ref name=xboxhornet/> The score was composed by Milan Malik, while Jan Ohajsky designed and animated the graphics.<ref name="Meta1"/> The game was developed using [[Microsoft XNA]], a set of [[video game development|game development]] tools. According to Levius, ''Ancient Trader'' took around one year to finish; "most of our team members [...] were working hard on other higher priority projects at the same time".<ref name=xboxhornet/> The game was submitted for XNA approval on 2 June 2010.<ref name=leviusblog/>

''Ancient Trader''{{'}}s design was influenced by that of [[board game]]s and of the video games ''[[Elite (video game)|Elite]]'' and ''[[Advance Wars]]''.<ref name=xboxhornet/> Levius and his girlfriend dedicated around two months to test and balance the game's mechanics, gathering groups of friends to play the game "without explaining anything to see if they can understand the rules and controls".<ref name=xboxhornet/> He worked with Vcelka to design ''Ancient Trader''{{'}}s appearance, including paper textures and clouds. He revealed that some of the maps' features came from a scan of an old military map of present-day Slovakia.<ref name=xboxhornet/> According to Chris Schilling from [[Eurogamer]], Vcelka took inspiration from sixteenth and seventeenth-century cartography, as well as from [[Abraham Ortelius]] and his ''[[Theatrum Orbis Terrarum]]'', to create "exceptionally detailed art".<ref name="euronet"/>

For the Xbox 360 version, the team introduced a feature to reduce the colour saturation in the game and allow players to decide how much colour they wanted to have. Levius acknowledged that this feature "is a big thing for me and Petr as we always wanted to also have a black and white version of the game in style of [[Jules Verne]]'s illustrations".<ref name=xboxhornet/> He also commented that several other features, such as more animations and leaderboards, were left out due to a lack of time, but he kept them in mind for a possible sequel.<ref name=xboxhornet/>

==Reception==
{{Video game reviews
| EuroG = 8/10<ref name=euronet/>
| rev1 = Eurogamer Italy
| rev1Score = 8/10<ref name=euroitaly/>
}}
''Ancient Trader'' received positive responses from several [[video game journalism|video game journalists]] upon its release. Most critics praised the game's art design and gameplay, but criticized the lack of key elements such as a saving feature and scoreboards. The British magazine ''[[Edge (magazine)|Edge]]'' included ''Ancient Trader'' in its 2010 list of the Best 20 Indie Games available in the Xbox Live Marketplace. They acknowledged that the game was "ambitious, devious and surprisingly hard to fault".<ref name="edge20"/> [[IGN]] staff called it "a simple, easy entry strategy game suitable for all ages".<ref name=IGN-aug2010/>

[[Tom Chick]] from FidGit compared ''Ancient Trader'' to video games such as [[The Seven Cities of Gold (video game)|''The Seven Cities of Gold'']] and [[Sid Meier's Pirates! (2004 video game)|''Sid Meier's Pirates!'']], as well as to other indie games such as "the sci-fi space operettas ''[[Strange Adventures in Infinite Space]]'' and [[Flotilla (video game)|''Flotilla'']], but wind-powered and sepia-toned".<ref name=fidgit/> Like most reviewers, Chick highly praised the artistic design, as well as the game's atmospheric music and vividness of the environment: "It's all ink and vellum and cursive script and layers of lovingly drawn facades drawn on plywood and stacked up for a 19th Century stage production."<ref name=fidgit/>

Eurogamer's Chris Schilling commented that although ''Ancient Trader'' was not as comprehensive and expansive as other Xbox Live titles such as ''[[Risk: Factions]]'', it deserved "the opportunity to do business with the big boys rather than risk getting washed away with the shovelware tide". Schilling expressed concern that "a game so elegant and accomplished should have to be dredged up from the depths of Indie Games".<ref name="euronet"/> Lorenzo Fantoni from Eurogamer Italy awarded ''Ancient Trader'' the same score as Schilling, and explained that "the final result is a quite simple title, which will last long enough to make you feel happy of having invested your money in this indie title".<ref name=euroitaly/>

Gus Mastrapa from [[Wired (magazine)|''Wired'']] commented that ''Ancient Trader'' was well worth its price "for its art alone". He praised the game's overall style, "borrowed from centuries-old maps ... more handsome than any other game you'll find in the Xbox Live indie category".<ref name=wired/>

== Sequel ==
A sequel, ''Fortune Winds: Ancient Trader'', was developed by Swedish studio [[Legendo Entertainment]] and released in July 2012 for Microsoft Windows and [[Mac OS X]].<ref name=legendo/><ref name=gamasutraFW/> ''Fortune Winds'' includes improved AI, new player avatars and a save feature.<ref name="FWATMC"/> ''[[GamesMaster (magazine)|GamesMaster]]'' was unimpressed with the sequel. The magazine awarded it 69 out of 100 and commented that it deviated too much from the original indie game.<ref name="GM"/>

==References==
{{reflist|30em|refs=
<ref name="GM">{{cite news|title=''Fortune Winds: Ancient Trader'' review|date=October 2012|page=92|work=[[GamesMaster (magazine)|GamesMaster]]|publisher=[[Future Publishing]]}}</ref>

<ref name=gamasutraFW>{{cite web
|title=Fortune Winds: Ancient Trader out now on PC; available on Origin and Gamersgate
|url=http://gamasutra.com/view/pressreleases/173701/Fortune_Winds_Ancient_Trader_out_now_on_PC_available_onOrigin_and_Gamersgate.php
|work=[[Gamasutra]]
|publisher=UBMTech
|accessdate=7 April 2014
|date=6 July 2012
}}</ref>

<ref name=legendo>{{cite web
|title=Fortune Winds: Ancient Trader
|url=http://www.legendo.com/fortune-winds-ancient-trader/
|publisher=[[Legendo Entertainment]]
|accessdate=7 April 2014
}}</ref>

<ref name="FWATMC">{{cite web
|title=Fortune Winds: Ancient Trader
|url=http://www.metacritic.com/game/pc/fortune-winds-ancient-trader
|work=[[Metacritic]]
|publisher=[[CBS Interactive]]
|accessdate=7 April 2014
}}</ref>

<ref name=leviusblog>{{cite web
|last=Levius
|first=Peter
|title=''Ancient Trader'', finally submitted to XNA!
|url=http://peter-levius.blogspot.com/2010/06/ancient-trader-finally-submitted-to-xna.html
|format=blog post
|publisher=Peter Levius's Blog
|accessdate=4 April 2014
|date=3 June 2010
|archiveurl=https://web.archive.org/web/20160304125907/http://peter-levius.blogspot.com/2010/06/ancient-trader-finally-submitted-to-xna.html
|archivedate=4 March 2016
}}</ref>

<ref name="Meta1">{{cite web
|title=''Ancient Trader'' credits and details for PC
|url=http://www.metacritic.com/game/pc/ancient-trader/details
|work=[[Metacritic]]
|publisher=[[CBS Interactive]]
|accessdate=4 April 2014
}}</ref>

<ref name=xboxhornet>{{cite web
|last=WDesm
|title=XBLA Indie Interview&nbsp;– Fourkidsgames
|url=http://www.xboxhornet.com/wordpress/?p=4691
|publisher=XboxHornet
|accessdate=4 April 2014
|date=13 July 2010
}}</ref>

<ref name=fidgit>{{cite web
|last=Chick
|first=Tom
|title=Who would believe the astonishing beauty of ''Ancient Trader''?
|url=http://fidgit.com/archives/2010/04/ancient_trader.php
|work=FidGit
|publisher=[[Sci Fi Channel (United States)|Sci Fi]]
|accessdate=28 March 2014
|archiveurl=https://web.archive.org/web/20100503135952/http://fidgit.com/archives/2010/04/ancient_trader.php
|archivedate=3 May 2010
|date=30 March 2010
}}</ref>

<ref name="edge20">{{cite web
|last=''Edge'' staff
|title=Best 20 Indie Games
|url=http://www.edge-online.com/review/best-20-indie-games/
|work=Edge
|publisher=Future Publishing Limited
|accessdate=6 May 2013
|archiveurl=https://web.archive.org/web/20130404025246/http://www.edge-online.com/review/best-20-indie-games/
|archivedate=4 April 2013
|date=16 November 2010
}}</ref>

<ref name="euronet">{{cite web
|last=Schilling
|first=Chris
|title=Ancient Trader
|url=http://www.eurogamer.net/articles/2010-08-09-ancient-trader-360-review
|work=[[Eurogamer]]
|publisher=Gamer Network
|accessdate=19 March 2014
|archiveurl=
|archivedate=
|date=9 August 2010
}}</ref>

<ref name=euroitaly>{{cite web
|last=Fantoni
|first=Lorenzo
|title=Ancient Trader
|url=http://www.eurogamer.it/articles/2010-08-09-ancient-trader-recensione?page=2
|work=[[Eurogamer]]
|publisher=Gamer Network
|accessdate=19 March 2014
|location=Italy
|page=2
|language=Italian
|date=10 August 2010
|archivedate=25 July 2012
|archiveurl=https://web.archive.org/web/20120725121655/http://www.eurogamer.it/articles/2010-08-09-ancient-trader-recensione
}}</ref>

<ref name=IGN-aug2010>{{cite web
|last=IGN staff
|title=Beautiful Looking Indie-Game Released on PC and Xbox 360
|url=http://www.ign.com/articles/2010/08/03/beautiful-looking-indie-game-released-on-pc-and-xbox-360
|work=[[IGN]]
|publisher=[[J2 Global]]
|accessdate=19 March 2014
|date=3 August 2010
}}</ref>

<ref name=Indiegamerev>{{cite web
|title=''Ancient Trader''&nbsp;– It's the simple things in life
|url=http://indiegamereviewer.com/ancient-trader-its-the-simple-things-in-life/
|publisher=Indie Game Reviewer
|accessdate=19 March 2014
|date=13 July 2010
}}</ref>

<ref name=wired>{{cite web
|last=Mastrapa
|first=Gus
|title=''Ancient Trader'' is a Hidden Xbox Treasure
|url=https://www.wired.com/2010/07/ancient-trader
|work=[[Wired (magazine)|Wired]]
|publisher=Condé Nast
|accessdate=25 March 2014
|date=1 July 2010
}}</ref>

}}
{{portal bar|Video games|2010s}}
{{good article}}

[[Category:2010 video games]]
[[Category:Art games]]
[[Category:Turn-based strategy video games]]
[[Category:Video games developed in Slovakia]]
[[Category:Windows games]]
[[Category:Xbox 360 Live Indie games]]
[[Category:IOS games]]
[[Category:Multiplayer and single-player video games]]
[[Category:Trade simulation games]]
[[Category:Naval video games]]