{{Multiple issues|
{{prose|date=December 2011}}
{{primary sources|date=December 2011}}
{{advert|date=December 2011}}
}}

{{Infobox company
| name             = Plante Moran
| logo             = 
| type             = Professional Limited Liability Company
| genre            =
| fate             =
| predecessor      =
| successor        =
| foundation       = January 20th, 1924
| founder          = Elorion Plante and Frank Moran
| defunct          =
| location_city    = Southfield, MI
| location_country = USA
| location         =
| locations        =
| area_served      =
| key_people       = Gordon Krater, ([[Firm Managing Partner]])<br>Frank Audia, Ron Eckstein, Jim Proppe, Bruce Shapiro, Craig Thornton ([[Group Managing Partners]])
| industry         = Professional Services
| products         =
| production       =
| services         = Accounting, Consulting, Wealth Management services
| revenue          = $404 million (2014)
| operating_income =
| net_income       =
| aum              =
| assets           =
| equity           =
| owner            =
| num_employees    = More than 2,000
| parent           =
| divisions        =
| subsid           =
| homepage         = [http://www.plantemoran.com]
| footnotes        =
| intl             =
}}
'''Plante Moran''' is the 14th largest certified public accounting and business advisory firm in the United States<ref>[http://www.plantemoran.com/about/pages/facts-figures.aspx]</ref> offering [[audit]], [[accounting]], [[tax]] and business advisory consulting services. Plante Moran employs more than 2,000 people and has 22 offices through Michigan, Ohio, Illinois, and in Mexico, India and China.<ref>[http://www.plantemoran.com/about/locations/Pages/Home.aspx Plante Moran Locations]</ref>

Plante Moran is a member of Praxity, an international association of independent accounting firms. This alliance is an association of independent firms in the major markets of North America, South America, Europe, and Asia.<ref>[http://www.praxity.com/Pages/default.aspx Praxity Website]</ref> Other firms associated with Praxity include [[BKD]] LLP, [[Moss Adams]] LLP and [[Dixon Hughes Goodman]] LLP.<ref>[http://www.praxity.com/Directory/Pages/Directory.aspx?Admin/PraxityOffices.Region=United+States Praxity Member Directory]</ref>

==History==
Key Dates:
* 1924: Elorion Plante founds accounting firm in Detroit, Michigan
* 1950: Frank Moran is named partner; firm becomes known as Plante & Moran.
* 1955: Moran becomes managing partner
* 1960s: Consulting work grows to account for one-third of revenues
* 1977: Financial Planning Group is founded as Total Personal Financial Planning (TPFP)
* 1981: Edward Parks is chosen as managing partner; Moran is named chairman
* 1984: Merger with Lansing, Michigan firm also adds offices in three Michigan cities (Kalamazoo, Battle Creek, Benton Harbor/St. Joseph) and Cleveland, Ohio
* 1993: William Matthews becomes managing partner
* 1994: Plante Moran CRESA is formed
* 1996: P&M Corporate Finance is founded
* 1997: Frank Moran dies; merger occurs with NCOT Accounting and Consulting of Ohio.
* 1998: Plante & Moran Benefits Administration LLC is founded
* 1999: Firm receives first of 17 consecutive honors as one of [[Fortune (magazine)|''Fortune'']] magazine's “100 Best Companies to Work For”<ref>{{cite web|title=Plante Moran Named to FORTUNE’s list of “The 100 Best Companies to Work For” 17 years in a row|url=http://www.plantemoran.com/about/media/2015/Pages/plante-moran-named-to-FORTUNE-list-of-the-100-best-companies-to-work-for-17-years-in-a-row.aspx|website=http://www.plantemoran.com/|accessdate=2015-04-04}}</ref>
* 2001: William Hermann takes role of managing partner
* 2002: Firm is chosen to help investigate [[Enron]] collapse; Plante Moran Trust is created
* 2004: Merger with Gleeson, Sklar, Sawyers and Cumpata (GSS&C) gives firm three Illinois offices;<ref>[http://www.accountingweb.com/item/98601 Plante Moran Merges with Gleeson, Sklar, Sawyers and Cumpata (GSS&C)]</ref> firm also merges with AFME, Inc.<ref>[http://www.accountingtoday.com/news/4713-1.html Accounting Today article: Plante Moran Merges with AFME, Inc.]</ref>
* 2009: Gordon Krater becomes managing partner<ref>[http://www.accountingtoday.com/news/Gordon-Krater-51674-1.html Accounting Today article: Gordon Krater Becomes Managing Partner]</ref>
* 2011: Plante & Moran Drops the &; becomes Plante Moran<ref>[http://www.plantemoran.com/about/media/2011/pages/plante-moran-bids-farewell-to-ampersand.aspx Plante & Moran Drops the &, Becomes Plante Moran]</ref>
* 2011: Former managing partner William Hermann and current Managing Partner Gordon Krater co-author Succession Transition: A Roadmap for Seamless Transitions in Leadership <ref>[http://www.momentumbooks.com/MOMENTUM_BOOKS/Momentum_Books___Home_Page.html Book Publisher Momentum Books web page for A Roadmap for Seamless Transitions in Leadership]</ref>
* 2012: Merger with Stuart Franey Matthews & Chantres P.C.<ref>[http://www.plantemoran.com/about/media/2012/Pages/stuart-franey-matthews-chantres-to-merge-with-plante-moran.aspx Merger with Stuart Franey Matthew & Chantres ]</ref> 
* 2012: Plante Moran named to ''Fortune'' Magazine’s list of “The 100 Best Companies to Work for”<ref>[[Plante Moran named "100 Best Companies to Work for"]]{{Incomplete short citation|date=January 2014}}</ref>
* 2012: Plant Moran wins First-Ever Global Workplace Recognition with International Accounting Bulletin’s Employer of the Year award<ref>[[Plante Moran wins Global Workplace Recognition]]{{Incomplete short citation|date=January 2014}}</ref>
* 2012: Merger with Blackman Kallick<ref>[http://www.chicagobusiness.com/article/20120626/NEWS04/120629853/blackman-kallick-to-merge-with-michigan-accounting-firm-plante-moran Merger with Blackman Kallick]</ref> 
* 2012: Financial advisors at Plante Moran celebrate 35 years serving clients<ref>[[Financial advisors celebrate 35 years]]{{Incomplete short citation|date=January 2014}}</ref>
* 2013: Plante Moran becomes 11th largest accounting firm in the US
* 2013: Plante Moran named to ''Fortune'' Magazine’s list of “The 100 Best Companies to Work for”; 15th consecutive year<ref>[[Plante Moran named "100 Best Companies to Work for" by Fortune Magazine]]{{Incomplete short citation|date=January 2014}}</ref>
* 2013: Plante Moran launched ‘Women in Leadership’ initiative<ref>[[Plante Moran's 'Women in Leadership' initiative]]{{Incomplete short citation|date=January 2014}}</ref>
* 2013: Plante Moran announces opening of Detroit office<ref>[[Plante Moran to open Detroit office]]{{Incomplete short citation|date=January 2014}}</ref>

==Services==
<ref>[http://www.plantemoran.com/about/pages/facts-figures.aspx Plante Moran Services]</ref>

* Audit and Accounting
* Business Advisory Services
* Financial Advisory Services
* Human Capital
* International Business Services
* Operational Effectiveness
* Tax
* Technology

==Affiliated services==
* P&M Corporate Finance <ref>[http://www.pmcf.com/pmcf/about/pages/our-firm.aspx P&M Corporate Finance]</ref>
* Plante Moran Financial Advisors<ref>[http://www.pmfa.com/pmfa/pages/default.aspx Plante Moran Financial Advisors]</ref>
* Plante Moran Trust <ref>[http://www.pmtrust.com/pmtrust/pages/default.aspx Plante Moran Trust]</ref>
* Plante Moran CRESA, LLC <ref>[http://www.plante-moran.com/pmcresa/pages/home.aspx Plante Moran CRESA, LLC]</ref>
* Plante Moran Group Benefit Advisors <ref>[http://www.plantemoran.com/services/humancapital/employee-benefits-consulting/employee-health-and-welfare-consulting/pages/home.aspx]</ref>

==Awards==
* For seventeen consecutive years up to 2015, Plante Moran has been named on the [[Fortune (magazine)|''Fortune'']] list “The 100 Best Companies to Work For”.<ref>{{cite web|title=Plante Moran Named to Fortune’s list of “The 100 Best Companies to Work For” 17 years in a row|url=http://www.plantemoran.com/about/media/2015/Pages/plante-moran-named-to-FORTUNE-list-of-the-100-best-companies-to-work-for-17-years-in-a-row.aspx|website=http://www.plantemoran.com/|accessdate=2015-04-04}}</ref>
* Accounting Today — In August 2015, Plante Moran Financial Advisors was ranked #1 by Accounting Today on its ninth annual ranking of CPA firms by assets under management. 
* Fortune Magazine’s “100 Best Workplaces for Women” — In September 2015, Plante Moran was named one of the “100 Best Workplaces for Women” by Fortune.com and Great Place to Work. 
* fortune magazine — In June 2015, Fortune magazine named Plante Moran one of the “100 Best Workplaces for Millennials.” 
* Crain’s “Fast 50” — In August 2015, Plante Moran was included on the Crain’s Detroit Business “Fast 50” list, a list of the area’s fastest growing companies.
* Crain’s Chicago Business Best Places to Work — In April 2015, Crain’s Chicago Business named Plante Moran one of its “Best Workplaces for Gen X.” We ranked #1. 
* Leadership 500 Excellence Awards — In April 2015, Plante Moran was recognized by Leadership Excellence magazine for excellence in its leadership development programs. This was the fifth consecutive year we made the “Leadership 500.”
* 2015 Best Public Accounting Firms for Women — In June 2015, firms were ranked on the range, depth, and success of programs and workplace culture proven to remove barriers to women's success, especially at midlevel and above.  The winners were drawn from firms that participated in the 2014 Accounting MOVE Project. This is the fifth consecutive year we received this distinction.
* Barron’s — In August 2015, Barron’s ranked John Lesser, President of Plante Moran Financial Advisors, #79 on its list of the Top 100 Independent Wealth Advisors.
* Vault Guide to the Top 50 Accounting Firms — In April 2015, Vault.com released its annual ranking of the “Best Accounting Firms to Work For.” Plante Moran ranked #8 overall. 
* 101 Best and Brightest Companies to Work For — In April 2015, Plante Moran was named one of “West Michigan’s 101 Best and Brightest Companies to Work For.” This was the 10th consecutive year we received the award.
* Inside Public Accounting’s “Most Admired Peers” — In October 2014, Inside Public Accounting unveiled its 2014 list of “Most Admired Peers.” Managing Partner Gordon Krater was among those named to this prestigious list. 
* Vault’s “Best Overall Internships” — In October 2014, Vault.com released its annual list of “Best Overall Internships.” Plante Moran ranked sixth.
* Detroit Free Press “Top Workplace” — In September 2014, Plante Moran was named a 2014 Detroit Free Press Top Workplace. We ranked fourth in the “large company” category. This marked the fifth consecutive year we made the list.
* NorthCoast 99 — In September 2014, Plante Moran was named a winner of the prestigious NorthCoast 99 award, which honors 99 great workplaces for top talent in Northeast Ohio. This is the third consecutive year we made the list.
* Forbes’ List of “Top 50 Wealth Managers”— In May 2014, PMFA was named #5 on Forbes’ list of “Top 50 Wealth Managers.” 
* Toledo Blade Top Workplaces — In January 2014, Plante Moran was named one of the Toledo Blade’s “Top Workplaces in Northwest Ohio.” We were #4 in the small business category and the top rated accounting firm on the list.
* Boutique Investment Banking Firm of the Year — In May 2013, P&M Corporate Finance was selected as the winner of the Boutique Investment Banking Firm of the Year award by M&A Advisor for its notable success in serving the investment banking needs of middle market businesses in 2012. The award is the second for PMCF, which was last awarded Boutique Investment Bank of the Year in 2010.
* Employer of the Year — In March 2013, International Accounting Bulletin awarded Plante Moran “Employer of the Year.” This was the second consecutive year we received this honor.
* “National Top 150 Workplaces” — In January 2013, Plante Moran was ranked #19 on WorkPlace Dyamics’ list of “National Top 150 Workplaces.” The list was compiled from 872 organizations with more than 1,000 employees that participated in regional top workplaces awards programs.
* Central Ohio’s 2012 Best Places to Work — In November 2012, Plante Moran was named to Columbus Business First’s list of “Central Ohio’s 2012 Best Places to Work.” We ranked first in the “medium company” category. This was the second consecutive year we made the list.
* National Association for Business Resources — In November 2012, Plante Moran was named to the National Best and Brightest Companies to Work For list. Only companies with the highest quality human resources initiatives can make the list.
* InformationWeek 500 — In September 2012, Plante Moran placed #128 on InformationWeek 500’s List of Top Technology Innovators Across America. 
* Financial Advisor magazine — In July 2012, Plante Moran Financial Advisors (PMFA), an independent investment advisory firm, was ranked in the top 10 of registered investment advisors (RIAs) by Financial Advisor magazine for the sixth year in a row.
* America’s Top 10 Workplaces — In May 2012, Plante Moran ranked seventh on WorkplaceDynamics’ list of “America’s Top Workplaces.” WorkplaceDynamics compiled the list based on employee satisfaction surveys it designs and conducts for major newspapers, including the Detroit Free Press, across the country. 
* Crain’s “Chicago’s Best Workplaces for Women” — In May 2012, Crain’s Chicago Business named Plante Moran seventh on its list of “Chicago’s Best Workplaces for Women.” 
* Huntington Pillar Award — In March 2011, Plante Moran’s Grand Rapids office received a Huntington Pillar Award from the Women’s Resource Center. The award honors West Michigan employers that empower women in their workforce with progressive policies and practices. Plante Moran was recognized for its decades-long focus on work-life balance and “intentionally creating policies and a culture that keeps their staff happy.”
* In 2010 the ''[[Detroit Free Press]]'' ranked Plante Moran as a "Top Workplace."<ref>[http://www.topworkplaces.com/survey/resultslist/detroit_10 Detroit Free Press Ranks Plante Moran as "Top Workplace"]</ref>
* 2011 101 Best & Brightest Companies to Work For - "Winner, West Michigan"<ref>[http://www.101bestandbrightest.com/winners/2011/west-michigan Plante Moran Named One of West Michigan's 101 Best & Brightest Companies to Work For]</ref>
* 2010 [[Crain Communications|Crain's]] Chicago Business "Best Places to Work" winner

==References==
{{Reflist|2}}

<!--- Categories --->
[[Category:Accounting firms of the United States]]
[[Category:Companies established in 1924]]