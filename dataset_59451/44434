{{Infobox NCAA team season
  |Year=1937
  |Team=Alabama Crimson Tide
  |Image=
  |ImageSize=
  |Conference=Southeastern Conference
  |ShortConference=SEC
  |Record=9–1
  |ConfRecord=6–0
  |APRank=4
  |HeadCoach=[[Frank Thomas (American football)|Frank Thomas]]
  |HCYear = 7th
  |Captain =[[Leroy Monsky]]
  |StadiumArena=[[Bryant–Denny Stadium|Denny Stadium]]<br/>[[Legion Field]]
  |Champion=SEC champion
  |BowlTourney=[[1938 Rose Bowl|Rose Bowl]]
  |BowlTourneyResult=L 0–13 vs. [[1937 California Golden Bears football team|California]]
}}
{{1937 SEC football standings}}
The '''1937 Alabama Crimson Tide football team''' (variously "Alabama", "UA" or "Bama") represented the [[University of Alabama]] in the [[1937 college football season]]. It was the Crimson Tide's 44th overall and 5th season as a member of the [[Southeastern Conference]] (SEC). The team was led by head coach [[Frank Thomas (American football)|Frank Thomas]], in his seventh year, and played their home games at [[Bryant–Denny Stadium|Denny Stadium]] in [[Tuscaloosa, Alabama|Tuscaloosa]] and [[Legion Field]] in [[Birmingham, Alabama|Birmingham]], [[Alabama]]. They finished the season with a record of nine wins and one loss (9–1 overall, 6–0 in the SEC), as SEC champions and with a loss against [[1937 California Golden Bears football team|California]] in the [[1938 Rose Bowl]].

The Crimson Tide opened the season with three consecutive [[shutout]]s against {{cfb link|year=1937|team=Howard Bulldogs|title=Howard}}, {{cfb link|year=1937|team=Sewanee Tigers|title=Sewanee}} and {{cfb link|year=1937|team=South Carolina Gamecocks|title=South Carolina}}. In their fourth game, Alabama surrendered their first points of the season on defense in their 14–7 victory over [[1937 Tennessee Volunteers football team|Tennessee]]. They then shutout their next two opponents, {{cfb link|year=1937|team=George Washington Colonials|title=George Washington}} and [[1936 Kentucky Wildcats football team|Kentucky]] prior to their game at [[1937 Tulane Green Wave football team|Tulane]]. Against the Green Wave, the Crimson Tide won 9–6 on a game-winning fourth-quarter field goal by [[Hayward Sanford]]. After their sixth shutout of the season against [[1937 Georgia Tech Yellow Jackets football team|Georgia Tech]], Alabama won their second game of the season on a fourth quarter Sanford field goal against [[1937 Vanderbilt Commodores football team|Vanderbilt]], and clinched the SEC championship with the win. With their undefeated regular season, Alabama accepted an invitation to play in the [[1938 Rose Bowl]] where they lost 13–0 to [[1937 California Golden Bears football team|California]].

==Before the season==
After the 1936 season, the first expansion of [[Bryant–Denny Stadium|Denny Stadium]] was undertaken. The stadium originally opened for the [[1929 Alabama Crimson Tide football team|1929 season]], and the concrete stands had a [[seating capacity]] of 12,000.<ref name="Denny">{{cite news |title=Denny Stadium |url=https://news.google.com/newspapers?id=ftY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6504%2C4372156 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 8, 1937 |page=4 |accessdate=April 12, 2012}}</ref> The 1937 expansion included the construction of a 6,000 seat eastern addition that was utilized primarily by students. Its construction was financed with a combination of funding from both the university ($140,000) and a grant from the [[Public Works Administration]] ($90,000).<ref name="Denny"/> Designed after the [[Yale Bowl]], at the time of this expansion the school envisioned a build-out of Denny Stadium at a capacity of 66,000.<ref name="Denny"/>

==Schedule==
{{CFB Schedule Start | time = no | rankyear = 1937 | tv = no | attend = yes }}
{{CFB Schedule Entry
| w/l          = w
| date         = September 25
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1937|team=Howard Bulldogs|title=Howard}}
| site_stadium = [[Bryant–Denny Stadium|Denny Stadium]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 41–0
| attend       = 7,500
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 2
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1937|team=Sewanee Tigers|title=Sewanee}}
| site_stadium = [[Legion Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| tv           = no
| score        = 65–0
| attend       = 7,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 9
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1937|team=South Carolina Gamecocks|title=South Carolina}}
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 20–0
| attend       = 9,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 16
| away         = yes
| time         = no
| rank         = 
| opponent     = [[1937 Tennessee Volunteers football team|Tennessee]]
| site_stadium = [[Neyland Stadium|Shields-Watkins Field]]
| site_cityst  = [[Knoxville, Tennessee|Knoxville, TN]]
| gamename     = [[Third Saturday in October]]
| tv           = no
| score        = 14–7
| attend       = 25,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 23
| away         = yes
| nonconf      = yes
| time         = no
| rank         = 2
| opponent     = {{cfb link|year=1937|team=George Washington Colonials|title=George Washington}}
| site_stadium = [[Griffith Stadium]]
| site_cityst  = [[Washington, D.C.|Washington, DC]]
| tv           = no
| score        = 19–0
| attend       = 24,666
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 30
| homecoming   = yes
| time         = no
| rank         = 3
| opponent     = [[1937 Kentucky Wildcats football team|Kentucky]]
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 41–0
| attend       = 13,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 6
| away         = yes
| time         = no
| rank         = 2
| opprank      = 19
| opponent     = [[1937 Tulane Green Wave football team|Tulane]]
| site_stadium = [[Tulane Stadium]]
| site_cityst  = [[New Orleans|New Orleans, LA]]
| tv           = no
| score        = 9–6
| attend       = 30,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 13
| time         = no
| rank         = 3
| opponent     = [[1937 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 7–0
| attend       = 26,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 25
| away         = yes
| time         = no
| rank         = 4
| opprank      = 12
| opponent     = [[1937 Vanderbilt Commodores football team|Vanderbilt]]
| site_stadium = [[Vanderbilt Stadium|Dudley Field]]
| site_cityst  = [[Nashville, Tennessee|Nashville, TN]]
| tv           = no
| score        = 9–7
| attend       = 22,000
}}
{{CFB Schedule Entry
| w/l          = l
| date         = January 1, 1938
| nonconf      = yes
| neutral      = yes
| time         = no
| rank         = 4
| opprank      = 2
| opponent     = [[1937 California Golden Bears football team|California]]
| site_stadium = [[Rose Bowl (stadium)|Rose Bowl]]
| site_cityst  = [[Pasadena, California|Pasadena, CA]]
| gamename     = [[1938 Rose Bowl|Rose Bowl]]
| tv           = no
| score        = 0–13
| attend       = 87,000
}}
{{CFB Schedule End
| rank     = 
| timezone = 
| poll     = [[AP Poll]]
| hc       = yes
}}
*<small>Source: Rolltide.com: 1937 Alabama football schedule<ref name="1937schedule">{{cite web| url=http://www.rolltide.com/sports/m-footbl/archive/m-footbl-results-archive.html#1937 |title=1937 Alabama football schedule |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics| accessdate=April 9, 2012}}</ref></small>

==Game summaries==

===Howard===
{{See also2|{{cfb link|year=1937|team=Howard Bulldogs}}}}
{{AFB game box start
|Visitor=Howard
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=14 |H2=14 |H3=7 |H4=6
|Date=September 25
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=7,500
}}
*'''Source:'''<ref name="HU1">{{cite news |title=Relentless Crimson Tide inundates Howard, 41–0, in opener |url=https://news.google.com/newspapers?id=c9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6394%2C3802439 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 26, 1937 |page=10 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
To open the 1937 season, Alabama outgained [[Howard Bulldogs football|Howard]] (now [[Samford University]]) in total yards 198 to 6, and defeated the Bulldogs 41–0 at Denny Stadium.<ref name="HU1"/><ref name=a1>1937 Season Recap</ref> The Crimson Tide scored a pair of touchdowns in each of the first two quarters to take a 28–0 halftime lead. First-quarter touchdowns were scored on a 21-yard [[Joe Kilgrow]] touchdown pass to George Zivich and on a Perron Shoemaker blocked [[Punt (gridiron football)|punt]] returned 15-yards for the score.<ref name="HU1"/> In the second, touchdowns were scored by Herschel Mosley on a 91-yard punt return and on a 10-yard Mosley to Bud Waites touchdown pass.<ref name="HU1"/> The Crimson Tide then closed the game with a pair of second half touchdowns for the 41–0 victory. [[Charlie Holm]] scored on a four-yard run in the third and Alvin Davis scored on a 19-yard run in the fourth.<ref name="HU1"/> The victory improved Alabama's all-time record against Howard to 15–0–1.<ref name="HAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Samford |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2867 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Sewanee===
{{See also2|{{cfb link|year=1937|team=Sewanee Tigers}}}}
{{AFB game box start
|Visitor=Sewanee
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=13 |H2=27 |H3=13 |H4=12
|Date=October 2
|Location=Legion Field<br/>Birmingham, AL
|Attendance=7,000
}}
*'''Source:'''<ref name="SU1">{{cite news |title=Raging Crimson Tide sweeps over Sewanee in 65 to 0 rout |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=edY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6388%2C4114571 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 3, 1937 |page=8 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
In the conference opener, Alabama defeated the [[Sewanee: The University of the South|Sewanee]] [[Sewanee Tigers football|Tigers]] 65–0 at Legion Field in rainy conditions.<ref name=a1/><ref name="SU1"/> In the game, Alabama outgained the Tigers 600 to 27 yards in total offense with both [[Charlie Holm]] and [[Joe Kilgrow]] each having gained over 100 yards rushing.<ref name="SU1"/> The Crimson Tide also scored ten total touchdowns with Silas Beard, Kilgrow, Herschel Mosley and Billy Slemons each scoring two and both Holm and George Zivich each scoring one in the victory.<ref name="SU1"/> The victory improved Alabama's all-time record against Sewanee to 16–10–3.<ref name="SEWAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Sewanee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2947 |accessdate=April 10, 2012}}</ref>
{{clear}}

===South Carolina===
{{See also2|{{cfb link|year=1937|team=South Carolina Gamecocks}}}}
{{AFB game box start
|Visitor=South Carolina
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=7 |H2=6 |H3=7 |H4=0
|Date=October 9
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=9,000
}}
*'''Sources:'''<ref name="SC1">{{cite news |title=Crimson Tide subdues South Carolina, 20–0, to continue surge |url=https://news.google.com/newspapers?id=f9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6347%2C4431151 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 10, 1937 |page=6 |accessdate=April 10, 2012}}</ref><ref name="SC2">{{cite news |title=Plucky Gamecocks go down before Tide's power, 20–0 |url=https://news.google.com/newspapers?id=z0UsAAAAIBAJ&sjid=z8oEAAAAIBAJ&pg=5101%2C3854829 |agency=Associated Press |publisher=Google News Archives |newspaper=The Spartanburg Herald-Journal |date=October 10, 1937 |page=22 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
Against the [[University of South Carolina|South Carolina]] [[South Carolina Gamecocks football|Gamecocks]] of the [[Southern Conference]] Alabama won 20–0 at Denny Stadium in what was the first all-time meeting between the schools.<ref name=a1/><ref name="SC1"/><ref name="SCT">{{Cite web |last=DeLassus |first=David |title=Alabama vs South Carolina |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3006 |accessdate=April 10, 2012}}</ref> Touchdowns were scored by [[Joe Kilgrow]] on a short run in the first, on a 33-yard Kilgrow pass to Silas Beard in the second and on a one-yard Hal Hughes run in the third.<ref name="SC1"/> 
{{clear}}

===Tennessee===
{{See also|1937 Tennessee Volunteers football team}}
{{AFB game box start
|Title=[[Third Saturday in October]]
|Visitor='''Alabama'''
|V1=0 |V2=7 |V3=7 |V4=0
|Host=Tennessee
|H1=0 |H2=0 |H3=0 |H4=7
|Date=October 16
|Location=Shields-Watkins Field<br/>Knoxville, TN
|Attendance=25,000
}}
*'''Source:'''<ref name="UT1">{{cite news |title=Alabama's Crimson Tide drowns out Tennessee Vols, 14 to 7 |url=https://news.google.com/newspapers?id=hdY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6160%2C4753902 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 17, 1937 |page=10 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
In Knoxville, Alabama defeated [[Third Saturday in October|rival]] [[University of Tennessee|Tennessee]] 14–7 at Shields-Watkins Field one year after their [[1936 Alabama Crimson Tide football team#Tennessee|scoreless tie]] at [[Legion Field]].<ref name=a1/><ref name="UT1"/> The overflow crowd of 25,000 included [[List of Governors of Tennessee|Tennessee Governor]] [[Gordon Browning]] and [[List of Governors of Alabama|Alabama Governor]] [[Frank M. Dixon]].<ref name="UT1"/> After a scoreless first quarter, the Crimson Tide took a 7–0 lead in the second quarter after a Carey Cox [[interception]] set up the scoring drive.<ref name=a1/> The touchdown was scored by [[Vic Bradford]] on a two-yard [[quarterback sneak]] to complete a 60-yard drive.<ref name="UT1"/> In the third, Alabama extended their lead to 14–0 after Hal Hughes scored on a one-yard quarterback sneak.<ref name="UT1"/> The Volunteers responded in the fourth with their only points, a three-yard [[George Cafego]] touchdown pass to Edwin Duncan to complete an 85-yard drive.<ref name="UT1"/> The Tennessee touchdown was the first points allowed by the Crimson Tide defense of the season.<ref name="UT1"/> The victory improved Alabama's all-time record against Tennessee to 13–5–2.<ref name="TNAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tennessee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3180 |accessdate=April 10, 2012}}</ref>
{{clear}}

===George Washington===
{{See also2|{{cfb link|year=1937|team=George Washington Colonials}}}}
{{AFB game box start
|Visitor= #2 '''Alabama'''
|V1=0 |V2=13 |V3=6 |V4=0
|Host=George Washington
|H1=0 |H2=0 |H3=0 |H4=0
|Date=October 23
|Location=Griffith Stadium<br/>Washington, DC
|Attendance=24,666
}}
*'''Source:'''<ref name="GW1">{{cite news |title=Crimson Tide rides through mud to overwhelm Colonials, 39–0 |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=i9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6291%2C5140504 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 24, 1937 |page=10 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
As the entered their game against [[George Washington University|George Washington]], Alabama was ranked No. 2 in the first AP Poll of the 1937 season.<ref name="GW2">{{cite news |title=National poll ranks California Bears as nation's leading football team |url=http://pqasb.pqarchiver.com/courant/access/843386422.html?dids=843386422:843386422&FMT=ABS&FMTS=ABS:AI&type=historic&date=Oct+19%2C+1937&author=&pub=Hartford+Courant&desc=National+Poll+Ranks+California+Bears+As+Nation%27s+Leading+Football+Team&pqatl=google |publisher=Proquest |newspaper=Hartford Courant |date=October 19, 1937 |page=19 |accessdate=April 10, 2012}}</ref> In the contest, the Crimson Tide defeated the [[George Washington Colonials football|Colonials]] 19–0 at [[Griffith Stadium]].<ref name=a1/><ref name="GW1"/> For the second week in a row, Alabama was held scoreless in the first quarter, however a pair of second-quarter touchdowns gave the Crimson Tide a 13–0 halftime lead. In the second, touchdowns were scored by Joe Kilgrow on a 35-yard pass to Perron Shoemaker and Kilgrow on a six-yard run.<ref name="GW1"/> The final points of the game were scored by Charlie Holm in the third after he returned an [[interception]] 30-yards for a touchdown.<ref name="GW1"/> The win improved Alabama's all-time record against George Washington to 3–0.<ref name="GWAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs George Washington |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1260 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Kentucky===
{{See also2|[[1937 Kentucky Wildcats football team]]}}
{{AFB game box start
|Visitor=Kentucky
|V1=0 |V2=0 |V3=0 |V4=0
|Host= #3 '''Alabama'''
|H1=6 |H2=21 |H3=7 |H4=7
|Date=October 30
|Location=Denny Stadium<br>Tuscaloosa, AL
|Attendance=13,000
}}
*'''Source:'''<ref name="KY1">{{cite news |title=Ambitious Tide machine grinds out 41–0 win over Kentucky |first=Jay |last=Thornton  |url=https://news.google.com/newspapers?id=kNY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6611%2C5404533 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 31, 1937 |page=6 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
As Alabama entered their [[homecoming]] contest against [[University of Kentucky|Kentucky]], they dropped one place to No. 3 in the weekly poll.<ref name="KY2">{{cite news |title=Rankings place Alabama third |first=Alan |last=Gould |agency=Associated Press |url=https://news.google.com/newspapers?id=jdY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6490%2C5238928 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 26, 1937 |page=8 |accessdate=April 10, 2012}}</ref> In the game, the Crimson Tide defeated the [[Kentucky Wildcats football|Wildcats]] 41–0 before 13,000 at Denny Stadium.<ref name=a1/><ref name="KY1"/> After [[Charlie Holm]] scored on a 27-yard touchdown run in the first, the Crimson Tide scored three second-quarter touchdowns for a 27–0 halftime lead.<ref name="KY1"/> The second-quarter touchdowns were scored by Gene Blackwell on a six-yard run, Joe Kilgrow on a 20-yard run and on a ten-yard Herschel Mosley pass to Johnny Roberts.<ref name="KY1"/> Alabama then closed the game with a pair one-yard touchdown runs in the second half for the 41–0 win. The first was scored by Billy Slemons in the third and the second by W. L. Waites in the fourth.<ref name="KY1"/> The victory improved Alabama's all-time record against Kentucky 16–1.<ref name="KYAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Kentucky |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1628 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Tulane===
{{see also|1937 Tulane Green Wave football team}}
{{AFB game box start
|Visitor= #2 '''Alabama'''
|V1=0 |V2=0 |V3=6 |V4=3
|Host= #19 Tulane
|H1=6 |H2=0 |H3=0 |H4=0
|Date=November 6
|Location=Tulane Stadium<br/>New Orleans, LA
|Attendance=30,000
}}
*'''Source:'''<ref name="TU1">{{cite news |title=Last minute field goal saves Tide in Tulane tussle, 9 to 6 |url=https://news.google.com/newspapers?id=ltY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6273%2C5740514 |first=Ben |last=Green |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 7, 1937 |page=6 |accessdate=April 12, 2012}}</ref>
{{AFB game box end}}
After their victory over Kentucky, the Crimson Tide moved up one position and regained the No. 2 spot in the weekly poll.<ref name="TU2">{{cite news |title=California tops and Tide second |first=Drew |last=Middleton |agency=Associated Press |url=https://news.google.com/newspapers?id=ktY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=4162%2C5556660 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 2, 1937 |page=7 |accessdate=April 10, 2012}}</ref> [[Tulane University|Tulane]] was ranked No. 19 in the poll after their victory over {{cfb link|year=1937|team=Ole Miss Rebels|title=Ole Miss}}.<ref name="TU2"/> In the contest, the Crimson Tide defeated the [[Tulane Green Wave football|Green Wave]] 9–6 after they converted a game-winning [[Field goal (American and Canadian football)|field goal]] late in the fourth quarter.<ref name=a1/><ref name="TU1"/> Tulane took a 6–0 lead in the first when John Andrews scored on a one-yard run three plays after William Kirchem blocked an Alabama [[Punt (gridiron football)|punt]] to give the Greenies possession at the Tide's 18-yard line.<ref name=a1/> Alabama did not score until early in the third when [[Vic Bradford]] completed an 87-yard drive with his one-yard touchdown run, and after a missed [[Conversion (gridiron football)|extra point]] the game was tied at six.<ref name="TU1"/> With less than two minutes remaining in the fourth, [[Hayward Sanford]] kicked a 23-yard field goal to win the game.<ref name="TU1"/> The victory improved Alabama's all-time record against Tulane to 11–3–1.<ref name="TUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tulane |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3267 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Georgia Tech===
{{see also|1937 Georgia Tech Yellow Jackets football team}}
{{AFB game box start
|Visitor=Georgia Tech
|V1=0 |V2=0 |V3=0 |V4=0
|Host=#3 '''Alabama'''
|H1=0 |H2=0 |H3=0 |H4=7
|Date=November 13
|Location=Legion Field<br>Birmingham, AL
|Attendance=26,000
}}
*'''Source:'''<ref name="GT1">{{cite news |title=Long march in last quarter enables Tide to take Tech, 7–0 |url=https://news.google.com/newspapers?id=nNY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6010%2C6064338 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 14, 1937 |page=10 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
After their close victory over Tulane, the Crimson Tide dropped one position to the No. 3 spot in the weekly poll in spite of having more first-place voted than No. 2 California.<ref name="GT2">{{cite news |title=Pittsburgh forges ahead as No. 1 team in poll of writers |first=Alan |last=Gould |agency=Associated Press |url=https://news.google.com/newspapers?id=mdY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6326%2C5868278 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 9, 1937 |page=8 |accessdate=April 10, 2012}}</ref> In their game against [[Georgia Institute of Technology|Georgia Tech]], the game remained scoreless before a fourth-quarter touchdown gave Alabama the 7–0 victory over the [[Georgia Tech Yellow Jackets football|Yellow Jackets]] at Legion Field.<ref name=a1/><ref name="GT1"/> In the fourth, Joe Kilgrow threw the game-winning two-yard touchdown pass to Erin Warren with only four minutes remaining in the game for the 7–0 win.<ref name="GT1"/> The victory improved Alabama's all-time record against Georgia Tech to 11–10–2.<ref name="GTAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Georgia Tech |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1273 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Vanderbilt===
{{See also|1937 Vanderbilt Commodores football team}}
{{AFB game box start
|Visitor= #4 '''Alabama'''
|V1=0 |V2=6 |V3=0 |V4=3
|Host= #12 Vanderbilt
|H1=0 |H2=0 |H3=7 |H4=0
|Date=November 25
|Location=Dudley Field<br>Nashville, TN
|Attendance=22,000
}}
*'''Source:'''<ref name="VU1">{{cite news |title=Crimson Tide ends "Season of Victory" by trouncing Vandy 9–7 |url=https://news.google.com/newspapers?id=p9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6052%2C6679611 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 26, 1937 |page=11 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
After their victory over Georgia Tech, Alabama had a [[Bye (sports)|bye week]] prior to their annual [[Thanksgiving (United States)|Thanksgiving Day]] game against [[Vanderbilt University|Vanderbilt]]. Prior to their game, Alabama dropped to No. 4 and Vanderbilt moved up to No. 12 in the weekly AP Poll.<ref name="VU2">{{cite news |title=Pitt strengthens No. 1 ranking, Alabama drops to fourth |first=Alan |last=Gould |agency=Associated Press |url=https://news.google.com/newspapers?id=pNY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6032%2C6542007 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 23, 1937 |page=7 |accessdate=April 10, 2012}}</ref> In the game against the [[Vanderbilt Commodores football|Commodores]], Alabama won their second game of the season with a fourth quarter [[Field goal (American and Canadian football)|field goal]] in their 9–7 win at Dudley Field.<ref name=a1/><ref name="VU1"/> After a scoreless first, the Crimson Tide took a 6–0 lead in the second quarter on a Joe Kilgrow touchdown pass to Erin Warren.<ref name="VU1"/> The Commodores took a 7–6 lead in the third when Hardy Housman scored on a one-yard run and Joe Agee kicked the [[Conversion (gridiron football)|extra point]].<ref name="VU1"/> However, late in the fourth [[Hayward Sanford]] kicked the 27-yard game-winning field goal to give the Crimson Tide the 9–7 win.<ref name="VU1"/> The victory clinched the SEC championship for the Crimson Tide and improved Alabama's all-time record against Vanderbilt to 10–9.<ref name="VU3">{{cite news |title=Bama captures title in S.E.C. |agency=Associated Press |url=https://news.google.com/newspapers?id=p9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6129%2C6681697 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 26, 1937 |page=11 |accessdate=April 10, 2012}}</ref><ref name="VUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Vanderbilt |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3363 |accessdate=April 10, 2012}}</ref>
{{clear}}

===California===
{{See also|1937 California Golden Bears football team}}
{{AFB game box start
|Title=[[1938 Rose Bowl]]
|Visitor=#4 Alabama
|V1=0 |V2=0 |V3=0 |V4=0
|Host=#2 '''California'''
|H1=0 |H2=7 |H3=6 |H4=0
|Date=January 1, 1938
|Location=Rose Bowl<br/>Pasadena, CA
|Attendance=87,000
}}
*'''Source:'''<ref name="Cal1">{{cite news |title=California overpowers 'Bama, 13–0 |first=Ben |last=Green |url=https://news.google.com/newspapers?id=c-4-AAAAIBAJ&sjid=IE0MAAAAIBAJ&pg=6283%2C10655 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=January 2, 1938 |page=1 |accessdate=April 10, 2012}}</ref>
{{AFB game box end}}
On November 30, Alabama accepted an invitation to play in the [[1938 Rose Bowl]] against the [[University of California, Berkeley|California]] [[California Golden Bears football|Golden Bears]].<ref name="Cal2">{{cite news |title=Rose Bowl Tide begins work Monday |url=https://news.google.com/newspapers?id=q9Y-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6616%2C6878573 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 1, 1937 |page=1 |accessdate=April 10, 2012}}</ref> In the game, the Crimson Tide were defeated in their only game of the season after their 13–0 [[shutout]] loss before 87,000 fans at Pasadena.<ref name="Cal1"/> [[Vic Bottari]] scored both touchdowns for the Golden Bears on runs of four-yards in the second and five-yards in the third.<ref name="Cal1"/> In the loss, the Crimson Tide turned the ball over eight times, on four [[fumble]]s and four [[interception]]s.<ref name="Cal1"/> Alabama had two scoring opportunities end inside the California ten-yard line, one on a fumble at the one-yard line and another at the six-yard line.<ref name="Cal1"/> The loss was also Alabama's first in the [[Rose Bowl Game]].
{{clear}}

==After the season==

===Awards===
After the season, [[Leroy Monsky]] was a consensus selection and both Joe Kilgrow and James Ryba were selected to various [[1937 College Football All-America Team]]s.<ref>{{cite book |url=http://fs.ncaa.org/Docs/stats/football_records/2011/Awards.pdf |title=Award Winners |author=National Collegiate Athletic Association (NCAA) |page=5 |work=2011 NCAA Division I Football Records |publisher=NCAA.org |accessdate=April 12, 2012 |format=PDF}}</ref><ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=First-Team All-America |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=163–172}}</ref>

===NFL Draft===
Several players that were [[Letterman (sports)|varsity lettermen]] from the 1937 squad were drafted into the [[National Football League Draft|National Football League (NFL)]] between the 1938 and 1940 drafts.<ref>{{cite web |url=http://www.pro-football-reference.com/colleges/alabama/drafted.htm |title=Alabama Drafted Players/Alumni |accessdate=April 7, 2012 |work=Sports Reference, LLC |publisher=Pro-Football-Reference.com}}</ref><ref name="NFLDraft">{{cite web |publisher=National Football League | url=http://www.nfl.com/draft/history/fulldraft?abbr=A&collegeName=Alabama&abbrFlag=0&type=school | title=Draft History by School–Alabama|accessdate=March 16, 2013}}</ref> These players included the following:
{| class="wikitable sortable" style="text-align:center"
! scope="col" | Year
! scope="col" | Round
! scope="col" | Overall
! scope="col" | Player name
! scope="col" | Position
! scope="col" | NFL team
|-
| rowspan=2|[[1938 NFL Draft|1938]]
| 2 
| 13 
! {{Sortname|Joe|Kilgrow|nolink=1}}
| Back 
| [[1938 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 7 
| 53 
! {{Sortname|Leroy|Monsky}}
| Guard 
| Brooklyn Dodgers
|-
| rowspan=2|[[1939 NFL Draft|1939]]
| 3 
| 23 
! {{Sortname|Charley|Holm|nolink=1}}
| Back 
| [[1939 Washington Redskins season|Washington Redskins]]
|-
| 9 
| 73 
! {{Sortname|Lew|Bostick|nolink=1}}
| Guard 
| [[1939 Cleveland Rams season|Cleveland Rams]]
|-
| rowspan=4|[[1940 NFL Draft|1940]]
| 4 
| 30 
! {{Sortname|Bobby|Wood|Bobby Wood (American football)}}
| Tackle 
| [[1940 Cleveland Rams season|Cleveland Rams]]
|-
| 5 
| 34 
! {{Sortname|Walt|Merrill|nolink=1}} 
| Tackle 
| [[1940 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 11 
| 93 
! {{Sortname|Cary|Cox|nolink=1}}
| Center 
| [[1940 Pittsburgh Steelers season|Pittsburgh Steelers]]
|-
| 11 
| 138 
! {{Sortname|Hayward|Sanford|Sandy Sanford}}
| End 
| [[1940 Washington Redskins season|Washington Redskins]]
|}

==Personnel==
{{Col-begin}}
{{Col-2}}

===Varsity letter winners===
{| class="wikitable" border="1"
|-;
! Player
! Hometown
! Position
|-
| Silas Beard
| [[Guntersville, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Gene Blackwell
| [[Blytheville, Arkansas]]
| [[End (American football)|End]]
|-
| Lewis Bostick
| [[Birmingham, Alabama]]
| [[Guard (American football)|Guard]]
|-
| [[Vic Bradford]]
| [[Memphis, Tennessee]]
| [[Quarterback]]
|-
| Henry Cochrane
| [[Paducah, Kentucky]]
| [[Quarterback]]
|-
| Carey Cox
| [[Bainbridge, Georgia]]
| [[Center (American football)|Center]]
|-
| Alvin Davis
| [[Green Forest, Arkansas]]
| [[Fullback (American football)|Fullback]]
|-
| Maurice Fletcher
| [[Clarksdale, Mississippi]]
| [[Quarterback]]
|-
| Jess Foshee
| [[Clanton, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Grover Harkins
| [[Gadsden, Alabama]]
| [[Guard (American football)|Guard]]
|-
| [[Charlie Holm]]
| [[Birmingham, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Hal Hughes
| [[Pine Bluff, Arkansas]]
| [[Quarterback]]
|-
| Thomas Keller
| [[Cullman, Alabama]]
| [[End (American football)|End]]
|-
| [[Joe Kilgrow]]
| [[Montgomery, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Jack Machtolff
| [[Sheffield, Alabama]]
| [[Center (American football)|Center]]
|-
| Walter Merrill
| [[Andalusia, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| [[Leroy Monsky]]
| [[Montgomery, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Herschel Mosley
| [[Blytheville, Arkansas]]
| [[Halfback (American football)|Halfback]]
|-
| William Peters
| [[Hammond, Indiana]]
| [[Guard (American football)|Guard]]
|-
| Jake Redden
| [[Vernon, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Johnny Roberts
| [[Birmingham, Alabama]]
| [[Fullback (American football)|Fullback]]
|-
| Jim Ryba
| [[Cicero, Illinois]]
| [[Tackle (American football)|Tackle]]
|-
| [[Hayward Sanford]]
| [[Plainview, Arkansas]]
| [[End (American football)|End]]
|-
| Perron Shoemaker
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| Billy Slemons
| [[Orlando, Florida]]
| [[Halfback (American football)|Halfback]]
|-
| Jim Tipton
| [[Blytheville, Arkansas]]
| [[Tackle (American football)|Tackle]]
|-
| Erin Warren
| [[Montgomery, Alabama]]
| [[End (American football)|End]]
|-
| [[Bobby Wood (American football)|Bobby Wood]]
| [[McComb, Mississippi]]
| [[Tackle (American football)|Tackle]]
|-
| George Zivich
| [[East Chicago, Indiana]]
| [[Halfback (American football)|Halfback]]
|-
|colspan="3" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Tide Football Lettermen |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=127–141}}</ref>
|}
{{Col-2}}

===Coaching staff===
{| class="wikitable" border="1" style="font-size:90%;"
|-
! Name !! Position !! Seasons at<br />Alabama !! Alma Mater
|-
| [[Frank Thomas (American football)|Frank Thomas]] || [[Head coach]] ||align=center| 7 || [[Notre Dame Fighting Irish football|Notre Dame]] (1923)
|-
| [[Bear Bryant]] || Assistant coach ||align=center| 2 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Paul Burnum]] || Assistant coach ||align=center| 8 || [[Alabama Crimson Tide football|Alabama]] (1922)
|-
| [[Tilden Campbell]] || Assistant coach ||align=center| 2 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Hank Crisp]] || Assistant coach ||align=center| 17 || [[Virginia Tech Hokies football|VPI]] (1920)
|-
| [[Harold Drew]] || Assistant coach ||align=center| 7 || [[Bates Bobcats football|Bates]] (1916)
|-
|colspan="4" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Assistant Coaches |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=142–143}}</ref>
|}
{{Col-2}}
{{Col-end}}

==References==
'''General'''
{{refbegin}}
* {{cite web |url=http://www.rolltide.com/datadump/fls_files/files/football/1930s/1937.pdf |title=1937 Season Recap |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics |accessdate=April 10, 2012 |format=PDF}}
{{refend}}

'''Specific'''
{{Reflist|30em}}
{{Alabama Crimson Tide football navbox}}
{{Southeastern Conference football champions}}

[[Category:Alabama Crimson Tide football seasons]]
[[Category:1937 Southeastern Conference football season|Alabama]]
[[Category:Southeastern Conference football champion seasons]]
[[Category:1937 in Alabama|Crimson Tide]]