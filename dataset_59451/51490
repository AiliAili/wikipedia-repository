{{about|the medical journal|the field of human resources in the health care sector|Health human resources}}
{{Infobox journal
| title = Human Resources for Health
| editor = [[Mario Roberto Dal Poz]]
| discipline = [[Public health]]
| former_names = Human Resources Development Journal
| abbreviation = Hum. Resour. Health
| publisher = [[BioMed Central]] in collaboration with the [[World Health Organization]]
| country =
| frequency = Upon acceptance
| history = 1997–present
| openaccess = Yes
| license = [[Creative Commons Attribution]]
| impact = 2.416
| impact-year = 2016
| website = http://www.human-resources-health.com/
| link1 = http://www.human-resources-health.com/content
| link1-name = Online access
| link2 = http://www.who.int/hrh/hrdj/en/index.html
| link2-name = ''Human Resources Development Journal'' archives
| JSTOR =
| OCLC = 52353314
| LCCN =
| CODEN =
| ISSN = 1478-4491
| eISSN =
}}
'''''Human Resources for Health''''' is a [[peer review|peer-reviewed]] [[open-access]] [[public health journal]] publishing original research and case studies on issues of [[HRHIS|information]], planning, production, management, and governance of the [[Health human resources|health workforce]], and their links with [[health care|health care delivery]] and health outcomes, particularly as related to [[global health]]. It was established in 1997 as the ''Human Resources Development Journal'' published by the Health Manpower Development Institute of the [[Ministry of Public Health (Thailand)|Ministry of Public Health of Thailand]].<ref>{{Cite web |last= |first= |title=Human Resources Development Journal |url=http://www.who.int/hrh/hrdj/en/index.html |publisher=World health Organization |date= |accessdate=19 September 2013}}</ref> Since 2003, it is published by [[BioMed Central]] in collaboration with the [[World Health Organization]] (WHO).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed]], [[Social Sciences Citation Index]], [[Current contents]], [[Scopus]], [[CINAHL]] and 10 other indexing services. The journal's [[impact factor]] as of 2016 is 2.416.

== Contents ==
The journal occasionally publishes themed collections. In 2007, the journal issued a [[call for papers]] jointly with 17 other public health journals under the theme "Towards a scaling-up of training and education for health workers".<ref>{{cite journal |doi=10.1186/1478-4491-5-22 |title=Final call for papers: "Towards a scaling-up of training and education for health workers" |year=2007 |last1=Shaw |first1=Daniel MP |journal=Human Resources for Health |volume=5 |pages=22}}</ref> Twenty-two articles were published in ''Human Resources for Health'' on this special theme between July 2008 and November 2009.<ref>{{Cite web |last=Mercer |first=Hugo |title=Article collections: Towards a scaling-up of training and education for health workers |url=http://www.human-resources-health.com/series/1478-4491-Scl |publisher=BioMed Central |work=Human Resources for Health |date=16 November 2009 |accessdate=19 September 2013}}</ref> In 2013, the journal issued a call for papers on the theme "Right Time, Right Place: Improving access to health service through effective retention and distribution of health workers."<ref>{{Cite web |last=Hoffman |first=Liz |title=Right Time, Right Place |url=http://blogs.biomedcentral.com/bmcblog/2013/03/21/right-time-right-place/ |publisher=BioMed Central |date=21 March 2013 |accessdate=19 September 2013 |work=BioMed central blog}}</ref>

In 2016, the journal published a supplementary collection of research evidence of the relevance and effectiveness of the WHO's [[Health human resources#Global Code of Practice on the International Recruitment of Health Personnel | Global Code of Practice on the International Recruitment of Health Personnel]].<ref>''Human Resources for Health'', Volume 14 Supplement 1, "The WHO global code of practice: early evidence of its relevance and effectiveness" [http://human-resources-health.biomedcentral.com/articles/supplements/volume-14-supplement-1] Accessed 11 July 2016.</ref>

==References==
<references/>

==External links==
* {{Official website|http://www.human-resources-health.com/}}

[[Category:Healthcare journals]]
[[Category:BioMed Central academic journals]]
[[Category:WHO academic journals]]
[[Category:Publications established in 2003]]
[[Category:English-language journals]]
[[Category:Human resource management publications]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Public health journals]]