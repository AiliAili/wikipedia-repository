<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
  |name= E-767
  |image= Boeing E-767 (6).jpg
  |caption= Boeing E-767 AWACS aircraft of the JASDF
}}{{Infobox aircraft type
  |type= [[Airborne early warning and control|Airborne Warning and Control System]] (AWACS)
  |manufacturer= [[Boeing Integrated Defense Systems|Boeing IDS]]
  |designer =
  |first flight= 4 October 1994 (without rotodome) <br>9 August 1996 (with [[rotodome]])
  |introduced= 10 May 2000
  |retired =
  |status= In service
  |primary user= [[Japan Air Self-Defense Force]]
  |more users= <!--up to three more. please separate with <br>.-->
  |produced= <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built= 4
  |unit cost =
  |developed from= [[Boeing 767]]
  |variants with their own articles =
}}
|}

The '''Boeing E-767''' is an [[Airborne early warning and control|Airborne Warning and Control System]] (AWACS) aircraft.<ref>{{cite web|url=http://www.boeing.com/defense-space/ic/awacs/767/767specs.html |title=Archived copy |accessdate=2007-05-30 |deadurl=yes |archiveurl=https://web.archive.org/web/20071011153156/http://boeing.com:80/defense-space/ic/awacs/767/767specs.html |archivedate=2007-10-11 |df= }}</ref> It was designed in response to the [[Japan Air Self-Defense Force]]'s requirements, and is essentially the [[Boeing E-3 Sentry]]'s surveillance [[radar]] and air control system installed on a [[Boeing 767#767-200|Boeing 767-200]].

==Development==

===Background===
On September 6, 1976, Soviet Air Force pilot [[Viktor Belenko]] successfully [[defection|defected]] to the West, flying his [[Mikoyan-Gurevich MiG-25|MiG-25]] 'Foxbat' to [[Hakodate]], Japan. During this incident, Japan Self-Defense Force radar lost track of the aircraft when Belenko flew his MiG-25 at a low altitude, prompting the Japan Air Self-Defense Force (JASDF) to consider procurement of airborne early warning aircraft.

In 1976, the [[United States Air Force|U.S. Air Force]] was about to deploy the [[E-3 Sentry]] airborne warning and control system aircraft, which was considered to be the prime candidate for the airborne early warning mission by JASDF. However, the Japan Defense Agency (JDA, now [[Ministry of Defense (Japan)|Ministry of Defense]]) realized that the E-3 would not be readily available due to USAF needs and instead opted to procure the American [[E-2 Hawkeye]] AWACS aircraft.<ref name=aw22>Air World, April 1998 Special edition "Airborne Early Warning and Control aircraft E-767 & E-3" p. 22.</ref> The E-2C was put into service with the Airborne Early Warning Group (AEWG) at JASDF [[Misawa Air Base]] in January 1987.

In 1991, the JDA requested funds to upgrade the airborne early warning system by procuring the E-3.  Unfortunately, production of the [[Boeing 707]]-based E-3 airframe had ended in 1991 and the plan was shelved.  The following year, Boeing proposed a 767-based AWACS, and the JDA agreed to procure two E-767 in fiscal year 1993 and two more in fiscal year 1994.<ref name=aw30>Air World, April 1998 Special edition "Airborne Early Warning and Control aircraft E-767 & E-3" p. 30.</ref>

===Procurement===
JDA requested a budget of [[Japanese yen|JPY]]113.9 billion to procure two E-767s in fiscal year 1993 and JPY108.1 billion for two more E-767s in FY 1994.<ref name=aw30 /> This large budget compared to approximately JPY8.6 billion for the E-2C and an estimated JPY29.6 billion for the E-3A was politically rationalized as a means to help ease the tension over Japan's large trade surplus against the U.S.  In addition, Japanese aerospace companies are responsible for 15% of the airframe production for the 767, meaning some of the money would indirectly return to Japan.

The procurement of E-767 by Japanese government was split into two stages. The first stage was the procurement of an unmodified 767 aircraft by the Japanese government through a trading company, [[Itochu]] Corporation.  In the second stage, the aircraft were modified to carry AWACS equipment by US and Japanese contractors supervised by the U.S. government under [[Foreign Military Sales]] rules.

==Design==
[[File:E-767 (501.1).jpg|thumb|A Boeing E-767 in 2005]]

The base airframe for E-767 is that of a 767-200ER, Boeing designation 767-27C. (The "7C" designation is the Boeing [[List of Boeing customer codes|customer code]] for the JASDF). The 767 airframe offers about 50 percent more floor space and nearly twice the volume of the 707 on which the E-3 is based.<ref name=boeing_e-767-plc>[http://www.boeing.com/defense-space/ic/awacs/767/767plc.html Boeing 767 AWACS Platform, Logical Choice] {{webarchive |url=https://web.archive.org/web/20071011162713/http://www.boeing.com/defense-space/ic/awacs/767/767plc.html |date=October 11, 2007 }}</ref>  The mission [[electronics]] equipment are installed in forward cabin to balance the weight with the [[rotodome]] mounted above the aft fuselage.  The aft cabin contains the crew's rest area, galley, and lavatory.

===External features===

The E-767's exterior is usually painted in gray.  The 767's windows were omitted in order to protect the crew and  equipment from the intense [[radio frequency]] transmissions from its [[radar]] equipment.{{clarify|What proof?|date=April 2015}}. A rotodome about 30 feet (9.14 m) in diameter and six feet (1.83 m) thick at the center is mounted above the aft fuselage on two struts. The rotodome rotates at about six rpm during operations and at 0.25 rpm to lubricate the rotation mechanisms even when the radar is not used.

There are numerous blade antennae for UHF and VHF communication along the centerline of the fuselage on the top and bottom. There is a rod antenna at each wing tip for HF communication.  A fairing in the aft fuselage contains an antenna for JTIDS ([[Joint Tactical Information Distribution System]]).

===Powerplants===

Two General Electric CF6-80C2B6FA high bypass turbofan engines, 61,500 pounds thrust each.  The original 90 [[kilovolt-ampere|kVA]] [[electrical generator]] in each engine was replaced with 150 kVA generators to provide electric power to the radar and other equipment.<ref>Kantosha World Aircraft Yearbook 2006-2007</ref>

===Airborne early warning and control system===

The electronics system on the E-767 is essentially the same as the later E-3 models, using [[Northrop Grumman]]'s (formerly Westinghouse Electronic Systems) AN/APY-2 [[Passive electronically scanned array]] radar system. This system is a [[3D radar|three-dimensional radar]] that measures azimuth, range, and elevation simultaneously, and has superior surveillance capability over water compared to the AN/APY-1 system on the earlier E-3 models.<ref name=aw37>Air World, April 1998 Special edition "Airborne Early Warning and Control aircraft E-767 & E-3" p. 37.</ref>

The AN/APY-2 is a [[Pulse-Doppler radar]] that can determine the velocity of a tracked target. This surveillance system includes a flexible, multi-mode radar, which enables AWACS to separate maritime and airborne targets from ground and sea clutter returns that limit other modern radar systems.

Its radar has a 360-degree view, and at operating altitudes it can detect targets more than 320 kilometers (200 miles) away.  AWACS mission equipment can separate, manage and display these targets individually on situational displays.<ref name=boeing_e-767-overview>[http://www.boeing.com/defense-space/ic/awacs/767/767overview.html Boeing 767 AWACS Overview] {{webarchive |url=https://web.archive.org/web/20070604152457/http://www.boeing.com/defense-space/ic/awacs/767/767overview.html |date=June 4, 2007 }}. Boeing</ref>

AN/APY-2's antenna and [[Identification Friend or Foe]] (IFF) Mk XII system's antenna are housed in the rotodome back to back.

The information acquired by the radar system is processed by IBM's CC-2E central computer conformed to E-3 Block 30/35 Modification and can be displayed on the 14 displays on board.

Other major subsystems in the E-767 are identification, tactical data link, and navigation.

===Radar System Improvement Program===

In November 2006, Boeing was awarded a $108 million contract to deliver Radar System Improvement Program (RSIP) kits to Japan's fleet of four E-767 Airborne Warning and Control System (AWACS) aircraft.  The Foreign Military Sale was contracted through the [[Electronic Systems Center]] at [[Hanscom Air Force Base]], [[Mass.]] The sale also includes spare and repair parts, support equipment and technical documentation. Installation of the kits was completed December 2012, in Seattle, Washington.

RSIP increases the AWACS aircraft's radar sensitivity, allowing it to detect and track smaller targets. It also improves the radar's existing computer with a new high-reliability multi-processor and rewrites the software to facilitate future maintenance and enhancements.  The RSIP kit, built principally by Northrop Grumman Electronic Systems, Baltimore, MD, under a subcontract to Boeing, consists of a new radar computer, a radar control maintenance panel as well as software upgrades to the radar and mission system programs.<ref name=boeing_e-767_RSIP>[http://www.boeing.com/defense-space/ic/awacs/767/767prodsales.html Boeing 767 AWACS Production & International Sales] {{webarchive |url=https://web.archive.org/web/20071011162718/http://www.boeing.com/defense-space/ic/awacs/767/767prodsales.html |date=October 11, 2007 }}</ref>

==Operational history==

The first E-767 made the first flight on October 4, 1994 at [[Paine Field]], [[Washington (U.S. state)|Washington]]. First flight with the rotodome installed occurred on August 9, 1996 and it was delivered to JASDF on March 11, 1998 along with the second E-767.<ref name=aw33>Air World, April 1998 Special edition "Airborne Early Warning and Control aircraft E-767 & E-3" p. 33.</ref> Aircraft No. 3 and No. 4 were delivered on January 5, 1999. On May 10, 2000, all four E-767s were put into service with Airborne Early Warning Group (AEWG) 601st Squadron at JASDF [[Hamamatsu Air Base]]. (The airbase's runways needed to be reinforced to accommodate the E-767.) On March 31, 2005, the two squadrons were merged.{{clarify|What 2 squadrons?|date=April 2014}}

==Operators==
;{{JPN}}
*[[Japan Air Self-Defense Force]] operates four E-767s.

==Specifications (E-767)==
{{aircraft specifications
 |plane or copter?=plane
 |jet or prop?=jet
 |ref=767 AWACS<ref>[http://www.boeing.com/defense-space/ic/awacs/767/767specs.html Boeing 767 AWACS specifications] {{webarchive |url=https://web.archive.org/web/20071011153156/http://www.boeing.com/defense-space/ic/awacs/767/767specs.html |date=October 11, 2007 }}</ref> B767-200<ref>[http://www.boeing.com/commercial/767family/pf/pf_200prod.html Boeing 767-200ER Technical Characteristics]</ref>
 |crew=Flight:2 Mission:8-10 <!-- needs to be updated -->
 |capacity=
 |length main=159 ft 2 in
 |length alt=48.5 m
 |span main=156 ft 1 in
 |span alt=47.6 m
 |height main=52 ft
 |height alt=15.8 m
 |area main=
 |area alt=
 |airfoil=
 |empty weight main=188,705 lb
 |empty weight alt=85,595 kg
 |loaded weight main=284,110 lb
 |loaded weight alt=128,870 kg
 |useful load main=
 |useful load alt=
 |max takeoff weight main=385,000 lb
 |max takeoff weight alt=175,000 kg
 |more general=

 |engine (jet)=[[General Electric CF6|General Electric CF6-80C2]]
 |type of jet=[[turbofan]]
 |number of jets=2
 |thrust main= 61,500 lbf
 |thrust alt= 273.6 kN
 |thrust original=

 |max speed main=Mach 0.86
 |max speed alt=
 |cruise speed main= Mach 0.80
 |cruise speed alt= 530 mph, 851 km/h
 |stall speed main=
 |stall speed alt=
 |never exceed speed main=
 |never exceed speed alt=
 |range main= 5,600 nmi
 |range alt= 10,370 km
 |ceiling main=40,100 ft
 |ceiling alt=12,200 m
 |climb rate main=
 |climb rate alt=
 |loading main=
 |loading alt=
 |thrust/weight=
 |power/mass main=
 |power/mass alt=
 |more performance=*'''[[Endurance]]''': 9.25 hours on station at 1,000 nautical-mile radius;<br>13 hours at 300-nautical mile radius (no aerial refueling capability).
 |armament=
 |avionics=*[[Northrop Grumman]] AN/APY-2 Doppler [[radar]]
}}

==See also==
{{aircontent
|see also=
|related=
* [[Boeing E-3 Sentry]]
* [[Boeing 767]]
* [[Boeing KC-767]]
* [[E-10 MC2A]]
* [[EL/M-2075]]
|similar aircraft=
* [[Beriev A-50]]
* [[Grumman E-2 Hawkeye]]
* [[IAI Phalcon]]
* [[KJ-2000]]
* [[Boeing 737 AEW&C]]
|lists=
}}

==References==
{{reflist|1}}

==External links==
{{Commons|E-767}}
*[https://web.archive.org/web/20070604152457/http://www.boeing.com:80/defense-space/ic/awacs/767/767overview.html Boeing E-767 page]
*[https://web.archive.org/web/20070521162824/http://www.mod.go.jp:80/asdf/equipment/03_e767.html JASDF E-767 page (Japanese)]
*[https://web.archive.org/web/20090401041958/http://www.mod.go.jp:80/asdf/gallery/e767.html JASDF E-767 gallery (Japanese)]

{{Boeing support aircraft}}

[[Category:United States command and control aircraft 1990–1999]]
[[Category:AWACS aircraft]]
[[Category:Boeing aircraft|E-767]]
[[Category:Twinjets]]
[[Category:Boeing 767]]