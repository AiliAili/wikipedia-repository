{{Use mdy dates|date=June 2013}}
{{Infobox University
|name            =Military Engineering-Technical University
|native_name     =Военный инженерно-технический университет
|latin_name      =
|image_name  =Mihailovskij zamok.jpg
|caption=Engineering castle. Main Military Engineering school with 1823, now branch of Russian Museum near VITU
|motto           =''Spiritual force and engineering competence''
|established     =1810
|type            =Public
|endowment       =
|staff           =
|president       =
|provost         =
|principal       = 
|rector          =Nikolai Ivanivich Ludchenko  
|chancellor      =
|vice_chancellor =
|dean            =
|head_label      =
|head            =
|students        =1500–2000
|undergrad       =
|postgrad        =
|doctoral        =
|faculty         =43/Ph.D.300
|city            =[[Saint Petersburg]]
|state           =
|country         =[[Russia]]
|campus          =Urban
|free_label      =
|free            =
|colors          =
|colours         =
|mascot          =
|nickname        =Vitu
|affiliations    =
|footnotes       =
|website         =[http://www.mil.ru/849/1051/1329/12472/index.shtml]
|address         =191123, St. Petersburg, 22&nbsp;Zakharyevskaya Street
|coor            =
|logo            =
}}
The '''Saint Petersburg Military Engineering-Technical University (Nikolaevsky)''' ({{lang-ru|Санкт-Петербургский Военный инженерно-технический университет}}, '''VITU'''), previously known as the Saint Petersburg Nikolaevsky Engineering Academy, was established in 1810 under [[Alexander I of Russia|Alexander&nbsp;I]]. The university is situated in the former barracks of the Cavalier-Guard Regiment where the university was founded.<ref>Historical essay of development the Main Engineering school of 1819–1869/ M. Мaksimovskiy, St Petersburg, 1869.</ref>

==Description==
Military Engineering-[[Technical University]] is a higher military educational institution preparing officers of [[engineering]] and building specialties for all branches of troops and navy. It is located in [[Saint Petersburg]] where the university was founded, near [[Saint Michael's Castle|Engineers Castle]], [[Summer Garden]], [[Suvorov Museum]], [[Tauride Palace]], and [[Smolny Convent]].

Military Engineering-Technical University has six faculties preparing specialists in the following branches: 
* Military construction,
* Military energy resource engineering,
* [[Naval base]] construction,
* Sanitary engineering,
* [[Mechanization]] of construction,
* Special for civil.<ref>{{cite web|url=http://www.vitu-spb.narod.ru/|title=НЕОФИЦИАЛЬНЫЙ САЙТ ВОЕННОГО ИНЖЕНЕРНО-ТЕХНИЧЕСКОГО УНИВЕРСИТЕТА -- ВИТУ|publisher=|accessdate=June 29, 2015}}</ref>

The university trains experts in the field of construction of buildings and special structures, engineering and technical systems and power industry. It has a modern experimental base for testing various thermal-mechanical and power equipment, structures and construction materials, and carries out research and development activities. It provides military university trained officers for all the Engineering Troops of Russia, a counterpart of the [[United States Army Corps of Engineers|U.S. Army Corps of Engineers]]. Also, possibly teaching of foreigners.

== History ==
[[File:Suvorov's museum (Saint Petersburg).jpg|thumb|250px|Suvorov Military Memorial Museum near VITU]]

This is one of Saint Petersburg's eldest Higher Military engineering schools, its history (as [[Higher learning institution]]) beginning in 1810.<ref name="ReferenceA">[Stephen Timoshenko - Engineering Education in Russia, McGraw-Hill Book Company, 1959]</ref> The Saint Petersburg Military Engineering-Technical University was founded as the Saint Petersburg military engineering School in 1810 on the base of the military school of engineering conductors (engineering of [[non-commissioned officers]]), after addition of officers classes and application of five-year term of teaching. In 1819 was renamed as the Main military [[engineering education|engineering School]].<ref>{{cite web|url=http://regiment.ru/reg/VI/C/29/1.htm|title=Николаевское инженерное училище|publisher=|accessdate=June 29, 2015}}</ref><ref name="works.tarefer.ru">{{cite web|url=http://works.tarefer.ru/33/101252/index.html|title=Реферат История Подготовка инженеров России в XIX веке|publisher=|accessdate=June 29, 2015}}</ref> How [[Stephen Timoshenko]] wrote in a book "Engineering Education in Russia" the system of Higher learning institution of five-year Education of Main military engineering school was used later on the example of Institute of railway Engineers by all Russia and develops until now.<ref name="ReferenceA"/> This engineering school was alma mater of graduate for [[Fyodor Dostoyevsky]].<ref>''Anna Dostoyevskaya'' [http://az.lib.ru/d/dostoewskij_f_m/text_0610.shtml Flashbacks] [1925]. M.: Artistic literature, 1981.</ref> In 1855, officers classes of the Nikolaevsky Engineering School was reformed as the Nikolaevsky Engineering Academy.<ref>{{cite web|url=http://www.grwar.ru/schools/schools.html?id=3&graduated=on&PHPSESSID=d2691c3775a822660dbc7248ec1f8e29|title=Русская армия в Великой войне: Военно-учебные заведения проекта|publisher=|accessdate=June 29, 2015}}</ref>

After 1917, numerous transformations of Nikolaevsky Engineering Academy and Engineering school were undertaken (but Higher learning institution survived). It was renamed as the Military-Engineering Academy, and then as Military-Technical Academy. But in 1932 followed the unsuccessful attempt of moving the Engineering Faculty to Moscow; it was completed later as the Sea Faculty returned to [[Leningrad]] in 1939 (As a result, from the base Saint Petersburg Engineering Higher learning institution was separated a new Moscow military Engineering-administrative academy). Only [[Nikolay Gerasimovich Kuznetsov]] could counteract [[Joseph Stalin]]`s policy against the Nikolaevsky Engineering Academy and school in 1939.<ref>*[http://admiral.centro.ru/start_e.htm Information page, including memoirs]</ref> He ordered that the university be revived, and that the Marine Engineering faculty be returned from Moscow.<ref>* [http://glavkom.narod.ru/ Сайт, посвящённый Н. Г. Кузнецову]</ref> The attempts at bureaucratic movings (or Stalin's unfavorable attitude, 1932–1939) of the Saint Petersburg High School of Military Engineers can be examined in the historical context of the [[Case of Trotskyist Anti-Soviet Military Organization|"Military Case"]] and [[Great Purge]], on the eve of war against [[fascism]].<ref>*[http://stalin.memo.ru/spiski/pg02267.htm List of accused of "Military Case"]</ref> Also, Stalin's dislike of Fyodor Dostoyevsky was the reason of the unfavorable attitude against a university (because Stalin did not understand Dostoyevsky) {{citation needed|date=September 2014}}.

There were destructive consequences of some degradation for the pedagogical and scientific forces of Saint Petersburg High School of Military Engineers, but it was successfully corrected only due to the donor help of [[Saint Petersburg Polytechnical University|Petersburg Polytechnical Institute]]. Nikolaevsky Engineering Academy was formally and legally reborn in 1939 as the Higher Naval Engineering Construction School on the base of the Leningrad Industrial Construction Engineers Institute (separate part of Saint Petersburg Polytechnical University), and enlarged with the Sea Engineering Faculty of the Moscow Military Academy.<ref name="encspb.ru">{{cite web|url=http://www.encspb.ru/en/article.php?kod=2804020570|title=Saint Petersburg encyclopaedia|publisher=|accessdate=June 29, 2015}}</ref> Higher Naval Engineering Construction School was renamed the Higher Naval Engineering Technical School. [[Leonid Kantorovich]] became the professor of Military Engineering-Technical University, previously known as the Nikolaevsky Engineering Academy, when it was revived on the site of part of the Polytechnical Institute.<ref>{{cite web|url=http://www.spbstu-eng.ru|title=Saint-Petersburg State Polytechnic University in Russia (SPBSTU) in Russia. Russian universities.|publisher=|accessdate=June 29, 2015}}</ref>

Also, the academician [[Boris Galerkin]] took a general's uniform in 1939, as the head of VITU's structural mechanics department became a lieutenant general. In September 1960, VITU university was called the Order of the Red Banner Higher Military Engineering School and became part of the construction troops.<ref name="encspb.ru"/> In 1974, the university was named after A.N. Komarovsky.<ref name="encspb.ru"/> In 1993, the university was reformed as the Military Engineering-Technical Institute, which received its present-day name in 1997, after as did merger the Pushkin Higher Military Engineering Construction School.<ref>{{cite web|url=http://www.sovinformburo.com/organization/detail/?item_id=1478&type=0|title=Санкт-Петербургский Военный инженерно-технический университет|publisher=|accessdate=June 29, 2015}}</ref>

=== The Second World War===
Military Engineering-Technical University directly took part in [[World War II]]. The graduating students of the university fought heroically at all fronts of that war. They showed spiritual force and quality of engineering competence. The forts and numerous fortifications buildings was established by the graduating students of university, all of it played a vital part in defending (for example [[Brest Fortress]]). So unique [[Krasnaya Gorka fort]] was constructed by the graduating students of VITU at the beginning of the 20th century with the installation of 12-inch guns in concrete casemates. The system of forts played a key part in the [[Siege of Leningrad]]. The VITU's graduating students by the commanders of [[Krasnaya Gorka fort]] did to finally stopped the offensive of fascists already in 1941. During the Siege of Leningrad, [[Boris Galerkin]] was the head of the city engineering defence department experts group. Also, he joined the military engineering commission of the [[Russian Academy of Sciences|Academy of Sciences]]. Hard non-stop work was undermining his health. Not long after the Victory, in {{Nowrap|July 1945}}, Galerkin died. [[Leonid Kantorovich]] was the professor of the VITU of the Navy, and there he was in charge of safety on the [[Road of Life]]; for his feat and courage he was awarded the [[Order of the Patriotic War]].<ref>Dochenko V. D., Navy. War. Victory. St. Petersburg, Shipbuilding. 1995</ref> All employees of the university were decorated with the medal "For Defense of Leningrad". In 1944, the university was the recipient of the Order of the Red Banner award, and VITU's cadets participated in the historic [[Moscow Victory Parade of 1945]].

== Traditions of Saint Petersburg High School of Military Engineers ==
[[File:Saint Petersburg St Michael's Castle.jpg|thumb|right|300px|Engineers Castle (southern façade) with the Monument to Peter&nbsp;I in the foreground, favourite place of VITU's cadets]]

[[Military Engineering]]-Technical University prolongs, saves and develops the scientific and pedagogical traditions of Saint Petersburg High (higher learning institution) School of Military Engineers, the Nikolaevsky Engineering Academy and Nikolaevsky Engineering School, in the place of its own historical motherland.<ref name="works.tarefer.ru"/> In present days, traditions about the unique quality of pedagogical qualification of professors and teachers are saved with punctilious care.  So [[Leonid Artamonov]], a Russian engineer, geographer and traveler, military adviser of [[Menelik II]], was the example of tradition of university about harmonic association of spiritual force and engineering competence. The Saint [[Ignatius Bryanchaninov]] chose way of spiritual service.

[[Dmitri Mendeleev]] was Professor of Chemistry at the Saint Petersburg Military Engineering-Technical University. Mendeleev got married at Nikolaev Engineering-technical University's church. So a professor Aleksey Shuliachenko, an engineer and chemist, known as the "grandfather of the Russian cement", was his student. Also, Aleksey Shuliachenko was able to become a professor for a future academician Boris Galerkin at the [[St. Petersburg Technological Institute]].

==Alumni and faculty==

In total, the University prepared more than 45,000 military engineers. Among its alumni and faculty are:
* [[Leonid Artamonov]], a Russian general, geographer and traveler, military adviser of [[Menelik II]], as one of Russian officers of volunteers was attached to the forces of Ras Tessema (wrote: «Through Ethiopia to the White Nile»)
* [[Alexander Vegener]] (Russian: [[:ru:Вегенер, Александр Николаевич|Вегенер, Александр Николаевич]]), a Russian military pilot, engineer, aircraft designer, chief of the main air field, first chief of [[Nikolay Yegorovich Zhukovsky|Zhukovsky]] Air Force Engineering Academy
* [[Konstantin Velichko]] (Russian: [[:ru:Величко, Константин Иванович|Величко, Константин Иванович]]),  — a Russian/Soviet general military engineer, professor of fortification and author of numerous fortifications projects, for example the [[Krasnaya Gorka fort|Red hill fort]]
* [[Boris Galerkin]], a Russian/Soviet [[mathematician]] and engineer
* [[Mikhail Glukharev]], the chief engineer at [[Sikorsky Aircraft]]
* [[Dmitry Grigorovich]], a Russian writer
* [[Fyodor Dostoyevsky]], a Russian writer and essayist
* [[Alexander Dutov]], a Lieutenant General and one of the leaders of the [[Cossack]] [[counterrevolution]]
* [[Dmitry Karbyshev]], a Red Army general and [[Hero of the Soviet Union]] (posthumously) who was taken prisoner during [[World War&nbsp;II]], tortured by the [[Nazis]], and died on {{Nowrap|February 17}}, 1945, in the [[concentration camp]] at [[Mauthausen-Gusen concentration camp|Mauthausen]]
* [[Leonid Kapitsa]] (Russian: [[:ru:Капица, Леонид Петрович|Капица, Леонид Петрович]]), was father for nobel laureate '''[[Pyotr Kapitsa]]''', a Russian general military engineer, oversaw [[Kronstadt]]'s forts construction
* [[Konstantin von Kaufman]], the first [[Governor-General]] of [[Russian Turkestan]]
* [[Leonid Kantorovich]], a winner of the [[Nobel Prize in Economics]], a Russian [[mathematician]] and [[economist]], known for his theory and development of techniques for the optimal allocation of resources
* [[Roman Kondratenko]], a Russian general famous for his steadfast defense of Port Arthur (now [[Lüshunkou]]) during the [[Russo-Japanese War]]
* [[Aleksander Sergeyevich Pechorin]] A Russian military officer
* [[Vladimir Korguzalov]] (Russian: [[:ru:Коргузалов, Владимир Леонидович|Коргузалов, Владимир Леонидович]]), a [[Hero of the Soviet Union]] chief of engineers, major of Guard troops of 47th army of Voronezh front
* [[Alexander Kvist]] (Russian: [[:ru:Квист, Александр Ильич|Квист, Александр Ильич]]), a Russian military engineer of [[fortification]]
* [[César Cui]], an army [[Officer (armed forces)|officer]] and a teacher of fortifications, as well as a [[composer]] and [[Music journalism|music critic]], known as a member of [[The Five (composers)|The Five]], the group of Russian composers under the leadership of [[Mily Balakirev]] dedicated to the production of a specifically Russian type of music
* [[Dmitri Dmitrievich Maksutov]] (1896–1964), a Russian/Soviet [[Optical engineering|optical engineer]] and [[amateur astronomer]]. He is best known as the inventor of the [[Maksutov telescope]].
* [[Alexander Lukomsky]], a Russian military commander, General Staff, Lieutenant-General ({{Nowrap|April 1916}}) who fought for the [[Imperial Russian Army]] during the [[First World War]] and was one of the organizers of the [[Volunteer army]] during the [[Russian Civil War]].
* [[Vladimir May-Mayevsky]], a Russian army general and one of the leaders of the counterrevolutionary [[White movement]] during the Russian Civil War
* [[Dmitri Mendeleev]], a Russian [[chemist]] and inventor, credited as the creator of the [[periodic table]] of elements
* [[Boris Mozhaev]] (Russian: [[:ru:Можаев, Борис Андреевич|Можаев, Борис Андреевич]]), a Russian writer and friend [[Aleksandr Solzhenitsyn]]
* [[Grand Duke Nicholas Nikolaevich of Russia (1856–1929)]]
* [[Mikhail Vasilievich Ostrogradsky]], a Russian mathematician, [[mechanics|mechanician]] and [[physicist]] who is considered to be a disciple of [[Leonhard Euler]] and one of the leading mathematicians of Imperial Russia
* [[Nicholas Petin]] (Russian: [[:ru:Петин, Николай Николаевич|Петин, Николай Николаевич]]), a Red Army general, chief of engineers of [[Red Army]]
* [[Alexei Polivanov]], a Russian military figure
* [[Ivan Sechenov]], a Russian [[physiologist]], named by [[Ivan Pavlov]] as "The Father of Russian [[physiology]]", who authored ''Reflexes of the Brain'', introducing [[electrophysiology]] and [[neurophysiology]] into laboratories and teaching of [[medicine]]
* [[Aleksey Shvarct]], a Lieutenant General, governor of Odessa
* [[Eduard Totleben]], a military engineer and [[Imperial Russian Army]] general who was in charge of fortification and [[sapping]] work during a number of important [[Russian Empire|Russian]] military campaigns
* Baron [[Peter von Uslar]] a Russian general, engineer and [[linguistics|linguist]] of [[German descent]], known for his research of languages and [[ethnography]] of peoples of [[Caucasus]].
* [[Pavel Yablochkov]], a Russian [[electrical engineer]], the inventor of the [[Yablochkov candle]] (a type of electric carbon [[arc lamp]]) and a businessman
* [[Shuliachenko Aleksey Romanovich]] (Russian: [[:ru:Шуляченко, Алексей Романович|Шуляченко, Алексей Романович]]), a Russian engineer, general and chemist, known as the "grandfather of the Russian Cement"
* [[Golovin Kharlampiy Sergeevich]] (Russian: [[:ru:Головин, Харлампий Сергеевич|Головин, Харлампий Сергеевич]]), a rector of [[Saint Petersburg State Institute of Technology]]
* [[Nestor Buinitsky]] (Russian: [[:ru:Буйницкий, Нестор Алоизиевич|Буйницкий, Нестор Алоизиевич]]), a Russian engineer, professor of fortification and Lieutenant General.

== Sources ==
{{reflist|2}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see [[WP:REFB]] for instructions on how to add citations. --->

==External links==
* ''[[Anna Dostoyevskaya]]'' [http://az.lib.ru/d/dostoewskij_f_m/text_0610.shtml Flashbacks] [1925]. M.: Artistic literature, 1981.
* [http://www.encspb.ru/en/article.php?kod=2804020570 St-Petersburg Military Engineering University]
* [http://eng.mil.ru/print/articles/article2671.shtml The Russian Federation Ministry of Defence Higher Military Schools]
* [http://www.mil.ru/849/1051/1329/12472/index.shtml Military Engineering-Technical University]
* [http://www.sovinformburo.com/organization/detail/?item_id=1478&type=0 Saint Petersburg Military Engineering-Technical University]
* [http://www.vitu-spb.narod.ru/ Military Engineering-Technical University]
* [http://regiment.ru/reg/VI/C/29/1.htm Russian emperor's army]
* [http://history.scps.ru/officer/03-2.htm Volkov. Russian officers]
* [http://www.spbstu-eng.ru Official Website of the Saint Petersburg Polytechnical University]
* Historical essay of development the Main Engineering school of 1819–1869/ M. Мaksimovskiy, St Petersburg, 1869.
* [http://admiral.centro.ru/start_e.htm Information page, including memoirs]
* [http://army.armor.kiev.ua/forma-2/ing-pogon-1909-991.jpg Age-old shoulder-straps of cadets]
* [http://www.pereplet.ru/text/denisov/denisov_inzhen.files/image002.jpg Officer and conductor of Main engineering school]
* [http://www.ruscadet.ru/history/rkk_1701_1918/common/vuz/jun-class.jpg Study of mathematics in Main engineering school]

{{coord|60|00|26.41|N|30|22|22.66|E|region:RU_type:edu|display=title}}

[[Category:Military Engineering-Technical University| ]]
[[Category:Military academies of Russia]]
[[Category:Universities in Saint Petersburg]]
[[Category:Naval academies]]
[[Category:Military academies of the Soviet Union]]
[[Category:Educational institutions established in 1810]]
[[Category:Technical universities and colleges]]
[[Category:Engineering universities and colleges in Russia]]
[[Category:Military high schools]]
[[Category:Public universities]]
[[Category:Military education and training in Russia]]
[[Category:Education in the Soviet Union]]
[[Category:Science and technology in Russia]]
[[Category:1810 establishments in Russia]]