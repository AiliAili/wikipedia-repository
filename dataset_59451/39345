{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Good article}}
[[File:Lorenzo Campeggio.jpg|thumb|right|[[Lorenzo Campeggio]], the last cardinal protector of England confirmed by the crown]]
The '''Cardinal protector of England''' was an appointed [[crown-cardinal]] of England from 1492 until 1539. A [[cardinal protector]] is the representative of a Roman Catholic nation or organisation within the [[College of Cardinals]], appointed by the pope. The role was terminated as a result of the [[English Reformation]].

The role of national [[protector (title)|protectorships]] within the College developed during the fifteenth century, due to developments in the emergence of national monarchies and Renaissance diplomacy.<ref name="w5">Wilkie, 1974, p. 5.</ref> Cardinal protectors of [[Roman Catholic religious order]]s date back farther to the thirteenth century.<ref name="w5"/> According to [[Henry VIII of England|King Henry VIII]], the cardinal protector "indueth as it were our owne Person, for the defence of Us and our Realme in al matiers [in the Curia]...touching the same".<ref name="w6">Wilkie, 1974, p. 6.</ref> The cardinal protector represented the monarch in [[papal consistory|consistory]], especially in cases where the right of [[investiture]] was divided between the pope and the monarch, and also led the English [[diplomatic corps]] in Rome.<ref name="w6"/>

Although earlier cardinals had filled similar roles, "the existence of national protectorships was first openly and regularly recognized only" by [[Pope Julius II]].<ref name="w7">Wilkie, 1974, p. 7.</ref> The terms 'cardinal protector' and 'cardinal procurator' were "used very loosely and sometimes interchangeably during the fifteenth century".<ref name="w8">Wilkie, 1974, p. 8.</ref> The earliest reference to a 'cardinal protector' of England dates from 1492, but according to Wilkie, this results from a confusion between this office and that of cardinal procurator.<ref name="w8"/>

Unlike other national cardinal protectors, the cardinal protectors of England, [[Scotland]], and Ireland were generally chosen exclusively by the pope.  The cardinal was "imposed from above, rather than chosen" and often had no direct relationship with the governments of these countries.<ref name="sig">Signorotto and Visceglia, 2002, p. 163</ref><ref>[https://books.google.com/books?id=KsJs14sBkCAC&pg=163#v=onepage&q=&f=false Court and politics in papal Rome, 1492–1700 By Gianvittorio Signorotto, Maria Antonietta Visceglia, pg163]</ref> The English cardinal protector played a large role in English ecclesiastical appointments, and a substantial role in similar appointments in Scotland and Ireland.<ref>Wilkie, 1974, pp. 53–73.</ref>

==History==

===Piccolomini (1492–1503)===
[[File:Pius III, Nordisk familjebok.png|thumb|right|[[Pope Pius III]]]]
Francesco Piccolomini, the future [[Pope Pius III]], was the first cardinal protector of England, elevated on the initiative of [[Henry VII of England|King Henry VII]], and also the first officially approved cardinal protector of "any nation whatever".<ref name="w10">Wilkie, 1974, p. 10.</ref> Henry VII viewed good relations with Rome as a protection against domestic and foreign enemies and sent [[Christopher Urswick]], his "[[almoner]] and trusted councillor" to Rome after receiving a bull of dispensation to marry [[Elizabeth of York]].<ref name="w11">Wilkie, 1974, p. 11.</ref> When Henry VII first sought a cardinal protector in 1492, he feared that many of the English bishops would support his [[Yorkist]] opponents (to whom they owed their appointments).<ref name="w15">Wilkie, 1974, p. 15.</ref>

A variety of other disagreements existed, such as the [[papal income tax]] and the refusal of the pope to create [[John Morton (archbishop)|John Morton]], the [[archbishop of Canterbury]], a cardinal; Innocent VII had passed over Morton in his first [[papal consistory|consistory]], despite creating two French cardinals.<ref name="w16">Wilkie, 1974, p. 16.</ref> Nor had [[John Sherwood (bishop)|John Sherwood]], the English ambassador in Rome, been created a cardinal in 1484, despite the request of [[Richard III of England|King Richard III]].<ref name="w17">Wilkie, 1974, p. 17.</ref> Piccolomini's creation as cardinal protector was requested by Henry VII in a letter congratulating the newly elected [[Pope Alexander VI]], and was confirmed in a response which was probably written by [[Giovanni Gigli]].<ref name="w18">Wilkie, 1974, p. 18.</ref>

Piccolomini was already the protector of the [[Camaldese Benedictines]] and was close to German princes, although he was not the German protector in any official sense, and his protectorship of England is "the first official one of any cardinal which can be firmly established".<ref>Wilkie, 1974, pp. 20–21.</ref> Henry VII did not object to Piccolomini's German connections, even viewing them as an asset against the French.<ref name="w21">Wilkie, 1974, p. 21.</ref>

===Castellesi (1503–1504)===
The appointment of [[Adriano Castellesi]] as cardinal on 31 May 1503 "eclipsed England's cardinal protector", with appointments to the English sees thereafter being referred through Castellesi instead of through Piccolomini.<ref>Wilkie, 1974, pp. 25–26.</ref> Piccolomini was himself elected as [[Pope Pius III]] on 22 September 1503, only to die less than a month later, on 18 October; Castellesi did not vote for him and Piccolomini was chosen for his perceived neutrality rather than for his English connections.<ref name="w27">Wilkie, 1974, p. 27.</ref> According to the account of Castellesi, Pius III acknowledged him as his ''de facto'' successor as protector.<ref name="w27"/>

Castellesi was a [[favourite]] of [[Pope Alexander VI]], which became a liability during the reign of [[Pope Julius II]].<ref name="w28">Wilkie, 1974, p. 28.</ref> During Julius II's reign, Castellesi, "although neither requesting nor mentioning the office of protector of England, certainly presented himself to Henry VII as the cardinal responsible for English affairs in the Curia".<ref name="w28"/> In an attempt to secure his status against the intrigues of [[Silvestro Gigli]], Castellesi donated his residence, the [[Palazzo Giraud Torlonia]] on the present [[Via della Conciliazione]], to Henry VII.<ref>Wilkie, 1974, pp. 29–30.</ref> In 1504, Henry VII named six official members of his embassy, headed by Castellesi, and also including [[Gilbert Talbot (soldier)|Sir Gilbert Talbot]], [[Richard Beere]], [[Robert Sherborne]], [[Silvestro Gigli]], and Edward Scott.<ref name="w30">Wilkie, 1974, p. 30.</ref>

[[Paris de Grassi]], the master of papal ceremonies, referred to Castellesi as "Regis Protector" in his notes of a meeting between the embassy and the pope.<ref name="w30"/>

===della Rovere (1504–1508)===
[[File:Galeotto della Rovere.jpg|thumb|right|[[Galeotto della Rovere]]]]
A letter from Julius II to Henry VII dated 6 July 1504, remarks that the king had chosen the pope's [[cardinal-nephew]] [[Galeotto della Rovere]] as cardinal protector; the letter does not mention Castellesi.<ref name="w31">Wilkie, 1974, p. 31.</ref> della Rovere's selection was likely arranged by Gigli.<ref name="w31"/> Castellesi was compensated by being promoted to the wealthier See of [[Bishop of Bath and Wells|Bath and Wells]].<ref name="w31"/> Castellesi lost favour with the king and fled Rome until the death of Julius II.<ref>Wilkie, 1974, pp. 31–34.</ref>

Rovere died on 11 September 1508, leaving England without a cardinal protector.<ref name="w35"/> Sherbone and [[Hugh Inge]] were back in England; Scott was dead; Gigli was in England as [[nuncio]].<ref name="w35"/> The "only man on whose loyalty the king could truly rely" was [[Christopher Fisher]], who was a "single, bumbling amateur" compared to the more seasoned curial diplomats who surrounded him.<ref name="w35"/> Henry VII himself died on 21 April 1509.<ref name="w35"/>

Another cardinal-nephew, [[Sisto della Rovere]], who received the vice-chancellorship and all the [[benefice]]s of his half-brother, was not explicitly named as protector, although he wrote to Henry VII stating his intent to "maintain his brother's friendships".<ref>Wilkie, 1974, pp. 35–36.</ref> Henry VIII replied to Sisto that he considered his friendship especially valuable, asserting that Sisto had been close to his father.<ref name="w36">Wilkie, 1974, p. 36.</ref> There is no evidence that Sisto was offered the protectorship.<ref name="w36"/>

===Aldiosi (1509–1510)===
[[File:Portrait_of_a_Cardinal,_by_Raffael,_from_Prado_in_Google_Earth.jpg|thumb|left|[[Francesco Adiosi]]]]
Cardinal [[Francesco Adiosi]] may have become cardinal protector, but this appointment "cannot be exactly established" as his only surviving letters to England do not mention the protectorate.<ref name="w37">Wilkie, 1974, p. 37.</ref> Adiosi is explicitly mentioned as protector in a 1509 letter from [[Christopher Bainbridge]] (the first English curial cardinal since the death of [[Adam Easton]] in 1397<ref name="w40">Wilkie, 1974, p. 40.</ref>), by which point Adiosi and go-between [[Girolamo Bonvisio]] were on the "brink of disgrace".<ref name="w38">Wilkie, 1974, p. 38.</ref>

Bonvisio disclosed the contents of his discussions with the king to a French agent and confessed his being employed by Aldiosi under threat of torture; by this point Adiosi was no longer protector.<ref name="w38"/> According to an 6 April 1510 letter from the Venetian ambassador, the king dismissed Adiosi as protector and gave the post to Sisto della Rovere.<ref name="w39">Wilkie, 1974, p. 39.</ref> There is no direct evidence that Sisto ever received the official title before he died in March 1517.<ref name="w39"/> Until the death of Julius II, Bainbridge "filled the vacuum, real or in effect, in the protectorship of England".<ref name="w40"/>

Castellesi returned to Rome on the death of Julius II on 21 February 1513 for the [[papal conclave, 1513]]; although Castellesi "tactually" voted for Bainbridge on the second ballot, the two inevitably came into conflict as "rival representatives of England".<ref name="w45">Wilkie, 1974, p. 45.</ref> The lack of consistorial records, which would list which cardinals referred the nominations of which bishops, are missing for this period, making it impossible to assess the extent of Bainbridge's role.<ref name="w74">Wilkie, 1974, p. 74.</ref>

===Medici (1514–1523)===
In 1514, Gigli (as the agent of Wolsey and Henry VIII) arranged for another cardinal-nephew [[Pope Clement VII|Giulio de'Medici]] (future Pope Clement VII) to be cardinal protector of England.<ref name="w31"/> Medici's letter of appointment makes no reference to Sisto della Rovere.<ref name="w39"/> A 8 February 1514 letter from [[Pope Leo X]] to [[Henry VIII of England]] flatters the king for having elevated the pope's cardinal-nephew and cousin as protector.<ref name="w48">Wilkie, 1974, p. 48.</ref> The pope's brother [[Giuliano de' Medici (1479-1516)|Giuliano de' Medici]] was also made a [[Knight of the Garter]] (just as [[Guidobaldo II della Rovere]] had been made when della Rovere had been made protector).<ref name="w48"/>
[[File:Raffael 040.jpg|thumb|right|[[Pope Leo X]] ''(center)'' with his [[cardinal-nephew]] [[Pope Clement VII|Giulio de'Medici]] ''(left, future Pope Clement VII)'']]
Bainbridge was "short-circuited" by the appointment of Medici, although he continued to play a role until his death on 14 July 1514.<ref>Wilkie, 1974, pp. 49–50.</ref> Gigli was accused of having played a role in the death of Bainbridge and Medici was charged with examining the facts, concluding that Gigli was innocent.<ref>Wilkie, 1974, pp. 50–51.</ref> The period between Piccolomini and Medici (from 1503 to 1514) is one where the role of the protector was not well-defined.<ref>Wilkie, 1974, p. 73.</ref> The importance of the office increased significantly with the appointment of Medici in 1514, due in no small part to the friendship between Medici and Wolsey.<ref name="w81">Wilkie, 1974, p. 81.</ref> According to Wilkie, "its importance stemmed from the special relationship of the papacy with England as the most reliable supporter of papal independence".<ref name="w81"/>

Medici accepted the protectorship of France as well in 1516, meeting [[Francis I of France]] personally in [[Bologna]], much to the "discomfiture of England".<ref name="w201">Wilkie, 1974, p. 201.</ref> Medici was elected Clement VII on 19 November 1523.

===Campeggio (1524–1539)===
[[Lorenzo Campeggio]] was close to Medici and served as cardinal protector to Germany at the time of Medici's election.<ref name="w141">Wilkie, 1974, p. 141.</ref> Campeggio received a variety of appointments from Clement VII before Henry VIII chose him as protector on 22 February 1524 (conditional on the pope's acceptance of Wolsey as legate for life).<ref>Wilkie, 1974, p. 143–144.</ref>

Campeggio found his loyalty divided when he was appointed with Wolsey to judge the issue of the requested annulment of [[Henry VIII of England]] from [[Catherine of Aragon]], the aunt of [[Charles V, Holy Roman Emperor]].<ref>Wilkie, 1974, pp. 176–190.</ref> Campeggio came out in favour of the legitimacy of the marriage, after considerable delay in travelling and reviewing the canonical evidence.<ref>Wilkie, 1974, pp. 190–199.</ref> The final sentence in the case was handed down in Rome in 1534, the same year the English Parliament passed the [[Acts of Supremacy|First Act of Supremacy]].<ref>Wilkie, 1974, p. 200.</ref> Henry VIII was particularly displeased by Campeggio's "constant company with the emperor" in the years prior to his verdict and Campeggio's rapidly growing income, having been granted the bishopric of [[Roman Catholic Diocese of Huesca|Huesca]] and [[Roman Catholic Diocese of Jaca|Jaca]] in 1530, and the [[bishopric of Mallorca]] in 1532, both by Charles V.<ref>Wilkie, 1974, pp. 203–205.</ref>

In January 1531, Campeggio was dismissed as cardinal protector, although it did not become public knowledge until May.<ref>Wilkie, 1974, p. 207.</ref> At first it was unclear whether Henry VIII intended to appoint a successor, with [[Giovanni Domenico de Cupis]] emerging as an active candidate in March 1532.<ref name="w208">Wilkie, 1974, p. 208.</ref> The king favoured [[Pope Paul III|Alessandro Farnese]] (future Pope Paul III), and instructed his ambassadors on 21 March to offer it to Farnese, and then de Cupis or [[Pope Julius III|Giovanni del Monte]] (future Pope Julius III) in the event that Farnese declined or was not approved.<ref name="w208"/>

Not knowing that Henry VIII had already secretly married the pregnant [[Anne Boleyn]], Clement VII decided to reach out to the monarch by appointing [[Thomas Cranmer]], an outspoken proponent of Henry VIII's annulment, as [[archbishop of Canterbury]].<ref>Wilkie, 1974, pp. 210–211.</ref> A threatened excommunication was handed down when Cranmer pronounced Henry VIII's marriage null and void; Henry VIII responded by telling Campeggio's vicar general for Salisbury to stop all revenues from his bishopric until further notice.<ref>Wilkie, 1974, pp. 213–214.</ref> Henry VIII then claimed the authority to act on behalf of Campeggio in making various ecclesiastical appointments.<ref>Wilkie, 1974, pp. 214–215.</ref> The fifth session of the [[English Reformation Parliament|Reformation Parliament]] deprived Campeggio and [[Girolamo Ghinucci]] of their English sees (unless they swore loyalty to the king).<ref>Wilkie, 1974, p. 216.</ref> Unaware of this statute, two days later on 23 March 1534 Campeggio entered Consistory for the final ruling against annulment.<ref>Wilkie, 1974, pp. 218–219.</ref>

According to Wilkie, "years of cooperation from both popes and cardinal protectors had taught a wilful Henry VIII to expect to have his way over the church of England".<ref>Wilkie, 1974, p. 217.</ref> Clement VII died on 25 September before learning of the denial of papal authority on 31 March by the [[Convocation of Canterbury]].<ref>Wilkie, 1974, p. 219.</ref> In the [[papal conclave, 1534]], Campeggio was the only cardinal to oppose Farnese's proposal for non-secret voting and the only cardinal not to kiss the feet of the newly elected Farnese as Paul III.<ref>Wilkie, 1974, p. 220.</ref>

==Attempts at reconciliation==
[[File:John Fisher (painting).jpg|thumb|right|[[John Fisher]], the only cardinal recognised by the church as a martyr]]
Many in Rome still thought reconciliation with England was possible, and Paul III elevated two English cardinals, [[John Fisher]] (at the time imprisoned and sentenced to death by Henry VIII) and [[Girolamo Ghinucci]].<ref name="w224">Wilkie, 1974, p. 224.</ref> The execution of Fisher prompted Paul III to excommunicate and purportedly depose Henry VIII.<ref name="w224"/> While Campeggio lived, no attempt was made in Rome to fill any of the thirteen episcopal vacancies in England.<ref>Wilkie, 1974, p. 234.</ref>

Queen [[Mary I of England]] briefly reconciled with Rome and appointed [[Reginald Pole]] as archbishop of Canterbury.<ref name="w238">Wilkie, 1974, p. 238.</ref> However, "papal restoration in England was doomed even before it was accomplished" when Mary I married [[Philip II of Spain]].<ref name="w238"/> In 1555, [[Pope Paul IV]] named a new cardinal protector, [[Giovanni Morone]], but the queen did not confirm the nomination and Campeggio remained the last cardinal protector "chosen by the crown".<ref name="w238"/>

Meanwhile, loyalty to the pope became a defining feature of the movement for [[Irish nationalism]] and bishops appointed by the pope garnered a larger following than the hierarchy of the church of Ireland appointed by the crown.<ref name="w239">Wilkie, 1974, p. 239.</ref> According to Wilkie, "the cardinal protectors had assisted in the loss of England to the papacy, and Ireland remained loyal to the papacy in spite of them".<ref name="w239"/>

==List of Cardinal protectors==
*[[Pope Pius III|Francesco Piccolomini]] (future Pope Pius III), first cardinal protector of England (''ante'' 8 February 1492 – 1503), ''de facto'' protector of Germany<ref name="s29">Signorotto and Visceglia, 2002, p. 29</ref><ref name="w20">Wilkie, 1974, p. 20.</ref>
*[[Adriano Castellesi]], ''de facto'' protector of England and official protector of Germany<ref>Wilkie, 1974, pp. 27–28.</ref>
*[[Galeotto Franciotti della Rovere]] (1505–11 September 1508)<ref name="w35">Wilkie, 1974, p. 35.</ref>
*[[Francesco Adiosi]] (1508–1510)
*[[Pope Clement VII|Giulio de'Medici]] (1514–1523) (future Pope Clement VII)<ref name="nenner">Nenner, Howard A. 1977, March. Book Review. ''Journal of the American Academy of Religion''. '''45''', 1: 101.</ref>
*[[Lorenzo Campeggio]] (1523–1534, died 1539)<ref name="nenner"/><ref>Wilkie, 1974, p. vii.</ref>

;Not confirmed by the crown
*[[Giovanni Morone]], (1578–1579)<ref name="English college"/><ref>Salvator, Miranda. 1998. "[http://www.fiu.edu/~mirandas/bios1542.htm Consistory of June 2, 1542 (VII)]."</ref>
*[[Philip Howard (cardinal)|Philip Howard]] (1682–1694)<ref name="English college">{{Cite CE1913|wstitle=The English College, in Rome}}</ref>
*[[Filippo Antonio Gualterio (cardinal)|Filippo Antonio Gualterio]] (circa 1717)<ref name="m5171706">Miranda, Salvator. 1998. "[http://www.fiu.edu/~mirandas/bios1706.htm Consistory of May 17, 1706 (II)]."</ref>
*[[Cardinal Baschi]] (circa 4 November 1797)<ref name="English college"/>
*[[Ercole Consalvi]] (circa 1817, acting)<ref name="English college"/>

;Similar prior offices
*[[Thomas of Jorz]], proctor for Kings [[Edward I of England|Edward I]] and [[Edward II of England]] (1305–1310)<ref>[http://www.fiu.edu/~mirandas/bios1305.htm#Jorz]</ref>
*[[Ferry de Clugny]], employed in Rome by [[Edward IV of England]] (d. 1483)<ref>Wilkie, 1974, pp. 10–11.</ref>

==Notes==
{{reflist|3}}

==References==
*Baumgartner, Frederic J. 2003. ''Behind Locked Doors: A History of the Papal Elections''. Palgrave Macmillan. ISBN 0-312-29463-8.
*[[Ludwig von Pastor|Pastor, Ludwig]]. 1902. ''The History of Popes''.  K. Paul, Trench, Trübner & Co., Ltd.
*Signorotto, Gianvittorio, and Visceglia, Maria Antonietta. 2002. ''Court and Politics in Papal Rome, 1492–1700''. Cambridge University Press. ISBN 0-521-64146-2.
*Wilkie, William E. 1974. ''The cardinal protectors of England''. Cambridge University Press. ISBN 0-521-20332-5.

{{DEFAULTSORT:Cardinal Protector Of England}}
[[Category:1492 establishments in England]]
[[Category:1539 disestablishments in England]]
[[Category:Cardinal protectors|England]]
[[Category:Catholicism-related controversies]]
[[Category:Christianity in medieval England]]
[[Category:English cardinals| ]]
[[Category:English diplomats]]
[[Category:English Reformation]]
[[Category:History of Roman Catholicism in England]]
[[Category:Lists of Roman Catholics]]