{{Infobox journal
| title = Immunotherapy
| cover = [[File:Immunotherapy_journal_cover.jpg]]
| former_name = <!-- or |former_names= -->
| abbreviation = Immunotherapy
| discipline = [[Immunology]]
| editors = Y. Kawakami, F.M. Marincola, K. Tsang, D.C. Wraith
| publisher = [[Future Medicine]]
| country =
| history = 2009-present
| frequency = Monthly
| openaccess = [[Hybrid open access journal|Hybrid]]
| license = 
| impact = 2.083
| impact-year = 2015
| ISSN = 1750-743X
| eISSN = 1750-7448
| CODEN = IMMUCO
| JSTOR = 
| LCCN = 2009243240
| OCLC = 495368971
| website = http://www.futuremedicine.com/loi/imt
| link1 =
| link1-name =
}}
'''''Immunotherapy''''' is a monthly [[peer reviewed]] [[medical journal]] covering [[immunology]] and more specifically [[immunotherapy]]. It was established in 2009 and is published by [[Future Medicine]]. The founding [[editor-in-chief]] was Duc Hong Le. The current editors-in-chief are Y. Kawakami, F.M. Marincola, K. Tsang and D.C. Wraith.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Biological Abstracts]]<ref name=BA>{{cite web |url=http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=BA |title=Biological Abstracts - Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-05-12}}</ref>
*[[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-13}}</ref>
*[[Chemical Abstracts]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-13 |deadurl=yes }}</ref>
*[[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/online-tools/embase/about |title=Journal titles covered in Embase |publisher=[[Elsevier]] |work=Embase |accessdate=2015-05-12}}</ref>
*[[Index Medicus]]/[[PubMed]]/[[MEDLINE]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101485158 |title=Immunotherapy |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-13}}</ref>
*[[Science Citation Index Expanded]]<ref name= ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-05-12}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.083, ranking it 113th out of 150 journals in the category "Immunology".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Immunology |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist|30em}}

== External links ==
{{Official website|http://www.futuremedicine.com/loi/imt}}

[[Category:Immunology journals]]
[[Category:Publications established in 2008]]
[[Category:Monthly journals]]
[[Category:Future Science Group academic journals]]
[[Category:English-language journals]]