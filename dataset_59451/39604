{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox accounting_body
|name_in_local_language= <!--Give name in other languages. Page name will be added as English name automatically  -->
|abbreviation          = CISPA
|image                 = logo_cispa.gif
|size                  = 75
|image_border          =
|alt                   = <!-- alt text; see [[WP:ALT]] -->
|image_caption         =
|motto                 =
|predecessor           =
|formation             = 1970
|legal_status          =
|objective             = Education and licensing of professional accountants
|headquarters          = {{flag|Cayman Islands}}
|coords                =
|region_served         = Cayman Islands
|membership_num        =
|students_num          = <!-- No of students -->
|members_designations  = <!--give the various designations awarded to the members like ACA, FCA, etc.. -->
|language              = English
|highest_officer_title = President
|highest_officer_name  = Baron Jacob
|second_officer_title  = <!-- position of the second ranking officer like Vice-President, Deputy Chairman etc.. -->
|second_officer_name   = <!-- name of the above person -->
|third_officer_title   = <!-- position of the third ranking officer like Secretary, CEO etc.. -->
|third_officer_name    = <!-- name of the above person -->
|fourth_officer_title  = <!-- position of the fourth ranking officer like Treasurer, CFO etc.. -->
|fourth_officer_name   = <!-- name of the above person -->
|administering_organ   = <!-- Council, General Body, board of directors, etc -->
|IFAC_membership_date  = <!-- Date when the body became a IFAC member if it is one -->
|additional_data_name1 = <!-- Optional data1 name -->
|additional_data1      = <!-- Optional data1 -->
|additional_data_name2 = <!-- Optional data2 name -->
|additional_data2      = <!-- Optional data2 -->
|additional_data_name3 = <!-- Optional data3 name -->
|additional_data3      = <!-- Optional data2 -->
|num_staff             =
|website               = {{url|www.cispa.ky/}}
|remarks               =
|former name           = <!-- former name if any -->
}}
The '''Cayman Islands Society of Professional Accountants (CISPA)''' is a professional association of accountants in the [[Cayman Islands]], a [[British Overseas Territory]] in the [[Caribbean]]. CISPA is responsible for licensing accounting practitioners, supports education of accountants and participates in decisions about the financial industry in the [[offshore financial centre]].<ref>{{cite web
 |url=http://www.caymanfinances.com/Industry-News/cayman-net-news.html
 |title=New body signs licence certificates for practicing public accountants
 |date=2 January 2007
 |work=Cayman Finance
 |accessdate=2 July 2011}}</ref>

==Authority==

CISPA was founded as a professional association in 1970.<ref>{{cite web
 |url=http://www.cispa.ky/content.aspx?contentid=2
 |title=About CISPA
 |publisher=CISPA
 |accessdate=3 July 2011}}</ref>
By 2007 the society had grown to 1000 members.
The passage of the Public Accountants Law and its associated regulations in 2007 gave increased regulatory responsibilities to CISPA.
Under the new law, CISPA became responsible for regulating the profession in the Caymans, including members and other accountants.<ref name=Compass20070403/>
CISPA has been an associate of the [[International Federation of Accountants]] (IFAC) since 2008.<ref>{{cite web
 |url=http://web.ifac.org/about/member-bodies
 |title=Member Bodies and Associates
 |publisher=IFAC
 |accessdate=2 July 2011}}</ref>
As of 2008 the [[Institute of Chartered Accountants of the Caribbean]] (ICAC) was continuing membership discussions with CISPA from the previous year.<ref>{{cite web
 |url=http://www.icac.org.jm/pdf/ICAC_2008_Annual_Report.pdf
 |title=Annual Report 2008
 |publisher=ICAC
 |accessdate=16 July 2011}}</ref>

==Education==

CISPA works with the [[University College of the Cayman Islands]] (UCCI) in defining the college's accounting program, and as of September 2007 to offer [[Certified Public Accountant]] (CPA), [[Certified General Accountant]] (CGA) and [[Association of Chartered Certified Accountants]] (ACCA) review classes.<ref name=Compass20070403/>
Reviewing the first year of UCCI's [[Certified Accounting Technician]] program, CISPA considered that it had been successful, although some changes could be made to better prepare students for their first work experience.<ref>{{cite web
 |url=http://www.caycompass.com/cgi-bin/CFPnews.cgi?ID=1023500
 |title=UCCI accounting building bridges
 |work=Caymanian Compass
 |author=Basia Pioro
 |date=12 July 2007
 |accessdate=2 July 2011}}</ref>

==Financial industry participation==

Accounting is an essential activity in the Cayman Islands: in 2006 it was estimated the banking sector contributes about 25&nbsp;percent of the Cayman Islands GDP.<ref>{{cite web
 |url=http://www.caymanfinances.com/Archive-Industry-News/impact-study-on-the-banking-industry-in-the-cayman-islands.html
 |title=Impact study on the banking industry in the Cayman Islands
 |date=24 July 2006
 |work=Cayman Finance
 |accessdate=2 July 2011}}</ref>
CISPA is a member of the Cayman Islands Financial Services Association (CIFSA), formed in November 2003.<ref>{{cite web
 |url=http://www.caycompass.com/cgi-bin/CFPnews.cgi?ID=1015761
 |title=Cayman gets high praise
 |work=Caymanian Compass
 |date=22 August 2006
 |accessdate=2 July 2011}}</ref>
In 2005 the [[Cayman Islands Monetary Authority]] (CIMA) requested that a working group be formed to review the recommendations of CIMA's Policy and Research Division, which had been examining regulation of the mutual funds industry.
CISPA was invited to participate, as were the Cayman Islands Fund Administrators Association, the Cayman Islands Law Society and the Cayman Islands Bar Association.<ref>{{cite web
 |url=http://goliath.ecnext.com/coms2/gi_0199-4904376/The-Evolution-of-the-Cayman.html
 |title=The Evolution of the Cayman Islands Fund Industry.
 |work=Mondaq Business Briefing
 |date=28 October 2005
 |accessdate=2 July 2011}}</ref>

==References==
{{reflist |refs=
<ref name=Compass20070403>{{cite web
 |url=http://www.caycompass.com/cgi-bin/CFPnews.cgi?ID=1021180
 |work=Caymanian Compass
 |title=Accountants usher in new era
 |date=3 April 2007
 |accessdate=2 July 2011}}</ref>
}}
{{IFAC Members}}
[[Category:Professional accounting bodies|Cayman Islands]]
[[Category:Organizations established in 1970]]
[[Category:Organisations based in the Cayman Islands]]
[[Category:1970 establishments in the Cayman Islands]]