{{featured article}}

{{Infobox person
| name   = Luo Yixiu
| birth_date  = 20 October 1889
| birth_place = [[Shaoshan]], [[Hunan]], [[Qing dynasty|China]]
| death_date  = {{Death date and age|1910|2|11|1889|10|20|df=yes}}
| death_place = Shaoshan, Hunan, China
| occupation  = 
| spouse      = {{marriage|[[Mao Zedong]]|1908|1910}}
| parents     = Luo Helou
}}
{{Chinese name|[[Luo (surname)|Luo]]}}
{{Infobox Chinese
|s = 罗一秀
|t=羅一秀
|p = Luó Yīxiù
|w = Luo<sup>2</sup> I<sup>1</sup>-hsiu<sup>4</sup>
|mi = {{IPAc-cmn|l|uo|2|-|yi|1|.|x|iu|4}}
}}
'''Luo Yixiu'''{{efn|Different biographers of Mao have rendered his first wife's name differently. In his 1999 biography, Philip Short referred to her as "Miss Luo",{{sfn|Short|1999|pp=29, 269}} while in their 2005 book, Jung Chang and Jon Halliday named her "Woman Luo".{{sfn|Chang|Halliday|2005|p=7}} Her name is rendered as "Luo Yixiu" in a number of encyclopedia articles about Mao, such as those of Yuen Ting Lee,{{sfn|Lee|2013|p=235}} and James Z. Gao.{{sfn|Gao|2009|p=137}} Conversely, in 2012, Alexander V. Pantsov and Stephen I. Levine asserted that her name was "Luo Yigu", meaning "First Daughter", although the reasons were unspecified.{{sfn|Pantsov|Levine|2012|pp=25–26, 589}} Many other biographers, such as Ross Terrill,{{sfn|Terrill|1980|p=12}} Clare Hollingworth,{{sfn|Hollingworth|1985|p=18}} and Lee Feigon,{{sfn|Feigon|2002|p=23}} did not name her.}} ({{zh|s=罗一秀|t=羅一秀}}; October 20, 1889 – February 11, 1910), a [[Han Chinese]] woman, was the first wife of the later Chinese [[communism|communist]] revolutionary and political leader [[Mao Zedong]], to whom she was married from 1908 until her death. Coming from the area around [[Shaoshan]], [[Hunan]], in south central China – the same region as Mao – her family were impoverished local landowners.

Most of what is known about their marriage comes from an account Mao gave to the American reporter [[Edgar Snow]] in 1936, which Snow included in his book ''[[Red Star Over China]]''. According to Mao, he and Luo Yixiu were the subject of an [[arranged marriage]] organised by their respective fathers, [[Mao Yichang]] and Luo Helou. Luo was eighteen and Mao just fourteen years old at the time of their betrothal. Although Mao took part in the wedding ceremony, he later stated that he was unhappy with the marriage, never consummating it and refusing to live with his wife. Socially disgraced, she lived with Mao's parents for two years until she died of [[dysentery]], while he moved out of the village to continue his studies elsewhere, eventually becoming a founding member of the [[Communist Party of China]]. Various biographers have suggested that Mao's experience of this marriage affected his later views, leading him to become a critic of arranged marriage and a vocal [[feminism|feminist]]. He would marry three more times throughout his life, to [[Yang Kaihui]], [[He Zizhen]] and [[Jiang Qing]], the last of whom was better known as Madame Mao.

==Early life==
Born on October 20, 1889, Luo Yixiu was the eldest daughter of Luo Helou ({{lang|hans|罗合楼}}; 1871–1943), a ''shenshi'' ({{lang|hans|绅士}}){{spaced ndash}} or rural intellectual who earned his living as a farmer{{spaced ndash}} and his wife (1869–1912), whose surname was Mao and who was a distant great-aunt of Mao Zedong.{{sfnm|1a1=Pantsov|1a2=Levine|1y=2012|1p=25|2a1=Hu|2a2=Liu|2y=1996|2p=111 (dates of Luo Helou; Luo Yixiu as his eldest daughter; relation to Mao's family)}}{{efn|Luo Yixiu's mother was the eldest daughter of Mao Yongtang ({{lang|hans|毛咏堂}}; 1846–1903). The [[Zupu|genealogy book]] of the Mao lineage places Mao Yongtang in the same generation as Mao Zedong's great-grandfather, but Yongtang was of the same [[Generation#Social generation|social generation]] as Mao Zedong's grandfather Mao Enpu ({{lang|hans|毛恩普}}).{{sfn|Hu|Liu|1996|p=111}}}} Although historian [[Lee Feigon]] stated that the Luo family was locally important,{{sfn|Feigon|2002|p=23}} Mao biographers Alexander V. Pantsov and Steven I. Levine claimed that they had fallen into poverty.{{sfn|Pantsov|Levine|2012|p=25}} Luo Helou and his wife had five sons and five daughters, but seven of these children died, leaving them only three daughters. The couple's lack of adult sons diminished their social status, for in Chinese society at the time, only sons could continue the family lineage.{{sfn|Pantsov|Levine|2012|p=26}}

==The marriage==

===Preparation===

{{double image|right|Mao Yichang.jpg|125|Mao Zedong 1913.jpg|141|Mao Yichang (left) organised the marriage for his eldest son, Mao Zedong (right).}}

Mao Zedong had been born and raised at his father's farm in Shaoshanchong, a small rural village named for the nearby Shaoshan mountain.{{sfn|Pantsov|Levine|2012|p=11}} His disciplinarian father, [[Mao Yichang]], had decided to deal with Zedong's rebellious attitude in a manner typical of the time, by forcing him into an arranged marriage that would compel him to take family matters seriously.{{sfnm|1a1=Terrill|1y=1980|1p=12|2a1=Feigon|2y=2002|2p=23|3a1=Hu|3a2=Liu|3y=1996|3pp=111–112}} Yichang also desired a helper for his own wife, [[Wen Qimei]], whose health had deteriorated through years of heavy agricultural labour.{{sfn|Pantsov|Levine|2012|p=26}} He selected Luo Yixiu in either late 1907 or 1908.{{sfn|Pantsov|Levine|2012|p=25}} Her [[kinship]] to the Maos may have helped in this selection, as Luo Yixiu's mother's four brothers, surnamed Mao, lived only two ''[[li (unit)|li]]'' (1 km) from Mao Yichang's home in Shaoshanchong.{{sfn|Hu|Liu|1996|p=111}} Following traditional procedures, a matchmaker would have been sent to the Luo family house, and the Luo family would have been socially expected to accept the marriage proposal immediately.{{sfn|Pantsov|Levine|2012|p=26}} Luo Helou was happy to see his eldest daughter married.{{sfn|Pantsov|Levine|2012|p=26}} The two families exchanged gifts and signed the marriage contract, after which the marriage was considered inviolable.{{sfn|Pantsov|Levine|2012|p=26}}

Zedong first met Yixiu on the day that the contract was signed.{{sfn|Pantsov|Levine|2012|p=26}} Years later, his granddaughter Kong Dongmei stated that Mao was unhappy with his father's choice, and that he instead was in love with his cousin, Wang Shigu. However, marriage to Wang had been ruled out by a local [[divination|diviner]] because their [[Chinese astrology|horoscope]]s were incompatible.{{sfn|Pantsov|Levine|2012|p=26}} Although displeased by the arrangement, Mao agreed to marry Luo.{{sfn|Pantsov|Levine|2012|p=26}} At the time he was fourteen, and later erroneously informed [[Edgar Snow]] that Luo was aged twenty,{{sfn|Snow|1961|p=145}} a claim independently accepted by Mao biographers [[Ross Terrill]] and [[Philip Short]],{{sfnm|1a1=Terrill|1y=1980|1p=12|2a1=Short|2y=1999|2p=29}} but later challenged by biographers [[Jung Chang]] and [[Jon Halliday]], and Alexander V. Pantsov and Steven I. Levine, who established that she was eighteen.{{sfnm|1a1=Chang|1a2=Halliday|1y=2005|1p=7|2a1=Pantsov|2a2=Levine|2y=2012|2p=25}}

===The wedding===

The wedding took place in 1908.{{sfn|Chang|Halliday|2005|p=7}} According to a number of Mao's biographers, the ceremony would have likely followed traditional rural Hunanese custom. Thus, it probably would have begun with a feast in the groom's home on the day before the ceremony, to which friends and relatives were invited. The next day, the bride would have been dressed in red, with a red veil over her face, and carried by red [[palanquin]] to the groom's family home.  There, her veil would have been removed, and she would have been expected to express unhappiness and dissatisfaction with the groom by publicly insulting him.{{sfnm|1a1=Terrill|1y=1980|1p=12|2a1=Pantsov|2a2=Levine|2y=2012|2p=26}} According to tradition, a display of [[fireworks]] would probably have taken place, before both bride and groom would have [[kowtow]]ed to each guest, to the groom's [[Ancestor veneration in China|ancestral altar]], to the spirits, and to one another, concluding the ceremony.{{sfnm|1a1=Terrill|1y=1980|1p=12|2a1=Short|2y=1999|2p=649|3a1=Pantsov|3a2=Levine|3y=2012|3p=26}}

If traditional practices were adhered to, feasting would have continued for two days, while guests would have given gifts, mainly of money, to the newlyweds.{{sfn|Pantsov|Levine|2012|p=26}} The wedding ceremony would have culminated with the guests entering the bridal chamber, where they would have made various sexual references and innuendos, led by a figure with his face painted black.{{sfn|Pantsov|Levine|2012|p=27}} In Chinese rural tradition, the bride was expected to show the bloodstains on the bed sheets from her wedding night to prove that her [[hymen]] had been broken during [[sexual intercourse]], and that she had therefore been a virgin.{{sfn|Pantsov|Levine|2012|p=27}}

===Married life===
[[File:Shaoshan 01.JPG|thumb|right|250px|Mao's Shaoshanchong home in 2010; it was here that Luo lived after the wedding.]]
According to what he told Snow, Mao refused to live with his wife and claimed that they had never consummated their marriage.{{sfnm|1a1=Snow|1y=1961|1p=145|2a1=Short|2y=1999|2p=29}} Soon after the wedding, he ran away from home to live with an unemployed student in Shaoshan.{{sfnm|1a1=Short|1y=1999|1p=29|2a1=Pantsov|2a2=Levine|2y=2012|2p=27}} There he spent much of his time reading, particularly historical works like [[Sima Qian]]'s ''[[Records of the Grand Historian]]'' and [[Ban Gu]]'s ''[[History of the Former Han Dynasty]],'' and political tracts like [[Feng Guifen]]'s ''Personal Protests from the Study of Jiao Bin''.{{sfnm|1a1=Short|1y=1999|1p=29|2a1=Pantsov|2a2=Levine|2y=2012|2p=27}}

Now considered part of the Mao family, Luo lived with Mao Yichang and Wen Qimei at their home, but was publicly humiliated by her husband's disappearance; some locals considered her to be Yichang's [[concubine]].{{sfnm|1a1=Short|1y=1999|1p=29|2a1=Pantsov|2a2=Levine|2y=2012|2p=27}} Luo Yixiu died of dysentery on February 11, 1910, the day after [[Chinese New Year]].{{sfnm|1a1=Chang|1a2=Halliday|1y=2005|1p=7|2a1=Pantsov|2a2=Levine|2y=2012|2pp=28, 589|3a1=Lü|3y=2009|3p=[http://dangshi.people.com.cn/GB/144956/11272977.html 2]}} Mao Zedong returned home; his father forgave him for his disobedience, and in the autumn of 1910 agreed to finance his son's studies at the Dongshan Higher Primary School, and so Mao left Shaoshanchong.{{sfn|Pantsov|Levine|2012|p=28}} When in 1936 Mao told Snow "I do not consider her my wife",{{sfn|Snow|1961|p=145}} he made no mention of her death.{{sfn|Chang|Halliday|2005|p=7}} Luo Yixiu's grave is located on the mountain facing Mao Zedong's former residence in Shaoshanchong, a few steps away from the tomb of his parents.{{sfn|Hu|Liu|1996|p=112}}

===Aftermath===
When Mao Zedong returned to Shaoshan in 1925 to organize a local peasant movement, he went to visit Luo Yixiu's relatives, including her father Luo Helou and his nephew Luo Shiquan (石泉). Luo Shiquan would join the Communist Party in the winter of that year and would remain a peasant activist until the [[Chinese Communist Revolution|1949 revolution]].{{sfn|Hu|Liu|1996|pp=112–113}} Because Luo Yixiu had died without offspring, when the Mao lineage updated its [[genealogy book]] in 1941, Mao Anlong ({{lang|hans|毛岸龙}}), who was Mao Zedong's third son with his second wife [[Yang Kaihui]], was listed as Luo's descendant.{{sfn|Hu|Liu|1996|p=112}} In 1950, Mao sent his eldest son [[Mao Anying]] to Shaoshan and instructed him to visit Luo Shiquan.{{sfn|Hu|Liu|1996|p=113}} Mao also kept in contact with two men who had married Luo Yixiu's sisters, and met one of these men when he returned to Shaoshan in 1959 for the first time since the 1920s.{{sfn|Hu|Liu|1996|pp=113–114}}

==Influence on Mao==
In ''[[Mao: A Reinterpretation]]'', American historian [[Lee Feigon]] argued that Mao's experience with arranged marriage inspired him to become "a vehement advocate of women's rights" in the late 1910s, as he began to write articles for the left-wing press criticizing the traditional Chinese family system and arguing that love, rather than societal or family pressures, should be the primary determinant in marriage.{{sfn|Feigon|2002|p=23}} This idea had previously been expressed by journalist and sinologist [[Clare Hollingworth]].{{sfn|Hollingworth|1985|pp=18–19}} In their biography ''[[Mao: The Unknown Story]]'', [[Jung Chang]] and [[Jon Halliday]] agreed, stating that it was this experience with Luo that turned Mao into a "fierce opponent" of arranged marriage.{{sfn|Chang|Halliday|2005|p=7}}

Mao would marry three more women over the course of his life: [[Yang Kaihui]] in December 1920, [[He Zizhen]] in May 1928, and [[Jiang Qing]] in November 1939.{{sfn|Pantsov|Levine|2012|p=589}}

==References==

===Notes===
{{notelist}}

===Footnotes===
{{reflist|colwidth=30em}}

===Bibliography===
{{Refbegin|30em|indent=yes}}
: {{cite book|last1= Chang |first1= Jung |last2= Halliday |first2= Jon |authorlink1= Jung Chang |authorlink2= Jon Halliday |title=Mao: The Unknown Story |year=2005 |publisher=Jonathan Cape |location=London |isbn=978-0-224-07126-0 |ref=harv}}

: {{cite book |last=Feigon |first=Lee |authorlink=Lee Feigon |title=Mao: A Reinterpretation |year=2002 |isbn=978-1-56663-458-8 |publisher=Ivan R. Dee |location=Chicago |ref=harv}}

: {{cite contribution |contribution=Mao Zedong (1893–1976) |last=Gao |first=James Z. |title=Historical Dictionary of Modern China (1800–1949) |others=James Z. Gao (editor) |publisher=Scarecrow Press, Inc |location=Lanham |pages=227–230 |year=2009 |isbn=978-0-8108-6308-8 |ref=harv}}

: {{cite book |last=Hollingworth |first=Clare |authorlink=Clare Hollingworth |title=Mao and the Men Against Him |year=1985 |publisher=Jonathan Cape |location=London |isbn=978-0-224-01760-2 |ref=harv}}

: {{cite journal|last1= Hu |first1= Changming 胡长明 |last2= Liu |first2= Shengsheng 刘胜生 |title= A Few Historical Facts on Mao Zedong's First Marriage 毛泽东第一次婚姻的若干史实|year=1996 |journal= Research on Mao Zedong Thought ''毛泽东思想研究'' |issue=1996.2 <!---this refers to the second issue of 1996 in a journal that does not mark the volume number; the format is correct: please do not modify!--->|pages=111–114|url=http://www.cnki.com.cn/Article/CJFDTotal-MSYJ602.037.htm|ref=harv|language=zh}}

: {{cite contribution |contribution=Mao Zedong (1893–1976) |last=Lee |first=Yuen Ting |title=Biographical Dictionary of the People's Republic of China |others=Yuwu Song (editor) |publisher=McFarland |location=Jefferson |pages=235–237 |year=2013 |isbn=978-0-7864-3582-1 |ref=harv}}

: {{cite journal|last=Lü |first=Chun 吕春|year=2009|pages=|title=Six Women who Influenced Mao Zedong 影响毛泽东一生的六位女性|journal=Dangshi wenyuan ''党史文苑''|volume=|issue=|url=http://dangshi.people.com.cn/GB/144956/11272977.html|ref=harv}}

: {{cite book|last1= Pantsov |first1= Alexander V. |last2= Levine |first2= Steven I. |title=Mao: The Real Story |year=2012 |publisher= Simon & Schuster |location=New York and London |isbn=978-1-4516-5447-9 |ref=harv}}

: {{cite book|last1=Short |first1=Philip |authorlink=Philip Short |title=Mao: A Life |year=1999 |publisher=Hodder & Stoughton |location=London |isbn=0-340-75198-3 |ref=harv}}

: {{cite book|last=Snow |first=Edgar |authorlink=Edgar Snow |title=Red Star Over China |year=1961 |origyear=1937 |publisher=Grove Press |location=New York City |ref=harv}}

: {{cite book|last=Terrill |first=Ross |authorlink= Ross Terrill |title=Mao: A Biography |year=1980 |publisher=Simon and Schuster |location=New York City |isbn=978-0-06-014243-8 |ref=harv}}
{{refend}}

{{Authority control}}
{{DEFAULTSORT:Luo, Yixiu}}

[[Category:1889 births]]
[[Category:1910 deaths]]
[[Category:Mao Zedong family]]
[[Category:People from Xiangtan]]
[[Category:Spouses of national leaders]]