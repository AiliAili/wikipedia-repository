{{Use mdy dates|date=January 2017}}
{{Infobox musical artist
|name            = Gary Clark Jr.
|birth_name      = Gary Lee Clark Jr.
|birth_date      = {{birth date and age|1984|02|15|mf=yes}}
|image           = Gary Clark, Jr. 2013.jpg
|caption         = Clark performing at the [[North Coast Music Festival]] in Chicago in 2013
|background      = solo_singer
|origin          = [[Austin, Texas|Austin]], [[Texas]], United States
|genre           = [[Blues rock]], [[Soul music|soul]], [[contemporary R&B|R&B]], [[blues]], [[hard rock]], [[rock and roll]]
|instruments     = Vocals, guitar, drums, trumpet, keyboards, harmonica
|years_active    = 1996–present
|label           = {{plainlist|
*Hotwire Unlimited
*[[Warner Bros. Records|Warner Bros]]
}}
|website         = {{URL|garyclarkjr.com}}
}}

'''Gary Clark Jr.'''<ref name="BMI-RepertoireProfile">{{cite web|url=http://repertoire.bmi.com/writer.asp?blnWriter=True&blnPublisher=True&blnArtist=True&page=1&fromrow=1&torow=25&querytype=WriterID&keyid=839219&keyname=CLARK+GARY+LEE+JR&CAE=416409570&Affiliation=BMI|title=Songwriter/Composer: Clark Gary Lee Jr.|work=[[Broadcast Music, Inc.|BMI]]|accessdate=September 27, 2015}}</ref> (born February 15, 1984) is an American [[musician]] from [[Austin, Texas]].<ref>http://www.suntimes.com/entertainment/2434930,CST-FTR-blues26.article</ref><ref name="motr.communication.utexas.edu">http://motr.communication.utexas.edu/musicians/prod75_005936.html</ref><ref>{{cite web|url=http://www.gibson.com/en-us/Lifestyle/Features/Gary%20Clark%20Jr_%20Gets%20Lowdown%20an/ |title=Gibson Guitar, Lifestyle, Gary Clark Jr. Gets Lowdown and Funky, December 21, 2007 |publisher=Gibson.com |date= |accessdate=September 10, 2012}}</ref> Dubbed 'The Chosen One', Clark has shared the stage with many legends of rock and roll, including [[Eric Clapton]], [[B. B. King]] and [[the Rolling Stones]].<ref name="texasmonthly.com">{{cite web|last=Holcomb |first=Christopher |url=http://www.texasmonthly.com/2009-05-01/webextra31.php |title=Texas Monthly Article, "Singin the Blues," May 2009 |publisher=Texasmonthly.com |date=May 1, 2009 |accessdate=September 10, 2012}}</ref> He is best known for his fusion of [[blues]], [[Rock music|rock]] and [[soul music]] with elements of [[hip hop music|hip hop]].<ref>Classic Rock Magazine, Issue 175, p72</ref> Clark's musical trademarks are his [[Distortion (music)|distorted]] guitar sound, heavy use of improvisation and his silky smooth vocal style.

==Musical career==
Gary Clark Jr. began playing guitar at the age of twelve. Born and raised in Austin, Texas, Clark played small gigs throughout his teens, until he met promoter [[Clifford Antone]], proprietor of the Austin music club [[Antone's]]. Antone's was the launch pad from which [[Jimmie Vaughan|Jimmie]] and [[Stevie Ray Vaughan]] redefined blues at the time.<ref name="Studio">{{cite web|url=http://www.studio360.org/2011/mar/25/gary-clark-jr-saves-blues/ |title=Studio360.org |publisher=Studio360.org |date= |accessdate=September 10, 2012}}</ref> Soon after meeting Clifford, Clark began to play with an array of musical icons, including Jimmie Vaughan. Vaughan and others in the Austin music community helped Clark along his musical path, facilitating his ascent in the Texas rock and roll scene.<ref name="crossroadsguitarfestival.com">{{cite web|url=http://crossroadsguitarfestival.com/index.php/artists/gary-clark-jr |title=Crossroads Guitar Festival, Artist Information, Gary Clark, Jr |publisher=Crossroadsguitarfestival.com |date=May 3, 2001 |accessdate=September 10, 2012}}</ref> Clark's music demonstrates how the blues have shaped virtually every medium of music over the past century, from hip-hop to country.<ref name="Studio"/>

''[[Rolling Stone]]'' declared Clark "Best Young Gun" in its April 2011, "Best of Rock" issue.<ref>{{cite web|url=https://finance.yahoo.com/news/Gary-Clark-Jr-Hits-the-Road-iw-785587520.html?x=0&.v=1 |title=Gary Clark Jr. Hits the Road for the Bonnaroo Buzz Tour on May 17th|work=[[Yahoo]] Business/Finance |publisher=Finance.yahoo.com |date=May 4, 2011 |accessdate=September 10, 2012}}</ref>

Clark sang on the bonus track cover of "[[I Want You Back]]" by the [[Jackson 5]] on Sheryl Crow's album ''[[100 Miles from Memphis]]''.<ref>{{cite web|url=http://www.bestbuy.com/site/100+Miles+from+Memphis+%5BDigipak%5D+-+CD/1008355.p?id=2117073&skuId=1008355 |title=100 Miles from Memphis CD |publisher=[[Best Buy]] |date= |accessdate=September 10, 2012}}</ref><ref>{{cite web|url=http://www.continentalclub.com/Austin/Confidential/new.html |title=Continental Club Austin, TX |publisher=Continentalclub.com |date= |accessdate=September 10, 2012}}</ref>

More recently,{{when|date=September 2015}} Clark recorded with [[Alicia Keys]] on two different songs in New York, NY.{{citation needed|date=September 2015}} He co-wrote the song Fire We Make with Alicia Keys, Andrew Wansel and Warren Felder for the album Girl on Fire of that one in 2012.<ref>http://www.allmusic.com/album/girl-on-fire-mw0002419336</ref><ref>http://www.allmusic.com/song/fire-we-make-mt0045887530</ref>

On August 28, 2012, Alicia Keys revealed via [[Twitter]] that Clark's new album and major-label debut called ''[[Blak and Blu]]'' would be released on October 22, 2012.<ref name=AliciaKeysTwitter>{{cite web|title=@aliciakeys|url=https://twitter.com/aliciakeys/status/240523427151241216|publisher=Twitter.com|accessdate=August 28, 2012}}</ref> Later that day, the news appeared on Clark's official website.<ref name=NewAlbum>{{cite web|title=Listen To New Music & See Gary on Tour!|url=http://www.garyclarkjr.com/news/listen-new-music-see-gary-tour|publisher=Garyclarkjr.com|accessdate=October 17, 2012}}</ref>

Clark worked with the [[Foo Fighters]] on the track "[[What Did I Do? / God As My Witness (song)|What Did I Do? / God as My Witness]]" on their 2014 album ''[[Sonic Highways]]'' recorded at [[Austin City Limits#Venue|KLRU-TV Studio 6A]] in Austin, TX.<ref>Foo Fighters – "What Did I Do? / God As My Witness" (Feat. Gary Clark, Jr.)[http://www.stereogum.com/1717240/foo-fighters-what-did-i-do-god-as-my-witness-feat-gary-clark-jr/mp3s/] stereogum.com. Retrieved November 7, 2014.</ref><ref>Here's Foo Fighters' Austin, TX, Song 'What Did I Do?/God As My Witness' With Gary Clark Jr. [http://www.billboard.com/articles/6312064/foo-fighters-austin-tx-gary-clark-jr-sonic-highways] billboard.com. Retrieved November 7, 2014.</ref>

Gary Clark released his new album ''The Story of Sonny Boy Slim'' worldwide on September 11, 2015.<ref>http://www.allmusic.com/album/release/the-story-of-sonny-boy-slim-mr0004459164</ref>

Clark also had a guest appearance on Tech N9nes' newly released album ''The Storm''. The album was released on December 9, 2016. Clark provided the chorus for the song "No Gun Control". The song is a mash up of blues and rap.

On Childish Gambino's album ''Awaken, my Love'' Gary Clark performs the guitar solo on Track11, "The Night Me and Your Mama Met".

==Live appearances==
{{BLP sources section|date=September 2015}}
{{Prose|section|date=September 2016}} 
Clark performed at the 2010 [[Crossroads Guitar Festival]] alongside [[B.B. King]], [[Eric Clapton]], [[Buddy Guy]], [[Steve Winwood]], [[John Mayer]], [[Sheryl Crow]], [[Jeff Beck]], and [[ZZ Top]].<ref name="crossroadsguitarfestival.com"/><ref>{{cite web|url=http://www.crossroadsguitarfestival.com |title=Crossroads Guitar Festival |publisher=Crossroads Guitar Festival |date= |accessdate=September 10, 2012}}</ref><ref>{{cite web|url=http://www.startribune.com/entertainment/music/97628014.html?elr=KArksD:aDyaEP:kD:aUt:aDyaEP:kD:aUiD3aPc:_Yyc:aUU |title=Minneapolis-St. Paul Star Tribune Article: July 3, 2010 |publisher=Startribune.com |date=July 3, 2010 |accessdate=September 10, 2012}}</ref> He joined [[Doyle Bramhall II]] and Sheryl Crow on stage for their performance with Eric Clapton, and also debuted several original songs.<ref>{{cite news|url=https://www.nytimes.com/2010/06/28/arts/music/28guitar.html|newspaper=[[The New York Times]]|title=Music Review: Eric Clapton Crossroads Guitar Festival|date=June 27, 2010|accessdate=May 18, 2015}}</ref><ref>{{cite web|url=http://www.premierguitar.com/Handler.ashx?Item_ID=0D5400E4-3BC4-499E-AE47-3CE158075E74&type=large |title=Premier Guitar, "Reporting from Eric Clapton's Crossroads Guitar Festival" Photo Gallery |date= |accessdate=September 10, 2012}}</ref>

In June 2011, Clark played at the annual [[2011 Bonnaroo Music Festival|Bonnaroo Music Festival]] in [[Manchester, Tennessee]],<ref>http://www.bonnaroo.com/artists/gary-clark-jr.aspx</ref> at the Miller Lite On Tap Lounge. On June 10, 2012, Clark again played [[2012 Bonnaroo Music Festival|at Bonnaroo]], and his performance was streamed live online via the Bonnaroo MusicFest Channel on YouTube.

In February 2012, Clark performed alongside blues legends at the ''Red, White and Blues'' event at the White House. The event, aired on PBS, also included [[B.B. King]], [[Mick Jagger]], [[Jeff Beck]] and [[Buddy Guy]], among others. Clark played "Catfish Blues" and "In the Evening (When the Sun Goes Down)", as well as contributing to performances of "Let the Good Times Roll", "Beat Up Old Guitar", "Five Long Years" and "Sweet Home Chicago".<ref name = "In Performance at the White House: Red, White, and Blues">{{cite web|url=http://www.pbs.org/inperformanceatthewhitehouse/songs.php |title=The Songs &#124; In Performance at the White House |publisher=PBS |date= |accessdate=September 10, 2012}}</ref><ref name=EpiAtTheBigHouse>{{cite web|title=Epi at the Big House|url=http://www.epiphone.com/News/Features/News/2012/Epi-at-the-Big-House.aspx|publisher=Epiphone.com|accessdate=May 18, 2015}}</ref><ref>{{cite web|title=Epiphone and Gary Clark Jr at the White House|url=http://www.epiphone.com/News/Features/News/Gary-Clark-Jr-and-Epi-at-the-White-House.aspx|publisher=Epiphone.com|accessdate=May 18, 2015}}</ref>

In June 2012, Clark guested with the [[Dave Matthews Band]] playing "Can't Stop" and "[[All Along the Watchtower]]" at dates in Virginia Beach and Indianapolis and on October 21 and 22, 2012, Clark appeared as the opening act at the [[Bridge School Benefit]] Concert, Bridge XXVI. On December 8, 2012, Clark appeared at [[The Rolling Stones]]' first US-gig of their [[50 & Counting|50th anniversary tour]] at the [[Barclay's Center]] in Brooklyn, NY to perform the [[Don Nix]] song "Going Down" with the band.<ref>{{cite web|url=http://www.iorr.org/tour12/brooklyn.htm |title=The Rolling Stones live at the Barclays Center, Brooklyn, New York, USA, December 8, 2012 by IORR |publisher=Iorr.org |date= |accessdate=March 18, 2013}}</ref> On December 15, 2012 he joined them onstage again to play the same song, along with [[John Mayer]], during the last date of the Stones' mini-tour at the [[Prudential Center]] in Newark, NJ.<ref>{{cite web|url=http://www.iorr.org/tour12/newark2.htm |title=The Rolling Stones live at the Prudential Center, Newark, New Jersey, USA, December 15, 2012 by IORR |publisher=Iorr.org |date= |accessdate=March 18, 2013}}</ref>

On May 13, 2013, Clark opened for Eric Clapton & His Band at the LG Arena, Birmingham, England. Many of the audience felt that he stole the show on the night, with Clapton being castigated by many for being unengaged with his audience and on June 12, 2013, Clark was the guest performer with the Rolling Stones at Boston's [[TD Garden]].  Clark joined the Stones in playing the [[Freddie King]] tune "Going Down". On June 30, 2013, he appeared on the Avalon stage at the [[Glastonbury Festival]]. His performance was declared 'the most electric performance of the festival, knocking the legendary appearance of [[The Rolling Stones]] (the previous night) well into second place' and on October 25, 2013, he appeared on long-running British music show ''[[Later... with Jools Holland]]''.<ref>{{cite web|url=http://www.bbc.co.uk/programmes/p01k5bmf|title=BBC Two – Later... with Jools Holland, Series 43 Live, Episode 6, Gary Clark Jr. – Numb|work=BBC}}</ref>

On February 9, 2014, Clark performed [[The Beatles]] song "[[While My Guitar Gently Weeps]]", along with [[Dave Grohl]] and [[Joe Walsh]] for [[The Night That Changed America: A Grammy Salute to The Beatles|The Beatles: the Night that Changed America]]. On February 16, 2014, Clark performed in during the [[NBA All-Star Game]] Halftime Show with Trombone Shorty, Earth Wind and Fire, Doctor John, and Janelle Monáe and on May 29, 2014, Gary Clark Jr performed solo at [[Rock in Rio]] in [[Lisbon]]. After yet been invited to participate in one of the songs of the legendary band [[The Rolling Stones]] during this festival day. Clark performed guitar, as a guest, on an episode of the PBS cable television show ''[[Austin City Limits]]'' (ACL), with the band "Foo Fighters", that aired on February 7, 2015. He and the Foo Fighters were accompanied, on stage, by another guest guitarist, Jimmie Vaughan, a native of Dallas, TX.

On May 24, 2015, Clark opened for the Rolling Stones at Petco Park in San Diego and on July 4, 2015, played as part of the lineup for the Foo Fighters 20th Anniversary show at RFK Stadium in Washington D.C. On June 8, 2016, he performed alongside [[Jon Batiste]] and [[Stay Human (band)|Stay Human]] as musical guest of [[The Late Show with Stephen Colbert]] and on June 26, 2016, he performed during West Holts Stage, Glastonbury Festival, UK and on July 8, 2016, performed on the Preferred One Stage at the Basilica Block Party in Minneapolis, [http://basilicablockparty.org Minnesota.a]

==In popular culture==
* Clark starred alongside [[Danny Glover]], [[Stacy Keach]], and [[Charles S. Dutton|Charles Dutton]] in [[John Sayles]]' 2007 [[film]], ''[[Honeydripper (film)|Honeydripper]]''.<ref>{{cite web|url=http://www.austinchronicle.com/gyrobase/Issue/story?oid=oid:582339 |title=From Stage to Screen with Gary Clark Jr. |work=[[Austin Chronicle]]  |date= January 18, 2008|accessdate=September 10, 2012}}</ref><ref name="autogenerated1">{{cite web|url=http://www.imdb.com/name/nm2237175/|title=Gary Clark Jr.|work=IMDb.com|accessdate=May 18, 2015}}</ref>
* In 2010, Clark and his band played onscreen in an episode of the acclaimed television series ''[[Friday Night Lights (TV series)|Friday Night Lights]]''.<ref>{{cite web|url=http://rockmrtom.files.wordpress.com/2013/09/gary-clark-jr-bluesrevue137.pdf |title=Gary Clark Jr., 21st century blues rising from Austin |author=Hyslop, Tom |work=Blues Review}}</ref>
* The song "Don't Owe You a Thang" is played in the [[Clint Eastwood]] movie ''[[Trouble with the Curve]]'' and ''[[Longmire (TV series)|Longmire]]''.
* "Bright Lights" can be heard in a 2011 Jack Daniel's commercial as well as in the 2012 videogame ''[[Max Payne 3]]''. It has also been used in soundtrack of ''[[Stand Up Guys]]'', in the final shootout scene. The song was also featured in the USA Network series ''[[Suits (TV series)|Suits]]'' in the episode "High Noon" and in the Showtime series ''[[House of Lies]]'' in the episode "Gods of Dangerous Financial Instruments".
* Clark appears with his band performing "Travis County" and "When My Train Pulls In" in the 2014 [[Jon Favreau]] film ''[[Chef (film)|Chef]]''.
* "Ain't Messin 'Round" was featured in the motion picture ''Identity Thief'' in 2013.<ref name="autogenerated1"/>
* Clark was featured in the motion picture ''Miles Ahead'' as part of [[Miles Davis]] band.<ref name="autogenerated1"/>
* "The Healing" was featured on [[CBS]] during their commercials for the [[59th Annual Grammy Awards]].

==Awards and recognitions==
[[Kirk Watson]], the Mayor of Austin, proclaimed May 3, 2001 to be Gary Clark Jr. Day. Clark was seventeen years old at the time.<ref name="motr.communication.utexas.edu"/><ref name="texasmonthly.com"/><ref>{{cite web|url=http://current.com/entertainment/music/90849213_you-gotta-check-out-gary-clark-jr.htm |title=Current Article, "You Gotta Check Out: Gary Clark Jr.," September 2, 2009 |publisher=Current.com |date= |accessdate=September 10, 2012}}</ref> Clark won the Austin Music Award for Best Blues and Electric Guitarist, on three different occasions.<ref name="crossroadsguitarfestival.com"/>

Clark was SPIN magazine's breakout artist for the month of November 2011.<ref>{{cite web|last=Barshad |first=Amos |url=http://www.spin.com/articles/breaking-out-gary-clark-jr |title=Breaking Out: Gary Clark Jr. &#124; SPIN &#124; Profiles &#124; Spotlight |publisher=SPIN |date=October 27, 2011 |accessdate=September 10, 2012}}</ref>

''[[Rolling Stone]]'' magazine ranked Clark's Bright Lights EP (named for the title track, an homage to [[Jimmy Reed]] and his song of the same name), number 40 on its list of its top 50 albums of 2011.<ref>{{cite web|url=http://www.rollingstone.com/music/lists/50-best-albums-of-2011-20111207/gary-clark-jr-the-bright-lights-ep-19691231 |title=50 Best Albums of 2011: Gary Clark Jr., 'The Bright Lights EP' |publisher=Rolling Stone |date= |accessdate=September 10, 2012}}</ref>

"Bright Lights" can be heard in the video game ''[[Max Payne 3]]'', the premiere episode of ''[[House of Lies]]'', as well as in the movie ''[[Think Like a Man]]'' near the end when the guys are in the bar before reconciling with their respective ladies, and "Don't Owe You a Thang" can be heard in ''[[Need for Speed: The Run]]''

[[Kirk Hammett]] from Metallica introduced Clark onstage before his performance at the [[Orion Festival]] in Atlantic City, NJ.

While playing music festivals such as [[Coachella Valley Music and Arts Festival|Coachella]], [[JazzFest]], Memphis [[Beale St.]], Hangout, [[High Sierra Music Festival|High Sierra]], [[Sasquatch! Music Festival|Sasquatch]], [[Mountain Jam (festival)|Mountain Jam]], [[Wakarusa Music and Camping Festival|Wakarusa]], Bonnaroo, Electric Forest, [[Hard Rock Calling]], [[Newport Folk Festival]], Orion Music Festival, [[Osheaga]], [[Lollapalooza]], and [[ACL Music Festival]], Clark was awarded ''[[SPIN Magazine]]'''s Golden Corndog award for performing in more major [[North American Music Festivals]] in 2012 than any other musician on the planet.<ref>''SPIN Magazine'', April 2012 Issue</ref>

Clark swept the 31st annual Austin Music Awards for 2012–2013, collecting eight awards, he earned the following: Band of the Year, Musician of the Year, Song of the Year – "Ain't Messin Round" (from ''Blak and Blu''), Album of the Year – ''Blak and Blu'', Electric Guitarist of the Year, Songwriter of the Year, Blues/Soul/Funk Artist of the Year, Male Vocalist of the Year.

For his song "Ain't Messin Round", Clark was nominated for the [[Grammy Award]] for Best Rock Song in 2013.  On January 26, 2014, Clark won the Grammy Award for Best Traditional R&B performance at the 56th annual Grammy Awards Ceremony for his song "Please Come Home."

In 2014 and 2015, Clark won a [[Blues Music Award]] in the 'Contemporary Blues Male Artist of the Year' category.<ref>{{cite web|url=http://blues.about.com/od/bluesawards/a/2014-Blues-Music-Awards.htm |title=2014 Blues Music Awards Nominees and Winners |publisher=Blues.about.com |date= |accessdate=May 16, 2014}}</ref><ref>{{cite web|author= |url=http://www.americanbluesscene.com/2015/05/2015-blues-music-awards-winners/ |title=2015 Blues Music Awards Winners |publisher=Americanbluesscene.com |date= |accessdate=May 18, 2015}}</ref>

==Instruments==
Gary Clark Jr. mainly uses [[Epiphone Casino]], both [[P-90]] and [[Humbucker]] [[Gibson SG]], and both [[Fender Stratocaster]] and [[Fender Telecaster]] electric guitars, as well as [[Epiphone]] Masterbilt and [[Gibson Hummingbird]] acoustic guitars. Clark has his own signature Black & Blu [[Epiphone Casino]] which features [[Gibson Guitar Corporation|Gibson]] USA made [[P-90]] pickups.<ref>{{cite web|url=https://www.youtube.com/watch?v=x_ZeDn-hHGE |title=Gary Clark Jr. – Bright Lights |publisher=YouTube |date=January 28, 2011 |accessdate=September 10, 2012}}</ref><ref name=EpiphoneGaryClarkJrInGuitarWorld>{{cite web|title=Gary Clark Jr in March 2012 Issue of Guitar World|url=http://www.epiphone.com/News/Features/Features/2012/Gary-Clark-Jr-in-March-2012-Issue-of-Guitar-World.aspx|publisher=Epiphone.com|accessdate=February 24, 2012}}</ref><ref name=EpiphoneCasino>{{cite web|title=Epiphone Casino|url=http://www.epiphone.com/Products/Archtop/Casino.aspx|accessdate=February 24, 2012}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=MPYLVQfjCts |title=GARY CLARK JR. – "When My Train Pulls In" (Live in Griffith Park, CA) #JAMINTHEVAN |publisher=YouTube |accessdate=February 24, 2012}}</ref>

Clark uses .011-.049 D'Addario Strings EXL 115<ref>{{cite web|url=http://kkguitar.com/gary-clark-jr/|title=Gary Clark Jr.|work=Kkguitar.com|accessdate=May 18, 2015}}</ref>

Clark uses a Fender Vibro-King amp purchased from Zapata (who currently tours with him and plays rhythm guitar) paired with a Fender Princeton. He is known for extensive use of fuzz pedals, with his most frequently used pedal being the Fulltone Octafuzz, and regular use of a wah pedal.<ref>{{cite web|url=http://www.guitarworld.com/interview-austins-gary-clark-jr-discusses-influences-gear-and-his-schizophrenic-style|title=Interview: Austin's Gary Clark Jr. Discusses Influences, Gear and His "Schizophrenic" Style|work=Guitarworld.com|accessdate=May 18, 2015}}</ref>

==Charity==
Clark performed at [[Alicia Keys]]' [[Keep a Child Alive]] [[Black Ball benefit]], in an effort to raise money for children with [[AIDS in Africa]].<ref>{{cite news| url=http://www.usatoday.com/life/people/story/2011-11-04/Alicia-Keys-Black-BALL/51071530/1 | work=USA Today | title=Most Popular E-mail Newsletter | date=November 4, 2011|accessdate=May 18, 2015}}}</ref> The two performed the [[Beatles]]' "[[While My Guitar Gently Weeps]]" as a tribute to [[George Harrison]].<ref>{{cite web|url=https://www.youtube.com/watch?v=ZXR9kLMa_D0 |title=Alicia Keys & Gary Clark Jr – Black Ball Performance [Live |publisher=YouTube |date=November 22, 2011 |accessdate=September 10, 2012}}</ref>

==Personal life==
On November 5, 2014, it was announced Clark Jr. is engaged to his longtime girlfriend model [[Nicole Trunfio]].<ref>{{cite web|url=http://www.dailymail.co.uk/tvshowbiz/article-2821723/Nicole-Trunfio-make-free-tosses-wild-hair-latest-selfie-pregnancy-rumours-persist.html|title=Nicole Trunfio pregnant with her first child|work=Mail Online|accessdate=May 18, 2015}}</ref> On January 11, 2015, the couple welcomed their son Zion.<ref>{{cite web|url=http://www.dailymail.co.uk/tvshowbiz/article-2906292/Heaven-sent-angel-Nicole-Trunfio-gives-birth-child-fiance-Gary-Clark-Jr-declaring-love.html|title=Nicole Trunfio gives birth to first child with fiance Gary Clark Jr|first=Bianca|last=Soldani|publisher=The Daily Mail|date=January 12, 2015|accessdate=February 22, 2016}}</ref>

==Discography==
{{BLP sources section|date=September 2015}}

===Studio albums===
{| class="wikitable"
|-
! rowspan="2" style= "width:200px;"|Title
! colspan="7" style= "width:20px;"|Peak positions
|-
! scope="col" style="width:3em;font-size:90%;"| [[Billboard 200|US]]<br><ref name="billboard 200">{{cite web | url=http://www.billboard.com/artist/302609/gary-clark-jr/chart?f=305| title=Gary Clark Jr. – Chart History | work=''[[Billboard (magazine)|Billboard]] '' |accessdate=December 28, 2015}}</ref>
! scope="col" style="width:3em;font-size:90%;"| [[ARIA Charts|AUS]]<br><ref name="AUS">{{cite web | url=http://australian-charts.com/showinterpret.asp?interpret=Gary+Clark+Jr%2E | title=Gary Clark Jr. discography | publisher=Hung Medien | work=''australian-charts.com'' |accessdate=October 1, 2013}}</ref>
! scope="col" style="width:3em;font-size:90%;"| [[Syndicat National de l'Édition Phonographique|FRA]]<br><ref>{{cite web | url=http://lescharts.com/showinterpret.asp?interpret=Gary+Clark+Jr%2E | title=Gary Clark Jr. discography | publisher=Hung Medien | work=''lescharts.com'' |accessdate=October 1, 2013}}</ref>
! scope="col" style="width:3em;font-size:90%;"| [[GfK Entertainment Charts|GER]]
! scope="col" style="width:3em;font-size:90%;"| [[MegaCharts|NLD]]
! scope="col" style="width:3em;font-size:90%;"| [[Official New Zealand Music Chart|NZ]]<br><ref>{{cite web | url=http://charts.org.nz/showinterpret.asp?interpret=Gary+Clark+Jr%2E | title=Gary Clark Jr. discography | publisher=Hung Medien | work=''australian-charts.com'' |accessdate=October 1, 2013}}</ref>
! scope="col" style="width:3em;font-size:90%;"| [[UK Albums Chart|UK]]
|-
|''110''
* Released: 2004
* Label: Hotwire Unlimited
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
|-
|''Worry No More''
* Released: January 10, 2008
* Label: Hotwire Unlimited
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
|-
|''[[Blak and Blu]]''
* Released: October 22, 2012
* Label: [[Warner Bros. Records|Warner Bros.]]
| {{center|6}}
| {{center|34}}
| {{center|93}}
| {{center|37}}
| {{center|6}}
| {{center|6}}
| {{center|44}}
|-
|''[[The Story of Sonny Boy Slim]]''
* Released: September 11, 2015
* Label: [[Warner Bros. Records|Warner Bros.]]
| {{center|8}}
| {{center|18}}
| {{center|126}}
| {{center|74}}
| {{center|10}}
| {{center|13}}
| {{center|40}}
|}

===Live albums===
{| class="wikitable"
|-
! rowspan="2" style= "width:200px;"|Title
! colspan="6" style= "width:20px;"|Peak positions
! rowspan="2" style= "width:200px;"|Notes
|-
! scope="col" style="width:3em;font-size:90%;" | [[Billboard 200|US]]<br><ref name="billboard 200"/>
! scope="col" style="width:3em;font-size:90%;" | [[ARIA Charts|AUS]]<br><ref name="AUS"/>
! scope="col" style="width:3em;font-size:90%;" | [[Ultratop|BEL]]<br><ref name="bel">{{cite web | url=http://ultratop.be/nl/showinterpret.asp?interpret=Gary+Clark+Jr%2E | title=Gary Clark Jr. discography | publisher=Hung Medien | work=''ultratop.be'' |accessdate=October 8, 2014}}</ref>
! scope="col" style="width:3em;font-size:90%;" | [[Syndicat National de l'Édition Phonographique|FRA]]
! scope="col" style="width:3em;font-size:90%;" | [[MegaCharts|NED]]
! scope="col" style="width:3em;font-size:90%;" | [[UK Albums Chart|UK]]<br><ref>{{cite web|url=http://www.zobbel.de/cluk/141004cluk.txt |title=Chart Log UK : 04.10.2014 |publisher=Zobbel.de |accessdate=May 18, 2015}}</ref>
|-
|''Gary Clark Jr. Live''
* Released: September 23, 2014
* Label: Warner Bros.
| {{center|26}}
| {{center|—}}
| {{center|95}}
| {{center|159}}
| {{center|94}}
| {{center|159}}
|Track listing
<small>
# "Catfish Blues"
# "Next Door Neighbor Blues"
# "Travis County"
# "When My Train Pulls In"
# "Don't Owe You a Thang"
# "Three O'Clock Blues"
# "Things Are Changin'"
# "Numb"
# "Ain't Messin 'Round"
# "If Trouble Was Money"
# "Third Stone from the Sun / If You Love Me Like You Say"
# "Please Come Home"
# "Blak and Blu"
# "Bright Lights"
# "When the Sun Goes Down"
</small>
|-
|''Live North America 2016''
* Released: March 17, 2017
* Label: Warner Bros.
| {{center|80}}
| {{center|32}}
| {{center|71}}
| {{center|—}}
| {{center|—}}
| {{center|—}}
|
|}

===EPs===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
! scope="col"| Title and details
! scope="col"| Notes
|-
! scope="row"| ''Gary Clark Jr. EP''
*Type: EP
*Released: 2010
*Label: Hotwire Unlimited
|<small>
# Intro (1:37)
# Bright Lights (5:12)
# Don't Owe You a Thang (3:33)
# Please Come Home (5:04)
# The Life (4:38)
# Things Are Changing (3:49)
# Outro (4:49)
# Breakdown (4:16)
</small>
|-
! scope="row"| ''The Bright Lights EP''
*Type: EP
*Released: November 30, 2010
*Label: Warner Bros.
|<small>
# Bright Lights (5:24)
# Don't Owe You a Thang (3:35)
# Things Are Changin' (Live) [Solo Acoustic] (4:31)
# When My Train Pulls In (Live) [Solo Acoustic] (8:13)
</small>
|-
! scope="row"| ''The Bright Lights EP Australian Tour Edition''
*Type: EP
*Released: 2012
*Label: Warner Bros.
|<small>
# Bright Lights (5:24)
# Don't Owe You a Thang (3:35)
# Things Are Changin' (Live) [Solo Acoustic] (4:31)
# When My Train Pulls In (Live) [Solo Acoustic] (8:13)
# Third Stone from the Sun / If You Love Me Like You Say – (Live in Charlottesville, VA) (12:32)
# Bright Lights (Live in London, UK) (10:55)
</small>
|}

;Others
* 2012 – ''Gary Clark Jr. Presents Hotwire Unlimited Raw Cuts Vol. 1'' – Hotwire Unlimited/Warner Bros. – released April 30, 2012 [U.K. vinyl 45rpm]
<small>
# Side A Third Stone from the Sun / If You Love Me Like You Say (Live in Charlottesville, VA) (12:32)
# Side B Bright Lights (Live in London, England) (10:55)
</small>
* 2013 – ''Gary Clark Jr. Presents Hotwire Unlimited Raw Cuts Vol. 2'' – Warner Bros. – released April 21, 2013 [U.K. vinyl Side A – 33 rpm, Side B – 45rpm]
<small>
# Side A When My Train Pulls In (Live at The DO512 Lounge in Austin, TX) (16:22)
# Side B When My Train Pulls In (Album Version) (7:45)
</small>

===Guest appearances===
* [[The-Dream]] - "Too Early" on [[IV Play (The-Dream album)|IV Play]] (2013)
* [[Talib Kweli]] - "Demonology" (featuring [[Big K.R.I.T.]]) on [[Gravitas (Talib Kweli album)|Gravitas]] (2013)
* [[Tech N9ne]] - "No Gun Control" (featuring [[Krizz Kaliko]]) on [[The Storm (Tech N9ne album)|The Storm]] (2016)

===Mixtapes===
* 2014 – ''[[Blak and Blu#Blak and Blu The Mixtape|Blak and Blu The Mixtape]]'' – presented by [[D-Nice]] – released April 30, 2014

==See also==
* {{portal-inline|Biography}}
* {{portal-inline|Blues}}

==References==
{{Reflist|30em}}

==External links==
{{commonscatinline}}
* {{Official website|http://www.garyclarkjr.com }}
* [http://www.crossroadsguitarfestival.com Crossroadsguitarfestival.com]
* [http://www.listenbeforeyoubuy.net/listen/introducing-gary-clark-jr/ Review]

{{DEFAULTSORT:Clark, Gary Jr.}}
[[Category:1984 births]]
[[Category:Living people]]
[[Category:American blues guitarists]]
[[Category:American male guitarists]]
[[Category:Male actors from Texas]]
[[Category:American male film actors]]
[[Category:American male television actors]]
[[Category:American film directors]]
[[Category:Space rock]]
[[Category:Lead guitarists]]
[[Category:African-American guitarists]]
[[Category:African-American rock musicians]]
[[Category:American blues singers]]
[[Category:Blues rock musicians]]
[[Category:Electric blues musicians]]
[[Category:Texas blues musicians]]
[[Category:Singers from Texas]]
[[Category:American blues singer-songwriters]]
[[Category:Contemporary blues musicians]]
[[Category:Soul-blues musicians]]
[[Category:Neo soul singers]]
[[Category:American male singer-songwriters]]
[[Category:American pop singers]]
[[Category:American rock guitarists]]
[[Category:American singer-songwriters]]
[[Category:American rock singers]]
[[Category:American rock songwriters]]
[[Category:American male songwriters]]
[[Category:American rhythm and blues singers]]
[[Category:American tenors]]
[[Category:Grammy Award winners]]
[[Category:Warner Bros. Records artists]]
[[Category:Film directors from Texas]]
[[Category:Songwriters from Texas]]
[[Category:Guitarists from Texas]]