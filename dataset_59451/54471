

{{Infobox software
| name                   = Orchard
| logo                   = [[File:Orchard logo 1.svg|250px]]
| screenshot             = Orchard dashboard.png
| caption                = Orchard 1.4 [[Dashboard (Web administration)|Dashboard]]
| released               = {{Start-date|January 2011}}
| latest release version = 1.10.1
| latest release date    = {{release date|2016|05|11}}<ref>http://docs.orchardproject.net/Documentation/Orchard-1-10-1.Release-Notes</ref>
| status                 = Active
| programming language   = [[ASP.NET]]
| operating system       = [[Windows]]
| genre                  = [[Content management framework]], [[Content management system]], [[Community]] and [[Blog software]]
| license                = [[New BSD License]]
| website                = {{URL|http://orchardproject.net}}
}}

'''Orchard''' is a [[free and open source software|free, open source]], community-focused [[content management system]] written in [[ASP.NET]] platform using the [[ASP.NET MVC]] framework. Its vision is to create shared components for building ASP.NET applications and extensions, and specific applications that leverage these components to meet the needs of end-users, scripters, and developers. 

Orchard is delivered as part of the ASP.NET Open Source Gallery under the [[.NET Foundation]]. It is licensed under a [[New BSD license]], which is approved by the [[Open Source Initiative]] (OSI). The predecessor of Orchard was [https://oxite.codeplex.com/ Microsoft Oxite].

==Project status==
Orchard is currently in active community-driven development. The project includes an extensibility model for both modules and themes, a dynamic content type system, ease of customization, localization and more.

Although several of primary developers work for [[Microsoft]], it is no longer an officially developed Microsoft product, and is under the auspices of the .NET Foundation.

The project is managed by the [http://orchardproject.net/about Orchard Steering Committee], based on a [http://orchardproject.net/governance published governance model]. Steering Committee members are elected by the community annually.

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.orchardproject.net/}}
* [http://gallery.orchardproject.net/ Orchard Themes and Modules Gallery]
* [http://www.zdnet.com/blog/microsoft/orchard-microsofts-open-source-cms-platform-is-reborn/4506 Orchard: Microsoft's open-source CMS platform is (re)born]
* [http://webmasterformat.com/tools/cms/orchard Why You Should Pay Attention to the Development of Orchard CMS]
* [http://www.orchardcmsboston.org Boston Orchard CMS User Group]

[[Category:Free content management systems]]
[[Category:Web frameworks]]
[[Category:Blog software]]
[[Category:Content management systems]]

{{cms-software-stub}}