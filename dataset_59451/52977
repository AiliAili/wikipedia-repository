{{Infobox journal
| title         = The German Quarterly
| cover         = 
| editor        = Carl Niekerk
| discipline    = [[German studies]]
| former_names  = 
| abbreviation  = Ger. Q.
| publisher     = [[Wiley-Blackwell]] on behalf of the [[American Association of Teachers of German]]
| country       = United States
| frequency     = Quarterly
| history       = 1928-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-1183
| link1         = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-1183/currentissue
| link1-name    = Online access
| link2         = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-1183/issues
| link2-name    = Online archive
| JSTOR         = 00168831
| OCLC          = 1258410
| LCCN          = 
| CODEN         = 
| ISSN          = 0016-8831
| eISSN         = 1756-1183
}}

'''''The German Quarterly''''' is a quarterly [[peer review|peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the [[American Association of Teachers of German]] dedicated to [[German studies]]. The [[editor-in-chief]] is Carl Niekerk ([[University of Illinois at Urbana-Champaign]]). It was established in 1928<ref name="gardt">{{cite book |last=Gardt |first=Andreas |title=Globalization and the Future of German&nbsp;— With a Select Bibliography |year=2004 |publisher=[[Walter de Gruyter#De Gruyter Mouton|Mouton de Gruyter]] ([[Berlin]]; [[New York City]]) |isbn=978-3-11-017918-7 |page=286 |url=https://books.google.com/books?id=r461DfrpIeEC&pg=PA286 |author2=Bernd-Rüdiger Hüppauf  |accessdate=8 January 2011}}</ref> and is published under the auspices of the [[American Association of Teachers of German]]. It has been called "one of the most widely and internationally read American journals in the field of German studies."<ref>{{cite book |last=Zinggeler |first=Margrit Verena |title=Literary Freedom and Social Constraints in the Works of Swiss Writer Gertrud Leutenegger |year=1995 |publisher=[[Rodopi Publishers|Rodopi]] ([[Amsterdam]]; [[Atlanta]]) |isbn=978-90-5183-763-6 |page=34 |url=https://books.google.com/books?id=qNTHKFTYNbwC&pg=PA34 |accessdate=2013-04-06}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Arts & Humanities Citation Index]]
* [[Current Contents]]/Arts & Humanities
* [[Education Index]]/Abstracts
* [[Expanded Academic ASAP]]
* [[International Bibliographies of Periodical Literature]]
* [[InfoTrac]]
* [[Linguistics & Language Behavior Abstracts]]
* [[MLA International Bibliography]]
* [[ProQuest|ProQuest databases]]
* [[RILM Abstracts of Music Literature]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-1183}}

{{DEFAULTSORT:German Quarterly, The}}
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1928]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Germanic philology journals]]