'''Refinancing''' may refer to the replacement of an existing [[Collateralized debt obligation|debt obligation]] with another debt obligation under different terms.  The terms and conditions of refinancing may vary widely by country, province, or state, based on several economic factors such as, [[inherent risk]], projected risk, political stability of a nation, currency stability, banking regulations, borrower's credit worthiness, and credit rating of a nation.  In many industrialized nations, a common form of refinancing is for a place of primary residency [[mortgage loan|mortgage]].

If the replacement of debt occurs under [[financial distress]], refinancing might be referred to as [[debt restructuring]].

A loan (debt) might be refinanced for various reasons:

#To take advantage of a better interest rate (a reduced monthly payment or a reduced term)
#To consolidate other debt(s) into one loan (a potentially longer/shorter term contingent on interest rate differential and fees)
#To reduce the monthly repayment amount (often for a longer term, contingent on interest rate differential and fees)
#To reduce or alter risk (e.g. switching from a variable-rate to a fixed-rate loan)
#To free up cash (often for a longer term, contingent on interest rate differential and fees)

Refinancing for reasons 2, 3, and 5 are usually undertaken by borrowers who are in financial difficulty in order to reduce their monthly repayment obligations, with the penalty that they will take longer to pay off their debt.

In the context of personal (as opposed to corporate) finance, refinancing multiple debts makes management of the debt easier.  If high-interest debt, such as [[credit card]] debt, is consolidated into the home mortgage, the borrower is able to pay off the remaining debt at mortgage rates over a longer period.  

For home mortgages in the United States, there may be tax advantages available with refinancing, particularly if one does not pay [[Alternative Minimum Tax]].

== Risks ==
Some fixed-term loans have penalty clauses ("call provisions") that are triggered by an early repayment of the loan, in part or in full, as well as "closing" fees.  There will also be transaction fees on the refinancing.  These fees must be calculated before embarking on a loan refinancing, as they can wipe out any savings generated through refinancing. Penalty clauses are only applicable to loans paid off prior to maturity. If a loan is paid off upon maturity it is a new financing, not a refinancing, and all terms of the prior obligation terminate when the new financing funds pay off the prior debt.

If the refinanced loan has the same interest rate as previously, but a longer term, it will result in a larger total interest [[cost]] over the life of the loan, and will result in the borrower remaining in debt for many more years.  Typically, a refinanced loan will have a lower interest rate.  This lower rate, combined with the new, longer term remaining on the loan will lower payments.  

A borrower should calculate the total cost of a new loan compared to the existing loan.  The new loan cost will include the closing costs, prepayment penalties (if any) and the interest paid over the life of the new loan.  This should be lower than the remaining interest that will be paid on the existing loan to see if it makes financial sense to refinance.

In some jurisdictions, varying by American state, refinanced mortgage loans are considered [[recourse debt]], meaning that the borrower is liable in case of default, while un-refinanced mortgages are [[non-recourse debt]].

== Points ==
{{main|Point (mortgage)}}
Refinancing lenders often require a percentage of the total loan amount as an upfront payment.  Typically, this amount is expressed in "points" (or "premiums"). 1 point = 1% of the total loan amount.  More points (i.e. a larger upfront payment) will usually result in a lower interest rate.  Some lenders will offer to finance parts of the loan themselves, thus generating so-called "negative points" (i.e. discounts).

==Types (US loans only)==
=== No Closing Cost ===

Borrowers with this type of refinancing typically pay few if any upfront fees to get the new mortgage loan. This type of refinance can be beneficial provided the prevailing market rate is lower than the borrower's existing rate by a formula determined by the lender offering the loan. Before you read any further do not provide any lender with a credit card number until they have provided you with a [[Good faith estimate|Good Faith Estimate]] verifying it is truly a 0 cost loan. The appraisal fee cannot be paid for by the lender or broker so this will always show up in the total settlement charges at the bottom of your [[Good faith estimate|GFE.]]

This can be an excellent choice in a declining market or if you are not sure you will hold the loan long enough to recoup the closing cost before you refinance or pay it off. For example, you plan on selling your home in three years, but it will take five years to recoup the closing cost. This could prevent you from considering a refinance, however if you take the zero closing cost option, you can lower your interest rate without taking any risk of losing money.

In this case the broker receives a credit or what's called [[yield spread premium]] (YSP).  Yield spread premiums are the cash that a mortgage company receives for originating your loan. The broker provides the client and the documentation needed to process the loan and the lender pays them for providing this service in lieu of paying one of their own loan officers. Since a brokerage can have more than one loan officer originating loans, they can sometimes receive additional YSP for bringing in a volume amount of loans. This is normally based on funding more than 1 million in total loans per month. This can greatly benefit the borrower, especially since April 1, 2011. New laws have been implemented by the federal government mandating that all brokers have set pricing with the lenders they do business with. Brokers can receive so much YSP that they can provide you with a lower rate than if you went directly to the lender and they can pay for all your closing cost as opposed to the lender who would make you pay for all the third party fees on your own. You end up with a lower rate and lower fees. Since the new [http://portal.hud.gov/hudportal/HUD?src=/program_offices/housing/rmra/res/respa_hm RESPA] law as of April came into effect in 2011, brokers can no longer decide how much they want to make off of the loan. Instead they sign a contract in April stating that they will keep only a certain percentage of the YSP and the rest will go toward the borrowers closing cost.

True No Closing Cost mortgages are usually not the best options for people who know that they will keep that loan for the entire length of the term or at least enough time to recoup the closing cost. When the borrower pays out of pocket for their closing costs, they are at a higher risk of losing the money they invested. In most cases, the borrower is not able to negotiate the fees for the appraisal or [[escrow]]. Sometimes, when wrapping closing costs into a loan you can easily determine whether it makes sense to go with the lower rate with closing cost or the slightly higher rate for free. Some cases your payment will be the same, in that case you would want to choose the higher rate with no fees. If the payment for 4.5% with $2,500 in settlement charges is the same for 4.625% for free then you will pay the same amount of money over the length of the loan, however if you choose the loan with closing cost and you refinance before the end of your term you wasted money on the closing cost. Your loan amount will be 2,500 less at 4.625% and your payment is the same.

=== No Appraisal Required ===

The [[Obama Administration]] authorized several refinance programs aimed at helping underwater homeowners take advantage of the historically low interest rates. Most of these programs do not require an appraisal, and encompass all loan types.  The programs offered in 2013 include:
# '''FHA Streamline Refinance''':  The largest group that benefits from this refinance program will be those who have a FHA loan that was endorsed prior to May 31, 2009.  For those who meet this date, the FHA PMI rates are very very low. This Streamline Refinance Program without an appraisal is also available to borrowers who no longer live in the property (as their primary residence)/ own the house as Investment Property.
# '''VA Loan Refinance''': The Veteran's Administration offers Interest Rate Reduction Refinances IRRR for Veteran Home Owners who simply want to reduce their interest rate, with no appraisal.  These loans are also available to qualifying Veteran's who no longer live in the property as their primary residence.
# '''HARP Refinance:''' When the Home Affordable Refinance Program (HARP) was launched in 2009, it sought to help homeowners with underwater mortgages refinance their loans into lower monthly payments and /or interest rates.  Unfortunately, the first version of the program failed to help as many homeowners with underwater mortgages as was hoped, leading to the release of a new and improved version of HARP, dubbed HARP 2, to deal with the complications. HARP 2 no longer caps the loan-to value at 125%, and allows any loan-to-value acceptable, thereby covering underwater homes.<ref>{{cite web|title=HARP 2.0 rules, and who will benefit|url=http://www.marketwatch.com/story/harp-20-rules-and-who-will-benefit-2011-11-18|publisher=[[MarketWatch]]|date=Nov 18, 2011|accessdate=30 May 2015}}</ref> 
#'''USDA Home Loans''':  No appraisal required – the current residence must be in a USDA “Footprint Area” and currently be insured under the USDA program.  So refinancing from a Conventional loan or a FHA loan to USDA will not work under this program. No Credit Report Required – the current mortgage must be current, and all of the previous 12 months of mortgage payments need to be made on time.  That’s all.  We just verify that you made your house payments on time. Employment Verification Required – we will need to verify that you are employed, and drawing enough money to meet the underwriting guidelines… meaning we must prove that you have enough income to make your house payments. Can not take cash out - All you can do is finance your current loan balance, and the new Guarantee Fee (USDA PMI) which is 1.5%.

=== Cash-Out ===
{{main|Cash out refinancing}}
This type of refinance may not help lower the monthly payment or shorten mortgage periods. It can be used for home improvement, credit cards, and other [[debt consolidation]] if the borrower qualifies with their current home equity; they can refinance with a loan amount larger than their current mortgage and keep the cash out.

In situations where the borrower has both a first and second mortgage, it is common to consolidate these loans as part of the refinance process. However, even if the borrower does not receive any net "cash out" as part of the transaction, in some cases lenders will consider this a cash-out transaction because of the "12-month rule".  This rule states that any refinance that occurs within 12 months of a second mortgage (that was not part of the original purchase transaction) is considered a cash-out refinance.
<ref>{{cite web|title=Why Is This Mortgage Refinance "Cash-Out"?|url=http://www.mtgprofessor.com/A%20-%20Refinance/why_is_this_refinance_cash-out.htm|website=http://www.mtgprofessor.com}}</ref>

== See also ==
*[[Refinancing risk]]
*[[Streamline refinancing]]
*[[Refunding]]
*[[Refinancing burnout]]

== References ==
{{reflist}}

== External links ==
*[http://www.hud.gov/offices/hsg/sfh/buying/streamli.cfm U.S. Department of Housing and Urban Development] - Streamlining your Mortgage
*[http://www.homeloans.va.gov/IRRRL.htm U.S. Department of Veteran's Affairs] - Mortgage Refinancing Information
*[https://portal.hud.gov/hudportal/HUD?src=/program_offices/housing/sfh/reo/goodn/gnndabot Good Neighbor Next Door] - FHA Good Neighbor Next Door
*[http://www.hud.gov/offices/pih/ih/homeownership/184/ Indian Home Loan Guarantee Program] - Indian Home Loan Guarantee Program
*[http://portal.hud.gov/portal/page/portal/HUD HUD] - Information about HUD
{{finance}}
[[Category:Personal finance]]
[[Category:Mortgage]]