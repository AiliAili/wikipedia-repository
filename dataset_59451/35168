{{good article}}
{{Infobox football match
|                   title = 1996 Football League Third Division Play-off Final
|                   image = [[Image:Old Wembley Stadium (external view).jpg|200px|alt=The main entrance of a football stadium.]]
|                   event = 
|                   team1 = [[Plymouth Argyle F.C.|Plymouth Argyle]]
|        team1association = 
|              team1score = 1
|                   team2 = [[Darlington F.C.|Darlington]]
|        team2association = 
|              team2score = 0
|                 details = 
|                    date = 25 May 1996
|                 stadium = [[Wembley Stadium (1923)|Wembley Stadium]]
|                    city = [[London]]
|      man_of_the_match1a = [[Matty Appleby]]
| man_of_the_match1atitle = 
|      man_of_the_match1b = 
| man_of_the_match1btitle = 
|                 referee = [[William Burns (referee)|William Burns]]<br> ([[Scarborough, North Yorkshire|North Yorkshire]])
|              attendance = 43,431<ref name="Match Report">[http://www.independent.co.uk/sport/pilgrims-progress-1349280.html "Pilgrims progress"] [[The Independent]]. 26 May 1996. Retrieved 9 March 2010.</ref>
|                 weather = 
|                previous = [[1995 Football League Third Division play-off Final|1995]]
|                    next = [[1997 Football League Third Division play-off Final|1997]]
}}

The '''1996 Football League Third Division play-off final''' was a [[Association football|football]] match played at [[Wembley Stadium (1923)|Wembley Stadium]] on 25 May 1996, to determine the fourth and final team to gain [[Promotion and relegation|promotion]] from the [[Football League Third Division|Third Division]] to the [[Football League Second Division|Second Division]] of the [[Football League]] in the [[1995–96 in English football|1995&ndash;96 season]].

It was contested by [[Plymouth Argyle F.C.|Plymouth Argyle]], who finished fourth in the Third Division table, and [[Darlington F.C.|Darlington]], who finished fifth. The teams reached the final by defeating [[Colchester United F.C.|Colchester United]] and [[Hereford United F.C.|Hereford United]] respectively in the two-legged semi-finals.

Plymouth Argyle won the match 1&ndash;0 thanks to a headed goal from [[Ronnie Mauge]] to gain promotion back to the third tier of [[Football in England|English football]] one season after being relegated. For the club's manager, [[Neil Warnock]], it was his fourth success in the play-offs as a manager, having achieved it twice with [[Notts County F.C.|Notts County]] and once with [[Huddersfield Town F.C.|Huddersfield Town]]. His counterpart, [[Jim Platt]], would leave full-time management at the end of that year.

==Route to the final==
{|align="right" style="border: 1px solid #000000; background-color: #ffffff" cellspacing="1" cellpadding="1" width="300"              
|+'''Football League Third Division<br> final table, leading positions'''
|- align="left" bgcolor="#efefef"
!Pos
!Team
!P
!W
!D
!L
!F
!A
!Pts
|- bgcolor="#ccffcc" 
|1.
|[[Preston North End F.C.|Preston North End]]
|46
|23
|17
|6
|78
|38
|86
|- bgcolor="#ccffcc"
|2.
|[[Gillingham F.C.|Gillingham]]
|46
|22
|17
|7
|49
|20
|83
|- bgcolor="#ccffcc"
|3.
|[[Bury F.C.|Bury]]
|46
|22
|13
|11
|66
|48
|79
|- bgcolor="#ccccff"
|4.
|[[Plymouth Argyle F.C.|Plymouth Argyle]]
|46
|22
|12
|12
|68
|49
|78
|- bgcolor="#ccccff"
|5.
|[[Darlington F.C.|Darlington]]
|46
|20
|18
|8
|60
|42
|78
|- bgcolor="#ccccff"
|6.
|[[Hereford United F.C.|Hereford United]]
|46
|20
|14
|12
|65
|47
|74
|- bgcolor="#ccccff"
|7.
|[[Colchester United F.C.|Colchester United]]
|46
|18
|18
|10
|61
|51
|72
|-
|}
Plymouth Argyle had finished the [[1995–96 Football League|1995&ndash;96 Football League season]] in fourth place in the [[1995–96 Football League#Third Division|Third Division]], one place ahead of Darlington. Therefore, both missed out on the three automatic [[Promotion and relegation#English example|promotion]] places and instead took part in the [[Football League play-offs|play-offs]] to determine who would join [[Preston North End F.C.|Preston North End]], [[Gillingham F.C.|Gillingham]], and [[Bury F.C.|Bury]] as the fourth promoted team.<ref>[http://www.soccerbase.com/league2.sd?competitionid=4&seasonid=125 "Final 1995&ndash;96 Football League Two Table"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> On the final day of the league season, Plymouth Argyle had the opportunity to finish third in the table and thereby clinch the final automatic promotion place but a 3&ndash;0 win at Home Park against [[Hartlepool United F.C.|Hartlepool United]] was not enough to overtake Bury,<ref>[http://www.soccerbase.com/results3.sd?gameid=226352 "Plymouth Argyle 3&ndash;0 Hartlepool United"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> after they also won 3&ndash;0 at home to [[Cardiff City F.C.|Cardiff City]].<ref>[http://www.soccerbase.com/results3.sd?gameid=226345 "Bury 3&ndash;0 Cardiff City"] [[Soccerbase]]. Retrieved 9 March 2010.</ref>

In the play-off semi-finals, Darlington were paired with sixth-placed [[Hereford United F.C.|Hereford United]] and Plymouth Argyle with seventh-place finishers [[Colchester United F.C.|Colchester United]]. Darlington won their first leg tie 2&ndash;1 at Edgar Street courtesy of goals from [[Robbie Blake]] and [[Sean Gregan]].<ref>[http://www.independent.co.uk/sport/darlington-stage-fightback-1347015.html "Darlington stage fightback"] [[The Independent]]. 13 May 1996. Retrieved 9 March 2010.</ref> They also won the second leg by the same scoreline at Feethams with [[Matty Appleby]] and [[Robbie Painter]] on the scoresheet.<ref name="Darlo">[http://www.independent.co.uk/sport/hereford-fall-to-painter-1347644.html "Hereford fall to Painter"] [[The Independent]]. 16 May 1996. Retrieved 9 March 2010.</ref>

Plymouth Argyle lost the first leg of their semi-final tie 1&ndash;0 at Layer Road after a goal from [[Mark Kinsella]],<ref>[http://www.independent.co.uk/sport/kinsella-lifts-colchester-1347014.html "Kinsella lifts Colchester"] [[The Independent]]. 13 May 1996. Retrieved 9 March 2010.</ref> but they responded in the second leg at Home Park. Goals from [[Mickey Evans (Irish footballer)|Michael Evans]], [[Chris Leadbitter]] and [[Paul Williams (footballer born 1969)|Paul Williams]] were enough to secure a 3&ndash;1 win and a 3&ndash;2 victory on aggregate.<ref name="Argyle">[http://www.independent.co.uk/sport/plymouth-land-knockout-blow-1347645.html "Plymouth land knockout blow"] [[The Independent]]. 16 May 1996. Retrieved 9 March 2010.</ref> The results set up a first visit to Wembley Stadium for the supporters of both clubs.<ref name="Darlo"/><ref name="Argyle"/>

{| width="100%" bgcolor=white
|- valign=top  bgcolor=#99ccff
!colspan=3 style="width:1*"|'''Plymouth Argyle'''
!
!colspan=3 style="width:1*"|'''Darlington'''
|- valign=top align="center" bgcolor=#c1e0ff
|Opponent
|Result
|Legs
|Round
|Opponent
|Result
|Legs
|- align="center"
|[[Colchester United F.C.|Colchester United]]
|3&ndash;2
|0&ndash;1 away; 3&ndash;1 home
|Semi-finals
|[[Hereford United F.C.|Hereford United]]
|4&ndash;2
|2&ndash;1 away; 2&ndash;1 home
|}

==Match summary==
[[Image:Gillsplayoff2000.jpg|thumb|right|alt=A full football stadium on a clear, bright day|The first play-off final of the 1995&ndash;96 season was bathed in sunshine.]]
The two teams were competing for [[Promotion and relegation|promotion]] to the third tier of the [[English football league system]],<ref>[http://www.independent.co.uk/sport/platt-keeps-the-faith-1348152.html?cmp=ilc-n "Platt keeps the faith"] [[The Independent]]. 19 May 1996. Retrieved 10 March 2010.</ref> at the time called the [[Football League Second Division|Second Division]], a familiar place for both sides. In its previous 68 seasons as a [[Football League]] club, Plymouth Argyle competed exclusively in the second and third tiers, exactly 34 seasons each.<ref>[http://www.independent.co.uk/sport/football-how-consistency-and-caution-made-arsenal-englands-greatest-team-of-the-20th-century-1133020.html "Greatest team of the 20th century"] [[The Independent]]. 17 December 1999. Retrieved 9 March 2010.</ref> While Darlington had featured in the second tier just twice, competing in the third tier on 32 occasions and the fourth tier another 32 times. The official attendance of 43,431 was a record for a play-off final at that level, beating the previous record set in 1994,<ref>[http://www.soccerbase.com/results3.sd?gameid=389870 "Wycombe Wanderers 4&ndash;2 Preston North End"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> until it was bettered a year later by 3,373 spectators.<ref>[http://www.soccerbase.com/results3.sd?gameid=389878 "Northampton Town 1&ndash;0 Swansea City"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> There was also a significant disparity in the number of tickets sold to the supporters of the two clubs, with fewer than 10,000 Darlington fans in attendance compared to 35,000 fans representing Plymouth Argyle.<ref>[http://www.wsc.co.uk/content/view/3040/29/ "Plymouth Argyle"] [[When Saturday Comes]]. Retrieved 9 March 2010.</ref><ref>[http://www.independent.co.uk/sport/platt-fired-up-for-wembley-1349123.html?cmp=ilc-n "Platt fired up for Wembley"] [[The Independent]]. 25 May 1996. Retrieved 10 March 2010.</ref>

Plymouth Argyle manager [[Neil Warnock]] picked ten players who had started both of the club's semi-final matches, with [[Ronnie Mauge]] keeping his place in the team having replaced [[Chris Billy]] for the second leg.<ref>[http://www.greensonscreen.co.uk/gosdb-season.asp?years=1995-1996 "Plymouth Argyle: 1995&ndash;96"] Greens on Screen. Retrieved 9 March 2010.</ref> The match was to be [[Goalkeeper (association football)|goalkeeper]] [[Steve Cherry]]'s last for the club, having returned for a second stint with the club three months earlier.<ref>[http://www.greensonscreen.co.uk/gosdb-players2.asp?pid=157&scp=1,2,3,4,5,6,7 "Steve Cherry"] Greens on Screen. Retrieved 9 March 2010.</ref> Darlington manager [[Jim Platt]], who was taking charge of the team for the last time before the return of [[David Hodgson (footballer)|David Hodgson]],<ref>[http://www.soccerbase.com/manager_history.sd?teamid=719 "Darlington Managers"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> made one change to the side that secured progress from the semi-final stage with [[Tony Carss]] coming in at the expense of [[Matt Carmichael (footballer)|Matt Carmichael]]. The final would prove to be [[Man of the match|Man of the Match]] [[Matty Appleby]]'s last for Darlington.<ref>[http://www.soccerbase.com/players_details.sd?playerid=210 "Matty Appleby"] [[Soccerbase]]. Retrieved 9 March 2010.</ref>

===First half===
[[File:Sean Gregan.jpg|thumb|left|150px|alt=A man, wearing a sports training kit|[[Sean Gregan]] was a key player for Darlington.]]
Darlington were the first to settle with [[Gary Bannister]] a prominent player in midfield, but Plymouth Argyle eventually found their rhythm and had the first real goalscoring opportunity after ten minutes.<ref name="Match Report"/> [[Adrian Littlejohn]] found space after a [[Push and run|one-two]] with [[Mickey Evans (Irish footballer)|Michael Evans]], but his first touch let him down and the opportunity was not taken. Darlington's main threat was coming from attacking [[Defender (association football)|full back]] Appleby and he had their best chance of the match midway through the first half. He carved out the initial chance, having carried the ball half the length of the pitch, but team-mate Steven Gaughan was unable to convert. The hasty clearance from Argyle found its way back to Appleby but he sent his shot over the crossbar with goalkeeper Cherry completely exposed.<ref name="Jim'll fix it">[http://www.thefreelibrary.com/JIM%27LL+FIX+IT%3b+Sad+Platt+sounds+a+play-off+pledge%3b+Darlington+0%2c...-a061161353 "Jim'll fix it"] [[The Sunday Mirror]]. 26 May 1996. Retrieved 10 March 2010.</ref>

Plymouth Argyle came close to making their opponents pay for their profligacy in front of goal with Evans lifting a volley over the crossbar.<ref name="Match Report"/> Darlington's captain [[Andy Crosby]] was proving to be a formidable figure at the heart of his team's defence, but Argyle fashioned another chance to open the scoring just before half-time. A flick on by Evans presented Adrian Littlejohn with the opportunity to redeem his earlier miss, but he dragged his shot wide.<ref name="Mauge">[http://www.thefreelibrary.com/Mauge+magic%3b+Ronnie+strikes+as+Pilgrims+progress%3b+Darlington+0%2c...-a061161366 "Mauge magic"] [[The Sunday Mirror]]. 26 May 1996. Retrieved 10 March 2010.</ref>

===Second half===
There were few clear-cut chances at the start of the second half, but Plymouth Argyle were winning the midfield battle with Mauge and [[Chris Leadbitter]] leading by example with a number of forceful tackles.<ref name="Match Report"/>  The pivotal moment arrived on 65 minutes after [[Martin Barlow]] had earnt a corner-kick on the right-hand side. Leadbitter played the ball short to full back [[Mark Patterson (footballer born 1968)|Mark Patterson]], whose well-measured cross was met firmly by the unmarked Mauge to head into the back of the net.<ref name="Match Report"/> Darlington tried to force their way back into the match, but were being thwarted by Plymouth Argyle captain [[Michael Heathcote]] and his defensive colleagues which left strikers [[Robbie Blake]] and [[Robbie Painter]],<ref>[http://archive.thenorthernecho.co.uk/2004/1/31/63213.html "Can we afford to lose 120 years of history?"] [[The Northern Echo]]. 31 January 2004. Retrieved 10 March 2010.</ref> who both scored in the semi-finals, with little to work on.<ref name="Mauge"/> As Darlington committed more players forward in search of an equaliser they left themselves exposed in defence which gave the leading side more space to launch counter-attacks. Evans and Littlejohn threatened to score the decisive second goal, but in the end Mauge's headed finish midway through the second half proved to be enough to claim the final promotion place for the team from [[Devon]].<ref name="Match Report"/> The match was by no means a classic, with serious goalscoring chances at a premium, but to the winners it didn't matter.<ref>{{cite book | title= Plymouth Argyle: A Complete Record | publisher= Breedon Books| first= Ryan | last= Danes | year= 2009| location= Plymouth | pages=73–74}}</ref>

===Post-match===
After the final whistle Plymouth Argyle's captain Michael Heathcote received the winners' trophy before parading it in front of the club's supporters on the pitch. For the club's manager, Neil Warnock,<ref>[http://www.greensonscreen.co.uk/managers.asp?name=NeilWarnock "Neil Warnock"] Greens on Screen. Retrieved 9 March 2010.</ref> it was his fourth play-off success at Wembley Stadium. He commented on his past experiences that "It can't be a hindrance, having done it before, but it doesn't make it any less tense. It makes it a very long season. I had booked a holiday starting today - I suppose I should have known better".<ref>[http://www.independent.co.uk/sport/devon-cream-rises-to-the-top-1349366.html "Devon cream rises to the top"] [[The Independent]]. 27 May 1996. Retrieved 9 March 2010.</ref>

Warnock was also full of praise for his counterpart, Jim Platt, commenting that "He should be made manager of the year for what he's done at that club". Citing Darlington's financial worries "Everyone thought they would blow up, but they didn't - they got within an ace. Unfortunately, someone has to lose".<ref name="Jim'll fix it"/> Platt, a former [[Northern Ireland national football team|Northern Ireland]] international, was equally optimistic. He said "My side is very young - nearly all of them in their early 20s. I think we will be here again next season or go up automatically - if we can keep the side together".<ref name="Jim'll fix it"/>

As a result of their victory, Plymouth Argyle returned to the third tier of English football just one year after being relegated to the fourth tier for the first time in its history. They returned to the Third Division two years later before being promoted as [[2001–02 Football League#Third Division|champions]] in 2002,<ref>{{cite book | title= Plymouth Argyle: A Complete Record | publisher= Breedon Books| first= Ryan | last= Danes | year= 2009| location= Plymouth | pages=340–341}}</ref> and the club followed that up by winning the [[2003–04 Football League#Football League Second Division|Second Division]] in 2004,<ref>{{cite book | title= Plymouth Argyle: A Complete Record | publisher= Breedon Books| first= Ryan | last= Danes | year= 2009| location= Plymouth | pages=344–345}}</ref> to reclaim their place in the second tier of English football after a twelve-year hiatus.<ref>[http://www.fchd.info/PLYMOUTA.HTM "Plymouth Argyle"] Football Club History Database. Retrieved 9 March 2010.</ref> For Darlington, there was to be more disappointment in the [[Football League Two play-offs#2000|play-offs]] four years later under [[David Hodgson (footballer)|David Hodgson]], Platt's successor.<ref>[http://www.soccerbase.com/managers2.sd?managerid=705 "Jim Platt"] [[Soccerbase]]. Retrieved 9 March 2010.</ref> They reached the Third Division final in 2000, the last to be played at the original Wembley Stadium, and were defeated 1&ndash;0 again; on this occasion to [[Peterborough United F.C.|Peterborough United]].<ref>[http://www.fchd.info/DARLINGT.HTM "Darlington"] Football Club History Database. Retrieved 9 March 2010.</ref>

==Match details==
{{football box
|date       = 25 May 1996
|time       = 15:00 [[Western European Summer Time|BST]]
|team1      = [[Plymouth Argyle F.C.|Plymouth Argyle]]
|score      = 1 – 0
|team2      = [[Darlington F.C.|Darlington]]
|report     =[http://www.independent.co.uk/sport/pilgrims-progress-1349280.html Report]
|goals1     = [[Ronnie Mauge|Mauge]] {{goal|65}}
|goals2     =
|stadium    = [[Wembley Stadium (1923)|Wembley Stadium]], [[London]]
|attendance = 43,431
|referee    = [[William Burns (referee)|William Burns]] ([[North Yorkshire]])
}}

{| width=92%
|-
|{{Football kit
 | pattern_la = 
 | pattern_b  = 
 | pattern_ra = 
 | leftarm    = 008000
 | body       = 008000
 | rightarm   = 008000
 | shorts     = FFFFFF
 | socks      = 008000
 | title      = Plymouth Argyle
}}
|{{Football kit
 | align      = right
 | pattern_la = 
 | pattern_b  = 
 | pattern_ra = 
 | leftarm    = 000000
 | body       = FFFFFF
 | rightarm   = 000000
 | shorts     = 000000
 | socks      = FFFFFF
 | title      = Darlington
}}
|}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|colspan="4"|'''PLYMOUTH ARGYLE:'''
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[Steve Cherry]]
|-
|WB ||'''2''' ||{{flagicon|ENG}} [[Mark Patterson (footballer born 1968)|Mark Patterson]] 
|-
|CB ||'''4''' ||{{flagicon|ENG}} [[Chris Curran (footballer born September 1971)|Chris Curran]]
|-
|CB ||'''5''' ||{{flagicon|ENG}} [[Mick Heathcote]] ([[Captain (association football)|c]])
|-
|CB ||'''6''' ||{{flagicon|ENG}} [[Richard Logan (footballer born 1969)|Richard Logan]]
|-
|WB ||'''3''' ||{{flagicon|ENG}} [[Paul Williams (footballer born 1969)|Paul Williams]]
|-
|CM ||'''7''' ||{{flagicon|ENG}} [[Martin Barlow]]
|-
|CM ||'''8''' ||{{flagicon|TRI}} [[Ronnie Mauge]] ||{{yel|}} ||
|-
|CM ||'''11'''||{{flagicon|ENG}} [[Chris Leadbitter]]
|-
|CF ||'''9''' ||{{flagicon|ENG}} [[Adrian Littlejohn]]
|-
|CF ||'''10'''||{{flagicon|IRL}} [[Mickey Evans (Irish footballer)|Michael Evans]]
|-
|colspan=3|'''Substitutes:'''
|-
|MF ||'''13'''||{{flagicon|ENG}} [[Chris Billy]]
|-
|FW ||'''14'''||{{flagicon|ENG}} [[Ian Baird]]
|-
|FW ||'''15'''||{{flagicon|CAN}} [[Carlo Corazzin]]
|-
|colspan=4|'''Manager:'''
|-
|colspan=4|{{flagicon|ENG}} [[Neil Warnock]]
|}
|valign="top"|[[File:1996 Football League Third Division play-off Final.svg|300px]]
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align="center"
|colspan="4"|'''DARLINGTON:'''
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[Paul Newell]]
|-
|WB ||'''2''' ||{{flagicon|ENG}} [[Matty Appleby]] ||{{yel|}} ||
|-
|CB ||'''4''' ||{{flagicon|ENG}} [[Andy Crosby]] ([[Captain (association football)|c]])
|-
|CB ||'''5''' ||{{flagicon|ENG}} [[Sean Gregan]]
|-
|CB ||'''6''' ||{{flagicon|ENG}} [[Phil Brumwell]]
|-
|WB ||'''3''' ||{{flagicon|ENG}} [[Mark Barnard (footballer)|Mark Barnard]]
|-
|CM ||'''7''' ||{{flagicon|ENG}} [[Steven Gaughan]] || ||{{suboff|85}}
|-
|CM ||'''8''' ||{{flagicon|ENG}} [[Gary Bannister]]
|-
|CM ||'''11'''||{{flagicon|ENG}} [[Tony Carss]]
|-
|FW ||'''9''' ||{{flagicon|ENG}} [[Robbie Painter]]
|-
|FW ||'''10'''||{{flagicon|ENG}} [[Robbie Blake]]
|-
|colspan=4|'''Substitutes:'''
|-
|DF ||'''13'''||{{flagicon|ENG}} [[Paul Mattison]]
|-
|MF ||'''14'''||{{flagicon|ENG}} [[Gary Twynham]]
|-
|FW ||'''15'''||{{flagicon|ENG}} [[Matt Carmichael (footballer)|Matt Carmichael]] || || {{subon|85}}
|-
|colspan=4|'''Manager:'''
|-
|colspan="4"|{{flagicon|NIR}} [[Jim Platt]]
|}
|}
{| width=100% style="font-size: 90%"
| width=50% valign=top|
'''MATCH RULES'''
*90 minutes.
*30 minutes of extra-time if necessary.
*Penalty shootout if scores still level.
*3 named substitutes.
*Maximum of 3 substitutions.
|}

==See also==
*[[1996 Football League First Division play-off Final]]
*[[1996 Football League Second Division play-off Final]]

==References==
{{reflist|2}}

==External links==
*[http://www.football-league.co.uk Official Football League website]

{{Football League Two play-off Finals}}
{{1995–96 in English football}}
{{Darlington F.C.}}
{{Plymouth Argyle F.C.}}

[[Category:1995–96 Football League Third Division]]
[[Category:Football League Third Division play-off finals]]
[[Category:Plymouth Argyle F.C. matches|1996]]
[[Category:Darlington F.C. matches|1996]]
[[Category:1996 Football League play-offs|3]]