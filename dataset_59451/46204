{{Orphan|date=October 2015}}

'''Isobel Mary "Pixie" Annat''' (born 1930) is a former Australian hospital [[matron]], administrator and office bearer of the Royal Australian Nursing Federation (RANF).

== Early life ==

Annet was born in [[Palmwoods, Queensland]] on 18 March 1930, to James Whammond Annat and Madoline Annat (née Chambers). She attended Palmwoods State School and [[Nambour State High School]] in [[Nambour, Queensland]].

== Nursing career ==

Annat commenced training as a nurse at [[Royal Brisbane and Women's Hospital|Brisbane General Hospital]] in 1948. As a student nurse, she was active in the Student Nurses Unit (SNU) at the hospital and was president of the SNU from 1951 to 1952. In 1953 she completed her [[midwifery]] training at the Maroochy District Hospital in Nambour.<ref>{{cite book|last1=Clur|first1=Colleen|title=Pixie Annat - Chamption of Nurses|date=2015|publisher=University of Queensland Press|isbn=978 0 70225372 0|pages=8–36}}</ref>

In June 1954, Annat and her twin sister, Ellen Madoline, travelled by ship to London, where they lived and worked until early 1956. On her return to Australia, Annat resumed her career as a nurse and was appointed Charge Sister of the Royal Brisbane Intermediate Wards for private patients. She had rejoined the RANF Queensland on returning to Australia and was a very active member of the Federation. In 1962 Annat accepted the position of assistant secretary and later secretary of the RANF Queensland, where she was employed from 1962 to 1965.<ref>{{cite book|last1=Clur|first1=Colleen|title=Pixie Annat - Chamption of Nurses|date=2015|publisher=University of Queensland Press|isbn=978 0 70225372 0 |pages=42–51}}</ref> In 1977, Annat was awarded an [[Order of the British Empire|MBE]] in the Queen's Birthday Honours List.<ref>{{cite web|title=Australian Women and Imperial Honours: 1901-1989|url=http://www.womenaustralia.info/exhib/honours/mbec.html|website=Faith, Hope, Charity|accessdate=18 May 2015}}</ref>

Annat was St Andrew’s War Memorial Hospital's longest-serving executive. Appointed Matron, later called Nursing Director, in 1965, she was Chief Executive Officer from 1978 to 1992.<ref>{{cite web|title=New biography charts career of remarkable Queensland nurse and hospital leader|url=http://standrewshospital.com.au/about-us/news/2015/03/18/pixieannat|website=St Andrew's War Memorial Hospital|accessdate=18 May 2015}}</ref> During her time at St Andrew’s, Annat worked closely with the Hospital’s Governing Board to raise money to advance the hospital’s services. She helped launch one of the first [[neurosurgery]] units in 1979, the first [[nuclear medicine]] department at an Australian private hospital in 1983, and other improvements to medical and surgical practices. Working with [[cardiac]] specialists, Annat helped establish the St Andrew’s Cardiac Investigation Unit and [[Cath lab|Catheter Laboratory]] which opened in 1984. The following year St Andrew’s became the first private hospital in Queensland to perform [[open heart surgery]].<ref>{{cite book|last1=Clur|first1=Colleen|title=Pixie Annat - Chamption of Nurses|date=2015|publisher=University of Queensland Press|isbn=978 0 70225372 0|pages=52–88}}</ref>

In 1992 Annat was awarded a [[Order of Australia|Medal of the Order of Australia]] (OAM) in the [[Queen's Birthday Honours List]].<ref>{{cite web|title=Honours|url=https://www.gg.gov.au/sites/default/files/files/honours/QB92.pdf|website=Governor General of the Commonwealth of Australia|accessdate=18 May 2015}}</ref>

== Retirement ==
After her retirement in 1992, Annat turned to voluntary service. She had been a member of the [[AHS Centaur|Centaur Memorial Fund for Nurses]] since 1975 and in 2000 she was appointed honorary secretary.<ref>{{cite web|title=Fund Administration|url=http://www.centaurnursesfund.org.au/index.php?option=com_content&view=article&id=12:fund-administration&catid=1:objectives&Itemid=14|website=The Centaur Memorial Fund for Nuses|accessdate=18 May 2015}}</ref>

From 2002 to 2014, Annat was president of the Lady Musgrave Trust, and she continues to serve on the Lady Musgrave Trust Board of Management.<ref>{{cite web|title=Board|url=http://www.ladymusgravetrust.org.au/board|website=The Lady Musgrave Trust|accessdate=18 May 2015}}</ref> She served on the Board of St Luke's Nursing Service from 1992 to 2005, was a Director of TriCare Ltd from 1994 to 2001, chaired the St Luke's Ethics Committee from 1995-2005 and since 2005 has been chair of the Spiritus Ethics Committee.<ref>{{cite web|title=Anglicare Southern Queensland|url=http://www.anglicaresq.org.au/images/pdf/CSC0968_Anglicare_SQ_Annual_Report_2012-web_3.pdf|accessdate=18 May 2015}}</ref>

In 1998 she became honorary secretary of the Royal Brisbane Hospitals Nurses' Association Inc, a position she still holds.<ref>{{cite web|title=Office of the Governor, Queensland|url=http://wensley.govhouse.qld.gov.au/the_governor/131002_sorensen_spch.aspx|accessdate=18 May 2015}}</ref> In 2007, she began serving as a volunteer at St Andrew's War Memorial Hospital. On 18 November 2014 she was awarded a UnitingCare Queensland Moderator's Medal for outstanding community service.<ref>{{cite web|title=Mmoderators Medals Recognise Service|url=http://www.qct.org.au/index.php?option=com_content&view=article&id=1537:moderators-medals-recognise-service&catid=1:latest&Itemid=39|website=Queensland Churches Together}}</ref> 

Her biography, ''Pixie Annat - Champion of Nurses'' was published by [[University of Queensland Press]] in March 2015 and reviewed in the ''[[The Australian|Weekend Australian Review]]''.<ref>{{cite web|url=http://www.uqp.uq.edu.au/book.aspx/1341/Pixie%20Annat-%20Champion%20of%20Nurses|title=UQP - Pixie Annat: Champion of Nurses|work=uq.edu.au}}</ref><ref>http://www.theaustralian.com.au/arts/review/world-war-ii-nurses-heroic-efforts-recalled-in-two-books/story-fn9n8gph-1227361583162</ref>

==References==
{{reflist|2}}

{{DEFAULTSORT:Annat, Pixie}}
[[Category:Australian nurses]]
[[Category:Living people]]
[[Category:1930 births]]
[[Category:Australian women nurses]]