{{Infobox airport
| name         = Songwe Airport
| nativename   = {{small|{{native phrase|sw|Uwanja wa Ndege wa Songwe}}}}
| nativename-a =
| nativename-r =
| image        = Songwe Airport.jpg
| image-width  = <!-- if less than 200 -->
| caption      = [[Air Tanzania]] DHC-8-300 at the airport
| image2       =
| image2-width = <!-- if less than 200 -->
| caption2     =
| IATA         =
| ICAO         = HTGW
| FAA          =
| TC           =
| LID          =
| GPS          =
| WMO          =
| type         = Public
| owner-oper   =
| owner        = [[Government of Tanzania]]
| operator     = [[Tanzania Airports Authority]]
| city-served  = [[Mbeya]]
| location     = [[Mbeya Region]], [[Tanzania]]
| hub          =
| focus_city   =
| built        = <!-- military airports -->
| used         = <!-- military airports -->
| commander    = <!-- military airports -->
| occupants    = <!-- military airports -->
| metric-elev  = 
| elevation-f  = 4412
| elevation-m  =
| website      =
| coordinates  = {{coord|8|55|10|S|33|16|25|E|region:TZ|display=inline,title}}
| image_map              = 
| image_mapsize          =
| image_map_alt          =
| image_map_caption      =
| pushpin_map            = Tanzania
| pushpin_mapsize        =
| pushpin_map_alt        =
| pushpin_map_caption    = Location of airport in Tanzania
| pushpin_relief         = 
| pushpin_image          =
| pushpin_label          = '''HTGW'''
| pushpin_label_position =
| pushpin_mark           =
| pushpin_marksize       =
| metric-rwy   = yes
| r1-number    = 09/27
| r1-length-f  = 
| r1-length-m  = 3330
| r1-surface   = Asphalt
| h1-number    =
| h1-length-f  =
| h1-length-m  =
| h1-surface   = <!-- up to h12 -->
| stat-year    = 2015
| stat1-header = Passengers
| stat1-data   = {{increase}} 133,774 
| stat2-header = Aircraft movements
| stat2-data   = {{decrease}} 1,913
| stat3-header = Cargo (kg)
| stat3-data   = {{decrease}} 659,029
| footnotes    = Source: TAA <ref>{{cite web|title=Tanzania airport traffic statistics until 2015|url=http://www.taa.go.tz/index.php/download/statisticts/16-traffic-statistics|website=taa.go.tz|publisher=Tanzania Airport Authority|accessdate=11 August 2016}}</ref> Google Maps<ref>[https://www.google.com/maps/@-8.9224438,33.2746862,5150m/data=!3m1!1e3 Google Maps - Songwe]</ref> GCM<ref>{{GCM|HTGW}}</ref>
}}

'''Songwe Airport''' {{airport codes||HTGW}}, usually referred to in flight schedules as '''Mbeya''', is an airport in the southern highland region of [[Tanzania]], serving the city of [[Mbeya]] and the surrounding [[Mbeya  Region]]. It is {{convert|20|km}} west of the city, off the A104 trunk road, and is able to accommodate commercial jet traffic, whereas the unpaved [[Mbeya Airport]] is not.

The Songwe [[non-directional beacon]] (Ident: '''SW''') is located {{convert|4.8|nmi|km}} off the threshold of runway 28.<ref>[https://skyvector.com/?ll=-8.919,33.376&chart=301&zoom=1 SkyVector Aeronautical Charts]</ref>

==History==
Redevelopment of Songwe Airport began in 2001.<ref>[http://www.tanzaniainvest.com/transport/new-tanzania-airport-prepares-to-open-this-year "Songwe airport prepares to open"] Tanzaniainvest.com 8 January 2009</ref> The [[Tanzania Civil Aviation Authority]] issued a [[NOTAM]] on 12 December 2012 saying "WEF 13 DEC 2012 new airport called Songwe opened and operational."<ref>{{cite web | url = http://www.tcaa.go.tz/do/tcaa_documents/NOTAMCURRENT_31.pdf | title = DAILY NOTAM UPDATE | format = [[PDF]] | publisher = [[Tanzania Civil Aviation Authority]] | date = 12 December 2012 | accessdate = 25 January 2013}}{{dead link|date=August 2016}}</ref>

==Airlines and destinations==
{{Airport-dest-list
|[[Fastjet]] | [[Julius Nyerere International Airport|Dar es Salaam]] 
}}

==Scheduled flights==
[[Fastjet]] operates two daily flights to and from [[Julius Nyerere International Airport|Dar es Salaam]], having commenced serving this airport on 1 November 2013. <ref>[https://www.fastjet.com/tz/corporate/investor-news/first-fastjet-flight-takes-off-to-mbeya "First fastjet flight takes off to Mbeya"], ''Fastjet Tanzania'', 01/11/2013</ref> <ref>[http://www.fastjet.com/tz/blog/new-flying-schedule "New Flying Schedule!"], ''Fastjet Tanzania'', 28/05/2014</ref> This is done on [[Airbus A319]] jet aircraft.

In the past, regular services have been provided by [[Air Tanzania]], [[Tropical Air]], [[Auric Air]], and [[Precision Air]], but these airlines have discontinued their scheduled services; charter flights can still be arranged with Tropical Air and Auric Air.<ref>[http://www.auricair.com/Destination/Mbeya Mbeya], Auric Air, 2 September 2015</ref><ref>[http://tropicalair.co.tz/general/OurServices Our Services], Tropical Air, 2 September 2015</ref>

==See also==
* [[List of airports in Tanzania]]
* [[Transport in Tanzania]]
{{Sister project links | wikt=no | b=no | q=no | n=no |s=no | v=no | voy=no | species=no |d=Q7561639 | display= Songwe Airport}}

==References==
{{Reflist}}

==External links==
*[http://ourairports.com/airports/HTGW/pilot-info.html#general OurAirports - Songwe]
*[https://www.openstreetmap.org/#map=13/-8.9327/33.2748 OpenStreetMap - Songwe]
* {{youtube|8az6DqWBybA|Songwe Airport}}
* {{youtube|zlQfKRsYwz4|Songwe Airport}}

{{Portalbar|Tanzania|Aviation}}
{{Airports in Tanzania}}
[[Category:Airports in Tanzania]]
[[Category:Buildings and structures in the Mbeya Region]]