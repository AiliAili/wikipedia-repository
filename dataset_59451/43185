<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=M.F.12
 | image=Marinens Flyvebaatfabrikk M.F.12.jpg
 | caption=The M.F.12 prototype F.14 (V).
}}{{Infobox Aircraft Type
 | type=Military trainer seaplane
 | national origin=Norway
 | manufacturer=[[Marinens Flyvebaatfabrikk]]
 | designer=[[Johan E. Høver]]
 | first flight=11 July [[1939 in aviation|1939]]
 | introduced=
 | retired=
 | status=
 | primary user=[[Royal Norwegian Navy Air Service]]
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Marinens Flyvebaatfabrikk M.F.12''' (sometimes known as the '''Høver M.F.12''', after its designer) was a seaplane built in Norway in 1939 as a military trainer aircraft<ref name="JEA">Taylor 1989, 620</ref> to replace the Norwegian Navy's aging fleet of [[Marinens Flyvebaatfabrikk M.F.8|M.F.8]] trainers.<ref>Hafsten 2003, 107</ref><ref name="MF-12">"Skoleflyet MF-12"</ref> Only a single prototype was constructed before Germany's [[Norwegian Campaign|invasion of Norway]] in 1940.<ref name="MF-12" />
The prototype, M.F.12 F.14 (V), was the last trainer built for the [[Royal Norwegian Navy Air Service]] (RNNAS).<ref>Hafsten 2003, 46</ref>

==Design and manufacture==
By the late 1930s flying school of the RNNAS requested that a new trainer aircraft should be acquired to replace the M.F.8s. Following international aircraft development, a monoplane designed seemed preferable and once again the RNNAS decided to design and construct the aircraft themselves. The order was given to the seaplane factory [[Marinens Flyvebaatfabrikk]] on 15 June 1937. The factory immediately began work on the design.<ref name="Hafsten102">Hafsten 2003, 102</ref>
The M.F.12 was a conventional design with two open cockpits in tandem and a low, strut-braced monoplane wing. The cockpit design was carried over from the [[Marinens Flyvebaatfabrikk M.F.11|M.F.11]] reconnaissance aircraft then in service, and while it was originally intended to use the same float design that Marinens Flyvebaatfabrikk had been producing for their licence-built [[Hansa-Brandenburg W.33]]s, eventually floats by American manufacturer [[EDO Corporation|EDO]] were chosen.<ref name="MF-12" /> Four engines were considered for the M.F-12: the [[Wright R-670]], [[Lycoming R-530]]-D2, [[Jacobs L-4]]M and the [[Continental K]], with the Jacobs engine eventually selected.<ref name="MF-12" />

==First flights==
The first, unofficial, flight occurred on 11 July 1939, the day the aircraft was launched,<ref name="Hafsten102"/> at the hands of [[Johan E. Høver]] himself, a fact initially kept secret because he was unauthorised to make it, and the first official flight took place six days later.<ref name="MF-12"/> The official orders for 11 July 1939 had been to merely taxi the M.F.12 F.14 (V) to the northern slipway, where it was to wait until the factory's control officer [[Kristian Østby]] returned from a trip to [[Nazi Germany|Germany]] to accept the first of the [[Heinkel He 115]] torpedo bombers ordered by the RNNAS.<ref name="Hafsten102"/> All new aircraft were supposed to be first tested by the control officer, who also received a 500 [[Norwegian krone]]r (NOK) fee for each such test. However, as Høver felt that the aircraft seemed to work fine, he made a 20-minute flight, being very pleased with its performance when he landed. The issue was kept secret, and Østby could make the official first flight and collect his 500 NOK on 17 July.<ref name="Hafsten103">Hafsten 2003, 103</ref>

==Development halted==
Development stalled after a flight test by Lützow Holm, commander of the [[Karljohansvern]] base. After a rough landing, following drastic and hazardous flying,<ref name="Hafsten103"/> he demanded that the design be revised to strengthen it before entering production.<ref name="MF-12" /> The construction yard opposed this, pointing out that the design was already 10% stronger than had been specified by the Navy.<ref name="MF-12" /> While the dispute continued, the M.F.12 was grounded at Karljohansvern.<ref>Hafsten 2003, 49</ref> In June 1939, shortly before the M.F.12 took to the air for the first time, the [[Norwegian Ministry of Defence]] set down a joint military and civilian commission to decide on a new common trainer for both the Royal Norwegian Navy Air Service, its Army cousin; the [[Norwegian Army Air Service]], and the civilian air schools. Further work on the M.F.12 was put on hold while the commission worked on its report. Before any conclusion could be made, the [[Operation Weserübung|Germans invaded Norway]] on 9 April 1940.<ref name="Hafsten103"/>

==Fate of the prototype==
The final disposition of the prototype is lost to history, but Høver recalls it being impounded by the [[Occupation of Norway by Nazi Germany|German occupation forces]],<ref name="MF-12" /> after having been abandoned at [[Horten]] on 9 April 1940.<ref>Hafsten 2003, 234</ref> F.14 (V) had been at Karljohansvern naval base in Horten for repairs when the Germans landed in Norway.<ref>Hafsten 2003, 272</ref>

==Quotations==
{{quote|It was a first-rate, nice little bus. It flew all on its own when in hands off mode, incredibly light and comfortable.|[[Johan E. Høver]] after his secret 11 July 1939 flight|page 103 in Bjørn Hafsten's ''Marinens Flygevåpen 1912&ndash;1944''<ref group="quote">{{lang-no|Det var en førsteklasses, fin liten buss. Den fløy helt av seg selv når innstillet på hands off, storartet lett og behagelig.}} Hafsten 2003, 103</ref>}}

==Specifications==
{{aerospecs
|ref=<!-- reference -->"Skoleflyet MF-12"
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=Two, pilot and instructor
|capacity=
|length m=9.73
|length ft=31
|length in=11
|span m=13.60
|span ft=34
|span in=7
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.60
|height ft=8
|height in=6
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Jacobs L-4]]M
|eng1 kw=<!-- prop engines -->168
|eng1 hp=<!-- prop engines -->225
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=195
|max speed mph=121
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References and notes==
;References
{{Reflist|2}}

;Notes
<references group="quote" />

==Bibliography==
{{commons category|Marinens Flyvebaatfabrikk M.F.12}}
* {{cite book |last= Hafsten |first= Bjørn |author2=Tom Arheim |title=Marinens Flygevåpen 1912&ndash;1944 |year=2003 |publisher=TankeStreken AS |location=Oslo |isbn=82-993535-1-3 |language=Norwegian}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London }}
* {{cite web |last=Stein |first=Gulli|title=Skoleflyet MF-12 |work=Borreminne |publisher=Borre Historielag |url=http://borreminne.hive.no/aargangene/1998_99/08-skoleflyet.htm |accessdate=22 October 2008 |language=Norwegian}}
<!-- ==External links== -->

{{Marinens Flyvebaatfabrikk aircraft}}

[[Category:Norwegian military trainer aircraft 1930–1939]]
[[Category:Floatplanes]]
[[Category:Marinens Flyvebaatfabrikk aircraft|MF12]]
[[Category:Single-engine aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Propeller aircraft]]