{{Infobox journal
| title = Cambridge Journal of Economics
| cover = 
| discipline = [[Economics]]
| abbreviation = Camb. J. Econ.
| editor = Jacqui Lagrue
| publisher = [[Oxford University Press]]
| country = [[United Kingdom]]
| frequency = Bimonthly
| history = 1977–present
| impact = 1.311
| impact-year = 2014
| website = http://cje.oxfordjournals.org/
| link1 = http://www.oxfordjournals.org/our_journals/cameco/content/current
| link1-name = Online access
| link2 = http://cje.oxfordjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 0309-166X
| eISSN = 1464-3545
| OCLC = 3020038
| LCCN = 80647135
| CODEN = 
| JSTOR = 0309166X
}}

The '''''Cambridge Journal of Economics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] of [[economics]]. The journal was founded in 1977 by the ''Cambridge Political Economy Society'' with the aim of publishing articles that followed the economic traditions established by [[Karl Marx]], [[J. M. Keynes]], [[Michał Kalecki]], [[Joan Robinson]], and [[Nicholas Kaldor]].<ref>[http://www.oxfordjournals.org/our_journals/cameco/for_authors/index.html Cambridge Journal of Economics:Information for Authors] (Accessed August 2012)</ref> [[Luigi Pasinetti]] has noted the "strong ties" between the Cambridge Journal of Economics and the [[Cambridge School of Keynesian Economics]].<ref>{{Cite journal | last = Pasinetti | first = Luigi L. | author-link = Luigi Pasinetti | title = The Cambridge School of Keynesian Economics | journal = Cambridge Journal of Economics, special issue: Economics for the Future | volume = 29 | issue = 6 |pages = 837–848 | publisher = [[Oxford University Press|Oxford Journals]] | doi = 10.1093/cje/bei073 | jstor = 23601601 | date = November 2005 | url = http://dx.doi.org/10.1093/cje/bei073 | ref = harv | postscript = .}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.311, ranking it 92nd out of 333 journals in the category "Economics".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Economics |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of economics journals]]

== References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://cje.oxfordjournals.org/}}
* [http://www.cpes.org.uk/ Cambridge Political Economy Society]

{{DEFAULTSORT:Cambridge Journal of Economics }}
[[Category:Bimonthly journals]]
[[Category:Economics journals]]
[[Category:English-language journals]]
[[Category:Oxford University Press academic journals]]