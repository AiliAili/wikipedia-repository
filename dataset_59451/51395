{{Infobox journal
| title = Geographical Review
| cover = Geographical Review Cover.JPG
| caption =The flagship journal of the American Geographical Society
| editor = David H. Kaplan
| discipline = [[Geography]]
| abbreviation = 
| publisher = [[Wiley-Blackwell]] on behalf of the [[American Geographical Society]]
| country = United States
| frequency = Quarterly
| history = 1916-present
| openaccess = 
| impact = 0.750
| impact-year = 2014
| website = http://americangeo.org/geographical-review/
| link1 = http://onlinelibrary.wiley.com/journal/10.1001/%28ISSN%291931-0846
| link1-name = Journal page at publisher's website
| link2 = http://onlinelibrary.wiley.com/journal/10.1001/(ISSN)1931-0846/currentissue
| link2-name = Online access
| link3 = http://onlinelibrary.wiley.com/journal/10.1001/(ISSN)1931-0846/issues
| link3-name = Online archive
| JSTOR = 00167428
| ISSN = 0016-7428
| eISSN = 1931-0846
| OCLC = 224456890
| LCCN = 17015422
| CODEN = GEORAD
}}
The '''''Geographical Review''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf  of the [[American Geographical Society]]. It covers all aspects of [[geography]]. The "Geographical Record" section presents short articles on current topical and regional issues. Each issue also includes reviews of recent books, monographs, and atlases in geography and related fields. The [[editor-in-chief]] is David H. Kaplan ([[Kent State University]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [http://onlinelibrary.wiley.com/journal/10.1001/(ISSN)1931-0846 5 year impact factor] of 0.750.<ref name=WoS>{{cite book |year=2013 |chapter=Geographical Review |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-01 |series=[[Web of Science]] |postscript=.}}</ref>

==History==
In 1852, the American Geographical Society began publishing its first academic journal, the ''Bulletin [and Journal] of the American Geographical Society''.<ref>{{cite web |url=http://www.gwu.edu/~geog/gat/gat.html |title=Geography in America Timeline |publisher=gwu.edu |accessdate=2010-06-17}}</ref> This publication continued through 1915, when it was succeeded by the ''Geographical Review''.<ref name="VCH4">Brooks, Arthur, ''Index to the Bulletin of the American Geographical Society: 1852-1915'' (1918) New York: American Geographical Society.</ref>

Notable contributors include [[Robert J. Flaherty]], who introduced the concept for ''[[Nanook of the North]]'' in his contributions to ''Geographical Review,''<ref name="WDL">{{cite web |url = http://www.wdl.org/en/item/6767/ |title = Map of Belcher Islands |website = [[World Digital Library]] |year = 1909 |accessdate = 2013-06-03 }}</ref> [[Mark Jefferson (geographer)|Mark Jefferson]], who was the chief cartographer of the American Delegation to the Paris Peace Conference in 1919 and presented his article ''The Law of the Primate City'' in the 1939, as well as [[Martin J. Pasqualetti|Dr. Martin Pasqualetti]] who in 2011 introduced his theory on ''Social Barriers to Renewable Energy Landscapes.'' 

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://americangeo.org/geographical-review/}}

[[Category:Geography journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1916]]
[[Category:Academic journals associated with learned and professional societies]]