{{good article}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name = Baby, Please Don't Go
| Cover = Baby, Please Don't Go Williams single cover.jpg
| Artist = [[Big Joe Williams|Joe Williams]]' Washboard Blues Singers
| B-side = "Wild Cow Blues"
| Released = {{Start date|1935}}
| Format = [[Gramophone record|10-inch 78&nbsp;rpm]]
| Recorded = Chicago, October 31, 1935
| Genre = [[Blues]]
| Writer = Unknown (traditional)
| Length = {{Duration|m=3|s=22}}
| Label = [[Bluebird Records|Bluebird]] (B–6200)
| Producer = [[Lester Melrose]]
}}
"'''Baby, Please Don't Go'''" is a [[blues]] song which has been called "one of the most played, arranged, and rearranged pieces in blues history" by music historian {{ill|Gerard Herzhaft|fr}}.{{sfn|Herzhaft|1992|p=437}} Its roots have been traced back to nineteenth-century American songs which deal with themes of bondage and imprisonment. [[Delta blues]] musician [[Big Joe Williams]] popularized the song with several versions beginning in 1935.

After World War II, [[Chicago blues]] and [[rhythm and blues]] artists adapted the song to newer music styles. In 1952, a [[doo-wop]] version by [[the Orioles]] reached the top ten on the race records chart. In 1953, [[Muddy Waters]] recorded the song as an electric Chicago-ensemble blues piece, which influenced many subsequent renditions.  By the early 1950s, the song became a [[blues standard]].

In the 1960s, "Baby, Please Don't Go" became a popular rock song after the Northern Irish group [[Them (band)|Them]] recorded it in 1964. Several music writers have identified [[Jimmy Page]], a studio guitarist at the time, as participating in the recording, although his exact contributions are unclear. Subsequently, Them's uptempo rock arrangement also made it a rock standard. "Baby, Please Don't Go" has been inducted into both the [[Blues Hall of Fame|Blues]] and [[Rock and Roll Hall of Fame|Rock and Roll]] Halls of Fame.

==Background==
"Baby, Please Don't Go" is likely an adaptation of "Long John", an old folk theme which dates back to the time of [[slavery in the United States]].{{sfn|Herzhaft|1992|p=437}} Blues researcher Paul Garon notes that the melody is based on "[[Alabamy Bound]]", composed by [[Tin Pan Alley]] writer [[Ray Henderson]], with lyrics by [[Buddy DeSylva]] and [[Bud Green]] in 1925.{{sfn|Garon|2004|p=39}}{{efn|An earlier "[[I'm Alabama Bound]]", with its own recording history,  was published by Robert Hoffman in 1909.}} The song, a [[vaudeville]] show tune, inspired several other songs between 1925 and 1935, such as "Elder Greene Blues", "Alabama Bound", and "Don't You Leave Me Here".{{sfn|Garon|2004|p=39}}<ref name="Blues Foundation"/>  These variants were recorded by [[Charlie Patton]], [[Lead Belly]], [[Monette Moore]], [[Henry Thomas (blues musician)|Henry Thomas]], and [[Tampa Red]].{{sfn|Garon|2004|p=39}}

Author Linda Dahl suggests a connection to a song with the same title by [[Mary Johnson (singer)|Mary Williams Johnson]] in the late 1920s and early 1930s.{{sfn|Dahl|1984|p=110}} However, Johnson, who was married to jazz-influenced blues guitarist [[Lonnie Johnson (musician)|Lonnie Johnson]], never recorded it and her song is not discussed as influencing later performers.{{sfn|Herzhaft|1992|p=437}}<ref name="Blues Foundation"/>{{sfn|Birnbaum|2012|p=302}} Blues researcher [[Jim O'Neal]] notes that Williams "sometimes said that the song was written by his wife, singer Bessie Mae Smith (aka Blue Belle and St. Louis Bessie) [not the same as the popular [[Bessie Smith]] of the 1920s and 1930s]".<ref name="Blues Foundation"/>

==Original song==
Big Joe Williams used the imprisonment theme for his October 31, 1935, recording of "Baby, Please Don't Go".  He recorded it during his first session for [[Lester Melrose]] and [[Bluebird Records]] in Chicago.<ref name="Blues Foundation"/> It is an ensemble piece with Williams on vocal and guitar accompanied by Dad Tracy on one-string fiddle and Chasey "Kokomo" Collins on [[washboard (musical instrument)|washboard]], who are listed as "Joe Williams' Washboard Blues Singers" on the single.<ref name="Blues Foundation"/>  Musical notation for the song indicates a moderate-tempo fifteen-bar blues in {{su|a=c|p=4|b=4|lh=0.85em}} or [[common time]] in the [[Key (music)|key]] of B flat.{{sfn|Hal Leonard|1995|pp=17–19}}{{efn|The sheet music includes a 1944 copyright date, indicating a later version of the song{{sfn|Hal Leonard|1995|p=17}} (Williams' 1935 recording is in the key of B).}} As with many Delta blues songs of the era, it remains on the [[Tonic (music)|tonic]] chord (I) throughout without the progression to the [[subdominant]] (IV) or [[Dominant (music)|dominant]] (V) chords.{{sfn|Hal Leonard|1995|pp=17–19}} The lyrics express a prisoner's anxiety about his lover leaving before he returns home:{{sfn|Gioia|2008|p=130}}
{{poemquote|Now baby please don't go, now baby please don't go
Baby please don't go back to New Orleans, and get your cold ice cream
I believe there's a man done gone, I believe there's a man done gone
I believe there's a man done gone to the county farm, with a long chain on}}
The song became a hit and established Williams' recording career.{{sfn|Herzhaft|1992|p=381}} On December 12, 1941, he recorded a second version titled "Please Don't Go" in Chicago for Bluebird, with a more modern arrangement and lyrics.{{sfn|Demetre|1994|p=23}} Blues historian Gerard Herzhaft calls it "the most exciting version",{{sfn|Herzhaft|1992|p=437}} which Williams recorded using his trademark [[nine-string guitar]]. Accompanying him are [[Sonny Boy Williamson I]] on harmonica and Alfred Elkins on imitation bass (possibly a [[washtub bass]]).{{sfn|Demetre|1994|p=29}} Since both songs appeared before recording industry publications began tracking such releases, it is unknown which version was more popular. In 1947, he recorded it for [[Columbia Records]] with Williamson and [[Ransom Knowling]] on bass and Judge Riley on drums. This version did not reach the [[Hot R&B/Hip-Hop Songs|''Billboard'' Race Records chart]],{{sfn|Whitburn|1988|pp=444–445}} but represents a move toward a more urban blues treatment of the song.

==Later blues and R&B recordings==
Big Joe Williams various recordings inspired other blues musicians to record their interpretations of the song{{sfn|Escott|2002|p=54}} and it became a [[blues standard]].{{sfn|Herzhaft|1992|p=437}} Early examples include [[Papa Charlie McCoy]] as "Tampa Kid" (1936), [[Leonard Caston|Leonard "Baby Doo" Caston]] (1939), [[Lightnin' Hopkins]] (1947), [[John Lee Hooker]] (1949) and [[Big Bill Broonzy]] (1952).{{sfn|Garon||p=40}} By the early 1950s, the song was reworked in contemporary musical styles, with an early [[rhythm and blues]]/[[jump blues]] version by [[Billy Wright (musician)|Billy Wright]] (1951),{{sfn|Herzhaft|1992|p=437}} a harmonized [[doo-wop]] version by [[the Orioles]] (a number eight R&B hit in 1952),{{efn|Music historian Larry Birbaum suggested that the Orioles' 1951 version inspired [[James Brown]]'s first hit "[[Please, Please, Please]]" (1956).{{sfn|Birnbaum|2012|p=302}}}} and a [[Afro-Cuban]]-influenced rendition by Rose Mitchell (1954).{{sfn|Herzhaft|1992|p=437}}

In 1953, [[Muddy Waters]] recast the song as a Chicago-blues ensemble piece with [[Little Walter]] and [[Jimmy Rogers]].{{sfn|Palmer|1989|p=28}}  [[Chess Records]] originally issued the single with the title "Turn the Lamp Down Low", although the song is also referred to as "Turn Your Lamp Down Low",<ref name="Blues Foundation"/> "Turn Your Light Down Low",{{sfn|Garon|2004|p=40}} or "Baby Please Don't Go".{{efn|Muddy Waters' original Chess single lists the songwriters as "Strutt, Alexander", although reissues credit "McKinley Morganfield" (his legal name).  The song is registered as "Turn the Lamps{{sic}} Down Low" with Joseph Lee Williams as the songwriter. [[International Standard Musical Work Code|ISWC]] T-070.278.618-2.}}  He regularly performed the song, several of which were recorded. Live versions appear on ''Muddy Waters at Newport 1960'' and on ''[[Live at the Checkerboard Lounge, Chicago 1981]]'' with members of [[the Rolling Stones]].{{sfn|Gordon|2002|p=266}} [[AllMusic]] critic Bill Janovitz cites the influence of Waters' adaptation:
{{quote|The most likely link between the Williams recordings and all the rock covers that came in the 1960s and 1970s would be the Muddy Waters 1953 Chess side, which retains the same swinging phrasing as the Williams takes, but the session musicians beef it up with a steady driving rhythm section, electrified instruments and Little Walter Jacobs wailing on blues harp.{{sfn|Bonomo|2010|loc=eBook}}}}

==Van Morrison and Them rendition==
{{Infobox single
| Name = Baby, Please Don't Go
| Cover = BabyPleaseDontGo-Them.jpg
| Artist = [[Them (band)|Them]]
| B-side = "[[Gloria (Them song)|Gloria]]"
| Released = {{Start date|1964|11|6|}} (UK)
| Format = [[Gramophone record#Formats|7-inch 45&nbsp;rpm]]
| Recorded = October 1964
| Writer = Unknown (traditional)
| Genre = [[Blues rock]]
| Length = {{Duration|m=2|s=38}}
| Label = [[Decca Records|Decca]] (F.12018)
| Producer = [[Bert Burns]]
}}

"Baby Please Don't Go" was one of the earliest songs recorded by [[Them (band)|Them]], fronted by a 19-year-old [[Van Morrison]]. Their rendition of the song was derived from a John Lee Hooker version recorded in 1949 as "Don't Go Baby" using the pseudonym "Texas Slim" (King 4334),{{sfn|Murray|2002|pp=212, 302}} which later appeared on a 1959 album, ''Highway of Blues'', that Van Morrison heard and felt was "something really unique and different" with "more soul" than he'd previously heard.{{sfn|Murray|2002|pp=212, 302}} Them recorded "Baby, Please Don't Go" for [[Decca Records]] in October 1964. 

Besides Morrison, there is conflicting information about who participated in the session. In addition to the group's original members (guitarist Billy Harrison, bassist Alan Henderson, drummer Ronnie Millings, and keyboard player [[Eric Wrixon]]), others have been suggested: Pat McAuley on keyboards, Bobby Graham on a second drum kit, [[Jimmy Page]] on second guitar,{{sfn|Thompson|2008|p=303}} and [[Peter Bardens]] on keyboards.{{sfn|Strong|2002|loc=eBook}} As Page biographer George Case notes, "There is a dispute over whether it is Page's piercing blues line that defines the song, if he only played a run Harrison had already devised, or if Page only backed up Harrison himself".{{sfn|Case|2007|p=35}} Morrison has acknowledged Page's participation in the early sessions: "He played rhythm guitar on one thing and doubled a bass riff on the other"{{sfn|Rogan|2006|pp=101, 111}} and Morrison biographer [[Johnny Rogan]] notes that Page "doubled the distinctive riff already worked out by Billy Harrison".{{sfn|Rogan|2006|pp=101, 111}} Music critic [[Greil Marcus]] comments that during the song's quieter middle passage "the guitarist, session player Jimmy Page or not, seems to be feeling his way into another song, flipping half-riffs, high, random, distracted metal shavings".{{sfn|Marcus|2010|loc=eBook}}{{efn|Beginning about 1:22 in Them's recording, bassy-sounding riffs appear.<ref>
{{cite AV media
| people = Them
| year = 1964
| title = Baby, Please Don't Go 
| medium = Song recording
| time = 1:22
| location = London
| publisher = [[Decca Records]]
| id = F.12018
}}</ref>}} Them's blues-rock arrangement is "now regarded justly as definitive", with "much of its appeal emanat[ing] from the tingling lead guitar section", according to music writer Adam Clayson.{{sfn|Clayson|2006|p=61}}

"Baby, Please Don't Go" was released as Them's second single on November 6, 1964.{{sfn|Thompson|2008|p=303}} With the B-side, "[[Gloria (Them song)|Gloria]]", it became their first hit, reaching number ten on the [[UK Singles Chart]] in February 1965.<ref name="OCC">
{{cite web
| url = http://www.officialcharts.com/artist/11592/them/
| title = Them – Singles
| website =  [[Official Charts Company|Official Charts]]
| accessdate = April 26, 2015
}}</ref> The single was released in the U.S. in 1965, but only "Gloria" became a hit the following year.<ref>
{{cite web
| url = http://www.allmusic.com/artist/them-mn0000925181/awards
| title = Them – Awards
| website = [[AllMusic]]
| accessdate = April 26, 2015
}}</ref> The song was not included on Them's original British or American albums (''[[The Angry Young Them]]'' and ''[[Them Again]]''), however, it has appeared on several compilation albums, such as ''[[The Story of Them Featuring Van Morrison]]'' and ''[[The Best of Van Morrison]]''.<ref>
{{cite web
| url = http://www.allmusic.com/song/baby-please-dont-go-mt0008565552
| title = Van Morrison/Them: Baby Please Don't Go – Appears On
| website = [[AllMusic]]
| accessdate = April 26, 2015
| ref = harv
}}</ref>  When it was reissued in 1991 as a single in the UK (London LON 292), it reached number 65 in the chart.<ref name="OCC"/> Van Morrison also accompanied John Lee Hooker during a 1992 performance, where Hooker sings and plays "Baby, Please Don't Go" on guitar while sitting on a dock, with harmonica backing by Morrison; it was released on the 2004 ''[[Come See About Me (DVD)|Come See About Me]]'' Hooker DVD.<ref>
{{cite web
| url = http://www.allmusic.com/album/come-and-see-about-me-mw0002401655
| title = John Lee Hooker: Come and See About Me – Overview
| website = [[AllMusic]]
| accessdate = April 26, 2015
| ref = harv
}}</ref>

==AC/DC version==
[[File:ACDC-Hughes-long ago.jpg|thumb|200px|right|upright|Angus Young and Bon Scott at the [[Ulster Hall]] in August 1979]]
"Baby, Please Don't Go" was a feature of [[AC/DC]]'s live shows since their beginning.{{sfn|Walker|2011|p=135}} Although they have expressed their interest and inspiration in early blues songs,{{sfn|Perkins|2011|loc=eBook}} music writer [[Mick Wall]] identifies Them's adaptation of the song as the likely source.{{sfn|Wall|2013|loc=eBook}} In November 1974, [[Angus Young]], [[Malcolm Young]], and [[Bon Scott]] recorded it for their 1975 Australian debut album, ''[[High Voltage (1975 album)|High Voltage]]''.{{sfn|Perkins|2011|loc=eBook}} [[Tony Currenti]] is sometimes identified as the drummer for the song, although he suggests that it had been already recorded by [[Peter Clack]].{{sfn|Fink|2014|p=83}} Wall notes that producer [[George Young (rock musician)|George Young]] played bass for most of the album,{{sfn|Wall|2013|loc=eBook}} although [[Rob Bailey (musician)|Rob Bailey]] claims that many of the album's tracks were recorded with him.{{sfn|Walker|2011|p=139}}

''High Voltage'' and a single with "Baby, Please Don't Go" were released simultaneously in Australia in February 1975.{{sfn|Walker|2011|p=139}}{{efn|The Albert Productions AC/DC single misidentified the songwriter as [[Big Bill Broonzy]].{{sfn|Walker|2011|p=139}}}} AllMusic critic Eduardo Rivadavia called the song "positively explosive".<ref name="Rivadavia">
{{cite web
| url = http://www.allmusic.com/album/high-voltage-australia-mw0000453412
| title = AC/DC: High Voltage (Australia) – Album Review
| last = Rivadavia
| first = Eduardo
| website = [[AllMusic]]
| accessdate = April 26, 2015
}}</ref> [[Albert Productions]] issued it as the single's B-side. However, the A-side was largely ignored and "Baby, Please Don't Go" began receiving airplay.{{sfn|Wall|2013|loc=eBook}} The single entered the chart at the end of March 1975{{sfn|Walker|2011|p=145}} and peaked at number 10 in April.{{sfn|Walker|2011|p=148}} Also in April 1975, one month after drummer [[Phil Rudd]] and bassist [[Mark Evans (musician)|Mark Evans]] joined AC/DC, the group performed the song for 
the Australian music program ''[[Countdown (Australian TV series)|Countdown]]''.{{sfn|Miller|2009|loc=eBook}}
{{sfn|Bonomo|2010|loc=eBook}} For their appearance, "Angus wore his trade mark schoolboy uniform while Scott took the stage wearing a wig of blonde braids, a dress, make-up, and earrings", according to author Heather Miller.{{sfn|Miller|2009|loc=eBook}} [[Joe Bonomo]] describes Scott as "a demented [[Pippi Longstocking]]", and Perkins notes his "tattoos and a disturbingly short skirt."{{sfn|Perkins|2011|loc=eBook}} Evans describes the reaction:
{{quote|As soon as his vocals are about to begin he comes out from behind the drums dressed as a schoolgirl. And it was like a bomb went off in the joint; it was pandemonium, everybody broke out in laughter. [Scott] had a wonderful sense of humor.{{sfn|Miller|2009|loc=eBook}}}}
Scott mugs for the camera and, during the guitar solo/vocal improvization section, he lights a cigarette as he duels with Angus with a green mallet.{{sfn|Bonomo|2010|loc=eBook}} Rudd laughs throughout the performance.{{sfn|Bonomo|2010|loc=eBook}} Although "Baby, Please Don't Go" was a popular part of AC/DC's performances (often as the closing number), the song was not released internationally until their 1984 compilation [[Extended play|EP]] ''[['74 Jailbreak]]''.{{sfn|Perkins|2011|loc=eBook}} The video from the ''Countdown'' show is included on the ''[[Family Jewels (AC/DC album)|Family Jewels]]'' DVD compilation in 2005.

==Recordings by other artists==
[[Aerosmith]] recorded "Baby, Please Don't Go" for their blues cover album, ''[[Honkin' on Bobo]]'', which was released on March 30, 2004.<ref name="Erlewine"/> The album was produced by [[Jack Douglas (record producer)|Jack Douglas]], who had worked on the group's earlier albums, and reflects a return to their [[hard rock]] roots.<ref name="Erlewine">
{{cite web
| url = http://www.allmusic.com/album/honkin-on-bobo-mw0000334649
| title = Aerosmith: Honkin' on Bobo – Album Review
| last = Erlewine
| first = Stephen Thomas
| authorlink = Stephen Thomas Erlewine
| website = [[AllMusic]]
| accessdate = April 26, 2015
}}</ref> [[Billboard (magazine)|''Billboard'' magazine]] describes the song as "the kind of straight-ahead, hard-driving track that always typified the band's [1970s] records".{{sfn|Billboard|2004|pp=13, 15}} [[Edna Gundersen]] of ''[[USA Today]]'' called their version a "terrific revival."<ref>{{cite web|url=http://www.usatoday.com/life/music/reviews/2004-03-29-listen-up_x.htm|title=Clapton, Aerosmith dabble in the blues|last=Gundersen|first=Edna|authorlink=Edna Gundersen|work=[[USA Today]]|publisher=[[Gannett Company]]|date=March 29, 2004|accessdate=July 1, 2015}}</ref> It was the first single to be released from the album and reached number seven on the [[Mainstream Rock Tracks]] chart.<ref>
{{cite web
| url = http://www.allmusic.com/album/honkin-on-bobo-mw0000334649/awards
| title = Aerosmith: Honkin' on Bobo – Awards
| website = [[AllMusic]]
| accessdate = April 26, 2015
}}</ref> A music video, directed by Mark Haefeli, was produced to promote the single.<ref>
{{cite web
| url = http://www.blabbermouth.net/news/aerosmith-baby-please-don-t-go-video-posted-online/
| title = Aerosmith: 'Baby Please Don't Go' Video Posted Online
| website = [[Blabbermouth.net]]
| date = May 20, 2004
| accessdate = July 1, 2015}}</ref> Subsequently, the song has become a staple of the band's concert repertoire.<ref>
{{cite news
| url = http://www.dallasnews.com/entertainment/music/headlines/20100804-Concert-review-Aerosmith-at-Superpages-6508.ece
| title = Concert review: Aerosmith at Superpages.com Center
| last = Hauk
| first = Hunter
| newspaper = [[The Dallas Morning News]]
| publisher = [[A. H. Belo Corporation|A.H. Belo]]
| date = August 6, 2010
| accessdate = July 1, 2015}}</ref><ref>
{{cite web
| url = http://www.hollywoodreporter.com/review/aerosmith-j-geils-band-concert-29885
| title = Aerosmith/The J. Geils Band – Concert Review
| last = Stingley
| first = Mick
| work = [[The Hollywood Reporter]]
| publisher = [[Prometheus Global Media]]
| date = October 14, 2010
| accessdate = July 1, 2015}}</ref>

Other recorded renditions include those by  [[Mose Allison]] (''Transfiguration of Hiram Brown'', 1960),<ref>
{{cite web 
| url=http://www.allmusic.com/song/baby-please-dont-go-mt0026840270 
| title=Mose Allison: Baby Please Don't Go, Composed by Big Joe Williams
| last =Greenwald
| first = Matthew
| website = [[AllMusic]]
| accessdate = March 15, 2016
}}</ref> [[Paul Revere & the Raiders]] (''[[Just Like Us!]]'', 1966),<ref>
{{cite web
| url = http://www.allmusic.com/album/just-like-us%21-mw0000042201
| title = Paul Revere & the Raiders – Just Like Us! album review
| last = Ruhlmann
| first = William
| website = [[AllMusic]]
| accessdate = December 18, 2015
| ref = harv
}}</ref> [[the Amboy Dukes]] (reached number 106 on the ''[[Billboard (magazine)|Billboard]]'' [[Bubbling Under Hot 100 Singles]] record chart in 1968),<ref>
{{cite journal
| author = Billboard
| date = March 23, 1968
| title = Bubbling Under the Hot 100
| journal = [[Billboard (magazine)|Billboard]]
| volume = 80
| issue = 9
| publisher = Nielsen Business Media
| issn = 0006-2510
| page = 54
| ref = harv
}}</ref> [[Gary Glitter]] (''[[Glitter (Gary Glitter album)|Glitter]]'', 1972) and [[Budgie (band)|Budgie]] (''[[Never Turn Your Back on a Friend]]'', 1973).<ref>
{{cite web
| url = http://www.allmusic.com/album/never-turn-your-back-on-a-friend-mw0000204474
| title = Budgie: Never Turn Your Back on a Friend – Album review
| last = Prato
| first = Greg
| website = [[AllMusic]]
| accessdate = December 18, 2015
| ref = harv
}}</ref>

==Recognition and legacy==
Big Joe Williams' "Baby, Please Don't Go" is included in the [[Rock and Roll Hall of Fame]] list of "500 Songs That Shaped Rock and Roll".<ref>
{{cite web
| url = http://www.rockhall.com/exhibithighlights/500-songs-by-name-ac/
| title = 500 Songs that Shaped Rock and Roll
| website = [[Rock and Roll Hall of Fame]]
| year = 1995
| accessdate = April 26, 2015
| archiveurl = https://web.archive.org/web/20070422234247/http://www.rockhall.com/exhibithighlights/500-songs-by-name-ac/
| archivedate = April 22, 2007}}
</ref>  In 1992, it was inducted into the [[Blues Foundation]] Hall of Fame in the "Classics of Blues Recordings" category.<ref name="Blues Foundation">
{{cite web
| url = https://blues.org/blues_hof_inductee/baby-please-dont-go-big-joe-williams-bluebird-1935/
| last = O'Neal
| first = Jim
| authorlink = Jim O'Neal
| title = 1992 Hall of Fame Inductees: "Baby Please Don't Go" – Big Joe Williams (Bluebird 1935)
| website = The [[Blues Foundation]]
| year = 1992
| accessdate = April 26, 2015
| ref = harv
}}</ref> The Foundation noted that, in addition to various blues recordings, "the song was revived in revved-up fashion by rock bands in the '60s such as Them, the Amboy Dukes, and Ten Years After".<ref name="Blues Foundation"/>

==Notes==
'''Footnotes'''
{{Notelist|30em}}
'''Citations'''
{{Reflist|20em}}
'''References'''
*{{cite journal
| date = April 3, 2004
| author = Billboard
| title = 'Honk' if You Love Old Aerosmith
| journal = [[Billboard (magazine)|Billboard]]
| volume = 116
| issue = 14
| publisher = Nielsen Business Media
| issn = 0006-2510
| ref = harv}}
*{{cite book
| last = Birnbaum
| first = Larry
| title = Before Elvis: The Prehistory of Rock 'n' Roll
| year = 2012
| publisher = Scarecrow Press
| isbn = 978-0-8108-8629-2
| ref = harv}}
*{{cite book
| last = Bonomo
| first = Joe
| authorlink = Joe Bonomo
| title = AC/DC's Highway To Hell
| year = 2010
| publisher = Bloomsbury Publishing USA
| isbn = 978-1441141583
| ref = harv}}
*{{cite book
| last = Case
| first = George
| title = Jimmy Page: Magus, Musician, Man – An Unauthorized Biography
| year = 2007
| publisher = Hal Leonard
| isbn = 978-1-4234-0407-1
| ref = harv}}
*{{cite book
| last = Clayson
| first = Adam
| title = Led Zeppelin: The Origin of the Species
| year = 2006
| publisher = Chrome Dreams
| isbn = 1-84240-345-1
| ref = harv}}
*{{cite book
| last = Dahl
| first = Linda
| title = Stormy Weather
| year = 1984
| publisher = Proscenium Publishers
| isbn = 
| ref= harv}}
*{{cite AV media notes
| title = The Prewar Blues Story 1926–1943
| others = Various artists
| year = 1994
| last = Demetre
| first = Jacques
| type = CD compilation booklet
| publisher = Best of Blues
| id = Best of Blues 20
| OCLC = 874878605
| ref = harv}}
*{{cite AV media notes
| title = B.B. King: The Vintage Years
| others = [[B.B. King]]
| year = 2002
| last = Escott
| first = Colin
| type = Box set booklet
| publisher = Ace Records, Ltd
| id = Ace ABOXCD 8
| OCLC = 52004950
| ref = harv}}
*{{cite book
| last = Fink
| first = Jesse
| title = The Youngs: The Brothers Who Built AC/DC
| year = 2014
| publisher = St. Martin's Press
| isbn = 978-1466865204
| ref = harv}}
*{{cite encyclopedia
| last = Garon
| first = Paul
| editor-last = Komara
| editor-first = Edward
| encyclopedia = Encyclopedia of the Blues
| section = Baby Please Don't Go/Don't You Leave Me Here
| year = 2004
| location = New York City
| publisher = [[Routledge]]
| isbn = 978-1135958329
| ref = harv}}
*{{cite book
| last = Gioia
| first = Ted
| authorlink = Ted Gioia
| title = Delta Blues
| edition = Norton Paperback 2009
| location = New York City
| publisher = [[W. W. Norton]]
| year = 2008
| isbn = 978-0-393-33750-1
| ref = harv}}
*{{cite book
| last = Gordon
| first = Robert
| title = Can't Be Satisfied: The Life and Times of Muddy Waters
| publisher  = Little, Brown
| year = 2002
| isbn = 0-316-32849-9
| ref = harv}}
*{{cite book
| author = Hal Leonard
| title = The Blues
| section = Baby Please Don't Go
| year = 1995
| location = Milwaukee, Wisconsin
| publisher = [[Hal Leonard Corporation|Hal Leonard]]
| isbn = 0-79355-259-1
| ref = harv}}
*{{cite encyclopedia
| last = Herzhaft
| first = Gerard
| encyclopedia = Encyclopedia of the Blues
| section = Baby, Please Don't Go
| year = 1992
| location = Fayetteville, Arkansas
| publisher = [[University of Arkansas Press]]
| isbn = 1-55728-252-8
| ref = harv}}
*{{cite book
| last = Marcus
| first = Greil
| authorlink = Greil Marcus
| title = When That Rough God Goes Riding: Listening to Van Morrison
| year = 2010
| publisher = PublicAffairs
| isbn = 978-1-58648-821-5
| ref = harv}}
*{{cite book
| last = Miller
| first = Heather
| title = AC/DC: Hard Rock Band
| year = 2009
| publisher = Enslow Publishers
| isbn = 978-0766030312
| ref = harv}}
*{{cite book
| last = Murray
| first = Charles Shaar
| authorlink = Charles Shaar Murray
| title = Boogie Man: The Adventures of John Lee Hooker in the American Twentieth Century
| publisher = Macmillan
| year = 2002
| isbn = 978-0-312-27006-3
| ref = harv}}
*{{cite AV media notes
| title = Muddy Waters: Chess Box
| others = [[Muddy Waters]]
| year = 1989
| last = Palmer
| first = Robert
| type = Box set booklet
| location = Universal City, California
| publisher = [[Chess Records]]/[[MCA Records]]
| id = CHD3-80002
| oclc = 154264537
| ref = harv}}
*{{cite book
| last = Perkins
| first = Jeff
| title = AC/DC – Uncensored on the Record
| year = 2011
| publisher = Coda Books
| isbn = 978-1908538543
| ref = harv}}
*{{cite book
| last = Rogan
| first = Johnny
| authorlink = Johnny Rogan
| title = Van Morrison: No Surrender
| year = 2006
| publisher = Random House
| isbn = 978-0-09-943183-1
| ref = harv}}
*{{cite book
| last = Strong
| first = Martin C.
| authorlink = Martin C. Strong
| title = The Great Rock Discography
| year = 2002
| publisher = Canongate Publishing
| isbn = 978-1-84195-312-0
| ref = harv}}
*{{cite book
| last = Thompson
| first = Gordon
| title = Please Please Me: Sixties British Pop, Inside Out
| year = 2008
| publisher = Oxford University Press
| isbn = 978-0-19-533318-3
| ref = harv}}
*{{cite book
| last = Walker
| first = Clinton
| authorlink = Clinton Walker
| title = Highway to Hell: The Life and Death of AC/DC Legend Bon Scott
| year = 2011
| publisher = Verse Chorus Press
| isbn =  978-1891241864
| ref = harv}}
*{{cite book
| last = Wall
| first = Mick
| authorlink = Mick Wall
| title = AC/DC: Hell Ain't a Bad Place to Be
| year = 2013
| publisher = Macmillan
| isbn = 978-1250038753
| ref = harv}}
*{{cite book
| last = Whitburn
| first = Joel
| authorlink = Joel Whitburn
| title = Top R&B Singles 1942–1988
| year = 1988
| location = Menomonee  Falls, Wisconsin
| publisher = [[Record Research]]
| isbn = 0-89820-068-7
| page = 
| ref = harv}}

{{Muddy Waters}}
{{Them (Band)}}
{{Aerosmith singles}}

{{DEFAULTSORT:Baby, Please Don't Go}}
[[Category:Year of song unknown]]
[[Category:Blues songs]]
[[Category:1949 singles]]
[[Category:John Lee Hooker songs]]
[[Category:1953 singles]]
[[Category:Muddy Waters songs]]
[[Category:1964 singles]]
[[Category:Them (band) songs]]
[[Category:2004 singles]]
[[Category:AC/DC songs]]
[[Category:Aerosmith songs]]
[[Category:Deram Records singles]]
[[Category:Decca Records singles]]
[[Category:Parrot Records singles]]
[[Category:Songwriter unknown]]