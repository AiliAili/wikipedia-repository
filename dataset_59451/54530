{{italic title}}
[[File:Canadian Crusoes.jpg|thumb|right|upight|First edition title page]]
'''''Canadian Crusoes: A Tale of the Rice Lake Plains''''' is a novel by [[Catharine Parr Traill]] published in 1852, considered the first Canadian novel for children.{{sfn|Phillips|2013|p=41}}  Written after ''[[The Backwoods of Canada]]'' (1836), it is Traill's second Canadian book. It was first published in 1852 by [[London]] publisher Arthur Hall, Virtue, and Company. It was edited by her sister [[Agnes Strickland]].

==Background==

[[File:CatherineParrTraillcrop.png|thumb|alt=|[[Catharine Parr Traill]] in late life]]

[[Catharine Parr Traill]] (1802–1899) was an English-born Canadian writer best known for he botanical book ''Canadian Wild Flowers'' (1865){{sfn|Phillips|2013|p=41}} as well as ''The Backwoods of Canada'' (1836) and ''The Canadian Settler's Guide'' (1855), works intended for an audience of potential English emigrants;{{efn|''The Canadian Settler's Guide'' also appeared under the title ''The Female Emigrant's Guide, and Hints on Canadian Housekeeping''.}}  Traill also published a large amount of fiction, beginning when she still lived in England{{sfn|Thompson|1991|p=9}} and written in a didactive style.{{sfn|Thompson|1991|p=10}}

Traill's Canadian works were amongst the first to have a Canadian setting and ''Canadian Crusoes'' was the first to feature a Canadian-born protagonist<!-- I imagine this will be contentious—let's find a better wording rather than simply delete it -->.{{sfn|Thompson|1991|p=10}}  Her sister and fellow emigrant [[Susanna Moodie]] also published fiction, but in an English setting following English conventions.{{sfn|Thompson|1991|p=10}}

==Summary==

The work is set in what is today central southern [[Ontario]], just south of [[Rice Lake (Ontario)|Rice Lake]], where three children become lost and must fend for themselves. Drawing from its namesake, Daniel Defoe's 1719 novel ''[[Robinson Crusoe]]'', the novel sets out to show that these children, two English Canadian and one French Canadian, are able to work together to survive in the new world of Canada. This spirit of cooperation is emphasized by the fact that the children later meet a [[Mohawk nation|Mohawk]] girl who joins their group and is able to help them with her own skills.

By the end of the novel, the children escape from the Canadian wilderness and are paired off - the English Canadian boy with the Mohawk girl and the French Canadian boy with the English Canadian girl.  Their skills are all useful, and they must work together to survive. Metaphorically, their cooperation suggests the activity of peaceful nation-building. However, the English Canadian ethic is still privileged over the other views.

==Notes==

{{Notelist}}

==References==

{{Reflist|colwidth=20em}}

===Works cited===

{{Refbegin|colwidth=40em}}

* {{cite book
|last      = Phillips
|first     = Richard
|title     = Mapping Men and Empire: Geographies of Adventure
|url       = https://books.google.com/books?id=OczWAQAAQBAJ
|year      = 2013
|publisher = Routledge
|isbn      = 978-1-135-63656-2
|ref       = harv}}
* {{cite book
|last      = Thompson
|first     = Elizabeth
|title     = Pioneer Woman: A Canadian Character Type
|url       = https://books.google.com/books?id=_BDTlSadu3kC
|year      = 1991
|publisher = [[McGill-Queen's University Press]]
|isbn      = 978-0-7735-0832-3
|ref       = harv}}

{{Refend}}

==Further==

* {{cite book
|last          = Bigot
|first         = Corinne
|chapter       = Interracial Relations: (Re)writing the Stereotype: Pauline Johnson, Catharine Parr Traill and Susanna Moodie
|editor1-last  = Besson
|editor1-first = Françoise
|editor2-last  = Omhovère
|editor2-first = Claire
|editor3-last  = Ventura
|editor3-first = Héliane
|title         = The Memory of Nature in Aboriginal, Canadian and American Contexts
|url           = https://books.google.com/books?id=OMkxBwAAQBAJ&pg=PA176
|year          = 2014
|publisher     = Cambridge Scholars Publishing
|isbn          = 978-1-4438-6161-8
|pages         = 176–191}}
* {{cite book
|last         = Copeland
|first        = David
|chapter      = Finding the Words: Form and Language in Canadian Crusoes
|editor-last1 = Chambers
|editor-first = Jennifer
|title        = Diversity and Change in Early Canadian Women’s Writing
|url          = https://books.google.com/books?id=tPQZBwAAQBAJ&pg=PA22
|year         = 2009
|publisher    = Cambridge Scholars Publishing
|isbn         = 978-1-4438-1505-5
|pages        = 22–35}}
* {{cite book
|last         = O'Malley
|first        = Andrew
|chapter      = Island Homemaking: Catharine Parr Traill's Canadian Crusoes and the Robinsonade Tradition
|editor-last  = Reimer
|editor-first = Mavis
|title        = Home Words: Discourses of Children’s Literature in Canada
|url          = https://books.google.com/books?id=0tffAgAAQBAJ&pg=PA67
|year         = 2009
|publisher    = [[Wilfrid Laurier University Press]]
|isbn         = 978-1-55458-772-8
|pages        = 67–86}}

==External links==
* {{Gutenberg|no=8382|name=Canadian Crusoes}}

{{Portal bar|Canada|Literature}}

{{Robinson Crusoe}}

[[Category:1852 novels]]
[[Category:Novels set in Ontario]]
[[Category:19th-century Canadian novels]]


{{1870s-novel-stub}}
{{Canada-novel-stub}}