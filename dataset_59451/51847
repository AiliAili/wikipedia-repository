{{Infobox journal
| title = Journal of Enzyme Inhibition and Medicinal Chemistry
| cover = [[File:Journal of Enzyme Inhibition and Medicinal Chemistry.jpg|150px]]
| editor = Claudiu T. Supuran
| discipline = [[Pharmacology]], [[pharmacy]], [[toxicology]]
| abbreviation = J. Enzyme Inhib. Med. Chem.
| publisher = [[Informa]]
| country =
| frequency = Bimonthly
| history = 1985-present
| openaccess =
| license =
| impact =  2.332
| impact-year = 2014
| website = http://www.informapharmascience.com/enz
| link1 = http://informahealthcare.com/toc/enz/current
| link1-name = Online access
| link2 = http://informahealthcare.com/loi/enz
| link2-name = Online archive
| JSTOR =
| OCLC = 50012503
| LCCN = 2002243339
| CODEN =
| ISSN = 1475-6366
| eISSN = 1475-6374
 }}
The '''''Journal of Enzyme Inhibition and Medicinal Chemistry''''' is a bimonthly[[Peer review|peer-reviewed]] [[medical journal]] that covers research on [[enzyme inhibitor]]s and inhibitory processes as well as agonist/antagonist receptor interactions in the development of medicinal and anti-cancer agents. The [[editor-in-chief]] is H. John Smith ([[Cardiff University]]).

== Abstracting and indexing ==
The ''Journal of Enzyme Inhibition and Medicinal Chemistry'' is abstracted and indexed in:
{{columns-list|2|
* [[Current Contents]]/Life Sciences
* [[Science Citation Index]]
* [[BIOSIS Previews|BIOSIS]]
* [[Biotechnology Citation Index]]
* [[Elsevier BIOBASE|Elsevier BIOBASE/Current Awareness in Biological Sciences]]
* [[Chemical Abstracts Service]]
* [[EMBASE]]/[[Excerpta Medica]]
* [[Index Medicus]]/[[MEDLINE]]
* [[Scopus]]
}}

== External links ==
* {{Official|http://www.informapharmascience.com/enz}}

{{DEFAULTSORT:Journal Of Enzyme Inhibition And Medicinal Chemistry}}

[[Category:Medicinal chemistry journals]]
[[Category:Enzyme inhibitors]]
[[Category:Publications established in 1985]]
[[Category:Bimonthly journals]]