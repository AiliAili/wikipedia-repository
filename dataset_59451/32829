{{featured article}}

{{Use dmy dates|date=September 2014}}
{{EngvarB|date=August 2014}}
{{Infobox company
|name=Oriental Film
|logo=Oriental Film Company logo (1941).jpg{{!}}alt=Black and white text, reading "Oriental Film Company". There is a picture of an elephant
|caption=Oriental logo, from an advertisement for ''[[Panggilan Darah]]''
| image            = Algemeen Nederlandsch Indisch Filmsyndicaat (ANIF) studio, Batavia.jpg{{!}}alt=A black-and-white photograph of a studio building
| image_caption    = The ANIF studios, which Oriental used from late 1940 until the company closed in 1941
|type= Private
|foundation= [[Jakarta|Batavia]], [[Dutch East Indies]] ({{Start date|df=yes|1940||}})
|defunct={{End date|1941}} (dissolved)
|fate=
| key_people       = {{plain list|
* Tjo Seng Han (owner)
* Tjan Hock Siong (manager)
}}
|intl=yes
|location= Batavia, Dutch East Indies
|area_served= Dutch East Indies
|industry= Film
|products=
}}
'''Oriental Film''' was a film production company in [[Batavia, Dutch East Indies|Batavia]], [[Dutch East Indies]] (now [[Jakarta]], Indonesia). Established by [[Chinese Indonesians|ethnic Chinese]] businessman Tjo Seng Han in 1940, it completed four [[black-and-white]] films before it was closed in 1941. All the company's films were screened into the 1950s but may now be [[lost film|lost]]. They were directed by two men, [[Njoo Cheong Seng]] and [[Sutan Usman Karim]], and launched the careers of actors such as [[Dhalia]] and [[Soerip]].

Established during the revival of the Indies film industry, Oriental released its first film, ''[[Kris Mataram]]'', in July 1940. It starred Njoo's wife [[Fifi Young]], and relied on her fame as a stage actress to draw audiences. This was followed by a further three films, which were targeted at low-income audiences and extensively used ''[[kroncong]]'' music. Their final production was ''[[Panggilan Darah]]'' in 1941, which was completed after Njoo and Young had migrated to [[Majestic Film]]. Oriental was unable to recoup its expenses of renting a Dutch-owned studio, and the company was shut down.

==Establishment==
Following the commercial successes of ''[[Terang Boelan]]'' (''Full Moon''; 1937), ''[[Fatima (1938 film)|Fatima]]'' (1938), and ''[[Alang-Alang (film)|Alang-Alang]]'' (''Grass''; 1939), the [[Dutch East Indies]] film industry&nbsp;– which had been severely weakened by the [[Great Depression]]&nbsp;– was revived. Film production increased and, in 1940, four new [[production house]]s were opened, including Oriental Film.{{sfn|Biran|2009|p=205}} Funded entirely by the [[Chinese Indonesians|ethnic Chinese]] businessman Tjo Seng Han, the company's first headquarters were at 42 Matraman Street, in eastern [[Jakarta|Batavia]] (now Jakarta);{{sfn|Biran|2009|p=228}} according to the weekly ''Sin Po'', this studio had simple facilities.{{sfn|Khoe 1940, Industrie|p=10}} Another ethnic Chinese businessman, Tjan Hock Siong, was brought on to manage the day-to-day activities of the studio.{{sfn|Biran|2009|p=228}}

Tjo and Tjan hired [[Njoo Cheong Seng]], a dramatist who had previously worked with the [[Miss Riboet's Orion|Orion Opera]] before establishing his own troupe, and his wife [[Fifi Young]]. The two had gained wide recognition through their stage work, and it was hoped that name recognition would bring in audiences.{{sfn|Biran|2009|p=228}} The hiring of Njoo and Young was part of a trend of bringing theatrically trained actors and crew into the film industry. ''Terang Boelan'' had used stage starlet [[Roekiah]] and her husband [[Kartolo]] to great effect, and the actors had brought similar financial success to [[Tan's Film]] after they were hired.{{efn|Java Industrial Film would later employ [[Astaman]], [[Ratna Asmara]], and [[Andjar Asmara]] of [[Dardanella (theatre company)|Dardanella]] beginning with their 1941 production ''[[Kartinah]]'' {{harv|Biran|2009|p=214}}.}}{{sfn|Biran|2009|p=204}}

==Productions==
[[File:Kris Mataram 1940.jpg|thumb|alt=A square-shaped advertisement for the film ''Kris Mataram''; it includes the title twice and various pieces of information regarding the film.|Advertisement for Oriental's first production, ''[[Kris Mataram]]'' (1940)]]
Oriental's first production, ''[[Kris Mataram]]'' (''Kris of Mataram''), was directed by Njoo and starred Young and Omar Rodriga. It followed a young noblewoman (played by Young) who marries a nobleman despite her parents' disapproval.{{sfn|Filmindonesia.or.id, Kris Mataram}} For this film, Njoo drew [[Wong brothers|Joshua Wong]] from Tan's as cinematographer, then used the Wong name as part of his advertising: "[''Kris Mataram'' has] the J. Wong guarantee".{{efn|Original: "''J. WONG's garantie''".}}{{sfn|Biran|2009|p=228}} Released in July 1940, the film was targeted at low-income audiences&nbsp;– particularly theatre-goers who would recognise Young.{{sfn|Biran|2009|p=250}} A review in the ''[[Soerabaijasch Handelsblad]]'' praised it, calling ''Kris Mataram'' "captivating to the last metre".{{efn|Original: "''... boeiend tot den laatsten meter.''"}}{{sfn|''Soerabaijasch Handelsblad'' 1940, Kris Mataram}}

Before its studio's second production, Oriental began renting the studios of Algemeen Nederlandsch Indisch Filmsyndicaat (ANIF) in Molenvliet, Batavia,{{efn|This area was around what is now Gajah Mada and Hayam Wuruk Streets {{harv|Setiogi 2003, 'Molenvliet'}}.}} for 1,500 [[Netherlands Indies gulden|gulden]] a month. This rent also included access to the studio's equipment and cameraman J.J.W. Steffans, as well as facilities such as air conditioning and telephones in each office, and lighting equipment for night-time shots.<ref>{{harvnb|Biran|2009|p=228}}; {{harvnb|Khoe 1940, Industrie|pp=10–11}}.</ref> A large adjacent plot of land was also included. By renting the ANIF complex, Oriental became the largest and most modern studio in the Indies.{{sfn|Khoe 1940, Industrie|pp=10–11}}

Njoo soon showed a proclivity for sensation, which was manifested in the December 1940 release ''[[Zoebaida]]''.{{sfn|''De Indische courant'' 1940, Sampoerna}} For the film, a love story set in [[Timor]] starring Young as the title character, Oriental used bright, extravagant costumes; Njoo gave the characters whimsical names which would not be found in the setting.{{sfn|Biran|2009|p=254}} Rather than shoot on location&nbsp;– which would have been prohibitively expensive&nbsp;– Oriental constructed sets behind the ANIF Studio.{{sfn|Khoe 1940, Industrie|p=10}}  Reviewers of the film noted with disdain that ''Zoebaida'' was exaggerated and clearly reflected its stage influences.<ref>{{harvnb|Biran|2009|p=254}}; {{harvnb|''De Indische courant'' 1940, Sampoerna}}.</ref>

Oriental released its third production, ''[[Pantjawarna]]'' (''Five Colours''), in March 1941.{{sfn|''Soerabaijasch Handelsblad'' 1941, Sampoerna}} Again starring Young, the film&nbsp;– in which a young woman must raise two daughters despite her husband's imprisonment&nbsp;– featured two new hires, [[Dhalia]] and [[Soerip]]. Both women, teenagers at the time, had established stage careers and were known for their singing voices, which were put to use in several of ''Pantjawarna''{{'s}} twelve ''[[kroncong]]'' songs.<ref>{{harvnb|Biran|1979|pp=115, 480}}; {{harvnb|Biran|2009|p=229}}.</ref> This film was well received by critics, and Young's acting was praised in both the ''[[Bataviaasch Nieuwsblad]]'' and ''Soerabaijasch Handelsblad'';<ref>{{harvnb|''Bataviaasch Nieuwsblad'' 1941, Rex}}; {{harvnb|''Soerabaijasch Handelsblad'' 1941, Sampoerna}}.</ref> the latter characterised ''Pantjawarna'' as "a success for O.F.C. [Oriental Film Company] and proof of the progress made in the cinema of the Indies".{{efn|Original: "''... een succes aangemerkt worden voor de O. F. C. en een bewijs van vooruitgang voor de Indische filmindustrie''".}}{{sfn|''Soerabaijasch Handelsblad'' 1941, Sampoerna}}

After ''Pantjawarna'', [[Fred Young (director)|Fred Young]] drew Njoo and his wife to the newly established [[Majestic Film]].{{efn|Fred and Fifi Young were unrelated {{harv|Biran|2009|p=239}}. Fifi Young had been born Nonie Tan, and taken up her stage name at Njoo's urging: [[Yang (surname)|Young]] was the [[Mandarin Chinese|Mandarin]] equivalent of Njoo's [[Hokkien]] surname, while Fifi was meant to be reminiscent of the French actress [[Fifi D'Orsay]] {{harv|TIM, Fifi Young}}.}}{{sfn|Biran|2009|p=229}} Deprived of their director and main star, Oriental hired the journalist Sutan Usman Karim{{efn|Commonly known as Suska {{harv|Biran|2009|p=216}}.}} to direct their fourth production, ''[[Panggilan Darah]]'' (''Call of Blood''). This film, written by Karim and starring Dhalia and Soerip, told of two young orphans as they tried to eke a living in Batavia.<ref>{{harvnb|Filmindonesia.or.id, Panggilan Darah}}; {{harvnb|''Pertjatoeran Doenia dan Film'' 1941, Warta dari Studio|p=18}}.</ref> This film, which debuted in June 1941, prominently featured the cigarette factory Nitisemito, leading the Indonesian film historian [[Misbach Yusa Biran]] to suggest that it may have paid for the advertisement.{{sfn|Biran|2009|p=270}} He records it as a modest success, although he notes that reviews were mixed.{{sfn|Biran|2009|pp=229, 270}}

==Closure and legacy==
{{multiple image
| footer    = Both [[Dhalia]] (left) and [[Soerip]] continued acting until the 1990s.
| image1    = Dhalia Film Varia Nov 1953 p18.jpg
| alt1      = Dhalia
| width1    = 170

| image2    = Miss Soerip, 1941.jpg
| alt2      = Soerip
| width2= 170
}}
Following ''Panggilan Darah'', Oriental&nbsp;– which had been losing money steadily&nbsp;– released its contract for the ANIF studio, which was taken over by the Dutch-run Multi Film. Despite hopes that they would continue producing [[fictional film|narrative films]], perhaps with less modern equipment, the company was dissolved.<ref>{{harvnb|''Pertjatoeran Doenia dan Film'' 1941, Tanja Djawab|p=52}}; {{harvnb|Biran|2009|p=229}}.</ref> Oriental's actors and crew members migrated to different studios.{{sfn|''Pertjatoeran Doenia dan Film'' 1941, Tanja Djawab|p=52}} Suska was signed to Java Industrial Film and directed a single film for them, ''[[Ratna Moetoe Manikam]]''.{{sfn|Filmindonesia.or.id, Suska}} Dhalia went to Populair's Film and acted in one production, ''[[Moestika dari Djemar]]'' (''The Jewel of Djemar''; 1942), before the [[Japanese occupation of the Dutch East Indies|Japanese occupation]] in March 1942 closed that studio.<ref>{{harvnb|Biran|2009|pp=319, 332}}; {{harvnb|Filmindonesia.or.id, Dhalia}}.</ref> Soerip, meanwhile, joined Njoo and Young at Majestic Film, acting in two productions before that studio was closed.<ref>{{harvnb|Biran|2009|pp=319, 332}}; {{harvnb|Filmindonesia.or.id, Soerip}}.</ref>

Although Oriental was short-lived, several of the actors and crew it hired went on to lengthy careers. Njoo, after handling two films for Majestic in 1941, spent much of the decade in theatre before returning to directing in the mid-1950s.{{sfn|Filmindonesia.or.id, Njoo Cheong Seng}} Fifi Young, who continued acting for Njoo until their divorce in 1945, appeared in more than eighty films before her death in 1975.{{sfn|Filmindonesia.or.id, Fifi Young}} Dhalia and Soerip likewise had lengthy careers: both acted until the 1990s, Dhalia in 52 productions and Soerip in 25.<ref>{{harvnb|Filmindonesia.or.id, Dhalia}}; {{harvnb|Filmindonesia.or.id, Soerip}}.</ref>

==Filmography==
In a period of two years, Oriental released four films; all were feature length, made in black-and-white, and received wide releases in the Dutch East Indies.{{sfn|Biran|2009|p=229}} Some, such as ''Panggilan Darah'', enjoyed international release; the film was screened in [[Singapore]] (then part of the [[Straits Settlements]]) by September 1941.{{sfn|''The Straits Times'' 1941, (untitled)}} The company's productions were targeted at low-income audiences and extensively used ''kroncong'' music, for the recording of which the company established the Oriental Novelty Five.<ref>{{harvnb|Biran|2009|pp=228–229}}; {{harvnb|Filmindonesia.or.id, Zoebaida}}.</ref> Though its films were screened at least into the 1950s,{{efn|For instance, ''Zoebaida'' played as late as October 1947 {{harv|''Pelita Rakjat'' 1947, (untitled)}} and ''Panggilan Darah'' was screened until at least August 1952 {{harv|''Algemeen Indisch Dagblad'' 1952, Cinemas}}.}} Oriental's output may be lost.{{efn|Movies in the Indies were recorded on highly flammable [[nitrate film]], and after a fire destroyed much of [[Produksi Film Negara]]'s warehouse in 1952, many old films shot on nitrate were deliberately destroyed to prevent further such mishaps {{harv|Biran|2012|p=291}}. As such, the American visual anthropologist [[Karl G. Heider]] suggests that all Indonesian films from before 1950 are lost.{{harv|Heider|1991|p=14}} However, J.B. Kristanto's ''Katalog Film Indonesia'' records several as having survived at [[Sinematek Indonesia]]'s archives, and Biran writes that several Japanese propaganda films have survived at the [[Netherlands Government Information Service]] {{harv|Biran|2009|p=351}}.}}

{{Div col||30em}}
*1940: ''[[Kris Mataram]]''
*1940: ''[[Zoebaida]]
*1941: ''[[Pantjawarna]]
*1941: ''[[Panggilan Darah]]''
{{Div col end}}

==Explanatory notes==
{{notelist}}

==References==
{{reflist|20em}}

==Works cited==
{{refbegin|40em}}
* {{Cite book
 |title=Apa Siapa Orang Film Indonesia 1926–1978
 |trans_title=What and Who: Film Figures in Indonesia, 1926–1978
 |year=1979
 |oclc=6655859
 |ref=harv
 |language=Indonesian
 |location=Jakarta
 |publisher=Sinematek Indonesia
 |editor-last=Biran
 |editor-first=Misbach Yusa
}}
* {{cite book
 |title=[[Sejarah Film 1900–1950: Bikin Film di Jawa]]
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
* {{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |publisher=Ministry of Education and Culture
 |location=Jakarta
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
* {{cite news
 |title=Cinemas
 |language=Dutch
 |date=15 August 1952
 |page=4
 |work=Algemeen Indisch Dagblad de Preangerbode
 |url=http://resolver.kb.nl/resolve?urn=ddd:010896108:mpeg21:a0080
 |accessdate=22 December 2015
 |ref={{sfnRef|''Algemeen Indisch Dagblad'' 1952, Cinemas}}
}}
* {{cite web
 |url=http://www.tamanismailmarzuki.com/tokoh/young.html
 |title=Fifi Young
 |language=Indonesian
 |publisher=[[Taman Ismail Marzuki]]
 |accessdate=19 August 2012
 |archivedate=19 August 2012
 |archiveurl=http://www.webcitation.org/6A1rPunZW
  |location=Jakarta
 |ref={{sfnRef|TIM, Fifi Young}}
}}
* {{cite web
  |url=http://filmindonesia.or.id/movie/name/nmp4bc420b30bec7_dhalia/award
  |title=Filmografi Dhalia
  |trans_title=Filmography for Dhalia
  |language=Indonesian
  |work=filmindonesia.or.id
  |publisher=Konfiden Foundation
  |accessdate=13 October 2013
  |archivedate=13 October 2013
  |archiveurl=http://www.webcitation.org/6KKPLqLGD
  |location=Jakarta
  |ref={{sfnRef|Filmindonesia.or.id, Dhalia}}
}}
* {{cite web
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bad37ea4e9_njoo-cheong-seng/filmography
 |title=Filmografi Njoo Cheong Seng
 |trans_title=Filmography for Njoo Cheong Seng
 |language=Indonesian
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |ref={{sfnRef|Filmindonesia.or.id, Njoo Cheong Seng}}
 |accessdate=3 September 2012
 |archivedate=3 September 2012
 |location=Jakarta
 |archiveurl=http://www.webcitation.org/6AObWmjHW
}}
* {{cite web
 |title=Filmografi Nonie Tan
 |trans_title=Filmography for Nonie Tan
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmp4bc303aa2c057_fifi-young/filmography
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |accessdate=19 August 2012
 |archivedate=19 August 2012
 |archiveurl=http://www.webcitation.org/6A1oQeHXi
 |location=Jakarta
 |ref={{sfnRef|Filmindonesia.or.id, Fifi Young}}
}}
* {{cite web
  | title = Filmografi Soerip
  |trans_title=Filmography for Soerip
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4bc304244f2ef_soerip/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | accessdate = 30 March 2013
  | archiveurl = http://www.webcitation.org/6FVRSMxpH
  | archivedate = 30 March 2013
  | ref = {{sfnRef|Filmindonesia.or.id, Soerip}}
  |location=Jakarta
  }}
* {{cite web
  | title = Filmografi Suska
  | trans_title = Filmography for Suska
  | language = Indonesian
  | location=Jakarta
  | url = http://filmindonesia.or.id/movie/name/nmp4b9bad3bc3bf9_suska/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | accessdate = 23 September 2012
  | archiveurl = http://www.webcitation.org/6AtNwxqoi
  | archivedate = 23 September 2012
  | ref = {{sfnRef|Filmindonesia.or.id, Suska}}
  }}
* {{cite book
 |url=https://books.google.com/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{Cite news
 |title=Industrie Film di Indonesia
 |trans_title=The Film Industry in Indonesia
 |language=Indonesian
 |last=Khoe
 |first=Tjeng Liem
 |work=Sin Po
 |location=Batavia
 |date=19 October 1940
 |pages=9–11
 |volume=18
 |issue=916
 |ref={{SfnRef|Khoe 1940, Industrie}}
}}
* {{cite web
 |title=Kris Mataram
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-k011-40-802576_kris-mataram
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=25 July 2012
 |archiveurl=http://www.webcitation.org/69QFQXype
 |archivedate=25 July 2012
 |ref={{sfnRef|Filmindonesia.or.id, Kris Mataram}}
}}
* {{Cite news
 |title='Kris Mataram' Heden in Sampoerna
 |trans_title='Kris Mataram' Today in Sampoerna
 |url=http://resolver.kb.nl/resolve?urn=ddd:011122551:mpeg21:a0108
 |language=Dutch
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |date=8 July 1940
 |page=6
 |ref={{SfnRef|''Soerabaijasch Handelsblad'' 1940, Kris Mataram}}
}}
* {{cite web
  | title = Panggilan Darah
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-p014-41-201804_panggilan-darah
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 26 July 2012
  | archiveurl = http://www.webcitation.org/69QzGQMNi
  | archivedate = 26 July 2012
  | ref = {{sfnRef|Filmindonesia.or.id, Panggilan Darah}}
  }}
* {{Cite news
 |title=Rex: Pantjawarna
 |url=http://resolver.kb.nl/resolve?urn=ddd:011221691:mpeg21:a0127
 |language=Dutch
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |date=19 March 1941
 |page=5
 |ref={{SfnRef|''Bataviaasch Nieuwsblad'' 1941, Rex}}
}}
* {{Cite news
 |title=Sampoerna: Pantjawarna
 |url=http://resolver.kb.nl/resolve?urn=ddd:011122658:mpeg21:a0171
 |language=Dutch
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |date=27 March 1941
 |page=6
 |ref={{SfnRef|''Soerabaijasch Handelsblad'' 1941, Sampoerna}}
}}
* {{cite news
 |title=Sampoerna: 'Zoebaida'
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011176396%3Ampeg21%3Ap002%3Aa0069
 |work=De Indische courant
 |accessdate=30 March 2013
 |publisher=Kolff & Co
 |date=21 December 1940
 |location=Surabaya
 |ref={{sfnRef|''De Indische courant'' 1940, Sampoerna}}
 |page=2
}}
* {{cite news
 |title='Molenvliet' Marked Batavia's Golden Age
 |url=http://www.thejakartapost.com/news/2003/04/23/039molenvliet039-marked-batavia039s-golden-age.html
 |work=The Jakarta Post
 |accessdate=9 August 2014
 |date=23 April 2003
 |location=Jakarta
 |last=Setiogi
 |first=Sari P.
 |archiveurl=https://web.archive.org/web/20140727211931/http://www.thejakartapost.com/news/2003/04/23/039molenvliet039-marked-batavia039s-golden-age.html
 |archivedate=27 July 2014
 |ref={{sfnRef|Setiogi 2003, 'Molenvliet'}}
}}
* {{Cite journal
 |title=Tanja Djawab
 |trans_title=Questions and Answers
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=June 1941
 |pages=52–53
 |volume=1
 |issue=4
 |location=Batavia
 |ref={{sfnRef|''Pertjatoeran Doenia dan Film'' 1941, Tanja Djawab|p=52}}
}}
* {{cite news
 |title=(untitled)
 |language=Indonesian
 |work=Pelita Rakjat
 |date=15 October 1947
 |url=http://niod.x-cago.com/maleise_kranten/article.do?code=Niod059&date=19471015&id=059-19471015-002040&words=Zoebaida%20zoebaida
 |page=2
 |location=Surabaya
 |accessdate=9 August 2014
 |ref={{sfnRef|''Pelita Rakjat'' 1947, (untitled)}}
}}
* {{cite news
 |title=(untitled)
 |date=21 September 1941
 |page=4
 |work=The Straits Times
 |url=http://newspapers.nl.sg/Digitised/Article/straitstimes19410918-1.2.15.4.aspx
 |accessdate=23 May 2013
 |location=Singapore
 |ref={{sfnRef|''The Straits Times'' 1941, (untitled)}}
}}
* {{Cite journal
 |title=Warta dari Studio
 |trans_title=Reports from the Studios
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=June 1941
 |pages=18–20
 |volume=1
 |issue=1
 |location=Batavia
 |ref={{sfnRef|''Pertjatoeran Doenia dan Film'' 1941, Warta dari Studio|p=18}}
}}
* {{cite web
  | title = Zoebaida
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-z008-40-792367_zoebaida
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 25 July 2012
  | archiveurl = http://www.webcitation.org/69QFzlQnC
  | archivedate = 25 July 2012
  | ref = {{sfnRef|Filmindonesia.or.id, Zoebaida}}
  }}
{{refend}}
{{Portal bar|Companies|Film|Indonesia|Netherlands}}

[[Category:1940 establishments in the Dutch East Indies]]
[[Category:1941 disestablishments in the Dutch East Indies]]
[[Category:Companies established in 1940]]
[[Category:Companies disestablished in 1941]]
[[Category:Defunct companies of the Dutch East Indies]]
[[Category:Film production companies of the Dutch East Indies]]
[[Category:Media in Batavia]]