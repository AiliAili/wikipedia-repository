<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name= Olympus 
 |image= File:Bristol Olympus.jpg
 |caption= Preserved Bristol Siddeley Olympus Mk 301 Engine Change Unit (ECU) complete with ancillaries and bulkheads.
}}
{{Infobox aircraft engine
 |type= [[Turbojet]]
 |national origin = [[United Kingdom]]
 |manufacturer= [[Bristol Aero Engines]]<br>[[Bristol Siddeley|Bristol Siddeley Engines Limited]]<br>[[Rolls-Royce Limited|Rolls-Royce Bristol Engine Division]]
 |first run= 1950
 |major applications= [[Avro Vulcan]] <br> [[BAC TSR-2]]
 |number built =
 |program cost =
 |unit cost =
 |developed from =
 |variants with their own articles =
 |developed into = [[Rolls-Royce/Snecma Olympus 593]] <br> [[Rolls-Royce Marine Olympus]]
}}
|}

The '''Rolls-Royce Olympus''' (originally the '''Bristol B.E.10 Olympus''') was the world's first two-spool [[axial-flow compressor|axial-flow]] [[turbojet]] aircraft engine design,<ref>{{cite web|title=The Rolls-Royce Olympus Aircraft Engine|url=http://www.airpowerworld.info/aircraft-engine-manufacturers/rolls-royce-olympus.htm|publisher=Air Power World|accessdate=13 September 2016}}</ref><ref>{{cite web|title=Rolls-Royce Olympus|url=http://www.gatwick-aviation-museum.co.uk/engines/olympus.html|publisher=Gatwick Aviation Museum|accessdate=13 September 2016}}</ref> dating from November 1946,<ref>Baxter 2012, p. 16</ref><ref>http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201748.html</ref> although not the first to run or enter service. It was originally developed and produced by [[Bristol Aero Engines]]. First running in 1950,<ref>Baxter 2012, p. 20</ref> its initial use was as the powerplant of the [[Avro Vulcan]] [[V bomber]]. The design was further developed for [[supersonic]] performance as part of the [[BAC TSR-2]] programme. Later it saw production as the [[Rolls-Royce/Snecma Olympus 593]], the powerplant for [[Concorde]] [[Supersonic transport|SST]]. Versions of the engine were licensed to [[Curtiss-Wright]] in the US as the TJ-32 or J67 (military designation) and the TJ-38 'Zephyr'. The Olympus was also developed with success as [[Rolls-Royce Marine Olympus|marine]] and industrial [[gas turbine]]s.

Bristol Aero Engines (formerly Bristol Engine Company) merged with [[Armstrong Siddeley]] Motors in 1959 to form [[Bristol Siddeley]] Engines Limited (BSEL), which in turn was taken over by [[Rolls-Royce Limited|Rolls-Royce]] in 1966.

As of 2012, the Olympus remains in service as both a marine and industrial gas turbine.

==Background==

===Origins===

At the end of World War II, the Bristol Engine Company’s major effort was the development of the [[Bristol Hercules|Hercules]] and [[Bristol Centaurus|Centaurus]] [[radial engine|radial]] [[Reciprocating engine|piston]] engines. By the end of 1946, the company had only 10 hours of [[turbojet]] experience with a small experimental engine called the Phoebus which was the [[gas generator]] or core of the [[Bristol Proteus|Proteus]] [[turboprop]] then in development.<ref>Baxter 1990, pp. 10–13</ref> In early 1947, the parent [[Bristol Aeroplane Company]] submitted a proposal for a medium-range bomber to the same [[List of Air Ministry specifications|specification]] B.35/46 which led to the [[Avro Vulcan]] and [[Handley Page Victor]]. The Bristol design was the Type 172 and was to be powered by four or six Bristol engines of {{convert|9000|lbf|kN|abbr=on}} thrust<ref>Baxter 1990, pp. 13, 18</ref> to the Ministry engine specification TE.1/46.

The thrust required of the new engine, then designated B.E.10 (later Olympus), would initially be {{convert|9000|lbf|kN|abbr=on}} with growth potential to {{convert|12000|lbf|kN|abbr=on}}. The [[Overall pressure ratio|pressure ratio]] would be an unheard of 9:1.<ref>Baxter 1990, p. 13</ref> To achieve this, the initial design used a low-pressure (LP) [[axial compressor]] and a high-pressure (HP) [[centrifugal compressor]], each being driven by its own single-stage [[turbine]]. This two-spool design eliminated the need for features such as variable inlet guide vanes (Avon, J79), inlet ramps (J65), variable stators (J79) or compressor bleed (Avon) which were required on single spool compressors with pressure ratios above about 6:1. Without these features an engine could not be started nor run at low speeds without destructive blade vibrations. Nor could they accelerate to high speeds with fast acceleration times ("[[Spooling up|spool up]]") without [[compressor stall|surge]].<ref>http://webserver.dmt.upm.es/zope/DMT/Members/jmtizon/turbomaquinas/NASA-SP36_extracto.pdf p.44 and fig.27a</ref> The design was progressively modified and the centrifugal HP compressor was replaced by an axial HP compressor. This reduced the diameter of the new engine to the design specification of {{convert|40|in|cm|abbr=on}}. The Bristol Type 172 was cancelled though development continued for the Avro Vulcan and other projects.<ref name>Baxter 1990, pp. 16, 18</ref>

===Initial Development===
[[File:Bristol Olympus 101 gas flow diagram.jpg|thumb|Gas-flow diagram of Olympus Mk 101]]

The first engine, its development designation being BOl.1 (Bristol Olympus 1), had six LP compressor stages and eight HP stages, each driven by a single-stage turbine. The combustion system was novel in that ten connected flame tubes were housed within a [[Combuster#Cannular type|cannular]] system: a hybrid of separate flame [[Combuster#Can type|cans]] and a true [[Combuster#Annular type|annular]] system. Separate combustion cans would have exceeded the diameter beyond the design limit, and a true annular system was considered too advanced.<ref name='baxter18'>Baxter 1990, p. 18</ref>

In 1950, Dr (later Sir) [[Stanley Hooker]] was appointed as Chief Engineer of Bristol Aero Engines.<ref name=baxter18 />

The BOl.1 first ran on 16 May 1950 and was designed to produce {{convert|9140|lbf|kN|abbr=on}} thrust and to be free from destructive rotating stall on start up to idle speed and to be free from surging on fast accelerations to maximum thrust. The engine started without a problem and Hooker, supervising the first test run and displaying the confidence he had in the design, slammed the throttle to give a surge-free acceleration to  maximum power.<ref>"Not Much Of An Engineer" Sir Stanley Hooker, The Crowood Press Ltd. 2002, ISBN 9780906393352, p.142</ref> The thrustmeter showed {{convert|10000|lbf|kN|abbr=on}}.<ref>"World Encyclopedia of Aero Engines - 5th edition" by [[Bill Gunston]], Sutton Publishing, 2006, p36</ref> The next development was the BOl.1/2 which produced {{convert|9500|lbf|kN|abbr=on}} thrust in December 1950. Examples of the similar BOl.1/2A were constructed for US manufacturer Curtiss-Wright which had bought a licence for developing the engine as the TJ-32 or J67. The somewhat revised BOl.1/2B, ran in December 1951 producing {{convert|9750|lbf|kN|abbr=on}} thrust.<ref>Baxter 1990, p. 20</ref>  The engine was by now ready for air testing and the first flight engines, designated Olympus Mk 99, were fitted into a [[English Electric Canberra|Canberra]] ''[[United Kingdom military aircraft serials|WD952]]'' which first flew with these engines derated to {{convert|8000|lbf|kN|abbr=on}} thrust in August 1952. In May 1953, this aircraft reached a [[Flight altitude record|world record altitude]] of {{convert|63668|ft|m|abbr=on}}.<ref>Baxter 1990, pp. 22, 24</ref> (Fitted with more powerful Mk 102 engines, the Canberra increased the record to {{convert|65876|ft|m|abbr=on}} in August 1955.<ref>Baxter 1990, p. 32</ref>)

==Variants==
[[File:rr olympus at bristol industrial museum arp.jpg|thumb|right|Preserved Bristol Olympus 101.]]
;BOl.1/2A:
;BOl.1/2B:
;BOl.1/2C:
;BOl.2:
;BOl.3: Of all the early initial developments, BOl.2 to BOl.5 (the BOl.5 was never built<ref>Baxter 1990, p. 173</ref>), perhaps the most significant was the BOl.3. Even before the Vulcan first flew, the Olympus 3 was being suggested as the definitive powerplant for the aircraft. In the event, the 'original' Olympus was continuously developed for the Vulcan B1. The BOl.3 was described in 1957 as "a high-ended product intermediate between the Olympus 100 and 200 series."<ref name='flight647'>[http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%201559.html?tracked=1 Arrow] ''Flight'' 25 October 1957, p. 647</ref>
;BOl.4:
;BOl.5:not built
;BOl.6:(Mk.200)
;BOl.7:(Mk.201)
;BOl.7SR:
;BOl.11:(Mk.102)
;BOl.12:(Mk.104)
;BOl.21:(Mk.301)
;BOl.21R: not built, proposed for R.A.E. Missile (A) designed to meet O.R. 1149 issued May 1956.<ref>Fildes 2012, p. 397</ref>
;BOl.22R:(Mk.320)
;BOl.23: not built, proposed with a 301 compressor, 22R turbine and reheat to give 25,000 lb at take-off (reheat).<ref name="Fildes 2012, p. 408">Fildes 2012, p. 408</ref>
;Olympus Mk 100: (BOl.1/2B) Similar to Olympus Mk 99 rated at {{convert|9250|lbf|kN|abbr=on}} thrust for second Vulcan prototype ''VX777''. First flew September 1953.<ref>Baxter 1990, p. 42</ref>{{#tag:ref|''VX777'' was retrofitted with Mk 101,<ref name='baxter44'>Baxter 1990, p. 44</ref> Mk 102 and Mk 104<ref>MOS Air Fleet Record of Aircraft Movements</ref> engines.|group=N}}
;Olympus Mk 101: (BOl.1/2C) Larger turbine, {{convert|11000|lbf|kN|abbr=on}} thrust for initial production Vulcan B1 aircraft. First flew (''XA889'') February 1955.<ref name=baxter44 />
;Olympus Mk 102: (BOl.11) Additional zero stage on LP compressor, {{convert|12000|lbf|kN|abbr=on}} thrust for later production Vulcan B1 aircraft.<ref name='baxter46'>Baxter 1990, p. 46</ref>
;Olympus Mk 104: (BOl.12) Designation for Olympus Mk 102 modified on overhaul with new turbine and burners, {{convert|13000|lbf|kN|abbr=on}} thrust initially, {{convert|13500|lbf|kN|abbr=on}} thrust on uprating,<ref name=baxter46 /> standard on Vulcan B1A.<ref>Pilots Notes AP 4505C—PN</ref>
;'Olympus 106': Used to describe the development engine for the Olympus 200 (BOl.6).<ref>[http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201753.html?tracked=1 Bristol Olympus] ''Flight'' 9 December 1955, p. 876</ref><ref>Blackman 2007, p. 101</ref> Possibly a corruption of BOl.6 (Olympus 6).
;Olympus Mk 97: This early engine tested an early annular combustion chamber. It was test flown on Bristol's [[Avro Ashton]] test bed ''WB493''.<ref>Baxter 1990, p. 33</ref>
;Olympus Mk 201: (BOl.7) Uprated Olympus Mk 200. {{convert|17000|lbf|kN|abbr=on}} thrust. Initial Vulcan B2 aircraft.<ref name=baxter50 />
;Olympus Mk 202: Disputed. Either Olympus Mk 201 modified with rapid air starter,<ref>Baxter 1990, p. 66</ref> or Olympus Mk 201 with redesigned oil separator breathing system.<ref>''Air Publication 101B-1902-1A Vulcan B Mk.2 Aircraft Servicing Manual'' Cover 2, Sect 4, Chap 1, AL 86, Sept '72, Para 54A.</ref> This was the definitive '200 series' engine fitted to Vulcans not fitted with the Mk 301. The restored Vulcan ''XH558'' is fitted with Olympus Mk 202 engines.<ref>{{cite web|url= http://www.caa.co.uk/aandocs/27038/27038000000.pdf |title=''CAA Airworthiness Approval Note 27038'' }} section 5.2.4 Engines.</ref>
;'Olympus Mk 203': Very occasional reference to this elusive mark of engine can be found in some official Air Publications relating to the Vulcan B2. It is also noted in a manufacturer's archived document dated ''circa'' 1960.<ref>[http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=144-pa1716_3-3&cid=1-2-3-5#1-2-3-5|Coventry Archives of the National Archive], PA1716/5/11/3/5.</ref>
;Olympus Mk 301: (BOl.21) Additional zero stage on LP compressor. {{convert|20000|lbf|kN|abbr=on}} thrust.<ref>Baxter 1990, p. 58</ref> Later Vulcan B2 aircraft plus nine earlier aircraft{{#tag:ref|XH557 (flight test), XJ784 (certification), XL384-390 (retrofit programme)|group=N}} retrofitted.<ref>Bulman 2001, pp. 149 & 150</ref> Later derated to {{convert|18000|lbf|kN|abbr=on}} thrust.<ref>Aircrew Manual AP101B-1902-15 Prelim, p. 10</ref> Restored to original rating for [[Operation Black Buck]].<ref>Baxter 1990, p. 70</ref>
;Olympus 510 series: With a thrust in the region of {{convert|15000|lbf|kN|abbr=on}} to {{convert|19000|lbf|kN|abbr=on}}, the 510 series were civilianised versions of the BOl.6.<ref>[http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%201024.html?tracked=1 Aero Engines 1957] ''Flight'' 26 July 1957, p. 114</ref> A team was sent to [[Boeing]] at [[Seattle]] to promote the engine in 1956 but without success.<ref name="Baxter 1990, p. 36"/>
;Olympus 551: The Olympus 551 'Zephyr' was a derated and lightened version of the BOl.6 and rated at {{convert|13500|lbf|kN|abbr=on}} thrust. The engine was the subject of a licence agreement between Bristol Aero Engines and the Curtiss-Wright Corporation - the engine being marketed in the US as the Curtiss-Wright TJ-38 Zephyr. There were hopes to fit the Olympus 551 to the Avro Type 740 and Bristol Type 200 [[trijet]] airliners which did not progress beyond the project stage. Curtiss-Wright also failed to market the engine.<ref>Baxter 1990, pp. 36–40</ref>
[[File:RAF Museum Cosford - DSC08318.JPG|thumb|Olympus Mk 320 at the RAF Museum, Cosford.]]
;Mk.320:The performance specification for the [[BAC TSR-2]] was issued in 1962. It was to be powered by two BSEL Olympus Mk 320 (BOl.22R) engines each rated at {{convert|19610|lbf|kN|abbr=on}} dry and {{convert|30610|lbf|kN|abbr=on}} with reheat at take-off. The engine, which was re-stressed for supersonic flight at sea level, and over Mach 2.0 at altitude, and featured much use of high-temperature alloys such as titanium and [[Nimonic]],<ref name="Sutton Publishing 2006, p. 38">"World Encyclopedia of Aero Engines - 5th edition" by [[Bill Gunston]], Sutton Publishing, 2006, p. 38</ref> was a cutting edge derivative of the Olympus Mk 301 with a Solar-type afterburner.<ref>Baxter 1990, pp. 78, 80</ref> The engine first ran in March 1961, soon achieving {{convert|33000|lbf|kN|abbr=on}},<ref name="Sutton Publishing 2006, p. 38"/> and was test flown in February 1962 in an underslung nacelle in the belly of Vulcan B1 ''XA894'' and was demonstrated at the [[Farnborough Air Show]] in September. In December 1962 during a full power ground run at [[Bristol Filton Airport|Filton]], the LP shaft failed. The liberated turbine disc ruptured fuel tanks and the subsequent fire completely destroyed the Vulcan.<ref>Baxter 1990, pp. 80–86</ref>
:On its first flight in September 1964 the engines of the TSR-2 were scarcely flightworthy being derated and cleared for one flight. Nevertheless, the risk was deemed acceptable in the political climate of the time. With new engines, the TSR-2 ''XR219'' flew another 23 times before the project was cancelled in 1965.<ref>Baxter 1990, pp. 96–100</ref>
;Olympus 593:{{main|Rolls-Royce/Snecma Olympus 593}} The Rolls-Royce/Snecma Olympus 593 was a reheated version of the Olympus which powered the supersonic airliner [[Concorde]].<ref name='baxter131'>Baxter 1990, p. 131</ref> The Olympus 593 project was started in 1964, using the TSR2's Olympus Mk 320 as a basis for development.<ref name="Baxter 1990, p. 135">Baxter 1990, p. 135</ref> BSEL and [[Snecma Moteurs]] of [[France]] were to share the project.<ref name='baxter131'/> Acquiring BSEL in 1966, Rolls-Royce continued as the British partner.<ref>Baxter 1990, p. 11</ref> 
;593D: Formerly Olympus 593. {{convert|28100|lbf|kN|abbr=on}} thrust.<ref name="Baxter 1990, p. 135"/> (the 'D' in the engine designation equalling 'derivation' - for smaller, short-range version of Concorde that was later cancelled)<ref>http://www.flightglobal.com/pdfarchive/view/1966/1966%20-%200036.html</ref>
;593B: Flight test and prototype aircraft. {{convert|34370|lbf|kN|abbr=on}} thrust with reheat. (the 'B' in the engine designation equalling 'big' - for long-range [[Concorde]] that subsequently entered service) <ref>Baxter 1990, p. 149</ref><ref>http://www.flightglobal.com/pdfarchive/view/1966/1966%20-%200080.html</ref>
;593-602:Production. Annular combustion chamber to reduce smoke<ref>Baxter 1990, p. 153</ref>
;593-610:Last production. {{convert|38075|lbf|kN|abbr=on}} thrust with reheat.<ref>Baxter 1990, p. 165</ref>
;Curtiss-Wright TJ-32: Examples of the BOl.1/2A were delivered to Curtiss-Wright in 1950. The engine was Americanised during 1951 and flew under a [[Boeing B-29]] testbed as the TJ-32.
;Curtiss-Wright J67: To meet a USAF demand for an engine in the 15000&nbsp;lb thrust class, the engine was the subject of a development contract, redesigned and designated J67. Development was protracted and in 1955, the USAF announced that there would be no production contract for the present J67. Several aircraft had been intended to receive the J67 including the [[Convair F-102 Delta Dagger]].<ref>[http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201752.html?tracked-1 Bristol Olympus] ''Flight'' 9 December 1955, p. 875</ref>
;Curtiss-Wright T47: The T47 was an attempt to produce a turboprop based upon the J67.<ref>[http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%201006.html?tracked=1 Aero Engines 1954] ''Flight'' 9 April 1954, p. 462</ref>
;TJ-38 Zephyr: See Olympus 551 (above).
[[File:Vulcan B2 XJ784 at Bagotville.jpg|thumb|Avro Vulcan XJ784 at [[CFB Bagotville]] in 1978. It is powered by four Olympus Mk 301 engines, identified by their shorter and wider jet pipe nozzles.<ref>Bulman 2001, p. 146</ref>]]

===Variant notes===
;Second-generation Engines: The initial design of the second-generation 'Olympus 6' began in 1952. This was a major redesign with five LP and seven HP compressor stages and a canullar combustor with eight interconected flame tubes. In spite of a much greater mass flow, the size and weight of the BOl.6 was little different from earlier models.<ref>[http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%200198.html?tracked=1 16,000 lb Thrust] ''Flight'' 15 February 1957, p. 200</ref>

Rival manufacturers Rolls-Royce lobbied very hard to have its [[Rolls-Royce Conway|Conway]] engine installed in the Vulcan B2 to achieve commonality with the Victor B2. As a consequence, Bristol undertook to complete development using company funds and peg the price to that of its fully government-funded rival.<ref name="Baxter 1990, p. 36">Baxter 1990, p. 36</ref>
;Olympus Mk 200: (BOl.6) {{convert|16000|lbf|kN|abbr=on}} thrust. First B2 (''XH533'') only.<ref name='baxter50'>Baxter 1990, p. 50</ref>

;Civilianised Olympus:Plans to civilianise the Olympus go back as far as 1953 with the unveiling of the [[Avro Atlantic]] airliner based upon the Vulcan.<ref>Baxter 1990, p. 40</ref> However, most of the civilian derivatives, except for supersonic airliners, were developed from the BOl.6.

;Thin-wing Javelin:One project that got beyond the drawing board was a supersonic development of the [[Gloster Javelin]], the P370, powered by two BOl.6, 7, or 7SR engines. The design evolved into the P376 with two BOl.21R engines rated at {{convert|28500|lbf|kN|abbr=on}} with reheat. Eighteen aircraft were ordered in 1955. The project was abandoned the following year.<ref>Baxter 1990, pp. 28 & 172</ref>

;Afterburning Olympus: As early as 1952, Bristol had considered the use of [[Afterburner (engine)|reheat, or afterburning]], to augment the thrust of the Olympus. Initially, a system called Bristol Simplifed Reheat was devised which was tested on a [[Rolls-Royce Derwent]] V mounted in an [[Avro Lincoln]]. Later it was tested on an [[Orenda Engines|Orenda]] engine in Canada and on an Olympus Mk 100 in the Avro Ashton test bed.<ref name='baxter26'/> Fully variable reheat became possible after an agreement with the [[Solar Turbines|Solar Aircraft Company]] of San Diego which manufactured bench units for the Olympus Mks 101 and 102.<ref name='baxter26'>Baxter 1990, p. 26</ref> An afterburning Olympus was just one proposal for the Vulcan Phase 6, a 350,000 lb aircraft with a 13/14 hour endurance.<ref name="Fildes 2012, p. 408"/>

;Olympus driving aft fan: '''BS.81''' rated at 28,000lb. As an alternative to afterburning a fan mounted at the trailing edge of the wing was proposed for the Vulcan Phase 6. The fan was driven by a turbine in the engine exhaust at the end of the jetpipe.<ref name="Fildes 2012, p. 407">Fildes 2012, p. 407</ref>

;Vectored thrust Olympus: A vertical take-off Vulcan was proposed in 1960. It used 4 vectored-thrust Olympus as well as 10 lift engines.<ref>Fildes 2012, p. 413</ref>

===Derivatives===
* [[Rolls-Royce Marine Olympus]]

====Industrial power generation====

The Olympus entered service as a [[peak demand]] industrial power generator in 1962 when the [[Central Electricity Generating Board]] (CEGB) commissioned a single prototype installation at its [[Hams Hall power stations|Hams Hall power station]]. Power was provided by an Olympus 201 exhausting through a two-stage turbine powering a [[Brush Electrical Machines|Brush]] synchronous alternator providing 20 MW at 3000 rpm. By 1972, the CEGB had installed 42 Olympus generating sets.<ref>Baxter 1990, pp. 110–123</ref> Olympus engines are also used to provide back up power in case of a loss of grid electrical power at some of Britain's nuclear power stations.

Many sets were exported and many found use on offshore platforms. By 1990, over 320 sets had been sold to 21 countries,<ref name="baxter131"/> many of which remain in service.

==Applications==

* [[Avro Vulcan]] 
* [[BAC TSR-2]]

===Proposed aircraft applications===
Over the years, the Olympus was proposed for numerous other applications including:
* C104 which led to the C105 [[Avro Arrow]]: BOl.3<ref name=flight647 />
* Avro 718: BOl.3<ref name=heritage>[http://homepage.ntlworld.com/david.fildes3/Type%20602%20to%20862] Avro Type List [http://www.avroheritage.com/page10a.html] Avro Heritage</ref> The Type 718 was a military transport aircraft with up to 110 seats.<ref>Fildes 2012, p. 424</ref>
* Avro 739 to OR339 (the requirement that culminated in TSR2): BOl.21R<ref name=heritage /><ref name=baxter172 />
* Avro 740: 3 x Mk 551<ref name=heritage />
* Avro 750: 2 x Mk 551<ref name=heritage />
* Avro Vulcan Phase 6 (B3): BOl.23, a development of the Mk 301.<ref name='baxter172'>Baxter 1990, p. 172</ref> Different engine configurations, BOl.21, BOl.21/2 and BOl.23, with either reheat or an [[Turbofan#Aft-fan turbofan|aft fan]], were proposed for this aircraft to provide the required increase in take-off thrust.<ref name="Fildes 2012, p. 407"/><ref>Addendum to Avro Brochure IPB 104</ref>
* Bristol T172: B.E.10<ref name=baxter172 />
* Bristol T177<ref name=baxter172 />
* Bristol T180<ref name=baxter172 />
* Bristol T198: Mk 591. Early supersonic airliner design (132 seats). The engine was a civilianised BOl.22R.<ref name=baxter172 />
* Bristol T201: Mk 551<ref name=baxter172 />
* Bristol T202<ref name=baxter172 />
* Bristol T204 to OR339: BOl.22SR (simplified reheat)<ref name=baxter172 />
* Bristol T205: Mark 551<ref name=baxter172 />
* Bristol T213<ref name=baxter172 />
* [[Bristol Type 223|Bristol T223]]: Mk 593. Later supersonic airliner design (100 seats). Engine as Mk 591 with zero stage LP compressor and cooled HP turbine.<ref name=baxter172 />
* [[de Havilland]] design to OR339: BOl.14R, BOl.15R. Developed from BOl.6R.<ref name=baxter172 />
* Handley Page HP98: Pathfinder variant of [[Handley Page Victor|Victor]].<ref name=baxter172 />
* Handley Page Victor B1: Mk 104<ref name=baxter172 />
* Handley Page Victor Phase 3<ref name=baxter172 />
* Handley Page HP107<ref name=baxter172 />
* Handley Page Pacific<ref name=baxter172 />
* [[Hawker P.1121]]: BOl.21R<ref name=baxter172 />
* Hawker P.1129 to OR339: BOl.15R<ref name=baxter172 />
* [[Martin/General Dynamics RB-57F Canberra]]: Mk 701 developed from Mk 301.<ref name=baxter172 />
* Gloster P492/3: Mk 591<ref name=baxter172 />
* [[Republic F-105 Thunderchief]]: BOl.21 for possible sale to RAF.<ref name=baxter172 />
* [[Saab 36]]<ref>[[Saab 36|Wikipedia article]] quoting Berns, Lennart ''A36 - SAABs atombombare avslöjad'', ''Flygrevyn'' issue #4, April 1991</ref>
* [[Saab 37 Viggen]]<ref>[http://www.datasaab.se/Papers/Pages%20from%20Protec.pdf] Historien om Viggen [http://www.fmv.se/WmTemplates/Page.aspx?id=246] ''Protec'' 2005 No 4</ref>
* [[Vickers VC10]]: Development of Mk 555 with aft fan.<ref name=baxter172 />

==Engines on display==
* [http://www.bristolaero.com/ Bristol Aero Collection] Kemble Airfield - Mk 104, 593B
* [[Imperial War Museum]] North, Manchester - Mk 101
* [[RAF Museum Cosford]] - Mk 320
* [http://www.gatwick-aviation-museum.co.uk/ Gatwick Aviation Museum] Charlwood Surrey - Two Mk 320
* [http://www.yorkshireairmuseum.org/ Yorkshire Air Museum]  Elvington, North Yorkshire - Mk 593
* [[Brooklands Museum]] Weybridge, Surrey - Mk 593B
* [[Montrose Air Station Heritage Centre]] - Bristol B.E.10
* [[Museum of Science and Industry (Manchester)]] - Mk 202 (Engine is displayed as a Mk 201 but its ECU plate reveals it as a Mk 202)

==Specifications Olympus 101==

{{jetspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|type=Axial flow two-spool turbojet
|ref=[http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200289.html?tracked=1 The Operational Olympus] ''Flight'' and ''Lecture Notes, Vulcan'' Bristol Aero Engine School 
|length= {{convert|152.2|in|ft m|abbr=on}}
|diameter= {{convert|40|in|ft m|abbr=on}}
|weight= {{convert|3615|lb|kg|abbr=on}}
|compressor= Axial 6 LP stages, 8 HP stages
|combustion= Cannular 10 flame tubes
|turbine= HP single stage, LP single stage
|fueltype=AVTUR or AVTAG
|oilsystem=
|power=
|thrust= {{convert|11000|lbf|kN|abbr=on}}
|compression=
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon= {{convert|.817|tsfc}}
|power/weight= 3.04:1
|thrust/weight=
}}

==Specifications Olympus 301==

{{jetspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|type=Axial flow two-spool turbojet
|ref=[https://www.flightglobal.com/FlightPDFArchive/1969/1969%20-%201207.PDF] ''flightglobal''  
|length= {{convert|131|in|ft m|abbr=on}}
|diameter= {{convert|44.5|in|ft m|abbr=on}}
|weight= {{convert|4290|lb|kg|abbr=on}}
|compressor= Axial 6 LP stages, 7 HP stages
|combustion= Cannular 10 flame tubes
|turbine=
|fueltype=
|oilsystem=
|power=
|thrust= {{convert|20000|lbf|kN|abbr=on}}
|compression=
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon=
|power/weight= 4.66:1
|thrust/weight=
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=
<!-- designs which were developed into or from this aircraft: -->
|related=
* [[Bristol Siddeley BS100]] (Olympus core)
<!-- * [[Curtiss-Wright J67]] -->
* [[Rolls-Royce Marine Olympus]]
* [[Rolls-Royce/Snecma Olympus 593]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=
<!-- relevant lists that this aircraft appears in: -->
|lists=
* [[List of aircraft engines]]
<!-- For aircraft engine articles. Engines that are of similar to this design: -->
|similar engines=
* [[Pratt & Whitney J75]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
;Notes
{{reflist|group=N}}

;Citations
{{Reflist|30em}}

;Bibliography
{{refbegin}}
* Baxter, Alan. ''Olympus — the first forty years''. Derby, UK: Rolls-Royce Heritage Trust, 1990. ISBN 978-0-9511710-9-7
* Blackman, Tony. ''Vulcan Test Pilot''. London, UK: Grub Street, 2009. ISBN 978-1-906502-30-0
* Bullman, Craig. ''The Vulcan B.Mk2 from a Different Angle''. Bishop-Auckland, UK: Pentland Books, 2001. ISBN 1-85821-899-3
* Fildes, David W. ''The Avro Type 698 Vulcan"Barnsley, UK: Pen % Sword Aviation, 2012, ISBN 978 1 84884 284 7
* Hooker, Stanley. ''Not Much of an Engineer''. Marlsborough, UK: Airlife Publishing, 2002. ISBN 978-1-85310-285-1
{{refend}}

==External links==
{{Commons category}}
* [http://www.rolls-royce.com/about/ourstory/heritage_trust/ Rolls-Royce Heritage Trust]
* [http://www.turbine-support.com/ Turbine Support] image of Olympus power station
* [http://www.enginehistory.org/G&jJBrossett/Coventry/Bristol%20Olympus%20301.JPG enginehistory.org] Good image of Mk 301
* [http://www.flightglobal.com/airspace/media/aeroenginesjetcutaways/images/5589/bristol-olympus-mk12a-cutaway.jpg ''Flight'' cutaway] of BOl.1/2A
* [http://www.flightglobal.com/pdfarchive/view/1961/1961%20-%200233.html "Olympian Heights"] 1961 ''Flight'' article

{{Navboxes
|title=Articles and topics related to Rolls-Royce Olympus
|state=collapsed
|titlestyle={{WPMILHIST Infobox style|nav_box_wide_header}}
|list1=
{{BristolAeroengines}}
{{RRaeroengines}}
{{Wright aeroengines}}
{{USAF gas turbine engines}}
}}

[[Category:Bristol Siddeley aircraft engines|Olympus]]
[[Category:Rolls-Royce aircraft gas turbine engines|Olympus]]
[[Category:Turbojet engines 1950–1959]]