{{Use dmy dates|date=June 2011}}
{{Use Australian English|date=June 2011}}
{{Infobox musical artist
| name            = The Superjesus
| image           = 
| image_size      = 
| landscape       = <!-- yes, if wide image, otherwise leave blank -->
| alt             = 
| caption         = 
| background      = group_or_band
| alias           = Hell's Kitchen
| origin          = [[Adelaide]], South Australia, Australia
| genre           = {{flatlist|
* [[Indie rock]]
* [[alternative rock]]
* [[post-grunge]]
* [[Rock music|rock]]
}}
| years_active    = {{flatlist|
* 1994–2004
* 2013–present
}}
| label           = {{flatlist|
* Aloha
* [[East West Records|EastWest]]/[[Warner Music Australasia|Warner]]
}}
| associated_acts = {{flatlist|
* [[The Androids]]
* [[Rogue Traders]]
* Screaming Bikini
* [[Baby Animals]]
* [[Divinyls]]
}}
| website         = {{URL|thesuperjesus.com}}
| current_members = 
* [[Sarah McLeod (musician)|Sarah McLeod]]
* [[Stuart Rudd]]
* Jason Slack
| past_members    = 
* [[Paul Berryman]]
* [[Tim Henwood]]
* Chris Tennent
* [[Aaron Tokona]]
* Patch Brown
}}

'''The Superjesus''' are an Australian [[Rock music|rock]] band formed in [[Adelaide]] in late 1994. Their debut album, ''[[Sumo (album)|Sumo]]'' (February 1998), peaked at No.&nbsp;2 on the [[ARIA Charts|ARIA Albums Chart]], their second album, ''[[Jet Age (Superjesus album)|Jet Age]]'' (October 2000) reached No.&nbsp;5 and their third album, ''[[Rock Music (album)|Rock Music]]'' (May 2003) got to No.&nbsp;14. Their singles include "Shut My Eyes" (1996), "Down Again" (1997), "Gravity" (2000) and "Secret Agent Man" (2001). At the [[ARIA Music Awards of 1997]] they won Best New Talent for ''[[Eight Step Rail]]'' (their debut extended play, May 1996) and Breakthrough Artist – Single for "Shut My Eyes". The group disbanded in mid-2004 and reunited in 2013 with mainstay members [[Paul Berryman]] on drums, [[Sarah McLeod (musician)|Sarah McLeod]] on lead vocals and Stuart Rudd on bass guitar.

==History==

The Superjesus formed in late 1994 as '''Hell's Kitchen''' in Adelaide by [[Paul Berryman]] on drums, [[Sarah McLeod (musician)|Sarah McLeod]] (ex-Fallen Down Monster) on lead vocals and guitar, [[Stuart Rudd]] on bass guitar and Chris Tennent on lead guitar.<ref name="McFarlane">{{cite book | last1 = McFarlane | first1 = Ian | authorlink1 = Ian McFarlane | title = [[Encyclopedia of Australian Rock and Pop]] | chapter = Encyclopedia entry for 'The Superjesus' | archiveurl = https://web.archive.org/web/20040828074512/http://www.whammo.com.au/encyclopedia.asp?articleid=427 | chapterurl = http://www.whammo.com.au/encyclopedia.asp?articleid=427 | accessdate = 12 June 2016 | year = 1999 | publisher = [[Allen & Unwin]] | location = [[St Leonards, New South Wales|St Leonards, NSW]] | archivedate = 28 August 2004 | isbn = 1-86508-072-1 }}</ref><ref name="Nimmervoll">{{cite web | archiveurl = http://pandora.nla.gov.au/pan/14231/20050129-0000/www.howlspace.com.au/en3/superjesus/superjesus.htm | url = http://www.howlspace.com.au/en3/superjesus/superjesus.htm | title = Superjesus | last1 = Nimmervoll | first1 = Ed | authorlink1 = Ed Nimmervoll | publisher = Howlspace – The Living History of Our Music. White Room Electronic Publishing Pty Ltd (Ed Nimmervoll) | archivedate = 29 January 2005 | accessdate = 12 June 2016 }}</ref><ref name="Holmgren">{{Cite web | archiveurl = https://web.archive.org/web/20121011095737/http://hem.passagen.se/honga/database/s/superjesus.html | url = http://hem.passagen.se/honga/database/s/superjesus.html | work = hem.passagen.se | title = Hell's Kitchen/The Superjesus | publisher = [[Australian Rock Database]] (Magnus Holmgren) | last1 = Holmgren | first1 = Magnus | archivedate = 11 October 2012 | accessdate = 12 June 2016 }}</ref> Rudd and Tennent had been jamming together for about a year when Rudd tried out for McLeod's latest band.<ref name="Nimmervoll"/> Tennent was McLeod's guitar teacher at the time and a veteran of the Adelaide music scene, having played in various bands since the 1980s.<ref name="Nimmervoll"/><ref name="Williamson">{{cite web | url = http://www.abc.net.au/local/stories/2011/08/12/3292151.htm | title = Sarah McLeod's New Screaming Bikini | last = Williamson | first = Brett | publisher = [[891 ABC Adelaide]] ([[Australian Broadcasting Corporation]] (ABC)) | date = 23 August 2011 | accessdate = 12 June 2016 }}</ref> [[Paul Berryman]] successfully auditioned for the group and they started rehearsing for over a year before their first gig.<ref name="McFarlane"/><ref name="Nimmervoll"/>

Hell's Kitchen changed their name to the Superjesus on the eve of the [[Big Day Out]] in Adelaide on [[Australia Day]] long-weekend, January 1996.<ref name="McFarlane"/> Tennent provided the new name.<ref name="Nimmervoll"/> Berryman explained, "It was basically just a piss-take on commonly used words in the '90s. Like [[The Jesus and Mary Chain]], "[[Jesus Built My Hotrod]]", [[the Jesus Lizard]], [[Jesus Jones]]; there's just heaps of Jesus' out there in the music world so we thought we would have that and super was kind of like a '90s catch word." Their debut five-track extended play, ''[[Eight Step Rail]]'', was released in August 1996 on Aloha Records.<ref name="McFarlane"/><ref name="Holmgren"/> It spent a couple of months on top of the Australian Independent charts.

''Eight Step Rail'' reached the [[ARIA Charts|ARIA Singles Chart]] Top&nbsp;50 in the following February.<ref name="AusCharts">{{cite web | url = http://australian-charts.com/showinterpret.asp?interpret=The+Superjesus | title = Discography The Superjesus | publisher = Australian Charts Portal. Hung Medien (Steffen Hung) | last = Hung | first = Steffen | accessdate = 12 June 2016 }}</ref> Its feature track, "Shut My Eyes", received high rotation on national youth radio station, [[Triple J]] – it was listed at No.&nbsp;81 on the station's [[Triple J Hottest 100, 1996|Hottest 100 for 1996]].<ref name="McFarlane"/><ref name="Triple J 96">{{cite web | archiveurl = http://www.webcitation.org/6ELYWZg5I?url=http://www.abc.net.au/triplej/hottest100/history/1996.htm | url = http://www.abc.net.au/triplej/hottest100/history/1996.htm | title = Hottest 100 History 1996 | publisher = [[Triple J]]. [[Australian Broadcasting Corporation]] (ABC) | archivedate = 11 February 2013 | accessdate = 12 June 2016 }}</ref> [[AllMusic]]'s Jonathan Lewis described how "their wall of guitar noise (somewhat reminiscent of ''Siamese Dream''-era Smashing Pumpkins) made them an overnight success on Australian radio. Sounding like a cross between Liz Phair and Catatonia's Cerys Matthews, McLeod's voice was a major drawcard; strong, confident and rarely lapsing into the girlishness that characterized some of the Superjesus' contemporaries."<ref name="Lewis">{{cite web | url = http://www.allmusic.com/album/eight-step-rail-ep-mw0001046026 | title = ''Eight Step Rail'' EP – Superjesus &#124; Songs, Reviews, Credits | last = Lewis | first = Jonathan | publisher = [[AllMusic]] | accessdate = 12 June 2016 }}</ref> Jasper Cooper of Oz Music Project felt that on the EP, "Musically, the band inherited much from contemporaries such as the Smashing Pumpkins, but McLeod's appeal at the band's fore, lent The Superjesus their own niche and stature. The single track 'Shut My Eyes' was the band's tour de force, something that was still unmatched in their latter albums."<ref name="Cooper">{{cite web | archiveurl = http://pandora.nla.gov.au/pan/36937/20060807-0000/www.ozmusicproject.net/top100/90.html | url = http://www.ozmusicproject.net/top100/90.html | title = Top 100 Australian Albums of the 90s | last = Cooper | first = Jasper (Jaz) | publisher = Oz Music Project | archivedate = 7 August 2006 | accessdate = 12 June 2016 }}</ref>

They followed with a tour of Australia's east coast, then supported shows by [[The Clouds (Australian band)|Clouds]], [[Hoodoo Gurus]] and United Kingdom band, [[Bush (British band)|Bush]].<ref name="McFarlane"/> As songwriters, Tennent typically composed the music and McLeod supplied the lyrics – the pair had also developed a personal relationship.<ref name="Nimmervoll"/> In January 1997 the Superjesus appeared at the Big Day Out and, in April, they travelled to Triclops Sound Studios, [[Atlanta]] to record their debut album, ''[[Sumo (album)|Sumo]]'', with [[Matt Serletic]] ([[Collective Soul]]) producing and Jeff Tomei (Smashing Pumpkins, Hole) as audio engineer.<ref name="McFarlane"/><ref name="Holmgren"/> At the [[ARIA Music Awards of 1997]] they won Best New Talent for ''Eight Step Rail'' and Breakthrough Artist – Single for "Shut My Eyes".<ref name="ARIA Awards list">ARIA Music Awards for the Superjesus:
* Search Results 'Superjesus': {{cite web | url = http://www.ariaawards.com.au/history/search/?view=list&text=superjesus | title = Winners by Year: Search Results for 'Superjesus' | publisher = Australian Recording Industry Association (ARIA) | accessdate = 12 June 2016 }} Note: The 2016 ARIA site does not list a second win for 1997: Breakthrough Artist – Single for "Shut My Eyes".
* 1997 winners and nominees: {{cite web | archiveurl = http://web.archive.org/web/20071222011152/http://www.ariaawards.com.au/history-by-year.php?year=1997 | url = http://www.ariaawards.com.au/history/year/1997?view=list | title = Winners by Year 1997 | publisher = Australian Recording Industry Association (ARIA) | archivedate = 22 December 2007 | accessdate = 12 June 2016 }} Note: The archive copy of the ARIA site lists two wins for 1997, the 2016 copy of the site does not.
* 1998 winners and nominees: {{cite web | archiveurl = http://web.archive.org/web/20110927121049/http://www.ariaawards.com.au/history-by-year.php?year=1998 | url = http://www.ariaawards.com.au/history/year/1998?view=list | title = Winners by Year 1998 | publisher = Australian Recording Industry Association (ARIA) | archivedate = 27 September 2011 | accessdate = 12 June 2016 }}
* 2001 winners and nominees: {{cite web | archiveurl = http://web.archive.org/web/20070926235727/http://www.ariaawards.com.au/history-by-year.php?year=2001 | url = http://www.ariaawards.com.au/history/year/2001?view=list | title = Winners By Year 2001 | publisher = Australian Recording Industry Association (ARIA) | archivedate = 26 September 2007 | accessdate = 12 June 2016 }}
</ref><ref name=femail/> McLeod and Tennent's personal relationship had ended and late that year; he temporarily left the group.<ref name="Nimmervoll"/><ref name="Scott"/> Aaron Tokona (of [[Weta (band)|Weta]]) filled in on guitar when the Superjesus toured New Zealand.<ref name="Scott">{{cite journal | title = Weta's Winning Ways | last = Scott | first = Jennifer | work = New Zealand Musician | volume = 8 | issue = 9 | date = July 2000 }}</ref> Tokona turned down the offer to become a permanent member and remained with Weta.<ref name="Scott"/>

With Tennent back on board they released ''Sumo'' in February 1998 through [[East West Records|EastWest]]/[[Warner Music Australasia]], which peaked at No.&nbsp;2 on the ARIA Albums Chart and was certified platinum by ARIA for shipment of 70,000 copies.<ref name="McFarlane"/><ref name="AusCharts"/><ref name="ARIA Cert98">{{cite certification | region = Australia | type = album | certyear = 1998 | accessdate = 12 June 2016 }}</ref> Australian musicologist, [[Ian McFarlane]], described it as "a big sounding album backed by a generous budget."<ref name="McFarlane"/> Its local success led to a United States version – with an altered track listing – being issued in June.<ref name="McFarlane"/><ref name="Holmgren"/><ref name="Lewis 2"/> Lewis felt that they "show that guitar rock with McLeod's vocals soaring over the top is definitely their strength. Unfortunately, it is also their weakness, with ''Sumo'' containing too little variation in style."<ref name="Lewis 2">{{cite web | url = http://www.allmusic.com/album/sumo-mw0000038065 | title = ''Sumo'' – Superjesus &#124; Songs, Reviews, Credits | last = Lewis | first = Jonathan | publisher = AllMusic | accessdate = 12 June 2016 }}</ref> An extended version, including a bonus seven-track [[enhanced CD]] of live performances, ''It's not over Till the Fat Sumo Sings'', appeared in October as ''Sumo II''.<ref name="McFarlane"/><ref name="Holmgren"/> At the [[ARIA Music Awards of 1998]] ''Sumo'' had them nominated for [[ARIA Award for Best Group|Best Group]], [[ARIA Award for Breakthrough Artist – Album|Breakthrough Artist – Album]] and [[ARIA Award for Best Cover Art|Best Cover Art]] (artwork by Chris and William Tennent<!-- Relationship not known -->).<ref name="ARIA Awards list"/>

In January 1999 they appeared at the Big Day Out, again, and then took a few months off.<ref name="McFarlane"/> Chris Tennent left the Superjesus permanently in mid-1999 when they were due to resume.<ref name="McFarlane"/><ref name=femail>{{cite web |url=http://www.femail.com.au/superjesus.htm |title = The Superjesus in the ''Jet Age'' | author = Buckingham, Louise | date = December 2000 | work = femail.com.au |publisher= |accessdate=25 May 2011}}</ref> The group relocated to Melbourne in November where they recruited [[Tim Henwood]] (ex-Jen Anderson Band, Supermann, Plasticine, [[Nick Barker|Barker]]) on guitar.<ref name="McFarlane"/><ref name="Holmgren"/> Henwood and McLeod became the principal songwriters.<ref name="Nimmervoll"/> The band released their second album, ''[[Jet Age (Superjesus album)|Jet Age]]'', in October 2000, which was produced by Ed Buller (Psychedelic Furs, Suede, Ben Lee) and peaked at No.&nbsp;5.<ref name="Holmgren"/><ref name="AusCharts"/> Louise Buckingham of femail.com.au noticed that the group's "rock engine is winding up and are fuelled to commence take-off down the runway of Australian rock music. With the recent release of blazing new album ''Jet Age'' and with the hit-single 'Gravity' now a friend to the radio waves, The Superjesus have proved they are keen to move into a new musical era."<ref name=femail/> [[Amazon.com]]'s editorial reviewer declared that it was an "intelligent, mature, sophisticated" release.<ref name="Amazon Jet">{{cite web | url = https://www.amazon.com/Jet-Age-Superjesus/dp/B000056H8N?ie=UTF8&*Version*=1&*entries*=0 | title = Superjesus – ''Jet Age'' – Music | work = [[Amazon.com]] | date = | accessdate = 12 June 2016 }}</ref> At the [[ARIA Music Awards of 2001]], ''Jet Age'' was nominated for [[ARIA Award for Best Rock Album|Best Rock Album]] and Best Cover Art (Darren Glindemann).<ref name="ARIA Awards list"/>

Henwood left in mid-2001 and eventually formed [[The Androids]] (he later joined [[Rogue Traders]]).<ref name="Nimmervoll"/><ref name="Wilson">{{cite web | url = {{Allmusic|class=artist|id=p530360/biography|pure_url=yes}} | title = Rogue Traders – Biography | last = Wilson | first = MacKenzie | publisher = AllMusic | accessdate = 12 June 2016 }}</ref> McLeod considered disbanding the Superjesus but took up lead guitar while working for their third album as a trio, ''[[Rock Music (album)|Rock Music]]'' (May 2003), with Marc Waterman producing,<ref name="Nimmervoll"/><ref name="Holmgren"/> which reached No.&nbsp;14.<ref name="AusCharts"/> Patch Brown had replaced Henwood, but due to stylistic differences during recording, he was replaced in turn by Jason Slack, a Slippery Rock Graduate. Tim Cashmere of ''Undercover'' felt that "they are just as powerful as before" with McLeod "singing better than ever over her chunkier-than-ever riffs."<ref name="Cashmere">{{cite web | archiveurl = http://pandora.nla.gov.au/pan/11299/20030929-0000/www.undercover.com.au/reviews/ursuperjesusrockmusic.html | url = http://www.undercover.com.au/reviews/ursuperjesusrockmusic.html | title = Superjesus, ''Rock Music'' | last = Cashmere | first = Tim | work = Undercover | publisher = [[Paul Cashmere]], Ros O'Gorman | archivedate = 29 September 2003 | accessdate = 12 June 2016 }}</ref> MediaSearch's Carmine Pascuzzi cautioned that "Those who have been ready to write them off should rethink as Sarah McLeod sings beautifully and the band work in harness efficiently to produce a strong effort. The riffs and tenacity are very much on show... They still have a knack for really rocking out, yet indulging in some slower ballads."<ref name="Pascuzzi">{{cite web | archiveurl = http://pandora.nla.gov.au/pan/63544/20110517-0001/www.mediasearch.com.au/music/cdreviews/gen_TheSuperjesusRockMusic.html | url = http://www.mediasearch.com.au/music/cdreviews/gen_TheSuperjesusRockMusic.html | title = CD Reviews :: ''Rock Music'', The Superjesus | last = Pascuzzi | first = Carmine | publisher = MediaSearch | archivedate = 17 May 2011 | year = 2009 | accessdate = 12 June 2016 }}</ref>

In June 2004 the Superjesus split with Warner Music Australasia as the members took time off to pursue other projects. McLeod worked on a solo album, ''[[Beauty Was a Tiger]]''. She then fronted the Sydney-based three-piece band, Screaming Bikini. On 27 November 2012 McLeod announced on radio station MMM in Adelaide and her Facebook page that there would be a one-off reunion show in that city on 1 February 2013. The band, with the line-up of Berryman, Henwood, McLeod and Rudd, played at the Stone Music Festival with [[Van Halen]] and [[Aerosmith]] in Sydney in April, before embarking on their first national tour in 10 years – The Resurrection Tour – in May and June.<ref name="musicfeeds1">{{cite web | url = http://musicfeeds.com.au/news/the-superjesus-announce-the-resurrection-tour-mayjune-2013/ | title = The Superjesus Announce 'The Resurrection Tour' – May/June 2013 – Music News, Reviews, Interviews and Culture | last = Egging | first = Kiel | publisher = Music Feeds | date = 7 March 2013 | accessdate = 12 June 2016 }}</ref>

In early 2015 the Superjesus undertook the She Who Rocks Tour sharing the stage with the [[Baby Animals]], fronted by [[Suze DeMarchi]].<ref name="Pinnegar">{{cite web | url = http://magazine.100percentrock.com/interviews/201505/118770 | title = Interview: Sarah McLeod, The Superjesus | author = Pinnegar, Shane | work = 100% Rock Magazine | date = 22 May 2015 | accessdate = 12 June 2016 }}</ref> In June that year the Superjesus released their first single in over a decade, "The Setting Sun".<ref name="Culpan">{{cite web | url = http://maytherockbewithyou.com/mtrbwy/2015/06/the-superjesus-have-signed-with-social-family-records-and-announce-their-new-single-the-setting-sun/ | title = The Superjesus have signed with Social Family Records and announce their new single, 'The Setting Sun' | author = Culpan, Troy | work = May the Rock Be with You | date = 4 June 2015 | accessdate = 12 June 2016 }}</ref> In November they followed with "St. Peters Lane", and embarked on The Setting Sun Tour.<ref name="Cashmer 2">{{cite news | url = http://www.noise11.com/news/music-news-the-superjesus-reveal-st-peters-lane-video-20151125 | title = Music News: The Superjesus Reveal 'St Peters Lane' Video | last = Cashmere | first = Paul | work = Noise11 | publisher = Paul Cashmere, Ros O'Gorman | date = 25 November 2015 | accessdate = 12 June 2016 }}</ref> In December, Henwood departed to return to [[The Androids]] and was replaced with former member, Jason Slack <ref name="Canavan">{{cite web | url = http://www.weekendnotes.com/a-day-on-the-green-at-leconfield-wines/ | title = A Day on the Green – Bryan Adams at Leconfield Wines – Review – Adelaide | last = Canavan | first = Stacey | work = Weekend Notes | date = 19 March 2016 | accessdate = 12 June 2016 }}</ref> and in April 2016, drummer [[Paul Berryman]] departed due to family commitments and residing in the United States. Meanwhile, McLeod traveled to New York City for three months to write new material for a planned fourth album.

In August 2016, the band released their [[Extended play|EP]], entitled ''Love and Violence'', released through Golden Robot Records, as a lead up to their album release in the coming months. To promote the EP release, TSJ embarked on a national tour in October with special guests The Art and Moon*.

The Superjesus will be inducted into the South Australian Music Hall Of Fame on March 3rd 2017 at the Mortlock Library, State Library of South Australia.<ref>{{Cite web|url=https://www.trybooking.com/Booking/BookingEventSummary.aspx?eid=246302|title=AMC SESSIONS AND HALL OF FAME INDUCTIONS AT THE MORTLOCK|website=TryBooking|access-date=2016-11-26}}</ref>

==Band members==
===Current members===

*Sarah McLeod - lead vocals, guitar <small>(1994–2004, 2013–present)</small>
*Stuart Rudd - bass <small>(1994–2004, 2013–present)</small>
*Jason Slack - lead guitar <small>(2001–2004, 2015–present)</small>

===Former members===

*Chris Tennent - lead guitar <small>(1994–1997, 1997-1999)</small>
*Paul Berryman - drums <small>(1994–2004, 2013–2016)</small>
*Aaron Tokona - lead guitar <small>(1997)</small>
*Tim Henwood - lead guitar <small>(1999-2001, 2013-2015)</small>
*Patch Brown - lead guitar <small>(2001)</small>

==Discography==

===Albums===
* ''[[Sumo (album)|Sumo]]'' (February 1998) ([[List of music recording certifications|AUS Platinum certification]])
* ''[[Jet Age (Superjesus album)|Jet Age]]'' (October 2000) (Peaked at No.&nbsp;5 on the ARIA Albums Chart,<ref name=jetage>{{cite web |url=http://australian-charts.com/showitem.asp?interpret=The+Superjesus&titel=Jet+Age&cat=a |title=The Superjesus - Jet Age (Album) |work=australian-charts.com |publisher=Hung Medien |accessdate=25 May 2011}}</ref> AUS Gold certification)
* ''[[Rock Music (album)|Rock Music]]'' (May 2003) - (Peaked at No.&nbsp;14 on the ARIA Albums Chart<ref name=aria>{{cite web |url=http://australian-charts.com/showitem.asp?key=62044&cat=a |title=The Superjesus - Rock Music (Album) |work=australian-charts.com |publisher=Hung Medien |accessdate=25 May 2011}}</ref>)

===EPs and singles===
* ''[[Eight Step Rail]]'' [[Extended play|EP]] (05/96) - Peaked at No.&nbsp;47 on the Australian charts
* "Down Again" (25/08/97) - Peaked at No.&nbsp;23 on the Australian charts
* "Saturation" (11/97) - Peaked at No.&nbsp;42 on the Australian charts
* "Now And Then" (04/98) - Peaked at No.&nbsp;40 on the Australian charts
* "Ashes" (09/98) - Peaked at No.&nbsp;66 on the Australian charts
* "Gravity" (11/09/00) - Peaked at No.&nbsp;35 on the Australian charts
* "Secret Agent Man" (12/02/01) - Peaked at No.&nbsp;43 on the Australian charts
* "Enough To Know" (20/08/01) - Peaked at No.&nbsp;42 on the Australian charts
* "Second Sun" (11/02/02) - Peaked at No.&nbsp;107 on the Australian charts
* "Stick Together" (14/04/03) - Peaked at No.&nbsp;35 on the Australian charts
* "Over And Out" (07/07/03) - Peaked at No.&nbsp;53 on the Australian charts
* "So Lonely" (09/02/04) - Peaked at No.&nbsp;45 on the Australian charts
* "The Setting Sun" (June 2015) - Radio release, first new single in over a decade.
* "St. Peters Lane" (November 2015) - Radio release, second new single.
* "Love and Violence" [[Extended play|EP]] (August 2016) - Golden Robot Records.

==References==

{{Reflist|30em}}

==External links==
* [http://www.discogs.com/artist/Superjesus%2C+The The Superjesus at discogs.com]
* [http://open-site.org/Arts/Music/Bands_and_Artists/S/Superjesus,_The/Discography  Comprehensive Superjesus Discography at open-site.org]

{{ARIA Award for Breakthrough Artist}}

{{Authority control}}

{{DEFAULTSORT:Superjesus, The}}
[[Category:ARIA Award winners]]
[[Category:Australian alternative rock groups]]
[[Category:Australian post-grunge groups]]
[[Category:Musical trios]]
[[Category:Musical groups established in 1994]]
[[Category:Musical groups disestablished in 2004]]
[[Category:Musical groups reestablished in 2013]]
[[Category:Musical groups from Adelaide]]
[[Category:1994 establishments in Australia]]
[[Category:2004 disestablishments in Australia]]
[[Category:Warner Music Group artists]]