[[File:Louis Bleriot.jpg|thumb|300px|Louis Blériot, photographed in London after his cross-channel flight]]'''Blériot Aéronautique''' was a [[France|French]] aircraft manufacturer founded by [[Louis Blériot]]. It also made a few motorcycles between 1921 and 1922 and [[cyclecar]]s during the 1920s.

==Background==
[[Louis Blériot]] was an engineer who had developed the first practical headlamp for cars and had established a successful business marketing them.  In 1901 he had built a small unmanned [[ornithopter]], but his serious involvement with aviation began in April 1905 when he witnessed [[Gabriel Voisin]]'s first experiments with a floatplane glider towed behind a motorboat on the river Seine.  A brief partnership with Voisin followed, but after the failure of the [[Blériot III]] and its modified version, the Blériot IV, the partnership was dissolved and Blériot set up his own company, "Recherches Aéronautique Louis Blériot" (Louis Blériot Aeronautical Research).

==Blériot's early experiments==
<gallery>
File:Bleriot III.jpg|[[Blériot III]]
File:Bleriot V.jpg|[[Blériot V]]
File:Bleriot VI.jpg|[[Blériot VI]]
File:Bleriot VII.jpg|[[Blériot VII]]
File:Bleriot VIII.jpg|[[Blériot VIII]]
File:Bleriot.jpg|[[Blériot XI]] as first built

</gallery>

Unlike the business started by Gabriel Voisin, which was a straightforward design and manufacturing concern with Voisin acting as aircraft designer, Bleriot's establishment was, as its name suggests, essentially a privately funded research establishment, employing various engineers and designers.<ref>Elliott 2000 p.50</ref> Owing to this it is difficult to establish the extent of Blériot's involvement in the actual design of the aircraft which bear his name. Over the next few years a series of aircraft of varying configurations were produced, each one marginally more successful than its predecessor, and culminating in the [[Blériot XI|Type XI]] with which he became famous for being the first to fly across the [[English Channel]] in 1909.

==Commercial success==
[[File:Oskar Bider 1913 Bern.jpg|thumbnail|Gnome engined production Blériot XI]]

===Aircraft design and manufacturing===
The publicity gained by this achievement brought the company orders for large numbers of the Type XI, and several hundred were eventually made. This commercial success enabled the research side of the business to expand considerably, and in the years before the [[First World War]] a startlingly heterogeneous collection of aircraft were produced, although none came close to being as successful as the Type XI.

===Flying schools===
In late 1909 Blériot established a flying training school for pilots at [[Etampes]] near [[Rouen]], and early the next year a second school was opened at [[Pau, Pyrénées-Atlantiques|Pau]],  Between 1910 and 1914 these schools trained around 1,000 pilots:  nearly half of the pilots holding an Aero Club de France brevet at the outbreak of the [[First World War]] had been trained by the Blériot schools.<ref>Elliott 2000 p. 173</ref>  In September 1910 another flying school was opened at the newly established [[Hendon Aerodrome|Hendon aerodrome]] near [[London]]. In July 1914 at [[Brooklands]] in [[Surrey]], Bleriot opened another flying school at [[Brooklands]] in Surrey and also a small factory there which was managed by Norbert Chereau and produced about 20 Bleriot Monoplane Trainers.

===SPAD===
In 1913 Blériot acquired the assets of the [[Deperdussin]] company, following the arrest on fraud charges of its founder [[Armand Deperdussin]].
The name of the company was changed from ''Société de Production des Aéroplanes Deperdussin'' to ''Société Pour L'Aviation et ses Dérivés'', generally referred to by its acronym '''[[Société Pour L'Aviation et ses Dérivés|SPAD]]'''.  This company became extremely successful during [[World War I]] with its mass production in French factories and worldwide exports. Production licenses were sold in several countries, including a larger British factory that was established near Brooklands at [[Addlestone]], Surrey by 1917, and a production line at the [[Curtiss Aeroplane and Motor Company|Curtiss]] Elmwood plant (Buffalo, NY) in August 1917.

==1914-18==
During the First World War Blériot Aéronautique was largely concerned with manufacturing aircraft designed by others. The only aircraft produced under the Blériot name was a series of prototype multi-engined heavy bombers, none of which entered service.

==After World War One==

The Allied victory in 1918 resulted in difficult times for the aircraft industry.  During the war a large manufacturing capability had been built up, but the end of the war resulted in the disappearance of the market for military aircraft, and commercial aviation was as yet undeveloped. Bleriot liquidated SPAD, selling its factories and bringing key workers, including the head of design [[André Herbemont]], to the Blériot works at Suresnes. On 6 April 1919 Blériot, in association with other leading French aircraft manufacturers, established the ''[[Compagnie des Messageries Aériennes]]'' (CMA), and a prototype 28-seat airliner, the Type 75 Mammoth, based on the earlier Type 74 bomber, was exhibited at the Aero Salon in Paris in December 1919, along with three SPAD designs, the [[Blériot-Spad S.27|S.27]], S.29 and S.30. The Type 75 proved unsuccessful, but 10 examples of the S.27 were ordered by CMA, and a five-seat development, the [[Spad S.33|S.33]] was produced, first flying at the end of 1920.<ref>Elliott 2000, p.219</ref>  This was followed by the larger [[Spad S.46|S.46]]   
[[Image:Bleriot 500 cc tweecilinder 1919.jpg|thumb| Motorcycle Bleriot 5HP
tourist-type with sidecar]]
Attempts were made to diversify: a contract to build fishing boats was accepted, and another for a motorcycle which was produced at [[Suresnes]].  At the [[Paris Motor Show|15th Paris Motor Show]], in October 1919,  the company was promoting a motorcycle then in 1921 a stylish little [[cyclecar]] with a 2-[[Cylinder (engine)|cylinder]] 750cc [[two-stroke engine]] and shaft drive.<ref name=Automobilia1920>{{cite journal| authorlink = René Bellu  | title =Automobilia| journal = Toutes les voitures françaises 1920 (salon [Oct] 1919)| volume = Nr. 31|page=63|year = 2004|isbn = |publisher=Histoire & collections|location=Paris }}</ref> The French Blériot cyclecars are sometimes confused with the Blériot-Whippet chain-driven cycle cars made at the Blériot-owned factory in [[Addlestone]], England, but in fact the two vehicles had "little save size in common".<ref name="Georgano">{{cite book|last=Georgano|first=Nick|title=The Complete Encyclopaedia of Motorcars 1885-1968|year=1968|publisher=George Rainbird Ltd for Ebury Press Limited|location=London|isbn=|page=86}}</ref>

In 1922 Blériot Aéronautique, which had been a private company became a limited-liability company, ''Blériot Aéronautique S.A.''.<ref>Elliott 2000, p.220</ref>  Although a single company, aircraft were produced using both the Blériot and SPAD names, the former generally being used for the larger multi-engined aircraft, while the smaller single-engined aircraft bore the SPAD name, and it was these that were most successful.

The only aircraft produced under the Blériot name to be produced in any quantity was the [[Blériot 127|Type 127]], initially designed in 1925 as the Type 117 [[escort fighter]], and later adapted to become a bomber. 42 examples were bought by the French air force.

The last aircraft built under the Blériot name was a large [[flying boat]] designed in response to a French Air Ministry requirement for an aircraft for a transatlantic mail service between [[Dakar]] and [[Natal, Rio Grande do Norte|Natal]] in [[Brazil]]. The resulting aircraft, the [[Blériot 5190]] first flew in August 1933, and this prototype, named the ''Santos-Dumont'' proved highly successful, and a number of passenger carrying variants were planned. In May 1935, after it had completed its twelfth Atlantic crossing, the French government ordered three more examples, only to cancel the order six weeks later.<ref>Elliott 2000, p.237</ref>

In October 1936 the French government provided capital incentive to boost military aircraft production, bought and merged several manufacturers, including Blériot Aéronautique<ref>{{cite news|newspaper=The Times|location=London |date =17 October 1936|page=12|issue= 47508 |title=French Aircraft Manufacture}}</ref> into [[SNCASO]] (now [[Airbus]]).

==Aircraft==

===Blériot aircraft before the First World War===
*[[Blériot I]]     (1901)  Unmanned [[ornithopter]] powered by a carbonic acid engine.
*[[Blériot II]]    (1905)  Biplane floatplane glider built for Blériot by [[Gabriel Voisin]]. Crashed on first attempt at flight and abandoned.
*[[Blériot III]]   (1906)  Tandem-wing biplane powered by 24&nbsp;hp (18&nbsp;kW) Antoinette engine. Not successful.
*[[Blériot IV]]    (1906)  Modification of Type III, powered by two Antoinette engines. Not successful.
*[[Blériot V]]     (1907)  Single-seat, single-engine monoplane of [[canard (aeronautics)|canard]] configuration.
*[[Blériot VI]]    (1907)  Single-seat, single-engine aircraft of [[tandem wing]] configuration.
*[[Blériot VII]]   (1907)
*[[Blériot VIII]]  (1908)
*[[Blériot IX]]    (1908)  [[Tractor configuration]] monoplane. Never flown. Preserved in the collection of the Musée de l'Air in Paris.
*[[Blériot X]]     (1908)  [[Canard (aeronautics)|canard]] configuration biplane, never flown.
*[[Blériot XI]]    (1909)  Single-seat, single-engine [[tractor configuration]] monoplane. The type in which the first flight across the English Channel was made.
*[[Blériot XII]]   (1909)  Single-seat, single-engine high-wing monoplane.
*[[Blériot XIII]]  (1910) Five-seat [[pusher configuration]] biplane.
*[[Blériot XIV]]   (1910) Two-seat monoplane.
*[[Blériot XX]] (1910) Single seat monoplane with elongated triangular tailplane.
*[[Blériot XXI]] (1911) Two-seat military monoplane with elongated triangular tailplane. Exhibited at the 1911 Paris Aero Salon. One example was flown the [[1912 British Military Aeroplane Competition]].
*[[Blériot XXIII]] (1911) Racing monoplane with narrow-chord wings powered by a {{convert|100|hp|kW|abbr=on}} Gnome. Flown by [[Alfred Leblanc]] in the 1911 [[Gordon Bennett Trophy (aeroplanes)|Gordon Bennett Trophy]] competition, coming second.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200583.html "The Gordon Bennett Race at Eastchurch"][[Flight International|''Flight'']] 8 July 1911</ref>
*[[Blériot XXIV]] (1911) The Bleriot ''Limousine'', similar to the Type XIII but with an enclosed passenger cabin. Exhibited at the 1911 Paris Aero Salon.
*[[Blériot XXV]]  (1911) Single-seater pusher canard monoplane.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200840.html New Blériot Canard][[Flight International|''Flight'']] 30 September 1930</ref>
*[[Blériot XXVI]]  (1911) Single-seater pusher canard triplane. One built, probably not flown.
*[[Blériot XXVII]] (1911) Single-seat racing monoplane powered by a {{convert|50|hp|kW|abbr=on}} Gnome. One built, exhibited at the 1911 Paris Aero Salon. Preserved and on display at the [[RAF Museum]].<ref>{{cite web|url=http://www.rafmuseum.org.uk/research/collections/bl-riot-xxvii/|title= Blériot XXVII Aircraft History|publisher=RAF Museum|accessdate =27 April 2012}}</ref>
*[[Blériot XXVIII]] ''Populaire'' (1911) A version of the Type XI with a modified engine cowling, powered by a {{convert|35|hp|kW|abbr=on}} Anzani. Exhibited at the 1911 Paris Aero Salon
*[[Blériot XXIX]] (1912) Unbuilt pusher two-seat military observation monoplane.
*[[Blériot XXIX]] (1912) Unbuilt sports monoplane.
*[[Blériot XXXIII]] (1912) Two-seat canard monoplane powered by a {{convert|70|hp|kW|abbr=on}} Gnome.<ref>[http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%201224.html "The New Blériot Canard"][[Flight International|''Flight'']] 28 December 1912</ref>
*[[Blériot XXXVI]] (1912) Two-seat military monoplane, exhibited at the 1912 Paris Aero Salon. Circular section fuselage with a streamlined cowling enclosing the engine, rudder in two parts above and below the fuselage, and an undercarriage consisting of a pair of wheels on a cross-axle mounted on V-struts supplemented by a single central skid projecting forward.
*[[Blériot XXXVII]] (1913) Development of the Type XXV. Crashed at Buc on 25 November 1913, killing the pilot, [[Edmond Perreyon]]<ref>[http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201283.html "The Accident to Perreyon"][[Flight International|''Flight]] 29 November 1913</ref>

===Blériot aircraft during the First World War===
*[[Blériot 67]] Four-engined bomber, single prototype only.
*[[Blériot 73]] Four-engined bomber, single prototype only.
*[[Blériot 74]] Four-engined bomber, single prototype only.

===Blériot aircraft after the First World War===<!--Chronological order-->
*[[Blériot 75]] (1919) Four-engined airliner, developed from the Type 74<ref>Eliott 2000 p.211</ref>
*[[Blériot 115]] (1923) Four-engined airliner.
*[[Bleriot 118]] (1925) Twin-engined amphibian flying-boat fighter aircraft.
*[[Blériot 106]] (1924) Single-engined cabin monoplane.
*[[Blériot 135]] (1924) Development of the Type 115.
*[[Blériot 155]] (1925) Four-engined airliner.
*[[Blériot 165]](1926) Four-engined airliner.
*[[Blériot 127]] (1929) Twin-engined bomber.
*[[Blériot 195]] (1929) Four-engined Monoplane Mail-carrier in both land and floatplane configurations.
*[[Blériot 110]] (1930)  Single-seat, single-engine high-wing long-distance monoplane.
* [[Blériot 111]] (1929) Four-seat passenger transport aircraft.
*[[Blériot 125]] (1931) Twin-engined airliner carrying passengers in twin fuselages.
*[[Bleriot 290]] (1931) Single-engine light amphibian flying boat.
*[[Blériot 5190]]  (1933) Four-engine parasol-wing monoplane flying boat, intended as a transatlantic mail carrier.

==Blériot-SPAD aircraft==
* [[Blériot-SPAD S.20]]
* [[Blériot-SPAD S.27]]
* [[Blériot-SPAD S.29]]
* [[Blériot-SPAD S.30]]
* [[Blériot-SPAD S.33]]
* [[Blériot-SPAD S.34]]
* [[Blériot-SPAD S.46]]
* [[Blériot-SPAD S.51]]
* [[Blériot-SPAD S.56]]
* [[Blériot-SPAD S.61]]
* [[Blériot-SPAD S.66]]
* [[Blériot-SPAD S.81]] (1923) Single-seat biplane fighter.
* [[Blériot-SPAD S.510]] (1933) Single-seat biplane fighter.

==Notes==
{{reflist}}

==Sources==
*Elliot, Brian A. ''Blériot: Herald of an Age.'' Stroud: Tempus, 2000 ISBN 0-7524-1739-8
*[[G.N. Georgano]], Nick (Ed.) (2000). ''The Beaulieu Encyclopedia of the Automobile.'' Fitzroy Dearborn Publishers. ISBN 1-57958-293-1
*Opdycke, Leonard E. ''French Aeroplanes Before the Great War.'' Atglen, PA: Schiffer, 1999. ISBN 0-7643-0752-5

{{Louis Blériot aircraft}}

{{SNCASO}}
{{Defunct aircraft manufacturers of France}}
{{DEFAULTSORT:Bleriot Aeronautique}}
[[Category:Cyclecars]]
[[Category:Defunct aircraft manufacturers of France]]