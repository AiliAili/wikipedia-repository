<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = G-73 Mallard
 |image = Pearl aviation Grumman G-73 Mallard DRW Butler.jpg
 |caption = Grumman G-73T Turbo Mallard of [[Pearl Aviation]] landing at [[Darwin Airport]] (2010)
}}{{Infobox Aircraft Type
 |type = Amphibious airliner
 |manufacturer = [[Grumman]]
 |designer = 
 |first flight = 30 April 1946
 |introduced = 
 |retired = 
 |status = Retired
 |primary user = [[Chalk's Ocean Airways]]
 |more users = <!--up to three more. please separate with <br/>.-->
 |produced = 1946–1951
 |number built = 59
 |unit cost = $115,000 <br/> $4.0 million for turbine powered G-73T
 |variants with their own articles = 
}}
|}

The '''Grumman G-73 Mallard''' is a medium, twin-engined [[amphibious aircraft]]. Many have been modified by replacing the original [[Pratt & Whitney Wasp|Pratt & Whitney Wasp H]] radial engines with modern [[turboprop engine]]s. Manufactured from 1946 to 1951, production ended when Grumman's larger [[HU-16 Albatross|SA-16 Albatross]] was introduced.

==Design and development==
Building on the success of the [[Grumman Goose|Goose]] and [[Grumman Widgeon|Widgeon]], Grumman Aircraft developed  larger G-73 Mallard for commercial use. Retaining many of the features of the smaller aircraft, such as twin radial engines, high wings with underwing floats, retractable landing gear and a large straight tail, the company built 59 Mallards between 1946 and 1951. Unlike the smaller aircraft, the Mallard featured [[tricycle gear]], stressed skin, a two-step hull and wingtip fuel tanks.

==Operational history==
[[File:Grumman G-73 Ward Wells collection AMHA US-Gov.jpg|thumb|left|Northern Consolidated Air G-73 Mallard ([[Katmai National Park & Preserve]])]]
The Mallard prototype first flew on 30 April 1946, with the first production aircraft entering service in September of that year. While the Mallard was designed for regional airline operations with two pilots and ten passengers, especially aimed at harbor-based, city-to-city hops on the eastern seaboard, postwar surplus aircraft sales and the availability of smaller airports limited market potential. A number of smaller air carriers did use the Mallard in its intended role, notably Tahiti-Hawaii Airlines and [[Pacific Western Airlines]] (Canada). However, most of the 59 Mallards delivered were for corporate use. A prominent user in the United States was Roy Fruehauf and the [[Fruehauf Trailer Corporation]]. Fruehauf owned and operated a fishing camp, Killarney Lodge at Georgian Bay, Canada, and ferried customers there from Detroit.  Another, Detroiter, William Packer of General Motors, also owned a Mallard, which he often flew to Killarney.<ref>[http://singingwheels.com The Fruehauf Trailer Historical Society]</ref> another Mallard was purchased in the early 1950s by the [[Aga Khan III|Aga Khan]].

The Royal Egyptian Air Force used a Mallard as part of their Royal Flight; it was reportedly [[King Farouk]]'s favourite aircraft.<ref>Lees, Norman ''Birds of Passage. Pictorial memories of visitors to Malta 1948/1950'' Air Enthusiast No.74 March/April 1998 p.6 with photograph</ref>

The Mallard received a new lease on life in the 1970s when a number of airframes were refitted by [[Frakes Aviation]] with [[Pratt & Whitney Canada PT6A]] turbines and upgraded for 17 passengers, to become "Turbo Mallards." Today, through attrition, only 32 Mallards remain registered in the United States. Many of the rest are in use around the globe. 

A similar program has been undertaken by [[Paspaley|Paspaley Pearling]] in combination with [[Aero Engineers Australia|Aeronautical Engineers Australia]] to fit new engines and modernize its Mallard fleet, which is used to support its pearling operations in Northern Australia. The fleet has been extensively rebuilt and also refitted with PT6A turbines, and is currently undergoing a life extension program.

==Incidents==
===Chalks flight 101 in 2005===
[[File:Chalks Turbo Mallard at Bimini.jpg|thumb|right|Turbo Mallard of Chalks International Airline on a scheduled service at [[Bimini]], [[Bahamas]], in November 1989 after arriving from Miami Harbor.]]

Chalk's Ocean Airways purchased Mallard N1208 from the [[Fruehauf Corporation]] and later acquired several other examples of the aircraft. The type received much attention after a Turbo Mallard, operating as [[Chalk's Ocean Airways Flight 101]], crashed after takeoff from [[Miami]] Harbor on 19 December 2005 bound for Bimini, Bahamas. Eighteen [[passenger]]s and two [[aircrew member|crew]] perished when the right wing separated from the [[fuselage]] of the 58-year-old aircraft.<ref>https://www.youtube.com/watch?v=hfEqjxX4wTI</ref> The cause of the accident was determined by the subsequent investigation to be undetected cracks and/or [[corrosion]] in the [[wing]] [[spar (aviation)|spar]].

Prior to 2005, [[Chalk's Ocean Airways]] had an exemplary safety record operating Mallards for many years between [[Florida]] and the [[Bahamas]], having never had a passenger [[death|fatality]] since the [[company]] began operations in 1917.

===Australia Day 2017 at Perth===
An original radial-engine Mallard, registration VH-CQA, crashed into the [[Swan River (Western Australia)|Swan River]] in [[Perth, Western Australia]], on 26 January 2017 during [[Australia Day]] celebrations, killing both the pilot, [[Peter Lynch (mining engineer)|Peter Lynch]], and his passenger.<ref>{{Cite web|url=https://aviation-safety.net/database/record.php?id=20170126-0|title=ASN Aircraft accident Grumman G-73 Mallard VH-CQA Swan River, Perth, WA|last=Ranter|first=Harro|website=aviation-safety.net|access-date=2017-01-26}}</ref><ref>{{cite news|url=http://www.abc.net.au/news/2017-01-27/perth-australia-day-plane-crash-pilot-peter-lynch-remembered/8216560|title=Australia Day Perth plane crash: Victims remembered as authorities search for answers|publisher=Australian Broadcasting Corporation|date=27 January 2017|access-date=27 January 2017}}</ref>

==Operators==

===Civil operators===
;{{AUS}}
*[[Pearl Aviation|Paspaley Pearling Co.]]
;{{flag|Canada|1921}}
*[[Pacific Western Airlines]]
;{{USA}}
* Antilles Air Boats
*[[Chalk's Ocean Airways]] (''operated both the piston engine and turboprop engine variants'')
* Virgin Islands Seaplane Shuttle (''operated both the piston engine and turboprop engine variants'')

===Military operators===
;{{flag|Egypt|1922}}
*[[Egyptian Air Force|Royal Egyptian Air Force]]

==Specifications (G-73T)==
[[File:Chalk's Mallard.jpg|thumb|right|Chalk's Turbo Mallard at Abaco, 1999]]
{{Aircraft specs
|ref=<!-- reference -->
|prime units?=kts
<!--
        General characteristics
-->
|crew=two
|capacity={{convert|5,000|lb|kg|abbr=on|0}} payload or up to 17 passengers
|length m=14.7
|span m=20.3
|height m=5.72
|empty weight lb=8,750
|gross weight lb=14,000
|max takeoff weight lb=14,000
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Pratt & Whitney R-1340 Wasp|Pratt & Whitney R-1340]] radial engine (originally) or, if modified, [[Pratt & Whitney Canada PT6A-34]]|eng1 type=turboprop engines
|eng1 hp=600
<!--
        Performance
-->
|max speed kts=187
|cruise speed kts=157
|never exceed speed kts=187
|range nmi=1,120
|ceiling ft=24,500
|climb rate ftmin=1,350
|power/mass=.086 hp/lb (0.029 kW/kg)
}}

==See also==
{{aircontent|
|related=
* [[HU-16 Albatross]]
|similar aircraft=
* [[PBY Catalina]]
* [[Short Sealand]]
|lists=
* [[List of flying boats]]
|see also=
}}

==References==
;Notes
{{Reflist}}

;Bibliography
{{Refbegin}}
* Hotson, Fred W. and Matthew E. Rodina. ''Grumman Mallard: The Enduring Classic''. Scarborough, Ontario: Robin Brass Studio, 2006. ISBN 978-1-896941-44-8.
* Thruelsen, Richard. ''The Grumman Story''. New York: Praeger Publishers, Inc., 1976. ISBN 0-275-54260-2.
* Winchester, Jim, ed. "Grumman Goose/Mallard." ''Biplanes, Triplanes and Seaplanes (The Aviation Factfile)''. Rochester, Kent, UK: Grange Books plc, 2004. ISBN 1-84013-641-3.
{{refend}}

==External links==
{{Commons category|Grumman G-73}}
* [https://books.google.com/books?id=_CADAAAAMBAJ&pg=PA121&dq=Popular+Science+1946+motor+gun+boat&hl=en&ei=niXuTNPCG5WSnweB77yqCw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDMQ6AEwAQ#v=onepage&q=Popular%20Science%201946%20motor%20gun%20boat&f=true "Grumman Hatches A Mallard", November 1946, Popular Science]
* [https://www.nytimes.com/2005/12/20/national/20plane.html ''New York Times' article about Chalk's accident] 
* [http://www.amtonline.com/article/article.jsp?id=2493&siteSection=1 ''Aircraft Maintenance Technology'' article about Chalk's accident]
* [http://www.ntsb.gov/ntsb/brief.asp?ev_id=20060106X00018&key=1 NTSB preliminary report on Chalk's accident DCA06MA010 of 19 December 2005]
* [https://www.nytimes.com/imagepages/2005/12/19/national/19cnd-plane2ready.html Picture of a Chalk's Mallard in Miami in 1996]
* [http://www.answers.com/topic/flying-boat-inc-chalk-s-ocean-airways History of Chalk's Ocean Airways at answers.com]
* [http://travelthenet.com/columns/clips/mallard.htm Left at the Evening Star - selling a beloved Mallard]
*[http://www.singingwheels.com/grumman-mallard.html Grumman Mallard's and the Fruehauf Trailer Company]

{{Grumman aircraft}}

[[Category:Grumman aircraft|Mallard]]
[[Category:United States civil utility aircraft 1940–1949]]
[[Category:Flying boats]]
[[Category:Amphibious aircraft]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]