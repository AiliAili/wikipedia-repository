{{Use mdy dates|date=August 2016}}
{{Good article}}
{{Infobox artwork
| image_file=Artist's Studio Look Mickey.jpg
| backcolor=
| painting_alignment=
| image_size=250px
| title=Artist's Studio Look Mickey
| artist=[[Roy Lichtenstein]]
| year=1973
| movement=[[Pop art]]
| material=oil, Magna, sand on canvas
| height_metric   = 
| width_metric    = 
| height_imperial = 96.125
| width_imperial  = 128.125
| city=Minneapolis
| museum=[[Walker Art Center]]
}}
'''''Artist's Studio—Look Mickey''''' (sometimes '''''Artist's Studio, Look Mickey''''', '''''Artist's Studio &ndash; Look Mickey''''' or '''''Artist's Studio No. 1 (Look Mickey)''''') is a 1973 painting by [[Roy Lichtenstein]]. It is one of five large-scale studio interior paintings in a series.<ref name=lRL1>{{cite book|title=Lichtenstein|author=Cowart, Jack|page=72|quote=}}</ref> The series is either referred to as the Artist's Studio series or more colloquially as the Studios and sometimes is described as excluding the other 1973 painting, reducing the series to four.

The series refers to a set of works by [[Henri Matisse]], with this work specifically referring to ''[[L'Atelier Rouge]]''. The work incorporates several other Lichtenstein's works and gets its name from the large portion of Lichtenstein's ''[[Look Mickey]]'' that is included. Lichtenstein used a much more realistic representation of his own works than is standard for most artists. Elements of the work also refer to works from both [[Fernand Léger]] and Matisse.

==Background==
[[File:Atelier rouge matisse 1.jpg|thumb|left|''Artist's Studio—Look Mickey'' was inspired by [[Henri Matisse]]'s ''[[L'Atelier Rouge]]'', 1911.]]
Lichtenstein's studios reference what are known as [[Henri Matisse|Matisse]]'s four Symphonic Interiors of 1911 (''The Pink Studio'', ''The Painter's Family'', ''Interior with Eggplants'', ''[[L'Atelier Rouge|The Red Studio]]'') and an earlier Matisse ''[[Dance (Matisse)|The Dance]]''.<ref name=lRL1/> ''Artist's Studio—Look Mickey''<ref name=lRL1/> was part of a series that included ''The Artist's Studio &ndash; with Model'',<ref>{{cite book|title=Roy Lichtenstein|author=Hendrickson, Janis|publisher=[[Benedikt Taschen]]|year=1993|isbn=3-8228-9633-0|page=64|chapter=Lichtenstein Looks At Art|quote=}}</ref> 1974, ''Artist's Studio, Foot Medication'',<ref>{{cite book|title=Roy Lichtenstein|author=Hendrickson, Janis|publisher=[[Benedikt Taschen]]|year=1993|isbn=3-8228-9633-0|page=65|chapter=Lichtenstein Looks At Art|quote=}}</ref> 1974, ''Artist's Studio, the "Dance"'',<ref name=lRL1/> 1974, ''Artist's Studio/A Still Life'', 1973. ''Artist's Studio—Look Mickey'' was the only one of the five to include a corner of the room, like ''The Red Studio''.<ref name=lRL1/> Many sources, including Lichtenstein himself only include four works in the series (excluding ''Artist's Studio/A Still Life'', 1973). In a 1995 lecture in conjunction with the [[Kyoto Prize]], he said "I did a series of four large, about 8' x10', paintings of interiors of artists' studios. They were inspired by Matisse's paintings..."<ref name=B65>{{cite book|editor=Bader|pages=65&ndash;66|chapter=A Review of My Work Since 1961&mdash;A Slide Presentation|quote=}}</ref> ''Artist's Studio—Look Mickey'' is regarded as the first of the four Artist's Studio works.<ref name=B178/> From among the Artist's Studio series works, this depicts "the deepest, most plainly articulated interior space."<ref name=p73>Rondeau and Wagstaff. p. 73.</ref>

==Description==
The work, which is in part a [[retrospective]], "conflated early [[Modern art|modernism]] with emergent [[Postmodern art|postmodernism]]".<ref name=p50>Rondeau and Wagstaff. p. 50.</ref> Lichtenstein refers to some of his paintings, including ''[[Look Mickey]]'' in this work, which depicts his own studio as the ideal studio and implies that the public consensus ratifies his choice of popular culture subject matter.<ref>{{cite book|title=Roy Lichtenstein|author=Alloway, Lawrence|isbn=0-89659-331-2|publisher=[[Abbeville Press]]|year=1983|page=87|quote=}}</ref> The series depicts individual Lichtenstein works as well as groups of works in closed room that is ironically devoid of [[paint brush]]es or [[easel]]s.<ref name=RLH85>{{cite book|author=Hendrickson, Janis|page=85|chapter=Compilations, Syncopations, Discombobulations|title=Roy Lichtenstein|publisher=[[Benedikt Taschen]]|year=1993|isbn=3-8228-9633-0|quote=}}</ref> The series served as a review of Lichtenstein's post 1961 work, with objects of his prior works decorating the room as furnishings.<ref name=RLH85/>  In ''Artist's Studio—Look Mickey'', the couch, door, wall frieze, telephone and fruit all are drawn from earlier works and serve this setting as interior decoration, while ''Look Mickey'' is almost presented undisturbed in its entirety.<ref name=RLH86-87>{{cite book|author=Hendrickson, Janis|pages=86&ndash;87|chapter=Compilations, Syncopations, Discombobulations|title=Roy Lichtenstein|publisher=[[Benedikt Taschen]]|year=1993|isbn=3-8228-9633-0|quote=}}</ref> Less notable works include the mirror and the [[Trompe-l'œil]] painting of the rear side of the canvas.<ref name=RLH86-87/> Two other paintings were works in progress at the time of this work and one became a painting within a year after the completion of this work: the gull and the dune landscape. The [[speech balloon]] was never produced as a separate work.<ref name=RLH86-87/> However, its juxtaposition to the speech balloon from [[Donald Duck]] is intriguing.<ref name=p76>Rondeau and Wagstaff. p. 76.</ref> He references his Entablatures works as ceiling molding.<ref>{{cite book|title=Lichtenstein|author=Cowart, Jack|page=32|quote=}}</ref>

Lichtenstein's approach to presenting his own works within his works was non-traditional. The works were revisited as exact duplicates rather than the more standard distanced revisitation. This choice of exact duplication contrary to popular practice intrigued Lichtenstein.<ref name=B65/> Lichtenstein liked this quality of his paintings within his paintings, saying "I like the combination of a very separate quality that each of my paintings has within the painting, and the fact that everything works as one painting too."<ref name=RLOF51/> In fact, Lichtenstein commented on this as an attempt to eliminate any modulation:

{{Quote|text=A couple of years ago I started some paintings that had my own paintings in them, and which were similar to the Matisse studios. There was one difference that I think shows up mostly in the ''Look Mickey'': When I reproduce one of my own paintings in my painting, it's different from Matisse reproducing one of his paintings in his painting, because even though in both paintings the depicted painting is submerged for the good of the whole work, it's much more so in Matisse. I wanted my paintings to read as individual paintings with the work, so that there would be some confusion. There’s no remove in my work, no modulation or subtlety of line, so the painting-of-a-painting looks exactly like the painting it's of. This is not true, of course, of many early&mdash;including Renaissance&mdash; depictions of paintings on walls, where there’s always a remove indicated through modulation, or some other way of showing that the depicted painting is not pasted on the picture or something like that.|sign=Sources|source=<ref name=RLOF51>{{cite book|title=Roy Lichtenstein: October Files|editor=Bader, Graham|publisher=[[The MIT Press]]|year=2009|isbn=978-0-262-01258-4|page=51|chapter=Roy Lichtenstein in Conversation on Matisse|author=Lebensztejn, Jean-Claude}}</ref> }}

The Studio series was inspired by [[Henri Matisse|Matisse]] paintings, and Diane Waldman claims that this particular effort was modelled upon Matisse's ''[[L'Atelier Rouge]]'' (''The Red Studio''),<ref name=RLR>{{cite book|title=Roy Lichtenstein: Reflections|author=Waldman, Diane|publisher=Electa|year=1999|isbn=88-435-7287-3|page=39|quote=}}</ref><ref>{{cite book|title= Roy Lichtenstein|author=Waldman, Diane|year=1993|publisher=[[Solomon R. Guggenheim Museum]]|page=219|chapter=Still Lifes, 1972&ndash;76|isbn=0-89207-108-7|quote=}}</ref> although the Lichtenstein Foundation website credits two other Matisse works as inspiration as well as [[Fernand Léger]]'s ''The Baluster'', 1925, which is represented in the far right corner.<ref>{{cite web|url=http://image-duplicator.com/main.php?work_id=0592&year=1973&decade=70#|title=Artist's Studio "Look Mickey"|accessdate=May 15, 2012|publisher=Lichtenstein Foundation}}</ref> Léger was one of Lichtenstein's closest friends.<ref name=RLR/> By incorporating Matisse in his own studio setting, Lichtenstein is perceived as presenting himself as Matisse's peer and in so doing repositions pop art as a historical style rather than a contemporary one.<ref name=p50/>

A [[Sketch (drawing)|sketch]] for this work demonstrates Lichtenstein's original intent to incorporate a "plant-in-vessel arrangement" as well as fruit on the table, but these Matisse references were instead depicted on the floor of the studio.<ref name=p73-4>Rondeau and Wagstaff. pp. 73-4.</ref> The table instead includes the [[telephone]] from Lichtenstein's own ''R-R-R-R-Ring!!'' with differences from the original that amount to puns. In the original, Lichtenstein depicted sound with motion lines that present a pronounced movement of the object. This contrasts with the same phone in the studio setting in "perfect stasis", which contributes to the pacific still life setting.<ref name=p73-4/>

==Reception==
According to Janis Hendrickson, "For someone familiar with the artist's oeuvre, the Studios could become mental playgrounds."<ref name=RLH86-87/> Waldman notes that "Perhaps the most significant aspect of the work is its retrospective nature and the decision by the artist to reflect on his life, past and present."<ref name=RLR/> The small portion of the original that was cropped out was the majority of Mickey Mouse, emphasizing Donald Duck, who Graham Bader sees as Lichtenstein's metaphorical representation.<ref name=B178>{{cite book|editor=Bader|page=178|chapter=Donald's Numbness|quote=}}</ref>

==See also==
* [[1973 in art]]

==Notes==
{{Reflist|30em}}

==References==
*{{cite book|title=Roy Lichtenstein: October Files|editor=Bader, Graham|publisher=[[The MIT Press]]|year=2009|isbn=978-0-262-51231-2|pages=|chapter=|quote=}}
*{{cite book|title=Lichtenstein: Roy Lichtenstein 1970&ndash;1980|author=Cowart, Jack|year=1981|publisher=Hudson Hills Press, Inc.|isbn=0-933920-14-8|page=}}
*{{cite book|title=Roy Lichtenstein|author=Hendrickson, Janis|publisher=[[Benedikt Taschen]]|year=1993|isbn=3-8228-9633-0|pages=|chapter=|quote=}}
*{{cite book|title=Roy Lichtenstein: A Retrospective|publisher=[[Art Institute of Chicago]]|author=Rondeau, James and Sheena Wagstaff|isbn=978-0-300-17971-2|year=2012|editor=Rigas, Maia M.}}

==External links==
*[http://collections.walkerart.org/item/object/247 Walker Museum website]
*[http://image-duplicator.com/main.php?work_id=0592&year=1973&decade=70 Lichtenstein Foundation website]

{{Roy Lichtenstein}}

{{DEFAULTSORT:Artist's Studio-Look Mickey}}
[[Category:1973 paintings]]
[[Category:Paintings by Roy Lichtenstein]]