{{Infobox person
| name        = John Chamberlain
| image       = 
| image_size  = 
| caption     = 
| birth_name = John Rensselaer Chamberlain
| birth_date = {{birth date|1903|10|28}}
| birth_place = [[New Haven, Connecticut]], U.S.
| death_date = {{Death date and age|1995|4|9|1903|10|28}}
| death_place = [[New Haven, Connecticut]], U.S.
| death_cause =
| residence   = [[Cheshire, Connecticut]], U.S.
| other_names =
| known_for   = [[Libertarian|Libertarian thought]]
| education   = [[Yale University]]
| employer    = ''[[New York Times]]'' (1926-1930s)<br/>''[[Fortune magazine|Fortune]]'' (1936–1941)<br/> ''[[Life magazine|Life]]'' (1941–1950)<br/>''[[The Wall Street Journal]]'' (1950-1960)<br/>''[[The Freeman]]'' (1946-1995)<br/>''[[National Review]]'' (1955-1995)<br/>[[King Features]] (1960-1985)<br>[[Columbia University Graduate School of Journalism]]
| occupation  = Writer, Journalist, Literary Critic
| predecessor =
| successor   =
| party       = Conservative
| boards      =
| religion    = 
| spouse      = Ernestine Stodelle
| partner     =
| children    = 
| parents     = 
| relatives   =
| signature   =
| honors    = 
| website     =
| footnotes   =
| nationality = American
}}
'''John Rensselaer Chamberlain''' (October 28, 1903 – April 9, 1995) was an American [[journalist]], business and economic [[historian]], syndicated columnist and literary critic. He was dubbed "one of America’s most trusted book reviewers" by the classical liberal and  slightly [[Libertarianism in the United States|libertarian]] magazine ''[[The Freeman]]''.<ref name= "Opitz">Opitz, Edmund A., "A Reviewer Remembered: John Chamberlain 1903–1995," ''[[The Freeman]]'', June, 1995, vol. 45, iss. 6.</ref>

== Early life ==

Born in [[New Haven, Connecticut]] in 1903, John Chamberlain graduated from [[Yale University]] in 1925,<ref name=NYTimes/> where he was chairman of the campus humor magazine ''[[The Yale Record]]''.<ref>Carnes, Marc C., ed. (2005) ''American National Biography: Supplement 2''. New York: Oxford University Press. p. 84.</ref>

He began his career in journalism at the ''[[New York Times]]'' in 1926, serving there as both an [[editor]] and book reviewer during the 1930s.<ref name=NYTimes/> Later, he worked on the staff at ''[[Scribner's Magazine|Scribner's]]'' and ''[[Harper's magazine|Harper's]]'' magazines.<ref name= "Opitz"/>  Serving on the editorial staffs of ''[[Fortune magazine|Fortune]]'' (1936–1941) and ''[[Life magazine|Life]]'' (1941–1950),<ref name=NYTimes/> for a time he wrote the [[editorial]]s for ''Life'' under the direction of [[Henry Luce]], the founder of [[Time, Inc.]]

Chamberlain was a member of the [[Dewey Commission]] and a contributor to ''Not Guilty: the Report of the Commission of Inquiry into the Charges Made Against [[Leon Trotsky]] in the [[Moscow Show Trials|Moscow Trials]]'' (1938) by [[John Dewey]]. For most of this period, Chamberlain was, in his own words, "a New York literary liberal" involved in political causes of the Left.<ref>Chamberlain, ''A Life With the Printed Word'', p. 65.</ref>

He also taught [[journalism]] at the [[Columbia University Graduate School of Journalism]], where his students included the noted journalists [[Marguerite Higgins]], [[Elie Abel]] and [[Edith Efron]].<ref>Chamberlain, pp. 93–94.</ref>

== Changing political beliefs ==

{{Quote box |width=250px |align=right |quoted=true |bgcolor=#FFFFFF |salign=right
 |quote  =There is nothing like a fact to kill a theory.|source =John Chamberlain}}

In the early 1940s, Chamberlain moved to the intellectual Right, along with friends such as former [[communism|communists]] [[Whittaker Chambers]] and [[John Dos Passos]], although Chamberlain was never himself a [[communism|communist]].<ref>Diggins, ''Up From Communism''.</ref>  Influenced by [[Albert Jay Nock]], he credits the writers [[Ayn Rand]], [[Isabel Paterson]] and [[Rose Wilder Lane]] with his final "conversion" to what he called "an older American philosophy" of [[Libertarianism|libertarian]] and [[conservative]] ideas.<ref>Chamberlain, p. 136.</ref> Along with his friends [[Henry Hazlitt]] and [[Max Eastman]], he helped to promote the work of the Austrian economist [[F. A. Hayek]], ''[[The Road to Serfdom]]'', writing the "Foreword" to the first American edition of the book in 1944.

In 1946, [[Leonard Read]] of the [[Foundation for Economic Education]] established a free market magazine named ''[[The Freeman]]'', reviving the name of a publication which had been edited by [[Albert J. Nock]] (1920–1924). Its first editors included Chamberlain, Hazlitt and [[Suzanne La Follette]], and its contributors during Chamberlain's tenure there included [[James Burnham]], John Dos Passos, Max Eastman, [[Frank Meyer (political philosopher)|Frank Meyer]], [[Raymond Moley]], [[Morrie Ryskind]], and the [[Austrian economics|Austrian School]] economists [[Ludwig von Mises]] and F. A. Hayek.<ref>Chamberlain, p. 138; Hamilton, Charles H., "''The Freeman'': the Early Years," ''The Freeman'', Dec. 1984, vol. 34, issue 12.</ref> He joined the [[Neoliberalism|neoliberal]] [[Mont Pelerin Society]] during this period. After stepping down as editor of ''The Freeman'', Chamberlain continued his regular column for the periodical, "A Reviewer’s Notebook."

From 1950 to 1960, he was an editorial writer for ''[[The Wall Street Journal]]''.<ref>Chamberlain, pp. 136–139, and pp. 72–173.</ref>

[[William F. Buckley, Jr.]], credited Chamberlain with "changing the course of his life" by writing the "Introduction" to Buckley's first book, ''[[God and Man at Yale]]''.<ref>Chamberlain, p. 147.</ref> Later, Chamberlain became a lifelong contributing editor for Buckley's magazine, ''[[National Review]]'', from its founding until his death.  He still occasionally differed from Buckley; for example, he praised ''[[Atlas Shrugged]]'' by [[Ayn Rand]].<ref>Chamberlain, pp. 149–150.</ref>

For twenty-five years, he wrote a syndicated column for [[King Features]] which appeared in newspapers across the US.<ref name=NYTimes>{{cite news|title=John Chamberlain, Columnist, Dies at 91|url=https://www.nytimes.com/1995/04/13/obituaries/john-chamberlain-columnist-dies-at-91.html|accessdate=25 August 2014|publisher=New York Times|date=1995-04-13}}</ref>

After his first wife died in 1954, he married Ernestine Stodelle, who had previously been married to the Russian theatrical director [[Theodore Komisarjevsky]].<ref>{{cite news|last1=Beach|first1=Randall|title=Komisarjevsky's Father Testifies During Penalty Phase|url=http://www.countytimes.com/articles/2011/10/31/news/doc4ea854db434ae365431505.txt|accessdate=25 August 2014|publisher=Litchfield County Times|date=2011-10-31}}</ref>

== Books ==
* ''Farewell to Reform, Being a History of the Rise, Life and Decay of the Progressive Mind in America'' (1932)
* ''The American Stakes'' (1940)
* ''[https://mises.org/books/roots_of_capitalism_chamberlain.pdf The Roots of Capitalism]'' (1959)
* ''The Enterprising Americans: a Business History of the United States''' (Harper & Row, 1963)
* ''The National Review Reader''
* ''Freedom and Independence:  The Hillsdale Story'' (1979)
* ''A Life With the Printed Word'' (Regnery, 1982)
* ''The Turnabout Years'' (Jameson, 1991)<ref name=NYTimes/>

== References ==
{{reflist}}

== Sources ==
* {{cite book |last=Chamberlain|first=John|title=A Life With the Printed Word|year=1982|publisher=[[Regnery Publishing|Regnery Gateway]]|location=Chicago|isbn=0895266563|page=147}}
* Diggins, John P., ''Up From Communism'', Harper & Row, 1975.
* Opitz, Edmund A., "A Reviewer Remembered: John Chamberlain 1903–1995," ''The Freeman'', June, 1995 [http://www.thefreemanonline.org/featured/a-reviewer-remembered-john-chamberlain-1903-1995/] (retrieved 4-12-09).
* ''The New York Times'', "John Chamberlain, Columnist, Dies at 91," April 13, 1995 [https://www.nytimes.com/1995/04/13/obituaries/john-chamberlain-columnist-dies-at-91.html] (retrieved 4-12-09).
* [https://books.google.com/books?id=wZczV8ZxgL4C&pg=PA84 Mark Christopher Carnes, Paul R. Betz – ''American National Biography: Supplement'']

==External links==
* {{FAG|37566314|John Rensselaer Chamberlain}}

{{Authority control}}

{{DEFAULTSORT:Chamberlain, John}}
[[Category:1903 births]]
[[Category:1995 deaths]]
[[Category:20th-century American writers]]
[[Category:20th-century historians]]
[[Category:American literary critics]]
[[Category:American libertarians]]
[[Category:Libertarian historians]]
[[Category:The New York Times writers]]
[[Category:The Wall Street Journal people]]
[[Category:Yale University alumni]]