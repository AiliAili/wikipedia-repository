{{good article}}
{{Planetbox begin
| name = 47 Ursae Majoris c
}}
{{Planetbox star
| star = [[47 Ursae Majoris]]
| constell = [[Ursa Major]]
| RA = {{RA|10|59|28.0}}
| DEC = {{DEC|+40|25|49}}
| app_mag = 5.03
| dist_ly = 45.9
| dist_pc = 14.06
| class = G1V
| mass = 1.08
| radius = 1.172 ± 0.111
| temperature = 5887 ± 3.8
| metallicity = 0.04
| age = 6.03
}}
{{Planetbox orbit
| semimajor = 3.6 ± 0.1<ref name="bayesian">
{{cite journal
 |author=P. C. Gregory
 |author2=D. A. Fischer
 |date=2010
 |title=A Bayesian periodogram finds evidence for three planets in 47 Ursae Majoris
 |journal=[[Monthly Notices of the Royal Astronomical Society]]
 |volume=403 |issue=2 |pages=731
 |doi=10.1111/j.1365-2966.2009.16233.x
|bibcode=2010MNRAS.403..731G
|arxiv = 1003.5549 }}</ref>
| semimajor_gm = ~540
| semimajor_mas = ~260
| periastron = 3.3{{±|0.4|0.3}} 
| periastron_gm = ~490
| apastron = 4.0{{±|0.2|0.5}}
| apastron_gm = ~600
| eccentricity = 0.098{{±|0.047|0.096}}<ref name="bayesian" />
| period = 2391{{±|100|70}}<ref name="bayesian" />
| period_year = ~6.55
| speed = 16.5{{±|1.1|1.0}} 
| arg_peri = 295{{±|114|160}}<ref name="bayesian" />
| t_peri = 2,452,441{{±|628|825}}<ref name="bayesian" />
| semi-amp = 7.0 ± 2.3<ref>
{{cite web
 |title=Planets Table
 |url=http://exoplanets.org/planets.shtml
 |work=[[Catalog of Nearby Exoplanets]]
 |accessdate=2008-10-04
| archiveurl= https://web.archive.org/web/20080921002236/http://exoplanets.org/planets.shtml| archivedate= 21 September 2008 <!--DASHBot-->| deadurl= no}}</ref>
}}
{{Planetbox character
| minimum_mass = 0.540{{±|0.066|0.073}}<ref name="bayesian" />
| stellar_flux = 0.115
| temperature = 152
}}
{{Planetbox discovery
| discovery_date = 15 August 2001<br>19 March 2002 (confirmed)
| discoverers = [[Debra Fischer|Fischer]],<br>[[R. Paul Butler|Butler]], and<br>[[Geoffrey Marcy|Marcy]] ''et al.''
| discovery_site = {{flag|United States}}
| discovery_method = [[Doppler spectroscopy]]
| discovery_status = Confirmed
}}
{{Planetbox catalog
| names = Taphao Kaew, 47 UMa c,<ref name="Raghavan">{{cite journal
 |author=D. Raghavan
 |date=2009
 |title=A Survey of Stellar Families: Multiplicity of Solar-type Stars
 |url=http://www.chara.gsu.edu/~raghavan/dissertation/Dissertation_Main_thru_AppendixA.pdf
 |work=PhD Thesis
 |publisher=[[Georgia State University]]
 |pages=224–226
}}</ref> [[Henry Draper catalogue|HD]] 95128 c<ref name="Milone">
{{cite book
 |author=E. F. Milone
 |author2=W. J. F. Wilson
 |date=2008
 |title=Solar System Astrophysics: Planetary Atmospheres and the Outer Solar
 |url=https://books.google.com/books?id=cf7SdsRK-fkC
 |publisher=[[Springer (publisher)|Springer]]
 |series=Solar System Astrophysics
 |volume=2 |pages=xv, 328, 339, 349
 |isbn=0-387-73153-9
}}</ref><ref group="note">These alternative planetary designations are taken from the alternative designations of the host star, and are used in scientific papers occasionally for some exoplanets (see Milone & Wilson 2008 and Raghavan 2009). The most commonly used star designations are Bayer, Flamsteed, HD, HIP, HR, and Gliese.</ref>
}}
{{Planetbox reference
| star = 47+UMa
| planet = c
}}
{{Planetbox end}}

'''47 Ursae Majoris c''' (abbreviated '''47 UMa c'''), also named '''Taphao Kaew''' ({{lang-th|ตะเภาแก้ว}}, {{RTGS|Taphaokaeo}}, {{IPA-th|tā.pʰāw.kɛ̂ːw|pron}}), is an [[extrasolar planet]] approximately 46 [[light-year]]s from Earth in the [[constellation]] of [[Ursa Major]]. The planet was discovered located in a long-[[orbital period|period]] around the star [[47 Ursae Majoris]]. Its orbit lasts 6.55 years and the planet has a mass at least 0.540 times that of [[Jupiter]].

In July 2014 the [[International Astronomical Union]] launched a process for giving proper names to certain exoplanets and their host stars.<ref>[http://www.iau.org/news/pressreleases/detail/iau1404/ NameExoWorlds: An IAU Worldwide Contest to Name Exoplanets and their Host Stars]. IAU.org. 9 July 2014</ref> The process involved public nomination and voting for the new names.<ref>[http://nameexoworlds.iau.org/process NameExoWorlds The Process]</ref> In December 2015, the IAU announced the winning name was Taphao Kaew for this planet.<ref>[http://www.iau.org/news/pressreleases/detail/iau1514/ Final Results of NameExoWorlds Public Vote Released], International Astronomical Union, 15 December 2015.</ref> The winning name was submitted by the Thai Astronomical Society of [[Thailand]]. Taphaokaeo was one of two sisters associated with a Thai folk tale.<ref>[http://nameexoworlds.iau.org/names NameExoWorlds The Approved Names]</ref>

==Discovery==
[[File:47UMaOrbits.svg|thumb|left|Orbits of the 47 Ursae Majoris system planets. 47 UMa c is the middle planet.]]
Like the majority of known extrasolar planets, 47 Ursae Majoris c was discovered by detecting changes in its star's radial velocity caused by the planet's [[gravity]]. This was done by measuring the [[Doppler shift]] of the star's [[spectrum]].

At the time of discovery in 2001, 47 Ursae Majoris was already known to host one extrasolar planet, designated [[47 Ursae Majoris b]]. Further measurements of the radial velocity revealed another periodicity in the data unaccounted for by the first planet. This periodicity could be explained by assuming that a second planet, designated 47 Ursae Majoris c, existed in the system with an [[orbital period]] close to 7 years. Observations of the [[photosphere]] of 47 Ursae Majoris suggested that the periodicity could not be explained by stellar activity, making the planet interpretation more likely. The planet was announced in 2002.<ref name="fischer">
{{cite journal
 | author=D. A. Fischer
 | date=2002
 | title=A Second Planet Orbiting 47 Ursae Majoris
 | journal=[[Astrophysical Journal]]
 | volume=564 | issue=2 | pages=1028–1034
 | doi=10.1086/324336
| bibcode=2002ApJ...564.1028F
 | name-list-format=vanc
 | display-authors=1
 | last2=Marcy
 | first2=Geoffrey W.
 | last3=Butler
 | first3=R. Paul
 | last4=Laughlin
 | first4=Gregory
 | last5=Vogt
 | first5=Steven S.
}}</ref>

Further measurements of 47 Ursae Majoris failed to detect the planet, calling its existence into question. Furthermore, it was noted that the data used to determine its existence left the planet's parameters "almost unconstrained".<ref>
{{cite journal
 |author=D. Naef
 |date=2004
 |title=The ELODIE survey for northern extra-solar planets. III. Three planetary candidates detected with ELODIE
 |journal=[[Astronomy and Astrophysics]]
 |volume=414 |pages=351–359
 |bibcode=2004A&A...414..351N
 |doi=10.1051/0004-6361:20034091
|arxiv = astro-ph/0310261
 |name-list-format=vanc
 |display-authors=1
 |last2=Mayor
 |first2=M.
 |last3=Beuzit
 |first3=J. L.
 |last4=Perrier
 |first4=C.
 |last5=Queloz
 |first5=D.
 |last6=Sivan
 |first6=J. P.
 |last7=Udry
 |first7=S. }}</ref> A more recent study with datasets spanning over 6,900 days came to the conclusion that while the existence of a second planet in the system is likely, periods around 2,500 days have high false-alarm probabilities, and gave a best-fit period of 7,586 days (almost 21 years).<ref name="wittenmyer">
{{cite journal
 |author=R. A. Wittenmyer
 |author2=M. Endl
 |author3=W. D.Cochran
 |date=2007
 |title=Long-Period Objects in the Extrasolar Planetary Systems 47 Ursae Majoris and 14 Herculis
 |journal=[[Astrophysical Journal]]
 |volume=654 |issue=1 |pages=625–632
 |bibcode=2007ApJ...654..625W
 |doi=10.1086/509110
|arxiv = astro-ph/0609117 }}</ref>

In 2010, a study was published that determined that there are three giant planets orbiting 47 Ursae Majoris, including one at 2,391 days that corresponds well with the original claims for 47 Ursae Majoris c.<ref name="bayesian"/>

==Physical characteristics==
Since 47 Ursae Majoris c was detected indirectly, properties such as its [[radius]], composition, and [[temperature]] are unknown. Based on its high mass, the planet is likely to be a [[gas giant]] with no [[solid]] surface.

==See also==
{{Wikipedia books|47 Ursae Majoris}}
*[[Stars and planetary systems in fiction#47 Ursae Majoris|47 Ursae Majoris in fiction]]

==Footnotes==
{{reflist|group="note"}}

==References==
{{reflist}}

==External links==
* {{cite web |url=http://exoplanet.eu/planet.php?p1=47+Uma&p2=c |title=Notes for Planet 47 Uma c |author=Jean Schneider |date=2011 |publisher=[[Extrasolar Planets Encyclopaedia]] |accessdate=3 October 2011}}
*{{cite web
 |url=http://www.solstation.com/stars2/47uma.htm
 |title=47 Ursae Majoris
 |accessdate=2008-06-26
 |work=SolStation| archiveurl= https://web.archive.org/web/20080511142234/http://www.solstation.com/stars2/47uma.htm| archivedate= 11 May 2008 <!--DASHBot-->| deadurl= no}}

{{47 Ursae Majoris}}
{{Sky|10|59|28.0|+|40|25|49|45.9}}

[[Category:47 Ursae Majoris]]
[[Category:Exoplanets discovered in 2001]]
[[Category:Giant planets]]
[[Category:Exoplanets detected by radial velocity]]
[[Category:Exoplanets with proper names]]