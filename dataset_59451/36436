{{Infobox Officeholder
| name        = Leonore Annenberg
| birthname   = Leonore Cohn
| image       = Leonore Annenberg.jpg
| image_size  = 200px
| caption =
| birth_date  = {{birth date|mf=yes|1918|2|20}}
| birth_place = [[New York City, New York]], United States
| death_date  = {{death date and age|mf=yes|2009|3|12|1918|2|20}}
| death_place = [[Rancho Mirage, California]], USA
| restingplace = [[Sunnylands]]<ref>{{Find a Grave|34747368|Leonore 'Lee' ''Cohn'' Annenberg|work=Businesswoman|author=Bob Hufford|date=March 13, 2009|accessdate=July 1, 2011}}</ref>
| residence   = [[Rancho Mirage, California]], USA
| nationality = [[United States|American]]
| order       = [[Chief of Protocol of the United States]]
| networth    = [[United States dollar|US$]]2 billion
| term        = March 20, 1981–January 6, 1982
| president = [[Ronald Reagan]]
| predecessor = Abelardo L. Valdez
| successor   = Selwa Roosevelt
| spouse      = [[Beldon Katleman]] (divorced; 1 child)<br> Lewis Rosensteil (divorced; 1 child) <br> [[Walter Annenberg]] (1951–2002)
| website     = [http://www.annenbergfoundation.org/ Annenberg Foundation]
|partner      =  <!--For those with a domestic partner and not married-->
|relations    = 
|children     = 
|alma_mater   = 
|occupation   = 
|profession   = 
|cabinet      = 
|committees   = 
|portfolio    = 
|religion     = [[Christian Science|Christian Scientist]]<ref name=PalmSpringLife />
|ethnicity    = Jewish<ref name=PalmSpringLife />
|signature    = 
|footnotes    = 
}}

'''Leonore Cohn Annenberg''' (February 20, 1918 – March 12, 2009), also known as '''Lee Annenberg''', was an American [[businesswoman]], [[government official]], and [[philanthropist]], noted for serving as [[Chief of Protocol of the United States]] from 1981 to 1982. Annenberg was married to [[Walter Annenberg]], who was an [[United States Ambassador to the United Kingdom|Ambassador to the United Kingdom]] and newspaper publishing magnate. She also served as the chairman and president of the [[Annenberg Foundation]] from 2002 until 2009.

Born in [[New York City]] and raised in [[Los Angeles]], she graduated from [[Stanford University]]. After her first two marriages ended in divorce, she married noted businessman Walter Annenberg, who was appointed U.S. Ambassador to the United Kingdom in 1969 under President [[Richard Nixon]]. In her role as the ambassador's wife, Leonore directed a major renovation of the ambassador's official residence. The Annenbergs contributed to [[Ronald Reagan]]'s 1980 presidential campaign and upon his inauguration, Leonore was named [[Chief of Protocol of the United States|Chief of Protocol]], placing her in charge of advising the president, vice president, and Secretary of State on matters relating to [[diplomatic protocol]].

The Annenbergs became major philanthropists, donating money to education facilities, [[Charitable organization|charitable causes]], and the arts. Leonore served on many committees and boards as well. After her husband's death in 2002, she continued to donate money and succeeded him as chairman and president of the Annenberg Foundation.

==Early life and family==
'''Leonore Cohn''' was born into a [[American Jews|Jewish]] family<ref name=PalmSpringLife>[http://www.palmspringslife.com/Palm-Springs-Life/February-2011/Return-to-Sunnylands-First-in-an-eight-part-series/ Palm Spring Life: "Return to Sunnylands" BY LINDA L. MEIERHOFFER] February 2011</ref> in [[New York City]] on February 20, 1918, to Maxwell and Clara Cohn.<ref>{{cite news|url=http://www.timesonline.co.uk/tol/comment/obituaries/article5913688.ece|title=Leonore Annenberg: philanthropist|work=The Times of London|date=March 16, 2009|accessdate=2009-03-16}}</ref> Nicknamed "Lee", her father operated a textile business. She was seven years old when her mother died.<ref name="lat-obit"/> She and her younger sister were raised in [[Fremont Place]], an upper-class neighborhood of [[Los Angeles]], by her uncle [[Harry Cohn]], the founder of [[Columbia Pictures]].<ref name="lat-obit"/> Leonore and her younger sister, Judith, attended the Page Boarding School for Girls in [[Pasadena, California|Pasadena]]. Harry Cohn's wife, Rose, raised the girls as [[Christian Scientist]]s.<ref name="lat-obit"/>

Leonore Cohn graduated from [[Stanford University]] in 1940 with a [[bachelor's degree|B.A.]]<ref name="american ambassadors">{{cite web|title=Leonore Annenberg |url=http://www.americanambassadors.org/index.cfm?fuseaction=Members.view&memberid=34|publisher=Council of American Ambassadors|year=2004|accessdate=2007-11-24}}</ref> After graduating, she married [[Beldon Katleman]], whose family owned real estate and a national parking lot chain; they had a daughter, Diane, but the marriage ended in divorce after a few years.<ref name="lat-obit">{{cite news|url=http://www.latimes.com/news/obituaries/la-me-leonore-annenberg13-2009mar13,0,1246571,full.story|title=Leonore Annenberg dies at 91; philanthropist was Reagan's protocol chief|author=Nelson, Valerie J|date=March 12, 2009|accessdate=2009-03-12|work=The Los Angeles Times}}</ref> In 1946, she married Lewis Rosensteil, the multimillionaire founder of the Schenley liquor distillery, and they had a daughter named Elizabeth; that marriage, too, ended in divorce.<ref name="lat-obit"/>

She and [[Walter Annenberg]], then editor of ''[[The Philadelphia Inquirer]]'', met in 1950 at a party in Florida<ref name="lat-obit"/> and the two were married the following year.<ref name="wa dies">{{cite news|title=Publisher, Philanthropist Walter Annenberg Dies|url=http://www.washingtonpost.com/ac2/wp-dyn/A30335-2002Oct1?language=printer|publisher=''[[The Washington Post]]''|date=2002-10-02|page=A01|accessdate=2007-11-24}}</ref>

==Ambassador's wife==
Upon her husband's appointment as the United States Ambassador to the United Kingdom in 1969, Mrs. Annenberg ordered a renovation of the thirty-five room [[Winfield House]], the ambassador's official [[London, England|London]] residence. The total cost of the project was about [[United States dollar|US$]]1 million and took six months to complete.<ref name="wadies-nyt">{{cite news|title=Walter Annenberg, 94, Dies; Philanthropist and Publisher|url=https://query.nytimes.com/gst/fullpage.html?res=9800E3DE1038F931A35753C1A9649C8B63&sec=&spon=&pagewanted=4|publisher=''[[The New York Times]]''|date=2002-10-02|page=4|accessdate=2007-11-24|first=Grace|last=Glueck}}</ref> While in London, Leonore founded the American Friends of Covent Garden,<ref name="american ambassadors"/> an organization designed to foster goodwill between the U.S. and Great Britain through musical expression.<ref>{{cite web|title=British-American Organizations in the New York Area|url=http://www.britainusa.com/consular/articles_show_nt1.asp?i=34004&L1=34000&L2=34004&a=39404|publisher=American British Embassy|accessdate=2007-11-25|archiveurl=https://web.archive.org/web/20060626062312/http://www.britainusa.com/consular/articles_show_nt1%2Easp?i%3D34004%26L1%3D34000%26L2%3D34004%26a%3D39404 <!--Added by H3llBot-->|archivedate=2006-06-26}}</ref>

==Chief of Protocol==
[[File:Annenbergs with Ronald Reagan 1981 cropped.jpg|thumb|right|Walter and Leonore Annenberg with President [[Ronald Reagan]], 1981]]
[[File:Leonore Annenberg with Presidents Nixon, Ford, Carter.jpg|thumb|right|Annenberg with former Presidents Nixon, Ford, and Carter during a flight to the funeral of Anwar Sadat, October 1981]]

The Annenbergs contributed substantially to [[Ronald Reagan]]'s 1980 presidential campaign, and upon Reagan's election in 1981, Lee Annenberg was named as [[Chief of Protocol of the United States]]. This position placed her in charge of advising the [[President of the United States|President]], [[Vice President of the United States|Vice President]], and [[United States Secretary of State|Secretary of State]] on matters dealing with [[diplomatic protocol]], and formally welcoming foreign [[dignitary|dignitaries]] upon their arrival to the United States.<ref name="leonore cop">{{cite web|title=Nomination of Leonore Annenberg To Have the Rank of Ambassador While Serving as Chief of Protocol for the White House|url=http://www.presidency.ucsb.edu/ws/index.php?pid=43365|publisher=americanpresidency.org|accessdate=2007-11-24|first=Peter|last=Gerhard}}</ref> Annenberg oversaw a staff of 60 who worked on myriad details, ranging from the choice of the state gifts that will be given to the guest, to the bathrooms the foreign delegation may visit.<ref>{{cite news|url= https://query.nytimes.com/gst/fullpage.html?res=9B0DE6D7153AF931A35751C1A961948260&sec=&spon=&pagewanted=all|title=Why It's Black Tie at White House|author=Gamarekian, Barbara|work=The New York Times|accessdate=2008-06-26|date=December 2, 1987}}</ref> She said of her position, "It's all about making your guests feel respected and welcome".<ref>{{cite book|author=Schifando, Peter|author2=J. Jonathan Joseph |year=2007|publisher=William Morrow|location=New York|page=43|title=Entertaining at the White House with Nancy Reagan}}</ref> Annenberg attracted some controversy during her tenure when she curtsied before the visiting [[Prince Charles]] upon arriving for a diplomatic visit, commentators saying it was unseemly in a republic which gained its independence from the same monarchy.<ref>{{cite news|url=http://www.csmonitor.com/1981/0514/051427.html|title=The great curtsy controversy|author=Melvin Maddocks|work=The Christian Science Monitor|accessdate=2011-09-26|date=May 14, 1981 }}</ref>

As Chief of Protocol, she achieved the rank of [[Ambassador of the United States|Ambassador]].<ref name="leonore cop"/> Friends of [[Ronald Reagan|Ronald]] and [[Nancy Reagan]], the Annenbergs hosted the Reagans annually at their [[Rancho Mirage, California]] estate, "[[Sunnylands]]".<ref name="wadies-nyt"/> Annenberg resigned her post in January 1982, stating that she wanted to spend more time with her husband.<ref>{{cite news|title=Tonight, big giver will be receiving|url=http://www.philly.com/inquirer/home_top_stories/20070618_Tonight__big_giver_will_be_receiving.html|publisher=''[[The Philadelphia Inquirer]]''|date=2007-06-18|accessdate=2007-11-25|first=Karen|last=Heller}} {{Dead link|date=September 2010|bot=H3llBot}}</ref>

==Philanthropy and committee work==
After leaving her post at the [[United States Department of State|State Department]], Lee Annenberg began work to promote and enhance cultural appreciation in the United States.<ref name="trust">{{cite web|url=http://www.sunnylandstrust.org/about/about_show.htm?doc_id=436150|publisher=The Annenberg Foundation Trust at Sunnylands|title=Leonore Annenberg|accessdate=2008-06-26}}</ref> She and her husband continued to donate money to worthy causes as [[philanthropist]]s. In 2001, Annenberg was awarded the Andrew Carnegie Medal of Philanthropy.<ref>{{cite web|url=http://www.carnegie.org/sub/awardees/annenbergs.html|title=Ambassador Leonore Annenberg|accessdate=2008-06-26|year=2001|publisher=Carnegie Corporation of New York |archiveurl = https://web.archive.org/web/20080515222317/http://www.carnegie.org/sub/awardees/annenbergs.html |archivedate = May 15, 2008}}</ref> Upon being presented the award, she explained why she and her husband donated to causes as philanthropists:

<blockquote>"Walter and I believe that education is the foundation of a democratic society. When asked what motivates his philanthropic work, my husband has responded with a very powerful statement: 'I regard my philanthropic work as an investment in the future of America. It is the most effective way I can serve my country and help to ensure its benefits for the next generation.'"<ref name="trust"/></blockquote>

Mrs. Annenberg served many charitable organizations and on many committees.<ref name="trust"/> Stemming from her interest in and endowments to the arts, she was a trustee emerita and a member of the Acquisitions Committee of the [[Metropolitan Museum of Art]], member of the Board of Trustees of the [[Philadelphia Museum of Art]], one of the managing directors of The [[Metropolitan Opera]], honorary trustee and former board president of the Palm Springs Art Museum, and a member of the [[American Philosophical Society]].<ref name="trust"/> Mrs. Annenberg was chairwoman emerita of the Foundation of Art and Preservation in Embassies, and a member of the [[Committee for the Preservation of the White House]].<ref name="trust"/>

She was also a member of the Distinguished Daughters of Pennsylvania and an active trustee emeritus of the [[University of Pennsylvania]].<ref name="trust"/> She served on the governing boards of both [[Annenberg School for Communication at the University of Pennsylvania|Annenberg Schools for Communication]].<ref name="trust"/> In 1993, she and her husband, [[Walter Annenberg|Walter]], were awarded the [[National Medal of Arts]].<ref>[http://www.nea.gov/honors/medals/medalists_year.html#93 Lifetime Honors - National Medal of Arts] {{webarchive |url=https://web.archive.org/web/20130826194408/http://www.nea.gov/honors/medals/medalists_year.html#93 |date=August 26, 2013 }}</ref> She was elected a Fellow of the [[American Academy of Arts and Sciences]] in 2004.<ref name=AAAS>{{cite web|title=Book of Members, 1780-2010: Chapter A|url=http://www.amacad.org/publications/BookofMembers/ChapterA.pdf|publisher=American Academy of Arts and Sciences|accessdate=19 April 2011}}</ref>

==Later life==
Walter Annenberg died on October 1, 2002, aged 94.<ref name="wa dies"/> Lee Annenberg succeeded her husband as
chairman and president of the [[Annenberg Foundation]], an organization founded by her late husband which funds nonprofit organizations as well as education institutes and programs of the arts. She continued to donate money to worthy causes in the fields of science, education, and art until her death.<ref name="forbes 400 usa">{{cite web|title=#133 Leonore Annenberg|url=http://www.forbes.com/lists/2005/54/MPF2.html|publisher=Forbes.com|accessdate=2007-11-25|year=2005}}</ref> In 2006, she was ranked as the 382nd wealthiest person in the world by ''[[Forbes]]'' magazine,<ref>{{cite web|title=#382 Leonore Annenberg|url=http://www.forbes.com/lists/2006/10/MPF2.html|publisher=Forbes.com|accessdate=2007-11-25|year=2006}}</ref> and the 488th in 2007.<ref>{{cite web|title=#488 Leonore Annenberg|url=http://www.forbes.com/lists/2007/10/07billionaires_Leonore-Annenberg_MPF2.html|publisher=Forbes.com|accessdate=2008-02-11|year=2007}}</ref> Annenberg, in 2007, was the 165th richest person in the United States, according to ''Forbes'', with a net worth of 2.5 billion dollars.<ref>{{cite news|url=http://www.usatoday.com/money/2007-09-20-forbes-list_N.htm|title=2007 'Forbes' 400 richest Americans|accessdate=2008-04-11|work=Forbes|publisher=''USA Today'' | date=2007-09-20}}</ref> Annenberg traveled to [[Washington, D.C.]] in May 2007 to attend the [[state dinner]] for [[Queen Elizabeth II]], hosted by President [[George W. Bush]].<ref>{{cite news|url=https://query.nytimes.com/gst/fullpage.html?res=9F01E3D81631F93BA35756C0A9619C8B63|title=Guests at the State Dinner|accessdate=2008-06-26|date=May 8, 2007|work=The New York Times}}</ref> The following month, she accepted the prestigious Philadelphia Award, an honor given to those in the Philadelphia region who worked to better the area.<ref>{{cite news|url=http://findarticles.com/p/articles/mi_km4470/is_200704/ai_n19027672|publisher=FindArticles|accessdate=2008-06-26|work=The Philadelphia Daily News|date=April 2007|author=Brennan, Chris|title=Philadelphia Award to Leonore Annenberg}} {{Dead link|date=October 2010|bot=H3llBot}}</ref>

Most recently before her death, Annenberg became an honorary board member of the [[Richard Nixon Foundation]] and an honorary fellow of the [[Royal Academy of Arts]].<ref name="trust"/>

==Death==
{{Wikinews| Billionaire philanthropist Leonore Annenberg dies at 91}}
Lee Annenberg resided in [[Rancho Mirage, California]] prior to her death on March 12, 2009, aged 91. According to a family spokesperson, Leonore Annenberg died at [[Eisenhower Medical Center]] of natural causes.
<ref>{{cite news|url=http://www.iht.com/articles/2009/03/12/america/12annenberg.php|title= Leonore Annenberg, doyenne of protocol, dies at 91|author=Mcfadden, Robert D|work=International Herald Tribune|date=March 12, 2009|accessdate=2009-03-12}}</ref> At the time of her death, Annenberg had been in declining health.<ref>{{cite news|url=http://www.philly.com/philly/hp/news_update/41142322.html |work=The Philadelphia Inquirer |accessdate=2009-03-12 |date=March 12, 2009 |author=Heller, Karen |title=Billionaire philanthropist Leonore Annenberg, 91, dies; Philadelphia benefited from her family’s largesse |deadurl=yes |archiveurl=https://web.archive.org/web/20090313234933/http://www.philly.com/philly/hp/news_update/41142322.html |archivedate=March 13, 2009 }}</ref>

At the announcement of her death, statements were issued by former President [[George H. W. Bush]] and [[Barbara Bush]], as well as former First Lady [[Nancy Reagan]]. Mrs. Reagan called Annenberg "a dear and longtime friend" and praised the Annenbergs' philanthropic work as having "left an indelible print on education in the United States".
<ref name="lat-obit"/>

==See also==
* [[Annenberg Public Policy Center]]

==References==
{{reflist|2}}

==External links==
{{Commons category|Leonore Annenberg}}
*[http://www.legacy.com/Philly/DeathNotices.asp?Page=Lifestory&PersonID=125046730 AP Obituary] in the [[Philadelphia Inquirer]]
*[http://www.annenbergfoundation.org/board-of-directors/leonore-annenberg Annenberg Foundation Biography of Leonore Annenberg]

{{s-start}}
{{s-dip}}
{{succession box|title=[[United States Chief of Protocol|U.S. Chief of Protocol]]|before=[[Abelardo L. Valdez]]|after=[[Selwa Roosevelt]]|years=1981–1982}}
{{s-end}}

{{good article}}

{{DEFAULTSORT:Annenberg, Leonore}}
[[Category:1918 births]]
[[Category:2009 deaths]]
[[Category:Ambassadors of the United States]]
[[Category:American billionaires]]
[[Category:American Christian Scientists]]
[[Category:American people of Jewish descent]]
[[Category:American philanthropists]]
[[Category:American socialites]]
[[Category:American women diplomats]]
[[Category:American women in business]]
[[Category:Annenberg family]]
[[Category:Burials in Riverside County, California]]
[[Category:California Republicans]]
[[Category:Fellows of the American Academy of Arts and Sciences]]
[[Category:Female billionaires]]
[[Category:Members of the American Philosophical Society]]
[[Category:Stanford University alumni]]