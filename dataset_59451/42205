{{About|a specific B-17 bomber|the bomber type which has been generically referred to as an "aluminum overcast"|Convair B-36}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Italic title}}
{|{{Infobox aircraft begin
  |name=''Aluminum Overcast''
  |image = Aluminum Overcast.1.jpg
  |caption = ''Aluminum Overcast'' at the 2006 [[EAA AirVenture Oshkosh]] in Oshkosh, Wisconsin
}}
{{Infobox aircraft career
  |other names          =
  |type                 = [[Boeing B-17 Flying Fortress|B-17G-105-VE Flying Fortress]]
  |manufacturer         = [[Lockheed Corporation]] under license from [[Boeing]]
  |construction number  =
  |construction date    = 1945
  |civil registration   = N5017N
  |military serial      = 44-85740
  |first flight         = 18 May 1945
  |owners               = <!-- owners or operators, whether private individuals, companies, or military services -->
  |in service           = <!-- time in military or revenue service, as a range of dates -->
  |flights              =
  |total hours          =
  |total distance       =
  |fate                 =
  |preservation         = [[Experimental Aircraft Association]], [[Oshkosh, Wisconsin]]
}}
|}

'''''Aluminum Overcast''''', B-17G-105-VE, s/n 44-85740, civil registration ''N5017N'', is one of only ten presently airworthy [[Boeing B-17 Flying Fortress]]es of the 48 complete [[Boeing B-17 Survivors|surviving]] airframes in existence.  It never saw combat, and it escaped the fate of many aircraft that were scrapped after [[World War II]]. It is owned by the [[Experimental Aircraft Association]] (EAA), and, as of 2016, it is still touring the United States and Canada offering flight experiences.

Through its association as the "flagship" of the EAA, ''Aluminum Overcast'' has become one of the most recognizable examples of the B-17, due to its extensive touring schedule with over one million flight miles accumulated. The overhaul and restoration of the airframe took more than 10 years and thousands of hours by staff and volunteers at EAA Oshkosh, Wisconsin, headquarters.<ref name="Thompson p. 26">Thompson 2010, p. 26.</ref> ''Aluminum Overcast'' carries the colors of the [[398th Air Expeditionary Group|398th Bomb Group]] of World War II, which flew hundreds of missions over Nazi-held territory during the war. ''Aluminum Overcast'' commemorates B-17G AAF Serial No. 42-102516, shot down over France in 1944.<ref name ="Aluminum Overcast">[http://b17.org/history/aluminum_overcast.asp "The B-17 ''Aluminum Overcast''."] ''b17.org'' Retrieved: 18 May 2007.</ref>

==History==
B-17G-105-VE, 44-85740 was built by the Vega Division of [[Lockheed Aircraft Company]] and delivered to the U.S. Army Air Corps on 18 May 1945, too late to serve in Europe where most B-17s were destined.<ref name="O'Leary p. 40">O'Leary 2001, p. 40.</ref> Declared surplus and initially stored in New York, the aircraft was sent to [[Altus, Oklahoma]] on 7 November 1945 where, stripped of all military equipment, it was sold as "scrap" for $750 in 1946 to Metal Products. The aircraft was rescued when resold shortly after to Universal Aviation, for use as an aerial mapping platform, acquiring civil registration N5017N.<ref name="O'Leary p. 41">O'Leary 2001, p. 41.</ref>

On 2 August 1947, the aircraft was sold to Charles T. Winters, of Miami, Florida, who, in turn, sold it on 16 August 1947, to Joe Lopez of the Vero Beach Import and Export Company who purchased the aircraft to serve as a cargo hauler, hauling cattle in Florida and Puerto Rico. Its new role necessitated removing the original radio compartment and floor, replacing it with a strengthened floor.<ref name="History">Sturrock, Gordon. [http://www.squadron13.com/B17/B17.htm "History of the ''Aluminum Overcast''."] ''squadron13.com''. Retrieved: 7 August 2009.</ref> In 1949, Aero Service Corporation bought the aircraft and turned it back into an aerial photography and surveying platform.<ref name="O'Leary p. 42"/> In this role, during the next 12 years, the B-17 carried out mapping operations over Arabia, Libya, Lebanon, Iran, Laos, Vietnam, Cambodia, Egypt and Jordan.<ref name="History"/>

[[File:Aluminum Overcast B-17 (2005).jpg|thumb|left|''Aluminum Overcast'' during one of the periodic overhauls, 2005]]
Its next role was in aerial application with first Chris Stolzfus and Associates in 1962, and then Dothan Aviation Corporation in 1966, where the aircraft had a chemical hopper and spray bars fitted under the wings.<ref name="O'Leary p. 29">O'Leary 2008, p. 29.</ref> After operating as a pest control, forest dusting and fire fighting aircraft, the aircraft wound up its postwar career in 1976.<ref name="History"/>

In 1978, Dr. Bill Harrison, heading up "B-17s Around the World", funded the purchase of the aircraft, which had been parked in open storage in [[Dothan, Alabama]] for two years.<ref name="O'Leary p. 29"/> Renamed ''Aluminum Overcast,'' the name commemorates the 601st Bomb Squadron,<ref>[http://www.airventure.org/news/2008/080602_warbirds.html "Outstanding Lineup Planned for Warbirds in Review."] ''EAA Airventure News'', 2009. Retrieved: 7 August 2009.</ref> [[398th Air Expeditionary Group|398th Bomb Group (Heavy)]]'s B-17G #42-102516 that was shot down on its 34th combat mission over [[Le Manoir, Eure|Le Manoir, France]], on 13 August 1944.<ref name="O'Leary, p. 39"/> Veterans of the 398th Bomber Group helped finance the bomber's restoration.<ref name="O'Leary p. 42">O'Leary 2001, p. 42.</ref> Harrison's group restored the B-17 to a near-wartime appearance, although no armament was installed, and flew the aircraft at numerous air shows across the United States.<ref name="O'Leary p. 30">O'Leary 2008, p. 30.</ref> As financing for maintenance and further restoration became difficult, more ambitious plans, such as an around-the-world goodwill flight, were shelved.<ref name="O'Leary, p. 39">O'Leary 1998, p. 39.</ref>

===EAA===
[[File:B-17 Aluminum Overcast noseart-20060603.jpg|thumb|right|Nose art was redone in 1988]]
[[File:B-24G-AlumOver-CentennialColo.jpg|thumb|'''Aluminum Overcast''' for <br /> Denver and the Front Range <br /> ''Denventure 2007'' tour stop <br /> at [[Centennial Airport]], [[Colorado|CO]], 2007.]]

On 21 May 1979, the group donated the aircraft to the Experimental Aircraft Association (EAA) Aviation Foundation, under the auspices of the B-17 Historical Society.<ref name="O'Leary, p. 31">O'Leary 1998, p. 31.</ref> ''Aluminum Overcast'' was put on display until 1983, before it began a 10-year restoration. The extensive work involved the rebuilding of all the interior stations, including the radio compartment, waist gunners' stations, tail turret and navigator's station, and replacing the cabin flooring, as well as locating original equipment such as the Norden bombsight. Where authentic components were not available, realistic replicas, such as the new-manufacture top turret, were substituted. While the aircraft is about 95 percent authentic, recreating the configuration in 1945 when it rolled off the assembly line, concessions to technological improvements such as a modern radio and avionics suite are incorporated to meet the current airworthiness requirements.<ref>Ford, Rosemary. [http://www.newburyportnews.com/pulife/local_story_228094019 "B-17 ''Aluminum Overcast'' to land in Lawrence this weekend."] ''Daily News of Newburyport'', 16 August 2007. Retrieved: 6 August 2009.</ref>

Sent to the EAA’s [[Kermit Weeks]] [[Fantasy of Flight|Flight Research Center]] in Florida in 1993, the aircraft was prepared for its first tour, which began in 1994.<ref name="O'Leary, p. 33"/> At present, of the 48 surviving B-17s, there are only 11 such B-17s still flying,<ref name="Thompson p. 26"/> with ''Aluminum Overcast'', the high-time leader, already logging 6,000 flight hours by 1969.<ref name="O'Leary p. 42"/> When ''Aluminum Overcast'' is on tours, flying extensively throughout the United States and Canada, it serves as the EAA's showcase aircraft. During a typical 60-city tour, flights are interspersed with opportunities for visitors to actually walk through the aircraft.<ref>Pratt, Richard. [http://thegazette.com/subject/news/b-17-flight-sparks-memories-of-greatest-generation-20140606 "B-17 flight sparks memories of 'greatest generation'"] ''The Gazette'', 6 June 2014. Retrieved: 18 April 2015.</ref> The connection to the wartime B-17 is maintained through the involvement of many EAA volunteers, some of whom have a personal connection to the period. One of the initial flight crew was Col Harold "Hal" Weekley (ret'd), who had flown the original namesake that was shot down over France.<ref name="O'Leary p. 37">O'Leary 2001, p. 37.</ref> Half-hour flights are also available at all tour stops, with proceeds from the tour helping to keep ''Aluminum Overcast'' flying and assisting in the continuing restoration, maintenance and preservation efforts of EAA.<ref name ="Aluminum Overcast"/>

As fitted out, ''Aluminum Overcast'' has one [[Studebaker]] 1820 and three [[Curtiss-Wright]] 1820-97 engines. Its paint scheme was redone in 1988 and now features a flat aluminum [[Metallic paint|metallic flake paint]].<ref name ="Aluminum Overcast"/> ''Aluminum Overcast'' is based at the Air Adventure Museum Eagle Hangar in [[Wittman Regional Airport]] (OSH), Oshkosh, Wisconsin.<ref name="O'Leary, p. 33">O'Leary 1998, p. 33.</ref> The aircraft's annual multi-city tour is scheduled for spring through fall, with maintenance scheduled for the winter months.<ref name="O'Leary p. 44">O'Leary 2001, p. 44.</ref>

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* ''EAA handout for flight experience''.
* O'Leary, Michael. "Forts on the Sky Road." ''Warbirds International'', Volume 27, No. 6, September/October 2008.
* O'Leary, Michael. "Last Warrior." ''Air Classics'', Volume 37, No. 9, September 2001.
* O'Leary, Michael. "On tour: ''Aluminum Overcast''." ''Air Classics'', Volume 34, No. 8, August 1998.
* Thompson, Mike. "The Flying Fortresses: 75 Years Later – Airworthy B-17s in 2010," ''Warbird Digest,'' Thirty-two, September/October 2010.
* Valenta, Shari. [http://www.airportjournals.com/display.cfm/Centennial/0607025 "''Aluminum Overcast'' Visits Colorado."] ''Airport Journals'', July 2006. Retrieved: 4 August 2009.
{{Refend}}

==External links==
{{Commons category|Aluminum Overcast }}
* [http://www.b17.org/ EAA's B-17 ''Aluminum Overcast'' site]
* [http://www.us8thaaf.com/aluminum_overcast.htm Flight in ''Aluminum Overcast'']
* [https://www.youtube.com/watch?v=B9FlvuziDqs YouTube video of a flight of "Aluminum Overcast"]

{{Use dmy dates|date=June 2011}}

[[Category:Individual aircraft of World War II]]
[[Category:Boeing B-17 Flying Fortress]]