{{Infobox journal
| title = NeuroMolecular Medicine
| cover = [[File:Neuromol Med 2013 cover.jpg]]
| editor = Mark P. Mattson
| discipline = [[Neurology]], [[molecular medicine]]
| former_names =
| abbreviation = Neuromol. Med.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 2002-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.springer.com/biomed/neuroscience/journal/12017
| link1 =
| link1-name =
| link2 = http://link.springer.com/journal/volumesAndIssues/12017
| link2-name = Online archive
| JSTOR =
| OCLC = 605176440
| LCCN = 2001213164
| CODEN = NMEEAN
| ISSN = 1535-1084
| eISSN = 1559-1174
}}
'''''NeuroMolecular Medicine''''' is a quarterly [[Peer review|peer-reviewed]] [[medical journal]] covering research on the molecular and biochemical basis of [[neurological disorder]]s. It is published by [[Springer Science+Business Media]] and the [[editor-in-chief]] is Mark P. Mattson ([[National Institute on Aging]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[PubMed]]/[[MEDLINE]]
* [[Scopus]]
* [[PsycINFO]]
* [[EMBASE]]
* [[Chemical Abstracts Service]]
* [[Academic OneFile]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Elsevier Biobase]]
* [[EMBiology]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 4.492.<ref name=WoS>{{cite book |year=2013 |chapter=NeuroMolecular Medicine |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/biomed/neuroscience/journal/12017}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Neurology journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2002]]