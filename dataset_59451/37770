{{Use British English|date=July 2013}}
{{Use dmy dates|date=June 2012}}
{{Infobox film
| name           = The Best Exotic Marigold Hotel 
| image          = The-best-exotic-marigold-hotel.jpg
| director       = [[John Madden (director)|John Madden]]
| producer       = [[Graham Broadbent]]<br />Peter Czernin
| screenplay     = [[Ol Parker]]
| based on       = {{Based on|''These Foolish Things''|[[Deborah Moggach]]}}
| starring       = [[Judi Dench]]<br />[[Bill Nighy]]<br />[[Penelope Wilton]]<br />[[Dev Patel]]<br />[[Celia Imrie]]<br />[[Ronald Pickup]]<br />[[Tom Wilkinson]]<br />[[Maggie Smith]]<!-- per poster -->
| music          = [[Thomas Newman]]
| cinematography = [[Ben Davis (cinematographer)|Ben Davis]]
| editing        = Chris Gill
| studio         = [[Participant Media]]<br />Imagenation Abu Dhabi FZ<br />[[Graham Broadbent|Blueprint Pictures]]
| distributor    = [[Fox Searchlight Pictures]]
| released       = {{Film date|df=y|2011|11|30|SIIdC|2012|02|24|United Kingdom}}
| runtime        = 124 minutes
| country        = United Kingdom
| language       = English
| budget         = $10 million<ref name="mojo">{{cite web | title=The Best Exotic Marigold Hotel (2012) | work=[[Box Office Mojo]] | url=http://boxofficemojo.com/movies/?page=main&id=bestexoticmarigoldhotel.htm | accessdate=8 October 2012}}</ref>
| gross          = $136.8 million<ref name="mojo"/>
}}

'''''The Best Exotic Marigold Hotel''''' is a 2011 British [[comedy-drama]] film, directed by [[John Madden (director)|John Madden]]. The screenplay, written by [[Ol Parker]], is based on the 2004 novel ''These Foolish Things'', by [[Deborah Moggach]], and features an [[ensemble cast]] consisting of [[Judi Dench]], [[Celia Imrie]], [[Bill Nighy]], [[Ronald Pickup]], [[Maggie Smith]], [[Tom Wilkinson]] and [[Penelope Wilton]], as a group of British [[pensioner]]s moving to a retirement hotel in [[British Raj|India]], run by the young and eager Sonny, played by [[Dev Patel]]. The movie was produced by [[Participant Media]] and Blueprint Pictures on a budget of $10 million.

Producers [[Graham Broadbent]] and Peter Czernin first saw the potential for a film in Deborah Moggach's novel with the idea of exploring the lives of the elderly beyond what one would expect of their age group. With the assistance of screenwriter [[Ol Parker]], they came up with a script in which they take the older characters completely out of their element and involve them in a romantic comedy.

[[Principal photography]] began on 10 October 2010 in India, and most of the filming took place in the Indian state of [[Rajasthan]], including the cities of [[Jaipur]] and [[Udaipur]]. Ravla Khempur, an equestrian hotel which was originally the palace of a tribal chieftain in the village of [[Khempur]], was chosen as the site for the film hotel.

The film was released in the United Kingdom on 24 February 2012 and received critical acclaim; ''The Best Exotic Marigold Hotel'' opened to strong box-office business in the United Kingdom and continued to build worldwide. It became a [[sleeper hit|surprise box-office hit]] following its international release, eventually grossing nearly $137 million worldwide.

It was ranked among the highest-grossing 2012 releases in [[Australia]], [[New Zealand]] and the United Kingdom, and as one of the highest-grossing speciality releases of the year. A sequel, ''[[The Second Best Exotic Marigold Hotel]]'', began production in [[India]] in January 2014, and was released on 26 February 2015.

==Plot==
Recently widowed housewife Evelyn ([[Judi Dench]]) must sell her home to cover huge debts left by her late husband. Graham ([[Tom Wilkinson]]), a high-court judge who had spent his first eighteen years in India, abruptly decides to retire and return there. Jean ([[Penelope Wilton]]) and Douglas ([[Bill Nighy]]) seek a retirement they can afford, having lost most of their savings through investing in their daughter's internet business. Muriel ([[Maggie Smith]]), a retired housekeeper prejudiced against Indians, needs a hip replacement operation which can be done far more quickly and inexpensively in India. Madge ([[Celia Imrie]]) is hunting for another husband, and Norman ([[Ronald Pickup]]), an aging [[Lothario]], is trying to recapture his youth. They each decide on a retirement hotel in India, based on pictures on its website.

When the group arrives at the picturesque hotel, they find an energetic young manager Sonny ([[Dev Patel]]) but a dilapidated facility, not yet what he had promised. Overwhelmed by the cultural changes, Jean often stays inside at the hotel, while her husband Douglas explores the sights. Graham finds that the area has greatly changed since his youth and disappears on long outings every day. Muriel, despite her xenophobia, starts to appreciate her doctor for his skill and the hotel maid for her good service. Evelyn gets a job advising the staff of a [[call centre]] on how to interact with older British customers. Sonny struggles to raise funds to renovate the hotel and sees his girlfriend Sunaina ([[Tena Desae]]), despite his mother's disapproval. Madge joins the Viceroy Club seeking a spouse, where she is surprised to find Norman. She introduces him to Carol ([[Diana Hardcastle]]). He admits he is lonely and seeking a companion, and the two begin an affair.

Graham confides in Evelyn that he is trying to find the Indian lover he was forced to abandon as a youth. Social-climber Jean is attracted to Graham, and makes a rare excursion to follow him, but is humiliated when he explains he is [[gay]]. Graham reunites with his former lover, who is in an arranged marriage of mutual trust and respect. Reconciled, the Englishman dies of an existing [[heart disease|heart condition]]. Evelyn and Douglas grow increasingly close, angering his wife, which results in an outburst from Douglas denouncing this marriage. Muriel reveals that she was once housekeeper to a family who had her train her younger replacement and now, having been forced out of the home and into retirement, she feels that she has lost any purpose in her life.

Sonny's more successful brothers each own a third of the hotel and plan to demolish it. His mother ([[Lillete Dubey]]) agrees and wants him to return to [[Delhi]] for an arranged marriage. Jean and Douglas prepare to return to England after money is found through their daughter's company. Jean eagerly awaits returning to England, but Douglas is more hesitant. Now that the hotel is closing against Sonny's wishes and pleas, Madge prepares to return to England, and Norman agrees to move in with Carol. Madge, after encouragement from Carol and Muriel, decides to keep searching for another husband.

Sonny, encouraged by Evelyn, finally tells Sunaina that he loves her. He confronts his mother, who first forbids the match but then is persuaded by Young Wasim, who speaks no English. He explains that he once knew another man who wanted to marry a smart beautiful woman against his family's wishes. Sonny's mother interprets for Young Wasim, realizing he is talking about her, and she finally gives the couple her blessing. She asks Sunaina to take good care of her "favourite son". Before the remaining guests can leave, Muriel reveals that her experience running the family's household gave her the knowledge how to balance a budget and that the hotel can make a profit. She approaches Sonny's investor privately and then invites him to visit the hotel to discuss matters with Sonny. The investor agrees to fund Sonny's plans for renovation so long as Muriel stays on as an assistant manager.

All the guests agree to stay — except Jean and Douglas. Due to their daughter's long-awaited success, they decide to return home but on the way to the airport, their taxi gets caught in a traffic jam. A rickshaw driver says that he can take only one of them. Jean sees it as a sign that it is time to split with Douglas; she bids him farewell and departs. He winds up at another hotel, discovering that it's nothing more than a [[brothel]] and drug den, and spends the rest of the night wandering the streets. He returns to the hotel just as Evelyn is leaving for work, and asks when she'll be back. A closing montage with a voiceover shows Muriel checking in customers in an elegant renovated lobby, Madge dining with a handsome older Indian man, and Norman and Carol living happily together. Sonny and Sunaina are shown riding a motorbike and passing Douglas and Evelyn on another bike.

==Cast==
* [[Judi Dench]] as Evelyn Greenslade, a recently widowed housewife whose house must be sold to pay off her husband's debts. Like his father, her son wants to "care" for her, without her input. At Sonny's home for the "elderly and beautiful", she keeps a blog of her activities. She narrates throughout the film, from bookend to bookend, from the opening sequence to the Day 51 moral "We get up in the morning, we do our best".<ref name="imdb">{{cite web |url=http://www.imdb.com/title/tt1412386/ |title=IMDB Page |last1= |first1= |last2= |first2= |date= |website=imdb.com |publisher=IMDB |accessdate=17 February 2014}}</ref>
* [[Bill Nighy]] as optimist Douglas Ainslie, husband of Jean for 39 years. His loyalty has kept them together when she sees they both "deserve better". He enjoys the food and sights, going out every day.<ref name="imdb" />
* [[Penelope Wilton]] as pessimist Jean Ainslie. After Douglas invested—and seemingly lost—all their savings in their daughter's internet business, they can afford only a "beige bungalow" installed with a panic button and hand rails "for the future". She hates everything about India; the perceived noise, poverty, and smells. It is for this reason that she stays indoors often, and wishes to return to the UK.<ref name="imdb" />
* [[Maggie Smith]] as Muriel Donnelly, an ex-[[nanny]] with a head for figures, is deemed surplus to requirements by her lifelong employers after she unwittingly trains her own replacement. She has no family of her own, having devoted her life to her employers. Although racist, she chooses not to wait six months for a hip replacement, and rather be "outsourced" to India.<ref name="imdb" />
* [[Tom Wilkinson]] as Graham Dashwood, a [[High Court of Justice|High Court]] judge who has, for many years, been retiring "any day now". During the retirement speech of a colleague, he decides that "today's the day". Having lived in India for his first 18 years, he returns to seek out the love of his early life, a man.<ref name="imdb" />
* [[Ronald Pickup]] as Norman Cousins, an aged [[lothario]], unable to face up to his own age and consequent undesirability by younger women; he hopes for a new start with new possibilities in India.<ref name="imdb" />
* [[Celia Imrie]] as Madge Hardcastle, who has had several unsuccessful marriages. Like Norman, she wants fun, adventure and a new mate. Tired of her daughter's attempts to keep her as unpaid babysitter, she flees for anywhere, choosing India.<ref name="imdb" />
* [[Dev Patel]] as Sonny Kapoor, manager of the hotel, and one-third owner with his older, more favoured brothers. Sonny is a dreamer eager for a first success, but his determination makes him unwilling to ask for help until the end.<ref name="imdb" />
* [[Tina Desai]] as Sunaina, call centre worker, Sonny's "modern" girlfriend. She befriends Evelyn when Evelyn gets a job at the same call centre (see below).<ref name="imdb" />
* [[Sid Makkar]] as Jay, Sunaina's brother, manager of a call centre, hires Evelyn to teach workers British culture.<ref name="imdb" />
* [[Lillete Dubey]] as Mrs. Kapoor, widowed mother of Sonny. She admits that Sonny is not her favourite son, and wants him to move back with her to Delhi for an arranged marriage, not to Sunaina.<ref name="imdb" />
* [[Diana Hardcastle]] as Carol, an Englishwoman but lifelong resident of Jaipur, who asks Norman to enter into [[cohabitation]], according to his perception, impetuously.<ref name="imdb" />
* [[Seema Azmi]] as Anokhi, a [[Dalit]] (lowest-caste) maid at the hotel who takes Muriel's acknowledgement for friendship. Via an interpreter, Muriel reveals that her bitterness was caused by her employers having tossed her aside as obsolete after having devoted her life as a nanny and housekeeper, and having unwittingly prepared her replacement.<ref name="imdb" />
* [[Paul Bhattacharjee]] as Dr. Ghujarapartidar; this was Bhattacharjee's final feature film appearance before his death in 2013.<ref name="imdb" />
* [[Liza Tarbuck]] as head nurse Karen.<ref name="imdb" />
* [[Denzil Smith]] as Mr Dhurana, the Viceroy Club Secretary <ref name="imdb" />
* [[Honey Chhaya]] as Young Wasim.<ref name="imdb" />

==Production==

===Background and script===
[[File:Deborah Moggach.jpg|thumb|right|Producers saw potential in Deborah Moggach's novel.]]
Producers [[Graham Broadbent]] and Peter Czernin were the ones who first saw the potential for a film in [[Deborah Moggach]]'s novel. The concept of outsourcing retirement, "taking our outsourcing of everyday tasks like banking and customer service one step further", appealed to them, and they commissioned screenwriter [[Ol Parker]] to formulate this concept into a screenplay.<ref name="CinemaReview">{{cite web |url=http://cinemareview.com/production.asp?prodid=7000|title=Exotic Marigold Hotel| work=Cinema Review |accessdate=17 February 2014}}</ref>

Parker wanted to take the older characters completely out of their element and involve them in a romantic comedy. They initially encountered difficulties finding a studio; [[Working Title Films]] rejected their proposals, considering it unmarketable,<ref>{{cite web|first=Emily|last=Jupp|url=http://www.independent.co.uk/voices/comment/delayed-diagnosis-the-persistence-of-hope--ol-parker-discusses-new-film-now-is-good-starring-dakota-fanning-and-jeremy-irvine-8198013.html|title=Delayed Diagnosis: The persistence of hope|work=[[The Independent]]|date= 5 October 2012|accessdate= 30 October 2012}}</ref> but they eventually aligned with [[Participant Media]], Imagination Abu Dhabi FZ, and Blueprint Pictures.<ref name="imdb" />

===Casting===
[[File:Jaipur 03-2016 19 City Palace complex.jpg|thumb|left|250px|Some of the footage of the film was shot around the [[City Palace, Jaipur|City Palace]] of [[Jaipur]].]]
To helm the project, the producers Broadbent and Czernin approached [[John Madden (director)|John Madden]], who was nominated for the [[Academy Award for Best Director]] for ''[[Shakespeare in Love]]'' in 1998. Madden considered the characters in ''The Best Exotic Marigold Hotel'' to be of "an analogous kind of geographical suspension", which have "entered a strange world removed from their former reality, cut off from their past, where they have to invent a new life for themselves".<ref name="CinemaReview" /> Dench and fellow cast members Maggie Smith, Penelope Wilton, Celia Imrie, Bill Nighy, Ronald Pickup, Tom Wilkinson, and director John Madden jumped at the opportunity to all work together for the first time in one film. Producer Broadbent considers Dench's character to be central to the story, and that Evelyn is much like Dench herself, being "the most wonderfully sympathetic person". John Madden considers Maggie Smith's character Muriel to be "instinctively xenophobic, never stepping out of her comfort zone in any way", which is not uncommon in England.<ref name="CinemaReview2">{{cite web |url=http://cinemareview.com/production.asp?prodid=7001|title=The Residents| work=Cinema Review |accessdate=17 February 2014}}</ref>

The filmmakers determined early on that the role of Sonny was crucial to the outcome of the picture, and they selected Dev Patel, who at the time was still revelling in the success of ''[[Slumdog Millionaire]]''. Dench described Patel as a "born comedian", and Madden considered him to be a "comic natural—a sort of Jacques Tati figure, with amazing physical presence and fantastic instincts".<ref name="CinemaReview3">{{cite web |url=http://cinemareview.com/production.asp?prodid=7002|title=The Management| work=Cinema Review |accessdate=17 February 2014}}</ref> Patel had personal experience of watching the elderly through his mother who had been a carer, and he was "enticed by how vivid these characters are, by their sarcasm and their wisdom", confessing that he "fell in love with the script because every character shines in his or her own different way and you believe in each of them."<ref name="CinemaReview3"/>  [[Lilette Dubey]] was cast as Sonny's mother, and English-language debutante [[Tina Desai]] portrayed Sunaina, Sonny's call-centre-operator girlfriend.

===Filming===
[[File:Jaipur 03-2016 04 Amber Fort.jpg|thumb|right|Footage was also shot in and around [[Amer Fort]].]]
[[File:Lake palace 05a.jpg|thumb|left|A culminating scene was shot at the insular [[Lake Palace Hotel]] on [[Lake Pichola]].]]
[[Principal photography]] began on 10 October 2010 in India.<ref>{{cite web|url=http://moviecitynews.com/2010/10/%e2%80%9cthe-best-exotic-marigold-hotel%e2%80%9d-to-begin-principal-photography/|title=John Madden to Direct All Star Cast with Dame Judi Dench|work=Movie City News|date= 6 October 2010|accessdate= 30 October 2012}}</ref> Most of the filming took place in the Indian state of [[Rajasthan]], including the cities of [[Jaipur]] and [[Udaipur]].<ref name="fodors">{{cite web|first=Erica|last=Duecy|url=http://www.fodors.com/news/on-location-in-india-with-the-best-exotic-marigold-hotel-5457|title=On Location in India with The Best Exotic Marigold Hotel|publisher=[[Fodors]]|date= 30 April 2012|accessdate= 10 October 2012}}</ref> In Jaipur, filming took place around the [[City Palace, Jaipur|City Palace]], the Marigold market, and on crowded buses. Other scenes were shot in [[Kishangarh]], and on the outskirts of Jaipur, footage was shot at [[Kanota Fort]], which stood in for the Viceroy Club. The place where Sonny and Sunaina meet in the film was shot nearby at the Step Well near [[Amer Fort]], a 10th-century establishment noted for its "ten stories of pale golden stone steps."<ref name="CinemaReview4"/> Ravla Khempur was chosen as the site for the Best Exotic Marigold Hotel; it is an equestrian hotel that was originally the palace of a tribal chieftain, located about an hour and a half outside of Udaipur in the village of [[Khempur]].<ref name="fodors" /> Madden considered the building to have a magical quality and unmistakable charm, remarking that it had "something special that could ultimately draw the characters in. It had these wonderful cool dark interiors, with glimpses of saturated light and the teeming life outside its walls."<ref name="CinemaReview4"/> Production designer [[Alan MacDonald (production designer)|Alan MacDonald]], who won Best Art Direction in a Contemporary Film from the [[Art Directors Guild]] for his work,<ref>{{cite web|url=http://www.artistdirect.com/nad/store/movies/principal/0,,2060654,00.html|title= Alan MacDonald|publisher=Artistdirect.com|accessdate=19 February 2014}}</ref> was brought in to embellish the interiors, intentionally making it clash with "interesting furniture inspired by colonial India, mismatched local textiles, all mixed together with modern plastic bits and pieces, with everything distressed and weather beaten."<ref name="CinemaReview4"/> Footage was also shot at the [[Lake Palace Hotel]] at [[Lake Pichola]].<ref name="fodors" />

Madden said that challenges of filming in India included the street noise, as well as the people's curiosity and hospitality when they saw him producing a camera.<ref name="fodors"/> The cast and crew were well received by the locals, as was the director who, along with the cast, was invited by [[Arvind Singh Mewar]], the Maharaja of Udaipur, to attend his lavish Diwali celebrations and firework display, as well as attend a royal wedding held at the [[Rambagh Palace Hotel]] in Jaipur.<ref name="CinemaReview4">{{cite web |url=http://cinemareview.com/production.asp?prodid=7003|title=The Sights| publisher=Cinema Review |accessdate=17 February 2014}}</ref> Chris Gill was the editor of the picture.<ref>{{cite web|url=https://www.nytimes.com/2012/05/04/movies/the-best-exotic-marigold-hotel-with-judi-dench.html| title=Seven Tickets to India, Please, and Reservations for an Adventure|work=[[The New York Times]]|date=4 May 2012|accessdate=18 February 2014}}</ref>

A sequel, ''[[The Second Best Exotic Marigold Hotel]]'', began production in India in January 2014 and was released in February 2015. Most of the cast returned, with additions including American actor [[Richard Gere]].<ref name=cast>{{cite news|last=Dowell|first=Ben|title=The Best Exotic Marigold Hotel 2 to start filming in January|url=http://www.radiotimes.com/news/2013-10-28/the-best-exotic-marigold-hotel-2-to-start-filming-in-january|accessdate=12 January 2014|work=[[RadioTimes]]|date=28 October 2013}}</ref><ref>{{cite web|url=http://www.bbc.co.uk/news/entertainment-arts-25680451|title=Richard Gere joins Best Exotic Marigold Hotel sequel|publisher=[[BBC]]|date=10 January 2014|accessdate=18 February 2014}}</ref>

==Soundtrack==
[[File:Thomas Newman.jpg|thumb|left|The soundtrack was composed by Thomas Newman.]]
{{Infobox album
| Name =The Best Exotic Marigold Hotel (Music from the Motion Picture)
| Cover = 
| Type =Soundtrack
| Artist =various artists
| Released =2012
| Genre =Soundtrack
| Length =46:42
| Label =[[Sony Classical]]
}}
The soundtrack, composed by [[Thomas Newman]], was released in the CD format in 2012.<ref>{{cite web |url=http://www.allmusic.com/album/the-best-exotic-marigold-hotel-music-from-the-motion-picture-mw0002311506/releases |title=The Best Exotic Marigold Hotel|publisher=AllMusic.com |accessdate=6 March 2014}}</ref>

{{Track listing
|collapsed=yes
| extra_column = Performer(s)
| title1 = Long Old Life
| length1 = 3:32
| extra1 = Instrumental
| title2 = This Is the Day
| length2 = 0:47
| extra2 = Instrumental
| title3 = The Chimes at Midnight
| length3 = 2:38
| extra3 = Instrumental
| title4 = Road to Jaipur
| length4 = 1:29
| extra4 = Instrumental
| title5 = Night Bus
| length5 = 1:15
| extra5 = Instrumental
| title6 = Tuk Tuks
| length6 = 1:41
| extra6 = Instrumental
| title7 = The Best Exotic Marigold Hotel
| length7 = 2:35
| extra7 = Instrumental
| title8 = Assault on the Senses
| length8 = 2:58
| extra8 = Instrumental
| title9 = Anokhi
| length9 = 1:10
| extra9 = Instrumental
| title10 = Cricket Spell
| length10 = 3:46
| extra10 = Instrumental
| title11 = More Than Nothing
| length11 = 2:22
| extra11 = Instrumental
| title12 = Day 22
| length12 = 1:41
| extra12 = Instrumental
| title13 = Mrs. Ainsley
| length13 = 1:43
| extra13 = Instrumental
| title14 = Do Your Worst
| length14 = 1:48
| extra14 = Instrumental
| title15 = Udaipur
| length15 = 4:10
| extra15 = Instrumental
| title16 = Turning Left
| length16 = 2:00
| extra16 = Instrumental
| title17 = Not Yet the End
| length17 = 0:50
| extra17 = Instrumental
| title18 = Progress
| length18 = 2:42
| extra18 = Instrumental
| title19 = Young Wasim
| length19 = 2:46
| extra19 = Instrumental
| title20 = What Happens Instead
| length20 = 0:57
| extra20 = Instrumental
| title21 = A Bit of Afters
| length21 = 3:52
| extra21 = Instrumental
| total_length=46:42
}}

==Reception==

===Box office===
[[File:Campus Theater (7497904460).jpg|200px|thumb|right|Marquee showing ''The Best Exotic Marigold Hotel'' at a [[Movie theater|cinema]] in [[Lewisburg, Pennsylvania]].]]
The film was first shown at the Italian cinema trade show ''Le Giornate Professionali di Cinema'' ("The Professional Days of Cinema") in [[Sorrento]] on 30 November 2011<ref>[http://www.giornatedicinema.it/programma-/534/1/30-novembre.php "Le Giornate Professionali di Cinema: Programma 30 novembre"]. Retrieved 28 May 2012. {{Dead link |date=January 2017}}</ref> and at the [[Glasgow Film Festival]] on 17 February 2012, before being released widely in the United Kingdom and Ireland on 24 February 2012. This was followed by release in a further 26 countries in March and April. From May to August, more and more nations saw the release of the film, before Japan's February 2013 release capped off the film's theatrical debut calendar.<ref>[http://www.imdb.com/title/tt1412386/releaseinfo?ref_=tt_dt_dt IMDB Release Calendar]</ref>

In the United Kingdom, ''The Best Exotic Marigold Hotel'' came in second to ''[[The Woman in Black (2012 film)|The Woman in Black]]'' at the box office during its first week, earning £2.2 million.<ref>{{cite web |url=http://www.dcm.co.uk/blog/2012/02/28/uk-box-office-28-february-2012/|title=UK Box Office – 28 February 2012|work=Digital Cinema Media|date= 28 February 2012|accessdate= 30 October 2012}}</ref> It eventually topped the UK box office, with £2.3 million, in its second weekend on release.<ref>{{cite web |url=http://www.dcm.co.uk/blog/2012/03/06/uk-box-office-06-march-2012/|title=UK Box Office – 06 March 2012|work=Digital Cinema Media|date=6 March 2012|accessdate= 30 October 2012}}</ref> By the end of its UK run, the film had grossed around US$31 million.<ref name="indiewire 20120502">{{cite web |first=David |last=Gritten|url=http://blogs.indiewire.com/thompsononhollywood/best-exotic-marigold-hotel-from-a-pleasant-surprise-to-a-phenomenon?page=2#blogPostHeaderPanel/|title='Best Exotic Marigold Hotel': From Pleasant Surprise to Box Office Phenomenon |work=Indiewire|date= 2 May 2012|accessdate= 30 October 2012}}</ref> Prior to its United States debut, the comedy had already grossed US$69 million worldwide and passed both ''[[The Queen (film)|The Queen]]'' (2006) and ''[[Calendar Girls]]'' (2003) in total international grosses.<ref name="indiewire 20120502"/><ref name="cs"/> After three months of release, it was ranked the third highest-grossing 2012 release in Australia and New Zealand, behind only ''[[The Avengers (2012 film)|The Avengers]]'' and ''[[The Hunger Games (film)|The Hunger Games]]'', and the fourth-highest-grossing 2012 title in the UK.<ref name="cs"/>

In the US and Canada, the film initially opened in 16 theatres in its first week. In its second week of release, it expanded from 16 to 178 screens in North America and grossed US$2.7 million for the weekend, ending eighth on the week's top hits.<ref>{{cite web|url=http://www.rediff.com/movies/report/best-exotic-marigold-hotel-already-a-hit/20120515.htm|title=Best Exotic Marigold Hotel Already a Hit?|work=[[Rediff.com]]|date= 15 May 2012|accessdate= 30 October 2012}}</ref> By the end of the month, ''Best Exotic Marigold Hotel'' had grossed US$100 million worldwide.<ref name="cs">{{cite web|url=http://www.comingsoon.net/news/movienews.php?id=90881|title=The Best Exotic Marigold Hotel Crosses $100 Million |publisher=ComingSoon.net|date= 31 May 2012|accessdate= 30 October 2012}}</ref> The film had a worldwide gross of US$136,836,156.<ref name="mojo"/> It ranks among the highest-grossing international films released by [[Fox Searchlight Pictures]] behind ''[[Black Swan (film)|Black Swan]]'' (2010), ''[[The Full Monty]]'' (1997), and ''[[The Descendants]]'' (2011),<ref name="cs"/> and  among the highest-grossing specialty releases of the year along with ''[[Moonrise Kingdom]]''  and ''[[To Rome with Love (film)|To Rome with Love]]''.<ref>{{cite web|first=Peter|last=Knegt|url=http://www.indiewire.com/article/specialty-box-office-queen-reigns-for-indie-debuts-lcd-soundsystem-doc-has-great-one-night-only?page=2|title=Specialty Box Office: 'Queen' Reigns For Indie Debuts; LCD Soundsystem Doc Has Great 'One Night Only'|work=Indiewire|date= 23 July 2012|accessdate= 30 October 2012}}</ref>

Elsewhere, ''The Best Exotic Marigold Hotel'' took in less than 58 million USD. Nations contributing sizable box office returns aside from the UK and North America included Australia (21.2 million USD), Germany (6 million USD), New Zealand (4.4 million USD), Spain (4.3 million USD), France (1.9 million USD), Sweden (1.3 million USD), Italy (1.1 million USD), South Africa (1 million USD), and Norway (797 thousand USD).<ref>{{cite web|url=http://boxofficemojo.com/movies/?page=intl&id=bestexoticmarigoldhotel.htm&sort=todateGross&order=ASC&p=.htm|title= The Best Exotic Marigold Hotel|publisher=Box Office Mojo|accessdate=18 February 2014}}</ref>

===Reception===
The film received positive reviews from critics. The review aggregator website [[Rotten Tomatoes]] reported that 78% of critics gave the film a positive rating, based on 148 reviews, with an average score of 6.6/10. Its consensus states "''The Best Exotic Marigold Hotel'' isn't groundbreaking storytelling, but it's a sweet story about the senior set featuring a top-notch cast of veteran actors."<ref>{{cite web |title=The Best Exotic Marigold Hotel (2012) |url=http://www.rottentomatoes.com/m/the_best_exotic_marigold_hotel_2012 |publisher=[[Rotten Tomatoes]] |accessdate= 30 October 2012}}</ref> On [[Metacritic]], which uses a normalized rating system, the film holds a 62/100 rating, based on 35 reviews, indicating "generally favourable reviews".<ref>{{metacritic film|the-best-exotic-marigold-hotel|The Best Exotic Marigold Hotel|accessdate= 30 October 2012}}</ref>
{{multiple image
| footer    = Dench and Smith both received favourable reviews from critics.
| image1    = Judi Dench at the BAFTAs 2007.jpg
| alt1      = 
| width1    = 153
| image2    = Dame Maggie Smith face.jpg
| alt2      =
| width2    = 120
| align     = right}}
[[Mick LaSalle]] of the ''[[San Francisco Chronicle]]'' remarked that the film was "a rare reminder from movies that the grand emotions are not only for the young and the middle-aged", citing it "too well made to be dismissed and contains too much truth to be scorned."<ref>{{cite web |url=http://www.sfgate.com/movies/article/Best-Exotic-Marigold-Hotel-review-Aging-s-truth-3532258.php#ixzz2Amrpt8wL|first=Mick |last=LaSalle|title='Best Exotic Marigold Hotel' review: Aging's truth|work=[[San Francisco Chronicle]]|date= 4 May 2012|accessdate= 30 October 2012}}</ref> [[Roger Ebert]], writing for the ''[[Chicago Sun-Times]]'', gave ''The Best Exotic Marigold Hotel'' three and a half out of four stars. He declared the film "a charming, funny and heartwarming movie [and] a smoothly crafted entertainment that makes good use of seven superb veterans."<ref>{{cite web |url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20120502/REVIEWS/120509993|first=Roger |last=Ebert|title=The Best Exotic Marigold Hotel|publisher=RogertEbert.com|work=[[Chicago Sun-Times]]|date= 2 May 2012|accessdate= 30 October 2012}}</ref> Claudia Puig from ''[[USA Today]]'' called it "a refreshing, mature fairy tale with a top-notch ensemble cast." While she felt the film was "about 15 minutes too long", she summarized it as "a delightful, droll and entertaining comedy of manners with an estimable cast" and an "ideal low-tech alternative to the special-effects laden" film projects of 2012.<ref>{{cite web |url=http://usatoday30.usatoday.com/life/movies/reviews/story/2012-05-04/the-best-exotic-marigold-hotel-judi-dench-maggie-smith/54738736/1?csp=34life&utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+UsatodaycomMovies-TopStories+%28Life+-+Movies+-+Top+Stories%29|first=Claudia |last=Puig|title='Exotic Marigold Hotel' lets senior citizens bloom|work=[[USA Today]]|date=3 May 2012|accessdate= 30 October 2012}}</ref>

[[Peter Travers]] from ''[[Rolling Stone]]'' rated the comedy three out of four stars. He found that "with a lesser cast, the movie would be a lineup of TV-movie clichés. But this is a cast that never makes a false move even when the script settles for formula."<ref>{{cite web |url=http://www.rollingstone.com/movies/reviews/the-best-exotic-marigold-hotel-20120503|first=Peter |last=Travers|title=The Best Exotic Marigold Hotel|work=[[Rolling Stone]]|date= 3 May 2012|accessdate= 30 October 2012}}</ref> ''[[Chicago Tribune]]'' critic [[Michael Phillips (critic)|Michael Phillips]] wrote that "as two-hour tours go, ''The Best Exotic Marigold Hotel'' goes smoothly." While he felt that the film focused on "pleasantly predictable story", he noted that the project was one of those films which "are better off concentrating on a reassuring level of actorly craft [than] going easy on the surprises."<ref>{{cite web |url=http://www.chicagotribune.com/entertainment/movies/sc-mov-0502-best-exotic-marigold-hotel-20120503,0,5707064.column|first=Michael |last=Phillips|title=Cast Makes 'Best Exotic Marigold Hotel' Shine|work=[[Chicago Tribune]]|date= 3 May 2012|accessdate= 30 October 2012}}</ref>

[[Lisa Schwarzbaum]] of ''[[Entertainment Weekly]]'' graded the film with a 'B–' rating, summarizing it as a "lulling, happy-face story of retirement-age self-renewal, set in a shimmering, weltering, jewel-colored India", and that it succeeded in selling "something safe and sweet, in a vivid foreign setting, to an underserved share of the moviegoing market."<ref>{{cite web |url=http://www.ew.com/ew/article/0,,20587639,00.html|first=Lisa |last=Schwarzbaum|title=The Best Exotic Marigold Hotel (2012)|work=[[Entertainment Weekly]]|date= 3 May 2012|accessdate= 30 October 2012}}</ref> [[Peter Bradshaw]], writing for ''[[The Guardian]]'', was more igneous in his 2/5 star review, saying that the film "needs a [[Stannah Lifts|Stannah chairlift]] to get it up to any level of watchability, and it is not exactly concerned to do away with condescending stereotypes about old people, or Indian people of any age." Noting the luminous, prolific resumes of the cast he noted "nothing in this insipid story does anything like justice to the cast's combined potential." He went on to opine that the film appeared "oddly like an [[Agatha Christie]] thriller with all the pasteboard characters, 2D backstories and foreign locale, but no murder."<ref>{{cite web |url=https://www.theguardian.com/film/2012/feb/23/best-exotic-marigold-hotel-review |title=The Best Exotic Marigold Hotel |work=The Guardian|date=23 February 2012|accessdate=17 February 2014}}</ref> In further negative reviews, critics from ''The Guardian'' and the blog Marshall and the Movies criticized the film for having a [[colonialist]] and [[Orientalism|orientalist]] point of view towards India.<ref>{{cite news|last=Lalwani|first=Nikita|title=The Best Exotic Marigold Hotel: an exercise in British wish-fulfilment|url=https://www.theguardian.com/commentisfree/2012/feb/27/best-exotic-marigold-hotel-compliance|accessdate=30 April 2014|newspaper=The Guardian|date=27 February 2012}}</ref><ref>{{cite web|title=REVIEW: The Best Exotic Marigold Hotel|url=http://marshallandthemovies.com/2012/06/24/marigold/|work=Marshall and the Movies|accessdate=30 April 2014}}</ref>

===Accolades===
At the Cinema Scapes Awards, organised on the sidelines of the 2012 [[Mumbai Film Festival]], the film was honoured with the Best International Film accolade for showcasing Indian filming locations.<ref name="thr">{{cite web |url=http://www.hollywoodreporter.com/news/best-exotic-marigold-hotel-honored-383406|first=Nyay |last=Bhushan |title='Best Exotic Marigold Hotel' Honored for Showcasing India Filming Locations|work=[[The Hollywood Reporter]]|date= 26 October 2012|accessdate= 30 October 2012}}</ref> The film and its cast earned five nominations from the [[British Independent Film Awards]].<ref>{{cite web|url=http://www.digitalspy.co.uk/movies/news/a435735/broken-sightseers-berberian-sound-studio-lead-bifa-nominations.html|title='Broken', 'Sightseers', 'Berberian Sound Studio' lead BIFA nominations|last=Reynolds|first=Simon|date=5 November 2012|work=[[Digital Spy]]|accessdate=6 November 2012}}</ref>

{| class="wikitable"
|-
! Award !! Category !! Recipient(s) !! Result
|-
| [[ADG Excellence in Production Design Award]]s
| [[Art Directors Guild Award for Excellence in Production Design for a Contemporary Film|Contemporary Film]]
| Alan MacDonald
| {{nom}}
|-
| [[London Film Critics Circle Awards 2012|ALFS Awards]]
| [[London Film Critics Circle Award for Actress of the Year|Actress of the Year]]
| [[Judi Dench]]
| {{nom}}
|-
| [[66th British Academy Film Awards|British Academy Film Awards]]
| [[BAFTA Award for Best British Film|BAFTA Award for Outstanding British Film]]
| [[John Madden (director)|John Madden]]
| {{nom}}
|-
| rowspan="5"| [[British Independent Film Awards]]
| [[BIFA Award for Best British Independent Film|Best British Independent Film]]
| [[Graham Broadbent]], Peter Czernin
| {{nom}}
|-
| Best Director of a British Independent Film
| John Madden
| {{nom}}
|-
| [[BIFA Award for Best Performance by an Actress in a British Independent Film|Best Performance by an Actress in a British Independent Film]]
| [[Judi Dench]]
| {{nom}}
|-
| [[BIFA Award for Best Supporting Actor|Best Supporting Actor]]
| [[Tom Wilkinson]]
| {{nom}}
|-
| [[BIFA Award for Best Supporting Actress|Best Supporting Actress]]
| [[Maggie Smith]]
| {{nom}}
|-
| [[18th Critics' Choice Awards|Critics' Choice Awards]]
| [[Broadcast Film Critics Association Award for Best Cast|Best Cast]]
| Ensemble
| {{nom}}
|-
| [[Costume Designers Guild|Costume Designers Guild Awards]]
| Excellence in Contemporary Film
| Louise Stjernsward
| {{nom}}
|-
| [[25th European Film Awards|European Film Awards]]
| [[European Film Awards|Audience Award]]
| John Madden
| {{nom}}
|-
| [[24th GLAAD Media Awards|GLAAD Media Awards]]
| [[GLAAD Media Award for Outstanding Film – Wide Release|Outstanding Film – Wide Release]]
| 
| {{nom}}
|-
| rowspan="2" | [[70th Golden Globe Awards|Golden Globe Award]]
| [[Golden Globe Award for Best Motion Picture – Musical or Comedy]]
| Film
| {{nom}}
|-
| [[Golden Globe Award for Best Actress – Motion Picture Musical or Comedy]]
| Judi Dench
| {{nom}}
|-
| rowspan="2"| [[19th Screen Actors Guild Awards|Screen Actors Guild Award]]
| [[Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture|Outstanding Performance by a Cast in a Motion Picture]]
| Cast
| {{nom}}
|-
| [[Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role|Outstanding Performance by a Female Actor in a Supporting Role]]
| Maggie Smith
| {{nom}}
|-
| rowspan="2"| [[Women Film Critics Circle|Women Film Critics Circle Awards]]
| Best Comedic Actress
| Maggie Smith
| {{won}}
|-
| Women's Work: Best Ensemble
| Maggie Smith, Judi Dench, [[Penelope Wilton]] and [[Celia Imrie]]
| {{won}}
|}

==See also==
{{portal|Film}}
* [[2012 in film]]
* [[Cinema of the United Kingdom]]
* [[List of British films of 2012]]

{{clear}}

==References==
{{reflist|25em}}

==External links==
* {{official website|http://www.foxsearchlight.com/thebestexoticmarigoldhotel/}}
* {{IMDb title|1412386|The Best Exotic Marigold Hotel}}
* {{allrovi movie|541446|The Best Exotic Marigold Hotel}}

{{John Madden|state=collapsed}}

{{good article}}

{{DEFAULTSORT:Best Exotic Marigold Hotel}}
[[Category:2011 films]]
[[Category:2010s comedy-drama films]]
[[Category:2010s independent films]]
[[Category:2010s LGBT-related films]]
[[Category:British comedy-drama films]]
[[Category:British films]]
[[Category:British independent films]]
[[Category:British LGBT-related films]]
[[Category:English-language films]]
[[Category:Film scores by Thomas Newman]]
[[Category:Films about old age]]
[[Category:Films based on British novels]]
[[Category:Films directed by John Madden]]
[[Category:Films produced by Graham Broadbent]]
[[Category:Films set in England]]
[[Category:Films set in hotels]]
[[Category:Films set in Rajasthan]]
[[Category:Films shot in England]]
[[Category:Films shot in Rajasthan]]
[[Category:Fox Searchlight Pictures films]]
[[Category:LGBT-related comedy-drama films]]
[[Category:Participant Media films]]
[[Category:Foreign films shot in India]]