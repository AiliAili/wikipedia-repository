{{EngvarB|date=March 2014}}
{{Use dmy dates|date=March 2014}}
{{Infobox accounting_body
|name_in_local_language= <!--Give name in other languages. Page name will be added as English name automatically  -->
|abbreviation          = BICA
|image                 = BICA Bahamas logo.png
|size                  = 125px
|image_border          = 
|alt                   = <!-- alt text; see [[WP:ALT]] -->
|image_caption         = 
|motto                 = 
|predecessor           = 
|formation             = {{Start date and years ago|df=yes|1971|08|06}}
|legal_status          = 
|objective             = Education and licensing of professional accountants
|headquarters          = Nassau, {{flagu|Bahamas}}
|coords                = 
|region_served         = Bahamas
|membership_num        = 
|students_num          = <!-- No of students -->
|members_designations  = <!--give the various designations awarded to the members like ACA, FCA, etc.. -->
|language              = English
|highest_officer_title = President
|highest_officer_name  = Darnell Osborne 
|second_officer_title  = President Elect
|second_officer_name   = Gowon Bowe
|third_officer_title   = <!-- position of the third ranking officer like Secretary, CEO etc.. -->
|third_officer_name    = <!-- name of the above person -->
|fourth_officer_title  = <!-- position of the fourth ranking officer like Treasurer, CFO etc.. -->
|fourth_officer_name   = <!-- name of the above person -->
|administering_organ   = <!-- Council, General Body, board of directors, etc -->
|IFAC_membership_date  = <!-- Date when the body became a IFAC member if it is one -->
|additional_data_name1 = <!-- Optional data1 name -->
|additional_data1      = <!-- Optional data1 -->
|additional_data_name2 = <!-- Optional data2 name -->
|additional_data2      = <!-- Optional data2 -->
|additional_data_name3 = <!-- Optional data3 name -->
|additional_data3      = <!-- Optional data2 -->
|num_staff             =
|website               = http://www.bica.bs/
|remarks               =
|former name           = <!-- former name if any -->
}}
The '''Bahamas Institute of Chartered Accountants''' (BICA) is a professional body that regulates the accountancy industry in the [[Bahamas]].<ref>{{cite web
|url=http://www.thebahamasweekly.com/publish/community/Bahamas_Institute_of_Chartered_Accountants16358.shtml
|title=Bahamas Institute of Chartered Accountants to honour Members in Grand Style
|author=Eileen Fielder
|date=9 June 2011
|work=Bahamas Weekly
|accessdate=2 July 2011}}</ref>
In theory anyone approved by the relevant government ministry can act as an independent auditor, but in practice, all auditors are members of BICA.<ref>{{cite web
|url=http://www.mondaq.com/article.asp?articleid=3879
|publisher=Deloitte & Touche
|title=Bahamas: International Tax and Business Guide – Setting Up A Business In The Bahamas – A Quick Guide For Foreigners
|date=17 December 1999
|author=Richard Evans
|accessdate=2 July 2011}}</ref>

==History==

BICA was formed by Maitland Cates, Basil Sands and Clifford Culmer, who were among the first Bahamians to qualify as chartered accountants in the 1960s.<ref>{{cite web
 |url=http://freeport.nassauguardian.net/print/291479711136207.php 
 |work=The Freeport News 
 |date=2 July 2007 
 |title=Inspired, Maitland Cates' staff past and present say thanks 
 |author=THEA RUTHERFORD 
 |accessdate=2 July 2011 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120320012219/http://freeport.nassauguardian.net/print/291479711136207.php 
 |archivedate=20 March 2012 
 |df=dmy 
}}</ref>
The Bahamas Institute of Chartered Accountants was incorporated on 6 August 1971. 
BICA became a member of the [[International Federation of Accountants]] in 1978, and a member of The [[Institute of Chartered Accountants of The Caribbean]] on 28 October 1988.
The Public Accountants Act 1991 was enacted on 17 July 1991, empowering the institute to regulate and govern the accounting industry in the Bahamas.<ref name=BICAhist/>
On 18 June 2011 BICA celebrated its fortieth anniversary with a banquet at the Atlantis Grand Ballroom.
BICA has over 300 active licensees and 500 members and associates at 31 December 2015.<ref>{{cite web
|url=http://www.thenassauguardian.com/index.php?option=com_content&view=article&id=10869&Itemid=2
|title=BICA celebrating 40 years
|work=Nassau Guardian
|author=JAMMAL SMITH
|date=9 June 2011
|accessdate=2 July 2011}}</ref>

==Objectives==

BICA defines the qualifications required of accountants in the Bahamas, regulates the professional conduct of members, associates, students, and public accountants who are not members, and promotes the interests of the accounting profession. 
The institute promotes best practices of financial reporting, promotes professional education of accountants, provides for education and examination of professionals, arranges lectures and discussions and disseminates information.<ref>{{cite web
|url=http://www.bica.bs/pdf/2011bica/BICAANNUAL2010_2011.pdf
|title=2010/2011 Annual Report 
|publisher=BICA
|accessdate=2 July 2011| archiveurl= https://web.archive.org/web/20110706163017/http://www.bica.bs/pdf/2011bica/BICAANNUAL2010_2011.pdf| archivedate= 6 July 2011 | deadurl= no}}</ref>

==Other organisations==

BICA is a member of the [[International Federation of Accountants]] (IFAC).<ref>{{cite web
 |url=http://web.ifac.org/about/member-body/baha1 
 |title=The Bahamas Institute of Chartered Accountants 
 |publisher=IFAC 
 |accessdate=30 June 2011 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110710002723/http://web.ifac.org:80/about/member-body/baha1 
 |archivedate=10 July 2011 
 |df=dmy 
}}</ref>
BICA is also a member of the [[Institute of Chartered Accountants of the Caribbean]].<ref>{{cite web
|url=http://www.icac.org.jm/index.php?option=com_content&task=view&id=14&Itemid=28
|title=Members And Affiliates
|publisher=ICAC
|accessdate=1 July 2011}}</ref>

==References==
{{reflist |refs=
<ref name=BICAhist>{{cite web
|url=http://www.bica.bs/history.lasso
|title=HISTORY OF THE BAHAMAS INSTITUTE OF CHARTERED ACCOUNTANTS (BICA)
|publisher=BICA
|accessdate=2 July 2011| archiveurl= https://web.archive.org/web/20110706164742/http://www.bica.bs/history.lasso| archivedate= 6 July 2011 | deadurl= no}}</ref>
}}
{{IFAC Members}}
[[Category:Professional accounting bodies]]