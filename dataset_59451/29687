{{Infobox London station
| name=Aldwych
| caption=Main entrance to Aldwych station {{nowrap|(formerly Strand station)}}
| image_name = Aldwych tube station 1.jpg
| imagesize  = x265px
| image_alt=Main entrance on the Strand, London. The station is referred to by its previous name, Strand.
| owner=[[Great Northern, Piccadilly and Brompton Railway]]
| locale=[[Aldwych]]
| borough=[[City of Westminster]]
| latitude = 51.51215
| longitude = -0.11595
| coord_region = GB-WSM
| map_type = Central London
| label_position = top
| platforms= {{plainlist|
* 2 (1907–1917)
* 1 (1917–1994)
}}
| years1 = {{start date|1907}}
| events1 = Opened
| years2 = 1940
| events2 = Closed
| years3 = 1946
| events3 = Reopened
| years4 = 1994
| events4 = Closed
| listing_grade    = II
| listing_detail   =
| listing_start    = 20 July 2011
| listing_amended  = 
| listing_entry    = 1401034
| listing_reference=<ref name="nhle">{{National Heritage List for England | num=1401034 | desc=Aldwych Underground Station | accessdate=27 July 2011}}</ref>}}
'''Aldwych''' is a [[List of former and unopened London Underground stations|closed station]] on the [[London Underground]], located in the [[City of Westminster]] in [[Central London]]. It was opened in 1907 with the name '''Strand''', after [[Strand, London|the street]] on which it is located, and was the terminus of the short [[Piccadilly line]] branch from [[Holborn tube station|Holborn]] that was a relic of the merger of two railway schemes. The station building is close to the Strand's junction with Surrey Street, near [[Aldwych]]. During its lifetime, the branch was the subject of a number of unrealised extension proposals that would have seen the tunnels through the station extended southwards, usually to [[Waterloo tube station|Waterloo]].

Served mostly by a [[shuttle train]] and having low passenger numbers, the station and branch were considered for closure several times. Service was offered only during weekday peak hours from 1962 and discontinued in 1994, when the cost of replacing the [[elevator|lifts]] was considered too high for the income generated.

Disused parts of the station and the running tunnels were used during both world wars to shelter artworks from London's public galleries and museums from bombing. The station has long been popular as a filming location and has appeared as itself and as other London Underground stations in a number of films. In recognition of its historical significance, the station is a Grade II [[listed building]].

==History==

===Planning===
[[File:Aldwych in 1900 (station location).png|thumb|left|310px|alt=Map showing planned station locations near the Strand.|Eastern end of [[Strand, London|Strand]] in 1900, before construction of [[Kingsway, London|Kingsway]] and [[Aldwych]]. Planned locations of the station are shown in red:<br />
1. Corner of Holles Street (not shown) and Stanhope Street;<br />
2. Future junction of Kingsway and Aldwych;<br />
3. Corner of [[Strand, London|Strand]] and Surrey Street.
]]
The [[Great Northern and Strand Railway]] (GN&SR) first proposed a station in the Strand area in a [[private bill]] presented to Parliament in November 1898.<ref name="gazette_27025">{{London Gazette | date=22 November 1898 | issue=27025 | startpage=7040 | endpage=7043 | accessdate=10 August 2011 }}</ref> The station was to be the southern terminus of an underground railway line planned to run from [[Alexandra Palace railway station|Wood Green]] station (now Alexandra Palace) via [[Finsbury Park station|Finsbury Park]] and [[King's Cross St. Pancras tube station|King's Cross]] and was originally to be located at the corner of Stanhope Street and Holles Street, north of the Strand. When the two streets were scheduled for demolition as part of the [[London County Council]]'s plans for the construction of [[Kingsway, London|Kingsway]] and [[Aldwych]], the GN&SR moved the location to the junction of the two new roads.{{sfn|Badsey-Ellis|2005|p=77}} [[Royal Assent]] to the bill was given and the ''Great Northern and Strand Railway Act 1899'' was enacted on 1 August.<ref name="gazette_27105">{{London Gazette | date=4 August 1899 | issue=27105 | startpage=4833 | endpage=4834 | accessdate=23 July 2010 }}</ref>

In September 1901, the GN&SR was taken over by the [[Brompton and Piccadilly Circus Railway]] (B&PCR), which planned to build an underground line from [[South Kensington tube station|South Kensington]] to [[Piccadilly Circus tube station|Piccadilly Circus]] via [[Knightsbridge tube station|Knightsbridge]]. Both were under the control of [[Charles Yerkes]] through his Metropolitan District Electric Traction Company and, in June 1902, were transferred to Yerkes' new [[holding company]], the [[Underground Electric Railways Company of London]] (UERL).{{sfn|Badsey-Ellis|2005|p=118}} Neither of the railways had carried out any construction, but the UERL obtained permission for new tunnels between Piccadilly Circus and Holborn to connect the two routes. The companies were formally merged as the [[Great Northern, Piccadilly and Brompton Railway]] (GNP&BR) following parliamentary approval in November 1902.{{sfn|Badsey-Ellis|2005|pp=152–53}}<ref name="gazette_27464a">{{London Gazette | date=12 August 1902 | issue=27464 | startpage=5247 | endpage=5248 | accessdate=23 July 2010 }}</ref><ref name="gazette_27464b">{{London Gazette | date=21 November 1902 | issue=27497 | startpage=7533 | accessdate=23 July 2010 }}</ref> Prior to confirmation of the merger, the GN&SR had sought permission to extend its line southwards from the future junction of Kingsway and Aldwych, under Norfolk Street to a new interchange under the [[District Railway|Metropolitan District Railway's]] station at [[Temple tube station|Temple]]. The extension was rejected following objections from the [[Henry Fitzalan-Howard, 15th Duke of Norfolk|Duke of Norfolk]] under whose land the last part of the proposed tunnels would have run.{{sfn|Badsey-Ellis|2005|p=138}}

In 1903, the GNP&BR sought permission for a branch from Piccadilly Circus to run under [[Leicester Square]], Strand, and [[Fleet Street]] and into the [[City of London]]. The branch would have passed and interchanged with the already approved Strand station,{{sfn|Badsey-Ellis|2005|p=215}} allowing travel on the GNP&BR from Strand in three directions. The deliberations of a [[Royal Commission]] on traffic in London prevented parliamentary consideration of the proposal, which was withdrawn.{{sfn|Badsey-Ellis|2005|p=222}}

In 1905, with the Royal Commission's report about to be published, the GNP&BR returned to Parliament with two bills for consideration. The first bill revived the 1903 proposal for a branch from Piccadilly Circus to the City of London, passing and interchanging with Strand station. The second proposed an extension and relocation of Strand station to the junction of Strand and Surrey Street. From there the line was to continue as a single tunnel under the River Thames to [[Waterloo tube station|Waterloo]]. The first bill was again delayed and withdrawn. Of the second, only the relocation of Strand station was permitted.{{sfn|Badsey-Ellis|2005|pp=241–42}}
{{Clear left}}

===Construction===
[[File:Aldwych tube station plan.svg|thumb|right|300px||alt=Station plan: only the westernmost lift and passage entered into use.|Layout of station at platform level, including parts of the station that were never used.]]
The linking of the GN&SR and B&PCR routes meant that the section of the GN&SR south of Holborn became a branch from the main route. The UERL began constructing the main route in July 1902. Progress was rapid, so that it was largely complete by the Autumn of 1906.{{sfn|Wolmar|2005|p=181}} Construction of the Holborn to Strand section was delayed while the London County Council constructed Kingsway and the [[Kingsway tramway subway|tramway subway]] running beneath it and while the UERL decided how the junction between the main route and the branch would be arranged at Holborn.{{sfn|Badsey-Ellis|2005|p=239}}{{refn|When originally planned, Holborn station was to have just two platforms, shared by trains on the main route and by the shuttle service from Aldwych. The junctions between the routes would have been south of the station. The interference that shuttle trains would have caused to the main route led to a redesign so that two northbound platforms were provided, one for the main line and one for the branch line, with a single southbound platform. The junctions between the two northbound tunnels would have been north of the platforms. When powers were sought to build the junction in 1905, the layout was changed again so that four platforms were to be provided. The southbound tunnel of the main route no longer connected to the branch, which was provided with an additional platform in a dead-end tunnel accessed from a cross tunnel from the northbound branch tunnel. As built, the dead-end platform tunnel was adjacent to the northbound main line platform, for ease of access.{{sfn|Badsey-Ellis|2005|pp=239–41}}|group="note"}}

Strand station was built on the site of the [[Royal Strand Theatre]], which had closed on 13 May 1905 and been demolished. Construction of the station began on 21 October 1905,{{sfn|Connor|2001|p=94}} to a design by the UERL's architect [[Leslie Green]] in the UERL house style of a two-storey steel-framed building faced with red glazed [[glazed architectural terra-cotta|terracotta]] blocks, with wide semi-circular windows on the upper floor.{{sfn|Wolmar|2005|p=175}} The station building is L-shaped, with two façades separated by the building on the corner of Strand and Surrey Street. The Strand façade is narrow with a single semi-circular window above the entrance. The façade in Surrey Street is wider with a separate entrance and exit and a shop unit. In anticipation of a revival of the extension to Waterloo and the City route, the station was built with three circular lift shafts able to accommodate six [[trapezoid|trapezium]]-shaped lifts. Only one of the shafts was fitted out, with two lifts.{{sfn|Badsey-Ellis|2005|pp=242–43}} The other two shafts rose from the lower concourse to the basement of the station, but could have been extended upwards into the space of the shop unit when required. A fourth smaller-diameter shaft accommodated an emergency spiral stair.<ref>Architect's drawing of station&nbsp;– {{harvnb|Connor|2001|p=95}}.</ref>

The platforms are {{convert|92|ft|6|in|m}} below street level and are {{convert|250|ft|m}} long;{{sfn|Connor|2001|p=94}} shorter than the GNP&BR's standard length of {{convert|350|ft|m}}.{{sfn|Badsey-Ellis|2005|p=238}} As with other UERL stations, the platform walls were tiled with distinctive patterns, in this case cream and dark green. Only parts of the platform walls were decorated because it was planned to operate the branch with short trains.{{sfn|Connor|2001|p=94}} Owing to the reduced lift provision, a second route between the platforms and lifts was never brought into use and was left in an unfinished condition without tiling.{{sfn|Badsey-Ellis|2005|p=243}}

===Operation===
[[File:Aldwych branch (en).svg|thumb|alt=Diagram of tunnels on the Aldwych branch: the route between 1917 and 1994 crosses over from the western track to the eastern and arrives in the branch's through platform at Holborn.|The layout of the Aldwych branch (not to scale).]]
The GNP&BR's main route opened on 15 December 1906, but the Strand branch was not opened until 30 November 1907.{{sfn|Rose|1999}} Initially, shuttle trains operated to Holborn from the eastern platform into the through platform at Holborn. At peak times, an additional train operated alternately in the branch's western tunnel into the bay platform at Holborn. During the first year of operation, a train for theatregoers operated late on Monday to Saturday evenings from Strand through Holborn and northbound to Finsbury Park; this was discontinued in October 1908.{{sfn|Connor|2001|p=94}}

In March 1908, the off-peak shuttle service began to use the western platform at Strand and the through platform at Holborn, crossing between the two branch tunnels south of Holborn. Low usage led to the withdrawal of the second peak-hour shuttle and the eastern tunnel was taken out of use in 1914.{{sfn|Connor|2001|p=95}}<ref name="open">{{cite book | title=Aldwych Underground Station Open Day | year=2011 | publisher=[[London Transport Museum]]/[[Transport for London]] | page=5 }}</ref> On 9 May 1915, three of the Underground stations in the area were renamed and Strand station became Aldwych.{{sfn|Rose|1999}}{{refn|The Strand name was transferred to [[Charing Cross tube station|Charing Cross (Strand) station]], now the [[Northern line]] platforms of Charing Cross. The other station renamed was [[Embankment tube station|Charing Cross (Embankment)]], which became Charing Cross, and in 1979, Embankment.{{sfn|Rose|1999}}|name=note1|group=note}} Sunday services ended in April 1917 and, in August of the same year, the eastern tunnel and platform at Aldwych and the bay platform at Holborn were formally closed.{{sfn|Connor|2001|p=98}} A German bombing campaign in September 1917 led to the disused platform being used as storage for around 300 paintings from the [[National Gallery]] from then until December 1918.{{sfn|Wolmar|2005|pp=212 & 214}}
[[File:Strand Tube Station - eastern platform - DSCF0806 (J50).jpg|thumb|left||alt=View along platform in 2011.|Eastern platform, taken out of use in 1914, showing the exposed structure of the tunnel.]]

In October 1922, the ticket office was replaced by a facility in the lifts.{{sfn|Connor|2001|p=98}} Passenger numbers remained low: when the station was one of a number on the network considered for closure in 1929, its annual usage was 1,069,650 and takings were £4,500.{{sfn|Connor|2001|p=31}}{{refn|The other stations considered for closure were [[Down Street tube station|Down Street]] (closed 1932), [[York Road tube station|York Road]] (closed 1932), [[Brompton Road tube station|Brompton Road]] (closed 1934), [[Regent's Park tube station|Regent's Park]], [[Mornington Crescent tube station|Mornington Crescent]], [[Hyde Park Corner tube station|Hyde Park Corner]], [[Arsenal tube station|Gillespie Road]] (now Arsenal), [[Gloucester Road tube station|Gloucester Road]] and [[Covent Garden tube station|Covent Garden]].{{sfn|Connor|2001|p=31}}|name=note2|group=note}} The branch was again considered for closure in 1933, but remained open.{{sfn|Connor|2001|p=98}}

Wartime efficiency measures led to the branch being closed temporarily on 22 September 1940, shortly after the start of [[The Blitz]], and it was partly fitted out by the [[City of Westminster]] as an [[air-raid shelter]]. The tunnels between Aldwych and Holborn were used to store items from the [[British Museum]], including the [[Elgin Marbles]]. The branch reopened on 1 July 1946, but patronage did not increase.{{sfn|Connor|2001|pp=98–99}} In 1958, the station was one of three that [[London Transport Executive|London Transport]] announced would be closed. Again it survived, but the service was reduced in June 1958 to run during Monday to Friday peak hours and Saturday morning and early afternoons.{{sfn|Connor|2001|p=99}}{{refn|[[Mornington Crescent tube station|Mornington Crescent]] and [[South Acton tube station|South Acton]] were the other stations which were to be closed. Only the latter did, on 28 February 1959.{{sfn|Rose|1999}}{{sfn|Connor|2001|p=99}}|name=note3|group=note}} The Saturday service was withdrawn in June 1962.{{sfn|Connor|2001|p=99}}
[[File:The Home Front in Britain during the Second World War HU44272.jpg|thumb|right|Shelterers inside Aldwych station during [[the Blitz]], 1940.]]

{{clear left}}

After operating only during peak hours for more than 30&nbsp;years, the closure announcement came on 4 January 1993. The original 1907 lifts required replacement at a cost of £3&nbsp;million. This was not considered justifiable in relation to the passenger numbers using the station and it was losing [[London Regional Transport]] £150,000 per year. The [[Secretary of State for Transport]] granted permission on 1 September 1994 to close the station and the branch closed on 30 September.{{sfn|Connor|2001|pp=100–101}}{{refn|The [[Epping tube station|Epping]] to [[Ongar railway station|Ongar]] section of the [[Central line (London Underground)|Central line]] also closed on 30 September.{{sfn|Rose|1999}}|name=note4|group=note}} Recognising the station's historical significance as a mostly unaltered station from the early 20th century, the station was given Grade II [[listed building]] status in 2011.<ref name="nhle"/>
{{Clear left}}

===Proposals for extension and new connections===
[[File:Aldwych tube station when open.jpg|thumb|left|alt=Station entrance when open: a canopy covers the station's previous name.|The station entrance before closure, with a canopy bearing the name it had from 1915.]]
Although the Piccadilly Circus to City of London branch proposal of 1905 was never revisited after its withdrawal, the early plan to extend the branch south to Waterloo was revived a number of times during the station's life. The extension was considered in 1919 and 1948, but no progress towards constructing the link was made.{{sfn|Connor|2001|pp=98–99}}

In the years after the Second World War, a series of preliminary plans for relieving congestion on the London Underground had considered various east-west routes through the Aldwych area, although other priorities meant that these were never proceeded with. In March 1965, a [[British Railways Board|British Rail]] and [[London Transport Board|London Transport]] joint planning committee published "A Railway Plan for London" which proposed a new tube railway, the [[Fleet line]] (later renamed the [[Jubilee line]]), to join the [[Bakerloo line]] at {{LUL stations|station=Baker Street}} then run via {{LUL stations|station=Bond Street}}, {{LUL stations|station=Green Park}}, {{LUL stations|station=Charing Cross}}, Aldwych and into the City of London via {{LUL stations|station=Ludgate Circus}}, {{LUL stations|station=Cannon Street}} and {{Stnlnk|Fenchurch Street}} before heading into south-east London. An interchange was proposed at Aldwych and a second recommendation of the report was the revival of the link from Aldwych to Waterloo.{{sfn|Horne|2000|pp=31–33}}<ref name="railarch">{{cite book | url=http://www.railwaysarchive.co.uk/documents/BRLT_RailwayPlanForLondon1965.pdf | format=PDF | title=A Railway Plan for London | date=March 1965 | author=[[British Railways Board]]/[[London Transport Board]] | page=23 }}</ref> London Transport had already sought parliamentary approval to construct tunnels from Aldwych to Waterloo in November 1964,<ref name="notices">{{cite journal | date=3 December 1964 | title=Parliamentary Notices | journal=[[The Times]] | issue=56185 | pages=2 | url=http://infotrac.galegroup.com/itw/infomark/322/785/116929157w16/purl=rc1_TTDA_0_CS33777539&dyn=17!xrn_1_0_CS33777539&hst_1 | archiveurl=https://web.archive.org/web/20120927174941/http://infotrac.galegroup.com/itw/infomark/322/785/116929157w16/purl=rc1_TTDA_0_CS33777539&dyn=17!xrn_1_0_CS33777539&hst_1 | archivedate=27 September 2012 | deadurl=no | subscription=yes }}</ref> and in August 1965, parliamentary powers were granted. Detailed planning took place, although public spending cuts led to postponement of the scheme in 1967 before tenders were invited.{{sfn|Connor|2001|p=99}}

Planning of the Fleet line continued and parliamentary approval was given in July 1969 for the first phase of the line, from Baker Street to Charing Cross.{{sfn|Horne|2000|p=34}} Tunnelling began on the £35&nbsp;million route in February 1972 and the Jubilee line opened north from Charing Cross in May 1979.{{sfn|Horne|2000|pp=35, 38 & 45}} The tunnels of the approved section continued east of Charing Cross under Strand almost as far as Aldwych station, but no work at Aldwych was undertaken and they were used only as sidings.{{sfn|Horne|2000|pp=35, 38 & 42}} Funding for the second phase of the work was delayed throughout the 1970s whilst the route beyond Charing Cross was reviewed to consider options for serving anticipated development in the [[London Docklands]] area. By 1979, the cost was estimated as £325&nbsp;million, a six-fold increase from the £51&nbsp;million estimated in 1970.{{sfn|Horne|2000|pp=35 & 52}} A further review of alternatives for the Jubilee line was carried out in 1980, which led to a change of priorities and the postponement of any further effort on the line.{{sfn|Horne|2000|p=53}} When the extension was eventually constructed in the late 1990s it took a different route, south of the River Thames via {{LUL stations|station=Westminster}}, {{LUL stations|station=Waterloo}} and {{LUL stations|station=London Bridge}} to provide a rapid link to {{LUL stations|station=Canary Wharf}}, leaving the tunnels between {{LUL stations|station=Green Park}} and Aldwych redundant.{{sfn|Horne|2000|p=57}}

In July 2005, [[Arup Group Limited|Ove Arup & Partners]] produced a report, ''DLR Horizon 2020 Study'', for the [[Docklands Light Railway]] (DLR) examining "pragmatic development schemes" to expand and improve the DLR network between 2012 and 2020. One of the proposals was an extension of the DLR from {{LUL stations|station=Bank}} to {{LUL stations|station=Charing Cross}} via {{Stnlnk|City Thameslink}} and Aldwych. The disused Jubilee line tunnels would be enlarged to accommodate the larger DLR trains and Aldwych station would form the basis for a new station on the line, although requiring considerable reconstruction to accommodate escalators. The estimated cost in 2005 was £232&nbsp;million for the infrastructure works and the scheme was described as "strongly beneficial" as it was expected to attract passengers from the London Underground's existing east-west routes and from local buses and reduce overcrowding at Bank station. The [[business case]] assessment was that the proposal offered high value, although similar values were calculated for other extension proposals from Bank. Further detailed studies were proposed.<ref name="wdtk">{{cite book | url=http://www.whatdotheyknow.com/request/18439/response/46991/attach/4/Report%20Horizon%202020%20Final%20Issue.pdf | format=PDF | title=DLR Horizon 2020 Study | date=July 2005 | author=Ove Arup & Partners | pages=34–38 & 66 | authorlink=Arup Group Limited | archiveurl=https://web.archive.org/web/20111108214018/http://www.whatdotheyknow.com/request/18439/response/46991/attach/4/Report%20Horizon%202020%20Final%20Issue.pdf | archivedate=8 November 2011 | deadurl=no }} As of August 2011 no decision has been taken about the future of Aldwych station as an element of an expanded DLR.</ref>

In 2015, a scheme was proposed by the design firm [[Gensler]] to convert disused London Underground tunnels into subterranean [[rail trail]]s, enabling the disused branches of the Piccadilly line and Jubilee line to be used as cycle paths. The scheme, which would involve re-opening Aldwych station as an access point for cyclists, has not been officially approved.<ref>{{cite news|last1=O'Sullivan|first1=Feargus|title=Bike paths in abandoned tube tunnels: is the London Underline serious?|url=https://www.theguardian.com/cities/2015/feb/05/bike-paths-abandoned-tube-tunnels-london-underline|accessdate=7 February 2015|work=The Guardian}}</ref>

==Use in media==
[[File:Aldwych tube station platform in 1994.jpg|thumb|alt=View along platform in 1994.|Western platform shortly before closure. The undecorated section of wall is visible at the far end of the tunnel.]]
Because it was a self-contained section of the London Underground which was closed outside weekday peak hours, Aldwych station and the branch line from Holborn were popular locations for filming scenes set on the Tube even before their closure. Since the branch's closure in 1994, its use in film productions has continued, with the station appearing as itself and, with appropriate signage, as other stations on the network.{{sfn|Connor|2001|p=99}} The track and infrastructure are maintained in operational condition, and a train of ex-[[Northern line]] [[London Underground 1972 Stock|1972 tube stock]] is permanently stabled on the branch. This train can be driven up and down the branch for filming. The physical connection with the [[Piccadilly line]] northbound tracks remains, but requires manual operation.<ref name="cravens">{{cite web | title=Filming At Aldwych July 2002 | url=http://www.cravensheritagetrains.co.uk/aldwych.htm | publisher=Cravens Heritage Trains | date=6 December 2006 | archiveurl=https://web.archive.org/web/20051230071006/http://www.cravensheritagetrains.co.uk/aldwych.htm | archivedate=30 December 2005 | deadurl=no }}</ref>

Films and television productions that have been shot at Aldwych include:
<!--PLEASE NOTE: DO NOT ADD THE JAMES BOND FILM 'DIE ANOTHER DAY' TO THIS LIST, AS THE TUBE STATION SEEN IN IT IS A STUDIO SET AND IS CATEGORICALLY NOT ALDWYCH-->
<!--PLEASE NOTE: DO NOT ADD THE FILM 'THE CHRONICLES OF NARNIA: PRINCE CASPIAN' TO THIS LIST, AS THE TUBE STATION INTERIOR SEEN IN IT IS A STUDIO SET AND IS CATEGORICALLY NOT ALDWYCH-->
<!--PLEASE NOTE: ONLY FILMS AND PROGRAMMES THAT CAN HAVE RELIABLE CITATIONS SHOULD BE ADDED-->
* ''[[The Gentle Gunman]]'' (1952){{sfn|Connor|2001|p=99}}
* ''[[Battle of Britain (film)|Battle of Britain]]'' (1969)<ref name="tfl film" />
* ''[[Death Line]]'' (1972){{sfn|Connor|2001|p=99}}
* ''[[Superman IV: The Quest for Peace]]'' (1986){{sfn|Connor|2001|p=99}}
* ''[[The Krays (film)|The Krays]]'' (1990){{sfn|Badsey-Ellis|Horne|2009|p=104}}
* ''[[Patriot Games (film)|Patriot Games]]'' (1994){{sfn|Badsey-Ellis|Horne|2009|p=104}}
* ''[[Creep (2004 film)|Creep]]'' (2004)<ref name="tfl film">{{cite web | url=http://www.tfl.gov.uk/tfl/corporate/media/lufilmoffice/ | publisher=[[Transport for London]] | title=London Underground Film Office | archiveurl=https://web.archive.org/web/20100803043241/http://www.tfl.gov.uk/tfl/corporate/media/lufilmoffice/ | archivedate=3 August 2010 | deadurl=no }}</ref>
* ''[[V for Vendetta (film)|V for Vendetta]]'' (2006)<ref name="tfl film" />
* ''[[The Good Shepherd (film)|The Good Shepherd]]'' (2006)<ref name="tfl film" />
* ''[[Atonement (film)|Atonement]]'' (2007)<ref name="tfl film" />
* ''[[28 Weeks Later]]'' (2007)<ref name="tfl film" />
* ''[[The Edge of Love]]'' (2008)<ref name="tfl film" />
* ''[[Mr Selfridge (TV series)|Mr Selfridge]]'' (2013)<ref name="itv">{{cite web | url=http://presscentre.itvstatic.com/presscentre/sites/presscentre/files/Mr%20Selfridge%20ITV.pdf#page=22 | title=Mr Selfridge: Production Notes | publisher=[[ITV Studios]] | archiveurl=https://web.archive.org/web/20140211180601/http://presscentre.itvstatic.com/presscentre/sites/presscentre/files/Mr%20Selfridge%20ITV.pdf#page=22 | archivedate=11 February 2014 | deadurl=no }}</ref>
* ''[[Sherlock (TV series)|Sherlock]]'' (2014)<ref name="sherlock">{{cite web | url=http://www.bbc.co.uk/news/magazine-25576814 | title=The allure of abandoned Tube stations | work=BBC News | date=2 January 2014 | archiveurl=https://web.archive.org/web/20140102172253/http://www.bbc.co.uk/news/magazine-25576814 | archivedate=2 January 2014 | deadurl=no }}</ref>
<!--PLEASE NOTE: DO NOT ADD THE JAMES BOND FILM 'DIE ANOTHER DAY' TO THIS LIST, AS THE TUBE STATION SEEN IN IT IS A STUDIO SET AND IS CATEGORICALLY NOT ALDWYCH-->
<!--PLEASE NOTE: DO NOT ADD THE FILM 'THE CHRONICLES OF NARNIA: PRINCE CASPIAN' TO THIS LIST, AS THE TUBE STATION INTERIOR SEEN IN IT IS A STUDIO SET AND IS CATEGORICALLY NOT ALDWYCH-->
<!--PLEASE NOTE: ONLY FILMS AND PROGRAMMES THAT CAN HAVE RELIABLE CITATIONS SHOULD BE ADDED-->

The pre-war operation of the station features in a pivotal scene in [[Geoffrey Household]]'s novel ''[[Rogue Male (novel)|Rogue Male]]'', when the pursuit of the protagonist by an enemy agent sees them repeatedly using the shuttle service on the branch line. A chase through Aldwych station ends with the agent's death by electrocution on the track.{{sfn|Household|1977|pp=62–63}} A much modified and expanded version of the station appears as a [[level (video gaming)|level]] in the [[video game]] ''[[Tomb Raider III]]''.<ref name="ia">{{cite web | url=https://archive.org/details/TombRaiderIii-AldwychIn418ByXxapelaumxx | work=[[Internet Archive]] | title=Tomb Raider III&nbsp;— Aldwych in 4:18 | accessdate=21 August 2011 }}</ref> The music video for [[The Prodigy]]'s song "[[Firestarter (The Prodigy song)|Firestarter]]" was filmed in the disused eastern tunnel and one of the unused lift shafts.<ref name="sub">{{cite web | url=http://www.subbrit.org.uk/sb-sites/sites/a/aldwych-holborn-branch_line/index2.shtml | work=Subterranea Britannica | title=Site Records: Aldwych&nbsp;— Holborn branch | archiveurl=https://web.archive.org/web/20030127093656/http://www.subbrit.org.uk/sb-sites/sites/a/aldwych-holborn-branch_line/index2.shtml | archivedate=27 January 2003 | deadurl=no }}</ref> The station was the subject of an episode of ''[[Most Haunted]]'' in 2002.<ref name="Most Haunted">{{cite web |url=http://www.tv.com/shows/most-haunted/aldwych-underground-station-282477// |title=Most Haunted – Season 1 – Episode 15 |publisher=[[TV.com]] |accessdate=28 February 2015}}</ref>

==See also==
* [[List of former and unopened London Underground stations]]

==Notes and references==

===Notes===
{{reflist|group=note|45em}}

===References===
{{Reflist|25em}}

===Bibliography===
{{refbegin}}
* {{cite book | last=Badsey-Ellis | first=Antony | title=London's Lost Tube Schemes | publisher=Capital Transport | isbn=1-85414-293-3 | year=2005 | ref=harv }}
* {{cite book | last1=Badsey-Ellis | first1=Antony | last2=Horne | first2=Mike | title=The Aldwych Branch | publisher=Capital Transport | isbn=978-1-85414-321-1 | year=2009 | ref=harv }}
* {{cite book | last=Connor | first=J.E. | title=London's Disused Underground Stations | year=2001 | origyear=1999 | publisher=Capital Transport | isbn=1-85414-250-X | ref=harv }}
* {{cite book | last=Horne | first=Mike | title=The Jubilee Line | year=2000 | publisher=Capital Transport | isbn=1-85414-220-8 | ref=harv }}
* {{cite book | title=Rogue Male | last=Household | authorlink=Geoffrey Household | first=Geoffrey | publisher=[[Penguin Books]] | origyear=1939 | year=1977 | isbn=0-14-000695-8 | ref=harv }}
* {{cite book | last=Rose | first=Douglas | title=The London Underground, A Diagrammatic History | year=1999 | publisher=Douglas Rose/Capital Transport | isbn=1-85414-219-4 | ref=harv }}
* {{cite book | last=Wolmar | first=Christian | authorlink=Christian Wolmar | title=The Subterranean Railway: How the London Underground Was Built and How It Changed the City Forever | publisher=Atlantic Books | isbn=1-84354-023-1 | origyear=2004 | year=2005 | ref=harv }}
{{refend}}

==External links==
{{commons category|Aldwych tube station}}
* {{cite web | url=http://underground-history.co.uk/aldwych.php | work=Underground History | title=Aldwych Station | archiveurl=https://web.archive.org/web/20051210020418/http://underground-history.co.uk/aldwych.php | archivedate=10 December 2005 | deadurl=no }}
* {{cite web | url=http://www.abandonedstations.org.uk/Aldwych_station_1.html | title=Aldwych | work=London's Abandoned Tube Stations | publisher= | archiveurl=https://web.archive.org/web/20080817041709/http://www.abandonedstations.org.uk/Aldwych_station_1.html | archivedate=17 August 2008 | deadurl=no }}
* {{cite web | url=http://www.subbrit.org.uk/sb-sites/sites/a/aldwych-holborn-branch_line/ | title=Aldwych-Holborn Branch | work=Disused stations | publisher=Subterranea Britannica | archiveurl=https://web.archive.org/web/20030127093625/http://www.subbrit.org.uk/sb-sites/sites/a/aldwych-holborn-branch_line/index.shtml | archivedate=27 January 2003 | deadurl=no }}
* [http://www.ltmcollection.org/photos/index.html London Transport Museum Photographic Archive]
** {{ltmcollection|6n/i000056n.jpg|Strand station, mid-1907}}
** {{ltmcollection|pt/i0000opt.jpg|Surrey Street elevation, mid-1907}}
** {{ltmcollection|25/9858825.jpg|Surrey Street elevation after vertical extension, 1958}}
* {{cite news |url=http://www.telegraph.co.uk/finance/10804379/In-pictures-Underground-London-during-the-Second-World-War.html?frame=2899760 |title=An ENSA concert party entertains shelterers in Aldwych station |work=[[Daily Telegraph]] |date=3 May 2014 |accessdate=29 May 2014}}
* {{cite AV media |url=https://www.youtube.com/watch?v=6xSzU0oM4mM | title=The Secret Station – Aldwych  |people=Oliver, Luke |publisher=[[YouTube]] |date=12 June 2010 |accessdate=29 June 2014}}

{{s-start}}
{{S-note|text=Former}}
{{s-rail|title=LUL}}
{{Rail line|previous=[[Holborn tube station|Holborn]]<br><small>''Terminus''</small> |route=[[Piccadilly line]]<br><small>Aldwych Branch<br />(1907–1994)</small> |col={{LUL colour|Piccadilly}} }}
{{S-note|text=Abandoned Plans}}
{{s-rail|title=LUL}}
{{s-line|system=LUL|line=Jubilee|previous=Charing Cross|next=Ludgate Circus|type2=Phase2|notemid=Phase 2 (never constructed)}}
{{Rail line|previous=[[Holborn tube station|Holborn]]<br><small>''Terminus''|next=[[Waterloo tube station|Waterloo]]<br><small>''Terminus''|route=[[Piccadilly line]]<br><small>Proposed extension to Waterloo<br />(never constructed)</small> |col={{LUL colour|Piccadilly}} }}
{{s-end}}
{{Piccadilly line navbox}}
{{Jubilee line navbox}}
{{closed london underground stations}}
{{featured article}}

[[Category:Disused London Underground stations]]
[[Category:Disused railway stations in the City of Westminster]]
[[Category:Former Great Northern, Piccadilly and Brompton Railway stations]]
[[Category:Railway stations opened in 1907]]
[[Category:Railway stations closed in 1994]]
[[Category:Former single platform tube stations]]
[[Category:Grade II listed buildings in the City of Westminster]]
[[Category:Grade II listed railway stations]]
[[Category:King's College London]]
[[Category:Aldwych|Tube station]]
[[Category:Reportedly haunted locations in England]]
[[Category:Leslie Green railway stations]]