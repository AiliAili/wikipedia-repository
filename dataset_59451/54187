{{Use Australian English|date=August 2015}}
'''Edge Church''' (formerly '''Southside Christian Church''') is a [[Pentecostalism|Pentecostal]] [[Christianity|Christian]] church affiliated with [[Australian Christian Churches]], the [[Assemblies of God]] in Australia. The church is currently pastored by Jonathan and Rebecca Fontanarosa.

[[File:Jonathan Fontanarosa.jpg|thumbnail|Photo taken on Puerto Plata, Dominican Republic, by Jerry Tostevin on the 1Nation1Day gatherin. ]]

==Establishment==
Edge Church International was originally called Southside Christian Church when it was established in 1994.<ref>{{cite web|url=http://trove.nla.gov.au/work/34782658?selectedversion=NBD42138208|title=Welcome to Edge Church [videorecording]. - Version details - Trove|publisher=}}</ref> Danny Guglielmucci established Edge Church South in May of that year, with just 80 people in a small building at [[O'Halloran Hill, South Australia|O'Halloran Hill]].

During the next 18&nbsp;months the church prospered and grew at an incredible rate.{{Citation needed|date=December 2009}} Filled to capacity, new premises were desperately needed.{{Citation needed|date=December 2009}} The church relocated to what had previously been the Old Reynella Markets in [[Old Reynella, South Australia|Old Reynella]].<ref>http://www.ffp.com.au/catalog/FFP%20Mag%20FINALWEB.pdf</ref> In 2001, over A$640,000 was given by the congregation in one offering enabling the purchase of the property.{{Citation needed|date=December 2009}}

==Church growth==
In 2001 a second congregation called Edge Church City was established. Initially meeting at the Wonderland Ballroom in [[Hawthorn, South Australia|Hawthorn]], the City congregation was forced to move seven times in two years as it experienced rapid growth.{{Citation needed|date=July 2011}} Finally, in 2003 the church took up residence in the former [[Investigator Science & Technology Centre]] building at the [[Royal Adelaide Showgrounds|Adelaide Showgrounds]].

Westside Assembly of God at [[Findon, South Australia|Findon]] became Edge Church West in 2006. Edge Church has an association with [[Tabor College Australia|Tabor College Adelaide]], as Gugliemucci was involved in its foundation. {{Citation needed|date=December 2009}}The Church also established a congregation in the [[United Kingdom]] in January 2008, led by Nick Resce and Jason Gowland. After two years, Resce moved back to Australia and left Edge Church.  In 2014, Gowland, too, left the organisation and moved to another church in the UK.  Edge Church Bristol UK has already seen growth.{{Citation needed|date=June 2011}}

==Community involvement==
Edge Church has grown into a large Pentecostal church that operates through partnership on several projects with many other churches across Adelaide.{{Citation needed|date=December 2009}} For example, the church has worked with over 30 other churches to renovate [[Adelaide Women's and Children's Hospital]] and the [[Adelaide Women's Prison]].{{Citation needed|date=December 2009}}

==Finances==
In its 2007 annual report the church recorded a 12 per-cent rise in revenue to $5.53&nbsp;million. In addition, the 2007 annual report showed a net asset-base of $11.56&nbsp;million, up from $4.01&nbsp;million the previous year, including a $4.5&nbsp;million donation of the land and property of Findon-based Westside Christian Church.<ref name="news.com.au">
{{cite news| url=http://www.news.com.au/adelaidenow/story/0,22606,24265180-5006301,00.html | work=The Advertiser | title=Fame, fortune and the business of religion | first=Michael | last=McGuire | date=29 August 2008}}</ref>

Attendees of the church contribute a [[tithe]] (one tenth) of their income to support the church.<ref name="news.com.au"/>
Church members contributed $3.12&nbsp;million in tithes in the 2007 financial year, up from $2.64&nbsp;million the previous year.<ref name="news.com.au"/>

"General Tithes" for Edge Church International in the 2009/10 financial year decreased by 10% from the previous year and totaled $3.41 million.<ref>
{{cite web
 | url          = http://www.edgechurch.com/filestore/pdf/ECI_annual_report_2010.pdf
 | title        = Edge Church Annual report 2010
 | year         = 2010
 | page         = 22
 | accessdate   = 2012-10-03
}}
</ref>

==Membership==
The Reynella church has a database of 3400 people and the [[Findon, South Australia]] location has a congregation of more than 250.<ref name="news.com.au"/>

==See also==
* [[Youth Alive]]
* [[Australian Christian Churches]]
* [[Influencers Church]]

==References==
{{reflist}}

==External links==
*[http://www.edgechurch.com/ Official website]
{{Use dmy dates|date=July 2011}}

{{Authority control}}
[[Category:Churches in Adelaide]]
[[Category:Pentecostal churches]]
[[Category:Australian Christian Churches]]
[[Category:Religious organizations established in 1994]]