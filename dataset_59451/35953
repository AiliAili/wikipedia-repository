{{good article}}
{{Use dmy dates|date=September 2014}}
{{Infobox airline
| airline          = Air Costa
| image            = Air_Costa_Logo.jpg
| image_size       = 175
| alt              = 
| IATA             = LB<ref name="chavprofile">{{cite web | url=http://www.ch-aviation.com/portal/airline/CVJ | title=Air Costa | work=[[ch-aviation]] | accessdate=4 March 2017}}</ref>
| ICAO             = LEP<ref name="chavprofile" />
| callsign         = LECOSTA<ref>{{cite web | url=https://www.faa.gov/documentLibrary/media/Order/7340.2G_Bsc_dtd_1-5-17.pdf | title=JO 7340.2G Contractions | work=[[Federal Aviation Administration]] | date=5 January 2017 | accessdate=4 March 2017 | page=3-1-61}}</ref>
| founded          = 
| commenced        = 15 October 2013
| ceased           = 28 February 2017 (suspended)<ref name="ch-aviation"/>
| aoc              = 
| bases            = [[Chennai International Airport]]
| frequent_flyer   = 
| lounge           = 
| alliance         = 
| subsidiaries     = 
| fleet_size       = 0
| destinations     = 0
| company_slogan   = ''Happy Flying!''
| parent           = LEPL Group
| headquarters     = [[Vijayawada]], India
| key_people       = <div>
*{{nowrap|Ramesh Lingamaneni {{small|Chairman}}}}
*LVS Rajasekhar {{small|MD}}
*Vivek Choudhary {{small|CEO}}
| revenue          = 
| operating_income = 
| net_income       = 
| profit           = 
| assets           = 
| equity           = 
| num_employees    = 800<ref name="TOI">{{cite news|url=http://timesofindia.indiatimes.com/business/india-business/Air-Costa-to-add-2-E-190s-in-2015/articleshow/46682051.cms|title=Air Costa to add 2 E-190s in 2015|first=Swati|last=Rathor|date=25 March 2015|access-date=14 April 2016|newspaper=[[Times of India]]}}</ref>
| website          = {{url|http://www.aircosta.in/|aircosta.in}}(now defunct)
}}

'''Air Costa''' was an [[India|Indian]] [[regional airline]] headquartered in [[Vijayawada]] and based out of [[Chennai Airport]]. It was owned by Indian business company LEPL Group. The airline commenced operations as a [[regional airline]] in October 2013 using two [[Embraer E-170]] aircraft with the first flight taking off from [[Chennai International Airport|Chennai]] on 16 October 2013. The airline got a permit for pan India operations in October 2016. The airline focused on connectivity between tier II and tier III cities in the country and invested {{INRconvert|600|c}} as of 2015. Air Costa operated 32 daily flights to nine destinations from its focus cities Chennai and [[Vijayawada Airport|Vijayawada]] as of 2015. The airline had a maintenance center in Chennai.<ref name="BS"/> On 28 February 2017, it suspended operations until further notice.<ref name="ch-aviation"/>

==History==
Air Costa was owned by Indian business company LEPL Group.<ref name="BS">{{cite news|title=LEPL to invest Rs 600 cr in Air Costa|url=http://www.business-standard.com/article/companies/lepl-to-invest-rs-600-cr-in-air-costa-113100800667_1.html|access-date=14 April 2016|date=16 October 2013|newspaper=[[Business Standard]]}}</ref> The airline received a No-Objection Certificate (NOC) from the [[Ministry of Civil Aviation (India)|Ministry of Civil Aviation]] in February 2012.<ref name = "TH">{{cite news|title=Air Costa to be launched soon|url=http://www.thehindu.com/todays-paper/tp-national/tp-andhrapradesh/air-costa-to-be-launched-soon/article2921813.ece|access-date=14 April 2016|date=23 February 2012|newspaper=[[The Hindu]]}}</ref> The airline initially planned to start operations with a fleet of [[Bombardier Dash 8|Q400]] aircraft but later announced its decision to acquire Embraer Jets at the [[Paris Air Show]] in June 2013.<ref>{{cite press release|url=http://www.embraer.com.br/en-US/ImprensaEventos/Press-releases/noticias/Pages/Air-Costa--da-India-adquire-tres-EJets.aspx|title=India’s Air Costa Acquires three E-Jets|date=June 2013|access-date=14 April 2016|work=[[Embraer]]}}</ref> Air Costa received its Air Operators’ Permit (AOP) from the [[Directorate General of Civil Aviation (India)|Directorate General of Civil Aviation]] (DGCA) in September 2013.<ref name = "BS2">{{cite news|title= Air Costa gets DGCA permit |url=http://www.business-standard.com/article/companies/air-costa-gets-dgca-permit-113092000335_1.html|date=20 September 2013|access-date=16 October 2013|newspaper=[[Business Standard]]}}</ref>

The airline commenced operations as a [[regional airline]] in October 2013 using two [[Embraer E-170]] aircraft with the first flight taking off from its hub at [[Chennai International Airport|Chennai]] on 16 October 2013.<ref name="deccan">{{cite news|title=Air Costa takes off to connect towns|url=http://archives.deccanchronicle.com/131009/news-businesstech/article/air-costa-takes-connect-towns|access-date=14 April 2016|date=16 October 2013|newspaper=[[Deccan Chronicle]]}}</ref><ref>{{cite news|title=Air Costa takes off from Chennai today|url=http://www.thehindu.com/news/cities/chennai/air-costa-takes-off-from-chennai-today/article5234601.ece|access-date=14 April 2016|date=16 October 2013|newspaper=The Hindu}}</ref><ref>{{cite news|title=First Air Costa flight flagged off by Kiran|url=http://newindianexpress.com/cities/hyderabad/First-Air-Costa-flight-flagged-off-by-Kiran/2013/10/16/article1837590.ece|access-date=14 April 2016|date=16 October 2013|newspaper=[[New Indian Express]]}}</ref><ref>{{cite news|title=Air Costa, India's newest airline, takes flight today|url=http://businesstoday.intoday.in/story/air-costa-india-newest-airline-starts-today-oct-14/1/199566.html|access-date=14 April 2016|date=16 October 2013|newspaper=[[Business Today (business magazine)|Business Today]]}}</ref> The airline focuses on connectivity between tier II and tier III cities in the country and has invested {{INRconvert|600|c}} as of 2015.<ref name="BS"/>

Air Costa ordered 50 Embraer [[E-Jets E2]] aircraft worth {{US$|2.94 billion}} during the [[Singapore Airshow]] on 13 February 2014.<ref name="TOI"/><ref name="embraer1"/> The airline will become the Asian launch customer for the type when it takes the first delivery in 2018.<ref>{{cite news|title=Embraer clinches  bln India deal with Air Costa|url=http://in.reuters.com/article/2014/02/13/airshow-singapore-embraer-idINDEEA1C04X20140213|date=14 February 2014|access-date=30 May 2015|work=[[Reuters]]}}</ref>

In September 2015, Air Costa, which held a regional airline's license, applied to the aviation regulator DGCA for its license to be upgraded to that of a national airline.<ref>{{cite news|url=http://economictimes.indiatimes.com/industry/transportation/airlines-/-aviation/regional-airlines-now-pitch-for-a-national-licence/articleshow/48863548.cms|title=Regional airlines now pitch for a national licence|newspaper=[[The Economic Times]]|date=8 September 2015|access-date=8 September 2015}}</ref> Air Costa received a no-objection certificate from the civil aviation ministry for pan-India operations on 19 December 2015.<ref>{{cite news|url=http://www.business-standard.com/article/companies/air-costa-gets-noc-for-pan-india-operations-115121800129_1.html|title=Air Costa gets aviation ministry approval for pan-India operations|first=TE|last=Narasimhan|newspaper=[[Business Standard]]|date=25 December 2015|access-date=14 April 2016}}</ref><ref name="mint">{{cite news|title=Air Costa secures NoC for pan-India flights|url=http://www.livemint.com/Companies/cIhRPX4dOdQmmhssV2S9bI/Air-Costa-secures-NoC-for-panIndia-flights.html|first=PR|last=Sanjai|access-date=14 April 2016|date=19 December 2015|newspaper=[[Live Mint]]}}</ref><ref>{{Cite news|url=http://www.business-standard.com/article/news-ians/india-s-domestic-passenger-demand-up-25-percent-iata-116040700595_1.html|title=India's domestic passenger demand up 25 percent: IATA|newspaper=[[Business Standard]]|date=6 April 2014|access-date=14 April 2016}}</ref><ref>{{cite news|url=http://economictimes.indiatimes.com/articleshow/50242782.cms?platform=hootsuite&utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst|title=Air Costa receives a no-objection certificate|newspaper=[[The Economic Times]]|date=20 December 2015|access-date=8 September 2015}}</ref> Two Embraer E-190s were added by September 2015.<ref>{{cite news|url=http://timesofindia.indiatimes.com/business/india-business/Air-Costa-to-add-2-E-190s-in-2015/articleshow/46682051.cms|title=Air Costa to add 2 E-190s in 2015|newspaper=[[The Times of India]]|access-date=14 April 2016|date=6 April 2015}}</ref> Air Costa added one more E-190 in December 2015 and plans to add three to five E-190 aircraft in its fleet in 2016.<ref>{{cite news|url=http://economictimes.indiatimes.com/articleshow/47143872.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst|title=Air Costa plans to raise funds through stake sale; to add three more Embraer aircraft|newspaper=[[The Economic Times]]|access-date=14 April 2016|date=5 May 2015}}</ref> The airline had a 0.8% market share in the Indian domestic airline market as of February 2016.

On 28 February 2017, Air Costa suspended operations until further notice stating financial difficulties regarding the lease of aircraft.<ref name="ch-aviation">{{cite web|url=http://ch-aviation.com/portal/news/53777-indias-air-costa-suspends-operations|work=ch-aviation.com|title=India's Air Costa suspends operations|date=28 February 2017}}</ref> [[GECAS]] ceased two of the airlines leased aircraft while a third one had already been returned to the lessor.<ref>{{cite news|url=http://www.aerotelegraph.com/air-costa-grounding-statt-ausbau-auf-100-flieger|work=aerotelegraph.com|title=Grounding instead of expansion to 100 aircraft|date=3 March 2017}}</ref>

==Destinations==
[[File:Air_Costa_VT-LNR_at_Bengaluru,_Aug_2015.jpg|thumb|A former Air Costa [[Embraer 170]]]]

Air Costa was flying to the following destinations in India at the time it suspended operations:<ref name="ch-aviation"/>

{|class="wikitable sortable"
|-
! State
! City
! Airport
! Notes
|-
|Andhra Pradesh||Tirupati||[[Tirupati Airport]]||{{Terminated}}
|-
|Andhra Pradesh||Vijayawada||[[Vijayawada Airport]]||{{Terminated}}
|-
|Andhra Pradesh||Visakhapatnam||[[Visakhapatnam Airport]]||{{Terminated}}
|-
|Gujarat||Ahmedabad||[[Sardar Vallabhbhai Patel International Airport]]||{{Terminated}}
|-
|Karnataka||Bangalore||[[Kempegowda International Airport]]||{{Terminated}}
|-
|Rajasthan||Jaipur||[[Jaipur International Airport]]||{{Terminated}}
|-
|Tamil Nadu||Chennai||[[Chennai International Airport]]||{{Terminated}}
|-
|Telangana||Hyderabad||[[Rajiv Gandhi International Airport]]||{{Terminated}}
|-
|}

==Fleet==
[[File:Air costa.JPG|thumb|Air Costa [[E190|Embraer 190]]]]
As of March 2017, the Air Costa fleet consists of the following aircraft:<ref name="planespotters.net">[https://www.planespotters.net/airline/Air-Costa planespotters.net - Air Costa Fleet Details and History] retrieved 1 March 2017</ref>

{| class="wikitable" border="1" cellpadding="1" style="border-collapse:collapse;text-align:center"
|+ Air Costa Fleet
|- 
! rowspan="2" | Aircraft
! rowspan="2" | In Service
! rowspan="2" | Orders
! colspan="3" | Passengers
! rowspan="2" | Notes
|- 
!<abbr title="Première">J</abbr>
!<abbr title="Economy">Y</abbr>
!Total
|-
|[[E190|Embraer 190]]
|0<ref>{{cite web|title=DGCA deregisters 2 aircraft of Air Costa|url=http://timesofindia.indiatimes.com/city/vijayawada/dgca-deregisters-2-aircraft-of-air-costa/articleshow/57808396.cms|publisher=Times of India|accessdate=24 March 2017}}</ref>
|1<ref name="planespotters.net"/>
|0
|112
|112
|Order cancelled by [[GECAS]]<ref name="planespotters.net"/>
|-
|[[Embraer E-Jet E2 family|Embraer 190-E2]]
|—
|25
|6
|92
|98
|Orders cancelled by [[Embraer]]; Supposed to be the Asian launch customer<ref name="embraer1">{{cite press release|title=India’s Air Costa places a firm order for 50 E-Jets E2s|url=http://www.embraer.com.br/en-US/ImprensaEventos/Press-releases/noticias/Pages/Air-Costa,-da-India,-faz-pedido-firme-para-50-E-Jets-E2.aspx|access-date=14 April 2016|work=[[Embraer]]}}</ref>
|-
|[[Embraer E-Jet E2 family|Embraer 195-E2]]
|—
|25
|12
|106
|118
|Orders cancelled by [[Embraer]]; Supposed to be the Asian launch customer<ref name="embraer1"/>
|-
|-
!Total 
!0<ref>{{cite web|title=DGCA deregisters 2 aircraft of Air Costa|url=http://timesofindia.indiatimes.com/city/vijayawada/dgca-deregisters-2-aircraft-of-air-costa/articleshow/57808396.cms|newspaper=Times of India|date=24 March 2017}}</ref>
!51
!colspan="4"|
|}

==References==
{{reflist|30em}}

==External links==
{{commonscat inline}}
*[http://www.aircosta.in Official website]

{{Portalbar|India|Companies|Aviation}}
{{Airlines of India}}

[[Category:Airlines of India]]
[[Category:Airlines established in 2013]]
[[Category:Low-cost carriers]]