{{Italic title}}
[[File:Journal of Computer-Mediated Communication.jpg|thumb|right|alt=Journal of Computer-Mediated Communication]]
The '''''Journal of Computer-Mediated Communication''''' JCMC is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers the interdisciplinary field of [[computer-mediated communication]]. It was established in 1994 and is published by [[Wiley-Blackwell]] on behalf of the [[International Communication Association]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] was  1.958, ranking it 3rd out of 67 journals in the category "Communication" and 13th out of 76 journals in the category "Information Science & Library Science". By 2014 the ISI Journal Citation Reports © Ranking reported JCMC to be 2nd of 76 in the field of Communication, and 4th of 85 in Information Science & Library Science, with an impact factor of 3.117

== Editors ==
The following persons have been [[editor-in-chief]] of the journal:
* 2014-present: Shyam Sundar ([[Pennsylvania State University]])
* 2011–2013: Maria Bakardjieva ([[University of Calgary]])
* 2008-2010: Kevin Wright ([[University of Oklahoma]])
* 2005-2007: [[Susan Herring]] ([[Indiana University]])
* 1994-2004: Founding Editors: Margaret McLaughlin ([[University of Southern California]]) and [[Sheizaf Rafaeli]] ([[Hebrew University of Jerusalem]] and [[University of Haifa]]) 

==External links==
* {{Official website|http://jcmc.indiana.edu/}}
* [http://www.wiley.com/bw/journal.asp?ref=1083-6101 Journal page at publisher's website]
* [http://www.icahdq.org/pubs/jnlcomcomm.asp Journal page at association's website]
* {{ISSN|1083-6101}}

[[Category:Computer science journals]]
[[Category:Publications established in 1995]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Communication journals]]
[[Category:Educational technology journals]]


{{compu-journal-stub}}