{{for|the Ohio politician|Ralph L. Humphrey}}
{{like resume|date=June 2016}}
{{Infobox artist
| name          = Ralph Humphrey
| image         = 
| image_size    = 
| alt           = 
| caption       = 
| birth_name    = Ralph Humphrey
| birth_date    = {{Birth date |1932|04|14}}
| birth_place   = [[Youngstown, Ohio]], U.S.
| death_date    = {{Death date and age|1990|07|14|1932|04|14}}
| death_place   = [[New York, New York]], U.S.
| nationality   = American
| spouse        = 
| field         = [[Painting]]
| training      = [[Youngstown State University]]
| movement      = [[Abstract expressionism]]<br>[[Minimalism]]
}}

'''Ralph Humphrey''' (April 14, 1932 – July 14, 1990) was an American abstract painter whose work has been linked to both [[Abstract Expressionism]] and [[Minimalism]].<ref name="NYTimes">{{cite news|last1=Smith|first1=Roberta|title=Ralph Humphrey, An Abstract Painter And a Teacher, 58|url=https://www.nytimes.com/1990/07/17/obituaries/ralph-humphrey-an-abstract-painter-and-a-teacher-58.html|accessdate=29 April 2016|publisher=New York Times|date=July 17, 1990}}</ref><ref name="Artforum 1977">{{cite journal|last1=Wilson|first1=William S.|title=Ralph Humphrey: An Apology for Painting|journal=Artforum|date=1977|volume=16|issue=3|pages=54–59}}</ref> He was active in the New York art scene in the 1960s and '70s. His paintings are best summarized as an exploration of space through color and structure. He lived and worked in New York, NY.

He is not to be confused with the percussionist Ralph Humphrey, best known for being the drummer of [[The Mothers Of Invention]] from 1973 until 1974. 

The Estate of Ralph Humphrey is represented exclusively by Garth Greenan Gallery, New York.<ref name="Garth Greenan Gallery">{{cite web|title=Ralph Humphrey|url=http://www.garthgreenan.com/artists/ralph-humphrey|website=Garth Greenan Gallery|publisher=Garth Greenan Gallery|accessdate=29 April 2016}}</ref>

==Biography==
Ralph Humphrey studied at [[Youngstown State University]].<ref name=NYTimes /> He moved to New York in 1957 and immediately became a part of the art scene that was known, at the time, for [[Abstract Expressionism]].<ref name=NYTimes /> He met artists such as [[Mark Rothko]], [[Theodoros Stamos]], [[Frank Stella]], [[Robert Ryman]], and [[Ellsworth Kelly]], who would end up having a large influence on his work.<ref name="Art in America 1984">{{cite journal|last1=Baker|first1=Kenneth|title=Material Feelings|journal=Art in America|date=1984|volume=72|issue=9|pages=162–167}}</ref> Humphrey was a prominent member of the generation of artists who laid the groundwork for American art in the 1970s and 60s.<ref name=NYTimes /> From 1966 until his death in 1990, he taught painting in the graduate department at [[Hunter College]].

==Artistic style==
Humphrey’s artistic style went through several phases and developments, which can be roughly outlined in the following way: monochromes from 1957–60; frame paintings 1961–65; shaped canvases 1967–70; constructed paintings 1971–1990.<ref name="interview 1982">{{cite journal|last1=Baker|first1=Amy|title=Painterly Edge|journal=Artforum|date=1982|volume=20|issue=8|pages=38–43}}</ref> Throughout these phases, Humphrey kept a keen eye on color, light, and space while he moved between abstraction and representation. As Kenneth Baker explains in Art in America in 1984, “Each of his works defines an ideal viewing distance that can be discovered only by patient observation of the focus of the details, the resolution of the image and the proper relationship between body and object. Finding the apt distance from which to contemplate Humphrey’s new paints is thus not something you do discursively: it is an exercise in feeling your way silently towards a correct spatial interval.” <ref name="Art in America 1984" />

===1957–1960===
Reviewing Humphrey’s show at [[Tibor de Nagy]] in 1960, [[Donald Judd]] said, of his monochromes, “They are large, subtle and single-colored. This is Purism of a sort, in which generality does not contain variables but excludes them, in which the basic diagram or color, the only continuity, is exposed, here the essence of a confused sequence of perceptions.”<ref name="judd 1960">{{cite journal|last1=Judd|first1=Donald|title=In the Galleries: Ralph Humphrey|journal=Arts Magazine|date=1960|volume=34|issue=6|page=54}}</ref> Donald Judd also likened these canvases tot the work of [[Kazimir Malevich]], [[Piet Mondrian]], and [[Josef Albers]].

===1961–1965===
Neil A. Levine wrote in 1965 about Humphrey’s solo exhibition at [[Green Gallery]], where he showed some of his frame paintings. Levine said, “His new work is serious and demanding. All the paintings are variations on one theme. The theme is, simply stated, an expansive, lightly brushed, large grey field…surrounded by a painted framing edge…”<ref name="NAL 1965">{{cite journal|last1=Levine|first1=Neil A.|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1965|volume=64|issue=4|page=16}}</ref> Here, Levine, too, references [[Josef Albers|Albers]], as well as TV screens, unfilled billboards, and [[Mark Rothko|Rothko]].<ref name="NAL 1965" />

===1967–1970===
Robert Pincus-Whitten reviewed Humphrey’s 1969 show at [[Bykert Gallery]], where his shaped canvases were hung. Pincus-Whitten explains how Humphrey created “a luminous cosmos of fragile exhalations, painted on large squares or horizontal rectangles, softly turned at the corner and curved back into the stretcher.”<ref name="artforum 1969">{{cite journal|last1=Pincus-Whitten|first1=Robert|title=New York: Ralph Humphrey|journal=Artforum|date=1969|volume=7|issue=8|page=69}}</ref> These canvases are noteworthy, too, for their use of day-glow colors. At this time, his work becomes increasingly more atmospheric than his previous efforts; multi-colored wavy lines and sprayed colors replace solid geometric fields of single colors.

===1971–1990===
The last definable phase of his artistic style approaches representation at times, sometimes calling to mind an open window. These constructed paintings also border on sculpture, often coming ten inches out from the wall, directly confronting the viewer in real space. The paint, too, is considerably built up, giving the surface of the paintings considerable texture that was not previously seen in his work. Ellen Schwartz writes in 1977 about his show at John Weber, where his constructed paintings were still abstract: “Humphrey’s latest works, meditative rather than communicative, require the suspension of conscious efforts to grasp them before they will yield their secrets, which lay within ourselves all the while. The rich blue variegated surfaces are like blotters onto which we pour our own fantasies.”<ref name="schwartz 1977">{{cite journal|last1=Schwartz|first1=Ellen|title=New York Reviews: Ralph Humphrey|journal=Art News|date=1977|volume=76|issue=4|page=126}}</ref> Deborah Phillips, writing about his Willard Gallery show in 1982, explains how his constructed paintings are natural extensions of the earlier frame paintings: “Frames-within-frames have long provided the structural basis for Humphrey’s colorful designs; he has simply made his window allusion literal.” She explains, too, that these paintings are a step forward: “The shift does, however, bring greater variety and complexity to the artist’s constructions. There is a more explicit sense of space, of indoors and outdoors.”<ref name="deborah 1982">{{cite journal|last1=Phillips|first1=Deborah C.|title=New York Reviews: Ralph Humphrey|journal=Art News|date=1982|volume=81|issue=7|page=161}}</ref> Beyond content, we see Humphrey using a brighter color palette and inserting vaguely figurative, whimsical patterns onto the surface.<ref name="Art in America 1984" /> Yet, by the mid 1980s, the paintings return to a more ambiguous, abstract state.<ref name="Art in America 1984" />

==Exhibitions==
Since his first solo exhibition at the [[Tibor de Nagy Gallery]] in New York City in 1959, Humphrey’s work has been the subject of 40 solo shows. During his lifetime, he had been represented by [[Green Gallery]], [[Bykert Gallery]], Andre Emmerich Gallery, [[Willard Gallery]], and John Weber Gallery.<ref name=NYTimes />

Solo exhibitions have continued to be mounted since his death in 1990, including Ralph Humphrey: Frame Paintings, 1964 to 1965 at [[Mary Boone]] Gallery, New York City, September 8–October 6, 1990 and Ralph Humphrey: Conveyance at Gary Snyder Gallery, April 2 – May 16, 2015.<ref name="Mary Boone Cat">{{cite book|last1=Mary Boone Gallery|title=Ralph Humphrey, Frame Paintings, 1964 to 1965|date=1990|publisher=Mary Boone Gallery|location=New York|isbn=9780941863155}}</ref><ref name="Garth Cat">{{cite book|last1=Greenan|first1=Garth|title=Ralph Humphrey|date=2012|publisher=Gary Snyder Gallery|location=New York|isbn=9780982974766}}</ref> Other exhibitions have been held elsewhere in New York, San Francisco, Los Angeles, and Boston.

Humphrey's paintings have also been in group shows such as Systemic Painting at the [[Solomon R. Guggenheim Museum]], New York, 1966, The Structure of Color at the [[Whitney Museum of American Art]], New York, 1971, the 1979 Biennial at the [[Whitney Museum]], and High Times, Hard Times: New York Painting, 1967-1975 at the [[Weatherspoon Art Museum]], 2006.<ref name=Systemic>{{cite book|last1=Solomon R. Guggenheim Museum|title=Systemic Painting|date=1966|publisher=Solomon R. Guggenheim Foundation|location=New York}}</ref><ref name=whit1979>{{cite book|last1=Tucker|first1=Marcia|title=The Structure of Color|date=1971|publisher=Whitney Museum of American Art|location=New York}}</ref><ref>{{cite book|last1=Whitney Museum of American Art|title=1979 Biennial Exhibition|date=1979|publisher=Whitney Museum of American Art|location=New York|isbn=9780874270129}}</ref><ref name=hightimes>{{cite book|last1=Siegel|first1=Katy|title=High Times, Hard Times: NEw York Painting, 1967-1975|date=2006|publisher=Independent Curators International|location=New York|isbn=9781933045399}}</ref>
{{like resume|section|date=June 2016}}

===Solo exhibitions===
1959

*Ralph Humphrey, [[Tibor de Nagy Gallery]], New York, February 3–21<ref>{{cite journal|last1=Campbell|first1=Lawrence|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1959|volume=57|issue=10|pages=17–18}}</ref>
 
1960

*Ralph Humphrey, [[Tibor de Nagy Gallery]], New York, February 2–21<ref name="judd 1960" /><ref>{{cite journal|last1=Campbell|first1=Lawrence|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1960|volume=58|issue=10|pages=14–15}}</ref>
 
1961

*Ralph Humphrey: Recent Paintings, Mayer Gallery, New York, March 14 – April 1<ref>{{cite journal|last1=Sandler|first1=Irving H.|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1961|volume=60|issue=3|pages=15–16}}</ref>
 
1965

*Ralph Humphrey, [[Green Gallery]], New York, May 5–29<ref name="NAL 1965" /><ref>{{cite journal|last1=Goldin|first1=Amy|title=In the Galleries: Ralph Humphrey|journal=Arts Magazine|date=1965|volume=39|issue=10|page=66}}</ref>
 
1967

*Ralph Humphrey, [[Bykert Gallery]], New York, January 10 – February 24<ref>{{cite journal|last1=Benedikt|first1=Michael|title=New York: Humphries|journal=Art International|date=1967|volume=11|issue=4|page=64}}</ref><ref>{{cite journal|last1=Kramer|first1=Hilton|title=Ralph Humphrey|journal=New York Times|date=January 21, 1967|page=27|url=http://timesmachine.nytimes.com/timesmachine/1967/01/21/82993705.html?pageNumber=23|accessdate=4 May 2016}}</ref><ref>{{cite journal|last1=Waldman|first1=Diane|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1967|volume=65|issue=10|page=15}}</ref>
 
1968

*Ralph Humphrey, [[Bykert Gallery]], New York, February 3–29<ref>{{cite journal|last1=Mellow|first1=James R.|title=New York Letter: Ralph Humphrey|journal=Art International|date=1968|volume=12|issue=4|pages=63–67}}</ref><ref>{{cite journal|last1=Battock|first1=Gregory|title=In the Galleries: Ralph Humphrey|journal=Arts Magazine|date=1968|volume=42|issue=4|page=62}}</ref><ref>{{cite journal|last1=Perreault|first1=John|title=Art: Too Much of the Same|journal=Village Voice|date=1968|volume=12|issue=19|page=18}}</ref><ref>{{cite journal|last1=Burton|first1=Scott|title=A Different Stripe|journal=Art News|date=1968|volume=66|issue=10|pages=36–37, 53–56}}</ref>
 
1969

*Ralph Humphrey, [[Bykert Gallery]], New York, February 1–27 Galerie Alfred Schmela, Düsseldorf<ref name="artforum 1969" /><ref>{{cite journal|last1=Kurtz|first1=Stephen A.|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1969|volume=68|issue=1|page=20}}</ref><ref>{{cite journal|last1=Glueck|first1=Grace|title=Sexy Phones, Shoe Trees and Faucets|journal=New York Times|date=February 15, 1969|url=http://timesmachine.nytimes.com/timesmachine/1969/02/15/90048836.html?pageNumber=25|accessdate=4 May 2016}}</ref><ref>{{cite journal|last1=Schjeldahl|first1=Peter|title=New York Letter|journal=Art International|date=1969|volume=13|issue=4|pages=62–67}}</ref><ref>{{cite journal|last1=Simon|first1=Rita|title=In the Galleries: Ralph Humphrey|journal=Arts Magazine|date=1969|volume=43|issue=5|page=58}}</ref>
 
1970

*Ralph Humphrey, [[Bykert Gallery]], New York, April 4–25<ref>{{cite journal|last1=Rosenstein|first1=Harris|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1970|volume=69|issue=3|page=67}}</ref><ref>{{cite journal|last1=Ratcliff|first1=Carter|title=New York Letter|journal=Art International|date=1970|volume=14|issue=6|pages=132–144}}</ref>
 
1971

*Ralph Humphrey, André Emmerich Gallery, New York, March 20 – April 8<ref>{{cite journal|last1=Baker|first1=Kenneth|title=New York: Ralph Humphrey|journal=Artforum|date=1971|volume=9|issue=9|page=74}}</ref><ref>{{cite journal|last1=Ratcliff|first1=Carter|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1971|volume=70|issue=3|page=57}}</ref>
 
1972

*Ralph Humphrey, [[Bykert Gallery]], New York, May 2–23<ref>{{cite journal|last1=Matthias|first1=Rosemary|title=In the Galleries: Ralph Humphrey|journal=Arts Magazines|date=1972|volume=46|issue=8|page=59}}</ref><ref>{{cite journal|last1=Rosenstein|first1=Harris|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1972|volume=71|issue=4|page=53}}</ref>
 
1973

*Ralph Humphrey, [[Bykert Gallery]], New York, May 12 – June 2<ref>{{cite journal|last1=Mellow|first1=James|title=A Summer Show|journal=New York Times|date=May 19, 1973|url=http://timesmachine.nytimes.com/timesmachine/1973/05/19/79856377.html|accessdate=4 May 2016}}</ref><ref>{{cite journal|last1=Mayer|first1=Rosemary|title=New York: Ralph Humphrey|journal=Arts Magazine|date=1973|volume=47|issue=5|pages=71}}</ref>
*Ralph Humphrey: Survey of Paintings, Texas Gallery, Houston, May 15 – June 9
 
1974

*Ralph Humphrey, [[Bykert Gallery]], New York, April 20 – May 15<ref>{{cite journal|last1=Dreiss|first1=Joseph|title=Arts Reviews: Ralph Humphrey|journal=Arts Magazine|date=1974|volume=49|issue=1|page=57}}</ref><ref>{{cite journal|last1=Frank|first1=Peter|title=Review of Exhibitions: Ralph Humphrey at Bykert Uptown|journal=Art in America|date=1974|volume=62|issue=5|pages=107–108}}</ref>
*Ralph Humphrey: Paintings, Daniel Weinberg Gallery, San Francisco, November–December
 
1975

*Ralph Humphrey: Paintings, 1974, [[Bykert Gallery]], New York, February 4–26<ref name="Zucker">{{cite journal|last1=Zucker|first1=Barbara|title=Reviews and Previews: Ralph Humphrey|journal=Art News|date=1975|volume=74|issue=4|page=98}}</ref>
*Ralph Humphrey: Paintings, 1958–1966, Bykert/ Downtown, New York, February 4–26<ref name="Zucker" />
 
1976

*Ralph Humphrey, John Weber Gallery, New York, January 31 – February 25<ref>{{cite journal|last1=Wilson|first1=William S.|title=Ralph Humphrey|journal=Arts Magazine|date=1976|volume=50|issue=6|page=5}}</ref><ref>{{cite journal|last1=Derfner|first1=Phyllis|title=Review of Exhibitions: Ralph Humphrey at John Weber|journal=Art in America|date=1976|volume=64|issue=3|page=106}}</ref>
 
1976–1977

*Ralph Humphrey: Recent Paintings, Daniel Weinberg Gallery, San Francisco, December 16, 1976 – January 22, 1977
 
1977

*Ralph Humphrey, John Weber Gallery, New York, February 9–26<ref name="schwartz 1977" />
 
1980

*Ralph Humphrey, [[Willard Gallery]], New York, April 5 – May 7<ref>{{cite journal|last1=Zimmer|first1=William|title=Surfacing: Ralph Humphrey|journal=Soho Weekly News|date=1980|volume=7|issue=30|page=60}}</ref><ref>{{cite journal|last1=Frank|first1=Elizabeth|title=Review of Exhibitions: Ralph Humphrey at Willard|journal=Art in America|date=1980|volume=68|issue=6|pages=157–158}}</ref>
 
1982

*Ralph Humphrey, [[Willard Gallery]], New York, April 3–May 8<ref name="deborah 1982" /><ref>{{cite journal|last1=Smith|first1=Roberta|title=Tempus Fidget|journal=Village Voice|date=1982|volume=27|issue=16|page=89}}</ref><ref>{{cite journal|last1=Raynor|first1=Vivien|title=Ralph Humphrey|journal=New York Times|date=April 16, 1982|url=https://www.nytimes.com/1982/04/16/arts/art-from-jay-coogan-sculptural-personalities.html|accessdate=5 May 2016}}</ref>
*Ralph Humphrey: Paintings, 1975–1982, Daniel Weinberg Gallery, Los Angeles, October 6–30
 
1983

*Ralph Humphrey: Selected Paintings, Daniel Weinberg Gallery, Los Angeles, May 14 – June 11
 
1984

*Delahunty Gallery, Dallas
*Ralph Humphrey, Willard Gallery, New York, April 7 – May 12<ref>{{cite journal|last1=Westfall|first1=Stephen|title=Ralph Humphrey|journal=Arts Magazine|date=1984|volume=59|issue=1|page=40}}</ref>
 
1985

*Ralph Humphrey: Recent Paintings, Daniel Weinberg Gallery, Los Angeles, October 16 – November 2
 
1987

*Ralph Humphrey, Jay Gorney Modern Art, New York, January–February<ref>{{cite journal|last1=Larson|first1=Kay|title=Guerilla Tactics|journal=New York Magazine|date=1987|volume=20|issue=5|pages=54–55}}</ref><ref>{{cite journal|last1=Smith|first1=Roberta|title=Art: Quieter Times for East Village's Galleries|journal=New York Times|date=February 6, 1987|url=https://www.nytimes.com/1987/02/06/arts/art-quieter-times-for-east-village-s-galleries.html|accessdate=5 May 2016}}</ref>
 
1990

*Ralph Humphrey: 1990, [[Mary Boone]] Gallery, New York, March 3–31<ref>{{cite journal|last1=Ostrow|first1=Saul|title=Ralph Humphrey|journal=Arts Magazine|date=1990|volume=64|issue=10|page=78}}</ref>
*Ralph Humphrey: Frame Paintings, 1964 to 1965, [[Mary Boone]] Gallery, New York, September 8–October 6<ref name="Mary Boone Cat" /><ref>{{cite journal|last1=Smith|first1=Roberta|title=Review/Art; For Judy Pfaff, Moderation at Last|journal=New York Times|date=September 28, 1990|url=https://www.nytimes.com/1990/09/28/arts/review-art-for-judy-pfaff-moderation-at-last.html|accessdate=5 May 2016}}</ref>
*Ralph Humphrey: A Retrospective View, 1954–1990, Daniel Weinberg Gallery, Los Angeles, November 8– December 5
 
1991

*Ralph Humphrey: The Late Paintings on Paper, Bertha and Karl Leubsdorf Art Gallery, [[Hunter College]], City University of New York, September 19 – October 26
*Ralph Humphrey: Paintings, 1975–1985, John Berggruen Gallery, San Francisco, October–November
 
1996

*Ralph Humphrey: Selected Paintings, Daniel Weinberg Gallery, San Francisco, August 17 – October 17
 
1998

*Ralph Humphrey, Danese Gallery, New York, January 16 – February 14
 
2000

*Ralph Humphrey: Early Paintings, 1957–1967, Daniel Weinberg Gallery, Los Angeles, November 1 – December 9
 
2001

*Ralph Humphrey: Later Paintings, 1975–1982, Daniel Weinberg Gallery, Los Angeles, April 5 – May 26
 
2008

*Ralph Humphrey: Selected Works from the Estate, Nielsen Gallery, Boston, May 17 – June 14
*Ralph Humphrey: Selected Paintings, 1957–1980, Daniel Weinberg Gallery, Los Angeles, May 31 – June 28<ref>{{cite web|title=Ralph Humphrey: Selected Paintings, 1957-1980|url=http://www.danielweinberggallery.com/exhibitions/1151|website=Daniel Weinberg Gallery|accessdate=3 May 2016}}</ref>
 
2012

*Ralph Humphrey, Gary Snyder Gallery, New York, September 13 – October 27<ref name="Garth Cat" /><ref>{{cite web|title=Ralph Humphrey|url=http://www.garthgreenan.com/exhibitions/2012-09-13_ralph-humphrey|website=Garth Greenan Gallery|accessdate=3 May 2016}}</ref>
 
2015

*Ralph Humphrey: Conveyance, Garth Greenan Gallery, New York, April 2–May 16<ref>{{cite web|title=Ralph Humphrey: Conveyance|url=http://www.garthgreenan.com/exhibitions/2015-04-02_ralph-humphrey-conveyance|website=Garth Greenan Gallery|accessdate=3 May 2016}}</ref>

===Group Exhibitions===
1961
*American Abstract Expressionists and Imagists, [[Solomon R. Guggenheim Museum]], New York, October–December<ref>{{cite book|last1=Solomon R. Guggenheim Museum|title=American Abstract Expressionists and Imagists|date=1961|publisher=Solomon R. Guggenheim Museum|location=New York}}</ref>
 
1966
*Systemic Painting, [[Solomon R. Guggenheim Museum]], New York, September–November<ref name=Systemic />
 
1967
*Selected N.Y.C. Artists 1967, [[Ithaca College Museum of Art]], Ithaca, New York, April 4 – May 27<ref>{{cite book|last1=Ithaca College Museum of Art|title=Selected N.Y.C. Artists 1967|date=1967|publisher=Ithaca College|location=Ithaca}}</ref>
*Focus on Light, [[New Jersey State Museum]], Trenton, May 20 – September 10<ref>{{cite book|last1=Bellamy|first1=Richard|title=Focus on Light|date=1967|publisher=New Jersey State Museum Cultural Center|location=Trenton}}</ref>
*Highlights of the 1966–1967 Art Season, [[Aldrich Museum of Contemporary Art]], Ridgefield, Connecticut, June 18 – September 4<ref>{{cite book|last1=Aldrich Museum of Contemporary Art|title=Highlights of the 1966-67 Art Season|date=1967|publisher=Aldrich Museum of Contemporary Art|location=Ridgefield, CT}}</ref>
*A Romantic Minimalism, [[Institute of Contemporary Art, Philadelphia]] September 13 – October 11<ref>{{cite book|last1=Prokopoff|first1=Stephen|title=A Romantic Minimalism|date=1967|publisher=University of Pennsylvania|location=Philadelphia}}</ref>
 
1968
*[[Bykert Gallery]], New York
*The Art of the Real: USA, 1948–1968, [[Museum of Modern Art]], New York, July 3 – September 8<ref>{{cite book|last1=Goossen|first1=E.C.|title=The Art of the Real; USA, 1948-1968|date=1968|publisher=Museum of Modern Art|location=New York}}</ref>
 
1968–1969
*The Pure and Clear: American Innovations, [[Philadelphia Museum of Art]], November 13, 1968 – January 21, 1969<ref>{{cite book|last1=Philadelphia Museum of Art|title=The Pure and Clear: American Innovations|date=1968|publisher=Philadelphia Museum of Art|location=Philadelphia}}</ref>
 
1969
*American Painting: The 1960s, [[Georgia Museum of Art]], University of Georgia, Athens, September 22– November 8<ref>{{cite book|last1=Green|first1=Samuel Adams|title=American Painting: The 1960s|date=1969|publisher=Georgia Museum of Art|location=Athens, GA}}</ref>
*Current Minimal Painting, [[Vassar College Art Gallery]], Poughkeepsie, New York
 
1969–1970
*1969 Annual Exhibition: Contemporary American Painting, [[Whitney Museum of American Art]], New York, December 16, 1969 – February 1, 1970<ref>{{cite book|last1=Whitney Museum of American Art|title=1969 Annual Exhibition: Contemporary American Painting|date=1969|publisher=Whitney Museum of American Art|location=New York}}</ref>
 
1970–1971
*Color and Field, 1890–1970, [[Albright-Knox Art Gallery]], Buffalo, September 15 – November 1, 1970; Dayton Art Institute, Ohio, November 20, 1970 – January 10, 1971; Cleveland Museum of Art, February 4–March 28, 1971<ref>{{cite book|last1=Albright-Knox Art Gallery|title=Color and Field: 1890-1970|date=1970|publisher=Albright Knox Art Gallery|location=Buffalo, NY}}</ref>
 
1971
*The Structure of Color, [[Whitney Museum of American Art]], New York, February 25 – April 18
*Spray, [[Santa Barbara Museum of Art]], California, April 24–May 30<ref>{{cite book|last1=Santa Barbara Museum of Art|title=Spray|date=1971|publisher=Santa Barbara Museum of Art|location=Santa Barbara}}</ref>
*[[Bykert Gallery]], New York
*Art of the Decade, 1960–1970: Paintings from the Collections of Greater Detroit, University Art Gallery, Oakland University, Rochester, Michigan, November 14–December 17<ref>{{cite book|last1=Oakland University Art Gallery|title=Art of the Decade, 1960-1970|date=1971|publisher=Oakland University|location=Detroit}}</ref>
 
1972
*Painting and Sculpture Today, [[Indianapolis Museum of Art]], April 26 – June 4
*Current American Abstract Painting, [[Vassar College Art Gallery]], Poughkeepsie, New York
*Dealers’ Choice, [[La Jolla Museum of Contemporary Art]], California, July 15–September 27
 
1973
*Drawings, [[Bykert Gallery]], New York, January 6–24
*Gallery Toselli, Milan
 
1974
*New Painting: Stressing Surface, Katonah Gallery, Katonah, New York, May 4 – June 23
*Painting and Sculpture Today, [[Indianapolis Museum of Art]], May 22 – July 14; Taft Museum of Art, Cincinnati, September 12–October 24
*Ten Painters in New York, Michael Walls Gallery, New York, June 15 – July 6
*Seventy-First American Exhibition, [[Art Institute of Chicago]], June 15 – August 11
 
1975
*22 Artists, Susan Caldwell Gallery, New York, January 4–25
*Fourteen Abstract Painters, Frederick S. Wight Art Gallery, University of California, Los Angeles, March 25 – May 25
*Fourteen Artists, [[Baltimore Museum of Art]], April 15 – June 1
*A Group Show Selected by Klaus Kertess, Texas Gallery, Houston, September 15 – October 11
*Douglas Drake Gallery, Kansas City, Missouri
 
1975–1976
*Painting, Drawing, and Sculpture of the ’60s and ’70s from the Dorothy and Herbert Vogel Collection, [[Institute of Contemporary Art, Philadelphia]], October 7–November 18, 1975; Contemporary Arts Center, Cincinnati, December 17, 1975 – February 15, 1976
 
1976
*Ideas on Paper: 1970–1976, [[Renaissance Society]] at the [[University of Chicago]], May 2 – June 6
*Daniel Weinberg Gallery, San Francisco
 
1977
*Paintings on Paper, [[Drawing Center]], New York, January 15–26<ref>{{cite journal|last1=Russell|first1=John|title=Art: Warhol's Hammer and Sickle|journal=New York Times|date=January 21, 1977|url=http://timesmachine.nytimes.com/timesmachine/1977/01/21/75025930.html?pageNumber=57|accessdate=5 May 2016}}</ref>
*Galerie Jean-Paul Najar, Paris
*’75, ’76, ’77: Painting, Part I, Sarah Lawrence College Art Gallery, Bronxville, New York, February 19–March 10; American Foundation for the Arts, Miami, April–May; Contemporary Arts Center, Cincinnati, June–July
*A View of a Decade, Museum of Contemporary Art, Chicago, September 10–November 10
*John Weber Gallery, New York
 
1977–1978
*Works from the Collection of Dorothy and Herbert Vogel, [[University of Michigan Museum of Art]], Ann Arbor, November 11, 1977 – January 1, 1978
 
1978–1979
*Late Twentieth Century Art from the Sydney and Frances Lewis Foundation, [[Anderson Gallery]], [[Virginia Commonwealth University]], Richmond, December 5, 1978 – January 9, 1979; Institute of Contemporary Art, University of Pennsylvania, Philadelphia, *March 22 – May 2, 1979
 
1979
*1979 Biennial Exhibition, [[Whitney Museum of American Art]], New York, February 6–April 1<ref name=whit1979 />
*Generation, Susan Caldwell Gallery, New York
*The Reductive Object: A Survey of the Minimalist Aesthetic in the 1960s, [[Institute of Contemporary Art, Boston]], March 7 – April 29
*The Implicit Image: Abstract Painting in the ’70s, Nielsen Gallery, Boston, April 29 – June 1
*Color and Structure, Hamilton Gallery, New York, May 5 – June 2
*Texas Gallery, Houston
 
1980
*Black, White, Other, R.H. Oosterom Gallery, New York, January 17 – February 17
*Current/New York: Recent Works in Relief, Joe and Emily Lowe Art Gallery, [[Syracuse University]], Syracuse, New York, January 27–February 24
*Painting in Relief, [[Whitney Museum of American Art]], Downtown Branch, New York, January 30 – March 5
*Painting and Sculpture Today, [[Indianapolis Museum of Art]], June 24 – August 17
*3 Dimensional Painting, [[Museum of Contemporary Art, Chicago]], August 2 – November 9<ref>{{cite journal|last1=Artner|first1=Alan G.|title=After recent misses, MCA hits target with three new shows|journal=Chicago Tribune|date=August 10, 1980}}</ref>
*Planar Painting: Constructs, 1975–1980, [[The Alternative Museum]], New York, October 18–November 15, 1980
*The Image Transformed, Art Latitude Gallery, New York, November 4–29<ref>{{cite journal|last1=Russell|first1=John|title=Art: The Zeitgeist Signals Just Downstairs on 73rd St|journal=New York Times|date=June 30, 1981|url=http://timesmachine.nytimes.com/timesmachine/1980/11/07/112170748.html?pageNumber=64|accessdate=5 May 2016}}</ref>
 
1981
*A Seventies Selection: An Exhibition of Works from the Collection of the [[Whitney Museum of American Art]], [[Miami University Art Museum]], Oxford, Ohio, February 14 – June 14
*Abstract Mythologies, Nielsen Gallery, Boston, March 1–31
*Between Painting and Sculpture, Pam Adler Gallery, New York, March 31 – April 25
 
1981–1982
*Drawing Invitational 1981, [[Harm Bouckaert]] Gallery, New York, December 2, 1981 – January 2, 1982<ref>{{cite journal|last1=Vernet|first1=Gwynne|title=Drawing Invitational 1981|journal=Arts Magazine|date=1982|volume=56|issue=6|page=25}}</ref>
 
1982
*The Erotic Impulse, Roger Litz Gallery, New York<ref>{{cite journal|last1=Moufarrege|first1=Nicolas A.|title=The Erotic Impulse|journal=Arts Magazine|date=1982|volume=57|issue=3|page=5}}</ref>
*Postminimalism, [[Aldrich Museum of Contemporary Art]], Ridgefield, Connecticut, September 19–December 19
 
1983
*Abstract Painting: 1960–1969, [[P.S. 1 Contemporary Art Center]], Queens, January 16 – March 13
*New Work, New York: Newcastle Salutes New York, Newcastle Polytechnic Gallery, Newcastle upon Tyne, United Kingdom, October 8–November 4
 
1984
*Parasol and Simca: Two Presses/Two Processes, Center Gallery, [[Bucknell University]], Lewisburg, Pennsylvania, February 3–April 4, 1984; Sordoni Art Gallery, Wilkes College, Wilkes-Barre, Pennsylvania, April 15–May 13
*The Meditative Surface, [[Renaissance Society]] at the [[University of Chicago]], April 1 – May 16
 
1985
*Abstract Painting Redefined, Louis K. Meisel Gallery, New York, February 16 – March 30
*Now and Then: A Selection of Recent and Earlier Paintings, Daniel Weinberg Gallery, Los Angeles, June 1 – August 31
*American Abstract Painting: 1960–1980, [[Margo Leavin]] Gallery, Los Angeles, June 19 – August 24
 
1986
*The Purist Image, Marian Locks Gallery, Philadelphia, November
 
1986–1987
*The Window in Twentieth-Century Art, [[Neuberger Museum of Art]], [[Purchase College]], State University of New York, September 21, 1986 – January 18, 1987; Contemporary Arts Museum, Houston, April 24– June 29, 1987
 
1997
*A Lasting Legacy: Selections from the Lannan Foundation Gift, [[Museum of Contemporary Art, Los Angeles]], September 9–December 14<ref>{{cite web|title=A Lasting Legacy: Selections from the Lannan Foundation Gift|url=http://www.moca.org/exhibition/a-lasting-legacy-selections-from-the-lannan-foundation-gift|website=Museum of Contemporary Art|accessdate=5 May 2016}}</ref>
 
2004
*A Minimal Future?: Art as Object, 1958–1968, [[Museum of Contemporary Art, Los Angeles]], March 28 – July 26<ref>{{cite web|title=A Minimal Future?: Art as Object, 1958–1968|url=http://www.moca.org/exhibition/a-minimal-future-art-as-object-19581968|website=Museum of Contemporary Art|accessdate=5 May 2016}}</ref>
 
2006–2007
*High Times, Hard Times: New York Painting, 1967–1975, [[Weatherspoon Art Museum]], [[University of North Carolina, Greensboro]], August 6–October 15, 2006; [[American University Museum]] at the [[Katzen Arts Center]], [[American University]], Washington, D.C., November 21, 2006 – January 21, 2007; [[National Academy Museum and School]], New York, February 13 – April 27, 2007<ref name=hightimes />
 
2008
*The Idea of Nature, 33 Bond Gallery, New York, June 12–July 31
*Into the Void: Abstract Art, 1948–2008, [[Tucson Museum of Art]], July 17–September 26
 
2008–2009
*Steve DiBenedetto, Ralph Humphrey, Chris Martin, and Andrew Masullo/Paintings, Daniel Weinberg Gallery, Los Angeles, December 6, 2008 – January 31, 2009<ref>{{cite web|title=Steve DiBenedetto, Ralph Humphrey, Chris Martin, and Andrew Masullo/Paintings|url=http://www.danielweinberggallery.com/exhibitions/1147|website=Daniel Weinberg Gallery|accessdate=5 May 2016}}</ref>
 
2009
*Image Matter, [[Mary Boone]] Gallery, New York, February 21 – March 28<ref>{{cite web|title=Image Matter|url=http://www.maryboonegallery.com/exhibitions/2008-2009/image-matter/index.html|website=Mary Boone Gallery|accessdate=5 May 2016}}</ref>
*Not New: Vincent Fecteau Selects from the Collection, [[San Francisco Museum of Modern Art]], July 25 – November 8<ref>{{cite web|title=VINCENT FECTEAU MINES RARELY SEEN GEMS FROM SFMOMA’S COLLECTION FOR NEW WORK SERIES|url=https://www.sfmoma.org/press/release/vincent-fecteau-mines-rarely-seen-gems-from-sfmom/|website=SFMOMA|accessdate=5 May 2016}}</ref>
 
2010
*Wall-to-Wall, Daniel Weinberg Gallery, Los Angeles, June 5 – August 14<ref>{{cite web|title=Wall to Wall|url=http://www.danielweinberggallery.com/exhibitions/1167|website=Daniel Weinberg Gallery|accessdate=5 May 2016}}</ref>
 
2011
*Surface Truths: Abstract Painting in the Sixties, [[Norton Simon Museum]], Pasadena, California, March 25 – August 15<ref>{{cite web|title=Surface Truths: Abstract Painting in the Sixties|url=http://www.nortonsimon.org/surface-truths-abstract-painting-in-the-sixties/|website=Norton Simon Museum|accessdate=5 May 2016}}</ref>
 
2011–2012
*The Language of Less: Then and Now, [[Museum of Contemporary Art, Chicago]], October 8, 2011 – April 8, 2012<ref>{{cite book|last1=Darling|first1=Michael|title=The Language of Less, Then and Now|date=2012|publisher=Museum of Contemporary Art|location=Chicago|isbn=978-0933856912}}</ref>
 
2012
*Susan Hartnett, Ralph Humphrey, Marilyn Lerner, and [[Dona Nelson]], [[Mary Boone]] Gallery, New York, March 22 – April 28<ref>{{cite web|title=Susan Hartnett, Ralph Humphrey, Marilyn Lerner, Dona Nelson|url=http://maryboonegallery.com/exhibitions/2011-2012/group/index.html|website=Mary Boone Gallery|accessdate=5 May 2016}}</ref>
 
2014–2015
*The Avant-Garde Collection, [[Orange County Museum of Art]], Newport Beach, California, September 7, 2014 – January 4, 2015<ref>{{cite web|title=The Avant-Garde Collection|url=http://www.ocma.net/exhibition/avant-garde-collection|website=Orange County Museum of Art|accessdate=5 May 2016}}</ref>
 
2015
*Pretty Raw: After and Around [[Helen Frankenthaler]], [[Rose Art Museum]], [[Brandeis University]], Waltham, Massachusetts, February 11 – June 7, 2015<ref>{{cite web|title=PRETTY RAW: AFTER AND AROUND HELEN FRANKENTHALER|url=http://www.brandeis.edu/rose/onview/spring2015/prettyraw.html|website=Rose Art Museum|accessdate=5 May 2016}}</ref>

==Collections==
Humphrey's work can be found in prominent collections in America and Australia, including the following:
*[[Addison Gallery of American Art]]<ref>{{cite web|title=Addison Gallery of American Art|url=http://accessaddison.andover.edu/Obj11663?sid=224&x=15046&sort=9|accessdate=29 April 2016}}</ref> 
*[[Allen Memorial Art Museum]]<ref>{{cite web|title=Allen Memorial Art Museum|url=http://allenartcollection.oberlin.edu/emuseum/view/objects/aslist/search$0040?t:state:flow=22ab5485-9183-40d2-b552-7147ff318277|accessdate=29 April 2016}}</ref> 
*[[Art Institute of Chicago]]<ref>{{cite web|title=Art Institute of Chicago|url=http://www.artic.edu/aic/collections/artwork/196077?search_no=1&index=4|accessdate=29 April 2016}}</ref> 
*[[Butler Institute of American Art]]
*[[Carnegie Museum of Art]]<ref>{{cite web|title=Carnegie Museum of Art|url=http://www.cmoa.org/CollectionDetail.aspx?item=1003269&retPrompt=Back+to+Results&retUrl=CollectionSearch.aspx%3fsrch%3dralph%2bhumphrey|accessdate=29 April 2016}}</ref> 
*[[Dayton Art Institute]]
*[[Museum of Contemporary Art, Chicago]]<ref>{{cite web|title=Museum of Contemporary Art, Chicago|url=https://mcachicago.org/Search?utf8=✓&query=humphrey&commit=Search|accessdate=29 April 2016}}</ref> 
*[[Museum of Contemporary Art, Los Angeles]]<ref>{{cite web|title=Museum of Contemporary Art, Los Angeles|url=http://www.moca.org/artist/ralph-humphrey|accessdate=29 April 2016}}</ref> 
*[[Museum of Contemporary Art, San Diego]]
*[[Museum of Fine Arts, Boston]]
*[[Museum of Fine Arts, Houston]]<ref>{{cite web|title=Museum of Fine Arts, Houston|url=https://collections.mfah.org/art/detail/52933?returnUrl=%2Fart%2Fsearch%3Fq%3Dralph%2Bhumphrey|accessdate=29 April 2016}}</ref> 
*[[Museum of Modern Art]]<ref>{{cite web|title=Museum of Modern Art|url=http://www.moma.org/collection/works?locale=en&utf8=✓&q=ralph+humphrey&classifications=&date_begin=Pre-1850&date_end=2016|accessdate=29 April 2016}}</ref> 
*[[National Gallery of Australia]]
*[[Norton Simon Museum]]<ref>{{cite web|title=Norton Simon Museum|url=https://www.nortonsimon.org/collections/browse_artist.php?name=Humphrey%2C+Ralph|accessdate=29 April 2016}}</ref> 
*[[Oklahoma City Museum of Art]]
*[[Orange County Museum of Art]]
*[[Palm Springs Art Museum]]
*[[Parrish Art Museum]]
*[[Pérez Art Museum Miami]]
*[[Philadelphia Museum of Art]]<ref>{{cite web|title=Philadelphia Museum of Art|url=http://www.philamuseum.org/collections/permanent/74942.html?mulR=68659109|accessdate=29 April 2016}}</ref> 
*[[Rose Art Museum]]
*[[San Francisco Museum of Modern Art]]<ref>{{cite web|title=San Francisco Museum of Modern Art|url=https://www.sfmoma.org/artwork/77.2|accessdate=29 April 2016}}</ref> 
*[[Smithsonian American Art Museum]]<ref>{{cite web|title=Smithsonian Museum of American Art|url=http://americanart.si.edu/collections/search/artwork/?id=10943|accessdate=29 April 2016}}</ref> 
*[[Tucson Museum of Art]]
*[[Virginia Museum of Fine Arts]]
*[[Walker Art Center]]<ref>{{cite web|title=Walker Art Center|url=http://www.walkerart.org/collections/artworks/untitled-171|accessdate=29 April 2016}}</ref> 
*[[Weatherspoon Art Museum]]
*[[Whitney Museum of American Art]]<ref>{{cite web|title=Whitney Museum of American Art|url=http://collection.whitney.org/artist/630/RalphHumphrey|accessdate=29 April 2016}}</ref>

== References ==
{{Reflist|2}}

== External links ==
* [https://www.artsy.net/artist/ralph-humphrey Ralph Humphrey on Artsy]

{{DEFAULTSORT:Humphrey, Ralph}}
[[Category:American abstract artists]]
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:Painters from Ohio]]
[[Category:1990 deaths]]
[[Category:1937 births]]
[[Category:Artists from Youngstown, Ohio]]
[[Category:Youngstown State University alumni]]
[[Category:Artists from New York City]]