{{Taxobox
| image = Agaricus hondensis 71317.jpg
| image_width = 234px
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Agaricaceae]]
| genus = ''[[Agaricus]]''
| species = '''''A. hondensis'''''
| binomial = ''Agaricus hondensis''
| binomial_authority = [[William Alphonso Murrill|Murrill]] (1912)
| synonyms_ref = <ref name="urlMycoBank: Agaricus hondensis"/>
| synonyms = ''Agaricus bivelatoides'' <small>Murrill (1912)</small><br>
''Agaricus hillii'' <small>Murrill (1912)</small><br>
''Agaricus macmurphyi'' <small>Murrill (1912)</small><br>
''Agaricus glaber'' <small>[[Sanford Myron Zeller|Zeller]] (1938)<ref name="Zeller 1938"/></small>
}}
{{mycomorphbox
| name = ''Agaricus hondensis''
| whichGills = free
| capShape = convex
| capShape2 = flat
| hymeniumType = gills
| stipeCharacter = ring
| ecologicalType = saprotrophic
| sporePrintColor = brown
| sporePrintColor2 = purple-brown
| howEdible = poisonous
}}

'''''Agaricus hondensis''''', commonly known as the '''felt-ringed agaricus''', is a species of [[fungus]] in the family [[Agaricaceae]]. The species was officially described in 1912 by mycologist [[William Alphonso Murrill]], along with three other ''[[Agaricus]]'' species that have since been placed in [[synonym (taxonomy)|synonymy]] with ''A.&nbsp;hondensis''. Found in the [[Pacific Northwest]] region of North America, ''A.&nbsp;hondensis'' fruits in the fall under [[conifer]]s or in [[mixed forest]]s.

The fungus produces [[basidiocarp|fruit bodies]] ([[mushroom]]s) with white to gray-brown [[pileus (mycology)|caps]] up to {{convert|15|cm|in|0|abbr=on}} in diameter covered with pale pinkish-brown scales that darken in age. The tightly-packed [[lamella (mycology)|gills]] on the cap underside are initially white before becoming pinkish, [[lilac (color)|lilac]]-gray, and finally brownish as the [[spore]]s mature.  The stout [[stipe (mycology)|stipe]] is bulbous and has a thick, white, felt-like [[annulus (mycology)|ring]]. The mushroom is [[poisonous mushroom|poisonous]], and causes severe gastrointestinal upset if consumed. It has an unpleasant odor similar to [[phenol]] or [[creosote]], and develops a soapy-metallic taste when cooked. ''Agaricus hondensis'' can be distinguished from similar ''Agaricus'' species by differences in geographic range, habitat, staining reaction, and odor.

==Systematics==

The species was first [[species description|described]] as new to science by American mycologist [[William Alphonso Murrill]] in 1912, based on collections he made in November, 1911 under Californian [[Sequoioideae|redwoods]]. In the same publication, Murrill also described the species ''Agaricus bivelatoides'', ''A.&nbsp;hillii'', and ''A.&nbsp;macmurphyi'', all from the [[Pacific Northwest]] region of North America. The latter two were named to honor their original collectors, Albert Hill and [[James Ira Wilson McMurphy]], respectively, while the former was named for its resemblance to ''[[Agaricus bivelatus|A.&nbsp;bivelatus]]''.<ref name="Murrill 1912"/> In 1944, [[Alexander H. Smith]] examined the type material of several of Murrill's species, and concluded that were no [[phenotypic trait|characters]] to separate ''A.&nbsp;hondensis'' from ''A.&nbsp;bivelatoides'', ''A.&nbsp;hillii'', or ''A.&nbsp;macmurphyi''. He also determined that [[Sanford Myron Zeller]]'s ''A.&nbsp;glaber'', published as new in 1938,<ref name="Zeller 1938"/> was also the same species as ''A.&nbsp;hondensis''.<ref name="Smith 1944"/> The nomenclatural database [[MycoBank]] considers these names [[synonym (taxonomy)|synonymous]].<ref name="urlMycoBank: Agaricus hondensis"/> The [[specific name (botany)|epithet]] ''hondensis'' refers to the [[type (biology)|type]] location, [[La Honda, California]].<ref name="Murrill 1912"/> The mushroom is [[common name|commonly]] known as the "felt-ringed agaricus".<ref name="Ammirati 1985"/>

''Agaricus hondensis'' has traditionally been [[classification (biology)|classified]] in the [[section (biology)|section]] ''Xanthodermatei'', a grouping of ''Agaricus'' species related to ''[[Agaricus xanthodermus|A.&nbsp;xanthodermus]]'' that are characterized by fruit bodies with [[phenol]]ic odors, temporary yellowing discolorations in some parts of the fruit body, a negative [[Chemical tests in mushroom identification#Schaeffer’s test|Schaeffer’s reaction]], and [[Mushroom poisoning|toxicity]]. A [[Molecular phylogenetics|molecular]] analysis has shown that it, along with the related species ''[[Agaricus freirei|A.&nbsp;freirei]]'' and ''[[Agaricus phaeolepidotus|A.&nbsp;phaeolepidotus]]'', comprise a [[basal (phylogenetics)|basal]] lineage in a [[clade]] of related [[wikt:sylvan|sylvan]] species that have weak yellowing reactions and some tendencies toward reddish bruising reactions. This lineage is closely related to a group of ''Agaricus'' species that are typically placed in the section ''Sanguinolenti''. Phylogenetic evidence suggests that these three species belong to a clade that [[Genetic divergence|diverged]] shortly after the presumed split of the sections ''Xanthodermatei'' and ''Duploannulati''.<ref name="Kerrigan 2005"/>

==Description==
[[File:Agaricus hondensis 300272.jpg|thumb|left|The thick, skirtlike ring flares outward from the stipe.]]
The [[pileus (mycology)|cap]] is initially convex before flattening out, and reaches a diameter of {{convert|6|–|15|cm|in|abbr=on}}. The dry and smooth cap surface is whitish or has pale pinkish-brown to pinkish-gray to [[fawn (colour)|fawn]]-colored flattened fibrils or fine [[wikt:fibrillose|fibrillose]] scales (at least in the center). In maturity, the fibrils usually darken to brown, reddish-brown, or reddish-gray, but in one northern form the fibrils are darker brown from the beginning. The [[trama (mycology)|flesh]] is thick and white. When bruised or injured, the flesh either does not change color, or may stain pale yellowish, then often slowly discolors pinkish. The odor of the crushed flesh is mild or faintly phenolic, but is usually distinctly phenolic in the base of the stipe.<ref name="Arora 1986"/>

The [[lamella (mycology)|gills]] are initially pale pinkish to pinkish-gray before becoming brown, then chocolate-brown or darker when the [[spore]]s mature. In maturity, the gills are free from attachment to the stipe, and are packed close together with little intervening spaced between them. The [[stipe (mycology)|stipe]] is {{convert|7|–|20|cm|in|abbr=on}} long, and {{convert|1|–|2.5|cm|in|1|abbr=on}} thick but with a thicker or bulbous base. Firm, smooth, and lacking the scales found on the cap, the stipe is white but discolors dingy pinkish or brownish in age or after handling. The flesh in the extreme base usually stains pale yellowish when bruised. The [[partial veil]] is membranous, white, and forms a thick, felt-like [[annulus (mycology)|ring]] on the upper portion of the stipe. The ring is skirtlike but often flares outward instead of collapsing against the stipe.<ref name="Arora 1986"/> A drop of dilute [[potassium hydroxide]] placed on the cap turns yellow.<ref name="urlMushroomExpert"/>
[[File:Agaricus hondensis spores 1000x Alan Rockefeller.JPG|thumb|''Agaricus hondensis'' spores (1000x)]]
[[Spore print]]s are purplish brown to chocolate brown. The smooth, thick-walled [[basidiospore|spores]] are broadly [[ellipsoidal]], and typically measure 5.8–7.3 by 3.7–4.4&nbsp;[[micrometre|μm]]. The [[basidia]] (spore-bearing cells) are four-spored, club-shaped, [[hyaline]] (translucent, and measure 20–21.3 by 5.8–7.0&nbsp;μm. [[Cystidia]] on the gill edge (cheilocystidia) are sac-shaped to club-shaped, hyaline to pale yellowish brown in color, and have dimensions of 18.3–25.6 by 7.3–11.0&nbsp;μm; there are no cystidia on the gill face (pleurocystidia).<ref name="Ammirati 1985"/>

===Similar species===
Distinctive field characteristics of ''Agaricus hondensis'' include its woodland habitat, the yellow staining reaction with KOH, and its odor.<ref name="Lincoff 1981"/> ''Agaricus freirei'' closely resembles ''A.&nbsp;hondensis'', and, based on similarities in [[DNA sequence]]s, is a close relative.<ref name="Kerrigan 2005"/> ''A.&nbsp;freirei'' is found in coastal regions of Spain.<ref name="Blanco-Dios 2001"/> ''A.&nbsp;hondensis'' has also been confused with ''[[Agaricus silvaticus|A.&nbsp;silvaticus]]'' and ''[[Agaricus placomyces|A.&nbsp;placomyces]]''.<ref name="Ammirati 1985"/> ''A.&nbsp;sylvaticus'' does not have foul-smelling flesh, and has a negative KOH reaction.<ref name="Noordeloos 2001"/> ''A.&nbsp;placomyces'' is found from the midwestern United States eastward.<ref name="McKnight 1987"/>  Another lookalike, the edible ''[[Agaricus subrutilescens|A.&nbsp;subrutilescens]]'', has similar overall coloration, but is distinguished from ''A.&nbsp;hondensis'' by a mild odor, a shaggy stipe, and a less substantial ring.<ref name="Ammirati 2009"/>

==Habitat and distribution==

A [[saprobic]] species,<ref name="urlMushroomExpert"/> the fruit bodies of ''Agaricus hondensis'' grow scattered or in groups under [[conifer]]s or in [[mixed forest]]s.<ref name="Ammirati 2009"/> They have also been reported to grow in [[fairy ring]]s.<ref name="urlMushroomExpert"/> The fungus is found in the [[Pacific Coast]] of North America, from [[British Columbia]] in Canada south to [[California]],<ref name="Schalkwijk-Barendsen 1991"/> but is most common in California.<ref name="Ammirati 2009"/> The mushroom fruits in the fall from September to October throughout much of its range, but in California the fruiting season tends to be from November to February.<ref name="Lincoff 1981"/>

==Toxicity==
''Agaricus hondensis'' mushrooms are [[poisonous mushroom|toxic]], and consuming the fruit bodies causes [[gastroenteritis]].<ref name="Barceloux 2008"/> Some fruit bodies smell of [[creosote]], an odor that becomes even more prevalent if the mushrooms are cooked. Cooking also introduces an unpleasant soapy-metallic flavor.<ref name="Ammirati 1985"/> The fruit bodies are used as food by the [[vagrant shrew]] (''Sorex vagrans'') and the [[American shrew mole]] (''Neurotrichus gibbsii'').<ref name="Terry 1978"/> Relatively high levels of the chemical [[hydroquinone]] are present in fruit bodies.<ref name="Jovel 1996"/>

==See also==
{{Portal|Fungi}}
* [[List of Agaricus species|List of ''Agaricus'' species]]

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Ammirati 1985">{{cite book |vauthors=Ammirati J, Traquair JA, Horgen PA |title=Poisonous Mushrooms of the Northern United States and Canada |year=1985 |publisher=Fitzhenry & Whiteside in cooperation with Agriculture Canada |location=Ottawa, Canada |isbn=978-0-88902-977-4 |page=226 |url=https://books.google.com/books?id=nhWbsGB7z4cC&pg=PA226}}</ref>

<ref name="Ammirati 2009">{{cite book |vauthors=Ammirati J, Trudell S |title=Mushrooms of the Pacific Northwest |series=Timber Press Field Guides |publisher=Timber Press |location=Portland, Oregon |year=2009 |page=190 |isbn=978-0-88192-935-5 |url=https://books.google.com/books?id=WevHvt6Tr8kC&pg=PA190}}</ref>

<ref name="Arora 1986">{{cite book |author=Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |pages=326–7 |isbn=978-0-89815-169-5}}</ref>

<ref name="Barceloux 2008">{{cite book |author=Barceloux DG |title=Medical Toxicology of Natural Substances: Foods, Fungi, Medicinal Herbs, Plants, and Venomous Animals |url=https://books.google.com/books?id=CpqzhHc072AC&pg=PA291 |year=2008 |publisher=John Wiley & Sons |isbn=978-0-470-33557-4 |page=291}}</ref>

<ref name="Blanco-Dios 2001">{{cite journal |author=Blanco-Dios JB. |title=Agaricales des dunes de Galice (nord-ouest de l'Espagne). 1. ''Agaricus freirei'' Blanco-Dios, sp. nov. |journal=Documents Mycologiques |year=2001 |volume=31 |issue=127 |pages=27–34 |language=French}}</ref>

<ref name="Jovel 1996">{{cite journal |vauthors=Jovel E, Kroeger P, Towers N |title=Hydroquinone: the toxic compound of ''Agaricus hondensis'' |journal=Planta Medica |year=1996 |volume=62 |issue=2 |page=195 |pmid=17252436 |doi=10.1055/s-2006-957852}}</ref>

<ref name="Kerrigan 2005">{{cite journal |vauthors=Kerrigan RW, Callac P, Guinberteau J |title=''Agaricus'' section ''Xanthodermatei'': a phylogenetic reconstruction with commentary on taxa |journal=Mycologia |year=2005 |volume=97 |issue=6 |pages=1292–315 |doi=10.3852/mycologia.97.6.1292 |pmid=16722221}}</ref>

<ref name="Lincoff 1981">{{cite book |author=Lincoff G. |title=The Audubon Society Field Guide to North American Mushrooms |year=1981 |publisher=Knopf |location=New York, New York |page=507 |isbn=978-0-394-51992-0}}</ref>

<ref name="McKnight 1987">{{cite book |vauthors=McKnight VB, McKnight KH |title=A Field Guide to Mushrooms: North America |series=Peterson Field Guides |publisher=Houghton Mifflin |location=Boston, Massachusetts |year=1987 |page=259 |isbn=978-0-395-91090-0}}</ref>

<ref name="Murrill 1912">{{cite journal |author=Murrill WA |title=The Agaricaceae of the Pacific Coast – III. Brown and black-spored genera |journal=Mycologia |year=1912 |volume=4 |issue=6 |pages=294–308 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0004/006/0294.htm |doi=10.2307/3753286}}</ref>

<ref name="Noordeloos 2001">{{cite book |vauthors=Noordeloos ME, Kuyper TW, Vellinga EC |title=Flora Agaricina Neerlandica |volume=5 |url=https://books.google.com/books?id=Ckubw5T_bo8C&pg=PA39 |year=2001 |publisher=CRC Press |isbn=978-90-5410-495-7 |page=39}}</ref>

<ref name="Schalkwijk-Barendsen 1991">{{cite book |author=Schalkwijk-Barendsen HME. |title=Mushrooms of Western Canada |publisher=Lone Pine Publishing |location=Edmonton, Canada |year=1991 |page=330 |isbn=978-0-919433-47-2}}</ref>

<ref name="Smith 1944">{{cite journal |author=Smith AH |title=Interesting North American Agarics |journal=Bulletin of the Torrey Botanical Club  |year=1944 |volume=71 |issue=4 |pages=390–409 |jstor=2481312}}</ref>

<ref name="Terry 1978">{{cite journal |author=Terry CJ |title=Food habits of three sympatric species of insectovora in western Washington |journal=Canadian Field-Naturalist |year=1978 |volume=92 |pages=38–44 |url=http://biodiversitylibrary.org/page/28062281}}</ref>

<ref name="urlMycoBank: Agaricus hondensis">{{cite web |title=''Agaricus hondensis'' Murrill 1912 |url=http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=321094&Fields=All |publisher=[[MycoBank]]. International Mycological Association |accessdate=2012-07-13}}</ref>

<ref name="urlMushroomExpert">{{cite web |author=Kuo M. |title=''Agaricus hondensis'' |url=http://www.mushroomexpert.com/agaricus_hondensis.html |publisher=MushroomExpert.com |date=February 2005 |accessdate=2014-01-04}}</ref>

<ref name="Zeller 1938">{{cite journal |author=Zeller SM |title=New or noteworthy agarics from the Pacific Coast States |journal=Mycologia |year=1938 |volume=30 |issue=4 |pages=468–74 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0030/004/0468.htm |jstor=3754471 |doi=10.2307/3754471}}</ref>

}}

==External links==
*{{IndexFungorum|155127}}

{{good article}}

[[Category:Agaricus|hondensis]]
[[Category:Fungi described in 1912]]
[[Category:Fungi found in fairy rings]]
[[Category:Fungi of North America]]
[[Category:Poisonous fungi]]
[[Category:Taxa named by William Alphonso Murrill]]