{{Infobox person
|name         = James Donovan
|image        = James B. Donovan.jpg  
|image_size   = 
|caption      =
|birth_name   = James Britt Donovan
|birth_place  = [[The Bronx]], [[New York (state)|New York]], [[United States|U.S.]]
|birth_date   = {{Birth date|1916|02|29}}
|death_date   = {{Death date and age|1970|01|19|1916|02|19|mf=y}}
|death_place  = [[Brooklyn]], New York, U.S.
|occupation   = [[Military officer]], [[lawyer]], [[educator]]
|alma_mater   = [[Fordham University]], B.A. 1937<br>[[Harvard Law School]], LL.B. 1940
|known_for    = Negotiating the 1962 exchange of [[Francis Gary Powers]] &  [[Frederic Pryor]] for [[Rudolf Abel]]
|spouse       = Mary McKenna (1941–1970; his death)
|children     = 4
|website = 
}}

'''James Britt Donovan''' (February 29, 1916 – January 19, 1970)<ref>[http://socialarchive.iath.virginia.edu/xtf/view?docId=donovan-james-b-james-britt-1916-1970-cr.xml Donovan, James B.], socialarchive.iath.virginia.edu, access date 5 June 2015</ref><ref>[http://jfk.hood.edu/Collection/White%20Materials/Security-CIA/CIA%200255.pdf Dr. James B. Donovan, 53, Dies], nytimes.com, access date 5 June 2015</ref> was an American lawyer, [[United States Navy]] officer in the Office of Scientific Research and Development and the Office of Strategic Services, ultimately becoming General Counsel at the OSS (the predecessor of the CIA), and international diplomatic negotiator.

Donovan is widely known for negotiating the 1960–1962 exchange of captured American U-2 pilot [[Francis Gary Powers]] for Soviet spy [[Rudolf Abel]], and for negotiating the 1962 release and return of 9,703 prisoners held by Cuba after the failed [[Bay of Pigs Invasion]].<ref name="NEGOTIATOR" /><ref name="NATION" /> Donovan was portrayed by [[Tom Hanks]] in the 2015 Oscar-winning film ''[[Bridge of Spies (film)|Bridge of Spies]]'' directed by [[Steven Spielberg]] from a screenplay written by [[Matt Charman]], [[Coen brothers|Ethan Coen, and Joel Coen]].

==Early life and early career==
James Britt Donovan was born on February 29, 1916, in the [[Bronx]]. He was the son of Harriet (O'Connor), a piano teacher, and John J. Donovan, a surgeon. His brother was New York state senator [[John J. Donovan Jr.]] Both sides of the family were of [[Irish American|Irish]] descent. He attended the Catholic [[All Hallows High School|All Hallows Institute]]. In 1933, he began his studies at [[Fordham University]], where he completed a [[Bachelor of Arts]] degree in English in 1937. He wanted to become a journalist but his father convinced him to study law at [[Harvard Law School]], beginning in autumn of 1937, where he completed his [[Bachelor of Laws]] degree in 1940.<ref name="NEGOTIATOR" />

After graduating from law school, Donovan started work at a private lawyer's office. He was a commander in the Navy during World War II. In 1942, he became Associate General Counsel at the [[Office of Scientific Research and Development]]. From 1943 to 1945, he was General Counsel at the [[Office of Strategic Services]]. In 1945, he became assistant to Justice [[Robert H. Jackson]] at the [[Nuremberg trials]] in Germany.<ref>[http://avalon.law.yale.edu/imt/prosecu.asp Nuremberg Trial Proceedings Vol. 1 : Prosecution Counsel], avalon.law.yale.edu, access date 5 June 2015</ref> Donovan was the presenter of visual evidence at the trial. While he prepared for the trials he also worked as an advisor for the documentary feature ''[[The Nazi Plan]]''. In 1950, Donovan became a partner in the New York-based law office of Watters and Donovan, specializing in insurance law.

==Release of Gary Powers==
In 1957, he defended the Soviet spy [[Rudolf Abel]], after many other lawyers refused.<ref name="TMJ">[https://news.google.com/newspapers?nid=1499&dat=19620216&id=V_4pAAAAIBAJ&sjid=-iYEAAAAIBAJ&pg=4788,2957770 The Man Who 'Sprung' Powers] in: ''The Milwaukee Journal'', 16 February 1962, page 8</ref> He later brought in [[Thomas M. Debevoise]] to assist him;<ref>{{cite news |date=February 9, 1995 |title=Thomas Debevoise, Prosecutor, 65, Dies |url=https://www.nytimes.com/1995/02/09/obituaries/thomas-debevoise-prosecutor-65-dies.html |newspaper=New York Times |location=New York, NY}}</ref> at trial, Abel was convicted, but Donovan was successful in persuading the court not to impose a death sentence. He then appealed Abel's case all the way to the [[Supreme Court of the United States|Supreme Court]], which in ''[[Abel v. United States]]'' rejected, by a 5–4 vote, Donovan's argument that evidence used against his client had been seized by the [[FBI]] in violation of the [[Fourth Amendment to the United States Constitution|Fourth Amendment]].<ref>{{cite web|title=Abel v. United States (1960)|url=http://caselaw.findlaw.com/us-supreme-court/362/217.html|website=FindLaw|publisher=Thompson Reuters|access-date=11 October 2015}}</ref> Chief Justice of the United States [[Earl Warren]] praised him and publicly expressed the "gratitude of the entire court" for his taking the case.<ref name="TMJ" />

In 1962, Donovan—who was lead negotiator—and CIA lawyer [[Milan C. Miskovsky]]<ref>[https://www.cia.gov/news-information/featured-story-archive/milan-miskovsky.html The People of the CIA … Milan Miskovsky: Fighting for Justice], cia.gov, access date 5 June 2015</ref> negotiated with Soviet mediators to free captured American pilot [[Francis Gary Powers]]. Donovan successfully negotiated for the exchange of Powers, along with American student [[Frederic Pryor]], for the still-imprisoned Rudolf Abel, whom Donovan had defended five years earlier.<ref name="EOK">[https://books.google.com/books?id=PEojQDou7MIC&pg=PA193#v=onepage&q&f=false Donovan, James Britt] in: ''Encyclopedia of the Kennedys: The People and Events That Shaped America.'', page 193, ABC-CLIO, 2012, ISBN 978-1-59884-538-9</ref>

==Involvement in Cuba==
In June 1962, Donovan was contacted by Cuban exile Pérez Cisneros, who asked him to support the negotiations to free the 1,113<ref name=":0">{{Cite web|title = Bay of Pigs survivors on US-Cuba thaw: 'Two American presidents betrayed us'|url = https://www.theguardian.com/us-news/2014/dec/23/bay-of-pigs-survivors-veterans-betrayal-cuba-us|website = the Guardian|accessdate = 2015-12-22|first = Richard Luscombe in|last = Miami}}</ref> prisoners of the failed [[Bay of Pigs Invasion]].<ref name="NATION">{{cite web |url=http://www.thenation.com/article/173756/us-cuban-diplomacy-nation-style |title=US-Cuban Diplomacy, 'Nation' Style |last=Kornbluh |first=Peter |website=The Nation |access-date=9 December 2015}}</ref><ref name="FGBK">[http://www.fg-berlin-kuba.de/index.php?option=com_content&view=article&id=53%3Afbk-dokumentation-nr-9&catid=11%3Afbk-dokumentationen&Itemid=17&limitstart=3 FBK-Dokumentation Nr. 9 – Seite 4], fg-berlin-kuba.de, access date 23 June 2015</ref> Donovan offered [[pro bono]] legal service for the Cuban Families Committee of prisoners' relatives.<ref name="FGBK" /> A few months later, he traveled to Cuba for the first time. [[Cuba–United States relations]] were extremely tense after the invasion attempt. When [[Fidel Castro]] met Donovan for the first time, he was very short-spoken.<ref>[http://www.cuba-l.com/l-document-albuquerque-how-metadiplomacy-works-jamesndonovan-and-castro/ How Metadiplomacy Works: James Donovan and Castro], cuba-l.com, access date 5 June 2015</ref> Donovan managed to create confidence. Castro also praised Donovan for bringing his son to Cuba.<ref name="NATION" />

On December 21, 1962, Castro and Donovan signed an agreement to exchange all 1,113 prisoners for $53 million in food and medicine,<ref name=":0" /> sourced from private donations and from companies expecting tax concessions.<ref name="EOK" /> Donovan had the idea to exchange the prisoners for medicine after he had found out that the Cuban medicine didn't help him with his own [[bursitis]].<ref>[http://www.spiegel.de/spiegel/print/d-45143788.html James Britt Donovan] in: ''[[Der Spiegel]]'' 23/1963, pahe 82</ref> By the end of the negotiations, July 3, 1963, Donovan had secured the release of 9,703 men, women and children from Cuban detention.<ref>{{Cite book|title = Encyclopedia of the Kennedys: The People and Events That Shaped America|url = https://books.google.com/books?id=PEojQDou7MIC|publisher = ABC-CLIO|date = 2012-09-07|isbn = 9781598845396|first = Joseph M.|last = Siracusa}}</ref> Donovan was once again teamed up with CIA lawyer [[Milan C. Miskovsky]] on these negotiations.<ref>[https://www.cia.gov/news-information/featured-story-archive/milan-miskovsky.html The People of the CIA … Milan Miskovsky: Fighting for Justice], cia.gov, access date 9 March 2016</ref> For his work, Donovan received the [[Distinguished Intelligence Medal]].

==Later life and death==
From 1961 to 1963, Donovan was Vice President of the New York Board of Education, and from 1963 until 1965, he was the President of the board. In June 1962, his alma mater Fordham presented Donovan with an honorary degree. In 1962, he was the Democratic nominee for U.S. Senate in New York but lost in November 1962 to Republican incumbent [[Jacob K. Javits]].<ref>[http://www.ourcampaigns.com/RaceDetail.html?RaceID=30176 NY US Senate – November 06, 1962], ourcampaigns.com, access date 5. June 2015</ref> In 1968, Donovan was appointed President of [[Pratt Institute]]. He died of a heart attack on January 19, 1970, in Brooklyn's Methodist Hospital in New York, after being treated for [[influenza]].<ref name="OBIT">{{cite news |date=20 January 1970 |title=Dr. James B. Donovan, 53, Dies; Lawyer Arranged Spy Exchange |newspaper=The New York Times |url=https://query.nytimes.com/gst/abstract.html?res=940DE5D91739E63BBC4851DFB766838B669EDE |archive-url=http://jfk.hood.edu/Collection/White%20Materials/Security-CIA/CIA%200255.pdf |archive-date=9 December 2015 |access-date=9 December 2015 }}</ref>

== Personal life ==
In 1941, Donovan married Mary E. McKenna. The couple had a son and three daughters, and lived in Brooklyn, New York while also maintaining seasonal residences in Spring Lake, New Jersey and Lake Placid, NY where Donovan is buried alongside his wife and daughter. He was a rare book collector, golfer, tennis player and gin rummy player. A collection of his papers is held at Stanford University's Hoover Library & Archives.

== In popular culture ==
The story of the Abel trial and defense, followed by the negotiation and prisoner exchange, was the basis for the book ''Strangers on a Bridge: The Case of Colonel Abel and Francis Gary Powers'', written by Donovan and [[ghost writer]] Bard Lindeman, which was published in 1964.<ref name="NEGOTIATOR">{{cite book |last=Bigger |first=Philip J. |date=2006 |title=Negotiator: The Life and Career of James B. Donovan }}</ref> Several similar works would come later, but ''Strangers'' was the definitive work and was widely critically acclaimed.<ref name="STRANGERS">{{cite web |url=http://books.simonandschuster.com/Strangers-on-a-Bridge/James-Donovan/9781501118784 |title=Strangers on a Bridge |date=August 2015 |website=Simon & Schuster |access-date=9 December 2015}}</ref><ref name="MISKOVSKY">{{cite web |url=https://www.cia.gov/library/center-for-the-study-of-intelligence/kent-csi/vol9no3/html/v09i3a12p_0001.htm |title=Strangers on a Bridge by James B. Donovan. Book review by M. C. Miskovsky |last=Miskovsky |first=M. C. |date=22 September 1993 |website=Central Intelligence Agency |access-date=9 December 2015}}</ref> The book was re-released by Simon & Schuster in August 2015.<ref name="STRANGERS" /><ref name="BESTSELLER">{{cite web |url=https://www.nytimes.com/best-sellers-books/2015-12-13/espionage/list.html |title=The New York Times: Best Sellers: Espionage |date=December 2015 |website=The New York Times |access-date=9 December 2015}}</ref> In 1967, Donovan published his second book, ''Challenges: Reflections of a Lawyer-at-Large''.

[[James Gregory (actor)|James Gregory]] played Donovan in the 1976 TV movie ''[[Francis Gary Powers]]: The True Story of the U-2 Spy Incident'', based on Powers' biography (written with [[Curt Gentry]]). [[Lee Majors]] played Powers.<ref>[http://www.imdb.com/title/tt0074545/ Francis Gary Powers: The True Story of the U-2 Spy Incident--IMDB]</ref> In 2006, Philip J. Bigger published a biography of Donovan, ''Negotiator: The Life and Career of James B. Donovan''.,<ref name="NEGOTIATOR" /> which will be re-released in paperback in January, 2017.

Though not officially the basis for the movie ''Bridge of Spies'', ''Strangers on a Bridge'' is the closest and first-hand narrative to the dramatic events in the Oscar-nominated and winning movie.  The 1964 New York Times Best Seller repeated itself in 2015, becoming #1 on the NYT Best Seller list for espionage books. The book has been widely acclaimed, including by Steven Spielberg and Tom Hanks themselves. The re-issue coincided with the pre-release promotion for the movie ''[[Bridge of Spies (film)|Bridge of Spies]]'', directed by [[Steven Spielberg]] and written by [[Matt Charman]] and the [[Coen Brothers]], which was released on October 16, 2015. [[Tom Hanks]] plays the role of Donovan, with [[Amy Ryan]] as his wife Mary.<ref>[http://variety.com/2014/film/news/tom-hanks-steven-spielberg-cold-war-thriller-set-for-oct-16-2015-1201221269/ Tom Hanks-Steven Spielberg Cold War Thriller Set for Oct. 16, 2015], variety.com, access date 5 June 2014</ref>

In October, 2016, Fordham inducted Donovan into its Hall of Honor in conjunction with its Dodransbicentennial, the 175th anniversary of the school, in a mass at St. Patrick's Cathedral with Cardinal Timothy Dolan, who was also named a founder of the school. Fordham was founded by Archbishop Hughes, who is an ancestor of James Donovan's.  Also in October, 2016, Donovan was inducted into the All Hallows School Hall of Fame.

== Works ==
* {{cite book|first=James B.|last=Donovan|title=Strangers on a Bridge, The Case of Colonel Abel|place=Atheneum|date= 1964|isbn=978-1-299-06377-8|ref=harv}}
* {{cite book|first=James B.|last=Donovan|title=Challenges: Reflections of a Lawyer-at-Large|place=Atheneum|date= 1967|ref=harv}} (with a preface by [[Erwin Griswold]])

== References ==
{{reflist|2}}

== Further reading ==
* {{cite book|last=Bigger|first= Philip J. |date=2006|title=Negotiator: The Life And Career of James B. Donovan|place= Bethlehem|publisher= Lehigh University Press|isbn=978-0-934-22385-0|ref=harv}}

== External links ==
* [http://jamesbdonovan.com/ James B. Donovan – A Legacy of Ideals and Action]
* [http://www.oac.cdlib.org/findaid/ark:/13030/tf8s2006kw/ James B. Donovan Papers, 1919–76] at the [http://www.hoover.org/library-archives/ Hoover Institution Archives] at [[Stanford University]]

{{s-start}}
{{s-ppo}}
{{succession box|title= Democratic Nominee for [[U.S. Senator from New York]] (Class 3) | before = [[Robert F. Wagner, Jr.]] <br>1956 |after = [[Paul O'Dwyer]] <br>1968 | years = 1962}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Donovan, James B.}}
[[Category:1916 births]]
[[Category:1970 deaths]]
[[Category:American people of Irish descent]]
[[Category:Central Intelligence Agency]]
[[Category:Fordham University alumni]]
[[Category:Harvard Law School alumni]]
[[Category:People of the Central Intelligence Agency]]
[[Category:Recipients of the Distinguished Intelligence Medal]]
[[Category:People from the Bronx]]