{{Infobox person
|name        = Bertha Bracey
|image       = Bertha Bracey.jpg
|birth_date  = 1893
|birth_place = [[Bournville]]
|death_date  = 1989 (aged 95)
|death_place =
|occupation  = teacher, youth worker, aid worker
}}

'''Bertha Lilian Bracey''' (1893–1989) was a [[Quaker]] teacher and aid worker who organised relief and sanctuary for Europeans affected by the turmoil before, during and after the Second World War.  These included many Jewish children threatened by the Holocaust and rescued in the operation known as the ''[[Kindertransport]]''.  In 2010, she was recognised as a [[British Hero of the Holocaust]].

==Early life and education==
Her father worked for the Quaker chocolate maker, [[Cadbury]], in their model village of [[Bournville]].{{sfn|Woodford|p=1}} Her mother was Annie née Miles.{{sfn|Oldfield|2001|p=27}}  She went to Birmingham University and, after graduating, she worked in personnel and as a teacher for five years.{{sfn|Oldfield|2001|p=27}}{{sfn|Smith|2013|p=31}}

==Quaker relief work==
She joined the [[Society of Friends]] – the Quakers – when she was about eighteen.{{sfn|Oldfield|2001|p=27}}{{sfn|Smith|2013|p=31}}  In 1921, she left teaching to work at the Quaker Centre in [[Vienna]] where she founded and operated [[youth club]]s.  She enjoyed singing with young people and her work in these centres gave her good fluency in the German language and a network of many contacts.{{sfn|Woodford|p=1}}  The Quaker International Centres had been conceived by [[Carl Heath]] in 1916 and eight of them were established across Europe after the First World War.  After Vienna, Bracey moved to Germany where the hyperinflation and instability of the [[Weimar Republic]] caused great hardship.  At the centres in [[Nuremberg]] and then Berlin, she organised aid for the population, especially children.  The provision of food to the impoverished and starving was known as the ''[[:de:Quäkerspeisung|Quäkerspeisung]]'' – the Quaker feeding – and it so endeared the Quakers to the German people that it enabled them to aid refugees during the Nazi era.{{sfn|Smith|2013|pp=31–34}}

In 1929, she became an Administrative Secretary of the Germany and Holland Committee in the [[Friends House|Quaker headquarters]] in London, responsible for the relief operations in Germany and Holland.{{sfn|Smith|2013|p=34}}  In 1933, she took charge of the newly-formed German Emergency Committee and this was later renamed as the Friends Committee for Refugees and Aliens.{{sfn|Burkitt|p=70}}  As the work expanded, her staff in Friends House grew from a single assistant to 59 case-workers in 1938 and, with crowds of refugees to process, they overflowed into Drayton House nearby.{{sfn|Bramsted}}

==Schools==
She helped found the [[Stoatley Rough School]] for German refugees in [[Haslemere]] in England.{{sfn|ESP|2012}}  This started when [[Hilde Lion]] contacted the German Emergency Committee in 1933 with plans to form a school to help German children adjust to British education.  Bracey chaired the board of governors from 1938 to 1945 and continued as a governor of the school until 1960.{{sfn|SRS|2004}}

In 1934, she helped establish a school for German Jewish children in the castle of [[Eerde, Ommen|Eerde]] in Holland.{{sfn|Smith|2013|p=37}}

==''Kindertransport''==
Bracey had recognised the threat to the Jews of Germany in 1933, after Hitler became Chancellor and the Nazi party took control, "Words are not adequate to tell of the anguish of some of my Jewish friends".{{sfn|Smith|2013|p=34}} After the great pogrom of ''[[Kristallnacht]]'' in 1938, she visited Berlin and was then part of the delegation which met with the British Home Secretary [[Samuel Hoare, 1st Viscount Templewood|Sir Samuel Hoare]] to convince him to expedite the acceptance of Jewish children as refugees from Germany.  She then led the Quaker team which formed part of the Movement for the Care of Children from Germany.  Initially, they were based in Friends House but this was overcrowded and so the Palace Hotel in Bloomsbury Street was bought to become Bloomsbury House – a centre for all the refugee organisations to work together.  Bracey became secretary of the Inter-Church Council for German Refugees and led a team of 80 Quaker case-workers on the third floor.{{sfn|Smith|2013|p=45}}  During the war, she took on further duties.  In 1940, after the fall of France caused concern about the security risk of having German refugees in Britain resulting in [[internment]], she led the Central Department for Interned Refugees which addressed the practical and humanitarian issues arising from this policy.  At the end of the war, she was still saving children.  For example, in 1945, she arranged for the [[Royal Air Force|RAF]] to fly 300 orphans from [[Theresienstadt concentration camp]] to a reception centre by [[Lake Windermere]].{{sfn|Smith|2013|pp=47–49}}

==Later life==
In the aftermath of the war, there were many [[displaced person]]s so she joined the [[Allied Control Commission]] to handle refugees in Germany.  Later she was made responsible for women's affairs in the [[Allied-occupied_Germany#American_Zone_of_Occupation|American and British Zones of Occupation]].  She then retired from this post in 1953, having reached the age of 60.{{sfn|Bramsted}}  Later, in her nineties, she suffered from [[Parkinson's disease]] but she was still cheerful and an inspiration to others,{{sfn|Grunwald-Spier|p=27}}{{quote|'Is there anything I can bring you?' asked a visitor of hers in her nursing home during her last days.  Bertha roused herself from a partial slumber to the alertness we remember so well, 'Yes,' she responded, 'bring me glad tidings of great joy.'}}

==Awards and memorials==
In 1942, she was awarded the [[Order of the British Empire]] (OBE) for her services to refugees.{{sfn|Blake|2010}}  In 1999, a rose was dedicated to her at the [[Beth Shalom Holocaust Centre]].{{sfn|Bramsted}}  In 2010, she was recognised as a [[British Hero of the Holocaust]] by Prime Minister [[Gordon Brown]].{{sfn|Blake|2010}}  [[Naomi Blake]], who was herself a survivor of Auschwitz, sculpted a statue dedicated to Bertha Bracey and it is now on display in [[Friends House]].{{sfn|QW}}  The inscription reads{{sfn|Grunwald-Spier|p=27}}{{quote|To honour Bertha Bracey (1893–1989)<br>who gave practical leadership to Quakers in quietly rescuing and re-settling<br>thousands of Nazi victims and lone children between 1933–1948}}

==References==

===Citations===
{{reflist|30em}}

===Sources===
* {{citation |last=Blake |url=http://www.telegraph.co.uk/history/britain-at-war/7406043/The-remarkable-stories-of-Britains-Heroes-of-the-Holocaust.html |first=Heidi |date=10 March 2010 |newspaper=The Daily Telegraph |title=The remarkable stories of Britain's Heroes of the Holocaust }}

* {{citation |last=Bramsted |url=http://jackwhite.net/quakers/bracey.html |first=Eric |title=Bertha Bracey OBE |year=2010}}

* {{citation |last=Bryan |title=Bertha L. Bracey: Friend of the Oppressed |date=January 1991 |journal=Friends' Quarterly |pages=233–241 |first=Alex }}

* {{citation |last=Burkitt |title=British Society and the Jews |first=Nicholas Mark |publisher=University of Exeter |year=2011}}

* {{citation |work=Exploring Surrey's Past |url=http://www.exploringsurreyspast.org.uk/themes/subjects/refugees/stoatley_rough_school/ |title=Stoatley Rough School|year=2012 |ref={{harvid|ESP|2012}}}}

* {{citation |last=Grunwald-Spier |title=The Other Schindlers: Why Some People Chose to Save Jews in the Holocaust |first=Agnes |publisher=The History Press |year=2010 |isbn=9780752462431}}

* {{citation |last=Kurer |url=http://www.ajr.org.uk/journalpdf/2006_june.pdf |journal=Association of Jewish Refugees |volume=6 |number=6 |year=2006 |title=How Did You Escape from Nazi Europe? |first=Peter |page=5}}

* {{citation |last=Lampos |url=http://cleolampos.com/bertha-bracey-ordinary-people-living-extraordinary-lives/ |date=31 May 2013 |title=The Sound of a Train Whistle |first=Cleo }}

* {{citation |url=http://www.quakersintheworld.org/quakers-in-action/118 |title=Kindertransport |work=Quakers in the World |ref={{harvid|QW}}}}

* {{citation |last=Oldfield |title=Women Humanitarians: A Biographical Dictionary of British Women Active Between 1900 and 1950 |first=Sybil |publisher=Continuum |year=2001 |isbn=9780826449627}}

* {{citation |last=Oldfield |title="It Is Usually She": The Role of British Women in the Rescue and Care of the Kindertransport Kinder |first=Sybil |journal=Shofar: An Interdisciplinary Journal of Jewish Studies |volume=23 |number=1 |year=2004 |pages=57–70 |doi=10.1353/sho.2005.0032}}

* {{citation |last=Schmitt |title=Quakers and Nazis: Inner Light in Outer Darkness |first=Hans |publisher=University of Missouri Press |year=1997 |isbn=9780826211347}}

* {{citation |last=Seymour |title=Noble Endeavours |first=Miranda |publisher=Simon and Schuster |year=2013 |isbn=9781847378262}}

* {{citation |last=Smith |title= Heroes of the Holocaust: Ordinary Britons Who Risked Their Lives to Make a Difference |first=Lyn|publisher=Random House |year=2013 |isbn=9780091940683 |chapter=Bertha Bracey}}

* {{citation |url=http://www.geo.brown.edu/BrownNASADataCenter/StoatleyRough/srexhibition/founding/founding.html |title=The Founding of Stoatley Rough School |publisher=Stoatley Rough School Historical Trust |year=2004 |ref={{harvid|SRS|2004}}}}

* {{citation |last=Taylor |first=Jennifer |year=2009 |publisher=Research Centre for German and Austrian Exile Studies, University of London |title=The Missing Chapter: How the British Quakers Helped to Save the Jews of Germany and Austria from Nazi Persecution |url=http://remember.org/unite/quakers.htm}}

* {{citation |last=Williams |title=Jews and Other Foreigners: Manchester and the Rescue of the Victims of European Fascism, 1933–40 |first=Bill |publisher=Oxford University Press |year=2013 |isbn=9780719089954}}

* {{citation |last=Wolfenden |title=Little Holocaust Survivors: And the English School that Saved Them |first=Barbara |publisher=Greenwood World |year=2008 |isbn=9781846450532}}

* {{citation |last=Woodford |url=http://journeys.quaker.org.uk/files/Issue%2057%20Bertha%20Bracey%20main.pdf |journal=Journeys in the Spirit |number=57 |year=2011 |title=Bertha Bracey – helping children to a safe home |first=Jane |publisher=Quaker Life}}


==Publications==
* {{citation |first=Bertha |last=Bracey |title=Europe's Displaced Persons and the Problems of Relocation |journal=International Affairs |publisher=Royal Institute of International Affairs |volume=20 |number=2 |year=1944 |pages=225–243 |doi=10.2307/3018099 |url=http://www.jstor.org/stable/3018099}}

{{Authority control}}

{{DEFAULTSORT:Bracey, Bertha}}
[[Category:1893 births]]
[[Category:1989 deaths]]
[[Category:Alumni of the University of Birmingham]]
[[Category:People from Bournville]]
[[Category:English Quakers]]
[[Category:Rescue of Jews in the Holocaust]]
[[Category:Members of the Order of the British Empire]]