{{For|the military use of the facility during and after the [[Vietnam War]]s|Tan Son Nhut Air Base}}
{{refimprove article|date=June 2016}}
{{Use dmy dates|date=May 2014}}
<!--{{DISPLAYTITLE:Tân Sơn Nhất International Airport}}-->
{{Infobox airport
| nativename = {{small|{{lang|vi|''Sân bay Quốc tế Tân Sơn Nhất''}}}}
| image2-width = 250
| location= [[Tan Binh District]]
| hub=
<div>
* [[Jetstar Pacific Airlines]]
* [[Vietnam Air Service Company|VASCO]]
* [[VietJet Air]]
* [[Vietnam Airlines]]
</div>
| elevation-m = 10
| metric-elev = y
| coordinates = {{coord|10|49|08|N|106|39|07|E|region:VN-SG|display=inline,title}}
| pushpin_label          = '''SGN'''
| pushpin_label_position = top
| r1-surface = Concrete
| metric-rwy = y
| name       = Tân Sơn Nhất International Airport
| image        = File:TIA-logo.jpg
| image-width  = 250
| image2       = File:Tan Son Nhat International Airport.jpg
| IATA= SGN
| ICAO= VVTS
| type= Public / Military
| owner= [[Government of Vietnam|Vietnamese government]]
| operator= [[Airports Corporation of Vietnam]]
| city-served= [[Ho Chi Minh City]], Vietnam
| website= [http://tansonnhatairport.vn/ http://tansonnhatairport.vn]
| pushpin_map            = Vietnam
| pushpin_map_caption    = Location of the airport in Vietnam
| r1-number  = 07L/25R
| r1-length-m= 3,050
| r2-number  = 07R/25L
| r2-length-m= 3,800
| r2-surface = Concrete
| stat-year   = 2016
| stat1-header= Total passengers
| stat1-data  = 32,486,537 {{increase}} 22.36%
| stat2-header= International passengers
| stat2-data  = 10,304,879 {{increase}} 12.9%
| stat3-header= Aircraft movements
| stat3-data  = 181,201 {{increase}} 18.0%
| stat4-header= Airfreight (tonne)
| stat4-data  = 930,627 {{increase}} 4.52%
| footnotes   = Source: ACV <ref name="TSN Stats15">{{cite web|title=2015 Statistics of ACV|url=http://vietnamairport.vn/page/2040/tin-tuc-va-su-kien/ket-qua-san-xuat-kinh-doanh-cua-acv-nam-2015-san-luong-hanh-khach-thong-qua-cang-dat-tren-63-trieu-luot-tang-242-so-voi-nam-2014|website=vietnamairport.vn|publisher=Airports Corporation of Vietnam|accessdate=19 January 2016}}</ref><ref name="acv_2016">{{cite web|title=ACV: Hội nghị tổng kết công tác năm 2016 và triển khai kế hoạch năm 2017|url=http://vietnamairport.vn/tin-tuc/hoat-dong-acv/acv-hoi-nghi-tong-ket-cong-tac-nam-2016-va-trien-khai-ke-hoach-nam-2017|website=ACV|accessdate=25 January 2017|date=20 January 2017}}</ref>
}}

'''Tân Sơn Nhất International Airport''' {{airport codes|SGN|VVTS}} ({{lang-vi|Sân bay quốc tế Tân Sơn Nhất}}, {{lang-vi|Cảng hàng không quốc tế Tân Sơn Nhất}}) is the busiest [[airport]] in [[Vietnam]] with 32.5 million passengers in 2016, <ref>{{Cite news|url=http://vietnamairport.vn/tin-tuc/hoat-dong-acv/acv-hoi-nghi-tong-ket-cong-tac-nam-2016-va-trien-khai-ke-hoach-nam-2017|title=ACV: Hội nghị tổng kết công tác năm 2016 và triển khai kế hoạch năm 2017|language=vi-vn|access-date=2017-01-20}}</ref> serving [[Ho Chi Minh City]] as well as the rest of [[Southeast (Vietnam)|southeastern Vietnam]]. As of January 2017, it had a total capacity of only 25 million passengers,<ref>{{Cite news|url=http://cafef.vn/mo-rong-tan-son-nhat-tai-sao-khong-20170120083144495.chn|title=Mở rộng Tân Sơn Nhất: Tại sao không?|date=2017-01-20|language=vi|access-date=2017-01-20}}</ref> which has caused constant congestion and sparked debate for expanding or building a new airport. The airport's [[IATA]] code, '''SGN''', is derived from the city's former name of [[Saigon]].

==History==
Tan Son Nhat International Airport has its origins in the early 1930s, when the French colonial government constructed a small airport with unpaved runways, known as '''Tân Sơn Nhất Airfield''' near the village of Tan Son Nhat. By mid-1956, with U.S. aid, a {{convert|7200|ft|m|-1|sing=on}} runway had been built and the airfield near [[Saigon]] became known as [[South Vietnam]]'s principal international gateway. During the [[Vietnam War]] (or Second Indochina War), [[Tan Son Nhut Air Base]] (then using the alternative spelling '''Tân Sơn Nhứt''') was an important facility for both the [[U.S. Air Force]] and the [[South Vietnamese Air Force]]. Between 1968 and 1974, Tan Son Nhut Airport was one of the busiest military airbases in the world. During the last days of South Vietnam, [[Pan American World Airways|Pan Am]] schedules from 1973 showed [[Boeing 747]] service was being operated four times a week to San Francisco via [[Guam]] and [[Manila]].<ref>[http://www.departedflights.com/PA042973p30.html Pan Am System Timetable, April 29, 1973]</ref> [[Continental Airlines]] operated up to 30 [[Boeing 707]] military charters per week to and from Tan Son Nhut Airport during the 1968–74 period.<ref name="Scott">Christian, J. Scott, former Continental employee and manager, Bring Songs to the Sky: Recollections of Continental Airlines, 1970–1986, Quadran Press, 1998.</ref>

===Post-war era===
On 9 December 2004, [[United Airlines]] became the first U.S. airline to fly to Vietnam since [[Pan American World Airways|Pan Am]]'s last flight during the [[Fall of Saigon]] in April 1975. Flight UA 869, operated using a [[Boeing 747-400]] landed at Ho Chi Minh City, the terminus of the flight that originated from San Francisco via Hong Kong. On 29 October 2006, this service was switched from San Francisco to Los Angeles with a stop in Hong Kong, operating as UA 867 (also using a Boeing 747-400). In 2009, the service UA 869 has resumed once again from San Francisco via [[Hong Kong International Airport]].<ref>[http://www.united.com/page/article/0,6722,1891,00.html?navSource=Dropdown07&linkTitle=timetables United Airlines – Flight Timetables, Download to PC, PDA or Blackberry<!-- Bot generated title -->]</ref> United ceased the route to San Francisco via Hong Kong on 30 October 2011. The airline resumed the route from Ho Chi Minh City to Hong Kong after its merger with [[Continental Airlines]]. The flight no longer makes a stop at San Francisco and it was flown on a Boeing 777-200ER instead of a Boeing 747-400.

In 2006, Tan Son Nhat International Airport served approximately 8.5 million passengers (compared with 7 million in 2005) with 64,000 aircraft movements.<ref>[http://www.tsnairport.hochiminhcity.gov.vn/vn/default.aspx?cat_id=717&news_id=605&cl1=695#content Official number from Tan Son Nhat Airport Authority at its official website]</ref> It has recently accounted for nearly two-thirds of the arrivals and departures at Vietnam's international gateway airports.<ref name=autogenerated2>Two more Hanoi<>Saigon flights per day for Pacific Airlines on "Vietnamnet.net, access date 11 November 2007, {{vi icon}} [http://vietnamnet.vn/kinhte/2007/11/753468/]</ref><ref name=autogenerated1>[http://www.mt.gov.vn/default.asp?op=tintuc&catid=2&newsid=1381 News about Tan Son Nhat International Airport on Official Website of Ministry of Transport of Vietnam, 12 November 2007, Vietnamese] {{webarchive |url=https://web.archive.org/web/20071112174458/http://www.mt.gov.vn/default.asp?op=tintuc&catid=2&newsid=1381 |date=12 November 2007 }}</ref> Due to increasing demand (about 15–20% per annum), the airport has been continuously expanded by the Southern Airports Corporation.<ref name=autogenerated1 />

Tan Son Nhat International Airport served 12.4 million passengers in 2008, compared to 11 million in 2007 and 15.5 million passenger in 2010. In 2010, the domestic terminal handled 8 million passengers which reached its maximum capacity. The airport reached its full capacity of 20 million passengers in 2013, two years earlier than predicted. Both domestic and international terminal are being expanded to meet the increasing demand. In December 2014, expansion for the domestic terminal was finished, boosting the terminal's capacity to 13 million passengers per annum.<ref name="TSN Expansion">{{cite web|first1=Thái Phương|title=Sân bay Tân Sơn Nhất tăng năng lực phục vụ|url=http://nld.com.vn/thoi-su-trong-nuoc/san-bay-tan-son-nhat-tang-nang-luc-phuc-vu-20150104211358545.htm|website=nld.com.vn|publisher=Người Lao Động|accessdate=7 January 2015}}</ref> The expansion plan also includes increasing the capacity of the international terminal up to 13 million passengers per year, 10 new parking spaces and a new taxiway. After extension, planned to be completed in 2015, Tan Son Nhat will be able to handle 25 million passengers per year with space for 60 aircraft.<ref>{{cite web|last=Lê|first=Anh|title=Tân Sơn Nhất có thêm đất làm sân đỗ máy bay|url=http://www.thesaigontimes.vn/home/dothi/hatang/112856/|publisher=Thời báo Kinh tế Sài Gòn|accessdate=30 April 2014}}</ref>

===International terminal===
[[File:new-tsn.jpg|200px|thumb|Passport check in the international terminal (August 2007)]]

A new international terminal funded by Japanese [[official development assistance]] and constructed by a consortium of four Japanese contractors (KTOM, abbreviation of four contractors' names: [[Kajima]] – [[Taisei Corporation|Taisei]] – [[Obayashi Corporation|Obayashi]] – [[Maeda Corporation|Maeda]]), opened in September 2007 with a capacity for 12 million passengers a year. The new terminal gives the airport a total annual capacity of 20 million passengers. The old terminal is now used for domestic flights.<ref name="tansonnhatairport">[http://www.tsnairport.hochiminhcity.gov.vn/vn/default.aspx?cat_id=717&news_id=605&cl1=695#content Official website of Tan Son Nhat International Airport]</ref> After 2025, when [[Long Thanh International Airport]] is completed, Tan Son Nhat will remain operational; however, it will mostly serve for domestic flights, a few international flights and no longer serve transit flights.

==Facilities==
Following the opening of its new international terminal in September 2007, Tan Son Nhat has two major terminal buildings with separate sections for international and domestic flights. The capacity of the new terminal, once fully completed, will be 8 million passengers per annum. When [[Long Thanh International Airport]] is completed, Tan Son Nhat will serve domestic passengers only.

The [[Prime Minister of Vietnam]], by Decision 1646/TTg-NN, has approved the addition of {{convert|40|ha|acre|0}} of adjacent area to extend the apron and to build a cargo terminal to handle the rapid increase of passenger (expected to reach 17 million in 2010, compared to 7 million and 8.5 million in 2005 and 2006 respectively) and cargo volume at the airport.<ref name=autogenerated2 /><ref>Two more Hanoi–Saigon flights per day for Pacific Airlines on "Vietnamnet.net, access date 11 November 2007, {{en icon}} [http://english.vietnamnet.vn/travel/2007/11/753609/]</ref>

Bus lines 152 and 109 (direct route) currently connect the airport to city center. Express minibus serves connections to [[Vung Tau]] and other cities in Mekong Delta.

==Airlines and destinations==
[[File:Vietnam TanSonNhat Airport Check-In.jpg|thumb|right|Check-in desks at terminal 2, Tan Son Nhat International Airport]]
[[File:Tan Son Nhat International Airport Level 3 Concourse.jpg|thumb|right|Level 3 of terminal 2, Tan Son Nhat International Airport]]
[[File:Tân_Sơn_Nhất_Business.JPG|thumb|right|Business lounge of Tan Son Nhat International Airport]]
[[File:Vietnam Airlines Boeing 777-200ER VN-A141 SGN 2008-4-6.png|thumb|right|[[Vietnam Airlines]] [[Boeing 777|Boeing 777-200ER]] taxiing at Tan Son Nhat International Airport]]
[[File:Vietnam Airlines Airbus A350-941 VN-A886.JPG|thumb|[[Vietnam Airlines]] [[A350-900]] taking off at Tan Son Nhat International Airport]]

===Passenger===
{{Airport-dest-list
|3rdcoltitle = Terminal
<!-- -->
| [[Aeroflot]]| [[Moscow–Sheremetyevo]] | 2
<!-- -->
| [[Air Astana]]| [[Almaty International Airport|Almaty]] | 2
<!-- -->
| [[Air China]]| [[Beijing Capital International Airport|Beijing–Capital]] | 2
<!-- --> 
| [[Air France]]| [[Paris–Charles de Gaulle]] | 2
<!-- -->
| [[AirAsia]]| [[Senai International Airport|Johor Bahru]], [[Kuala Lumpur International Airport|Kuala Lumpur–International]], [[Penang International Airport|Penang]] | 2
<!-- -->
| [[Air New Zealand]]| '''Seasonal:''' [[Auckland Airport|Auckland]] | 2
<!-- -->
| [[All Nippon Airways]]<br>{{nowrap|operated by [[Air Japan]]}}| [[Tokyo–Narita]] | 2
<!-- -->
| [[Asiana Airlines]]| [[Incheon International Airport|Seoul–Incheon]] | 2
<!-- -->
| {{Nowrap|[[Cambodia Angkor Air]]}}| [[Phnom Penh International Airport|Phnom Penh]], [[Siem Reap International Airport|Siem Reap]], [[Sihanoukville International Airport|Sihanoukville]] | 2
<!-- -->
| [[Cathay Pacific]]| [[Hong Kong International Airport|Hong Kong]] | 2
<!-- -->
| [[Cebu Pacific]]| [[Ninoy Aquino International Airport|Manila]] | 2
<!-- -->
| [[China Airlines]]| [[Taiwan Taoyuan International Airport|Taipei–Taoyuan]] | 2
<!-- -->
| [[China Eastern Airlines]]| [[Kunming Changshui International Airport|Kunming]], [[Shanghai–Pudong]] | 2
<!-- -->
| {{nowrap|[[China Southern Airlines]]}}| [[Beijing Capital International Airport|Beijing–Capital]],<ref>http://www.routesonline.com/news/38/airlineroute/271304/china-southern-adds-beijing-ho-chi-minh-city-route-in-march-2017/</ref> [[Guangzhou Baiyun International Airport|Guangzhou]], [[Shanghai Pudong International Airport|Shanghai–Pudong]], [[Shenzhen Bao'an International Airport|Shenzhen]] | 2
<!-- -->
| [[Emirates (airline)|Emirates]]| [[Dubai International Airport|Dubai–International]] | 2
<!-- -->
| [[Etihad Airways]]| [[Abu Dhabi International Airport|Abu Dhabi]] | 2
<!-- -->
| [[EVA Air]]| [[Taiwan Taoyuan International Airport|Taipei–Taoyuan]] | 2
<!-- -->
| [[Finnair]] | '''Seasonal:''' [[Helsinki Airport|Helsinki]] | 2
<!-- -->
| [[Japan Airlines]]| [[Tokyo–Haneda]], [[Tokyo–Narita]] | 2
<!-- -->
| [[Jetstar Airways]]| [[Melbourne Airport|Melbourne]] (begins 10 May 2017), [[Sydney Airport|Sydney]] (begins 11 May 2017)<ref>{{cite news|url=http://australianaviation.com.au/2017/01/jetstar-to-serve-ho-chi-minh-city-nonstop-from-may/|title=Jetstar to serve Ho Chi Minh City nonstop from May|publisher=Australian Aviation|date=19 January 2017}}</ref> | 2
<!-- -->
| [[Jetstar Asia Airways]]| [[Singapore Changi Airport|Singapore]] | 2
<!-- -->
| [[Jetstar Pacific Airlines]]| [[Buon Ma Thuot Airport|Buon Ma Thuot]], [[Chu Lai Airport|Chu Lai]], [[Da Nang International Airport|Da Nang]], [[Cat Bi International Airport|Hai Phong]], [[Noi Bai International Airport|Hanoi]], [[Vinh Airport|Vinh]], [[Da Lat Airport|Da Lat]], [[Tho Xuan Airport|Thanh Hoa]], [[Phu Cat Airport|Quy Nhon]], [[Dong Hoi Airport|Dong Hoi]], [[Dong Tac Airport|Tuy Hoa]], [[Cam Ranh International Airport|Nha Trang]] | 1A
<!-- -->
| [[Jetstar Pacific Airlines]]| [[Suvarnabhumi Airport|Bangkok–Suvarnabhumi]], [[Guangzhou Baiyun International Airport|Guangzhou]], [[Hong Kong International Airport|Hong Kong]], [[Singapore Changi Airport|Singapore]] | 2
<!-- -->
| [[Korean Air]]| [[Incheon International Airport|Seoul–Incheon]] | 2
<!-- -->
| [[Lao Airlines]]| [[Pakse International Airport|Pakse]] | 2
<!--  --> 
| [[Malaysia Airlines]]| [[Kuala Lumpur International Airport|Kuala Lumpur–International]] | 2
<!-- -->
| [[Malindo Air]] | [[Kuala Lumpur International Airport|Kuala Lumpur–International]] | 2
<!-- -->
| [[Mandarin Airlines]]| [[Taichung Airport|Taichung]] | 2
<!-- -->
| [[Nok Air]]| [[Don Mueang International Airport|Bangkok–Don Mueang]] | 2
<!-- -->
| [[Philippine Airlines]]| [[Ninoy Aquino International Airport|Manila]] | 2
<!-- -->
| [[Qatar Airways]]| [[Hamad International Airport|Doha]], [[Phnom Penh International Airport|Phnom Penh]] | 2
<!--  -->
| [[Royal Brunei Airlines]]| [[Brunei International Airport|Bandar Seri Begawan]] | 2
<!--  -->
| [[Sichuan Airlines]]| [[Nanning Wuxu International Airport|Nanning]] | 2
<!-- -->  
| [[Singapore Airlines]]| [[Singapore Changi Airport|Singapore]] | 2
<!-- -->
| [[Thai AirAsia]]| [[Don Mueang International Airport|Bangkok–Don Mueang]] | 2
<!-- -->
| [[Thai Airways]]| [[Bangkok–Suvarnabhumi]] | 2
<!-- -->
| [[Thai Lion Air]]|  [[Don Mueang International Airport|Bangkok–Don Mueang]]| 2
<!-- -->
| [[Thai Vietjet Air]]| [[Suvarnabhumi Airport|Bangkok–Suvarnabhumi]] | 2
<!-- -->
| [[Tigerair]]| [[Singapore Changi Airport|Singapore]] | 2 
<!-- -->
| [[Turkish Airlines]]|<!-- Do not add Hanoi as Turkish Airlines will not have traffic rights from SGN to HAN. --> [[Istanbul–Atatürk]] | 2
<!-- --> 
|[[T'way Airlines]]| [[Incheon International Airport|Seoul–Incheon]] | 2
<!-- -->
| [[Vanilla Air]]| [[Taoyuan International Airport|Taipei–Taoyuan]], [[Narita International Airport|Tokyo–Narita]] | 2
<!-- -->
| [[Vietjet Air]]| [[Buon Ma Thuot Airport|Buon Ma Thuot]] [[Chu Lai Airport|Chu Lai]], [[Lien Khuong Airport|Da Lat]], [[Da Nang International Airport|Da Nang]], [[Dong Hoi Airport|Dong Hoi]], [[Cat Bi International Airport|Hai Phong]], [[Noi Bai International Airport|Hanoi]], [[Phu Bai International Airport|Hue]], [[Cam Ranh International Airport|Nha Trang]], [[Phu Quoc International Airport|Phu Quoc]], [[Pleiku Airport|Pleiku]], [[Phu Cat Airport|Qui Nhon]], [[Tho Xuan Airport|Thanh Hoa]], [[Vinh Airport|Vinh]]  | 1B
<!-- -->
| [[VietJet Air]]| [[Bangkok–Suvarnabhumi]], [[Hong Kong International Airport|Hong Kong]], [[Kaohsiung International Airport|Kaohsiung]], [[Kuala Lumpur International Airport|Kuala Lumpur–International]], [[Incheon International Airport|Seoul–Incheon]], [[Singapore Changi Airport|Singapore]], [[Taichung Airport|Taichung]], [[Tainan Airport|Tainan]], [[Taiwan Taoyuan International Airport|Taipei–Taoyuan]], [[Yangon International Airport|Yangon]] | 2
<!-- -->
| [[Vietnam Airlines]]| [[Buon Ma Thuot Airport|Buon Ma Thuot]], [[Lien Khuong Airport|Da Lat]], [[Da Nang International Airport|Da Nang]], [[Dong Hoi Airport|Dong Hoi]], [[Cat Bi International Airport|Hai Phong]], [[Noi Bai International Airport|Hanoi]], [[Phu Bai International Airport|Hue]], [[Cam Ranh International Airport|Nha Trang]], [[Phu Quoc International Airport|Phu Quoc]], [[Pleiku Airport|Pleiku]], [[Phu Cat Airport|Qui Nhon]], [[Rach Gia Airport|Rach Gia]], [[Tho Xuan Airport|Thanh Hoa]], [[Vinh Airport|Vinh]] | 1A
<!-- -->
| [[Vietnam Airlines]]<br>{{nowrap|operated by [[Vietnam Air Service Company|VASCO]]}} | [[Cà Mau Airport|Ca Mau]], [[Chu Lai International Airport|Chu Lai]], [[Con Dao Airport|Con Dao]], [[Dong Tac Airport|Tuy Hoa]] | 1A
<!-- --> 
| [[Vietnam Airlines]]| [[Bangkok–Suvarnabhumi]], [[Chengdu Shuangliu International Airport|Chengdu]], [[Gimhae International Airport|Busan]], [[Frankfurt Airport|Frankfurt]], [[Fukuoka Airport|Fukuoka]], [[Guangzhou Baiyun International Airport|Guangzhou]], [[Hong Kong International Airport|Hong Kong]], [[Soekarno–Hatta International Airport|Jakarta–Soekarno–Hatta]], [[Kaohsiung International Airport|Kaohsiung]], [[Kuala Lumpur International Airport|Kuala Lumpur–International]], [[London–Heathrow]], [[Melbourne Airport|Melbourne]], [[Chūbu Centrair International Airport|Nagoya–Centrair]], [[Osaka–Kansai]], [[Paris–Charles de Gaulle]], [[Phnom Penh International Airport|Phnom Penh]], [[Incheon International Airport|Seoul–Incheon]], [[Shanghai–Pudong]], [[Siem Reap International Airport|Siem Reap]], [[Singapore Changi Airport|Singapore]], [[Sydney Airport|Sydney]], [[Taiwan Taoyuan International Airport|Taipei–Taoyuan]], [[Tokyo–Narita]], [[Yangon International Airport|Yangon]] | 2
<!-- -->
| [[XiamenAir]]| [[Xiamen Gaoqi International Airport|Xiamen]] | 2
}}

<small>{{note|1|1}} [[Turkish Airlines]]'s flight from Ho Chi Minh City to Istanbul make a stop or continue on to Hanoi. Turkish Airlines, however, does not have eighth freedom traffic rights to transport passengers solely from Ho Chi Minh City to Hanoi.</small>

===Cargo===
{{Airport-dest-list
|3=[[Air Hong Kong]]|4=[[Hong Kong International Airport|Hong Kong]], [[Penang International Airport|Penang]]
<!-- -->|5=[[Cargolux]]|6=[[Luxembourg Findel Airport|Luxembourg]] 
<!-- -->|7=[[Cathay Pacific]]|8=[[Soekarno–Hatta International Airport|Jakarta–Soekarno–Hatta]], [[Hong Kong International Airport|Hong Kong]]
<!-- -->|9={{nowrap|[[China Airlines Cargo]]}}|10=[[Abu Dhabi International Airport|Abu Dhabi]], [[Bangkok–Suvarnabhumi]], [[Luxembourg Findel Airport|Luxembourg]], [[Singapore Changi Airport|Singapore]], [[Taiwan Taoyuan International Airport|Taipei–Taoyuan]]
<!-- -->|11=[[FedEx Express]]|12=[[Guangzhou Baiyun International Airport|Guangzhou]], [[Noi Bai International Airport|Hanoi]], [[Soekarno–Hatta International Airport|Jakarta–Soekarno–Hatta]]
<!-- -->|13=[[Korean Air Cargo]]|14=[[Soekarno–Hatta International Airport|Jakarta–Soekarno–Hatta]], [[Incheon International Airport|Seoul–Incheon]]
<!-- -->|15=[[MASkargo]]|16=[[Kuala Lumpur International Airport|Kuala Lumpur–International]], [[Labuan Airport|Labuan]], [[Suvarnabhumi Airport|Bangkok–Suvarnabhumi]] 
<!-- -->|17=[[Raya Airways]]|18=[[Labuan Airport|Labuan]] 
<!-- -->}}

==Statistics==
{{refimprove section|date=October 2012}}
{{Col-begin|width=100%}}
{{Col-1-of-2}}

{| class="wikitable sortable" style="font-size:89%; align=center;"
|+ '''Busiest international flights Out of Tan Son Nhat International Airport by Frequency'''
|- style="background:darkgrey;"
! |Rank || |Destinations || |Frequency (Weekly)
|-
| 1
|  Singapore
| 112
|-
| 2
| Bangkok (Don Mueang + Suvarnabhumi)
| 112
|-
| 3
|  Kuala Lumpur
| 77
|-
| 4
|  Siem Reap
| 42
|-
| 5
|  Tokyo
| 40
|-
| 6
|  Taipei
| 63
|-
| 7
| Hong Kong
| 35
|-
| 7
| Seoul
| 63
|-
| 9
| Guangzhou
| 35
|-
| 10
|  Phnom Penh
| 21
|-
| 10
|  Shanghai–Pudong
| 21
|-
|-
| 13
|  Manila
| 14
|-
| 13
| Jakarta
| 14
|-
| 15
|  Vientiane
| 11
|-
| 16
| Kaohsiung
| 10
|-
| 18
| Taichung
| 9
|-
| 18
| Moscow (Domodedovo + Sheremetyevo)
| 9 
|-
| 19
| Paris
| 8
|-
| 20
| Sydney
| 7
|-
| 20
|  Doha
| 7 
|-
| 21
| Dubai
| 7
|-
| 21
| Frankfurt
| 7
|-
| 21
|  Melbourne
| 7
|-
| 21
|  Johor Bahru
| 7
|-
| 21
| Busan
| 7
|-
| 21
| Osaka
| 7
|-
| 21
| Abu Dhabi
| 7
|-
| 22
|  Penang
| 4
|-
| 23
| Auckland
| 3
|-
| 24
| London
| 2
|-
| 24
| Almaty
| 2
|-
| 25
|Helsinki
|1
|}
{{Col-2-of-2}}

{| class="wikitable sortable" style="font-size:89%; align=center;"
|+ '''Busiest domestic flights Out of Tan Son Nhat International Airport by Frequency'''
|- style="background:darkgrey;"
! |Rank || |Destinations || |Frequency (Weekly)
|-
| 1
| Hanoi
| 420
|-
| 2
| Da Nang
| 238
|-
| 3
| Hai Phong
| 77
|-
| 4
| Phu Quoc
| 84
|-
| 4
| Vinh
| 63
|-
| 6
| Buon Ma Thuot
| 39
|-
| 7
| Con Dao
| 42
|-
| 7
| Hue
| 56
|-
| 9
| Pleiku
| 32
|-
| 9
| Nha Trang
| 84
|-
| 11
| Da Lat
| 49
|-
| 11
| Qui Nhon
| 42
|}
{|class="toccolours sortable" style="padding:0.5em;font-size: 87.5%" 
|+ '''Operational statistics of Tan Son Nhat International Airport'''<ref name="TSN Stats">{{cite web|title=2005-2014 Statistics of Tan Son Nhat Intl Airport|url=http://www.caa.mt.gov.vn/Default.aspx?tabid=0&catid=419.435&articleid=10812|website=www.caa.mt.gov.vn|publisher=Civil Aviation Authority of Vietnam|accessdate=22 January 2015}}</ref><ref name="acv_2016"/>
|-
! | <center>Year</center>
! | <center>Passengers<br />handled</center>
! | <center>Passenger<br />% Change</center>
! | <center>Cargo<br />(tonnes)</center>
! | <center>Cargo<br />% Change</center>
! | <center>Aircraft<br />Movements</center>
! | <center>Aircraft<br />% Change</center>
|- style="background:#eee;"
|2005||7,368,420||{{increase}} 17.3||192,781||{{increase}} 6.5||59,395||{{increase}} 10.1
|- style="background:#eee;"
|2006||8,493,698||{{increase}} 15.3||217,486||{{increase}} 12.8||64,182||{{increase}} 8.1
|- style="background:#eee;"
|2007||10,240,813||{{increase}} 20.6||252,528||{{increase}} 16.1||75,585||{{increase}} 17.8
|- style="background:#eee;"
|2008||11,765,467||{{increase}} 14.9||274,251||{{increase}} 8.6||86,533||{{increase}} 14.5
|- style="background:#eee;"
|2009||12,778,554||{{increase}} 8.6||273,965||{{decrease}} 0.1||94,694||{{increase}} 9.4
|- style="background:#eee;"
|2010||15,107,927||{{increase}} 18.2||340,504||{{increase}} 24.3||109,324||{{increase}} 15.4
|- style="background:#eee;"
|2011||16,725,974||{{increase}} 10.7||333,777||{{decrease}} 2.0||127,471||{{increase}} 16.6
|- style="background:#eee;"
|2012||17,508,317||{{increase}} 4.7||340,221||{{increase}} 1.9||132,481||{{increase}} 3.9
|- style="background:#eee;"
|2013||20,030,773||{{increase}} 14.4||372,823||{{increase}} 9.6||140,091||{{increase}} 5.7
|- style="background:#eee;"
|2014||22,140,348||{{increase}} 10.5||408,006||{{increase}} 9.4 ||154,378||{{increase}} 10.2
|- style="background:#eee;"
|2015||26,549,829||{{increase}} 19.85||430,627||{{increase}} 4.52 ||181,701||{{increase}} 18.0
|- style="background:#eee;"
|2016||32,486,537||{{increase}} 22.36||N/A||N/A ||N/A||N/A
|}

{{Col-end}}

==The airport's future==
{{Refimprove section|date=November 2010}}
Tan Son Nhat International Airport is located inside the crowded city of Ho Chi Minh City, making expansions difficult.

Following a recent decision by the Vietnamese Prime Minister, a new airport—[[Long Thanh International Airport]]—will replace Tan Son Nhat airport for international departure use. The master plan for the new airport was approved in April 2006. The new airport will be built in Long Thanh county, Dong Nai province, about {{convert|50|km|mi|0|abbr=on}} northeast of Ho Chi Minh City and {{convert|70|km|mi|0|abbr=on}} northwest of the petroleum-focused city of Vung Tau, near Highway 51A.

Long Thanh International Airport will be constructed on an area of {{convert|50|km2|sqmi|0|sp=us}}, and will have four runways (4,000&nbsp;m x 60&nbsp;m or 13,100&nbsp;ft x 200&nbsp;ft) and be capable of receiving the [[Airbus A380]]. The project will be divided in two stages. Stage One calls for the construction of two parallel runways and a terminal with a capacity of 20 million passengers per year, due to be completed in 2025. Stage Two is scheduled for completion in 2035, giving the airport with three passenger terminals and a cargo terminal designed to receive 5 million metric tons of cargo per year. The total invested capital of this project is an estimated US$8 billion.

Upon completion of Long Thanh International Airport, Tan Son Nhat Airport will serve domestic passengers only. Long Thanh International Airport is expected to be the leading airport on the Indochinese peninsula, and one of the busiest air transportation hubs in the southeast Asian region.<ref>[http://pilot.vn/?mod=news&page=view&id=9380 Củng cố luận chứng xây dựng sân bay Long Thành, Tiên Lãng – Website thông tin Hàng không & Cuộc sống<!-- Bot generated title -->]</ref>

==See also==
{{Portal|Vietnam|Aviation}}

* [[Bombing of Tan Son Nhut Air Base]]
* [[Da Nang International Airport]]
* [[Long Thanh International Airport]]
* [[Noi Bai International Airport]]
* [[List of airports in Vietnam]]

==References==
{{Reflist}}

==External links==
{{Commons category|Tân Sơn Nhất International Airport}}
* [http://www.tsnairport.hochiminhcity.gov.vn/ Tan Son Nhat Airport Official Website]
* [http://www.sac.vn/ Southern Airports Corporation Official Website (SAC)]
* [http://www.sags-saa.com/ Saigon Ground Services official website, a subsidiary of SAC]
* [http://www.tiags.com.vn:8080/ Tan Son Nhat International Airport Ground Services (TIAGS) official website; a subsidiary of Vietnam Airlines]
* [http://www.vietnamonline.com/guide/transportation/airports/tan-son-nhat-international-airport-sgn.html Tan Son Nhat airport – Terminals]
* {{WAD|VVTS}}
* [http://nz.news.yahoo.com/a/-/world/5105664 News Item on Fire at Airport on Monday 27 October 2008]

{{Airports in Vietnam}}

{{DEFAULTSORT:Tan Son Nhat International Airport}}
[[Category:Airports in Vietnam]]
[[Category:Transport in Ho Chi Minh City]]
[[Category:Buildings and structures in Ho Chi Minh City]]