{{taxobox
| name = ''Disinae''
| image = 
| image_width = 
| image_caption = 
| regnum = [[Plantae]]
| unranked_divisio = [[Angiosperms]]
| unranked_classis = [[Monocots]]
| ordo = [[Asparagales]]
| familia = [[Orchidaceae]]
| subfamilia = [[Orchidoideae]]
| tribus = [[Diseae]]
| subtribus = Disinae
| subtribus_authority = {{Au|[[George Bentham|Bentham]] 1881}}
| subdivision_ranks = Genera
| subdivision = 
* ''[[Disa (plant)|Disa]]''
* ''[[Schizodium]]''
}}
'''Disinae''' is a [[subtribe]] of [[Orchidaceae|orchids]] that has been differently [[Circumscription (taxonomy)|defined]] and [[Phylogenetic tree|placed]] in the two [[History of plant systematics|classification systems]] that are currently in use for orchids. ''Genera Orchidacearum'', which is currently the definitive [[Treatise|work]] on orchid [[Plant taxonomy|taxonomy]], delimits Disinae as consisting of two closely [[Affinity (taxonomy)|related]] [[genera]], ''[[Disa]]'' and ''[[Schizodium]]'', and it places Disinae in the mostly [[Africa]]n [[Tribe (biology)|tribe]] [[Diseae]], along with four other subtribes: [[Brownleeinae]], [[Huttonaeinae]], [[Coryciinae]], and [[Satyriinae]].<ref name=pridgeon6>Alec M. Pridgeon, Phillip J. Cribb, Mark W. Chase, and Finn N. Rasmussen. 1999-2014. ''Genera Orchidacearum''  Oxford University Press. ISBN 978-0-19-850513-6 (volume 1), ISBN 978-0-19-850710-9 (volume 2), ISBN 978-0-19-850711-6 (volume 3), ISBN 978-0-19-850712-3 (volume 4), ISBN 978-0-19-850713-0 (volume 5), ISBN 978-0-19-964651-7 (volume 6)</ref> In the [[Taxonomy (biology)|classification]] for orchids that was [[Academic publication|published]] by [[Mark Wayne Chase|Chase]] [[et alii]] in 2015, ''Schizodium'' was placed in [[Synonym (botany)|synonymy]] under ''Disa'', while ''[[Pachites]]'' and ''[[Huttonaea]]'' were transferred to Disinae.<ref name=chase2015>Mark W. Chase, Kenneth M. Cameron, John V. Freudenstein, Alec M. Pridgeon, Gerardo A. Salazar, Cássio van den Berg, and André Schuiteman. 2015. "An updated classification of Orchidaceae". ''Botanical Journal of the Linnean Society'' '''177'''(2):151-174. (See ''External links'' below).</ref> In ''Genera Orchidacearum'', ''Pachites'' and ''Satyrium'' form the subtribe Satyriinae, and ''Huttonaea'' is the sole genus in the subtribe Huttonaeinae. The transfer of ''Pachites'' and ''Huttonaea'' to Disinae by Chase et alii (2015) was done with considerable doubt, and was based upon uncertainty about the relationships of these two genera. In 2009, a [[molecular phylogenetic]] [[Research|study]] found only weak [[Resampling (statistics)|statistical support]] for a [[Sister taxon|sister]] relationship between ''Huttonaea'' and ''Disa''.<ref name=waterman2009>Richard J. Waterman, Anton Pauw, Timothy G. Barraclough, and Vincent Savolainen. 2009. "Pollinators underestimated: A molecular phylogeny reveals widespread floral convergence in oil-secreting orchids (sub-tribe Coryciinae) of the Cape of South Africa". ''Molecular Phylogenetics and Evolution'' '''51'''(1):100-110. {{doi|10.1016/j.ympev.2008.05.020}}.</ref>

Disinae was covered, along with the rest of the tribe Diseae, in volume 2 of ''Genera Orchidacearum'', which was published in 2001. Molecular phylogenetic studies published after 1999 showed that Diseae was [[paraphyletic]] over another tribe, [[Orchideae]].<ref name=douzery1999>Emmanuel J. P. Douzery, Alec M. Pridgeon, Paul Kores, H. P. Linder, Hubert Kurzweil, and Mark W. Chase. 1999. "Molecular phylogenetics of Diseae (Orchidaceae): a contribution from nuclear ribosomal ITS sequences". ''American Journal of Botany'' '''86'''(6):887-899. [http://www.amjbot.org/content/86/6/887.full.pdf+html PDF]</ref> Consequently, Chase et alii (2015) combined Diseae and Orchideae into a larger version of Orchideae and included Disinae as one of its subtribes.

The subtribe Disinae was erected by [[George Bentham]] in 1881,<ref name="ipni">Disinae in [[International Plant Names Index]]. (see ''External links'' below).</ref><ref name=bentham1881>George Bentham. 1881. page 288. In: "Notes on Orchideae". ''The Journal of the Linnean Society. Botany''. 18(110):281-367. (See ''External links'' below).</ref> in preparation for the publication of a new classification of orchids in the 1883 edition of [[Genera Plantarum (disambiguation)|''Genera Plantarum'' (Bentham & Hooker)]].<ref name=b&h1883>George Bentham and Joseph Dalton Hooker. 1883. ''Genera Plantarum'' (Bentham & Hooker) volume 3, part 2, pages 460-488. L.Reeve & Co.; Williams & Norgate: London, UK. (See ''External links'' below).</ref> Using the [[suffix]]es of that time for [[taxonomic rank]], he called it subtribe "Diseae" of his now obsolete tribe Ophrydeae.

== References ==
{{reflist}}

== External links ==
* [http://onlinelibrary.wiley.com/doi/10.1111/boj.12234/epdf An updated classification of Orchidaceae (2015)] {{color|green|At:}} [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1095-8339 Botanical Journal of the Linnean Society] {{color|green|At:}} [http://onlinelibrary.wiley.com/ Wiley Online Library]
* [http://www.ipni.org/ipni/advPlantNameSearch.do?find_family=&find_genus=&find_species=&find_infrafamily=Disinae+&find_infragenus=&find_infraspecies=&find_authorAbbrev=&find_includePublicationAuthors=on&find_includePublicationAuthors=off&find_includeBasionymAuthors=on&find_includeBasionymAuthors=off&find_publicationTitle=&find_isAPNIRecord=on&find_isAPNIRecord=false&find_isGCIRecord=on&find_isGCIRecord=false&find_isIKRecord=on&find_isIKRecord=false&find_rankToReturn=all&output_format=normal&find_sortByFamily=on&find_sortByFamily=off&query_type=by_query&back_page=plantsearch Disinae] {{color|green|At:}} [http://www.ipni.org/ipni/plantnamesearchpage.do Plant Names] {{color|green|At:}} [http://www.ipni.org/ IPNI]
* [http://www.biodiversitylibrary.org/item/8373#page/295/mode/1up page 288] {{color|green|In:}} [http://www.biodiversitylibrary.org/item/8373#page/1/mode/1up volume 18] {{color|green|Of:}} [http://www.biodiversitylibrary.org/bibliography/350#/summary ''The Journal of the Linnean Society. Botany''] {{color|green|At:}} [http://www.biodiversitylibrary.org/browse/titles/j#/titles titles] {{color|green|At:}} [http://www.biodiversitylibrary.org/ BHL]
* [http://www.biodiversitylibrary.org/item/14690#page/21/mode/1up orchids, page 460] {{color|green|At:}} [http://www.biodiversitylibrary.org/item/14690#page/1/mode/1up View Book] {{color|green|At:}} [http://www.biodiversitylibrary.org/bibliography/747#/summary ''Genera Plantarum'' (Bentham & Hooker, 1883) volume 3, part 2] {{color|green|At:}} [http://www.biodiversitylibrary.org/browse/titles/g#/titles titles] {{color|green|At:}} [http://www.biodiversitylibrary.org/ BHL]

[[Category:Diseae]]
[[Category:Plant subtribes]]
