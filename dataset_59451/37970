{{about||the series as a whole|Blackadder|the unaired pilot|The Black Adder (pilot episode)}}
{{Use dmy dates|date=November 2012}}
{{Use British English|date=May 2011}}
{{Infobox television
| show_name = The Black Adder
| image = The Black Adder.jpg
| caption = Title screen, showing [[Alnwick Castle]] in [[Northumberland]], a main location for the series.
| camera =
| picture_format = [[Aspect ratio (image)|4:3]]
| audio_format = [[Monaural|Mono]]
| runtime = 33 minutes (approx)
| creator = [[Richard Curtis]]<br/>[[Rowan Atkinson]]
| developer =
| producer = [[John Lloyd (writer)|John Lloyd]]
| executive_producer = 
| starring = [[Rowan Atkinson]]<br/>[[Tony Robinson]]<br/>[[Tim McInnerny]]<br/>[[Brian Blessed]]<br/>[[Elspet Gray]]<br/>[[Robert East (actor)|Robert East]]
| voices =
| narrated = [[Patrick Allen]]
| theme_music_composer = [[Howard Goodall]]
| opentheme =
| endtheme =
| country = United Kingdom
| location = 
| language = English
| network = [[BBC 1]]
| first_aired = 15 June [[1983 in television|1983]]
| last_aired = <br> 20 July 1983
| num_series =
| num_episodes = [[List of Blackadder episodes|6]]
| list_episodes =
| preceded_by = [[The Black Adder (pilot episode)|Unaired pilot]]
| followed_by = ''[[Blackadder II]]''
| related =
| website = http://www.bbc.co.uk/comedy/blackadder/episodes/one/
}}
'''''The Black Adder''''' is the first series of the [[BBC]] [[sitcom]] ''[[Blackadder]]'', written by [[Richard Curtis]] and [[Rowan Atkinson]], directed by Martin Shardlow and produced by [[John Lloyd (writer)|John Lloyd]]. The series was originally aired on [[BBC One]] from 15 June 1983 to 20 July 1983, and was a joint production with the Australian [[Seven Network]]. Set in 1485 at the end of the [[England in the Middle Ages|British Middle Ages]], the series is written as a [[secret history]] which contends that [[Richard III of England|King Richard III]] won the [[Battle of Bosworth Field]], only to be unintentionally assassinated by his nephew Edmund and succeeded by Richard IV, one of the [[Princes in the Tower]]. The series follows the exploits of Richard IV's unfavoured second son Edmund (who calls himself "The Black Adder") in his various attempts to increase his standing with his father and, in the final episode, his quest to overthrow him.<ref name="Comedy guide">''[http://www.bbc.co.uk/comedy/theblackadder/ The Black Adder]'' at the BBC Comedy Guide. Retrieved 17 April 2008</ref>

Conceived while Atkinson and Curtis were working on ''[[Not the Nine O'Clock News]]'', the series covers a number of medieval issues in Britain in a humorous and often anachronistic manner—[[witchcraft]], [[Primogeniture|royal succession]], [[List of conflicts in Europe|European relations]], the [[Crusade]]s and the conflict between [[the Crown]] and [[History of the Church of England|the Church]]. The filming of the series was highly ambitious, with a large cast and much [[location shooting]].<ref>[http://www.imdb.com/title/tt0084988/trivia Trivia] at the [[Internet Movie Database]]. Retrieved 17 April 2008</ref> [[Shakespeare's influence|Shakespearean dialogue]] is sometimes adapted for comic effect.<ref name="Interviews">''I Have a Cunning Plan—20th Anniversary of Blackadder'', [[BBC Radio 4]] documentary broadcast, 23 August 2003. Excerpts available at [http://www.bbc.co.uk/comedy/blackadder/interviews/ bbc.co.uk] and [http://www.bbc.co.uk/programmes/p00f4hy2 Blackadder] at [[BBC Two]]. Retrieved 30 July 2016</ref> Despite winning an [[International Emmy]], ''The Black Adder'' is generally regarded as the weakest series of ''Blackadder'', with the more acclaimed following series departing from ''The Black Adder'' in their less ambitious production and reworked characters.

==Plot==
Set in the [[England in the Middle Ages|Middle Ages]], the series is written as an [[alternate history]]. It opens on 21 August 1485, the eve of the [[Battle of Bosworth Field]], which in the series is won not by [[Henry VII of England|Henry Tudor]] (as in reality) but by Richard III. [[Richard III of England|Richard III]], played by [[Peter Cook]], is presented as a good king who doted on his nephews, contrary to the [[Richard III (play)|Shakespearean view]] of him as a hunchbacked, [[Princes in the Tower|infanticidal]] monster.

After his victory in the battle, Richard III is unintentionally killed by [[Edmund Blackadder|Lord Edmund Plantagenet]]; Richard attempts to take Edmund's horse, which he thinks is his own. Not recognizing the king, Edmund thinks Richard is stealing it and cuts his head off. The late King's nephew, [[Richard IV of England (Blackadder)|Richard, Duke of York]] (played by [[Brian Blessed]]) who is Lord Edmund [[House of Plantagenet|Plantagenet]]'s father, is then crowned as Richard IV. Lord Edmund himself did not take part in the battle after arriving late, but later claims to have killed 450 peasants and several nobles, one of whom had actually been killed by [[Harry, Prince of Wales (Blackadder)|his brother]] in the battle.<ref name="shardlow">{{cite video | people = M.Shardlow | title = The Black Adder episode 1 "The Foretelling" | medium = DVD | publisher = [[BBC Worldwide]] | location = United Kingdom | date = 1 November 1999 }}</ref>

King Richard IV of England and XII of Scotland and his Queen [[Gertrude of Flanders (Blackadder)|Gertrude of Flanders]] have two sons: [[Harry, Prince of Wales (Blackadder)|Harry, Prince of Wales]] and his younger brother [[Prince Edmund (Blackadder)|Prince Edmund]]. Of the two, Harry is by far his father's favourite, the King barely acknowledging his second son's existence. It is a [[running gag]] throughout the series that Edmund's father cannot even remember his name.<ref>{{cite video | people = M.Shardlow | title = The Black Adder episode 6 "The Black Seal" | medium = DVD | publisher = [[BBC Worldwide]] | location = United Kingdom | date = 1 November 1999 }}</ref> However, despite his mostly dismissive attitude toward his second son, the series' finale makes it clear that the King loves Edmund as much as Harry: on Edmund's deathbed, the King does his best to console him and has the entire court drink a toast in his honour. In the third episode, when Edmund becomes the Archbishop of Canterbury and helps his father to secure some land before the church, the King acknowledges Edmund as his son, embraces him and even mentions to the Queen that he has "turned out well".

Using this premise, the series follows the fictitious reign of Richard IV (1485&ndash;98) through the experiences of Prince Edmund, who styles himself as "The Black Adder", and his two sidekicks: the imbecilic [[Lord Percy Percy]], the [[Duke of Northumberland]] ([[Tim McInnerny]]); and [[Baldrick]] ([[Tony Robinson]]), a more intelligent servant of no status.

By the end of the series, events converge with accepted history, when King Richard IV and his entire family are poisoned, allowing Henry Tudor to take the throne as [[Henry VII of England|King Henry VII]]. He then rewrites history, presenting Richard III as a monster, and eliminating Richard IV's reign from the history books. In reality, [[Richard of Shrewsbury, 1st Duke of York|Richard, Duke of York]], one of the [[Princes in the Tower]], was only twelve years old (and perhaps two years dead) when the [[Battle of Bosworth Field]] took place in 1485, and thus too young to have had two adult sons. One notable anachronism is Edmund´s title, [[Duke of Edinburgh]], as Scotland was an independent kingdom ruled by a different royal house from its inception in the ninth century until 1603 (with the exception of some periods during the turmoil of the [[Scottish Wars of Independence]]) though given that this is an alternate history, it is possible that Henry Tudor falsified the history of Scotland as well.

===Episodes===
The episodes in this series, written by [[Rowan Atkinson]] and [[Richard Curtis]], were originally shown on [[BBC One]] on Wednesday evenings, 21:25 &ndash; 22:00.<ref>[http://www.bbc.co.uk/comedy/blackadder/episodes/one/ The Black Adder] at the [[bbc.co.uk]] minisite. Retrieved 17 April 2008</ref> Each episode ran for roughly 33 minutes. The series began on 15 June 1983.

Each of the episodes was based on a medieval theme &mdash; the [[Wars of the Roses]], the [[Crusades]] and [[primogeniture|Royal succession]], the conflict between [[the Crown]] and [[History of the Church of England|the Church]], [[arranged marriage]]s between monarchies, and [[Black Death|the Plague]] and [[witchcraft]]. The final episode follows a planned [[coup d'état]].

The series was broadcast shortly after the [[BBC Television Shakespeare]] productions of Shakespeare's four plays about the [[Wars of the Roses]], the three-part ''Henry VI'' plays, followed by ''Richard III'', which was first shown on 23 January 1983. Some of the same actors were used to enhance the parody of Shakespearean history. [[Ron Cook]], who played Richard III in the Shakespeare productions, is cast as the villainous "Sean the Irish Bastard". [[Peter Benson (actor)|Peter Benson]], who played Shakespeare's Henry VI, played Henry VII in the first episode.

{| class="wikitable" style="width:100%; background:#EEDEFF"
! width=5%|No.
! Title
! width=15%|Airdate
|- align="center"
| 1-1
| '''[[The Foretelling]]'''
| 15 June 1983
|- bgcolor="FFFFFF"
| colspan=3| As the [[Wars of the Roses]] reach their climax, Edmund finds that he has accidentally killed the [[King Richard III (Blackadder)|King]] and become a prince of the realm.
|- align="center"
| 1-2
| '''[[Born to Be King (Blackadder)|Born to Be King]]'''
| 22 June 1983
|- bgcolor="FFFFFF"
| colspan=3| Edmund plots revenge when [[Dougal McAngus]], the King's Supreme Commander, is awarded Edmund's Scottish lands.
|- align="center"
| 1-3
| '''[[The Archbishop]]'''
| 29 June 1983
|- bgcolor="FFFFFF"
| colspan=3| With the crown and church at each other's throats, the [[King Richard IV of England|King]] decides that Edmund should become the new [[Archbishop of Canterbury]].
|- align="center"
| 1-4
| '''[[The Queen of Spain's Beard]]'''
| 6 July 1983
|- bgcolor="FFFFFF"
| colspan=3| Edmund is to be married to an ugly Spanish princess and tries everything to stop the wedding. First appearance of [[Miriam Margolyes]].
|- align="center"
| 1-5
| '''[[Witchsmeller Pursuivant]]'''
| 13 July 1983
|- bgcolor="FFFFFF"
| colspan=3| Edmund is suspected of being a [[witch]] by a mysterious witch-hunter and sentenced to death.
|- align="center"
| 1-6
| '''[[The Black Seal]]'''
| 20 July 1983
|- bgcolor="FFFFFF"
| colspan=3| When all of Edmund's titles are removed except Warden of the Royal Privies, Edmund is furious and decides to seize the throne with the help of the six most evil men in the kingdom. <br> First appearance of [[Rik Mayall]], here playing the part of Mad Gerald (though the character is credited as "himself").
|}

===Character development===
In this series, the character of the Black Adder is somewhat different from later incarnations, being largely unintelligent, naive, and snivelling. The character does evolve through the series, however, and he begins showing signs of what his descendants will be like by the final episode, where he begins insulting everyone around him and making his own plans. This evolution follows naturally from the character's situation. "The Black Adder" is the title that Edmund adopts during the first episode (after first considering "The Black Vegetable").<ref name="shardlow"/> Presumably one of his descendants adopted it as a surname before ''[[Blackadder II]]'', in which the title character becomes "[[Edmund Blackadder]]".

==Production==

===Development===
Rowan Atkinson and Richard Curtis developed the idea for the sitcom while working on ''[[Not the Nine O'Clock News]]''. Eager to avoid comparisons to the critically acclaimed ''[[Fawlty Towers]]'', they proposed the idea of a historical sitcom.<ref name="Interviews"/><ref name="Curtis interview">[http://www.blackadderhall.com/?page_id=330 Interview] at Blackadder Hall. Retrieved 17 April 2008</ref> An [[The Black Adder (unaired pilot)|unaired pilot episode]] was made in 1982, and a six-episode series was commissioned.

In the [[The Black Adder (unaired pilot)|unaired pilot episode]], covering the basic plot of "[[Born to be King]]", Rowan Atkinson speaks, dresses and generally looks and acts like the later Blackadder descendants of the second series onwards, but no reason is given as to why he was instead changed to a snivelling wretch for the first series. Richard Curtis has stated he cannot remember the exact reason, but has suggested it was because they wanted to have a more complicated character (implying that the change was driven by the writing) instead of a swaggering lead from the pilot.<ref name="Curtis interview"/>

Curtis admitted in a 2004 [[documentary film|documentary]] that just before recording began, producer [[John Lloyd (writer)|John Lloyd]] came up to him with Atkinson and asked what Edmund's character was. Curtis then realised that, despite writing some funny lines, he had no idea how Rowan Atkinson was supposed to play his part.<ref>''[http://www.bbc.co.uk/sitcom/advocate_blackadder.shtml Britain's Best Sitcom - Blackadder]'', 2004 BBC Television documentary, presented by [[John Sergeant (journalist)|John Sergeant]]</ref> On the 25th anniversary documentary ''Blackadder Rides Again'', Atkinson added that as the cameras were about to roll for the first time, he suddenly realised he wasn't even sure what voice to use for the character.<ref name=ridesagain>''Blackadder Rides Again'', 2009 BBC Television/Tiger Aspect documentary</ref>

===Filming===
[[File:The Black Adder production values.jpg|thumb|right|The series featured elaborate location shooting outside [[Alnwick Castle]] in Northumberland with horses, medieval-style costumes and a large cast of extras. In this scene from the opening of the episode "[[Born to be King]]", King Richard IV rides off to the Crusades, leaving Blackadder, Baldrick and Percy (front right) to plan a way to gain power.]]
The budget for the series was considerable, with much location shooting particularly at [[Alnwick Castle]] in [[Northumberland]] and the surrounding countryside in February 1983.<ref>[http://www.alnwickcastle.com/television.php Alnwick Castle official website], URL accessed 2 June 2008</ref><ref>[http://www.imdb.com/title/tt0084988/locations Locations] at the [[Internet Movie Database]]. Retrieved 17 April 2008</ref> [[Brinkburn Priory]], an authentic reconstruction of a medieval monastery church, was used for the episode "[[The Archbishop (Blackadder)|The Archbishop]]".<ref>[http://www.english-heritage.org.uk/daysout/properties/brinkburn-priory/ Brinkburn Priory], URL accessed 9 August 2010</ref> The series also used large casts of extras, as well as horses and expensive medieval-style costumes. Filming at the castle was hindered by bad weather - snow is visible in many of the outdoor location shots.<ref name=ridesagain/>

Atkinson had to suffer during the making of the programme, having to trim his hair in an unflattering medieval style and wearing a selection of "[[Priapus|priapic]] [[codpiece]]s".<ref name="Interviews"/>
Atkinson has said about the making of the first series:

<blockquote>The first series was odd, it was very extravagant. It cost a million pounds for the six programmes... [which] was a lot of money to spend...It looked great, but it wasn't as consistently funny as we would have liked.<ref name="Interviews"/></blockquote>

===Cast===

{| class="wikitable"
|-
! Character !! Pilot !! TV series
|-
| [[Edmund Blackadder|Edmund, Duke of Edinburgh]] || colspan="2"| <center> [[Rowan Atkinson]]</center> 
|-
| [[List of Blackadder characters#King Richard IV of England|King Richard IV of England]] || [[John Savident]] || [[Brian Blessed]]
|-
| [[List of Blackadder characters#Gertrude of Flanders|Gertrude, Queen of Flanders]] || colspan="2"| <center> [[Elspet Gray]]</center> 
|-
| [[List of Blackadder characters#Harry.2C Prince of Wales|Harry, Prince of Wales]] || [[Robert Bathurst]] || [[Robert East (actor)|Robert East]]
|-
| [[List of Blackadder characters#Lord Percy Percy|Percy, Duke of Northumberland]] || colspan="2"| <center> [[Tim McInnerny]]</center> 
|-
| [[List of Blackadder characters#Minor characters|Dougal MacAngus, 4th Duke of Argyll]] || colspan="2"| <center> [[Alex Norton]]</center> 
|-
| [[Baldrick|Baldrick, Son of Robin the Dung Gatherer]] || [[Philip Fox (actor)|Philip Fox]] || [[Tony Robinson]]
|-
| [[List of Blackadder characters#Minor characters|Lord Chiswick]] ||||[[Stephen Tate]]
|-
| Rudkin || [[Simon Gipps-Kent]] || 
|-
| Jesuit || Oengus MacNamara || 
|}

Robinson stated in a 2003 radio documentary that he was originally flattered to be offered a part and it was only later he found that every other small-part actor had also been offered the role and turned it down.<ref name="Interviews"/> The series also featured a number of guest roles, often featuring noted actors such as [[Peter Cook]] and [[Peter Benson (actor)|Peter Benson]] in "[[The Foretelling]]"; [[Miriam Margolyes]] and [[Jim Broadbent]] in "[[The Queen of Spain's Beard]]"; [[Frank Finlay]] in "[[Witchsmeller Pursuivant]]"; and [[Rik Mayall]] and [[Patrick Allen]] (who also narrated the series) in "[[The Black Seal]]".<ref>[http://www.imdb.com/title/tt0084988/fullcredits Cast] at the [[Internet Movie Database]]. Retrieved 19 April 2008</ref>

===Title sequence and music===
The [[title sequence]] consisted of several stock shots of Edmund riding his horse on location, interspersed with different shots of him doing various silly things (and, usually, a shot of King Richard IV to go with Brian Blessed's credit). The closing titles were a similar sequence of Edmund riding, and eventually falling off, his horse and then chasing after it. All the credits of the first series featured deliberately eccentric orderings of the cast list (such as "[[order of precedence]]", "order of witchiness" and "order of disappearance") and included "with additional dialogue by [[William Shakespeare]]" and "made in glorious television".<ref>[http://www.imdb.com/title/tt0084988/crazycredits Credits] at the [[Internet Movie Database]]. Retrieved 17 April 2008</ref>

The series used the first incarnation of the ''Blackadder'' theme by [[Howard Goodall]] (with the exception of the unaired pilot, which featured a different arrangement).<ref>''[http://www.howardgoodall.co.uk/tvthemes/Blackadder.htm The Black Adder theme]'' at [[Howard Goodall]]'s official website. Retrieved 17 April 2008</ref> For the opening theme, a trumpet solo accompanied by an orchestra was used. For the end titles, the theme gained mock-heroic lyrics sung by a baritone ([[Simon Carrington]], a member of the [[King's Singers]]). In the final episode, the theme was sung with altered lyrics by a [[Boy soprano|treble]], in a more reflective style. The series' incidental music was unusually performed by [[pipe organ]] and percussion.<ref>[http://www.howardgoodall.co.uk/tvthemes/Blackadder.htm Howard Goodall official website]. Retrieved 17 April 2008</ref>

==Awards and criticism==
The series won an [[International Academy of Television Arts and Sciences|International Emmy]] award in the popular arts category in 1983.<ref name="Lewisohn">Lewisohn, Mark, [http://web.archive.org/web/20050408071319/www.bbc.co.uk/comedy/guide/articles/b/blackadderthe_7770760.shtml ''The Black Adder] at the former [[BBC Guide to Comedy]]. Retrieved 17 April 2008</ref> The four series of ''[[Blackadder]]'' were voted into second place in the BBC's ''[[Britain's Best Sitcom]]'' in 2004 with 282,106 votes, although the series' advocate, [[John Sergeant (journalist)|John Sergeant]], was not complimentary of the first series, suggesting it was "grandiose, confused and expensive".<ref>[http://www.bbc.co.uk/sitcom/winner.shtml The final top-ten of Britain’s Best Sitcom], URL accessed 4 April 2008</ref><ref>''[http://www.bbc.co.uk/sitcom/advocate_blackadder.shtml Britain's Best Sitcom - Blackadder]''. Retrieved 25 April 2008</ref>

Members of the cast and crew, looking back for the documentary ''Blackadder Rides Again'', are also not particularly complimentary of the first series. John Lloyd recalls that a colleague commented at the time that the series "looks a million dollars, but cost a million pounds", although admits that they were proud of the result at the time.<ref name=ridesagain/> Due to the high cost of the first series, the Controller of BBC1 at the time the second series was commissioned, [[Michael Grade]], was reluctant to authorise a second series without major improvements and cost-cutting, leaving a gap of three years before ''[[Blackadder II]]'' was broadcast, on the condition that it remained largely studio-bound.<ref>Lewisohn, Mark, [http://web.archive.org/web/20050408060711/www.bbc.co.uk/comedy/guide/articles/b/blackadderii_7770765.shtml ''Blackadder II] at the former [[BBC Guide to Comedy]]. Retrieved 17 March 2007</ref>

==Releases==
The complete series of ''The Black Adder'' is available as a [[DVD region code|Region 2]] [[DVD]] from [[BBC Worldwide]], as well as in a complete [[box set]] with the other series. An earlier [[VHS]] release of the series was also produced in September 1996. The series is also available in Region 1 DVD in a box set of the complete series. "The Complete Collected Series 1, 2, 3, 4 and Specials", a 15-disc complete set of audiobooks published by BBC Audiobooks Ltd, was released in 2009. A selection of one-off episodes, documentaries and other appearances by "Blackadder" are featured, with some of this extra material being released on audio for the first time.

===VHS releases===
* On about 5 February 1990, BBC Enterprises Ltd released all 6 episodes of ''The Black Adder'' on two single videos and on 7 September 1992 all 6 episodes of ''Blackadder II'' were re-released on 'Complete' double VHS releases.  All 6 episodes were re-released on a single video release on 2 October 1995.

{|class="wikitable" width=99%
!VHS video title
!Year of release/BBFC rating
!Episodes

|-
| align="center"|''The Black Adder- The Foretelling'' (BBCV 4293)
| align="center"|5 February 1990 (PG)
| align="center"|The Foretelling, Born to be King, The Archbishop
|-
| align="center"|''The Black Adder- The Queen Of Spain's Beard'' (BBCV 4296)
| align="center"|5 February 1990 (15)
| align="center"|The Queen of Spain's Beard, Witchsmeller Pursuivant, The Black Seal
|-
| align="center"|''The Complete Blackadder'' (Double Pack) (BBCV 4782)
| align="center"|7 September 1992 (15)
| align="center"|'''TAPE 1:''' The Foretelling, Born to be King, The Archbishop<br>'''TAPE 2:''' The Queen of Spain's Beard, Witchsmeller Pursuivant, The Black Seal
|-
| align="center"|''Blackadder- The Entire Historic First Series'' (BBCV 5711)
| align="center"|2 October 1995 (15)
| align="center"|same as 'The Complete Blackadder' but with all 6 episodes on a single video: The Foretelling, Born to be King, The Archbishop, The Queen of Spain's Beard, Witchsmeller Pursuivant, The Black Seal
|}

==References==
{{reflist|30em}}

==External links==
{{wikiquote|Blackadder#The_Black_Adder|The Black Adder}}
*{{IMDb title|id=0084988|title=The Black Adder (1983)}}
* {{tv.com show|blackadder|The Black Adder}}
*''[http://www.bbc.co.uk/comedy/theblackadder/ The Black Adder]'' at the BBC Comedy Guide

{{Blackadder}}
{{Richard Curtis}}
{{InternationalEmmyAward Popular Arts Programming}}

{{good article}}

{{DEFAULTSORT:Black Adder, The}}
[[Category:1983 British television programme debuts]]
[[Category:1983 British television programme endings]]
[[Category:Blackadder|The Black Adder]]
[[Category:Period television series]]
[[Category:1980s British comedy television series|Blackadder I]]
[[Category:1480s in fiction]]
[[Category:English-language television programming]]
[[Category:Parodies]]
[[Category:Television series set in the 15th century]]

[[es:La víbora negra]]