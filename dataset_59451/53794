{{Infobox Politician
|name = William Pinkney Whyte
|image = William Pinkney Whyte 1865-1880 Maryland politician.jpg
|caption = 
|birth_date = August 9, 1824
|birth_place = [[Baltimore, Maryland]]
|residence =
|death_date = {{death date and age|1908|3|17|1824|8|9}}
|death_place = [[Baltimore, Maryland]]
|order = 35th
|office = Governor of Maryland
|term = January 10, 1872 &ndash; March 4, 1874
|predecessor = [[Oden Bowie]]
|successor = [[James Black Groome|James B. Groome]]
|jr/sr2=United states Senator
|state2=[[Maryland]]
|term_start2     = June 8, 1906 
|term_end2       = March 17, 1908
|predecessor2    = [[Arthur Pue Gorman|Arthur P. Gorman]]
|successor2      = [[John Walter Smith|John W. Smith]]
|term_start3     = March 4, 1875
|term_end3       = March 3, 1881
|predecessor3    = [[William Thomas Hamilton|William T. Hamilton]]
|successor3      = [[Arthur Pue Gorman|Arthur P. Gorman]]
|term_start4     = July 13, 1868
|term_end4       = March 3, 1869
|predecessor4    = [[Reverdy Johnson]]
|successor4      = [[William Thomas Hamilton|William T. Hamilton]]
|office5         = [[Attorney General of Maryland]]
|term_start5     = 1887
|term_end5       = 1891
|preceded5       = [[Charles Boyle Roberts]]
|succeeded5      = [[John Prentiss Poe]]
|order6          = [[Mayor of Baltimore]]
|term_start6     = 1881 
|term_end6       = 1883
|predecessor6    = [[Ferdinand Claiborne Latrobe]] 
|successor6      = [[Ferdinand Claiborne Latrobe]] 
|office7         = Member of the [[Maryland House of Delegates]]
|term7           = 1847-1849
|party = [[Democratic Party (United States)|Democrat]]
|religion =[[Episcopal Church (United States)|Episcopalian]]
|spouse = Louisa D. Hollingsworth<br>Mary McDonald Thomas
|children = four children
|website =
|footnotes = [http://www.nga.org/portal/site/nga/menuitem.29fab9fb4add37305ddcbeeb501010a0/?vgnextoid=2b3e224971c81010VgnVCM1000001a01010aRCRD&vgnextchannel=e449a0ca9e3f1010VgnVCM1000001a01010aRCRD National Governors Association, Governor's Information, Maryland Governor William Pinkney Whyte]
}}
'''William Pinkney Whyte''' (August 9, 1824{{spaced ndash}}March 17, 1908), a member of the [[Democratic Party (United States)|United States Democratic Party]], was a politician who served the [[State of Maryland]] as a [[Maryland House of Delegates|State Delegate]], the State [[Comptroller]], a [[United States Senate|United States Senator]], the [[List of Governors of Maryland|35th]] [[Governor of Maryland|Governor]], the [[List of Mayors of Baltimore|Mayor of Baltimore, Maryland]], and the State [[Attorney General]].

==Early life and education==
Whyte was born in [[Baltimore, Maryland]], the son of Joseph and Isabella White (he later changed his surname to Whyte following a family disagreement).  His grandfather, [[William Pinkney]], had been a famous [[United States]] politician, administrator, and diplomat.

Whyte's early education involved instruction by a private tutor, who had been personal secretary to [[Napoleon Bonaparte]].  From 1842-1844, Whyte was unable to attend college as a result of his family's poor financial situation, and began work at the banking firm of Peabody, Riggs and Co. in Baltimore.  He began to study law in Baltimore at the law office of Brown and Brune for one year before being admitted to the law school of [[Harvard University]] in 1844.  Whyte returned to Baltimore in 1845 for further study, and was admitted to the [[bar (law)|Bar]] soon after in 1846.

==Political career==
From 1847 to 1849, Whyte served one term as a member of the Maryland House of Delegates.  In 1850, Whyte was unsuccessful in a bid for election to the 32nd Congress.  From 1853-1855, he served one term as [[Comptroller]] of the State Treasury of Maryland, for which he was credited for introducing a more simplified financial system to the State.

[[File:Governor william whyte of maryland.jpg|thumb|left|A later portrait of Whyte]]

In 1857, Whyte was again nominated to serve in Congress.  He was defeated, but brought forth evidence before the [[United States House of Representatives|House of Representatives]] of fraud and corruption regarding the election.  The House did not concur on whether or not he should have been appointed, however.

After nearly a decade out of the political arena, Whyte was asked by then-governor [[Thomas Swann]] to fill the remainder of resigning senator [[Reverdy Johnson]]'s term from July 13, 1868 to March 3, 1869.  During his short tenure as senator, Whyte steadfastly supported the embattled [[President of the United States|President]] [[Andrew Johnson]], and also supported easing the tension on the [[U.S. Southern states|Southern states]] during [[Reconstruction era (United States)|Reconstruction]].  He chose not to be a candidate for re-election in 1868, however.

In 1872, Whyte was elected Governor of Maryland, defeating Republican challenger [[Jacob Tome]].  In the election of 1874, Whyte was elected by the legislature as a Democrat to the United States Senate and accordingly resigned from the position of Governor.  In 1874, in between his terms as governor and senator, he served as counsel for Maryland before the arbitration board in the boundary dispute between [[Virginia]] and Maryland. During this tenure as senator, Whyte opposed paying the nation's debt with silver and gold instead of solely gold, and served as the chairman of the [[U.S. Senate Committee on Printing]] (46th Congress).  In the election of 1880, Whyte chose not to run for re-election, due to family illness and strife between him and his counterpart senator, [[Arthur P. Gorman]].

Whyte was elected unopposed to be mayor of Baltimore in 1881.  At the conclusion of his term in 1883, Whyte chose to go back to practicing law.  From 1887-1891, Whyte was Attorney General of Maryland, and from 1900–1903, the Baltimore City Solicitor.  In 1906, Whyte was appointed by Maryland Governor [[Edwin Warfield]] to fill the Senate seat vacancy caused by the death of Arthur P. Gorman. At 25 years, 3 months, 5 days since his last day in the chamber, Whyte's return set the all-time mark for the longest gap in service to the U.S. Senate.<ref>{{cite web |url= http://editions.lib.umn.edu/smartpolitics/2013/12/04/bob-smith-and-the-12-year-itch/ |title= Bob Smith and the 12-Year Itch |work=Smart Politics |first=Eric |last=Ostermeier |date= December 4, 2013}}</ref>

Whyte served as senator until his unexpected death in Baltimore, and is buried in [[Greenmount Cemetery]].

==References==
{{Reflist}}

==Sources==
{{CongBio|W000435}}
*Frank F. White, Jr., The Governors of Maryland 1777-1970 (Annapolis:  The Hall of Records Commission, 1970), 179-183.
*[http://babel.hathitrust.org/cgi/pt?id=loc.ark:/13960/t05x2dm0f;view=1up;seq=7 William Pinkney Whyte, late a senator from Maryland, Memorial addresses delivered in the House of Representatives and Senate frontispiece 1909]

==Further reading==
*Tracy Matthew Melton, ''Hanging Henry Gambrill: The Violent Career of Baltimore's Plug Uglies, 1854-1860'' (2005). Includes information on Whyte's early legal and political careers.

{{S-start}}
{{s-off}}
{{succession box | before = [[Henry E. Bateman]] |title=[[List of Comptrollers of Maryland|Comptroller of Maryland]] | years = 1854&ndash;1856 | after = [[William Henry Purnell]]}}
{{succession box | before = [[Oden Bowie]] |title=[[Governor of Maryland]] | years = 1872&ndash;1874 | after = [[James Black Groome|James B. Groome]]}}
{{succession box | before = [[Ferdinand Claiborne Latrobe]] |title=[[List of Mayors of Baltimore|Mayor of Baltimore]] | years = 1881&ndash;1883 | after = [[Ferdinand Claiborne Latrobe]]}}
{{s-par|us-sen}}
{{U.S. Senator box|state=Maryland|class=1|before=[[Reverdy Johnson]]|after=[[William Thomas Hamilton|William T. Hamilton]]|alongside=[[George Vickers]]|years=1868&ndash;1869}}
{{U.S. Senator box|state=Maryland|class=1|before=[[William Thomas Hamilton|William T. Hamilton]]|after=[[Arthur Pue Gorman|Arthur P. Gorman]]|alongside=[[George R. Dennis]], [[James Black Groome|James B. Groome]]|years=1875&ndash;1881}}
{{U.S. Senator box|state=Maryland|class=3|before=[[Arthur Pue Gorman|Arthur P. Gorman]]|after=[[John Walter Smith|John W. Smith]]|alongside=[[Isidor Rayner]]|years=1906&ndash;1908}}
{{s-legal}}
{{succession box | before = [[Charles Boyle Roberts]] |title=[[Attorney General of Maryland]] | years = 1887&ndash;1891 | after = [[John P. Poe, Sr.|John Prentiss Poe]]}}
{{S-end}}
{{Governors of Maryland}}
{{USSenMD}}
{{BaltimoreMayors}}

{{Authority control}}

{{DEFAULTSORT:Whyte, William Pinkney}}
[[Category:1824 births]]
[[Category:1908 deaths]]
[[Category:Comptrollers of Maryland]]
[[Category:Governors of Maryland]]
[[Category:Harvard Law School alumni]]
[[Category:Mayors of Baltimore]]
[[Category:Members of the Maryland House of Delegates]]
[[Category:Politicians from Baltimore]]
[[Category:Maryland Attorneys General]]
[[Category:United States Senators from Maryland]]
[[Category:Burials at Green Mount Cemetery]]
[[Category:Democratic Party United States Senators]]
[[Category:Maryland Democrats]]
[[Category:Democratic Party state governors of the United States]]