{{italic title}}
[[File:AnOutcastOfTheIslands.jpg|thumb|First UK edition (publ. [[T. Fisher Unwin]])]]

'''''An Outcast of the Islands''''' is the second novel by [[Joseph Conrad]], published in [[1896 in literature|1896]], inspired by Conrad's experience as mate of a steamer, the ''Vidar''.

The novel details the undoing of Peter Willems, a disreputable, immoral man who, on the run from a scandal in [[Makassar]], finds refuge in a hidden native village, only to betray his benefactors over lust for the tribal chief's daughter. The story features Conrad's recurring character Tom Lingard, who also appears in ''[[Almayer's Folly]]'' (1895) and ''[[The Rescue (Conrad novel)|The Rescue]]'' (1920), in addition to sharing other characters with those novels.  It is considered to be underrated as a work of literature for many.  Conrad romanticizes the jungle environment and its inhabitants in a similar style to his "Heart of Darkness".

This novel was adapted into the film ''[[Outcast of the Islands]]'' in 1951 by director [[Carol Reed]], featuring [[Trevor Howard]] as Willems, [[Ralph Richardson]] as Lingard, [[Robert Morley]], and [[Wendy Hiller]].

The work was quoted in [[T. S. Eliot]]'s ''[[The Hollow Men]]'' ('Life is very long.').

==External links==
{{Wikisource}}
{{Gutenberg|no=638|name=An Outcast of the Islands}}
*[https://archive.org/details/AnOutcastOfTheIslands ''An Outcast of the Islands'']  at [[Internet Archive]]
* {{librivox book | title=An Outcast Of The Islands | author=Joseph Conrad}}
*[http://www.mantex.co.uk/2012/08/19/an-outcast-of-the-islands/ ''An Outcast of the Islands'', Study Guide, with plot, characters, critical summary, resources.]

{{Conrad}}
{{Lingard Trilogy}}

{{DEFAULTSORT:Outcast Of The Islands, An}}
[[Category:1896 novels]]
[[Category:Novels by Joseph Conrad]]
[[Category:Novels set in Indonesia]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]

{{1890s-novel-stub}}