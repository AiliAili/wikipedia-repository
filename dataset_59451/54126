{{Use dmy dates|date=May 2011}}
{{Use Australian English|date=May 2011}}
{{Infobox television channel
| name              = Channel 44
| logofile          = 44adelaidelogo.png
| logosize          = 
| logocaption       = 
| launch            = 23 April 2004
| closed date       = <!-- 31 December 2016 -->
| picture format    = [[576i]] ([[SDTV]]) [[Anamorphic widescreen|16:9]]
| share             = 
| share as of       = 
| share source      = 
| network           = 
| owner             = C44 Adelaide Ltd
| slogan            = ''Adelaide - It's Your 44''
| country           = [[Australia]]
| language          = English
| broadcast area    = [[Adelaide]], surrounding areas<ref>{{cite web |url=http://www.acma.gov.au/licplan/defmaps/documents/maps/la_162.pdf |title=Channel 44 licence area |publisher=Australian Community Television Alliance |format=PDF |access-date=19 April 2016}}</ref>
| headquarters      = 
| former names      = C31 Adelaide (2004-2010)<br />44 Adelaide (2010-2014)
| replaced names    = 
| sister names      = 
| website           = [http://c44.com.au/ c44.com.au]
| terr serv 1       = DVB-T
| terr chan 1       = 44
| terr serv 2       = [[Freeview (Australia)|Freeview]] ([[virtual channel|virtual]])
| terr chan 2       = 44
}}

'''Channel 44''' ('''C44''') is a [[free-to-air]] [[community television]] channel in [[Adelaide]], [[South Australia]]. C44 features locally and nationally made content and has been broadcasting since 23 April 2004. Previously known as C31 when on analogue television, C44 made the switch to digital on 5 November 2010 and switched off its analogue signal on 31 May 2012. C44 airs a range of local, interstate and international content that is relevant to the local community.

==History==
Before C44, Adelaide's community television station was [[ACE TV]], run by Adelaide Community and Education Television Inc. ACE TV held a temporary licence from May 1994<ref>Lawrence, G. [http://www.waikato.ac.nz/film/NAME/script/acetv.html Adelaide Community and Educational Television Inc.]. Accessed on 7 March 2007.</ref> until December 2002, when it was cancelled due to breaches of its licence conditions.<ref>Australian Broadcasting Authority (5 December 2002). [http://www.acma.gov.au/WEB/STANDARD/pc=PC_91276 ABA cancels ACE TV licence.] {{webarchive |url=https://web.archive.org/web/20130427000000/http://www.acma.gov.au/WEB/STANDARD/pc=PC_91276 |date=27 April 2013 }} Accessed on 29 December 2009.</ref> ACE TV had its last broadcast in May 2002.<ref name="C31launch">{{cite news |last=Yeaman|first=Simon |date=21 February 2004 |title=Community TV ready for comeback |url=http://www.c31.com.au/viewupdate.asp?id=7 |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |access-date=17 April 2016 |archiveurl=https://web.archive.org/web/20060820020646/http://www.c31.com.au/viewupdate.asp?id=7 |archivedate=20 August 2006 |dead-url=unfit}}</ref>

In 2003, after ACE TV's closure, C31 Adelaide Ltd received the community television licence for Adelaide.<ref name="C31launch" /> Its station, called '''C31 Adelaide''' (C31), launched on 23 April 2004 on analogue channel UHF 31.<ref>{{cite web |url=http://www.abc.net.au/stateline/sa/content/2003/s1093966.htm |title=''Stateline'' Transcript: C31, Community TV in Adelaide |date=23 April 2004 |publisher=Australian Broadcasting Corporation |access-date=11 September 2016 |archive-url=https://web.archive.org/web/20160911113500/http://www.abc.net.au/stateline/sa/content/2003/s1093966.htm |archive-date=11 September 2016 |dead-url=no}}</ref> In 2004, most community TV services in capital cities received permanent licences from the [[Australian Communications and Media Authority]] (ACMA). However, the process for allocating a permanent licence in Adelaide, which began in 2004, was terminated in mid-2006; the two prospective applicants (of which C31 was one) were declined for different reasons.<ref>Australian Communications and Media Authority. (29 June 2006). [http://www.acma.gov.au/WEB/STANDARD//pc=PC_100649 ACMA to take no further action in the current allocation of a permanent community television licence in Adelaide] {{webarchive |url=https://web.archive.org/web/20070925011550/http://www.acma.gov.au/WEB/STANDARD/ |date=25 September 2007 }}. Accessed on 7 March 2007.</ref>

On 5 November 2010, the station was moved to digital channel 44 and was renamed '''44 Adelaide''', with its analogue signal switched off on 31 May 2012. 44 Adelaide received a new logo in 2013 along with the Kids Foodnifht watering [[Humphrey B. Bear]].and was later renamed '''Channel 44''' (C44) in 2014. 

In September 2014, Australian federal communications minister [[Malcolm Turnbull]] announced that licensing for community television stations would end in December 2015.<ref>{{cite web|title=Community TV: Malcolm Turnbull confirms licensing for stations will end in 2015 |url=http://www.abc.net.au/news/2014-09-10/community-television-kicked-off-air-by-federal-government/5733690 |publisher= Australian Broadcasting Corporation |date=10 September 2014 |accessdate=19 January 2016}}</ref> In September 2015, Turnbull, now Prime Minister, announced an extension of the deadline to 31 December 2016.<ref>{{cite web |url=http://www.tvtonight.com.au/2015/09/community-tv-lifeline-extended-to-2016.html |last=Knox |first=David |title=Community TV lifeline: extended to 2016 |publisher=TV Tonight |date=17 September 2015 |accessdate=17 November 2015 }}</ref> The deadline was again extended to 30 June 2017 by Minister for Communications Mitch Fifield in December 2016.<ref>{{cite web |url=http://tvtonight.com.au/2016/12/new-switch-off-date-for-community-tv.html |last=Knox |first=David |title=New switch-off date for Community TV |publisher=TV Tonight |date=15 December 2016 |accessdate=18 December 2016 }}</ref> C44 has followed other community television stations and has moved operations online, streaming its channel live on their website. The move online means that local content can now be viewed by those outside Adelaide.<ref>{{cite web |url=http://www.abc.net.au/news/2016-07-24/adelaide-community-tv-station-channel-44-prepares-to-go-online-/7656346 |last=Smith |first=Matt |title=Channel 44: Adelaide community TV station prepares to leave the airwaves and go online-only |publisher=Australian Broadcasting Corporation |date=24 July 2016 |accessdate=27 July 2016 }}</ref><ref>{{cite news |last=Turner |first=Adam |date=20 April 2016 |title=Community TV: shift to online begins |url=http://www.smh.com.au/technology/mobiles/community-tv-shift-to-online-begins-20160412-go45mw.html |newspaper=The Sydney Morning Herald |location=Sydney |access-date=20 November 2016}}</ref>

==Identity history==
* 2010–2016: ''Bringing You the Community through TV''
* 2016–present: ''Adelaide - It's Your 44''

==See also==
{{Portal|Television in Australia}}
* [[Community television in Australia]]
* [[Television broadcasting in Australia]]

==References==
{{Reflist|30em}}

{{Channel 31}}
{{South Australia TV|state = collapsed}}
{{Australian free-to-air television networks}}

[[Category:Australian community television]]
[[Category:Television stations in Adelaide]]
[[Category:Television channels and stations established in 2004]]
[[Category:English-language television stations in Australia]]