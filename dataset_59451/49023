{{Infobox person
|name = Ben Kerner
|image = 
|image size = 
|caption =
|birth_name = 
|birth_date = {{birth date|1913|6|9}}
|birth_place = 
|death_date = {{death_date and age|2000|11|22|1913|6|9}}
|death_place = [[St. Louis, Missouri]]
|occupation = Businessman, sports-franchise founder and owner, basketball executive
|years_active = 1946–1968, as owner of [[NBA]]'s [[Tri-Cities Blackhawks]], [[Milwaukee Hawks]] and [[St. Louis Hawks]] 
|spouse = Ima Jean Kerner (1934–2011)
|website = 
}}

'''Ben Kerner''' (June 9, 1913 – November 22, 2000) was an American [[Basketball in the United States|professional basketball]] owner. He was the founder and owner of the [[St. Louis Hawks]] of the [[National Basketball Association]], the present-day [[Atlanta Hawks]].
In 1946 Kerner founded a professional team in [[Buffalo, New York]], which became the [[Moline, Illinois]]-based [[Tri-Cities Blackhawks]] after a few games. Kerner moved the team to [[Milwaukee]] in 1951 and to [[St. Louis]] in 1955. His 1958 St. Louis Hawks won the [[NBA Championship]].<ref>{{cite web|url=http://www.si.com/vault/1962/01/22/590480/singing-the-blues-in-st-louis|title=SINGING THE BLUES IN ST. LOUIS|author=Ray Cave|work=SI.com|accessdate=March 20, 2016}}</ref><ref name="basketball-reference.com">{{cite web|url=http://www.basketball-reference.com/executives/kernebe99x.html|title=Ben Kerner|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref>

==Notable transactions==
[[File:Bob Pettit 1957.jpeg|thumb|left|alt=Bob Pettit taking a shot|Bob Pettit in 1957]]
During the 1946-47 season, Kerner added [[Pop Gates]] to the Buffalo-Tri-Cities team. Gates finished second on the team in scoring, behind 1948 [[NBL Most Valuable Player Award|NBL MVP]] [[Don Otten]]. A [[Naismith Memorial Basketball Hall of Fame]] player, Gates was a factor in [[Race and ethnicity in the NBA|integrating the league]] and the first African-American coach in a major league when he coached Dayton in 1948.<ref>{{cite web|url=http://www.hoophall.com/hall-of-famers/tag/william-p-gates|title=The Naismith Memorial Basketball Hall of Fame - Hall of Famers|publisher=|accessdate=March 20, 2016}}</ref><ref>{{cite web|url=http://www.todaysfastbreak.com/from-the-courts/have-you-ever-heard-of-william-pop-gates/|title=Have You Ever Heard of - William 'Pop' Gates - TFB|author=Kelly Scaletta|work=Today's FastBreak|accessdate=March 20, 2016}}</ref>

Kerner hired Naismith Hall of Fame coach [[Red Auerbach]] for the Tri-Cities Blackhawks in 1949. When he discovered that Kerner had traded a player without consulting him, Auerbach left the Blackhawks to coach the [[Boston Celtics]] for the 1950-51 season<ref>{{cite web|url=http://espn.go.com/classic/biography/s/auerbach_red.html|title=ESPN Classic - Auerbach's Celtics played as a team|publisher=|accessdate=March 20, 2016}}</ref> and won a record nine NBA championships with the Celtics.<ref>{{cite web|url=http://www.encyclopedia.com/topic/Red_Auerbach.aspx|title=Red Auerbach|publisher=|accessdate=March 20, 2016}}</ref><ref>"Atlanta Hawks"2012Drew Silvermanp.15ABDOPublishing</ref>

In 1950, Kerner drafted Naismith Hall of Fame player [[Bob Cousy]] in the first round (number four) and sold him to the [[Chicago Stags]]. Cousy, reportedly unhappy to go to a small-town area, wanted $10,000 to sign with the Blackhawks and Kerner countered with $6,000 before selling him to the Stags.<ref name="basketball-reference.com"/> Cousy played for Auerbach in Boston when the Stags were sold, and played in 13 consecutive All-Star games.<ref>{{cite web|url=http://www.basketball-reference.com/players/c/cousybo01.html|title=Bob Cousy|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref>

Kerner drafted Hall of Fame player [[Bob Pettit]] in the first round (number two) in 1954. Pettit, who averaged 26 points and 16 rebounds per game over his career, was voted the [[NBA Most Valuable Player]] in 1956 and 1959.<ref name="basketball-reference.com"/><ref>{{cite web|url=http://www.basketball-reference.com/players/p/pettibo01.html|title=Bob Pettit|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref> At {{convert|6|ft|9|in|cm}}, Pettit was a ten-time first-team All-NBA member and retired as the all-time leading NBA scorer. His 16.2 per-game rebound average is third in league history, behind [[Bill Russell]] and [[Wilt Chamberlain]].<ref>{{cite web|url=http://www.nba.com/history/players/pettit_bio.html|title=NBA.com: Bob Pettit Bio|publisher=|accessdate=March 20, 2016}}</ref> 

In 1956, Kerner drafted Hall of Fame player [[Bill Russell]] as the second pick in the first round and traded him to the [[Boston Celtics]] for [[Cliff Hagan]] and former St. Louis University star [[Ed Macauley]].<ref name="basketball-reference.com"/><ref>{{cite web|url=http://www.nba.com/history/players/russell_bio.html|title=NBA.com: Bill Russell Bio|publisher=|accessdate=March 20, 2016}}</ref> Russell replaced Auerbach as coach of the Celtics, winning two titles as player-coach.<ref>{{cite web|url=http://www.basketball-reference.com/coaches/russebi01c.html|title=Bill Russell|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref>

[[File:Bill Russell and Red Auerbach 1956.jpeg|thumb|alt=Bill Russell and Red Auerbach watching a game from the bench|Bill Russell and Red Auerbach in 1956]]
From 1953-54 to 1956-57, the Hawks were coached by Hall of Fame coach [[Red Holzman]]. Holzman was replaced in 1956-57 by Hall of Fame coach [[Alex Hannum]].<ref>[[Naismith Memorial Basketball Hall of Fame]]</ref> Holzman later won two NBA championships with the [[New York Knicks]],<ref>{{cite web|url=http://www.basketball-reference.com/coaches/holzmre01c.html|title=Red Holzman|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref> and Hannum led the Hawks to the NBA championship before he was fired after the title season.<ref name="sportsecyclopedia.com">{{cite web|url=http://www.sportsecyclopedia.com/nba/stlhawks/stlhawks.html|title=St. Louis Hawks (1955-1968)|publisher=|accessdate=March 20, 2016}}</ref>

In 1960, Kerner drafted Hall of Fame player and coach [[Lenny Wilkens]] as the sixth pick of the first round.<ref name="basketball-reference.com"/> After retiring as a player, Wilkens coached for 32 NBA seasons and won over 1,300 games.<ref>{{cite web|url=http://www.basketball-reference.com/players/w/wilkele01.html|title=Lenny Wilkens|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref>

=={{anchor|Hawks - Celtics Rivalry}}Hawks-Celtics rivalry==
Kerner had a number of ties to the Boston Celtics; he had employed Celtics coach Red Auerbach, drafted Bob Cousy and Bill Russell and obtained former Celtics [[Cliff Hagan]] and [[Ed Macauley]]. By the late 1950s, the teams had met three times in the NBA finals; Kerner's Hawks were built around four Hall of Fame players: Hagan, Macauley, [[Med Park]] and Bob Pettit.<ref>"Rags to Riches Story For Ben Kerner"Bill Lutwein, Milwaukee Journal April a5, 1958p.21</ref><ref name="mosportshalloffame.com">{{cite web|url=http://mosportshalloffame.com/inductees/ben-kerner/|title=Ben Kerner  - Missouri Sports Hall of Fame|work=Missouri Sports Hall of Fame|accessdate=March 20, 2016}}</ref>

The [[1957 NBA Finals]] went to seven games as the Hawks lost to the Celtics' Auerbach, Russell and Cousy. During the finals, Auerbach and Kerner confronted each other on the court in a dispute over the height of the basket and Auerbach punched Kerner. Although he was not ejected, Auerbach was later fined $300 for the incident.<ref>{{cite web|url=https://lockoutschmockout.wordpress.com/2011/09/21/the-5-most-important-punches-in-nba-history-part-i/|title=The 5 Most Important Punches in NBA History, Part I|work=lockoutschmockout|accessdate=March 20, 2016}}</ref><ref>"Tall Tales: The Glory Years of the NBA" 1992 Terry Pluto, Simon & Schusterp.138-139</ref>

=={{anchor|1958 NBA Champions}}1958 NBA championship==
The next season gave Kerner and the Hawks their championship, as the Hawks and Celtics met in the [[1958 NBA Finals]] for the second consecutive year. This time the Hawks won, defeating the Celtics four games to two. Pettit scored 50 points in the deciding game, tipping in the final basket for a 110-109 victory at home.<ref name="sportsecyclopedia.com"/><ref>{{cite web|url=http://www.nba.com/history/finals/19571958.html|title=NBA.com: Pettit Drops 50 on Celtics in Game 6|publisher=|accessdate=March 20, 2016}}</ref>

The Celtics, still coached by Auerbach, and the Hawks (coached by Ed Macauley) met for a third time in the [[1960 NBA Finals]]. The finals went seven games, with the Celtics winning game seven 122-103 at the [[Boston Garden]]. Pettit averaged 25 points per game during the series.<ref>{{cite web|url=http://www.basketball-reference.com/playoffs/1960-nba-finals-hawks-vs-celtics.html|title=1960 NBA Finals - Basketball-Reference.com|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref>

==Sale and move to Atlanta==
After the 1967-68 season, Kerner sold the St. Louis Hawks to Thomas Cousins and former Georgia governor [[Carl Sanders]]. The new owners moved the team to [[Atlanta]], where they remain as the [[Atlanta Hawks]].<ref name="sportsecyclopedia.com"/>

=={{anchor|Franchise venues}}Venues==
Under Kerner's ownership, the [[Tri-Cities Blackhawks]] played at [[Wharton Field House]] in Moline, Illinois,<ref>{{cite web|url=http://www.basketball-reference.com/teams/TRI/1951.html|title=1950-51 Tri-Cities Blackhawks|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref> and the [[Milwaukee Hawks]] played at [[Milwaukee Arena]].<ref name="basketball-reference.com1">{{cite web|url=http://www.basketball-reference.com/teams/MLH/1952.html|title=1951-52 Milwaukee Hawks|work=Basketball-Reference.com|accessdate=March 20, 2016}}</ref> The [[St. Louis Hawks]] played at [[Kiel Auditorium]], and occasionally at the [[St. Louis Arena]].<ref name="basketball-reference.com1"/>

==Cultural influence==
A book about the St. Louis Hawks by Greg Marecek, ''Full Court: The Untold Stories of the St. Louis Hawks'', was published in 2006.<ref>http://www.websterkirkwoodtimes.com/pdalpeditorial.lasso?-token.story=170188.113118</ref>

=={{anchor|Personal life|Awards}}Awards and personal life==
Kerner died on November 22, 2000, and is buried in Mt. Sinai Cemetery in [[Affton, Missouri]]. He and his wife, Irma Jean, had two sons: Ben Jr. and Kyle.<ref>https://business.highbeam.com/435553/article-1G1-67283382/ben-kerner-19132000-exhawks-owner-helped-shape-city</ref><ref>{{cite web|url=http://www.legacy.com/obituaries/stltoday/obituary.aspx?n=ima-jean-kerner&pid=152377371|title=Ima Kerner Obituary - Saint Louis, MO - St. Louis Post-Dispatch|work=St. Louis Post-Dispatch|accessdate=March 20, 2016}}</ref><ref>{{cite web|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=37367779|title=Benjamin "Ben" Kerner (1913 - 2000) - Find A Grave Memorial|publisher=|accessdate=March 20, 2016}}</ref> Kerner was inducted into the [[Missouri Sports Hall of Fame]] in 1992,<ref name="mosportshalloffame.com"/> and the St. Louis Sports Hall of Fame in 2015.<ref name="stlshof.com">https://www.stlshof.com/index.php?option=com_content&view=article&id=218&Itemid=890 {{dead link|date=January 2017}}</ref>

==References==
{{reflist|30em}}

{{1957-58 NBA season by team}}
{{St. Louis Hawks 1957–58 NBA champions}}
{{Atlanta Hawks seasons}}
{{NBAOwners}}
{{NBA}}
{{NBA Champions}}

{{DEFAULTSORT:Kerner, Benjamin}}
[[Category:1913 births]]
[[Category:2000 deaths]]
[[Category:Atlanta Hawks owners]]
[[Category:Milwaukee Hawks]]
[[Category:National Basketball Association executives]]
[[Category:National Basketball Association history]]
[[Category:National Basketball Association owners| ]]
[[Category:Sportspeople from St. Louis]]
[[Category:St. Louis]]
[[Category:Tri-Cities Blackhawks]]