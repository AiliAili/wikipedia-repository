{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Blood
| Type        = studio
| Artist      = [[OSI (band)|OSI]]
| Cover       = OSI Blood Cover.jpg
| Alt         = Faint, red blood droplets cover the top half of the otherwise black cover. The OSI logo, in red, is in the bottom right-hand corner.
| Released    = April 27, 2009
| Recorded    = 2008
| Genre       = [[Progressive metal]]
| Length      = 47:03
| Label       = [[InsideOut Music|InsideOut]]
| Producer    = [[Jim Matheos]], [[Kevin Moore]]
| Last album  = ''[[Free (OSI album)|Free]]''<br />(2006)
| This album  = '''''Blood'''''<br />(2009)
| Next album  = ''[[Fire Make Thunder]]''<br />(2012)
}}

'''''Blood''''' is the third [[studio album]] by American [[progressive rock]] band [[OSI (band)|OSI]], released by [[InsideOut Music]] on April 27, 2009 in Europe and May 19, 2009 in North America.

Guitarist [[Jim Matheos]] and keyboardist and vocalist [[Kevin Moore]] started work on the album in 2008, collaborating by [[email]]. Matheos would send Moore a song idea which Moore would edit and send back to Matheos. [[Mike Portnoy]] of [[Dream Theater]], who performed drums on ''[[Office of Strategic Influence (album)|Office of Strategic Influence]]'' and ''[[Free (OSI album)|Free]]'', was replaced by [[Porcupine Tree]] drummer [[Gavin Harrison]] on ''Blood''. Matheos played bass guitar on the album, having hired guest musicians to perform bass duties on the first two OSI albums. [[Mikael Åkerfeldt]] of [[Opeth]] and [[Tim Bowness]] of [[No-Man]] wrote lyrics and performed vocals on one track each.

Critical reception of ''Blood'' was generally positive. The more atmospheric and [[ambient music|ambient]] tracks were praised; the more [[Heavy metal music|metal]]-oriented tracks received mixed reactions. Moore's lyrics and Harrison's drumming, in particular, were met with acclaim.

==Background==
On September 4, 2008, keyboardist and vocalist [[Kevin Moore]] posted a news update to the official [[Chroma Key]] website. In it, he stated that he had been working with guitarist [[Jim Matheos]] on a third OSI album "for several months now, slowly but surely."<ref name = chromakey>{{cite web | last = Moore | first = Kevin | url = http://www.chromakey.com/ | title = OSI III | work = Chroma Key - News | date = 2008-09-04 | accessdate = 2010-07-14 }}</ref> Moore later stated that with OSI, he and Matheos "usually play it by ear. We never really know if we're going to do another OSI, we never agree on it. I don't remember ever agreeing to start a new one, really. But Jim will send me an idea or something and we just start talking about it."<ref name = brave>{{cite web | last = Perri | first = David | url = http://www.bravewords.com/news/113604 | title = OSI – "Almost Natural Enemies…" | work = Bravewords.com | date = 2009-05-14 | accessdate = 2010-04-15 }}</ref>

''Blood'' was written and recorded in the same way as the first two OSI albums. Matheos and Moore worked together on the album long-distance, mainly by [[email]]ing each other files. Matheos would send Moore song ideas, "from just a guitar riff to elaborate, almost completed songs," Moore said. "Then I ask what I can do, and I mess around, complete a verse and add a chorus, do editing, add some vocals, and send it back to him."<ref name = AboutInt>{{cite web | last = Marsicano | first = Dan | url = http://heavymetal.about.com/od/interviews/a/osiinterview.htm | title = OSI Interview: A Conversation With Vocalist/Keyboardist Kevin Moore | work = [[About.com]] | date = 2009-05-26 | accessdate = 2010-04-15 }}</ref>

Moore considered the writing of ''Blood'' to be harder compared to previous OSI albums. Previously, Matheos would send Moore guitar parts to work with, allowing Moore to "work with editing them, pitching them, and fucking about them different ways, and then programming drums and keyboards."<ref name = MadeLoud>{{cite web | last = Reilly | first = Andrew | url = http://www.madeloud.com/articles/interviews/madeloud-interview-jim-matheos-and-kevin-moore-osi | title = MadeLoud Interview: Jim Matheos and Kevin Moore of OSI | work = MadeLoud | date = 2009-05-19 | accessdate = 2010-04-15 }}</ref> When working on ''Blood'', Matheos also provided programming, keyboard and drum parts. "If he sent me stuff that already had drums and keyboard parts, and it wasn't a complete idea, I had to figure out a way to elaborate on it without having that same equipment that he has there," Moore said.<ref name = MadeLoud /> Overall, he considered the writing process to be "smoother because we got on a roll and we're more fluid together".<ref name = AboutInt />

[[Mike Portnoy]] of [[Dream Theater]] played drums on ''[[Office of Strategic Influence (album)|Office of Strategic Influence]]'' and ''[[Free (OSI album)|Free]]'', but found the experience frustrating.<ref name = lifting>{{Cite book  | last = Wilson | first = Rich | authorlink = Rich Wilson (journalist) | title = [[Lifting Shadows|Lifting Shadows: The Authorized Biography of Dream Theater]] | publisher = Essential Works | year = 2009 | location = London | pages = 421 | isbn = 978-1-906615-02-4}}</ref> Moore regarded Portnoy's contributions to the first two albums as "great",<ref name = brave /> but he and Matheos decided to recruit a different drummer for ''Blood''. "We were continuing to try to get new voices involved with the album", he explained.<ref name = brave /> Matheos, a fan of [[Porcupine Tree]], was in control of personnel on ''Blood'' and asked Porcupine Tree drummer [[Gavin Harrison]] to perform drums on the album.<ref name = brave /> Harrison had a schedule gap in the fourth quarter of 2008, so agreed to work on the album.<ref name = MadeLoud /> He never met Matheos and Moore, recording his drums parts in his recording studio in England and exchanging files with them over the Internet.<ref name = AboutInt /> Most songs were sent to Harrison with programmed drums. "For the most part, we usually told him to try to stay away from the programmed drums and come up with his own parts, and he did. So much of that is his taking it in a different direction," Moore said. "There was probably at least one song, maybe two, where we said 'We'd like you to keep closer to the programmed drums,' but he had a lot of say in what went on."<ref name = MadeLoud /> Matheos and Moore were "very happy"<ref name = katehizis /> with Harrison's work on ''Blood'', and hoped to work with him on the fourth OSI album.<ref name = katehizis>{{cite web | url = http://katehizis.com/publications/interview.php?i=fates_warning2010 | title = Интервю с Jim Matheos | work = Metal Katehizis | language = Bulgarian | accessdate = 2010-07-14}}</ref> ''Blood'' marked the first time Matheos played bass on an OSI album instead of recruiting an outside bassist.<ref name = chromakey />

Matheos asked [[Opeth]] vocalist and guitarist [[Mikael Åkerfeldt]] to write lyrics and perform vocals on one track, which would become "Stockholm". Matheos and Moore sent him the track they wanted him to work on, and he sent back a rough mix of his vocals. "We were really happy with it," Moore said. "We didn't do any revisions or ask for any changes, like we were expecting to... It was a different approach than I would have taken, and that's what was refreshing about it. That's why we wanted to get somebody else to do some vocals on the album."<ref name = brave /> [[Tim Bowness]] of [[No-Man]] wrote lyrics and performed vocals on "No Celebrations", a bonus track.<ref name = AboutInt />

==Music==
Moore regarded ''Blood'' as "[distilling] the OSI vibe that we've been going for all along with this album".<ref name = brave /> He noted that there was "conflict"<ref name = AboutInt /> due to his and Matheos' different musical tastes, "but it's not a personality conflict. It's a musical conflict where we want different things to happen and have to work it musically."<ref name = AboutInt /> Moore considered this conflict to be "pretty much the only thing that makes OSI relatively original. If it was just one way or the other, like the singer-songwriter stuff, [[electronic music]] or [[progressive metal]], there's a lot of people doing those things already."<ref name = AboutInt />

Moore stated that he found writing songs which combined progressive metal and electronic music easier than writing songs confined to one genre. "There's people who do that better. Working with the two genres, they're different enough from each other – almost natural enemies – that it keeps it interesting," he said. "It doesn't feel like being in conquered territory, it feels like there's still room to explore. There's always conflicts between the two writing elements that keeps it interesting."<ref name = brave />

When writing lyrics for previous releases, Moore would "start mumbling and I try to figure out what I'm saying and then try to make it make sense".<ref name = AboutInt /> With ''Blood'', he "really wrote them down and tried to make them coherent. I didn't want it to be like 'Oh, you get your own impression of the lyrics. Everybody has their own idea!' I wanted to have an idea that I wanted to communicate, and something communicable."<ref name = MadeLoud /> Moore noted that he did occasionally use his "mumbling" method as it "puts you in the right direction as far as sounds, vowel sounds, and stresses on certain syllables. And if you can write some words and lyrics that match that, sometimes it flows really well."<ref name = MadeLoud /> He described all the songs as based on "personal experiences".<ref name = brave /> "I'll start writing a song that has a little bit of a world view or political view or something," he said, "but then by the time I'm finished the lyrics it'll be about a relationship or something like that."<ref name = brave />

Matheos recorded his parts in ''[[Pro Tools]]'' using a FocusRite Saffire PRO 40 [[preamp]]. Matheos primarily used [[PRS Guitars|PRS]] guitars and [[Mesa Boogie]] amplifiers. Moore used ''[[Ableton Live]]'' to write music and record his vocals. The only keyboard he used was a [[Minimoog Voyager]].<ref name = MadeLoud />

==Release and promotion==
''Blood'' was released by [[InsideOut Music]] on April 27, 2009 in Europe and May 19 in North America.<ref name = iout>{{cite web | url = http://www.insideout.de/catalog/product_info.php?products_id=640&osCsid=031b3442cdceb8660441f3773cd0f168 | title = OSI: Blood Special Edition | work = [[InsideOut Music]] | accessdate = 2010-07-14 }}</ref> Matheos and Moore unsuccessfully tried to organise a tour in support of ''Free''. "We sort of disappointed people by saying that we were trying to making [a tour] happen and we wanted to make it happen," Moore said, "so this time we're not going to say that stuff."<ref name = brave /> Moore stated that OSI would remain a studio-only project "until further notice".<ref name = brave />

The special edition of ''Blood'' came with a bonus disc featuring three extra tracks. [[Tim Bowness]] (of [[No-Man]]) wrote lyrics and performed vocals on the first song, "No Celebrations". The second track is a cover of "Christian Brothers" by [[Elliott Smith]]. The third track is "Terminal (Endless)", an extended version of the regular CD track "Terminal". "It still has something interesting drum stuff where Gavin just plays steady beats with variations on it," Moore said. "We had some mercy on the album version, but on the bonus CD, we don't have any mercy and we just let it go."<ref name = AboutInt />

==Reception==
{{Album ratings
| rev1 = [[Allmusic]]
| rev1Score = {{rating|3.5|5}}<ref name = allmusic>{{cite web | last = Henderson | first = Alex | url = {{Allmusic|class=album|id=r1552931|pure_url=yes}} | title = Blood | work = [[Allmusic]] | accessdate = 2010-07-15 }}</ref>
| rev2 = [[Blogcritics]]
| rev2Score = (favorable)<ref name = blogcritics>{{cite web | last = Dodge | first = Marty | url = http://blogcritics.org/music/article/music-reviews-white-lion-moody-blues/page-2/ | title = Music Reviews: White Lion, Moody Blues, Hardline, OSI, Joe Lynn Turner - Page 2 | work = [[Blogcritics]] | date = 2009-05-22 | accessdate = 2010-07-15 }}</ref>
| rev3 = MadeLoud
| rev3Score = {{rating|4|5}}<ref name = maderev>{{cite web | last = Reilly | first = Andrew | url = http://www.madeloud.com/node/6767 | title = OSI - Blood <nowiki>|</nowiki> Underground Band Reviews | work = MadeLoud | date = 2009-06-05 | accessdate = 2010-07-15 }}</ref>

}}

Critical reception of ''Blood'' was generally positive. Writing for [[Blogcritics]], Marty Dodge praised the album as "jaw droppingly impressive",<ref name = blogcritics /> considering it to "[have] the potential to be up there in top albums of the year".<ref name = blogcritics /> Andrew Reilly of MadeLoud lauded ''Blood'' as OSI's best album, although considered other individual songs from the band's catalog to be stronger than those on the album.<ref name = maderev /> Alex Henderson of [[Allmusic]] regarded ''Blood'' as "an album that falls short of earth-shattering but is still solid and worthwhile".<ref name = allmusic />

Reilly described ''Blood'' as "fully mastering the [[techno]]-informed brand of progressive metal so many others have pursued to no avail... with ''Blood'' the two have finally found the stylistic fusion their first two discs hinted at".<ref name = maderev /> Henderson described the sound of the album as "[[Pink Floyd]] and [[King Crimson]] by way of [[Radiohead]], [[Nirvana (band)|Nirvana]], [[grunge]], [[alternative metal|alt-metal]] and [[alternative rock|alt rock]]."<ref name = allmusic />

Reilly lauded Moore's lyrics, describing them as closer to his work in [[Dream Theater]] and [[Chroma Key]] than on the first two OSI albums. "Moore turns conventional phrases... into thinly-veiled warnings to some unknown subject," Reilly commented. "OSI has long been Moore's avenue for exploring the political as the personal, but here those two subjects fully cannibalize each other which, given this group's past efforts and considerable talents, would not at all be an impossible intention or unforeseen consequence."<ref name = maderev /> He praised Gavin Harrison's drum performance, describing it as "phenomenal".<ref name = maderev /> He considered "Stockholm" and "Blood" to be the highlights of the album.<ref name = maderev /> Dodge praised "Radiologue" as "stunning".<ref name = blogcritics />

==Track listing==
{{Tracklist
| all_music = [[Jim Matheos]] and [[Kevin Moore]], except where noted
| all_lyrics = [[Kevin Moore]], except where noted
| total_length = 47:17
| title1 = The Escape Artist
| length1 = 5:51
| title2 = Terminal
| length2 = 6:29
| title3 = False Start
| length3 = 3:04
| title4 = We Come Undone
| length4 = 4:03
| title5 = Radiologue
| length5 = 6:05
| title6 = Be The Hero
| length6 = 5:51
| title7 = Microburst Alert
| note7 = Matheos; instrumental
| length7 = 3:49
| title8 = Stockholm
| note8 = Lyrics: [[Mikael Åkerfeldt]]
| length8 = 6:41
| title9 = Blood
| length9 = 5:24
}}

;Special edition bonus disc
{{Tracklist
| total_length = 21:21
| title1 = No Celebrations
| note1 = Lyrics: [[Tim Bowness]]
| length1 = 6:27
| title2 = Christian Brothers
| note2 = [[Elliott Smith]] [[cover version|cover]]
| length2 = 4:34
| title3 = Terminal (Endless)
| length3 = 10:21
}}

==Personnel==
* [[Jim Matheos]]&nbsp;– [[guitar]], [[bass guitar|bass]], [[keyboard instrument|keyboards]], programming
* [[Kevin Moore]]&nbsp;– [[singing|vocals]], keyboards, programming
* [[Gavin Harrison]]&nbsp;– [[Drum kit|drums]]
* [[Mikael Åkerfeldt]]&nbsp;– vocals on "Stockholm"
* [[Tim Bowness]]&nbsp;– vocals on "No Celebrations"
*Produced by Jim Matheos and Kevin Moore
*Mixed by [[Phil Magnotti]], Jim Matheos and Kevin Moore

== References ==
{{reflist}}

==External links==
* [http://www.osiband.com/ Official OSI website], featuring samples from ''Blood''

{{OSI (band)}}

{{Good article}}

{{DEFAULTSORT:Blood (Osi Album)}}
[[Category:2009 albums]]
[[Category:OSI (band) albums]]
[[Category:Inside Out Music albums]]