{{refimprove|date=December 2011}}
[[Image:Bode High-Pass.PNG|350px|thumb|right|Figure 1(a): The Bode plot for a first-order (one-pole) [[highpass filter]]; the straight-line approximations are labeled "Bode pole"; phase varies from 90° at low frequencies (due to the contribution of the numerator, which is 90° at all 
frequencies) to 0° at high frequencies (where the phase contribution of the denominator is &minus;90° and cancels the contribution of the numerator).]]
[[Image:Bode Low-Pass.PNG|350px|thumb|right|Figure 1(b): The Bode plot for a first-order (one-pole) [[lowpass filter]]; the straight-line approximations are labeled "Bode pole"; phase is 90° lower than for Figure 1(a) because the phase contribution of the numerator is 0° at all frequencies.]]

In [[electrical engineering]] and [[control theory]], a '''Bode plot''' {{IPAc-en|ˈ|b|oʊ|d|i|}} is a [[plot (graphics)|graph]] of the [[frequency response]] of a system.  It is usually a combination of a '''Bode magnitude plot,''' expressing the magnitude (usually in [[decibel]]s) of the frequency response, and a '''Bode phase plot,''' expressing the [[phase (waves)|phase shift]]. Both quantities are plotted against a horizontal axis proportional to the [[logarithm]] of frequency. Given that the [[decibel]] is itself a [[logarithmic scale]], the Bode amplitude plot is [[log–log plot]], whereas the Bode phase plot is a [[semi-log plot|lin-log]] plot.<ref name="Feucht1990">{{cite book|author=Dennis L. Feucht|title=Handbook of Analog Circuit Design|year=1990|publisher=Elsevier Science|isbn=978-1-4832-5938-3|page=194}}</ref>

As originally conceived by [[Hendrik Wade Bode]] in the 1930s, the plot is only an [[asymptotic]] [[approximation]] of the frequency response, [[piecewise linear function|using straight line segments]].<ref name="Yarlagadda2010">{{cite book|author=R. K. Rao Yarlagadda|title=Analog and Digital Signals and Systems|year=2010|publisher=Springer Science & Business Media|isbn=978-1-4419-0034-0|page=243}}</ref> However, with the advent of low cost computing, it is often taken nowadays to mean the precise plot of the actual frequency response.

== Overview ==

Among his several important contributions to circuit theory and control theory, engineer [[Hendrik Wade Bode]], while working at Bell Labs in the United States in the 1930s, devised a simple but accurate method for graphing gain and phase-shift plots. These bear his name, ''Bode gain plot'' and ''Bode phase plot''.  "Bode" is pronounced {{IPAc-en|ˈ|b|oʊ|d|i}} {{respell|BOH|dee}} ({{IPA-nl|ˈboːdə|lang}}).<ref name="Van Valkenburg">Van Valkenburg, M. E. University of Illinois at Urbana-Champaign, "In memoriam: Hendrik W. Bode (1905-1982)", [[IEEE]] Transactions on Automatic Control, Vol. AC-29, No 3., March 1984, pp. 193-194. Quote: "Something should be said about his name. To his colleagues at Bell Laboratories and the generations of engineers that have followed, the pronunciation is boh-dee. The Bode family preferred that the original Dutch be used as boh-dah."</ref><ref>{{cite web|title=Vertaling van postbode, NL>EN|url= http://www.mijnwoordenboek.nl/vertalen.php?s1=&s2=NL+%3E+EN&s3=NL+%3E+EN&woord=postbode|publisher=mijnwoordenboek.nl|accessdate=2013-10-07}}</ref>

Bode was faced with the problem of designing stable amplifiers with [[feedback]] for use in telephone networks. He developed the graphical design technique of the Bode plots to show the [[gain margin]] and [[phase margin]] required to maintain stability under variations in circuit characteristics caused during manufacture or during operation.<ref>David A. Mindell ''Between Human and Machine: Feedback, Control, and Computing Before Cybernetics'' JHU Press, 2004 ISBN 0801880572,  pp. 127-131</ref> The principles developed were applied to design problems of [[servomechanism]]s and other feedback control systems. The Bode plot is an example of analysis in the [[frequency domain]].

== Definition ==
The Bode plot for a [[LTI system theory|linear, time-invariant]] system with [[transfer function]] <math> H(s) </math> (<math>s</math> being the complex frequency in the [[Laplace domain]]) consists of a magnitude plot and a phase plot.

The '''Bode magnitude plot''' is the graph of the function <math> |H(s=j \omega)|</math> of frequency <math> \omega</math> (with <math>j</math> being the [[imaginary unit]]). The <math> \omega</math>-axis of the magnitude plot is logarithmic and the magnitude is given in [[decibel]]s, i.e., a value for the magnitude <math>|H|</math> is plotted on the axis at <math>20 \log_{10} |H|</math>.

The '''Bode phase plot''' is the graph of the [[Argument (complex analysis)|phase]] of the transfer function <math> \arg \left( H(s =j \omega) \right)</math> as a function of <math>\omega</math>, commonly expressed in [[Degree_(angle)|degrees]]. The phase is plotted on the same logarithmic <math>\omega</math>-axis as the magnitude plot, but the value for the phase is plotted on a linear vertical axis.

== Examples ==

=== Highpass Filter ===
The one-pole [[highpass filter|highpass]] [[RC circuit#Transfer functions|RC-filter]] has the transfer function

::<math>\mathrm{H_{\mathrm{hp}}}(s) = \frac{ s \tau_c }{ 1 +  s \tau_c } \; ,</math>

where <math>\tau_c = 1/\omega_c </math> is the inverse of the cut-off frequency <math> \omega_c = 2 \pi f_c = \frac{1}{RC}</math>.

==== Magnitude Plot ====

To obtain the Bode magnitude plot one calculates the magnitude of the transfer function along the imaginary axis

::<math>\left|\mathrm{H_{\mathrm{hp}}}(\mathrm j \omega)\right| = \left|\frac{ \mathrm{j} \omega \tau_c }{ 1 + \mathrm{j} \omega \tau_c } \right| = \frac{\omega \tau_c }{\sqrt{ 1 +  (\omega \tau_c)^2} }\; .</math>

The last equality is obtained using the rules for [[complex number]]s. On the Bode magnitude plot, decibels are used, and the plotted magnitude is:

:<math>A_{\mathrm{hp}} := 20 \log_{10} \left|\mathrm{H_{\mathrm{hp}}}(\mathrm j \omega)\right| = 20 \log_{10} \left( \omega \tau_c \right) -20 \log_{10} \left( \sqrt{ 1 + \left( \omega \tau_c \right)^2 } \right) \; .</math>

==== Phase Plot ====
To obtain the Bode phase plot one calculates the [[Argument (complex analysis)|argument]] of <math>\mathrm{H_{\mathrm{hp}}}(\mathrm j \omega) </math>:

::<math>\arg H_{\mathrm{hp}}(\mathrm j \omega) = \arg \left(\frac{ \mathrm{j} \omega \tau_c }{ 1 + \mathrm{j} \omega \tau_c } \right) = 90^\circ - \tan^{-1} \left( \omega \tau_c \right) \; .</math>

Care must be taken that the inverse tangent is set up to return ''degrees'', not radians. The resulting Bode plot is shown in Figure 1(a) for <math>f_c =100 \mathrm{Hz}</math>.

=== Lowpass Filter ===

A passive (unity pass band gain) [[lowpass]] [[RC circuit|RC filter]] has the following [[transfer function]]:

::<math>H_{\mathrm{lp}}(s) = \frac{1}{1 + s \tau_c} \; ,</math>

where, again, <math>\tau_c</math> is the inverse of the cut-off frequency. The corresponding Bode Plot is shown in Figure 1(b).

The magnitude and the phase of the transfer function are related. For [[minimum-phase]] systems there is a unique relationship between the magnitude and the phase, the [[Bode gain-phase relationship]].<ref name=multivar_fb_control>{{cite book|last1=Skogestad|first1=Sigurd|last2=Postlewaite|first2=Ian|title=Multivariable Feedback Control|date=2005|publisher=John Wiley & Sons, Ltd.|location=Chichester, West Sussex, England|isbn=0-470-01167-X}}</ref>

If the transfer function is a [[rational function]] with real poles and zeros, then the Bode plot can be approximated with straight lines. These asymptotic approximations are called '''straight line Bode plots''' or '''uncorrected Bode plots''' and are useful because they can be drawn by hand following a few simple rules.  Simple plots can even be predicted without drawing them.

The approximation can be taken further by ''correcting'' the value at each cutoff frequency. The plot is then called a '''corrected Bode plot'''.

==Frequency Response and Bode Plot==
This section illustrates that a Bode Plot is a visualization of the frequency response of a system.

Consider a [[LTI system theory|linear, time-invariant]] system with transfer function <math>H(s)</math>. Assume that the system is subject to a sinusoidal input with frequency <math>\omega</math>,

::<math>u(t) = \sin (\omega t) \; ,</math>

that is applied persistently, i.e. from a time <math>-\infty</math> to a time <math>t</math>. The response will be of the form

::<math>y(t) = y_0 \sin (\omega t + \varphi) \;, </math>

i.e., also a sinusoidal signal with amplitude <math>y_0</math> shifted in phase with respect to the input by a phase <math>\varphi</math>.

It can be shown<ref name=multivar_fb_control /> that the magnitude of the response is
{{NumBlk|::|<math>y_0 = |H(\mathrm{j} \omega)| \; </math>|{{EquationRef|1}}}}
and that the phase shift is
{{NumBlk|::|<math>\varphi = \arg H(\mathrm{j} \omega) \;. </math>|{{EquationRef|2}}}}
A sketch for the proof of these equations is given in the [[Bode Plot#Proof Sketch for the Relation to Frequency Response|appendix]].

In summary, subjected to an input with frequency <math>\omega</math> the system responds  at the same frequency with an output that is amplified by a factor <math>|H(\mathrm{j} \omega)|</math> and phase-shifted by <math>\arg( H(\mathrm{j} \omega) )</math>. These quantities, thus, characterize the frequency response and are shown in the Bode plot.

==Rules for handmade Bode plot==

For many practical problems, the detailed Bode plots can be approximated with straight-line segments that are [[asymptote]]s of the precise response. The effect of each of the terms of a multiple element [[transfer function]] can be approximated by a set of straight lines on a Bode plot.  This allows a graphical solution of the overall frequency response function. Before widespread availability of digital computers, graphical methods were extensively used to reduce the need for tedious calculation; a graphical solution could be used to identify feasible ranges of parameters for a new design.

The premise of a Bode plot is that one can consider the log of a function in the form:
:<math>f(x) = A \prod (x - c_n)^{a_n}</math>

as a sum of the logs of its [[Pole (complex analysis)|poles]] and [[Zero (complex analysis)|zeros]]:
:<math>\log(f(x)) = \log(A) + \sum a_n \log(x - c_n) \; .</math>

This idea is used explicitly in the method for drawing phase diagrams. The method for drawing amplitude plots implicitly uses this idea, but since the log of the amplitude of each pole or zero always starts at zero and only has one asymptote change (the straight lines), the method can be simplified.

===Straight-line amplitude plot===
Amplitude decibels is usually done using <math>\mathbf{dB} = 20 \log_{10}(X)</math> to define decibels. Given a transfer function in the form

:<math>H(s) = A \prod \frac{(s - x_n)^{a_n}}{(s - y_n)^{b_n}}</math>

where <math>x_n</math> and <math>y_n</math> are constants, <math>s = \mathrm{j}\omega</math>, <math>a_n, b_n > 0</math>, and <math>H</math> is the transfer function:

* at every value of s where <math>\omega = x_n</math> (a zero), '''increase''' the slope of the line by <math>20 \cdot a_n \, \mathrm{dB}</math> per [[Decade (log scale)|decade]].
* at every value of s where <math>\omega = y_n</math> (a pole), '''decrease''' the slope of the line by <math>20 \cdot b_n \, \mathrm{dB}</math> per decade.
* The initial value of the graph depends on the boundaries. The initial point is found by putting the initial angular frequency <math>\omega</math> into the function and finding <math>|H(\mathrm{j}\omega)|</math>.
* The initial slope of the function at the initial value depends on the number and order of zeros and poles that are at values below the initial value, and are found using the first two rules.

To handle irreducible 2nd order polynomials, <math>ax^2 + bx + c</math> can, in many cases, be approximated as <math> (\sqrt{a}x + \sqrt{c})^2 </math>.

Note that zeros and poles happen when <math>\omega</math> is ''equal to'' a certain <math>x_n</math> or <math>y_n</math>. This is because the function in question is the magnitude of <math>H(\mathrm{j}\omega)</math>, and since it is a complex function, <math>|H(\mathrm{j}\omega)| = \sqrt{H \cdot H^* }</math>. Thus at any place where there is a zero or pole involving the term <math>(s + x_n)</math>, the magnitude of that term is <math>\sqrt{(x_n + \mathrm{j}\omega) \cdot (x_n - \mathrm{j}\omega)} = \sqrt{x_n^2 + \omega^2}</math>.

===Corrected amplitude plot===

To correct a straight-line amplitude plot:

* at every zero, put a point <math>3 \cdot a_n\ \mathrm{dB}</math>  '''above''' the line,
* at every pole, put a point <math>3 \cdot b_n\ \mathrm{dB}</math> '''below''' the line,
* draw a smooth curve through those points using the straight lines as asymptotes (lines which the curve approaches).

Note that this correction method does not incorporate how to handle complex values of <math>x_n</math> or <math>y_n</math>. In the case of an irreducible polynomial, the best way to correct the plot is to actually calculate the magnitude of the transfer function at the pole or zero corresponding to the irreducible polynomial, and put that dot over or under the line at that pole or zero.

=== Straight-line phase plot ===

Given a transfer function in the same form as above:

:<math>H(s) = A \prod \frac{(s - x_n)^{a_n}}{(s - y_n)^{b_n}}</math>

the idea is to draw separate plots for each pole and zero, then add them up. The actual phase curve is given by
<math>-\arctan \left( \tfrac{\mathrm{Im}[H(s)]}{\mathrm{Re}[H(s)]} \right)</math>.

To draw the phase plot, for '''each''' pole and zero:

* if <math>A</math> is positive, start line (with zero slope) at <math>0 \deg</math>
* if <math>A</math> is negative, start line (with zero slope) at <math>-180 \deg</math>
* if the sum of the number of unstable zeros and poles is odd, add 180 degrees to that basis
* at every <math> \omega = |x_n| </math> (for stable zeros – <math>\operatorname{Re}(z) < 0</math>), '''increase''' the slope by <math>45 \cdot a_n</math> degrees per decade, beginning one decade before <math> \omega = |x_n| </math> (E.g.: <math> \frac{|x_n|}{10} </math>)
* at every <math> \omega = |y_n| </math> (for stable poles – <math>\operatorname{Re}(p) < 0</math>), '''decrease''' the slope by <math>45 \cdot b_n</math> degrees per decade, beginning one decade before <math> \omega = |y_n| </math> (E.g.:  <math> \frac{|y_n|}{10} </math>)
* "unstable" (right half plane) poles and zeros (<math>\operatorname{Re}(s) > 0</math>) have opposite behavior
* flatten the slope again when the phase has changed by <math>90 \cdot a_n</math> degrees (for a zero) or <math>90 \cdot b_n</math> degrees (for a pole),
* After plotting one line for each pole or zero, add the lines together to obtain the final phase plot; that is, the final phase plot is the superposition of each earlier phase plot.

==Example==
To create a straight-line plot for the lowpass filter, introduced above, one considers the transfer function in terms of the angular frequency:
:<math>H_{\mathrm{lp}}(\mathrm{j} \omega)  = {1 \over 1 + \mathrm{j}{\omega \over {{\omega_\mathrm{c}}}}} \; .</math>
The above equation is the normalized form of the transfer function. The Bode plot is shown in Figure 1(b) above, and construction of the straight-line approximation is discussed next.

===Magnitude plot===
The magnitude (in [[decibel]]s) of the transfer function above, (normalized and converted to angular frequency form), given by the decibel gain expression <math>A_\mathrm{vdB}</math>:
:<math>\begin{align}
A_\mathrm{vdB} &= 20 \cdot \log|H_{\mathrm{lp}}(\mathrm{j}\omega)| \\
    &= 20 \cdot \log \frac{1}{\left| 1 + \mathrm{j} \frac{\omega}{\omega_\mathrm{c}} \right|} \\
    &= -20 \cdot \log \left| 1 + \mathrm{j} \frac{\omega}{\omega_\mathrm{c}} \right| \\
    &= -10 \cdot \log \left( 1 + \frac{\omega^2}{\omega_\mathrm{c}^2} \right)
\end{align}</math>
then plotted versus input frequency <math>\omega</math> on a logarithmic scale, can be approximated by two lines and it forms the asymptotic (approximate) magnitude Bode plot of the transfer function:
* for angular frequencies below <math>\omega_\mathrm{c}</math> it is a horizontal line at 0&nbsp;dB since at low frequencies the <math>{\omega \over {\omega_\mathrm{c}}}</math> term is small and can be neglected, making the decibel gain equation above equal to zero,
* for angular frequencies above <math>\omega_\mathrm{c}</math> it is a line with a slope of −20&nbsp;dB per decade since at high frequencies the <math>{\omega \over {\omega_\mathrm{c}}}</math> term dominates and the decibel gain expression above simplifies to <math>-20 \log {\omega \over {\omega_\mathrm{c}}}</math> which is a straight line with a slope of <math>- 20 \, \mathrm{dB}</math> per decade.

These two lines meet at the [[corner frequency]]. From the plot, it can be seen that for frequencies well below the corner frequency, the circuit has an attenuation of 0&nbsp;dB, corresponding to a unity pass band gain, i.e. the amplitude of the filter output equals the amplitude of the input. Frequencies above the corner frequency are attenuated – the higher the frequency, the higher the [[attenuation]].

===Phase plot===
The phase Bode plot is obtained by plotting the phase angle of the transfer function given by

: <math>\arg H_{\mathrm{lp}}(\mathrm{j} \omega) = -\tan^{-1}{\omega \over {\omega_\mathrm{c}}}</math>

versus <math>\omega</math>, where <math>\omega</math> and <math>\omega_\mathrm{c}</math> are the input and cutoff angular frequencies respectively. For input frequencies much lower than corner, the ratio <math> {\omega \over {\omega_\mathrm{c}}}</math> is small and therefore the phase angle is close to zero. As the ratio increases the absolute value of the phase increases and becomes –45 degrees when <math>\omega =\omega_\mathrm{c}</math>. As the ratio increases for input frequencies much greater than the corner frequency, the phase angle asymptotically approaches &minus;90 degrees. The frequency scale for the phase plot is logarithmic.

===Normalized plot===
The horizontal frequency axis, in both the magnitude and phase plots, can be replaced by the normalized (nondimensional) frequency ratio <math>{\omega \over {\omega_\mathrm{c}}}</math>. In such a case the plot is said to be normalized and units of the frequencies are no longer used since all input frequencies are now expressed as multiples of the cutoff frequency <math>\omega_\mathrm{c}</math>.

==An example with pole and zero==
Figures 2-5 further illustrate construction of Bode plots. This example with both a pole and a zero shows how to use superposition. To begin, the components are presented separately.

Figure 2 shows the Bode magnitude plot for a zero and a low-pass pole, and compares the two with the Bode straight line plots. The straight-line plots are horizontal up to the pole (zero) location and then drop (rise) at 20&nbsp;dB/decade. The second Figure 3 does the same for the phase. The phase plots are horizontal up to a frequency factor of ten below the pole (zero) location and then drop (rise) at 45°/decade until the frequency is ten times higher than the pole (zero) location. The plots then are again horizontal at higher frequencies at a final, total phase change of 90°.

Figure 4 and Figure 5 show how superposition (simple addition) of a pole and zero plot is done. The Bode straight line plots again are compared with the exact plots. The zero has been moved to higher frequency than the pole to make a more interesting example. Notice in Figure 4 that the 20&nbsp;dB/decade drop of the pole is arrested by the 20&nbsp;dB/decade rise of the zero resulting in a horizontal magnitude plot for  frequencies above the zero location. Notice in Figure 5 in the phase plot that the straight-line approximation is pretty approximate in the region where both pole and zero affect the phase. Notice also in Figure 5 that the range of frequencies where the phase changes in the straight line plot is limited to frequencies a factor of ten above and below the pole (zero) location. Where the phase of the pole and the zero both are present, the straight-line phase plot is horizontal because the 45°/decade drop of the pole is arrested by the overlapping 45°/decade rise of the zero in the limited range of frequencies where both are active contributors to the phase.

<gallery caption="Example with pole and zero" widths="300px" perrow="2">
Image:Bode Low Pass Magnitude Plot.PNG|Figure 2: Bode magnitude plot for zero and low-pass pole; curves labeled "Bode" are the straight-line Bode plots
Image:Bode Low Pass Phase Plot.PNG|Figure 3: Bode phase plot for zero and low-pass pole; curves labeled "Bode" are the straight-line Bode plots
Image:Bode Pole-Zero Magnitude Plot.PNG|Figure 4: Bode magnitude plot for pole-zero combination; the location of the zero is ten times higher than in Figures 2&3; curves labeled "Bode" are the straight-line Bode plots
Image:Bode Pole-Zero Phase Plot.PNG|Figure 5: Bode phase plot for pole-zero combination; the location of the zero is ten times higher than in Figures 2&3; curves labeled "Bode" are the straight-line Bode plots
</gallery>

==Gain margin and phase margin==
{{See also|Phase margin}}
Bode plots are used to assess the stability of [[negative feedback amplifier]]s by finding the gain and [[phase margin]]s of an amplifier. The notion of gain and phase margin is based upon the gain expression for a negative feedback amplifier given by

::<math>A_\mathrm{FB} = \frac {A_\mathrm{OL}} {1 + \beta A_\mathrm{OL}} \; ,</math>

where A<sub>FB</sub> is the gain of the amplifier with feedback (the '''closed-loop gain'''), β is the '''feedback factor''' and ''A''<sub>OL</sub> is the gain without feedback (the '''open-loop gain'''). The gain ''A''<sub>OL</sub> is a complex function of frequency, with both magnitude and phase.<ref group="note">Ordinarily, as frequency increases the magnitude of the gain drops and the phase becomes more negative, although these are only trends and may be reversed in particular frequency ranges. Unusual gain behavior can render the concepts of gain and phase margin inapplicable. Then other methods such as the [[Nyquist plot]] have to be used to assess stability.</ref> Examination of this relation shows the possibility of infinite gain (interpreted as instability) if the product β''A''<sub>OL</sub> = &minus;1. (That is, the magnitude of β''A''<sub>OL</sub> is unity and its phase is &minus;180°, the so-called [[Barkhausen stability criterion]]). Bode plots are used to determine just how close an amplifier comes to satisfying this condition.

Key to this determination are two frequencies. The first, labeled here as ''f''<sub>180</sub>, is the frequency where the open-loop gain flips sign. The second, labeled here ''f''<sub>0&nbsp;dB</sub>, is the frequency where the magnitude of the product | β ''A''<sub>OL</sub> | = 1 (in dB, magnitude 1 is 0&nbsp;dB). That is, frequency ''f''<sub>180</sub> is determined by the condition:

:<math>\beta A_\mathrm{OL} \left( f_{180} \right) = - | \beta A_\mathrm{OL} \left( f_{180} \right) | = - | \beta A_\mathrm{OL}|_{180} \; ,</math>

where vertical bars denote the magnitude of a complex number (for example, <math>|a + \mathrm{j}b| = \left[ a^2 + b^2 \right]^{\frac12}</math>), and frequency ''f''<sub>0&nbsp;dB</sub> is determined by the condition:

:<math>| \beta A_\mathrm{OL} \left( f_\mathrm{0dB} \right) | = 1 \; .</math>

One measure of proximity to instability is the '''gain margin'''. The Bode phase plot locates the frequency where the phase of β''A''<sub>OL</sub> reaches &minus;180°, denoted here as frequency ''f''<sub>180</sub>. Using this frequency, the Bode magnitude plot finds the magnitude of β''A''<sub>OL</sub>. If |β''A''<sub>OL</sub>|<sub>180</sub> = 1, the amplifier is unstable, as mentioned. If ''A''<sub>OL</sub>|<sub>180</sub> < 1, instability does not occur, and the separation in dB of the magnitude of |β''A''<sub>OL</sub>|<sub>180</sub> from |β''A''<sub>OL</sub>| = 1 is called the ''gain margin''. Because a magnitude of one is 0&nbsp;dB, the gain margin is simply one of the equivalent forms: <math>20 \log_{10} ( | \beta A_\mathrm{OL} |_{180} ) = 20 \log_{10} ( |A_\mathrm{OL}|) - 20 \log_{10} (\beta^{-1})</math>.

Another equivalent measure of proximity to instability is the '''[[phase margin]]'''. The Bode magnitude plot locates the frequency where the magnitude of |β''A''<sub>OL</sub>| reaches unity, denoted here as frequency ''f''<sub>0&nbsp;dB</sub>. Using this frequency, the Bode phase plot finds the phase of β''A''<sub>OL</sub>. If the phase of β''A''<sub>OL</sub>( ''f''<sub>0&nbsp;dB</sub>) > &minus;180°,  the instability condition cannot be met at any frequency (because its magnitude is going to be < 1 when ''f = f''<sub>180</sub>), and the distance of the phase at ''f''<sub>0&nbsp;dB</sub> in degrees above &minus;180° is called the ''phase margin''.

If a simple ''yes'' or ''no'' on the stability issue is all that is needed, the amplifier is stable if ''f''<sub>0&nbsp;dB</sub> < ''f''<sub>180</sub>. This criterion is sufficient to predict stability only for amplifiers satisfying some restrictions on their pole and zero positions ([[minimum phase]] systems). Although these restrictions usually are met, if they are not another method must be used, such as the [[Nyquist plot]].<ref name=Lee>
{{cite book 
|author=Thomas H. Lee
|title=The design of CMOS radio-frequency integrated circuits
|page=§14.6 pp. 451–453
|year= 2004
|edition=Second
|publisher=Cambridge University Press
|location=Cambridge UK
|isbn=0-521-83539-9
|url=http://worldcat.org/isbn/0-521-83539-9}}
</ref><ref name=Levine>
{{cite book 
|author=William S Levine
|title=The control handbook: the electrical engineering handbook series
|page=§10.1 p. 163
|year= 1996
|edition=Second
|publisher=CRC Press/IEEE Press
|location=Boca Raton FL
|isbn=0-8493-8570-9
|url=https://books.google.com/books?id=2WQP5JGaJOgC&pg=RA1-PA163&lpg=RA1-PA163&dq=stability+%22minimum+phase%22&source=web&ots=P3fFTcyfzM&sig=ad5DJ7EvVm6In_zhI0MlF_6vHDA}}
</ref>
Optimal gain and phase margins may be computed using [[Nevanlinna–Pick interpolation]] theory.<ref name=Tannenbaum>
{{cite book
|author=Allen Tannenbaum | authorlink = Allen Tannenbaum
|title=Invariance and Systems Theory: Algebraic and Geometric Aspects
|publisher=Springer-Verlag
|location=New York, NY
|isbn=9783540105657}}
</ref>

===Examples using Bode plots===
Figures 6 and 7 illustrate the gain behavior and terminology. For a three-pole amplifier, Figure 6 compares the Bode plot for the gain without feedback (the ''open-loop'' gain) ''A''<sub>OL</sub> with the gain with feedback ''A''<sub>FB</sub> (the ''closed-loop'' gain). See [[negative feedback amplifier]] for more detail.

In this example, ''A''<sub>OL</sub> = 100&nbsp;dB at low frequencies, and 1 / β = 58&nbsp;dB. At low frequencies, ''A''<sub>FB</sub> ≈ 58&nbsp;dB as well.

Because the open-loop gain ''A''<sub>OL</sub> is plotted and not the product β ''A''<sub>OL</sub>, the condition ''A''<sub>OL</sub> = 1 / β decides ''f''<sub>0&nbsp;dB</sub>. The feedback gain at low frequencies and for large ''A''<sub>OL</sub> is ''A''<sub>FB</sub> ≈ 1 / β (look at the formula for the feedback gain at the beginning of this section for the case of large gain ''A''<sub>OL</sub>), so an equivalent way to find ''f''<sub>0&nbsp;dB</sub> is to look where the feedback gain intersects the open-loop gain. (Frequency ''f''<sub>0&nbsp;dB</sub> is needed later to find the phase margin.)

Near this crossover of the two gains at ''f''<sub>0&nbsp;dB</sub>, the Barkhausen criteria are almost satisfied in this example, and the feedback amplifier exhibits a massive peak in gain (it would be infinity if β ''A''<sub>OL</sub> = −1). Beyond the unity gain frequency ''f''<sub>0&nbsp;dB</sub>, the open-loop gain is sufficiently small that ''A''<sub>FB</sub> ≈ ''A''<sub>OL</sub> (examine the formula at the beginning of this section for the case of small ''A''<sub>OL</sub>).

Figure 7 shows the corresponding phase comparison: the phase of the feedback amplifier is nearly zero out to the frequency ''f''<sub>180</sub> where the open-loop gain has a phase of −180°. In this vicinity, the phase of the feedback amplifier plunges abruptly downward to become almost the same as the phase of the open-loop amplifier. (Recall, ''A''<sub>FB</sub> ≈ ''A''<sub>OL</sub> for small ''A''<sub>OL</sub>.)

Comparing the labeled points in Figure 6 and Figure 7, it is seen that the unity gain frequency ''f''<sub>0&nbsp;dB</sub> and the phase-flip frequency ''f''<sub>180</sub> are very nearly equal in this amplifier, ''f''<sub>180</sub> ≈ ''f''<sub>0&nbsp;dB</sub> ≈ 3.332&nbsp;kHz, which means the gain margin and phase margin are nearly zero. The amplifier is borderline stable.

Figures 8 and 9 illustrate the gain margin and phase margin for a different amount of feedback β. The feedback factor is chosen smaller than in Figure 6 or 7, moving the condition | β ''A''<sub>OL</sub> | = 1 to lower frequency. In this example, 1 / β = 77&nbsp;dB, and at low frequencies ''A''<sub>FB</sub> ≈ 77&nbsp;dB as well.

Figure 8 shows the gain plot. From Figure 8, the intersection of 1 / β and ''A''<sub>OL</sub> occurs at  ''f''<sub>0&nbsp;dB</sub> = 1&nbsp;kHz. Notice that the peak in the gain ''A''<sub>FB</sub> near ''f''<sub>0&nbsp;dB</sub> is almost gone.<ref group="note">The critical amount of feedback where the peak in the gain ''just'' disappears altogether is the ''maximally flat'' or [[Butterworth filter#Maximal flatness|Butterworth]] design.</ref><ref name=Sansen>
{{cite book 
|author=Willy M C Sansen
|title=Analog design essentials
|pages=157–163
|year= 2006
|publisher=Springer
|location=Dordrecht, The Netherlands
|isbn=0-387-25746-2
|url=http://worldcat.org/isbn/0-387-25746-2}}
</ref>
 
Figure 9 is the phase plot. Using the value of ''f''<sub>0&nbsp;dB</sub> = 1&nbsp;kHz found above from the magnitude plot of Figure 8, the open-loop phase at ''f''<sub>0&nbsp;dB</sub> is −135°, which is a phase margin of 45° above −180°.

Using Figure 9, for a phase of −180° the value of ''f''<sub>180</sub> = 3.332&nbsp;kHz (the same result as found earlier, of course<ref group="note">The frequency where the open-loop gain flips sign ''f''<sub>180</sub> does not change with a change in feedback factor; it is a property of the open-loop gain. The value of the gain at ''f''<sub>180</sub> also does not change with a change in β. Therefore, we could use the previous values from Figures 6 and 7. However, for clarity the procedure is described using only Figures 8 and 9.</ref>). The open-loop gain from Figure 8 at ''f''<sub>180</sub> is 58&nbsp;dB, and 1 / β = 77&nbsp;dB, so the gain margin is 19&nbsp;dB.

Stability is not the sole criterion for amplifier response, and in many applications a more stringent demand than stability is good [[Step response#Step response of feedback amplifiers|step response]]. As a [[rule of thumb]], good step response requires a phase margin of at least 45°, and often a margin of over 70° is advocated, particularly where component variation due to manufacturing tolerances is an issue.<ref name=Sansen/> See also the discussion of phase margin in the [[Step response#Phase margin|step response]] article.

<gallery caption="Examples" widths="300px" perrow="2">
Image:Magnitude of feedback amplifier.PNG|Figure 6: Gain of feedback amplifier ''A''<sub>FB</sub> in dB and corresponding open-loop amplifier ''A''<sub>OL</sub>. Parameter 1/β = 58&nbsp;dB, and at low frequencies ''A''<sub>FB</sub> ≈ 58&nbsp;dB as well. The gain margin in this amplifier is nearly zero because &#124; β''A''<sub>OL</sub>&#124; = 1 occurs at almost ''f'' = ''f''<sub>180°</sub>.
Image:Phase of feedback amplifier.PNG|Figure 7: Phase of feedback amplifier ''°A''<sub>FB</sub> in degrees and corresponding open-loop amplifier ''°A''<sub>OL</sub>. The phase margin in this amplifier is nearly zero because the phase-flip occurs at almost the unity gain frequency ''f'' = ''f''<sub>0&nbsp;dB</sub> where &#124; β''A''<sub>OL</sub>&#124; = 1.
Image:Gain Margin.PNG|Figure 8: Gain of feedback amplifier ''A''<sub>FB</sub> in dB and corresponding open-loop amplifier ''A''<sub>OL</sub>. In this example, 1 / β = 77&nbsp;dB. The gain margin in this amplifier is 19&nbsp;dB.
Image:Phase Margin.PNG|Figure 9: Phase of feedback amplifier ''A''<sub>FB</sub> in degrees and corresponding open-loop amplifier ''A''<sub>OL</sub>. The phase margin in this amplifier is 45°.
</gallery>

==Bode plotter==
[[Image:Bodeplot.png|380px|thumb|right|Figure 10: Amplitude diagram of a 10th order [[Chebyshev filter]] plotted using a Bode Plotter application. The chebyshev transfer function is defined by poles and zeros which are added by clicking on a graphical complex diagram.]]
The Bode plotter is an electronic instrument resembling an [[oscilloscope]], which produces a Bode diagram, or a graph, of a circuit's voltage gain or phase shift plotted against [[frequency]] in a feedback control system or a filter. An example of this is shown in Figure 10. It is extremely useful for analyzing and testing filters and the stability of [[feedback]] control systems, through the measurement of corner (cutoff) frequencies and gain and phase margins.

This is identical to the function performed by a vector [[Network analyzer (electrical)|network analyzer]], but the network analyzer is typically used at much higher frequencies.

For education/research purposes, plotting Bode diagrams for given transfer functions facilitates better understanding and getting faster results (see external links).

== Related plots ==
{{main article|Nyquist plot|Nichols plot}}
Two related plots that display the same data in different [[coordinate systems]] are the [[Nyquist plot]] and the [[Nichols plot]]. These are [[parametric plots]], with frequency as the input and magnitude and phase of the frequency response as the output. The Nyquist plot displays these in [[polar coordinates]], with magnitude mapping to radius and phase to argument (angle). The Nichols plot displays these in rectangular coordinates, on the [[log scale]].
<gallery mode=packed heights=175>
Image:Nyquist plot.svg|A [[Nyquist plot]].
Image:Nichols plot.svg|A [[Nichols plot]] of the same response.
</gallery>

== Appendix ==

=== Proof Sketch for the Relation to Frequency Response ===
This section sketches a proof for the relation between frequency response and the magnitude and phase of the transfer function in Eqs.({{EquationNote|1}})-({{EquationNote|2}}).

Slightly changing the requirements for Eqs.({{EquationNote|1}})-({{EquationNote|2}}) one assumes that the input has been applied starting at time <math>t=0</math> and one calculates the output in the limit <math>t \to \infty</math>. In this case, the output is given by the [[Laplace transform#Properties and theorems|convolution]]

::<math>y(t) = \int_0^t d\tau h(\tau) u(t-\tau) \;, </math>

of the input signal with the inverse Laplace transform of the transfer function <math>h(t)</math>. The integral can be split in the following way

::<math>y(t) = \int_0^\infty d\tau h(\tau) u(t-\tau) - \int_t^\infty d\tau h(\tau) u(t-\tau) \;, </math>

where the second term vanishes in the limit <math>t \to \infty</math> if <math>h</math> has certain properties. Thus, inserting the sinusodial input signal one obtains

::<math>\lim_{t \to \infty }y(t) = \int_0^\infty d\tau h(\tau) \sin \left(\omega (t-\tau) \right) \;. </math>

Since <math>h(t)</math> is a real function this can be written as

::<math>\lim_{t \to \infty }y(t) = \mathrm{Im} \left\{ \left[ \int_0^\infty d\tau h(\tau) e^{-\mathrm{j} \omega \tau} \right]  e^{\mathrm{j} \omega t}  \right\} \;. </math>

The term in brackets is the definition of the Laplace transform of <math>h</math> at <math>s=\mathrm{j} \omega</math>. Inserting the definition in the form <math>H(\mathrm{j} \omega) = |H(\mathrm{j} \omega )| \exp \left( \arg  H(\mathrm{j} \omega)  \right )</math> one obtains the output signal

::<math>\lim_{t \to \infty }y(t) =  |H(\mathrm{j} \omega )| \sin \left( \omega t +  \arg\left(H(\mathrm{j} \omega) \right)  \right) \; </math>

stated in Eqs.({{EquationNote|1}})-({{EquationNote|2}}).

== See also ==
* [[Analog signal processing]]
* [[Phase margin]]
* [[Bode's sensitivity integral]]
* [[Electrochemical impedance spectroscopy]]

== Notes ==
{{reflist|group=note}}

== References ==
{{reflist}}

== External links ==
{{Commons category|Bode plots}}
* [http://www.facstaff.bucknell.edu/mastascu/eControlHTML/Freq/Freq5.html Explanation of Bode plots with movies and examples]
* [http://lpsa.swarthmore.edu/Bode/BodeHow.html How to draw piecewise asymptotic Bode plots]
* [http://lims.mech.northwestern.edu/~lynch/courses/ME391/2003/bodesketching.pdf Summarized drawing rules] ([[PDF]])
* [http://www.uwm.edu/People/msw/BodePlot/ Bode plot applet] - Accepts transfer function coefficients as input, and calculates magnitude and phase response
* [http://www.abc.chemistry.bsu.by/vi/fit.htm Circuit analysis in electrochemistry]
* [http://www.en-genius.net/includes/files/acqt_013105.pdf Tim Green: ''Operational amplifier stability''] Includes some Bode plot introduction
* Gnuplot code for generating Bode plot: [[:Image:Bode plot template.pdf|DIN-A4 printing template (pdf)]]
* [http://www.mathworks.com/help/control/ref/bode.html MATLAB function for creating a Bode plot of a system]
* [http://www.mathworks.com/videos/tech-talks/controls/ MATLAB Tech Talk videos explaining Bode plots and showing how to use them for control design]
* [http://www.onmyphd.com/?p=bode.plot Insert the poles and zeros and this website will draw the asymptotic and accurate Bode plots]
* [http://reference.wolfram.com/mathematica/ref/BodePlot.html Mathematica function for creating the Bode plot]

[[Category:Plots (graphics)]]
[[Category:Signal processing]]
[[Category:Electronic feedback]]
[[Category:Electronic amplifiers]]
[[Category:Electrical parameters]]
[[Category:Classical control]]
[[Category:Filter frequency response]]