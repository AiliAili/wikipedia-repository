{{Infobox journal
| title = International Journal of Quantum Chemistry
| cover = [[File:IntJQuantChem cover.jpg]]
| discipline = [[Quantum chemistry]]
| abbreviation = Int. J. Quantum Chem.
| publisher = [[John Wiley & Sons]]
| country =
| frequency = 24/year
| history = 1967-present
| openaccess = Hybrid
| impact = 2.184
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-461X
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-461X/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-461X/issues
| link2-name = Online archive
| ISSN = 0020-7608
| eISSN = 1097-461X
| OCLC = 1753588
}}
The '''''International Journal of Quantum Chemistry''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] publishing original, primary research and [[review article]]s on all aspects of [[quantum chemistry]], including an expanded scope focusing on aspects of [[materials science]], [[biochemistry]], [[biophysics]], [[quantum physics]], [[quantum information theory]], etc.<ref>{{Cite journal|last=Cavalleri|first=Matteo|date=2013-01-05|title=Quantum chemistry reloaded|url=http://onlinelibrary.wiley.com/doi/10.1002/qua.24364/abstract|journal=International Journal of Quantum Chemistry|language=en|volume=113|issue=1|pages=1–1|doi=10.1002/qua.24364|issn=1097-461X}}</ref> The 2015 [[impact factor]] of the journal is 2.184.

It was established in 1967 by [[Per-Olov Löwdin]].<ref>{{Cite journal|last=Löwdin|first=Per-Olov|date=1967-01-01|title=Nature of quantum chemistry|url=http://onlinelibrary.wiley.com/doi/10.1002/qua.560010103/abstract|journal=International Journal of Quantum Chemistry|language=en|volume=1|issue=1|pages=7–12|doi=10.1002/qua.560010103|issn=1097-461X}}</ref> In 2011, the journal moved to an in-house editorial office model, in which a permanent team of full-time, professional editors is responsible for article scrutiny and editorial content.

== References ==
<references/>

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-461X}}

[[Category:Chemistry journals]]
[[Category:Publications established in 1967]]
[[Category:Hybrid open access journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Computational chemistry]]


{{chem-journal-stub}}