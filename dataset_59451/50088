{{Infobox character
| name        = Theon Greyjoy
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Alfie Allen]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Theon Greyjoy-Alfie Allen.jpg
| caption     = Theon Greyjoy portrayed by [[Alfie Allen]]
| first       = '''Novel''': <br />''[[A Game of Thrones]]'' (1996) <br />'''Television''': <br/>"[[Winter Is Coming]]" (2011)
| last        = 
| occupation  =
| title       = Prince of [[Winterfell]]
| alias       = Reek
| gender      = Male
| family      = [[House Greyjoy]]
| spouse      = 
| children    = 
| relatives   =  [[Balon Greyjoy]] {{small|(father)}}<br />Alannys Harlaw {{small|(mother)}}<br/>Rodrik Greyjoy {{small|(brother)}}<br />Maron Greyjoy {{small|(brother)}}<br />[[Asha Greyjoy|Asha/Yara Greyjoy]] {{small|(sister)}}<br />[[Euron Greyjoy]] {{small|(uncle)}}<br />[[Victarion Greyjoy]] {{small|(uncle)}}<br />[[Aeron Greyjoy]] {{small|(uncle)}}
| lbl21       = Kingdom
| data21      = [[Iron Islands]]
}}

'''Theon Greyjoy''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''. Theon is the son and heir of [[Balon Greyjoy]], taken as a ward by Lord [[Eddard Stark]] following Balon's failed rebellion.<ref>{{cite web|url=http://viewers-guide.hbo.com/guide/houses/stark/theon-greyjoy/|title=Game of Thrones Viewer's Guide|publisher=}}</ref>

Introduced in 1996's ''[[A Game of Thrones]]'', Theon is the son of [[Balon Greyjoy]] from the kingdom of [[Westeros]]. He subsequently appeared in ''[[A Clash of Kings]]'' (1998) and ''[[A Dance with Dragons]]'' (2011). He is one of the major third person points-of-view through which Martin narrates both books.

Theon is portrayed by [[Alfie Allen]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/theon-greyjoy/bio/theon-greyjoy.html |title=''Game of Thrones'' Cast and Crew: Theon Greyjoy played by Alfie Allen |publisher=[[HBO]] | accessdate=December 25, 2015}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref>

== Character description ==
Theon Greyjoy is not a [[Narration#Third-person|point of view]] character in the first novel, but becomes one beginning with the second novel ''A Clash of Kings''.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=}}</ref>

Theon Greyjoy is the only living son, and [[heir apparent]] of [[Balon Greyjoy]]. He is the [[third-person limited narrative|third-person narrator]] for thirteen chapters throughout ''A Clash of Kings'' and ''A Dance with Dragons''. He is initially arrogant, cocky, and proud, but becomes more fragile after being tortured by [[Ramsay Snow]]. Ten years before the events of the series, he was taken hostage by [[Eddard Stark]] to be executed if Balon displeased the king, [[Robert Baratheon]]. Theon was raised at Winterfell with the Stark children and became a close friend to [[Robb Stark]] in particular.

As a prisoner of [[Ramsay Snow]], the "Bastard of Bolton", he has been subjected to physical and psychological torture, emasculated and mockingly renamed as "'''Reek'''".<ref>{{cite web|url=http://www.hitfix.com/whats-alan-watching/game-of-thrones-star-alfie-allen-on-theons-torture|title=Game of Thrones star Alfie Allen on Theons torture|work=HitFix}}</ref>

== Reception ==
Originally auditioning for the role of [[Jon Snow (character)|Jon Snow]],<ref>{{cite web|url=http://www.denofgeek.com/tv/game-of-thrones/39270/game-of-thrones-season-5-what-we-learned-from-the-blu-rays#ixzz436hmC8Fh|title=Game Of Thrones season 5: what we learned from the Blu-rays|publisher=}}</ref> the English actor [[Alfie Allen]] has received positive reviews for his role as Theon Greyjoy in the TV series.<ref>{{cite web|url=http://www.digitaltrends.com/features/game-of-thrones-season-5-interview-reek-reveals-all/|title=Theon Greyjoy: 'Game of Thrones' actor reveals all|publisher=}}</ref>

== Storylines ==
[[File:A Song of Ice and Fire arms of House Greyjoy black scroll.png|right|thumb|150px|alt=A coat of arms showing a golden kraken on a black field|Coat of arms of House Greyjoy]]

===Books===

==== ''A Game of Thrones'' ====
Theon Greyjoy becomes a trusted companion of Robb Stark on the battlefield, participating in the North's victories at Riverrun and the Whispering Wood.

==== ''A Clash of Kings'' ====
Robb sends Theon as an envoy to [[Iron Islands|Pyke]], seeking Balon Greyjoy's aid in his rebellion against [[House Lannister]]. Theon arrives to find Balon instead intends to seize the North while Robb is fighting in the Riverlands. Theon is charged to reave on the Stony Shore, but is jealous when he learns that his sister Asha has captured Deepwood Motte. After sending some his men to besiege Torrhen's Square and lure Winterfell's garrison away from the castle, Theon and his party invade and capture Winterfell. He releases the prisoner Reek, formerly a servant of House Bolton. When Bran and Rickon Stark apparently escape Winterfell, Reek advises Theon to kill two young boys and pass their bodies off as those of the Stark children.

Winterfell's garrison soon repels the ironborn at Torrhen's Square and besieges Winterfell. When Asha refuses to give Theon men to hold the castle, he allows Reek to seek reinforcements from the Dreadfort. Reek returns with several hundred Bolton men and defeats the Stark soldiers, but then reveals his true identity is Roose Bolton's bastard Ramsay Snow and takes Theon prisoner.

==== ''A Storm of Swords'' and ''A Feast for Crows''====
Ramsay imprisons Theon in the Dreadfort's dungeons and tortures him for his own amusement, though most of the Ironborn believe that Theon is dead. At some point, Theon manages to escape with his former bedwarmer, Kyra, though this turns out to be a trick of Ramsay's and the two are soon recaptured.

==== ''A Dance with Dragons'' ====
Ramsay's torture leaves Theon with many of his toes, fingers and teeth missing; it is implied that Ramsay also [[Penis removal|removes his penis]]. The trauma of this torture causes Theon to lose much of his body weight, turns his hair white, and leaves him resembling an old man. He is forced to assume the identity of Reek. When Roose Bolton begins to lead his forces back to the North, Ramsay (who has since been legitmised as a Bolton) has Theon convince the Ironborn garrison holding Moat Cailin to surrender, but flays them regardless. Theon is ordered to give away Jeyne Poole (who is posing as Arya Stark) at her wedding to Ramsay, who later forces Theon to participate in his sexual abuse of Jeyne. Theon later encounters Mance Rayder (disguised as Abel the bard) and his spearwives, who enlist his help in freeing Jeyne, having been sent by Jon Snow. When the alarm is raised, Theon jumps from Winterfell's battlements with Jeyne and is rescued by Mors Umber, who sends him to Stannis Baratheon's camp several days' ride away. There he is reunited with Asha, who initially does not recognise him.

==== ''The Winds of Winter'' ====
Theon is kept prisoner by Stannis, who notes that he may have useful information about the Boltons but means to execute him for the supposed murders of Bran and Rickon. Asha unsuccessfully tries to ransom Theon before convincing Stannis to behead him rather than burn him alive.

===Television adaptation===
[[File:Alfie Allen by Gage Skidmore.jpg|thumb|right|upright|[[Alfie Allen]] plays the role of Theon Greyjoy in the [[Game of Thrones|television series]].]]
Theon Greyjoy is played by [[Alfie Allen]] in the television adaptation of the series of books.<ref>{{cite web|url=http://www.esquire.com/entertainment/tv/interviews/a34910/game-of-thrones-alfie-allen-on-theon-reek|title='Game of Thrones' Q&A: Alfie Allen on Theon Greyjoy|work=Esquire}}</ref>

====Season 1====
Theon is introduced as the hostage and ward of Lord Eddard Stark, stemming from the failed Greyjoy Rebellion. Despite his position, he remains loyal to Eddard and is good friends with his sons Robb and Jon. While he has never questioned his position, he soon begins to have doubts after Tyrion Lannister tells him he is nothing more than a servant to the Starks and that not everyone respects him. Nevertheless, Theon initially remains loyal to Robb after he goes to war with the Lannisters and supports his decision to have the North secede from the Seven Kingdoms and form their own kingdom.

====Season 2====
Theon is sent to the Iron Islands to persuade Balon to ally with the Starks against the Lannisters, but Balon instead intends to conquer the North while its army is fighting in the Westerlands. Theon is insulted when he is given command of a single ship to raid the Stony Shore and contemplates sending a warning to Robb, but ultimately decides to remain loyal to his family.<ref name=westeros>{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/The_North_Remembers/Book_Spoilers/|title=EP201: The North Remembers|work=Westeros.org|first=Elio|last=Garcia|accessdate=April 2, 2012}}</ref> When his crew proves to be disrespectful of Theon's station, his first mate Dagmer Cleftjaw suggests that Theon prove himself by capturing Winterfell. Theon lures the Stark garrison away from Winterfell and easily captures the castle, but is forced to execute his old mentor Ser Rodrik Cassel when he refuses to yield. Theon is seduced by the wildling servant Osha, who later frees [[Bran Stark|Bran]] and [[Rickon Stark]]. Theon's men are unable to recapture the two and Theon kills two farm boys to pass their bodies off as those of the Stark boys, an act he soon feels guilty for.

Theon asks his sister Yara to bring 500 men as reinforcements, but instead she arrives with a paltry force to warn Theon of his unstable position and to return to the Iron Islands. Theon refuses, and soon afterwards Winterfell is besieged by men of House Bolton commanded by (the then unnamed) Ramsay Snow, bastard son of Lord Roose Bolton. Theon attempts to rally his men to fight to the death, but they knock him out and hand him over to Ramsay, hoping for amnesty. Unknown to the viewer, Ramsay disobeys his orders to free the Ironborn and instead flays them all and sacking Winterfell, before taking Theon back to the Dreadfort as prisoner.

====Season 3====
Theon is taken captive and kept in an unknown castle, where he is briefly tortured, but later manages to escape with the help of a serving boy who claims to work for his sister Yara. He is brought back to the very castle he escaped from, the serving boy proving to actually be his captor and torturer, Ramsay Snow, Lord Bolton's sadistic bastard child. Theon is subsequently brutally tortured, flayed and castrated by Ramsay, who forces him to rename himself Reek, and beats him until he submits to his new name. Theon's penis is sent in a box to his father, with Ramsay threatening to mutilate Theon further unless the Ironmen retreat from the North. Balon refuses, as Theon defied him and is now unable to further the Greyjoy line. Outraged, Yara responds she intends to save her brother of her own accord.<ref>{{cite web|url=http://yeoldecybershoppe.com/malahide/photos/analysis-of-scene-yara-and-balon-greyjoy-got-s03e10-mhysa/|title=Analysis of Scene: Yara & Balon Greyjoy, Game of Thrones: Mhysa|date=16 June 2013|publisher=}}</ref>

====Season 4====
Theon is freed from his restraints by Ramsay, but is forced to work for him as a servant and answer to the name Reek. Roose Bolton orders Ramsay to launch an attack on Moat Cailin, an Ironborn-occupied fortress that is blockading the Northern lands from the rest of Westeros, and to take Theon with him. Before they leave, the Dreadfort is infiltrated by Yara and her men, but Theon refuses to flee with her, believing it may be another trick on Ramsay's part, forcing Yara to leave without him. Impressed by his loyalty, Ramsay has Theon masquerade as himself and tricks the Ironborn into opening Moat Cailin, leading to them all being flayed by the Bolton army. Theon subsequently accompanies the Boltons to their new seat of Winterfell.

====Season 5====
Theon is stunned to find that Ramsay has been betrothed to Sansa Stark. He tries to avoid her until Ramsay's psychotic paramour Myranda leads Sansa to Theon's sleeping quarters, a cage in the kennels.<ref>{{cite web|url=http://tvline.com/2015/05/10/game-of-thrones-sansa-theon-reek-reunite-recap-season-5/|title=Game of Thrones Recap: The Reek Shall Inherit the Worst|first1=Kimberly|last1=Roots|first2=Kimberly|last2=Roots|date=11 May 2015|publisher=}}</ref> After learning of their reunion, Ramsay involves Theon in his torment of Sansa by having him give her away at the wedding and then forces him to watch as he rapes her on their wedding night.<ref>{{cite web|url=http://thefederalist.com/2015/05/22/game-of-thrones-fans-shouldnt-leave-because-of-sansa-starks-rape/|title=‘Game Of Thrones’ Fans Need To Understand Sansa's Rape|first=|last=Television|publisher=}}</ref> Sansa asks for Theon's assistance to escape from Ramsay, but Theon, wishing to spare her Ramsay's wrath, instead warns Ramsay, causing him to flay the maid who had tried to help Sansa escape. When Sansa confronts Theon, he confesses that he did not kill her brothers. While Ramsay and the Bolton army attack Stannis Baratheon's forces, Sansa makes another escape attempt, but is caught and threatened by Myranda. Finally snapping, Theon pushes Myranda over a walkway to her death, just as the victorious Boltons return. Rather than face Ramsay's anger, Theon and Sansa jump from the Winterfell wall into the snow and flee.

====Season 6====
Theon and Sansa are caught by Bolton soldiers in the woods surrounding Winterfell, but are saved by the arrival of Brienne of Tarth, who kills the hunters. Although Sansa and Brienne decide to make for Castle Black, where Sansa's half-brother Jon is Lord Commander of the Night's Watch, Theon tells her that he does not deserve the Starks' forgiveness and instead decides to return to the Iron Islands. Theon returns to the Iron Islands to discover that Balon is dead, and offers to support Yara at the Kingsmoot, a ceremony where the Ironborn elect their new leader. However, the Kingsmoot is won by Theon's uncle Euron Greyjoy, who admits to killing Balon but wins over the Ironborn by promising to conquer Westeros through marriage to [[Daenerys Targaryen]], who possesses the only living dragons in the world. Correctly surmising that Euron will have them put to death, Theon, Yara, and their loyalists flee the Iron Islands with the best ships of the Iron Fleet. They travel to Meereen, which Daenerys has taken as her seat, and offer her the Iron Fleet in exchange for the Iron Islands' independence and the removal of Euron as king. Daenerys agrees, on the condition that the Ironborn cease their practices of pillaging. Theon and Yara then join Daenerys as she sets sail for Westeros.

=== Family tree of House Greyjoy ===
{{Family tree of House Greyjoy}}

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Greyjoy, Theon}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional nobility]]
[[Category:Fictional amputees]]
[[Category:Fictional murderers]]
[[Category:Fictional slaves]]
[[Category:Fictional traitors and defectors]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional child killers]]
[[Category:Fictional princes]]
[[Category:Fictional victims of torture]]