{{Infobox journal
| title = Management Communication Quarterly 
| cover = [[File:Management Communication Quarterly.tif]]
| editor = Patricia M. Sias
| discipline = [[Communication studies]], [[management]]
| former_names = 
| abbreviation = Manag. Commun. Q.
| publisher = [[SAGE Publications]]
| country = [[United States of America]]
| frequency = Quarterly
| history = 1987-present
| openaccess = 
| license = 
| impact = 2.085
| impact-year = 2014
| website = http://mcq.sagepub.com/
| link1 = http://mcq.sagepub.com/content/current
| link1-name = Online access
| link2 = http://mcq.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0893-3189
| eISSN =
| OCLC = 41181749
| LCCN = 93091431
}}
'''''Management Communication Quarterly''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[communication studies]] pertaining to [[management]] and organizational communication. The [[editor-in-chief]] is Patricia Sias ([[University of Arizona]]). It was established in 1987 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Management Communication Quarterly'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 2.085, ranking it 44 out of 185 journals in the category ‘Management’.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Management |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> and 6 out of 76 journals in the category ‘Communication’. <ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Communication |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://mcq.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Business and management journals]]
[[Category:Communication journals]]
[[Category:Publications established in 1987]]
[[Category:Quarterly journals]]