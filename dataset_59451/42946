{{Use dmy dates|date=July 2013}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = D.I<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Hansa Brandenburg D.I.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Fighter
  |manufacturer = [[Hansa-Brandenburg]]
  |designer = [[Ernst Heinkel]] 
  |first flight = 1916<!--if it hasn't happened, leave it out!-->
  |introduced = 1916<!--date the aircraft entered military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Austria-Hungary]]<!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = [[Austria]]<!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 122
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Hansa-Brandenburg KDW]]<br />[[Phönix D.I]]
}}
|}

The '''Hansa-Brandenburg D.I''', also known as the '''KD''' (''Kampf Doppeldecker'') was a [[Germany|German]] fighter aircraft of [[World War I]]. Despite poor handling, it was built for [[Austria-Hungary]], some aircraft serving to the end of the war.

==Development and design==
[[Ernst Heinkel]] chief designer of the [[Hansa und Brandenburgische Flugzeug-Werke]] developed  the KD in 1916 to meet the requirements of the Austro-Hungarian [[Austro-Hungarian Imperial and Royal Aviation Troops|Air Force]] (''Kaiserliche und Königliche Luftfahrtruppen ''or'' K.u.K. Luftfahrtruppen''). It was a single seat, single engined [[biplane]], of wooden construction, with [[plywood]] [[fuselage]] skinning and fabric wing skins. The wings featured an unusual "Star-Strutter" arrangement of [[interplane strut]]s, where four Vee struts joined in the centre of the wing bay to result in a "star" arrangement. The interplane struts themselves were steel tubes.<ref name="complete fighters p83">Green and Swanborough 1994, p.83.</ref>

The KD had a deep fuselage, which gave a poor forward view for the pilot and tended to blanket the small [[rudder]], giving poor lateral stability and making recovery from spins extremely difficult.<ref name="Thetford p64">Gray and Thetford 1962, p.64.</ref>  Armament was a single [[Schwarzlose MG M.07/12#Use as an aircraft gun|Schwarzlose]] [[machine gun]], which owing to difficulties in synchronising the Schwarzlose, was fitted in a fairing on the upper wing, firing over the propeller.<ref name="flying gunsWW1p62">Williams and Gustin 2003, p.62</ref>

Despite these handling problems, the aircraft was ordered by Austro-Hungary as the '''D.I'''. A total of 122 D.Is were built, with 50 built by Hansa-Brandenburg in Germany - powered by 110&nbsp;kW (150&nbsp;hp) [[Austro-Daimler]] engines - while a further 72 were built under license by Phönix in [[Vienna]], powered by 138&nbsp;kW (185&nbsp;hp) Austro-Daimler engines. While it was intended that the D.I also be built by [[Ufag]], none were delivered.<ref name="ipms windsock">Silen, Art. [http://www.ipmsusa2.org/Reviews/Books/Aircraft/windsock_hansa_d1/windsock_hansa_d1.htm IPMS Book Review: Albatros Productions, Ltd Windsock Datafile #118 Hansa Brandenburg D.1] ''IPMS USA''. Retrieved 2 March 2008</ref> The KD also formed the basis for the [[Hansa-Brandenburg KDW]] [[floatplane]] fighter.

==Operational history==
The D.I entered service in Autumn 1916. Its unusual arrangement of interplane bracing gave rise to the nickname "Spider",<ref name="aerodrome">[http://www.theaerodrome.com/aircraft/austrhun/hansa_di.php Hansa-Brandenburg D.I - The Aerodrome - Aces and Aircraft of World War I] Retrieved 2 March 2008</ref> while its poor handling gave rise to the less complementary nickname "the Coffin".<ref name="Angelucci p54">Angelucci 1981, p.54.</ref> The D.I was the standard fighter aircraft of the ''Luftfahrtruppen'' until mid 1917, being used by several Austro-Hungarian air aces such as [[Godwin Brumowski]] and [[Frank Linke-Crawford]].<ref name="aerodrome"/> Some Phönix built D.Is remained in use until the end of the war, being used briefly by the armed forces of [[German Austria|The Republic of German Austria]] (''Republik Deutschösterreich''), where they were used to fight [[Yugoslavia|Yugoslav]] attacks on [[Klagenfurt]] in [[Carinthia (state)|Carinthia]].<ref name="complete fighters p83"/><ref name="Hooton p25-26">Hooton 1994, p.25-26</ref>

==Operators==
;{{flag|Austria-Hungary}}
*[[Austro-Hungarian Imperial and Royal Aviation Troops|''Kaiserliche und Königliche Luftfahrtruppen'']]
*[[Austro-Hungarian Navy]]
;{{flag|Austria}}
*[[German Austria|Republic of German Austria]]

==Specifications (D.I)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. To add a new line, end the old one with a right parenthesis ")" and start a new, fully formatted line beginning with * -->
|ref=''The Complete Book of Fighters'' <ref name="complete fighters p83"/> 
|crew=1
|capacity=
|length main= 6.35 m
|length alt= 20 ft 10 in
|span main= 8.50 m
|span alt= 27 ft 10⅔ in
|height main= 2.79 m <ref name="Angelucci p43">Angelucci 1981, p. 43</ref>
|height alt= 9 ft 2¾ in
|area main= 23.95 m²
|area alt= 257.8 ft²
|airfoil=
|empty weight main= 672 kg
|empty weight alt= 1,481 lb
|loaded weight main= 920 kg
|loaded weight alt= 2,028 lb
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)= [[Austro-Daimler]]
|type of prop= 6-cylinder air cooled inline
|number of props=1
|power main= 138 kW
|power alt= 185 hp
|power original=
|max speed main= 187 km/h
|max speed alt= 101 kn, 116 mph
|cruise speed main=  
|cruise speed alt=  
|stall speed main=  
|stall speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|range main=  
|range alt=  
|ceiling main= 5,000 m<ref name="Angelucci p43"/>
|ceiling alt= 16,400 ft
|climb rate main=  
|climb rate alt=  
|loading main= 
|loading alt= 
|thrust/weight=
|power/mass main= 
|power/mass alt= 
|more performance=*'''Endurance:''' 2 hours 30 minutes<ref name="Angelucci p43"/>
*'''Climb to 1,000 m (3,280 ft):''' 3 minutes.
|armament=*1 × 8 mm (.315 in) [[Schwarzlose MG M.07/12#Use as an aircraft gun|Schwarzlose]] machine gun
|avionics=
}}

==See also==
{{aircontent|
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Hansa-Brandenburg KDW]]<br />
*[[Phönix D.I]]<!-- related developments -->
|similar aircraft=*[[Albatros D.I]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
*Angelucci, Enzo (ed.). ''World Encyclopedia of Military Aircraft.'' London: Jane's, 1981. ISBN 0-7106-0148-4.
*Gray, Peter and Thetford, Owen. ''German Aircraft of the First World War''. London: Putnam, 1962.
*Green, William and Swanborough, Gordon. ''The Complete Book of Fighters''. New York: Smithmark, 1994. ISBN 0-8317-3939-8.
* Hooton, E.R. ''Phoenix Triumphant: The Rise and Rise of the Luftwaffe''.London: Arms & Armour Press, 1994. ISBN 1-85409-181-6.
*{{cite book|last1=Meindl|first1=Karl|last2=Schroeder|first2=Walter|title=Brandenburg D.I|series=Great War Aircraft in Profile 2|publisher=Flying Machines Press|year=1997|isbn=1891268015}}
* Williams, Anthony G. and Gustin, Emmanuel. ''Flying Guns World War I''. Ramsbury, Wiltshire: Airlife, 2003. ISBN 1-84037-396-2.
{{refend}}

==External links==
{{commons category|Hansa-Brandenburg}}
*[https://web.archive.org/web/20170314065154/http://www.history-of-flight.net/Aviation%20history/photo_albums/timeline/Hansa.htm Hansa Brandenburg D.I]
*[http://www.theaerodrome.com/aircraft/austrhun/hansa_di.php Hansa-Brandenburg D.I - The Aerodrome - Aces and Aircraft of World War I]
*[http://www.swwisa.net/kuklft/planes.html Fighter Aircraft of the LFT]
*[http://www.ipmsusa2.org/Reviews/Books/Aircraft/windsock_hansa_d1/windsock_hansa_d1.htm IPMS Book Review: Albatros Productions, Ltd Windsock Datafile #118 Hansa Brandenburg D-1]

{{Hansa-Brandenburg aircraft}}
{{World War I Aircraft of the Central Powers}}
{{KuKLFT D-class designations}}

[[Category:Military aircraft of World War I]]
[[Category:German fighter aircraft 1910–1919]]
[[Category:Hansa-Brandenburg aircraft|D.I]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]