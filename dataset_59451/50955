{{Infobox journal
| title = Cognition
| cover = 
| editor = [[Gerry Altmann]]
| discipline = [[Cognitive sciences]]
| abbreviation = Cognition
| formernames = 
| publisher = [[Elsevier]]
| country = 
| frequency = Bimonthly
| history = 1975-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license = 
| impact = 3.634
| impact-year = 2013
| website = http://www.journals.elsevier.com/cognition/
| link1 = http://www.sciencedirect.com/science/journal/00100277
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| LCCN = 79646376
| CODEN = CGTNAU
| ISSN = 0010-0277
| OCLC = 38537176
}}

'''''Cognition: International Journal of Cognitive Science''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering [[cognitive science]]. It was established in 1972 and is published by [[Elsevier]].

== History ==
''Cognition''  is one of the few [[psychology]] journals of the 20th century that developed outside of the United States.<ref name=Cole>{{cite book |last1=Cole |first1=Jim |last2=Stankus |first2=Tony |title=Journals of the Century |date=2014 |publisher=[[Taylor & Francis]] |location=Hoboken |isbn=1317720148 |page=36}}</ref> The journal was initially published quarterly, then every other month from 1982, and finally on a monthly basis since 1985.<ref name=SD>{{cite web |title=Cognition |url=http://www.sciencedirect.com/science/journal/00100277/ |website=ScienceDirect |publisher=[[Elsevier]] |accessdate=20 November 2014}}</ref>

Work in the journal is considered "highly cited," and is noted to adequately represent research on an international level.<ref name=Cole /> In terms of representing topics within the cognitive sciences, a 2005 meta-analysis demonstrated that the journal primarily publishes work in psychology, with a small proportion of publications in [[linguistics]] and [[neuroscience]].<ref name=Schunn>{{cite book|last1=Schunn|first1=C.D.|last2=Crowley|first2=K.|last3=Okada|first3=T.|editor1-last=Derry|editor1-first=S.J.|editor2-last=Gernsbacher|editor2-first=M.A.|title=Problems and promises of interdisciplinary collaboration: Perspectives from cognitive science.|chapter=Cognitive science: Interdisciplinarity now and then |date=2005|location=Mahwah, NJ}}</ref> The analysis concluded that for several reasons, including the explicit editorial policy of the journal and efforts to create special issues for specific disciplines, that the disparity could not be well explained by editorial bias.<ref name=Schunn />

== References ==
{{reflist}}

==External links==
*{{Official website|http://www.journals.elsevier.com/cognition/}}

[[Category:English-language journals]]
[[Category:Publications established in 1975]]
[[Category:Cognitive science journals]]
[[Category:Elsevier academic journals]]
[[Category:Monthly journals]]