{{Infobox political party
|country = Abkhazia
|name = Forum for the National Unity of Abkhazia
|native_name = Форум Народного единства Абхазии <br /> აფხაზეთის ეროვნული ერთიანობის ფორუმი
|logo = logo_fnua.jpg
|leader = [[Raul Khajimba]]
|foundation = February 8, 2005
|ideology = [[Nationalism]]<ref>{{cite web|title=Georgia's separatist Abkhazia set to elect nationalist leader|url=http://reliefweb.int/report/georgia/georgias-separatist-abkhazia-set-elect-nationalist-leader|accessdate=30 August 2016|language=English|date=3 October 2004}}</ref><ref>{{cite web|title=Abkhazia's New Leader Insists on Deeper Ties With Moscow|url=http://www.cacianalyst.org/publications/field-reports/item/13040-abkhazias-new-leader-insists-on-deeper-ties-with-moscow.html|website=www.cacianalyst.org|accessdate=30 August 2016}}</ref>
|international =
|colours = Red, Green
|colorcode = black
|headquarters = აფხაზეთის ეროვნული ერთიანობის ფორუმი
|website =
|seats1_title = [[People's Assembly of Abkhazia|People's Assembly]]
|seats1 = {{Composition bar|6|35|blue}}
}}

The '''Forum for the National Unity of Abkhazia''' ('''FNUA''', {{lang-ru|Форум Народного единства Абхазии}}, {{lang-ka|აფხაზეთის ეროვნული ერთიანობის ფორუმი}}) is a prominent oppositional [[political party]] in [[Abkhazia]]. It is led by Former Prime Minister, Vice President and President Elect [[Raul Khajimba]].

==History==

The FNUA was founded on 8 February 2005 to unite 12 opposition parties, socio-political movements, public organisations and youth organisations, which however continued to exist as individual entities. It mainly brought together supporters of [[Raul Khadjimba|Raul Khajimba]] in the [[Abkhazian presidential election, 2004|October 2004 presidential election]]. Khajimba had lost that election, but became Vice President following the power-sharing agreement concluded to end the post-election crisis, and the FNUA considered ensuring the strict implementation of that agreement one of its principal tasks. The FNUA was initially governed by a coordinating board of twelve members, headed by [[Gennadi Alamia]], co-chairman of the [[Social-Democratic Party of Abkhazia]].<ref name="uzel69877">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/69877|script-title=ru:В Абхазии создан Форум народного единства, в который вошли двенадцать партий и движений республики|last=Kuchuberia|first=Angela|date=2005-02-09|publisher=Caucasian Knot|language=Russian|accessdate=2009-05-29}}</ref>

The FNUA became a socio-political movement on 10 October 2005 and [[Avtandil Gartskia]], [[Vitali Gabnia]] and [[Daur Arshba]] were elected its co-chairmen.<ref name="uzel82870">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/82870|script-title=ru:Абхазская оппозиция создала Общественно-политическое движение "Форум Народного единства Абхазии"|last=Kuchuberia|first=Angela|date=2005-10-07|publisher=Caucasian Knot|language=Russian|accessdate=2009-05-29}}</ref> On 6 March 2008 the FNUA was transformed into a political party and Daur Arshba and [[Astamur Tania]] became its two co-chairmen.<ref name="uzel133218">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/133218|script-title=ru:Общественно-политическое движение "Форум народного единства Абхазии" стало политической партией|last=Kuchuberia|first=Angela|date=2008-03-06|publisher=Caucasian Knot|language=Russian|accessdate=2009-05-29}}</ref>

In the run-up to the [[Abkhazian presidential election, 2009|12 December 2009 Presidential election]], the FNUA intended to nominate Raul Khajimba and [[Zaur Ardzinba]], but in the end the pair could not agree on who would get what position, and the congress of the FNUA, planned for 29 October, was called off.<ref name="kommersant1267836">{{cite news|url=http://www.kommersant.ru/doc.aspx?DocsID=1267836|script-title=ru:Сергей Багапш столкнется с поддержанными конкурентами|last=Allenova|first=Olga|date=2009-11-02|publisher=[[Kommersant]]|language=Russian|accessdate=2009-11-15}}</ref> Khajimba and Ardzinba then entered the election separately, ending up 2nd and 3rd respectively, behind incumbent President Sergei Bagapsh.

On 12 May 2010, the FNUA held a congress during which the number of Chairmen was reduced from 2 to 1, and the number of Deputy Chairmen from 4 to 2. Raul Khajimba was elected to the new leadership position, with Daur Arshba and [[Rita Lolua]] becoming his deputies.<ref name="uzel168754">{{cite web|last=Kuchuberia|first=Anzhela|script-title=ru:Рауль Хаджимба возглавил оппозиционный "Форум народного единства Абхазии"|url=http://abkhasia.kavkaz-uzel.ru/articles/168754/|work=Caucasian Knot|accessdate=19 May 2010|language=ru|date=13 May 2010}}</ref>

On 10 July 2013, the Forum signed a cooperation agreement with fellow opposition parties [[United Abkhazia]], the [[People's Party of Abkhazia]] and the [[Party for the Economic Development of Abkhazia]], and with a number of social movements.<ref name=apress9506>{{cite news|title=Ряд политических партий и общественных движений Абхазии подписали Соглашение о сотрудничестве и совместную декларацию |url=http://apsnypress.info/news/9506.html |accessdate=29 July 2013 |newspaper=[[Apsnypress]] |date=10 July 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131220180623/http://apsnypress.info/news/9506.html |archivedate=20 December 2013 |df= }}</ref>

On 31 March 2015, Daur Arshba again succeeded Khajimba as Chairman.<ref name=press_daur2>{{cite news|script-title=ru:Даур Аршба назначен Руководителем Администрации Президента Абхазии|language=Russian|url=http://apsnypress.info/news/daur-arshba-naznachen-rukovoditelem-administratsii-prezidenta-abkhazii/|accessdate=11 November 2016|newspaper=[[Apsnypress]]|date=10 October 2016}}</ref>

On 2 March 2016, the FNUA condemned the [[Abkhazian early presidential elections referendum, 2016|planned referendum to hold an early presidential election]].<ref name=apress_rpp>{{cite news|title=РПП ФНЕА: действия лидеров оппозиции направлены на дезинформацию населения и на преднамеренную дестабилизацию обстановки в стране|url=http://apsnypress.info/news/rpp-fnea-deystviya-liderov-oppozitsii-napravleny-na-dezinformatsiyu-naseleniya-i-na-prednamerennuyu-/|accessdate=11 March 2016|agency=[[Abkhazia Inform]]|date=2 March 2016}}</ref> After the referendum had been approved by President Khajimba, the party held a congress on 10 June in the hall of the hotel Inter-Sukhum, attended by 356 delegates, during which various speakers called for a boycott of the referendum.<ref name="press_daur">{{cite news|last1=Poluyan|first1=Elena|title=Даур Аршба: призываем не идти на поводу у реваншистов!|url=http://apsnypress.info/news/daur-arshba-prizyvaem-ne-idti-na-povodu-u-revanshistov/|accessdate=31 July 2016|agency=[[Apsnypress]]|date=10 June 2016}}</ref><ref name="ekho27790978">{{cite news|last1=Gogoryan|first1=Anaid|title=Форум против референдума|url=http://www.ekhokavkaza.com/a/27790978.html|accessdate=2 August 2016|agency=[[Echo of the Caucasus]]|date=10 June 2016}}</ref>

==Original composition==

The Forum for the National Unity of Abkhazia originally brought together three political parties:

* the [[People's Party of Abkhazia]]
* the [[Social-Democratic Party of Abkhazia]]
* the [[Republican Party (Abkhazia)|Republican Party of Abkhazia]]

one socio-political movement:

* [[Aidgylara]]
six public organisations and movements:
* [[Aiaaira (political party)|Aiaaira]]
* [[Akhatsa]]
* the [[Union of Defenders of Abkhazia]]
* Rodina
* Apsadgil
* the Abkhazian branch of the [[LDPR]]

and two youth organisations:

* Smena
* the Social-Democratic Youth of Abkhazia

==Election results==
===Presidential===
{| class="wikitable"
|-
! Year !! Candidate !! First Round !! Second Round !! Result
|-
| [[Abkhazian presidential election, 2005|2005]] || [[Sergei Bagapsh]] || 91.54% || - ||  {{yes2|Won}}
|-
| [[Abkhazian presidential election, 2009|2009]] || [[Raul Khajimba]] || 15.32% || - ||  {{no2|2nd place}}
|-
| [[Abkhazian presidential election, 2011|2011]] || [[Raul Khajimba]] || 19.82% || - ||  {{no2|3rd place}}
|-
| [[Abkhazian presidential election, 2014|2014]] || [[Raul Khajimba]] || 50.60% || - ||  {{yes2|Won}}
|}

===Parliamentary===
{| class="wikitable"
|-
! Year !! Seats !! Places
|-
| [[Abkhazian parliamentary election, 2007|2007]] || 28 || 1st
|-
| [[Abkhazian parliamentary election, 2012|2012]]  || 6 || 1st
|}

==References==
{{Reflist|2}}

{{Abkhazian political parties}}

[[Category:2005 establishments in Abkhazia]]
[[Category:Political parties established in 2005]]
[[Category:Political parties in Abkhazia]]


{{abkhazia-poli-stub}}
{{Georgia-party-stub}}