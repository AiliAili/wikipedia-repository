{{redirect|UNIA}}
{{Infobox organization
| name                = Universal Negro Improvement Association and African Communities League 
| native_name         = 
| native_name_lang    = 
| named_after         = 
| image               = 
| image_size          = 
| alt                 = 
| caption             = 
| logo                =
| logo_size           = 
| logo_alt            = 
| logo_caption        = 
| abbreviation        = UNIA
| motto               = One God! One Aim! One Destiny!
| predecessor         = 
| merged              = 
| successor           = 
| formation           = {{start date and age|1914|07|15}}
| founder             = [[Marcus Garvey]]
| founding_location   = [[Jamaica]]
| extinction          = <!-- use {{end date and age|YYYY|MM|DD}} -->
| merger              = 
| type                = 
| tax_id              = <!-- or | vat_id = (for European organizations) -->
| registration_id     = <!-- for non-profit org -->
| status              = 
| purpose             = 
| headquarters        = 
| location            = 
| coords              = <!-- {{coord|LAT|LON|display=inline, title}} -->
| region              = 
| services            = 
| products            = 
| methods             = 
| fields              = 
| membership          = 
| membership_year     = 
| language            = 
| owner               = <!-- or | owners = -->
| sec_gen             = 
| leader_title        = President
| leader_name         = [[Cleo Miller|Cleo Miller, Jr.]]
| leader_title2       = 
| leader_name2        = 
| leader_title3       = 
| leader_name3        = 
| leader_title4       = 
| leader_name4        = 
| board_of_directors  = 
| key_people          = 
| main_organ          = 
| parent_organization = 
| subsidiaries        = 
| secessions          = 
| affiliations        = 
| budget              = 
| budget_year         = 
| revenue             = 
| revenue_year        = 
| disbursements       = 
| expenses            = 
| expenses_year       = 
| endowment           = 
| staff               = 
| staff_year          = 
| volunteers          = 
| volunteers_year     = 
| slogan              = 
| mission             = 
| website             = <!-- {{URL|example.com}} -->
| remarks             = 
| formerly            = 
| footnotes           = 
}}
{{Portal|African American}}
{{African American topics sidebar|RIGHT}}

The '''Universal Negro Improvement Association and African Communities League''' ('''UNIA-ACL''') is a [[black nationalist]] [[fraternal organization]] founded in 1914 by [[Marcus Mosiah Garvey]].  The organization enjoyed its greatest strength in the 1920s, prior to Garvey's deportation from the [[United States|United States of America]], after which its prestige and influence declined.

According to the preamble of the 1929 constitution as amended, the UNIA is a "[[social]], friendly, [[humanitarian]], charitable, educational, [[institutional]], constructive and expansive [[society]], and is founded by persons desiring to do the utmost to work for the general uplift of the people of [[Africa]]n [[ancestry]] of the world. And the members pledge themselves to do all in their power to conserve the rights of their noble race and to respect the rights of all mankind, believing always in the Brotherhood of Man and the [[Fatherhood of God]]. The motto of the organization is 'One God! One Aim! One Destiny!' Therefore, let justice be done to all mankind, realizing that if the strong oppresses the weak, confusion and discontent will ever mark the path of man but with love, faith and charity towards all the reign of peace and plenty will be heralded into the world and the generations of men shall be called Blessed."

The broad mission of the UNIA-ACL led to the establishment of numerous auxiliary components, among them the [[Universal African Legion]], a [[paramilitary]] group; the African [[Black Cross Nurses]]; [[African Black Cross Society]]; the Universal African Motor Corps; the Black Eagle Flying Corps; the [[Black Star Steamship Line]]; the [[Black Cross Trading and Navigation Corporation]]; as well as the [[Negro Factories Corporation]].

==Name==
In an article entitled "The Negro's Greatest Enemy",<ref>[http://www.international.ucla.edu/africa/mgpp/sample01 "The Negro's Greatest Enemy"], American Series Sample Documents, Volume I: 1826--August 1919. UCLA African Studies Center.</ref> published in ''[[Current History]]'' (September 1923), Garvey explained the origin of the organization's name:
:"Where did the name of the organization come from? It was while speaking to a [[West Indian]] [[Negro]] who was a passenger with me from [[Southampton]], who was returning home to the [[East Indies]] from [[Basutoland]] with his [[Basuto]] wife, I further learned of the horrors of native life in Africa. He related to me in conversation such horrible and pitiable tales that my heart bled within me. Retiring from the conversation to my cabin, all day and the following night I pondered over the subject matter of that conversation, and at midnight, lying flat on my back, the vision and thought came to me that I should name the organization the Universal Negro Improvement Association and African Communities (Imperial) League. Such a name I thought would embrace the purpose of all black humanity. Thus to the world a name was born, a movement created, and a man became known."

==Early history==
Originally from [[Saint Ann's Bay]], [[Colony of Jamaica|Jamaica]], Marcus Garvey left at 23 and traveled throughout Central America  and moved for a time to [[England]].  During his travels he became convinced that uniting Blacks was the only way to improve their condition. Towards that end, he departed England on June 14, 1914, aboard the [[SS Trent|SS ''Trent'']], reaching Jamaica on July 15, 1914. He founded the Universal Negro Improvement Association (UNIA) in August 1914 in [[Akron, Ohio]]<ref>{{cite web|title=Akron's Black History Timeline|url=http://www.akronohio.gov/cms/site/0b144cf2a61a53e3/index.html|website=http://www.akronohio.gov|publisher=City of Akron|accessdate=12 January 2017}}</ref> <ref>{{cite web|last1=Giffin|first1=William|title=African Americans and the Color Line in Ohio, 1915-1930|url=https://books.google.com/books?id=Z2MCDFaRcD0C&pg=PA210&lpg=PA210&dq=United+Negro+Improvement+Association+(UNIA)+was+founded+in+Akron&source=bl&ots=tGpo-RTRO1&sig=MGIF3AD_mJULcTrUHWoJdXwwk1Q&hl=en&sa=X&ved=0ahUKEwiY5J7smr3RAhVDuhQKHYl0BWUQ6AEIMzAD#v=onepage&q=United%20Negro%20Improvement%20Association%20(UNIA)%20was%20founded%20in%20Akron&f=false|website=google|publisher=Thompson Shore, Inc.|accessdate=12 January 2017}}</ref> as a means of uniting all of Africa and its diaspora into "one grand racial hierarchy." After traveling through the United States beginning in March 1916, Garvey inaugurated the New York Division of the UNIA in 1917 with 13 members. After only three months, the organization's dues-paying membership reached 3,500.

''[[Negro World|The Negro World]]'' was founded on August 17, 1918, as a weekly newspaper to express the ideas of the organization. Garvey contributed a front-page editorial each week in which he developed the organization's position on different issues related to people of African ancestry around the world, in general, and the UNIA, in particular. Eventually claiming a circulation of 500,000, the newspaper was printed in several languages. It contained a page specifically for women readers, documented international events related to people of African ancestry, and was distributed throughout the African diaspora until publication ceased in 1933.

In 1919, the UNIA purchased the first of what would be numerous [[Liberty Hall (Harlem)|Liberty Halls]]. Located at 114 West 138th Street, [[New York City]], the building had a seating capacity of 6,000. The single-level hall with low ceilings had previously been home to the Metropolitan Baptist Tabernacle. It was dedicated on July 27, 1919. On Sunday evenings, it hosted the weekly UNIA meeting; it also housed a restaurant.<ref name=Robertson>Stephen Robertson, [http://digitalharlemblog.wordpress.com/2011/04/26/unia-harlem/ "The Universal Negro Improvement Association (UNIA) in Harlem"], ''Digital Harlem Blog'', April 26, 2011, accessed August 23, 2011.</ref> Later that year the Association organized the first of its two steamship companies and a separate business corporation.

Incorporated in [[Delaware]] as a domestic corporation on June 27, 1919, the [[Black Star Line|Black Star Line, Inc.]] (BSL) was capitalized at 10 million dollars. It sold shares individually valued at five dollars to both UNIA members and non-members alike. Proceeds from stock sales were used to purchase first the [[SS Yarmouth|SS ''Yarmouth'']] and then the [[SS Shadyside|SS ''Shadyside'']]. The ''Shadyside'' was used by the association for summer outings and excursions, as well as rented out on charter to other organizations. The BSL later purchased the ''[[Kanawha (1899)|Kanawha]]'' as its third vessel. This small yacht was intended for inter-island transportation in the West Indies and was rechristened the [[SS Antonio Maceo|SS ''Antonio Maceo'']].

Also established in 1919 was the [[Negro Factories Corporation]], with a capitalization of one million dollars. It generated income and provided around 700 jobs by its numerous enterprises: three grocery stores, two restaurants, a laundry, a tailor shop, a dressmaking shop, a millinery store, a printing company and doll factory. However, most went out of business by 1922.<ref name=Robertson/>

With the growth of its membership from 1918 through 1924, as well as income from its various economic enterprises, UNIA purchased additional Liberty Halls in the USA, Canada,<ref>Peter Edwards, "Black history looms large at busy corner".  ''[[Toronto Star]]'', January 26, 1998, p. B3, with reference to the importance of the hall located at 355 [[College Street (Toronto)|College Street]] in [[Toronto]], [[Canada]].</ref> Costa Rica, Belize, Panama, Java, and other countries. Furthermore, UNIA purchased farms in Ohio and other states. It purchased land in [[Claremont, Virginia]] with the intention of founding Liberty University.

==First international convention==
By 1920 the association had over 1,900 divisions in more than 40 countries. Most of the divisions were located in the United States, which had become the UNIA's base of operations. There were, however, offices in several Caribbean countries, with [[Cuba]] having the most. Divisions also existed in such diverse countries as [[Panama|Republic of Panama]], [[Costa Rica|Republic of Costa Rica]], [[Nicaragua|Republic of Nicaragua]], [[Ecuador|Republic of Ecuador]], [[Venezuela|Republic of Venezuela]], [[Ghana]], [[Sierra Leone]], [[Liberia]], [[India|British India]], [[Australia|Commonwealth of Australia]], [[Nigeria|Colonial Nigeria]], [[Namibia]], and [[South Africa|Union of South Africa]].[[Image:Marcus Garvey 1924-08-05.jpg|thumb|left|[[Marcus Garvey]]]]

For the entire month of August 1920, the [[UNIA-ACL]] held its first international convention at [[Madison Square Garden (1890)|Madison Square Garden]] in [[New York City]]. The 20,000 members in attendance promulgated [[The Declaration of the Rights of the Negro Peoples of the World|The Declaration of Rights of the Negro Peoples of the World]]<ref>Wikisource contributors, "The Declaration of the Rights of the Negro Peoples of the World," Wikisource, The Free Library, [http://en.wikisource.org/w/index.php?title=The_Declaration_of_the_Rights_of_the_Negro_Peoples_of_the_World&oldid=189864 "The Declaration of the Rights of the Negro Peoples of the World"] (accessed October 6, 2007).</ref> on August 13, 1920, and elected the leaders of the UNIA as "leaders for the Negro people of the world".
{{Listen|filename=Marcus Garvey, speech, 1921.ogg|title="Explanation of the Objects of the Universal Negro Improvement Association"|description=Complete 1921 speech|format=[[Ogg]]}}

The organization put forth a program based on "The Declaration of Rights of the Negro Peoples of the World", marking the evolution of the movement into a black nationalist one. It sought the uplift of the black race and encouraged self-reliance and nationhood. Among the declarations was one proclaiming the [[Pan-African flag|red, black and green flag]] the official banner of the African race. (Beginning in the 1960s, [[black nationalism|black nationalists]] and [[Pan-African]]ists adopted the same flag as the Black Liberation Flag.) UNIA-ACL officially designated the song "[[Ethiopia]], Thou Land of Our Fathers" as the official anthem of "Africa and the Africans, at home and Abroad".

Under the provisions of the UNIA constitution, [[Gabriel Johnson]] was elected Supreme Potentate; G. O. Marke, Supreme Deputy Potentate; J. W. [H]. Eason, leader of the fifteen million "Negroes" of the United States of America; and [[Henrietta Vinton Davis]], International Organizer. Garvey was elected "Provisional President of Africa", a mostly ceremonial title.

The opening parade provided one of the most striking features of the convention. It began outside the UNIA headquarters on West 135th Street, went uptown as far as 145th and downtown as far as 125th Street, taking it beyond the boundaries of black residence into white areas. Four mounted policemen, and the heads of Black Star Line and Negro Factories Corporation, led the parade, followed by cars carrying Garvey and Mayor of Monrovia, capital of Liberia, and other UNIA officials, then the Black Star Line Choir, on foot, and contingents from throughout US and Caribbean, Canada, Nigeria, carrying banners, and including 12 bands, with about 500 cars bringing up the rear.<ref>Stephen Robertson, [http://digitalharlemblog.wordpress.com/2011/02/01/parades-in-1920s-harlem/ "Parades in 1920s Harlem"], ''Digital Harlem Blog'', February 1, 2011, accessed August 23, 2011. </ref>  Similar dramatic parades featured in the 1922 and 1924 conventions; even after Garvey had left Harlem, the UNIA paraded each August throughout the 1920s, with the place of honour given to portraits of their absent leader.  The spectacle of uniformed members, particularly the African Legion and Black Cross Nurses, made a particular impact on the crowds.<ref name=Robertson/> Many photographs survive of the 1924 Convention Parade, as Garvey employed noted photographer [[James Van Der Zee]] to record the event.

==Liberian program==
Although UNIA was not solely a "Back to Africa" movement, the organization did work to arrange for migration for [[African Americans]] who wanted to go there. In late 1923, an official UNIA delegation which included [[Robert Lincoln Poston]] and [[Henrietta Vinton Davis]] travelled to [[Liberia]] to survey potential landsites. They also assessed the general condition of the country from the standpoint of UNIA members interested in living in [[Africa]].

By 1924 the Chief Justice [[J. J. Dossen]] of Liberia wrote to UNIA conveying the government's support: "The President directs me to say in reply to your letter of June 8 setting forth the objects and purposes of the Universal Negro Improvement Association, that the Government of Liberia, appreciating as they do the aims of your organization as outlined by you, have no hesitancy in assuring you that they will afford the Association every facility legally possible in effectuating in Liberia its industrial, agricultural and business projects."

About two months later, however, the Liberian President unexpectedly ordered all Liberian ports to refuse entry to any member of the "Garvey Movement". This action closely followed the [[Firestone Tire and Rubber Company|Firestone]] Rubber Company's agreement with Liberia for a 99-year lease of one million acres (4,000&nbsp;km²) of land. The land deal had been assisted by American and European governments. Originally Liberia had intended to lease the land to UNIA at an unprecedented dollar an acre ($247/km²). The commercial agreement with Firestone Tire dealt a severe blow to the UNIA's African repatriation program and inspired speculation that the actions were linked.<ref>[http://www.international.ucla.edu/africa/mgpp/sample06 Editorial by Marcus Garvey in the ''Negro World''], September 2, 1924.</ref>

[[Image:Flag of the UNIA.svg|thumb|right|The [[Pan-African flag|UNIA flag]] (also known as the [[Black nationalism|Black Nationalist Flag]]) uses three colors: red, black and green.]]

==Post-Garvey era==
After Garvey's conviction and imprisonment on mail fraud charges in 1925 and deportation to Jamaica in 1927, the organization began to take on a different character. In 1926, George Weston succeeded Garvey in a UNIA Convention Election, becoming the next second elected President-General of the UNIA, Inc. This angered many Garvey supporters and as a result spawned many rival entities such as the "Garvey Clubs" and other organizations based on members' differing interpretation of the original aims and objects of the UNIA.

As a result, the UNIA continued to be officially recognized as the "Universal Negro Improvement Association and African Communities League", and a rival "UNIA-ACL August 1929 of the World" emerged, headed by Marcus Garvey after his deportation to Jamaica.

Garvey appointed [[Maymie de Mena]] as his official representative to head the American field after his ouster to Jamaica. She was a fiery orator and spoke both Spanish and English, helping with the spread of the organization through [[Latin America]] and the [[Caribbean]].<ref>{{cite book|last1=Castillo-Garsow|first1=Melissa|editor-last1=Knight|editor-first1=Franklin W.|editor-last2=Gates, Jr.|editor-first2=Henry Louis|title=Dictionary of Caribbean and Afro–Latin American Biography|url=http://www.oxfordreference.com/search?source=%2F10.1093%2Facref%2F9780199935796.001.0001%2Facref-9780199935796&q=maymie+aiken|year=2016|publisher=Oxford University Press|location=Oxford, England|isbn=978-0-199-93579-6|chapter=Aiken, Maymie “Madame De Mena” (1891–1953)}} {{subscription needed|via=[http://www.oxfordreference.com/ Oxford University Press]'s Reference Online}}</ref> 

===The UNIA Inc===

The UNIA, Inc., after Garvey's departure, continued to operate out of New York until 1941. After Weston's 1926 election to President-General, he was succeeded by Frederick Augustus Toote (1929), Clifford Bourne (1930), Lionel Antonio Francis (1931–34), [[Henrietta Vinton Davis]] (1934–40), Lionel Antonio Francis (1940–61), Captain A L King (1961–81) and Milton Kelly, Jr. (1981–2007).

In a historic 1939 [[Supreme Court of the United Kingdom|British Supreme Court]] decision, President-General Francis was recognized as the rightful administrative heir to the huge Sir [[Isaiah Emmanuel Morter]] (DSOE) Estate in [[Belize]]. The organization's administrative headquarters were then shifted to Belize in 1941 when the President-General relocated there from New York.

Upon his death in 1961 during [[Hurricane Hattie]], the presidency shifted back to New York under the leadership of King, formerly president of the Central Division of the UNIA in New York. After his death in the early 1980s, longtime Garveyite organizer Kelly assumed the administrative reigns and continued to head the association until 2007.

===The UNIA-ACL 1929 of the World===

The UNIA 1929 headed by Garvey continued operating in Jamaica until he moved to [[England]] in 1935. There he set up office for the parent body of the UNIA 1929 and maintained contact with all its divisions. UNIA 1929 conventions were held in [[Canada]] in 1936, 1937 and 1938; the 1937 sessions were highlighted by the introduction of the first course of African philosophy conducted by Garvey.

Garvey became ill in January 1940, and died on June 10, 1940. UNIA members worldwide participated in eulogies, memorial services and processions in his honor. Secretary-General Ethel Collins briefly managed the affairs of the UNIA from New York until a successor to Garvey could be formally installed to complete his term as President-General.

During an emergency commissioners' conference in June 1940, [[James R. Stewart]], a commissioner from [[Ohio]] and graduate of the course of African philosophy, was named the successor. In the months to follow, the Parent Body of the UNIA was moved from its temporary headquarters in New York to [[Cleveland]]. In October 1940 the ''New Negro World'' started publishing out of Cleveland. After the 1942 International Convention in Cleveland, a rehabilitating committee of disgruntled members was held in New York during September.

===Parent Body in Monrovia===
Stewart moved to [[Monrovia]], [[Liberia]], in 1949. He took Liberian citizenship and moved the Parent Body of the UNIA there. He continued to lead the Association as President-General until his death in 1964. Stewart and his entire family relocated deeper into the interior of the country, establishing themselves in [[Gbandela]], [[Liberia]]. There they established a hospital, school and farm.

When Stewart died from cancer in 1964, the Parent Body was moved from Monrovia to [[Youngstown, Ohio]], where James A. Bennett took the reins. In 1968 Bennett was succeeded by Vernon Wilson.

After President-General Wilson's death in 1975, Mason Harvgrave became next President General. Hargrave testified during the congressional hearings in August 1987 in relation to the exoneration of Marcus Garvey on charges of mail fraud. The findings of the Judiciary Committee were: Garvey was innocent of the charges against him. Although the Committee determined he had been found guilty earlier due to the social climate of America at the time, they had no legal basis upon which to exonerate a person who was deceased.

After President General Hargrave died in 1988, all his papers and other Parent Body material were turned over to the [[Western Reserve Historical Society]] in Cleveland, Ohio, for safe-keeping. From 1988 until the present, [[Cleo Miller]], Jr. has held the title of President General.

==Notable members of the UNIA==
{{div col}}
*[[Dusé Mohamed Ali]]
*[[Eliezer Cadet]]
*[[Henrietta Vinton Davis]]
*[[William H. Ferris]]
*[[Arnold Josiah Ford]]
*[[Timothy Thomas Fortune]]
*[[Amy Jacques Garvey]]
*[[Marcus Garvey]]
*[[Hubert Henry Harrison]]
*[[Thomas Watson Harvey]]
*[[Samuel Alfred Haynes]]
*[[Maymie de Mena]]
*[[Robert Lincoln Poston]]
*[[Thomas Vincent Ramos]]
*[[James R. Stewart|James Robert Stewart]]
*[[Eric D. Walrond]]
{{div col end}}

==References==
{{Reflist}}

== Further reading ==

*{{Cite book |title=Defending the Master Race: Conservation, Eugenics, and the Legacy of Madison Grant |last=Spiro |first=Jonathan P. |publisher=Univ. of Vermont Press |year=2009 |isbn=978-1-58465-715-6 |laysummary=http://www.upne.com/1-58465-715-4.html |laydate=29 September 2010 |ref=harv }}
*Van Leeuwen, David. [http://nationalhumanitiescenter.org/tserve/twenty/tkeyinfo/garvey.htm "Marcus Garvey and the Universal Negro Improvement Association"], ''Divining America: Religion in American History'', National Humanities Center, 2000.

== External links ==
*[http://www.theunia-acl.com Official UNIA-ACL website]
*[http://ech.cwru.edu/ech-cgi/article.pl?id=UNIA Encyclopedia of Cleveland History:UNIVERSAL NEGRO IMPROVEMENT ASSN. (UNIA)]
*[https://www.facebook.com/groups/Millions4MarcusGarvey/ Official Blog of the UNIA on Facebook: Millions For Marcus Garvey On Facebook]
*[http://www.isop.ucla.edu/africa/mgpp/ The Marcus Garvey and Universal Negro Improvement Association Papers Project]
*[http://www.marcusgarvey.com Marcus Garvey: The Official Site]
*[http://www.galegroup.com/pdf/scguides/universalnegro1/uniawrintro.pdf Gale Group guide to UNIA]
*[http://www.isop.ucla.edu/africa/mgpp/sample01.asp American Series Sample Documents]—Volume I: 1826—August 1919
*[http://www.blackpast.org/primary/constitution-universal-negro-improvement-association-1918 1918 UNIA Constitution] 
{{Pan-Africanism}}

{{DEFAULTSORT:Universal Negro Improvement Association And African Communities League}}
[[Category:Universal Negro Improvement Association and African Communities League members| ]]
[[Category:African Americans' rights organizations]]
[[Category:African-American history between emancipation and the Civil Rights Movement]]
[[Category:African and Black nationalist organizations]]
[[Category:Black Star Line]]
[[Category:Community building]]
[[Category:Ethnic fraternal orders in the United States]]
[[Category:Pan-Africanist organizations]]
[[Category:Organizations established in 1914]]
[[Category:Repatriated Africans]]
[[Category:African diaspora history]]
[[Category:African-American repatriation organizations]]