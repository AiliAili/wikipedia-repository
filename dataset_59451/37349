{{for|the former airport|Båtsfjord Airport (1973–1999)}}
{{good article}}
{{Use dmy dates|date=April 2013}}
{{Infobox airport
| name         = Båtsfjord Airport
| nativename   = {{smaller|Båtsfjord lufthavn}}
| image        = Batsfjord-lufthavn.jpg
| image-width  = 300
| IATA         = BJF
| ICAO         = ENBS
| type         = Public
| owner        = 
| operator     = [[Avinor]]
| city-served  = [[Båtsfjord]], [[Norway]]
| location     = <!--if different than above-->
| elevation-f  = 490
| elevation-m  = 149
| coordinates  = {{coord|70|36|01|N|029|41|34|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_mapsize        = 300
| pushpin_label          = '''BJF'''
| pushpin_relief         = yes
| website      = [https://avinor.no/en/airport/batsfjord-airport/ Official website]
| metric-elev  = yes
| metric-rwy   = yes
| r1-number    = 03/21
| r1-length-m  = 1,000
| r1-length-f  = 3,281
| r1-surface   = Asphalt
| stat-year    = 2014
| stat1-header = Passengers
| stat1-data   = 12,363
| stat2-header = Aircraft movements
| stat2-data   = 2,494
| stat3-header = Cargo (tonnes)
| stat3-data   = 3
| footnotes    = Source:<ref name="info">{{cite web |title=ENBS – Båtsfjord |publisher=[[Avinor]] |url=https://www.ippc.no/norway_aip/current/AIP/AD/ENBS/EN_AD_2_ENBS_en.pdf |accessdate=9 April 2012 |format=PDF |archiveurl=https://web.archive.org/web/20120613194509/https://www.ippc.no/norway_aip/current/AIP/AD/ENBS/EN_AD_2_ENBS_en.pdf |archivedate=13 June 2012}}</ref><ref name=stats />
}}
'''Båtsfjord Airport''' ({{lang-no|Båtsfjord lufthavn}}, {{airport codes|BJF|ENBS|p=n}}) is a [[regional airport]] serving [[Båtsfjord]] in [[Finnmark]], [[Norway]]. It consists of a {{convert|1000|by|30|m|sp=us}}&nbsp;[[runway]] and served 12,363&nbsp;passengers in 2014. Scheduled services are provided by [[Widerøe]] using the [[Bombardier Dash 8|Dash 8]] to [[Kirkenes]], [[Hammerfest]] and other communities in Finnmark. The airport is owned and operated by the state-owned [[Avinor]].

It is the second airport in Båtsfjord. [[Båtsfjord Airport (1973–1999)|The first]] was built in 1973, but only had a gravel runway. When Widerøe replaced their smaller [[de Havilland Canada Twin Otter]]s with the Dash&nbsp;8, a new airport was needed to serve Båtsfjord. The new airport cost 178&nbsp;million [[Norwegian krone]] and opened on 9&nbsp;September&nbsp;1999.

==History==
:{{See also|Båtsfjord Airport (1973–1999)}}
The first airline to operate to Båtsfjord was Varangfly, later renamed Norving, who flew [[seaplane]] taxi and [[Air ambulance|ambulance]] flights in the early 1960s.<ref>Melling: 39</ref> Construction of an airfield in Båtsfjorddalen was started by the aviation club Båtsfjord Flyklubb in 1972 and completed in May&nbsp;1973. It consisted of an {{convert|800|m|sp=us|adj=on}}&nbsp;gravel runway and was served with regular flights by Norving. The municipality took over ownership of the airport in 1976.<ref>Melling: 101</ref> Norving remained the sole operator until 1990, when the routes were taken over by Widerøe using the Twin Otter.<ref name=gynnild>{{cite web |url=http://avinor.moses.no/index.php?seks_id=135&element=kapittel&k=2 |title=Flyplassenes og flytrafikkens historie |work=Kulturminner på norske lufthavner – Landsverneplan for Avinor |publisher=[[Avinor]] |last=Gynnild |first=Olav |year=2009  |accessdate=25 January 2012 |archivedate=25 January 2013 |archiveurl=http://www.webcitation.org/6DwQ1MXLE?url=http://avinor.moses.no/index.php?seks_id%3D135%26element%3Dkapittel%26k%3D2 |deadurl=no}}</ref>

[[File:Batsfjord-lufthavn-terminal.jpg|thumb|upright|left|Departures hall with the winged-shaped curve of the roof visible]]
The [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]] announced in February&nbsp;1993 that they were considering taking over ownership and operations of the airport through the Civil Aviation Administration (CAA, later renamed Avinor).<ref>{{cite news |title=flyplass Båtsfjord |last=Gustad |first=Ragnhild |work=[[Nordlys]] |date=9 February 1993 |page=13 |language=Norwegian}}</ref> In December, Widerøe articulated that Båtsfjord Airport did not meet the demands for future operation as it lacked proper [[navigational aid]]s and an asphalt runway. Widerøe stated that with the introduction of the Dash&nbsp;8, which would be phased in between 1993 and 1995, they would no longer be able to serve the airport. The CAA stated, instead, that the new airport should be built, estimated to cost between 70 and 80&nbsp;million [[Norwegian krone]] (NOK).<ref>{{cite news |title=Store utgifter for små flyplasser |last=Rapp |first=Magnus |work=[[Aftenposten]] |date=1 December 1993 |page=3 |language=Norwegian}}</ref> The main reason was that the former airport was poorly located in relation to the terrain, being located in a valley. In April&nbsp;1994 the ministry stated that they were considering closing many of the smallest regional airports, including Båtsfjord.<ref>{{cite news |title=Widerøe vil fly  |last=Gustad |first=Ragnhild |work=[[Nordlys]] |date=23 April 1994 |page=13 |language=Norwegian}}</ref>

In April&nbsp;1995, when [[Parliament of Norway|Parliament]] decided to nationalize twenty-six regional airports owned by their respective municipalities, they also debated a proposal to build a new airport in Båtsfjord. At the time Widerøe was obliged to operate routes to the airport until 31&nbsp;March&nbsp;1997.<ref>{{cite news |title=Båtsfjord kan miste flyplass |last=Rapp |first=Magnus |work=[[Aftenposten]] |date=6 April 1995 |page=60 |language=Norwegian}}</ref> In December&nbsp;1995, Widerøe took delivery of their fifteenth Dash&nbsp;8 and thus their only need for a Twin Otter was to serve Båtsfjord.<ref>{{cite news |title=Widerøe vil nå videre ut |last=Evensen |first=Kjell |work=[[Dagens Næringsliv]] |date=13 December 1995 |page=10 |language=Norwegian}}</ref> From 1994 to 1997, the cost estimates for the new airport increased from NOK&nbsp;95 to 125&nbsp;million. By June&nbsp;1998, Parliament approved additional funding which brought the cost to NOK&nbsp;178 million, including a new county road for NOK&nbsp;8&nbsp;million.<ref>{{cite news |title=Torp flyplass tapte for Båtsfjord |last=Width |first=Henrik |work=[[Aftenposten]] |date=19 June 1998 |page=5 |language=Norwegian}}</ref> The airport opened on 9 September 1999.<ref>{{cite news |title=Flertall på Stortinget Nei til småfly på kortbanene |last=Rapp |first=Magnus |work=[[Aftenposten]] |date=8 September 1999 |page=48 |language=Norwegian}}</ref> From the opening Widerøe used the Twin Otter until 1&nbsp;April&nbsp;2000, when the route was taken over by larger Dash&nbsp;8 aircraft.<ref>{{cite news |title=Twin Otter: Trofast sliter settes på bakken |last=Rapp |first=Magnus |work=[[Aftenposten]] |date=18 March 2000 |page=39 |language=Norwegian}}</ref> [[Airport security]] was introduced on 1&nbsp;January&nbsp;2005.<ref>{{cite news |title=Tre usikre flyplasser i Midt-Norge |last=Solberg |first=Pål E. |work=[[Adresseavisen]] |date=30 September 2004 |page=4 |language=Norwegian}}</ref>

==Facilities==
[[File:Båtsfjord lufthavn.jpg|thumb|Airport air side]]
The terminal consists of a single building, with an integrated control tower, capable of handling 120&nbsp;passengers per hour.<ref name=barents>{{cite web|url=http://www.regjeringen.no/upload/OED/pdf%20filer/Barentshavet_S/KI/14_Luftfart.pdf |title=Konsekvenser for luftfart |publisher=Avinor |date=October 2012 |language=Norwegian |pages=55–54 |format=PDF |accessdate=25 January 2012 |archivedate=25 January 2013 |archiveurl=http://www.webcitation.org/6DvtAOwz2?url=http://www.regjeringen.no/upload/OED/pdf%20filer/Barentshavet_S/KI/14_Luftfart.pdf |deadurl=no |df=dmy }}</ref> The facade is covered in [[Russian larch]];<ref name=fisk>{{cite news |title=Flyplass for fisk og folk |last=Rapp |first=Magnus |work=[[Aftenposten]] |date=6 April 1999 |page=64 |language=Norwegian}}</ref> the focus on wood as a dominant material was common for [[Norwegian architecture]] the 1990s, and is also seen at [[Oslo Airport, Gardermoen]] (opened in 1998) and for the [[venues of the 1994 Winter Olympics]].<ref name=gynnild /> The terminal was designed by Alta-based architect Solveig Thorsen and is shaped like an aircraft wing, similar to the terminal building at Gardermoen. There is a single gate at the airport, which prior to the installation of security control was 17&nbsp;steps from the entrance.<ref name=fisk />

The airport resides at an elevation of {{convert|149|m|sp=us}}&nbsp;above [[mean sea level]]. The runway is designated 03–21, with an asphalt surface measuring {{convert|1000|by|30|m|sp=us}}.<ref name="info" /> [[SCAT-I]], a [[Global Positioning System]]-based landing system, is under deployment. It is possible to extend the runway to {{convert|1200|m|sp=us}} if desirable in the future. The tarmac has space for three simultaneous aircraft.<ref name=barents /> The airport is located ten minutes' drive (7 km/4 mi) from the town center. There are taxis, paid parking and car rental available, but no bus service.<ref>{{cite news|title=Getting to and from the airport |publisher=[[Avinor]] |url=http://www.avinor.no/en/airport/batsfjord/tofromairport |accessdate=19 April 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120528113400/http://www.avinor.no:80/en/airport/batsfjord/tofromairport |archivedate=28 May 2012 |df=dmy }}</ref>

== Airlines and destinations ==
All services from the airport are operated by Widerøe with 39-seat Dash-8-100 aircraft. All routes, though not necessarily direct, fly to either [[Kirkenes Airport, Høybuktmoen]], [[Hammerfest Airport]] or [[Tromsø Airport]].<ref>{{cite web|url=http://www.regjeringen.no/Upload/SD/Vedlegg/rapporter_og_planer/2012/rapp_ruter_2013.pdf |title=Alternative ruteopplegg for Finnmark og Nord Troms |last=Draagen |first=Lars |last2=Wilsberg |first2=Kjell |publisher=Gravity Consult |year=2011 |page=11 |language=Norwegian |accessdate=1 October 2012 |archivedate=1 October 2012 |archiveurl=http://www.webcitation.org/6B5OfdMM6?url=http://www.regjeringen.no/Upload/SD/Vedlegg/rapporter_og_planer/2012/rapp_ruter_2013.pdf |deadurl=no |format=PDF |df=dmy }}</ref> The routes from Båtsfjord are operated on a [[public service obligation]] with the Ministry of Transport and Communications.<ref>{{cite web |title=Routes operated on public service obligation in EU and EEA |publisher=[[European Commission]] |url=http://ec.europa.eu/transport/air/internal_market/doc/2009_12_pso-eu_and_eea.pdf |format=PDF |accessdate=19 April 2012}}</ref> The airport served 12,363&nbsp;passengers, 2,494&nbsp;aircraft movements and handled 3&nbsp;tons of cargo in 2012.<ref name=stats>{{cite web |url=http://www.mynewsdesk.com/material/document/42080/download?resource_type=resource_document |title=Månedsrapport |publisher=[[Avinor]] |format=XLS |accessdate=13 January 2015 |year=2015}}</ref> The airport operated with a deficit of NOK&nbsp;20 million in 2012.<ref>{{cite news |url=http://www.nrk.no/mr/7-av-46-flyplasser-med-overskudd-1.10970207 |title=Kun 7 av 46 flyplasser gikk med overskudd i 2012 |publisher=[[Norwegian Broadcasting Corporation]] |last=Riise |first=Ivar Lid |language=Norwegian |date=2 April 2013 |accessdate=13 January 2015}}</ref>

{{Airport-dest-list
| [[Widerøe]] | [[Berlevåg Airport|Berlevåg]], [[Hammerfest Airport|Hammerfest]], [[Kirkenes Airport, Høybuktmoen|Kirkenes]], [[Mehamn Airport|Mehamn]], [[Tromsø Airport|Tromsø]], [[Vadsø Airport|Vadsø]], [[Vardø Airport, Svartnes|Vardø]]
}}

==References==
{{reflist|30em}}

;Bibliography
* {{cite book |last=Melling |first=Kjersti |title=Nordavind fra alle kanter |year=2009 |publisher=Pilotforlaget |location=Oslo |language=Norwegian}}

==External links==
[[File:Wind rose 2016 for ENBS Båtsfjord, Norway.jpg|thumb|left|[[Wind rose]] showing distribution of wind speed and direction for Båtsfjord Airport for the year 2016.]]
{{commons category|Båtsfjord Airport}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway|Arctic}}

{{DEFAULTSORT:Batsfjord Airport}}
[[Category:Airports in Finnmark]]
[[Category:Avinor airports]]
[[Category:1999 establishments in Norway]]
[[Category:Airports established in 1999]]
[[Category:Båtsfjord]]