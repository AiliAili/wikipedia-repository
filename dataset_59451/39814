{{Infobox military person
|name= Richard John Cork
|image= 
|caption= 
|birth_date= {{birth date|1917|4|4|df=yes}}
|death_date= {{death date and age|1944|4|14|1917|4|4|df=yes}}
|birth_place= [[London]], England
|death_place= [[China Bay]]
|placeofburial= [[Trincomalee British War Cemetery]], Ceylon
|nickname= Dickie
|allegiance= {{flag|United Kingdom}}
|branch= {{navy|United Kingdom}}
|serviceyears= 1939–1944
|rank= [[Lieutenant Commander]]
|commands= [[880 Naval Air Squadron]]<br/>[[15th Naval Fighter Wing]]
|unit= [[Fleet Air Arm]]
|battles= [[Second World War]]
* [[Battle of Britain]]
|awards= [[Distinguished Service Order]]<br />[[Distinguished Service Cross (United Kingdom)|Distinguished Service Cross]]
|laterwork=
}}

'''Richard John (Dickie) Cork''' {{postnominals|country=GBR|size=100%|sep=,|DSO|DSC}} (4 April 1917 – 14 April 1944) was a [[fighter ace]] in the [[Fleet Air Arm]] of the [[Royal Navy]] during the [[Second World War]]. Cork served in the [[Battle of Britain]] as the [[wingman]] for [[Douglas Bader]] of [[No. 242 Squadron RAF]]. Paul Brickhill states that in October 1940, he was awarded a [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC), which at the insistence of the [[Admiralty]] was exchanged for a [[Distinguished Service Cross (United Kingdom)|Distinguished Service Cross]] (DSC).<ref>Brickhill (2009), p.166</ref> However, Hugh Halliday corrects this myth and advises that the DFC was recommended by Bader but actually awarded as the naval equivalent, the DSC, in the ''London Gazette'' issue of 18 October 1940,<ref>Halliday (1981), p.144</ref><ref name="thegazette.co.uk">https://www.thegazette.co.uk/London/issue/34974/supplement/6111</ref> although he confirms that as squadron commander Bader insisted that Cork wear the ribbon of the DFC whilst serving with the RAF; this is confirmed by contemporary photographs.<ref>Halliday (1981), p.145</ref>

When he returned to the Fleet Air Arm, Cork served with [[880 Naval Air Squadron]] in the Arctic, Mediterranean and Indian Ocean. It was during [[Operation Pedestal]] in 1942 that he became the only Royal Navy pilot to shoot down five aircraft in one day, and was the leading naval ace using the [[Hawker Hurricane]].<ref>Thomas (2007), p.89.</ref> He was given command of the [[15th Naval Fighter Wing]] aboard {{HMS|Victorious|R38|6}} before being killed in a flying accident over Ceylon in 1944.

==Early life==
Richard John Cork was born in London, England on 4 April 1917.<ref name="Shores 1994, p.191">Shores (1994), p.191</ref> He was the son of Harold James Cork and Ethel Mary Cork, of [[Burnham, Buckinghamshire|Burnham]] in [[Buckinghamshire]].<ref name=duty>{{cite web|accessdate=31 October 2010|title=Royal Navy Officers|publisher=Unit Histories|url=http://www.unithistories.com/officers/RN_officersC2a.html}}</ref> 
In the months prior to the war the [[Royal Navy]] encouraged school leavers to enlist by offering them short-service commissions. Cork was one of those that signed up in 1939. Successfully passing an interview and medical, he joined the air branch and was promoted to acting [[sub-lieutenant]] on 1 May 1939.<ref name=duty/><ref>Barber (2002) p.9.</ref><ref>Burns (1999), p.53</ref> Attached to HMS ''President'' he was posted to No.14 Elementary and Reserve Flying Training School at Gravesend aerodrome where his flying course began on 21 August 1939. On 28 October 1939 on completion of his course Cork was posted to No.1 Flying Training School at Netheravon and on graduation from this school wrote in his flying logbook "Authorised to wear the flying badge with effect from 20 January 1940".<ref name="Burns 1999, p.54">Burns (1999), p.54</ref>

==Second World War==
Cork was promoted sub-lieutenant in March 1940.<ref name=duty/> From 21 April until 11 June 1940 he served with No.759 and No.760 Squadrons flying Skuas and Gladiators and on 11 June he was graded as an above average pilot.<ref name="Burns 1999, p.54"/> A shortage of fighter pilots during the [[Battle of Britain]] led to the Fleet Air Arm asking for volunteers to serve with the [[Royal Air Force]]. On 1 July 1940, Cork and two other naval pilots joined the [[Hawker Hurricane]] equipped [[No. 242 Squadron RAF|No. 242 Squadron]] under the command of [[Squadron Leader]] [[Douglas Bader]]; Cork was assigned to become Bader's [[wingman]].<ref>Thomas (2007), p.18.</ref><ref>{{cite web|accessdate=1 November 2010|title=The Recent Battle of Britain celebrations|publisher=The Fleet Air Arm Association|url=http://www.faaa.org.uk/}}</ref> On 30 August, he was involved in his first combat action with No. 242 Squadron. The unit claimed 12 aircraft destroyed, and Cork was credited with a [[Messerschmitt Bf 110]] destroyed and a share in a second. By 13 September he had shot down five aircraft and became a [[fighter ace]].<ref name=th21>Thomas (2007), P.21.</ref> For his exploits he was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC) on 18 October, which at the insistence of the [[Admiralty]] was exchanged for a [[Distinguished Service Cross (United Kingdom)|Distinguished Service Cross]] (DSC).<ref name="thegazette.co.uk"/><ref>{{cite journal|year=2010 |title=Opinion |journal=Navy News |publisher= |volume= |issue= |pages= |url= http://content.yudu.com/A1p9ls/navynewsoct10/resources/28.htm}}</ref> Out of the 58 Fleet Air Arm pilots seconded to the RAF during the Battle of Britain, 12 of them shot down at least one aircraft, five became aces, seven were killed and two wounded.<ref name=th21/>
[[File:Hawker Sea Hurricanes.jpg|thumb|||[[Hawker Sea Hurricane]]s flying in formation.]]

Cork returned to the Fleet Air Arm after the battle and was posted to [[880 Naval Air Squadron]]. The unit was equipped with the [[Grumman Martlet]], which were exchanged for [[Hawker Sea Hurricane]]s by mid-1941.<ref>Thomas (2007), pp.44–45.</ref> The squadron then joined {{HMS|Furious|47|6}} for attacks on [[Pechengsky District|Petsamo]] and [[Kirkenes]] in Arctic Norway. Cork flew two missions but did not come into contact with the German defenders.<ref>Thomas (2007), P.45.</ref> After this attack, 880 Squadron joined the newly built [[fleet carrier]] {{HMS|Indomitable|92|6}} in October 1941,<ref>Thomas (2007), p.46.</ref> and Cork was promoted to [[Lieutenant (navy)|lieutenant]] the following month.<ref name=duty/> One of the squadron's first operations with ''Indomitable'' involved the attack on [[Vichy French]] gun positions during the landings at [[Antsiranana|Diego Suarez]], [[Madagascar]] on 6 May 1942. During these operations Cork claimed three [[Morane-Saulnier M.S.406]]s and four [[Potez 63]]s, all destroyed on the ground.<ref>Thomas (2007), p.37.</ref> On 12 August 1942, during [[Operation Pedestal]], he became the only Royal Navy pilot to shoot down five aircraft in one day, for which he was awarded the [[Distinguished Service Order]] (DSO)on 10 November 1942.<ref>https://www.thegazette.co.uk/London/issue/35780/supplement/4879</ref> Flying a Sea Hurricane, his first success was at 12:30 hours when he shot down a [[Savoia-Marchetti SM.79]] over the convoy. Then, flying off the coast of [[Tunisia]], he shot down a [[Junkers Ju 88]] and shared in the destruction of another. Later in the day he shot down a Messerschmitt Bf 110 and another Savoia-Marchetti SM.79.<ref>Thomas (2003), p.33.</ref> The squadron leader, [[Lieutenant-Commander]] [[F.E.C. Judd]], was killed during these battles and Cork as the senior pilot was given command of 880 Squadron.<ref>Thomas (2007), P.40.</ref> In September 1942 he was promoted to acting lieutenant commander.<ref name=duty/>

In November 1943 he was posted to HMS ''Illustrious'' as wing leader of the [[15th Naval Fighter Wing]],<ref name="Shores 1994, p.191"/> comprising three squadrons of [[Vought F4U Corsair]]s, on board {{HMS|Victorious|R38|6}}. The carrier sailed for the Indian Ocean to join the [[British Pacific Fleet]]. After arriving Cork was killed in a flying accident while landing at [[China Bay]], [[Ceylon]] on 14 August 1944.<ref name="Shores 1994, p.191"/> His final score was nine destroyed, two shared, one probable, four damaged and seven destroyed on the ground. He was fifth on the table of Royal Navy Second World War aces.<ref>{{cite news|accessdate=31 October 2010|publisher=''[[Daily Telegraph]]''|title=Obituary, Mike Tritton|url=http://www.telegraph.co.uk/news/obituaries/1405742/Lieutenant-Commander-Mike-Tritton.html|location=London|date=30 August 2002}}</ref><ref>Holmes (1998), p.98.</ref><ref>Thomas (2007), p.87.</ref> He was buried in Trincomalee War Cemetery.<ref>[http://www.cwgc.org/search/casualty_details.aspx?casualty=2112046 CWGC entry]</ref>

==Notes==
{{reflist|group=nb}}
{{Reflist|30em}}

==References==
*{{cite book|last=Barber|first=Mark|title=The British Fleet Air Arm in World War II|publisher=Osprey Publishing|year=2008|location=Oxford|isbn=1-84603-283-0}}
*{{cite book|last=Brickhill|first=Paul|year=2009|title=Reach for the sky|publisher=W and N|location=London|isbn=978-0304356744}}
*{{cite book|last=Burns|first=Michael|year=1990|title=Bader: The Man and His Men|publisher=Arms and Armour|location=London|isbn=1-85409-246-4}}
*{{cite book|last=Halliday|first=Hugh|year=1981|title=242 Squadron: The Canadian years|publisher=Midland Counties|location=Earl Shilton|isbn=0-920002-09-9}}
*{{cite book|last=Holmes|first=Tony|year=1998|title=Hurricane Aces 1939–40|publisher=Osprey Publishing|location=Oxford|isbn=978-1-85532-597-5}}
*{{cite book|last=Shores|first=Christopher|year=1994|title=Aces High|publisher=Grub Street|location=London|isbn=1-898697-00-0}}
*{{cite book|last=Thomas|first=Andrew|year=2002|title=Hurricane Aces 1941–45|publisher=Osprey Publishing|location=Oxford|isbn=1-84176-610-0}}
*{{cite book|last=Thomas|first=Andrew|year=2007|title=Royal Navy Aces of World War 2|publisher=Osprey|location=Oxford|isbn=978-1-84603-178-6}}
*{{cite book|last=Wren|first=AH|year=1998|title=Naval Fighter Pilot: Lt.Cdr.R.J.Cork DSO, DSC, RN|publisher=Heron Books|location=Lichfield|isbn=978-0953225002}}

{{Top UK World War II Aces}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Royal Navy
| portal4=World War II
}}

{{DEFAULTSORT:Cork, Richard John}}
[[Category:Companions of the Distinguished Service Order]]
[[Category:Recipients of the Distinguished Service Cross (United Kingdom)]]
[[Category:British World War II flying aces]]
[[Category:Royal Navy officers]]
[[Category:The Few]]
[[Category:1944 deaths]]
[[Category:Fleet Air Arm aviators]]
[[Category:British military personnel killed in World War II]]
[[Category:1917 births]]
[[Category:British World War II pilots]]