{{peacock|date=February 2017}}
{{Infobox Organization 
|image = Canadian_International_Council_Official_Logo.pdf
|name = Canadian International Council
|image_border = 
|caption =
|membership =
|headquarters = [[Toronto]], [[Ontario]], [[Canada]]
|formation = 1928 (as Canadian Institute of International Affairs)
|website = [http://www.opencanada.org www.opencanada.org]
}}
The '''Canadian International Council''' (CIC) is [[Canada]]’s foreign relations council. It is an independent, member-based council established to strengthen Canada’s role in international affairs. The CIC reflects the ideas and interests of a broad constituency of Canadians who believe that a country’s foreign policy is not an esoteric concern of experts but directly affects the lives and prosperity of its citizens. The CIC uses its deep historical roots, its cross-country network, and its research to advance debate on international issues across academic disciplines, policy areas, and economic sectors.

The CIC interacts with its audience and membership in person, in print, and online. The Council is headquartered in [[Toronto]], [[Ontario]], with 15 volunteer-run branches across Canada. CIC branches present offer CIC members speakers’ programs, study groups, conferences, and seminars. Branches are located in [[Calgary]], [[Edmonton]], [[Halifax Regional Municipality|Halifax]], [[Hamilton, Ontario|Hamilton]], [[Montreal]], National Capital ([[Ottawa]]), Nipissing ([[North Bay, Ontario|North Bay]]), [[Saskatoon]], South Saskatchewan ([[Regina, Saskatchewan|Regina]]), [[Thunder Bay]], Toronto, [[Vancouver]], [[Victoria, British Columbia|Victoria]], [[Waterloo, Ontario|Waterloo]], and [[Winnipeg]].

The CIC’s top quality foreign policy research program consists of an annual research project, the [http://opencanada.org/projects/china/ China Working Group], the [http://opencanada.org/projects/strategic-studies/ Strategic Studies Working Group], the [http://www.irdtp.org/ International Relations and Digital Technology Project] (IRDTP), and [http://opencanada.org/international-journal/ International Journal], the Council’s academic publication.

The CIC’s digital media platform, [http://www.opencanada.org OpenCanada.org] is Canada’s hub for international affairs. Building on the CIC’s mandate to promote discussion on international affairs, the platform is the Canadian venue for those discussions. '''OpenCanada.org''' has published over 300 articles, 90 videos, and 20 In Depth series. OpenCanada leads the social media conversation on international affairs in Canada, with a highly engaged audience of over 20,000 on Twitter, Facebook, Tumblr, LinkedIn, Google+, and YouTube.

==History==

The CIC has its roots in 1928, in the '''Canadian Institute of International Affairs''' (CIIA). In 1932, [[Escott Reid]] was appointed as the Institute’s first full-time National Secretary and began organizing annual study conferences where ideas could be exchanged. The conferences were largely round-table discussions and members of branch study groups were invited to participate. Reid also encouraged expansion of the CIIA’s membership and greater public participation in the work of the Institute. The CIC’s first corporate record dates to 1950, with the objective “to give attention to Canada’s position both as a member of the international community of nations and as a member of the British Commonwealth of Nations.”<ref>[https://www.ic.gc.ca/app/scr/cc/CorporationsCanada/fdrlCrpDtls.html?corpId=347591&V_TOKEN=1369861189749&crpNm=Canadian%20INternational%20Council&crpNmbr=&bsNmbr= Federal Corporation Information - 347591 - Corporations Canada - Corporations - Industry Canada]. Ic.gc.ca (2013-10-17). Retrieved on 2013-10-23.</ref>

In October 2007, [[Jim Balsillie]] (the former co-[[CEO]] of the Canadian information technology company [[Research In Motion]] ('BlackBerry')) initiated the formation of the CIC as a partnership between the CIIA and the [[Centre for International Governance Innovation]] (CIGI), a [[think tank|think-tank]] based in [[Waterloo, Ontario]] that works on global issues, in order to create a research base on Canadian foreign policy similar to the American [[Council on Foreign Relations]] and the [[United Kingdom]]'s [[Royal Institute of International Affairs]].<ref name="Balsillie2007">{{cite web|last1=Balsillie|first1=Jim|title=Why we’re creating the Canadian International Council|url=https://www.cigionline.org/articles/2007/10/why-we%E2%80%99re-creating-canadian-international-council|publisher=Centre for International Governance Innovation|accessdate=19 March 2016}}</ref> In making the announcement, Balsillie wrote, "CIC will be a research-based, non-partisan vehicle. Applying expert and fact-based research to complex issues is the essential foundation for creating effective policy."<ref name="Balsillie2007"/> In November 2007, members of the CIIA voted to become the Canadian International Council | Conseil international du Canada.

In May 2008, the '''Canadian Institute of Strategic Studies''' (CISS) folded its operations into the CIC as the Strategic Studies Working Group.<ref>[http://opencanada.org/projects/strategic-studies/ Canadian International Council – Canada's hub for international affairs » Strategic Studies Working Group]. Opencanada.org. Retrieved on 2013-10-23.</ref>

Today, the CIC continues to expand nationally and globally as a voice in international affairs.

==Awards==

For two years running, the CIC was recognized at the Canadian Online Publishing Awards for its work with OpenCanada.<ref>http://www.canadianonlinepublishingawards.com/2013/winners.php</ref> In 2013 the site won the Content of the Year award, as well as two gold medals for best overall online-only publication and online-only article or series in the academic and nonprofit media category.<ref>http://opencanada.org/features/blogs/roundtable/opencanada-org-wins-gold-at-copa/</ref>

==Research==

CIC research extends through various programs, working groups, and projects.

=== IRDTP ===

[[File:IRDTP logo.jpg|thumb|IRDTP logo]] 
The [http://www.irdtp.org International Relations and Digital Technology Project](IRDTP) is managed jointly Between the CIC, the [[Liu Institute for Global Issues]] and the [[University of British Columbia]]’s Graduate School of Journalism. IRDTP is a new and innovative research initiative analyzing the impact of ubiquitous digital technology on the theory and practice of International Relations.

=== Annual Research Program ===

The CIC’s annual research programs have produced the following reports:

''[http://opencanada.org/features/reports/the-9-habits-of-highly-effective-resource-economies/ 9 Habits of Highly Effective Resource Economies: Lessons for Canada]'', by Madelaine Drohan, Canada correspondent for The Economist

''[http://opencanada.org/features/reports/ip/ Rights and Rents: Why Canada must harness its intellectual property resources]'', by Karen Mazurkewich

''[http://opencanada.org/features/reports/opencanada/ Open Canada: A Global Positioning Strategy for a Networked Age]'', by [[Edward Greenspon]]

=== Projects and Working Groups ===

CIC Projects and Working Groups include:<ref>[http://opencanada.org/category/projects/ Canadian International Council – Canada's hub for international affairs » Projects]. Opencanada.org. Retrieved on 2013-10-23.</ref>

* [http://opencanada.org/projects/strategic-studies/ The Strategic Studies Working Group], in partnership with the [[Canadian Defence and Foreign Affairs Institute]] (CDFAI)
* Arctic Sovereignty and Security Working Group 
* Border Issues Working Group
* Canada and the Americas project 
* Canada-India Relations project
* China Working Group
* Energy Working Group

==Governance==

The CIC’s Board of Directors<ref>[http://opencanada.org/about/board-of-directors/ Canadian International Council – Canada's hub for international affairs » Board of Directors]. Opencanada.org. Retrieved on 2013-10-23.</ref> includes:
* [[Bill Graham (Canadian politician)|Bill Graham]], Chancellor of Trinity College (Chair) 
* [[Perrin Beatty]], President and CEO, Canadian Chamber of Commerce (Vice Chair)
* [[Jim Balsillie]], Chair of the Board of CIGI
* [[David Bercuson]], Director, Centre for Military and Strategic Studies, [[University of Calgary]]
* [[Raymond Chrétien]], Strategic Advisor, Fasken Martineau
* Nicholas Hirst, President, CIC-Winnipeg Branch
* Keith Martin, Past-President, CIC-Toronto Branch 
* [[Jodi White]], Distinguished Senior Fellow, Norman Patterson School of International Affairs and Arthur Kroeger College of Public Affairs, [[Carleton University]]
* Jennifer Jeffs, President, CIC (ex officio)
* Scott Burk, President, Wealhouse Capital Management
* Edward Goldenberg, Partner, Bennett Jones LLP
* Doug Horswill, Senior Vice President, [[Teck Resources]]
* [[Janice Stein]], Director, [[Munk School of Global Affairs]], University of Toronto

CIC's president is Dr. Jennifer Jeffs, former Deputy Executive Director at the [[Centre for International Governance Innovation]]. Dr. Jeffs has held a variety of leadership positions in think tanks, universities and public policy institutes in Canada, Mexico, and the U.S. She is a member of the editorial board of Foreign Affairs Latinoamérica; a member of the editorial board of International Journal; a Director of the Centro de Estudios y Programas Interamericanos (CEPI) in Mexico City; a Director of the World Wildlife Fund, Canada; and a member of the International Women’s Forum (IWF). Dr. Jeffs holds a Ph.D. in International Political Economy from the University of Toronto.<ref>[http://opencanada.org/about/team/ Canadian International Council – Canada's hub for international affairs » Team]. Opencanada.org. Retrieved on 2013-10-23.</ref>

==Publications==
{{Infobox journal
| title = International Journal
| cover = 
| discipline = Interdisciplinary
| abbreviation = 
| editor = 
| publisher = [[Sage Publications|Sage]]
| country = [[Canada]]
| frequency = <!-- e.g. Annual -->
| history = 1946–present
| impact = 
| impact-year = 
| website = https://www.opencanada.org/features/international-journal/
| link1 = https://www.jstor.org/journals/00207020.html
| link1-name = Online access
| link2 = 
| link2-name = 
| ISSN = 0020-7020
| eISSN = 
| OCLC = 
| LCCN = 
| JSTOR = 00207020
}}
''International Journal'' (''IJ''), established in 1946, is the CIC’s scholarly publication and Canada’s pre-eminent journal of global policy analysis. ''IJ'' is cross disciplinary, combining the insights of history, political science, and economics with anthropology and other social sciences to advance research and dialogue on issues of global significance.

In 2013 the CIC partnered with the [http://ccih.utoronto.ca/ Bill Graham Centre for Contemporary International History] and [[SAGE Publications]] to publish ''International Journal''.

==Funding==

The Canadian International Council is a non-for-profit organization and a registered charity with [[Canada Revenue Agency]].

Funding comes from private sponsorship, membership fees, donations, and events. The CIC's major fundraiser is its Annual Gala Dinner, where the Globalist of the Year Award is presented. Past recipients include [[Christine Lagarde]], Managing Director of the [[International Monetary Fund]] in 2012; [[Naguib Sawiris]], CEO of [[Orascom Telecom Holding]] and Founding Member of the [[Free Egyptians Party]] (Al Masreyeen Al Aharrar), a [[political party]] founded in the wake of the [[Arab Spring]] in 2011; [[George Soros]], Chairman of Soros Fund Management LLC and Founder and Chairman of the Open Society Institute in 2010; [[Pascal Lamy]], Director-General of the World Trade Organization in 2009; His Excellency Cheng Siwei, former Vice-Chairman of the 9th and 10th Standing Committee of the National People’s Congress of the Republic of China and Chairman of the China Democratic National Construction Assembly in 2008; and [[Ángel Gurría]], Secretary-General of the Organisation for Economic Co-operation and Development in 2007.

==References==
{{reflist}}

==External links==
*[http://www.opencanada.org Canadian International Council]
*[http://www.irdtp.org International Relations and Digital Technology Project]
*[http://opencanada.org/international-journal/ International Journal]

[[Category:Political and economic think tanks based in Canada]]
[[Category:Charities based in Canada]]
[[Category:Non-profit organizations based in Canada]]
[[Category:Organizations established in 1928]]