{{Infobox organization
|name           = '''Estonian Physical Society''' ('''''Eesti Füüsika Selts''''')
|bgcolor        =
|fgcolor        =
|image          = Estonian Physical Society.jpg
|image_border   =
|size           = 150px
|alt            =
|caption        =
|map            =
|msize          =
|malt           =
|mcaption       =
|abbreviation   = EFS
|motto          =
|formation      = 19 May 1989<ref name="aboutus"/>
|extinction     =
|type           =
|status         =
|purpose        =
|headquarters   =
|location       =
|region_served  = Estonia
|membership     =
|language       =
|leader_title   = President
|leader_name    = [[Kaido Reivelt]]
|main_organ     =
|parent_organization =
|affiliations   =
|num_staff      =
|num_volunteers =
|budget         =
|website        = http://www.fyysika.ee/efs/
|remarks        =
}}

The '''Estonian Physical Society''' ('''''Eesti Füüsika Selts''''', EFS) is a voluntary not-for-profit research society bringing together all those active in physics in Estonia. The EFS was established in 1989 and is affiliated with the [[Estonian Academy of Sciences]] (''Eesti Teaduste Akadeemia'') and is a [[European Physical Society]] Member Society.<ref name="aboutus">[http://www.fyysika.ee/efs/eesmark.html Estonian Physical Society: About us]</ref><ref>[http://www.akadeemia.ee/en/academy/history/ Estoanian Academy of Sciences: Facts of History]</ref><ref>http://www.eps.org/directory/directory/national-societies  European Physical Society: Member Societies</ref>

==Presidents of the Estonian Physical Society==

[[Kaido Reivelt]] ([[University of Tartu]]) is the current president (since 2007).<ref>https://www.etis.ee/portaal/isikuCV.aspx?LastNameFirstLetter=R&PersonVID=567&lang=et&FromUrl0=isikud.aspx</ref> 
The first president of the Society was [[Jaak Aaviksoo]],<ref>{{cite news|title=Eesti ja Soome Füüsika Seltsi ühised füüsikapäevad|url=http://www.horisont.ee/node/45|accessdate=17 January 2011|newspaper=[[Horisont]]|year=2007|volume=2}}</ref> followed later by [[Piret Kuusk]] (1998-2001),<ref>http://www.springerlink.com/content/pjwvu9wj1x3n72rn/fulltext.pdf</ref> [[Raivo Jaaniso]] (2001-2004),<ref>https://www.etis.ee/portaal/isikuCV.aspx?LastNameFirstLetter=J&PersonVID=36945&lang=et&FromUrl0=isikud.aspx</ref> and [[Arvo Kikas]] (2004-2007).<ref>https://www.etis.ee/portaal/isikuCV.aspx?LastNameFirstLetter=K&PersonVID=329&lang=et&FromUrl0=isikud.aspx</ref>

== Honorary members ==
* [[Henn Käämbre]]
* [[Karl Rebane]]
* [[Harald Keres]]
* [[Jaan Einasto]]
* [[Piret Kuusk]]

==Awards==
The EFS gives several awards:
* Annual Award:  in recognition of theoretical, experimental, or an area of physics research results. The award is published in the ''Annals of the Estonian Physical Society''; [http://www.fyysika.ee/efs/autasud/aastapreemia.html (List of Estonian Physical Society Annual Award winners)]
:::1992: Enn Realo
:::1993: Rein Kaarli
:::1994: Georg Liidja
:::1995: Vladimir Hiznjakov
:::1996: Mart Elango
:::1997: Arlentin Laisaar
:::1999: Hannes Tammet
:::2000: Nikolai Kristoffel
:::2001: Ants Lõhmus
:::2002: Romi Mankin
:::2003: Viktor Peet
:::2005: Jaan Aarik 
:::2006: Enn Saar 
:::2007: Jaan Kalda
:::2008: Laser Diagnostic Instruments Collective
:::2009: Els Heinsalu
:::2010: Toomas Rõõm
:::2011: Ivo Heinmaa

* A certificate of honor status: for promoting physics in Estonia;
* And two for students for research and for the promotion of Physics.

==Publications==
* ''Annals of the Estonian Physical Society'' ({{issn|1406-0574}})

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.fyysika.ee/efs/}}
* [http://www.fyysika.ee/ Fyysika.ee] – Estonian physics portal created by the EFS
* [http://www.teadusbuss.ee Teadusbuss] – ''Science Bus'', a science popularization project led by the EFS

{{European Physical Society}}

{{DEFAULTSORT:Estonian Physical Society}}
[[Category:Physics societies]]
[[Category:Organizations based in Estonia]]
[[Category:Scientific organizations established in 1989]]
[[Category:1989 establishments in Estonia]]
[[Category:Science and technology in Estonia]]