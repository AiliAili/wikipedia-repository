{|{{Infobox Aircraft Begin
  |name=Pützer Elster
  |image=Puetzer Elster B Kiel2007.jpg
  |caption= '''Pützer Elster B'''  
}}{{Infobox Aircraft Type
  |type=Light recreational aircraft
  |manufacturer=[[Pützer]]
  |designer= [[Alfons Pützer]] 
  |first flight=10 January 1959
  |introduction=
  |retired= 
  |primary user=[[Luftwaffe]]
  |more users= <!-- "More users" field limited to THREE total. -->
  |number built=45
  |status= In civilian use
  |unit cost=
  |developed from = [[Pützer Doppelraab]], [[Pützer Motorraab]]
  |variants with their own articles= 
}}
|}

The '''Pützer Elster''' was a [[Germany|German]] single-engined light [[aircraft]], manufactured by Alfons Pützer KG (later Sportavia) in [[Bonn]]. It served with the [[Luftwaffe]] and [[German Navy|Marineflieger]] and was used solely for recreational sport flying. Some continue to fly in 2007 in private ownership.

==Development history==
The '''Pützer Elster''' "Magpie" was developed from the [[Pützer Motorraab|Motorraab]] [[motor glider]] which had itself been developed from the [[Raab Doppelraab|Doppelraab]]  glider. The Elster was the first aircraft produced in Germany after [[World War II]] in any significant numbers. The design shared the wing of the ''Doppelraab'', braced by metal struts, but was given a new [[monocoque]] [[fuselage]] constructed of [[plywood]] with seats for two occupants arranged side by side. The tricycle [[landing gear]] unusually featured a steerable nosewheel controlled by a hand grip.
Production ceased in 1967, by which time 45 examples had been built.<ref>Green p.32. 1964</ref>

==Variants==

===Elster===
:Prototype aircraft fitted with a 52 hp [[Porsche|Porsche 678/3]] engine, first flight 10 January 1959.

===Elster B===
[[File:Pützer Elster B (Luftwaffe) at Pferdsfeld 1972 (1).jpg|thumb|right|Elster B of Luftwaffe, Pferdsfeld air base 1972]]

:Main production version fitted with a 95 hp [[Continental O-200|Continental C-90]] engine. 25 aircraft were operated by the [[Luftwaffe]] and [[German Navy|Marineflieger]] sport flying groups. These aircraft were initially operated with civilian registrations but were allocated military serials in 1971.<ref>Jackson p.134. 1976</ref> In 1978 the maintenance contract with Pützer expired and the aircraft were placed on the civil market.

===Elster C===
:The Elster C was fitted with the more powerful 150 hp [[Lycoming O-320]] engine and other modifications for use as a glider tug.

==Operators==

===Military operators===
;{{GER}}
*[[Luftwaffe]]
*[[Marineflieger]]

==Specifications (Pützer Elster B)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] --> 
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|ref= ''Macdonald Aircraft Handbook, 1964.''
|crew= One  
|capacity= One passenger
|length main= 7.1 m 
|length alt= 23 ft 3.5 in
|span main= 13.22 m 
|span alt= 43 ft 4 in
|height main= 2.5 m 
|height alt= 8 ft 2.5 in
|area main=  
|area alt= 188 sq ft
|airfoil=
|empty weight main= 470 kg
|empty weight alt= 1,014 lb
|loaded weight main= <!--kg-->
|loaded weight alt= <!--lb-->
|useful load main= <!--kg--> 
|useful load alt= <!--lb--> 
|max takeoff weight main= 700 kg 
|max takeoff weight alt=  1,543 lb
|more general=
|engine (prop)= [[Continental O-200|Continental C-90-12F]]
|type of prop= 4-cylinder piston engine
|number of props=1
|power main= 70 kw
|power alt= 95 hp
|power original= 
 
|max speed main= 185 km/h 
|max speed alt= 104 mph
|cruise speed main= 140 km/h
|cruise speed alt= 86 mph
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 450 km 
|range alt=  270 nm
|ceiling main=  4,000 m 
|ceiling alt= 16,100 ft
|climb rate main=  3 m/s<!--m/s--> 
|climb rate alt=  720 ft/min<!--ft/min--> 
|loading main= <!--kg/m²--> 
|loading alt= <!--lb/ft²--> 
|thrust/weight=<!--a unitless ratio--> 
|power/mass main= <!--W/kg--> 
|power/mass alt= <!--hp/lb--> 
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
*[[Piper PA-22|Piper PA-22 Tripacer]]
|sequence=
|see also=

}}

==References==

===Notes===
{{reflist|2}}

===Bibliography===
{{refbegin}}
* Green, William. ''Aircraft Handbook''. London. Macdonald & Co. (Publishers) Ltd., 1964.
* Jackson, Paul A. ''German Military Aviation 1956-1976''. Hinckley, Leicestershire, UK: Midland Counties Publications, 1976. ISBN 0-904597-03-2.
{{refend}}

==External links==
{{commons category|Pützer Elster}}
*[http://www.airteamimages.com/51774.html Photo of Elster B in Luftwaffe markings]

{{DEFAULTSORT:Putzer Elster}}
[[Category:Pützer aircraft|Elster]]
[[Category:German civil utility aircraft 1950–1959]]
[[Category:German military utility aircraft 1950–1959]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Glider tugs]]