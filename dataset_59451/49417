'''Cecily Mackworth''' (15 August 1911 – 22 July 2006) was a Welsh author and explorer.

== Early life ==
Mackworth was born in [[Gwent (county)|Gwent]] in 1911, to an illustrious and well-connected Welsh coal-owner family. Her great-grandfather Sir Digby Mackworth, an officer in [[Arthur Wellesley, 1st Duke of Wellington|Wellington]]'s army, married Julie de Richepense, the daughter of one of [[Napoleon]]'s generals. Her aunt Margaret, Viscountess de Rhondda, was an editor of the newspaper ''Time and Tide''. Her father, Francis, was killed early in [[World War I]].

Mackworth moved to [[Sidmouth]] after her mother remarried, and she subsequently studied at the [[London School of Economics]], where her aunt Margaret was also a governor. She did not graduate, absconding after two years with a Hungarian classmate, the economist [[Nicholas Kaldor]].<ref name="The Guardian">{{cite news|last1=Sheridan|first1=Anthony|title=Cecily Mackworth|url=https://www.theguardian.com/news/2006/aug/07/guardianobituaries.booksobituaries|work=The Guardian|date=7 August 2006}}</ref>

== Life and work ==
After leaving LSE, Mackworth spent much of the next two decades traveling. She married Leon Donckier de Donceel, a Belgian lawyer, at the age of 22 after meeting him in a Swiss sanitarium. The couple had one daughter before he died three years into their marriage.<ref name="The Guardian" /> She spent time in Hungary and Germany, witnessing the [[Reichstag fire|burning of the Reichstag]] in 1933, before settling in Paris in 1936.<ref name="The Guardian" /> Forced to flea Paris in 1940, she worked for the [[Free French]] during the war, in addition to giving lectures to the army and writing for [[Cyril Connolly]]'s literary magazine [[Horizon (magazine)|Horizon]].<ref name="The Guardian" /> While in London she became familiar with a number of contemporary writers, including [[T.S. Eliot]], who admired an early book of hers detailing her recent flight from France.<ref name="The Independent">{{cite news|last1=Bowker|first1=Gordon|title=Cecily Mackworth|url=http://www.independent.co.uk/news/obituaries/cecily-mackworth-410094.html|work=The Independent|date=31 July 2006}}</ref> In one notable incident during this time, [[Dylan Thomas]]' wife, Caitlin, stubbed a cigarette out on Mackworth's hand, on the grounds that Mackworth was acting too intimately with her husband at a party.<ref name="The Independent" />

She returned to Paris after the war, but traveled widely, including to Palestine in 1948, and to Algeria in 1950, following the path of [[Isabelle Eberhardt]]. In 1956 she married again, to the Marquis de Chaballes La Palice. The two were married until his death in 1980.<ref name="The Independent" />

Mackworth's first book, ''Eleven Poems'', was published by [[Henry Miller]]<ref name="The Independent" /> in 1938. Her first major success detailed her escape from France in front of the Nazi advance, and route through Spain and Portugal to London, ''I Came out of France'' (1941). After the war she wrote studies on the poets [[Francois Villon]] (in 1947) and [[Guillaume Apollinaire]] (in 1967). She was one of two female journalists working in Palestine at the time of the birth of Israel, and published a book about the experience, ''The Mouth of the Sword'', in 1949. Her book about Isabelle Eberhardt, based on her travels in the region Eberhardt had lived earlier, ''The Destiny of Isabelle Eberhardt'', was published in 1954. She also published two novels, ''Spring's Green Shadow'' (1952) and ''Lucy's Nose'' (1992), as well as two volumes of autobiography, ''Ends of the World'' (1987)<ref name="The Independent" /> and ''Out of Black Mountains'', the latter completed weeks before her death in 2006.<ref name="The Independent" />

== References ==
{{reflist}}

{{DEFAULTSORT:Mackworth, Cecily}}
[[Category:Welsh writers]]
[[Category:1911 births]]
[[Category:2006 deaths]]