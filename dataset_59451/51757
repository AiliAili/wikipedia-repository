{{Infobox journal
| cover = [[File:Journal of Bisexuality.jpg]]
| editor = [[M. Paz Galupo]]
| discipline = [[Sexology]], [[queer studies]]
| abbreviation =
| publisher= [[Routledge]]
| country =
| frequency = Quarterly
| history = 2000–present
  Managing Editor = Regina Reinhardt 2000-2015
| website = http://www.informaworld.com/smpp/title~content=t792306887~db=all
| ISSN = 1529-9716
| eISSN = 1529-9724
}}
The '''''Journal of Bisexuality''''' is a [[peer review|peer-reviewed]] [[academic journal]] published quarterly by the [[Taylor & Francis|Taylor & Francis Group]] under the [[Routledge]] imprint. It is the official journal of the [[American Institute of Bisexuality]]. It covers a wide range of topics on [[bisexuality]] including new bisexuality research, bisexual issues in therapy, differences from the straight, lesbian and gay communities, growth of the bisexual movement, bisexuality and the media, bisexual history, and different bisexual lifestyles.

In addition, the journal also publishes book and movie reviews covering bisexual lead characters from every era. Special thematic issues cover topics singularly; such as women and bisexuality — a global perspective, bisexual women in the 21st century, bisexual men in culture and society, and bisexuality in the lives of men.

The journal was established in 1999. Its first [[editor-in-chief]] was [[Fritz Klein (sex researcher)|Fritz Klein]], followed by Jonathan Alexander, Brian Zamboni, and [[James D. Weinrich]]. In 2014, M. Paz Galupo became its first female editor-in-chief.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Academic Search Premier]], [[PsycINFO]], [[ProQuest]], [[GLBT Life]], [[Contemporary Women's Issues]], [[LGBT Life]], and [[Sociological Abstracts]].<ref>{{cite web | url=http://www.informaworld.com/smpp/title~mode=abstracting_a_indexing~tab=summary?content=t792306887 | title=About this Journal: Abstracting & Indexing | publisher=Routledge | work=Journal of Bisexuality | accessdate=2011-01-02 | quote = Abstracted and/or indexed in: Academic Search Premier (EBSCO); Psychological Abstracts (PsycINFO); ProQuest CSA; GLBT Life (EBSCO); Google Scholar; TOC Premier; Contemporary Women's Issues, LGBT Life, Sociological Abstracts. Social Services Abstracts.}}</ref>

== See also ==
* ''[[Journal of Homosexuality]]''
* [[List of sexology journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.informaworld.com/smpp/title~content=t792306887~db=all}}

{{Bisexuality topics}}

{{DEFAULTSORT:Journal Of Bisexuality}}
[[Category:Bisexuality-related literature]]
[[Category:LGBT-related media in the United States]]
[[Category:Publications established in 1999]]
[[Category:Quarterly journals]]
[[Category:Sexology journals]]
[[Category:Sexual orientation and science]]
[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:LGBT-related journals]]