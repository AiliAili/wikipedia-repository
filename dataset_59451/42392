<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name= Boeing Business Jet
  |image= Boeing's commercial aircraft in BBJ livery.jpg
  |caption= Artist's impression of the entire BBJ family
}}{{Infobox aircraft type
  |type= [[Business jet]]
  |manufacturer= [[Boeing Commercial Airplanes]]
  |designer=
  |first flight= September 4, 1998
  |introduced= 1999
  |retired=
  |status=
  |primary user=
  |more users= 
  |produced= 1998–present
  |number built= 217 (as of 31 October 2016) (including BBJ1, BBJ2, BBJ3, 747BBJ, 757BBJ, 767BBJ, 777BBJ & 787BBJ)<ref name="boeing.com">http://www.boeing.com/commercial/bbj/#/orders-and-deliveries</ref>
  |unit cost =[[United States dollar|US$]]80 million for 737 BBJ variants (2012)<ref name="dailymail.co.uk">http://www.dailymail.co.uk/news/article-2121800/Inside-80-million-customized-Boeing-business-jet.html</ref>
  |developed from= 
  |variants with their own articles= 
}}
|}

The '''Boeing Business Jet''' series are variants of [[Boeing Commercial Airplanes|Boeing]] [[jet airliner]]s for the [[corporate jet]] market. The Boeing Business Jet is a 50/50 partnership between Boeing Commercial Airplanes and [[GE Aviation]].

The '''BBJ''' designation denotes the business jets based upon the [[Boeing 737|737]] series airliners. These aircraft usually seat between 25 and 50 passengers within a luxurious configuration. This may include a master bedroom, a washroom with showers, a conference/dining area, and a living area. Boeing Business Jets also has corporate jet configurations based on the [[Boeing 777]], [[Boeing 787]] and the [[Boeing 747-8]] Intercontinental, which are known as '''777 VIP''', '''787 VIP''', and '''747-8 VIP''', respectively.

==Boeing BBJ (737-based)==
[[File:RAAFBBJA36001.JPG|thumb|A [[Royal Australian Air Force]] Boeing 737-700 BBJ at [[Sydney Airport]]]]

The Boeing BBJ is primarily a 737 commercial airframe with modifications to provide for private jet service. The BBJ1 is based on a [[Boeing 737 Next Generation|737-700]] airframe, with elements from the 737-800. The BBJ2 and BBJ3 are based on the [[Boeing 737 Next Generation|737-800]] and [[Boeing 737 Next Generation|737-900ER]] series, respectively.

All 11<ref name=ain2015-05-16>{{cite news |first=Kerry |last=Lynch |url=http://www.ainonline.com/aviation-news/business-aviation/2015-05-16/boeing-business-jets-confident-it-studies-combi |title=Boeing Business Jets confident as it studies Combi |work=Aviation International News |date=16 May 2015 |accessdate=18 May 2015 }}</ref> models include changes to the airframe regardless of the BBJ series. [[Fokker Technologies#Business units|Fokker Services]] are developing 1.5–meter wide windows for the BBJ in 2016.<ref>http://www.ainonline.com/aviation-news/business-aviation/2015-05-20/first-skyview-bbj</ref><ref>[http://www.flightglobal.com/news/articles/ebace-fokker-proposes-giant-window-to-boeing-business-399588/ Fokker proposes giant window to Boeing Business Jets]" [https://web.archive.org/web/20150413005250/http://www.flightglobal.com/news/articles/ebace-fokker-proposes-giant-window-to-boeing-business-399588/ Archive]<!--strange date--></ref>

Changes from the normal 737 include:
* [[Wingtip device|Blended winglets]] for additional fuel economy (3–5% improvement) as standard (winglets are optional on airliner 737s)
* Self-contained [[airstair]]s for disembarking at airports with limited ground support
* Additional fuel tanks, for intercontinental range
* [[ETOPS|ETOPS-180]] certification

After the launch of the BBJ, [[Airbus]] followed suit with the launch of the [[Airbus Executive and Private Aviation|Airbus ACJ]] derived from its [[Airbus A319|A319]] commercial airframe. It has also launched the larger A320 and the smaller [[Airbus A318|A318 Elite]]. Other competitors at the smaller end of the market include the [[Embraer Lineage]], the [[Bombardier Global Express]], the [[Gulfstream G500/G550|Gulfstream G550]] and the [[Gulfstream G650]]. A BBJ may cost around {{usd|10}}/mile to operate, whereas the G650 and similar may cost $5–6.<ref>[http://aviationweek.com/site-files/aviationweek.com/files/gallery_images/BCA_2015_Operations_Planning_Guide_6b.jpg?1440787936 Table] [http://aviationweek.com/bca/mission-costs-ultra-long-range-jets# Mission Costs for Ultra-Long-Range Jets]</ref><!--compare to speedprice for other vehicles-->

==Models==

===Narrow-body models===
* '''BBJ''', or less frequently '''BBJ1''', is based on the 737-700, and formed the basis for the 737-700ER. This was the initial variant. In [[United States Air Force]] service, this is known as the [[Boeing C-40 Clipper|C-40B Clipper]].
* '''BBJ2''' is based on the 737-800.
* '''BBJ3''' is based on the 737-900ER.
* '''BBJ C''' is a variant of the BBJ featuring the "quick change" capabilities of the 737-700C. This allows the aircraft to be used for executive duty during one flight, and to be quickly reconfigured for cargo duty for the next flight.
* '''BBJ MAX 8''' and '''BBJ MAX 9''' are proposed variants of the [[Boeing 737 MAX]] 8 and 9 with new [[CFM LEAP]]-1B engines and advanced winglets providing 13% better fuel burn; the BBJ MAX 8 will have a {{nowrap|{{convert|6,325|nmi|mi km}}}} range and the BBJ MAX 9 a {{nowrap|{{convert|6,255|nmi|mi km}}}} range.<ref>{{cite press release |url= http://boeing.mediaroom.com/2012-10-29-Boeing-Business-Jets-to-Offer-the-BBJ-MAX |title= Boeing Business Jets to Offer the BBJ MAX |publisher= Boeing |date= October 29, 2012 |access-date=October 30, 2012}}</ref> The BBJ MAX 7 was unveiled in October 2016 and will have a {{nowrap|{{convert|7,000|nmi|mi km}}}} range, with 10% lower operating costs than the original BBJ while having a longer cabin and more under-floor baggage space.<ref>{{cite press release |url= http://boeing.mediaroom.com/news-releases-statements?item=129797 |title= Boeing Business Jets Unveils BBJ MAX 7 |date= October 31, 2016 |publisher= Boeing |access-date=November 1, 2016}}</ref>

===Wide-body models===
[[File:2014-04-05 Boeing 777-200LR VIP (VP-CAL) at LHR.jpg|thumb|[[Boeing 777#777-200LR|Boeing 777-200LR]] BBJ. A Boeing 777 ''Worldliner'' in VIP configuration]]

*'''747 VIP''': version of the 747-8 ordered by the Boeing Business Jet division. There are currently seven orders for this aircraft. The VIP 747 is delivered by BBJ in a "green" condition, meaning there are no interior furnishings so that the owner can design it to personal preference. This plane has a range of 9,260&nbsp;nmi (17,150&nbsp;km).
*'''777 VIP''': version of the 777 ordered by the Boeing Business Jet division. It is a modified version of the 777-200LR, and [[Boeing 777X]] with a range of 10,100&nbsp;[[nautical mile|nmi]] (18,700&nbsp;km). 
*'''787 VIP''': version of the 787-8/-9 ordered by the Boeing Business Jet division. There are 15 orders for this aircraft. As with the 747, the VIP 787 is delivered by BBJ in a "green" condition. The −8 has a range of 9,590&nbsp;nmi (17,760&nbsp;km) and the −9 has a range of 9,950&nbsp;nmi (18,430&nbsp;km).

===Orders and deliveries===

{| class="wikitable sortable"
|-
! Breakdown by aircraft !! 737 !! BBJ !! MAX !! 757 !! 767 !! 777 !! 787 !! 747-400 !! 747-8 !! '''Total'''
|-
| '''Orders''' || 15 || 163 || 14 || 5 || 8 || 9 || 16 || 3 || 8 || '''241'''
|-
| '''Deliveries''' || 15 || 163 || 0 || 5 || 8 || 8 || 9 || 3 || 8 || '''219'''
|-
| '''In Service''' || 14 || 156 || 0 || 5 || 8 || 5 || 3 || 3 || 6 || '''200'''
|}

Data through January 30, 2017<ref name="boeing.com"/>

==Operators==
[[File:royaljet b737-bbj a6-rjz arp.jpg|thumb|right|Boeing 737-700/BBJ of the [[Abu Dhabi]] airline [[Royal Jet]] ]]
[[File:Kazakhstan Government Boeing 737-7EJ BBJ Wallner-1.jpg|thumb|right|Boeing 737-700/BBJ of the [[Armed Forces of the Republic of Kazakhstan]] ]]
[[File:State of Kuwait Boeing 737-9BQ(ER) (Boeing Business Jet 3) 9K-GCC.jpg|thumb|Boeing 737-900(ER)/BBJ3 of the [[Kuwait|State of Kuwait]] ]]

'''State VIP users'''
;{{flag|Niger}}: [[Niger Air Force]] (1) for government VIP flight
; {{flag|Australia}}: [[Royal Australian Air Force]] (2) leased BBJ737
::* [[No. 34 Squadron RAAF]]
; {{flag|Belarus}}: [[Belarus Air Force]] (1){{Citation needed|date=December 2012}} BBJ2 for Government VIP flight<ref name="aerospace-technology">http://www.aerospace-technology.com/projects/bbj/{{Unreliable source?|reason=domain on WP:BLACKLIST|date=June 2016}}</ref>
; {{flag|Colombia}}: [[Colombian Air Force]] (1)
; {{flag|India}}: [[Indian Air Force]] (3)
; {{flag|Indonesia}}: [[Indonesian Presidential Aircraft|Presidency]] (1)  BBJ2 for Government VVIP flight.<ref>{{cite news|title =RI ‘Air Force One’ will not be armed  | date = 14 April 2014 | author = tjs |newspaper = The Jakarta Post |url = http://www.thejakartapost.com/news/2014/04/14/ri-air-force-one-will-not-be-armed.html | accessdate = 17 January 2015}}</ref>
; {{flag|Kazakhstan}}: [[Government of Kazakhstan]] (1)
; {{flag|Kuwait}}: [[Government of Kuwait]] (2)
; {{flag|Madagascar}}: Presidency (1)
; {{flag|Malaysia}}: [[Royal Malaysian Air Force]] (1)
; {{flag|Morocco}}: [[Royal Moroccan Air Force]] (2)
; {{flag|Nigeria}}: [[Nigerian Air Force]] (1)
; {{flag|Netherlands}}: [[Politics of the Netherlands | Government of the Kingdom of the Netherlands]] beginning in 2019 (1)<ref>[https://www.flightglobal.com/news/articles/dutch-to-replace-royal-transport-with-737-bbj-436119/ Dutch to replace Royal transport with 737 BBJ.]</ref>
; {{flag|Poland}}: [[Polish Air Force]] for government VIP flight, beginning in 2020 (2 BBJ2)<ref>http://ch-aviation.com/portal/news/54827-polish-govt-orders-three-vip-configured-b737nextgens</ref>
; {{flag|Qatar}}: [[Qatar Amiri Flight]] (1)
; {{flag|South Africa}}: [[South African Air Force]] (1)
; {{flag|Tunisia}}: [[Tunisia|Republic of Tunisia Government]] (1)
; {{flag|United Arab Emirates}}: [[Abu Dhabi Amiri Flight]] (9), [[Royal Jet]] (6) BBJ1 for Government VIP flight<ref name="aerospace-technology" />

==Specifications==
The specifications for the various members of the BBJ family are as follows:<ref name="747-8_specs">[http://www.boeing.com/commercial/747family/747-8_fact_sheet.html Boeing 747-8 Technical Specifications]. Boeing. Retrieved November 11, 2007.</ref><ref name=747-8_airport>[http://www.boeing.com/assets/pdf/commercial/airports/acaps/747_8.pdf 747-8 Airport Compatibility Report]. Boeing, December 2011.</ref><ref>[http://www.boeing.com/commercial/airports/acaps/7478brochure.pdf 747-8 Airport Compatibility brochure] {{webarchive |url=http://www.webcitation.org/5inLN3DMp?url=http://www.boeing.com/commercial/airports/acaps/7478brochure.pdf |date=2009-08-05 }}. Boeing, January 2008.</ref><ref name=787_Airport_report>{{cite web |url=http://www.boeing.com/assets/pdf/commercial/airports/acaps/787.pdf |title= 787 Airplane Characteristics for Airport Planning |publisher= Boeing Commercial Aircraft |date= November 2014 |format=PDF}}</ref><ref>http://www.boeing.com/assets/pdf/commercial/airports/acaps/737sec2.pdf</ref><ref>[http://www.boeing.com/commercial/737family/specs.html Boeing 737 Technical Information], Boeing Commercial Airplanes.</ref><ref name=Boeing_airp_report>[http://www.boeing.com/commercial/airports/737.htm Boeing 737 Airplane Characteristics for Airport Planning], Boeing Commercial Airplanes.</ref><ref>[http://www.boeing.com/commercial/bbj/#/aircraft/bbj-777/characteristics/777-200lr/ "BBJ 777-200LR: Specifications"]. Boeing. Retrieved 9 July 2016.</ref><ref>[http://www.boeing.com/commercial/bbj/#/aircraft/bbj-777/characteristics/777-200lr/ "BBJ-777-LR"]. Boeing. Retrieved 9 July 2016.</ref><ref>[http://www.boeing.com/commercial/bbj/#/aircraft/bbj-737max/characteristics/bbj-max-8/ Boeing 737 MAX 8 specifications] retrieved 16 July 2016</ref>

{| class=wikitable style="text-align: center;"
|-
!Measurement
!BBJ1
!BBJ MAX 8
!747 VIP
!777 VIP
!787 VIP
|-
! Crew
|colspan=3| 4 
|colspan=2| (Unknown) 
|-
! Length 
| 33.63 m (110&nbsp;ft 4 in) || 39.5 m (129&nbsp;ft 8 in) || 76.4 m (250&nbsp;ft 8 in) || {{convert|209|ft|1|in|m}} || 62.8&nbsp;m (206&nbsp;ft)
|-
! Wingspan 
| 35.79 m (117&nbsp;ft 5 in) || 35.92 m (117&nbsp; ft 10 in) || 68.4 m (224&nbsp;ft 7 in) || {{convert|212|ft|7|in|m}} || 60.1 m (197&nbsp;ft 3 in)
|-
! Wing area 
| 1,341 sq ft || 1,373 sq ft || 5,960 sq ft || 5,520 sq ft || 3,880 sq ft
|-
! Height 
| 12.57 m (41&nbsp;ft 3 in) || 12.3 m (40&nbsp;ft 4 in) || 19.6 m (64&nbsp;ft 4 in) || {{convert|61|ft|1|in|m|disp=flip}} || 16.9 m (55&nbsp;ft 6 in)
|-
! Empty weight 
| 94,570&nbsp;lb (42,895&nbsp;kg) - {{convert|98500|lb|kg|abbr=on}} || {{convert|141700|lb|kg|abbr=on}} || {{convert|472900|lb|kg|abbr=on}} || {{convert|352100|lb|kg|abbr=on}} || {{convert|304,000|lb|kg|abbr=on}}
|-
! Maximum fuel capacity 
| {{convert|10,707|usgal|L|0}} || {{convert|10420|usgal|L|0}} || {{convert|63,034|usgal|L|0}} || {{convert|47890|usgal|L|0}} || {{convert|33,384|usgal|L|0}}
|-
! Maximum take-off weight 
| {{convert|171000|lb|kg|0}} || {{convert|181200|lb|kg|0}} || {{convert|987000|lb|kg|0}} || {{convert|766000|lb|kg|0}} || {{convert|557000|lb|kg|0}}
|-
! Maximum landing weight
| {{convert|134000|lb|kg|0}} || {{convert|152800|lb|kg|0}} || {{convert|688000|lb|kg|0}} || {{convert|492000|lb|kg|0}} || {{convert|425000|lb|kg|0}}
|-
! Cruising speed
| Mach 0.78 || Mach 0.79 || Mach 0.85 || Mach 0.84 || Mach 0.85
|-
! Maximum speed 
| 890&nbsp;km/h (481 [[knot (unit)|kn]], Mach 0.82) || Mach 0.84 || Mach 0.92 || Mach 0.89 || Mach 0.95 (323&nbsp;m/s)
|-
! Required [[runway]] at [[MTOW]] 
| {{convert|5249|ft|m|abbr=on}} || (unknown) || {{convert|10300|ft|m|abbr=on}} || {{convert|9200|ft|m|abbr=on}} || {{convert|9400|ft|m|abbr=on}}
|-
! Range 
| 6200&nbsp;nmi (11,480&nbsp;km) <br/>8 passengers || {{convert|6555|nmi|km|abbr=on}} <br/>8 passengers|| {{convert|9260|nmi|km|abbr=on}} || {{convert|10030|nmi|km|abbr=on}} || {{convert|9950|nmi|km|abbr=on}}
|-
! Service Ceiling 
| {{convert|41000|ft|m|abbr=on}} || (unknown) || {{convert|45100|ft|m|abbr=on}} || {{convert|43100|ft|m|abbr=on}} || {{convert|43100|ft|abbr=on}}
|-
! Wing sweep 
| 25.02° || 27.5° || 37.5° || 33.5° || 32.2°
|-
! Powerplants 
| Two [[CFM-56|CFM International CFM56-7]] || Two [[CFM International LEAP|CFM International LEAP-1B]] || Four [[General Electric GEnx]] || Two [[General Electric GE90|General Electric GE90-115B1]] || Two [[Rolls-Royce Trent 1000]]
|-
! Maximum thrust
| 11,740&nbsp;kg (26,400 [[pound-force|lb<sub>f</sub>]]) each || 12,500&nbsp;kg (28,000 [[pound-force|lb<sub>f</sub>]]) each || 32,680&nbsp;kg (72,300 [[pound-force|lb<sub>f</sub>]]) each || 52,300&nbsp;kg (115,300 [[pound-force|lb<sub>f</sub>]]) each ||32,540&nbsp;kg (72,000 [[pound-force|lb<sub>f</sub>]]) each
|}

==See also==
{{Aircontent
|related=
* [[Boeing C-40 Clipper]]
|similar aircraft=
* [[Airbus Corporate Jets]]
|lists=
* [[Air transports of heads of state and government]]
* [[List of active United States military aircraft]]
* [[List of civil aircraft]]
* [[List of military aircraft of the United States]]
}}

== References ==
{{reflist|35em}}

==External links==
{{Commons category|Boeing Business Jet}}
* [http://www.boeing.com/commercial/bbj/ Official site]

{{Boeing airliners}}

[[Category:Boeing aircraft|Business Jet]]
[[Category:United States business aircraft]]
[[Category:Boeing 737]]
[[Category:Boeing 747]]
[[Category:Boeing 777]]
[[Category:Boeing 787 Dreamliner]]