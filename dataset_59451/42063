<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Gloster III <!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Gloster IIIA -001a.jpg
  |caption = Gloster IIIA at 1925 Schneider Trophy 
}}{{Infobox Aircraft Type
  |type = Racing seaplane
  |national origin=United Kingdom
  |manufacturer = [[Gloster Aircraft Company]]
  |designer = [[Henry Folland]]
  |first flight = 29 August 1925
  |introduced = 1925<!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Air Force]]<!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 2
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}
The '''Gloster III''' was a [[United Kingdom|British]] racing [[floatplane]] of the 1920s intended to compete for the [[Schneider Trophy]] air race.  A single-engined, single-seat [[biplane]], two were built, with one finishing second in the 1925 race.

==Design and development==
In 1924, the [[Gloster Aircraft Company]] designed and built the [[Gloster II]], a development of the [[Nieuport Nighthawk|Gloster I]] racing aircraft to compete in that year's [[Schneider Trophy]] air race.  The first aircraft was written off during trials, however, and as there were no other competitors, the American Aero Club postponed the competition to 1925.<ref name="James p107-9">James 1971, pp.107-109.</ref>

In order to compete in the 1925 race, the British [[Air Ministry]] placed an order with Gloster for the design and build of two examples of a new racing seaplane in February 1925.{{ref label|Note1|a|a}} The resulting design, the Gloster III, like the Gloster II, was a wooden [[biplane]] with single bay wings, powered by  a 700&nbsp;hp (522&nbsp;kW) [[Napier Lion]] VII engine.  The aircraft was fitted with Lamblin radiators on the leading edge of the lower wings.<ref name="donald world p454">Donald 1997, p.454.</ref>  With a 20&nbsp;ft (6.1 m) wingspan, the Gloster was the smallest British aircraft ever built with that power at that time.<ref name="James p124">James 1971, p.124.</ref>

The first prototype, with the serial number ''N194'' was flown by [[Hubert Broad]] on 29 August 1925, with the second aircraft (with the civil registration ''G-EBLJ'' and the military serial ''N195'') being flown a few days later by [[Bert Hinkler]].<ref name="James p125">James 1971, p.125.</ref>

==Operational history==
The pilots had little time to practice flying the Gloster IIIs, with ''N194'' only flying four times and ''N195'' flying once before departing for America.<ref name="James p125 127">James 1971, pp. 125, 127.</ref> When the Supermarine S.4 crashed during navigation trials on 23 October 1925, ''N195'', which was brought as a reserve was prepared to take part in the race instead of the Supermarine monoplane, to be flown by Hinkler.<ref name="James p127">James 1971, p.127.</ref> On the morning of the race, however, ''N195'' was damaged during taxiing tests, leaving Broad in N194 to carry British hopes in the afternoon's race.

When the race took place, the Gloster III was outclassed by the [[Curtiss R3C]]s of the United States, with the race being won by [[Lieutenant]] [[Jimmy Doolittle|James Doolittle]], flying a [[Curtiss R3C]] at an average speed of 232.573&nbsp;mph (374.443&nbsp;km/h),<ref name="flightp703">Flight 29 October 1925, p.703.</ref> {{convert|33|mi/h|km/h|abbr=on}} faster than Broad, who recorded a speed of 199.091&nbsp;mph (320.537&nbsp;km/h),<ref name="flight p752">Flight 12 November 1925, p.752.</ref> with De Briganti in the [[Macchi M.33]] finishing third and the remaining two Curtiss R3Cs failing to finish.<ref name="flight p752"/>

Following the race, the two Gloster IIIs were returned to the United Kingdom.  After modification, they were used for training pilots of the [[RAF]]s [[High Speed Flight RAF|High Speed Flight]] in preparation for the 1927 race.<ref name="James p129"/>

==Variants==
;Gloster III
:Original variant, powered by 700&nbsp;hp (522&nbsp;kW) Napier Lion.  Two built.
;Gloster IIIA
:Modified tail to resolve stability problems encountered on first flight.<ref name="James p125">James 1971, p.125.</ref>
;Gloster IIIB
:Modification of N195 following return of aircraft from 1925 Schneider Trophy.Low drag surface radiators fitted to all four wings and modified cantilevered tail.  Maximum speed increased to {{convert|252|mi/h|km/h|abbr=on}}.<ref name="James 128-9">James 1971, pp.128-129.</ref>

==Operators==
;{{UK}}
*[[Royal Air Force]]
**[[High Speed Flight RAF]]

==Specifications (Gloster IIIA)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=Gloster Aircraft since 1917 <ref name="James p129">James 1971, p.129</ref> 
|crew=one
|capacity=
|length main= 26 ft 10 in
|length alt=8.1 m
|span main= 20 ft 0 in
|span alt=  6.09 m
|height main= 9 ft 8 in
|height alt= 2.94 m
|area main= 152 ft²
|area alt= 14.1 m²
|airfoil=
|empty weight main= 2,028 lb
|empty weight alt= 920 kg
|loaded weight main= 2,687 lb
|loaded weight alt= 1,218 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Napier Lion]] VII
|type of prop=12-cylinder water cooled [[W engine|W block]] engine
|number of props=1
|power main= 700 hp
|power alt=522 kW
|power original=
   
|max speed main= 225 mph 
|max speed alt= 196 knots, 362 km/h
|cruise speed main= <!-- knots -->
|cruise speed alt= <!-- mph,  km/h -->
|never exceed speed main= <!-- knots -->
|never exceed speed alt= <!-- mph,  km/h -->
|stall speed main= <!-- knots -->
|stall speed alt= <!-- mph,  km/h -->
|range main= <!-- nm -->
|range alt= <!-- mi,  km -->
|ceiling main= <!-- ft -->
|ceiling alt= <!-- m -->
|climb rate main= <!-- ft/min -->
|climb rate alt= <!-- m/s -->
|loading main= 17.7 lb/ft²
|loading alt= 86.4 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.26 hp/lb
|power/mass alt= 430 W/kg
|more performance=
|armament=
|avionics=

}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Gloster II]]
*[[Gloster IV]]
|similar aircraft=*[[Supermarine S.4]]
*[[Curtiss R3C]]<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=*[[Schneider Trophy]]<!-- other relevant information -->

}}

==References==
{{commons category|Gloster III}}

===Notes===
*{{note label|Note1|a|a}}In addition, the Air Ministry also placed an order with [[Supermarine]] for the [[Supermarine S.4|S.4]] monoplane.<ref name="Flight p612">Flight 24 September 1925, p.612.</ref>
{{Reflist}}

===Bibliography===
{{refbegin}}
*"[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200609.html The Schneider Cup Seaplane Race: British Representatives Leave on Saturday ]". ''[[Flight International|Flight]]''. 24 September 1925. pp.&nbsp;609–614.
* "[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200703.html The 1925 Schneider Trophy Race]". ''[[Flight International|Flight]]''. 29 October 1925. p.&nbsp;703.
* "[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200726.html The 1925 Schneider Trophy Race:Average Speed Raised from 45.75 m.p.h. in 1913 to 232.573 m.p.h. in 1925]". ''[[Flight International|Flight]]''. 5 November 1925. pp.&nbsp;726–732.
*"[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200747.html The 1925 Schneider Trophy Race:"Flight" Correspondent's Special Account]". ''[[Flight International|Flight]]''. 12 November 1925. pp.&nbsp;747–752.
*"[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200063.html THE SCHNEIDER CUP RACE, 1925].
*Donald, David (editor). ''The Encyclopedia of World Aircraft''. Leicester, UK: Blitz, 1997. ISBN 1-85605-375-X.
*Jackson, A.J. ''British Civil Aircraft since 1919: Volume 2''. London:Putnam, Second edition 1973. Pages 454-455. ISBN 0-370-10010-7.
*James, Derek M. ''Gloster Aircraft since 1917''. London:Putnam, 1971. ISBN 0-370-00084-6.
{{refend}}

<!-- ==External links== -->

{{Gloster aircraft}}

[[Category:British sport aircraft 1920–1929]]
[[Category:Schneider Trophy]]
[[Category:Floatplanes]]
[[Category:Gloster aircraft|III]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]