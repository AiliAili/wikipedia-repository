{{Infobox journal
| title = Journal of Disability Policy Studies
| cover = [[File:Journal of Disability Policy Studies Journal Front Cover.jpg]]
| editor = Mitchell Yell, Antonis Katsiyannis
| discipline = [[Disability studies]]
| former_names = 
| abbreviation = J. Disabil. Policy Stud.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1990-present
| openaccess = 
| license = 
| impact = 1.095
| impact-year = 2013
| website = http://dps.sagepub.com/
| link1 = http://dps.sagepub.com/content/current
| link1-name = Online access
| link2 = http://dps.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1044-2073
| eISSN =
| OCLC = 19730450
| LCCN = 90641127
}}
The '''''Journal of Disability Policy Studies''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[disability studies]], including issues in [[ethics]], [[public policy]], and the law related to individuals with [[disability|disabilities]]. The [[editors-in-chief]] are Mitchell Yell ([[University of South Carolina]]) and Antonis Katsiyannis ([[Clemson University]]). It was established in 1990 and is currently published by [[SAGE Publications]] in association with the [[Hammill Institute on Disabilities]].

== Abstracting and indexing ==
The ''Journal of Disability Policy Studies'' is abstracted and indexed in:
* [[CINAHL]]
* [[Contents Pages in Education]]
* [[EBSCO Nursing and Allied Health Collection]]
* [[Educational Research Abstracts Online]]
* [[Elsevier BIOBASE]]
* [[EMCare]]
* [[PAIS International]]
* [[PsycINFO]]
* [[Scopus]]
* [[Sociological Abstracts]]

According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.095, ranking it 33 out of 69 journals in the category "Rehabilitation".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Education & Educational Research|title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://dps.sagepub.com/}}
* [http://www.hammill-institute.org/ Hammill Institute official website]

[[Category:SAGE Publications academic journals]]
[[Category:Disability studies]]
[[Category:English-language journals]]
[[Category:Sociology journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1990]]
[[Category:Disability publications]]

{{Social-science-journal-stub}}