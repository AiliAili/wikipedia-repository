<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Hü 17
 | image=TMW - Hütter H 17 b 1.jpg
 | caption=Hü 17B in the [[Technisches Museum Wien]]
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[Germany]]
 | manufacturer=
 | designer=[[Ulrich Hütter]] and [[Wolfgang Hütter]]
 | first flight=
 | introduced=
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built="several hundred"<ref name="SoaringNov83" />
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Hütter Hü 17''', is a [[Germany|German]] [[high-wing]], [[strut-braced]], single-seat, utility training [[Glider (sailplane)|glider]] that was designed by brothers [[Ulrich Hütter]] and [[Wolfgang Hütter]] in the 1930s.<ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 86, [[Soaring Society of America]] November 1983. USPS 499-920</ref><ref name="SD">{{Cite web|url=http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=124 |title=Goppingen 5 Hutter 17 |accessdate=13 July 2011 |last=Activate Media |authorlink= |year=2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20120322204252/http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=124 |archivedate=22 March 2012 |df= }}</ref>

The aircraft's correct designation is unclear and various sources refer to is as the Hütter Hü 17, Hütter-17, Hütter H-17, Hutter H-17, Hütter Hü-17, Göppingen Gö 5 and Goppingen 5.<ref name="SoaringNov83" /><ref name="SD" /><ref name="NSMCollection">{{cite web|url=http://www.soaringmuseum.org/collection.html |title=Sailplanes in Our Collection |accessdate=1 July 2011 |last=[[National Soaring Museum]] |authorlink= |year=2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110516142717/http://www.soaringmuseum.org/collection.html |archivedate=May 16, 2011 }}</ref><ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=HUTTER+H-17&PageNo=1|title = Make / Model Inquiry Results|accessdate = 13 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref>

==Design and development==
The Hütter brothers designed the Hü 17 in [[Salzburg, Austria]], the designation indicating the aircraft's [[glide ratio]]. The design was made available as plans for [[Homebuilt aircraft|amateur construction]] and several hundred were completed. The brothers then joined the [[Schempp-Hirth]] company which constructed about five of the aircraft under the designation Göppingen Gö 5.<ref name="SoaringNov83" /><ref name="SD" />

The aircraft is of wooden construction, using a D-tube wing with a single strut and doped [[aircraft fabric covering]]. The wing employs a Göppingen 535 [[airfoil]] at the [[wing root]] and a [[NACA]] M-6 at the [[wing tip]]. The [[fuselage]] is [[plywood]] covered.<ref name="SoaringNov83" /><ref name="SD" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 1 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

==Operational history==
In the 1980s a number of aircraft were still flying in [[Australia]], [[West Germany]], the [[United Kingdom]] and the [[United States]].<ref name="SoaringNov83" /> In July 2011 one example was registered with the American [[Federal Aviation Administration]] in the ''Experimental - Exhibition'' category, having been constructed in 1990.<ref name="FAAReg2">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=17HU|title = Make / Model Inquiry Results N17HU|accessdate = 14 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref>

==Variants==
[[File:TMW - Hütter H 17 b 2.jpg|thumb|right|Hü 17B in the [[Technisches Museum Wien]]]]
;Hü 17
:Initial model with a {{convert|9.7|m|ft|1|abbr=on}} wing span<ref name="SoaringNov83" /><ref name="SD" />
;Hü 17B
:Improved model, introduced after the [[Second World War]] with increased wing span and higher empty and gross weights<ref name="SoaringNov83" /><ref name="SD" />
;Göppingen Gö 5
:Model built by [[Schempp-Hirth]]<ref name="SoaringNov83" /><ref name="SD" />

==Aircraft on display==
*[[National Soaring Museum]]<ref name="NSMCollection" />
*[[Technisches Museum Wien]]

==Specifications (Hü 17b) ==
{{Aircraft specs
|ref=Sailplane Directory and Soaring<ref name="SoaringNov83" /><ref name="SD" /> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=5.18
|length ft=
|length in=
|length note=
|span m=9.96
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=9.47
|wing area sqft=
|wing area note=
|aspect ratio=10.5:1
|airfoil=root: Göttingen 535, tip [[NACA airfoil|NACA M-6]]
|empty weight kg=110
|empty weight lb=
|empty weight note=
|max takeoff weight kg=210
|more general=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=160
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Aerotow speed:''' {{convert|100|km/h|mph kn|abbr=on|1}}
*'''Winch launch speed:''' {{convert|80|km/h|mph kn|abbr=on|1}}
|g limits=
|roll rate=
|glide ratio= 17:1 at {{convert|64|km/h|mph|0|abbr=on}}
|sink rate ms=
|sink rate ftmin=192
|sink rate note= at {{convert|61|km/h|mph|0|abbr=on}}
|lift to drag=
|wing loading kg/m2=22.2
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
* [[Schweizer SGU 1-7]]
|lists=
* [[List of gliders]]
}}

==References==
{{reflist|30em}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}

==External links==
{{commons category|Hütter Hü 17}}
{{Ulrich Hütter and Wolfgang Hütter aircraft}}
{{Schempp-Hirth}}

{{DEFAULTSORT:Hutter H 17}}
[[Category:German sailplanes 1930–1939]]
[[Category:Hütter aircraft]]