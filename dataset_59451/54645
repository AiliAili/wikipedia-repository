{{Italic title}}
[[File:Lichtenstein, Wilhelm Hauff.JPG|thumb|Cover of [[Wilhelm Hauff]]'s most popular novel, ''Lichtenstein'' (1926)]]
[[File:Lichtenstein.jpg|thumb|[[Lichtenstein Castle (Württemberg)|Lichtenstein Castle]], inspired by the novel]]
'''''Lichtenstein''''' is a [[historical novel]] by [[Wilhelm Hauff]], first published in 1826, the year before his early death.  Set in and around [[Württemberg]], it is considered his greatest literary success next to his fairy-tales, and, together with the work of the almost forgotten [[Benedikte Naubert]], represents the beginning of historical novel-writing in Germany.{{sfn|Isaacs|1920}}

Hauff follows Sir [[Walter Scott]] (and Naubert) by allowing an invented figure in a real family, Georg Sturmfeder, to witness the events during the struggle of [[Ulrich, Duke of Württemberg|Ulrich von Württemberg]] against the [[Swabian League]] under [[Georg, Truchsess von Waldburg-Zeil]], who plays the part of foil to the positively depicted [[Georg von Frundsberg]].  Hauff idealises Ulrich as a wronged man, who, in his moment of need, is restored to his rights through the efforts of his people, symbolised by the character of the Piper of Hardt.  The historical Ulrich is remembered largely for his exorbitant taxes on meat, wine and fruit, which provoked the ''[[Armer Konrad]]'' peasant uprising of 1514, quelled only with the help of his enemy the Steward of Waldburg-Zeil.{{sfn|Isaacs|1920}}

''Lichtenstein'' was a romantic and patriotic German's tribute to the work of Sir Walter Scott.  No foreign author was more popular than Scott in the Germany of the early 19th century.  His novels, translated in full, were so generally read, that it was said that the soil of old [[Scotland]] was more familiar to Germans than their native land.  Yet the hills of Scotland, Hauff said, were not of a richer green than the German [[Harz]], the waves of the [[River Tweed|Tweed]] were no bluer than those of the [[Danube]], [[Scottish people|Scotch]] men were no braver, Scotch women no lovelier than the old Swabians and [[Saxon]]s.  ''Lichtenstein'', modelled after the [[Waverley Novels]], was a direct protest against the novel with a foreign historical background, and was an attempt to recreate the romance of a bit of Swabian history; as Scott had done for the country of ''[[Ivanhoe]]'' and ''[[Waverley (novel)|Waverley]]''.{{sfn|Isaacs|1920}}

The great success of the book prompted the rebuilding of [[Lichtenstein Castle (Württemberg)|Lichtenstein Castle]] in 1840–1842.  The 1846 [[opera]] of the same name by [[Peter Josef von Lindpaintner]] is based on this novel.{{sfn|Isaacs|1920}}

==Notes==
{{reflist}}

==References==
*{{Americana|wstitle=Lichtenstein|year=1920|first=Edith J. R.|last=Isaacs}}

==External links==
*[http://gutenberg.spiegel.de/hauff/lichtens/lichtens.htm Complete text] at ''[[Der Spiegel]]'''s Projekt Gutenberg {{De icon}}
{{Gutenberg|no=6726|name=Lichtenstein}} {{De icon}}

{{Authority control}}
<!--Categorization may encourage further elaboration-->
[[Category:1826 novels]]
[[Category:19th-century German novels]]
[[Category:German historical novels]]
[[Category:16th century in fiction]]

{{1820s-novel-stub}}