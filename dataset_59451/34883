{{Use mdy dates|date=November 2013}}
{{good article}}
{{Infobox NFL season
| team = Columbus Panhandles
| year = 1920
| record = 2–7–2
| league_place = 13th APFA
| coach = [[Ted Nesser]]
| general manager = [[Joseph Carr]]
| owner =
| stadium = Neil Park
| playoffs = <!--No playoffs until 1932-->
| AP All-pros =
| AFL allstars =
| MVP =
| ROY =
| uniform =
}}

The '''[[1920 APFA season|1920]] [[Columbus Panhandles]] season''' was the franchise's inaugural season in the [[American Professional Football Association]] (APFA)—later named the National Football League. The season concluded with the team going 2–7–2 and finishing 13th place in the APFA standings. The Panhandles entered the season after a 3–6–1 record in [[1919 Columbus Panhandles season|1919]]. The team opened the 1920 season with a loss to the [[Dayton Triangles]], and the Panhandles lost five straight until a victory over the [[Zanesville Mark Greys]]. Not a single player was on the [[All-Pro]] list.

{{TOC limit|2}}

== Offseason ==
The Columbus Panhandles finished their [[1919 Columbus Panhandles season|1919 season]] with a 3–6–1 record in the [[Ohio League]].<ref>Willis (2007), p. 165</ref> On August 20, 1920, a meeting was held at Ralph Hay's automobile attended by representatives of four Ohio League teams: the [[Canton Bulldogs]], the [[Cleveland Tigers (NFL)|Cleveland Tigers]], the [[Dayton Triangles]], and the [[Akron Pros]]. At the meeting, they tentatively agreed to introduce a salary cap for the teams, not to sign college players nor players under contract with another team, and became united as the American Professional Football Conference.<ref>Siwoff, Zimmber & Marini (2010), pp. 352–353</ref> They then contacted other major professional teams and invited them to a meeting for September 17.<ref>P.F.R.A. Research (1980), p. 5</ref>

At the meeting in September, representatives of the [[Rock Island Independents]], the [[Muncie Flyers]], the [[History of the Chicago Bears|Decatur Staleys]], the [[Racine Cardinals]], the [[Massillon Tigers]], the [[Chicago Cardinals (NFL, 1920–59)|Chicago Cardinals]], the [[Rochester Jeffersons]], and the [[Hammond Pros]]. The following was achieved during the September 17 meeting: the name of American Professional Football Association was chosen; officers of the league were elected with [[Jim Thorpe]] as president; a membership fee of $100 was set; a committee to draft a constitution was named; the secretary of the league was to receive a list of all players used during the season by January 1, 1921; and the trophy that would be awarded to the league champions.<ref>P.F.R.A. Research (1980), p. 6</ref><ref>{{Cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9503E5DC173AE532A2575AC1A96F9C946195D6CF | title=Thorpe Made President | newspaper=[[The New York Times]] | date=September 19, 1920 | format=PDF}}</ref><ref>{{Cite news | url=https://news.google.com/newspapers?id=f8MWAAAAIBAJ&sjid=OiEEAAAAIBAJ&pg=3568,5105560&dq=gridders&hl=en | title=Organize Pro Gridders; Choose Thorpe, Prexy | newspaper=[[The Milwaukee Journal]] | date=September 19, 1920 | page=24}}</ref> Even though the Panhandles were not at the meetings, they were still a charter member of the APFA.<ref>Willis (2007), p. 167</ref>

== Regular season ==
The Panhandles played their only home game at [[Neil Park]]. Joseph Carr made the team play mostly away games because they were able to travel on the railroads for free. This cut down on stadium cost and saved the team money.<ref name="B&C 79 p.4">Braunwart & Carroll (1979), p. 4</ref> The regular season schedule was not a fixed schedule but was created dynamically by each team as the season progressed.<ref>Peterson (1997), p. 74</ref><ref>Davis (2005), p. 59</ref> Over the course of the 1920 season, the Panhandles played a total of 11 games. Of those 11 games, five were against APFA teams, and the others were against non-APFA teams. Every game played against league teams resulted in a loss.

The records kept for the 1920 season included games played against APFA and non-APFA teams.<ref>Siwoff, Zimmber & Marini (2010), p. 392</ref> The Panhandles opened the season with a 14–0 loss to the [[Dayton Triangles]] in the first football game with two APFA teams.<ref name="Braunwart">Braunwart & Carroll (1981), p. 1</ref> The previous week, considered week one, the Rock Island Independents played against the [[St. Paul Ideals]] in the first APFA game. The Panhandles lost their next five games without scoring a point, until a 10–0 win over the [[Zanesville Mark Greys]]. The Panhandles ended the season with a 24–0 victory and finished with a 2–7–2 record.

=== Schedule ===
Table gathered from ''The Columbus Panhandles''<ref>Willis (2007), pp. 176–177</ref> and NFL History which uses various newspapers. For the results column, the winning team's score is posted first followed by the result for the Flyers. For the attendance, if a cell is greyed out and has "N/A", then that means there is an unknown figure for that game. The green-colored cells indicates a win; and the red-colored cells indicate a loss. The games against the local teams are listed, but are not counted in the standings. This is why the record column does not change following the result of those games.

{| class="wikitable" style="text-align:center"
|-
! Week !! Date !! Opponent !! Result !! Venue !! Attendance !! Record
|-
! 1
| colspan="8" | ''No game scheduled''
|-style="background: #ffdddd;"
! 2
| October 3, 1920 || at [[Dayton Triangles]] || 14–0 L || [[Triangle Park]] || — || 0–1
|-style="background: #ffdddd;"

! 3
| October 10, 1920 || at [[Akron Pros]] || 37–0 L || [[League Park (Akron)|League Park]] || — || 0–2
|-style="background: #ffdddd;"

! 4
| October 17, 1920 || at [[Fort Wayne Friars]]{{Dagger}} || 14–0 L || League Park || 5,000 || 0–3
|-style="background: #ffdddd;"

! 5
| October 24, 1920 || at [[Detroit Heralds]] || 6–0 L || [[Mack Park]] || — || 0–4
|-style="background: #ffdddd;"

! 6
| October 31, 1920 || at [[Cleveland Tigers (NFL)|Cleveland Tigers]] || 7–0 L || League Park || 5,000 || 0–5
|-style="background: #ddffdd;"

! 7
| November 7, 1920 || at [[Zanesville Mark Greys]]{{Dagger}} || 10–0 W || [[Mark Athletic Field]] || — || 1–5
|-style="background: #ffdddd;"

! 8
| November 14, 1920 || at [[Buffalo (NFL)|Buffalo All-Americans]] || 43–7 L || [[Canisius Field]] || 9,000 || 1–6
|-style="background: #ffeeaa;"

! 9 
| November 21, 1920 || at Zanesville Mark Greys{{Dagger}} || 0–0 T || Mark Athletic Field || — || 1–6–1
|-style="background: #ffeeaa;"

! rowspan="2" | 10
| November 25, 1920 || at [[Elyria Athletics]]{{Dagger}} || 0–0 T || [[Lorain Athletic Field]] || — || 1–6–2
|-style="background: #ffdddd;"
| November 28, 1920 || at [[Youngstown Patricians]]{{Dagger}} || 2–0  L|| [[Idora Park]] || — || 1–7–2
|-style="background: #ddffdd;"

! 11 
| December 5, 1920 || vs [[Columbus Wagner Pirates]]{{Dagger}} || 24–0 W || Neil Park || 2,000 || 2–7–2
|-
! 12
| colspan="8" | ''No game scheduled''
|-
! 13
| colspan="8" | ''No game scheduled''
|}

== Game summaries ==

=== Week 2: at Dayton Triangles ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Triangles'''
|H1=0|H2=0|H3=7|H4=7
}}
''October 3, 1920, at Triangle Park''

The Panhandles' opening game against the Dayton Triangles is considered to be the first football game between two APFA teams. The Panhandles lost 14–0 to the Triangles.<ref name="Braunwart" /> Despite the first two quarters resulting in ties, the crowd was excited.<ref name="B&C p. 2">Braunwart & Carroll (1981), p. 2</ref> In the second quarter, the Triangles made a goal line stand while the Panhandles had the ball on the 3-yard line.<ref name="B&C p. 2" /> Before halftime, the Triangles' [[Al Mahrt]] completed a 30-yard pass to [[Dutch Thiele]], which resulted in the Triangles to having the ball on the 5-yard line.<ref name="B&C p. 2" /> The Triangles' did not score on that possession due to the clock running out.<ref name="B&C p. 2" />

Early in the third quarter, the Triangles started a possession on their own 35-yard line.<ref name="B&C p. 2" /> Four consecutive run plays carried them to midfield.<ref name="B&C p. 2" /> Then, [[Lou Partlow]] had a long run to the 10-yard line.<ref name="B&C p. 2" /> The possession ended with a rushing touchdown from Partlow.<ref name="B&C p. 2" /> The other Triangle score came in the middle of the fourth quarter. [[Frank Bacon (American football)|Frank Bacon]] returned a punt return for a 60-yard touchdown.<ref name="B&C p. 2" /> After both touchdowns, [[George Kinderdine]] was responsible for the extra points.<ref name="B&C p. 2" />

=== Week 3: at Akron Pros ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Pros'''
|H1=7|H2=14|H3=14|H4=2
}}
''October 10, 1920, at League Park''

Following the loss, the Panhandles played against the Akron Pros. Running back [[Frank McCormick (American football)|Frank McCormick]] of the Pros rushed for two touchdowns to give Akron a 14–0 lead in the second quarter. Bob Nash later recovered a fumble in the end zone for the first score from a fumble recovery.<ref name="B&C p. 4">Braunwart & Carroll (1981), p. 4</ref> [[Harry Harris (American football)|Harry Harris]] and fullback [[Fred Sweetland]] also contributed for the Pros, each scoring one rushing touchdown. The defense added another safety in the fourth quarter—which was the first safety in APFA history<ref name="B&C p. 4" />—to give to Panhandles their second loss of the season, 37–0.<ref>{{Cite news | title=Panhandlers Lose to Akron | date= October 11, 1920 | url=https://news.google.com/newspapers?id=eGxKAAAAIBAJ&sjid=QYYMAAAAIBAJ&pg=5019%2C4223434 | newspaper=[[Youngstown Vindicator]] | page=10}}</ref><ref name="News-Bee Oct 11 1920">{{Cite news | url=https://news.google.com/newspapers?id=9e9XAAAAIBAJ&sjid=BEUNAAAAIBAJ&pg=422%2C1570727 | title=Maroons Can't Stand Gaff and Canton Wins, 42 to 0 | newspaper=[[The Toledo News-Bee]] | page=14 | date=October 11, 1920}}</ref>

=== Week 4: at Fort Wayne Friars ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Friars'''
|H1=7|H2=0|H3=7|H4=0
}}
''October 17, 1920, at League Park''

In their third game of the season, the Panhandles played against the non-APFA Fort Wayne Friars. In the first ten minutes of the game, [[Lee Snoots]] was injured and had to miss the rest of the game.<ref name="Willis2007–172">Willis (2007), p. 172</ref> In front of 5,000 fans, the Panhandles lost 14–0. Huffine for the Friars scored two rushing touchdowns, one in the first and one in the third. This was the Panhandles' fourth straight loss to the Friars.<ref name="Willis2007–172" />

=== Week 5: at Detroit Heralds ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Heralds'''
|H1=0|H2=6|H3=0|H4=0
}}
''October 24, 1920, at Mack Park''

Following the loss, the Panhandles traveled to play the Detroit Heralds, an APFA team. The Panhandles' passing attack helped them outgain the Heralds, but, according to the ''Ohio State Journal'', it was a close game and "one play decided the outcome."<ref name="Willis2007–172" /> The Heralds' left end, Fitzgerald, intercepted a pass from Frank Nesser and ran it back for an 85-yard touchdown.<ref name="Willis2007–172" />

=== Week 6: at Cleveland Tigers ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Tigers'''
|H1=0|H2=7|H3=0|H4=0
}}
''October 31, 1920, at League Park''

In week 6, the Panhandles played against the Cleveland Tigers. In front of 5,000 fans,<ref name="Willis2007–177" /> the Tigers won 7–0.<ref name="week6news">{{Cite news | date=November 1, 1920 | title=Brickley Boy Wins for Cleveland | url=https://news.google.com/newspapers?nid=k_8v9Q84L5sC&dat=19201101&printsec=frontpage&hl=en | newspaper=[[The Toledo News-Bee]] | page=12}}</ref> The lone score came from a rushing touchdown in the second quarter from Charlie Brickley. This was the eighth straight loss for the Panhandles, dating back to 1919, and the seventh straight without scoring.<ref name="Willis2007–173">Willis (2007), p. 173</ref> According to football historian Chris Willis, this loss for the Panhandles crushed the city of Columbus and made the Panhandles challenge lesser teams for the rest of the season.<ref name="Willis2007–173" />

=== Week 7: at Zanesville Mark Greys ===
{{Linescore Amfootball
|Road='''Panhandles'''
|R1=10|R2=0|R3=0|R4=0
|Home=Mark Greys
|H1=0|H2=0|H3=0|H4=0
}}
''November 7, 1920, at Mark Athletic Field''

The Panhandles recorded their first victory of the season with a 10–0 win against the non-APFA Zanesville Mark Greys. On the day before the game, the ''Zanesville Signal'' ran an advertisement to help promote the game, and the city of Zanesville was "excited" to host the Panhandles.<ref name="Willis2007–173" /> In the first quarter, Jim Flower caught a touchdown pass from Frank Nesser. In the same quarter, Nesser kicked a 35-yard field goal. The points scored in the first quarter ended a streak of 28 straight scoreless quarters.<ref name="Willis2007–173">Willis (2007), p. 173</ref>

=== Week 8: at Buffalo All-Americans ===
{{Linescore Amfootball
|Road=Panhandles
|R1=7|R2=0|R3=0|R4=0
|Home='''All-Americans'''
|H1=6|H2=23|H3=7|H4=7
}}
''November 14, 1920, at Canisius Field''

In front of 9,000 fans, the Panhandles played their last against an APFA opponent, the Buffalo All-Americans, in week 8.<ref name="Willis2007–174">Willis (2007), p. 174</ref> Coming into the game, the All-Americans had a 6–0 undefeated record.<ref>{{Cite web | url=http://www.pro-football-reference.com/teams/bff/1920.htm | title=1920 Buffalo All-Americans Statistics & Players | work=''[[Pro-Football-Reference.com]]'' | publisher=Sports Reference | accessdate=February 27, 2012}}</ref> At the end of the first quarter, the game near-even; the score was 7–6, Panhandles. After that, the game "proved disastrous" for the Panhandles.<ref name="Willis2007–173" /> The final score was 43–7; the only score was a receiving touchdown from Homer Ruh. The Panhandles' defense allowed six rushing touchdowns, four of which came from the All-Americans' Smith. The other two came from Anderson and Hughitt. From these six rushing touchdowns, five of the extra points were converted, and the Panhandles' offense allowed a safety.

=== Week 9: at Zanesville Mark Greys ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home=Mark Greys
|H1=0|H2=0|H3=0|H4=0
}}
''November 21, 1920, at Mark Athletic Field''

In the Panhandles' rematch against the Mark Greys, the final score was a 0–0 tie. Chris Willis stated the game was a "nightmare" for the Panhandles, and the game felt like a loss for them.<ref name="Willis2007–174" /> The ''Zanesville Signal'' claimed the Mark Greys outplayed the Panhandles in every aspect and called the game "one of the best&nbsp;... of the season."<ref name="Willis2007–174" /> According to [[Pro-Football-Reference.com]], this game was the seventh game in NFL history to result in a 0–0 tie.<ref>{{Cite web | url=http://www.pro-football-reference.com/boxscores/game_scores_find.cgi?pts_win=0&pts_lose=0 | title=All Games in Pro Football History with a 0 to 0 score | work=''[[Pro-Football-Reference.com]]'' | publisher=Sports Reference | accessdate=February 29, 2012}}</ref>

=== Week 10: at Elyria Athletics ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home=Athletics
|H1=0|H2=0|H3=0|H4=0
}}
''November 25, 1920, at Lorain Athletic Field''

Following the tie to the Mark Greys, the Panhandles traveled to [[Lorain, Ohio]], to play against the Elyria Athletics [[NFL on Thanksgiving Day|on Thanksgiving Day]]. The result of the game was another 0–0 tie, making it the seventh time in nine games the Panhandles were held scoreless.<ref name="Willis2007–174" /> Chris Willis stated this tie was not as bad as the previous weeks because the Athletics had old players from the [[Akron Pros|Akron Indians]], a winning team in the Ohio League.<ref name="Willis2007–174" />

=== Week 10: at Youngstown Patricians ===
{{Linescore Amfootball
|Road=Panhandles
|R1=0|R2=0|R3=0|R4=0
|Home='''Patricians'''
|H1=0|H2=2|H3=0|H4=0
}}
''November 28, 1920, at Idora Park''

In their second game in three days, the Panhandles played against the Youngstown Patricians. It was a low-scoring, 2–0 loss. The lone score came from a safety in the second quarter.<ref name="week10.2vind">{{Cite news | url=https://news.google.com/newspapers?nid=pqgf-8x9CmQC&dat=19201129&printsec=frontpage&hl=en | title={{-'}}Pats' Defeat Panhandles, in Last Game | newspaper=[[Youngstown Vindicator]] | date=November 29, 1920 | page=5}}</ref> The Panhandles were about to punt, but it was blocked. The ball landed in the end zone, and a Panhandle player landed on the ball.<ref name="week10.2vind" /> According to the ''Youngstown Vindicator'', the football's condition was damaged due to the water on the field and several fumbles occurred throughout the game.<ref name="week10.2vind" />

Even though the Nesser brothers "starred on several occasions", the Patricians' defense was more dominant. It was called a "stone wall" and stopped the Panhandles several times on short-yardage plays.<ref name="week10.2vind" /> According to the ''Vindicator'', the number of first downs the Panhandles got were less than five.<ref name="week10.2vind" />

=== Week 11: vs Columbus Wagner Pirates ===
{{Linescore Amfootball
|Road=Wagner Pirates
|R1=0|R2=0|R3=0|R4=0
|Home='''Panhandles'''
|H1=3|H2=0|H3=14|H4=7
}}
''December 5, 1920, at Neil Park''

In week 11, the Panhandles played their only home game of the season against the Columbus Wagner Pirates. In front of a crowd of 2,000, the Panhandles won their second game of the season 24–0.<ref name="Willis2007–177">Willis (2007), p. 177</ref> In the first quarter, Frank Nesser scored the first points of the game with a 42-yard field goal. Even though the first half score was 3–0, the Panhandles heavily outplayed the Wanger Pirates.<ref name="Willis2007–176">Willis (2007), p. 176</ref> In the third quarter, Snoots ran for two rushing touchdowns. In the last quarter, Frank Nesser also contributed with a rushing touchdown. Despite Nesser kicking a field goal early in the game, Jim Flowers was the person who kicked the extra points in the game. This victory over the Wagner Pirates allowed the Panhandles to win their "city's championship".<ref>PFRA Research (n.d.), p. 4</ref>

==Standings==
{{1920 APFA standings}}

== Roster ==
The list of players and the coaching staff is gathered from ''Uniform Numbers of the NFL: Pre-1933 Defunct Teams'',<ref>{{Cite web | last1=Steffenhagen | first2=John | last2=Ruehle | first=Bernie | url=http://www.rci.rutgers.edu/~maxymuk/home/ongoing/columbus.html | title=Columbus Panhandles (1920–22)/Columbus Tigers (1923–26) | publisher=[[Rutgers University]] | date=July 18, 2007 | accessdate=December 21, 2011}}</ref> ''The Columbus Panhandles'',<ref>Willis (2007), pp.177–178</ref> and [[Pro-Football-Reference.com]].<ref>{{Cite web | url=http://www.pro-football-reference.com/teams/col/1920_roster.htm | title=1920 Columbus Panhandles Starters, Rosters, & Players | work=''[[Pro-Football-Reference.com]]'' | publisher=Sports Reference | accessdate=February 19, 2012}}</ref>
{{Col-start}}
{{Col-2}}

=== Players ===
{| class="wikitable plainrowheaders sortable"
|-
! scope="col" | Name
! scope="col" | Position
|-
! scope="row" | Otto Beckwith
| HB
|-
! scope="row" | Hi Brigham
| G
|-
! scope="row" | John Davis
| FB/HB/QB
|-
! scope="row" | Charlie Essman
| G
|-
! scope="row" | [[Jim Flower (American football)|Jim Flower]]
| E
|-
! scope="row" | Hal Gaulke
| QB/FB
|-
! scope="row" | Orlan Houck
| G
|-
! scope="row" | Oscar Kuehner
| T
|-
! scope="row" | [[Frank Lone Star]]
| G/T
|-
! scope="row" | Wilkie Moody
| HB/FB/G
|-
! scope="row" | [[Joe Mulbarger]]
| T
|-
! scope="row" | [[Frank Nesser]]
| FB/T/G
|-
! scope="row" | [[Phil Nesser]]
| G
|-
! scope="row" | [[Ted Nesser]]
| G
|-
! scope="row" | Dwight Peabody
| E
|-
! scope="row" | Homer Ruh
| E
|-
! scope="row" | [[John Schneider (American football)|John Schneider]]
| HB
|-
! scope="row" | Lee Snoots
| HB
|-
! scope="row" | Will Waite
| C
|-
! scope="row" | Oscar Wolford
| FB/G
|-
! scope="row" | Howard Yerges
| HB
|}
{{Col-2}}

=== Coaching staff ===
{| class="wikitable plainrowheaders sortable"
|-
! scope="col" | Name
! scope="col" | Position
|-
! scope="row" | Joe Carr
| Manager
|-
! scope="row" | Ted Nesser
| Head Coach
|}
{{Col-end}}

== Notes ==
{{Reflist|2}}

== References ==
* {{Cite journal | last1=Braunwart | first1=Bob | last2=Carroll | first2=Bob | year=1981 | url=http://www.profootballresearchers.org/Coffin_Corner/03-02-059.pdf | title=The First NFL Game(s) | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=3 | issue=2}}
* {{Cite journal | last1=Braunwart | first1=Bob | last2=Carroll | first2=Bob | year=1979 | url=http://www.profootballresearchers.org/Coffin_Corner/01-08-013.pdf | title=The Columbus Panhandles: Last of the Sandlotters | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=1 | issue=8}}
* {{Cite book | last=Davis | first=Jeff | title=Papa Bear, The Life and Legacy of George Halas | publisher=McGraw-Hill | year=2005 | location = New York | isbn=0-07-146054-3}}
* {{cite journal | url=http://www.profootballresearchers.org/Coffin_Corner/02-08-038.pdf | title=Happy Birthday NFL? | publisher=[[Professional Football Researchers Association]] | author=P.F.R.A. Research | journal=[[The Coffin Corner]] | year=1980 | volume=2 | issue=8}}
* {{Cite web | author=PFRA Research | url=http://www.profootballresearchers.org/Articles/Forward_Into_Invisibility.pdf | title=Forward Into Invisibility: 1920 | publisher=[[Professional Football Researchers Association]] | accessdate=February 27, 2012|archiveurl=http://www.webcitation.org/65r51k68q|archivedate=March 1, 2012|deadurl=no}}
* {{cite book | last = Peterson | first1 = Robert W | year=1997 | title = Pigskin: The Early Years of Pro Football | publisher = Oxford University Press | location = New York | isbn = 0-19-507607-9}}
* {{Cite book | last1=Siwoff | first1=Seymour | last2=Zimmber | first2=Jon | last3=Marini | first3=Matt | year=2010 | title=The Official NFL Record and Fact Book 2010 | publisher=[[National Football League]] | isbn=978-1-60320-833-8}}
* {{Cite book | last=Willis | first=Chris | title=The Columbus Panhandles: A Complete History of Pro Football's Toughest Team, 1900–1922 | location=Lanham, Maryland | publisher=Scarecrow Press | year=2007 | isbn=978-0-8108-5893-0}}

== External links ==
* [http://www.pro-football-reference.com/teams/col/1920.htm 1920 Columbus Panhandles] at [[Pro-Football-Reference.com]]

{{1920 APFA season by team}}
{{Columbus Panhandles}}
{{Columbus Tigers seasons}}

{{DEFAULTSORT:1920 Columbus Panhandles Season}}
[[Category:Columbus Panhandles seasons]]
[[Category:1920 American Professional Football Association season by team|Columbus Panhandles]]