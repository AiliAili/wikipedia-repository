<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name= Scimitar
|image= Scimitars 62.jpg
|caption= Scimitars of 736 Naval Air Squadron at Farnborough 1962
}}{{Infobox Aircraft Type
|type= Naval strike fighter
|manufacturer= [[Supermarine]]
|first flight= 19 January 1956
|introduced= 1957
|retired= 1969
|status= No longer in service
|primary user= [[Royal Navy]]
|more users=
|produced=
|number built= 76
|unit cost=
|variants with their own articles =
}}
|}
The '''Supermarine Scimitar''' was a [[United Kingdom|British]] naval [[fighter aircraft]] operated by the [[Royal Navy]] [[Fleet Air Arm]]. The prototype for the eventual production version flew in January 1956 and production aircraft were delivered in 1957. It saw service with the Royal Navy from 1958 until 1969.

==Design and development==
The Scimitar stemmed from a number of designs from Supermarine for a naval jet aircraft, initially to a requirement for an undercarriage-less fighter aircraft to land on flexible "sprung" [[rubber deck]]s.<ref name="Thunder">[http://www.thunder-and-lightnings.co.uk/scimitar/history.php "Scimitar History."]  ''thunder-and-lightnings.co'', 5 April 2009. Retrieved: 23 March 2010.</ref> which would allow for a lighter and simpler structure.<ref name="Andrews Super p297-8">Andrews and Morgan 1987, pp. 297–298.</ref> Supermarine's design to meet this requirement was the Type 505, featuring a thin, straight wing and a [[V-tail]] (or "butterfly tail") to keep the tail surfaces away from the jet exhausts, and to be powered by two [[Rolls-Royce Avon]] [[turbojet]]s, mounted side-by-side in the fuselage. In 1948, the Admiralty had second thoughts about the undercarriage-less fighter and Supermarine reworked their design by including a [[Tricycle gear|nosewheel undercarriage]], becoming the Type 508.<ref name="Mason Fighter p375">Mason 1992, p. 375.</ref> The Vickers-Supermarine Type 508 was the first Scimitar ancestor and shared the layout of the Type 505, i.e. a twin-engined straight-winged type with a V-tail. Pitch control was by moving the whole tail, with [[Elevator (aircraft)|elevators]] for additional pitch control when working in tandem and to replace the rudder on a conventional tail when working differentially. [[Aileron]]s were fitted to the wings for lateral control and leading and trailing edge [[Flap (aircraft)|flaps]] were also fitted to the wings.<ref name="Andrews Super p298">Andrews and Morgan 1987, p. 298.</ref><ref name="IAPR 2 p158-9">Buttler 2001, pp. 158–159.</ref> An order for three Type 508s was placed in November 1947, to [[List of Air Ministry specifications#1940-1949|Specification N.9/47]].<ref name="IAPR 2 p158-9"/>

The first Type 508 made its maiden flight from [[MoD Boscombe Down|Boscombe Down airfield]] on 31 August 1951, with the aircraft carrying out carrier trials aboard [[HMS Eagle (R05)|HMS ''Eagle'']] in May 1952.<ref name="Mason Fighter p376">Mason 1992, p. 376.</ref> The second aircraft had significant differences, carrying a cannon armament and was different enough in detail to be renamed the Type 529, flying for the first time on 29 August 1952.<ref name="IAPR 2 p158-9"/> One unusual modification was the larger tailcone for a proposed tail-warning radar.<ref>Buttler 2008, p. 56.</ref> The maximum speed of the straight-winged Type 508 and 529 was relatively modest, with the Type 529 reaching 607&nbsp;mph (977&nbsp;km/h) and it had already been decided when the Type 508 first flew, to redesign the third prototype with swept wings to improve performance. The resulting [[Supermarine 525|Type 525]] also featured conventional swept tail surfaces as well as [[blown flap]]s to reduce the aircraft's landing speed and first flew on 27 April 1954.<ref name="IAPR 2 p159-0">Buttler 2001, pp. 159–160.</ref>  It later crashed but the basic design had already proved sound enough to proceed with an outwardly fairly similar looking aircraft, the Type 544, to specification N.113.<ref name="Flight">[http://www.flightglobal.com/FlightPDFArchive/1957/1957%20-%201236.PDF "Supermarine Scimitar."] ''Flight'', 1957. Retrieved: 5 August 2013.</ref> A total of 100 were ordered, although the Royal Navy had changed the specification to a low level strike aircraft with nuclear capability rather than a fighter.

The first of the Type 544s serving as prototypes for the later production series flew on 19 January 1956. The aircraft evolved more with the third Type 544 incorporating different aerodynamic changes and a stronger airframe for the new low level role - to quote ''[[Flight International|Flight]]''; "To permit uninhibited manoeuvring in thick turbulent air at low levels while carrying heavy loads of strike weapons, the structure is extremely sturdy".<ref name="Flight"/> Various aerodynamic "fixes" to try and counter pitch-up effects at high speed and altitude included flared-out wingtips and wing fences. The tailplane was also changed from dihedral to anhedral. The combined modifications led to the final Type 544 being considered the "production standard". The first production Scimitar flew on 11 January 1957.<ref>Buttler 2008, pp. 62–63.</ref>

==Operational history==
[[File:US Navy and Royal Navy aircraft refueling c1962.jpeg|thumb|An [[803 Naval Air Squadron|803 NAS]] Scimitar from [[HMS Hermes (R12)|HMS ''Hermes'']] with U.S. Navy aircraft over the Mediterranean Sea]]
At the time of introduction most of the Royal Navy's carriers were quite small and the Scimitar was a comparatively large and powerful aircraft. Landing accidents were common and the introduction of the type was marred by a fatal accident which took the life of Commander John Russell, commanding officer of [[803 Naval Air Squadron]], the first squadron to operate the Scimitar. After a perfect landing on the newly recommissioned {{HMS|Victorious|R38|6}} and in full view of the press, one of the [[arrestor wire]]s broke, and Russell's Scimitar (serial XD240) fell into the sea.  With no means of ejecting through the jammed canopy and despite the best efforts of the crew of the [[Westland Whirlwind (helicopter)|Westland Whirlwind]] planeguard helicopter to perform a rescue, Russell's Scimitar sank to the bottom and Cdr Russell drowned.<ref>"Flight & Aircraft Engineer." ''[[Flight International]],'' 2593, October 1958.</ref> The incident was later broadcast by British Pathé News.<ref>https://www.youtube.com/watch?v=Qg0Jj-2x5rM&list=PLADB9C9B996134C1C</ref> Overall the Scimitar suffered from a high loss rate; 39 were lost in a number of accidents, amounting to 51 percent of the Scimitar's production run.<ref name="Thunder"/>

The aircraft pioneered fuel flow proportioning and integral main-plane tanks, along with "blown" flying surfaces, to reduce landing speeds. At one time, it held the record of 1,000 maintenance hours per flying hour.{{Citation needed|date=March 2009}} Although the Scimitar could operate as a fighter, the interceptor role was covered by the [[de Havilland Sea Vixen]]. In the attack role it was replaced by the [[Blackburn Buccaneer]]. The Scimitar was retained initially as a tanker to allow the underpowered Buccaneer S.1 to be launched from aircraft carriers with a useful weapons load. To save weight, the Buccaneer would take off with minimum fuel then top up from a Scimitar. Late in the Scimitar's operational career, examples were flown between 1965 and 1970 by the Fleet Requirements Unit (FRU) based at Bournemouth Airport (Hurn). The FRU was managed by [[Airwork Services]] and provided realistic flight operations for land and sea based naval training units.{{Citation needed |date=September 2015}}

==Variants==
[[File:Supermarine 508 two-view silhouette.png|thumb|right|Supermarine 508 research aircraft]]

===Predecessors===
;Type 508
:Straight-wing research aircraft.
;Type 529
:Straight-wing research aircraft.
;[[Supermarine 525|Type 525]]
:Swept-wing research aircraft.

===Prototypes===
;Type 544
:Prototype for the Scimitar F.1, 3 built by Vickers-Armstrong Experimental Department at [[Hursley Park]]

===Production model===
;Scimitar F.1
:Single-seat multi-role fighter aircraft, 76 built by Vickers-Armstrong at [[South Marston]]. Original order was for 100 aircraft in 1952 later reduced to 76.

==Operators==
;{{UK}}
* [[Royal Navy]] [[Fleet Air Arm]]
**   700X Naval Air Squadron{{sfn|Sturtivant|Ballance|1994|p=16}}
** [[736 Naval Air Squadron]]{{sfn|Sturtivant|Ballance|1994|p=58}}
** [[800 Naval Air Squadron]]{{sfn|Sturtivant|Ballance|1994|p=125}}
** [[803 Naval Air Squadron]]{{sfn|Sturtivant|Ballance|1994|p=138}}
** [[804 Naval Air Squadron]]{{sfn|Sturtivant|Ballance|1994|p=141}}
** [[807 Naval Air Squadron]]{{sfn|Sturtivant|Ballance|1994|p=152}}
** [[Fleet Requirements Unit]]

==Survivors==
* Scimitar F1 ''XD220'' at the [[Empire State Aerosciences Museum]], formerly at the [[Intrepid Sea-Air-Space Museum]], New York, United States (on loan from the Fleet Air Arm Museum).<ref>McGeehan, Patrick. [https://www.nytimes.com/2012/04/19/nyregion/anticipating-space-shuttles-arrival-old-warplanes-ship-out.html?_r=1  "Anticipating Space Shuttle’s Arrival, Old Warplanes Ship Out."] ''The New York Times,'' 18 April 2012.</ref>

[[File:supermarine-scimitar.jpg|thumb|right|''XD220'' in [[Intrepid Sea-Air-Space Museum|Intrepid Museum]]]]
* Scimitar F1 ''XD317'' at the [[Fleet Air Arm Museum]], Yeovilton, England.
* Scimitar F1 ''XD332'' at the [[Solent Sky]] Museum, Southampton, England.

==Specifications (Scimitar F.1)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |ref=, |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specification]]. -->
|ref=<ref name="Andrews Super p306">Andrews and Morgan 1987, p. 306.</ref>
|crew=1
|length main=55 ft 3 in
|length alt=16.84 m
|span main=37 ft 2 in
|span alt=11.33 m
|height main=17 ft 4 in
|height alt=5.28 m
|area main=485 ft²
|area alt=45.06 m²
|empty weight main=23,962 lb
|empty weight alt=10,869 kg
|loaded weight main=34,200 lb
|loaded weight alt=15,513 kg
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (jet)=[[Rolls-Royce Avon]] 202
|type of jet=[[turbojet]]
|number of jets=2
|thrust main=11,250 lbf <ref name="Thetford Navy p339">Thetford 1978, p. 339.</ref>
|thrust alt=50.1 kN
|max speed main=640 kn
|max speed alt=736 mph, 1,185 km/h
|max speed more=at sea level
|range main=1,237 nmi
|range alt=1,422 mi, 2,289 km
|ceiling main=46,000 ft
|ceiling alt=14,000 m
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|thrust/weight=
|more performance=*'''Climb to 45,000 ft (13,700 m):''' 6.65 min
|avionics=
|guns=4 x 30 mm [[ADEN cannon]] with 160 rounds per gun<ref name="IAPR 2 p164">Buttler 2001, p. 164.</ref>
|bombs=
|rockets=
|missiles=
|hardpoints=4
|hardpoint capacity=
** 4 x 1,000 lb (454 kg) bombs ''or''
** 4 x [[AGM-12 Bullpup]] ''or'' [[AIM-9 Sidewinder]] missiles ''or''
** up to 16 two or three inch unguided rockets (4 per pylon)
** 1 x [[Red Beard (nuclear weapon)|Red Beard]] freefall nuclear bomb<ref name="IAPR 2 p164"/>
}}

==See also==
{{Aircontent
|related =
|similar aircraft =
* [[Dassault Étendard IV]]
* [[McDonnell F3H Demon]]
|see also =
|lists =
}}

==References==

===Notes===
{{Reflist|20em}}

===Bibliography===
{{Refbegin}}
* Andrews, C.F. and E.B. Morgan. ''Supermarine Aircraft since 1914''. London: Putnam, 1987. ISBN 0-85177-800-3.
* Birtles, Philip. ''Supermarine Attacker, Swift and Scimitar (Postwar Military Aircraft 7)''. London: Ian Allan, 1992. ISBN 0-7110-2034-5.
* Buttler, Tony. "Database: Supermarine Scimitar". ''Aeroplane''. Volume 36, No. 12, Issue No. 428, December 2008.
* Buttler, Tony. "Type Analysis: Supermarine Scimitar". ''International Air Power Review''. Norwalk, Connecticut, USA:AIRtime Publishing. Volume Two, Autumn/Fall 2001, pp.&nbsp;158–173. ISBN 1-880588-34-X, ISSN 1473-9917.
* Gibbings, David and J.A. Gorman. ''Scimitar''. RNAS Yeovilton, Somerset, UK: Society of Friends of the Fleet Air Arm Museum, 1988. ISBN 0-948251-39-5.
* Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* Morgan, Eric and John Stevens. ''The Scimitar File''. Tunbridge Wells, Kent, UK: Air Britain (Historians) Ltd., 2000. ISBN 978-0-85130-323-9.
* Thetford, Owen. ''British Naval Aircraft since 1912''. London: Putnam, 1978. ISBN 0-370-30021-1.
* {{cite book |last1=Sturtivant |first1=Ray |first2=Theo |last2=Ballance |title=The Squadrons of The Fleet Air Arm |location=Tonbridge, Kent |publisher=Air-Britain (Historians) |year=1994 |isbn=0-85130-223-8 |ref=harv}}
{{Refend}}

==External links==
{{commons category}}
* [http://www.thunder-and-lightnings.co.uk/scimitar/index.php Scimitar at Thunder and Lightnings]
* [http://www.flightglobal.com/pdfarchive/view/1956/1956%20-%200129.html "Supermarine's New Fighter - the Naval N.113"] a 1956 ''Flight'' article on the aircraft later to be named the "Scimitar"

{{Supermarine aircraft}}
{{British military aircraft since World War II}}

[[Category:Supermarine aircraft|Scimitar]]
[[Category:British fighter aircraft 1950–1959]]
[[Category:Twinjets]]
[[Category:Monoplanes]]
[[Category:Carrier-based aircraft]]