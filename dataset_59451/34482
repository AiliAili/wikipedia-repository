{{good article}}
{{infobox military unit
|unit_name= 1st SAS Brigade
|dates= 1942-1945
|country= {{flag|United Kingdom}}
|branch= {{army|United Kingdom}}
|type=[[Airborne forces]]
|role= Fictional unit, part of [[Operation Cascade]]
}}
The '''1st Special Air Service Brigade''' (1st SAS) was a fictional brigade during the [[World War II|Second World War]]. It was first formed in [[Cairo]] in 1941, as part of a deception by Brigadier [[Dudley Clarke]], to play on Italian fears of airborne attacks. Clarke used documents, photographs, news reports and even fake SAS soldiers to plant information about the [[brigade]] &ndash; he even named the Cairo-based deception department, 'A' Force, to bolster evidence of their existence.

In the summer of 1941, when [[David Stirling]] was pitching his idea for a dedicated airborne [[commando]] unit (which later evolved into the [[Special Air Service]]) he obtained Clarke's support partly by promising to use the SAS name. From late 1942, Clarke used the 1st SAS in his major order of battle deception (codenamed [[Operation Cascade]]). The brigade formed part of the fictional 4th Airborne Division; between then and the end of the war it was used to mislead [[Axis powers|Axis]] commanders about the strength of Allied forces in North Africa, and as part of several specific tactical deceptions.

==Formation==
In late 1940 the Allies recovered the journal of an Italian officer during an operation at [[Sidi Barrani]]. The diary referred to fears of British paratroopers being landed to the Italian rear. At the time the Allies had no airborne troops in the North African region. Dudley Clarke, in charge of [[military deception]] in the region, decided to play on these fears by creating a fictional airborne unit.<ref name=Rankin279/> Clarke began Operation Abeam in January 1941 by creating a paper trail for the 1st SAS Brigade. The fictional unit was supposedly training for special missions in [[Emirate of Transjordan|Transjordan]]. Clarke established their existence using documentary and physical subterfuge. Photographs of parachutists were printed in local papers, documents were planted with the enemy, Allied airmen were warned to look out for gliders ([[Victor Jones (colonel)|Victor Jones]] mocked up some dummies to support the story), and a section of desert was cordoned off for "training". To aid the rumours, two soldiers were dressed in 1st SAS uniforms and wandered around the Allied-held cities of Cairo, [[Port Said]], and [[Alexandria]], where they were briefed to hint at missions in [[Crete]] or [[Libya]].<ref name=HoltTrans/><ref name=Rankin279/>

By March, Clarke's deception operations had grown and it was decided to create a formal department. He chose the name ''Advanced Headquarters 'A' Force'' - the 'A' being a subtle reference to airborne - in the hope that it would help support the existence of paratroopers in the area (as well as hide the true purpose of the department).<ref name=Rankin279/><ref name=HoltAForce/> Clarke continued actively promoting the existence of 1st SAS until around June 1941, and it appears that Axis commanders accepted the brigade as real. Clarke had created the unit with no specific aims, although it did mean that the enemy command had to factor the existence of airborne troops into any battle plan. However, Abeam and the 1st SAS represented his first attempt at inflating the apparent strength of Allied forces, a tactic he would use significantly over the rest of the war.<ref name=HoltTrans/> The brigade occasionally featured in Clarke's tactical deceptions; such as a March 1941 threat to Axis supply lines near Tripoli.<ref name=Howard35/>

=== Stirling's SAS ===
{{main|History of the SAS}}
In May 1941, David Stirling, an injured British forces Commando in the [[North African campaign|North Africa theatre]], proposed the idea of small airborne [[special forces]] units to operate behind enemy lines. Whilst lobbying for support within [[Middle East Command]] it came to Clarke's attention, who offered his backing provided the force could also be used to help his own deception schemes.<ref name=Rankin279/><ref name=Molinari/>

{{quote|The name SAS came mainly from the fact I was anxious to get the full co-operation of a very ingenious individual called Dudley Clark[e], who was responsible for running a deception operation in Cairo... Clark[e] was quite an influential chap and promised to give me all the help he could if I would use the name of his bogus brigade of parachutists, which is the Special Air Service, the SAS|source=-- [[David Stirling]], 1985<ref name=Rankin279/>}}

Stirling's plan received approval in the summer of 1941 and the unit was designated "L" Detachment, Special Air Service. The name was intended to add further evidence of an airborne presence, specifically 1st SAS, in the region.<ref name=Rankin279/><ref name=Molinari/>

==4th Airborne==
Through 1941 and 1942, 1st SAS formed part of Clarke's overall strategy to deceive the Axis about the strength of Allied forces in the region. In early 1942, this ad hoc situation was formalised as Operation Cascade; an entire fictional [[order of battle]] featuring numerous units.<ref name=Holt4th/>

By late 1942, the Allies had begun to train a number of real airborne units in the North African theatre. Clarke created the fictional [[4th Airborne Division]] out of several units, both real and fictional, including 1st SAS. Under the umbrella of Cascade, the  aim was to mislead the Axis that paratroopers, most of whom were still training, posed a realistic threat. The 4th Airborne were utilised in a number of specific operational deceptions over the next few years (including [[Operation Zeppelin (Allies)|Operation Zeppelin]] and [[Operation Barclay]]), most often to threaten fictional invasions as a distraction from real Allied operations.<ref name=Holt4th/>

== References ==
{{Reflist|30em|refs=
<ref name=Rankin279>Rankin (2008), pg. 279–280</ref>
<ref name=HoltAForce>Holt (2004), pg. 26–30</ref>
<ref name=HoltTrans>Holt (2004), pg. 22–23</ref>
<ref name=Holt4th>Holt (2004), pg. 225–226</ref>
<ref name=Molinari>Molinari (2007), pg. 22</ref>
<ref name=Howard35>Howard (1990), pg. 35</ref>
}}

=== Bibliography ===
* {{cite book|author=Holt, Thaddeus|title=The Deceivers: Allied Military Deception in the Second World War|publisher=Scribner|year=2004|isbn=0-7432-5042-7|location=New York}}
* {{cite book|author=Howard, Michael|author2=Hinsley, Francis Harry|title=British Intelligence in the Second World War: Strategic Deception|publisher=[[Cambridge University Press]]|date=26 October 1990|isbn=0-521-40145-3|location=London}}
* {{cite book|author=Molinari, Andrea|title=Desert Raiders: Axis and Allied Special Forces 1940–43|publisher=Osprey Publishing|year=2007|isbn=978-1-84603-006-2|location=Oxford}}
* {{cite book|title=Churchill's Wizards: The British Genius for Deception, 1914–1945|author=Rankin, Nicholas|authorlink=Nicholas Rankin|date=1 October 2008|publisher=[[Faber and Faber]]|location=London|isbn=0-571-22195-5}}

{{Allied Military Deception in World War II}}

[[Category:Military units and formations established in 1941]]
[[Category:Fictional units of World War II]]