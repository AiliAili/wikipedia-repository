{{Infobox journal
| title = Pediatrics
| cover = 
| editor = [[Lewis First]]
| discipline = [[Pediatrics]]
| abbreviation = Pediatr.
| publisher = [[American Academy of Pediatrics]]
| country = United States
| frequency = Monthly
| history = 1948–present
| openaccess = 
| license =
| impact = 5.473
| impact-year = 2014
| website = 
| link1 = 
| link1-name = 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 0031-4005 
| eISSN = 1098-4275
}}
'''''Pediatrics''''' is a [[peer-reviewed]] [[medical journal]] published by the [[American Academy of Pediatrics]]. In the inaugural January 1948 issue, the journal's first [[editor-in-chief]], Hugh McCulloch, articulated the journal's vision: "The content of the journal is... intended to encompass the needs of the whole child in his physiologic, mental, emotional, and social structure. The single word, Pediatrics, has been chosen to indicate this catholic intent."

''Pediatrics'' has been continuously published by the [[American Academy of Pediatrics]] since January 1948. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.473, ranking it fourth out of 119 journals in the category "Pediatrics".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pediatrics |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.|chapter-url=http://admin-apps.webofknowledge.com/JCR/JCR?RQ=IF_CAT_BOXPLOT&rank=113&journal=PEDIATRICS}}</ref>

==Editors==
The following persons have been editor-in-chief of ''Pediatrics'':
*1948–1954 Hugh McCulloch
*1954–1961 [[Charles D. May]]
*1962–1974 [[Clement A. Smith]]
*1974–2009 [[Jerold F. Lucey]]
*2009–present [[Lewis First]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://pediatrics.aappublications.org}}

[[Category:Publications established in 1948]]
[[Category:Pediatrics journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]