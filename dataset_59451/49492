{{Advert|date=May 2011}}

{{Infobox software
| name                   = Managed Extensibility Framework
| logo                   = [[Image:Blocks.png|250px|MEF logo]]
| screenshot             =
| caption                = Part of [[.NET Framework]] 4.0
| developer              = [[Microsoft]]
| latest_release_version = V1 in .NET Framework 4.0
| latest_release_date    = {{Start date and age|2010|4|12|}}
| latest_preview_version = V2 Preview 5
| latest_preview_date    = {{start date and age|2011|12|19}}
| genre                  = [[Web application framework]]
| license                = [[Shared source|Ms-PL]]
| programming language   = [[.NET Languages]]
| website                = [http://mef.codeplex.com/ mef.codeplex.com]
}}
'''Managed Extensibility Framework''' ('''MEF''') is a component of [[.NET Framework]] 4.0 aiming to create lightweight, extensible applications. It aims to allow .NET application developers to discover and use extensions with no configuration required. It also aims to let extension developers encapsulate code easily and avoid fragile hard dependencies. Furthermore, it aims to allow extensions to be reused across applications. MEF was introduced as a part of .NET 4.0 and Silverlight 4. 

Unsupported, pre-release versions of MEF are also available on [[CodePlex]] along with source code<ref>{{Cite web|url = http://mef.codeplex.com/|title = Managed Extensibility Framework - Home|accessdate = 2014-04-30|website = Codeplex|publisher = Microsoft}}</ref> and can be used, albeit unsupported and with limitations, on version 3.5 of the framework{{Citation needed|date = April 2014}}.

==What problems does MEF aim to solve?==
MEF aims to solve the runtime extensibility problem. Without MEF, any application that wants to support a plugin model needs to create its own infrastructure from scratch. Those plugins will often be application-specific and cannot be reused across multiple implementations.

* MEF aims to provide a standard way for the host application to expose itself and consume external extensions. Extensions, by their nature, could be reused amongst different applications. However, an extension can still be implemented in a way that is application-specific. Extensions themselves can depend on one another and MEF aims to make sure they are wired together in the correct order, sparing the developer from doing it manually.
* MEF offers a set of discovery approaches for the application to locate and load available extensions.
* MEF allows tagging extensions with additional metadata which aims to facilitate rich querying and filtering.

==How does MEF work?==
Roughly speaking, MEF's core consists of a catalog and a CompositionContainer. A catalog is responsible for discovering extensions and the container coordinates creation and satisfies dependencies.
* MEF's first-class citizen is the ComposablePart class. A composable part offers up one or more Exports, and may also depend on one or more externally provided services or Imports. A composable part also manages an instance, which can be an object instance of a given type (it is in the default MEF implementation){{Clarify|date=November 2013}}. MEF, however, is extensible and additional ComposablePart implementations can be provided as long as they adhere to the Import/Export contracts.
* Exports and imports each have a Contract. Contracts are the bridge between exports and imports. An export contract can consist of further metadata that can be used to filter on its discovery. For example, it might indicate a specific capability that the export offers.
* MEF's container interacts with Catalogs to have access to composable parts. The container itself resolves a part's dependencies and exposes Exports to the outside world. Composable part instances may be added directly to the container.
* A ComposablePart returned by a catalog will likely be an extension to the application. It might have Imports (dependencies) on components the host application offers, and it's likely to Export others.
* The default MEF composable part implementation uses attribute-based metadata to declare exports and imports. This allows MEF to determine which parts, imports, and exports are available through discovery.

==References==
<references />

== External links ==
*[http://mef.codeplex.com MEF at CodePlex]
*[http://msdn.microsoft.com/en-us/library/dd460648(VS.100).aspx Managed Extensibility Framework Overview]
*[http://msdn.microsoft.com/en-us/magazine/ee291628.aspx Building Composable Apps in .NET 4 with the Managed Extensibility Framework]
*[http://buksbaum.us/2011/08/20/gentle-introduction-to-mefpart-one/ Gentle Introduction to MEF - Part One]
{{.NET Framework}}

[[Category:.NET Framework terminology]]
[[Category:C Sharp libraries]]
[[Category:Software using the MS-PL license]]