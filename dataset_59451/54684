{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = The Nursery “Alice”
| title_orig   = 
| translator   = 
| image        = Nursery-alice-1890.png
| caption      = First edition cover of ''The Nursery “Alice”''
| author       = [[Lewis Carroll]]
| cover_artist = [[E. Gertrude Thomson]]
| country      = United Kingdom
| language     = English
| series       = 
| genre        = [[Fantasy novel]]
| publisher    = [[Macmillan Publishers|Macmillan]]
| release_date = [[1890 in literature|1890]]
| media_type   = Print ([[Hardcover|Hardback]])
| pages        = 312 p.
| isbn         = <!-- N/A: First released before ISBN system implemented -->
| notes = Adaptations: [[Alice Through the Looking Glass|Alice in Wonderland: Nursery (film)]]
| followed_by  = 
}}

'''''The Nursery "Alice"''''' (1890) is a shortened version of ''[[Alice's Adventures in Wonderland]]'' (1865) by [[Lewis Carroll]] — pseudonym of Charles Lutwidge Dodgson (1832–1898) — adapted by the author himself for children ''"from nought to five"''. It includes 20 of [[John Tenniel]]'s illustrations from the original book coloured, enlarged and, in some cases, revised.<ref>[http://www.worldcat.org/title/nursery-alice/oclc/2571409/editions ''The Nursery "Alice"''] at [[WorldCat]]</ref>

It was first published in 1890 by [[Macmillan Publishers|Macmillan]], 25 years after the original ''Alice'', and featured a new illustrated cover by [[E. Gertrude Thomson]], who was a good friend of Dodgson.<ref>Morton N. Cohen and Edward Wakeling (2003), ''Lewis Carroll and his illustrators'', Macmillan, London,  pp.&nbsp;229–231</ref>

The work is not merely a shortened and simplified version, along the lines of ''[[Alice's Adventures in Wonderland retold in words of one syllable]]''. It is written as though the story is being read aloud by someone who is also talking to the child listener, with many interpolations by the author, pointing out details in the pictures and asking questions, such as "Which would ''you'' have liked the best, do you think, to be a little tiny Alice, no larger than a kitten, or a great tall Alice, with your head always knocking against the ceiling?"<ref>''The Nursery "Alice"'', Chapter II</ref> There are also additions, such as an anecdote about a puppy called Dash,<ref>''The Nursery "Alice"'', Chapter VI</ref> and an explanation of the word "foxglove".<ref>''The Nursery "Alice"'', Chapter IX</ref>

==References==
{{reflist}}

==External links==
*{{Commonscat_inline|The Nursery Alice (1890)|''The Nursery Alice''}}
*[http://www.aliang.net/literature/the_nursery_alice/ The complete text and illustrations of ''The Nursery Alice'' online]
{{Portal |Children's literature}}

{{Alice}}

{{DEFAULTSORT:Nursery Alice, The}}
[[Category:1890 books]]
[[Category:Alice in Wonderland]]
[[Category:Children's fiction books]]
[[Category:Works by Lewis Carroll]]
[[Category:Macmillan Publishers books]]


{{1890s-child-novel-stub}}