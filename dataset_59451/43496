[[File:Griffon58.JPG|thumb|upright=1.14|A preserved Rolls-Royce Griffon 58, one of the last Rolls-Royce piston engines to be produced. The red and white "dumb bell" object to the left of the engine is an air raid siren exhibit]]

[[Rolls-Royce Limited|Rolls-Royce]] produced a range of piston engine types for aircraft use in the first half of the 20th Century. Production of own design engines ceased in 1955 with the last versions of the Griffon, licensed production of [[Teledyne Continental Motors]] [[general aviation]] engines was carried out by the company in the 1960s and 1970s.

Examples of Rolls-Royce aircraft piston engine types remain airworthy today with many more on public display in museums.

==WWI==
In 1915, the [[Rolls-Royce Eagle|Eagle]], [[Rolls-Royce Falcon|Falcon]], and  [[Rolls-Royce Hawk|Hawk]] engines were developed in response to wartime needs. The Eagle was very successful, especially for bombers.  It was scaled down by a factor of 5:4 to make the Falcon or by deleting one bank of its V12 cylinders to make the Hawk. The smaller engines were intended for fighter aircraft. Subsequently, it was enlarged to make the [[Rolls-Royce Condor|Condor]] which saw use in [[airships]].<ref>Lumsden 2003, pp.183-190.</ref>

==Inter-war years==
[[File:RRKestrel.JPG|thumb|right|The Rolls-Royce Kestrel]] 
The [[Rolls-Royce Kestrel|Kestrel]] was a post-war redesign of the Eagle featuring wet cylinder liners in (two) common cylinder blocks.  It was developed into the [[supercharged]] [[Rolls-Royce Peregrine|Peregrine]] and later the [[Rolls-Royce Goshawk|Goshawk]].<ref>Lumsden 2003, pp.190-198.</ref>

Developed concurrently with the Kestrel was the unusual [[Rolls-Royce Eagle XVI]] [[X engine]] that was cancelled in favour of the Kestrel despite performing well on the test stand.

The [[Rolls-Royce Buzzard|Buzzard]] was an enlargement of the Kestrel <ref>Lumsden 2003, p.198.</ref> of Condor size, developed in its most extreme form into the [[Rolls-Royce R]] racing engine used for the [[Schneider Trophy]] competition.<ref>Lumsden 2003, p.199.</ref>

The [[Rolls-Royce Vulture|Vulture]] of 1939 was essentially two Peregrines on a common crankshaft in an X-24 configuration, both of these types being deemed unsuccessful.<ref>Lumsden 2003, p.200.</ref>

==WWII and beyond==
The [[Rolls-Royce Merlin]], and later the development of the Buzzard, the [[Rolls-Royce Griffon]] were the two most successful designs for Rolls-Royce to serve in the [[Second World War]], the Merlin powering [[RAF]] fighters the [[Hawker Hurricane]], [[Supermarine Spitfire]], fighter/bomber [[de Havilland Mosquito]], [[Avro Lancaster|Lancaster]] and [[Handley Page Halifax|Halifax]] heavy bombers and also [[Allies of World War II|allied]] aircraft such as the American [[North American P-51 Mustang|P-51 Mustang]] and some marks of [[Curtiss P-40 Warhawk|Kittyhawk]].

Experimental engines were developed as alternatives for high performance aircraft such as the H-24 configuration [[Rolls-Royce Eagle 22]],<ref>Lumsden 2003, p.221.</ref> the [[Two-stroke engine|two-stroke]] [[Rolls-Royce Crecy]]<ref>Nahum, Foster-Pegg, Birch 2004.</ref> and the [[Rolls-Royce Pennine]]<ref>Rubbra 1990, p.148.</ref> and [[Rolls-Royce Exe]], the Exe being the only one of these last three engines to fly.<ref>Lumsden 2003, p.201.</ref>   However the successful development of the Merlin and Griffon, and the introduction of [[jet engines]] precluded significant production of these types.

Production of Rolls-Royce designed aircraft piston engines ceased in 1955 with the last variants of the Griffon.<ref>Lumsden 2003, p.218.</ref> Between 1961 and 1981 Rolls-Royce was licensed to build the [[Teledyne Continental Motors|Teledyne Continental]] range of light aircraft piston engines including the [[Continental O-520]].<ref>Gunston 1989, p.42.</ref>

==Survivors==
As of 2011 examples of the Falcon, Griffon, Kestrel and Merlin remain airworthy.<ref>See individual articles for details</ref>

==Engines on display==
Various types of Rolls-Royce aircraft piston engines are on public display at the following museums:
*[[Fleet Air Arm Museum]]
*[[Imperial War Museum Duxford]]
*[[Lone Star Flight Museum]]
*[[Midland Air Museum]]
*[[Royal Air Force Museum Cosford]]
*[[Royal Air Force Museum London]]
*[[Science Museum (London)]]
*[[Shuttleworth Collection]]

==Chronological list==
<ref>By first run date</ref>
[[File:Rolls-Royce Eagle VIII.jpg|thumb|right|1915 Rolls-Royce Eagle V-12]]
[[File:RollsRoyce Merlin 23.jpg|thumb|right|The Rolls-Royce Merlin]] 
* [[Rolls-Royce Eagle|Rolls-Royce Eagle (V-12)]]
* [[Rolls-Royce Hawk]]
* [[Rolls-Royce Falcon]]
* [[Rolls-Royce Condor]]
* [[Rolls-Royce Eagle XVI|Rolls-Royce Eagle XVI (X-16)]]
* [[Rolls-Royce Kestrel]]
* [[Rolls-Royce Buzzard]]
* [[Rolls-Royce Goshawk]]
* [[Rolls-Royce R]]
* [[Rolls-Royce Peregrine]]
* [[Rolls-Royce Merlin]]
* [[Rolls-Royce Exe]]
* [[Rolls-Royce Vulture]]
* [[Rolls-Royce Crecy]]
* [[Rolls-Royce Griffon]]
* [[Rolls-Royce Eagle (1944)|Rolls Royce Eagle (H-24)]]
* [[Rolls-Royce Pennine]]

==See also==
{{Aircontent
|related=

|similar engines=
|lists=
* [[List of aircraft engines]]
|see also=
*[[Ernest Hives, 1st Baron Hives|Ernest Hives]]
*[[Stanley Hooker]]
*[[Cyril Lovesey]]
*[[Arthur Rowledge]]
*[[Arthur Rubbra]]
*[[Packard V-1650]]
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
* Nahum, A., Foster-Pegg, R.W., Birch, D. ''The Rolls-Royce Crecy'', Rolls-Royce Heritage Trust. Derby, England. 1994 ISBN 1-872922-05-8
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Rubbra, A.A. ''Rolls-Royce Piston Aero Engines - a designer remembers: Historical Series no 16'' :Rolls Royce Heritage Trust, 1990. ISBN 1-872922-00-7
{{refend}}

==Further reading==
* [[Bill Gunston]], ''Development of Piston Aero Engines''. Cambridge, England. Patrick Stephens Limited, 2006. ISBN 0-7509-4478-1
* Bill Gunston, ''Rolls-Royce Aero Engines'', Patrick Stephens Limited (Haynes Group) ISBN 1-85260-037-3
* Sir [[Stanley Hooker]], ''Not Much of an Engineer'', Airlife Publishing, ISBN 0-906393-35-3
* Pugh, Peter. ''The Magic of a Name - The Rolls-Royce Story - The First 40 Years''. Cambridge, England. Icon Books Ltd, 2000. ISBN 1-84046-151-9

==External links==
{{commons category|Rolls-Royce piston aircraft engines}}
*[http://www.rolls-royce.com/about/heritage/heritage_trust/ Rolls-Royce Heritage Trust]
*[http://www.flightglobal.com/pdfarchive/view/1939/1939-1-%20-%200297.html "From Eagle to Merlin"] a 1939 ''Flight'' article

{{RRaeroengines}}

[[Category:Rolls-Royce aircraft piston engines| ]]