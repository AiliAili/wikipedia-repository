{{BLP sources|date=November 2013}}

'''Arthur Maud'''  (born December 29, 1932) Anglo-American composer, conductor, [[musicologist]].

==Education==
Born and raised in [[Airedale]], the [[West Riding of Yorkshire]], England, Maud studied singing at an early age and was [[chorister]] and
soloist in local churches until the family emigrated to the USA in 1948.  He has studied composition in [[Germany]] ([[Hochschule für Musik]],
[[Munich]] 1956-7)) and America with [[Dominic Argento]], Paul Fetler, [[Harald Genzmer]], [[Carl Orff]], [[Leroy Robertson]], [[Leo Sowerby]] and others.  A '[[Christmas cantata|Christmas Cantata]]' on Crashaw's 'Song on the Birth of our Lord' was written for the MA at the [[University of Utah]] in 1960; and 'Sinfonia Concertante for Organ and Orchestra' for the PhD at the [[University of Minnesota]] in 1977.

==Performance==
As Director of Concentus Musicus Minnesota he prepared and directed more than twenty years of concert seasons for its vocal and early
instrumental ensembles and produced the following recordings containing major works by 15th and 16th century composers:

*[[Tomás Luis de Victoria|Tomas Luis de Victoria]] (‘Spanish Music of the Golden Age’ CMLP 01, 1970)
*[[Jean Mouton]] (‘The Field of the Cloth of Gold’ CMLP 02, 1972, collaboration with William Pohl)
*[[Heinrich Finck]] (‘Music from the Royal Court of Cracow’ CMLP 03, 1975)
*[[Josquin Desprez]] ‘Missa de Beata Virgine’ (CMLP 04, 1977)
*[[Robert Fayrfax]] “Magnificat O bone Jesu” & [[John Taverner]] ‘Missa Sancti Wilhelmi devocio’ (CMLP 05, 1980)
*Wilhelmi devocio’ (CMLP 05, 1980)
*[[William Byrd]] (‘Nowell Sing We’—Musical Heritage Society MHC 9436, 1986)
*[[Jacob Handl]] (‘From Bohemia’s Castles and Chapels’—Meridian Records CDE 84309, 1996)
*[[Francisco peñalosa|Francisco Peñalosa]] (‘Court and Cathedral’—Meridian Records CDE 84406, 2000),

In addition to the Season Concert Series, Concentus Musicus presented hundreds of concerts and workshops throughout the Upper Midwest for colleges, universities, churches, concert societies, and professional associations.  The last of these concerts--'Music from Papal Avignon'.--was presented to the National Conference of the [[Medieval Academy of America]] held in [[Minneapolis]], April 2003.

==Compositions==
Beginning in 1995 an association with [[St John on Bethnal Green]] Festival, [[London]], resulted in composition of six secular song sets for solo/choir/strings and keyboard:

*''Of Life, Love and Our Learned Old Vicar'' (1996)
*''Three sonnets of Jones Very''  (1997) ECS Publishing, Boston, Cat. No. 5425-7
*''A Sir John Soane Triptych'' (1998)
*''From Ann with Love'' (1999)
*''Voices of Bethnal Green'' (2000)
*''A Toy Trilogy'' (2001)

Other works composed for this venue include:
*"Humoresque"—string quintet (1998)
*"Love and Age"—piano, flute and alto (1995)
*"A prayer for unity"—tenor solo, choir and keyboard (2004) . . . and several other anthems.

Most of Maud’s religious compositions were written for St Clement's Episcopal Church, [[St. Paul, Minnesota|St Paul, Minnesota]], they include:
*Two orchestral masses--'Missa Sancti Clementi' (1995) and 'Missa de Requiem' (1997)
*Service music--'St Clement’s Vespers' (1985) and 'Rite II setting of the Eucharist' (1981)
*Two carols for choir--”From a distant home” and “Ave plena gracia” (ECS Publishing Cat. No. 5869 in 2003 and recorded by Bruce Barber and the choir of St James' Cathedral, [[Chicago]]). . . . and a dozen anthems. ‘Missa Sancti Clementi’ and ‘St Clement’s Vespers’ were included in an ‘Evensong and Concert of Music by Arthur Maud’, October 1996, as one of the Cathedral Concert Series at St Mark’s Cathedral, Minneapolis. In 2001 the [[Schubert Club]] commissioned  “In te Domine speravi” setting a new translation of Psalm 31 (Vulgate) by Christopher Brunelle and Joyce Sutphen and premiered by the [[Rose Ensemble]].

Chamber works include:
*''Canonic variations on a theme partly by [[Harald Genzmer]]'' performed by the Utah Symphonic Trio at USC, Los Angeles, CA (1960)
*''Parade’s End'', wind quintet, commissioned and performed by the Sylmar Ensemble, Minneapolis, MN (1986)
*''Sonata for Viola and Piano'' composed for violist Helen Kirklin and pianist Roberta Swedien (2013)

A sole work for the stage, 'Masque of the Resurrection', was performed at the University Episcopal Center, University of Minnesota, 1966

For the Concentus Musicus Minnesota programs Maud made over fifty arrangements of dances and Minnesinger and troubadour songs, and transcribed many polyphonic works from manuscript. A web site is being assembled where much of this recorded material can be accessed.

==Teachings==
All his working life Dr. Maud has been involved with music education, teaching music theory and history and directing choirs in college and university and in churches.  He introduced the Orff 'Schulwerk' to the [[Twin Cities]] in 1963 with Project IV—a school of music and the arts for children ages 4–14.  He has presented workshops in [[Renaissance music]] performance in America and Europe, including 2 weeks in [[Latvia]] in 2005 with Mara Marnauza at Rigas Pedagogias Augstskola and with Ilze Valce at the [[Liepāja]] Academy of Pedagogy funded by the [[U.S. State Department]].

==References==
{{Reflist}}

==External links==
*[http://www.schubert.org/commissions/ The Schubert Club] 
*[http://cla.umn.edu/alumni/ana.php University of Minnesota distinguished alumni] 
*[http://www.amazon.com/Music-From-Royal-Court-Cracow/ Amazon Music-From-Royal-Court-Cracow] 
*[http://minnesota.publicradio.org/radio/programs/new_releases/ Minnesota Public Radio] 
*[http://journals.cambridge.org/article_S0038713400132816 Program of Medieval Academy 2003] 
*[http://conservancy.umn.edu/bitstream/49909/1/lehmbergStanford.pdf More from the board chairman]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} 
*[http://www.ecspublishing.com ECS Publishing] 
*[http://www.meridian-records.co.uk/.../Online_Catalogue_CDE_84309_ Meridian Records]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
*[https://web.archive.org/web/20081118195542/http://www.kathrynwhitney.net/premieres.html Kathryn Whitney] Soloist at several of the Bethnal Green premieres
*[http://www.saintjamescathedral.org/download_file/view/346/ St James Cathedral] Concert performance of "Ave plena"
*[http://www.rpiva.lv/index.php?mh=balta www.rpiva.lv] Guest conductor in Riga
*[http://www.katedrale.lv/index.php?id=3776 www.katedrale.lv] Guest conductor in Liepaja

{{DEFAULTSORT:Maud, Arthur}}
[[Category:1932 births]]
[[Category:Living people]]
[[Category:People from West Yorkshire]]
[[Category:English emigrants to the United States]]
[[Category:American conductors (music)]]
[[Category:American male composers]]
[[Category:American musicologists]]