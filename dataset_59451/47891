{{Infobox artist
| name          = Irving Fierstein
| image         = Irving_Fierstein,_New_York_City,_1970.jpg
| imagesize     = 220px
| caption       = Irving Fierstein, 1970
| birth_name     =
| birth_date     = {{birth date|1915|1|11|df=y}}
| birth_place      = New York City, United States
| death_date     = {{death date and age|2009|5|25|1915|1|11|df=y}}
| death_place    = New York City, United States, United States
| field         = [[Painting]], [[printmaking]]
| training      =
| movement      = [[Cubism]], [[Impressionism]]
}}
'''Irving Fierstein''' (January 11, 1915 - May 25, 2009) Brooklyn-born artist whose work spanned over half a century was the son of Romanian and Polish Jewish immigrant parents and raised on New York City’s lower east side. In his lifetime Fierstein created a prolific body of fine artworks including oils, acrylics, lithographs, etchings and mixed medium reflecting impressionist, cubist, and expressionist schools, many dedicated to themes about social justice.

Fierstein began his studies of art and architecture at the [[Hebrew Technical Institute (New York City)]] from which he graduated in 1932. He also studied at the [[National Academy of Design]] where he was awarded the top medal in 1937, and later at [[Cooper Union]] where he also learned commercial art and lettering.<ref>Records & Briefs New York State Appellate Division, 36 AD 2nd, Vol. 286, ,1969, pp. A-186-187</ref>

One of his earliest projects was working with painter [[Rockwell Kent]] in 1938 on a Times Square (New York City) billboard in support of the Spanish Civil War freedom fighters against fascism.<ref>''Workers World,'' January 10, 2002, “65 years as a people’s artist – Celebrating Irving Fierstein” by Sue Davis (http://www.workers.org/2009/us/irving_fierstein_0618/); Leaflet “Celebrate Irving Fierstein’s 65 Years as a People’s Artist” for retrospective one-artist show, December 18, 2001, International Action Center, New York, New York</ref> His 1969 oil on canvas depicting the 1963 beating of African-American civil rights activist [[Fanny Lou Hamer]] in a Winona, Mississippi jail was presented to the Martin Luther King, Jr. Center for Social Change in Atlanta in 1977.<ref>''Amsterdam News'' (New York), August 20, 1977, p. D-13</ref><ref>Letter to Irving Fierstein from the Martin Luther King, Jr. Center for Social Change in Atlanta, July 19, 1977 on behalf of Coretta Scott King by Administrative Secretary Delores A. Harmon</ref><ref>''Chelsea Clinton News'' (New York), July 14, 1977, p. 8</ref> Fierstein had been deeply moved by the treatment of Hamer by the segregationist authorities and was inspired to undertake the painting while studying at the [[Art Students League]] with impressionist portrait painter [[Sidney Dickinson]] (1890 - 1980). This painting was featured in his first one-artist show at the Lynn Kottler Galleries in New York City in December 1970.<ref>''Daily News'' (New York), Night Owl Reporter column, “The Aspect of Pain”, December 19, 1970</ref><ref>''The Brooklyn Record'', November 6, 1970</ref><ref>''The Daily News'', Tarrytown, NY, December 23, 1970</ref><ref>''The Citizen Register'', Ossining, NY, December 22, 1970</ref><ref>''Croton-Cortlandt News'', Croton-on-Hudson, NY, December 17, 1970</ref><ref>''The United Teacher'', New York, New York, December 20, 1970</ref>

==Background==

[[File:"Ballerina Contemplating" Oil on Canvas by Irving Fierstein, 1970.jpg|thumb|right|''"Ballerina Contemplating", 30"x54" Oil on Canvas by Irving Fierstein, 1970'']]

From early on in his life, Fierstein veered toward art and a non-conforming social consciousness and activism.<ref>Speech by Irving Fierstein, December 18, 2001, Art Exhibition retrospective, International Action Center, New York, New York</ref> As early as 1937 while working as an advertising artist for Hearn’s Department Store in New York City, he helped to organize the Commercial Artists and Designers Union. After his marriage to [[Hannah Tompkins (artist)]] in 1940, the two worked to organize a Greenwich Village (NYC) branch of the American Labor Party.

During World War II Fierstein was a sergeant in the Army Air Corps during which he worked maintenance and instruction on the mechanical and electrical systems of the Boeing B-29 heavy bombardment airplanes. He was awarded the American Campaign Medal, [[World War II Victory Medal (United States)]].<ref>Army of the United States, Separation papers, Vol. 181, p. 493, December 11, 1945: Honorable Discharge, Irving Fierstein, Sergeant 244th Army Air Force Base Unit, Mitchell Field, New York</ref>

In the years after the war by 1948 Irving and Hannah Fierstein joined with the families of 9 other artists and architects to establish the community of Harmon Park in [[Croton-on-Hudson, New York]] in Westchester County just north of Manhattan. Here they built a home and raised a family of four children.<ref>''New York Star,'' October 24, 1948, p. 24 “Artists Build Homes at Savings by Cooperating on Land, Architect”</ref>

In the early 1950s Fierstein established Irv Fierstein Art, a commercial and advertising art studio on Fifth Avenue in New York City, later called Art Dimensions. He continued to make his living as graphic designer and art director until he retired in 1980. He and his wife divorced in 1964. He would remarry and divorce once more.

In July 1965 He participated in a call by Artists and Writers of Dissent directed to U.S. Ambassador to the United Nations Adlai Stevenson demanding an end to U.S. involvement in Vietnam and the Dominican Republic.<ref>Report from the Bureau of Special Services, July 1, 1965 “Artists and Writers dissent picket demonstration at the United States Mission to the United Nations” (Freedom of Information Act)</ref> He continued his activism in the progressive and socialist movements until his death at age 94 at his home in New York City’s uptown Washington Heights neighborhood.<ref>Death Transcript, Irving Fierstein, New York City Department of Health, May 2009, No. 156-09-021406</ref>

In 1990 Fierstein participated as a race-walker and successfully completed the 26 mile [[New York City Marathon]] at age 75.<ref>''New York Post,'' November 6, 1990, p. A-69, 1990 New York City Marathon results</ref><ref>''New York Running News,'' December–January 1991, p. 130</ref>

==The “People’s Artist”==

In addition to his fine art and commercial art achievements, Fierstein’s contributions to the social justice and anti-war movements included political cartoons and designing/illustrating leaflets, posters, buttons, placards, and banners.  Starting in 1981 he pioneered the creation of painted illustrated banners to be used as visual inspiration in progressive marches and demonstrations.<ref>''The Boston Globe,'' January 10, 1981, Front page, “Haig says US vital interests could require use of N-arms” (photo of banner “General Haig’s Military Industrial Complex”)</ref><ref>''Chicago Sun-Times,'' January 10, 1981, page 5</ref><ref>''Finger Lakes Times'' (Geneva, NY), January 10, 1981, Front page</ref><ref>''Time'' magazine, April 2, 1982, p. 14, “Trying to Be Mr. Nice Guy – In a second-year slump, Reagan softens his rhetoric” (photo of banner “Feed the People, Not the Pentagon”)</ref> In 1982 he created an illustrated a street-wide banner for the New York Lesbian and Gay Pride March (now LGBTQ) that was carried in the parade for many years after.<ref>''New York Times,'' June 18, 1983, (photo of banner “Fight Lesbian and Gay Oppression”)</ref>

[[File:FreeSouthAfricaButtonbyIrvFierstein.jpg|thumb|upright|left|''Free South Africa Button designed by Irv Fierstein'', 1985]]

Responding to the mushrooming anti-Apartheid movement in South Africa and derived from a 1984-85 New Years greeting card he created, Fierstein designed and illustrated a button, banner, placard, and T-shirt depicted two fists breaking a chain with the slogan “Free South Africa”. This design was not only used in anti-apartheid marches in the United States, but the image came to be used the world over and to symbolize the struggle in South Africa. The button appeared on the collar of white South African anti-apartheid writer, journalist, and filmmaker [[Rian Malan]] when he was featured on Esquire magazine’s cover in November 1985.<ref>''Esquire'' magazine, November 1985, Cover, “My Father, My Country- A powerful story of divided loyalty” by Rian Malan</ref>  Actor [[Laurence Fishburne]] wore the T-shirt with the image in Spike Lee’s 1988 movie “School Daze”,<ref>“School Daze”, 1988, written and directed by Spike Lee (http://www.imdb.com/title/tt0096054/)</ref> and a clip of the banner painted by Fierstein which was carried in an August 1985 New York anti-Apartheid march and elsewhere was shown in the 2012 Documentary “Sing Your Song” celebrating the life of singer, actor, and activist [[Harry Belafonte]],<ref>“Sing Your Song”, 2012 (Australia), directed by Susanne Rostock (http://www.imdb.com/title/tt1787797/)</ref>  as well as in other national and international media.<ref>''Daily News'' (New York), August 14, 1985, Front page</ref><ref>''The Washington Post'', January 22, 1985</ref><ref>''Daily News'' (New York), June 15, 1986, p. 3</ref>

Fierstein created over 200 illustrated banners carried in street marches and rallies in cities nationally addressing issues such as racism, war, and economic injustice, to name just a few: “Money for Jobs! Not Murder in El Salvador!”;<ref>''Workers World'', January 11, 1982</ref> Stop U.S./Israeli Terror against the Palestinian and Lebanese People!”; “Schools, Not War”;<ref>''Columbia Spectator'' (New York), March 24, 1982, Front page article, “Award inspires huge anti-Reagan protest”</ref>  “Women Fight Back – for jobs, equality, human needs and reproductive rights”;<ref>''Workers World'', March 5, 1983, Front page</ref> “Stop All Evictions”;<ref>''The Tenant,'' June 1984, article “Housing for people not profit”</ref> “U.S. Out of Central America, Caribbean, Africa, Mideast”;<ref>''The Boston Globe,'' January 22, 1985, article: “Cold no deterrent to protest marchers”</ref> Stop Racist Terror Against the Black Community – Jail Goetz and Killer Cops”;<ref>''Workers World,'' February 14, 1985, caption: “Anti-racist protest in New York City charges, ‘Goetz no hero!’”</ref> “No Fare Hike – Good for Bankers’ Profits”.<ref>''Daily News'' (New York), November 26, 1991, caption: “Banner of disapproval is unfurled yesterday a transit-fare hearing”</ref><ref>''The Queens Tribune,'' November 28-December 4, 1991, caption: “Queens protesters say: No Fare!”</ref>

==Exhibitions==

A partial list of his exhibitions includes:

* 1970 - Lynn Kottler Galleries, Irving Fierstein: One artist show, New York, New York<ref>Catalogue, Lynn Kottler Galleries, Irving Fierstein, December 13–26, 1972, NY, New York</ref>
* 1972 - Knickerbocker Artist Exhibition, National Arts Club, 22nd Annual Exhibition, Group show, New York, New York<ref>Catalogue, Knickerbocker Artists, 22rd Annual Exhibition, National Arts Club, April 6th – April 20, 1972, NY, New York</ref>
* 1973 - Knickerbocker Artists Exhibition, National Arts Club, 23rd Annual Exhibition, Group show, New York, New York<ref>Catalogue, Knickerbocker Artists, 23rd Annual Exhibition, National Arts Club, April 5th – April 19, 1973, NY, New York</ref>
* 1973 – National Academy Galleries, Allied Artists of America, 16th Annual Exhibition, Group show, New York, New York<ref>Catalogue, Allied Artists of America, 16th Annual Exhibition, National Academy Galleries, October 25 – November 11, 1973, NY, New York</ref>
* 1998 - New York Public Library, Fort Washington Branch, “Contemporary Lithographs and Etchings by Irving Fierstein”, New York, NY<ref>WBAI Radio Bulletin Board, March 30, 1998, New York, New York</ref>
* 2000 – International Action Center, The Iraq Exhibit, with eyewitness photographs by Bill Hackwell and Sara Flounders, New York, New York<ref>''Workers World,'' February 3, 2000, “Canvas captures cruelty of sanctions” by Deirdre Griswold</ref>
* 2001 - International Action Center, “Celebrating Irving Fierstein’s 65 years as a people’s artist”, New York, New York<ref>''Workers World,'' January 10, 2002, “65 years as a people’s artist – Celebrating Irving Fierstein” by Sue Davis</ref>
* 2002 - New York Public Library, Fort Washington Branch, “Impressionist Paintings of Washington Heights and Central Park by Irving Fierstein”, New York, New York
* 2003 - New York Public Library, Fort Washington Branch, “Paintings with Social Themes by Irving Fierstein”, New York, New York

==References==
{{Reflist|30em}}

==External links==
* [http://irving-fierstein.com/Banners/Banners01.html The Political Banners of Irving Fierstein]
* [http://irving-fierstein.com/ Irving Fierstein memorial site]
* [http://africanactivist.msu.edu/image.php?objectid=32-131-449  Free South Africa Button designed by Irv Fierstein]
* [http://africanactivist.msu.edu/image.php?objectid=32-131-100  An alternate South Africa Button designed by Irv Fierstein]

{{DEFAULTSORT:Fierstein, Irving}}
[[Category:1915 births]]
[[Category:2009 deaths]]
[[Category:American people of Romanian-Jewish descent]]
[[Category:20th-century American painters]]
[[Category:American male painters]]