{{Orphan|date=June 2012}}

{{Infobox person
| birth_name  = 
| birth_place = [[Austin]], [[Texas]], [[United States]]
| image       = 20th Annual NAACP Nominee.jpg<!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt         = 
| caption     = Regina Louise
| known_for   = Children's rights
| occupation  = Author
}}
'''Regina Louise''' (born May 2, 1963) is an American [[author]], child [[advocate]], and motivational [[Public speaker|speaker]], who is best known for successfully navigating through more than thirty [[foster home]] placements as a ward of the [[California Division of Juvenile Justice|California Juvenile Court]] system. Louise is the second child of the late singer/songwriter [[Tom Brock (singer)|Tom Brock]]. Brock abandoned his daughter into the [[foster care]] system during the late-1970s to pursue musical interests which included writing and arranging hit songs for [[Barry White]], [[Love Unlimited]] and [[Gloria Scott (singer)|Gloria Scott]].

==Biography==
Regina Louise was born in Austin, Texas on May 2. She attended Molly Dawson elementary school and left at the age of eleven. Due to her father's [[Wiktionary:estrangement|estrangement]], Regina turned herself in to the [[Richmond Police Department (California)|Richmond Police Department]] and was taken into custody on May 1, 1975, the day before her thirteenth birthday. She is well known for her work as a [[foster care]] abolitionist.

She lived in over 30 [[foster homes]], [[group homes]] and [[psychiatric]] facilities before age 18. After missing many years of formal education and being labeled "below-average or marginal at best", she is a now speaker on children's issues, a National Child Permanency Advocate and a spokesperson for Foster Care Awareness Month. She currently resides in the [[San Francisco Bay Area]], where she hopes to finish a college degree she started more than thirty years ago.

Author of the self-memoir ''Somebody's Someone'', she has appeared on [[NPR|National Public Radio's]] [[All Things Considered]]<ref name="npr">[http://www.npr.org/templates/story/story.php?storyId=1576773 "Women Reunited Decades After Separation"] [[All Things Considered]], 30 December 2003. Retrieved 05 January 2012.</ref> and the [[CBS Early Show]]. Regina and her story have also been covered in various newspapers and magazines.<ref name="sfgate">[http://www.sfgate.com/cgi-bin/article.cgi?file=/chronicle/archive/2004/04/25/MNGI56ASTU1.DTL "An orphan no more -- foster kid finds mom"] [[San Francisco Chronicle]], San Francisco, 25 April 2004. Retrieved 05 January 2012.</ref><ref>[http://articles.chicagotribune.com/2006-01-03/news/0601030252_1_children-with-black-families-foster-shelter "30 years later, adoption dream granted"][[Chicago Tribune]],Los Angeles, 03 January 2006. Retrieved 05 January 2012</ref><ref>[http://www.diablomag.com/Diablo-Magazine/May-2006/Fostering-Foster-Care/ "Fostering Foster Care"] [[Diablo Magazine]], May 2006. Retrieved 05 January 2012.</ref> Her second memoir ''Someone Has Led This Child to Believe: A Case History of Love, Luck & Self-Determination'' was self-published in 2016.

According to Regina,<ref name="regina">[http://www.reginalouise.com/reginalouise.htm "Regina Louise"] Retrieved 05 January 2012.</ref> she devotes her life to helping others as a speaker attempting to raise awareness about the plight of foster children. In seven short years, Regina Louise has delivered her message of hope, optimism and inspiration to well over four million people. Her goal is to speak to every state in the nation on the significance of all children finding life-time connections.<ref name="regina" />

== References ==
{{Reflist}}

==External links==
{{official website|http://www.thereginalouisefoundation.org/}}

{{Authority control}}

{{DEFAULTSORT:Louise, Regina}}
<!--- Categories --->
[[Category:1963 births]]
[[Category:Living people]]
[[Category:Articles created via the Article Wizard]]
[[Category:American motivational speakers]]
[[Category:American women activists]]
[[Category:African-American activists]]
[[Category:African-American women writers]]
[[Category:African-American writers]]
[[Category:American women writers]]
[[Category:Writers from Austin, Texas]]
[[Category:21st-century women writers]]
[[Category:20th-century women writers]]