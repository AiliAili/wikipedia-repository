{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:USS Indiana (BB-1) - NH 73975.jpg|300px|alt=A battleship with a white hull is steaming at sea, men crowd its main deck]]
| Ship caption = USS ''Indiana''&nbsp;– the lead ship of the class
}}
{{Infobox ship class overview
| Builders =
* William Cramp & Sons ({{USS|Indiana|BB-1|2}} and {{USS|Massachusetts|BB-2|2}})
* Union Iron Works ({{USS|Oregon|BB-3|2}}){{sfn|Reilly|Scheina|1980|p=51}}
  
| Operators    = [[File:US flag 46 stars.svg|20px]] United States Navy
| Class before = {{USS|Maine|ACR-1|6}} and {{USS|Texas|1892|6}}
| Class after  = {{USS|Iowa|BB-4|6}}
| Subclasses   = 
| Cost =
* [[United States dollar|$]]6,000,000 ''Indiana'' (BB-1) and 
* ''Massachusetts'' (BB-2){{sfn|Reilly|Scheina|1980|p=69}}
* $6,500,000 ''Oregon'' (BB-3){{sfn|Reilly|Scheina|1980|p=69}}
  
| Built range           = 
| In commission range   = 20 November 1895&nbsp;– 4 October 1919
| Total ships building  = 
| Total ships planned   = 3
| Total ships completed = 3
| Total ships retired   = 3
| Total ships preserved = 0
}}
{{Infobox ship characteristics
| Hide header       = 
| Header caption    = {{sfn|Reilly|Scheina|1980|p=68}} {{sfn|Chesneau|Koleśnik|Campbell|1979|p=140}}{{sfn|Friedman|1985|p=425}}{{sfn|Bryan|1901}}
| Ship type         = [[Pre-dreadnought battleship]]
| Ship displacement = {{cvt|10288|LT|t ST|lk=on}} (standard)
| Ship length       =
* {{cvt|350|ft|11|in|lk=on}} ([[overall length|overall]])
* {{cvt|358|ft}} ([[Waterline length|waterline]])
| Ship beam         = {{cvt|69|ft|3|in}} (wl)
| Ship draft        = {{cvt|27|ft}}
| Ship power =
* 4 × [[Scotch marine boiler#Double-ended|double ended Scotch boilers]] later replaced by 8 × [[Babcock & Wilcox boiler]]s
* {{cvt|9000|ihp|MW|lk=in}} (design)
* {{cvt|9700|-|11000|ihp|MW}} (trial)
| Ship propulsion   =
* 2 sets vertical inverted [[triple expansion]] [[reciprocating steam engine]]s
* 2 × [[Propeller|screws]]   
| Ship speed =
* {{cvt|15|kn}} (design)
* {{cvt|15.6|-|16.8|kn}} (trial)
| Ship range =
* {{cvt|4900|nmi|-2}} (BB-1 & 2){{efn|name=range}}
* {{cvt|5600|nmi|-2}} (BB-3){{efn|name=range}}
  
| Ship complement = 32 officers 441 men
| Ship armament =
*  4 × [[13"/35 caliber gun|{{cvt|13|in|0}}/35 caliber guns]] (2x2)
*  8 × [[8"/35 caliber gun|{{cvt|8|in|0}}/35 cal guns]] (4x2)
*  4 × [[6"/40 caliber gun|{{cvt|6|in|0}}/40 cal guns]]
* 20 × [[QF 6 pounder Hotchkiss#American service|6-pounder {{cvt|57|mm|1}} guns]] 
*  6 × [[QF 1-pounder pom-pom#United States|1-pounder {{cvt|37|mm|1}} guns]]
*  6 × [[British 18 inch torpedo|{{cvt|18|in|0}} Whitehead]] [[torpedo tubes]]{{efn|name=torpedo}}
| Ship armor =* [[Harvey armor|Harvey]] and [[Nickel-steel|NS]]
* [[Belt armor|Belt]]: {{cvt|4|-|18|in|0}}
* [[Gun turret#Warships|Turrets]] (main): {{cvt|15|in|0}}
* [[Barbette]]s (main): {{cvt|17|in|0}}
* Turrets (secondary): {{cvt|5|-|8|in|0}}
* Barbettes (secondary): 8-in
* [[Conning tower]]: {{cvt|9|in|0}}
| Ship notes = 
}}
|}

The three '''''Indiana''-class battleships''' were the first [[battleship]]s to be built by the [[United States Navy]] that were comparable to contemporary European ships, such as the British {{HMS|Hood|1891|6}}. Authorized in 1890 and commissioned between November 1895 and April 1896, they were relatively small battleships with heavy armor and ordnance that pioneered the use of an [[Pre-dreadnought battleship#Armament|intermediate battery]]. Specifically intended for [[Coastal defence and fortification|coastal defense]], their [[freeboard (nautical)|freeboard]] was insufficient to deal well with the waves of the open ocean. Their [[Gun turret#Warships|turrets]] lacked counterweights, and the main [[belt armor]] was placed too low to be effective under most conditions.

The ships were named {{USS|Indiana|BB-1|2}}, {{USS|Massachusetts|BB-2|2}}, and {{USS|Oregon|BB-3|2}} and were designated ''Battleship Number 1'' through ''3''. All three served in the [[Spanish–American War]], although ''Oregon''—which was stationed on the [[West Coast of the United States|West Coast]]—had to cruise {{convert|14000|nmi|lk=on}} around South America to the [[East Coast of the United States|East Coast]] first. After the war, ''Oregon'' returned to the Pacific and participated in the [[Philippine–American War]] and [[Boxer Rebellion]], while her sister ships were restricted to training missions in the Atlantic Ocean. After 1903, the obsolete battleships were de- and recommissioned several times, the last time during [[World War I]] when ''Indiana'' and ''Massachusetts'' served as training ships, while ''Oregon'' was a transport escort for the [[Siberian Intervention]].

In 1919, all three ships were decommissioned for the final time. ''Indiana'' was sunk in shallow water as an explosives test target a year later and sold for scrap in 1924. ''Massachusetts'' was [[scuttled]] off the coast of [[Pensacola]] in 1920 and used as an artillery target. The wreck was never scrapped and is now a [[Florida Underwater Archaeological Preserve]]. ''Oregon'' was initially preserved as a museum, but was sold for scrap during [[World War II]]. The scrapping was later halted and the stripped hulk was used as an ammunition barge during the [[Battle of Guam (1944)|battle of Guam]]. The hulk was finally sold for scrap in 1956.

== Background ==
The ''Indiana'' class was very controversial at the time of its approval by the [[United States Congress]]. A policy board convened by the [[Secretary of the Navy]] [[Benjamin F. Tracy]] came up with an ambitious 15-year naval construction program on 16 July 1889, three years after the {{USS|Maine|ACR-1|2}} and the {{USS|Texas|1892|2}} were authorized. The battleships in their plan would include ten [[first-rate]] long-range battleships with a {{convert|17|kn|lk=on}} top speed and a steaming radius of {{cvt|5400|nmi}} at {{cvt|10|kn}}—{{cvt|6500|nmi}} maximum. These ocean-going ships were envisioned as a possible [[fleet in being]], a fleet capable of raiding an enemy's home ports and intended to deter powerful warships from ranging too far from home. Twenty-five short-range [[second-rate]] battleships would provide home defense in both the Atlantic and Pacific and support the faster and larger long-range vessels. With a range of roughly {{cvt|2700|nmi}} at 10&nbsp;knots and a [[draft (hull)|draft]] of {{cvt|23.5|ft|lk=on}}, they would roam from the [[St. Lawrence River]] in the north to the [[Windward Islands]] and [[Panama]] in the south and would be able to enter all of the ports in the southern United States.{{sfn|Friedman|1985|pp=23–24, 29}}{{sfn|Reilly|Scheina|1980|p=52}}

It was proposed, probably for cost reasons, that the short-range battleships should have a hierarchy of three subclasses. The first would mount four {{convert|13|in|adj=on|0}} guns each on eight {{convert|8000|LT|t ST|adj=on}} ships, the second would mount four {{convert|12|in|adj=on|0}} guns each on ten {{convert|7150|LT|t ST|adj=on}} ships, and the third would mount two 12-inch and two {{convert|10|in|adj=on|0}} guns each on five {{convert|6000|LT|t ST|adj=on}} ships. The two battleships already under construction, ''Texas'' and ''Maine'', were to be grouped under the last class. In addition, 167 smaller ships, including [[Naval ram|rams]], [[cruiser]]s and [[torpedo boat]]s, would be built, coming to a total cost of [[United States dollar|$]]281.55&nbsp;million,{{sfn|Reilly|Scheina|1980|p=52}}{{sfn|Friedman|1985|p=24}} approximately equal to the sum of the entire US Navy budget during the previous 15 years (adjusted for inflation, $6.6 billion in 2009 dollars).{{sfn|US Navy Budget: 1794–2004}}

Congress balked at the plan, seeing in it an end to the United States [[United States non-interventionism|policy of isolationism]] and the beginning of [[imperialism]]. Even some supporters of naval expansion were wary; Senator [[Eugene Hale]] feared that because the proposal was so large, the entire bill would be shot down and no money appropriated for any ships. However, in April 1890, the [[United States House of Representatives]] approved funding for three 8,000-long ton battleships. Tracy, trying to soothe tensions within Congress, remarked that these ships were so powerful that only twelve would be necessary instead of the 35 called for in the original plan. He also slashed the operating costs of the Navy by giving the remaining [[American Civil War|Civil War]]-era [[Monitor (warship)|monitors]]—which were utterly obsolete by this time—to navy militias operated by the states.{{sfn|Friedman|1985|pp=24–25}} The appropriation was also approved by the Senate, and in total three coast-defense battleships (the ''Indiana'' class), a cruiser, and a torpedo boat were given official approval and funding on 30 June 1890.{{sfn|Friedman|1985|pp=24–25}}{{sfn|Gardiner|Lambert|1992|p=121}}

The first class of short-range ships as envisioned by the policy board were to mount [[13"/35 caliber gun|13-inch/35]] [[caliber#Caliber as measurement of length|caliber]] and new {{convert|5|in|adj=on|0}} guns, with {{cvt|17|in|0}} of [[belt armor]], {{cvt|2.75|in|0}} of deck armor and {{cvt|4|in|0}} of armor over the [[casemate]]s. The ''Indiana'' class, as actually built, exceeded the design in displacement by 25 percent, but most other aspects were relatively similar to the original plan. An {{convert|18|in|adj=on|0}} belt and a secondary battery of {{convert|8|in|adj=on|0}} and {{convert|6|in|adj=on|0}} guns were adopted, the latter because the [[Bureau of Ordnance]] did not have the capability to construct [[Quick-firing gun|rapid-firing]] 5-inch weaponry. The larger weapons were much slower firing and much heavier, but without the bigger guns, the ships would not be able to penetrate the armor of foreign battleships.{{sfn|Friedman|1985|pp=26–27}}

== Design ==

[[File:USS Oregon 1893 USNHC NH 76619 010332.jpg|thumb|Outboard profile of ''Oregon'', with position and arc of fire of the armament|alt=drawing schematic, showing two large turrets before and aft and four smaller turrets on wing positions midships]]

=== General characteristics ===

The ''Indiana''-class ships were designed specifically for coastal defense and were not intended for offensive actions.{{sfn|Scientific American|1896|p=297}} This design view was reflected in their moderate coal endurance, relatively small displacement and low freeboard, which limited seagoing capability.{{sfn|Gardiner|Lambert|1992|p=121}} However, they were heavily armed and armored, so much in fact that ''Conway's All The Worlds Fighting Ships'' describes them as "attempting too much on a very limited displacement."{{sfn|Chesneau|Koleśnik|Campbell|1979|p=140}} They resembled the British battleship {{HMS|Hood|1891}}, but were {{cvt|60|ft}} shorter and featured an intermediate battery consisting of eight 8-inch guns not found in European ships,{{sfn|Gardiner|Lambert|1992|p=121}} giving them a very respectable amount of firepower for their time.{{sfn|Reilly|Scheina|1980|p=52}}

The original design of the ''Indiana'' class included [[bilge keel]]s, but with keels they would not fit in any of the American drydocks at the time, so they were omitted during construction. This meant a reduction in stability and caused a serious problem for ''Indiana'', when both main turrets broke loose from their clamps in heavy seas a year after being commissioned. Because the turrets were not centrally balanced, they swung from side to side with the motion of the ship, until they were secured with heavy ropes. When the ship encountered more bad weather four months later, she promptly steamed back to port for fear the clamps would break again.{{sfn|Reilly|Scheina|1980|p=59}} This convinced the navy that bilge keels were necessary and they were subsequently installed on all three ships.{{sfn|Reilly|Scheina|1980|p=60}}

=== Armament ===
Given their limited displacement, the ''Indiana'' class had formidable armament for the time: four 13-inch guns, an intermediate battery of eight 8-inch guns and a secondary battery of four 6-inch guns, twenty [[QF 6 pounder Hotchkiss|Hotchkiss 6-pounders]], and six [[QF 1 pounder pom-pom|Maxim-Nordenfelt 1-pounders]], as well as six{{efn|name=torpedo}} 18-inch [[British 18 inch torpedo|Whitehead torpedo]] tubes.{{sfn|Reilly|Scheina|1980|pp=54–55}}

[[File:USS Indiana (BB-1) - NH 52653.jpg|thumb|left|alt=several men stand on deck next to a large turret, with a smaller one visible in the background|The forecastle of ''Indiana'', showing its fore 13-inch turret and one of the 8-inch turrets]]

The 13-inch gun was 35 [[Caliber (artillery)|calibers]] long and used [[black powder]], giving a range of about {{convert|12000|yd}} at 15 degrees of elevation. At {{convert|6000|yd}}, a shell was expected to penetrate {{convert|10|-|12|in}} of side armor.{{sfn|NavWeaps 13"/35 (33 cm) Marks 1 and 2}} The four guns were mounted in two centerline turrets, located fore and aft. The turrets were originally designed to feature sloping side armor, but space requirements made this impossible without using significantly larger gun turrets or redesigning the gun mounts (which was later done for the {{sclass-|Illinois|battleship}}s).{{sfn|Reilly|Scheina|1980|p=55}} The ships' low freeboard greatly hindered the use of the main battery in rough weather conditions, because the deck would become awash. Also, because the ship lacked a counterweight to offset the weight of the gun barrels, the ship would [[List (watercraft)|list]] in the direction the guns were aimed. This reduced the maximum arc of elevation (and thus range) to about 5 degrees, brought the main armor belt under water on that side, and exposed the unarmored bottom on the other. It was considered in 1901 to replace the turrets with new balanced models used in later ships, but that was decided to be too costly as the ships were already obsolete. Instead, counterweights were added, which partially solved the problem. The hydraulic rammers and turning mechanisms of the 8-inch turrets were also replaced by faster and more efficient electric equivalents, new sights were fitted on ''Indiana'' and ''Massachusetts'', and new turret hoists were installed to improve the reloading speed,{{sfn|Reilly|Scheina|1980|p=62}} but the gun mountings never performed in an entirely satisfactory manner.{{sfn|Gardiner|Lambert|1992|p=121}}

The eight 8-inch guns were mounted in pairs in four wing turrets placed on the superstructure. Their arc of fire, although big on paper, was in reality limited. Adjacent gun positions and superstructure would be damaged by their muzzle blast if the gun was trained alongside it, a defect also suffered by the 13-inch guns.{{sfn|Reilly|Scheina|1980|p=60}} The smaller 6-inch guns were mounted in twin wing [[casemate]]s midships on the main deck level, with a 6-pounder in between. The other Hotchkiss 6-pounders lined the superstructure and bridge decks. Four of the 1-pounders were placed in hull casemates at the bow and stern of the ship and two more in the [[fighting top]]s of the masts.{{sfn|Reilly|Scheina|1980|p=52}} In 1908, all the 6-inch and most of the lighter guns were removed to compensate for the counterweights added to the main battery and because ammunition supply for the guns was considered problematic. A year later, twelve [[3"/50 caliber gun|{{convert|3|in|adj=on|0}}/50-caliber]] single-purpose guns were added midships and in the fighting tops.{{sfn|Reilly|Scheina|1980|p=62}}

Sources conflict on the number of torpedo tubes originally included in the ships,{{efn|name=torpedo}} but it is clear they were located on the [[berth deck]] and had above-water ports located on the extreme front and aft and midships. Located too close to the waterline to allow use while moving and vulnerable to gunfire when opened, they were considered useless and were quickly reduced in number, and removed entirely before 1908.{{sfn|Reilly|Scheina|1980|p=62}}

=== Protection ===
With the exception of the deck armor, 8-inch turrets and [[conning tower]]—which consisted of conventional nickel steel—the ''Indiana'' class was protected with the new [[Harvey armor]]. Its main protection was a belt {{cvt|18|in|0}} thick, placed along two thirds of the length of the hull from {{cvt|3|ft}} above to {{cvt|1|ft}} under the waterline. Beyond this point, the belt gradually grew thinner until it ended {{cvt|4|ft|3|in}} under the waterline, where the belt was only {{cvt|8.5|in}} thick. Below the belt the ship had no armor, only a double bottom. On both ends the belt was connected to the [[barbette]]s of the main guns with {{convert|14|in|adj=on}} armored [[Bulkhead (partition)|bulkheads]]. In the waterline sections outside this central citadel, compartments were filled with compressed cellulose, intended to self-seal when damaged. Between the deck and the main belt, 5-inch hull armor was used. The deck armor was {{cvt|2.75|in}} thick inside the citadel and {{cvt|3|in}} outside it. The hollow conning tower was a single forging 10&nbsp;inches thick. The 13-inch gun battery had {{cvt|15|in}} of vertical turret plating and {{convert|17|in|mm|adj=mid|-thick}} barbettes, while the 8-inch cannons only had 6&nbsp;inches of vertical turret plating and {{convert|8|in|mm|adj=mid|-thick}} barbettes. The casemates protecting the 6-inch guns were 5&nbsp;inches thick and the other casemates, lighter guns, shell hoists and turret crowns were all lightly armored.{{efn|name=armor}}{{sfn|Reilly|Scheina|1980|pp=56, 58 & 68}}

[[File:Fire room Massachusetts.jpg|thumb|alt=Two dirty men feeding coal into an oven in a rather gloomy looking room| The fire room (boiler room) of ''Massachusetts'']]

The placement of the belt armor was based on the [[draft (hull)|draft]] from the design, which was {{convert|24|ft}} with a normal load of {{convert|400|LT|t ST|0}} of coal on board. Her total coal storage capacity was {{convert|1600|LT|t ST|0}}, and fully loaded her draft would increase to {{convert|27|ft}}, entirely submerging the armor belt. During actual service, especially at war, the ships were kept fully loaded whenever possible, rendering her belt armor almost useless. That this was not considered in the design outraged the Walker policy board–convened in 1896 to evaluate the existing American battleships and propose a design for the new {{sclass-|Illinois|battleship}}s–and they set a standard that the load of coal and ammunition that future ships were designed for had to be at least two-thirds of the maximum, so similar problems would be prevented in new ships.{{sfn|Friedman|1985|p=29}}

=== Propulsion ===
Two vertical inverted [[triple expansion]] [[reciprocating steam engine]]s powered by four [[Scotch marine boiler#Double-ended|double-ended Scotch boilers]] drove twin propellers, while two single-ended Scotch boilers supplied steam for auxiliary machinery.{{sfn|Reilly|Scheina|1980|p=58}} The engines were designed to provide {{convert|9000|ihp|lk=on}}, giving the ships a top speed of {{convert|15|kn|lk=on}}.{{sfn|Friedman|1985|p=425}} During [[sea trial]]s, which were conducted with limited amounts of coal, ammunition and supplies on board, it was found that the indicated horsepower and top speed exceeded design values and a significant variation between the three ships existed. The engines of ''Indiana'' delivered {{cvt|9700|ihp}}, giving a top speed of {{cvt|15.6|kn}}. ''Massachusetts'' had a top speed of {{cvt|16.2|kn}} with {{cvt|10400|ihp}} and ''Oregon'' reached a speed of {{cvt|16.8|kn}} with {{cvt|11000|ihp}}.{{sfn|Reilly|Scheina|1980|p=68}} Eight [[Babcock & Wilcox boiler]]s, including four with [[superheater]]s, were installed on ''Indiana'' in 1904 and the same number on ''Massachusetts'' in 1907 to replace the outdated Scotch boilers.{{sfn|Reilly|Scheina|1980|p=62}}

== Ships in class ==

{| class="wikitable plainrowheaders sortable" style="margin-right: 0;"
! scope="col" | Name
! scope="col" style="width:  4em;" | Hull
! scope="col" style="width: 12em;" | Builder
! scope="col" style="width:  9em;" | Laid down
! scope="col" style="width:  9em;" | Launched
! scope="col" style="width:  9em;" | Commissioned
! scope="col" class="unsortable" | Fate
|-
! scope="row" | {{USS|Indiana|BB-1|2}}
| BB-1
| [[William Cramp and Sons]]
| {{dts|7 May 1891}}
| {{dts|28 February 1893}}
| {{dts|20 November 1895}}
| Sunk in explosive tests; hulk sold for scrap 1924
|-
! scope="row" | {{USS|Massachusetts|BB-2|2}}
| BB-2
| [[William Cramp and Sons]]
| {{dts|25 June 1891}}
| {{dts|10 June 1893}}
| {{dts|10 June 1896}}
| Sunk as gunnery target 1921; now an artificial reef
|-
! scope="row" | {{USS|Oregon|BB-3|2}}
| BB-3
| [[Union Iron Works]]
| {{dts|19 November 1891}}
| {{dts|26 October 1893}}
| {{dts|16 July 1896}}
| Initially preserved as a museum; sold for scrap 1956
|}

=== Indiana (BB-1) ===

[[File:Indiana post war.jpg|thumb|alt=''Indiana'' painted wartime gray is anchored. On the background a second ship is visible|USS ''Indiana'' after the Spanish–American War. The ship in the background is [[USS Iowa (BB-4)|''Iowa'']].]]

{{main article|USS Indiana (BB-1)}}

Commissioned in 1895, ''Indiana'' did not participate in any notable events until the outbreak of the [[Spanish–American War]] in 1898, when ''Indiana'' was part of the [[North Atlantic Squadron]] under Rear Admiral [[William T. Sampson]].{{sfn|DANFS Indiana (BB-1)}} His squadron was ordered to the Spanish port of [[San Juan, Puerto Rico|San Juan]] in an attempt to intercept and destroy [[Admiral]] [[Pascual Cervera y Topete|Cevera]]'s Spanish squadron, which was en route to the Caribbean from Spain. The harbor was empty, but ''Indiana'' and the rest of the squadron bombarded it for two hours before realizing their mistake. Three weeks later news arrived that [[Commodore (United States)|Commodore]] [[Winfield Scott Schley|Schley]]'s [[Flying Squadron (US Navy)|Flying Squadron]] had found Cervera and was now blockading him in the port of [[Santiago de Cuba]]. Sampson reinforced Schley two days later and assumed overall command.{{sfn|Graham|Schley|1902|p=203}} Cervera saw that his situation was desperate and attempted to run the blockade on 3 July 1898. ''Indiana'' did not join in the chase of the fast Spanish [[cruiser]]s because of her extreme eastern position on the blockade{{sfn|DANFS Indiana (BB-1)}} and low speed caused by engine problems,{{sfn|Graham|Schley|1902|pp=304 & 317}} but was near the harbor entrance when the Spanish [[destroyer]]s [[Spanish destroyer Pluton|''Pluton'']] and [[Spanish destroyer Furor|''Furor'']] emerged. Together with the battleship {{USS|Iowa|BB-4|2}} and [[armed yacht]] {{USS|Gloucester|1891|2}} she opened fire, destroying the lightly armored enemy ships.{{sfn|DANFS Indiana (BB-1)}}

After the war, ''Indiana'' returned to training exercises before being decommissioned in 1903. The battleship was recommissioned in January 1906 to function as a training vessel until she was decommissioned again in 1914. Her third commission was in 1917 when ''Indiana'' served as a training ship for gun crews during [[World War I]]. She was decommissioned for the final time on 31 January 1919, shortly after being reclassified ''Coast Battleship Number 1'' so that the name ''Indiana'' could be assigned to the newly authorized—but never completed—battleship {{USS|Indiana|BB-50|3}}. She was sunk in shallow water as a target in underwater explosion and aerial bombing tests in November 1920. Her hulk was sold for scrap on 19 March 1924.{{sfn|DANFS Indiana (BB-1)}}

=== Massachusetts (BB-2) ===

{{main article|USS Massachusetts (BB-2)}}

[[File:USS Massachusetts (BB-2) sinking 1921.jpg|thumb|left|alt=A sinking stripped battleship seen from a birds eyes view|''Massachusetts'' being scuttled off the coast of [[Pensacola]]]]

Between being commissioned in 1896 and the outbreak of the Spanish–American War in 1898, ''Massachusetts'' conducted training exercises off the eastern coast of the United States.{{sfn|DANFS Massachusetts (BB-2)}} During the war, she was placed in the Flying Squadron under Commodore [[Winfield Scott Schley]]. Schley went searching for Cervera's Spanish squadron and found it in the port of Santiago. The battleship was part of the blockade fleet until 3 July, but missed the [[Battle of Santiago de Cuba]], because she had steamed to [[Guantánamo Bay]] the night before to resupply coal.{{sfn|Graham|Schley|1902|p=300}} The next day, the battleship came back to Santiago, where she and ''Texas'' fired at the [[Spanish cruiser Reina Mercedes|Spanish cruiser ''Reina Mercedes'']], which was being scuttled by the Spanish in a failed attempt to block the harbor entrance channel.{{sfn|Graham|Schley|1902|pp=471–472}}

During the next seven years, ''Massachusetts'' cruised the Atlantic coast and eastern Caribbean as a member of the North Atlantic Squadron and then served for a year as a training ship for [[USNA|Naval Academy]] midshipmen until she was decommissioned in January 1906. In May 1910, she was placed in reduced commission as a training ship again before entering the [[Atlantic Reserve Fleet]] in September 1912, where she stayed until being decommissioned in May 1914. ''Massachusetts'' was recommissioned in June 1917 to serve as a training ship for gun crews during World War I. She was decommissioned for the final time on 31 March 1919, after being redesignated ''Coast Battleship Number 2'' two days earlier so her name could be reused for {{USS|Massachusetts|BB-54|3}}. On 6 January 1921 she was scuttled off the coast of [[Pensacola]] and used as an artillery target for [[Fort Pickens]]. The Navy attempted to sell her for scrap, but no buyer could be found and in 1956 the ship was declared the property of the state of Florida.{{sfn|DANFS Massachusetts (BB-2)}} The wreck is currently one of the [[Florida Underwater Archaeological Preserves]] and serves as an artificial reef.{{sfn|Museums In The Sea}}

=== Oregon (BB-3) ===

{{main article|USS Oregon (BB-3)}}

''Oregon'' served for a short time with the [[Pacific Station]] before being ordered on a voyage around South America to the East Coast in March 1898 in preparation for war with Spain. She departed from [[San Francisco]] on 19 March, and reached [[Jupiter Inlet]] on 24 May, stopping several times for additional coal on the way. A journey of over 14,000 nautical miles was completed in 66 days, which was considered a remarkable achievement at the time.{{sfn|Reilly|Scheina|1980|pp=66–67}} The ''[[Dictionary of American Naval Fighting Ships]]'' describes the effect of the journey on the American public and government as follows: "On one hand the feat had demonstrated the many capabilities of a heavy battleship in all conditions of wind and sea. On the other it swept away all opposition for the construction of the [[Panama Canal]], for it was then made clear that the country could not afford to take two months to send warships from one coast to the other each time an emergency arose."{{sfn|DANFS Oregon (BB-3)}} After completing her journey, ''Oregon'' was ordered to join the blockade at Santiago as part of the North Atlantic Squadron under Rear Admiral Sampson. She took part in the [[Battle of Santiago de Cuba]], where she and the cruiser {{USS|Brooklyn|CA-3|2}} were the only ships fast enough to chase down the [[Spanish cruiser Cristobal Colon|Spanish cruiser ''Cristobal Colon'']], forcing its surrender.{{sfn|Graham|Schley|1902|pp=339 & 345}} Around this time, she received the nickname "Bulldog of the Navy", most likely because of her high bow wave—known as "having a bone in her teeth" in nautical slang—and perseverance during the cruise around South America and the battle of Santiago.{{sfn|Lomax|2005}}

[[File:USS Oregon in dry dock, 1898.jpg|thumb|alt=''Oregon'' seen from behind in drydock|USS ''Oregon'' in drydock in 1898, showing her starboard bilge keel and pudgy underwater shape]]

After the war, ''Oregon'' was refitted in [[New York City]] before she was sent back to the Pacific, where she served as a [[guard ship]] for two years. She served for a year in the [[Philippines]] during the [[Philippine–American War]] and then spent a year in China at [[Wusong]] during the [[Boxer Rebellion]] until May 1901, when she was ordered back to the United States for an overhaul. In March 1903, ''Oregon'' returned to Asiatic waters and the ship remained in the Far East, returning only shortly before decommissioning in April 1906. ''Oregon'' was recommissioned in August 1911, but saw little activity and was officially placed on reserve status in 1914. On 2 January 1915, the ship was returned to full commission and sailed to [[San Francisco]] for the [[Panama–Pacific International Exposition]]. A year later, she was back to reserve status, only to be returned to full commission in April 1917 when the United States joined World War I. ''Oregon'' acted as one of the escorts for transport ships during the [[Siberian Intervention]]. In June 1919, she was decommissioned, but a month later she was temporarily recommissioned as the reviewing ship for President [[Woodrow Wilson]] during the arrival of the Pacific Fleet at [[Seattle]]. In October 1919, she was decommissioned for the final time. As a result of the [[Washington Naval Treaty]], ''Oregon'' was declared "incapable of further warlike service" in January 1924. In June 1925, she was loaned to the [[Government of Oregon|State of Oregon]], who used her as a floating monument and museum in [[Portland, Oregon|Portland]].{{sfn|DANFS Oregon (BB-3)}}

In February 1941, ''Oregon'' was redesignated {{USS|Oregon|IX-22|1}}. Due to the outbreak of [[World War II]], it was decided that the scrap value of the ship was more important than her historical value, so she was sold. Her stripped hulk was later returned to the Navy and used as an ammunition barge during the [[battle of Guam (1944)|battle of Guam]], where she remained for several years. During a typhoon in November 1948, she broke loose and drifted out to sea. She was located {{cvt|500|mi}} southeast of Guam and towed back. She was sold on 15 March 1956 and scrapped in Japan.{{sfn|DANFS Oregon (BB-3)}}

== See also ==
{{portal|Battleships}}

* [[Battleship Illinois (replica)]]

== Footnotes ==

'''Notes'''

{{notes
| notes =

{{efn
| name = range
| Rounded average calculated from the experimental data in this paper, with BB-1 and BB-2 lumped together. See {{harvnb|Bryan|1901}}.
}}

{{efn
| name = torpedo
| Sources conflict on this. {{harvnb|Reilly|Scheina|1980|p=}} claim six on p. 56, then four on p. 68. They also claim on ''Massachusetts'' and ''Oregon'' one broadside tube was left out to provide additional space, leaving them with three or five tubes. [[DANFS]] claims six tubes on the latter two and no information is given on ''Indiana''. {{harvnb|Friedman|1985|p=}} claims the contract called for seven tubes, but two of the ships were completed with five, and only four on ''Indiana''.
}}

{{efn
| name = armor
| The armor of ''Oregon'', built by a different shipyard, had slightly different dimensions: {{cvt|8|in}} at the lowest point of the belt, {{cvt|6.25|in}} on the hull and {{cvt|4.5|in}} on the deck inside the citadel. She also had no double bottom outside the central citadel. See {{harvnb|Reilly|Scheina|1980|pp=56, 58 & 68}}.
}}

}}

'''Citations'''

{{reflist
| colwidth = 30em
| refs =

}}

== Bibliography ==

'''Print references'''

* {{cite book
  | last1     = Chesneau
  | first1    = Roger
  | last2     = Koleśnik
  | first2    = Eugène M.
  | last3     = Campbell
  | first3    = N.J.M.
  | year      = 1979
  | title     = Conway's All the World's Fighting Ships, 1860–1905
  | publisher = [[Conway Maritime Press]]
  | location  = [[London]]
  | isbn      = 978-0-85177-133-5
  | ref       = harv
  }}
* {{cite book
  | last      = Friedman
  | first     = Norman
  | authorlink= Norman Friedman
  | year      = 1985
  | title     = U.S. Battleships, An Illustrated Design History
  | publisher = [[Naval Institute Press]]
  | location  = [[Annapolis, Maryland]]
  | isbn      = 978-0-87021-715-9
  | ref       = harv
  }}
* {{cite book
  | last1     = Gardiner
  | first1    = Robert
  | last2     = Lambert
  | first2    = Andrew D.
  | year      = 1992
  | title     = Steam, Steel & Shellfire: The Steam Warship 1815–1905
  | publisher = Conway Maritime Press
  | location  = London
  | isbn      = 978-0-85177-564-7
  | ref       = harv
  }}
* {{cite book
  | last1     = Graham
  | first1    = George E.
  | last2     = Schley
  | first2    = Winfield S.
  | authorlink2 = Winfield Scott Schley
  | year      = 1902
  | title     = Schley and Santiago: an historical account of the blockade and final destruction of the Spanish fleet under command of Admiral Pasquale Cervera, July&nbsp;3,&nbsp;1898
  | publisher = W.B. Conkey company
  | location  = [[Texas]]
  | url       = https://archive.org/details/schleysantiago00graham
  | oclc      = 1866852
  | ref       = harv
  }}
* {{cite book
  | last1     = Reilly
  | first1    = John C.
  | last2     = Scheina
  | first2    = Robert L.
  | year      = 1980
  | title     = American Battleships 1886–1923: Predreadnought Design and Construction
  | publisher = Arms and Armour Press
  | location  = London
  | isbn      = 978-0-85368-446-6
  | ref       = harv
  }}
* {{cite journal
  | title   = The Speed Trial of the United States Battleship Massachusetts
  | journal = [[Scientific American]]
  | volume  = 74
  | pages   = 297
  | date    = 9 May 1896
  | ref     = {{sfnRef|Scientific American|1896}}
  }}; cited in {{harvnb|Reilly|Scheina|1980|p=210}}

'''Dictionary of American Naval Fighting Ships'''

* {{cite DANFS
  | title      = Indiana
  | url        = http://www.history.navy.mil/danfs/i1/indiana-i.htm{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}
  | accessdate = 22 September 2011
  | ref        = {{sfnRef|DANFS Indiana (BB-1)}}
  }}
* {{cite DANFS
  | title      = Massachusetts
  | url        = http://www.history.navy.mil/danfs/m6/massachusetts-iv.htm
  | accessdate = 22 September 2011
  | ref        = {{sfnRef|DANFS Massachusetts (BB-2)}}
  | link       = off
  }}
* {{cite DANFS
  | title      = Oregon
  | url        = https://web.archive.org/web/20110605144714/http://www.history.navy.mil/danfs/o3/oregon-ii.htm
  | accessdate = 22 September 2011
  | ref        = {{sfnRef|DANFS Oregon (BB-3)}}
  | link       = off
  }}

'''Other'''

* {{cite journal
  | doi = 10.1111/j.1559-3584.1901.tb03372.x
  | last = Bryan
  | first = B. C.
  | year = 1901
  | title = The Steaming Radius of United States Naval Vessels
  | journal = Journal of the American Society for Naval Engineers
  | volume = 13
  | issue = 1
  | pages = 50–69
  | pmid = 
  | pmc = 
  | ref = harv}} {{subscription required}}
* {{cite journal
  |last=Lomax 
  |first=Ken 
  |title=A Chronicle of the Battleship ''Oregon''
  |journal=Oregon Historical Quarterly 
  |volume=106 
  |issue=1 
  |pages=132–146 
  |publisher=[[Oregon Historical Society]] 
  |location=[[Portland, Oregon]] 
  |year=2005 
  |jstor=20615507   
  |url=http://www.historycooperative.org/journals/ohq/106.1/lomax.html 
  |accessdate=25 September 2011 
  |ref=harv 
  |deadurl=yes 
  |archiveurl=https://web.archive.org/web/20080617042254/http://www.historycooperative.org/journals/ohq/106.1/lomax.html
  |archivedate=17 June 2008 }}
* {{cite web
  | title      = USS ''Massachusetts''
  | publisher  = Florida's "Museums in the Sea"
  | url        = http://www.museumsinthesea.com/massachusetts/
  | accessdate = 23 September 2011
  | ref        = {{sfnRef|Museums In The Sea}}
  }}
* {{cite web
  | title      = Budget of the US Navy: 1794 to 2004
  | date       = 27 April 2006
  | publisher  = US Navy
  | work       = www.history.navy.mil
  | url        = http://www.history.navy.mil/library/online/budget.htm
  | accessdate = 27 December 2011
  | ref = {{sfnRef|US Navy Budget: 1794–2004}}
  }}
* {{cite web
  | title      = United States of America 13"/35 (33 cm) Mark 1 and Mark 2
  | date       = 15 August 2008
  | publisher  = NavWeaps.com
  | url        = http://navweaps.com/Weapons/WNUS_13-35_mk1.htm
  | accessdate = 27 December 2011
  | ref = {{sfnRef|NavWeaps 13"/35 (33 cm) Marks 1 and 2}}
  }}

== External links ==
{{Commons category|Indiana class battleships}}
* Navsource.org photos of [http://www.navsource.org/archives/01/01a.htm ''Indiana''], [http://www.navsource.org/archives/01/02a.htm ''Massachusetts''] and [http://www.navsource.org/archives/01/03a.htm ''Oregon'']
* [http://www.cr.nps.gov/nr/travel/flshipwrecks/mas.htm U.S. National Park Service photos of BB-2 wreckage off Fort Pickens, Florida]
* [http://www.spanamwar.com/ Spanish American War Centennial Website] encyclopedic website on the Spanish–American War, has several witness accounts of the Battle of Santiago de Cuba and the events surrounding it.

{{Indiana class battleship}}
{{WWI US ships}}
{{Use dmy dates|date=May 2012}}
{{Featured article}}

{{DEFAULTSORT:Indiana Class Battleship}}
[[Category:Battleship classes]]
[[Category:Indiana-class battleships|*]]
[[Category:World War I battleships of the United States]]