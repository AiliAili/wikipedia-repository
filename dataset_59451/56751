{{Infobox journal
| title = Energy Research & Social Science
| abbreviation = Energy Res. Soc. Sci.
| editor = [[Benjamin K. Sovacool]]
| discipline = [[Social sciences]], [[energy system]]s, [[energy policy]]
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 2014-present
| openaccess = 
| impact =
| impact-year =
| website = https://www.journals.elsevier.com/energy-research-and-social-science/
| link2 = http://www.sciencedirect.com/science/journal/22146296
| link2-name = Online archive
| ISSN = 2214-6296
| OCLC = 889252485
| LCCN = 2014205258
}}
'''''Energy Research & Social Science''''' is a monthly [[peer-reviewed]] [[academic journal]] covering [[social science]] research on [[energy systems]] and [[energy and society]], including [[anthropology]], [[economics]], [[geography]], [[psychology]], [[political science]], [[social policy]], [[sociology]], [[science and technology studies]] and [[legal studies]]. It was established in 2014 and is published by [[Elsevier]]. The [[editor-in-chief]] is [[Benjamin K. Sovacool]] ([[Aarhus University]] and [[University of Sussex]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
*[[Emerging Sources Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-02-02}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/21100325067 |title=Source details: Energy Research & Social Science |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-02-02}}</ref>

==See also==
*[[Climate change adaptation]]
*[[Climate change mitigation]]
*[[Energy policy]]
*[[Renewable energy]]

==References==
{{Reflist}}

==External links==
*{{Official website|https://www.journals.elsevier.com/energy-research-and-social-science/}}

{{DEFAULTSORT:Energy Research and Social Science}}
[[Category:Publications established in 2014]]
[[Category:Sociology journals]]
[[Category:Elsevier academic journals]]
[[Category:Energy and fuel journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]