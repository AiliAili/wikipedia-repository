{{Infobox Journal
| title = European Political Science 
| discipline = [[Political science]], [[international relations]]
| abbreviation = Eur. Polit. Sci. 
| publisher = [[Palgrave Macmillan]] in association with the [[European Consortium for Political Research]]
| country = [[United Kingdom]]
| frequency = Quarterly
| history = 2001-present
| website = http://www.palgrave-journals.com/eps/index.html
| link1 = http://www.palgrave-journals.com/eps/journal/v14/n4/
| link1-name = Online access
| link2 = http://www.palgrave-journals.com/eps/archive/index.html
| link2-name = Online archive
| impact = 0.705
| impact-year = 2014
| ISSN =  1680-4333
| eISSN = 1682-0983
| OCLC = 884650554
| LCCN = 2006238655
}}
'''''European Political Science''''' ('''''EPS''''') is a [[Peer review|peer-reviewed]] [[academic journal]]. The professional journal of the [[European Consortium for Political Research]] (ECPR), it has been published in collaboration with [[Palgrave Macmillan]] since 2005. The journal's interpretation of 'political science' is wide, and encompasses comparative politics, political economy, international relations, public administration, political theory, European studies and related disciplines. It publishes pieces on how the discipline is, can be and ought to be. Articles address research matters (including debates in the discipline, research projects, political science information sources, funding opportunities); professional matters (such as career structures and prospects, external evaluation, higher education reforms, accreditation issues); doctoral training provision and teaching matters; and relations between academics and politicians, policy-makers, journalists and ordinary citizens. EPS also includes more substantive pieces that provide a political science perspective on important current events. In addition to original articles, the journal carries shorter notes, review articles and symposia, progress reports on lively areas of research and profiles of people in the profession. Each issue of EPS also contains book reviews.

The main content of the journal is edited by Martin Bull (Academic Director of the ECPR), Luis de Sousa, Jonathon Moses and Jacqui Briggs. The Reviews section of each issue is edited by Lasse Thomassen. EPS  has published reviews by leading European and international political scientists and theorists such as [[Jean Blondel]], Rachel A Epstein, Michelle Everson, Michael Freeden, [[Michael Keating (political scientist)|Michael Keating]],  Peter Lassman,  [[Peter Mair]], Glyn Morgan, Gianfranco Pasquino, and Philippe C. Schmitter. Other reviewers have included [[John Bruton]], former Prime Minister of Ireland and EU Ambassador to the United States, and senior EU officials such as Albrecht Rothacher and Martin Westlake.

==History==
The journal emerged from the old ''ECPR News'', starting in 2001 as an in-house publication distributed free to members by the ECPR before moving to Palgrave Macmillan in 2005. The full text of these early issues (Vols 1-3) is available online for free.

==Abstracting and indexing==
The journal is abstracted in The International Political Science Abstracts, a publication of the International Political Science Association. It is also covered in Worldwide Political Science Abstracts, European Sources Online and the [[International Bibliography of the Social Sciences]]. 

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.705, ranking it 84th out of 161 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.palgrave-journals.com/eps/index.html}}, includes free access to the full text of ECPR News
*[http://www.ecprnet.eu/publications/journals/eps.asp European Political Science (EPS)] Homepage at the ECPR, includes free access to the full text of Vols 1-3, 2001-2004

{{Georg von Holtzbrinck Publishing Group}}

[[Category:Political science journals]]
[[Category:Publications established in 2001]]
[[Category:Palgrave Macmillan academic journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]