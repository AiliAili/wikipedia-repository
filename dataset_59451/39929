<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Hans-Martin Pippart
| image         =
| caption       =
| birth_date          = <!-- {{Birth date and age|YYYY|MM|DD}} -->14 May 1888
| death_date          = {{Death date and age|df=yes|1918|8|11|1888|5|14}}
| placeofburial_label = 
| placeofburial = 
| birth_place  =[[Mannheim]], [[Baden]], [[Germany]]
| death_place  =Near [[Noyon]], [[France]]
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =German Empire
| branch        =Flying service
| serviceyears  =1914 - 1918
| rank          =Leutnant
| unit          =FF(A) 220, [[Kasta 1]], [[Jasta 13]]
| commands      =[[Jasta 19]]
| battles       =[[World War I]]
| awards        =[[Iron Cross]]
| relations     =
| laterwork     =
}}
Leutnant '''Hans Martin Pippart''' (14 May 1888 – 11 August 1918) [[Iron Cross]] was a pioneer aircraft manufacturer and early pilot.<ref name="ReferenceA">{{cite book |title=''Jagdgeschwader Nr. II: Jagdgeschwader Berthold'' |page=13  }}</ref> As a [[World War I]] [[fighter ace]] he was credited with 22 victories.<ref name="theaerodrome.com">{{cite web|url=http://www.theaerodrome.com/aces/germany/pippart.php |title=Hans Martin Pippart |publisher=Theaerodrome.com |date= |accessdate=2010-03-18}}</ref>

==Early life==
Pippart had built airplanes and become a pilot before the beginning of [[World War I]]. He was a principal in the aeronautical firm of ''Pippart und Noll'', which produced ''Eindekker'' airplanes. In 1913, he set an aviation endurance record.<ref name="ReferenceA"/>

He joined the German air forces at the start of the war. His initial assignment was as an instructor. He then served some time flying artillery coordination missions in two-seaters with FF(A) 220 in [[Galicia (eastern Europe)|Galicia]]. Single seater scouts were also available to him.<ref name="theaerodrome.com"/>

==Aerial victories==
Pippart scored his first win on 25 May 1917, after he had been assigned to [[Kasta 1]]. They were operating [[Roland D.II]]s on the [[Eastern Front (World War I)|Eastern Front]] against the Russians.<ref name="ReferenceA"/><ref name="theaerodrome.com"/>

By the end of 1917, despite flying on a quiet sector,<ref name="ReferenceB">{{cite book |title=''Jagdgeschwader Nr. II: Jagdgeschwader Berthold'' |page=11  }}</ref> he had six victories to his credit—two planes and four observation balloons—and was transferred to [[Jasta 13]] on the [[Western Front (World War I)|Western Front]]. On 21 February 1918, he took up his balloon busting again, downing a gas bag. By the time he left Jasta 13 at the end of April, he was a double ace. His six victories over balloons made him an ace on them alone.<ref name="theaerodrome.com"/>

[[Image:D7f.jpg|thumb|Fokker D.VII]]
[[Image:DRI neu.jpg|right|thumb|Fokker D.I]]

On 18 April 1918, he was reassigned to take the command of the Royal Prussian [[Jasta 19]].<ref name="ReferenceB"/><ref>{{cite web|url=http://www.theaerodrome.com/services/germany/jasta/jasta19.php |title=Jasta 19 |publisher=Theaerodrome.com |date= |accessdate=2010-03-18}}</ref> However, it seems there must have been some lag in his transfer, as victory number ten is scored to his old squadron on 20 April. On 2 May, he scored his first victory flying a [[Fokker D.VII]] for his new squadron. With four wins in May, one in June, and six in August, his score had soon grown to 21.<ref name="theaerodrome.com"/><ref name="ReferenceB"/>

==His death==
Pippart ended his career as a [[balloon buster]] on 11 August 1918. He shot down a balloon, but was hit by anti-aircraft fire and found he had to abandon his airplane at an altitude of 150 feet. His parachute failed to open.<ref name="theaerodrome.com"/><ref>{{cite web|url=http://www.ejection-history.org.uk/PROJECT/Parachutes/1914_18.htm |title=1914_1918 |publisher=Ejection-history.org.uk |date= |accessdate=2010-03-18}}</ref>

In his victory log, he was credited with seven enemy observation balloons destroyed, eight opposing fighter planes vanquished, and seven reconnaissance aircraft shot down.<ref name="theaerodrome.com"/>

==Sources of information==
{{reflist}}
{{wwi-air}}

{{Authority control}}

{{DEFAULTSORT:Pippart, Hans Martin}}
[[Category:German World War I flying aces]]
[[Category:Recipients of the Iron Cross (1914), 1st class]]
[[Category:1888 births]]
[[Category:1918 deaths]]
[[Category:People from Mannheim]]
[[Category:People from the Grand Duchy of Baden]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Prussian Army personnel]]
[[Category:German military personnel killed in World War I]]
[[Category:Aviators killed by being shot down]]
[[Category:Aviation pioneers]]