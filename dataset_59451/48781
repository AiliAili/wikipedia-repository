{{Infobox pro wrestling championship
| championshipname = IPWA Light Heavyweight Championship
| image = Joey Mathews (Adam Birch).jpg
| image_size = 
| caption = [[Joey Mercury|Joey Matthews]] is a former IPWA Light Heavyweight Champion.
| promotion = [[Independent Professional Wrestling Alliance]]
| brand = 
| created = June 7, 1996
| mostreigns = [[Julio Dinero|Julio Sanchez]] (4)
| firstchamp = [[Mark Shrader]]
| finalchamp = Jacey North<br />(won March 25, 2000)
| longestreign = Julio Sanchez (11 days)
| shortestreign = Julio Sanchez (273 days)
| oldest = 
| youngest = 
| heaviest = 
| lightest = 
| pastnames = 
| titleretired = 2001
| pastlookimages = 
}}
The '''IPWA Light Heavyweight Championship''' was a [[professional wrestling]] [[Cruiserweight (professional wrestling)|light heavyweight]] [[Championship (professional wrestling)#Weight class championships|championship]] in [[Independent Professional Wrestling Alliance]] (IPWA). The inaugural champion was [[Mark Shrader|Mark "The Shark" Shrader]], who defeated Quinn Nash (substituting for [[Earl the Pearl]]) in a tournament final on June 7, 1996 to become the first IPWA Light Heavyweight Champion.

There were 7 officially recognized champions with [[Julio Dinero|Julio Sanchez]] winning the title a record 4-times. He was also its longest reigning champion with his second title reign lasting 273 days. Some of the top "indy cruiserweights" on the East Cost wrestled for the title during its near 6-year history with former champions including [[Steve Corino]], [[Christian York]], and [[Joey Matthews]].

==Title history==
{| class="wikitable"
|-
|'''#'''
|Order in reign history
|-
|'''Reign'''
|The reign number for the specific set of wrestlers listed
|-
|'''Event'''
|The event in which the title was won
|-
| &mdash;
|Used for vacated reigns so as not to count it as an official reign
|-
|N/A
|The information is not available or is unknown
|-
|'''+'''
|Indicates the current reign is changing daily
|}

===Names===
{|class="wikitable" border="1"
|-
!Name
!Years
|-
|IPWA Light Heavyweight Championship
|1996 &mdash; 2001
|}

===Reigns===
{| class="wikitable sortable" width=98% style="text-align:center;"
!style="background:#e3e3e3" width=0%| #
!style="background:#e3e3e3" width=20%|Wrestlers
!style="background:#e3e3e3" width=0%|Reign
!style="background:#e3e3e3" width=10%|Date
!style="background:#e3e3e3" width=0%|Days<br />held
!style="background:#e3e3e3" width=16%|Location
!style="background:#e3e3e3" width=18%|Event
!style="background:#e3e3e3" width=60% class="unsortable"|Notes
!style="background:#e3e3e3;" width=0% class="unsortable"|Ref.
|-
|{{sort|01|1}}
|{{sort|Shrader|[[Mark Shrader]]}}
|{{sort|01|1}}
|{{dts|link=off|1996|06|07}}
|{{age in days nts|month1=06|day1=07|year1=1996|month2=09|day2=13|year2=1996}}
|{{sort|Alexandria|[[Alexandria, Virginia]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Shrader defeated Quinn Nash (substituting for [[Earl the Pearl]] in a tournament final to become the first IPWA Light Heavyweight Champion.
|align="left"|<ref name="Titles">{{cite book|author=Royal Duncan & Gary Will|title=Wrestling Title Histories|publisher=Archeus Communications|year=2000|edition=4th|isbn=0-9698161-5-4}}</ref><ref>DiMuzio, Michael J. "Arena Reports: Virginia - IPWA at Secret Cove Sports Arena." ''[[Pro Wrestling Illustrated]]''. November 1996: 52+.</ref>
|-
|{{sort|38.5|&mdash;}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|1996|09|13}}
|{{sort|zz|&mdash;}}
|N/A
|N/A
|align="left"|The championship is vacated after Mark Shrader wins the [[MEWF Heavyweight Championship]].
|align="left"|<ref name="Titles"/>
|-
|{{sort|02|2}}
|{{sort|Pearl|[[Earl the Pearl]]}}
|{{sort|01|1}}
|{{dts|link=off|1996|09|21}}
|{{age in days nts|month1=09|day1=21|year1=1996|month2=10|day2=26|year2=1996}}
|{{sort|Alexandria|[[Alexandria, Virginia|Alexandria]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Defeated Johnny Graham and Sheik Ali Amin in a three-way match to win the vacant title.
|align="left"|<ref name="Titles"/>
|-
|{{sort|03|3}}
|{{sort|Shrader|Mark Shrader}}
|{{sort|02|2}}
|May 1996
|N/A
|{{sort|Alexandria|[[Alexandria, Virginia|Alexandria]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Titles"/>
|-
|{{sort|04|4}}
|{{sort|Sanchez|[[Julio Dinero|Julio Sanchez]]}}
|{{sort|01|1}}
|{{dts|link=off|1997|03|22}}
|{{age in days nts|month1=03|day1=22|year1=1997|month2=04|day2=29|year2=1997}}
|{{sort|Lenoir|[[Lenoir, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Titles"/><ref name="Solie2">{{cite web |url=http://www.solie.org/titlehistories/lhwipwa.html |title=IPWA Light Heavyweight Title History |author=Westcott, Brian |year=1998 |work=Solie's Title Histories |publisher=[[Solie.org]] |accessdate=November 8, 2011}}</ref><ref name="Genickbruch">{{cite web |url=http://www.genickbruch.com/index.php?befehl=titles&titel=1850 |title=IPWA Light Heavyweight Title |author= |date= |work=Independent Professional Wrestling Alliance |publisher=Genickbruch.com |accessdate=November 10, 2011}}</ref><ref name="Cagematch">{{cite web |url=http://www.cagematch.de/?id=5&nr=837 |title=IPWA Light Heavyweight Championship |author= |date= |work=Independent Professional Wrestling Alliance |publisher=Cagematch.de |accessdate=November 10, 2011}}</ref><ref>Watkins, Marcus. "Arena Reports: North Carolina - IPWA at Lenoir High 
School." ''[[Pro Wrestling Illustrated]]''. August 1997: 74+.</ref> <!-- Cagematch, Genickbruch, and Solie.org claim the match took place on April 22. -->
|-
|{{sort|4.5|&mdash;}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|1997|04|29}}
|{{sort|zz|&mdash;}}
|N/A
|N/A
|align="left"|
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/>
|-
|{{sort|05|5}}
|{{sort|Corino|[[Steve Corino]]}}
|{{sort|01|1}}
|{{dts|link=off|1997|05|02}}
|{{age in days nts|month1=05|day1=02|year1=1997|month2=12|day2=11|year2=1997}}
|{{sort|Pikeville|[[Pikeville, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Corino defeated [[Christian York]] in a tournament final to win both the IPWA and NWA 2000 Light Heavyweight Championships.
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/> <!-- Cagematch claims the match took place on May 3. -->
|-
|{{sort|06|6}}
|{{sort|Sanchez|Julio Sanchez}}
|{{sort|02|2}}
|{{dts|link=off|1997|12|07}}
|{{age in days nts|month1=08|day1=21|year1=2005|month2=05|day2=21|year2=2006}}
|{{sort|Alexandria|[[Alexandria, Virginia|Alexandria]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/><ref>Walker, Timothy A. "Arena Reports: Virginia - Independent Pro Wrestling Alliance at the Secret Cove at Secret Cove Sports Arena." ''[[Pro Wrestling Illustrated]]''. May 1998: 51+.</ref> <!-- Cagematch, Genickbruch, and Solie.org claims the match took place on December 11. -->
|-
|{{sort|07|7}}
|{{sort|York|[[Christian York]]}}
|{{sort|01|1}}
|{{dts|link=off|1998|06|13}}
|{{age in days nts|month1=06|day1=13|year1=1998|month2=08|day2=22|year2=1998}}
|{{sort|Alexandria|[[Alexandria, Virginia|Alexandria]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/><ref>Walker, Timothy A. and Jeff Amdur. "Arena Reports: Virginia - IPWA at Secret Cove." ''[[Pro Wrestling Illustrated]]''. December 1998: 51+.</ref>
|-
|{{sort|08|8}}
|{{sort|Sanchez|Julio Sanchez}}
|{{sort|03|3}}
|{{dts|link=off|1998|08|22}}
|{{age in days nts|month1=08|day1=22|year1=1998|month2=09|day2=02|year2=1998}}
|{{sort|Chincoteague|[[Chincoteague, Virginia]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/>
|-
|{{sort|09|9}}
|{{sort|York|[[Christian York]]}}
|{{sort|02|2}}
|{{dts|link=off|1998|09|02}}
|{{age in days nts|month1=06|day1=13|year1=1998|month2=01|day2=09|year2=1999}}
|{{sort|Richmond|[[Richmond, Virginia]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/>
|-
|{{sort|10|10}}
|{{sort|Matthews|[[Joey Matthews]]}}
|{{sort|01|1}}
|{{dts|link=off|1999|01|09}}
|{{age in days nts|month1=01|day1=09|year1=1999|month2=01|day2=23|year2=1999}}
|{{sort|Chincoteague|[[Chincoteague, Virginia|Chincoteague]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/><ref>{{cite web |url=http://itscwrestling.com/ipwa/results.htm |title=IPWA Results - 9 January 1999 |author=Independent Professional Wrestling Alliance |year=1999 |work=Results |publisher=ITSCwrestling.com |archiveurl=https://web.archive.org/web/20000305031307/http://itscwrestling.com/ipwa/results.htm |archivedate=March 5, 2000 |accessdate=November 10, 2011}}</ref>
|-
|{{sort|11|11}}
|{{sort|Sanchez|Julio Sanchez}}
|{{sort|04|4}}
|{{dts|link=off|1999|01|23}}
|{{sort|zz|N/A}}
|{{sort|Alexandria|[[Alexandria, Virginia|Alexandria]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Sanchez defeated Joey Matthews and Christian York in a three-way match.
|align="left"|<ref name="Titles"/><ref name="Solie2"/><ref name="Genickbruch"/><ref name="Cagematch"/>
|-
|{{sort|12|12}}
|{{sort|Matthews|Joey Matthews}}
|{{sort|02|2}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|
|align="left"|<ref name="IPWA-Results">{{cite web |url=http://itscwrestling.com/ipwa/results.htm |title=IPWA Results - 25 March 2000 |author=Independent Professional Wrestling Alliance |year=2000 |work=Results |publisher=ITSCwrestling.com |archiveurl=https://web.archive.org/web/20010211203951/http://itscwrestling.com/ipwa/results.htm |archivedate=February 11, 2001 |accessdate=November 10, 2011}}</ref>
|-
|{{sort|13|13}}
|{{sort|North|Jacey North}}
|{{sort|01|1}}
|{{dts|link=off|2000|03|25}}
|{{sort|zz|N/A}}
|{{sort|Quantico|[[Quantico, Virginia]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="IPWA-Results"/>
|-
|{{sort|33.5|&mdash;}}
|{{sort|Deactivated|[[Glossary of professional wrestling terms#Vacated|Deactivated]]}}
|{{sort|zz|&mdash;}}
|2001
|{{sort|zz|&mdash;}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|The IPWA ceases promoting events and closes in the spring of 2001.
|align="left"|
|}

==List of combined reigns==
[[File:Julio Dinero.jpg|thumb|200px|[[Julio Dinero|Julio Sanchez]], who was the longest-reigning IPWA Light Heavyweight Champion|alt=A picture of wrestler Julio Sanchez in the ring.]]
{| class="wikitable"
|-
|'''<1'''
|Indicates that the reign lasted less than one day.
|}
{| class="wikitable sortable" style="text-align:center;"
!style="background: #e3e3e3;"|Rank<ref group="N" name="Rank">Each reign is ranked highest to lowest; reigns with the same number mean that they are tied for that certain rank.</ref>
!style="background: #e3e3e3;"|Wrestler
!style="background: #e3e3e3;"|# of reigns
!style="background: #e3e3e3;"|Combined days
|-
|{{sort|01|1}}
|align="left"|{{sort|Sanchez|[[Julio Dinero|Julio Sanchez]]}}
|{{sort|04|4}}
|{{sort|344|344}}
|-
|{{sort|02|2}}
|align="left"|{{sort|York|[[Christian York]]}}
|{{sort|02|2}}
|{{sort|280|280}}
|-
|{{sort|03|3}}
|align="left"|{{sort|Corino|[[Steve Corino]]}}
|{{sort|01|1}}
|{{sort|223|223}}
|-
|{{sort|04|4}}
|align="left"|{{sort|Shrader|[[Mark Shrader]]}}
|{{sort|02|2}}
|{{sort|98|98+}}
|-
|{{sort|05|5}}
|align="left"|{{sort|Earl the Pearl|[[Earl the Pearl]]}}
|{{sort|01|1}}
|{{sort|35|35}}
|}

==Footnotes==
{{Reflist|group=N}}

==References==
{{Reflist|2}}

==External links==
{{Portal|Professional wrestling}}
*[https://web.archive.org/web/20010708043449/http://www.itscwrestling.com/ipwa/ Official website]

{{DEFAULTSORT:List Of IPWA Light Heavyweight Champions}}
[[Category:Cruiserweight wrestling championships]]