{{unreferenced|date=April 2017}}

{{Infobox NCAA team season
  |CoachRank= 
  |APRank= 
  |Mode=Basketball
  |Year=2017–18
  |Prev year=2016–17
  |Next year=2018–19
  |Team=North Carolina Tar Heels
  |Image=University of North Carolina Tarheels Interlocking NC logo.svg
  |ImageSize=140px
  |Conference= Atlantic Coast Conference
  |ShortConference= ACC
  |Record= 0-0
  |ConfRecord= 0-0
  |HeadCoach=[[Roy Williams (coach)|Roy Williams]]
  |HCYear = 15th
  |AsstCoach1=[[Steve Robinson (basketball coach)|Steve Robinson]]
  |AST1Year = 15th
  |AsstCoach2= [[Hubert Davis]]
  |AST2Year = 6th
  |StadiumArena=[[Dean E. Smith Center]]
  |Champion=
  |BowlTourney= 
  |BowlTourneyResult=
}}

The '''2017–18 North Carolina Tar Heels men's basketball team''' will represent the [[University of North Carolina at Chapel Hill]] during the [[2017–18 NCAA Division I men's basketball season]]. The team's [[head coach]] will be [[Roy Williams (coach)|Roy Williams]], who will be in his 15th season as UNC's head men's basketball coach. The Tar Heels will play their home games at the [[Dean Smith Center]] as members of the [[Atlantic Coast Conference]].

==Previous season==

The [[2016–17 North Carolina Tar Heels men's basketball team|Tar Heels]] finished the [[2016–17 NCAA Division I men's basketball season|2016–17 season]] with a record of 33–7, 14–4 in [[2016-17 Atlantic Coast Conference men's basketball season|ACC play]] to finish in first place, winning their 31st ACC regular season title. They received an at-large bid to the [[2017 NCAA Men's Division I Basketball Tournament|NCAA Tournament]] as a No. 1 seed, and there they defeated [[2016–17 Texas Southern Tigers men's basketball team|Texas Southern]], [[2016–17 Arkansas Razorbacks men's basketball team|Arkansas]], [[2016–17 Butler Bulldogs men's basketball team|Butler]], and [[2016–17 Kentucky Wildcats men's basketball team|Kentucky]] to earn a trip to their 20th Final Four. In a matchup against [[2016–17 Oregon Ducks men's basketball team|Oregon]], the Tar Heels won 77-76 to advance to the [[2017 NCAA Men's Division I Basketball Championship Game|National Championship]] against [[2016–17 Gonzaga Bulldogs men's basketball team|Gonzaga]] where they won, 71-65.

==Departures==
{| class="wikitable sortable" border="1"
! Name !! Number !! Pos. !! Height !! Weight !! Year !! Hometown !! class="unsortable" | Notes
|-
| sortname|Nate Britt|| 0 || [[Guard (basketball)|G]] || 6'1" || 175 || Senior || [[Upper Marlboro, Maryland]] || Graduated
|-
| sortname|[[Kennedy Meeks]]|| 3 || [[Forward (basketball)|F]] || 6'10" || 260 || Senior || [[Charlotte, North Carolina]] || Graduated
|-
| sortname|[[Isaiah Hicks]]|| 4 || [[Forward (basketball)|F]] || 6'9" || 235 || Senior || [[Oxford, North Carolina]] || Graduated
|-
| sortname|Kanler Coker|| 13 || [[Guard (basketball)|G]] || 6'4" || 200 || Senior || [[Gainesville, Georgia]] || Graduated
|-
|sortname|Stilman White|| 30 || [[Guard (basketball)|G]] || 6'1" || 175 || Senior || [[Wilmington, North Carolina]] || Graduated 
|}

==2017 recruiting class==
{{College Athlete Recruit Start|40=no|collapse=no|year=2017}}
{{College Athlete Recruit Entry
| recruit  = Jalek Felton
| position = [[Shooting Guard (basketball)|SG]]
| hometown = [[Mullins, SC]]
| highschool = Gray Collegiate Academy
| feet   = 6
| inches = 3
| weight = 180
| 40     =
| commitdate = 12/30/2014
| scout stars  = 5
| rivals stars = 5
| 247 stars = 4
| espn stars = 5
}}
{{College Athlete Recruit Entry
| recruit  = Brandon Huffman 
| position = [[Center (basketball)|C]]
| hometown = [[Anchorage, AK]]
| highschool = [[Word of God Christian Academy]]
| feet   = 6
| inches = 9
| weight = 250
| 40     =
| commitdate = 9/26/16
| scout stars  = 3
| rivals stars = 3
| 247 stars = 3
| espn stars = 4
}}
{{College Athlete Recruit Entry
| recruit  = Andrew Platek
| position = [[Shooting Guard|SG]]
| hometown = [[Guilderland, NY]]
| highschool = [[Northfield Mount Hermon]]
| feet   = 6
| inches = 4
| weight = 180
| 40     =
| commitdate = 7/18/16
| scout stars  = 3
| rivals stars = 3
| 247 stars = 3
| espn stars = 4
}}
{{College Athlete Recruit Entry
| recruit  = Sterling Manley 
| position = [[Center (basketball)|C]]
| hometown = [[Pickerington, OH]]
| highschool = [[Pickerington Central High School]]
| feet   = 6
| inches = 10
| weight = 220
| 40     =
| commitdate = 10/16/16
| scout stars  = 3
| rivals stars = 3
| 247 stars = 3
| espn stars = 3
}}
{{College Athlete Recruit End
| 40               =
| year             = 
| rivals ref title = 
| scout ref title  = 
| espn ref title   = 
| rivals school    = 
| scout s          = 
| espn schoolid    = 
| scout overall    = #22
| rivals overall   = #34
| 247 overall = #29
| espn overall     = #14
| accessdate  = 04/04/2017
| bball            = yes

}}

==Roster==
{{CBB roster/Header|year=2017|team=North Carolina Tar Heels|teamcolors=y|high_school=yes}}
{{CBB roster/Player|first=Jalek|last=Felton|num=--|pos=G|ft=6|in=3|lbs=180|class=FR|rs=|inj=|home=[[Mullins, South Carolina]]|high_school=Gray Collegiate Academy}}
{{CBB roster/Player|first=Brandon|last=Huffman|num=--|pos=C|ft=6|in=9|lbs=250|class=FR|rs=|inj=|home=[[Anchorage, Alaska]]|high_school=[[Word of God Christian Academy]]}}
{{CBB roster/Player|first=Andrew|last=Platek|num=--|pos=G|ft=6|in=4|lbs=180|class=FR|rs=|inj=|home=[[Guilderland, New York]]|high_school=[[Northfield Mount Hermon]]}}
{{CBB roster/Player|first=Sterling|last=Manley|num=--|pos=C|ft=6|in=10|lbs=220|class=FR|rs=|inj=|home=[[Pickerington, Ohio]]|high_school=[[Pickerington Central High School]]}}
{{CBB roster/Player|first=Theo|last=Pinson|link=y|num=1|pos=F/G|ft=6|in=6|lbs=205|class=SR|rs=|inj=|home=[[Greensboro, North Carolina]]|high_school=[[Wesleyan Christian Academy]]}}
{{CBB roster/Player|first=Joel|last=Berry II|link=y|num=2|pos=G|ft=6|in=0|lbs=195|class=SR|rs=|inj=|home=[[Apopka, Florida]]|high_school=[[Lake Highland Prep]]}}
{{CBB roster/Player|first=Tony|last=Bradley|num=5|pos=C|ft=6|in=10|lbs=235|class=SO|rs=|inj=|home=[[Bartow, Florida]]|high_school=[[Bartow High School]]}}
{{CBB roster/Player|first=Shea|last=Rush|num=11|pos=SF|ft=6|in=6|lbs=185|class=SO|rs=|inj=|home=[[Kansas City, Missouri]]|high_school=[[The Barstow School]] |note=W}}
{{CBB roster/Player|first=Brandon|last=Robinson|link=n|num=14|pos=G|ft=6|in=5|lbs=170|class=SO|rs=|inj=|home=[[Douglasville, Georgia]]|high_school=[[Douglas County High School (Douglasville, Georgia)|Douglas County High School]]}}
{{CBB roster/Player|first=Seventh|last=Woods|link=y|line=n|num=21|pos=G|ft=6|in=1|lbs=185|class=SO|rs=|inj=|home=[[Columbia, South Carolina]]|high_school=[[Hammond School (South Carolina)|Hammond School]]|Columbia]]}}
{{CBB roster/Player|first=Kenny|last=Williams|link=n|num=24|pos=G|ft=6|in=4|lbs=175|class=JR|rs=|inj=|home=[[Midlothian, Virginia]]|high_school=[[L.C. Bird High School|L.C. Bird]]}}
{{CBB roster/Player|first=Aaron|last=Rohlman|link=n|num=25|pos=F|ft=6|in=6|lbs=210|class=SR|home=[[Gastonia, North Carolina]]|high_school=[[Hunter Huss High School|Hunter Huss]]|note=W}}
{{CBB roster/Player|first=Luke|last=Maye|link=y|num=32|pos=F|ft=6|in=7|lbs=230|class=JR|rs=|inj=|home=[[Huntersville, North Carolina]]|high_school=[[William A. Hough High School|Hough]]}}
{{CBB roster/Player|first=Justin|last=Jackson|dab=basketball, born 1995|link=y|num=44|pos=F/G|ft=6|in=8|lbs=200|class=SR|rs=|inj=|home=[[Tomball, Texas]]|high_school=Homeschool Christian Youth Assoc.}}
{{CBB roster/Footer
|head_coach=
* [[Roy Williams (coach)|Roy Williams]] ({{college|UNC}})
|asst_coach=
* [[Steve Robinson (basketball coach)|Steve Robinson]] ({{college|Radford}})
* [[Hubert Davis]] ({{college|UNC}})
| roster_url=http://www.goheels.com/SportSelect.dbml?SITE=UNC&DB_OEM_ID=3350&SPID=12965&SPSID=667867
|accessdate=
}}

==Schedule and results==
{{CBB schedule start|gamehighs=yes|attend=yes}}
{{CBB schedule entry
| date         = 
| time         = 
| w/l          = 
| nonconf      = 
| rank         = 
| opponent     = 
| site_stadium = 
| site_cityst  =
| highscorer   = 
| points       = 
| highrebounder   =
| rebounds     = 
| highassister = 
| assists      = 
| score        = 
| record       = 
}}
|-

|-
{{CBB schedule end
|poll=[[AP Poll]]
|timezone=[[Eastern Time Zone|Eastern Time]]
|region=|regionname=}}

==Rankings==
{{See also|2017–18 NCAA Division I men's basketball rankings}}
{{Ranking Movements
| poll1title     = [[AP Poll|AP]]
| poll1firstweek = 0
| poll1lastweek  =18
| poll1_0=
| poll1_1=
| poll1_2=
| poll1_3=
| poll1_4=
| poll1_5=
| poll1_6=
| poll1_7=
| poll1_8=
| poll1_9=
| poll1_10=
| poll1_11=
| poll1_12=
| poll1_13=
| poll1_14=
| poll1_15=
| poll1_16=
| poll1_17=
| poll1_18=
| poll1_19=
| poll2title     = [[Coaches Poll|Coaches]]
| poll2firstweek = 0
| poll2lastweek  =19
| poll2_0=
| poll2_1=
| poll2_2=
| poll2_3=
| poll2_4=
| poll2_5=
| poll2_6=
| poll2_7=
| poll2_8=
| poll2_9=
| poll2_10=
| poll2_11=
| poll2_12=
| poll2_13=
| poll2_14=
| poll2_15=
| poll2_16=
| poll2_17=
| poll2_18=
| poll2_19=
| poll2_20=
| poll3title     = <!-- [[The Poll]] --> 
| poll3firstweek = 
| poll3lastweek  =
| poll3_0=
| poll3_1=
| poll3_2=
| poll3_3=
| poll3_4=
| poll3_5=
| poll3_6=
| poll3_7=
| poll3_8=
| poll3_9=
| poll3_10=
| poll3_11=
| poll3_12=
| poll3_13=
| poll3_14=
| poll3_15=
| poll3_16=
| poll3_17=
| poll3_18=
| poll3_19=
| poll3_20=
| poll4title     = <!-- [[The Poll]] --> 
| poll4firstweek = 
| poll4lastweek  =
| poll4_0=
| poll4_1=
| poll4_2=
| poll4_3=
| poll4_4=
| poll4_5=
| poll4_6=
| poll4_7=
| poll4_8=
| poll4_9=
| poll4_10=
| poll4_11=
| poll4_12=
| poll4_13=
| poll4_14=
| poll4_15=
| poll4_16=
| poll4_17=
| poll4_18=
| poll4_19=
| poll4_20=
| poll5title     = <!-- [[The Poll]] --> 
| poll5firstweek = 
| poll5lastweek  =
| poll5_0=
| poll5_1=
| poll5_2=
| poll5_3=
| poll5_4=
| poll5_5=
| poll5_6=
| poll5_7=
| poll5_8=
| poll5_9=
| poll5_10=
| poll5_11=
| poll5_12=
| poll5_13=
| poll5_14=
| poll5_15=
| poll5_16=
| poll5_17=
| poll5_18=
| poll5_19=
| poll5_20=
}}
<nowiki>*</nowiki>AP does not release post-NCAA Tournament rankings

==References==
{{Reflist}}

{{North Carolina Tar Heels men's basketball navbox}}

{{DEFAULTSORT:2017-18 North Carolina Tar Heels men's basketball team}}
[[Category:North Carolina Tar Heels men's basketball seasons]]
[[Category:2017–18 Atlantic Coast Conference men's basketball season|North Carolina]]