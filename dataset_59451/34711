{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = 75
| Type       = live
| Artist     = [[Joe Zawinul]]
| Border     = yes
| Cover      = Joe Zawinul, 75 album cover.jpg
| Alt        = 
| Released   = 24 September 2008
| Recorded   = 7 July 2007 in [[Lugano]], [[Switzerland]] and 2 August 2007 in [[Veszprém]], [[Hungary]]
| Genre      = [[Jazz]]
| Length     = {{Duration|m=93|s=00}}
| Label      = [[Victor Entertainment|JVC Compact Discs]]
| Producer   = Joachim Becker
| Last album = ''Brown Street''<br />(2006)
| This album = '''''75'''''<br />(2008)
| Next album = ''Absolute Zawinul''<br />(2010)
}}
'''''75''''' is a [[live album]] by Austrian-American jazz musician [[Joe Zawinul]] and his band the Zawinul Syndicate. It was recorded in 2007 at two performances in Switzerland and Hungary, among [[bandleader]] Joe Zawinul's final performances. The album was produced by Joachim Becker and originally released in 2008 by [[Victor Entertainment|JVC Compact Discs]], with the Zawinul Estate and Becker serving as [[executive producer]]s. It was later released by BHM Productions and [[Heads Up International]], the BHM release with the alternate title '''''75th'''''. It peaked at number eighteen on ''[[Billboard (magazine)|Billboard]]''{{'s}} Top Jazz Albums chart and won the [[52nd Grammy Awards|2010]] [[Grammy Award]] for [[Grammy Award for Best Contemporary Jazz Album|Best Contemporary Jazz Album]].<ref name="chart"/> The album received a generally positive critical reception. 

==Overview==
With the exception of one track, ''75'' was recorded during the Zawinul Syndicate's 7&nbsp;July&nbsp;2007 appearance at a festival in [[Lugano]], Switzerland, which happened to be bandleader Joe Zawinul's seventy-fifth birthday.<ref name="Wilkins">{{cite web|url=http://www.allaboutjazz.com/php/article.php?id=31825|title=Joe Zawinul & The Zawinul Syndicate ''75''|last=Wilkins|first=Woodrow|date=17 February 2009|publisher=[[All About Jazz]]|accessdate=6 May 2011}}</ref> The concert was a part of the Zawinul Syndicate's twentieth anniversary world tour. The remaining track, "In a Silent Way", was recorded from their 2&nbsp;August&nbsp;2007 show in [[Veszprém]], Hungary. Zawinul was joined on stage by [[Wayne Shorter]] on [[soprano saxophone]] for this track. This marked a reunion for Zawinul and Shorter, two original members of [[Weather Report]], both of whom played on the original version of this song from [[Miles Davis]]'s 1969 [[In a Silent Way|album of the same name]].<ref name="Milkowski">{{cite web|url=http://jazztimes.com/articles/21163-75-joe-zawinul-the-zawinul-syndicate|title=''75'' Joe Zawinul & the Zawinul Syndicate|last=Milkowski|first=Bill|date=January–February 2009|work=[[JazzTimes]]|accessdate=6 May 2011}}</ref> Shortly after these performances, on 11&nbsp;September&nbsp;2007, Zawinul died of [[Merkel cell carcinoma]].<ref name="Kelman">{{cite web|url=http://www.allaboutjazz.com/php/article.php?id=32161|title=Joe Zawinul & The Zawinul Syndicate ''75''|last=Kelman|first=John|date=13 March 2009|publisher=[[All About Jazz]]|accessdate=6 May 2011}}</ref> The Veszprém concert was Zawinul's penultimate performance.<ref name="Milkowski" />

==Composition==
[[File:Joe zawinul 2007-03-28 live in freiburg.jpg|right|thumb|[[Joe Zawinul]] (''pictured in 2007'') died shortly after ''75'' was recorded.]]

''75'' opens with "Orient Express" from Zawinul's 1992 [[solo album]] ''[[My People (Joe Zawinul album)|My People]]''. Zawinul plays the [[vocoder]] on this track.<ref name="Milkowski" /> The second track, "Madagascar", also features Zawinul on vocoder and is one of two tracks that originally appeared on [[Weather Report]]'s album ''[[Night Passage (album)|Night Passage]]''.<ref name="Milkowski" /> Another Weather Report piece, "Scarlet Woman", follows and features a bass solo by Linley Marthe.<ref name="Wilkins" /> "Zansa II" is a duet with [[Paco Sery]] on [[kalimba]] and Zawinul on [[synthesizer]]<ref name="Milkowski" /> and vocoder.<ref name="Nastos" /> The first disc concludes with "Cafe Andalusia". Sabine Kabongo provides [[scat singing|scat vocals]] on this track.<ref name="Milkowski" />

A combination of two Weather Report pieces "Fast City" and "Two Lines" opens disc two and features more scat singing by Kabongo.<ref name="Wilkins" /> Next, "Clario" features vocals by Alegre Corrêa.<ref name="Milkowski" />  Another melding of Weather Report tunes, "Badia" and "Boogie Woogie Waltz", follows and features Corrêa on [[Berimbau]] and Kabongo on vocals.<ref name="Milkowski" /> The next track is a recording of Kabungo leading the audience in a chorus of "[[Happy Birthday to You|Happy Birthday]]" directed at Zawinul.<ref name="Milkowski" /> "In a Silent Way", a duet between Shorter and Zawinul originally from [[Miles Davis]]'s [[In a Silent Way|album of the same name]], follows. The album closed with "Hymn", which seemed to one reviewer "as though [Zawinul] knew the end was near".<ref name="Milkowski" /><ref name="Kelman" />

==Critical reception==
{{Album ratings
| subtitle =''75''
| rev1 =[[Allmusic]]
| rev1Score ={{Rating|4|5}}<ref name="Nastos">{{cite web|url=http://www.allmusic.com/album/75-r1440922/review|title=Review: ''75''|last=Nastos|first=Michael G.|work=[[Allmusic]]|publisher=[[All Media Guide]]|accessdate=6 May 2011}}</ref>
| rev2 =''[[The Times]]''
| rev2Score ={{Rating|4|5}}<ref name="Bungey">{{cite news|url=http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/music/cd_reviews/article5081008.ece|title=Joe Zawinul: ''75th'' review|last=Bungey|first=John|date=8 November 2008|work=[[The Times]]|publisher=[[News Corporation]]|issn=0140-0460|accessdate=6 May 2011|location=London, United Kingdom}}</ref>
}}
''75'' received a generally positive critical reception. Michael G. Nastos of [[Allmusic]] wrote that the album exemplified Zawinul's "personalized direction" before he died and that it "exudes all of the energy the group produced in concert".<ref name="Nastos" /> ''[[JazzTimes]]''{{'s}} Bill Milkowski described Zawinul's keyboard playing as creating "dazzling, free-flowing lines with the right hand while deftly orchestrating dense chords and Ellingtonian shout choruses with the left hand".<ref name="Milkowski" /> [[All About Jazz]]'s Woodrow Wilkins called the album a "musical adventure" and Zawinul's performance "a testament to his talent and dedication in sharing his gift".<ref name="Wilkins" /> John Kelman, managing editor for All About Jazz, wrote that based on his performance Zawinul gave "no indicators that he was ill, let alone approaching death". He closed his review by calling ''75'' a "fitting finale to the career of an artist whose creativity, forward thinking and extensive discography mean that he may be gone, but he'll never be forgotten."<ref name="Kelman" /> In the ''[[Pittsburgh Tribune-Review]]'', Bob Karlovitis called the release "a great statement about [Zawinul's] creativity". He described the album's opening piece, "Orient Express" as "almost tiring in its energy".<ref name="Karlovits">{{cite news|url=http://www.pittsburghlive.com/x/pittsburghtrib/ae/s_613584.html|title=''75'' is a fitting testament to Zawinul's talent|last=Karlovits|first=Bob|date=1 March 2009|work=[[Pittsburgh Tribune-Review]]|accessdate=6 May 2011|location=Pittsburgh, Pennsylvania|publisher=Tribune-Review Publishing Company}}</ref>

The [[BBC]]'s Jon Lusk did not share the high opinions of other critics. He was "not mad about" vocalists Aziz Sahmaoui and Sabine Kabongo but found Alegre Corrêa "agreeable enough". He liked "In a Silent Way", calling it "beautifully serene" but wished there were other performances with similar "reflective moments".<ref name="Lusk">{{cite web|url=http://www.bbc.co.uk/music/reviews/jcqv|title=Review of Joe Zawinul – ''75th''|last=Lusk|first=Jon|date=28 October 2008|publisher=[[BBC Online]]|accessdate=6 May 2011}}</ref> The review in ''[[The Times]]'' by John Bungey was more positive. He noted that it was not a "generally sad affair, hard-to-take document" as are most final recordings of great artists, but instead "a compelling last testament of a mighty group and a fine human being".<ref name="Bungey" /> Nick Coleman's review in ''[[The Independent]]'' was mixed; he wrote that the "tempos border on the frantic, phrases are spat, the will to trade licks is never less than testosteronal" but quipped that for "every sublime passage there's a butch one".<ref name="Coleman">{{cite news|url=http://www.independent.co.uk/arts-entertainment/music/reviews/album-joe-zawinul--the-zawinul-syndicate-75th-bhm-974107.html|title=Joe Zawinul & the Zawinul Syndicate, ''75th'', (BHM)|last=Coleman|first=Nick|date=26 October 2008|work=[[The Independent]]|publisher=Independent Print Limited|location=London, United Kingdom|accessdate=6 May 2011|issn=0951-9467|oclc=185201487}}</ref> John Fordham of ''[[The Guardian]]'' contrasted the release to Zawinul's 2005 live album ''Vienna Nights''. One difference he emphasized was "the typhoon drumming of Paco Sery and a battalion of percussionists [that] gives Zawinul the option of letting long stretches of the music simply groove". He also noted that there was no comparable track with the duet with Shorter on ''Vienna Nights''.<ref name="Fordham">{{cite news|url=https://www.theguardian.com/music/2008/oct/24/zawinul|title=Joe Zawinul and the Zawinul Syndicate, ''75th''|last=Fordham|first=John|date=24 October 2008|work=[[The Guardian]]|accessdate=6 May 2011|issn=0261-3077|location=London, United Kingdom|oclc=60623878|publisher=[[Guardian Media Group]]}}</ref>

==Track listing==
{{Tracklist
| headline = Disc One
| writing_credits =yes
| title1 = Introduction to Orient Express
| length1 = 3:10
| writer1=[[Joe Zawinul]]
| note1=originally from ''[[My People (Joe Zawinul album)|My People]]''
| title2 = Orient Express
| length2 = 10:07
| writer2 = Zawinul
| note2 = originally from ''My People''
| title3 = Madagascar
| writer3 = Zawinul
| note3 = originally from ''[[Night Passage (album)|Night Passage]]''
| length3 = 10:00
| title4 = Scarlet Woman
| length4 = 6:55
| writer4= [[Alphonso Johnson]], [[Wayne Shorter]], Zawinul
| note4= originally from ''[[Mysterious Traveller]]''
| title5 = Zansa II
| length5 = 6:39
| writer5 = [[Paco Sery]], Zawinul
| note5 = originally from ''World Tour''
| title6 = Cafe Andalusia
| length6 = 8:52
| writer6 = Zawinul
| note6= originally from ''[[Faces & Places]]''
}}
{{Tracklist
| headline = Disc Two
| writing_credits =yes
| title1 = Fast City / Two Lines
| length1 = 12:37
| writer1 = Zawinul
| note1 = originally from ''Night Passage'' / ''[[Procession (album)|Procession]]''
| title2 = Clario
| length2 = 5:45
| writer2 = Elomar
| title3 = Badia / Boogie Woogie Waltz
| length3 = 10:16
| writer3 = Zawinul
| note3 = originally from ''[[Tale Spinnin']]'' / ''[[Sweetnighter]]''
| title4 = [[Happy Birthday to You|Happy Birthday]]
| length4 = 1:39
| writer4 = traditional
| title5 = In a Silent Way
| length5 = 14:20
| writer5 = Zawinul
| note5 = originally from ''[[In a Silent Way]]''
| title6 = Hymn
| length6 = 3:30
| writer6 = traditional
}}

==Credits==
[[Image:Wayne Shorter.jpg|right|thumb|[[Wayne Shorter]] joined Joe Zawinul for a duet on "In a Silent Way".]]
;Performance
* [[Joe Zawinul]]&nbsp;– [[electronic keyboard|keyboards]], [[vocoder]]
* Jorge Bezerra&nbsp;– [[percussion instrument|percussion]], [[singing|vocals]]
* Alegre Corrêa&nbsp;– [[berimbau]], [[guitar]], vocals
* Sabine Kabongo&nbsp;– percussion, vocals
* Linley Marthe&nbsp;– [[bass (instrument)|bass]]
* Abdelaziz Sahmaoui&nbsp;– percussion, vocals
* [[Paco Sery]]&nbsp;– [[drum kit|drums]], [[kalimba]], vocals
* [[Wayne Shorter]]&nbsp;– [[Soprano saxophone|soprano sax]] on "In a Silent Way"

;Production
* Joachim Becker&nbsp;– [[executive producer]], [[Audio mixing (recorded music)|mixing]], [[Record producer|producer]]
* Klaus Genuit&nbsp;– mixing
* Holger Keifel&nbsp;– [[portrait]]s
* Knut Schotteldreier&nbsp;– [[album cover|cover design]]
* Matjaz Vrecko &nbsp;– [[photos]]

Credits adapted from Allmusic and album liner notes.<ref name="Nastos"/><ref name=LinerNotes>{{cite AV media notes|title=75|others=Joe Zawinul and the Zawinul Syndicate|year=2008|type=CD insert|publisher=[[Heads Up International]]}}</ref>

==Charts==
''75'' reached a peak position of number eighteen on ''[[Billboard (magazine)|Billboard]]''{{'s}} Top Jazz Albums chart.<ref name="chart">{{cite web|url=http://www.allmusic.com/album/75-r1440922/charts-awards|title=75: Charts & Awards|publisher=Allmusic|accessdate=6 May 2011}}</ref>

{| class="wikitable"
|-
! Year
! Chart
! Peak position
|-
| 2009
| ''[[Billboard (magazine)|Billboard]]''{{'s}} Top Jazz Albums
|align=center| 18
|}

==Awards==
The album won the [[52nd Grammy Awards|2010]] [[Grammy Award]] for [[Grammy Award for Best Contemporary Jazz Album|Best Contemporary Jazz Album]].<ref name="grammy win">{{cite news|url=http://www.victoriaadvocate.com/news/2010/jan/31/bc-mus-grammys-jazzla-_-entertainment-300-words/?entertainment&national-entertainment|title=Familiar names dot jazz categories|last=Barton|first=Chris|date=31 January 2010|work=[[The Victoria Advocate]]|accessdate=6 May 2011|location=Victoria, Texas| archiveurl= https://web.archive.org/web/20110512013534/http://www.victoriaadvocate.com/news/2010/jan/31/bc-mus-grammys-jazzla-_-entertainment-300-words/?entertainment&national-entertainment| archivedate= 12 May 2011 <!--DASHBot-->| deadurl= no}}</ref> The other nominees for the award were ''[[Urbanus (album)|Urbanus]]'' by [[Stefon Harris]], ''[[Sounding Point]]'' by [[Julian Lage]], ''[[At World's Edge]]'' by [[Philippe Saisse]], and ''[[Big Neighborhood]]'' by [[Mike Stern]].<ref name="Levine">{{cite web|url=http://www.voanews.com/english/news/arts-and-entertainment/A-Look-at-This-Years-Grammy-Award-Jazz-Nominees--82727857.html |title=A Look at This Year's Grammy Award Jazz Nominees |last=Levine |first=Doug |date=26 January 2010 |publisher=[[Voice of America]] |accessdate=6 May 2011 |archiveurl=https://web.archive.org/web/20110511180455/http://www.voanews.com/english/news/arts-and-entertainment/A-Look-at-This-Years-Grammy-Award-Jazz-Nominees--82727857.html |archivedate=11 May 2011 |deadurl=yes |df= }}</ref>

==Release history==
{| class="wikitable"
|-
! Date
! Type
! Title
! Label
!Catalog #
|-
| 24 September 2008
| rowspan="3"|[[Compact Disc|CD]]
| ''75''
| [[Victor Entertainment|JVC Compact Discs]]
|61575/6<ref name="Nastos" />
|-
|24 October 2008
|''75th''
| BHM Productions
| 4002-2<ref>{{cite web|url=http://www.bhm-music.de/bhm/en/releases_detail.php?PHPSESSID=be2f76fbc25cb75e5c3db523e59487b8&s_name=4&upc=%3F%3F%3F&PHPSESSID=be2f76fbc25cb75e5c3db523e59487b8|title=Joe Zawinul ''75th''|publisher=BHM Productions|accessdate=6 May 2011}}</ref>
|-
| 24 February 2009
|''75''
|[[Heads Up International|Heads Up Records]]
|3162-25<ref>{{cite web|url=http://www.concordmusicgroup.com/albums/75/|title=''75'' Joe Zawinul|publisher=[[Concord Music Group]]|accessdate=6 May 2011}}</ref>
|-
|}

==References==
{{Reflist|2}}

==External links==
{{Portal|Jazz}}
* [http://www.jazzpolice.com/content/view/8177/79/ Jazz Police review by Don Berryman] (2009)

{{Good article}}

[[Category:2008 live albums]]
[[Category:Grammy Award for Best Contemporary Jazz Album]]
[[Category:Heads Up International live albums]]
[[Category:Joe Zawinul live albums]]
[[Category:Live albums published posthumously]]
[[Category:Live jazz albums]]
[[Category:Victor Entertainment live albums]]