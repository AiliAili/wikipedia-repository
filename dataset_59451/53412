{{Infobox Officeholder
|name=George Aubrey Burton, Jr.
|image=George_A._Burton,_Jr.,_of_Shreveport,_LA.jpg
|image_size=175px
|office= Finance Commissioner of [[Shreveport, Louisiana|Shreveport]], [[Caddo Parish]], [[Louisiana]]
|party=[[Republican Party (United States)|Republican Party]] 
|term_start=December 14, 1971
|term_end=December 31, 1978
|preceded= Dwight E. Saur
|succeeded= Position abolished by change in city charter
|birth_date= June 21, 1926
|birth_place= [[Texarkana, Arkansas|Texarkana]], [[Miller County, Arkansas|Miller County]], [[Arkansas]], [[United States|USA]]
|death_date={{death date and age|2014|6|10|1926|6|21}} 
|death_place=[[Little Rock, Arkansas|Little Rock]], [[Pulaski County, Arkansas|Pulaski County]], Arkansas
|occupation=[[Certified Public Accountant]]
|alma_mater=[[C. E. Byrd High School]]<br>
[[Centenary College of Louisiana]]
|residence= [[Shreveport, Louisiana|Shreveport]], [[Caddo Parish, Louisiana|Caddo Parish]], [[Louisiana]]
|spouse= (1) Joan Cunningham Burton (deceased)
(2) Gloria Brantley Garvin Burton (divorced, deceased)
|children=George A. Burton, III (deceased)<br/>
Sandra Burton Batten
|religion=[[United Methodist Church]]
|branch=[[United States Navy]]
|battles=[[Asiatic-Pacific Theater|Pacific Theater of Operations]] of [[World War II]]
}}
'''George Aubrey Burton, Jr.''' (June 21, 1926 &ndash; June 10, 2014), was a [[Certified Public Accountant]] and the last elected municipal finance commissioner in [[Shreveport, Louisiana|Shreveport]], [[Louisiana]], a position he filled from 1971 to 1978. He was the first [[Republican Party (United States)|Republican]] since [[Reconstruction era of the United States|Reconstruction]] to have been elected to citywide office in Shreveport.<ref name=obit>{{cite web|url=http://www.legacy.com/obituaries/shreveporttimes/obituary.aspx?n=george-a-burton&pid=171346250|title=George A. Burton, Jr. obituary|publisher=''[[The Times (Shreveport)|The Shreveport Times]]''|accessdate=June 15, 2014}}</ref>

==Early years, education, military==

Burton was born in [[Texarkana, Arkansas|Texarkana]], [[Arkansas]], to George A. Burton, Sr. (1903–1980) and the former Theo Simmons (1908–1983). The Burtons thereafter resided in [[Alexandria, Louisiana|Alexandria]] in [[Central Louisiana]] and relocated to  Shreveport in 1935.<ref name=obit/> Burton's grandfather was a railroad employee, and his father was secretary-treasurer of Cahn Electric Company in Shreveport.

Burton attended Line Avenue Elementary School in Shreveport and graduated in 1941 at the age of fifteen from [[C.E. Byrd High School]] in Shreveport. He enlisted in the [[United States Navy]] and entered the [[Seabees]] at the age of sixteen. From 1943 to 1946, he served in the [[Asiatic-Pacific Theater|Pacific Theater of Operations]] of [[World War II]]. He was a life member of [[Disabled American Veterans]]. He obtained a [[Bachelor of Science]] degree in accounting from [[Centenary College of Louisiana|Centenary College]], where he later taught accounting. He passed the C.P.A. examination in 1952.<ref name=obit/><ref name=whoswho/>

After graduation from Centenary College, he was employed by the Operkuch, McGuirt, Watts and West accounting firm, of which he became a partner in 1953. However, he left the company in 1954 to begin his own firm. He was active in the American Institute Of CPA's, the Louisiana Society of CPA's, and the Association of Governmental Accountants.<ref name=obit/>

==Political life==

A lifelong Republican, he ran unsuccessfully on November 8, 1966, for a seat on the [[Caddo Parish School Board]], as did his fellow Republican [[George Despot]], later the state party chairman.<ref>''The Shreveport Times'', November 9, 1966</ref> Her subsequently served on the Bond Issue and Rapid Learner study committees of the Caddo school board/<ref name=obit/>

In September 1971, Shreveport Finance Commissioner Dwight E. Saur, a [[Democratic Party (United States)|Democrat]], died after serving less than a year of his second elected term. Burton won the [[special election]] to fill the remaining three years of the term at took office on December 14, 1971.<ref>"Sworn in as finance commissioner", ''The Shreveport Times'', December 14, 1971</ref> Despite his overt Republicanism, Burton carved his niche with his Democratic colleagues. In 1974, Burton was reelected finance commissioner with 17,488 votes (68.8 percent) over Democrat (later Republican) David R. Carroll (1926-2011), a [[Mississippi]] native and a Caddo Parish police juror,<ref>{{cite web|url=http://www.legacy.com/obituaries/shreveporttimes/obituary.aspx?n=david-r-carroll&pid=152770391|title=David R. Carroll obituary|publisher=''[[The Times (Shreveport)|Shreveport Times]]'', July 27, 2011|accessdate=August 4, 2011}}</ref> received 7,938 ballots (31.2 percent). That year, Burton had the tacit support of Democratic [[Mayor of Shreveport, Louisiana|Mayor]] [[Calhoun Allen]], who won a second term. Under the [[City commission|commission]] form of government then in effect, changed in November 1978 to the [[mayor-council]] format, the mayor was technically the "commissioner of administration."

Running with Burton in 1974 was another Republican, [[Billy Guin]], who waged his second campaign against Public Utilities Commissioner William "Bill" Collins and polled 43 percent of the vote.<ref>''[[Shreveport Journal]]'', November 6, 1974</ref> Collins resigned in 1977, and Guin won the seat in a special election. Guin was in effect only the second Republican in modern times to hold municipal office in Shreveport. Serving with Burton were Public Safety Commissioner [[George W. D'Artois]] and Public Works Commissioner [[Don Hathaway]], both Democrats.

D'Artois was forced from office in a multi-faceted political scandal that surfaced in the spring of 1976. At one point before [[Attorney General of Louisiana|Attorney General]] [[William J. Guste]] was called to investigate, the five commissioners, including D'Artois, were conducting their own probe; "the city investigating itself," said the critics. Burton spoke out against the situation, which he found particularly alarming: "What's going on now is devastating to the city's image. I will favor anything that brings some final disposition to this matter. The city cannot move on anything until this is cleared up. We couldn't pull a public election right now endorsing motherhood."<ref>{{cite book|url=https://books.google.com/books?id=NvpN1t9TMrgC&pg=PA81&lpg=PA81&dq=George+Wendell+D'Artois&source=bl&ots=FLoGsv9uIL&sig=M4vFHkw4ijLolZvKDlHRFDjmACg&hl=en&sa=X&ei=w0-XU5b6FuqH8AHr-YCoCQ&ved=0CEAQ6AEwBA#v=onepage&q=George%20Wendell%20D'Artois&f=false|title=The Commissioner: A True Story of Deceit, Dishonor, and Death|author=[[Bill Keith (Louisiana politician)|Bill Keith]]|publisher=[[Pelican Publishing Company]]|location=[[Gretna, Louisiana|Gretna]], [[Louisiana]]|year=2009|pages=101|isbn=9781-58980-655-9|accessdate=June 18, 2014}}</ref>

From 1960 to 1980, Burton was chairman of the Caddo Parish Republican Executive Committee. From 1972 to 1980, he was secretary of the [[Louisiana Republican Party|Louisiana Republican State Central Committee]], a 144-member body which meets periodically in [[Baton Rouge, Louisiana|Baton Rouge]]. He was president of the Caddo Parish Board of Election Supervisors from 1978 to 2008.<ref name=obit/>

==Family and civic commitments==

Burton married the former Joan (pronounced JO ANN) Cunningham (1928–2002), who lived in  [[Amory, Mississippi|Amory]], [[Mississippi]] and [[Memphis, Tennessee|Memphis]], [[Tennessee]], before her parents, William and Lucille Cunningham, relocated to Shreveport. The couple had two children, son George A. Burton, III (1948–2010), a poet in Shreveport, and his wife Melissa Fowle Burton, and daughter Sandra Burton "Sandy" Batten<ref>{{cite web|url=http://www.legacy.com/obituaries/shreveporttimes/obituary.aspx?n=george-a-burton&pid=140510758|title=Obituary of George A. Burton, III|publisher=''[[The Times (Shreveport)|Shreveport Times]]'', March 9, 2010|accessdate=March 9, 2010}}</ref> (born 1950), the wife of Rick Alan Batten (born 1954) of [[Maumelle, Arkansas|Maumelle]] near [[Little Rock, Arkansas|Little Rock]], Arkansas. After Joan's death, Burton married Gloria Nell Brantley (1930-2013), a native of [[Homer, Louisiana|Homer]], Louisiana, and the widow of Bernard Garvin (1927–1997) of Shreveport. They were no longer married at the time of her death.<ref>{{cite web|url=http://www.legacy.com/obituaries/shreveporttimes/obituary.aspx?pid=165926990|title=Gloria Garvin Obituary|date=July 20, 2013|publisher=''The Shreveport Times|accessdate=June 15, 2014}}</ref>

Burton was a charter member of the Shreveport chapter of [[Sertoma International]] and was highly active in his younger years in the [[Junior Chamber International|Jaycees]] as the Shreveport president, state vice-president, and a national director. He was a founding board member of East Ridge Country Club. He was a director and treasurer of the Shreveport Legal Aid Society. Burton was a member of the [[United Methodist Church]].<ref name=obit/><ref name=whoswho>"Louisiana: Burton, George A.", ''Who's Who in American Politics, 2007-2008'' (Marquis Who's Who: [[New Providence, New Jersey|New Providence]], [[New Jersey]], 2007), p. 653</ref>

He died in Little Rock eleven days before his 88th birthday. A memorial service was held on June 20, 2014 at Noel Memorial United Methodist Chuirch in Shreveport.<ref name=obit/>

==See also==
{{Portalbar|Biography|Arkansas|Louisiana|Politics|Conservatism|Business and Economics|World War II|United States Navy|Methodism}}

*[[John McWilliams Ford]], Shreveport finance commissioner, 1930-1965

==References==
{{Reflist}}

{{DEFAULTSORT:Burton, George A.}}
[[Category:1926 births]]
[[Category:2014 deaths]]
[[Category:People from Texarkana, Arkansas]]
[[Category:Politicians from Alexandria, Louisiana]]
[[Category:Louisiana Republicans]]
[[Category:American accountants]]
[[Category:Louisiana city council members]]
[[Category:Politicians from Shreveport, Louisiana]]
[[Category:American military personnel of World War II]]
[[Category:United States Navy sailors]]
[[Category:C. E. Byrd High School alumni]]
[[Category:Centenary College of Louisiana alumni]]
[[Category:American United Methodists]]