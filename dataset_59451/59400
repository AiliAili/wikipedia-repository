{{Refimprove|date=June 2015}}
{{Infobox journal
| title = Philosophical Explorations
| cover = 
| discipline = [[Philosophy]]
| abbreviation = 
| editor = Anthonie Meijers
| publisher = [[Taylor and Francis]]
| country = [[United Kingdom]]
| frequency = Triannually
| history = 1998–present
| impact = 
| impact-year = 
| website = http://www.tandfonline.com/toc/rpex20/current
| link1 = http://www.tandfonline.com/loi/rpex20
| link1-name = Online archive
| link2 = 
| link2-name = 
| ISSN = 1386-9795
| eISSN = 1741-5918
| OCLC = 901011239
| LCCN = 2001233142
| CODEN = 
}}
'''Philosophical Explorations''' is a peer reviewed [[philosophy]] journal published triannually, specializing in the [[philosophy of mind]] and [[philosophy of action|action]].<ref name="auto">{{cite journal|url=http://www.tandfonline.com/toc/rpex20/current |journal=Philosophical Explorations : An International Journal for the Philosophy of Mind and Action |volume=18 |date=2015 |issn=1386-9795 |title=Introduction: self-knowledge in perspective |publisher=Tandfonline.com |accessdate=2015-06-26}}</ref>

The editors of this journal intend to "publish outstanding articles in the philosophy of mind and action, with an emphasis on issues concerning the interrelations between cognition and agency." They are particularly interested in publishing articles on "philosophy of mind and action and related disciplines such as moral psychology, ethics, philosophical anthropology, social philosophy, political philosophy and philosophy of the social sciences....[and] interdisciplinary [work], establishing bridges between philosophy and, for example, evolutionary biology, neuroscience, psychology, and political science.<ref name="auto1">[http://www.gbhap.com/journals/journal.asp?issn=1386-9795&linktype=1] {{dead link|date=June 2015}}</ref> In 2010, the journal initiated the Philosophical Explorations Essay Prize.<ref>[http://www.tandf.co.uk/journals/pdf/rpex_essay_prize_2011.pdf] {{dead link|date=June 2015}}</ref>

Since 2007, the Editor in Chief is [[Anthonie Meijers]] of [[Eindhoven University of Technology]].  

Submissions to Philosophical Explorations are screened by the (associate) editors and undergo double blind peer review by at least two referees before being accepted for publication.

==References==
{{Reflist}}

==External links==
* [http://www.tandfonline.com/toc/rpex20/current. Link to recent journal content ]
* [http://www.gbhap.com/journals/titles/13869795.asp Philosophical Explorations: Publishers information, [[Taylor and Francis]]]

[[Category:Philosophy journals]]
[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Triannual journals]]


{{philo-journal-stub}}