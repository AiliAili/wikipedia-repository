{{about||the ex-[[Confederate States of America|Confederate]] [[ironclad]] ram|Japanese ironclad Kōtetsu}}
{{good article}}

{{Use dmy dates|date=August 2014}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image= [[File:Azuma1900.jpg|300px]] 
|Ship caption=Colorized photo of ''Azuma'' at anchor, [[Portsmouth]], 1900
}}
{{Infobox ship class overview
|Name=
|Builders=
|Operators={{navy|Empire of Japan}}
|Class before={{ship|Japanese cruiser|Yakumo||2}}
|Class after={{sclass-|Kasuga|cruiser|4}}
|Cost=
}}
{{Infobox ship career
|Hide header=
|Ship country=
|Ship flag=
|Ship name=''Azuma''
|Ship namesake=[[Kantō region]]
|Ship ordered=12 October 1897
|Ship builder= [[Ateliers et Chantiers de la Loire]], [[Saint-Nazaire]], France
|Ship laid down=1 February 1898
|Ship launched= 24 June 1899
|Ship completed= 28 July 1900
|Ship commissioned=  
|Ship decommissioned= 
|Ship reclassified=As 1st class [[Kaibokan|coast-defense ship]], 1 September 1921
|Ship out of service=
|Ship struck=1941
|Ship fate=[[ship breaking|Scrapped]], 1946
}}
{{Infobox ship characteristics
|Hide header=
|Header caption= 
|Ship type= [[Armored cruiser]]
|Ship displacement= {{convert|9278|t|LT|abbr=on}}
|Ship length={{convert|137.9|m|ftin|abbr=on}} ([[Length overall|o/a]])
|Ship beam={{convert|17.74|m|ftin|abbr=on}}
|Ship draft={{convert|7.18|m|ftin|abbr=on}}
|Ship propulsion=*2 Shafts
*2 [[Marine steam engine#Triple or multiple expansion|Vertical triple-expansion steam engines]]
|Ship power=*{{convert|18000|ihp|lk=in|abbr=on}}
*24 [[Belleville boiler]]s
|Ship speed={{convert|21|kn|lk=in}}
|Ship range={{convert|7000|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=670
|Ship armament=*2 × twin [[20.3 cm/45 Type 41 naval gun]]s
*12 × single [[QF 6 inch /40 naval gun]]s
*12 × single [[QF 12 pounder 12 cwt naval gun]]s
*8 × single [[QF 3 pounder Hotchkiss]] guns
*5 × single {{convert|457|mm|in|abbr=on}} [[torpedo tube]]s
|Ship armor=*[[Belt armor|Waterline belt]]: {{convert|89|-|178|mm|in|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|63|mm|in|abbr=on}}
*[[Gun Turret]]: {{convert|160|mm|in|abbr=on}}
*[[Barbette]]: {{convert|152|mm|in|abbr=on}}
*[[Casemate]]: {{convert|51|-|152|mm|in|abbr=on}}
*[[Conning tower]]: {{convert|356|mm|in|abbr=on}}
*[[Bulkhead (partition)|Bulkhead]]: {{convert|76|mm|in|1|abbr=on}}
|Ship notes=
}}
|}
{{nihongo|'''''Azuma'''''|吾妻}} (sometimes transliterated (archaically) as ''Adzuma'') was an [[armored cruiser]] built for the [[Imperial Japanese Navy]] (IJN) in the late 1890s. As Japan lacked the industrial capacity to build such warships herself, the ship was built in France. She participated in most of the naval battles of the [[Russo-Japanese War]] of 1904–05 and was lightly damaged during the [[Battle off Ulsan]] and the [[Battle of Tsushima]]. ''Azuma'' began the first of five training cruises in 1912 and saw no combat during World War I. She was never formally reclassified as a [[training ship]] although she exclusively served in that role from 1921 until she was disarmed and [[hulk (ship)|hulked]] in 1941. ''Azuma'' was badly damaged in an American [[aircraft carrier|carrier]] raid in 1945, and subsequently [[ship breaking|scrapped]] in 1946.

==Background and design==
The 1896 Naval Expansion Plan was made after the [[First Sino-Japanese War]], and included four armored cruisers in addition to four more battleships, all of which had to be ordered from overseas shipyards as Japan lacked the capability to build them itself. Further consideration of the Russian building program caused the IJN to believe that the battleships ordered under the original plan would not be sufficient to counter the [[Imperial Russian Navy]]. Budgetary limitations prevented ordering more battleships and the IJN decided to expand the number of more affordable armored cruisers to be ordered from four to six ships, believing that the recent introduction of tougher [[Krupp cemented armor]] would allow them to stand in the [[line of battle]]. The revised plan is commonly known as the "Six-Six Fleet". The first four ships were built by [[Armstrong Whitworth]] in the United Kingdom, but the last two ships were built in Germany and France. To ensure [[ammunition]] compatibility, the IJN required their builders to use the same British guns as the other four ships.<ref>Evans & Peattie, pp. 57–62</ref> In general, the IJN provided only a sketch design and specifications that each builder had to comply with; otherwise each builder was free to build the ships as they saw fit. Unlike most of their contemporaries which were designed for [[commerce raiding]] or to defend colonies and trade routes, ''Azuma'' and her half-[[sister ship|sisters]] were intended as fleet scouts and to be employed in the [[line of battle|battleline]].<ref>Milanovich, p. 72</ref>

==Description==
[[File:Japanese armored cruiser Azuma left elevation plan.jpg|thumb|left|Left elevation and plan of ''Azuma'' from [[Jane's Fighting Ships]] 1904]]
The ship was {{convert|137.9|m|ftin|sp=us}} long [[length overall|overall]] and {{convert|131.56|m|ftin|sp=us}} [[Length between perpendiculars|between perpendiculars]]. She had a [[beam (nautical)|beam]] of {{convert|17.74|m|ftin|sp=us}} and had an average [[draft (ship)|draft]] of {{convert|7.18|m|ftin|sp=us}}. ''Azuma'' [[Displacement (ship)|displaced]] {{convert|9278|t|LT|sp=us}} at normal load and {{convert|9953|t|LT|sp=us}} at [[deep load]]. The ship had a [[metacentric height]] of {{convert|0.85|m|ftin|sp=us}}. She had a [[double bottom]] and her hull was subdivided into 213 [[watertight compartments]].<ref>Milanovich, pp. 74, 80</ref> Her crew consisted of 670 officers and enlisted men.<ref name=j3>Jentschura, Jung & Mickel, p. 73</ref>

''Azuma'' had two 4-cylinder [[Marine steam engine#Triple or multiple expansion|triple-expansion steam engine]]s, each driving a single [[propeller shaft]].<ref>Milanovich, p. 81</ref> Steam for the engines was provided by 24 [[Belleville boiler]]s and the engines were rated at a total of {{convert|18000|ihp|lk=on}}. The ship had a designed speed of {{convert|21|kn|lk=in}}. She carried up to {{convert|1200|t|sp=us}} of coal and could steam for {{convert|7000|nmi|lk=in}} at a speed of {{convert|10|kn}}.<ref>Jentschura, Jung & Mickel, pp. 73–74</ref>

===Armament===
The [[main battery|main armament]] for all of the "Six-Six Fleet" armored cruisers was four Armstrong Whitworth-built 45-[[caliber (artillery)|caliber]] [[20.3 cm/45 Type 41 naval gun|eight-inch guns]] in twin-[[gun turret]]s fore and aft of the [[superstructure]]. The electrically operated turrets were capable of 130° rotation left and right, and the guns could be elevated to +30° and depressed to −5°. The turret accommodated 65 [[Shell (projectile)|shells]], but could only be reloaded through doors in the turret floor and the ship's [[deck (ship)|deck]] that allowed the electric [[winch]] in the turret to hoist shells up from the shell room deep in the hull.<ref>Milanovich, pp. 76–77</ref> The guns were manually loaded and had a rate of fire about 1.2 [[Cartridge (firearms)|rounds]] per minute. The 203-millimeter gun fired {{convert|113.5|kg|adj=on|sp=us}} [[armor-piercing shot and shell|armor-piercing (AP)]] projectiles at a [[muzzle velocity]] of {{convert|760|m/s|sp=us}} to a range of {{convert|18000|m|yd|sp=us}}.<ref name="Milanovich, p. 78">Milanovich, p. 78</ref>

The [[Battleship secondary armament|secondary armament]] consisted of a dozen [[Elswick Ordnance Company]] "Pattern Z" [[quick-firing gun|quick-firing (QF)]], 40-caliber, 6-inch guns. All but four of these guns were mounted in armored [[casemate]]s on the main and upper decks, and their mounts on the upper deck were protected by [[gun shield]]s. Their {{convert|100|lb|kg|1|adj=on|disp=flip}} AP shells were fired at a muzzle velocity of {{convert|2300|ft/s|m/s|disp=flip|sp=us}}.<ref>Friedman, p. 276; Milanovich, p. 78</ref> ''Azuma'' was also equipped with a dozen 40-caliber [[QF 12 pounder 12 cwt naval gun|QF 12-pounder 12-cwt guns]]<ref group=Note>"Cwt" is the abbreviation for [[hundredweight]], 12 cwt referring to the weight of the gun.</ref> and eight QF 2.5-pounder [[Hotchkiss gun|Yamauchi guns]] as close-range defense against [[torpedo boat]]s.<ref name="Milanovich, p. 78"/> The former gun fired {{convert|3|in|adj=on|0|disp=flip|sp=us}}, {{convert|12.5|lb|adj=on|disp=flip|sp=us}} projectiles at a muzzle velocity of {{convert|2359|ft/s|m/s|disp=flip|sp=us}}.<ref>Friedman, p. 114</ref>

''Azuma'' was equipped with five {{convert|457|mm|in|abbr=on}} [[torpedo tube]]s, one above water in the bow and four submerged tubes, two on each [[broadside]]. The Type 30 torpedo had a {{convert|100|kg|sp=us|adj=on}} [[warhead]] and three range/speed settings: {{convert|800|m|yd|sp=us}} at {{convert|27|kn}}, {{convert|1000|m|yd|sp=us}} at {{convert|23.6|kn}} or {{convert|3000|m|yd|sp=us}} at {{convert|14.2|kn}}.<ref>Milanovich, p. 80</ref>

===Armor===
All of the "Six-Six Fleet" armored cruisers used the same armor scheme with some minor differences, one of which was that the four later ships all used Krupp cemented armor. The [[Belt armor|waterline belt]] ran the full length of the ship and its thickness varied from {{convert|178|mm|in|sp=us}} amidships to {{convert|89|mm|in|sp=us}} at the bow and stern. It had a height of {{convert|2.13|m|ftin|sp=us}}, of which {{convert|1.50|m|ftin|sp=us}} was normally underwater. The upper [[strake]] of belt armor was {{convert|127|mm|in|sp=us}} thick and extended from the upper edge of the waterline belt to the main deck. It extended {{convert|61.49|m|ftin|sp=us}} from the forward to the rear [[barbette]]. ''Azuma'' had only a single transverse 76&nbsp;mm armored [[bulkhead (partition)|bulkhead]] that closed off the forward end of the central armored citadel.<ref>Milanovich, pp. 80–81</ref>

The barbettes, gun turrets and the front of the casemates were all 152 millimeters thick while the sides and rear of the casemates were protected by {{convert|51|mm|in|sp=us}} of armor. The deck was {{convert|63|mm|sp=us}} thick and the armor protecting the [[conning tower]] was {{convert|356|mm|in|sp=us}} in thickness.<ref name=Conway>Chesneau & Kolesnik, p. 225</ref>

==Construction and career==
The contract for ''Azuma'', named after the [[Kantō region]],<ref>Silverstone, p. 326</ref> was signed on 12 October 1897 with [[Ateliers et Chantiers de la Loire]], and the ship was [[keel laying|laid down]] at their shipyard in [[Saint-Nazaire]] on 1 February 1898. She was [[Ship naming and launching|launched]] on 24 June 1898 and completed on 29 July 1900. ''Azuma'' left for Japan the next day and arrived in [[Yokosuka, Kanagawa|Yokosuka]] on 29 October.<ref>Milanovich, p. 73</ref> [[Captain (naval)|Captain]] [[Fujii Kōichi]] assumed command before the start of the Russo-Japanese War in February 1904, until he was relieved in January 1905<ref>Kowner, p. 125</ref> by Captain [[Murakami Kakuichi]].<ref>Corbett, II, p. 153</ref>

===Russo-Japanese War===
At the start of the Russo-Japanese War, ''Azuma'' was assigned to the 2nd Division of the [[2nd Fleet (Imperial Japanese Navy)|2nd Fleet]].<ref>Kowner, p. 465</ref> She participated in the [[Battle of Port Arthur]] on 9 February 1904, when [[Vice Admiral]] [[Tōgō Heihachirō]] led the [[Combined Fleet]] in an attack on the Russian ships of the [[Pacific Fleet (Russia)|Pacific Squadron]] anchored just outside [[Lüshunkou District|Port Arthur]]. Tōgō had expected the surprise night attack by his destroyers to be much more successful than it was, anticipating that the Russians would be badly disorganized and weakened, but they had recovered from their surprise and were ready for his attack. The Japanese ships were spotted by the [[protected cruiser]] {{ship|Russian cruiser|Boyarin|1901|2}}, which was patrolling offshore and alerted the Russians. Tōgō chose to attack the Russian coastal defenses with his main armament and engage the ships with his secondary guns. Splitting his fire proved to be a poor decision as the Japanese eight- and six-inch guns inflicted little damage on the Russian ships, which concentrated all their fire on the Japanese ships with some effect.<ref>Forczyk, pp. 42–43</ref> Although many ships on both sides were hit, Russian casualties numbered some 150, while the Japanese suffered roughly 90 killed and wounded before Tōgō disengaged.<ref>Corbett, I, p. 105</ref>

In early March, Vice Admiral [[Kamimura Hikonojō]] was tasked to take the reinforced 2nd Division north and make a diversion off [[Vladivostok]]. While scouting for Russian ships in the area, the Japanese cruisers bombarded the harbor and defenses of Vladivostok on 6 March to little effect. Upon their return to Japan a few days later, the 2nd Division was ordered to escort the transports ferrying the [[Imperial Guards Division]] to Korea and then to join the ships blockading Port Arthur. Kamimura was ordered north in mid-April to cover the [[Sea of Japan]] and defend the [[Korea Strait]] against any attempt by the Vladivostok Independent Cruiser Squadron, under the command of Rear Admiral [[Karl Jessen]], to break through and unite with the Pacific Squadron. The two units narrowly missed each other on the 24th in heavy fog, and the Japanese proceeded to Vladivostok, where they laid several [[minefield]]s before arriving back at [[Wonsan]] on the 30th.<ref>Corbett, I, pp. 138–39, 142–45, 160, 177, 188–89, 191–96</ref>

The division failed to intercept the Russian squadron as it attacked [[Hitachi Maru Incident|several transports]] south of [[Okinoshima, Shimane|Okinoshima Island]] on 15 June, due to heavy rain and fog. The Russians sortied again on 30 June, and Kamimura finally was able to intercept them the next day near Okinoshima. The light was failing when they were spotted and the Russians were able to disengage in the darkness. Jessen's ships sortied again on 17 July, headed for the eastern coast of Japan to act as a diversion and pull Japanese forces out of the Sea of Japan and the [[Yellow Sea]]. The Russian ships passed through [[Tsugaru Strait]] two days later and began capturing ships bound for Japan. The arrival of the Russians off [[Tokyo Bay]] on the 24th caused the [[Imperial Japanese Navy General Staff|Naval General Staff]] to order Kamimura to sail for Cape Toi Misaki, [[Kyūshū]], fearing that Jessen would circumnavigate Japan to reach Port Arthur. Two days later he was ordered north to the [[Kii Channel]] and then to Tokyo Bay on the 28th. The General Staff finally ordered him back to [[Tsushima Island]] on the 30th; later that day he received word that Jessen's ships had passed through the Tsugaru Strait early that morning and reached Vladivostok on 1 August.<ref>Corbett, I, pp. 283–89, 319–25, 337–51</ref>

====Battle off Ulsan====
{{main|Battle off Ulsan}}

On 10 August, the ships at Port Arthur attempted a breakout to Vladivostok, but were turned back in the [[Battle of the Yellow Sea]]. Jessen was ordered to rendezvous with them, but the order was delayed. His three armored cruisers, {{ship|Russian cruiser|Rossia||2}}, {{ship|Russian cruiser|Gromoboi||2}}, and {{ship|Russian cruiser|Rurik|1892|2}}, had to raise steam, so he did not sortie until the evening of 13 August. By dawn he had reached Tsushima, but turned back when he failed to see any ships from the Port Arthur squadron. {{Convert|36|mi|0}} north of the island he encountered Kamimura's squadron, which consisted of four modern armored cruisers, {{ship|Japanese cruiser|Iwate||2}}, {{ship|Japanese cruiser|Izumo||2}}, {{ship|Japanese cruiser|Tokiwa||2}}, and ''Azuma''. The two squadrons had passed during the night without spotting one another and each had reversed course around first light. This put the Japanese ships astride the Russian route to Vladivostok.<ref>Brook, pp. 34, 37</ref>

Jessen ordered his ships to turn to the northeast when he spotted the Japanese at 05:00 and they followed suit, albeit on a slightly converging course. Both sides opened fire around 05:23 at a range of {{convert|8500|m|yd|sp=us}}. The Japanese ships concentrated their fire on ''Rurik'', the rear ship of the Russian formation. She was hit fairly quickly and began to fall astern of the other two ships. Jessen turned southeast in an attempt to open the range, but this blinded the Russian gunners with the rising sun and prevented any of their broadside guns from bearing on the Japanese. About 06:00, Jessen turned 180° to starboard in an attempt to reach the Korean coast and to allow ''Rurik'' to rejoin the squadron. Kamimura followed suit around 06:10, but turned to port, which opened the range between the squadrons. ''Azuma'' then developed engine problems and the Japanese squadron slowed to conform with her best speed. Firing recommenced at 06:24 and ''Rurik'' was hit three times in the stern, flooding her steering compartment; she had to be steered with her engines. Her speed continued to decrease, further exposing her to Japanese fire, and her steering jammed to port around 06:40.<ref>Brook, pp. 39, 43</ref>

Jessen made another 180° turn in an attempt to interpose his two ships between the Japanese and ''Rurik'', but the latter ship suddenly turned to starboard and increased speed and passed between Jessen's ships and the Japanese. Kamimura turned 180° as well so that both squadrons were heading southeast on parallel courses, but Jessen quickly made another 180° turn so that they headed on opposing courses. The Russians reversed course for the third time around 07:45 in another attempt to support ''Rurik'' although ''Rossia'' was on fire herself; her fires were extinguished about twenty minutes later. Kamimura circled ''Rurik'' to the south at 08:00, then allowed the other two Russian ships to get to his north and gave them an uncontested route to Vladivostok. Despite this, Jessen turned back once more at 08:15 and ordered ''Rurik'' to make her own way back to Vladivostok before turning north at his maximum speed, about {{convert|18|kn}}.<ref name=b3>Brook, p. 43</ref>

About this time Kamimura's two elderly protected cruisers, {{ship|Japanese cruiser|Naniwa||2}} and {{ship|Japanese cruiser|Takachiho||2}}, were approaching from the south. Their arrival allowed Kamimura to pursue Jessen with all of his armored cruisers while the two new arrivals dealt with ''Rurik''. They fought a running battle with the Russians for the next hour and a half; scoring enough hits on them to force their speed down to {{convert|15|kn}}. ''Azuma''{{'}}s engines again broke down during this chase and she was replaced in the line by ''Tokiwa''. The Japanese closed to a minimum of about {{convert|5000|m|yd|sp=us}}, but Kamimura then opened the range up to {{convert|6500|m|yd|sp=us}}.<ref name=b3/>

About 10:00, Kamimura's gunnery officer erroneously informed him that ''Izumo'' had expended three-quarters of her ammunition, and he turned back after a five-minute rapid-fire barrage. He did not wish to leave the Tsushima Strait unguarded and thought that he could use his remaining ammunition on ''Rurik''. By this time she had been sunk by ''Naniwa'' and ''Takachiho'', which had closed to within {{convert|3000|m|yd|sp=us}} of ''Rurik'' in order to finish her off. They had radioed Kamimura that she was sunk, but he did not receive the message. Shortly after the Japanese turned back, ''Gromoboi'' and ''Rossia'' were forced to [[heave-to]] to make repairs. None of the Japanese ships were seriously damaged and ''Azuma'' only suffered eight men wounded during the battle.<ref>Brook, pp. 43, 45</ref>

On 30 December, ''Azuma'' and the armored cruiser {{ship|Japanese cruiser|Asama||2}} were ordered to patrol Tsugaru Strait to prevent any blockade runners from reaching Vladivostok. In mid-February, ''Azuma'' was relieved by ''Iwate'' so the former could be refitted.<ref>Corbett, II, pp. 153, 162</ref>

====Battle of Tsushima====
{{main|Battle of Tsushima}}
[[File:IJN Azuma 2.jpg|thumb|A postcard of ''Azuma'' at anchor, circa 1905]]
As the Russian [[Pacific Fleet (Russia)|2nd and 3rd Pacific Squadrons]] approached Japan on 27 May, having sailed from the [[Baltic Sea]], ''Yakumo'' was assigned to Kamimura's 2nd Division of the 2nd Fleet. The Russians were spotted by patrolling Japanese ships early that morning, but visibility was limited and radio reception poor. The preliminary reports were enough to cause Tōgō to order his ships to put to sea and the 2nd Division spotted the Russian ships under the command of Vice Admiral [[Zinovy Rozhestvensky]] at around 11:30. Kamimura closed to about a range of {{convert|8000|m|yd|sp=us}} before sheering off under fire to join Tōgō's battleships.<ref>Corbett, II, pp. 232, 235</ref> ''Azuma'' was second of six when Tōgō opened fire on the 2nd Pacific Squadron at 14:10 and, unlike most of the ships in the division, initially engaged the battleship {{ship|Russian battleship|Knyaz Suvorov||2}}. At 14:50, a {{convert|12|in|0|adj=on}} shell knocked out ''Azuma''{{'}}s aft right 8-inch gun. By 15:00, the Russian formation was in disorder and ''Knyaz Suvorov'' suddenly appeared out of the mist at 15:35 at a range of about {{convert|2000|m|sp=us}}. All of Kamimura's ships engaged her for five minutes or so, with ''Azuma'' and the armored cruiser {{ship|Japanese cruiser|Yakumo||2}} also firing torpedoes at the Russian ship without effect.<ref>Campbell, Part 2, pp. 128–32</ref>

After 17:30 Kamimura led his division in a fruitless pursuit of some of the Russian cruisers, leaving Tōgō's battleships to their own devices. He abandoned his chase around 18:03 and turned northwards to rejoin Tōgō. His ships spotted the rear of the Russian battleline around 18:30 and opened fire when the range closed to {{convert|8000|-|9000|m|yd|sp=us}}. Nothing is known of any effect on the Russians and they ceased fire by 19:30 and rejoined Tōgō at 20:08 as night was falling.<ref>Campbell, Part 3, pp. 186–87</ref> The surviving Russian ships were spotted the next morning and the Japanese ships opened fire around 10:30, staying beyond the range at which the Russian ships could effectively reply. Rear Admiral [[Nikolai Nebogatov]] therefore decided to surrender his ships as he could neither return fire nor close the range.<ref>Corbett, II, pp. 319–20</ref> Over the course of the entire battle, ''Azuma'' was struck by seven large shells, mostly 12 inch, four 6- and about four 75-millimeter shells. They inflicted only minor damage other than destroying one 6-inch and one 12-pounder gun mounts.<ref>Campbell, Part 4, p. 263</ref>

On 14 June, ''Azuma'' (along with ''Yakumo'', the armored cruisers {{ship|Japanese cruiser|Nisshin||2}} and {{ship|Japanese cruiser|Kasuga||2}}) was assigned of the [[3rd Fleet (Imperial Japanese Navy)|3rd Fleet]], under the command of Vice Admiral [[Kataoka Shichirō]], for the capture of [[Sakhalin]] in July.<ref>Corbett, II, p. 357</ref>

===Subsequent career===
[[File:Azuma AllanGreen1.jpg|thumb|''Azuma'' at anchor in Australia, 1910s]]
''Azuma'' was assigned to the Training Squadron on 20 April 1912, where she conducted long-distance oceanic navigation and officer training for cadets in the [[Imperial Japanese Navy Academy]]. From 5 December 1912 to 21 April 1913, the ship accompanied the ex-Russian protected cruiser {{ship|Japanese cruiser|Soya||2}} on a training cruise to Australia and Southeast Asia, the first of five overseas training cruises that she would make. ''Azuma'' was briefly relieved of her assignment from 1 May to 1 December before beginning another training cruise to North America and [[Hawaii]], together with ''Asama'', on 20 April–11 August 1914. The ship was again relieved of her assignment with the Training Squadron on 18 August, and was not reassigned until 1 September 1915 in preparation for her next training cruise that lasted from 20 April to 22 August 1916, and visited Australia and Southeast Asia again. The next month she was relieved of her assignment and then became the [[flagship]] of Destroyer Squadron (''Suiraisentai'') 2 from 1 December 1916 to 28 March 1917, and then again from 4 August to 24 January 1918.<ref>Lacroix & Wells, pp. 552, 656–59</ref> In early 1917, ''Azuma'' was dispatched on a diplomatic mission to return the body to the United States of [[George W. Guthrie]], the Ambassador to Japan, who had died while in office.<ref>Welles, p. 49</ref> The ship rejoined the Training Squadron on 10 August 1918, together with ''Tokiwa'', and made her last two training cruises over the next two years. From 1 March to 26 July 1919, the cruisers visited Australia and Southeast Asia and then the Mediterranean from 24 November to 20 May 1920. ''Azuma'' left the Training Squadron on 6 June.<ref>Lacroix & Wells, pp. 657, 659</ref>

===Final years===
On 1 September 1921, the ship was re-designated as a 1st-class [[Kaibokan|coast-defense ship]]. By this time her engines were in bad shape and she became a training ship for the [[Maizuru Naval District|Maizuru Naval Corps]] a week later.<ref name=lw9>Lacroix & Wells, p. 659</ref> In 1924, four of ''Azuma''{{'}}s 12-pounder guns were removed, as were all of her QF 2.5-pounder guns, and a single [[8 cm/40 3rd Year Type naval gun|8&nbsp;cm/40 3rd Year Type]] [[anti-aircraft gun]] was added. In addition three of her torpedo tubes were removed.<ref name=c4>Chesneau, p. 174</ref> On 1 October 1927, she became a stationary training ship for the Maizuru engineering school.<ref name=lw9/> ''Azuma'' was refitted again in 1930; this included replacement of her boilers that reduced her horsepower to {{convert|9400|ihp|abbr=on}} and her speed to {{convert|16|kn}}. Four each of her 6-inch and 12-pounder guns were removed during the refit.<ref name=c4/>

''Azuma'' was stricken from the [[navy list]], hulked, and disarmed in 1941. On 18 July 1945, she was badly damaged by aircraft from the [[United States Navy]] [[Fast Carrier Task Force|TF-38]] when they [[Attack on Yokosuka|attacked Yokosuka]]. The ship was scrapped in 1946.<ref name=j3/>

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{reflist|30em}}

==References==
*{{cite book|last=Brook|first=Peter|title=Warship 2000–2001|editor=Preston, Antony|publisher=Conway Maritime Press|location=London|year=2000|chapter=Armoured Cruiser vs. Armoured Cruiser: Ulsan 14 August 1904|isbn=0-85177-791-0}}
*{{cite book|last=Campbell|first=N.J.M.|title=Warship|chapter=The Battle of Tsu-Shima, Parts 2, 3 and 4|editor=Preston, Antony|publisher=Conway Maritime Press|location=London|year=1978|volume=II|pages=127–35, 186–192, 258–65|isbn=0-87021-976-6}}
*{{cite book|title=Conway's All the World's Fighting Ships 1922–1946|editor1-last=Chesneau|editor1-first=Roger|publisher=Conway Maritime Press|location=Greenwich, UK|year=1980|isbn=0-85177-146-7}}
* {{cite book|title=Conway's All the World's Fighting Ships 1860–1905|editor1-last=Chesneau|editor1-first=Roger|editor2-last=Kolesnik|editor2-first=Eugene M.|publisher=Conway Maritime Press|location=Greenwich|year=1979|isbn=0-8317-0302-4|lastauthoramp=y}}
*{{cite book|last=Corbett|first=Julian Stafford|authorlink=Julian Corbett|title=Maritime Operations in the Russo-Japanese War, 1904–1905|year=1994|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=1-55750-129-7}}
* {{cite book| last = Evans| first = David| first2 = Mark R. |last2=Peattie| year = 1997| title = Kaigun: Strategy, Tactics, and Technology in the Imperial Japanese Navy, 1887–1941| publisher =Naval Institute Press| location =Annapolis, Maryland| isbn = 0-87021-192-7|lastauthoramp=y|author2link=Mark Peattie}}
* {{cite book|title=Russian Battleship vs Japanese Battleship, Yellow Sea 1904–05|author=Forczyk, Robert|publisher=Osprey|year=2009|location=Botley, UK|isbn=978-1-84603-330-8}}
*{{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
*{{cite book| last = Jentschura| first = Hansgeorg| first2 = Dieter |last2=Jung|first3=Peter |last3=Mickel| year = 1977| title = Warships of the Imperial Japanese Navy, 1869–1945| publisher = United States Naval Institute| location = Annapolis, Maryland| isbn = 0-87021-893-X|lastauthoramp=y}}
*{{cite book|last=Kowner|first=Rotem|authorlink=Rotem Kowner|title=Historical Dictionary of the Russo-Japanese War|series=Historical Dictionaries of War, Revolution, and Civil Unrest|volume=29|year=2006|publisher=Scarecrow Press|location=Lanham, Maryland|isbn=978-0-81084-927-3}}
*{{cite book|last1=Lacroix|first1=Eric|last2=Wells|first2=Linton|title=Japanese Cruisers of the Pacific War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1997|isbn=0-87021-311-3|lastauthoramp=y}}
* {{cite book|editor=Jordan, John|chapter=Armored Cruisers of the Imperial Japanese Navy|last=Milanovich|first=Kathrin|publisher=Conway |location=London|year=2014|title=Warship 2014|isbn=978-1-84486-236-8}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
*{{cite book|last= Welles|first= Benjamin|title= Sumner Welles: FDR's Global Strategist : A Biography  |publisher= Palgrave Macmillan |year=1997|isbn=0-312-17440-3}}

==External links==
{{Commons category|Armored cruiser Azuma}}
* [http://www.scientificamericanpast.com/Scientific%20American%201900%20to%201909/4/lg/sci2131904.htm 1901 article in Scientific American]
*[http://homepage2.nifty.com/nishidah/e/stc0314.htm Materials of the IJN]

{{Russo-JapaneseWarJapaneseShips}}
{{WWIJapaneseShips}}

{{DEFAULTSORT:Azuma}}
[[Category:Cruisers of the Imperial Japanese Navy]]
[[Category:Ships built in France]]
[[Category:1899 ships]]
[[Category:Russo-Japanese War cruisers of Japan]]
[[Category:World War I cruisers of Japan]]
[[Category:Cruisers sunk by aircraft]]
[[Category:Ships sunk by US aircraft]]