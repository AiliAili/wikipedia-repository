{{For|the decommissioned ''Majestic''-class aircraft carrier|INS Vikrant (R11){{!}}INS ''Vikrant'' (R11)}}
{{EngvarB|date=August 2013}}
{{Use dmy dates|date=August 2013}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:INS Vikrant being undocked at the Cochin Shipyard Limited in 2015.jpg|border|300px]]
|Ship caption=''Vikrant'' being moved for fitting out in June 2015
}}
{{Infobox ship career
|Hide header=
|Ship country=India
|Ship flag={{shipboxflag|India|naval}}
|Ship name=INS ''Vikrant''
|Ship namesake={{INS|Vikrant|R11}}
|Ship owner= [[Ministry of Defence (India)|Ministry of Defence]]
|Ship operator= [[Indian Navy]]
|Ship ordered= 2004
|Ship awarded=
|Ship builder=[[Cochin Shipyard Limited]]
|Ship original cost= $0.5bn (planned), $3.765bn (to date)
|Ship yard number=
|Ship way number=
|Ship laid down= 28 February 2009
|Ship launched= 12 August 2013
|Ship sponsor=
|Ship christened=
|Ship completed=estimated 2023<ref name="TOI-2023">{{cite web|last1=Pandit|first1=Rajat|title=India without aircraft carrier for 8 months|url=http://timesofindia.indiatimes.com/india/India-without-aircraft-carrier-for-8-months/articleshow/53407213.cms|website=Times of India|accessdate=17 November 2016}}</ref>
|Ship acquired=
|Ship commissioned= December 2018(expected)<ref>{{cite web|url=http://newsworldindia.in/india/others/ins-vikrant-due-to-be-inducted-by-december-2018-says-navy-chief-sunil-lanba/212956/|title=INS Vikrant Due To Be Inducted By December 2018, Says Navy Chief Sunil Lanba|first=News World|last=India|date=|work=newsworldindia.in|accessdate=1 August 2016}}</ref><ref name="timesofindia.indiatimes.com">{{cite web|url=http://timesofindia.indiatimes.com/city/chennai/indias-first-indigenous-aircraft-carrier-to-be-inducted-in-2018/articleshow/56023951.cms|title=India’s first indigenous aircraft carrier to be inducted in 2018|work=Times of India|accessdate=22 December 2016}}</ref>
|Ship recommissioned=
|Ship decommissioned=
|Ship maiden voyage=
|Ship in service=
|Ship out of service=
|Ship renamed=
|Ship reclassified=
|Ship refit=
|Ship struck=
|Ship reinstated=
|Ship homeport=
|Ship identification=
|Ship motto=  "I defeat those who fight against me". [[Sanskrit]] जयेम सं युधि स्पृध:
|Ship nickname=
|Ship captured=
|Ship fate=
|Ship status=Fitting out
|Ship notes=
|Ship badge=
|Ship honours=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Vikrant|aircraft carrier}}
|Ship tonnage=
|Ship displacement={{convert|40000|t|ST}}
|Ship length={{convert|262|m|ft|abbr=on}}
|Ship beam={{convert|60|m|ft|abbr=on}}
|Ship height=
|Ship draught={{convert|8.4|m|ft|abbr=on}}
|Ship depth={{convert|25.6|m|ft|abbr=on}}
|Ship hold depth=
|Ship decks=
|Ship deck clearance=
|Ship ramps=
|Ship power= 4{{nbsp}}&times;{{nbsp}}[[General Electric LM2500+]] [[gas turbine]]s
|Ship propulsion=Two shafts
|Ship speed={{convert|28|kn}}
|Ship range={{convert|8000|nmi}}<ref name=DN449/>
|Ship endurance=
|Ship boats=
|Ship capacity=
|Ship troops=
|Ship complement=
|Ship crew=1,400 (including air crew)
|Ship time to activate=
|Ship sensors=*[[Selex RAN-40L]] long-range early warning and surveillance [[Active electronically scanned array|AESA]] radar.
*Elta [[EL/M-2248 MF-STAR]] [[Active electronically scanned array|AESA]] multifunction radar. 
|Ship EW=
|Ship armament=* 4 × [[OTO Melara 76 mm|Otobreda {{convert|76|mm|in|0|adj=on|abbr=on}}]] dual purpose cannons
* [[Barak 1]] & [[Barak 8]] surface-to-air missile launchers (2 x 32 cells [[Vertical launching system|VLS]])
* [[AK-630]] [[Close-in weapon system|CIWS]]<ref>{{cite web|title=List of Aircraft Carriers Under Construction: 2013 |url=http://world-defece-review.blogspot.com/2013/02/list-of-aircraft-carriers-under.html#ixzz2nvBh8WNd |publisher=World Defense Review |accessdate=17 May 2015}}</ref>

|Ship armour=
|Ship aircraft=
* 30 Total Aircraft <ref name="timesofindia.indiatimes.com"/>
*fixed-wing aircraft including<ref name="Business Standard">{{cite news |title=Cochin Shipyard undockes INS Vikrant |url=http://wap.business-standard.com/article/current-affairs/cochin-shipyard-undocks-ins-vikrant-115061101387_1.html |publisher=BUSINESS STANDARD |date=11 June 2015 |accessdate=12 March 2016}}</ref> 
** 26 x Fixed Wing Aircraft including [[Mikoyan MiG-29K]] 
* rotatory-wing including<ref name="Business Standard"/>
** 10 x [[Kamov Ka-31]], [[Westland Sea King]] and [[HAL Dhruv]].<ref>{{cite web|title=India’s First Indigenous Aircraft Carrier - INS Vikrant|url=http://pib.nic.in/newsite/efeatures.aspx?relid=98885}}</ref>
|Ship aircraft facilities={{convert|10000|m2|sqft|abbr=on}} flight deck
|Ship notes=
}}
|}

'''INS ''Vikrant''''' ({{lang-sa|विक्रान्त}} ''víkrānta'' "courageous"<ref name="unveils">{{cite news|title=India unveils home-built aircraft carrier, INS Vikrant |url=http://edition.cnn.com/2013/08/12/world/asia/india-aircraft-carrier/index.html|first=Harmeet|last=Singh|date=12 August 2013|work=[[CNN]] |accessdate=18 May 2015}}</ref>) (IAC-I) is the first [[aircraft carrier]] built in India and the first {{sclass-|Vikrant|aircraft carrier|0}} aircraft carrier built by [[Cochin Shipyard| Cochin Shipyard (CSL)]] in [[Kochi]], [[Kerala]] for the [[Indian Navy]]. The motto of the ship is ''Jayema Sam Yudhi Sprdhah'' ({{lang-sa|जयेम सं युधि स्पृध:}}), which is taken from [[Rig Veda]] 1.8.3 and is translated as "I defeat those who fight against me".

Work on the ship's design began in 1999, and the [[Keel laying|keel]] was laid in February 2009. The carrier was floated out of its [[dry dock]] on 29 December 2011<ref name=hindu-float/> and was launched in 2013. The ship is currently being fitted out, according to the Comptroller & Auditor General (CAG) it is expected to be completed by 2023,<ref name="TOI-2023" /> though the Navy still hopes to partially commission the ship in late 2018. The project cost has escalated dramatically to {{INRConvert|19341|c}} as of 2014.<ref name=janesfloated>{{cite journal|last1=Mazumdar|first1=Mrityunjoy|title=India's indigenous carrier is floated out|journal=IHS Jane's Defence Weekly|date=17 June 2015|volume=52|issue=24|page=8|accessdate=30 June 2015}}</ref>

[[Cochin Shipyard]] has offered repeatedly to build a second similar Project 71 Indigenous Aircraft Carrier while the Navy finalizes plans for the configuration of its [[INS Vishal|larger and likely nuclear-powered next carrier]].<ref name=janesfloated />

==Design==
[[File:ഐ.എൻ.എസ്. വിക്രാന്ത് കൊച്ചി കപ്പൽ ശാലയിൽ ഓഗസ്റ്റ് 2013.jpg|300px|thumbnail|INS ''Vikrant'' during its launch in August 2013]]
[[File:INS Vikrant being undocked at the Cochin Shipyard Limited in 2015 (08).jpg|300px|thumbnail|INS ''Vikrant'' during its undocking in June 2015]]
INS ''Vikrant'' is the first ship of the ''Vikrant'' class of aircraft carriers. The name ''Vikrant'' ([[Sanskrit]] ''vikrānta'', literally "stepping beyond") means "courageous" or "bold". It is {{convert|262|m|ft}} long and {{convert|60|m|ft}} wide, and displaces about {{Convert|40,000|MT|LT|lk=out}}. It features a [[STOBAR|Short Take-Off But Arrested Recovery]] (''STOBAR'')<ref>{{cite news |url=http://www.theworldreporter.com/2013/08/aicraft-carrier-liaoning-vs-indian-ins-vikrant.html |title=Comparison of Chinese Aircraft Carrier Liaoning and Indian INS Vikrant |work=The World Reporter |last=Shrivastava |first=Sanskar |date=25 August 2013 |accessdate=18 May 2015}}</ref> configuration with a [[Ski-jump ramp|ski-jump]]. The deck is designed to enable aircraft such as the MiG-29K to operate from the carrier. It is expected to carry an air group of up to thirty aircraft, which will include up to 24–26 fixed-wing combat aircraft,<ref name="sp'snavalforces">{{cite journal |title=Force Projection and Modernization of Indian Navy |url=http://www.spsnavalforces.com/ebook.asp?Id=140203043725-c916f52beb13022808869dac8e2950ae&Name=sp_s_naval_forces_01_-_2014&Info=SP%27s%20Naval%20Forces%20February%20-%20March%202014&t=1359441862394&r=85&mob=10055951&year=2014 |date=March 2014 |publisher=SP's Naval Forces |last=Ramsay |first=Sushil |volume=9 |number=1 |pages=4–6 |accessdate=18 May 2015}}</ref> primarily the [[Mikoyan MiG-29K]] . The naval variant of the [[HAL Tejas]] was rejected by the navy on Dec 2, 2016 for being overweight.<ref>{{cite web|url=http://www.janes.com/article/65993/indian-navy-rejects-naval-version-of-tejas-lca-seeks-alternative|title=Indian Navy rejects naval version of Tejas LCA, seeks alternative|work=IHS Jane's 360|accessdate=22 December 2016}}</ref> Besides carrying 10 [[Kamov Ka-31]] or [[Westland Sea King]] helicopters. The Ka-31 will fulfill the [[airborne early warning]] (AEW) role and the Sea King will provide [[anti-submarine warfare]] (ASW) capability.<ref name=hindu-keel/><ref name=indrus5dec12>{{cite news|first=Rakesh Krishnan |last=Simha |title=Vikramaditya and Liaoning – forces of the future |url=http://indrus.in/articles/2012/12/05/vikramaditya_and_liaoning_-_forces_of_the_future_19567.html |accessdate=18 May 2015 |newspaper=IndRus |date=5 December 2012}}</ref>

[[File:INS Vikrant CGI.png|300px|thumbnail|A schematic diagram of INS ''Vikrant'']]
''Vikrant'' is powered by four [[General Electric LM2500+]] gas turbines on two shafts, generating over {{convert|80|MW|hp}} of power. The gearboxes for the carriers were designed and supplied by [[Elecon Engineering]].<ref name=hindu-float>{{cite news |url=http://www.thehindu.com/news/national/article2758985.ece |title=Navy floats out first Indigenous Aircraft Carrier |work=The Hindu |last=Anandan |first=S. |last2=Martin |first2=K. A. |date=30 December 2011 |accessdate=18 May 2015}}</ref><ref name=bs-gears>{{cite news |url=http://www.business-standard.com/india/news/elecon-to-supply-gears-for-indias-first-aircraft-carrier/459654/ |title=Elecon to supply gears for India's first aircraft carrier |work=Business Standard |last=Vora |first=Rutan |date=26 December 2011 |accessdate=18 May 2015}}</ref><ref name=ect5a>{{cite news |title=India starts work on second indigenous aircraft carrier |url=http://economictimes.indiatimes.com/news/politics/nation/India-starts-work-on-second-indigenous-aircraft-carrier/articleshow/15004643.cms|work=The Economic Times |date=16 July 2012 |accessdate=18 May 2015}}</ref>

==Construction==
''Vikrant'' is the first aircraft carrier to be designed by the Directorate of Naval Design of the Indian Navy and the first warship to be built by [[Cochin Shipyard]]. Its construction involved participation of a large number of private and public firms. The [[keel]] for ''Vikrant'' was laid by [[Ministry of Defence (India)|Defence Minister]] [[A. K. Antony|A.K. Antony]] at the Cochin Shipyard on 28 February 2009.<ref>{{cite web |url=http://www.dnaindia.com/india/report-govt-overhauls-coastal-security-gives-overall-charge-to-navy-1235004 |title=Govt overhauls coastal security; gives overall charge to Navy |publisher=dnaindia |access-date=7 April 2016 }}</ref><ref>{{cite news|url=http://en.rian.ru/world/20090226/120325539.html |title=India to lay keel of new aircraft carrier on Saturday &#124; World &#124; RIA Novosti |work=sputniknews |date=26 February 2009 |accessdate=18 May 2015}}</ref>

The [[ABS Steels|AB/A grade steel]] which was supposed to be supplied from Russia faced problems in delivery. To resolve this, the [[Defence Metallurgical Research Laboratory]] (DMRL) and [[Steel Authority of India Limited]] (SAIL) created facilities to manufacture the steel in India.<ref name=hindu-float/><ref name=hindu-keel>{{cite web |url=http://www.thehindu.com/todays-paper/tp-national/keellaying-of-indigenous-aircraft-carrier-in-december/article1348007.ece |title=Keel-laying of indigenous aircraft carrier in December |last=Anandan |first=S. |work=The Hindu |date=29 September 2008 |accessdate=18 May 2015}}</ref> Reportedly, three types of special steel for the hull, flight deck and floor compartments were manufactured at the [[Bhilai Steel Plant]], [[Chhattisgarh]]  and [[Rourkela Steel Plant]], [[Odisha]]. Due to this, this is the first ship of the Indian navy to be built completely using domestically-produced steel.<ref>{{cite news|title=INS Vikrant's first victory: being built from Indian steel|url=http://www.business-standard.com/article/current-affairs/ins-vikrant-s-first-victory-being-built-from-indian-steel-113080701287_1.html |last=Shukla |first=Ajai |accessdate=18 May 2015 |newspaper=Business Standard|date=7 August 2013}}</ref> The main switch board, steering gear and water tight hatches have been manufactured by [[Larsen & Toubro]] in [[Mumbai]] and Talegaon; high-capacity air conditioning and refrigeration systems have been manufactured in [[Kirloskar Group]]’s plants in [[Pune]]; most pumps have been supplied by Best and Crompton; [[Bharat Heavy Electricals]] (BHEL) supplied the Integrated Platform Management System (IPMS), which is being installed by [[Avio]], an [[Italy|Italian]] company; the gear box was supplied by [[Elecon Engineering]]; and the electrical cables are being supplied by Nicco Industries.<ref name=idr12aug13>{{cite news |title=‘Vikrant’ Reborn in Indigenous Avtar |url=http://www.indiandefencereview.com/news/vikrant-reborn-in-indigenous-avtar/ |accessdate=18 May 2015 |newspaper=Indian Defence Review |date=12 August 2013}}</ref>

The ship uses modular construction, with 874 blocks joined together for the hull. By the time the keel was laid, 423 blocks weighing over 8,000 tons had been completed.<ref name=Herald_Elite_club>{{cite news|title=India joins elite warships club |url=http://archive.deccanherald.com/Content/Mar12009/scroll20090301121399.asp|accessdate=18 May 2015 |newspaper=[[Deccan Herald]] |archiveurl= http://www.webcitation.org/query?url=http%3A%2F%2Farchive.deccanherald.com%2FContent%2FMar12009%2Fscroll20090301121399.asp&date=2012-07-15 |archivedate= 15 July 2012 |date=1 March 2009 |last=Ray |first=Kaylan}}</ref> The construction plan called for the carrier to be launched in 2010, when it would displace some 20,000 tonnes, as a larger displacement could not be accommodated in the building bay. It was planned that after about a year's development in the refit dock, the carrier would be launched when all the major components, including underwater systems, would be in place. Outfitting would then be carried out after launch. As per the [[Cabinet Committee on Security]] (CCS), sea trials were initially planned to commence in 2013, with the ship to be commissioned in 2014.<ref>{{cite news|first=Sandeep |last=Unnithan |url=http://indiatoday.intoday.in/index.php?option=com_content&task=view&id=29613&sectionid=4&issueid=93&Itemid=1 |title=Keel laying of indigenous aircraft carrier next week |work=Indiatoday |date=18 February 2009 |accessdate=18 May 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20090727031555/http://indiatoday.intoday.in/index.php?option=com_content&task=view&id=29613&sectionid=4&issueid=93&Itemid=1 |archivedate=27 July 2009 |df=dmy-all }}</ref><ref>{{cite web|url=http://www.dnaindia.com/india/report_indigenous-aircraft-carrier-s-nucleus-ready_1448720 |title=Indigenous Aircraft Carrier's nucleus ready |last=Sharma |first=Suman |work=Dnaindia.com |date=7 October 2010 |accessdate=18 May 2015}}</ref>

In March 2012, it was reported that the project had been affected by the delay in the delivery of the main gearboxes for the carrier. The supplier, Elecon, attributed it to having to work around a number of technical complexities due to the length of the propulsion shafts.<ref name=hindu-schedule>{{cite news|url=http://www.thehindu.com/todays-paper/tp-national/tp-kerala/article1530996.ece |title=Indigenous aircraft carrier a year behind schedule |last=Anandan |first=S. |work=The Hindu |date=12 March 2011 |accessdate=18 May 2015}}</ref> Other issues resulting in delays included an accident with a diesel generator and an issue with its alignment.<ref name=hindu-arihant>{{cite news|url=http://www.thehindu.com/news/national/article2681534.ece|title=INS Arihant on track |work=The Hindu |last=Prasad |first=K.V. |date=3 December 2011 |accessdate=18 May 2015}}</ref> In August 2011, the Defence Ministry reported to the [[Lok Sabha]] that 75% of the construction work for the hull of the lead carrier had been completed and the carrier would be first launched in December 2011, following which further works would be completed until commissioning.<ref>{{cite news|url=http://zeenews.india.com/news/nation/indigenous-aircraft-carrier-launch-this-dec_723865.html |title=‘Indigenous aircraft carrier launch this Dec’ |work=Zeenews |date=2 August 2011 |accessdate=18 May 2015}}</ref><ref>{{cite web|url=http://ibnlive.in.com/generalnewsfeed/news/first-indigenous-aircraft-carrier-to-be-completed-by-dec-govt/773900.html |title=First indigenous aircraft carrier to be completed by Dec: Govt |publisher=Ibnlive.in.com |date=2 August 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121107013318/http://ibnlive.in.com/generalnewsfeed/news/first-indigenous-aircraft-carrier-to-be-completed-by-dec-govt/773900.html |archivedate=7 November 2012 }}</ref> On 29 December 2011, the completed hull of the carrier was first floated out of its dry dock at CSL, with its displacement at over 14,000 tonnes.<ref name=bs-gears/> Interior works and fittings on the hull would be carried out until the second half of 2012, when it would again be dry-docked for integration with its propulsion and power generation systems.<ref name=DN449>{{cite web|url=http://www.defencenow.com/news/449/india-floats-out-its-first-indigenous-aircraft-carrier-ins-vikrant-from-cochin.html |title=India Floats out Its First Indigenous Aircraft Carrier |publisher=DefenceNow.com |date=2 January 2012 |accessdate=18 May 2015}}</ref><ref name=hindu-float/>

In July 2012, ''[[The Times of India]]'' reported that construction of ''Vikrant'' has been delayed by three years, and the ship would be ready for commissioning by 2018.<ref name="auto">{{Cite news|url=http://timesofindia.indiatimes.com/city/chennai/indias-first-indigenous-aircraft-carrier-to-be-inducted-in-2018/articleshow/56023951.cms|title=India’s first indigenous aircraft carrier to be inducted in 2018 - Times of India|newspaper=The Times of India|access-date=2016-12-17}}</ref> Later, in November 2012, Indian English-language news channel ''[[NDTV]]'' reported that cost of the aircraft carrier had increased and the delivery has been delayed by at least five years and is expected to be with the Indian Navy only after 2018 as against the scheduled date of delivery of 2014.<ref name="Ndtv">{{cite news|url=http://www.ndtv.com/article/india/indian-aircraft-carrier-more-costly-already-delayed-294997 |title=Indian aircraft carrier: More costly, already delayed |work=Ndtv.com |date=20 November 2012 |last=Sen |first=Sudhi Ranjan |accessdate=18 May 2015}}</ref> Work then commenced for the next stage of construction, which included the installation of the integrated propulsion system, the superstructure, the upper decks, the cabling, sensors and weapons.<ref>{{cite news|first=Rajat |last=Pandit |date=16 August 2014 |url=http://timesofindia.indiatimes.com/india/PM-Modi-inducts-Indias-largest-indigenously-built-warship-INS-Kolkata/articleshow/40313665.cms |title=PM Modi inducts India's largest indigenously built warship INS Kolkata |work=The Times of India'' |accessdate=18 May 2015}}</ref>

===Launch===
In July 2013, the Defence Minister [[A. K. Antony]] announced that ''Vikrant'' would be launched on 12 August at the Cochin Shipyard. The ship was launched by his wife, Elizabeth Antony, on 12 August 2013.<ref name=NationalPost2013-08-12>{{cite news| url= http://news.nationalpost.com/2013/08/12/india-launches-first-aircraft-carrier-and-powers-up-nuclear-sub-a-shot-across-the-bow-to-china-for-pacific-dominance/ | title = India launches home-built, 37,500-tonne aircraft carrier in a shot across the bow to China | work= [[National Post]]| date        = 12 August 2013 |accessdate=18 May 2015 |agency=Associated Press | archivedate = 14 August 2013 | archiveurl  = http://www.webcitation.org/query?url=http%3A%2F%2Fnews.nationalpost.com%2F2013%2F08%2F12%2Findia-launches-first-aircraft-carrier-and-powers-up-nuclear-sub-a-shot-across-the-bow-to-china-for-pacific-dominance%2F&date=2013-08-14 | deadurl= No | quote= The 37,500 tonne INS Vikrant is expected to go for extensive trials in 2016 before being inducted into the navy by 2017, reports say.  With this, India joins the select group of countries comprising the United States, the United Kingdom, Russia and France capable of building such a vessel.}}</ref>
Extensive sea trials are expected to begin in mid of 2017 and the ship will be inducted into the navy by late 2018.<ref name="auto"/>

According to Admiral Robin Dhowan, about 83% of the fabrication work and 75% of the construction work had been completed at the time of launching. He said that 90% of the body work of the aircraft carrier had been designed and made in India, about 50% of the propulsion system, and about 30% of its weaponry. He also said that the ship would be equipped with a long range missile system with multi-function radar and a [[close-in weapon system]] (CIWS).<ref name="NDTVLaunch">{{cite news|url=http://www.ndtv.com/article/india/ins-vikrant-first-indian-made-aircraft-carrier-enters-water-next-week-402556|title=INS Vikrant, first Indian-made aircraft carrier, enters water next week|last=Sen|first=Sudhi Ranjan|date=11 August 2013|work=NDTV|accessdate=18 May 2015}}</ref> After the launch, ''Vikrant'' was re-docked for the second phase of construction, in which the ship will be fitted with various weapons and sensors, and the propulsion system, flight deck and the aircraft complex will be integrated.<ref name=idr12aug13/> In December 2014 it was reported that ''Vikrant'' will be commissioned by 2018.

===Undocking and fitting-out phase===
''Vikrant'' was undocked on 10 June 2015 after the completion of structural work. Cabling, piping, heat and ventilation works will be completed by 2017; sea trials will begin thereafter.<ref>{{cite web | url= http://www.thehindu.com/news/national/kerala/ins-vikrant-undocked/article7301993.ece |title= Cochin Shipyard undocks INS Vikrant |last=Anandan |first=S. |work=The Hindu |date=10 June 2015|accessdate=12 June 2015}}</ref> By October 2015, the construction of the hull was close to 98 percent complete, with flight deck construction underway.<ref>{{cite web | url=http://timesofindia.indiatimes.com/india/Aircraft-carrier-INS-Vikrant-will-be-delivered-to-Navy-on-time-Cochin-Shipyard-chief/articleshow/49392647.cms |title=Aircraft carrier INS Vikrant will be delivered to Navy on time: Cochin Shipyard chief  |last=Gupta |first=Jayanta |work=Times of India |date=15 October 2015|accessdate=18 October 2015}}</ref> The installation of machinery, piping and the propeller shafts was in progress by January 2016; it was reported, however, that there were delays in the delivery of equipment from Russia for the carrier's aviation complex.<ref>{{cite web | url=http://www.thehindu.com/news/cities/Kochi/navy-chief-reviews-vikrant-project/article8115862.ece |title=Navy chief reviews Vikrant project  |last= |first= |work=The Hindu |date=17 January 2016|accessdate=30 March 2016}}</ref>

== See also ==
*[[Future of the Indian Navy|Future ships of the Indian Navy]]
*[[List of active Indian Navy ships]]

==References==
{{reflist|30em}}

==External links==
{{commons category|INS Vikrant (ship, 2013)}}
*[http://www.indianexpress.com/picture-gallery/the-ins-vikrant-know-all-about-the-carrier/3204-1.html Images during the launch of Vikrant]

{{Indian Navy aircraft carriers}}

{{DEFAULTSORT:Vikrant}}
[[Category:Aircraft carriers of the Indian Navy]]
[[Category:Vikrant-class aircraft carriers]]
[[Category:2013 ships]]