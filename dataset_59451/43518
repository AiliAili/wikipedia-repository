<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=S.E.4a
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Scout aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=[[Royal Aircraft Factory]]
 | designer=
 | first flight=25 June 1915
 | introduced=
 | retired=1917
 | status=
 | primary user=[[Royal Flying Corps]]
 | number built=4
 | developed from=[[Royal Aircraft Factory S.E.4]]
 | variants with their own articles=
}}
|}
The '''Royal Aircraft Factory S.E.4a''' was an experimental [[United Kingdom|British]] single-engined scout aircraft of the [[First World War]]. Four S.E.4a aircraft were built, being used for research purposes and as home-defence fighters by the [[Royal Flying Corps]]. In spite of its type number it had little or no relationship to the earlier [[Royal Aircraft Factory S.E.4|S.E.4]]

==Development and design==
In 1915, [[Henry Folland]] of the [[Royal Aircraft Factory]] designed a new single-engined scout aircraft, the '''S.E.4a'''.  While it had a similar designation to Folland's earlier [[Royal Aircraft Factory S.E.4]] of 1914, which had been designed to be the fastest aircraft in the world, the S.E.4a was fundamentally a new aircraft, intended to investigate the relationship between stability and manoeuvrability, and for possible operational use.<ref name="Bruce v2 p72-3">Bruce 1968, pp. 72—73.</ref>

The resulting design was a single-engined, [[interplane strut|single bay]] [[biplane]]. The fuselage structure was of mixed construction, with a steel tube forward section and a wooden box-girder rear section.<ref name="Bruce v2 p73">Bruce 1968, p.73.</ref> The first prototype's fuselage was smoothly faired out to a circular section using formers and stringers, with the forward fuselage back to the cockpit covered in metal skinning and the rear fuselage fabric covered.<ref name="Lewis Fighter p54-5">Lewis 1979, pp. 54—55.</ref>  The wood and fabric single-bay wings, unlike the S.E.4, had noticeable [[Stagger (aviation)|stagger]] between the upper and lower wings, but were fitted with similar, full span control surfaces which could be moved differentially as [[aileron]]s or together as [[Camber (aerodynamics)|camber]] changing [[Flap (aircraft)|flaps]], to those used on the S.E.4.<ref name="Lewis Fighter p54-5"/><ref name="Bruce RFC p469">Bruce 1982, p.469.</ref>

The first prototype's engine, an 80&nbsp;hp (60&nbsp;kW) Gnome [[rotary engine]], was mounted within a smooth cowling driving a two-bladed propeller fitted with a large, blunt spinner.  This was found to lead to engine overheating and was replaced by a more conventional arrangement.<ref name="Mason Fighter p32">Mason 1992, p.32.</ref>

The remaining three prototypes had simpler structures, with flat-sided fuselages, and many of the drag reducing features of the first prototype omitted. They were powered by a range of engines of similar power to that used in the first prototype, including [[Clerget]] and [[Le Rhône]] rotaries.<ref name="Bruce RFC p469"/>

==Operational history==
The first prototype flew on 25 June 1915,<ref name="Bruce RFC p469"/> with the remaining three aircraft all having flown by mid August.<ref name="Bruce v2 p74">Bruce 1968, p.74.</ref>  The S.E.4a proved easy to fly, demonstrating excellent aerobatic capabilities,<ref name="Bruce British p444">Bruce 1957, p.444.</ref> but were overweight and underpowered, and was not developed further.<ref name="Bruce RFC p470">Bruce 1982, p.470.</ref>

Two of the aircraft, armed with a [[Lewis gun]] mounted above the upper wing were issued to Home Defence squadrons of the [[Royal Flying Corps]] in the winter of 1915-16, based at [[Hounslow Heath Aerodrome]] and [[RAF Joyce Green|Joyce Green]]. One of these was lost in a fatal crash on 24 September 1915.  The third prototype remained in use for trials purposes until September 1917.<ref name="Bruce RFC p470"/><ref name="Bruce v2 p75">Bruce 1968, p.75.</ref>

==Specifications==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=The British Fighter since 1912 <ref name="Mason Fighter p32">Mason 1992, p.32.</ref><!-- the source(s) for the information -->
|crew=1
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 20 ft 10 in
|length alt=6.35 m
|span main=27 ft 5 in
|span alt=8.36 m
|height main=9ft 5 in
|height alt=2.87 m
|area main= 
|area alt= 
|airfoil=
|empty weight main= 
|empty weight alt= 
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=
|engine (prop)=[[Le Rhône]]
|type of prop=[[rotary engine]] <!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 80 hp
|power alt=60 kW
|power original=
|power more=
|propeller or rotor?=<!-- options: propeller/rotor -->
|propellers=
|number of propellers per engine= 
|propeller diameter main=
|propeller diameter alt= 
|max speed main= 90 mph
|max speed alt=67 knots, 145 km/h
|max speed more= at sea level
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|guns= Provision for 1× [[.303 British|.303 in]] [[Lewis gun]] above upper wing
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=*[[Royal Aircraft Factory S.E.4]]<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Royal Aircraft Factory}}
{{reflist}}
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957.
*Bruce J.M. ''War Planes of the First World War: Volume Two Fighters''. London:Macdonald, 1968. ISBN 0-356-01473-8.
*Bruce, J.M. ''The Aeroplanes of the Royal Flying Corps (Military Wing)''. London:Putnam, 1982. ISBN 0-370-30084-X.
*Lewis, Peter. ''The British Fighter since 1912''. London:Putnam, Fourth edition, 1979. ISBN 0-370-10049-2.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, USA:Naval Institute Press, 1992. ISBN 1-55750-082-7.
{{refend}}
<!-- ==External links== -->

{{Royal Aircraft Factory aircraft}}
{{wwi-air}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Royal Aircraft Factory aircraft|SE04a]]