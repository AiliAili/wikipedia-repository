{{good article}}
{{Use American English|date=December 2016}}
{{Use dmy dates|date=May 2011}}
{{Infobox military unit
|unit_name= 1st Sustainment Brigade
|image=United States Army 1st Infantry Division CSIB.svg
|caption=1st Infantry Division shoulder sleeve insignia
|dates=15 February 2007 – present
|country={{flag|United States}}
|allegiance=
|branch=[[United States Army]]
|type=[[Sustainment Brigade]]
|role=
|size=[[Brigade]]
|command_structure=[[1st Infantry Division (United States)|1st Infantry Division]]
|garrison=[[Fort Riley]], [[Kansas]]
|garrison_label=
|nickname=
|patron=
|motto=''Sustain to Victory''
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|commander1=[[Colonel (United States)|COL]] Allen T. Cassell
<ref>[http://www.1divpost.com/newsdetail.asp?article_id=5796 1st Sustainment Brigade welcomes new commander], Miller, Mollie. Retrieved 18 November 2011.</ref>
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:1st Sust Bde DUI.png|150px]]
|identification_symbol_label=Distinctive Unit Insignia
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''1st Sustainment Brigade''' is a [[Sustainment Brigade|sustainment]] [[brigade]] of the [[United States Army]] based at [[Fort Riley]], [[Kansas]]. It provides logistics support to the [[1st Infantry Division (United States)|1st Infantry Division]].

Activated in 2007, the unit is a modular brigade capable of a variety of actions. Though assigned to the 1st Infantry Division on a permanent basis, it is capable of independent operations and taking on subordinate units to fulfill large scale sustainment operations for the United States Army.

Formed from the [[Division Support Command]] of the 1st Infantry Division, the Brigade carries the lineage and honors of the division dating back to World War I campaigns as early as 1917. Having also seen action in World War II, the [[Vietnam War]], and the [[Gulf War]], the brigade has numerous awards and decorations from its previous designation. The brigade has also seen three tours in [[Iraq]] in support of [[Operation Iraqi Freedom]].

==Organization==
The 1st Sustainment Brigade (1SB) in garrison at Fort Riley is composed of four subordinate battalions. The 1st Special Troops Battalion (STB) contains a Headquarters and Headquarters Company (HHC), 267th Signal Company, and the 24th Transportation Company.<ref>[http://www.riley.army.mil/UnitPage.aspx?unit=1SB_STB Special Troops Bn., 1st Sustainment Brigade] Retrieved 15 October 2010</ref> The 1st STB and the 541st Combat Sustainment Support Battalion are permanently assigned to the 1SB. The 1SB has an Administrative Control Minus (ADCON-) Training and Readiness (TRA) relationship with the 84th Explosive Ordnance Demolition (EOD) Battalion and the 1st Infantry Division Headquarters and Headquarters Battalion (1DHHB).<ref>[http://www.1id.army.mil/units/default.aspx 1st Infantry Division: Units], 1st Infantry Division staff. Retrieved 30 March 2008</ref> Thanks to the brigade's modular design, it is also capable of gaining additional subordinate units upon deployment to a theater of operations.<ref name= "BdeHist">[http://www.riley.army.mil/Units/1stSustainBde/ 1st Infantry Division Homepage: 1st Sustainment Brigade], United States Army. Retrieved 29 March 2008</ref>

==History==
===Origins===
The 1st Infantry Division Support Command ([[DISCOM]]) traces its origins to World War I, where in 1917, the Division Trains were formed to support the newly formed 1st Infantry Division. In 1921, the Division trains were consolidated into the Special Troops, 1st Infantry Division. After World War I, the Special Troops deployed to Fort Riley, Kansas . Three of the DISCOM's former subordinate battalions, the 101st Forward Support Battalion (FSB) and 201st Forward Support Battalions, and the 701st Main Support Battalion, served in World War I, but with different divisions.<ref name= "BdeHist" />

These units deployed back to Germany to support the 1st Infantry Division during World War II, and participated in all eight campaigns credited to the 1st Infantry Division. In 1955, the Division and its support organizations returned to Fort Riley, Kansas.<ref name="DivHist">[http://www.riley.army.mil/view/article.aspx?articleId=0afc2a15f91b4b92979baad790a6468b 1st Infantry Division: 1st Sustainment Brigade], United States Army. Retrieved 28 March 2008</ref>

In 1965, the division deployed to [[South Vietnam]], as a part of the [[Vietnam War]] buildup. DISCOM units supported the Division in all of the eleven campaigns it participated in while deployed to South Vietnam .<ref name="DivHist"/>

After Vietnam, the DISCOM underwent many changes. The Division Material Management Center (DMMC) was established, and the Finance and Personnel Services Companies (PSC) were reorganized into battalion commands. In 1990, the DISCOM deployed again, this time to Southwest Asia in support of [[Operation Desert Storm]].<ref name="DivHist"/>

In 1996, the DISCOM, deployed to Europe for a third time and consisted of the 101st FSB at Fort Riley, Kansas, the 201st FSB in [[Vilseck, Germany]], the 701st MSB in [[Kitzingen, Germany]], the 601st Aviation Support Battalion(ASB) in Katterbach, Germany, and the Headquarters and Headquarters Company (HHC), also in Kitzingen.<ref name="DivHist"/>

===Operation Iraqi Freedom===

In 2003, the DISCOM was deployed to [[Turkey]] in support of [[Operation Iraqi Freedom I]]. The DISCOM simultaneously supported peacekeeping operations in the [[Balkans]] and deployed a logistics task force to support Operation Iraqi Freedom I throughout [[Iraq]]. In 2004, the DISCOM redeployed to Southwest Asia in Support of [[Operation Iraqi Freedom II]].<ref name="1STimes">[http://www.1stid.org/about/pressreleases/1sb0907.pdf The 1st Sustainment Times], 1st Sustainment Brigade Public Affairs. Retrieved 25 June 2008.</ref> In addition to the organic DISCOM units, the [[225th Brigade Support Battalion|225th Forward Support Battalion]] from Hawaii and the 230th Support Battalion from [[North Carolina]] deployed to support logistical operations for Task Force Danger. Finally, in 2005, the DISCOM redeployed to Germany to reconstitute and prepare for future contingency operations. The DISCOM was awarded the [[Meritorious Unit Commendation]] for its contributions during Operation Iraqi freedom II.<ref name= "DivHist"/>

[[File:1st US Infantry Division.svg|thumb|100px|left|Shoulder Sleeve Insignia of the 1st Infantry Division; the basis for the 1st Sustainment Brigade's own SSI.]]
As of January 2006, the DISCOM consisted of the 201st Field Support Battalion in Vilseck, Germany, the 701st Maneuver Support Battalion in Kitzingen, Germany, the 299th Field Support Battalion in Schweinfurt Germany, the [[601st Aviation Support Battalion]] in [[Katterbach]], Germany, and the Headquarters and Headquarters Company (HHC), also in Kitzingen. Over the months between January 2006 and July 2006 the 601st [http://www.riley.army.mil/Units/CAB/601ASB.aspx] returned to Fort Riley, the 701st was inactivated, the 299th was task organized to the [[2nd Brigade Combat Team, 1st Infantry Division (United States)|2nd Brigade Combat Team]] in preparation for another deployment to Operation Iraqi Freedom, and the 201st was task organized under the [[3rd Brigade Combat Team, 1st Infantry Division (United States)|3rd Brigade Combat Team]] and later deactivated. The DISCOM HHC was re-deployed to Fort Riley Kansas in August 2006 to build the 1st Sustainment Brigade.<ref name="DivHist"/><ref name="1STimes6">[http://www.1stid.org/about/pressreleases/1sb0907.pdf The 1st Sustainment Times], Page 6. 1st Sustainment Brigade Public Affairs. Retrieved 25 June 2008.</ref>

For a brief period, the DISCOM gained administrative control over the [[97th Military Police Battalion (United States)|97th Military Police Battalion]], the 541st CSSB, the Band, and the [[101st Military Intelligence Battalion]]. The 101st was in-activated in December 2006, the 541st and 97th were deployed and task organized away from the DISCOM.<ref name="DivHist"/>

In November 2006, the brigade reviewed its own [[Shoulder Sleeve Insignia]] (SSI) and [[Distinctive Unit Insignia]].<ref>[http://www.tioh.hqda.pentagon.mil/Sustain/1SustainmentBrigade.htm The Institute of Heraldry: 1st Sustainment Brigade], The Institute of Heraldry. Retrieved 25 June 2008.</ref> These items were based heavily on the SSI of the 1st Infantry Division. Later that month, the Brigade was informed that it would be deployed to Iraq again in 2007.<ref>[http://www.1stid.org/about/pressreleases/1sb0907.pdf The 1st Sustainment Times], Page 7. 1st Sustainment Brigade Public Affairs]. Retrieved 25 June 2008.</ref>

The 1st Sustainment Brigade (SB) was activated on 15 February 2007 at 10:00&nbsp;am local time at [[Fort Riley]], Kansas.<ref>[http://www4.army.mil/outreach/calendar/index.php?event_id=4019&date=2007-02-15&r=2007-02-01 US Army Public Affairs], United States Army. Retrieved 29 March 2008</ref> It is a scalable tailorable Sustainment Brigade, with a mission statement of: Plans, synchronizes, monitors, and executes distribution operations. Conducts sustainment operations within assigned area of operation. Conducts Theater Opening and/or Theater Distribution operations when directed. Provides support to joint, interagency, and multinational forces as directed.<ref name= "BdeHist" />

[[File:Army mil-2008-05-29-141932.jpg|thumb|A 1st Sustainment Brigade soldier briefs 10th Sustainment Brigade soldiers on the [[MRAP]] in Iraq.]]
The brigade deployed to Iraq again in late 2007, operating in the areas such as those around [[Central Iraq]].<ref name="1STimes6"/> The brigade's headquarters during this time has been [[Camp Taji]].<ref name="Taji's DFAC two gets new name">[http://www.blackanthem.com/News/living/Taji-s-DFAC-two-gets-new-name12079.shtml Taji's DFAC two gets new name], Maude, Bryant. ''Blackanthem.com Military News''. Retrieved 25 June 2008.</ref> The brigade made history on 16 June 2008 when it heralded the return of the [[battlefield promotion]] system in the US Army. The system, which was previously discontinued, was part of a pilot program that the Army was looking at to bring battlefield promotions back.<ref name="Taji's DFAC two gets new name"/>

Soldiers of the brigade were also some of the first to use the [[MRAP]], or Mine Resistant Ambush Protected vehicle. The soldiers used the vehicles to train other soldiers on use of the vehicle, particularly leaders of the [[10th Sustainment Brigade]], which replaced the 1st Sustainment Brigade in late 2008.<ref>[http://www.mnf-iraq.com/index.php?option=com_content&task=view&id=19926&Itemid=110 Soldiers Get First Look at MRAP], Schweizer, Jennifer. Multi-National Force-Iraq Homepage. Retrieved 25 June 2008.</ref>

The 1st Sustainment Brigade provides a full spectrum support including: configuring for, distributions and retrogrades to and from maneuver [[Brigade Combat Team]]s, other support brigades, and to joint interagency and multinational elements as directed. The 1st SB supports Early Entry Operations or Hub operations with augmentation, providing postal, replacement, and casualty operations as well as essential personnel services and Trial Defense Services on an area basis.<ref name= "BdeHist" /> The brigade is assigned as the sustainment unit of the 1st Infantry Division, however it can also operate independently, being assigned other units and other missions independent of the division.<ref>[http://www.1id.army.mil/UnitPage.aspx?unit=1stSustainmentBDE 1st Infantry Division Homepage: 1st Sustainment Brigade], 1st Infantry Division staff. Retrieved 29 March 2008</ref>

== Honors ==
As it was a part of the 1st Infantry Division's command, the Brigade received campaign participation credit and awards for all of the same conflicts as the Division Headquarters up until it became an independent unit in 2006. Thereafter, it retained separate lineage.<ref>[http://www.history.army.mil/html/forcestruc/lineages/branches/div/001id.htm 1st Infantry Division Headquarters: Lineage], United States Army. Retrieved 25 June 2008.</ref>

===Unit Decorations===
{| class="wikitable" border="1" cellpadding="4" cellspacing="0"
|- bgcolor="#efefef"
! Ribbon
! Award
! Year
! Notes
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]] 
||[[Meritorious Unit Commendation]] (Army)
||1968
||for service in Vietnam
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]] 
||[[Meritorious Unit Commendation]] (Army)
||1991
||for service in Southeast Asia
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]] 
||[[Meritorious Unit Commendation]] (Army)
||2004–2005
||for service in Operation Iraqi Freedom
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]] 
||[[Meritorious Unit Commendation]] (Army)
||2007–2009
||For service in [[Iraq]]
|-
||[[File:Ruban de la croix de guerre 1939-1945.PNG|50px]]
||[[Croix de guerre 1939–1945 (France)|French Croix de guerre]], World War II (With Palm)
||1943
||For service in [[Kasserine]]
|-
||[[File:Ruban de la croix de guerre 1939-1945.PNG|50px]]
||[[Croix de guerre 1939–1945 (France)|French Croix de guerre]], World War II (With Palm)
||1944
||For service in [[Normandy]]
|-
||[[File:Ruban de la croix de guerre 1939-1945.PNG|50px]]
||[[Croix de guerre 1939–1945 (France)|French Croix de guerre]], World War II (With Palm)
||1945
||For service in [[Fourragère]]
|-
||
||[[Belgian Fourragere]]
||1940
||For service in Belgium
|-
||
||Cited in the Order of the Day 
||1944
||For service in [[Mons]]
|-
||
||Cited in the Order of the Day 
||1944
||For service in [[Eupen]]–[[Malmedy]]
|-
||[[File:Gallantry Cross Unit Citation.png|50px]]
||[[Republic of Vietnam Cross of Gallantry]], with Palm
||1965–1968
||For service in [[Vietnam]]
|-
||[[File:Civil Action Unit Citation.png|50px]]
||[[Republic of Vietnam Civil Action Honor Medal]], First Class
||1965–1970
||For service in [[Vietnam]]
|}
{{-}}

===Campaign Streamers===
{| class=wikitable
|- bgcolor="#efefef"
! Conflict
! Streamer
! Year(s)
|-
|| World War I
|| [[United States campaigns in World War I#Montdidier-Noyon.2C 9.E2.80.9313 June 1918|Montdidier–Noyon]]
|| 1917
|-
|| World War I
|| [[United States campaigns in World War I#Aisne-Marne.2C 18 July .E2.80.93 6 August 1918|Aisne–Marne]]
|| 1917
|-
|| World War I
|| [[United States campaigns in World War I#St. Mihiel.2C 12.E2.80.9316 September 1918|St. Mihiel]]
|| 1917
|-
|| World War I
|| [[United States campaigns in World War I#Meuse-Argonne.2C 26 September .E2.80.93 11 November 1918|Meuse-Argonne]]
|| 1917
|-
|| World War I
|| [[Lorraine (province)|Lorraine]]
|| 1917
|-
|| World War I
|| Lorraine
|| 1918
|-
|| World War I
|| [[Picardy]]
|| 1918
|-
|| World War II
|| [[Algeria]]–[[French Morocco]] (With Arrowhead)
|| 1944–1945
|-
|| World War II
|| [[Tunisia]]
|| 1942
|-
|| World War II
|| [[Sicily]] (With Arrowhead)
|| 1943
|-
|| World War II
|| [[Normandy]] (With Arrowhead)
|| 1944
|-
|| World War II
|| [[Northern France Campaign|Northern France]]
|| 1944–1945
|-
|| World War II
|| [[Rhineland Campaign]]
|| 1944–1945
|-
|| World War II
|| [[Ardennes-Alsace Campaign]]
|| 1944–1945
|-
|| World War II
|| [[Central Europe Campaign]]
|| 1945
|-
|| [[Vietnam War]]
|| Vietnam Defense
|| 1965
|-
|| Vietnam War
|| Counteroffensive, Phase I
|| 1965–1966
|-
|| Vietnam War
|| Counteroffensive, Phase II
|| 1966–1967
|-
|| Vietnam War
|| Counteroffensive, Phase III
|| 1967–1968
|-
|| Vietnam War
|| [[Tet Offensive|Tet Counteroffensive]]
|| 1968
|-
|| Vietnam War
|| Counteroffensive, Phase IV
|| 1968
|-
|| Vietnam War
|| Counteroffensive, Phase V
|| 1968
|-
|| Vietnam War
|| Counteroffensive, Phase VI
|| 1968–1969
|-
|| Vietnam War
|| Tet 69/Counteroffensive
|| 1969
|-
|| Vietnam War
|| Summer–Fall 1969
|| 1969
|-
|| Vietnam War 
|| Winter–Spring 1970
|| 1970
|-
|| [[Gulf War]]
|| Defense of Saudi Arabia
|| 1990–1991
|-
|| [[Gulf War]]
|| Liberation and Defense of Kuwait
|| 1990–1991
|-
|| [[Gulf War]]
|| Cease-Fire
|| 1990–1991
|-
||[[Iraq War]]
|| [[Operation Iraqi Freedom I]]
|| 2003
|-
||Iraq War
|| Operation Iraqi Freedom II
|| 2004–2005
|-
||Iraq War
|| Operation Iraqi Freedom V
|| 2007–2008
|-
||Iraq War
|| Operation New Dawn
|| 2010-2011

|-
||[[War in Afghanistan (2001–present)|War in Afghanistan]]
|| Transition I
|| 2012-2013

|}
{{-}}

== References ==
{{reflist}}

== External links ==
* [http://www.riley.army.mil/Units/1stSustainBde/ 1st Infantry Division Homepage: 1st Sustainment Brigade]
* [http://www.tioh.hqda.pentagon.mil/Sustain/1SustainmentBrigade.htm The Institute of Heraldry: 1st Sustainment Brigade]

[[Category:Sustainment Brigades of the United States Army|001]]