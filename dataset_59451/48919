[[File:Johnny J. Jones,The Billboard, 1917.jpg|thumbnail|right|Johnny J. Jones, The Billboard, 1917]] 
'''Johnny J. Jones''' (June 8, 1874 – December 25, 1930) was an American [[Traveling carnival|carnival]] showman, the founder and manager of the [[Johnny Jones Exposition|Johnny J. Jones Exposition]]. The Exposition was one of the first to use steel railroad cars and one of the largest of its kind, exceeded in size only by the  [[Ringling Bros. and Barnum & Bailey Circus]]. In operation for over 50 years through the continental United States and Canada, the show reached a total of 50 steel cars carrying 100 wagons during its heyday in the 1920s.

==Personal life==
A native of Arnot, [[Tioga County, Pennsylvania]], Jenkins was the son of Welsh immigrants, working alongside his father in the local coal mines from the age of ten.<ref>The primary biographers of Johnny J. Jones are historians Bob Goldsack and Joe McKennon. See Bob Goldsack, ''A History of the Johnny J. Jones Exposition: ‘The Might Monarch of the Tented World’'', Midway Publications, Nashua, NH (1990) (hereinafter “Goldsack”) and Joe McKennon, “The Pictorial History of the American Carnival, Volume II”, Carnival Publishers of Sarasota, Florida (1972) (hereinafter “McKennon”); see also Fred Dahlinger, Jr.'s ''Show Trains of the 20th Century'', Circus World Museum (2000)<!-- ISBN # needed -->.</ref> He graduated to hawking newspapers first on the streets and then aboard the Pennsylvania Railroad passenger train, where he also sold sandwiches, cigars and sundries. [[Du Bois, Pennsylvania]] historian [[Major Israel McCreight]] recalled: “After the arrival of the 1 o'clock train Johnny J. Jones could be heard yelling at the top of his voice, "[[Pittsburgh Post-Gazette|Pittsburgh Gazette]]; all about the big fire." <ref>M.I. McCreight, Memory Sketches of Du Bois Pennsylvania 1874-1938: A History (1938) at p.7., Goldsack at p.7</ref>

Jones wed Etta Louise "Hody" Hurd in 1920 and fathered a son, Johnny J. Jones, Jr. (1921).<ref>McKennon at p.24 and Goldsack at p. 17.</ref> In his mid-40s at the time, he would not have long with his family. Jones had developed an alcohol problem in his youth, and in the mid 1920s was hospitalized for alcoholism. The impact on his health was profound. He died of [[renal failure]] at the age of 56 on Christmas Day in 1930.<ref>McKennon at p.24.</ref>

==Carnival career==
Jones entered the carnival business in 1895 with his purchase of a cane rack booth at a local fairground and opened his first small traveling fair in 1899. Localized to Western Pennsylvania,  'The Johnny J. Jones Ferris Wheel Company'' featured a miniature railroad and Ferris wheel among its entertainments.<ref>Goldsack, p. 7</ref> Jones's carnival evolved to become the "Johnny J. Jones Exposition Shows & Trained Wild Animal Exhibition" in 1906 when he added a circus, and he began touring regionally through the Eastern United States and Canada.<ref>Goldsack at p.49 and The Billboard, “Obituary of Johnny J. Jones”, January 3, 1931.</ref> In 1916, he expanded into the west.<ref name="McKennonat">McKennon at p. 23.</ref>

His fame grew along with his program; in 1917, he was featured on the covers of  ''[[Billboard (magazine)|Billboard]]'' and [[Optimist International|Optimist]] magazines. Jones had a reputation for running a clean operation, suitable for children.<ref>McKennon at p. 94, Goldsack at p.33. "'Bring the Sunday School class' was one classic he contributed to the annals of ballyhoo.  The reference was to the clean, wholesome amusements the Jones' show boasted." "Pretty Young Widow is Operator of Successful Carnival-Circus", ''The Atlanta Constitution'', Feb. 2, 1933, p. 17.</ref> By 1928, Jones had two traveling carnivals. He combined these into the second-largest traveling show in America, the 50 car, 100-wagon exhibition coming in behind Ringling Bros. and Barnum & Bailey Circus.<ref name="McKennonat" />

In 1929, Jones made national press again when he paid $2,000 to fly a female gorilla into the United States from France.  Susie the Gorilla was not only the only female gorilla in the United States at the time, but the first gorilla in the world to be trained. Her twice daily performances featured her eating with a knife and fork. Susie was featured in the Johnny J. Jones Exposition, the [[Miller Brothers 101 Ranch]] and the Ringling Bros. and Barnum & Bailey Circus before permanently relocating to the [[Cincinnati Zoo and Botanical Garden]] in 1931.<ref>See http://circusnospin.blogspot.com/2009/10/vintage-cincinnati-zoo-suzie-graf.html.</ref><ref>“Johnny J. Jones Exposition”, Circus Magazine (1930) at p.42-47, and Du Bois Courier, “Obituary of Johnny J. Jones”, December 27, 1930.</ref>

Jones suffered badly during the Great Depression. During the 1930, debts amassed, but the show survived his death on 25 December 1930. His family continued the show without him, Hody Hurd Jones managing the Exposition until 1950,<ref>Goldsack at p.29.</ref> with the help (aside from a military stint in [[World War II]] of the couple's son.<ref>Goldsack at p.66.</ref> It could not be sustained forever and closed with a final show in Du Bois Pennsylvania. In 1951, the IRS sold its equipment to satisfy tax debt.<ref>McKennon at p. 111.</ref>
<gallery>
File:Johnny J. Jones and Midget Troupe.jpg|Johnny J. Jones and his Troupe of Belgian and French Midgets
File:Johnny J. Jones Exposition, Edmonton Exhibition of 1919.jpg|Johnny J. Jones Midway, Edmonton Exhibition of 1919, Glenbow Museum Archives, Alberta, Canada
<!-- Deleted image removed: File:Susie the Graf Zepplin Gorilla and Johnny J. Jones.jpg|"Susie the ''[[LZ 127 Graf Zeppelin|Graf Zeppelin]]'' Gorilla" and Johnny J. Jones "The Midway King", August 4, 1929 {{deletable image-caption|Friday, 12 December 2014}} -->
File:Johnny J .Jones, Du Bois Sign.jpg|The sign honoring Du Bois on the show
</gallery>

== Notes ==
{{Reflist|2}}

== Bibliography ==
* Bob Goldsack, “A History of the Johnny J. Jones Exposition: ‘The Mighty Monarch of the Tented World’”, Midway Museum Publications, Nashua, NH. (1990)
* Joe McKennon, “The Pictorial History of the American Carnival, Volume II”, [http://openlibrary.org/publishers/Carnival_Publishers_of_Sarasota Carnival Publishers] of Sarasota, Florida (1972)
* Fred Dahlinger, Jr., “Show Trains of the 20th Century”, [http://circusworld.wisconsinhistory.org/ Circus World Museum] (2002)

== External links ==
* Find a Grave, http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=130129715
* Circus World of the Wisconsin Historical Society http://circusworld.wisconsinhistory.org/
* Circus Historical Society http://www.circushistory.org/
* Showmen's League of America http://www.showmensleague.org/
* Buckles Blog http://bucklesw.blogspot.com/

{{DEFAULTSORT:Jones, Johnny J.}}
[[Category:1874 births]]
[[Category:1930 deaths]]
[[Category:American people of Welsh descent]]
[[Category:American entertainment industry businesspeople]]
[[Category:Circus proprietors]]
[[Category:People from Tioga County, Pennsylvania]]
[[Category:People from Clearfield County, Pennsylvania]]