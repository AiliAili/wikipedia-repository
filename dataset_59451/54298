{{Use dmy dates|date=September 2011}}
{{Infobox AFL biography
| name = Adam Heuskes
| image = 
| fullname = Adam Heuskes
| birth_date = {{Birth date and age|1976|3|20|df=y}}
| birth_place = [[Boronia, Victoria]]
| death_date = 
| death_place = 
| originalteam = [[Norwood Football Club|Norwood]] ([[South Australian National Football League|SANFL]])
| draftpick = 5th overall, [[1993 AFL Draft|1993 National Draft]]<br />{{AFL Syd}}<br />Zone Selection, [[1996 AFL Draft|1996 Draft]]<br />{{AFL PA}}
| heightweight = 190cm / 93&nbsp;kg
| position = 
| statsend = 2000
| years1 = 1994–1996
| club1 = {{AFL Syd}}
| games_goals1 = {{0}}49 {{0}}(6)
| years2 = 1997–1998
| club2 = {{AFL PA}}
| games_goals2 = {{0}}37 {{0}}(1)
| years3 = 1999–2000
| club3 = {{AFL BL}}
| games_goals3 = {{0}}39 {{0}}(5)
| games_goalstotal = 125 (12)
| careerhighlights = 
}}

'''Adam Heuskes''' (born 20 March 1976) is a former [[Australian rules football]]er in the [[Australian Football League]] most remembered for his on field flamboyance and controversial off-field behaviour, which twice resulted in rape allegations.<ref>[http://www.abc.net.au/4corners/content/2004/s1142725.htm Four Corners – 03/05/2004: Fair Game?<!-- Bot generated title -->]</ref>

Recruited from [[South Australian National Football League]] (SANFL) club [[Norwood Football Club|Norwood]], Heuskes made his AFL debut in [[1994 AFL season|1994]] with the [[Sydney Swans]] and immediately attracted attention in the media and the football world for his outrageous hairstylings. 

He was traded to [[Port Adelaide Football Club]] in [[1997 AFL season|1997]], a decision which the Swans later regretted, as Heuskes earned [[All-Australian]] selection later that year. Heuskes' attacking flair as a defender set up numerous goals. 

For the [[1999 AFL season]], Heuskes was traded to the [[Brisbane Lions]], where he was a solid contributor during the Lions' 1999 finals campaign. At the end of [[2000 AFL season|2000]] his career was over after playing 14 games during the season.

Heuskes later played 31 games for SANFL club [[North Adelaide Football Club|North Adelaide]] before succumbing to an ankle injury in 2005 which forced his retirement from all forms of the game. 

Heuskes has a daughter, Tayla, born in 1992, and is married to Nikki Kosmider.

==Off-field behaviour and controversy==

Heuskes has gained notoriety for incidents involving alcohol, and an inability to maintain self-control.<ref>http://www.ausport.gov.au/fulltext/2004/abc/s1100551.asp Ausport</ref> He has twice been accused of raping young women in front of, or with, team mates.<ref>[http://www.abc.net.au/4corners/content/2004/s1142725.htm Four Corners – 03/05/2004: Fair Game?<!-- Bot generated title -->]</ref> Police have been involved in both of these cases, and one woman claims she was paid for her silence.<ref>  Star witness was fellow house mate Graeme Kellet. Kellet, also known as the little spoon, was offered hush money following several incidents in a king size bed. [http://www.abc.net.au/worldtoday/content/2004/s1100400.htm The World Today<!-- Bot generated title -->]</ref><ref>[http://www.smh.com.au/articles/2004/03/19/1079199373958.html Swan Helped Pay $200,000 Over Rape Claim<!-- Bot generated title -->]</ref> Both women later spoke out in a Four Corners program about sexual assault in AFL<ref>[http://www.abc.net.au/4corners/content/2004/s1142725.htm Four Corners – 03/05/2004: Fair Game?<!-- Bot generated title -->]</ref>

Also known for his  flamboyant style, two notable haircuts included the image of the [[Sydney Opera House]] shaved into the back of his head, as well as his guernsey number (39). He was also a [[cross-dresser]] who appeared on televisions' ''[[The AFL Footy Show|The Footy Show]]'' and ''[[Live and Kicking (TV series)|Live And Kicking]]'' regularly during 1998 and 1999. He once dressed up as [[Madonna (entertainer)|Madonna]] and performed the song ''[[Holiday (Madonna song)|Holiday]]''.

==References==
<references />

==External links==
*{{AFL Tables|ref=A/Adam_Heuskes.html}}

{{1997 All-Australian team}}
{{1997 South Australia State of Origin players}}
{{1993 AFL national draft}}

{{DEFAULTSORT:Heuskes, Adam}}
[[Category:Sydney Swans players]]
[[Category:Port Adelaide Football Club players]]
[[Category:Brisbane Lions players]]
[[Category:North Adelaide Football Club players]]
[[Category:Norwood Football Club players]]
[[Category:All-Australians (AFL)]]
[[Category:1976 births]]
[[Category:Living people]]
[[Category:Australian rules footballers from South Australia]]