{{redirect |Sky Knight|the Lakewood, California law enforcement program|Sky Knight Helicopter Program}}
{{use dmy dates|date=September 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name        = F3D (F-10) Skyknight
|image       = EF-10B VMCJ-1.jpg|caption= EF-10B ''Skyknight'' of VMCJ-2 ''Playboys''
}}{{Infobox Aircraft Type
|type        = [[Fighter aircraft]]
|manufacturer= [[Douglas Aircraft Company]]
|first flight= 23 March 1948
|introduced  = [[1951 in aviation|1951]]
|retired     = [[1970 in aviation|1970]]
|status      = Retired
|primary user= [[United States Navy]]
|more users  = [[United States Marine Corps]]
|produced    =
|number built=265
|variants with their own articles=[[Douglas F6D Missileer]]}}
|}
The '''Douglas F3D Skyknight''' (later designated '''F-10 Skyknight''') was a [[United States]] twin-engined, mid-wing [[fighter aircraft|jet fighter aircraft]] manufactured by the [[Douglas Aircraft Company]] in [[El Segundo, California]]. The F3D was designed as a carrier-based all-weather [[night fighter]] and saw service with the [[United States Navy]] and [[United States Marine Corps]]. The mission of the F3D was to search out and destroy enemy aircraft at night.<ref>''Standard Aircraft Characteristics F3D-2 Skyknight'' NAVAER 1335C REV. 10-51.</ref>

The F3D Skyknight was never produced in great numbers but it did achieve many firsts in its role as a night fighter over [[Korea]]. While it never achieved the fame of the [[North American F-86 Sabre]], it did down several Soviet-built [[Mikoyan-Gurevich MiG-15|MiG-15]]s as a night fighter over Korea with only one air-to-air loss of its own against a [[PLA Air Force|Chinese MiG-15]], which occurred on the night of 29 May 1953.<ref name="dtic.mil">[http://www.dtic.mil/dtic/tr/fulltext/u2/a407636.pdf "Defense POW/Missing Personnel Office: Korean War Air Loss Database (KORWALD)."] ''dtic.mil.'' Retrieved: 10 August 2013.</ref><ref>Zhang 2002, pp. 194–195.</ref>

The Skyknight played an important role in the development of the radar-guided [[AIM-7 Sparrow]] missile which led to further guided [[air-to-air missile]] developments. It also served as an electronic warfare platform in the [[Vietnam War]] as a precursor to the [[Grumman A-6 Intruder|EA-6A Intruder]] and [[Northrop Grumman EA-6B Prowler|EA-6B Prowler]]. The aircraft is sometimes unofficially called "Skynight", dropping the second "k". The unusual, portly profile earned it the nickname "Willie the Whale".<ref>[http://www.boeing.com/history/mdc/skyknight.htm "Douglas Skyknight."] {{webarchive |url=https://web.archive.org/web/20080511222616/http://www.boeing.com/history/mdc/skyknight.htm |date=11 May 2008 }} ''Boeing history.'' Retrieved: 23 August 2010.</ref> Some Vietnam War [[United States Marine Corps|U.S. Marine]] veterans have referred to the Skyknight as "[[Turd|Drut]]", whose meaning becomes obvious when read backwards. This may be in reference to its age, unflattering looks or its low-slung air intakes that made it vulnerable to [[foreign object damage]] (FOD).<ref name="Goebel"/>

==Design and development==
The F3D was not intended to be a typical sleek and nimble dogfighter, but as a standoff night fighter, packing a powerful radar system and a second crew member. It originated in 1945 with a US Navy requirement for a jet-powered, radar-equipped, carrier-based night fighter. The Douglas team led by [[Ed Heinemann]] designed around the bulky air intercept radar systems of the time, with [[side-by-side seating]] for the pilot and radar operator.<ref name= "donald world">Donald 1997, p. 365.</ref> The result was an aircraft with a wide, deep, and roomy fuselage. Instead of ejection seats, an escape tunnel was used, similar to the type used in the [[Douglas A-3 Skywarrior|A-3 Skywarrior]].<ref name="donald world"/>

The XF3D-1 beat out [[Grumman]] Aircraft Engineering Corporation's G-75 two-seat, four-engined, [[Westinghouse J30]]-powered night fighter design, and a contract was issued on 3 April 1946. The US Navy's Bureau of Aeronautics ([[BuAer]]) also issued a contract to Grumman for two G-75 (company designation) XF9F-1 (BuAer designation) experimental aircraft on 11 April 1946 in case the Skyknight ran into problems. Grumman soon realized that the G-75 was a losing design but had been working on a completely different, single-engined day fighter known as the Grumman G-79 which became the [[F9F Panther]].<ref>Sullivan 1982, pp. 4, 6.</ref>{{refn|Due to some interesting bureaucracy, BuAer did not cancel the G-75 (XF9F-1) contract, however, but changed the wording to include three entirely different G-79 prototypes. The G-79 became the successful [[Grumman F9F Panther]].<ref>Hardy 1987, p. 79.</ref><ref name="Airvectors.net">Goebel, Greg. [http://www.airvectors.net/avf9f.html "The Grumman F9F Panther/Cougar."] ''Airvectors,'' 1 July 2013. Retrieved 4 August 2013.</ref>|group=N}}
[[File:F3D-2 VMFN-513 Kunsan radar2 1953.jpg|thumb|Maintenance on an APQ-35 radar of an F3D-2 in Korea, 1953]]

The first flight of the '''XF3D-1''' was on 23 March 1948 at Douglas' El Segundo facility with test pilot [[Russell William Thaw|Russell Thaw]] at the controls.{{refn|Russell Thaw as a test pilot for Douglas, and besides the F3D, was responsible for a number of test programs, including the [[Douglas XB-43 Jetmaster]] and [[Douglas F4D Skyray|XF4D-1 Skyray]].<ref>[http://thetartanterror.blogspot.ca/2011/04/russell-william-thaw-1910-1984.html "Russell William Thaw, 1910–1984."] ''Test & Research pilots, Flight Test Engineers.'' Retrieved: 3 August 2013.</ref>|group=N}} Further flight testing followed at El Segundo until October 1948. Three prototypes were then taken to [[Muroc Air Force Base]] (later renamed Edwards Air Force Base) for service trials. These units were powered by two [[Westinghouse J34]]-WE-24 turbojets of 3,000&nbsp;lbf (1,361 kgf) thrust, mounted under the roots of then-standard straight wings of the early jet era. A production contract for 28 '''F3D-1''' J34-WE-32 powered production aircraft was issued in June 1948 with the first production aircraft flying on 13 February 1950.<ref name="Gunstonp172">Gunston 1981, p. 172.</ref>

As a night fighter that was not expected to be as fast as smaller daylight fighters, the expectation was to have a stable platform for its radar system and the four 20&nbsp;mm cannon mounted in the lower fuselage. The F3D was, however, able to outturn a MiG-15.<ref name="Dorrp154">Dorr 1994, p. 154.</ref> The fire control system in the F3D-1 was the [[Westinghouse Electric (1886)|Westinghouse]] AN/APQ-35. The AN/APQ-35 was advanced for the time, a combination of three different radars, each performing separate functions: an AN/APS-21 search radar, an AN/APG-26 tracking radar, both located in the nose, and an AN/APS-28 tail warning radar.<ref name="Badrocke p41,44-5">Badrocke 1993, pp. 41, 44–45.</ref> The complexity of this [[vacuum tube]]-based radar system, which was produced before the advent of semiconductor electronics, required intensive maintenance to keep it operating properly.

The F3D-1 was followed by the F3D-2, which was first ordered in August 1949. The '''F3D-2''' was intended to have [[Westinghouse J46]] engines in enlarged nacelles to replace the J34-WE-32 engines of the F3D-1, but because of development problems with the J46, the F3D-2 was initially fitted with J34-WE-36 engines instead. Higher-thrust J34-WE-38 engines which increased aircraft performance were installed later.<ref name="Gunstonp172"/> The F3D-2 also incorporated an improved Westinghouse AN/APQ-36 fire control system. A total of 237 F3D-2s were built before production ended on 23 March 1952. A higher performance F3D-3 version with swept wings and J46 engines was planned, but was cancelled when the trouble-plagued J46 engine program was terminated.

==Operational history==
[[File:F3D-2 VMFN-513 Kunsan 1953.jpg|thumb|F3D-2s of VMFN-513 at Kunsan, Korea, in 1953]]

===Korean War===
The 28 F3D-1 aircraft were used primarily to train F3D crews and did not see combat in the [[Korean War]]. The F3D-2 Skyknight was only deployed to Korea by [[USMC]] land–based squadrons, beginning in September 1952.<ref>[https://web.archive.org/web/19980703150755/http://www.history.navy.mil:80/branches/hist-ac/f3d-2.pdf "SkyKnight".] ''Naval Aviation News''. Retrieved: 2 August 2013.</ref> The Skyknight downed more enemy aircraft in Korea than any other single type of naval aircraft.<ref>[http://www.flyingleathernecks.org/jets4.html "Douglas F3D-2 Sky Knight."] ''Flying Leatherneck Historical Foundation and Aviation Museum.'' Retrieved: 16 December 2007.</ref> The first air-to-air victory occurred on the night of 2 November 1952 in a USMC F3D-2 piloted by [[Major (United States)|Major]] William T. Stratton, Jr., and his radar operator, [[Master Sergeant]] Hans C. Hoglind of [[VMA-513|VMF(N)-513]] Flying Nightmares,<ref name="Grossnick p768"/> Major Stratton shot down what he believed was a [[Yakovlev Yak-15]] (even though no Yak-15s were reported in Korea)<ref name="Goebel">Goebel, Greg.[http://www.faqs.org/docs/air/avskykt.html "The Douglas F3D Skyknight."] ''Airvectors.net,'' 1 September 2002. Retrieved: 2 August 2013.</ref> which was the first successful night radar interception by a jet of a jet.<ref name="Dorrp143">Dorr 1994, p. 143</ref> The Skyknight claimed its first MiG-15 jet fighter on 8 November 1952, when Captain O.R. Davis and [[Warrant Officer]] D.F. "Ding" Fessler downed a MiG-15 northwest of [[Pyongyang]].<ref name="Dorrp143"/> USMC pilot Lt. Joseph Corvi and his radar operator Sergeant Dan George set another record with the Skyknight on the night of 10 December 1952, when they downed the first aircraft by an aircraft with a radar track and lock-on and without visual contact; they performed the feat by using their radar to lock onto a [[Polikarpov Po-2]] [[biplane]]. They were also credited with another probable kill that night.<ref name="Dorrp149">Dorr 1994, p. 149.</ref>

The number of USMC Skyknights in Korea was doubled in January 1953 to 24 which allowed them to effectively escort [[Boeing B-29 Superfortress|B-29 Superfortresses]] on night bombing missions.<ref name="Dorrp153">Dorr 1994, p. 153.</ref> On 12 January 1953, an F3D-2 of VMF(N)-513 that was escorting B-29s on a night bombing mission was vectored to a contact and shot down the fourth aircraft by a Skyknight.<ref name="Dorrp154">Dorr 1994, p. 154.</ref> By the end of the war, Skyknights had claimed six enemy aircraft (one Polikarpov Po-2, one Yakovlev Yak-15 and four MiG-15s).<ref name="Grossnick p768">Grossnick 1997, p. 768.</ref> One aircraft was lost to enemy fire, which was piloted by LTJG Bob Bick and his crewman, [[Chief Petty Officer]] Linton Smith, on 2 July 1953. This aircraft was with a detachment from Fleet Composite Squadron FOUR (VC-4) at [[Atlantic City Air National Guard Base|NAS Atlantic City]], and was attached to Marine Fighter Squadron 513 (VMF(N)-513).<ref>O'Rourke, G.G. and E.T. Woodbridge. ''Night Fighters Over Korea''. Annapolis, Maryland: Naval Institute Press, 1998. ISBN 1-55750-653-1.</ref> While the Skyknight lacked the swept wings and high subsonic performance of the MiG-15, its powerful fire control system enabled it to find and kill other fighters at night, while most MiG-15s could only be guided by ground-based radar.

===Post Korean War===
[[File:XF3D-1 AAM-N-2 NAN7-54.jpg|thumb|left|F3D–1 Skyknight carrying AAM-N-2 Sparrow I missiles during tests in the early 1950s]]
[[File:F3D-1 Skyknight launches Sparrow missile in 1950.jpg|thumb|left|F3D–1 Skyknight firing a AAM-N-2 Sparrow I missile during a test in 1950]]
After the Korean War, the F3D was gradually replaced by more powerful aircraft with better radar systems. Its career was not over though; its stability and spacious fuselage made it easily adaptable to other roles. The F3D (under the designations '''F3D-1M''' and '''F3D-2M''') was used to support development of a number of air-to-air missile systems during the 1950s, including the [[AIM-7 Sparrow|Sparrow]] I, II, and III and Meteor missiles.<ref name="Parsch">Parsch, Andreas. [http://www.designation-systems.net/dusrm/m-7.html "Raytheon AAM-N-2,3,6/AIM-101/AIM-7/RIM-7 Sparrow."] ''Directory of U.S. Military Rockets and Missiles'', 2007. Retrieved: 5 August 2013.</ref> The Sparrow missile was developed at [[Naval Air Missile Test Center|Pacific Missile Test Center]] and early test firings were conducted at [[Naval Air Weapons Station China Lake|Naval Ordnance Test Station China Lake]].

In 1954, the F3D-2M was the first Navy jet aircraft to be fitted with an operational air-to-air missile: the Sparrow I,<ref>[https://books.google.com/books?id=sdwDAAAAMBAJ&pg=PA116&dq=1954+Popular+Mechanics+January&hl=en&sa=X&ei=Pv4YT5SlG5PoggekxISEDA&ved=0CDgQ6AEwAg#v=onepage&q&f=true "Guided Missiles Ride Navy Jet."] ''Popular Mechanics'', November 1954, p. 116.</ref> an all weather day/night [[Beyond-visual-range missile|BVR]] missile that used beam riding guidance for the aircrew to control the missile's track. Only 28 aircraft (12 F3D-1Ms,<ref name="swanp183">Swanborough and Bowers 1976, p. 183.</ref> and 16 F3D-2Ms<ref name="swanp182">Swanborough and Bowers 1976, p. 182.</ref>) were modified to use the missiles.

In the late 1950s, a number of the Marine F3D-2 aircraft were re-configured as electronic warfare aircraft and were designated '''F3D-2Q''' (later '''EF-10B'''). A few aircraft were also converted for use as trainers and were designated  '''F3D-2T'''. Some of these aircraft were fitted with a single 10" aerial reconnaissance photography camera, which was mounted in the tail section.{{Citation needed|date=December 2014}}

When the U.S. Navy issued a requirement for a fleet defense missile fighter in 1959, Douglas responded with the [[Douglas F6D Missileer|F6D Missileer]], essentially an updated and enlarged F3D that would carry the AAM-N-10 Eagle long-range [[air-to-air missile]], with the most important characteristics being its generous fuel capacity, its considerable time-on-station, a crew of two and sophisticated electronics, rather than speed or maneuverability. This concept which kept the straight wings in an age of supersonic jets was soon cancelled because it would not be able to defend itself against more nimble fighters.<ref name="Badrocke p47">Badrocke 1993, p. 47.</ref><ref name="Frnc 79 p717">Francillon 1979, p. 717.</ref> Its weapons system would be adapted for the supersonic swing-wing [[General Dynamics-Grumman F-111B]], the U.S. Navy version of a joint USAF/USN tactical jet aircraft which also specified side-by-side seating. The USAF version would eventually see service as an air-to-ground fighter bomber, but the Navy version, envisioned as a Fleet Air Defense fighter and dogfighter, would be cancelled when it was clear that its performance was not sufficient for an air-to-air dogfighter role. The AWG-9/Phoenix and TF30 turbofan engine would eventually enter service on the F-111B's successor, the swing-wing [[Grumman F-14 Tomcat]].

Skyknights continued in service through the 1960s in a gull white color scheme, when their contemporaries had long since been retired. In 1962, when the U.S. Navy and U.S. Air Force unified their designation systems, the F3D-1 was redesignated '''F-10A''' and the F3D-2 was re-designated '''F-10B'''.

===Vietnam War===
[[File:EF-10B VMCJ-1 DaNang.jpg|thumb|EF-10B ''Skyknight'' of VMCJ-1]] 
[[File:EF-10B VMCJ-2 Vietnam NAN11-66.jpg|thumb|left|EF-10B (BuNo 127041) of VMCJ-1 over Vietnam in 1966. This aircraft was downed by an SA-2 missile from the North Vietnamese 61st Battalion, 236th Missile Regiment over Nghe An province on 18 March 1966 (coordinates 191958N 1050959E). The crew, 1stLt Brent Davis and 1stLt Everett McPherson, were killed.]]
The Skyknight was the only Korean war jet fighter that also flew in Vietnam. EF-10Bs served in the [[Electronic warfare]] role during the Vietnam War until 1969. The large interior provided ample room for electronic equipment. U.S. Marine [[VMAQT-1|Composite Reconnaissance Squadron One VMCJ-1]] ''Golden Hawks'' began operating the EF-10B in Vietnam on 17 April 1965 under Lt. Col Wes Corman at [[Da Nang Air Base]] [[Republic of Vietnam]] with six aircraft.<ref name="Whitten"/> No more than 10 EF-10Bs were in Vietnam at one time. The Electronic Warfare (EW) Skyknight was a valuable [[Electronic countermeasure]] (ECM) asset to jam the [[SA-2]] [[surface-to-air missile]]s (SAM) tracking and guidance systems.<ref name="Goebel"/> VMCJ-1 made history when its EF-10Bs conducted the first USMC airborne [[radar jamming]] mission on 29 April 1965 to support a [[United States Air Force|USAF]] strike mission. Four EF-10Bs also supported a massive strike on the SA-2 [[Surface-to-air missiles|SAM]] sites near [[Hanoi]] on 27 July 1965.

Many U.S. aircraft were lost to [[S-75 Dvina|SA-2]] surface-to-air missiles in Vietnam and the electronic attack on the associated radar systems was known as "Fogbound" missions. The F3D also dropped chaff over the radar sites.<ref name="Goebel"/> The first EF-10B lost in Vietnam was to an SA-2 SAM on 18 March 1966, while four more EF-10Bs were lost in Vietnam to accidents and unknown causes.<ref name="Whitten"/> Their mission was gradually taken over by the more capable EA-6A "Electric Intruder", an Electronic Warfare/Electronic Countermeasures (EW/ECM) variant of the [[Grumman A-6 Intruder]] attack bomber.<ref name="Combatairmuseum.org">[http://www.combatairmuseum.org/aircraft/douglasf3skyknight.html "Douglas F-3D-2T2 (TF-10B) Skyknight."]''Combat Air Museum.'' Retrieved: 3 August 2013.</ref> The EF-10B Skyknight continued to fly lower–threat EW missions until they were withdrawn from Vietnam in October 1969.<ref name="Whitten">Whitten, H. Wayne, Col USMC Retired.[http://www.mcara.us/VMCJ-1.html "VMCJ-1 History".] ''Marine Corps Aviation Reconnaissance Association'', June 2008. Retrieved: 8 August 2013.</ref> The U.S. Navy's EKA-3 Skywarrior and the USAF's [[Douglas B-66 Destroyer|Douglas RB-66 Destroyer]] also took on EW missions.

The U.S. Marine Corps retired its last EF-10Bs in May 1970.

===Post Vietnam===
The U.S. Navy continued to use the F-10s for [[avionics]] systems testing. The F-10 was used as a [[radar]] [[testbed]] to develop the APQ-72 radar. The nose of an [[McDonnell Douglas F-4 Phantom II|F-4 Phantom]] was added to the front of an F-10B. Another F-10 had a modified [[radome]] installed by the radar manufacturer [[Westinghouse Electronic Systems|Westinghouse]]. Yet another TF-10B was modified with the nose from an [[Douglas A-4 Skyhawk|A-4 Skyhawk]].<ref name="Joseph F. Baugher">[http://www.joebaugher.com/navy_fighters/f10_2.html "Douglas F3D-2/F-10B Skyknight"]''Joseph F. Baugher.'' Retrieved: 19 April 2015.</ref> In 1968, three Skyknights were transferred to the U.S. Army. These aircraft were operated by the [[Raytheon]] Corporation at [[Holloman AFB]] where they were used testing at the [[White Sands Missile Range]] into the 1980s; they were the last flyable Skyknights.<ref>Yakubov, Vladimir. [http://svsm.org/gallery/f3d-skyknight/P13704101 "Douglas F3D-2N Skyknight, USS Intrepid Museum."] ''SVSM Gallery.'' Retrieved: 18 August 2013.</ref>

==Variants==
;XF3D-1
:Prototype aircraft, two Westinghouse J34-WE-24 turbojet engines of 3,000 lbf (1,361 kgf), APQ-35 search and target acquisition radar, four 20mm cannon, three built.<ref name="Gunstonp172"/>
;F3D-1
:Two-seat all-weather day or night-fighter aircraft, powered by two 3,000 lbf Westinghouse J34-WE-32 turbojet engines, tail warning radar, ECM, and other electronics that added over 5,000 lb (2,267 kg) of weight, 28 built. First flight: 13 February 1950.<ref name="Gunstonp172"/>
;F3D-1M
:12 F3D-1s were converted into missile-armed test aircraft, used in the development of the [[AIM-7 Sparrow]] air-to-air missile.
;F3D-2
:Second Production version, initially powered by two 3,400 lbf (1,542 kgf) Westinghouse J34-WE-36 and later by two 3,600 lbf (1,633 kgf) Westinghouse J34-WE-38 turbojet engines, 565 mph (909 km/h) @ 20,000 ft (6,096 m), equipped with wing spoilers, autopilot and an improved Westinghouse AN/APQ-36 radar, 237 built. First flight: 14 February 1951.<ref name="Goebel" /><ref name="Gunstonp172"/>
;F3D-2B
:One F3D-1 was used for special armament test in 1952.
;F3D-2M
:16 F3D-2s were converted into missile armed aircraft. The F3D-2Ms were armed with AIM-7 Sparrow air-to-air missiles.
;F3D-2Q
:35 F3D-2s were converted into electronic warfare aircraft.
;F3D-2T
:Five F3D-2s were converted into night fighter training aircraft.
;F3D-2T2
:55 F3D-2s were used as radar-operator trainers and electronic warfare aircraft.
;F3D-3
:Unbuilt project, intended to be an advanced version incorporating swept wings.

;F-10A
:1962 re-designation of the F3D-1.
;F-10B
:1962 re-designation of the F3D-2.
;EF-10B
:1962 re-designation of the F3D-2Q.
;MF-10A
:1962 re-designation of the F3D-1M.
;MF-10B
:1962 re-designation of the F3D-2M.
;TF-10B
:1962 re-designation of the F3D-2T2.

==Operators==
{{USA}}
*[[United States Army]]
*[[United States Marine Corps]]
*[[United States Navy]]

==Aircraft on display==
;F3D-2
*BuNo 124598 - [[National Museum of Naval Aviation]] at [[NAS Pensacola]], [[Florida]].<ref>[http://www.navalaviationmuseum.org/attractions/aircraft-exhibits/item/?item=f3d_skyknight "F3D Skyknight/124598."] ''National Museum of Naval Aviation.'' Retrieved: 16 January 2015.</ref>
*BuNo 124629 - [[Pima Air & Space Museum]] adjacent to [[Davis-Monthan AFB]] in [[Tucson]], [[Arizona]].<ref>[http://www.pimaair.org/visit/aircraft-by-name/item/douglas-tf-10b-skyknight "F3D Skyknight/124629."] ''Pima Air & Space Museum.''Retrieved: 16 January 2015.</ref>
*BuNo 125807 - [[Combat Air Museum]] in [[Topeka, Kansas]].<ref>[http://combatairmuseum.org/aircraft/douglasf3skyknight.html "F3D Skyknight/125807."] ''Combat Air Museum.''Retrieved: 21 July 2011.</ref>
*BuNo 125870, (repainted as BuNo 127039) - Korean War and Vietnam War memorial in Del Valle Park in [[Lakewood, California]]. Originally displayed in 1950s-era dark blue coloring, the aircraft was repainted in 1963 to the grey and white color scheme of Marine Corps aircraft at the time.<ref>[http://aerialvisuals.ca/LocationDossier.php?Serial=7951 "F3D Skyknight/125870."] ''aerialvisuals.ca'' Retrieved: 8 April 2015.</ref> With this repainting, the aircraft had the tail code "7L," which was the 1960s-era tail code for Marine Corps Air Reserve and Naval Air Reserve aircraft at nearby [[Naval Air Station Los Alamitos]], California. In 2015, the aircraft was refurbished and painted in grey and white to depict a late 1950s-era Skyknight of VMFT(N)-20 with tail code "BP." {{Citation needed|date=May 2015}}
;F3D-2Q
*BuNo 124618 - [[National Museum of the Marine Corps]], in [[Quantico, Virginia]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=74522 "F3D Skyknight/124618."] ''aerialvisuals.ca'' Retrieved: 16 January 2015.</ref>
*BuNo 124620 - [[Quonset Air Museum]] at [[Quonset State Airport]] (former [[NAS Quonset Point]]) in [[Quonset Point]], [[Rhode Island]].<ref>[http://www.quonsetairmuseum.com/index.php?page=exhibitions "F3D Skyknight/124620."] ''Quonset Air Museum.'' Retrieved: 16 January 2015.</ref>
*BuNo 124630 - [[Flying Leatherneck Aviation Museum]] at [[MCAS Miramar]], [[California]].<ref>[http://flyingleathernecks.org/wp-content/uploads/2013/10/Aircraft_Listing.pdf "F3D Skyknight/124630."] ''Flying Leatherneck Aviation Museum.'' Retrieved: 16 January 2015.</ref>
*BuNo 125850 - [[Air Force Test Center|Air Force Flight Test Center Museum]] at [[Edwards AFB]], [[California]]. This aircraft served until 1970 as part of VMCJ-3 (U.S. Marine Composite Reconnaissance Squadron 3) based at [[Marine Corps Air Station El Toro]], California, carrying tail code "TN."<ref>[http://afftcmuseum.org/exhibits/museum-aircraft-exhibits/ "F3D Skyknight/125850."] ''Air Force Flight Test Center Museum'' Retrieved: 16 January 2015.</ref>
;F3D-2T
*BuNo 127074 - [[Empire State Aerosciences Museum]] (ESAM) near [[Schenectady]], New York.<ref>[http://www.schenectadyspotlight.com/news/2012/may/04/final-mission-fighter-jets-photo-gallery-video/ "Final mission for fighter jets."] {{webarchive |url=https://web.archive.org/web/20140204043943/http://www.schenectadyspotlight.com/news/2012/may/04/final-mission-fighter-jets-photo-gallery-video/ |date=4 February 2014 }} ''Schenectady Spotlight,'' 5 May 2012.</ref><ref>McGeehan, Patrick.[https://www.nytimes.com/2012/04/19/nyregion/anticipating-space-shuttles-arrival-old-warplanes-ship-out.html?_r=1 "Anticipating Space Shuttle’s Arrival, Old Warplanes Ship Out."] ''The New York Times,'' 18 April 2012.</ref> This F3D was operated by [[Raytheon]] in [[Massachusetts]] for electronics tests until it was donated to the [[Intrepid Sea-Air-Space Museum]] in [[New York City]], [[New York (state)|New York]]. It was displayed at the museum from 1987 until April 2012, when it was one of three aircraft moved to the ESAM to make room for the [[Space Shuttle Enterprise]]. It is painted in the livery of U.S. Marine Night Fighter Squadron 513 (VMF(N)-513) as flown during the Korean War.<ref>[http://www.esam.org/images/F3-D%20Photo1.JPG "F3D Skyknight/127074."] ''Empire State Aerosciences Museum''. Retrieved: 16 January 2015.</ref>

==Specifications (F3D-2)==
[[File:F3D Skyknight 3-view Greg Goebel.png|right|300px|Orthographically projected diagram of the F3D-2 Skyknight]]

{{aircraft specifications
|plane or copter?=plane
|jet or prop?=jet
|ref=Standard Aircraft Characteristics F3D-2 "Skyknight"<ref name="Goebel"/><ref name="Gunstonp172"/><ref name="Dorrp153"/><ref>[https://web.archive.org/web/19980703150755/http://www.history.navy.mil:80/branches/hist-ac/f3d-2.pdf "Standard Aircraft Characteristics F3D-2 'Skyknight'."] ''Naval Historical Centre.'' Retrieved: 23 June 2007.</ref>
|crew=two
|length main=45 ft 5 in
|length alt=13.84 m
|span main=50 ft 0 in
|span alt=15.24 m
|span more=26 ft 10 in (8.18 m) folded
|height main=16 ft 1 in
|height alt=4.90 m
|area main=400 ft²
|area alt=37.16 m²
|empty weight main=18,160 lb
|empty weight alt=8,237 kg
|loaded weight main=21,374 lb
|loaded weight alt=9,694 kg
|max takeoff weight main=26,850 lb
|max takeoff weight alt=12,178 kg
|engine (jet)=[[Westinghouse J34]]-WE-38
|type of jet=[[turbojet]]s
|number of jets=2
|thrust main=3,600 lbf 
|thrust alt=1633 kg
|max speed main=491 knots
|max speed alt= 565 mph, 909 km/h
|max speed more=at 20,000 ft (6,096 m)
|cruise speed main=454 mph
|cruise speed alt=394 kn, 731 km/h
|stall speed main=93 mph
|stall speed alt=81 kn, 150 km/h
|stall speed more (with approach power)
|range main=1,200 mi/1,000 nmi, 1,931 km internal 
|range alt=1,374 mi/1,193 nmi, 2,211 km with 2 × 150 gal/568 l tanks
|ceiling main=38,200 ft
|ceiling alt=11,643 m
|loading main=53.4 lb/ft²
|loading alt=260.9 kg/m²
|thrust/weight=0.34
|climb rate main=2,970 ft/min
|climb rate alt=15.1 m/s
|guns=4 × 20 mm [[Hispano-Suiza HS.404|Hispano-Suiza M2]] [[autocannon|cannon]], 200 rpg
|rockets=2 × 11.75 in (29.8 cm) [[Tiny Tim (rocket)]]
|missiles=4× [[AIM-7 Sparrow|Sparrow I]] [[air-to-air missile]]s (F3D-2M)
|bombs=2 × 2,000 lb (900 kg) bombs
}}

==See also==
{{aircontent
|see also=
|related=
*[[Douglas F6D Missileer]]
|similar aircraft=
*[[Curtiss-Wright XF-87 Blackhawk]]
*[[de Havilland Sea Venom]]
*[[de Havilland Sea Vixen]]
*[[Lockheed F-94 Starfire]]
*[[Northrop F-89 Scorpion]]
|lists=
*[[List of fighter aircraft]]
*[[List of military aircraft of the United States]]
*[[List of military aircraft of the United States (naval)]]
}}

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Andrade, John M. ''U.S. Military Aircraft Designations and Serials since 1909''. Earl Shilton, Leicester, UK: Midland Counties Publications, 1979, ISBN 0-904597-22-9
* Badrocke, Mike. "Electronic Warrior". ''[[Air Enthusiast]]'', Fifty-one, August to October 1993, pp.&nbsp;41–48. Stamford, UK: Key Publishing. ISSN 0143-5450
* Donald, David, ed. ''The Encyclopedia of World Aircraft''. London: Aerospace Publishing, 1997. ISBN 1-85605-375-X
* Dorr, Robert F. and Warren Thompson. ''Korean Air War''. St. Paul, Minnesota: Motorbooks International, 1994.ISBN 0-879-38862-5
* Francillon, René. ''McDonnell Douglas Aircraft Since 1920''. London: Putnam, 1979. ISBN 0-370-00050-1
* Gunston, Bill, ed. ''The Illustrated History of Fighters''. New York, New York: Exeter Books Division of Simon & Schuster, 1981. ISBN 0-89673-103-0
* Grossnick, Roy A. and William J. Armstrong.  ''United States Naval , Aviation, 1910–1995''. Annapolis, Maryland: Naval Historical Center, 1997. ISBN 0-16-049124-X
* Hardy, Michael John. ''Sea, Sky and Stars: An Illustrated History of Grumman Aircraft''. London: Arms & Armour Press, 1987. ISBN 978-0853688327
* Heinemann, Edward H. and Rosario Rausa. ''Ed Heinemann: Combat Aircraft Designer''. Annapolis, Maryland: Naval Institute Press, 1980. ISBN 0-87021-797-6
* ''The Illustrated Encyclopedia of Aircraft'' (Part Work 1982–1985). London: Orbis Publishing, 1985
* Jones, Lloyd. ''U.S. Fighters: Army-Air Force 1925 to 1980s''. Fallbrook, California: Aero Publishers, 1975. ISBN 0-8168-9200-8
* Jones, Lloyd. ''U.S. Naval Fighters: 1922 to 1980s''. Fallbrook, California: Aero Publishers, 1977. ISBN 0-8168-9254-7
* Sullivan, Jim. ''F9F Panther/Cougar in action''. Carrollton, [[Texas|TX]]: Squadron/Signal Publications, 1982. ISBN 0-89747-127-X
* Swanborough, Gordon and Peter M. Bowers. ''United States Navy Aircraft since 1911''. London: Putnam, Second edition, 1976. ISBN 0370-10054-9
{{Refend}}

==External links==
{{Commons category|Douglas F3D Skyknight}}
*[https://web.archive.org/web/20080511222616/http://www.boeing.com/history/mdc/skyknight.htm Boeing "McDonnell Douglas History, Skyknight, F3D (F-10)"]
*[http://www.globalsecurity.org/military/systems/aircraft/f-10.htm GlobalSecurity "F3D (F-10) Skyknight"]
*[http://www.combatairmuseum.org/aircraft/douglasf3skyknight.html "US Navy BuNo 125807 on display" at Combat Air Museum]

{{Douglas aircraft}}
{{USN fighters}}
{{US fighters}}

[[Category:Douglas aircraft|F3D Skyknight]]
[[Category:United States fighter aircraft 1940–1949|Douglas FD3 Skyknight]]
[[Category:Twinjets]]
[[Category:Monoplanes]]
[[Category:Carrier-based aircraft]]