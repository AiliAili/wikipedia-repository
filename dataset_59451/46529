{{Infobox person
| image       = Bruce Bialosky.jpg
| alt         = 
| caption     = 
| name        = Bruce L. Bialosky
| birth_date  = {{Birth date and age|1953|10|07|df=yes}}
| birth_place = Detroit, Michigan
| education = [[San Diego State University]] (B.S.)
| residence = [[Studio City, California]]
| nationality = [[United States|American]]
| occupation = [[Certified Public Accountant|CPA]], [[Real Estate Broker]], [[Columnist]]
| spouse = [[Teri Michaels Bialosky]]
| children =  Samuel and Hannah
}}

'''Bruce Lawrence Bialosky''', CPA (born October 7, 1953) is a political columnist who founded the Republican Jewish Coalition of California and was appointed by President [[George W. Bush]] to the U.S. Holocaust Memorial Council.  He currently writes a weekly column on Townhall.com, and is a frequent writer for other publications as well as often being a guest on talk radio. Bialosky writes often on financial issues.

==Early Life and Education ==

Bialosky was born in [[Detroit, Michigan]].  At the age of five his family moved back to the birthplace of his parents, [[Cleveland, Ohio]].  There he grew up in [[Shaker Heights, Ohio]], with his two older brothers – Robert and David.  In 1969, he moved to [[California]] with his now-divorced mother.  After high school, Bialosky put himself through college—first graduating from [[Los Angeles Valley College]] and then attending [[San Diego State University]] where he graduated in 1976 [[summa cum laude]] with a Bachelor of Science degree in accounting.  During his senior year at SDSU, Bialosky served as President of the School of Business.

== Professional career ==

Bialosky started his career at [[KPMG|Peat Marwick, Mitchell & Co.]] in 1976.  He obtained his [[Certified Public Accountant|CPA]] license in 1978.  At the end of 1979, he moved to [[Reno, Nevada]], to become [[Chief Financial Officer]] for his brothers’ auto parts chain – Parts World.  The chain was sold thereafter and Bialosky stayed on to run the chain for the new owners along with one of his brothers.  In 1983, he returned to [[Los Angeles]] where he established a CPA practice emphasizing real estate and taxation.  In 1987, he obtained a real estate broker’s license.  He has maintained his practice in both areas since that time.

== Political career ==

Upon arrival in [[Reno, Nevada]], in 1980 Bialosky involved himself in the election of [[Ronald Reagan]] for President.  He worked with [[Patty Cafferata]] (later State Treasurer of Nevada and Gubernatorial candidate in 1986) and her mother, [[Barbara Vucanovich]] (who was Senator [[Paul Laxalt]]’s chief of staff and later the first member of Congress from Northern Nevada).  In 1982 with their encouragement, Bialosky ran for state assembly in Reno, but lost in the primary.  He then managed Cafferata’s successful campaign for State Treasurer.

Bialosky reignited his political career in the 1990s as Treasurer of Valley VOTE, the movement to have the San Fernando Valley to separate from the City of Los Angeles.  In 2000, Bialosky worked extensively on the Presidential campaign of [[George W. Bush]].  After his election, Bialosky formed a chapter of the Republican Jewish Coalition in Los Angeles.  Based on the success of the chapter, he started chapters throughout the state of California.  He became the national treasurer of the Republican Jewish Coalition.  In 2004, Bialosky was appointed California Statewide Chair for Jewish Outreach for the Bush Reelection Campaign.<ref>http://www.gwu.edu/~action/2004/bush/bushorgca.html</ref> In October, 2004, Bialosky was nominated by President Bush to the U.S. Holocaust Council and formally appointed in April, 2005.<ref>http://www.life.com/news-pictures/52754203/chertoff-speaks-at-holocaust-memorial-museum</ref><ref>{{cite web|url=http://www.rjchq.org/Newsroom/newsdetail.aspx?id%3Dd8bf5f8d-f602-443b-b649-5b23e2bcc5a4%26type%3D |title=Archived copy |accessdate=2012-01-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20120506041702/http://www.rjchq.org/Newsroom/newsdetail.aspx?id=d8bf5f8d-f602-443b-b649-5b23e2bcc5a4&type= |archivedate=2012-05-06 |df= }}</ref><ref>{{cite web|url=http://www.ushmm.org/museum/press/archives/detail.php?category%3D12-council%26content%3D2005-04-25 |title=Archived copy |accessdate=2012-01-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20120119122820/http://www.ushmm.org/museum/press/archives/detail.php?category=12-council&content=2005-04-25 |archivedate=2012-01-19 |df= }}</ref> He served four years on the Council.

== Writing and other media Career ==

Bialosky began his writing career while in Reno as a frequent contributor to the [[Reno Gazette-Journal]].  In the 1990s, Bialosky started writing for the [[Los Angeles Daily News]].  He also had columns published by the [[Los Angeles Times]] and the [[Houston Chronicle]].  Based on the notoriety he obtained from his writing and involvement in the political scene, he became a regular on local television issue shows and talk radio shows such as the [[Al Rantel]] radio show ([[KABC (AM)|KABC]] in Los Angeles).  In 2004, Bialosky started a blog that became successful on a national basis.  He maintained the blog until he was appointed to the U.S. Holocaust Memorial Council when, upon request by the Administration, he suspended his blog.

Upon completion of his Presidential appointment in 2008, Bialosky started his weekly column on [[Townhall.com]].<ref>http://townhall.com/columnists/brucebialosky/</ref>  He is also a frequent columnist on Flashreport.org, the premiere California publication for Republican-leaning columnists.<ref>{{cite web|url=http://www.flashreport.org/featured-columns-library0b.php?faID%3D2011120708553918 |title=Archived copy |accessdate=2012-01-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20120107135917/http://www.flashreport.org/featured-columns-library0b.php?faID=2011120708553918 |archivedate=2012-01-07 |df= }}</ref>  Bialosky now appears frequently as a guest on radio shows throughout the country.<ref>http://www.texasconservativereview.com/vol10n18.html</ref><ref>http://marthamontelongomp3.blogspot.com/2010/07/to-stop-duplicating-my-efforts-i.html</ref><ref>{{YouTube|WT5sb4PiI3A}}</ref><ref>http://stores.dennisprager.com/PROD/04142004.html</ref>

== References ==
{{reflist}}

{{DEFAULTSORT:Bialosky, Bruce}}
[[Category:1953 births]]
[[Category:Living people]]
[[Category:People from Studio City, Los Angeles]]