{{refimprove|date=February 2013}}
{{Use dmy dates|date=February 2017}}
{{Use British English|date=May 2013}}
[[File:Denge acoustic mirrors-10July2005 (2).jpg|thumb|The acoustic mirrors at Denge. Left to right, the 200 foot, 20 foot and 30 foot mirrors.]]
'''Denge''' is a former [[Royal Air Force]] site near [[Dungeness (headland)|Dungeness]], in [[Kent]], England. It is best known for the early experimental [[acoustic mirror]]s which remain there.

The acoustic mirrors, known colloquially as 'listening ears', at Denge are located between [[Greatstone-on-Sea]] and [[Lydd]] airfield, on the banks of a now disused gravel pit. The mirrors were built in the late 1920s and early 1930s as an experimental early warning system for incoming aircraft, developed by Dr [[William Sansome Tucker]]. Several were built along the south and east coasts, but the complex at Denge is the best preserved, and are protected as scheduled monuments.<ref>{{NHLE|num= 1005119 |desc=|accessdate=20 February 2017}}</ref>

==Denge complex==
There are three acoustic mirrors in the complex, each consisting of a single [[concrete]] hemispherical reflector.<ref>{{cite web
 | url = http://www.doramusic.com/soundmirrors.htm
 | title = Sound Mirrors on the South Coast
 | accessdate = 12 December 2008 | date = 3 March 2002}}</ref><ref>{{cite web
 | url = http://www.andrewgrantham.co.uk/soundmirrors/locations/denge/
 | title = Denge sound mirrors
 | accessdate = 8 March 2009}}</ref>

*The '''200 foot mirror''' is a near vertical, curved wall, 200 feet (60m) long. It is one of only two similar acoustic mirrors in the world, the other being in [[Magħtab]], [[Malta]].

[[File:200 ft Acoustic mirror at Denge.jpg|thumb|200 ft Acoustic mirror at Denge]]

*The '''30 foot mirror''' is a circular dish, similar to a deeply curved satellite dish, 9&nbsp;m (30&nbsp;ft) across, supported on concrete buttresses. This mirror still retains the metal microphone pole at its centre.
*The '''20 foot mirror''' is similar to the 30 foot mirror, with a smaller, shallower dish 6&nbsp;m (20&nbsp;ft) across. The design is close to that of an acoustic mirror in [[Kilnsea]], [[East Riding of Yorkshire]].

[[File:ListeningEars(PaulRusson)Mar2005.jpg|right|thumb|View of all three mirrors]]
Acoustic mirrors did work, and could effectively be used to detect slow moving enemy aircraft before they came into sight. They worked by concentrating sound waves towards a central point, where a microphone would have been located. However, their use was limited as aircraft became faster. Operators also found it difficult to distinguish between aircraft and seagoing vessels. In any case, they quickly became obsolete due to the invention of [[radar]] in 1932. The experiment was abandoned, and the mirrors left to decay. The gravel extraction works caused some undermining of at least one of the structures.

[[File:Denge acoustic mirrors -bridge -water-10July2005.jpg|right|thumb|The mirrors with the swing bridge visible in the foreground]]
The striking forms of the sound mirrors have attracted artists and photographers. British artist [[Tacita Dean]] created a film inspired by the complex. The band [[Turin Brakes]] featured the mirrors on some of their album covers. The object appeared in the [[music video]] for [[Blank & Jones]]' "A Forest".The mirrors have also been featured in the music videos for ''Invaders Must Die by The Prodigy'' & ''Young Kato - Something Real''.

==Restoration==
In 2003, [[English Heritage]] secured £500,000 from the Aggregates Levy Sustainability Fund and from the EU's Interreg programme under the Historic Fortifications Network, as administered by [[Kent County Council]].<ref>{{cite web
 | url = http://www.culture24.org.uk/science+%26+nature/technology/art17649
 | title = Britain's Concrete Ears To Be Saved By English Heritage
 | accessdate = 12 December 2008 | date = 29 July 2003}}</ref> This money was spent to restore the damage caused by the gravel works, as well as to install a swing bridge which now is the only means of access, reducing the monument's exposure to vandalism. The mirrors are situated on an island within an [[Royal Society for the Protection of Birds|RSPB]] nature reserve, and can only be accessed on open days as the designated site (which has both [[Site of Special Scientific Interest]] and [[Special Protection Area]] status) is sensitive to disturbance. 

==References==
{{Reflist}}

==External links==
*[http://www.theromneymarsh.net/history/mirrors.htm Greatstone Sound Mirrors]
*[http://www.rmcp.co.uk/cgi-bin/db_public.cgi?page=walking Guided Tours by the Romney Marsh Countryside Project]
{{Commons category|Denge acoustic mirrors}}
{{coord|50|57|22|N|0|57|14|E|scale:5000_region:GB|display=title}}

{{Shepway}}

[[Category:Acoustics]]
[[Category:Warning systems]]
[[Category:History of Kent]]