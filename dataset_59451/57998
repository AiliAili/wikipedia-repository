{{Infobox Journal
| title=Journal of Clinical Epidemiology
| cover= [[File:Journal of Clinical Epidemiology cover.gif]]
| discipline=[[Medicine]]
| abbreviation=J. Clin. Epidemiol.
| publisher=[[Elsevier]]
| country=
|frequency=monthly<ref>[http://www.ncbi.nlm.nih.gov/nlmcatalog/8801383] Journal of Clinical Epidemiology at NLM Database</ref>
| history=
| openaccess=
| website=http://www.jclinepi.com/home
| ISSN=0895-4356
}}

The '''''Journal of Clinical Epidemiology''''' is a peer reviewed journal of [[Epidemiology]] that promotes the quality of clinical and patient-oriented health services research through the advancement and application of innovative methods of :

*Conducting and presenting primary research;
*Synthesizing research results;
*Disseminating results; and
*Translating results into optimal clinical practice;
*With special attention to the training of new generations of scientists and clinical practice leaders.<ref>http://www.jclinepi.com/content/aims</ref>

The Journal, published by [[Elsevier]], was previously published as the '''Journal of Chronic Diseases'''. The journal was first published in 1955 as a follow-up to [[Harry S. Truman]]'s 1951 Presidential Task Force on national health concerns and the subsequently written Magnuson Report. This journal aimed to discuss chronic diseases and how they affected all ages, rather than merely the elderly. Furthermore, consistent with the [[epidemiological transition]] model, the landscape of the United States' healthcare woes transformed from primarily infectious disease mortality to mortality induced by chronic diseases.

Under the editorial leadership of [[Alvan Feinstein]] and [[Walter O. Spitzer]], the title of the journal was changed to the '''''Journal of Clinical Epidemiology''''' with the January 1988 issue.<ref>{{cite journal | url = http://www.sciencedirect.com/science/article/pii/0895435688900029 | doi=10.1016/0895-4356(88)90002-9 | volume=41 | title=The journal of clinical epidemiology: same wine, new label for the journal of chronic diseases | journal=Journal of Clinical Epidemiology | pages=1–7}}</ref> The current editors are André Knottnerus (Professor of General Practice, Netherlands School of Primary Care Research, President of the Scientific Council for Government Policy, Maastricht, The Netherlands) and Peter Tugwell (Professor, Department of Medicine, Department of Epidemiology and Community Medicine; Canada Research Chair, Institute of Population Health, University of Ottawa, Ottawa, ON, Canada).<ref>http://www.jclinepi.com/content/edboard</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[Impact Factor]] of 5.478, ranking it 6th out of 160 journals in the category "Public, Environmental & Occupational Health" and 1st out of 85 journals in the category "Health Care Sciences & Services".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Public, Environmental & Occupational Health |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

==External links==
*[http://www.jclinepi.com/home Main Site]

[[Category:Epidemiology journals]]
[[Category:Elsevier academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1955]]


{{med-journal-stub}}