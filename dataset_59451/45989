{{Use dmy dates|date=July 2015}}
{{Use British English|date=July 2015}}
{{Infobox hospital
| Name       = Aberdeen Community Health and Care Village
| Org/Group  = [[NHS Grampian]]
| Image      = <!-- optional --> 
| Caption    = <!-- optional --> 
| Logo       = [[File:Nhsgrampianlogo.png|150px]]
| Location   = [[Aberdeen]]
| Region     = [[Grampian]]
| State      = Scotland
| Country    = UK
| HealthCare = NHS
| Type       = [[Community hospital]]
| Speciality = <!-- if devoted to a speciality, i.e. not if has broad spectrum of specialities -->
| Standards  = <!-- optional if no national standards -->
| Emergency  = No
| Affiliation= [[University of Aberdeen]]<br>[[Robert Gordon University]]<br>[[Queen Margaret University]]
| Beds       = N/A
| Founded    = 2013
| Closed     = <!-- optional -->
| Website    = [http://www.nhsgrampian.co.uk/nhsgrampian/ariroofgarden.jsp?pContentID=6960&p_applic=CCC&pMenuID=3&p_service=Content.show& Aberdeen Community Health and Care Village- NHS Grampian]
| Wiki-Links =
|}}

'''Aberdeen Community Health and Care Village''' also known as '''The Health Village''' is an urban [[community hospital]] located in the centre of [[Aberdeen]], [[Scotland]].<ref>{{cite web|url=http://www.nhsgrampian.co.uk/nhsgrampian/ariroofgarden.jsp?pContentID=6960&p_applic=CCC&pMenuID=3&p_service=Content.show& |title=Aberdeen Community Health and Care Village|publisher=[[NHS Grampian]] |accessdate= 25 May 2014}}</ref>

The facility is used to provide a wide range of diagnostic and treatment services in a way that can support people to retain a degree of independence.<ref name=Hub>{{cite web |url=http://www.hubnorthscotland.co.uk/Projects/View/60003 |title=Aberdeen Community Health Care Village |publisher= Hub North Scotland |accessdate= 25 May 2014}}</ref> This approach allows people to remain in their own communities rather than using inpatient beds.<ref name=Miller>{{cite web |url=http://www.millerconstruction.co.uk/markets/health/community-mental-health/aberdeen-community-health-care-village.aspx |title=Aberdeen Community Health & Care Village |publisher=[[Miller Group (construction company)]] |accessdate= 25 May 2014}}</ref>

==History==
The health village is the first healthcare facility to be completed in Scotland under the hub framework of the [[Scottish Futures Trust]].<ref name=launch>{{cite web|url=http://www.nhsgrampian.co.uk/nhsgrampian/ariroofgarden.jsp?pContentID=8832&p_applic=CCC&p_service=Content.show& |title=First Minister opens state-of-the-art healthcare village |publisher=[[NHS Grampian]] |date=3 December 2013 |accessdate= 25 May 2014}}</ref><ref name=fundingfuture>{{cite web|url=http://www.bbc.co.uk/news/uk-scotland-27297838 |title=Funding the future: New wave of NHS 'big build' projects |publisher=[[BBC News]] |date=7 May 2014 |accessdate=30 May 2014}}</ref>

Plans for the complex were announced in 2010.<ref>{{cite news|url=http://www.scotsman.com/news/163-29m-plans-for-health-and-care-village-in-city-centre-1-786827|title=£29m plans for 'health and care village' in city centre |work=[[The Scotsman]] |date=18 January 2010 |accessdate=11 July 2014}}</ref> It cost more than £15 million to construct and has brought state-of-the-art healthcare facilities to the area.<ref name=fundfacilities>{{cite news |url=https://www.theguardian.com/society/2014/jan/22/scotland-health-ditch-pfi-nhs |title=Scotland's prescription for health: ditch the PFI |work=[[The Guardian]] |date= 22 January 2014 |accessdate= 25 May 2014}}</ref>

==Current use==
The health village is expected to offer services to 700 patients a day, once all departments are operating at full capacity.<ref name=fundfacilities/> The complex contains 275 rooms- including 3 dental suites, 2 minor procedure rooms, and 29 consulting rooms.<ref name="newScotsman">{{cite news|url=http://www.scotsman.com/news/health/first-minister-opens-aberdeen-health-village-1-3217283 |title=First Minister opens Aberdeen health village| work=The Scotsman |date=2 December 2013 |accessdate=25 May 2014}}</ref>

Up to 500 members of staff will be delivering services from the health village with 150-200 members of staff expected to be in the building at any one time.<ref name=launch/>

Services being run from the health village include: [[cardiac rehabilitation]], [[dentistry]], [[dietetics]], [[physiotherapy]], [[podiatry]], [[radiology]], [[sexual health]], [[speech and language therapy]] and minor surgery. An [[endoscopy]] service is planned to be located in the Health Village from June 2014.<ref name=newsletter>{{cite web|url=http://www.nhsgrampian.co.uk/nhsgrampian/files/09%20-%20Health%20Village%20Newsletter%20-%20April%202014.pdf|title=Aberdeen Community Health and Care Village Newsletter April 2014 |publisher=[[NHS Grampian]] |accessdate=25 May 2014}}</ref>

The health village will act as an educational resource for patients, carers and community-based staff.<ref name="newExpress">{{cite news|url=http://www.eveningexpress.co.uk/news/local/first-minister-opens-new-15-million-health-village-in-aberdeen-1.160999 |title=First Minister opens new £15 million health village in Aberdeen |publisher=[[Evening Express (Scotland)]] |date=3 December 2013 |accessdate=25 May 2014}}</ref> The health village will also provide improved access to community based learning opportunities for healthcare professionals such as [[general practitioner]]s (GPs) and GP trainees.<ref name="GPTraining">{{cite web |url=http://scotlandgptraining.blogspot.co.uk/2013/12/aberdeen-community-health-village.html |title=Aberdeen Community Health Village |publisher=North of Scotland GP Training |date=11 January 2014 |accessdate=25 May 2014}}</ref>

==References==
{{Reflist|33em}}

==External links==
*[http://www.nhsgrampian.org/nhsgrampian/files/HV_Public_Booklet.pdf Aberdeen Community Health and Care Village] Official information for patients, carers, visitors and members of the public

{{portal|Social Welfare and Social Work}}
{{Hospitals in NHS Grampian}}
{{Coord|57.150243|-2.091621|display=title|region:GB_type:landmark}}

[[Category:Hospital buildings completed in 2013]]
[[Category:Hospitals in Aberdeen]]
[[Category:NHS Grampian]]
[[Category:NHS Scotland hospitals]]