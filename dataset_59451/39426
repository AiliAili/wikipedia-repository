{{Infobox military person
|name=Hugh John Casey
|birth_date= {{birth date|1898|6|7|df=yes}}
|death_date= {{death date and age|1981|8|30|1898|6|7|df=yes}}
|birth_place=[[Brooklyn, New York]]
|death_place=[[White River Junction, Vermont]]
|placeofburial=[[Arlington National Cemetery]]
|placeofburial_label= Place of burial
|image=Hugh J Casey.jpg
|alt=Head and shoulders of a man in uniform with a neat moustache
|caption=Brigadier General Hugh J. Casey in 1943
|nickname=Pat
|allegiance= {{flag|United States of America}}
|branch={{army|United States}}
|serviceyears=1918–1949
|rank=[[File:US-O8 insignia.svg|30px]] [[Major General (United States)|Major General]]
|servicenumber=0-9298
|commands=
|unit=
|battles=[[World War I]]:
*[[Occupation of the Rhineland]]
[[World War II]]:
*[[Battle of the Philippines (1941–1942)|Philippines Campaign (1941–42)]]
*[[New Guinea Campaign]]
*[[Philippines Campaign (1944–45)]]
*[[Occupation of Japan]]
|awards=[[Distinguished Service Cross (United States)|Distinguished Service Cross]]<br/>[[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] (2)<br/>[[Silver Star]]<br/>[[Legion of Merit]]<br/>[[Bronze Star Medal|Bronze Star]]<br/>[[Distinguished Service Star]] (Philippines) (2)<br/>Honorary [[Commander of the Order of the British Empire]] (Australia)<br/>Commander, [[Order of Orange-Nassau]] (Netherlands)<br/>Officer, [[Légion d'honneur]] (France)
|relations=[[Hugh Boyd Casey]] (son)<br/>[[Frank Butner Clay]] (son in law)
|laterwork=Chairman, [[New York City Transit Authority]]
}}
'''Hugh John "Pat" Casey''' (24 July 1898 – 30 August 1981) was a [[Major General (United States)|major general]] in the [[United States Army]]. A 1918 graduate of the [[U.S. Military Academy]] at [[West Point]], Casey served in Germany during the [[Occupation of the Rhineland]]. He later returned to Germany to attend the [[Berlin Institute of Technology|''Technische Hochschule'' in Berlin]], earning a [[Doctor of Engineering]] degree.

As an engineer, Casey prepared a voluminous report on flood control for the [[Pittsburgh]] District. He was involved with the design and construction of the Deadman Island Lock and Dam on the [[Ohio River]], and was chief of the Engineering Division at the [[Passamaquoddy Bay#The Passamaquoddy Tidal Power Project/"Quoddy Dam" Project|Passamaquoddy Tidal Power Project]], a [[New Deal]] public works project. He went to the [[Philippines]] in 1937 to advise the government there on [[hydropower]] and flood control. In the early part of [[World War II]],  he became involved with the enormous wartime construction program. Perhaps his most notable and lasting achievement was his involvement with the design of [[The Pentagon]], the largest office building in the world.

Casey served as [[General of the Army]] [[Douglas MacArthur]]'s [[engineering|chief engineer]] during the [[Battle of Bataan]], in the jungles and mountains of [[New Guinea Campaign|New Guinea]] and the [[Philippines Campaign (1944–45)|Philippines]], and during the [[occupation of Japan]]. In the [[Battle of Leyte]], he commanded the Army Support Command (ASCOM), which was responsible for all construction and logistics activities in the forward area. He hoped to become [[Chief of Engineers]], but [[President of the United States|President]] [[Harry S. Truman]] passed him over. Later, Casey worked for [[Schenley Industries]] from 1951 until his retirement in 1965, and was chairman of the [[New York City Transit Authority]] from 1953 to 1955.

==Early life==
Hugh John Casey was born in [[Brooklyn]], [[New York City|New York]] on 7 June 1898, the son of John J. Casey, a plumbing and heating contractor, and Margaret L. Casey.<ref>{{harvnb|Casey|1993|pp=xiv, 4}}</ref> John's grandparents were immigrants from [[Ireland]] and [[England]]. His grandfather served on the [[Union (American Civil War)|Union]] side in the [[American Civil War]] and was killed in the [[Battle of Shiloh]]. Margaret’s parents were Irish immigrants who settled in [[Pennsylvania]].<ref name="Casey, p. 5">{{harvnb|Casey|1993|p=5}}</ref>

Hugh Casey was educated at Manual Training High School from 1910 to 1914, graduating at the age of 15. He won a New York State scholarship and entered [[Brooklyn Polytechnic Institute]], where he studied [[civil engineering]]. After a year there, he took a competitive examination for the [[U.S. Military Academy]] at [[West Point]] held by [[Congressman]] [[Daniel J. Griffin]], the chairman of the [[House Committee on Military Affairs]], ranking first out of 62 applicants for the appointment.<ref>{{harvnb|Casey|1993|p=3}}</ref> To enter, Casey claimed to be slightly older, adopting his brother's 7 June birthday.<ref name="Casey, p. 5"/>

Casey entered West Point in 1915, where his best friend and roommate was [[Lucius D. Clay]]. At West Point, Casey played [[American football|football]] as a [[Halfback (American football)|halfback]], substituting for [[Elmer Oliphant]]. One of Casey's duties was tutoring Oliphant in mathematics. Casey decided that winning games was more important than playing, and he helped keep Oliphant proficient at math. Unlike most appointees to West Point, a grateful Casey wrote frequently to Griffin about his progress and sent him football tickets. When Casey's younger brother Martin Charles Casey wanted to enter West Point, Griffin directly appointed him to the class of 1920 without having to pass the examination. Martin served with the [[coastal artillery]] for eleven years before being medically discharged due to [[migraine]] headaches on 30 November 1931. Martin later became a successful lawyer.<ref>{{harvnb|Casey|1993|pp=5–7}}</ref><ref>{{harvnb|Cullum|1940|p=477}}</ref> Both brothers acquired the nickname "Pat" at West Point.<ref>{{harvnb|Casey|1993|p=98}}</ref>

==World War I==
Because of the United States' entry into [[World War I]], Casey's class graduated early on 12 June 1918. Casey was ranked third in the class and was commissioned as a [[Captain (US Army)|captain]] in the [[United States Army Corps of Engineers]].<ref name="Casey, p. xiii"/> He was stationed at [[Camp A. A. Humphreys]], [[Virginia]], first as an instructor and then starting in September 1918 as a company commander with the 219th Engineers, part of the [[19th Division (United States)|19th Division]]. The 219th Engineers moved to [[Camp Dodge]], Iowa in November 1918. Casey returned to the Engineer School at Camp Humphreys as a student in September 1919.<ref name="Casey, p. xi"/>

He served with the US [[Occupation of the Rhineland|Occupation forces in the Rhineland]] from June 1920 to May 1922.<ref name="Casey, p. xi">{{harvnb|Casey|1993|p=xi}}</ref> While there, Casey improved on his high school German to become  fluent enough in the language to write his doctoral thesis in the language.<ref>{{harvnb|Casey|1993|p=70}}</ref>  He also married Dorothy Ruth Miller, the daughter of Colonel R. B. Miller, the chief surgeon of the American forces stationed in [[Koblenz]], on 22 May 1922. On their honeymoon they traveled through south Germany, Austria, and Switzerland. The couple had three children: two sons, [[Hugh Boyd Casey|Hugh Boyd]] and Keith Miles, and a daughter, Patricia.<ref>{{harvnb|Casey|1993|pp=xiv, 71}}</ref>

==Between the wars==
From 1922 to 1926, Casey was the officer in charge of the Engineer Unit of the [[Reserve Officers Training Corps]] (ROTC) at the [[University of Kansas]] in [[Lawrence, Kansas]],<ref name="Casey, p. xi"/> reverting to his substantive rank of [[first lieutenant]] on 27 November 1922.<ref>{{harvnb|Cullum|1930|p=1289}}</ref> He again returned to Camp Humpreys in 1926 to attend the Company Officers Course.<ref name="Casey, p. xiv">{{harvnb|Casey|1993|p=xiv}}</ref> In 1927, Casey received his first civil works assignment, as assistant District Engineer at the [[Pittsburgh]] District. Casey took over the task of preparing a voluminous report on flood control. The Corps of Engineers was criticized by the Pittsburgh Flood Control Commission for over-engineering, in planning for a "flood that had never happened and never would happen", and the report was shelved. However, [[Pittsburgh Flood of 1936|in 1936 the flood did happen]]. The report was then dusted off and its recommendations were adopted. The [[Flood Control Act of 1936]] assigned responsibility for flood control to the Corps of Engineers and other Federal agencies.<ref>{{harvnb|Casey|1993|pp=76–79}}</ref> Casey was also responsible for construction at Deadman Island Lock and Dam (now called the [[List of locks and dams of the Ohio River|Dashields Lock and Dam]]) on the [[Ohio River]].<ref>{{harvnb|Casey|1929|pp=444–451}}</ref>

In September 1929 Casey was assigned to the Rivers and Harbors Section of the Office of the Chief of Engineers in [[Washington, DC]]. This job involved reviewing the project studies, plans and specifications of all river and harbor projects throughout the United States, including flood control and [[hydroelectric power]] projects. He also had responsibility for correspondence with [[U.S. senators]] and congressmen.<ref>{{harvnb|Casey|1993|p=23}}</ref> During this time he co-designed and patented the Kingman-Casey Floating Mooring Bit for navigation locks.<ref name="Casey, p. xiv"/><ref>US Patent no. 1,997,586, dated 16 April 1935</ref> He was promoted to the substantive rank of captain on 1 May 1933.<ref name="Casey, p. xiii">{{harvnb|Casey|1993|p=xiii}}</ref>

Casey won a John R. Freeman fellowship from the [[American Society of Mechanical Engineers]] in 1933 to study [[hydraulics]] and civil engineering in Germany. For the next two years, he attended the [[Berlin Institute of Technology|''Technische Hochschule'' in Berlin]], earning a [[Doctor of Engineering|Doctorate in Engineering]]. His thesis, written in German, was on ''Geschieb Bewegung'', the bedload movement in streams.<ref>{{cite web |title=''Über Geschiebebewegung'' |url=http://www.google.com/search?tbs=bks:1&tbo=p&q=+bibliogroup:%22Mitteilungen+der+Preu%C3%9Fischen+Versuchsanstalt+f%C3%BCr+Wasserbau+und+Schiffbau%22&source=gbs_metadata_r&cad=6 |publisher=''Preussische Druckerei-und Verlags-Aktiengesellschaft'' |accessdate=22 June 2010 |language=German }}</ref><ref>{{harvnb|Casey|1993|pp=99–100}}</ref> Returning to the United States in June 1935, Casey was posted to [[Eastport, Maine]] as chief of the Engineering Division at the [[Passamaquoddy Bay#The Passamaquoddy Tidal Power Project/"Quoddy Dam" Project|Passamaquoddy Tidal Power Project]], a [[New Deal]] public works project. There, he established a concrete testing laboratory under Charles E. Wuerpel which is now part of the Structures Laboratory at the [[Waterways Experiment Station]] at [[Vicksburg, Mississippi]]. Due to political forces, the project came to nothing and was allowed to die.<ref>{{harvnb|Casey|1993|pp=105–112}}</ref> After the Passamaquoddy project fell through, Casey served with the [[Boston]] Engineer District on flood control surveys of the [[Connecticut River]] Valley.<ref>{{harvnb|Casey|1993|pp=112–115}}</ref>

Along with Lucius Clay, Casey was sent to the [[Philippines]] in 1937 to advise the government there on [[hydropower]] and flood control. They worked with [[Meralco]] and other power companies in the Philippines, and conducted a series of surveys, including a detailed one of the [[Agno River]]. After Clay returned to the United States, Casey developed plans for the [[Caliraya Dam]], a  {{convert|40000|hp|kW}} hydroelectric project with an estimated cost of $5 million. Along with [[Lieutenant Colonel (United States)|Lieutenant Colonel]] [[Dwight D. Eisenhower]], the chief of staff to [[Major General (United States)|Major General]] [[Douglas MacArthur]], the [[Office of the Military Advisor to the Commonwealth Government of the Philippines|Military Advisor to the Commonwealth Government of the Philippines]], and Mr. Rodriquez of the [[National Power Corporation]], Casey presented the project to [[President of the Philippines|President]] [[Manuel Quezon]], who approved it.<ref>{{harvnb|Casey|1993|pp=117–120}}</ref> After over twenty years as a captain, Casey was promoted to [[major (United States)|major]] on 1 February 1940.<ref name="Casey, p. xiii"/>

==World War II==

===Construction Division===
[[File:Pentagon construction.jpg|thumb|right|320px|Northwest exposure of the Pentagon's construction underway, 1 July 1942|alt=Expansive view of a construction site with lots of parked cars, scaffolding and cranes. There are a number of demountables in the foreground.]]
Casey returned to [[Washington, D.C.]] in October 1940 to become chief of the Design and Engineering Section in the Construction Division of the [[Quartermaster general (USA)|Office of the Quartermaster General]], under [[Brigadier General (United States)|Brigadier General]] [[Brehon B. Somervell]].<ref>{{harvnb|Fine|Remington|1972|p=265}}</ref> An enormous construction program was underway to meet the needs of [[World War II]]. Working with a staff that included [[George Bergstrom]], a former president of the [[American Institute of Architects]],<ref>{{harvnb|Fine|Remington|1972|p=266}}</ref> Casey set about revising the standard designs for barracks. A number of new features were added to improve comfort, safety, and durability. Substitutions were made for scarce materials. It was discovered that the standard 63-man barracks was now too small. Of the 81 companies in the new [[triangular division]], 51 fitted more easily into 74-man barracks. By slightly increasing the barracks size, substantial savings were made by reducing the overall number of buildings that needed to be constructed, the size of the [[cantonment]] areas required, and the length of required roads and utility lines.<ref>{{harvnb|Fine|Remington|1972|pp=347–354}}</ref> Casey was promoted to [[lieutenant colonel (United States)|lieutenant colonel]] on 8 April 1941.<ref name="Casey, p. xiii"/>

On the afternoon of Thursday, 17 July 1941, Somervell summoned Casey and Bergstrom and gave them a new special project: the design of an enormous office complex to house the [[United States Department of War|War Department's]] 40,000-person staff together in one building. Somervell gave them until 09:00 on Monday morning to design the building, which he envisaged as a modern, four-story structure with no elevators, on the site of the old [[Hoover Field|Washington Hoover Airport]].  This would ultimately become [[The Pentagon]], the largest office building in the world.  Over that "very busy weekend", Casey, Bergstrom and their staff roughed out the design for a four-story, five-sided structure with a floor area of {{convert|5100000|sqft|sqm}}—twice that of the [[Empire State Building]].<ref>{{harvnb|Fine|Remington|1972|p=431 }}</ref> The estimated cost was $35 million.<ref>{{harvnb|Fine|Remington|1972|p=432}}</ref> [[President of the United States|President]] [[Franklin D. Roosevelt|Roosevelt]] subsequently moved the site of the building, over Somervell's objections, away from [[Arlington National Cemetery]].<ref>{{harvnb|Fine|Remington|1972|p=434}}</ref>

===Southwest Pacific===
In September 1941, General [[Douglas MacArthur]] requested Casey's services as his chief engineer. Casey arrived in [[Manila]] in October, shortly before the outbreak of war between the United States and [[Japan]]. He acquired construction equipment from the National Power Corporation that was being used on the Caliraya project.<ref>{{harvnb|Casey|1993|pp=141–145}}</ref> Casey supervised demolitions as MacArthur's troops retreated to [[Bataan]], for which he was awarded the [[Distinguished Service Cross (United States)|Distinguished Service Cross]].<ref>{{harvnb|Casey|1993|pp=ix}}</ref> Unlike the rest of MacArthur's headquarters, Casey, who was promoted to [[colonel (United States)|colonel]] on 19 December 1941 and [[brigadier general (United States)|brigadier general]] on 25 January 1942,<ref name="Casey, p. xiii"/> did not relocate to [[Corregidor]] but remained on Bataan with a small staff of five officers.<ref>{{harvnb|Dod|1966|pp=85–87}}</ref> However, he joined MacArthur and sixteen other members of his staff in their [[Douglas MacArthur's escape from the Philippines|escape from Corregidor]] by PT boat in March 1942.<ref>{{harvnb|Dod|1966|p=99}}</ref> For his service in the 1942 campaign in the Philippines, he was awarded the [[Army Distinguished Service Medal]].<ref name="medals">{{cite web |url=http://militarytimes.com/citations-medals-awards/recipient.php?recipientid=6048 |title=Valor Awards for Hugh John Casey |accessdate=10 December 2012 |publisher=Military Times}}</ref>

[[File:Hugh J. Casey.jpg|thumb|320px|left|Major General Hugh J. Casey at his desk in Brisbane.|alt=A name plaque reads: "Maj. Gen. Hugh J. Casey". The wall behind him is covered in huge map of New Guinea.]]
In [[Australia]], Casey became Chief Engineer at MacArthur's General Headquarters (GHQ), [[Southwest Pacific Area]] (SWPA). He faced enormous engineering challenges. Most of [[New Guinea]] consisted of mountains and jungle, with very few airstrips, ports or roads. All of these had to be developed to support operations. To provide additional expertise in construction, Casey had [[Leif Sverdrup]] assigned to his staff as chief of the Construction Section, with the rank of colonel. As U.S. Army engineers were few, Casey worked closely with his [[Australian Army]] counterpart at [[General (Australia)|General]] Sir [[Thomas Blamey]]'s Allied Land Forces headquarters, [[Major General (Australia)|Major General]] [[Clive Steele]]. Construction activities in Australia were also undertaken by civilians of the [[Allied Works Council]]. Casey attempted to coordinate the activities of the various agencies. He had to fend off attempts by the [[U.S. Army Air Forces]] to gain control of his aviation engineer battalions. The [[Royal Australian Air Force]] organized its own airbase construction squadrons and only with difficulty was Casey able to control their activities.<ref>{{harvnb|Dod|1966|pp=130–137}}</ref>

Casey's initial need was for engineer units to accomplish the daunting construction program, but soon stocks of engineer supplies and equipment began to run low. This was exacerbated by incoming units arriving without their equipment, or with it stowed on numerous ships, which often arrived at various ports in a theater where ports were hundred or thousands of miles apart. Critical shortages developed of [[tractor]]s, [[grader]]s, [[concrete mixer]]s and [[welding]] equipment. In the absence of a proper stock control system, an overall coordinating agency, and adequate numbers of engineer depot units, the allocation and distribution of the meager supplies on hand were difficult tasks. The worst problem was spare parts. Equipment was operated around the clock under harsh conditions and soon wore out or broke. A large proportion of equipment became unserviceable for lack of spare parts. Requisitions sent to the United States took months to arrive, so recourse was made to the limited sources of supply in Australia.
<ref>{{harvnb|Dod|1966|pp=260–}}</ref>

In September 1942, MacArthur decided to outflank Japanese troops on the [[Kokoda Trail]] by sending an American [[regimental combat team]] over the [[Owen Stanley Range]]. Two alternate means of crossing the mountains seemed possible. One, the [[Kapa Kapa Trail]] was known to climb to elevations above {{convert|9000|ft|m}} and present formidable obstacles. Casey and Sverdrup took charge of investigating the Abau Trail. They reached Abau on 18 September. Casey explored the harbor, taking depth soundings from a native canoe. Sverdrup set out for Jaure with a party of one American, two Australians from the [[Australian New Guinea Administrative Unit]], ten native police from the [[Royal Papua New Guinea Constabulary|Royal Papuan Constabulary]] and 26 native carriers. After eight days on the trail, scaling heights of {{convert|5000|ft|m}}, Sverdrup concluded that it would not be practical for troops to traverse the route and turned back. Meanwhile, Casey had concluded that the harbor was too shallow even for [[lighter (barge)|lighters]]. However, the trip was not a total loss, for Sverdrup had sighted a plateau north of the Owen Stanley Range suitable for airstrips, allowing troops to be flown across the Owen Stanley Range.<ref>{{harvnb|Dod|1966|pp=178–180}}</ref> Casey was awarded the [[Silver Star]].<ref name="medals"/>

In New Guinea, logistics and construction activities were coordinated by task force engineer staffs. These were often hastily assembled and had not always been able to meet the demands imposed by base development in such a challenging theater of operations. The scale of operations in the Philippines was much greater, so for this purpose the Army Service Command (ASCOM) was formed in Brisbane on 23 July 1944. Casey was appointed to command ASCOM. In his absence, Sverdrup became MacArthur's chief engineer. Although part of [[USASOS]], ASCOM operated under the control of [[Sixth United States Army|Sixth Army]], moving as far forward as combat operations allowed, developed new bases, and operated them until [[USASOS]] was ready to take over, at which point the units under ASCOM simply reverted to USASOS, allowing a seamless transfer of command.<ref>{{harvnb|Dod|1966|p=571}}</ref> For the [[Battle of Leyte]] Casey's ASCOM had 43,000 men, of whom 21,000 were engineers.<ref>{{harvnb|Dod|1966|p=575}}</ref>

Casey and some members of his staff came ashore on A-Day; the advance echelon of his ASCOM headquarters arrived two days later. Work began immediately on the airfield at [[Tacloban City|Tacloban]], and commenced on airfields in central Leyte soon after they were captured.<ref>{{harvnb|Dod|1966|pp=579–581}}</ref> Heavy seasonal rains thwarted attempts to develop the airbases in central Leyte and it was decided to abandon their development and construct a new airbase on the coast at a site occupied by Sixth Army headquarters. The need to get aircraft based on Leyte to stop the Japanese from reinforcing the island was so pressing that Lieutenant General [[Walter Krueger]] agreed to move his headquarters.<ref>{{harvnb|Dod|1966|pp=583–584}}</ref>

Casey had intended to come ashore on the first day of the [[Invasion of Lingayen Gulf|landing at Lingayen Gulf]] in January 1945 but was delayed a day because the destroyer he was traveling on had to escort a crippled transport. Despite enormous difficulties ASCOM was able to finish numerous projects on time and some ahead of schedule. On 13 February 1945, ASCOM was transferred to USASOS and redesignated the Luzon Base Section (LUBSEC). Casey then resumed his old post, now renamed Chief Engineer, US Army Forces Pacific.<ref>{{harvnb|Dod|1966|pp=598–601}}</ref> For his services as commander of ASCOM, he was awarded the [[Legion of Merit]]. He was subsequently awarded a bronze [[oak leaf cluster]] to his Distinguished Service Medal for his services as Chief Engineer, US Army Forces Pacific.<ref name="medals"/>

Casey hoped to become [[Chief of Engineers]] when Lieutenant General [[Raymond A. Wheeler]] retired in 1948, but President [[Harry S. Truman]] passed him over in favor of the Missouri River Division Engineer, Major General [[Lewis A. Pick]]. Instead, Casey remained in Japan as MacArthur's Chief Engineer until Casey's retirement on 31 December 1949.<ref>{{harvnb|Casey|1993|pp=270–271}}</ref> He edited ''Engineers of the Southwest Pacific'', a seven-volume series about their wartime service.<ref name="Casey, p. x"/> He received a number of foreign awards for his service, including the [[Distinguished Service Star]] from the Philippines, the [[Commander of the Order of the British Empire]] from Australia, the Commander of the [[Order of Orange-Nassau]] from the Netherlands, and the Officer of [[Légion d'honneur]] from France.<ref>{{harvnb|Casey|1993|pp=xi–xv}}</ref>

==Later life==
Casey was Chairman of the [[New York City Transit Authority]] from 1953 to 1955, and served in various positions with [[Schenley Industries]] from 1951 until his retirement in 1965.<ref name="Arlington"/> He was a member of a number of professional societies, and civic organizations.<ref>{{harvnb|Casey|1993|p=xv}}</ref> He died of a heart attack on 30 August 1981 at the Veterans Administration Hospital at [[White River Junction, Vermont]], survived by his wife Dorothy and his son Keith. His other son, Hugh, had been killed in an air crash during the [[Korean War]]. Father and son were buried adjacent to each other in [[Arlington National Cemetery]].<ref name="Arlington">{{citation |url=http://www.arlingtoncemetery.net/hjcasey.htm |accessdate=4 April 2010 |title=Hugh John Casey, Major General, United States Army}}</ref> His daughter Patricia, who married Major General [[Frank Butner Clay]], the son of Lucius Clay, had died on 1 January 1973, and is also buried in Arlington National Cemetery.<ref>{{citation |url=http://www.arlingtoncemetery.net/fbclay.htm |accessdate=4 April 2010 |title=Frank Butner Clay, Major General, United States Army}}</ref> In August 1982,  a new building at the Humphreys Engineer Center at Fort Belvoir was  dedicated in his honor by Dorothy and the Chief of Engineers, Lieutenant General [[Joseph K. Bratton]].<ref name="Casey, p. x">{{harvnb|Casey|1993|p=x}}</ref>

<!--
==Decorations==

Here is the ribbon bar of Major general Hugh John Casey:

<center>
{|
|-
|colspan="4" align="center"|{{Ribbon devices|number=0|type=oak|ribbon=Distinguished Service Cross ribbon.svg|width=106}} {{Ribbon devices|number=1|type=oak|ribbon=Distinguished Service Medal ribbon.svg|width=106}}
|-
|{{Ribbon devices|number=0|type=oak|ribbon=Silver Star ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Legion of Merit ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Bronze Star ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=award-star|ribbon=World War I Victory Medal ribbon.svg|width=106}}
|-
|{{Ribbon devices|number=0|type=award-star|ribbon=Army of Occupation of Germany ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=award-star|ribbon=American Defense Service ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=award-star|ribbon=American Campaign Medal ribbon.svg|width=106}}
|{{Ribbon devices|number=8|type=service-star|ribbon=Asiatic-Pacific Campaign ribbon.svg|width=106}}
|-
|{{Ribbon devices|number=0|type=service-star|ribbon=World War II Victory Medal ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Army of Occupation ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Order of the British Empire (Military) Ribbon.png|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Legion Honneur Officier ribbon.svg|width=106}}
|-
|{{Ribbon devices|number=0|type=oak|ribbon=NLD Order of Orange-Nassau - Commander BAR.png|width=106}}
|{{Ribbon devices|number=1|type=award-star|ribbon=PHL Distinguished Service Star BAR.png|width=106}}
|{{Ribbon devices|number=1|type=award-star|ribbon=Philippine Defense ribbon.png|width=106}}
|{{Ribbon devices|number=2|type=award-star|ribbon=Phliber rib.png|width=106}}
|-
|} </center>
{| class="wikitable"
|-
!1st Row
|colspan="9" align="center"|[[Distinguished Service Cross (United States)|Distinguished Service Cross]]
|colspan="9" align="center"|[[Distinguished Service Medal (U.S. Army)|Army Distinguished Service Medal]] w/ [[Oak Leaf Cluster|OLC]]
|-
!2nd Row
|colspan="4" align="center"|[[Silver Star]]
|colspan="4" align="center"|[[Legion of Merit]]
|colspan="4" align="center"|[[Bronze Star Medal]]
|colspan="4" align="center"|[[World War I Victory Medal (United States)|World War I Victory Medal]]
|-
!3rd Row
|colspan="4" align="center"|[[Army of Occupation of Germany Medal]]
|colspan="4" align="center"|[[American Defense Service Medal]]
|colspan="4" align="center"|[[American Campaign Medal]]
|colspan="4" align="center"|[[Asiatic-Pacific Campaign Medal]] w/ one silver and three bronze service stars
|-
!4th Row
|colspan="4" align="center"|[[World War II Victory Medal (United States)|World War II Victory Medal]]
|colspan="4" align="center"|[[Army of Occupation Medal]]
|colspan="4" align="center"|[[Order of the British Empire|Commander of the Order of the British Empire]] ([[Australia]])
|colspan="4" align="center"|[[Legion of Honour|Officer of the Legion of Honour]]
|-
!5th Row
|colspan="4" align="center"|[[Order of Orange-Nassau|Commander of the Order of Orange-Nassau]]
|colspan="4" align="center"|[[Distinguished Service Star|Philippines Distinguished Service Star]] w/ Bronze Star
|colspan="4" align="center"|[[Philippine Defense Medal|Philippine Defense Medal]] w/ Bronze Star
|colspan="4" align="center"|[[Philippine Liberation Medal]] w/ two Bronze Stars
|-
|}
-->

==Notes==
{{Reflist|colwidth=30em}}

==References==
{{Portal|Biography|United States Army}}
{{Refbegin}}
*{{cite book
  | title = Biographical Register of the Officers and Graduates of the US Military Academy at West Point New York Since Its Establishment in 1802: Supplement Volume VII 1920–1930
  | last = Cullum
  | first = George W.
  | author-link = George Washington Cullum
  | publisher = R. R. Donnelly and Sons, The Lakeside Press
  | location = Chicago
  | year = 1930
  | url=http://digital-library.usma.edu/cdm/compoundobject/collection/p16919coll3/id/24660/rec/8
  | accessdate=6 October 2015
  | ref = harv
}}
*{{cite book
  | title = Biographical Register of the Officers and Graduates of the US Military Academy at West Point New York Since Its Establishment in 1802: Supplement Volume VIII 1930–1940
  | last = Cullum
  | first = George W.
  | author-link = George Washington Cullum
  | publisher = R. R. Donnelly and Sons, The Lakeside Press
  | location = Chicago
  | year = 1940
  | url=http://digital-library.usma.edu/cdm/compoundobject/collection/p16919coll3/id/19424/rec/9
  | accessdate=6 October 2015
  | ref = harv
}}
*{{citation
  | editor-last = Casey
  | editor-first = Hugh J.
  | editor-link = 
  | title = Deadman Island Lock and Dam, Ohio River
  | journal = The Military Engineer
  | volume = Vol. XXI
  | issue = No. 119
  | pages = 444–451
  | location = [[Washington, D.C.]]
  | publisher = [[U.S. Army Corps of Engineers]]
  | year = 1929
}}
*{{citation
  | editor-last = Casey
  | editor-first = Hugh J.
  | editor-link = 
  | title = Major General Hugh J. Casey
  | series = Engineer Memoirs
  | location = [[Washington, D.C.]]
  | publisher = [[United States Department of the Army]]
  | year = 1993
}}
*{{Citation
  | last = Dod
  | first =  Karl
  | title = The Corps of Engineers: The War Against Japan
  | publisher = [[United States Department of the Army]]
  | location = [[Washington, D.C.]]
  | url = http://www.scribd.com/doc/48271033/Corps-of-Engineers-the-War-Against-Japan
  | year = 1966
}}
*{{citation
  | last = Fine
  | first = Lenore
  | last2 = Remington
  | first2 = Jesse A.
  | title = The Corps of Engineers: Construction in the United States
  | publisher = [[United States Army Center of Military History]]
  | location = Washington, D.C. 
  | year = 1972
}}
{{Refend}}

{{Good article}}

{{Authority control}}

{{DEFAULTSORT:Casey, Hugh John}}
[[Category:1898 births]]
[[Category:1981 deaths]]
[[Category:People from Brooklyn]]
[[Category:American people of Irish descent]]
[[Category:American military engineers]]
[[Category:American military personnel of World War II]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:Polytechnic Institute of New York University alumni]]
[[Category:Technical University of Berlin alumni]]
[[Category:United States Army generals]]
[[Category:United States Military Academy alumni]]
[[Category:United States Distinguished Marksman]]
[[Category:Recipients of the Distinguished Service Cross (United States)]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Distinguished Service Star]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Silver Star]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Officiers of the Légion d'honneur]]
[[Category:Honorary Commanders of the Order of the British Empire]]
[[Category:Commanders of the Order of Orange-Nassau]]
[[Category:American expatriates in Germany]]
[[Category:American people of English descent]]