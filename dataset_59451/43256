<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name =Military Aircraft HM-1
  |image =Military Aircraft HM-1.jpg
  |caption =Pilot Leigh Wade and the Military Aircraft HM-1 at the 1938 Thompson Trophy race
}}{{Infobox Aircraft Type
  |type = [[Air racing|Racing aircraft]]
  |manufacturer =[[Miller Aviation Corporation]]
  |designer =Howell W. "Pete" Miller
  |first flight =  October 18, [[1936 in aviation|1936]] (earlier Hawks Miller HM-1)
  |introduced =
  |retired =
  |status =
  |primary user =
  |more users =
  |produced =
  |number built =1
  |unit cost =
  |variants with their own articles = 
  |developed from =[[Hawks Miller HM-1]]}}
|}

The '''Military Aircraft HM-1''', derived from the earlier [[Hawks Miller HM-1]] [[Air racing|racing aircraft]] nicknamed, "Time Flies" was an [[United States|American]] [[prototype]] attack/observation aircraft. The HM-1 did not achieve production after the sole example was destroyed during testing.
{{TOC limit|limit=2}}

==Design and development==
In 1936, Hawks had approached Howell W. "Pete" Miller, chief engineer for the [[Granville Brothers Aircraft|Granville Brothers]] and responsible for their famous [[Granville Brothers|Gee Bee]] racers, to create a racing aircraft from his own design, the Hawks Miller HM-1. With an advanced aircraft design that still relied heavily on wood construction, the HM-1 featured innovative design elements, including the unusual feature of "burying" the cockpit with a curved windshield contoured to fit the [[fuselage]] top, creating a very streamlined shape. The cockpit was extended for takeoff and landing, but retracted in flight, with the pilot's seat lowered and the windshield becoming flush with the fuselage. 
[[File:Frank Hawks and Time Flies.jpg|thumb|Frank Hawks and "Time Flies", c. 1936]]
After its first flight on October 18, 1936, Hawks flew "Time Flies", from [[Hartford, Connecticut]] to [[Miami, Florida]] on April 13, 1937, in 4 hours and 55 minutes.<ref name= "Matthews p, 98"/> He then flew to [[Newark Liberty International Airport|Newark Airport]], New Jersey, in 4 hours and 21 minutes, but bounced on landing at Newark, and broke a wooden spar in the right wing with other spars also damaged.<ref>Kinert 1969, pp. 84–85.</ref>

Short of funds, Hawks decided not to rebuild the aircraft and sold the rights to the design, including engineering data to Tri-American Aviation, a concern that wanted to convert the design into a fast two-seat attack/observation aircraft.<ref name="Boyne p. 12">Boyne 1978, p. 12.</ref> The aircraft was redesigned to include two machine guns in the wings and another machine gun mounted in a flexible mount in the new rear cockpit.{{#tag:ref|Only dummy armament fittings were ever carried.<ref name="Ortman"/>|group=N}}

==Operational history==
The principals of Tri-American Aviation, Leigh Wade and Edward Connerton, engaged Miller to rebuild the aircraft in 1938 as a two-seater with a more conventional greenhouse canopy added, looking a great deal like Miller's earlier [[Gee Bee Q.E.D.]] design. The aircraft was first renamed the Miller HM-2, but when company was reorganized as the Miller Aircraft Co., it was called the MAC-1 and Military Aircraft HM-1, although often described in the press as the "Hawks Military Racer", although Hawks was no longer actively involved.<ref name= "Matthews p, 98">Matthews 2001, p. 98.</ref>

With the intention to demonstrate the aircraft's potential, pilot Leigh Wade entered the MAC-1/HM-1 in the 1938 [[Thompson Trophy]] race.<ref>Kinert 1952, p. 111.</ref>  In essentially military configuration with dummy machine guns fitted, Wade flew the aircraft to a fourth-place finish.<ref name="Boyne p. 12"/>{{#tag:ref|The prototype was not fully sorted out when entered, and although capable of a high turn of speed, the fuel consumption on the Twin Wasp was prodigious. Wade had to throttle back in order to even finish the race, precluding any chance of showing the true potential of the aircraft, which had demonstrated a {{convert|90|mph|km/h|abbr=on}} mph advantage over the current [[United States Army Air Corps]] (USAAC) [[Seversky P-35]] fighter aircraft.<ref>Kinert 1969, p. 85.</ref> |group=N}}

Despite the showing in the Thompson race, the U.S. military considered the predominantly wood construction in the design as unsuitable. Air racer and test pilot Earl Ortman was hired to fly the HM-1 at East Hartford, Connecticut where a {{convert|25|miles|km}} course was laid out to display flight capabilities for foreign military interests, and seek out military contracts.{{#tag:ref|Tri-American had interests in South America, the likely place to sell the HM-1.<ref name="Boyne p. 12"/> |group=N}}

On August 23, 1938, Ortman flew above Rentschler Field, adjacent to the Pratt & Whitney Aircraft factory where their employees and Hamilton Standard technicians were available. He made four passes over the course in the HM-1, achieving an average speed of {{convert|369|mph|km/h|abbr=on}}. The next phase of the testing called for determination of climb rates. From 1,000&nbsp;ft (304.93 m), Ortman climbed to 10,000&nbsp;ft (3,048 m), then to dive down to 1,000&nbsp;ft (304.93 m) and start up on another climb.<ref name="Ortman"/>

On his final dive, at a reported {{convert|425|mph|km/h|abbr=on}}, the fuel in tanks was being transferred when the stresses placed on the wings were too great and a wing sheared off.<ref name="Ortman">[http://goldenageofaviation.org/ortman.htm "Earl Ortman."] ''Golden Age of aviation.'' Retrieved: October 15, 2012.</ref> Ortman was able to bail out safely, but the aircraft was demolished and the project was abandoned.<ref>Boyne 1978, p. 16.</ref>{{#tag:ref|On August 23, 1938, Frank Hawks had contacted Miller for an update on the HM-1 project but on the same day, had later died in an air crash when flying his latest project, the revolutionary [[Gwinn Aircar]].</blockquote><ref>[http://www.time.com/time/magazine/article/0,9171,760095,00.html "Transport: Hawks End."] ''Time'', September 5, 1938.</ref>|group=N}}

== Specifications (Military Aircraft HM-1) ==
{{Aircraft specs
|ref=<!-- reference -->
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=7.16
|length ft=
|length in=
|length note=
|span m=9.14
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=48.77
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=1,840
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Pratt & Whitney R-1830 Twin Wasp]] 
|eng1 type=14-cyl. two-row air-cooled radial piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=1,150
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=375
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following 
specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Boyne, Walt. "Built for Speed: Pt. II of the Howell Miller Legend." ''Wings,'' Vol. 8, No. 2, April 78.
* Cowin, Hugh W. ''The Risk Takers, A Unique Pictorial Record 1908-1972: Racing & Record-setting Aircraft'' (Aviation Pioneer 2). London: Osprey Aviation, 1999. ISBN 1-85532-904-2.
* Daniels, C.M. "Speed: The Story of Frank Hawks." ''Air Classics'', Vol. 6, No. 2, December 1969.
* "Frank Hawks Obituary." ''[[Lima News]]'', [[Lima, Ohio]], August 24, 1938.
* Fraser, Chelsea Curtis. ''Famous American Flyers'' (Flight, Its First Seventy-five Years). Manchester, New Hampshire: Ayer Company Publishers Inc., 1979. ISBN 978-0-405-12165-4.
* Hull, Robert. ''September Champions: The Story of America's Air Racing Pioneers''. Harrisburg, Pennsylvania: Stackpole Books, 1979. ISBN 0-8117-1519-1.
* Kinert, Reed. ''American Racing Planes and Historic Air Races''. New York: Wilcox and Follett Company, 1952.
* Kinert, Reed. ''Racing Planes and Air Races: A Complete History, Vol. 1 1909-1923''. Fallbrook, California: Aero Publishers, Inc., 1969.
* Lewis, Peter. "Hawks HM-1 'Time Flies'." ''Air Pictorial'', Volume 3, No. 11, November 1973.
* Matthews, Birch. ''Race With The Wind: How Air Racing Advanced Aviation''. St. Paul, Minnesota: Motorbooks, 2001. ISBN 978-0-7603-0729-8.
* Musciano, Walter A. "Frank Hawks: The Story of the Legendary Speed Flying King." ''Aviation History'', November 2005.
{{Refend}}

==External links==
* [http://www.jitterbuzz.com/manfil/MAN_Feb_1937.jpg Model Aircraft magazine cover, May 1937]
* [http://air-boyne.com/the-gee-bee-story-hottest-racers-of-the-time/ The GEE BEE Story: Hottest Racers of The Time, Posted on September 30, 2011 by Col. Walter J. Boyne USAF (Ret) at The Surly Bonds of Earth]
* [http://www.aerofiles.com/_h.html Hawks on Aerofiles]
* [http://www.aerofiles.com/_mi.html Miller on Aerofiles]
* [http://www.airrace.com/1938NAR.htm 1938 National Air Races]

[[Category:Miller aircraft]]
[[Category:United States sport aircraft 1930–1939]]
[[Category:United States attack aircraft 1930–1939]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Racing aircraft]]