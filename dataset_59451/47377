[[Alhaji]] '''Abdurrahman Shugaba Darman''' (1920–2010) was a Nigerian politician from [[Borno State]], and a contemporary of the late [[Sir]] [[Ahmadu Bello]]. Shugaba Darman was a founding member of the [[Great Nigeria People's Party]] and was elected as a member of the Borno State House of Assembly in 1979 where he also became the house [[Majority Leader]].<ref name="bare_url">{{cite web
 |url=http://allafrica.com/stories/201004230493.html
 |publisher=www.allafrica.com
 |title=Nigeria: Shugaba Darman Dies At 80
 |author=Isa Umar Gusau
 |accessdate=2012-01-01}}</ref>

==Deportation==
On January 24, 1980, Immigration officers arrested Alhaji Darman on the strength of a deportation order signed by the Federal Minister of internal affairs Alhaji [[Maitama Bello Yusuf|Bello Maitama]]. The deportation order entitled "Shugaba Abdurrahman Darman's Deportation Order 1980", stated amongst other things that "...Shugaba Abdurrahman Darman at present in Nigeria ought to be classified as a prohibited immigrant" and also that "Shugaba Abdurrahman be deported from [[Nigeria]] by the first available means...." Shugaba Darman was promptly deported to a village in [[Chad]].<ref name="bare_url" /> In response to the public outcry against the obviously politically motivated deportation, the government instituted a one man tribunal of inquiry presided over by Justice P.C Okanbo. The NPN government and President [[Shehu Shagari]] in particular were quite concerned about the negative press that the issue was generating and also about allusions in the press to the partiality of the tribunal.<ref name=Williams>{{Cite book
|title =President and power in Nigeria: the life of Shehu Shagari
|first = David
|last = Williams
|year = 1982
|isbn = 0-7146-3182-5
}}</ref>

Shugaba Darman's troubles had allegedly started as a result of him being regarded as a threat by the ruling party of the time, the [[National Party of Nigeria]] (NPN). Shugaba Darman was a charismatic politician who attracted large crowds at political rallies, the crowds were drawn to his speeches in which he criticized the ruling NPN government. The government claimed that Shugaba's father was a Chadian hence he was from Chad. During the subsequent court case filed by the GNPP led legal team of Chief D. O.A Oguntoye, to challenge the deportation order, the government brought a Chadian woman who claimed that Shugaba was her biological son whom she wanted back; this while weeping profusely. Shugaba denied knowing the woman and claimed that his mother was alive and well known in [[Maiduguri]] even though her sight was now poor.<ref name="bare_url" />

The Maiduguri high court ruled in Shugaba's favour in the case of "Shugaba Darman vs Federal Minister of Internal Affairs and Others", revoked the deportation order and awarded damages to the tune of 350,000 Naira to Shugaba. The government appealed the verdict at the [[Appellate Court|appeal court]] in [[Kaduna]] and lost. The case was then taken to the Supreme Court and that court yet again ruled in Shugaba's favour in a unanimous judgement by the four justices led by Justice Coker. The case was overtaken by the subsequent military coup and no compensation was paid; Shugaba said he had forgiven the Shagari led government for his deportation.<ref>{{cite web
 |url=http://nationalmirroronline.net/features/10735.html
 |publisher=National Mirror
 |title=The legal tussle over Shugaba's deportation
 |author=Francis Famoroti
 |accessdate=2012-01-01}}</ref>

==Legacy==
Alhaji Shugaba Darman died on Wednesday April 20, 2010 at the age of 80, he left behind three wives and 29 children. Alhaji Shugaba was a founding member of the [[Shehu Musa Yar'Adua]] led [[Social Democratic Party (Nigeria)|Social Democratic Party]] (SDP), he was also a founding member of the [[People's Democratic Party (Nigeria)|People's Democratic Party]] (PDP) in which he was active as a party elder till he died.<ref name="bare_url" />

Perhaps his greatest legacy has been his legal battle with the federal government challenging his deportation; the case has been used as relevant case law in numerous human rights trials in Nigeria ever since. The judgement in itself was ground-breaking and has set a precedent for subsequent cases of human rights violation against the government.

==References==
{{reflist}}

{{DEFAULTSORT:Darman, Abdurrahman Shugaba}}

[[Category:Nigerian politicians]]
[[Category:1920 births]]
[[Category:2010 deaths]]