{{good article}}
{{Use dmy dates|date=April 2014}}
{{Infobox Grand Prix race report
| Type = F1
| Country = Brazil
| Grand Prix = Brazilian
| Details ref =<ref name="formal">{{cite web|title=Formula 1 Grande Prêmio Petrobras do Brasil 2012 |url=http://www.formula1.com/races/in_detail/brazil_883/ |publisher=Formula One |accessdate=15 October 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121017113142/http://www.formula1.com/races/in_detail/brazil_883/ |archivedate=17 October 2012 }}</ref>
| Image = Autódromo José Carlos Pace (AKA Interlagos) track map.svg
| Caption =
| Date = 25 November
| Year = 2012
| Official name = Formula 1 Grande Prêmio Petrobras do Brasil 2012
| Race_No = 20
| Season_No = 20
| Location = [[Autódromo José Carlos Pace]], São Paulo, Brazil
| Course = Permanent racing facility
| Course_mi = 2.677
| Course_km = 4.309
| Distance_laps = 71
| Distance_mi = 190.067
| Distance_km = 305.909
| Weather = Race start: drops of rain, light rain shower getting heavier at the end.<ref name=weather>{{cite news|title=FORMULA 1 GRANDE PRÊMIO PETROBRAS DO BRASIL 2012 (Race) |url=http://www.formula1.com/results/season/2012/883/7155/live_timing_popup.html# |work=formula1.com |publisher=[[Formula One Group|Formula One Administration]] |date=25 November 2012 |accessdate=22 February 2013 }}{{cbignore}}{{dead link|date=January 2016}}</ref>
Air Temp {{convert|19|C}}<ref name=weather/>
<br />Track Temp {{convert|21|C}}<ref name=weather/>
| Pole_Driver = [[Lewis Hamilton]]
| Pole_Team = [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| Pole_Time = 1:12.458
| Pole_Country = Great Britain
| Fast_Driver = [[Lewis Hamilton]]
| Fast_Team = [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| Fast_Time = 1:18.069
| Fast_Lap = 38
| Fast_Country = GBR
| First_Driver = [[Jenson Button]]
| First_Team = [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| First_Country = GBR
| Second_Driver = [[Fernando Alonso]]
| Second_Team = [[Scuderia Ferrari|Ferrari]]
| Second_Country = ESP
| Third_Driver = [[Felipe Massa]]
| Third_Team = [[Scuderia Ferrari|Ferrari]]
| Third_Country = BRA
| Lapchart = {{F1Laps2012|BRA}}
|}}

The '''2012 Brazilian Grand Prix''' (formally known as the '''Formula 1 Grande Prêmio Petrobras do Brasil 2012''')<ref name="formal"/> was a [[Formula One]] [[motor race]] that took place at the [[Autódromo José Carlos Pace]] in [[São Paulo]], Brazil on 25 November 2012.<ref name="7Dec calendar">{{cite news|url=http://www.f1fanatic.co.uk/2011/12/07/united-states-grand-prix-remains-unchanged-2012-f1-calendar/|title=United States Grand Prix remains on unchanged 2012 F1 calendar|first=Keith|last=Collantine|work=F1 Fanatic|publisher=Keith Collantine|date=7 December 2011|accessdate=15 October 2012}}</ref> The race was the twentieth and final round of the [[2012 Formula One season|2012 Formula One World Championship]], and marked the forty-first running of the [[Brazilian Grand Prix]]. The race was won by [[Jenson Button]] driving for [[McLaren]].<ref>{{cite web|url=http://www.telegraph.co.uk/sport/motorsport/formulaone/9701489/Brazilian-Grand-Prix-2012-as-it-happened.html
|title=Brazilian Grand Prix 2012: as it happened|date=25 November 2011|work=Daily Telegraph UK|accessdate=26 November 2012 }}</ref><ref>{{cite web|url=https://www.theguardian.com/sport/2012/nov/25/formula-one-brazilian-grand-prix-live?intcmp=239
|title=Brazilian Grand Prix 2012: as it happened|date=25 November 2011|work=Guardian UK|accessdate=26 November 2012 }}</ref><ref>{{cite web|url=http://www.bbc.co.uk/sport/0/formula1/20481809 |title=Brazilian Grand Prix 2012: as it happened |date=25 November 2011 |work=BBC Sport |accessdate=26 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121125172545/http://www.bbc.co.uk/sport/0/formula1/20481809 |archivedate=25 November 2012 }}</ref>

It was at this race that [[Sebastian Vettel]] won the [[2012 Formula One season|2012]] [[List of Formula One World Drivers' Champions|World Drivers' Championship]], his third title in a row. [[Fernando Alonso]] finished second in both the race and the championship, and had the provisional championship lead at a late stage of the race, before Vettel moved up the order to have a three-point cushion. The race also marked the last race for [[Michael Schumacher]], after he announced his retirement for the second time.

==Report==

===Background===
[[File:Alonso 2012 brazil.jpg|thumb|left|[[Fernando Alonso]] needed to overcome a 13-point deficit to [[Sebastian Vettel]].]]
The 2012 Brazilian Grand Prix marked [[Michael Schumacher]]'s final race before his second retirement and was also the last race for Bruno Senna<ref>{{cite news|title=Michael Schumacher announces F1 retirement|url=http://www.f1fanatic.co.uk/2012/10/04/michael-schumacher-announces-f1-retirement/|first=Keith|last=Collantine|work=F1 Fanatic|publisher=Keith Collantine|date=4 October 2012|accessdate=20 November 2012}}</ref> and the [[HRT Formula 1 Team]]. HRT in their history did not score a point, and withdrew because of financial issues.<ref>{{cite web|last=Straw|first=Edd|title=HRT fails to find buyer before 2013 Formula 1 entry deadline|website=AUTOSPORT.com|url=http://www.autosport.com/news/report.php/id/104673|date=1 December 2012|accessdate=28 January 2015}}</ref>

Tyre supplier [[Pirelli]] provided teams with early prototypes of their [[2013 Formula One season|2013]] compounds during Friday practice for testing and review.<ref>{{cite news|title=Teams to trial 2013 prototype Pirelli tyres in Brazil |url=http://www.formula1.com/news/headlines/2012/11/14086.html |publisher=Formula One |date=19 November 2012 |accessdate=20 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121121161722/http://www.formula1.com/news/headlines/2012/11/14086.html |archivedate=21 November 2012 }}</ref>

Pirelli brought its silver-banded hard compound tyre as the harder "prime" tyre and the white-banded medium compound tyre as the softer "option" tyre, as opposed to the [[2011 Brazilian Grand Prix|previous year]] when they brought the medium and soft compound dry tyres.<ref>{{cite news|title=Pirelli reveal tyre choices for final three rounds |url=http://www.formula1.com/news/headlines/2012/10/13931.html |publisher=Formula One |date=16 October 2012 |accessdate=22 February 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130111030532/http://www.formula1.com/news/headlines/2012/10/13931.html |archivedate=11 January 2013 }}</ref>

In order to become world champion for the third time, [[Sebastian Vettel]] needed to defend a 13-point advantage over [[Fernando Alonso]] meaning the Spaniard needed at least third place even if the German failed to score. Conversely, a fourth place was sufficient for Vettel even if Alonso won.

===Free Practice===
During free practice on Friday and Saturday, it was [[Lewis Hamilton]] and [[McLaren]] who set the pace, closely followed by championship contender [[Sebastian Vettel|Vettel]] and his [[Red Bull Racing|Red Bull]] teammate [[Mark Webber]].<ref name="whtimes.co.uk">{{cite web|last1=Davies|first1=Alan|title=F1 2012 Brazilian Grand Prix qualifying: Hamilton battles Vettel for pole|url=http://www.whtimes.co.uk/sport_2_365/formula-1/f1_2012_brazilian_grand_prix_qualifying_hamilton_battles_vettel_for_pole_1_1707567|website=whtimes.com|accessdate=11 March 2015}}</ref> While Hamilton topped the time sheets in both Friday sessions, [[Jenson Button]] was quickest during the third session on Saturday morning.<ref>{{cite web|last1=Collantine|first1=Keith|title=McLaren remain quickest as Brazil practice ends|url=http://www.f1fanatic.co.uk/2012/11/24/2012-brazilian-grand-prix-practice-result-fp3/|website=f1fanatic.co.uk|accessdate=11 March 2015}}</ref> [[Fernando Alonso]] had a difficult first free practice, trailing Vettel by 0.465 seconds, twice as much as in previous races. However, he was able to lap faster than the German on the harder compound tyres in the second session. [[Scuderia Ferrari|Ferrari]] looked stronger during the long run trials with [[Felipe Massa]] putting in the most consistently fast lap times, 0.1 seconds ahead of Hamilton.<ref>{{cite web|last1=Benson|first1=Andrew|title=Brazilian GP: Lewis Hamilton leads Sebastian Vettel in second practice|url=http://www.bbc.com/sport/0/formula1/20470296|website=BBC.com|accessdate=11 March 2015}}</ref> With high temperatures, tyre degradation was a concern for the teams in the paddock<ref>{{cite web|last1=Vijayenthiran|first1=Viknesh|title=Formula 1 Brazilian Grand Prix Weather Forecast|url=http://www.motorauthority.com/news/1080644_formula-1-brazilian-grand-prix-weather-forecast|website=motorauthority.com|accessdate=11 March 2015}}</ref> with Hamilton commenting that "it felt almost as though I was sliding around the track with the tyres melting".<ref name="whtimes.co.uk"/>

===Qualifying===
{{Quote box
|quote  = "I'm grateful to be able to put the car on the front row and to have had my last qualifying with McLaren as a one–two."
|source = [[Lewis Hamilton]] commenting on taking pole position.<ref>{{cite web|last1=Benson|first1=Andrew|title=Brazilian GP: Lewis Hamilton on pole ahead of Jenson Button|url=http://www.bbc.com/sport/0/formula1/20478359|website=bbc.com|accessdate=12 March 2015}}</ref>
| align = right
| width = 37%
}}
After the very warm practice sessions, qualifying on Saturday was run under difficult weather conditions. Qualifying one (Q1) started on a damp track and therefore on intermediate tyres, while by Q2 the track was almost dry and slick tyres were used. In Q1 [[Romain Grosjean]] collided with the slower running [[HRT F1 Team|HRT]] of [[Pedro de la Rosa]] and lost his front wing. While he succeeded in nursing the car back into the pit lane, he was unable to change his tyres and was therefore left in a disappointing 18th position on the grid. [[Michael Schumacher]]'s last ever qualifying did not go well either. He took 14th position, almost half a second down on his teammate, admitting he had perhaps compromised his setup too much for an expected wet race.<ref>{{cite web |url=http://www.autosport.com/news/report.php/id/104539 |title=Brazilian GP: Michael Schumacher says Mercedes' wet set-up backfired |publisher=Autosport |date=24 November 2012 |accessdate=17 January 2016}}</ref> The [[McLaren]]s were fastest all weekend and easily took the front row, with [[Lewis Hamilton]] securing his last pole position with the team. Title contenders [[Sebastian Vettel|Vettel]] and [[Fernando Alonso|Alonso]] both struggled, taking 4th and 8th position on the grid respectively.<ref>{{cite news|title=Hamilton on pole as Vettel and Alonso struggle|url=http://uk.eurosport.yahoo.com/news/formula-1-hamilton-pole-vettel-alonso-struggle-155645941.html;_ylt=AhIp5KBUVjk9A39zw.g7fKVYkcAF;_ylu=X3oDMTRvOGw2YmxsBG1pdANMSVNUUyBTdG9yeSBMaXN0IFNwb3J0cyBNYWluIEFyY2hpdmUEcGtnA2U5ZWUyNzQ5LTlkOWMtMzIxYi04ZjYwLTFjODQ0ZDUxZmUwZARwb3MDOTgEc2VjA01lZGlhU3RvcnlMaXN0VGVtcAR2ZXIDYTg3ODk2NzAtMzY2OC0xMWUyLWFkN2QtYzgzY2QzMGJmM2Yy;_ylg=X3oDMTFlMTluMDVoBGludGwDZ2IEbGFuZwNlbi1nYgRwc3RhaWQDBHBzdGNhdAMEcHQDc2VjdGlvbnM-;_ylv=3|publisher=Eurosport UK|date=24 November 2012|accessdate=30 November 2012}}</ref> [[Pastor Maldonado]] was handed a 10-place grid penalty for missing the weighbridge in the second part of qualifying. He dropped from sixth to 16th, moving [[Fernando Alonso|Alonso]] one place up to seventh.<ref>{{cite news|title=Maldonado handed 10-place grid penalty|url=http://uk.eurosport.yahoo.com/news/maldonado-handed-10-place-grid-190006693.html;_ylt=AntoOoZQ2pPzozSQtCL0WapYkcAF;_ylu=X3oDMTRvcWgzM25xBG1pdANMSVNUUyBTdG9yeSBMaXN0IFNwb3J0cyBNYWluIEFyY2hpdmUEcGtnA2Y2YTk0ZTM1LTgxZGItM2FkMS1iYTcyLWVjYzZhNWMxOWZlZQRwb3MDODUEc2VjA01lZGlhU3RvcnlMaXN0VGVtcAR2ZXIDOTc0MmYxNDEtMzY3MC0xMWUyLWFiZDktNTBhYWY0Yzg2Yjhi;_ylg=X3oDMTFlMTluMDVoBGludGwDZ2IEbGFuZwNlbi1nYgRwc3RhaWQDBHBzdGNhdAMEcHQDc2VjdGlvbnM-;_ylv=3|publisher=Eurosport UK|date=24 November 2012|accessdate=30 November 2012}}</ref>

===Race===

====Pre-race====
[[File:Start 2012 brazil GP.jpg|thumb|Start of the race]]
Title contender [[Fernando Alonso|Alonso]] and [[Scuderia Ferrari|Ferrari]] were hoping for damp conditions during the race, stating: "Wet races are a bit more unpredictable so we need some kind of damp race with many things happening, because we know that in normal conditions, fighting for the championship will be very difficult".<ref>{{cite news|title=Alonso keen to just enjoy race now|url=http://uk.eurosport.yahoo.com/news/alonso-keen-just-enjoy-race-145005219.html;_ylt=AthAjnpp3H3Bbh2zqLMlGdFYkcAF;_ylu=X3oDMTRvMXNmbWk5BG1pdANMSVNUUyBTdG9yeSBMaXN0IFNwb3J0cyBNYWluIEFyY2hpdmUEcGtnAzkxZTgwMTk4LTJiOGUtMzYzYy1hZWY1LWMyZDk1ZmI5NTE2OQRwb3MDNjgEc2VjA01lZGlhU3RvcnlMaXN0VGVtcAR2ZXIDNzA2NTE1ZDEtMzcxNS0xMWUyLWI3ZmItMDkxMTg5MzdiZTA5;_ylg=X3oDMTFlMTluMDVoBGludGwDZ2IEbGFuZwNlbi1nYgRwc3RhaWQDBHBzdGNhdAMEcHQDc2VjdGlvbnM-;_ylv=3|publisher=Eurosport UK|date=25 November 2012|accessdate=30 November 2012}}</ref>
[[Felipe Massa]] discounted rumours he might try to help Alonso win the title by forcing [[Sebastian Vettel|Vettel]] into retirement, stating "I've always been an honest person and an honest driver. And that will continue to be the case. My limit is the limit of the regulations, and I will stay true to this limit".<ref>{{cite web|title=Massa upset with notion he could take Vettel out|url=http://sports.espn.go.com/espn/wire?section=rpm&id=8670581|publisher=ESPN|accessdate=12 April 2015}}</ref>

====Race report====
With only light rain just 10 minutes before the start, all teams opted to start on slick tyres. [[Sebastian Vettel|Vettel]] got away poorly, dropping to seventh position, while his rival [[Fernando Alonso|Alonso]] moved up into fifth. Vettel and [[Bruno Senna]] collided at turn 4 in a racing incident, which saw Vettel spin while Senna ended up hitting the rear of [[Sergio Pérez|Pérez]]. Vettel suffered damage to his left sidepod but was able to carry on, in 22nd position. However, the reigning world champion began to move up the order, as there was close racing for position at the front of the field as well.  [[Felipe Massa|Massa]] helped his teammate overtake [[Mark Webber]] on lap two going into the Senna-S, giving the Spaniard the crucial third position he would need to win the title. But Alonso lost the position again to the fast running [[Force India]] of [[Nico Hülkenberg]] after running wide on lap four. Maldonado crashed out on lap two, when he lost control of his car on the kerb through the inside of turn 3, and crashed into the tyre barrier on the exit of the corner.

Rain began to fall during the next few laps and the first cars made pit stops for intermediate tyres, including the two title contenders, while [[Jenson Button|Button]] and [[Nico Hülkenberg|Hülkenberg]] stayed out. This proved to be the right decision since the rain decreased shortly afterwards and  cars on intermediates were forced to pit for slick tyres once again. Hülkenberg continued his strong performance by overtaking Button for the lead on lap 19.
[[File:Hamilton 2012 brazil retire.jpg|thumb|[[Lewis Hamilton]] retires from the race on lap 54]]
With the two cars in front leading the field by almost a minute, debris on the track brought out the [[safety car]] on lap 23. At this point [[Fernando Alonso|Alonso]] and [[Sebastian Vettel|Vettel]] were running in fourth and fifth place. At the restart on lap 29, [[Kamui Kobayashi]] took fifth position from Vettel. The damage on the Red Bull car slowed Vettel down considerably more in dry conditions, and [[Felipe Massa|Massa]] helped his teammate by overtaking the German shortly after.<ref>{{cite web|last1=Collantine|first1=Keith|title=Button wins intense race as Vettel recovers to seal third championship|url=http://www.f1fanatic.co.uk/2012/11/25/2012-brazilian-grand-prix-report/|website=f1fanatic.co.uk|accessdate=11 March 2015}}</ref> Meanwhile, at the front, [[Lewis Hamilton|Hamilton]] took second from his teammate and was able to pass [[Nico Hülkenberg|Hülkenberg]] when the German half-spun. Hülkenberg pursued the [[McLaren]] but slid into him on lap 54, leaving Hamilton out of the race with damage to the left front suspension and himself with a drive-through penalty, handing [[Jenson Button|Button]] the lead.

When the rain started again, [[Sebastian Vettel|Vettel]] was one of the first to pit for intermediates, but since his radio had failed the team was not ready for him, causing a long delay. [[Scuderia Ferrari|Ferrari]] timed [[Felipe Massa|Massa's]] pit stop well, letting [[Fernando Alonso|Alonso]] take second position from him. Vettel took sixth position from [[Michael Schumacher]] which proved sufficient to retain the title even with Alonso finishing second.

The race ended after [[Paul di Resta]] crashed on the start/finish straight shortly before the end, bringing out the [[safety car]] once again until the conclusion of the race. [[Caterham F1|Caterham's]] [[Vitaly Petrov]] took 11th position which meant his team overtook [[Marussia F1|Marussia]] in the constructors' championship. Former world champion [[Kimi Räikkönen]] had an eventful race as well, almost crashing into [[Sebastian Vettel|Vettel]] after the start and later trying to use an escape road only to find it to be a dead end. He later stated the way had been open 11 years prior, having made the same manoeuver at the [[2001 Brazilian Grand Prix|2001 race]].<ref>{{cite web|title=Räikkönens kuriose Irrfahrt: "Vor elf Jahren war das Tor noch offen"|url=http://www.spiegel.de/sport/formel1/raeikkoenens-kuriose-irrfahrt-vor-elf-jahren-war-das-tor-noch-offen-a-869325.html|website=spiegel.de|publisher=Spiegel Online|accessdate=4 February 2015|language=German}}</ref> He also had a close contest with the retiring [[Michael Schumacher]], who finished seventh after recovering from an early puncture, but gave up sixth to Vettel in order to help him extend his small point advantage over [[Fernando Alonso]] in the [[2012 Formula One season|championship]].<ref>{{cite news|title=Red Bull thanks 'gracious' Schumacher for giving Vettel sixth |url=http://www.autosport.com/news/report.php/id/104626|publisher=Autosport|date=27 November 2012|accessdate=2 March 2013}}</ref>

===Post-race===
{{Quote box
 |quote  = "I think everything that could go wrong went wrong. As a matter of fact, though, I think we always kept believing. [...] I think they tried everything to make it even harder for us, not just the others but also the circumstances: as I said, with the damage on the car, losing radio, in these conditions, when communication is so crucial, stopping just a lap too early, not having the tires ready because communication wasn't there. Where do you start, really? I think you guys had your show and we had to really fight until the end."
 |source = [[Sebastian Vettel]] during the post-race press conference.<ref>{{cite web|title=Interview with 2012 F1 champion Sebastian Vettel after Brazilian Grand Prix|url=http://autoweek.com/article/formula-one/interview-2012-f1-champion-sebastian-vettel-after-brazilian-grand-prix|website=autoweek.com|accessdate=11 March 2015}}</ref>
 |align = right
 |width = 37%
}}
The race received widespread acclaim, with ''[[The Guardian]]'' calling it a "rollercoaster".<ref>{{cite web|title=Sebastian Vettel wins F1 title after rollercoaster Brazilian Grand Prix|url=https://www.theguardian.com/sport/2012/nov/25/sebastian-vettel-wins-f1-title|publisher=Guardian|accessdate=4 February 2015}}</ref> Three-time world champions [[Niki Lauda]] and [[Nelson Piquet]] stated that they had never seen a race like it.<ref>{{cite news|title=So ein Rennen habe ich noch nie erlebt|url=https://www.welt.de/sport/formel1/article111489271/So-ein-Rennen-habe-ich-noch-nie-erlebt.html|publisher=Welt Online|language=German|date=25 November 2012|accessdate=30 November 2012}}</ref><ref name="nytimes" /> German magazine ''[[Der Spiegel]]'' deemed the race "spectacular" and "historic" due to the record-breaking 147 successful overtaking manoeuvres carried out during the race.<ref>{{cite web|title=Überholmanöver-Rekord in São Paulo aufgestellt|url=http://www.spiegel.de/sport/formel1/formel-1-rennen-in-sao-paulo-ueberholmanoever-rekord-a-871005.html|website=spiegel.de|publisher=Spiegel Online|accessdate=4 February 2015|language=German}}</ref> [[Sebastian Vettel]] himself called it his "toughest race ever".<ref>{{cite web|title=Sebastian Vettel says Brazilian Grand Prix was 'toughest race ever'|url=https://www.theguardian.com/sport/2012/nov/25/sebastian-vettel-brazilian-grand-prix|publisher=Guardian|accessdate=4 February 2015}}</ref>

At age 25, [[Sebastian Vettel|Vettel]] became the youngest triple world champion of the sport, six years younger than [[Ayrton Senna]] had been in [[1991 Formula One season|1991]]. On drawing level with Senna, Vettel commented: "To win that third title here, where one of my greatest idols, Ayrton Senna, was from, it is very difficult to imagine I join him and other great names by winning three successive titles".<ref name="nytimes" /> [[Red Bull Racing|Red Bull]]'s team principal [[Christian Horner]] praised his driver, saying "Sebastian has driven better than ever this season and has fought his way back into this championship, he's never given up and you saw that in today's race".<ref>{{cite web|last1=Collantine|first1=Keith|title=Vettel clinches drivers' championship hat-trick|url=http://www.f1fanatic.co.uk/2012/11/25/vettel-claims-drivers-championship-hattrick/|website=f1fanatic.co.uk|accessdate=11 March 2015}}</ref>

The ''[[Daily Mirror]]'' lauded [[Red Bull Racing|Red Bull]] designer [[Adrian Newey]] for making the decision to change the engine mapping on Vettel's car after reviewing photos of the damage sustained in the first lap accident. Newey was quoted saying: "So we photographed it going past, saw huge crease in exhaust, which usually means it will soon crack and the bodywork catch fire. [...] So we changed the engine mapping to keep the exhaust as cool as we could, even though it meant lost performance".<ref>{{cite web|last1=Young|first1=Byron|title=Photo finish: Design genius Adrian Newey has camera lens to thank for Vettel's success|url=http://www.mirror.co.uk/sport/formula-1/brazilian-grand-prix-design-genius-1457230|website=mirror.co.uk|accessdate=11 March 2015}}</ref>

{{Quote box
|quote  = "I didn’t win the title, but I won so much respect from everybody."
|source = [[Fernando Alonso]] after the race.<ref name="nytimes">{{cite web|last1=Spurgeon|first1=Brad|title=Vettel Takes His Third Straight Formula One Title|url=https://www.nytimes.com/2012/11/26/sports/autoracing/26iht-prix26.html?_r=1|website=nytimes.com|accessdate=11 March 2015}}</ref>
 |align = right
 |width =25%
}}
Championship runner-up [[Fernando Alonso]] expressed pride for his team, going so far as to call the [[2012 Formula One season|2012 season]] "the best season of my career". During the podium interview with [[Nelson Piquet]], he asserted: "Obviously we lost the championship now, but I don't think, as you said, that we lost here in Brazil, we lost in some races where we were a little bit unlucky. But this is a sport, but when you do something with your heart, when you do something with 100 per cent, you have to be proud of your team, happy for them, and we will try next year."<ref>{{cite web|title=Brazilian GP - Sunday - Press Conference|url=http://www.grandprix.com/race/r878sunpc.html|website=grandprix.com|accessdate=11 March 2015}}</ref> The press later pointed out Alonso's "grace after the race", contrasting him to Vettel who had criticized the competition's "dirty tricks" during his post-race press conference.<ref>{{cite web|title='Vettel overcame the odds'|url=http://en.espnf1.com/f1/motorsport/story/96489.html|website=espnf1.com|accessdate=11 March 2015}}</ref>

Speaking about his victory, [[Jenson Button]] congratulated his team, saying: "This is the perfect way to end the season. We have had ups and downs and to end on a high bodes well for 2013."<ref>{{cite web|last1=Benson|first1=Andrew|title=Sebastian Vettel wins his third F1 world championship for Red Bull|url=http://www.bbc.com/sport/0/formula1/20477032|website=bbc.com|accessdate=11 March 2015}}</ref>

Upon his second retirement from the sport, Michael Schumacher commented on the three years at [[Mercedes GP]]: "I have tried that mission to end successful. It didn't work this time but I'm quite happy to finish from here and go for a different life again."<ref>{{cite web|last1=Cary|first1=Tom|title=Michael Schumacher prepares for a second Formula One retirement before riding off into the sunset|url=http://www.telegraph.co.uk/sport/motorsport/formulaone/9697205/Michael-Schumacher-prepares-for-a-second-Formula-One-retirement-before-riding-off-into-the-sunset.html|website=telegraph.co.uk|accessdate=11 March 2015}}</ref>

Commenting on the outcome of the season, former team owner [[Eddie Jordan]] said he believed Alonso would have deserved the title more, considering the "less competitive machine".<ref>{{cite web|title=Eddie Jordan Alonso more than earned the title than Vettel|url=http://nonf1.com/2012/11/Eddie_Jordan_Alonso_more_than_earned_the_title_than_Vettel.html|website=nonf1.com|accessdate=11 March 2015}}</ref> [[BBC]] columnist and former Formula One driver [[Jaime Alguersuari]] took a different viewpoint, saying the German deserved the title, having gone into the last race with "the most difficult position [...] – he had everything to lose".<ref>{{cite web|last1=Alguersuari|first1=Jaime|title=Jaime Alguersuari column: Sebastian Vettel deserved title|url=http://www.bbc.com/sport/0/formula1/20529678|website=bbc.com|accessdate=11 March 2015}}</ref>

====Controversies====
Three days after the race, [[Scuderia Ferrari|Ferrari]] announced they were contemplating filing an appeal with the [[Fédération Internationale de l'Automobile|FIA]] due to video footage allegedly showing [[Sebastian Vettel]] overtaking [[Jean-Éric Vergne]] under yellow flags. Should the allegations have proven valid, the FIA could have added 20 seconds to Vettel's finishing time, demoting him to 8th in the race result, handing the driver's title to [[Fernando Alonso|Alonso]].<ref>{{cite web|title=Ferrari contemplate challenge to Sebastian Vettel's F1 world title win|url=https://www.theguardian.com/sport/2012/nov/28/ferrari-challenge-sebastian-vettel-f1|publisher=Guardian|accessdate=4 February 2015}}</ref> Just two days later however, Ferrari opted not to appeal the result after receiving additional information from the FIA.<ref>{{cite web|title=Ferrari drops Vettel appeal|url=http://espn.go.com/racing/f1/story/_/id/8693532/ferrari-drops-appeal-fia-explanation|publisher=ESPN|accessdate=4 February 2015}}</ref> Formula One CEO [[Bernie Ecclestone]] had previously stated that he considered Ferrari's allegations "a shame" and "a joke".<ref>{{cite web|title=Formel-1-Boss Ecclestone über Ferrari-Verdacht: "Es ist eine Schande"|url=http://www.spiegel.de/sport/formel1/formel-1-bernie-ecclestone-ist-sauer-auf-ferrari-a-870181.html|website=spiegel.de|publisher=Spiegel Online|accessdate=4 February 2015|language=German}}</ref>

After losing 10th place in the constructor's championship to [[Caterham F1|Caterham]], [[Marussia F1|Marussia]]'s director of engineering [[Nikolai Fomenko]] accused driver [[Charles Pic]] of deliberately letting [[Vitaly Petrov]] pass him due to Pic having already signed a contract with Caterham for [[2013 Formula One season|next season]].<ref>{{cite news|url=http://izvestia.ru/news/541456|script-title=ru:Николай Фоменко: "Марусе" нужно объединиться с Петровым|trans_title=Nikolay Fomenko: "Marussia" should unite with Petrov|date=16 December 2012|work=[[Izvestia]]|publisher=News Media|language=Russian|accessdate=7 January 2013}}</ref> However, no actions were taken following these accusations.

==Classification==

===Qualifying===
{| class=wikitable style="font-size:95%"
! {{Tooltip|Pos.|Qualifying position}}
! {{Tooltip|No.|Number}}
! Driver
! Constructor
! Part 1
! Part 2
! Part 3
! {{Tooltip|Grid|Final grid position}}
|-
! 1
| 4
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| '''1:15.075'''
| 1:13.398
| '''1:12.458'''
| 1
|-
! 2
| 3
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:15.456
| 1:13.515
| 1:12.513
| 2
|-
! 3
| 2
| {{flagicon|AUS}} [[Mark Webber]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| 1:16.180
| 1:13.667
| 1:12.581
| 3
|-
! 4
| 1
| {{flagicon|DEU}} [[Sebastian Vettel]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| 1:15.644
| '''1:13.209'''
| 1:12.760
| 4
|-
! 5
| 6
| {{flagicon|BRA}} [[Felipe Massa]]
| [[Scuderia Ferrari|Ferrari]]
| 1:16.263
| 1:14.048
| 1:12.987
| 5
|-
! 6
| 18
| {{flagicon|VEN}} [[Pastor Maldonado]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 1:16.266
| 1:13.698
| 1:13.174
| 16{{ref|1|1}}
|-
! 7
| 12
| {{flagicon|DEU}} [[Nico Hülkenberg]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:15.536
| 1:13.704
| 1:13.206
| 6
|-
! 8
| 5
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[Scuderia Ferrari|Ferrari]]
| 1:16.097
| 1:13.856
| 1:13.253
| 7
|-
! 9
| 9
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 1:16.432
| 1:13.698
| 1:13.298
| 8
|-
! 10
| 8
| {{flagicon|DEU}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:15.929
| 1:13.848
| 1:13.489
| 9
|-
! 11
| 11
| {{flagicon|GBR}} [[Paul di Resta]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:15.901
| 1:14.121
|
| 10
|-
! 12
| 19
| {{flagicon|BRA}} [[Bruno Senna]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 1:15.333
| 1:14.219
|
| 11
|-
! 13
| 15
| {{flagicon|MEX}} [[Sergio Pérez]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:15.974
| 1:14.234
|
| 12
|-
! 14
| 7
| {{flagicon|DEU}} [[Michael Schumacher]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:16.005
| 1:14.334
|
| 13
|-
! 15
| 14
| {{flagicon|JPN}} [[Kamui Kobayashi]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:16.400
| 1:14.380
|
| 14
|-
! 16
| 16
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 1:16.744
| 1:14.574
|
| 15
|-
! 17
| 17
| {{flagicon|FRA}} [[Jean-Éric Vergne]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 1:16.722
| 1:14.619
|
| 17
|-
! 18
| 10
| {{flagicon|FRA}} [[Romain Grosjean]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 1:16.967
|
|
| 18
|-
! 19
| 21
| {{flagicon|RUS}} [[Vitaly Petrov]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 1:17.073
|
|
| 19
|-
! 20
| 20
| {{flagicon|FIN}} [[Heikki Kovalainen]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 1:17.086
|
|
| 20
|-
! 21
| 24
| {{flagicon|DEU}} [[Timo Glock]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 1:17.508
|
|
| 21
|-
! 22
| 25
| {{flagicon|FRA}} [[Charles Pic]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 1:18.104
|
|
| 22
|-
! 23
| 23
| {{flagicon|IND}} [[Narain Karthikeyan]]
| [[HRT F1|HRT]]-[[Cosworth]]
| 1:19.576
|
|
| 23
|-
! 24
| 22
| {{flagicon|ESP}} [[Pedro de la Rosa]]
| [[HRT F1|HRT]]-[[Cosworth]]
| 1:19.699
|
|
| 24
|-
! colspan=8|107% time: 1:20.330
|-
! colspan=8|Source:<ref>{{cite news |title=Hamilton leads all-McLaren front row |first=Matt |last=Beer |url=http://www.autosport.com/news/report.php/id/104526 |work=[[Autosport]] |publisher=[[Haymarket Media Group|Haymarket Publications]] |date=24 November 2012 |accessdate=24 November 2012}}</ref>
|}
;Notes:
* {{note|1|1}} – [[Pastor Maldonado]] received a ten-place grid penalty after receiving his 3rd reprimand of the season, for missing a call in to the weighbridge from the [[Fédération Internationale de l'Automobile|FIA]].<ref>{{cite news|title=Maldonado relegated ten places|url=http://en.espnf1.com/brazil/motorsport/story/96185.html|publisher=ESPN F1|date=24 November 2012|accessdate=24 November 2012}}</ref>

===Race===
{| class="wikitable" style="font-size: 95%"
! Pos
! No
! Driver
! Constructor
! Laps
! Time/Retired
! Grid
! Points
|-
! 1
| 3
| '''{{flagicon|GBR}} [[Jenson Button]]'''
| '''[[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| 71
| 1:45:22.656
| 2
| '''25'''
|-
! 2
| 5
| '''{{flagicon|ESP}} [[Fernando Alonso]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| 71
| +2.734
| 7
| '''18'''
|-
! 3
| 6
| '''{{flagicon|BRA}} [[Felipe Massa]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| 71
| +3.615
| 5
| '''15'''
|-
! 4
| 2
| '''{{flagicon|AUS}} [[Mark Webber]]'''
| '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]'''
| 71
| +4.936
| 3
| '''12'''
|-
! 5
| 12
| '''{{flagicon|DEU}} [[Nico Hülkenberg]]'''
| '''[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| 71
| +5.708
| 6
| '''10'''
|-
! 6
| 1
| '''{{flagicon|DEU}} [[Sebastian Vettel]]'''
| '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]'''
| 71
| +9.453
| 4
| '''8'''
|-
! 7
| 7
| '''{{flagicon|DEU}} [[Michael Schumacher]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| 71
| +11.907
| 13
| '''6'''
|-
! 8
| 17
| '''{{flagicon|FRA}} [[Jean-Éric Vergne]]'''
| '''[[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]'''
| 71
| +28.653
| 17
| '''4'''
|-
! 9
| 14
| '''{{flagicon|JPN}} [[Kamui Kobayashi]]'''
| '''[[Sauber]]-[[Scuderia Ferrari|Ferrari]]'''
| 71
| +31.250
| 14
| '''2'''
|-
! 10
| 9
| '''{{flagicon|FIN}} [[Kimi Räikkönen]]'''
| '''[[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]'''
| 70
| +1 Lap
| 8
| '''1'''
|-
! 11
| 21
| {{flagicon|RUS}} [[Vitaly Petrov]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 70
| +1 Lap
| 19
|
|-
! 12
| 25
| {{flagicon|FRA}} [[Charles Pic]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 70
| +1 Lap
| 22
|
|-
! 13
| 16
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 70
| +1 Lap
| 15
|
|-
! 14
| 20
| {{flagicon|FIN}} [[Heikki Kovalainen]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 70
| +1 Lap
| 20
|
|-
! 15
| 8
| {{flagicon|DEU}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 70
| +1 Lap
| 9
|
|-
! 16
| 24
| {{flagicon|DEU}} [[Timo Glock]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 70
| +1 Lap
| 21
|
|-
! 17
| 22
| {{flagicon|ESP}} [[Pedro de la Rosa]]
| [[HRT F1|HRT]]-[[Cosworth]]
| 69
| +2 Laps
| 24
|
|-
! 18
| 23
| {{flagicon|IND}} [[Narain Karthikeyan]]
| [[HRT F1|HRT]]-[[Cosworth]]
| 69
| +2 Laps
| 23
|
|-
! 19
| 11
| {{flagicon|GBR}} [[Paul di Resta]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 68
| Accident
| 10
|
|-
! Ret
| 4
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 54
| Collision
| 1
|
|-
! Ret
| 10
| {{flagicon|FRA}} [[Romain Grosjean]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 5
| Accident
| 18
|
|-
! Ret
| 18
| {{flagicon|VEN}} [[Pastor Maldonado]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 1
| Accident
| 16
|
|-
! Ret
| 19
| {{flagicon|BRA}} [[Bruno Senna]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 0
| Collision
| 11
|
|-
! Ret
| 15
| {{flagicon|MEX}} [[Sergio Pérez]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 0
| Collision
| 12
|
|-
! colspan=8|Source:<ref>{{cite web|last1=Collantine|first1=Keith|title=2012 Brazilian Grand Prix result|url=http://www.f1fanatic.co.uk/2012/11/25/2012-brazilian-grand-prix-result/|website=f1fanatic.co.uk|accessdate=18 March 2015}}</ref>
|}

==Championship standings after the race==
*'''Bold text''' indicates the World Champions.
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{| class="wikitable" style="font-size: 95%;"
|-
!
! {{Tooltip|Pos.|Position}}
! Driver
! Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 1
| {{flagicon|GER}} '''[[Sebastian Vettel]]'''
| align="left"| 281
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 2
| {{flagicon|ESP}} [[Fernando Alonso]]
| align="left"| 278
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 3
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| align="left"| 207
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 4
| {{flagicon|GBR}} [[Lewis Hamilton]]
| align="left"| 190
|-
|align="left"| [[File:1uparrow green.svg|10px]] 1
|align="center"| 5
| {{flagicon|GBR}} [[Jenson Button]]
| align="left"| 188
|}
{{col-2}}
;Constructors' Championship standings
{|class="wikitable" style="font-size: 95%;"
|-
!
! {{Tooltip|Pos.|Position}}
! Constructor
! Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 1
| {{flagicon|AUT}} '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| align="left"| 460
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 2
| {{flagicon|ITA}} [[Scuderia Ferrari|Ferrari]]
| align="left"| 400
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 3
| {{flagicon|GBR}} [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="left"| 378
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 4
| {{flagicon|GBR}} [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| align="left"| 303
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
|align="center"| 5
| {{flagicon|GER}} [[Mercedes-Benz in Formula One|Mercedes]]
| align="left"| 142
|}
{{col-end}}

<small>'''Notes:'''
* Only the top five positions are included for both sets of standings.</small>

==References==
{{reflist|2}}
{{Commons category|2012 Brazilian Grand Prix}}

{{F1 race report
|Name_of_race = [[Brazilian Grand Prix]]
|Year_of_race = 2012
|Previous_race_in_season = [[2012 United States Grand Prix]]
|Next_race_in_season = [[2013 Australian Grand Prix]]
|Previous_year's_race = [[2011 Brazilian Grand Prix]]
|Next_year's_race = [[2013 Brazilian Grand Prix]]
}}
{{F1GP 10-19}}

[[Category:2012 Formula One races|Brazil]]
[[Category:Brazilian Grand Prix|2012]]
[[Category:2012 in Brazilian motorsport|Grand Prix]]