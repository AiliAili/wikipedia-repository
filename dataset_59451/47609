
'''<big>Overview</big>'''

'''Dubai Parks and Resorts''' is the Middle East’s largest integrated leisure and theme park destination located on [[Sheikh Zayed road]] in [[Dubai]], United Arab Emirates. Spread over 25 million square feet<ref>{{Cite web|url=https://www.dubaiparksandresorts.com/en|title=Official Website|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, it features more than 100 rides and attractions, and consists of three theme parks: [[Motiongate Dubai|MOTIONGATE™ Dubai]], Bollywood Parks™ Dubai<ref name=":0">{{Cite web|url=https://www.bollywoodparksdubai.com/|title=Bollywood Parks Dubai|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref> and LEGOLAND Dubai<ref name=":1">{{Cite web|url=https://www.legoland.com/dubai/|title=LEGOLAND Dubai|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, and one water park: LEGOLAND Water Park<ref name=":2">{{Cite web|url=https://www.legoland.com/dubai/map-and-explore/legoland-water-park/|title=LEGOLAND Water Park|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>. It also encompasses Riverland Dubai<ref name=":3">{{Cite web|url=https://www.riverlanddubai.com/en|title=Riverland Dubai|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, a themed retail and dining destination, as well as the Polynesian-themed family resort, Lapita Hotel Dubai.<ref name=":4">{{Cite web|url=https://www.dubaiparksandresorts.com/en/hotels/lapita|title=Lapita Hotel|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

The official opening took place on 18 December 2016. 

Six Flags Dubai, will be the fourth theme park addition to Dubai Parks and Resorts and will open its doors in late 2019 with an additional 27 rides and attractions, and growing the destination to cover 30.6 million square feet.<ref name=":5">{{Cite web|url=http://www.thenational.ae/business/travel-tourism/building-begins-at-six-flags-theme-park-in-dubai|title=Six Flags|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

== Development history ==
The Dubai Parks and Resorts project was announced in 2012 and construction started in 2014. <ref>{{Cite web|url=http://www.thenational.ae/business/industry-insights/economics/dubai-on-white-knuckle-ride-to-revival-with-dh10bn-theme-park-project|title=Dubai Parks and Resorts project start|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

In 2014, the Company signed agreements with DreamWorks Animation<ref>{{Cite web|url=http://www.thenational.ae/uae/tourism/dreamworks-animation-to-join-dubai-theme-park|title=DreamWorks Animation at Dubai Parks and Resorts|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, Columbia Pictures<ref>{{Cite web|url=http://www.emirates247.com/news/emirates/dubai-parks-and-resorts-celebrates-its-inauguration-with-gala-spectacular-2016-12-18-1.645136|title=Columbia Pictures|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, Merlin Entertainments and various Bollywood studies to bring beloved Hollywood and Bollywood characters to its theme parks. 

In 2015, Dubai Parks and Resorts signed an agreement with Lionsgate<ref>{{Cite web|url=http://www.prnewswire.com/news-releases/lionsgate-partners-with-dubai-parks-and-resorts-for-the-hunger-games-inspired-theme-park-attractions-at-motiongate-dubai-300058262.html|title=Lionsgate at Dubai Parks and Resorts|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>, for a fifth themed zone in MOTIONGATE Dubai. In 2016, the Company announced the commencement of construction for Six Flags Dubai, the fourth theme park to be added to the Dubai Parks and Resorts destination.<ref>{{Cite web|url=http://www.thenational.ae/business/travel-tourism/building-begins-at-six-flags-theme-park-in-dubai|title=Six Flags commencement|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

In 2016 Dubai Parks and Resorts released its official theme song, ''All the Wonders of the Universe,'' created by Academy Award winner Alan Menken<ref>{{Cite web|url=http://www.thenational.ae/arts-life/family/dubai-parks-and-resorts-releases-official-theme-song|title=Dubai Parks and Resorts Song|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>.

In October 31, 2016, Dubai Parks and Resorts opened the doors to LEGOLAND Dubai and Riverland Dubai<ref name=":6">{{Cite web|url=http://gulfnews.com/business/sectors/tourism/dubai-parks-and-resorts-legoland-dubai-and-riverland-officially-open-on-monday-1.1921225|title=LEGOLAND Dubai Grand Opening|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>. This followed by the opening of Bollywood Parks Dubai on the 17<sup>th</sup> November 2016 <ref>{{Cite web|url=http://www.emirates247.com/news/emirates/bollywood-parks-open-in-dubai-with-shah-rukh-aamir-leading-the-way-2016-11-18-1.643787|title=Bollywood Parks Dubai Grand Opening|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref> and MOTIONGATE Dubai on the 16<sup>th</sup> December 2016. The official inauguration was held on the 18<sup>th</sup> December 2016, an event that was broadcast live across the globe<ref>{{Cite web|url=http://gulfnews.com/news/uae/leisure/dubai-parks-and-resorts-officially-inaugurated-1.1948057|title=Dubai Parks and Resorts Grand Opening|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>.

In 2017, the remaining elements of Dubai Parks and Resorts launched when both the LEGOLAND Water Park and the Lapita Hotel opened to paying guests on the 2<sup>nd</sup> January. Later in the year, DXB Entertainments announced a 60:40 partnership with Merlin Entertainments to bring a 250 room LEGO themed hotel to the Dubai Parks and Resorts destination.

2012: 

·        Dubai Parks and Resorts project announced by His Highness Sheikh Mohammed bin Rashid Al Maktoum, Vice-President and Prime Minister of the United Arab Emirates (UAE), and Ruler of Dubai<ref>{{Cite web|url=http://dxbentertainments.com/press-release-20140907/|title=Commencement announcement|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>.

2014: 

·        Groundbreaking starts on the Dubai Parks and Resorts project in Jebel Ali, Dubai<ref>{{Cite web|url=http://dxbentertainments.com/press-release-20140907/|title=Groundbreaking commencement|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

·        Agreements signed with leading entertainment properties DreamWorks, Columbia Pictures, Merlin Entertainments (LEGOLAND) and various Bollywood studios<ref>{{Cite web|url=http://dxbentertainments.com/press-release-20140907/|title=Dubai Parks and Resorts theme parks|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref> 

2015:

·        Agreement signed with Lionsgate for the fifth zone at MOTIONGATE Dubai<ref>{{Cite web|url=http://dxbentertainments.com/dubai-parks-and-resorts-announces-partnership-with-lionsgate-for-the-hunger-games-inspired-theme-park-attractions-at-motiongate-dubai/|title=Agreement with Lionsgate|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

2016:

·        Company announces the addition of Six Flags Dubai to the Dubai Parks and Resorts destination, due to open in late 2019<ref>{{Cite web|url=http://dxbentertainments.com/dubai-parks-and-resorts-breaks-ground-on-the-construction-of-six-flags-dubai-theme-park/|title=Addition of Six Flags|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

·        LEGOLAND Dubai opens to the public on 31 October 2016<ref name=":6" />

·        Bollywood Parks Dubai opens to the public on 17 November 2016<ref>{{Cite web|url=http://dxbentertainments.com/dubai-parks-and-resorts-celebrates-the-opening-of-bollywood-parks-dubai-following-the-successful-launch-of-legoland-dubai/|title=Opening of Bollywood Parks Dubai|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

·        MOTIONGATE Dubai opens to the public on 16 December 2016<ref>{{Cite web|url=http://dxbentertainments.com/dubai-parks-and-resorts-celebrates-its-inauguration-with-gala-spectacular/|title=Opening of MOTIONGATE Dubai|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

2017”

·        LEGOLAND Water Park opens to the public on 10 January 2017<ref>{{Cite web|url=http://dxbentertainments.com/legoland-water-park-makes-a-splash-as-it-officially-opens-in-dubai-parks-and-resorts/|title=LEGOLAND Water Park opens|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

·        Lapita Hotel opens to the public on 2 January 2017<ref>{{Cite web|url=http://gulfbusiness.com/lapita-hotel-opens-dubai-parks-resorts/|title=Lapita Hotel Opening|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

·        The Company announces a joint partnership with Merlin Entertainments to bring a LEGO themed hotel to the Dubai Parks and Resorts destination<ref>{{Cite web|url=http://dxbentertainments.com/dxb-entertainments-pjsc-and-merlin-entertainments-group-announce-the-legoland-dubai-hotel/|title=Merlin Entertainments Partnership|last=|first=|date=|website=|archive-url=|archive-date=|dead-url=|access-date=}}</ref>

== External links ==
* https://www.dubaiparksandresorts.com/en
* https://www.bollywoodparksdubai.com/en
* https://www.motiongatedubai.com/EN
* https://www.legoland.com/dubai/
* https://www.riverlanddubai.com/en
* https://www.dubaiparksandresorts.com/en/hotels/lapita

==References==
{{reflist|30em}}

{{coord missing|United Arab Emirates}}

[[Category:Amusement parks opened in 2016]]
[[Category:Resorts in Dubai]]