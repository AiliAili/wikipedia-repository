<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = O-17 Courier
  |image = Consolidated O-17 Maryland NG in flight 1931.jpg
  |caption = A Maryland Air National Guard Consolidated O-17 
}}{{Infobox Aircraft Type
  |type = Observation
  |manufacturer = [[Consolidated Aircraft Company]]
  |designer = 
  |first flight = April 1927
  |introduced = 
  |retired = 
  |status =
  |primary user = [[United States National Guard]]
  |more users = [[Royal Canadian Air Force]]
  |produced = 1928
  |number built = 35
  |unit cost =  
  |variants with their own articles = [[Consolidated PT-3]]
}}
|}

The [[Consolidated Aircraft Company|Consolidated]] '''Model 2 Courier''' was an observation and training [[airplane]] used by the [[United States National Guard]], under the designation '''O-17'''.

==Development==
A parallel development to the [[Consolidated PT-3|PT-3]] series, the '''XO-17''' was a converted PT-3 with such refinements as improved fuselage streamlining, oleo shock absorbers, wheel brakes, balanced elevators and increased fuel capacity.<ref name="Swanborough">{{citation | title=United States Military Aircraft Since 1909 | url=https://books.google.com/books?id=3QZUAAAAMAAJ&q=O-17 | first1=F. G. | last1= Swanborough | first2=Peter M. |last2=Bowers | publisher=Putnam | location =New York | isbn=0-85177-816-X | year=1964}}</ref>

It was used almost exclusively as a cross-country flying, gunnery, photographic and radio trainer.<ref name="EdenMoeng">{{citation | title=The Complete Encyclopedia of World Aircraft | url=https://books.google.com/books?id=6xMYAAAACAAJ | first1=Paul | last1=Eden | first2=Soph |last2=Moeng | publisher=Amber Books | location = London | year=2002 | isbn=978-0-7607-3432-2}}</ref> The '''O-17''' had a removable fairing (carrying a [[Scarff ring]] mounting for one .30 cal (7.62&nbsp;mm) trainable [[Browning machine gun]].

The [[Royal Canadian Air Force]] purchased three generally similar aircraft, two '''Model 7''' landplanes and one '''Model 8''' floatplane, the latter with the same float gear as the NY series.

The sole XO-17A was converted from the [[Consolidated PT-3|PT-3]] as a demonstrator that failed to secure any orders.<ref name="EdenMoeng" /> It was later fitted with the experimental [[Packard DR-980]] Diesel engine of 225&nbsp;hp (168&nbsp;kW).<ref name="Swanborough" />

The '''Model 15''' was also an O-17 type airframe fitted with a [[Pratt & Whitney R-1340]] engine. It too failed to win any contracts.<ref name="EdenMoeng" />

==Variants==
;XO-17 (prototype)
:[[Consolidated PT-3]] Conversion with a 225&nbsp;hp (168&nbsp;kW) [[Wright R-790]]-1 engine, streamlined fuselage, modified undercarriage, increased fuel capacity, provision for dual controls and a dorsal 0.3&nbsp;in (7.62&nbsp;mm) gun, one conversion.<ref name="Andrade">{{citation | last=Andrade | first= John M. | title=U.S. Military Aircraft Designations and Serials Since 1909 | url=https://books.google.co.uk/books?id=-KMgAAAAMAAJ&q=Courier | location= Hinckley, UK | publisher=Midland Counties Publications | year= 1979 | isbn = 0-904597-22-9}}</ref>
;O-17 Model 2 Courier
:Production version for United States National Guard use, 29 built.<ref name="EdenMoeng" />
;XO-17A (prototype)
:One [[Consolidated PT-3]] converted with a [[Wright R-790|Wright R-790-3]] engine intended for export.<ref name="Andrade" />
;Model 7 (RCAF landplane)
:Royal Canadian Air Force, two built.<ref name="EdenMoeng" />
;Model 8 (RCAF floatplane)
:Royal Canadian Air Force, one built.<ref name="EdenMoeng" />
;XPT-8 (demonstrator)
:The airframe of the XO-17A prototype fitted with a [[Packard DR-980]] Diesel engine of 225&nbsp;hp (168&nbsp;kw), scrapped in 1932.<ref name="Swanborough" />
;XPT-8A
:A single PT-3A (''29-115'') similarly converted with a [[Packard DR-980]] Diesel engine with Project Number 'P-564',<ref>http://www.joebaugher.com/usaf_serials/1922.html</ref>  but returned to PT-3A configuration.<ref>Andrade, John M. ''U.S. Military Aircraft Designations and Serials since 1909''. Earl Shilton, Leicester: Midland Counties Publications, 1979. ISBN 0-904597-22-9, page 198.</ref> The airframe was subsequently lost in a fatal midair with a P-12C of the [[17th Pursuit Squadron]] 2 miles W of [[New Baltimore, Michigan]] on 17 December 1931.<ref>http://www.aviationarchaeology.com/src/1940sB4/1931.htm</ref>
;Model 15 (demonstrator)
:Conversion with a [[Pratt & Whitney R-1340]] engine.<ref name="EdenMoeng" />

==Operators==
;{{flag|Canada|1921}}
*[[Royal Canadian Air Force]]
;{{flag|United States|1912}}
*[[United States National Guard]]

==Specifications (O-17)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Eden & Moeng (2002)<ref name="EdenMoeng" />
|crew= two
|capacity=
|payload main= 
|payload alt=
|length main= 27 ft 11 in
|length alt= 8.51 m
|span main= 34 ft 5.5 in
|span alt= 10.5 m
|height main= 9 ft 9 in
|height alt= 2.97 m
|area main= 296 ft<sup>2</sup>
|area alt= 27.5 m<sup>2</sup>
|airfoil=
|empty weight main= 1,881 lb
|empty weight alt= 853 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 2,723 lb
|max takeoff weight alt= 1235 kg
|more general= 
|engine (prop)= [[Wright R-790-1]]
|type of prop= radial
|number of props= 1
|power main= 225 hp
|power alt= 168 kW
|power original=
|max speed main= 118 mph
|max speed alt= 190 km/h
|cruise speed main= 100 mph
|cruise speed alt= 161 km/h
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 550 miles 
|range alt= 885 km
|ceiling main= 12,000'
|ceiling alt= 3660 m
|climb rate main= 865 ft/min
|climb rate alt= 264 m/min
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
* 1 × .30 cal (7.62 mm) [[M1919 Browning machine gun]]
|avionics=
}}

==See also==
*[[Consolidated PT-1 Trusty]]
*[[Consolidated PT-3]]
*[[Consolidated PT-11]]

==References==
{{commons category|Consolidated O-17 Courier}}
{{reflist}}

{{Consolidated aircraft}}
{{USAAF observation aircraft}}
{{USAF trainer aircraft}}

[[Category:United States military reconnaissance aircraft 1930–1939|Consolidated O-17]]
[[Category:Consolidated aircraft|O-17]]
[[Category:Single-engine aircraft]]