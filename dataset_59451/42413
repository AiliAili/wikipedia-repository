<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
  |name= X-51
  |image= File:X-51A_Waverider.jpg
  |caption= Artist's concept of X-51A during flight
}}{{Infobox aircraft type
  |type= [[Robot]]ic flight demonstrator
  |manufacturer= [[Boeing]]
  |designer= <!--only appropriate for single designers, not project leaders-->
  |first flight= 26 May 2010
  |introduced= <!--date the aircraft entered or will enter military or revenue service-->
  |retired= <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status= Flight testing <!--in most cases, this field is redundant; use it sparingly-->
  |primary user= [[United States Air Force]]
  |more users= <!--limited to three "more users" total; please separate with <br/>-->
  |produced= <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 4<ref name=USAF_X-51_fact_sht/>
  |program cost= <!-- total program cost. -->
  |unit cost= <!-- incremental or flyaway cost for military aircraft or retail price for commercial aircraft. -->
  |developed from= <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles= <!-- variants OF the topic type -->
}}
|}

The '''Boeing X-51''' (or '''X-51 WaveRider''') is an [[Unmanned aerial vehicle|unmanned]] research [[scramjet]] aircraft for [[hypersonic flight]] at {{convert|5|Mach|70000|mph km/h|sigfig=2}}, an altitude of {{convert|70000|ft|m}}. The aircraft was designated X-51 in 2005. It completed its first powered hypersonic flight on 26 May 2010.  After two unsuccessful test flights, the X-51 completed a flight of over six minutes and reached speeds of over Mach 5 for 210 seconds on 1 May 2013 for the longest duration powered hypersonic flight.

''[[Waverider]]'' refers in general to aircraft that take advantage of [[compression lift]] produced by their own [[shock wave]]s.  The X-51 program was a cooperative effort by the [[United States Air Force]], [[DARPA]], [[NASA]], [[Boeing]], and [[Pratt & Whitney Rocketdyne]]. The program was managed by the Aerospace Systems Directorate within the U.S. [[Air Force Research Laboratory]] (AFRL).<ref name=X-51_Closer_to_Flight>{{cite web |url= http://www.boeing.com/news/releases/2007/q2/070601a_nr.html |title= Successful Design Review and Engine Test Bring Boeing X-51A Closer to Flight |publisher= Boeing |date= 1 June 2007}}</ref><ref name=New_AFRL_Directorate>{{cite web|url=http://www.wpafb.af.mil/news/story.asp?id=123307634 |title=New AFRL Aerospace Systems Directorate takes shape |publisher=USAF |date=27 June 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20130309233339/http://www.wpafb.af.mil/news/story.asp?id=123307634 |archivedate=2013-03-09 |df= }}</ref>  X-51 technology is proposed for use in the High Speed Strike Weapon (HSSW), a Mach 5+ missile which could enter service in the mid-2020s.

==Design and development==
In the 1990s, the Air Force Research Laboratory (AFRL) began the [[Scramjet programs#HyTECH|HyTECH]] program for hypersonic [[air propulsion|propulsion]].  [[Pratt & Whitney]] received a contract from the AFRL to develop a [[hydrocarbon fuel|hydrocarbon-fueled]] [[scramjet]] engine which led to the development of the SJX61 engine. The SJX61 engine was originally meant for the [[NASA X-43#X-43C|NASA X-43C]], which was eventually canceled.  The engine was applied to the AFRL's Scramjet Engine Demonstrator program in late 2003.<ref name=FI_practical_scramjet>Warwick, Graham. [http://www.flightglobal.com/articles/2007/07/20/215592/x-51a-to-demonstrate-first-practical-scramjet.html "X-51A to demonstrate first practical scramjet"] {{webarchive |url=https://web.archive.org/web/20100208001749/http://www.flightglobal.com/articles/2007/07/20/215592/x-51a-to-demonstrate-first-practical-scramjet.html |date=February 8, 2010 }}. ''Flight International'', 20 July 2007.</ref>  The scramjet flight test vehicle was designated X-51 on 27 September 2005.<ref name=PD-MAP-2005>{{Cite web|url=http://www.pr.afrl.af.mil/mar/2005/sep2005.pdf |title=Propulsion Directorate Monthly Accomplishment Report |publisher=U.S. Air Force |date=September 2005 |format=PDF |deadurl=yes |archiveurl=https://web.archive.org/web/20061212072043/http://www.pr.afrl.af.mil/mar/2005/sep2005.pdf |archive-date=12 December 2006 }}</ref>

[[File:X-51A Waverider on B-52 2009.jpg|thumb|right|X-51A under the wing of a B-52 at [[Edwards Air Force Base]], July 2009]]
In flight demonstrations, the X-51 is carried by a [[Boeing B-52 Stratofortress|B-52]] to an [[altitude]] of about {{convert|50000|ft|km mi}} and then released over the [[Pacific Ocean]].<ref name=AFT>{{cite web |url= http://www.airforcetimes.com/news/2009/12/airforce_waverider_122109w/ |archive-url= https://archive.is/20130101192900/http://www.airforcetimes.com/news/2009/12/airforce_waverider_122109w/ |dead-url= yes |archive-date= 1 January 2013 |title= WaveRider makes first flight |date = 21 December 2009 |publisher= Air Force Times |accessdate= 22 December 2009}}</ref>  The X-51 is initially propelled by an [[MGM-140 ATACMS]] [[solid rocket booster]] to approximately {{convert|4.5|Mach|50000|mph km/h|sigfig=2}}. The booster is then jettisoned and the vehicle's [[Pratt & Whitney Rocketdyne]] SJY61 scramjet accelerates it to a top flight speed near {{convert|6|Mach|70000|mph km/h|sigfig=2}}.<ref>[http://www.boeing.com/news/releases/2007/q2/070601a_nr.html  "Successful Design Review and Engine Test Bring Boeing X-51A Closer to Flight"] {{webarchive |url=https://web.archive.org/web/20080611210339/http://www.boeing.com/news/releases/2007/q2/070601a_nr.html |date=June 11, 2008 }}. Boeing, 1 June 2007. Retrieved: 28 July 2008.</ref><ref name="AFMIL20100520">[http://www.af.mil/news/story.asp?id=123205648 "X-51A Waverider flight planned for May 25"] {{webarchive |url=https://web.archive.org/web/20121020101017/http://www.af.mil/news/story.asp?id=123205648 |date=October 20, 2012 }}. US Air Force, 20 May 2010. Retrieved: 20 May 2010. </ref>  The X-51 uses [[JP-7]] fuel for the SJY61 scramjet, carrying some {{Convert|270|lb|kg|abbr=on}} on board.<ref name=USAF_X-51_fact_sht>{{cite web |title=Factsheets: X-51A Waverider |publisher=U.S. Air Force |url= http://www.af.mil/AboutUs/FactSheets/Display/tabid/224/Article/104467/x-51a-waverider.aspx |date= |accessdate= 6 April 2015}}</ref>

===Applications for hypersonic technology===
DARPA once viewed X-51 as a stepping stone to [[DARPA Falcon Project#Blackswift|Blackswift]],<ref>Berger, Brian.[http://www.space.com/businesstechnology/080908-busmon-x15-flights.html "NASA Helping U.S. Air Force Gear Up for 2009 X-51 Flights"] {{webarchive |url=https://web.archive.org/web/20090605003912/http://www.space.com/businesstechnology/080908-busmon-x15-flights.html |date=June 5, 2009 }}. Space.com, 8 September 2008.</ref> a planned hypersonic demonstrator which was canceled in October 2008.<ref name=Flt_HTV_3_cancel>Trimble, Stephen. [http://www.flightglobal.com/articles/2008/10/13/317382/videos-darpa-cancels-blackswift-hypersonic-test-bed.html "DARPA cancels Blackswift hypersonic test bed"] {{webarchive |url=https://web.archive.org/web/20110520154029/http://www.flightglobal.com/articles/2008/10/13/317382/videos-darpa-cancels-blackswift-hypersonic-test-bed.html |date=May 20, 2011 }}. Flight Global, 13 October 2008.</ref>

In May 2013, the U.S. Air Force plans have X-51 technology applied to the High Speed Strike Weapon (HSSW), a missile similar in size to the X-51.  The HSSW could fly in 2020 and enter service in the mid-2020s.  It is envisioned to have a range of 500-600 nmi, fly at Mach 5-6, and fit on an [[Lockheed Martin F-35 Lightning II|F-35]] or in the internal bay of a [[Northrop Grumman B-2 Spirit|B-2]] bomber.<ref>{{cite news |url=http://www.aviationweek.com/Article.aspx?id=/article-xml/AW_05_20_2013_p24-579062.xml |title=High-Speed Strike Weapon To Build On X-51 Flight |first=Guy |last=Norris |work=Aviation Week & Space Technology |date= 20 May 2013}}</ref>

==Testing==
[[File:X-51A Makes Longest Scramjet Flight.jpg|thumb|right|upright|SJX61-2 engine successfully completes ground tests simulating Mach 5 flight conditions.]]

===Ground and unpowered testing===
Ground tests of the X-51A began in late 2005.{{cn|date=September 2016}}  A preliminary version of the X-51, the "Ground Demonstrator Engine No. 2", completed [[wind tunnel]] tests at the NASA [[Langley Research Center]] on 27 July 2006.<ref>{{Cite web |publisher= Space daily |url= http://www.spacedaily.com/reports/PWR_Completes_Mach_5_Testing_Of_Hypersonic_Propulsion_System_999.html |title= Completes Mach 5 Testing Of Hypersonic Propulsion System |accessdate= 28 July 2008}}</ref>  Testing continued there until a simulated X-51 flight at Mach 5 was successfully completed on 30 April 2007.<ref>[http://www.pw.utc.com/media_center/press_releases/2007/04_apr/4-30-2007_7712537.asp "Pratt & Whitney Rocketdyne's Revolutionary Scramjet Engine Successfully Powers First X-51A Simulated Flight"] {{webarchive |url=https://web.archive.org/web/20120319194421/http://www.pw.utc.com/media_center/press_releases/2007/04_apr/4-30-2007_7712537.asp |date=March 19, 2012 }}. Pratt & Whitney Rocketdyne, 30 April 2007.</ref><ref>[https://info.aiaa.org/tac/pc/HYTAPC/Newsletter/April_2008_HyTASP_Newsletter.pdf "AIAA HyTASP Program Committee  Inaugural Newsletter"] {{webarchive |url=https://web.archive.org/web/20110726054847/https://info.aiaa.org/tac/pc/HYTAPC/Newsletter/April_2008_HyTASP_Newsletter.pdf |date=July 26, 2011 }}. AIAA, April 2008.</ref> The testing is intended to observe [[acceleration]] between Mach 4 and Mach 6 and to demonstrate that hypersonic [[thrust]] "isn't just luck".<ref name=december15>Coppinger, Rob. "[http://www.flightglobal.com/articles/2009/08/06/330670/hypersonic-x-51a-gets-december-launch-date.html Hypersonic X-51A gets December launch date] {{webarchive |url=https://web.archive.org/web/20090809185652/http://www.flightglobal.com/articles/2009/08/06/330670/hypersonic-x-51a-gets-december-launch-date.html |date=August 9, 2009 }}". ''[[Flight Global]]'', 6 August 2009. Retrieved: 29 April 2010.</ref><ref>[http://www.aviationweek.com/aw/generic/story_generic.jsp?channel=space&id=news/Hyper080509.xml&headline=Hypersonic%20Test%20Flight%20On%20Track "Hypersonic Test Flight On Track"]{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}. ''Aviation Week'', 5 August 2009. {{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>  Four captive test flights were initially planned for 2009. However, the first captive flight of the X-51A on a B-52 was conducted on 9 December 2009,<ref name=ride_aboard_B-52>[http://www.edwards.af.mil/news/story.asp?id=123181983 "X-51A WaveRider gets first ride aboard B-52"] {{webarchive |url=https://web.archive.org/web/20110607122111/http://www.edwards.af.mil/news/story.asp?id=123181983 |date=June 7, 2011 }}. Edwards AFB News, 11 December 2009.</ref><ref>{{Cite web |url= http://www.space-travel.com/reports/X_51A_WaveRider_Gets_First_Ride_Aboard_B_52_999.html |title= X-51A WaveRider Gets First Ride Aboard B-52 |publisher= Space travel |date= 18 December 2009}}</ref> with further flights in early 2010.<ref name=getting_ready>[http://www.edwards.af.mil/news/story.asp?id=123193363 "X-51 getting ready for first flight"] {{webarchive |url=https://web.archive.org/web/20100308050239/http://www.edwards.af.mil/news/story.asp?id=123193363 |date=March 8, 2010 }}. USAF Edwards AFB News, 4 March 2010.</ref><ref>[http://www.afmc.af.mil/news/story.asp?id=123205546 "X-51A flight planned May 25"] {{webarchive |url=https://web.archive.org/web/20100525051747/http://www.afmc.af.mil/news/story.asp?id=123205546 |date=May 25, 2010 }}. US Air Force, 20 May 2010.</ref>

===Powered flight testing===
The first powered flight of the X-51 was planned for 25 May 2010, but the presence of a [[cargo ship]] traveling through a portion of the [[Naval Air Station Point Mugu]] Sea Range caused a 24-hour delay.<ref>{{cite web|url=http://www.af.mil/news/story.asp?id=123206309 |title=Shipping traffic delays X-51A launch |publisher=USAF |date=26 May 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100608233059/http://www.af.mil/news/story.asp?id=123206309 |archivedate=8 June 2010 }}</ref> The X-51 completed its first powered flight successfully on 26 May 2010.  It reached a speed of {{convert|5|Mach|70000|mph km/h|sigfig=2}}, an altitude of {{convert|70000|ft|m}} and flew for over 200 seconds; it did not meet the planned 300 second flight duration, however.<ref>{{cite web |last=Warwick |first=Graham |url=http://www.aviationweek.com/aw/generic/story.jsp?id=news/awx/2010/05/26/awx_05_26_2010_p0-230043.xml&headline=First%20X-51A%20Hypersonic%20Flight%20Deemed%20Success&channel=defense |title=First X-51A Hypersonic Flight Deemed Success |work=Aviation Week |date=26 May 2010 }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}{{Dead link|date=March 2016}}</ref><ref name=FI_1st_test/>  The test had the longest hypersonic flight time of 140 seconds while under its scramjet power.<ref name=FI_1st_test/> The [[NASA X-43|X-43]] had the previous longest flight burn time of 12 seconds,<ref name=FI_1st_test>{{Cite web |last= Croft |first= John |url= http://www.flightglobal.com/news/articles/pictures-342468/ |format= |title= X-51A Waverider reaches Mach 5 in 140s scramjet flight |newspaper= Flight International |publisher= Flight global |date= 27 May 2010}}</ref><ref name= Boeing_1st_flight>{{cite press release |url=http://boeing.mediaroom.com/index.php?s=43&item=1227 |title=Boeing X-51A WaveRider Breaks Record in 1st Flight |work=Boeing |date=26 May 2010 }}</ref><ref name=historic_flight>{{cite news|url=http://www.wpafb.af.mil/news/story.asp?id=123206524 |title=X-51 Waverider makes historic hypersonic flight |work=US Air Force |date=26 May 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20160410144856/http://www.wpafb.af.mil/news/story.asp?id=123206524 |archivedate=2016-04-10 |df= }}</ref> while setting a new speed record of Mach 9.68.

Three more test flights were planned and used the same flight [[trajectory]].<ref name=historic_flight/>  Boeing proposed to the Air Force Research Laboratory (AFRL) that two test flights be added to increase the total to six, with flights taking place at four to six week intervals, provided there are no failures.<ref name=Trimble9>{{cite journal |last= Trimble |first= Stephen |authorlink= |coauthors= |date= 31 March 2009 |title= X-51A flight may lead to B version |url= |journal= Flight International |page= 9}}</ref>

The second test flight was initially scheduled for 24 March 2011,<ref>Hennigan, W.J., "Retest Is Set For Hypersonic Craft", ''[[Los Angeles Times]]'', 24 March 2011, p. B2.</ref> but was not conducted due to unfavorable test conditions.<ref>Croft, John. [http://www.flightglobal.com/articles/2011/03/25/354798/air-force-launches-mission-opts-not-to-drop-x-51a.html "Air Force launches mission, opts not to drop X-51A"] {{webarchive |url=https://web.archive.org/web/20110401112538/http://www.flightglobal.com/articles/2011/03/25/354798/air-force-launches-mission-opts-not-to-drop-x-51a.html |date=April 1, 2011 }}. ''Flight International'', 25 March 2011.</ref>  The flight took place on 13 June 2011.  However, the flight over the Pacific Ocean ended early due to an inlet [[unstart]] event after being boosted to Mach 5 speed.  The flight data from the test was being investigated.<ref name=flightglobal-2nd-flight>{{cite web |url= https://www.flightglobal.com/news/articles/second-x-51-hypersonic-flight-ends-prematurely-358056/ |title= Second X-51 hypersonic flight ends prematurely |publisher= Flightglobal |work= Flight International |date= 15 June 2011}}</ref>  A B-52 released the X-51 at an approximate altitude of {{convert|50000|ft}}. The X-51's scramjet engine lit on [[ethylene]], but did not properly transition to [[JP-7]] fuel operation.<ref name=AFAmag2011>{{cite journal |last= Mehuron |first= Tamar A |title= Air Force World, Second X-51 Test Cut Short |journal= Air Force magazine |volume= 94 |issue= 8 |page= 17 |date= August 2011 |publisher= Air Force Association |url= http://www.airforcemag.com/MagazineArchive/Documents/2011/August%202011/0811world.pdf |accessdate= 4 August 2011}}</ref>

The third test flight took place on 14 August 2012.<ref>{{cite journal |last= Shachtman |first= Noah |authorlink= |coauthors= |title= It's do or die for Mach 5 Missile |journal= Danger Room |date= August 2012 |publisher= Wired |url= https://www.wired.com/dangerroom/2012/08/mach-5-missile/#more-89143 |accessdate= 14 August 2012}}</ref>  The X-51 was to make a 300-second (5 minutes) experimental flight at speeds of {{convert|5|Mach|60000|mph km/h}}.<ref>{{Cite news |url= http://www.nydailynews.com/news/national/hypersonic-plane-fly-new-york-los-angeles-hour-article-1.1135917 |title= Hypersonic plane fly New York–Los Angeles in an hour |publisher= NY daily news}}</ref> After separating from its rocket booster, the craft lost control and crashed into the Pacific.<ref name=Weinberger15Aug12>{{cite news |last=Weinberger |first=Sharon |title=X-51 Waverider: Hypersonic jet ambitions fall short |url=http://www.bbc.com/future/story/20120815-hypersonic-ambitions-fall-short |publisher=BBC |date=15 August 2012 |accessdate=15 August 2012}}</ref>  The Air Force Research Laboratory (AFRL) determined the problem was the X-51's upper right aerodynamic fin unlocked during flight and became uncontrollable; all four fins are needed for aerodynamic control.  The aircraft lost control before the scramjet engine could ignite.<ref name=flightglobal-3rd>{{cite news |author= Majumdar, Dave |title=X-51A Waverider test flight ends in failure |url=http://www.flightglobal.com/news/articles/x-51a-waverider-test-flight-ends-in-failure-375529/ |publisher=Flight International |date= 15 August 2012 |accessdate=15 August 2012}}</ref><ref>[http://www.flightglobal.com/news/articles/august-failure-of-boeing-x-51-likely-due-to-fin-resonance-378080/ "August failure of Boeing X-51 likely due to fin, resonance"] {{webarchive |url=https://web.archive.org/web/20161014135102/http://www.flightglobal.com/news/articles/august-failure-of-boeing-x-51-likely-due-to-fin-resonance-378080/ |date=October 14, 2016 }}. Flightglobal.com, 25 October 2012.</ref>

On 1 May 2013, the X-51 performed its first fully successful flight test on its fourth test flight.  The X-51 and booster detached from a B-52H and was powered to {{convert|4.8|Mach|50000|mph km/h|sigfig=2}} by the booster rocket.  It then separated cleanly from the booster and ignited its own engine.  The test aircraft then accelerated to {{convert|5.1|Mach|60000|mph km/h|sigfig=2}} and flew for 210 seconds until running out of fuel and plunging into the Pacific Ocean off Point Mugu for over six minutes of total flight time; this test was the longest air-breathing [[hypersonic]] flight.  Researchers collected telemetry data for 370 seconds of flight.  The test signified the completion of the program.<ref name=boeing-4th>[http://boeing.mediaroom.com/index.php?s=20295&item=128669 "Boeing X-51A WaveRider Sets Record with Successful 4th Flight"] {{webarchive |url=https://web.archive.org/web/20160304052913/http://boeing.mediaroom.com/index.php?s=20295&item=128669 |date=March 4, 2016 }}. Boeing, 3 May 2013.</ref><ref name=fg20130503>[http://www.flightglobal.com/news/articles/hypersonic-x-51-programme-ends-in-success-385481/ "Hypersonic X-51 programme ends in success"] {{webarchive |url=https://web.archive.org/web/20161011073942/http://www.flightglobal.com/news/articles/hypersonic-x-51-programme-ends-in-success-385481/ |date=October 11, 2016 }}. Flight International, 3 May 2013.</ref><ref name=aw20130502>[http://aviationweek.com/defense/x-51a-waverider-achieves-goal-final-flight "X-51A Waverider Achieves Hypersonic Goal On Final Flight"] {{webarchive |url=https://web.archive.org/web/20160412141711/http://aviationweek.com/defense/x-51a-waverider-achieves-goal-final-flight |date=April 12, 2016 }}. Aviation Week, 2 May 2013.</ref>  The Air Force Research Laboratory believes the successful flight will serve as research for practical applications of hypersonic flight, such as a missile, reconnaissance, transport, and [[Air-breathing engine|air-breathing]] first stage for a space system.<ref name=aw20131002>[http://www.aviationweek.com/Article/PrintArticle.aspx?id=/article-xml/AW_05_20_2013_p24-579062.xml&p=1&printView=true "High-Speed Strike Weapon To Build On X-51 Flight"] {{webarchive |url=https://web.archive.org/web/20140104023933/http://www.aviationweek.com/Article/PrintArticle.aspx?id=/article-xml/AW_05_20_2013_p24-579062.xml&p=1&printView=true |date=January 4, 2014 }}. Aviation Week, 20 May 2013.</ref>

==Specifications==
{{Aerospecs
|ref= Boeing,<ref>[http://www.boeing.com/defense-space/military/waverider/docs/X-51A_overview.pdf "X-51A Waverider Backgrounder"] {{webarchive |url=https://web.archive.org/web/20120906233323/http://www.boeing.com/defense-space/military/waverider/docs/X-51A_overview.pdf |date=September 6, 2012 }}. Boeing, September 2012.</ref> Air Force<ref name=USAF_X-51_fact_sht/>
|met or eng?=eng
|genhide=y
|crew= None
|capacity=
|length m= 7.62
|length ft= 25
|length in=
|span m=
|span ft=
|span in=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|empty weight kg= 1,814
|empty weight lb= 4,000
|gross weight kg=
|gross weight lb=
|eng1 number=1
|eng1 type=[[MGM-140 ATACMS]] rocket
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=Scramjet
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|perfhide=n
|max speed kmh= >6,200
|max speed mph= >3,900
|max speed mach= >5.1
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km= 740
|range miles= 460
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m= 21,300
|ceiling ft= 70,000
|climb rate ms=
|climb rate ftmin=
}}

==See also==
{{Portal|United States Air Force}}
*[[WU-14]] – a similar Chinese system
*[[Yu-71]] – a similar Russian system
*[[Hypersonic Technology Vehicle 2]]
*[[Hypersonic Technology Demonstrator Vehicle]] 
*[[Boeing Small Launch Vehicle]] concept, includes a hypersonic wave rider as the second stage
*[[Flight airspeed record]]
*[[Scramjet programs]]
*[[NASA X-43]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Boeing X-51}}
* [http://www.af.mil/AboutUs/FactSheets/Display/tabid/224/Article/104467/x-51a-waverider.aspx X-51 fact sheet on USAF site]
* {{cite web|title=WaveRider page on Boeing.com|url=http://www.boeing.com/defense-space/military/waverider/index.html|archiveurl=https://web.archive.org/web/20121114010656/http://www.boeing.com/defense-space/military/waverider/index.html|archivedate=14 November 2012}}
* [http://www.flightglobal.com/articles/2009/03/26/324373/afrl-mulls-adding-scope-to-x-51a-waverider-hypersonic.html "AFRL mulls adding scope to X-51A Waverider hypersonic tests"]. Flight International, March 2009.
* [http://www.aviationweek.com/search/AvnowSearchResult.do?reference=xml/awst_xml/2007/05/07/AW_05_07_2007_p93-01.xml&searchAction=display_result "Pratt & Whitney Rocketdyne Scramjet Excels in USAF Tests"]. Aviation Week - subscription
* [https://www.youtube.com/watch?v=a6PXTYO2Bx0 YouTube video of FoxNews report, preceding test flight]
* [https://www.youtube.com/watch?v=0qGNEmDMVDU YouTube video of test flight, shot from NASA chase plane]

{{X-planes}}
{{AFRL spacecraft}}
{{Boeing support aircraft}}

[[Category:Hypersonic aircraft|X-51, Boeing]]
[[Category:United States experimental aircraft 2000–2009|X-51, Boeing]]
[[Category:Boeing aircraft|X-51]]
[[Category:Scramjet-powered aircraft|X-51, Boeing]]
[[Category:Unmanned aerial vehicles of the United States|X-51, Boeing]]
[[Category:Single-engined jet aircraft]]
[[Category:Aircraft with auxiliary rocket engines]]