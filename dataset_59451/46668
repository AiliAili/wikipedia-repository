{{infobox state representative
| name= Betty Boyd
| above= Betty Boyd
| image= Betty Boyd.jpg
| header1= Oklahoma State Representative
| alt          = 
| state_house  =  Oklahoma 
| district     =  23
| term_start   =  1991
| term_end     =  2000
| predecessor  =  [[Kevin Easley]]
| successor    =  [[Sue Tibbs]]
| birth_name         = Margaret Elizabeth Carman
| birth_date         = {{birth date|1924|12|09}}
| birth_place        = [[Tulsa, Oklahoma]]
| death_date         = {{death date and age|2011|01|06|1924|12|09}}
| death_place        = [[Tulsa, Oklahoma]]
| party              = [[Democratic Party (United States)|Democratic Party]]
| spouse             = Bill Boyd
| children           = Beverlie Boyd, Barry Boyd
| profession         = Tulsa television personality
}}

'''Betty Carman Boyd''' (December 9, 1924 – January 6, 2011) was a longtime Tulsa television personality and a member of the [[Oklahoma State House of Representatives]]. Considered a pioneer for women in both fields, Boyd began her career in television in 1955 and was elected as a state legislator in 1990, serving until 2000.

==Early life==
Betty Boyd was born in 1924 in Tulsa, Oklahoma to Ted and Marie (Fairchild) Carman. Boyd was raised by her mother, grandmother, and several maiden aunts. Her interest in reading and writing began early in life. When asked about her interests, Boyd said "I wanted to write. I knew that. I loved journalism when I got to that point in school. In junior high, we had a junior high school paper at that time and I loved writing for that, that kind of writing,not books although I’ve done that some since.”<ref name="Oral History Interview">{{cite journal|title=Oral History Interview with Betty Boyd|journal=Oklahoma Women's Hall of Fame|date=February 22, 2008|url=http://dc.library.okstate.edu/cdm/ref/collection/Fame/id/735|accessdate=20 February 2015}}</ref> She graduated from [[Tulsa Central High School]] where she served as the yearbook editor.<ref name="Funeral Home Announcement">{{cite web|title=Betty Boyd|url=http://kennedycares.com/betty-boyd/|website=Kennedy funeral and cremation|publisher=Kennedy-Kennard Funeral & Cremation|accessdate=20 February 2015}}</ref>

==Education==
Boyd attended the [[University of Tulsa]] in Tulsa, and [[Iowa State University]] in Ames, Iowa. She double majored in home economics and journalism. While in college she was a member of the [[Delta Delta Delta]] sorority.<ref name="Funeral Home Announcement" /> Betty met her husband, Bill Boyd, on a bus returning home from the University of Tulsa. They both attended the same high school but had not met before.<ref name="Oral History Interview" /> Bill Boyd enlisted in the United States Army Air Corps and Betty left the University of Tulsa at age 17 to attend Iowa State University in the fall of 1943.<ref name="Oral History Interview" />

==Married life==
On August 31, 1943 Betty married Bill Boyd. Approximately three months after they married, Bill was stationed overseas in Italy as a B-24 pilot. While Bill was serving in Italy, Betty worked the swing-shift (3 pm - midnight) at the Tulsa Bomber Plant.<ref name="Oral History Interview" /> Betty and Bill had their daughter, Beverlie, in 1947 and their son, Barry, in 1950. The following summer of Beverlie's birth, Bill became temporarily paralyzed due to the complications of [[polio]]. Betty joined the [[March of Dimes]] at this time in an attempt to support her family and give back to the organization that helped the family pay their bills. She started speaking to groups about how to keep children safe from exposure to polio in 1948 on behalf of the March of Dimes. Her volunteer work with the [[March of Dimes]] eventually lead to her television career in 1955. When asked how the March of Dimes contributed to her later career, Betty said "it was just because this friend of mine knew that I had given lots of talks on behalf of the March of Dimes. So I always like to tell youngsters today, “Don’t fail to volunteer for anything you can.”<ref name="Oral History Interview" />

==Career==

===Television and radio career===
Boyd made her first television appearance with [[KOTV-DT|KOTV]] in 1955, where she was hired to host a daily women's show. There were not many women involved with broadcasting at this time which is why she is referred to as a pioneer in the field of journalism.<ref name="Oral History Interview" /> She joined [[KTUL]] in 1965 where she hosted ''The Betty Boyd Show''. In 1980 Boyd left KTUL to serve as the director of information for Tulsa Vo-Tech (now [[Tulsa Technology Center]]) where she became involved with radio and television commercials.<ref name=Emmys>{{cite web|title=Betty Boyd, Known as the Queen of Tulsa TV|url=http://m.emmys.com/news/news/betty-boyd-known-queen-tulsa tv|website=emmys.com|publisher=Television Academy|accessdate=20 February 2015}}</ref> In 1983, Betty published a book discussing Tulsa television, radio, and print personalities, ''If I Could Sing, I'd Be Dangerous.''<ref name=worldcat>{{cite web|title=If I could sing - I'd be dangerous|url=https://www.worldcat.org/title/if-i-could-sing-id-be-dangerous/oclc/10520341&referer=brief_results|website=worldcat.org|accessdate=23 February 2015}}</ref>

===Political career===
Boyd was elected to the [[Oklahoma State House of Representatives]] in 1990 and began serving in 1991 as a representative of District 23 until 2000. During her first term she served as the vice-chair of the large Education Committee and in her second term served as the chair of the Common Education Committee.<ref name="Oral History Legislature Interview" /> While in office, she was involved with the bill that created [[Oklahoma State University - Tulsa]].<ref name=Emmys /> Boyd's Legislature reflected the concerns of East Tulsa, such as education and health care.<ref name="Tulsa World">{{cite web|title=For Betty Boyd|url=http://www.tulsaworld.com/archives/for-betty-boyd/article_7c737581-865b-58bb-bb6c-fe7a57912f4f.html|website=tulsaworld.com|publisher=Tulsa World|accessdate=20 February 2015}}</ref> Boyd started the Reading Sufficiency Act and was an integral part in starting the Teacher Preparation Commission.<ref name="Oral History Legislature Interview" /> A survivor of breast cancer, Boyd promoted breast cancer research while in office. Also while in the legislature, Boyd opposed the legalization of casino gambling.<ref name="Oral History Legislature Interview">{{cite journal|last1=Finchum|first1=Tanya|title=Oral History Interview with Betty Boyd|journal=Women of the Oklahoma Legislature Oral History Project|date=January 26, 2007|url=http://dc.library.okstate.edu/cdm/ref/collection/legislature/id/393|accessdate=23 February 2015}}</ref> Betty began her term when she was 65 years old, making her the oldest member of the Oklahoma House at the time, and the first great-grandmother to serve in the House of Representatives.<ref name="Oral History Interview" /> When asked about her age at the time of her election, Boyd stated "I was not very young but that was not a factor, you know, as long as you have the energy for it. I could still do it. As long as you have the energy for it, the interest in it and if you like people and do not mind listening to them."<ref name="Oral History Legislature Interview" /> After her time in the House, Boyd refused to retire, and instead served on committees such as [[Friends of the Library]], Tulsa Health Department Board, among others.<ref name="Funeral Home Announcement" /> In 1996, Boyd was recognized for her lifetime achievements by being inducted into the [[Oklahoma Women's Hall of Fame]]. Two years later in 1998, Boyd received the American Women in Radio and Television Lifetime Achievement Award.<ref name="Oral History Legislature Interview" />

Betty Boyd's political philosophy: "...do the best you can for the people you live around and the people around the state and the people around the country and let it go at that. It does not matter which party’s name is on it."<ref name="Oral History Legislature Interview" />

==Awards and memberships==
*Inducted into the Oklahoma Women's Hall of Fame (1996)
*Queen of the Tulsa Centennial (1997)
*National Board of the March of Dimes
*American Women in Radio and Television Lifetime Achievement Award (1998)
*Tulsa Cerebral Palsy Association
*Delta Delta Delta sorority
*Defense Advisory Committee for Woman in the Service
*Beta Sigma Phi

===Retirement memberships===
Following her retirement, Boyd remained active in the following organizations:<ref name="Funeral Home Announcement" />
*Friends of the Library
*Tulsa Health Department Board
*Silver Haired Legislature
*Area Agency on Aging
*OSU Board of Trustees

==References==
{{reflist}}

==External links==
*[http://www.library.okstate.edu/oralhistory/owhof/ Oklahoma Women’s Hall of Fame Oral History Project – OSU Library]
*[http://www.library.okstate.edu/oralhistory/wotol/index.htm Women of the Oklahoma Legislature Oral History Project -- OSU Library]
*[http://www.voicesofoklahoma.com/interview/boyd-betty/ Voices of Oklahoma interview with Betty Boyd.] First person interview conducted on August 1, 2009, with Betty Boyd. 

{{DEFAULTSORT:Boyd, Betty}}
[[Category:1924 births]]
[[Category:2011 deaths]]
[[Category:Politicians from Tulsa, Oklahoma]]
[[Category:Iowa State University alumni]]
[[Category:University of Tulsa alumni]]
[[Category:Oklahoma Democrats]]
[[Category:Women state legislators in Oklahoma]]
[[Category:Members of the Oklahoma House of Representatives]]