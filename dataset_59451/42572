{|{{Infobox aircraft begin
 |name            = Virtus
 |image           = Virtus dropping orbiter.png
 |size            = 300px
 |alt             = 
 |caption         = Artist's concept of the Virtus
 |long caption    = 
}}{{Infobox aircraft type
 |type            = Outsized cargo aircraft
 |national origin = [[United States]]
 |manufacturer    = 
 |builder         = [[Turbo-Three Corporation]]
 |designer        = [[John M. Conroy]]
 |design group    = 
 |first flight    = 
 |introduction    = 
 |retired         = 
 |status          = Canceled
 |primary user    = [[NASA]]
 |more users      = 
 |produced        = 
 |number built    = 
 |program cost    = 
 |unit cost       = [[United States dollar|US$]]12.5 million (est.)
 |developed from  = [[Boeing B-52 Stratofortress]]
 |variants with their own articles = 
 |developed into  =
}}
|}
The '''Conroy Virtus''' was a proposed [[United States|American]] large transport aircraft intended to transport the [[Space Shuttle]]. Designed beginning in 1974 by [[John M. Conroy]] of the [[Turbo-Three Corporation]],<ref name="Bee">"[https://news.google.com/newspapers?id=sEchAAAAIBAJ&sjid=rn8FAAAAIBAJ&pg=1147,3211291&dq=virtus+aircraft+shuttle&hl=en Space Shuttle Orders Giant Plane Developed]" ''[[The Modesto Bee]]''. February 13, 1974, p.A17.</ref> it was to incorporate a pair of [[Boeing B-52 Stratofortress]] fuselages to form a new craft utilizing existing parts for cost-savings.

While the project was seriously considered, it proved impractically large, and [[NASA]] chose to develop the [[Boeing 747]]-based [[Shuttle Carrier Aircraft|Shuttle Carrier]] from surplus commercial aircraft instead.

==History==
The Space Shuttle was originally designed to utilize on-board [[turbofan]] engines for propulsion within the atmosphere, both upon [[re-entry]] and for ferry flights between landing sites, such as [[Edwards Air Force Base]], the [[White Sands Missile Range]], or contingency landing sites such as [[Easter Island]], to the launch site at [[Kennedy Space Center]] at [[Cape Canaveral]].<ref name="Low1">Lowther 2012, p. 26.</ref> When the air-breathing engines were deleted from the Shuttle design due to cost and weight concerns, a requirement arose for a transport aircraft capable of carrying the Shuttle from landing sites back to the Kennedy Space Center.<ref name="Jenkins">Jenkins 2001, p. 195.</ref> One early design for a shuttle carrier aircraft was proposed by [[John M. Conroy]], developer of the [[Pregnant Guppy]] and [[Super Guppy]] oversized cargo aircraft, in cooperation with the [[NASA Langley Research Center]]; named Virtus, a contract was issued for design and development work in 1974.<ref name="Bee"/>

Expected to cost [[United States dollar|US$]]12.5 million each,<ref name="Bee"/> Virtus was a twin-fuselage design powered by four large jet engines;<ref name="Bee"/> it was intended for these to be [[Pratt & Whitney JT9D]] turbofans.<ref name="Jenkins"/> Conroy proposed extensive use of 'off the shelf' military parts in the design to reduce costs;<ref name="Bee"/> this included the use of fuselages from [[Boeing B-52 Stratofortress]] strategic bombers to form the aircraft's main fuselage pods, added to the new-design wing and tail section.<ref name="Low2">Lowther 2012, p. 28.</ref> The [[Space Shuttle Orbiter]] would be carried under the center section of the Virtus aircraft's wing, between the fuselages;<ref name="Jenkins"/> other large cargos, including the [[Space Shuttle external tank]],<ref name="Bee"/> the [[Space Shuttle Solid Rocket Booster]]s, or dedicated cargo pods, could be alternatively carried.<ref name="Low2"/>

The Virtus design was tested in the NASA Langley [[wind tunnel]];<ref name="Jenkins"/><ref name="Low2"/> while the results of the wind tunnel tests were considered promising,<ref name="Jenkins"/> the drawbacks of such a large design, including the cost of developing an entirely new aircraft, flight testing the design, and the sheer size of the aircraft requiring the development and/or expansion of infrastructure to support it, militated against further development of Virtus.<ref name="Jenkins"/> The [[Lockheed Corporation]], which had proposed a twin-fuselage version of its [[C-5 Galaxy]] airlifter to carry the Shuttle, also saw its proposal rejected for the same reasons.<ref name="Jenkins"/> A more modest conversion of existing C-5s was proposed, and nearly taken up by NASA, but it was determined that having aircraft continually available was preferable to being restricted by the [[United States Air Force]] on the use of C-5s, and [[Boeing]]'s proposal for a conversion of the [[Boeing 747|747]] airliner was selected, becoming the [[Shuttle Carrier Aircraft]].<ref name="Jenkins"/> A proposed commercial version of the Virtus design, named Colossus,<ref>[https://books.google.com/books?id=VqkiAQAAMAAJ&q=VIRTUS+aircraft+shuttle&dq=VIRTUS+aircraft+shuttle&source=bl&ots=pmEvlq6k8a&sig=vksjU4qtAhY-Ke6FOtLGYIN2iuM&hl=en&sa=X&ei=aZYhUKCmM4Sa9gTc-oCgBQ&ved=0CDQQ6AEwAQ Cargo Aircraft, volume 64]. R. H. Donnelley Corp., 1974.</ref> also failed to gain any further interest, and the Virtus design was abandoned.<ref name="Low3">Lowther 2012, p. 29.</ref>

==Specifications==
{{Aircraft specs
|ref=Lowther 2012<ref name="Low2"/>
|prime units?=imp

|genhide=

|crew=
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=450
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=22166
|wing area note=
|aspect ratio=9
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=850000
|max takeoff weight note=
|fuel capacity=
|more general=

|eng1 number=4
|eng1 name=[[Pratt & Whitney JT9D]]-3A
|eng1 type=[[high-bypass turbofan]]s
|eng1 kn=
|eng1 lbf=45800
|eng1 note=
|power original=
|thrust original=

|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=
|cruise speed mph=300
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=3000
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=35000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|thrust/weight=

|more performance=
|avionics=
}}

==See also==
{{aircontent
|see also=
* [[Antonov An-225 Mriya]]
* [[Scaled Composites Stratolaunch]]
* [[Lockheed_C-5_Galaxy#C-5_Shuttle_Carrier|C-5 Shuttle Carrier]]

|related=
* [[Boeing B-52 Stratofortress]]
|similar aircraft=
* [[Myasishchev VM-T]]
* [[Shuttle Carrier Aircraft]]
|lists=
}}

==References==
===Citations===
{{reflist|1}}

===Bibliography===
{{refbegin}}
* {{cite book|last=Jenkins|first=Dennis R.|title=Space Shuttle: The History of the National Space Transportation System - The First 100 Missions|year=2001|publisher=Voyageur Press|location=Stillwater, MN|isbn=0-9633974-5-1}}
* {{cite journal|last=Lowther|first=Scott|date=March–April 2012|title=Virtus|journal=Horizons|publisher=American Institute of Aeronautics and Astronautics Houston Section|location=Houston, TX|volume=37|issue=5|url=http://www.aiaahouston.org/Horizons/Horizons_2012_03_and_04.pdf}}
{{refend}}

{{Outsized cargo aircraft}}
{{Space Shuttle}}

[[Category:Conroy aircraft|Virtus]]
[[Category:Quadjets]]
[[Category:Twin-fuselage aircraft]]
[[Category:High-wing aircraft]]
[[Category:NASA aircraft]]
[[Category:Abandoned civil aircraft projects of the United States]]
[[Category:Space Shuttle program]]