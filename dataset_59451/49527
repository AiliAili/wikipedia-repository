[[File:DotNet3.0.svg|alt=dot net three point zero windows stack diagram|thumb|right|300px|This [[subsystem]] is a part of [[.NET Framework 3.0]]]]

'''Windows Workflow Foundation''' (WF) is a [[Microsoft]] technology that provides an [[Application programming interface|API]], an in-process [[workflow engine]], and a rehostable designer to implement long-running processes as workflows within [[.NET Framework|.NET]] applications. The current version of WF was released as part of the [[.NET Framework]] version 4.5 and is referred to as (WF45).<ref>{{cite web |url=http://msdn.microsoft.com/en-us/library/ee342461.aspx |title=A Developer's Introduction to Windows Workflow Foundation (WF) in .NET 4 |accessdate=August 21, 2012 }}</ref>

A workflow, as defined here, is a series of distinct programming steps or phases. Each step is modeled in WF as an Activity. The .NET Framework provides a library of activities (such as WriteLine, an activity that writes text to the console or other form of output). Custom activities can also be developed for additional functionality. Activities can be assembled visually into workflows using the Workflow Designer, a design surface that runs within [[Visual Studio]]. The designer can also be hosted in other applications.

Encapsulating programming functionality into the activities allows the developer to create more manageable applications; each component of execution can be developed as a [[Common Language Runtime]] object whose execution will be managed by the workflow runtime.

==Workflow Foundation versions==
* Workflow Foundation was first released in Version 3 of the .NET Framework, and primarily uses the System.Workflow{{Not a typo|.}}Activities, System.Workflow.ComponentModel, and System.Workflow{{Not a typo|.}}Runtime namespaces. Workflows in version 3 were created using either the Sequential model (in which activities are executed in order, with the completion of one activity leading to the next), or the State Machine model (in which activities are executed in response to external events). Microsoft SharePoint 2007 uses WF 3.
* In .NET 3.5, messaging activities were introduced that integrated Workflow with Windows Communication Foundation (WCF). With the new ReceiveActivity, workflows could respond to incoming WCF messages. The new features of Workflow in version 3.5 use the System.ServiceModel namespace. Microsoft SharePoint 2010 uses WF 3.5.
* In .NET 4, Windows Workflow Foundation was greatly updated, with new features such as Data Contract Resolver, Flowchart, and other flow control activities added. Workflow in .NET 4 uses the System{{Not a typo|.}}Activities namespace. Most notably, there is no longer a Workflow Runtime object in version 4; workflows are executed directly using WorkflowApplication or WorkflowInvoker instead.
* Activities created in previous versions of the .NET Framework can be executed by .NET 4 workflows using the Interop activity.
* Future versions and releases of Windows Phone will include an updated State Machine and Dynamic Update.

==Workflow usage scenarios==
Windows Workflow Foundation is used to create applications that execute an ordered business process, such as the steps needed to approve a document, hire a candidate for a position, or make a purchase. These processes can execute in a short amount of time, but are typically long-running, in which the application will need to shut down to conserve memory between steps. Typically, business processes to be modeled as workflows have the following features:
* Have specific business logic that may need to change periodically, such as the tax or shipping calculation needed to determine the purchase price of an item, or the series of steps needed to approve a purchase, hire, or process.
* Have several inputs into the workflow that may come hours or days apart
* Have advanced business logic that might require workflow execution to travel down different branches depending on different circumstances.
* Need to interact with other systems, such as a database, website or other client application, or web service.

==Authoring workflows==
Workflows are created either by being defined in XAML [[Extensible Application Markup Language]] using the workflow designer, or by being assembled programmatically in a .NET language such as C# or VB.NET.
If the designer is used, activities are assembled on the workflow designer canvas by dragging them from the toolbox. Workflow arguments and variables are also created and assigned within the designer. If a workflow is assembled in code, activities are instantiated like other CLR objects, and assembled into collections of a single parent activity, usually a Sequence or Flowchart. The single parent activity is then executed using WorkflowApplication or WorkflowInvoker, and runs as a workflow. The term "Workflow" here usually refers to the root activity that is executed by the host. Workflows can use both out-of-box activities and custom activities. Out-of-box activities include flow control activities such as DoWhile, Flowchart-related activities such as FlowDecision, WCF Messaging activities such as Send, and primitive activities that perform simple tasks like Assign and WriteLine. Custom activities are user-created CLR objects that derive from the class System.Activities{{Not a typo|.}}Activity, and provide declarative functionality by allowing the developer to define the execution behavior of the activity in code. Custom activities can benefit from having a custom activity designer associated with them to enhance the visual authoring experience in the Visual Studio IDE.

==Books on Workflow==

===.NET 3.0 and 3.5===
* Dharma Shukla, Bob Schmidt: ''Essential Windows Workflow Foundation'', Addison-Wesley Professional, 13 October 2006, ISBN 0-321-39983-8
* Michael Stiefel: ''Building Applications with Windows Workflow Foundation (WF): Basics of Windows Workflow Foundation (Digital Short Cut)'', June 5, 2007, Kindle, ISBN 0-321-51454-8
* Brian Noyes: ''Developing Applications with Windows Workflow Foundation (WF) (Video Training)'', June 7, 2007, Brian Noyes, ISBN 0-321-50313-9
* Brian R. Myers: ''Foundations of WF'', Apress, 23 October 2006, ISBN 1-59059-718-4
* Bruce Bukovics: ''Pro WF:  Windows Workflow in .NET 3.0'', Apress, 19 February 2007, ISBN 1-59059-778-8
* Todd Kitta: ''Professional Windows Workflow Foundation'', Wrox, 12 March 2007, ISBN 0-470-05386-0
* Kenn Scribner: ''Microsoft Windows Workflow Foundation Step by Step'', Microsoft Press, 28 February 2007, ISBN 0-7356-2335-X

===.NET 4.0===
* Mark Collins: ''Beginning WF: Windows Workflow in .NET 4.0'', Apress, March 19, 2010, ISBN 978-1-4302-2485-3
* Bruce Bukovics: ''Pro WF: Windows Workflow in .NET 4'', Apress, June 29, 2010, ISBN 978-1-4302-2721-2
* Andrew Zhu: ''Microsoft Windows Workflow Foundation 4.0 Cookbook'', Packt Publishing, September 24, 2010, ISBN 978-1-84968-078-3

===.NET 4.5===
* Bayer White: ''Pro WF 4.5'', Apress, November 14, 2012, ISBN 978-1-4302-4383-0

==References==
{{reflist}}

==External links==
* [http://msdn.microsoft.com/en-us/library/dd489441.aspx MSDN Library: Windows Workflow Foundation]

{{.NET Framework}}
{{Microsoft APIs}}

{{Authority control}}
[[Category:Windows Vista|Workflow Foundation|Windows 7|Windows Server 2008|Windows Server 2008 R2|Windows Server AppFabric]]
[[Category:.NET Framework terminology]]