<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Zenith CH 200 & 250
 | image=ZenairCH200C-GIHT.JPG
 | caption=CH 200
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft|Homebuilt]] light aircraft
 | national origin=[[France]]/[[Canada]]
 | manufacturer=[[Zenair]]
 | designer=Chris Heintz
 | first flight=22 March 1970
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Zenair Zenith CH 200''' and '''CH 250''' are a family of [[Canada|Canadian]] single-engined [[homebuilt aircraft|homebuilt]] light aircraft. It is a low-winged single engine [[monoplane]], that was first flown in France in 1970, with kits being made by the Canadian company [[Zenair]] from 1974, with hundreds built and flown.

==Development and design==
In October 1968, the German [[aeronautical engineer]] Chris Heintz, who worked for [[Avions Pierre Robin]], started work on the design of a two-seat all-metal light aircraft suitable for amateur construction, the Zenith, with the prototype making its maiden flight on 22 March 1970.<ref name="Janes 76 p457">Taylor 1976, p. 457.</ref>

Heintz migrated to Canada in 1973,<ref>[http://www.zenithair.com/c-heintz.html Chris Heintz:Designer]". ''Zenith Aircraft Company''. Retrieved 27 February 2010.</ref> and set up [[Zenair]] in 1974 to sell plans and kits of the Zénith.<ref name="Manu p525">Gunston 2005, p.525.</ref> The Zenith, which gained the designation Zenith CH 200 when Heintz produced plans for larger and smaller derivatives, is a low-winged [[Cantilever#In aircraft|cantilever]] [[monoplane]] of all metal construction. The pilot and passenger sit side-by-side under a clear, sideways opening [[plexiglas]] canopy, while the aircraft is fitted with a fixed [[Tricycle gear|nosewheel undercarriage]]. It is designed to be powered by a single piston engine of between 85 and 160&nbsp;hp (63.5 and 119&nbsp;kW).<ref name="Janes 82 p494"/>

The first Zenith to be built in North America flew in October 1975, and by 1976, over 300 plans had been sold.<ref name="Janes 76 p458">Taylor 1976, p.458.</ref> Plans continued to be available in 1999, by which time hundreds were flying.<ref name="Brasseys p535">Taylor 1999, p.535.</ref>

At the 1976 [[AirVenture|EAA Convention]] in [[Oshkosh, Wisconsin]] the factory used volunteer labour to build and fly a CH 200 in eight days, using 550 person-hours.<ref name="ZenPam">[[Zenair]], ''Zenair pamphlet'', circa 1986.</ref>

==Variants==
[[File:Zenair ch250 zenith g-rays arp.jpg|thumb|Zenair CH 250]]
[[File:Zenair CH250 C-GAHQ.JPG|thumb|Zenair CH 250]]
[[File:Zenair ZENITH CH200 C-GGLH 01.JPG|thumb|Zenair CH 200 with forward sliding canopy]]
[[File:Zenair CH250 TD Zenith C-GTJR.JPG|thumb|Zenair CH 250 TD with conventional landing gear]]
;Zenair CH 200
:Initial version. Could be built as a cross country cruiser with an engine of {{convert|85|to|125|hp|kW|0|abbr=on}} or as an aerobatic trainer with modifications and a powerplant producing {{convert|125|to|160|hp|kW|0|abbr=on}}. The aircraft can be constructed as a [[taildragger]] or on [[tricycle gear]] and flown as a skiplane or on floats.<ref name="ZenPam" />
;Zenair CH 250
:Improved version with more fuel, larger baggage area, rear windows and a forward sliding canopy.<ref name="Janes 82 p494"/><ref name="ZenPam" />
;Zenair CH 250 TD
:"Tail Dragger" version with [[conventional landing gear]].<ref name="ZenPam" />

==Specifications (100 hp engine)==
{{Aircraft specs
|ref=Jane's All The World's Aircraft 1982-83<ref name="Janes 82 p494">Taylor 1982, p.494.</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=6.25
|length ft=
|length in=
|length note=
|span m=7.00
|span ft=
|span in=
|span note=
|height m=2.11
|height ft=
|height in=
|height note=
|wing area sqm=9.80
|wing area sqft=
|wing area note=
|aspect ratio=5:1<!-- sailplanes -->
|airfoil=NACA 64A515 (Modified)
|empty weight kg=
|empty weight lb=900
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=1450
|max takeoff weight note=
|fuel capacity=16 Imp Gallons (72.5 L)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental O-200]]
|eng1 type=air-cooled [[flat-four]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=(suitable for engines of between 85 hp (63.5 kW) and 160 hp (119 kW))
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=147
|max speed kts=
|max speed note=at sea level
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=130
|cruise speed kts=
|cruise speed note=(75% power) at sea level
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=53<!-- aerobatic -->
|stall speed kts=
|stall speed note=(flaps down)
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=520
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=15100
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=800
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=*[[Bede BD-1]]
|related=*[[Zenair CH 100]]
<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category}}
{{refbegin}}
*[[Bill Gunston|Gunston, Bill]]. ''World Encyclopedia of Aircraft Manufacturers''. Second edition. Stroud, UK:Sutton Publishing, 2005. ISBN 0-7509-3981-8.
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1976-77''. London:Jane's Yearbooks, 1976. ISBN 0-354-00538-3.
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1982-83''. London:Jane's Yearbooks, 1982. ISBN 0-7106-0748-2.
*Taylor, Michael J. H. ''Brassey's World Aircraft & Systems Directory 1999/2000''. London:Brassey's, 1999. ISBN 1-85753-245-7.

<!-- insert the reference sources here -->
{{refend}}

<!-- ==External links== -->

{{Zenair}}

[[Category:Canadian sport aircraft 1970–1979]]
[[Category:Homebuilt aircraft]]
[[Category:Zenair aircraft|CH 200]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]