{{external links|date=June 2013}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject_Musicians -->
| name                = John Micco
| image               =John Micco(2).png
|caption             = John Micco in 2012 
|background          = solo_singer
|alias               = Micco
|birth_date   = 
|birth_place         = 
|origin              = 
|genre               = 
| occupation          = Musician, writer
|instrument          = [[Bass Guitar]], [[Singing|vocals]]
|years_active        = 
|label               = 
|associated_acts     = [[Crush (American band)|Crush]], [[Cats on a Smooth Surface]]
| website             = {{URL|http://www.johnmicco.com/}}
|notable_instruments = 
}}
'''John Micco''', aka '''Micco''', is an American bass player, back-up vocalist and songwriter based in [[New York City]].

Micco is best known as a member of the alternative rock band [[Crush (American band)|Crush]] from 1991-1993.  Crush also included [[Paul Ferguson]] from the British 
post-punk/industrial group [[Killing Joke]], [[John Valentine Carruthers|John Carruthers]] from the British rock bands [[Siouxsie and the Banshees]] and [[Clock DVA]] and NJ’s [[Fred Schreck]] from The Ancients. They were signed to [[Atlantic Records]] where they recorded their eponymous first album ''Crush'' .<ref>[http://www.discogs.com/Crush-Crush/release/2646528 "Crush-Crush Release"] "Discogs.com" accessed October 1, 2012</ref> This album was recorded in Wales and produced by [http://www.allmusic.com/artist/pat-moran-mn0000179699 Pat Moran] (Robert Plant, Iggy Pop, Edie Brickell and the New Bohemians).<ref>[http://www.allmusic.com/artist/pat-moran-mn0000179699 "Pat Moran Discography"]</ref>

Micco's other major releases include [http://www.cduniverse.com/search/xx/music/pid/1011458/a/Bad+Vibes.htm Lloyd Cole's "Bad Vibes"] produced by [http://www.filmmusicmag.com/?p=9705 Adam Peters] and mixed by [[Bob Clearmountain]] in 1993 and [http://www.allmusic.com/album/shakeup-mw0000010969/credits Robert Becker's "Shakeup"] in 2001.

From 1995-2001, Micco was the house bassist/vocalist for the Brooklyn, N.Y. based indie label, Catatonic Records.<ref>[http://www.allmusic.com/artist/john-micco-mn0001733414 "John Micco"] "Allmusic.com" accessed October 1, 2012</ref>

Micco was a member of [[Cats on a Smooth Surface]], the house band at [[The Stone Pony]] in [[Asbury Park, NJ]] from 1981-1983. [[Bruce Springsteen]] would regularly jam with Cats at the Stone Pony on Sunday nights, and other top area musicians would sit in as well.<ref>{{Cite web|title=Bruce Springsteen With Friends At The Stone Pony: 25 July 1983|url=http://brucespringsteen.altervista.org/bs1982-07-25.htm|work=Bruce Springsteen - Maclen's Live Archive|publisher=Gianmaria Rizzardi|accessdate=6 October 2012}}{{Brucebase}}</ref>

Notably, Micco has worked with Grammy award-winning engineer/producer [[Andy Wallace (producer)|Andy Wallace]] on several unreleased projects. He has also enjoyed recording and touring with artists: [[Joe Lynn Turner]] ([[Rainbow (English band)|Rainbow]], [[Deep Purple]]) and [[Gary Corbett]] ([[Cinderella (band)|Cinderella]]) as well as jamming with the following artists: [[Reeves Gabrels]] ([[Tin Machine]], [[David Bowie]]), [[Clarence Clemons]], and [[Eric Carr]] ([[Kiss (band)|KISS]]), and members from the following bands: [[Iron Maiden]], [[Blue Öyster Cult]], [[Southside Johnny & The Asbury Jukes|Asbury Jukes]], and The News (The Huey Lewis band circa 1980s).  His bands have opened for [[Chicago (band)|Chicago]], [[Bryan Adams]],<ref>[http://www.setlist.fm/setlist/chicago/1984/roberto-clemente-coliseum-san-juan-puerto-rico-33d51459.html "Chicago at Roberto Clemente Collesium"]</ref> [[The Go Go's]] and  [[Twisted Sister]].<ref>[http://www.jammagazineonline.com/mf201112-twisted-sister.aspx "Twisted Sister"]</ref>

Micco does session work and plays weekly with a number of bands.  In December 2013 one of his original bands, The Darrens played at Irving Plaza, opening up for Mark McGrath and his band [https://www.youtube.com/watch?v=pQRI9QR96Os Sugar Ray.]  

Most recently, Micco has been playing with The Bobby Bandiera Band at the Jersey Shore.  
==Discography==
{| class="wikitable"
! Year
! Artist
! Album
|-
| 1993
| ''Crush''
|  Crush
|-
| 1993
| ''Lloyd Cole''
| Bad Vibes
|-
| 2001
| ''Robert Becker''
| Shakeup
|-
|}

==References==
{{Reflist}}

==External links==
[[File:John Micco in 1999.jpg|thumb|upright|left|John Micco in 2011]]
*[http://www.johnmicco.com John Micco Official Website]
*{{allmusic|artist|john-micco-mn0001733414}}
*[http://albumcredits.com/Profile/105572 John Micco Album Credits]
*{{YouTube|id=_nWwNCy1Vbw|title=Crush "(I Can't Stop) The Rain" Music Video}}
*[http://vassifer.blogs.com/alexinnyc/2006/03/interview_with_.html "Interview with Big Paul Ferguson Mentioning John Micco"]
*[http://www.cduniverse.com/search/xx/music/artist/John+Micco/a/albums.html "John Micco Discography"]
*[http://www.allmusic.com/artist/crush-mn0000782845 "Crush biography"]
*[http://www.allmusic.com/album/crush-mw0000095561 "Crush Album"]
*[http://www.discogs.com/search?q=john+micco&type=all "John Micco Disc Catalogue"]
*[http://brucebase.wikispaces.com/1982-05-02+-+THE+STONE+PONY%2C+ASBURY+PARK%2C+NJ "Cat's on a Smooth Surface at Stone Pony"]
*[http://www.allmusic.com/album/crush-mw0000095561 "Crush Review"]

{{Authority control}}

{{DEFAULTSORT:Micco, John}}
[[Category:American bass guitarists]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]