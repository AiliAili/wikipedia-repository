{{italic title}}
{{Wikisource}}
{{unreferenced|date=October 2009}}
'''''Diana of the Crossways''''' is a novel by [[George Meredith]] which was published in [[1885 in literature|1885]].  It is an account of an intelligent and forceful woman trapped in a miserable marriage and was prompted by Meredith's friendship with society beauty and author [[Caroline Elizabeth Sarah Norton|Caroline Norton]].

The heroine Diana Warwick says: "we women are the verbs passive of the alliance, we have to learn, and if we take to activity, with the best intentions, we conjugate a frightful disturbance.  We are to run on lines, like the steam-trains, or we come to no station, dash to fragments.  I have the misfortune to know I was born an active.  I take my chance."  Her efforts to advance her husband, through cultivating a friendship with Cabinet Minister Lord Dannisburgh, leads to scandal and alienation from her husband, Augustus Warwick. Her intention to live "independently" through writing, are initially successful, but her involvement in politics brings her to grief, both personal and public.

Diana, beautiful, charming and intelligent but hotheaded, becomes embroiled in a political as well as a social scandal (the politics are based on the troubled history of [[Robert Peel]]'s administration, and the 1845 [[Corn Laws]] in particular).

==Adaptation==
In 1922 the novel was adapted into a film ''[[Diana of the Crossways (film)|Diana of the Crossways]]'' directed by [[Denison Clift]] and starring [[Fay Compton]] and [[Henry Victor]].

==See also==
{{portal|Novels}}
{{George Meredith}}

*[[Caroline Norton]]
*[[History of feminism]]

{{DEFAULTSORT:Diana Of The Crossways}}
[[Category:1885 novels]]
[[Category:Feminism and history]]
[[Category:Feminist novels]]
[[Category:Novels by George Meredith]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]

{{1880s-novel-stub}}