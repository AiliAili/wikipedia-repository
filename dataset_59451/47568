{{Infobox person
| name = Don Richardson
| birth_name =
| birth_date = {{birth date|1918|4|30|mf=y|}}
| birth_place = [[New York City, New York]], [[United States|U.S.]]
| death_date = {{death date and age|1996|1|10|1918|4|30|mf=y|}}
| death_place = [[Los Angeles, California]], [[United States|U.S.]]
| nationality = American
| occupation = Actor, director, teacher, author
| alma_mater = 
| years_active = 1949&ndash;1973
| spouse =
| children =
}}
'''Don Richardson'''<ref name="bare_url">{{cite web|title=Don Richardson; Director, Acting Teacher|url=http://articles.latimes.com/1996-01-19/local/me-26523_1_zero-mostel-dramatic-arts-jewish-museum|publisher=Richard T. Schlosberg III|accessdate=30 June 2012}}</ref> (born April 30, 1918 &ndash; January 10, 1996)<ref name="imdb">{{cite web|title=Don Richardson|url=http://www.imdb.com/name/nm0724504/|accessdate=30 June 2012}}</ref> was an American actor, director, acting teacher, as well as an author.

==Career==
Richardson was an original member of the [[Group Theatre (New York)|Group Theatre]], which was the nucleus of Acting in the United States. His fellow Group Theatre members included [[Lee Strasberg]], [[Sanford Meisner]], [[Stella Adler]], and [[Uta Hagen]], although Richardson was the only student who said, "The Method isn’t what acting is about."  Richardson directed three Broadway productions and over 800 television shows,<ref name="bare_url" /> including ''[[Get Smart]]'' (TV series) (1965), ''[[One Day at a Time]]'' (TV series) (1975–1976), ''[[Lost in Space]]'' (TV series) (1966–1968), ''[[The Defenders (1961 TV series)|The Defenders]]'' (TV series) (1961–1963), and ''[[Bonanza]]'' (TV series) (1968–1971).<ref name="thehellerapproach">{{cite web|title=Approach|url=http://www.thehellerapproach.com/approach/|accessdate=30 June 2012}}</ref> His work remains in the permanent collection of [[Paley Center for Media|The Museum of Broadcasting]], [[Jewish Museum (New York)|The Jewish Museum of New York]], and [[UCLA Film and Television Archive]].<ref name="bare_url" /> Richardson taught acting at [[UCLA]], Colombia’s [[Barnard College]] and [[American Academy of Dramatic Arts]] in New York and California. He was awarded a professorship at [[Tel Aviv University]] in Israel. As an acting coach, he wrote and published the book ''Acting Without Agony: An Alternative to the Method'',<ref name="amazon">{{cite web|title=Acting without Agony: An Alternative to the Method|url=http://www.amazon.com/Acting-without-Agony-Alternative-Method/dp/0205151655|accessdate=30 June 2012}}</ref> which is still used today by his successors.<ref name="thehellerapproach_a">{{cite web|title=The Team|url=http://www.thehellerapproach.com/the-team-2/|accessdate=30 June 2012}}</ref>  Richardson's students include [[Grace Kelly]], [[Anne Bancroft]], [[Zero Mostel]], and [[John Cassavetes]].<ref name="answers">{{cite web|title=Don Richardson|url=http://www.answers.com/topic/don-richardson|accessdate=30 June 2012}}</ref>

==Personal life==
Richardson had a two-year relationship with his student [[Grace Kelly]] which began in the Autumn of 1948, despite the disapproval of her parents.<ref>"Grace : The secret lives of a princess" by James Spada. Sidgewick and Jackson. 1987</ref>

== Filmography ==

=== Directing credits ===
*''[[The Oregon Trail (TV series)|The Oregon Trail]]'' (TV series) (1977)<ref>{{cite web|title=The Oregon Trail (1976)|url=http://www.imdb.com/title/tt0075552/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[One Day at a Time]]'' (TV series) (1975–1976)<ref>{{cite web|title=One Day at a Time (1975-1984)|url=http://www.imdb.com/title/tt0072554/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Emergency!]]'' (TV series) (1974)<ref>{{cite web|title=Emergency! (1972-1979)|url=http://www.imdb.com/title/tt0068067/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Arnie (TV series)|Arnie]]'' (TV series) (1970–1972)<ref>{{cite web|title=Arnie (1970-1972)|url=http://www.imdb.com/title/tt0065274/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Bonanza]]'' (TV series) (1968–1971)<ref>{{cite web|title=Bonanza (1959-1973)|url=http://www.imdb.com/title/tt0052451/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Lancer (TV series)|Lancer]]'' (TV series) (1968–1970)<ref>{{cite web|title=Lancer (1968-1970)|url=http://www.imdb.com/title/tt0062577/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The High Chaparral]]'' (TV series) (1969–1970)<ref>{{cite web|title=The High Chaparral (1967-1971)|url=http://www.imdb.com/title/tt0061263/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Mission: Impossible]]'' (TV series) (1968)<ref>{{cite web|title=Mission: Impossible (1966-1973)|url=http://www.imdb.com/title/tt0060009/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Lost in Space]]'' (TV series) (1966–1968)<ref>{{cite web|title=Lost in Space (1965-1968)|url=http://www.imdb.com/title/tt0058824/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Custer (TV series)|Custer]]'' (TV series) (1967)<ref>{{cite web|title=Custer (1967)|url=http://www.imdb.com/title/tt0061244/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''Vacation Playhouse'' (TV series) (1966)<ref>{{cite web|title=Vacation Playhouse (1963-1967)|url=http://www.imdb.com/title/tt0056792/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Long, Hot Summer (TV series)|The Long, Hot Summer]]'' (TV series) (1965–66)<ref>{{cite web|title=The Long, Hot Summer (1965-1966)|url=http://www.imdb.com/title/tt0058823/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Munsters]]'' (TV series) (1965)<ref>{{cite web|title=The Munsters (1964-1966)|url=http://www.imdb.com/title/tt0057773/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Virginian (TV series)|The Virginian]]'' (TV series) (1964–65)<ref>{{cite web|title=The Virginian (1962-1971)|url=http://www.imdb.com/title/tt0055710/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Get Smart]]'' (TV series) (1965)<ref>{{cite web|title=Get Smart (1965-1970)|url=http://www.imdb.com/title/tt0058805/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Broadside (TV series)|Broadside]]'' (TV series) (1965)<ref>{{cite web|title=Broadside (1964-1965)|url=http://www.imdb.com/title/tt0057738/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Defenders (1961 TV series)|The Defenders]]'' (TV series) (1961–1963)<ref>{{cite web|title=The Defenders (1961-1965)|url=http://www.imdb.com/title/tt0054531/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Nurses (CBS TV series)|The Doctors and the Nurses]]'' (TV series) (1962–1963)<ref>{{cite web|title=The Doctors and the Nurses (1962-1965)|url=http://www.imdb.com/title/tt0055694/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Sam Benedict]]'' (TV series) (1963)<ref>{{cite web|title=Sam Benedict (1962-1963)|url=http://www.imdb.com/title/tt0055703/|publisher=IMDb|accessdate=5 July 2012}}</ref>
*''[[Margie (TV series)|Margie]]'' (TV series) (1961–1962)<ref>{{cite web|title=Margie (1961-1962)|url=http://www.imdb.com/title/tt0054555/|publisher=IMDb|accessdate=5 July 2012}}</ref>
*''The Chevy Mystery Show'' (TV series) (1960)<ref>{{cite web|title=The Chevy Mystery Show (1960)|url=http://www.imdb.com/title/tt0053539/|publisher=IMDb|accessdate=5 July 2012}}</ref>
*''[[Play of the Week]]'' (TV series) (1959–1960)<ref>{{cite web|title=Play of the Week (1959-1961)|url=http://www.imdb.com/title/tt0446859/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''Don Juan in Hell'' (TV movie) (1960)<ref>{{cite web|title=Don Juan in Hell (1960)|url=http://www.imdb.com/title/tt0207422/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''Lullaby'' (TV movie) (1960)<ref>{{cite web|title=Lullaby (1960)|url=http://www.imdb.com/title/tt0211491/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''The World of Sholom Aleichem'' (TV movie) (as Donald Richardson) (1959)<ref>{{cite web|title=The World of Sholom Aleichem (1959)|url=http://www.imdb.com/title/tt0217157/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''The Elgin Hour'' (TV series) (1954–1955)<ref>{{cite web|title=The Elgin Hour (1954-1955)|url=http://www.imdb.com/title/tt0203251/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Mama (TV series)|Mama]]'' (TV series) (1955)<ref>{{cite web|title=Mama (1949-1957)|url=http://www.imdb.com/title/tt0041039/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Armstrong Circle Theatre]]'' (TV series) (1954)<ref>{{cite web|title=Armstrong Circle Theatre (1950-1963)|url=http://www.imdb.com/title/tt0042074/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Motorola Television Hour]]'' (TV series) (1953–1954)<ref>{{cite web|title=The Motorola Television Hour (1953-1954)|url=http://www.imdb.com/title/tt0045415/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[Kraft Television Theatre|Kraft Theatre]]'' (TV series) (1953)<ref>{{cite web|title=Kraft theatre (1947-1958)|url=http://www.imdb.com/title/tt0039123/|publisher=IMDb|accessdate=4 July 2012}}</ref>
*''[[The Adventures of Ellery Queen]]'' (TV series) (1950–1952)<ref>{{cite web|title=The Adventures of Ellery Queen (1950-1952)|url=http://www.imdb.com/title/tt0042069/|publisher=IMDb|accessdate=4 July 2012}}</ref>

==References==
{{reflist|2}}

==External links==
* {{IMDb name|0724504}}

{{DEFAULTSORT:Richardson, Don}}
[[Category:1918 births]]
[[Category:1996 deaths]]
[[Category:Male actors from New York City]]
[[Category:American television directors]]
[[Category:Acting coaches]]
[[Category:20th-century American male actors]]