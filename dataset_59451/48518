{{Infobox scientist 
|name              = Ada Florence Remfry Hitchins
|other_names       = Mrs. John R. Stephens
|image             = 
|birth_date        = {{birth date|df=y|1891|06|26}}
|birth_place       = [[Tavistock, Devon]], England
|death_date        = {{death date and age|df=y|1972|01|04|1891|06|26 }}
|death_place       = [[Bristol, England]]
|residence         = England; [[Kenya]]
|nationality       = English
|field             = 
|work_institutions = [[University of Glasgow]]; [[University of Aberdeen]]; [[Oxford University]]; Mining and Geological Department of the Colonial Government of Kenya
|alma_mater        = [[University of Glasgow]]
|awards            = Carnegie Scholar (1914–1915)
}}

'''Ada Florence Remfry Hitchins'''  (26 June 1891 – 4 January 1972) was the principal research assistant of British chemist [[Frederick Soddy]], who won the [[Nobel Prize|Nobel prize]] in 1921 for work on [[radioactive element]]s and the theory of [[isotope]]s.<ref name=Devotion>{{cite book|first1=Marelene F. |last1=Rayner-Canham |first2= Geoffrey W. |last2=Rayner-Canham|title=A Devotion to their science : pioneer women of radioactivity|date=1997|publisher=[[Chemical Heritage Foundation]]|location=Philadelphia|isbn=9780941901154|url=https://books.google.com/books?id=AMc4n2Y7ILkC&pg=PA152&lpg=PA152|pages=152–155}}</ref><ref name=Bulletin>{{cite journal|first1=Marelene F. |last1=Rayner-Canham |first2= Geoffrey W. |last2=Rayner-Canham|title=Stefanie Horovitz, Ellen Gleditsch, Ada Hitchins, and the discovery of isotopes|journal=Bulletin for the History of Chemistry|date=2000|volume=25|issue=2|pages=103–108|url=http://www.scs.illinois.edu/~mainzv/HIST/bulletin_open_access/v25-2/v25-2%20p103-108.pdf|accessdate=9 April 2014}}</ref><ref name=Matriculation>{{cite web|last=Egan|first=Rachel|title=Graduates 2013/1913: Ada Hitchins|url=http://universityofglasgowlibrary.wordpress.com/2013/06/25/graduates-20131913-ada-hitchins/|publisher=University of Glasgow Library|accessdate=2014-04-08|date=25 June 2013}}</ref> Hitchins isolated samples from [[uranium]] ores, taking precise and accurate measurements of [[atomic mass]] that provided the first experimental evidence for the existence of different isotopes.<ref name=Life /> She also helped to discover the [[Chemical element|element]] [[protactinium]],<ref name=Life /> which [[Dmitri Mendeleev]] had predicted should occur in the [[periodic table]] between uranium and [[thorium]].<ref name="Emsley"/>

== Education ==

Ada Hitchins was born on 26 June 1891<ref name=Life /> in [[Tavistock, Devon]], England, the daughter of William Hedley Hitchins, a supervisor of customs and excise.<ref name=Ogilvie>{{cite book|last1=Ogilvie|first1=Marilyn|last2=Harvey|first2=Joy|title=The biographical dictionary of women in science|date=2000|publisher=Routledge|location=New York|isbn=0415920388|pages=1227|url=https://books.google.com/books?id=rUCUAgAAQBAJ&pg=PA1227&lpg=PA1227}}</ref> The family lived for a time in Campbelltown, Scotland, where Hitchins attended high school, graduating in 1909. From there, she went to the [[University of Glasgow]], obtaining her bachelor's degree in science, with honors, in 1913.<ref name=Matriculation /><ref name=Life/> She was awarded prizes in botany and geology, as well as being accorded special distinction for her work in chemistry.<ref name=GlasgowBio />

== A career disrupted by war and peace ==
In her final year at the University of Glasgow, Hitchins began to work with [[Frederick Soddy]]. When he moved to the [[University of Aberdeen]] as the Chair of Chemistry in 1914, Hitchins and another assistant, John A. Cranston, accompanied him.<ref name=Life /> At Aberdeen, Hitchins was a Carnegie Research Scholar, receiving a one-year appointment and monetary award given to a graduate of a Scottish institution for research and study by American philanthropist [[Andrew Carnegie]].<ref name=Devotion/>

Expectations that Soddy would establish a thriving research center at Aberdeen were disrupted by [[World War I]], as male students became soldiers, and women students were encouraged to graduate from accelerated courses so as to fill positions in industry and government.<ref name=WiC>{{cite book|last1=Rayner-Canham|first1=Marelene|last2= Rayner-Canham|first2= Geoffrey|title=Women in chemistry : their changing roles from alchemical times to the mid-twentieth century|date=1998|publisher=American Chemical Society|location=Washington, DC|isbn=9780841235229|page=166}}</ref>  Hitchins was drafted to work in the Admiralty Steel Analysis Laboratories in 1916.<ref name=Life /> After the war, women workers were displaced from their jobs by returning soldiers. Hitchins, released from the Admiralty, was able to find work at a Sheffield steel works.<ref name=Life />

In 1921, Frederick Soddy, then at the [[University of Oxford]], obtained funding to rehire Hitchins as a technical assistant.<ref name=Ogilvie /> He had recently received the Nobel prize for his work on radioactivity and isotopes. In 1922, Hitchins became his private research assistant.<ref name=Life /><ref name=Ogilvie /> She continued to work with him until 1927, when she emigrated to [[Kenya]] to be nearer to her family.<ref name=Life />

== Radioactive research ==

Hitchins worked with Soddy over a period of 15 years, which included his most productive periods of achievement. She was his primary research assistant, and the only person to work with him for a long period of time.<ref name=Devotion/> Her careful preparation of radioactive materials, and her painstaking experimental work with uranium, protactinium, and lead isotopes, made crucial contributions to the research for which Soddy received the Nobel Prize.<ref name=Life/>

=== Uranium and ionium ===

[[File:Uranium ore square.jpg|thumb | right | Uranium ore]]
When Hitchins first worked with Soddy, researchers were still searching for new [[chemical elements]], and [[isotopes]] were not yet understood.  As early as 1904, researchers had hypothesized that the decay of uranium resulted in the creation of [[radium]], but how this occurred was not clear.  In 1907, the American radiochemist [[Bertram Boltwood]] had isolated what he believed to be a new intermediate element in the decay chain between uranium and radium, "[[Isotopes of thorium#Thorium-230|ionium]]".<ref name=Devotion/> Researchers eventually determined that ionium was actually an isotope of [[thorium]], <sup>230</sup>Th.

Soddy asked Hitchins to investigate ionium.  She selectively extracted uranium from ore samples to create purified uranium preparations and established a [[half-life]] for ionium. Her research also showed that there was a steady rate of increase in the amount of radium in her uranium solutions, the first direct experimental evidence that radium was formed by the decay of uranium. Her results were published in 1915.<ref name=Ionium>{{cite journal|last1=Soddy|first1=Frederick|last2=Hitchins|first2= A. F. R.|title=XVII. The relation between uranium and radium.—Part VI. The life-period of ionium|journal=Philosophical Magazine|series=6|date=August 1915|volume=30|issue=176|pages=209–219|doi=10.1080/14786440808635387}}</ref>

=== Atomic weight of "thorium" lead ===

Hitchins helped to determine the atomic weight of lead based on measurements of radioactive ores, work that was important in developing an understanding of isotopes.<ref name="Devotion"/> The samples of distilled lead which Hitchins prepared from Ceylon [[thorite]] were used by Frederick Soddy and supplied by him to [[Otto Hönigschmid]], who did important  work confirming that the atomic weight of thorium lead is higher than that of common lead.<ref name=Bulletin /><ref name=ThoriumLead />

Soddy indicated that Hitchins also worked on the actual analyses, in his published report of 1917: "According to analyses by Miss A. F. R. Hitchins and myself, the 20 kilos of selected thorite worked upon contained 0-4 per cent, of lead, 57 per cent, of thorium, 1-03 per cent, of uranium, and 0-5 c.c. of helium per gram."<ref name=ThoriumLead>{{cite journal|last=Soddy|first=Frederick|title=The Atomic Weight of "Thorium" Lead|journal=Nature|date=15 February 1917|volume=98|issue=2468|pages=469–469|doi=10.1038/098469a0|url=http://babel.hathitrust.org/cgi/pt?q1=Soddy%20Hitchins;id=mdp.39015038751890;view=plaintext;seq=793;start=1;size=10;page=search;num=469|accessdate=12 April 2014|bibcode=1917Natur..98Q.469S}}</ref> This work proved that atomic weight was not a constant. Chemically pure elements could be mixtures of isotopes with differing atomic weights.

=== Actinium and protactinium ===

[[File:Uraninite-39029.jpg|thumb | right |Uraninite ore, a source of protactinium]]
John A. Cranston, who had also come from Glasgow to Aberdeen with Soddy as a research assistant, was drafted in March 1915.  Hitchins continued Cranston's research before she herself was drafted for war work in 1916.  This research resulted in the successful identification of a new element in the decay chain between uranium-235 and actinium, later named [[protactinium]]. The discovery of protactinium completed the early version of the periodic table proposed by Dmitri Mendeleev, who predicted the existence of an element between thorium and uranium in 1871.<ref name="Emsley">{{cite book|title = Nature's Building Blocks: An A&ndash;Z Guide to the Elements|last = Emsley|first = John|publisher = Oxford University Press|year = 2003|location = Oxford, England, UK|isbn = 0-19-850340-7|chapter = Protactinium|pages = 347–349|edition=2nd|url = https://books.google.com/books?id=j-Xu07p3cKwC&pg=PA348}}</ref>  The same isotope, <sup>231</sup>Pa, was independently discovered around the same time by [[Otto Hahn]] and [[Lise Meitner]].

Soddy and Cranston published their paper in 1918. Although Hitchins was not included as a co-author, Soddy gave Hitchins significant credit for the contributions she had made to the research:
<blockquote>The experiments were undertaken when the course of the disintegration of uranium and its connection with radium was quite obscure, but the experiments afford valuable data, which has never been properly discussed in the light of recent discoveries, on the possible modes of origin of actinium. With regard to the new work, in the absence of one of us on military service since 1915, the experiments were continued for a time by Miss Ada Hitchens, [sic] B.Sc, Carnegie Research Scholar, until she also left to engage in war duties. Her valuable assistance has contributed very materially to the definiteness of the conclusions that it has 
been possible to arrive at.<ref name=Parent>{{cite journal |last=Soddy|first=F.|author2=Cranston, J. A.|title=The Parent of Actinium |journal=Proceedings of the Royal Society A: Mathematical, Physical and Engineering Sciences|date=1 June 1918| volume=94|issue=662 |pages=384–404|  doi=10.1098/rspa.1918.0025|url=https://archive.org/details/philtrans02410094|accessdate=10 April 2014|bibcode=1918RSPSA..94..384S}}</ref></blockquote>

=== Measuring radioactivity ===

In her early work with Soddy, Hitchins helped to prepare radium standards for the calibration of [[gold-leaf electroscope]]s, used to measure radioactivity.<ref name="Devotion"/>

After she returned to work with Soddy in 1921, Hitchins further refined measurements of the half-life of ionium, and determined ratios of thorium isotopes in mineral samples. She also developed methods for extracting radioactive elements from minerals and ores.<ref name="Devotion"/>

Soddy wrote of her: 
<blockquote>I regard Miss Hitchins as an exceedingly accomplished chemist with a wide knowledge and experience of difficult chemical and mineral analysis. It is much to her credit that in her work with extremely rare and difficultly obtainable materials she has never had any accident or lost any of the material.<ref name="Devotion"/></blockquote>

== Later life ==

In 1927 Hitchins moved to [[Kenya]], to join other members of her family who had emigrated there. Frederick Soddy wrote to the [[British Colonial Office]], recommending her for a government position.<ref name=Devotion />  Hitchins was employed as a Government Assayer and Chemist in the Mining and Geological Department of the Colonial Government until 1946.<ref name=Life /> After her retirement, the department's annual report said of her that "she filled the post of Chemist and Assayer gaining an outstanding reputation for accuracy and complete reliability, and her loss was keenly felt by the mining industry."<ref name=Devotion />

In 1946, Hitchins married a farmer, John Ross Stephens<ref name=Life>{{cite book|first1=Marelene F. |last1=Rayner-Canham |first2= Geoffrey W. |last2=Rayner-Canham|title=Chemistry was their life : pioneer British women chemists, 1880&ndash;1949|date=2008|publisher=Imperial College Press|location=London|isbn=186094986X|pages=279–281|url=https://books.google.com/books?id=yD_XlVSwJbcC&pg=PA279&lpg=PA279}}</ref> (also spelled Rees<ref name=GlasgowBio>{{cite web|title=Ada Florence Remfry Hitchins|url=http://www.universitystory.gla.ac.uk/biography/?id=WH24585&type=P&o=&start=0&max=20&l=|publisher=University of Glasgow|accessdate=9 April 2014}}</ref>).

Hitchins died in Bristol, England, on January 4, 1972.<ref name=Life />

== References ==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Hitchins, Ada}}
[[Category:1891 births]]
[[Category:1972 deaths]]
[[Category:English chemists]]
[[Category:Experimental physicists]]
[[Category:Nuclear chemists]]
[[Category:People from Devon]]
[[Category:Radioactivity]]
[[Category:Women chemists]]
[[Category:20th-century women scientists]]