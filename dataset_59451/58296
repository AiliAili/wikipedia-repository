{{Infobox journal
| title  = Journal of Medical Ethics
| cover  = [[File:Journal of Medical Ethics.gif]]
| editor = Julian Savulescu
| discipline = [[Medical ethics]]
| abbreviation = J. Med. Ethics
| publisher = [[BMJ Group]]
| country =
| frequency = Monthly
| history = 1975-present
| openaccess = 
| license = 
| impact = 1.764
| impact-year = 2015
| website = http://jme.bmj.com/
| link1  = 
| link1-name = 
| link2  = 
| link2-name =
| JSTOR  = 
| OCLC  = 1838186
| LCCN  = 
| CODEN  = 
| ISSN  = 0306-6800
| eISSN  = 1473-4257
}}
The '''''Journal of Medical Ethics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[bioethics]] established in 1975.<ref name=about>{{cite web| url=http://jme.bmj.com/site/about/index.xhtml |title=About |publisher=Journal of Medical Ethics |accessdate=2010-01-14}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.764, ranking it 5th out of 51 journals in the category "Ethics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Ethics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== Editors ==
[[Julian Savulescu]] ([[University of Oxford]]) has been [[editor-in-chief]] since 2011.<ref>{{cite web| url=http://jme.bmj.com/site/about/edboard.xhtml |title=Editorial board |publisher=Journal of Medical Ethics |accessdate=2010-01-14}}</ref> Previous editors have been [[Soren Holm|Søren Holm]] ([[Cardiff University]]) and [[John Harris (bioethicist)|John Harris]] ([[University of Manchester]], 2001-2011); [[Raanan Gillon]] ([[Imperial College London]], 1980-2001);<ref>{{cite web|url=http://www1.imperial.ac.uk/medicine/people/raanan.gillon/|title=Home - Emeritus Professor Raanan Gillon|work=imperial.ac.uk}}</ref> and Alastair V. Campbell ([[University of Edinburgh]], 1975-1980, founding editor).<ref>Reynolds, L.A., and E.M. Tansey, eds. Medical Ethics Education in Britain, 1963-1993. London: UK: Wellcome Trust Centre for the History of Medicine at UCL, 2007: p.198. Available from: http://eprints.ucl.ac.uk/14885/</ref>

== See also ==
* [[List of ethics journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://jme.bmj.com/}}

[[Category:Bioethics journals]]
[[Category:Publications established in 1975]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:BMJ Group academic journals]]


{{ethics-journal-stub}}