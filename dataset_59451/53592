{{Use dmy dates|date=August 2014}}
{{Infobox accounting body
| name_in_local_language= <!--Give name in other languages other than English here -->
| image                 = ICAEW_logo.jpg
| image_border          =
| alt                   = <!-- alt text; see [[WP:ALT]] -->
| image_caption         =
| abbreviation          = ICAEW
| motto                 =
| predecessor           = Incorporated Society of Liverpool Accountants,
Institute of Accountants in London and three others
| formation             = {{Start date and years ago|df=yes|1880|05|11}}
| legal_status          = [[Royal Charter|Chartered]] body
| objective             = <!-- focus as e.g. education and licensing of professional accountants etc -->
| headquarters          = <!--city of the headquarters in the format: City, {{flag|country} -->
| coords                = <!-- Coordinates of headquarters using {{coord}} -->
| region_served         = <!-- Name of the country or regional block or global -->
| membership_num        = 147,000<ref>[http://www.icaew.com/en/about-icaew/who-we-are ICAEW Who we are]</ref>
| students_num          = <!-- No of students -->
| members_designations  = ACA (Associate) and FCA (Fellow)
| language              = <!-- official languages -->
| highest_officer_title = President
| highest_officer_name  = Hilary Lindsay EdD MBA FCA<ref>[http://www.icaew.com/en/about-icaew/who-we-are/governance/council/lindsay-hilary Hilary Lindsay biography]</ref>
| second_officer_title  = Deputy President
| second_officer_name   = Nick Parker<ref>[http://www.icaew.com/en/about-icaew/who-we-are/governance/council/parker-nick Nick Parker biography]</ref>
| third_officer_title   = Vice President
| third_officer_name    = Paul Aplin<ref>[http://www.icaew.com/en/about-icaew/who-we-are/governance/council/aplin-paul]</ref>
| fourth_officer_title  = <!-- position of the fourth ranking officer like Treasurer etc.. -->
| fourth_officer_name   = <!-- name of the above person -->
| administering_organ   = Council
| IFAC_membership_date  = 7 October 1977
| additional_data_name1 = <!-- Optional data1 name -->
| additional_data1      = <!-- Optional data1 -->
| additional_data_name2 = <!-- Optional data2 name -->
| additional_data2      = <!-- Optional data2 -->
| additional_data_name3 = <!-- Optiona3 data1 name -->
| additional_data3      = <!-- Optional data3-->
| num_staff             =
| website               = {{url|icaew.com}}
| remarks               =
}}

The '''Institute of Chartered Accountants in England and Wales''' ('''ICAEW''') was established by [[royal charter]] in 1880.<ref name=Perks16>{{cite book| last=Perks | first=R. W. | year=1993 |title=Accounting and Society | publisher=[[Chapman & Hall]] |location=London |ISBN=0-412-47330-5 |page=16 }}</ref> It has over 147,000 members. Over 15,000 of these members live and work outside the UK. In 2015, 8,256 students joined ICAEW - the highest ever figure. 82 of FTSE 100 (the leading UK) companies have an ICAEW Chartered Accountant on the board.

==Overview==
The Institute is a member of the [[Consultative Committee of Accountancy Bodies]] (CCAB), formed in 1974 by the major accountancy professional bodies in the UK and Ireland.  The fragmented nature of the accountancy profession in the UK is in part due to the absence of any legal requirement for an accountant to be a member of one of the many Institutes, as the term ''accountant'' does not have legal protection.  However, a person must belong to the ICAEW, ICAS or CAI to hold themselves out as a ''[[chartered accountant]]'' in the UK (although there are other chartered bodies of [[British qualified accountants]] whose members are likewise authorised to conduct restricted work such as [[financial audit|auditing]]).

The ICAEW has two offices in the UK; the main one is in [[Moorgate]], London and the other in [[Central Milton Keynes]], in the newly-built [[Hub:MK]] complex. It also has offices in Belgium (Europe Region), China (Greater China Region), Hong Kong, Indonesia, Vietnam, Malaysia, Singapore and the United Arab Emirates (Middle East, Africa and South East Asia Region).<ref>{{cite web |url=http://www.icaew.com/index.cfm/route/169318/icaew_ga/en/Home/Press_and_policy/Press_and_policy_home/ICAEW_opens_Middle_East_regional_office |title=ICAEW opens Middle East regional office |publisher= ICAEW }}</ref><ref name=china1>[http://www.accountancyage.com/aa/news/2039893/icaew-pushes-regional-expansion-china ICAEW pushes regional expansion in China], ''[[Accountancy Age]]'', 4 April 2011</ref>

==History==

===Early years===
Until the mid-nineteenth century the role of accountants in England and Wales was restricted to that of bookkeepers in that accountants merely maintained records of what other business people had purchased and sold.  However, with the growth of the [[limited liability company]] and large scale manufacturing and logistic in [[Victorian era|Victorian]] [[United Kingdom of Great Britain and Ireland|Britain]] a demand was created for more technically proficient accountants to deal with the increasing complexity of accounting transactions dealing with depreciation of assets, inventory valuation and the Companies legislation being introduced.

To improve their status and combat criticism of low standards, accountants in the cities of Britain formed [[professional bodies]]. The ICAEW was formed from the five of these associations that existed in England prior to its establishment by [[Royal Charter]] in May 1880.<ref name=Perks16/>

# The '''Incorporated Society of Liverpool Accountants''', formed in January 1870;<ref name=Perks16/>
# The '''Institute of Accountants in London''' was formed in November 1870,<ref name=Perks16/> comprising 37 members under the leadership of William Quilter.  In 1871, standards for membership were established with new members having to show knowledge and aptitude through successfully passing an oral examination. Initially the London Institute restricted its membership to that city, but as other institutes were established elsewhere (for example, in [[Manchester]] and [[Sheffield]]) it was decided to remove this restriction and as such in 1872 it simply became known as the Institute of Accountants to reflect its new national coverage;
# The '''Manchester Institute of Accountants''', formed in February 1871;<ref name=Perks16/>
# The '''Society of Accountants in England''' (1872);<ref name=Perks16/>
# The '''Sheffield Institute of Accountants''' (1877).<ref name=Perks16/>

The headquarter of the Institute, Chartered Accountants' Hall, in the City of London, was designed in the Italian Renaissance style by [[John Belcher (architect)|John Belcher]] in 1890. It was built by [[Trollope & Colls|Colls & Sons]].<ref>[http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=074-btrl&cid=0#0 Trollope & Colls at the National Archives]</ref> It is widely regarded as one of the finest examples of [[Victorian Baroque architecture]]. [[William Whitfield|Sir William Whitfield]] designed the 1964–70 extension and new entrance.<ref name=OI>{{cite web|title=Sir William Whitfield|url=http://oxfordindex.oup.com/view/10.1093/oi/authority.20110803122322799|website=Oxford Index|publisher=OUP|accessdate=30 November 2014}}</ref>

In 1948, the institute received a Supplemental Charter. In 1957, the ICAEW merged with the Society of Incorporated Accountants (founded in 1885 as the Society of Incorporated Accountants and Auditors).<ref name=Perks16/>

===Recent developments===

In 2005 the ICAEW sought to merge with the [[Chartered Institute of Management Accountants]] (CIMA) and the [[Chartered Institute of Public Finance and Accountancy]] (CIPFA). However, this project proved unsuccessful, and wasted a considerable amount of money.  The Institute also announced at this time that it was considering dropping the reference to England and Wales in its title to become the Institute of Chartered Accountants.  However, this plan was also withdrawn following objections from the [[Institute of Chartered Accountants of Scotland]].

The Institute introduced a new syllabus in 2007. In order to make it more appealing to prospective students the mandatory examinations will become more flexible based on a modular structure. In addition to paper based assessments, there are now computer based assessments of objective test questions (multiple choice).

In 2017, ICAEW launched a new brand identity - 'A world of strong economies', with an updated logo and brand positioning. <ref>http://www.icaew.com/about-icaew/who-we-are/a-world-of-strong-economies</ref>

==Admission to membership==
===Examinations and practical work experience===
To be admitted to membership of the ICAEW, applicants must generally complete 450 days of relevant work experience (training) and pass a series of examinations. During the training, the candidate will also need to display professional ethics and skepticism along with showing a commitment towards continuous professional development, which must be maintained even once the qualification has been obtained. This pathway is defined as the '''ACA qualification'''. It is common for accountancy employers in the UK, such as the [[Big Four (audit firms)|Big Four]], to offer the ACA as part of their training contract for new joiners.

The work experience lasts between three and five years and must be with an employer or employers approved by the Institute for training; there are more than 2,850 authorised employers around the world.<ref>{{Cite web|url=http://www.icaew.com/en/qualifications-and-programmes/aca-evolved/practical-work-experience|title=Practical work experience: overview|publisher=ICAEW|accessdate=14 August 2014}}</ref> The examinations are in three stages. The certificate level consists of six modules examined via e-assessments; the professional stage consists of six written papers while the advanced stage, usually taken on the final year of training, consists of two technical integration papers and a case study.<ref>{{Cite web|url=http://www.icaew.com/en/qualifications-and-programmes/aca-evolved/exams|title=15 accountancy, finance and business modules|publisher=ICAEW|accessdate=14 August 2014}}</ref>

Since 2014, the ICAEW has been offering in parallel with the [[Chartered Institute of Taxation]] (CIOT) a joint ACA/CTA route which allows tax students to achieve both prestigious qualifications in a shorter length of time.<ref>{{Cite web|url=http://www.tax.org.uk/students_qualifications/Joint+Programme|title=Chartered Tax Adviser and ICAEW Chartered Accountant Joint Programme|publisher=CIOT|accessdate=14 August 2014}}</ref>

The full ACA training programme is also available in Cyprus, Greece, Malaysia, Mauritius, Romania and the [[Arab states of the Persian Gulf|Gulf states]].

===Other ways to membership and affiliations===

Members of equivalent bodies in other [[European Economic Area]] countries and [[Switzerland]] may also be admitted to membership after passing an aptitude test, provided they are a citizen of an [[European Economic Area|EEA]] state or [[Switzerland]].

The Institute has mutual recognition agreements in place with:
* [[Canadian Institute of Chartered Accountants]]
* [[Hong Kong Institute of Certified Public Accountants]]
* [[Institute of Chartered Accountants of Australia]]
* [[Institute of Chartered Accountants of Bangladesh]]
* [[Institute of Chartered Accountants of India]]
* [[Institute of Chartered Accountants in Ireland]]
* [[Institute of Chartered Accountants of Pakistan]]
* [[Institute of Chartered Accountants of Scotland]]
* [[Institute of Chartered Accountants of Zimbabwe]]
* [[New Zealand Institute of Chartered Accountants]]
* [[South African Institute of Chartered Accountants]]

Starting from 8 January 2010, ICAEW introduced a new "Pathways to Membership" programme whereby it offers the ACA designation on passing its Examination of Experience and sponsorship by ICAEW member or under ICAEW's ATE training environment, to members (qualified through normal examination and training route) of the following accounting institutes:

* [[American Institute of Certified Public Accountants]] (AICPA)
* [[Association of Chartered Certified Accountants]] (ACCA)
* [[Chartered Institute of Management Accountants]] (CIMA)
* [[Chartered Institute of Public Finance and Accountancy]] (CIPFA)
* [[CPA Australia]]
* [[Malaysian Institute of Certified Public Accountants]] (MICPA)

However, AICPA and MICPA members who join the ICAEW under the Pathways to Membership scheme must maintain their home body membership.

ICAEW has signed Memoranda of Understanding (MOU) with accounting bodies in overseas countries.  These bodies are The [[Institute of Chartered Accountants of Bangladesh]] (ICAB), [[Institute of Chartered Accountants of India]] (ICAI), [[Institute of Chartered Accountants of Pakistan]] (ICAP) and [[Institute of Certified Public Accountants of Singapore]] (ICPAS).  There is also a joint scheme in place to enable members of the [[Chinese Institute of Certified Public Accountants]] (CICPA) 
and the [[Hong Kong Institute of Certified Public Accountants]] to qualify as ACA with ICAEW.<ref name=china1/>
ICAEW is an affiliate of the [[Institute of Chartered Accountants of the Caribbean]].<ref>{{cite web
 |url=http://www.icac.org.jm/index.php?option=com_content&task=view&id=14&Itemid=28
 |title=Members And Affiliates
 |publisher=ICAC
 |accessdate=2011-07-01}}</ref>

==Membership categories==
Members have the designation ACA (Associate Chartered Accountant) or FCA (Fellow Chartered Accountant) after their name.<ref name=Fellowship>{{cite web|title=Fellowship|url=http://www.icaew.com/en/members/membership-information/fellowship|publisher=ICAEW|accessdate=11 January 2016}}</ref>

Fellowship is intended to designate those who have achieved a higher level of professional experience, and to raise more money through charging a higher subscription.   It is awarded automatically (previously on application) to those members who have attained at least ten years of membership and who, at the date of application, have complied with the Institute's requirements on continuing professional development in the preceding three years and have no outstanding disciplinary charges against them.<ref name=Fellowship/>

==Faculties==
The ICAEW has seven faculties, each run by an in-house team working together with members who are experts in their particular sector:<ref>{{cite web|title=Join a faculty|url=http://www.icaew.com/en/join-us/join-a-faculty|publisher=ICAEW|accessdate=11 January 2016}}</ref>

# Audit and Assurance
# Corporate Finance
# Finance and Management
# Financial Reporting
# Financial Services
# Information Technology
# Tax

The Tax Faculty was the first to be formed in 1990; The monthly TAXline publication started in 1991, and an annual Technical
Review (now Tax Planning) was first published in October 1992. The Tax Faculty joined the [[Confédération Fiscale Européenne]] (CFE) in 2001.

==District societies==
The Institute has 25 district societies, the largest of which is the London Society of Chartered Accountants (LSCA) with over 31,000 members. The LSCA has often taken the lead in making proposals for change,<ref>{{cite web |url=http://www.icaew.com/index.cfm/route/135562/icaew_ga/en/Home/ICAEW_worldwide/London_district_society/London_Society_of_Chartered_Accountants  |title=London Society of Chartered Accountants: history and purpose |accessdate= 2010-12-08 |author= |date= |work= |publisher= ICAEW |archiveurl=http://www.webcitation.org/query?url=http%3A%2F%2Fwww.icaew.com%2Findex.cfm%2Froute%2F129461%2Ficaew_ga%2Fen%2FHome%2FICAEW_worldwide%2FLondon_district_society%2Fhistory_and_purpose&date=2010-12-08 |archivedate=8 December 2010  }}</ref> and was instrumental in the formation of the influential [[Hundred Group]] of [[finance director]]s.

==See also==
* [[British qualified accountants]]

==References==
{{Reflist}}

==External links==
* [http://www.icaew.com/ ICAEW official website]
* [https://web.archive.org/web/20111113154007/http://www.frc.org.uk/pob/publications/index.cfm?mode=list&cID=1 Key Facts and Trends in the UK Accountancy Profession], annual publication by the [[Professional Oversight Board]]

{{IFAC Members}}

{{DEFAULTSORT:Institute Of Chartered Accountants in England and Wales}}
[[Category:1880 establishments in England]]
[[Category:Organizations established in 1880]]
[[Category:Accounting in the United Kingdom]]
[[Category:Professional associations based in the United Kingdom]]
[[Category:Professional accounting bodies|England and Wales]]
[[Category:Business organisations based in London]]
[[Category:Finance in England]]