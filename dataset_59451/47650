'''Economy-wide material flow accounts (EW-MFA)''' is a framework to compile statistics linking flows of materials from [[natural resource]]s to a [[national economy]].<ref name="Eurostat2009"/> EW-MFA are descriptive statistics, in physical units such as tonnes per year.

EW-MFA is consistent with the principles and system boundaries of the [[United Nations System of National Accounts|System of National Accounts (SNA)]] and follows the residence principle.<ref>EC, IMF, OECD, UN & World Bank [http://unstats.un.org/unsd/nationalaccount/docs/SNA2008.pdf  “System of National Accounts 2008”]. European Commission, International Monetary Fund, Organisation for Economic Co-operation and Development, United Nations and World Bank, New York, Dec. 2009, 1993, lvi + 662 pp.</ref> This means that EW-MFA is also a part of the [[System of Integrated Environmental and Economic Accounting|System of Integrated Environmental and Economic Accounting (SEEA)]].<ref>Handbook of National Accounting System of Integrated Environmental and Economic Accounting 2003, UN, EC, IMF, OECD and World Bank [http://unstats.un.org/unsd/envaccounting/seea2003.pdf "System of Integrated Environmental and Economic Accounting"], United Nations, European Commission, International Monetary Fund, Organisation for Economic Co-operation and Development and World Bank 2003, 598 pp.</ref>

== Scope ==
The underlying definition of economy-wide material flow accounts includes statistics on the overall material inputs into national economies, the changes of material stock within the economic system and the material outputs to other economies or to the environment. Statistics on EW-MFA cover all solid, gaseous, and liquid materials, except for water and air. However, water in products is included. EW-MFA includes statistics on material flows crossing the national (geographical) border, i.e. imports and exports.<ref name="Eurostat2009" >Eurostat [http://epp.eurostat.ec.europa.eu/portal/page/portal/environmental_accounts/documents/Eurostat%20MFA%20compilation%20guide%20for%202009%20reporting.pdf "Economy Wide Material Flow Accounts: Compilation Guidelines for reporting to the 2009 Eurostat questionnaire"], Eurostat, 125 pp.</ref>

EW-MFA strives to produce a [[mass balance]] of material flows. It systematically categorises material input and output flows crossing the functional border between economy (technosphere, anthroposphere) and environment. Mass balances are defined as "...on the first law of thermodynamics (called the law of conservation of matter), which states that matter (mass, energy) is neither created nor destroyed by any physical process".<ref>, OECD [http://www.oecd.org/dataoecd/46/48/40485853.pdf " Economy-wide MFA (EMFA)"], OECD, 164 pp.</ref>

==Interpreting the statistics==
In principle, the statistics will show which countries are dependent on others for natural resources and which are major exporters of natural resources. The statistics also show if a countries production is sustainable, i.e. whether the economy of a country can produce more products using fewer natural resources.

In the European Union between 2000 and 2007, resource productivity increased by almost eight percent.<ref>[http://epp.eurostat.ec.europa.eu/statistics_explained/index.php/Material_flow_accounts "Eurostat MFA explained"].</ref> Resource productivity of the EU is expressed by the amount of [[gross domestic product]] (GDP) generated per unit of material consumed (Domestic Material Consumption, see below), in other words GDP / DMC in euro per kg. This means that less material was consumed in order to produce the same amount of products in the EU. However, breaking down the components of the index it is seen that both GDP and DMC are increasing, only not equally fast.

== History ==
When the [[European Council]] met in Helsinki in December 1999, part of the agenda was dedicated to establishing an understanding of how economies are dependent on the use of natural resources and that these resources are not in limitless supply.<ref>Eurostat [http://epp.eurostat.ec.europa.eu/cache/ITY_OFFPUB/KS-AO-01-002/EN/KS-AO-01-002-EN.PDF “Material use indicators for the European Union, 1980–1997 No. 2/2001/B/2 ”]. Wuppertal Institute, Stefan Bringezu and Helmut Schütz, Luxembourg, March. 2011, 109 pp.</ref>

The following year, [[Eurostat]], together with the [[Wuppertal Institute for Climate, Environment and Energy|Wuppertal Institute]] and national statistical offices in Europe, developed the first statistical guideline for how to compile statistics and subsequent indicators on material flows.

== Implementing EW-MFA ==
There is a link between the [[System of Integrated Environmental and Economic Accounting|System of Integrated Environmental and Economic Accounting (SEEA)]] and EW-MFA. Statistics are based on the same principles (the residence principle of the SNA) and thus become the EW-MFA a sub-component of the SEEA. The EW-MFA links the environment to the economy through the flows of materials extracted, processed and traded.

===Compiling the statistics===
The only international data collection on EW-MFA is conducted through [[Eurostat]]. In 2011 the [[European Council]] and [[European Parliament]] passed a statistical regulation for the compilation of annual statistics on material flows.<ref>Regulation (EU) No 691/2011 of the European Parliament and of the Council of 6 July 2011 on European environmental economic accounts, Official Journal of the European Union L 192 Volume 54 22 July 2011, [http://eur-lex.europa.eu/JOHtml.do?uri=OJ:L:2011:192:SOM:EN:HTML "Regulation (EU) No 691/2011 of the European Parliament and of the Council of 6 July 2011 on European environmental economic accounts"], Official Journal of the European Union, EUR-LEX, L 192 Volume 54 22 July 2011.</ref>

Most European statistical offices compile the statistics on EW-MFA through the use of existing statistics. Trade statistics, some agricultural statistics and other sources are used in combination to create EW-MFA statistics.

===Compiling the indicators===
The statistics on EW-MFA are usually combined in order to create indicators. 
The definitions explained below are extracted from the work of Eurostat and are applied by the national statistical officies who are following the framework of EW-MFA.

Input side: DE, DMC, and DMI
Output side: DPO

'''Direct Material Consumption (DMC)''' is defined as the total amount of material directly used in an economy, i.e. it equals domestic extraction plus imports minus exports.<ref name="Eurostat2009"/> DMC does not include upstream hidden flows related to imports and exports of raw materials and products.<ref name="Economy-wide material flows 2000 and 2007" >Eurostat [http://epp.eurostat.ec.europa.eu/cache/ITY_OFFPUB/KS-SF-11-009/EN/KS-SF-11-009-EN.PDF  “Economy-wide material flows, Statistics in Focus 9/2011”]. Eurostat, Hass, J., Popescu, C., Luxembourg, March. 2011, 8 pp.</ref>

'''Domestic Material Input (DMI)''' summarizes domestic extraction of reources and the imports, i.e. all materials which are of economic value and are used in production and consumption activities, except balancing items.<ref name="Eurostat2009"/> DMI is not additive across countries. Due to the inlclusion of trade within the EU double counting would occur if one would add several countries together.<ref name="Economy-wide material flows 2000 and 2007"/>

'''Physical trade balance (PTB)''' equals physical imports minus physical exports. This means that in relation to monetary trade balances which is exports minus imports) the flows are the reverse. It measures the fact that in economies money and goods move in opposite direction. A physical trade surplus indicates a net import of materials, whereas a physical trade deficit indicates a net export.<ref name="Eurostat2009"/>

'''Net Additions to Stock (NAS)''' measures the ‘physical growth of the economy’, i.e. the quantity (weight) of new construction materials used in buildings and other infrastructure, and materials incorporated into new durable goods such as cars, industrial machinery, and household appliances. Materials are added to the economy’s stock each year (gross additions), and old materials are removed from stock as buildings are demolished, and durable goods.<ref name="Eurostat2009"/>

'''Domestic processed output (DPO)''' measures the total weight of materials which are released back to the environment after having been used in the domestic economy. These flows occur at the processing, manufacturing, use, and final disposal stages of the
production-consumption chain. Included in DPO are emissions to air, industrial and household wastes deposited in controlled and uncontrolled landfills, material loads in wastewater and materials dispersed into the environment as a result of product use
(dissipative flows). Recycled material flows in the economy (e.g. of metals, paper, glass) are not included in DPO.<ref name="Eurostat2009"/>

==See also==

{{Portal|Business and economics|Ecology}}

{{columns-list|3|
* [[Anthropogenic metabolism]]
* [[Environmental accounting]]
* [[Green accounting]]
* [[Industrial metabolism]]
* [[Material flow accounting]]
* [[Material flow analysis]]
* [[Social metabolism]]
* [[System of Integrated Environmental and Economic Accounting]]
* [[United Nations System of National Accounts]]
* [[Urban metabolism]]
}}

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.statcan.gc.ca/cgi-bin/imdb/p2SV.pl?Function=getSurvey&SDDS=5115&lang=en&db=imdb&adm=8&dis=2 Canadian System of Environmental and Resource Accounts – Material and Energy Flow Accounts (MEFA)] 
* [http://epp.eurostat.ec.europa.eu/statistics_explained/index.php/Material_flow_accounts Economy-Wide Material Flow Accounts at Eurostat] 
* [http://www.env.go.jp/en/recycle/smcs/material_flow/2006_en.pdf Material Flows in Japan]

{{Statistics|state=collapsed}}

[[Category:Environmental statistics]]
[[Category:Statistical data agreements]]
[[Category:Official statistics]]
[[Category:Articles created via the Article Wizard]]