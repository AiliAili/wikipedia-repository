{{italic title}}
[[File:Banu musa mechanical.jpg|thumb|right|Drawing of ''Self trimming lamp'' in [[Ahmad ibn Mūsā ibn Shākir]]'s treatise on  mechanical devices. The manuscript was written in [[Arabic]].]]

The '''''Book of Ingenious Devices''''' ([[Arabic language|Arabic]]: كتاب الحيل ''Kitab al-Hiyal'', literally: "The Book of Tricks") was a large illustrated work on [[mechanical device]]s, including [[Automaton|automata]], published in 850 by the three [[Iran|Iranian]] brothers known as the [[Banu Musa]] (Ahmad, Muhammad and Hasan bin Musa ibn Shakir) working at the [[House of Wisdom]] (''Bayt al-Hikma'') in [[Baghdad]], [[Iraq]], under the [[Abbasid Caliphate]].<ref>Dimarogonas, 2000, p. 15.</ref> The book described about one hundred devices and how to use them.<ref name=Hill-44>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=44}}</ref>

==Overview==
The book was commissioned by the [[Abassid]] [[Caliph]] of [[Baghdad]],also made by Al-Jazari, [[Al-Ma'mun|Abu Jafar al-Ma'mun ibn Harun]] (786-833), who instructed the Banu Musa to acquire all of the [[Hellenistic civilization|Hellenistic]] texts that had been preserved by monasteries and by scholars during the decline and fall of [[Roman Empire|Roman]] civilization.<ref>Rosheim, 1994, p. 9.</ref> The [[Banū Mūsā]] brothers invented a number of [[automaton|automata]] (automatic [[machine]]s) and mechanical devices, and they described a hundred such devices in their ''Book of Ingenious Devices''.<ref name=Hill-44/>

Some of the devices described in the ''Book of Ingenious Devices'' were inspired by the works of [[Hero of Alexandria]]<ref>Bunch, 2004, p. 107</ref> and [[Philo of Byzantium]], as well as [[Science and technology in Iran|ancient Persian]], [[History of science and technology in China|Chinese]] and [[History of Indian science and technology|Indian engineering]].<ref name=Hill-21/> Many of the other devices described in the book, however, were [[Inventions in medieval Islam|original inventions]] by the Banu Musa brothers.<ref>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|pages=13, 19, 23}}</ref> While they took Greek works as a starting point, the Banu Musa went "well beyond anything achieved by Hero or Philo." Their preoccupation with [[automatic control]]s distinguishes them from their Greek predecessors, including the Banu Musa's "use of self-operating [[valve]]s, timing devices, delay systems and other concepts of great ingenuity."<ref name=Hill-23>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=23}}</ref> Many of their innovations involved subtle combinations of [[pneumatics]] and [[aerostatics]].<ref name=Hill-23/> The closest modern parallel to their work lies in [[control engineering]] and pneumatic instrumentation.<ref name=Hill-24/>

In turn, the Banu Musa's work was later cited as an influence on the work of [[Al-Jazari]], who produced a similarly titled book in 1206.<ref>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=22}}</ref> Given that the ''Book of Ingenious Devices'' was widely circulated across the [[Muslim world]], some of its ideas may have also reached Europe through [[Al-Andalus|Islamic Spain]], such as the use of automatic controls in later European machines or the use of conical valves in the work of [[Leonardo da Vinci]].<ref name=Hill-24>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=24}}</ref>

==Mechanisms and components==
===Automatic controls===
The Banu Musa brothers described a number of early [[automatic control]]s.<ref name=Hassan>[[Ahmad Y Hassan]], [http://www.history-science-technology.com/Articles/articles%2071.htm Transfer Of Islamic Technology To The West, Part II: Transmission Of Islamic Engineering]</ref> Two-step level controls for fluids, an early form of discontinuous [[variable structure control]]s, was developed by the Banu Musa brothers.<ref>{{citation|title=Soft variable-structure controls: a survey|author=J. Adamy & A. Flemming|journal=Automatica|volume=40|issue=11|date=November 2004|publisher=[[Elsevier]]|pages=1821–1844|doi=10.1016/j.automatica.2004.05.017}}</ref> They also described an early [[Control theory|feedback controller]].<ref name=Mayr/>{{Page needed|date=May 2011}} [[Donald Routledge Hill]] wrote the following on the automatic controls underlying the [[Mechanical puzzle|mechanical trick devices]] described in the book:

{{quote|The trick vessels have a variety of different effects. For example, a single outlet pipe in a vessel might pour out first wine, then water and finally a mixture of the two. Although it cannot be claimed that the results are important, the means by which they were obtained are of great significance for the history of engineering. The Banu Musa were masters in the exploitation of small variations in [[Aerostatics|aerostatic]] and [[Fluid statics|hydrostatic]] pressures and in using conical valves as "in-line" components in flow systems, the first known use of conical valves as automatic controllers.<ref name=Hill/>}}

The Banu Musa also developed an early [[fail-safe]] system for use in their trick devices, as described by Hill:

{{quote|In several of these vessels, one can withdraw small quantities of liquid repeatedly, but if one withdraws a large quantity, no further extractions are possible. In modern terms, one would call the method used to achieve this result a fail-safe system.<ref name=Hill/>}}

===Automatic crank===
The non-manual [[Crank (mechanism)|crank]] appears in several of the hydraulic devices described by the Banū Mūsā brothers in their ''Book of Ingenious Devices''.<ref>{{citation|title=The Cambridge History of Arabic Literature|last=A. F. L. Beeston, M. J. L. Young|first=J. D. Latham, Robert Bertram Serjeant|publisher=[[Cambridge University Press]]|year=1990|isbn=0-521-32763-6|page=266}}</ref> These automatically operated cranks appear in several devices, two of which contain an action which approximates to that of a [[crankshaft]], anticipating [[Al-Jazari]]'s invention by several centuries and its first appearance in Europe by over five centuries. However, the automatic crank described by the Banu Musa would not have allowed a full rotation, but only a small modification was required to convert it to a crankshaft.<ref>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|pages=23–4}}</ref>

===Valves===
A mechanism developed by the Banu Musa, of particular importance for future developments, was the conical [[valve]], which was used in a variety of different applications.<ref name=Hill-23/> This includes using conical valves as "in-line" components in flow systems, which was the first known use of conical valves as automatic controllers.<ref name=Hill/> Some of the other valves they described include:

* [[Plug valve]]<ref name=Mayr>Otto Mayr (1970). ''The Origins of Feedback Control'', [[MIT Press]].</ref><ref name=Hill>[[Donald Routledge Hill]], "Mechanical Engineering in the Medieval Near East", ''Scientific American'', May 1991, p. 64-69. ([[cf.]] [[Donald Routledge Hill]], [http://home.swipnet.se/islam/articles/HistoryofSciences.htm Mechanical Engineering])</ref>
* [[Ballcock|Float valve]]<ref name=Mayr/>
* [[Tap (valve)|Tap]]<ref name=Hill-1979/>

===Other mechanisms===
The double-concentric [[siphon]] and the [[funnel]] with bent end for pouring in different liquids, neither of which appear in any earlier Greek works, were also original inventions by the Banu Musa brothers.<ref name=Hill-21>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=21}}</ref> Some of the other mechanisms they described include a [[float chamber]]<ref name=Hassan/> and an early [[Pressure sensor|differential pressure]].<ref>{{citation|title=Ancient Discoveries, Episode 12: Machines of the East|publisher=[[History (U.S. TV channel)|History Channel]]|url=https://www.youtube.com/watch?v=n6gdknoXww8|accessdate=2008-09-06}}</ref>

==Machines and devices==
===Automatic fountains===
The book describes the construction of various automatic [[fountain]]s, an aspect that was largely neglected in earlier Greek treatises on technology.<ref name=Hill-21/> In one of these fountains, the "water issues from the fountainhead in the shape of a [[shield]], or like a [[Lily of the Valley|lily-of-the-valley]]," i.e. "the shapes are discharged alternately&mdash;either a sheet of water [[concave downward]]s, or a [[Sprayer|spray]]." Another fountain "discharges a shield or a single [[Water jet (recreation)|jet]]," while a variation of this features double-action alternation, i.e. has two fountainheads, with one discharging a single jet and the other a shield, and the two alternating repeatedly. Another variation features one main fountainhead and two or more subsidiary ones, such that when the main one ejects a single jet, the subsidiaries eject shields, with the two alternating.<ref name=Hill-44/>

The Banu Musa brothers also described the earliest known [[Wind power|wind-powered]] fountain,<ref>{{citation|title=History of, and Recent Progress in, Wind-Energy Utilization|author=[[Bent Sørensen (physicist)|Bent Sorensen]]|journal=Annual Review of Energy and the Environment|volume=20|pages=387–424|date=November 1995|doi=10.1146/annurev.eg.20.110195.002131}}</ref> which is described as, "operated by wind or water, it discharges a single jet or a lily-of-the-valley." A variation of this fountain incorporates a [[Worm drive|worm]]-and-[[Rack and pinion|pinion]] [[gear]], while another variation features double-action alternation. The book also describes a fountain with variable discharge.<ref name="Hill-44"/> The book also describes fountains that change shapes at intervals.<ref name=Hill/>

===Mechanical musical machines===
The Banu Musa invented an [[Hydraulis|early]] [[Musical box|mechanical]] [[musical instrument]], in this case a [[hydropower]]ed [[Organ (music)|organ]] which played interchangeable cylinders automatically. According to Charles B. Fowler, this "cylinder with raised pins on the surface remained the basic device to produce and reproduce music mechanically until the second half of the nineteenth century."<ref>{{citation|title=The Museum of Music: A History of Mechanical Instruments|first=Charles B.|last=Fowler|journal=Music Educators Journal|volume=54|issue=2|date=October 1967|pages=45–49|doi=10.2307/3391092|jstor=3391092|publisher=Music Educators Journal, Vol. 54, No. 2}}</ref>

The Banu Musa also invented an [[Automaton|automatic]] [[flute]] player which may have been the first [[Program (machine)|programmable machine]].<ref name=Koetsier>{{Citation |last1=Koetsier |first1=Teun  |year=2001 |title=On the prehistory of programmable machines: musical automata, looms, calculators |journal=Mechanism and Machine Theory |volume=36 |issue=5 |pages=589–603 |publisher=Elsevier |doi=10.1016/S0094-114X(01)00005-2 |postscript=.}}</ref> The flute sounds were produced through hot [[Steam power|steam]] and the user could adjust the device to various patterns so that they could get various sounds from it.<ref>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|pages=76–7}}</ref>

===Practical tools===
The mechanical [[Grab (tool)|grab]],<ref name=Hill-21/> specifically the [[Dredging#Grab|clamshell grab]],<ref name=Hill/> is an original invention by the Banu Musa brothers that does not appear in any earlier Greek works.<ref name=Hill-21/> The grab they described was used to extract objects from underwater,<ref name=Hill-44/> and recover objects from the beds of streams.<ref name=Hill/>

The Banu Musa also invented an early [[gas mask]],<ref name=Hill/> for protecting workers in polluted [[Oil well|wells]].<ref>{{citation|title=The Cambridge history of Arabic literature|first=M. J. L.|last=Young|publisher=[[Cambridge University Press]]|year=1990|isbn=0-521-32763-6|page=264}}</ref> They also described [[bellows]] that could remove foul air from wells.<ref name=Hill-44/> They explained that these instruments allow a worker to "descend into any well he wishes for a while and he will not fear it, nor will it harm him, if God wills may he be exalted."<ref>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|page=240}}</ref>

===Water dispensers===
The book describes a dispenser for hot and cold water, where the two outlets alternate, one discharging cold water and the other hot, then vice versa repeatedly. It also describes a vessel with a basin by its side where, when cold water is poured into the top of the vessel, it discharges from the mouth of a figure into the basin; when hot water or another liquid is poured into the basin, the same quantity of cold water is discharged from the mouth of the figure.<ref name=Hill-44/>

The book also describes a [[boiler]] with a [[Tap (valve)|tap]] to access [[Water heating|hot water]]. The water is heated through cold water being poured into a pipe which leads to a tank at the bottom of the boiler, where the water is heated with fire. A person can then access hot water from the boiler through a tap.<ref name=Hill-1979>{{citation|title=The book of ingenious devices (Kitāb al-ḥiyal)|author=[[Banu Musa]] (authors), [[Donald Routledge Hill]] (translator)|publisher=[[Springer Science+Business Media|Springer]]|year=1979|isbn=90-277-0833-9|pages=74–7}}</ref>

===Other devices===
Some of the other devices the Banu Musa described in their book include:

* [[Mechanical puzzle|Mechanical trick devices]]<ref name=Hill/>
* [[Hurricane lamp]]<ref name=Hill/>
* Self-trimming lamp<ref name=Hill/> (by Ahmad ibn Mūsā ibn Shākir)
* Self-feeding [[Oil lamp|lamp]]<ref name=Hill/>

==See also==
*[[Banū Mūsā]], the authors of the book
*[[Al-Jazari]], who wrote a book with a similar name
*[[Inventions in medieval Islam]]
*[[Islamic Golden Age]]
*[[Physics in medieval Islam]]
*[[Science and technology in Iran]]

==Notes==
{{Reflist}}

==References and further reading==
*Bunch, Bryan (2004). ''The History of Science and Technology''. Houghton Mifflin Books. ISBN 0-618-22123-9
*Dimarogonas, Andrew D. (2000). ''Machine Design: A CAD Approach''. Wiley-IEEE. ISBN 0-471-31528-1
*[[Donald Routledge Hill|Hill, Donald Routledge]] (Trans). (1978). ''Book of Ingenious Devices''. Kluwer Academic Publishers. ISBN 90-277-0833-9
*Rosheim, Mark E. (1994). ''Robot Evolution: The Development of Anthrobotics''. Wiley-IEEE. ISBN 0-471-02622-0

==External links==
*[http://www.muslimheritage.com/topics/default.cfm?ArticleID=285 A Review of Early Muslim Control Engineering]

[[Category:Islamic technology]]
[[Category:Scientific works of medieval Islam]]
[[Category:850 books]]
[[Category:Technology books]]
[[Category:9th-century Arabic books]]