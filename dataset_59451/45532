'''Robert William Prescott''' (May 5, 1913 &ndash; March 3, 1978) was an American aviator and [[entrepreneur]]. An [[flying ace|ace]] with the [[Flying Tigers]] in the early part of [[World War II]], he went on to found the [[Flying Tiger Line]], the first scheduled cargo airline in the United States.<ref name=Starliner>{{cite journal |last1=Zalaz |first1=T. J. |year=2002 |title=An Aviation First |journal=The Starliner |publisher=Houston Aeronautical Heritage Society, Inc. |volume=3 |issue=2 |pages=5 |url=http://www.1940airterminal.org/news/Starliner/2002/Starliner_12-2002.pdf |doi= }}</ref><ref name=CNAC>{{cite web |url=http://www.cnac.org/prescott04.pdf |title=Robert W. Prescott ... "Bob" |publisher=Public Relations Department, Flying Tiger Line |accessdate=October 14, 2012}}</ref>

==Early life==
He was born in [[Fort Worth, Texas]]. After high school, in 1934 he moved to California and worked his way through [[Compton Junior College]] as a truck driver.<ref name=Starliner/> He was also working and attending [[Loyola Law School]] in [[Los Angeles]] when some friends dragged him along on a visit to the naval flying school at [[Long Beach, California|Long Beach]].<ref name=Allaz>{{cite book |title=History of Air Cargo and Airmail from the 18th Century |last=Allaz |first=Camille |year=2005 |publisher=Google Consultant |isbn=0954889606 |pages=178–181 |url=http://books.google.ca/books?id=fPLm9omt_YIC&pg=PA178&lpg=PA178&dq=%22Robert+William+Prescott%22&source=bl&ots=0OxSz2gbto&sig=1rgSZahRg_JAg5nYtcXPsYuHmKI&hl=en&sa=X&ei=L2Z7UI-OE4GsiALEw4CgBA&ved=0CDYQ6AEwBDgK#v=onepage&q=%22Robert%20William%20Prescott%22&f=false |accessdate=October 14, 2012}}</ref> Prescott was hooked. In 1939, he quit studying law and enlisted in the [[United States Navy]] to become a pilot.<ref name=Starliner/>

==Aviator==
He completed training and qualified as an aviator, and was commissioned as an ensign in 1940.<ref name=CNAC/> He became an instructor at the naval flying school in [[Pensacola, Florida]].<ref name=Starliner/> He resigned his commission in September 1941 to join the [[American Volunteer Group]] (AVG) to fight the [[Imperial Japan|Japanese]] in China.<ref name=Starliner/> Before the AVG disbanded in the summer of 1942, he was credited with either 5.5<ref name=Olynk>Olynyk, Frank J. ''AVG & USAAF (China-Burma-India Theater) Credits for Destruction of Enemy Aircraft in Air to Air Combat, World War 2''. Aurora, Ohio: Privately published, 1986. Figure obtained from [http://www.warbirdforum.com/vics.htm warbirdforum.com].</ref><ref name=AFAA>{{cite web |url=http://www.americanfighteraces.org/wwllavg_a-z.html |title=American Volunteer Group Aces, World War II, 1939-1945 |publisher=American Fighter Aces Association (americanfighteraces.org) |accessdate=October 14, 2012}}</ref> or 6<ref name=Starliner/><ref>{{cite web |url=http://www.flyingtigersavg.com/Bio%27s/bio-Prescott.htm |title=Robert Prescott |publisher=flyingtigersavg.com |accessdate=October 14, 2012}}</ref> victories. Rather than follow his commander, [[Claire Chennault]], and a few of his comrades into the US military, Prescott returned to Fort Worth, where he was interviewed by ''[[Fort Worth Press]]'' journalist Helen Ruth.<ref name=Allaz/>

In 1943, he returned to Asia and, as an employee of the [[China National Aviation Corporation|China National Airways Corporation]], made over 300 supply flights over "[[the Hump]]" into China.<ref name=Allaz/> After returning to the United States in 1944, he married Helen Ruth.<ref name=Allaz/> He was the co-pilot of the "Mission to Moscow" flight of US Ambassador [[Joseph E. Davies]].<ref name=CNAC/>

==Flying Tiger Line==
In 1945, he met a group of businessmen headed by Los Angeles oil magnate [[Samuel B. Mosher]] who were interested in starting a cargo airline to serve the west coast of the United States and Mexico.<ref name=CNAC/> He convinced them that it would make better business sense to cover all of the continental United States instead.<ref name=CNAC/> They agreed to match whatever funds Prescott could raise. He raised $89,000 and recruited nine of his Flying Tigers pilot buddies, many of them fellow aces: [[William Bartling]], Clifford Groh, C. H. "Link" Laughlin, Thomas Haywood, [[Robert Hedman]], Ernest "Bus" Loane, Robert J. "Catfish" Raine, [[Joseph C. Rosbert|Joseph Rosbert]] and [[J. Richard Rossi|Richard Rossi]].<ref>{{cite web |url=http://www.flyingtigerline.org/founders.htm |title=The Founders |publisher=Flying Tiger Line Pilots Association (flyingtigerline.org) |accessdate=October 15, 2012}}</ref>

The '''National Skyway Freight Corporation''' was established on June 25, 1945, with Mosher as president and Prescott as managing director.<ref name=Allaz/> The new company's motto was "We'll Fly Anything, Anywhere, Anytime".<ref name=Starliner/> Prescott purchased 14 [[Budd RB Conestoga]] Navy surplus cargo planes.<ref name=CNAC/><ref name=Allaz/>

Their first shipment took place in July.<ref name=CNAC/><ref name=Allaz/> They lost $21,000 the first month of operations and $12,000 the second, but were making a profit by the third.<ref name=Starliner/> However, the charter freight airline began to have financial trouble, so Prescott applied to the [[United States government role in civil aviation#Civil Aeronautics Authority|Civil Aeronautics Board]] for a certificate allowing scheduled services.<ref name=Allaz/> In the meantime, salvation came in the form of a six-month contract with the US Army's [[Air Transport Command (United States Air Force)|Air Transport Command]] for pilots and maintenance services, later extended to November 1947.<ref name=Allaz/>

In 1947, the company's name was changed to '''Flying Tiger Line Inc.'''<ref name=Allaz/> In 1949, government approval was finally obtained for "the nation's first commercial all-cargo route."<ref name=CNAC/> The company prospered and expanded, and Prescott remained its only president and chief executive officer until his death in 1978.<ref name=CNAC/>

==Death==
He died of cancer at his home in [[Palm Springs, California]], at the age of 64.<ref name=CNAC/> He was survived by his wife, Anne-Marie, and daughters French and Kirchy. His 11-year-old son, Peter, was killed in a [[Learjet]] crash near [[Palm Springs, California|Palm Springs]] in 1965.

==References==
{{reflist}}

{{DEFAULTSORT:Prescott, Robert William}}
[[Category:1913 births]]
[[Category:1978 deaths]]
[[Category:American aviation businesspeople]]
[[Category:American World War II flying aces]]
[[Category:Aviators from Texas]]
[[Category:Businesspeople from Texas]]
[[Category:People from Fort Worth, Texas]]
[[Category:20th-century American businesspeople]]
[[Category:People from Palm Springs, California]]