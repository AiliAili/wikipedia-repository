{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Dubrovsky
| title_orig    = Дубровский
| translator    = 
| image         = Pushkin Dubrovsky 1919.jpg
| image_size = 180px
| caption = ''Dubrovsky'', illustrated by [[Boris Kustodiev]], 1919
| author        = [[Alexander Pushkin]]
| illustrator   = 
| cover_artist  = 
| country       = Russia
| language      = [[Russian language|Russian]]
| series        = 
| genre         = [[Novel]]
| publisher     = 
| release_date  = 1841
| media_type    = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| isbn = 1-84391-053-5
| isbn_note = (recent paperback edition)
| oclc= 52056603
| preceded_by   = 
| followed_by   = 
}}

'''''Dubrovsky''''' ({{lang-ru|Дубровский}}) is an unfinished [[novel]] by [[Alexander Pushkin]], written in 1832 and published after Pushkin’s death in 1841. The name ''Dubrovsky'' was given by the editor.

==Plot summary==
Vladimir Dubrovsky is a young [[Nobility|nobleman]] whose land is confiscated by a greedy and powerful aristocrat, Kirila Petrovitch Troekurov. Determined to get justice one way or another, Dubrovsky gathers a band of serfs and goes on the rampage, stealing from the rich and giving to the poor. Along the way, Dubrovsky falls in love with Masha,  Troekurov’s daughter, and lets his guard down, with tragic results.

==Editions and translations==

*“The Works of Alexander Pushkin”. Lyrics, Narrative poems, Folk tales, Plays, Prose. Selected and edited, with an introduction by Avrahm Yarmolinsky. New York: Random House[1936]. viii, 893 p.&nbsp;Contents: Introduction. Lyrics and ballads <…>  Dubrovsky. Egyptian nights (all translated by T. Keane). The Captain’s daughter (translated by [[Natalie Duddington]]).
*Reprinted: Unto myself I reared a monument, translated by Babette Deutsch, in Russki golos, New York, 1937, Feb. 7, sec. 1, also in Moscow News, Moscow, 1937, v. 7, no. 7, p.&nbsp;3, also in Moscow Daily News, Moscow, 1937, no 29, p 3,
*Behold a sower went forth to sow. Verses written during a sleepless night. Work, Parting, all translated by Babette Deutsch, in Russki golos, New York, 1937, Feb. 21, p.&nbsp;5. Excerpts from Evgeny Onegin, translated by Babette Deutsch, in Moscow Daily News. Moscow, 1937, no 33. p.&nbsp;2, 4,
*The Snowstorm reprinted in Moscow Daily News, 1937, no. 34—36.
*“The Captain’s daughter and other tales...”, Translated, with an introduction, by [[Natalie Duddington]].  London: J. M. Dent & Sons, Ltd.; New York: E. P. Dutton & Co., 1933. xi, 266 p. (Everyman’s library.) Contents: Introduction. Bibliographical notes. The Captain’s daughter. The Queen of spades. Dubrovsky. Peter the Great’s negro. The Station-master. Reviewed by Ben Ray Redman in Books, New York, v. 10, Feb. 11, 1934, p.&nbsp;12
*“Dubrovsky.” (In: Tales from the Russian [translated by Mrs. Sutherland Edwards?]. London: Railway and General Automatic Library [1892]) A copy in [[British Museum]].
* “Dubrovski; a tale of old Russia... “Translated by Reginald Merton. (The Argosy. London, 1928. 8°. v. 4, no. 27, p.&nbsp;33—54.)
*The Lady peasant. Dubrovsky; translated by T. Keane. (In: The Omnibus of romance... edited by John Grove [pseud, of John R. Colter]. New York: Dodd, Mead & Company, 1931. 594 p.) A copy in Circulation Department.
*The Prose tales of A. Pushkin; translated by T. Keane. London: G. Bell and Sons, Ltd., 1894. 402 p.&nbsp;8°. Copy in British Museum.
*Contents: The Captain’s daughter. Dubrovsky. Queen of spades. Amateur peasant girl. The Shot. The Snowstorm. The Postmaster. The Coffin-maker. Kirdjali. Authority: W. S. Sonnenschein, A reader’s guide to contemporary literature. New York: G. P. Putnam’s Sons, 1895, p.&nbsp;587.
*Reprinted with the addition of The Egyptian nights and Peter the Great’s negro in Bohn’s Standard Library, London, in 1896, 1911, 1916. Copy of 1896 edition in Library of Congress. A copy of 1916 edition. Authority for 1911 edition on verso of t.-p. of 1916 edition.
*Another edition issued in London by G. Bell and Sons In 1914, and reprinted in 1919, 1926. Authority for 1914 and 1919 editions on verso of 1926 edition. A copy of 1914 edition in the [[Library of Congress]].
*Also published by the Macmillan Company, New York in 1894, 1896, 1914. Authority: American catalog, 1890—95, p.&nbsp;358; 1895—1900, p.&nbsp;399, and the Cumulative book index, v. 18, p.&nbsp;618.
*Also published under title: "The Captain’s daughter and other tales", by Hodder and Stoughton, London, in 1915. Copy in Circulation Department. Reprinted in 1916. Authority: English catalog, 1916/20, p.&nbsp;918.
*Also published in 1925 by Harcourt, Brace and Company. New York. Authority: Cumulative book index, v. 28, p.&nbsp;1009.
*“Dubrovsky and Egyptian Nights” by [[Alexander Pushkin]]. Translated by Robert Chandler, with foreword by Patrick Neate,
[[Hesperus Press]], October 2003, Paperback, English ISBN 1-84391-053-5

==Film, TV or theatrical adaptations==
* '''''[[Dubrovsky (opera)]]''''' by [[Eduard Nápravník]] (1839–1916), premiered on January 15, [OS January 3] 1895, [[St Petersburg]].
*'''''[[The Eagle (1925 film)|The Eagle]]''''', silent film
:Director: 	Clarence Brown
:Producer: 	John W. Considine, Jr./Art Finance Corp.
:Writer:        Hans Kraly (scenario)
:Main Cast: [[Rudolph Valentino]], Vilma Bánky, Louise Dresser, James A. Marcus
:Cinematography: George Barnes, Dev Jennings
:Distributed by: 	United Artists
:Release date: 	November 8, 1925 (USA)
:Country: USA
:Running Time: 	80 min.

* '''''Dubrovsky''''', film, romantic adventure drama
:Director: Alexander V. Ivanovsky
:Main Cast: Boris Livanov, Nikolai Monakhov, Galina Grigoryeva, [[Vladimir Gardin]], [[Mikhail Tarkhanov (actor)|Mikhail Tarkhanov]]
:Release Year: 1935
:Country: Soviet Union
:Running Time: 75 min.

* '''''[[Black Eagle (1946 film)|Black Eagle]]''''' (''Aquila nera''), feature film
:Director: [[Riccardo Freda]]
:Main Cast: [[Rossano Brazzi]], Irasema Dilián, Gino Cervi
:Minor Cast: [[Gina Lollobrigida]]
:Release Year: 1946
:Country: Italy
:Running Time: 97 min.

* '''''[[Dubrowsky]]''''', feature film
:Director: William Dieterle
:Main Cast: John Forsythe, Rosanna Schiaffino, William Dieterle
:Release Year: 1959
:Country: Italy / Yugoslavia
:Running Time: 115 min.

* '''''[[:ru:Благородный разбойник Владимир Дубровский (фильм)|Noble Brigand Vladimir Dubrovsky]]''''', TV series
:Director: Vyacheslav Nikiforov
:Main Cast: [[Mikhail Olegovich Yefremov|Mikhail Yefremov]], [[Marina Zudina]], [[Vladimir Samoylov]], [[Kirill Lavrov]]
:Release Year: 1989
:Country: Soviet Union

* '''''[[:ru:Дубровский (фильм, 2014)|Dubrovskiy]]''''', feature film and TV mini-series
:Director: Alexander Vartanov and Kirill Mikhanovsky
:Writers: Konstantin Chernozatonsky, Mikhail Brashinsky
:Main Cast: [[Danila Kozlovsky]], Igor Gordin, Klavdiya Korshunova, Yuriy Tsurilo
:Producer: Evgeniy Gindilis
:Cinematohraphy: Vsevolod Kaptur, Anastasiy Mikhailov
:Composer: Aleksey Aygi 
:Release Year: 2014
:Country: Russia
:Running Time: 123 min. (film), 5 episodes of 42 min. each (TV mini-series)

==External links==
*{{ru icon}} [http://www.rvb.ru/pushkin/01text/06prose/01prose/0865.htm «Дубровский»] available at ''Russian Virtual Library''
*[http://home.freeuk.net/russica2/books/pushk/dub/dub.html ''Dubrovsky''], transl. by [[Natalie Duddington]]. Dead Link
*{{IMDb name|0027554|name=Dubrovskiy (1936)}}
*{{IMDb name|0053408|name=Il Vendicatore (1958)}}
*{{IMDb name|0096937|name=Blagorodnyy razboynik Vladimir Dubrovskiy (1989)}}
* [https://www.isliada.org/libros/dubrovsky/ Epub version (Spanish)]

{{Alexander Pushkin}}
{{Dubrovsky}}

{{Authority control}}

[[Category:1841 novels]]
[[Category:Novels by Aleksandr Pushkin]]
[[Category:Unfinished novels]]
[[Category:Novels published posthumously]]