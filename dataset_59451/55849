{{Infobox journal
| title         = Berkeley Fiction Review
| cover         = 
| editor        = 
| discipline    = [[Literary journal]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = BFR
| publisher     = [[University of California, Berkeley]]
| country       = [[United States]]
| frequency     = Annual
| history       = 1981-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://bfictionreview.wordpress.com
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 34383126
| LCCN          = sn 96000146
| CODEN         = 
| ISSN          = 1087-7053
| eISSN         = 1087-7053
| boxwidth      = 
}}

'''''Berkeley Fiction Review''''' is an American [[literary magazine]] founded in 1981 and based at the [[University of California, Berkeley]].<ref>"East Bay a Hotbed of Literary Journals," ''Contra Costa Times'' Feb 25, 2005</ref>  Stories that have appeared in the ''Berkeley Fiction Review'' have been reprinted in ''[[The Best American Short Stories]]'' and the [[Pushcart Prize]] anthology.<ref>"Awards for Local Literati," ''San Francisco Chronicle'', Jun 21, 1986</ref> The ''Berkeley Fiction Review'' sponsors an annual Sudden Fiction Contest.<ref>[http://berkeleyfictionreview.com/sudden-fiction/ Sudden Fiction Contest]</ref>
  
==Notable contributors==
{{Col-start}}
{{col-break}}
*[[Ellen Akins]]
*[[Jacob M. Appel|Jacob Appel]]
*Tamsen Armstrong
*[[Peter Bichsel]]
*[[Charles Bukowski]]
*Amina Cain
*Virginia Cerenio
*[[John Michael Cummings]] 
*Kenneth J. Emberly
{{col-break}}
*[[Will Eno]]
*[[Seamus Deane]]
*[[Mark Dery]]
*[[Jürgen Fauth]]
*Sergey Gandlevsky
*Alisa J. Golden
*[[DeWitt Henry]]
*Dale Herd
*Michael A. Hollister
{{col-break}}
*Sherril Jaffe
*G. Davies Jandrey
*[[Neil Jordan]]
*Paul Kafka
*[[Perri Klass]]
*[[Jeanne M. Leiby]] 
*[[Karin Lin-Greenberg]]
*[[B. K. Loren]]
*Scott Malcomson
{{col-break}}
*[[Valerie Miner]]
*[[John Montague (poet)|John Montague]]
*[[Jess Mowry]]
*[[Álvaro Mutis]]
*Scott Nagele
*Claudia Nogueira
*[[Joyce Carol Oates]]
*[[Dmitri Prigov]]
*Lev Rubinshteyn
{{col-break}}
*[[Olga Sedakova (poet)|Olga Sedakova]]
*[[Hal Sirowitz]]
*W. A. Smith
*[[Elena Shvarts]]
*Julia Vinograd
*[[Gerald Vizenor]]
*Glen Weldon
*[[Nellie Wong]]
{{col-end}}

==Masthead==
*Managing Editors
**Alagia Cirolia
**Georgia Peppe
**Clare Suffern
*Editors
**Jordan Abbott
**Evan Bauer
**Sarah Beydoun
**Margaret Chen
**Neha Dabke
**Sean Dennison
**Bailey Dunn
**Summer Farah 
**Cindy Ho
**Michelle Lee
**Rosina Miranda
**Moira Peckham

==Founders==
*Julia Littleton
*Jenne Mowry
*Joe Sciallo
*Paul Wedderien

==Past Managing Editors==
{{Col-start}}
{{col-break}}
*Lauren Cooper
*Hannah Harrington
*Ben Rowen
*Lisa Jenkins
*Miranda King
*Kelsey Nolan 
*Paige Vehlewald 
{{col-break}}
*Tessa Gregory 
*Eva Nierenberg 
*Christian White 
*Brighton Earley 
*Jennifer Brown 
*Caitlin McGuire
*Rachel Brumit  
{{col-break}}
*Bryce Kobrin
*Rhoda Piland 
*John Rauschenberg
*Elaine Wong
*Grace Fujimoto
*Nikki Thompson 
*Daphne Young
{{col-break}}
*Hugh O'Byrne Pedy
*Gregory Charles Magnuson
*Mark Landsman 
*Sean Andrew Locke 
*James Penner 
*Shelley Crist
*Julia E. Lave
{{col-break}}
*Julie Christianson 
*Christina Ferrari 
*Christopher Greger
*Dionisio Valesco
*Terrence Gee
{{col-end}}

==Special Features==
*Issue 6 (1985–86): Contemporary Poetry from the Soviet Union
*Issue 8 (Fall 1988): Works by, and Interviews with, Contemporary Irish Authors
*Issue 9 (Fall 1989): Stereoscopic Photographs
*Issue 16 (Spring 1997): First Annual Sudden Fiction Contest
*Issue 17 (Fall 1997): An early work by international award-winning Colombian writer Álvaro Mutis

==See also==
*[[List of literary magazines]]

==References==
<references/>

==External links==
*[http://bfictionreview.wordpress.com Berkeley Fiction Review Homepage]

[[Category:American literary magazines]]
[[Category:Magazines established in 1981]]
[[Category:University of California, Berkeley]]
[[Category:Magazines published in California]]


{{US-lit-mag-stub}}