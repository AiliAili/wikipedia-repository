{{good article}}
{{Use mdy dates|date=January 2016}}
{{Infobox song
| Name     = Best Mistake
| Border   = yes
| Artist   = [[Ariana Grande]] featuring [[Big Sean]]
| Album    = [[My Everything (Ariana Grande album)|My Everything]]
| Cover    =
| Caption =
| Released = {{start date|2014|08|12}}
| Type     = [[Promotional single]]
| Format   = Digital download
| Recorded = 2014
| Genre    = [[R&B]]
| Length   = {{duration|m=3|s=53}}
| Label    = [[Republic Records|Republic]]
| Writer   = {{flatlist|
* Ariana Grande
* [[Big Sean|Sean Anderson]]
* Denisea "Blu June" Andrews
* Brittany Coney
}}
| Producer = {{flat list|
* Key Wane
* Sauce
}}
| prev    = "[[Break Free (song)|Break Free]]"
| prev_no    = 5
| track_no   = 6
| next    = "Be My Baby"
| next_no    = 7
}}

"'''Best Mistake'''" is a song by American recording artist [[Ariana Grande]] that features American hip hop recording artist [[Big Sean]]. The song served as a [[promotional single]] from Grande's second studio album, ''[[My Everything (Ariana Grande album)|My Everything]]'' (2014), and was released at midnight on August 12, 2014. Written by Denisia "Blu June" Andrews, Brittany Coney, Grande, Big Sean, and produced by [[Key Wane]], the song is a [[ballad]] with piano, string, and drum machine instrumentation that lyrically deals with a couple trying to decide on what their future, troubled relationship is going to be like. Grande has claimed it is her favorite track off ''My Everything''.

It peaked on the US ''[[Billboard (magazine)|Billboard]]'' [[Billboard Hot 100|Hot 100]] at number 49, and within the top 50 on other record charts in North America, Europe, and Oceania. On the US [[Digital Songs]] chart, it debuted at number six, making Grande the first act since [[Michael Jackson]], and also the first female artist, to have three songs in the top ten on that chart the same week, with the other two songs being "[[Bang Bang (Jessie J, Ariana Grande and Nicki Minaj song)|Bang Bang]]" (with [[Jessie J]] and [[Nicki Minaj]]) and "[[Break Free (song)|Break Free]]" (featuring [[Zedd (musician)|Zedd]]). Grande and Big Sean have performed the song live, including at the [[iHeartRadio]] theater in Los Angeles.

==Production and composition==
"Best Mistake" was written by Denisia "Blu June" Andrews, Brittany Coney, Ariana Grande, [[Big Sean]], Corey Jackson Carter, and with production, programming and instruments done by [[Key Wane]]. The vocals were produced by Curtis "Sauce" Wilson, with Gregg Rominiecki engineering Big Sean's vocals. Serban Ghenea handled the mixing of the track, which was engineered by John Hanes, and finally the mix was mastered by Aya Merrill and Tom Coyne.<ref name="Album Credits"/> It is a [[Minimal music|minimal]] [[Hip-hop music|hip-hop]] [[Piano ballad|piano]] [[ballad]] [[lament]] that utilizes instrumentation from strings and a drum machine.<ref name = "Billboard"/> It is about a couple trying to "make up their minds about the future of their relationship, with deep affection buried underneath their problems."<ref name = "Billboard">{{cite web|last=Lipshutz|first=Jason|date=August 12, 2014|title=Ariana Grande & Big Sean Re-Team For 'Best Mistake': Listen|url=http://www.billboard.com/articles/columns/pop-shop/6214406/ariana-grande-big-sean-best-mistake|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|archiveurl=https://web.archive.org/web/20140902202810/http://www.billboard.com/articles/columns/pop-shop/6214406/ariana-grande-big-sean-best-mistake|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref><ref name = "Idolator"/><ref>{{cite web|last=Zaleski|first=Annie|date=August 26, 2014|url=http://www.avclub.com/review/ariana-grandes-sweet-sweet-fantasies-are-polished--208534|title=Ariana Grande's sweet, sweet fantasies are polished but disappointing|work=[[The A.V. Club]]|publisher=[[The Onion]]|archiveurl=https://web.archive.org/web/20140901091610/http://www.avclub.com/review/ariana-grandes-sweet-sweet-fantasies-are-polished--208534|archivedate=September 1, 2014|accessdate=September 2, 2014}}</ref><ref>{{cite web|last=Wood|first=Mikael|title=Ariana Grande Dials It Down With Big Sean In 'Best Mistake'|url=http://www.latimes.com/entertainment/music/posts/la-et-ms-listen-ariana-grande-big-sean-best-mistake-20140812-story.html|work=[[Los Angeles Times|The Los Angeles Times]]|publisher=[[Tribune Publishing]]|date=August 12, 2014|archiveurl=https://web.archive.org/web/20140901053026/http://www.latimes.com/entertainment/music/posts/la-et-ms-listen-ariana-grande-big-sean-best-mistake-20140812-story.html|archivedate=September 1, 2014|accessdate=September 2, 2014}}</ref> Grande revealed that "Best Mistake" was her favorite track on ''My Everything'': "I just think the world of [Big Sean] and I'm obsessed with his writing on this song in particular. I'm a huge fan of his in general but I feel like his writing on this song is so, so fantastic, it like strikes a chord in my heart. I love it so much."<ref>{{cite web|title=Ariana Grande Walks MTV Through 'My Everything' Track-By-Track|archive-url=https://web.archive.org/web/20140902201453/http://www.mtv.co.uk/ariana-grande/news/ariana-grande-walks-mtv-through-my-everything-track-by-track|archivedate=September 2, 2014|url=http://www.mtv.co.uk/ariana-grande/news/ariana-grande-walks-mtv-through-my-everything-track-by-track|work=[[MTV News]] (UK)|publisher=[[Viacom]]|date=August 18, 2014|accessdate=September 2, 2014}}</ref>

==Release and commercial performance==
[[File:Michael Jackson1 1988.jpg|thumb|left|With "Best Mistake", Grande became the first act since [[Michael Jackson]] (''pictured in 1988'') to have three songs in the top ten of the US [[Digital Songs]] chart on the same week.]]
Grande first confirmed the title of "Best Mistake" on June 28, 2014, the same day that she confirmed the name of her second studio album, ''My Everything''.<ref>{{cite web|last1=Coleman|first1=Miriam|title=Ariana Grande Reveals New Album With Sci-Fi Video Teaser|archiveurl=https://web.archive.org/web/20140819105620/http://www.rollingstone.com/music/news/ariana-grande-announces-new-album-with-sci-fi-video-teaser-20140629|archivedate=August 19, 2014|url=http://www.rollingstone.com/music/news/ariana-grande-announces-new-album-with-sci-fi-video-teaser-20140629|work=[[Rolling Stone]]|publisher=[[Jann Wenner|Wenner Media]] [[Limited liability company|LLC]]|date=June 29, 2014|accessdate=September 2, 2014}}</ref> On July 8, 2014, Grande released a 15-second snippet of "Best Mistake" onto her Instagram profile.<ref name = "Idolator">{{cite web|last=Lee|first=Christina|title=Ariana Grande Teases "Best Mistake" Featuring Big Sean: Listen|archiveurl=https://web.archive.org/web/20140808065350/http://www.idolator.com/7525751/ariana-grande-big-sean-best-mistake-preview|archivedate=August 8, 2014|url=http://www.idolator.com/7525751/ariana-grande-big-sean-best-mistake-preview|work=[[Idolator (website)|Idolator]]|publisher=[[SpinMedia]]|date=July 8, 2014|accessdate=September 2, 2014}}</ref> The song finally came out on August 12, and the release added more speculation to the relationship rumors between Grande and Big Sean.<ref name = "Under the Gun Review">{{cite web|last1=Shotwell|first1=James|title=Ariana And Big Sean Make The "Best Mistake" On New Single|url=http://www.underthegunreview.net/2014/08/13/ariana-grande-and-big-sean-make-the-best-mistake-on-new-single/|website=Under The Gun Review|date=August 13, 2014|archiveurl=https://web.archive.org/web/20140902202244/http://www.underthegunreview.net/2014/08/13/ariana-grande-and-big-sean-make-the-best-mistake-on-new-single/|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref><ref>{{cite web|last=Zupkus|first=Lauren|date=August 12, 2014|title=Ariana Grande and Big Sean Are Probably Dating|url=http://www.huffingtonpost.com/2014/08/12/ariana-grande-big-sean-dating_n_5672348.html|archiveurl=https://web.archive.org/web/20140815101857/http://www.huffingtonpost.com/2014/08/12/ariana-grande-big-sean-dating_n_5672348.html|archivedate=August 15, 2014|work=[[Huffington Post]]|publisher=[[AOL]]|accessdate=September 2, 2014}}</ref><ref name = "Bustle">{{cite web|last=Griffiths|first=Kadeen|title=Ariana Grande & Big Sean's "Best Mistake" Is A Sultry Ballad That Will Fuel Those Dating Rumors|url=http://www.bustle.com/articles/35395-ariana-grande-big-seans-best-mistake-is-a-sultry-ballad-that-will-fuel-those-dating|publisher=Bustle|date=August 12, 2014}}</ref>

In the United States, shortly after its release, the song reached number two on the weekly ''[[Billboard (magazine)|Billboard]]''  [[Billboard Twitter Real-Time|Twitter Real-Time]] chart<ref>{{cite web|url=http://www.billboard.com/biz/charts/2014-08-30/twitter-top-tracks|title=Billboard Twitter Top Tracks|work=Billboard|publisher=Prometheus Global Media|archiveurl=https://web.archive.org/web/20140902221423/http://www.billboard.com/biz/charts/2014-08-30/twitter-top-tracks|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref> and topped the [[iTunes Store|iTunes]] singles chart.<ref>{{cite web|last1=Grande|first1=Ariana|title=ok this is the last 1 I'm posting of these iTunes updates I swear but aaaaah!!! when's the next time this will ever happen?!? 1, 3, 4, and 12.... seriously can't thank u enough. I can't wait for you to hear the rest of the album. thank you so much for making this such a special time for me. y'all are everything. 💜💜💜💜💜|url=http://instagram.com/p/roPaqVyWZq/?modal=true|work=[[Instagram]]|publisher=[[Facebook, Inc.]]|date=August 13, 2014|accessdate=August 20, 2014}}</ref> It sold 104,000 units in its first week, landing at number six on the [[Digital Songs]] chart. This made Grande the first female to have three songs in the top ten on that chart, the other two being "[[Bang Bang (Jessie J, Ariana Grande and Nicki Minaj song)|Bang Bang]]" and "[[Break Free (Ariana Grande song)|Break Free]]." The last artist to do this was [[Michael Jackson]], shortly after his death, on the issue dated July 18, 2009.<ref>{{cite web|last=Caulfield|first=Keith|date=August 20, 2014|title=Billboard 200: Did 'Guardians of the Galaxy' Soundtrack Stay at No. 1?|url=http://www.billboard.com/articles/columns/chart-beat/6221958/guardians-of-the-galaxy-soundtrack-billboard-200-chart|archiveurl=https://web.archive.org/web/20140824224535/http://www.billboard.com/articles/columns/chart-beat/6221958/guardians-of-the-galaxy-soundtrack-billboard-200-chart|archivedate=August 24, 2014|work=Billboard|publisher=Prometheus Global Media|accessdate=September 2, 2014}}</ref> The sales of "Best Mistake" also helped it land at number 49 on the [[Billboard Hot 100|Hot 100]].<ref name="USHot100" /> On the [[Canadian Hot 100]], it appeared at number 39.<ref name="can" />

In European nations, "Best Mistake" debuted at number 49 and 10 on the Flanders [[Ultratop 50]] and Urban chart respectively, number 29 on the Danish [[Tracklisten]] chart,<ref name = "dk"/> 23 on the [[The Official Finnish Charts|Finnish Singles Chart]],<ref name = "fin"/> 103 on the French [[Syndicat National de l'Édition Phonographique|SNEP]] chart,<ref name = "fra"/> 5 on the ''Billboard'' Greek Digital chart,<ref name = "gr"/> 67 on the Netherlands [[MegaCharts|Mega]] [[Single Top 100]],<ref name = "NLD"/> 35 on the Spain [[Productores de Música de España|PROMUSICAE]] chart,<ref name = "es"/> and number 154 on the [[UK Singles Chart]].<ref name = "uk"/> In Oceania, it reached number 45 and 19 on the Australian [[ARIA Charts|ARIA]] pop and urban songs chart respectively,<ref name = "aus"/> and 29 on the [[Official New Zealand Music Chart]].<ref name = "NZ"/> On the [[Japan Hot 100]], it peaked at the 74th spot.<ref name = "jp"/> As of Year-End 2016, Best Mistake has sold 684,337 digital downloads in South Korea, according to [[Gaon Music Chart]].<ref>{{cite web|url=https://web.archive.org/web/20170329224723/http://www.gaonchart.co.kr/main/section/chart/online.gaon?serviceGbn=S1020&termGbn=year&hitYear=2015&targetTime=15&nationGbn=E |title=2015년 Download Chart|work=Gaon Chart|accessdate=March 29, 2017}}</ref> <ref>{{cite web|url=https://web.archive.org/web/20170329224839/http://www.gaonchart.co.kr/main/section/chart/online.gaon?serviceGbn=S1020&termGbn=year&hitYear=2016&targetTime=16&nationGbn=E |title=2016년 Download Chart|work=Gaon Chart|accessdate=March 29, 2017}}</ref>

==Critical reception==
[[File:Big sean.jpg|thumb|Big Sean's verse on "Best Mistake" garnered mixed reviews.]]
Evan Sawdey, a [[Popmatters]] interview Editor, called it the second best song from ''My Everything'', coming close to "Love Me Harder",<ref name = "Popmatters">{{cite web|last=Sawdey|first=Evan|date=September 2, 2014|url=http://www.popmatters.com/review/185041-ariana-grande-my-everything/|title=Ariana Grande: My Everything|publisher=[[Popmatters]]|accessdate=September 2, 2014}}</ref> and ''Bustle'' writer Kadeen Griffiths called it "one of Grande's best love songs so far."<ref name = "Bustle"/> [[Penske Media Corporation|HollywoodLife]]'s Caitlin Beck said it was "sure to be another hit!"<ref>{{cite web|last=Beck|first=Caitlin|date=August 12, 2014|archiveurl=https://web.archive.org/web/20140817202100/http://hollywoodlife.com/2014/08/12/big-sean-ariana-grande-best-mistake-listen-dating-2/|url=http://hollywoodlife.com/2014/08/12/big-sean-ariana-grande-best-mistake-listen-dating-2/|archivedate=August 17, 2014|title=Ariana Grande Releases Sexy New Song With Big Sean Amid Dating Rumors|work=HollywoodLife|publisher=[[Penske Media Corporation]]|accessdate=September 2, 2014}}</ref> Carolyn Menyes of the ''Music Times'' applauded Grande for her calming vocals and transition into a more mature sound of music.<ref>{{cite web|last=Menyes|first=Carolyn|title=Review: Ariana Grande, Big Sean 'Best Mistake' Marks Mature Sound for 'Bang Bang' Singer and Possible Boyfriend|date=August 12, 2014|url=http://www.musictimes.com/articles/8652/20140812/review-ariana-grande-big-sean-best-mistake-bang-bang-boyfriend-listen.htm|website=MusicTimes.com|publisher=Music Times|accessdate=August 19, 2014}}</ref> ''[[Billboard (magazine)|Billboard]]''<nowiki>'</nowiki>s Jason Lipshutz called the production "impressive" and said the song "rows stickier upon each listen".<ref name = "Billboardtrackbytrack">{{cite web|last=Lipshutz|first=Jason|date=August 25, 2014|url=http://www.billboard.com/articles/review/6229287/ariana-grande-my-everything-billboard-album-review|title=Ariana Grande Expands on 'My Everything': Track-By-Track Review|work=Billbosrd|publisher=Prometheus Global Media|accessdate=September 2, 2014}}</ref> [[Digital Spy]] writer Lewis Corner and [[Gigwise|Entertainmentwise]]'s Shaun Kitchener noted the song showed her R&B roots,<ref>{{cite web|last=Corner|first=Lewis|date=August 22, 2014|url=http://www.digitalspy.com/music/review/a591987/ariana-grande-my-everything-album-review-her-voice-shines-supreme.html#~oOJwebsWcO8WPB|title=Ariana Grande My Everything album review: 'Her voice shines supreme'|publisher=[[Digital Spy]]|accessdate=September 2, 2014|archiveurl=https://web.archive.org/web/20140826114852/http://www.digitalspy.com/music/review/a591987/ariana-grande-my-everything-album-review-her-voice-shines-supreme.html#~oOK5WLmTllliLI|archivedate=August 26, 2014}}</ref><ref name = "Entertainmentwise">{{cite web|last=Kitchener|first=Shaun|date=August 16, 2014|title=Ariana Grande's 'My Everything': Track-By-Track Review|url=http://www.entertainmentwise.com/news/155907/Ariana-Grandes-New-Album-My-Everything-Track-By-Track-Review|work=Entertainmentwise|publisher=[[Gigwise]]|archiveurl=https://web.archive.org/web/20140902203121/http://www.entertainmentwise.com/news/155907/Ariana-Grandes-New-Album-My-Everything-Track-By-Track-Review|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref> the latter stating that it "wouldn't have sounded out of place on [[Kelly Rowland]]'s under-rated last album [''[[Talk a Good Game]]'']."<ref name = "Entertainmentwise"/> The [[Official Charts Company]] critic Rob Copsey felt it was "like an extension of ''[[Yours Truly (Ariana Grande album)|Yours Truly]]'', albeit moodier and more grown up."<ref name = "OCCreview">{{cite web|last=Copsey|first=Rob|date=August 17, 2014|url=http://www.officialcharts.com/chart-news/ariana-grandes-new-album-my-everything-track-by-track-review-3046/|title=Ariana Grande's new album My Everything: Track-by-track|publisher=[[Official Charts Company]]|accessdate=September 2, 2014}}</ref> Brennan Carley of ''[[Spin (magazine)|Spin]]'' called it "classic Grande, eshcewing any of the bells and whistles that she's fond of, instead focusing entirely on her carefully sung vocals and the quiet piano line in the song's background."<ref>{{cite web|last=Carley|first=Brennan|date=August 12, 2014|url=http://www.spin.com/articles/ariana-grande-big-sean-new-single-best-mistake/|archiveurl=https://web.archive.org/web/20140826171438/http://www.spin.com/articles/ariana-grande-big-sean-new-single-best-mistake/|archivedate=August 26, 2014|title=Ariana Grande Teases New Album With Big Sean Single|work=[[Spin (magazine)|Spin]]|publisher=SpinMedia|accessdate=September 2, 2014}}</ref> ''[[Newsday]]'' critic Glenn Gamboa described "Best Mistake" as a "gorgeous hip-hop" song that "showcases her wide-ranging voice, without focusing on the upper notes too much."<ref>{{cite web|last=Gamboa|first=Glenn|date=August 25, 2014|url=http://www.newsday.com/entertainment/music/ariana-grande-s-my-everything-review-mini-mariah-comes-into-her-own-1.9128910|title=Ariana Grande's 'My Everything' review: 'Mini-Mariah' comes into her own|work=[[Newsday]]|publisher=[[Cablevision]]|archiveurl=https://web.archive.org/web/20140831081007/http://www.newsday.com/entertainment/music/ariana-grande-s-my-everything-review-mini-mariah-comes-into-her-own-1.9128910|archivedate=August 31, 2014|accessdate=September 2, 2014}}</ref> Sydney Gore of The 405 called it an improvement of the two's previous collaboration "[[Right There (Ariana Grande song)|Right There]]", writing that "the singer and rapper serenade us on the grounds that sneaking around with each other was the best mistake they ever made."<ref>{{cite web|last=Gore|first=Sydney|date=August 29, 2014|url=http://www.thefourohfive.com/review/article/ariana-grande-my-everything-140|title=Ariana Grande – My Everything |publisher=The 405|archiveurl=https://web.archive.org/web/20140902203313/http://www.thefourohfive.com/review/article/ariana-grande-my-everything-140|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref>

There were, however, some mixed reviews of "Best Mistake". [[Idolator (website)|Idolator]]'s Kathy Iandoli described it as "the average looking cousin of their previous duet "Right There"".<ref>{{cite web|last=Iandoli|first=Kathy|date=August 25, 2014|url=http://www.idolator.com/7532086/ariana-grandes-my-everything-album-review|title=Ariana Grande's 'My Everything': Album Review|work=Idolator|publisher=SpinMedia|archiveurl=https://web.archive.org/web/20140826185419/http://www.idolator.com/7532086/ariana-grandes-my-everything-album-review|archivedate=August 26, 2014|accessdate=September 2, 2014}}</ref> Reviewing for ''[[Slant Magazine]]'', Andrew Chan said it "makes the mistake of hemming her into her frail middle register, where she has a habit of delivering every word as if it were a pout."<ref>{{cite web|last=Chan|first=Andrew|date=August 24, 2014|url=http://www.slantmagazine.com/music/review/ariana-grande-my-everything|archiveurl=https://web.archive.org/web/20140902203537/http://www.slantmagazine.com/music/review/ariana-grande-my-everything|archivedate=September 2, 2014|title=Ariana Grande: My Everything|work=[[Slant Magazine]]|accessdate=September 2, 2014}}</ref> Big Sean's appearance on "Best Mistake" also got varied reception. [[Pitchfork Media]]'s Meaghan Garvey said his rap on the track made "a mockery of the song's serious tone with hysterically awful lines like "How can we keep the feelings fresh/ How do we Ziploc it?""<ref>{{cite web|last=Garvey|first=Meaghan|date=August 29, 2014|url=http://pitchfork.com/reviews/albums/19765-ariana-grande-my-everything/|archiveurl=https://web.archive.org/web/20140830043900/http://pitchfork.com/reviews/albums/19765-ariana-grande-my-everything/|archivedate=August 30, 2014|title=Ariana Grande: My Everything|publisher=[[Pitchfork Media]]|accessdate=September 2, 2014}}</ref> Lipshutz found his verse "unnecessary, yet [it] has morphed into an interesting confessional now that the dating rumors are on."<ref name = "Billboardtrackbytrack"/> Sawdey called it a "pretty outstanding verse," with "his own voice never overpowering the sparse atmosphere, his rhymes measured and metered in a way that fits the song perfectly",<ref name = "Popmatters"/> while James Shotwell of ''Under the Gun Review'' said "Grande is a treat, but I think it's Big Sean who steals the show."<ref name = "Under the Gun Review"/>

==Live performances==
In 2014, Grande and Big Sean performed "Best Mistake" on the [[Honda]] Stage at the [[iHeartRadio]] Theater in Los Angeles.<ref>{{cite web|url=https://www.youtube.com/watch?v=UCZzepkKjD0|title=Best Mistake (Live on the Honda Stage at the iHeartRadio Theater LA)|publisher=Ariana Grande [[VEVO]]|date=September 2, 2014|accessdate=September 2, 2014}}</ref> They also performed "Best Mistake" at ''A Very Grammy Christmas'' on November 18, 2014.<ref>{{cite web|url=http://www.usmagazine.com/entertainment/news/ariana-grande-big-sean-hold-hands-in-romantic-grammy-christmas-show-20141911|title=Ariana Grande, Big Sean Hold Hands in Romantic Grammy Christmas Performance: Photos|work=[[Us Weekly]]|date=November 19, 2014|accessdate=December 6, 2014}}</ref> Grande also performed the song during [[The Honeymoon Tour]]. Big Sean performed the song with her at the tour's stop in Detroit.

==Credits and personnel==
Credits are adapted from the liner notes of ''My Everything''.<ref name="Album Credits">Ariana Grande – ''My Everything''. Album booklet. [[Republic Records]] ([[Universal Music Group]]).</ref>

{{Div col|cols=2}}
*Songwriting, vocals&nbsp;– Denisia Andrews ("Blu June"), Brittany Coney, [[Ariana Grande]], [[Big Sean|Sean Anderson ("Big Sean")]]
*Songwriting, production, [[Programming (music)|programming]], [[Musician|instruments]]&nbsp;– [[Key Wane]]
*Vocal production&nbsp;– Curtis Wilson ("Sauce")
*Big Sean Vocal [[Recording engineer|engineering]]&nbsp;– Greg Rominiecki
*[[Mixing (music production)|Mixing]]&nbsp;– Serban Ghenea
*[[Mixing engineer|Mix engineering]]&nbsp;– John Hanes
*[[Mastering engineer|Mastering]]&nbsp;– Aya Merrill, Tom Coyne
{{Div col end}}

==Charts==
{| class="wikitable plainrowheaders sortable" style="text-align:center;"
|-
! Chart (2014)
! Peak<br/>position
|-
{{singlechart|Australia|45|artist = Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014}}
|-
!scope="row"|Australia Urban ([[ARIA Charts|ARIA]])<ref name = "aus">{{cite web|url=http://pandora.nla.gov.au/pan/23790/20140828-0957/Issue1278.pdf|title=ARIA Report|publisher=ARIA|accessdate=September 2, 2014}}</ref>
|14
|-
{{singlechart|Flanders|49|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 22, 2014}}
|-
{{singlechart|Flanders Urban|10|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 22, 2014}}
|-
{{singlechart|Canada|39|artist=Ariana Grande|artistid=148434|rowheader=true|accessdate=August 21, 2014|refname="can"}}
|-
{{singlechart|Czechdigital|46|year=2014|week=37|rowheader=true|accessdate=September 16, 2014}}
|-
{{singlechart|Denmark|29|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014|refname="dk"}}
|-
{{singlechart|Finnishdownload|23|artist=Ariana Grande|song=Best Mistake (feat. Big Sean)|rowheader=true|accessdate=August 21, 2014|refname="fin"}}
|-
{{singlechart|France|103|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014|refname="fra"}}
|-
!scope="row"|Greece (''[[Billboard (magazine)|Billboard]]'')<ref name = "gr">{{cite web|url=http://www.billboard.com/biz/charts/2014-08-30/greece|title=Greece Digital Songs|work=Billboard|publisher=Prometheus Global Media|archiveurl=https://web.archive.org/web/20140902212828/http://www.billboard.com/biz/charts/2014-08-30/greece|archivedate=September 2, 2014|accessdate=September 2, 2014}}</ref>
|5
|-
{{singlechart|Billboardjapanhot100|74|artist=Ariana Grande|artistid=148434|rowheader=true|accessdate=August 21, 2014|refname="jp"}}
|-
{{singlechart|Dutch100|67|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014|refname="NLD"}}
|-
{{singlechart|New Zealand|29|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014|refname="NZ"}}
|-
{{singlechart|Slovakdigital|59|year=2014|week=37|accessdate=September 16, 2014|rowheader=true}}
|-
!scope="row"|South Korea International Chart ([[Gaon Chart|Gaon]])<ref>{{cite web|url=http://www.gaonchart.co.kr/main/section/chart/online.gaon?serviceGbn=ALL&termGbn=week&hitYear=2015&targetTime=31&nationGbn=E|title=Gaon Digital Chart – Week 3, 2015|publisher=[[Gaon Music Chart|Gaon]]|language=Korean|accessdate=August 27, 2015}}</ref>
| style="text-align:center;"|1
|-
{{singlechart|Spain|34|artist=Ariana Grande feat. Big Sean|song=Best Mistake|rowheader=true|accessdate=August 21, 2014|refname="es"}}
|-
!scope="row"|UK Singles ([[UK Singles Chart|Official Charts Company]])<ref name = "uk">{{cite web|last=Zywietz|first=Tobias|url=http://zobbel.de/cluk/140906cluk.txt |title=CHART: CLUK Update 6.09.2014 (wk35)|publisher=Zobbel|accessdate=September 6, 2014}}</ref>
|align="center"|154
|-
{{singlechart|Billboardhot100|49|artist=Ariana Grande|artistid=1484343|rowheader=true|accessdate=August 21, 2014|refname="USHot100"}}
|-
|}

== References ==
{{reflist|30em}}

{{Ariana Grande singles}}
{{Big Sean singles}}

[[Category:2010s ballads]]
[[Category:2014 songs]]
[[Category:Ariana Grande songs]]
[[Category:Big Sean songs]]
[[Category:Pop ballads]]
[[Category:Republic Records singles]]
[[Category:Rhythm and blues ballads]]
[[Category:Songs written by Ariana Grande]]
[[Category:Songs written by Big Sean]]