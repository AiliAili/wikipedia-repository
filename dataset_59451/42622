<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=AX2000
 | image=G-WAKYa.jpg
 | caption=A HKS 700E powered AX2000
}}{{Infobox Aircraft Type
 | type=[[microlight]]
 | national origin=[[United Kingdom]]
 | manufacturer=Cyclone Airsports Ltd
 | designer=
 | first flight=c.1997
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=at least 29
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=Cyclone AX3 
 | variants with their own articles=
}}
|}
The Cyclone '''AX2000''' is a British built [[Ultralight aviation#Types of aircraft|three axis microlight]], first flown in the 1990s.  It seats two in [[side-by-side configuration]].

==Design and development==
The AX2000 is a development of the earlier [[Cyclone AX3]],<ref name=AX2000/> a UK regulation compliant variant of the French [[Ultralair Premier AX3]].  This, in turn, was a three axis development of the US [[Chotia Weedhopper]] from the early 1980s.<ref name=AX3/>

The Cyclone AX2000 is an aluminium tube framed three axis microlight with flying surfaces covered with a [[polyester]] fabric with outer [[PVF]] lamination (URLAM).  The whole aircraft is built around a long, high aluminium keel boom, which bears the engine, wings and [[empennage]].  The wing is a two spar structure, with surfaces formed by upper and lower battens rather than ribs.  It carries conventional, full span, tapering [[ailerons]] but no [[flap (aircraft)|flaps]].<ref name=AX2000/> The [[tailplane]] is mounted on the keel and has an anti-balance/trim tab on the [[starboard]] [[elevator (aircraft)|elevator]].<ref name=AX2000/><ref name=TADS/>  There is a small fixed [[fin]] below the keel but none above; the [[rudder]] is [[balanced rudder|balanced]] and extends below the elevators, moving in a cut-out.<ref name=TADS/>

The short, deep [[fuselage]] is built around a wire braced tube beam.  Lift and landing loads are taken by V-shaped, cross braced pairs of [[lift strut]]s from the bottom of the fuselage to each wing, assisted by a centre line strut to the boom at the wing [[leading edge]].  The cockpit enclosure is non-structural, with forward opening doors to a pair of side-by-side seats.  These are equipped with separate rudder pedals but the occupants share a central [[control column]].  The AX2000 has a [[tricycle undercarriage]] mounted close to the fuselage.<ref name=AX2000/>

The engine is mounted, uncowled, ahead and above the wing leading edge, with the propeller shaft on the boom line.  One of three engines may be fitted: a 48&nbsp;kW (64&nbsp;hp) [[Rotax 582|Rotax 582/48]] or 38&nbsp;kW (50&nbsp;hp) [[Rotax 503|503 2V]], both upright twin cylinder [[two stroke]]s, or the 45&nbsp;kW (60&nbsp;hp) [[HKS 700E]] (either V3 or Beta variants), [[flat twin]] [[four stroke]].<ref name=TADS/> Both Rotax engines drive propellers with ground adjustable pitch: the 582 has a three blade, composite propeller and the 503 a two blade wooden one.<ref name=AX2000/>

As well the new engines, the AX2000 differs chiefly from the AX3 in having a completely double surface wing of reduced area and modified structure, a change of alloy for the main keel beam, a revised and lightened fuselage with new undercarriage, an increased fuel capacity and the addition of an elevator anti-balance trim tab.<ref name=AX2000/>

The AX2000 is certificated as a glider tug, for gliders of class 1 and 2. A V-shaped forward tow line extension joins the main line to a pair of tug pillars attached to the rear spar on the under surfaces of the AX2000's wing.<ref name=AX2000/>

==Operational history==
In mid-2011 there were 29 AX2000s on the [[UK]] civil aircraft register.<ref name=CAAreg/>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref=Civil Aircraft Authority microlight type approval data sheet (TADS) no.BM53 issue 9<ref name=TADS/><!-- for giving the reference for the data -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|length m=5.60
|length note=
|span m=9.10
|height m=2.10
|height note=
|wing area sqm=15.77
|wing area note=<ref name=AX2000/>
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=201
|empty weight note=notional
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=450
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=62 L

|more general=
<!--
        Powerplant
-->
|eng1 number=
|eng1 name=
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed mph=62

|max speed note=manoeuvring
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=36
|stall speed kts=
|stall speed note=
|never exceed speed mph=90
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{commons category|Cyclone AX2000}}
{{reflist|refs=
<ref name=AX3>{{cite web |url=http://www.caa.co.uk/aandocs/23042/23042020000.pdf |title=CAA airworthiness approval note - Cyclone AX3 |author= |date= |work= |publisher= |accessdate=2011-08-23}}</ref>

<ref name=AX2000>{{cite web |url=http://ulav8r.com/PDF%20files/25727020000.pdf |title=CAA airworthiness approval note - Cyclone AX2000 |author= |date= |work= |publisher= |accessdate=2011-08-23}}</ref>

<ref name=TADS>{{cite web |url=http://www.caa.co.uk/docs/1419/BM53I09.pdf |title=CAA TADS for Cyclone AX2000|author= |date= |work= |publisher= |accessdate=2011-08-22}}</ref>

<ref name=CAAreg>{{cite web |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=CYCLONE%20AX2000 |title=CAA registrations of Cyclone AX2000|author= |date= |work= |publisher= |accessdate=2011-08-24}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:British ultralight aircraft 1990–1999]]