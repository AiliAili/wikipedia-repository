{{Other ships|HMS Brazen}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Brazen AWM 302321.jpg|300px|HMS Brazen]]
|Ship caption=
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag= {{Shipboxflag|UK|naval}}
|Ship name=HMS ''Brazen''
|Ship namesake=
|Ship ordered=22 March 1929
|Ship awarded=
|Ship builder=[[Palmers Shipbuilding and Iron Company|Palmers]], [[Hebburn]]
|Ship original cost=£221,156
|Ship yard number=
|Ship way number=
|Ship laid down=22 July 1929
|Ship launched=25 July 1930
|Ship sponsor=
|Ship christened=
|Ship completed= 8 April 1931
|Ship commissioned=
|Ship identification=[[Pennant number]]: H80<ref name=w78/>
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate= Sunk by German aircraft, 20 July 1940
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(as built)
|Ship class= {{sclass2-|B|destroyer}} 
|Ship displacement=*{{convert|1360|LT|t|abbr=on}} (standard)
*{{convert|1790|LT|t|abbr=on}} ([[deep load]])
|Ship length={{convert|323|ft|m|1|abbr=on}} [[Length overall|o/a]]
|Ship beam={{convert|32|ft|3|in|m|1|abbr=on}}
|Ship draught={{convert|12|ft|3|in|m|1|abbr=on}}
|Ship power=*{{convert|34000|shp|kW|lk=in|abbr=on}}
*3 × [[Admiralty 3-drum boiler]]s
|Ship propulsion=*2 × shafts
*2 × [[Parsons Marine Steam Turbine Company|Parsons]] geared [[steam turbine]]s
|Ship speed={{convert|35|kn|lk=in}}
|Ship range={{convert|4800|nmi|lk=in|abbr=on}} at {{convert|15|kn}}
|Ship complement=142 (wartime)
|Ship sensors=Type 119 [[ASDIC]] 
|Ship armament=*4 × 1 – [[4.7 inch QF Mark XII|4.7-inch (120 mm)) Mk IX gun]]s
*2 × 1 – [[QF 2 pounder naval gun|QF 2-pounder (40 mm) Mk II]] [[anti-aircraft gun|AA guns]]
*2 × 4 – [[British 21 inch torpedo|{{convert|21|in|mm|abbr=on|0}} torpedo]] [[torpedo tube|tubes]]
*20 × [[depth charge]]s, 1 rail and 2 throwers
|Ship notes=
}}
|}
'''HMS ''Brazen''''' was a {{Sclass2-|B|destroyer}} built for the [[Royal Navy]] around 1930. Initially assigned to the [[Mediterranean Fleet]], she was transferred to [[Home Fleet]] in 1936. The ship escorted convoys and conducted [[anti-submarine warfare|anti-submarine patrols]] early in [[World War II]] before participating in the [[Norwegian Campaign]] in April–May 1940. ''Brazen'' later began escorting coastal convoys in the [[English Channel]] and was sunk in late July 1940 by German aircraft whilst doing so.

==Description==
''Brazen'' displaced {{convert|1360|LT|t}} at [[Displacement (ship)|standard]] load and {{convert|1790|LT|t}} at [[deep load]]. The ship had an [[length overall|overall length]] of {{convert|323|ft|m|1}}, a [[beam (nautical)|beam]] of {{convert|32|ft|3|in|m|1}} and a [[draft (hull)|draught]] of {{convert|12|ft|3|in|m|1}}. She was powered by [[Parsons Marine Steam Turbine Company|Parsons]] geared [[steam turbine]]s, driving two shafts, which developed a total of {{convert|34000|shp|lk=in}} and gave a maximum speed of {{convert|35|kn|lk=in}}. Steam for the turbines was provided by three [[Admiralty 3-drum boiler]]s. ''Brazen'' carried a maximum of {{convert|390|LT|t}} of [[fuel oil]] that gave her a range of {{convert|4800|nmi|lk=in}} at {{convert|15|kn}}.<ref name=w78>Whitley, p. 99</ref> The ship's complement was 134 officers and enlisted men, although it increased to 142 during wartime.<ref name=f8>Friedman, p. 298</ref>

The ship mounted four 45-[[caliber (artillery)|calibre]] [[4.7 inch QF Mark XII|QF 4.7-inch Mk IX guns]] in single mounts. For [[Anti-aircraft warfare|anti-aircraft]] (AA) defence, ''Brazen'' had two {{convert|40|mm|1|adj=on}} [[QF 2 pounder naval gun|QF 2-pounder Mk II]] AA guns mounted on a platform between her [[funnel (ship)|funnels]]. She was fitted with two above-water quadruple [[torpedo tube]] mounts for [[British 21 inch torpedo|{{convert|21|in|adj=on|0}}]] torpedoes.<ref name=f8/> One [[depth charge]] rail and two throwers were fitted; 20 depth charges were originally carried, but this increased to 35 shortly after the war began.<ref>English, p. 141</ref>

==Career==
The ship was ordered on 22 March 1929 from [[Palmers Shipbuilding and Iron Company]] at [[Hebburn]] under the 1928 [[Glossary of nautical terms#N|Naval Programme]]. She was laid down on 22 July 1929, and launched on 25 July 1930,<ref>English, pp. 29–30</ref> as the seventh RN ship to carry this name.<ref>Colledge, p. 33</ref> ''Brazen'' was completed on 8 April 1931 at a cost of £220,342, excluding items supplied by the Admiralty such as guns, ammunition and communications equipment.<ref>March, p. 260</ref> After her commissioning, she was assigned to the [[4th Destroyer Flotilla]] with the [[Mediterranean Fleet]] until the end of 1935. The ship received a refit at [[HMNB Devonport|Devonport]] from August to October 1933 and another at [[Malta]] a few months later. ''Brazen'' was assigned to the Home Fleet in 1936 and participated in the effort to rescue the crew of the submarine {{HMS|Thetis|N25|2}} which had sunk during [[sea trial]]s on 1 June 1939.<ref>English, p. 37</ref>

The ship was reassigned to the [[19th Destroyer Flotilla]] in August, shortly before World War II began. She spent the next seven months escorting convoys and patrolling in the English Channel and the [[North Sea]]. On 13 October, ''Brazen'' rescued three survivors from {{GS|U-40|1938|6}} which had sunk after striking a [[naval mine|mine]] a few hours earlier. The ship, together with the destroyer {{HMS|Encounter|H10|2}}, assumed the escort of Convoy HN12 after the destroyer {{HMS|Daring|H16|2}} had been sunk by {{GS|U-23|1936|2}}. Later that day she rescued some survivors from the Norwegian merchant ship ''Sangstad''.<ref name=e8>English, p. 38</ref> ''Brazen'' escorted the [[capital ship]]s of the Home Fleet as they sortied into the North Sea on 7 April and continued that duty for the next several weeks.<ref>Haar (2009), pp. 86, 372</ref> The ship was detached to escort a troop convoy to ''Namsos'' on 13 April and sank {{GS|U-49|1939|2}} two days later with the destroyer {{HMS|Fearless|H67|2}} near [[Harstad]], Norway.<ref>Haar (2010), pp. 203–05</ref> The two destroyers rescued 41 of the submarine's crew. ''Brazen'' escorted several more convoys to and from Norway over the next several weeks.<ref name=e8/>

On 30 May, the ship was en route to [[Harwich Dockyard|Harwich]] when she struck some submerged wreckage and suffered damage that required five weeks to repair. ''Brazen'' was transferred to the [[1st Destroyer Flotilla]], based at [[Dover]], upon their completion, where she began escorting coastal convoys. Whilst escorting Convoy CW7 on 20 July, during the [[Kanalkampf|initial phase]] of the [[Battle of Britain]], the ship was attacked by German [[Junkers Ju 87]] ''Stuka'' dive bombers belonging to II./''[[Sturzkampfgeschwader 1]]'' (Dive Bomber Wing 1—or StG 1).<ref>Weal, pp. 70–71</ref> The shock effect from several near misses broke her [[keel]] and then she was hit in the engine room. ''Brazen'' sank at position {{coord|51|01|05|N|01|17|15|E}} at 20:40. Only one member of her crew was killed during the attack and her gunners claimed to have shot down three Ju 87s.<ref name=e8/> German records confirm only two losses—both fell to defending British fighter aircraft.<ref>Mason, p. 183</ref>

==See also==
* [[List of shipwrecks in 1940]]

==Notes==
{{reflist|30em}}

==References==
* {{Colledge}}
* {{cite book|last=English|first=John|title=Amazon to Ivanhoe: British Standard Destroyers of the 1930s|year=1993|publisher=World Ship Society|location=Kendal, England|isbn=0-905617-64-9}}
* {{cite book|last=Friedman|first=Norman|title=British Destroyers From Earliest Days to the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2009|isbn=978-1-59114-081-8}}
* {{cite book|last=Haarr|first=Geirr H.|title=The Battle for Norway: April–June 1940|year=2010|publisher=Naval Institute Press|location=Annapolis, MD|isbn=978-1-59114-051-1}}
* {{cite book|last=Haarr|first=Geirr H.|title=The German Invasion of Norway, April 1940|year=2009|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=978-1-59114-310-9}}
* {{cite book|last=Lenton|first=H. T.|title=British & Empire Warships of the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|date=1998|isbn=1-55750-048-7}}
*{{cite book|last=March|first=Edgar J.|title=British Destroyers: A History of Development, 1892–1953; Drawn by Admiralty Permission From Official Records & Returns, Ships' Covers & Building Plans|year=1966|publisher=Seeley Service|location=London |OCLC=164893555}}
* {{cite book|last=Mason|first=Francis|title=Battle Over Britain. McWhirter Twins Ltd, London. 1969.|year=1993|publisher=McWhirter Twins Ltd|location=London, England|isbn=978-0-901928-00-9}}
* {{cite book|last=Rohwer|first=Jürgen|title=Chronology of the War at Sea 1939–1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2005|edition=Third Revised|isbn=1-59114-119-2}}
* {{cite book|last=Weal|first=John|title=Junkers Ju 87 Stukageschwader 1937–41|year=1997|publisher=Osprey|location=Oxford, England|isbn=978-1-85532-636-1}}
* {{cite book|last=Whitley|first=M. J.|title=Destroyers of World War 2|publisher=Naval Institute Press|year=1988|isbn=0-87021-326-1|location=Annapolis, Maryland}}

==External links==
* [http://www.wrecksite.eu/wreck.aspx?94 HMS ''Brazen'' wrecksite]

<!-- non-breaking space to keep AWB drones from altering the space before the navbox-->

{{A class destroyer}}
{{July 1940 shipwrecks}}

{{DEFAULTSORT:Brazen (H80)}}
[[Category:A- and B-class destroyers]]
[[Category:Tyne-built ships]]
[[Category:1930 ships]]
[[Category:World War II destroyers of the United Kingdom]]
[[Category:World War II shipwrecks in the English Channel]]
[[Category:Destroyers sunk by aircraft]]
[[Category:Maritime incidents in July 1940]]
[[Category:Ships sunk by German aircraft]]