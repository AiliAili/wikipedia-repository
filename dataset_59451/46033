{{Infobox Hindu leader
| name           = Robert Adams
| image          = File:Robert_Adams_-_Los_Angeles_-_early_1990s.jpg
| image_size     = 
| alt            = 
| caption        = Robert Adams in the early 1990s
| birth_date     = January 21, 1928
| birth_place    = [[New York City]], US
| death_date     = March 2, 1997 (aged 69)
| death_place    = [[Sedona]], [[Arizona]], US
| nationality    = American
| honors         = 
| founder        = <!-- Sects/Traditions/Institutions founded by the Hindu leader-->
| sect           = <!-- Sects/Traditions associated with the Hindu leader-->
| order          = 
| guru           = [[Bhagavan Sri Ramana Maharshi]]
| philosophy     = [[Advaita Vedanta]]
| literary_works = ''Silence of the Heart: Dialogues with Robert Adams''
| disciple       = <!-- Prominent disciple(s) only -->
| influenced     = <!-- People the Hindu leader influenced (as mentioned in main article) -->
| signature      = <!--Preferably uploaded on wikipedia commons-->
| signature_alt  = 
| footnotes      = 
}}
{{Advaita}}
'''Robert Adams''' (January 21, 1928 – March 2, 1997) was an American [[Non-dualism|Advaita]] teacher. In his late teens, he was a devotee of [[Sri Ramana Maharshi]] in [[Tiruvannamalai]], India.{{sfn|Ganesan|1993|p=22}} In later life Adams held [[satsang]] with a small group of devotees in [[California]], US.{{sfn|Ramamani|1997|p=94}} He mainly advocated the path of [[jnana yoga|jñāna yoga]]{{refn|group=note|Jñāna means 'knowledge' in [[Sanskrit]]. The pronunciation can be approximated to 'gyaan yoga'.}} with an emphasis on the practice of [[self-enquiry]].{{sfn|Parker|2009|p=32}}

Adams' teachings were not that well known in his lifetime, but have since been widely circulated amongst those investigating the [[philosophy]] of Advaita and the Western devotees of Bhagavan Sri Ramana Maharshi.{{sfn|Waite|2010|p=13}}{{refn|group=note|[[Bhagavan]] means God, Sri is an honorific title, Ramana is a short form of Venkataraman, and Maharshi means 'great seer' in Sanskrit.}} A book of his teachings, ''Silence of the Heart: Dialogues with Robert Adams,'' was published in 1999.

== Biography ==

=== Early life ===

Robert Adams was born on January 21, 1928 in [[Manhattan]]{{sfn|Ganesan|1993|p=21}} and grew up in [[New York City]], USA.{{sfn|Muzika|1998|p=86}} Adams claimed that from as far back as he could remember, he had had [[Vision (spirituality)|visions]] of a white haired, bearded man seated at the foot of his bed, who was about two feet tall, and who used to talk to him in a language which he did not understand.{{sfn|Krushel|2010|p=40}} He told his parents but they thought he was playing games. He would later find out that this man was a vision of his future [[guru]] Sri Ramana Maharshi. At the age of seven, Adams's father died and the visitations suddenly stopped.{{sfn|Ganesan|1993|p=21}}

Adams said that he then developed a [[siddhi]] whereby whenever he wanted something, from a candy bar to a violin, all he needed to do was say '[[God]]' three times and the desired object would appear from somewhere, or be given to him by someone.{{sfn|Ullman|2001|p=191}} If there was a test at school Adams would simply say 'God, God, God,' and the answers would immediately come to him; no prior study was necessary.{{sfn|Ganesan|1993|p=21}}

=== Awakening ===
{{Main|Enlightenment (spiritual)}} 
Adams claimed to have had a profound [[Enlightenment (spiritual)|spiritual awakening]] at the age of fourteen. It was the end of term finals maths test{{sfn|Ramamani|1997|p=94}} and Adams had not studied for it at all. As was his custom he said 'God' three times, but with a phenomenal and unintended outcome:{{sfn|Ullman|2001|p=191}}

{{quote|Instead of the answers coming, the room filled with light, a thousand times more brilliant than the sun. It was like an atomic bomb, but it was not a burning light. It was a beautiful, bright, shining, warm glow. Just thinking of it now makes me stop and wonder. The whole room, everybody, everything was immersed in light. All the children seemed to be myriads particles of light. I found myself melting into radiant being, into consciousness. I merged into consciousness. It was not an out of body experience. This was completely different. I realised that I was not my body. What appeared to be my body was not real. I went beyond the light into pure radiant consciousness. I became omnipresent. My individuality had merged into pure absolute bliss. I expanded. I became the universe. The feeling is indescribable. It was total bliss, total joy. The next thing I remembered was the teacher shaking me. All the students had gone. I was the only one left in the class. I returned to human consciousness. That feeling has never left me.{{sfn|Ganesan|1993|p=21}}}}
Not long after this experience, Adams went to the school library to do a book report. While passing through the philosophy section he came across a book on [[yoga]] masters. Having no idea what yoga was, he opened the book and for the first time saw a photo of the man he had experienced visions of as a young child, Bhagavan Sri Ramana Maharshi.{{sfn|Ganesan|1993|p=21}}
[[File:Paramahansa Yogananda Standard Pose.jpg|thumb|150px|[[Paramahansa Yogananda]]]]

=== Journey to the Guru ===
{{Main|Guru}} 
At the age of 16, Adams' first [[Spirituality|spiritual]] mentor was [[Joel S. Goldsmith]], a [[Christian mysticism|Christian mystic]] from New York,{{sfn|Ganesan|1993|p=22}} who he used to visit in Manhattan, in order to listen to his sermons. Goldsmith helped Adams to better understand his enlightenment and advised him to go and see [[Paramahansa Yogananda]]. Adams did so and visited Yogananda at the [[Self-Realization Fellowship]] in [[Encinitas]], California, where he intended to be initiated as a [[monk]].{{sfn|Ganesan|1993|p=22}}
However, after speaking to him, Yogananda felt that Adams had his own path and should go to India.{{sfn|Krushel|2010|p=40}} He told him that his [[satguru]] was Sri Ramana Maharshi and that he should go to him as soon as possible because Ramana Maharshi's body was old and in ill-health. Sri Ramana Maharshi lived at [[Sri Ramana Ashram|Sri Ramanasramam]] at the foot of [[Arunachala]] in [[Tamil Nadu]], [[South India]].{{sfn|Ganesan|1993|p=22}}

=== Ramana Maharshi ===
{{Main|Ramana Maharshi}} 
With $14,000 of inheritance money from a recently deceased aunt, Adams set off for India and his guru Sri Ramana Maharshi in 1946:{{sfn|Ramamani|1997|p=94}}

{{quote|When I was eighteen years old, I arrived at Tiruvannamalai. In those days they didn’t have jet planes. It was a propeller plane. I purchased flowers and a bag of fruit to bring to Ramana. I took the rickshaw to the ashram. It was about 8:30 a.m. I entered the hall and there was Ramana on his couch reading his mail. It was after breakfast. I brought the fruit and the flowers over and laid them at his feet. There was a guardrail in front of him to prevent fanatics from attacking him with love. And then I sat down in front of him. He looked at me and smiled, and I smiled back. I have been to many teachers, many saints, many sages. I was with Nisargadatta, Anandamayi Ma, Papa Ramdas, Neem Karoli Baba and many others, but never did I meet anyone who exuded such compassion, such love, such bliss as Ramana Maharshi.<ref group=web name="ramana">[http://sri-ramana-maharshi.blogspot.co.uk/2008/06/robert-adams-again.html Godman, David (2008, June 11th), Arunachala and Sri Ramana Maharshi Blog - ''Robert Adams Again'']</ref>}}
[[File:Ramana 3 sw.jpg|170px|left|thumb|[[Bhagavan Sri Ramana Maharshi]]]]

Adams stayed at Sri Ramanasramam for the final three years of Sri Ramana Maharshi's life.{{sfn|Whitwell|2004|p=84}} Over the course of this time he had many conversations with Sri Ramana Maharshi, and through abiding in his presence was able to confirm and further understand his own experience of awakening to the non-dual [[Ātman (Hinduism)|''Self'']].{{sfn|Ganesan|1993|p=22}}
After Sri Ramana Maharshi left the body in 1950 Adams spent a further seventeen years travelling around India{{refn|group=note|[[Arthur Osborne (writer)|Arthur Osborne]], a writer and devotee of Sri Ramana Maharshi, was reputed to have given some money to Adams in order for him to travel around India after Sri Ramana Maharshi died.{{sfn|Ramamani|1997|p=94}}}} and stayed with well known gurus such as [[Nisargadatta Maharaj]],{{refn|group=note|Adams is said to have stayed six months with Nisargadatta Maharaj, at the time when [[Ramesh Balsekar]] was his interpreter.{{sfn|Ganesan|1993|p=23}}}} [[Anandamayi Ma]], [[Neem Karoli Baba]] and [[Swami Ramdas]] to name but a few. He also spent time with less well-known teachers such as Swami Brahmananda "the Staff of God" in the [[Sapta Puri|holy city]] of [[Varanasi]].{{sfn|Ganesan|1993|p=25}}

=== Later years ===

In the 1960s Adams returned to the United States and lived in [[Hawaii]] and [[Los Angeles]] before finally moving to [[Sedona]], [[Arizona]]{{sfn|Muzika|1998|p=138}} in the mid 1990s.{{refn|group=note|Clear biographical details of Adams' life, from the 1950s up until the 1990s, are hard to come by as he rarely talked about his past.{{sfn|Ramamani|1997|p=93}}}} He was married to Nicole Adams and fathered two daughters. In the 1980s Adams developed [[Parkinson's Disease]],{{sfn|Muzika|1998|p=89}} which forced him to settle in one location and receive the appropriate care.{{sfn|Ganesan|1993|p=25}} A small group of devotees soon grew up around him and in the early 1990s he gave weekly [[satsang]]s in the [[San Fernando Valley]], along with other surrounding areas of Los Angeles.{{sfn|Ramamani|1997|p=94}} These satsangs were both recorded and transcribed.{{sfn|Waite|2010|p=380}} After several years of deteriorating health, Adams died on March 2, 1997{{sfn|Ramamani|1997|p=94}} in Sedona, Arizona, where he was surrounded by family members and devotees. He died at the age of 69 from [[cancer of the liver]].{{sfn|Muzika|1998|p=85}}

== Teachings ==

=== Confessions of a Jnani ===
{{See also|Jnana Yoga|Jnana}} 
{{Rquote|right|The teacher is really yourself. You have created a teacher to wake you up. The teacher would not be here if you were not dreaming about the teacher. You have created a teacher out of your mind in order to awaken, to see that there is no teacher, no world - nothing. You've done this all by yourself.|Robert Adams|''The Mountain Path''{{sfn|Ganesan|1993|p=24}}}}

Adams did not consider himself to be a teacher, a philosopher or a preacher.{{sfn|Whitwell|2004|p=84}} What he imparted he said was simply the confession of a [[Jnana yoga|jnani]].{{sfn|Ullman|2001|p=196}} He said he confessed his and everyone else's own [[Brahman|reality]], and encouraged students not to listen to him with their heads but with their hearts. Adams' way of communicating to his devotees was often funny,{{sfn|Dasarath|2002|p=53}} and with interludes of silence or music between questions and answers. He stated that there was no such thing as a new teaching. This knowledge could be found in the [[Upanishads]], the [[Vedas]] and other [[List of Hindu scriptures|Hindu scriptures]].{{refn|group=note|The [[Ashtavakra Gita]], for instance, was a beloved scripture of both Adams and Sri Ramana Maharshi.{{sfn|Jacobs|1999|p=205}}}}

=== Silence of the Heart ===
{{See also|Vimalakirti Sutra}}
Adams did not write any books himself nor publish his teachings as he did not wish to gain a large following. He instead preferred to teach a small number of dedicated seekers.{{sfn|Ganesan|1993|p=25}} However, in 1992, a book of his dialogues was transcribed, compiled and distributed by and for the sole use of his devotees.<ref group=web name="silence">[http://www.wearesentience.com/silence-of-the-heart-first-edition.html Muzika, Ed, (1992) Photographs of First Edition - ''Silence of the Heart - First Edition'']</ref>{{refn|group=note|After being shown the first edition of ''Silence of the Heart'' in 1994, [[H. W. L. Poonja]] read out the whole book at his satsang in [[Lucknow]]. Something he never did for any other living teacher.{{sfn|Premananda|2009|p=67}}}} In 1999, a later edition of this book, ''Silence of the Heart: Dialogues with Robert Adams'', was posthumously published by Acropolis Books Inc.{{refn|group=note|Acropolis Books is a publishing company, set up in 1993, with the sole purpose of publishing and preserving the books of Adams' first spiritual mentor, Joel S. Goldsmith.}} As conveyed by the title of these dialogues, Adams considered [[Silence#In spirituality|silence]] to be the highest of spiritual teachings:{{sfn|Parker|2009|p=28}}

{{quote|The highest teaching in the world is silence. There is nothing higher than this. A devotee who sits with a Sage purifies his mind just by being with the Sage. The mind automatically becomes purified. No words exchanged, no words said. Silence is the ultimate reality. Everything exists in this world through silence. True silence really means going deep within yourself to that place where nothing is happening, where you transcend time and space. You go into a brand new dimension of nothingness. That's where all the power is. That's your real home. That's where you really belong, in deep Silence where there is no good and bad, no one trying to achieve anything. Just being, pure being.{{sfn|Ganesan|1993|p=25}}}}

=== Advaita Vedanta ===
{{Main|Advaita Vedanta|Brahman}}
{{listen|type=talk
 | filename     = Robert adams clip 2 - now what does this have to do with you.ogg
 | title        = Robert Adams - I Am That
 | description  = Robert Adams talking to students at satsang (4 November 1990).
}}
Although Adams was never [[initiated]] into a [[Matha|religious order]] or [[spiritual practice]], nor became a [[Sannyasa|renunciate]], his teachings were described by Dennis Waite as being firmly based in the [[Vedic]] philosophy and [[Hinduism|Hindu]] tradition of [[Advaita Vedanta]].{{sfn|Waite|2010|p=13}} Advaita (''non-dual'' in [[sanskrit]]) refers to the ultimate and supreme reality, [[Brahman]],{{refn|group=note|[[Brahman]], with the accent on the second syllable, is not to be confused with the Hindu god [[Brahmā]] nor the Hindu class/caste [[Brahmin]].}} which according to Ramana Maharshi, as interpreted by some of his devotees, is the substratum of the manifest [[universe]],{{sfn|Maharshi|1923|p=1-2}} and if describable at all, could be defined as pure consciousness. Another term for Brahman is [[Ātman (Hinduism)|Ātman]].{{sfn|Maharshi|1923|p=1-2}} The word Ātman is used when referring to Brahman as the inmost spirit of man. Ātman and Brahman are not different realities, but identical in nature.{{sfn|Maharshi|1923|p=1-2}} Adams used a metaphor to explain this:

{{quote|A clay pot has space inside of it and outside of it. The space inside is not any different from the space outside. When the clay pot breaks, the space merges the inside with the outside. It's only space. So it is with us. Your body is like a clay pot, and it appears you have to go within to find the truth. The outward appears to be within you. The outward is also without you. There's boundless space. When the body is transcended, it's like a broken clay pot. The Self within you becomes the Self outside of you ... as it's always been. The Self merges with the Self. Some people call the inner Self the Ātman. And yet it is called Brahman. When there is no body in the way, the Atman and the Brahman become one ... they become free and liberated.{{sfn|Adams|1999|p=277}}}}

Those in search of [[Moksha|liberation]] from the manifest world will gain it only when the [[mind]] becomes quiescent. The world is in fact nothing other than the creation of the mind, and only by the removal of all thoughts, including the 'I' thought, will the true reality of Brahman shine forth.{{sfn|Maharshi|1923|pp=1&ndash;2}} Adams taught self-enquiry, as previously taught by Sri Ramana Maharshi, in order to achieve this.

=== Self-enquiry ===
[[File:Robert_Adams_-_Empty_-_by_Jane_Adams.jpg|thumb|210px|Sketch of Robert Adams in 1996.]]
{{Main|Self-enquiry|Vichara}}
In his weekly satsangs Adams advocated the practice of [[self-enquiry]] (''ātma-vichāra''){{sfn|Waite|2010|p=380}} as the principal means of transcending the [[Ahamkara|ego]] and realising oneself as [[Satchitananda|sat-chit-ananda]] (''being-consciousness-bliss''). After acknowledging to oneself that one exists, and that whether awake, dreaming or in deep sleep one always exists, one then responds to every thought that arises with the question "Who am I?":{{sfn|Wilson|2011|pp=161&ndash;162}}

{{quote|What you are really doing is, you’re finding the source of the 'I'. You're looking for the source of 'I', the personal 'I'. 'Who am I?' You're always talking about the personal 'I'. 'Who is this I? Where did it come from? Who gave it birth?' Never answer those questions. Pose those questions, but never answer them ... do nothing, absolutely nothing. You're watching the thoughts come. As soon as the thoughts come, in a gentle way you enquire, 'To whom do these thoughts come? They come to me. I think them. Who is this I? Where did it come from? How did it arise? From where did it arise? Who is the I? Who am I?' You remain still. The thoughts come again. You do the same thing again and again and again.{{sfn|Adams|1999|pp=27&ndash;29}}}}

=== Four Principles of Self-Realization ===
{{See also|Lankavatara Sutra}} 
Adams rarely gave a [[sadhana]] to his devotees, however, he did often have visions,{{sfn|Ganesan|1993|p=25}}{{refn|group=note|Adams would explain that visions were not the same as dreams. A vision is an actual experience that occurs in the phenomenal world. After Sri Ramana Maharshi died, Adams would have visions of them walking along the [[Ganges]] together and discussing simple things like the weather.{{sfn|Ganesan|1993|p=25}}}} and in one such vision he gave a teaching as the [[Buddha]]. He visualised himself sitting under a tree in a beautiful open field with a lake and a forest nearby. He was wearing the [[Kasaya (clothing)|orange garb]] of a [[Bhikkhu|Buddhist renunciate]]. All of a sudden hundreds of [[bodhisattvas]] and [[Mahāsattva|mahasattvas]] came out of the forest and sat down in a semi-circle around Adams as the Buddha. Together they proceeded to [[meditate]] for several hours. Afterwards, one of the bodhisattvas stood up and asked the Buddha what he taught. The Buddha answered, "I teach Self-realization of [[Buddhist wisdom|Noble Wisdom]]." Again they sat in silence for three hours before another bodhisattva stood up and asked how one could tell whether they were close to [[Self-realization#Eastern understanding|self-realization]]. In reply, Adams as the Buddha, gave the bodhisattvas and mahasattvas four principles, which he named ''The Four Principles of Self-Realization of Noble Wisdom'':{{sfn|Adams|1999|p=222}}<ref group=web name="robert">[http://sri-ramana-maharshi.blogspot.co.uk/2008/06/robert-adams-on-self-enquiry.html Godman, David (2008, June 5th), Arunachala and Sri Ramana Maharshi Blog - ''Robert Adams On Self-Enquiry'']</ref> 
[[File:Raja Ravi Varma - Sankaracharya.jpg|thumb|225px|[[Adi Shankara]] with Disciples, by [[Raja Ravi Varma]], 1904.]]
*'''First Principle:''' You have a feeling, a complete understanding that everything you see, everything in the universe, in the world, emanates from your mind. In other words, you feel this. You do not have to think about it, or try to bring it on. It comes by itself. It becomes a part of you. The realization that everything you see, the universe, people, worms, insects, the mineral kingdom, the vegetable kingdom, your body, your mind, everything that appears, is a manifestation of your mind.{{sfn|Adams|1999|p=222}}
*'''Second Principle:''' You have a strong feeling, a deep realization, that you are unborn. You are not born, you do not experience a life, and you do not disappear, you do not die ... You exist as I Am. You have always existed and you will always exist. You exist as pure intelligence, as absolute reality. That is your true nature. You exist as sat-chit-ananda. You exist as bliss consciousness ... But you do not exist as the body. You do not exist as person, place or thing.{{sfn|Adams|1999|p=230}}
*'''Third Principle:''' You are aware and you have a deep understanding of the egolessness of all things; that everything has no ego. I'm not only speaking of sentient beings. I'm speaking of the mineral kingdom, the vegetable kingdom, the animal kingdom, the human kingdom. Nothing has an ego. There is no ego ... It means that everything is sacred. Everything is God. Only when the ego comes, does God disappear ... When there is no ego, you have reverence for everybody and everything ... There is only divine consciousness, and everything becomes divine consciousness.{{sfn|Adams|1999|pp=237&ndash;238}}
*'''Fourth Principle:''' You have a deep understanding, a deep feeling, of what self-realization of noble wisdom really is ... You can never know by trying to find out what it is, because it’s absolute reality. You can only know by finding out what it is not. So you say, it is not my body, it is not my mind, it is not my organs, it is not my thoughts, it is not my world, it is not my universe, it is not the animals, or the trees, or the moon, or the sun, or the stars, it is not any of those things. When you've gone through everything and there's nothing left, that's what it is. Nothing. Emptiness. Nirvana. Ultimate Oneness.{{sfn|Adams|1999|pp=242&ndash;243}}

== Publications ==

*Adams, Robert (1999). ''Silence of the Heart: Dialogues with Robert Adams'', Acropolis Books Inc. ISBN 978-1889051536

== See also ==
{{Portal|Hinduism|Indian religions|Spirituality|Yoga}}
{{div col|cols=2}}
*[[Ramana Maharshi]]
*[[Paramahansa Yogananda]]
*[[Joel S. Goldsmith]]
*[[Nisargadatta Maharaj]]
*[[Anandamayi Ma]]
*[[Neem Karoli Baba]]
*[[Paramacharya]]
*[[Advaita Vedanta]]
*[[Brahman]]
*[[Self-enquiry]]
*[[Jnana Yoga]]
*[[Lankavatara Sutra]]
*[[Siddhi]]
{{div col end}}

== References ==

===Notes===
{{reflist|group=note|colwidth=50em}}

=== Citations ===

{{Reflist|30em}}

=== Sources ===

==== Published sources ====

{{refbegin}}
* {{Citation | last =Adams | first =Robert | year =1999 | title =Silence of the Heart: Dialogues with Robert Adams | place = USA | publisher =Acropolis Books Inc. ISBN 978-1889051536}}
* {{Citation | last =Dasarath | first = | year =2002 | title =Freedom Dreams | place = | publisher =Book Tree. ISBN 978-1585091300}}
* {{Citation | last =Ganesan | first =V. | year =June 1993  | title =The Mountain Path - Silence is the Ultimate Reality - Aradhana Issue, Vols 30 Nos. 1 & 2  | place =Tiruvannamalai, India | publisher =T. N. Venkataraman, Sri Ramana Ashram | url =https://s3.amazonaws.com/ramanafiles/mountainpath/1993%20Aradhana.pdf}}
* {{Citation | last =Jacobs | first =Alan | year =1999 | title =The Principal Upanishads | place = | publisher =Watkins Publishing. ISBN 978-1905857081}}
* {{Citation | last =Krushel | first =Renee M. | year =2010 | title =The Spiritual Evolution of a Mystic | place = | publisher =iUniverse. ISBN 978-1-45026-907-0}}
* {{Citation | last =Maharshi | first =Bhagavan Sri Ramana | year =1923 | title =Who Am I? (Nan Yar?) | place =Tiruvannamalai, India | publisher =Sri Ramanasramam | url =http://www.sriramanamaharshi.org/wp-content/uploads/2012/12/who_am_I.pdf}}
* {{Citation | last =Muzika | first =Edward | year =February 1998 | title =Yoga Journal - The Mysterious Sage of Sedona - Issue 138  | place =California, USA | publisher =California Yoga Teachers Association | url =http://www.wearesentience.com/yoga-journal-article.html}}
* {{Citation | last =Parker | first =John W. | year =2009 | title =Dialogues With Emerging Spiritual Teachers | place = | publisher =Sagewood Press. ISBN 978-0970365903}}
* {{Citation | last =Premananda | first = | year =2009 | title =Arunachala Shiva | place = | publisher =Open Sky Press. ISBN 978-0955573064}}
* {{Citation | last =Ramamani | first = | year =June 1997 | title =The Mountain Path - Obituary - Aradhana Issue, Vols 34 Nos. 1 & 2 | place =Tiruvannamalai, India | publisher =V.S Ramanan, Sri Ramana Ashram | url =https://s3.amazonaws.com/ramanafiles/mountainpath/1997%20Aradhana.pdf}}
* {{Citation | last =Ullman | first =Robert | year =2001 | title =Mystics, Masters, Saints, and Sages | place = USA | publisher =Conari Press. ISBN 978-1573245074}}
* {{Citation | last =Waite | first =Dennis | year =2010 | title =The Book of One | place = | publisher =O Books. ISBN 978-1846943478}}
* {{Citation | last =Whitwell | first =Mark | year =2004 | title =Yoga of Heart | place =USA | publisher =Lantern Books. ISBN 978-1590560686}}
* {{Citation | last =Wilson | first =Carol A. | year =2011 | title =Healing Power Beyond Medicine | place = | publisher =O Books. ISBN 978-1846943973}}
{{refend}}

==== Web-sources ====

{{reflist|group=web}}

== External links ==
* [http://keep-quiet.com/robert-adams.php Keep Quiet] - a website with quotes by Robert Adams
* {{YouTube|KIo0AbN8LzA|David Godman - talking about Robert Adams}}

{{Indian Philosophy}}
{{Portalbar|Hinduism|Indian religions|Spirituality|Yoga}}

{{DEFAULTSORT:Adams, Robert}}
[[Category:1928 births]]
[[Category:1997 deaths]]
[[Category:Advaitin philosophers]]
[[Category:American spiritual teachers]]
[[Category:Nondualism]]
[[Category:Spiritual teachers]]
[[Category:People from New York City]]
[[Category:Neo-Advaita teachers]]