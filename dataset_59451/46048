An '''advanced electronic signature''' is an [[electronic signature]] that has met the requirements set forth under [[eIDAS|EU Regulation No 910/2014 (eIDAS-regulation)]] on electronic identification and trust services for electronic transactions in the [[internal market]].<ref name="Turner-advancedESig">{{cite web|last1=Turner|first1=Dawn M.|title=Advanced Electronic Signatures for eIDAS|url=http://www.cryptomathic.com/news-events/blog/advanced-electronic-signatures|publisher=Cryptomathic|accessdate=12 May 2016}}</ref>

==Description==

eIDAS created standards for the use of [[electronic signature]]s so that they could be used in a secure manner when conducting [[business]] online, such as an [[electronic fund transfer]] or official business across borders with [[Member state of the European Union|EU Member States]].<ref name="Forget-eIDAS">{{cite web|last1=Forget|first1=Guillaume|title=The eIDAS regulation is coming. How can banks benefit from it?|url=http://www.cryptomathic.com/news-events/blog/the-eidas-regulation-is-coming.-how-can-banks-benefit-from-it|publisher=Cryptomathic|accessdate=12 May 2016}}</ref> The advanced electronic signature is one of the standards outlined in eIDAS.

For an electronic signature to be considered as advanced, it must meet several requirements:<ref name="eIDAS-Regulation">{{cite web|last1=THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION|title=Regulation (EU) No 910/2014 of the European Parliament and of the Council of 23 July 2014 on electronic identification and trust services for electronic transactions in the internal market and repealing Directive 1999/93/EC|url=http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2014.257.01.0073.01.ENG|publisher=EUR-Lex|accessdate=12 May 2016}}</ref><ref name="UK-ESig">{{cite web|last1=Department for Business Innovation & Skills|title=Electronic Signatures (Guide)|url=https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/356786/bis-14-1072-electronic-signatures-guide.pdf|publisher=The Government of the United Kingdom|accessdate=12 May 2016}}</ref>
# The [[signature|signatory]] can be uniquely identified and linked to the signature
# The signatory must have sole control of the [[Public-key cryptography|private key]] that was used to create the electronic signature
# The signature must be capable of identifying if its accompanying data has been tampered with after the message was signed
# In the event that the accompanying data has been changed, the signature must be invalidated

Advanced electronic signatures that are compliant with eIDAS may be technically implemented through three digital signature standards that have been developed by the [[European Telecommunications Standards Institute]] (ETSI):<ref name="eIDAS-Regulation" />
* [[XAdES]]
* [[PAdES]]
* [[CAdES (computing)|CAdES]]

==Vision==

The implementation of advanced electronic signatures under the specification of eIDAS serves several purposes. Business and [[public service]]s processes, even those that go across borders can be safely expedited by using electronic signing. With eIDAS, EU States are required to establish “points of single contact” (PSCs) for trust services that ensure the electronic ID schemes can be used in public sector transactions that occur cross-borders, including access to healthcare information across borders.<ref name="eIDAS-Regulation" />

In the past, when signing a document or message, the signatory would sign it and then return it to its intended recipient through the postal service, via [[facsimile]] service, or by scanning and attaching it to an email. This could lead to delays and of course, the possibility that signatures could be forged and documents altered, especially when multiple signatures from different people located in different locations are required. The process of using an advanced electronic signature saves time, is legally binding and assures a high level of technical security.<ref name="eIDAS-Regulation" /><ref>{{cite web|last1=Mazzeo|first1=Mirella|title=Digital Signatures and European Laws|url=http://www.symantec.com/connect/articles/digital-signatures-and-european-laws|publisher=Symantec|accessdate=12 May 2016}}</ref>

==Legal implications of advanced electronic signatures==

Following Article 25 (1) of the eIDAS regulation,<ref name="eIDAS-Regulation" /> an advanced electronic signature shall “not be denied legal effect and admissibility as evidence in legal proceedings". However it will reach a higher [[Relevance (law)|probative value]] when enhanced to the level of a [[qualified electronic signature]]. By adding a certificate that has been issued by a qualified [[trust service provider]] that attests to the authenticity of the qualified signature, the upgraded advanced signature then carries according to Article 24 (2) of the eIDAS Regulation<ref name="eIDAS-Regulation" /> the same legal value as a handwritten signature.<ref name="Turner-advancedESig" /> However, this is only regulated in the European Union and similarly through [[ZertES]] in [[Switzerland]]. A qualified electronic signature is not defined in the United States.<ref name="TurnerDSS">{{cite web|last1=Tuner|first1=Dawn M.|title=Is the NIST Digital Signature Standard DSS legally binding?|url=http://www.cryptomathic.com/news-events/blog/is-the-nist-digital-signature-standard-dss-legally-binding|publisher=Cryptomathic|accessdate=12 May 2016}}</ref><ref name="DSS-Ref">{{cite web|last1=Information Technology Laboratory National Institute of Standards and Technology|title=FEDERAL INFORMATION PROCESSING STANDARDS  PUBLICATION (FIPS PUB 186 -4): Digital Signature Standard (DSS)|url=http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.186-4.pdf|accessdate=12 May 2016}}</ref>

==References==
{{reflist}}


[[Category:Cryptography standards]]
[[Category:XML-based standards]]
[[Category:Regulation]]