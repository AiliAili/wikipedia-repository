{{Infobox military person
|name= Odas Moon
|birth_date= February 11, 1892<ref>Ancestry.com. Virginia, Death Records, 1912-2014 [database on-line]. Provo, UT, USA: Ancestry.com Operations, Inc., 2015.</ref>
|death_date= November 19, 1937<ref>''U.S. Army Register'', p. 829. U.S. Government Printing Office, 1938</ref>
|birth_place=
|death_place=[[Old Point Comfort]], [[Virginia]]
|boyhood home= 
|placeofburial=
|placeofburial_label= 
|image= Odas Moon 1929.jpg
|caption=Odas Moon in 1929
|nickname= 
|allegiance= {{flagcountry|United States}}
|branch= [[File:USAAC Roundel 1919-1941.svg|25px]] [[United States Army Air Service|U.S. Army Air Service]]<br />[[File:Us army air corps shield.svg|25px]] [[United States Army Air Corps|U.S. Army Air Corps]]
|serviceyears= 1917–1937
|rank= [[File:US-O4 insignia.svg|20px]] [[Major (United States)|Major]]}}

'''Odas Moon''' (February 11, 1892 – November 19, 1937) was an American [[aviation]] pioneer who was among a team of [[United States Army Air Corps]] (USAAC) aviators to break endurance records by performing [[aerial refueling]]. Moon was a founding member of the [[Order of Daedalians]].<ref name=Daedalians/> Through his teaching and leadership at the [[Air Corps Tactical School]], Moon helped shape and promote the concept of daylight precision bombing, using [[heavy bomber]]s.

==Early career==
Along with many other [[United States Army Air Service]] fliers, Moon was assigned to fly patrols along the [[Mexico – United States border]] from May to September, 1919.<ref>Hinkle, Stacy C. ''Wings and saddles: the Air and Cavalry Punitive Expedition of 1919''. University of Texas at El Paso. Southwestern Studies, Volume V, No. 3, Monograph Number 19. Editor: Samuel D. Myres. Texas Western Press, 1967, pp. 8, 11, 23.</ref> As part of his duties, Moon ferried a Mexican colonel back to [[Puerto Palomas, Chihuahua]], Mexico.<ref>Hinkle, Stacy C. ''Wings Over the Border: The Army Air Service Armed Patrol of the United States &ndash; Mexico Border 1919&ndash;1921''. University of Texas at El Paso. Southwestern Studies, Monograph Number 26. Texas Western Press, 1970, pp. 21, 26.</ref>

In January 1924, Moon was flying a delivery of mail to an American Army base in the [[Panama Canal Zone]] when he discovered an American invasion fleet lying in wait {{convert|125|mi|-1}} away from Panama. The fleet was made up of four battleships, a carrier and attendant vessels. Their plan was to surprise and "defeat" the defenders of Panama as part of annual winter maneuvers.<ref name=Holley>Holley, I. B. Jr. (1975) [https://books.google.com/books?id=K4w69q1_IAYC&pg=PA425 "An Enduring Challenge: The Problem of Air Force Doctrine"], within ''The Harmon memorial lectures in military history, 1959-1987: a collection of the first thirty Harmon lectures given at the United States Air Force Academy'', edited by Harry R. Borowski. Diane Publishing, 1988, pp. 425–439. ISBN 0-912799-58-7</ref> Moon was carrying a case of ripe tomatoes to give to his wife, and he proceeded to carry out a series of [[dive bombing]] attacks against the [[USS Langley (CV-1)|USS ''Langley'']], delivering three direct hits from tomato "bombs".<ref name=Holley/> When he landed at the Panama base with the information that the fleet was coming, Moon was congratulated by his superiors. Soon, though, word was passed back regarding an unspoken goal of the surprise invasion fleet: they were there to prove that the Canal Zone needed a $10M purchase of 16-inch coastal defense guns. After the invasion fleet was discovered by an air unit while still 125 miles out, the need for coastal guns was no longer seen as urgent by Congress.<ref name=Holley/> However, air defense doctrine was not re-examined as a result.<ref name=Holley/>

In 1924 or 1925, Moon taught bombardment to new airmen at [[Kelly Field Annex|Kelly Field]] in Texas. Among his students was [[Charles Lindbergh]] who, in 1970,<ref>Heritage Auction Galleries. [http://historical.ha.com/common/auction/catalogprint.php?SaleNo=6031&src= "Charles Lindbergh Typed Letter Signed".] Retrieved on November 3, 2009.</ref> wrote a letter to Senator [[Ralph Yarborough]] of Texas describing his memories of the foundational experience he was given by Moon.

==Aerial refueling==
[[File:Atlantic C-2A refuelled by Douglas C-1 USAF.JPG|thumb|The [[Question Mark (aircraft)|''Question Mark'']] being refueled by a [[Douglas C-1]] above it]]
As a trusted member of the [[7th Bomb Wing|7th Bombardment Group]], First Lieutenant Moon was selected to take part in the endurance demonstration flight of the [[Question Mark (aircraft)|''Question Mark'']], a modified [[Fokker F.VII|Atlantic-Fokker C-2A]]. During January 1–7, 1929, Moon piloted one of two [[aerial refueling]] aircraft, a [[Douglas C-1]] based out of [[Van Nuys Airport]],<ref name=Maurer/> and helped refuel the ''Question Mark'' as it flew for six days back and forth between [[San Diego, California|San Diego]] and [[Van Nuys, California]]. Moon and his crew refueled the ''Question Mark'' sixteen times—two conducted at night.<ref>Smith, ''Seventy-Five Years of Inflight Refueling'', p. 6.</ref> The ''Question Mark'' set a world record for endurance.<ref>''Time'' Magazine, January 14, 1929. [http://www.time.com/time/magazine/article/0,9171,737211,00.html "Aeronautics: Question Mark."] Retrieved on November 2, 2009.</ref>

The USAAC followed up the flight of the ''Question Mark'' with a mission to demonstrate the applicability of aerial refueling in combat. On May 21, 1929, during annual maneuvers, Moon took off from [[Wright-Patterson Air Force Base|Fairfield Air Depot]] in [[Dayton, Ohio]] in a [[Keystone LB-7]] on a simulated mission to [[New York City]] via [[Washington, D.C.]] Plans were for the bomber to be refueled in flight several times, drop a flash bomb over New York harbor and a parachute flare over [[Atlantic City, New Jersey|Atlantic City]],<ref name=Maurer>Maurer, Maurer. ''Aviation in the U.S. Army, 1919–1939, Part 301. United States Air Force, Office of Air Force History. Diane Publishing, 1987, pp. 244–245, 261–262. ISBN 0-912799-38-2</ref> then return to Dayton non-stop, again by way of Washington. The C-1 tanker employed to refuel the LB-7 was forced by icing to land in [[Uniontown, Pennsylvania]], where it got stuck in mud. After flying over [[Manhattan]], Moon circled south to drop three flares in the harbor near the [[Statue of Liberty]]. Moon landed at [[Bolling Field]].<ref>Smith, p. 7.</ref> The next day the tanker joined the bomber and both flew over New York where they made a public demonstration of air refueling and four dry runs.<ref>Robert F. Adams (ed) (1997). ''Defenders of Liberty: 2nd Bombardment Group/Wing 1918–1993'', Turner Publishing, Paducah, Kentucky, ISBN 1-56311-238-8.</ref>

==Order of Daedalians==
Moon was a charter member of the [[Order of Daedalians]], a group of former [[World War I]]-era military pilots who formed the organization on March 26, 1934. Under [[Harold L. George]], Moon served as the first vice commander of the group.<ref name=Daedalians>Order of Daedalians. About Us: [http://www.daedalians.org/about/history.htm "History of the Order of Daedalians".] Retrieved on November 2, 2009.</ref>

==Bomber mafia==
As a first lieutenant, Moon graduated from the class of 1930–1931 at the [[Air Corps Tactical School]] (ACTS).<ref name=Finney>Finney, Robert T. (1998) Air Force History and Museums Program. [http://www.airforcehistory.hq.af.mil/Publications/fulltext/HistoryOfTheAirCorpsTacticalSchool.pdf ''History of the Air Corps Tactical School 1920–1940.] Third imprint. Retrieved on November 3, 2009.</ref> From 1933 at the rank of captain until January 29, 1936, at the rank of major,<ref name=Finney/> Moon was assigned to teach at ACTS. Moon challenged his students to thoroughly examine the bombardment theories he presented so they could discover any flaws. There, Moon joined Harold L. George, [[Eugene L. Eubank]], [[Haywood S. Hansell]] and [[Ralph A. Snavely]] in arguing for the primacy of an independent bomber arm. Moon was vocal in his rebuttal of the arguments used by a group of ACTS teachers who were promoting [[Fighter aircraft|fighters]] as the all-around useful aircraft that the USAAC should invest in. The fighter enthusiasts—[[Claire Chennault]], [[Hoyt S. Vandenberg]] and [[Earle E. Partridge]]—argued that the fighter in large numbers was capable of doing more damage to ground targets than the number of bombers that could be bought for the same money. Moon and the so-called "[[Bomber mafia]]" tore the fighter argument apart and established precision bombing as the war-winning strategy.<ref>Moy, Timothy. ''War machines: transforming technologies in the U.S. military, 1920–1940'', p. 66. Volume 71 of Texas A&M University military history series. Texas A&M University Press, 2001. ISBN 1-58544-104-X</ref>

==Death and legacy==
Moon was relieved of duty at ACTS on January 29, 1936<ref name=Finney/> and died at the age of 45 on November 19, 1937.<ref name=obit>''The New York Times'', November 21, 1937. Obituaries. [https://select.nytimes.com/gst/abstract.html?res=F30B15FD3B59177A93C3AB178AD95F438385F9 "Captain Odas Moon; Army Air Corps Officer Was to Be Retired From Service on Dec. 31"]. Retrieved on November 3, 2009.</ref>  At the time of his death, Moon had been waiting in the Hotel Chamberlin, [[Old Point Comfort]], [[Virginia]] to be retired from active service which was to have taken effect on December 31, 1937.<ref name=obit/>

Retired Air Force Major General [[Eugene L. Eubank]] said in a 1982 interview that Odas Moon, a very close friend of his, "drank himself to death".<ref name=Eubank>Eubank, Eugene L. [http://lanbob.com/lanbob/H-42Auth/EE19CO-I.htm U.S. Air Force Oral History Interview K239.0512-1345. Maj Gen Eugene L. Eubank.] June 30 – July 1, 1982. Retrieved on November 14, 2009.</ref> Eubank said that he and "Odie" Moon had served in the Air Corps together from 1919 on the Mexico–U.S. border flights and had been devoted friends ever since.<ref name=Eubank/> Eubank served as Moon's copilot in May 1929 for the New York aerial refueling demonstration.<ref name=Maurer/>

During [[World War II]], all of Moon's staff mates and commanders at ACTS went on to become influential [[general officer]]s, as did all four officers on board the ''Question Mark'' and two of the officers flying with him in the tankers.<ref name=Turner>''Airlift Tanker: History of U.S. Airlift and Tanker Forces'', p. 53. Turner Publishing. 1995. ISBN 1-56311-125-X</ref>

All the crew members of the ''Question Mark'' were given [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]es, but none of the tanker crews were so honored.<ref name=Turner/> Instead, Moon and the others received letters of commendation.<ref name=Turner/> On May 26, 1976, the two surviving tanker crewmembers—retired brigadier generals [[Ross G. Hoyt]] and [[Joseph G. Hopkins]]—were awarded the DFC for their exceptional contributions to air-to-air refueling by [[Chief of Staff of the United States Air Force]] General [[David C. Jones]].<ref name=Turner/>

==See also==
*[[Aviation history]]

==References==
;Notes
{{reflist|colwidth=30em}}
;Bibliography
{{refbegin}}
*Smith, Richard K. (1998). [http://www.airforcehistory.hq.af.mil/Publications/fulltext/75yrs_inflight_refueling.pdf ''Seventy-Five Years of Inflight Refueling: Highlights 1923–1998''] Air Force History and Museums, Air University, Maxwell AFB.
{{refend}}

==External links==
*[http://digital.hagley.org/cdm4/item_viewer.php?CISOROOT=/p268001coll33&CISOPTR=616&CISOBOX=1&REC=11 1929 photograph of five aviators including Lieutenant Odas Moon]

{{DEFAULTSORT:Moon, Odas}}
[[Category:1892 births]]
[[Category:1937 deaths]]
[[Category:Aerial warfare pioneers]]
[[Category:American aviators]]
[[Category:Air Corps Tactical School alumni]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots]]
[[Category:Place of birth unknown]]
[[Category:Alcohol-related deaths in Virginia]]