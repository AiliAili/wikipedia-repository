{{good article}}
{{Infobox NRHP | name =Battery White
  | nrhp_type =
  | image = Battery White 3.jpg
  | caption = Battery White earthworks.  Winyah Bay is out of the photo to the left.
  | location = 1228 Belle Isle Road<ref name=location/>
  | nearest_city= [[Georgetown, South Carolina]]
  | coordinates = {{coord|33|18|13|N|79|17|38|W|display=inline,title}}
| locmapin = South Carolina#USA
  | area =
  | built = 1862
 | added = November 16, 1977
 | refnum=77001222<ref name=focus/>
}}

'''Battery White''' was an [[artillery battery]] constructed by the [[Confederate States of America|Confederates]] during the [[American Civil War]].  Built in 1862&ndash;63 to defend [[Winyah Bay]] on the [[South Carolina]] coast, the battery was strongly situated and constructed; however, it was inadequately manned, and was captured without resistance during the final months of the war.

The battery is listed in the [[National Register of Historic Places]].  It is located on private land, but is open to the public.

==Island fortifications==

Even before the outbreak of the Civil War, the secessionist government of South Carolina was concerned with the possibility of attack by sea in [[Georgetown County, South Carolina|Georgetown County]].  Shortly after the December 20, 1860 passage of the [[Ordinance of Secession]],<ref name=secession/> an aide-de-camp to governor [[Francis Pickens]] urged [[South Carolina Lowcountry|Lowcountry]] planters to "aid in the erection of Batteries to protect and defend the entrance of Winyah Bay and the [[Santee River]]".<ref name=alston/>

The area offered a tempting target to [[Union (American Civil War)|Union]] forces.  Winyah Bay would furnish a sheltered anchorage large enough for the entire United States Navy of 1861.  The city of [[Georgetown, South Carolina|Georgetown]] on the bay was the largest on the South Carolina coast north of [[Charleston, South Carolina|Charleston]].<ref name=simmons-11/>  Georgetown County produced nearly half of the rice grown in the United States, amounting to some 54 million pounds ({{Convert|54000000|lb|t|abbr=off|disp=output only}}) in 1860; Georgetown exported more rice than any other port in the world.<ref name=half-rice/><ref name=culture/>  This production and shipping could be disrupted by gunboats moving up the [[Black River (South Carolina)|Black]], the [[Pee Dee River|Pee Dee]], the [[Waccamaw River|Waccamaw]], and the [[Sampit River|Sampit]] rivers, which flow into the bay; and the two [[Distributary|distributary channels]] of the Santee River, whose mouths lie just below the bay.<ref name=simmons-11/>  Curtailing rice production would not only damage the local economy, but would impair the Confederacy's ability to feed its armies.<ref name=simmons-46-47/>

[[File:Battery White in Winyah Bay, SC map.svg|thumb|Winyah Bay, including locations of Battery White and sunken [[USS Harvest Moon (1863)|USS ''Harvest Moon'']]]]

In May 1861, General [[P. G. T. Beauregard]] ordered the development of coastal defenses for South Carolina, including batteries situated on three islands flanking the mouth of Winyah Bay: North Island, South Island, and Cat Island.  The [[Battle of Port Royal|Federal capture of Port Royal]] in November 1861 lent urgency to the construction and improvement of these works, which was done under [[Robert E. Lee]], the newly appointed commander of the Department of South Carolina, Georgia, and East Florida, with Colonel [[Arthur Middleton Manigault]] in charge of the district that included Georgetown and [[Horry County, South Carolina|Horry]] counties.<ref name=simmons-13-14/><ref name=lee-appt/>

The island fortifications were never tested against a major Union attack.  However, they served a useful purpose in dealing with grounded ships, both Confederate and Federal; in protecting the entrance to the bay; and in maintaining Confederate possession of the islands.<ref name=simmons-18/>

[[File:John C. Pemberton(hh21b2).jpg|thumb|John C. Pemberton]]

==Withdrawal==

Matters changed in early 1862.  In March of that year, Lee was recalled to [[Richmond, Virginia|Richmond]] as military advisor to President [[Jefferson Davis]].<ref name=lee-remove1/><ref name=lee-remove2/>  He was replaced by General [[John C. Pemberton]],<ref name=pemberton/> who ordered the withdrawal of troops and artillery from the positions around Georgetown, apparently in order to concentrate his limited manpower on shorter defensive lines.<ref name=simmons-103-104/><ref name=pemberton2/>

Union naval forces were quick to take advantage of this new vulnerability.  In May 1862, the gunboats [[USS Albatross (1858)|USS ''Albatross'']] and [[USS Norwich (1861)|USS ''Norwich'']] noted that the island forts were unoccupied.  They landed troops to occupy North Island, which became the principal local Union base for much of the war; and they destroyed the fortifications on South and Cat Islands.<ref name=simmons-19-25/><ref name=albwich/>  They also sailed some {{Convert|10|mi}} up the Waccamaw River, where they raided a mill and carried off 80 slaves.<ref name=simmons-105-106/>  Settled on North Island, these freed slaves formed the nucleus of a colony of "[[Contraband (American Civil War)|contrabands]]" that grew to more than a thousand before being removed to Port Royal for fear of Confederate raids leading to their recapture or massacre.<ref name=simmons-19-20/>

The Federal forces made no attempt to seize territory up the rivers, and their expeditions were limited by the draft of their vessels.  Nevertheless, they conducted a number of raids in which they damaged facilities, seized rice, and released slaves; and these raids severely disrupted the region's economy.  Rice production in particular suffered, since it depended on a labor force of skilled slaves performing carefully timed tasks.<ref name=culture/><ref name=simmons-54-60/>

Pemberton still refused to move artillery and men to the Georgetown area, maintaining that all of his resources were necessary for the protection of Charleston.<ref name=pemberton-chesnut/>  However, his superiors ordered him to construct new fortifications at Winyah Bay.  Since the Union now controlled the islands, it was necessary to find sites further up the bay.  On August 3, 1862, Pemberton visited the area and selected Mayrant's Bluff and Frazier's Point as the sites for the new batteries.<ref name=simmons-56/>

==Battery White==

Later in August 1862, Pemberton was promoted to lieutenant general and sent to the Department of Mississippi and East Louisiana, where he would eventually surrender [[Vicksburg Campaign|Vicksburg]].  He was replaced in the Department of South Carolina and Georgia by Beauregard, who assumed command on September 24, 1862.<ref name=pemberton/><ref name=p-b-1/><ref name=p-b-2/>  The new commander supported the fortification of Winyah Bay: on October 8, 1862, he assured Governor Pickens that he had ordered the construction of a battery of five or six pieces of artillery at Mayrant's Bluff;<ref name=beauregard-to-pickens/> on November 10, he wrote Colonel [[James Chesnut, Jr.]] that the battery was "armed and completed", and that he had sent a new regiment of the State Reserves to General [[James H. Trapier]], in command of the Georgetown District.<ref name=beauregard-to-chesnut/>

[[File:JHTrapierCSA.jpg|thumb|James H. Trapier]]

Trapier was less than pleased with the troops and artillery that he had been given.  The new regiment, he noted, arrived at the battery without arms and ammunition; and as Reserves, would only be in service for 90 days, not enough time to make effective soldiers of them even had they been armed.  He had also been given fairly light guns; what he wanted was [[Columbiad]]s, suitable for defending the battery and the bay against incursions by [[Ironclad warships|ironclads]].<ref name=simmons-109-110/>

Despite the paucity of men and weaponry, the new battery proved effective.  On November 11, 1862, two Union gunboats entered Winyah Bay and began firing on the Mayrant's Bluff works.  The Second South Carolina Artillery, which had set up nine guns in the fortification, responded; and within a few minutes, the Federal vessels were forced to retire.<ref name=simmons-60/>

In February 1863, Trapier reported that the Mayrant's Bluff fortification, now named Battery White, was occupied by only 53 men and nine guns.<ref name=nomform/>  Even this small force sufficed to stop Federal incursions up the rivers of Winyah Bay.  However, rice production in the Georgetown area did not recover: the raids had destroyed too much of the physical plant, carried off too many of the slaves, and created too much uncertainty for planters to return to anything near full production.<ref name=simmons-60/>

The battery continued to lose manpower, despite Trapier's ongoing calls for more troops and guns.<ref name=nomform/>  In October 1864, a body of eleven deserters from the Confederate German Artillery reached the gunboat [[USS Potomska (1861)|USS ''Potomska'']]; they reported that there was great discontent among the troops, and that many would desert were they not so strongly guarded.  They also reported that there were ten guns at the battery; the captain of the ''Potomska'' concluded that the bay was too well defended for him to render aid to prospective deserters.<ref name=potomska/><ref name=potomska2/>

==Capture==

In November 1864, Trapier was ordered to bring most of his forces to [[Mount Pleasant, South Carolina|Mount Pleasant]], just north of Charleston.  Only a company of the German Artillery was left to defend the Georgetown district.  By the end of January 1865, only a small crew commanded by a lieutenant remained at Battery White.<ref name=simmons-116/>

In January and early February 1865, Union forces under General [[William Tecumseh Sherman]] moved northward from Georgia into South Carolina.  On February 2 and 3, they defeated a Confederate force in the [[Battle of Rivers' Bridge]], clearing their pathway into the state.  After a feint at Charleston, they marched to [[Columbia, South Carolina|Columbia]], which they entered on February 17; on the same day, Charleston was evacuated, and on the 18th, it was surrendered.<ref name=sherman/><ref name=charleston-surrender/>

[[File:Battery White Columbiad 1.jpg|thumb|alt=Large cannon under trees hung with Spanish moss|Columbiad at Battery White]]

From Charleston, Union naval forces under Admiral [[John A. Dahlgren]] moved up the coast to Georgetown, which Dahlgren thought might be a useful point of communication with Sherman's land forces.<ref name=dahlgren-report/>  On February 23, deserters told the captain of the gunboat [[USS Mingoe (1863)|USS ''Mingoe'']] that Battery White had been or would soon be evacuated.  The ''Mingoe'' fired four rounds into the battery.  When no response was made, a party was sent ashore; they found the battery abandoned and its guns [[Touch hole#Spiking the guns|spiked]].<ref name=mingoe-report/><ref name=mingoe2/>  The [[sloop-of-war]] [[USS Pawnee (1859)|USS ''Pawnee'']] and the gunboat [[USS Nipsic|USS ''Nipsic'']] continued up the bay and landed a party of Marines to take possession of Georgetown;<ref name=dahlgren-report/><ref name=nipnee/> the [[Intendant#United States|intendant and wardens]] of the city formally surrendered it on February 25.<ref name=intendant/>

Soon thereafter, Dahlgren inspected the battery, and was impressed.  The fortification, he wrote, was well situated and designed, laid out to defend against both shipborne bombardment and attack by landing parties.  Eleven guns bore on the channel: two {{convert|10|in|adj=on|0}} Columbiads, three banded [[rifled]] 32-pounders, four [[smoothbore]] 24-pounders, and two banded rifled 12-pounders.  A 6-pound smoothbore flanked the ditch.  The rear was defended by a "formidable" rampart and ditch, with a 24-pound smoothbore at either end; in the fort were numerous traverses and magazines.  The approach along the beach to the battery's right flank was covered by a 24-pound and a 12-pound smoothbore.  "If the works had been sufficiently manned", wrote Dahlgren, "it would have required good troops to take the work."<ref name=dahlgren-report2/>

===Sinking of the USS ''Harvest Moon''===

By the fall of 1864, the Union fleet had effectively closed Winyah Bay to blockade runners.  Accordingly, the Confederate command had elected to lay [[Naval mine|mines]], at the time known as "torpedoes", in the bay.  Eighteen mines had been constructed in Georgetown by Captain Thomas West Daggett and Stephen W. Rouquie and placed strategically in the bay.<ref name=simmons-146-148/><ref name=daggett/><ref name=rouquie/>

As early as January 1865, Union sympathizers in Georgetown had warned Dahlgren's forces about the existence of mines in the channel.<ref name=simmons-148/>  While approaching Battery White, the ''Mingoe'' had sent its boats out to sweep for such devices.<ref name=mingoe-report/>  However, their efforts may have been perfunctory: according to Dahlgren's report, "...so much has been said in ridicule of torpedoes that very little precautions are deemed necessary, and if resorted to are probably taken with less care than if due weight was attached to the existence of these mischievous things."<ref name=dahlgren-report3/>

[[File:Battery White monument.jpg|thumb|alt=Stone marker 3-4 feet high, inscribed: Lest We Forget/In memory of the Confederate soldiers/who served at Battery White/during the War Between the States/1861-1865/Erected by Arthur Manigault Chapter/United Daughters of the Confederacy/May 25, 1929|United Daughters of the Confederacy monument]]

On the morning of March 1, 1865, Dahlgren's flagship, the side-wheel [[steamboat|steamer]] [[USS Harvest Moon (1863)|USS ''Harvest Moon'']], sailed from Georgetown toward Battery White, which Dahlgren intended to inspect.  En route, the vessel struck one of Daggett and Rouquie's mines, which blew a large hole in it, killing one sailor; the boat quickly sank in two and a half [[fathom]]s of water.<ref name=simmons-148/><ref name=dahlgren-report3/><ref name=moon/>

==Battery White postbellum==

For over a century after the Civil War, the grounds on which Battery White stood were part of the Belle Isle Plantation.  During the late 19th century, extensive landscaping was undertaken on the plantation.  The [[United Daughters of the Confederacy]] erected a memorial stone on the site in 1929.  In about 1946, the plantation gardens, including the battery, were opened to the public, and remained thus until 1974.<ref name=nomform/><ref name=udc/>

In the 1970s, the plantation was developed as a condominium complex.  Portions of the {{convert|100|acre|adj=on}} complex of fortifications were lost to construction.  However, the owners elected to preserve Battery White itself.  In 1977, a {{convert|3|acre|adj=on}} area encompassing the battery was nominated to the [[National Register of Historic Places]].<ref name=nomform/>  In 2008, the site was re-opened to the public.<ref name=open2008/>

The battery's earthworks are for the most part well preserved.  The powder magazines have deteriorated, owing to collapse of the earth mounds following decay of their wooden interior shoring.  The two Columbiads have been re-mounted and once again point out over Winyah Bay.<ref name=nomform/>

[[File:USS Harvest Moon 53685.jpg|thumb|alt=Remains of metal cylinder, 4-6 feet in diameter, protrude from water; three divers in foreground|U.S. Navy divers survey the ''Harvest Moon'', 1963]]

Three of the battery's guns have been placed in Georgetown.  A 24-pound gun has been mounted in front of the National Guard Armory,<ref name=natguard/> and two cannon are displayed in Constitution Park on the Georgetown waterfront.<ref name=conpark/>

The ''Harvest Moon'' was never salvaged, and has gradually sunk deeper into the mud of the bay.  In the mid-1960s, the top deck lay under an estimated six feet ({{Convert|6|ft|disp=output only}}) of mud.<ref name=simmons-150-151/>  A Georgetown group attempted to salvage and restore the vessel as a tourist attraction, and in 1964 the U.S. Navy formally abandoned it, rendering it eligible for private salvage; but the attempt failed for lack of funds.<ref name=salvage/>  As of 2011, the ship's boiler stack was still visible at low tide.<ref name=stack/>

==Notes==
<!-- References are listed alphabetically by "name=" -->
<references>

<ref name=albwich>The ''Albatross'' is described as a "378-ton (burden) wooden screw gunboat" at [http://www.history.navy.mil/photos/sh-usn/usnsh-a/albatros.htm "USS ''Albatross'' (1861-1865)"].  The ''Norwich'' is described as a "431-ton (burden) gunboat" at [http://www.history.navy.mil/photos/sh-usn/usnsh-n/norwich.htm "USS ''Norwich'' (1861-1865)"].  Both websites at [http://www.history.navy.mil/index.html Naval History and Heritage Command]; both retrieved 2014-04-16.</ref>

<ref name=alston>Charles Alston, aide-de-camp to Pickens; December 30, 1860.   Quoted in George C. Rogers, Jr., ''The History of Georgetown County, South Carolina'', University of South Carolina Press, 1970; quote reproduced at [http://batterywhite.org/1860/12/30/battery-white-identified-as-a-strategic-location/ "Battery White Identified as a Strategic Location",] [http://batterywhite.org/ Battery White website.]  Retrieved 2011-09-15.</ref>

<ref name=beauregard-to-pickens>P. G. T. Beauregard to Francis Pickens, October 8, 1862; quoted in Simmons, p. 107.</ref>

<ref name=beauregard-to-chesnut>P. G. T. Beauregard to James Chesnut, Jr., November 10, 1862; quoted in Alfred Roman, ''The military operations of General Beauregard in the war between the states, 1861 to 1865'', [http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A2001.05.0137%3Achapter%3D3 chapter 28].  Retrieved 2011-09-16.</ref>

<ref name=charleston-surrender>[http://www.draytonhall.org/research/history/civil_war.html "The Civil War: How Drayton Hall Survived".]  [http://www.draytonhall.org/ Drayton Hall website.]  Retrieved 2011-09-17.</ref>

<ref name=conpark>[http://www.hmdb.org/Marker.asp?Marker=4860 "These Two Cannons"]  [http://www.hmdb.org/ The Historical Marker Database.]  Retrieved 2011-09-17.</ref>

<ref name=culture>Power, J. Tracy, and Sherry Piland.  [http://www.nationalregister.sc.gov/MPS/MPS031.pdf "National Register of Historic Places Multiple Property Documentation Form: Georgetown County Rice Culture, c. 1750 - c. 1910".]  [http://scdah.sc.gov/ South Carolina Department of Archives and History.]  Retrieved 2011-09-15.</ref>

<ref name=daggett>Lewis, Catherine H.  [http://www.hchsonline.org/bio/twd.html "Thomas West Daggett".]  ''Independent Republic Quarterly'', vol. 27, no. 3 (Summer 1993), p. 19.  Reproduced at [http://www.hchsonline.org/index.html Horry County Historical Society website.]  Retrieved 2011-09-17.</ref>

<ref name=dahlgren-report>Report by Admiral J. A. Dahlgren to Secretary of the Navy Gideon Welles; February 28, 1865.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, pp. 273–74.  Retrieved 2011-09-17.</ref>

<ref name=dahlgren-report2>Report by Admiral J. A. Dahlgren to Secretary of the Navy Gideon Welles; February 28, 1865.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, pp. 277–78.  Retrieved 2011-09-17.</ref>

<ref name=dahlgren-report3>Report by Admiral J. A. Dahlgren to Secretary of the Navy Gideon Welles; March 1, 1865.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, pp. 282–83.  Retrieved 2011-09-17.</ref>

<ref name=focus>Obtained by entering "Battery White" in "Resource Name" search field at [http://nrhp.focus.nps.gov/natreghome.do?searchtype=natreghome NPS Focus.]  Retrieved 2011-09-15.</ref>

<ref name=half-rice>[http://www.cityofgeorgetownsc.com/history/history.cfm "History of Georgetown".]  [http://www.cityofgeorgetownsc.com/index.cfm City of Georgetown website.]  Retrieved 2011-09-15.</ref>

<ref name=intendant>Letter of surrender from R. O. Bush, intendant, and wardens of Georgetown, South Carolina; February 25, 1865.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, p. 275. Retrieved 2011-09-17.</ref>

<ref name=lee-appt>[http://www.researchonline.net/sccw/battles.htm "The War for Southern Independence in South Carolina".]  Retrieved 2011-09-15.</ref>

<ref name=lee-remove1>[http://www.civilwarhome.com/leebio.htm "Robert Edward Lee (1807-1870)".]  [http://www.civilwarhome.com/index.html Home of the American Civil War.]  Retrieved 2011-09-15.</ref>

<ref name=lee-remove2>[http://www.historylearningsite.co.uk/march-1862-civil-war.htm "American Civil War March 1862".]  Retrieved 2011-09-15.</ref>

<ref name=location>[http://batterywhite.org/location/ "Location".]   [http://batterywhite.org/ Battery White website.]  Retrieved 2011-09-15.</ref>

<ref name=mingoe-report>Report of Commander J. Blakeley Creighton, captain of USS ''Mingoe''; February 24, 1865.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, p. 276. Retrieved 2011-09-17.</ref>

<ref name=mingoe2>The ''Mingoe'' is described as a "side-wheel steam gunboat" at [http://www.history.navy.mil/danfs/m11/mingoe.htm ''Mingoe''], entry in [https://web.archive.org/web/20000819035053/http://history.navy.mil:80/danfs/index.html Dictionary of American Naval Fighting Ships], [http://www.history.navy.mil/index.html Naval History and Heritage Command], retrieved 2014-04-16.</ref>

<ref name=moon>The ''Harvest Moon'' is described as a "SwStr" (side-wheel steamer) at [http://www.history.navy.mil/danfs/h3/harvest_moon.htm ''Harvest Moon''], entry in [https://web.archive.org/web/20000819035053/http://history.navy.mil:80/danfs/index.html Dictionary of American Naval Fighting Ships], [http://www.history.navy.mil/index.html Naval History and Heritage Command], retrieved 2014-04-16.</ref>

<ref name=natguard>[http://www.hmdb.org/marker.asp?marker=31642 "1st Battalion 178th Field Artillery".]  [http://www.hmdb.org/ The Historical Marker Database.]  Retrieved 2011-09-17.</ref>

<ref name=nipnee>The ''Pawnee'' is listed as "ScSlp" (screw sloop-of-war) at [http://www.history.navy.mil/danfs/p3/pawnee-i.htm ''Pawnee'']; the ''Nipsic'' is described as a "gunboat" at [http://www.history.navy.mil/danfs/n5/nipsic.htm ''Nipsic''.]  Both entries in [https://web.archive.org/web/20000819035053/http://history.navy.mil:80/danfs/index.html Dictionary of American Naval Fighting Ships], at [http://www.history.navy.mil/index.html Naval History and Heritage Command]; both retrieved 2014-04-16.</ref>

<ref name=nomform>McNulty, Kappy, and Donald R. Sutherland.  [http://www.nationalregister.sc.gov/georgetown/S10817722010/S10817722010.pdf "National Register of Historic Places Inventory--Nomination Form: Battery White."]  [http://scdah.sc.gov/ South Carolina Department of Archives and History.]  Retrieved 2011-09-15.</ref>

<ref name=open2008>[http://batterywhite.org/2008/09/22/battery-white-now-open-to-the-public-again/ "Battery White is now open to the public again".]  [http://batterywhite.org/ Battery White website.]  Retrieved 2011-09-17.</ref>

<ref name=p-b-1>Cutler, Harry Gardner.  ''[https://books.google.com/books?id=ajcVAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false History of South Carolina,]'' vol. 2, pp. 727–30.  Retrieved 2011-09-16.</ref>

<ref name=p-b-2>[http://www.csawardept.com/history/armies/index.html#Dept-SC&GA "Department of South Carolina and Georgia".] {{webarchive |url=https://web.archive.org/web/20120206020006/http://www.csawardept.com/history/armies/index.html#Dept-SC&GA |date=February 6, 2012 }}  [http://www.csawardept.com/history/armies/index.html Organization of the Confederate Armies.]  Retrieved 2011-09-16.  [https://web.archive.org/web/20120206020006/http://www.csawardept.com/history/armies/index.html#Dept-SC&GA Archived] at Wayback Machine, 2012-02-06.</ref>

<ref name=pemberton>[http://www.civilwarhome.com/pembertonbio.htm "John Clifford Pemberton (1814-1881)".]  [http://www.civilwarhome.com/index.html Home of the American Civil War.]  Retrieved 2011-09-15.</ref>

<ref name=pemberton2>Ballard, Michael B.  ''Pemberton: The General who Lost Vicksburg''.  University Press of Mississippi, 1991.  pp. 98–99.</ref>

<ref name=pemberton-chesnut>Pemberton John C., [http://ehistory.osu.edu/osu/sources/recordView.cfm?Content=020/0589 letter] to James Chesnut, Jr., July 26, 1862; retrieved 2011-09-16.</ref>

<ref name=potomska>Report of Lieutenant R. P. Swann, commander of the USS ''Potomska''; October 5, 1864.  In ''[https://books.google.com/books?id=ybg8AAAAIAAJ&printsec=frontcover#v=onepage&q&f=false Official Records of the Union and Confederate Navies in the War of the Rebellion,]'' Government Printing Office, 1903, pp. 7–8. Retrieved 2011-09-16.</ref>

<ref name=potomska2>The ''Potomska'' is listed as a "ScGbt" (screw gunboat) at [http://www.history.navy.mil/danfs/p10/potomska.htm "''Potomska''"], entry in [https://web.archive.org/web/20000819035053/http://history.navy.mil:80/danfs/index.html Dictionary of American Naval Fighting Ships], [http://www.history.navy.mil/index.html Naval History and Heritage Command], retrieved 2014-04-16.</ref>

<ref name=rouquie>"Sinking of the USS Harvest Moon", historical marker text reproduced at [http://rootsweb.ancestry.com/~scoconee/markers/markers5.html "SC Historical Roadside Markers - Florence to Hampton Counties".]  Retrieved 2014-03-20.</ref>

<ref name=salvage>[http://www.riverboatdaves.com/aboutboats/log.html "Logbook of the U.S.S. Harvest Moon".]  [http://www.riverboatdaves.com/aboutboats/hmoon.html U.S.S. Harvest Moon.]  Retrieved 2011-09-17.</ref>

<ref name=secession>[http://scdah.sc.gov/aboutus/schistory.htm "A Brief History of South Carolina".] {{webarchive |url=https://web.archive.org/web/20111130150116/http://scdah.sc.gov/aboutus/schistory.htm |date=November 30, 2011 }}  [http://scdah.sc.gov/ South Carolina Department of Archives and History.]  Retrieved 2011-09-15.  [https://web.archive.org/web/20111130150116/http://scdah.sc.gov/aboutus/schistory.htm Archived] at Wayback Machine, 2011-11-30.</ref>

<ref name=sherman>Power, J. Tracy.  [http://sc150civilwar.palmettohistory.org/edu/cw-sc.htm "Civil War in South Carolina 1861-65".]  [http://sc150civilwar.palmettohistory.org/index.htm South Carolina's Civil War Sesquicentennial.]  Retrieved 2011-09-17.</ref>

<ref name=simmons-11>Simmons (2009), p. 11.</ref>

<ref name=simmons-13-14>Simmons (2009), pp. 13–14.</ref>

<ref name=simmons-18>Simmons (2009), p. 18.</ref>

<ref name=simmons-19-20>Simmons (2009), pp. 19–20.</ref>

<ref name=simmons-19-25>Simmons (2009), pp. 19–25.</ref>

<ref name=simmons-46-47>Simmons (2009), pp. 46–47.</ref>

<ref name=simmons-54-60>Simmons (2009), pp. 54–60.</ref>

<ref name=simmons-56>Simmons (2009), p. 56.</ref>

<ref name=simmons-60>Simmons (2009), p. 60.</ref>

<ref name=simmons-103-104>Simmons (2009), pp. 103–04.</ref>

<ref name=simmons-105-106>Simmons (2009), pp. 105–06.</ref>

<ref name=simmons-109-110>Simmons (2009), pp. 109–110.</ref>

<ref name=simmons-116>Simmons (2009), p. 116.</ref>

<ref name=simmons-146-148>Simmons (2009), pp. 146–48.</ref>

<ref name=simmons-148>Simmons (2009), p. 148.</ref>

<ref name=simmons-150-151>Simmons (2009), pp. 150–51.</ref>

<ref name=stack>Holtcamp, Amy.  [http://www.discoversouthcarolina.com/Insider/Arts_and_Culture_2011/Blog/5311 "The Harvest Moon: Georgetown’s sunken treasure".]  [http://www.discoversouthcarolina.com/ South Carolina: Made For Vacation.]  Retrieved 2011-09-17.</ref>

<ref name=udc>[http://batterywhite.org/photos/battery-white-memorial-stone/ "Memorial stone at Battery White".]  [http://batterywhite.org/ Battery White website.]  Retrieved 2011-09-17.</ref>

</references>

==References==
Simmons, Rick (2009).  ''Defending South Carolina's Coast: The Civil War from Georgetown to Little River''.  Charleston, South Carolina: The History Press.

==External links==
*[http://batterywhite.org/ Battery White website]: includes visitor information
*[http://www.nationalregister.sc.gov/georgetown/S10817722010/index.htm Battery White] at South Carolina Department of Archives and History: includes a number of photos

{{National Register of Historic Places in South Carolina}}

[[Category:National Register of Historic Places in Georgetown County, South Carolina]]
[[Category:Military facilities on the National Register of Historic Places in South Carolina]]
[[Category:Infrastructure completed in 1862]]
[[Category:Forts in South Carolina]]
[[Category:American Civil War forts]]
[[Category:South Carolina in the American Civil War]]
[[Category:Buildings and structures in Georgetown County, South Carolina]]
[[Category:Tourist attractions in Georgetown County, South Carolina]]
[[Category:Batteries|White]]
[[Category:1862 establishments in South Carolina]]
[[Category:American Civil War on the National Register of Historic Places]]