<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Viastra
 | image=Vickers Viastra.jpg
 | caption=Viastra I
}}{{Infobox Aircraft Type
 | type=Passenger transport
 | national origin=United Kingdom
 | manufacturer=[[Vickers Ltd]]
 | designer=
 | first flight=1 October 1930
 | introduced=
 | retired=1937
 | status=
 | primary user=West Australian Airways
 | number built=6
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Vickers Viastra''' was an all-metal 12-seat passenger high-wing [[monoplane]], with variants powered by one, two and three engines. Two twin-engined Viastras operated commercially in Australia from 1931-6; another served as a Royal transport.

==Development==
The Vickers-Wibault construction method was based on the patents of [[Michel Wibault]], who began working with Vickers in 1922.<ref>{{Harvnb|Andrews|Morgan|1988|pages=207–9}}</ref> It was a way of producing an all-metal aircraft with an airframe built up from simple, non-machined metal shapes, covered with very thin e.g. 0.016 in (0.4&nbsp;mm) corrugated light alloy sheets. Skin panels were riveted to each other and to the underlying structure; the skin on the wings was not stressed. Vickers' first experience of the method was with the licence built [[Vickers Wibault|Wibault Scout]]. The first Vickers design using this construction was the [[Vickers Vireo|Vireo]] and the third, the Viastra, used a blend of Wibault and Vickers construction techniques.<ref name="Andrews"/>

The Viastra was envisaged as a one or three-engined commercial aircraft capable of carrying ten passengers and designed with an eye to operations in countries with poor surface transport facilities. Vickers built several variants, largely similar apart from the engines: ultimately, there were Viastras with one, two and three engines. They were high-wing monoplanes, the wings having constant chord, square tips and a thick Raf 24 section. The wings, like the rest of the aircraft were covered with the corrugated skin and carried balanced ailerons and outboard slots. There was a biplane tail unit with a pair of narrow chord planes, the lower attached to the fuselage underside and the upper mounted clear above. The outboard vertical tail surfaces were again very narrow and served as the main rudders; the single-engined and tri-motor variants also had a small triangular central fin, carrying an auxiliary rudder and supporting the upper horizontal plane. With one exception, the twin-engined Viastras had the central fin and rudder replaced by tubular bracing.<ref name="Andrews">{{Harvnb|Andrews|Morgan|1988|pages=215–22, 515}}</ref>

The fuselage had a square cross-section, parallel in the passenger cabin area and narrowing towards the tail. The passenger compartment had six rectangular windows on each side and the crew had an enclosed cabin forward of the wings. Each wing was braced to the lower fuselage with a parallel pair of struts; the forward of these was used to carry a main undercarriage leg with the help of further robust strutting. The two and three-engined Viastras had their outer engines mounted just below the wings. The outer engines were enclosed by a narrow chord [[Townend ring]], but the central engines of the tri-motored and single-engined Viastras were uncowled.<ref name="Andrews"/>

==Operational history==
The first Viastra, registered as G-AAUB and powered by three 270&nbsp;hp (200&nbsp;kW) [[Armstrong Siddeley Cheetah|Armstrong Siddeley Lynx Major]] 7-cylinder radial engines first flew on 1 October 1930. It was followed by a pair of Viastra IIs that flew on the [[Perth, Western Australia|Perth]]-[[Adelaide]] route with [[West Australian Airways]] from March 1931. One was lost in a landing accident in 1933, but the other continued in service until 1936. They were both configured as twelve seaters and initially powered by a pair of 525&nbsp;hp (390&nbsp;kW)  [[Bristol Jupiter]] XIF radials, although on occasions they flew with one or two Jupiter VIs because the higher rated XIFs proved troublesome. The Australian operation showed that the twin-engined Viastra II was a little underpowered in that it could not hold altitude on only one engine. Vickers confirmed this by replacing the Lynx engines of G-AAUB with Jupiter VIFMs. In this guise it was known as the Viastra VIII. The first of the two West Australian Airways aircraft was briefly converted into a Viastra IX,<ref>{{Harvnb|Andrews|Morgan|1988|pages=515}}</ref> which had its engines lowered by 15 in (380&nbsp;mm) before reconversion to a Viastra&nbsp;II. West Australian Airways also ordered a single-engined Viastra, with Jupiter XIF power but the order was cancelled, probably because of the unreliability of that engine, and it remained in the UK.<ref name="Andrews"/>

In between its tri-motor Lynx and Jupiter lives, G-AAUB flew for a time as a twin [[Armstrong Siddeley Jaguar]] VIc (470&nbsp;hp (350&nbsp;kW) 14-cylinder radial engines) powered Viastra III, otherwise similar to the Viastra II. The Viastra X G-ACCC was a one-off twin powered by [[Bristol Pegasus]] IIL3s 9-cylinder radials, built as a "Royal barge" for the [[Prince of Wales]] and lavishly kitted out. This aircraft had a spatted undercarriage, an under-fuselage pannier and the central fin/rudder otherwise reserved for the odd engined Viastras. First flown in April 1933, it was little used as a barge, though it joined the 1934 Hendon RAF display in royal livery. After that and until 1937 it was used to test airborne radios, carrying the wireless operators in unusual luxury.<ref name="Andrews"/>

One more Viastra was built, using a new and lighter weight wing designed by [[Barnes Wallis|B. E. Wallis]]. It was taken to the [[Royal Aircraft Establishment]] at Farnborough for structural tests but there is no record that it flew. It did not receive a Viastra variant number.<ref name="Andrews"/>

==Variants==
;Viastra I (Type 160)
:Prototype ten-passenger variant with three Armstrong Siddeley Lynx V engines, one built registered G-AUUB later modified as a two-engined Vastra III and later back to three-engines as the Viastra VIII.
;Viastra II (Type 198)
:12-seater passenger variant for West Australian Airways with two Jupiter XIF engines, two built
;Viastra III (Type 199)
:Viastra I converted with two Jaguar VIc engines, later modified back to three-engines as the Viasta VIII.
;Viastra VI (Type 203)
:Single-engined freighter version powered by a Jupiter XIF: one built but never delivered.
;Viastra VIII (Type 220)
:Viastra III converted with three Jupiter VIFM
;Viastra IX (Type 242)
:brief conversion of a Viastra II, with lowered engines
;Viastra X (Type 259)
:Seven-seat VIP passenger version for the Prince of Wales with two Pegasus radial engines, one built registered G-ACCC.
;Wallis-Viastra (Type 256)
:new wing, possibly never flown: one built.

==Specifications (Viastra II) ==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aerospecs
|ref={{Harvnb|Andrews|Morgan|1988|pages=222}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=2
|capacity=12 passengers
|length m=13.89
|length ft=45
|length in=6
|span m=21.33
|span ft=70
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.12
|height ft=13
|height in=6
|wing area sqm=69.2
|wing area sqft=745
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=3,538
|empty weight lb=7,880
|gross weight kg=5,602
|gross weight lb=12,350
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=2
|eng1 type=[[Bristol Jupiter]] XIF 9-cylinder radial
|eng1 kw=390<!-- prop engines -->
|eng1 hp=525<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=193
|max speed mph=120
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=860
|range miles=535
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4.93
|climb rate ftmin=(initial) 970
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==

*{{cite book |title= Vickers Aircraft since 1908 |last= Andrews |first= CF |last2=Morgan|first2=E.B.  |edition= 2nd |year= 1988|publisher= Putnam|location= London|isbn= 0-85177-815-1|ref=harv}}
{{refbegin}}
{{refend}}

==External links==
{{commons category|Vickers Viastra}}
*[http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%201115.html Vickers "Viastra I"], Flight, September 26, 1930

{{Vickers aircraft}}

[[Category:Vickers aircraft|Viastra]]
[[Category:British civil utility aircraft 1930–1939]]
[[Category:Trimotors]]
[[Category:High-wing aircraft]]