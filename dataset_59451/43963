{{Use American English|date=August 2016}}
[[File:Cyclone combustor.jpg|thumb|upright=1.14|Flame stabilized by cyclone.]]

A '''combustor''' is a component or area of a [[gas turbine]], [[ramjet]], or [[scramjet]] [[engine]] where [[combustion]] takes place. It is also known as a '''burner''', '''combustion chamber''' or '''flame holder'''. In a gas turbine engine, the ''combustor'' or [[combustion chamber]] is fed high pressure air by the compression system. The combustor then heats this air at constant pressure. After heating, air passes from the combustor through the nozzle guide vanes to the turbine. In the case of a ramjet or scramjet engines, the air is directly fed to the nozzle.

A combustor must contain and maintain stable combustion despite very high air flow rates. To do so combustors are carefully designed to first mix and ignite the air and fuel, and then mix in more air to complete the combustion process. Early gas turbine engines used a single chamber known as a can type combustor. Today three main configurations exist: can, annular and cannular (also referred to as can-annular tubo-annular). Afterburners are often considered another type of combustor.

Combustors play a crucial role in determining many of an engine's operating characteristics, such as fuel efficiency, levels of emissions and transient response (the response to changing conditions such a fuel flow and air speed).

==Fundamentals==
[[File:Combustor on Rolls-Royce Nene turbojet (1).jpg|thumb|Combustor on a [[Rolls-Royce Nene]] [[turbojet]]]]
The objective of the combustor in a gas turbine is to add energy to the system to power the [[turbines]], and produce a high velocity gas to exhaust through the nozzle in aircraft applications. As with any engineering challenge, accomplishing this requires balancing many design considerations, such as the following: 
*Completely combust the fuel. Otherwise, the engine is wastes the unburnt fuel and creates unwanted emissions of unburnt hydrocarbons, carbon monoxide (CO) and soot.
*Low pressure loss across the combustor. The turbine which the combustor feeds needs high pressure flow to operate efficiently.
*The flame (combustion) must be held (contained) inside of the combustor. If combustion happens further back in the engine, the turbine stages can easily be overheated and damaged. Additionally, as [[turbine blade]]s continue to grow more advanced and are able to withstand higher temperatures, the combustors are being designed to burn at higher temperatures and the parts of the combustor need to be designed to withstand those higher temperatures.
*Uniform exit temperature profile. If there are hot spots in the exit flow, the turbine may be subjected to [[thermal stress]] or other types of damage. Similarly, the temperature profile within the combustor should avoid hot spots, as those can damage or destroy a combustor from the inside.
*Small physical size and weight. Space and weight is at a premium in aircraft applications, so a well designed combustor strives to be compact. Non-aircraft applications, like power generating gas turbines, are not as constrained by this factor.
*Wide range of operation. Most combustors must be able to operate with a variety of inlet pressures, temperatures, and mass flows. These factors change with both engine settings and environmental conditions (I.e., full throttle at low altitude can be very different from idle throttle at high altitude).
*Environmental emissions. There are strict regulations on aircraft emissions of pollutants like carbon dioxide and nitrogen oxides, so combustors need to be designed to minimize those emissions. (See ''Emissions'' section below)
Sources:<ref>Flack, p. 440.</ref><ref>Mattingly, Heiser, and Pratt, p. 325.</ref>

===History===
Advancements in combustor technology focused on several distinct areas; emissions, operating range, and durability. Early jet engines produced large amounts of smoke, so early combustor advances, in the 1950s, were aimed at reducing the smoke produced by the engine. Once smoke was essentially eliminated, efforts turned in the 1970s to reducing other emissions, like unburned [[hydrocarbons]] and [[carbon monoxide]] (for more details, see the ''Emissions'' section below). The 1970s also saw improvement in combustor durability, as new manufacturing methods improved liner (see ''Components'' below) lifetime by nearly 100 times that of early liners. In the 1980s combustors began to improve their efficiency across the whole operating range; combustors tended to be highly efficient (99%+) at full power, but that efficiency dropped off at lower settings. Development over that decade improved efficiencies at lower levels. The 1990s and 2000s saw a renewed focus on reducing emissions, particularly [[nitrogen oxides]]. Combustor technology is still being actively researched and advanced, and much modern research focuses on improving the same aspects.<ref>Koff, Bernard L. (2004). Gas Turbine Technology Evolution: A Designer’s Perspective. ''Journal of Propulsion and Power''. Vol. 20, No. 4, July–August 2004</ref>

===Components===
[[File:Combustor diagram componentsPNG.png|frameless|border|upright=2|right]]
;Case
The case is the outer shell of the combustor, and is a fairly simple structure. The casing generally requires little maintenance.<ref>Henderson and Blazowski, pp. 119–20.</ref> The case is protected from thermal loads by the air flowing in it, so thermal performance is of limited concern. However, the casing serves as a pressure vessel that must withstand the difference between the high pressures inside the combustor and the lower pressure outside. That mechanical (rather than thermal) load is a driving design factor in the case.<ref>Mattingly, Heiser, and Pratt, p. 378.</ref>
;Diffuser
The purpose of the diffuser is to slow the high speed, highly compressed, air from the [[Gas compressor|compressor]] to a velocity optimal for the combustor. Reducing the velocity results in an unavoidable loss in total pressure, so one of the design challenges is to limit the loss of pressure as much as possible.<ref>Mattingly, Heiser, and Pratt, p. 375.</ref> Furthermore, the diffuser must be designed to limit the flow distortion as much as possible by avoiding flow effects like [[boundary layer separation]]. Like most other gas turbine engine components, the diffuser is designed to be as short and light as possible.<ref>Henderson and Blazowski, p. 121.</ref> <!--possible add text about curved wall vs. dump diffusers?--> 
;Liner
The liner contains the combustion process and introduces the various airflows (intermediate, dilution, and cooling, see ''Air flow paths'' below) into the combustion zone. The liner must be designed and built to withstand extended high temperature cycles. For that reason liners tend to be made from [[superalloy]]s like [[Hastelloy|Hastelloy X]]. Furthermore, even though high performance alloys are used, the liners must be cooled with air flow.<ref name=Mattingly760>Mattingly, p. 760.</ref> Some combustors also make use of [[thermal barrier coating]]s. However, air cooling is still required. In general, there are two main types of liner cooling; film cooling and transpiration cooling. Film cooling works by injecting (by one of several methods) cool air from outside of the liner to just inside of the liner. This creates a thin film of cool air that protects the liner, reducing the temperature at the liner from around 1800 [[kelvin]]s (K) to around 830 K, for example. The other type of liner cooling, transpiration cooling, is a more modern approach that uses a [[Porous medium|porous]] material for the liner. The porous liner allows a small amount of cooling air to pass through it, providing cooling benefits similar to film cooling. The two primary differences are in the resulting temperature profile of the liner and the amount of cooling air required. Transpiration cooling results in a much more even temperature profile, as the cooling air is uniformly introduced through pores. Film cooling air is generally introduced through slats or louvers, resulting in an uneven profile where it is cooler at the slat and warmer between the slats. More importantly, transpiration cooling uses much less cooling air (on the order of 10% of total airflow, rather than 20-50% for film cooling). Using less air for cooling allows more to be used for combustion, which is more and more important for high performance, high thrust engines.<ref>Mattingly, Heiser, and Pratt, pp. 372–4.</ref><ref>Henderson and Blazowski, pp. 124–7.</ref>
;Snout
The snout is an extension of the dome (see below) that acts as an air splitter, separating the primary air from the secondary air flows (intermediate, dilution, and cooling air; see ''Air flow paths'' section below).<ref name=HandB124>Henderson and Blazowski, p. 124.</ref>
;Dome / swirler
The dome and swirler are the part of the combustor that the primary air (see ''Air flow paths'' below) flows through as it enters the combustion zone. Their role is to generate [[turbulence]] in the flow to rapidly mix the air with fuel.<ref name=Mattingly760/> Early combustors tended to use ''bluff body domes'' (rather than swirlers), which used a simple plate to create [[wake turbulence]] to mix the fuel and air. Most modern designs, however, are ''swirl stabilized'' (use swirlers). The swirler establishes a local low pressure zone that forces some of the combustion products to recirculate, creating the high turbulence.<ref name=HandB124/> However, the higher the turbulence, the higher the pressure loss will be for the combustor, so the dome and swirler must be carefully designed so as not to generate more turbulence than is needed to sufficiently mix the fuel and air.<ref>Flack, p. 441.</ref> 
;Fuel injector
[[File:Cannular combustor on a Pratt & Whitney JT9D turbofan.jpg|thumb|Fuel injectors of a cannular combustor on a [[Pratt & Whitney JT9D]] turbofan]]
The fuel injector is responsible for introducing fuel to the combustion zone and, along with the swirler (above), is responsible for mixing the fuel and air. There are four primary types of fuel injectors; pressure-atomizing, air blast, vaporizing, and premix/prevaporizing injectors.<ref name=Mattingly760/> Pressure atomizing fuel injectors rely on high fuel pressures (as much as {{convert|500|psi|disp=flip}}) to atomize<ref group="nb" name="note1">While '''atomize''' has several definitions, in this context it means to form a fine spray. It is not meant to imply that the fuel is being broken down to its atomic components.</ref> the fuel. This type of fuel injector has the advantage of being very simple, but it has several disadvantages. The fuel system must be robust enough to withstand such high pressures, and the fuel tends to be [[heterogeneous]]ly atomized, resulting in incomplete or uneven combustion which has more pollutants and smoke.<ref>Henderson and Blazowski, p. 127.</ref><ref name=MHB379>Mattingly, Heiser, and Pratt, p. 379.</ref>

The second type of fuel injector is the air blast injector. This injector "blasts" a sheet of fuel with a stream of air, atomizing the fuel into homogeneous droplets. This type of fuel injector led to the first smokeless combustors. The air used is just same amount of the primary air (see ''Air flow paths'' below) that is diverted through the injector, rather than the swirler. This type of injector also requires lower fuel pressures than the pressure atomizing type.<ref  name=MHB379/>

The vaporizing fuel injector, the third type, is similar to the air blast injector in that primary air is mixed with the fuel as it is injected into the combustion zone. However, the fuel-air mixture travels through a tube within the combustion zone. Heat from the combustion zone is transferred to the fuel-air mixture, vaporizing some of the fuel (mixing it better) before it is combusted. This method allows the fuel to be combusted with less [[thermal radiation]], which helps protect the liner. However, the vaporizer tube may have serious durability problems with low fuel flow within it (the fuel inside of the tube protects the tube from the combustion heat).<ref>Henderson and Blazowski, p. 128.</ref>

The premixing/prevaporizing injectors work by mixing or vaporizing the fuel before it reaches the combustion zone. This method allows the fuel to be very uniformly mixed with the air, reducing emissions from the engine. One disadvantage of this method is that fuel may auto-ignite or otherwise combust before the fuel-air mixture reaches the combustion zone. If this happens the combustor can be seriously damaged.<ref>Henderson and Blazowski, p. 129.</ref>

;Igniter
Most igniters in gas turbine applications are electrical spark igniters, similar to [[Spark plug|automotive spark plugs]]. The igniter needs to be in the combustion zone where the fuel and air are already mixed, but it needs to be far enough upstream so that it is not damaged by the combustion itself. Once the combustion is initially started by the igniter, it is self-sustaining and the igniter is no longer used.<ref>Mattingly, Heiser, and Pratt, p. 368.</ref> In can-annular and annular combustors (see ''Types of combustors'' below), the flame can propagate from one combustion zone to another, so igniters are not needed at each one. In some systems ignition-assist techniques are used. One such method is oxygen injection, where oxygen is fed to the ignition area, helping the fuel easily combust. This is particularly useful in some aircraft applications where the engine may have to restart at high altitude.<ref>Henderson and Blazowski, pp. 129–30.</ref>

===Air flow paths===
[[File:Combustor diagram airflow.png|frameless|border|upright=2|right]]
;Primary air
This is the main combustion air. It is highly compressed air from the high-pressure compressor (often decelerated via the diffuser) that is fed through the main channels in the dome of the combustor and the first set of liner holes. This air is mixed with fuel, and then combusted.<ref>Henderson and Blazowski, p. 110.</ref>
;Intermediate air
Intermediate air is the air injected into the combustion zone through the second set of liner holes (primary air goes through the first set). This air completes the reaction processes, cooling the air down and diluting the high concentrations of [[carbon monoxide]] (CO) and [[hydrogen]] (H<sub>2</sub>).<ref name=HandB111>Henderson and Blazowski, p. 111.</ref>
;Dilution air
Dilution air is airflow injected through holes in the liner at the end of the combustion chamber to help cool the air to before it reaches the turbine stages. The air is carefully used to produce the uniform temperature profile desired in the combustor. However, as turbine blade technology improves, allowing them to withstand higher temperatures, dilution air is used less, allowing the use of more combustion air.<ref name=HandB111/>
;Cooling air
Cooling air is airflow that is injected through small holes in the liner to generate a layer (film) of cool air to protect the liner from the combustion temperatures. The implementation of cooling air has to be carefully designed so it does not directly interact with the combustion air and process. In some cases, as much as 50% of the inlet air is used as cooling air. There are several different methods of injecting this cooling air, and the method can influence the temperature profile that the liner is exposed to (see ''Liner'', above).<ref>Henderson and Blazowski, pp. 111, 125–7.</ref>

==Types==
[[File:CanCombustor.svg|thumb|right|Arrangement of can-type combustors for a gas turbine engine, looking axis on, through the exhaust. The blue indicates cooling flow path, the orange indicates the combustion product flow path.]]

===Can===
Can combustors are self-contained cylindrical combustion chambers. Each "can" has its own fuel injector, igniter, liner, and casing.<ref name="nasa">Benson, Tom. [http://www.grc.nasa.gov/WWW/K-12/airplane/burner.html Combustor-Burner]. NASA Glenn Research Center. Last Updated 11 Jul 2008. Accessed 6 Jan 2010.</ref> The primary air from the compressor is guided into each individual can, where it is decelerated, mixed with fuel, and then ignited. The secondary air also comes from the compressor, where it is fed outside of the liner (inside of which is where the combustion is taking place). The secondary air is then fed, usually through slits in the liner, into the combustion zone to cool the liner via thin film cooling.<ref>Flack, p. 442.</ref>

In most applications, multiple cans are arranged around the central axis of the engine, and their shared exhaust is fed to the turbine(s). Can type combustors were most widely used in early gas turbine engines, owing to their ease of design and testing (one can test a single can, rather than have to test the whole system). Can type combustors are easy to maintain, as only a single can needs to be removed, rather than the whole combustion section. Most modern gas turbine engines (particularly for aircraft applications) do not use can combustors, as they often weigh more than alternatives. Additionally, the pressure drop across the can is generally higher than other combustors (on the order of 7%). Most modern engines that use can combustors are [[turboshafts]] featuring [[centrifugal compressor]]s.<ref>Flack, pp. 442–3.</ref><ref>Henderson and Blazowski, p. 106.</ref>

===Cannular===
[[File:CanAnnularCombustor.svg|thumb|right|Cannular combustor for a gas turbine engine, viewing axis on, through the exhaust]]

The next type of combustor is the ''cannular'' combustor; the term is a [[portmanteau]] of "can annular". Like the can type combustor, can annular combustors have discrete combustion zones contained in separate liners with their own fuel injectors. Unlike the can combustor, all the combustion zones share a common ring (annulus) casing. Each combustion zone no longer has to serve as a pressure vessel.<ref>Mattingly, Heiser, and Pratt, pp. 377–8.</ref> The combustion zones can also "communicate" with each other via liner holes or connecting tubes that allow some air to flow circumferentially. The exit flow from the cannular combustor generally has a more uniform temperature profile, which is better for the turbine section. It also eliminates the need for each chamber to have its own igniter. Once the fire is lit in one or two cans, it can easily spread to and ignite the others. This type of combustor is  also lighter than the can type, and has a lower pressure drop (on the order of 6%). However, a cannular combustor can be more difficult to maintain than a can combustor.<ref>Flack, pp. 442–4.</ref> An example of a gas turbine engine utilizing a cannular combustor is the [[General Electric J79]] The Pratt & Whitney [[JT8D]] and the [[Rolls-Royce RB.183 Tay|Rolls-Royce Tay]] [[turbofans]] use this type of combustor as well.<ref>Henderson and Blazowski, pp. 106–7.</ref>

===Annular===
[[File:AnnularCombustor.svg|thumb|right|Annular combustor for a gas turbine engine, viewed axis on looking through the exhaust. The small orange circles are the fuel injection nozzles.]]

The final, and most commonly used type of combustor is the fully annular combustor.  Annular combustors do away with the separate combustion zones and simply have a continuous liner and casing in a ring (the annulus). There are many advantages to annular combustors, including more uniform combustion, shorter size (therefore lighter), and less surface area.<ref>Henderson and Blazowski, p. 108.</ref><ref>Mattingly, p. 757.</ref> Additionally, annular combustors tend to have very uniform exit temperatures. They also have the lowest pressure drop of the three designs (on the order of 5%).<ref>Flack, p. 444.</ref> The annular design is also simpler, although testing generally requires a full size test rig. An engine that uses an annular combustor is the [[CFM International CFM56]]. Most modern engines use annular combustors; likewise, most combustor research and development focuses on improving this type. 
;Double annular combustor
One variation on the standard annular combustor is the ''double annular combustor'' (DAC). Like an annular combustor, the DAC is a continuous ring without separate combustion zones around the radius. The difference is that the combustor has two combustion zones around the ring; a pilot zone and a main zone. The pilot zone acts like that of a single annular combustor, and is the only zone operating at low power levels. At high power levels, the main zone is used as well, increasing air and mass flow through the combustor. GE's implementation of this type of combustor focuses on reducing NOx and CO2 emissions.<ref>[http://www.cfm56.com/press/news/cfms+advanced+double+annular+combustor+technology/198?page_index=23 CFM'S Advanced Double Annular Combustor Technology]. Press Release. 9 Jul 1998. Accessed 6 Jan 2010.</ref> [http://cobweb.ecn.purdue.edu/~propulsi/propulsion/images/jets/basics/combust.jpg A good diagram of a DAC is available from Purdue]. Extending the same principles as the double annular combustor, triple annular and "multiple annular" combustors have been proposed and even patented.<ref>Ekstedt, Edward E., et al (1994). {{US patent|5323604}} Triple annular combustor for gas turbine engine].</ref><ref>Schilling, Jan C., et al (1997). {{US patent|5630319}} Dome assembly for a multiple annular combustor].</ref>

==Emissions==
One of the driving factors in modern gas turbine design is reducing emissions, and the combustor is the primary contributor to a gas turbine's emissions. Generally speaking, there are five major types of emissions from gas turbine engines: smoke, [[carbon dioxide]] (CO<sub>2</sub>), [[carbon monoxide]] (CO), unburned [[hydrocarbons]] (UHC), and [[nitrogen oxides]] (NO<sub>x</sub>).<ref name="reg">Verkamp, F. J., Verdouw, A. J., Tomlinson, J. G. (1974). Impact of Emission Regulations on Future Gas Turbine Engine Combustors. ''Journal of Aircraft''. June 1974. Vol. 11, No. 6. pp. 340–344.</ref><ref name="reduct">Sturgess, G.J., and Zelina, J., Shouse D. T., Roquemore, W.M. (2005). [http://pdf.aiaa.org/getfile.cfm?urlX=%2C%3CWI%277D%2FQKS%2B-S%20OA%0A&urla=%25*RH.%22%40%2C%20%0A&urlb=!*%20%20%20%0A&urlc=!*0%20%20%0A&urld=%27*%22%5C.%23%40.FWP%20%20%0A&urle=%27*%22D.%23%202GU%40%20%20%0A Emissions Reduction Technologies for Military Gas Turbine Engines]. ''Journal of Propulsion and Power''. March–April 2005. Vol. 21, No. 2. pp. 193–217.</ref>

Smoke is primarily mitigated by more evenly mixing the fuel with air. As discussed in the fuel injector section above, modern fuel injectors (such as airblast fuel injectors) evenly atomize the fuel and eliminate local pockets of high fuel concentration. Most modern engines use these types of fuel injectors and are essentially smokeless.<ref name="reg"/>

Carbon dioxide is a [[Product (chemistry)|product]] of the [[combustion]] process, and it is primarily mitigated by reducing fuel usage. On average, 1&nbsp;kg of jet fuel burned produces 3.2&nbsp;kg of CO<sub>2</sub>. Carbon dioxide emissions will continue to drop as manufacturers make gas turbine engines more efficient.<ref name="reduct"/>

Unburned hydrocarbon (UHC) and carbon monoxide (CO) emissions are highly related. UHCs are essentially fuel that was not completely combusted, and UHCs are mostly produced at low power levels (where the engine is not burning all the fuel).<ref name="reduct"/> Much of the UHC content reacts and forms CO within the combustor, which is why the two types of emissions are heavily related. As a result of this close relation, a combustor that is well optimized for CO emissions is inherently well optimized for UHC emissions, so most design work focuses on CO emissions.<ref name="reg"/>

Carbon monoxide is an intermediate product of combustion, and it is eliminated by [[oxidation]]. CO and [[Hydroxyl radical|OH]] react to form CO<sub>2</sub> and [[Hydrogen|H]]. This process, which consumes the CO, requires a relatively long time ("relatively" is used because the combustion process happens incredibly quickly), high temperatures, and high pressures. This fact means that a low CO combustor has a long ''residence time'' (essentially the amount of time the gases are in the combustion chamber).<ref name="reg"/>

Like CO, Nitrogen oxides (NO<sub>x</sub>) are produced in the combustion zone. However, unlike CO, it is most produced during the conditions that CO is most consumed (high temperature, high pressure, long residence time). This means that, in general, reducing CO emissions results in an increase in NO<sub>x</sub> and vice versa. This fact means that most successful emission reductions require the combination of several methods.<ref name="reg"/> 
<!--Is this enough, or should the article dive into the nitty-gritty of emissions reduction?-->

==Afterburners==
{{Main|Afterburner}}
An afterburner (or reheat) is an additional component added to some [[jet engine]]s, primarily those on military [[supersonic]] aircraft. Its purpose is to provide a temporary increase in [[thrust]], both for supersonic flight and for takeoff (as the high [[wing loading]] typical of supersonic aircraft designs means that take-off speed is very high).  On [[military aircraft]] the extra thrust is also useful for [[aerial combat|combat]] situations.  This is achieved by injecting additional [[Jet fuel|fuel]] into the jet pipe downstream of (i.e. ''after'') the [[turbine]] and combusting it.  The advantage of afterburning is significantly increased thrust; the disadvantage is its very high fuel consumption and inefficiency, though this is often regarded as acceptable for the short periods during which it is usually used.

Jet engines are referred to as operating ''wet'' when afterburning is being used and ''dry'' when the engine is used without afterburning. An engine producing maximum thrust wet is at ''maximum power'' or ''max reheat'' (this is the maximum power the engine can produce); an engine producing maximum thrust dry is at ''military power'' or ''max dry''.

As with the main combustor in a gas turbine, the afterburner has both a case and a liner, serving the same purpose as their main combustor counterparts. One major difference between a main combustor and an afterburner is that the temperature rise is not constrained by a turbine section, therefore afterburners tend to have a much higher temperature rise than main combustors.<ref>Mattingly, pp. 770–1.</ref> Another difference is that afterburners are not designed to mix fuel as well as primary combustors, so not all the fuel is burned within the afterburner section.<ref>Flack, pp. 445–6.</ref> Afterburners also often require the use of [[Flame holder|flameholders]] to keep the velocity of the air in the afterburner from blowing the flame out. These are often bluff bodies or "vee-gutters" directly behind the fuel injectors that create localized low speed flow in the same manner the dome does in the main combustor.<ref>Mattingly, p. 747.</ref>

==Ramjets==
{{Main|Ramjet}}
[[Ramjet]] engines differ in many ways from traditional gas turbine engines, but most of the same principles hold. One major difference is the lack of rotating machinery (a turbine) after the combustor. The combustor exhaust is directly fed to a nozzle. This allows ramjet combustors to burn at a higher temperature. Another difference is that many ramjet combustors do not use liners like gas turbine combustors do. Furthermore, some ramjet combustors are ''dump combustors'' rather than a more conventional type. Dump combustors inject fuel and rely on recirculation generated by a large change in area in the combustor (rather than swirlers in many gas turbine combustors).<ref name="fh">Stull, F. D. and Craig, R. R. (1975). Investigation of Dump Combustors with Flameholders. ''13th AIAA Aerospace Sciences Meeting''. Pasadena, CA. 20–22 January 1975. AIAA 75-165</ref> That said, many ramjet combustors are also similar to traditional gas turbine combustors, such as the combustor in the ramjet used by the [[RIM-8 Talos]] missile, which used a can-type combustor.<ref name="hist">Waltrup, P.J. and White M.E., Zarlingo F., Gravlin E. S. (2002). [http://pdf.aiaa.org/downloads/1996/1996_3152.pdf History of U.S. Navy Ramjet, Scramjet, and Mixed-Cycle Propulsion Development]. ''Journal of Propulsion and Power''. Vol. 18, No. 1, January–February 2002.</ref>

==Scramjets==
[[File:ScramjetDiagram.gif|thumb|right|upright=2.0|Diagram illustrating a scramjet engine. Notice the isolator section between the compression inlet and combustion chamber. (Illustration from [[The Hy-V Scramjet Flight Experiment]].)]]
{{Main|Scramjet}}

[[Scramjet]] (''[[supersonic]] [[combustion]] [[ramjet]]'') engines present a much different situation for the combustor than conventional gas turbine engines (scramjets are not gas turbines, they generally have few or no moving parts). While scramjet combustors may be physically quite different from conventional combustors, they face many of the same design challenges, like fuel mixing and flame holding. However, as its name implies, a scramjet combustor must address these challenges in a [[supersonic]] flow environment. For example, for a scramjet flying at [[Mach number|Mach]] 5, the air flow entering the combustor would nominally be Mach 2. One of the major challenges in a scramjet engine is preventing [[shock waves]] generated by combustor from traveling upstream into the inlet. If that were to happen, the engine may [[unstart]], resulting in loss of thrust, amongst other problems. To prevent this, scramjet engines tend to have an isolator section (see image) immediately ahead of the combustion zone.<ref name="hyv">Goyne, C. P., Hall, C. D., O'Brian, W. F., and Schetz, J. A. (2006). [http://pdf.aiaa.org/preview/CDReadyMHYP06_1276/PV2006_7901.pdf ''The Hy-V Scramjet Flight Experiment'']. 14th AIAA/AHI Space Planes and Hypersonic Systems and Technologies Conference. AIAA 2006-7901. Nov 2006.</ref>

==Notes==
{{Commons category|Combustors}}
<references group="nb" />
{{-}}

==References==
;Notes
{{Reflist|colwidth=35em}}

;Bibliography
*{{cite book
|last1=Flack 
|first1=Ronald D. 
|title=Fundamentals of Jet Propulsion with Applications
|series=Cambridge Aerospace Series 
|year=2005|url=https://books.google.com/books?id=MLlmJSRUY50C&pg=PA440
|publisher=Cambridge University Press
|location=New York, NY
|isbn=978-0-521-81983-1
|chapter=Chapter 9: Combustors and Afterburners
}}
*{{cite book
|last1=Henderson 
|first1=Robert E. 
|last2=Blazowski 
|first2= William S.
|editor1-first=Gordon C.
|editor1-last=Oates 
|title=Aircraft Propulsion Systems Technology and Design 
|series=AIAA Education Series 
|year=1989 
|publisher=American Institute of Aeronautics and Astronautics 
|location=Washington, DC 
|isbn=0-930403-24-X
|chapter=Chapter 2: Turbopropulsion Combustion Technology
}}
*{{cite book
|last1=Mattingly 
|first1=Jack D. 
|last2=Heiser 
|first2= William H.
|last3=Pratt
|first3= David T.
|title=Aircraft Engine Design
|edition=2nd 
|series=AIAA Education Series 
|year=2002 
|publisher=American Institute of Aeronautics and Astronautics 
|location=Reston, VA 
|isbn=1-56347-538-3
|chapter=Chapter 9: Engine Component Design: Combustion Systems
}}

*{{cite book
|last1=Mattingly 
|first1=Jack D. 
|title=Elements of Propulsion: Gas Turbines and Rockets
|series=AIAA Education Series 
|year=2006
|publisher=American Institute of Aeronautics and Astronautics 
|location=Reston, VA 
|isbn=1-56347-779-3
|chapter=Chapter 10: Inlets, Nozzles, and Combustion Systems
}}

{{Aircraft gas turbine engine components}}

[[Category:Combustion chambers]]
[[Category:Jet engine technology]]
[[Category:Jet engines]]