{{For|the film|Battleship Potemkin}}
{{featured article}}
{{use British English|date=August 2013}}

{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image = [[File:Panteleimon, 1906.jpg|300px]]
| Ship caption =''Panteleimon'' at sea, 1906
}}
{{Infobox ship career
| Ship country = [[Russian Empire]]
| Ship flag = {{shipboxflag|Russian Empire|naval}}
| Ship name =*1904: ''Kniaz Potemkin Tavricheskiy''
*1905: ''Panteleimon''
*1917: ''Potemkin-Tavricheskiy''
*1917: ''Borets za Svobodu''
| Ship namesake =*[[Grigory Potemkin]]
*[[Saint Pantaleon]]
| Ship ordered =
| Ship builder = [[Shipyard named after 61 Communards|Nikolaev Admiralty Shipyard]]
| Ship laid down =10 October 1898<ref group=Note>All dates used in this article are [[Adoption of the Gregorian calendar#Adoption in Eastern Europe|New Style]]</ref>
| Ship launched = 9 October 1900
| Ship completed = 1905
| Ship decommissioned =March 1918
| Ship in service =
| Ship out of service =19 April 1919
| Ship struck =21 November 1925
| Ship fate =[[ship breaking|Scrapped]], 1923
| Ship status =
}}
{{Infobox ship characteristics
| Hide header =
| Header caption =
|Ship type=[[Pre-dreadnought battleship]]
|Ship displacement=*{{Convert|12480|LT|t|abbr=on}} (designed)
*{{Convert|12900|LT|t|0|abbr=on}} (actual)
|Ship length={{convert|378|ft|6|in|m|1|abbr=on}}
|Ship beam={{convert|73|ft|m|1|abbr=on}}
|Ship draught={{convert|27|ft|m|1|abbr=on}}
|Ship propulsion=2 shafts, 2 [[Steam engine#Multiple expansion engines|Vertical triple-expansion steam engines]]
|Ship speed= {{Convert|16|kn|lk=in}}
|Ship range={{convert|3200|nmi|lk=in}} at {{convert|10|kn}}
|Ship power=*{{Convert|10600|ihp|kW|abbr=on|lk=in}}
*22 [[Belleville boiler]]s
|Ship complement=26 officers, 705 enlisted men
|Ship armament=*2 × twin [[Russian 12 inch 40 caliber naval gun|{{convert|12|in|mm|0|abbr=on}}/40 M1895 naval guns]]
*16 × single [[152 mm 45 caliber Pattern 1892|{{convert|6|in|mm|0|abbr=on}}]]/45 M1892 naval guns
*14 × single {{convert|75|mm|1|abbr=on}}/50 M1892 Canet guns
*6 × single [[QF 3-pounder Hotchkiss|{{convert|47|mm|1|abbr=on}}/40 Hotchkiss guns]]
*5 × {{convert|15|in|mm|0|sing=on}} [[torpedo tube]]s
|Ship armour=*[[Krupp cemented armour]]
*[[Belt armor|Waterline belt]]: {{convert|9|in|mm|0|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|2|-|3|in|mm|0|abbr=on}}
*[[Barbette]]s: {{convert|4.2|-|10|in|mm|abbr=on|0}}
*[[Gun turret]]s: {{convert|10|in|mm|0|abbr=on}}
*[[Conning tower]]: {{convert|9|in|mm|0|abbr=on}}
*[[Bulkhead (partition)|Bulkhead]]s: {{convert|7|in|mm|0|abbr=on}}
| Ship notes =
}}
|}

The '''Russian battleship ''Potemkin''''' ({{lang-ru |Князь Потёмкин Таврический, ''Kniaz Potyomkin Tavricheskiy''}}, "Prince Potemkin of [[Tauris]]") was a [[pre-dreadnought battleship]] built for the [[Imperial Russian Navy]]'s [[Black Sea Fleet]]. She became famous when the crew rebelled against the officers in June 1905 (during [[1905 Russian Revolution|that year's revolution]]), which is now viewed as a first step towards the [[Russian Revolution]] of 1917. The mutiny later formed the basis of [[Sergei Eisenstein]]'s 1925 [[silent film]] ''[[The Battleship Potemkin]]''.

After the mutineers sought asylum in [[Constanța]], Romania, and after the Russians recovered the ship, her name was changed to '''''Panteleimon'''''. She accidentally sank a Russian [[submarine]] in 1909 and was badly damaged when she [[Ship grounding|ran aground]] in 1911. During [[World War I]], ''Panteleimon'' participated in the [[Battle of Cape Sarych]] in late 1914. She covered several bombardments of the [[Bosphorus]] fortifications in early 1915, including one where she was attacked by the Turkish [[battlecruiser]] [[SMS Goeben|''Yavuz Sultan Selim'']] – ''Panteleimon'' and the other Russian pre-dreadnoughts present drove her off before she could inflict any serious damage. The ship was relegated to secondary roles after Russia's first [[dreadnought battleship]] entered service in late 1915. She was by then obsolete and was reduced to [[Reserve fleet|reserve]] in 1918 in [[Sevastopol]].

''Panteleimon'' was captured when the Germans took Sevastopol in May 1918 and was handed over to the [[Allies of World War I|Allies]] after the [[Armistice with Germany|Armistice]] in November 1918. Her engines were destroyed by the British in 1919 when they withdrew from Sevastopol to prevent the advancing [[Bolsheviks]] from using them against the [[White movement|White Russians]]. She was abandoned when the Whites evacuated the [[Crimea]] in 1920 and was finally [[ship breaking|scrapped]] by the Soviets in 1923.

==Design and construction==

===Planning===
Planning began in 1895 for a new battleship that would utilise a [[slipway]] slated to become available at the [[Shipyard named after 61 Communards|Nikolayev Admiralty Shipyard]] in 1896. The Naval Staff and the commander of the Black Sea Fleet, [[Vice Admiral]] K. P. Pilkin, agreed on a copy of the {{sclass-|Peresvet|battleship|2}} design, but they were overruled by [[General Admiral]] [[Grand Duke Alexei Alexandrovich of Russia|Grand Duke Alexei Alexandrovich]]. The General Admiral decided that the long range and less powerful {{convert|10|in|adj=on|0}} guns of the ''Peresvet'' class were inappropriate for the narrow confines of the [[Black Sea]], and ordered the design of an improved version of the battleship {{ship|Russian battleship|Tri Sviatitelia||2}} instead. The improvements included a higher [[forecastle]] to improve the ship's [[seakeeping]] qualities, [[Krupp cemented armour]] and [[Belleville boilers]]. The design process was complicated by numerous changes demanded by various departments of the Naval Technical Committee. The ship's design was finally approved on 12 June 1897, although design changes continued to be made that slowed the ship's construction.<ref>McLaughlin 2003, pp. 117–18</ref>

===Construction and sea trials===
Construction of ''Potemkin'' began on 27 December 1897 and she was [[keel laying|laid down]] at the Nikolayev Admiralty Shipyard on 10 October 1898. She was named in honour of Prince [[Grigory Potemkin]], a Russian soldier and statesman.<ref>Silverstone, p. 378</ref> The ship was launched on 9 October 1900 and transferred to Sevastopol for fitting out on 4 July 1902. She began sea trials in September 1903 and these continued, off and on, until early 1905 when her gun turrets were completed.<ref>McLaughlin 2003, pp. 116, 121</ref>

==Description==
''Potemkin'' was {{convert|371|ft|5|in|1}} [[length at the waterline|long at the waterline]] and {{convert|378|ft|6|in|1}} long [[length overall|overall]]. She had a [[beam (nautical)|beam]] of {{convert|73|ft|1}} and a maximum [[draft (hull)|draught]] of {{convert|27|ft|1}}. She displaced {{convert|12900|LT|t}}, {{convert|420|LT|t}} more than her designed displacement of {{convert|12480|LT|t}}. ''Potemkin''{{'}}s crew consisted of 26 officers and 705 enlisted men.<ref name=m72>McLaughlin 2003, p. 116</ref>

===Power===
The ship had a pair of three-cylinder [[Marine steam engine#Triple or multiple expansion|vertical triple-expansion steam engines]], each of which drove one [[propeller]], that had a total designed output of {{convert|10600|ihp|lk=in}}. Twenty-two [[Belleville boiler]]s provided steam to the engines at a pressure of {{convert|15|atm|kPa psi|0|abbr=on|lk=on}}. The eight boilers in the forward boiler room were oil-fired and the remaining 14 were coal-fired. During her [[sea trial]]s on 31 October 1903, she reached a top speed of {{convert|16.5|kn|lk=in}}. Leaking oil caused a serious fire on 2 January 1904 that caused the navy to convert her boilers to coal firing at a cost of 20,000 [[ruble]]s. She carried a maximum of {{convert|1100|LT|MT}} of coal at full load that provided a range of {{convert|3200|nmi|lk=in}} at a speed of {{convert|10|kn}}.<ref>McLaughlin 2003, pp. 116, 119–20</ref>

===Armament===
[[File:Panteleimon1906-1910.jpg|thumb|''Panteleimon'' at anchor, circa 1906–10]]
The main armament consisted of four 40-[[caliber (artillery)|calibre]] {{convert|12|in|mm|0|adj=on}} guns mounted in twin [[gun turret]]s fore and aft of the [[superstructure]]. The electrically operated turrets were derived from the design of those used by the {{sclass-|Petropavlovsk|battleship|2}}s. These guns had a maximum elevation of +15° and their rate of fire was very slow, only one round every four minutes during gunnery trials.<ref name=m57>McLaughlin 2003, p. 119</ref> They fired a {{convert|337.7|kg|lb|adj=on|disp=flip}} shell at a [[muzzle velocity]] of {{convert|2792|ft/s|m/s|abbr=on}}. At an elevation of +10° the guns had a range of {{convert|12000|m|yd|disp=flip}}.<ref>Friedman, pp. 251–53</ref> ''Potemkin'' carried 60 rounds for each gun.<ref name=m57/>

The sixteen 45-calibre, [[152 mm 45 caliber Pattern 1892|{{convert|6|in|mm|adj=on|0|spell=in}}]] [[Canet guns|Canet]] Pattern 1891 [[quick-firing gun|quick-firing (QF) guns]] were mounted in [[casemate]]s. Twelve of these were placed on the sides of the hull and the other four were positioned at the corners of the superstructure.<ref name=m57/> They fired shells that weighed {{convert|41.46|kg|lb|abbr=on|disp=flip}} with a muzzle velocity of {{convert|792|m/s|ft/s|abbr=on|disp=flip}}. They had a maximum range of {{convert|11523|m|yd|disp=flip}} when fired at an elevation of +20°.<ref>Friedman, pp. 260–61</ref> The ship stowed 160 rounds per gun.<ref name=m72/>

Smaller guns were carried for close-range defence against [[torpedo boat]]s. These included fourteen 50-calibre Canet QF {{convert|75|mm|adj=on}} guns: four in hull [[embrasure]]s and the remaining 10 mounted on the superstructure. The ship carried 300 shells for each gun.<ref name=m57/> They fired an {{convert|4.9|kg|lb|adj=on|disp=flip}} shell at a muzzle velocity of {{convert|2700|ft/s|m/s|abbr=on}} to a maximum range of {{convert|6405|m|yd|disp=flip}}.<ref>Friedman, p. 264</ref> She also mounted six {{convert|47|mm|in|adj=on}} [[Hotchkiss gun]]s. Four of these were mounted in the [[Top (sailing ship)|fighting top]] and two on the superstructure.<ref name=m57/> They fired a {{convert|2.2|lb|kg|adj=on}} shell at a muzzle velocity of {{convert|1400|ft/s|m/s|abbr=on}}.<ref>Smigielski, p. 160</ref>

''Potemkin'' had five underwater {{convert|15|in|mm|0|adj=on}} [[torpedo tube]]s: one in the bow and two on each [[broadside]]. She carried three torpedoes for each tube.<ref name=m57/> The model of torpedo in use changed over time; the first torpedo that the ship would have been equipped with was the M1904. It had a [[warhead]] weight of {{convert|70|kg|disp=flip}} and a speed of {{convert|33|kn}} with a maximum range of {{convert|800|m|yd|disp=flip}}.<ref>Friedman, p. 348</ref>

In 1907 [[telescopic sight]]s were fitted for the 12-inch and 6-inch guns. In that or the following year {{convert|2.5|m|ftin|sp=us|adj=on}} [[rangefinder]]s were installed. The bow torpedo tube was removed in 1910–11, as was the fighting top. The following year the main gun turret machinery was upgraded and the guns were modified to improve their rate of fire to one round every 40 seconds.<ref name=m5>McLaughlin 2003, pp. 294–95</ref>

Two {{convert|57|mm|adj=on}} [[Anti-aircraft gun|anti-aircraft (AA) guns]] were mounted on the ship's superstructure on 3–6 June 1915; they were supplemented by two 75&nbsp;mm AA guns, one on top of each turret, probably during 1916. In February 1916 the ship's four remaining torpedo tubes were removed. At some point during World War I her 75&nbsp;mm guns were also removed.<ref name=m04/>

===Protection===
The maximum thickness of the Krupp cemented armour [[Belt armor|waterline belt]] was {{convert|9|in|mm|0|spell=in}} which reduced to {{convert|8|in|mm|0|spell=in}} abreast the [[magazine (artillery)|magazine]]s. It covered {{convert|237|ft|m|1}} of the ship's length and {{convert|2|in|0|adj=on|spell=in}} plates protected the waterline to the ends of the ship. The belt was {{convert|7|ft|6|in|1}} high, of which {{convert|5|ft|0}} was below the [[waterline]], and tapered down to a thickness of {{convert|5|in|mm|0|spell=in}} at its bottom edge. The main part of the belt terminated in {{convert|7|in|mm|0|adj=on|spell=in}} transverse [[bulkhead (partition)|bulkhead]]s.<ref name=m57/>

Above the belt was the upper [[strake]] of six-inch armour that was {{convert|156|ft|1}} long and closed off by six-inch transverse bulkheads fore and aft. The upper casemate protected the six-inch guns and was five inches thick on all sides. The sides of the turrets were {{convert|10|in|0|spell=in}} thick and they had a two-inch roof. The [[conning tower]]'s sides were nine inches thick. The nickel-steel armour deck was two inches thick on the flat amidships, but {{convert|2.5|in}} thick on the slope connecting it to the armour belt. Fore and aft of the armoured [[Citadel#Naval term|citadel]], the deck was {{convert|3|in|spell=in}} to the bow and stern.<ref name=m57/> In 1910–11, additional {{convert|1|in|adj=on|spell=in}} armour plates were added fore and aft; their exact location is unknown, but they were probably used to extend the height of the two-inch armour strake at the ends of the ship.<ref name=m5/>

==Service==

=== The mutiny ===
[[File:Golikov EN.jpg|thumb|150px|left|Captain Golikov]]
[[File:Gilyarovskiy II.jpg|thumb|150px|left|Ippolit Giliarovsky]]
[[File:Leader of Potemkin revolt.jpg|thumb|right|upright|[[Afanasi Matushenko|Matushenko]], the leader of the mutiny, is seen to the left of centre. Photo taken July, 1905, after arrival at [[Constanța]]&nbsp;– officer at left is in Romanian uniform.]]
During the [[Russo-Japanese War]] of 1904–05, many of the Black Sea Fleet's most experienced officers and enlisted men were transferred to the ships in the Pacific to replace losses. This left the fleet with primarily raw recruits and less capable officers. With the news of the disastrous [[Battle of Tsushima]] in May 1905 morale dropped to an all-time low, and any minor incident could be enough to spark a major catastrophe.<ref>Watts, p. 24</ref> Taking advantage of the situation, plus the disruption caused by the [[Revolution of 1905|ongoing riots and uprisings]], the Central Committee of the Social Democratic Organisation of the Black Sea Fleet, called "Tsentralka", had started preparations for a simultaneous mutiny on all of the ships of the fleet, although the timing had not been decided.<ref>Bascomb, p. 20–24</ref>

On 27 June 1905, ''Potemkin'' was at gunnery practice near [[Tendra Island]] off the Ukrainian coast when many enlisted men refused to eat the [[borscht]] made from rotten meat partially infested with [[maggot]]s. The uprising was triggered when [[Ippolit Giliarovsky]], the ship's second in command, allegedly threatened to shoot crew members for their refusal. He summoned the ship's marine guards as well as a [[tarpaulin]] to protect the ship's deck from any blood in an attempt to intimidate the crew. Giliarovsky was killed after he mortally wounded [[Grigory Vakulinchuk]], one of the mutiny's leaders. The mutineers killed seven of the ''Potemkin''{{'}}s eighteen officers, including Captain [[Evgeny Golikov]], and captured the [[torpedo boat]] {{ship|Russian torpedo boat|Ismail||2}} (No. 627). They organized a ship's committee of 25 sailors, led by [[Afanasi Matushenko]], to run the battleship.<ref>Bascomb, pp. 60–72, 88–94, 96–103</ref>

The committee decided to head for [[Odessa]] flying a [[red flag (politics)|red flag]] and arrived there later that day at 22:00. A [[general strike]] had been called in the city and there was some [[riot]]ing as the police tried to quell the strikers. The following day the mutineers refused to land armed sailors to help the striking revolutionaries take over the city, preferring instead to await the arrival of the other battleships of the Black Sea Fleet. Later that day the mutineers aboard the Potemkin captured a military transport, ''[[Vekha]]'', that had arrived in the city. The riots continued as much of the port area was destroyed by fire. On the afternoon of 29 June, Vakulinchuk's funeral turned into a political demonstration and the army attempted to ambush the sailors who participated in the funeral. In retaliation, the ship fired two six-inch shells at the theatre where a high-level military meeting was scheduled to take place, but missed.<ref>Bascomb, pp. 55–60, 112–27, 134–53, 164–67, 170–78</ref>

The government issued an order to send two [[Squadron (naval)|squadrons]] to Odessa either to force the ''Potemkin'' crew to give up or sink the battleship. ''Potemkin'' sortied on the morning of 30 June to meet the three [[battleship]]s ''Tri Sviatitelia'', {{ship|Russian battleship|Dvenadsat Apostolov||2}}, and {{Ship|Russian battleship|Georgii Pobedonosets||2}} of the first squadron, but the loyal ships turned away. The second squadron arrived with the battleships {{ship|Russian battleship|Rostislav||2}} and {{ship|Russian battleship|Sinop||2}} later that morning, and Vice Admiral [[Aleksander Krieger]], acting commander of the Black Sea Fleet, ordered the ships to proceed to Odessa. ''Potemkin'' sortied again and sailed through the combined squadrons as Krieger failed to order his ships to fire. Captain Kolands of ''Dvenadsat Apostolov'' attempted to [[naval ram|ram]] ''Potemkin'' and then detonate his ship's magazines, but he was thwarted by members of his crew. Krieger ordered his ships to fall back, but the crew of ''Georgii Pobedonosets'' mutinied and joined ''Potemkin''.<ref>Bascomb, pp. 179–201</ref>

The following morning, loyalist members of ''Georgii Pobedonosets'' retook control of the ship and ran it aground in Odessa harbor.<ref>Zebroski, p. 21</ref> The crew of ''Potemkin'', together with ''Ismail'', decided to sail for Constanța later that day where they could restock food, water and coal. The Romanians [[Action of 2 July 1905|refused]] to provide the supplies, backed by the presence of their small [[protected cruiser]] [[NMS Elisabeta|''Elisabeta'']], so the ship's committee decided to sail for the small, barely defended port of [[Feodosiya|Theodosia]] in the [[Crimea]] where they hoped to resupply. The ship arrived on the morning of 5 July, but the city's governor refused to give them anything other than food. The mutineers attempted to seize several barges of coal the following morning, but the port's garrison ambushed them and killed or captured 22 of the 30 sailors involved. They decided to return to Constanța that afternoon.<ref>Bascomb, pp. 224–27, 231–47, 252–54, 265–70, 276–81</ref>

[[File:Knyaz'Potemkin-Tavricheskiy1905Constanta.jpg|thumb|''Potemkin'' at anchor with the Romanian flag hoisted on her mast, Constanța, July 1905]]
''Potemkin'' reached its destination at 23:00 on 7 July and the Romanians agreed to give asylum to the crew if they would disarm themselves and surrender the battleship. ''Ismail''{{'}}s crew decided the following morning to return to Sevastopol and turn themselves in, but ''Potemkin''{{'}}s crew voted to accept the terms. Captain Negru, commander of the port, came aboard at noon and hoisted the Romanian flag and then allowed the ship to enter the inner harbor. Before the crew disembarked, Matushenko ordered that the ''Potemkin''{{'}}s [[Kingston valve]]s be opened so ''Potemkin'' would sink to the bottom.<ref>Bascomb, pp. 286–99</ref>

===Later service===
When Rear Admiral Pisarevsky reached Constanța on the morning of 9 July, he found the ''Potemkin'' half sunk in the harbor and flying the Romanian flag. After several hours of negotiations with the Romanian Government, the battleship was handed over to the Russians. Later that day the [[Russian Navy Ensign]] was raised over the battleship.<ref>Bascomb, p.297</ref> She was then easily refloated by the navy, but the salt water had damaged her engines and boilers. She left Constanța on 10 July, having to be towed back to Sevastopol, where she arrived on 14 July.<ref name=m1>McLaughlin 2003, p. 121</ref> The ship was renamed ''Panteleimon'' ({{lang-ru|Пантелеймон}}), after [[Saint Pantaleon]],<ref>Silverstone, p. 380</ref> on 12 October 1905. Some members of ''Panteleimon''{{'}}s crew joined a mutiny that began aboard the cruiser {{ship|Russian cruiser|Ochakov||2}} in November, but it was easily suppressed as both ships had been earlier disarmed.<ref name=m1/>

''Panteleimon'' received an experimental underwater communications set<ref>Godin & Palmer, p. 33</ref> in February 1909. Later that year, she accidentally rammed and sank the submarine {{ship|Russian submarine|Kambala||2}} at night on 11 June,<ref name=m1/> killing the 16 crewmen aboard the submarine.<ref>Polmar & Noot, p. 230</ref>

While returning from a port visit to Constanța in 1911, ''Panteleimon'' ran aground on 2 October. It took several days to refloat her and make temporary repairs, and the full extent of the damage to her bottom was not fully realized for several more months. The ship participated in training and gunnery exercises for the rest of the year; a special watch was kept to ensure that no damaged seams were opened while firing. Permanent repairs, which involved replacing her boiler foundations, plating, and a large number of her hull frames, lasted from 10 January to 25 April 1912. The navy took advantage of these repairs to [[wikt:overhaul|overhaul]] her engines and boilers.<ref>McLaughlin 2003, pp. 120–21, 172, 295</ref>

=== World War I ===
''Panteleimon'', flagship of the 1st Battleship Brigade, accompanied by the pre-dreadnoughts {{ship|Russian battleship|Evstafi||2}},  {{ship|Russian battleship|Ioann Zlatoust||2}}, and ''Tri Sviatitelia'', covered the pre-dreadnought ''Rostislav'' while she bombarded [[Trabzon|Trebizond]] on the morning of 17 November 1914. They were intercepted the following day by the Ottoman battlecruiser ''Yavuz Sultan Selim'' (the ex-German [[SMS Goeben|SMS ''Goeben'']]) and the [[light cruiser]] [[SMS Breslau|''Mdilli'']]  on their return voyage to Sevastopol in what came to be known as the [[Battle of Cape Sarych]]. Despite the noon hour the conditions were foggy; the [[capital ship]]s initially did not spot each other. Although several other ships opened fire, hitting the ''Goeben'' once, ''Panteleimon'' held fire because her turrets could not see the German ships before they disengaged.<ref>McLaughlin 2001, pp. 123, 127</ref>

''Tri Sviatitelia'' and ''Rostislav'' bombarded Ottoman fortifications at the mouth of the [[Bosphorus]] on 18 March 1915, the first of several attacks intended to divert troops and attention from the ongoing [[Gallipoli Campaign]], but fired only 105 rounds before sailing north to rejoin ''Panteleimon'', ''Ioann Zlatoust'' and ''Evstafi''.<ref name=N49,53/> ''Tri Sviatitelia'' and ''Rostislav'' were intended to repeat the bombardment the following day, but were hindered by heavy fog.<ref>Halpern, p. 230</ref> On 3 April, ''Yavuz Sultan Selim'' and several ships of the Turkish navy raided the Russian port at Odessa; the Russian battleship squadron sortied to intercept them. The battleships chased ''Yavuz Sultan Selim'' the entire day, but were unable to reach effective gunnery range and were forced to break off the chase.<ref>Halpern, p. 231</ref> On 25 April ''Tri Sviatitelia'' and ''Rostislav'' repeated their bombardment of the Bosporus forts. ''Tri Sviatitelia'', ''Rostislav'' and ''Panteleimon'' bombarded the forts again on 2 and 3 May. This time a total of 337 main gun rounds were fired in addition to 528 six-inch shells between the three battleships.<ref name=N49,53>Nekrasov, pp. 49, 54</ref>

On 9 May 1915, ''Tri Sviatitelia'' and ''Panteleimon'' returned to bombard the Bosphorus forts, covered by the remaining pre-dreadnoughts. ''Yavuz Sultan Selim'' intercepted the three ships of the covering force, although no damage was inflicted by either side. ''Tri Sviatitelia'' and ''Pantelimon'' rejoined their consorts and the latter scored two hits on ''Yavuz Sultan Selim'' before she broke off the action. The Russian ships pursued her for six hours before giving up the chase. On 1 August, all of the Black Sea pre-dreadnoughts were transferred to the 2nd Battleship Brigade, after the more powerful dreadnought {{ship|Russian battleship|Imperatritsa Mariya||2}} entered service. On 1 October the new dreadnought provided cover while ''Ioann Zlatoust'' and ''Pantelimon'' bombarded [[Zonguldak]] and ''Evstafi'' shelled the nearby town of Kozlu.<ref name=m04>McLaughlin 2003, p. 304</ref> The ship bombarded [[Varna]] twice in October 1915; during the second bombardment on 27 October, she entered Varna Bay and was unsuccessfully attacked by two German submarines stationed there.<ref>Nekrasov, p. 67</ref>

''Panteleimon'' supported Russian troops in early 1916 as they captured Trebizond<ref name=m1/> and participated in an anti-shipping sweep off the northwestern [[Anatolia]]n coast in January 1917 that destroyed 39 Ottoman sailing ships.<ref>Nekrasov, p. 116</ref> On 13 April 1917, after the [[February Revolution]], the ship was renamed ''Potemkin-Tavricheskiy'' ({{lang-ru|Потёмкин-Таврический}}), and then on 11 May renamed ''Borets za svobodu'' ({{lang-ru|Борец за свободу}}&nbsp;– ''Freedom Fighter'').<ref name=m1/>

===Reserve and decommissioning===
She was placed in reserve in March 1918 and was captured by the Germans at Sevastopol in May. They handed the ship over to the Allies in December 1918 after the Armistice. The British wrecked her engines on 19 April 1919 when they left the Crimea to prevent the advancing Bolsheviks from using her against the White Russians. Thoroughly obsolete by this time, the ship was captured by both sides during the [[Russian Civil War]], but was abandoned by the White Russians when they evacuated the Crimea in November 1920. ''Borets za svobodu'' was scrapped beginning in 1923, although she was not stricken from the [[Navy List]] until 21 November 1925.<ref name=m1/>

==Legacy==
[[File:Vintage Potemkin.jpg|thumb|upright|Poster for [[Sergei Eisenstein]]'s [[Battleship Potemkin|1925 film]] dramatising the mutiny]]
The immediate effects of the mutiny are difficult to assess. It may have influenced [[Tsar]] [[Nicholas II of Russia|Nicholas II]]'s decisions to end the Russo-Japanese War and accept the [[October Manifesto]], as the mutiny demonstrated that his régime no longer had the unquestioning loyalty of the military. The mutiny's failure did not stop other revolutionaries from inciting insurrections later that year, including the [[Sevastopol Uprising]]. [[Vladimir Lenin]], leader of the [[Bolshevik Party]], called the 1905 Revolution, including the ''Potemkin'' mutiny, a "dress rehearsal" for his successful [[Russian Revolution|revolution]] in 1917.<ref>Bascomb, p. 185</ref> The Communists seized upon it as a [[propaganda]] symbol for their party and unduly emphasized their role in the mutiny. In fact, Matushenko explicitly rejected the Bolsheviks because he and the other leaders of the mutiny were [[Socialism|Socialists]] of one type or another and cared nothing for [[Communism]].<ref name=B34>Bascomb, pp. 183–84</ref>

The mutiny was memorialized most famously by [[Sergei Eisenstein]] in his 1925 silent film ''[[Battleship Potemkin]]'', although the French [[silent film]] ''La Révolution en Russe'' (''Mutiny on a Man-of-War in Odessa'' or ''Revolution in Odessa'', 1905), directed by [[Ferdinand Zecca]] or [[Lucien Nonguet]] (or both), was the first film to depict the mutiny,<ref>Oscherwitz & Higgins, pp. 320–21</ref> preceding Eisenstein's far more famous film by 20 years. Filmed shortly after the Bolshevik victory in the Russian Civil War of 1917–22,<ref name=B34/> with the derelict battleship ''[[Russian battleship Dvenadsat Apostolov|Dvenadsat Apostolov]]'' standing in for the broken-up ''Potemkin'',<ref>McLaughlin 2003, p. 52</ref> Eisenstein recast the mutiny into a predecessor of the [[October Revolution]] of 1917 that swept the Bolsheviks to power. He emphasized their role, and implied that the mutiny failed because Matushenko and the other leaders were not better Bolsheviks. Eisenstein made other changes to dramatize the story, ignoring the major fire that swept through Odessa's dock area while ''Potemkin'' was anchored there, combining the many different incidents of rioters and soldiers fighting into a famous sequence on the steps (today known as [[Potemkin Stairs]]), and showing a tarpaulin thrown over the sailors to be executed.<ref name=B34/>

In accordance with the Marxist doctrine that history is made by collective action, not individuals, Eisenstein forbore to single out any person in his film, but rather focused on the "mass protagonist".<ref>Bordwell, pp. 43, 267</ref> Soviet film critics hailed this approach, including the [[dramaturge]] and critic, [[Adrian Piotrovsky]], writing for the [[Leningrad]] newspaper ''Krasnaia gazeta'': <blockquote>The hero is the sailors' battleship, the Odessa crowd, but characteristic figures are snatched here and there from the crowd. For a moment, like a conjuring trick, they attract all the sympathies of the audience: like the sailor Vakulinchuk, like the young woman and child on the Odessa Steps, but they emerge only to dissolve once more into the mass. This signifies: no film stars but a film of real-life types.<ref>Quoted in Taylor, p. 76</ref></blockquote>

Similarly, theatre critic Alexei Gvozdev wrote in the journal ''Artistic Life'' (''Zhizn ikusstva''):<ref>Taylor, p. 76</ref> "In ''Potemkin'' there is no individual hero as there was in the old theatre. It is the mass that acts: the battleship and its sailors and the city and its population in revolutionary mood."<ref>Quoted in Taylor, p. 78</ref>

The last survivor of the mutiny was Ivan Beshoff, who died on 24 October 1987 at the age of 102.<ref>{{cite web|url=http://query.nytimes.com/gst/fullpage.html?res=9B0DE1DA1E3BF93BA15753C1A961948260|title=Last Survivor of Mutiny on the Potemkin|first=Ivan|last=Beshoff|publisher=The New York Times|date=28 October 1987|agency=Associated Press}}</ref>

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{reflist|30em}}

== References ==
* {{cite book
  | last = Bascomb
  | first = Neal
  | year = 2007
  | title = [[Red Mutiny: Eleven Fateful Days on the Battleship Potemkin]]
  | publisher = Houghton Mifflin
  | location = Boston|isbn=0-618-59206-7
  }}
*{{cite book|last=Bordwell|first=David|authorlink=David Bordwell|title=The Cinema of Eisenstein|year=1993|publisher=Harvard University Press|location=Cambridge, Massachusetts|isbn=0-674-13138-X}}
*{{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
* {{cite book|last1=Godin|first1=Oleg A.|last2=Palmer|first2=David R.|title=History of Russian Underwater Acoustics|url=https://books.google.com/books?id=tsZYbQoNijsC|year=2008|publisher=World Scientific|location=Singapore|isbn=981-256-825-5|last-author-amp=yes}}
*{{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-55750-352-4}}
*{{cite book|last=McLaughlin|first=Stephen|editor=Preston, Antony|title=Warship 2001–2002|year=2001|publisher=Conway Maritime Press|location=London|isbn=0-85177-901-8|pages=117–40|chapter=Predreadnoughts vs a Dreadnought: The Action off Cape Sarych, 18 November 1914}}
* {{cite book|last=McLaughlin|first=Stephen|title=Russian & Soviet Battleships|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2003|isbn=1-55750-481-4}}
*{{cite book|last=Nekrasov|first=George|title=North of Gallipoli: The Black Sea Fleet at War 1914–1917|series=East European Monographs|volume=CCCXLIII|year=1992|publisher=East European Monographs|location=Boulder, Colorado|isbn=0-88033-240-9}}
*{{cite book|last1=Oscherwitz|first1=Dayna |last2=Higgins|first2=MaryEllen|title=Historical Dictionary of French Cinema|series=Historical Dictionaries of Literature and the Arts|volume=15|year=2007|publisher=Scarecrow Press|location=Lanham, Maryland|isbn=978-0-8108-7038-3|last-author-amp=yes}}
*{{cite book|last1=Polmar|first1=Norman|last2=Noot|first2=Jurrien|title=Submarines of the Russian and Soviet Navies, 1718–1990|year=1991|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-570-1|last-author-amp=yes}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
*{{cite book|editor=Roberts, John|last=Smigielski|first=Adam|chapter=Imperial Russian Navy Cruiser Varyag|title=Warship III|year=1979|publisher=Conway Maritime Press|location=London|isbn=0-85177-204-8|pages=155–67}}
*{{cite book|last=Taylor|first=Richard|title=The Battleship Potemkin|series=KINOfiles Film Companion|volume=1|year=2000|publisher=I.B. Tauris |location=London & New York|isbn=1-86064-393-0}}
* {{cite book
  | last = Zebroski
  | first = Robert
  | chapter = The Battleship ''Potemkin'' and Its Discontents, 1905
  | title = Naval Mutinies of the Twentieth Century: An International Perspective
  |editor1-last= Bell|editor1-first = Christopher&nbsp;M.|editor2-last=Elleman|editor2-first=Bruce&nbsp;A.| location = London
  | publisher = Frank Cass
  | year = 2003|isbn=0-203-58450-3|pages=7–25
  }}

== External links ==
{{Commons category|Potemkin (ship, 1900)|''Potemkin''}}
{{Portal|Battleships}}

* [http://flot.sevastopol.info/eng/ship/predreadnoughts/potemkin.htm Battleship ''Kniaz Potemkin Tavricheskiy'' on Black Sea Fleet]
* [http://www.steelnavy.com/CombrigPanteleimon.htm A history of the ship, with photos of a model under construction]
* [http://www.marxists.org/archive/lenin/works/1905/jul/10d.htm A brief contemporary article by Lenin  on the mutiny with the text of the sailors' manifesto] 
*[https://www.youtube.com/watch?t=22&v=tKHLw3qLU0M Annotated version of Zecca's ''La Révolution en Russe'']

{{Potemkin}}
{{Russo-JapaneseWarRussianShips}}
{{WWIRussianShips}}
{{1905 shipwrecks}}
{{1919 shipwrecks}}

{{Use dmy dates|date=November 2010}}

{{DEFAULTSORT:Potemkin}}
[[Category:1900 ships]]
[[Category:1905 Russian Revolution]]
[[Category:Battleships of the Imperial Russian Navy]]
[[Category:Captured ships]]
[[Category:Conflicts in 1905]]
[[Category:History of Odessa]]
[[Category:Maritime incidents in 1905]]
[[Category:Shipwrecks in the Black Sea]]
[[Category:Shipwrecks of Romania]]
[[Category:Scuttled vessels]]
[[Category:Naval mutinies]]
[[Category:Ships built at the Black Sea Shipyard]]
[[Category:Ships of the Romanian Naval Forces]]
[[Category:Victorian-era battleships of Russia]]
[[Category:World War I battleships of Russia]]