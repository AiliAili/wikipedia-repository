{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name          = Sir Ralph Alexander Cochrane
|image         = AVM Sir Ralph Cochrane.jpg
|image_size    = 250px
|alt           = 
|caption       = Air Vice Marshal Cochrane in 1943
|birth_date    = {{birth date|1895|02|24|df=y}}
|death_date    = {{Death date and age|1977|12|17|1895|02|24|df=y}}
|birth_place   = [[Springfield, Fife]], Scotland
|death_place   = 
|placeofburial = 
|nickname      = 
|allegiance    = United Kingdom
|branch        = [[Royal Navy]] (1908–20)<br/>[[RAF]] (1920–52)
|serviceyears  = 1912–52
|rank          = [[Air Chief Marshal]]
|servicenumber = 
|unit          = 
|commands      = [[Vice Chief of the Air Staff (United Kingdom)|Vice Chief of the Air Staff]] (1950–52)<br/>[[RAF Flying Training Command|Flying Training Command]] (1947–50)<br/>[[RAF Transport Command|Transport Command]] (1945–47)<br/>[[No. 5 Group RAF|No. 5 Group]] (1943–45)<br/>[[No. 3 Group RAF|No. 3 Group]] (1942–43)<br/>[[No. 7 Group RAF|No. 7 Group]] (1940)<br/>[[RAF Abingdon]] (1939–40)<br/>[[Chief of the Air Staff (New Zealand)|Chief of the New Zealand Air Staff]] (1937–39)<br/>[[No. 8 Squadron RAF|No. 8 Squadron]] (1929)<br/>[[No. 3 Squadron RAF|No. 3 Squadron]] (1924–25)
|battles       = [[First World War]]<br/>[[Second World War]]
|awards        = [[Knight Grand Cross of the Order of the British Empire]]<br/>[[Knight Commander of the Order of the Bath]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]]<br/>[[Mentioned in Despatches]] (4)
|relations     = 
|laterwork     = Director of Rolls Royce
}}
[[Air Chief Marshal]] '''Sir Ralph Alexander Cochrane''', {{postnominals|country=GBR|size=100%|sep=,|GBE|KCB|AFC}} (24 February 1895 – 17 December 1977) was a British aviator and [[Royal Air Force]] officer, perhaps best known for his role in [[Operation Chastise]], the famous "Dambusters" raid.

==Early RAF career==
Ralph Cochrane was born on 24 February 1895, the youngest son of [[Thomas Cochrane, 1st Baron Cochrane of Cults]], in the Scottish village of [[Springfield, Fife|Springfield]]. Although not certain, it is likely that he attended the [[Osborne House|Royal Naval College at Osborne]] in 1908. On 15 September 1912, Cochrane entered the [[Royal Navy]] proper as a [[midshipman]].<ref name=air>[http://www.rafweb.org/Biographies/Cochrane.htm Air of Authority – A History of RAF Organisation – Air Chief Marshal The Hon Sir Ralph Cochrane]</ref>

During the [[First World War]], Cochrane served in the [[Royal Naval Air Service]] piloting airships.<ref name=air/> He also completed a tour as a staff officer in the [[Admiralty]]'s Airship Department.<ref name=air/>

In January 1920, he was removed from the Navy List and granted a commission in the Royal Air Force.<ref name=air/> Between the wars, Cochrane served in various staff positions and commanded [[No. 3 Squadron RAF|No. 3 Squadron]] from 1924 before attending the RAF Staff College and commanding [[No. 8 Squadron RAF|No. 8 Squadron]] from 1929.<ref name=air/> He attended the [[Royal College of Defence Studies|Imperial Defence College]] in 1935.<ref name=air/>

At the request of Group Captain [[T. M. Wilkes]], New Zealand Director of Air Services,<ref>http://nzetc.victoria.ac.nz/tm/scholarly/tei-WH2AirF-c3.html</ref> in 1936 Cochrane was sent to New Zealand to assist with the establishment of the [[Royal New Zealand Air Force]] as an independent service from the army.<ref name=air/> On 1 April 1937, Cochrane was appointed [[Chief of the Air Staff (New Zealand)|Chief of the Air Staff]] of the Royal New Zealand Air Force.<ref name=air/>

[[Image:Cochrane, Gibson, King George VI and Whitworth discussing the Dambusters Raid.jpg|thumb|right|300px|Air Vice-Marshal Ralph Cochrane, Wing Commander [[Guy Gibson]], King [[George VI of the United Kingdom|George VI]] and Group Captain [[John Whitworth]] discussing the Dambusters Raid in May 1943]]

==Second World War and the post-war years==
During the Second World War, Cochrane commanded [[No. 7 Group RAF|No. 7 Group]] from July 1940, [[No. 3 Group RAF|No. 3 Group]] from September 1942 and [[No. 5 Group RAF|No. 5 Group]] from February 1943; all these Groups were in [[RAF Bomber Command]].<ref name=air/> 5 Group became the most efficient and elite Main Force bomber group undertaking spectacular raids.<ref name=odnb>[http://www.oxforddnb.com/view/article/30945 Oxford Dictionary of National Biography]</ref> Cochrane commanded the Dam-Busters raid.<ref>[http://www.scottish-places.info/people/famousfirst117.html Sir Ralph Alexander Cochrane] Gazetteer for Scotland</ref> There was intense, sometimes openly hostile, rivalry between Cochrane and Air Vice Marshal [[Don Bennett]], who saw Cochrane's experimentation with low-level target marking through 617 Squadron in 1944 as a direct threat to his own specialist squadrons' reputation.<ref name=odnb/>

In February 1945, Cochrane became Air Officer Commanding at [[RAF Transport Command]], a position he held until 1947 when he became Air Officer Commanding at [[RAF Flying Training Command]].<ref name=air/> During this time he managed the [[Berlin Airlift]]. In 1950 Cochrane was appointed [[Vice-Chief of the Air Staff (United Kingdom)|Vice-Chief of the Air Staff]].<ref name=air/> Ralph Cochrane retired from the service in 1952.<ref name=air/> Following his retirement, Cochrane entered the business world notably as director of [[Rolls-Royce Limited|Rolls-Royce]].<ref name=air/> He was also chairman of RJM exports which manufactured scientific models and is now known as [http://www.cochranes.co.uk/ Cochranes of Oxford].<ref name=air/>

==Honours and Awards==
In the [[1939 New Year Honours|New Year Honours 1939]] Cochrane was appointed a [[Commander of the Order of the British Empire]] (Military Division).<ref>{{London Gazette |issue=34585 |date=30 December 1938 |startpage=10 |supp=x}}</ref> In the [[1943 New Year Honours|New Year Honours 1943]] Cochrane was appointed as a [[Companion of the Order of the Bath]] (Military Division).<ref>{{London Gazette |issue=35841 |date=29 December 1942 |startpage=4}}</ref> In the 1945 New Years Honour list he was appointed a [[Knight Commander of the Order of the British Empire]]. In the [[1948 Birthday Honours|1948 King's Birthday Honours]] he was appointed as a [[Knight Commander of the Order of the Bath]]. In the [[1950 Birthday Honours|1950 King's Birthday Honours]], he was appointed [[Knight Grand Cross of the Order of the British Empire]].<ref>http://www.rafweb.org/Biographies/Cochrane.htm</ref>

== Dates of Rank ==
{| class="wikitable"
! Rank !! Date !! Role
|-
| Wing Commander || 1933<ref>{{London Gazette |issue=33955 |date=30 June 1933 |startpage=4386 }}</ref> ||
|-
| Acting Group Captain || 1937<ref>{{London Gazette |issue=34419 |date=20 July 1937 |startpage=4670 }}</ref> || On secondment to RNZAF 
|-
| Group Captain || 1938<ref>{{London Gazette |issue=34527 |date=1 July 1938 |startpage=4248 }}</ref> || 
|-
| Air Commodore (temporary) || 1940<ref>{{London Gazette |issue=34949 |date=20 September 1940 |startpage=5580}}</ref> || 
|-
| Air Marshal (Acting) || 1945<ref>{{London Gazette |issue=36945 |date=16 February 1945 |startpage=983 }}</ref> ||
|-
| Air Marshal  || 1946<ref>{{London Gazette |issue=37423 |date=4 January 1946 |startpage=347 }}</ref> ||
|-
| Air Chief Marshal || 1949<ref>{{London Gazette |issue=38583 |date=12 April 1949 |startpage=1821 }}</ref> ||
|-
|}

==References==
{{reflist|30em}}

{{commons|Ralph A. Cochrane}}

{{s-start}}
{{s-mil}}
{{s-new|reason=Service became independent}} 
{{s-ttl|title=[[Chief of the Air Staff (New Zealand)|Chief of the Air Staff]] ([[Royal New Zealand Air Force|RNZAF]])|years=1937–1939}}
{{s-aft|after=[[Hugh Saunders]]}}
|-
{{s-vac|last=[[Duncan Pitcher]]}} 
{{s-ttl|title=[[Air Officer Commanding]] [[No. 7 Group RAF|No. 7 Group]]|years=1940}}
{{s-aft|after=[[Leonard Cockey]]}}
|-
{{s-bef|before=[[Alec Coryton]]}} 
{{s-ttl|title=Air Officer Commanding [[No. 5 Group RAF|No. 5 Group]]|years=1943–1945}}
{{s-aft|after=[[Hugh Constantine]]}}
|-
{{s-bef|before=[[Frederick Bowhill|Sir Frederick Bowhill]]}}
{{s-ttl|title=Air Officer Commanding-in-Chief [[RAF Transport Command|Transport Command]]|years=1945–1947}}
{{s-aft|after=[[Brian Edmund Baker|Sir Brian Baker]]}}
|-
{{s-bef|before=[[Arthur Coningham (RAF officer)|Sir Arthur Coningham]]}}
{{s-ttl|title=Air Officer Commanding-in-Chief [[RAF Flying Training Command|Flying Training Command]]|years=1947–1950}}
{{s-aft|after=[[Hugh Walmsley|Sir Hugh Walmsley]]}}
|-
{{s-bef|before=[[Arthur Sanders (RAF officer)|Sir Arthur Sanders]]}}
{{s-ttl|title=[[Vice Chief of the Air Staff (United Kingdom)|Vice Chief of the Air Staff]]|years=1950–1952}}
{{s-aft|after=[[John Baker (RAF officer)|Sir John Baker]]}}
|-
{{s-end}}

{{DEFAULTSORT:Cochrane, Ralph Alexander}}
[[Category:1895 births]]
[[Category:1977 deaths]]
[[Category:Royal Navy officers]]
[[Category:Royal Naval Air Service aviators]]
[[Category:Royal Air Force air marshals of World War II]]
[[Category:Knights Grand Cross of the Order of the British Empire]]
[[Category:Knights Commander of the Order of the Bath]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Younger sons of barons]]
[[Category:Royal Navy officers of World War I]]