<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Arrowbile
 | image=Waterman_Aerobile_in_flight.jpg
 | caption=The third Arrowbile, NR18932
}}{{Infobox Aircraft Type
 | type=[[roadable aircraft|Roadable light aircraft]]
 | national origin=United States
 | manufacturer=Watermann Arrowplane Co.
 | designer=
 | first flight=21 February 1937
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=5
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=Waterman Arrowplane
 | variants with their own articles=
}}
|}

The '''Waterman Arrowbile''' was a [[tailless aircraft|tailless]], two-seat, single-engine, [[pusher configuration]] [[roadable aircraft]] built in the US in the late 1930s. One of the first of its kind, it flew safely but generated little customer interest, and only five were produced.

==Design and development==

[[Waldo Waterman]]'s first flying wing aircraft was the unofficially named [[Waterman Whatsit]], a [[pusher configuration]] [[Mid-wing|low swept-wing monoplane]] with [[fin]]s near its  [[wing tip]]s.  The Whatsit also featured a wing-mounted [[tricycle undercarriage]] and a trim [[foreplane]].  Powered by a 100&nbsp;hp (75&nbsp;kW) [[Kinner]] K-5 [[radial engine|5-cylinder radial]] pusher engine, it first flew in 1932.<ref name=AB/>

In May 1935 Waterman completed a submission to the government funded Vidal Safety Airplane competition.  This was the '''Arrowplane''', sometimes known as the '''W-4'''.  This adopted a similar layout to the Whatsit but had a [[strut]]-braced [[Mid-wing|high wing]] on a blunt-nosed, narrow  [[fuselage]] pod with a tricycle undercarriage mounted under it.  Its wings had wooden spars and metal ribs and were [[aircraft fabric covering|fabric covered]], with triangular endplate fins carrying upright [[rudder]]s. Its fuselage was steel framed and aluminium covered. It was powered by a 95&nbsp;hp (71&nbsp;kW) [[straight engine|inverted inline]] 4-cylinder [[Menasco Pirate|Menasco B-4 Pirate]] pusher engine mounted high in the rear of the fuselage.<ref name=AB/>

The Arrowplane was not intended for production or to be roadable, but its success in the Vidal competition encouraged Waterman to form the Waterman Arrowplane Co. in 1935 for production of a roadable version.  The resulting Arrowbile, referred to by Waterman as the '''W-5''', was similar both structurally and aerodynamically to the Arrowplane, though the fins differed in shape, with rounded [[leading edge]]s and swept-back rudder hinges. For road use the wings and propeller could be quickly detached.  The main other differences were in engine choice, the need to drive the wheels and to use conventional car floor-type controls on the road. The air-cooled Menasco was replaced by a water-cooled engine as used by most cars.  Waterman modified a 6-cylinder upright, 100&nbsp;hp (75&nbsp;kW) [[Studebaker]] unit and placed it lower down in the pod, driving the propeller shaft at the top of the fuselage via six ganged V-belts with a 1.94:1 speed reduction. The radiator was in the forward fuselage, fed from a duct opening in the extreme upper nose. On the ground the engine drove the main wheels through a differential gear, as normal, and the car was steered by its nosewheel. The wheels were enclosed in [[fairing (aircraft)|fairings]], initially as a road safety measure. Instead of removing the propeller for the road, it could be de-clutched to prevent it windmilling the engine at speed.<ref name=AB/><ref name=JAWA41/>

The wheel in the two-seat cabin controlled the Arrowbile both on the road and in the air.  Outer wing [[elevon]]s moved together to alter [[pitch (aviation)|pitch]] and differentially to [[banked turn|bank]].  The rudders, interconnected with the elevons when the wheel was turned, moved only outwards, so in a turn only the inner rudder was used, both adjusting [[Aircraft principal axes|yaw]] as normal and assisting the elevon in depressing the inner [[wing tip]]. This system had been used on the Arrowplane as a safety feature to avoid the commonly fatal [[spin (flight)|spin]] out of climb and turn from take-off accident but the raked rudder hinge of the Arrowbile provided the banking component even from a nose-down attitude.  There were no conventional [[flap (aircraft)|flaps]] or wing mounted [[air brake (aircraft)|airbrakes]] but the rudders could be operated as brakes by opening them outwards together with a control independent of the wheel.<ref name=AB/> The cabin interior was designed to motor car standards, with easy access and a baggage space under the seats.<ref name=JAWA41/>

The Arrowbile first flew on 21 February 1937, making it a close contemporary of the [[Gwinn Aircar]], and a second prototype with a number of minor modifications followed.  Studebaker were interested in the Arrowbile because of the use of their engine and ordered five.  The third Arrowbile was the first of this order. However there was little market response and the line was halted in 1938, with no more production aircraft completed.  The production aircraft had several changes, some of which aimed to emphasise the similarities with cars; there was a radiator grille with a single headlight centrally above it and also car type doors and petrol filler cap.<ref name=AB/>

The fourth Aerobile was completed as a conventional, non-roadable aircraft; Waterman initially retained the Studebaker engine but in 1941 replaced it with an air-cooled {{convert|120|hp|kW|0|abbr=on}} Franklin. In 1943 he modified the wings with slotted flaps and later still replaced the braced wing with a cantilever one, using the wing from the unbuilt fifth aircraft.<ref name=aerofiles2/>

The last, sixth aircraft was not completed and flown until May 1957.  It was a three-seat, roadable version powered by a water-cooled {{convert|120|hp|kW|0|abbr=on}} Tucker-Franklin. This was cooled by radiators on each side of the engine, fed air by fuselage side scoops. In the absence of the forward radiator the nose was remodelled, becoming shorter and blunter. The fins were also altered so that the upper and lower leading edges met at an [[Types of angles|acute angle]]. At some point this particular Arrowbile was renamed the '''Aerobile''', though it was not a name that Waterman used.<ref name=AB/><ref name=aerofiles/>

==Operational history==
In early September 1937 the first three Arrowbiles flew from the factory at [[Santa Monica]] to the National Air Races venue at [[Cleveland]], a great circle distance of about {{convert|2060|mi|km|0|abbr=on}}. The first force landed en route, but the other two reached the races and gave demonstration flights.<ref name=AB/>

==Variants==
''Data from'' Meaden<ref name=AB/> ''and'' Lennart Johnsson<ref name=aerofiles2/>
;Arrowplane: Arrowbile precursor, establishing its aerodynamic configuration. Not roadable. 95&nbsp;hp (71&nbsp;kW) Menasco Pirate inverted [[inline engine (aviation)|inline engine]].
;Arrowbile:Road-going version, 3 built. 4th modified to aircraft-only configuration. Later fitted with an air-cooled, 120&nbsp;hp (90&nbsp;kW) Franklin engine.
;Aerobile: 5th completed Arrowbile renamed, though not by Waterman.  A three-seater with a modified, water-cooled [[Franklin Engine Company|Franklin]] engine and revised fins.

==Aircraft on display==
[[File:Waterman Aerobile 6.jpg|thumb|right|National Air and Space Museum Aerobile ''N54P'']]
*National Air and Space Museum [[Steven F. Udvar-Hazy Center]] - Aerobile ''N54P''<ref name=Ogden/>

== Specifications (Arrowbile) ==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1941<ref name=JAWA41/>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|capacity=Two
|length ft=19
|length in=4
|length note=
|span ft=38
|span in=0
|span note=
|height ft=
|height in=8
|height not8=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight lb=1941
|empty weight note=
|gross weight lb=2500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=25 US gal (21 Imp gal; 95 L)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Studebaker-Waterman]]
|eng1 type=6-cylinder inline, water-cooled
|eng1 hp=100
|eng1 note=
|power original=
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed mph=120
|max speed note=
|cruise speed mph=102
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range miles=350
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ftmin=600
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|more performance=
*'''Landing speed:''' 45 mph (72 km/h; 39 kn)
*'''Maximum road speed:''' approximately 70 mph (113 km/h)<ref name=aerofiles/>
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name=JAWA41>{{cite book |title= Jane's All the World's Aircraft 1941|last= Bridgman |first= Leonard |coauthors= |year=1941|publisher=Sampson, Low, Marston and Co. Ltd|location= London|page=232c}}</ref>

<ref name=Ogden>{{cite book |title= Aviation Museums and Collections of North America|last=Ogden|first=Bob| year=2011|edition=2|publisher=Air-Britain (Historians)|location=Tonbridge, Kent |isbn=978-0851304274}}</ref>

<ref name=AB>{{cite journal |last=Meaden |first=Jack|year=1998 |title= The Waterman Aeroplanes|journal=Air Britain Archives|volume= |issue=3 |page=81 }}</ref>

<ref name=aerofiles>{{cite web |url=http://www.aerofiles.com/_water.html|title=Waterman aircraft |author= |date= |work= |publisher= |accessdate=5 August 2012}}</ref>

<ref name=aerofiles2>{{cite web |url=http://www.aerofiles.com/waterman.html|title=Waterman aircraft history |author= |date= |work= |publisher= |accessdate=6 August 2012}}</ref>

}}

==External links==
{{commons category|Waterman Arrowbile}}

*{{cite web |url=http://www.criticalpast.com/products/view/65675038371_Arrowbile-plane_attaching-wings_tailless-machine_taking-off|title= CriticalPast video showing Arrowbile on the road in Santa Monica,California. |author= |date= |work= |publisher= |accessdate=2 March 2014}}

{{Flying cars}}

[[Category:Roadable aircraft]]
[[Category:United States sport aircraft 1930–1939]]
[[Category:Tailless aircraft]]