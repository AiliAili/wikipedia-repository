{{Infobox ice hockey player
| image = File:Nikita Filatov 7 (cropped3).jpg
| caption =
| image_size = 230px
| position = [[Left Wing (ice hockey)|Left Wing]]
| shoots = Right
| height_ft = 6
| height_in = 0
| weight_lb = 190
| team = [[Admiral Vladivostok]]
| league = [[Kontinental Hockey League|KHL]]
| former_teams = [[HC CSKA Moscow|CSKA Moscow]]<br>[[Columbus Blue Jackets]]<br>[[Ottawa Senators]]<br>[[Salavat Yulaev Ufa]]<br>[[Yugra Khanty-Mansiysk]]
| birth_date = {{birth date and age|1990|5|25}}
| birth_place = Moscow, [[Russian Soviet Federative Socialist Republic|Russian SFSR]], Soviet Union
| draft = 6th overall
| draft_year = 2008
| draft_team = [[Columbus Blue Jackets]]
| career_start = 2007
}}

'''Nikita Vasilyevich Filatov''' ({{lang|ru|Никита Васильевич Филатов}}; born May 25, 1990) is a Russian professional [[ice hockey]] [[winger (ice hockey)|left winger]] currently playing for [[Admiral Vladivostok]] of the [[Kontinental Hockey League]] (KHL). He has previously played for [[HC CSKA Moscow|CSKA Moscow]] and [[Salavat Yulaev Ufa]] of the KHL. Prior to 2012, Filatov played in North America for the [[Ottawa Senators]] and [[Columbus Blue Jackets]] of the [[National Hockey League]] (NHL), along with their respective affiliates in the [[American Hockey League]] (AHL).

At the [[2008 NHL Entry Draft]], Filatov was selected sixth overall by the Columbus Blue Jackets. Filatov was the top-ranked European skater by the [[NHL Central Scouting Bureau]]. Filatov played two seasons with the Blue Jackets organization. During the [[2009–10 NHL season|2009–10 season]], Filatov was unhappy with his situation in Columbus and was loaned to CSKA Moscow for the remainder of the season. At the [[2011 NHL Entry Draft]], the Blue Jackets then traded him to Ottawa in exchange for a third-round draft pick. In December 2011, the Senators loaned Filatov to CSKA Moscow for the balance of the [[2011–12 KHL season|2011–12 season]]. The following season, Filatov signed with Salavat Yulaev. The Senators chose not to tender Filatov a qualifying offer, making him a free agent.

Filatov has represented Russia in international hockey at two [[IIHF World U18 Championship|World U18 Championships]], winning gold and silver medals, and three [[IIHF  World U20 Championship|World Junior Championships]], where he has won two bronze medals. He was named to the Tournament All-Star Team at the [[2008 IIHF World U18 Championships|2008 World U18 Championships]] and the [[2009 World Junior Ice Hockey Championships|2009 World Junior Championships]].

==Playing career==

===Junior===
[[File:NikitaFilatovCloseup.JPG|thumb]]
Filatov played [[minor ice hockey|minor]] and [[junior ice hockey|junior hockey]] in the [[HC CSKA Moscow|CSKA Red Army hockey system]] from the age of 13. At the age of 15 during the 2005–06 season, he made his debut for CSKA-2—the club's junior team—where he continued to play during the 2006–07 season, and averaged more than three points per game.<ref name=russprosp>{{cite web|title=Russian Hockey NHL prospects Nikita Filatov Columbus Blue Jackets|url=http://www.russianprospects.com/public/profile.php?player_id=640|accessdate=2010-01-22|publisher=Russian Prospects}}</ref> In the same season, Filatov made his international debut for [[Russia men's national under-18 ice hockey team|Russia]] at the [[IIHF World U18 Championship|World Under 18 Championship]].<ref name=russprosp /><ref name=nhlprof>{{cite web|title=Nikita Filatov, Blue Jackets | url = http://bluejackets.nhl.com/club/player.htm?id=8474566&view=stats | accessdate = 2010-01-22 | publisher = [[Columbus Blue Jackets]]}}</ref>
 
During the 2007–08 season, Filatov made his professional [[Russian Superleague]] (RSL) debut with CSKA, seeing limited action in five games.<ref name=russprosp /> He spent the majority of the season playing at the junior level in Russia. With his CSKA junior team, Filatov played in 23 games, scoring 23 goals and providing 24 assists.<ref name=nhlprof /> Leading up to the [[2008 NHL Entry Draft]], the League's annual meeting at which the rights to amateur players are divided among teams, [[NHL Central Scouting Bureau|NHL's Central Scouting Bureau]], ranked Filatov as the top European skater in their mid-term and final rankings.<ref name=draftprofile>{{cite web|title=Filatov, Nikita|url=http://www.nhl.com/ice/draftprospectdetail.htm?dpid=211 | accessdate = 2010-01-22 | publisher = [[National Hockey League]]}}</ref> After the 2007–08 season, Filatov was subsequently selected sixth overall at the Draft by the [[Columbus Blue Jackets]].<ref name=nhlprof />

Filatov was also the first overall selection in the 2008 [[Canadian Hockey League]] [[Canadian Hockey League#CHL Import Draft|Import Draft]], selected by the [[Sudbury Wolves]] of the [[Ontario Hockey League]] (OHL). Sudbury General Manager [[Mike Foligno]] was comfortable with the risks of not knowing whether Filatov would play at junior or professional level when he came to North America. Blue Jackets General Manager [[Scott Howson]] would not guarantee Filatov a place on their team, saying, "We've already told Nikita that we'll see how things go in training camp and we'll decide what's best for him."<ref name=chldraft>{{cite web|title=Sudbury Wolves will select Nikita Filatov first overall in CHL import draft|url=http://www.thehockeynews.com/articles/16824-Sudbury-Wolves-will-select-Nikita-Filatov-first-overall-in-CHL-import-draft.html|accessdate=2010-01-22|date=2008-06-25|author=Campbell, Ken|work=[[The Hockey News]]}}</ref>

=== Professional ===
After being drafted by the Blue Jackets, Filatov signed a three-year contract with the club on July 10, 2008.<ref name=tsnprof>{{cite web |title=Nikita Filatov|url=http://www.tsn.ca/nhl/teams/players/bio/?id=6497|accessdate=2010-01-22 |publisher=[[The Sports Network]]}}</ref> His base salary for the contract was $875,000, with bonus clauses that could bring the value as high as $1.35&nbsp;million per season.<ref name=contractterms>{{cite web|title=Blue Jackets sign No. 1 pick|url=http://www.dispatch.com/live/content/sports/stories/2008/07/10/cbjno1.html|work=The Columbus Dispatch|author=Portzline, Aaron|date=2008-07-10|accessdate=2010-06-06}}</ref> His signing with Columbus created some controversy within the [[Kontinental Hockey League]] (KHL), however, as League President [[Alexander Medvedev]] claimed that the Blue Jackets owed CSKA Moscow compensation of at least $1.5&nbsp;million for signing Filatov. Medvedev claimed that although the term of the contract had expired, under Russian law it did not terminate until an indemnity amount had been negotiated.<ref name=khlcontract>{{cite web|title=K.H.L. Chief Says Columbus Owes CSKA ‘at Least $1.5 Million’ for Filatov [Updated]|url=http://slapshot.blogs.nytimes.com/2008/07/09/khl-chief-says-columbus-owes-cska-at-least-500000-for-filatov/|accessdate=2010-01-23|date=2008-07-09|author=Klein, Jeff K.|work=[[The New York Times]]}}</ref> The Blue Jackets and Filatov believed that giving his club 30 days notice was sufficient to terminate the contract.<ref name=dispatchcontract>{{cite web|title=Blue Jackets sign No. 1 pick|url=http://www.dispatch.com/live/content/sports/stories/2008/07/10/cbjno1.html?sid=101|author=Portzline, Aaron|work=[[The Columbus Dispatch]]|accessdate=2010-01-23|date=2008-07-10}}</ref> CSKA threatened to withhold Filatov's transfer card, thus impeding his ability to play in another league,<ref name=khlcontract /><ref name=dispatchcontract /> but Filatov, his lawyers and the Blue Jackets believed they had followed the necessary tenets of Russian law.<ref name=dispatchcontract /> Filatov's contract was one of six reviewed by the [[International Ice Hockey Federation]] (IIHF) in an attempt to mediate the disputes between the KHL and NHL. During this investigation, the players were unable to play international hockey sanctioned by the IIHF.<ref name=iihfcontract>{{cite web|title=Hockey's governing body probing KHL contracts|url=http://m.espn.go.com/nhl/story?storyId=3495585|agency=[[Associated Press]]|accessdate=2010-01-23|date=2008-08-18}}</ref> In September, the KHL dropped its opposition to Filatov's contract with the Blue Jackets, and he received his transfer.<ref name=hncontract>{{cite web|title=KHL makes concessions, but NHL not impressed|url=http://www.thehockeynews.com/articles/17776-KHL-makes-concessions-but-NHL-not-impressed.html|author=Campbell, Ken|accessdate=2010-01-23|date=2008-09-06|work=[[The Hockey News]]}}</ref>

Filatov did not make the team after attending training camp with the Blue Jackets, instead making his North American professional debut for the Blue Jackets' [[American Hockey League]] (AHL) affiliate, the [[Syracuse Crunch]].<ref name=tsnprof /> On October 15, 2008, however, Filatov was called up to the NHL,<ref name=tsnprof /> playing in his first game and scoring his first goal with the Blue Jackets on October 17 against the [[Nashville Predators]].<ref name=firstgame>{{cite web|title=Blue Jackets recall Nikita Filatov from Syracuse|url=http://www.nhl.com/ice/news.htm?id=402600|accessdate=2010-01-22|date=2009-01-07|publisher=[[National Hockey League]]}}</ref> For the rest of the [[2008–09 NHL season|2008–09 season]], Filatov split time between the AHL and the NHL. He played eight games with the Blue Jackets, finishing the season with four goals.<ref name=nhlprof /> He became the first Blue Jackets rookie to record a [[Hat-trick#Hockey|hat-trick]] in the January 10, 2009, game against the [[Minnesota Wild]].<ref name=hattrick>{{cite web|title=Columbus rookie Nikita Filatov nets hat trick|url=https://sports.yahoo.com/nhl/recap?gid=2009011029|accessdate=2010-01-22|date=2009-01-10|publisher=Yahoo!}}</ref> At the end of the season, Filatov had played 39 games with the Crunch in the AHL, scoring 16 goals and 16 assists.<ref name=nhlprof /> For his performances, he was named as a starter for the PlanetUSA team in the 2009 [[AHL All-Star Game]].<ref name=ahlasg>{{cite web|title=Nikita Filatov Named to Starting Line-Up for AHL All-Star Game|url=http://bluejackets.nhl.com/club/news.htm?id=478906|accessdate=2010-01-22|date=2009-01-07|publisher=[[Columbus Blue Jackets]]}}</ref>

At the start of the [[2009–10 NHL season|2009–10 season]], Filatov made the Blue Jackets roster after training camp. Although healthy, he did not play in six of the team's first 18 games, a decision made by Columbus Head Coach [[Ken Hitchcock]].<ref name=cdrussia>{{cite web|title=Filatov leaving Blue Jackets|url=http://www.dispatch.com/live/content/sports/stories/2009/11/17/1117cbjup.html|author=Portzline, Aaron|accessdate=2010-01-22|date=2009-11-17|work=The Columbus Dispatch}}</ref> Filatov was unhappy with his playing time and role on the team under Hitchcock and requested to be transferred back to his Russian club team.<ref name=cdrussia /> Hitchcock and the Blue Jackets coaching staff tried to improve Filatov's attention to defensive aspects of the game.<ref name=sidefense>{{cite web|title=Jackets, Stars offer lessons in young asset management; more|url=http://sportsillustrated.cnn.com/2009/writers/allan_muir/11/13/filatov.brunnstrom.howard/index.html|publisher=CNNSI.com|date=2009-11-13|accessdate=2010-07-15}}</ref> <!-- pov? This emphasis on defensive play did not suit Filatov, and he did not receive much playing time. --> The Blue Jackets management agreed to Filatov's request to return to Russia for the remainder of the season.<ref name=cdrussia /> This arrangement resulted from direct dealings between the Blue Jackets and CSKA Moscow, where Filatov's salary was paid by the Russian club, and the Blue Jackets retained his rights.<ref name=gmfil>{{cite web|title=Filatov finds a comfort zone |url=http://www.theglobeandmail.com/sports/hockey/filatov-finds-a-comfort-zone/article1414659/|accessdate=2010-01-22|date=2009-12-30|author=Sekeres, Matthew|work=The Globe and Mail}}</ref> Shortly after his return to Russia, Filatov set a KHL record by scoring the game-winning goal in three consecutive games.<ref name=khlgamewinners>{{cite web|title=Filatov keeps on winning|url=http://en.khl.ru/news/2009/11/25/23731.html|publisher=[[Kontinental Hockey League]]|date=2009-11-25|accessdate=2010-08-17}}</ref> He was named the League's best newcomer (defined by the KHL as "a player born in 1987 or later, who has played no more than 20 top-level matches in previous national tournaments") for November,<ref name=newcomer>{{cite web|title=November's finest|url=http://en.khl.ru/news/2009/12/2/23740.html|publisher=[[Kontinental Hockey League]]|date=2009-12-02|accessdate=2010-08-17}}</ref> and the League's best rookie for the 11th week of the season.<ref name=rotw>{{cite web|title=Players of the week|url=http://en.khl.ru/news/2009/11/30/23737.html|publisher=[[Kontinental Hockey League]]|date=2009-11-30|accessdate=2010-08-17}}</ref> In his [[2009–10 KHL season|shortened season]] in Russia, Filatov played 26 games, scoring nine goals and adding 13 assists.<ref name=hockeydb>{{cite web|title=Nikita Filatov hockey statistics|url=http://www.hockeydb.com/ihdb/stats/pdisplay.php?pid=108670|publisher=HockeyDB.com|accessdate=2014-01-01}}</ref> Initial statements by both sides indicated the player would return to Columbus after the 2009–10 season, but since returning to Russia, Filatov has been less clear about his intentions for the [[2010–11 NHL season|2010–11 season]], stating, "I hope I'll be back next year, but right now, it's really hard to say because it will again be a tough decision."<ref name=gmfil /> Howson did not comment on Filatov's stance, except to say that he expects Filatov to be at the team's training camp prior to the 2010–11 season.<ref name=11tc>{{cite web|title=Jackets notebook: Russell's game on upswing|url=http://www.dispatch.com/content/stories/sports/2010/01/08/jackets_notes_1-8.ART_ART_01-08-10_C4_T8G80RL.html |accessdate=2010-01-24 |date=2010-01-08 |last=Reed |first=Tom |work=The Columbus Dispatch}}</ref> Subsequently, Filatov stated after the 2009–10 season that he intended to return to Columbus for training camp.<ref name=comingback>{{cite web|title=Filatov plans to return for Jackets training camp|url=http://www.dispatch.com/live/content/sports/stories/2010/04/24/filatov-plans-to-return-for-training-camp.html?sid=101|author=Portzline, Aaron|work=The Columbus Dispatch|date=2010-04-24|accessdate=2010-05-08}}</ref>

During the off-season, the Blue Jackets sent Development Coach [[Tyler Wright]] to Russia to train with Filatov to assess his readiness for the upcoming season, and to communicate the team's desire to work with him.<ref name=wrightoffseason>{{cite web|title=Jackets send Wright to Russia to meet with Filatov|url=http://www.bluejacketsxtra.com/live/content/sports/stories/2010/07/07/jackets-send-wright-to-russia-to-meet-with-filatov.html?sid=101|work=The Columbus Dispatch|author=Portzline, Aaron|date=2010-07-07|accessdate=2010-07-07|archiveurl=https://web.archive.org/web/20100712125718/http://www.bluejacketsxtra.com/live/content/sports/stories/2010/07/07/jackets-send-wright-to-russia-to-meet-with-filatov.html?sid=101 <!--Added by H3llBot-->|archivedate=2010-07-12}}</ref> Blue Jackets Head Coach [[Scott Arniel]] was pleased to see Filatov arrive in Columbus six weeks ahead of the teams' 2010–11 season, allowing him a chance to mend relationships with his teammates who may have been annoyed by Filatov's departure.<ref name=earlyarrival>{{cite web|title=Blue Jackets notebook: Filatov returns after shaky departure|url=http://www.dispatch.com/live/content/sports/stories/2010/08/04/filatov-returns-after-shaky-departure.html?sid=101|work=The Columbus Dispatcher|author=Portzline, Aaron|date=2010-08-04|accessdate=2010-08-11}}</ref> Howson said the team never doubted Filatov's skill: "Nikita has the skill and the ability to play in a top-six role."<ref name=wrightoffseason /> Arniel was optimistic about Filatov's return to the team, offering him a clean slate and a chance to earn a spot on one of the team's top two lines.<ref name=earlyarrival /> After speaking with his friend [[Sergei Shirokov]] (who played for Arniel on the [[Manitoba Moose]]), Filatov was optimistic about working with Columbus' new head coach.<ref name=1011interview>{{cite web|title=Blue Jackets: Winger moves past 'bad season'|url=http://www.dispatch.com/content/stories/sports/2010/08/05/winger-moves-past-bad-season.html|work=The Columbus Dispatch|author=Portzline, Aaron|date=2010-08-05|accessdate=2010-08-11}}</ref> Filatov started the season with the Blue Jackets at the NHL level and recorded seven assists in 23 games.<ref name=hockeydb /> In December 2010, Filatov was demoted to the AHL and spent the remainder of the season with the [[Springfield Falcons]], Columbus' new AHL affiliate.<ref name=demote>{{cite web|title=Columbus Blue Jackets Assign Forward Nikita Filatov to AHL's Springfield Falcons, Recall Forward Tom Sestito|url=http://bluejackets.nhl.com/club/news.htm?id=545992 |date=2010-12-10 |accessdate=2011-05-10 |publisher=[[Columbus Blue Jackets]]}}</ref> With the Falcons, he played in 36 games, scoring nine goals and adding 11 assists.<ref name=hockeydb />

At the [[2011 NHL Entry Draft]], Columbus severed their ties with Filatov, trading him to the [[Ottawa Senators]] in exchange for a third-round pick.<ref name=senstrade>{{cite web|title=Senators acquire Filatov from Blue Jackets|url=http://www.tsn.ca/nhl/story/?id=369973|publisher=[[The Sports Network]]|date=2011-06-25|accessdate=2011-06-25}}</ref> Filatov left his family vacationing in the [[Dominican Republic]] and joined the Senators for their development camp at [[Scotiabank Place]] in [[Ottawa]].<ref>{{cite web|title=Filatov: 'It's a new time now and a new team'|url=http://senators.nhl.com/club/news.htm?id=567682|publisher=[[Ottawa Senators]]|date=June 29, 2011 |accessdate=July 4, 2011}}</ref> He made the Senators out of training camp, but was a healthy scratch several times, splitting his time with Ottawa and the [[Binghamton Senators]] of the AHL. In November 2011, the ''[[Ottawa Citizen]]'''s Allen Panzeri reported from sources in Columbus that Filatov had refused to play the style the Blue Jackets asked of him, saying to his coach, "Filly [Filatov] don't do rebounds."<ref>{{cite web |title=Filly don’t do rebounds |publisher=senatorsextra.com |url=http://www.senatorsextra.com/main/filly-dont-do-rebounds |first=Allen |last=Panzeri |date=November 17, 2011}}</ref> In December, the Senators offered Filatov the choice to play the rest of the season with CSKA Moscow of the KHL and Filatov agreed.<ref name="puck-daddy-2011">{{cite web |publisher=Yahoo! Sports |url=https://sports.yahoo.com/nhl/blog/puck_daddy/post/Chatting-with-Nikita-Filatov-about-leaving-for-K?urn=nhl-wp19772&utm_source=twitterfeed&utm_medium=twitter |title=Chatting with Nikita Filatov about leaving for KHL, struggles in Ottawa and ‘stupid question’ about NHL future |first=Dmitry |last=Chesnokov |date=December 13, 2011}}</ref> Senators General Manager [[Bryan Murray (ice hockey)|Bryan Murray]] suggested it was better for his development as Murray felt that Filatov needed to work on becoming stronger and compete harder to make it in the NHL. Head Coach [[Paul MacLean (ice hockey)|Paul MacLean]] felt Filatov's struggles in the NHL were surprising, considering he had 12 points in 15 games in the AHL. Filatov himself stated that he had to get better.<ref name="sovsport">{{cite news |work=Sovsport.ru |language=Russian |url=http://www.sovsport.ru/gazeta/article-item/498348 |title=Door in the NHL not close ... Today Nikita Filatov sign a contract with CSKA and again will play in Russia |date=December 13, 2011}}</ref> MacLean had hoped that Filatov would have stayed and worked on his game in Binghamton, but Murray explained that it was more lucrative for Filatov to play in the KHL than at Binghamton, and did not block the transfer.<ref name="sovsport"/> Filatov did not immediately agree to terms with CSKA. For a few days it appeared that Filatov would not sign a contract with CSKA, so the Senators assigned him to Binghamton and threatened to suspend him in order to remove him from the Senators' salary cap.<ref name=suspension>{{cite web| title = Senators Assign Filatov to AHL; Suspension Expected |url=http://www.tsn.ca/nhl/story/?id=382975 |publisher = TSN.ca | accessdate = 2014-01-01 | date = 2011-12-17}}</ref> On December 18, 2011, Filatov agreed to a contract with CSKA Moscow.<ref name=CSKA>{{cite web|url = http://slam.canoe.ca/Slam/Hockey/NHL/Ottawa/2011/12/18/19140436.html |title = 'Terrific' Turris a great fit for Sens | publisher=SLAM! Sports | date = 2011-12-18 | author = Garrioch, Bruce | accessdate = 2014-01-01 }}</ref>

Despite Filatov leaving for the KHL, the Senators still had hopes to develop Filatov. Filatov's contract was ready to expire in June 2012, and the Senators planned to make a qualifying contract offer to Filatov to retain his NHL rights. However, in May 2012, Filatov chose to stay in the KHL and agreed to a one-year contract with [[Salavat Yulaev Ufa]].<ref name=salavatcontract>{{cite web|title=Report: Filatov agrees to one-year deal with KHL's Salavat | url=http://www.tsn.ca/nhl/story/?id=395939 | publisher = TSN.ca | accessdate = 2014-01-01 |date = 2012-05-14}}</ref>  After he signed with Salavat Yulaev, the Senators chose to not make the offer, making him an unrestricted free agent.<ref>{{cite news |work=NBC Sport |url=http://prohockeytalk.nbcsports.com/2012/06/14/shocker-ottawa-wont-make-qualifying-offer-to-filatov/ |title=Shocker: Ottawa won’t make qualifying offer to Filatov |first=Mike |last=Halford |date=June 14, 2012}}</ref> After three seasons in the NHL, Filatov left with only six goals and eight assists in 53 NHL games. Stephen Whyno of ''[[The Globe and Mail]]'' identified him as one of the top five biggest "[[draft bust]]s" in recent NHL history.<ref name="gm-draft-bust">{{cite news |newspaper=The Globe and Mail |title=The top bargains and busts from the past 20 years of the NHL draft |url=http://www.theglobeandmail.com/sports/hockey/the-top-bargains-and-busts-from-the-past-20-years-of-the-nhl-draft/article12865098/ |last=Whyno |first=Stephen |date=2013-06-27 |accessdate=2014-02-19 |agency=The Canadian Press}}</ref> ''Hockey's Future'' also rated Filatov a bust.<ref>{{cite web |publisher=Hockey's Future |title=Columbus Blue Jackets added NHL players at 2008 Draft despite whiff at sixth overall |first=Richard |last=Greco |date=February 11, 2013  |url=http://www.hockeysfuture.com/articles/85031/columbus-blue-jackets-added-nhl-players-at-2008-draft-despite-whiff-at-sixth-overall/#more-85031}}</ref>

In the [[2012–13 KHL season|2012–13 season]], Filatov played in 47 games with Salavat Yulaev, scoring ten goals and adding 11 assists. He remained with Salavat for the [[2013–14 KHL season|2013–14 season]].<ref name=hockeydb/>

== International play ==
{{MedalTableTop|name = |NikitaFilatov POG.JPG|230px|alt=A young man with dark hair wearing a white jersey holds a small trophy in his right hand.|Filatov accepts his player of the game award during the 2010 World Junior Hockey Championship.}}
{{MedalSport | [[Ice hockey]]}}
{{MedalCountry | {{ih|RUS}} }}
{{MedalCompetition|[[IIHF  World U20 Championship|World Junior Championship]]}}
{{MedalBronze|[[2009 World Junior Ice Hockey Championships|2009 Canada]]|}}
{{MedalBronze|[[2008 World Junior Ice Hockey Championships|2008 Czech Republic]]|}}
{{MedalCompetition|[[IIHF World U18 Championship|IIHF U18 Championship]]}}
{{MedalSilver|[[2008 IIHF World U18 Championships|2008 Russia]]|}}
{{MedalGold|[[2007 IIHF World U18 Championships|2007 Finland]]|}}
{{MedalBottom}}

Filatov has played extensively for Russia's national teams in under-18 and under-20 tournaments. His first IIHF competition for [[Russia men's national under-18 ice hockey team|Russia]] was the [[2007 IIHF World U18 Championships|2007 U18 Championship]] held in the Finnish cities of [[Rauma, Finland|Rauma]] and [[Tampere]]. Russia won a gold medal in that tournament, and as an underaged player, Filatov contributed four goals and five assists in seven games.<ref name=nhlprof /> He led the Russian team in total points and was second to [[Alexei Cherepanov]] in goals scored.<ref name=russprosp /> After this tournament, Russia's coaches named Filatov as one of the team's three best players.<ref name=073best>{{cite web|title=Three Best Players Of Each Team Selected By Coaches|url=http://www.iihf.com/Hydra/Tournaments_07/output/w18/hydra.iihf.com/data/iihf/output/xml/109/IHM109000_85J_1_0.pdf|format=pdf|date=2007-04-22|accessdate=2010-01-24|publisher=[[International Ice Hockey Federation]]}}</ref> Filatov also played at the [[2008 IIHF World U18 Championships|2008 U18 Championship]] held in his native Russia, in [[Kazan]]. Filatov captained the team to a silver medal, scoring three goals and adding six assists. He was named to the Tournament All-Star Team.<ref name=nhlprof />

At the [[2008 World Junior Ice Hockey Championships|2008 World Junior Championship]] in [[Pardubice]] and [[Liberec]], Czech Republic, Filatov made his debut with [[Russia men's national junior ice hockey team|Russia's under-20 junior squad]]. At the tournament, he scored four goals and added five assists,<ref name=nhlprof /> leading the Russian squad in total points and placing second to [[Viktor Tikhonov (ice hockey, born 1988)|Viktor Tikhonov]] in goals scored.<ref name=russprosp /> The Russian team captured bronze at the tournament after defeating the [[United States men's national junior ice hockey team|United States]] 4–2. Filatov scored two goals in the bronze medal game,<ref name=2008bronze>{{cite web|title=Russia earns U20 bronze|url=http://www.iihf.com/channels/iihf-world-u20-championship/news-singleview-world-u20-ia-channel/article/russia-earns-u20-bronze.html?tx_ttnews%5BbackPid%5D=706&cHash=69f534da80|publisher=[[International Ice Hockey Federation]]|date=2008-01-05|accessdate=2010-07-06}}</ref> and was named Russia's best player of the game by the IIHF.<ref name=08pog>{{cite web|title=Best Players Per Game|url=http://stats.iihf.com/Hydra/142/IHM142000_85K_13_0.pdf|format=pdf|accessdate=2010-01-24|date=2008-01-06|publisher=[[International Ice Hockey Federation]]}}</ref>

The AHL's Syracuse Crunch released Filatov to participate in the [[2009 World Junior Ice Hockey Championships|2009 World Junior Championship]] held in Ottawa.<ref name=09wjrelease>{{cite web|title=Blue Jackets will release Nikita Filatov to allow him to play for Russia at world juniors|url=http://www.thehockeynews.com/articles/21009-Blue-Jackets-will-release-Nikita-Filatov-to-allow-him-to-play-for-Russia-at-world-juniors.html|accessdate=2010-01-22|date=2008-12-10|author=Campbell, Ken|work=[[The Hockey News]]}}</ref> Filatov served as Russia's captain for the tournament.<ref name=wjcaptain>{{cite web|title=World Junior fever leaves Canadians delirious|url=http://www.nhl.com/ice/news.htm?id=402436|accessdate=2010-01-22|date=2009-01-06|author=Kimelman, Adam|publisher=[[National Hockey League]]}}</ref> In seven games at the tournament, he scored eight goals and added three assists, which tied him for fourth in tournament scoring.<ref name=nhlprof /> The Russian team again captured the bronze medal, this time by defeating [[Slovakia men's national junior ice hockey team|Slovakia]] 5–2.<ref name=09results>{{cite web|title=2009 IIHF World Junior Championship – Schedule/Results/Rosters|url=http://www.hockeycanada.ca/index.php/ci_id/23176/la_id/1.htm|accessdate=2010-01-22|publisher=[[Hockey Canada]]}}</ref> Filatov was named best player of the game for a preliminary round game against [[Finland men's national junior ice hockey team|Finland]] and for the bronze medal game against Slovakia,<ref name=09pog>{{cite web|title=Best Players Per Game|url=http://stats.iihf.com/Hydra/172/IHM172000_85K_11_0.pdf|accessdate=2010-01-24|format=pdf|date=2009-01-06|publisher=[[International Ice Hockey Federation]]}}</ref> and he was named to the Tournament All-Star Team.<ref name=09asg>{{cite web|title=All-Star Teams – IIHF World Junior Championship|url=http://www.hockeycanada.ca/index.php/ci_id/3572/la_id/1.htm|accessdate=2010-01-22|publisher=[[Hockey Canada]]}}</ref>

After returning to Russia early in the 2009–10 season, Filatov had the opportunity to compete in a third World Junior Championship at the [[2010 World Junior Ice Hockey Championships|2010 tournament]] held in [[Saskatoon]] and [[Regina, Saskatchewan|Regina]]. As in 2009, he served as Russia's team captain.<ref name=10captain>{{cite web|title=Third time's the charm? Russia's Filatov eyes first world junior gold|url=http://www.canada.com/sports/Third+time+charm+Russia+Filatov+eyes+first+world+junior+gold/2384201/story.html|accessdate=2010-01-22|date=2009-12-28|author=Harder, Greg|publisher=Can West News Service}}</ref> During preliminary round play, Filatov was named best player for Russia in their game against Finland.<ref name=pog>{{cite web|title=Best Players Per Game|url=http://stats.iihf.com/Hydra/205/IHM205000_85K_11_0.pdf|format=pdf|accessdate=2010-01-24|date=2010-01-06|publisher=[[International Ice Hockey Federation]]}}</ref> The tournament, however, was a disappointment for the Russians after they lost to [[Switzerland men's national junior ice hockey team|Switzerland]] in the quarterfinals.<ref name=qfloss>{{cite web|title=Switzerland Stuns Russia World Juniors|url=http://aol.sportingnews.com/nhl/story/2010-01-03/switzerland-stuns-russia-world-juniors|accessdate=2013-02-25|date=2010-01-03|agency=The Canadian Press}}</ref> Prior to the fifth place game against the [[Czech Republic men's national junior ice hockey team|Czech Republic]], Filatov was stripped of his captaincy and replaced by teammate [[Kirill Petrov]] after criticizing the team personnel during a [[media scrum]].<ref name=hncapt>{{cite web|title=THN at the World Junior Championship: Focused U.S. spoils Canada's party|url=http://www.thehockeynews.com/articles/30550-THN-at-the-World-Junior-Championship-Focused-US-spoils-Canadas-party.html|accessdate=2010-01-22|date=2010-01-06|author=Kennedy, Ryan|work=[[The Hockey News]]}}</ref> After participating in three World Junior Ice Hockey Championships, Filatov is tied with [[Evgeny Kuznetsov]] as Russia's all-time leading scorer at the event—both forwards finished their junior careers with 26 points.<ref name=u2-0russia>{{cite web|url=http://www.quanthockey.com/wjc-u20/en/teams/team-russia-players-career-wjc-u20-stats.html | title =  Team Russia all-time scoring leaders in World Junior Ice Hockey Championships | publisher = QuantHockey.com |accessdate = 2014-01-12}}</ref>

==Playing style==
Scouting reports on Filatov were mixed in advance of the 2008 NHL Entry Draft. [[Sergei Nemchinov]], head coach of Russia's national junior team, said of Filatov, "He definitely has an NHL upside because he can score, is a well-rounded player and is responsible in the defensive zone."<ref name=draftprofile /> Independent scouting service Red Line Report at one point declared Filatov "the next best thing to [[Steven Stamkos]]" (who was eventually selected first overall in the 2008 Draft).<ref name=stamkos>{{cite web|title=Race for top pick heating up thanks to Filatov|url=http://www.usatoday.com/sports/hockey/columnist/woodlief/2008-02-22-redline_N.htm|work=USA Today|author=Woodlief, Kyle|date=2008-02-22|accessdate=2010-08-15}}</ref> McKeen's Hockey scouts described him as a cross between [[Ilya Kovalchuk]] and [[Maxim Afinogenov]].<ref name=islesdraft>{{cite web|title=Nikita Filatov|url=http://islanders.nhl.com/club/news.htm?id=465637|publisher=[[New York Islanders]]|accessdate=2010-07-15}}</ref> His strengths included his skating and vision, as well as the inclination to play at high intensity.<ref name=islesdraft /> The NHL's Director of European Scouting, Goran Stubb, assessed Filatov's NHL readiness as, "Nikita is a leader, has a great attitude, an excellent work ethic and tons of talent."<ref name=euroscout>{{cite web|title=Filatov up next in NHL Russian Revolution|url=http://bluejackets.nhl.com/club/news.htm?id=479524|publisher=[[Columbus Blue Jackets]]|author=Morreale, Mike|date=2008-06-20|accessdate=2010-08-27}}</ref> Other scouts were not as impressed with his defensive game, however, preferring to focus on his offensive abilities.<ref name=sidefense /> Off the ice, Filatov does not train in a traditional gym or weight room, preferring to run outside in sand and lift objects such as trees and boulders.<ref name=geared>{{cite web|title=Filatov geared up for return|url=http://www.dispatch.com/live/content/sports/stories/2010/07/15/filatov-geared-up-for-return.html?sid=101|work=The Columbus Dispatch|author=Portzline, Aaron|date=2010-07-15|accessdate=2010-08-11}}</ref>

== Personal life ==
Filatov was born in Moscow, Russia, to parents Slava and Yelena.<ref name=family>{{cite web|title=A Nest for Nikita|url=http://www.syracuse.com/crunch/index.ssf?/base/sports-0/1228470962199430.xml&coll=1|accessdate=2010-01-23|date=2008-12-05|author=Kramer, Lindsay|publisher=Syracuse Online LLC}}</ref> He speaks fluent English due in large part to his mother, a teacher who gave him lessons at home.<ref name=draftprofile /> When he started playing professional hockey in North America for Syracuse, his mother stayed for several weeks to help him get acclimated to his new surroundings.<ref name=family />

== Career statistics ==

=== Regular season and playoffs ===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:60em"
|- bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Playoffs]]
|- bgcolor="#e0e0e0"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|- 
| 2005–06
| CSKA-2 Moscow
| RUS-3
| 6 || 6 || 7 || 13 || 4
| — || — || — || — || — 
|- bgcolor="#f0f0f0"
| 2006–07
| CSKA-2 Moscow
| RUS-3
| 29 || 13 || 9 || 22 || 32
| — || — || — || — || — 
|-
| 2007–08 
| CSKA-2 Moscow
| RUS-3
| 23 || 24 || 23 || 47 || 62
| 11 || 14 || 9 || 23 || 28
|- bgcolor="#f0f0f0"
| 2007–08 
| [[HC CSKA Moscow|CSKA Moscow]] 
| [[Russian Superleague|RSL]]
| 5 || 0 || 0 || 0 || 0
| — || — || — || — || —
|-
| [[2008–09 NHL season|2008–09]]
| [[Columbus Blue Jackets]]
| [[National Hockey League|NHL]]
| 8 || 4 || 0 || 4 || 0
| — || — || — || — || —
|- bgcolor="#f0f0f0"
| [[2008–09 AHL season|2008–09]] 
| [[Syracuse Crunch]] 
| [[American Hockey League|AHL]]
| 39 || 16 || 16 || 32 || 24
| — || — || — || — || —
|-
| [[2009–10 NHL season|2009–10]]
| Columbus Blue Jackets
| NHL
| 13 || 2 || 0 || 2 || 8
| — || — || — || — || —
|- bgcolor="#f0f0f0"
| [[2009–10 KHL season|2009–10]]
| CSKA Moscow
| [[KHL]]
| 26 || 9 || 13 || 22 || 16
| 3 || 0 || 1 || 1 || 4
|- 
| [[2010–11 AHL season|2010–11]]
| [[Springfield Falcons]]
| AHL
| 36 || 9 || 11 || 20 || 20
| — || — || — || — || —
|- bgcolor="#f0f0f0"
| [[2010–11 NHL season|2010–11]]
| Columbus Blue Jackets
| NHL
| 23 || 0 || 7 || 7 || 8
| — || — || — || — || —
|- 
| [[2011–12 NHL season|2011–12]]
| [[Ottawa Senators]]
| NHL
| 9 || 0 || 1 || 1 || 4
| — || — || — || — || —
|- bgcolor="#f0f0f0"
| [[2011–12 AHL season|2011–12]]
| [[Binghamton Senators]]
| AHL
| 15 || 7 || 5 || 12 || 12
| — || — || — || — || —
|- 
| [[2011–12 KHL season|2011–12]]
| CSKA Moscow
| KHL
| 18 || 4 || 4 || 8 || 12
| 5 || 0 || 1 || 1 || 4
|- bgcolor="#f0f0f0"
| 2011–12
| Krasnaya Armiya
| [[Minor Hockey League|MHL]]
| 1 || 1 || 2 || 3 || 2
| 2 || 1 || 0 || 1 || 4
|- 
| [[2012–13 KHL season|2012–13]]
| [[Salavat Yulaev Ufa]]
| KHL
| 47 || 10 || 11 || 21 || 24
| 13 || 3 || 3 || 6 || 6
|- bgcolor="#f0f0f0"
| [[2013–14 KHL season|2013–14]]
| Salavat Yulaev Ufa
| KHL
| 35 || 13 || 7 || 20 || 18
| 5 || 1 || 0 || 1 || 0
|- bgcolor="#e0e0e0"
! colspan="3" | KHL totals
! 126 !! 36 !! 35 !! 71 !! 70
! 26 !! 4 !! 5 !! 9 !! 14
|- bgcolor="#e0e0e0"
! colspan="3" | NHL totals
! 53 !! 6 !! 8 !! 14 !! 20
! — !! — !! — !! — !! —
|}
<!-- Do not add stats until end of season -->

=== International ===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:40em"
|- ALIGN="center" bgcolor="#e0e0e0"
! Year
! Team
! Event
! Result
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! GP
! G
! A
! Pts
! PIM
|-
| [[2007 IIHF World U18 Championships|2007]]
| [[Russia men's national under-18 ice hockey team|Russia]]
| [[IIHF World U18 Championship|U18]]
| {{goca}}
| 7
| 4
| 5
| 9
| 6
|- bgcolor="#f0f0f0"
| [[2008 IIHF World U18 Championships|2008]]
| Russia
| U18
| {{sica}}
| 6
| 3
| 6
| 9
| 29
|-
| [[2008 World Junior Ice Hockey Championships|2008]]
| [[Russia men's national junior ice hockey team|Russia]]
| [[IIHF World U20 Championship|WJC]]
| {{brca}}
| 7
| 4
| 5
| 9
| 10
|- bgcolor="#f0f0f0"
| [[2009 World Junior Ice Hockey Championships|2009]]
| Russia
| WJC
| {{brca}}
| 7
| 8
| 3
| 11
| 6
|-
| [[2010 World Junior Ice Hockey Championships|2010]]
| Russia
| WJC
| 6th
| 6
| 1
| 5
| 6
| 6
|- bgcolor="#e0e0e0"
! colspan="4" | Junior totals
! 33
! 20
! 24
! 44
! 57
|}

''Statistics Sources''<ref name=russprosp /><ref name=hockeydb />

== Awards ==

=== International ===
{| class="wikitable" border="1"
|+
! Award !! Year
|-
| World U18 Championships Top Three Player for Team Russia || 2007<ref name=073best />
|-
| World U18 Championships Tournament All Star Team || 2008<ref name=nhlprof />
|-
| World Junior Championships Player of the Game || 2008 vs. United States<ref name=08pog /><br>2009 vs. Finland<ref name=09pog /><br>2009 vs. Slovakia<ref name=09pog /><br>2010 vs. Finland<ref name=pog />
|-
| World Junior Championships Tournament All Star Team || 2009<ref name=nhlprof />
|}

=== Professional ===
{| class="wikitable" border="1"
|+
! Award !! Year
|-
| KHL Rookie of the Week || Week of November 22, 2009<ref name=rotw />
|-
| KHL Best Newcomer of the Month || November 2009<ref name=newcomer />
|}

== References ==
{{reflist|colwidth=30em}}

== External links ==
{{commons category}}
* {{Eliteprospects}}
* {{eurohockey|114249}}
* {{hockeydb|108670}}
* {{legendsofhockey|22676}}
* {{nhlprofile|8474566}}
* [http://www.russianprospects.com/public/profile.php?player_id=640 Russian Prospects Profile]

{{s-start}}
{{S-ach}}
{{succession box | before = [[Jakub Voráček|Jakub Voracek]] | title = [[List of Columbus Blue Jackets draft picks|Columbus Blue Jackets first round draft pick]] | years = [[2008 NHL Entry Draft|2008]] | after = [[John Moore (ice hockey)|John Moore]]}}
{{s-end}}

{{featured article}}

{{DEFAULTSORT:Filatov, Nikita}}
[[Category:1990 births]]
[[Category:Living people]]
[[Category:Columbus Blue Jackets draft picks]]
[[Category:Columbus Blue Jackets players]]
[[Category:HC CSKA Moscow players]]
[[Category:Sportspeople from Moscow]]
[[Category:National Hockey League first round draft picks]]
[[Category:Ottawa Senators players]]
[[Category:Russian ice hockey left wingers]]
[[Category:Salavat Yulaev Ufa players]]
[[Category:Springfield Falcons players]]
[[Category:Syracuse Crunch players]]