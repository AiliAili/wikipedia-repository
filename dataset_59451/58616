{{Use dmy dates|date=June 2013}}
{{italictitle}}
{{Infobox journal
| title = Journal of Theoretical and Computational Chemistry
| cover = [[File:JTCCcover.jpg|180px]]
| discipline = [[Chemistry]]
| abbreviation = J. Theor. Comput. Chem.
| editor = [http://www.math.msu.edu/~wei Guowei Wei]
| publisher = [[World Scientific]]
| country = Singapore
| history = 2002–present
| impact = 0.638
| impact-year = 2014
| website = http://www.worldscinet.com/jtcc/jtcc.shtml
| ISSN = 0219-6336
| eISSN = 1793-6888
}}
{{distinguish|Computational and Theoretical Chemistry}}
The '''''Journal of Theoretical and Computational Chemistry''''' (JTCC) was founded in 2002 and is published by [[World Scientific]]. It is an interdisciplinary journal covering developments in theoretical and [[computational chemistry]], as well as their applications to other scientific fields, such as physics, biology and material sciences. Articles are broadly categorized into quantum chemistry, chemical dynamics, statistical mechanics, kinetic theory, biochemistry, chemical biology, electrochemistry, and nanotechnology, ranging from fundamental theory, theoretical methods and computational algorithms to numerical applications, such as in chemical, biological and nano–bio systems.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Science Citation Index]] Expanded (Title: Journal of Theoretical & Computational Chemistry)
* ISI Alerting Services
* Chemistry Citation Index
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Inspec]]

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.638, ranking it 127th out of 157 journals in the category "Chemistry Multidisciplinary".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry Multidisciplinary |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.worldscinet.com/jtcc/jtcc.shtml Journal Website]

[[Category:Chemistry journals]]
[[Category:Publications established in 2002]]
[[Category:English-language journals]]
[[Category:2002 establishments in Singapore]]
[[Category:World Scientific academic journals]]