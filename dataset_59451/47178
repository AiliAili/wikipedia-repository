[[Image:Bi2212 Unit Cell.png|thumb|The crystal structure of bismuth strontium calcium copper oxide, a [[high-temperature superconductor]] and complex oxide.]]

A '''complex oxide''' is a chemical compound that contains oxygen and at least two other elements (or oxygen and just one other element that's in at least two [[oxidation states]]).<ref>{{cite book|last1=Ishihara|first1=Tatsumi|title=Perovskite Oxide for Solid Oxide Fuel Cells|date=2009|publisher=Springer US|isbn=978-0-387-77708-5|page=1|edition=1|url=http://dx.doi.org/10.1007/978-0-387-77708-5}}</ref> Complex oxide materials are notable for their wide range of magnetic and electronic properties, such as [[ferromagnetism]], [[ferroelectricity]], and [[high-temperature superconductivity]]. These properties often come from their [[Strongly correlated material|strongly correlated]] electrons in d or f [[atomic orbital|orbitals]].

== Natural occurrence ==

Many [[mineral]]s found in the ground are complex oxides. Commonly studied mineral crystal families include [[spinel group|spinels]] and [[perovskite (structure)|perovskites]].

== Applications ==

Complex oxide materials are used in a variety of commercial applications.

=== Magnets ===

[[Image:Cable end.JPG|thumb|right|A [[ferrite bead]] near the end of a [[Mini USB]] cable helps suppress high-frequency noise.]]

[[Magnet]]s made of the complex oxide [[ferrite (magnet)|ferrite]] are commonly used in [[transformer]] [[Magnetic core|cores]] and in [[inductor]]s.<ref>{{cite book|last1=Goldman|first1=Alex|title=Modern Ferrite Technology|date=2006|publisher=Springer US|isbn=978-0-387-28151-3|pages=217–226|edition=2nd|url=http://dx.doi.org/10.1007/978-0-387-29413-1_8}}</ref> Ferrites are ideal for these applications because they are magnetic, [[insulator (electricity)|electrically insulating]], and inexpensive.

=== Transducers and actuators ===

[[Piezoelectric]] [[transducers]] and [[actuators]] are often made of the complex oxide PZT ([[lead zirconate titanate]]).<ref>{{cite web|title=What is "PZT"?|url=https://www.americanpiezo.com/piezo-theory/pzt.html|website=American Piezo|publisher=APC International, Ltd.|accessdate=19 June 2015}}</ref> These transducers are used in applications such [[ultrasound]] imaging and some [[Microphone#Piezoelectric microphone|microphones]]. PZT is also sometimes used for [[piezo ignition]] in [[lighter]]s and [[barbecue grill|gas grills]].

=== Capacitors ===

Complex oxide materials are the dominant [[dielectric]] material in [[ceramic capacitor]]s.<ref name="capacitor">{{Cite journal | last1 = Ho | first1 = J. | last2 = Jow | first2 = T. R. | last3 = Boggs | first3 = S. | title = Historical introduction to capacitor technology | doi = 10.1109/MEI.2010.5383924 | journal = IEEE Electrical Insulation Magazine | volume = 26 | pages = 20 | year = 2010 | pmid =  | pmc = }}[http://www.ifre.re.kr/board/filedown.php?seq=179]</ref> About one trillion ceramic capacitors are produced each year to be used in electronic equipment.

=== Fuel cells ===

[[Solid oxide fuel cell]]s often use complex oxide materials as their [[electrolyte]]s, [[anode]]s, and [[cathode]]s.<ref>{{cite web|title=Lanthanum strontium cobalt oxide cathode powder|url=https://www.fuelcellmaterials.com/site/powders-and-pastes/cathodes?page=shop.product_details&category_id=11&flypage=vmj_naru.tpl&product_id=199|website=Fuel Cell Materials|accessdate=19 June 2015}}</ref>

=== Gemstone jewelry ===

[[File:Spanish jewellery-Gold and emerald pendant at VAM-01.jpg|thumb|upright|Spanish-made emerald and gold pendant exhibited at [[Victoria and Albert Museum]].<ref>{{cite web | url=http://collections.vam.ac.uk/item/O73034/pendant-unknown/ |title= Pendant &#124; V&A Search the Collections |website=Victoria and Albert Museum | accessdate=30 Jan 2014 | others=Given by Dame Joan Evans}} Museum item number M.138-1975</ref> ]]

Many precious gemstones, such as [[emerald]] and [[topaz]], are complex oxide crystals. Historically, some complex oxide materials (such as [[strontium titanate]], [[yttrium aluminium garnet]], and [[gadolinium gallium garnet]]) were also synthesized as inexpensive [[diamond simulant]]s, though after 1976 they were mostly eclipsed by [[cubic zirconia]].

=== New electronic devices ===

As of 2015, there is research underway to commercialize complex oxides in new kinds of electronic devices, such as [[ReRAM]], [[FeRAM]], and [[memristor]]s. Complex oxide materials are also being researched for their use in [[spintronics]].

Another potential application of complex oxide materials is [[Electric power transmission#Superconducting cables|superconducting power lines]].<ref>{{cite web|title=Superconductor cable systems|url=http://www.amsc.com/gridtec/superconductor_cable_systems.html|publisher=AMSC}}</ref> A few companies have invested in pilot projects, but the technology is not widespread.

== Commonly studied complex oxides ==
* [[Lead zirconate titanate]] (a [[piezoelectric]] material)
* [[Lanthanum aluminate]] (a [[high-κ dielectric|high-dielectric]] insulator)
* [[Strontium titanate]] (a high-dielectric [[semiconductor]])
* [[Lanthanum strontium manganite]] (a material exhibiting [[colossal magnetoresistance]])
* [[Barium titanate]] (a [[multiferroic]] material)
* [[Bismuth ferrite]] (a multiferroic material)
* [[Yttrium barium copper oxide]] (a [[high-temperature superconductor]])
* [[Bismuth strontium calcium copper oxide]] (a high-temperature superconductor)

== See also ==
* [[Multiferroics]]
* [[Mott insulator]]
* [[Colossal magnetoresistance]]
* [[Half metal]]
* [[Lanthanum aluminate-strontium titanate interface]]

==References==
{{reflist}}

*
*
*
*

== External links ==
* [http://www.nature.com/news/2009/090506/full/459028a.html Materials science: Enter the oxides]
* [http://www.nature.com/nmat/journal/v7/n9/full/nmat2264.html Condensed-matter physics: Complex oxides on fire]
* [http://www.nature.com/nmat/journal/v8/n4/full/nmat2414.html Complex oxides: A tale of two enemies]
* [http://www.nature.com/nmat/focus/oxide-interfaces/index.html Oxide interfaces]

[[Category:Oxides]]
[[Category:Ferromagnetic materials]]
[[Category:Superconductivity]]