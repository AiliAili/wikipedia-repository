{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox military installation
| name = RAF Throwley<br><s>Throwley Aerodrome</s>
| ensign=[[File:RAF type A roundel.svg|50px]][[File:Ensign of the Royal Air Force.svg|90px]]
| native_name = 
| partof = 
| location = 
| nearest_town = [[Throwley]], [[Kent]]
| country = England
| image = 
| caption = 
| coordinates = {{Coord|51|14|52|N|000|51|07|E|region:GB_type:airport|display=inline,title}}
| type = Royal Air Force base
| pushpin_map = Kent
| pushpin_label = RAF Throwley
| pushpin_map_caption = Shown within Kent
| ownership = [[Air Ministry]]
| operator = [[Royal Air Force]]<br>[[Royal Flying Corps]]
| controlledby = 
| site_area = 
| code = 
| built = {{Start date|1917}}
| builder = 
| used = 1917-{{End date|1919}}
| height = 
| materials = 
| condition = 
| fate = 
| battles = [[First World War]]
| events = 
| current_commander = <!-- current commander -->
| past_commanders = <!-- past notable commander(s) -->
| garrison = 
| occupants = 
| open_to_public = 
| website = 
| IATA = 
| ICAO = 
| FAA = 
| TC = 
| LID = 
| GPS = 
| WMO = 
| elevation = {{Convert|112|m|0}}
| r1-number =00/00
| r1-length = <!--{{Convert||m|0}}-->
| r1-surface = Grass field
}}
'''Royal Air Force Throwley''' or more simply '''RAF Throwley''' is a former [[Royal Air Force]] (RAF) installation located {{Convert|1.2|mi}} south of [[Throwley]], [[Kent]] and {{Convert|7|mi}} north of [[Ashford, Kent|Ashford]], Kent. The installation was also used by the [[Royal Flying Corps]] was previously called Throwley Aerodrome before being taken over the RAF during April 1918 and renamed to its current name.

==History==

Land situated between Bells Forstal and Throwley Forstal, including Dodds Willows and the Bells Forstal farmhouse was acquired by the [[Royal Flying Corps]] in 1916 for use as a landing ground for home defence squadrons defending [[London]] and the [[Thames Estuary]] and Kent.<ref>{{cite web|url=http://www.throwleypc.kentparishes.gov.uk/default.cfm?pid=2464|title=Past Times- Throwley Airfield|publisher=Throwley Parish Council|accessdate=10 February 2014}}</ref> From October 1916 [[No. 50 Squadron RAF|50 Squadron RFC]] detached aircraft to Throwley.{{sfn|Jefford|1988|p=41}} In July 1917 newly formed [[No. 112 Squadron RAF|112 Squadron]] was based with a variety of biplane fighters including the [[Sopwith Pup]], [[Sopwith Camel]] and [[Sopwith Snipe]].{{sfn|Jefford|1988|p=56}} In February 1918 [[No. 143 Squadron RAF|143 Squadron]] was formed at Throwley flying the [[Armstrong Whitworth F.K.8]] but it moved soon after to nearby [[RAF Detling]].{{sfn|Jefford|1988|p=61}}

[[No. 188 Squadron RAF|188 Squadron]] was formed at Throwley on 20 December 1917 as a training unit with the [[Avro 504]]K, in June 1918 the squadron provided training for the units flying the [[Sopwith Camel]]. In March 1919 188 Squadron RAF was disbanded<ref>{{cite web|url=http://www.raf.mod.uk/history/188squadron.cfm|title=188 Squadron|publisher=Royal Air Force|accessdate=10 February 2014}}</ref> and in June 1919 112 Squadron RAF was disbanded and the land was returned to agricultural use.{{sfn|Jefford|1988|p=56}}

===Units and aircraft===
* [[No. 50 Squadron RAF|No. 50 Squadron RFC]] (1916-1918) detachments from [[RAF Detling|Detling Aerodrome]]
* [[No. 112 Squadron RAF|No. 112 Squadron RFC/RAF]] (1918-1919) [[Sopwith Pup]], [[Sopwith Camel]] and [[Sopwith Snipe]]
* [[No. 142 Squadron RAF|No. 142 Squadron RFC]] (1918) [[Armstrong Whitworth F.K.8]]

==References==

===Citations===
{{Reflist}}

===Bibliography===
*{{cite book |last1=Jefford [[Order of the British Empire|MBE]] |first1=[[Wing Commander (rank)|Wg Cdr]] C G |title= RAF Squadrons. A comprehensive record of the movement and equipment of all RAF squadrons and their antecedents since 1912 |year=1988 |publisher= Airlife |location= [[Shrewsbury]] |isbn= 1-85310-053-6 |ref= {{harvid|Jefford|1988}} }}

==External links==
*[http://www.pigstrough.co.uk/ww1/Zom.htm Reminisces of operations involving Throwley]

{{DEFAULTSORT:Throwley}}
[[Category:Royal Air Force stations in Kent]]
[[Category:Royal Flying Corps airfields]]
[[Category:Royal Flying Corps airfields in Kent]]
[[Category:1916 establishments in the United Kingdom]]
[[Category:1919 disestablishments]]