{{Infobox book 
|name       = Tom Grogan
|image      = Charles S. Reinhart (1895) from Tom Grogan.jpg
|caption    = Illustration by [[Charles Stanley Reinhart]] appearing in serialization of ''Tom Grogan'' in ''The Century''
|author     = [[Francis Hopkinson Smith]]
|country    = United States
|language   = English
|genre      = Novel
|publisher  = 
|pub_date   = 1896
|media_type = Print ([[Hardcover]])
|pages      = 211 pp
}}
'''''Tom Grogan''''' is a novel published in 1896 by [[Francis Hopkinson Smith]] that was the [[Publishers Weekly list of bestselling novels in the United States in the 1890s#1896|best selling book in the United States in 1896]].<ref name="hackett">Hackett, Alice Payne. [https://books.google.com/books?id=cOI8AQAAIAAJ Seventy Years of Best Sellers 1895-1965], p. 92 (1967) (the lists for 1895-1912 in this volume are derived from the lists published in ''[[The Bookman (New York)]]'')</ref><ref name="review1">(22 April 1896). [http://query.nytimes.com/mem/archive-free/pdf?res=F50A17FE385515738DDDAB0A94DC405B8685F0D3 An Idyl of Staten Island (book review)], ''[[The New York Times]]''</ref> The novel was also [[Serial (literature)|serialized]] in ''[[The Century Magazine]]'' starting in December 1895,<ref>[https://books.google.com/books?id=E8hZAAAAYAAJ&pg=PA238#v=onepage&q&f=false ''Tom Grogan'', December 1895 Century]</ref> with illustrations by [[Charles Stanley Reinhart]].

==Plot==
An 1898 literature guide provided this synopsis of the plot:
<blockquote>
''Tom Grogan'', by F. Hopkinson Smith (1895.) is a spirited and most entertaining and ingenious study of laboring life in [[Staten Island]], [[New York (state)|New York]].

Tom Grogan was a [[stevedore]], who died from the effects of an injury. With a family to support, his widow conceals the fact of her husband's death, saying that he is sick in a hospital, that she may assume both his name and business.

She is thenceforth known to all as 'Tom Grogan'. A sturdy, cheery, capable Irishwomen, she carries on the business with an increasing success, which arouses the jealous opposition of some rival stevedores and walking delegates of the labor union, which she has refused to join.

The story tells how, with marvelous pluck, Tom meets all the contemptible means which her enemies employ in order to down her, they resorting even to the law, blackmail, arson, and attempted murder. In all her mannish employments her mother-heart beats warm and true, and her little crippled Patsy, a companion to [[Charles Dickens|Dickens's]] [[Tiny Tim (A Christmas Carol)|Tiny Tim]], and Jenny the daughter with her own tender love affair, are objects of Tom's constant solicitude.

The author has given a refreshing view of a soul of heroic mold beneath an uncouth exterior, and a pure life where men are wont to expect degradation.<ref name="library">[[Charles Dudley Warner|Warner, Charles Dudley]], ed. [https://books.google.com/books?id=Kk0mAQAAMAAJ&pg=PA482#v=onepage&q&f=false Library of the World's Best Literature, Vol. XXX], pp. 482-83 (1898)</ref>
</blockquote>

==References==
{{reflist}}

==External links==
*[http://www.gutenberg.org/ebooks/850 ''Tom Grogan''] full text at [[Project Gutenberg]]

[[Category:1896 novels]]
[[Category:19th-century American novels]]
[[Category:Novels set in New York City]]
[[Category:Works originally published in The Century Magazine]]
[[Category:Novels by Francis Hopkinson Smith]]