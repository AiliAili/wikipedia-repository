{{Multiple issues|{{more footnotes|date=April 2017}}{{pov|date=April 2017}}}}

{{Infobox Politician
| name = Andy McGuire
| residence =
| image  = Andy-mcguire-2016.jpg
| caption = Andy McGuire
| birth_date = {{Birth date and age|1956|11|26|mf=y}}
| birth_place = [[Waterloo, Iowa]]
| birth_name = 
| death_date =
| death_place =
| death_cause =
| known = 
| alma_mater = [[Creighton University]] ([[Bachelor of Science|B.S.]]), [[Creighton University School of Medicine]] ([[Medical Doctorate|M.D.]]), [[Kennesaw State University]] – [[Michael J. Coles College of Business]] ([[Masters of Business Administration|M.B.A.]]) 
| office1 = Chair of the [[Iowa Democratic Party]]<ref name="bowen">{{cite web
 | url = https://caffeinatedthoughts.com/2015/04/some-fun-facts-about-the-new-iowa-democratic-party-chair/
 | title = Some Fun Facts About the New Iowa Democratic Party Chair
 | last = Bowen
 | first = Jenifer
 | date = April 14, 2015
 | website = caffeinatedthoughts.com
 | publisher = Caffeinated Thoughts
 | access-date = November 22, 2016
 | quote = }}</ref><ref name="boshart">{{cite news
 | last = Boshart
 | first = Rod
 | title = Iowa Democrats elect chairwoman
 | newspaper = Sioux City Journal
 | publisher = Des Moines Bureau
 | date = Jan 17, 2015
 | url = http://siouxcityjournal.com/news/state-and-regional/iowa/iowa-democrats-elect-chairwoman/article_24df96a9-2c2a-566f-8e78-525d16733adc.html
 | access-date = January 19, 2017}}</ref>
| term_start1 = January 2015
| term_end1 = Present
| succeeding1 = Scott Brennan
| successor1 = 
| constituency1 = 
| office2 = 
| term_start2 = 
| term_end2 = 
| predecessor2 = 
| successor2 = 
| occupation = IDP Chair
| title =
| term =
| predecessor = 
| successor = 
| party = [[Iowa Democratic Party]] 
| religion = 
| spouse = Dr. Dan McGuire
| children = 
| relations =
| website = 
| footnotes = 
}}

'''Andy McGuire''' (born November 26, 1956) is leader of Iowa Democratic party. She serves as [[Iowa Democratic Party]] (IDP) chair;<ref name="IDP">{{cite web| url = http://iowademocrats.org/leadership/| title = Party Leadership & Staff| website = iowademocrats.org| publisher = Iowa Democratic Party| access-date = November 21, 2016}}</ref> elected in January 2015 for a two-year term by the Party’s State Central Committee.<ref name="desmoinesdem">{{cite web| url = http://www.bleedingheartland.com/2015/01/17/three-pros-and-three-cons-of-andy-mcguire-as-iowa-democratic-party-chair-updated/| title = Three pros and three cons of Andy McGuire as Iowa Democratic Party chair (updated)| last = desmoinesdem| date = January 17, 2015| website = bleedingheartland.com| access-date = November 22, 2016| quote = Earlier today the Iowa Democratic Party’s State Central Committee selected Dr. Andy McGuire to lead the party for the next two years.}}</ref> Prior to becoming the Chair of the Iowa Democratic Party, McGuire ran for the Democratic nomination for Iowa’s Lieutenant Governor.<ref name="bowen"/>

==Early life==
{{Refimprove section|date=April 2017}}
McGuire was born in [[Waterloo, Iowa]]. She was the fifth of six children. Her father owned a small business that made concrete mixers and pumps. McGuire went to Kingsley, Hoover and [[Waterloo West High School|West High]] schools in Waterloo and attended [[University of Northern Iowa|UNI]] for some classes while in high school. She graduated with honors from West High in 1975.

==Higher Education==
In 1978, McGuire graduated with honors from [[Creighton University]] with a degree in [[Chemistry]]. She continued onto Medical School at Creighton and graduated with a Medical Doctorate from [[Creighton University School of Medicine]].<ref name="uiowa">{{cite web| url =http://ppc.uiowa.edu/forkenbrock/speaker/andrea-mcguire| title =Andrea McGuire| date = 2016| website =uiowa.edu| publisher =University of Iowa| access-date =November 22, 2016}}</ref>  There were 11 women in her class. McGuire remembers this time as one where women were not encouraged to become physicians.{{Citation needed|date=April 2017}}

After graduation, McGuire went to [[St. Louis]] for her medical internship and residency. She did a rotation at St. Johns in St. Louis for a year and then worked at a walk-in clinic for a year which helped to pay off her student loans. After that, she went into a Nuclear Medicine Residency for the next three years at the Veteran’s Administration and [[Saint Louis University Hospital|St. Louis University Medical Center]].

In 1999, McGuire pursued an MBA from [[Kennesaw State University]]<ref name="uiowa" /> near [[Atlanta, Georgia]]. She graduated in 2001 with a [[Master of Business Administration|Master’s in Business Administration]] Physician Executives at Michael Cole School of Business.

==Career and Family==
While attending Creighton University, McGuire met fellow student Dan McGuire of [[Holstein, Iowa]] and they married.

In 1987, McGuire followed her husband, currently an orthopedic spine surgeon, to his spine fellowship in [[Springfield, Illinois]]. There she worked part-time with a radiology group. In the meantime, McGuire also landed another job at [[Washington University]]. 

After several years working in [[nuclear medicine]] and publishing in her field, McGuire and her husband moved back to Iowa to raise their children. In short time, McGuire found work at the Des Moines Veterans Administration before becoming Medical Director at [[Wellmark Blue Cross Blue Shield|Wellmark Insurance]].

McGuire left Wellmark to become Chief Medical Officer and Vice President of Medical and Network Management/Risk Selection at American Republic Insurance Company (which later became American Enterprise)<ref name="uiowa" />. McGuire headed up disease management, case management division, managed the network of doctors across the United States, and headed up the pharmacy to make sure insureds could get medications for their illness.{{Citation needed|date=April 2017}}

In 2012, McGuire was asked to be the President of Meridian of Iowa, a [[Medicaid]] managed care company.<ref name="Gardyasz">{{cite news| last = Gardyasz| first =Joe| title =LAUNCH: Meridian Health Plan of Iowa| newspaper =Business Record| publisher =Business Record| date =April 13, 2012| url = http://www.businessrecord.com/Content/Health---Wellness/Health---Wellness/Article/LAUNCH--Meridian-Health-Plan-of-Iowa/174/836/54721| access-date = November 22, 2016 }}</ref> Under McGuire’s leadership, Meridian doubled immunizations rates and prenatal care<ref name="PRN">{{cite press release| title = McGuire Steps Down as President of Meridian Health Plan| publisher = Meridian Health Plan| date = March 3, 2015| url = http://www.prnewswire.com/news-releases/mcguire-steps-down-as-president-of-meridian-health-plan-300044755.html| access-date = January 19, 2017 }}</ref>. After three years of leading Meridian, McGuire left the company<ref name="PRN" /> to become the IDP chair. 

As chair, McGuire has raised more than $2 million for the IDP, which represents among the largest amounts raised by a chair in Iowa Democratic Party history. She has traveled to all of Iowa’s 99 counties to meet with activists to build a 99 county strategy for a statewide Democratic Party; and formed the Caucus Committee, which recommended changes and improvements for the 2020 caucuses.{{Citation needed|date=April 2017}}

Currently, McGuire lives with her husband in Des Moines, Iowa. They have seven children, and one grandchild.

==References==
{{reflist|2}}



{{s-start}}
{{s-off}}
{{s-bef|before=Scott Brennan}}
{{s-ttl|title=Chair of the [[Iowa Democratic Party]]|years=2015–present}}
{{s-inc}}
{{s-end}}


{{DEFAULTSORT:McGuire, Andy}}

[[Category:1956 births]]
[[Category:Living people]]
[[Category:Iowa Democrats]]
[[Category:People from Waterloo, Iowa]]
[[Category:American women in politics]]