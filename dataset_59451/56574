{{Infobox journal
| title = Diamond and Related Materials
| cover = [[File:DRM cover.jpg|250px]]
| editor = Ken Haenen
| discipline = [[Materials science]]
| abbreviation = Diamond Relat. Mater.
| publisher = [[Elsevier]]
| country =
| frequency = 10/year
| history = 1991-present
| openaccess = 
| license =
| impact = 2.125
| impact-year = 2015
| website = http://www.journals.elsevier.com/diamond-and-related-materials
| link1 = http://www.sciencedirect.com/science/journal/09259635
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR =
| OCLC = 905470023
| LCCN = sn93033809
| CODEN = DRMTE3
| ISSN = 0925-9635
| eISSN =
}}
'''''Diamond and Related Materials''''' is a [[peer-reviewed]] [[scientific journal]] in [[materials science]] covering research on all forms of [[diamond]] and other related materials, including [[diamond-like carbon]]s, [[carbon nanotube]]s, [[graphene]], and [[boron nitride|boron]] and [[carbon nitride]]s. The journal is published by [[Elsevier]] and the [[editor-in-chief]] is Ken Haenen ([[University of Hasselt]]).

==Abstracting and indexing==
The journal is abstracted and indexing in:
{{columns-list|colwidth=30em|
*[[Cambridge Scientific Abstracts|CSA databases]]
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-06-07 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
*[[Current Contents]]/Physical, Chemical & Earth Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-06-07}}</ref>
*Current Contents/Engineering, Computing & Technology<ref name=ISI/>
*[[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2016-06-07}}</ref>
*[[Materials Science Citation Index]]
*[[PASCAL (database)|PASCAL]]
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-06-07}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.125.<ref name=WoS>{{cite book |year=2016 |chapter=Diamond and Related Materials |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist|30em}}

==External links==
*{{Official website|http://www.journals.elsevier.com/diamond-and-related-materials}}

[[Category:English-language journals]]
[[Category:Materials science journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1991]]


{{Physics-journal-stub}}