{{Orphan|date=June 2013}}
{{Infobox_gene}}

'''Glutamine Serine Rich Protein 1''' or '''QSER1''' is a [[protein]] encoded by the ''QSER1'' [[gene]].<ref name="NCBI QSER1 Gene">{{cite web|title=NCBI QSER1 Gene|url=http://www.ncbi.nlm.nih.gov/gene/79832}}</ref>

The function of this protein is currently unknown. QSER1 has one alias, FLJ21924.<ref name="NCBI QSER1 Gene" />

__TOC__

== Gene ==

=== Location ===


The QSER1 gene is found on the short arm of chromosome 11 (11p13), beginning at 32,914,792 bp and ending at 33,001,816 bp. It is 87,024 bp in length. It is located between the genes DEPDC7 and PRRG4 and is 500,000 bp downstream from the Wilms Tumor 1 gene ([[WT1]]), which is implicated in multiple pathologies.<ref name="NCBI QSER1 Gene" /><ref name="Genecards QSER1">{{cite web|title=Genecards QSER1|url=http://genecards.org/cgi-bin/carddisp.pl?gene=QSER1&search=qser1}}</ref>

=== Homology ===

==== Orthologs ====

QSER1 is highly conserved in most species of the clade [[Chordata]]. [[Ortholog]]s have been found in primates, birds, reptiles, amphibians, and fish as far back as the [[coelacanth]], which diverged 414.9 million years ago.<ref name="NCBI QSER1 Gene" /><ref name="Genecards QSER1"/>

==== Paralogs ====

QSER1 has one [[paralog]] in humans, [[Proline-rich 12]], or PRR12. PRR12 is found on chromosome 9 at 9q13.33, which does not have known function. PRR12 is found in most chordate species as far back as the coelacanth.<ref name="NCBI PRR12 Gene">{{cite web|title=NCBI PRR12 Gene|url=http://www.ncbi.nlm.nih.gov/gene/57479}}</ref> The duplication event likely occurred sometime in the chordate lineage near the divergence of the coelacanth. Both PRR12 and QSER1 contain the conserved DUF4211 domain near the 3’ ends of the genes.<ref name="NCBI QSER1 Gene" /><ref name="NCBI PRR12 Gene" />

== mRNA ==

=== Promoter and transcription factors ===

The promoter region for QSER1 is 683 bp in length and is found on chromosome 11 between 32,914,224 bp and 32,914,906. There is some overlap between the promoter region and the 5’ UTR of QSER1. Predicted transcription factors with conservation include (but are not limited to) [[EGR1]], [[p53]], [[E2F3]], [[E2F4]], [[PLAG1]], [[NeuroD2]], [[Myf5]], IKAROS1, [[SMAD (protein)|SMAD]]3, [[KRAB]], [[MZF1]], and c-[[MYB (gene)|Myb]].<ref name="Genomatix Tools: El Dorado">{{cite web | title = Genomatix Tools: El Dorado | url = http://www.genomatix.de/}}</ref>

=== Expression ===

==== Normal expression ====

Expression of QSER1 is seen at levels lower than 50% in many tissues. However, notable expression is seen in [[skeletal muscle]], the [[Vermiform appendix|appendix]], [[trigeminal ganglia]], cerebellum peduncles, [[pons]], [[spinal cord]], [[ciliary ganglion]], [[globus pallidus]], [[subthalamic nucleus]], [[dorsal root ganglion]], fetal [[liver]], [[adrenal gland]], [[ovary]], uterus corpus, [[cardiac myocyte]]s, the [[atrioventricular node]], [[skin]], [[pituitary gland]], [[tongue]], early erythroid progenitors, and [[tonsil]].<ref name="NCBI GeoProfiles db; QSER1 GDS596">{{cite web | title = NCBI GeoProfiles db; QSER1 GDS596| url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS596:219705_at}}</ref><ref name="NCBI EST Profile db; QSER1">{{cite web|title=NCBI EST Profile db; QSER1|url=http://www.ncbi.nlm.nih.gov/UniGene/ESTProfileViewer.cgi?uglist=Hs.369368}}</ref>
[[File:NCBI GeoProfiles expression data for QSER1.png|thumb|NCBI GeoProfiles expression data for QSER1]]

==== Differential expression ====

A notable decrease in QSER1 expression has been noted in renal [[mesangial cells]] in response to treatment with 25 mM glucose. This condition was studied as differential expression of genes involved in cell cycle regulation had been noted in these cells in response to high glucose levels seen with [[diabetes mellitus]].<ref>{{cite journal | vauthors = Clarkson MR, Murphy M, Gupta S, Lambe T, Mackenzie HS, Godson C, Martin F, Brady HR | title = High glucose-altered gene expression in mesangial cells. Actin-regulatory protein gene expression is triggered by oxidative stress and cytoskeletal disassembly | journal = The Journal of Biological Chemistry | volume = 277 | issue = 12 | pages = 9707–12 | date = Mar 2002 | pmid = 11784718 | doi = 10.1074/jbc.M109172200 | url = http://www.jbc.org/content/277/12/9707.long }}</ref><ref name="NCBI GeoProfiles db;  QSER1 GDS1891">{{cite web|title=NCBI GeoProfiles db;  QSER1 GDS1891|url=http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS1891:1427151_at}}</ref>
A different study noted overexpression of QSER1 in pathological [[cardiomyopathy]]. This condition is associated with altered expression of genes involved in immune responses, signaling, cell growth, and proliferation as well as infiltration of [[B lymphocytes]].<ref name=Galindo>{{cite journal | vauthors = Galindo CL, Skinner MA, Errami M, Olson LD, Watson DA, Li J, McCormick JF, McIver LJ, Kumar NM, Pham TQ, Garner HR | title = Transcriptional profile of isoproterenol-induced cardiomyopathy and comparison to exercise-induced cardiac hypertrophy and human cardiac failure | journal = BMC Physiology | volume = 9 | issue = 23 | pages = 23 | date = 9 Dec 2009 | pmid = 20003209 | doi = 10.1186/1472-6793-9-23 | url = http://link.springer.com/article/10.1186%2F1472-6793-9-23 }}</ref><ref name="NCBI GeoProfiles db;  QSER1 GDS3596">{{cite web|title=NCBI GeoProfiles db;  QSER1 GDS3596|url=http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS3596:1427152_at}}</ref>

Differential expression of QSER1 is seen in multiple cancer conditions. Overexpression of QSER1 was noted in Burkitt’s Lymphoma.<ref name="NCBI GeoProfiles db; QSER1 GDS596"/> QSER1 expression also increases with increasing [[Gleason score]] (more advanced stages) of prostate cancer.<ref name="NCBI GeoProfiles db; QSER1 GDS1746">{{cite web | title = NCBI GeoProfiles db; QSER1 GDS1746| url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS1746:219705_at}}</ref> In a study on breast cancer response to [[paclitaxel]] and fluorouracil‐doxorubicin‐cyclophosphamide [[chemotherapy]], it was noted that breast cancer lines with higher levels of QSER1 were more likely to respond to treatment than those with underexpression of QSER1.<ref name="NCBI GeoProfiles db; QSER1 GDS3721">{{cite web | title = NCBI GeoProfiles db; QSER1 GDS3721| url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS3721:226265_at}}</ref>
	Greater expression of QSER1 was also noted in mammary epithelial cells of  immortalized cell lines than in mammary epithelial cells from cell lines with finite lifespan.<ref name="NCBI GeoProfiles db; QSER1 GDS2810">{{cite web | title = NCBI GeoProfiles db; QSER1 GDS2810| url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS2810:219705_at}}</ref>

=== 3’ UTR ===

Over 20 stem loops are predicted in the 3’ UTR of QSER1. 16 stem loops are found within the first 800 bp of 3’ UTR.<ref name=mfold>{{cite web|title=mfold|url=http://mfold.rna.albany.edu/?q=mfold/RNA-Folding-Form2.3}}</ref> The 3’ UTR is almost entirely conserved in mammals with less conservation seen in other organisms.<ref name="SDSC Biology Workbench ClustalW">{{cite web|title=SDSC Biology Workbench ClustalW|url=http://workbench.sdsc.edu/}}</ref>

== Protein ==

=== General properties ===

[[File:QSER1 protein overview.jpg|thumb|QSER1 protein overview]]
QSER1 protein is 1735 amino acids in length.<ref name="NCBI QSER1 Protein">{{cite web | title = NCBI QSER1 Protein | url = http://www.ncbi.nlm.nih.gov/protein/115647981}}</ref> The composition of the peptide is significantly high in serine and glutamine: 14.7% serine residues and 8.9% glutamine.<ref name="SDSC Biology Work Bench; SAPS">{{cite web | title = SDSC Biology Work Bench; SAPS | url = http://workbench.sdsc.edu/}}</ref>

=== Conservation ===

QSER1 protein is highly conserved in chordate species.
The table below shows information on the protein orthologs.
[[File:Tree of QSER1 Orthologs.pdf|thumb|Tree of QSER1 Orthologs]]
{| class="wikitable"
|-
! Genus and species name !! Common name !! Protein accession number<ref name=NCBI>{{cite web|title=NCBI National Center for Biotechnology Information|url=http://www.ncbi.nlm.nih.gov/}}</ref>!!Sequence Identity to human protein<ref name="NCBI"/>
|-
| ''Homo sapiens'' || Humans || NP_001070254.1 ||
|-
| ''Pan troglodytes'' || Chimpanzee || XP_508354.3 || 99%
|-
| ''Macaca mulatta'' || Rhesus macaque || NP_001244647.1 || 98%
|-
| ''Callithris jacchus'' || Marmoset || XP_002755192.1 || 96%
|-
| ''Ailuropoda melanoleuca'' || Giant panda || XP_002917539.1 || 90%
|-
| ''Loxodonta africana''|| Elephant|| XP_003412344.1 || 88%
|-
| ''Mus musculus'' ||Mouse || NP_001116799.1 || 81%
|-
| ''Monodelphis domestica''  ||Opossum || XP_001368629.1 || 71%
|-
| ''Ornithorhynchus anatinus'' ||Platypus || XP_001506659.2 || 75%
|-
| ''Taeniopygia guttata'' ||Zebra finch || XP_002195876.1 || 69%
|-
| ''Gallus gallus'' ||Chicken || NP_001186343.1 || 69%
|-
| ''Anolis carolinensis'' || Carolina anole (lizard) || XP_003214747.1 || 62%
|-
| ''Takifugu rubripes'' || Japanese pufferfish || XP_003977915.1 || 48%
|-
| ''Latimeria chalumnae ''|| Coelacanth || N/A || 62%
|}

=== Domains and motifs ===

[[File:PSORT prediction of conserved nuclear localization and nuclear localization signals in QSER1 protein.png|thumb|PSORT prediction of conserved nuclear localization and nuclear localization signals in QSER1 protein]]
QSER1 protein contains two high conserved domains found not only in QSER1 but also in other protein products. These include the PHA02939 domain from amino acid 1380-1440 and the DUF4211 domain from amino acid 1522-1642.<ref name="NCBI Conserved domains db; DUF4211">{{cite web | title = NCBI Conserved domains db; DUF4211 | url = http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=206097}}</ref><ref name="NCBI Conserved domains db; PHA02939">{{cite web | title = NCBI Conserved domains db; PHA02939 | url = http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=165249}}</ref>
Nuclear localization was predicted by pSORT. This property was conserved from the human QSER1 to the coelacanth QSER1. Multiple conserved nuclear localization signals were also predicted within the QSER1 protein by pSORT.<ref name="pSORT II Prediction">{{cite web|title=pSORT II Prediction|url=http://psort.hgc.jp/form2.html}}</ref>

=== Structure ===

Predictions of the QSER1 protein structure indicate that the protein contains many [[alpha helices]].<ref name="SDSU Biology Workbench; PELE">{{cite web|title=SDSC Biology Workbench; PELE|url=http://workbench.sdsc.edu/}}</ref><ref name="NCBI cBLAST; QSER1">{{cite web|title=NCBI cBLAST; QSER1|url=http://www.ncbi.nlm.nih.gov/Structure/cblast/cblast.cgi}}</ref><ref name=Phyre2>{{cite web|title=Phyre2|url=http://www.sbg.bio.ic.ac.uk/~phyre/index.cgi}}</ref> NCBI cBLAST predicted structural similarity between the QSER1 protein and the ''[[Schizosaccharomyces pombe]]'' (fission yeast) [[RNA Polymerase II]] A chain. The two regions of similarity occur between amino acids 56-194 and 322-546.<ref name="NCBI cBLAST; QSER1"/> This first region (56-194) is a regulatory region in both the human and yeast RNA Polymerase II containing multiple repeats of the sequence YSPTSPSYS. Phosphorylation of serine residues in this region regulates progression through the steps of gene transcription.<ref name=Moller>{{cite journal | vauthors = Möller A, Xie SQ, Hosp F, Lang B, Phatnani HP, James S, Ramirez F, Collin GB, Naggert JK, Babu MM, Greenleaf AL, Selbach M, Pombo A | title = Proteomic analysis of mitotic RNA polymerase II reveals novel interactors and association with proteins dysfunctional in disease | journal = Molecular & Cellular Proteomics | volume = 11 | issue = 6 | pages = M111.011767 | date = Jun 2012 | pmid = 22199231 | doi = 10.1074/mcp.M111.011767 | url = http://www.mcponline.org/content/11/6/M111.011767.long }}</ref> A 3D structure was provided for this region. The structurally similar region is on the exterior of the protein molecule and forms part of the DNA binding cleft.

Further structural similarity to a viral RNA Polymerase binding protein was predicted by Phyre2.<ref name="Phyre2"/> This structure is found at the very end of the protein between amino acids 1671-1735. The structure has a long region of alpha helices that were also predicted by SDSC Biology Workbench PELE. An image of the structurally similar region and sequence alignment is shown on the right. Regions before the identified structurally similar domain show two other alpha helices predicted with high confidence.<ref name="Phyre2"/>

=== Post translational modifications ===

==== Phosphorylation ====

[[File:ExPASy NetPhos Predicted Phosphorylation Sites.png|thumb|ExPASy NetPhos Predicted Phosphorylation Sites]]
There are 12 confirmed phosphorylation sites on the QSER1 protein. Eight are phosphoserines, one phosphotyrosine, and three phosphothreonines. Three of these sites have been shown to be phosphorylated by ATM and ATR in response in DNA damage.<ref name=Matsuoka>{{cite journal | vauthors = Matsuoka S, Ballif BA, Smogorzewska A, McDonald ER, Hurov KE, Luo J, Bakalarski CE, Zhao Z, Solimini N, Lerenthal Y, Shiloh Y, Gygi SP, Elledge SJ | title = ATM and ATR substrate analysis reveals extensive protein networks responsive to DNA damage | journal = Science | volume = 316 | issue = 5828 | pages = 1160–6 | date = May 2007 | pmid = 17525332 | doi = 10.1126/science.1140321 | url = http://www.sciencemag.org/content/316/5828/1160.short%7Caccessdate | bibcode = 2007Sci...316.1160M | displayauthors = 8 }}</ref> 123 other possible phosphorylation sites have been predicted using the ExPASy NetPhos tool.<ref name="ExPASy NetPhos">{{cite web|title=ExPASy NetPhos|url=http://www.cbs.dtu.dk/services/NetPhos/}}</ref>

==== SUMOylation ====

Interaction of QSER1 protein with [[SUMO protein|SUMO]] has been noted in multiple [[proteome]]-wide studies.<ref name=Tatham>{{cite journal | vauthors = Tatham MH, Matic I, Mann M, Hay RT | title = Comparative proteomic analysis identifies a role for SUMO in protein quality control | journal = Science Signaling | volume = 4 | issue = 178 | pages = rs4 | date = 21 Jun 2011 | pmid = 21693764 | doi = 10.1126/scisignal.2001484 | url = http://stke.sciencemag.org/cgi/content/abstract/sigtrans;4/178/rs4 }}</ref><ref name=Burderer>{{cite journal | vauthors = Bruderer R, Tatham MH, Plechanovova A, Matic I, Garg AK, Hay RT | title = Purification and identification of endogenous polySUMO conjugates | journal = EMBO Reports | volume = 12 | issue = 2 | pages = 142–8 | date = Feb 2011 | pmid = 21252943 | pmc = 3049431 | doi = 10.1038/embor.2010.206 }}</ref>  Predicted SUMOylation sites have been found in QSER1 protein. Highly conserved SUMOylation sites occur with the sequence MKMD at amino acid 794, VKIE at 1057, VKTG at 1145, LKSG at 1157, VKQP at 1487, and VKAE at 1492 .<ref name="ExPASy SUMOplot">{{cite web|title=ExPASy SUMOplot|url=http://www.abgent.com/sumoplot}}</ref>
[[File:Predicted SUMOylation Sites in QSER1 protein.png|thumb|Predicted SUMOylation Sites in QSER1 protein]]

=== Interactions ===

==== ATM/ATR ====

Phosphorylation of QSER1 at three serine residues, S1228, S1231, and S1239, by ATM and ATR in response to DNA damage was found in a [[proteome]]-wide study.<ref name=Matsuoka />

==== SUMO ====

Interaction of QSER1 with [[SUMO protein|SUMO]] has been confirmed in multiple studies.<ref name=Tatham /><ref name=Burderer /> The role of SUMOylation in QSER1 function is unclear. However, there may be a connection between QSER1 and SUMO in response to [[endoplasmic reticulum]] stress (often caused by accumulation of misfolded proteins). In a study on ER stress, QSER1 was tagged as an ER stress response gene with altered expression.<ref name=Dombroski>{{cite journal | vauthors = Dombroski BA, Nayak RR, Ewens KG, Ankener W, Cheung VG, Spielman RS | title = Gene expression and genetic variation in response to endoplasmic reticulum stress in human cells | journal = American Journal of Human Genetics | volume = 86 | issue = 5 | pages = 719–29 | date = May 2010 | pmid = 20398888 | doi = 10.1016/j.ajhg.2010.03.017 | url = https://genomics.med.upenn.edu/vcheung/pdfs/(33)Dombroski10.pdf }}</ref>  Further, in a study on SUMOylation in response to accumulated misfolded proteins and ER stress found QSER1 to be a SUMO interactant in this situation.<ref name=Tatham /> Any connection between these two activities is unstudied and unconfirmed.

==== RNA polymerase II ====

Direct interaction of QSER1 with RNA polymerase II was found in a study performed by Moller, et al. Interaction was shown to occur with the DNA-directed RNA polymerase II subunit, RPB1, of RNA polymerase II during both [[mitosis]] and [[interphase]]. Colocalization/interaction of QSER1 was shown to the regulatory region of RPB1 with 52 heptapeptide (YSPTSPSYS) repeats.<ref name=Moller />

==== NANOG and TET1 ====

Interaction between [[homeobox protein NANOG]] and [[Tet methylcytosine dioxygenase 1]] (TET1) has been shown to be important in establishing pluripotency during the generation of [[induced pluripotent stem cell]]s. QSER1 protein was shown to interact with both NANOG and TET1.<ref name=Costa>{{cite journal | vauthors = Costa Y, Ding J, Theunissen TW, Faiola F, Hore TA, Shliaha PV, Fidalgo M, Saunders A, Lawrence M, Dietmann S, Das S, Levasseur DN, Li Z, Xu M, Reik W, Silva JC, Wang J | title = NANOG-dependent function of TET1 and TET2 in establishment of pluripotency | journal = Nature | volume = 495 | issue = 7441 | pages = 370–4 | date = Mar 2013 | pmid = 23395962 | pmc = 3606645 | doi = 10.1038/nature11925 | url = http://www.nature.com/nature/journal/v495/n7441/full/nature11925.html?WT.ec_id=NATURE-20130321 | bibcode = 2013Natur.495..370C }}</ref>

==== Ubiquitin ====

QSER1 was found to interact with [[ubiquitin]] in two proteome-wide substrate studies.<ref name=Kim>{{cite journal | vauthors = Kim W, Bennett EJ, Huttlin EL, Guo A, Li J, Possemato A, Sowa ME, Rad R, Rush J, Comb MJ, Harper JW, Gygi SP | title = Systematic and quantitative assessment of the ubiquitin-modified proteome | journal = Molecular Cell | volume = 44 | issue = 2 | pages = 325–40 | date = Oct 2011 | pmid = 21906983 | pmc = 3200427 | doi = 10.1016/j.molcel.2011.08.025 }}</ref><ref name=Danielsen>{{cite journal | vauthors = Danielsen JM, Sylvestersen KB, Bekker-Jensen S, Szklarczyk D, Poulsen JW, Horn H, Jensen LJ, Mailand N, Nielsen ML | title = Mass spectrometric analysis of lysine ubiquitylation reveals promiscuity at site level | journal = Molecular & Cellular Proteomics | volume = 10 | issue = 3 | pages = M110.003590 | date = Mar 2011 | pmid = 21139048 | doi = 10.1074/mcp.M110.003590 | url = http://www.mcponline.org/content/10/3/M110.003590.long | pmc=3047152}}</ref> Specific details about this interaction have not been studied.

== Pathology ==

Altered expression of QSER1 is noted in pathological cardiomyopathy, Burkitt's Lymphoma, prostate cancer, and some breast cancers mentioned above.<ref name="Genomatix Tools: El Dorado" /><ref name="NCBI GeoProfiles db; QSER1 GDS596" /><ref name=Galindo /><ref name="NCBI GeoProfiles db; QSER1 GDS1746" /> NCBI AceView lists multiple mutations associated with other pathologies including an eight base pair and 13 base pair deletion in QSER1 associated with [[leiomyosarcoma]] of the [[uterus]], and 57 base pair difference in a neuroblastoma. Also listed are multiple splice variants with truncated 5' and/or 3' ends often noted in cancerous conditions.<ref>{{cite web|title=NCBI AceView db; QSER1|url=http://www.ncbi.nlm.nih.gov/IEB/Research/Acembly/av.cgi?db=human&term=qser1&submit=Go}}</ref>  Further, according to the NCBI OMIM database, multiple pathologies are associated with alterations in the 11p13 region and therefore may implicate QSER1.<ref name=OMIM>{{cite web|title=NCBI OMIM db; 11p13|url=http://www.ncbi.nlm.nih.gov/omim/?term=11p13}}</ref> These include Exudative [[Familial exudative vitreoretinopathy|Vitreoretinopathy]] 3,<ref>{{cite web|title=NCBI OMIM db; Exudative Vitreoretinopathy 3|url=http://omim.org/entry/605750}}</ref> Familial [[Candidiasis]] 3,<ref>{{cite web|title=NCBI OMIM db; Candidiasis, Familial 3|url=http://omim.org/entry/607644}}</ref> Centralopathic [[Epilepsy]],<ref>{{cite web|title=NCBI OMIM db; Centralopathic Epilepsy|url=http://omim.org/entry/117100}}</ref> and Autosomal Recessive Deafness 51.<ref>{{cite web|title=NCBI OMIM db; Autosomal Recessive Deafness 51|url=http://omim.org/entry/609941}}</ref>
QSER1 was also noted as a susceptibility gene for [[Parkinson's Disease]].<ref name=Dombroski />

== References ==
{{reflist|33em}}

[[Category:Human proteins]]