{{Infobox organization
| image         = 
| alt           = <!-- alt text; see [[WP:ALT]] -->
| caption       = 
| map           = <!-- optional -->
| motto         = 
| predecessor   = 
| successor     = 
| formation     = 
| extinction    = <!-- date of extinction, optional -->
| type          = <!-- [[Governmental organization|GO]], [[Non-governmental organization|NGO]], [[Intergovernmental organization|IGO]], [[International nongovernmental organization|INGO]], etc -->
| status        = <!-- ad hoc, treaty, foundation, etc -->
| purpose       = Development and promotion of accounting standards<ref name=about />
| headquarters  = 30 Canon Street, London {{flag|UK}}
| coords        = <!-- Coordinates of location using a coordinates template -->
| language      = <!-- official languages -->
| leader_title  = Executive Director
| leader_name   = Yael Almog<ref name=staff/>
| leader_title2 = 
| leader_name2  = 
| leader_name3  = 
| leader_title3 = 
| leader_title4 = 
| leader_name4  = 
| key_people    = 
| main_organ    = <!-- gral. assembly, board of directors, etc -->
| parent_organization = <!-- if one -->
| affiliations  = <!-- if any -->
| budget        = 
| remarks       = 
| name          = IFRS Foundation
| bgcolor       = <!-- header background color -->
| fgcolor       = <!-- header text color -->
| image_border  = 
| size          = <!-- default 200px -->
| msize         = <!-- map size, optional, default 250px -->
| malt          = <!-- map alt text -->
| mcaption      = <!-- optional -->
| map2          = 
| abbreviation  = 
| location      = 
| region_served = 
| membership    = 
| general       = <!-- Secretary General -->
| num_staff     = 
| num_volunteers = 
| website       = {{url|http://www.ifrs.org/}}
| former name   = IASC Foundation
}}
{{accounting}}
The '''International Financial Reporting Standards Foundation''', or '''IFRS Foundation''', is a [[nonprofit organization|nonprofit]] [[accounting]] organization. Its main objectives include the development and promotion of the [[International Financial Reporting Standards|International Financial Reporting Standards (IFRSs)]] through the [[International Accounting Standards Board|International Accounting Standards Board (IASB)]], which it oversees.<ref name=about>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/IASCF+and+IASB.htm About the IFRS Foundation and the IASB]. Retrieved on April 25, 2012.</ref><ref name=ceo>Deloitte Global Services Limited, 2012. [http://www.deloitte.com/view/en_GX/global/press/d08b3ded18365310VgnVCM2000001b56f00aRCRD.htm Former Deloitte Global CEO James Quigley appointed to the IFRS Foundation]. Retrieved on April 25, 2012.</ref>

The foundation was formerly named the [[International Accounting Standards Committee]] (IASC) Foundation until a renaming on 1 July 2010, and as of 2012 is governed by a board of 22 [[trustees]].<ref name=deloitte>Deloitte Global Services Limited, 2012. [http://www.iasplus.com/en/resources/resource24 IFRS Foundation]. Retrieved on April 25, 2012.</ref>

==Activities==

===Standard-setting===
The IFRS Foundation sets out the IFRSs and their interpretations, which include the following:
* the International Financial Reporting Standards (IFRSs);
* the International Accounting Standards (IASs);
* the International Financial Reporting Standards Interpretations (IFRICs); and
* the Standing Interpretation Committee interpretations (SICs).
*Other Pronouncements. By Mukaila Hadi & Esther Bangase Anankware
Of these, the IASs and SICs are previously-developed standards and interpretations that have been adopted by the IASB and IFRS Interpretations Committee respectively.<ref name=ifrss>IFRS Foundation, 2012. [http://www.ifrs.org/IFRSs/IFRS.htm Access the unaccompanied standards and their technical summaries]. Retrieved on April 25, 2012.</ref> The IFRSs are developed and published by the IASB, the 15-member standard-setting body of the IFRS Foundation, while the IFRICs are provided by the IFRS Interpretations Committee.<ref name=about/>

Via the IASB, the IFRS Foundation also sets out the IFRS for [[Small and medium enterprises|small and medium-sized entities (SMEs)]] to better meet the needs of SMEs and relieve the burden imposed on them by the full IFRSs.<ref>IFRS Foundation, 2012. [http://www.ifrs.org/IFRS+for+SMEs/histroy/History.htm Project history]. Retrieved on April 26, 2012.</ref> At a 2012 panel discussion co-sponsored by the [[American Institute of Certified Public Accountants]] and the Institute of Chartered Accountants of Scotland, [[David Tweedie|Sir David Tweedie]] said that the IFRS for SMEs "has been a howling success" and that 70 million businesses are using it globally, although other panelists expressed doubts about its ability to solve problems in certain areas.<ref name=mc>Cohn M, 2012. [http://www.accountingtoday.com/news/Former-FASB-IASB-Accounting-Standards-SEC-IFRS-62419-1.html Former Accounting Board Chairs Urge SEC to Commit to IFRS]. Retrieved on April 26, 2012.</ref>

===IFRS Taxonomy===
The IFRS Foundation also develops and maintains the IFRS Taxonomy, which is the representation of the IFRSs in [[XBRL|eXtensible Business Reporting Language (XBRL)]], via its XBRL team. The team is supported by the XBRL Advisory Council and the XBRL Quality Review Team, which respectively provide strategic advice and reviews developed taxonomies.<ref name=xbrl>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/About+XBRL/About+XBRL.htm The IFRS XBRL initiative]. Retrieved on April 26, 2012.</ref> Additionally, in 2012 the foundation issued a call for industry participants in a project to develop "common industry practice concepts" for the taxonomy.<ref>inAudit, 2012. [http://inaudit.com/events/xbrl-industry-practice-project-2012-call-for-participants-17298/ XBRL Industry Practice Project 2012 – Call for Participants]. Retrieved on April 26, 2012.</ref>

XBRL provides a "common, electronic format for business and financial reporting",<ref name=xbrland>IFRS Foundation, 2012. [http://www.ifrs.org/XBRL/XBRL.htm XBRL and IFRSs]. Retrieved on April 26, 2012.</ref> which will contribute to the global convergence of accounting standards towards IFRS;<ref name=kr>Ramin K, 2008. [http://www.cpa2biz.com/Content/media/PRODUCER_CONTENT/Newsletters/Articles_2008/CorpFin/Hands.jsp XBRL and IFRS Joining Hands]. Retrieved on April 26, 2012.</ref> the director of XBRL activities at the IFRS Foundation, Olivier Servais, hopes that "everybody will be using it" in future.<ref name=director>Institute of Certified Public Accountants of Singapore, 2010. [http://www.icpas.org.sg/mediacentre/Article.aspx?artid=288 Director of XBRL Activities, IFRS Foundation to Speak at Singapore Accountancy Convention 2011]. Retrieved on April 26, 2012.</ref> As of March 2012, the IFRS Taxonomies have "considerably fewer" tags than [[Generally accepted accounting principles|GAAP]] taxonomies, and the [[Security and Exchange Commission]] has not approved the IFRS Taxonomy for use in XBRL filings in the United States.<ref name=secapprove>Whitehouse T, 2012. [http://www.complianceweek.com/sec-approves-2012-gaap-taxonomy-for-xbrl-filings/article/234354/ SEC Approves 2012 GAAP Taxonomy for XBRL Filings]. Retrieved on April 26, 2012.</ref>

==Organization and governance==
The IFRS Foundation's executive director is Yael Almog,<ref name=staff>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/Senior+staff.htm Senior staff]. Retrieved on April 26, 2012.</ref> who is also the Director of the Department of International Affairs of the [[Israel Securities Authority]],<ref name=ya>IFRS Foundation, 2012. [http://www.ifrs.org/Alerts/PressRelease/Yael+Almog+Jan+2012.htm Yael Almog appointed as Executive Director of the IFRS Foundation]. Retrieved on April 26, 2012.</ref> and is funded in part by country-specific funding regimes involving [[Project stakeholder|stakeholder]] groups, or levies and other contributions through regulatory authorities.<ref name=financing>IFRS Foundation, 2012.[http://www.ifrs.org/The+organisation/Governance+and+accountability/Financing/Financing.htm Financing]. Retrieved on April 26, 2012.</ref>

The foundation  is governed by a board of 22 [[trustees]],<ref name=deloitte/> including
* [[Michel Prada]] (chairman), also co-Chairman of the Council on Global Financial Regulation;<ref>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/Trustees/Trustee+Chairman.htm Trustee Chairman]. Retrieved on April 29, 2012.</ref>
* Tsuguoki Fujinuma (vice-chairman), also Professor at [[Chuo University]] and Board member of the [[Tokyo Stock Exchange]] Group Inc.;<ref>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/Trustees/Trustee+Vice+Chairman+1.htm Trustee Vice-Chair]. Retrieved on April 29, 2012.</ref>
* [[Robert R. Glauber|Robert Glauber]] (vice-chairman), former Chairman and Chief Executive Officer of the [[Financial Industry Regulatory Authority]];<ref>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/Trustees/Trustee+Vice+Chairman+2.htm Trustee Vice-Chair]. Retrieved on April 29, 2012.</ref> and
* [[Ronald Arculli]], also chairman of [[Hong Kong Exchanges and Clearing]] Limited, and the [[World Federation of Exchanges]].<ref name=trustees>IFRS Foundation, 2012. [http://www.ifrs.org/The+organisation/Trustees/Trustees.htm Trustees]. Retrieved on April 29, 2012.</ref>
The trustees' responsibilities include appointing members to and establishing the operating procedures of the IASB, interpretations committee and advisory council, and approving the foundation's budget. They are accountable to a monitoring board of public authorities, and their effectiveness is assessed by the Trustees’ Due Process Oversight Committee.<ref name=trustee>IFRS Foundation, 2012.[http://www.ifrs.org/The+organisation/Trustees/Trustee+responsibilities/Trustee+responsibilities.htm Trustee responsibilities]. Retrieved on April 26, 2012.</ref>

==References==
{{reflist|2}}

==External links==
*{{official website|http://www.ifrs.org}}

{{International Financial Reporting Standards}}

[[Category:International accounting organizations]]
[[Category:International Financial Reporting Standards]]
[[Category:International Accounting Standards Board]]
[[Category:Non-profit organizations based in New York City]]
[[Category:Standards organizations]]
[[Category:Organizations with year of establishment missing]]