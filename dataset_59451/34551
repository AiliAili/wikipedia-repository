{{EngvarB|date=May 2014}}
{{Good article}}
{{Infobox banknote
| country             = [[Eurozone]] and [[Institutions of the European Union|Institutions]]
| denomination        = Five [[euro]]
| value               = 5
| unit                = [[euro]]
| Colour              = Grey<ref name="Security">{{cite web|url=http://www.ecb.int/euro/banknotes/security/html/index.en.html |title=ECB: Security Features|publisher=ECB|work=ECB}}</ref>
| width_mm            = 120
| height_mm           = 62
| security_features   = First series: [[Security hologram|hologram]] stripe with [[perforation]]s, reflective glossy stripe, [[EURion constellation]], [[watermark]]s, raised printing, [[microprinting]], [[Invisible ink#Inks visible under ultraviolet light|ultraviolet ink]], [[security thread]], matted surface, see-through number, [[barcode]]s and [[serial number]]<ref name="Interactive security features" /><br />
Europa series: portrait watermark, portrait hologram, emerald number<ref name="Security features Europa">{{cite web|url=http://www.ecb.europa.eu/euro/banknotes/security/html/index.en.html|title=ECB: Security features|accessdate=9 July 2015|work=European Central Bank|publisher=ecb.int}}</ref>
| paper_type          = [[Cotton]] [[fibre]]<ref name="Interactive security features">{{cite web|url=http://www.ecb.int/euro/html/security_features.en.html |title=ECB: Security Features|accessdate=22 October 2011|work=European Central Bank|publisher=ecb.int|year=2002}}</ref>
| years_of_printing   = 1999 - 2011 (1st series)<ref name="Introduction" /><br> Since 2012 (Europa series)<ref>{{cite news | url=http://www.rte.ie/news/business/2013/0430/388965-new-five-euro-note/ | title=New €5 note to be circulated from Thursday - RTÉ News | work=Raidió Teilifís Éireann | date=30 April 2013 | agency=RTÉ | accessdate=7 August 2013}}</ref>
| obverse             = EUR 5 obverse (2013 issue).png
| obverse_design      = Arch in [[Classical architecture]]<ref name="Banknotes index" />
| obverse_designer    = [[Reinhold Gerstetter]]<ref name="Europa series design">{{cite web | url=http://www.new-euro-banknotes.eu/Europa-Series/Europa-Series-Design | title=Europa series design - ECB - Our Money | publisher=www.new-euro-banknotes.eu | accessdate=6 August 2013 | year=2013}}</ref>
| obverse_design_date = 10 January 2013<ref name="Europa series ECB" />
| reverse             = EUR 5 reverse (2013 issue).png
| reverse_design      = Bridge in [[Classical architecture]] and map of [[Europe]]<ref name="Banknotes index" />
| reverse_designer    = Reinhold Gerstetter<ref name="Europa series design" />
| reverse_design_date = 10 January 2013<ref name="Europa series ECB" />
}}

The '''five [[euro]] note (€5)''' is the lowest value [[euro banknote]] and has been used since the introduction of the euro (in its cash form) in 2002.<ref>{{cite news|url=http://pqasb.pqarchiver.com/smgpubs/access/97637858.html?dids=97637858:97637858&FMT=ABS&FMTS=ABS:FT&type=current&date=Jan+01%2C+2002&author=Alf+Young%3B+on+Tuesday&pub=The+Herald&desc=Witnessing+a+milestone+in+European+history&pqatl=google |title=Witnessing a milestone in European history|publisher=Back Issue|work=The Herald|date=1 January 2002|accessdate=23 October 2011}}</ref> 
The note is used in the 25 countries which have it as their sole currency (with 23 legally adopting it); with a population of about 332&nbsp;million.<ref>
* {{cite web
 |url=http://www.eurocoins.co.uk/andorra.html 
 |title=Andorran Euro Coins 
 |year=2003 
 |accessdate=15 October 2011 
 |publisher=Eurocoins.co.uk 
 |work=Eurocoins.co.uk 
}}
* {{cite web
 |url=http://www.unmikonline.org/regulations/admdirect/1999/089%20Final%20%20ADE%201999-02.htm 
 |title=By UNMIK administration direction 1999/2 
 |publisher=Unmikonline.org 
 |date=4 October 1999 
 |accessdate=30 May 2010 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110607234444/http://www.unmikonline.org/regulations/admdirect/1999/089%20Final%20%20ADE%201999-02.htm 
 |archivedate=7 June 2011 
 |df= 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2002:142:0059:0073:EN:PDF 
 |title=By monetary agreement between France (acting for the EC) and Monaco 
 |date=31 May 2002 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:C:2001:209:0001:0004:EN:PDF 
 |title=By monetary agreement between Italy (acting for the EC) and San Marino 
 |date=27 July 2001 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:C:2001:299:0001:0004:EN:PDF 
 |title=By monetary agreement between Italy (acting for the EC) and Vatican City 
 |date=25 October 2001 
 |accessdate=30 May 2010 
}}
* {{cite web
 |url=http://www.ecb.europa.eu/euro/intro/html/map.en.html 
 |title=ECB: Map of euro area 1999 – 2011 
 |date=1 January 2011 
 |accessdate=27 October 2011 
 |work=ECB 
 |publisher=ecb.int 
}}
* {{cite web
 |url=http://epp.eurostat.ec.europa.eu/tgm/table.do?tab=table&language=en&pcode=tps00001&tableSelection=1&footnotes=yes&labeling=labels&plugin=1 
 |title=Total population as of 1 January 
 |publisher=Epp.eurostat.ec.europa.eu 
 |date=2011-03-11 
 |accessdate=2011-07-17 
}}
* {{cite web
 |url=http://www.ecb.int/euro/banknotes/europa/html/index.en.html 
 |title=The "Europa" series of banknotes 
 |publisher=ECB 
 |date=2013-01-10 
 |accessdate=2013-01-13 
}}
</ref>

Measuring 120 x 62mm, it is the smallest of the euro notes, and has a grey colour scheme.<ref name="Banknotes index">{{cite web|url=http://www.ecb.int/euro/banknotes/html/index.en.html |title=ECB: Banknotes|publisher=European Central Bank|work=European Central Bank|year=2002|accessdate=1 January 2013}}</ref> The five euro banknotes depict bridges and arches/doorways in [[Classical architecture]] (up to the fifth century).

The five euro note contains several complex security features such as watermarks, invisible ink, holograms and microprinting that document its authenticity. In November 2014, there were approximately 1,653,458,000 five euro banknotes in circulation around the [[eurozone]].<ref>{{cite web | url=https://www.ecb.europa.eu/stats/euro/circulation/html/index.en.html| title=ECB: Banknotes and coins circulation | publisher=ECB|accessdate=1 January 2015}}</ref>

On 8 November 2012, the European Central Bank announced the first series of notes will be replaced, starting with the 5 euro note on 2 May 2013.<ref name="Banknotes index"/><ref>{{cite web | url=http://banknotenews.com/files/45818fb2453e6395f81f0497809dad74-2423.php | title=European Central Bank unveils new 5-euro note due 02.05.2013 | publisher=Banknote News | date=10 January 2013 | accessdate=10 January 2013}}</ref>

== History ==
{{main article|History of the euro}}
The euro was founded on 1 January 1999, when it became the currency of over 300 million people in Europe.<ref name="Introduction">{{cite web|url=http://www.ecb.int/euro/intro/html/index.en.html |title=ECB: Introduction|publisher=ECB|work=ECB| accessdate= 21 October 2011 <!--Added by DASHBot-->}}</ref> For the first three years of its existence it was an invisible currency, only used in accountancy. Euro cash was not introduced until 1 January 2002, when it replaced the national banknotes and coins of the countries in eurozone 12, such as the [[Irish pound]] and the [[Austrian schilling]].<ref name="Introduction" />

[[Slovenia]] joined the [[Eurozone]] in 2007,<ref>{{cite web|url=http://ec.europa.eu/economy_finance/focuson/focuson9120_en.htm |title=Slovenia joins the euro area - European Commission |publisher=European Commission |date=16 June 2011 |accessdate=6 August 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130911232930/http://ec.europa.eu:80/economy_finance/focuson/focuson9120_en.htm |archivedate=11 September 2013 |df= }}</ref> [[Cyprus]] and [[Malta]] in 2008,<ref>{{cite news | url=http://news.bbc.co.uk/2/hi/europe/7165622.stm | title=Cyprus and Malta adopt the euro - BBC NEWS | work=BBC News | date=1 January 2008 | agency=British Broadcasting Corporation | accessdate=6 August 2013}}</ref> [[Slovakia]] in 2009,<ref>{{cite news | url=http://www.businessweek.com/stories/2008-12-31/slovakia-joins-decade-old-euro-zonebusinessweek-business-news-stock-market-and-financial-advice | title=Slovakia Joins Decade-Old Euro Zone - Businessweek | work=Bloomberg Businessweek | date=31 December 2008 | agency=Bloomberg | accessdate=6 August 2013 | author=Kubosova, Lucia}}</ref> [[Estonia]] in 2011<ref>{{cite news | url=http://www.rte.ie/news/2010/0713/133287-euro/ | title=Estonia to join euro zone in 2011 | work=RTÉ News | date=13 July 2010 | agency=Radió Teilifís Éireann | accessdate=6 August 2013}}</ref> and [[Latvia]] on 1 January 2014.<ref name="Latvia Eurozone">{{cite web | url=https://www.wsj.com/article/SB10001424127887324507404578595440293862344.html | title=Latvia Gets Green Light to Join Euro Zone -WSJ.com | publisher=Wall Street Journal | work=Wall Street Journal | date=9 July 2013 | accessdate=31 July 2013 | author1=Van Tartwijk, Maarten | author2=Kaza, Juris}}</ref>

=== The changeover period ===
The changeover period during which the former currencies' notes and coins were exchanged for those of the euro lasted about two months, going from 1 January 2002 until 28 February 2002. The official date on which the national currencies ceased to be legal tender varied from member state to member state.<ref name="Introduction"/> The earliest date was in Germany, where the mark officially ceased to be legal tender on 31 December 2001, though the exchange period lasted for two months more. Even after the old currencies ceased to be legal tender, they continued to be accepted by national central banks for periods ranging from ten years to forever.<ref name="Introduction" /><ref>{{cite web | url=http://www.centralbank.ie/about-us/Documents/PRESSKIT%20-%2010th%20anniversary%20of%20the%20euro.pdf | title=Press kit - tenth anniversary of the euro banknotes and coins | publisher=Central Bank of Ireland | work=ECB | year=2011 | accessdate=21 August 2012}}</ref>

=== Changes ===
Notes printed before November 2003 bear the signature of the first [[President of the European Central Bank|president]] of the [[European Central Bank]], [[Wim Duisenberg]], who was replaced on 1 November 2003 by [[Jean-Claude Trichet]], whose signature appears on issues from November 2003 to March 2012. Notes issued after March 2012 bear the signature of the third president of the European Central Bank, incumbent [[Mario Draghi]].<ref name="Banknotes index" />

{{multiple image
<!-- Essential parameters -->
| align     = right
| direction = vertical
| width     = 220
| header    = Design for the first series of five euro notes

<!-- Image 1 -->
| image1   = EUR 5 obverse (2002 issue).jpg
| width1   = 220
| alt1     = Former 5 euro note (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = EUR 5 reverse (2002 issue).jpg
| width2   = 220
| alt2     = Former 5 euro note (Reverse)
| caption2 = Reverse
}}
{{multiple image
<!-- Essential parameters -->
| align     = left
| direction = vertical
| width     = 150
| header    = 5 euro banknote under fluorescent light (UV-A) (1st series)

<!-- Image 1 -->
| image1   = 005euro-uv.JPG
| width1   = 135
| alt1     = 5 euro note under UV light (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = 005euro-uv2.JPG
| width2   = 135
| alt2     = 5 euro note under UV light (Reverse)
| caption2 = Reverse
}}
{{multiple image
<!-- Essential parameters -->
| align     = left
| direction = vertical
| width     = 150
| header    = 5 euro banknote under fluorescent light (UV-A) (Europa series)

<!-- Image 1 -->
| image1   = EUR 5 2S UV.jpg
| width1   = 135
| alt1     = 5 euro note under UV light (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = 5 euro UV light back 2S.jpg
| width2   = 135
| alt2     = 5 euro note under UV light (Reverse)
| caption2 = Reverse
}}
Until May 2013 there was only one series of euro notes, however a new series, similar to the first one, is planned to be released.<ref name="Euro series 2">{{cite web | url=http://www.ecb.int/pub/pdf/other/p041-048_mb200508en.pdf | title=ECB Monthly bulletin- August 2005 - THE EURO BANKNOTES: DEVELOPMENTS AND FUTURE CHALLENGES | publisher=ecb.int | work=ECB | date=August 2005 | accessdate=21 August 2012 | quote=p.43, section 'THE SECOND SERIES OF EURO BANKNOTES'}}</ref> The bank notes are going to be replaced in ascending order.<ref>{{cite web | url=http://www.new-euro-banknotes.eu/Europa-Series/Europa-Series-Introduction | title=Euro Series Introduction | accessdate=7 July 2013}}</ref> Therefore, the first new note is the 5 euro note which is in circulation since 2 May 2013. Its new design was made public on 10 January 2013 in the Archaeological Museum of [[Frankfurt]] (Germany).  While broadly similar to the current notes, minor design changes include an updated map and a hologram of [[Europa (mythology)|Europa]].<ref>{{cite news | url=https://www.bloomberg.com/news/2012-08-31/ecb-said-to-use-greek-myth-for-security-on-new-euro-banknotes.html | title=ECB Said to Use Greek Myth for Security on New Euro Notes | work=Bloomberg | date=1 September 2012 | agency=Bloomberg | accessdate=7 August 2013 | author=Randow, Jana}}</ref>
Moreover, the new notes will reflect the expansion of the European Union; the current issues do not include the recent members [[Cyprus]] and [[Malta]] (Cyprus is off the map to the east and Malta was too small to be depicted<ref>{{cite web| url = http://www.ecb.europa.eu/euro/banknotes/html/index.en.html#main| title = The Euro: Banknotes: Design elements| accessdate = 2009-07-05| author = European Central Bank| authorlink = European Central Bank| quote = The banknotes show a geographical representation of Europe. It excludes islands of less than 400 square kilometres because high-volume offset printing does not permit the accurate reproduction of small design elements.}}</ref>). 
It will be the first time when the [[Bulgarian alphabet|Bulgarian Cyrillic alphabet]] is going to be used on the banknotes as a result of [[Bulgaria]] joining the European Union in 2007. Therefore, the new series of Euro banknotes will include "ЕВРО", which is the Bulgarian spelling for EURO as well as the abbreviation  "ЕЦБ" (short for ''Европейска централна банка'' in [[Bulgarian language|Bulgarian]]).<ref>{{cite web | url=http://www.new-euro-banknotes.eu/Euro-banknotes/Compare/Compare-both-5-banknotes/Superimpose/(cur_bn)/171 | title=Superimpose - ECB - Our Money | publisher=Our Money | accessdate=7 August 2013 | year=2013}}</ref>
The European Central Bank will, in due time, announce when banknotes from the first series lose legal tender status.<ref>{{cite web | url=http://www.ecb.europa.eu/pub/pdf/other/10thanniversaryoftheecbmb200806en.pdf | title=MONTHLY BULLETIN - 10th anniversary OF THE ECB | publisher=European Central Bank | accessdate=7 August 2013 | year=2008}}</ref>

== Design ==
[[File:Euro note close 2.jpg|thumb|Close up of the reverse; a map of Europe and a classical era bridge]]
[[File:EUR 5 holographic band.jpg|thumb|Holographic band on the five euro note]]
{{multiple image
<!-- Essential parameters -->
| align     = left
| direction = vertical
| width     = 150
| header    = 5 euro banknote under special ulraviolet light (UV-C) (Europa series)

<!-- Image 1 -->
| image1   = 5 euro 2S front uvc.jpg
| width1   = 135
| alt1     = 5 euro note under UV light (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = 5 euro UV light back 2S.jpg
| width2   = 135
| alt2     = 5 euro note under UV light (Reverse)
| caption2 = Reverse
}}
{{multiple image
<!-- Essential parameters -->
| align     = left
| direction = vertical
| width     = 150
| header    = 5 euro banknote under infrared light (Europa series)

<!-- Image 1 -->
| image1   = 5 euro infrared front 2S.jpg
| width1   = 135
| alt1     = 5 euro note under infrared light (Obverse)
| caption1 = Obverse

<!-- Image 2 -->
| image2   = 5 euro infrared back 2S.jpg
| width2   = 135
| alt2     = 5 euro note under UV light (Reverse)
| caption2 = Reverse
}}

The five euro note is the smallest at {{convert|120|x|62|mm}} with a grey colour scheme.<ref name="Banknotes index"/> All bank notes depict bridges, arches or doorways in a different historical European style; the five euro note shows the [[Classical architecture|Classical era]] (up to the fifth century).<ref>{{cite web | url=http://www.ecb.int/euro/banknotes/html/index.en.html | title=ECB: Banknotes | publisher=ecb.int | work=European Central Bank | year=2002 | accessdate=5 December 2011}}</ref> Although [[Robert Kalina]]'s original designs were intended to show real monuments, for political reasons the bridge and art are merely hypothetical examples of the architectural era.<ref>{{cite news| url=http://news.bbc.co.uk/hi/english/static/slideshow/money_talks/slide2.stm |title=Money talks&nbsp;— the new Euro cash|date=December 1996|accessdate=13 October 2011|work=BBC News|publisher=BBC News}}</ref>

Like all euro notes, it contains the denomination, the [[Flag of Europe|EU flag]], the signature of the president of the [[European Central Bank|ECB]] and the initials of said bank in different [[Languages of the European Union|EU languages]], a depiction of EU territories overseas, the stars from the EU flag and twelve security features as listed below.<ref name="Banknotes index" />

=== Security features (First Series) ===
As a lower value note, the security features of the five euro note are not as high as the other denominations, however, it is protected by: 
* A [[hologram]],<ref name="Interactive security features" /> tilt the note and one should see the hologram image change between the value and a window or doorway, but in the background, one should see rainbow-coloured concentric circles of micro-letters moving from the centre to the edges of the patch.<ref name="Titling">{{cite web|url=http://www.ecb.int/euro/banknotes/security/tilt/html/index.en.html |title=ECB:Tilt|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* A [[EURion constellation]],<ref name="Interactive security features" /> special printing processes give the euro notes their unique feel.
* A glossy stripe,<ref name="Interactive security features" /> tilt the note and a glossy stripe showing the value numeral and the euro symbol will appear.
* [[Watermark]]s,<ref name="Interactive security features" /> it appears when the banknote is against the light.
* Raised printing,<ref name="Interactive security features" /> special methods of printing makes the ink feel raised or thicker in the main image, the lettering and the value numerals on the front of the banknotes. To feel the raised print, run your finger over it or scratch it gently with your fingernail.<ref name="Feel">{{cite web|url=http://www.ecb.int/euro/banknotes/security/feel/html/index.en.html |title=ECB: Feel|work=ECB|publisher=ecb.int|date=1 January 2011|accessdate=22 October 2011}}</ref>
* [[Invisible ink#Inks visible under ultraviolet light|Ultraviolet ink]],<ref name="Interactive security features" /> Under ultraviolet light, the paper itself should not glow, fibres embedded in the paper should appear, and should be coloured red, blue and green, the European Union flag looks green and has orange stars, the ECB President signature turns green, the large stars and small circles on the front glow and the European map, a bridge and the value numeral on the back appear in yellow.<ref name="Additional Features">{{cite web|url=http://www.ecb.int/euro/banknotes/security/additional/html/index.en.html |title=ECB: Additional features|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* Microprinting,<ref name="Interactive security features" /> On numerous areas of the banknotes you can see microprinting, for example, inside the "ΕΥΡΩ" (EURO in Greek characters) on the front. You will need a magnifying glass to see it. The tiny text is sharp, and not blurred.<ref name="Additional Features" />
* A security thread,<ref name="Interactive security features" /> The security thread is embedded in the banknote paper. Hold the banknote against the light - the thread will appear as a dark stripe. The word "EURO" and the value can be seen in tiny letters on the stripe.<ref name="Look">{{cite web|url=http://www.ecb.int/euro/banknotes/security/look/html/index.en.html |title=ECB: Look|work=ECB|publisher=ecb.int|date=1 January 2002|accessdate=22 October 2011}}</ref>
* Perforations,<ref name="Interactive security features" /> Hold the banknote against the light. You should see perforations in the hologram which will form the € symbol. You should also see small numbers showing the value.<ref name="Look" />
* A matted surface,<ref name="Interactive security features" /> the note paper is made out of pure cotton, which feels crisp and firm, but not limp or waxy.<ref name="Feel" />
* Barcodes,<ref name="Interactive security features" />
* A serial number.<ref name="Interactive security features" />

=== Security Features (Europa Series) ===
[[File:Microprint New 5 Euro 03.jpg|thumb|Microprinting on the 5 euro note of the Europa series]]
* '''Watermark''': When the note is held under a normal light source, a portrait of Europa and an electrotype denomination appear on either side.<ref name="Europa series ECB">{{cite web | url=http://www.ecb.int/euro/banknotes/europa/html/index.en.html | title=ECB: Europa series | publisher=ECB | work=ECB | accessdate=24 June 2013 | year=2013}}</ref><ref name="Europa series Our money">{{cite web | url=http://www.new-euro-banknotes.eu/Euro-banknotes/Security-features/FEEL/THE-NEW-5 | title=THE NEW €5 - ECB - Our Money | publisher=Our Money | work=ECB | accessdate=24 June 2013 | year=2013}}</ref>
* '''Portrait Hologram''': When the note is tilted, the silver-coloured holographic stripe reveals the portrait of Europa-the same one as in the watermark. The stripe also reveals a window and the value of the banknote.<ref name="Europa series ECB" /><ref name="Europa series Our money" />
* '''Emerald Number''': When the note is tilted, the number on the note displays an effect of light that moves up and down. The number also changes color from emerald green to deep blue.<ref name="Europa series ECB" /><ref name="Europa series Our money" />
* '''Raised Printing''': On the front of the note, there is a series of short raised lines on the left and right edges. The main edge, the lettering and the large value numeral also feel thicker.<ref name="Europa series ECB" /><ref name="Europa series Our money" />
* '''Security Thread''': When the note is held against the light, the security thread appears as a dark line. The Euro symbol (€) and the value of the banknote can be seen in tiny white lettering in the thread.<ref name="Europa series ECB" /><ref name="Europa series Our money" />
* '''Microprint''': Tiny letters which can be read with a magnifying glass. The letters should be sharp, not blurred.<ref name="Europa series Our money" />
* '''Ultraviolet ink''': Some parts of the banknote shine when under UV or UV-C light. These are the stars in the flag, the small circles, the large stars and several other areas on the front. On the back, a quarter of a circle in the centre as well as several other areas glow green. The horizontal serial number and a stripe appear in red.<ref name="Europa series Our money" />
* '''Infrared light''': Under infrared light, the emerald number, the right side of the main image and the silvery stripe are visible on the obverse of the banknote, while on the reverse, only the denomination and the horizontal serial number are visible.<ref name="Europa series Our money" />

== Circulation ==

The European Central Bank is closely monitoring the circulation and stock of the euro coins and banknotes. It is a task of the Eurosystem to ensure an efficient and smooth supply of euro notes and to maintain their integrity throughout the euro area.<ref name="Circulation" />

On January 2017, there are {{formatnum:1761487885}} €5 banknotes in circulation around the Eurozone.<ref name="Circulation">{{cite web|url=http://www.ecb.int/stats/euro/circulation/html/index.en.html |title=ECB: Circulation|work=ECB|publisher=European Central Bank}}</ref> That is €{{formatnum:8807439425}} worth of €5 banknotes.

This is a net number, i.e. the number of banknotes issued by the Eurosystem central banks, without further distinction as to who is holding the currency issued, thus also including the stocks held by credit institutions.

Besides the date of the introduction of the first set to January 2002, the publication of figures is more significant through the maximum number of banknotes raised each year. The number is higher the end of the year, except for this €5 banknote in 2002.

The figures are as follows (March 14, 2017) :

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="40%"
|-align="center"
!scope="row"| Key date
| Banknotes 
| Amount
|-align="center"
!scope="row"| January 2002
| {{formatnum:1919890327}}
| {{formatnum:9599451635}}
|-align="center"
!scope="row"| December 2003
| {{formatnum:1218288843}}
| {{formatnum:6091444215}}
|-align="center"
!scope="row"| December 2004
| {{formatnum:1246528720}}
| {{formatnum:6232643600}}
|-align="center"
!scope="row"| December 2005
| {{formatnum:1284662576}}
| {{formatnum:6423312880}}
|-align="center"
!scope="row"| December 2006
| {{formatnum:1345643994}}
| {{formatnum:6728219970}}
|-align="center"
!scope="row"| December 2007
| {{formatnum:1421089850}}
| {{formatnum:7105449250}}
|-align="center"
!scope="row"| December 2008
| {{formatnum:1475610499}}
| {{formatnum:7378052495}}
|-align="center"
!scope="row"| December 2009
| {{formatnum:1497585692}}
| {{formatnum:7487928460}}
|-align="center"
!scope="row"| December 2010
| {{formatnum:1522271959}}
| {{formatnum:7611359795}}
|-align="center"
!scope="row"| December 2011
| {{formatnum:1545677368}}
| {{formatnum:7728386840}}
|-align="center"
!scope="row"| December 2012
| {{formatnum:1613104679}}
| {{formatnum:8065523395}}
|}

On May 2013, a new 'Europe' series was issued. The first series of notes were issued in conjunction with those for a few weeks in the series 'Europe' until existing stocks are exhausted, then gradually withdrawn from circulation. Both series thus run parallel but the proportion tends inevitably to a sharp decrease in the first series.

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="80%"
|-align="center"
!scope="row"| Key date
| Banknotes 
| Amount
| Series '1' remainder
| Amount
| Proportion
|-align="center"
!scope="row"| December 2013
| {{formatnum:1672391858}}
| {{formatnum:8361959290}}
| {{formatnum:829305109}}
| {{formatnum:4146525545}}
| 49,6 %
|-align="center"
!scope="row"| December 2014
| {{formatnum:1715872011}}
| {{formatnum:8579360055}}
| {{formatnum:500770403}}
| {{formatnum:2503852015}}
| 29,2 %
|-align="center"
!scope="row"| December 2015
| {{formatnum:1766164560}}
| {{formatnum:8830822800}}
| {{formatnum:397807951}}
| {{formatnum:1989039755}}
| 22,5 %
|-align="center"
!scope="row"| December 2016
| {{formatnum:1805152448}}
| {{formatnum:9025762240}}
| {{formatnum:342245848}}
| {{formatnum:1711229240}}
| 19,0 %
|}

The latest figures provided by the ECB are the following :

{|border="1" cellspacing="0" cellpadding="2" class="wikitable" style="border-collapse:collapse" width="80%"
|-align="center"
!scope="row"| key date
| Banknotes 
| Amount
| Series '1' remainder
| Amount
| Proportion
|-align="center"
!scope="row"| February 2017
| {{formatnum:1757167269}}
| {{formatnum:8785836345}}
| {{formatnum:339073478}}
| {{formatnum:1695367390}}
| 19,3 %
|}

== Legal information ==
Legally, both the European Central Bank and the central banks of the [[eurozone]] countries have the right to issue the 7 different euro banknotes. In practice, only the national central banks of the zone physically issue and withdraw euro banknotes. The European Central Bank does not have a cash office and is not involved in any cash operations.<ref name="Introduction" />

== Tracking ==
There are several communities of people at European level, most of which is [[EuroBillTracker]],<ref name="EuroBillTracker About">{{cite web|url=http://en.eurobilltracker.com/about/ |title=EuroBillTracker - About this site|date=1 January 2002|accessdate=21 October 2011|work=Philippe Girolami, Anssi Johansson, Marko Schilde|publisher=EuroBillTracker}}</ref> that, as a hobby, it keeps track of the euro banknotes that pass through their hands, to keep track and know where they travel or have travelled.<ref name="EuroBillTracker About"/> The aim is to record as many notes as possible to know details about its spread, like from where and to where they travel in general, follow it up, like where a ticket has been seen in particular, and generate statistics and rankings, for example, in which countries there are more tickets.<ref name="EuroBillTracker About"/> EuroBillTracker has registered over 155 million notes as of May 2016,<ref name="EuroBilltracker Statistics">{{cite web |url=http://en.eurobilltracker.com/stats/ |title=EuroBillTracker - Statistics|date=1 January 2002|accessdate=21 October 2011|work=Philippe Girolami, Anssi Johansson, Marko Schilde |publisher=EuroBillTracker}}</ref> worth more than €2.897 billion.<ref name="EuroBilltracker Statistics"/>

== References ==
{{reflist|30em}}

== External links ==
* {{Commonscat-inline|5 euro banknotes}}

{{Euro topics}}

[[Category:Euro banknotes]]
[[Category:Five-base-unit banknotes]]