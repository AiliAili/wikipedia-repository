{{featured article}}
{{italic title}}
[[File:Amazing stories quarterly 1928win.jpg|thumb|The first issue of ''Amazing Stories Quarterly'', dated Winter 1928.  The cover art is by [[Frank R. Paul]].<ref name=":1"/>]]
'''''Amazing Stories Quarterly''''' was a U.S. [[science fiction]] [[pulp magazine]] published from 1928 to 1934.  It was launched by [[Hugo Gernsback]] as a companion to his ''[[Amazing Stories]]'', the first [[science fiction magazine]], which had begun publishing in April 1926.  ''Amazing Stories'' had been successful enough for Gernsback to try a single issue of ''Amazing Stories Annual'' in 1927, which had sold well, and he decided to follow it up with a quarterly magazine.  The first issue of ''Amazing Stories Quarterly'' was dated Winter 1928 and carried a reprint of [[H. G. Wells|H.G. Wells]]' ''[[When The Sleeper Wakes|When the Sleeper Wakes]]''.  Gernsback's policy of running a novel in each issue was popular with his readership, though the choice of Wells' novel was less so.  Over the next five issues only one more reprint appeared: Gernsback's own novel ''[[Ralph 124C 41+]]'', in the Winter 1929 issue.  Gernsback went bankrupt in early 1929, and lost control of both ''Amazing Stories'' and ''Amazing Stories Quarterly''; his assistant, [[T. O'Conor Sloane]], took over as editor.  The magazine began to run into financial difficulties in 1932, and the schedule became irregular; the last issue was dated Fall 1934.

Authors whose work appeared in ''Amazing Stories Quarterly'' include [[Stanton A. Coblentz]], [[Miles J. Breuer]], [[Alpheus Hyatt Verrill|A. Hyatt Verrill]], and [[Jack Williamson]].  Critical opinions differ on the quality of the fiction Gernsback and Sloane printed: [[Brian Stableford]] regards several of the novels as being important early science fiction, but [[E. F. Bleiler|Everett Bleiler]] comments that few of the stories were of acceptable quality.  Milton Wolf and [[Mike Ashley (writer)|Mike Ashley]] are more positive in their assessment; they consider the work Sloane published in the early 1930s to be some of the best in the new genre.

== Publication history ==
{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-left: 2em; text-align: center; float: right"
! !!Winter !! Spring !!Summer !!Fall !! Winter
|-
!1928
| bgcolor=#ccffff|1/1 || bgcolor=#ccffff|1/2 || bgcolor=#ccffff|1/3 || bgcolor=#ccffff|1/4 ||
|-
!style="width:10%;" |1929
|style="width:18%;" bgcolor=#ccffff|2/1 || style="width:18%;" bgcolor=#ccffff|2/2 || style="width:18%;" bgcolor=#ffff99|2/3 || style="width:18%;" bgcolor=#ffff99|2/4 ||
|-
!1930
| bgcolor=#ffff99|3/1 || bgcolor=#ffff99|3/2 || bgcolor=#ffff99|3/3 || bgcolor=#ffff99|3/4 ||
|-
!1931
| || bgcolor=#ffff99|4/1 || bgcolor=#ffff99|4/2 || bgcolor=#ffff99|4/3 || bgcolor=#ffff99|4/4
|-
!1932
| bgcolor=#ffff99|5/1 || colspan="2" bgcolor=#ffff99|5/2 || colspan="2" bgcolor=#ffff99|5/3
|-
!1933
| || colspan="2" bgcolor=#ffff99|6/4 || || bgcolor=#ffff99|7/1
|-
!1934
| || || || bgcolor=#ffff99|7/2 || 
|-
|colspan="13" style=" width:100%; font-size: 8pt; text-align:left"|Issues of ''Amazing Stories Quarterly'' from 1928 to 1934, showing issue numbers, and<br />indicating editors: Gernsback (blue, first six issues), and Sloane (yellow,<br />remaining sixteen issues).  Note that the apparent error in numbering starting<br />in 1933 is in fact shown correctly.<ref name=":0">Wolf & Ashley (1985), pp. 51–57.</ref>
|}Although science fiction (sf) had been published before the 1920s, it did not begin to coalesce into a separately marketed genre until the appearance in 1926 of ''[[Amazing Stories]]'', a U.S. [[pulp magazine]] published by [[Hugo Gernsback]].<ref>Edwards & Nicholls (1993), pp.&nbsp;1066–1067.</ref>  The new magazine was successful, and in 1927 Gernsback brought out a double-sized ''Amazing Stories Annual'', which also sold well.  These successes convinced him to start a companion magazine to ''Amazing Stories'', titled ''Amazing Stories Quarterly''.<ref>Ashley (2000), p. 54.</ref>  The first issue, dated Winter 1928, appeared on newsstands on January 5 that year.<ref>Ashley (2004), p. 267.</ref>

Gernsback went bankrupt in early 1929, and lost control of both ''Amazing Stories'' and ''Amazing Stories Quarterly''.  After a short period in receivership, they were acquired by Bergan Mackinnon, who sold them on to [[Bernarr Macfadden]]'s Teck Publications.  [[T. O'Conor Sloane]], who had worked on both magazines for Gernsback, took over as editor.  In 1932 the magazine, which was probably never very profitable, began to suffer from financial problems, and the quarterly schedule became irregular after the Winter 1932 issue.  The last two issues were filled completely with reprints from early issues and from ''Amazing Stories''.  The last issue was dated Fall 1934, though the decision to discontinue the magazine was not taken until some time later, as an editorial comment in the May 1935 issue of ''Amazing Stories'' mentioned that further issues might still appear.<ref name=":1" /><ref name=":0" />

== Contents ==
[[File:Amazing stories quarterly 1930win.jpg|thumb|left|The Winter 1930 issue of ''Amazing Stories Quarterly''.  The cover art is by [[Wesso]].<ref name=":1">Bleiler & Bleiler (1998), pp. 561–564.</ref>]]The first issue of ''Amazing Stories Quarterly'' contained a reprint of [[H. G. Wells]]' novel ''When the Sleeper Wakes'', though for some reason Wells did not provide Gernsback with the revised text published in 1910 under the title ''[[The Sleeper Awakes]]''; the text printed was that of the original 1899 edition.<ref name=":1" />  The other material in the issue was original, and the following issues included material by [[Edmond Hamilton]], [[Stanton A. Coblentz]], [[Roman Frederick Starzl|R.F. Starzl]], [[David H. Keller]], [[S. P. Meek|S.P. Meek]], [[Joseph Schlossel|J. Schlossel]], and [[Clare Winger Harris]], one of the earliest women writers of sf.<ref name=":0" /><ref>{{Cite web|url=http://sf-encyclopedia.com/entry/harris_clare_winger|title=Authors : Harris, Clare Winger : SFE : Science Fiction Encyclopedia|last=Donawerth|first=Jane|website=sf-encyclopedia.com|access-date=2016-05-28}}</ref>  Although readers' reactions to the Wells novel were negative, they approved of Gernsback's policy of publishing a novel in each issue.  The only other reprint in the early days of the magazine was Gernsback's own novel ''[[Ralph 124C 41+]]'', which appeared in the Winter 1929 issue.<ref name=":1" /><ref name=":0" />  The novel, set in the year 2660, was little more than a series of predictions about the future tied together by a minor plot.<ref>Bleiler & Bleiler (1998), p. 146.</ref>  Gernsback included a letter column, and began a competition for the best editorials submitted by readers; the first prize was awarded to [[Jack Williamson]], later to become a successful science fiction writer but at that time just starting his career.  Gernsback also started other departments to engage the readers, including book reviews, science quizzes, and science news.  The last issue under Gernsback's control was dated Spring 1929;<ref name=":1" /><ref name=":0" /> under Sloane's editorship, most of these nonfiction departments ceased.<ref name=":1" />

According to Milton Wolf and [[Mike Ashley (writer)|Mike Ashley]], historians of science fiction, over the next two years Sloane published some of the best sf of the early years of the field in ''Amazing Stories Quarterly''.  Wolf and Ashley cite "Paradox", by [[Charles Cloukey]], an early time-travel story; ''[[The Bridge of Light]]'', by [[Alpheus Hyatt Verrill|A. Hyatt Verrill]], a novel about a lost civilization in South America; ''The Birth of a New Republic'', by [[Miles J. Breuer]] and Jack Williamson, in which a man of the 24th century reminisces about a revolt by the inhabitants of the Moon rebel against the Earth; "Paradise and Iron", by Breuer; and ''White Lily'', by [[Eric Temple Bell]], under the pseudonym John Taine, about a form of crystal life that endangers the planet.<ref name=":1" /><ref name=":0" />  After 1931, according to Wolf and Ashley, the fiction in ''Amazing Stories Quarterly'' became less entertaining.<ref name=":0" />  [[E. F. Bleiler|Everett Bleiler]], the author of a detailed review of the first ten years of science fiction magazines, is less complimentary, describing [[John W. Campbell|John W. Campbell, Jr.]]'s [[Space opera|space operas]], which appeared from 1930 to 1932, as "turgid", and commenting that only a dozen or so of the stories in the magazine's entire run "might have been considered worth reading if one could put oneself back in the 1930s, accepting the standards of the time".<ref name=":1" /><ref name=":0" />  Bleiler mentions three authors, Coblentz, Taine, and Breuer, as having produced notably original material, but adds that their work was "not strong enough for mainstream fiction" and had "too little action and too much sophistication for pulp".  Bleiler does however agree with Wolf and Ashley that the magazines's quality declined over time.<ref name=":1" />  [[Brian Stableford]], in the ''[[The Encyclopedia of Science Fiction|Science Fiction Encyclopedia]]'', also highlights Coblentz, Taine and Breuer, along with Williamson and Verrill, among the magazine's contributors; Stableford regards their contributions as being among "the most important early pulp sf novels".<ref>{{Cite web|url=http://sf-encyclopedia.com/entry/amazing_stories_quarterly|title=Culture : Amazing Stories Quarterly : SFE : Science Fiction Encyclopedia|last=Brian|first=Stableford|website=sf-encyclopedia.com|access-date=2016-05-28}}</ref>

== Bibliographic details ==
''Amazing Stories Quarterly'' was published by Hugo Gernsback's Experimenter Publishing until Spring 1929.  A single issue appeared from Irving Trust, the trustee in Gernsback's bankruptcy; then four issues, from Fall 1929 to Summer 1930, again under the Experimenter Publishing imprint, and then four more from Radio-Science Publications.  The last ten issues, from Fall 1931 to Fall 1934, were published by Teck Publishing, of [[Washington, D.C.|Washington]] and [[Dunellen, New Jersey|Dunellen]].  The magazine was in large pulp format throughout, and was 144 pages long, except for the last two issues, which were 128 pages.  It was priced at 50 cents.  The first six issues were edited by Hugo Gernsback; from the Summer 1929 issue on, the editor was T. O'Conor Sloane.<ref name=":0" />  There was a Canadian reprint of a single issue, Fall 1934.<ref>{{Cite web|url=http://www.philsp.com/mags/amazing_stories.html#canadian|title=Amazing Stories|last=Stephensen-Payne|first=Phil|website=www.philsp.com|access-date=2016-06-09}}</ref>

Another 27 issues of ''Amazing Stories Quarterly'' appeared from Ziff-Davis from 1940 to 1943, and also from 1949 to 1951, but these were not original magazines, only rebound issues of ''Amazing Stories''.<ref name=":0" />

== Footnotes ==
{{reflist|40em}}

== References ==
* {{cite book|title=The Time Machines: The Story of the Science-Fiction Pulp Magazines from the Beginning to 1950|last=Ashley|first=Mike|publisher=Liverpool University Press|year=2000|isbn=0-85323-865-0|location=Liverpool}}
* {{cite book|title=The Gernsback Days: A Study of the Evolution of Modern Science Fiction From 1911 to 1936|last=Ashley|first=Mike|publisher=Wildside|year=2004|isbn=0-8095-1055-3|editor-last=Ashley|editor-first=Mike|location=Holicong, Pennsylvania|chapter=The Gernsback Days|editor-last2=Lowndes|editor-first2=Robert A.W.}}
*{{Cite book|title = Science-Fiction: The Gernsback Years|publisher = Kent State University Press|year = 1998|isbn = 0-87338-604-3|location = Westport CT|last = Bleiler|first = Everett F.|last2 = Bleiler|first2 = Richard J.}}
* {{cite book|title=The Encyclopedia of Science Fiction|last=Edwards|first=Malcolm|last2=Nicholls|first2=Peter|publisher=St. Martin's Press, Inc.|year=1993|isbn=0-312-09618-6|editor-last=Clute|editor-first=John|location=New York|pages=1066–1068|chapter=SF Magazines|editor2-last=Nicholls|editor2-first=Peter}}
* {{cite book|title=Science Fiction, Fantasy and Weird Fiction Magazines|last=Wolfe|first=Milton|last2=Ashley|first2=Mike|publisher=Greenwood Press|year=1985|isbn=0-313-21221-X|editor-last=Tymn|editor-first=Marshall B.|location=Westport, Connecticut|pages=51–57.|chapter=''Amazing Stories Quarterly''|editor2-last=Ashley|editor2-first=Mike}}

{{ScienceFictionFantasyWeirdPulpMagazines}}

[[Category:Pulp magazines]]
[[Category:Defunct science fiction magazines of the United States]]
[[Category:Magazines established in 1928]]
[[Category:Magazines disestablished in 1949]]
[[Category:Magazines published in New York]]
[[Category:Science fiction magazines established in the 1920s]]