{{redirect|HARV|the American basketball player and coach|Harv Schmidt|the character from Cars|List of Cars characters#Harv}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name=F-18 High Alpha Research Vehicle (HARV)
  |image=FA18 LEX.jpg
  |caption=F-18 HARV in high alpha maneuver
}}{{Infobox aircraft type
  |type=Test aircraft
  |manufacturer=[[McDonnell Douglas]]
  |designer=
  |first flight= 
  |introduction=
  |retired=
  |number built=1
  |status=
  |primary user= [[NASA]]
  |more users=
  |unit cost=
  |developed from=[[McDonnell Douglas F/A-18 Hornet]]
  |variants with their own articles=
}}
|}

The '''High Alpha ([[angle of attack]]) Research Vehicle''' was an American modified [[McDonnell Douglas F/A-18 Hornet]] used by [[NASA]] in a 3-phase program investigating controlled flight at high angles of attack using [[thrust vectoring]], modifications to the flight controls, and with actuated forebody [[Strake (aviation)|strakes]]. The program lasted from April 1987 to September 1996.<ref name="Jenkins">{{cite book | last = Jenkins | first = Dennis R. | authorlink =  | coauthors =  | title = F/A-18 Hornet: A Navy Success Story | publisher = McGraw-Hill |date= 2000 | location = New York | url =  | doi =  | isbn =  0-07-134696-1 }}</ref><ref>[http://www.nasa.gov/centers/dryden/news/FactSheets/FS-002-DFRC.html F-18 High Alpha Research Vehicle (HARV) fact sheet], NASA/Dryden Flight Research Center.</ref>

[[NASA]] reported that in one phase of the project, [[Armstrong Flight Research Center]] "research pilots [[William H. "Bill" Dana]] and Ed Schneider completed the envelope expansion flights in February 1992. Demonstrated capabilities included stable flight at approximately 70 degrees angle of attack (previous maximum was 55 degrees) and rolling at high rates at 65 degrees angle of attack. Controlled rolling would have been nearly impossible above 35 degrees without vectoring."<ref>[http://www.nasa.gov/centers/dryden/history/pastprojects/HARV/index.html NASA Past Projects: F-18 High Alpha Research Vehicle], NASA</ref> Performance figures were not listed for other phases.

==See also==
{{aircontent
|related=
*[[F/A-18 Hornet]]

|see also=
*[[List of experimental aircraft]]
*[[List of military aircraft of the United States]]
}}
{{commons category|High Alpha Research Vehicle}}

==References==
<references />

==External links==
*[http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19970012895_1997021250.pdf Overview of HATP Experimental Aerodynamics Data for the Baseline F/A-18 Configuration], NASA, September 1996.
*[http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19950007843_1995107843.pdf Preparations for flight research to evaluate actuated forebody strakes on the F-18 high-alpha research vehicle], NASA,  July 1994.

[[Category:McDonnell Douglas aircraft|HARV]]
[[Category:NASA aircraft]]
[[Category:United States experimental aircraft 1980–1989]]