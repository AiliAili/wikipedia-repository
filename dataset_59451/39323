{{good article}}
{{Infobox military conflict
| conflict   = Capture of Le Quesnoy
| partof     = the [[Western Front (World War I)|Western Front]] of [[World War I]] <br> [[Hundred Days Offensive]] : [[Battle of the Sambre (1918)]]
| image      = [[File:Walls of Le Quesnoy, 1918.jpg|300px]]
| caption    = The walls of Le Quesnoy, scaled by New Zealand troops when taking the town from German forces on 4 November 1918
| date       = 4 November 1918
| place      = [[Le Quesnoy]], [[Nord-Pas-de-Calais]], [[France]]
|coordinates = {{Coord|50|14|59|N|03|38|18|E|type:event_region:FR|display=inline,title}}
|map_type=France
|longitude = 3.6383
|latitude = 50.2497
|map_size=200
|map_caption=<center>'''Le Quesnoy''' is a [[Communes of France|commune]] in the [[Nord (French department)|Nord]] [[Departments of France|department]] in northern [[France]]</center>
| result     = New Zealand victory
| combatant1 = {{flag|New Zealand}}
| combatant2  = {{flag|German Empire}}
| commander1  = {{flagicon|New Zealand}} [[Andrew Hamilton Russell|Andrew Russell]]
| commander2  = 
| units1       = [[New Zealand Division]]
*[[New Zealand Rifle Brigade (Earl of Liverpool's Own)|New Zealand Rifle Brigade]]
| units2 = Unknown
| strength1   = 
| strength2   = 1,537 (town garrison)
| casualties1 = 122 killed<br/>375 wounded
| casualties2 = 43 killed<br>251 wounded<br/>2,000 prisoners of war
| campaignbox = {{Campaignbox Hundred Days 1918}}{{Campaignbox Western Front (World War I)}}
}}

The '''Capture of Le Quesnoy''' was an engagement of the [[First World War]] that took place on 4 November 1918 as part of the [[Battle of the Sambre (1918)|Battle of the Sambre]].{{sfn|James|1924|p=40}} Elements of the [[New Zealand Division]] [[Escalade|scaled]] the fortified walls of the French town of [[Le Quesnoy]] and captured it from the defending German garrison

Beginning at 5:30 am, the [[New Zealand Rifle Brigade (Earl of Liverpool's Own)|New Zealand Rifle Brigade]] advanced from its starting positions east of the town, aiming to surround it and link up on the far side.  By late morning, the linkup had been achieved and other elements of the New Zealand Division moved further west into the [[Forêt de Mormal|Mormal Forest]], leaving the Rifle Brigade to capture the town itself. After mopping up outlying outposts, the New Zealanders moved up to the ramparts of the town, but were held back by machine-gun fire. Late in the afternoon, a scouting party located an unguarded section of the walls and the brigade's 4th Battalion managed to climb the ramparts and move into the town, quickly seizing it. The capture of Le Quesnoy was the last major engagement of the war for the New Zealanders.

==Prelude==
By mid-1918, the German Army had been fought to a standstill after its [[Spring Offensive]] and the [[Allies of World War I|Allies]] had sought to take the initiative. Accordingly, the [[Hundred Days Offensive]] began on 8 August, with an [[Battle of Amiens (1918)|attack on Amiens]] which marked the beginning of a series of advances by the Allies that ultimately ended the war.{{sfn|Wright|2005|pp=143–144}} By late October, the [[New Zealand Division]], commanded by Major General [[Andrew Hamilton Russell|Andrew Russell]], along with part of the British [[Third Army (United Kingdom)|Third Army]], had advanced to the west of the town of [[Le Quesnoy]].{{sfn|Wright|2010|p=283}}

The [[Battle of the Sambre (1918)|Battle of the Sambre]], which was planned to begin on 4 November, was the next phase of the Allied advance. The battle was to consist of a series of engagements mounted by the British [[First Army (United Kingdom)|First]], Third and [[Fourth Army (United Kingdom)|Fourth Armies]] across a {{convert|30|mi|km|adj=on}} front, extending from Oisy to Valenciennes, that were conceptualised to cut off the German line of retreat from the French Army front. IV Corps, with the New Zealand Division and the 37th Division, was to surround Le Quesnoy{{sfn|Wyrall|1928|p=127}} and its garrison of over 1,500 soldiers.{{sfn|Edmonds|Maxwell-Hyslop|1947|p=483}} The 37th Division was on the southern flank of the New Zealand Division while to its north, 62nd Division, of VI Corps, moved south to shorten the New Zealand front.{{sfn|Wyrall|1928|p=127}} The New Zealand Division was to extend the front line to and around Le Quesnoy and into the [[Forêt de Mormal|Mormal Forest]].{{sfn|Edmonds|Maxwell-Hyslop|1947|pp=480–481}}{{sfn|Austin|1924|pp=435–436}}

===Le Quesnoy===
Positioned on high ground between the Ecaillon and Rhonelle Rivers, Le Quesnoy was a medieval town that had been fought over several times in previous centuries. It guarded a natural approach across plains to the north-east and had fortress walls with ramparts designed by [[Sébastien Le Prestre de Vauban]], a 17th-century [[Military engineering|military engineer]].{{sfn|Stewart|1921|pp=564–565}} A moat surrounded the town and was in fact two distinct ditches, with {{convert|20|–|30|ft|m|adj=on}} high fortifications, effectively an outer rampart, separating them.{{sfn|Stewart|1921|p=584}} The town could be entered by three roads, guarded by gates. Le Quesnoy had a population of 5,000 and had been in German hands since August 1914.{{sfn|Stewart|1921|pp=564–565}} The Germans also held the Cambrai railway line to the west of Le Quesnoy, and had a strong presence in the area around the intersection of the Valenciennes–Cambrai railway lines, immediately to the north-west of the town.{{sfn|Gray|2010|pp=351–354}}

===Plan of attack===
On 3 November, the New Zealand Division section of the front line was around {{convert|2500|yd|m}} in length, running southwards from a level crossing on the Valenciennes railway line. It was {{convert|400|yd|m}} from the Cambrai railway, with the ramparts of Le Quesnoy a further {{convert|400|yd|m}} to the east. The front line was manned by the four [[battalion]]s of the [[New Zealand Rifle Brigade (Earl of Liverpool's Own)|New Zealand Rifle Brigade]], commanded by Brigadier General [[Herbert Ernest Hart|Herbert Hart]].{{sfn|Gray|2010|p=363}}

The ramparts of Le Quesnoy clearly made a frontal attack undesirable and [[Field artillery|artillery]] could not be used on the town, due to the presence of the civilian population. Instead, it was intended that under the cover of a smokescreen, the town be enveloped from the north and south, thereby encircling it. Two New Zealand [[brigade]]s were to be involved; Hart's Rifle Brigade was tasked with the capture of the town, while the [[1st Infantry Brigade (New Zealand)|1st Infantry Brigade]], under the command of Brigadier General [[Charles Melvill]], was to push into the Mormal Forest. The division's flanks were held by the 62nd Division and the 37th Division, on the left and right respectively and these formations were to make corresponding movements forward.{{sfn|Stewart|1921|pp=570–571}}

[[File:Plan of attack, Le Quesnoy 1918.jpg|thumb|<center>Plan of attack on Le Quesnoy, 4 November 1918. Published in ''The New Zealand Division 1916–1918'' by Col. H. Stewart, 1921.</center>]]
The capture of Le Quesnoy was to be achieved through a series of advances, covered by artillery, by the battalions of the Rifle Brigade with some of the battalions of 1st Infantry Brigade in support. Beginning from the brigade's existing positions, the first advance was to involve the 1st, 2nd and 4th Rifle Battalions moving forward to a line defining an arc to the west of Le Quesnoy, including the railway line, which was designated the "Blue Line". Then the 1st Battalion of the 1st Infantry Brigade would push north-east around the town, while the 3rd Rifle Battalion went to the south-east.  The advance westwards would culminate in the establishment of a new front line, designated the "Green Line" to the east of Le Quesnoy, which would be manned by the battalions of 1st Brigade. Once the "Green Line" had been formed, the Rifle Brigade was to move into the town, while the battalions of 1st Brigade were to advance further to the west up to the Mormal Forest.{{sfn|Stewart|1921|pp=570–571}}

==Battle==
The covering artillery barrage commenced at {{nowrap|5:30 am}} and three battalions of the Rifle Brigade moved off towards its first objective, the railway line, which established a continuous front west of Le Quesnoy. This was captured by {{nowrap|7:29 am.}} A reserve [[Company (military unit)|company]] moved to the railway line to hold it, while the attacking battalions moved forward. A [[platoon]] of the reserve company had to deal with 150 Germans, who were retreating from the advance of the flanking 37th Division and quickly secured their surrender.{{sfn|Stewart|1921|pp=573–575}} By {{nowrap|10:00 am,}} the battalions of the Rifle Brigade had surrounded Le Quesnoy and established a new front line {{convert|1|mi|km|adj=on}} to the east of the town. The 1st Infantry Brigade moved off to the Mormal Forest, leaving the Rifle Brigade to complete its planned move into the town. The German garrison, despite being surrounded, did not make any indications of surrender.{{sfn|Gray|2010|p=366}}

The 2nd Rifle Battalion probed from the north, while the 3rd Rifle Battalion did the same from the south-east, driving for the Landrecies road which led to one of the entry points into Le Quesnoy. German troops held a bridge on the road in force and were able to keep the New Zealanders at bay in this area. In the north, a small party reached the outer rampart dividing the moat along their stretch of the front. Gunfire from the main ramparts soon drove them off but Lieutenant Colonel Leonard Jardine, commanding the 2nd Rifle Battalion, co-ordinated the movements of his companies, which gradually moved forward. By {{nowrap|4:00 pm}} [[Mortar (weapon)|mortar]] fire was able to be brought to bear on the main ramparts and this silenced the German machine-guns.{{sfn|Stewart|1921|pp=585–586}}

[[File:NZ soldiers at the front near Le Quesnoy, 1918.jpg|thumb|left|<center>Members of the New Zealand Rifle Brigade operating a mortar at the front near Le Quesnoy, 1918</center>]]
In the meantime, the 4th Rifle Battalion, commanded by Lieutenant Colonel [[Harold Barrowclough]] and positioned a distance from the west wall of the ramparts, had carried out scouting expeditions to explore the defences. Gradually, the German posts around the fortifications fell to the attacking platoons of the battalion. As on the northern side of the town, machine-gun fire from the ramparts prevented any further advance. One party reached the outer rampart but became pinned down by gunfire for several hours. By midday, the situation had settled into a temporary stalemate.{{sfn|Gray|2010|pp=367–368}} In the afternoon, some German prisoners of war were sent into the town with an invitation to surrender but this approach was rebuffed, as had been a similar attempt earlier that morning.{{sfn|Austin|1924|pp=454–455}}

Second Lieutenant [[Leslie Cecil Lloyd Averill|Leslie Averill]], the intelligence officer for 4th Rifle Battalion, continued to investigate the defences. He was able to locate a route to a section of the ramparts that appeared unmanned and was not under fire from the defenders.  He was ordered by Barrowclough to force an approach. With the benefit of covering mortar fire, Averill and a platoon of the battalion reserve company, managed to cross the moat and found themselves at the inner ramparts. With the aid of a {{convert|30|ft|m|adj=on}} ladder supplied by the Divisional Engineers, Averill was able to ascend to the top of the ramparts, closely followed by the platoon commander. With his revolver, Averill fired at two Germans manning a guard post, forcing them to cover, and the rest of the platoon joined him. Shortly afterwards, Barrowclough and the rest of the battalion used the ladder and entered the town.{{sfn|Gray|2010|pp=367–368}}  At the same time, a party from 2nd Rifle Battalion, seized the gate guarding the road into Le Quesnoy from Valenciennes and began entering the town from the north and the Germans quickly surrendered.{{sfn|Gray|2010|pp=367–368}}{{sfn|Austin|1924|p=463}}

==Aftermath==
[[File:George Edmund Butler -The scaling of the walls of Le Quesnoy.jpg|thumb|<center>A painting depicting the scaling of the walls of Le Quesnoy by Second Lieutenant Averill, executed in 1920 by [[George Edmund Butler]], who was an official artist of the New Zealand Expeditionary Force.</center>]]
Over 2,000 Germans were taken prisoner by the division, of whom 711 surrendered in Le Quesnoy.{{sfn|Wright|2010|p=285}}{{sfn|Austin|1924|pp=464–465}} German casualties in the town were 43 killed and 251 men wounded and many more German troops were killed during the advance of the brigade to the ramparts. Four 8-inch [[howitzer]]s, forty-two 4.2-inch guns and 26 [[field gun]]s were captured by the 1st New Zealand Brigade.{{sfn|Edmonds|Maxwell-Hyslop|1947|p=483}} The New Zealand Division operation on 4 November was its most successful day on the Western Front.{{sfn|McGibbon|2000|p=609}} Of the 122 New Zealanders who died during the capture of Le Quesnoy, the Rifle Brigade suffered 43 killed and 250 men wounded.{{sfn|Austin|1924|p=465}} Other units of the New Zealand Division involved in the battle lost 79 men killed and about 125 wounded.{{sfn|Gray|2010|pp=367–368}}

An advance into the Mormal Forest was continued the next day by the [[2nd Infantry Brigade (New Zealand)|2nd Infantry Brigade]] but the capture of Le Quesnoy was the last major engagement of the war for the New Zealand Division. The New Zealanders began withdrawing to the rear area at midnight on 5 November.{{sfn|Gray|2010|pp=372–373}} A few days after the capture of the town, the mayor of Le Quesnoy presented the Rifle Brigade commander, Herbert Hart, with the French flag that was raised over the town on the day it was captured from the Germans.{{sfn|Crawford|2008|p=267}}

The town retains links to New Zealand, with some streets named for prominent New Zealanders, including Averill. Since 1999, it has been twinned with [[Cambridge, New Zealand|Cambridge]].  A monument commemorating the liberation of Le Quesnoy by the New Zealand Division is set into the rampart wall, near where Averill scaled them.{{sfn|McGibbon|2001|p=70}}

==Notes==
{{Reflist|3}}

==References==
{{refbegin}}
* {{cite book|last=Austin|first=Lieut.-Col W. S.|title=The Official History of the New Zealand Rifle Brigade|year=1924|publisher=L. T. Watkins|location=Wellington, New Zealand|url=http://nzetc.victoria.ac.nz//tm/scholarly/tei-WH1-NZRi.html|accessdate=1 September 2014|oclc=220312361|ref=harv}}
* {{cite book|editor-last=Crawford|editor-first=John|title=The Devil's Own War: The First World War Diary of Brigadier-General Herbert Hart|year=2008|publisher=Exisle Publishing|location=Auckland, New Zealand|isbn=978-1-877437-30-4|ref=harv}}
* {{cite book |series=History of the Great War Based on Official Documents by Direction of the Historical Section of the Committee of Imperial Defence |title=Military Operations France and Belgium 1918: 26th September – 11th November, The Advance to Victory |volume=V |last1=Edmonds |first1=J. E. |last2=Maxwell-Hyslop |first2=R. |authorlink1=James Edward Edmonds |year=1947 |publisher=[[HMSO]] |location=London |edition=IWM & Battery Press 1993 |isbn=0-89839-192-X|ref=harv}}
* {{cite book|last=Gray|first=John H.|title=From the Uttermost Ends of the Earth: The New Zealand Division on the Western Front 1916–1918|year=2010|publisher=Wilson Scott Publishing|location=Christchurch, New Zealand|isbn=978-1-877427-30-5|ref=harv}}
* {{cite book|last=James|first=Captain E. A.|title=A Record of the Battles and Engagements of the British Armies in France and Flanders, 1914–1918|year=1924|publisher=Gale & Polden|location=Aldershot, United Kingdom|oclc=6794231|ref=harv}}
* {{cite book|editor-last=McGibbon|editor-first=Ian|title=The Oxford Companion to New Zealand Military History|year=2000|publisher=Oxford University Press|location=Auckland, New Zealand|isbn=0-19-558376-0|ref=harv}}
* {{cite book|last=McGibbon|first=Ian|title=New Zealand Battlefields and Memorials of the Western Front|year=2001|publisher=Oxford University Press|location=Auckland, New Zealand|isbn=0-19-558444-9|ref=harv}}
* {{cite book|last=Stewart|first=H.|authorlink=Hugh Stewart (classical scholar)|title=The New Zealand Division 1916–1919|series=[[Official History of New Zealand's Effort in the Great War]]|year=1921|publisher=Whitcombe & Tombs|location=Auckland, New Zealand |url=http://nzetc.victoria.ac.nz//tm/scholarly/tei-WH1-Fran.html|accessdate=1 September 2014|oclc=2276057 |ref=harv}}
* {{cite book|last=Wright|first=Matthew|title=Western Front: The New Zealand Division in the First World War 1916–18|year=2005|publisher=Reed Books|location=Auckland, New Zealand|isbn=0-7900-0990-0|ref=harv}}
* {{cite book|last=Wright|first=Matthew|title=Shattered Glory: The New Zealand Experience at Gallipoli and the Western Front|year=2010|publisher=Penguin Books|location=Auckland, New Zealand|isbn=978-0-14-302056-1|ref=harv}}
* {{cite book |last=Wyrall |first=E. |title=The Story of the 62nd (West Riding) Division, 1914–1919 |volume=II |year=1928 |publisher=The Bodley Head |location=London |edition=Naval & Military Press 2003 |isbn=1-84342-582-3|ref=harv}}
{{refend}}

==External links==
{{Commons category|Capture of Le Quesnoy (1918)}}
{{Portal|World War I}}

* [http://www.teara.govt.nz/en/artwork/34135/capture-of-the-walls-of-le-quesnoy NZ Encyclopaedia, The Walls of Le Quesnoy]
* [http://www.cambridgelequesnoy.co.nz/Orders.html Map used by the NZ Division for the attack]

{{World War I}}

[[Category:Conflicts in 1918]]
[[Category:1918 in France]]
[[Category:Battles of the Western Front (World War I)]]
[[Category:Battles of World War I involving New Zealand]]
[[Category:Battles of World War I involving Germany]]
[[Category:November 1918 events]]