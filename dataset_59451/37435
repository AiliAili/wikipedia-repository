{{Infobox military conflict
| conflict    = Battle of Marion
| partof      = The [[American Civil War]]
| image       = [[File:VAMap-doton-Marion.PNG|300px]]
| caption     = Location of the battle.
| date        = {{Start date|1864|12|17}} &ndash; {{End date|1864|12|18}}
| place       = [[Smyth County, Virginia]]
| casus       = 
| territory   = Southwest Virginia
| result      = [[Union (American Civil War)|Union]] victory<ref name=NPS/>
| combatant1  = {{flagicon|USA|1863}} [[United States]] [[Union (American Civil War)|Union]]
| combatant2  = {{flagicon|CSA|1863}} [[Confederate States of America|Confederacy (CSA)]]
| commander1  = [[George Stoneman]]
| commander2  = [[John C. Breckinridge]]
| strength1   = 4,200&ndash;5,500<ref name= Marvel129>Marvel, p. 129.</ref>
| strength2   = 1,200&ndash;1,500<ref name=Marvel129/>
| casualties1 = 89 killed <br/> 55 wounded<ref name=Marvel129/>
| casualties2 = 91 killed <br/> 29 wounded<ref name=Marvel129/>
| campaignbox = {{Campaignbox Stoneman's Raid into Southwest Virginia}}
}}

The '''Battle of Marion''' (December 17&ndash;18, 1864)<ref>Official Records, p. 437.</ref> was a military engagement fought between units of the [[Union Army]] and the [[Confederate States Army|Confederate Army]] during the [[American Civil War]] near the town of [[Marion, Virginia]]. The battle was part of Union [[Major general (United States)|Maj. Gen.]] [[George Stoneman]]'s attack upon southwest [[Virginia in the American Civil War|Virginia]], aimed at destroying Confederate industrial infrastructure near [[Saltville, Virginia|Saltville]] and Marion.  Union Cavalry and Infantry regiments&mdash;some 4,500 soldiers in total&mdash;left [[Tennessee in the American Civil War|Tennessee]] on December 17 for southwestern Virginia.<ref name=USWD442>Official Records, p. 442.</ref>

Through two days of fighting, a Confederate force under the command of [[John C. Breckinridge]]&mdash;totalling 1,200&ndash;1,500 infantry and cavalry&mdash;was successful in holding defensive positions in and around the town of Marion.<ref name= Mosgrove/><ref name=Foote/>  On the first day, successive Union attacks were defeated by a well-coordinated Confederate defenses near a covered bridge outside of Marion.<ref name=Foote>Foote, pp. 721&ndash;723.</ref>  By the end of the second day, dwindling ammunition supplies forced Confederate forces to withdraw from the area.  With casualties for both sides approaching 300,<ref name= Guerrent599>Guerrent, p. 599.</ref> Union forces proceeded to destroy the salt mines, lead works, and other beneficial Confederate infrastructure in Marion and Saltville.<ref>Marvel, p. 128.</ref>

==Background==
By 1864, the [[American Civil War]] was slowly drawing to a close.  With [[Abraham Lincoln]] re-elected as President of the Union, and Gen. [[Ulysses Grant]] made commander of the Union Army, the possibility of a Confederate victory was steadily lessened.<ref name=McPherson>McPherson p. 778.</ref> Along the Eastern Seaboard, Union forces pushed the Confederate forces of Gen. [[Robert E. Lee]] steadily back in successive Union victories at [[Battle of the Wilderness|Wilderness]] and [[Battle of Spotsylvania Court House|Spotsylvania]].  In the [[Appalachian Mountains|Appalachian]] mountains, [[Phillip Sheridan]] had defeated Confederate armies in the [[Shenandoah valley]].<ref name=McPherson/>  As Union forces pushed southward, they destroyed significant portions of the Confederate agriculture base.  As Union forces defeated Confederate armies in the northern reaches of the CSA, Gen. [[William T. Sherman]] began his [[Sherman's march to the sea|march to the sea]], which would eventually succeed in destroying 20% of the agricultural production in Georgia.<ref name=McPherson/>

As Union forces advanced south, the infrastructure near the town of [[Marion, Virginia|Marion]]&mdash;located in Southwest Virginia on the Middle Fork of the [[Holston River]],<ref name=NPS>{{cite web |url=http://www.nps.gov/hps/abpp/battles/va081.htm|title=NPS Battle Summary|publisher=CWSAC}}</ref><ref name=chaltas>{{cite web |url=http://web.archive.org/web/20160322180029/http://www.bencaudill.com/documents_msc/battle_of_marion.html |title=The Battle of Marion |last=Chaltas & Brown |accessdate=2008-04-28}}</ref> between Saltville and Wytheville&mdash;became a major objective of Union forces.<ref name=chaltas/> Marion itself was politically divided, with citizens fighting for the Union and the Confederacy.<ref name =McKnight223>McKnight, p. 223.</ref> Until the winter of 1864, the town's location in a mountainous region had protected it from major fighting.<ref>Guerrent, p. 600.</ref> In November 1864, [[George Stoneman]]&mdash;deputy commander of the [[Department of the Ohio]] and in charge of all Union cavalry units in eastern Tennessee&mdash;proposed an expedition into southwest Virginia to disrupt the production of supplies and facilities beneficial to the Confederacy.<ref name=chaltas/> This gained the approval of Maj. Gen. [[John Schofield]] on December 6, 1864.<ref name="USWD442" />

[[File:General George Stoneman.jpg|thumb|left|George Stoneman]]

==Troops==
The Union forces consisted of about 4,500 men from a variety of different units, including several units which had participated in smaller-scale raids into Southwest Virginia earlier in the conflict. The Union army was under the command of Maj. Gen. [[George Stoneman]], Brig. Gen. [[Alvan Cullem Gillem|Alvan Gillem]], and Brig. Gen. [[Stephen Burbridge]].  The majority of the forces that would have been stationed at Marion had been transferred to the [[Army of Northern Virginia]].<ref name=Marvel130>Marvel, p. 130.</ref>  The heavily scaled-down Confederate forces consisted of approximately 1,500 men, under the overall command of Maj. Gen. [[John C. Breckinridge]] and Brig. Gen. [[Basil Duke]].<ref name=Marvel132>Marvel, p. 132</ref>

===Union Forces===
Stoneman used troops under [[Brigadier general (United States)|Brig. Gens.]] [[Alvan Cullem Gillem|Alvan Gillem]] and [[Stephen Burbridge]],<ref name= Giltner>Henry Giltner.</ref> including the 5th and the 6th [[5th United States Colored Cavalry|U.S. Colored Cavalry]] Regiments&mdash;both of which had participated in the previous attempt to destroy the salt works during the [[Battle of Saltville I|First Battle of Saltville]]. Stoneman ordered Burbridge to bring his division of 4,200 cavalrymen through the [[Cumberland Gap]] to join Stoneman and Gillem at [[Knoxville, Tennessee]],<ref name=Marvel123>Marvel p. 123.</ref> where Gillem was refitting his own command into a picked force of 1,500 men.<ref name=McKnight222>McKnight, p. 222.</ref> Stoneman did not reveal the objectives of the expedition to his subordinates until three days after they had departed Knoxville on December 10.<ref name=Marvel130/> On December 12, Stoneman's force flanked and forced back Confederate Brig. Gen. Basil W. Duke's cavalry at [[Rogersville, Tennessee]].<ref>Guerrent, p. 601.</ref> Union forces defeated and scattered Confederate troops the next day at [[Kingsport, Tennessee]]. There Gillem captured 84 prisoners, including Col. Richard C. Morgan and the brigade's supply train.<ref name="USWD442"/>
[[File:John C. Breckinridge by E. F. Andrews.jpg|thumb|right|upright|John C. Breckinridge as brigadier general]]

On December 14, the Union regiments began to push Duke's cavalry back toward [[Abingdon, Virginia]]. The next day, Stoneman and his cavalry went into camp at [[Glade Spring, Virginia]], which was approximately {{convert|13|mi|km}} west of Marion. On December 16, Stoneman's cavalry advanced towards Marion, destroying infrastructure and public buildings in their path.<ref>Guerrent, p. 602.</ref>

===Confederate Forces===
The Confederate forces were under command of Maj. Gen. [[John C. Breckinridge]]&mdash;former Vice-President of the United States, and also candidate for U.S. President in 1860&mdash;the commander of the Department of Southwest Virginia. His command consisted of approximately 1,000 regular troops with another 500 militia in reserve.<ref name= Weaver>Weaver.</ref> Most of the companies had been transferred to the [[Army of Northern Virginia]] to help in the defense of [[Richmond, Virginia|Richmond]].<ref name=chaltas/> Breckinridge's forces consisted of Colonel Henry Giltner's brigade&mdash;formed from the soldiers of the 4th Kentucky Cavalry and the 10th Kentucky Cavalry Battalions&mdash;the 11th Kentucky Mounted Rifles&mdash;later renamed the 13th Kentucky Cavalry Battalion&mdash;and the [[64th Virginia Mounted Infantry]].<ref name=Marvel130/> It also included Basil Duke's cavalry, Brig. Gen. [[George B. Cosby|George Cosby's]] cavalry, and Col. Vincent Witcher's 34th Battalion of Virginia Cavalry.<ref name="USWD442"/>

On the night of December 18, Breckinridge and his troops moved out of Saltville, Virginia, in an effort to stem Stoneman's advance.<ref>Guerrent, p. 603</ref> Taking the regular troops with him, Breckinridge left Col. Robert Preston in charge of the 500 militia men to defend the salt works.<ref name= Marvel123/> Breckinridge sent Witcher and his men of the 34th on ahead of the main force and ordered them to harass the Union forces.<ref>McKnight, p. 224.</ref> At about 3 a.m., Breckinridge and his small company began to cross Walkers Mountain. In the last few days before the march, {{convert|4|in|cm}} of rain had fallen, leaving the roads muddy and travel difficult. By about 4 a.m., they reached the main road near [[Seven Mile Ford, Virginia]] where Breckinridge halted to wait for daylight before continuing.<ref name=chaltas/>

==Battle==

===Advance===
[[File:Stonemans advance.jpg|thumb|right|Stoneman's advance toward [[Marion, Virginia]].]]
[[File:Marion_Battlefield_Virginia.jpg|305px|thumb|right|Map of Marion Battlefield core and study areas by the [[American Battlefield Protection Program]].]]

Around noon of December 17, 1864, Breckinridge's men mounted their horses and rode towards Marion.<ref name=chaltas/> Meanwhile, Stoneman sent some of his Tennessee regiments to Wytheville to destroy anything that looked valuable.<ref name=McKnight225/> Stoneman also sent two regiments of cavalry to destroy the lead mines and smelting facilities that were located about {{convert|10|mi|km}} from Wytheville.<ref name= Mosgrove>Mosgrove, p. 48.</ref>

Stoneman and Burbridge continued on toward Marion where they encountered Witcher and his men.<ref name=McKnight225/> Burbridge's front regiment easily pushed back Witcher's small regiment, who stopped just so they could fire a volley into the Union cavalry.<ref name="USWD442"/> They then continued to retreat toward Marion. Witcher sent a courier to inform Breckinridge that they were coming to join them at Marion.<ref>Guerrent, p. 604.</ref>

===First day===
[[File:Stoneman-position-BOM.jpg|thumb|left|The hill where General Stoneman positioned his army]]

Breckinridge's front regiment was the 10th Kentucky Mounted Rifles, under the command of Col. Benjamin Caudill.<ref name="USWD442"/> Caudill's men dismounted and fired into the Union cavalry, inflicting minor casualties.<ref name="USWD442"/> As the rest of Breckinridge's troops began to arrive on the scene, Stoneman's soldiers secured elevated positions overlooking the river.<ref name=chaltas/> Breckinridge observed that these hills were the best defensive positions in the area, ordering his front regiments to eliminate Union resistance on the hills.<ref>McKnight, p. 226.</ref> The rest of Giltner's Brigade also joined in the charge, routing the Union soldiers and allowing the Confederate forces to use the defensive positions themselves.<ref name=chaltas/>

Upon losing the heightened positions, Burbridge ordered his own forces to counterattack the Confederate positions.<ref>Official Records, p. 439.</ref>  When the Union regiments advanced on the hills, Confederate infantry and cavalry inflicted heavy casualties, slowing Burbridge's progress.<ref>Marvel, p. 124.</ref> As Union forces continued to attack the hill, Maj. Richard Page&mdash;commander of the Confederate artillery squadrons at Marion&mdash;fired his battery of 10-pounder [[Parrott rifle]]s, in an attempt to slow the Union charge.  Taking heavy casualties, and facing heavy fire from all sides, Burbridge's front regiments withdrew.<ref name=" Weaver" />

The Union officers, refusing to withdraw, reorganized their regiments and resumed the attack.  As with the previous charge, the Confederate line held, repelling what remained of the Union regiments.<ref name=" Weaver"/>  After repelling a final charge, Confederate forces had succeeded in holding their elevated positions throughout the first day of combat.  Throughout the night, Breckinridge ordered his forces to move forward and construct new barricades to receive the next day's attacks.<ref name="USWD442"/>  These new positions placed the opposing armies within {{convert|150|yards|m}} of one another.  In the lull between the fighting, elements of the Union forces were ordered to take up positions at a covered bridge on the river.<ref name=McKnight225>McKnight, p. 225.</ref>  With 75 men advancing to positions near the bridge,<ref name="USWD442"/> both sides prepared to resume combat the following day.<ref name=Marvel132/>

[[File:Harpersfield (Ashtabula County, Ohio) Covered Bridge 1.jpg|right|thumb|A covered bridge of the type where the Union Forces were positioned]]

===Second day===
At dawn, Union forces positioned at the covered bridge opened fire, harassing the Confederate forward positions.  As the morning's fog lifted, Burbridge's regiments attacked.<ref name=chaltas/>  Columns of Union soldiers moved across the fields, subjected to heavy defensive fire from Breckinridge's Confederate forces.  As the day progressed, a combination of Union regiments succeeded in pushing back the 4th Kentucky Infantry Regiment.  Confederate counterattacks, however, succeeded in recapturing the [[Breastwork (fortification)|breastwork]] positions.<ref name=Giltner/>

As the counterattack progressed, Union forces at the covered bridge took increasing pressure from the 4th Kentucky Regiment.<ref name=Marvel130/>  Realizing that the location was unprotected, the remaining Union forces attempted to withdraw to the starting lines.<ref name=McKnight222/> Confederate forces&mdash;now stationed near the covered bridge&mdash;exacted heavy casualties on retreating forces.<ref name=McKnight222/> The few Union soldiers who remained at the bridge&mdash;now caught between multiple Confederate regiments&mdash;refrained from attacking.  When Union forces attempted to break through to the bridge, Confederate forces inflicted further casualties, forcing the attack to withdraw.<ref name=McKnight222/>

[[File:Cumberland Gap foggy.jpg|thumb|left|Cumberland Gap.]]

On the far right, Duke was pressed hard by columns of attacking Union soldiers. Seeing this, Col. Giltner sent his regiment to reinforce Duke. Before Giltner's reinforcements arrived, Duke and his men counterattacked the Union line&mdash;routing it and forcing a withdrawal.<ref name=McKnight223/> Duke and Witcher then combined forces and charged the Union's extreme left flank, inflicting significant damage on a Union colored regiment.<ref name=chaltas/>

Having taken heavy casualties and losing strategic superiority, Burbridge and his men conducted a disorganized withdrawal.<ref name=Marvel130/> The Confederates had succeeded in holding the rail breastworks, yet had expended most of their ammunition in doing so.<ref name=McKnight222/> Each Confederate infantryman had fired at least seventy-five rounds, with some firing significantly more.<ref name=Marvel130/> The Union commanders then ordered another charge with a cavalry regiment that reinforced the Union infantry. The unexpected fighting capabilities of the small Confederate force had temporarily created a reprieve for the salt works.<ref name="USWD442"/>

==Aftermath and significance==
Breckinridge ordered his field officers to inspect the troops and to report back with the condition of his troops. The number of men wounded and killed had depleted his troops to a point that he judged that he could no longer hold back the Union forces at his front lines.<ref name=chaltas/> Ammunition in the camp was also dwindling; each man had no more than ten cartridges apiece.<ref name=McKnight222/> With their supplies destroyed by Stoneman's troops at the towns of Wytheville and Abingdon, there was little hope of being resupplied or reinforced in the near future.<ref name=chaltas/>

Although the vastly outnumbered Confederates had inflicted casualties and slowed the Union advance on Saltville, they were incapable of halting it. Finding their own path to Saltville's defenses blocked, Breckinridge and his men retreated further south, while a Union company advanced.<ref name=McKnight223/> Saltville fell to a night attack on December 20&ndash;21 and the salt works were destroyed by the Union forces.<ref name=Marvel132/>

Salt had always been in short supply in Virginia and after the destruction of the salt-mines became "practically nonexistent", giving Lee's [[sutler]]s "no means of preserving what little meat they could lay hands on&nbsp;... for the hungry men in the trenches outside Petersburg and Richmond".<ref name=Foote/> Additionally, damage to the lead mines near Wytheville would keep them from contributing fully to the war effort for three months.<ref name=McKnight222/>  Many wells and water sources were also fouled,<ref name=chaltas/> leading to the disruption of water supplies.<ref name=Foote/> Many of the railroad locomotives, cars, depots, and bridges in the vicinity were destroyed beyond repair during Stoneman's campaign.<ref name=NPS/> In a memoir, Stoneman wrote that his troops captured 34 officers and 845 enlisted men during the attack into Marion, Virginia.<ref name=chaltas/>

==Notes==
{{reflist|2}}

==References==
{{refbegin}}
* {{cite book|last=Foote|first=Shelby|authorlink= Shelby Foote|title=[[The Civil War: A Narrative (Vol. 3) Red River to Appomattox]]|publisher=Pimlico (UK)|year=1992|isbn = 978-0-7126-9812-2}}
* {{cite web |url=http://home.cinci.rr.com/secondtennessee/giltner.html |title=Henry Giltner's Report on the Battle of Marion |accessdate=2008-04-28 |archiveurl = https://web.archive.org/web/20070912085713/http://home.cinci.rr.com/secondtennessee/giltner.html |archivedate = September 12, 2007}}
* {{cite book|last=Guerrent|first=Edward|title=Diary of a Bluegrass Confederate|publisher=Louisiana State University Press|year=1999|isbn = 0-8071-3058-3}}
* {{cite book|last=Marvel|first=William|title=Southwest Virginia in the Civil War: The Battles for Saltville|publisher=H.E. Howard|year=1992|isbn=978-1-56190-026-8}}
* {{cite book|last=McKnight|first=Brian|title=Contested Borderland:The Civil War in Appalachian Kentucky and Virginia|publisher=University of Press, Kentucky|year=2006|pages=220–222|isbn = 978-0-8131-2389-9}}
* {{cite book |isbn=0-19-503863-0 |year=1988 |author=[[James M. McPherson]] |title=[[Battle Cry of Freedom: The Civil War Era]] |quote=900 page survey of all aspects of the war; Pulitzer prize}}
* {{cite book|last=Mosgrove|first=George|title=Kentucky Cavaliers in Dixie: Reminiscences of a Confederate Cavalryman |publisher=University of Nebraska Press|year=1999|isbn = 978-0-8032-8253-7}}
* '''Official Records''' {{cite book|last=[[United States Department of War|United States War Department]]|title=The War of the Rebellion: A Compilation of the Official Records of the Union and Confederate Armies|url=http://cdl.library.cornell.edu/cgi-bin/moa/sgml/moa-idx?notisid=ANU4519-0093|series=1|volume=XLV (Pt. I)|year = 1894|publisher = Government Printing Office, Washington}}
* {{cite book|last=Weaver|first=Jeffery|title=64th Virginia Infantry Regimental History|publisher=H. E. Howard|year=1992|isbn= 1-56190-041-9}}
* {{cite web|url=http://www.nps.gov/hps/abpp/battles/va081.htm|title=NPS Battle Summary|publisher=CWSAC}}
* [http://www.nps.gov/hps/abpp/CWSII/VirginiaBattlefieldProfiles/Manassas%20Gap%20to%20Middleburg.pdf CWSAC Report Update]
{{refend}}

==External links==
* [http://www.marionva.org/attractions/wartrails/ Virginia Civil War Trails description of the battle]
* [http://www.marionva.org/history/ History of Marion County]
* [http://web.archive.org/web/20160202133926/http://www.bencaudill.com/documents_msc/battle_of_marion.html Step by step of the battle]

{{Navboxes
| list = 
{{American Civil War |expanded=CTCBS}}
}}

{{coord|36.8520|-81.4873|display=title|type:event_region:US-VA}}

{{good article}}

[[Category:Stoneman's 1864 raid|Marion]]
[[Category:Union victories of the American Civil War|Marion]]
[[Category:Battles of the Western Theater of the American Civil War|Marion]]
[[Category:Raids of the American Civil War|Marion]]
[[Category:Battles of the American Civil War in Virginia|Marion]]
[[Category:Smyth County, Virginia]]
[[Category:Conflicts in 1864|Marion]]
[[Category:1864 in Virginia]]
[[Category:December 1864 events]]