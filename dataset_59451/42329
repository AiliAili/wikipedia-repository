{{Use dmy dates|date=October 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Be-10
 |image= 
 |caption= 
 |nocat_wdimage= yes <!--Wikidata image checked on 29 Jan 2017 and not used-->
}}{{Infobox Aircraft Type
 |type= [[Patrol bomber]]
 |manufacturer= [[Beriev OKB]] (OKB-49)
 |designer= 
 |first flight= 20 June 1956<ref name="GordonBJFB">{{cite book|last=Gordon|first=Yefim |author2=Andrey Sal'nikov |author3=Aleksander Zablotskiy|title=Beriev's Jet Flying Boats|publisher=Midland publishing|location=Hinkley|year=2006|series=Red Star|volume=28|pages=21–48|isbn=1-85780-236-5}}</ref>
 |introduced= 
 |retired= 
 |status= Retired
 |primary user= [[Soviet Naval Aviation]]
 |more users= <!--up to three more. please separate with <br/>.-->
 |produced= 1958-1961<ref name="GordonBJFB"/>
 |number built= 28<ref name="GordonBJFB"/>
 |unit cost= 
 |variants with their own articles= 
}}
|}

The '''Beriev Be-10''', also known as '''Izdelye M''', ([[NATO reporting name]]: '''Mallow''') was a twin engined, turbojet powered, [[flying-boat]], [[patrol bomber]] built by the [[Soviet Union]] from 1955. The Be-10 is sometimes referred to as the '''M-10''', though this designation is believed to apply only to the modified Be-10 that established 12 [[Fédération Aéronautique Internationale|FAI]] world records in 1961, Bort no. ''40 Yellow'',<ref name="GordonBJFB"/> still holding class records for speed and altitude.<ref name="FAI">[http://records.fai.org/general_aviation/aircraft.asp?id=780 FAI Records - M-10]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==Design and development==
The Be-10 was designed in response to [[Council of Ministers of the Soviet Union]] directive No.2622-1105ss which called for a turbojet powered flying boat for open-sea reconnaissance, bombing, torpedo attack and mine-laying. Stipulated performance was to include a maximum speed of {{convert|950|to|1000|km/h|mph|abbr=on|0}} and the ability to operate in wave heights of {{convert|1.5|m|ft|abbr=on|0}} at wind speeds up to {{convert|20|m/s|mph|abbr=on|0}} with submission for state acceptance trials in November 1955.<ref name="GordonBJFB"/>

OKB-49, under the leadership of [[Gheorgiy M. Beriev]] took up the challenge of designing and building the '''Izdeliye M''' (Beriev OKB in-house designation), approval to proceed with prototype manufacture was received in mid-1954, but OKB-49 did not have facilities to build the prototype, so this was performed at the nearby [[GAZ no.89]] (''Gosudarstvenny Aviatsionnyy Zavod'' – state aviation plant/factory), also known as [[TMZD]] (''Taganrogskiy Mashinostroitel'nyy Zavod imeeni Dimitrova'' - Taganrog Machinery Plant named after [[Gheorgi Dimitrov]]).<ref name="GordonBJFB"/>

Beriev realised that the Be-10 would be ready for trials during the winter of 1955, but [[Taganrog]], where the OKB was located, is ice-bound in winter, so an alternative site was chosen at [[Gelendzhik]] on the [[Black Sea]] coast, which is clear of ice all year round. The completed components of the first prototype were transported to Ghelendjik where they were assembled and trials begun on 20 December 1955. Initial ground running of the engines revealed potentially disastrous vibration of the rear fuselage, which caused cracking of skin and structural components as well as loosening nuts and fasteners, also the fracture of pipeline and wiring loom support brackets. To reduce the vibration caused by the jet exhaust, the jet-pipes were splayed outboard by a further 3° to 6°. The prototype also underwent trial installation of raised extended air intakes to reduce water ingestion, but they were not adopted for in-service aircraft due to the degradation in performance.<ref name="GordonBJFB"/>

After the first flight of the Be-10 on 20 June 1956, manufacturers flight testing lasted a total of 76 flights up to 20 October 1958, whence the prototype was submitted for state acceptance trials along with the first production aircraft.<ref name="GordonBJFB"/> Anticipating that any deficiencies could be rectified during production or by modification, production of 27 examples for service with [[Soviet Naval Aviation]] (AV-MF) was authorised from 1958 to 1961.<ref name="GordonBJFB"/>

The Be-10 is an all-metal high-wing monoplane with the engines located beneath the wing roots and with splayed-out tailpipes. To minimize the risk of water ingestion, the engine air intakes are located on the forward fuselage section with spray fences on either side of the [[Bow (ship)|bow]] protecting the engines from water ingestion. The streamlined hull was fitted with a shallow single-step, sea [[rudder]] under the rear fuselage, 50° [[swept wing]]s with marked [[Dihedral (aircraft)#Anhedral|anhedral]] and balance floats attached by short pylons at the wing-tips.

Conventional swept-back stressed skin construction tail surfaces were a 35° sweptback fin and 40° swept tail-planes just above the rear fuselage. Control surfaces were Ailerons on each wing, a rudder on the fin and elevators on the tail-planes. The fuselage was a two step design with a high length to beam ratio to improve rough water handling, v-section planing bottom, two steps and was divided into nine water-tight compartments the forward and rear compartments being pressurised. Engine nacelles for the [[Lyul'ka AL-7F]] engines were attached to the fuselage sides under the wing centre-section.<ref name="GordonBJFB"/>

The original design showed some weaknesses in the seaworthiness and had to be modified; after modification the Be-10 was seaworthy up to a wave height of 1.2 m and able to fly with wind speeds up to {{convert|16|m/s|mph|abbr=on|0}} from water or land.<ref name="GordonBJFB"/>

==Operational history==
Operational use of the Be-10 began when the 2nd Squadron of the [[977th OMDRAP|977th Independent Naval Long-range Reconnaissance Air Regiment]] (977th OMDRAP) started replacing its [[Beriev Be-6]] flying boats with Be-10 aircraft. This squadron and the 1st Squadron of the 977th OMDRAP became the only operators of the Be-10, operating from a [[Southern Naval Base (Ukraine)|naval base]] at [[Lake Donuzlav]] on the [[Crimean peninsula]]

The first public appearance of the Be-10 was when four aircraft flew over the 1961 [[Aviation Day]] air display at [[Tushino]], giving the impression that the Be-10 was already in service. However, the Be-10 proved to be difficult to fly and there were several accidents. The Be-10 suffered from [[metal fatigue]] due to the stress on the airframe from the high-speed takeoffs and landings, together with corrosion. The Be-10 was removed from service in 1968, and was replaced by the [[turboprop]]-powered [[Beriev Be-12|Be-12]], which was easier to operate and had better endurance.<ref name="GordonBJFB"/>

==Operators==
''Data from:''<ref name="GordonBJFB"/>
;{{USSR}}
*[[Soviet Naval Aviation]] (AV-MF)
**1st Squadron of the 977th OMDRAP
**2nd Squadron of the 977th OMDRAP

==Variants==
;Izdeliye M
:The prototype Be-10, Bort no. ''10 Red'' completed in 1955 and flown on 20 June 1956 from [[Gelendzhik]] on the [[Black Sea]]. One built.<ref name="GordonBJFB"/>
;Be-10
:The standard production version built for the AV-MF, 27 built from 1958 to 1961.<ref name="GordonBJFB"/>
;Be-10N
:Proposed cruise missile carrier to be part of the K-12B airborne strike system, carrying two K-12BS cruise missiles on pylons under the wings. Although reaching mock-up stages and being granted approval from the ''Gosudarstvenny Komitet Po Aviatsionny Tekhnike'' ("state committee on aircraft technology") Scientific & Technical Council, no further action was authorised.<ref name="GordonBJFB"/>
;Be-10S
:A proposed anti submarine warfare seaplane project to have been armed with the [[SK-1 Skal'p]] ("Scalp") nuclear depth charge. No hardware was built.<ref name="GordonBJFB"/>
;Be-10U
:A proposed Target designator seaplane project to have been fitted with the ''[[Uspekh]]'' radar system, to pass targetting information to ship or shore based missile or artillery batteries. Discontinued by August 1960.<ref name="GordonBJFB"/>
;Be-10 Trainer
:A proposed trainer version with a duplicate set of controls in a cockpit in the extreme nose. Conversions were to have taken place, but there is no evidence that any were completed.<ref name="GordonBJFB"/>
;M-10
:A single Be-10, (c/n 0600505), Bort No. ''40 Yellow'', modified to attempt record breaking flights in 1961. The tail turret was removed and faired over, an additional pitot tube was fitted to the fin, and guns removed. Twelve world records were broken, some of which still stand.<ref name="GordonBJFB"/>

==World records==
The M-10 (modified from a Be-10) set twelve new world records in 1961, ratified by the ''[[Federation Aeronautique Internationale|FAI]]''

{|class="wikitable"
|+''Data from: [[Fédération Aéronautique Internationale|FAI]]''<ref name = "FAI"/>
!Category
!Record
!Pilot
!Date
|-
|Speed over a {{convert|15|km|mi|abbr=on|2}} to {{convert|25|km|mi|abbr=on|0}} track
|{{convert|912|km/h|mph|abbr=on|2}}
|[[N.I. Andriyevskiy]] 
|7 August 1961
|-
|Speed over a {{convert|1,000|km|mi|abbr=on|0}} circuit with no payload.
|{{convert|875.86|km/h|mph|abbr=on|2}}
|[[G.I. Boor'yanov]]
|3 September 1961
|-
|Speed over a {{convert|1,000|km|mi|abbr=on|0}} circuit with {{convert|1,000|kg|lb|abbr=on|0}} payload.
|{{convert|875.86|km/h|mph|abbr=on|2}}
|[[G.I. Boor'yanov]]
|3 September 1961
|-
|Speed over a {{convert|1,000|km|mi|abbr=on|0}} circuit with {{convert|2,000|kg|lb|abbr=on|0}} payload.
|{{convert|875.86|km/h|mph|abbr=on|2}}
|[[G.I. Boor'yanov]]
|3 September 1961
|-
|Speed over a {{convert|1,000|km|mi|abbr=on|0}} circuit with {{convert|5,000|kg|lb|abbr=on|0}} payload.
|{{convert|875.86|km/h|mph|abbr=on|2}}
|[[G.I. Boor'yanov]]
|3 September 1961
|-
|Altitude with {{convert|1,000|kg|lb|abbr=on|0}} payload.
|{{convert|14,062|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
|8 September 1961
|-
|Altitude with {{convert|2,000|kg|lb|abbr=on|0}} payload.
|{{convert|14,062|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
|8 September 1961
|-
|Altitude with {{convert|5,000|kg|lb|abbr=on|0}} payload.
|{{convert|14,062|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
|8 September 1961
|-
|Altitude.
|{{convert|14,062|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
| 9 September 1961
|-
|Altitude with {{convert|10,000|kg|lb|abbr=on|0}} payload.
|{{convert|12,733|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
|11 September 1961
|-
|Altitude with {{convert|15,000|kg|lb|abbr=on|0}} payload.
|{{convert|11,997|m|ft|abbr=on|2}}
|[[G.I. Boor'yanov]]
|12 September 1961
|-
|Maximum payload to an altitude of {{convert|2,000|m|ft|abbr=on|0}}
|{{convert|15,206|kg|lbs|abbr=on|2}}
|[[G.I. Boor'yanov]]
|12 September 1961
|}

==Specifications (Be-10)==
[[File:1965. Бе-10.jpg|thumb|right|Soviet stamp showing Beriev Be-10]]
{{Aircraft specs
|ref=Beriev's Jet Flying Boats<ref name="GordonBJFB"/><!-- for giving the reference for the data -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=3
|capacity=
|length m=31.45
|length ft=
|length in=
|length note=
|span m=28.6
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=10.7
|height ft=
|height in=
|height note=
|wing area sqm=130
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=27,356
|empty weight lb=
|empty weight note=
|gross weight kg=45,000
|gross weight lb=
|gross weight note=
|max takeoff weight kg=48,500
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|18,750|kg|lb|abbr=on|0}}
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
*'''Draught''' {{convert|1.75|m|ft|abbr=on|2}}
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Lyul'ka AL-7PB]]
|eng1 type=Axial flow turbojet
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=71.2<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|more power=

<!--
        Performance
-->
|perfhide=

|max speed kmh=910
|max speed mph=
|max speed kts=
|max speed note= at {{convert|5,000|m|ft|abbr=on|-2}}
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=2,895
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=12,500
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=10.288
|climb rate ftmin=
|climb rate note=
|time to altitude={{convert|5,000|m|ft|abbr=on|-2}} in 8.1 minutes
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns= 4 × 23 mm (0.90 in) [[Afanasev Makarov AM-23]] [[autocannon|cannon]]. 2 forward firing, and 2 in a radar-controlled tail turret
|missiles= Up to 3 [[RAT-52]] torpedoes.
|bombs= 12 × [[FAB-250]] 250 lb bombs or 1 [[FAB-3000]] 3,000 lb bomb.
|rockets= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament= Anti-shipping mines.

|avionics=
}}

==See also==
{{Aircontent
|related=*[[Beriev R-1]]
|similar aircraft=*[[P6M SeaMaster]]
|lists=* [[List of seaplanes and flying boats]]
* [[List of military aircraft of the Soviet Union and the CIS]]
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
* {{cite book|title=Russian aircraft since 1940|last=Alexander|first=Jean|pages=61–63|year=1975|isbn=0-370-10025-5|publisher=Putnam|location=[[New York City|New York, New York]]}}
* {{cite book|last=Gordon|first=Yefim |author2=Andrey Sal'nikov |author3=Aleksander Zablotskiy|title=Beriev's Jet Flying Boats|publisher=Midland publishing|location=Hinkley|year=2006|series=Red Star|volume=28|pages=21–48|isbn=1-85780-236-5}}

==External links==
{{commons category|Beriev Be-10}}
* [http://www.ctrl-c.liu.se/misc/RAM/be-10.html Be-10 M-10, G.M. Beriev 'Mallow'] at Russian Aviation Museum

{{Beriev aircraft}}

[[Category:Beriev aircraft|Be-0010]]
[[Category:Jet seaplanes and flying boats]]
[[Category:Soviet patrol aircraft 1960–1969]]
[[Category:High-wing aircraft]]
[[Category:Twinjets]]
[[Category:Flying boats]]