{{Infobox journal
| title = ALTEX
| cover =
| former_names = Alternativen zu Tierexperimenten
| abbreviation = ALTEX-Altern. Anim. Exp.
| discipline = [[Alternatives to animal testing]]
| editor = Sonja von Aulock
| publisher = [[Springer Science+Business Media|Springer Spektrum]]
| history = 1984-present
| frequency = Quarterly
| openaccess = Yes
| license = [[Creative Commons Attribution 4.0 International]]
| impact = 5.824
| impact-year = 2015
| ISSN = 1868-596X
| eISSN = 1868-8551
| ISSN2 = 0946-7785
| ISSN2label= ALTEX: Alternativen zu Tierexperimenten:
| CODEN =
| JSTOR =
| LCCN =
| OCLC = 690935010
| website = http://www.altex.ch/Home.12.html
| link1 = http://www.altex.ch/Current-issue.16.html
| link1-name = Online access
| link2 = http://www.altex.ch/All-issues.20.html
| link2-name = Online archive
| link3 = http://www.springer.com/springer+spektrum/biowissenschaften/journal/12270
| link3-name = Journal page at publisher's website
}}
'''ALTEX: Alternatives to Animal Experimentation''' is a quarterly [[peer-reviewed]] [[scientific journal]] covering [[alternatives to animal experimentation]] and related issues of [[bioethics]], seeking to promote the [[The Three Rs (animals)|replacement, reduction, and refinement]] of animal use in research (the "3Rs").<ref name="About">{{cite web |title=About ALTEX |website=Homepage |url=http://www.altex.ch/About-ALTEX.36.html |accessdate=23 June 2016 |publisher=Altex}}</ref><ref>{{cite journal |last1=von Aulock |first1=S. |title=Editorial: 30th anniversary of ALTEX |journal=ALTEX |date=2014 |volume=31 |issue=4 |pmid=25538978 |url=http://www.altex.ch/resources/raltex_2014_4_U2U2_Edi.pdf |accessdate=23 June 2016}}</ref> It was originally published in [[German language|German]] and established in 1984 as ''ALTEX: Alternativen zu Tierexperimenten'' and is published by [[Springer Science+Business Media|Springer Spektrum]] on behalf of the Swiss Society ALTEX Edition.<ref name="About"/><ref name="springer.com">{{cite web |title=ALTEX |publisher=Springer Spektrum |date=2010 |url=http://www.springer.com/springer+spektrum/biowissenschaften/journal/12270 |accessdate=23 June 2016}}</ref>

It is the official journal of [[Center for Alternatives to Animal Testing]], the [[American Society for Cellular and Computational Toxicology]], the European consensus platform for alternatives, the [[European Society for Alternatives to Animal Testing]], and the transatlantic think tank for toxicology.<ref name="About" /> It has been an [[open access journal]] since 2011 under a [[Creative Commons Attribution 4.0]] license.<ref name="About" />

The journal has two companion publications: ''ALTEX Proceedings'' ({{ISSN|2194-0479}}), which publishes proceedings of scientific conferences relating to the 3Rs, and ''TIERethik'', a biannual German-language periodical on [[bioethics]] and [[Anthrozoology|human-animal studies]].<ref name="home">{{cite web |title=Altex |publisher=Altex |url=http://www.altex.ch/Home.12.html |accessdate=23 June 2016}}</ref>

==Abstracting and indexing==
The journal is [[abstracting and indexing|abstracted and indexed]] in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/100953980 |title=ALTEX |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=23 June 2016}}</ref> [[Science Citation Index Expanded]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=23 June 2016}}</ref> [[Scopus]],<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=23 June 2016}}</ref> and [[Embase]].<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=23 June 2016}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 5.824, ranking it 11th out of 124 journals in the category "Medicine, Research and Experimental".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Medicine Research and Experimental |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist|30em}}

==External links==
* {{Official website|http://www.altex.ch/Home.12.html}}
* [http://caat.jhsph.edu Johns Hopkins University Center for Alternatives to Animal Testing]

[[Category:Alternatives to animal testing]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Biology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1984]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]