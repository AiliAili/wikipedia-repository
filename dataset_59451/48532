{{Taxobox
| image = Close up of Hoff crab carapace.jpg
| image_caption = Dorsal view of carapace showing distinctive markings
| regnum = [[Animal]]ia
| phylum = [[Arthropod]]a
| subphylum = [[Crustacean|Crustacea]]
| classis = [[Malacostraca]]
| ordo = [[Decapoda]]
| familia = [[Kiwaidae]]
| genus = ''[[Kiwa (genus)|Kiwa]]''
| species = '''''K. tyleri'''''
| binomial = ''Kiwa tyleri''
| binomial_authority = Thatje S, Marsh L, Roterman CN, Mavrogordato MN, Linse K, 2015 
}}
The "'''Hoff crab'''" is a species of deep-sea [[squat lobster]] in the family [[Kiwaidae]], which lives on [[hydrothermal vent]]s near [[Antarctica]].<ref name="Rogers 2012">{{cite journal |author1=Alex D. Rogers |author2=Paul A. Tyler |author3=Douglas P. Connelly |author4=Jon T. Copley |author5=Rachael James |author6=Robert D. Larter |author7=Katrin Linse |author8=Rachel A. Mills |author9=Alfredo Naveira Garabato |author10=Richard D. Pancost |author11=David A. Pearce |author12=Nicholas V. C. Polunin |author13=Christopher R. German |author14=Timothy Shank |author15=Philipp H. Boersch-Supan |author16=Belinda J. Alker |author17=Alfred Aquilina |author18=Sarah A. Bennett |author19=Andrew Clarke |author20=Robert J. J. Dinley |author21=Alastair G. C. Graham |author22=Darryl R. H. Green |author23=Jeffrey A. Hawkes |author24=Laura Hepburn |author25=Ana Hilario |author26=Veerle A. I. Huvenne |author27=Leigh Marsh |author28=Eva Ramirez-Llodra |author29=William D. K. Reid |author30=Christopher N. Roterman |author31=Christopher J. Sweeting |author32=Sven Thatje |author33=Katrin Zwirglmaier |year=2012 |title=The discovery of new deep-sea hydrothermal vent communities in the Southern Ocean and implications for biogeography |journal=[[PLoS Biology]] |volume=10 |issue=1 |page=e1001234 |doi=10.1371/journal.pbio.1001234 |ref=Rogers2012 |pmid=22235194 |pmc=3250512}}</ref> The crustacean was given its nickname in 2010 by UK deep-sea scientists aboard the [[RRS James Cook|RRS ''James Cook'']], owing to resemblance between its dense covering of [[seta]]e on the ventral surface of the exoskeleton and the hairy chest of the actor [[David Hasselhoff]].<ref>{{cite news |title='The Hoff' crab is new ocean find|url=http://www.bbc.co.uk/news/science-environment-16394430 |publisher=[[BBC News]] |date=4 January 2012}}</ref> The 2010 expedition to explore hydrothermal vents on the [[East Scotia Ridge]] was the second of three expeditions to the Southern Ocean by the UK led research consortium, ChEsSo (Chemosynthetic Ecosystems of the Southern Ocean).<ref>{{cite web|title=ChEsSo – Exploration for vents south of the Polar Front|url=http://www.noc.soton.ac.uk/chess/science/chesso.html}}</ref>

==Distribution==
[[File:East Scotia Ridge vents map.png|left|thumb|Location of E2 and E9 sites in the [[Scotia Sea]] showing the [[East Scotia Ridge]] (ESR) between the [[Scotia Plate]] (SCO) and [[South Sandwich Plate]] (SAN)]]
This species – the only member of its genus found outside the [[Pacific Ocean]], is known from two sites adjacent to and on the chimney sides of hydrothermal vents in the [[East Scotia Ridge]] of the south [[Atlantic Ocean]]: from around {{convert|2394|m}} depth at the E9 vent site and from around {{convert|2608|m|abbr=on}} depth at the E2 site.<ref name="Rogers 2012"/>

==Description==
Phylogenetic analysis of the mitochondrial [[16S ribosomal RNA]] gene and a range of other anomuran crustaceans, using [[Bayesian inference]], places this species from the East Scotia Ridge as a sister taxon to ''[[Kiwa hirsuta]]'', with a sequence divergence from this species of 6.45%, which is consistent for within-genus divergence in squat lobsters.<ref name="Rogers 2012"/>

== Ecology ==
[[File:Dense mass of anomuran crab Kiwa around deep-sea hydrothermal vent.jpg|thumb|left|Population of ''Kiwa'' around a [[hydrothermal vent]]]]
Unlike the two currently described species of the genus, ''[[Kiwa hirsuta]]'' and ''[[Kiwa puravida]]'', which are notable for having a dense covering of setae on their elongated [[chela (organ)|chelae]], this species has shorter chelae, with most of the setae concentrated instead on the ventral surface of the crab.<ref name="Rogers 2012"/>

Filamentous bacteria were found on the [[seta]]e and similar-looking sulfur-oxidising bacteria have been found amongst the setae of ''Kiwa hirsuta'' and ''Kiwa puravida''. It has been hypothesised that these sulfur-oxidising bacteria, which fix carbon from the water by oxidising sulfides in the hydrothermal fluid, are a significant source of nutrition to the crabs.<ref name="Rogers 2012"/><ref name=PLOS>{{cite journal |author1=Andrew R. Thurber |author2=William J. Jones |author3=Kareen Schnabel |year=2011 |title=Dancing for food in the deep sea: bacterial farming by a new species of yeti crab |journal=[[PLoS ONE]] |volume=6 |issue=11 |page=e26243 |doi=10.1371/journal.pone.0026243 |url=http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0026243 |pmid=22140426 |pmc=3227565}}</ref><ref name="Macpherson">{{cite journal |author1=E. Macpherson |author2=W. Jones |author3=M. Segonzac | year = 2006 |url=http://www.mnhn.fr/museum/front/medias/publication/6892_z05n4a3.pdf |format=[[Portable Document Format|PDF]] |title=A new squat lobster family of Galatheoidea (Crustacea, Decapoda, Anomura) from the hydrothermal vents of the Pacific-Antarctic Ridge | journal = [[Zoosystema]] | volume = 27 | issue = 4 | pages = 709–723}}</ref>

The Hoff crabs were found living adjacent to and on the sides of hydrothermal vent chimneys living in close proximity to fluid emanating from the chimneys at temperatures greater than of {{convert|350|C|F}}. At E9, densities of the crabs were observed in excess of 600 per square metre (56 per square foot).<ref name="Rogers 2012"/>

Small limpets ([[Lepetodrilus sp. East Scotia Ridge|a new species of ''Lepetodrilus'']]) are often found on the carapace apparently grazing on bacteria.<ref name="Rogers 2012"/> Other marine fauna, such as [[sea anemone]]s (family [[Actinostolidae]]), gastropods ''[[Gigantopelta chessoia]]'', a species of stalked [[barnacle]] (most likely of the genus ''[[Vulcanolepas]]''), a [[pycnogonid]] close to the genus ''[[Sericosura]]'', and a predatory seven armed [[starfish]] can be found living together with this species.<ref name="Rogers 2012"/>

== Media ==
The "Hoff crab" nickname was widely reported in the media whilst the species still [[undescribed taxon|lacked a binomial name]],<ref>{{cite news|title=Near icy waters, marine life gets by swimmingly |url=http://www.npr.org/2012/01/06/144801675/near-icy-waters-marine-life-gets-by-swimmingly |newspaper=NPR}}</ref><ref>{{cite news |title=New crab with hairy chest dubbed "The Hoff" |url=http://www.cbsnews.com/8301-31749_162-57352993-10391698/new-crab-with-hairy-chest-dubbed-the-hoff/ |publisher=[[CBS]]}}</ref><ref>{{cite news |title=Unknown 'Hoff' crab species found |url=http://www.irishtimes.com/newspaper/breaking/2012/0104/breaking45.html |publisher=[[The Irish Times]]}}</ref><ref>{{cite news |title=Weird & wild: 5 weirdest Antarctic species |url=http://newswatch.nationalgeographic.com/2012/05/17/weird-wild-5-weirdest-antarctic-species/ |publisher=[[National Geographic Society|National Geographic]]}}</ref> and [[David Hasselhoff]] tweeted favourably in response to his "Hoff" moniker being used to describe a hydrothermal vent crab.<ref>{{cite news|title=The Hoff, a yeti crab with a very hairy chest, discovered in deep sea vent |url=http://www.huffingtonpost.co.uk/2012/01/04/the-hoff-yeti-crab-with-hairy-chest-ocean-volcanic-vent_n_1183011.html |newspaper=[[Huffington Post]] |date=4 January 2012}}</ref><ref>{{cite news|title=David Hasselhoff interview: 'failure usually works for me' |url=http://www.huffingtonpost.co.uk/2012/01/13/david-hasselhoff-interview-magicians_n_1204136.html |newspaper=[[Huffington Post]] |date=3 January 2012}}</ref>

==References==
This article incorporates CC-BY-2.5 text from [[#Rogers2012|Rogers ''et al.'' (2012)]].
{{reflist|32em}}

==External links==
*[https://www.youtube.com/watch?v=r17cPDVzTls&feature=player_embedded Ghostly yeti crab swarms discovered near Antarctica], National Geographic YouTube Channel

[[Category:Squat lobsters]]
[[Category:Animals living on hydrothermal vents]]
[[Category:Undescribed arthropod species]]