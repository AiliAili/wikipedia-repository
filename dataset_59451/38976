{{good article}}
{{Infobox album
| Name        = Burlesque: Original Motion Picture Soundtrack
| Type        = [[Soundtrack]]
| Artist      = [[Christina Aguilera]] and [[Cher]]
| Cover       = Burlesque Soundtrack Cover.jpg
| Border      = yes
| Released    = {{Start date|2010|11|19}}
| Recorded    = 2009–10
| Length      = {{duration|m=31|s=53}}
| Genre       = {{flat list|
*[[Dance-pop]]
*[[jazz]]
*[[Contemporary R&B|R&B]]}}
| Label       = {{flat list|
*Screen Gems, Inc
*[[RCA Records|RCA]]}}
| Producer    = {{flat list|
*Christina Aguilera
*[[Steve Antin|Steven Antin]]
*[[Samuel Dixon]]
*[[Ron Fair]]
*[[Matthew Gerrard]]
*[[Claude Kelly]]
*Steve Lindsey
*[[Linda Perry]]
*[[The Phantom Boyz]]
*[[Matt Serletic]]
*[[Tricky Stewart|C. "Tricky" Stewart]]
*Mark Taylor
}}
| Misc        = {{Extra chronology
  | Type       = Soundtrack
  | Artist            = [[Christina Aguilera]]
  | Last album  = ''[[Bionic (Christina Aguilera album)|Bionic]]''<br />(2010)
  | This album  = '''''Burlesque'''''<br />(2010)
  | Next album  =  ''[[Lotus (Christina Aguilera album)|Lotus]]''<br />(2012)
  }}
{{Extra chronology
  | Type        = Soundtrack
  | Artist      = [[Cher]]
  | Last album  = ''[[Gold (Cher album)|Gold]]''<br />(2005)
  | This album  = '''''Burlesque'''''<br />(2010)
  | Next album  = ''[[Closer to the Truth (Cher album)|Closer to the Truth]]''<br />(2013)
  }}
{{Singles
  | Name            = Burlesque: Original Motion Picture Soundtrack
  | Type              = soundtrack
  | single 1         = [[You Haven't Seen the Last of Me]]
  | single 1 date = November 24, 2010
  | single 2         = [[Express (Christina Aguilera song)|Express]]
  | single 2 date = December 6, 2010
  | single 3         = [[Show Me How You Burlesque]]
  | single 3 date = February 4, 2011
  }}
}}

'''''Burlesque: Original Motion Picture Soundtrack''''' is the [[soundtrack album]] to the [[Burlesque (2010 American film)|film of the same name]] by American singers [[Christina Aguilera]] and [[Cher]]. Screen Gems, Inc and [[RCA Records]] released it on November 19, 2010.

The soundtrack comprises ten songs, eight performed by Aguilera and two performed by Cher. ''Burlesque'' is inspired by [[jazz]] music style, in contrast to Aguilera's previous release ''[[Bionic (Christina Aguilera album)|Bionic]]'' (2010) but similar to her 1920s, 1930s and 1940s-influenced album ''[[Back to Basics (Christina Aguilera album)|Back to Basics]]'' (2006). Upon its release, the album received generally favorable reviews from [[Music journalism|music critics]]. It won a [[Golden Globe Award for Best Original Song]] at the [[68th Golden Globe Awards|2011 ceremony]] for "[[You Haven't Seen the Last of Me]]" by Cher and received another nomination in the same category for "Bound to You". Also album received nomination [[Grammy Award for Best Compilation Soundtrack for Visual Media|Best Compilation Soundtrack for Visual Media]] at the [[54th Annual Grammy Awards]].

''Burlesque'' peaked at number eighteen on the U.S. [[Billboard 200|''Billboard'' 200]] chart and reached the top ten of several national [[record chart]]s. As of August 2014, the album has sold 707,000 copies in the United States.<ref name=salesupdate2014>{{cite web|title=Ask Billboard: Taylor Swift Out-'Shake's Mariah Carey|url=http://www.billboard.com/articles/columns/chart-beat/6236538/ask-billboard-taylor-swift-out-shakes-mariah-carey?page=0%2C3|work=Billboard|author=Trust, Bary|accessdate=September 1, 2014|date=September 1, 2014}}</ref> The soundtrack spawned three [[single (music)|promotional single]]s: "You Haven't Seen the Last of Me" (performed by Cher), "[[Express (Christina Aguilera song)|Express]]" and "[[Show Me How You Burlesque]]" (performed by Aguilera).

==Background==
In early May 2010, Aguilera announced that she would embark on the supporting concert tour for her sixth studio album, ''[[Bionic (Christina Aguilera album)|Bionic]]''.<ref>{{cite web|url=http://www.billboard.com/articles/photos/live/958304/christina-aguilera-announces-bionic-summer-tour-dates|title=Christina Aguilera Announces 'Bionic' Summer Tour Date|first=David|last=J. Prince|date=May 10, 2010|work=[[Billboard (magazine)|Billboard]]|accessdate=October 4, 2013}}</ref> However, her management team announced shortly afterwards that the tour was postponed due to Aguilera's promotion for her first feature film, ''[[Burlesque (2010 musical film)|Burlesque]]'' (2010), in which she starred alongside [[Cher]]. Aguilera reported that she would reschedule the tour in 2011,<ref>{{cite web|url=http://www.rollingstone.com/music/news/christina-aguilera-delays-20-date-summer-tour-for-new-lp-bionic-20100525|title=Christina Aguilera Delays 20 Date Summer Tour for New LP 'Bionic'|work=[[Rolling Stone]]|first=Daniel|last=Kreps|date=May 25, 2010|accessdate=February 17, 2014|archiveurl=https://web.archive.org/web/20130203050706/http://www.rollingstone.com/music/news/christina-aguilera-delays-20-date-summer-tour-for-new-lp-bionic-20100525|archivedate=February 3, 2013}}</ref> though the tour never happened.<ref>{{cite web|url=http://www.christinaaguilera.com/us/events/past?page=4|title=Christina Aguilera: Past Events|publisher=christinaaguilera.com|accessdate=June 19, 2014}}</ref>

''Burlesque'' was released in November 2010. The film was met with mixed reviews from critics, who criticized it for being "campy and [[cliché]]d", yet praised Aguilera's acting debut.<ref>{{cite web|url=http://www.metacritic.com/movie/burlesque|title='Burlesque' Reviews|publisher=[[Metacritic]]|accessdate=May 2, 2014}}</ref> The accompanying soundtrack, ''Burlesque: Original Motion Picture Soundtrack'', comprises ten songs: eight performed by Aguilera and two performed by Cher.<ref name="AllMusic">{{cite web|url=http://www.allmusic.com/album/burlesque-mw0002058722|title=Burlesque - Original Soundtrack|publisher=[[AllMusic]]|first=Stephen|last=Thomas Erlewine|accessdate=May 2, 2014}}</ref> It was Cher's first major release since her compilation ''[[Gold (Cher album)|Gold]]'' in 2005.<ref name="AllMusic"/> Producer [[Tricky Stewart]] stated that the collaboration with Aguilera on ''Burlesque'' soundtrack was "a crazy opportunity different from anything [he]'ve ever done before".<ref name="BillboardWorking">{{cite web|first=Gail|last=Mitchell|url=http://www.billboard.com/articles/news/951073/tricky-stewart-talks-jessica-simpson-burlesque-production|title=Tricky Stewart Talks Jessica Simpson, 'Burlesque' Production|work=Billboard|date=November 12, 2010|accessdate=November 12, 2010}}</ref> [[Danja (record producer)|Danja]] was looking forward to working with Aguilera on the soundtrack, however it was not done.<ref>{{cite web|url=http://www.billboard.com/articles/news/267295/producer-danja-talks-working-with-whitney-houston|title=Producer Danja Talks Working With Whitney Houston|first=Sandra|last=Gordon|work=Billboard|date=September 24, 2009|accessdate=May 2, 2014}}</ref>

==Composition==
{{Listen|filename=Christina Aguilera - Express (Song sample).ogg|title="Express" {{small|(performed by Christina Aguilera)}}|description=A 26-second sample of "Express", the second single from the soundtrack|format=Ogg|pos=left}}
''Burlesque: Original Motion Picture Soundtrack'' comprises ten songs: eight performed by Aguilera and two performed by Cher.<ref name="AllMusic"/> The soundtrack has a "brassy, jazzy" sound inspired by [[jazz music|jazz]],<ref name="NYDailyNews">{{cite web|url=http://www.nydailynews.com/entertainment/music-arts/burlesque-soundtrack-review-christina-aguilera-vocal-athletics-out-diva-cher-chops-article-1.452395|title='Burlesque' soundtrack review: Christina Aguilera's vocal athletics can't out-diva Cher's chops|first=Jim|last=Farber|work=[[Daily News (New York)|Daily News]]|location=New York|date=November 25, 2010|accessdate=May 3, 2014}}</ref> in contrast to Aguilera's last [[electronic music]]-inspired studio album ''[[Bionic (Christina Aguilera album)|Bionic]]'' (2010) but similar to her fifth 1920s, 1930s and 1940s-influenced studio album ''[[Back to Basics (Christina Aguilera album)|Back to Basics]]'' (2006).<ref name="BillboardBiz">{{cite web|archiveurl=https://web.archive.org/web/20120112044802/http://www.billboard.biz/bbbiz/content_display/magazine/reviews/albums/e3ibde532c77149bc3146237ecd8761e87d |url=http://www.billboard.biz/bbbiz/content_display/magazine/reviews/albums/e3ibde532c77149bc3146237ecd8761e87d |title=Burlesque: Original Motion Picture Soundtrack |work=Billboard |first=Kerri |last=Mason |date=December 11, 2010 |accessdate=May 3, 2014 |archivedate=January 12, 2012 |deadurl=unfit }}</ref> Four of the songs performed by Aguilera are [[cover version]]s, two of [[Etta James]]' works: "[[Something's Got a Hold on Me]]" and "Tough Lover",<ref name="NYDailyNews"/> a cover version of [[Marlene Dietrich]]'s "Guy What Takes His Time",<ref name="SlantMagazine">{{cite web|url=http://www.slantmagazine.com/music/review/burlesque-original-soundtrack/2327|title=Burlesque: Original Motion Picture Soundtrack Review|work=[[Slant Magazine]]|first=Eric|last=Handerson|date=November 21, 2010|accessdate=May 3, 2014}}</ref> and a [[dance music|dance]] version of "[[The Beautiful People (song)|The Beautiful People]]" by [[Marilyn Manson]], including the "unmistakable" [[drum]] beats and [[guitar riff]] in the original version.<ref name="BillboardBiz"/> "I Am a Good Girl" "sticks to the sassy swing of a bygone musical era",<ref name="Blogcritics">{{cite web|url=http://blogcritics.org/music-review-burlesque-original-motion-picture/|title=Music Review: Burlesque – Original Motion Picture Soundtrack|publisher=[[Blogcritics]]|date=December 13, 2010|accessdate=May 3, 2014}}</ref> while "[[Express (Christina Aguilera song)|Express]]" has the similar musical style to Aguilera's previous single "[[Lady Marmalade#Moulin! Rouge cover|Lady Marmalade]]"<ref name="SlantMagazine"/> and lyrically evokes sexual theme as Aguilera "seductively" sings, "Show a little leg / You gotta shimmy your chest".<ref>{{cite web|url=http://idolator.com/5652662/express-christina-aguilera-burlesque-theme-song|title=Hop On The 'Express' That Is Christina Aguilera's 'Burlesque' Theme Song|work=[[Idolator (website)|Idolator]]|first=Robbie|last=Daw|date=October 11, 2010|accessdate=September 12, 2013}}</ref> The uptempo number "[[Show Me How You Burlesque]]" has a "modern sounding" dance production, but "lack of melody and strong hooks".<ref name="Blogcritics"/> The [[power ballad]] "Bound to You", co-written by [[Sia Furler]], has the same musical style to "[[You Lost Me]]" which was also co-written by Furler from ''Bionic'', features Aguilera's strong vocal delivery.<ref name="Blogcritics"/>

The two songs performed by Cher are: "Welcome to Burlesque" and "[[You Haven't Seen the Last of Me]]". "Welcome to Burlesque" was described as "a '[[Cabaret]]'-style oompah that shows both skill and humor".<ref name="NYDailyNews"/> The power ballad "You Haven't Seen the Last of Me" features Cher's powerful vocals.<ref name="NYDailyNews"/>

==Promotion==
In August 2010, a video containing a scene from the film featuring Aguilera performing "Something's Got a Hold on Me" was released onto [[YouTube]].<ref>{{cite web|url=http://www.billboard.com/articles/columns/viral-videos/956897/christina-aguilera-covers-etta-james-something-got-a-hold-on-me|title=Christina Aguilera Covers Etta James' 'Something Got A Hold On Me'|first=Walter|last=Frazier|work=Billboard|date=August 12, 2010|accessdate=May 3, 2014}}</ref> Later in early November, a clip featuring Aguilera performing "But I Am a Good Girl" from the movie was also released.<ref>{{cite web|url=http://www.billboard.com/articles/columns/viral-videos/951721/christina-aguilera-plays-a-good-girl-in-new-burlesque-clip|title=Christina Aguilera Plays A 'Good Girl' In New 'Burlesque' Clip|work=Billboard|date=November 4, 2011|first=Megan|last=Vick|accessdate=May 3, 2014}}</ref> On November 17, 2010, Aguilera performed the track "Bound to You" on ''[[The Tonight Show with Jay Leno]]''.<ref>{{cite web|url=http://www.billboard.com/articles/columns/viral-videos/950656/christina-aguilera-perfoms-bound-to-you-on-leno|title=Christina Aguilera {{sic|Perfoms|nolink=y}} 'Bound to You' on 'Leno'|first=Megan|last=Vick|work=Billboard|date=November 18, 2010|accessdate=May 3, 2014}}</ref> On November 19, 2010, she performed "Something's Got a Hold on Me" on ''[[The Ellen DeGeneres Show]]''.<ref>{{cite web|url=http://www.christinaaguilera.com/us/event/2010/11/19/ellen-degeneres-show|title=The Ellen DeGeneres Show|publisher=christinaaguilera.com|accessdate=May 3, 2014}}</ref> Aguilera performed "Express" at the [[American Music Awards of 2010]] on November 21, 2010.<ref name="amaperform">{{cite web|url=http://www.mtv.com/news/articles/1652768/christina-aguilera-struts-through-express-burlesque-at-amas.jhtml|title=Christina Aguilera Strust Through 'Express' (Burlesque) At AMAs|first=Gil|last=Kaufman|date=November 21, 2010|accessdate=May 23, 2013|publisher=MTV News}}</ref> The following day, Aguilera performed "Something's Got a Hold on Me" on ''[[Conan (talk show)|Conan]]''.<ref>{{cite web|url=http://www.christinaaguilera.com/us/event/2010/11/22/conan|title=Conan|publisher=christinaaguilera.com|accessdate=May 3, 2014}}</ref> On November 23, 2010, Aguilera performed "Show Me How You Burlesque" and "[[Beautiful (Christina Aguilera song)|Beautiful]]" during the season finale of [[Dancing with the Stars (U.S. season 11)|the eleventh season]] of U.S. television dancing competition ''[[Dancing with the Stars (U.S. TV series)|Dancing with the Stars]]''.<ref>{{cite web|url=http://www.hitfix.com/blogs/monkeys-as-critics/posts/recap-dancing-with-the-stars-season-11-finale-the-winner-is|title=Recap: Dancing with the Stars Season 11 Finale|publisher=[[HitFix]]|first=Liane|last=Bonin|date=November 23, 2010|accessdate=May 3, 2014}}</ref>

{{Listen|filename=You Haven't Seen the Last of Me.ogg|title="You Haven't Seen the Last of Me" {{small|(performed by Cher)}}|description=A 21-second sample of ''Burlesque''s lead single "You Haven't Seen the Last of Me"|format=Ogg|pos=right}}
"You Haven't Seen the Last of Me" performed by Cher was the first song to be released from ''Burlesque''. On November 24, 2010, a digital [[remix]] [[extended play]] of "You Haven't Seen the Last of Me" was released via [[iTunes Store]]s worldwide.<ref>{{cite web|url=https://itunes.apple.com/be/album/you-havent-seen-last-me-the/id406402313|title=iTunes – Music – You Haven't Seen the Last of Me (The Remixes) – EP|publisher=[[iTunes Store]] (BE)|accessdate=May 3, 2014}}</ref> On December 7, the digital remix version of the song was purchased for sales onto iTunes Stores.<ref>{{cite web|url=https://itunes.apple.com/de/album/you-havent-seen-last-me-stonebridge/id408671481|title=iTunes – Music &ndash; You Haven't Seen the Last of Me (StoneBridge Dub from Burlesque) |language=German|publisher=iTunes Store (DE)|accessdate=June 19, 2014}}</ref> In the United States, the single impacted [[adult contemporary]] radio stations on January 15, 2011.<ref>{{cite web|url=http://gfa.radioandrecords.com/publishGFA/GFANextPage.asp?sDate=01/15/2011&Format=3|title=R&R :: Going for Adds :: AC|work=[[Radio & Records]]|accessdate=May 3, 2014}}</ref> "Express" and "Show Me How You Burlesque" performed by Aguilera were the two next singles from the soundtrack, respectively: "Express" impacted Australian [[contemporary hit radio]] stations on December 6, 2010,<ref>{{cite journal |url=http://www.themusicnetwork.com/music-releases/singles/2010/12/06/issue-816/Christina-Aguilera--Express/ |title=Christina Aguilera – Express (SME) |work=[[The Music Network]] |issue=816 |accessdate=December 12, 2013 |archiveurl=https://web.archive.org/web/20120601225843/http://www.themusicnetwork.com/music-releases/singles/2010/12/06/issue-816/Christina-Aguilera--Express/ |archivedate=June 1, 2012}}</ref> while "Show Me How You Burlesque" was made available for digital sales on February 4, 2011.<ref>{{cite web|url=https://itunes.apple.com/fi/album/show-me-how-you-burlesque/id416083150|title=iTunes &ndash; Music &ndash; Show Me How You Burlesque|publisher=iTunes Store (FI)|accessdate=July 23, 2013}}</ref>

==Critical reception==
{{Album ratings
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|3|5}}<ref name="AllMusic"/>
| rev2 = ''[[Billboard (magazine)|Billboard]]''
| rev2Score = (favorable)<ref name="BillboardBiz"/>
| rev3 = [[Slant Magazine]]
| rev3Score = {{Rating|3|5}}<ref name="SlantMagazine"/>
| rev4 = ''[[The Advertiser (Adelaide)|The Advertiser]]''
| rev4Score = {{Rating|2.5|5}}<ref name="Advertiser"/>
}}
[[Stephen Thomas Erlewine]] from [[AllMusic]] gave the soundtrack three out of five stars, commenting that "some of this stuff is quite good".<ref name="AllMusic"/> [[Slant Magazine]]'s Eric Henderson provided a mixed review, writing that the soundtrack "seems to indicate her efforts are coming from a similarly era-straddling psychological place".<ref name="SlantMagazine"/> Jim Farber from ''[[New York Daily News]]'' criticized Aguilera for her "vocals offer the same distracting loop-de-loops and showy tics", but complimented Cher that she "balances both aspects ideally".<ref name="NYDailyNews"/> A reviewer from [[Blogcritics]] labelled the album a "grab bag of tracks that don't really add up to cohesive album".<ref name="Blogcritics"/> Leah Greanblatt from ''[[Entertainment Weekly]]'' gave the album a "B" score, naming it a "shamelessly diva-fied mix of balladry, Broadway cabaret, and backroom boogie-woogie" and complimented on its musical departure from Aguilera's previous studio album ''Bionic''.<ref>{{cite web|url=http://www.ew.com/ew/article/0,,20442959,00.html|title=Burlesque Review|work=[[Entertainment Weekly]]|accessdate=May 3, 2014|date=November 17, 2010|first=Leah|last=Greanblatt}}</ref> In a positive review, ''Billboard'' editor Kerri Mason praised ''Burlesque'' as "a campy celebration of diva-dom and an over-the-top, triple-threat performance".<ref name="BillboardBiz"/> James Wigney of ''[[The Advertiser (Adelaide)|The Advertiser]]'' praised Aguilera's "vocal gymnastics", but was mixed towards Cher's numbers on the soundtrack.<ref name="Advertiser">Wigney, James (January 16, 2011). [http://www.adelaidenow.com.au/ipad/showgirl-aguilera-shows-her-skills/story-fn6br9e4-1225988297869?nk=8f39251496af0bba3cfa30a4fc7cab5f "Showgirl Aguilera shows her skills"]. ''[[The Advertiser (Adelaide)|The Advertiser]]''. Retrieved August 8, 2014.</ref> Maura Johnston from ''[[The Village Voice]]'' named "The Beautiful People (from ''Burlesque'')" as the thirteenth worst song of 2010, calling it "an interesting failure."<ref name="VV">{{cite web|title=The 20 Worst Songs of 2010, #13: Christina Aguilera, 'The Beautiful People (From Burlesque)'|work=[[The Village Voice]]|first=Maura|last=Johnston|url=http://blogs.villagevoice.com/music/2010/12/the_20_worst_so_7.php|date=December 7, 2010|accessdate=September 2, 2014}}</ref>

==Commercial performance==
On the US [[Billboard 200|''Billboard'' 200]], ''Burlesque'' debuted and peaked at number eighteen during the week of November 28, 2010, selling 63,000 copies in its first week.<ref>{{cite web|url=http://new.music.yahoo.com/blogs/chart_watch/70273/week-ending-nov-28-2010-the-king-and-queen-of-hip-hop/ |title=Week Ending Nov. 28, 2010: The King And Queen Of Hip-Hop|publisher=[[Yahoo! Music]]|first=Paul|last=Grein |date=December 1, 2010 |accessdate=May 3, 2014}}</ref> It was certified Gold by the [[Recording Industry Association of America]],<ref name="RIAA"/> having sold 707,000 copies in the US as of September 2014.<ref name=salesupdate2014/> On the Australian [[ARIA Charts|ARIA Albums Chart]], the soundtrack peaked at number two. It was certified Platinum by the [[Australian Recording Industry Association]] in 2015 for selling over 70,000 copies.<ref name="ARIA"/> ''Burlesque'' peaked at number 16 on the [[Canadian Albums Chart]] and was certified gold by the [[Music Canada]] for shipments of 40,000 units in the region.<ref name="MC"/> The soundtrack also gained commercial success on several [[record chart]]s: peaking at number five in Austria<ref name="Austria"/> and New Zealand,<ref name="NZ"/> and number eight in Switzerland.<ref name="Swiss"/>

==Track listing==
All songs performed by [[Christina Aguilera]], except two, which are performed by [[Cher]].
{{Track listing
| headline = ''Burlesque: Original Motion Picture Soundtrack''<ref>{{cite AV media notes|others=Christina Aguilera, Cher|title=Burlesque: Original Motion Picture Soundtrack|year=2010|type= inlay cover|publisher= Screen Gems, Inc|page=iTunes Digital Booklet}}</ref>
| extra_column    = Producer(s)
| writing_credits = yes
| total_length    = 31:53

| title1          = [[Something's Got a Hold on Me]]
| writer1         = {{flat list|
*[[Etta James]]
*Leroy Kirkland
*Pearl Woods
}}
| extra1          = [[Tricky Stewart|C. "Tricky" Stewart]]
| length1         = 3:04

| title2          = Welcome to Burlesque
| note2         = performed by Cher
| writer2         = {{flat list|
*[[Charlie Midnight]]
*[[Matthew Gerrard]]
*Steve Lindsey
*[[John Patrick Shanley]]
}}
| extra2          = {{flat list|
*Lindsey
*Gerrard
*Mark Taylor{{efn|Indicates a vocal producer|name=Vocal}}
}}
| length2         = 2:46

| title3          = Tough Lover
| writer3         = {{flat list|
*James
*Joe Josea
}}
| extra3          = Stewart
| length3         = 2:00

| title4          = But I Am a Good Girl
| writer4         = {{flat list|
*[[Jacques Morali]]
*Alain Bernardini
}}
| extra4          = Stewart
| length4         = 2:29

| title5          = Guy What Takes His Time
| writer5         = [[Ralph Rainger]]
| extra5          = [[Linda Perry]]
| length5         = 2:43

| title6          = [[Express (Christina Aguilera song)|Express]]
| writer6         = {{flat list|
*Aguilera
*Stewart
*[[Claude Kelly]]
}}
| extra6          = {{flat list|
*Stewart
*Kelly{{efn|name=Vocal}}
}}
| length6         = 4:20

| title7          = [[You Haven't Seen the Last of Me]]
| note7         = performed by Cher
| writer7         = [[Diane Warren]]
| extra7          = {{flat list|
*[[Matt Serletic]]
*Taylor{{efn|name=Vocal}}
}}
| length7         = 3:30

| title8          = Bound to You
| writer8         = {{flat list|
*Aguilera
*[[Samuel Dixon]]
*[[Sia Furler]]
}}
| extra8          = Dixon
| length8         = 4:23

| title9          = [[Show Me How You Burlesque]]
| writer9         = {{flat list|
*Aguilera
*Stewart
*Kelly
}}
| extra9          = {{flat list|
*Stewart
*Kelly{{efn|name=Vocal}}
}}
| length9         = 2:59

| title10         = [[The Beautiful People (song)#Cover versions|The Beautiful People (from ''Burlesque'')]]
| writer10        = {{flat list|
*[[Ron Fair]]
*[[Ester Dean]]
*[[Stefanie Ridel]]
*[[Tommy Lee James]]
*[[Nicole Scherzinger]]
*[[LP (singer)|Laura Pergolizzi]]
*[[The Phantom Boyz|Melvin K. Watson]]
*[[The Phantom Boyz|Larry Summerville, Jr.]]
*[[Marilyn Manson]]
*[[Jeordie White|Twiggy Ramirez]]
}}
| extra10         = {{flat list|
*Fair
*[[The Phantom Boyz]]
*Aguilera{{efn|name=Vocal}}
}}
| length10        = 3:31
}}

'''Notes'''
{{notelist}}

==Personnel==
Credits adapted from [[AllMusic]].<ref name="Credits">{{cite web|url=http://www.allmusic.com/album/burlesque-mw0002058722/credits|title=Burlesque: Credits|publisher=AllMusic|accessdate=June 19, 2014}}</ref> 

{{div col}}
* [[Christina Aguilera]] – vocal arrangement, vocal producer
* [[Cher]] – vocals
* Jess Collins – background vocals
* Gene Cipriano – tenor saxophone
* Lauren Chipman – viola
* Daphne Chen – violin
* Andrew Chavez – Pro-Tools
* Chris Chaney – bass
* Alejandro Carballo – trombone
* Frank Capp – castanets
* Jebin Bruni – piano
* Richard Brown – Pro-Tools
* Eddie Brown – piano
* Felix Bloxsom – drums
* Stevie Blacke – cello, viola, violin
* Charlie Bisharat – violin
* Robert Bacon – guitar
* Spring Aspers – executive in charge of music
* Keith Armstrong – mixing assistant
* Alex Arias – assistant, engineer, Pro-Tools
* Steven Antin – executive soundtrack producer
* Alex Al – acoustic bass
* Thomas Aiezza – assistant engineer
* Andrew Wuepper – engineer, horn engineer, percussion engineer
* Ben Wendell – saxophone
* Roy Weigand – trumpet
* Eric Weaver – assistant
* Ian Walker – contrabass
* [[Lia Vollack]] – executive in charge of music
* Gabe Veltri – engineer
* Rich King vocal producer
* Stephen Vaughan – photography
* Doug Trantow – engineer, Pro-Tools
* Brad Townsend – mixing
* Pat Thrall – engineer
* Brian "B-Luv" Thomas – engineer, horn engineer, percussion engineer
* Chris Tedesco – contracting
* Mark Taylor – vocal producer
* C. "Tricky" Stewart – producer, vocal producer
* Eric Spring – engineer
* Josh Freese – drums
* Ron Fair – arranger, producer, vocal arrangement, vocal producer
* Peter Erskine – drums
* Ron Dziubla – baritone sax
* George Doering – guitar
* Richard Dodd – cello
* Mark Dobson – engineer
* [[Samuel Dixon]] – percussion, producer, programming
* Buck Damon – music supervisor
* Jim Cox – horn arrangements, piano
* Pablo Correa – percussion
* Arturo Solar – trumpet
* Joel Shearer – guitar
* Gus Seyffert – acoustic bass, electric bass, baritone guitar
* Matt Serletic – arranger, keyboards, producer, programming
* The Section Quartet – strings
* Andrew Schubert – mixing
* John Salvatore Scaglione – guitar
* Oscar Ramirez – engineer, vocal engineer
* Christian Plata – assistant
* Phantom Boyz – arranger, keyboards, producer, programming
* Linda Perry – engineer, producer
* Gordon Peeke – drums, percussion
* Paul III – acoustic bass
* Ray Parker, Jr. – guitar
* Mimi Parker – assistant
* 'Lil' Tal Ozz – assistant
* Geoff Nudell – clarinet
* Michael Neuble – drums
* Luis Navarro – assistant
* Jamie Muhoberac – keyboards
* Dean Mora – horn arrangements, transcription
* [[Peter Mokran]] – mixing
* Jim McMillen – trombone
* Andy Martin – trombone
* Manny Marroquin – mixing
* Chris Lord-Alge – mixing
* Steve Lindsey – producer
* Mike Leisz – assistant
* Juan Manuel Leguizamón – percussion
* Greg Kurstin – piano
* Oliver Kraus – string arrangements, string engineer, strings
* James King – saxophone
* Claude Kelly – vocal producer
* Rick Keller – alto sax
* Nik Karpen – mixing assistant
* Alan Kaplan – trumpet
* Jaycen Joshua – mixing
* Graham Hope – assistant
* Mark Hollingsworth – tenor sax
* Dan Higgins – clarinet, baritone sax
* Tal Herzberg – engineer, Pro-Tools
* Trey Henry – bass
* Erwin Gorostiza – art direction, design
* Eric Gorfain – string arrangements, violin
* Matthew Gerrard – producer
* Jesus Garnica – assistant
* Brian Gardner – mastering
* Chris Galland – assistant
* James Gadson – drums
{{div col end}}

==Charts==
{{col-begin}}
{{col-2}}

=== Weekly charts ===
{| class="wikitable plainrowheaders sortable" style="text-align:center;"
|-
!Chart (2010–11)
!Peak<br />position
|-
{{albumchart|Australia|2|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|refname=ARIACharts|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Austria|5|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|refname=Austria|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Flanders|86|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|refname=HungMedien|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|BillboardCanada|16|artist=Soundtrack|refname="canada"|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Czech|29|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|refname="cezch"|date=201110|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Denmark|38|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Netherlands|78|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|France|108|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Germany|12|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|refname=ARIAChartsII|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Italy|14|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Mexico|11|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|New Zealand|5|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014|refname=NZ}}
|-
{{albumchart|Spain|69|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|Switzerland|8|artist=Soundtrack / Cher / Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014|refname=Swiss}}
|-
{{albumchart|UKComp|27|date=2011-01-15|rowheader=true|accessdate=May 3, 2014|refname=UKcomp}}
|-
{{albumchart|Billboard200|18|artist=Christina Aguilera|album=Burlesque|rowheader=true|accessdate=May 3, 2014}}
|-
{{albumchart|BillboardSoundtrack|1|artist=Soundtrack|rowheader=true|accessdate=May 3, 2014}}
|-
|-
!Chart (2015)
!Peak<br />position
|-
! scope="row" | UK Soundtrack Albums ([[Official Charts Company|OCC]])<ref>{{cite web|url=http://www.officialcharts.com/charts/soundtrack-albums-chart/2015-04-26  |title=Soundtrack Albums Chart Top 40 |publisher=[[Official Charts Company]] |accessdate=April 26, 2015}}</ref>
|9
|-
|}
{{col-2}}

===Year-end charts===
{|class="wikitable sortable plainrowheaders"
|-
!align="left"|Chart (2011)
! style="text-align:center;"|Position
|-
!scope="row"|Australian Albums Chart<ref>{{cite web|url=http://www.aria.com.au/pages/documents/ARIAEOYChart2011.pdf |title=ARIA End of Year Chart 2011 |publisher=ARIA Charts |accessdate=May 4, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20120414212836/http://www.aria.com.au/pages/documents/ARIAEOYChart2011.pdf |archivedate=April 14, 2012 |df= }}</ref>
| style="text-align:center;"|44
|-
!scope="row"|New Zealand Albums Chart<ref>{{cite web|url=http://nztop40.co.nz/chart/albums?chart=1862|title=Top Selling Albums of 2011|publisher=Recorded Music NZ|accessdate=June 19, 2014}}</ref>
| align="center"| 44
|-
!scope="row"|Swiss Albums Chart<ref>{{cite web|url=http://hitparade.ch/year.asp?key=2011 |title=Swiss Albums Chart: Year End 2011 |publisher=Swiss Music Charts |accessdate=May 4, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20130815083859/http://hitparade.ch/year.asp?key=2011 |archivedate=August 15, 2013 |df= }}</ref>
| align="center" |92
|-
!scope="row" align="left"|US ''Billboard'' 200<ref>{{cite web |  title=Best of 2011: Billboard 200 Albums | work=Billboard | url=http://www.billboard.com/charts/year-end/2011/the-billboard-200?page=5&begin=41&order=position | accessdate=May 4, 2014}}</ref>
| style="text-align:center;"|53
|-
!scope="row" align="left"|US ''Billboard'' Soundtracks<ref>{{cite web | title=Best of 2011: Soundtracks | work=Billboard | url=http://www.billboard.com/charts/year-end/2011/top-soundtracks-albums | accessdate=May 4, 2014}}</ref>
| style="text-align:center;"|3
|-
|}

{{col-end}}

==Certifications and sales==
{{certification Table Top}}
{{certification Table Entry|type=album|region=Australia|artist=Various Artists|title=Burlesque|award=Platinum|certyear=2015|relyear=2011|refname="ARIA"}}
{{certification Table Entry|type=album|region=Canada|artist=Various Artists|title=Burlesque|award=Gold|certyear=2011|relyear=2011|refname="MC"}}
{{certification Table Entry|title=Burlesque|artist=Various Artists|type=album|region=Japan|award=Gold|certyear=2012|relyear=2010|refname="GoldAwardJapan"}}
{{certification Table Entry|title=Burlesque|artist=Various Artists|type=album|region=United Kingdom|award=Silver|certyear=2014|relyear=2010|refname="BPI"}}
{{certification Table Entry|type=album|region=United States|artist=Various Artists|title=Burlesque|award=Gold|certyear=2011|relyear=2011|refname="RIAA"|salesamount=707,000<ref name=salesupdate2014/>}}
{{Certification Table Bottom}}

==Release history==
{| class="wikitable plainrowheaders"
|-
! scope="col" | Region
! scope="col" | Date
! scope="col" | Format
! scope="col" | Label
|-
! scope="row" | Belgium<ref>{{cite web|url=https://itunes.apple.com/be/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (BE)|accessdate=May 4, 2014}}</ref>
|rowspan="9"| November 19, 2010
| rowspan="8"| [[Music download|Digital download]]
| rowspan="8"| Screen Gems, Inc
|-
! scope="row"| France<ref>{{cite web|url=https://itunes.apple.com/fr/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (FR)|accessdate=May 4, 2014}}</ref>
|-
!scope="row" | Germany<ref>{{cite web|url=https://itunes.apple.com/de/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (DE)|accessdate=May 4, 2014}}</ref>
|-
! scope="row"| Norway<ref>{{cite web|url=https://itunes.apple.com/no/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (NO)|accessdate=May 4, 2014}}</ref>
|-
! scope="row"| Portugal<ref>{{cite web|url=https://itunes.apple.com/pt/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (PT)|accessdate=May 4, 2014}}</ref>
|-
! scope="row" | Spain<ref>{{cite web|url=https://itunes.apple.com/es/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (ES)|accessdate=May 4, 2014}}</ref>
|-
!scope="row"| United Kingdom<ref>{{cite web|url=https://itunes.apple.com/gb/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (GB)|accessdate=May 4, 2014}}</ref>
|-
! scope="row"| United States<ref>{{cite web|url=https://itunes.apple.com/us/album/burlesque-original-motion/id401685353|title=Burlesque (Original Motion Picture Soundtrack)|publisher=iTunes Store (US)|accessdate=May 4, 2014}}</ref>
|-
! scope="row"| Germany<ref>{{cite web|url=http://www.amazon.de/dp/B0045I6X90|title=Burlesque: Original Motion Picture Soundtrack|publisher=[[Amazon.com]] (DE)|accessdate=May 4, 2014}}</ref>
| rowspan="3"| [[Compact disc|CD]]
| rowspan="3"| [[RCA Records|RCA]]
|-
! scope="row" | United Kingdom<ref>{{cite web|url=http://www.amazon.co.uk/dp/B0045I6X90|title= Burlesque: Cher, Christina Aguilera|publisher=Amazon.com|accessdate=May 4, 2014}}</ref>
| rowspan="2"| November 22, 2010
|-
!  scope="row" | United States<ref>{{cite web|url=http://www.amazon.com/dp/B0043KK4N6|title= Burlesque – Original Motion Picture Soundtrack: Various, Christina Aguilera, Cher|publisher=Amazon.com|accessdate=May 4, 2014}}</ref>
|-
|}

==References==
{{Reflist|30em}}

==External links==
{{Wikipedia books|Burlesque (film)|''Burlesque'' (film)}}
Videos of song performances from the film on YouTube:
*{{YouTube|TZFQgqhNoEI|"Something's Got a Hold on Me"}} 
*{{YouTube|YDPR5EoYqOs|"I'm a Good Girl"}}
*{{YouTube|yDL90Gan57M|"The Beautiful People (from ''Burlesque'')"}}
{{Christina Aguilera}}
{{Cher}}

{{DEFAULTSORT:Burlesque (Soundtrack)}}
[[Category:2010 soundtracks]]
[[Category:Cher albums]]
[[Category:Christina Aguilera albums]]
[[Category:Film soundtracks]]
[[Category:RCA Records soundtracks]]
[[Category:Albums produced by Samuel Dixon]]