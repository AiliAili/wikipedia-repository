{{for|the baseball team, the Bristol Badgers|Bristol Badgers}}
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Badger
 | image=BBadger.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Fighter plane|Fighter]]-[[Reconnaissance aircraft|reconnaissance]]
 | national origin=[[United Kingdom]]
 | manufacturer=The Brititsh & Colonial Aeroplane Co. Ltd
 | designer=[[Frank Barnwell]]
 | first flight=4 February 1919
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=5
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Bristol Badger''' was designed to meet a [[United Kingdom|British]] need for a two-seat [[Fighter aircraft|fighter]]-[[reconnaissance]] aeroplane at the end of [[World War I]]. Despite the 1918 [[Armistice]], three Badgers were delivered to the [[Air Ministry|Air Board]] to develop air-cooled radial engines, particularly that which became the [[Bristol Jupiter]]; two other Badgers were also built.

==Development==
The Bristol Badger had its roots<ref name="Barnes">{{Harvnb|Barnes|1964|pp=134–7}}</ref> in the Type 22 F.2C, a proposed upgrade of the [[Bristol F.2 Fighter|Bristol F.2B]] using a 200&nbsp;hp (150&nbsp;kW) Salmson radial (Type 22), a 300&nbsp;hp (220&nbsp;kW) [[ABC Dragonfly]] radial (Type 22A), or a 230&nbsp;hp (170&nbsp;kW) [[Bentley B.R.2]] rotary (Type 22B).  The Type 23 Badger was a new design using the Dragonfly engine, drawn up at the end of 1917 to meet a two-seat fighter-reconnaissance role and owing a good deal to the [[Bristol Scout F]]. It was a single-bay [[biplane]] with strongly [[Stagger (aviation)|staggered]], unswept and unequal-span wings. The pilot and observed sat in tandem, the pilot in front under the upper-wing trailing edge and the observer behind with a ring-mounted 0.303&nbsp;in (7.7&nbsp;mm) [[Lewis Gun]]. At first, the Badger carried almost no fixed fin. Construction was the traditional wood-and-fabric of the time and the [[Landing gear|undercarriage]] was a single axle plus tailskid arrangement.<ref name="Barnes"/>

During the design process, it became clear that the  Dragonfly engine, was proving unreliable and Bristol looked to a new nine-cylinder, 400&nbsp;hp (300&nbsp;kW) radial produced by [[Cosmos Engineering|Brazil Straker]] and known then as the [[Cosmos Jupiter]] as a possible alternative. Later, this engine became the [[Bristol Jupiter]].<ref name="Barnes"/>

Bristol was awarded a contract to build three Badgers, two powered by the Dragonfly and one (the second) by a Jupiter. The first Badger flew on 4 February 1919 but crashed on this first flight with fuel supply problems. It was rebuilt with a larger rudder and delivered to the Air Board eleven days later. The second, Jupiter-engined Badger, flew on 24 May but was re-engined with a Dragonfly and was purchased by the Air Board in September. It had full armament and a fixed, rounded fin, introduced to cope with the heavier Jupiter engine. The Badger proved to have a lateral stability problem, an adverse yaw effect caused by aileron drag,<ref>{{Harvnb|Barnes|1964|p=161}}</ref> and because of this, the third machine was not accepted by the Air Board. These first three machines were designated Badger I.<ref name="Barnes"/>

Despite the instability and without having received a Jupiter-powered Badger, the Air Board were sufficiently encouraged by this engine's promise to order a fourth, fully armed Badger with this powerplant. After some testing, the [[rudder]] was modified with a horn balance and larger [[aileron]]s were fitted. This aircraft was the sole Badger II and was loaned by the Air Board to Bristol for the development of the Jupiter and its cowling during 1920-1.<ref name="Barnes"/>

The lateral stability problems of the Badger worried its designer Frank Barnwell because a 1/10 scale model had been carefully tested in the [[National Physical Laboratory (United Kingdom)|NPL]] wind tunnel without any alarm. Scaling from model to full size was a problem because the [[Reynolds number]]s reached in the atmospheric pressure wind tunnels of the time were much lower than those encountered in full-size flight.<ref>{{Harvnb|Anderson|1998|pp=296–305}}</ref> Flight tunnel tests also often involved the use of simplified aircraft models, with no attempt made to model the fuselage shape in detail. Using a spare set of Badger wings and [[empennage]], Barnwell designed a new, single-seat flat-sided and very simple fuselage made from [[plywood]] on a wooden frame for a fifth and final Badger, the Badger X. It first flew on 13 May 1919 and was Bristol's first civil registered aircraft,<ref>{{cite book |title= British Civil Register|last= |first= |authorlink= |coauthors= |year= |publisher= |location= |isbn=|page= |pages= |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EABU }}</ref> initially as ''K110'', then ''G-EABU'', but was never able to provide the intended comparative data with tunnel models, crashing on 22 May.<ref name="Barnes"/> 
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Badger II)==
<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->
{{aerospecs
|ref={{harvnb|Barnes|1964|pages=137}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=two
|capacity=
|length m=7.21
|length ft=23
|length in=8
|span m=11.20
|span ft=36
|span in=9
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.77
|height ft=9
|height in=1
|wing area sqm=33.2
|wing area sqft=357
|empty weight kg=885
|empty weight lb=1,950
|gross weight kg=885
|gross weight lb=1,429
|eng1 number=1
|eng1 type=[[Cosmos Jupiter I]] nine-cylinder air-cooled  radial
|eng1 kw=300
|eng1 hp=400
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|max speed kmh=228
|max speed mph=142
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=6,280
|ceiling ft=20,600
|climb rate ms=
|climb rate ftmin=
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Bristol Badger}}

===Notes===
{{reflist}}

===Bibliography===
*{{cite book |title= Bristol Aircraft since 1910|last= Barnes|first=C. H. |authorlink= |coauthors= |year=1964 |publisher=Putnam Publishing |location=London |isbn= 0-370-00015-3|page= |pages= |url=|ref=harv }}
*{{cite book |title= A History of Aerodynamics|last=Anderson|first=John D. |authorlink= |coauthors= |year=1998 |publisher=Cambridge University Press |location=Cambridge |isbn= 0-521-66955-3|page= |pages= |url=|ref=harv }}
{{refbegin}}
{{refend}}

<!-- ==External links== -->
{{Bristol aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Bristol Aeroplane Company aircraft|Badger]]