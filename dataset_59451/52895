{{Infobox website
| name = Sparrho
| logo = [[File:Sparrho.png|200px]]
| logo_size =          <!-- default 250px -->
| logo_alt = 
| logo_caption =       <!-- or: | logocaption =  -->
| screenshot = 
| collapsible =        <!-- set as "on", "y", etc, otherwise omit/leave blank -->
| collapsetext =       <!-- collapsible area's heading (default "Screenshot"); omit/leave blank if collapsible not set -->
| background =         <!-- for collapsetext heading; default grey (gainsboro) -->
| screenshot_size =    <!-- default 300px -->
| screenshot_alt = 
| caption = 
| url =                [http://www.sparrho.com www.sparrho.com]
| slogan =             Blending machine learning and expert curation to democratise science
| commercial =         
| type = 
| registration = 
| language = 
| num_users = 
| content_license =    
| programming_language = [[Clojure]]<ref>{{cite web|url=http://stackshare.io/sparrho/sparrho|title=Sparrho's technology stack|work=StackShare}}</ref>
| owner =              <!-- or: | owners = -->
| author =             Vivian Chan, Nilu Satharasinghe
| editor =             <!-- or: | editors = -->
| launch_date =        <!-- {{start date and age|YYYY|MM|DD|df=yes/no}} -->
| revenue = 
| alexa =              <!-- {{increase}} {{steady}} {{decrease}} [http://www.alexa.com/siteinfo/example.com ##] (US/Global MM/YYYY) -->
| ip = 
| current_status = 
| footnotes = 
}}
'''Sparrho''' combines human and [[artificial intelligence]] to help research professionals and layman users stay up-to-date with new scientific publications and patents.<ref>{{cite web|url=http://blog.sparrho.com/about|title=About Sparrho|work=Sparrho}}</ref> Sparrho's recommendation engine provides personalized scientific news-feeds by using proprietary [[machine learning]] algorithms to "aggregate, distill and recommend" relevant content.<ref>{{cite web|url=http://www.oxbridgebiotech.com/review/business-development/sparrho-discovery-platform-birds-eye-view-science/|title=Sparrho: the discovery platform with a bird’s eye view on science|work=Oxbridge Biotech Roundtable}}</ref> The platform aims to complement traditional methods of finding relevant academic material such as [[Google Scholar]] and [[PubMed]] with a system which enables the serendipitous discovery of pertinent content and across relevant scientific fields through user input-aided machine learning and contextual analysis.<ref>{{cite web|url=http://digitalmedia.strategyeye.com/article/JhAO5vNc8Vg/2014/09/02/interview_content_recommendation_platform_sparrho_talks_stay/|title=INTERVIEW: Content Recommendation Platform Sparrho Talks Staying On Top Of Science|work=StrategyEye - Digital Media}}</ref>

== Recommendation engine ==
Sparrho uses a "three pronged approach" to content recommendation. Firstly, "data-data analysis" is tackled using techniques such as [[natural language processing]] to provide appropriate research based on data provided by users.<ref>{{cite web|url=http://blog.sparrho.com/post/98321187242/peeking-under-the-hood-sparrhos-discovery-engine|title=Peeking under the hood: Sparrho's Discovery Engine|work=Sparrho}}</ref> Secondly, "user-user interactions" are utilized to propose a wider range of potentially relevant subject areas to users with similar interests . Finally, "user-data interactions" such as labeling articles as "relevant" or "irrelevant" within a particular scientific field allows Sparrho to personalize user newsfeeds.<ref>{{cite web|url=http://www.sparrho.com/how-it-works/|title=Sparrho - How does our recommendation engine for science work?|work=Sparrho}}</ref>

== History ==
Sparrho was founded in 2013 by Vivian Chan and Nilu Satharasinghe as a solution to issues in staying up to date with scientific literature encountered over the course of Chan's biochemistry PhD at the University of Cambridge.<ref>{{cite web|url=https://www.theguardian.com/technology/2013/dec/01/cambridge-university-internet-tech-startup|title=Cambridge's leading tech startups|author=Nicola Davis|work=The Guardian}}</ref><ref>{{cite web|url=http://www.joinef.com/why-ef/|title=Why EF|work=ef.}}</ref><ref name="nature.com">{{cite web|url=http://www.nature.com/news/how-to-tame-the-flood-of-literature-1.15806|title=How to tame the flood of literature|work=Nature News & Comment}}</ref> Initially established at Ideaspace in West Cambridge, and moving subsequently to Camden Collective<ref>{{cite web|url=http://blog.sparrho.com/post/100100099802/sparrho-finds-a-new-nest|title=Sparrho finds a new nest!|work=Sparrho}}</ref> and the Ministry of Startups<ref>{{cite web|url=http://startups.co.uk/tech-pitch-sparrho/|title=Tech Pitch: Sparrho|work=Startups.co.uk}}</ref> in London, the company is now based in the data science hub SHACK15 in Shoreditch, London<ref>{{cite web|url=http://news.shack15.com/this-search-engine-uses-ai-to-keep-scientists-updated/|title=This Search Engine Uses AI to Keep Scientists Updated|work=SHACK15 News}}</ref>.
In 2014, Chan was a semi-finalist for the 2014 Duke of York New Entrepreneur of the Year Award,<ref>{{cite web|url=http://www.nationalbusinessawards.co.uk/Content/Finalists|title=National Business Awards 2014 - Finalists|work=nationalbusinessawards.co.uk}}</ref><ref>{{cite web|url=http://www.cambridge-news.co.uk/Vivian-Chan-Sparrho-semifinalists-competing-Duke-York-New-Entrepreneur-Year-Award-week/story-22510610-detail/story.html|title=Vivian Chan of Sparrho one of ten semi-finalists competing for the Duke of York New Entrepreneur of the Year Award next week|work=Cambridge News}}</ref> and in 2015, Chan was included on the 35 Women Under 35 list compiled by [[Management Today]].<ref>{{cite web|url=http://www.managementtoday.co.uk/go/35-women-under-35-2015-features|title=I've never been regarded as weak' - 35 Women Under 35 2015|work=Management Today}}</ref> In 2016, Sparrho was a semi-finalist in [[Pitch@Palace]] 5.0.<ref>{{cite web|url=http://blog.sparrho.com/post/140388251747/sparrho-will-attend-pitchpalace-50-hosted-by-hrh|title=Sparrho will attend Pitch@Palace 5.0 hosted by HRH The Duke of York at St James Palace on 7th March 2016|work=Sparrho}}</ref>

== Competitors ==
Sparrho's primary competitors within the academic content recommendation field include [[PubChase]] and [[Scizzle]],<ref name="nature.com"/><ref>{{cite web|url=http://blogs.plos.org/tech/diving-haystack-make-hay/|title=Diving into the haystack to make more hay?|work=plos.org}}</ref> though both PubChase and Scizzle are targeted towards biomedical sciences and solely recommend academic journal papers.<ref name="pubchase.com">{{cite web|url=http://www.pubchase.com/|title=PubChase - discover life sciences research of interest to you.|work=PubChase}}</ref><ref>{{cite web|url=http://www.myscizzle.com/Index/faq|title=Scizzle - Frequently Asked Questions|work=Scizzle}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sparrho.com}}


[[Category:Articles created via the Article Wizard]]
[[Category:Search engine software]]
[[Category:Scholarly search services]]