{{other uses}}
{{coord|34|57|S|150|30|W|display=title}}
{{refimprove|date=March 2010}}
{{Infobox book <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = The Mysterious Island
| title_orig    = L'Île mystérieuse
| translator    = Agnes Kinloch Kingston and [[William Henry Giles Kingston|W. H. G. Kingston]] (1875)<br>[[Stephen William White|Stephen W. White]] (1876)<br>I. O. Evans (1959)<br>Lowell Bair (1970)<br>Sidney Kravitz (2001)<br>Jordan Stump (2001)
| image         = Ile Mysterieuse 02.jpg
| caption       = Cover page of ''The Mysterious Island''
| author        = [[Jules Verne]]
| illustrator   = [[Jules Férat]]
| cover_artist  =
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #12
| genre         = [[Adventure novel]]
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1874
| english_pub_date = 1874
| media_type    = Print ([[Hardcover|Hardback]])
| pages         =
| preceded_by   = [[Around the World in Eighty Days]]
| followed_by   = [[The Survivors of the Chancellor]]
}}

'''''The Mysterious Island''''' ({{lang-fr|L'Île mystérieuse}}) is a novel by [[Jules Verne]], published in [[1874 in literature|1874]]. The original edition, published by [[Pierre-Jules Hetzel|Hetzel]], contains a number of illustrations by [[Jules Férat]]. The novel is a [[Fictional crossover|crossover]] [[sequel]] to Verne's famous ''[[Twenty Thousand Leagues Under the Sea]]'' and ''[[In Search of the Castaways]]'', though its themes are vastly different from those books. An early draft of the novel, initially rejected by Verne's publisher and wholly reconceived before publication, was titled ''Shipwrecked Family: Marooned With Uncle Robinson,'' seen as indicating the influence on the novel of ''[[Robinson Crusoe]]''<ref>[http://www.unmuseum.org/verne.htm "Jules Verne: An Author Before His Time?"]</ref> and ''[[The Swiss Family Robinson]]''.<ref>''[[Science Fiction Studies]]'', [http://www.depauw.edu/sfs/birs/bir87.htm "Books in Review], July 2002</ref> Verne developed a similar theme in his later novel, ''[[Godfrey Morgan]]'' (French: ''L'École des Robinsons'', 1882).<ref>Nash, Andrew (2001-11-27). [http://www.julesverne.ca/vernebooks/jvbkschool.html ''École des Robinsons (L') - 1882'']. Retrieved on 2009-08-13.</ref>

==Plot summary==
The plot focuses on the adventures of five Americans on an uncharted island in the [[Australasia|South Pacific]].  During the [[American Civil War]], five northern prisoners of war decide to escape, during the siege of [[Richmond, Virginia]], by hijacking a balloon.

<!-- Please note: the names are correcriginal -->
The escapees are [[Cyrus Smith]], a [[railroad]] [[engineer]] in the [[Union (American Civil War)|Union]] army (named Cyrus Harding in [[William Henry Giles Kingston|Kingston]]'s version); his ex-slave and loyal follower [[Neb]] (short for [[Nebuchadnezzar]]); [[Bonadventure Pencroff]], a [[sailor]] (who is addressed only by his surname. In Kingston's translation, he is named Pencroft); his [[protégé]] and adopted son [[Harbert Brown]] (called Herbert in some translations); and the [[journalist]] [[Gedéon Spilett]] (Gideon Spilett in English versions). The company is completed by Cyrus' dog "Top".{{Ref|orig-names}}

After flying in a great storm for several days, the group crash-lands on a cliff-bound, volcanic, unknown island, described as being located at {{coord|34|57|S|150|30|W|}}, about {{convert|2500|km|mi}} east of New Zealand. They name it "Lincoln Island" in honor of their president, [[Abraham Lincoln]]. With the knowledge of the brilliant engineer Smith, the five are able to sustain themselves on the island, producing fire, pottery, bricks, [[nitroglycerin]], iron, a simple electric [[telegraph]], a home on a stony cliffside called "Granite House", and even a seaworthy ship, which they name the "Bonadventure" (in honor of Pencroff, the driving force behind its construction). They also manage to figure out their geographical location.
[[File:Ile Mysterieuse 03.jpg|left|thumb|Map of "Lincoln Island"]]

During their stay on the island, the group endures bad weather, and domesticates an [[orangutan]], Jupiter, abbreviated to Jup (or Joop, in Jordan Stump's translation).
There is a mystery on the island in the form of an unseen ''[[deus ex machina]]'', responsible for Cyrus' survival after falling from the balloon, the mysterious rescue of Top from a [[dugong]], the appearance of a box of equipment (guns and ammunition, tools, etc.), and other seemingly inexplicable occurrences.

The group finds a [[message in a bottle]] directing them to rescue a [[castaway]] on nearby [[Tabor Island]], who is none other than [[Tom Ayrton]] (from ''[[In Search of the Castaways]]''). On the return voyage to Lincoln Island, they lose their way in a tempest but are guided back to their course by a mysterious fire beacon.

Ayrton's former companions arrive by chance on Lincoln Island, and try to make it into their lair. After some fighting with the protagonists, the pirate ship is mysteriously destroyed by an explosion. Six of the pirates survive and kidnap Ayrton. When the colonists go to look for him, the pirates shoot Harbert, seriously injuring him. Harbert survives, but suffers from his injury, narrowly cheating death. The colonists at first assume Ayrton to have been killed, but later they find evidence that he was not instantly killed, leaving his fate uncertain. When the colonists rashly attempt to return to Granite House before Harbert fully recovers, Harbert contracts [[malaria]] and is saved by a box of quinine sulphate, which mysteriously appears on the table in Granite House. After Harbert recovers, they attempt to rescue Ayrton and destroy the pirates. They discover Ayrton at the sheepfold, and the pirates dead, without any visible wounds.

The secret of the island is revealed when it is discovered to be [[Captain Nemo]]'s hideout, and home port of the ''[[Nautilus (Verne)|Nautilus]]''. Having escaped the Maelstrom at the end of ''[[Twenty Thousand Leagues Under the Sea]]'', the ''Nautilus'' sailed the oceans of the world until all its crew except Nemo had died. Now an old man with a beard, Nemo returned the ''Nautilus'' to its secret port within Lincoln Island. Nemo had been the mysterious benefactor of the settlers, providing them with the box of equipment, sending the message revealing Ayrton, planting the mine that destroyed the pirate ship, and killing the pirates with an "electric gun". On his death bed Captain Nemo reveals his true identity as the lost Indian Prince Dakkar, son of a [[Raja]] of the then independent territory of [[Bundelkhand|Bundelkund]] and a nephew of the Indian hero [[Tipu Sultan|Tippu-Sahib]]. After taking part in the failed [[Indian Rebellion of 1857]], Prince Dakkar escaped to a [[deserted island]] with twenty of his compatriots and commenced the building of the ''Nautilus'' and adopted the new name of "Captain Nemo". Nemo also tells his life story to [[Cyrus Smith]] and his friends. Before he dies, he gives them a box of diamonds and pearls as a keepsake. Afterwards, he dies, crying "God and my country!" ("Independence!", in Verne's original manuscript). The ''Nautilus'' is scuttled and serves as Captain Nemo's tomb.{{Ref|nemo-time-paradox}}

Afterward, the island's central volcano erupts, destroying the island. Jup the orangutan falls into a crack in the ground and dies. The colonists, forewarned of the eruption by Nemo, find themselves safe but stranded on the last remaining piece of the island above sea level. They are rescued by the ship ''Duncan'', which had come to rescue Ayrton but were redirected by a message Nemo had previously left on Tabor Island. After they return to United States, they form a new colony in Iowa with Nemo's gift, and live happily ever after.

==Publication history in English==

In September 1875 Sampson Low, Marston, Low, and Searle published the first British edition of ''Mysterious Island'' in three volumes entitled ''Dropped from the Clouds'', ''The Abandoned'', and ''The Secret of the Island'' (195,000 words). In November, 1875 Scribners published the American edition of these volumes from the English plates of Sampson Low. The purported translator, [[William Henry Giles Kingston|W. H. G. Kingston]], was a famous author of boys' adventure and sailing stories who had fallen on hard times in the 1870s due to business failures, and so he hired out to Sampson Low as the translator for these volumes. However, it is now known that the translator of ''Mysterious Island'' and his other Verne novels was actually his wife, Agnes Kinloch Kingston, who had studied on the continent in her youth. The Kingston translation changes the names of the hero from "Smith" to "Harding"; "Smith" is a very common name in the UK and would have been associated, at that time, with the lower classes. In addition many technical passages were abridged or omitted and the anti-imperialist sentiments of the dying Captain Nemo were purged so as not to offend English readers. This became the standard translation for more than a century.

In 1876 the [[Stephen William White|Stephen W. White]] translation (175,000 words) appeared first in the columns of ''The Evening Telegraph'' of Philadelphia and subsequently as an Evening Telegraph Reprint Book. This translation is more faithful to the original story and restores the death scene of Captain Nemo, but there is still condensation and omission of some sections such as Verne's description of how a sawmill works. In the 20th century two more abridged translations appeared: the Fitzroy Edition (Associated Booksellers, 1959) abridged by I. O. Evans (90,000 words) and ''Mysterious Island'' (Bantam, 1970) abridged by Lowell Bair (90,000 words).

Except for the Complete and Unabridged Classics Series CL77 published in 1965 (Airmont Publishing Company, Inc), no other unabridged translations appeared until 2001 when the illustrated version of Sidney Kravitz appeared (Wesleyan University Press) almost simultaneously with the new translation of Jordan Stump published by Random House Modern Library (2001).  Kravitz also translated ''Shipwrecked Family:  Marooned With Uncle Robinson'', published by the North American Jules Verne Society and BearManor Fiction in 2011.

==''Wrecked On A Reef'' influence==
The 2003 English edition of ''Wrecked On A Reef'' (1869), a memoir by French shipwreck survivor [[François Édouard Raynal]], has additional appendices by French scholar Dr Christiane Mortelier who presents a case for the influence of Raynal's book on Verne's ''The Mysterious Island''. The ''[[Grafton (ship)|Grafton]]'' was wrecked near New Zealand on the [[Auckland Islands]] on 3 January 1864, where the crew of five survived for 19 months before obtaining rescue. ''Wrecked On A Reef'', Raynal's memoir of the incident, was very popular at the time of publication, being translated into multiple languages. According to Mortelier, Verne read Raynal's account and loosely based his novel on the true life story of ''Grafton'' shipwreck, survival, privation, and ultimate rescue.

==Translations into other languages==
The novel has been translated into [[Marathi language|Marathi]] by [[Bhaskar Ramachandra Bhagwat|B. R. Bhagwat]] titled 'निर्जन बेटावरचे धाडसी वीर'. The title roughly translates to 'Brave fighters on a deserted island'. It has a cult following in [[Maharashtra]], a state in India where the official language is Marathi.
The novel is translated to Malayalam, a South Indian language. The title of the book is NIGOODADWEEP and the translator is  Kesavan Nambisan.
The novel is also translated in [[Bengali language|Bengali]] by Shamsuddin Nawab from Sheba Prokashoni in 1979 titiled "Rahosshor Dip".

==Film and television adaptations==

[[File:Lobby Card for the 1929 version of The Mysterious Island.jpg|right|thumb|Lobby Card for the 1929 film version of ''The Mysterious Island'', filmed in [[Technicolor]] ]]
* [[20,000 Leagues Under the Sea (1916 film)|''20,000 Leagues Under the Sea'' (1916 film)]]: This classic American silent feature combines ''20,000 Leagues Under the Sea'' and ''The Mysterious Island'' into a single narrative, shifting back and forth between the ''Nautilus'' and the island.
* [[The Mysterious Island (1929 film)|''The Mysterious Island'' (1929 film)]]: loosely based on the back-story given for Captain Nemo in the novel. It is an American part-talking feature short largely in [[Technicolor#Process 3|Technicolor]], and features talking sequences, sound effects and synchronized music. Filmed as a silent but a talking sequence was added to the beginning and brief talking sequences were integrated into the film. Directed by [[Lucien Hubbard]] with [[Benjamin Christensen]] and [[Maurice Tourneur]].
* [[Mysterious Island (1941 film)|''Mysterious Island'' (1941 film)]]: a [[USSR]] production, directed by Eduard Pentslin.
* [[Mysterious Island (1951 film)|''Mysterious Island'' (1951 film)]]: directed by [[Spencer Gordon Bennet]].
* [[Mysterious Island (1961 film)|''Mysterious Island'' (1961 film)]]: directed by [[Cy Endfield]], also known as ''Jules Verne's Mysterious Island'', featuring special effects from [[Ray Harryhausen]] and [[Herbert Lom]] as Nemo and a score by [[Bernard Hermann]].
* The 1967 live-action/animated film ''[[The Stolen Airship]]'' by Czech film maker [[Karel Zeman]] is based loosely on Jules Verne's novels ''[[Two Years' Vacation]]'' and ''The Mysterious Island''.
* ''[[La Isla misteriosa y el capitán Nemo]]'' (''L'Île mystérieuse'') (1973): directed by [[Juan Antonio Bardem]] and [[Henri Colpi]]: a TV [[miniseries]] featuring [[Omar Sharif]] as Captain Nemo.
* [[Mysterious Island (TV series)|''Mysterious Island'']]: a Canadian television series that ran for one season in 1995.
* [[Mysterious Island (2005 film)|''Mysterious Island'' (2005)]]: a TV movie featuring [[Patrick Stewart]] as Captain Nemo which is only loosely based on the novel. Nominated for a Saturn award for best TV presentation.
*''[[Journey to the Mysterious Island]]'': a 2012 film loosely based on the novel, directed by [[Brad Peyton]], done as a sequel to an earlier adaptation of Verne's [[Journey to the Center of the Earth]] with [[Dwayne Johnson]] taking over the lead role from [[Brendan Fraser]].<ref>{{cite web|work=[[The New York Times]]|url=https://www.nytimes.com/2012/02/10/movies/journey-2-the-mysterious-island-starring-josh-hutcherson.html?_r=0|title=Volcanic Adventures in Jules Verne Country|first=Neil|last=Genzlinger|date=February 9, 2012}}</ref>
*''[[Jules Verne's Mysterious Island (2012)|Jules Verne's Mysterious Island]]'' - A 2012 cinematic adaptation, loosely based on the novel, made for the [[Syfy Channel]].

==Other works inspired by ''The Mysterious Island''==
[[File:Nemo s death.jpg|right|thumb|[[Cyrus Smith]] blessing [[Captain Nemo]] on his death bed in ''The Mysterious Island'']]
*The computer game ''[[Myst]]'', released 1993, and several locations featured in the game were also inspired by Jules Verne's novel.<ref>[https://www.wired.com/wired/archive/2.08/myst_pr.html Wired.com]</ref>
*[[Mysterious Island (Disney)|Mysterious Island]] is also the name of a themed land at [[Tokyo DisneySea]] opened in 2001 and features two attractions based on other Jules Verne novels, ''[[20,000 Leagues Under the Sea]]'' and ''[[Journey to the Centre of the Earth]]''.
*The 2002 novel ''[[Captain Nemo: The Fantastic History of a Dark Genius]]'' has the events of this novel based on 'real' events that occurred to the real Nemo, Andre, who gave the details of his encounters to Verne.
*The computer game ''[[Return to Mysterious Island]]'' (2004) is an [[adventure game]] sequel to the story. Its heroine, Mina, is shipwrecked alone on the uncharted island, and finds the body of the previous inhabitant, Captain Nemo (whom she buries). She finally escapes by locating the ''[[Nautilus (Verne)|Nautilus]]'' and disabling the island's defenses.<ref>[http://www.mysteriousislandgame.com/1/uk/ Mysteriousislandgame.com]</ref> On November 25, 2008 [[Microïds]] ([[Anuman Interactive]]'s adventure games label) announced that a sequel was being made, ''Return to Mysterious Island II''. It has been in development by [[Kheops Studio]] since April 2008, and was released on PC and Apple [[iPhone]] on August 14, 2009.<ref>[http://www.mysteriousislandgame.com/2/en/ Mysteriousislandgame.com]</ref>

==Notes==
#{{note|orig-names}} In the French original, some characters were named a little differently: Gédéon Spillet, Nabuchodonosor (Nab) and Harbert Brown. In the Kingston translation, the engineer is named Cyrus Harding, and the sailor is named Pencroft.
#{{note|nemo-time-paradox}} There are discrepancies in [[Continuity (fiction)|continuity]] between this novel and ''[[Twenty Thousand Leagues Under the Sea]]''. Although this novel was written in 1874, its events take place from 1865 to 1869. The events of ''Twenty Thousand Leagues Under the Sea'' take place between 1867 and 1868. For example, the Captain Nemo appearing in this novel dies at a time when the Captain Nemo in ''Twenty Thousand Leagues Under the Sea'' was still alive. There is usually a note in most editions of the book admitting date discrepancies. There are also similar discrepancies with ''[[In Search of the Castaways]]'', although, these are not as often pointed out.
{{Clear}}

==References==
{{Reflist|2}}

==External links==
{{commons category|The Mysterious Island}}
{{wikisourcelang|fr|L’Île mystérieuse}}
*{{wikisource-inline}}
{{Gutenberg|no=8993|name=The Mysterious Island}} Stephen W. White translation (1876)
{{Gutenberg|no=1268|name=The Mysterious Island}} W. H. G.Kingston (Mrs. Agnes Kinloch Kingston) translation (1875)
*[http://jv.gilead.org.il/kravitz/ ''The Mysterious Island''] Sidney Kravitz's unedited unabridged translation (2001). The extensive introduction and notes for this volume are at [http://www.ibiblio.org/julesverne/books/mi%20crit%20mat.pdf ''Mysterious Island Introduction''].
*[http://mysteryisland.narod.ru ''The Mysterious Island''] interactive 3D model on CryEngine 1 by Crytek
* {{librivox book | title=The Mysterious Island | author=Jules Verne}}
* [http://najvs.org North American Jules Verne Society]
* [http://commons.wikimedia.org/wiki/Category:The_Mysterious_Island Link to a map of Lincoln Island with English labels]

{{The Mysterious Island}}
{{Twenty Thousand Leagues Under the Sea}}
{{Verne}}
{{In Search of the Castaways}}
{{Pirates}}

{{Authority control}}

{{DEFAULTSORT:Mysterious Island, The}}
[[Category:1874 novels]]
[[Category:1870s fantasy novels]]
[[Category:Novels by Jules Verne]]
[[Category:1870s science fiction novels]]
[[Category:French science fiction novels]]
[[Category:Science fantasy novels]]
[[Category:American Civil War novels]]
[[Category:Pirate books]]
[[Category:Crossover novels]]
[[Category:Novels set on islands]]
[[Category:Fictional islands]]
[[Category:Novels set in Oceania]]
[[Category:Novels set in Virginia]]
[[Category:Indian Rebellion of 1857]]
[[Category:Novels about survival]]
[[Category:Southern United States in fiction]]
[[Category:Dogs in literature]]
[[Category:Fictional orangutans]]
[[Category:Castaways in fiction]]
[[Category:Adventure novels]]
[[Category:Uninhabited islands in fiction]]
[[Category:Novels adapted into comics]]
[[Category:French novels adapted into films]]
[[Category:Novels adapted into television programs]]
[[Category:Novels adapted into video games]]