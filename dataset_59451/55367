{{italic title}}
[[File:Agalma Title.jpg|thumb|Agalma Title]]
{{Infobox Journal
| title = Agalma
| cover =
| editor = [[Mario Perniola]]
| discipline = [[Philosophy]], [[Cultural Studies]], [[Aesthetics]]
| language = [[Italian language|Italian]]
| abbreviation =
| publisher = Mimesis
| country = [[Italy]]
| frequency = [[Biannual]]
| history = 2000–present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.agalmaweb.org/
| link1 =
| link1-name =
| link2 =
| link2-name =
| RSS =
| atom =
| JSTOR =
| OCLC = 499179471
| LCCN =
| CODEN =
| ISSN = 1723-0284
| eISSN =
}}
'''''Agalma: Rivista di Studi Culturali e di Estetica''''' is an [[academic journal]] dedicated to [[cultural studies]] and [[aesthetics]]. It was established in 2000 by the [[philosopher]] [[Mario Perniola]] and a group of [[Italy|Italian]] and international intellectuals <ref>[http://www.agalmaweb.org/redazione.php Official Editorial Board of Agalma]</ref> in response to the widely felt discontent with the international and Italian philosophical and cultural contexts at the time. Its purpose is to provide a forum for the theoretical work around [[cultural studies]] and aesthetics in Italy. The title of the journal reflects its aims, contents, and orientation. "Agalma" is an ancient [[Greek language|Greek]] word that means ornament, gift, image, and statue. It possesses a broad meaning in which [[economic]] value, [[aesthetic]] aspects, [[cultural studies]], and [[symbolic capital|symbolic]] power come together. The first part of an issue is usually [[monograph]]ic and connected to the theme of an international conference organized by the Chair of Aesthetics of the [[University of Rome Tor Vergata]] and other cultural institutions.<ref>[https://strategiedelbello.wordpress.com/2013/11/17/mario-perniola-agalma-che-cose-lintimita-ibs-roma/ Agalma's Presentation at the IBS]</ref> The other sections include discussions, interviews, and reviews covering a wide range of concepts associated to both Western and non-Western philosophical thought. Among the collaborators: [[Jean Baudrillard]],<ref>[http://www.agalmaweb.org/articoli.php?rivistaID=1 Baudrillard in Agalma: "Design e Dasein"]</ref> [[Bernardo Bertolucci]],<ref>[http://www.agalmaweb.org/articoli2.php?rivistaID=1 Bernardo Bertolucci Interview in Agalma]</ref> [[Luc Boltanski]], [[Peter Burke (historian)|Peter Burke]], [[Bruno Latour]], [[Michel Maffesoli]], [[Richard Shusterman]], [[Joseph Kosuth]].<ref>[http://www.agalmaweb.org/articoli1.php?rivistaID=9 Joseph Kosuth Interview in Agalma]</ref>

==External links==
* [https://web.archive.org/web/20150201194035/http://www.prismanews.net/libri/agalma-rivista-di-studi-culturali-e-di-estetica-n-22-divi-di-oggi-e-divi-di-ieri.html Review of the issue 22]
* [http://www.paradisodegliorchi.com/Mario-Perniola.35+M5263a29ea2e.0.html Interview with the Director of the journal]

==References==
{{Reflist}}

[[Category:Philosophy journals]]
[[Category:Italian-language journals]]
[[Category:Publications established in 2000]]
[[Category:Biannual journals]]
[[Category:Aesthetics publications]]