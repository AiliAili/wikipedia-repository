{{Use dmy dates|date=February 2015}}
{{Use South African English|date=February 2015}}
{{infobox military award
| name             = Efficiency Medal (South Africa)
| image            = [[File:Efficiency Medal (South Africa) George VI.jpg|300px]]
| caption          = King George VI version
| awarded_by       = [[George V|the Monarch of the United Kingdom and the British Dominions, and Emperor of India]]
| country          = {{flagicon| South Africa|1928}} [[Union of South Africa]]
| type             = Military long service medal
| eligibility      = Part-time other ranks and some officers
| for              = Twelve years of efficient service
| campaign         = 
| status           = Discontinued in 1952
| description      = 
| motto            = 
| clasps           = For further periods of 6 years service
| post-nominals    = 
| established      = 1930
| first_award      = 
| last_award       = 1952
| total            = 
| posthumous       = 
| recipients       = 
| precedence_label = South African order of wear
| individual       = 
| higher           = [[File:Flag of South Africa 1928-1994.svg|25px]] [[Efficiency Decoration (South Africa)]]
| same             = [[File:Flag of the United Kingdom.svg|25px]] [[Efficiency Medal]]<br>[[File:Flag of New Zealand.svg|25px]] [[Efficiency Medal (New Zealand)]]
| lower            = [[File:Flag of the United Kingdom.svg|25px]] [[Special Reserve Long Service and Good Conduct Medal]]
| related          = 
| image2           = [[File:Ribbon - Efficiency Medal (South Africa).png|x29px]]
| caption2         = Ribbon Bar
}}

The '''Efficiency Medal (South Africa)''' was instituted in 1930 for award to part-time warrant officers, non-commissioned officers and men after twelve years of efficient service on the active list of the Citizen Force of the [[Union of South Africa]]. At the same time, a clasp was instituted for award to holders of the medal upon completion of further periods of six years of efficient service. The medal superseded the [[Colonial Auxiliary Forces Long Service Medal]].<ref name="LGaz1930">{{londongazette |issue=33653 |startpage=6311 |date=17 October 1930}}</ref><ref name="Alexander">{{cite book|last = Alexander|first = EGM|authorlink = |author2=Barron, GKB |author3=Bateman, AJ |title = South African Orders, Decorations and Medals |publisher = Human and Rousseau Publishers|date = 1986|location = Cape Town|pages = 160|url = |doi = |id = |isbn = 0-7981-1895-4}}</ref>

The Efficiency Medal (South Africa) was superseded by the [[John Chard Medal]] in 1952.<ref name="Alexander"/><ref name="Union1939">[http://www.geocities.ws/militaf/mil39.htm South African Medal Website - Union Defence Forces (1939-52)] (Accessed 3 May 2015)</ref><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 - ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref>

==Origin==
In 1896, the [[Volunteer Long Service Medal for India and the Colonies]] was instituted by [[Queen Victoria]]. It was superseded by the Colonial Auxiliary Forces Long Service Medal in 1899.<ref name="Mayo499">{{cite book|last=Mayo|first=John Horsley|title=Medals and Decorations of the British Army and Navy, Vol. II, 1897 (No. 225. Volunteer Long Service Medal for Indian and Colonial Forces, 1896.)|url=https://archive.org/stream/medalsdecoration02mayouoft#page/498/mode/2up|year=1897|publisher=A. Constable|page=499|location=London}}</ref><ref name="LGaz1899">{{londongazette|issue=27085|startpage=3517|date=2 June 1899}}</ref>

==Institution==
The Efficiency Medal (South Africa) was instituted by Royal Warrant on 23 September 1930, as a long service award for part-time warrant officers, non-commissioned officers and men of the Citizen Force of the [[Union Defence Force (South Africa)|Union Defence Forces]]. At the same time, a clasp was instituted, for award to recipients of the medal upon completion of further periods of efficient service.<ref name="LGaz1930"/><ref name="Union1939"/><ref name="Fforde">{{cite book|last = Fforde |first = JPI|authorlink = |author2=Monick, S|title = A Guide to South African Orders, Decorations and Medals and their Ribbons 1896-1985|publisher = |date = 1986|location = Johannesburg|pages = 40|url = |doi = |id = |isbn = }}</ref>

The medal bears a subsidiary title to denote that the recipient qualified for its award while serving in the Citizen Force in South Africa. The subsidiary title, in English and Afrikaans, is inscribed on a scroll bar attached to the medal suspender.<ref name="LGaz1930"/><ref name="Mackay">Mackay, J., Mussell, J.W., Editorial Team of Medal News, (2005), ''The Medal Yearbook'', page 228, (Token Publishing Limited)</ref>

The similar award for officers was the [[Efficiency Decoration (South Africa)]].<ref name="Alexander"/>

==Award criteria==
The medal could be awarded to part-time warrant officers, non-commissioned officers and men after twelve years of continuous efficient service as a volunteer on the active list of the Citizen Force. Service in West Africa, natives of West Africa and periods spent on leave excluded, and war service were reckoned two-fold as qualifying service for the medal. Service during the period from 3 September 1939 to 1 March 1950 inclusive need not have been continuous, while breaks in service under certain specified conditions, though not counting as qualifying service,  were not considered as a break in the twelve years of continuous qualifying service for the medal.<ref name="LGaz1930"/><ref name="Alexander"/><ref name="Fforde"/><ref name="RegulationsEM">[http://medals.nzdf.mil.nz/warrants/e7reg.html New Zealand Defence Force - The Efficiency Medal Regulations] (Accessed 16 July 2015)</ref>

Clasps could initially be awarded to holders of the medal upon completion of eighteen and twenty-four reckoned years of efficient service. This was amended on 26 August 1944 to authorise the award of additional clasps for each additional completed period of six years of efficient service after twenty-four years. When medals are not worn, recipients of clasps would wear a silver rosette on the ribbon bar to donate each clasp.<ref name="LGaz1930"/><ref name="Warrant">[http://medals.nzdf.mil.nz/warrants/e7warrant.html New Zealand Defence Force - Royal Warrants for the Efficiency Medal] (Accessed 16 July 2015)</ref>

A further amendment, on 10 May 1946, made part-time officers who served during the [[World War II|Second World War]] also eligible for the award of the medal and clasp, provided they were serving on the active list of the Citizen Force on 2 September 1939 and were embodied or called up for war service. The reason for this amendment originated from the anomaly that, during the war, a large number of officers were commissioned from the ranks, and merely by the fact that they were so promoted owing to their efficiency, would be denied the right to the Efficiency Medal. Such officers were allowed to reckon their service as officers as qualifying service for the medal and clasps. Officers who had already qualified for the award of the Efficiency Decoration before that date were, however, not eligible.<ref name="Warrant"/><ref name="Hansard">[http://hansard.millbanksystems.com/lords/1945/nov/15/territorial-efficiency-medal Hansard - HL Deb 15 November 1945 vol 137 cc994-9 - Lord Croft on the Territorial Efficiency Medal] (Accessed 19 July 2015)</ref>

The medal was initially only awarded to Citizen Force members of the [[South African Army]] and [[South African Air Force]]. On 1 August 1942, when the [[South African Navy]] was established by the consolidation of the Seaward Defence Force and the South African Division of the [[Royal Naval Reserve|Royal Naval Volunteer Reserve]] into the South African Naval Forces, eligibility for the award of the medal was extended to South African Naval Citizen Force members.<ref name="Alexander"/><ref name="SANavy">{{cite web|url=http://www.navy.mil.za/aboutus/history/index.htm |title=History of the SA Navy |publisher=Navy.mil.za |accessdate=2012-07-12}}</ref>

==Order of wear==
In the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], the Efficiency Medal (South Africa) ranks on par with the British [[Efficiency Medal]] and takes precedence after the [[Territorial Efficiency Medal]] and before the [[Special Reserve Long Service and Good Conduct Medal]].<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3353|date=17 March 2003}}</ref>

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
[[File:SA & UK group miniatures.jpg|thumb|340px|Example of post-1952 South African order of wear. The medals depicted are:{{bulleted list|[[Southern Cross Medal (1952)]]|[[Permanent Force Good Service Medal]]|[[Order of the British Empire#Current awards|Member of the Most Excellent Order of the British Empire]]|[[1939–1945 Star]]|[[Africa Star]]|[[Italy Star]]|[[Defence Medal (United Kingdom)]]|[[War Medal 1939–1945|1939–1945 War Medal]]|[[Africa Service Medal]]|Efficiency Medal (South Africa)}}]]
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals which were applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African decorations and medals awarded to South Africans on or after that date. Of the official British medals which were applicable to South Africans, the Efficiency Medal (South Africa) takes precedence as shown.<ref name="Notice1982"/><ref name="London Gazette"/><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:Ribbon - Efficiency Decoration (South Africa).png|x37px|Efficiency Decoration (South Africa)]] [[File:Ribbon - Efficiency Medal (South Africa).png|x37px|Efficiency Medal (South Africa)]] [[File:Ribbon - Decoration for Officers of the Royal Naval Volunteer Reserve.png|x37px|Decoration for Officers of the Royal Naval Volunteer Reserve]]
* Preceded by the [[Efficiency Decoration (South Africa)]] (ED).
* Succeeded by the [[Decoration for Officers of the Royal Naval Volunteer Reserve]] (VRD).

==Description==
The medal was struck in silver and is oval, {{convert|39|mm|2|abbr=off}} high and {{convert|32|mm|2|abbr=off}} wide. The fixed suspender bar, a pair of laurel leaves, is affixed to the medal by means of a single-toe claw and a horizontal pin through the upper edge of the medal. The suspender is decorated on the obverse with a scroll-pattern bar, inscribed with the name of the country. The name of the recipient was impressed on the rim of the medal.<ref name="LGaz1930"/><ref name="Mackay"/>

;Obverse
Three versions of the medal were produced for South Africa, but only the first two were awarded. The obverse has a raised rim on all three versions and bears the crowned effigy of the reigning monarch.<ref name="Mackay"/><ref name="NorthEast">[http://www.northeastmedals.co.uk/britishguide/efficiency_medal_efficient_service.htm North East Medals - Efficiency Medal] (Accessed 17 July 2015)</ref>
* The original version has the effigy of [[George V|King George V]] in coronation robes and wearing the [[Tudor Crown (heraldry)|Tudor Crown]]. It is circumscribed "GEORGIVS•V•D•G•BRITT•OMN REX•ET•INDIÆ•IMP•". The initials "BM" at the bottom of the effigy are those of the designer of the obverse, Sir [[Bertram Mackennal]] [[Knight Commander of the Royal Victorian Order|KCVO]], an Australian sculptor.<ref name="Mackay"/><ref name="NorthEast"/><ref name="Mackennal">[http://adb.anu.edu.au/biography/mackennal-sir-edgar-bertram-7387 Australian Dictionary of Biography, Volume 10, (MUP), 1986 – Mackennal, Sir Edgar Bertram (1863–1931), article by Noel S. Hutchison] (Accessed 14 June 2015)</ref>
* The [[George VI|King George VI]] version was introduced after his succession to the throne in 1936 and has his effigy in coronation robes and wearing the Tudor Crown, facing left and circumscribed "GEORGIVS•VI•D•G•BR•OMN•REX•ET•INDIÆ•IMP•". It is identical to the first King George VI obverse design of the British Efficiency Medal. The initials "PM" below the effigy are those of the designer of the obverse, sculptor [[Percy Metcalfe]] [[Commander of the Royal Victorian Order|CVO]] [[Royal Designers for Industry|RDI]].<ref name="Alexander"/><ref name="Mackay"/><ref name="NorthEast"/><ref name="Metcalfe">[http://www.worldofcoins.eu/forum/index.php/topic,9021.0.html World of Coins - Percy Metcalfe, Coin Designer] (Accessed 17 July 2015)</ref>
[[File:Efficiency Medal (South Africa) Elizabeth II v1.jpg|thumb|Queen Elizabeth II version]]
* The [[Elizabeth II|Queen Elizabeth II]] version was introduced after her succession to the throne in 1952 and has her effigy, facing right and wearing the Tudor Crown. It is circumscribed "ELIZABETH II D: G: BR: OMN: REGINA F: D:" reading around from the top. It is identical to the first Queen Elizabeth II obverse design of the British Efficiency Medal.<ref name="Mackay"/><ref name="NorthEast"/>

;Scroll bar
Approved by a Royal Warrant dated 29 December 1939, the scroll bar inscription on the King George VI version of the medal was in English and Afrikaans, "UNION OF SOUTH AFRICA" and "UNIE VAN SUID-AFRIKA" in two lines. The scroll bar on the Queen Elizabeth II version was English only, but this medal was never awarded since it was superseded within a few months of the Queen's succession to the throne.<ref name="Alexander"/><ref name="Union1939"/><ref name="Notice1982"/><ref name="Fforde"/><ref name="NorthEast"/>

;Reverse
The reverse is smooth with a raised rim and bears the inscription "FOR EFFICIENT SERVICE" in three lines. On the bilingual King George VI version, the Afrikaans and English inscriptions are "VIR BEKWAME DIENS" and "FOR EFFICIENT SERVICE", each language in three lines and the languages separated by a 13 millimetres long line.<ref name="LGaz1930"/><ref name="Alexander"/><ref name="Union1939"/><ref name="Mackay"/><ref name="NorthEast"/>

[[File:Efficiency Medal clasp.jpg|thumb|124px|]]
;Clasp
The clasp, struck in silver, is decorated with an embossed Tudor Crown and was designed to be sewn onto the medal ribbon.<ref name="LGaz1930"/><ref name="NorthEast"/>

;Ribbon
The ribbon is 32 millimetres wide and dark green, edged with 3 millimetres wide lime yellow bands.<ref name="Fforde"/><ref name="Mackay"/>

==Discontinuation==
On 6 April 1952, the Efficiency Medal was superseded by the John Chard Medal, which could be awarded to all ranks after twelve years of continuous efficient service in the Citizen Force.<ref name="Alexander"/><ref name="Union1939"/><ref name="Notice1982"/>

==References==
{{Reflist|30em}}
{{South African military decorations and medals}}
{{Efficiency and long service decorations and medals}}

[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|405]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:Long and Meritorious Service Medals of Britain and the Commonwealth]]