{{DISPLAYTITLE:''Journal of the Royal Asiatic Society''}}
The '''''Journal of the Royal Asiatic Society''''' is an [[academic journal]] which publishes articles on the [[history]], [[archaeology]], [[literature]], [[language]], [[religion]] and [[art]] of [[South Asia]], the [[Middle East]] (together with [[North Africa]] and [[Ethiopia]]), [[Central Asia]], [[East Asia]] and [[South-East Asia]]. It has been published by the [[Royal Asiatic Society of Great Britain and Ireland]] since 1834.

{| class=wikitable
! Years !! Title
|-
| 1824&ndash;1834 || ''Transactions of the Royal Asiatic Society of Great Britain and Ireland''
|-
| 1835&ndash;1990 || ''The Journal of the Royal Asiatic Society of Great Britain and Ireland''
|-
| 1991&ndash;2004 || ''Journal of the Royal Asiatic Society''
|}

==Publications==
*{{cite book|title=Journal of the Royal Asiatic Society of Great Britain and Ireland, Volume 1|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland|edition=|volume=|year=1834|publisher=Cambridge University Press for the Royal Asiatic Society|url=https://books.google.com/books?id=MwABAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the North-China Branch of the Royal Asiatic Society, Volume 11|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. North-China Branch|edition=|volume=|year=1877|publisher=The Branch|url=https://books.google.com/books?id=Eg45AQAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the North China Branch of the Royal Asiatic Society, Volumes 11-12|author=Royal Asiatic Society of Great Britain and Ireland. North China Branch, Shanghai|last=|first=|others=|edition=|volume=|year=1877|publisher=Kelly & Walsh.|url=https://books.google.com/books?id=5kADAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|archiveurl=https://books.google.com/books?id=J35JAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|archivedate=Nov 11, 2009|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the Royal Asiatic Society of Great Britain & Ireland, Volume 17|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland|edition=|volume=|year=1885|publisher=Cambridge University Press for the Royal Asiatic Society|url=https://books.google.com/books?id=vwRp8-H9NxsC&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the Royal Asiatic Society of Great Britain & Ireland, Volume 19|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland|edition=|volume=|year=1887|publisher=Cambridge University Press for the Royal Asiatic Society|url=https://books.google.com/books?id=JLFfVFU1mCoC&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the North China Branch of the Royal Asiatic Society, Volume 24|author=Royal Asiatic Society of Great Britain and Ireland. North China Branch, Shanghai|last=|first=|others=Contributor China Branch of the Royal Asiatic Society|edition=|volume=|year=1890|publisher=Kelly & Walsh.|url=https://books.google.com/books?id=LVkDAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the China Branch of the Royal Asiatic Society for the Year ..., Volumes 24-25|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. China Branch|edition=|volume=|year=1890|publisher=The Branch|url=https://books.google.com/books?id=saJDAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}


*{{cite book|title=Journal of the North-China Branch of the Royal Asiatic Society, Volumes 26-27|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. North-China Branch|edition=|volume=|year=1894|publisher=The Branch|url=https://books.google.com/books?id=zKRBAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=8 October 2014| ref=harv }}
*{{cite book|title=Journal of the North China Branch of the Royal Asiatic Society, Volumes 26-27|author=Royal Asiatic Society of Great Britain and Ireland. North China Branch, Shanghai|last=|first=|others=|edition=|volume=|year=1894|publisher=Kelly & Walsh.|url=https://books.google.com/books?id=7TwDAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=8 October 2014| ref=harv }}


*{{cite book|title=Journal of the North China Branch of the Royal Asiatic Society, Volumes 28-29|author=Royal Asiatic Society of Great Britain and Ireland. North China Branch, Shanghai, China Branch of the Royal Asiatic Society, Royal Asiatic Society of Great Britain and Ireland. China Branch, Shanghai Literary and Scientific Society|last=|first=|others=|edition=|volume=|year=1898|publisher=Kelly & Walsh.|url=https://books.google.com/books?id=Aj4DAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the North-China Branch of the Royal Asiatic Society, Volume 29|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. North-China Branch|edition=|volume=|year=1895|publisher=The Branch|url=https://books.google.com/books?id=IKVBAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the China Branch of the Royal Asiatic Society for the Year ..., Volume 29|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. China Branch|edition=|volume=|year=1895|publisher=The Branch|url=https://books.google.com/books?id=4KNDAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the China Branch of the Royal Asiatic Society for the Year ..., Volume 30, Issue 1|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. China Branch|edition=|volume=|year=1897|publisher=The Branch|url=https://books.google.com/books?id=5J6gAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the Straits Branch of the Royal Asiatic Society, Volumes 31-33|author=|last=|first=|others=Contributor Royal Asiatic Society of Great Britain and Ireland. Straits Branch|edition=|volume=|year=1898|location=Singapore|publisher=Printed At The American Mission Press|url=https://books.google.com/books?id=oPgaAAAAYAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Transactions of the Royal Asiatic Society of Great Britain and Ireland, Volume 1|author=|last=|first=|others=|edition=|volume=|year=1827|publisher=Parbury|url=https://books.google.com/books?id=qaJSAAAAcAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|isbn=|accessdate=24 April 2014| ref=harv }}
*{{cite book|title=Journal of the Royal Asiatic Society of Great Britain & Ireland|editor-first=|editor-last=|volume=|edition=|year=1965|publisher=|url=https://books.google.com/books?id=1QksAQAAIAAJ|isbn=|accessdate=24 April 2014| ref=harv }}

== External links ==
{{Portal|Asia}}
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=JRA}}
* [http://www.royalasiaticsociety.org/site/?q=taxonomy/term/9 Journals] at the Royal Asiatic Society of Great Britain and Ireland
* [http://www.jstor.org/publisher/rasgbi Royal Asiatic Society of Great Britain and Ireland] at [[JSTOR]]

[[Category:Multidisciplinary humanities journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:Publications established in 1824]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]


{{humanities-journal-stub}}