{{Chembox
<!-- Images -->
| ImageFile =Gephyronic Acid.png 
| ImageSize = 
| ImageAlt =
<!-- Names -->
| IUPACName = 
| OtherNames =
<!-- Sections -->
| Section1 = {{Chembox Identifiers
| CASNo = 
| PubChem = 
| SMILES = 
  }}
| Section2 = {{Chembox Properties
| Formula = C26H48O8
| MolarMass = 488.660 g/mol
| Appearance = 
| Density = 
| MeltingPt = 
| BoilingPt = 
| Solubility = 
  }}
| Section3 = {{Chembox Hazards
| MainHazards =
| FlashPt =
| AutoignitionPt =
  }}
}}'''Gephyronic acid''' is a [[polyketide]] that exists as an equilibrating mixture  of structural isomers. In nature, Gephyronic acid is produced by slow growing [[Myxobacteria|myxobacterium]]: Archangium gephyra strain Ar3895 and Cystobacter violaceus strain Cb vi76.<ref>{{Cite journal|last=Wenzel|first=Silke C.|last2=Müller|first2=Rolf|date=2009-05-21|title=Myxobacteria—‘microbial factories’ for the production of bioactive secondary metabolites|url=http://xlink.rsc.org/?DOI=b901287g|journal=Molecular BioSystems|language=en|volume=5|issue=6|doi=10.1039/b901287g|issn=1742-2051|page=567}}</ref> It is the first antibiotic in myxobacteria that was reported to specifically inhibit eukaryotic protein synthesis.

== Biological Properties ==
Preliminary studies demonstrated that Gephyronic acid inhibited growth of yeast and molds as well as elicited a [[Cytostasis|cytostatic]] effect through the inhibition of eukaryotic protein synthesis in mammalian cell cultures. Feeding experiments done with radioactive precursors showed a drastic difference in incorporation of leucine by a human leukemic cell like K-562, but little difference in the incorporation of uridine and thymidine.<ref name=":1">{{Cite journal|last=Sasse|first=F.|last2=Steinmetz|first2=H.|last3=Höfle|first3=G.|last4=Reichenbach|first4=H.|date=1995-01-01|title=Gephyronic acid, a novel inhibitor of eukaryotic protein synthesis from Archangium gephyra (myxobacteria). Production, isolation, physico-chemical and biological properties, and mechanism of action|journal=The Journal of Antibiotics|volume=48|issue=1|pages=21–25|issn=0021-8820|pmid=7868385|doi=10.7164/antibiotics.48.21}}</ref> This suggested that the primary target of gephyronic acid is protein synthesis. As such, it is a potential target for cancer [[chemotherapy]]. Gene expression profiling of human breast cancer cell lines is underway in an effort to further define the potential of Gephyronic acid as a chemotherapeutic lead.<ref>{{Cite journal|last=Young|first=Jeanette|last2=Leliaert|first2=Amy|last3=Schafer|first3=Zachary T.|last4=Taylor|first4=Richard E.|date=2014-10-28|title=Abstract C67: Gene expression profiling with human breast cancer cell lines exposed to gephyronic acid|url=http://cancerres.aacrjournals.org/lookup/doi/10.1158/1538-7445.FBCR11-C67|journal=Cancer Research|volume=71|issue=18 Supplement|pages=C67|doi=10.1158/1538-7445.fbcr11-c67}}</ref>

Screening of a library of compounds derived from myxobacteria found that Gephyronic acid was the strongest inhibitor of [[P-bodies|processing bodies]] (P-bodies) assembly.<ref name=":2">{{Cite journal|last=Martínez|first=Javier P.|last2=Pérez-Vilaró|first2=Gemma|last3=Muthukumar|first3=Yazh|last4=Scheller|first4=Nicoletta|last5=Hirsch|first5=Tatjana|last6=Diestel|first6=Randi|last7=Steinmetz|first7=Heinrich|last8=Jansen|first8=Rolf|last9=Frank|first9=Ronald|date=2013-11-01|title=Screening of small molecules affecting mammalian P-body assembly uncovers links with diverse intracellular processes and organelle physiology|url=http://dx.doi.org/10.4161/rna.26851|journal=RNA Biology|volume=10|issue=11|pages=1661–1669|doi=10.4161/rna.26851|issn=1547-6286|pmc=3907476|pmid=24418890}}</ref> P-bodies are discrete cytoplasmic mRNP granules that contain non-translating mRNA and protein from the mRNA decay pathway and from miRNA silencing machinery. Within P-bodies, mRNAs can be degraded, but components of P-bodies can rapidly cycle in and out to return to translation.<ref name=":2" /> The mechanism of P-body assembly inhibition by Gephyronic acid has not been characterized, but initial studies suggest that the mode of action could be through stalling ribosomes on mRNA or by reflecting early steps of translation initiation, such as binding of ribosomal subunits or initiation factors.

The same study also found that Gephyronic acid inhibits eIF2α-phosphorylation and formation of stress granules under stress conditions. Stress granules contain non-translating mRNAs and translation initiation factors, suggesting that they may form as a result of aggregation of mRNPs stalled during translation initiation.<ref name=":2" /> By monitoring immunofluresence of an established stress granules marker, it was found that stress granule formation was inhibited in the presence of Gephyronic acid. Gephyronic acid may have a direct or indirect effect on translation initiation factor eIF2α, which would trap mRNA into nonfunctional initiation complexes, inhibiting both P-body and stress granule formation.<ref name=":2" />

== Biosynthesis ==
Sequencing of the PKS gene cluster in C. violaceus was conducted and confirmed to reveal five type I polyketide synthases and post-PKS tailoring enzymes with ''[[O-methyltransferase|O]]''[[O-methyltransferase|-methyltransferase]] and [[cytochrome P450]] monooxygenase.<ref name=":0" /> The overall structure correlates well with modular arrangement of PKS-encoded proteins, aside from some unexpected elements that are likely caused by inactive domains. Initial loading uses a GCN5-related ''[[N-acetyltransferase|N]]''[[N-acetyltransferase|-acetyltransferase]] (GNAT) domain instead of the typical AT domain.[[File:Gephyronic biosynthesis3.png|thumb|753x753px|Model of gephyronic acid biosynthesis. PKS domains: GNAT (N-acetyltransferase), ACP (acyl carrier protein), KS (ketosynthase), AT (acyltransferase), ATo (inactive acyltransferase), DH (dehydratase), DHo (inactive dehydratase), MT (methyltransferase), ER (enoylreductase), KR (ketoreductase).<ref name=":0">{{Cite journal|last=Young|first=Jeanette|last2=Stevens|first2=D. Cole|last3=Carmichael|first3=Rory|last4=Tan|first4=John|last5=Rachid|first5=Shwan|last6=Boddy|first6=Christopher N.|last7=Müller|first7=Rolf|last8=Taylor|first8=Richard E.|date=2013-12-27|title=Elucidation of Gephyronic Acid Biosynthetic Pathway Revealed Unexpected SAM-Dependent Methylations|url=http://pubs.acs.org/doi/abs/10.1021/np400629v|journal=Journal of Natural Products|volume=76|issue=12|pages=2269–2276|doi=10.1021/np400629v|issn=0163-3864|pmid=24298873}}</ref>|center]][[File:Mechanism of SAM O-methyltransferase.png|thumb|394x394px|Mechanism of SAM mediated methylation]]Gephyronic acid contains a methyl ether at C-5 and a C-12/C-13 epoxide. These functional groups are incorporated by post-PKS tailoring enzymes. GphA is likely responsible for installation of the C-5 methyl ether. O-methyltransferases SpiB and SpiK used in spirangien biosynthesis exhibit the same SAM-binding motif as GphA.<ref>{{Cite journal|last=Clarke|first=Steven|date=2002-02-05|title=The methylator meets the terminator|url=http://www.pnas.org/content/99/3/1104|journal=Proceedings of the National Academy of Sciences|language=en|volume=99|issue=3|pages=1104–1106|doi=10.1073/pnas.042004099|issn=0027-8424|pmc=122150|pmid=11830650}}</ref>[[File:Mechanism of p450 epoxidation.tif|thumb|395x395px|Consensus mechanism of P450 epoxidation in EpoK]]
GphK is a member of the [[Cytochrome P450|cytochrome p450]] superfamily and is suspected to carry out the epoxidation of the C12-C13 olefin. Such epoxidation in post-PKS modications has been seen in [[epothilone]] biosynthesis by EpoK.<ref name=":1" /> In EpoK, the consensus mechanism of epoxidation by P450 involves the formation of a pi-complex between an oxoferryl pi-cation radical species (Fe<sup>IV</sup>) and the olefin pi bond, followed by electron transfer, formation of the olefin pi-cation radical and finally epoxidation.<ref>{{Cite journal|last=Kells|first=Petrea M.|last2=Ouellet|first2=Hugues|last3=Santos-Aberturas|first3=Javier|last4=Aparicio|first4=Jesus F.|last5=Podust|first5=Larissa M.|date=2010-08-27|title=Structure of Cytochrome P450 PimD Suggests Epoxidation of the Polyene Macrolide Pimaricin Occurs via a Hydroperoxoferric Intermediate|url=http://www.sciencedirect.com/science/article/pii/S1074552110002516|journal=Chemistry & Biology|volume=17|issue=8|pages=841–851|doi=10.1016/j.chembiol.2010.05.026|pmc=2932657|pmid=20797613}}</ref>

However, it is also possible that in addition to cytochrome p450, a FAD-dependant monoxygenase is also required to install the epoxide. This codependent process is seen in tirandamycin biosynthesis by TamL.<ref name=":1" /> Experiments to clarify the function of these enzymes in gephyronic acid biosynthesis are underway.<ref name=":0" />

== References ==
<references />

[[Category:Polyketides]]