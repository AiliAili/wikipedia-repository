{{Infobox laboratory
| name             =  Institute for Biodiversity and Environmental Research
| native_name      = Institut Penyelidikan Biodiversiti dan Alam Sekitar
| image            = 
| caption          = 
| latin_name       = 
| motto            = 
| established      = {{start date|1985}}
| type             =  <!--Research type (basic, applied, classified, unclassified, …)-->
| budget           = 
| debt             = 
| research_field   = Biodiversity and Environment
| president        = 
| vice-president   = 
| dean             = 
| director         =  <!--or: | directors = -->
| head_label       = 
| head             = 
| faculty          = 
| staff            = 
| students         = 
| alumni           = 
| address          = 
| city             =  <!--(required if location to be displayed)-->
| state            = 
| province         = 
| country          = Brunei Darussalam
| coor             =  <!--{{coord|LAT|LON|type:landmark|display=inline,title}}-->
| zipcode          = 
| campus           = 
| free_label       = 
| free             = 
| affiliations     = 
| operating_agency = 
| nobel_laureates  =  <!--Number and/or names of Nobel laureates associated with the laboratory/institute-->
| website          =  <!--{{URL|example.edu}}-->
| logo             = 
| footnotes        = 
}}

'''The Institute for Biodiversity and Environmental Research''' ([[Abbreviation]]: '''IBER'''; {{lang-ms|Institut Penyelidikan Biodiversiti dan Alam Sekitar}}; [[Jawi script|Jawi]]: اينستيتوت ڤڽليديقن بيوديۏرسيتي دان عالم سکيتر) in [[Brunei|Brunei Darussalam]] ([[Borneo]]/[[Southeast Asia]]) is a research institute of [[Universiti Brunei Darussalam]] (UBD) dedicated to [[biodiversity]]  and [[environmental science]]s research and education. IBER’s location within the northwest Borneo hotspot offers unique opportunities for long-term studies in both terrestrial and marine tropical ecosystems.

== History ==

Biodiversity and environment have  been the university's research strengths since its founding in 1985.<ref name=iber>{{cite web|title=Institute for Biodiversity and Environmental Research|url=http://www.ubd.edu.bn/faculties-and-institutes/iber|publisher=Universiti Brunei Darussalam|accessdate=18 December 2013}}</ref> This was further developed  through the Brunei Rainforest Project, a collaborative project between UBD and the [[Royal Geographical Society]] (RGS) in the establishment of a permanent field research facility in the [[Ulu Temburong National Park]] - the Kuala Belalong Field Studies Centre (KBFSC)  where a 14-month scientific expedition was carried out in 1991-1992.<ref name=rgs>{{cite web|title=Brunei Rainforest project 1991-1992|url=http://www.rgs.org/OurWork/Fieldwork+and+Expeditions/Resources/Past+Field+Programmes/Brunei/Brunei+Rainforest+Project+1991-1992.htm|publisher=Royal Geographical Society|accessdate=19 December 2013}}</ref> Since then, KBFSC has gained local and international recognition as a centre for tropical rainforest research and education in Borneo<ref name=lifecanopy>{{cite book|title=Life Underneath the canopy|year=2011|publisher=Universiti Brunei Darussalam|isbn=99917-1-242-9|pages=82|editor=Hjh Anis F.D. |editor2=Tennakoon, K. |editor3=Malai Alya S.M.A}}</ref>
UBD has participated in and led several expeditions to forest areas within the [[Heart of Borneo]] (HoB) boundary<ref name=hobforestry>{{cite web|title=Heart of Borneo|url=http://www.forestry.gov.bn/heart.htm|publisher=Forestry Department, Brunei Darussalam|accessdate=19 December 2013}}</ref> including the [[Sarawak]] Forestry Department expeditions to [[Lanjak Entimau Wildlife Sanctuary|Lanjak Entimau]] and Paya Maga, the UBD-led biodiversity surveys to Sungai Ingei<ref name=Ingeiwwf>{{cite web|title=Sungai Ingei Biodiversity Survey|url=http://wwf.panda.org/?209726/Sungei-Ingei-Faunal-Biodiversity-Survey wwf.panda.org|publisher=World Wildlife Fund|accessdate=19 December 2013}}</ref><ref>{{cite web|title=Heart of Borneo Seminar: Sungai Ingei Biodiversity Survey|url=http://www.bruneimipr.gov.bn/index.php?option=com_content&view=article&id=569%3Aheart-of-borneo-seminar--sungai-ingei-faunal-biodiversity-survey&catid=96%3Aarchieve&Itemid=48|publisher=Ministry of Industry and Primary Resources, Brunei Darussalam|accessdate=19 December 2013}}</ref> Conservation Forest and the iCUBE Scientific Expedition to Bukit Pagon, Brunei Darussalam.<ref name=iber /><ref name=pagonicube>{{cite web|title=iCUBE Expedition to Bukit Pagon, Brunei|url=http://www.icubeconsortium.org/index.php/programmes/expedition|publisher=iCUBE Consortium|accessdate=19 December 2013}}</ref>
Following these significant milestones achieved in the biodiversity and environment research frontier, IBER was established in 2013 to further develop and expand prospects and international collaborations in biodiversity explorations within Brunei Darussalam.<ref name=discubd>{{cite journal|title=The Official Establishment of IBER, FIT and CARe|journal=Discover UBD|date=July–September 2013|issue=11|pages=5|url=http://www.ubd.edu.bn/Resources/docs/ubdMagazine/discover%20ubd%20issue%2011.pdf|accessdate=19 December 2013|publisher=Universiti Brunei Darussalam}}</ref>  The Institute’s establishment was officially announced by [[Hassanal Bolkiah|His Majesty the Sultan and Yang Di-Pertuan of Brunei Darussalam]], Chancellor of Universiti Brunei Darussalam, at UBD’s 25th Convocation Ceremony on 12 September 2013.<ref name=iberbrudirect>{{cite web|title=IBER: A centre of excellence for ecosystem studies|url=http://www.brudirect.com/national/national/national-local/8527-iber-a-centre-of-excellence-for-ecosystem-studies www.brudirect.com|publisher=brudirect.com|accessdate=19 December 2013}}</ref>

== Scope ==

IBER was set up as a focal agency within UBD that functions to coordinate, facilitate, support and execute research, education and outreach activities or programmes within the biodiversity and environmental scope (both terrestrial and marine).<ref name=researchhighlight>{{cite web|title=Research at the Institute for Biodiversity and Environmental Research|url=http://www.ubd.edu.bn/Resources/docs/IBER/Research%20highlights.pdf|publisher=Universiti Brunei Darussalam|accessdate=28 January 2014}}</ref> KBFSC, previously an independent research centre of UBD, is now managed as the premier international research and educational facility for IBER and remains as a core research focus of UBD.<ref name=discubd />

== Research ==

Brunei Darussalam is one of the top ten forested nations in the world,<ref name=fao>{{cite web|title=Global Forest Resources Assessment 2010|url=http://www.fao.org/docrep/013/i1757e/i1757e.pdf|publisher=fao.org|accessdate=19 December 2013}}</ref> and is situated in the hyperdiverse northwest Borneo tropical forests and the edge of the [[Coral Triangle]]. Recognising Brunei's strengths and prominence in biodiversity, the university has biodiversity and environmental studies as one of its five key research areas.

Biodiversity and environmental research at the institute is conducted through university- and government-funded research projects, as well as international.<ref name=iberbrudirect /> A major initiative of the institute is the establishment of the [[International Consortium of Universities for the study of Biodiversity and the Environment]] ([[iCUBE]]).<ref name=icubeestablishmt>{{cite web|title=International Consortium of Universities for the study of Biodiversity and the Environment (iCUBE)|url=http://www.icubeconsortium.org/|publisher=iCUBE Consortium|accessdate=19 December 2013}}</ref>   This group brings together a core group of research universities ([[King's College London]], [[Korea University]], [[Monash University]], [[National University of Singapore]],  [[Universiti Brunei Darussalam]], [[University of Auckland]],  [[University of Bonn]], and [[University of North Carolina at Chapel Hill]]) united in a shared goal of research and education on biodiversity and the environment.

The institute  has a network of 1 ha permanent forest plots located throughout Brunei Darussalam. Set up in the early 1990s, five plots are located in lowland Mixed Dipterocarp Forests in the Belait and Tutong districts and at KBFSC, and two heath (Kerangas) forest plots are located in the Belait district.<ref name=davies>{{cite journal|author1=Davies S.J. |author2=Becker P. |title=Floristic composition and stand structure of mixed dipterocarp and heath forests in Brunei Darussalam|journal=Journal of Tropical Forest Science|year=1996|volume=8|issue=4|pages=542–569}}</ref><ref name=small>{{cite journal|author1=Small A. |author2=Martin T.G. |author3=Kitching R.L. |author4=Wong K.H. |title=Contribution of tree species to the biodiversity of a 1 ha Old World rainforest in Brunei, Borneo|journal=Biodiversity and Conservation |year=2004|volume=13|pages=2067–2088|doi=10.1023/B:BIOC.0000040001.72686.e8 }}</ref><ref name=hedl>{{cite journal|author1=Hedl R. |author2=Svatek M. |author3=Dancak M. |author4=Rodzay A.W. |author5=M. Salleh A.B. |author6=Kamariah A.S. |title=A new technique for inventory of permanent plots in tropical forests: a case study from lowland dipterocarp forest in Kuala Belalong, Brunei Darussalam|journal=Blumea|year=2009|volume=54|pages=124–130|doi=10.3767/000651909X475482}}</ref>   The institute also manages a series of nine 0.25 ha plots  at three altitudinal ranges at KBFSC.<ref name=Pendry>{{cite journal|author1=Pendry C.A. |author2=Proctor J. |title=Altitudinal zonation of rain forest on Bukit Belalong, Brunei: Soils, forest structure and floristics|journal=Journal of Tropical Ecology|year=1997|volume=2|issue=13|pages=221–241|doi=10.1017/S0266467400010427}}</ref>  In 2007, the  University, in collaboration with the [[Center for Tropical Forest Science]] of [[Harvard University]], commenced the establishment of the 25 ha UBD-CTFS Plot at KBFSC.<ref name=ctfskb>{{cite web|title=Kuala Belalong|url=http://www.ctfs.si.edu/site/Kuala+Belalong|publisher=Center for Tropical Forest Science|accessdate=19 December 2013}}</ref>

== Education and outreach ==

KBFSC is the core facility for educational activities organised by IBER. Since its inception, KBFSC has served as a venue for various education programmes, field courses and field based workshops participated by undergraduate and high school students. Themes encompass tropical rainforest biodiversity and ecology, emergent issues of biodiversity conservation, sustainability and climate change.<ref name=iberedu>{{cite web|title=Education|url=http://www.ubd.edu.bn/faculties-and-institutes/iber/education/|publisher=Universiti Brunei Darussalam|accessdate=19 December 2013}}</ref>
‘Friends of Belalong’ is an outreach programme embarked by KBFSC and is currently adopted by IBER. It is a volunteer programme aimed at promoting biodiversity and environmental awareness and instilling the values of conservation among the public. The programme welcomes local and international volunteers.<ref name=ibergetinvolved>{{cite web|title=Get Involved|url=http://www.ubd.edu.bn/faculties-and-institutes/iber/outreach-programme/|publisher=Universiti Brunei Darussalam|accessdate=28 January 2014}}</ref>

== References ==
{{reflist}}

== External links ==
* [http://www.ubd.edu.bn Universiti Brunei Darussalam official website]
* [http://www.ubd.edu.bn/faculties-and-institutes/iber Institute for Biodiversity and Environmental Research official webpage] 
* [http://www.ubd.edu.bn/faculties-and-institutes/iber/kuala-belalong-field-studies-centre Kuala Belalong Field Studies Centre official webpage]

[[Category:1985 establishments]]
[[Category:Brunei]]
[[Category:Environmental research]]
[[Category:Biodiversity hotspots]]