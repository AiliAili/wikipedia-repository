{{Infobox journal
| cover = The Journal of the Learning Sciences.gif
| discipline = [[Education]]
| editor = Iris Tabak, Josh Radinsky
| abbreviation = J. Learn. Sci.
| publisher = [[Routledge]]
| country = 
| frequency = Quarterly
| history = 1991-present
| openaccess =
| impact = 2.312
| impact-year = 2014
| website = http://www.informaworld.com/smpp/title~db=all~content=g924504738~tab=summary
| link1 =http://www.informaworld.com/smpp/title~db=all~content=g924504738~tab=summary
| link1-name = Online access
| link2 = http://www.informaworld.com/smpp/title~db=all~content=t775653672~tab=issueslist
| link2-name = Online archive
| ISSN = 1050-8406
| eISSN = 1532-7809
| LCCN = 91641725
| CODEN = JLSBE3
| OCLC = 45007206
}}
'''''The Journal of the Learning Sciences''''' (JLS) is a [[Peer review|peer-reviewed]] [[academic journal]] that provides a multidisciplinary forum for research on education and learning as theoretical and design sciences. It publishes research that elucidates processes of learning, and the ways in which technologies, instructional practices, and learning environments can be designed to support learning in different contexts.

JLS is one of the two official publications of the [[International Society of the Learning Sciences]], and is published by [[Taylor & Francis]]. The current Editors in Chief are Iris Tabak ([[Ben-Gurion University of the Negev]]) and Josh Radinsky ([[University of Illinois at Chicago]]). Prior Editors in Chief include Cindy Hmelo-Silver, Yasmin Kafai, and founding Editor Emeritus Janet Kolodner.

JLS articles draw on theoretical frameworks from such diverse fields as cognitive science, sociocultural theory, educational psychology, computer science, and anthropology. Published articles are not limited to any particular research method, but are based on rigorous analyses that present new insights into how people learn and/or how learning can be supported and enhanced. They reflect the core practices and foci that have defined the learning sciences as a field: privileging ''design'' in methodology and pedagogy; emphasizing interdisciplinarity and methodological innovation; grounding research in real-world contexts; answering questions about learning process and mechanism, alongside outcomes; pursuing technological and pedagogical innovation; and maintaining a strong connection between research and practice.

JLS publishes studies of learning in a broad range of contexts, including schools, higher education, community settings, museums, workplaces, play spaces, and family life, as well as online and virtual worlds. Domains of learning include subject areas such as literacy, history, science or mathematics, as well as other domains, such as teaching expertise, medical diagnosis, or craft knowledge. Research that problematizes disciplinary boundaries is also welcome. Work that foregrounds the design of innovative technologies for learning is a priority for JLS. In all cases, however, the fundamental focus is on understanding the processes, tools, and contexts, as well as outcomes, of learning in its myriad forms.
== Impact ==
According to the ''[[Journal Citation Reports]]'', ''The Journal of the Learning Sciences'' had a 2014 impact factor of 2.312, ranking it 9th out of 53 in the category "Social Science, Psychology, Educational" and 12th out of 219 in "Social Science, Education & Educational Research."

== External links ==
* {{Official website|1=http://www.informaworld.com/smpp/title~db=all~content=g924504738~tab=summary}}
* [http://www.isls.org International Society of the Learning Sciences]

{{DEFAULTSORT:Journal of the Learning Sciences}}
[[Category:Education journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]


{{edu-journal-stub}}