{{good article}}
{{Infobox Simpsons episode
|episode_name=Bart After Dark
|image=
|image_caption=
|episode_no=158
|prod_code=4F06
|airdate=November 24, 1996<ref name="officialsite">{{cite web |url=http://www.thesimpsons.com/#/recaps/season-8_episode-5 |title=Bart After Dark |accessdate=2011-09-21 |publisher=The Simpsons.com}}</ref>
|show runner=[[Bill Oakley]]<br/>[[Josh Weinstein]]
|writer=[[Richard Appel]]
|director=[[Dominic Polcino]]
|couch_gag=A parody of the ''[[Sgt. Pepper's Lonely Hearts Club Band|Sgt. Pepper's]]'' album cover.<ref name="bbc">{{cite web |url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season8/page5.shtml |title=Bart After Dark |accessdate=2007-04-10 |publisher=BBC.co.uk}}</ref>
|commentary=[[Matt Groening]]<br/>[[Josh Weinstein]]<br/>[[Richard Appel]]<br/>[[Dominic Polcino]]<br/>[[David Silverman (animator)|David Silverman]]<br/>[[Ken Keeler]]
|season=8
}}

"'''Bart After Dark'''" is the fifth episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 8)|eighth season]]. It first aired on the [[Fox Broadcasting Company|Fox network]] in the United States on November 24, 1996.<ref name="officialsite"/> After accidentally breaking a stone gargoyle at a local house, Bart is forced to work there as punishment. He assumes it will be boring work, but is pleasantly surprised when he learns that it is actually a [[American burlesque|burlesque]] house. Marge is horrified to find out about the burlesque house, and resolves to have it shut down. The episode was directed by [[Dominic Polcino]] and was written by [[Richard Appel]].<ref name="bbc"/> It won an [[Primetime Emmy Award|Emmy Award]] for "Outstanding Music and Lyrics" for the song "We Put the Spring in Springfield".<ref name="Keeler"/>

==Plot==
An [[The Itchy & Scratchy Show|Itchy & Scratchy cartoon]] is interrupted with a news report that an [[oil tanker]] has crashed and spilled millions of gallons of oil on "Baby [[Seal Beach]]". [[Lisa Simpson|Lisa]] sees the report and asks [[Marge Simpson|Marge]] if she can go and help save the wildlife. After some begging, Marge agrees and the two go to help out, leaving [[Bart Simpson|Bart]] and [[Homer Simpson|Homer]] home alone. The house quickly becomes a mess and Bart goes out to play with his friends. [[Milhouse Van Houten|Milhouse]]'s toy airplane crashes into a house after [[Nelson Muntz|Nelson]] tampers with the remote control and Bart, despite warnings that the house is inhabited by a witch, goes to retrieve it. While doing so, he accidentally knocks down a stone [[gargoyle]] and Belle, the owner of the house, goes to Homer and says she will not press charges but demands he be punished. Homer originally dismisses this, but Belle threatens to come back and speak with Marge, leading Homer to force Bart to do chores for Belle. Fearing the worst, Bart soon discovers that the house is actually a [[American burlesque|burlesque house]] called the ''Maison Derrière'' (the "back house" as Belle translates it to Bart) and quickly takes a new enthusiasm to his job.

Meanwhile, Marge and Lisa arrive at the beach, but discover that the ability to help the animals is reserved for celebrities, who are already doing the job. The two are put to work cleaning rocks, and soon abandon the job and drive home.

While picking up Bart, Homer learns about the true nature of the burlesque house, but does nothing about Bart working there. [[Principal Skinner]] visits the house and sees Bart as the door greeter. He reports it to the [[Timothy Lovejoy|Lovejoys]] and the [[Ned Flanders|Flanders]] who confront Homer about the matter. Homer declares that he has no problem with Bart working there just as Marge returns home unexpectedly.

Marge asks Belle to close down the burlesque house, but Belle refuses saying that the house is a part of [[Springfield (The Simpsons)|Springfield]]. At a town meeting, Marge brings up the matter of the house and shows pictures of several prominent citizens leaving. They form a mob so they can go destroy the burlesque house. The mob arrives at the house and immediately starts destroying things. Bart and Homer arrive and Homer decides to try to convince the mob to stop. He does so by singing a musical number accompanied by Belle and some of her dancers. The town soon joins in and are convinced to let the house stay. However, Marge arrives with a [[bulldozer]], having missed the song. The town announces their intentions to let the house stay, but Marge tries to sing her own song about her views. During the opening lines, she accidentally puts the bulldozer into drive and destroys a wing of the burlesque house.  Marge now finds that the tables have turned, and loses any support she once had. She apologizes profusely and, to pay for the damage, she starts a [[Ventriloquism|ventriloquist act]] at the house, but when Homer yells "Take it off!", Bart (now a [[Bouncer (doorman)|bouncer]]) kicks him out of the house.<ref name=officialsite/><ref name="book">{{cite book |last=Groening |first=Matt |authorlink=Matt Groening |editor1-first=Ray |editor1-last=Richmond |editor1-link=Ray Richmond |editor2-first=Antonia |editor2-last=Coffman |title=[[The Simpsons episode guides#The Simpsons: A Complete Guide to Our Favorite Family|The Simpsons: A Complete Guide to Our Favorite Family]]  |edition=1st |year=1997 |location=New York |publisher=[[HarperPerennial]] |lccn=98141857 |ol=433519M |oclc=37796735 |isbn=978-0-06-095252-5 |page=236 |ref={{harvid|Richmond & Coffman|1997}}}}.</ref>

==Production==
The episode was written by [[Richard Appel]] and directed by [[Dominic Polcino]].<ref name="bbc"/> Appel was looking for new locales to put Bart and thought it would be funny to have him work at a burlesque house. The problem was to find a way to put such a house in Springfield, which was solved with the bit with the toy airplane.<ref name="Appel">{{cite video |people=Appel, Richard |date=2006 |title=The Simpsons season 8 DVD commentary for the episode "Bart After Dark" |medium=DVD |publisher=20th Century Fox}}</ref> There were a dozen different possible names for the Burlesque house, some of which were raunchy.<ref name="Appel"/>

[[Josh Weinstein]] has said that there are so many sleazy characters in ''The Simpsons'' that it was easy to find people for the scenes in the burlesque house.<ref name="Weinstein">{{cite video |people=Weinstein, Josh |date=2006 |title=The Simpsons season 8 DVD commentary for the episode "Bart After Dark" |medium=DVD |publisher=20th Century Fox}}</ref> A character modeled after [[John Swartzwelder]] can also be seen.<ref name="Weinstein"/>

Belle was not modeled after anyone in particular<ref name="Polcino">{{cite video |people=Polcino, Dominic |date=2006 |title=The Simpsons season 8 DVD commentary for the episode "Bart After Dark" |medium=DVD |publisher=20th Century Fox}}</ref> and she was redesigned several times.<ref name="Groening">{{cite video |people=Groening, Matt |date=2006 |title=The Simpsons season 8 DVD commentary for the episode "Bart After Dark" |medium=DVD |publisher=20th Century Fox}}</ref> Belle was voiced by [[Tress MacNeille]] but there had been previous efforts to cast a guest voice for the role.<ref name="Appel"/>

==Cultural references==
[[Image:Simpsons Pepper.png|right|200px|thumb|The couch gag for the episode, which is a reference to ''[[Sgt. Pepper's Lonely Hearts Club Band]]''.<ref name=bbc/>]]

A lot of the episode's plot is based on the film ''[[The Best Little Whorehouse in Texas (film)|The Best Little Whorehouse in Texas]]''.<ref name="Weinstein"/> The oil spill is a reference to the [[Exxon Valdez oil spill]]. [[List of recurring The Simpsons characters#Sea Captain|The Sea Captain]] was shown to be drunk at the helm, which is a reference to [[Joseph Hazelwood]], who was the captain of the Exxon Valdez and was accused of being drunk.<ref name="Weinstein"/> [[Reverend Lovejoy]] says "This house is a very, very, very fine house", a reference to the [[Crosby, Stills & Nash (and Young)|Crosby, Stills, Nash, & Young]] song "[[Our House (CSNY song)|Our House]]".<ref name=bbc/> The vehicle driven by the environmentalist on the beach is called the "Begley 2000" after environmentalist [[Ed Begley Jr.]] <ref>https://www.reddit.com/r/TheSimpsons/comments/2ruwu2/heres_another_joke_i_missed_hundreds_of_times_the/</ref> The song "We Put the Spring in Springfield" shares many similarities with "Main Street, USA" - a song and dance routine performed by [[Lucille Ball]] in an episode of ''[[The Lucy Show]]'' in a successful attempt to stop a developer demolishing a small town.

==Reception==
In its original broadcast, "Bart After Dark" finished 57th in ratings for the week of November 18–24, 1996, with a [[Nielsen ratings|Nielsen rating]] of 8.5, equivalent to approximately 8.2 million viewing households. It was the fourth-highest-rated show on the Fox network that week, following ''[[The X-Files]]'', ''[[Melrose Place]]'' and ''[[Beverly Hills, 90210]]''.<ref>{{cite news |title=NBC sweeps its way to a hat trick |work=Sun-Sentinel |author=Associated Press |page=4D |date=November 29, 1996}}</ref>

[[Ken Keeler]] and [[Alf Clausen]] won a [[Primetime Emmy Award]] for "Outstanding Music and Lyrics" for "We Put the Spring in Springfield".<ref name="Keeler">{{cite video |people=Keeler, Ken |date=2006 |title=The Simpsons season 8 DVD commentary for the episode "Bart After Dark" |medium=DVD |publisher=20th Century Fox}}</ref> The song was also a part of the album ''[[Go Simpsonic with The Simpsons]]''.<ref>{{cite web |url={{Allmusic |class=album |id=r431501 |pure_url=yes}} |title=Go Simpsonic with the Simpsons |accessdate=2007-02-10 |author=Erlewine, Stephen Thomas |publisher=allmusic.com}}</ref>

==References==
{{reflist|2}}

==External links==
{{wikiquote|The Simpsons/Season 8#Bart After Dark|Bart After Dark}}
{{portal|The Simpsons}}
*[http://www.thesimpsons.com/#/recaps/season-8_episode-5 "Bart After Dark"] at The Simpsons.com
*{{imdb episode|0763023}}
*{{snpp capsule|4F06}}
*{{tv.com episode|the-simpsons/bart-after-dark-1443}}

{{The Simpsons episodes|8}}

[[Category:The Simpsons (season 8) episodes]]
[[Category:1996 American television episodes]]