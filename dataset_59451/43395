{|{{Infobox Aircraft Begin
  |name = VZ-8 Airgeep
  |image = Flyingjeep.jpg
  |caption = 
}}{{Infobox Aircraft Type
  |type = Experimental rotorcraft
  |manufacturer = [[Piasecki Aircraft]]
  |designer = 
  |first flight = 15 February 1962
  |introduced = 
  |retired = 
  |status = 
  |primary user = [[U.S. Army]]
  |more users = 
  |produced = 
  |number built = 
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}

The '''Piasecki VZ-8 Airgeep''' (company designation PA-59) was a prototype [[vertical takeoff and landing]] (VTOL) aircraft developed by [[Piasecki Aircraft]]. The Airgeep was developed to fulfill a US Army Transportation Research Command contract for a flying jeep in 1957.<ref>[http://www.vectorsite.net/avplatfm.html The Flying Platforms & Jeeps]</ref> The flying jeep was envisioned to be smaller and easier to fly than a helicopter.

==Design and development==

To meet the US Army's requirement, Piasecki's design featured two tandem, three-blade ducted rotors, with the crew of two seated between the two rotors.  Power was by two {{convert|425|hp|kW|abbr=on|1}} [[Lycoming O-360|Lycoming O-360-A2A]] piston engines, driving the rotors by a central gearbox. The first of two aircraft ordered by the Army, initially designated the '''Model 59K Skycar''' (and later renamed '''Airgeep''') by Piasecki and designated '''VZ-8P''' by the Army, flew on 22 September 1958.<ref name="Harding p11">Harding 1998, p.11.</ref><ref name="pia V59K">"[http://www.piasecki.com/geeps_pa59k.php PA-59K: ''History - Flying Jeeps'']". ''Piasecki Aircraft Corporation''. 2009. Retrieved 26 March 2010.</ref>

It was re-engined with a single {{convert|425|hp|kW|abbr=on|0}} [[Turbomeca Artouste IIB]] [[turboshaft]] replacing the two piston engines, flying in this form in June 1959.<ref name="Harding p11"/>  After being loaned to the US Navy for evaluation as the '''Model 59N''' where it was fitted with floats, it was returned to the Army and its engine replaced by a lighter and more powerful {{convert|550|hp|kW|abbr=on|1}} [[Garrett AiResearch TPE331]]-6 engine.<ref name="Harding p12"/>

The second prototype was completed to a modified design, designated '''Model 59H AirGeep II''' by Piasecki and '''VZ-8P (B)''' It was powered by two Artouste engines, with [[ejection seat]]s for the pilot and co-pilot/gunner and a further three seats for passengers.  It was also fitted with a powered tricycle undercarriage to increase mobility on land.

The  AirGeep IIs first flight occurred on 15 February 1962 piloted by "Tommy" Atkins.<ref>"[http://www.piasecki.com/geeps_pa59h.php  PA-59H: ''History - Flying Jeeps'']". ''Piasecki Aircraft Corporation''. 2009. Retrieved 26 March 2010.</ref>

While the Airgeep would normally operate close to the ground, it was capable of flying to several thousand feet, proving to be stable in flight. Flying low allowed it to evade detection by radar.<ref>http://www.helicoptermuseum.org/AircraftDetails.asp?HelicopterID=27</ref> Despite these qualities, and its superiority over the other two types evaluated by the US Army to meet the same requirement (the [[Chrysler VZ-6]] and the [[Curtiss-Wright VZ-7]]), the Army decided that the "Flying Jeep concept [was] unsuitable for the modern battlefield", and concentrated on the development of conventional helicopters.<ref name="Harding p12"/>

==Variants==
;Model 59K Skycar: Company designation for the first aircraft powered by two {{convert|180|hp|kW|abbr=on|1}}) [[Lycoming O-360-A2A]] piston engines, given the military designation VZ-8P Airgeep. Later, the piston engines were replaced by a single {{convert|425|hp|kW|abbr=on|1}}) [[Turbomeca Artouste IIB]] turboshaft engine.<ref name=JAWA62-63>{{cite book|last=Taylor|first=John W.R. FRHistS. ARAeS|title=Jane's All the World's Aircraft 1962-63|year=1962|publisher=Sampson, Low, Marston & Co Ltd|location=London}}</ref>
;Model 59N SeaGeep I: The first aircraft, (after the piston engines were replaced by a single Artouste), whilst on loan to the [[US Navy]], fitted with floats.<ref name=JAWA62-63/>
;PA-59H AirGeep II: The second aircraft, military designation VZ-8P (B), completed with two {{convert|400|hp|kW|abbr=on|1}}) [[Turbomeca Artouste IIC]] turboshaft engines and seats for up to five, including the crew.<ref name=JAWA62-63/>
;VZ-8P Airgeep I: The military designation of the first aircraft as delivered
;VZ-8P-1 Airgeep I:The first aircraft after the piston engines were replaced by a single {{convert|425|hp|kW|abbr=on|1}} [[Turbomeca Artouste IIB]].
;VZ-8P-2 Airgeep I: The first aircraft after the Artouste engine was replaced by a lighter and more powerful {{convert|550|hp|kW|abbr=on|1}}) [[Garrett AiResearch TPE331]]-6 engine.
;VZ-8P (B) Airgeep II:The military designation of the second aircraft.<ref name=JAWA62-63/>

==Specifications (VZ-8P (B))==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1962-63<ref name="JAWA62-63"/> Flying Jeeps: The US Army's Search for the Ultimate 'Vehicle' <ref name="Harding p12">Harding 1998, p.12.</ref>
|prime units?=kts
<!--
        General characteristics
-->
|genhide=

|crew=two (pilot and co-pilot/gunner)
|capacity=up to three passengers
|length m=7.45
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=2.82
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=1.78
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=2,611
|empty weight note=
|gross weight kg=
|gross weight lb=3,670
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=4,800
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Turbomeca Artouste IIC]]
|eng1 type=turbo-shaft engines
|eng1 kw=<!-- prop engines -->
|eng1 hp=550
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=2
|rot dia m=2.5<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=136
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=112
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=56
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=914
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns= Provision for one recoilless rifle - not fitted
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Avro Canada VZ-9 Avrocar]]
*[[Curtiss-Wright VZ-7]]
*[[Chrysler VZ-6]]
|lists=<!-- related lists -->
}}

==References==
;Notes
{{Reflist}}
;Bibliography
*Harding, Stephen. "Flying Jeeps: The US Army's Search for the Ultimate 'Vehicle'". ''[[Air Enthusiast]]'', No. 73, January/February 1998. Stamford, Lincs, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;10–12.
*{{cite book|last=Taylor|first=John W.R. FRHistS. ARAeS|title=Jane's All the World's Aircraft 1962-63|year=1962|publisher=Sampson, Low, Marston & Co Ltd|location=London}}
*[https://archive.org/stream/Aviation_Week_1962-05-07#page/n41/mode/1up Piasecki Tests Twin-Turbine and Seagoing VTOLs]. // ''Aviation Week & Space Technology'', May 7, 1962, v. 76, no. 19, p. 83.

==External links==
{{commons category|Piasecki VZ-8 Airgeep}}
* [http://www.piasecki.com Piasecki Aircraft Corporation]

{{Piasecki/Vertol aircraft}}
{{Flying cars}}
{{US Army VTOL}}

[[Category:Piasecki aircraft|VZ-8]]
[[Category:United States military utility aircraft 1950–1959]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Turboshaft-powered aircraft]]
[[Category:Twin-engined aircraft]]
[[Category:VTOL aircraft]]
[[Category:Ducted fan-powered aircraft]]