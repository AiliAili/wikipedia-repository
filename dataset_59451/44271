{{Infobox airport 
| name = RAF Catfoss
| nativename = [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename-a =   
| nativename-r =   
| image =
| image-width = 
| caption =    
| IATA =    
| ICAO =  
| type = Military   
| owner = [[Ministry of Defence (United Kingdom)|Ministry of Defence]]
| operator = [[Royal Air Force]]
| city-served =    
| location = [[Brandesburton]]
| built = 1930
| used = 1932–1945<br />1958–1963
| elevation-f = {{Convert|11|m|disp=output number only|0}}
| elevation-m = 11
| coordinates = {{coord|53|55|15|N|000|16|30|W|type:airport_region:GB|display=inline,title}}
| pushpin_map            = East Riding of Yorkshire
| pushpin_label          = RAF Catfoss
| pushpin_map_caption    = Location in East Riding of Yorkshire
| website =    
| r1-number = 00/00
| r1-length-f = 0 
| r1-length-m = 0
| r1-surface = [[Concrete]] and wood chips
| r2-number = 00/00
| r2-length-f = 0 
| r2-length-m = 0
| r2-surface = Concrete and wood chips
| r3-number = 00/00
| r3-length-f = 0 
| r3-length-m = 0
| r3-surface = Concrete and wood chips
| stat-year =    
| stat1-header =    
| stat1-data =    
| stat2-header = 
| stat2-data =    
| footnotes =    
}}
'''RAF Catfoss''' was a [[Royal Air Force]] station during the [[Second World War]]. It was located {{Convert|4.0|mi}} west of [[Hornsea]], Yorkshire, [[England]] and {{Convert|8.0|mi}} north east of [[Leconfield]], Yorkshire, with the nearest village being [[Brandesburton]].

The airfield was opened in 1932 for an Armament Training Camp. A small number of fighters were posted there at the start of the Second World War, before the airfield was rebuilt as a bomber station. It closed in November 1945. The site was re-opened in 1959 as the site for the [[PGM-17 Thor]] ballistic missile. It closed again in 1963.

==History==
Catfoss was originally used as a grass airfield in the 1930s. On 1 January 1932, No. 1 Armament Training Camp was formed there, with a wide variety of aircraft that used the nearby gunnery ranges to teach air-to-air and air-to-ground gunnery, and bombing.<ref name="Lake1999p33">{{Harvnb|Lake|1999|p=33.}}</ref><ref name="CT">{{cite web|url=http://www.controltowers.co.uk/C/Catfoss.htm |title=RAF Catfoss |publisher=Control Towers|accessdate=12 June 2012}}</ref> In 1935 a number of [[Handley Page Heyford]] heavy bombers were based at Catfoss with [[No. 97 Squadron RAF|97 Squadron]]. The armament camp continued to be busy into the late 1930s, being renamed '''No. 1 Armament Training Station'''. With the approach of war, it was decided that the east coast was too vulnerable to attack for training and the unit moved out during 1939.<ref name="Jefford1988p53">{{Harvnb|Jefford|1988|p=53.}}</ref>

A detachment of [[Supermarine Spitfire]]s of [[No. 616 Squadron RAF]] from [[RAF Leconfield]]<ref name="Jefford1988p100">{{Harvnb|Jefford|1988|p=100.}}</ref> arrived for air defence in 1940. The airfield was expanded and re-opened in August that year.<ref name="CT"/> From July to October, [[No. 16 Operational Training Unit RAF]] (OTU) used the station and nearby ranges for night-bomber training. The airfield was then transferred to [[Coastal Command]] and [[No. 2 (Coastal) Operational Training Unit RAF]] was formed on 1 October 1940 to train crews on the command's twin-engined fighter and strike aircraft.<ref name="CT"/><ref name="Lake1999p144">{{Harvnb|Lake|1999|p=144.}}</ref> The airfield became very busy with different aircraft types and training courses and three concrete runways were built at the end of 1942.<ref>{{cite PastScape|mnumber=912166|mname= RAF Catfoss|accessdate=18 June 2012 }}</ref>

The unit also trained [[Bristol Beaufighter]] crews for deployment to Far East and Middle East squadrons. Having completed this work, the OTU was disbanded in October 1944.<ref name="CT"/>

The Central Gunnery School transferred in from [[RAF Sutton Bridge]] in March 1944, continuing its role of training experienced aircrew to become Gunnery Instructors for both [[RAF Fighter Command|Fighter]] and [[RAF Bomber Command|Bomber]] Commands. ( For further detail of training undertaken see [[RAF Sutton Bridge]]). The principal aircraft used were Spitfires and [[Vickers Wellington]]s, together with support aircraft for roles such as target towing.<ref name="CT"/> With the end of the Second World War the number of students reduced and the school moved to [[RAF Leconfield]] in October 1945. The station was closed down on 12 November 1945.<ref name="CT"/>

A proposal was announced in 1947 to turn Catfoss into a civil airport, but this never eventuated. In 1958 the need to base the new [[PGM-17 Thor|Thor]] intermediate-range ballistic missile led to a massive concrete launch site being built in the centre of the airfield, under the control of [[No. 226 Squadron RAF]].<ref name="CT"/> The site began operating on 1 August 1959. The missiles were withdrawn from service on 9 March 1963 and the airfield was again closed.<ref name="Jefford1988p73">{{Harvnb|Jefford|1988|p=73.}}</ref>

==RAF units and aircraft==
{|class="wikitable"
|-
!Unit
!Dates
!Aircraft
!Variant
!Notes
|-
|[[No. 97 Squadron RAF]]
|1935
|[[Handley Page Heyford]]
|IA
|Twin-engined heavy biplane bomber only posted here for a few weeks in September.<ref name="Jefford1988p73" />
|-
|[[No. 226 Squadron RAF]]
|1959–1963
|[[PGM-17 Thor|Douglas Thor]]
|
|Intermediate-range ballistic missile.<ref name="Jefford1988p73" />
|-
|[[No. 616 Squadron RAF]]
|1939–1940
|[[Supermarine Spitfire]]
|I
|Air defence detachment from [[RAF Leconfield]]<ref name="Jefford1988p73" />
|-
|[[No. 2 (Coastal) Operational Training Unit RAF]]
|1940–1944
|[[Bristol Blenheim]]<br>[[Avro Anson]]<br>[[Bristol Beaufighter]]
|
|Coastal Command training unit<ref name="Sturtivant2007p124">{{Harvnb|Sturtivant|2007|p=124.}}</ref>
|}

* Central Gunnery School (1944–1945)<ref name="Sturtivant2007p124" />
* No. 1 Armament Training Camp (1932–1938)<ref name="Sturtivant2007p124" />
* No. 1 Armament Training Station (1938–1939)<ref name="Sturtivant2007p124" />

==Current use==
[[File:Catfoss Lane Industrial Estate - geograph.org.uk - 34947.jpg|thumb|right|Catfoss Industrial Estate]]
The site is now used by various businesses and is called the Catfoss Industrial Estate.<ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/catfoss |title=Catfoss |publisher=[[Airfields of Britain Conservation Trust]]|accessdate=12 June 2012}}</ref>

==References==

===Citations===
{{reflist|2}}

===Bibliography===
*{{wikicite|ref={{harvid|Jefford|1988}}|reference=Jefford, C.G, [[Order of the British Empire|MBE]],BA,RAF (Retd). ''RAF Squadrons, a Comprehensive Record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 1988. ISBN 1-84037-141-2.}}
*{{wikicite|ref={{harvid|Lake|1999}}|reference=Lake, A ''Flying Units of the RAF''. Shrewbury,  Airlife Publishing Ltd., 1999. ISBN 1-84037-086-6.}}
*{{wikicite|ref={{harvid|Sturtivant|2007}}|reference=Sturtivant, R. ''RAF Flying Training and Support Units since 1912''. Air Britain, 2007. ISBN 0-85130-365-X.}}

==External links==
{{Commons category|RAF Catfoss}}
*[http://www.airfieldinformationexchange.org/community/showthread.php?170-RAF-Catfoss-Yorkshire Airfield Information Exchange]
{{Royal Air Force}}

{{DEFAULTSORT:Catfoss}}
[[Category:Royal Air Force stations in Yorkshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]