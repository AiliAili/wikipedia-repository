{{Infobox journal
| title = Psychiatric Rehabilitation Journal
| cover = [[File:Psychiatric_Rehabilitation_Journal_cover_image.jpg]]
| editor = Judith A. Cook and Kim T. Mueser
| discipline = [[Psychiatric rehabilitation]]
| abbreviation = Psychiatr. Rehabil. J. 
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Quarterly
| history = 2002-present
| openaccess = 
| license =
| impact = 1.169
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/prj/index.aspx
| link1 = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=prj
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN =
| ISSN = 1095-158X
| eISSN = 1559-3126
}}
'''''Psychiatric Rehabilitation Journal''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by the [[American Psychological Association]]. It was established in 2002 and covers research on the topics of "rehabilitation, psychosocial treatment, and recovery of people with serious mental illnesses".<ref>{{cite web |date=14 November 2012| url=http://www.apa.org/pubs/journals/prj/index.aspx |title=Psychiatric Rehabilitation Journal|publisher=[[American Psychological Association]] |accessdate=2012-11-14}}</ref> The current [[editors-in-chief]] are Judith A. Cook ([[University of Illinois]]) and Kim T. Mueser ([[Geisel School of Medicine]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]]/[[PubMed]] and the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.169, ranking it 88th out of 133 journals in the category "Psychiatry" and 36th out of 70 journals in the category "Rehabilitation".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact:Psychiatry, Rehabilitation |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2015-08-26 |series=Web of Science |postscript=.}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official|http://www.apa.org/pubs/journals/prj/index.aspx}}

[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Rehabilitation medicine journals]]
[[Category:Psychiatry journals]]
[[Category:Publications established in 2002]]
[[Category:Psychotherapy journals]]