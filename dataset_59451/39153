{{Redirect|CA 16|the congressional district|California's 16th congressional district}}
{{Infobox road
|state=CA
|type=SR
|route=16
|section=316
|maint=[[Caltrans]]
|length_mi=111.17
|length_ref=<ref name="bridge log">[http://www.dot.ca.gov/hq/structur/strmaint/brlog2.htm July 2007 California Log of Bridges on State Highways]</ref>
|length_round=2
|length_notes=(includes 28.82 mi (46.38 km) on [[Interstate 5 in California|I-5]] and [[U.S. Route 50 in California|US 50]])
|established=1934<ref name="cah 16">[http://cahighways.org/009-016.html#016 California Highways: State Route 16]</ref>
|direction_a=West
|terminus_a1={{jct|state=CA|SR|20}} near [[Rumsey, California|Rumsey]]
|junction1={{jct|state=CA|I|505}} in [[Cottonwood, California|Cottonwood]]
|terminus_b1={{jct|state=CA|I|5}} in [[Woodland, California|Woodland]]
|terminus_a2={{jct|state=CA|US|50}} in [[Sacramento, California|Sacramento]]
|direction_b=East
|terminus_b2={{jct|state=CA|SR|49}} near [[Drytown, California|Drytown]]
|previous_type=SR
|previous_route=15
|next_type=SR
|next_route=17
}}
'''State Route 16''' ('''SR 16''') is a [[state highway]] in the [[Northern California|northern]] region of the [[U.S. state]] of [[California]] that runs from [[California State Route 20|Route 20]] in [[Colusa County, California|Colusa County]] to [[California State Route 49|Route 49]] just outside [[Plymouth, California|Plymouth]] in [[Amador County, California|Amador County]]. It is discontinuous through [[Sacramento]], specifically between [[Interstate 5 in California|Interstate 5]] in [[Woodland, California|Woodland]] and [[U.S. Route 50 in California|U.S. Route 50]] east of Sacramento.

== Route description ==
SR 16 is part of the [[California Freeway and Expressway System]]<ref name="cafes">[http://www.leginfo.ca.gov/cgi-bin/displaycode?section=shc&group=00001-01000&file=250-257 CA Codes (shc:250-257)]</ref> and is eligible for the [[State Scenic Highway System (California)|State Scenic Highway System]].<ref name="scenic">[http://www.leginfo.ca.gov/cgi-bin/displaycode?section=shc&group=00001-01000&file=260-284 CA Codes (shc:260-284)]</ref> However, it is not designated as a scenic highway by [[Caltrans]].<ref name="actualscenic">[[California Department of Transportation]], [http://www.dot.ca.gov/hq/LandArch/scenic/schwy.htm Officially Designated Scenic Highways], accessed 2010-03-20</ref> It is known as the Stanley L. Van Vleck Memorial Highway from Dillard Road in Sacramento County to the Amador County line, honoring a former prominent leader in the state's agricultural organizations.<ref>{{cite book | url=http://www.dot.ca.gov/hq/tsip/hseb/products/2006_Named_Freeways.pdf | title=2006 Named Freeways, Highways, Structures and Other Appurtenances in California | publisher=[[Caltrans]] | accessdate=2007-03-28 | page=77|archiveurl = https://web.archive.org/web/20070616022843/http://www.dot.ca.gov/hq/tsip/hseb/products/2006_Named_Freeways.pdf |archivedate = June 16, 2007|deadurl=yes}}</ref>

=== Western section ===
State Route 16 begins in [[Colusa County, California|Colusa County]] near [[Wilbur Springs, California|Wilbur Springs]] at the junction with [[State Route 20 (California)|State Route 20]]. SR 16 goes south alongside Bear Creek, which enters a narrow canyon and joins with [[Cache Creek (Sacramento River)|Cache Creek]] near the [[Yolo County, California|Yolo County]] line. SR 16 continues in the canyon, running close to the river, passing ''Cache Creek Canyon Regional Park'', and emerging from the canyon north of [[Rumsey, California|Rumsey]]. This section is so prone to rock slides that there are permanent gates at each end.

SR 16 continues to parallel Cache Creek, at a greater distance, going south-east through [[Capay Valley]], with Blue Ridge to its west and the Capay Hills (including [[Bald Mountain (California)|Bald Mountain]]) to its east. It goes through  [[Rumsey, California|Rumsey]], [[Guinda, California|Guinda]], [[Brooks, California|Brooks]], [[Cache Creek Casino Resort]], [[Capay, California|Capay]], [[Esparto, California|Esparto]] (intersecting with [[County Route E4 (California)|County Route E4]] to [[Dunnigan, California|Dunnigan]]), and [[Madison, California|Madison]].

East of Madison, and now in the [[Central Valley (California)|Central Valley]], SR 16 interchanges with [[Interstate 505 (California)|Interstate 505]] before heading east toward [[Woodland, California|Woodland]]. In west Woodland it merges with County Road 22 and then turns north, concurrently with [[County Route E7 (California)|County Route E7]] and [[Interstate 5 Business (Woodland, California)|Interstate 5 Business]], until it meets its interchange with [[Interstate 5 (California)|Interstate 5]].

=== Eastern section ===
The eastern segment of SR 16 begins at [[U.S. Route 50 (California)|U.S. Route 50]] east of Sacramento. SR 16 heads east through [[Perkins, California|Perkins]] as Jackson Road. After it passes near [[Bridge House, California|Bridge House]] and [[Rancho Murieta, California|Rancho Murieta]], where it crosses the [[Cosumnes River]], SR 16 enters [[Amador County, California|Amador County]]. SR 16 then ascends into the [[Sierra Nevada (U.S.)|Sierra Nevada]] foothills, leaving the [[Central Valley (California)|Central Valley]]. In Amador County, SR 16 passes near [[Forest Home, California|Forest Home]] before intersecting with [[State Route 124 (California)|State Route 124]] and terminating at [[California State Route 49|State Route 49]].

== History ==
[[Image:CA16 Woodland.JPG|thumb|right|SR 16 heading east through Yolo County, California.]]
The two ends of SR 16 were added to the state highway system by the third bond issue, passed by the state's voters in 1919: '''Route 50''' from [[Lower Lake, California|Lower Lake]] east to [[Rumsey, California|Rumsey]] and '''Route 54''' from the [[Sacramento County, California|Sacramento]]-[[Amador County, California|Amador]] County line east to [[Drytown, California|Drytown]].<ref>{{cite CAstat|year=1919|res=yes|ch=46|p=1520}}: "Rumsey to Lower Lake"; "county line near Michigan Bar via Huot's ranch to Drytown"</ref> Each was connected to [[Sacramento, California|Sacramento]] by existing or planned paved [[county highway]]s.<ref name=blow>Ben Blow, California Highways: A Descriptive Record of Road Development by the State and by Such Counties as Have Paved Highways, 1920 ([https://archive.org/details/californiahighwa00blowrich Archive.org] or [https://books.google.com/books?id=osgNAAAAYAAJ Google Books]), pp. 115-116, 206-207, 284-285</ref> Although the exact alignment of Route 50 was not specified, the state [[Department of Engineering (California)|Department of Engineering]] had already surveyed a 35-mile (56&nbsp;km) route through [[Cache Creek Canyon]] pursuant to a 1915 law, which defined the '''Yolo and Lake Highway''' "following generally, the meanderings of Cache creek" but did not make it a state highway.<ref>[[Department of Engineering (California)|Department of Engineering]], [https://books.google.com/books?id=Kc83AAAAIAAJ Fifth Biennial Report of the Department of Engineering of the State of California, December 1, 1914, to November 30, 1916], 1917, pp. 185-186</ref><ref>{{cite CAstat|year=1915|ch=283|p=478}}</ref> By 1924, the [[California Highway Commission]]'s engineers had realized that building Route 50 through the canyon was impractical, and adopted a substitute plan for two highways connecting Lower Lake and Rumsey with the planned [[Legislative Route 15 (California pre-1964)|Route 15]] ([[Tahoe-Ukiah Highway]], now [[California State Route 20|State Route 20]]) to the north<ref name=1925report>California Highway Advisory Committee and Arthur Hastings Breed, Report of a Study of the State Highway System of California, California State Printing Office, 1925, p. 91</ref> in September 1925.<ref>[[Oakland Tribune]], Highway Commission Lays Plans for Building Lake Connection, September 6, 1925</ref><ref>[[Fresno Bee]], Route Follows Lake Shore, September 27, 1925</ref> The western connection, to Lower Lake, became part of [[Legislative Route 49 (California pre-1964)|Route 49]] (now [[California State Route 53|State Route 53]] there), which continued south from Lower Lake to [[Calistoga, California|Calistoga]].

Each route was extended to Sacramento in 1933 over the aforementioned county highways, taking Route 50 southeast from Rumsey to [[Woodland, California|Woodland]] near Cache Creek and then alongside the [[Sacramento River]] to the [[I Street Bridge]], and Route 54 west from the county line to [[Legislative Route 11 (California pre-1964)|Route 11]] just outside Sacramento.<ref>{{cite CAstat|year=1933|ch=767|p=2035}}: "State Highway Route 50 near Rumsey to State Highway Route 7 near Woodland." "Woodland to Sacramento." "State Highway Route 11 near Perkins to State Highway Route 54 near Michigan Bar."</ref><ref>{{cite CAstat|year=1935|ch=29|p=278-279}}: "Route 50 is from Route 15 to Sacramento via Rumsey and Woodland." "Route 54 is from Route 11 near Perkins to Drytown, passing near Michigan Bar and via Huot's Ranch."</ref> The entirety of both routes, from SR 20 near [[Wilbur Springs, California|Wilbur Springs]] through Sacramento to [[California State Route 49|State Route 49]] just north of [[Drytown, California|Drytown]] (and initially [[overlap (road)|overlapping]] SR 49 to [[Jackson, California|Jackson]]), was included in the initial state sign route system in 1934 as '''Sign Route 16'''.<ref>[[California Highways and Public Works]], [http://www.gbcnet.com/roads/ca_routes_1934.html State Routes will be Numbered and Marked with Distinctive Bear Signs], August 1934</ref> Through downtown Sacramento, SR 16 followed [[U.S. Route 40 (California)|U.S. 40]] ([[Legislative Route 6 (California pre-1964)|Legislative Route 6]]) and [[U.S. Route 50 (California)|U.S. 50]] ([[Legislative Route 11 (California pre-1964)|Legislative Route 11]]), mostly on Capitol Avenue, while Legislative Route 50 continued south on 5th Street (later a [[one-way pair]] of 3rd and 5th Streets) and turned east on Broadway, carrying [[California State Route 24|Sign Route 24]] most of the way to Freeport Boulevard.<ref>[[Division of Highways (California)|Division of Highways]], [http://www.americanroads.us/citymaps/1944CaStateMapSacramento.png Sacramento], 1944</ref><ref>[[Division of Highways (California)|Division of Highways]], [http://cahighways.org/maps/1963sac.jpg Sacramento], 1963</ref> In the [[1964 renumbering (California)|1964 renumbering]], Route 16 became the new legislative designation,<ref name=law-renumbering>{{cite CAstat|year=1963|ch=385|p=1173}}: "Route 16 is from: (a) Route 20 to Route 5 near Woodland via Rumsey and Woodland. (b) Route 5 near Woodland to Sacramento. (c) Route 50 near Perkins to Route 49 near Drytown."</ref> and Sign Route 24 through Sacramento was replaced with [[California State Route 99|State Route 99]] and [[California State Route 160|State Route 160]]. As neither of these used what had been Sign Route 24 along 3rd and 5th Streets and Broadway, part of Route 16's new definition ("Route 5 near Woodland to Sacramento") was used for several years on this alignment until it became part of State Route 99 later that decade. This left the western segment of SR 16 ending at [[Interstate 5 (California)|Interstate 5]] near the east end of the I Street Bridge<ref>[[Division of Highways (California)|Division of Highways]], Annual Traffic Census: 1964, 1966, 1968</ref> until 1984, when the Woodland-Sacramento portion, which had become redundant with the parallel Interstate 5 complete, was deleted from the legislative definition.<ref>{{cite CAstat|year=1984|ch=409|p=1770}}</ref> It was at about this time that SR 16 was rerouted from the intersection with [[County Route E7 (California)|County Route E7]] to continue north on a bypass of Woodland instead of east to Interstate 5.<ref name="cah 16"/><!--this could use a better source, but I can't find one-->

On September 15, 2014, Assembly Bill No. 1957 was passed, authorizing relinquishment of the segment of SR 16 in Eastern Sacramento near US 50.<ref name="law-relinquishment2015">{{cite web | url=http://leginfo.legislature.ca.gov/faces/billNavClient.xhtml?bill_id=201320140AB1957 | title=An act to amend Section 316 of the Streets and Highways Code, relating to state highways | author=California State Assembly | publisher=2014 Session of the Legislature | work=State of California}}</ref>

== Major intersections ==
{{CAinttop|post_ref=<br><ref name=trucklist /><ref name=bridgelog>[[California Department of Transportation]], [http://www.dot.ca.gov/hq/structur/strmaint/brlog2.htm Log of Bridges on State Highways], July 2007</ref><ref>[[California Department of Transportation]], [http://traffic-counts.dot.ca.gov/ All Traffic Volumes on CSHS], 2005 and 2006</ref>
}}
{{CAint
|county=Colusa
|county_note=COL 0.00-7.26
|location=none
|postmile=0.00
|road={{Jct|state=CA|SR|20|city1=Williams|city2=Clearlake|city3=Ukiah}}
|notes=West end of SR 16
}}
{{CAint
|county=Yolo
|cspan=4
|county_note=YOL 0.00-R43.42
|location=Capay
|postmile=none
|road={{Jct|state=CA|CR|E4|county1=Yolo|name1=Road 85|city1=Dunnigan}}
|notes=
}}
{{CAint
|location=none
|postmile=32.23
|road={{Jct|state=CA|I|505|city1=Redding|city2=Vacaville}}
|notes=Interchange
}}
{{CAint
|location=Woodland
|type=concur
|postmile=R40.57
|road={{Jct|state=CA|BL|5|dab1=Woodland|dir1=south|name1=West Main Street|CR|E7|county2=Yolo|name2=Road 98}}
|notes=West end of I-5 Bus. overlap; West Main Street was former SR 16 east
}}
{{CAint
|location=none
|type=concur
|postmile=R43.42
|road={{Jct|state=CA|I|5|city1=Redding|city2=Sacramento|road=Road 18}}
|notes=Interchange; east end of I-5 Bus. overlap
}}
{{jctgap}}
{{CAint
|county=Sacramento
|cspan=4
|county_note=SAC T1.66-R23.96
|location=Sacramento
|lspan=3
|postmile=T1.66
|road={{Jct|state=CA|US|50|name1=El Dorado Freeway|city1=South Lake Tahoe|city2=Sacramento|road=Howe Avenue}}
|notes=Interchange
}}
{{CAint
|postmile=T1.95
|road=[[Folsom Boulevard]] west, Power Inn Road
|notes=Former SR 16 west / [[U.S. Route 50 in California|US 50]] west
}}
{{CAint
|postmile=T2.53
|road=[[Folsom Boulevard]] east, Notre Dame Drive
|notes=Former [[U.S. Route 50 in California|US 50]] east
}}
{{CAint
|location=Rancho Cordova
|postmile=R11.47
|road={{Jct|state=CA|CR|E2|county1=Sacramento|name1=Sunrise Boulevard}}
|notes=
}}
{{CAint
|county=Amador
|cspan=2
|county_note=AMA R0.00-9.37
|location=none
|postmile=9.09
|road={{Jct|state=CA|SR|124|city1=Ione}}
|notes=
}}
{{CAint
|location=none
|postmile=9.37
|road={{Jct|state=CA|SR|49|city1=Plymouth|city2=Placerville|city3=Sutter Creek|city4=Jackson}}
|notes=East end of SR 16
}}
{{Jctbtm|keys=concur}}

==See also==
*{{portal-inline|California Roads}}

== References ==
{{reflist|30em}}

== External links ==
{{Attached KML|display=title,inline}}
{{commonscat}}
* [http://www.aaroads.com/california/ca-016.html AARoads - State Route 16]
* [http://www.dot.ca.gov/hq/roadinfo/sr16 Caltrans: Route 16 highway conditions]
* [http://www.cahighways.org/009-016.html#016 California Highways: SR 16]
{{good article}}

[[Category:State highways in California|016]]
[[Category:Roads in Colusa County, California|State Route 016]]
[[Category:Roads in Yolo County, California|State Route 016]]
[[Category:Roads in Sacramento County, California|State Route 016]]
[[Category:Roads in Amador County, California|State Route 016]]
[[Category:U.S. Route 40]]
[[Category:U.S. Route 50]]