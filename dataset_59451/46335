{{ WAP assignment | course = Wikipedia:USEP/Courses/Introduction to Mass Communications (Chad Tew) | university = University of Southern Indiana | term = 2012 Q3 | project = WikiProject Journalism }} 
{{Infobox person
| name                      = Marco Antonio Ávila
| native_name               = Marco Antonio Ávila García
| native_name_lang          = es
| image                     = <!-- just the name, without the File: or Image: prefix or enclosing [[brackets]] -->
| image_size                = 
| alt                       = 
| caption                   = 
| birth_name                = 
| birth_date                = 15 January 1973 (Age 39)
| birth_place               = 
| disappeared_date          = 17 May 2012<!-- {{Disappeared date and age|YYYY|MM|DD|YYYY|MM|DD}} (disappeared date then birth date) -->
| disappeared_place         = Ciudad Obregón, Sonora, Mexico
| death_date                = 18 May  2012<!-- {{Death date and age|2012|05|18|YYYY|MM|DD}} (death date then birth date) -->
| death_place               = 
| death_cause               = Drug cartel violence, torture and strangulation
| body_discovered           = in Guaymas, Mexico and near Empalme, Sonora, Mexico
| nationality               = Mexican
| occupation                = Print newspaper journalist 
| years_active              = 15
| employer                  = ''Diario Sonora de la Tarde'' and ''El Regional de Sonora''
| home_town                 = Ciudad Obregón, Sonora, Mexico
| spouse                    = Karina Judith
| children                  = Three
| website                   = {{URL|http://www.diariodesonora.com.mx/}} <br />{{URL|http://www.elregionaldesonora.com.mx/}}
}}
'''Marco Antonio Ávila Garcia''',  (15 January 1973  &ndash; 17/18 May 2012), a Mexican crime [[Journalist|reporter]] for ''Diario Sonora de la Tarde'' in [[Ciudad Obregón]] and ''El Regional de Sonora'' newspapers in [[Sonora]] state, [[Mexico]].<ref name=CPJ>{{cite web|author=Marco Antonio Ávila García |url=http://cpj.org/killed/2012/marco-antonio-avila-garcia.php |title=Marco Antonio Ávila García - Journalists Killed - Committee to Protect Journalists |publisher=Cpj.org |date=2012-05-21 |accessdate=2012-10-19}}</ref> He was abducted in Ciudad Obregón and tortured to death by a drug cartel and left on the side of the road near [[Empalme, Sonora|Empalme]], which is also in Sonora.<ref name=Guardian1>{{cite news|author=Associated Press in Mexico City |url=https://www.theguardian.com/world/2012/may/19/kidnapped-reporter-found-dead-mexico |title=Kidnapped reporter found dead in Mexico &#124; World news &#124; guardian.co.uk |publisher=Guardian |date= 2012-05-19|accessdate=2012-10-19 |location=London}}</ref><ref name=ai>{{cite news|first=Jess |last=Cotton |url=http://www.argentinaindependent.com/currentaffairs/newsfromlatinamerica/mexico-kidnapped-reporter-found-dead/ |title=Mexico: Kidnapped Reporter Found Dead |publisher=Argentinaindependent.com |date=2012-05-21 |accessdate=2012-10-19}}</ref> Garcia was the fifth journalist killed in 2012 in Mexico, which has become one of the most dangerous countries for journalists, and his murder preceded the 1 July [[Mexican general election, 2012|2012 Mexican elections]], in which drug violence had become a major issue.<ref name=fpri>{{cite news|author=Foreign Policy Research Institute |title=Will Bloodshed Mar the July 1 Mexican Election? |url=http://www.minyanville.com/business-news/politics-and-regulation/articles/mexican-election-mexican-politics-violence-in/6/29/2012/id/42071 |date=2012-06-29 |accessdate=2012-12-02}}</ref>

== Personal ==
Marco Antonio Ávila's mother is Josefina García.<ref name=eu>{{cite news|url=http://www.eluniversal.com.mx/estados/85937.html |title=Exigen justicia tras asesinato - Los Estados |publisher=El Universal |date=2012-05-20 |accessdate=2012-10-19}}</ref><ref name=latimesblogs>{{cite news|author=&nbsp; |url=http://latimesblogs.latimes.com/world_now/2012/05/six-current-former-journalists-killed-in-mexico-in-less-than-a-month.html |title=Six journalists killed in Mexico in under a month - latimes.com |publisher=Latimesblogs.latimes.com |date=2012-05-20 |accessdate=2012-10-19|page=m}}</ref> Ávila was thirty-nine-years old at the time of his murder. He was married to wife Karina Judith and the couple had two young children.<ref name=eu /> He was buried at Panteón Jardín Mission in Ciudad Obregón.<ref name=eu />

== Career ==
Ávila had been a reporter for 15 years at the time of his murder and the most experienced on the staff of the sister newspapers for which he worked.<ref name=usatoday>{{cite news|url=http://www.usatoday.com/news/world/story/2012-05-18/journalist-kidnapped-killed-mexico/55069956/1 |title=Journalist kidnapped and killed in northern Mexico – |publisher=USA Today |date=2012-05-18 |accessdate=2012-10-19}}</ref> He often wrote about organized crime throughout the country. Eduardo Flores, his director at both newspapers, said Ávila was never allowed to write information that would be considered overly aggressive and was not allowed to mention names associated with drug trafficking either, and he added that Ávila had not been threatened.<ref name=usatoday /><ref name=IPI>{{cite web|url=http://www.freemedia.at/home/singleview/article/kidnapped-mexican-reporter-found-dead.html |title=IPI International Press Institute: Kidnapped Mexican Reporter Found Dead |publisher=Freemedia.at |date=2012-05-21 |accessdate=2012-10-19}}</ref>

== Death ==
{{Location map+|Mexico|AlternativeMap = Mexico States blank map.svg
 | float = left
 | position = left
 | width = 280
 |caption= Mentioned locations within Mexico relative to the capital Mexico City.
 |alt = THE ADDED CITY is located in Mexico.
 |places=
  {{Location map~ | Mexico 
  |lat_deg=19 |lat_min=25 |lat_sec=57 |lat_dir=N
  |lon_deg=99 |lon_min=07 |lon_sec=59 |lon_dir=W 
  |position=left    
  |background=#FFFFFF 
  |label=Mexico City}}
  {{Location map~ | Mexico 
  |lat_deg=29 |lat_min=27 |lat_sec=29 |lat_dir=N
  |lon_deg=109 |lon_min=55 |lon_sec=55 |lon_dir=W 
  |position=left    
  |background=#FFFFFF 
  |label=Ciudad Obregón}}
}}
Marco Antonio Ávila Garcia was abducted in Ciudad Obregón 17 May 2012 around 4:30&nbsp;p.m.<ref name=IFEX>{{cite web|url=http://www.ifex.org/mexico/2012/05/22/avila_killed/ |title=Journalist Marcos Ávila García found dead in Sonora after being kidnapped |publisher=IFEX |date=2012-05-22 |accessdate=2012-10-19}}</ref> While waiting at a car wash, Ávila was asked if he was a journalist and then he was taken by three armed and masked men.<ref name=RSF>{{cite web|url=http://en.rsf.org/mexico-more-attacks-on-media-targets-just-15-05-2012,42624.html |title=Crime reporter is fourth journalist to be killed in less than a month - Reporters Without Borders |publisher=En.rsf.org |date= |accessdate=2012-10-19}}</ref> The net that authorities created around the area after the journalist's disappearance failed to capture the criminals as his body was found 120 kilometers (almost 75 miles) from the scene of the abduction.<ref name=IFEX /> The cause of death was strangulation.<ref name=CPJ /> His corpse was found the next day wrapped in a large, black plastic bag on the side of the road around [[Guaymas, Sonora|Guaymas]] and near Empalme in the state of Sonora, Mexico. Police say his body showed signs of torture<ref name=Guardian1 /><ref name=ai /> and one ear had been cut off.<ref name=eu /> A note written by a drug cartel was found near his body. The attorney general's office would not reveal which of the two warring cartels—the [[Gulf Cartel]] or the [[H. Cartel]]<ref name=RSF />—in Ciudad Obregón wrote the note.<ref name=Guardian1 /><ref name=ai />

== Context ==
Mexico has become one of the most dangerous places for journalists in the world, but observers have noticed a sharp rise in the violence against journalists heading into the [[Mexican general election, 2012|2012 presidential elections]].<ref name=IPI /><ref name=Guardian2>{{cite news|last=Greenslade |first=Roy |url=https://www.theguardian.com/media/greenslade/2012/may/22/journalist-safety-mexico |title=Mexican journalist tortured and murdered &#124; Media &#124; guardian.co.uk |publisher=Guardian |date=2012-05-22 |accessdate=2012-10-19 |location=London}}</ref> In the month before Ávila's murder, six other journalists have been killed in drug-related violence.<ref name=latimesblogs /><ref name=usatoday /><ref name=IPI /><ref name=Guardian2 /><ref>{{cite news|url=http://www.theatlanticwire.com/global/2012/05/being-journalist-mexico-can-be-deadly/52594/ |title=Being a Journalist in Mexico Can Be Deadly - Global |publisher=The Atlantic Wire |date=2012-05-21 |accessdate=2012-10-19}}</ref> Two weeks before Garcia's murder, the bodies of three other reporters were found in large trash bags similar to that in which Garcia was found but submerged in water. A reporter who had disappeared in January was found dead one week before Ávila's death.<ref name=Guardian1 />

== Impact ==
Ávila's death shows how dangerous the country is getting as criminals kill with impunity.<ref name=sipiapa>{{cite web|title=Impunity/Mexico|publisher=Inter American Press Association|url=http://www.sipiapa.org/v4/det_resolucion.php?asamblea=49&resid=743&idioma=us|date=2012-10-16|accessdate=2012-11-13}}</ref> Depending on the source, He was the 81st or 92nd journalist killed between 2000 and 2012 and 14 to 15 others had also disappeared in the same period.<ref name=ai /> According to the Foreign Policy Research Institute, violence by the drug cartels was expected to increase in the period before the election and would be the major issue of the Mexican general election.<ref name=fpri />

== Reactions ==
[[Irina Bokova]], director-general of [[UNESCO]], said, “The number of murders in the ranks of the Mexican media has reached such alarming  proportions that it seriously undermines freedom of expression and democracy in the country. Impunity cannot be allowed to prevail and I urge the authorities to take every necessary step to bring his assassins to trial.”<ref name=UNESCO>{{cite web|author=Source: Unescopress |url=http://www.unesco.org/new/en/media-services/single-view/news/director_general_calls_for_end_to_violence_following_murder_of_mexican_journalist_marco_antonio_avila_garcia/ |title=Director-General calls for end to violence following murder of Mexican journalist Marco Antonio Ávila García &#124; United Nations Educational, Scientific and Cultural Organization |publisher=Unesco.org |date= |accessdate=2012-10-19}}</ref>

Bethel McKenzie, executive director of the [[International Press Institute]], said: "It is no secret that Mexico is facing a major public safety crisis. But it should be no less obvious that journalists play an extremely critical role in Mexico by bringing the activities of drug cartels and of the corrupt politicians who support them to the public light. This valuable work is the reason that journalists are being silenced, and it is also the reason that - despite the difficulties involved - the federal government must step up and bring those responsible for crimes against the media to justice."<ref name=IPI /><ref name=Guardian2 />

Raul Plascencia Villanueva, commission president of the [[National Human Rights Commission]], called for an investigation and urged a policy of zero tolerance against the murder of journalists.<ref>{{cite news|url=http://articles.cnn.com/2012-05-18/americas/world_americas_mexico-journalist-killed_1_crime-reporter-sonora-state-mexican-authorities?_s=PM:AMERICAS |title=Mexican crime reporter found killed in Sonora state |publisher=CNN |date=2012-05-18 |accessdate=2012-10-19}}</ref>

Josefina García, who is Ávila's mother, said, "I am a mother devastated by pain. I do not want this to be so. I do not want impunity. Like many others, this murder needs to be investigated with results because if we don't, then after a while those who murder will continue their attacks against other journalists."<ref name=eu />

==See also==
*[[Mexican Drug War]]
*[[List of journalists killed in Mexico]]

== References ==
{{Reflist}}

{{Mexican Drug War}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Garcia, Marco Antonio Avila}}
[[Category:Articles created via the Article Wizard]]
[[Category:1973 births]]
[[Category:2012 deaths]]
[[Category:Assassinated Mexican journalists]]
[[Category:Journalists killed in the Mexican Drug War]]
[[Category:Murder in 2012]]
[[Category:Deaths by strangulation]]
[[Category:Mexican murder victims]]