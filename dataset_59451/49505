{{infobox organization
| image               = OuterCurve Foundation Logo.gif
| image_border        =
| size                = 300px
| caption             =
| map                 =
| msize               =
| mcaption            =
| abbreviation        = 
| motto               = 
| formation           = 10 September 2009
| extinction          =
| type                = [[Non-governmental organization|NGO]] and [[Non-profit organization]]
| status              = Foundation
| purpose             = Educational
| headquarters        = 
| location            =
| region_served       = Worldwide
| membership          = 
| language            =
| leader_title        = Executive Director
| leader_name         = Erynn Petersen
| main_organ          =
| parent organization =
| num_staff           = 
| num_volunteers      =
| budget              =
| website             = {{url|http://www.outercurve.org/}}
| remarks             =
}}

The '''Outercurve Foundation''' is an independent [[501(c)(6)]] non-profit corporation founded by [[Microsoft]].<ref>{{cite web | url=http://www.outercurve.org/About/FAQ/Mission | title=Mission | publisher=Outercurve | accessdate=June 24, 2012}}</ref> with the goal to "enable the exchange of code and understanding among software companies and open source communities."  They run several [[software project]]s, some of which are connected to the [[.NET Framework]].<ref>{{cite web | url=http://www.outercurve.org/Galleries/ASPNETOpenSourceGallery | title=ASP.NET Open Source Gallery | publisher = Outercurve  | accessdate=June 24, 2012}}</ref><ref>{{cite web | url=http://www.outercurve.org/Galleries/ResearchAccelerators | title=Research Accelerators | publisher = Outercurve | accessdate=June 24, 2012}}</ref><ref>{{cite web | url=http://www.outercurve.org/Galleries/DataLanguagesandSystemsInteroperability | title=Data, Languages, and Systems Interoperability Gallery | publisher = Outercurve | accessdate=June 24, 2012}}</ref><ref>{{cite web | url=http://www.outercurve.org/Galleries/InnovatorsGallery | title=Innovators Gallery | publisher=Outercurve | accessdate=June 24, 2012}}</ref>

It was founded on September 10, 2009 as the [[CodePlex]] Foundation, led mostly by Microsoft employees and affiliates.<ref>{{cite web | url=http://www.hanselman.com/blog/MicrosoftCreatesTheCodePlexFoundation.aspx | title=Microsoft creates the CodePlex foundation | work=Scott Hanselman's Computer Zen | accessdate=June 24, 2012}}</ref>  The name Outercurve Foundation was adopted In September 2010.<ref>{{cite web | url=http://www.outercurve.org/News/articleType/ArticleView/articleId/19/The-CodePlex-Foundation-Rebrands-Renamed-Outercurve-Foundation | title=The CodePlex Foundation Rebrands, Renamed Outercurve Foundation | publisher=Outercurve | accessdate=March 27, 2013}}</ref>  In November 2010 changes to by-laws were made and the board was expanded.<ref>{{cite web | url=http://www.outercurve.org/About | title=About the Outercurve Foundation | publisher=Outercurve Foundation | accessdate=June 24, 2012}}</ref>  Outercurve now serves the larger [[free and open-source]] community as a generalized low-overhead foundation for projects and organizations. Projects contributed by the group to the [[.NET Foundation]] include [[Nuget]], Kudu and the [[ASP.NET AJAX]] library.<ref>{{cite web|url=https://www.dotnetfoundation.org/blog/announcing-new-governance-model-and-project-contributions-to-the-net-foundation|title=Announcing new governance model and project contributions to the .NET Foundation|website=dotnetfoundation.org|date=November 12, 2014}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.outercurve.org/}}


{{Microsoft}}

[[Category:Free software]]
[[Category:Non-profit corporations]]
[[Category:Microsoft]]
[[Category:501(c)(6) nonprofit organizations]]


{{org-stub}}