{{redirect|Suzhou Airport|the military airport in Suzhou|Suzhou Guangfu Airport}}
{{Infobox airport
| name         = Sunan Shuofang International Airport
| image        = 无锡机场-nosign.jpg
| IATA         = WUX
| ICAO         = ZSWX
| coordinates  = {{coord|31|29|40|N|120|25|46|E|display=inline,title}}
| pushpin_map         = China
| pushpin_label       = '''WUX'''
| pushpin_map_caption = Location of airport in China
| type         = Public
| owner-oper   = Sunan Shuofang International Airport Ltd.
| city-served  = [[Wuxi]] and [[Suzhou]]
| location     = [[Shuofang Subdistrict]], Wuxi, Jiangsu
| elevation-m  = 5
| website      = {{url|www.wuxiairport.com}}
| metric-elev  = y
| metric-rwy   = y
| r1-number    = 03/21
| r1-length-m  = 3,200
| r1-surface   = [[Asphalt]]
| stat-year    = 2013
| stat1-header = Passengers
| stat1-data   = 3,590,188
| stat2-header = Cargo (tons)
| stat2-data   = 87,641.6
| stat3-header = Aircraft movements
| stat3-data   = 31,844
| footnotes    = Sources:<ref name="Airport">{{zh icon}} [http://www.wuxiairport.com/ Wuxi Airport Co., Ltd.]</ref><ref name="GCM">{{GCM|ZSWX|Sunan Shuofang International Airport}}</ref>
}}
{{Chinese
|s=苏南硕放国际机场
|t=苏南碩放國際機場
|p=Sūnán Shuòfàng Guójì Jīchǎng
}}
'''Sunan Shuofang International Airport''' {{airport codes|WUX|ZSWX}} is an airport serving the cities of [[Wuxi]] and [[Suzhou]] in southern [[Jiangsu Province]], China (Sunan meaning "Southern Jiangsu" in Chinese). It is located in [[Shuofang Subdistrict]] (硕放街道), {{convert|12|km|sp=uk}} southeast of Wuxi and {{convert|22|km|0|abbr=on}} northwest of Suzhou.<ref>[http://baike.baidu.com/view/1472258.htm 苏南硕放国际机场]</ref>  The airport was built in 1955 for military use, and commercial flights only started in 2004.  Formerly called '''Wuxi Shuofang Airport''' (无锡硕放机场), it took the current name in November 2010 and is now co-owned by the governments of Wuxi, Suzhou, and Jiangsu Province.<ref>[http://www.wuxiairport.com/web101/jtdt/jtyw/190.shtml 苏南硕放国际机场有限公司正式揭牌]</ref>  In 2013, Sunan Shuofang Airport handled 3,590,188 passengers, making it the [[List of the busiest airports in China|42nd busiest airport]] in China.

==Facilities==
The airport has one [[runway]] designated 03/21 which measures {{convert|3200|x|50|m|0}}.<ref name="GCM"/>

==Airlines and destinations==

===Passenger<ref>{{cite web|url=http://www.wuxiairport.com/web101/hbskb/index.shtml|title=苏南机场2016夏秋季航班时刻表 |publisher=Sunan Shuofang International Airport |accessdate=2016-04-13}}</ref>===
{{Airport-dest-list
| [[Air China]] | [[Beijing Capital International Airport|Beijing-Capital]]
| [[Cambodia Angkor Air]] | '''Charter:''' [[Siem Reap International Airport|Siem Reap]]
| [[China Airlines]] | [[Taoyuan International Airport|Taipei-Taoyuan]]
| [[China Eastern Airlines]] | [[Suvarnabhumi Airport|Bangkok-Suvarnabhumi]], [[Beijing Capital International Airport|Beijing-Capital]], [[Changsha Huanghua International Airport|Changsha]], [[Chengdu Shuangliu International Airport|Chengdu]], [[Chongqing Jiangbei International Airport|Chongqing]], [[Dalian Zhoushuizi International Airport|Dalian]], [[Guangzhou Baiyun International Airport|Guangzhou]], [[Guiyang Longdongbao International Airport|Guiyang]], [[Harbin Taiping International Airport|Harbin]], [[Hohhot Baita International Airport|Hohhot]],<ref name="cesjs2017"/> [[Hong Kong International Airport|Hong Kong]], [[Kunming Changshui International Airport|Kunming]], [[Lanzhou Zhongchuan Airport|Lanzhou]], [[Lijiang Sanyi Airport|Lijiang]], [[Qingdao Liuting International Airport|Qingdao]], [[Incheon International Airport|Seoul-Incheon]], [[Sanya Phoenix International Airport|Sanya]], [[Shenyang Taoxian International Airport|Shenyang]], [[Shenzhen Bao'an International Airport|Shenzhen]], [[Singapore Changi Airport|Singapore]], [[Taiwan Taoyuan International Airport|Taipei-Taoyuan]], [[Taiyuan Wusu International Airport|Taiyuan]],<ref name="cesjs2017">[http://mp.weixin.qq.com/s/NKzFX677xFhJcxMM_LGbxg 换季！换季！]</ref> [[Tongren Fenghuang Airport|Tongren]],<ref>[http://www.weibo.com/1684957861/DwS357Jj0?from=page_1005051684957861_profile&wvr=6&mod=weibotime&type=comment#_rnd1464106271844]</ref> [[Xiamen Gaoqi International Airport|Xiamen]], [[Xianyang International Airport|Xi'an]], [[Yinchuan Hedong International Airport|Yinchuan]], [[Zhuhai Sanzao Airport|Zhuhai]]<br /> '''Charter:''' [[Jeju International Airport|Jeju]], [[Phuket International Airport|Phuket]], [[Siem Reap International Airport|Siem Reap]]<br>'''Seasonal charter''': [[Da Nang International Airport|Da Nang]]<ref>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/267902/china-eastern-adds-new-se-asia-routes-in-early-july-2016/|title=China Eastern Adds New SE Asia Routes in early-July 2016|publisher=Routes Online|date=13 July 2016|accessdate=13 July 2016}}</ref>
| {{nowrap|[[China Southern Airlines]]}} | [[Guangzhou Baiyun International Airport|Guangzhou]], [[Shenzhen Bao'an International Airport|Shenzhen]]
| [[HK Express]] | [[Hong Kong International Airport|Hong Kong]]
| [[Jin Air]] | [[Gimhae International Airport|Busan]]<ref name="Wuxi">{{cite web|url=http://airlineroute.net/2016/04/18/wux-apr16/|title=Wuxi Enhances International/Regional Links from late-April 2016|publisher=airlineroute|accessdate=18 April 2016}}</ref>
| [[Mandarin Airlines]] | [[Taichung Airport|Taichung]]<ref name="Wuxi"/>
| [[New Gen Airways]]|[[Krabi Airport|Krabi]]
| [[Shenzhen Airlines]] | [[Suvarnabhumi Airport|Bangkok-Suvarnabhumi]], [[Beijing Capital International Airport|Beijing-Capital]], [[Changchun Longjia International Airport|Changchun]], [[Changsha Huanghua International Airport|Changsha]], [[Chengdu Shuangliu International Airport|Chengdu]], [[Chongqing Jiangbei International Airport|Chongqing]], [[Dalian Zhoushuizi International Airport|Dalian]], [[Fuzhou Changle International Airport|Fuzhou]], [[Guangzhou Baiyun International Airport|Guangzhou]], [[Guilin Liangjiang International Airport|Guilin]], [[Guiyang Longdongbao International Airport|Guiyang]], [[Kunming Changshui International Airport|Kunming]], [[Lanzhou Zhongchuan Airport|Lanzhou]], [[Macau International Airport|Macau]], [[Manzhouli Xijiao Airport|Manzhouli]], [[Kansai International Airport|Osaka-Kansai]], [[Sanya Phoenix International Airport|Sanya]], [[Jieyang Chaoshan International Airport|Shantou]], [[Shenyang Taoxian International Airport|Shenyang]], [[Shenzhen Bao'an International Airport|Shenzhen]], [[Taiwan Taoyuan International Airport|Taipei-Taoyuan]], [[Narita International Airport|Tokyo-Narita]],<ref>{{cite web|url=http://airlineroute.net/2016/04/13/zh-wuxnrt-may16/|title=Shenzhen Airlines Resumes Wuxi – Tokyo Service May/June 2016|publisher=airlineroute|accessdate=13 April 2016}}</ref> [[Wuhan Tianhe International Airport|Wuhan]], [[Xiamen Gaoqi International Airport|Xiamen]], [[Xi'an Xianyang International Airport|Xi'an]], [[Yichang Sanxia Airport|Yichang]], [[Yinchuan Hedong International Airport|Yinchuan]]
| [[Sichuan Airlines]] | [[Chengdu Shuangliu International Airport|Chengdu]], [[Chongqing Jiangbei International Airport|Chongqing]]
|[[Sriwijaya Air]] | '''Seasonal Charter:''' [[Ngurah Rai International Airport|Denpasar/Bali]]
| [[Tigerair]] | [[Singapore Changi Airport|Singapore]]<ref>{{cite web|url=http://airlineroute.net/2016/03/10/tr-wux-s16/|title=tigerair Adds Singapore – Wuxi Service from late-April 2016|publisher=airlineroute|accessdate=10 March 2016}}</ref>
| [[Uni Air]] | [[Taichung Airport|Taichung]]<ref name="Wuxi"/><br>'''Charter:''' [[Kaohsiung International Airport|Kaohsiung]]<ref>{{cite web|url=http://airlineroute.net/2015/09/04/b7-khhwux-w15/|title=UNI Air Adds Kaohsiung - Wuxi Scheduled Charter Service from Nov 2015|publisher=Airlineroute.net|date=4 September 2015|accessdate=4 September 2015}}</ref>
| [[Vietnam Airlines]] | [[Cam Ranh International Airport|Nha Trang]] (begins 12 July 2017)<ref>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/272053/vietnam-airlines-adds-new-cam-ranh-china-routes-from-july-2017/|title=Vietnam Airlines adds new Cam Ranh - China routes from July 2017|publisher=routesonline|accessdate=28 March 2017}}</ref><br>'''Charter:''' [[Phu Quoc International Airport|Phu Quoc]]
| [[Xiamen Airlines]] | [[Fuzhou Changle International Airport|Fuzhou]], [[Harbin Taiping International Airport|Harbin]]
}}

===Cargo===
{{ Airport-dest-list
|  [[Donghai Airlines]] | [[Beijing Capital International Airport|Beijing-Capital]], [[Quanzhou Jinjiang International Airport|Quanzhou]], [[Shenzhen Bao'an International Airport|Shenzhen]]
|  [[SF Airlines]] |  [[Shenzhen Bao'an International Airport|Shenzhen]]
|  [[Yangtze River Express]]| [[Hong Kong International Airport|Hong Kong]], [[Shanghai Pudong International Airport|Shanghai-Pudong]]
}}

==See also==
{{Portal|China|Aviation}}
* [[List of airports in China]]
* [[List of the busiest airports in China]]

==References==
{{reflist}}

==External links==
*[http://www.wuxiairport.com Official web site]
*{{ASN|WUX|WUX / ZSWX}}

{{Airports in China}}
{{Southern Jiangsu transit}}

[[Category:Airports in Jiangsu]]
[[Category:Wuxi]]
[[Category:Transport in Suzhou]]
[[Category:Airports established in 1955]]
[[Category:1955 establishments in China]]