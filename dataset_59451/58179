{{Distinguish|American Journal of Human Genetics}}
{{Infobox journal
| title = Journal of Human Genetics
| formernames = Japanese Journal of Human Genetics
| cover = [[File:JHumGenetCover.jpg]]
| editor = Katsushi Tokunaga
| discipline = [[Human genetics]]
| abbreviation = J. Hum. Genet.
| publisher = [[Nature Publishing Group]] on behalf of the [[Japan Society of Human Genetics]]
| country = Japan
| frequency = Monthly
| history = 1956-present
| openaccess =
| impact = 2.487
| impact-year = 2015
| website = http://www.nature.com/jhg/index.html
| link1 =
| link1-name =
| link2 = http://www.nature.com/jhg/archive/index.html
| link2-name = Online archive
| JSTOR =
| OCLC = 474778834
| LCCN = sn98039238
| CODEN = JHGEFR
| ISSN = 1434-5161
| eISSN = 1435-232X
}}
The '''''Journal of Human Genetics''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[human genetics]] and [[genomics]]. It was established in 1956 as the ''Japanese Journal of Human Genetics'' and was independently published by the [[Japan Society of Human Genetics]]. In 1998 [[Springer Science+Business Media]] took over publishing. It has been published by the [[Nature Publishing Group]] since January 2009. While under Springer the publication frequency changed from bimonthly to monthly. Since 2014 the [[editor-in-chief]] is [[Naomichi Matsumoto]] ([[Yokohama City University]]).<ref>{{cite journal|doi=10.1038/jhg.2013.127|title=A message from the new Editor-in-Chief|journal=Journal of Human Genetics|volume=59|pages=1|year=2014|last1=Matsumoto|first1=Naomichi}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.462.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Human Genetics |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.nature.com/jhg/archive/index.html}}

[[Category:Medical genetics journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1956]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]


{{Sci-journal-stub}}