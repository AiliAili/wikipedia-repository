<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Spitfire Mk 26
  |logo = 
  |image =SupermarineSpitfireMk26.jpg
  |caption = 
}}{{Infobox Aircraft Type
  |type = [[Homebuilt aircraft]]
  |national origin=Australia
  |manufacturer = [[Supermarine Aircraft]]
  |designer = Michael O'Sullivan
  |first flight = November {{avyear|1994}} 
  |introduction = 
  |retired = 
  |status = 
  |primary user =                   
  |more users = 
  |produced = 
  |number built = 100 (Dec 2011)<ref name="KitplanesDec2011" />
  |program cost= 
  |unit cost = A$395,000 (Mk 26b kit)<br>A$45,500 (GM Isuzu V6 engine) 
  |developed from = 
  |variants with their own articles = 
}}
|}

The '''Supermarine Spitfire Mk 26''' is a two-seat Australian [[homebuilt aircraft]] produced in kit form by [[Supermarine Aircraft]]<ref>Not to be confused with the original [[Supermarine]] company</ref> for completion by amateur builders.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2011 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 71. Belvoir Publications. ISSN 0891-1851</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 121. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The Spitfire Mk 26 is an '80% or 90% scale' (but see note of scale inaccuracies in the Variants section below) replica based on the original [[Supermarine Spitfire]] design. Despite its smaller size provision is made for a passenger to be seated in tandem behind the pilot. The [[stressed skin]] structure consists of [[2024 aluminium alloy]] skins, [[former]]s and [[longeron]]s with some [[Glass-reinforced plastic|fibre-glass]] mouldings for parts such as [[Aircraft fairing|fairings]] and air scoops. The design features retractable [[Landing gear|undercarriage]] with [[Landing gear#Differential braking|differential braking]] to the main wheels. The aircraft has been approved by the British [[Light Aircraft Association]].<ref>[http://www.lightaircraftassociation.co.uk/pdf/Engineering/Website%20Accepted%20Aircraft%20List.pdf Light Aircraft Association - Approved aircraft list] Retrieved: 31 January 2009</ref>

===Powerplants===
Early versions of the Spitfire Mk 26 used an eight-cylinder 180 [[Horsepower|hp]] [[Jabiru Aircraft|Jabiru]] 5100 horizontally opposed aero engine, but early installations suffered from inadequate cooling. The company now offers a converted [[General Motors]] [[Isuzu]] [[V6 engine]]. The [[normally aspirated]] version of this engine produces 226&nbsp;hp (168&nbsp;kW) with a [[supercharger|supercharged]] version producing up to 310&nbsp;hp (231&nbsp;kW).<ref>[http://www.supermarineaircraft.com/Engine.htm Supermarine Aircraft - Engine and Propeller] Retrieved: 31 January 2009</ref>
The maximum rpm of a propeller (at about 2800 depending on its diameter) is about half that at the maximum torque/power rpm of about 5500rpm for a car engine and these engines therefore need to be fitted with a drive reduction unit. Such units absorb about 20% of the engine power and therefore the normally aspirated Isuzu unit delivers the same maximum power as the Jabiru 5100. Recent developments of the Jabiru engine range by Rotec have produced replacement water-cooled cylinder heads for the 5100. This has removed the overheating problems and also allowed the nose cowling of the Mk26 to be reshaped to remove the air intake and considerably reduce the frontal area to be in keeping with the original sleek design of the Spitfire. First flights of a Mk26 with the redeveloped water-cooled Jabiru 5100 are expected in 2016.

==Variants==
[[File:F-PFAF Super Marine Aircraft Spitfire Mk.26 (9705875977).jpg|thumb|Side view of a Spitfire Mk.26, Sywell Airfield, 2013]]

;Mk 25
:Single-seat version, no longer produced, 75% scale. This was a true 'three-quarter' scale size of the original WW2 MK5 Spitfire
;Mk 26
:Two-seat version. '80% scale'. Discontinued by 2011 in favor of the '90%' version. The '80% scale' refers only to the fuselage that was lengthened to 80% of the original MK5 Spitfire. The same wing was used from the 75% scale aircraft, resulting in a wingspan and undercarriage height identical to the original 75% scale aircraft.<ref name="WDLA11" />
;Mk 26b
:Improved Mk 26. Option of dual controls, '90% scale'. Again the '90% scale' refers only to the fuselage, that was again lengthened (in fact to 89.5% the length of the original MK5 Spitfire); the fuselage was also made fatter in the cockpit area. The same wing was used from the 75% scale aircraft, resulting in a wingspan and undercarriage height identical to the original 75% scale aircraft.

==Construction==
Construction of the Mk 26 requires metalworking skills and tools and makes extensive use of aviation grade pull rivets as used in the manufacture of the DC3, sometimes in error called [[Rivet|pop rivets]] (pop rivets were originally patented for use in shoe-making). Pre-assembled kits are provided but still leave the builder with 1200 [[man-hour]]s of work to be completed.<ref>[http://www.supermarineaircraft.com/Construct.htm Supermarine Aircraft - Construction] Retrieved: 31 January 2009</ref>

==Fatal accidents/ controversy==

After a fatal accident at Gympie, Australia, in Oct 2010, involving the Mk 26, the coroner reported, on December 29, 2014 -  - [http://www.courts.qld.gov.au/__data/assets/pdf_file/0005/337622/cif-uscinski-20141229.pdf] that Michael O’Sullivan, the CEO of Supermarine Pty Ltd, admitted that the aircraft test flight period had only been 20 hours instead of the 37.5 hours declared; that he had “knowingly falsified documents to achieve registration of his aircraft with RA-Aus (Recreational Aviation Australia), rather than the more stringent registration with CASA (Civil Aviation Safety Authority)”; and admitted to “significantly understating the weight of the aircraft (by about 200kg)" (around half of the aircraft’s stated empty weight of 401&nbsp;kg).

In 2013, A MK26 80% scale Spitfire crashed in Adelaide, AUS. The pilot, as the only passenger in the aircraft, died as a result of the accident. Official findings show pilot error as the main contributor of the accident, however, the ATSB report: http://www.atsb.gov.au/media/4548260/ao-2013-051_final.pdf stated: “The aircraft was prone to aerodynamically stall with little or no aerodynamic precursors and it was not fitted with a stall warning device, increasing the risk of inadvertent stall.”

==Specifications (Mk 26b)==
[[File:SpitfireMk26b.jpg|thumb|right|Instrument panel of the Mk26b]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?= plane
|jet or prop?= prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|ref=Supermarine Aircraft <ref>[http://www.supermarineaircraft.com/Specs.htm Supermarine Aircraft - Specifications] Retrieved: 31 January 2009</ref>
|crew= 1 
|capacity= 1 passenger
|length main= 23 ft 9 in 
|length alt= 7.24 m
|span main= 27 ft 8 in
|span alt= 8.43 m
|height main= 6 ft 9 in
|height alt=2 m
|area main= 122 ft²
|area alt= 11.3 m²
|airfoil=
|empty weight main= 992 lb
|empty weight alt= 450 kg
|loaded weight main= 1,785 lb
|loaded weight alt= 810 kg
|useful load main= <!--lb-->
|useful load alt= <!--kg-->
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[General Motors]] [[Isuzu]] 
|type of prop=[[V6 engine]] driving 3-bladed composite [[Ivoprop]]
|number of props=1
|power main= 226 hp<!--hp-->
|power alt= 168 kW<!--kW-->
|power original=
|max speed main= 230 knots
|max speed alt= 265 mph, 426 km/h
|cruise speed main= 160 knots
|cruise speed alt= 184 mph, 296 km/h
|never exceed speed main= 230 knots
|never exceed speed alt= 265 mph, 426 km/h
|stall speed main= 42 knots
|stall speed alt= 48 mph, 78 km/h
|range main= <!--nm-->
|range alt= <!--mi,km-->
|ceiling main= <!--ft-->
|ceiling alt= <!--m-->
|climb rate main= 2,500 ft/min
|climb rate alt= 762 m/min
|loading main= 14.6 lb/ft²
|loading alt= 71.7 kg/m²
|thrust/weight= <!--a unitless ratio-->
|power/mass main= <!--hp/lb-->
|power/mass alt= <!--W/kg-->
|more performance=*Limited aerobatics, +6 -4 g 
|armament=
|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
*[[Isaacs Spitfire]]
*[[Jurca Spit]]
*[[Sindlinger Hawker Hurricane]]
|lists=
|see also=

}}

==References==
{{reflist}}

==External links==
{{Commons category}}
*[http://www.supermarineaircraft.com/About.htm Supermarine Aircraft - Main page]
*[http://www.pilotfriend.com/experimental/acft6/39.htm Spitfire Mk 26 article at Pilotfriend.com]
*[http://www.pbase.com/snwau/image/95688235 Image of Spitfire Mk 26]

[[Category:Australian sport aircraft 1990–1999]]
[[Category:Homebuilt aircraft]]
[[Category:Replica aircraft]]