{{Infobox journal
| title = Journal of Urban History
| cover = Journal of Urban History.tif
| editor = David R. Goldfield
| discipline = [[Urban Studies]]
| former_names =
| abbreviation = J. Urban Hist.
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 1974-present
| openaccess =
| license =
| impact = 0.178
| impact-year = 2015
| website = https://sagepub.com/en-gb/eur/journal-of-urban-history/journal200943
| link1 = http://juh.sagepub.com/content/current
| link1-name = Online access
| link2 = http://juh.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0096-1442
| eISSN = 1552-6771
| OCLC = 1798556
| LCCN = 78641478
}}
The '''''Journal of Urban History''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering the field of [[urban studies]]. The [[editor-in-chief]] is [[David Goldfield|David R. Goldfield]] ([[University of North Carolina at Charlotte]]). It was established in 1974 and is published by [[SAGE Publications]] in association with the [[Urban History Association]].

==History==
The journal published its first issue in November 1974. Raymond A. Mohl served as editor from the inaugural issue until the position was passed on to Blaine Brownwell in 1977.<ref>{{cite journal |last1=Goldfield |first1=David |title=Editor's Tribute to Raymond A. Mohl |journal=Journal of Urban History |date=May 2015 |volume=41 |issue=3 |page=359}}</ref> Mohl's first editorial noted that the journal would necessarily encompass a variety of subjects, methodologies, and interpretations of urbanity since the emerging field of [[urban history]] was, as an editor described it, a "big tent." <ref>{{cite journal |last1=Mohl |first1=Raymond |title=Editorial |journal=Journal of Urban History |date=November 1974 |volume=1 |issue=1 |page=4}}</ref>

==Editors==
The following persons are or have been [[editors-in-chief]] of the journal:
*Raymond A. Mohl (1974-1977)
*Blaine Brownwell (1977-1990)
*David R. Goldfield (1990-present)

==Abstracting and indexing==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2015 [[impact factor]] is 0.178.<ref name=WoS>{{cite book |year=2016 |chapter=Journal of Urban History |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|https://sagepub.com/en-gb/eur/journal-of-urban-history/journal200943}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Urban studies and planning journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1974]]