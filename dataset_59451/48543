{{About|the album|the style of guitar|semi-acoustic guitar}}
{{Use mdy dates|date=August 2013}}
<!-- For administrator use only: {{Old AfD multi|page=Hollow Bodies (Blessthefall Album)|date=July 29, 2013|result='''keep'''}} -->
<!-- End of AfD message, feel free to edit beyond this point -->
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Hollow Bodies
| Type        = studio
| Artist      = [[Blessthefall]]
| Cover       = Blessthefall's_"Hollow_Bodies"_Cover.jpg
| Released    = {{Start date|2013|8|20}}
| Genre       = [[Metalcore]]
| Length      = 46:15
| Label       = [[Fearless Records|Fearless]]
| Producer    = [[Joey Sturgis]]
| Last album  = ''[[Awakening (Blessthefall album)|Awakening]]''<br />(2011)
| This album  = '''''Hollow Bodies'''''<br />(2013)
| Next album  = ''[[To Those Left Behind]]''<br />(2015)
| Misc           = {{Singles
  |Name          = Hollow Bodies
  |Type          = Studio
  |single 1      = You Wear a Crown, But You're No King
  |single 1 date = June 25, 2013{{citation needed|date=July 2015}}
  |single 2      = Déjà Vu
  |single 2 date = July 30, 2013{{citation needed|date=July 2015}}
  }}}}

'''''Hollow Bodies''''' is the fourth studio album by American [[metalcore]] band [[Blessthefall]]. It was released on August 20, 2013 through [[Fearless Records]] and produced by [[Joey Sturgis]].<ref>{{cite web|url=http://www.altpress.com/news/entry/blessthefall_recording_new_album_with_joey_sturgis |title=Blessthefall recording new album with Joey Sturgis |publisher=Altpress.com |date=April 6, 2013 |accessdate=July 29, 2013 |last=Kraus |first=Brian}}</ref><ref name="puregrainaudio1">{{cite web|url=http://puregrainaudio.com/news/blessthefall-reveals-album-artwork-and-track-listing-for-new-album-hollow-bodies |title=BLESSTHEFALL Reveals Album Artwork and Track Listing for New Album 'Hollow Bodies' « News « |publisher=Puregrainaudio.com |date=June 10, 2013 |accessdate=June 18, 2013}}</ref> It is the third album to include lead singer [[Beau Bokan]], second album to include rhythm guitarist, Elliott Gruenberg and the first to include the same members in consecutive albums.

The band announced the album on June 10, 2013 and published the album cover and track list.<ref>{{cite web|url=http://www.altpress.com/news/entry/blessthefall_announce_new_album_hollow_bodies |title=Blessthefall announce new album, 'Hollow Bodies' |publisher=Altpress.com |date=June 10, 2013 |accessdate=July 29, 2013 |last=Whitt |first=Cassie}}</ref> On June 25 the song "You Wear a Crown, But You're No King" was released as the first single.<ref>{{cite web|publisher=Revolver Magazine |url=http://www.revolvermag.com/news/exclusive-blessthefall-premiere-new-song-you-wear-a-crown-but-youre-no-king.html |title=Blessthefall Premiere New Song, "You Wear a Crown, but You're No King" |date=June 25, 2013 |accessdate=July 29, 2013}}</ref> A video of interviews with the band members in the studio was released on July 24 that also showed a preview of the second single "Déjà Vu" which was released on July 30.<ref>{{cite web| url=http://bloody-disgusting.com/news/3244962/exclusive-blessthefall-hollow-bodies-studio-update/ |title=[Exclusive] Blessthefall 'Hollow Bodies' Studio Update |date=July 24, 2013 |accessdate=August 9, 2013 |author=Jonathan Barkan}}</ref>  On August 13, the entire album was streamed through Billboard.com.<ref>{{cite web|url=http://www.billboard.com/articles/news/5650632/blessthefall-hollow-bodies-exclusive-album-premiere?utm_source=twitter |title=Blessthefall, 'Hollow Bodies': Exclusive Album Premiere |publisher=Billboard |date=October 22, 2011 |accessdate=August 13, 2013}}</ref>  

The album debuted at No.&nbsp;15 on the [[Billboard 200|''Billboard'' 200]] and reached No.&nbsp;1 on the Hard Rock chart selling 21,888 copies in the first week, up over 10,000 from their previous release.<ref>{{cite web|last=Stagg |first=David |url=http://hmmagazine.com/blog/news/blessthefalls-hollow-bodies-tops-hard-rock-charts/ |title=Blessthefall's 'Hollow Bodies' tops Billboard Hard Rock chart |publisher=HM Magazine |date= |accessdate=August 29, 2013}}</ref> The album has sold 58,000 copies in the US as of August 2015.<ref>{{cite web|archiveurl= https://web.archive.org/web/20150825183753/http://hitsdailydouble.com/new_album_releases |archivedate= August 25, 2015 |url=http://hitsdailydouble.com/new_album_releases |title=Upcoming Releases |website=''Hits Daily Double'' |publisher=HITS Digital Ventures }}</ref>

== Background ==

In 2011, the band released their third album [[Awakening (Blessthefall album)|Awakening]] to mostly positive reviews and began numerous tours to support the album.<ref>{{cite web|url=http://www.ign.com/articles/2011/10/11/blessthefall-awakening-review |title=blessthefall: Awakening Review – Metalcore outfit's latest suffers from too much of a good thing. |publisher=IGN |date=October 11, 2011 |accessdate=July 29, 2013 |last=Grischow |first=Chad}}</ref> In late 2012, they revealed they had begun writing new music and were hoping to release their fourth studio album in mid to late 2013. It was announced on [[Twitter]] that [[Joey Sturgis]] would produce the album.<ref>{{cite web|url=https://twitter.com/joeyismusic/status/267569736869617665 |title=Twitter / joeyismusic |publisher=Twitter.com |date= |accessdate=July 29, 2013}}</ref> The band entered the studio in April 2013 and confirmed the album was complete on May 21.<ref>{{cite web|url=https://twitter.com/blessthefall/status/336900685218070528 |title=Twitter / blessthefall: We are done tracking album |publisher=Twitter.com |date= |accessdate=August 8, 2013}}</ref>

The band played at the Vans [[Warped Tour 2013]] on the main stage for the first time and played the song "You Wear A Crown But You're No King" which was later released as a single on June 25, 2013.<ref>{{cite web|url=http://www.altpress.com/news/entry/blessthefall_release_you_wear_a_crown_but_youre_no_king_lyric_video |title=Blessthefall release "You Wear A Crown But You're No King" lyric video |publisher=Altpress.com |last=Bird |first=Michele |date=June 25, 2013 |accessdate=July 29, 2013}}</ref> On July 24, a studio update video was released which discussed the writing recording process. The band stated in short interviews that the album will be different than anything they've ever done; that the album takes on a much heavier mood and sound.<ref>{{cite web|url=http://www.altpress.com/news/entry/blessthefall_post_hollow_bodies_studio_update |title=Blessthefall post 'Hollow Bodies' studio update – Alternative Press |publisher=Altpress.com |date=July 25, 2013 |accessdate=August 8, 2013}}</ref>

The second single, "Déjà Vu", was streamed through altpress.com on July 29, 2013 and was officially released the following day.<ref>{{cite web|url=http://www.altpress.com/aptv/video/song_premiere_blessthefall_deja_vu |title=Song Premiere: Blessthefall, "Déjà Vu" - Alternative Press |publisher=Altpress.com |date=July 29, 2013 |accessdate=August 8, 2013}}</ref>

It was announced that Vic Fuentes of [[Pierce the Veil]] was working on a song with the band.<ref>{{cite web|last=Ableson |first=Jon |url=http://www.alterthepress.com/2013/01/vic-fuentes-pierce-veil-collaboration-w.html |title=Vic Fuentes (Pierce The Veil) Collaboration w/ blessthefall? |publisher=Alter The Press! |date=August 20, 2009 |accessdate=January 30, 2013}}</ref> It was later revealed to be the track, "See You On the Outside" which was streamed through [[Hot Topic]] on August 8, 2013.<ref>{{cite web|url=http://www.infectiousmagazine.com/stream-blessthefall-see-you-on-the-outside/?utm_source=rss&utm_medium=rss&utm_campaign=stream-blessthefall-see-you-on-the-outside |title=STREAM: Blessthefall "See You On The Outside" |publisher=Infectious Magazine |date= |accessdate=August 8, 2013}}</ref>

On August 13, the entire album was streamed through Billboard's website, allowing fans to listen to the album before the official release date.<ref>{{cite web|url=http://www.billboard.com/articles/news/5650632/blessthefall-hollow-bodies-exclusive-album-premiere |title=Stream New Blessthefall Album |publisher = Billboard}}</ref>

== Music and lyrics ==

Phil Freeman of ''[[Alternative Press (music magazine)|Alternative Press]]'' wrote that "this is digitally crunchy modern metalcore" that contains "clean vocals" that "soar over beds of synth while hoarse spittle-flecked roars are bolstered by hammering, downturned guitar riffs." At ''[[HM (magazine)|HM]]'', Anthony Bryant told that this "is another example of the group’s expert harmony of symphonicism and vicious instrumentals".<ref name="hm" /> Andy Biddulph stated that blessthefall has "creat[ed] a perfect marriage of electronica and metalcore."<ref name="rocksound" /> On the subject of lyricism, Freeman wrote that it "is overwrought but not embarrassingly so—the end of the relationship is compared to the collapse of an empire, but somehow it works."<ref name="altpress" />

== Critical reception ==
{{Album ratings
 | rev1 = [[About.com]]
 | rev1Score = {{Rating|3.5|5}}<ref name=about>{{cite web |last=Bowar |first=Chad |date=August 20, 2013 |title=Blessthefall – Hollow Bodies Review |publisher=[[About.com]] |url=http://heavymetal.about.com/od/blessthefall/fr/Blessthefall-Hollow-Bodies-Review.htm | accessdate=December 19, 2013 }}</ref>
 | rev2 = ''[[Alternative Press (music magazine)|Alternative Press]]''
 | rev2Score = {{Rating|4|5}}<ref name=altpress>{{cite journal |last =Freeman | first=Phil |year=2013 |title=blessthefall – ''Hollow Bodies'' |journal=[[Alternative Press (music magazine)|Alternative Press]] | volume=September 2013 |issue=302 |page=83 |publisher= Alternative Press Magazine, Inc. |issn=1065-1667 |url=http://www.altpress.com/magazine/issue/302_asking_alexandria | accessdate=July 26, 2013}}{{broken citation|date=August 2013}}</ref>
 | rev3 = ''[[HM (magazine)|HM]]''
 | rev3Score = {{Rating|4|5}}<ref name=hm>{{cite web |last=Bryant |first=Anthony |date=August 13, 2013 |title=Blessthefall – ''Hollow Bodies'' |work=[[HM (magazine)|HM]] |url=http://hmmagazine.com/blog/album-reviews/blessthefall-hollow-bodies/ |accessdate=August 16, 2013 }}</ref>
 | rev4 = ''[[Music Connection]]''
 | rev4Score = 7/10<ref>{{cite web|author=Album Review: Blessthefall – Hollow Bodies Aug 29 |url=http://musicconnection.com/album-review-blessthefall-hollow-bodies/ |title=Album Review: Blessthefall – Hollow Bodies |publisher=Musicconnection.com |date= |accessdate=August 30, 2013}}</ref>
 | rev5 = ''[[Outburn]]''
 | rev5Score = 8/10<ref name=outburn>{{cite journal |last=Lay |first=Nathaniel |year=2013 |title=Blessthefall: ''Hollow Bodies'' (Fearless) |journal=[[Outburn]] | volume=Nov/Dec 2013 |issue=71 |page=61 |publisher=Octavia Laird and Rodney Kusano |issn=1542-1309 |url=http://www.outburn.com/OUTBURN_71_Asking_Alexandria_p/outburn71.htm | accessdate=December 19, 2013}}</ref>
 | rev6 = ''[[Rock Sound]]''
 | rev6Score = 9/10<ref name=rocksound>{{cite web|author=Andy Biddulph |url=http://www.rocksound.tv/reviews/article/blessthefall-hollow-bodies?utm_source=dlvr.it&utm_medium=twitter&utm_campaign=rocksound |title=Blessthefall – Hollow Bodies &#124; Reviews &#124; Rock Sound |publisher=Rocksound.tv |date= |accessdate=August 13, 2013}}</ref>
}}

''Hollow Bodies'' has received positive reception by [[Music journalism|music critics]]. At ''[[Alternative Press (music magazine)|Alternative Press]]'', Phil Freeman called it "an ambitious, impressive album."<ref name=altpress /> Anthony Biddulph of ''[[Rock Sound]]'' affirmed that this album "isn't a simple step up, this is the album that will define their career."<ref name=rocksound /> At ''[[HM (magazine)|HM]]'', Anthony Bryant wrote that "this album is a blistering example of why many have come to love Blessthefall and shows that, despite its hiccups, this group is still the undisputed ruler of the post-hardcore scene."<ref name=hm /> Nathaniel Lay of ''[[Outburn]]'' stated that the release "carries on their tradition of putting out exciting albums that are worthy of praise and recommendation."<ref name=outburn /> At [[About.com]], Chad Bowar called the album a "step forward".<ref name=about />

== Track listing ==

{{tracklist
| headline =
| writing_credits = no
| title1 = Exodus
| length1 = 5:28
| title2 = You Wear a Crown, But You're No King
| length2 = 3:43
| title3 = Hollow Bodies
| length3 = 4:17
| title4 = Déjà Vu
| length4 = 4:17
| title5 = Buried In these Walls
| length5 = 3:46
| title6 = See You On the Outside
| length6 = 4:14
| title7 = Youngbloods
| note7 = featuring Jesse Barnett of [[Stick to Your Guns]]
| length7 = 2:56
| title8 = Standing On the Ashes
| length8 = 3:12
| title9 = Carry On
| note9 = featuring [[Jake Luhrs]] of [[August Burns Red]]
| length9 = 3:15
| title10 = The Sound of Starting Over
| length10 = 4:08
| title11 = Open Water
| note11 = featuring [[Lights (musician)|Lights]]
| length11 = 6:59
| total_length = 46:15
}}

== Personnel ==
{{unsourced section|date=August 2015}}
'''Blessthefall'''
* [[Beau Bokan]] – lead vocals, keyboards, piano, programming
* Eric Lambert – lead guitar, backing vocals
* Elliott Gruenberg – rhythm guitar
* Jared Warth – bass guitar, unclean vocals
* [[Matt Traynor]] – drums, percussion

'''Other personnel'''
* [[Vic Fuentes]]&nbsp;– co-writing on "See You on the Outside"
* Jesse Barnett&nbsp;– guest vocals on "Youngbloods"
* [[Jake Luhrs]]&nbsp;– guest vocals on "Carry On"
* [[Lights (musician)|Lights]]&nbsp;– guest vocals on "Open Water"

'''Production'''
* [[Joey Sturgis]]&nbsp;– production, mixing, mastering, programming
* Nick Scott – engineering
* Danielle Tunstall&nbsp;– artwork

== References ==
{{reflist}}

{{Blessthefall}}

[[Category:Articles with hAudio microformats]]
[[Category:Blessthefall albums]]
[[Category:2013 albums]]
[[Category:Albums produced by Joey Sturgis]]
[[Category:Christian metal albums]]