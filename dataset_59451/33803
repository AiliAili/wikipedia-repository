{{featured article}}
{{Taxobox
| image = Tachycineta leucorrhoa.jpg
| image_caption=White-rumped swallow in Buenos Aires
| status = LC
| status_system = IUCN3.1
| status_ref = <ref name="IUCN">{{IUCN|id=22712068 |title=''Tachycineta leucorrhoa'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| regnum = [[Animal]]ia
| phylum = [[chordate|Chordata]]
| classis = [[bird|Aves]]
| ordo = [[passerine|Passeriformes]]
| familia = [[swallow|Hirundinidae]]
| genus = ''[[Tachycineta]]''
| species = '''''T. leucorrhoa'''''
| binomial = ''Tachycineta leucorrhoa''
| binomial_authority = ([[Louis Jean Pierre Vieillot|Vieillot]], 1817)
|range_map            = White-rumped swallow map.jpg
|range_map_width      = 
|range_map_caption    =Range of ''T. leucorrhoa''
<big>{{leftlegend|#38c51f|Resident range|outline=gray}}
{{leftlegend|#fcd22f|Breeding visitor|outline=gray}}
{{leftlegend|#269cd1|Winter visitor|outline=gray}}</big>
| synonyms = }}

The '''white-rumped swallow''' (''Tachycineta leucorrhoa'') is a species of [[bird]] in the family [[Hirundinidae]]. First described and given its binomial name by [[France|French]] [[ornithologist]] [[Louis Jean Pierre Vieillot|Louis Vieillot]] in 1817, it was for many years considered a subspecies of the [[Chilean swallow]]. The species is [[monotypic]] with no known population variations. It has a white {{birdgloss|supraloral}} streak, or streak above its [[lores]], the region between a bird's eye and nostrils, which can be used to differentiate it from the Chilean swallow. The lores, [[ear covert]]s, tail, and wings are black, with white tips on the inner secondaries, tertials, and greater coverts of the wings. The rest of the upperparts are a glossy blue. Its underparts and underwing-coverts are white, in addition to the rump, as the name suggests. The sexes are similar, and the juvenile is duller and browner with a dusky breast.

This species usually builds its nest in holes in trees or dead snags or under or in artificial structures like fence posts and the [[eaves]] of buildings. The white-rumped swallow is [[solitary animal|solitary]] and nests in distributed pairs during the [[breeding season]]. The breeding season is from October to December in Brazil and from October to February in neighboring Argentina. Usually only one brood with four to seven eggs is laid, although a second one will occasionally be laid. The female incubates the eggs over a period usually between 15 and 16 days, with the fledging after usually between 21 and 25 days.

This [[swallow]] is found in [[Argentina]], [[Bolivia]], [[Brazil]], [[Paraguay]], [[Peru]], and [[Uruguay]]. Its natural [[habitat]]s are dry [[savanna]], pastureland, the edge of forests, and subtropical or tropical seasonally wet or flooded lowland [[grassland]]. It is classified as a [[least-concern species]] by the [[International Union for Conservation of Nature]] (IUCN). Its population is increasing and it may benefit from the increase in availability of artificial nest sites. An occasional [[brood parasite]] of this bird is the [[shiny cowbird]].

==Taxonomy and etymology==

The white-rumped swallow was first formally described as ''Hirundo leucorrhoa'' by French ornithologist [[Louis Jean Pierre Vieillot|Louis Vieillot]] in 1817 in his ''Nouveau Dictionnaire d'Histoire Naturelle''.<ref name= viellot>{{cite book|last=Vieillot|first=Louis Jean Pierre|year=1817|title=Nouveau Dictionnaire d'Histoire Naturelle, nouvelle édition|volume=14|language=French|page=519}}</ref> It was since moved to its current genus, ''[[Tachycineta]]'', which was created in 1850 by [[Jean Cabanis]].<ref name=cabanis>{{cite book | last1 = Cabanis | first1 = Jean| last2 =  | first2 = | last3 =  | first3 = | authorlink = Jean Cabanis | title = Museum Heineanum : Verzeichniss der ornithologischen Sammlung des Oberamtmann Ferdinand Heine auf Gut St. Burchard vor Halberstatdt | publisher = Independently commissioned by  R. Frantz | series = | volume = 1| edition = | year =1850 | page =48 | language = German| url =http://www.biodiversitylibrary.org/item/196124#page/64/mode/1up }}</ref> The binomial name is derived from [[Ancient Greek]]. ''Tachycineta'' is from ''takhukinetos'', "moving quickly", and the specific ''leucorrhoa'' is from ''leukos'', "white", and ''orrhos'', "rump".<ref name=job>{{cite book | last= Jobling | first= James A. | year= 2010| title= The Helm Dictionary of Scientific Bird Names | publisher= Christopher Helm | isbn = 978-1-4081-2501-4 | pages = 225, 377}}</ref>

The white-rumped swallow was formerly considered a [[subspecies]] of the [[Chilean swallow]], most likely due to the similarity in morphology and calls. It is occasionally placed in the genus ''Iridoprocne'' with the [[tree swallow]], [[mangrove swallow]], [[white-winged swallow]], and Chilean swallow.<ref name="hbw">{{cite web |last1=Turner |first1=Angela |editor1-last=del Hoyo |editor1-first=Josep |editor2-last=Elliott |editor2-first=Andrew |editor3-last=Sargatal |editor3-first=Jordi |editor4-last=Christie |editor4-first=David A. |editor5-last=de Juana |editor5-first=Eduardo |year=2017 |title=White-rumped Swallow (''Tachycineta leucorrhoa'') |work=Handbook of the Birds of the World Alive |url=http://www.hbw.com/species/white-rumped-swallow-tachycineta-leucorrhoa |publisher=Lynx Edicions |access-date=January 14, 2017|subscription=true}}</ref> A study of the mitochondrial DNA of ''Tachycineta'' supports the split, although studies do show that the white-rumped swallow forms a [[superspecies]], ''leucorrhoa'', with the Chilean swallow.<ref>{{cite journal | author=Whittingham, Linda A.; Slikas, Beth; Winkler, David W.; Sheldon, Frederick H. |title=Phylogeny of the tree swallow genus, ''Tachycineta'' (Aves: Hirundinidae), by Bayesian analysis of mitochondrial DNA sequences| journal=  Molecular phylogenetics and evolution |volume= 22|issue=3 |year=2002|pages= 430–441|doi=10.1006/mpev.2001.1073|pmid=11884168}}</ref><ref>{{cite journal | author=Dor, Roi; Carling, Matthew D.; Lovette, Irby J.; Sheldon, Frederick H.; Winkler, David W. |title=Species trees for the tree swallows (Genus ''Tachycineta''): An alternative phylogenetic hypothesis to the mitochondrial gene tree | journal= Molecular phylogenetics and evolution |volume= 65|issue= 1 |year=2012|pages= 317–322|doi=10.1016/j.ympev.2012.06.020|pmid=22750631 }}</ref> This species is monotypic, with no known subspecies.<ref name="hbw"/>

This swallow is named for its white rump but it is also sometimes called the white-browed swallow, due to its white [[supraloral]] streak.<ref name="Turner">{{cite book|last1=Turner|first1=Angela|last2=Rose|first2=Chris|title=A Handbook to the Swallows and Martins of the World|url=https://books.google.com/books?id=0diG7y7Kk54C|date=June 30, 2010|publisher=Bloomsbury Publishing|isbn=978-1-4081-3172-5|pages=106–108}}</ref>

==Description==
[[File:White-rumped Swallow (Tachycineta leucorrhoa) (15958781331).jpg|thumb|left|White-rumped swallow perching]]
The white-rumped swallow measures {{convert|13|cm|in|sigfig=2}} in length and weighs {{convert|17|-|21|g|oz}}. It has an average wingspan of {{convert|115.7|mm|in}}. It has a white supraloral streak,<ref name="hbw"/> a white streak above its eye, and black [[lores]] and [[ear covert]]s. The lores and ear-coverts have a blue-green gloss. It has black wings, with white tips on its inner secondaries, tertials, and greater wing-coverts. The white tips erode with age. The tail is black and has a shallow fork. The white-rumped swallow also has, as the name implies, a white rump. The rump is not totally white; it has some fine shaft streaks. The rest of the upperparts, in addition to the crown, nape, and forehead, are a glossy blue. These features, when this bird is not breeding, are more greenish-blue. The underparts and underwing-coverts are white.<ref name="hbw"/> The bill, legs, and feet are black, and the [[Iris (anatomy)|irides]] are brown. The sexes are alike, and the juvenile can be distinguished by its dusky breast and the fact that it is duller and more brownish.<ref name="hbw"/><ref name="Turner"/>

This swallow is similar to the Chilean swallow, but can be differentiated by the lack of a supraloral white streak on the Chilean swallow.<ref name="hbw"/> The Chilean swallow also seems to keep its glossy blue upperparts when not breeding.<ref name="RidgelyGuy1989">{{cite book|last1=Ridgely|first1=Robert S.|last2=Guy|first2=Tudor|title=The Birds of South America: Volume 1: The Oscine Passerines|url=https://books.google.com/books?id=tRBb15pk4w0C|year=1989|publisher=University of Texas Press|isbn=978-0-292-70756-6|page=55}}</ref> The white-rumped swallow is, in addition, larger than the Chilean swallow.<ref name="Turner"/>

The song of the white-rumped swallow is often described as a soft gurgling<ref name="hbw"/> or a broken warble. It usually sings while flying at dawn.<ref name="Turner"/> The call is described as a quick and toneless ''zzt''.<ref name="Perlo2009">{{cite book|last=Perlo|first=Ber van|title=A Field Guide to the Birds of Brazil|url=https://books.google.com/books?id=jOjH_XSeIyAC|date=October 9, 2009|publisher=Oxford University Press|isbn=978-0-19-974565-4}}</ref> The alarm note it uses is short and harsh.<ref name="Turner"/>

==Distribution==

This swallow is native to Argentina, Bolivia, Brazil, Paraguay, Peru, and Uruguay.<ref name="IUCN"/> It inhabits open and semi-open country near water, the edge of woodland, and human settlements.<ref name="hbw"/> It also occurs in dry [[savanna]]s, degraded former forest, and both subtropical and tropical seasonally flooded grassland.<ref name="IUCN"/> It is additionally known to occur in the [[pampas]] of Argentina and Uruguay. During the [[austral winter]], the birds in the southern population usually move to the northern parts of its range.<ref name="RidgelyGuy1989"/> This bird can be found at altitudes ranging from sea level to {{convert|1100|m|ft|sigfig=2}}.<ref name="IUCN"/>

==Behaviour==

After the breeding season, the white-rumped swallow forms flocks that sometimes consist of hundreds of individuals.<ref name="hbw"/> These flocks frequently consist of both the white-rumped swallow and other species of swallows.<ref name="Turner"/>

===Breeding===
[[File:Andorinha-de-testa-branca (Tachycineta leucorrhoa) usando um ninho abandonado de forneira (Furnarius rufus).jpg|left|thumb|White-rumped swallow using the abandoned nest of [[rufous hornero]]]]
The white-rumped swallow builds nests in holes or crevices in a tree or dead snag. It also builds them in artificial structures like holes in fence posts or under [[eaves]], typically under those of abandoned buildings.<ref name="RidgelyGuy1989"/> They occasionally nest in abandoned nests of the [[firewood-gatherer]]. The nests themselves are usually made of plant fibres and lined with hair and feathers. This swallow is solitary and,<ref name="hbw"/> during the breeding season, is scattered in pairs.<ref name="RidgelyGuy1989"/> Pairs can be seen to fight and chase each other at the nest site.<ref name="Turner"/>

This swallow displays nest prospecting behaviour, visiting potential future nesting sites. Nest prospecting is a behaviour recorded in both breeding and non-breeding individuals, and occurs both after the failure or success of a nest and while the bird is actively nesting. After a nest failure, the average distance an individual travels during a prospecting visit increases dramatically, from about {{convert|121|m|ft}} to about {{convert|5119|m|ft}}. Nest prospecting seems to occur more frequently in individuals with a smaller [[Clutch (eggs)|clutch]] size. Male visits to other nests could be to care for extra-pair young, although it does not explain female visits.<ref name="WischhoffMarques-Santos2015">{{cite journal|last1=Wischhoff|first1=Uschi|last2=Marques-Santos|first2=Fernando|last3=Ardia|first3=Daniel R.|last4=Roper|first4=James J.|title=White-rumped swallows prospect while they are actively nesting|url=http://link.springer.com/article/10.1007%2Fs10164-015-0425-9|journal=Journal of Ethology|volume=33|issue=2|year=2015|pages=145–150|issn=0289-0771|doi=10.1007/s10164-015-0425-9}}</ref> Extra-pair young, or young with parents outside of the breeding pair, account for about 56 percent of all offspring.<ref name="FerrettiMassoni2011">{{cite journal|last1=Ferretti|first1=V.|last2=Massoni|first2=V.|last3=Bulit|first3=F.|last4=Winkler|first4=D. W.|last5=Lovette|first5=I. J.|title=Heterozygosity and fitness benefits of extrapair mate choice in White-rumped Swallows (''Tachycineta leucorrhoa'')|url=https://www.researchgate.net/publication/227464375_Heterozygosity_and_fitness_benefits_of_extrapair_mate_choice_in_White-rumped_Swallows_Tachycineta_leucorrhoa|journal=Behavioral Ecology|volume=22|issue=6|year=2011|pages=1178–1186|issn=1045-2249|doi=10.1093/beheco/arr103}}</ref>

The breeding season of the white-rumped swallow is from October to December in Brazil, and from October to February in Argentina.<ref name="hbw"/> During this period, one brood is usually laid, although it will occasionally lay a second brood.<ref name="MassoniBulit2006">{{cite journal|last1=Massoni|first1=Viviana|last2=Bulit|first2=Florencia|last3=Reboreda|first3=Juan Carlos|title=Breeding biology of the White-rumped Swallow ''Tachycineta leucorrhoa'' in Buenos Aires Province, Argentina|journal=Ibis|volume=149|issue=1|year=2006|pages=10–17|issn=0019-1019|doi=10.1111/j.1474-919X.2006.00589.x}}</ref> On average, 58 percent of nests will fledge at least one chick.<ref name="hbw"/>

The clutch is usually four to seven eggs that transition from pinkish-white when laid to pure white. The eggs measure {{convert|19.6|x|13.7|mm|in}} and weigh {{convert|1.9|g|oz}} on average.<ref name="Turner"/> Clutch size and egg size are noted to usually decrease as the breeding season progresses. Late-season nestlings also weigh less than early-season nestlings.<ref name="MassoniBulit2006"/> It takes 15 to 16 days for the female to incubate the clutch.<ref name="hbw"/> About 58 percent of the broods hatch synchronously, although the hatching sometimes lasts over four days.<ref name="MassoniBulit2006"/> On average, 78 percent of the eggs will hatch. The fledging period is 21 to 25 days, with about 95 percent of the nestlings fledging.<ref name="hbw"/> The white-rumped swallow, on average, lives for 2.12 years. The male lives slightly longer than the female.<ref name="BulitMassoni2011">{{cite journal|last1=Bulit|first1=Florencia|last2=Massoni|first2=Viviana|title=Apparent survival and return rate of breeders in the southern temperate White-rumped Swallow Tachycineta leucorrhoa|journal=Ibis|volume=153|issue=1|year=2011|pages=190–194|issn=0019-1019|doi=10.1111/j.1474-919X.2010.01079.x}}</ref>

===Diet===
[[File:Andorinha-de-testa-branca (Tachycineta leucorrhoa).jpg|thumb|Juveniles being fed by adult in flight]]
The white-rumped swallow is an aerial insectivore that usually feeds alone or in small groups, feeding on flies, beetles, flying ants, [[Orthoptera]], and [[Lepidoptera]]. It usually feeds close over water, pastures, and open woodland. Occasionally skimming the ground, its flight is fast and direct. It follows [[human]]s and other animals,<ref name="hbw"/> and can usually be seen near humans and animals that are disturbing insects.<ref name="Turner"/>

==Parasites==

The [[shiny cowbird]] is a [[brood parasite]] that occasionally lays its eggs in the nest of white-rumped swallow. After a shiny cowbird fledges, it exhibits behaviour that causes it to be fed more, much to the detriment of white-rumped swallow nestlings. About six percent of nests are affected by this.<ref name="MassoniWinkler2006">{{cite journal|last1=Massoni|first1=Viviana|last2=Winkler|first2=David W.|last3=Reboreda|first3=Juan C.|title=Brood parasitism of White-rumped Swallows by Shiny Cowbirds|journal=Journal of Field Ornithology|volume=77|issue=1|year=2006|pages=80–84|issn=0273-8570|doi=10.1111/j.1557-9263.2006.00027.x}}</ref> This swallow has been known to lose nests to the southern house wren, a subspecies of the [[house wren]].<ref name="hbw"/>

==Conservation status==

The white-rumped swallow is classified as a [[least-concern species]] by the IUCN. This is due to its large range, estimated to be {{convert|5580000|km2|mi2}}, its increase in population, and its large population.<ref name="IUCN"/> Increase in the availability of artificial nest sites may benefit this bird,<ref name="hbw"/> and could be a factor in its increasing population.<ref name="IUCN"/> It is described as being fairly common in its range.<ref name="hbw"/>

==References==
{{reflist|30em}}

{{Hirundinidae}}

{{DEFAULTSORT:swallow, white-rumped}}
[[Category:Tachycineta|white-rumped swallow]]
[[Category:Birds of Argentina]]
[[Category:Birds of Bolivia]]
[[Category:Birds of Brazil]]
[[Category:Birds of Paraguay]]
[[Category:Birds of Uruguay]]
[[Category:Birds described in 1817|white-rumped swallow]]