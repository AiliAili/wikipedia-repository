'''Jorge de Juan García''' (born 6 June 1961) is a Spanish film and theatre actor, producer and director, known artistically as '''Jorge de Juan'''.

==Biography==

===Early life and education===

Jorge de Juan was born into a very artistic family in [[Cartagena (Spain)|Cartagena]], Spain, in 1961. At the age of 17 he decided to be an actor and entered the Royal School of Dramatic Art of Madrid in 1978, graduating three years later.{{citation needed|date=June 2015}}

In 1979 he made his full debut, when he was still at the school, in ''Five hours with Mario'', directed by [[Josefina Molina]] in the Theatre Marquina, Madrid.<ref>{{cite news |date=28 November 1979 |title=Prestigio |url=http://elpais.com/diario/1979/11/28/cultura/312591614_850215.html |language=es |newspaper=El País}}</ref><ref>{{cite web |url=http://madridteatro.net/index.php?option=com_content&view=article&id=2812:cinco-horas-con-mario-1979-resena-&catid=235:crit2012&Itemid=213 |title=Cinco Horas con Mario. 1979. Reseña |date=18 September 2012 |language=es |website=Madrid Teatro}}</ref> Then Miguel Narros cast him in ''[[Macbeth]]'' at the [[Teatro Español de Madrid]].<ref>{{cite news |date=30 October 1980 |title=Un producto híbrido |url=http://elpais.com/diario/1980/10/30/cultura/341708414_850215.html |language=es |newspaper=El País}}</ref><ref>{{cite news |date=30 October 1980 |title=''Macbeth'', al fin un espectáculo para el Español |url=http://hemeroteca.abc.es/nav/Navigate.exe/hemeroteca/madrid/abc/1980/10/30/073.html |format=PDF |language=es |work=ABC (Madrid)}}</ref>

In 1981 Jorge de Juan moved to London where he studied direction and production at the British Theatre Association. Michael McCallion was his voice teacher; he had classes with Clifford Williams and David Perry. Around the same time, he assisted [[Ian McKellen]] in directing ''[[Acting Shakespeare]]'', at Teatro Español de Madrid.{{citation needed|date=June 2015}}

===Career===
On his return to Spain in 1982, [[Jaime Chávarri]], cast him in what would be his debut film, ''[[Bicycles Are for the Summer]]',<ref>http://www.cervantesvirtual.com/portal/alece/pcuartonivel.jsp?conten=ficha&ficha=pelicula&nomportal=alece&id=500</ref> and at the same time he was called by [[José Luis Gómez]] to appear in the very successful ''Oedipus Rex'', directed by Stavros Doufexis.<ref>{{cite web |url=http://biblioteca.ucm.es/tesis/19911996/H/3/AH3000903.pdf |title=Puesta en escena y recepcion del teatro clasico y medieval en Espana (desde 1939 a nuestros dias) |date=1992 |format=PDF |language=es |website= }}</ref>

Since then, Jorge de Juan has combined his work as an actor and producer in both theatre and film, and in the past decade as a director,too. He produced and performed in ''[[Kiss of the Spider Woman (play)|Kiss of the Spider Woman]]'', directed by Felipe Vega.<ref>{{cite news |date=3 July 1996 |title=El beso de la mujer araña |url=http://elpais.com/diario/1996/07/03/radiotv/836344824_850215.html |language=es |newspaper=El País}}</ref>

From 1998, the success of the play ''The Woman in Black'' led him to perform it more than 400 times with [[Emilio Gutiérrez Caba]].<ref>http://elpais.com/diario/1999/05/17/cvalenciana/926968701_850215.html</ref> He produced the same play, in 2007, again appearing with Caba, and directed by Eduardo Bazo.<ref>http://www.europapress.es/cultura/noticia-emilio-gutierrez-caba-jorge-juan-vuelven-siete-anos-despues-teatro-infanta-isabel-mujer-negro-20070411130451.html</ref> They repeated their former success and reached a thousand shows over the two productions.

In 2009, he produced, co-directed and starred in the play ''The 39 Steps'', in his own version of Patrick Barlow's adaptation of the Hitchcock classic.<ref>{{cite news |date=25 August 2012 |title=''Los 39 escalones'', la obra maestra de Hitchcock, pasa de la gran pantalla al escenario con más de 130 personajes |url=http://www.europapress.es/cultura/exposiciones-00131/noticia-39-escalones-obra-maestra-hitchcock-pasa-gran-pantalla-escenario-mas-130-personajes-20120825131741.html |language=es |website=Europa Press}}</ref> In 2012 he starred in ''Hay que Deshacer la Casa'',<ref>{{cite news |date=16 January 2012 |title=Luis Fernando Alvés y Jorge de Juan descubren que 'Hay que deshacer la casa' |url=http://www.publico.es/actualidad/luis-fernando-alves-y-jorge.html |language=es |website=Publico}}</ref> in which Andoni Ferreño played the lead. And he starred in, produced and directed the second run of his ''The 39 Steps''.

As a film and television actor his credits include a total of 36 films and TV series, with roles in ''El Mejor de los Tiempos'',<ref>http://elpais.com/diario/1990/03/18/cultura/637714813_850215.html</ref> ''Nadie como tú'',<ref>http://ocio.diariodemallorca.es/cine/pelicula/nadie-como-tu-flm12935.html</ref> ''Manos de Seda'',<ref>http://www.elcultural.com/revista/cine/Temia-no-saber-transmitir-emociones/13586</ref> ''Aquitania'',<ref>{{cite web |url=http://www.fotogramas.es/Peliculas/Aquitania |title=Aquitania |language=es |website=Fotogramas}}</ref> ''Otra Ciudad'',<ref>http://www.elcultural.com/revista/cine/La-sombra-de-Cain-de-Paco-Lucio/14431</ref> ''La sombra de Caín'',<ref>http://www.rebelion.org/noticia.php?id=19335</ref> and ''Imaginario'',<ref>http://www.elmundo.es/elmundo/2008/04/14/castillayleon/1208192130.html</ref> amongst others. Then in Spanish or English, there have been ''[[Open Your Eyes (1997 film)|Open your Eyes]]'',<ref>http://elpais.com/diario/1998/01/09/cultura/884300409_850215.html</ref> ''[[One of the Hollywood Ten]]'',<ref>https://www.nytimes.com/movies/movie/227073/One-of-the-Hollywood-Ten/overview</ref> ''[[Talk of Angels]]'',<ref>http://variety.com/1998/film/reviews/talk-of-angels-1117487819/</ref> ''[[Beltenebros]]'',<ref>http://elpais.com/diario/1991/01/28/cultura/665017202_850215.html*</ref> ''Las Razones de mis Amigos'',<ref>http://decine21.com/peliculas/Las-razones-de-mis-amigos-2586</ref> and ''[[Bicycles Are for the Summer]]'',<ref>http://www.uhu.es/cine.educacion/cineyeducacion/historia_guerracivil_bicicletas.htm</ref> etc.
 
Jorge de Juan was awarded the Francisco Rabal Film award for his starring role in ''El Mejor de los Tiempos'' by Vega, (winner of the [[San Sebastián International Film Festival]]). Furthermore, he received the Billboard Turia Theatre Award in 1998 for his performance in ''The Woman in Black'.

One of his most significant achievements was being the director of El Palenque in the [[Seville Expo '92]]<ref>http://hemeroteca.sevilla.abc.es/nav/Navigate.exe/hemeroteca/sevilla/abc.sevilla/1992/07/22/048.html</ref> For three and a half years he coordinated the construction and design of the {{convert|10,000|m2|sqft}} space, its programming, its staffing and management, the booking of shows. At the same time, he developed a specific plan of operations for the Expo period. Under his direction over a period of six months there were 1,162 performances, as well as official events for the 110 countries involved, their government agencies and businesses. He was personally responsible for receiving kings, heads of states, prime ministers, ministers and other personalities.

As a result of all this experience he created El Palenque Productions SL in Valencia with the aim of developing film and theatre projects. Their first jobs were the short film ''M de Amor''<ref>{{cite news |date=28 March 2000 |title='M de amor', un 'thriller' rodado en Valencia, se presenta hoy |url=http://elpais.com/diario/2000/03/28/cvalenciana/954271105_850215.html |language=es |newspaper=El País}}</ref> and the production of the ''Miradas'' series: 48 30-minute episodes on different topics to do with the Valencian community, commissioned by the Government of Valencia.

He wrote and produced the feature film ''Bala Perdida'' starring [[David Carradine]] and [[Juanjo Puigcorbé]]. It was awarded Best Feature Film and Best Soundtrack at the Mostra de Valencia Cinema.<ref>{{cite news |date=24 October 2003 |title='Bala perdida' y 'De colores' vencen en la sección dedicada al cine valenciano |url=http://elpais.com/diario/2003/10/24/cvalenciana/1067023104_850215.html |language=es |newspaper=El País}}</ref> Furthermore, he co-produced the feature film ''Imaginario'' with Dexiderius Productions. He was also associate producer on the film ''Kordon''<ref>{{cite news |date=8 September 2003 |title='Cordon' Takes Ribbon at Montreal |url=http://www.boston.com/ae/movies/articles/2003/09/08/cordon_takes_ribbon_at_montreal/ |newspaper=Boston Globe |agency=Reuters}}</ref> by Serbian director [[Goran Marković (film director)|Goran Markovic]] ([[Montreal International Film Festival]], The New Directors Award at the Montpellier Festival, Special Prize Festival Jury Orense).

More recently he co-directed and produced the theatre hit ''[[End of the Rainbow]]'' by [[Peter Quilter]] in the Marquina theatre in Madrid.<ref>http://www.gentedigital.es/portada/noticia/483571/al-final-del-arco-iris-retrata-con-maestria-la-degradacion-de-judy-garland/</ref> Next he was the executive producer and co-director of ''Dracula.''<ref>{{cite news |date=22 September 2011 |title='Drácula', de Bram Stoker, dirigida por Eduardo Bazo y Jorge de Juan llega este jueves al Lope de Vega |url=http://www.europapress.es/andalucia/sevilla-00357/noticia-dracula-bram-stoker-dirigida-eduardo-bazo-jorge-juan-llega-jueves-lope-vega-20110922055840.html |language=es |website=Europa Press}}</ref>

He is a former member of the Board of Directors of AISGE (Management Society for Actors and Performers' Rights) and a former patron of the Foundation AISGE. He is a member of the [[Academia de las Artes y las Ciencias Cinematográficas de España|Academy of Arts and Cinematographic Sciences of Spain]]. On two occasions, he was a member of the Expert Committee of the Ministry of Culture.{{citation needed|date=June 2015}}

He now lives in London, where he is the artistic director of Spanish Theatre Company.<ref>{{cite web |url=http://www.britesmag.com/article/spanish-theatre-company-dream-come-true |title=The Spanish Theatre Company: A Dream Come True |date=1 April 2015 |website=Brit Es Magazine}}</ref>

==Films==
{{BLP unsourced section|date=June 2015}}

===Lead===

* ''[[Mientras Haya Luz]]'' (Dir. Felipe Vega). CIGA prize, Best Film Prize, in the [[San Sebastián International Film Festival]]. Best Film Prize in the Festival de Alcalá de Henares. 1986.
* ''[[El Mejor de los Tiempos]]'' (Dir. Felipe Vega). Zabaltegui Prize in the [[San Sebastián International Film Festival]]. Best Film Prize in los Festivales de Alcalá de Henares y Murcia. 1989.
* ''[[El Anónimo]]'' (Dir. Alfonso Arandia). 1989.
* ''[[No Me Ccompliques la Vida]]'' (Dir. Ernesto del Río). 1990.
* ''[[Nadie Como Tú (film)|Nadie Como Tú]]'' (Dir. Criso Renovell). 1996.
* ''[[Manos de Seda]]'' (Dir. Cesar Martínez). 1997.	
* ''[[La Sombra de Cain]]'' (Dir. Paco Lucio). 1998. 	
* ''[[Aquitania (film)|Aquitania]]'' (Dir. Rafa Montesinos). 2004.	
* ''[[Imaginario]]'' (Dir. Pablo Cantos). 2008.

===Supporting===

* ''[[Bicycles Are for the Summer]]'' (''Las bicicletas son para el verano'') (Dir. [[Jaime Chávarri]]). 1983.
* ''[[La reina del mate]]'' (Dir. Fermín Cabal). 1984.
* ''[[Prince of Shadows]]'' (''Beltenebros'') (Dir. [[Pilar Miró]]). 1990.
* ''[[The Worst Years of Our Lives]]'' (''Los peores años de nuestra vida'') (Dir. [[Emilio Martínez Lázaro]]). 1993.
* ''[[Talk of Angels]]'' (''Pasiones rotas'') (Dir. Nick Hamm). 1994.
* ''[[Puede ser divertido]]'' (Dir. Azucena Rodríguez). 1995.
* ''[[Brujas (film)|Brujas]]'' (Dir. Alvaro Fernández Armero). 1995.
* ''[[Open Your Eyes (1997 film)|Open Your Eyes]]'' (''Abre los Ojos'') (Dir. [[Alejandro Amenábar]]). 1997.
* ''[[Las razones de mis amigos]]'' (Dir. Gerardo Herrero). 1999.
* ''[[Punto de mira (film)|One of the Hollywood Ten]]'' (''Punto de mira'') (Dir. Karl Francis). 2000.
* ''[[Juego de Luna]]'' (Dir. Mónica Laguna). 2000.
* ''[[Una ciudad de otro mundo]]'' (Dir. Daniel Múgica). 2001.	
* ''[[Saharaui, historias de una guerra]]'' (Dir. Pedro Rosado). 2003.
* ''[[Arena en los Bolsillos (film)|Arena en los Bolsillos]]'' (Dir. César Martínez). 2005.	
* ''[[El sindrome de Svensson]]'' (Dir. Kepa Sojo). 2005.
* ''[[Sangre en la nieve (2011 film)|Sangre en la nieve]]'' (Dir. [[Gerardo Herrero]]). 2011.

==Television==
{{BLP unsourced section|date=June 2015}}

===Lead===

* ''Los caprichos'' (Jaime Chávarri). For TVE. 1984.
* ''[[El número marcado]]'' (Juan Manuel Chumilla) Elías Querejeta for TVE. 1987.
* ''[[Passover (television)|Passover]]'' (Jamil Dehiavi). Film for the BBC. (As actor and Executive Producer). 1993.
* ''Hospital'' (Joan Guitar). Serie de Antena 3. 1996.	
* ''[[El beso de la mujer araña (television)|El beso de la mujer araña]]'' (Pedro Amalio López) Recording of the play for TVE. 1996.
* ''[[Otra ciudad]]'' (Best TV Movie of the Year Prize from la Academia de Televisión and from los Premios Tirant de Valencia). 2005.
* ''[[Para que nadie olvide tu nombre]]'' (César Martínez). TV Movie for Canal NOU, TV3 and FORTA. 2005.

===Supporting===

* ''[[Richard III (play)|Richard III]]'' Recording of the play for TVE. 1983.
* ''[[Página de sucesos]]'' (Dir. Antonio Gímenez Rico). For TVE. 1984.
* ''[[Turno de oficio]]'' (Dir. Antonio Mercero). TVE. 1985.
* ''[[Un chupete para ella]]'' Pedro Masó for A3. 2000-01. 		
* ''[[El síndrome de Ulises]]'' Series of TV fiction for A3

==Theatre==
{{BLP unsourced section|date=June 2015}}

===Lead===

* ''[[Caligula (play)|Caligula]]'' by Camus. (Dir. A. Mantovani). 1979.
* ''Yvonne, Princesa de Borgoña'' by Gombrowich. (Dir. Jorge Eines). 1981.
* ''[[La ciudad y los perros (play)|La ciudad y los perros]]'' by Vargas Llosa. (Dir. Edgar Saba).1982.
* ''[[Medea es un buen chico]]'' by Luis Riaza. (Dir. Luis Vera). 1984.	
* ''[[Blood Wedding]]'' by F. G. Lorca (Dir. José Luis Gómez), Tour of South America. Jerusalem y Edinburgh festivals. 1985/7.
* ''[[Motor (play)|Motor]]'' by Alvaro del Amo. (Dir. Guillermo Heras). BERCEUSE by Luis de Pablo. Festival de Otoño (Dr. Piere Audi, Dr. del Teatro Almeida de Londres). 1988.-
* ''[[Caricias (play)|Caricias]]'' de Sergi Belbel. C.N.N.T.E. (Dir. Guillermo Heras). 1994.- 	
* ''[[El beso de la mujer araña (play)|El beso de la mujer araña]]'' by Manuel Puig. (As actor y producer). 1995/6.
* ''[[La Mujer de Negro]]'' by Susan Hill and Stephen Mallatratt. (Dir. Rafael Calayatud). 1998/00.
* ''[[La Mujer de Negro]]'' by Susan Hill and Stephen Mallatratt. (Dir. Eduardo Bazo). 2006/08. Produced by his own producer. 
* ''[[The 39 Steps (play)|The 39 Steps]]'' (Alfred Hitchcock) by Patrick Barlow adapted by Jorge de Juan. (Dir. Eduardo Bazo y Jorge de Juan). Produced by his own producer.
* ''Hay que deshacer la casa'' de Sebastián Junyent (Dir. Andoni Ferreño). 2011/12.	
* ''[[The 39 Steps (play)|The 39 Steps]]'' (Alfred Hitchcock) by Patrick Barlow adapted by Jorge de Juan. New version (Dir. Jorge de Juan y Eduardo Bazo). 2012/13.
* ''[[Hay que deshacer la casa]]'' de Sebastián Junyent (Dir. Andoni Ferreño). New version together with Andoni Ferreño. 2013.

===Supporting===

* ''[[Cinco horas con Mario]]'' by Miguel Delibes. (Dir. Josefina Molina). 1979.
* ''[[Anexo:Obras representadas en el Teatro Español|Macbeth]]'' by Shakespeare. (Dir.Miguel Narros). 1980.
* ''[[Anexo:Obras representadas en el Festival de Mérida|Los carboneros]]'' by Aristophanes. (Dir. Roberto Villanueva, as actor and assistant director). 1981.
* ''[[Oedipus Rex]]'' by Sophocles. (Dir. Stavros Doufenixis). Tour of Spain. 1982.
* ''[[Richard III (play)|Richard III]]'' by Shakespeare (Dir. Clifford Williams). 1983.
* ''En el corazón del teatro'', Work about Hamlet. (Dir. Guillermo Heras). 1983.
* ''[[Anexo:Obras representadas en el Teatro Español|Absalon]]'' by Calderón. (Dir. José Luis Gómez). 1983.

==Director and Producer==

* Works on numerous publicity shoots as assistant director, and on production, and as a casting director for numerous commercial brands.
* Founds 2Cs in Madrid, company, with seat in London, dedicated to multimedia production, which produces and films company videos and presentations. 1989.
* Director of El Palenque in the Seville Expo '92. 1990/92.
* ''M de amor'' Short film (Directed by Pau Martínez). 2001. Produced by his El Palenque Producciones S.L., en Valencia. 2001
* ''[[Bala Perdida]]'' Producer and screenwriter (Directed by Pau Martínez and starring David Carradine and Juanjo Puigcorbé. Prize for the Best Feature Film and Best Soundtrack in La Mostra Cinema de Valencia). 2002
* ''[[Festival Internacional de Cine de Montreal|Kordon]]'' Associate producer of the feature film by Serbian director Goran Markovic, Great Prize Montreal Festival, Prize for the Best Feature Film, Jury's Special Prize Orense Festival. 2003		
* ''[[Ricardo Muñoz Suay memorias del otro]]'' Directs and produces the documentary. 2009
* ''[[Al final del arcoíris]]'' by Peter Quilter. Co-directs, with Eduardo Bazo, and produces, for the Teatro Marquina. 2011
* ''[[Dracula (1924 play)|Dracula]]'' by H.Deane and J.Balderston in a version of Jorge de Juan. Co-directs, with Eduardo Bazo, and works as Executive Producer, for the Teatro Marquina. 2012
* ''[[The 39 Steps (play)|The 39 Steps]]''. Directs the new version of (Alfred Hitchcock) by Patrick Barlow adapted by Jorge de Juan. Produced by his own company. 2012

==Awards==
{{BLP unsourced section|date=June 2015}}
''Semana de cine español de Murcia''
{| class="wikitable"
|-
! Year !! Category !! Film !! Result
|-
| '''1990''' || '''Best Actor''' || '''El Mejor de los Tiempos'''|| '''Winner'''

|}

==References==
{{reflist|2}}

==External links==
*{{IMDb name|0209056|Jorge de Juan}}
* Jorge de Juan in [http://www.laopiniondemurcia.es/cultura-sociedad/2012/01/29/carne-escenario-actuando-o-dirigiendo/381749.html?ref_=fn_al_nm_3 La Opinion De Murcia]
* Jorge de Juan in [http://ccaa.elpais.com/ccaa/2012/09/03/madrid/1346693883_852474.html?ref_=fn_al_nm_4 El Pais]

{{DEFAULTSORT:Juan, Jorge de}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Spanish actors]]
[[Category:Spanish directors]]
[[Category:1961 births]]
[[Category:Living people]]