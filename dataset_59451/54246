{{refimprove|date=January 2008}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place 
| type     = suburb
| name     = Glenside
| city     = Adelaide
| state    = sa
| image    = 
| caption  = 
| lga      = City of Burnside
| postcode = 5065
| pop      = 2,191 
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref>{{Census 2006 AUS|id=SSC41641|name=Glenside (State Suburb)|quick=on|accessdate=7 June 2008}}</ref>
| est      = 1860
| area     = 1.40
| stategov = 
| fedgov   = 
| near-nw  = ''[[Adelaide Parklands]]''
| near-n   = [[Dulwich, South Australia|Dulwich]] & [[Toorak Gardens, South Australia|Toorak Gardens]]
| near-ne  = [[Tusmore, South Australia|Tusmore]]
| near-e   = [[Linden Park, South Australia|Linden Park]]
| near-se  = [[St. Georges, South Australia|St. Georges]]
| near-s   = [[Glenunga, South Australia|Glenunga]]
| near-sw  = [[Fullarton, South Australia|Fullarton]]
| near-w   = [[Eastwood, South Australia|Eastwood]]
| dist1    = 2
| location1= Adelaide city centre
}}
'''Glenside''' is a [[suburb]] in the [[City of Burnside]], [[Adelaide]], [[South Australia]], around 2 kilometres south-east of the [[Adelaide city centre]], home to 2,985 people in a total land area of 1.40&nbsp;km².

It is bordered on the north by [[Greenhill Road, Adelaide|Greenhill Road]], on the east by [[Portrush Road, Adelaide|Portrush Road]], on the south by Flemington Street and Windsor Road and the west by [[Fullarton Road]]. The suburb has a rectangular layout. A number of residential streets in the suburb contain [[Avenue (landscape)|avenues]] of [[jacaranda|jacaranda trees]], which provide lush colour when they flower in Spring.

== History ==
Glenside, along with its neighbouring suburb of [[Glenunga, South Australia|Glenunga]] were originally known by the name of 'Knoxville'. They were first settled in the 1840s as farming land, and wheat grown in the area was awarded first prize in the Royal Adelaide Show. The area now taken up by Glenunga International High School and Webb Oval, was previously home to slaughterhouses established in the nineteenth century. At one point, the slaughterhouses were exporting overseas and at the same time providing half of Adelaide's lamb requirements.

A number of coach companies, notably [[Cobb & Co]] and those of [[William Rounsevell]], and [[John Hill (businessman)|John Hill]] were set up in the 1870s and 1880s. Up to 1000 horses grazed the land. At this point, most of the streets were beginning to be named. Most were named by the inhabitants at the time, usually in reference to their original homes in Ireland, England, Scotland, Wales and the United States. However, one street was named after an Aboriginal Word - "Allinga", meaning sun.

In the early twentieth century, a number of businesses started locating themselves in Glenside. The Australian icon, the Hills Hoist clothes line, was invented by the Hill family in neighbouring Glenunga. Other notable businesses were the Symons & Symons glass merchants and one involved in "Bland Radios".

In September 2007, following a review of mental health services in South Australia by Social Inclusion Commissioner Monsignor David Cappo, Premier Mike Rann announced a major redevelopment of the [[Glenside Hospital (Adelaide)|Glenside hospital]] site. The redevelopment included a new 129 bed specialist psychiatric hospital that was opened on 2012.<ref>{{Cite news |publisher=Government of South Australia |title=Press release: New Era in Mental Health |date=20 September 2007}}</ref> The Victorian-era hospital buildings were also refurbished as part of the building of the new Adelaide Studios of the South Australian Film Corporation opened by Mr Rann on October 20, 2011.<ref>Advertiser 21 October 2011</ref>

== Geography and built environs ==
{{unreferenced section|date=June 2016}}

The suburb is home to the mental health campus of the [[Royal Adelaide Hospital]] previously the home of [[Glenside Hospital (Adelaide)|Glenside Hospital]], a primary mental health facility in the state. Originally established in March 1870 as Parkside Lunatic Asylum, it once occupied approximately one-third of the area of the suburb. As much of the open grounds and some buildings were surplus to needs due to a relocation of residents to community housing or other facilities, areas were gradually sold for other purposes. Twelve hospital structures were heritage-listed including Z-Ward, a freestanding fortress-like building which once housed the criminally insane. A modern mental health facility has been built in the southern portion of the grounds and the public are encouraged to walk through the hospital grounds to create a sense of community for those receiving treatment.

The [[South Australian Film Corporation]] has since taken over a redeveloped portion of the former hospital property as its headquarters following a move from the former studio at [[Hendon, South Australia|Hendon]] in the north-western suburbs of Adelaide. The new studio includes office accomomodation and facilities for film production.

The [[Burnside Village]] Shopping Centre is located in Glenside, on the corner of Greenhill and Portrush Roads. It is a large shopping centre and serves much of the City of Burnside council area.

Glenside also contains South Australia's only orthodox Jewish [[synagogue]]. Many of Adelaide's [[Jewish]] community live nearby in the eastern suburbs in order to walk to the synagogue on Sabbath or other holy days. Although most of the rest of Glenside is residential, there are offices related to mining, veterinary health, primary industries and health services along Flemington and Conyngham Streets and a variety of food and service businesses along Greenhill Road.

There is one park in Glenside located on the corner of Conyngham and Cator Street named Symons & Symons Reserve, after a well-known glassmaker who lived and worked in the area in the mid 1900s. It is located next to a retirement home and a childcare centre. A dog exercise park has been created in Conyngham Street. Glenunga Reserve is located in the directly adjacent suburb of [[Glenunga, South Australia|Glenunga]].

== Demography ==
{{unreferenced section|date=June 2016}}
The suburb is home to many families and retirees of predominantly [[Anglo-Celtic Australian|Anglo-Celtic]] background. Much of the suburb is quite [[upper-middle class]] and is currently undergoing residential development along Conyngham St as older larger properties are being subdivided with new houses and roads being built. Despite its upper middle class socioeconomic background, the suburb still contains quite a few [[unit (housing)|unit]]s and housing developments from the 1950s and 1960s,  many of which have been re-developed or demolished completely.

Two large retirement villages, including a bowling green, exist on Greenhill Road - Victoria Grove Village and Pineview Village, as well as The Glenbrook facility on L'Estrange Street. Amber Woods Estate is a housing development comprising rows of townhouses and courtyard homes which share park space and is located behind the retirement facilities.

Some of the orthodox Jewish community live within a short walk to the synagogue in Flemington Street.

Glenunga International High School in the adjacent suburb has created a multicultural community. International students and their families, many of them from Asia, also reside in the area.

==See also==
* [[Glenside Hospital (Adelaide)|Glenside Hospital]]
* [[List of Adelaide suburbs]]

==References==
{{Reflist}}

{{Coord|-34.943|138.635|format=dms|type:city_region:AU-SA|display=title}}
{{City of Burnside suburbs}}

[[Category:Suburbs of Adelaide]]