{{good article}}
{{Infobox television
| show_name=AJ's Infinite Summer
| image=File:AJ's Infinite Summer title card.jpg
| image_alt=300px
| caption=The words "AJ's Infinite Summer" over a sky background with clouds.
| creator=Toby Jones
| writer=Toby Jones
| creative_director=Phil Rynda
| director={{unbulleted list|Sue Mondt <small>(art)</small>|[[Robert Alvarez]] <small>(timing)</small>}}
| voices={{unbulleted list|David Hill|[[Wallace Langham]]|[[Chris Parnell]]|AJ Thompson|[[Mae Whitman]]}}
| composer=[[Secret Cities]]
| country=United States
| language=English
| channel=[[Cartoon Network]]
| executive_producer={{unbulleted list|[[Brian A. Miller]] and Jennifer Pelphrey <small>(for Cartoon Network Studios)</small>|Curtis Lelash and [[Rob Sorcher]] <small>(for Cartoon Network)</small>}}
| producer=Nate Funaro
| editor=Bobby Gibis
| company=[[Cartoon Network Studios]]
| runtime=8 {{Sfrac|2}} minutes
| released={{Start date|2014|5|16}}
}}

'''''AJ's Infinite Summer''''' is an [[animated]] [[television pilot]] created by Toby Jones for [[Cartoon Network]]. The pilot follows AJ, whose new job during the summer drives him crazy with power. It is loosely based on Jones' 2006 film, ''AJ Goes to France''. Both the film and the pilot star AJ Thompson as the eponymous character. Jones additionally sought inspiration from his hometown of Fargo, North Dakota, and the comics he drew featuring Thompson and his other friends. The pilot was released on the official website of Cartoon Network on May 16, 2014.

==Plot==
Summer vacation starts for AJ and his friends Danny and Morgan, who live in the city of Downer. When AJ wants to be spontaneous for his plans to spend the vacation, he impulsively decides to get a job. The trio comes across a corporate office. AJ wants to apply for the corporation that owns it, but Danny and Morgan suggest that he prepares for the interview. AJ turns to his father, Peter, for advice. He equips AJ with an oversized business suit, and afterward the trio enter the office to help AJ apply. AJ shows his resume, and immediately he is hired by the Instructor of First Impressions. Workers of the office welcome AJ, who wears the same style of business suit as they do. When he sees that each worker has their own assistant, AJ hires Morgan as his "secretary" and tells her to fetch papers. Meanwhile, he asks "towel boy" Danny to wipe the sweat off his brow.

After this demonstration of power, AJ is promoted by his manager. A montage of AJ being promoted for doing absurd tasks follows. Soon, AJ is promoted to a rank with a private office. Danny and Morgan refuse to work for AJ further, finding him corrupt with power. AJ throws them out, and afterward he is promoted to CEO by a former officer—a decrepit man within a robotic business suit. AJ floats to the top of the suit, from which he spots a beach ball-destroying machine to the side. He hallucinates the beach balls as the heads of Morgan and Danny, who say that he has destroyed the spirit of summer. Realizing his mistake, AJ rejects the promotion. He returns to his friends outside, and together they plan the rest of their summer.

==Production==
''AJ's Infinite Summer'' was created by Toby Jones. Produced by Nate Funaro at [[Cartoon Network Studios]], the pilot had [[Robert Alvarez]] as timing director, Sue Mondt as art director, and Phil Rynda as creative director. AJ Thompson provided the voice for the eponymous character, [[Wallace Langham]] for Danny, [[Mae Whitman]] for Morgan, [[Chris Parnell]] for Peter, and David Hill for various characters.<ref name="Jones 2014" />

[[File:Secret Cities (2010-06-29 by Ian T. McFarland).jpg|thumb|[[Secret Cities]] provided the score of the pilot. Members of the band are friends of Jones from his hometown of Fargo.]]
Jones had previously codirected ''AJ Goes to France'', a 2006 live-action [[independent film]] that also has Thompson as the leading actor.<ref name="Amidi 2014" /> It was produced as an assignment for [[Concordia College (Moorhead, Minnesota)|Concordia College]] in Moorhead, Minnesota, where Jones majored in film. Jones had moved from his hometown of Fargo, North Dakota, to Minneapolis in 2005, after he graduated from [[Fargo South High School]]. For ''AJ's Infinite Summer'', Jones was inspired by his hometown, as well as the comics he drew that featured AJ and his other friends, Danny Davy and Morgan. Additionally, the character of Peter is based on Greg Carlson, Jones' professor from Concordia and also a film director and a critic for the ''[[High Plains Reader]]''. [[Secret Cities]], a Fargo-based band in which Jones' friends play, provided the score for the pilot. Jones found it excellent that both his friends and his friend's band were allowed by the network to do work for the pilot.<ref name="Lamb 2014" />

Jones moved to Los Angeles in 2011 to work for [[Cartoon Network]]. He has written and storyboarded for ''[[Regular Show]]'', another production on the network. His work on it gave Jones the foresight to pitch another show he knew the network would want. Initially unsure if he was ready to pitch ''AJ's Infinite Summer'', the network rejected the pilot the first time he did but approved it on the second, after he had reworked it throughout a few months. Jones contrasted the physical limitations of animating in time and energy to the animation of the pilot itself. He cited having the character of AJ run up a wall in one scene as an example of this.<ref name="Lamb 2014" />

==Release and reception==
''AJ's Infinite Summer'' was released without announcement on May 16, 2014, on the official website of Cartoon Network. ''[[Long Live the Royals (film)|Long Live the Royals]]'', another pilot, was released on the same day. This pilot was created by Sean Szeles, who has also worked on ''Regular Show''. Jason Krell of ''[[io9]]'' found that the plot for ''AJ's Infinite Summer'' was simple yet flexible. He described its take on life during summer vacation as "charming" and comparable with an "aged-up ''[[Phineas and Ferb]]''". He said that he was amazed by both and that his viewership will be granted for both, should they be picked up as full series.<ref name="Krell 2014" /> John Lamb of ''[[The Forum of Fargo-Moorhead]]'' recognized features of Fargo in the pilot, namely the high school, which he found analogous to Fargo South where Jones attended.<ref name="Lamb 2014" /> Meanwhile, Amid Amidi of ''[[Cartoon Brew]]'' expected that the pilot would not have continuity from ''AJ Goes to France''.<ref name="Amidi 2014" />

After the pilot was released, Jones found that people back in Fargo were amused to spot the differences in landmarks between their city and Downer. Jones told Lamb that since he worked in animation, he has been "surrounded by these people that I've looked up to for years as a fan, and having them tell me they enjoyed it is the greatest thing ever". {{As of|2014|06}}, Jones is still mainly working on ''Regular Show'' but said that he would like it very much to have the network commission it as a series. He expressed interest in submitting it at the Fargo Film Festival.<ref name="Lamb 2014" /> The pilot was listed in the ballot for "Outstanding Short-Format Animated Program" at the [[66th Primetime Emmy Awards]],<ref name="Television Academy 2014" />{{rp|{{^}}5}} although it did not win. The ''Long Live the Royals'' pilot did win in this category, however,<ref name="Anon. 2014" />{{rp|{{^}}7}} and the network later commissioned it as a [[Long Live the Royals|miniseries of the same name]].<ref name="Anon. 2015" />

==References==
{{Reflist|colwidth=45em|refs=
<ref name="Amidi 2014">{{Cite web
 |author=Amidi, Amid 
 |date=May 16, 2014 
 |url=http://www.cartoonbrew.com/tv/watch-2-new-cn-pilots-by-regular-show-staffers-99518.html 
 |title=Watch Two New Cartoon Network Pilots by ''Regular Show'' Staffers 
 |work=Cartoon Brew 
 |publisher=Cartoon Brew, Inc. 
 |accessdate=May 16, 2014 
 |archiveurl=http://www.webcitation.org/6PdIh05LZ?url=http%3A%2F%2Fwww.cartoonbrew.com%2Ftv%2Fwatch-2-new-cn-pilots-by-regular-show-staffers-99518.html 
 |archivedate=May 17, 2014 
 |deadurl=no 
 |df= 
}}</ref>
<ref name="Anon. 2014">{{Cite web
| url=http://www.emmys.com/sites/default/files/Downloads/2014-creative-arts-winners-v1.pdf
| title=66th Emmy Awards
| work=Television Academy
| publisher=Academy of Television Arts & Sciences
| date=August 16, 2014
| accessdate=May 3, 2015
| archiveurl=https://web.archive.org/web/20140819073654/http://www.emmys.com/sites/default/files/Downloads/2014-creative-arts-winners-v1.pdf
| archivedate=August 19, 2014
}}</ref>
<ref name="Anon. 2015">{{Cite web
| url=http://deadline.com/2015/02/cartoon-network-upfront-powerpuff-girls-adventure-time-1201376480/
| title=Cartoon Network Unveils Upfront Slate for 2015–16
| work=Deadline Hollywood
| publisher=Penske Media Corporation
| date=February 19, 2015
| accessdate=May 3, 2015
| archiveurl=https://web.archive.org/web/20150219173319/http://deadline.com/2015/02/cartoon-network-upfront-powerpuff-girls-adventure-time-1201376480/
| archivedate=February 19, 2015
}}</ref>
<ref name="Jones 2014">{{Cite AV media
| people=Jones, Toby (creator)
| date=May 14, 2014
| url=http://www.cartoonnetwork.com/tv_shows/cartoon-network-videos/video/ajs-infinite-summer-short-clip.html
| title=AJ's Infinite Summer
| medium=Animated television pilot
| publisher=Cartoon Network. Turner Broadcasting System
| archiveurl=https://web.archive.org/web/20140519035625/http://www.cartoonnetwork.com/tv_shows/cartoon-network-videos/video/ajs-infinite-summer-short-clip.html
| archivedate=May 19, 2014
}}</ref>
<ref name="Krell 2014">{{Cite web
 |author=Krell, Jason 
 |date=May 16, 2014 
 |url=http://animation.io9.com/new-pilots-from-regular-show-writers-are-fantastic-1577510569/ 
 |title=New Pilots from ''Regular Show'' Writers Are Fantastic 
 |work=io9 
 |publisher=Gawker Media 
 |accessdate=May 16, 2014 
 |archiveurl=http://www.webcitation.org/6PdIh05Lt?url=http%3A%2F%2Fanimation.io9.com%2Fnew-pilots-from-regular-show-writers-are-fantastic-1577510569 
 |archivedate=May 17, 2014 
 |deadurl=no 
 |df= 
}}</ref>
<ref name="Lamb 2014">{{Cite news
| author=Lamb, John
| date=June 1, 2014
| url=http://www.inforum.com/content/fargo-appears-cartoon-network-movie-filmmaker-draws-inspiration-real-life-friends-and
| title=Fargo Appears in Cartoon Network Movie: Filmmaker Draws Inspiration from Real-Life Friends and Hometown
| work=Forum of Fargo-Moorhead
| publisher=Forum Communications Company
| accessdate=November 14, 2014
| archiveurl=https://web.archive.org/web/20140603212931/http://www.inforum.com/content/fargo-appears-cartoon-network-movie-filmmaker-draws-inspiration-real-life-friends-and
| archivedate=June 3, 2014
}} {{subscription required}}</ref>
<ref name="Television Academy 2014">{{cite web
| author=
| date=June 5, 2014
| url=http://www.emmys.com/sites/default/files/Animation.pdf
| title=2014 Primetime Emmy Awards Ballot
| publisher=Television Academy. Academy of Television Arts & Sciences
| accessdate=November 15, 2014
| archiveurl=https://web.archive.org/web/20141115235936/http://www.emmys.com/sites/default/files/Animation.pdf
| archivedate=November 15, 2014
}}</ref>
}}

==External links==
* {{IMDb title|3744948|AJ's Infinite Summer}}

{{Cartoon Network pilots, films and specials}}

[[Category:Cartoon Network Studios series and characters]]