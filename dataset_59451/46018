[[File:Wacom Bamboo Capture tablet and pen.jpg|thumb|A Wacom Bamboo Capture graphics tablet with supplied inductive pen. The crop marks on the surface indicate the active area, which measures 14.7×9.2 cm or 5.8×3.6 in.]]
[[File:ActivePenSamples.jpg|thumb|right|N-trig Active Pens]]
An '''active pen''' (also referred to as '''active [[Stylus (computing)|stylus]]''') is an input device that includes electronic components and allows users to write directly onto the [[LCD]] screen surface of a computing device such as a [[smartphone]], [[tablet computer]] or [[Ultrabook]].<ref name=HughesThesis>{{cite web|last=Hughes|first=Andrew|title=Active Pen Input and the Android Input Framework|url=http://digitalcommons.calpoly.edu/cgi/viewcontent.cgi?article=1568&context=theses|work=M.S. thesis|publisher=California Polytech State University|year=2011|page=2}}</ref> The active pen marketplace has long been dominated by [[N-trig]] <ref name=Engadget>{{cite web|last=Cooper|first=Andrew|title=N-trig Release Teases that Fujitsu's Stylistic Q702 will Come with Active Pen Support|url=http://www.engadget.com/2012/09/24/n-trig-pen-fujitsu-stylistic-q702/|work=Engadget.com|accessdate=February 28, 2014|date=September 12, 2013}}</ref> and [[Wacom (company)|Wacom]],<ref name=Gigaom>{{cite web|last=Kendrick|first=James|title=Wacom Digitizer for Smartphones|url=http://gigaom.com/2004/09/28/wacom_digitizer/|work=GigaOm.com|accessdate=February 28, 2014|date=September 28, 2004}}</ref> but newer firms [[Atmel]]<ref name=Venturebeat>{{cite web|last=Takahashi|first=Dean|title=New Sensors Could Lower the Cost of Touchscreen Pens – and Make Them as Good as Pen and Paper|url=http://venturebeat.com/2014/01/07/new-sensors-could-lower-the-cost-of-touchscreen-pens-and-make-them-as-good-as-pen-and-paper/|work=VentureBeat.com|accessdate=February 28, 2014|date=January 7, 2014}}</ref> and [[Synaptics]]<ref name=Techreport>{{cite web|last=Gasior|first=Geoff|title=Synaptics Intros Pressure-Sensitive Stylus Tech for Win8.1|url=http://techreport.com/news/25521/synaptics-intros-pressure-sensitive-stylus-tech-for-win8-1|work=techreport.com|accessdate=February 28, 2014|date=October 17, 2013}}</ref> also offer active pen designs.
[[File:ActivePenComponents.jpg|thumb|right|Active Pen, electronic components]]
Active pens are typically used for note taking, on-screen drawing/painting and electronic document annotation, as well as accurate object selection and scrolling.<ref name=Linenberger>{{cite web|last=Linenberger|first=Michael|title=The Importance of an Active Digitizer Pen|url=http://michaellinenberger.com/blog/the-importance-of-an-active-digitizer-pen/|work=MichaelLinenberger.com|accessdate=January 6, 2014|date=October 16, 2013}}</ref>  When used in conjunction with [[handwriting recognition]] software, the active pen's handwritten input can be converted to [[digital ink|digital text]], stored in a digital document, and edited in a text or drawing application.
[[File:ActivePenSensitivity1.jpg|thumb|right|Active Pen, natural pressure sensing]]
The active pen's electronic components generate wireless signals that are picked up by a built-in [[digitizer]] and transmitted to its dedicated controller, providing data on pen location, pressure and other functionalities. Additional features enabled by the active pen's electronics include palm rejection to prevent unintended touch inputs, and hover, which allows the computer to track the pen's location when it is held near, but not touching the screen.<ref name=Bricklin>{{cite book|last=Bricklin|first=Dan|title=Bricklin on Technology|year=2009|publisher=Wiley Publishing|location=Indianapolis, IN|isbn=978-0-470-40237-5|page=283}}</ref>  Most active pens feature one or more function buttons (e.g. eraser and right-click) that can be used in place of a mouse or keyboard.
[[File:ActivePenPalmRejection.jpg|thumb|right|Active Pen, palm-on-screen natural writing experience]]
An input device known as a passive [[stylus]] can also be used to write directly onto the screen, but it does not include electronics and thus lacks all of the features that are unique for an active pen.<ref name=Linenberger /> The typical passive stylus also has a large tip made of rubber or conductive foam in order to emulate a user's finger, rather than the more accurate ballpoint pen-like tip used in the active pen.

Active pen devices support most modern [[operating systems]], including Google's [[Android (operating system)|Android]] and [[Microsoft Windows]].<ref name=Sumocat>{{cite web|title=Android Getting Native Active Pen Support in Ice Cream Sandwich|url=http://www.gottabemobile.com/2011/10/21/android-getting-native-active-pen-support-in-ice-cream-sandwich/|work=GottaBeMobile.com|publisher=Notebooks.com Inc.|accessdate=January 6, 2014|date=October 21, 2011}}</ref>

== See also ==

*[[Digital pen]]
*[[Pen computing]]
*[[Stylus (computing)]]
*[[Graphics tablet]]
*[[Tablet computer]]

== References ==
{{reflist}}

{{Pens}}

<!-- See WP:HD: Prusionf wants this to be submitted -->

[[Category:Computing input devices]]
[[Category:American inventions]]