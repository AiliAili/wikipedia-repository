{{Infobox book 
| name          = The Purple Land
| image         = <!-- prefer 1st edition cover -->
| author        = [[William Henry Hudson]] 
| country       = United Kingdom
| language      = English
| subject       = [[Uruguay]], [[Gaucho]]s
| genre         = [[Adventure novel]]
| publisher     = Sampson Low
| release_date  = 1885
| media_type    = Print ([[Hardback]] & [[Paperback]])
| pages         = 2 vol.
}}
'''''The Purple Land''''' is a novel set in 19th century [[Uruguay]] by [[William Henry Hudson]], first published in 1885 under the title ''The Purple Land that England Lost''. Initially a commercial and critical failure, it was reissued in 1904 with the full title ''The Purple Land, Being One Richard Lamb's Adventures in the Banda Orientál, in South America, as told by Himself''. Towards the end of the novel, the narrator explains the title, "I will call my book ''The Purple Land.'' For what more suitable name can one find for a country so stained with the blood of her children?"

==Plot summary==

The novel tells the story of Richard Lamb, a young Englishman who marries a teenage Argentinian girl, Paquita, without asking her father's permission, and is forced to flee to [[Montevideo, Uruguay]] with his bride. Lamb leaves his young wife with a relative while he sets off for eastern Uruguay to find work for himself. He soon becomes embroiled in adventures with the Uruguayan [[gauchos]] and romances with local women. Lamb unknowingly helps a rebel guerrilla general, Santa Coloma, escape from prison and joins his cause. However, the rebels are defeated in battle and Lamb has to flee in disguise. He helps Demetria, the daughter of an old rebel leader, escape from her persecutors and returns to Montevideo. Lamb, Paquita, Demetria and Santa Coloma evade their government pursuers by slipping away on a boat bound for Buenos Aires. Here the novel ends, but in the opening paragraphs, Lamb had already informed the reader that after the events of the story he was captured by Paquita's father and thrown into prison for three years, during which time Paquita herself died of grief.

==Literary significance & criticism==
[[Jorge Luis Borges]] dedicated an essay to ''The Purple Land'' in his book ''Other Inquisitions'' (1952). He compared Hudson's novel to the ''[[Odyssey]]'' and described it as perhaps the "best work of [[gaucho literature]]". Borges sees the novel as the story of Richard Lamb's gradual "acriollamiento" ("Creolisation"). In other words, Lamb "goes native." To begin with, Lamb looks down on the Uruguayans, with their disorganised political system and lack of law and order and civilised amenities, thinking it would have been better for Uruguay to become part of the British Empire. But he slowly comes to see the advantages of the freedom they enjoy, especially in comparison to the stuffiness of Victorian England. According to [[Ezequiel Martínez Estrada]], who is quoted by Borges, the final pages of the novel contain "the supreme justification of America compared with Western civilisation." Hudson writes:<blockquote>I cannot believe that if this country had been conquered and recolonised by England, and all that is crooked in it made straight according to our notions, my intercourse with the people would have had the wild, delightful flavour I have found in it. And if that distinctive flavour cannot be had along with the material prosperity resulting from Anglo-Saxon energy, I must breathe the wish that this land may never know such prosperity ... We do not live by bread alone, and British occupation does not give to the heart all the things for which it craves... The unwritten constitution, mightier than the written one, is in the heart of every man to make him still a republican and free with a freedom it would be hard to match anywhere else on the globe. The Bedouin himself is not so free, since he accords an almost superstitious reverence and implicit obedience to his sheikh. Here the lord of many leagues of land and of herds unnumbered sits down to talk with the hired shepherd, a poor, bare-footed fellow in his smoky rancho, and no class or caste difference divides them, no consciousness of their widely different positions chills the warm current of sympathy between two human hearts. How refreshing it is to meet with this perfect freedom of intercourse, tempered only by that innate courtesy and native grace of manner peculiar to Spanish Americans! What a change to a person coming from lands with higher and lower classes, each with its innumerable hateful subdivisions &mdash; to one who aspires not to mingle with the class above him, yet who shudders at the slouching carriage and abject demeanour of the class beneath him If this absolute equality is inconsistent with perfect political order, I for one should grieve to see such order established.</blockquote>[[Ernest Hemingway]] famously referred to Hudson's book in his novel [[The Sun Also Rises]]:<blockquote>Then there was another thing. He had been reading W.H. Hudson. That sounds like an innocent occupation, but Cohn had read and reread ''The Purple Land.'' ''The Purple Land'' is a very sinister book if read too late in life. It recounts splendid imaginary amorous adventures of a perfect English gentleman in an intensely romantic land, the scenery of which is very well described. For a man to take it at thirty-four as a guide-book to what life holds is about as safe as it would be for a man of the same age to enter Wall Street direct from a French convent, equipped with a complete set of the more practical [[Horatio Alger|Alger]] books.</blockquote>

==Radio play==
An adaption for radio was written by [[Andrew Davies (writer)|Andrew Davies]].<ref>{{cite web |url= http://www.kenilworthweeklynews.co.uk/news/local/get_set_for_more_andrew_davies_drama_1_2401063 |title=Get set for more Andrew Davies drama |publisher=Kenilworth Weekly News |date=11 February 2011 |accessdate=2 July 2011}}</ref> ''The Purple Land'' was broadcast on 16 July 2011 as [[BBC Radio 4]]'s Saturday play, starring [[David Tennant]] as Richard Lamb.<ref>{{cite web |url= http://www.bbc.co.uk/pressoffice/proginfo/radio/2011/wk29/sat.shtml |title=Network Radio Programme Information BBC Week 29 |work=BBC Press Office |accessdate=2 July 2011}}</ref>

==References==
*W.H. Hudson, ''The Purple Land'', edited by Ilan Stavans, The University of Wisconsin Press (2002)
*Jorge Luis Borges, "Sobre ''The Purple Land''", in ''Otras Inquisiciones'' (first published 1952)
*Ernest Hemingway, ''The Sun Also Rises'' (first published 1926)
{{Reflist}}

{{DEFAULTSORT:Purple Land}}
[[Category:1885 novels]]
[[Category:Debut novels]]
[[Category:Adventure novels]]
[[Category:Novels set in Uruguay]]