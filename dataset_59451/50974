{{About|the journal|the discipline|Communication theory}}
{{Infobox journal
| title = Communication Theory
| cover = [[File:Communication_Theory.png|195px|alt=Cover]]
| discipline = [[Communication theory]]
| abbreviation = 
| editor = Thomas Hanitzsch
| publisher = [[Wiley-Blackwell|Wiley]] on behalf of the [[International Communication Association]]
| country = [[United States]]
| frequency = Quarterly
| history = 1991–present
| impact = 1.667
| impact-year = 2014
| website = http://www.wiley.com/WileyCDA/WileyTitle/productCd-COMT.html
| link1 = http://www.onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-2885/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291468-2885/issues
| link2-name = Online archive
| ISSN = 1050-3293
| eISSN = 1468-2885
| OCLC = 21463248
| LCCN = 
}}
'''''Communication Theory''''' is a quarterly [[peer review|peer-reviewed]] [[academic journal]] publishing research articles, theoretical essays, and reviews on topics of broad theoretical interest from across the range of [[communication studies]]. It was established in 1991 and the current [[editor-in-chief]] is Thomas Hanitzsch ([[University of Munich]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.667, ranking it 13th out of 76 journals in the category "Communication".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Communication |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] | accessdate = 24 October 2015 |postscript=.}}</ref> It is published by [[Wiley-Blackwell]] on behalf of the [[International Communication Association]].

== Editors ==
The following persons have been editor-in-chief of the journal:
{{columns-list|colwidth=30em|
* 2012–present: [[Thomas Hanitzsch]] (University of Munich)
* 2009–2011: [[Angharad N. Valdivia]] ([[University of Illinois at Urbana–Champaign]])
* 2006–2008: [[Francois Cooren]] ([[Université de Montréal]])
* 2003–2005: [[Chris Segrin]] ([[University of Arizona]])
* 2003: [[Scott Jacobs (academic)|Scott Jacobs]] (University of Illinois)
* 2000–2002: [[Michael J. Cody]] ([[University of Southern California]])
* 1997–1999: [[James A. Anderson (University of Utah)|James A. Anderson]] ([[University of Utah]])
* 1994–1996: [[Don Ellis (academic)|Don Ellis]] ([[University of Hartford]]) 
* 1991–1993: [[Robert T. Craig]] ([[University of Colorado at Boulder]])
}}

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.wiley.com/WileyCDA/WileyTitle/productCd-COMT.html}}

[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1991]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Communication journals]]
[[Category:1991 establishments in the United States]]