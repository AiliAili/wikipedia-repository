{{infobox book | 
| name          = Caleb West, Master Diver
| title_orig    = 
| translator    = 
| image         = 
| image_caption = 
| author        = [[Francis Hopkinson Smith]]
| illustrator   = [[Malcolm Fraser (artist)|Malcolm Fraser]] and [[Arthur I. Keller]]
| cover_artist  = 
| country       = United States
| language      = English
| series        = 
| genre         = [[Novel]]
| publisher     = 
| pub_date      = April 1898
| english_pub_date =
| media_type    = Print ([[Hardcover]])
| pages         = 378 pp
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}

'''''Caleb West, Master Diver''''' is a novel published in 1898 by [[Francis Hopkinson Smith]] that was the [[Publishers_Weekly_list_of_bestselling_novels_in_the_United_States_in_the_1890s#1898|best selling book in the United States in 1898]].<ref name="hackett">Hackett, Alice Payne. [https://books.google.com/books?id=cOI8AQAAIAAJ Seventy Years of Best Sellers 1895-1965], p. 94 (1967) (the lists for 1895-1912 in this volume are derived from the lists published in ''[[The Bookman (New York)]]'')</ref> It was first serialized in ''[[The Atlantic|The Atlantic Monthly]]'' from October 1897 to March 1898, and was published in book form by [[Houghton Mifflin]] in April 1898 with illustrations by [[Malcolm Fraser (artist)|Malcolm Fraser]] and [[Arthur I. Keller]].<ref name="nyt-1">(2 April 1898). [https://select.nytimes.com/gst/abstract.html?res=FB0E1EF73D5C11738DDDAB0894DC405B8885F0D3 "Caleb West."; Mr. F. Hopkinson Smith's New Work of Fiction], ''[[The New York Times]]''</ref>

The book is based on Smith's experience in the building of the [[Race Rock Light]] near [[Fishers Island, New York]] in the 1870s.

==Adaptations==

The novel was adapted into a play.<ref>http://www.loc.gov/pictures/item/var1993000035/PP/</ref>

It was also adapted into a silent film in 1912, and a 1920 silent film called ''Deep Waters''.<ref>https://news.google.com/newspapers?id=JQVfAAAAIBAJ&sjid=TIoDAAAAIBAJ&pg=3203,5917269&dq=</ref>

==References==
{{reflist}}

==External links==
* ''[https://archive.org/details/cu31924022179828 Caleb West: Master Driver]'', full scan of 1898 Houghton & Mifflin edition via archive.org
* {{ibdb show|id=2352}}
* {{IMDb title|0338833|Caleb West}} (1912)
* {{IMDb title|0011103|Deep Waters}} (1920)
* [http://www.lilighthousesociety.org/historicalcollection/masterdiver/default.htm Captain Thomas A. Scott, Master Diver] (1908) (Smith's short tribute to the actual master driver on the Race Rock project)

[[Category:1898 novels]]
[[Category:19th-century American novels]]
[[Category:Works originally published in The Atlantic (magazine)]]
[[Category:Novels set in New York]]
[[Category:Novels by Francis Hopkinson Smith]]
[[Category:American novels adapted into films]]

{{1890s-novel-stub}}