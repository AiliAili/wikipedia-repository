<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Viper
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]]
 | national origin=[[Italy]]
 | manufacturer=[[Eurofly Srl]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]12,900 (2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Eurofly Viper''' is an [[Itay|Italian]] [[ultralight trike]], designed and produced by [[Eurofly Srl]] of [[Galliera Veneta]]. The aircraft is supplied as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 211. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The aircraft was designed to comply with the [[Fédération Aéronautique Internationale]] [[microlight]] category, including the category's maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. The Viper has a maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. It features a [[Flying wires|cable-braced]] [[hang glider]]-style [[high-wing]], weight-shift controls, a two-seats-in-[[tandem]] open cockpit, [[tricycle landing gear]] with [[wheel pants]] and a single engine in [[pusher configuration]].<ref name="WDLA11" />

The aircraft is made from welded tubing, with its double surface wing covered in [[Dacron]] sailcloth. Its {{convert|10.5|m|ft|1|abbr=on}} span [[Skyrider Hazard 12]] wing is supported by a single tube-type [[kingpost]] and uses an "A" frame weight-shift control bar. The standard powerplant is a twin cylinder, liquid-cooled, [[two-stroke]], [[dual-ignition]] {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine, with the twin cylinder, air-cooled, [[two-stroke]], [[dual-ignition]] {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] engine optional. The Viper has an empty weight of {{convert|126|kg|lb|0|abbr=on}} and a gross weight of {{convert|450|kg|lb|0|abbr=on}}, giving a useful load of {{convert|324|kg|lb|0|abbr=on}}. With full fuel of {{convert|65|l}} the payload is {{convert|277|kg|lb|0|abbr=on}}.<ref name="WDLA11" />

A number of different wings can be fitted to the basic carriage, including the Skyrider Hazard 12 and Hazard 15. A {{convert|70|l}} fuel tank is optional.<ref name="WDLA11" />

A basic version of the aircraft, powered by the {{convert|50|hp|kW|0|abbr=on}} Rotax 503 engine and without fairings is also offered.<ref name="WDLA11" />
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Viper with Hazard 12 wing) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=10.5
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=12
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=126
|empty weight lb=
|empty weight note=
|gross weight kg=450
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|65|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type= twin cylinder, liquid-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=48
|eng1 hp=

|prop blade number=2
|prop name=wooden
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=160
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=125
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=48
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=8
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=37.5
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
*{{Official website|http://www.euroflyulm.com/it/}}
{{Euro Fly aircraft}}

[[Category:Eurofly srl aircraft|Viper]]
[[Category:Italian sport aircraft 2000–2009]]
[[Category:Italian ultralight aircraft 2000–2009]]
[[Category:Single-engined pusher aircraft]]
[[Category:Ultralight trikes]]