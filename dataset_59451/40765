{{Use dmy dates|date=April 2013}}
[[File:Ogrodzieniec zamek 2.jpg|thumb|300px|The Ogrodzieniec Castle]]
[[File:Ogrodzieniec castle.jpg|thumb|300px|The Ogrodzieniec Castle, [[Napoleon Orda]]]]
[[File:Ogrodzieniec.png|thumb|300px|Draft of the Ogrodzieniec Castle]]

'''Ogrodzieniec Castle''' is a ruined medieval castle in the [[Kraków-Częstochowa Upland]]. Rebuilt several times in its history, the castle was originally built in the 14th–15th century by the Włodkowie Sulimczycy family.<ref>[http://www.polishcastles.eu/castles/podzamcze/podzamcze.php "Podzamcze – Ogrodzieniec Castle"], ''Castles in Poland''</ref> The castle is situated on the 515.5 m high Castle Mountain ([[Polish language|Polish]]: Góra Zamkowa), the highest hill of the [[Kraków-Częstochowa Upland]]. Located on the [[Trail of the Eagles' Nests]], the ruins are opened for visitors.

== History ==
Established in the early 12th century, during the reign of [[Bolesław III Wrymouth]] ([[Polish language|Polish]]: Bolesław Krzywousty), the first stronghold was razed by the [[Tatars]] in 1241. In the mid-14th century a new  [[Gothic architecture|gothic]] castle was built here to accommodate the Sulimczycy family. Surrounded by three high rocks, the castle was well integrated into the area. The defensive walls were built to close the circuit formed by the rocks, and a narrow opening between two of the rocks served as an entrance.

In 1470 the castle and lands were bought by the wealthy [[Kraków|Cracovian]] townsmen, Ibram and Piotr Salomon. Then, Ogrodzieniec became the property of Jan Feliks Rzeszowski,  the rector of [[Przemyśl]] and the canon of [[Kraków|Cracow]]. The owners of the castle about that time were also Jan and Andrzej Rzeszowskis, and later Pilecki and Chełmiński families. In 1523 the castle was bought by Jan Boner. After his death, the castle passed to his nephew, Seweryn Boner, who replaced the medieval stronghold with a [[Renaissance architecture|renaissance]] castle in 1530–1545.

In 1562 the castle became the property of the Great Marshal of the Crown [[Jan Firlej]], as a result of his marriage with Zofia- the daughter of Seweryn Boner. Later on, in 1587, the castle was captured by the arms of the Austrian archduke [[Maximilian III, Archduke of Austria|Maximilian III]], the rejected candidate to the [[Polish–Lithuanian Commonwealth|Polish-Lithuanian]] throne. In 1655, it was partly burnt by the Swedish troops, who -deployed here for almost two years- destroyed the buildings considerably. From 1669 on, the castle belonged to Stanisław Warszycki, the Cracow's [[castellan]], who managed to partly rebuild the castle after the Swedish devastations.

About 1695 the castle changed hands once again becoming the property of the Męciński family. Seven years later, in 1702, over a half of the castle had burnt down in the fire set by the Swedish troops of [[Charles XII of Sweden|Charles XII]]. After the fire, it was never to be rebuilt. About 1784 the castle was purchased by Tomasz Jakliński, who did not care for its condition. Consequently, the last tenants left the devastated castle about 1810. The next owner of the Ogrodzieniec Castle was Ludwik Kozłowski, who used the remains of the castle as a source of building material and sold out the castle's equipment to the Jewish merchants.

The last proprietor of the castle was the neighbouring Wołoczyński family.
After the [[Second World War]], the castle was nationalized. The works aimed at preserving the ruins and opening them to the visitors were started in 1949 and finished in 1973.

== Curiosities ==
[[File:Zamek w Ogrodzieńcu Widok z przedzamcza 2.jpg|300px|thumbnail|view of ward]]
According to some investigators of paranormal phenomena, the Ogrodzieniec Castle is a place haunted by mighty dark powers. There have been locally famous reports of the "Black Dog of Ogrodzieniec" being seen prowling the ruins in the night-time. Witnesses have claimed that the spectre is a black dog much larger than an ordinary dog, and is supposed to have burning eyes and pull away a heavy chain. The dog is believed to be the soul of the [[Castellan]] of Cracow, [[Stanisław Warszycki]]. Interestingly, his soul also haunts the ruins of the [[Danków, Silesian Voivodeship|Dańków]] Castle, where it appears as a headless horseman.<ref>Bogna Wiernikowska, Maciej Kozłowski "Duchy polskie" 1985</ref>

On the bottom floor, fragments of the renaissance frescos of lilies are still visible.

Close to the castle, on the market of Podzamcze village, there is a chapel built out of the castle's architectural elements ([[Portal (architecture)|portal]], [[volute]]s, [[cornice]]). Inside the chapel, there are original elements of the castle chapel: the vault [[Keystone (architecture)|keystone]], [[round shot]], which is said to have fallen into the castle during the Swedish [[Deluge (history)|Deluge]] (1655–1660) and the renaissance Our Lady sculpture. Unfortunately, the sculpture has been painted in the folk style (with oil-paint) by the locals, which makes it rather difficult to notice its original beauty.

In 1973 the Ogrodzieniec Castle was used as an outdoor-scenery for the television series "[[Juraj Jánošík|Janosik]]".

In 1980 the film "The Knight" by [[Lech Majewski]] was shot in the Ogrodzieniec Castle.

In 1984 the castle was presented in the "[[Behind the Iron Curtain (video)|Behind The Iron Curtain]]" part of the [[Iron Maiden]]'s video "[[Live After Death]]". The material filmed in the Ogrodzieniec Castle wad used in the song "[[Hallowed Be Thy Name]]".

In 2001 the ruins were used as a scenery for [[Andrzej Wajda]]'s film "[[The Revenge (film)|The Revenge]]". For the purpose of film making, large decorations were built in the castle, which remained there after the realization of the film was finished.

== Gallery ==
<gallery>
File:Ogrodzieniec-Zamek-Tamerlan.jpg|The Ogrodzieniec Castle- view from the East
File:Ogrodzieniec sala tortur 1.JPG|The torture chamber as seen from the tower
File:Ogrodzieniec sala tortur 2.JPG|The torture chamber
File:Ogrodzieniec zamek MG 1.JPG|A tower of the Ogrodzieniec Castle with the Polish flag
File:Ogrodzieniec zamek MG 2.JPG|The castle windows
File:Ogrodzieniec zamek 3.JPG|The castle walls
File:Ogrodzieniec zamek 4.JPG|The castle walls
File:Ogrodzieniec zamek 5.JPG|The castle walls
File:Ogrodzieniec - ściana południowa (2012-08-26).jpg|The South wall of the Ogrodzieniec Castle (2012-08-26)
File:Ogrodzieniec Castle.jpg|Inside castle
</gallery>

== See also ==
* [[Ogrodzieniec]]

== References ==
{{Reflist}}

==External links==
* [http://www.zamek-ogrodzieniec.pl/en Official site]

{{Royal Residences in Poland |state=expanded}}

{{coord|50|27|12|N|19|33|12|E|region:PL_type:city_source:kolossus-plwiki|display=title}}

[[Category:Castles in Lesser Poland Voivodeship]]