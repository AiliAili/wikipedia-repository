{{Infobox journal
| title = Management Information Systems Quarterly
| cover = [[File:MIS Quarterly low res cover.jpg]]
| editor = [[Arun Rai (Professor)]]
| discipline = [[Information science]], [[management]]
| former_names = 
| abbreviation = Manag. Inf. Syst. Q.
| publisher = [[Management Information Systems Research Center]]
| country = United States
| frequency = Quarterly
| history = 1977-present
| openaccess = 
| license = 
| impact = 5.384| impact-year = 2015
| website = http://www.misq.org/
| link1 = http://www.misq.org/current/
| link1-name = Online access
| link2 = http://www.misq.org/archive/
| link2-name = Online archive
| JSTOR = 
| OCLC = 805082941
| LCCN = 85652044
| CODEN = MISQDP
| ISSN = 0276-7783
| eISSN = 2162-9730
}}
'''''Management Information Systems Quarterly''''' is a quarterly [[peer-reviewed]] [[academic journal]] that covers research in the areas of [[management information system]]s and [[information technology]]. It was established in 1977 and is widely regarded as one of the two most prestigious journals in the information systems discipline.<ref name="MISQ_8refs">{{cite web |last = |first = |authorlink = |coauthors = |title =Introduction - MIS Journal Rankings |work = |publisher = Association for Information Systems |date = |url = http://ais.affiniscape.com/displaycommon.cfm?an=1&subarticlenbr=432 |format = |doi = |accessdate = 2008-09-01}}{{dead link|date=October 2013}}</ref><ref>{{cite journal|last1=Ferratt |first1=T.W. |last2=Gorman |first2=M.F. |last3=Kanet |first3=J.J. |last4=Salisbury |first4=W.D. |year=2007 |url=http://aisel.aisnet.org/cgi/viewcontent.cgi?article=2677&context=cais |title=IS Journal Quality Assessment Using the Author Affiliation Index |journal=Communications of the Association for Information Systems |volume=19 |pages=710–724}}</ref> It is an official journal of the [[Association for Information Systems]]<ref>{{cite web |author=Galletta, D. |date=October 2007 |url=http://home.aisnet.org/displaycommon.cfm?an=1&subarticlenbr=1 |title=President's Message |publisher=Association for Information Systems}}</ref> and is published by the [[Management Information Systems Research Center]] ([[University of Minnesota]]). The current [[editor-in-chief]] is Dr. [[Arun Rai (Professor) |Arun Rai]] from [[Georgia State University]].<ref>{{cite web|title=MIS Quarterly appoints new Editor-in-Chief|url=http://misq.org/skin/frontend/default/misq/pdf/Announcements/ArunRaiAnnouncement.pdf}}</ref>

The journal had the highest [[impact factor]] (4.978) of all peer-reviewed academic journals in the field of Business from 1992–2005.<ref>{{cite journal |last1=Mangematin |first1=V. |authorlink=Vincent Mangematin |last2=Baden-Fuller |first2=C. |year=2008 |title=Global Contests in the Production of Business Knowledge: Regional Centres and Individual Business Schools |journal=Long Range Planning |volume=41 |pages=117–139 |doi=10.1016/j.lrp.2007.11.005}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 impact factor of 5.384.<ref name=WoS>{{cite book |year=2015 |chapter=Management Information Systems Quarterly |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==Editors-in-chief==
Past editors-in-chief in order of succession have been:
{{columns-list|colwidth=30em|
*[[Gary W. Dickson]] (1977-1982)
*William R. King (1983-1985)
*Warren McFarlan (1986-1988)
*James Emery (1989-1991)
*Blake Ives (1992-1994)
*Robert Zmud (1995-1998)
*[[Allen S Lee|Allen Lee]] (1999-2001)
*Ron Weber (2002-2004)
*Carol Saunders (2005-2007)
*Detmar Straub (2008-2012)
*Paulo Goes (2013-2015)
*[[Arun Rai (Professor)|Arun Rai]] (2016-Present)
}}

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.misq.org}}
{{University of Minnesota campus}}
[[Category:Business and management journals]]
[[Category:Quarterly journals]]
[[Category:Information systems journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1977]]
[[Category:University of Minnesota]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:Academic journals published by universities and colleges of the United States]]