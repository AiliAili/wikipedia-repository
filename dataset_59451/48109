{{Infobox settlement
|official_name            = City of Wichita Falls
|subdivision_type         = [[List of countries|Country]]
|subdivision_name         = {{flagicon|USA}} [[United States]]
|subdivision_type1        = [[Political divisions of the United States|State]]
|subdivision_name1        = {{flagicon|Texas}} [[Texas]]
|subdivision_type2        = [[List of counties in Texas|County]]
|subdivision_name2        = [[Wichita County, Texas|Wichita]]

|area_total_sq_mi         = 70.1
|area_land_sq_mi          = 70.66
|area_water_sq_mi         = 0.04
|area_water_percent       =
|area_urban_km2           =
|area_total_km2           = 183.1
|area_land_km2            = 183.0
|area_water_km2           = 0.1
|elevation_m              = 289
|elevation_ft             = 948
|coordinates              = {{coord|33|53|49|N|98|30|54|W|region:US_type:city|display=inline,title}}
}}

==Exposed Rocks==
The exposed strata at the surface in and around [[Wichita Falls, Texas|Wichita Falls]] are the products of one ancient period of deposition with a modest amount of recent and modern alteration.  In all cases, the strata are products of [[terrigenous]] (non-marine) environments dominated by [[fluvial]]  depositional and [[erosion]]al systems (rivers and streams).

===Brief geologic history===
The rocks  found in and around Wichita Falls result from southwesterly-flowing [[Permian]] streams that deposited sands in channels and silts and clays on the surrounding floodplains.  Calcium-carbonate rich soils concurrently developed adjacent to these streams.  These were likely buried by further Permian sedimentation and then [[lithification|lithified]].  [[Pleistocene]] erosion removed the younger rocks, exposing the current strata.  Exposures of sediments indicate that northeast-flowing streams locally deposited silts, clays, sands, and some gravels on the Permian rocks.  These are subsequently modified by modern ([[Holocene]]) stream erosion and deposition.

===Permian Rocks===
In the [[Permian]] geologic period, North-Central Texas was a part of the western coastal zone of equatorial [[Pangea]], a super-continental land mass.<ref name=Ziegler>Ziegler, A.M., Hulver, M.L. and Rowley, D.B., 1997, Permian World Topography and Climate. In: Late Glacial and Postglacial Environmental Changes – Pleistocene, Carboniferous- Permian and Proterozoic, Ed. I.P. Martini, pp. 111–146. Oxford University Press, Oxford.</ref>  Nearby uplifts and mountainous regions, such as the Muenster Arch and Red River Uplift, the [[Wichita Mountains|Wichita]], [[Arbuckle Mountains|Arubckle]], and [[Ouachita mountains|Ouachita]] mountains developed by the end of the [[Pennsylvanian (geology)|Pennsylvanian]],<ref name=Oriel>Oriel, S., Myers, D.A. and Crosby, E.J., 1967, Paleotectonic investigations of the Permian system in the United States. US Geol. Surv. Prof. Paper, 515, Chapter C, 80 pp.</ref> providing elevated topography to the north and east during the Permian.  The rocks of the [[Permian Basin (North America)|Permian Basin]] of West Texas record a contemporaneous shallow inland sea.  The resulting topography provided northeast-to-southwest gradients for stream flow and sediment movement.  The sediments deposited by the Permian streams of North-Central Texas were likely reworked [[clastic]] materials from Middle Pennsylvanian stream and fan-delta sediments proximal to the Ouachita foldbelt and Muenster Highlands.<ref name=Brown>Brown, L.F., Jr (1973) Pennsylvanian rocks of North-Central Texas: an introduction. In: Pennsylvanian Depositional Systems in North-Central Texas, Ed. L.F. Brown, Jr,, Bur. Econ. Geol. Guidebook, 14, 1–9.</ref>
[[File:Wiley-Rd-ss6.jpg|thumb|Sandstone channel in mudstone as exposed along Wiley Road, west of Wichita Falls.  The buff colored rock preserves numerous fluvial sedimentary structures.]]

The Petrolia Formation (of the Late Wolfcampian-Leonardian systems) dominates the exposed Permian strata in Wichita falls, as mapped by the 1987 Texas Atlas of Geology.<ref name=Hentz>Hentz, T. F., and Brown, L. F., Jr., 1987, Wichita Falls–Lawton sheet: The University of Texas at Austin, Bureau of Economic Geology, Geologic Atlas of Texas, 1 sheet, scale 1:250,000.</ref>  The map describes the formation as 360–400 feet of weakly or unstratified [[mudstone]] with laminated, [[cross-bedding|cross-bedded]] [[sandstone]] lenses.  The formation increases in mudstone content upsection.  Sandstone lenses contain terrestrial fossils of plants, vertebrates, and footprints.  The unit contains [[calcareous]] nodules of varying sizes as well as poorly [[indurated]] "conglomerate" with vertebrate fossils.  In general the entire package is only weakly lithified, perhaps indicating that the region was not appreciably covered by a thick package of younger strata.

Several correlated sandstone units crop out in the immediate region.  These dominate the region adjacent to the Seymour Highway, on the slopes to the south of the Wichita River (locally known as "the bluffs").  The  Texas Atlas of Geology mapped these as ss6.<ref name=Hentz />  Most outcrops are buff colored medium-grained, well-sorted quartzose sandstones.  These exhibit extensive [[cross-bedding]] and [[soft-sediment deformation]] features.  Some deposits are [[friable]], others well cemented.  Locally, there appears to be three prominent layers of sandstone separated by mudstone.  Because of variable erosion rates, each influences topography by forming ledges and benches, and in places may form [[mesa]]-like landforms.  In and around the city, these occur roughly at 960, 1000, and 1060 feet above sea level.

===Pleistocene and/or early Holocene Sediments===
[[File:Quaternary gravels.JPG|thumb|Pleistocene and/or early Holocene quartzite and other hard-stone gravels on soil surface.]]
Up to 30 feet of fluvial deposits of unconsolidated gravel, [[sand]], and [[silt]] mapped as [[fluvial terrace|terrace]] deposition by the 1987 Texas Atlas of Geology.<ref name=Hentz />  Gravels are granule-to-cobble size, with clasts of angular to well-rounded quartzite, quartz, and chert from distal sources and lesser fragments of local strata.  The sands are orange-brown to tan, fine- to coarse-grained with preserved soils.

===Holocene Sediments===
Up to 30 feet of [[floodplain]] and [[stream channel|channel]] [[sand]], [[silt]], [[clay]], and gravel from the modern stream systems.  This includes terrace deposits near floodplain, [[colluvium]] on valley slopes, and local wind-blown sand and silt.<ref name=Hentz />

==Structural Geology==
The region is largely underlain with shallowly west-dipping strata, but a significant uplifted block is found in the subsurface immediately north of Wichita Falls.   This block, locally known as the Red River uplift, may be part of an uplifted system that extends eastward, joining the Muenster Arch.  The uplifts offset [[Pennsylvanian (geology)|Pennsylvanian]] and older strata in the subsurface and are thought to be contemporaneous with the [[Ouachita Orogeny|Ouachita]] and [[Ancestral Rocky Mountains|Ancestral Rocky]] [[Orogeny|Orogenies]].  These Pennsylvanian orogenies resulted from the closure of the [[Iapetus Ocean|Iapetus]] ocean as the [[Gondwana]] and [[Laurentia]] continents collided to form [[Pangea]].

== Oil and Gas Resources==
Petroleum resources were discovered in the region in the early 1900s, and the area remains a locus of exploration and production.

==Water Resources==

===Groundwater===
Groundwater in Wichita Falls is drawn from  the Seymour aquifer or from alluvial aquifers associated with local streams.<ref name=Duffin>Duffin, G., L., and Beynon, B.E., 1992, Evaluation of Water Resources in Parts
of the Rolling Prairies Region of North-Central Texas, Texas Water Development Board, Report 337, 93 p.</ref>  Groundwater withdrawals are limited to individual property owners and do not feed into the municipal supply for the city of Wichita Falls.  However, the nearby city of  [[Burkburnett, TX|Burkburnett]], located 15 miles north of (and down hydraulic gradient from) Wichita Falls relies in part on this aquifer for municipal supply.<ref name=McGregor>McGregor, W., Hancock-White, K., and Oswald, R., 2011, Texoma Water Resources, unpublished student report, Midwestern State University Geosciences.</ref>

The Seymour aquifer may be locally confined under clay-rich soils and sediments.  In the southern part of the city, the distribution and nature of the aquifer is consistent with one hosted by Pleistocene and/or Holocene fluvial channel deposits.  The depth to water (hydrostatic head) averages 14 feet below the surface, closely correlated with topography.  The water is likely sourced in coarse sands and gravels 10-19' below the surface. Dry holes in this same area contain no typically sand-gravel layers within the first 22 feet.  The horizontal distribution of this shallow aquifer is irregular; dry holes may be adjacent to those with water yields.<ref name=McGregor />

====Cedar Springs Pools====
Seymour aquifer groundwater once fed the Cedar Springs Pools, a popular recreational public swimming facility in the 1930s and 40s.  The two pools were located between Taft Boulevard and Robin Lane north of Hampstead Rd. in the southern part of Wichita Falls.  Long-time residents have vivid memoirs of the frigid waters in these ponds.<ref name=McGregor />

===Surface water===
[[File:USGS FS 2011-3018-f1.tif|thumb|Wichita River watershed and adjacent streams and lakes]]

====The Wichita River====
Wichita Falls is located along the [[Wichita River]] roughly 25 miles southwest of its confluence with the [[Red River of the South|Red River]].  The river essentially bisects the city into north and south.  The river exhibits many of the classic morphological features associated with meandering streams; Lucy Park, an expansive city green space, occupies a large meander bend near downtown and the Tanglewood subdivision surrounds an oxbow lake.  The upstream waters of the Wichita River are impounded for flood control and reservoir capacity.

=====Lake Diversion=====
Lake Diversion was impounded in 1924  in northwest [[Archer County, TX|Archer]] and northeast [[Baylor County, TX|Baylor]] counties, 71 miles upstream from Wichita Falls.  Lake Diversion sits at an elevation of 1,053 feet above sea level, has a surface area of 3,133 acres, and a maximum depth of 35 feet.  The lake serves to alleviate flooding, and a source of water for the Dundee fish hatchery, and as an irrigation source, feeding a 135-mile network of irrigation canals that extend to the east side of Wichita Falls (Wichita County Water Improvement District #2).<ref name=fdlkdiversion>FindLakes website, 2011, http://findlakes.com/lake_diversion_texas~tx01011.htm.  accessed November 26, 2011</ref>

=====Lake Kemp=====
Located 8 miles north of [[Seymour, Texas|Seymour]] in [[Baylor County, Texas|Baylor County]] and 40 miles upstream of Wichita Falls, Lake Kemp was developed in 1924 to alleviate persistent flooding issues downstream and to provide irrigation and drinking water for the city and the adjacent farming areas.  The elevation of Lake Kemp is 1,142 feet asl and contains 245,308 acre feet (80 billion gallons)at its maximum capacity.  The lake has 100 miles of shoreline, some of which is used for housing and recreation.<ref name=kemp>"LAKE KEMP," Handbook of Texas Online Published by the Texas State Historical Association. http://www.tshaonline.org/handbook/online/articles/rol46, accessed November 23, 2011</ref>

Lake Kemp was used as a source of drinking water through the 1950s but lost popularity because of its [[gypsum]] taste.  In 2008, the City of Wichita Falls began [[reverse osmosis]] water treatment and is now processing water from Lake Kemp through this [[desalination]] system<ref name=Langdon>Langdon, J., "City Realizes Dreams of Potable Water From Lake Kemp with Reverse Osmosis Plant", Times Record News, Wichita Falls, Texas, September 27, 2008.</ref>  Additionally, Lake Kemp provides water to the American Electric Power Company in [[Oklaunion, TX|Oklaunion]], Texas. Lake Kemp is named for entrepreneur [[Joseph A. Kemp]].<ref name=McGregor />

=====Truscott Brine Lake=====
[[File:Truscott Brine Lake, Knox County, Texas.jpg|thumb|right|Truscott Brine Lake]]
Truscott Brine Lake was completed in 1987 by the [[United States Army Corps of Engineers|Army Corps of Engineers]], Tulsa District.<ref name=Haynie>Haynie, M.M., Burke, G.F., Baldys, S., 2011, "Chloride Control and Monitoring Program in the Wichita River Basin, Texas, 1996-2009", United States Geological Survey Fact Sheet #2011-3018</ref>  The lake covers 2.98 acres with 24 miles of shoreline. The lake has a normal storage of 107,000 acre-feet, a maximum capacity of 116,200 acre-feet of water with a maximum discharge rate of 35,400 cubic feet per second.  The lake drains a 26-square mile area.<ref name=fdlktruscott>FindLakes website, 2011, http://findlakes.com/truscott_brine_lake_texas~tx05996.htm.  accessed November 27, 2011.</ref>

This  project is aimed at controlling chloride and other salinity components in the Wichita River watershed.  There are naturally occurring salt springs in the area.  The salt springs result from the percolation of groundwater through Permian strata rich in [[evaporite]] minerals.<ref name=Haynie />

Authorized in 1974, and completed in 1987,  a 5-foot tall inflatable collection dam was constructed across the South Wichita River near [[Guthrie, Texas|Guthrie]], Texas.  During low flow periods, this salty water is piped 23 miles to Truscott Brine Lake, where the water is contained for eventual evaporation.<ref name=Haynie />  An additional low-water inflatable dam has been installed on the Middle Fork of the Wichita River, but a pipeline to Truscott Lake has not yet been completed.<ref name=Truscott>"Truscott Brine Lake" Army Corps of Engineers website, 1999, http://www.swt.usace.army.mil/library/Chloride%20Control%20-%20Wichita%20River%20Basin/library/fs-110-00.pdf.  accessed November 26, 2011</ref>

====Holliday Creek====
[[Holliday Creek (Wichita River)|Holliday Creek]] is a tributary to the Wichita River that originates south of Dundee in [[Archer County, Texas|Archer County]], Texas, to the north edge of Wichita Falls.  It is impounded at the southern edge of Wichita Falls to form Lake Wichita.<ref name=FEMA>Federal Emergency Management Agency, 2008, "FEMA Flood Insurance Study for Wichita County," FEMA Flood Insurance Study Number 48485CV000A.</ref>

=====Lake Wichita=====
Located on the southern edge of the city limits, partly in [[Wichita County, TX|Wichita County]] and partly in Archer County, this lake at 971 feet above sea level was developed in 1901 by the entrepreneur [[Frank Kell]], his brother-in-law Joseph Kemp, and the Lake Wichita Irrigation and Water Company to alleviating flooding and provide irrigation water for local farms and municipal water for Wichita Falls. The lake is recharged from the Holliday Creek watershed and from irrigation conduits from Lake Diversion.
The original lake covered 2,200 acres with a maximum capacity of 14,000 [[acre-feet]] of water. In 1995, the city constructed a new spillway that was 4.7 feet lower than the previous spillway.<ref name=Howell>Howell, M., "After 15 years, Lake Wichita is Slowly Reaching Its New Potential", Times Record News, Wichita Falls, Texas, August 19, 2010.</ref> as part a flood control project on Holliday Creek.<ref name="FEMA" />  The current lake now covers 1,224 acres to a maximum depth of 8 feet at its deepest spot. Because of its shallow nature, this lake is currently only used for recreation and not as a municipal reservoir.<ref name=wichita>"LAKE WICHITA," Handbook of Texas Online Published by the Texas State Historical Association.  http://www.tshaonline.org/handbook/online/articles/rol85, accessed November 10, 2011.</ref>

====The Little Wichita River====
The [[Little Wichita River]] is a near-parallel drainage system to the south of Wichita River and a tributary to the Red River, emptying into the Red about 20 miles downstream of the Wichita River.  The Little Wichita River drains the regions to the south and east of the Wichita Falls city limits, and its waters are impounded in two lakes that feed the Wichita Falls municipal water system.

=====Lake Arrowhead=====
This lower lake in the Little Wichita River watershed is located 15 miles southeast of Wichita Falls on the western edge of Clay County.  The most recently built of the region's large reservoirs, it was impounded in 1965 following a prolonged legal battle and the eventual relocation of the residents (living and deceased)of [[Halsell, Texas]].  It has a drainage area of 832 square miles, a maximum capacity of 228,000 [[acre-feet]] (74 billion gallons), and sits at 926 feet above sea level <ref name=arrowhead>"LAKE ARROWHEAD," Handbook of Texas Online Published by the Texas State Historical Association. http://www.tshaonline.org/handbook/online/articles/rol11, accessed October 26, 2011.</ref>   Wichita Falls is authorized by the Texas Water Development Board to utilize up to 45,000 acre-feet of water annually for municipal purposes <ref name=TWDBarrowhd>Texas Water Development Board, 2002, "Volumetric Survey of Lake Arrowhead".</ref>

=====[[Lake Kickapoo]]=====
This upper lake on the Little Wichita River watershed is located approximately 18 miles southwest of Wichita Falls, in Archer County.  Constructed in 1945, the lake is at an elevation of 1,014 feet above sea level, has a surface area of 6,200 acres, and has a maximum capacity of 106,000 [[acre feet]] (35 billion gallons).<ref name=kickapoo>"LAKE KICKAPOO," Handbook of Texas Online Published by the [[Texas State Historical Association]]. http://www.tshaonline.org/handbook/online/articles/rol47, accessed October 26, 2011.</ref>

=====Lake Ringgold=====
Lake Ringgold is a proposed reservoir site on the Little Wichita River near the city of [[Henrietta, TX|Henrietta]] with a potential of 27,000 acre-feet per year by 2050 to provide 77,003 acre-feet of additional water storage by the year 2060.  The total capital costs to develop this additional water resource are $383 million.<ref name=TWDBstateplan>Texas Water Development Board, 2012, Water for Texas 2012, State Water Plan http://www.twdb.state.tx.us/publications/state_water_plan/2012/2012_SWP.pdf</ref>

==References==
{{reflist}}

*
*
*
*

[[Category:Geology of Texas]]
[[Category:Wichita Falls, Texas]]