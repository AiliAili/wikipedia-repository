{{Infobox journal
| title = Annals of the Rheumatic Diseases
| cover = [[File:ARD_cover_image.gif]]
| editor = Tore K. Kvien
| discipline = [[Rheumatology]]
| formernames =
| abbreviation = Ann. Rheum. Dis.
| publisher = [[BMJ Group]]
| country =
| frequency = Monthly
| history = 1939–present
| openaccess = [[Delayed open access journal|Delayed]], after 12 months
| license =
| impact = 10.377
| impact-year = 2014
| website = http://ard.bmj.com
| link1 = http://ard.bmj.com/content/current
| link1-name = Online access
| link2 = http://ard.bmj.com/content/
| link2-name = Online archive
| ISSN = 0003-4967
| eISSN = 1468-2060
| OCLC = 746945890
| CODEN = ARDIAO
}}
The '''''Annals of the Rheumatic Diseases''''' is a [[peer-reviewed]] [[medical journal]]. It is co-owned by the [[BMJ Group]] and the [[European League Against Rheumatism]] and covers all aspects of [[rheumatology]], including [[human musculoskeletal system|musculoskeletal]] conditions, [[arthritis]], and [[connective tissue disease]]s. The journal publishes basic, clinical, and translational research and abstracts from conferences.<ref name="ARD Homepage">[http://ard.bmj.com Journal homepage]</ref> The journal was established in 1939.<ref name="ARD Homepage"/> It is available online by subscription and archived editions of the journal (older than one year) are available free of charge. Each issue, the [[editor-in-chief]] selects a paper to be published open access as an "Editors Choice". The editor-in-chief is [[Tore K. Kvien]].<ref name="ARD Homepage"/>

== Lay summaries ==
The journal publishes lay summaries for patients and non-clinicians that explain the findings and treatment implications arising from key research papers published in the journal. Written by ''[[BMJ]]'''s Best Health team, they are also checked for accuracy by the journal's editors.

== Abstracting and indexing ==
The journal is abstracted and indexed by the [[Science Citation Index]], [[Index Medicus]], [[Excerpta Medica]], and [[BIOSIS Previews]].<ref name="ARD Homepage"/> According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 10.377 ranking it first out of 32 journals in the category "Rheumatology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Rheumatology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

The ''Annals of the Rheumatic Diseases'' has been cited most often by: ''[[Arthritis & Rheumatism]]'', ''Annals of the Rheumatic Diseases'', ''[[Rheumatology (journal)|Rheumatology]]'', ''[[Journal of Rheumatology]]'', and ''[[Clinical and Experimental Rheumatology]]''. The journals that have been cited most by ''Annals of the Rheumatic Diseases'' are ''Arthritis & Rheumatism'', ''Annals of the Rheumatic Diseases'', ''Journal of Rheumatology'', ''Rheumatology'', and ''[[Journal of Immunology]]''.

==References==
<references/>

== External links ==
* {{Official website|http://ard.bmj.com}}

{{DEFAULTSORT:Annals Of The Rheumatic Diseases}}
[[Category:BMJ Group academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1939]]
[[Category:English-language journals]]
[[Category:Rheumatology journals]]