{{good article}}
{{Infobox song
| Name           = All in Love Is Fair
| Border         = yes
| Cover          = File:Stevie Wonder "All in Love Is Fair".jpg
| Alt          = A white vinyl record of the single appears
| Caption          = A-side of 1974 "Too High" Brazil single
| Artist         = [[Stevie Wonder]]
| Album          = [[Innervisions]]
| Released       = August 3, 1973
| Recorded       = 
| Format         = [[Gramophone record|7"]]
| Genre          = [[Pop music|Pop]]<ref name="Multicultural Perspectives in Music Education" />
| Length         = {{Duration|m=3|s=41}}
| Writer         = Stevie Wonder
| Producer       = Stevie Wonder
| Label          = {{hlist|Tapecar|[[Motown|Tamla]]}}
| prev           = "Jesus Children of America"
| prev_no        = 6
| track_no       = 7
| next           = "[[Don't You Worry 'bout a Thing]]"
| next_no        = 8
| Misc           = 
}}
"'''All in Love Is Fair'''" is a song recorded by American singer-songwriter [[Stevie Wonder]] for his sixteenth studio album, ''[[Innervisions]]'' (1973). Written and produced by Wonder, it was released as a [[Gramophone record|7" single]] in Brazil in 1974. The song is a [[Pop music|pop]] [[ballad]] with lyrics that describe the end of a relationship through the use of clichés. Critical reaction to the song was varied: Matthew Greenwald of AllMusic wrote that it was among Wonder's "finest ballad statements",<ref name="AllMusic Streisand review" /> while [[Robert Christgau]] felt that the singer's performance was "immature".<ref name="The Sound of Stevie Wonder" /> It has been recorded by a number of other artists, including [[Brook Benton]], [[Nancy Wilson (jazz singer)|Nancy Wilson]] and [[Cleo Laine]]. Wonder also placed it on several of his [[greatest hits album]]s, including his most recent one, ''[[The Complete Stevie Wonder]]'', in 2005.

American vocalist [[Barbra Streisand]] released "All in Love Is Fair" as a single in 1974 for her fifteenth studio album, ''[[The Way We Were (Barbra Streisand album)|The Way We Were]]'' (1974). [[Tommy LiPuma]] handled the production for the 7" single release by [[Columbia Records]]. Among [[Music journalism|music critics]], Greenwald called her version "unforgettable",<ref name="AllMusic Streisand review" /> and ''Rolling Stone''{{'s}} Stephen Holden wrote that it was "almost as interesting" as Wonder's rendition.<ref name="The Super Seventies review" /> Commercially, the song peaked on the [[Billboard Hot 100|''Billboard'' Hot 100]] in the United States at number 63, and Canada's Top Singles chart at number 60. She too has included it on numerous [[compilation albums]], including ''[[Barbra Streisand's Greatest Hits Vol. 2]]'' (1978) and ''[[The Essential Barbra Streisand]]'' (2002).

== Release and composition ==
"All in Love Is Fair" is taken from Stevie Wonder's sixteenth studio album, ''[[Innervisions]]'', released on August 3, 1973 by [[Motown|Tamla Records]].<ref name="Innervisions">{{cite AV media notes |title=[[Innervisions]] |others=[[Stevie Wonder]] |year=1973 |type=Liner notes |publisher=[[Motown|Tamla]] |id=T 326L}}</ref> Despite not being released as a commercial single in his native country of the United States, Tapecar Records and Tamla released it as a [[Gramophone record|7" single]] sometime in 1974, exclusively in Brazil. It was paired alongside the opener track for side one of ''Innervisions'', "Too High".<ref name="All in Love Is Fair SW">{{cite AV media notes |title=''"All in Love Is Fair" / "Too High"'' |others=Stevie Wonder |year=1974 |type=Liner notes |publisher=Tapecar |id=CS-829}}</ref>

A [[Pop music|pop]] [[ballad]],<ref name="Multicultural Perspectives in Music Education" /> "All in Love Is Fair" was compared to the works of [[Johnny Mathis]] by Lenny Kaye of ''[[Rolling Stone]]'' and the editors at ''[[Playboy]]''.<ref name="The Super Seventies reviews">{{cite news|title=''Innversions'' – Stevie Wonder|url=http://www.superseventies.com/spwonderstevie1.html|accessdate=January 9, 2017|publisher=[[MTV Classic (U.S. TV network)|The Super Seventies]]}}</ref> Additionally, Lawrence Gabriel, author of ''MusicHound Rock: The Essential Album Guide'', described the track as a "classic" pop song.<ref name="Music Hound Rock">{{harvnb|Graff|1996|p=837}}</ref> It is written in the key of [[D minor]] with Wonder's vocals ranging from C<sub>4</sub> to A<sub>5</sub>; it is additionally accompanied by the instrumentation of a piano and a guitar.<ref name="Sheet music">{{cite web|url=http://www.musicnotes.com/sheetmusic/mtd.asp?ppn=MN0063622|title=Stevie Wonder 'All in Love Is Fair' Digital Sheet Music|publisher=Musicnotes.com|last1=Wonder|first1=Stevie|year=1974|accessdate=January 10, 2017}}</ref> The lyrics of the composition describe a couple that is nearing the end of their relationship; Wonder purposely uses "cliché lines" to get his point across and to prove the clichés true.<ref name="The Sound of Stevie Wonder">{{harvnb|Perone|2006|p=53}}</ref> Janine McAdams from ''[[Billboard (magazine)|Billboard]]'' found "dramatic intensity" within the lyrics, "I should have never left your side / The writer takes his pen / To write the words again / That all in love is fair".<ref name="Billboard lyrics">{{cite journal|last1=McAdams|first1=Janine|title=A Songbook in the Key of Life|journal=[[Billboard (magazine)|Billboard]]|date=May 13, 1995|volume=107|issue=19|pages=30, 38|url=https://books.google.com/books?id=vAsEAAAAMBAJ|accessdate=January 9, 2017|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> Author Herb Jordan included the track's lyrics in his book ''Motown in Love: Lyrics from the Golden Era''; they were placed under the section for songs in which the lyrics detail "lessons of love".<ref name="Motown in Love">{{harvnb|Jordan|2006|p=2}}</ref>

== Reception and further promotion ==
[[File:Cleo Laine.jpg|right|thumb|160px|upright|alt=Cleo Laine appearing live in concert in 2007.|[[Cleo Laine]] recorded a cover of "All in Love Is Fair" in 1974.]]
AllMusic's Matthew Greenwald found "All in Love Is Fair" to be among Wonder's "finest ballad statements", which contained "one of the most graceful and memorable hooks from the era".<ref name="AllMusic Streisand review" /> Brian Ives of radio.com described it as a "beautiful and sad piano ballad" that he thought could have come from the Broadway stage.<ref name="radio.com">{{cite web |url=http://radio.com/2013/08/02/not-fade-away-stevie-wonder-innervisions/ |title=Not Fade Away: Stevie Wonder ''Innervisions'' |last=Ives |first=Brian |date=August 2, 2013 |access-date=February 11, 2017 |work=radio.com}}</ref> [[Robert Christgau]] disliked Wonder's balladry singing in "All in Love Is Fair", and considered his performance to be "immature".<ref name="The Sound of Stevie Wonder" /> In contrast, author James E. Perone wrote that there was "no better example" of a "pure, autobiographical-sounding vocal showpiece for Stevie Wonder".<ref name="The Sound of Stevie Wonder" />

After its initial release in 1973, Wonder placed "All in Love Is Fair" on several of his later albums. It was included on the [[Motown]] compilation ''Baddest Love Jams, Vol. 2: Fire & Desire'' in 1995.<ref name="Baddest Love Jams, Vol. 2: Fire & Desire">{{cite AV media notes |title=Baddest Love Jams, Vol. 2: Fire & Desire |others=Various artists |year=1995 |type=Liner notes |publisher=[[Motown]] |id= 530573}}</ref> Wonder's fourth box set, ''[[At the Close of a Century]]'' (1999), also featured the song.<ref name="At the Close of a Century">{{cite AV media notes |title=[[At the Close of a Century]] |others=Stevie Wonder |year=1999 |type=Liner notes |publisher=Motown |id= 1539922}}</ref> In that same year it was featured on ''Ballad Collection'',<ref name="Ballad Collection">{{cite AV media notes |title=Ballad Collection |others=Stevie Wonder |year=1999 |type=Liner notes |publisher=Motown |id= 1539282}}</ref> and in 2005 it was selected for inclusion on ''[[The Complete Stevie Wonder]]''.<ref name="The Complete Stevie Wonder">{{cite AV media notes |title=[[The Complete Stevie Wonder]] |others=Stevie Wonder |year=2005 |type=Liner notes |publisher=Motown |id= B00006JSS8}}</ref>

== Cover versions ==
*[[Nancy Wilson (jazz singer)|Nancy Wilson]] recorded it in 1974 for her 39th studio album, ''All in Love Is Fair''. [[AllMusic]]'s Jason Ankeny called the cover "sensual" and declared that it "wouldn't be out of place on Motown or [[Philadelphia International Records|Philadelphia International]]".<ref name="AllMusic Nancy Wilson review">{{cite news|last1=Ankeny|first1=Jason|title=Nancy Wilson – ''All in Love Is Fair''|url=http://www.allmusic.com/album/all-in-love-is-fair-mw0000842617|accessdate=January 9, 2017|publisher=[[AllMusic]]}}</ref> 
*[[Cleo Laine]] released it as a standalone 7" promotional single in 1974, enlisting the help of [[Mike Berniker]] for production.<ref name="All in Love Is Fair CL">{{cite AV media notes |title=''"All in Love Is Fair" / "All in Love Is Fair"'' |edition=Promotional |others=[[Cleo Laine]] |year=1974 |type=Liner notes |publisher=[[RCA Records|RCA]] |id=JH-10068}}</ref> 
*[[Billy Eckstine]]'s rendition was featured on ''If She Walked Into My Life'' in 1974.<ref name="If She Walked Into My Life">{{cite AV media notes |title=If She Walked Into My Life |others=[[Billy Eckstine]] |year=1974 |type=Liner notes |publisher=[[Stax Records|Enterprise]] |id= ENS-7503}}</ref>
*[[Dionne Warwick]] performed it live in 1975 and included it on her DVD concert ''Dionne Warwick: Live in Cabaret'' (2008).<ref name="Amazon Dionne Warwick">{{cite web|title=Dionne Warwick: Live in Cabaret July 18th 1975|url=https://www.amazon.com/Dionne-Warwick-Live-Cabaret-July/dp/B0011ETNRA|publisher=[[Amazon.com]] (US)|accessdate=January 31, 2017|date=February 26, 2008}}</ref>
*[[Brook Benton]] made a version for his 1976 album ''This Is Brook Benton''.<ref name="This Is Brook Benton">{{cite AV media notes |title=This Is Brook Benton |others=[[Brook Benton]] |year=1976 |type=Liner notes |publisher=[[All Platinum Records|All Platinum]] |id=AP-3015}}</ref>
*American [[Trombone|trombonist]] [[Slide Hampton]] recorded his own version of "All in Love Is Fair" for his 28th studio album, ''Spirit of the Horn'' (2002).<ref name="Multicultural Perspectives in Music Education">{{harvnb|Anderson|Campbell|2011|p=62}}</ref>

== Track listing ==
{{Track listing
| headline  = Brazil 7" single<ref name="All in Love Is Fair SW" />
| total_length = 8:27
| title1  = All in Love Is Fair
| length1  = 3:41
| title2  = Too High
| length2  = 4:36
}}

== Personnel==

*Stevie Wonder – lead vocal, piano, [[Rhodes piano|Fender Rhodes]], drums
*Scott Edwards – electric bass<ref>''Innervisions'', 1973, album cover and CD booklet</ref>

== Barbra Streisand version ==
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = All in Love Is Fair
| Cover          = File:Barbra Streisand "All in Love Is Fair".jpg
| Alt          = A red-orange vinyl record of the single appears
| Artist         = [[Barbra Streisand]]
| Album          = [[The Way We Were (Barbra Streisand album)|The Way We Were]]
| B-side         = Medley: "My Buddy"/"[[How About Me?|How About Me]]"
| Released       = March 1974
| Format         = 7"
| Recorded       = December 14, 1973; [[United Western Recorders|United Recorders]]<br><small>([[Los Angeles]], [[California]])</small>
| Genre          = 
| Length         = {{duration|m=3|s=50}}
| Label          = [[Columbia Records|Columbia]]
| Writer         = Stevie Wonder
| Producer       = [[Tommy LiPuma]]
| Last single    = "[[The Way We Were (song)|The Way We Were]]"<br/>(1973)
| This single    = "'''All in Love Is Fair'''"<br/>(1974)
| Next single    = "[[Guava Jelly (song)#Barbra Streisand version|Guava Jelly]]"<br/>(1974)
}}

=== Background and recording ===
American vocalist [[Barbra Streisand]] recorded a version of "All in Love Is Fair" for her fifteenth studio album, ''[[The Way We Were (Barbra Streisand album)|The Way We Were]]'' (1974). Shortly following the commercial success of her previous single, "[[The Way We Were (song)|The Way We Were]]", [[Columbia Records]] began compiling tracks for the singer's then-upcoming fifteenth studio album (''The Way We Were''). Since time was limited, the majority of the tracks were taken from material recorded by Streisand as much as seven years previously.<ref name="AllMusic album review">{{cite news|last1=Ruhlmann|first1=William|title=Barbra Streisand – ''The Way We Were''|url=http://www.allmusic.com/album/the-way-we-were-mw0000650840|accessdate=November 14, 2016|publisher=[[AllMusic]]}}</ref> According to the liner notes of her 1991 [[greatest hits album]] ''[[Just for the Record (Barbra Streisand album)|Just for the Record]]'', the only tracks specifically created for the album were "All in Love Is Fair", "The Way We Were", "Being at War with Each Other", and "Something So Right".<ref name="Just for the Record">{{cite AV media notes |title=[[Just for the Record (Barbra Streisand album)|Just for the Record]] |others=Barbra Streisand |year=1991 |type=Liner notes |publisher=Columbia |id=C4K-44111}}</ref> "All in Love Is Fair" was recorded on December 14, 1973, at [[United Western Recorders|United Recorders Studios]] in [[Los Angeles]].<ref name="The Way We Were liner notes">{{cite AV media notes |title=[[The Way We Were (Barbra Streisand album)|The Way We Were]] |edition=CD release |others=Barbra Streisand |year=1974 |type=Liner notes |publisher=Columbia |id=C4K 44111}}</ref> It was released in March 1974 as a [[Gramophone record|7" single]] through [[Columbia Records]],<ref name="Goldmine Standard Catalog of American Records">{{harvnb|Popoff|2010|p=1136}}</ref> and would later be paired alongside Streisand's previous single, "[[The Way We Were (song)|The Way We Were]]", on a 7" single released in 1975, also by Columbia, in the United States and Canada.<ref name="The Way We Were 1">{{cite AV media notes |title=''"The Way We Were" / "All in Love Is Fair"'' |edition=United States release |others=Barbra Streisand |year=1975 |type=Liner notes |publisher=Columbia |id=13-33262}}</ref><ref name="The Way We Were 2">{{cite AV media notes |title=''"The Way We Were" / "All in Love Is Fair"'' |edition=Canada release |others=Barbra Streisand |year=1975 |type=Liner notes |publisher=Columbia |id=13-33262}}</ref>

=== Reception ===
The staff at ''Billboard'' described Streisand's cover as a "musical gem",<ref name="Billboard review">{{cite journal|title=Billboard's Top Album Picks|journal=Billboard|date=February 9, 1974|volume=86|issue=6|page=61|url=https://books.google.com/books?id=1ycEAAAAMBAJ|accessdate=January 9, 2017|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> while author Allison J. Waldman enjoyed Streisand's personal take on it.<ref name="The Barbra Streisand Scrapbook">{{harvnb|Waldman|2001|p=54}}</ref> Greenwald from AllMusic liked the singer's cover and wrote of the song's hook, "Streisand's performance&nbsp;– particularly her phrasing of this line&nbsp;– is unforgettable".<ref name="AllMusic Streisand review">{{cite news|last1=Greenwald|first1=Matthew|title=Barbra Streisand – ''All in Love Is Fair''|url=http://www.allmusic.com/song/all-in-love-is-fair-mt0036284278|accessdate=January 10, 2017|publisher=AllMusic}}</ref> Stephen Holden from ''Rolling Stone'' compared her rendition to Wonder's version and wrote that it was "almost as interesting as the original".<ref name="The Super Seventies review">{{cite news|title=''The Way We Were'' – Barbra Streisand: Columbia PC 32801|url=http://www.superseventies.com/spstreisandbarbra.html|accessdate=February 11, 2017|publisher=The Super Seventies|date=April 11, 1974}}</ref>

Streisand's version of "All in Love Is Fair" achieved moderate commercial success in the United States and Canada. It debuted on the [[Billboard Hot 100|''Billboard'' Hot 100]] on March 30, 1974, at number 82, becoming the week's "Hot Shot Debut", or the publication's highest entry position for that particular listing.<ref name="Billboard March 30, 1974">{{cite web|title=The Hot 100 – The Week Of March 30, 1974|url=http://www.billboard.com/charts/hot-100/1974-03-30|work=Billboard |accessdate=January 9, 2017| date = March 30, 1974}}</ref> It climbed the chart for an additional three weeks before reaching its peak position on April 20 of the same year, at number 63.<ref name="Billboard April 20, 1974">{{cite web|title=The Hot 100 – The Week Of April 20, 1974|url=http://www.billboard.com/charts/hot-100/1974-04-20|work=Billboard |accessdate=January 9, 2017| date = April 20, 1974}}</ref> The following week, Streisand's rendition dropped to number 75, after which it left the Hot 100.<ref name="Billboard April 27, 1974">{{cite web|title=The Hot 100 – The Week Of April 27, 1974|url=http://www.billboard.com/charts/hot-100/1974-04-27|work=Billboard |accessdate=January 9, 2017| date = April 27, 1974}}</ref><ref name="Billboard May 4, 1974">{{cite web|title=The Hot 100 – The Week Of May 4, 1974|url=http://www.billboard.com/charts/hot-100/1974-05-04|work=Billboard |accessdate=January 9, 2017| date = May 4, 1974}}</ref> On the [[Adult Contemporary (chart)|Adult Contemporary chart]], which was then titled the Easy Listening chart, "All in Love Is Fair" peaked at number 10.<ref name="US Adult Contemporary Barbra" /> On Canada's official chart, compiled by ''[[RPM (magazine)|RPM]]'', it debuted at number 99 for the week of April 6, 1974.<ref name="Canada entry">{{cite web|title=Top RPM Singles: Issue 5000b|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.5000b&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.5000b.gif&Ecopy=nlc008388.5000b|publisher=''[[RPM (magazine)|RPM]]''. [[Library and Archives Canada]]|accessdate=January 9, 2017}}</ref> It soared twenty places the following week,<ref name="Canada soaring week">{{cite web|title=Top RPM Singles: Issue 5006b|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.5006b&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.5006b.gif&Ecopy=nlc008388.5006b|publisher=''[[RPM (magazine)|RPM]]''. [[Library and Archives Canada]]|accessdate=January 9, 2017}}</ref> and two weeks later, on April 27, it reached its peak position at number 60.<ref name="Canada Barbra" /> It also lasted a total of five consecutive weeks in this country.<ref name="Canada five weeks">{{cite web|title=Top RPM Singles: Issue 5017b|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.5017b&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.5017b.gif&Ecopy=nlc008388.5017b|publisher=''[[RPM (magazine)|RPM]]''. [[Library and Archives Canada]]|accessdate=January 9, 2017}}</ref>

=== Promotion ===
"All in Love Is Fair" has been included on numerous albums released by Streisand. Its first appearance after ''The Way We Were'' was on ''[[Barbra Streisand's Greatest Hits Volume 2]]'' (1978).<ref name="Barbra Streisand's Greatest Hits Vol. 2">{{cite AV media notes |title=[[Barbra Streisand's Greatest Hits Volume 2]] |others=Barbra Streisand |year=1978 |type=Liner notes |publisher=Columbia |id=FCA 35679 }}</ref> She also included it on ''Just for the Record'' (1991) and ''[[The Essential Barbra Streisand]]'' (2002).<ref name="Just for the Record" /> <ref name="The Essential Barbra Streisand">{{cite AV media notes |title=[[The Essential Barbra Streisand]] |others=Barbra Streisand |year=2002 |type=Liner notes |publisher=Columbia |id=C2K 86123 }}</ref>

=== Charts ===
{| class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (1974)
! scope="col"| Peak<br> position
|-
{{singlechart|Canadatopsingles|60|artist=Barbra Streisand|rowheader=true|chartid=5024b|accessdate=January 8, 2017|refname="Canada Barbra"}}
|-
{{singlechart|Billboardhot100|63|artist=Barbra Streisand|rowheader=true|accessdate=January 8, 2017|refname="US Barbra"}}
|-
{{singlechart|Billboardadultcontemporary|10|artist=Barbra Streisand|rowheader=true|accessdate=January 8, 2017|refname="US Adult Contemporary Barbra"}}
|-
|}

=== Track listings and formats ===
{{col-begin}}
{{col-2}}
;'''United States and United Kingdom 7" single'''<ref name="United States 7 single">{{cite AV media notes |title=''"All in Love Is Fair" / "[[My Buddy (song)|My Buddy]]"/"[[How About Me?|How About Me]]"'' |others=Barbra Streisand |year=1974 |type=Liner notes |publisher=[[Columbia Records|Columbia]] |id=4-46024}}</ref>
* A1 "All in Love Is Fair" &ndash; 3:50
* B1 Medley: "[[My Buddy (song)|My Buddy]]"/"[[How About Me?|How About Me]]" &ndash; 4:09
{{col-2}}
;'''United States promotional 7" single'''<ref name="United States promotional 7 single">{{cite AV media notes |title=''"All in Love Is Fair" / "All in Love Is Fair"'' |edition= Promotional |others=Barbra Streisand |year=1974 |type=Liner notes |publisher=Columbia |id=4-46024}}</ref>
* A1 "All in Love Is Fair" (Stereo) &ndash; 3:50
* B1 "All in Love Is Fair" (Mono) &ndash; 3:50
{{col-end}}

== References ==
{{Reflist|30em}}

'''Works cited'''
*{{cite book|last1=Anderson|first1=William M.|last2=Campbell|first2=Patricia Shehan|title=Multicultural Perspectives in Music Education, Volume 1|date=January 16, 2011|publisher=R&L Education |ref=harv|isbn= 1607095416 |edition=third, illustrated }}
*{{cite book|last1=Graff|first1=Gary|title=MusicHound Rock: The Essential Album Guide|date=1996|publisher=Visible Ink Press |ref=harv|isbn= 0787610372 }}
*{{cite book|last1=Jordan|first1=Herb|title=Motown in Love: Lyrics from the Golden Era|date=2006|publisher=Pantheon |ref=harv|isbn= 0375422005 }}
*{{cite book|last1=Perone|first1=James E.|title=The Sound of Stevie Wonder: His Words and Music|date=2006|publisher=Greenwood Publishing Group |ref=harv|isbn= 027598723X |edition= illustrated, annotated }}
*{{cite book|last1=Popoff|first1=Martin|title=Goldmine Standard Catalog of American Records: 1948-1991|date=August 5, 2010|publisher=Krause Publications |ref=harv|isbn= 1440216215 |edition= revised }}
*{{cite book|last1=Waldman|first1=Allison J.|title=The Barbra Streisand Scrapbook|date=2001|publisher=Citadel Press|ref=harv|isbn= 0806522186 |edition= illustrated, revised}}

== External links ==
* {{MetroLyrics song|stevie-wonder|all-in-love-is-fair}}<!-- Licensed lyrics provider -->

{{Stevie Wonder}}
{{Barbra Streisand}}

[[Category:1973 songs]]
[[Category:1974 singles]]
[[Category:Barbra Streisand songs]]
[[Category:Songs written by Stevie Wonder]]
[[Category:Stevie Wonder songs]]