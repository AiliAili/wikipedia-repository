{{Infobox University 
|name           = BYU Jerusalem Center
|image_name     = BYU JC logo.PNG
|image_size     = 140px
|established    = 1989
|type           = [[Private university]]<br />Satellite campus of [[BYU]]
|endowment      = 
|staff          =
|faculty        = 
|free_label     = Director
|free           = [[James R. Kearl]]
|dean           = 
|students       = 
|undergrad      = 
|postgrad       =  
|doctoral       = 
|alumni         = 
|city           = [[Mount of Olives]], [[Jerusalem, Israel|Jerusalem]]
|state          = 
|country        = [[Israel]]
|campus         = [[Suburban]], {{convert|5|acre|km2}}
|colors         = 
|mascot         = 
|nickname       = 
|affiliation    = [[The Church of Jesus Christ of Latter-day Saints]]
|website        = [http://ce.byu.edu/jc/ www.ce.byu.edu/jc/]
}}
{{coord|31|47|12|N|35|14|40|E|type:edu|display=title}}
The '''Brigham Young University Jerusalem Center for Near Eastern Studies''' (often simply referred to as the '''BYU Jerusalem Center''', '''BYU–Jerusalem''' or '''Mormon University'''<ref>{{Cite web|url = http://goisrael.com/Tourism_Eng/Tourist%20Information/attractions/Pages/Mormon%20University.aspx|title = Mormon University|date = |accessdate = 2015-08-03|website = |publisher = Ministry of Tourism, Government of Israel.|last = |first = }}</ref><ref>{{Cite web|title = Brigham Young University (Mormon University)|url = http://www.jerusalem.muni.il/en/SitesMuni/Sites/National/Pages/PageSite_460.aspx|website = Website of Jerusalem Municipality|accessdate = 2015-08-03}}</ref><ref>{{Cite journal|url = http://www.uvu.edu/religiousstudies/docs/mormonamerican/van_dyke_blair_a_mormon_university_on_the_mount_of_olives.pdf|title = The Mormon University on the Mount of Olives: A Case Study in LDS Public Relations|last = G. Van Dyke|first = Blair|date = 21 Nov 2013|journal = Journal of Media and Religion|doi = |pmid = |access-date = 2015-08-03|quote = " Their sole intention was to block the construction of the  Mormon University” (as the Jerusalem Center came to be known by locals)."}}</ref>), situated on [[Mount of Olives]] in [[East Jerusalem]], is a satellite campus of [[Brigham Young University]] (BYU), the largest religious university in the United States.<ref>{{cite web |url=http://www.signaturebooks.com/reviews/lords.htm |title=The Lord's University |accessdate=2008-05-01 |last=Naparsteck |first=Martin |publisher=''[[The Salt Lake Tribune]]'' |archiveurl = https://web.archive.org/web/20071213133440/http://www.signaturebooks.com/reviews/lords.htm |archivedate = December 13, 2007|deadurl=yes}}</ref> Owned by [[The Church of Jesus Christ of Latter-day Saints]] (LDS Church), the center provides a curriculum that focuses on [[Old Testament|Old]] and [[New Testament]], ancient and modern [[Near East|Near Eastern studies]], and language ([[Hebrew]] and [[Arabic]]). Classroom study is built around field trips that cover the Holy Land, and the program is open to qualifying full-time undergraduate students at either BYU, [[BYU-Idaho]], or [[BYU-Hawaii]].<ref name="generalinfo">{{cite web |url=http://ce.byu.edu/jc/info.php |title=Jerusalem Center - General Information |accessdate=2013-07-25 |publisher=[[Brigham Young University]]}}</ref>

Plans to build a center for students were announced by LDS Church [[President of the Church (LDS Church)|president]] [[Spencer W. Kimball]] in 1979. By 1984, the church had obtained a 49-year lease on the land and had begun construction. The center's prominent position on the Jerusalem skyline quickly brought it notice by the religious conservatives, or [[Haredi Jews|Haredim]], of Israel. Protests and opposition to the building of the center springing from the Haredim made the issue of building the center a national and even international issue. After several investigative committees of Israel's [[Knesset]] reviewed and debated the issue, Israeli officials decided to allow the center's construction to continue in 1986. The center opened to students in May 1988 and was dedicated by [[Howard W. Hunter]] on May 16, 1989.<ref name="Dedication">{{cite web |url=http://universe.byu.edu/2005/11/30/byu-jerusalem-center-dedicatedlds-church-delays-news-to-keep-peace/ |title=BYU Jerusalem Center dedicated-LDS Church delays news to keep peace |accessdate=2013-07-25 |date=2005-11-30 |last=Mccann |first=Jessica |publisher=''[[The Daily Universe]]''}}</ref>  It did not admit students from 2001 to 2006 due to security issues during the [[Second Intifada]] but continued to provide tours for visitors and weekly concerts.<ref>{{cite web |url=http://universe.byu.edu/2008/04/08/students-experience-religion-and-culture-at-the-jerusalem-center/ |title=Students Experience Religion and Culture at the Jerusalem Center |accessdate=2013-07-25 |last=Shaha |first=Abigail |date=2008-04-08 |work= |publisher=''[[The Daily Universe]]''}}</ref>

== History ==

=== Before the center ===
The first LDS official to enter Jerusalem was [[Quorum of the Twelve Apostles (LDS Church)|apostle]] [[Orson Hyde]], who came in 1841 and dedicated the land for the gathering of the people of Israel, the creation of a Jewish state, and the building of an LDS temple at some future time. After his visit, LDS presence in the city was virtually non-existent. By 1971, the city saw enough LDS visitors for the church to lease a building in East Jerusalem for church services.<!-- , first at the City Hotel in Jerusalem.{{cn}} and then at the Vienna Hotel in [[Sheikh Jarrah]].{{cn}} In 1978, the program relocated to dormitory facilities in [[Kibbutz]] [[Ramat Rachel]].{{cn}}--> BYU's study abroad program to Jerusalem, which began in 1968, played a key role in the growth of LDS visitors to the area. The LDS presence in the area soon grew too large for the leased space to provide adequate space for worship, so the church began looking into building a center for students.<ref name="JC">{{cite web |url= http://contentdm.lib.byu.edu/cdm/ref/collection/EoM/id/5561 |title=BYU Jerusalem Center |accessdate=2008-06-06 |author= |date= |work=Encyclopedia of Mormonism |publisher=Macmillan Publishing Company}}</ref><ref name = spatial>Olsen, Daniel H. and Jeanne Kay Guelke. "Spatial transgression and the BYU Jerusalem center controversy." ''The Professional Geographer.'' (Nov 2004) 56.4 pp. 503-516.</ref>  In 1972, [[David B. Galbraith]] became the director of BYU's program in Jerusalem.  He remained in this position until 1987 when the church's [[First Presidency (LDS Church)|First Presidency]] asked him to organize the BYU Jerusalem Center.<ref>Galbraith and Van Dyke. "The BYU Jerusalem Center: Reflections of a Modern Pioneer" in ''The Religious Educator'' Vol. 9 (2008) no. 1, p. 29ff.</ref>

On October 24, 1979, church president Spencer W. Kimball visited Jerusalem to dedicate the Orson Hyde Memorial Gardens, located on the [[Mount of Olives]].<ref name="KimballDedicatesGarden">{{cite web |url=http://www.lds.org/ensign/1979/12/news-of-the-church/president-kimball-dedicates-orson-hyde-memorial-garden-in-jerusalem |title=President Kimball Dedicates Orson Hyde Memorial Garden in Jerusalem |accessdate=2008-06-06 |author= |date=1979-12-01 |work= [[Ensign (LDS magazine)|Ensign Magazine]]|publisher=[[The Church of Jesus Christ of Latter-day Saints]]}}</ref> The church had donated money to beautify the Jerusalem area, and officials of the Jerusalem government were present at the occasion. It was at this dedication that Kimball announced the church's intent to build a center for BYU students in the city. Negotiations between the church and the Israeli government stretched from 1980-1984. The land the church wanted for the center, located at the northwestern margin of Mount Olivet, right next to the [[Emek Tzurim National Park|valley]] which separates it from [[Mount Scopus]], had been acquired by Israel during the [[Six Day War]] of 1967 and could not be sold under Israeli law. The church decided to obtain a lease on the land instead. Leasing the land also prevented the politically controversial problem of the church owning a piece of Jerusalem land. Israeli officials saw the building of the center on the land as a way of solidifying control over land whose ownership was ambiguous under international law. By August 1984, the church had the land on a 49-year lease, building permits had been obtained, and construction on the building began.<ref name ="spatial" /><ref name="KimballDedicatesGarden" /><ref name="CenterSafe">
{{cite web
 |url=http://nn.byu.edu/story.cfm/38136 
 |title=Jerusalem Center safe amid conflict 
 |accessdate=2008-06-10 
 |author=Hadfield, Joseph 
 |date=2002-04-04 
 |publisher=[[Brigham Young University|BYU]] 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20080908010356/http://nn.byu.edu/story.cfm/38136 
 |archivedate=2008-09-08 
 |df= 
}}</ref><ref>Benvenisti, M. 1996. ''City of stone: The hidden history of
Jerusalem'', trans. M. K. Nunn. London: University
of California Press. As cited in Olsen and Guelke.</ref><ref>Cohen, S. E. 1993. The politics of planting: Israeli-
Palestinian competition for control of land in the Jerusalem
periphery. Chicago: University of Chicago Press. As cited in Olsen and Guelke.</ref><ref>Benvenisti, M. 2000. Sacred landscape: The buried history of the
Holy Land since 1948. Berkeley: University of California
Press. As cited in Olsen and Guelke.</ref>

=== Construction and controversy ===
[[Image:Bet Orot.jpg|thumb|right|300px|The distinctive multiple-arched BYU Jerusalem Center (top left) amid the buildings of Jerusalem]]

The 1980s saw not only Mormons, but many Christian groups vying for representation and space in the city.<ref>Arieli, Y. S. 2000. Evangelizing the chosen people: Missions
to the Jews in America, 1880–2000. Chapel Hill:
University of North Carolina Press. As cited in Olsen and Guelke.</ref><ref>Kark, R., and M. Oren-Nordheim. 2001. Jerusalem
and its environs, quarters, neighbourhoods, and villages,
1800–1948. Detroit:Wayne State University Press. As cited in Olsen and Guelke.</ref><ref name = dumper>Dumper, M. 1997. The politics of Jerusalem since 1967.
New York: Columbia University Press. As cited in Olsen and Guelke.</ref> These groups constantly faced opposition from a strong political minority of Orthodox Jews living in the city. Neither major political party in Israel (the [[Likud]] and [[Labor Party (Israel)|Labor Parties]]) could achieve a majority vote in the [[Knesset]] without support from the more religious parties. Religious parties used this situation to pass laws in favor of Jewish Orthodoxy in exchange for their support on other issues.<ref>Kna’an, M., ed. 1986. The Israeli yearbook. Vol. 41. Tel
Aviv: Israel Yearbook Publications Ltd. As cited in Olsen and Guelke.</ref><ref>Stump, R. 2000. Boundaries of faith: Geographical perspectives
on religious fundamentalism. Lanham, MD:
Rowman & Littlefield. As cited in Olsen and Guelke.</ref> At the time, the conservative Jews, who made up the "religious right" in Israel, or the [[Haredi Judaism|Haredim]], constituted 27% of the population of Jerusalem, and was decidedly against the building of the BYU Jerusalem Center or any other similar Christian structure. Larger parties faced loss of a majority if they stood opposite on this issue. Many Israeli officials, however, such as the [[Mayor of Jerusalem]] at the time, [[Teddy Kollek]], along with others in attendance at the [[Orson Hyde Memorial Garden]] dedication, supported the center because of what the church had done for the city. Kollek specifically stated that "the Mormon church's presence in Jerusalem can do a great deal of work in providing the bridge of understanding between the Arab and Jews...because its members look with sympathy and understanding at both sides."<ref>{{cite web |url=http://newsroom.byuh.edu/node/517 |title=Former Jerusalem Center director draws parallels between Arab-Israeli and Book of Mormon conflicts |accessdate=2008-06-10 |author= Foley, Mike |date=2004-01-06 |publisher=[[Brigham Young University Hawaii|BYU Hawaii]]}}</ref> The land on which the center was built was then still considered Arab land by many, and many officials saw that its lease would add an image of religious tolerance to their government and increase Israeli control of the land.<ref name = spatial/>

Because of its prominent location in the Jerusalem skyline, construction was quickly noticed, and this sparked a major controversy in [[Israel]] and in the [[Jew|Jewish world]] as a whole beginning in 1985. The Haredim led the opposition, their main concern being that the building would be used not as a school, but as a center for Mormon proselyting efforts in Jerusalem. The Haredim warned of a "spiritual holocaust".<ref>Sittner, A. 1985. Miari blasts Knesset switch to offices
of Chief Rabbinate. The Jerusalem Post, 4 June.
n.v.:2. As cited in Olsen and Guelke.</ref> The LDS Church, they argued, had no local presence in the population of the Jerusalem area and no historical connections to the land.<ref name = dumper/> The group spread warnings through letters, newspapers, and television that [[Mormon missionaries]] would convert Jews throughout the city, saying that:<ref name = spatial/>

{{blockquote|"The Mormon organization is one of the most dangerous, and in America they have already struck down many Jews. At the present the Mormons are cautious because of the tremendous opposition their missionary activities would engender, but the moment their new Center is completed, we won’t be able to stop them." -- ''Kol Ha’Ir''<ref name = spatial/><ref>''Kol Ha’Ir'' June 8, 1984. As cited in Olsen and Guelke.</ref>}}

and that:

{{blockquote|"At the heart of the "emotional" and "bitter" controversy brewing in Jerusalem is whether [[Christian Zionism]], based on Christian eschatological expectations, should function in Israel with the help and active aid of government and municipal authorities, such as the assistance being rendered to the Brigham Young University." -- ''Inter Mountain Jewish News''<ref name = spatial/><ref>''Inter Mountain Jewish News'' January 4, 1985. As cited in Olsen and Guelke.</ref>}}
[[File:Brigham Young University Jerusalem CenterDSCN4689.JPG|thumb|left|Overlooking the [[Dome of the Rock]] from inside the center]]
Warnings in the media led to street protests and demonstrations. Orthodox Jews marched on [[Safra Square|City Hall]] and to the construction site in 1986.<ref>Golan, P. 1985. Hysteria inside, demos outside. Jerusalem
Post, 20 September. As cited in Olsen and Guelke.</ref> Some even gathered at the [[Western Wall]] in a public prayer of mourning because of the center.<ref>Shapiro, H. 1985c. Prayers said against Mormon study
centre. Jerusalem Post, 19 July. As cited in Olsen and Guelke.</ref> They also gathered at the hotel at which the BYU President was staying at one point, carrying signs saying: "Conversion is Murder!" and "Mormons, stop your mission now".<ref name = fh>Friedland, R., and R. Hecht. 1996. To rule Jerusalem.
Cambridge U.K.: Cambridge University Press. As cited in Olsen and Guelke.</ref><ref>Watzman, H. M. 1985a. Building for Brigham Young
U. in Jerusalem challenged anew. The Chronicle of
Higher Education 31 (10): 41. As cited in Olsen and Guelke.</ref> Despite the intensity of the Haredi opposition, at no point did the protests become physically violent.<ref name = "miracle" /> In late 1985, the Haredim motioned for a [[no-confidence vote]] against the leading Labor Party. Prime Minister [[Shimon Peres]] organized a committee of eight, four for the center and four against, to debate the issue and come up with a solution either for or against the center's presence. Another committee was formed to look into the allegation that the money the church had put into Jerusalem was a bribe to gain Mayor Kolleck's support for the center<ref>Shapiro, H. 1985a. Jerusalem Foundation link to
Mormons ‘‘ridiculous’’. Jerusalem Post, 20 August. As cited in Olsen and Guelke.</ref> (the committee found the church "Not Guilty"). A subcommittee of the Knesset requested that the LDS Church issue a formal promise not to proselytize Jews. Some Israelis considered this discriminatory, as no other Christian church had been asked to do this in Jerusalem. Church leaders, however, agreed to comply and sent a formally signed statement soon after.<ref>Shapiro, H. 1985b. Mormon: Proselytizers will go home.
Jerusalem Post, 7 August. As cited in Olsen and Guelke.</ref> Some Jews in the area were still uneasy and doubted the church's intent,  believing that religious belief among Mormons would supersede adherence to the law. One protestor stated that "converting the sons of [[Judah (Bible)|Judah]], us, is a basic article of their faith. . . . They regard themselves as sons of [[Joseph (Hebrew Bible)|Joseph]] and believe there will be no [[Second Coming]] for as long as we and they do not fuse."<ref name = spatial/><ref>The Economist 1985, 31. As cited in Olsen and Guelke.</ref>

In addition to the promise not to proselyte, BYU began a public relations campaign to inform the public of their intentions for the center as a school and a gathering place for those already of the LDS faith. Ads were purchased in local newspapers, magazines, and on television, and the center had personnel appear on radio talk shows. Government officials in favor of the center also began to speak out, saying that Jerusalem should deny no one a place to worship, Jew, Muslim, or Christian. The Minister for Economic
Planning, [[Gad Yaakobi]] said that the debate had "already caused considerable damage to Israel",<ref>Watzman, H. M. 1986. Israeli Cabinet agrees to investigate
Brigham Young’s Jerusalem Center. The Chronicle of
Higher Education 31 (17): 33–34. As cited in Olsen and Guelke.</ref> and Former Foreign Minister [[Abba Eban]] stated that the "free exercise of conscience and dissent in a democratic society" was at stake.<ref>''Time'' 1986, 73. As cited in Olsen and Guelke.</ref> The center also received support in the U.S., as former President [[Gerald Ford]] spoke for the center, as well the [[United Jewish Council of Utah]], who wrote a letter stating that "For over one hundred years, the Jewish and LDS communities have coexisted in the Salt Lake Valley in a spirit of true friendship and harmony. It has been our experience that when the leaders of the LDS Church make a commitment of policy, it is a commitment which can be relied upon. The stated commitment of Brigham Young University not to violate the laws of the state of Israel, or its own commitment regarding proselytizing in the state of Israel through the Jerusalem-based Brigham Young facility, is a commitment which we sincerely believe will be honored."<ref name="NewsCenterOpens">{{cite web |url=https://www.lds.org/ensign/1987/06/news-of-the-church/byus-jerusalem-center-opens |title=News of the Church: BYU’s Jerusalem Center Opens |accessdate=2008-06-10 |author= |date=1987-06-01 |work= [[Ensign (LDS magazine)|Ensign Magazine]] |publisher=[[The Church of Jesus Christ of Latter-day Saints]]}}</ref> The U.S. government also became an intermediary for BYU as 154 members of [[United States Congress|Congress]] issued a letter to the Knesset in support of the BYU Jerusalem Center. In 1986, the Knesset approved the completion of the center.<ref name = spatial/>
<!-- Protests lead to careful diplomatic negotiations between the LDS Church and Jewish leaders, to ensure that the church would not use the Center as a base to [[proselyte|proselytize]] in the Jewish State. Eventually this dispute was settled, and the Center opened to students in 1987, and the building was dedicated for Church use by in 1989.<ref name="JC"/> As part of the agreement, students are forbidden to proselytize. If a student breaks this agreement, he or she is sent home. Students also must dress conservatively out of respect for the local religions and while in Israel observe the Sabbath on Saturday. (hiding this until expansion is done. -->

=== Opening and dedication ===
Students moved into the center on May 8, 1987. The school remained unfinished, but the dormitory levels had been completed. Students had formerly been housed at [[Kibbutz]] [[Ramat Rachel]].<ref name="NewsCenterOpens" /> In 1988, before the center's dedication, a few Jerusalem locals complained that the arrangement of the windows at night looked like a Christian [[cross]]. The center purchased blinds and carefully arranged them over the windows so that no such sign would be seen. Members of the LDS Church do not use the symbol of the cross as other Christian denominations do, due to their focus on the resurrection, rather than the death, of Christ.<ref name = spatial/><ref name = dumper/>

The center was dedicated on May 16, 1989 by [[Howard W. Hunter]], the [[President of the Quorum of the Twelve Apostles (LDS Church)|President of the Quorum of the Twelve Apostles]].<ref name="Dedication"/> The dedication ceremony was small, as the church decided not to announce it until a month later. The church did not want a large ceremony to cause concern among those in opposition to the center, who may have seen it as a religious gathering. [[Thomas S. Monson]], then a second counselor in the [[First Presidency (LDS Church)|First Presidency]] of the LDS Church, and [[Boyd K. Packer]], another member of the church's Quorum of the Twelve, were among those in attendance, as well as [[List of presidents of Brigham Young University|BYU President]] [[Jeffrey R. Holland]]. Robert C. Taylor, director of the BYU Travel Study program was in attendance and stated in an interview with ''[[The Daily Universe]]'' that the dedication of the building was centered solely on the educational aspect of the school, as well as for "whatever purposes [the Lord] has in store" in the future. Taylor stated that the church would respect the laws of the land and their commitment not to proselyte.<ref name="Dedication"/>

=== Center closings ===
After the onset of the [[Second Intifada]], security for BYU students became increasingly difficult to maintain, and the center closed indefinitely to students in 2000. During the fighting, BYU sources reported that the center's staff remained on location and managed to maintain good relations on both Israeli and Palestinian sides. As negotiations to stop the fighting continued, one proposed settlement had the center placed within the borders of a proposed Palestinian state (this, however, was not the proposal ultimately agreed upon by the two sides).<ref name="CenterSafe" />  While closed to students, the center remained open for visitors and concerts.<ref>{{cite news|author=Shaha, Abigail |title=Students Experience Religion and Culture at the Jerusalem Center |date=April 8, 2008 |work=BYU NewsNet |url=http://newnewsnet.byu.edu/story.cfm/68263 |accessdate=2009-04-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20090904014542/http://newnewsnet.byu.edu:80/story.cfm/68263 |archivedate=2009-09-04 |df= }}</ref>

On June 9, 2006, officials announced their intention to reopen the Jerusalem Center for the Fall 2006 semester.  However, escalating violence in the area from the [[2006 Israel-Lebanon Conflict]] frustrated these plans and raised new concerns about students' safety in the area.  School officials deemed the center would remain closed until the conflict was resolved.<ref>{{cite web|url=http://byunews.byu.edu/archive06-Jul-nojerusalem.aspx |title=BYU students will not return to Jerusalem Center this fall |accessdate=2008-06-06 |last= |first= |coauthors= |date=2006-01-28 |work= |publisher=[[BYU]] |deadurl=yes |archiveurl=https://web.archive.org/web/20080923022722/http://byunews.byu.edu:80/archive06-Jul-nojerusalem.aspx |archivedate=2008-09-23 |df= }}</ref>  During this time, some LDS members in Northern Israel were "voluntarily relocated" into the center, away from border missile strikes.<ref>{{cite web|title=Jerusalem Center acts as refuge |author=Erb, Emilee |work=BYU NewsNet |date=July 20, 2006 |url=http://newnewsnet.byu.edu/story.cfm/60451 |accessdate=2009-04-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20090903233039/http://newnewsnet.byu.edu:80/story.cfm/60451 |archivedate=2009-09-03 |df= }}</ref>  BYU officials announced on October 9, 2006 that the center would be reopening for student academic programs for Winter Semester 2007. The initial program was limited to only 44 students.<ref>{{cite web|url=http://newnewsnet.byu.edu/story.cfm/62604 |title=Students Return to Jerusalem |accessdate=2008-06-06 |author=Preusz, Jared |date=2007-01-18 |work=BYU NewsNet |publisher=[[Brigham Young University|BYU]] |deadurl=yes |archiveurl=https://web.archive.org/web/20090904002327/http://newnewsnet.byu.edu:80/story.cfm/62604 |archivedate=2009-09-04 |df= }}</ref>  The center remains open into future academic terms.<ref>{{cite web | title=General Information | work=BYU Jerusalem Center | publisher=BYU Continuing Education | url=http://ce.byu.edu/jc/application-info.cfm | accessdate=2009-04-01}}</ref>

== Facilities and architecture ==
[[Image:Kidron jerusalem (10).JPG|thumb|right|Building seen from below]]
The center was designed in partnership with Frank Ferguson of FFKR Architects (Salt Lake City)<ref>{{citation |url= http://www.ffkr.com/#studios/HIGHER_EDUCATION/Jerusalem_Ctr |title= Jerusalem Center for Near East Studies (Jerusalem, Israel) |work= FFKR.com |accessdate= 2013-01-18 }}</ref> and by Brazilian-Israeli architect [[David Resnick]],<ref>{{citation |url= https://www.lds.org/ensign/1993/04/one-voice?lang=eng |title= One Voice |first= LaRene |last= Gaunt |date=April 1993 |journal= [[Ensign (LDS magazine)|Ensign]] }}</ref> who also designed the nearby campus of the [[Hebrew University]].  The center is situated on the western slope of the Mount of Olives, right where it connects to Mount Scopus, overlooking the [[Kidron Valley]] and the [[Old City (Jerusalem)|Old City]]. The {{convert|125000|sqft|m2}}, eight-level structure is set amid {{convert|5|acre|sqmi km2|3|abbr=on}} of gardens. The first five levels provide dormitory and apartment space for up to 170 students, each of these apartments having a patio overlooking the Old City. The sixth level houses a cafeteria, classrooms, computer facilities, and a gymnasium, while administrative and faculty offices are located on the seventh level, along with a 250-seat auditorium. The main entry is on the eighth level, which also contains a recital and special events auditorium with organ, lecture rooms, general and reserve libraries, offices, a domed theater, and a learning resource area.<ref>{{cite web |url=http://ce.byu.edu/jc/ |title=BYU JC homepage |accessdate=2008-06-06 |last= |first= |coauthors= |date= |work= |publisher=[[BYU]]}}</ref> This auditorium is surrounded by glass on three sides, providing views of the city. The organ within it is a Scandinavian-made [[Marcussen & Søn|Marcussen organ]]. The aforementioned library on the same floor as the auditorium contains 10,000-15,000 volumes focusing largely on the Near East.<ref name="miracle">{{cite web|url=http://www.brightcove.tv/title.jsp?title=1078573121&channel=240043692 |title=The Miracle of the Jerusalem Center for Near Eastern Studies |accessdate=2008-06-11 |last=Ogden |first=Kelly |coauthors= |date= |work= |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20090902222513/http://www.brightcove.tv/title.jsp?title=1078573121&channel=240043692 |archivedate=September 2, 2009 }}</ref>

The center's design reflects the architecture of the Near East. It is constructed of cast concrete. Hand-carved [[Meleke|Jerusalem limestone]] adorn the building, according to local custom.<ref name = miracle/> The use of arches and domes closely models other building of Jerusalem and the gardens throughout the center contain many trees and other plants named in the Bible. The interior contains the arches and cupolas typical of the Near East, and large, windowed pavilions provide wide views of Jerusalem.<ref name="JC"/>

Over 400 [[micropile]]s were drilled into the Mount to secure the foundation in case of an earthquake. The building also contains, in adherence to Israeli law, bomb shelters capable of holding all faculty, staff, and students in case of emergency.<ref name = miracle/>

== Research and education ==
The Jerusalem Center played a role in the research of the [[Dead Sea Scrolls]] in cooperation with the Dead Sea Scrolls Foundation of Jerusalem. They developed a comprehensive CD-ROM database of the contents of the Scrolls, enabling researchers worldwide the ability to study them.<ref>{{cite web |url=http://www.lds.org/ensign/1995/12/news-of-the-church/byu-projects-aid-dead-sea-scrolls-studies |title=News of the Church: BYU Projects Aid Dead Sea Scrolls Studies |accessdate=2008-06-10 |author= |date=1995-12-01 |work= [[Ensign (LDS magazine)|Ensign Magazine]] |publisher=[[The Church of Jesus Christ of Latter-day Saints]]}}</ref>

The center provides a curriculum that focuses on [[Old Testament|Old]] and [[New Testament]], ancient and modern [[Near East|Near Eastern studies]], and language ([[Hebrew]] and [[Arabic]]). Classroom study is built around field trips that cover the Holy Land, and the program is open only to qualifying full-time undergraduate students at either BYU, [[BYU-Idaho]], or [[BYU-Hawaii]].<ref name="generalinfo" />

The center teaches classes in four-month semesters occurring three times per year. Each semester costs $10,815. Students are required to take a small orientation course online before entering the center and are interviewed individually. Application requirements state that students must have attended at least two semesters (including the semester immediately preceding the trip abroad) at BYU, BYU-Hawaii, or BYU-Idaho, have a GPA of at least 2.5, and sign an agreement not to proselytize. Married students are not allowed to attend.<ref name="generalinfo" />

== Mission ==
[[File:Brigham Young University Jerusalem CenterDSCN4696.JPG|thumb|right|The [[pipe organ]] inside the center]]
Members of the LDS Church believe that Jesus Christ will return in glory in his Second Coming.<ref>{{cite web
|url= http://scriptures.lds.org/en/a_of_f/1/10a
|title= Tenth Article of Faith
|accessdate= 2008-06-14
|author= Joseph Smith, Jr.
|date= 
|publisher= LDS Church
}}</ref> Howard W. Hunter, who was president of the church's Quorum of the Twelve at the time of the center's construction, pointed out that although there would be no proselytizing from the center, it still served a valuable purpose. One church member quoted him this way: "Elder Hunter said that our mission was not to harvest, probably not even to plant, but to clear away a few more stones."<ref>
{{cite web
 |url=http://speeches.byu.edu/reader/reader.php?id=7931 
 |title=The Comprehending Soul: Open Minds and Hearts 
 |accessdate=2008-06-11 
 |author=Staheli, Ronald 
 |date=1996-06-18 
 |publisher=[[Brigham Young University|BYU Speeches]] 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20060901170510/http://speeches.byu.edu/reader/reader.php?id=7931 
 |archivedate=2006-09-01 
 |df= 
}}</ref> Latter-day Saints often see the center as a way for them to show local Jews what the church is about by example, rather than by proselyting. This is done by the way students and faculty at the center live their lives,<ref>{{cite web
|url= http://speeches.byu.edu/?act=viewitem&id=188
|title= Becoming You
|accessdate= 2013-07-25
|author= Edgley, Richard C.
|authorlink= Richard C. Edgley
|coauthors= 
|date= 2002-11-03
|publisher= [[Brigham Young University|BYU Speeches]]
}}</ref> as well as through the hiring of both Israeli and Palestinian workers, as an example of what can be done through cooperation.<ref>{{cite web
|url= http://speeches.byu.edu/?act=viewitem&id=184
|title= The Far and the Near
|accessdate= 2013-07-25
|author= Kaye Terry Hanson
|date= 2002-12-03
|publisher= [[Brigham Young University|BYU Speeches]]
}}</ref> During construction of the center, for example, the church hired as many as 300 workers at one time, with about 60% of them being Arab and the other 40% being Jewish. Similar cooperation continues today.<ref name = miracle/>

The center also strives to meet the goals of the BYU Mission statement, "to assist individuals in their quest for perfection and eternal life" as well as in their educational endeavors. The center aims to give students not only an educational experience by experiencing cultures and languages firsthand, but a spiritual experience by taking them to the sites of biblical events and encouraging them to live their lives in a Christian way.<ref>
{{cite web
 |url=http://speeches.byu.edu/reader/reader.php?id=7043 
 |title=Where Is the Church? 
 |accessdate=2008-06-11 
 |author=Faust, James E. 
 |authorlink=James E. Faust 
 |date=1989-09-24 
 |publisher=[[Brigham Young University|BYU Speeches]] 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20080501064111/http://speeches.byu.edu:80/reader/reader.php?id=7043 
 |archivedate=2008-05-01 
 |df= 
}}</ref><ref>
{{cite web
 |url=http://speeches.byu.edu/reader/reader.php?id=11610 
 |title=The Dream Is Ours to Fulfill 
 |accessdate=2008-06-11 
 |author=Hafen, Bruce C. 
 |authorlink=Bruce C. Hafen 
 |date=1992-08-25 
 |publisher=[[Brigham Young University|BYU Speeches]] 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20090903222712/http://speeches.byu.edu:80/reader/reader.php?id=11610 
 |archivedate=2009-09-03 
 |df= 
}}</ref>

== References ==
{{reflist|30em}}

== External links ==
{{Wikipedia books
 |1=Culture of The Church of Jesus Christ of Latter-day Saints
 |3=Historic Sites of The Church of Jesus Christ of Latter-day Saints
}}
{{Portal|LDS Church|Book of Mormon}}
{{commons category|Brigham Young University Jerusalem Center}}
* [http://ce.byu.edu/jc/video.php BYU Jerusalem Center Hosting Video]
* [http://www.jerusalemstudyabroad.com/ BYU Jerusalem Study Abroad Winter Semester 1984 website]
* [http://jerusalem.nielsonpi.com/ BYU Jerusalem Study Abroad Winter Semester 1978 group website]
* [http://www.deadseascrollsfoundation.com/index.html The Dead Sea Scrolls Foundation]
* [http://www.brightcove.tv/title.jsp?title=1078573121&channel=240043692 The Miracle of the Jerusalem Center for Near Eastern Studies] - Slideshow of the construction of the center and protests.
{{Clear}}
{{Brigham Young University}}
{{LDSsites}}
{{good article}}

←

[[Category:Mount of Olives]]
[[Category:Education in Jerusalem]]
[[Category:Significant places in Mormonism]]
[[Category:Brigham Young University|Jerusalem Center]]
[[Category:Buildings and structures in Jerusalem]]
[[Category:Christianity in Jerusalem]]
[[Category:The Church of Jesus Christ of Latter-day Saints in Israel]]
[[Category:Educational institutions established in 1989]]
[[Category:Mormon studies|University, Brigham Young]]
[[Category:Religious buildings completed in 1989]]
[[Category:1989 establishments in Israel]]
[[Category:Organizations based in Jerusalem]]
[[Category:Mormonism-related controversies]]