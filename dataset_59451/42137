<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= SA 360 Dauphin 
 |image= File:D-HOPQ (2872896222).jpg
 |caption= Aérospatiale Dauphin of the German [[Landespolizei|police]]
}}{{Infobox Aircraft Type
 |type= utility helicopter
 |national origin=France
 |manufacturer= [[Aérospatiale]]
 |designer=
 |first flight= 2 June [[1972 in aviation|1972]]
 |introduced= 1976
 |retired=
 |status=
 |primary user= 
 |more users=
 |produced= 1976-77
 |number built=2 prototypes + 34 production examples
 |unit cost=
 |developed from = 
 |variants with their own articles=
 |developed into = [[Eurocopter AS365 Dauphin]] 
}}
|}

The '''Aérospatiale SA 360 Dauphin''' was a single-engine French utility helicopter developed as a replacement for Aérospatiale's [[Aérospatiale Alouette III|Alouette III]] in the early 1970s and to fill a gap in the company's product line in the six to ten-seat helicopter category. However, as the new helicopter offered little advantage over its predecessor and thus limited market appeal, production of the SA 360 Dauphin was abandoned after only a few dozen of them had been built.

Aérospatiale had also developed a twin-engine derivative, the [[Eurocopter AS365 Dauphin|Dauphin 2]], which proved to be quite successful and has been in production for nearly 40 years. After the merger of Aérospatiale's helicopter division into [[Eurocopter]] in [[1992 in aviation|1992]], the Dauphin 2 designation was dropped, and Eurocopter-built Dauphin 2s are simply referred to as "Dauphin". The [[retronym]] "'''Dauphin 1'''" is sometimes applied to the original Dauphin.

==Design and development==
[[File:Meravo 04.JPG|right|thumb|Aerospatiale SA 360]]

The Dauphin design was originally derived from the Alouette III it was intended to succeed, and in fact used the rotor blades from that aircraft.<ref name="Simpson_RW_p16">{{cite book |last= Simpson |first= R. W. |title=Airlife's Helicopters and Rotorcraft |year=1998 |publisher=Airlife Publishing |location=Ramsbury |pages=16 }}</ref> Power was provided by a 730&nbsp;kW (980&nbsp;hp) [[Turbomeca Astazou]] XVI turboshaft, in place of later Alouette IIIs' Astazou XIV. The aircraft featured a fully enclosed cabin with seating for up to nine passengers, a four-bladed main rotor and a thirteen-bladed [[fenestron]] in the tail. It was fitted with fixed, tailwheel undercarriage with spatted mainwheels. The first of two prototypes (registration ''F-WSQL'') made its first of 180 test flights in this configuration on 2 June [[1972 in aviation|1972]].<ref name="World Aircraft Info">{{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing |location=London |pages=File 889 Sheet 25}}</ref><ref name="G_Apostolo">{{cite book |last= Apostolo |first= Giorgio |title=The Illustrated Encyclopedia of Helicopters |year=1984 |publisher=Bonanza |location=New York |pages= }}</ref>

Following this initial evaluation, a number of modifications were incorporated into the design. These included increasing engine power by use of an Astazou XVIIIA of 780&nbsp;kW (1,050&nbsp;hp), and replacement of the original rotor blades with plastic blades. Aérospatiale engineers hoped to reduce vibration and ground resonance.<ref name="G_Apostolo"/> Thus modified, test flights resumed in May [[1973 in aviation|1973]], in time to present the new aircraft at that year's [[Paris Air Show]]. In the meantime, a second prototype (registration ''F-WSQX'') joined the test programme, flying first on 29 January.<ref name="World Aircraft Info"/> At the show, the first prototype broke three world airspeed records for helicopters in the 1,750&nbsp;kg - 3,000&nbsp;kg class ([[Fédération Aéronautique Internationale|FAI]] class E-1d). Piloted by Roland Coffignot and carrying a dummy payload to represent eight passengers, it broke the 100&nbsp;km closed-circuit (299&nbsp;km/h, 186&nbsp;mph), 3&nbsp;km straight-course (312&nbsp;km/h, 195&nbsp;mph), and 15&nbsp;km straight-course (303&nbsp;km/h, 189&nbsp;mph) records.<ref name="G_Apostolo"/>

Series production of the definitive  '''SA 360C''' version began in 1974,<ref name="G_Apostolo"/> with the first completed aircraft flying in April 1975.<ref name="airliners">{{cite web | title =airliners.net | url = http://www.airliners.net/info/stats.main?id=11 | accessdate = 2007-04-28}}</ref> French civil certification was obtained in December that year,<ref name="World Aircraft Info"/> and deliveries to customers commenced in January 1976.<ref name="World Aircraft Info"/> In the meantime, Aérospatiale had flown the prototype Dauphin 2 nearly a year earlier, on 24 January 1975,<ref name="World Aircraft Info"/> which would ultimately prove the death-knell for the Dauphin. A helicopter of this size powered by only a single engine was perceived in the marketplace as something of an anomaly and rather under-powered, meaning that by the end of 1976, Aérospatiale was left with 15 airframes - almost half those produced to date - with no buyers.<ref name="G_Apostolo"/> Accordingly, production was terminated the following year.<ref name="Lenton_L">{{cite web|last=Lenton |first=Lenny |title=Copter Crazy |url=http://members.tripod.com/coptercrazy/production/aero/360/3601.htm |accessdate=2007-04-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20040822123257/http://members.tripod.com/coptercrazy/production/aero/360/3601.htm |archivedate=August 22, 2004 }}</ref>

A single airframe (Construction Number 1012, registration ''F-WZAK'').<ref name="Lenton_L"/> was modified by Aérospatiale from standard SA 360C configuration to develop a version optimised for hot-and-high conditions, designated '''SA 360H'''. The main differences were the use of a 1,040&nbsp;kW (1,400&nbsp;hp) Astazou XXB engine and the [[Starflex]] rotorhead that had been developed for the [[Aérospatiale Ecureuil|Ecureuil]].<ref name="World Aircraft Info"/> Deciding that the main customers for this more powerful aircraft were likely to be military ones, the aircraft was further modified and re-designated '''SA 360HCL''' (''Helicoptere de Combat Leger'' - "Light Combat Helicopter").<ref name="G_Apostolo"/>  It was fitted with a [[SFIM APX M397]] roof-mounted, gyro-stabilised sight, and a nose-mounted sensor package incorporating a [[SFIM Vénus]] night-vision system and [[TRT Hector]] thermal-vision system.<ref name="airwar.ru">{{cite web | title =airwar.ru | url = http://www.airwar.ru/enc/uh/sa360.html | accessdate = 2007-04-28}}</ref> Armament consisted of eight launcher tubes for [[Euromissile HOT]] missiles, with options to carry most of the armament packages used by the [[Aérospatiale Gazelle|Gazelle]].<ref name="G_Apostolo"/> So equipped, it could carry thirteen combat-ready troops into battle, or be used in the area neutralisation or anti-tank role.<ref>{{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=36 }}</ref> This aircraft was taken on by the ''[[Armée de Terre]]'' for evaluation, but no production order ensued.<ref name="World Aircraft Info"/>
<!-- ==Operational history== -->

==Variants==
* '''SA 360''' - two prototypes
* '''SA 360C''' - standard production version, 34 built
* '''SA 360A''' - navalised version for [[Aeronavale]], 1 converted from SA 360C.<ref name="Simpson_RW_p25">{{cite book |last= Simpson |first= R. W. |title=Airlife's Helicopters and Rotorcraft |year=1998 |publisher=Airlife Publishing |location=Ramsbury |pages=25 }}</ref>
* '''SA 361H''' - "hot and high" version with more powerful (969&nbsp;kW (1,300&nbsp;shp)) Astazou XX engine, glassfibre rotor blades and new rotor hub. Three converted from SA 360 and 360C.<ref name="AI Jul95 p15">Francillon and McKenzie ''Air International'' July 1995, p. 15.</ref>
** '''SA 361HCL''' - militarised version, 1 converted from SA 361H.<ref name="AI Jul95 p15"/>

==Operators==
[[Image:RHKAAF Aerospatiale Dauphin 1982.JPG|thumb|right|RHKAAF SA 360 Dauphin at Kai Tak in 1982]]

;{{HKG}} 
*[[Royal Hong Kong Auxiliary Air Force]]<ref>{{cite web|url= http://www.flightglobal.com/pdfarchive/view/1981/1981%20-%202518.html?search=Royal%20Hong%20Kong%20Auxiliary%20Air%20Force |title=World Air Forces 1981 |publisher=flightglobal.com |date=|accessdate=15 February 2014}}</ref>
;{{USA}} 
* [[New York Helicopter]]<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1981/1981%20-%200007.html?search=New%20York%20Helicopters |title= New York airports helo link set to go |publisher= flightglobal.com |date= 3 January 1981 |accessdate=15 February 2014}}</ref>

==Notable accidents==

* One Dauphin (Construction number 1028, Registration ''RP-C220'') was destroyed in an accident while filming ''[[Delta Force 2: The Colombian Connection]]''.<ref name="Lenton_L"/> Four members of the film crew were killed, in addition to the pilot.
* One SA 360C (Construction number 1006, Registration ''N49505'') crashed in the water during take-off from Laguardia Airport, NY, due to loss of engine power on 04/26/1985. Seven persons managed to evacuate the helicopter as it rolled over and sank, but one passenger was trapped and drowned with his seat belts still fastened.<ref>{{cite web | title = National Transportation Safety Board Accident Report DCA85AA020 | url = http://www.ntsb.gov/ntsb/brief.asp?ev_id=20001214X36145&key=1 | accessdate = 2009-11-05 }}</ref>

==Specifications (SA 360C Dauphin)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=copter
|jet or prop?=prop

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully formatted line beginning with * 
-->
|ref= To the throne...on its third try<ref name="AE Jul95 p16">Francillon and McKenzie ''Air International'' July 1995, p. 16.</ref>
|crew=1 or 2 pilots
|capacity=8-9 passengers
|length main= 13.20 m<ref>(overall length), 10.98 m (36 ft 0⅓ in) (fuselage length)</ref>
|length alt=43 ft 3¾ in
|span main= 11.50 m
|span alt= 37 ft 8¾ in
|height main= 3.50 m
|height alt= 11 ft 5¾ in
|area main= 103.9 m²
|area alt= 1,118 ft²
|empty weight main=1,580 kg
|empty weight alt=3,483 lb
|loaded weight main= 
|loaded weight alt= 
|max takeoff weight main=3,000 kg
|max takeoff weight alt=6,614 lb
|engine (prop)= [[Turbomeca Astazou]] XVIIIA [[turboshaft]]
|number of props=1
|power main= 783 kW
|power alt= 1,050 shp
|never exceed speed main= 315 km/h
|never exceed speed alt=170 knots, 197 mph
|cruise speed main=274 km/h
|cruise speed alt=148 knots, 170 mph
|range main= 675 km
|range alt= 365 nmi, 419 mi
|range more= (max fuel)
|ceiling main= 4,600 m
|ceiling alt=15,100 ft
|climb rate main= 9.0 m/s
|climb rate alt=1,770 ft/min
|armament=
}}

==See also==
{{Portal|Aviation|France}}
{{aircontent
|related= 
* [[Eurocopter Dauphin]]
* [[HH-65 Dolphin]] 
* [[Eurocopter AS365]]
* [[Harbin Z-9]]
|similar aircraft=
* [[AgustaWestland AW119]]
|lists=
* [[List of helicopters]]
* [[List of utility helicopters]]
|see also=
}}

==References==
{{reflist|1}}

* Francillon, René J. and Carol A McKenzie. "To the throne...on its third try: Dauphin, eldest son of King Alouette III". ''[[Air International]]'', July 1995, Vol 49 No 1. pp.&nbsp;14–19. ISSN 0306-5634.

==External links==
{{Commons category|Aérospatiale SA 360 Dauphin}}
* [http://celag.free.fr/museum/sa360/us_sa360_0.htm NYH]

{{Sud/Aérospatiale aircraft}}

{{DEFAULTSORT:Aerospatiale Sa 360}}
[[Category:French civil utility aircraft 1970–1979]]
[[Category:French military utility aircraft 1970–1979]]
[[Category:French helicopters 1970–1979]]
[[Category:Aérospatiale aircraft]]

[[de:Eurocopter Dauphin#SA 360]]