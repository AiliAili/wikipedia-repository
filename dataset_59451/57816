{{Infobox journal
| title = JAMA Oncology
| cover =
| editor = Mary L. (Nora) Disis
| discipline = [[Oncology]]
| former_names = 
| abbreviation = JAMA Onc.
| publisher = [[American Medical Association]]
| country =
| frequency = Monthly
| history = 2015-present
| openaccess =
| license =
| impact = 
| impact-year = 
| website = http://oncology.jamanetwork.com/journal.aspx
| link1 = http://oncology.jamanetwork.com/issue.aspx
| link1-name = Online access
| link2 = http://oncology.jamanetwork.com/issues.aspx
| link2-name = Online archive
| JSTOR =
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 2374-2437
| eISSN = 2374-2445
}}
'''''JAMA Oncology''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] published by the [[American Medical Association]], with a focus on [[cancer]]. The scope of this journal covers all aspects of [[medical oncology]], [[radiation oncology]], and [[surgical oncology]] as well as subspecialties; a full list of included topics is provided on the [http://oncology.jamanetwork.com/public/About.aspx JAMA Oncology website].<ref name=":0">{{Cite web|url = http://oncology.jamanetwork.com/public/About.aspx|title = About JAMA Oncology|date = |accessdate = December 10, 2015|website = JAMA Oncology|publisher = |last = |first = }}</ref>

== History ==
Publication of JAMA Oncology began in 2015, with the first issue (Vol. 1, No. 1) appearing in April and is published on a monthly basis. The first and current editor is Mary L. (Nora) Disis, MD. JAMA Oncology is part of the extensive JAMA network of publications, which includes the following titles:<ref name=":0" />

[[JAMA (journal)|JAMA]]

JAMA Cardiology

[[JAMA Dermatology]]

[[JAMA Facial Plastic Surgery]]

[[JAMA Internal Medicine]]

[[JAMA Neurology]]

[[JAMA Ophthalmology]]

[[JAMA Otolaryngology—Head and Neck Surgery]]

[[JAMA Pediatrics]]

[[JAMA Psychiatry]]

[[JAMA Surgery]]

Archives of Neurology and Psychiatry

JAMAevidence

Evidence-based Medicine: An Oral History

JAMA Network webcasts

The JAMA Report

== Indexing and Abstracting ==
The journal is abstracted and indexed in [[Index Medicus]], [[MEDLINE]], and [[PubMed|Pubmed.]]<ref>{{Cite web|url = http://www.ncbi.nlm.nih.gov/nlmcatalog/?term=JAMA+oncology|title = JAMA Oncology|date = |accessdate = December 10, 2015|website = NLM Catalog|publisher = National Library of Medicine|last = |first = }}</ref>

== See also ==
* [[List of American Medical Association journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://oncology.jamanetwork.com/}}
*
* [http://www.ama-assn.org/ama American Medical Association]

[[Category:Oncology journals]]
[[Category:Publications established in 2015]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:American Medical Association academic journals]]


{{med-journal-stub}}