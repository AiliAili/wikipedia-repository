{{Multiple issues|
{{notability|date=April 2009}}
{{primary sources|date=April 2009}}
{{refimprove|date=April 2009}}
}}

{{Infobox Journal
| cover	=	
| discipline	=	[[Economics]]
| abbreviation	=	BPEA
| publisher	=	Brookings Press 
| editors       =       [[Janice Eberly]]
                        [[James H. Stock]]
| country	=	[[United States|USA]]
| frequency	=	semiannually
| history	=	1970–present
| openaccess	=	
| website	=	http://www.brookings.edu/economics/bpea.aspx
| ISSN          =       0007-2303
}}

The '''''Brookings Papers on Economic Activity'' (''BPEA'')'''  is a journal of macroeconomics published twice a year by the [[Brookings Institution Press]].<ref>[http://muse.jhu.edu/journals/eca/ Journal Information from Project MUSE]</ref> BPEA commenced publication in 1970 under the editorship of [[Arthur Okun]] and [[George Perry (American economist)|George Perry]], and since then has been ranked as a leading journal in the field.<ref>[http://ideas.repec.org/top/top.journals.simple.html IDEAS Ratings, RePEc]</ref> The journal was edited by [[David Romer]] and [[Justin Wolfers]] from 2009 through 2015.<ref>[http://www.brookings.edu/media/NewsReleases/2009/0318_bpea.aspx Press Release: Brookings Appoints Romer, Wolfers as New Editors for Brookings Papers on Economic Activity]</ref> [[Janice Eberly]] and [[James H. Stock]] assumed the editorship in 2016.<ref>[http://www.brookings.edu/about/media-relations/news-releases/2015/0911-eberly-stock-new-bpea-editors News Release: Brookings appoints Eberly and Stock as new editors of the Brookings Papers on Economic Activity ]</ref>

Each issue of the journal comprises the proceedings of a conference held in at the [[Brookings Institution]] in [[Washington D.C.]] each spring and fall. The conference and journal both focus on the empirical analysis of issues of current relevance to [[macroeconomic policy]] with particular attention given to "recent and current economic developments that are directly relevant to the contemporary scene or especially challenging because they stretch our understanding of economic theory or previous empirical findings".<ref>[http://www.jstor.org/journal/broopapeeconacti Journal Information from JSTOR]</ref> Each issue typically publishes six full-length papers, along with two comments on each paper and a summary of the general discussion from the conference.

==Nobel Prize–Winning Authors==
Sixteen winners of the [[Nobel Memorial Prize in Economic Sciences|Nobel Prize in Economics]] have contributed as authors or discussants for ''BPEA'' since its inception in 1970, including: [[Robert J. Shiller]], [[Thomas J. Sargent]], [[Christopher A. Sims]], [[Peter Diamond|Peter A. Diamond]], [[Oliver E. Williamson]], [[Paul Krugman]], [[Edmund Phelps|Edmund S. Phelps]], [[George Akerlof]], [[Joseph Stiglitz]], [[Daniel McFadden]], [[Gary Becker|Gary S. Becker]], [[Robert Solow]], [[Franco Modigliani]], [[George Stigler]], [[James Tobin]], and [[Lawrence Klein]].<ref>[http://www.brookings.edu/about/projects/bpea/nobel-winning-authors Nobel Prize–Winning Authors]</ref>

==''Brookings Papers on Economic Activity: Microeconomics''==
From 1989 to 1998, ''BPEA'' published an annual issue dedicated to microeconomics. Papers in these volumes focused on issues of economic performance that confronted public policymakers and executives in the private sector.<ref>[http://www.brookings.edu/about/projects/bpea/past-editions Past BPEA Editions]</ref>

==References==
<references/>

== External links ==
*[http://www.brookings.edu/economics/bpea.aspx Brookings Papers on Economic Activity]

[[Category:Economics journals]]


{{econ-journal-stub}}