{{Infobox person
|occupation=computer scientist and university administrator
|alma_mater=University of Paris XI}}
'''Brigitte Plateau''' is a French computer scientist. A former student of the [[École normale supérieure de lettres et sciences humaines|École Normale Supérieure at Fontenay-aux-Roses]] majoring in Mathematics (option in Probability Theory), she is a Doctor of Information Studies. A University Professor at [[Grenoble Institute of Technology]] (Grenoble INP) since 1988, since February 2012 she has been the General Administrator of the Grenoble INP cluster.<ref name=govtPlateaupresidentAllistene>{{cite web|url=http://www.enseignementsup-recherche.gouv.fr/cid83633/brigitte-plateau-nommee-presidente-d-allistene.html|title=Brigitte Plateau, nommée présidente d'Allistene|publisher=}}</ref><ref>{{cite web|url=http://www.grenoble-inp.fr/grenoble-inp/brigitte-plateau-elue-administrateur-general-du-groupe-grenoble-inp-446273.kjsp|title=Grenoble INP - Brigitte Plateau élue Administratrice générale de Grenoble INP|author=Grenoble INP|work=Grenoble INP}}</ref>

Plateau is president of the AFDESRI (Association of Women Leaders of Higher Education, Research and Innovation) since September 2014<ref>{{cite web|url=http://www.grenoble-inp.fr/grenoble-inp/creation-de-l-association-pour-les-femmes-dirigeantes-de-l-enseignement-superieur-de-la-recherche-et-de-l-innovation-635304.kjsp|title=Grenoble INP - Création de l’Association pour les femmes dirigeantes de l’enseignement supérieur, de la recherche et de l’innovation|author=Grenoble INP|work=Grenoble INP}}</ref> and [[Allistene]] (Alliance of the Digital Sciences and Technologies) since November 2014.

==Childhood==
Born to a father who was an engineer and a mother who was a teacher,<ref>[https://www.whoswho.fr/bio/brigitte-plateau_71689 Brigitte Plateau] at [[Who's Who in France]] {{subscription required|via=[[Factiva]]}}</ref> as a child of 7 or 8, Plateau enjoyed solving maths problems.<ref name=lefigchef/>

==Career==

At the age of 18, Plateau had wanted to become a medical doctor,<ref>{{cite web|url=http://www.usinenouvelle.com/article/une-femme-pour-un-billet-de-banque-reclame-brigitte-plateau.N239297|title="Une femme pour un billet de banque !", réclame Brigitte Plateau|author=Cécile Maillard|date=16 February 2014|work=[[L'Usine nouvelle|usinenouvelle.com]]}}</ref> but her father advised against it. After studying at the École Normale Supérieure and {{Interlanguage link multi|Aggregation of Mathematics|fr|3=Agrégation_de_mathématiques}}, Brigitte Plateau submitted in 1980 a [[Master_of_Advanced_Studies|postgraduate thesis (DEA)]] in computer science at the [[Université Paris-Sud|University of Paris XI]] and in 1984, a state computer thesis.<ref name=lefigchef>{{cite web|url=http://www.lefigaro.fr/mon-figaro/2014/04/02/10001-20140402ARTFIG00315-brigitte-plateau-ingenieur-en-chef.php|title=Brigitte Plateau, ingénieur en chef|work=[[Le Figaro]]}}</ref> She became [[:fr:Chercheur des établissements publics scientifiques et technologiques français|primarily responsible for research]]<sup>(fr)</sup> at the [[Centre national de la recherche scientifique]] (CNRS), then teaching as a [[visiting scholar]] at the [[University of Maryland]] in the United States.<ref name=lefigchef/> In 1988, she was appointed professor of universities in Grenoble Polytechnic Institute, assigned to [[Ensimag]] and to the laboratory of Computer Engineering. In 1999, she created the IT and distribution Laboratory (Grenoble INP-[[Joseph Fourier University|UJF]]-CNRS) that she ran until 2004. In January 2007 Plateau created the [[Laboratoire d'Informatique de Grenoble|Grenoble Informatics Laboratory]]<ref name="liglab.fr">{{cite web|url=https://www.liglab.fr/evenements/keynote-speeches/brigitte-plateau-modelisation-du-parallelisme-et-la-synchronisation|title=Brigitte Plateau - Modélisation du parallélisme et de la synchronisation|publisher=}}</ref> associated with the [[French Institute for Research in Computer Science and Automation]] (INRIA).  As of 2014, Plateau was in charge of 500 computer scientists at the laboratory.<ref name=lefigchef/> Her research work is on the performance of computer systems, in particular [[Distributed computing|distributed]] and [[Parallel computing|parallel systems]]. She is studying queueing [[Data modeling|models]], [[distributed algorithm]]s and [[Massively parallel (computing)|massively parallel computers]] (by simulation and observation). She is an expert in high speed calculations using massive parallelism.

In 2010, Brigitte Plateau became director of the [[Ensimag]] school in Grenoble-INP.<ref>{{cite web|url=http://mescal.imag.fr/membres/brigitte.plateau/|title=Brigitte Plateau|publisher=}}</ref> In February 2012, she was elected director of the group Grenoble-INP, the first woman to have this position.<ref>{{cite av media|url=https://www.youtube.com/watch?v=HAwZmd-b5OI|title=Brigitte Plateau, administrateur générale de Grenoble INP, sur Télé Grenoble - 01 03 2012|date=26 March 2012|publisher=|via=YouTube}}</ref><ref>{{cite web|url=http://www.lesechos.fr/11/10/2012/LesEchos/21289-507-ECH_une-femme-a-la-tete-de-grenoble-inp.htm|title=Une femme à la tête de Grenoble INP|date=11 October 2012|work=[[Les Échos (newspaper)]]}}</ref> She was re-elected for a term of 4 years in February 2016.

In November 2014, she became president of the AFDESRI whose goal is to fight against the [[glass ceiling]] that affects women in the academic field.<ref>{{cite web|url=http://etudiant.lefigaro.fr/les-news/actu/detail/article/universites-seules-11-des-femmes-a-la-tete-des-facs-francaises-9848/|title=Universités : seules 11% de femmes à la tête des facs françaises|author=Par  Amélie Petitdemange|work=[[:fr:Le Figaro Étudiant]]}}</ref> She also directs the Allistene, making her the first woman president of a research alliance.<ref>{{cite web|url=https://www.allistene.fr/brigitte-plateau-nouvelle-presidente-dallistene/|title=Brigitte Plateau, nouvelle présidente d’Allistene - Allistene|publisher=}}</ref><ref name=govtPlateaupresidentAllistene/>

She has participated in the national scientific bodies CNRS, ANR, INRIA, and the [[Ministry of Higher Education and Research]] (MESR).

== Awards and distinctions==
* 2011: Knight of the [[Legion of Honour]]<ref>{{cite web|url=http://www.letudiant.fr/educpros/personnalites/plateau-brigitte-783.html|title=Brigitte PLATEAU: biographie et actualités sur EducPros|publisher=}}</ref><ref>{{cite web|url=https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022496905|title=Décret du 13 juillet 2010  portant promotion et nomination - Legifrance|publisher=|accessdate=25 April 2016}}</ref>
* 2012: Grand Prize of the [[Airbus Group|EADS]] Corporate Foundation (IT) of the [[French Academy of Sciences]]<ref name="liglab.fr"/>
* 2015: Officer of the [[National Order of Merit (France)|National Order of Merit]]<ref>{{cite web|url=http://www.genevieve-fioraso.com/2015/06/12/hommage-a-une-grande-dame-de-linformatique-la-scientifique-brigitte-plateau/|title=Hommage à une grande dame de l’informatique, la scientifique Brigitte Plateau|publisher=}}</ref><ref>http://www.isere.gouv.fr/content/download/20871/141633/file/ONM%20-%2015%20NOV%202014.pdf</ref>

==Selected publications==
*Plateau, Brigitte. "On the stochastic structure of parallelism and synchronization models for distributed algorithms." ACM SIGMETRICS Performance Evaluation Review. Vol. 13. No. 2. ACM, 1985.
*Plateau, Brigitte, and Karim Atif. "Stochastic automata network of modeling parallel systems." Software Engineering, IEEE Transactions on 17.10 (1991): 1093-1108.
*Fernandes, Paulo, Brigitte Plateau, and William J. Stewart. "Efficient descriptor-vector multiplications in stochastic automata networks." Journal of the ACM (JACM) 45.3 (1998): 381-414.
*Plateau, Brigitte, and Jean-Michel Fourneau. "A methodology for solving Markov models of parallel systems." Journal of parallel and distributed computing 12.4 (1991): 370-387.
*Baccelli, François, Erol Gelenbe, and Brigitte Plateau. "An end-to-end approach to the resequencing problem." Journal of the ACM (JACM) 31.3 (1984): 474-485.

==References==
{{reflist}}

==Further reading==
*{{cite news|title=BRIGITTE PLATEAU, mathématicienne connectée|work=La Tribune - Acteurs de l’économie|date=1 October 2014|language=French}} {{subscription required|via=[[Factiva]]}}


{{DEFAULTSORT:Plateau, Brigitte}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:French computer scientists]]
[[Category:Women computer scientists]]