{{featured article}}
{{Infobox film
| name        = St. Elmo
| image       = St Elmo 1914 film poster.jpg
| caption     = Theatrical release poster
| starring    = {{Plainlist|
* William Jossey
}}
| based on    = {{based on|[[St. Elmo (novel)|''St. Elmo'']]|[[Augusta Jane Evans]]}}
| director    = {{Plainlist|
* [[Bertram Bracken]] and/or
* [[J. Gordon Edwards]]
}}
| producer    = [[William Fox (producer)|William Fox]]
| writer      = William Jossey
| studio      = [[Balboa Amusement Producing Company]]
| distributor = [[Box Office Attractions Company]]
| released    = {{film date|1914|8}}
| country     = United States
| runtime     = 6 reels
| language    = [[Silent film|Silent]] (English [[intertitle]]s)
}}
'''''St. Elmo''''' is a 1914 American [[silent film|silent]] [[drama film]] produced by the [[Balboa Amusement Producing Company]] and distributed by [[William Fox (producer)|William Fox]]'s [[Box Office Attractions Company]]. It was the first [[feature-length]] film adaptation of [[Augusta Jane Evans]]'s 1866 [[St. Elmo (novel)|novel of the same name]]. The story follows the life of the title character (played by William Jossey), who kills his cousin ([[Francis McDonald]]) over the love of Agnes (Madeline Pardee), falls from grace, and eventually finds redemption and love with Edna ([[Gypsy Abbott]]). It is disputed who directed the film; many sources credit [[Bertram Bracken]], while others list ''St. Elmo'' as [[J. Gordon Edwards]]'s directorial debut.

Some reviewers praised the scenery and overall production quality, considering the film an improvement over staged adaptations of the novels. Others found the scenery irrelevant and the story confusing. Despite mixed reviews, the film was financially successful, reportedly setting box office records. The following year, a film adaptation of an unrelated Evans novel, ''Beulah'', was marketed as a sequel. As with most Balboa films, ''St. Elmo'' is now believed [[lost film|lost]].

==Plot==
St. Elmo Murray and Murray Hammond, his cousin and best friend, are both in love with a young woman named Agnes Hunt. Although Agnes loves Murray, she rejects him for being too poor. Instead, she accepts the wealthy St. Elmo's marriage proposal. St. Elmo's mother holds a ball to celebrate the engagement. After the betrothal is publicly announced, Murray and Agnes meet covertly in the gardens. St. Elmo discovers their affair, challenges Murray to a duel, and kills his cousin with the first shot. The Devil possesses St. Elmo, and he becomes a cruel wanderer, spreading misery and misfortune where he travels.

Twenty years later, the [[Ingenue (stock character)|ingenue]] Edna Earle is traveling by train, hoping to find employment in a cotton mill after the death of her father, the village blacksmith. The train derails, and St. Elmo saves her from the burning wreckage. This act sets St. Elmo on the path to salvation as he and Edna slowly fall in love. Initially planning to depart for distant lands, a vision of Christ leads him to aid the impoverished. Freed from the Devil, he becomes a minister and marries Edna.<ref name=AFI /><ref name=MPN70 /><ref name=Trenton />

==Cast==
* William Jossey as St. Elmo Murray
* [[Mollie McConnell]] as Mrs. Murray
* Madeline Pardee as Agnes Hunt
* [[Francis McDonald]] as Murray Hammond
* [[Gypsy Abbott]] as Edna Earle
* Henry Stanley as Parson Hammond
* Richard Johnson (credited as Dick Johnson) as Mr. Grady
* [[Gus Leonard]] (credited as Pop Leonard) as Gabe
* [[Eulalie Jensen]] (credited as Miss Jensen) as Hagar
* Fred Whitman as Dent
* [[Frank Erlanger]] as Clinton{{sfn|Jura|Bardin|2007|p=251}}

==Production==
[[File:Elmo kills Murray.JPG|thumb|St. Elmo (Jossey) has killed his cousin (McDonald) as others look on. This scene was censored for Chicago showings.|alt=A man kneels over a dead body as a crowd watches.]]
[[Augusta Jane Evans]]'s 1866 [[domestic novel]] ''St. Elmo'' was one of the best-selling novels of the 19th century, surpassed at the time only by ''[[Uncle Tom's Cabin]]'',<ref name=Maurice/> and later by ''[[Ben-Hur: A Tale of the Christ]]''.{{sfn|Fidler|2002|p=129}} With the novel's success came broad cultural impact. Various consumer products, hotels, steamboats, railway carriages, and even several towns were named after the book, and many families named children after its characters.<ref name=Maurice/>{{sfn|Fidler|2002|pp=128–129}} Although there was considerable interest in a [[theatrical adaptation]], Evans was concerned about how the novel's themes would be portrayed on the stage and did not approve the first script for a ''St. Elmo'' play until 1909. Other stage versions, many of which were financially successful, were quickly developed after her death later that year.{{sfn|Fidler|2002|pp=142–143}} The first film versions of the story followed shortly. In 1910, the [[Thanhouser Company]] chose ''St. Elmo'' for its [[St. Elmo (1910 Thanhouser film)|second production]], and [[Vitagraph Studios]] produced [[St. Elmo (1910 Vitagraph film)|its own adaptation]] the same year. Both were one-reel [[short films]].{{sfn|Slide|1978|p=70}}{{sfn|Slide|Gevinson|1987|p=195}}

In 1914, while working for the [[Balboa Amusement Producing Company]], William Jossey wrote the screenplay for the first [[feature film|feature-length]] film adaptation.<ref name=MPW1676/> Filming took place in [[Long Beach, California]],<ref name=MPN70/> where the Balboa studios were located.<ref name=LAT/> Footage of an actual church under construction across the street from the studio was used for a scene in which one was built by St. Elmo.<ref name=MPW1676/> Contemporary writers credited [[Bertram Bracken]] as director,{{sfn|Justice|Smith|1914|p=28}}<ref name=BB52 /> as do some modern sources, including the [[American Film Institute]].<ref name=AFI /> Others consider the film the directorial debut of [[J. Gordon Edwards]].<ref name=LOC />{{sfn|Goble|1999|p=504}}{{sfn|Keister|2011|p=91}} Both men subsequently had long careers directing for [[Fox Film]].{{sfn|Solomon|2011|p=14}}

Balboa highlighted the film's production value and artistry in its marketing;<ref name=Atlanta /> its [[film poster]] advertises "194&nbsp;gorgeous scenes".<ref name=Poster /> However, Balboa was not a film distributor,{{sfn|Solomon|2011|p=13}} so in May 1914 they contracted with [[William Fox (producer)|William Fox]]'s [[Box Office Attractions Company]] to have Fox handle the distribution of all Balboa films,{{sfn|Slide|2001|pp=26–27}} beginning with ''St. Elmo''.<ref name=Contracts /> Copies of these films were then shown at Fox's theaters or rented out to other theater franchise owners, in what was known as the states' rights distribution system.{{sfn|Neale|Hall|2010|p=24}} On 1 February 1915, William Fox incorporated Fox Film, which inherited Box Office's assets.{{sfn|Solomon|2011|p=19}} The new company continued to distribute some Box Office films, including ''St. Elmo'',<ref name=Fox /> which played in some areas into 1916.<ref name=Honolulu />

==Reception and legacy==
[[File:St Elmo drinks with the devil.jpg|thumb|St. Elmo shares a drink with the Devil, with Murray's body in a coffin between them. This scene was also censored in Chicago.|alt=A man, next to a corpse in a standing coffin, drinks with another man who represents the Devil.]]
Contemporary reviews were mixed. Writing for ''[[Motion Picture News]]'', A.&nbsp;Danson Michell found the film superior to stage adaptations of the novel, and especially praised the photography.<ref name=MPN70/>  ''[[Moving Picture World]]''{{'s}} Hanford Judson also gave a generally positive review, believing that its production qualities and popular appeal more than compensated for the "artificiality" of a few scenes.<ref name=MPW70 /> Not all critics praised the film. Vanderheyden Fyles of ''Movie Pictorial'' felt the Long Beach scenery lauded elsewhere was irrelevant to the plot and the adapted story was a "baffling mix-up".<ref name=MP28/> A particularly negative review appeared in ''[[Variety (magazine)|Variety]]'', suggesting the film was so bad that its makers "might have got a little profit out of the raw [film] by not ruining it through putting ''St. Elmo'' on it."<ref name=Variety17/> The Chicago Board of [[Film censorship in the United States|Censorship]] found some scenes objectionable and required that Chicago showings of the film be edited to remove depictions of dueling and Murray's dead body.<ref name=Menace/>

''St. Elmo'' was a financial success,{{sfn|Jura|Bardin|2007|p=204}} reported by ''The Photoplayers' Weekly'' as breaking box office records.<ref name=TPW/> The following year, Bertram Bracken directed a film adaptation of another Augusta Jane Evans novel, ''Beulah'' (1859), for Balboa. Though not directly related to ''St. Elmo'',{{sfn|Fidler|2002|pp=56, 125}} the 1915 ''Beulah'' film was marketed as a sequel.<ref name=BB52 />{{sfn|Jura|Bardin|2007|p=204}} In 1923, Fox Film produced [[St. Elmo (1923 American film)|another adaptation]] of the novel, also titled ''St. Elmo'', which was also a success.{{sfn|Fidler|2002|p=144}}

Around 90% of Balboa's films have been [[lost film|lost]],<ref name=LAT/> probably including the 1914 ''St. Elmo''.{{sfn|Tarbox|1983|pp=188, 191}} The [[Library of Congress]] is not aware of any extant copies.<ref name=LOC/>

==See also==
* [[List of lost silent films (1910–14)]]

==References==
{{reflist|30em|refs=
<ref name=AFI>{{cite web |title=St. Elmo |work=Catalogue of Feature Films |publisher=American Film Institute |url=http://www.afi.com/members/catalog/DetailView.aspx?s=1&Movie=16398 |accessdate=2015-03-17}}</ref>
<ref name=Atlanta>{{cite news |title=At the Grand |newspaper=The Atlanta Constitution |date=1914-10-04 |volume=47 |issue=111 |p=14M |url=https://www.newspapers.com/image/26823027/ |subscription=yes}}</ref>
<ref name=BB52>{{cite journal |title=Bracken Back with Balboa Co. |journal=Billboard |volume=26 |issue=48 |date=1914-11-28 |page=52}}</ref>
<ref name=Contracts>{{cite journal |title=Fox Contracts for Whole Balboa Output |journal=Motion Picture News |volume=9 |issue=20 |date=1914-05-23 |p=30 |url=https://archive.org/stream/motionp09moti#page/n583/mode/1up}}</ref>
<ref name=Fox>{{cite journal |title=Fox Film Company Formed in Milwaukee |journal=Motion Picture News |volume=10 |issue=21 |date=1914-11-28 |p=33 |url=https://archive.org/stream/motionpicturenew102unse#page/n622/mode/1up/}}</ref>
<ref name=Honolulu>{{cite news |title='St. Elmo' to be Film Feature at New Playhouse |newspaper=Honolulu Star-Bulletin |volume=23 |issue=7425 |date=1916-01-29 |p=7 |url=https://www.newspapers.com/image/85833017/ |subscription=yes}}</ref>
<ref name=LAT>{{cite web |title=The movie capital of yore |website=Los Angeles Times |url=http://articles.latimes.com/2009/oct/25/local/me-then25 |date=2009-10-25 |author-last=Harvey |author-first=Steve |accessdate=2014-04-10}}</ref>
<ref name=LOC>{{cite web |title=St. Elmo [motion picture] |work=American Silent Feature Film Database |publisher=Library of Congress |date=2014-03-31 |accessdate=2015-01-09 |url=http://memory.loc.gov/diglib/ihas/loc.mbrs.sfdb.9500/default.html}}</ref>
<ref name=Maurice>{{cite journal |author-last=Maurice |author-first=Arthur Bartlett. |title='Best Sellers' of Yesterday: I—Augusta Jane Evans's 'St. Elmo' |journal=The Bookman |volume=31 |issue=1 |year=1910 |pp=35–42}}</ref>
<ref name=Menace>{{cite journal |title=Growing Menace of Chicago Censors: More Films Cut |journal=Motography |volume=12 |issue=14 |date=1914-11-03 |url=https://archive.org/stream/motography12elec#page/458/mode/1up |pp=459–462}}</ref>
<ref name=MP28>{{cite journal |author-last=Fyles |author-first=Vanderheyden |title=Famous Feature Films |journal=Movie Pictorial |volume=1 |issue=20 |date=1914-09-19 |url=https://archive.org/stream/MoviePictorialSept.-nov.1914/MoviePictorialSept-Nov1914#page/n98/mode/1up |p=28}}</ref>
<ref name=MPN70>{{cite journal |author-last=Michell |author-first=A. Danson |title=St. Elmo |journal=Motion Picture News |volume=10 |issue=1 |date=1914-07-11 |page=70 |url=https://archive.org/stream/motionpicturenew101unse#page/n75/mode/1up}}</ref>
<ref name=MPW70>{{cite journal |author-last=Judson |author-first=Hanford C. |title=St. Elmo |journal=The Moving Picture World |volume=21 |issue=1 |date=1914-07-04 |page=70 |url=https://archive.org/stream/movingpicturewor21newy#page/70/mode/1up}}</ref>
<ref name=MPW1676>{{cite journal |title=Doings at Los Angeles |journal=The Moving Picture World |volume=20 |issue=12 |date=1914-06-20 |page=1676 |url=https://archive.org/stream/movingpicturewor20newy#page/1676/mode/1up}}</ref>
<ref name=Poster>{{cite web |title=ST. ELMO / [poster] ; J. Gordon Edwards ; 1914. |publisher=Margaret Herrick Library |url=http://catalog.oscars.org/vwebv/holdingsInfo?bibId=27965 |accessdate=2015-03-07}}</ref>
<ref name=TPW>{{cite journal |title=Balboa's Phenomenal Success in the Film World |journal=The Photoplayers' Weekly |volume=3 |issue=21 |date=1915-12-25 |p=3 |url=https://archive.org/stream/ThePhotoplayersWeeklySept.1915ToMarch1916/PhotoplayersWeeklySept1915-Mar1916#page/n70/mode/1up/}}</ref>
<ref name=Trenton>{{cite news |title='St. Elmo' Pictures |newspaper=Trenton Evening Times |location=Trenton, NJ |date=1915-03-07 |p=4 |url=https://www.newspapers.com/image/6942642 |subscription=yes}}</ref>
<ref name=Variety17>{{cite journal |title=St. Elmo |journal=Variety |volume=35 |issue=13 |date=1914-09-28 |page=17 |url=https://archive.org/stream/variety35-1914-08#page/n116/mode/1up}}</ref>
}}

==Bibliography==
* {{cite book |author-last=Fidler |author-first=William Perry |title=Augusta Evans Wilson, 1835–1909 |publisher=University of Alabama Press |year=2002 |isbn=978-0-8173-5026-0 |ref=harv}}
* {{cite book |author-last=Goble |author-first=Alan |title=The Complete Index to Literary Sources in Film |publisher=Bowker-Saur |year=1999 |isbn=978-1-85739-229-6 |ref=harv}}
* {{cite book |author1-last=Jura |author1-first=Jean-Jacques |author2-last=Bardin |author2-first=Rodney Norman |title=Balboa Films: A History and Filmography of the Silent Film Studio |year=2007 |isbn=978-0-7864-3098-7 |publisher=McFarland |ref=harv}}
* {{cite book |editor1-last=Justice |editor1-first=Fred C. |editor2-last=Smith |editor2-first=Tom R. |title=Who's Who in the Film World |publisher=Film World Publishing |year=1914 |oclc=8807170 |url=https://archive.org/stream/whoswhoinfilmwor00just#page/28/mode/1up |ref=harv}}
* {{cite book |author-last=Keister |author-first=Douglas |title=Stories in Stone New York: A Field Guide to New York City Cemeteries and Their Residents |publisher=Gibbs Smith |year=2011 |isbn=978-1-4236-2102-7 |ref=harv}}
* {{cite book |author1-last=Neale |author1-first=Steve |author2-last=Hall |author2-first=Sheldon |title=Epics, Spectacles, and Blockbusters: A Hollywood History |publisher=Wayne State University Press |year=2010 |isbn=978-0-8143-3008-1 |ref=harv}}
* {{cite book |author-last=Slide |author-first=Anthony |title=Aspects of American Film History Prior to 1920 |publisher=Scarecrow Press |year=1978 |isbn=978-0-8108-1130-0 |ref=harv}}
* {{cite book |author-last=Slide |author-first=Anthony |title=The New Historical Dictionary of the American Film Industry |edition=2nd |publisher=Scarecrow Press |isbn=978-1-57886-015-9 |year=2001 |ref=harv}}
* {{cite book |author1-last=Slide |author1-first=Anthony |author2-last=Gevinson |author2-first=Alan |title=The Big V: A History of the Vitagraph Company |edition=revised |publisher=Scarecrow Press |isbn=978-0-8108-2030-2 |year=1987 |ref=harv}}
* {{cite book |author-last=Solomon |author-first=Aubrey |title=The Fox Film Corporation, 1915–1935: A History and Filmography |publisher=McFarland |year=2011 |isbn=978-0-7864-6286-5 |ref=harv}}
* {{cite book |author-last=Tarbox |author-first=Charles H. |title=Lost Films 1895–1917 |publisher=Jef Films |year=1983 |isbn=978-0-9610916-0-6 |ref=harv}}

==External links==
{{commons category}}
*{{IMDb title|id=tt0004636|title=St. Elmo}}

{{J. Gordon Edwards}}

{{DEFAULTSORT:Saint Elmo}}
[[Category:1910s drama films]]
[[Category:1914 films]]
[[Category:American romantic drama films]]
[[Category:American silent feature films]]
[[Category:American black-and-white films]]
[[Category:Films based on American novels]]
[[Category:Films directed by J. Gordon Edwards]]
[[Category:Lost films]]
[[Category:American films]]