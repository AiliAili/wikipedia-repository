{{about|the adventure novel|the philanthropist|Godfrey Morgan, 1st Viscount Tredegar|the Australian politician|Godfrey Morgan (Australian politician)}}

{{Infobox book 
| name          = Godfrey Morgan: A Californian Mystery
| title_orig    = L'École des Robinsons
| translator    = 
| image         =  Verne-Robinson-cover.jpg
| image_size = 200px
| caption =  Original illustration of Jules Verne's ''L'École des Robinsons'' 
| author        = [[Jules Verne]]
| illustrator   = [[Léon Benett]]
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #22
| genre         = [[Adventure novel]] 
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1882
| english_release_date = 1883
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| isbn          = 978-1515089131
| preceded_by   = [[Eight Hundred Leagues on the Amazon]]
| followed_by   = [[The Green Ray]]
}}

'''''Godfrey Morgan: A Californian Mystery''''' ({{lang-fr|L'École des Robinsons}}, literally ''The School for Robinsons''), also published as '''''School for Crusoes''''', is an 1882 [[adventure novel]] by French writer [[Jules Verne]].<ref>Nash, Andrew (2001-11-27). [http://www.julesverne.ca/vernebooks/jvbkschool.html ''École des Robinsons (L') - 1882'']. Retrieved on 2009-08-13.</ref> The novel tells of a wealthy young man, '''Godfrey Morgan''' who, with his deportment instructor, [[Professor T. Artelett]], embark from [[San Francisco|San Francisco, California]] on a round-the-world ocean voyage. They are [[Castaway|cast away]] on an uninhabited Pacific island where they must endure a series of adversities. Later they encounter an [[African slave]], Carefinotu, brought to the island by [[cannibals]].  In the end, the trio manage to work together and survive on the island.

The novel is a [[robinsonade]]—a play on [[Daniel Defoe]]'s [[1719 in literature|1719]] novel ''[[Robinson Crusoe]]''.

==Plot summary==
The narrative begins with the auction by the [[Federal government of the United States|US Government]] of fictional Spencer Island, located 460 miles off the California coast ({{coord|32|15|N|145|18|W|}}).  The island is uninhabited and there are only two bidders, [[William W. Kolderup]], a very wealthy San Franciscan, and his arch-rival [[J. R. Taskinar]], a resident of [[Stockton, California]].  Kolderup wins the auction, buying Spencer Island for four million dollars.  J. R. Taskinar mutters, "I will be avenged!" before retiring to his hotel.

Godfrey, an idle twenty-two-year-old, lives with Kolderup, his uncle, and Kolderup's adopted goddaughter, Phina, whom Godfrey has grown to love. Prior to marrying Phina, Godfrey asks to undertake a world tour. Acceding to his desire, his uncle sends Godfrey on a sea voyage around the world on board one of his [[steamship]]s, the ''Dream'', commanded by Captain Turcott.  Godfrey is accompanied by his mentor, teacher, and dance instructor, Professor T. Artelett aka "Tartlet".

After some time at sea, Godfrey is awakened one foggy night and told to abandon ship as the Dream is [[Shipwreck (accident)#Causes|founder]]ing.  After jumping into the sea, Godfrey is washed ashore on a deserted island, where he soon finds Tartlet has also been [[maroon]]ed. Godfrey, with scant help from Tartlet, will have to learn to survive, to organize his life, face hostile intruders, and overcome other [[obstacles]]. Eventually, they are also joined by Carefinotu, whom Godfrey rescues from [[Polynesians]] visiting the island. By the end of the story the formerly jaded young man has discovered the value of independent effort, and he gains poise and courage.  The marooned group are rescued and returned to San Francisco, where Godfrey is reunited with [[Phina]]. They agree to marry before continuing the world tour, this time together.

==Theme==
Although the setting is different, the [[robinsonade]] plot is a variation on the theme of rational self-sufficiency that Verne developed earlier in ''[[The Mysterious Island]]'' (1874). At the time of publication, it was common for a young man of wealth to undertake travel as an educational [[rite of passage]], for example the California heir [[Leland Stanford, Jr.]] who took two European [[Grand Tour]]s, one in 1880-81, and died on the second in 1884.  The original French version of Verne's novel was published in 1882, after Stanford's first tour.<ref>Johnston, Theresa, [http://www.stanfordalumni.org/news/magazine/2003/julaug/features/junior.html "About a Boy,"] ''Stanford'', July–August 2003.</ref>

==Film adaptation==
The novel was adapted for a 1981 USA/Spain co-production by director [[Juan Piquer Simón]], titled '''''Jules Verne's Mystery on Monster Island''''' ({{lang-es|Misterio en Isla de los Monstruos}}), and starring [[Peter Cushing]] and [[Terence Stamp]], with [[David Hatton]], [[Ian Sera]], [[Paul Naschy]], [[Blanca Estrada]], [[Ana Obregón]] and [[Frank Braña]] in supporting roles.<ref>{{cite book
|title=Jules Verne on film: a filmography of the cinematic adaptations of his works, 1902 through 1997
|first= Thomas C. 
|last= Renzi
|edition= illustrated
|publisher= McFarland 
|year= 1998
|ISBN= 0-7864-0450-7
|pages=167–169}}</ref> Monsters were prominently included as an element in the film, but were absent from the novel, in which the [[villain]] J. R. Taskinar introduces [[Introduced species|non-indigenous]] [[carnivore]]s to the island to take revenge on Kolderup's auction win. In 2007 the film was released on DVD as part of a [[double feature]].<ref>[http://www.foxstore.com/detail.php?item=3280 FoxStore.com] "Gorilla at Large/Mystery at {{sic}} Monster Island Double Feature." Retrieved on 2009-08-13.</ref> Despite the similar title, the film has no connection with the better-known Verne novel ''[[The Mysterious Island]]'', though some reviewers have disregarded this.<ref>[http://www.moria.co.nz/index.php?option=com_content&task=view&id=2576&Itemid=1 Moria.co.nz] "Monster Island is ostensibly based on Jules Verne’s Mysterious Island (1875), although it gives the impression that none of the filmmakers have actually read the Verne story." Retrieved on 2009-06-12.</ref><ref>[http://www.answers.com/topic/monster-island-film-2 Answers.com] "Spanish director Juan Piquer Simon returned to the author with this substandard retelling of Mysterious Island." Retrieved on 2009-06-12.</ref>

==Notes==
{{Reflist}}

==External links==
{{commons category}}
{{wikisourcelang|fr|L’École des Robinsons}}
* {{gutenberg|no=23489|name=Godfrey Morgan}} (English)
* [http://www.julesverne.ca/vernebooks/jvbkschool.html École des Robinsons (L') - 1882], summary and cover images
* {{IMDb title|0082769|Jules Verne's Mystery on Monster Island}}

==See also==
* ''[[Robinson Crusoe]]''
* ''[[The Swiss Family Robinson]]''
* ''[[The Mysterious Island]]''

{{Verne}}

{{Authority control}}

[[Category:1882 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels about survival]]
[[Category:Novels set on islands]]
[[Category:Castaways in fiction]]
[[Category:Novels set in San Francisco]]
[[Category:Novels set in Oceania]]
[[Category:French novels adapted into films]]
[[Category:Cannibalism in fiction]]