<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=H36 Dimona and HK36 Super Dimona
 | image=Diamond HK36 Super Dimona.jpg
 | caption=Diamond HK36 Super Dimona
}}{{Infobox Aircraft Type
 | type=[[Motor glider]]
 | national origin=[[Austria]]
 | manufacturer=[[Diamond Aircraft Industries]]
 | designer=[[Wolf Hoffmann (aircraft designer)|Wolf Hoffmann]]
 | first flight=9 October 1980 (H36)<br>October 1989 (HK36R)
 | introduced=
 | retired=
 | status=In production (HK36 Super Dimona)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=more than 900
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=[[Diamond DV20]]
}}
|}
[[File:Motorsegelflygplan.jpg|right|thumb|Hoffmann H36 Dimona]]
[[File:ILA 2008 PD 772.JPG|thumb|right|Super Dimona, showing its [[winglets]]]]
The '''Diamond HK36 Super Dimona''' is an extensive family of [[Austria]]n [[low-wing]], [[T-tail]]ed, two-seat [[motor glider]]s that were designed by [[Wolf Hoffmann (aircraft designer)|Wolf Hoffmann]] and currently produced by [[Diamond Aircraft Industries]].<ref name="SD-HK36TS">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=185|title = Katana Xtreme HK 36TS Diamond |accessdate = 26 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SD-H36">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=86|title = Dimona H 36 Diamond |accessdate = 26 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SD-HK36TTS">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=432|title = Katana Xtreme HK-36 TTS Diamond |accessdate = 26 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SD-HK36">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=343|title = Super Dimona HK 36 Diamond |accessdate = 26 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 121. Soaring Society of America, November 1983. USPS 499-920</ref><ref name="g51eu">{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/152317b72328c6a4862577de0055cca7/$FILE/g51eu.PDF|title = Type Certificate Data Sheet No. G51eu |accessdate = 26 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=November 1997}}</ref><ref name="G07CE">{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/53c3b7dd4be4926a86256ea10057412c/$FILE/G07CE.pdf|title = Type Certificate Data Sheet No. G07CE |accessdate = 26 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=January 2004}}</ref><ref name="Diamond">{{Cite web|url = http://www.diamond-air.at/hk36_super_dimona+M52087573ab0.html|title = HK36 Super Dimona|accessdate = 26 July 2011|last = [[Diamond Aircraft Industries]]|authorlink = |year = n.d.}}</ref>

==Design and development==
The series started with the Hoffmann H36 Dimona, a touring motorglider introduced in the early 1980s. The aircraft were initially produced by [[Hoffmann Flugzeugbau]], which became [[HOAC Flugzeugwerk]] and later Diamond Aircraft Industries.<ref name="SD-H36" /><ref name="SoaringNov83" /><ref name="g51eu" /><ref name="Hist">{{Cite web|url = http://www.diamondaircraft.com/why/history.php|title = Diamond Aircraft - A history|accessdate = 26 July 2011|last = Diamond Aircraft Industries |authorlink = |year = 2010}}</ref>

Built entirely from [[fibreglass]], the H36 family all use a Wortmann FX 63-137 [[airfoil]]. The wings feature top-surface [[Schempp-Hirth]]-style [[Air brake (aircraft)|airbrakes]]. The wings can be folded by two people in a few minutes to allow storage. The original H36 has {{convert|16.0|m|ft|1|abbr=on}} wings, while the later members of the family added slightly greater span. The H36 offers a 27:1 [[glide ratio]], while later variants improved that by one point, to 28:1. Cockpit accommodation seats two in [[side-by-side configuration]], under a hinged [[bubble canopy]].<ref name="SD-HK36TS" /><ref name="SD-H36" /><ref name="SD-HK36TTS" /><ref name="SD-HK36" /><ref name="SoaringNov83" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 26 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

The series are [[type certificate|type certified]] in Europe and North America. The H36 received its US [[Federal Aviation Administration]] certification on 9 July 1986. Due to its fibreglass construction, the US certification includes the restriction "All external portions of the glider exposed to sunlight must be painted white except of ([[sic]]) wing tips, nose of fuselage and rudder."<ref name="g51eu" /><ref name="G07CE" />

In March 1987 an improved variant was developed by Dieter Köhler and the subsequent HK36R first flew with a [[Limbach L2400]] engine in October 1989.

When equipped with the larger available engines, particularly the {{convert|86|kW|hp|0|abbr=on}} [[Rotax 914]] [[turbocharged]] powerplant, the aircraft can be used for [[Aero-tow|glider towing]]. A commercial success, more than 900 H36s and HK36s have been completed.<ref name="Diamond" />

The HK36 provided the basis from which the [[Diamond DV20 Katana]] from which the improved DA20 and four-seat [[Diamond DA40|DA40]] series were later developed.<ref name="Hist" />

==Operational history==
In 1991, an HK36, flown by Peter Urach in Austria, set an absolute altitude record in its class for a piston engined aircraft of {{convert|36188|ft|m|0|abbr=on}}. The record held until surpassed in 2002 by the [[Bohannon B-1]].<ref>Flying Magazine, July 2002, pg 36</ref>

[[File:Boeing Fuel Cell Demonstrator AB1.JPG|thumb|right|In February 2008, The Boeing Fuel Cell Demonstrator used a Diamond HK36 Super Dimona motor glider airframe and achieved straight-level flight on a manned mission powered by a hydrogen [[fuel cell]].]]

The FCD (Fuel Cell Demonstrator) was a project led by [[Boeing]] that used a Diamond HK36 Super Dimona motor glider as a [[testbed]] for a fuel cell-powered light airplane research project. The project achieved level flight using fuel cells only in February and March 2008.<ref>{{cite web|url = http://www.boeing.com/news/releases/2003/q3/nr_030711p.html|title = Boeing Announces Partners for Fuel Cell Demonstrator Airplane Project |accessdate = 26 July 2011}}</ref><ref name="AvWeb04Apr08">{{cite web|url = http://www.avweb.com/avwebflash/news/BoeingFliesFuelCellAircraft_197531-1.html|title = Boeing Flies Fuel Cell Aircraft |accessdate = 26 July 2011|last = Niles|first = Russ|authorlink = |date=April 2008}}</ref>

In December 2016 there were nine H36s and thirty HK36s registered with the US FAA, two HK36Rs and two HK36TTSs registered with [[Transport Canada]], along with seven H36s and eight HK36s registered with the UK [[Civil Aviation Authority (United Kingdom)|Civil Aviation Authority]].<ref name="FAARegH36">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=H36&PageNo=1|title = Make / Model Inquiry Results|accessdate = 13 December 2016|last = [[Federal Aviation Administration]]|authorlink = |date=13 December 2016}}</ref><ref name="TCCAR">{{Cite web|url=http://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchSimpRes.aspx?cn=%7c%7c&mn=%7cHK36%7c&sn=%7c%7c&on=%7c%7c&m=%7c%7c |title=Canadian Civil Aircraft Register |accessdate=13 December 2016 |last=[[Transport Canada]] |authorlink= |date=13 December 2016}}</ref><ref name="GINFO">{{Cite web|url = http://publicapps.caa.co.uk/modalapplication.aspx?catid=1&pagetype=65&appid=1&mode=summary&aircrafttype=HK36|title = GINFO Search Results Summary|accessdate = 13 December 2016|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=13 December 2016}}</ref>

==Variants==
;H36 Dimona
:Original version produced by Hoffmann and later by [[HOAC]], with a {{convert|16.0|m|ft|1|abbr=on}} wingspan, [[conventional landing gear]], 27:1 glide ratio and powered by a [[Limbach L2000 EB1C]] engine of {{convert|60|kW|hp|0|abbr=on}}, a [[Rotax 912A]] of {{convert|60|kW|hp|0|abbr=on}} or [[Limbach L2400 EB]] of {{convert|65|kW|hp|0|abbr=on}}. Applied for US FAA certificate on 4 April 1982 and received on 9 July 1986 in the [[utility category]] at a gross weight of {{convert|770|kg|lb|0|abbr=on}}.<ref name="SD-H36" /><ref name="SoaringNov83" /><ref name="g51eu" />
[[File:Super Dimona D-KLAI.jpg|thumb|right|Super Dimona, showing its wingspan]]
[[File:Hoffmann HK-36R Super Dimona ( D-KMGA) 02.jpg|right|thumb|Hoffmann HK-36 R Super Dimona]]
;HK36 R Super Dimona
:Developed from the H36, with a [[carbon-fibre]] [[Spar (aviation)|spar]], modified [[fuselage]], {{convert|16.2|m|ft|1|abbr=on}} wingspan and {{convert|60|kW|hp|0|abbr=on}} [[Rotax 912A]] engine. Optional [[wing tip]]s can extend the span to {{convert|17.6|m|ft|1|abbr=on}}. Received US FAA type approval on 23 July 1993 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}.<ref name="SD-H36" /><ref name="g51eu" />
;HK36TS Super Dimona
:Developed from the HK36 R Super Dimona, the HK36TS has a {{convert|60|kW|hp|0|abbr=on}} [[Rotax 912 A3]] engine, {{convert|16.6|m|ft|1|abbr=on}} wingspan, 28:1 glide ratio and conventional landing gear. Received US FAA type approval on 25 September 1997 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}. Marketed as the ''Katana Xtreme'' in Canada and the USA.<ref name="G07CE" />
;HK36TC Super Dimona
:The HK36TC has a {{convert|60|kW|hp|0|abbr=on}} [[Rotax 912 A3]] engine. Received US FAA type approval on 25 September 1997 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}. Marketed as the ''Katana Xtreme'' in Canada and the USA.<ref name="SD-HK36TS" /><ref name="SD-HK36TTS" /><ref name="G07CE" /><ref name="Diamond" />
[[File:Diamond HK36 Super Dimona TC 100 (D-KWZE) 02.jpg|thumb|right|Diamond HK36TC-100 Super Dimona]]
;HK36TC-100 Super Dimona
:The HK36TC-100 has a {{convert|74|kW|hp|0|abbr=on}} [[Rotax 912 S3]] engine. Applied for US FAA type approval on 16 January 2003 and received on 12 January 2004  in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}.<ref name="G07CE" /> Minimum sink rate: 1.18 m/s at 97 km/h , glide ratio 1:27 at 105 km/h<ref>[http://support.diamond-air.at/hk36-t_fhb_bas+M52087573ab0.html Flight manual Diamond aircraft industries, 3.01.12, 9 January 2002]</ref> Marketed as the ''Katana Xtreme'' in Canada and the USA.<ref name="G07CE" />
;HK36TTS Super Dimona
:The HK36TTS has a {{convert|86|kW|hp|0|abbr=on}} [[Rotax 914 F3]] or F4 [[turbocharged]] engine, a Muhlbauer MTV-21-A-C-F/CF 175-05 [[propeller (aircraft)|propeller]], {{convert|16.6|m|ft|1|abbr=on}} wingspan, 28:1 glide ratio and conventional landing gear. Received US FAA type approval on 25 September 1997 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}. Marketed as the ''Katana Xtreme'' in Canada and the USA.<ref name="SD-HK36TS" /><ref name="SD-HK36TTS" /><ref name="G07CE" /><ref name="Diamond" />
;HK36TTC Super Dimona
:The HK36TTC has a {{convert|86|kW|hp|0|abbr=on}} [[Rotax 914 F3]] or F4 turbocharged engine, {{convert|16.6|m|ft|1|abbr=on}} wingspan, 28:1 glide ratio and tricycle landing gear. Received US FAA type approval on 25 September 1997 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}}. Marketed as the ''Katana Xtreme'' in Canada and the USA.<ref name="SD-HK36TS" /><ref name="SD-HK36TTS" /><ref name="G07CE" /><ref name="Diamond" />
;HK36TTC Eco Dimona
:Special mission version of the HK36 for the surveillance role, it is equipped with a gimbal-mounted Wescam camera and cockpit display, a {{convert|86|kW|hp|0|abbr=on}} [[Rotax 914 F3]] or F4 turbocharged engine and a Muhlbauer MTV-21-A-C-F/CF 175-05 propeller. Received US FAA type approval on 29 March 1999 in the utility category at a gross weight of {{convert|770|kg|lb|0|abbr=on}} and 21 December 2000 in the restricted category, limited to aerial photography only, at a gross weight of {{convert|930|kg|lb|0|abbr=on}}. Marketed as the ''Multi Purpose Xtreme'' in Canada.<ref name="G07CE" /><ref name="Diamond" />
;Diamond DA36 E-Star
:Developed by Siemens, EADS and Diamond Aircraft to reduce fuel consumption and emissions by up to 25 percent, using a serial hybrid drive that turns the aircraft's prop with a Siemens {{convert|70|kW|hp|0|abbr=on}} electric motor, from power generated by a {{convert|40|hp|kW|0|abbr=on}} Austro Engines Wankel rotary engine and generator, stored in batteries. The prototype first flew 8 June 2011.<ref name="AVWeb23Jun11">{{Cite news|url = http://www.avweb.com/avwebflash/news/hybrid_electric_motor_aircraft_paris_air_show_estar_204867-1.html |title = Hybrid Powered Aircraft In Paris|accessdate = 27 June 2011|last = Pew|first = Glenn|authorlink = |date=June 2011| work = AvWeb}}</ref>
;[[Hoffmann H38 Observer]]
:A surveillance aircraft largely based on the H36 Dimona which failed to enter flight testing due to failure of the partnership between Wolf Hoffmann and Hoffmann Flugzeugbau.{{Citation needed|date=May 2016}}
<!-- ==Aircraft on display== -->

==Operators==
;{{IND}}
* [[Indian Air Force]]<ref>{{cite magazine|title=Airscene: Military Affairs: India|magazine=[[Air International]]|volume=59| issue=No. 3|date= September 2000| p=133|issn=0306-5634}}</ref>

==Specifications (Hoffmann H36 Dimona) ==
[[File:HOFFMANN H-36 DIMONA.jpg|thumb|right|Hoffmann H36 Dimona]]
{{Aircraft specs
|ref=Sailplane Directory, Soaring and FAA Type Certificate G51EU<ref name="SD-H36" /><ref name="SoaringNov83" /><ref name="g51eu" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=16.0
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=15.24
|wing area sqft=
|wing area note=
|aspect ratio=16.8:1
|airfoil=Wortmann FX 63-137
|empty weight kg=497
|empty weight lb=
|empty weight note=
|gross weight kg=770
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|80|liters|lk=on}}
|more general=

<!--
        Powerplant
-->
|eng1 number=[[Limbach L2000 EB1C]]
|eng1 name=
|eng1 type=
|eng1 kw=60<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=Hoffmann HO-V 62-R/L 160 T, three position, fully feathering
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=65<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=113
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=275
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=sea level to 6000 feet
|range km=
|range miles=680
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=27:1 at {{convert|105|km/h|mph|0|abbr=on}}
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|sink rate ms=0.91
|sink rate ftmin=
|sink rate note= at {{convert|79|km/h|mph|0|abbr=on}}
|lift to drag=
|wing loading kg/m2=48.56
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
* [[List of gliders]]
|related=
* [[Diamond DV20]]
|similar aircraft=
*[[Grob G 109]]
*[[Pipistrel Sinus]]
*[[Schweizer SGM 2-37]]
*[[Valentin Taifun]]
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

==External links==
{{Commons category|Diamond HK36}}
*{{Official website|http://www.diamond-air.at/en/single-engine-aircraft/hk36-super-dimona/}}
{{Diamond Aircraft}}
{{Hoffmann aircraft}}

[[Category:United States sailplanes 1980–1989]]
[[Category:Diamond aircraft|HK36]]
[[Category:T-tail aircraft]]