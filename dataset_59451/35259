{{Use dmy dates|date=April 2014}}
{{good article}}
{{Infobox football match
|                   title = 2003 All-Ireland Football Final
|                   image = 
|                   event = [[2003 All-Ireland Senior Football Championship]]
|                   team1 = {{GG|Armagh|football}}
|        team1association = [[File:Armagh colours.PNG|40px]]
|              team1score = 0&ndash;09
|                   team2 = {{GG|Tyrone|football}}
|        team2association = [[File:Derry colours.PNG|40px]]
|              team2score = 0&ndash;12
|                 details = 
|                    date = 28 September 2003
|                 stadium = [[Croke Park]]
|                    city = [[Dublin]]
|      man_of_the_match1a = 
|                 referee = Brian White
|              attendance = 79,391
|                 weather = 
|                previous = [[2002 All-Ireland Senior Football Championship Final|2002]]
|                    next = [[2004 All-Ireland Senior Football Championship Final|2004]]
}}

The '''2003 All-Ireland Senior Football Championship Final''' was the 116th final of the [[All-Ireland Senior Football Championship]], a [[Gaelic football]] tournament. It was held on 28 September 2003 at [[Croke Park]], Dublin and featured defending champions [[Armagh GAA|Armagh]] against [[Tyrone GAA|Tyrone]]. The counties are both in the province of [[Ulster]] and share a boundary &ndash; this was the first All-Ireland Football Final between sides from the same province. Tyrone won their first title after the match finished 0&ndash;12 &ndash; 0&ndash;09 in their favour. Many commentators were critical of the game's entertainment value.

==Competition structure==
{{details|topic=the competition|All-Ireland Senior Football Championship}}
{{details|topic=county sides|GAA County}}

Each of the 32 [[Counties of Ireland|traditional counties of Ireland]] is represented by a [[GAA County|county side]]. Apart from [[Kilkenny GAA|Kilkenny]], they all participated in the [[2003 All-Ireland Senior Football Championship]]. The "overseas counties" of [[London GAA|London]] and [[New York GAA|New York]] also participated. Every county in Ireland is located in a [[Provinces of Ireland|province]]; London and New York were in [[Connacht]] for the purpose of the championship. It began with four provincial championships &ndash; knock-out competitions between sides in the same province. The four winners progressed to the All-Ireland quarter-finals. The remaining sides, apart from New York, entered the All-Ireland qualifiers to determine the other four teams to contest the quarter-finals. These were followed by the semi-finals and the All-Ireland final.

==Background==
{{details|topic=the sport and the terminology used in this article|Gaelic football}}

[[File:County Armagh and County Tyrone.png|thumb|County Armagh (orange) and County Tyrone (red) shown within Ireland|alt=County Armagh and County Tyrone shown within Ireland]]

This was the first final between two counties from the same province;<ref name="tickets">{{cite news|last=McHugh|first=Michael|title=Ulster's Gaelic glory; Armagh and Tyrone fans begin scramble for tickets to GAA final|newspaper=Belfast Telegraph|date=1 September 2003}}</ref> in this case, Ulster. This was only possible because of the qualifying system introduced in the [[2001 All-Ireland Senior Football Championship|2001 championship]].<ref>{{cite news|last=Regan|first=Claire|title=Ulster exodus - Thousands of fans flood south for epic GAA clash|newspaper=Belfast Telegraph|date=27 September 2003}}</ref>  Previously, the All-Ireland Championship was only contested by the four winners of the provincial championships, so a final between two counties from the same province was impossible.

Tyrone had contested the final in [[1986 All-Ireland Senior Football Championship Final|1986]] and [[1995 All-Ireland Senior Football Championship Final|1995]] but lost on both occasions, against [[Kerry GAA|Kerry]] and [[Dublin GAA|Dublin]] respectively.<ref name="background stats">{{cite news|last=Regan|first=Claire|title=History in making as Red and Orange set for throw-in|newspaper=Belfast Telegraph|date=27 September 2003}}</ref> Armagh were the defending champions,<ref name="last year">{{cite news|title=GAA: Stevie Wonder points the way|newspaper=Belfast Telegraph|date=23 September 2002}}</ref> having won their first title the year previously.<ref name="last year" /> They had lost the final in [[1953 All-Ireland Senior Football Championship Final|1953]] and [[1977 All-Ireland Senior Football Championship Final|1977]].<ref name="last year" /> No side had won consecutive titles since [[Cork GAA|Cork]] in 1989 and 1990, while no side from Ulster had done so since [[Down GAA|Down]], who won in 1960 and 1961.<ref name="background stats" />

Tyrone had already won the [[2003 National Football League (Ireland)|2003 National Football League]]<ref>{{cite news|last=Campbell|first=John|title=GAA: How's this for the 'other' Tyrone!|newspaper=Belfast Telegraph|date=7 May 2003}}</ref> and the year's Ulster Championship,<ref name="ulster win">{{cite news|last=Campbell|first=John|title=GAA: Down in the dumps; Tyrone now turn their eyes to the big prize|newspaper=Belfast Telegraph|date=21 July 2003}}</ref> in what was manager [[Mickey Harte|Mickey Harte's]] first year in charge.<ref name="Harte interview">{{cite news|title=Major influence from minor days; Sean Moran talks to Mickey Harte about some of the key factors behind Tyrone's first All-Ireland|newspaper=The Irish Times|date=30 September 2003|page=22}}</ref>

==Routes to the final==
{{main|2003 All-Ireland Senior Football Championship}}

Tyrone progressed directly to the All-Ireland quarter finals as they won the [[2003 Ulster Senior Football Championship]], through victories against [[Derry GAA|Derry]],<ref>{{cite news|last=Campbell|first=John|title=GAA: Red Hand aces in class of their own|newspaper=Belfast Telegraph|date=26 May 2003}}</ref> [[Antrim GAA|Antrim]]<ref>{{cite news|last=Campbell|first=John|title=GAA: Red Hand men give rivals slap in the face; Tyrone prove they're a class apart|newspaper=Belfast Telegraph|date=16 June 2003}}</ref> and Down.<ref name="ulster win" /> Armagh lost to [[Monaghan GAA|Monaghan]] in the preliminary round of the Ulster Championship<ref name="monaghan defeat">{{cite news|last=Campbell|first=John|title=GAA: Monaghan lap up a feast at Armagh; But Champs eat humble pie|newspaper=Belfast Telegraph|date=12 May 2003}}</ref> and therefore entered the All-Ireland qualifiers at the first round.<ref name="monaghan defeat" /> They defeated [[Waterford GAA|Waterford]],<ref>{{cite news|last=Campbell|first=John|title=GAA: Armagh break losing streak|newspaper=Belfast Telegraph|date=9 June 2003}}</ref> [[Antrim GAA|Antrim]],<ref>{{cite news|title=GAA: Antrim so near and yet so far from shocker|newspaper=Belfast Telegraph|date=23 June 2003}}</ref> Dublin<ref>{{cite news|last=Campbell|first=John|title=GAA: Orchard warriors put Dubs to sword; Sensational upset has given Armagh hope of Sam again|newspaper=Belfast Telegraph|date=7 July 2003}}</ref> and [[Limerick GAA|Limerick]]<ref>{{cite news|title=GAA: Armagh machine rolls over Limerick|newspaper=Belfast Telegraph|date=21 July 2003}}</ref>  to join Tyrone at the quarter-final stage; the ties were played whilst Tyrone were competing in the latter stages of the Ulster Championship.

Sides that had played each other in the provincial championships could not be drawn together in the quarter-finals<ref name="quarter" /> but none of these restrictions affected Armagh or Tyrone.<ref name="quarter">{{cite news|last=Campbell|first=John|title=GAA: Ulster sides ask for no final quarter|newspaper=Belfast Telegraph|date=21 July 2003}}</ref> Tyrone were drawn against [[Fermanagh GAA|Fermanagh]], who had unexpectedly defeated [[Meath GAA|Meath]] and [[Mayo GAA|Mayo]] in the qualifiers,<ref name="quarter" />  while Armagh were drawn against [[Leinster]] championship winners [[Laois GAA|Laois]].<ref name="quarter" /> Tyrone won their game by 1&ndash;21 (24 points) to 0&ndash;05.<ref>{{cite news|last=Campbell|first=John|title=GAA: Tyrone are simply on their own; Red Hands look a different class|newspaper=Belfast Telegraph|date=4 August 2003}}</ref> Armagh defeated Laois by 0&ndash;15 to 0&ndash;13, although the sides were level on points on nine occasions.<ref>{{cite news|last=Campbell|first=John|title=GAA: Armagh kept on the Laois; Cool heads see orchard county progress to semis|newspaper=Belfast Telegraph|date=4 August 2003}}</ref> Even before [[Donegal GAA|Donegal's]] win over [[Galway GAA|Galway]], which meant three of the four semi-finalists were from Ulster, there was intense media speculation about the possibility of an all-Ulster final.<ref>{{cite news|last=Campbell|first=John|title=Galway, Kerry worth watching|newspaper=Belfast Telegraph|date=8 August 2003}}</ref>

Tyrone's semi-final was against Kerry. Despite their captain [[Peter Canavan]] suffering an injury early on, Tyrone won by 0&ndash;13 to 0&ndash;06.<ref>{{cite news|last=Archer|first=Kenny|title=Red Hand fans can keep on dreaming; Bank of Ireland All-Ireland SFC semi-final: Tyrone. 0-13 Kerry. . .0-6|newspaper=Irish News|date=25 August 2003}}</ref> Much of the match analysis focused on the manner in which it was played.<ref name="style">{{cite news|last=Moran|first=Sean|title=Style will change but fouling must be addressed|newspaper=The Irish Times|date=27 August 2003|page=23}}</ref> It was characterised by persistent fouling<ref name="referee" /> (73 frees were awarded in total<ref name="style 2">{{cite news|last=Humphries|first=Tom|title=Tyrone's passage far from idyllic|newspaper=The Irish Times|date=25 August 2003|page=50}}</ref>) and Tyrone's defensive tactics.<ref>{{cite news|last=Cummiskey|first=Gavin|title=Dragging down the game of football; Gavin Cummiskey looks at a different kind of professionalism that's threatening to dominate the game of Gaelic football|newspaper=The Irish Times|date=27 August 2003|page=23}}</ref> While many commentators expressed frustration about the quality of the game,<ref name="style 2" /><ref name="style 3">{{cite news|last=Moran|first=Sean|title=Another Tyrone man goes on the defensive|newspaper=The Irish Times|date=26 August 2003}}</ref> some appreciated the skill with which Tyrone employed their tactics.<ref name="style 3" /><ref>{{cite news|last=O'Mahony|first=John|title=Intensity and hunger really something|newspaper=The Irish Times|date=25 August 2003|page=51}}</ref> Mickey Harte countered the criticism by saying: "There's no use in us playing flamboyantly and losing."<ref name="style 3" />

Donegal were Armagh's opposition in an all-Ulster semi-final. Armagh were behind at half time<ref name="armagh semi">{{cite news|last=Moran|first=Sean|title=Armagh feed their hunger; Armagh - 2-10, Donegal - 1-9|newspaper=The Irish Times|date=1 September 2003}}</ref> but took advantage of [[Raymond Sweeney|Raymond Sweeney's]] dismissal just after the interval to finish with a 2&ndash;10 (16 points) &ndash; 1&ndash;09 (12 points) victory.<ref name="armagh semi" /> Armagh may have had a larger winning margin had they not amassed 21 wides.<ref name="armagh semi" /> A death threat was allegedly made against referee Michael Monahan in the closing minutes.<ref>{{cite news|last=Regan|first=Claire|title=Murder threat against GAA referee probed|newspaper=Belfast Telegraph|date=3 September 2003}}</ref>

==Pre-match==

Brian White, who had previously refereed two All-Ireland finals,<ref name="referee" /> was announced as the match referee in early September.<ref name=referee>{{cite news|last=Cummiskey|first=Gavin|title=White to referee final|newspaper=The Irish Times|date=11 September 2003|page=21}}</ref> He had once previously refereed a game between Armagh and Tyrone &ndash; an Ulster Championship quarter-final replay in 2002.<ref name="referee" />

The final was highly anticipated, particularly as Armagh and Tyrone are neighbouring counties.<ref>{{cite news |title=Red Hand taxi gears up for Sam Maguire's fare |last=Regan |first=Claire |newspaper=Belfast Telegraph |date=27 September 2003}}</ref> [[Police Service of Northern Ireland]] Deputy Chief Constable Paul Leighton estimated that 40,000 fans would travel from Northern Ireland to Dublin,<ref>{{cite news |title=Police in All-Ireland traffic warning |last=Regan |first=Claire |newspaper=Belfast Telegraph |date=26 September 2003 }}</ref> despite each competing county only being allocated approximately 10,000 match tickets.<ref name="tickets" /> Declan Martin, policy director for Dublin Chamber of Commerce, expected the revenue generated in the city as a result of the final to double because two sides from Ulster were involved.<ref>{{cite news |title=Ulster GAA fans' huge cash boost for Dublin |last=Regan |first=Claire |newspaper=Belfast Telegraph |date=15 August 2003 }}</ref>

Road signs in the [[Pomeroy, County Tyrone|Pomeroy]] area were painted in Tyrone colours leading up to the match. This was condemned by the [[Roads Service]], who said the signs would cost thousands of pounds to replace.<ref>{{cite news |title=Call to stop painting road signs |last= |first= |newspaper=Belfast Telegraph |date=20 September 2003 }}</ref> In [[Strabane]], a sculpture was covered in Tyrone kit.<ref name="strabane">{{cite news |title=Unionist gets shirty over 'GAA sculpture' |newspaper=Belfast Telegraph |date=25 September 2003 }}</ref> [[Ulster Unionist Party]] councillor for the town, [[Derek Hussey]], responded by saying: "I know it is a unique sporting occasion, an all-British All-Ireland final, but the hysteria that has developed around the whole event is intimidatory to some people."<ref name="strabane" />

John Boyle, a native of Armagh and owner of [[Boylesports]], expressed an interest in placing a £250,000 bet with nine other businessmen, each of whom would contribute £25,000, on Armagh winning the championship.<ref name="bet">{{cite news |title=Bet plan stirs up GAA |last=Khan |first=Frank |newspaper=Belfast Telegraph |date=8 September 2003 }}</ref> The winnings and the stake would have been given to the Armagh squad.<ref>{{cite news |title=Bookie bets GBP 250,000 |last=Campbell |first=Brian |page=4 |newspaper=Irish News |date=8 September 2003 }}</ref> [[Gaelic Athletic Association|GAA]] president [[Seán Kelly (Irish politician)|Seán Kelly]] denounced the idea: "Playing is a voluntary activity and should have nothing to do with gambling. Such bets put too much pressure on the players and are somewhat obscene."<ref name="bet" /> The GAA was also critical of tickets for the final being sold in newspaper columns and on online auction sites.<ref>{{cite news |title=E-touting of final tickets criticised |last=McCann |first=Darran |newspaper=Irish News |date=12 September 2003 |page=1 }}</ref>

==Match==

[[Martin McGuinness]] of [[Sinn Féin]] and [[Ian Pearson]] of the [[Northern Ireland Office]] were at the match,<ref name="homecoming" /> as were eight family members of victims of the [[Omagh bombing]], who sat in the Hogan Stand as guests of the GAA.<ref name="homecoming" /> Donegal singer [[Mickey Joe Harte]] sang Ireland's national anthem, [[Amhrán na bhFiann]], before the match.<ref>{{cite news|last=McGregor|first=Roddy|title=Singer excited about All-Ireland privilege|newspaper=Irish News|date=5 September 2003}}</ref> In the [[All-Ireland Minor Football Championship|All-Ireland Minor Football Championship Final]], held just before the senior game, Laois and Dublin drew, each side scoring 1&ndash;11 (14 points).<ref>{{cite news|last=Roche|first=Pat|title=Murphy makes late call, Dublin 1-11, Laois 1-11|newspaper=The Irish Times|date=29 September 2003}}</ref>

The starting line-ups for the senior game were released several days before the match; both sides chose to start with the same fifteen players that had started their respective semi-final wins.<ref name="line up">{{cite news |title=Neighbours keep familiar look for showdown |last=Moran |first=Sean |newspaper=The Irish Times |date=24 September 2003 }}</ref> Peter Canavan had recovered sufficiently from the ankle injury he sustained during Tyrone's semi-final<ref>{{cite news |title=Canavan on the right road |last=O'Riordan |first=Ian |newspaper=The Irish Times |date=17 September 2003 }}</ref> to captain them.<ref name="line up" /> He was the only player in their starting line-up who had played in the county's last All-Ireland final.<ref name="line up" /> [[Ciaran Gourley]], who was also an injury concern for Tyrone, was deemed fit enough to play.<ref name="line up" /> [[Brian McGuigan]] was suffering from the flu but started.<ref name="Harte interview" /> Armagh had no injury concerns<ref name="line up" /> and twelve of their starting fifteen had played in the final the previous year &ndash; only [[Paul Hearty]], [[Andy Mallon]] and Phillip Loughran were debutants,<ref name="line up" />

===Match report===

Both sides played defensively<ref name="diving">{{cite news |title=Minor objections to an ugly spectacle |last=Humphries |first=Tom |newspaper=The Irish Times |page=59 |date=29 September 2003}}</ref> which led many commentators to bemoan the poor quality of the match.<ref name="moran report">{{cite news |title=Fitting reward after hard struggle |last=Moran |first=Sean |newspaper=The Irish Times |page=52 |date=29 September 2003 }}</ref><ref name="humphries report">{{cite news |title=Beautiful struggle but an awful game |last=Humphries |first=Tom |newspaper=The Irish Times |date=29 September 2003 |page=50 }}</ref> There were frequent pauses for injuries and accusations of diving.<ref name="diving" /> However, some analysts commented on the genuine desire to win both teams displayed.<ref name="humphries report" /> Numerous goal opportunities were missed, most notably by Tyrone,<ref name="humphries report" /> although only a block from [[Conor Gormley]] prevented [[Steven McDonnell (Gaelic footballer)|Steven McDonnell]] from equalising for Armagh in the 68th minute.<ref name="moran report" /><ref name="humphries report" /> Tyrone led 0&ndash;08 &ndash; 0&ndash;04 at half-time;<ref name="bbc" /> five of their points were scored by Peter Canavan from frees.<ref name="bbc" /> He was replaced during the interval due to a relapse of his ankle injury during training,<ref name="humphries report" /> although he did return to the pitch for the final few minutes.<ref name="moran report" /> [[Diarmuid Marsden]] was controversially sent off in the second half following an off-the-ball incident,<ref name="humphries report" /> leaving Armagh with only fourteen players. Marsden had been arguing with Conor Gormley when he was approached by Philip Jordan.<ref name="humphries report" /> Marsden raised his arm and made contact with Jordan, who fell to the ground.<ref name="humphries report" /> Some commentators claimed Jordan was feigning injury to get his opponent sent off.<ref>{{cite news|last=Breheny|first=Martin|title=Tyrone player mocked Marsden after wrongful dismissal in 2003 final|url=http://www.independent.ie/sport/gaelic-football/tyrone-player-mocked-marsden-after-wrongful-dismissal-in-2003-final-26782128.html|work=independent.ie|accessdate=7 January 2014}}</ref>  Armagh managed to stay within two points of Tyrone at times<ref name="moran report" /> but were ultimately unable to catch Tyrone. At the final whistle, Tyrone fans invaded the pitch and remained there for an hour.<ref>{{cite news |title=Fifty years on... the dream comes true |page=5 |newspaper=Irish News |date=29 September 2003}}</ref> In his speech after lifting the trophy, Peter Canavan dedicated the victory to every Tyrone team he had played on, the [[1986 All-Ireland Senior Football Championship Final|1986]] team (beaten in the final by Kerry) and every player who had played on teams without success.<ref name=speech>{{cite news|title=Worth the wait|newspaper=The Irish Times|date=30 September 2003}}</ref> He also spoke of his father, who had died over the summer,<ref name="speech" /> and of Paul McGirr, who had played alongside many of the Tyrone team before he died in a freak accident aged 18.<ref name="speech" /><ref>{{cite news|title=Tragedy casts shadow for Tyrone GAA team|url=http://www.bbc.co.uk/news/uk-northern-ireland-12161491|work=BBC News | date=11 January 2011}}</ref>

===Match details===
{{footballbox
|date       = 28 September 2003
|time       = 
|team1      = [[Armagh GAA|Armagh]] <br/> [[File:Armagh colours.PNG|40px]]
|score      = 0&ndash;09  &ndash;  0&ndash;12
|report     = 
|team2      = [[Tyrone GAA|Tyrone]] <br/> [[File:Derry colours.PNG|40px]]
|goals1     = Diarmuid Marsden (0-1) <br /> Oisín McConville (0-3, 3 frees) <br /> Steven McDonnell (0-2) <br /> John McEntee (0-1) <br /> Paddy McKeever (0-2, 2 frees)
|goals2     = Peter Canavan (0-5, 5 frees) <br /> Gerard Cavlan (0-1) <br /> Enda McGinley (0-1) <br /> Brian McGuigan (0-1) <br /> Eoin Mulligan (0-2, 2 frees) <br /> Stephen O'Neill (0-2)
|stadium    = [[Croke Park]], Dublin
|attendance = 79,391<ref>{{cite news |title=14 years on, the God of small forwards gets hands on Sam
|last=Humphries |first=Tom |newspaper=The Irish Times |date=29 September 2003}}</ref>
|referee    = Brian White 
}}
{| width=92% |
|-
|{{Football kit
 | alt        = Armagh kit
 | pattern_la = _shoulder_stripes_white_stripes
 | pattern_b  = _white_sholderg
 | pattern_ra =_shoulder_stripes_white_stripes
 | pattern_sh =
 | pattern_so = _whitetop2
 | leftarm    = FF8C00
 | body       = FF8C00
 | rightarm   = FF8C00
 | shorts     = 
 | socks      = FF8C00 
 | title      = Armagh
}}
|{{Football kit
|alt        = Tyrone kit
|pattern_la = _fredrikstad11h
|pattern_b  = _genoa1112t
|pattern_ra = _fredrikstad11h
|pattern_sh = _Caiman12Home
|pattern_so = _goyang13h
|leftarm    = 
|body       = 
|rightarm   = 
|shorts     = 
|socks      = 
|title      = Tyrone
}}
|}
{| width="92%"
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="4" cellpadding="0" align="center"
|colspan="4"|'''Armagh:'''
|-
!width=25|
|-
|'''1''' || [[Paul Hearty]] || || 
|-
|'''2''' || [[Francie Bellew]] || || {{yel}}
|-
|'''3''' || [[Enda McNulty]] || || 
|-
|'''4''' || [[Andy Mallon]] || {{suboff}} || 
|-
|'''5''' || [[Aidan O'Rourke (footballer)|Aidan O'Rourke]] ||  || 
|-	
|'''6''' || [[Kieran McGeeney]] <small>(Captain)</small> || || {{yel}}
|-
|'''7''' || Andy McCann || ||
|-
|'''8''' || Philip Loughran || || {{yel}}
|-
|'''9''' || [[Paul McGrane]] || || 
|-
|'''10''' || [[Rónán Clarke]] || {{suboff}} || 
|-
|'''11''' || [[John McEntee (Gaelic footballer)|John McEntee]] || {{suboff}} || 
|-
|'''12''' || [[Oisín McConville]] || || 
|-
|'''13''' || [[Steven McDonnell (Gaelic footballer)|Steven McDonnell]] ||  || 
|-
|'''14''' || [[Diarmuid Marsden]] || {{suboff}} {{subon}} || {{sentoff}}
|-
|'''15''' || Tony McEntee || || 
|-
|colspan=3|'''Substitutes:'''
|-
| || Paddy McKeever || {{subon}} || {{yel}}
|-
| || Kieran Hughes || {{subon}} || 
|-
| || Barry O'Hagan || {{subon}} || 
|-
|colspan=3|'''Manager:'''
|-
|colspan=3| [[Joe Kernan (Gaelic footballer)|Joe Kernan]]
|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="4" cellpadding="0" align="center"
|colspan="4"|'''Tyrone:'''
|-
!width=25|
|-
|'''1'''  || [[John Devine (Gaelic footballer)|John Devine]] || || 
|-
|'''2'''  || [[Ciaran Gourley]] || {{suboff}} || 
|-
|'''3'''  || [[Cormac McAnallen]] || || {{yel}}
|-
|'''4'''  || [[Ryan McMenamin]] || || 
|-
|'''5'''  || [[Conor Gormley]] || {{suboff}} || {{yel}}
|-
|'''6'''  || [[Gavin Devlin]] ||  || 
|-
|'''7'''  || [[Philip Jordan]] || || 
|-
|'''8'''  || [[Kevin Hughes (Gaelic footballer)|Kevin Hughes]] ||  || {{yel}}
|-
|'''9'''  || [[Sean Cavanagh]] || || 
|-
|'''10''' || [[Brian Dooher]] || || 
|-
|'''11''' || [[Brian McGuigan]] || {{suboff}} {{subon}} || {{yel}}
|-
|'''12''' || [[Gerard Cavlan]] || {{suboff}} || 
|-
|'''13''' || [[Enda McGinley]] ||  || 
|-
|'''14''' || [[Peter Canavan]] <small>(Captain)</small> || {{suboff}} {{subon}} || {{yel}}
|-
|'''15''' || [[Eoin Mulligan]] ||  || 
|-
|colspan=3|'''Substitutes:'''
|-
| || [[Stephen O'Neill]] || {{subon}} || 
|-
| || [[Colin Holmes (Gaelic footballer)|Colin Holmes]] || {{subon}} || 
|-
| || [[Chris Lawn]] || {{subon}} || 
|-
|colspan=3|'''Manager:'''
|-
|colspan=4| [[Mickey Harte]]
|}
|}

<small>'''References:'''</small><ref name="bbc">{{cite news |url=http://news.bbc.co.uk/sport1/hi/northern_ireland/gaelic_games/3147218.stm |title=Tyrone are All-Ireland champions |date=28 September 2003 |accessdate=11 June 2013 |work=BBC News}}</ref><ref>{{cite web |url=http://www.gaa.ie/about-the-gaa/gaa-history/final-teams/0/0/0/0/3/ |title=Recent Final Teams |website=www.gaa.ie |accessdate=11 June 2013}}</ref>

==Post-match==

Northern Ireland Secretary of State [[Paul Murphy (British politician)|Paul Murphy]] congratulated Tyrone on their victory.<ref name=homecoming>{{cite news|last=Regan|first=Claire|title=Party on, Tyrone; Thousands prepare for historic homecoming|newspaper=Belfast Telegraph|date=29 September 2003}}</ref> The [[Ulster GAA|Ulster Council]] of the GAA congratulated both sets of fans, in particular the Tyrone fans who formed a guard of honour for the Armagh players as they returned to their team bus.<ref>{{cite news|title=Fans get a pat on the back|newspaper=Irish News|date=30 September 2003}}</ref> [[Joe Kernan (Gaelic footballer)|Joe Kernan]], manager of Armagh, also praised the opposition fans: "...when the final whistle went and all the Tyrone supporters came running past me there wasn't one bad word said. To me that was great."<ref name="kernan interview">{{cite news |title=Kernan decides to remain as manager for a third year; Joe Kernan talks to Ian O'Riordan about the sending-off of Diarmuid Marsden and players diving |author= |newspaper=The Irish Times |date=30 September 2003 |page=22}}</ref>

Crowds gathered across Tyrone the following day to celebrate the arrival of the [[Sam Maguire Cup]]. The players' homecoming began at [[Aughnacloy, County Tyrone|Aughnacloy]] before moving on to [[Ballygawley, County Tyrone|Ballygawley]]<ref name="homecoming" /> and [[Omagh]], where upwards of 40,000 fans gathered.<ref>{{cite news |title=Omagh turns red and white as cup hits town |last=Cardwell |first=Peter |newspaper=Belfast Telegraph |date=30 September 2003 }}</ref> Despite their defeat, Armagh were greeted by hundreds of fans on the Louth&ndash;Armagh border on their return.<ref>{{cite news|last=McGonagle|first=Suzanne|title=Fans welcome defeated but defiant heroes onto home turf|newspaper=Irish News|date=30 September 2003}}</ref>

Armagh manager Joe Kernan claimed he would have resigned had his side won<ref name="kernan interview" /> but defeat encouraged him to continue.<ref name="kernan interview" /> He lauded his players for their effort nonetheless, saying: "..we've won an All-Ireland, and got back to the final. I think that's a phenomenal achievement."<ref name="kernan interview" /> He added that he was confident Armagh would win another title in the future.<ref name="kernan interview" /> Of the match itself, Kernan said: "I think if Steven McDonnell had got that goal towards the end, even with the man down I think we would have won the game. Big matches hinge on certain things and that was one of them." McDonnell applauded Conor Gormley's tackle which prevented him from scoring: "...I'd say it was one of the best tackles ever."<ref name="kernan interview" />

The match received extensive media coverage in Northern Ireland, especially from the predominantly [[Irish nationalism|nationalist]] ''[[Irish News]]''.<ref name="ni media">{{cite news |title=Northern attitudes still slow to change |last=Keenan |first=Dan |newspaper=The Irish Times |date=30 September 2003 |page=22 }}</ref> ''[[The Belfast Telegraph]]'' dedicated several pages to the match the following day, including the front and back covers,<ref name="ni media" /> whereas ''[[The News Letter]]'', a largely [[Unionism in Ireland|unionist]] publication,<ref name="ni media" /> had sparse coverage,<ref name="ni media" /> highlighting traditional attitudes to Gaelic games in Northern Ireland.<ref name="ni media" />

About a week after the final, a family in [[Coleraine]] were targeted in an allegedly sectarian attack, thought to be because they were flying a Tyrone GAA flag from their home.<ref>{{cite news |title=Second attack forces family to flee |last=Doyle |first=Simon |newspaper=Irish News |date=3 October 2003 |page=9 }}</ref>

Two years later, Peter Canavan's return from injury as a substitute in the final ten minutes finished 14th in [[RTÉ]]'s 2005 series ''[[Top 20 GAA Moments]]''.<ref>{{cite news|last=Canning|first=Margaret|title=Gaelic's 'Babe Ruth' loses top score title|newspaper=The Irish News}}</ref>

===Match controversies===

Joe Kernan was adamant that Diarmuid Marsden did not deserve to be sent off<ref name="kernan interview" /> and criticised players for pretending to be injured during the game.<ref name="kernan interview" /> The player himself also disagreed with the decision: "The umpire said I struck him but I just saw the man coming towards me and it was more a case of getting myself out of the way or protecting myself." He added: "I'd never been sent off for Armagh before and to be sent off in an All-Ireland final is hard to take. Hopefully I won't be remembered for that. And I wouldn't like to end the career on that note."<ref name="kernan interview" /> Kernan and Marsden contested the decision and subsequent ban, but the GAA's Games Administration Committee upheld the penalty.<ref>{{cite news|last=O'Riordan|first=Ian|title=Marsden's case to go further|date=14 November 2003 |newspaper=The Irish Times|page=24}}</ref> However, after taking their case to the Central Council, the ban was overturned.<ref>{{cite news|last=Moran|first=Sean|title=Marsden wins appeal over ban|newspaper=The Irish Times|date=8 December 2003|page=54}}</ref>

==References==
{{reflist|3}}

{{Tyrone Football Team 2003}}
{{Armagh Football Team 2003}}
{{All-Ireland Senior Football Championship}}

[[Category:2003 in Gaelic football|1]]
[[Category:2003 in Northern Ireland sport|All]]
[[Category:All-Ireland Senior Football Championship Finals]]
[[Category:Armagh GAA matches]]
[[Category:Tyrone GAA matches]]