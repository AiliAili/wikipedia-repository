{{good article}}
{{Infobox hurricane season
| Basin=SWI
| Year=1990
| Track=1989-1990 South-West Indian Ocean cyclone season summary.jpg
| First storm formed=December 14, 1989
| Last storm dissipated=May 21, 1990
| Strongest storm name=Walter-Gregoara
| Strongest storm pressure=927
| Strongest storm winds=92
| Average wind speed=10
| Total depressions=9 (4 unofficial)
| Total storms=9 (2 unofficial)
| Total hurricanes=4
| Fatalities=46
| Damages=1.5
| five seasons=[[1987–88 South-West Indian Ocean cyclone season|1987–88]], [[1988–89 South-West Indian Ocean cyclone season|1988–89]], '''1989–90''', [[1990–91 South-West Indian Ocean cyclone season|1990–91]], [[1991–92 South-West Indian Ocean cyclone season|1991–92]]
| Australian season=1989–90 Australian region cyclone season
| South Pacific season=1989–90 South Pacific cyclone season
}}
The '''1989–90 South-West Indian Ocean cyclone season''' was an average cyclone season, with nine [[tropical cyclone naming|named storms]] and five [[Tropical cyclone scales#South-Western Indian Ocean|tropical cyclones]] – a storm attaining [[maximum sustained wind]]s of at least {{convert|120|km/h|mph|abbr=on}}. The season officially ran from November 1, 1989, to April 30, 1990. Storms were officially tracked by the [[Météo-France]] office (MFR) on [[Réunion]] while the [[Joint Typhoon Warning Center]] (JTWC) in an unofficial basis. The first storm, [[Cyclone Alibera]], was the second longest-lasting tropical cyclone on record in the basin, with a duration of 22&nbsp;days. Alibera meandered and changed directions several times before striking southeastern [[Madagascar]] on January&nbsp;1, 1989, where it was considered the worst storm since 1925. The cyclone killed 46&nbsp;people and left widespread damage. Only the final storm of the year – Severe Tropical Storm Ikonjo – also had significant impact on land, when it left $1.5&nbsp;million in damage (1990&nbsp;[[United States dollar|USD]]) in the [[Seychelles]].

Of the remaining storms, several passed near the [[Mascarene Islands]] but did not cause much impact. In early February, Severe Tropical Storm Cezera and Tropical Cyclone Dety were active at the same time and interacted with each other through the process of the [[Fujiwhara interaction|Fujiwhara effect]]. Cyclone Gregoara was the strongest of the season, which originated as Cyclone Walter from the adjacent [[1989–90 Australian region cyclone season|Australian basin]]. Gregoara attained peak winds of {{convert|170|km/h|mph|abbr=on}} over the open waters of the Indian Ocean in March, although the JTWC considered Alibera to be stronger. In April, Moderate Tropical Storm Hanta approached the northwest coast of Madagascar, but dissipated over the [[Mozambique Channel]].

==Season summary==
<center>
<timeline>
ImageSize = width:1000 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270

AlignBars  = early
DateFormat = dd/mm/yyyy
Period     = from:01/12/1989 till:01/06/1990
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/12/1989
Colors     =
  id:canvas value:gray(0.88)
  id:GP value:red
  id:TD value:rgb(0.38,0.73,1)   legend:Tropical_Depression
  id:TS value:rgb(0,0.98,0.96)   legend:Moderate_Tropical_Storm
  id:ST value:rgb(0.80,1,1)      legend:Severe_Tropical_Storm
  id:TC value:rgb(1,1,0.80)      legend:Tropical_Cyclone
  id:IT value:rgb(1,0.76,0.25)   legend:Intense_Tropical_Cyclone
  id:VI value:rgb(1,0.38,0.38)   legend:Very_Intense_Tropical_Cyclone

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=
  barset:Hurricane width:11 align:left fontsize:S shift:(4,-4) anchor:till
  from:14/12/1989 till:07/01/1990 color:TC text:"Alibera (TC)"
  from:29/12/1989 till:09/01/1990 color:TC text:"Baomavo (TC)"
  from:31/01/1990 till:11/02/1990 color:ST text:"Cezera (STS)"
  from:02/02/1990 till:11/02/1990 color:TC text:"Dety (TC)"
  from:01/03/1990 till:08/03/1990 color:TC text:"Edisoana (TC)"
  from:07/03/1990 till:16/03/1990 color:TS text:"Felana (MTS)"
 barset:break
  from:13/03/1990 till:27/03/1990 color:IT text:"Walter-Gregoara (ITC)"
  from:11/04/1990 till:14/04/1990 color:TS text:"Hanta (MTS)"
  from:11/05/1990 till:21/05/1990 color:ST text:"Ikonjo (STS)"

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/12/1989 till:01/01/1990 text:December
  from:01/01/1990 till:01/02/1990 text:January
  from:01/02/1990 till:01/03/1990 text:February
  from:01/03/1990 till:01/04/1990 text:March
  from:01/04/1990 till:01/05/1990 text:April
  from:01/05/1990 till:01/06/1990 text:May

TextData =
   pos:(569,23)
   text:"(For further details, please see"
   pos:(713,23)
   text:"[[Tropical cyclone scales#Comparisons_across_basins|scales]])"
</timeline>
</center>
During the season, the [[Météo-France]] office (MFR) on [[Réunion]] island issued warnings in tropical cyclones within the basin. Using satellite imagery from [[National Oceanic and Atmospheric Administration]], the agency estimated intensity through the [[Dvorak technique]], and warned on tropical cyclones in the region from the coast of Africa to 90°&nbsp;[[Longitude|E]], south of the equator.<ref name="car">{{cite report|author=Philippe Caroff|date=June 2011|title=Operational procedures of TC satellite analysis at RSMC La Reunion|publisher=World Meteorological Organization|accessdate=2013-04-22|url=http://www.wmo.int/pages/prog/www/tcp/documents/RSMCLaReunionforIWSATC.pdf|format=PDF|display-authors=etal}}</ref> The [[Joint Typhoon Warning Center]] (JTWC), which is a joint [[United States Navy]]&nbsp;– [[United States Air Force]] task force, also issued tropical cyclone warnings for the southwestern Indian Ocean.<ref name="atcr">{{cite report|publisher=Joint Typhoon Warning Center|page=iii, 183–185|title=Annual Tropical Cyclone Report|accessdate=2014-10-07|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1990atcr.pdf|format=PDF}}</ref> The season's nine [[tropical cyclone naming|named storms]] and five [[Tropical cyclone scales#South-Western Indian Ocean|tropical cyclones]] – a storm attaining [[maximum sustained wind]]s of at least {{convert|120|km/h|mph|abbr=on}} – is the same as the long term average for the basin.<ref>{{cite web|author=Chris Landsea|author2=Sandy Delgado|title=Subject: E10) What are the average, most, and least tropical cyclones occurring in each basin?|series=Frequently Asked Questions|date=2014-05-01|publisher=Hurricane Research Division|accessdate=2014-10-13|url=http://www.aoml.noaa.gov/hrd/tcfaq/E10.html}}</ref>

Operationally, the MFR considered the tropical cyclone year to begin on August&nbsp;1 and continue to July&nbsp;31 of the following year.<ref name="car"/> However, the JTWC began the year on July&nbsp;1 and it lasted through June&nbsp;30 of the following year.<ref name="atcr"/> The latter agency tracked two short-lived tropical cyclones in July 1989, labeling them Tropical Cyclone 01S and 02S, but they are not considered part of MFR's season.<ref name="atcr"/> After these early storms, another tropical depression formed east of Diego Garcia on September&nbsp;21, classified as Tropical Cyclone 03S.<ref name="3bt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 HSK0390 (1989265S05085)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1989265S05085}}</ref> Forming from the [[near-equatorial trough]],<ref>{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=8|number=9|date=September 1989|title=Darwin Tropical Diagnostic Statement|accessdate=2014-10-07|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-198909.pdf|format=PDF}}</ref> the system moved generally to the southwest, dissipating on September&nbsp;27 as it approached [[Mauritius]].<ref name="3bt"/> In the next month, Tropical Cyclone 04S formed closer to Diego Garcia on October&nbsp;11. The JTWC classified it as a tropical depression on October&nbsp;13 but dropped advisories the next day. The system initially drifted to the south but later turned to the northwest, dissipating on October&nbsp;17.<ref name="atcr"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 HSK0490 (1989284S08080)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1989284S08080}}</ref> The final of a series of early tropical systems was a tropical depression that formed east of Diego Garcia on October&nbsp;28. It moved southeastward, classified by the JTWC as Tropical Cyclone 05S on October&nbsp;31. The agency briefly estimated peak winds of {{convert|65|km/h|mph|abbr=on}}, making it a tropical storm, before the storm looped back to the west and dissipated on November&nbsp;2.<ref name="atcr"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 HSK0590 (1989302S05079)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1989302S05079}}</ref> Later, the precursor to Australian [[1989–90 Australian region cyclone season#Tropical Cyclone Bessi|Tropical Cyclone Bessi]] was tracked in the eastern portion of the south-west Indian Ocean basin in the middle of April. The Australian [[Bureau of Meteorology]] (BOM) classified the system as a minimal tropical storm while still west of 90°&nbsp;E, although the MFR did not classify the system before it entered the Australian basin on April&nbsp;15.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Bessi (1990102S11092)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-13|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990102S11092}}</ref>

==Systems==
=== Tropical Cyclone Alibera ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Alibera dec 24 1989 1020Z.jpg
|Track=Alibera 1989 track.png
|Formed=December 16
|Dissipated=January 5
|1-min winds=135
|10-min winds=75
|Pressure=954
}}
{{main|Cyclone Alibera}}
The first named storm of the season, Alibera formed on December&nbsp;16, well to the northeast of [[Madagascar]]. For several days, it meandered southwestward while gradually intensifying. On December&nbsp;20, Alibera intensified to [[Tropical cyclone scales#South-Western Indian Ocean|tropical cyclone status]] with 10&#8209;minute winds of {{convert|120|km/h|mph|abbr=on}}, or the equivalent of a minimal hurricane. That day, the [[Joint Typhoon Warning Center]] (JTWC), an unofficial warning agency for the region, estimated peak 1&#8209;minute winds of {{convert|250|km/h|mph|abbr=on}}, while the [[Météo-France]] office in [[Réunion]] (MFR) estimated 10&#8209;minute winds of only {{convert|140|km/h|mph|abbr=on}}. After drifting erratically for several days, the storm began a steady southwest motion on December&nbsp;29 as a greatly weakened system. On January&nbsp;1, Alibera struck southeastern [[Madagascar]] near [[Mananjary, Fianarantsoa|Mananjary]], having re-intensified to just below tropical cyclone status. It weakened over land but again restrengthened upon reaching open waters on January&nbsp;3. The storm shifted directions while moving generally southward, dissipating on January&nbsp;5.<ref name="atcr"/><ref name="abt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Alibera (1989349S08065)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1989349S08065}}</ref> It was the second longest-lasting tropical cyclone in the basin since the start of satellite imagery, with a duration of 22&nbsp;days. Only [[List of South-West Indian Ocean cyclones before 1970#Cyclone Georgette .281968.29|Cyclone Georgette]] in 1968 lasted longer at 24&nbsp;days.<ref name="faq">{{cite web|author=Neal Dorst|author2=Anne-Claire Fontan|series= Records relatifs aux cyclones tropicaux|title=Sujet E6) Which tropical cyclone lasted the longest?|accessdate=2014-10-07|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/faq/FAQ_Ang_E.html}}</ref>

Early in its duration, Alibera produced gusty winds in the [[Seychelles]].<ref>{{cite news|accessdate=2014-10-10|title=Earthweek: A Diary of the Planet|author=Steve Newman|newspaper=Moscow-Pullman Daily News|date=1989-12-23|url=https://news.google.com/newspapers?nid=2026&dat=19891222&id=iyYuAAAAIBAJ&sjid=ttAFAAAAIBAJ&pg=6290,3541833}}</ref> Upon moving ashore in Madagascar, the cyclone lashed coastal cities with heavy rainfall and up to {{convert|250|km/h|mph|abbr=on}} wind gusts.<ref name="rw">{{cite web|work=United Nations Department of Humanitarian Affairs|date=January 1990|title=Madagascar Cyclone January 1990 UNDRO Information Reports 1 – 2|publisher=ReliefWeb|accessdate=2014-10-09|url=http://reliefweb.int/report/madagascar/madagascar-cyclone-jan-1990-undro-information-reports-1-2}}</ref> In Mananjary, nearly every building was damaged or destroyed, and locals considered it the worst storm since 1925.<ref name="usaid">{{cite report|publisher=Office of United States Foreign Disaster Assistance|title=OFDA Annual Report FY 1994|page=28|accessdate=2014-10-11|url=http://pdf.usaid.gov/pdf_docs/PNABL085.pdf|format=PDF}}</ref> Across the region, the cyclone destroyed large areas of crops, thousands of houses, and several roads and bridges. Alibera killed 46&nbsp;people and left 55,346&nbsp;people homeless. After the storm, the Malagasy government requested for international assistance.<ref name="rw"/>
{{Clear}}

=== Tropical Cyclone Baomavo ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Bavomavo jan 5 1990 0809Z.jpg
|Track=Bavomavo 1989 track.png
|Formed=January 2
|Dissipated=January 9
|1-min winds=85
|10-min winds=82
|Pressure=941
}}
A tropical disturbance formed on January&nbsp;2 to the northwest of the [[Cocos (Keeling) Islands|Cocos Islands]],<ref name="bbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Baomavo (1989364S05095)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-11|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1989364S05095}}</ref> which was tracked by the JTWC for the preceding few days before being classified as Tropical Cyclone 09S.<ref name="atcr"/> It originated from the [[monsoon trough]], which is an extended [[low pressure area]] within a [[convergence zone]].<ref name="jan">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=9|number=1|date=January 1990|title=Darwin Tropical Diagnostic Statement|accessdate=2014-10-11|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199001.pdf|format=PDF}}</ref> It gradually intensified as it moved slowly to the southwest due to a high pressure system, or [[ridge (meteorology)|ridge]], to the east.<ref name="jan"/> On January&nbsp;3, the system intensified into Moderate Tropical Storm Baomavo, and two days later attained tropical cyclone status while turning more to the south. The JTWC estimated peak 1&#8209;minute winds of {{convert|155|km/h|mph|abbr=on}} on January&nbsp;5, and on the next day the MFR estimated peak 10&#8209;minute winds of {{convert|150|km/h|mph|abbr=on}}.<ref name="bbt"/> An approaching [[cold front]] caused Baomavo to weaken while the storm turned southeastward.<ref name="jan"/> By January&nbsp;8, the system weakened to tropical depression status while looping back to the northwest,<ref name="bbt"/> steered by a [[ridge (meteorology)|ridge]] to the southwest.<ref name="b">{{cite report|series=Global tropical/extratropical cyclone climatic atlas|title=Tropical Cyclone Baomavo, 29 December 1989 - 8 January 1990|year=1996|work=National Climatic Data Center|accessdate=2015-01-20|url=http://webapp1.dlib.indiana.edu/virtual_disk_library/index.cgi/4274123/FID218/DATA/TROPIC/SWI_NAR/1989_19.NAR}}</ref> On the next day, Baomavo dissipated over the open waters of the Indian Ocean.<ref name="bbt"/>
{{Clear}}

=== Severe Tropical Storm Cezera ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Cezera feb 2 1990 0955Z.jpg
|Track=Cezera 1990 track.png
|Formed=January 31
|Dissipated=February 11
|1-min winds=80
|10-min winds=52
|Pressure=976
}}
On January&nbsp;31, a tropical disturbance formed just east of [[Agaléga]]. Moving southeastward, it intensified into Moderate Tropical Storm Cezera on February&nbsp;1,<ref name="cbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Cezera (1990032S11059)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-12|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990032S11059}}</ref> the same day that the JTWC began tracking it as Tropical Cyclone 14S.<ref name="atcr"/> Cezera quickly intensified, and the JTWC upgraded it to the equivalence of a minimal hurricane on February&nbsp;3 with 1&#8209;minute peak winds of {{convert|150|km/h|mph|abbr=on}}. By contrast, the MFR only estimated peak 10&#8209;minute winds of {{convert|95|km/h|mph|abbr=on}}.<ref name="cbt"/> By February&nbsp;4, Cezera began a [[Fujiwhara interaction]] with Tropical Cyclone Dety, which was located to the east; this caused the former storm to turn back to the northwest while gradually weakening.<ref name="cbt"/><ref name="feb">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=9|number=2|date=February 1990|title=Darwin Tropical Diagnostic Statement|accessdate=2014-10-12|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199002.pdf|format=PDF}}</ref> On February&nbsp;6, the storm turned to the south and later southeast, weakening to tropical depression status that day. Cezera briefly re-intensified into a moderate tropical storm on February&nbsp;7, but weakened again on the next day while passing just north of [[St. Brandon]]. After turning more to the east-northeast, the system turned sharply southward on February&nbsp;10, and dissipated the next day.<ref name="cbt"/>
{{Clear}}

=== Tropical Cyclone Dety ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Dety feb 4 1990 0932Z.jpg
|Track=Dety 1990 track.png
|Formed=February 2
|Dissipated=February 11
|1-min winds=95
|10-min winds=72
|Pressure=954
}}
A tropical depression developed within the monsoon trough on February&nbsp;2 to the southwest of Diego Garcia,<ref name="feb"/><ref name="dbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Dety (1990031S09070)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-12|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990031S09070}}</ref> about {{convert|1125|km|mi|abbr=on}} east of Cezura.<ref name="cbt"/><ref name="dbt"/> The JTWC had been tracking the system for several days previously, classifying it as Tropical Cyclone 16S also on February&nbsp;2.<ref name="atcr"/> With an [[anticyclone]] &ndash; a high pressure area over the system &ndash; providing favorable conditions,<ref name="feb"/> the depression quickly intensified while moving generally south-southwestward. It became Moderate Tropical Storm Dety on February&nbsp;3 and a tropical cyclone the next day. The MFR estimated peak 10&#8209;minute winds of {{convert|135|km/h|mph|abbr=on}}, while the JTWC assessed stronger 1&#8209;minute winds of 175&nbsp;km/h (110&nbsp;mph).<ref name="dbt"/> Around that time, Dety began a Fujiwhara interaction with Tropical Storm Cezura to the west, causing the former storm to turn to the east-southeast. Increased wind shear weakened the cyclone,<ref name="feb"/> although it maintained much of its intensity through February&nbsp;7 as a severe tropical storm. However, Dety quickly fell to tropical disturbance status the next day while undergoing a counterclockwise loop. After turning back to the southeast, Dety remained a weak system for several days, dissipating on February&nbsp;11.<ref name="dbt"/>
{{Clear}}

=== Tropical Cyclone Edisoana ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Edisaona mar 4 1990 0933Z.jpg
|Track=Edisaona 1990 track.png
|Formed=March 1
|Dissipated=March 8
|1-min winds=100
|10-min winds=72
|Pressure=954
}}
The MFR began tracking a tropical disturbance on March&nbsp;1 between Mauritius and Diego Garcia,<ref name="ebt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Edisoana (1990058S16075)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-12|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990058S16075}}</ref> which was followed by the JTWC for several days previously and classified as Tropical Cyclone 18S.<ref name="atcr"/> Within a day, it intensified into Moderate Tropical Storm Edisoana while tracking southwestward. On March&nbsp;4, the JTWC upgraded the storm to the equivalent of a minimal hurricane, and on the next day the MFR followed suit by upgrading Edisoana to tropical cyclone status. The latter agency estimated peak 10&#8209;minute winds of {{convert|135|km/h|mph|abbr=on}}, while the JTWC assessed a peak 1&#8209;minute intensity of {{convert|185|km/h|mph|abbr=on}}. Around that time, the storm passed west of [[Rodrigues]] island. Edisoana accelerated southward and gradually weakened,<ref name="ebt"/> influenced by an approaching [[trough (meteorology)|trough]]. On March&nbsp;7, the storm began transitioning into an [[extratropical cyclone]],<ref name="et">{{cite journal|author=Kyle S. Griffin|author2=Lance F. Bosart|title=The Extratropical Transition of Tropical Cyclone Edisoana (1990)|journal=Monthly Weather Review|date=August 2014|accessdate=2014-10-12|url=http://journals.ametsoc.org/doi/abs/10.1175/MWR-D-13-00282.1?journalCode=mwre|doi=10.1175/MWR-D-13-00282.1|volume=142|pages=2772–2793}}</ref> completing it by the next day.<ref name="ebt"/> The extratropical cyclone rapidly intensified due to the influence of the trough and a nearby ridge,<ref name="et"/> later being absorbed by the [[westerlies]].<ref name="e">{{cite report|series=Global tropical/extratropical cyclone climatic atlas|title=Tropical Cyclone Edisaona, 28 February-9 March, 1990|year=1996|work=National Climatic Data Center|accessdate=2015-01-20|url=http://webapp1.dlib.indiana.edu/virtual_disk_library/index.cgi/4274123/FID218/DATA/TROPIC/SWI_NAR/1990_3.NAR}}</ref>
{{Clear}}

=== Moderate Tropical Storm Felana ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Felana mar 13 1990 0936Z.jpg
|Track=Felana 1990 track.png
|Formed=March 7
|Dissipated=March 16
|1-min winds=45
|10-min winds=35
|Pressure=991
}}
On March&nbsp;7, a tropical depression formed in the eastern portion of the basin to the east-southeast of Diego Garcia. The nascent quickly intensified into Moderate Tropical Storm Felana by March&nbsp;8,<ref name="fbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Felana (1990065S07083)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-13|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990065S07083}}</ref> the same day that the JTWC began tracking it as Tropical Cyclone 22S.<ref name="atcr"/> Felana moved steadily to the southwest, although on March&nbsp;10 it turned to the west-northwest, followed by another turn to the south-southwest on the next day. During this time, the MFR only estimated peak 10&#8209;minute winds of {{convert|65|km/h|mph|abbr=on}}, although the JTWC assessed a peak 1&#8209;minute intensity of {{convert|85|km/h|mph|abbr=on}}. Upon turning back southward, Felana passed east of Rodrigues on March&nbsp;12. It weakened to tropical depression status on March&nbsp;15 and dissipated the following day, having curved back to the southeast.<ref name="fbt"/>
{{Clear}}

=== Intense Tropical Cyclone Walter-Gregoara ===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Gregoara mar 17 1990 0854Z.jpg
|Track=Walter-Gregoara 1990 track.png
|Formed=March 13 (entered basin)
|Dissipated=March 27
|1-min winds=110
|10-min winds=92
|Pressure=927
}}
On March&nbsp;4, a tropical low formed from the monsoon trough in the Australian basin southwest of the Cocos Islands. It executed a large loop and later turned back to the west due to a [[ridge (meteorology)|ridge]] to the south, during which it was named Walter by the BOM. On March&nbsp;13, Walter crossed 90°&nbsp;E into the south-west Indian Ocean and was renamed Gregoara by the Mauritius Weather Service.<ref>{{cite web|title=Tropical Cyclone Walter|accessdate=2014-10-13|publisher=Bureau of Meteorology|url=http://www.bom.gov.au/cyclone/history/pdf/walter90.pdf|format=PDF}}</ref><ref name="mar">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=9|number=3|date=March 1990|title=Darwin Tropical Diagnostic Statement|accessdate=2014-10-13|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199003.pdf|format=PDF}}</ref> However, the MFR did not begin issuing advisories until March&nbsp;15, when Gregoara reached 85°&nbsp;E.<ref name="gbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Gregoara:Walter (1990062S13091)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-13|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990062S13091}}</ref> The JTWC classified the system as Tropical Cyclone 23S, which was a separate number from when the storm existed in the Australian basin.<ref name="atcr"/> Gregoara moved to the southwest, intensifying into a tropical cyclone on March&nbsp;16, the same day that the JTWC upgraded it to the equivalent of a minimal hurricane. On the next day, the cyclone attained peak winds – the MFR estimated 10&#8209;minute winds of {{convert|170|km/h|mph|abbr=on}}, while the JTWC estimated 1&#8209;minute winds of {{convert|205|km/h|mph|abbr=on}}. The storm subsequently weakened slowly, and was below tropical cyclone status by March&nbsp;19. Three days later, Gregoara turned to the southeast as a weakened tropical depression,<ref name="gbt"/> subjected to cooler waters and stronger wind shear,<ref name="mar"/> and it became extratropical.<ref name="e4">{{cite report|series=Global tropical/extratropical cyclone climatic atlas|title=Tropical Depression Gregoara (ex-Walter), 15-27 March, 1990|year=1996|work=National Climatic Data Center|accessdate=2015-01-20|url=http://webapp1.dlib.indiana.edu/virtual_disk_library/index.cgi/4274123/FID218/DATA/TROPIC/SWI_NAR/1990_4.NAR}}</ref> For several days, the system moved slowly over the southern Indian Ocean, turning to the southwest and later to the southeast before dissipating on March&nbsp;27.<ref name="gbt"/>
{{Clear}}

=== Moderate Tropical Storm Hanta ===
{{Infobox hurricane small
|Basin=SWI
|Image=Hanta apr 12 1990 1059Z.jpg
|Formed=April 11
|Dissipated=April 14
|1-min winds=45
|10-min winds=35
|Pressure=991
|Track=
}}
A tropical disturbance originated on April&nbsp;11 just north of the [[Comoros]] in the [[Mozambique Channel]].<ref name="hbt"/> Originally it only consisted of a spiral area of thunderstorms, but it gradually organized.<ref name="h">{{cite report|series=Global tropical/extratropical cyclone climatic atlas|title=Tropical Depression Hanta, 10-14 April, 1990|year=1996|work=National Climatic Data Center|accessdate=2015-01-20|url=http://webapp1.dlib.indiana.edu/virtual_disk_library/index.cgi/4274123/FID218/DATA/TROPIC/SWI_NAR/1990_6.NAR}}</ref> It moved southeastward and intensified into Moderate Tropical Storm Hanta on April&nbsp;12, passing just north of [[Mayotte]].<ref name="hbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Hanta (1990101S07048)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-13|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990101S07048}}</ref> On the next day, the JTWC classified it as Tropical Cyclone 27S with peak 1&#8209;minute winds of {{convert|85|km/h|mph|abbr=on|round=5}}, although the agency did not include the name ''Hanta'' in advisories.<ref name="atcr"/> By contrast, the MFR only estimated peak 10&#8209;minute winds of {{convert|65|km/h|mph|abbr=on}}. Hanta approached the northwest coast of Madagascar on April&nbsp;14, passing within {{convert|11|km|mi|abbr=on}} of the [[Anjajavy Forest]] before turning back to the west,<ref name="hbt"/> due to a ridge to the south.<ref name="h"/> Later that day, the system weakened and dissipated.<ref name="hbt"/>

In the [[Glorioso Islands]] north of Madagascar, Hanta produced 50&nbsp;km/h (31&nbsp;mph) wind gusts and {{convert|32|mm|in|abbr=on}} of rainfall. Later, gusts reached {{convert|65|km/h|mph|abbr=on}} on Mayotte, and the storm dropped {{convert|75|mm|in|abbr=on}} of precipitation over 24&nbsp;hours.<ref name="h"/>
{{Clear}}

=== Severe Tropical Storm Ikonjo ===
{{Infobox hurricane small
|Basin=SWI
|Image=Ikonjo may 15 1990 1002Z.jpg
|Track=Ikonjo 1990 track.png
|Formed=May 11
|Dissipated=May 21
|1-min winds=55
|10-min winds=52
|Pressure=976
}}
The final storm of the season formed as a tropical disturbance on May&nbsp;11 west-southwest of Diego Garcia. It moved erratically at first, initially to the west, followed by a turn to the south and later a small loop.<ref name="ibt"/> Its movement during this time and for its duration was dictated by a powerful ridge to the south.<ref name="i">{{cite report|series=Global tropical/extratropical cyclone climatic atlas|title=Tropical Depression Ikonjo, 11-21 May 1990|year=1996|work=National Climatic Data Center|accessdate=2015-01-20|url=http://webapp1.dlib.indiana.edu/virtual_disk_library/index.cgi/4274123/FID218/DATA/TROPIC/SWI_NAR/1990_8.NAR}}</ref> During this time, the system remained weak, although it intensified into Moderate Tropical Storm Ikonjo on May&nbsp;14.<ref name="ibt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1990 Ikonjo (1990130S03081)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-13|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1990130S03081}}</ref> The JTWC began classifying the storm as Tropical Cyclone 29S about two days prior.<ref name="atcr"/> After becoming a tropical storm, Ikonjo began a steadier westward movement, gradually curving back to the west-northwest, and bringing it just north of Agaléga on May&nbsp;16. Two days later, the storm quickly intensified to attain peak 10&#8209;minute winds of {{convert|95|km/h|mph|abbr=on}}, which made Ikonjo a severe tropical storm according to the MFR. Around that time it stalled, even drifting slightly to the west, before resuming a northwest motion,<ref name="ibt"/> influenced by a ridge to the south.<ref name="pre">{{cite web|page=43|date=July 2008|work=United Nations Development Programme|title=Disaster risk profile of the Republic of Seychelles|author=Denis Chang Seng|author2=Richard Guillande|accessdate=2014-10-13|url=http://www.preventionweb.net/files/18276_18276disasterriskprofileofseychelle.pdf|format=PDF}}</ref> Ikonjo subsequently weakened while moving near or through the [[Outer Islands (Seychelles)|Outer Islands]] of the Seychelles. On May&nbsp;21, Ikonjo dissipated at the low latitude of 5°&nbsp;S.<ref name="ibt"/>

Late in its duration, Ikonje became a rare storm to affect the nation of Seychelles. It passed nearest to [[Desroches Island]], where it destroyed much of the island's hotel. On the primary island of [[Mahé, Seychelles|Mahé]], Ikonje produced strong winds reaching {{convert|83|km/h|mph|abbr=on}} at [[Seychelles International Airport]], strong enough to knock over several trees. Nationwide, the storm caused $1.5&nbsp;million (1990&nbsp;[[United States dollar|USD]]) in damage and two injuries.<ref name="pre"/> A ship passing through the center of Ikonjo reported wind gusts of 148&nbsp;km/h (92&nbsp;mph).<ref name="i"/>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
{{commons|1989-90 Southern Hemisphere tropical cyclone season}}
*Atlantic hurricane seasons: [[1989 Atlantic hurricane season|1989]], [[1990 Atlantic hurricane season|1990]]
*Pacific hurricane seasons: [[1989 Pacific hurricane season|1989]], [[1990 Pacific hurricane season|1990]]
*Pacific typhoon seasons: [[1989 Pacific typhoon season|1989]], [[1990 Pacific typhoon season|1990]]
*North Indian Ocean cyclone seasons: [[1989 North Indian Ocean cyclone season|1989]], [[1990 North Indian Ocean cyclone season|1990]]

==References==
{{Reflist|2}}

{{TC Decades|Year=1980|basin=South-West Indian Ocean|type=cyclone|shem=yes}}

{{DEFAULTSORT:1989-90 South-West Indian Ocean cyclone season}}
[[Category:South-West Indian Ocean cyclone seasons]]
[[Category:1989–90 South-West Indian Ocean cyclone season]]
[[Category:Articles which contain graphical timelines]]