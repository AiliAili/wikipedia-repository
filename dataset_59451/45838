{{More footnotes|date=February 2009}}
{{italic title}}
'''''Mimesis: The Representation of Reality in Western Literature''''' ({{lang-de|Mimesis: Dargestellte Wirklichkeit in der abendländischen Literatur}}) is a book of [[literary criticism]] by [[Erich Auerbach]], and his most well known work. Written while Auerbach was teaching in [[Istanbul]], [[Turkey]], where he fled after being ousted from his professorship in Romance [[Philology]] at the [[University of Marburg]] by the [[Nazis]] in 1935,<ref>{{cite journal
  | last = 
  | first = 
  | authorlink = 
  | title = Rev. of ''Scholarship in Times of Extremes: Letters of Erich Auerbach (1933–46), on the Fiftieth Anniversary of His Death''
  | journal = [[Publications of the Modern Language Association|PMLA]]
  | volume = 122
  | issue = 3
  | pages = 742–62
  | publisher = [[Modern Language Association]]
  | location = 
  | year = 2007
  | url = 
  | issn = 0030-8129
  | doi = 10.1632/pmla.2007.122.3.742
  | id = 
  | accessdate = 2009-02-10
  | last1 = Auerbach
  | first1 = Erich}}</ref> it was first published in 1946 by A. Francke Verlag.  

''Mimesis'' famously opens with a comparison between the way the world is represented in [[Homer]]’s ''[[Odyssey]]'' and the way it appears in the [[Bible]]. From these two seminal [[Western culture|Western]] texts, Auerbach builds the foundation for a unified theory of [[representation (arts)|representation]] that spans the entire history of Western literature, including even the [[Modernist]] novelists writing at the time Auerbach began his study.

==Overview==
''Mimesis'' gives an account of the way in which [[everyday life]] in its seriousness has been represented by many Western writers, from ancient [[Ancient Greece|Greek]] and [[Ancient Rome|Roman]] writers such as [[Petronius]] and [[Tacitus]], early [[Christian]] writers such as [[Augustine of Hippo|Augustine]], [[Medieval]] writers such as [[Chretien de Troyes]], [[Dante]], and  [[Giovanni Boccaccio|Boccaccio]], [[Renaissance]] writers such as [[Montaigne]], [[Rabelais]], [[Shakespeare]] and [[Cervantes]], seventeenth-century writers such as [[Molière]] and [[Jean Racine|Racine]], [[Age of Enlightenment|Enlightenment]] writers such as [[Voltaire]], nineteenth-century writers such as [[Stendhal]], [[Balzac]], [[Flaubert]], and [[Émile Zola|Zola]], all the way up to twentieth-century writers such as [[Proust]], and [[Virginia Woolf|Woolf]]. Despite his treatment of the many major works, Auerbach apparently did not think he was comprehensive enough, and apologized in the original publication in 1946 explaining that he had access only to the 'insufficient' resources available in the library at [[Istanbul University]] where he worked.<ref>{{cite book
  | last = Auerbach
  | first = Erich
  | authorlink = 
  | others=Willard R. Trask (trans.)
  | title = Mimesis: The Representation of Reality in Western Literature
  | publisher = Princeton UP
  | year = 1953
  | location = Princeton
  | page =
  | url = 
  | doi = 
  | id = 
  | isbn = 0-691-01269-5}} 557.</ref> Many scholars consider this relegation to primary texts a happy accident of history, since in their view one of the great strengths of Auerbach’s book is its focus on fine-grained close reading of the original texts rather than an evaluation of critical works.

The mode of literary criticism in which ''Mimesis'' operates is often referred to among contemporary critics as [[historicism]], since Auerbach largely regarded the way reality was represented in the literature of various periods to be intimately bound up with social and intellectual conventions of the time in which they were written. Auerbach considered himself a historical [[Perspectivism|perspectivist]] in the German tradition (he mentioned [[Hegel]] in this respect) exploring specific features of [[literary genre|style]], [[grammar]], [[syntax]], and [[diction]] claims about much broader cultural and historical questions. Of ''Mimesis'', Auerbach wrote that his "purpose is always to write history."  

He is in the same German tradition of [[philology]] as [[Ernst Robert Curtius|Ernst Curtius]], [[Leo Spitzer]], and [[Karl Vossler]], having a mastery of many languages and epochs and all-inclusive in its approach, incorporating just about any intellectual endeavor into the discipline of literary criticism. 

Auerbach was a [[Romance language]] specialist, which explains his admitted bias towards treating texts from French compared to other languages. [[Chaucer]] and [[Wordsworth]] are not mentioned even in passing, though [[Shakespeare]] and [[Virginia Woolf]] are given full chapters and [[Dickens]] and [[Henry Fielding]] make appearances.

===Chapters===

{| class="wikitable"
|-
! # !! Chapter title !! Main works discussed
|-
| 1 || [[Odysseus' scar (Auerbach)|Odysseus' Scar]] || ''[[Odyssey]]'' by [[Homer]] and [[Book of Genesis|Genesis]] 22
|-
| 2 || Fortunata || ''[[Satyricon]]'' by [[Petronius]], ''[[Annals]]'' Book 1 by [[Tacitus]] and ''[[Gospel of Mark|Mark]]'' ch. 14
|-
| 3 || The Arrest of Peter Valvomeres || ''[[Res Gestae (Ammianus Marcellinus)|Res Gestae]]'' by [[Ammianus Marcellinus]]
|-
| 4 || Sicharius and Chramnesindus || ''[[Historia Francorum|History of the Franks]]'' by [[Gregory of Tours]]
|-
| 5 || Roland Against Ganelon || ''[[Chanson de Roland]]''
|-
| 6 || The Knight Sets Forth || ''[[Yvain]]'' by [[Chrétien de Troyes]]
|-
| 7 || Adam and Eve || ''The medieval mystery play [[Mystère d'Adam]]; [[St. Bernard of Clairvaux]]; [[St. Francis of Assisi]]''
|-
| 8 || [[Farinata degli Uberti|Farinata]] and [[Cavalcante de' Cavalcanti|Cavalcante]] || ''[[Inferno (Dante)|Inferno]]'', ''[[The Divine Comedy]]'' by [[Dante Alighieri]]
|-
| 9 || Frate Alberto || ''[[The Decameron]]'' by [[Giovanni Boccaccio]]
|-
| 10 || Madame Du Chastel || ''[[Le Réconfort de Madame du Fresne]]'' by [[Antoine de la Sale]]
|-
| 11 || The World in Pantagruel's Mouth || ''[[Gargantua and Pantagruel]]'' by [[François Rabelais]]
|-
| 12 || L'Humaine Condition || ''[[Essais|Essays]]'' by [[Michel de Montaigne]]
|-
| 13 || The Weary Prince || ''Henry IV, Parts [[Henry IV, Part 1|1]] and [[Henry IV, Part 2|2]]'' by [[William Shakespeare]]
|-
| 14 || The Enchanted Dulcinea || ''[[Don Quixote]]'' by [[Miguel de Cervantes]]
|-
| 15 || The Faux Dévot || ''[[Tartuffe]]'' by [[Molière]]
|-
| 16 || The Interrupted Supper || ''[[Manon Lescaut]]'' by [[Abbé Prévost]]; ''[[Candide]]'' by [[Voltaire]]; ''[[Mémoires]]'' by [[Louis de Rouvroy, duc de Saint-Simon]]
|-
| 17 || Miller the Musician || ''[[Luise Miller]]'' by [[Friedrich Schiller]]
|- 
| 18 || In the Hôtel de la Mole || ''[[The Red and the Black]]'' by [[Stendhal]] and ''[[Madame Bovary]]'' by [[Gustave Flaubert]]
|-
| 19 || Germinie Lacerteux || ''[[Germinie Lacerteux]]'' by [[Goncourt brothers|Edmond and Jules de Goncourt]] and ''[[Germinal (novel)|Germinal]]'' by [[Émile Zola]]
|-
| 20 || The Brown Stocking || ''[[To the Lighthouse]]'' by [[Virginia Woolf]] and ''[[In Search of Lost Time]]'' by [[Marcel Proust]]
|}

==Position and evaluation of rhetoric==
To the consternation of his colleague, Ernst Curtius, Auerbach's work is marked by an openly anti-rhetorical position. Classical writers such as [[Homer]], [[Tacitus]] and [[Petronius]], as well as Medieval theologians (except St. [[Augustine of Hippo|Augustine]]), and writers of the seventeenth century, like [[Jean Racine|Racine]], are criticized for adherence to the rhetorical doctrine of "styles" with their corresponding subject matters: the low style's association with the comedic and the popular classes, and the elevated style's association with the tragic, the historic and the heroic. Auerbach sees the Bible as opposing this rhetorical doctrine in its serious and poignant portrayals of common folk and their encounter with the divine. As Auerbach notes in chapter two when discussing the New Testament:

<blockquote>But the spirit of rhetoric&mdash;a spirit which classified subjects in ''genera'' and invested every subject with a specific form of style as one garment becoming it in virtue of its nature [i.e. lower classes with the farcical low-style, upper classes with the tragic, the historic and the sublime elevated-style]&mdash;could not extend its dominion to them [the Bible writers] for the simple reason that their subject would not fit into any of the known genres. A scene like Peter's denial fits into no antique genre. It is too serious for comedy, too contemporary and everyday for tragedy, politically too insignificant for history&mdash;and the form which was given it is one of such immediacy that its like does not exist in the literature of antiquity.<ref name="mimesis">{{cite book
  | last = Auerbach
  | first = Erich
  | authorlink = 
  | others=Willard R. Trask (trans.)
  | title = Mimesis: The Representation of Reality in Western Literature
  | publisher = Princeton UP
  | year = 1953
  | location = Princeton
  | pages = 
  | url = 
  | doi = 
  | id = 
  | isbn = 0-691-01269-5}} 45.</ref></blockquote>     

The Bible will ultimately be responsible for the "[[mixed style]]" of Christian rhetoric, a style that is described by Auerbach in chapter seven as the "antithetical fusion" or "merging" of the high and low style.  The model is Christ's [[Incarnation]] as both ''sublimitas'' and ''humilitas''.  This mixture ultimately leads to a "popular realism" seen in the religious plays and sermons of the 12th Century.  Auerbach also discusses the development of an intermediate or middle style due to Medieval influences from the Bible and [[Courtly Love]] (see chapters nine and fifteen on [[Boccaccio]] and [[Molière]]). This development of an intermediate and then ultimately another "mixed style" (Shakespeare, Hugo) leads to what Auerbach calls the "modern realism" of the nineteenth-century (see chapter nineteen on [[Flaubert]]).      
  
Auerbach champions writers during periods under the sway of rhetorical forms of writing like [[Gregory of Tours]] and St. [[Francis of Assisi]], whose Latin was poor and whose rhetorical education was minimal, but who were still able to convey vivid expression and feeling. He also champions the diarist [[Louis de Rouvroy, duc de Saint-Simon|Saint-Simon]] who wrote about the late seventeenth and early eighteenth century French court. Completely free of the absolute constraints of style found in Racine or the superficial use of reality found in [[Antoine François Prévost|Prévost]] or [[Voltaire]], Saint-Simon's portraits of court life are considered by Auerbach, somewhat surprisingly, to be the precursor of [[Proust]] (an admirer of Saint-Simon) and [[Émile Zola|Zola]].

==Critical reception==
''Mimesis'' is almost universally respected for its penetrating insights on the particular works it addresses but is frequently criticized for what is sometimes regarded as its lack of a single overarching claim. For this reason, individual chapters of the book are often read independently. Most critics praise his sprawling approach for its reveling in the complexities of each work and [[Epoch (reference date)|epoch]] without resorting to generalities and [[reductionism]]. However, a work of this complexity comes with problems of its own. Auerbach has the habit sometimes of using a minor work as a representation of an era, such as upholding the obscure Antoine de la Sale as representative of the inferiority of Medieval prose literature while ignoring monuments like the [[Prose Lancelot]] or [[Prose Tristan]].  

By far the most frequently reprinted chapter is chapter one, "[[Odysseus' scar (Auerbach)|Odysseus' Scar]]" in which Auerbach compares the scene in book 19 of [[Homer]]’s ''[[Odyssey]]'', when [[Odysseus]] finally returns home from his two decades of warring and journeying, to [[Book of Genesis|Genesis]] 22, the story of [[Binding of Isaac|The Binding of Isaac]]. Highlighting the rhetorically determined simplicity of characters in the ''Odyssey'' (what he calls the "external") against what he regards as the psychological depth of the figures in the [[Old Testament]], Auerbach suggests that the Old Testament gives a more powerful and historical impression than the ''Odyssey'', which he classifies as closer to "legend" in which all details are leisurely fleshed out and all actions occur in a simple present – indeed even flashbacks are narrated in the present tense. 

Auerbach summarizes his comparison of the texts as follows:

<blockquote>The two styles, in their opposition, represent basic types: on the one hand [''The Odyssey'' 's] fully externalized description, uniform illustration, uninterrupted connection, free expression, all events in the foreground, displaying unmistakable meanings, few elements of historical development and of psychological perspective; on the other hand [in the Old Testament], certain parts brought into high relief, others left obscure, abruptness, suggestive influence of the unexpressed, "background" quality, multiplicity of meanings and the need for interpretation, universal-historical claims, development of the concept of the historically becoming, and preoccupation with the problematic.</blockquote>

Auerbach concludes by arguing that the "full development" of these two styles, the rhetorical tradition with its constraints on representing reality and the Biblical or "realist" tradition with its engagement of everyday experience, exercised a "determining influence upon the representation of reality in European literature."  

It is in the context of this comparison between the Biblical and the Homeric that Auerbach draws his famous conclusion that the Bible’s claim to truth is "tyrannical," since <blockquote>  
What he [the writer of the Old Testament] produced then, was not primarily oriented towards "realism" (if he succeeded in being realistic, it was merely a means, not an end): it was oriented to truth.</blockquote> 

However, by the time Auerbach treats the work of [[Flaubert]] we have come full circle. Like the Biblical writers whose faith in the so-called "tyrannical" truth of God produces an authentic expression of reality, Flaubert's "faith in the truth of language" (ch. 18) likewise represents "an entire human experience."

==References==
{{reflist}}

==Bibliography==
* Auerbach, Erich. ''Mimesis: The Representation of Reality in Western Literature.'' Fiftieth Anniversary Edition. Trans. Willard Trask. Princeton: Princeton University Press, 2003.
* Bakker, Egbert. "Mimesis as Performance: Rereading Auerbach’s First Chapter." ''[[Poetics Today]]'' 20.1 (1999): 11–26.
* [[Chris Baldick|Baldick, Chris]]. “Realism.” ''Oxford Concise Dictionary of Literary Terms.'' New York: Oxford University Press, 1996. 184.
* Bremmer, Jan.  "Erich Auerbach and His Mimesis." ''Poetics Today'' 20.1 (1999): 3–10.
* [[Calin, William]]. "Erich Auerbach’s ''Mimesis'' – ’Tis Fifty Years Since: A Reassessment." ''Style'' 33.3 (1999): 463–74.
* Doran, Robert. "[http://muse.jhu.edu/journals/new_literary_history/toc/nlh38.2.html Literary History and the Sublime in Erich Auerbach´s ''Mimesis'']."  ''New Literary History'' 38.2 (2007): 353–69.
* Green, Geoffrey. "Erich Auerbach." ''Literary Criticism & the Structures of History: Erich Auerbach & Leo Spitzer.'' Nebraska: University of Nebraska Press, 1982.
* Holmes, Jonathan, and Streete, Adrian, eds. ''Refiguring ''Mimesis'': Representation in Early Modern Literature.'' Hatfield: University of Hertfordshire Press, 2005.
* Holquist, Michael. “Erich Auerbach and the Fate of Philology Today.” ''Poetics Today'' 20.1 (1999): 77-91.
* Landauer, Carl. "Mimesis and Erich Auerbach’s Self-Mythologizing." ''German Studies Review'' 11.1 (1988): 83-96.
* [[Seth Lerer|Lerer, Seth]]. ''Literary History and the Challenge of Philology: The Legacy of Erich Auerbach.'' Stanford: Stanford University Press, 1996.
* [[Anthony Nuttall|Nuttall, A. D.]]. "New Impressions V: Auerbach’s ''Mimesis''." ''Essays in Criticism'' 54.1 (2004): 60-74.

==Further reading==
* [http://www.newyorker.com/magazine/2013/12/09/the-book-of-books "The Book of Books: Erich Auerbach and the making of ''Mimesis''"], by [[Arthur Krystal]] in ''[[The New Yorker]]'' (Dec. 9, 2013)


[[Category:1946 essays]]
[[Category:Aesthetics literature]]
[[Category:Contemporary philosophical literature]]
[[Category:Literary criticism]]