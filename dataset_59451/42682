<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= HD 153 Motor-Möwe
 | image=File:Dittmar HD.153 D-EKIH STT 29.07.65 edited-2.jpg
 | caption=HD 153A-1 Motor-Möwe two-seat aircraft at [[Stuttgart Airport]] in 1965
}}{{Infobox Aircraft Type
 | type=Light civil utility aircraft
 | national origin=West Germany 
 | manufacturer=Dittmar
 | designer=[[Heini Dittmar]]
 | first flight=November 1953
 | introduced=1956
 | retired=
 | status=out of service
 | primary user=private pilot owners and aero clubs
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Dittmar HD 53 Möwe]]
 | variants with their own articles=
}}
|}
The '''Dittmar HD 153 Motor-Möwe''' was a [[West Germany|West German]] [[light aircraft]] that was first flown in November 1953.

==Design and development==
The Motor-Möwe, designed by [[Heini Dittmar]] who had designed the HD 53 Möwe sailplane, was a motorized development of this sailplane and was initially designed to be powered by engines of {{convert|65|-|85|hp|kW|abbr=on|disp=flip}}.  The HD 153 prototype was a two-seat side-by-side high wing monoplane powered by a {{convert|65|hp|kW|abbr=on|disp=flip}} [[Continental A65]] engine and was of wooden construction with detachable wing and tail assemblies to facilitate road transportation when needed. A second prototype aircraft was fitted with a {{convert|60|hp|kW|abbr=on|disp=flip}} [[Hirth]] engine.<ref>Green & Pollinger, 1955, p. 65</ref>

==Operational history==
The aircraft was designed for private and club use and served as a trainer and glider tug. Heini Dittmar was killed in 1960 when his Motor-Möwe crashed near Essen/Mulheim airport. Small numbers of the type were completed by the end of 1960 and on 1 January 1961 four HD 153 and four HD 156 Motor–Möwen appeared in the West German civil aircraft register.<ref>Gerhard, 1961</ref> By 1965 four HD 153 and five HD 156 Motor-Möwen were registered in West Germany.<ref>Green & Pollinger, 1965, p. 73</ref> By 2007, no examples were known to be active.

==Variants==
[[File:Dittmar HD.156A-1 M.Mowe D-ELUR Egelsbach 280765 edited-3.jpg|thumb|right|HD 156 three-seat aircraft showing the revised front and side cabin glazing of this model.]]
; HD 153 : two-seat aircraft
::'''HD 153A-1''' production two-seaters
; HD 156 : three-seat aircraft fitted with additional side windows

==Specifications (HD 153 with A65 engine) ==
{{Aircraft specs
|ref=<ref name=apjul56>{{cite journal|journal=Air Pictorial|date=July 1956|title=Dittmar HD 153 Motor-Möwe}}</ref>
|prime units?=met
|crew=one
|capacity=one passenger ({{convert|736|lb|kg|abbr=on|disp=flip}} payload)
|length m=
|length ft=21  
|length in=4
|length note=
|span m=
|span ft=39  
|span in=4
|span note=
|height m=
|height ft=5
|height in=3
|height note=
|wing area sqm=
|wing area sqft=197
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=815.5
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=1542
|max takeoff weight note=
|fuel capacity={{convert|75|l|USgal impgal|abbr=on|2}} + optional {{convert|200|l|USgal impgal|abbr=on|2}} overload tank
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental A65]]
|eng1 type=four-cylinder piston
|eng1 kw=<!-- prop engines -->
|eng1 hp=65
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=2
|prop name=fixed pitch wooden propeller
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=5
|prop dia in=10.8
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=165
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=144
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=650
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=1872
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=4.5 hours (13 hours with overload tank)
|ceiling m=
|ceiling ft=14760
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=6 minutes to {{convert|1000|m|abbr=on}}
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=</li>
* '''Fuel consumption:''' {{convert|24|mpgimp|abbr=on}}

}}

==References==
{{reflist}}
* {{cite book|last=Gerhard|first=Peter|title=World Aviation Register - West Germany 1961 edition|publisher=Air-Britain|location=Acton, London}}
* {{cite book|last=Green and Pollinger|first=William and Gerald|title=The Aircraft of the World|year=1955|publisher=Macdonald & Co. (Publishers) Ltd|location=London}}
* {{cite book|last=Green and Pollinger|first=William and Gerald|title=The Aircraft of the World|year=1965|publisher=Macdonald & Co. (Publishers) Ltd|location=London}}

<!-- ==Further reading== -->

==External links==
* [http://aviation-safety.net/wikibase/wiki.php?id=166562 28-APR-1960 Heini Dittmars fatal test flight]
* [http://aviation-safety.net/wikibase/wiki.php?id=170369 11-APR-1959 wing separated during descent after glider towing]

<!-- Navboxes go here -->

{{DEFAULTSORT:Dittmar HD 153 Motor-Mowe}}
[[Category:German civil utility aircraft 1950–1959]]
[[Category:High-wing aircraft]]
[[Category:Single-engined tractor aircraft]]