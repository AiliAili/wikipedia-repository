{{Infobox book
| name = White-Jacket
| image = White-Jacket 1st.jpg
| caption = First edition title page
| alt = 
| author = [[Herman Melville]]
| illustrator = 
| cover_artist = 
| country = United States, England
| language = English
| subject = 
| genre = [[Adventure fiction]]
| published = {{Plainlist|
* 1850 {{small|(New York: Harper & Brothers)}}
* 1850 {{small|(London: Richard Bentley)}}
}}
| media_type = Print
| awards = 
| isbn = 
| oclc = 
| dewey = 
| congress = 
| preceded_by = [[Redburn]]
| followed_by = [[Moby-Dick]]
}}

'''''White-Jacket; or, The World in a Man-of-War''''' is the fifth book by American writer [[Herman Melville]], first published in London in 1850.<ref>Hayford, Harrison, "Chronology," which is included at the back of all three volumes of the Library of America edition of Melville's writings.</ref> The book is based on the author's fourteen months service in the United States Navy, aboard the frigate ''USS "Neversink"'' (actually the ''[[USS United States (1797)|USS United States]])''.

==Overview==
Based on Melville's experiences as a common seaman aboard the frigate {{USS|United States|1797|6}} from 1843 to 1844 and stories that other sailors told him, the novel is severely critical of virtually every aspect of American naval life and thus qualifies as Melville's most politically strident work.{{Citation needed|date=May 2007}} At the time, though, the one thing that journalists and politicians focused on in the novel was its graphic descriptions of flogging and the horrors caused by its arbitrary use; in fact, because Harper & Bros. made sure the book got into the hands of every member of Congress, ''White-Jacket'' was instrumental in abolishing flogging in the U.S. Navy forever. Melville scholars also acknowledge the huge number of parallels between ''White-Jacket'' and ''[[Billy Budd]]'' and view the former as a rich source for possible interpretations of the latter.<ref>Hayford, Harrison and Sealts, Jr., Merton, eds. ''Billy Budd, Sailor (An Inside Narrative)'', "Editors' Introduction", p. 31. ISBN 0-226-32132-0.</ref>

The symbolism of the color white, introduced in this novel in the form of the narrator's jacket, is more fully expanded upon in ''[[Moby-Dick]],'' where it becomes an all-encompassing "blankness."<ref>Melville, Herman and Bryant, ed. ''Tales, Poems, and Other Writings'', p. xxv. ISBN 0-679-64105-X.</ref> The mixture of journalism, history, and fiction; the presentation of a sequence of striking characters; the metaphor of a sailing ship as the world in miniature—all of these prefigure his next novel, ''[[Moby-Dick]].''

==Characters==

''This list is not exhaustive.''

*White-Jacket, the main character and narrator, so nicknamed because his coat is the only white one on board; a novice sailor (at least on a naval ship), his jacket often gets him into trouble, mostly because of its whiteness
*Jack Chase, a sailor of British origin who is universally regarded by his fellow seamen and even by the officers as the epitome of a true and good sailor; he shows contempt for any man who has shipped out on a whaler
*Captain Claret, a captain of usually severe tendencies, his name reflects the fact that he is also an alcoholic
*Commodore
*Selvagee, a foppish lieutenant whose leadership style is tyrannical
*Mad Jack, a lieutenant whose leadership style is collegial
*Lemsford, a sailor who aspires to be a poet
*Quoin, a sailor "indefatigable in attending to his duties, which consisted in taking care of one division of the guns"<ref>''White-Jacket'', Quality Paperback Book Club edition (reprint), p. 51. 1996. No ISBN found.</ref>
*Nord, a sailor of surly look and melancholy disposition; when White-Jacket first encounters him, the only friend Nord has on board is Lemsford
*Williams
*Wooloo, the commodore's Polynesian servant
*Old Revolver
*Old Combustibles
*Chaplain
*Shakings
*Bland, the ship's master-at-arms
*[[Emperor]] [[Don Pedro II]]

==Publication history==
''White-Jacket'' was published in London by Richard Bentley on February 1, 1850, and in New York by Harper & Brothers on March 21, 1850.<ref name="thorp">{{cite book|last=Thorp |first=Willard |editor=Harrison Hayford |editor2=Hershel Parker |editor3=G. Thomas Tanselle |title=White-Jacket |chapter=Historical Notes |publisher=The Northwestern University Press and The Newberry Library |location=Evanston and Chicago |year=1970 |pages=407-08 |isbn=978-0810102583}}</ref> Melville referred to it and his previous book ''[[Redburn]]'' as "two ''jobs'' which I have done for money—being forced to it as other men are to sawing wood."<ref name=Delbanco111>[[Andrew Delbanco|Delbanco, Andrew]]: ''Melville, His World and Work''. New York: Alfred A. Knopf, 2005, p. 111. ISBN 0-375-40314-0</ref>

==Legal impact==
At the urging of [[New Hampshire]] Senator [[John P. Hale]], whose daughter, Lucy, would later become the fiancee of [[John Wilkes Booth]], the murderer of President [[Abraham Lincoln]],<ref>O'Reilly, William. ''Killing Lincoln'', New York: Holt, 2011, pp. 28-30.</ref> the United States Congress banned [[Flagellation|flogging]] on all U.S. ships in September 1850.<ref name=Congress>George Hodak, "Congress Bans Maritime Flogging," ''ABA Journal'' September 1850, p. 72. Found at [http://www.abajournal.com/magazine/article/congress_bans_maritime_flogging/ ABA Journal website].  Accessed October 18, 2010.</ref>  He was inspired by Melville's "vivid description of flogging, a brutal staple of 19th century naval discipline" in his "novelized memoir" ''White-Jacket''.<ref name=Congress />

The man whom Melville based the fictional Commodore on, [[Thomas ap Catesby Jones]], a former commander of the [[USS United States (1797)|USS ''United States'' (1797)]], was brought up on a [[court-martial]] in 1850 and found guilty on three counts mostly related to "oppression" of junior officers. Jones was relieved of command for two and a half years. In 1853, President Millard Fillmore reinstated him and in 1858, the United States Congress restored his pay.<ref name="smith">{{cite book |author= Gene A. Smith |title= ''Thomas ap Catesby Jones, Commodore of Manifest Destiny'' |publisher= Annapolis, MD: Naval Institute Press |year= 2000 |isbn= 1-55750-848-8 }}</ref>

==References==
{{Reflist|2}}

==External links==
{{Wikiquote}}
{{wikisource}}
* {{Gutenberg|no=10712|name=White-Jacket}} (plain text and HTML)
* [https://archive.org/search.php?query=title%3Ajacket%20creator%3A%22Melville%22%20AND%20mediatype%3Atexts ''White-Jacket''] at [[Internet Archive]], [[Google Books]] (scanned books original editions illustrated)
* {{librivox book | dtitle=White-Jacket | stitle=White Jacket | author=Herman Melville}}

{{Herman Melville}}

{{Authority control}}
[[Category:1850 novels]]
[[Category:Novels by Herman Melville]]
[[Category:19th-century American novels]]
[[Category:Novels set in the 1840s]]
[[Category:Novels set on ships]]