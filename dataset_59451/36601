{{good article}}
{|{{Infobox ship begin |infobox caption= |italic title=}}
{{Infobox ship image
|Ship image=[[File:Italian torpedo cruiser Aretusa 1895 IWM Q 22386.jpg|300px]]
|Ship caption=''Aretusa'' in 1895
}}
{{Infobox ship career
|Hide header=
|Ship country=[[Kingdom of Italy|Italy]]
|Ship flag={{shipboxflag|Kingdom of Italy}}
|Ship name=''Aretusa''
|Ship owner=
|Ship namesake=[[Arethusa (mythology)|Arethusa]]
|Ship ordered=
|Ship builder=[[Cantiere navale fratelli Orlando]], [[Livorno]]
|Ship laid down=1 June 1889
|Ship launched=14 March 1891
|Ship acquired=
|Ship commissioned=1 September 1892
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship honors=
|Ship fate=Discarded December 1912
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Partenope|cruiser|0}} [[torpedo cruiser]]
|Ship displacement=Normal: {{convert|833|MT|sp=us}}
|Ship length= {{convert|73.1|m|ftin|abbr=on}}
|Ship beam= {{convert|8.22|m|ftin|abbr=on}}
|Ship draft= {{convert|3.48|m|ftin|abbr=on}}
|Ship propulsion=*2 × [[Marine steam engine#Triple or multiple expansion|Vertical triple-expansion steam engines]]
*2 [[screw propeller]]s
|Ship power 4 × [[locomotive boiler]]s
*{{convert|3884|to|4422|ihp|abbr=on}}
|Ship speed= {{convert|18.1|to|20.8|kn}}
|Ship range={{convert|1800|nmi|lk=in}} at {{convert|10|kn}}
|Ship complement=96&ndash;121
|Ship armament=*1 × {{convert|120|mm|abbr=on}} gun
*6 × {{convert|57|mm|abbr=on}} guns
*3 × {{convert|37|mm|abbr=on}} guns
*5 × {{convert|450|mm|abbr=on|1}} [[torpedo tube]]s
|Ship armor=
}}
|}
'''''Aretusa''''' was a [[torpedo cruiser]] of the {{sclass-|Partenope|cruiser|4}} built for the Italian ''[[Regia Marina]]'' (Royal Navy) in the 1880s. [[keel laying|Laid down]] in June 1889 at the [[Cantiere navale fratelli Orlando]] shipyard, she was [[ship launching|launched]] in March 1891 and was [[ship commissioning|commissioned]] in September 1892. Her main armament were her six [[torpedo tube]]s, which were supported by a battery of ten small-caliber guns. ''Aretusa'' spent most of her career in the main Italian fleet, where she was primarily occupied with training exercises. At the start of the [[Italo-Turkish War]] in September 1911, she was assigned to the Red Sea Squadron in [[Italian Eritrea]]. She bombarded Ottoman positions in the [[Arabian Peninsula]] and took part in a blockade of the coast. Worn out by the end of the war in October 1912, ''Aretusa'' was sold for scrap that December and [[ship breaking|broken up]].

==Design==
{{main|Partenope-class cruiser}}

''Aretusa'' was {{convert|73.1|m|ftin|sp=us}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|8.22|m|ftin|abbr=on}} and an average [[draft (hull)|draft]] of {{convert|3.48|m|ftin|abbr=on}}. She displaced {{convert|833|MT|sp=us}} normally. Her propulsion system consisted of a pair of horizontal [[triple-expansion engine|triple-expansion]] [[steam engine]]s, each driving a single [[screw propeller]], with steam supplied by four coal-fired [[locomotive boiler]]s. Specific figures for ''Aretusa''{{'}}s engine performance have not survived, but the ships of her class had top speeds of {{convert|18.1|to|20.8|kn|lk=in}} at {{convert|3884|to|4422|ihp|lk=in}}. The ship had a cruising radius of about {{convert|1800|nmi|lk=in}} at a speed of {{convert|10|kn}}. She had a crew of between 96 and 121.<ref name=G347>Gardiner, p. 347</ref>

''Aretusa'' was armed with a [[main battery]] of one {{convert|120|mm|abbr=on}} /40 gun and six {{convert|57|mm|abbr=on}} /43 guns mounted singly.{{ref|Alpha|α}} She was also equipped with three {{convert|37|mm|abbr=on|1}} /20 guns in single mounts. Her primary offensive weapon was her five {{convert|450|mm|abbr=on|1}} [[torpedo tube]]s. The ship was protected by an armored deck that was up to {{convert|1.6|in|abbr=on}} thick; her [[conning tower]] was armored with the same thickness of steel plate.<ref name=G347/>

==Service history==
[[File:NH 88661 cruiser Aretusa.tiff|thumb|left|''Aretusa'' in the late 1890s]]
''Aretusa'' was laid down at the [[Cantiere navale fratelli Orlando]] (Orlando Brothers' Shipyard) in [[Livorno]] on 1 June 1889, and was launched on 14 March 1891. After [[fitting-out]] work was completed, the ship was commissioned into the fleet on 1 September 1892.<ref name=G347/> During the 1893 fleet maneuvers, ''Aretusa'' served with the 3rd Division of the Reserve Squadron, along with the [[protected cruiser]]s {{ship|Italian cruiser|Vesuvio||2}} and {{ship|Italian cruiser|Ettore Fieramosca||2}} and four [[torpedo boat]]s. During the maneuvers, which lasted from 6 August to 5 September, the ships of the Reserve Squadron defended against a simulated attack by the Active Squadron, which [[Military simulation|gamed]] a French attack on the Italian fleet.<ref>Clarke & Thursfield, pp. 202&ndash;203</ref> In 1895, ''Aretusa'' was stationed in the 2nd Maritime Department, split between [[Taranto]] and  [[Naples]], along with most of the [[torpedo cruiser]]s in the Italian fleet. These included her [[sister ship]]s {{ship|Italian cruiser|Partenope||2}}, {{ship|Italian cruiser|Minerva||2}}, {{ship|Italian cruiser|Euridice||2}}, {{ship|Italian cruiser|Iride||2}}, {{ship|Italian cruiser|Urania||2}}, and {{ship|Italian cruiser|Caprera||2}}, the four {{sclass-|Goito|cruiser|1}}s, and {{ship|Italian cruiser|Tripoli||2}}.<ref>Garbett 1895, p. 90</ref> As of 1898, ''Aretusa'' was assigned to the Active Squadron, with included the [[ironclad warship|ironclads]] {{ship|Italian ironclad|Sicilia||2}} and {{ship|Italian ironclad|Sardegna||2}} and two other cruisers.<ref>Garbett 1898, p. 200</ref>

At the start of the [[Italo-Turkish War]] in September 1911, ''Aretusa'' was stationed in [[Italian Eritrea]] in the Red Sea Squadron. Italian naval forces in the region also included five protected cruisers and several smaller vessels.<ref>Beehler, pp. 9&ndash;11</ref> Shortly after the start of the war on 2 October, ''Aretusa'' and the [[gunboat]] {{ship|Italian gunboat|Volturno||2}} encountered the Ottoman torpedo cruiser {{ship|Ottoman cruiser|Peyk-i Şevket||2}} off [[Al Hudaydah]]. In a short engagement, the Italians vessels forced the Ottoman ship to flee into Al Hudaydah, bombarded the port facilities, and then withdrew.<ref>Stephenson, p. 62</ref> ''Peyk-i Şevket'' was later interned in British-controlled [[Suez]]<ref>Langensiepen & Güleryüz, p. 25</ref> The threat of an Ottoman attack from the [[Arabian Peninsula]] led the Italian High Command to reinforce the Red Sea Squadron; the additional ships included another cruiser and several [[destroyer]]s. The protected cruiser {{ship|Italian cruiser|Piemonte||2}} and two destroyers annihilated a force of seven Ottoman gunboats in the [[Battle of Kunfuda Bay]] on 7 January 1912.<ref name=B51>Beehler, p. 51</ref>

Following the neutralization of Ottoman naval forces in the region, ''Aretusa'' and the rest of the Italian ships then commenced a bombardment campaign against the Turkish ports in the Red Sea before declaring a [[blockade]] of the city of Al Hudaydah on 26 January.<ref name=B51/> On 27 July and 12 August, ''Aretusa'', her sister ship ''Caprera'', and ''Piemonte'' conducted two bombardments of Al Hudaydah. During the second attack, they destroyed an Ottoman ammunition dump.<ref>Beehler, p. 90</ref> With the threat of an Ottoman attack greatly reduced, the High Command thereafter began to withdraw forces from the Red Sea Squadron. By the end of August, the unit was reduced to three protected cruisers, ''Aretusa'', ''Caprera'' and two auxiliaries.<ref>Beehler, p. 93</ref> On 14 October, the Ottoman government agreed to sign a peace treaty, ending the war.<ref>Beehler, p. 95</ref> ''Aretusa''{{'}}s career ended shortly thereafter; the ''Regia Marina'' discarded the ship in December and she was subsequently [[ship breaking|broken up]] for scrap.<ref name=G347/>

==Notes==
:{{note|Alpha|α|"/40" refers to the length of the gun in terms of [[caliber (artillery)|calibers]], meaning that the length of the barrel is 40 times its internal diameter.}}

{{reflist|20em}}

==References==
*{{Cite book |last=Beehler|first=William Henry|title=The History of the Italian-Turkish War: September 29, 1911, to October 18, 1912|year=1913|location=Annapolis|publisher=United States Naval Institute|url=https://books.google.com/books?id=OWcoAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false|oclc=1408563}}
* {{cite book|last1=Clarke|first1=George S.|last2=Thursfield|first2=James R.|title=The Navy and the Nation|year=1897|location=London|publisher=John Murray|oclc=669157022}}
* {{cite journal|editor-last=Garbett|editor-first=H.|title=Naval Notes &ndash; Italy|journal=Journal of the Royal United Service Institution|publisher=J. J. Keliher|location=London|year=1898|volume=XLII|pages=199&ndash;204|oclc=8007941}}
* {{cite journal|editor-last=Garbett|editor-first=H.|title=Naval and Military Notes &ndash; Italy|journal=Journal of the Royal United Service Institution|publisher=J. J. Keliher|location=London|year=1895|volume=XXXIX|pages=81&ndash;111|oclc=8007941}}
* {{cite book |editor-last=Gardiner|editor-first=Robert|title=Conway's All the World's Fighting Ships: 1860–1905|year=1979|location=London|publisher=Conway Maritime Press|isbn=0-85177-133-5}}
*{{cite book|last1=Langensiepen|first1=Bernd|last2=Güleryüz|first2=Ahmet|year=1995|title=The Ottoman Steam Navy 1828–1923|publisher=Conway Maritime Press|location=London|isbn=978-0-85177-610-1|lastauthoramp=y}}
*{{cite book|last=Stephenson|first=Charles|title=A Box of Sand: The Italo-Ottoman War 1911-1912|location=Ticehurst|publisher=Tattered Flag Press|year=2014|isbn=9780957689220}}

{{Partenope-class cruiser}}

{{DEFAULTSORT:Aretusa}}
[[Category:Partenope-class cruisers]]
[[Category:Ships built in Livorno]]
[[Category:1891 ships]]