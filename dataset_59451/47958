{{jargon|date=March 2017}}
{{About|the CSS framework|the Objective-C framework|Foundation Kit}}
{{Infobox software
| name                   = ZURB Foundation
| logo                   = 
| screenshot             = 
| caption                = 
| developer              = ZURB
| released               = {{start date and age|2011|09}}
| frequently updated     = yes <!-- Release version update? Don't edit this page, just click on the version number! -->
| status                 = Active
| latest release version = 6.3.0 (Proxima Centauri b)
| latest release date    = {{release date and age|2016|12|15}} <ref>[https://github.com/zurb/foundation-sites/releases/ Foundation Changelog] on Dec 15, 2016</ref>
| latest preview version =
| latest preview date    = 
| operating system       = [[Cross-platform]]
| platform               =
| programming language   = [[HTML]], [[CSS]], [[Sass (stylesheet language)|Sass]] and [[JavaScript]]
| genre                  = [[HTML]] and [[CSS]]-based design templates
| license                = [[MIT License]]
| website                = {{url|http://foundation.zurb.com/}}

}}
'''Foundation''' is a responsive front-end framework. Foundation provides a responsive grid and [[HTML]] and [[CSS]] UI components, templates, and code snippets, including typography, forms, buttons, navigation and other interface components, as well as optional [[JavaScript]] extensions. Foundation is maintained by ZURB and is an [[open source]] project.

== Origin ==

Foundation emerged as a ZURB project to develop front-end code faster and better. In October 2011, ZURB released Foundation 2.0 as open-source under the [[MIT License]].<ref name="zurb">{{cite web|url=http://www.zurb.com/article/805/start-here-build-everywhere-announcing-fo|title=Announcing Foundation by ZURB|accessdate=22 Aug 2012}}</ref> ZURB released Foundation 3.0 in June 2012,<ref>{{cite web|url=http://techcrunch.com/2012/06/29/zurb-launches-foundation-3-to-take-on-twitters-bootstrap-framework/|title=ZURB Launches Foundation 3 To Take On Twitter's Bootstrap Framework|accessdate=22 Aug 2012}}</ref> 4.0 in February 2013,<ref>{{cite web|url=http://techcrunch.com/2013/02/28/responsive-design-framework-foundation-4-launches/|title=Responsive Design Framework Foundation 4 Goes Mobile-First, Switches From jQuery To Zepto|accessdate=28 Feb 2013}}</ref> 5.0 in November 2013, 6.0 in November 2015. The team started working on the next version of Foundation for Sites 7 which most likely will drop support for older browsers and implement newer technologies like flexbox or maybe calculated grid system.

Foundation for Email was released in September 2013.

Foundation for Apps was released in December 2014.

== Features ==

Foundation was designed for and tested on numerous browsers and devices. It is a mobile first responsive framework built with Sass/SCSS giving designers best practices for rapid development. The framework includes most common patterns needed to rapidly prototype a responsive site. Through the use of Sass mixins, Foundation components are easily styled and simple to extend.

Since version 2.0 it also supports [[Responsive Web Design|responsive design]].<ref>{{cite web|url=http://www.alistapart.com/articles/dive-into-responsive-prototyping-with-foundation/|title=A List Apart: Dive into Responsive Prototyping with Foundation|accessdate=22 Aug 2012}}</ref> This means the graphic design of web pages adjusts dynamically, taking into account the characteristics of the device used (PC, tablet, mobile phone). Additionally, since 4.0 it has taken a mobile-first approach, designing and developing for mobile devices first, and enhancing the web pages and applications for larger screens.<ref>{{cite web|url=http://venturebeat.com/2013/02/28/zurb-foundation-4/|title=Zurb releases Foundation 4, a mobile-first, ‘forward-thinking dev/designer’s dream’|accessdate=28 Feb 2013}}</ref>

Foundation is open source and available on Github. Developers are encouraged to participate in the project and make their own contributions to the platform.

== Structure and function ==
{{Unreferenced section|date=August 2012}}
Foundation is modular and consists essentially of a series of [[Sass (stylesheet language)|Sass stylesheets]] that implement the various components of the toolkit. Component stylesheets can be included via Sass or by customizing the initial Foundation download. Developers can adapt the Foundation file itself, selecting the components they wish to use in their project.

Adjustments are possible through a central configuration stylesheet. More profound changes are possible by changing the Sass variables.

The use of Sass stylesheet language allows the use of variables, functions and operators, nested selectors, as well as so-called mixins.

Since version 3.0, the configuration of Foundation also has a special "Customize" option in the documentation. Moreover, developers use  on a form to choose the desired components and adjust, if necessary, the values of various options to their needs. The subsequently generated package already includes the pre-built CSS style sheet.

=== Grid system and responsive design ===

Foundation comes standard with a 940&nbsp;pixel wide, flexible grid layout. The toolkit is fully responsive to make use of different resolutions and types of devices: mobile phones, portrait and landscape format, tablets and PCs with a low and high resolution (widescreen). This adjusts the width of the columns automatically.

=== Understanding CSS stylesheet ===

Foundation provides a set of stylesheets that provide basic style definitions for all key HTML components. These provide a browser and system-wide uniform, modern appearance for formatting text, tables and form elements.

=== Re-usable components ===

In addition to the regular HTML elements, Foundation contains other commonly used interface elements. These include buttons with advanced features (for example, grouping of buttons or buttons with drop-down option, make and navigation lists, horizontal and vertical tabs, navigation, breadcrumb navigation, pagination, etc.), labels, advanced typographic capabilities, and formatting for messages such as warnings.

=== JavaScript components and plug-ins ===

[http://foundation.zurb.com/docs/v/3.2.5/javascripts.php Official Zurb Foundation Main documentation page for Javascript]

The JavaScript components of Foundation 4 were moved from [[jQuery]] Javascript library to Zepto, on a presumption that the physically smaller, but API-compatible alternative to JQuery would prove faster for the user. However, Foundation 5 moved back to the newer release JQuery-2. "jQuery 2.x has the same API as jQuery 1.x, but does not support Internet Explorer 6, 7, or 8." the official Zurb blog explains, [http://zurb.com/article/1293/why-we-dropped-zepto Why we dropped Zepto]; and the unsigned writer claims that the switch back was due to issues of compatibility with customized efforts; and that performance was found to be not as good, on use testing with the newer jQuery-2.

Foundation jQuery components provide general user-interface elements and branded extensions. The list includes: dialog, tooltips, carousels, alerts, clearing, cookies, dropdown, forms, joyride, magellan, orbit, placeholder, reveal, section, topbar, flex video, and many others.

Plug-ins that use jQuery can be installed that are incorporated into the Foundation framework to provide advanced functionality in any UI area, including animation and "off-canvas" elements like slide-in menus.

JQuery elements like forms will need to be connected to a backend infrastructure (server-based database and scripting) using tools and methods outside the Foundation framework in order to work. External services like MailChimp are still installed as for any static HTML page, and do not require a home-rolled backend.

[[JQuery]] has become an acknowledged standard part of the evolution of the web. Wikipedia claims 65% of the top 100 Javascript sites employ it. Javascript itself is considered the de facto standard for frontend web development work, with HTML and CSS (by general consensus.)

== Use ==

There are three levels of integration for Foundation: CSS, SASS, and Ruby on Rails with the Foundation Rails Gem.<ref>[http://foundation.zurb.com/docs/index.html Foundation Documentation] on Thursday, April 30, 2015</ref>

=== CSS ===

To use Foundation CSS, default or custom CSS packages can be downloaded from the download page and installed into the appropriate web server folders.  Foundation is then integrated into HTML page markup.<ref>[http://foundation.zurb.com/docs/css.html Foundation Getting Started] on Thursday, April 30, 2015</ref>

=== SASS ===

The Foundation SASS install uses Ruby, NodeJS, and Git to install Foundation sources.  Foundation then provides a command line interface to modify and compile source to CSS for use in HTML page markup.<ref>[http://foundation.zurb.com/docs/sass.html Foundation Getting Started] on Thursday, April 30, 2015</ref>

=== Foundation Rails Gem ===

The Foundation Rails gem can be installed by adding "gem 'foundation-rails'" to the Rails Application Gemfile.<ref>[http://foundation.zurb.com/docs/applications.html Foundation Getting Started] on Thursday, April 30, 2015</ref>

==References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://foundation.zurb.com/}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:HTML]]
[[Category:Web design]]
[[Category:CSS frameworks]]
[[Category:Software using the MIT license]]