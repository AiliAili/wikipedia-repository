{{Infobox journal
| title = Nitric Oxide
| cover =[[File:Nitric_Oxide_FrontCover.gif]]
| editor = Neil Hogg
| discipline = [[Biological functions of nitric oxide|Nitric oxide]]
| formernames =
| abbreviation = Nitric Oxide
| publisher = [[Elsevier]]
| country =
| frequency = 8/year
| history = 1997-present
| openaccess =
| license =
| impact = 3.548 
| impact-year = 2011
| website = http://www.journals.elsevier.com/nitric-oxide-biology-and-chemistry/
| link1 = http://www.sciencedirect.com/science/journal/10898603
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 35147437
| LCCN = 97660095
| CODEN = NIOXF5
| ISSN = 1089-8603
| eISSN = 1089-8611
}}
'''''Nitric Oxide''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] and official journal of the [[Nitric Oxide Society]]. The journal covers the broad field of [[nitric oxide]] research and includes basic and clinical topics such as [[cell biology]], [[molecular biology]], [[biochemistry]], [[immunology]], [[pathology]], [[genetics]], [[physiology]], [[pharmacology]], and disease processes. 

== Abstracting and indexing ==
The journal is abstracted and indexed in [[EMBASE]], [[EMBiology]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 3.548.<ref name=WoS>{{cite book |year=2012 |chapter=Nitric Oxide |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-24 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.journals.elsevier.com/nitric-oxide-biology-and-chemistry/}}
* [http://www.nitricoxide.ws/ Nitric Oxide Society]

[[Category:Biochemistry journals]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1997]]