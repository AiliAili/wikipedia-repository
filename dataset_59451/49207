{{Orphan|date=August 2015}}

{{Use dmy dates|date=October 2012}}
{{Use South African English|date=October 2012}}

{{Infobox person
| name            = Avi Lasarow
| image           = File:SAAWARDLASAROWA.jpeg
| caption         = Winner South African Business Awards 2015
| birth_name      = 
| birth_date      = 1976
| birth_place     = [[Johannesburg, South Africa]]
| death_date      = 
| death_place     = 
| nationality     = South African / British
| spouse          = Kelly Lasarow
| height          = 
| weight          = 
| ethnicity       = South African
| alias           = 
| website         =  www.honconsul.za.com
}}

'''Avi Lasarow''' (born 1976) is a South African entrepreneur. Described as lifestyle genetics pioneer,<ref>{{Cite web|title = South African lifestyle genetics pioneer joins the Guild of Entrepreneurs|url = http://www.thesouthafrican.com/south-african-lifestyle-genetics-pioneer-joins-the-guild-of-entrepreneurs/|website = The South African|access-date = 2016-02-23|first = Sarah|last = Adams}}</ref> he’s known for his work associated with [[DNA testing]] for various purposes. He’s recognised for his success relating to the commercialisation of Hair Alcohol Testing used by the UK court system as a means to keep children safe <ref>http://www.managementtoday.co.uk/features/1107499/my-week-avi-lasarow-trimega-laboratories/</ref><ref>http://www.journalonline.co.uk/Magazine/54-8/1006890.aspx</ref>(South African Medical Journal)<ref>http://www.scielo.org.za/scielo.php?pid=S0256-95742011000400010&script=sci_arttext</ref>(Buckingham Palace PRN)<ref>http://www.prnewswire.co.uk/news-releases/lasarow-speaks-at-buckingham-palace-152648505.html</ref><ref>http://www.bdlive.co.za/articles/2010/10/14/building-a-fortune-by-detecting-drugs?service=print</ref>(<ref>http://www.lawgazette.co.uk/analysis/alls-hair-in-love-and-war/51594.fullarticle</ref>(UK Patent Office)<ref>http://www.google.com/patents/WO2008122805A1?cl=en</ref><ref>http://www.manchesterlawsociety.org.uk/files/pdf/messenger-july-11-cmp.pdf</ref><ref>http://www.surreylawsociety.org.uk/documents/document_library/96561170SurreyLawyerWinter09.pdf{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>(UKTI Mayor of London);<ref>{{cite web|url=http://legacy.london.gov.uk/mayor/publications/2009/docs/taking-london-world.pdf |title=Archived copy |accessdate=2015-08-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20120817231520/http://legacy.london.gov.uk/mayor/publications/2009/docs/taking-london-world.pdf |archivedate=17 August 2012 |df=dmy }}</ref> assisting the Libyan Attorney General with the victims of Afriqiyah Airways Airbus A330-200 which crashed shortly before landing; the launch of the DrugAlyser in South Africa (News24, 2010) (IOL 2008) <ref>http://www.iol.co.za/news/south-africa/drugalyser-legality-questioned-1.427427#.Vd-06RNVhHw</ref>)and being South Africa’s youngest Honorary Consul appointed to the UK. (Africa, 2011).

His newest venture, DNAFit uses DNA testing for nutrition and fitness.<ref>{{Cite web|title = DNAFit - Genetic Information for Fitness & Nutrition|url = https://www.dnafit.com|website = DNA-based diet and fitness - DNA Tests - DNAFit|access-date = 2016-02-23|language = en-US}}</ref> The business has received numerous awards and has been awarded Market Gravity Innovation Award at 2015 Lloyds UK National Business Awards<ref name=":0">{{Cite web|title=National Business Awards 2015 - Winners of the Lloyds Bank National Business Awards 2015 |url=http://www.nationalbusinessawards.co.uk/Content/Winners-of-the-National-Business-Awards-2015 |website=www.nationalbusinessawards.co.uk |access-date=2016-02-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20160222152134/http://www.nationalbusinessawards.co.uk:80/Content/Winners-of-the-National-Business-Awards-2015 |archivedate=22 February 2016 |df=dmy }}</ref> and shortlisted for Cutting Edge Sport Award at 2016 BT Sport Industry Awards.<ref>{{Cite web|title = Shortlist|url = https://www.sportindustry.biz/events/awards/2016/shortlist|website = www.sportindustry.biz|access-date = 2016-02-23}}</ref>

==Early life==

Lasarow was born in [[Johannesburg, South Africa]] and received his American grade 12 general equivalency diploma at 16 while simultaneously working at a sandwich shop. He believes that living alone in the US at this young age was the start of his entrepreneurial drive. After briefly returning to South Africa, Lasarow then moved to the United Kingdom where he began his career which ultimately led to his multinational company, Trimega Laboratories.  (Bartlett, 2010)

==Career==

From 1996 until 2001 Lasarow worked at Citigroup (Bartlett, 2010) in the UK as a support manager, designing and implementing a helpdesk in India and implementing technology solutions globally. In 2002 he worked at Citigroup in the capacity of a technology project manager, integrating software on a global basis.  (Bartlett, 2010)

=== DNA Bioscience===
At 26, Lasarow founded and was Managing Director of DNA Bioscience, a UK DNA testing company which he started after witnessing the clinical - and often cold manner in which paternity testing was done in the UK (Bartlett, 2010). The company supplied kits for determining parentage to individuals, solicitors and other corporate bodies (Potts). DNA Bioscience was known for the recruitment of British MP [[David Blunkett]],   education secretary in Tony Blair’s first cabinet in 1997 (Bartlett, 2012), as a non-executive director, who bought £15 000 of shares two weeks before the 2005 UK general election. (Telegraph, 2009) After the election, Blunkett rejoined the government as Work and Pensions Secretary and was questioned why he had not consulted the Advisory Committee on Business Appointments regards his directorship and shareholding of DNA Bioscience. The controversy led to his resignation and as a result, a month later DNA Bioscience failed to secure final listing support and went into liquidation (Telegraph, 2009).

It was in this same year that Lasarow authored and published a book entitled, "Who is Really Who?" – the UK’s first guide to DNA paternity testing. (Lasarow, 2011)

===Trimega===
The collapse of DNA Bioscience led to Lasarow founding, being Chief Executive Officer and Managing Director of Trimega Laboratories in 2005, a leading, forensic science company for testing hair for evidence of alcohol and drug abuse. The firm turned approximately £140 000 in its first year, and approximately £6 million in 2011 which was six years later (Burn-Callander, 2011).

Its products are commercially viable across a wide range of other applications and the company receives requests from doctors who need to test a patient’s suitability for transplant surgery, lawyers involved in employment tribunals, for routine testing on army conscripts, testing for safety-critical jobs such as aviation and for steroid testing among athletes. (Pitman, 2011)  The company initially had facilities in the UK, South Africa, the USA and Germany and was recognised as a ‘World Leading’ UK but South African lead company by the Society of Hair Testing (Newswire, 2011). In 2011, Trimega invested £1 million in the state-of-the-art, 1 900 square-foot laboratory  in Manchester (PRnewswire, 2011). The firm is recognised internationally as a gold standard in the industry and is also a member of the Birmingham Chamber of Commerce, and sponsors the Birmingham Law Society, Leicestershire Law Society and Warwickshire Law Society 2011. (Newswire, 2011)

In 2008, together with Real World Diagnostics, Trimega launched its DrugAlyser product in South Africa, which allows traffic officials to test drivers for drug use via a sample of sweat or saliva (Pitman, 2011). Trimega also works in partnership with the Department of Community Safety and Traffic Law Enforcement to test over 500 motorists at roadblocks for various drugs, including cocaine, opiates, ecstasy, marijuana and methamphetamine (Pitman, 2011).

In this same year, Trimega was asked by the Libyan government to lead the investigation into the DNA testing on the 103 victims who died in the Libyan airplane crash. (IOL, 2010) The 103 passengers were aboard the Afriqiyah Airways Airbus A330-200 flight from Johannesburg to Libya which crashed just before landing at Tripoli airport, Libya. (IOL, 2010) Lasarow had prior to this assisted the Libyan government in more than 50 cases. (IOL, 2010)

In 2011, Trimega won a contract to test for evidence of the illegal drugs in the Brazilian military police (Owen, 2011). Later in the year, Lasarow’s company conducted the first ever study into alcohol patterns in pregnant women in partnership with the Medical Research Council of South Africa, with a view to assist Foetal Alcohol Syndrome.  (HonoraryConsul for South Africa in Birmingham) The firm is partnered with Cape Town-based forensic science company Tripelo, and is diagnosing and analysing Foetal Alcohol Syndrome in newborns. (Newswire, 2011)

===Mole Detect===

In 2012, Lasarow incorporated L-Health a company setup to acquire existing technology from MIT and New Consumer Solutions LLC. The technology was designed for educational purposes only to scan pictures of an individuals mole and report on them using well known ABCDE criteria. Within 1 week post the acquisition of the technology and having all the warranties in place, the product was featured on the Dr. Oz show generating an excess of $50,000 in downloads. As a result, the product became subject to litigation with the Federal Trade Commission (The UK Independent).<ref>http://www.independent.co.uk/life-style/health-and-families/health-news/mole-tester-app-deceptively-claiming-to-analyse-cancer-risk-without-evidence-says-watchdog-10154846.html</ref> It was only in 2015 that Lasarow settled on the basis that the cost of litigation cross-boarder was cost prohibitive (Washington Post 2015).<ref>https://www.washingtonpost.com/news/the-switch/wp/2015/02/23/ftc-fines-marketers-of-two-apps-that-claim-to-detect-melanoma/</ref> The final settlement was completed in 2015 (PCWORLD)<ref>http://www.pcworld.com/article/2971132/government/seller-of-melanoma-detection-apps-settles-ftc-complaint.html</ref>

===DNAFit===

In 2013 Lasarow created the brand DNAFit. The company uses DNA testing for fitness and nutritional purposes.

DNAFit uses a mouth swab to test 45 gene variants scientifically-linked to a body’s capacity to respond to training and nutrition. It then provides detailed reports to help adjust training and nutrition plans to the individual.

DNAFit has now become a recognised global trademark that is associated to the world of genetics and fitness. The company DNAFit Limited since its inception has achieved global media coverage mainly for assisting athletes to tap into their genetic potential and use the power of their genetics to get performance gains naturally. (Daily Mail)<ref>http://www.dailymail.co.uk/sport/worldcup2014/article-2642162/Glen-Johnson-Bryan-Ruiz-clash-World-Cup-DNA-battle.html</ref>(KickOff)<ref>http://www.kickoff.com/news/44998/kagisho-dikgacoi-makes-world-history-in-dna-genetic-testing</ref> BBC- Jenny Meadows<ref>https://www.youtube.com/watch?v=z1Z64_cDn6E</ref>

The company also successfully assisted leading international players prior to the world cup 2015 to include [[Bryan Ruiz]], [[Glen Johnson (English footballer)|Glen Johnson]] and Georgio Karagounis.<ref>{{Cite web|title = Glen Johnson and Bryan Ruiz clash before World Cup with DNA battle|url = http://www.dailymail.co.uk/sport/worldcup2014/article-2642162/Glen-Johnson-Bryan-Ruiz-clash-World-Cup-DNA-battle.html|website = Mail Online|access-date = 2016-02-23}}</ref>

DNAFit is also being used in a lottery-funded pilot project in Essex<ref>{{Cite web|title = Could a DNA test help you get fitter?|url = http://www.telegraph.co.uk/wellbeing/fitness/could-a-dna-test-help-you-get-fitter/|website = The Telegraph|access-date = 2016-02-23}}</ref> to help obese people lose weight.

A growing business, DNAFit has recently won Market Gravity Innovation Award at 2015 Lloyds UK National Business Awards.<ref name=":0" />

==Honours and awards==

Lasarow has received numerous awards for business.

The Shell Significant Business Progress Award 2005 The Prestigious Shell Young Entrepreneur of the year award 2004 Finalist in the Innovation Category - Essex Business Awards 2003 Roll of honour award for the county of Essex presented by Sally Gunnell [Olympic Gold Medallist] 2004 The East & South East Regional Final award for Innovation 2004 Shortlised for the Entrepreneur of the Future with Real Business and the CBI, 2008 The HSBC Sky News Start-up Stars London regional winner of 2008, Startups 2008 finalist, UK National business awards finalist 2008, listed by the Courvoisier The Future 500 list - a definitive list of the country's successful achievers across 10 wide-ranging categories and listed and Business Excel 2008 top 50 rising stars. 2009 - Shortlist for the UK National Entrepreneur Award.

In 2014, Lasarow scooped the UK Active "Spark of Innovation" for the fitness industry.,<ref>http://www.ukactive.com/home/more/8351/page/9/industry-stalwarts-gather-at-ukactive-matrix-flame-awards-2014-to-honour-excellence-in-the-physical-activity-sector</ref> he also won the African Businessman of the year in London at the African Enterprise Awards.<ref>{{cite web|url=http://africanenterpriseawards.co.uk/african_enterprise_awards_winners_2014/ |title=Archived copy |accessdate=2015-08-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20150822021747/http://africanenterpriseawards.co.uk:80/african_enterprise_awards_winners_2014/ |archivedate=22 August 2015 |df=dmy }}</ref> and in 2015 he won Market Gravity Innovation Award at Lloyds UK National Business Awards.<ref name=":0" />

In 2015 was the 1st South Africa inducted into the City of London Entrepreneurs Guild, a City Livary Company for Entrepreneurs.

==Patriotism==

Lasarow is a facilitator and founding member of the UK’s ANC Progressive Business Forum (PBF (Bartlett, 2010)), initiated as a platform to encourage dialogue between friends of South Africa, UK businesses that have an interest in South Africa and members of the PBF that want to understand the initiatives of the South African government. Lasarow attended a series of meetings held in London between ANC Treasurer General and the PBF in October 2011, the outcome being the commitment from the South African and UK government to double their bilateral trade by 2015, as well as financial institutions in London UK wanting to fund development in South Africa. (PBF, 2011) He also attends regular events held between UK Trade Investment and South Africa (Africa, 2011), highlighting the bilateral successes as a global South African.

In 2012, Lasarow became a board member of the South African Chamber of Commerce UK (PRNewsWire).

== References ==
{{Reflist}}

* Lasarow, Avrom (14 September 2011). the southafrican.com. Retrieved 12 September 2012, from the southafrican.com: https://web.archive.org/web/20120401012206/http://www.thesouthafrican.com/business/avi-lasarow.htm
* Bartlett, S. (14 October 2010). www.bdlive.co.za. Retrieved 12 September 2012, from www.bdlive.co.za: http://www.bdlive.co.za/articles/2010/10/14/building-a-fortune-by-detecting-drugs
* Burn-Callander, R. (2 December 2011). www.managementtoday.co.za. Retrieved 12 September 2012, from www.managementtoday.co.za: http://www.managementtoday.co.uk/features/1107499/week-avi-lasarow-trimega-laboratories
* HonoraryConsul for South Africa in Birmingham. (n.d.). Retrieved 12 September 2012, from www.honconsul.za.com: http://www.honconsul.za.com/birmingham/biography/
* Pitman, J. (30 November 2011). www.entrepreneurmag.co.za. Retrieved 12 September 2012, from www.entrepreneurmag.co.za: http://www.entrepreneurmag.co.za/advice/success-stories/snapshots/trimega-laboratories-avi-lasarow/
* Trimega Labs. (n.d.). www.trimega.co.uk. Retrieved 12 September 2012, from www.trimega.co.uk: https://web.archive.org/web/20120920004540/http://www.trimegalabs.co.uk/info/awards.php
* Lasarow, Avi (14 November 2011). The South Africa. Retrieved 18 September 2012, from www.thesouthafrican.com: https://web.archive.org/web/20120403023203/http://www.thesouthafrican.com/news/avi-lasarow-appointed-honorary-consul-of-the-midlands.htm
* BBC. (27 January 2012). BBC News. Retrieved 17 September 2012, from www.bbc.co.uk: http://www.bbc.co.uk/news/uk-england-hereford-worcester-16743351
* IOL. (2 June 2010). IOL. Retrieved 13 September 2012, from www.oil.co.za: http://www.iol.co.za/news/world/sa-man-to-help-libya-crash-victims-1.485886
* News24. (3 August 2010). News24. Retrieved 18 September 2012, from www.news24.cokm: http://m.news24.com/news24/SciTech/News/SA-scientist-up-for-UK-award-20100803
* Owen, V. (9 October 2011). FMWF. Retrieved 13 September 2012, from www.fmwf.com: http://www.fmwf.com/media-type/news/2011/10/trimega-laboratories-wins-contract-to-test-for-illegal-drug-use-in-brazilian-military-police/
* Park, R. (9 September 2011). IVD Technology. Retrieved 18 September 2012, from www.ivdtechnology.com: http://www.ivdtechnology.com/blog/ivdt-insight/trimega-laboratories-develops-test-fetal-alcohol-syndrome{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* PBF. (16 October 2011). Progressive Business Forum. Retrieved 17 September 2012, from www.pbf.org.za: http://www.pbf.org.za/show.php?id=8068
* Potts, Y. (n.d.). BBC News. Retrieved 17 September 2012, from www.bbc.co.uk: http://news.bbc.co.uk/2/hi/programmes/working_lunch/4392992.stm
* Telegraph.co (19 May 2009). Business Truth. Retrieved 13 September 2012, from www.telegraph.co.uk: http://www.telegraph.co.uk/sponsored/business/businesstruth/5351561/Drug-testing-Trimega-Laboratories-find-gold-in-hair.html

{{DEFAULTSORT:Lasarow, Avi}}
[[Category:1976 births]]
[[Category:Living people]]
[[Category:South African businesspeople]]