{{EngvarB|date=January 2017}}
{{Use dmy dates|date=January 2017}} 
{{Infobox person
|name        = Harshavardhan Neotia
|image       = Harshavardhan_Neotia1.jpg
|birth_place = [[Kolkata]], [[West Bengal]], [[India]]
|birth_date  = {{birth date and age|1961|07|19|df=y}}
|nationality = [[India]]n
|residence   = [[Kolkata]], [[West Bengal]], [[India]]
|occupation  = [[Chairman]] of [[Ambuja Neotia Group]]
|alma_mater  = [[St. Xavier's College]]<br />[[Harvard Business School]]
|spouse      = Madhu Neotia
|website     = [http://www.ambujaneotia.com/ ambujaneotia.com]
|footnotes   =
}}

'''Harshavardhan Neotia''' (born on 19 July 1961) is the current chairman of the Ambuja Neotia Group, a corporate house headquartered in [[Kolkata]].<ref name="NDTV Video">{{cite news|last=India|first=NDTV|title=boss's day out|url=http://www.ndtv.com/video/player/boss-s-day-out/harshavardhan-neotia-chairman-ambuja-realty-group/12609|accessdate=24 August 2012|newspaper=NDTVINDIA|date=March 30, 2007}}</ref><ref>http://www.infrawindow.com/people/harshavardhan-neotia_59/</ref> He is also one of the co-owners of [[Atletico de Kolkata]] , which is an [[Indian Super League]] football team along with [[Sourav Ganguly]], [[Atletico Madrid]], [[Sanjeev Goenka]], Utsav Parekh and Santosh Shaw.<ref>http://sports.ndtv.com/football/news/223905-indian-super-league-football-sourav-ganguly-backed-atletico-de-kolkata-launched</ref><ref>http://sports.ndtv.com/football/news/223016-sachin-tendulkar-sourav-ganguly-to-own-teams-in-indian-super-league-football</ref>

==Life==

Harshavardhan Neotia was born and brought up in [[Kolkata]] in a Marwari family.<ref name="bibs">http://www.bibs.co.in/Interview_Archive_HVN.html</ref> He attended [[La Martiniere Calcutta]] for Boys and graduated in Commerce from [[St. Xavier's College, Kolkata]].<ref name="afaqs">http://www.afaqs.com/events/index.html?e=21&p=home</ref> He has completed HBS's Executive Education Programme - Owner President Management Program (OPM) from [[Harvard Business School]].<ref name="Harvard & Projects">{{cite journal|last=Kader|first=Syed Ameen|title=Top 20 entrepreneurs of construction industry|journal=Construction week online|date=4 May 2011|pages=1|url=http://www.constructionweekonline.in/article-7210-top_20_entrepreneurs_of_construction_industry/|accessdate=24 August 2012}}</ref>

He is the founder of one of the first joint sector companies in India, “Bengal Ambuja Housing Development Limited” in partnership with the government of West Bengal.<ref name="Financial Express">{{cite news|last=Agarwal|first=Surabhi|title='My Life has been a series of accidents'|url=http://www.financialexpress.com/news/my-life-has-been-a-series-of-accidents/321643/0|accessdate=24 August 2012|newspaper=THE FINANCIAL EXPRESS|date=Jun 12, 2008}}</ref> The idea behind this joint sector venture was to promote social housing development in urban India.<ref>http://www.financialexpress.com/news/my-life-has-been-a-series-of-accidents/321643/0</ref> Under his guidance, [[Ambuja Realty Group]] has developed many properties across India. Harshavardhana Neotia's maiden project, “Udayan” was declared a ‘Model Housing Project’ by the union government.<ref name=Book>{{cite book|last=Reddi|first=C.V. Narasimha|title=1. Effective Public Relations And Media Strategy|publisher=PHI publisher|isbn=9788120336469|pages=158|url=https://books.google.com/books?id=yhRoWSJlXmIC&pg=PA158&lpg=PA158&dq=harshavardhan+neotia#v=onepage&q=harshavardhan%20neotia&f=false}}</ref> He is also the pioneer of modern [[Public–private partnership]] (PPP) housing system in [[Kolkata]].
He also serves as a member on ‘Board of Governors of [[IIM Calcutta]].<ref name="afaqs" /> His company [[Ambuja Realty Group]] has developed many modern, plush and remarkable real estate projects in West Bengal including Udayan, The Condoville / Swissotel Kolkata, Neotia Vista and Ganga Kutir.<ref name="Harvard & Projects" />

==Achievements==

He was conferred the [[Padmashri]] in the year 1999 for his outstanding initiative in the field of social housing.<ref name=Book />
He has been awarded with [[Young Presidents' Organization]] YPO Legacy of Honor Award and works as the Honorary Consul of [[Israel]] in [[West Bengal]].<ref name="bare_url">{{cite web|author=Harshavardhan Neotia |url=http://investing.businessweek.com/research/stocks/private/person.asp?personId=22353044&privcapId=25727187&previousCapId=3138393&previousTitle=Tata%20Sons%20Limited |title=Harshavardhan Neotia: Executive Profile & Biography - Businessweek |publisher=Investing.businessweek.com |date=2011-07-25 |accessdate=2012-08-12}}</ref>

== See also ==
* [[Tarachand Ghanshyamdas]]

== References ==
{{reflist}}

== External links ==
* http://www.bengalambuja.com/about/index.asp
* http://www.bengalambuja.com/news/images/may-12.pdf
* http://www.ambujarealty.com/news/images/pr-214.html
* http://www.ambujarealty.com/news/images/may-12.pdf
* http://www.ambujarealty.com/news/images/pr-204.html

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{Padma Shri Awards}}

{{DEFAULTSORT:Neotia, Harshvardhan}}
[[Category:University of Calcutta alumni]]
[[Category:1961 births]]
[[Category:People from Kolkata]]
[[Category:Marwari people]]
[[Category:Living people]]
[[Category:Harvard Business School alumni]]
[[Category:Recipients of the Padma Shri in trade and industry]]
[[Category:Atlético de Kolkata owners]]