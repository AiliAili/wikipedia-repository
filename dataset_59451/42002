{{Infobox person
|name        = Mel Boozer
|image       = Boozer1.jpg
|caption     = Melvin "Mel" Boozer
|
|birth_date  = {{birth date|1945|6|21|mf=yes}}
|birth_place = [[Washington, D.C.]]
|death_date  = {{death date and age |1987|3|6 |1945|6|21 |mf=yes}}
|death_place = Washington, D.C.
|death_cause = [[HIV/AIDS|AIDS-related illness]]
|
|nationality = [[United States|American]]
|alma_mater  = {{Plainlist}}
* [[Dartmouth College]] <small>([[undergraduate degree|undergrad]])</small>
* [[Yale University]] <small>([[Doctor of Philosophy|Ph.D.]])</small>
|occupation  = {{Plainlist}}
* Professor of [[sociology]]
* [[LGBT social movements|LGBT rights activist]]
* President, [[Gay and Lesbian Activists Alliance|Gay Activists Alliance of Washington, D.C.]]
|known_for   = The first openly gay candidate for [[Vice President of the United States]]
}}
'''Melvin "Mel" Boozer''' (June 21, 1945 – March 6, 1987)<ref name="Find a Grave">{{cite web |url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=98716349 |title=Dr Melvin "Mel" Boozer |publisher=[[Find a Grave]] |date=October 12, 2012 |accessdate=December 17, 2015 }}</ref> was a university professor and activist for [[African Americans|African American]], [[LGBT]] and [[HIV/AIDS]] issues. He was active in both the [[Democratic Party (United States)|Democratic Party]] and [[Socialist Party USA]].

==Biography==
Boozer grew up in [[Washington, D.C.]] where he graduated as [[salutatorian]] of his class at [[Dunbar High School (Washington, D.C.)|Dunbar High School]]. Boozer attended [[Dartmouth College]] on a scholarship; he entered the university in 1963, one of only three African Americans admitted that year.<ref>Sears, [https://books.google.com/books?id=wqt4krhmQrwC&pg=PA298 p. 298].</ref> Following his graduation he studied for a [[Doctor of Philosophy|Ph.D.]] at [[Yale University]],<ref name="Find a Grave"/> before becoming a professor of [[sociology]] at the [[University of Maryland, College Park|University of Maryland]].<ref name="Clendinen 419">Clendenin, et al., [https://books.google.com/books?id=6zRFBGTSgoUC&pg=PA419 p. 419].</ref>

In 1979, Boozer was elected president of the [[Gay and Lesbian Activists Alliance|Gay Activists Alliance of Washington, D.C.]],<ref name="GLAA 20 years later">{{cite web |url=http://www.glaa.org/archive/2000/boozer0816.shtml |title=20 years later, GLAA remembers Mel Boozer |publisher=[[Gay and Lesbian Activists Alliance]] |accessdate=March 12, 2010 }}</ref> in which office he served for two one-year terms.<ref name="GLAA 35 years">{{cite web |title=1971–2007: Thirty-five years of fighting for equal rights |publisher=Gay and Lesbian Activists Alliance |url=http://www.glaa.org/resources/timeline.shtml |accessdate=January 2, 2009 }}</ref> He was the first African American to serve as GAA president and became "a leading moderate voice among black gays nationally".<ref name="witt">Witt, et al., [https://books.google.com/books?id=1dw5AQAAQBAJ&pg=PT26 p. 18].</ref> While president of the GAA, the organization won unanimous passage of the [[Sexual Assault Reform Act]] by the [[Council of the District of Columbia|D.C. Council]], which decriminalized [[sodomy laws in the United States|sodomy]] and repealed [[solicitation]] laws for consenting adults.<ref name="GLAA 20 years later"/> Under pressure from the [[Moral Majority]], a [[Christian right]] [[advocacy group|lobbying group]], Congress exercized its [[District of Columbia home rule|power to overturn DC acts]] for only the second time to repeal this change.<ref name="GLAA 20 years later"/> During his leadership, the GAA also saw established the right for the GAA to lay a wreath at the [[Tomb of the Unknowns]] in [[Arlington National Cemetery]]<ref name="Find a Grave"/> and won a court battle with the [[Washington Metropolitan Area Transit Authority]] for the right to place [[Metrobus (Washington, D.C.)|Metrobus]] posters reading "Someone in Your Life is Gay."<ref name="GLAA 20 years later"/>

Boozer also wrote for ''[[BlackLight]]'', the first national black gay periodical, founded by [[Sidney Brinkley]].<ref name="Find a Grave"/>

Boozer was nominated in 1980 for the office of [[Vice President of the United States]] by the [[Socialist Party USA]]<ref name="smith193">Smith, et al., [https://books.google.com/books?id=m_boGY8AUTIC&pg=PA193 p. 193]</ref> and, by petition at the convention, by the [[Democratic Party (United States)|Democratic Party]].<ref name="witt"/><ref>Shilts, [https://books.google.com/books?id=nYs8AAAAQBAJ&pg=PT49 p. 32]</ref><ref name="Clendinen 419"/> He was the first openly gay person ever nominated for the office.<ref name="smith193"/> Boozer spoke to the Democratic convention in a speech televised in [[prime time]], calling on the party to support equality for LGBT people:

<blockquote>Would you ask me how I dare to compare the civil rights struggle with the struggle for lesbian and gay rights? I can compare them and I do compare them, because I know what it means to be called a '[[nigger]]' and I know what it means to be called a '[[faggot (slang)|faggot]],' and I understand the differences in the marrow of my bones. And I can sum up that difference in one word: ''none''.<ref>Rutledge, [https://books.google.com/books?id=FpKHAAAAIAAJ&focus=searchwithinvolume&q=boozer p. 156]</ref></blockquote>

Boozer received 49 votes before the balloting was suspended and then-Vice President [[Walter Mondale]] was renominated by [[acclamation]].<ref>Sears, [https://books.google.com/books?id=wqt4krhmQrwC&pg=PA389 p. 389].</ref>

In 1981, Boozer was hired by the [[National LGBTQ Task Force|National Gay Task Force]] as district director<ref name="witt"/> and a [[lobbying|lobbyist]]. NGTF executive director [[Virginia Apuzzo]] fired him in 1983,<ref name="smith42">Smith, [https://books.google.com/books?id=JEGGAAAAIAAJ&focus=searchwithinvolume&q=Boozer p. 42].</ref><ref>Clendinen, et al., [https://books.google.co.uk/books?id=6zRFBGTSgoUC&pg=PA491 p. 491].</ref> replacing him with then-GAA president [[Jeff Levi]].<ref>Clendinen, et al., [https://books.google.co.uk/books?id=6zRFBGTSgoUC&pg=PA477 p. 477].</ref> This had the effect of "leav[ing] the nation's oldest gay organization even whiter"<ref>Clendinen, et al., [https://books.google.co.uk/books?id=6zRFBGTSgoUC&pg=PA495 p. 495].</ref> and drew protests from other gay African Americans.<ref name="smith42"/>

In 1982, he co-founded the [[Langston Hughes]]–[[Eleanor Roosevelt]] Democratic Club to advocate for black LGBT people in D.C., leading the club in 1983 and 1984.<ref name="Find a Grave"/>

Boozer died of an [[HIV/AIDS|AIDS]]-related illness<ref name="GLAA 20 years later"/><ref>Clendinen, et al., pp. [https://books.google.co.uk/books?id=6zRFBGTSgoUC&pg=PA568 568] and [https://books.google.co.uk/books?id=6zRFBGTSgoUC&pg=PA575 575].</ref> in March 1987 at the age of 41 in [[Washington, D.C.]]<ref name="Find a Grave"/><ref name="witt"/><ref>{{cite web|url=http://www.inlamagazine.com/1105/aids25/908_aidsat25_rem_group.html |title=AIDS at 25 |publisher=[[In LA|''In LA'' magazine]] |accessdate=April 30, 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20110722121414/http://www.inlamagazine.com/1105/aids25/908_aidsat25_rem_group.html |archivedate=July 22, 2011 }}</ref> Boozer is featured in a panel of the [[NAMES Project AIDS Memorial Quilt|AIDS Memorial Quilt]].<ref name="GLAA 20 years later"/>

==Notes==
{{reflist|2}}

==References==
{{Portal|LGBT}}
* {{cite book |last1=Cleninden |first1=Dudley |author2=[[Adam Nagourney]] |year=1999 |title=Out for Good: The Struggle to Build a Gay Rights Movement in America |url=https://books.google.com/books?id=6zRFBGTSgoUC |location=New York |publisher=Simon & Schuster |isbn=0-684-81091-3 }}
* {{cite book |last1=Rutledge |first1=Leigh |year=1992 |title=The gay decades: from Stonewall to the present — the people and events that shaped gay lives |url=https://books.google.com/books?id=FpKHAAAAIAAJ |location=New York |publisher=Penguin |isbn=0-452-26810-9 }}
* {{cite book |last1=Sears |first1=Thomas James |year=2001 |title=Rebels, Rubyfruit, and Rhinestones: Queering Space in the Stonewall South |url=https://books.google.com/books?id=wqt4krhmQrwC |publisher=Rutgers University Press |isbn=0-8135-2964-6 }}
* {{cite book |last1=Shilts |first1=Randy |authorlink=Randy Shilts |year=1987 |title=[[And the Band Played On|And the Band Played On: Politics, People, and the AIDS Epidemic]] |publisher=St. Martin's Press |isbn=0-312-00994-1 }}
* {{cite book |last1=Smith |first1=Michael J. |year=1983 |title=Colorful People and Places: A Resource Guide for Third World Lesbians and Gay Men, and for White People who Share their Interests |url=https://books.google.com/books?id=JEGGAAAAIAAJ |publisher=Quarterly Press of BWMT }}
* {{cite book |last1=Smith |first1=Raymond A. |author2=Donald P. Haider-Markel |year=2003 |title=Gay and Lesbian Americans and Political Participation: A Reference Handbook |url=https://books.google.com/books?id=m_boGY8AUTIC |publisher=ABC-CLIO |isbn=1-57607-256-8 }}
* {{cite book |last1=Witt |first1=Lynn |author2=Sherilyn Thomas |author3=[[Eric Marcus]] |year=1995 |title=Out in All Directions: The Almanac of Gay and Lesbian America |url=https://books.google.com/books?id=1dw5AQAAQBAJ |location=New York |publisher=Warner Books |isbn=0-446-67237-8 }}

{{DEFAULTSORT:Boozer, Mel}}
[[Category:1945 births]]
[[Category:1987 deaths]]
[[Category:AIDS-related deaths in Washington, D.C.]]
[[Category:African-American people in Washington, D.C. politics]]
[[Category:African-American United States vice-presidential candidates]]
[[Category:American sociologists]]
[[Category:Dartmouth College alumni]]
[[Category:Yale University alumni]]
[[Category:LGBT African Americans]]
[[Category:Gay politicians]]
[[Category:LGBT rights activists from the United States]]
[[Category:University of Maryland, College Park faculty]]
[[Category:Washington, D.C. Democrats]]
[[Category:Socialist Party USA politicians from Washington, D.C.]]
[[Category:Socialist Party USA vice-presidential nominees]]