{{cleanup rewrite|date=January 2017}}
The "'''Hasbro Universe'''" is the [[Setting (narrative)|setting]] concept that describes a [[Shared universe|shared]] [[fictional universe]] where characters from all of the franchises owned by [[toy]] company [[Hasbro]] take place.<ref>{{Cite web| title = Transformers, G.I. Joe, and other Hasbro characters come together for IDW's 'Revolution'| work = EW.com| accessdate = 2017-01-02| date = 2016-06-01| url = http://ew.com/article/2016/06/01/idw-revolution-hasbro-universe/}}</ref>

== Comics ==
=== Marvel Comics (1979-2004) ===
Since 1979, [[Marvel Comics]] has published its own comic books based on ''[[G.I. Joe]]'', ''[[Micronauts]]'', ''[[Rom (comics)|Rom]]'' and ''[[Transformers]]'', set in [[Earth-616]], known as the [[Marvel Universe]].

Some of the crossover series featuring both Hasbro and Marvel characters were ''The X-Men and the Micronauts'' (guest starring the [[X-Men]]) and ''[[New Avengers/Transformers]]'' (guest starring the [[The New Avengers (comics)|New Avengers]]).

Despite the license rights were brought back to Hasbro, some indirect references to ''Rom'' and ''Micronauts'' were made.{{Citation needed|date=December 2016}}

=== Dreamwave Productions (2004) ===
On 2004, [[Dreamwave Productions]] published ''Transformers/G.I. Joe'', another crossover set in the time of [[World War I]], and including a one-shot titled ''Transformers/G.I. Joe: Divided Front''. This series only lasted six issues, due to Dreamwave's bankruptcy.{{Citation needed|date=December 2016}}

=== IDW Publishing (2005-present) ===
{{Further information|The Transformers (IDW Publishing)|G.I. Joe (IDW Publishing)}}

Between 2005 and 2008, [[comic book]] publisher [[IDW Publishing]] acquired the license to make comics based on ''Transformers'' and ''G.I. Joe'', but neither have ever shared the same [[continuity (fiction)|continuity]] before. On May 2, 2016, IDW announced the crossover event ''[[Revolution (IDW Publishing)|Revolution]]'', which officially set ''[[The Transformers (IDW Publishing)|Transformers]]'' and ''[[G.I. Joe (IDW Publishing)|G.I. Joe]]'' to co-exist within one same universe, with the addition of ''Micronauts'', ''Rom'', ''[[Action Man]]'' and ''[[M.A.S.K.]]''<ref>{{cite web|url=http://io9.gizmodo.com/idw-is-merging-gijoe-transformers-and-its-other-hasbr-1779841772|title=IDW Is Merging G.I. Joe, Transformers, and Its Other Hasbro Comics Into One Giant Universe|publisher=io9|last=Whitbrook|first=James|date=January 6, 2016|accessdate=December 9, 2016}}</ref><ref>{{cite web|url=https://www.idwpublishing.com/idw-catalogue-winter-2016/|title=IDW Catalogue – Winter 2016|publisher=IDW Publishing|date=December 2, 2016|accessdate=December 9, 2016}}</ref>

==== List of IDW Publishing titles ====
{| class="wikitable sortable" width="99%"
! Title || Issue(s) || Start date || End date || Note(s)
|-
! colspan="5" | ''Transformers''
|-
| ''The Transformers: Infiltration''
| style="text-align: center;" | 7
| style="text-align: center;" | October 19, 2005<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-infiltration-1-of-6/|title=Transformers: Infiltration #1 (of 6) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | July 12, 2006<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-infiltration-6-of-6/|title=Transformers: Infiltration #6 (of 6) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Stormbringer''
| style="text-align: center;" | 6
| style="text-align: center;" | July 19, 2006<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-stormbringer-1-of-4/|title=Transformers: Stormbringer #1 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | October 18, 2006<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-stormbringer-4-of-4/|title=Transformers: Stormbringer #4 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''[[The Transformers: Spotlight]]''
| style="text-align: center;" | 6
| style="text-align: center;" | September 13, 2006{{citation needed|date=January 2017}}
| style="text-align: center;" | May 1, 2013{{citation needed|date=January 2017}}
|
|-
| ''The Transformers: Escalation''
| style="text-align: center;" | 6
| style="text-align: center;" | November 29, 2006<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-escalation-1/|title=Transformers: Escalation #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | May 2, 2007<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-escalation-6/|title=Transformers: Escalation #6 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Megatron Origin''
| style="text-align: center;" | 4
| style="text-align: center;" | June 20, 2007<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-megatron-origin-1-of-4/|title=Transformers: Megatron Origin #1 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | October 24, 2007<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-megatron-origin-4-of-4/|title=Transformers: Megatron Origin #4 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Devastation''
| style="text-align: center;" | 6
| style="text-align: center;" | October 3, 2007<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-devastation-1/|title=Transformers: Devastation #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | February 27, 2008<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-devastation-6/|title=Transformers: Devastation #6 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''[[The Transformers: All Hail Megatron]]''
| style="text-align: center;" | 16
| style="text-align: center;" | July 10, 2008<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-all-hail-megatron-1/|title=Transformers: All Hail Megatron #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | October 14, 2009<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-all-hail-megatron-16/|title=Transformers: All Hail Megatron #16 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Maximum Dinobots''
| style="text-align: center;" | 5
| style="text-align: center;" | December 10, 2008<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-maximum-dinobots-1/|title=Transformers: Maximum Dinobots #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | April 15, 2009<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-maximum-dinobots-5/|title=Transformers: Maximum Dinobots #5 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Continuum''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | November 11, 2009{{Citation needed|date=January 2017}}
| One-shot
|-
| ''The Transformers''
| style="text-align: center;" | 31
| style="text-align: center;" | November 18, 2009{{citation needed|date=January 2017}}
| style="text-align: center;" | December 7, 2011{{citation needed|date=January 2017}}
|
|-
| ''The Transformers: Bumblebee''
| style="text-align: center;" | 4
| style="text-align: center;" | December 16, 2009<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-bumblebee-1/|title=Transformers: Bumblebee #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | March 10, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-bumblebee-4/|title=Transformers: Bumblebee #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Last Stand of the Wreckers''
| style="text-align: center;" | 5
| style="text-align: center;" | January 27, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-last-stand-of-the-wreckers-1/|title=Transformers: Last Stand of the Wreckers #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | May 19, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-last-stand-of-the-wreckers-5/|title=Transformers: Last Stand of the Wreckers #5 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Ironhide''
| style="text-align: center;" | 4
| style="text-align: center;" | May 12, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-ironhide-1/|title=Transformers: Ironhide #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | August 4, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-ironhide-4/|title=Transformers: Ironhide #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Drift''
| style="text-align: center;" | 4
| style="text-align: center;" | September 8/9, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-drift-1-of-4/|title=Transformers: Drift #1 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | October 20, 2010<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-drift-4-of-4/|title=Transformers: Drift #4 (of 4) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Heart of Darkness''
| style="text-align: center;" | 4
| style="text-align: center;" | March 23, 2011<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-heart-of-darkness-1/|title=Transformers: Heart of Darkness #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | June 29, 2011<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-heart-of-darkness-4/|title=Transformers: Heart of Darkness #4|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
|
|-
| ''[[The Transformers: The Death of Optimus Prime]]''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | December 21, 2011<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-death-of-optimus-prime/|title=Transformers: Death of Optimus Prime - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| One-shot
|-
| ''[[The Transformers: More than Meets the Eye]]''
| style="text-align: center;" | 57
| style="text-align: center;" | January 11, 2012<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-meets-eye-1/|title=Transformers: More Than Meets the Eye #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | September 28, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-meets-eye-57/|title=Transformers: More Than Meets the Eye #57 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Retitled as ''Transformers: More than Meets the Eye'' in issue #39
|-
| ''[[The Transformers: Autocracy]]''
| style="text-align: center;" | 12
| style="text-align: center;" | January 18, 2012<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-autocracy-1/|title=Transformers: Autocracy #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | June 20, 2012<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-autocracy-12/|title=Transformers: Autocracy #12 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Robots in Disguise''
| style="text-align: center;" | 57
| style="text-align: center;" | January 25, 2012{{citation needed|date=January 2017}}
| style="text-align: center;" | September 28, 2016{{citation needed|date=January 2017}}
| Retitled as ''The Transformers'' in issue #35; retitled as ''Transformers'' in issue #39 
|-
| ''[[The Transformers: Monstrosity]]''
| style="text-align: center;" | 12
| style="text-align: center;" | March 1, 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-monstrosity-1/|title=Transformers: Monstrosity #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | July 31, 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-monstrosity-12/|title=Transformers: Monstrosity #12 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Sequel to ''The Transformers: Autocracy''
|-
| ''The Transformers: Dark Cybertron''
| style="text-align: center;" | 12
| style="text-align: center;" | November 6, 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-dark-cybertron-1/|title=Transformers: Dark Cybertron #1 Part 1(of 12) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | March 26, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-dark-cybertron-finale/|title=Transformers: Dark Cybertron Finale – Part 12 (of 12) - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''[[The Transformers: Windblade]]'' Volume 1
| style="text-align: center;" | 4
| style="text-align: center;" | April 16, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-windblade-1-of-4/|title=Transformers: Windblade #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | July 23, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-windblade-4-of-4/|title=Transformers: Windblade #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''The Transformers: Punishment''
| style="text-align: center;" | 5
| style="text-align: center;" colspan="2" | June 2014 (printed on January 21, 2015)<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-punishment/|title=Transformers: Punishment|publisher=IDW Publishing|accessdate=December 10, 2016}}</ref>
| Digital comic
|-
| ''[[The Transformers: Primacy]]''
| style="text-align: center;" | 4
| style="text-align: center;" | August 13, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-primacy-1/|title=Transformers: Primacy #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | November 19, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-primacy-4/|title=Transformers: Primacy #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Sequel to ''The Transformers: Monstrocity''
|-
| ''The Transformers: Drift - Empire of Stone''
| style="text-align: center;" | 4
| style="text-align: center;" | November 26, 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-drift-empire-of-stone-1/|title=Transformers: Drift: Empire of Stone #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | February 25, 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-drift-empire-of-stone-4/|title=Transformers: Drift—Empire of Stone #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Sequel to ''The Transformers: Drift''
|-
| ''[[The Transformers: Windblade]]'' Volume 2
| style="text-align: center;" | 7
| style="text-align: center;" | March 25, 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-windblade-1-3-combiner-wars-part-one/|title=Transformers: Windblade—Combiner Wars #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | September 23, 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-windblade-7/|title=Transformers: Windblade #7 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Sequel to ''The Transformers: Windblade'' Volume 1; issues #1-5 tied to ''Combiner Wars''
|-
| ''Transformers: Combiner Hunters''
| style="text-align: center;" | 1
| colspan="2" style="text-align: center;" | July 29, 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-combiner-hunters-special-one-shot/|title=Transformers: Combiner Hunters Special one-shot|publisher=IDW Publishing|accessdate=December 16, 2016}}</ref>
| One-shot; tied to ''Combiner Wars''
|-
| ''Transformers: Redemption''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | October 28, 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-redemption/|title=Transformers: Redemption - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| One-shot; sequel to ''The Transformers: Punishment''
|-
| ''Transformers: Till All are One''
| style="text-align: center;" | 5
| style="text-align: center;" | June 15, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-till-one-1/|title=Transformers: Till All Are One #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | Ongoing
| Sequel to ''The Transformers: Windblade'' Volume 2
|-
| ''Transformers: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | October 26, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-revolution-1/|title=Transformers: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
| ''Transformers: Till All are One: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | November 2, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-till-one-revolution-1/|title=Transformers: Till All Are One: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
| ''Transformers: More than Meets the Eye: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | December 7, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-meets-eye-revolution-1/|title=Transformers: More Than Meets the Eye: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
| ''Optimus Prime''
| style="text-align: center;" | -
| style="text-align: center;" | December 14, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/optimus-prime/|title=Optimus Prime #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 10, 2016}}</ref>
| style="text-align: center;" | -
| Sequel to ''The Transformers: Robots in Disguise''
|-
| ''Transformers: Lost Light''
| style="text-align: center;" | -
| style="text-align: center;" | December 14, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/transformers-lost-light-1/|title=Transformers: Lost Light #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 10, 2016}}</ref>
| style="text-align: center;" | -
| Sequel to ''The Transformers: More than Meets the Eye''
|-
! colspan="5" | ''G.I. Joe''
|-
| ''G.I. Joe'' Volume 1
| style="text-align: center;" | 28
| style="text-align: center;" | October 2008{{citation needed|date=January 2017}}
| style="text-align: center;" | February 2011{{citation needed|date=January 2017}}
|
|-
| ''G.I. Joe: Origins''
| style="text-align: center;" | 23
| style="text-align: center;" | February 2009{{citation needed|date=January 2017}}
| style="text-align: center;" | January 2011{{citation needed|date=January 2017}}
|
|-
| ''[[G.I. Joe: Cobra]]'' Volume 1
| style="text-align: center;" | 21
| style="text-align: center;" | March 2009<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-cobra-1/|title=G.I. Joe: Cobra #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | June 2009{{citation needed|date=January 2017}}
|
|-
| ''G.I. Joe: Hearts & Minds''
| style="text-align: center;" | 5
| style="text-align: center;" | May 2010{{citation needed|date=January 2017}}
| style="text-align: center;" | September 2010{{citation needed|date=January 2017}}
|
|-
| ''G.I. Joe'' Volume 2
| style="text-align: center;" | 21
| style="text-align: center;" | May 2011{{citation needed|date=January 2017}}
| style="text-align: center;" | January 2013{{citation needed|date=January 2017}}
| Part of the storyline ''Cobra Civil War''
|-
| ''[[G.I. Joe: Cobra]]'' Volume 2
| style="text-align: center;" | 21
| style="text-align: center;" | May 2011<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-cobra-1-cobra-ii-forked-tongue/|title=G.I. Joe: Cobra #1  Cobra II: Forked Tongue|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | December 2012{{citation needed|date=January 2017}}
| Part of the storyline ''Cobra Civil War''
|-
| ''G.I. Joe: Snake Eyes''
| style="text-align: center;" | 21
| style="text-align: center;" | May 18, 2011<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-snake-eyes-ongoing-1/|title=G.I. Joe: Snake Eyes Ongoing #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | February 6, 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-snake-eyes-ongoing-21-of-21/|title=G.I. Joe: Snake Eyes Ongoing #21 (of 21)|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Separated inside three stoylines
|-
| ''G.I. Joe'' Volume 3
| style="text-align: center;" | 15
| style="text-align: center;" | February 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-2013-2014-1/|title=G.I. Joe (2013-2014) #1|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | April 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-2013-2014-15/|title=G.I. Joe (2013-2014) #15|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''G.I. Joe: Special Missions''
| style="text-align: center;" | 13
| style="text-align: center;" | March 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-special-missions-1/|title=G.I. Joe: Special Missions #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | April 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-special-missions-14/|title=G.I. Joe: Special Missions #14 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''G.I. Joe: The Cobra Files''
| style="text-align: center;" | 9
| style="text-align: center;" | April 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-the-cobra-files-1/|title=G.I. Joe: The Cobra Files #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | December 2013<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-the-cobra-files-9/|title=G.I. Joe: The Cobra Files #9 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''G.I. Joe'' Volume 4
| style="text-align: center;" | 8
| style="text-align: center;" | September 2014<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-1-2/|title=G.I. JOE #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | April 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-8-2/|title=G.I. JOE #8 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| Retitled as ''The Fall of G.I. Joe''
|-
| ''G.I. Joe: Snake Eyes, Agent of Cobra''
| style="text-align: center;" | 5
| style="text-align: center;" | January 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-snake-eyes-agent-of-cobra-1/|title=G.I. JOE: Snake Eyes, Agent of Cobra #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | May 2015<ref>{{cite web|url=https://www.idwpublishing.com/product/g-i-joe-snake-eyes-agent-of-cobra-2/|title=G.I. JOE: Snake Eyes, Agent of Cobra #2 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''G.I. Joe: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | November 16, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/g-joe-revolution-1/|title=G.I. JOE: Revolution #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
| ''G.I. Joe'' Volume 5
| style="text-align: center;" | -
| style="text-align: center;" | December 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/g-joe-1/|title=G.I. JOE #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | -
|
|-
! colspan="5" | ''Micronauts''
|-
| ''Micronauts''
| style="text-align: center;" | 7
| style="text-align: center;" | May 6, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/micronauts-1/|title=Micronauts #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | Ongoing
|
|-
| ''Micronauts: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | September 28, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/micronauts-revolution-1/|title=Micronauts: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
! colspan="5" | ''Rom''
|-
| ''Rom''
| style="text-align: center;" | 5
| style="text-align: center;" | May 8, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/rom-1/|title=Rom #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | Ongoing
|
|-
| ''Rom: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | September 21, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/rom-revolution-1/|title=Rom: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
! colspan="5" | ''Action Man''
|-
| ''Action Man''
| style="text-align: center;" | 4
| style="text-align: center;" | June 22, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/action-man-1/|title=Action Man #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | September 21, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/action-man-4/|title=Action Man #4 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
|
|-
| ''Action Man: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | October 26, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/action-man-revolution-1/|title=Action Man: Revolution #1|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
! colspan="5" | ''M.A.S.K.: Mobile Armored Strike Kommand''
|-
| ''M.A.S.K.: Mobile Armored Strike Kommand: Revolution''
| style="text-align: center;" | 1
| style="text-align: center;" colspan="2" | September 29, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/m-s-k-revolution-1/|title=M.A.S.K.: Revolution #1|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| One-shot; part of the crossover event ''Revolution''
|-
| ''M.A.S.K.: Mobile Armored Strike Kommand''
| style="text-align: center;" | 1
| style="text-align: center;" | November 30, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/m-s-k-mobile-armored-strike-kommand-1/|title=M.A.S.K.: Mobile Armored Strike Kommand #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 10, 2016}}</ref>
| style="text-align: center;" | Ongoing
|
|-
! colspan="5" | Other
|-
| ''[[Revolution (IDW Publishing)|Revolution]]''
| style="text-align: center;" | 6
| style="text-align: center;" | July 1, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/revolution-prelude/|title=Revolution: #00 Prelude - IDW Publishing|publisher=IDW Publishing|accessdate=December 9, 2016}}</ref>
| style="text-align: center;" | November 30, 2016<ref>{{cite web|url=https://www.idwpublishing.com/product/revolution-5/|title=Revolution #5 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| First crossover event in the Hasbro Universe
|-
| ''Revolutionaries''
| style="text-align: center;" | -
| style="text-align: center;" | January 2017<ref>{{cite web|url=https://www.idwpublishing.com/product/revolutionaries-1/|title=Revolutionaries #1 - IDW Publishing|publisher=IDW Publishing|accessdate=December 13, 2016}}</ref>
| style="text-align: center;" | -
| [[Team-up]] between various Hasbro characters
|}

== Television ==

=== Marvel Productions and Sunbow Productions (1983-1987) ===
From 1983 to 1987, [[Marvel Productions]] and [[Sunbow Productions]] developed the series ''[[G.I. Joe: A Real American Hero (1985 TV series)|G.I. Joe: A Real American Hero]]'', ''[[The Transformers (TV series)|The Transformers]]'', ''[[Jem (TV series)|Jem]]'' and ''[[Inhumanoids]]''. These series had been hinted to share the same universe, mostly due to the appearance of the character named [[Hector Ramirez (Sunbow)|Hector Ramirez]].{{Citation needed|reason=Ni official link to be|date=December 2016}}

== Film ==

=== Paramount Pictures (2009-present) ===
{{Further information|Transformers (film series)|G.I. Joe (film series)}}

On March 28, 2014, producer [[Lorenzo di Bonaventura]] announced that he is open to doing a crossover film between [[Paramount Pictures|Paramount]]'s ''[[Transformers (film series)|Transformers]]'' and ''[[G.I. Joe (film series)|G.I. Joe]]'' film series.<ref>{{cite web|url=http://m.cinemablend.com/new/G-I-Joe-Transformers-Crossover-Producer-Tells-Us-How-It-Could-Happen-36650-p5.html|title=G.I. Joe And Transformers Crossover? The Producer Tells Us How It Could Happen|publisher=m.cinemablend.com|date=October 1, 2014|accessdate=December 10, 2016}}</ref> On June 23, di Bonaventura stated that a crossover was not likely to happen,<ref>{{cite web|url=http://www.slashfilm.com/gi-joe-transformers-movie-crossovers-not-likely/|title=G.I. Joe/Transformers Movie Crossover Not Likely to Happen, Says Producer|publisher=slashfilm.com|date=June 23, 2014|accessdate=December 10, 2016}}</ref> but on July 9, he reassured there would still be a possibility.<ref>{{cite web|url=http://www.fashiontimes.com/articles/9569/20140709/transformers-and-g-i-joe-crossover-movie-possible.htm|title=‘Transformers’ and ‘G.I. Joe’ Crossover Movie Is Possible, Says Producer|publisher=fashiontimes.com|date=July 9, 2014|accessdate=December 10, 2016}}</ref> On July 26, director [[Jon M. Chu]] stated that he is interested in directing that kind of film.<ref>{{cite web|url=http://www.tfw2005.com/transformers-news/transformers-movie-just-movie-31/jon-m-chu-likes-the-idea-of-a-g-i-joetransformers-crossover-movie-178100/|title=Jon M. Chu Likes the idea of a G.I. Joe/Transformers Crossover Movie|publisher=www.tfw2005.com|accessdate=December 10, 2016}}</ref>

On October 23, 2015, Chu confirmed his intentions to make a crossover film between ''Transformers'' and ''G.I. Joe'' with [[Universal Studios|Universal]]'s ''[[Jem and the Holograms (film)|Jem and the Holograms]]''.<ref>{{cite web|url=http://www.avclub.com/article/john-chu-wants-make-crossover-film-gi-joe-transfor-227381|title=John Chu wants to make a crossover film with G.I. Joe, Transformers, and Jem|first=B.G.|last=Henne|date=October 23, 2015|accessdate=December 10, 2016}}</ref> On October 29, he hinted about ''Transformers'' possibly doing crossover with other Hasbro products.<ref>{{cite web|url=http://www.movienewsguide.com/transformers-crossover-hasbro-properties-possible-g-joe-jem-hologram-included-hinted-director-jon-chu/112184|title=‘TRANSFORMERS’: CROSSOVER WITH OTHER HASBRO PROPERTIES POSSIBLE; ‘G.I. JOE,’ ‘JEM THE HOLOGRAM’ INCLUDED; HINTED BY DIRECTOR JON CHU|publisher=movienewsguide.com|date=October 29, 2015|accessdate=December 10, 2016}}</ref> On December 15, [[Hasbro Studios]] agreed with Paramount to a deal creating a five-property movie universe by financing unit Allspark Pictures and distributed by Paramount Pictures. The following properties are ''G.I. Joe'', ''[[M.A.S.K.]]'', ''[[Micronauts]]'', ''[[Rom (comics)|Rom]]'' and ''[[Visionaries: Knights of the Magical Light|Visionaries]]''.<ref>{{cite news|last1=Kilday|first1=Gregg|title=Paramount, Hasbro Creating Movie Universe Around G.I. Joe, Four Other Brands|url=http://www.hollywoodreporter.com/news/paramount-hasbro-gi-joe-universe-848920|work=The Hollywood Reporter|date=December 15, 2015|accessdate=December 10, 2016}}</ref>

On April 21, 2016, [[The Hollywood Reporter]] confirmed that Lindsey Beer, [[Michael Chabon]], Cheo Hodari Coker, Joe Robert Cole, [[John Francis Daley]], [[Jonathan Goldstein (screenwriter)|Jonathan Goldstein]], [[Jeff Pinkner]], [[Nicole Perlman]], Nicole Riegel, Geneva Robertson and [[Brian K. Vaughan]] have joined the writers' room for the "Hasbro Cinematic Universe".<ref>{{cite web|url=http://www.hollywoodreporter.com/heat-vision/hasbro-cinematic-universe-takes-shape-886316|title=Hasbro Cinematic Universe Takes Shape With Michael Chabon, Brian K. Vaughan, Akiva Goldsman (Exclusive)|publisher=The Hollywood Reporter |last=Kit|first=Borys|date=April 21, 2016|accessdate=December 10, 2016}}</ref>

On January 18, 2017, [[D. J. Caruso]] has stated to Collider that the script for the crossover film between ''Transformers'' and ''G.I. Joe'' is now being written.<ref>{{cite web|url=http://collider.com/gi-joe-3-transformers-dj-caruso/|title=Director D.J. Caruso Says His ‘G.I. Joe 3’ Idea Involved Meeting the Transformers|publisher=Collider|last=Chitwood|first=Adam|date=January 18, 2017}}</ref>

== See also ==
* [[List of comics based on Hasbro properties]]
* [[List of films based on Hasbro properties]]
* [[List of television programs based on Hasbro properties]]

== References ==
{{Reflist|3}}

{{Hasbro}}

[[Category:Continuity (fiction)]]
[[Category:Hasbro]]
[[Category:Fictional universes]]