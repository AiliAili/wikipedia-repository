{{multiple issues|
{{COI|date=June 2015}}
{{disputed|date=June 2015}}
{{primary sources|date=June 2015}}
}}
{{Infobox mine
| name = Kayenta mine
| image =
| width =
| pushpin_map = Arizona
| coordinates = {{coord|36|31|36|N|110|25|52|W|region:US|format=dms|display=inline,title}}
| display = inline,title
| place = [[Navajo Indian Reservation]], [[Navajo County, Arizona|Navajo County]]
| subdivision_type = [[U.S. state|State]]
| state/province = [[Arizona]]
| country = United States
| owner = [[Peabody Energy]]
| official website = http://www.peabodyenergy.com
| acquisition year = 
| stock_exchange = 
| stock_code = 
| products = 
| financial year =
| amount =
| opening year = 1973
| closing year =
}}

The '''Kayenta mine''' is a [[Coal mining#Area mining|surface coal mine]] operated by Peabody Western Coal Company (a subsidiary of [[Peabody Energy]]) on the [[Navajo Nation|Navajo Indian Reservation]] in northern Arizona.<ref>
{{cite web|title= EIA Annual Coal Report – Major US Coal Mines, 2010
|year=2010
|publisher= U.S. Energy Information Administration
|url=http://www.eia.gov/coal/annual/pdf/table9.pdf 
|accessdate=2011-05-23
}}</ref>
About 400 acres are mined and reclaimed each year, providing about 8 million tons of coal annually to the [[Navajo Generating Station]].<ref>
{{cite web|title=Geographic Information Systems at Peabody Western Coal Company’s Black Mesa Complex, Geospatial Conference 2004.pdf
|year=2004
|author= Eric Bronston
|publisher=Office of Surface Mining Reclamation and Enforcement 
|url=http://www.tips.osmre.gov/newsroom/conferences/2004GeoSpatial/post-conference/Papers/Bronston2_AZ.pdf
|accessdate=2011-09-06
}}</ref> {{rp|p.1}}

== History ==
The Kayenta mine is located near the northern edge of [[Black Mesa (Apache-Navajo Counties, Arizona)|Black Mesa]], a 5,400 square mile highland plateau in northeast Arizona that contains extensive coal deposits in several [[geologic formation]]s.
<ref name="USGS water data">
{{cite web|title= Groundwater, Surface-Water, and Water-Chemistry Data, Black Mesa Area, Northeastern Arizona 2008 -2009, Open File Report 2010-1038
|year=2010
|author= Jamie P. Macy
|publisher= U.S. Geological Survey
|url=http://pubs.usgs.gov/of/2010/1038/of2010-1038.pdf
|accessdate=2011-11-16
}}</ref>
<ref name="Kayenta fact sheet">
{{cite web|title=Peabody Kayenta Mine Fact Sheet
|publisher=Peabody Energy
|url=http://www.peabodyenergy.com/content/276/Publications/Fact-Sheets/Kayenta-Mine 
|accessdate=2011-09-06
}}</ref>

Since about 1300 AD, the [[Hopi]] had extracted coal from the area for pottery firing and domestic heating.
<ref>
{{cite web|title= Status of Mineral Resource Information, Hopi Indian Reservation, Coconino and Navajo Counties, Arizona
|date=January 1987
|author1=Robert M. Thompson |author2=Jean A. Dupree |publisher= U.S. Bureau of Indian Affairs
|url= http://www.bia.gov/idc/groups/xieed/documents/text/idc010764.pdf
|accessdate=2012-03-21
}}</ref>
<ref name="summary of coal resources">
{{cite web|title= Summary of Coal Resources of the Black Mesa Coal Field, Arizona (Paper in the Guidebook of the Black Mesa Basin, Northeastern Arizona, New Mexico Geological Society Ninth Field Conference)
|year=1958
|author=R. B. O’Sullivan
|publisher= New Mexico Geological Society
|url= http://nmgs.nmt.edu/publications/guidebooks/downloads/9/9_p0169_p0171.pdf
|accessdate=2012-05-08
}}</ref>{{rp|p.171}}
In the early 1900s, limited surveying of the coal field was made and a number of small underground mines were put into production, supplying local reservation schools and communities in the region.
<ref>
{{cite web|title=A Summary of the Coal Deposits in the Colorado Plateau: Arizona, Colorado, New Mexico, and Utah. USGS professional paper 1625-B 
|author1=Mark A. Kirschbaum |author2=Laura R.H. Biewick |date=
|publisher= U.S. Geological Survey
|url=http://pubs.usgs.gov/pp/p1625b/Reports/Chapters/Chapter_B.pdf 
|accessdate=2012-03-16
}}</ref> 
<ref name="EIA coal profiles">
{{cite web|title= EIA State Coal Profiles
|date=January 1994
|publisher= U.S. Energy Information Administration
|url=ftp://ftp.eia.doe.gov/coal/0576.pdf
|accessdate=2012-04-19
}}</ref>

The coal field received increased attention beginning in 1950 when the Navajo - Hopi Long Range Rehabilitation Act was enacted in response to dire economic conditions on the reservations. The Act promoted economic development and better utilization of the reservations’ natural resources.
<ref>
{{cite web|title=Public Laws of the Eighty-First Congress, Second Session, 1950-1951, Chapter 92, Public Law 474 (64 Stat. 44)
|date=1950-04-19
|publisher= Oklahoma State University
|url=http://digital.library.okstate.edu/kappler/Vol6/html_files/v6p0498b.html#mn18
|accessdate=2012-04-02
}}</ref>

As a result, the [[strata]] and coal deposits of Black Mesa were more extensively studied; and in 1961 and 1964 Sentry Royalty Company, a subsidiary of Peabody Coal Company, was issued drilling and exploration permits on the Mesa to determine areas where coal could be economically recovered.
<ref>
{{cite web|title= Coal, oil, natural gas, helium, and uranium in Arizona: The Arizona Bureau of Mines Bulletin 180
|year=1970
|author= Peirce, H.W., Keith, S.B., and Wilt, J.C.
|publisher= Arizona Geological Survey
|url=http://repository.azgs.az.gov/sites/default/files/dlio/files/nid1270/b-182_coal_natural_gas_helium_uranium.pdf
|accessdate=2012-04-02
}}</ref>{{rp|p.14–34}}

In 1964 and 1966, Sentry entered into mining lease agreements with the tribes for 64,848 acres.
The first lease was made in 1964 for 24,858 acres of land held exclusively by the Navajo Tribe, designated Navajo Coal Lease No. 14-20-0603-8580, or N-8580 for short. The additional 40,000 acres were leased in 1966 in the Navajo-Hopi Joint Use Area, leases being entered into with each of the two tribes – Joint Mineral Lease No. 14-20-0450-5743, aka J-5743, with the Hopi tribe, and 14-20-0603-9910, aka J-9910, with the Navajo Tribe. When the Joint Use Area was later partitioned between the tribes in 1974, much of the mine lease area and surface rights ended up with Navajo Nation, but the two tribes retained joint and equal interest in the coal and other minerals under the partitioned lands. 
<ref name="mine renewal EA sec D">
{{cite web|title=Kayenta Mine Permit Renewal – Environmental Assessment, Section D. Affected Environment
|date=August 2011
|publisher=U.S. Office of Surface Mining Reclamation and Enforcement 
|url= http://www.wrcc.osmre.gov/Current_Initiatives/Kayenta_Mine/Renewal/EA-SecD.pdf
|accessdate=2012-01-16
}}</ref>{{rp|p.82–83}}
<ref>
{{cite web|title= Navajo and Hopi Settlement Act, Public Law 93-531
|year=1974
|publisher= U.S. Office of Navajo and Hopi Indian Relocation
|url= http://www.onhir.gov/N-H_Settlement_Act_-_titled.pdf
|accessdate=2012-04-03
}}</ref>{{rp|p.7}}

Peabody developed two separate mines within the leasehold area – the Black Mesa mine and Kayenta mine. The Black Mesa mine, located on 20,775 acres in the southwestern section of the leasehold area, began operation in 1970 supplying the [[Mohave Generating Station]] in Laughlin, Nevada. The Kayenta mine, on 44,073 acres in the northern and eastern sections, began operation in 1973 supplying the Navajo Generating Station near Page, Arizona.
<ref name="mine renewal EA sec A">
{{cite web|title=Kayenta Mine Permit Renewal – Environmental Assessment, Section A. Proposed Action
|date=August 2011
|publisher=U.S. Office of Surface Mining Reclamation and Enforcement 
|url= http://www.wrcc.osmre.gov/Current_Initiatives/Kayenta_Mine/Renewal/EA-SecA.pdf
|accessdate=2012-01-16
}}</ref>{{rp|p.2,5}}
<ref name="NNEPA Statement of Basis">
{{cite web|title=Part 71 Federal Operating Permit Statement of Basis, Peabody Western Coal Company Black Mesa Complex, Permit No. NN-OP 08-010
|date=2009-12-07
|publisher=Navajo Nation Environmental Protection Agency 
|url= http://www.navajonationepa.org/opp/PDF/2009-Dec-07-PWCC_sob.pdf
|accessdate=2011-05-23
}}</ref>{{rp|p.2}}

[[File:Kayenta mine permit area and PWCC lease areas map.png|thumb|400px|Mine complex leasehold area]]
Mine locations were set relative to conveying transfer points, natural divisions between recoverable sections of coal (called coal resource areas), and projected quantities to be supplied to the respective power plants, such that each mine had portions on both the Navajo Reservation and in the Joint Use Area. The leased areas, mines, and facilities were collectively termed the Black Mesa Complex.

In 1982, the U.S. [[Office of Surface Mining Reclamation and Enforcement]] (OSM) issued permit AZ-0001 for surface mining and reclamation operations on the Black Mesa and Kayenta mines. The permit was most recently renewed in 2012 for the Kayenta Mine as permit No. AZ-0001E.
<ref name="mine renewal EA signed">
{{cite web|title= OSMRE Indian Lands Program, Permit for surface coal mining operations, Permit number AZ-0001E
|date=2010-07-06
|publisher=U.S. Office of Surface Mining Reclamation and Enforcement 
|url=http://www.wrcc.osmre.gov/Current_Initiatives/Kayenta_Mine/Renewal/Signed_Permit_Fnl.pdf 
|accessdate=2012-03-28
}}</ref>

In 2005, the Black Mesa Mine ceased production when the Mohave Generating Station was shut down, leaving the Kayenta Mine as the only active mine in the leasehold area.

== Coal characteristics ==
The coal that is mined comes from the Wepo formation, the uppermost coal-bearing formation on the Mesa, and is removed from multiple seams that are typically 3 to 15 feet thick and lie within about 130 feet of the surface.
<ref name="Kayenta fact sheet"/>
 
It is classified as low-sulfur, high volatile C [[bituminous coal|bituminous]], having a heat content of 10,820 Btu/ lb, ash content of 10.4%, and sulfur content of 0.63%. 
<ref name="summary of coal resources"/>{{rp|p.171}}
Average mercury content is 0.04 [[Parts-per notation#ppm|ppm]], or about 4&nbsp;lb/ trillion Btu, a concentration that is about 1/10th that of average [[shale]] rock and among the lowest concentrations in US coals.
<ref>
{{cite web|title=Chemical Analyses of Coal Samples from the Black Mesa Field, Arizona
|year=1977
|publisher= Arizona Bureau of Mines, Geological Survey branch
|url= http://repository.azgs.az.gov/sites/default/files/dlio/files/nid1067/c18.pdf
|accessdate=2012-12-13
}}</ref>{{rp|p.12}}
<ref>
{{cite web|title=US Coal Quality Database
|date=
|publisher= U.S. Geological Survey
|url=http://energy.er.usgs.gov/coalqual.htm 
|accessdate=2012-10-02
}}</ref> 
<ref>
{{cite web|title=Mercury in U.S. Coals map (Mercury in pounds per trillion Btu for selected US coal areas)
|date=
|publisher= U.S. Geological Survey
|url= http://pubs.usgs.gov/of/1998/of98-772/hglay.pdf
|accessdate=2013-12-13
}}</ref>

== Operation ==
Coal is removed by the open cut or strip mining method. Topsoil is first removed and placed on previously mined and re-contoured areas or stored separately for later reclamation. [[Overburden]] is then blasted to loosen the materials and is removed primarily by electric [[Dragline excavator|dragline]] to expose the coal seams. [[Excavator]]s, [[Loader (equipment)|loaders]] and [[haul truck]]s then remove the exposed coal to a collection point, where it is crushed and loaded on a 17 mile long overland belt conveyor.<ref name="EIA coal profiles"/>

The conveyor carries the coal to rail load-out silos, where the coal is loaded into [[Black Mesa and Lake Powell Railroad|BM&LP]] rail cars bound for the Navajo Generating Station.

Areas disturbed by mine activities cover less than half of the leasehold area, and traditional land use and occupancy are allowed to continue within the leased area where safely removed from active mining operations.
<ref name="mine renewal EA sec A"/>{{rp|p.4}}
As of 2010, approximately 83 households were located within the leasehold area, four of them in an area planned for future mining operations. For those households that must be moved, Peabody provides for relocation to an agreed-upon area within or adjacent to the leasehold area, as well as compensation for acreage removed from customary grazing as a result of mining activities.
<ref name="mine renewal EA sec D"/>{{rp|p.67}}

==Environmental controls and impacts==

===Archaeology===
From 1967 to 1986, one of the largest and longest-running archaeological projects in North America - the Black Mesa Archaeological Project - was conducted with the assistance of Peabody Coal Company to record, preserve or relocate archaeological artifacts within the leasehold area. 
Since 1986, Peabody has continued archaeological testing and mitigation ahead of mine activities. Mining operations have also been adjusted to avoid a number of locations identified as sacred or ceremonial sites.
<ref name="mine renewal EA sec D"/>{{rp|p.17–18}}
<ref>{{cite book|
last1=Powell
|first1=Shirley
|authorlink =
|last2 =Gumerman
|first2 =George J.
|title = People of the Mesa: Archaeology of Black Mesa
|publisher = Southern Illinois University Press
|date =1987-12-01
|isbn =978-0809314003}}</ref>

===Air quality===
Measures to protect air quality include water sprays and sprinklers to control dust on dirt roads and disturbed areas, enclosures and water sprays at belt conveyor transfer points, baghouses on coal storage silos, and other requirements of the mine’s Clean Air Act operating permit NN-OP 08-010. The permit is administered, monitored and enforced by the Navajo Nation Environmental Protection Agency (NNEPA), as authorized by the US EPA.
<ref>
{{cite web|title=Clean Air Act Title V Final Operating Permit #NN-OP 80-010 for PWCC Kayenta Complex
|date=2009-12-07
|publisher=Navajo Nation Environmental Protection Agency 
|url=http://navajonationepa.org/opp/PDF/PWCC%20Permit_041411.pdf
|accessdate=2012-11-05
}}</ref> 
Peabody maintains a network of PM10 particulate monitors to assess the effectiveness of fugitive dust control measures and ensure compliance with national ambient air quality standards ([[NAAQS]]). Monitoring data are reported quarterly to the OSM and NNEPA. 
<ref name="mine renewal EA sec D"/>{{rp|p.51}}

===Water resources===
Two main [[Arroyo (creek)|washes]] – Moenkopi Wash and Dinnebito Wash - pass through the mine permit area and eventually run into the [[Little Colorado River]]. Although mining does not occur in these washes, storm water runoff from areas disturbed by mining activities could potentially degrade the quality of water going into them. To prevent degradation of surface waters, [[sedimentation pond]]s are constructed under the mine’s surface water protection program. Water quality is monitored and controlled at these ponds, as well as at livestock watering ponds, seeps and storm water runoff. The program is regulated through the US EPA’s National Pollutant Discharge Elimination System under NPDES permit No. NN0022179, and Multi-Sector General Permit No. AZR05000I.
<ref name= "Kayenta CHIA">
{{cite web|title= Cumulative Hydrologic Impact Assessment (CHIA) for Peabody Western Coal Company - Kayenta Complex
|date=December 2011
|publisher= U.S. Office of Surface Mining Reclamation and Enforcement 
|url= http://www.wrcc.osmre.gov/Current_Initiatives/CHIA/Kayenta/KayentaCHIA2011.pdf 
|accessdate=2012-09-11
}}</ref>{{rp|p.11}}
<ref>
{{cite web|title= Peabody Western Coal Company - Black Mesa Complex NPDES (National Pollutant Discharge Elimination System Program) Final Permit Fact Sheet
|year=2010
|publisher= U.S. Environmental Protection Agency
|url= http://www.epa.gov/region9/water/npdes/pdf/navajo/PeabodyWesternCoal-FactSheetFnl-2010.pdf 
|accessdate=2011-09-12
}}</ref>

The mine pumps about 1240 [[acre-foot|acre-feet]] of ground water per year from the Navajo aquifer (aka N-aquifer). The water is used primarily for dust control, with about 60 acre-feet made available to area residents. Up until December 2005, the Black Mesa mine had used ground water to slurry coal through a 273 mile long pipeline to the Mohave Generating Station. Until the station was shut down, the combined water usage by both mines had been 4,450 acre-feet per year.<ref name="mine renewal EA sec D"/>{{rp|p.30, 36}}

Peabody monitors and reports groundwater and surface water conditions within the leasehold area. The USGS additionally monitors wells and springs outside the leasehold area on and around the Mesa in support of the OSM and other regulatory agencies.  
The OSM provides assessment and oversight to insure that mining operations will not result in material damage to the aquifers and surface waters.

== Reclamation of mined lands ==
Once a section has been mined out, it is backfilled and graded to the approximate previous contour and topped with suitable spoil, subsoil and topsoil to create a four-foot thick root zone. The land is then seeded and restored for grazing, wildlife habitat, and cultural plant cultivation.
<ref name="mine renewal EA sec D"/>{{rp|p.50}}
<ref name="mine renewal EA app A">
{{cite web|title= Kayenta Mine Permit Renewal - Appendix A. Mining and Reclamation Procedures
|date=August 2011
|publisher=U.S. Office of Surface Mining Reclamation and Enforcement 
|url= http://www.wrcc.osmre.gov/Current_Initiatives/Kayenta_Mine/Renewal/EA-AppA.pdf  
|accessdate=2012-03-19
}}</ref>{{rp|p.A-17}} 
Replanted areas are monitored for a minimum of 10 years to correct any erosion problems, control invasive weeds, and insure the plant systems are fully established before being released for grazing. Reclaimed areas are typically 20 times more productive than the native range.
<ref name="Kayenta fact sheet"/>

As of 2012, more than 15,000 acres had been restored on the Black Mesa Complex and 39 permanent ponds had been created for livestock watering and wildlife habitat.
<ref name="mine renewal EA sec D"/>{{rp|p.26, 40}}

Reclamation efforts were recognized by the U.S. Office of Surface Mining for reclamation excellence 1998, for cultural, historic & archaeological preservation in 2002, good neighbor award in 2003, and reclamation excellence and good neighbor awards in 2005.
<ref>
{{cite web|title=Excellence in Surface Coal Mining Reclamation Award Winners (1986 – 2005 archives)
|year=2005
|publisher= U.S. Office of Surface Mining Reclamation and Enforcement
|url= http://www.osmre.gov/topic/awards/Archive/86-05%20Archive%20Awards_winners.shtm
|accessdate=
}}</ref> 
<ref>
{{cite web|title= OSMRE 2005 Excellence in Surface Mining Awards
|date=2010-07-31
|publisher= Public.Resource.Org
|url= https://www.youtube.com/watch?v=4cDV5hxWZaQ
|accessdate=2011-09-08
}}</ref>

== References ==
{{div col|2}}
{{Reflist}}
{{div col end}}

== External links ==
* [https://www.youtube.com/watch?v=Hd07iVrDbYM/ Peabody Celebrates 40 Years on Black Mesa] – Photos of archaeological work, mining operations and equipment, air monitoring station, reclamation work, and reclaimed areas on Black Mesa.

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Coal mines in the United States]]
[[Category:Buildings and structures in Navajo County, Arizona]]
[[Category:Mines in Arizona]]