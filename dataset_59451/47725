{{Infobox television episode
| title        = Enemies Foreign
| series       = [[NCIS (TV series)|NCIS]]
| image        = 
| caption      = Ziva and Eli's confrontation in "Enemies Foreign"
| season       = 8
| episode      = 8
| director     = [[Dennis Smith (director)|Dennis Smith]]
| writer       = Jesse Stern
| airdate      = {{Start date|2010|11|16}}
| guests       = * [[Michael Nouri]] as Mossad Director Eli David
* [[TJ Ramini]] as Mossad Officer Malachi Ben Gidon
* [[Sarai Givaty]] as Mossad Officer Liat Tuvia
* [[Arnold Vosloo]] as Mossad Officer Amit Hadar
* [[Catherine Dent]] as Former NCIS Special Agent Whitney Sharp
* [[Michael O'Neill (actor)|Michael O'Neill]] as Former NCIS Special Agent Riley McCallister
* Oren Dayan as Karif Yasin
* Stan Ivar as Former NCIS Special Agent Ben Robinson
* [[Leslie-Anne Huff]] as Hillary Lange
* [[Alex Morris]] as Former NCIS Special Agent Kurt Nelson
* [[Maura Soden]] as Former NCIS Special Agent Melora Koss
| prev         = [[NCIS (season 8)#ep169|Broken Arrow]]
| next         = [[NCIS (season 8)#ep171|Enemies Domestic]]
| episode_list = [[NCIS (season 8)|''NCIS'' (season 8)]]<br />[[List of NCIS episodes|List of ''NCIS'' episodes]]
}}

'''"Enemies Foreign"''' is the eighth episode of the eighth season of the American police procedural drama ''[[NCIS (TV series)|NCIS]]'' and the 170th episode overall. It originally aired on CBS in the United States on November 16, 2010. The episode is the first of a two-part story arc and continues the long-term storyline within the series of the complex relationship between protagonist Ziva ([[Cote de Pablo]]) and her father, Mossad Director Eli David (Michael Nouri). During the first installment, the NCIS team is assigned to protect Eli when he arrives in Washington D.C. for an inter-agency conference and must deal with three Palestinian terrorists who are attempting to assassinate him.

A number of recurring actors appeared in the episode to portray the Mossad officers, including TJ Ramini as Malachi Ben Gidon and Arnold Vosloo as Amit Hadar. Israeli actress Sarai Givaty was cast for a new role as Liat Tuvia, the officer who replaced Ziva on the [[Kidon]] unit.

The episode is written by Jesse Stern and directed by Dennis Smith. It was watched live by 19.43 million viewers.<ref name="us8x08">{{cite web|url=http://tvbythenumbers.zap2it.com/2010/11/17/tuesday-finals-glee-sings-louder-adjusted-up-detroit-187-adjusted-down/72516|title=Tuesday Finals: ''Glee'' Sings Louder, Adjusted Up; ''Detroit 187'' Adjusted Down|publisher=[[Tribune Media Services]]|work=[[TV by the Numbers]]|date=November 17, 2010|accessdate=July 9, 2013|last=Seidman|first=Robert}}</ref>

== Background ==

''NCIS'' follows a team of government agents who work for the Naval Criminal Investigative Service.<ref>{{cite web|url=http://www.washingtonmonthly.com/magazine/mayjune_2011/features/ncis_bureaucrats_with_guns029134.php?page=2|title=NCIS: Bureaucrats with Guns|work=[[The Washington Monthly]]|date=May–June 2011|accessdate=July 7, 2013|last=Rosenberg|first=Alyssa}}</ref> The main cast includes [[Mark Harmon]] as team leader [[Leroy Jethro Gibbs]], [[Michael Weatherly]] as Senior Agent [[Anthony DiNozzo|Anthony "Tony" DiNozzo]], [[Cote de Pablo]] as Mossad Liaison Officer turned NCIS Agent [[Ziva David]], [[Pauley Perrette]] as Forensic Specialist [[Abby Sciuto]], [[Sean Murray (actor)|Sean Murray]] as Special Agent [[Timothy McGee]], [[David McCallum]] as Autopsy Technician Donald "Ducky" Mallard, [[Rocky Carroll]] as NCIS Director Leon Vance, and [[Brian Dietzen]] as Medical Assistant Jimmy Palmer.

== Plot ==

In the opening scene, the team arrests a young woman on suspicion of espionage and terrorism. Shortly thereafter, they learn that she had only used an electronic device to shoplift from the actual terrorist. The investigation leads to Ziva's former team leader, Mossad Officers Malachi Ben Gidon ([[TJ Ramini]]), and replacement Liat Tuvia ([[Sarai Givaty]]). They inform her that her father, Director Eli David ([[Michael Nouri]]), is set to arrive in Washington D.C. to attend an NCIS conference. Ziva is at first incredulous, as Eli has not left Israeli soil in over a decade, but he appears the following day.

On learning that three Palestinian terrorists are planning an attempt on Eli's life, the NCIS team is designated to protect him during his stay in the United States. The assignment is complicated by Ziva and Eli's estrangement, though she asserts that she can operate as normal, a claim that Gibbs backs. Liat still questions this, and Ziva agrees to a practice exercise to determine if she is fit to defend her father.

Ziva later confronts Eli about his nonchalant attitude towards her ordeal in the desert and the prospect of his own death. He responds that the responsibility of his work renders him unable to allow his personal emotions to dictate his choices. He further reminds her that there had been a time when "things were different". The episode ends on a cliffhanger when, after an attack by the terrorists at the conference apparently fails, Vance and David go to a safe house. Gibbs cannot reach them on the radio and Officer Hadar is shown to be lying dead at the safe house.

== Production ==

=== Writing ===

In August, executive producer Shane Brennan was asked if the show would delve more deeply into the storyline surrounding Ziva's captivity and torture in Somalia that took place in the previous season. He did not directly address this but divulged, "In November sweeps, we dip again into a story that perhaps everyone thought was played out. It involves Ziva."<ref>{{cite web|url=http://insidetv.ew.com/2010/08/04/ask-ausiello-spoilers-greys-house-bones/|title=Ask Ausiello: Spoilers on ''Grey's'', ''House'', ''Bones'', ''NCIS'', ''The Office'', and more!|publisher=[[Time Inc.]]|work=[[Entertainment Weekly]]|date=August 4, 2010|accessdate=May 30, 2013|last=Ausiello|first=Michael}}</ref><ref>{{cite web|url=http://www.tvfanatic.com/2010/08/ncis-scoop-zivas-past-gibbs-father-allison-harts-return/|title=''NCIS'' Scoop: Ziva's Past, Gibbs' Dad, Allison Hart's Return|publisher=Mediavine|work=TV Fanatic|date=August 5, 2010|accessdate=May 30, 2013|last=Marsi|first=Steve}}</ref> A month later, Brennan stated that this would be brought about through the return of Ziva's father, Eli David, who had sent her on the suicide mission a year prior. Described as "an emotion-charged reunion",<ref>{{cite web|url=http://origin.tvfanatic.com/2010/09/ncis-spoilers-the-return-of-eli-david/|title=''NCIS'' Spoilers: The Return of Eli David|publisher=Mediavine|work=TV Fanatic|date=September 23, 2010|accessdate=June 4, 2013|last=Marsi|first=Steve}}</ref> the two-part episode arc would reportedly "[reveal] much about the enigmatic Eli David, and one of the other team members".<ref>{{cite web|url=http://www.digitalspy.com/tv/s164/ncis/news/a278355/ncis-ziva-to-reunite-with-father.html|title=''NCIS'' Ziva 'to reunite with father'|publisher=[[Hearst Magazines UK]]|work=[[Digital Spy]]|date=September 23, 2010|accessdate=May 30, 2013|last=Jeffery|first=Morgan}}</ref>
In an interview before the episode aired, Cote de Pablo (portraying Ziva), said, "It's a two-part episode where the whole Israeli team comes back from Israel, including Ziva's father. So obviously that's going to be a big thing for Ziva because she finally gets to confront her father for sending her away on, basically, what I would call a suicide mission. They get to see each other after all of that and there's conflict."<ref name="latina">{{cite web|url=http://www.latina.com/entertainment/tv/exclusive-cote-de-pablos-major-ncis-spoiler-whole-kitchen-blows-and-im-it|title=Exclusive: Cote de Pablo's Major ''NCIS'' Spoiler: 'The Whole Kitchen Blows Up and I'm in It!'|work=[[Latina (magazine)|Latina]]|date=November 5, 2010|accessdate=May 18, 2013|last=Hernandez|first=Lee}}</ref> She added that the episode "affects [Ziva's] relationship with her father" and that "People will have to watch to see exactly how, but maybe there will be an unspoken forgiveness between father and daughter."<ref>{{cite web|url=http://www.latina.com/entertainment/tv/exclusive-cote-de-pablo-tonights-ncis-one-main-characters-gets-critically-injured#axzz2YgixfPsP|title=Exclusive: Cote de Pablo on Tonight's ''NCIS'': 'One of the Main Characters Gets Critically Injured'|work=[[Latina (magazine)|Latina]]|date=November 16, 2010|accessdate=July 10, 2013|last=Hernandez|first=Lee}}</ref>

=== Casting ===

Israeli actress [[Sarai Givaty]] was cast to depict Mossad Officer Liat Tuvia in Enemies Foreign and Enemies Domestic.<ref name="Ynet">{{cite web|url=http://www.ynetnews.com/articles/0,7340,L-3986875,00.html|title=Sarai Givaty to star in ''NCIS''|work=[[Ynetnews]]|date=November 21, 2010|accessdate=May 20, 2013|last=Shechnik|first=Raz}}</ref> Givaty's casting was reported in the Israeli news, and ADD agent Yaron Lichtenstein commented, "We are proud of Sarai for joining the agency's series of international success stories."<ref name="Ynet"/> Brennan elaborated on the character's role in the story arc, saying that Liat's presence would leave Ziva feeling replaced "personally and professionally".<ref>{{cite web|url=http://www.tvfanatic.com/2010/10/sarai-givaty-to-guest-star-on-ncis/|title=Sarai Givaty to Guest Star on ''NCIS''|publisher=Mediavine|work=TV Fanatic|date=October 17, 2010|accessdate=May 30, 2013|last=Marsi|first=Steve}}</ref> Givaty commented, "There's a bit of tension there, and it gets to high levels. But there's a good ending!"<ref>{{cite web|url=http://insidetv.ew.com/2010/11/12/smallville-sons-of-anarchy-vampire-diaries-tv-spoilers/|title=''Vampire Diaries'', ''Smallville'', ''Sons of Anarchy'', more: Find out what's coming next in the Spoiler Room|publisher=[[Time Inc.]]|work=[[Entertainment Weekly]]|date=November 12, 2010|accessdate=May 30, 2013|last=Gonzalez|first=Sandra}}</ref>
Jesse Stern remarked on the recurring appearance of Mossad agents within the series:

{{quote|I love our Israelis. I love when they're right and I love when they're wrong and I love trying to figure out why they do things so differently from our Americans. And maybe that's an intercontinental curiosity since our five Israelis are played by people from five different countries, each of them bringing their own unique take, their own attitude to the set everyday. I'm grateful to all of them for taking the ball and running with it since I was barely on set this episode.<ref>{{cite web|title=Insider's Blog: 'Enemies Foreign'|work=CBS|date=November 17, 2010|last=Stern|first=Jesse}}</ref>}}

Michael Nouri returned to portray Ziva's father, Eli.<ref>{{cite web|url=http://abc.soapsindepth.com/2010/11/nouri-stops-by-ncis.html|title=Nouri Stops By NCIS!|work=[[ABC Soaps in Depth]]|date=November 1, 2010|accessdate=July 10, 2013}}</ref> Arnold Vosloo and TJ Ramini also appeared again to depict Mossad Officers Amit Hadar and Malachi Ben Gidon respectively.

=== Filming ===

Cote de Pablo divulged, "Stuff ''is'' going to be blowing up, and not only that, but it was a crazy episode to shoot because there were ''sooo'' many stunts for me. And so that was a little hard. I had a big fight sequence, I had to shoot a lot—there was a lot of gunfire—and there was one huge explosion in which a whole kitchen blows up and I'm in the middle of it! Needless to say it was fun, but incredibly challenging."<ref name=latina/>

== Reception ==

Mandi Bierly from ''[[Entertainment Weekly]]'' wrote, "For viewers, however, those scenes of the joint NCIS-Mossad protection detail running through an ambush scenario in the hotel parking garage, Ziva confronting her father about why he wasn't begging her forgiveness for leaving her for dead in the desert, and the actual ambush itself, were the highlight of the episode."<ref>{{cite web|url=http://popwatch.ew.com/2010/11/17/ncis-paper-shredding-scoop-mystery-solved/|title=''NCIS'' scoop: Vance paper-shredding mystery solved next week!|publisher=[[Time Inc.]]|work=[[Entertainment Weekly]]|date=November 17, 2010|accessdate=June 4, 2013|last=Bierly|first=Mandi}}</ref>
[[BuddyTV]] contributor Jacky Jackman enjoyed the Liat and Ziva's interactions and wrote, "The dynamics between the two women is interesting. Ziva extols her achievements with Mossad, but Liat has chops of her own."<ref>{{cite web|url=http://www.buddytv.com/articles/ncis/ncis-fan-columnist-daddy-deare-38671.aspx|title=''NCIS'' Fan Columnist: Daddy Dearest|work=[[BuddyTV]]|date=November 17, 2010|accessdate=July 10, 2013|last=Jackman|first=Jacky}}</ref>

== References ==

{{reflist|30em}}

{{NCIS television}}

 

[[Category:2010 television episodes]]
[[Category:NCIS (TV series) episodes]]