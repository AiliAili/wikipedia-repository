{{Use mdy dates|date=November 2013}}
{{Good article}}
{{Infobox NFL season
| team = Muncie Flyers
| year = 1920
| record = 0–1 (APFA)<br />3–1 (Overall)
| league_place = 14th in APFA
| coach = [[Ken Huffine]]
| stadium = [[Traveling team]]
| playoffs = <!--No playoffs until 1932 -->
| previous = [[1919 Muncie Congerville Flyers season|1919]]
}}

The '''[[1920 APFA season|1920]] [[Muncie Flyers]] season''' was the franchise's inaugural season in the [[American Professional Football League]] (APFA)—later named the National Football League. The Flyers entered the season coming off a 4–1–1 record in [[1919 Muncie Congerville Flyers season|1919]]. Several representatives from the Ohio League wanted to form a new professional league; thus, the APFA was created. The 1920 team only played in one game that counted in the standings: a 45–0 loss against the [[Rock Island Independents]]. This game and the [[Columbus Panhandles]]–[[Dayton Triangles]] on the same date is considered to be the first league game featuring two APFA teams. The Flyers tried to schedule other games, but the opponents canceled to play better teams. As a result, the Flyers had to play the rest of the season's game versus local teams. In week 10, the Flyers won a game against the [[Muncie Offers More AC]] for the Muncie City Championship. No players from the 1920 Muncie Flyers were listed on the 1920 [[All-Pro]] Team, and no player has been enshrined in the [[Pro Football Hall of Fame]].

== Offseason ==
The Muncie Flyers, playing as the Muncie Congerville Flyers, finished 4–1–1 as an independent team in [[1919 Muncie Congerville Flyers season|1919]].<ref>{{Cite web | url=http://www.profootballarchives.com/1919munc.html | title=1919 Muncie Congerville Flyers | work=The Pro Football Archives | publisher=Maher Sports Media | archiveurl=http://www.webcitation.org/66Mrf3ZBj | archivedate=March 22, 2012}}</ref> They concluded this season with a win over [[Avondale AA]] and won the Muncie City Championship.<ref name="sye1">{{Harvnb|Sye|2002|p=1}}</ref> Representatives of four [[Ohio League]] teams—the [[Canton Bulldogs]], the [[Cleveland Tigers (NFL)|Cleveland Tigers]], the [[Dayton Triangles]], and the [[Akron Pros]]—called a meeting on August 20, 1920, to discuss the formation of a new, professional league. At the meeting, they tentatively agreed on a [[salary cap]] and pledged not to sign college players or players already under contract with other teams. They agreed on a name for the circuit: the American Professional Football Conference.<ref>{{Harvnb|PFRA Research|1980|pp=3–4}}</ref><ref>{{Harvnb|Siwoff|Zimmber|Marini|2010|pp=352–353}}</ref>

[[Earl Ball]], the Flyers' manager, heard about this gathering on August 29 and was interested in participating.<ref name="sye1" /> The original four representatives then invited other professional teams to a second meeting on September 17. At that meeting, held at Bulldogs owner [[Ralph Hay]]'s [[Hupmobile]] showroom in Canton, representatives of the [[Rock Island Independents]], the Flyers, the [[Decatur Staleys]], the [[Racine Cardinals]], the [[Massillon Tigers]], the [[Chicago Cardinals (NFL, 1920–59)|Chicago Cardinals]], and the [[Hammond Pros]] agreed to join the league. Representatives of the [[Buffalo All-Americans]] and [[Rochester Jeffersons]] could not attend the meeting but sent letters to Hay asking to be included in the league.<ref name="nflbday4">{{Harvnb|PFRA Research|1980|p=4}}</ref> Team representatives changed the league's name slightly to the American Professional Football Association and elected officers, installing [[Jim Thorpe]] as president.<ref name="nflbday4" /><ref>{{Cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9503E5DC173AE532A2575AC1A96F9C946195D6CF | title=Thorpe Made President | newspaper=The New York Times| date=September 19, 1920 | format=PDF}}</ref><ref>{{Cite news | url=https://news.google.com/newspapers?id=f8MWAAAAIBAJ&sjid=OiEEAAAAIBAJ&pg=3568,5105560&dq=gridders&hl=en | title=Organize Pro Gridders; Choose Thorpe, Prexy | newspaper=[[The Milwaukee Journal]] | date=September 19, 1920 | page=24}}</ref> Under the new league structure, teams created their schedules dynamically as the season progressed,<ref name="Peterson">{{Harvnb|Peterson|1997|p=74}}</ref><ref name="Davis">{{Harvnb|Davis|2005|p=59}}</ref> and representatives of each team voted to determine the winner of the APFA trophy.<ref>{{Cite news | last=Price | first=Mark | date=April 25, 2011 | url=http://www.ohio.com/news/searching-for-lost-trophy-1.204246 | title=Searching for Lost Trophy | newspaper=[[Akron Beacon-Journal]] | accessdate=June 23, 2012}}</ref>

== Regular season ==
The Flyers hosted a practice game against the Muncie Tigers on September 26, 1920, but the result of the game is unknown.<ref name="sye2" /> The Flyers' first game of the season was against the Rock Island Independents. The Independents beat the Flyers 45–0. As a result, the Staleys, who were supposed to play the Flyers the next week, cancelled because they wanted to play a better team.<ref name="sye1" /> The Flyers tried to schedule game for the next few weeks but were unsuccessful. Since there were no rules to keep players on teams, several Flyers' players left and played for other teams.<ref name="sye2" /> The Flyers scheduled a game against the Cleveland Tigers three weeks later, but the game was cancelled because the Tigers decided to play against the Panhandles instead.<ref name="sye2" /> The same result happened next week against the Dayton Triangles.<ref name="archives">{{Cite web | url=http://www.profootballarchives.com/1920apfamun.html | title=1920 Muncie Flyers | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=August 31, 2012}}</ref> The Flyers were challenged by the [[Gas City Tigers]] and [[Muncie Offers More AC]]—two teams of Muncie. These games are not counted in the APFA standings.

=== Schedule ===
The table below was compiled using information from The Pro Football Archives and ''[[The Coffin Corner]]'', both of which used various contemporary newspapers.<ref name="sye2">{{Harvnb|Sye|2002|p=2}}</ref><ref name="archives" /> A dagger ({{Dagger}}) represents a non-APFA team. For the results column, the winning team's score is posted first followed by the result for the Flyers. For the attendance, if a cell is greyed out and has "N/A", then that means there is an unknown figure for that game. The green-colored cells indicates a win; and the red-colored cells indicate a loss. The games against the local teams are listed, but are not counted in the final APFA standings.

{| class="wikitable" style="text-align:center"
|-
! Week !! Date !! Opponent !! Result !! Venue !! Attendance !! Record
|-
! scope="row" | 1
| colspan="6" | ''No game scheduled''
|-style="background: #ffdddd;"
! scope="row" | 2
| October 3, 1920
| at [[Rock Island Independents]]
| 45–0 L
| [[Douglas Park (Rock Island)|Douglas Park]]
| 3,110
| 0–1
|-
! scope="row" | 3
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 4
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 5
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 6
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 7
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 8
| colspan="6" | ''No game scheduled''
|-
! scope="row" | 9
| colspan="6" | ''No game scheduled''
|-style="background:#dfd;"
! scope="row" rowspan="2" | 10
| November 25, 1920
| at [[Gas City Tigers]]{{Dagger|alt=Non-APFA team}}
| 19–7 W
| [[Gas City, Indiana]]
| {{NA}}
| 1–1
|-style="background:#dfd;"
| November 28, 1920
| at [[Muncie Offers More AC]]{{Dagger|alt=Non-APFA team}}
| 24–0 W
| [[Walnut Park (Indiana)|Walnut Park]]
| {{NA}}
| 2–1
|-style="background:#dfd;"
! scope="row" | 11
| December 5, 1920
| at Gas City Tigers{{Dagger|alt=Non-APFA team}}
| 13–7 W
| Gas City, Indiana
| {{NA}}
| 3–1
|-
! scope="row" | 12
| colspan="6" | ''No game scheduled''
|-
|-
! scope="row" | 13
| colspan="6" | ''No game scheduled''
|}

== Game summaries ==

=== Week 2: at Rock Island Independents ===
{{Linescore Amfootball
|Road=Flyers
|R1=0|R2=0|R3=0|R4=0
|Home='''Independents'''
|H1=21|H2=10|H3=7|H4=7
}}
''October 3, 1920, at Douglas Park''

In their only APFA game counted in the standings, the Muncie Flyers played against the Rock Island Independents. It is considered to be one of the first games played with two APFA teams.<ref>{{Harvnb|Braunwart|Carroll|1981|p=1}}</ref> Since kickoff times were not standardized, it is unknown if the Muncie–Rock Island or [[Columbus Panhandles|Columbus]]–[[Dayton Triangles|Dayton]] game is the first game. In the first quarter, the Independents scored three touchdowns: two from [[Arnold Wyman]] and one from [[Rube Ursella]]. In the second quarter, Ursella kicked a 25-yard field goal, and Wyman scored from an 86-yard kickoff return. In the third quarter, [[Sid Nichols]] had a 5-yard rushing touchdown, and [[Waddy Kuehl]] scored a 7-yard rushing touchdown, en route to a final score of the game was 45–0.<!--This is a paragraph citation used to cite all the scores--><ref>{{Cite news | last=Copeland | first=Bruce | date=October 4, 1920 | title=Wyman's Play too much for Hoosier Team | newspaper=[[Rock Island Argus]]}}</ref>

=== Week 10: at Gas City Tigers ===
{{Linescore Amfootball
|Road='''Flyers'''
|R1=13|R2=6|R3=0|R4=0
|Home=Tigers
|H1=0|H2=0|H3=7|H4=0
}}
''November 25, 1920, in Gas City, Indiana''

It took the Flyers eight weeks in order to have a game played; they accepted the Gas City Tigers' challenge to play in Gas City, Indiana, on November 18.<ref name="sye2" /> The Tigers were 9–0 and outscored their opponents 443–9 this season.<ref>{{Cite web | url=http://www.profootballarchives.com/1920gas.html | title=1920 Gas City Tigers | work=The Pro Football Archives | publisher=Maher Sports Media | archiveurl=http://www.webcitation.org/66MrkAOZg | archivedate=March 22, 2012}}</ref> Halfback [[Mickey Hole]] scored a 45-yard rushing touchdown three minutes into the game. On the next possession, the Flyers scored again; [[Kenneth Huffine]] scored the touchdown, and [[Cooney Checkaye]] kicked the point after, which made the score 13–0. Near the beginning of the second quarter, Checkaye scored, but the extra point was missed. The Tigers scored their only touchdown in the game in the third quarter. The final score of the game was 19–7.<ref name="sye2" />

=== Week 10: at Muncie Offers More AC ===
{{Linescore Amfootball
|Road='''Flyers'''
|R1=0|R2=0|R3=14|R4=10
|Home=Offers More AC
|H1=0|H2=0|H3=0|H4=0
}}
''November 28, 1920, at Walnut Park''

After their first victory of the season, the Flyers were challenged by the Muncie Offers More AC. The game was labelled as the Muncie City Championship. Since most of the other local teams' seasons were finished, both Muncie teams hired as many as 20 players for the game. The field was muddy, which caused Offers More AC to fumble the ball several times. In the third quarter, the Flyers scored two touchdowns to give them a 14–0 lead going into the fourth quarter. The Flyers added 10 more points—a touchdown and a field goal—in the final quarter to win the Championship 24–0.<ref name="sye2" />

=== Week 11: at Gas City Tigers ===
{{Linescore Amfootball
|Road='''Flyers'''
|R1=0|R2=0|R3=7|R4=6
|Home=Tigers
|H1=0|H2=7|H3=0|H4=0
}}
''December 5, 1920, in Gas City, Indiana''

The Flyers last game of the 1920 season was against the Gas City Tigers. The Tigers signed up several players to help them defeat the Flyers. The first quarter was scoreless, as the only near score was from a failed [[drop kick]] from the Flyers. The first score of the game came in the second quarter. Weaver of the Tigers returned a punt 85&nbsp;yards for a touchdown. On the Tigers' next possession, they dropped a pass in the endzone on fourth down. Early in the third quarter, a member of the Tigers fumbled, and the Flyers recovered it on the 5-yard line. Huffine score a rushing touchdown on that possession to tie the game 7–7. The last score of the game came from the Flyers; Checkaye returned a punt 60&nbsp;yards for a touchdown to give the Flyers a 13–7 victory. The Tigers almost scored on their final possession on the game but fumbled.<ref name="sye2" />

== Post-season ==
Without any APFA wins, the Flyers could not contend for the APFA Championship. However, with wins against the Gas City Tigers and the Muncie Offers More AC, the Flyers claimed to have won the Indiana State Championship.<ref name="sye2" /> Sportswriter Bruce Copeland compiled the [[All-Pro]] list for the 1920 season, but no player from the Flyers was on the list.<ref>{{Harvnb|Hogrogian|1984|pp=1–2}}</ref> Ken Huffine decided to be affiliated with the Chicago Stayles after the 1920 season, and [[Cooney Checkaye]] took over the role the following season.<ref name="encyc">{{Cite web | url=http://www.pro-football-reference.com/teams/mun/ | title=Muncie Flyers Team Encyclopedia | work=''[[Pro-Football-Reference.com]]'' | publisher=Sports Reference | accessdate=July 30, 2012}}</ref> It did not help, and the Flyers' final year in the APFA was 1921.<ref name="encyc" /> As of 2012, no players from the 1920 Muncie Flyers have been enshrined into the [[Pro Football Hall of Fame]].<ref>{{Cite web | url=http://www.profootballhof.com/hof/alphabetical.aspx | title=Alphabetically | publisher=[[Pro Football Hall of Fame]] | accessdate=July 30, 2012}}</ref>

== Standings ==
{{1920 APFA standings}}

== Roster ==
{| class="toccolours" style="text-align: left;"
|-
! colspan="9" style="background:#C41E3A; color:white; text-align:center;"|'''Muncie Flyers 1920 roster'''<ref name="sye1" />
|-
|
| style="font-size:95%; vertical-align:top;"| '''Quarterbacks'''
* [[Cooney Checkaye]]

'''Running backs'''
* [[Archie Erehart]]
* [[Ken Huffine]] FB

'''Ends'''
* [[Spencer Pope]]
* [[Jess Reno]]
* [[Pete Slane]]
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| '''Tackles'''
* [[Russ Hathaway]]
* [[Bobby Berns]]

'''Guards'''
* [[John Redmond]]
* [[Babe Hole]]

'''Center'''
* [[Owen Floyd]]
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| '''Substitutes'''
* [[Mickey Hole]]
* [[Cliff Baldwin]]

'''Coaching staff'''
* [[Earl Ball]] – manager
* [[Ken Huffine]] – head coach

|}

== Notes ==
{{Reflist|30em}}

== References ==
* {{Cite journal | last1=Braunwart | first1=Bob | last2=Carroll | first2=Bob | year=1981 | title=The First NFL Game(s) | url=http://www.profootballresearchers.org/Coffin_Corner/03-02-059.pdf | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=3 | issue=2 | archiveurl=http://www.webcitation.org/5zNOTWb1I | archivedate=June 11, 2011 | ref=harv}}
* {{cite book | last1 = Davis | first1 = Jeff | title = Papa Bear, The Life and Legacy of George Halas | publisher = McGraw-Hill | year = 2005 | location = New York | isbn = 0071460543 | ref=harv}}
* {{cite book | last = Peterson | first1 = Robert | year=1997 | title = Pigskin: The Early Years of Pro Football | publisher = [[Oxford University Press]] | location = New York | isbn = 0195076079 | ref=harv}}
* {{Cite journal | author=PFRA Research | year=1980 | url=http://www.profootballresearchers.org/Coffin_Corner/02-08-038.pdf | title=Happy Birthday NFL? | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=2 | issue=8 | archiveurl=http://www.webcitation.org/5zNOTWbno | archivedate=June 11, 2011 | ref=harv}}
* {{Cite book | last1=Siwoff | first1=Seymour | last2=Zimmber | first2=Jon | last3=Marini | first3=Matt | year=2010 | title=The Official NFL Record and Fact Book 2010 | publisher=[[National Football League]] | isbn=9781603208338 | ref=harv}}
* {{Cite journal | last=Sye | first=Roy | year=2002 | url=http://www.profootballresearchers.org/Coffin_Corner/24-03-937.pdf | title=Muncie Flyers – 1920 | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=24 | issue=2|archiveurl=http://www.webcitation.org/65r57V35U | archivedate=March 1, 2012 | ref=harv}}

{{1920 APFA season by team}}
{{Muncie Flyers seasons}}

{{DEFAULTSORT:1920 Muncie Flyers Season}}
[[Category:Muncie Flyers seasons]]
[[Category:1920 American Professional Football Association season by team|Muncie Flyers]]