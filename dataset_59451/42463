{{About|the aircraft engine|the historic newspaper in [[Bristol]]|Bristol Mercury (newspaper)|the 1917 14-cylinder radial engine|Cosmos Mercury}}
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Mercury              
 |image=Bristol_Mercury_RAFM.JPG
 |caption=Preserved Bristol Mercury VII on display at the [[Royal Air Force Museum London]]
}}{{Infobox Aircraft Engine
 |type=[[Reciprocating engine|Piston]] [[Aircraft engine|aero engine]]
 |manufacturer=[[Bristol Aeroplane Company]]
 |designer=[[Roy Fedden]]
 |first run={{avyear|1925}}
 |major applications=[[Bristol Blenheim]]<br>[[Gloster Gladiator]]<BR>[[Fokker D.XXI]]
 |number built =20,700  
 |program cost = 
 |unit cost = 
 |developed from =[[Bristol Jupiter]] 
 |developed into =[[Bristol Pegasus]] 
 |variants with their own articles =
}}
|}
The '''Bristol Mercury''' is a nine-cylinder, air-cooled, single-row, piston [[radial engine]]. Designed by [[Roy Fedden]] of the [[Bristol Aeroplane Company]] it was used to power both civil and military aircraft of the 1930s and 1940s. Developed from the earlier [[Bristol Jupiter|Jupiter]] engine, later variants could produce 800 [[horsepower]] (600&nbsp;kW) from its [[Engine displacement|capacity]] of 1,500 [[cubic inch]]es (25 L) by use of a geared [[supercharger]].

Almost 21,000 engines were produced, with a number also being built in Europe under licence. At least three Bristol Mercuries remain airworthy in 2010, with other preserved examples on public display in [[aviation museum]]s.

==Design and development==
The '''Mercury''' was developed by the [[Bristol Aeroplane Company]] in 1925 as their [[Bristol Jupiter]] was reaching the end of its lifespan. Although the Mercury initially failed to attract much interest, the [[Air Ministry]] eventually funded three prototypes and it became another winner for the designer [[Roy Fedden]].

With the widespread introduction of [[supercharger]]s to the aviation industry in order to improve altitude performance, Fedden felt it was reasonable to use a small amount of boost at all times in order to improve performance of an otherwise smaller engine. Instead of designing an entirely new block, the existing Jupiter parts were re-used with the stroke reduced by one inch (25&nbsp;mm). The smaller capacity engine was then boosted back to Jupiter power levels, while running at higher rpm and thus requiring a reduction gear for the [[Propeller (aircraft)|propeller]]. The same techniques were applied to the original Jupiter-sized engine to produce the [[Bristol Pegasus|Pegasus]].

The Mercury's smaller size was aimed at [[fighter aircraft|fighter]] use and it powered the [[Gloster Gauntlet]] and its successor, the [[Gloster Gladiator]]. It was intended that the larger Pegasus would be for [[bomber aircraft|bomber]]s but as the power ratings of both engines rose, the Mercury found itself being used in almost all roles. Perhaps its most famous use was in a twin-engine [[light bomber]], the [[Bristol Blenheim]].

In 1938 Roy Fedden pressed the Air Ministry to import supplies of  [[Avgas#100.2F130|100 octane aviation spirit (gasoline)]] from the USA. This new fuel would allow aero engines to run at higher [[compression ratio]]s and [[supercharger]] boost pressure than the existing 87-octane fuel, thus increasing the power. The Mercury XV was one of the first British aero engines to be type-tested and cleared to use the 100-octane fuel in 1939. This engine was capable of running with a boost pressure of +9&nbsp;lbs/sq.in and was first used in the Blenheim Mk IV.<ref>Warner 2005, pp. 100, 112, 135.</ref>

The Mercury was also the first British aero engine to be approved for use with variable-pitch propellers.

The Bristol company and its [[shadow factories]] produced 20,700 examples of the engine.<ref>Bridgman (Jane's) 1998, p. 270.</ref>
Outside the United Kingdom, Mercury was licence-built in Poland and used in their [[PZL P.11]] fighters. It was also built by [[NOHAB]] in Sweden and used in the Swedish Gloster Gladiator fighters and in the [[Saab 17]] dive-bomber. In Italy, it was built by [[Alfa Romeo]] as the Mercurius. In [[Czechoslovakia]] it was built by [[Walter Engines]]. In Finland, it was built by [[Tampella]] and mainly used on [[Bristol Blenheim]] bombers.

==Variants==
''Note:''<ref>List from Lumsden 2003, pp. 104-108</ref>
;Mercury I
:(1926) 808 hp, direct drive. [[Schneider Trophy]] racing engine.<ref>Gunston 1989, p.32.</ref>
;Mercury II
:(1928) 420 hp, [[compression ratio]] 5.3:1.
;Mercury IIA
:(1928) 440 hp
;Mercury III
:(1929) 485 hp, compression ratio 4.8:1, 0.5:1 reduction gear.
[[File:Bristol (NOHAB) Mercury.jpg|thumb|right|Mercury license built by [[NOHAB]]]]
;Mercury IIIA
:Minor modification of Mercury III.
;Mercury IV
:(1929) 485 hp, 0.656:1 reduction gear.
;Mercury IVA
:(1931) 510 hp.
;Mercury IVS.2
:(1932) 510 hp.
;Mercury (Short stroke)
:Unsuccessful experimental short stroke (5.0 in) version, 390 hp.
;Mercury V
:546 hp (became the Pegasus IS.2)
;Mercury VIS
:(1933) 605 hp, see specifications section.
[[File:Bristol Mercury.jpg|thumb|right|Side view showing valve gear detail.]]
;Mercury VISP
:(1931) 605 hp, 'P' for [[Iran|Persia]].
;Mercury VIS.2
:(1933) 605 hp.
;Mercury VIA
:(1928) 575 hp (became the Pegasus IU.2)
;Mercury VIIA
:560 hp (became the Pegasus IM.2)
;Mercury VIII
:(1935) 825 hp, compression ratio 6.25:1, lightened engine.
;Mercury VIIIA
:Mercury VIII fitted with [[Synchronization gear|gun synchronisation]] gear for the Gloster Gladiator
;Mercury VIIIA
:535 hp, second use of VIIIA designation, (became the Pegasus IU.2P)
;Mercury IX
:(1935) 825 hp, lightened engine.
;Mercury X
:(1937) 820 hp.
;Mercury XI
:(1937) 820 hp.
;Mercury XII
(1937) 820&nbsp;hp
;Mercury XV
:(1938) 825 hp, developed from Mercury VIII. Converted to run on 100 Octane fuel (previously 87 Octane).
;Mercury XVI
:830 hp.
;Mercury XX
:(1940) 810 hp
;Mercury 25
:(1941) 825 hp. Mercury XV with crankshaft modifications.
;Mercury 26
:825 hp. As Mercury 25 with modified carburettor.
;Mercury 30
:(1941) 810 hp, Mercury XX with crankshaft modifications.
;Mercury 31
:(1945) 810 hp, Mercury 30 with carburettor modifications and fixed pitch propeller for [[General Aircraft Hamilcar|Hamilcar X]].

==Applications==
[[File:Fokker g1.gif|thumb|right|The Mercury powered Fokker G.1]]
''Note:''<ref>List from Lumsden, the Mercury may not be the main powerplant for these types</ref>
{{colbegin|3}}
*[[Airspeed Cambridge]]
*[[Blackburn Skua]]
*[[Boulton Paul Balliol|Boulton Paul P.108]]
*[[Bristol Blenheim]]
*[[Bristol Bolingbroke]]
*[[Bristol Bulldog]]
*[[Bristol Bullpup]]
*[[Bristol Type 101]]
*[[Bristol Type 118]]
*[[Bristol Type 133]]
*[[Bristol Type 142]]
*[[Bristol Type 146]]
*[[Bristol Type 148]]
*[[Breda Ba.27]]
*[[Fairey Flycatcher]]
*[[Fokker D XXI]]
*[[Fokker G.1]]
*[[General Aircraft Hamilcar|General Aircraft Hamilcar X]]
*[[Gloster Gamecock]]
*[[Gloster Gladiator]]
*[[Gloster Gauntlet]]
*[[Gloster Gnatsnapper]]
*[[Gloster Goring]]
*[[Hawker Audax]]
*[[Hawker F.20/27]]
*[[Hawker Fury]]
*[[Hawker Hart]]
*[[Hawker Hind]]
*[[Hawker Hoopoe]]
*[[Hawker F.20/27]]
*[[IMAM Ro.30]]
*[[Koolhoven F.K.52]]
*[[Letov Š-31]]
*[[Miles Martinet]]
*[[Miles Master]]
*[[PZL P.11]]
*[[Saab 17]]
*[[Short Crusader]]
*[[Supermarine Sea Otter]]
*[[Valmet Vihuri]]
*[[Vickers Jockey]]
*[[Westland Interceptor]]
*[[Westland Lysander]]
{{colend}}

==Survivors==
A Bristol Mercury (Mk. 20) powered [[Westland Lysander]] (G-AZWT) remains airworthy in 2017 at the [[Shuttleworth Collection]], [[Old Warden]], United Kingdom and is flown at home air displays throughout the summer months.<ref>[http://www.shuttleworth.org/collection/westlandlysander/ The Shuttleworth Collection - Lysander] Retrieved: 07 February 2017</ref>

A second Bristol Mercury (Mk. 30) is airworthy as of 2017 at The Shuttleworth Collection, Old Warden powering the collection's [[Gloster Gladiator]] (G-AMRK) and is flown at home air displays throughout the summer months.<ref>[http://www.shuttleworth.org/collection/glostergladiator/ The Shuttleworth Collection - Gladiator] Retrieved: 07 February 2017</ref>

The [[Canadian Warplane Heritage Museum]] has a Lysander IIIA in flying condition as does the [[Vintage Wings of Canada]].<ref>[http://www.warplane.com/pages/restoration_lysander_update.html Canadian Warplane Heritage Museum - Westland Lysander] www.warplane.com Retrieved: 22 September 2010</ref><ref>[http://www.vintagewings.ca/page?a=450&lang=en-CA Vintage Wings - Westland Lysander] www.vintagewings.ca Retrieved: 22 September 2010</ref>

==Engines on display==
*A Bristol Mercury VII is on display at the [[Royal Air Force Museum London]].<ref>[http://navigator.rafmuseum.org/results.do?view=detail&db=object&pageSize=1&id=33278 RAF Museum - Bristol Mercury] Retrieved: 4 August 2009</ref>
*Another example of a Bristol Mercury VII is on display at the [[Shuttleworth Collection]].
*A sectioned Bristol Mercury is on display at the [[Fleet Air Arm Museum]], [[RNAS Yeovilton (HMS Heron)|RNAS Yeovilton]].

==Specifications (Mercury VI-S)==
[[File:Bristol MercuryVII.JPG|thumb|right|Bristol Mercury VII on display at the [[Shuttleworth Collection]]]]
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref=''Lumsden''<ref>Lumsden 2003, p.105.</ref>
|type=Nine-cylinder single-row supercharged air-cooled [[radial engine]]
|bore=5.75 in (146 mm)
|stroke=6.5 in (165 mm)
|displacement=1,519 in³ (24.9 L)
|length=47 in (1,194 mm)
|diameter=51.5 in (1,307 mm)
|width=
|height=
|weight=966 lb (438 kg)
|valvetrain=Overhead valve, 4 valves per cylinder – 2 intake and 2 [[sodium]]-filled exhaust
|supercharger=Single-speed [[centrifugal type supercharger]]
|turbocharger=
|fuelsystem=[[Claudel-Hobson]] [[carburetor|carburettor]]
|fueltype=87 [[octane rating|Octane]] [[gasoline|petrol]]
|oilsystem=
|coolingsystem=Air-cooled
|power=<br>
* 612 hp (457 kW) at 2,750 rpm for takeoff
* 636 hp (474 kW) at 2,750 rpm at 15,500 ft (4,730 m)
|specpower=0.4 hp/in³ (18.35 kW/L)
|compression=6:1
|fuelcon=
|specfuelcon=0.49 lb/(hp&middot;h) (300 g/kWh)
|oilcon=
|power/weight=0.63 hp/lb (1.04 kW/kg)
|reduction_gear=[[Farman Aviation Works|Farman]] [[epicyclic gearing]], 0.5:1, left hand tractor
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Bristol Jupiter]]
*[[Bristol Pegasus]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[BMW 114]]
*[[BMW 132]]
*[[Bramo 323]]
*[[Pratt & Whitney R-1340]]
*[[Shvetsov ASh-62]]
*[[Wright R-1820]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Bridgman, L, (ed.) (1998) ''Jane's fighting aircraft of World War II.'' Crescent. ISBN 0-517-67964-7
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Warner, G. ''The Bristol Blenheim: A Complete History''. London: Crécy Publishing, 2nd edition 2005. ISBN 0-85979-101-7.
{{refend}}

==Further reading==
* Gunston, Bill. ''Development of Piston Aero Engines''. Cambridge, England. Patrick Stephens Limited, 2006. ISBN 0-7509-4478-1

==External links==
{{Commons category|Bristol Mercury}}
* [http://republika.pl/rola1/files/pzl11_start_run_01.mp3 Recorded sound of the Mercury V S2 engine] used in [[PZL P.11c]] (mp3 format)

{{BristolAeroengines}}
{{Alfa Romeo aeroengines}}
{{Walter aeroengines}}

[[Category:Radial engines]]
[[Category:Bristol aircraft engines|Mercury]]
[[Category:Aircraft piston engines 1920–1929]]