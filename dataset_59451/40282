{{other people|James Corcoran}}
{{Notability|Biographies|date=December 2015}}

{{Infobox person
|name          = Jimmy Corcoran
|image         = 
|image_size    = 
|caption       = 
|birth_name    = James J. Corcoran
|birth_date    = Circa 1819
|birth_place   = [[Balbriggan]], [[County Dublin]], Ireland
|death_date    = {{death date and age|mf=yes|1900|11|13|1819|1|1}}
|death_place   = [[Manhattan]], New York, U.S.
|resting_place = 
|nationality   = [[Irish-American]]
|other_names   = Paddy Corcoran 
|known_for     = Founder of the Manhattan shanty enclaves of "[[Corcoran's Roost]]" and [[Kip's Bay]]. 
|occupation    = Truckman and businessman
|spouse        = Kathleen Corcoran 
|children      = 10}}

'''James J. "Jimmy" Corcoran''' (c. 1819 – November 13, 1900) was an Irish-born American laborer and well-known personality among the [[Irish-American]] community of the historic "[[Corcoran's Roost]]" and the [[Kip's Bay]] districts, roughly the area near [[40th Street (Manhattan)|40th Street]] and [[First Avenue (Manhattan)|First Avenue]] in [[Manhattan]], and was widely regarded as the champion of working class Irish immigrants between 1850 and 1880.

He is alleged to have been somewhat of an underworld figure, under the alias '''Paddy Corcoran''', founding the ''Rag Gang'' which operated with his sons on the Manhattan waterfront during the late 19th century.<ref>Federal Writers' Project. ''New York City: Vol 1, New York City Guide''. Vol. I. American Guide Series. New York: Random House, 1939.</ref><ref>Rachlis, Eugene and John E. Marqusee. ''The Landlords''. New York: Random House, 1963. (pg. 163)<!-- ISSN/ISBN needed --></ref><ref>Wolfe, Gerard R. ''New York, 15 Walking Tours: An Architectural Guide to the Metropolis''. New York: McGraw-Hill Professional, 2003. (pg. 355); ISBN 0-07-141185-2</ref><ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 335) ISBN 1-56025-275-8</ref>

==Biography==
Corcoran was born in [[Balbriggan]], [[County Dublin]], and emigrated to the United States when he was 25. He worked as a laborer in [[New Orleans]] for a time and also lived in [[Cold Spring, New York|Cold-Spring-on-the-Hudson (present-day Cold Spring, New York)]] before settling in New York City prior to the [[American Civil War]]. He found work as a truckman and, experiencing some [[anti-Irish|prejudice]], Corcoran made a home in a [[squatter|squatter colony]] in Dutch Hill. The colony was constructed on an earth mound near [[40th Street (Manhattan)|40th Street]] and the [[First Avenue (nightclub)|First Avenue]] and was considered a high-crime poverty-stricken area of the city.{{citation needed|date=August 2015}}

Corcoran was the first to organize neighboring squatters to build a permanent shanty community. By the 1860s, he had become acknowledged as head of the colony. During its early years, residents feuded with neighboring squatters on Clara's Hill, founded and named by immigrants who had lived in the area of the same name in [[Mountmellick|Mountmellick, County Laois]]. Frequent fighting led to altercations with police, whom the squatters often turn against to the amusement of onlookers, and Corcoran would often put up bail for offenders and was reputed to have "a caustic tongue and a ready wit" when he arrived at the local station house. {{Citation needed|date=February 2010}}

The Corcoran family eventually left the colony and moved to a nearby brick house on East 40th Street but remained involved in the shanty's affairs for another two decades. In May 1899, he offered the deed to Corcoran's Roost as security to release Robert Dougherty {{Who|date=August 2015}} {{Clarify|date=August 2015}} on bail from Yorkville Court.<ref>"James Corcoran In Court; Ruler of Corcoran's Roost Goes Bail for a Man After His Own Heart".''New York Times.'' May 2, 1899.</ref> 

Corcoran's wife, Kathleen, mother to his 10 children, died in August 1899. After his wife's death, Corcoran lived for another year before he died at his home "shrived and regretted" on November 13, 1900. He had been successful in business during his later years, with an estate worth $25,000 and owning several [[roadhouse (facility)|roadhouses]], which he left to his four surviving children upon his death.<ref name="New York Times 1900">[https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9D02E4DE143FE433A25757C1A9679D946197D6CF "James J. Corcoran Dead.; Ex-Chief of Irish Squatter Colony on Old Dutch Hill Passes Away in His Eighty-second Year"], nytimes.com, November 14, 1900.</ref>

The earth from Dutch Hill was later partly used to construct present-day [[Cob Dock]] at the [[New York Navy Yard]] and its site became a tenement district.<ref name="New York Times 1900"/> [[Tudor City]] was built on the site of Corcoran's Roost during the late 1920s and a [[Gothic architecture|Gothic inscription]] was later engraved above the entrance of the central [[Tudor City|Tudor Tower]] in his memory.<ref>Nash, Eric Peter. ''Manhattan Skyscrapers''. New York: Princeton Architectural Press, 1999. (pg. 43); ISBN 1-56898-181-3</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Corcoran, James J.}}
[[Category:1810s births]]
[[Category:1900 deaths]]
[[Category:American people of Irish descent]]
[[Category:Criminals from New York City]]
[[Category:Date of birth missing]]
[[Category:People from Manhattan]]
[[Category:People from County Dublin]]
[[Category:Disease-related deaths in New York]]