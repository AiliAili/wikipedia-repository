{{Italic title}}
{{Infobox book
| name         = Green Henry
| title_orig   = Der grüne Heinrich
| translator  = 
| image        = Der grüne Heinrich I.JPG
| image_size    = 180px
| caption = Covers of the first edition
| author       = [[Gottfried Keller]]
| cover_artist = 
| country      = Germany  
| language     = German
| subject      =
| genre        = [[Bildungsroman]]
| publisher    = Vieweg Verlag (1855) and Verlag Göschen (1879-1880)
| release_date = [[1855 in literature|1855]] and [[1880 in literature|1880]]
| english_pub_date = 
| media_type   = Print ([[Hardcover]] & [[Paperback]])
| pages        = 
| isbn         = 
| dewey=
| congress= 
| oclc= 
}}

'''''Green Henry''''' ({{Lang-de|'''Der grüne Heinrich'''}}) is a partially [[autobiographical]] [[novel]] by the [[Swiss]] author [[Gottfried Keller]], first published in 1855, and extensively revised in 1879. Truth is freely mingled with fiction, and there is a generalizing purpose to exhibit the psychic disease that affected the whole generation of the transition from [[romanticism]] to [[Literary realism|realism]] in life and art. The work stands with Goethe's ''[[Wilhelm Meister's Apprenticeship]]'' and Adalbert Stifter's ''[[Der Nachsommer]]'' as one of the three most important examples of a [[Bildungsroman]].

==Plot==
''Green Henry'' details the life of Heinrich Lee from childhood through his first romantic encounters, his fledgling attempts at becoming a [[Painting|painter]] in [[Munich]], and his eventual installation as a [[Chancery (diplomacy)|chancery]] [[clerk (position)|clerk]]. The story gets its name from the color that Heinrich affected in dress.

Heinrich is a Swiss burgher's son, brought up too tenderly by a widowed mother. After youthful pranks and experiences, and a not altogether justified dismissal from school, he idles away some time in his mother's village in activities of which the description is far better worthwhile than was the reality. He determines to be a painter, and goes to Munich's artistic Bohemia. From there, he finds his way to a count's mansion, and then he returns home to his dying mother and an all-too-tardy and brief repentance.

The much revised second version has Heinrich abandoning art to enter the civil service. This experience affords occasion for extended political reflections. The tone of the reminiscences makes it clear that Keller would have the reader understand that Heinrich has lived through and risen out of his instability and irresolution and sees life steadily and cheerfully at last.

==Development==
''Green Henry'' was written from 1850 to 1855.  It is the most personal of all of Keller's works, and is significantly influenced by [[Jean-Jacques Rousseau]]'s doctrine of a return to nature. At first intended as a short narrative of the collapse of the life of a young artist, the book expanded as its composition progressed into a huge work that treats, in poetically transfigured manner, all the events in Keller's life up to his return to Zürich in 1842.

Its reception by the literary world was cool, but after a revised edition was issued in 1879, it won general and often extravagant praise.

==Evaluation==
[[Benjamin W. Wells]] reviewed the book so:

<blockquote>
Keen insight, fresh humor and instinct for realistic narration are its outstanding merits; its faults are lack of proportion, occasional garrulity and obtruded moralizing, but most of all the doubt that it leaves in the reader whether the Heinrich who had shown such persistent lack of character, especially in his relations with his mother, would so quickly be capable of discovering, rather than recovering, a normal balance of mind.
</blockquote>

Jacob Wittmer Hartmann characterizes the 2nd edition of 1879 and a “rounded and satisfying artistic product.” [[New International Encyclopedia|''The New International Encyclopædia'']] praises the 2nd edition as a significant improvement over the first.

==See also==
{{portal|Novels}}
*[[Bildungsroman]]
{{clear}}

==Notes==
{{No footnotes|date=May 2013}}
{{reflist}}

==References==
* {{Cite Americana|wstitle=Keller, Gottfried|first=Jacob Wittmer |last=Hartmann }}
* {{Cite NIE|wstitle=Keller, Gottfried|year=1905}}
;Attribution
*{{Americana|wstitle=Der Grüne Heinrich|first=Benjamin W. |last=Wells |authorlink=Benjamin W. Wells}}

{{Authority control}}
[[Category:1855 novels]]
[[Category:Swiss bildungsromans]]
[[Category:German novels]]
[[Category:Autobiographical novels]]