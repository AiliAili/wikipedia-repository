{{featured article}}

{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:RNConte di Cavour-Original.jpg|300px]]
|Ship caption=''Conte di Cavour'' at speed in her original configuration
}}
{{Infobox ship class overview
|Name=''Conte di Cavour'' class
|Builders=
|Operators=*{{navy|Kingdom of Italy}}
*{{navy|Soviet Union}}
|Class before={{ship|Italian battleship|Dante Alighieri||2}}
|Class after={{sclass-|Andrea Doria|battleship|4}}
|Cost=
|Built range=1910–15
|In service range=
|In commission range=1914–55
|Total ships completed=3
|Total ships scrapped=2
|Total ships lost=1
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(as built)
|Ship type=[[Dreadnought battleship]]
|Ship displacement=*{{convert|23088|LT|t}} ([[Displacement (ship)#Standard displacement|standard]])
*{{convert|25086|LT|t}} ([[deep load]])
|Ship length={{convert|176|m|ftin|abbr=on}} ([[Length overall|o/a]])
|Ship beam={{convert|28|m|ftin|abbr=on}}
|Ship draught= {{convert|9.3|m|ftin|abbr=on}}
|Ship power=*{{convert|30700|-|32800|shp|abbr=on|lk=in}}
*20 × [[Water-tube boiler]]s
|Ship propulsion=*4 × Shafts
*4 × [[Steam turbine]]s
|Ship speed={{convert|21.5|kn|lk=in}}
|Ship range={{convert|4800|nmi|abbr=on|lk=in}} at {{convert|10|kn}}
|Ship complement=31 officers and 969 enlisted men
|Ship armament=* 3 × triple, 2 × twin [[305 mm /46 Model 1909|{{convert|305|mm|in|0|abbr=on}} guns]]
* 18 × single {{convert|120|mm|in|abbr=on}} guns
* 14 × single {{convert|76.2|mm|in|abbr=on|0}} guns
* 3 × {{convert|450|mm|in|1|abbr=on}} [[torpedo tube]]s 
|Ship armor=* [[Belt armor|Waterline belt]]: {{convert|250|-|130|mm|in|abbr=on|1}}
* [[Deck (ship)|Deck]]: {{convert|24|-|40|mm|in|abbr=on|1}}
* [[Gun turret]]s: {{convert|280|-|240|mm|in|abbr=on|1}}
* [[Barbette]]s: {{convert|230|-|130|mm|in|abbr=on|1}}
* [[Conning tower]]s: {{convert|280|-|180|mm|in|abbr=on|1}} 
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(after reconstruction)
|Ship displacement={{convert|29100|LT|t}} (deep load)
|Ship length={{convert|186.4|m|ftin|abbr=on}}
|Ship beam={{convert|33.1|m|ftin|abbr=on}}
|Ship power=*{{convert|75000|shp|abbr=on}}
*8 × [[Yarrow boiler]]s
|Ship propulsion=*2 × Shafts
*2 × Geared steam turbines
|Ship speed={{convert|27|kn}}
|Ship range={{convert|6400|nmi|abbr=on}} at {{convert|13|kn}}
|Ship complement=1,260
|Ship armament=* 2 × triple, 2 × twin {{convert|320|mm|in|1|abbr=on}}
* 6 × twin {{convert|120|mm|in|abbr=on}}
* 4 × twin [[Škoda_10_cm_K10#OTO_100.2F47_History|{{convert|100|mm|in|abbr=on|0}} / 47&nbsp;caliber]] [[AA gun]]s 
|Ship armor=*Deck: {{convert|166|-|135|mm|in|abbr=on|1}}
*Barbettes: {{convert|280|-|130|mm|in|abbr=on|1}}
|Ship notes=
}}
|}

The '''''Conte di Cavour''–class battleships''' were a group of three [[dreadnought]]s built for the Royal Italian Navy (''[[Regia Marina]]'') in the 1910s. The ships were completed during [[World War I]], but none saw action before the end of hostilities. {{ship|Italian battleship|Leonardo da Vinci||2}} was sunk by a [[magazine (artillery)|magazine]] explosion in 1916 and sold for [[ship breaking|scrap]] in 1923. The two surviving ships, {{ship|Italian battleship|Conte di Cavour||2}} and {{ship|Italian battleship|Giulio Cesare||2}}, supported operations during the [[Corfu Incident]] in 1923. They were extensively reconstructed between 1933 and 1937 with more powerful guns, additional armor and considerably more speed than before.

Both ships participated in the [[Battle of Calabria]] in July 1940, when ''Giulio Cesare'' was lightly damaged. They were both present when British torpedo bombers [[Battle of Taranto|attacked]] the fleet at [[Taranto]] in November 1940, and ''Conte di Cavour'' was torpedoed. She was [[Ship grounding|grounded]] with most of her hull underwater and her repairs were not completed before the [[Italian surrender]] in September 1943. ''Conte di Cavour'' was scrapped in 1946. ''Giulio Cesare'' escorted several convoys, and participated in the [[Battle of Cape Spartivento]] in late 1940 and the [[First Battle of Sirte]] in late 1941. She was designated as a [[training ship]] in early 1942, and escaped to [[Malta]] after Italy surrendered. The ship was transferred to the [[Soviet Union]] in 1949 and renamed ''Novorossiysk''. The Soviets also used her for training until she was sunk when a [[naval mine|mine]] exploded in 1955. She was scrapped in 1957.

==Design and description==
[[File:Drawing of Conte di Cavour-class battleships.jpg|thumb|left|Original configuration]]
The ''Conte di Cavour''–class ships were designed by [[Rear Admiral]] Engineer [[Edoardo Masdea]], Chief Constructor of the ''Regia Marina'', and were ordered in response to French plans to build the {{sclass-|Courbet|battleship|2}}s. They were intended to be superior to the ''Courbet''s and to remedy {{ship|Italian battleship|Dante Alighieri||2}}'s perceived flaws of weak protection and armament. As upgrading a warship's protection and armament on a similar displacement typically requires a loss in speed, the ships were not designed to reach the {{convert|24|kn|lk=in}} of their predecessor. They were still given a {{convert|1.5|to|2|kn}} advantage over the {{convert|20|to|21|kn|adj=on}} standard of most foreign dreadnoughts.<ref>Giorgerini, pp. 268–70, 272</ref> Foreign dreadnoughts were being designed with {{convert|13.5|in|adj=on|disp=flip|sp=us}} guns, but the ''Regia Marina'' was forced to use {{convert|305|mm|in|adj=on|sp=us|0}} guns in the ''Conte di Cavour''s because Italy lacked the ability to build larger guns.<ref name=s12>Stille, p. 12</ref> An additional gun, making a total of 13, was added to offset this deficiency.<ref>Giorgerini, p. 269</ref>

Taking advantage of the lengthy building times of these ships, other countries were able to build dreadnoughts that were superior in protection and armament,<ref>Giorgerini, p. 270</ref> with the exception of the French.<ref name=s12/> Construction was delayed by late deliveries of the 305-millimeter guns and armor plates as well as shortages of labor. The [[Italo-Turkish War]] of 1911–1912 diverted workers at the shipyards for repairs and maintenance of the ships participating in the war. The Italians imported the raw nickel steel for their armor from America and Britain and processed it into their equivalent of [[Krupp cemented armor]], called Terni cemented, but there were problems with this process and suitable plates took longer to produce than planned.<ref name=g02/>

===Basic characteristics===
The ships of the ''Conte di Cavour'' class were {{convert|168.9|m|ftin|sp=us}} [[length at the waterline|long at the waterline]], and {{convert|176|m|ftin|sp=us}} [[length overall|overall]]. They had a [[beam (nautical)|beam]] of {{convert|28|m|ftin|sp=us}}, and a [[draft (hull)|draft]] of {{convert|9.3|m|ftin|sp=us}}.<ref name=randal259>Gardiner & Gray, p. 259</ref> They [[displacement (ship)|displaced]] {{convert|23088|LT|t}} at normal load, and {{convert|25086|LT|t}} at [[deep load]]. The ''Conte di Cavour'' class was provided with a complete [[double bottom]] and their hulls were subdivided by 23 longitudinal and transverse [[bulkhead (partition)|bulkheads]]. The ships had two [[rudder]]s, both on the centerline. They had a crew of 31 officers and 969 enlisted men.<ref name=g02>Giorgerini, pp. 270, 272</ref>

===Propulsion===
The original machinery for all three ships consisted of three [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]] sets, arranged in three engine rooms. The center engine room housed one set of turbines that drove the two inner [[propeller shaft]]s. It was flanked by compartments on either side, each housing one turbine set which powered the outer shafts. Steam for the turbines was provided by 20 Blechynden [[water-tube boiler]]s in ''Conte di Cavour'' and ''Leonardo da Vinci'', eight of which burned oil and twelve of which burned both oil and coal. ''Giulio Cesare'' used a dozen each oil-fired and mixed-firing [[Babcock & Wilcox boiler]]s. Designed to reach a maximum speed of {{convert|22.5|kn}}, none of the ships reached this goal on their [[sea trial]]s, despite generally exceeding the rated power of their turbines. They only achieved speeds ranging from {{convert|21.56|to|22.2|kn}} using {{convert|30700|to|32800|shp|lk=in}}. The ships could store a maximum of {{convert|1450|LT|t}} of coal and {{convert|850|LT|t}} of [[fuel oil]]<ref>Giorgerini, pp. 268, 272–73</ref> that gave them a range of {{convert|4800|nmi|lk=in}} at {{convert|10|kn}}, and {{convert|1000|nmi}} at {{convert|22|kn}}.<ref name=randal259/> Each ship was equipped with three [[turbo generator]]s that provided a total of 150&nbsp;kilowatts at 110&nbsp;[[volt]]s.<ref>Bargoni & Gay, p. 17</ref>

===Armament===
As built, the ships' main armament comprised thirteen 46-[[Caliber#Caliber as measurement of length|caliber]] [[305 mm /46 Model 1909|305-millimeter guns]],<ref name=hore175/> designed by [[Armstrong Whitworth]] and [[Vickers]],<ref>Friedman, p. 234</ref> in five gun turrets. The turrets were all on the centerline, with a twin-gun turret [[superfire|superfiring]] over a triple-gun turret in fore and aft pairs, and a third triple turret amidships, designated 'A', 'B', 'Q', 'X', and 'Y' from bow to stern. This was only one fewer gun than the Brazilian {{ship|Brazilian battleship|Rio de Janeiro||2}}, then the most heavily armed battleship in the world; ''Rio de Janeiro''{{'}}s guns were mounted in seven twin-gun turrets.<ref name=hore175>Hore, p. 175</ref> The turrets had an elevation capability of −5° to +20 degrees and the ships could carry 100 [[Cartridge (firearms)|rounds]] for each gun, although 70 was the normal load. Sources disagree regarding these guns' performance, but naval historian Giorgio Giorgerini claims that they fired {{convert|452|kg|lb|adj=on}} [[Armor-piercing shot and shell|armor-piercing (AP)]] projectiles at the rate of one round per minute and that they had a [[muzzle velocity]] of {{convert|840|m/s|ft/s|abbr=on}} which gave a maximum range of {{convert|24000|m|yd|sp=us}}.<ref name=g86>Giorgerini, pp. 268, 276</ref>{{refn|Friedman provides a variety of sources that show armor-piercing shell weights ranging from {{convert|919.16|to|997.2|lb|kg|disp=flip}} and muzzle velocities around {{convert|861|m/s|ft/s|abbr=on}}.<ref>Friedman, pp. 233–34</ref>|group=Note}} The turrets had hydraulic training and elevation, with an auxiliary electric system.<ref>Bargoni & Gay, p. 14</ref>

The [[Battleship secondary armament|secondary armament]] on the first two ships consisted of eighteen 50-caliber {{convert|120|mm|in|adj=on|sp=us|1}} [[120 mm Italian naval gun#50-calibre M1909|guns]],<ref name=c12>Campbell, p.336</ref> also designed by Armstrong Whitworth and Vickers,<ref>Friedman, pp. 240–41</ref> mounted in [[casemate]]s on the sides of the hull. These guns could depress to −10 degrees and had a maximum elevation of +15 degrees; they had a rate of fire of six shots per minute. They could fire a {{convert|22.1|kg|lb|adj=on}} high-explosive projectile with a muzzle velocity of {{convert|850|m/s|ft/s|sp=us}} to a maximum distance of {{convert|12000|yd|m|disp=flip|sp=us}}. The ships carried a total of 3,600 rounds for them. For defense against [[torpedo boat]]s, the ships carried fourteen 50-caliber {{convert|76|mm|abbr=on}} guns; thirteen of these could be mounted on the turret tops, but they could be mounted in 30 different positions, including some on the [[forecastle]] and upper decks. These guns had the same range of elevation as the secondary guns, and their rate of fire was higher at 10 rounds per minute. They fired a {{convert|6|kg|lb|adj=on}} AP projectile with a muzzle velocity of {{convert|815|m/s|ft/s|sp=us}} to a maximum distance of {{convert|10000|yd|m|disp=flip|sp=us}}. The ships were also fitted with three submerged {{convert|45|cm|in|adj=on|sp=us|1}} [[torpedo tube]]s, one on each [[broadside]] and the third in the stern.<ref>Giorgerini, pp. 268, 276–77</ref>

===Armor===
The ''Conte di Cavour''-class ships had a complete [[waterline]] armor belt that was {{convert|2.8|m|ftin|sp=us}} high; {{convert|1.6|m|ftin|sp=us}} of this was below the waterline and {{convert|1.2|m|ftin|sp=us}} above. It had a maximum thickness of {{convert|250|mm|in|1|sp=us}} amidships, reducing to {{convert|130|mm|in|sp=us|1}} towards the stern and {{convert|80|mm|in|sp=us|1}} towards the bow. The lower edge of this belt was a uniform {{convert|170|mm|in|1|sp=us}} in thickness. Above the main belt was a [[strake]] of armor {{convert|220|mm|in|sp=us|1}} thick that extended {{convert|2.3|m|ftin|sp=us}} up to the lower edge of the main deck. Above this strake was a thinner one, 130 millimeters thick, that extended {{convert|138|m|ftin|sp=us}} from the bow to 'X' turret. The upper strake of armor protected the casemates and was {{convert|110|mm|in|1|sp=us}} thick. The ships had two armored [[deck (ship)|decks]]: the main deck was {{convert|24|mm|abbr=on}} thick in two layers on the flat that increased to {{convert|40|mm|sp=us|1}} on the slopes that connected it to the main belt. The second deck was {{convert|30|mm|sp=us|1}} thick, also in two layers. Fore and aft transverse bulkheads connected the armored belt to the decks.<ref>Giorgerini, pp. 270–71</ref>

The frontal armor of the [[gun turrets]] was {{convert|280|mm|in|1|sp=us}} in thickness with {{convert|240|mm|in|adj=on|sp=us|1}} thick sides, and an {{convert|85|mm|in|adj=on|sp=us}} roof and rear.<ref name=g2/> Their [[barbette]]s also had 230-millimeter armor above the forecastle<ref>McLaughlin, p. 421</ref> deck that reduced to {{convert|180|mm|in|sp=us|1}} between the forecastle and upper decks and 130 millimeters below the upper deck. The forward [[conning tower]] had walls 280 millimeters thick; those of the aft conning tower were 180 millimeters thick.<ref>Giorgerini, pp. 270–72</ref> The total weight of the protective armor was {{convert|5150|LT|t}},<ref name=randal259/> just over 25 per cent of the ships' designed displacement. The total weight of the entire protective system was {{convert|6122|LT|t}}, 30.2 per cent of their intended displacement.<ref name=g2>Giorgerini, p. 272</ref>

==Modifications and reconstruction==
[[File:ONI Drawing of Conte di Cavour-class battleship.jpg|thumb|right|upright=1.3|[[Office of Naval Intelligence]] drawing of the ''Conte di Cavour'' class, January 1943]]
Shortly after the end of World War I, the number of 50-caliber 76&nbsp;mm guns was reduced to 13, all mounted on the turret tops, and six new [[Cannon 76/40 Model 1916|40-caliber 76-millimeter]] [[Anti-aircraft gun|anti-aircraft (AA) guns]] were installed abreast the aft funnel. In addition two [[license-built]] [[QF 2 pounder naval gun|2-pounder]] AA guns were mounted on the forecastle deck abreast 'B' turret. In 1925–26 the foremast was replaced by a tetrapodal mast, which was moved forward of the funnels,<ref name=g7>Giorgerini, p. 277</ref> the [[rangefinder]]s were upgraded, and the ships were equipped to handle a [[Macchi M.18]] [[seaplane]] mounted on the center turret. Around that same time, one or both of the ships was equipped with a fixed [[aircraft catapult]] on the port side of the forecastle.{{refn|Sources disagree if ''Giulio Cesare'' was fitted with a catapult or not. Giorgerini says both ships were so equipped;<ref name=g7/> Whitley, Bagnasco & Grossman and Bargoni & Gay say that only ''Conte di Cavour'' received one.<ref name=w8>Whitley, p. 158</ref><ref name=eb4>Bagnasco & Grossman, p. 64</ref><ref>Bargoni & Gay, p. 18</ref>|group=Note}}

The sisters began an extensive reconstruction program directed by [[Vice Admiral]] (''Generale del Genio navale'') Francesco Rotundi in October 1933.<ref name="Bargoni19" /> This lasted until June 1937 for ''Conte di Cavour'' and October 1937 for ''Giulio Cesare'', and resulted in several changes. A new bow section was grafted over the existing bow which increased their length by {{convert|10.31|m|ftin|sp=us}} to {{convert|186.4|m|ftin|sp=us}} and their beam increased to {{convert|28.6|m|ftin|sp=us}}. Their draft at deep load increased to {{convert|10.02|m|ftin|sp=us}} for ''Conte di Cavour'' and {{convert|10.42|m|ftin|sp=us}} for ''Giulio Cesare''.<ref name=eb4/> All of the changes made during their reconstruction increased their displacement to {{convert|26140|LT|t}} at [[Displacement (ship)#Standard displacement|standard load]] and {{convert|29100|LT|t}} at deep load. The ships' crews increased to 1,260 officers and enlisted men.<ref>Brescia, p. 58</ref>
Only 40% of the original ship's structure remained after the reconstruction was completed.<ref name="Bargoni19">Bargoni & Gay, p. 19</ref>
Two of the propeller shafts were removed and the existing turbines were replaced by two Belluzzo geared steam turbines rated at {{convert|75000|shp|abbr=on}}.<ref name=eb4/> The boilers were replaced by eight [[superheated]] [[Yarrow boiler]]s with a working pressure of {{convert|22|atm|kPa psi|0|abbr=on|lk=on}}. On her sea trials in December 1936, before her reconstruction was fully completed, ''Giulio Cesare'' reached a speed of {{convert|28.24|kn}} from {{convert|93430|shp|abbr=on}}.<ref>McLaughlin, p. 422</ref> In service their maximum speed was about {{convert|27|kn}}. The ships now carried {{convert|2550|-|2605|LT|t}} of fuel oil which provided them with a range of {{convert|6400|nmi}} at a speed of {{convert|13|kn}}.<ref>Bagnasco & Grossman, pp. 64–65</ref>

[[File:Giulioce07.jpg|thumb|right|''Giulio Cesare'' leading her sister after their reconstruction]]
The center turret and the torpedo tubes were removed and all of the existing secondary armament and AA guns were replaced by a dozen [[120 mm Italian naval gun#50-calibre OTO 1933|120-millimeter guns]] in six twin-gun turrets<ref name=c12/> and eight {{convert|102|mm|in|adj=on|sp=us|0}} AA guns in twin turrets. In addition the ships were fitted with a dozen 54-caliber [[Società Italiana Ernesto Breda|Breda]] [[Cannone-Mitragliera da 37/54 (Breda)|{{convert|37|mm|sp=us|adj=on|1}}]] light AA guns in six twin-gun mounts and twelve [[Breda Model 1931 Machine Gun|{{convert|13.2|mm|sp=us|adj=on|2}} Breda M31 anti-aircraft]] [[machine gun]]s, also in twin mounts.<ref name=eb5>Bagnasco & Grossman, p. 65</ref> The {{convert|305|mm|in|adj=on|sp=us}} guns were bored out to 320 millimeters (12.6&nbsp;in) and their turrets were modified to use electric power, a fixed loading angle of +12 degrees, and the guns could now elevate to +27 degrees.<ref>McLaughlin, p. 420</ref> The 320&nbsp;mm AP shells weighed {{convert|525|kg|sp=us}} and had a maximum range of {{convert|28600|m|yd|sp=us}} with a muzzle velocity of {{convert|830|m/s|ft/s|abbr=on}}.<ref>Campbell, p. 322</ref> In 1940 the 13.2&nbsp;mm machine guns were replaced by 65-caliber {{convert|20|mm|sp=us|adj=on|1}} AA guns in twin mounts. ''Giulio Cesare'' received two more twin mounts as well as four additional 37&nbsp;mm guns in twin mounts on the forecastle between the two turrets in 1941.<ref name=w8/>
The tetrapodal mast was replaced with a new forward conning tower, protected with {{convert|260|mm|in|sp=us|adj=on|1}} thick armor.<ref name="Bargoni21">Bargoni & Gay, p. 21</ref> Atop the conning tower there was a director fitted with two rangefinders, with a base length of {{convert|7.2|m|sp=us|1}}.<ref name="Bargoni21"/>

The deck armor was increased during reconstruction to a total of {{convert|135|mm|in|sp=us}} over the engine and boiler rooms and {{convert|166|mm|in|sp=us}} over the magazines, although its distribution over three decks, each with multiple layers, meant that it was considerably less effective than a single plate of the same thickness. The armor protecting the barbettes was reinforced with {{convert|50|mm|in|sp=us|adj=on}} plates.<ref name=m12/> All this armor weighed a total of {{convert|3227|LT|t}}.<ref name=w8/>

The existing underwater protection was replaced by the [[Pugliese torpedo defense system|Pugliese system]] that consisted of a large cylinder surrounded by fuel oil or water that was intended to absorb the blast of a torpedo [[warhead]]. It lacked enough depth to be fully effective against contemporary torpedoes. A major problem of the reconstruction was that the ships' increased draft meant that their waterline armor belt was almost completely submerged with any significant load.<ref name=m12>McLaughlin, pp. 421–22</ref>

==Ships==
{|class="wikitable" border="1"
|-
!Ship
!Namesake
!Builder
!Laid down<ref name=s7>Preston, p. 176</ref>
!Launched<ref name=s7/>
!Completed<ref name="g2"/>
!Fate<ref>Brescia, pp. 58–59</ref>
|-
|{{ship|Italian battleship|Conte di Cavour||2}}
|[[Count#In Italy|Count]] [[Camillo Benso di Cavour]]<ref>Silverstone, p. 296</ref>
|[[La Spezia Naval Base|La Spezia Arsenale]], [[La Spezia]]
|10 August 1910
|10 August 1911
|1 April 1915
|Sunk during the [[Battle of Taranto]] 12 November 1940, [[Marine salvage|salvaged]] 1941, and scrapped, 1946
|-
|{{ship|Italian battleship|Giulio Cesare||2}}
|[[Julius Caesar]]<ref>Silverstone, p. 298</ref>
|[[Gio. Ansaldo & C.]], [[Genoa]]
|24 June 1910
|15 October 1911
|14 May 1915
|Transferred to the Soviet Union, 1949, and sank 29 October 1955 after hitting a [[naval mine|mine]], salvaged 1957, and subsequently scrapped
|-
|{{ship|Italian battleship|Leonardo da Vinci||2}}
|[[Leonardo da Vinci]]<ref>Silverstone, p. 300</ref>
|[[Cantieri navali Odero|Odero]], [[Genoa]]-[[Sestri Ponente]]
|18 July 1910
|14 October 1911
|17 May 1914
|Sunk by [[magazine (artillery)|magazine]] explosion, 2 August 1916, salvaged 1919, and sold for scrap, 22 March 1923<ref name=s7/>
|}

==Service==
[[File:Leonardodavinci.jpg|thumb|left|Postcard of ''Leonardo da Vinci'' in Taranto]]
''Conte di Cavour'' and ''Giulio Cesare'' served as [[flagship]]s in the southern [[Adriatic Sea]] during World War I,<ref name=s7/> but saw no action and spent little time at sea.<ref name=g7/> ''Leonardo da Vinci'' was also little used and was sunk by an internal [[magazine (artillery)|magazine]] explosion at [[Taranto]] harbor on the night of 2/3 August 1916 while loading ammunition. Casualties included 21 officers and 227 enlisted men<ref>Whitley, pp. 157–58</ref> killed.<ref name=s7/> The Italians blamed [[Austro-Hungarian]] [[saboteur]]s, but unstable propellant may well have been responsible.<ref name=hore175/> The ship was refloated, upside down, on 17 September 1919 and righted on 24 January 1921.<ref name=w8/> The ''Regia Marina'' planned to modernize her by replacing her center turret with six {{convert|102|mm|adj=on|0|sp=us}} AA guns,<ref name=randal259/> but lacked the funds to do so and sold her for scrap on 22 March 1923.<ref name=s7/>

In 1919, ''Conte di Cavour'' sailed to North America and visited ports in the United States as well as [[Halifax Regional Municipality|Halifax]], Canada. ''Giulio Cesare'' made port visits in the [[Levant]] in 1919 and 1920. ''Conte di Cavour'' was mostly inactive in 1921 because of personnel shortages and was refitted at La Spezia from November to March 1922. Both battleships supported Italian operations on [[Corfu]] in 1923 after an Italian general and his staff [[Corfu incident|were murdered on the Greco-Albanian border]]; [[Benito Mussolini]] was not satisfied with the Greek Government's response so he ordered Italian troops to occupy the island. ''Conte di Cavour'' bombarded the town with her 76&nbsp;mm guns,<ref name=w1/> killing 20 and wounding 32 civilians.<ref>{{cite news |url=http://nla.gov.au/nla.news-article54795961 |title=Bombardment of Corfu |newspaper=[[The Morning Bulletin]] |location=Rockhampton, Queensland, Australia |date=1 October 1935 |accessdate=16 March 2013 |page=6 |publisher=National Library of Australia}}</ref>

''Conte di Cavour'' escorted [[Victor Emmanuel III of Italy|King Victor Emmanuel III]] and his wife aboard ''Dante Alighieri'', on a state visit to Spain in 1924 and was placed in reserve upon her return until 1926, when she conveyed Mussolini on a voyage to Libya. The ship was again placed in reserve from 1927 until 1933. Her sister became a gunnery training ship in 1928, after having been in reserve since 1926. ''Conte di Cavour'' was reconstructed at the CRDA [[Trieste]] Yard while ''Giulio Cesare'' was rebuilt at [[Cantieri del Tirreno]], Genoa between 1933 and 1937. Both ships participated in a naval review by [[Adolf Hitler]] in the [[Bay of Naples]] in May 1938 and covered the [[invasion of Albania]] in May 1939.<ref name=w1>Whitley, pp. 158–61</ref>

[[File:Conti de Cavour5May38.jpg|right|thumb|''Conte di Cavour'' in Naples, 5 May 1938]]
Early in World War II, the sisters took part in the Battle of Calabria (also known as the Battle of Punta Stilo) on 9 July 1940, as part of the 1st Battle Squadron, commanded by Admiral [[Inigo Campioni]], during which they engaged major elements of the British [[Mediterranean Fleet]]. The British were escorting a convoy from Malta to [[Alexandria]], while the Italians had finished escorting another from [[Naples]] to [[Benghazi]], Libya. Admiral [[Andrew Cunningham, 1st Viscount Cunningham of Hyndhope|Andrew Cunningham]], commander of the Mediterranean Fleet, attempted to interpose his ships between the Italians and their base at Taranto. Crew on the fleets spotted each other in the middle of the afternoon and the Italian battleships opened fire at 15:53 at a range of nearly {{convert|29000|yd|disp=flip|sp=us}}. The two leading British battleships, {{HMS|Warspite|03|6}} and {{HMS|Malaya||2}}, replied a minute later. Three minutes after she opened fire, shells from ''Giulio Cesare'' began to straddle ''Warspite'' which made a small turn and increased speed, to throw off the Italian ship's aim, at 16:00. At that same time, a shell from ''Warspite'' struck ''Giulio Cesare'' at a distance of about {{convert|26000|yd|disp=flip|sp=us}}. The shell pierced the rear funnel and detonated inside it, blowing out a hole nearly {{convert|20|ft|m|disp=flip|sp=us}} across. Fragments started several fires and their smoke was drawn into the boiler rooms, forcing four boilers off-line as their operators could not breathe. This reduced the ship's speed to {{convert|18|kn}}. Uncertain how severe the damage was, Campioni ordered his battleships to turn away in the face of superior British numbers and they successfully disengaged.<ref>O'Hara, pp. 28–35</ref> Repairs to ''Giulio Cesare'' were completed by the end of August and both ships unsuccessfully attempted to intercept British convoys to [[Malta]] in August and September.<ref>Whitley, p. 161</ref>

On the night of 11 November 1940, ''Conte di Cavour'' and ''Giulio Cesare'' were at anchor in Taranto harbor when they were attacked by 21 [[Fairey Swordfish]] [[torpedo bomber]]s from the British [[aircraft carrier]] {{HMS|Illustrious|R87|6}}, along with several other warships. One torpedo exploded underneath 'B' turret at 23:15, and her captain requested tugboats to help [[Ship grounding|ground]] the ship on a nearby {{convert|12|m|ft|adj=on|sp=us}} [[sandbank]]. His admiral vetoed the request until it was too late and ''Conte di Cavour'' had to use a deeper, {{convert|17|m|ft|adj=on|sp=us}}, sandbank at 04:30 on 12 November. In an effort to lighten the ship, her guns and parts of her superstructure were removed and ''Conte di Cavour'' was refloated on 9 June 1941. Temporary repairs to enable the ship to reach Trieste for permanent repairs took until 22 December. Her guns were operable by September 1942, but replacing her entire electrical system took longer and she was still under repair when Italy surrendered a year later.<ref>Cernuschi & O'Hara, pp. 81–93</ref> The ''Regia Marina'' made plans to replace her secondary and anti-aircraft weapons with a dozen {{convert|135|mm|sp=us|adj=on|1}} [[dual-purpose gun]]s in twin mounts, twelve 64-caliber {{convert|65|mm|sp=us|adj=on|1}}, and twenty-three 65-caliber 20&nbsp;mm AA guns.<ref name="eb5"/> Her hulk was damaged in an air raid and capsized on 23 February 1945. Refloated shortly after the end of the war, ''Conte di Cavour'' was scrapped in 1946.<ref name=b9>Brescia, p. 59</ref>

[[File:AerialViewCavour.jpg|thumb|left|Aerial view of ''Conte di Cavour'' after her reconstruction]]
''Giulio Cesare'' participated in the [[Battle of Cape Spartivento]] on 27 November 1940, but never got close enough to any British ships to fire at them. The ship was damaged in January 1941 by a near miss during an air raid on Naples; repairs were completed in early February. She participated in the [[First Battle of Sirte]] on 17 December 1941, providing distant cover for a convoy bound for Libya, again never firing her main armament.<ref>Whitley, pp. 161–62</ref> In early 1942, ''Giulio Cesare'' was reduced to a training ship at Taranto and later [[Pula|Pola]].<ref name=b9/> She steamed to Malta in early September 1943 after the Italian surrender. The {{GS|U-596}} unsuccessfully attacked the ship in the [[Gulf of Taranto]] in early March 1944.<ref>Rohwer, pp. 272, 298</ref>

After the war, ''Giulio Cesare'' was allocated to the Soviet Union as [[Paris Peace Treaties, 1947|war reparations]] in 1949, and renamed ''[[Novorossiysk]]'', after the Soviet city on the [[Black Sea]]. The Soviets used her as a training ship when she was not undergoing one of her eight refits in their hands. In 1953, all remaining Italian light AA guns were replaced by eighteen [[37 mm automatic air defense gun M1939 (61-K)|37 mm 70-K]] AA guns in six twin mounts and six singles. They also replaced her fire-control systems and added [[radar]]s, although the exact changes are unknown. The Soviets intended to rearm her with their own [[Obukhovskii 12"/52 Pattern 1907 gun|305&nbsp;mm guns]], but this was forestalled by her loss. While at anchor in [[Sevastopol]] on the night of 28/29 October 1955, she detonated a large German [[naval mine|mine]] left over from World War II. The explosion blew a hole completely through the ship, making a {{convert|4|by|14|m|sp=us|adj=on}} hole in the forecastle forward of 'A' turret. The flooding could not be controlled and she later capsized with the loss of 608 men. ''Novorossiysk'' was stricken from the Navy List on 24 February 1956, salvaged on 4 May 1957, and subsequently scrapped.<ref>McLaughlin, pp. 419, 422–23</ref>

==See also==
{{Portal|Battleships}}
*[[List of ships of the Second World War]]
*[[List of ship classes of the Second World War]]

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{reflist|24em}}

==References==
* {{cite book|last1=Bagnasco|first1=Erminio|last2=Grossman|first2=Mark|title=Regia Marina: Italian Battleships of World War Two: A Pictorial History|year=1986|publisher=Pictorial Histories Publishing|location=Missoula, Montana|isbn=0-933126-75-1}}
* {{cite book|last1=Bargoni|first1=Franco|last2=Gay|first2=Franco|title=Corazzate classe Conte di Cavour|year=1972|publisher=Bizzarri|location=Roma|oclc=34904733}}
* {{cite book|last=Brescia|first=Maurizio|title=Mussolini's Navy: A Reference Guide to the Regia Marina 1930–45|year=2012|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=978-1-59114-544-8}}
* {{cite book|last=Campbell|first=John|title=Naval Weapons of World War II|year=1985|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-459-4}}
* {{cite book|last1=Cernuschi|first1=Ernesto|last2=O'Hara|first2=Vincent P.|chapter=Taranto: The Raid and the Aftermath|pages=77–95|editor=Jordan, John|publisher=Conway|location=London|year=2010|title=Warship 2010|isbn=978-1-84486-110-1}}
* {{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5}}
* {{cite book|last=Giorgerini|first=Giorgio|chapter=The Cavour & Duilio Class Battleships|pages=267–79|editor=Roberts, John|title=Warship IV|year=1980|publisher=Conway Maritime Press|location=London|isbn=0-85177-205-6}}
* {{cite book|last=Hore|first=Peter|title=Battleships|year=2005|publisher=Lorenz Books|location=London|isbn=0-7548-1407-6}}
* {{cite book|last=McLaughlin|first=Stephen|title=Russian & Soviet Battleships|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2003|isbn=1-55750-481-4}}
* {{cite book|last=O'Hara|first=Vincent P.|chapter=The Action off Calabria and the Myth of Moral Ascendancy|pages=26–39|editor=Jordan, John|publisher=Conway|location=London|year=2008|title=Warship 2008|isbn=978-1-84486-062-3}}
* {{cite book|last=Preston|first=Antony|title=Battleships of World War I: An Illustrated Encyclopedia of the Battleships of All Nations 1914–1918|publisher=Galahad Books|location=New York|year=1972|isbn=0-88365-300-1}}
* {{cite book|last=Rohwer|first=Jürgen|title=Chronology of the War at Sea 1939–1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2005|edition=Third Revised|isbn=1-59114-119-2}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
* {{cite book |last=Stille|first=Mark|title=Italian Battleships of World War II|year=2011|location=Oxford, UK|publisher=Osprey Publishing|isbn=978-1-84908-831-2}}
* {{cite book |last=Whitley|first=M. J.|title=Battleships of World War II|year=1998|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-55750-184-X}}

==Further reading==
* {{cite book|last=Fraccaroli|first=Aldo |title=Italian Warships of World War I|location=London|publisher=Ian Allan|year=1970|isbn=978-0-7110-0105-3}}

==External links==
{{Commons category|Conte di Cavour class battleship}}
* [http://digilander.libero.it/planciacomando/unita/cavour.htm Cavour class]&nbsp;– Plancia di Comando

{{Conte di Cavour-class battleship}}
{{WWI Italian ships}}
{{WWII Italian ships}}

{{DEFAULTSORT:Conte Di Cavour-class Battleship}}
[[Category:Battleship classes]]
[[Category:Conte di Cavour-class battleships]]
[[Category:World War II battleships of Italy]]