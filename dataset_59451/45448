{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=Brian Lane
|honorific_suffix=
|image=Brian Lane.jpg
|image_size=300
|caption=Brian Lane, Centre, Battle of Britain
|birth_date={{birth date|1917|6|18|df=yes}}
|birth_place=[[Harrogate]]
|death_date={{death date and age|1942|12|13|1917|6|18|df=yes}}
|death_place=[[Missing In Action]], [[North Sea]]
|nickname= Sandy
|birth_name=Brian John Edward Lane
|allegiance={{flag|United Kingdom}}
|serviceyears=1936&ndash;1942
|rank=[[Squadron Leader]]
|branch={{air force|United Kingdom}}
|commands=[[No. 19 Squadron RAF]]
|unit=
|battles=
{{plainlist|
*[[World War II]]
*:[[Battle of Britain]]
}}
|awards=[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
|laterwork=
}}

[[Squadron Leader]] '''Brian "Sandy" Lane''' [[Distinguished Flying Cross (United Kingdom)|DFC]] (18 June 1917 – 13 December 1942), was a [[United Kingdom|British]] [[Battle of Britain]] [[fighter pilot]] and author.

Born in [[Pannal]], [[Harrogate]], [[England]],<ref>[http://www.unithistories.com/officers/RAF_officers_L01.html]</ref> Lane grew up in [[Pinner]] <ref>[http://www.amazon.co.uk/dp/1860772870] A history of Pinner by Patricia A. Clarke - page 195</ref> before entering the RAF in 1936.<ref name="raf entry" /> He was the son of Henry Fitzgerald William Lane and Bessie Elinor Lane (née Hall). After leaving school, Lane worked as a factory supervisor.<ref>[http://www.telegraph.co.uk/news/newstopics/world-war-2/7621051/Heroes-of-the-Battle-of-Britain.html Heroes of the Battle of Britain website]</ref>

==Career==

===1936–1941===
[[File:Memorial Plaque to Sqn Ldr Brian Lane DFC.jpg|thumb|right|306px|Memorial Plaque to Sqn Ldr Brian Lane DFC.]]

Lane joined the Royal Air Force in 1936, with service number 37859.  After training at 3 E&RFTS Hamble in March 1936, he was posted to [[No. 11 Flying Training School]] at [[RAF Wittering]] on 1 June and was commissioned into the RAF General Duties Branch on 18 May 1936 on a short service commission.  On completion of his training joined [[No. 66 Squadron RAF]] at [[RAF Duxford]] on 8 January 1937 at the rank of Pilot Officer.  In June 1937 Lane moved to [[No. 213 Squadron RAF]] at [[RAF Northolt]]. He was promoted to Flying Officer on 23 December 1938.  Shortly before the outbreak of war, Lane joined [[No. 19 Squadron RAF]] at RAF Duxford as an Officer Commanding "A" Flight, flying [[Supermarine Spitfire|Spitfires]].

In June 1940, Lane married famous racing driver [[Eileen Ellison]] in Cambridge.<ref>http://www.kolumbus.fi/leif.snellman/de.htm</ref>

During the Dunkirk evacuation in May 1940 Lane was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC) for his bravery<ref>http://www.london-gazette.co.uk/issues/34910/pages/4675</ref> and his official rating as a fighter pilot classed ‘exceptional’. He became acting Squadron commander on 25 May 1940 when the incumbent CO was shot down over Dunkirk.<ref>http://duxford.iwm.org.uk/upload/pdf/Dux_Sept_Air_Show_Prog_2010.pdf</ref>

By September 1940, during the peak of the Battle of Britain, Lane’s abilities as a fighter pilot and leader were duly recognised and he was promoted to Squadron Leader.<ref>''Spitfire!'', pp 7-12, Foreword by Dilip Sarkar MBE FRHistS</ref>

Lane's [[No. 19 Squadron RAF]] often operated with [[No. 242 Squadron RAF]], and led by 242's Squadron Leader [[Douglas Bader]], the Squadrons often working together as part of the [[Duxford Wing]], 12 Group's controversial "[[Big Wing]]" formation. <ref>{{cite web|url=http://duxford.iwm.org.uk/server/show/conEvent.3639 |title=Archived copy |accessdate=2011-10-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20110914000525/http://duxford.iwm.org.uk/server/show/conEvent.3639 |archivedate=14 September 2011 |df=dmy }}</ref>

After the Battle of Britain, Lane continued flying with 19 Squadron until June 1941 when he was posted to the [[No. 12 Group RAF]] staff at [[RAF Hucknall]].<ref name="Aces High' page 386">'Aces High', Shores & Williams, biog on page 386</ref>

===1941–1942===
In November 1941 Lane was posted on a staff appointment to the Middle East.<ref>http://www.london-gazette.co.uk/issues/35383/supplements/7111</ref> In June 1942 Lane returned to England to command No. 61 OTU at [[Mountford Bridge]], until December 1942 when he joined [[No. 167 Squadron RAF]] at [[RAF Ludham]] as a [[wikt:supernumerary|supernumerary]] [[Squadron Leader]] flying the Spitfire Mk. V.<ref>http://www.london-gazette.co.uk/issues/35727/supplements/4276</ref>

He made his first operational flight with the Squadron on 13 December 1942, during which he was last seen giving chase to two Focke-Wulf 190 fighters. He never returned from this mission and was listed as "[[Missing in Action]]".  Lane has no known grave having most likely been shot down over the North Sea.<ref>http://www.dailymail.co.uk/news/article-1267916/Unseen-images-Battle-Britain-heroes-duty.html</ref> It is probable he was a victim of Oblt. Leonhardt of 6./[[Jagdgeschwader 1 (World War II)|JG 1]] and crashed into the sea 30&nbsp;km west of Schouwen {{coord|51.618|3.455}} <ref>[http://www.wingstovictory.nl/database/database_detail-en.php?wtv_id=216 Wings to Victory crash site database]</ref> at 16:34 hrs.<ref name="Aces High' page 386"/>

During Lane’s operational career he claimed 6 (and 1 shared) enemy aircraft shot down, 2 unconfirmed destroyed, 1 probable destroyed and 1 damaged.<ref name="Aces High' page 386"/><ref>http://www.the-battle-of-britain.co.uk/pilots/La-pilots.htm</ref><ref>http://www.acesofww2.com/UK/UK.htm</ref>

==Published work==
Lane was the author of ''Spitfire!'', originally published in 1942 under the pseudonym B.J. Ellan. The book is a first hand account of his experiences as a front line Spitfire pilot and is one of only a few contemporaneous autobiographical accounts of the life of a Battle of Britain Spitfire pilot.<ref>http://www.amazon.co.uk/dp/1848683545</ref>

Historian Dilip Sarkar spent many years editing and researching the original work to replace the code words used by the war time censor with the correct names of people and places. The revised book was republished in 2009 and again in 2011.

==Interviews==
This is what some of Lane’s fellow 19 Squadron pilots and crew thought of him:<ref>''Spitfire!'', pp 7-12, Foreword by Dilip Sarkar</ref>

[[George Unwin|Flight Sergeant George "Grumpy" Unwin DFM]]: "He was completely unflappable, no matter what the odds, his voice always calm and reassuring, issuing orders which always seemed to be the right decisions."

Sergeant David Cox: "Quite simply Brian Lane was the best CO [Commanding Officer] I ever served under, in every respect, and when my turn came to lead, I modelled myself on him."

Corporal Fred Roberts: "...he was an absolutely wonderful man. Early on in the war, some of the officers could still be a bit snobbish, but not Brian Lane, who knew everyone in his command, no matter how lowly their rank or status, by their first names."

==Memorial plaque==
A permanent memorial plaque, organised by local resident Paul Baderman, was unveiled on Lane's former home in [[Pinner]], [[London]] on 25 September 2011, 69 years after his death.<ref>{{cite web|url=http://www.pinnerassociation.co.uk/content/?p%3D1034 |title=Archived copy |accessdate=2011-10-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20111022153309/http://www.pinnerassociation.co.uk:80/content/?p=1034 |archivedate=22 October 2011 |df=dmy }}</ref> A crowd of about 400 people attended the short ceremony which saw guests of honour Flt Lt K A Wilkinson RAF and Mr John Milne unveil the plaque. Flt Lt Wilkinson flew under Lane's command in 19 Squadron and Milne was Lane's Rigger, responsible for refuelling Lane's Spitfire and the repair of its airframe.<ref>http://www.times-series.co.uk/news/9241005.Event_to_remember_war_veteran_with_flypast_to_be_held_this_month/</ref>

==References==
{{reflist|refs=
<ref name="raf entry">{{London Gazette|issue=34290 |date=2 June 1936 |startpage=3526 |supp= |accessdate=16 August 2011}}</ref>
}}

==External links==
{{commons category|Brian Lane (RAF officer)}}
* [http://politicsworldwide.com/journal/the-face-of-the-battle-of-britain/ Photograph of Lane]

{{coord|51.618|03.455|type:event_globe:earth_region:NL|display=title}}

{{DEFAULTSORT:Lane, Brian}}
[[Category:1917 births]]
[[Category:1942 deaths]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:English biographers]]
[[Category:English non-fiction writers]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:The Few]]
[[Category:British World War II flying aces]]
[[Category:Missing in action of World War II]]
[[Category:Aerial disappearances of military personnel in action]]
[[Category:20th-century biographers]]
[[Category:English male writers]]