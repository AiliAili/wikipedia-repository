{{For|others of the same or similar names|Allison Taylor (The Simpsons)|Alison Taylor (disambiguation)}}
{{Infobox character
|series=[[24 (TV series)|24]]
| image        = [[Image:Allison Taylor.jpg|200px]]
| caption      = [[Cherry Jones]] as Allison Taylor
| name         = Allison Taylor
| portrayer    = [[Cherry Jones]]
|lbl1=Days
|first=[[24: Redemption]]
|last=[[24 (season 8)|Day 8 – Episode 24]]
|data1=[[24 (season 7)|7]], [[24 (season 8)|8]]
|lbl2=Other Appearances
|data2=[[24: Redemption]]
|spouse = Henry Taylor (Divorced)}}
'''Allison Taylor''' is a [[fictional character]] portrayed by [[Cherry Jones]] on the TV series ''[[24 (TV series)|24]]''.  The first female [[President of the United States]] within the ''24'' universe and a [[Republican Party (United States)|Republican]], she took office in the TV movie, ''[[24: Redemption]]'' and served in [[24 (season 7)|Season 7]] and [[24 (season 8)|Season 8]]. She is said to be based on [[Hillary Clinton]].

==Concept and creation==
[[Cherry Jones]] was the first major casting addition to the seventh season.<ref>{{cite news|url=http://www.reuters.com/article/entertainmentNews/idUSN2027850820070721?feedType=RSS&rpc=22&sp=true|title=Jones moves into 24 Oval Office|publisher=Reuters|author=Nellie Andreeva|accessdate=2008-07-26 | date=2007-07-21}}</ref> Jones was cast as Taylor when the program's producers contacted her agent; she had no prior experience of ''24'', as she said in an interview, "It was the last thing in the world [she] saw coming".<ref>[http://www.newsweek.com/id/178860/page/1 Hillary Will Be Soooo Jealous]</ref>

"It’s time to go back to an idealized president—a well-articulated and idealized president," said [[Howard Gordon]].<ref name="iFMagazineHG">{{cite web|author=Anthony C. Ferrante |publisher=iFMagazine.com |date=2007-07-24 |accessdate=2007-07-24 |title=Exclusive Interview: Howard Gordon Gives the Early Scoop on '24' - Season 7: Version 3.0 |url=http://www.ifmagazine.com/feature.asp?article=2257 |archiveurl=https://web.archive.org/web/20070911120151/http://www.ifmagazine.com/feature.asp?article=2257 |archivedate=2007-09-11 |deadurl=no |df= }}</ref> "[Her] greatest flaw is her [[idealism]]. And I think that's a fun part to play. What happens when somebody's idealism butts up against the [[realpolitik]] of the world? But she's a very human character, and I think a very strong president and aspirational in the kind of way [[David Palmer (24 character)|David Palmer]] was."<ref name="orlandosentinel715">{{cite web| author=Hal Boedeker|publisher=[[Orlando Sentinel]]|date=2008-07-15|accessdate=2008-07-26|title=United States to get idealistic woman president -- on "24"|url=http://blogs.orlandosentinel.com/entertainment_tv_tvblog/2008/07/united-states-t.html | archiveurl= https://web.archive.org/web/20080802235535/http://blogs.orlandosentinel.com/entertainment_tv_tvblog/2008/07/united-states-t.html| archivedate= 2 August 2008 <!--DASHBot-->| deadurl= no}}</ref>  "It is an heroic administration. Cherry Jones plays the president brilliantly," he continued.<ref name="orlandosentinel715" />

Cherry Jones described her character as nothing like [[Hillary Clinton]], and instead described her as a combination of [[Lyndon Johnson]],<ref name="startelegram">{{cite web|url=http://www.star-telegram.com/living/story/1215599.html|title=A conversation with Cherry Jones of '24'|last=Martindale|first=David|date=February 20, 2009|publisher=The Star Telegram|accessdate=2009-03-29}}{{dead link|date=September 2016|bot=medic}}{{cbignore|bot=medic}}</ref> [[Eleanor Roosevelt]], [[Golda Meir]], and [[John Wayne]], however she would have similar opinions to [[Barack Obama]].<ref>[http://nymag.com/daily/entertainment/2008/11/cherry_jones.html ‘24’ Star Cherry Jones on Playing the President and What She Thinks of Meryl Streep in ‘Doubt’]</ref> Jones said of her portrayal of Taylor as making her look exhausted, commenting that the job of President has a way of sucking the life out of people.<ref name="startelegram"/> She hopes that her portrayal of Taylor might convince the American public that a woman can be president,<ref>[http://www.detnews.com/apps/pbcs.dll/article?AID=/20081120/OPINION03/811200305 Cherry Jones is model president]</ref> similar to the way that Dennis Haysbert's portrayal of David Palmer [[Bradley effect#2008 United States presidential election|apparently helped]] Obama's election.<ref>[http://news.bbc.co.uk/2/hi/americas/7717578.stm The US election in figures]</ref>

Jones has said that Taylor is "an honorable, courageous, upright kind of gal [and] very much her own person. [...] The president makes a very bold move, which then sets off the cataclysmic events" that the US has yet to experience.<ref>{{cite web|url=http://nymag.com/daily/entertainment/2007/11/24s_new_president_cherry_jones.html|title=New ‘24’ President Cherry Jones Almost Spoils Upcoming Season|last=Murphy|first=Tim|date=11 July 2007|publisher=New York Entertainment|accessdate=2008-07-27}}</ref>

==Characterization==
Prior to her election as president, Taylor was a U.S. Senator, and defeated the incumbent President [[Noah Daniels]] in the election. Ironically, given Cherry Jones's characterization of President Taylor, that would make her a Republican, since Daniels was vice-president in the administration of Democrat [[Wayne Palmer]].

==Appearances==
<!-- This is not an episode guide, please see the 24 project's manual of style for guidelines -->

===Redemption===
Taylor is shown preparing for her Inauguration as the President and preparing to take office after [[Noah Daniels]].

===Season 7===
Shortly after Taylor's inauguration, her son, Roger, died under mysterious circumstances.  A thorough investigation concluded that Roger, who worked in the private financial sector, committed suicide.  She was later informed that Roger had been fingered by the federal government for insider trading, and killed himself shortly before being indicted.  According to her chief of staff, a political ally of the President covered it up.  While Taylor was able to cope with Roger's death and effectively continued her executive duties, the First Gentleman, Henry Taylor, did not take Roger's death very well, and struggled mightily with its aftermath.

On Day 7, Taylor is preparing to send U.S. troops into Sangala to stop a genocidal campaign by the regime of General Benjamin Juma. During Day 7 as the US is threatened by Iké Dubaku, one of Juma's Colonels, she stands her ground throughout and refuses to withdraw US forces from Sangala despite pressure from her advisers and even after Dubaku uses the CIP device to crash two airliners into each other, inflicting hundreds of casualties. Furthermore, still after her husband Henry Taylor was kidnapped and threatened with execution by Dubaku's men, Taylor refused to order US forces to withdraw from Sangala.

Later after the threat from Dubaku had subsided, General Benjamin Juma lead a team of commandos to attack the White House, taking Taylor and many other White House personnel hostage. Taylor is forced under duress to deliver a live speech via the internet apologising for the US invasion of Sangala, during which Bill Buchanan sacrifices his life as part of the rescue operation which eventually ends the hostage situation against Taylor and the White House.

Taylor later offers her previously estranged daughter, Olivia, a position on her administration as an adviser much to the dismay of Ethan Kanin, Taylor's Chief of Staff. As the events of the day unfold, Ethan Kanin is forced to resign after Jack Bauer is framed for the murders of Senator Mayer and Ryan Burnett, one of the US-based co-conspirators as Kanin had authorized Bauer to be freed from custody to continue the investigation. As a result, President Taylor makes Olivia Taylor her acting Chief of Staff.

She orders an airstrike on the Starkwood compound after Tony Almeida locates the bioweapon canisters, but is forced to abort when Hodges discovers the strike and threatens to deploy the missiles. She indirectly authorizes a deniable covert operation however, allowing Tony to destroy the canisters before the missiles could be launched.

After the Starkwood threat is averted and Jonas Hodges was taken into custody, she decides to offer Hodges an immunity and witness protection deal in exchange for information about the other co-conspirators. This leads to her daughter, the Acting Chief of Staff, Olivia, to order Hodges' assassination as revenge for Roger's death. This is later discovered and President Taylor is faced with the dilemma of either covering Olivia's crime up or turning her in to the authorities, decides the latter, stating that she swore an oath when she became President of The United States to uphold the constitution. This leads to her husband filing for a divorce, and in Season 8 she is no longer married.

===Season 8===
President Taylor is in New York at the UN to sign a peace treaty with President [[Omar Hassan (24 character)|Omar Hassan]] of the Islamic Republic of Kamistan. During the turmoil that would visit the day, she built a great rapport and deep trust and friendship with President Omar Hassan and his family.   Following Hassan's assassination by IRK extremists who threatened New York with a dirty bomb and the assassination of President Omar Hassan, President Taylor called upon former president [[Charles Logan (24 character)|Charles Logan]] to assist in keeping the negotiations on-track. She is persuaded by Logan to cover-up Russian involvement in the day's events, and to lock-down [[Jack Bauer]] to stop him revealing it; when Bauer escapes all law-enforcement agencies are given "shoot to kill" orders in Taylor's name via President Charles Logan. Also at Logan's behest, she allows CTU mole [[Minor government agents in 24#Dana Walsh|Dana Walsh]] to be secretly tortured for the location of an incriminating data file she made and arrests journalist Meredith Reed to stop her publishing the story. When new IRK president Dalia Hassan learns of the cover-up she threatens to withdraw from the negotiations. Taylor responds by threatening to declare war and use the full might of the US military (strongly implied to include nuclear retaliation) on the country if the treaty is not signed.

During the season finale, Taylor has a moral epiphany and refuses to sign the peace treaty at the last moment as the last signee. She reveals the cover-up and the lengths many have gone to maintain it that day. She realizes that she betrayed her own moral fiber and guiding light. As the season progressed she insulated herself from others&nbsp;– much like is seen with Charles Logan before his fall from the Presidency. Interestingly, he is also the "poison" that fatally sickens the Taylor administration. Citing her role in the cover-up, Taylor also plans to resign from office, surrender herself to the Attorney General and face whatever consequences there are for her actions. She also cancels Logan's order for Bauer to be murdered, but advises him to flee the country as both Americans and Russians will be after him for his actions. Taylor and Chloe both promise to buy him as much time as they can, but neither are sure how long he will have.

It is believed that Taylor's vice president Mitchell Hayworth concluded her term after her resignation.

==Critical reception==
Ken Tucker describes the character  played by Cherry Jones as "[[Hillary Clinton]] if there was no [[Barack Obama|Obama]]" and adds that this "fresh character...mingles nicely with familiar faces".<ref>Ken Tucker, “''24'': Mondays, 9 p.m., premiering Sunday, Jan. 11, at 8 p.m.,” ''Entertainment Weekly'' 1030 (January 16, 2009): 56.</ref> For her role as Allison Taylor, Cherry Jones won the [[61st Primetime Emmy Award]] for [[Primetime Emmy Award for Outstanding Supporting Actress - Drama Series|Outstanding Supporting Actress in a Drama Series]], the series' only award win that year.<ref>{{cite press |url=http://www.emmys.tv/sites/emmys.tv/files/ptemmys09winners_pressrel.pdf |title=61st Primetime Emmy Winners |publisher=emmy.tv| page=6 |date=September 20, 2009 |accessdate=2010-10-10}}</ref>

==References==
{{reflist|2}}
{{24 (TV series)}}

{{DEFAULTSORT:Taylor, Allison}}
[[Category:24 (TV series) characters]]
[[Category:Fictional Presidents of the United States]]
[[Category:Fictional Republicans (United States)]]
[[Category:Fictional United States Senators]]