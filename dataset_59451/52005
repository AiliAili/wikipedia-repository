{{Infobox journal
| title = Journal of Personality and Social Psychology
| cover = [[File:Journal of Personality and Social Psychology cover.gif]]
| editor = Eliot R. Smith, M. Lynne Cooper, Kerry Kawakami
| discipline = [[Personality psychology]], [[social psychology]]
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Monthly
| history = 1965-present
| impact = 4.736
| impact-year = 2015
| website = http://www.apa.org/pubs/journals/psp/
| link1 = http://content.apa.org/journals/psp
| link1-name = Online access
| ISSN = 0022-3514
| eISSN = 
| OCLC = 1783133
| LCCN = 65009855
| CODEN = JPSPB2
}}
The '''''Journal of Personality and Social Psychology''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[American Psychological Association]] that was established in 1965. It covers the fields of [[social psychology (psychology)|social]] and [[personality psychology]]. The [[editors-in-chief]] are Eliot R. Smith ([[Indiana University]]; ''Attitudes and Social Cognition Section''), Kerry Kawakami ([[York University]]); ''Interpersonal Relations and Group Processes Section''), and M. Lynne Cooper ([[University of Missouri]]; ''Personality Processes and Individual Differences Section'').

== Contents ==
The journal's focus is on empirical research reports; however, specialized theoretical, methodological, and review papers are also published. Articles typically involve a lengthy introduction and literature review, followed by several related studies that explore different aspects of a theory or test multiple competing hypotheses. Some researchers see the multiple-experiments requirement as an excessive burden that delays the publication of valuable work,<ref>{{cite journal |doi=10.1177/0146167292184017 |url=http://www.wjh.harvard.edu/~wegner/pdfs/Wegner%20Premature%20Demise%201992.pdf |title=The Premature Demise of the Solo Experiment |year=1992 |last1=Wegner |first1=D. M. |journal=Personality and Social Psychology Bulletin |volume=18 |issue=4 |pages=504}}</ref> but this requirement also helps maintain the impression that research that is published in JPSP has been thoroughly vetted and is less likely to be the result of a [[type I error]] or an unexplored [[confound]].{{cn|date=December 2013}}

The journal is divided into three independently edited sections. ''Attitudes and Social Cognition'' addresses those domains of social behavior in which cognition plays a major role, including the interface of cognition with overt behavior, affect, and motivation. ''Interpersonal Relations and Group Processes'' focuses on psychological and structural features of interaction in dyads and groups. ''Personality Processes and Individual Differences'' publishes research on all aspects of personality psychology. It includes studies of individual differences and basic processes in behavior, emotions, coping, health, motivation, and other phenomena that reflect personality.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic ASAP]]
* [[Academic OneFile]]
* [[Academic Search Premier]]
* [[AgeLine]]
* [[Bibliography of Asian Studies]]
* [[Current Contents]]
* [[Current Index to Statistics]]
* [[Dietrich's Index Philosophicus]]
* [[FRANCIS]]
* [[Family Index]]
* [[Family Studies Abstracts]]
* [[Higher Education Abstracts]]
* [[InfoTrac]]
* [[International Bibliography of the Social Sciences]]
* [[MLA International Bibliography]]
* [[MEDLINE]]/[[PubMed]]
* [[Peace Research Abstracts Journal]]
* [[ProQuest]]
* [[PsycINFO]]
* [[Race Relations Abstracts]]
* [[Religion Index One]]
* [[Russian Academy of Sciences Bibliographies]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[Social Services Abstracts]]
* [[Social Work Abstracts]]
* [[SocINDEX]]
* [[Sociological Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.736, ranking it 3rd out of 62 journals in the category "Psychology, Social".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Psychology, Social |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== In popular culture ==
Non-fiction author [[Malcolm Gladwell]] writes frequently about findings that are reported in the journal.{{cn|date=December 2013}} Gladwell, upon being asked where he would like to be buried, replied "I'd like to be buried in the current-periodicals room, maybe next to the unbound volumes of the ''Journal of Personality and Social Psychology'' (my favorite journal)."<ref>[https://books.google.com/books?id=H6m7C_HLx7IC&lpg=PP1&pg=PA141#v=onepage&q&f=false Doonan, S. (2008). ''Eccentric Glamour: Creating an Insanely More Fabulous You''. Simon & Schuster: New York, NY.]</ref><ref>[http://www.nypost.com/p/pagesix/item_8dxpsptyL3YS0M72gl7udP Creative casting for barneys] ''[[New York Post]]'', March 24, 2008.</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/psp/}}

[[Category:Social psychology journals]]
[[Category:Personality journals]]
[[Category:Publications established in 1965]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:American Psychological Association academic journals]]
[[Category:Differential psychology journals]]