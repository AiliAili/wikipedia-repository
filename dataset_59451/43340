{{Redirect|P-89|the P89 semi-automatic pistol|Ruger P-Series}}
{{Use dmy dates|date=October 2013}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name= F-89 Scorpion
  |image= File:59fis-f-89-goosebay.jpg
  |caption=Formation of three F-89Ds of the 59th Fighter Squadron, Goose Bay, Labrador
}}{{Infobox Aircraft Type
  |type=[[Interceptor aircraft|Interceptor]]
  |manufacturer=[[Northrop Corporation]]
  |designer=[[Jack Northrop]]
  |first flight= 16 August 1948
  |introduced= September 1950
  |retired= 1969
  |status=
  |primary user= [[United States Air Force]]
  |more users=
  |produced=
  |number built=1,050 and 2 [[prototype]]s
  |unit cost=US$801,602 (F-89D)<ref name="Knaack p93">Knaack 1978, p. 93.</ref><br />US$988,884 (F-89H)<ref name="knaack p96">Knaack 1978, p. 96.</ref>
  |variants with their own articles=
}}
|}

The '''Northrop F-89 Scorpion''' was an American all-weather [[interceptor aircraft|interceptor]] built during the 1950s, the first [[turbojet|jet-powered]] aircraft designed as such from the outset to enter service.<ref>Kinsey 1992, p. 3</ref> Though its straight wings limited its performance, it was among the first [[United States Air Force]] (USAF) jet [[fighter aircraft|fighters]] equipped with [[guided missile]]s and notably the first combat aircraft armed with air-to-air [[nuclear weapon]]s (the unguided [[AIR-2 Genie|Genie]] rocket).

==Design and development==
The Scorpion stemmed from a [[United States Army Air Forces]] (USAAF) Air Technical Service Command specification ("Military Characteristics for All-Weather Fighting Aircraft") for a [[night fighter]] to replace the [[Northrop P-61 Black Widow|P-61 Black Widow]]. The preliminary specification, sent to aircraft manufacturers on 28 August 1945, required two engines and an armament of six guns, either .60-caliber (15&nbsp;mm) machine guns or {{convert|20|mm|adj=on|sp=us}} [[autocannon]]. The revised specification was issued on 23 November; it did not specify jet propulsion, but the desired maximum speed of {{convert|530|mph}} virtually dictated that all the submissions would be jet-powered. The aircraft was to be armed with aerial rockets stored internally and six guns split between two flexible mounts, four guns forward and two in the rear. Each mount had to be capable of 15° of movement from the longitudinal axis of the aircraft. Each mount's guns were to be automatically controlled by radar. For ground attack, it had to be capable of carrying {{convert|1000|lb|adj=on}} bombs and to be able to carry a minimum of eight rockets externally.<ref>Blazer and Dorio 1993, pp. 1–3.</ref> [[Bell Aircraft]], [[Consolidated-Vultee]], [[Douglas Aircraft]], [[Goodyear Tire and Rubber Company|Goodyear]], [[Northrop Corporation|Northrop]] and [[Curtiss-Wright]] all submitted proposals. In March 1946, the USAAF selected the [[Curtiss-Wright XF-87 Blackhawk|Curtiss-Wright XP-87]], adapted from their proposed [[Curtiss-Wright XA-43|XA-43]] [[attack aircraft]], and Northrop's N-24 design, one of four submitted by the company.<ref>''Air International'' July 1988, pp. 44–45.</ref>

The N-24, designed by [[Jack Northrop]], was a slim-bodied swept-wing aircraft with a two-man pressurized [[cockpit]] and [[conventional landing gear]].<ref name=a5>''Air International'' July 1988, p. 45.</ref> To reduce [[drag (physics)|drag]], the two [[Allison J35]] [[turbojet]] engines were buried in the lower [[fuselage]], directly behind their air intakes, and they exhausted underneath the rear fuselage. The [[horizontal stabilizer]] was mounted just above the junction of the vertical stabilizer with the fuselage and had some [[dihedral (aircraft)|dihedral]].<ref>Isham and McLaren, p. 9.</ref>

A contract for two aircraft, now designated the XP-89, and a full-scale mock-up was approved on 13 June, although construction of the mock-up had begun immediately after the USAAF announced that the N-24 had been selected. It was inspected on 25 September, and the USAAF was not impressed. The inspectors believed that the radar operator needed to be moved forward, closer to the pilot, with both crewmen under a single [[Aircraft canopy|canopy]], the magnesium components of the wing replaced by aluminum, and the fuel stowage directly above the engines moved. Other changes had to be made as wind tunnel and other aerodynamic tests were conducted. The swept wings proved to be less satisfactory at low speeds, and a thin straight wing was selected instead. Delivery of the first prototype was scheduled for November 1947, 14 months after the inspection.<ref>Blazer and Dorio 1993, pp. 5–7, 9.</ref> The position of the horizontal stabilizer proved to be unsatisfactory, as it was affected by the engine exhaust, and it would be "blanked-out" by airflow from the wing at high [[angle of attack|angles of attack]]. It was moved halfway up the tail, but its position flush with the leading edge of the vertical stabilizer proved to cause extra drag through turbulence and reduced the effectiveness of the [[elevator (aircraft)|elevators]] and [[rudder]]. Moving the horizontal stabilizer forward solved the problem.<ref>Isham and McLaren, pp. 9–10.</ref> Another major change occurred when USAAF revised its specification to delete the rear gun installation on 8 October. Another inspection of the mock-up was held on 17 December, and the inspectors only suggested minor changes, even though the fuselage fuel tanks were still above the engines. Northrop's efforts to protect the fuel tanks were considered sufficient, as the only alternative was to redesign the entire aircraft.<ref>Blazer and Dorio 1993, pp. 6–8.</ref>

The XP-89 had a thin, straight, mid-mounted wing and a crew of two, seated in tandem. The slim rear fuselage and the high-mounted horizontal stabilizer led Northrop employees calling it the Scorpion—a name later formally adopted by the Air Force.<ref name="a5"/> The intended armament of four 20&nbsp;mm [[Hispano-Suiza HS.404|M-24]] cannon in a small nose turret was not ready when the XP-89 was completed in 1948.<ref name= "Davis p. 5">Davis and Menard 1990, p. 5.</ref> Pending the availability of either of the two turrets under development, an interim six-gun fixed installation, with 200 rounds per gun, was designed for the underside of the nose. The thin wing had a [[Aspect ratio (wing)|aspect ratio]] of 9% and used a NACA 0009-64 section, which was selected for its low drag at high speed and stability at low speeds. A further advantage of the straight wing was that it could accommodate heavy weights at the wingtips.<ref>''Air International'' July 1988, pp. 45–46.</ref> The wing could not fit the circular-type ailerons used in the P-61, so Northrop used the "[[deceleron]]s" designed for the unsuccessful [[Northrop XP-79|XP-79]] [[prototype]]. These were clamshell-style split [[aileron]]s, which could be used as conventional ailerons, as [[dive brake]]s, or function as [[Flap (aircraft)|flaps]] as needed.<ref name= "Davis p. 4">Davis and Menard 1990, p. 4.</ref> All flying surfaces, the flaps and the landing gear were hydraulically powered. The thin wing dictated tall, thin, high-pressure ({{convert|200|psi|kPa kg/cm2|0|abbr=on|lk=on}}) mainwheel tires, while the low height of the fuselage required the use of dual wheels for the nose gear.<ref name="AI Jul p46"/>

The terms of the initial contract were revised and formalized on 21 May 1947 with the price increased to $5,571,111. The delivery date of the first aircraft was scheduled 14 months (July 1948) from signing and the second 2 months after that. A month before the prototype made its first flight on 16 August 1948 at [[Muroc Army Air Field]], the USAF changed its [[1924 United States Army Air Service aircraft designation system|designation]] for fighter aircraft from "P" to "F".<ref>Blazer and Dorio 1993, pp. 9–10.</ref> The XF-89 was fitted with 4,000&nbsp;lbf (17.8&nbsp;kN) J-35-A-9 turbojets and proved to be seriously underpowered. Initial flights were made with conventional ailerons, decelerons not being installed until December.<ref name="AI Jul p46">''Air International'' July 1988, p. 46.</ref>

Several months earlier the Air Force conducted a competitive evaluation of the three existing all-weather interceptor prototypes, the XF-87, the XF-89, and the [[United States Navy|US Navy's]] [[Douglas F3D Skyknight|XF3D Skyknight]]. The evaluators were qualified night-fighter pilots, radar operators, and experienced maintenance [[non-commissioned officer]]s. The pilots were not impressed with any of the aircraft and recommended procurement of an interim aircraft that resulted in the development of the [[Lockheed F-94 Starfire]] from the training version of the [[Lockheed F-80 Shooting Star]]. The F-89 proved to be the fastest of the three contenders,<ref name=b12>Blazer and Dorio 1993, p. 12.</ref> although it was in last place in cockpit arrangement and ease of maintenance.<ref name= "Davis p. 5"/> One pilot claimed that the XF-89 was the only real fighter and compared the XF-87 to a medium [[bomber]] and the XF3D to a [[trainer (aircraft)|trainer]].<ref name=b12/> The full Committee on Evaluation overruled those evaluators, preferring the Douglas design and selecting the XF-89, as it had the greatest potential for development. The Air Force subsequently canceled the production contract for the F-87 to free up money for the Scorpion.<ref name="AI Jul p46"/>

By November 1949 the second aircraft was virtually complete, but the Air Force was concerned about the design's poor thrust-to-weight ratio and decided to implement a weight-reduction program, as well as upgrading the engines to the more powerful J-33-A-21 fitted with an [[afterburner (engine)|afterburner]]. Other major changes included the replacement of the nose gun turret by the [[Hughes Aircraft Company|Hughes]]-designed six-gun nose, AN/ARG-33 radar, and E-1 [[fire-control system]], permanent wing-tip fuel tanks, and the ability to lower the complete engine for better maintenance access. The new nose added {{convert|3|ft|m}} to the length of the aircraft. It was redesignated YF-89A to better reflect its role as a pre-production testbed to evaluate equipment and changes planned for the F-89A production aircraft. The aircraft was essentially complete by February 1950.<ref>Blazer and Dorio 1993, p. 16.</ref>

After repairs from a [[crash landing]] on 27 June 1949, the XF-89 was flown to [[March AFB]] to participate in the [[RKO]] movie [[Jet Pilot (film)|''Jet Pilot'']] in February 1950. Shortly afterward, the aircraft crashed on 22 February, killing the observer, when [[Aeroelastic flutter|flutter]] developed in the elevator and the subsequent vibrations caused the entire tail to break off. Construction of the production models was suspended until the reasons for the accident were discovered. Engineering and wind-tunnel tests revealed that the geometry of the rear fuselage and the engine exhaust created flutter-inducing turbulence that was aggravated by the high-frequency acoustic energy from the exhaust. Fixes for the problem involved the addition of a "jet wake fairing" at the bottom rear of the fuselage between the engines, external ("ice tong") mass balances for the elevator, pending the design of internal mass balances,<ref>Blazer and Dorio 1993, pp. 15–16, 19.</ref> and the addition of exhaust deflectors to the fuselage to reduce the turbulence and the consequent flutter.<ref>Davis and Menard 1990, p. 7.</ref>

Well before the YF-89A was complete, a $39,011,622 contract was awarded to Northrop on 13 May 1949 for 48 F-89A aircraft, one static test airframe and the modifications made to the YF-89A.<ref>Blazer and Dorio 1993, p. 25.</ref>

==Operational history==
[[File:Northrop F-89A Scorpion in flight.jpg|thumb|An early F-89A]]
Production was authorized in January 1949,<ref name="Knaack p85">Knaack 1978, p. 85.</ref> with the first production F-89A flying in September 1950. It had AN/APG-33 radar and an armament of six 20&nbsp;mm (.79&nbsp;in) [[Hispano-Suiza HS.404|T-31]] cannons with 200 rpg. The swiveling nose turret was abandoned, and 300&nbsp;US gal (1,100&nbsp;l) fuel tanks were permanently fitted to the wingtips. Underwing racks could carry 16 5&nbsp;in (127&nbsp;mm) aerial rockets or up to 3,200&nbsp;lb (1,455&nbsp;kg) of bombs.<ref name="AI Jul p47-8"/>

Only 18 F-89As were completed, which were mainly used for tests and trials, before the type was upgraded to '''F-89B''' standard, with new avionics.<ref name="AI Jul p47-8">''Air International'' July 1988, pp. 47–48.</ref> The type entered service with the [[84th Flying Training Squadron|84th Fighter-Interceptor Squadron]] in June 1951.<ref name="Knaack p87">Knaack 1978, p. 87.</ref> These had considerable problems with engines and other systems, and soon gave way to the '''F-89C'''. Despite repeated engine changes, problems persisted, compounded by the discovery of structural problems with the wings that led to the grounding of the F-89 and forced a refit of 194 -A, -B, and -C models.<ref name="Knaack p88-9">Knaack 1978, pp. 88–89.</ref>

The major production model was the '''F-89D''', which first flew 23 October 1951 and entered service in 1954. It removed the cannon in favor of a new Hughes E-6 fire control system with AN/APG-40 radar and an AN/APA-84 computer. Armament was two pods of 52 2.75&nbsp;in (70&nbsp;mm) "Mighty Mouse" [[Mk 4/Mk 40 Folding-Fin Aerial Rocket|FFAR]] rockets, for a total of 104.<ref name="AI Aug p88-9">''Air International'' August 1988, pp. 88–89.</ref> A total of 682 were built.<ref name="Knaack p93"/> In August 1956 a pair of F-89D interceptors were scrambled from [[Oxnard Air Force Base]] to shoot down a runaway [[Grumman F6F Hellcat|F6F-5K drone]] leading to [[The Battle of Palmdale]] incident.

Proposed re-engined F-89s, designated '''F-89E''' and '''F-89F''', were not built, nor was a proposed '''F-89G''' that would have used Hughes MA-1 fire control and [[AIM-4 Falcon|GAR-1/GAR-2 Falcon]] [[air-to-air missile]]s like the [[Convair F-106 Delta Dart]].

[[File:Northrop F-89H with AIM-4 Falcon missiles.jpg|thumb|right|F-89H showing its GAR-1/2 Falcon missiles extended from the wingtip pods]]
The subsequent '''F-89H''', which entered service in 1956, had an E-9 fire control system like that of the early F-102 and massive new wingtip pods each holding three Falcons (usually three [[semi-active radar homing]] GAR-1s and three [[infrared homing|infrared]] GAR-2s) and 21 FFARs, for a total of six missiles and 42 rockets. Problems with the fire control system delayed the -H's entry into service, by which time its performance was notably inferior to newer [[supersonic]] interceptors, so it was phased out of USAF service by 1959.

The final variant was the '''F-89J'''. This was based on the F-89D, but replaced the standard wingtip missile pod/tanks with 600&nbsp;gal (2,271&nbsp;l) fuel tanks and fitted a pylon under each wing for a single [[AIR-2|MB-1 Genie]] nuclear rocket (sometimes supplemented by up to four conventional Falcon air-to-air missiles). The F-89J became the only aircraft to fire a live Genie as the ''John'' Shot of [[Operation Plumbbob]] on 19 July 1957. There were no new-build F-89Js, but 350 -Ds were modified to this standard. They served with the [[Air Defense Command]], later renamed the [[Aerospace Defense Command]] (ADC), through 1959 and with ADC-gained units of the [[Air National Guard]] through 1969.  This version of the aircraft was extensively used within the [[Semi Automatic Ground Environment]] (SAGE) air defense system.<ref name=CBoF>Green and Swanborough 1994, pp. 457–458.</ref>

A total of 1,050 Scorpions of all variants were produced.

==Variants==
;XF-89
:First prototype, powered by two 4,000 lbf (17.8 kN) Allison J-35-A-9 engines.<ref name="Angel p370">Angelucci and Bowers 1987, p. 370.</ref>
;XF-89A
:Second prototype. Fitted with more powerful (5,100 lbf (22.7 kN) dry, 6,800 lbf (30.3 kN) with afterburner) J-35-A-21A engines and revised, pointed nose with cannon armament.<ref name="Angel p370"/>
;F-89A
:First production version, eight built. Fitted with revised tailplane and six cannon armament.<ref name="Angel p370"/>
;DF-89A
:F-89As converted into drone control aircraft.
;F-89B
:Second production version with upgraded avionics. 40 built.<ref name="Angel p370"/>
;DF-89B
:F-89Bs converted into drone control aircraft.
;F-89C
:Third production version with more powerful engines (5,600 lbf (25.0 kN) dry, 7,400 lbf (32.0 kN) with afterburning J-35-A-21 or −33). 164 built.<ref name="Angel p370"/><ref>Ramirez, Charles E. [http://www.detroitnews.com/article/20120418/METRO03/204180352/1014/metro03/Selfridge-museum-restore-fighter-jet "Selfridge museum to restore fighter jet."] ''The Detroit News,'' 18 April 2012. Retrieved: 18 April 2012.</ref>
;YF-89D
:Conversion of one F-89B to test new avionics and armament of F-89D.<ref name="Angel p370"/>
;F-89D
:Main production version which saw deletion of the six 20&nbsp;mm (.79&nbsp;in) cannons in favor of 104 rockets in wing pods, installation of new Hughes E-6 fire control system, AN/APG-40 radar and the AN/APA-84 computer. This new system allowed the use of a lead-collision attack in place of the previous lead-pursuit-curve technique. A total of 682 built.<ref name="AI Aug p88-9"/><ref name="Angel p370"/>
;YF-89E
:One-off prototype to test the [[Allison J71|Allison YJ71-A-3]] engine (7,000 lbf (31.2 kN) dry, 9,500 lbf (42.4 kN) with afterburner), converted from F-89C.<ref name="Angel p370"/><ref name="AI Aug p92">''Air International'' August 1988, p. 92.</ref>
;F-89F
:Proposed version with new fuselage, wings, and J71-A-7 engines (10,200 lbf (45.4 kN) dry, 14,500 lbf (64.5 kN) with afterburner), never built.<ref name="AI Aug p92"/><ref name=F-89F_Scorpion_SAC_-_24_March_1952>[http://www.alternatewars.com/SAC/F-89F_Scorpion_SAC_-_24_March_1952.pdf "Standard Aircraft Characteristics: Northrop F-89F "Scorpion"."] ''National Museum of the United States Air Force''. Retrieved: 23 October 2016.</ref>
;F-89G
:Proposed version equipped with Hughes MA-1 fire control and [[AIM-4 Falcon|GAR-1/GAR-2 Falcon]] [[air-to-air missile]]s, never built.
;YF-89H
:Modified F-89D to test features of F-89H. Three converted.<ref name="Angel p372">Angelucci and Bowers 1987, p. 372.</ref>
;F-89H
:Version with E-9 fire control system, six GAR-1/GAR-2 Falcon missiles and 42 Folding Fin Aircraft Rockets (FFAR). 156 built.<ref name="Angel p372"/><ref name="AI Aug p89-0">''Air International'' August 1988, pp. 89–90.</ref>
[[File:Northrop F-89J Scorpion.jpg|thumb|Northrop F-89J in 1972]]
;F-89J
:Conversion of F-89D with underwing hardpoints for two MB-1 (later AIR-2) Genie nuclear armed rocket and four Falcon missiles, and carrying either the standard F-89D rocket/fuel pod or pure fuel tanks. A total of 350 were converted from F-89Ds.<ref name="AI Aug p90">''Air International'' August 1988, p. 90.</ref>

==Operators==
: ''see also: [[F-89 Scorpion units of the United States Air Force]]''
;{{USA}}
* [[United States Air Force]]
* [[Air National Guard]]

==Aircraft on display==
[[File:F-89J Hampton Air Power Park VA 2007.jpg|thumb|F-89J, AF Serial No. ''52-2129'', on display at the [[Air Power Park]] and Museum in [[Hampton, Virginia]].]]
;[[File:F-89J Montana ANG display Great Falls 2008.jpg|thumb|F-89J, AF Ser. No. ''53-2547'']]
;F-89B
* 49-2457 – Lakeview Park, [[Nampa, Idaho]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=272 "F-89 Scorpion/49-2457."] ''aerialvisuals.ca'' Retrieved: 2 February 2015.</ref>

;F-89D
* 52-1862 - [[Elmendorf AFB]], [[Anchorage, Alaska]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=1852 "F-89 Scorpion/52-1862."] ''aerialvisuals.ca'' Retrieved: 2 February 2015.</ref>
* 53-2463 – [[Museum of Aviation (Warner Robins)|Museum of Aviation]], [[Robins Air Force Base]], [[Georgia (U.S. state)|Georgia]].<ref>[http://www.museumofaviation.org/F89.php "F-89 Scorpion/53-2463."] ''Robins Air Force Base.'' Retrieved: 25 September 2011.</ref>
* 53-2494 – home base of the [[158th Fighter Wing]], [[Vermont Air National Guard]], [[Burlington Air National Guard Base]], [[Vermont]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=9429 "F-89 Scorpion/53-2494."] ''aerialvisuals.ca'' Retrieved: 2 February 2015.</ref>
* 53-2519 – [[Planes of Fame Museum]], [[Chino, California]].<ref>[http://www.planesoffame.org/index.php?mact=staircraft,m5c7c8,default,1&m5c7c8what=stplanes&m5c7c8forcelist=1&m5c7c8orderby=&m5c7c8detailpage=aircraft-details&m5c7c8nbperpage=20&m5c7c8pageindex=8&m5c7c8returnid=81&m5c7c8returnid=81&page=81 "F-89 Scorpion/53-2519."] ''Planes of Fame Museum.'' Retrieved: 25 September 2011.</ref>
* 53-2536 – [[EAA AirVenture Museum]], [[Oshkosh, Wisconsin]].<ref>[http://www.eaa.org/en/eaa-museum/museum-collection/aircraft-collection-folder/1952-northrop-f-89j-scorpion "F-89 Scorpion/53-2536."] ''EAA AirVenture Museum.'' Retrieved: 12 January 2015.</ref>
* 53-2610 – [[Air Force Armament Museum]], [[Eglin Air Force Base]], Florida.<ref>[http://www.afarmamentmuseum.com/outside.shtml# "F-89 Scorpion/53-2610."] ''Eglin Air Force Base.'' Retrieved: 25 September 2011.</ref>
* 53-2646 – [[Friendship Park (Smithfield, Ohio)|Friendship Park]], [[Smithfield, Ohio]].<ref>[http://www.aerialvisuals.ca/AirframeDossier.php?Serial=54949 "F-89 Scorpion/53-2646."] ''aerialvisuals.ca'' Retrieved: 12 January 2015.</ref>
* 53-2674 – [[Pima Air & Space Museum]] (adjacent to [[Davis-Monthan Air Force Base]]), [[Tucson, Arizona]].<ref>[http://www.pimaair.org/visit/aircraft-by-name/item/northrop-f-89j-scorpion "F-89 Scorpion/53-2674."] ''Pima Air & Space Museum.'' Retrieved: 12 January 2015.</ref>
* 53-2677 - [[Minnesota Air National Guard]] Museum, [[Minneapolis, Minnesota]].<ref>[http://mnangmuseum.org/exhibits/northrop-scorpion/ "F-89 Scorpion/53-2677."] ''Minnesota Air Guard Museum.'' Retrieved: 12 January 2015.</ref>

;F-89H
* 54-0298 – Dyess Linear Air Park, [[Dyess Air Force Base]], [[Texas]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=59757 "F-89 Scorpion/54-0298."] ''aerialvisuals.ca'' Retrieved: 2 February 2015.</ref>
* 54-0322 - [[Hill Aerospace Museum]], [[Hill Air Force Base]], [[Utah]].<ref>[http://www.hill.af.mil/library/factsheets/factsheet.asp?id=5699 "F-89 Scorpion/54-0322."] ''Hill Aerospace Museum.'' Retrieved: 9 October 2012.</ref>

;F-89J
* 52-1856 – [[Bangor International Airport]] / [[Bangor Air National Guard Base]] (former [[Dow AFB]]), [[Maine]].<ref>[http://aerialvisuals.ca/AirframeDossier.php?Serial=3259 "F-89 Scorpion/52-1856."] ''aerialvisuals.ca'' Retrieved: 2 February 2015.</ref>
* 52-1896 – [[New England Air Museum]], [[Windsor Locks, Connecticut]].<ref>[http://www.neam.org/index.php?option=com_content&view=article&id=903 "F-89 Scorpion/52-1896."] ''New England Air Museum.'' Retrieved: 9 October 2012.</ref>
* 52-1911 (painted as 53-2509) – [[National Museum of the United States Air Force]], [[Wright-Patterson Air Force Base]], [[Dayton, Ohio]]. This aircraft was the last F-89 remaining in service when it was transferred to the Museum from the [[Maine Air National Guard]] in July 1969.<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=363 "F-89 Scorpion/52-1911."] ''National Museum of the USAF.'' Retrieved: 9 October 2012</ref>
* 52-1927 – [[Castle Air Museum]] (former [[Castle AFB]]), [[Atwater, California]].<ref>[http://www.castleairmuseum.org/ondisplay "F-89 Scorpion/52-1927."] ''Castle Air Museum.'' Retrieved: 12 January 2015.</ref>
* 52-1941 – [[Peterson Air and Space Museum]], [[Peterson Air Force Base]], [[Colorado]].<ref>[http://petemuseum.org/museum-tour-aerial-photos-airpark/ "F-89 Scorpion/52-1941."] ''Peterson Air and Space Museum.'' Retrieved: 12 January 2015.</ref>
* 52-1949 – [[March Field Air Museum]], [[March Air Reserve Base]] (former [[March AFB]]), [[Riverside, California]].<ref>[http://www.marchfield.org/aircraft-exhibits/aircraft/f-89j-scorpion-northrop/ "F-89 Scorpion/52-1949."] ''March Field Air Museum.'' Retrieved: 12 January 2015.</ref>
* 52-2129 – [[Air Power Park]] and Museum (near [[Langley Air Force Base]]), [[Hampton, Virginia]].<ref>[http://www.militaryaircrafthistorian.com/hamptonairpower.html "F-89 Scorpion/52-2129."] ''Hampton Air Power Park.'' Retrieved: 25 September 2011.</ref>
* 53-2547 – [[120th Fighter Wing]] of the [[Montana Air National Guard]] at [[Great Falls Air National Guard Base]], [[Great Falls International Airport]], [[Montana]]. It is the only F-89 to have ever fired a [[AIR-2 Genie|Genie]] rocket with a live nuclear warhead, having done so as part of [[Operation Plumbob]].<ref>[http://www.aerialvisuals.ca/AirframeDossier.php?Serial=515 "F-89 Scorpion/53-2547."] ''aerialvisuals.ca'' Retrieved: 12 January 2015.</ref>
* 53-2453 – [[Heritage Flight Museum]], [[Bellingham, Washington]].<ref>[http://www.heritageflight.org/collections/f-89j-scorpion/ "F-89 Scorpion/52-2453."] ''Heritage Flight Museum.'' Retrieved: 12 January 2015.</ref>
* 53-2604 – [[119th Wing]] of the [[North Dakota Air National Guard]], [[Fargo Air National Guard Base]] / [[Hector Field]], [[Fargo, North Dakota]].<ref>[http://www.aerialvisuals.ca/AirframeDossier.php?Serial=60143 "F-89 Scorpion/53-2604."] ''aerialvisuals.ca'' Retrieved: 12 January 2015.</ref>

==Specifications (F-89D)==
[[File:F-89 Scorpion afg-041110-020.svg|right|300px|Orthographically projected diagram of the F-89 Scorpion.]]
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
|ref=Scorpion with a Nuclear Sting<ref name="AI Jul p49">''Air International'' July 1988, p. 49.</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully formatted line beginning with an asterisk "*" -->
|crew=2
|length main=53 ft {{frac|9|1|2}} in
|length alt=16.40 m
|span main=59 ft {{frac|8|1|2}} in
|span alt=18.20 m
|height main=17 ft 6 in
|height alt=5.33 m
|area main=606 ft<sup>2</sup>
|area alt=56.30 m<sup>2</sup>
|empty weight main=25,194 lb
|empty weight alt=11,428 kg
|loaded weight main=37,190 lb
|loaded weight alt=16,869 kg
|max takeoff weight main=42,241 lb
|max takeoff weight alt=19,161 kg
|engine (jet)=[[Allison J35]]-A-35
|type of jet=afterburning [[turbojet]]s
|number of jets=2
|thrust main=5,440 lbf
|thrust alt=24.26 kN
|afterburning thrust main=7,200 lbf
|afterburning thrust alt=32.11 kN
|max speed main=635 mph
|max speed alt=552 knots, 1,022 km/h
|max speed more=at 10,600 ft (3,200 m)
|ferry range main=1,366 mi
|ferry range alt=1,188 nm, 2,200 km
|ceiling main=49,200 ft
|ceiling alt=15,000 m
|climb rate main=7,440 ft/min
|climb rate alt=37.8 m/s
|loading main=
|loading alt=
|thrust/weight=
|rockets=<br>
** 104 × 2.75 in (70 mm) "Mighty Mouse" [[Mk 4/Mk 40 Folding-Fin Aerial Rocket|folding-fin aerial rockets]]
** 16 × 5 in (127 mm) aerial rockets on underwing racks ''or''
|bombs=3,200 lb (1,500 kg)
}}

==See also==
{{Portal|United States Air Force}}
{{Aircontent
|related=
|similar aircraft=
* [[Avro Canada CF-100]]
* [[Lockheed F-94 Starfire]]
* [[North American F-86D Sabre]]
* [[Sud Aviation Vautour]]
* [[Yakovlev Yak-25]]
|lists=
* [[List of military aircraft of the United States]]
* [[List of fighter aircraft]]
|see also=
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
* Angelucci, Enzo and [[Peter M. Bowers|Peter Bowers]]. ''The American Fighter''. Yeovil, UK: Haynes Publishing Group, 1987. ISBN 0-85429-635-2.
* Blazer, Gerald and Mike Dario. ''Northrop F-89 Scorpion''. Leicester, UK; Aerofax, 1993. ISBN 0-942548-45-0.
* Davis, Larry and Dave Menard. ''F-89 Scorpion in Action'' (Aircraft Number 104). Carrollton, Texas: Squadron/Signal Publications, 1990. ISBN 0-89747-246-2.
* Green, William and Gordon Swanborough. ''The Complete Book of Fighters: An Illustrated Encyclopedia of Every Fighter Aircraft Built and Flown''. London: Salamander Books, 1994. ISBN 1-85833-777-1.
* Isham, Marty J. and David R. McLaren. ''Northrop F-89 Scorpion: A Photo Chronicle''. Atglen, Pennsylvania: Schiffer Military History, 1996. ISBN 0-7643-0065-2.
* Jenkins, Dennis R. and Tony R. Landis. ''Experimental & Prototype U.S. Air Force Jet Fighters.'' North Branch, Minnesota: Specialty Press, 2008. ISBN 978-1-58007-111-6.
* Kinsey, Bert. ''F-89 Scorpion'', (Detail and Scale Vol. 41). Waukesha, Wisconsin: Kalmbach Publishing, 1992. ISBN 1-85310-630-5.
* Knaack, Marcelle Size. ''Encyclopedia of US Air Force Aircraft and Missile Systems, Volume 1, Post-World War Two Fighters, 1945–1973''. Washington, D.C.: Office of Air Force History, 1978. ISBN 0-912799-59-5.
* "Scorpion with a Nuclear Sting: Northrop F-89". ''[[Air International]]'', Vol. 35, No. 1, July 1988, pp.&nbsp;44–50. Bromley, UK: Fine Scroll. {{ISSN|0306-5634}}.
* "Scorpion with a Nuclear Sting: Northrop F-89—Part Two". ''Air International'', Vol. 35, No. 2, August 1988, pp.&nbsp;86–92. Bromley, UK: Fine Scroll. {{ISSN|0306-5634}}.
* Swanborough, F. Gordon and Peter M. Bowers. ''United States Military Aircraft Since 1909''. London: Putnam, 1963. ISBN 0-87474-880-1.

==External links==
{{Commons|F-89 Scorpion}}
* [http://www.joebaugher.com/usaf_fighters/p89.html Joe Baugher F-89 pages]
* [https://books.google.com/books?id=JyEDAAAAMBAJ&pg=RA1-PA43&dq=popular+science+1951+chrysler+lifts+hood&hl=en&ei=RRLETKGOL83wngflv_TRCQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CDkQ6AEwAA#v=onepage&q&f=true "First Look Inside The USAF F-89 Scorpion Fighter," ''Popular Science'' 1951 article with cutaway of F-89 with original six 20 mm cannon nose, article at bottom of page]
* [http://www.scribd.com/doc/71364790 (1957) T.O. 1F-89D-1 Flight Handbook USAF Series F-89D Scorpion Aircraft (Part 1)], [http://www.scribd.com/doc/71364891  (Part 2)]

{{Northrop aircraft}}
{{USAF fighters}}

[[Category:United States fighter aircraft 1950–1959]]
[[Category:Northrop aircraft|F-089 Scorpion]]
[[Category:Twinjets]]
[[Category:Mid-wing aircraft]]