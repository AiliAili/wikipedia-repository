{{refimprove|date=March 2013}}
{{Infobox prepared food
| name             = Afghan bread
| image            = [[File:Afghan bread.jpg|250px]]
| caption          = 
| alternate_name   = Naan
| country          = [[Afghanistan]]
| region           = 
| creator          = 
| course           = 
| type             = [[Flatbread]]
| served           = 
| main_ingredient  = 
| variations       = 
| calories         = 
| other            = National [[bread]] of [[Afghanistan]]
}}

'''Afghan bread''', or '''Naan-e Afghani''' ([[Persian language|Persian]]:
نان افغانی), is the national [[bread]] of [[Afghanistan]]. The bread is oval or rectangular and baked in a [[tandoor]], a cylindrical oven that is the primary cooking equipment of the sub-continental region. The Afghan version of the tandoor sits above ground and is made of bricks, which are heated to cook the bread. The bread, also known as ''naan'', is shaped and then stuck to the interior wall of the oven to bake. [[Nigella sativa#Etymology|Black cumin]] or [[caraway]] seeds are often sprinkled on the bread, as much for decoration as for taste, and lengthwise lines are scored in the dough to add texture to the bread.

Afghan bread is commonly stocked at Middle Eastern grocery stores in western countries. In Afghanistan the baker still cooks the bread the traditional way by spreading the [[dough]] around the tandoor, so that it quickly puffs up and starts to colour and emit a fresh bread smell that draws the early morning throngs of people. The [[baker]] then uses two long iron [[tongs]] to pull the bread from the tandoor wall. Afghans carry the bread in cloth bags. [[File:Afgan Bread Production.webm|thumb|A video of a baker in Bahrain making Afghan bread.]]

Similar to that in [[Arab]] countries, bread is served with most meals and is generally torn into shreds and used by those eating to envelope foods, so that they can be picked up and conveyed to the mouth, in a manner similar to a [[sandwich]], and also to soak up liquids on the plate. Since people in Afghanistan normally use their hands to eat, the bread thus acts as both a [[fork]] and a [[spoon]].

The bread tastes similar to the Armenian [[lavash]] bread, as well as to [[Iranian cuisine|Iranian]] barbari bread. It has a dense and rich taste.

==References==
{{Reflist}}

{{Flatbreads}}

[[Category:Afghan cuisine]]
[[Category:Pashtun cuisine]]
[[Category:Pakistani breads]]
[[Category:Flatbreads]]
[[Category:Yeast breads]]
[[Category:Articles containing video clips]]