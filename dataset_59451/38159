{{good article}}
{{Infobox person
| name                      = Amir Blumenfeld
| native_name               = אמיר בלומנפלד
| native_name_lang          = Hebrew
| image                     = File:AmirBlumenfeld2016.jpg
| image_size                = 220
| alt                       = 
| caption                   = 
| birth_name                = Amir Shmuel Blumenfeld
| birth_date                = {{Birth date and age|1983|1|18}}
| birth_place               = [[Afula]], [[Israel]]
| nationality               = [[Israeli American]]
| education                = [[University of California, Berkeley]]
| occupation                = Comedian, actor, writer, presenter
| years_active              = 2004–present
| website                   = http://amirblumenfeld.com
}}
'''Amir Shmuel Blumenfeld''' ({{IPAc-en|ə|ˈ|m|ɪər|_|ʃ|m|uː|ˈ|ɛ|l|_|ˈ|b|l|uː|m|ᵻ|n|f|ɛ|l|d}}; {{lang-he|אמיר שמואל בלומנפלד}}; born January 18, 1983) is an [[Israeli-American]] comedian, actor, writer and [[Television presenter|television host]]. Born in Israel, he moved to [[Los Angeles]] when he was two, and was hired by the [[New York City|New York]]-based [[CollegeHumor]] in 2005. As well as contributing to its books and articles, he has written and starred in original videos for the comedy website—appearing in series such as ''Hardly Working'' and ''[[Very Mary-Kate]]''—and was a cast member on its short-lived [[MTV]] program ''[[The CollegeHumor Show]]''.

Amir first came to national prominence in 2004 when he was a semi-finalist during Yahoo's inaugural national IM Live contest, losing to the eventual champions.  Now, he is best known for appearing in the [[web series]] ''[[Jake and Amir]]'' with [[Jake Hurwitz]], in which he plays an annoying and exaggerated version of himself. Originally made by Hurwitz and Blumenfeld in their spare time, the series was then produced by CollegeHumor. Blumenfeld's acting in the series gained him a [[Webby Award]] for Best Individual Performance in 2010.<ref name="Webby">{{cite web|title=2010 – Best Individual Performance – People's Voice – Amir Blumenfeld|url=http://winners.webbyawards.com/2010/online-film-video/performance-craft-categories/best-individual-performance/amir-blumenfeld|work=[[Webby Award]]s|publisher=[[International Academy of Digital Arts and Sciences]]|accessdate=September 15, 2013}}</ref> In 2011, CollegeHumor released ''Jake and Amir: Fired'', a 30-minute special that is the company's first paid content. Episodes of ''Jake and Amir'' average over 500,000 views; by 2012, over 500 had been produced.  The pair have also hosted numerous live shows, and started the advice podcast ''[[If I Were You (podcast)|If I Were You]]'' in 2013. In December 2013, it was announced that ''Jake and Amir'' would be adapted into a [[TBS (TV network)|TBS]] television comedy with [[Ed Helms]] as an executive producer. TBS never filmed the pilot, despite a fan-created #GreenlightJakeAndAmir Twitter campaign that garnered tens of thousands of tweets.<ref>{{cite web|url=https://twitter.com/hashtag/greenlightjakeandamir|title=#greenlightjakeandamir|last=Twitter|first= |work=Hashtag|publisher=Twitter |date=Dec 31, 2015|accessdate=January 5, 2016}}</ref> Instead, sister network [[truTV]] picked up the pilot and filmed it, though it was never aired.

The ''Prank War'' series, which depicts Blumenfeld and [[Streeter Seidell]] as they play a series of escalating [[practical joke]]s on each other, became popular and led to the two appearing on ''[[Jimmy Kimmel Live!]]'' in 2009. MTV later hired Seidell and Blumenfeld to host ''[[Pranked (TV series)|Pranked]]'', a [[clip show]] featuring prank videos from the Internet. Outside of CollegeHumor, Blumenfeld has appeared in the short film ''[[The Old Man and the Seymour]]'', the television series ''[[Louie (TV series)|Louie]]'' and ''[[I Just Want My Pants Back]]'', and the 2011 film ''[[A Very Harold & Kumar 3D Christmas]]''. He also writes for ''[[ESPN The Magazine]]'' and ''[[Mental Floss]]''.

==Early life==
Blumenfeld was born in [[Afula]], [[Israel]],<ref name="MJL">{{cite web|last=Moses|first=Jeremy|title=Jake & Amir: Funnier Than You|url=http://www.myjewishlearning.com/blog/blog/general/jake-amir-funnier-than-you/|work=MyJewishLearning|date=April 21, 2009 |accessdate=September 14, 2013}}</ref> and moved to [[Los Angeles]], [[California]] at the age of two<ref name="COAHP">{{cite web|last=Luu|first=Valerie|title=Making CollegeHumor|url=http://www.cityonahillpress.com/2009/04/02/making-collegehumor/|work=City on a Hill Press|date=April 2, 2009|accessdate=September 14, 2013}}</ref> with his parents and two older brothers—his family is [[Reform Judaism|Reform Jewish]].<ref name="MJL" /> He has described how he became aware of his humor early on: "I realized I was funny at an early age, I realized I could make people laugh at a later age, and then by [[Higher education in the United States|college]] time, I was trying to make jokes in terms of writing".<ref name="Lorenzini">{{cite AV media | url=https://www.youtube.com/watch?v=yEjaTwYFq04 | title=Amir Blumenfeld Interview | publisher=[[YouTube]] | date=December 12, 2008 | accessdate=September 14, 2013 | people=Lorenzini, Wesley (director)}}</ref>

He attended a Jewish [[kindergarten]] and [[elementary school]],<ref name="MJL" /> before going to [[Milken Community High School]], a [[Private school|private]] Jewish [[high school]].<ref>{{cite web|last=Tabibzadeh|first=Sara|title=Who is the Milken student: What websites are we on?|url=http://milkenroar.com/blog/2011/03/02/who-is-the-milken-student-what-websites-are-we-on/|work=The Roar|publisher=[[Milken Community High School|Milken Student Press]]|date=March 2, 2011|accessdate=September 14, 2013}}</ref> During the summer, he attended [[Tech camp|computer camp]] and [[Canada/USA Mathcamp|mathematics camp]], but has expressed regret that he did not go to a Jewish [[summer camp]].<ref>{{cite AV media | url=https://www.youtube.com/watch?v=rg9qLcjdU9s | title=Amir Blumenfeld: The Truth About Jewish Summer Camps | publisher=[[YouTube]] | people=Hutt, Jason (director) | medium=June 10, 2010 | work=MyJewishLearning}}</ref>

Blumenfeld graduated from  the [[Haas School of Business]] at the [[University of California, Berkeley]] with a Bachelor of Science degree, hoping to get a creative job in [[advertising]] or [[marketing]] while writing comedy on the side. He now uses his undergraduate degree "to make somewhat intelligent jokes about finance and accounting, but nothing much beyond that."<ref name="COAHP" />

==Career==

===CollegeHumor===
In 2003, while a [[Sophomore year|sophomore]] at Berkeley, Blumenfeld began writing articles for the comedy website [[CollegeHumor]] after he emailed its co-founder [[Ricky Van Veen]] ideas, which Van Veen found funny and posted on the website. When Blumenfeld graduated in 2005, CollegeHumor hired him and [[Streeter Seidell]] full-time to write ''The CollegeHumor Guide to College''<ref name="MJL" />—a humorous book presented as a guide to [[university]] education—and he moved to [[New York City]] aged 22.<ref name="Lorenzini" /> He later moved to writing original videos for CollegeHumor with [[Dan Gurewitch]],<ref name="Lorenzini" /> and has acted in CH Originals,<ref>{{cite web|title=Amir Blumenfeld|url=http://www.collegehumor.com/user/65455|work=[[CollegeHumor]]|publisher=[[IAC (company)|IAC]]|accessdate=September 14, 2013}}</ref> as well as the series ''Hardly Working''. He has portrayed [[Woody Allen]] in episodes of ''Hardly Working''<ref name="GuyCodeBlog">{{cite web|last=Smiley|first=Brett|title=The 8 Best Woody Allen Impressions We Found On The Internet|url=http://guycodeblog.mtv.com/2010/12/01/the-best-woody-allen-impressions-on-the-internet/|work=[[MTV]]|publisher=[[Viacom]]|accessdate=September 15, 2013|date=December 1, 2010}}</ref> and ''[[Very Mary-Kate]]''<ref>{{cite web|last=Dreier|first=Troy|title=Elaine Carroll Is Very Funny and Very Mary-Kate|url=http://www.streamingmedia.com/Articles/ReadArticle.aspx?ArticleID=76954&PageNum=2|work=Streaming Media|publisher=[[Information Today, Inc.|Information Today]]|accessdate=September 15, 2013|date=August–September 2011}}</ref>—[[MTV]]'s ''[[Guy Code]] Blog'' listed his among "The 8 Best Woody Allen Impressions We Found On The Internet".<ref name="GuyCodeBlog" /> His favorite sketch written for CollegeHumor is entitled "Moments Before Cup Chicks", and involves a director briefing the participants of the viral scatological video ''[[2 Girls 1 Cup]]''.<ref name="Lorenzini" />

Beginning in 2007, he and Streeter Seidell have appeared in the ''Prank War'' series of videos, in which the two play a series of escalating [[practical joke]]s on each other. Seidell has described how some of the pranks "showed Amir's true colors, his desire to be famous ... [and] cut deeper emotionally", and how he thought Blumenfeld's faking a [[marriage proposal]] from Seidell to his girlfriend went "too far". After seven videos were posted over two years, there was an 18-month hiatus culminating in Seidell tricking Blumenfeld into thinking he had won [[United States dollar|USD$]]500,000 after taking a blindfolded half-court basketball shot.<ref name="Wired">{{cite web|last=Tanz|first=Jason|title=Practical Joking Becomes a Battle for the Last Laugh|url=https://www.wired.com/culture/culturereviews/magazine/17-09/mf_hoax_collegehumor|work=[[Wired (magazine)|Wired]]|publisher=[[Condé Nast Publications|Condé Nast]]|date=August 24, 2009|accessdate=September 14, 2013}}</ref> The pranks have led to Seidell and Blumenfeld being interviewed by ''[[Wired (magazine)|Wired]]'' magazine<ref name="Wired" /> and appearing on ''[[Jimmy Kimmel Live!]]''.<ref>{{cite web|last=Hurwitz|first=Jake|title=Prank War on Kimmel!|url=http://www.collegehumor.com/article/3943621/prank-war-on-kimmel|work=[[CollegeHumor]]|publisher=[[IAC (company)|IAC]]|date=March 18, 2009|accessdate=September 14, 2013|authorlink=Jake Hurwitz}}</ref>

In 2009, Blumenfeld starred in ''[[The CollegeHumor Show]]'' on [[MTV]] along with eight other CollegeHumor employees.<ref>{{cite web|title=The CollegeHumor Show – Cast Bios|url=http://www.mtv.com/shows/college_humor/season_1/cast.jhtml|work=[[MTV]]|publisher=[[Viacom]]|accessdate=September 14, 2013}}</ref> The staff members wrote, filmed and starred in the show, which is set in the CollegeHumor offices and has a scripted [[Reality television|reality]] premise.<ref>{{cite web|last=Stelter|first=Brian|title=Dudes! Time for Beer Pong! CollegeHumor.com Invades MTV|url=https://www.nytimes.com/2009/02/05/arts/television/05coll.html|work=[[The New York Times]]|publisher=[[The New York Times Company]]|date=February 4, 2009|accessdate=September 14, 2013|authorlink=Brian Stelter}}</ref> Structured as a half-hour sitcom, it incorporates sketches that had alreadly been published online.<ref name="GigaOM">{{cite web|last=Miller|first=Liz|title=MTV's ''CollegeHumor Show'' Stumbles With Sitcom Cliches|url=http://gigaom.com/2009/02/09/mtvs-collegehumor-show-stumbles-with-sitcom-cliches/|work=[[GigaOM]]|publisher=GigaOmniMedia|date=February 9, 2009|accessdate=September 14, 2013}}</ref> However, the show was lambasted by critics—''Pajiba''{{'s}} Dustin Rowles called it "a series of atrocious sketches haphazardly strung together";<ref>{{cite web|last=Rowles|first=Dustin|title=Only Stupid People are Breeding / The Cretins Cloning and Feeding|url=http://www.pajiba.com/tv_reviews/the-college-humor-show-review.php|work=Pajiba|date=February 10, 2009|accessdate=September 14, 2013}}</ref> ''[[GigaOM]]''{{'s}} Liz Shannon Miller said the show was "deeply disappointing", and that although Blumenfeld's character is "the iconic face of the web site ... none of the other personalities on the show have been developed beyond the surface level"<ref name="GigaOM" />—only one season, consisting of six episodes, was made.<ref>{{cite web|title=The CollegeHumor Show – Episodes|url=http://www.mtv.com/shows/college_humor/season_1/episodes.jhtml|work=[[MTV]]|publisher=[[Viacom]]|accessdate=September 14, 2013}}</ref>

Since 2010, Seidell and Blumenfeld have hosted ''[[Pranked (TV series)|Pranked]]'', an [[MTV]] series featuring pranks recorded on video and posted online.<ref>{{cite web|title=Pranked|url=http://www.mtv.com/shows/pranked/series.jhtml|work=[[MTV]]|publisher=[[Viacom]]|accessdate=September 15, 2013}}</ref> The show has generally received poor reviews, with critics looking down on its [[clip show]] format and use of content from [[YouTube]], and calling it inferior to the "prank war" that inspired it.<ref name="MichiganDaily">{{cite web|last=Chiu|first=Eric|title=MTV's 'Pranked' is a joke with no laughs|url=http://www.michigandaily.com/content/mtvs-pranked-joke-no-laughs|work=[[The Michigan Daily]]|publisher=[[University of Michigan]]|date=September 7, 2009|accessdate=September 14, 2013}}</ref><ref>{{cite web|last=Gilbert|first=Matthew|title='Pranked' is another MTV lowlight|url=http://www.boston.com/ae/tv/articles/2009/08/27/reality_show_pranked_is_another_mtv_lowlight/|work=[[The Boston Globe|Boston.com]]|publisher=[[The New York Times Company]]|date=August 27, 2009|accessdate=September 14, 2013}}</ref> ''[[The Michigan Daily]]''{{'s}} Eric Chiu said "Hosts Blumenfeld and Seidell do what they can with their material, but their banter and commentary is mostly forgettable", and "the ''Prank War'' series on CollegeHumor.com is a perfect example of discomforting gags done right&nbsp;...&nbsp;It's a shame that ''Pranked'' can't muster up anything near the same level of ingenuity."<ref name="MichiganDaily" />

===''Jake and Amir''===
{{Main article|Jake and Amir}}
[[File:Jake Hurwitz 2007.jpg|thumb|Blumenfeld's comedy partner [[Jake Hurwitz]] in 2007]]
Blumenfeld met his colleague [[Jake Hurwitz]] in 2006,<ref name="Variety">{{cite web|last=Kushigemachi|first=Todd|title=Hurwitz & Blumenfeld: College Humor duo graduate to next level|url=http://variety.com/2012/scene/people-news/hurwitz-blumenfeld-college-humor-duo-graduate-to-next-level-1118056720/|work=[[Variety (magazine)|Variety]]|publisher=[[PMC (company)|Penske Media Corporation]]|date=July 24, 2012|accessdate=September 14, 2013}}</ref> when the latter began an [[internship]] at CollegeHumor. The two were seated across from each other, and began to make short videos together, which they uploaded to the video-sharing website [[Vimeo]].<ref name="Haaretz">{{cite web|last=Kenan|first=Ido|title=Meet Jake and Amir, the most successful Jewish comedians on the Internet|url=http://www.haaretz.com/weekend/magazine/meet-jake-and-amir-the-most-successful-jewish-comedians-on-the-internet-1.433684|work=[[Haaretz]]|date=May 31, 2012|accessdate=September 14, 2013}}</ref> Their first video was called "Quick Characters": it was unscripted, and involved either Hurwitz or Blumenfeld spontaneously pointing a camera at the other and instructing them to act in a certain way.<ref name="Lorenzini" />

The two later began the [[web series]] ''Jake and Amir'', episodes of which they posted to <code>jakeandamir.com</code>.<ref name="Haaretz" /> In it, Hurwitz plays Jake, a "normal guy", and Blumenfeld plays Amir, his annoying and obsessive co-worker, who craves Jake's attention.<ref name="Variety" /><ref name="Haaretz" /> Their videos began to be promoted on CollegeHumor, and the website later adopted the series.<ref name="Haaretz" /> By 2012, the duo had made over 500 episodes of ''Jake and Amir''—two per week for five years.<ref name="Variety" /> Each episode averages more than 500,000 views.<ref name=":30" /> The series has featured [[guest appearance]]s by [[Thomas Middleditch]], [[Ben Schwartz]], [[Allison Williams (actress)|Allison Williams]], [[Hoodie Allen]] and [[Rick Fox]], among others.<ref name="Splitsider" />

Blumenfeld has described how his character "sort of evolved" from being "super needy [and] weird" to "a little crazier", but that "the root of my character is still the same, the insecurity of it". Regarding his similarity to his character, he said he is "hopefully very different but maybe at the root of it we're the same person. I'm probably a little smarter than the character though. Maybe the things that he thinks I also think but I'm able to suppress them."<ref name="Splitsider">{{cite web|last=Davis|first=Deirdre Ann|title=Talking to Jake and Amir About Their Web Series, CollegeHumor, and More|url=http://splitsider.com/2013/02/talking-to-jake-and-amir-about-their-web-series-collegehumor-and-more/|work=Splitsider|publisher=[[The Awl]]|date=February 20, 2013|accessdate=September 15, 2013}}</ref>

{{ external media
| float  = left
| width  = 200px
| video1 = [https://www.youtube.com/watch?v=Xab53S5nnE0 Amir Blumenfeld's nomination reel] for the [[Webby Award]]s
}}

In 2008, ''[[PC Magazine]]'' listed the series among its "Top 100 Undiscovered Web Sites", saying: "Considering it's mainly a hobby they do after work, the webisodes at JakeAndAmir.com are better than some of the stuff they get paid to do for CollegeHumor."<ref>{{cite web|last=Monson|first=Kyle|title=The Top 100 Undiscovered Web Sites|url=http://www.pcmag.com/article2/0,2817,2327435,00.asp|work=[[PC Magazine]]|publisher=[[Ziff Davis]]|date=August 11, 2008|accessdate=September 15, 2013}}</ref> At the 14th [[Webby Award]]s in 2010, ''Jake and Amir'' won a People's Voice award for Comedy: Long Form or Series,<ref>{{cite web|title=2010 – Comedy: Long Form or Series – People's Voice – Jake and Amir|url=http://winners.webbyawards.com/2010/online-film-video/general-film-categories/comedy-long-form-or-series/jake-and-amir|work=[[Webby Award]]s|publisher=[[International Academy of Digital Arts and Sciences]]|accessdate=September 15, 2013}}</ref> and Blumenfeld won one for Best Individual Performance.<ref name="Webby" /> ''PC Magazine'' again featured ''Jake and Amir'' in 2011, when it listed the series as one of its "15 Best Web-Only Shows"—Eric Griffith said "they show no sign of running out of very bizarre situations for this sometimes disturbing comedy."<ref>{{cite web|last=Griffith|first=Eric|title=15 Best Web-Only Shows|url=http://www.pcmag.com/slideshow/story/264992/15-best-web-only-shows/3|work=[[PC Magazine]]|publisher=[[Ziff Davis]]|accessdate=September 15, 2013|date=May 27, 2011}}</ref> Blumenfeld received a nomination for the 2013 [[Streamy Awards]] for Best Male Performance: Comedy because of his role in ''Jake and Amir''.<ref>{{cite web|last=Gutelle|first=Sam|title=The Nominees for the 3rd Annual Streamy Awards Are…|url=http://www.tubefilter.com/2012/12/17/3rd-annual-streamy-awards-nominees/|work=[[Tubefilter]]|accessdate=September 27, 2013}}</ref>

On October 12, 2011, CollegeHumor released ''Jake and Amir: Fired'',<ref name="PRNewswire">{{cite web|title=CollegeHumor Media Presents 'Jake and Amir: Fired'|url=http://www.prnewswire.com/news-releases/collegehumor-media-presents-jake-and-amir-fired-131583298.html|work=[[PR Newswire]]|date=October 12, 2011|accessdate=September 15, 2013|medium=Press release}}</ref> a 30-minute episode of ''Jake and Amir'' that the pair had produced and edited in the previous months, while continuing to release short episodes. Available to buy for $2.99 on CollegeHumor and [[Facebook]] and for $13 on [[DVD]], the special was the company's first paid content.<ref>{{cite web|last=Kafka|first=Peter|title=Serious Business? CollegeHumor Tries Selling Web Video.|url=http://allthingsd.com/20111011/serious-business-collegehumor-tries-selling-web-video/?refcat=media|work=[[All Things Digital]]|publisher=[[Dow Jones & Company]]|date=October 11, 2011|accessdate=September 15, 2013}}</ref> Its plot involves the fictitious new CEO of CollegeHumor, Alan Avery ([[Matt Walton]]), promoting Jake and firing Amir; Jake realizes this was a mistake, and the two try to get Amir's job back. Sam Reich, CollegeHumor Media's President of Original Content said "''Fired'' is very much an extension of the ''Jake and Amir'' that people already know. ... That being said, it also expands the universe considerably by adding characters, locations, and something even newer to a ''Jake and Amir'' plot."<ref name="PRNewswire" />

He and Hurwitz have also hosted various live events as Jake and Amir, including ''CollegeHumor Live'' at various locations such as the [[Upright Citizens Brigade Theatre|UCB Theatre]] in New York<ref>{{cite web|title=Two Live Shows This Week!|url=http://jakeandamir.com/post/352719822/two-live-shows-this-week|work=[[Jake and Amir]]|publisher=[[CollegeHumor]]|accessdate=September 15, 2013|date=January 25, 2010}}</ref> and the [[University of California, Berkeley]].<ref>{{cite web|title=CollegeHumor Live! Featuring Jake and Amir|url=http://www.ocf.berkeley.edu/~superb/?p=775|work=SUPERB|publisher=[[Associated Students of the University of California]]|accessdate=September 15, 2013}}</ref> They have also performed in [[Toronto|Toronto, Ontario]], Canada<ref>{{cite web|last=Kopun|first=Francine|title=Jake and Amir touch down in Toronto|url=http://www.thestar.com/entertainment/2011/01/26/jake_and_amir_touch_down_in_toronto.html|work=[[Toronto Star]]|publisher=[[Star Media Group]]|accessdate=September 15, 2013|date=January 26, 2011}}</ref> and [[London]]'s [[Soho Theatre]]. The latter show was in June 2013 with Streeter Seidell,<ref name=":30">{{cite web|title=Jake Hurwitz & Amir Blumenfeld with Streeter Seidell|url=http://sohotheatre.com/whats-on/jake-andamp-amir-with-streeter-siedell/|work=[[Soho Theatre]]|accessdate=September 15, 2013}}</ref> and although it sold out and extra dates were added,<ref>{{cite web|title= 
Jake Hurwitz and Amir Blumenfeld with Stretter Seidell|url=http://www.timeout.com/london/comedy/jake-hurwitz-and-amir-blumenfeld-with-stretter-seidell|work=[[Time Out (magazine)|Time Out]]|publisher=[[Time Out (company)|Time Out Group]]|date=April 8, 2013|accessdate=September 15, 2013}}</ref> the performance was poorly received by local media: ''[[The Guardian]]''{{'s}} Brian Logan said Hurwitz and Blumenfeld "cackle a lot, as they find various ways to repackage tales of puerile behaviour as comedy".<ref>{{cite web|last=Logan|first=Brian|title=Jake Hurwitz & Amir Blumenfeld with Streeter Seidell – review|url=https://www.theguardian.com/stage/2013/jun/27/jake-hurwitz-blumenfeld-seidell-review|work=[[The Guardian]]|publisher=[[Guardian Media Group]]|date=June 27, 2013|accessdate=September 15, 2013}}</ref> In June 2012, at the [[International Student Film Festival]] in [[Tel Aviv]], Israel, the pair gave a lecture at the New Media Conference.<ref>{{cite web|title=Israel Meet Up|url=http://jakeandamir.com/post/24062999660/israel-meet-up|work=[[Jake and Amir]]|publisher=[[CollegeHumor]]|accessdate=September 15, 2013|date=May 30, 2012}}</ref> They have also appeared together on the MTV show ''Money from Strangers''.<ref>{{cite web|title=Money from Strangers|url=http://jakeandamir.com/post/21850235360/money-from-strangers|work=[[Jake and Amir]]|publisher=[[CollegeHumor]]|accessdate=September 21, 2013}}</ref> On December 18, 2013, it was announced that ''Jake and Amir'' would be adapted into a [[television comedy]] for the network [[TBS (U.S. TV channel)|TBS]], and that Blumenfeld would star in the series, as well as serve as a writer and [[executive producer]] (alongside Hurwitz and [[Ed Helms]], among others).<ref>{{cite web|last1=Andreeva|first1=Nellie|title=TBS To Adapt Hit Online Series 'Jake & Amir' As TV Comedy With Ed Helms Producing|url=http://www.deadline.com/2013/12/tbs-to-adapt-hit-online-series-jake-amir-as-tv-comedy-with-ed-helms-producing|website=[[Deadline.com]]|publisher=[[Penske Media Corporation]]|accessdate=September 13, 2014|date=December 18, 2013}}</ref>

====''If I Were You''====
{{Main article|If I Were You (podcast)}}
On May 13, 2013, Hurwitz and Blumenfeld announced their first new project since ''Jake and Amir'': a comedy audio [[podcast]] called ''If I Were You'', in which they [[Advice column|give advice to listeners who submit questions]].<ref>{{cite web|title=New Podcast!|url=http://jakeandamir.com/post/50374908528/new-podcast|work=[[Jake and Amir]]|publisher=[[CollegeHumor]]|date=May 13, 2013|accessdate=September 14, 2013}}</ref> New episodes have been released every Monday since then,<ref>{{cite web|title=If I Were You Show|url=http://ifiwereyoushow.com/|work=[[If I Were You (podcast)|If I Were You]]|accessdate=September 15, 2013}}</ref> and the show has featured several guest stars, including Schwartz, Middleditch, Van Veen,<ref>{{cite AV media | url=http://ifiwereyoushow.com/post/52632996402/episode-6-tattoos-with-ricky-van-veen | title=Episode 6: Tattoos (with Ricky Van Veen) | work=[[If I Were You (podcast)|If I Were You]] | medium=Podcast | date=June 10, 2013 | accessdate=September 14, 2013 }}</ref> Seidell<ref>{{cite AV media | url=http://ifiwereyoushow.com/post/54352604679/episode-9-communism | title=Episode 9: Communism | work=[[If I Were You (podcast)|If I Were You]] | medium=Podcast | date=July 1, 2013 | accessdate=September 14, 2013 }}</ref> and Williams.<ref>{{cite AV media | url=http://ifiwereyoushow.com/post/55515709086/episode-11-zero-to-d-with-allison-williams | title=Episode 11: Zero to D (with Allison Williams) | work=[[If I Were You (podcast)|If I Were You]] | medium=Podcast | date=July 15, 2013 | accessdate=September 14, 2013 }}</ref>  Kayla Culver of ''The Concordian'' lauded the podcast as "comfortable to listen to" and "genuinely funny" and said "It's like listening to two best friends having a hilarious conversation on the couch next to you."<ref>{{cite web|last=Culver|first=Kayla|title='If I Were You'|url=http://theconcordian.org/2013/09/30/if-i-were-you|work=The Concordian|publisher=[[Concordia College (Moorhead, Minnesota)|Concordia College]]|accessdate=October 2, 2013|date=September 30, 2013}}</ref> ''The Guardian''{{'s}} [[Miranda Sawyer]] called ''If I Were You'' "a typical example of a comedy podcast" and "amiable enough", but said it contained "far too much laughing", commenting that "New Yorkers Jake and Amir laugh and laugh, giggle and chortle their way around a topic" and "if I wanted [[Stream of consciousness (narrative mode)|stream-of-consciousness]] waffle with the occasional funny line, I'd listen to [my small children]."<ref>{{cite web|last=Sawyer|first=Miranda|title=Rewind radio: If I Were You; TED Radio Hour; Stuff You Should Know; Stuff Mom Never Told You; Desert Island Discs – review|url=https://www.theguardian.com/tv-and-radio/2013/jul/27/podcasts-review-radio-miranda-sawyer|work=[[The Guardian]]|publisher=[[Guardian Media Group]]|date=July 27, 2013|accessdate=19 August 2013|authorlink=Miranda Sawyer}}</ref>

'''''HeadGum'''''{{Main article|HeadGum}}In 2015, following the success of Hurwitz and Blumenfeld's If I Were You podcast, the duo founded the HeadGum podcasting network. HeadGum includes 11 podcasts, many of which are hosted by comedians who were involved with CollegeHumor, including Streeter Seidell and Josh Ruben. Some of the notable program that can be found on the network includes Blumenfeld's very own ''If I Were You'' podcast, ''[[The Talk of Shame]]'', and ''[[Gilmore Guys]]''.

[[File:AmirBlumenfeldIfIWereYou.jpg|AmirBlumenfeldIfIWereYou|Amir in 2016, definitely not sitting on a cactus]]

'''''Lonely and Horny'''''

In 2016, Hurwitz and Blumenfeld announced their newest show "Lonely and Horny" available exclusively on Vimeo on Demand.

===Other work===
[[File:Amir Blumenfeld.jpg|thumb|Amir Blumenfeld in 2012|alt=Amir Blumenfeld smiling at the camera, with a hand on his shoulder]]
Blumenfeld starred in the 2009 short film ''[[The Old Man and the Seymour]]'' alongside colleagues Streeter Seidell and Dan Gurewitch, as well as [[Shawn Harrison (actor)|Shawn Harrison]], [[Liz Cackowski]] and [[Jordan Carlos]]. It is about a [[Growth hormone deficiency|growth-hormone deficient]] man who is mistaken for a student at his nephew's [[high school]]. The movie was chosen as a "Staff Pick" on Vimeo, and screened at the [[Austin Film Festival]], the [[Sacramento Film and Music Festival]], the [[LA Shorts Fest]], the [[New York Friars' Club#Friars Club Comedy Film Festival|Friars Club Comedy Film Festival]] and the [[Portable Film Festival]].<ref>{{cite web|url=http://www.theoldmanandtheseymour.com/|title=The Old Man and the Seymour|publisher=Wiseguy Pictures|accessdate=September 21, 2013|archiveurl=https://web.archive.org/web/20120702194216/http://www.theoldmanandtheseymour.com/|archivedate=July 2, 2012}}</ref>

In 2011, Blumenfeld appeared in comedian [[Louis C.K.]]'s television series ''[[Louie (TV series)|Louie]]'', during the 10th episode of [[Louie (season 2)|its second season]], entitled "Halloween/Ellie". He played a writer hired to improve a movie script in the second half of the episode. ''Better With Popcorn''{{'s}} George Prax said that he played "the 'unfunny' guy who actually ends up coming off as the funniest of all to the audience", and that Blumenfeld "should be guesting and starring in many more things". He also called Blumenfeld's first sitcom appearance a "clearly momentous occasion".<ref>{{cite web|last=Prax|first=George|title=Louie S02E10 Recap: 'Halloween/Ellie'|url=http://betterwithpopcorn.com/blog/george-prax/tv-news/louie-s02e10-recap-halloweenellie|work=Better With Popcorn|publisher=Electronic Press|date=August 21, 2011|accessdate=September 14, 2013}}</ref> Blumenfeld also had a part in ''[[I Just Want My Pants Back]]'', an MTV show produced by [[Doug Liman]].<ref>{{cite web|title=Amir Blumenfeld|url=http://www.cinemareview.com/castcrew.asp?id=6050|work=Cinema Review|accessdate=September 21, 2013}}</ref>

He played [[Harold & Kumar#Kumar Patel|Kumar Patel]]'s friend Adrian in the 2011 [[Stoner film|stoner]] comedy film ''[[A Very Harold & Kumar 3D Christmas]]'', alongside [[Thomas Lennon (actor)|Thomas Lennon]] as [[Harold & Kumar#Harold Lee|Harold Lee]]'s friend Todd.<ref name="IGN">{{cite web|last=Goldman|first=Eric|title=Twas the night before Christmas, and two potheads were running around New York...|url=http://ign.com/articles/2011/11/03/a-very-harold-kumar-3d-christmas-review|work=[[IGN]]|publisher=IGN Entertainment|date=November 3, 2011|accessdate=September 14, 2013}}</ref> Reviews largely did not remark on his performance,<ref>{{cite web|last=Ebert|first=Roger|title=A Very Harold & Kumar 3D Christmas|url=http://www.rogerebert.com/reviews/a-very-harold-and-kumar-3d-christmas-2011|work=RogerEbert.com|publisher=Ebert Digital|date=November 2, 2011|accessdate=September 15, 2013|authorlink=Roger Ebert}}</ref><ref>{{cite web|last=Gleiberman|first=Owen|title=A Very Harold & Kumar 3D Christmas (2011)|url=http://www.ew.com/ew/article/0,,20541244_20518799,00.html|work=[[Entertainment Weekly]]|publisher=[[Time Warner]]|date=November 9, 2011|accessdate=September 15, 2013|authorlink=Owen Gleiberman}}</ref><ref>{{cite web|last=White|first=James|title=A Very Harold & Kumar 3D Christmas|url=http://www.empireonline.com/reviews/reviewcomplete.asp?FID=137355|work=[[Empire (film magazine)|Empire]]|publisher=[[Bauer Media Group]]|accessdate=September 15, 2013}}</ref><ref>{{cite web|last=O'Sullivan|first=Michael|title=A Very Harold & Kumar Christmas|url=http://www.washingtonpost.com/gog/movies/a-very-harold-and-kumar-christmas,1206285/critic-review.html|work=[[The Washington Post]]|publisher=[[The Washington Post Company]]|date=November 4, 2011|accessdate=September 15, 2013}}</ref><ref>{{cite web|last=Huddleston|first=Tom|title= 
A Very Harold & Kumar 3D Christmas (18)|url=http://www.timeout.com/london/film/a-very-harold-kumar-3d-christmas|work=[[Time Out (magazine)|Time Out]]|publisher=[[Time Out (company)|Time Out Group]]|date=December 6, 2011|accessdate=September 15, 2013}}</ref> although [[IGN]]{{'s}} Eric Goldman said "there isn't much to the Todd and Adrian scenes",<ref name="IGN" /> and ''Pajiba''{{'s}} Daniel Carlson thought the scriptwriters treated the characters as "living props".<ref>{{cite web|last=Carlson|first=Daniel|title=''A Very Harold & Kumar 3D Christmas'' Review: The Gift That Keeps on Taking|url=http://www.pajiba.com/film_reviews/a-very-harold-kumar-3d-christmas-review-the-gift-that-keeps-on-taking.php|work=Pajiba|date=November 4, 2011|accessdate=September 14, 2013}}</ref> T.&nbsp;J. Mulligan of ''Movies on Film'' commented that "anything Adrian says or does ... elicit[s] a slight chuckle at best".<ref>{{cite web|last=Mulligan|first=T.&nbsp;J.|title=A Very Harold & Kumar 3D Christmas Review|url=http://moviesonfilm.com/post/14653633477/a-very-harold-kumar-3d-christmas-review|work=Movies on Film|publisher=Mooshoo Media|date=December 22, 2011|accessdate=September 14, 2013}}</ref> However, Robert Zak of ''WhatCulture!'' commended the film's "strong supporting cast", saying that Lennon and Blumenfeld "provid[ed] constant amusement".<ref>{{cite web|last=Zak|first=Robert|title=A Very Harold & Kumar 3D Christmas Review: Welcomed Gross-out X-mas Comedy|url=http://whatculture.com/film/a-very-harold-kumrar-3d-christmas-review-welcomed-gross-out-x-mas-comedy.php|work=WhatCulture!|date=December 9, 2011|accessdate=September 14, 2013}}</ref>

As a writer, Blumenfeld works freelance for ''[[ESPN The Magazine]]'',<ref name="COAHP" /> and contributed to the ''ESPN Guide to Psycho Fan Behavior''.<ref>{{cite web|title=ESPN Guide to Psycho Fan Behavior|url=http://www.amazon.com/ESPN-Guide-Psycho-Fan-Behavior/dp/B001PTG4PA|work=[[Amazon.com]]|accessdate=September 21, 2013}}</ref> He also has a section in ''[[Mental Floss]]'' called "The Curious Comedian".<ref>{{cite web|title=Amir Blumenfeld|url=http://mentalfloss.com/authors/amir-blumenfeld|work=[[Mental Floss]]|publisher=[[Dennis Publishing]]|accessdate=September 14, 2013}}</ref>

==Influences==
Speaking about the role his [[education]] had in shaping his humor, Blumenfeld said: "I went to Jewish schools growing up and that's where my sense of humor was cultivated. Everybody was funny."<ref name="Haaretz" /> Blumenfeld has said he is influenced by television programs that he watched while growing up, including ''[[The Simpsons]]'', ''[[Seinfeld]]'', and ''[[Saturday Night Live]]'', which he says "taught me how to think ... absurdly ... creatively and originally about jokes that people were making" and make "jokes on jokes".<ref name="Lorenzini" /> He has cited [[Larry David]] as an influence,<ref>{{cite web|last=King|first=Catherine|title=Jake And Amir Interviewed: From College Humour To Global Domination|url=http://sabotagetimes.com/people/jake-and-amir-interviewed-from-college-humour-to-global-domination/|work=[[Sabotage Times]]|accessdate=September 15, 2013}}</ref> and also likes ''[[Louie (TV series)|Louie]]'' and ''[[Curb Your Enthusiasm]]''.<ref name="Variety" /> Blumenfeld has compared the [[NBC]] series ''[[The Office (U.S. TV series)|The Office]]'' and ''[[Parks and Recreation]]'' to ''Jake and Amir'', saying they "have these office workplace dynamics and the situations are funny and the characters are very funny".<ref name="Haaretz" />

==Personal life==
Blumenfeld is [[Judaism|Jewish]],<ref name="Haaretz" /> although he has described himself as "not too religious" and does not attend [[Synagogue|Temple]] every Saturday, nor does he [[Kashrut|keep Kosher]]. He does, however, celebrate Jewish holidays with his family<ref name="MJL" /> and speaks [[Hebrew]].<ref name="Haaretz" /> He is a [[basketball]] fan, and supports the [[Los Angeles Lakers]]—his favorite players are [[Kobe Bryant]] and [[Nick Van Exel]].<ref>{{cite AV media | url=http://blogs.thescore.com/tbj/2011/12/12/tbj-tour-video-two-minutes-with-amir-blumenfeld/ | title=TBJ Tour Video: Two minutes with Amir Blumenfeld | work=The Basketball Jones | publisher=[[theScore Inc.]] | date=December 12, 2011 | accessdate=September 15, 2013}}</ref>

==Filmography==

===Film===
{| class="wikitable plainrowheaders sortable"
|+ '''Amir Blumenfeld's film appearances'''
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" class="unsortable" | Role
! scope="col" class="unsortable" | Notes
|-
| {{sort|2009|2009}}
! scope="row" | {{sort|Old Man and the Seymour|''[[The Old Man and the Seymour]]''}}
| Lewis Plunkett 
| Short film
|-
| {{sort|2011|2011}}
! scope="row" | {{sort|Very Harold & Kumar 3D Christmas|''[[A Very Harold & Kumar 3D Christmas]]''}}
| Adrian 
|
|}

===Television===
{| class="wikitable plainrowheaders sortable"
|+ '''Amir Blumenfeld's television appearances'''
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" class="unsortable" | Role
! scope="col" class="unsortable" | Notes
|-
| {{sort|2009|2009}}
! scope="row" | {{sort|CollegeHumor Show|''[[The CollegeHumor Show]]''}}
| Amir 
|
|-
| {{sort|2010|2010–2012}}
! scope="row" | {{sort|Pranked|''[[Pranked (TV series)|Pranked]]''}}
| Co-host 
| 
|-
| {{sort|2011|2011}}
! scope="row" | {{sort|Louie|''[[Louie (TV series)|Louie]]''}}
| Young nervous writer 
| Episode "[[Louie (season 2)#ep10|Halloween/Ellie]]"
|-
| {{sort|2012|2012}}
! scope="row" | ''[[I Just Want My Pants Back]]''
| [[Hipster (contemporary subculture)|Hipster]] guy
| Episode "[[List of I Just Want My Pants Back episodes#ep3|Never Trust a Moonblower]]"
|-
| {{sort|2012|2012, 2013}}
! scope="row" | ''Money from Strangers''
| 
| With [[Jake Hurwitz]]; 2 episodes
|-
|}

===Online video===
{| class="wikitable plainrowheaders sortable"
|+ '''Amir Blumenfeld's online video appearances'''
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" class="unsortable" | Role
! scope="col" class="unsortable" | Notes
|-
| {{sort|2007|2007–2016}}
! scope="row" | ''[[Jake and Amir]]''
| Amir
| Also writer and editor
|-
| {{sort|2007|2007–2015}}
! scope="row" | ''Hardly Working'' 
| Amir 
| Also writer
|-
| {{sort|2010|2010–2015}}
! scope="row" | ''CollegeHumor Originals''
| Various
|
|-
| {{sort|2010|2010}}
! scope="row" | ''[[Very Mary-Kate]]''
| [[Woody Allen]]
| 2 episodes
|-
| {{sort|2016|2016}}
! scope="row" | ''Lonely and Horny'' 
| Ruby Jade
| Writer and Actor
|-
|}

==Bibliography==
* {{cite book|title=The CollegeHumor Guide to College: Selling Kidneys for Beer Money, Sleeping with Your Professors, Majoring in Communications, and Other Really Good Ideas|year=2006|publisher=[[Penguin Books|Penguin]]|location=New York|isbn=9780525949398|author=The writers of CollegeHumor.com}}
* {{cite book|last1=Blumenfeld|first1=Amir|last2=Hall|first2=Spencer|last3=Trex|first3=Ethan|title=ESPN Guide to Psycho Fan Behavior|year=2007|publisher=[[ESPN Books]]|location=New York|isbn=9781933060361|editor=[[Warren St. John]]}}
* {{cite book|last1=Blumenfeld|first1=Amir|last2=Shah|first2=Neel|last3=Trex|first3=Ethan|title=Faking It: How to Seem Like a Better Person Without Actually Improving Yourself|year=2008|publisher=[[New American Library]]|location=New York|isbn=9780451222527}}
* {{cite book|title=CollegeHumor: The Website. The Book.|year=2011|publisher=[[Da Capo Press]]|location=New York|isbn=9780306820496|author=The writers of CollegeHumor.com}}

==See also==
{{Portal bar|Biography|Comedy|Internet|Israel|United States}}
* [[History of the Jews in Los Angeles]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Amir Blumenfeld}}
* {{Official website|http://www.amirblumenfeld.com}}
* {{IMDb name|3292100}}
* {{Twitter}}

{{CollegeHumor}}

{{Authority control}}

{{DEFAULTSORT:Blumenfeld, Amir}}
[[Category:1983 births]]
[[Category:American male actors]]
[[Category:American male comedians]]
[[Category:American people of Israeli descent]]
[[Category:Israeli emigrants to the United States]]
[[Category:Israeli expatriates in the United States]]
[[Category:Israeli Jews]]
[[Category:Jewish American male actors]]
[[Category:Jewish comedians]]
[[Category:Living people]]
[[Category:People from Afula]]
[[Category:People from Los Angeles County, California]]
[[Category:Webby Award winners]]
[[Category:Haas School of Business alumni]]
[[Category:CollegeHumor people]]
[[Category:Comedians from California]]