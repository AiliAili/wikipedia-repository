{{Infobox journal
| title = Ufahamu: A Journal of African Studies
| cover =
| abbreviation = Ufahamu
| discipline = [[African studies]]
| editor = Janice Levi, Madina Thiam
| publisher = James S. Coleman African Studies Center, UCLA
| country = United States
| history = 1970-present
| frequency = 2 to 3 times/year
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| ISSN = 0041-5715
| eISSN = 
| CODEN = 
| JSTOR =
| LCCN = 2009223306
| OCLC = 471078199
| website = http://escholarship.org/uc/international_asc_ufahamu
| link1 = 
| link1-name = 
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}
'''''Ufahamu: A Journal of African Studies''''' is a graduate-student run, [[peer-reviewed]] [[academic journal]] published at the [[University of California, Los Angeles]] (UCLA). It was established by the [[UCLA African Activist Association]] in 1970 and named after the [[Swahili language |Swahili]] word for comprehension, understanding, or being.<ref name=":02">[http://www.international.ucla.edu/africa/ufahamu/ Ufahamu Home Page]</ref> The journal is published three times a year and is available from the [[University of California]]'s eScholarship website.<ref name=":0">{{Cite journal |last= |first= |year=Spring 2011 |title=UCLA Graduate Division |url= |journal=UCLA Graduate Quarterly: Public Revolution |volume= |pages= |via=}}</ref> It is "the longest running ... graduate student journal in the United States."<ref name="Slyomovics 15–40">{{Cite journal |last=Slyomovics |first=Susan |date=2014-01-01 |title="Every Slight Movement of the People … is Everything" Sondra Hale and Sudanese Art |journal=Journal of Middle East Women's Studies |language=en |volume=10 |issue=1 |pages=15–40 |doi=10.2979/jmiddeastwomstud.10.1.15 |issn=1552-5864}}</ref>

''Ufahamu'' is published in English, with occasional poetry or articles in African and European languages.<ref name=":1">{{Cite journal |last=Abrahams |first=Trevor |title='Coloured’ politics' in South Africa: the quislings' trek into the abyss |journal=Review of African Political Economy |volume=11 |issue=29 |pages=132–138 |doi=10.1080/03056248408703573}}</ref> It is indexed in the [[MLA International Bibliography]], Africa-Wide Information, and [[Historical Abstracts]].

==Origin and purpose==
''Ufahamu'' was conceived in 1969 by a group of graduate students active in UCLA's African Activist Association and [[UCLA African Studies Center |African Studies Center]], after a black-white confrontation at the 1969 African Studies Association in [[Montreal]], where the black caucus critiqued African studies scholars and journals for being "overwhelmingly white and male."<ref name="Waterman 609–611">{{Cite journal |last=Waterman |first=Peter |date=1971-01-01 |title=Radical African Studies in the United States |url=http://www.jstor.org/stable/160217 |journal=The Journal of Modern African Studies |volume=9 |issue=4 |pages=609–611}}</ref> The journal was established with an "activist orientation" to act "as a pressure group with regard to the sociopolitical problems relating to Africa,”<ref name="Slyomovics 15–40"/> and to provide a forum for new perspectives on Africa and sharp discussion.<ref name=":3">{{Cite journal |date=1971-01-01 |title=New Publications |url=http://www.jstor.org/stable/3818429 |journal=Research in African Literatures |volume=2 |issue=1 |pages=88–93}}</ref><ref>{{Cite book |url=https://books.google.com/books?id=jtdWCgAAQBAJ&pg=PA436&dq=%22african+activist+association%22&hl=en&sa=X&ved=0ahUKEwi5p-SttafRAhUBSyYKHTZQAtYQ6AEIVjAL#v=onepage&q=%22african%20activist%20association%22&f=false |title=African Social Studies: A Radical Reader |last=Gutkind |first=C. W. |last2=Waterman |first2=Peter |date=1977-01-01 |publisher=NYU Press |isbn=9780853453819 |language=en}}</ref>

Since its founding, the journal served as a platform for scholars across the diaspora, giving voice to "Africans and Afro-Americans, students, non-academics and academics."<ref name=":3" />

==Topics==
''Ufahamu'' publishes writing aimed at both general readers and scholars, and publishes material "supportive of the African revolution", about Africa and the African diaspora.<ref>{{Cite web |url=http://escholarship.org/uc/search?entity=international_asc_ufahamu;view=aboutUs |title=Ufahamu: A Journal of African Studies [eScholarship] |website=escholarship.org |access-date=2016-12-31}}</ref> This has included articles about African history, politics, economics, sociology, anthropology, law, planning and development, and literature.<ref>{{Cite web |url=http://escholarship.org/uc/international_asc_ufahamu |title=Ufahamu: A Journal of African Studies [eScholarship] |website=escholarship.org |access-date=2016-11-29}}</ref><ref>{{Cite book |url=https://books.google.com/books?id=jI4LAQAAMAAJ&q=ufahamu+%22african+activist+association%22&dq=ufahamu+%22african+activist+association%22&hl=en&sa=X&ved=0ahUKEwiSpLmCuKfRAhWC5iYKHXdBDZcQ6AEIigEwFA |title=Research in African Literatures |date=1970-01-01 |publisher=African and Afro-American Studies and Research Center, University of Texas [at Austin] |language=en}}</ref> It also publishes work on racism, inequality, and language use, such as the use of "coloured" in he South African context.<ref name=":1" />

It has published analyses of "crucial influences" in the study of postcolonial Africa<ref name="Slyomovics 15–40"/> and reviews of modern African art by African critics, drawing favorable comparisons with [[Wole Soyinka]]'s journal ''Transition''.

==Editors==
The journal was initially edited by seven graduate students: Robert Cummings, Salih El Arifi, Sondra Hale, Adolfo Mascarenhas, Reynee Pouissant, Joy Stewart, and Allen Thurm.<ref>{{Cite journal |last=n/a |first=n/a, |date=1970-01-01 |title=Front Matter |url=http://escholarship.org/uc/item/250328v8 |journal=Ufahamu: A Journal of African Studies |volume=1 |issue=1}}</ref><ref name="Slyomovics 15–40"/> By the third volume in 1973, it had an [[editor-in-chief]]. Editors such as [[Es'kia Mphahlele]] tried to use the journal to "actualise African modernity" and encourage the emergence of modern African literature.<ref>{{Cite journal |last=Raditlhalo |first=Tlhalo Sam |last2=Raditlhalo |first2=Sam Tlhalo |date=2011-01-01 |title=Mokgaga wa Maupaneng: A Tribute to Zeke (17 Dec. 1919–27 Oct.2008) |url=http://www.jstor.org/stable/23074948 |journal=English in Africa |volume=38 |issue=2 |pages=9–28}}</ref>

==Impact==
The journal published the first articles of some of the most cited scholars in African studies, including [[Walter Rodney]], whose essays became the book [[How Europe Underdeveloped Africa]], [[John Thornton (historian) |John Thornton]], and [[Sondra Hale]].

Influential articles from ''Ufahamu'' include Judith Van Allen's 1975 essay on the [[Igbo Women's War of 1929]], [[Sondra Hale]] on the controversy over [[Female genital mutilation |genital cutting]], Edward Alpers on African economic history and underdevelopment,<ref>{{Cite journal |last=Edward |first=Alpers, |date=1973-01-01 |title=Rethinking African Economic History: A Contribution to the Discussion of the Roots of Under-Development |url=http://escholarship.org/uc/item/2bd6w2j0?query=alpers%20economic%20history |journal=Ufahamu: A Journal of African Studies |volume=3 |issue=3}}</ref> [[Christopher Ehret]] on African history,<ref>{{Cite journal |last=Christopher |first=Ehret, |date=1974-01-01 |title=Some Thoughts on the Early History of the Nile-Congo Watershed |url=http://escholarship.org/uc/item/2gd85265?query=christopher%20ehret |journal=Ufahamu: A Journal of African Studies |volume=5 |issue=2}}</ref><ref>{{Cite journal |last=Christopher |first=Ehret, |date=1972-01-01 |title=Outlining Southern African History: A Re-evaluation A.D. 100-1500 |url=http://escholarship.org/uc/item/0k49j9nf?query=christopher%20ehret |journal=Ufahamu: A Journal of African Studies |volume=3 |issue=1}}</ref> and articles on the limitations of universal literary critiera,<ref>{{Cite journal |last=Ibe |first=Nwoga, D. |date=1973-01-01 |title=The Limitations of Universal Literary Criteria |url=http://escholarship.org/uc/item/2b61d7j1 |journal=Ufahamu: A Journal of African Studies |volume=4 |issue=1}}</ref> Garveyism,<ref>{{Cite journal |last=Robert |first=Edgar, |date=1976-01-01 |title=Garveyism in Africa: Dr. Wellington and the American Movement in the Transkei |url=http://escholarship.org/uc/item/6wd2119f?query=garveyism%20in%20africa%20wellington |journal=Ufahamu: A Journal of African Studies |volume=6 |issue=3}}</ref> and trance in Nigerian theater.<ref>{{Cite journal |last=Dapo |first=Adelugba, |date=1976-01-01 |title=Trance and Theater [The Nigerian Experience] |url=http://escholarship.org/uc/item/5wm9k8th?query=trance%20theater%20nigerian%20experience |journal=Ufahamu: A Journal of African Studies |volume=6 |issue=2}}</ref>

The journal has been described by scholars as an important part of the "black radical tradition,"<ref name="Waterman 609–611"/> and is credited for introducing important terms in African studies, such as [[Ali Mazrui]]'s concept of "Afrabia"<ref>{{Cite book |url=https://books.google.com/books?hl=en&lr=&id=IzRr1_iPKVcC&oi=fnd&pg=PR9&dq=%22journal+ufahamu%22&ots=qiCuntvwZa&sig=0dEMsixPVt5l_Vogarn9tEaPpj4#v=onepage&q=Ufahamu&f=false |title=The Scholar Between Thought and Experience: A Biographical Festschrift in Honor of Ali A. Mazrui |last=Morewedge |first=Parviz |date=2001-01-01 |publisher=Global Academic Publishing |isbn=9781586840617 |language=en}}</ref> and for contributing to the nature and direction of African studies.<ref>{{Cite web |url=https://www.jstor.org/stable/pdf/160217.pdf |title=Radical African Studies in the United States on JSTOR |website=www.jstor.org |access-date=2017-01-05}}</ref>

==Controversy==
The journal was sometimes banned as a "militant African activist" journal.<ref name="Langhan">{{Cite book |url=https://books.google.com/books?id=xYS4LY_24oYC&lpg=PA111&ots=--R2GsqOUV&dq=ufahamu&pg=PA111#v=onepage&q=ufahamu&f=false |title=The Unfolding Man: The Life and Art of Dan Rakgoathe |last=Langhan |first=Donve |last2=Rakgoathe |first2=Dan |date=2000-01-01 |publisher=New Africa Books |isbn=9780864863706 |language=en}}</ref> The apartheid South African government banned the spring 1982 edition of journal for publishing papers from a recent conference of the [[African Activist Association]], which criticized the government and was titled "''From Apartheid and Imperialism to the Total Liberation of Southern Africa.''"<ref name="Langhan"/>

==References==
{{Reflist|30em}}

==External links==
*{{Official website|http://escholarship.org/uc/international_asc_ufahamu}}

[[Category:Publications established in 1970]]
[[Category:African studies journals]]
[[Category:Academic journals edited by students]]
[[Category:University of California, Los Angeles]]