{{Infobox person
|name        = Philip S. Orsino
|image       = 
|caption     = 
|birth_date  = {{Birth date and age|1954|06|21}}
|birth_place = [[Toronto]], [[Ontario]]
|death_date  = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
|death_place = 
|other_names = 
|known_for   = President and Chief Executive Officer of [[Masonite International]] Corporation
|occupation  = Businessman
|nationality = 
}}
'''Philip S. Orsino''', {{Post-nominals|country=CAN|OC}} (born June 21, 1954) is a [[Canadians|Canadian]] businessman. He is the former President and Chief Executive Officer of [[Masonite International]] Corporation.

Born in [[Toronto]], [[Ontario]], he received a [[Bachelor of Arts]] degree in 1976 from the [[University of Toronto]]'s [[Victoria University in the University of Toronto|Victoria College]]. He became a [[Chartered Accountant]] in 1979 and later a partner in the firm of Hilborn, Ellis, Grant, Chartered Accountants. He was made a Fellow of the [[Canadian Institute of Chartered Accountants|Institute of Chartered Accountants]] in 1997.<ref name=UoT>[http://www.rotman.utoronto.ca/news/newsrelease_101702.htm Masonite International's Philip S. Orsino Receives Rotman Distinguished Business Alumni Award]</ref>

In 1983, he co-founded Century Wood Door Limited and became the President and CEO in 1984. In 1989, it merged with its largest competitor, Premdor. In 2001, it acquired Masonite Corporation from [[International Paper]] and changed its name in 2002 to Masonite International Corporation.<ref name="UoT"/> Masonite International was purchased by [[Kohlberg Kravis Roberts & Co.]] in December 2004 for C$3.1 billion.<ref>[http://www.kkr.com/news/press_releases/2004/12-22-04.html Masonite International Corporation to be Acquired by KKR]</ref>

He is a member of the [[Board of Directors]] of the [[Bank of Montreal]] and is a member of the audit committee and conduct review Committee. He was also Chairman of the Board of Trustees of [[University Health Network]] until 2009.<ref name=BMO>[http://www2.bmo.com/content/0,1263,divId-3_langId-1_navCode-3552,00.html#orsino Bank of Montreal Board of Directors biography]</ref>

He is the author of ''Successful Business Expansion: Practical Strategies for Planning Profitable Growth'' (1994, ISBN 0-471-59737-6).

In 2011, he was appointed President of JELD-WEN,inc., as part of a partnership with ONEX, a Canadian equity investment firm.

==Honours==
* In 1992, he was named the [[American Marketing Association]]'s Business-to-Business Marketer of the Year.<ref name="UoT"/>
* In 1998, he was named Ontario's Entrepreneur of the Year in the Manufacturing sector.<ref name="UoT"/>
* In 2002, he received the 2002 Rotman Distinguished Business Alumni Award from the University of Toronto's [[Rotman School of Management]].<ref name="UoT"/>
* In 2003, he was made an Officer of the [[Order of Canada]].
* In 2003, he was named Canada’s Outstanding CEO of the Year Award.<ref name="BMO"/>
* The Philip S. Orsino Cell Therapy Facility at Toronto's University Health Network is named in his honour.

==References==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Orsino, Philip}}
[[Category:1954 births]]
[[Category:Living people]]
[[Category:Canadian accountants]]
[[Category:Businesspeople from Toronto]]
[[Category:Canadian chief executives]]
[[Category:Canadian economics writers]]
[[Category:Canadian finance and investment writers]]
[[Category:Directors of Bank of Montreal]]
[[Category:Officers of the Order of Canada]]
[[Category:University of Toronto alumni]]
[[Category:Writers from Toronto]]