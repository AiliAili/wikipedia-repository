<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name=Hawk 4
 | image=Utah Olympics Hawk 4.jpg
 | caption=The turboprop powered second prototype.
}}{{Infobox aircraft type
 | type=Four seat [[autogyro]]
 | national origin=United States
 | manufacturer=[[Groen Brothers Aviation|Groen Brothers Aviation Inc]]
 | designer=
 | first flight=4 February 1997 (H2X)
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Groen Hawk 4''' was a single engine, [[pusher configuration]], four seat [[autogyro]] built in the United States in the late 1990s.  Three prototypes, two piston engined and one turboprop powered, were flown but the Hawk did not go into production.

==Design and development==

Design work on the Hawk utility/passenger autogyro began in 1996 and the first prototype, the two seat '''H2X''', first flew on 4 February 1997.  It was later modified to include a third seat and renamed the '''Hawk III'''.  The production prototype, a four-seater named the Hawk 4,  was significantly different from the Hawk III in detail but retained the pod and mid-line pusher engine configuration, combined with low stub wings, twin booms and [[fins]]. Changes between the Hawk III and 4, apart from cabin revisions to accommodate the extra seat, included a new constant [[chord (aircraft)|chord]], aluminium rotor in place of the earlier tapered, composite blade.  The rotor was also lowered and the pitch control levers enclosed within a shallower [[fairing (aircraft)|fairing]].   The wings and fixed [[tailplane]] were cropped, so that they no longer extended beyond the booms and fins, the [[rudder]]s were shortened so that the tailplane no longer needed cut-outs for their movement and extra, short, inboard rudders were added.  Successive engine changes increased the power by almost a factor of two.<ref name=JAWA99/><ref name=JAWA00/>

The Hawk 4 had a steel rotor mast and engine mountings but was mostly aluminium elsewhere, including the rotor, rotor head and [[propeller]]. The [[fuselage]] was an aluminium [[semi-monocoque]].  Some components, such as the nosecone, engine cowling and cabin door were formed from [[composite material]]s.  The rotor was a [[Helicopter rotor#Semirigid|semi-rigid]] or teetering design with a [[swash plate]], rotating at up to 270 rpm.  [[yaw (rotation)|Yaw]] was jointly controlled by the [[balanced rudder]]s on the endplate fins and the all-moving inboard rudders.  It had a fixed, [[tricycle undercarriage]] with mainwheels mounted at the tips of the stub wings.  Two rows of seats accommodated the pilot and up to three passengers; alternatively, the rear seats could be folded to provide baggage space.<ref name=JAWA00/>

The Hawk 4 was powered by a 261&nbsp;kW (350&nbsp;hp) [[Continental TSIO-550]] air cooled [[flat-six]] piston engine, which provided pre-rotation power to the rotor for jump starts and drove a four blade propeller in forward flight. The Hawk 4 first flew on 29 September 1999 and had logged 120 hours by the following April.<ref name=JAWA00/> A second aircraft, initially designated '''Hawk 4T''' and powered by a 313&nbsp;kW (420&nbsp;hp) [[Rolls-Royce 250]]-B17C [[turboprop]] driving a three blade propeller first flew on 12 July 2000.  It was redesignated Hawk 4 when development of the piston engine version was dropped.<ref name=Janes/>  It had a taller undercarriage, high set tailplane, endplate fins with rounded extensions below the tailbooms and modifications to the inboard rudders.  The Hawk 5 was a planned five seat development.  Despite optimistic sales predictions before the 2000 recession, the Hawk did not reach production.<ref name=Janes/>

==Operational history==
In 2002, the [[2002 Winter Olympics|Winter Olympic Games]] contracted with GBA to provide aerial security over Salt Lake International Airport. The Hawk 4 second prototype bearing the acronym ''UOPSC'' (Utah Olympic Public Safety Command) was used for this assignment.<ref>[http://www.aero-news.net/index.cfm?do=main.textpost&id=32b64ddd-24e0-487a-bbf7-dd62f386af7a “Olympic Security Aided by Groen Brother’s Hawk”, Aero-News Network]</ref>

==Specifications (Hawk 4, piston engine)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2000/1<ref name=JAWA00/>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=4
|capacity=
|length m=6.71
|length note=fuselage
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=3.05
|height note=to top or rotor head
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=835
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=1270
|max takeoff weight note=
|fuel capacity=284 L (75 US gal; 62 Im gal)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental TSIO-550]]
|eng1 type=air cooled [[flat-six]] piston
|eng1 kw=261
|eng1 note=
|power original=
|more power=

|prop blade number=4
|prop name=MTV
|prop dia m=1.93
|prop dia note=constant speed

|rot number=1
|rot dia m=12.80
|rot area sqm=128.71
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=225
|max speed note=
|cruise speed kmh=209
|cruise speed note=at 75% power
|never exceed speed kmh=240
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=965
|range note=75% power, maximum fuel
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=4875
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=7.6
|climb rate note=
|time to altitude=
|disk loading kg/m2=9.87
|disk loading note=maximum
|power/mass=
|thrust/weight=

|more performance=
*'''Take-off to 15 m (50 ft):''' 46 m (150 ft)
*'''Landing from 15 m (50 ft):''' 46 m (150 ft)

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name=JAWA99>{{cite book |title= Jane's All the World's Aircraft 1999-2000|last= Jackson|first= Paul |coauthors= |edition= |year=1999|publisher= Jane's Information Group|location= London|isbn=0-7106-1898-0|pages=650}}</ref>

<ref name=JAWA00>{{cite book |title= Jane's All the World's Aircraft 2000-01|last= Jackson |first= Paul |coauthors= |edition= |year=2000|publisher=Jane's Information Group |location= Coulsdon, Surrey|isbn=0-7106-2011-X|pages=671–2}}</ref>

<ref name=Janes>{{cite web|url=http://articles.janes.com/articles/Janes-All-the-Worlds-Aircraft/Groen--Groen-Brothers-Aviation-Inc-United-States.html |title=Jane's - Groen 4 |author= |date= |work= |publisher= |accessdate=25 December 2011 |deadurl=bot: unknown |archiveurl=https://web.archive.org/web/20120509204043/http://articles.janes.com/articles/Janes-All-the-Worlds-Aircraft/Groen--Groen-Brothers-Aviation-Inc-United-States.html |archivedate=9 May 2012 |df= }} </ref>

}}
<!-- ==Further reading== -->

==External links==
{{ external media
|align=left
| video1 ={{youtube|XCEYfW8DyxQ |Hawk 4 Gyroplane Takeoffs and Landings}}
}}

<!-- Navboxes go here -->

[[Category:Autogyros]]
[[Category:United States civil aircraft 1990–1999]]