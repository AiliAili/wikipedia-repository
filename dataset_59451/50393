{{Italic title}}
{{Infobox journal
| title         = African Economic Outlook 
| cover         = 
| editor        = 
| discipline    = [[African studies]], [[economy]]
| peer-reviewed = 
| language      = [[English language|English]], [[French language|French]], [[Portuguese language|Portuguese]]
| formernames   = 
| abbreviation  = Afr. Econ. Outlook
| publisher     = [[African Development Bank]] and [[OECD Development Centre]]
| country       = France 
| frequency     = Annual
| history       = 2002–present  
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.africaneconomicoutlook.org 
| link1         =
| link1-name    =
| link2         =
| link2-name    =
| RSS           = http://www.oecd-ilibrary.org/rss/content/serial/19991029/latest?fmt=rss  
| atom          =
| OCLC          = 49280667  
| LCCN          = 2002242033
| CODEN         = 
| ISSN          = 1995-3909 
| eISSN         = 1999-1029 
}}

'''''African Economic Outlook''''' is an annual reference book-[[academic journal|journal]] which focuses on the [[Economy of Africa|economics of most African countries]]. It reviews the recent [[economy|economic]] situation and predicts the short-term interrelated economic, social, and political evolution of all African economies. The report is published by the [[OECD Development Centre]], the [[African Development Bank]], the [[United Nations Development Programme]] and [[United Nations Economic Commission for Africa]]. It was established in 2002.<ref name=EJL>{{Cite web
  | last =Electronic Journal Library 
  | title =General information on the online edition 
  | work = 
  | publisher =University Library of Regensburg 
  | date =July 2010 
  | url =http://ezb.uni-regensburg.de/detail.phtml?bibid=AAAAA&colors=7&lang=en&jour_id=91430 
  | format = 
  | accessdate =2010-07-25}}</ref><ref name=first-issue>{{Cite journal
  | last =Berthélemy 
  | first =Jean-Claude  
  | authorlink = 
  |author2= Kauffmann, Céline|author3=  Longo Roberto
  | title =African Economic Outlook 2002 OECD Development Centre 
  | journal =African Economic Outlook 
  | volume =
  | pages = 
  | date =February 14, 2002
  | url =http://www.oecd-ilibrary.org/content/book/aeo-2002-en 
  | doi =10.1787/aeo-2002-en  
  | accessdate = }}</ref><ref name=2010-issue>{{Cite journal
  | last =Berthélemy 
  | first =Jean-Claude  
  | authorlink = 
  |author2= Kauffmann, Céline|author3=  Longo Roberto
  | title =African Economic Outlook 2010 OECD Development Centre 
  | journal =African Economic Outlook 
  | volume =
  | pages = 
  | date =June 22, 2010
  | url =http://www.oecd-ilibrary.org/content/serial/19991029
  | doi =10.1787/aeo-2010-en 
  | accessdate = }}</ref>

==Scope==
This annual publication covers [[economic policy]], conditions, and outlook for most of the economies of Africa. It includes [[macroeconomics|macroeconomic]] forecasting for the current and the following year, combined with an analysis of its social and political context. Comparative synthesis of the prospects for African countries in the context of global economics is part of this periodical. Finally, a statistical appendix currently has 24 tables comparing economic and social variables across all the countries of Africa.<ref name=first-issue/><ref name=2010-issue/>

For example,  topic coverage includes the international environment, macroeconomic performances in Africa, structural changes, [[microeconomic reform|economic reform]]s,  external financial flows to Africa,  assessment of [[privatization]] policies, and reduction of [[poverty]] as a challenge for the future. Moreover, coverage includes [[governance]], [[politics|political issue]]s, regional trade policies, and regional integration.<ref name=first-issue/><ref name=2010-issue/>

==See also==
{{Portal|Africa|Sustainable development}}
*[[African Economic Community]]
*[[Economy of the African Union]]
*[[Global Economic Prospects]] (World Bank)

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.africaneconomicoutlook.org/en/}}

[[Category:African studies journals]]
[[Category:Economics journals]]
[[Category:Annual journals]]
[[Category:International development in Africa]]
[[Category:Economy of Africa]]
[[Category:Multilingual journals]]
[[Category:Publications established in 2002]]