{{good article}}
{{Use Australian English|date=December 2013}}
{{Use dmy dates|date=March 2011}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name          = Breathe
| Cover         = Kylie-Minogue-Breathe2.png
| Artist        = [[Kylie Minogue]]
| Album         = [[Impossible Princess]]
| Released      = {{Start date|1998|3|16|df=y}}
| Format        = {{flat list|
* [[CD single]]
* [[Compact Cassette|Cassette]]}}
| Recorded      = Mayfair Studios, London, England. 1997
| Genre         = [[Electronica]]
| Length        = {{Duration|m=3|s=39}}
| Label         = {{flat list|
* [[Deconstruction Records|Deconstruction]]
* [[Mushroom Records|Mushroom]]
* [[Bertelsmann Music Group|BMG]]}}
| Writer        = {{flat list|
*Kylie Minogue
*[[David Ball (musician)|Dave Ball]]
*Ingo Vauk}}
| Producer      = {{flat list|
*Minogue
*Ball
*Vauk}}
| Certification = 
| Chronology    = 
| Last single   = "[[Did It Again (Kylie Minogue song)|Did It Again]]"<br>(1997)
| This single   = "'''Breathe'''"<br>(1998)
| Next single   = "[[Cowboy Style]]"<br>(1998)
}}

"'''Breathe'''" is a song by Australian recording artist [[Kylie Minogue]] from her sixth studio album, ''[[Impossible Princess]]'' (1997). The song was released on 16 March 1998, as the third single from the album, and her final one for the label, [[Deconstruction Records]]. "Breathe" was co-written and co-produced by Minogue with [[David Ball (musician)|Dave Ball]] and Ingo Vauk. Backed by synthesisers and keyboards, it is an [[electronica]] track. The lyrics revolve around [[contemplation]] and [[calmness]]. "Breathe" received mostly positive reviews from music critics, some who highlighted the track as an album stand-out and commended the lyrical and vocal delivery. However, some critics were unimpressed by the composition.

Released in Australia, New Zealand and the United Kingdom, it peaked at number{{nbsp}}twenty-three and fourteen on the [[ARIA Charts|Australian Singles Chart]] and the [[UK Singles Chart]], respectively. This was her last charting single in the UK until her 2000 single "[[Spinning Around]]". To promote "Breathe", Minogue performed the song on ''[[Top of the Pops]]'' and her [[Intimate and Live (concert tour)|Intimate and Live Tour]]. [[Kieran Evans]] directed the song's music video, which features Minogue floating in an open airspace with spiral effects that were generated by [[Computer Generated Imagery|CGI]]. The video was positively received for the production and the visual effects.

==Background and composition==
"Breathe" is about contemplation and holding back emotions. The lyrics were written by Minogue in [[Tokyo]] during her trips with her boyfriend, [[Stéphane Sednaoui]], in late 1995.<ref name="breathe"/> She explained the idea came to mind when her friends were worried about her being silent: "My girlfriend told me 'You don't realize how loud you are when you are quiet'."<ref name="breathe"/> She felt that it was "typical" of her to be thinking and "deciding what was wrong" because she felt that things in her head "were not clear."<ref name="breathe">{{cite AV media notes|url=http://991.com/Buy/ProductInformation.aspx?StockNumber=121264&PrinterFriendly=1|title=An Interview with Kylie Minogue|others=Kylie Minogue|date=1997|type=Liner notes for Question 32: "Breathe" |work=Deconstruction Records|id=KM002}}</ref>

"Breathe" is an [[electronica]] song that lasts a duration of four minutes and thirty-eight seconds.<ref name="nicklevine">{{cite web| first=Nick | last=Levine | url=http://www.digitalspy.co.uk/music/thesound/a223660/kylie-revisited-6-impossible-princess.html#~oH1A8rOhtbk4KA |title=Kylie Revisited: Album 6 - ''Impossible Princess'' |work=[[Digital Spy]] | accessdate=6 June 2010| date=20 March 2015}}</ref> Minogue composed the bridge section using a [[synthesizer|synthesiser]] and [[keyboard instrument|keyboards]].<ref name="notes"/><ref>{{cite web|url=http://www.allmusic.com/song/breathe-mt0032103585 |title=Breathe |work=Allmusic |date= |accessdate=8 February 2012}}</ref>{{efn|group=upper-alpha|Minogue said on ''An Interview with Kylie Minogue'' that she co-produced and co-composed "Breathe", "Say Hey" and "Too Far", but omitted her name from the liner notes' production credits from the studio album.<ref name="breathe"/>}} Livingston Brown played the [[bass guitar]], and Steve Sidelnyk played the [[drums]] on the track; other additional instruments were handled by Ball and Vauk.<ref name="notes"/> "Breathe" was recorded in London, England at Ball and Vauk's home studio's while additional recording was handled at Mayfair Studios.<ref name="notes">{{cite AV media notes|title=Impossible Princess (2xCD)|others=Kylie Minogue|date=2003|type=Liner notes of Special Edition|work=Deconstruction Records|id=82876511152}}</ref> Minogue flew to [[Chicago]], USA to re-record her vocals for the remix by American producer and [[disc jockey]], [[Todd Terry]].<ref name="notes"/>

==Release and commercial response==

"Breathe" was released on 16 March 1998 as the third single from ''Impossible Princess''.<ref name="negative">{{cite book |last=Smith |first=Sean |date=13 March 2014 |title=Kylie |location=London, United Kingdom |publisher=Simon & Schuster Ltd |pages=138–139–141–142 |isbn=978-147-113-5804 }}<!--|access-date=3 April 2015--></ref>{{rp|(138–9)}} It was her final single for the labels, Deconstruction and BMG.<ref name="negative"/>{{rp|(141–2)}} Like "Did It Again", "Breathe" appeared as a double-set of CD singles.<ref>{{cite AV media notes|title=Breathe|others=Kylie Minogue|date=March 1998|type=CD 1, single notes|work=Deconstruction, BMG, Mushroom|id=MUSH01739.2}}</ref> The CD set features remixes and the album version, where the first set includes an interactive music video of "Did It Again".<ref name="cd">{{cite AV media notes|title=Breathe|others=Kylie Minogue|date=March 1998|type=CD 2, single notes|work=Deconstruction, BMG, Mushroom|id=MUSH01739.5}}</ref> The artwork for the singles was shot by Sednaoui, who had photographed the album cover and photo shoot for ''Impossible Princess''. Both CD sets feature close-up shots of Minogue's face, individually taken from different directions. The song was released as a promotional CD and [[cassette single]] in the UK and in [[Gramophone record|vinyl]] format in Spain, Australia and the UK.<ref>{{cite AV media notes|title=Breathe|others=Kylie Minogue|date=February 1998|type=Promotional CD|work=Deconstruction|id=Breathe 02}}</ref><ref>{{cite AV media notes|title=Breathe|others=Kylie Minogue|date=February 1998|type=Cassette|work=Deconstruction, BMG, Mushroom|id=74321 570134}}</ref><ref>{{cite AV media notes|title=Breathe|others=Kylie Minogue|date=February 1998|type=Promotional Vinyl|work=Deconstruction, BMG, Mushroom|id=74321 57834 1}}</ref>

"Breathe" debuted at number&nbsp;14 on the [[UK Singles Chart]] on the week end of 21 March 1998.<ref name="ukcharts">{{cite web|title=Official Singles Chart Top Top 100 - Kylie Minogue - 'Breathe' |url=http://www.officialcharts.com/search/singles/BREATHE/|work=[[Official Charts Company]]|accessdate=8 May 2015}}</ref> The song lasted four{{nbsp}}weeks in the chart.<ref name="ukcharts"/> The song was the fourth highest debut single from that week, the highest being [[Spice Girls]]' single "[[Stop (Spice Girls song)|Stop]]" at number{{nbsp}}2.<ref>{{cite web|title=Official Singles Chart Top Top 100 - Spice Girls - 'Stop'|url=http://www.officialcharts.com/search/singles/STOP/|work=Official Charts Company|accessdate=8 May 2015}}</ref> This became Minogue's twenty-eighth consecutive top{{nbsp}}forty single. "Breathe" and "Did It Again" were equally Minogue's highest charting singles from ''Impossible Princess''.<ref>{{cite web|title=Official Singles Chart Top Top 100 - Kylie Minogue|url=http://www.officialcharts.com/artist/43484/KYLIE%20MINOGUE/|work=Official Charts Company|accessdate=11 December 2013}}</ref> "Breathe" debuted and peaked at number{{nbsp}}twenty-three on the [[Australian Singles Chart]] on the week end of 26 April 1998.<ref name="AUS Charts"/> The song lasted thirteen{{nbsp}}weeks in the top&nbsp;fifty, one of her longest spanning singles in the chart.<ref name="AUS Charts">{{cite web | first = Steffen | last = Hung | url = http://australian-charts.com/showinterpret.asp?interpret=Kylie+Minogue | title = Discography Kylie Minogue | publisher = Australian Charts Portal. Hung Medien | accessdate = 7 May 2015 }}</ref>

==Critical reception==

"Breathe" received mostly positive commentary from music critics. Michael R. Smith from ''Daily Vault'' said "Breathe" and album track "Did It Again" are "most notable for the videos that went along with them and are fair representations of the album at large (which should be the purpose of singles), though there are many more undiscovered gems here."<ref>{{cite web| first=Michael R. | last=Smith | work=Daily Vault |url=http://dailyvault.com/toc.php5?review=4566 |title=''Impossible Princess'' (review) |date=11 May 2008 | accessdate=20 March 2015}}</ref> Nick Levine from ''[[Digital Spy]]'' selected the song as the album's best track by writing "Truth be told, this album lacks an absolute classic to match 'Confide in Me', but 'Breathe' – subtle but sneakily catchy with it – could be one of [Minogue]'s most underrated singles."<ref name="nicklevine"/> While reviewing Minogue's 2004 compilation, ''[[Ultimate Kylie]]'', Jaime Gill from [[Yahoo! Music]] gave it a mixed review by writing "Beginning in 1994 with the gorgeously glacial, slinkily synthetic 'Confide In Me', Kylie enjoyed brief success before fans fled in droves from awkward faux-rock like 'Some Kind of Bliss' (not included) and flimsy house like 'Breathe'."<ref>{{cite web |last=Gill |first=Jaime |title=Yahoo! Music Review - ''Ultimate Kylie'' by Kylie Minogue |url=http://uk.launch.yahoo.com/041116/33/1xaag.html |work=[[Yahoo! Music]] |publication-date=16 November 2004 |archiveurl=https://web.archive.org/web/20081007131801/http://uk.launch.yahoo.com/041116/33/1xaag.html |archivedate=7 October 2008 |accessdate=20 March 2015}}</ref> Reviewing her 2002 compilation, ''[[Hits+]]'', Mackenzie Wilson from [[Allmusic]] commended Minogue's "seductive vocals" on "Breathe", "Automatic Love" and "Confide in Me".<ref>{{cite web|last=Wilson|first=MacKenzie|url=http://www.allmusic.com/album/hits--mw0000659317|title=''Hits+'' - Kylie Minogue|accessdate=17 September 2013|work=AllMusic}}</ref>

Chris True, also from Allmusic, selected the song as a standout track on her ''[[Greatest Hits 87-97]]'' album.<ref>{{cite web|last=True|first=Chris|url=http://www.allmusic.com/album/greatest-hits-87-97-mw0000693625|title=''Greatest Hits 87-97'' - Kylie Minogue|accessdate=6 May 2015|work=AllMusic. Rovi Corporation}}</ref> Jason Lipshutz from ''[[Billboard (magazine)|Billboard]]'' listed "Breathe" at four on their "Kylie Minogue Primer: The Top 10 Past Hits You Need to Know" stating "'Breathe’ is the obvious stand out, sounding almost like a [[Nine Inch Nails]] throwaway in its opening seconds and morphing into a silky, sexy defence of Minogue's experimental side."<ref>{{cite web| first=Jason | last=Lipshutz | url=http://www.billboard.com/articles/columns/pop-shop/5937619/kylie-minogue-primer-the-top-10-past-hits-you-need-to-know |title=Kylie Minogue primer: The top 10 past hits You need to know |work=[[Billboard (magazine)|Billboard]] | accessdate=17 March 2012| date=20 March 2015}}</ref> Louis Virtel from ''The Backlot'' listed "Breathe" at number&nbsp;14 on their "Kylie Minogue’s 50 Best Songs, in Honor of Her Birthday" and said "'Breathe' would be a meditative masterpiece if it weren't so confrontational, deadpan, and sexually domineering. ''Impossible Princess'' unassuming heart stopper trips into unexpected carnality in its choruses, assuring you 'It won’t be long now' as you try to time your hyperventilation."<ref>{{cite web| last=Virtel| first=Louis| url=http://www.thebacklot.com/kylie-minogues-50-best-songs-in-honor-of-her-birthday/05/2013/3/ |title=The Top 50 Best Kylie Minogue Songs in Honor of her Birthday! |work=[[Logo TV]] | accessdate=18 May 2013| date=20 March 2015}}</ref> According to Tom Parker, who provided the special edition album notes for ''Impossible Princess'', "['Breathe'] is a seductive electronic groove, with a hypnotic subtlety and timelessness befitting the theme inward contemplating and resolve."<ref name="notes"/> Larry Flick from ''[[Billboard (Magazine)|Billboard]]'' said that "Breathe" is a "user-friendly jam" which is "largely due to its big-beat electronic groove and ear-tickling pop chorus."<ref>{{cite journal |last=Flick |first=Larry |url=https://books.google.co.nz/books?id=ug4EAAAAMBAJ&pg=PA18&lpg=PA18&dq=billboard+impossible+princess&source=bl&ots=E7UzrRId0h&sig=bMWfj8AgXiYlJgLvk720y-ieBTY&hl=en&sa=X&ei=oARLVdLvMuO7mAWho4DgDQ&ved=0CEMQ6AEwBg#v=onepage&q=billboard%20impossible%20princess&f=false |title=Minogue Makes Mature Turn on deConstruction Set |journal=[[Billboard (magazine)|Billboard]] |volume=110 |issue=14 |page=18 |date=4 April 1998 |issn=0006-2510 |accessdate=2 March 2013}}</ref>

==Music video==

An accompanying [[music video]] was directed by Welsh film director [[Kieran Evans]].<ref name="cd"/> "Breathe" was Evan's directional debut and he went on to work for Heavenly Films, a sister project of British record label [[Heavenly Recordings]].<ref>{{cite web|url=http://heavenlyfilms.net/index.php?section=about|title=Heavenly Films|accessdate=23 February 2014|work=[[Martin Kelly (Heavenly)|Martin Kelly]]}}</ref> It opens with close-up shots of Minogue's body parts. Throughout the video, a giant glass [[Sphere|orb]] is seen on the screen and a mysterious light is shown.<ref name="youtube">{{cite web|url=https://www.youtube.com/watch?v=Y-YTX_-I9qU |title=Kylie Minogue - 'Breathe' |work=Youtube | accessdate=17 October 2009 | date=21 March 2015}}</ref> From there onwards, it shows Minogue in an airspace of spiral effects, all produced by [[green screen]] and CGI effects. In the second verse, it has three shots of Minogue layering over top, with one of the scenes having her singing the track.<ref name="youtube"/> The video then finishes with Minogue floating away, being an in-set of her own eye, which was seen at the start of the video.<ref name="youtube"/> For the radio-video version, the song is sped up at a faster tempo.<ref name="youtube"/>

One of the shots of the video was used as the album cover for her 2002 greatest hits compilation, ''[[Confide in Me (album)|Confide in Me]]''.<ref>{{cite AV media notes|title=''Confide in Me''|others=Kylie|work=[[Bertelsmann Music Group|BMG]]|year=2002|type=Compilation album}}</ref> The music video is featured on other of Minogue's releases including ''[[The Kylie Tapes 94–98]]'', ''[[Greatest Hits 1987–1999]]'', ''[[Kylie Minogue: Artist Collection]]'' and the most recent was on her 2004 DVD, ''[[Ultimate Kylie]]''.<ref>{{cite AV media notes|title=''Ultimate Kylie''|others=Kylie|work=[[Mushroom Records]]|year=2004|type=DVD}}</ref>

==Live performances==
[[File:Kylie Minogue - Kiss Me Once Tour - Manchester - 26.09.14. - 002 (15372943616).jpg|thumb|left|"Breathe" was used as an introduction for Minogue's 2014 [[Kiss Me Once Tour]].]]
Minogue performed "Breathe" in April 1998 on ''[[The Ben Elton Show]]'', and on the Australian evening TV series, ''[[Hey Hey It's Saturday]]''.<ref>{{cite web|url=https://www.youtube.com/watch?v=aGe7MPM9ZFM |title=Kylie Minogue - 'Breathe' & Interview - ''Hey Hey It's Saturday'' 1998 |work=[[Nine Network]] | accessdate=3 February 2014 | date=20 March 2015}}</ref><ref>{{cite web|url=http://www.bbc.co.uk/programmes/p00glzwb |title=The Ben Elton Show, Episode 3 |work=[[BBC One]] | accessdate=9 May 2015 | date=April 1998}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=gmZLPta2vCg |title=Kylie Minogue - 'Breathe' (Live ''The Ben Elton Show'' 1998) |work=BBC One | accessdate=24 August 2014 | date=April 1998}}</ref> The song was included on several live shows including the ''[[Top of the Pops]]'', ''[[The National Lottery Draws|The National Lottery Live]]'' and ''[[Live & Kicking]]'' music show.<ref>{{cite web|url=https://www.youtube.com/watch?v=vdBJTtFyErM |title=Kylie Minogue - 'Breathe' (TOTP 1998) [Live] |work=[[BBC One]] | accessdate=13 April 2014 | date=20 March 2015}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=nOnUVAv_vUA |title=Kylie Minogue - 'Breathe' (''National Lottery Show'' 1998) [Live] |work=BBC | accessdate=9 April 2014 | date=20 March 2015}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=deIUdFXXAQc |title=Kylie Minogue - 'Breathe' (''Live & Kicking'' 1998) [Live] |work=BBC | accessdate=9 April 2014 | date=20 March 2015}}</ref> "Breathe" was included on the [[Intimate and Live Tour]]; it was part of second segment for which Minogue wore a black, long-collared shirt and three-quarter pants, similar to the "Indie Kylie" costume from her music video 'Did It Again'.<ref name="video">{{cite web|url=https://www.youtube.com/watch?v=qXW-nqafI1E |title=Kylie Minogue - 'Some Kind of Bliss' [Intimate and Live Tour] |work=Youtube| accessdate=9 July 2009 | date=14 March 2015}}</ref> Like the rest of the costumes on the tour, including the performance of "Breathe", it featured Minogue with a lot of "princess"-inspired outfits.<ref name="video"/> The performance was recorded on 30 June and 1 July at [[Capitol Theatre, Sydney|Capitol Theatre]] in Sydney, and appeared on the related [[Intimate and Live (album)|CD]] and DVD.<ref>{{cite AV media notes|others=Kylie Minogue|title=[[Intimate and Live (album)|Intimate and Live]]|type=Live album liner notes|work=[[Parlophone]]|date=30 November 1998|id=MUSH33183.2}}</ref><ref>{{cite video|people=Kylie Minogue|title=[[Intimate and Live (album)|Intimate and Live]]|type=DVD |work=Warner Vision|date=2001|id=335248}}</ref> "Breathe" was performed by Minogue on [[Money Can't Buy]], a one-off concert show held on 15 November 2003 at [[Hammersmith Apollo]], London, to promote her ninth studio album ''[[Body Language (Kylie Minogue album)|Body Language]]''. The "Bardello" act of the concert commenced with a mashup of "Breathe" and "[[Je t'aime... moi non plus]]", a 1969 French duet between [[Serge Gainsbourg]] and [[Jane Birkin]].<ref name=steveandersomwork>{{cite web|last = Anderson | first = Steve | title=Kylie – Money Can't Buy |authorlink=Steve Anderson (musician)|url=http://steveandersonproductions.com/kylie-money-cant-buy-body-language-live/|work = Steve Anderson Productions.|accessdate=30 June 2014|archiveurl=https://web.archive.org/web/20140630092900/http://steveandersonproductions.com/kylie-money-cant-buy-body-language-live/|archivedate=30 June 2014}}</ref> Craig McLean from the ''[[Daily Telegraph]]'' described the backup dancers during this segment as "[[Tour de France]] cyclists moonlighting as [[Moulin Rouge]] hostesses."<ref name=reviewtelegraph>{{cite web|last1=McLean|first1=Craig|title=A real tour de force|url=http://www.telegraph.co.uk/culture/3606754/A-real-tour-de-force.html|work=[[Telegraph Media Group]]|accessdate=3 July 2014|date=17 November 2003}}</ref> The performance was later added to Minogue's ''[[Money Can't Buy#Broadcasts and live recording|Body Language Live]]'' DVD from the concert.<ref name=dvdkylie.com>{{cite web|title=Body Language Live|url=http://stage.kylie.com/discography/dvdvideo/body-language-live/|work=Kylie.com. Parlophone|accessdate=4 July 2014|archiveurl=https://web.archive.org/web/20140227144230/http://stage.kylie.com/discography/dvdvideo/body-language-live/|archivedate=27 February 2014 |id=7243 599676 9 1}}</ref> In 2011, Minogue sang an acoustic version while on her 2011 [[Aphrodite World Tour]].<ref>{{cite web|url=https://www.youtube.com/watch?v=vqJRiS5dtmQ |title=Kylie Minogue - 'Breathe' (Acoustic Performance) |accessdate=8 February 2012}}</ref> In 2012, the orchestral version of the song didn't make the track list of ''[[The Abbey Road Sessions (Kylie Minogue album)|The Abbey Road Sessions]]'' but was uploaded on Minogue's official [[YouTube]] account.<ref>{{cite web|url=https://www.youtube.com/watch?v=CapwfJVKhhc |title=Kylie Minogue - 'Breathe 2012' - Recording at Abbey Road |accessdate=21 December 2012}}</ref> In 2014 and 2015, Minogue sampled the song as an introduction for her [[Kiss Me Once Tour]] and [[Kylie Summer 2015|Kylie Summer 2015 Tour]] respectly.<ref>{{cite web|url=http://www.popjustice.com/thenews/kylie-minogues-kiss-me-once-tour-will-be-out-on-dvd-dont-you-worry/133383/|title=Kylie Minogue’s Kiss Me Once tour will be out on DVD, don’t you worry|work=[[Popjustice]]|last1=O'Mance|first1=Brad|date=24 December 2014|accessdate=24 December 2014}}</ref>

==Formats and track listings==

These are the formats and track listings of major single releases of "Breathe".
{{col-begin}}
{{col-2}}
*"'''CD Single #1'''"
#"Breathe" (Radio edit) &mdash; 3:39
#"Breathe" (Tee's Freeze mix) &mdash; 6:59
#"Breathe" (Nalin & Kane remix) &mdash; 10:11
#"Breathe" (Album mix) &mdash; 4:38

*"'''CD Single #2'''"
#"Breathe" (Radio edit) &mdash; 3:39
#"Breathe" ([[Sash!]] Club mix) &mdash; 5:20
#"Breathe" (Tee's Radio edit) &mdash; 3:29
#"Did It Again" music video &mdash; 4:15
{{col-2}}
*"'''CD-Maxi'''"
#"Breathe" (Radio edit) &mdash; 3:39
#"Breathe" (Tee's Radio edit) &mdash; 3:39
#"Breathe" (Tee's Freeze mix) &mdash; 6:59
#"Breathe" (Sash! Club mix) &mdash; 5:20
#"Breathe" (Nalin & Kane remix) &mdash; 10:11

*"'''Cassette tape'''"
#"Breathe" (Radio edit) &mdash; 3:39
#"Breathe" (Sash! Club mix edit) &mdash; 3:43
{{col-end}}

==Personnel==

Credits adapted from both maxi-single [[liner notes]].<ref name="notes"/>

===Song credits===

*[[Kylie Minogue]]&nbsp;– [[Singing|vocals]], [[song writing]], [[record producer|production]], [[synthesizer|synthesiser]], [[Keyboard instrument|keyboard]]s
*[[David Ball (musician)|Dave Ball]]&nbsp;– songwriting, production, other instruments
*Ingo Vauk&nbsp;– songwriting, production, other instruments
*Livingstone Brown&nbsp;– [[bass guitar]]
*Steve Sidelnyk&nbsp;– drums, percussion
*Richard Lowe&nbsp;– [[Music|engineer]], mixing
*Sunny Lizic&nbsp;– engineer, mixing

===Cover credits===

*Kylie Minogue&nbsp;– [[Model (person)|model]]
*[[Stephane Sednaoui]]&nbsp;– [[photographer]], designer
*Farrow Design&nbsp;– cover sleeve programmer

==Charts==

{| class="wikitable sortable plainrowheaders"
|-
!Chart (1997)
!Peak<br />position
|-
!scope="row"|Australia ([[ARIA]])<ref name="AUS Charts"/>
| style="text-align:center;"|23
|-
!scope="row"|UK Singles ([[Official Charts Company]])<ref name="ukcharts"/>
| style="text-align:center;"|14
|}

==Notes==

{{notelist-ua}}

==References==

{{Reflist|30em}}

==External links==
* {{Official website|http://www.kylie.com/}}
* {{MetroLyrics song|kylie-minogue|breathe}}<!-- Licensed lyrics provider -->

{{Kylie Minogue songs}}

{{DEFAULTSORT:Breathe (Kylie Minogue Song)}}
[[Category:1997 songs]]
[[Category:1998 singles]]
[[Category:Kylie Minogue songs]]
[[Category:Songs written by Kylie Minogue]]