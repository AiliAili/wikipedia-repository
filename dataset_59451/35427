{{Use mdy dates|date=December 2016}}
{{good article}}
{{Infobox NCAA team season
  |Mode=Basketball
  |Year=2010–11
  |Prev year= 2009–10
  |Next year= 2011–12
  |Team=Princeton Tigers
  |Image=Princeton Tigers logo.png
  |Image_size=150
  |Conference=Ivy League
  |Division=
  |ShortConference=Ivy
  |CoachRank=
  |APRank= 
  |Record=25–7
  |ConfRecord=12–2, 1st-T
  |HeadCoach=[[Sydney Johnson]] (4th year)
  |Captain=Kareem Maddox
  |Captain2=[[Dan Mavraides]]
  |Captain3=Patrick Saunders
  |AsstCoach1=
  |AsstCoach2=
  |AsstCoach3=
  |StadiumArena=[[Jadwin Gymnasium]]
  |Champion=Ivy League Co-Champion
  |BowlTourney= [[2011 NCAA Men's Division I Basketball Tournament|2011 NCAA Tournament]]
  |BowlTourneyResult=#13 Seed, lost 59–57 to [[2010–11 Kentucky Wildcats men's basketball team|Kentucky]], round of 64
|ConfChamp = [[One-game playoff]] vs. [[2010–11 Harvard Crimson men's basketball team|Harvard]]
|ConfChampResult = Won 63–62
}}
{{2010–11 Ivy League men's basketball standings}}
The '''2010–11 Princeton Tigers men's basketball team''' represented [[Princeton University]] in intercollegiate [[college basketball]] during the [[2010–11 NCAA Division I men's basketball season]]. The [[head coach]] was [[Sydney Johnson]], who was in his fourth season.<ref name=MBRBATR>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?DB_OEM_ID=10600&ATCLID=3749695|title=Men's Basketball Record Book • All-Time Results |date=June 12, 2009|publisher=Princeton Athletic Communications|website=GoPrincetonTigers.com}}</ref> The team's tri-[[captain (sports)|captains]] were senior Kareem Maddox, senior [[Dan Mavraides]], and junior Patrick Saunders. The team played its home games in the [[Jadwin Gymnasium]] on the University campus in [[Princeton, New Jersey]].  The team competes in the [[Ivy League]] athletic conference. The team was coming off of a 22–9 [[2009–10 Princeton Tigers men's basketball team|2009–10 season]] in which it achieved the most wins by a Tigers men's basketball team since the [[1998–99 Princeton Tigers men's basketball team|1998–99 team]] and its first back-to-back finishes of at least second place in the Ivy since [[2001–02 Princeton Tigers men's basketball team|2001–02 season]]. The team was also following on the heels of its first postseason appearance since the [[2003–04 Princeton Tigers men's basketball team|2003–04 team]] went to the [[2004 NCAA Men's Division I Basketball Tournament]],<ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46553&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=204915546|title=Saint Louis Ends Men's Basketball's Postseason Run, 69–59 (with video)|accessdate=October 7, 2010|date=March 25, 2010|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref> and its first postseason victory since the 1998–99 team won two games in the [[1999 National Invitation Tournament]].<ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46553&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=204910872|title=Men's Basketball Gets First Postseason Win Since '99 (with video)|accessdate=October 7, 2010|date=March 17, 2010|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref>

The team was led by returning second team All-Ivy League selections junior Douglas Davis and senior [[Dan Mavraides]].<ref>{{cite web|url=http://www.goprincetontigers.com/ViewArticle.dbml?SPSID=46553&SPID=4231&DB_LANG=C&DB_OEM_ID=10600&ATCLID=204905433|title=Davis, Mavraides Tabbed as All-Ivy for Men's Basketball |accessdate=October 7, 2010|date=March 10, 2010|publisher=Princeton University|website=GoPrincetonTigers.com}}</ref>  This was the first team to have two returning first or second All-Ivy players since the 2003–04 team returned [[Will Venable]] and [[Judson Wallace]]. The team was attempting to defend its [[point (basketball)|scoring]] defense statistical championship, which it won for the twentieth time since 1976 in 2010.<ref>{{cite web|url=http://web1.ncaa.org/stats/StatsSrv/ranksummary|title=Men's Basketball Ranking Summary|accessdate=October 7, 2010|publisher=[[National Collegiate Athletic Association]]}}</ref><ref name=DIR48>{{cite web|url=http://web1.ncaa.org/web_files/stats/m_basketball_RB/2010/D1.pdf|title=Division I Records|accessdate=October 2, 2010|publisher=[[National Collegiate Athletic Association]]|page=48}}</ref> Following the season, seniors Mavraides (2nd team) and Maddox (1st team, unanimous) earned All-Ivy recognition. They were joined by sophomore [[Ian Hummer]] (2nd team).  Maddox earned conference Defensive Player of the Year.

After the annual 14-game [[Round-robin tournament|round robin]] home and away schedule, [[2010–11 Harvard Crimson men's basketball team|Harvard]] and Princeton tied as co-champion, resulting in a [[one-game playoff]] to determine the league's automatic bid to the [[2011 NCAA Men's Division I Basketball Tournament]].  The Tigers prevailed.  Princeton then lost its round of 64 NCAA contest against [[2010–11 Kentucky Wildcats men's basketball team|Kentucky]].  The season marked the team's 26th Ivy League championship and 24th invitation to the [[NCAA Men's Division I Basketball Championship]].

==Preview==
Princeton entered the season having not won a championship since the [[2003–04 Princeton Tigers men's basketball team]] achieved the feat and went to the [[2004 NCAA Men's Division I Basketball Tournament]].<ref name=PtPsuogpwH/><ref>{{cite web|url=http://www.dailyprincetonian.com/2011/03/04/27838/|title=Men's Basketball: Princeton on doorstep of Big Dance|accessdate=March 6, 2011|date=March 4, 2011|work=[[The Daily Princetonian]]|author=Abrams, Mike}}</ref> The six-season championshipless spell tied a school record and put the team on the verge of establishing a new one should the season not be successful.<ref name=PtPsuogpwH/>  The team entered the season with its top five scorers from the prior season returning.<ref>{{cite web|url=http://espn.go.com/mens-college-basketball/team/stats/_/id/163/year/2010/princeton-tigers|title=Princeton Tigers Stats – 2009–10|accessdate=April 24, 2011|publisher=[[ESPN]]}}</ref> The team's schedule included the [[2010 NCAA Men's Division I Basketball Tournament]] champion [[2010–11 Duke Blue Devils men's basketball team|Duke]] as well as Tournament participant {{cbb link|2010|sex=men|team=Siena Saints|school=Siena College|title=Siena}} who were the 2010 [[Metro Atlantic Athletic Conference]] champions (both regular season and [[2010 MAAC Men's Basketball Tournament|tournament]]).<ref>{{cite web|url=http://www.goprincetontigers.com/SportSelect.dbml?SPID=4231&SPSID=46550&DB_OEM_ID=10600|title=M. Basketball – 2010–11 Schedule|accessdate=October 8, 2010|publisher=[[Princeton University]]|website=GoPrincetonTigers.com}}</ref>

Most preseason publications predicted Princeton would finish in first place and [[2010–11 Harvard Crimson men's basketball team|Harvard]] would finish in second place, although the ''[[Sporting News]]'' projected that {{cbb link|2010|sex=men|team=Cornell Big Red|school=Cornell University|title=Cornell}} would finish in first followed by Princeton and Harvard.<ref name="ILMBAwPHEt2S"/> Breaking a three-year streak by [[Cornell Big Red men's basketball|Cornell]], the Ivy League media poll selected Princeton as the top team with twelve first place votes, Harvard second with four first place votes and Cornell third with one first place vote.<ref name=ILMBAwPHEt2S>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/Mens_Basketball_Abound_with_Preseason_Honors|title=Ivy League Men's Basketball Abound with Preseason Honors Entering the 2010–11 Season|accessdate=March 6, 2011|date=October 15, 2010|website=IvyLeagueSports.com}}</ref> It was the first Princeton team to be the preseason top-ranked media selection since the {{cbb link|2004|sex=men|team=Princeton Tigers|school=Princeton University|title=2004–05 Princeton team}}.<ref name=ILMBAwPHEt2S/>

==Playing style==
Head coach Johnson employs the [[Princeton offense]].<ref>{{Cite web|url=http://thequad.blogs.nytimes.com/2009/02/10/the-quad-q-a-princeton-coach-sydney-johnson/|title=The Quad Q.& A.: Princeton Coach Sydney Johnson|accessdate=March 13, 2011|date=February 10, 2009|work=[[The New York Times]]|author=Plutnicki, Ken}}</ref>  However, in 2010, Johnson tweaked it to be a slightly more uptempo version of the motion offense, which resulted in the highest scoring Princeton team in decades.<ref>{{cite web|url=https://www.wsj.com/article/SB10001424052748703380104576015680481303852.html?SPSID=46548&SPID=4231&DB_OEM_ID=10600|title=Princeton Moves Into the Fast(er) Lane: The Tigers Are Using the Same Intricate Half-Court Offense They Made Famous—They're Just Pushing the Tempo More|accessdate=March 14, 2011|date=December 14, 2010|work=[[The Wall Street Journal]]|author=Cohen, Ben}}</ref>  The offense was still considered slow compared to most schools.<ref name="HPmiawgfIt"/>

==Schedule==
The team lost to its only two ranked opponents (#1 Duke and #21 [[2010–11 UCF Knights men's basketball team|University of Central Florida]]),<ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303180150|title=Blue Devils push nonconference winning streak at Cameron to 78 games|accessdate=March 6, 2011|date=November 14, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110227072810/http://espn.go.com/ncb/recap?gameId=303180150| archivedate= February 27, 2011 | deadurl= no}}</ref><ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303642116|title=Marcus Jordan's second-half surge helps Central Florida stay unbeaten|accessdate=March 6, 2011|date=December 30, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110228131854/http://espn.go.com/ncb/recap?gameId=303642116| archivedate= February 28, 2011 | deadurl= no}}</ref> but defeated Siena in [[overtime (sports)|overtime]].<ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303320163|title=Princeton 86, Siena 77|accessdate=March 6, 2011|date=November 28, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110228131849/http://espn.go.com/ncb/recap?gameId=303320163| archivedate= February 28, 2011 | deadurl= no}}</ref>  The team also defeated power conference opponents {{cbb link|2010|sex=men|team=Tulsa Golden Hurricane|school=University of Tulsa|title=Tulsa}} of [[Conference USA]], {{cbb link|2010|sex=men|team=Saint Joseph's Hawks|school=Saint Joseph's University|title=Saint Joseph's}} of the [[Atlantic 10 Conference]] and {{cbb link|2010|sex=men|team=Rutgers Scarlett Knights|school=Rutgers University|title=Rutgers}} of the [[Big East Conference (1979–2013)|Big East Conference]].<ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303160163|title=Princeton nips Rutgers in OT, spoils coach Mike Rice's debut|accessdate=March 6, 2011|date=November 12, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110228131847/http://espn.go.com/ncb/recap?gameId=303160163| archivedate= February 28, 2011 | deadurl= no}}</ref><ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303460202|title=Princeton 82, Tulsa 78|accessdate=March 6, 2011|date=December 12, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110228131849/http://espn.go.com/ncb/recap?gameId=303460202| archivedate= February 28, 2011 | deadurl= no}}</ref><ref>{{cite web|url=http://espn.go.com/ncb/recap?gameId=303390163|title=Princeton 74, Saint Joseph's 65|accessdate=March 6, 2011|date=December 5, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20110228083312/http://espn.go.com/ncb/recap?gameId=303390163| archivedate= February 28, 2011 | deadurl= no}}</ref> Princeton recorded its 11th and the Ivy League's 30th perfect conference record at the halfway point of the 14-game conference schedule.<ref name=10-11Hp5>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/weeklyreleases/2010-11_MBB_Final_Release.pdf|title=2010–11 Highlights|website=IvyLeagueSports.com|page=5}}</ref>  The team posted a perfect 12–0 home record.<ref>{{cite web|url=http://scores.espn.go.com/ncb/recap?gameId=310570163|title=Princeton 66, Columbia 61|accessdate=February 27, 2011|date=February 26, 2011|publisher=[[ESPN]]}}</ref> It was the team's sixth perfect home season in 42 full seasons at Jadwin gymnasium.<ref name=10-11Hp5/> In 2009, the team adopted 11-year-old Christian Michael "Crunch" Regulski, a pediatric brain tumor patient, as part of the Friends of Jaclyn program. In early February, Crunch, who had previously sat on the team's bench, lost his cancer struggle.<ref>{{cite web|url=http://thequad.blogs.nytimes.com/2011/03/17/princeton-players-driven-by-memories-of-crunch/|title=Princeton Players Driven by Memories of ‘Crunch’|accessdate=March 17, 2011|date=March 17, 2011|author=McMurphy, Brett|work=[[The New York Times]]}}</ref> During the season, Princeton swept [[2009–10 Cornell Big Red men's basketball team|defending Ivy League champion Cornell]].<ref name=PaHKP>{{cite web|url=https://www.nytimes.com/2011/02/26/sports/ncaabasketball/26hoops.html|title=Princeton and Harvard Keep Pace|accessdate=March 17, 2011|date=February 25, 2011|work=[[The New York Times]]}}</ref>

Mavraides scored 25 including 5 three-point shots,<ref name=HPmiawgfIt>{{cite web|url=http://sportsillustrated.cnn.com/2011/basketball/ncaa/03/11/harvard.princeton/|title=Harvard, Princeton meet in a winner-take-all game for Ivy title|accessdate=March 14, 2011|date=March 11, 2011|work=[[Sports Illustrated]]|author=Friedman, Dick}}</ref> but Princeton lost to [[2010-11 Harvard Crimson men's basketball team|Harvard]] at [[Lavietes Pavilion]] on March 5 and giving them a split of the season series.<ref name=HtP7tsIt/><ref>{{cite web|url=https://www.nytimes.com/2011/03/06/sports/ncaabasketball/06ivy.html|title=Harvard Has Piece of Title but Wants the Rest of It|accessdate=March 17, 2011|date=March 5, 2011|work=[[The New York Times]]|author=May, Peter| archiveurl= https://web.archive.org/web/20110306130416/http://www.nytimes.com//2011//03//06//sports//ncaabasketball//06ivy.html| archivedate= March 6, 2011 | deadurl= no}}</ref> Princeton fell a half game behind Harvard who clinched at least a share of the [[2010–11 Ivy League men's basketball season]] Championship with a 12–2 conference record. Princeton fell to 11–2 with one conference game remaining to force a one-game playoff for the conferences automatic bid to the [[2011 NCAA Men's Division I Basketball Tournament]].<ref name=HtP7tsIt>{{cite web|url=http://espn.go.com/ncb/recap?gameId=310640108|title=Harvard tops Princeton 79–67 to share Ivy title|accessdate=March 6, 2011|date=March 5, 2011|publisher=[[ESPN]]}}</ref> Following the game, [[Sydney Johnson]] made his team sit on the bench and watch the Harvard fans celebrate.<ref>{{cite web|url=http://espn.go.com/video/clip?id=6191433|title=Harvard Celebrates Early |accessdate=March 9, 2011|date=March 7, 2011|publisher=[[ESPN]]}}</ref> On March 8, Princeton defeated Penn to force a one-game playoff at the [[Payne Whitney Gymnasium]] at [[Yale University]] in [[New Haven, Connecticut]].<ref name=PtPsuogpwH>{{cite web|url=http://scores.espn.go.com/ncb/recap?gameId=310670219|title=Princeton tops Penn, sets up one-game playoff with Harvard|accessdate=March 9, 2011|date=March 8, 2011|publisher=[[ESPN]]}}</ref>  Maddox scored 21 of his 23 points from off the bench in the second half to key the victory.<ref name=HPmiawgfIt/><ref>{{cite web|url=https://www.nytimes.com/2011/03/09/sports/ncaabasketball/09hoops.html|title=Princeton Forces a Playoff for the Ivy’s N.C.A.A. Bid|accessdate=March 17, 2011|date=March 8, 2011|work=[[The New York Times]]}}</ref>

The one-game playoff is the eighth in Ivy League history and the eighth for Princeton, who has won at least a share of twenty-six league titles.<ref name=PtPsuogpwH/><ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/2011_Mens_Basketball_Playoff|title=2011 Men's Basketball Playoff Information|accessdate=March 12, 2011|date=March 11, 2011|website=IvyLeagueSports.com}}</ref>  The share of the championship ended a six-season championshipless run, which tied a record for the longest in school history.<ref name=PtPsuogpwH/>  In the one-game playoff, Harvard took  58–57 lead with 2:33 remaining and then the teams traded one-point leads 5 times.<ref>{{cite web|url=http://scores.espn.go.com/ncb/playbyplay?gameId=310710163|title=Harvard 62 (23–6, 12–2 Ivy); Princeton 63 (25–6, 12–2 Ivy) Complete Play-By-Play|accessdate=May 5, 2011|date=March 12, 2011|publisher=[[ESPN]]}}</ref> Princeton won by a 63–62 margin thanks to a last second shot by Davis to earn its 24th [[NCAA Men's Division I Basketball Tournament]] bid.<ref name=PKHoljteNb>{{cite web|url=http://scores.espn.go.com/ncb/recap?gameId=310710163|title=Princeton KO's Harvard on last-second jumper to earn NCAA bid|accessdate=March 12, 2011|date=March 12, 2011|publisher=[[ESPN]]}}</ref><ref>{{cite web|url=https://www.nytimes.com/2011/03/13/sports/ncaabasketball/13ivy.html|title=Princeton Pauses but Still Beats Buzzer on Way to N.C.A.A. Bid|accessdate=March 17, 2011|date=March 12, 2011|author=Pennington, Bill|work=[[The New York Times]]}}</ref>  Although Princeton did not appear in the [[2010–11 NCAA Division I men's basketball rankings]] all season, in the final regular season poll on March 13, Princeton received 3 points in the [[Coaches' Poll]].<ref>{{cite web|url=http://www.usatoday.com/sports/college/mensbasketball/usatpoll.htm |archiveurl=http://www.webcitation.org/5xBwujmfR?url=http%3A%2F%2Fwww.usatoday.com%2Fsports%2Fcollege%2Fmensbasketball%2Fusatpoll.htm |archivedate=March 15, 2011 |title=Top 25: Ohio State finishes regular season at No. 1 |accessdate=March 15, 2011 |date=March 13, 2011 |work=[[USAToday]] |deadurl=no |df= }}</ref>

Princeton was awarded the number thirteen seed and a first round match against the [[2010-11 Kentucky Wildcats men's basketball team|Kentucky Wildcats]].<ref>{{cite web|url=http://espn.go.com/ncb/news/story?id=6212994|title=2011 NCAA tournament selections|accessdate=March 13, 2011|date=March 13, 2011|publisher=[[ESPN]]}}</ref> Kentucky had eliminated Ivy League representative Cornell the prior season.<ref>{{cite web|url=http://thequad.blogs.nytimes.com/2011/03/16/looking-ahead-to-thursdays-games/|title=Looking Ahead to Thursday’s Games|accessdate=March 17, 2011|date=March 16, 2011|author=Ennis, Connor|work=[[The New York Times]]| archiveurl= https://web.archive.org/web/20110317094048/http://thequad.blogs.nytimes.com/2011/03/16/looking-ahead-to-thursdays-games/| archivedate= March 17, 2011 | deadurl= no}}</ref>  Kentucky emerged victorious by a 59–57 margin on a last second layup.<ref>{{cite web|url=http://scores.espn.go.com/ncb/recap?gameId=310760096|title=Brandon Knight's only basket lifts Kentucky past Princeton in final seconds|accessdate=March 17, 2011|date=March 17, 2011|publisher=[[ESPN]]}}</ref><ref>{{cite web|url=https://www.nytimes.com/2011/03/18/sports/ncaabasketball/18kentucky.html|title=Pushed by Princeton, Kentucky Wins in Final Seconds|accessdate=March 19, 2011|date=March 18, 2011|work=[[The New York Times]]|author=McMurphy, Brett}}</ref>

{{CBB schedule start|attend=yes}}
|-
!colspan=9| Regular season
{{CBB schedule entry
| date         =  November 12, 2010
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = 
| rank         = 
| opponent     = [[Rutgers Scarlet Knights men's basketball|Rutgers]]
| opprank      = 
| site_stadium = [[Jadwin Gymnasium]]
| site_cityst  = [[Princeton, NJ]]
| gamename     = 
| tv           = 
| score        = 78–73
| overtime     = OT
| attend       = 3,530
| record       = 1–0
}}
{{CBB schedule entry
| date         =  November 14, 2010
| time         = 5:00&nbsp;pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[2010-11 Duke Blue Devils Men's Basketball Team|Duke]]
| opprank      = 1
| site_stadium = [[Cameron Indoor Stadium]]
| site_cityst  = [[Durham, NC]]
| gamename     = [[CBE Classic|O'Reilly Auto Parts CBE Classic]]
| tv           = [[ESPNU]]
| score        = 60–97
| overtime     =
| attend       = 9,314
| record       = 1–1
}}
{{CBB schedule entry
| date         =  November 22, 2010
| time         = 7:00&nbsp;pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[James Madison Dukes men's basketball|James Madison]]
| opprank      = 
| site_stadium = [[JMU Convocation Center]]
| site_cityst  = [[Harrisonburg, VA]]
| gamename     = CBE Classic
| tv           = 
| score        = 64–65
| overtime     = 
| attend       = 3,113
| record       = 1–2
}}
{{CBB schedule entry
| date         =  November 23, 2010
| time         = 4:30&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = yes
| rank         = 
| opponent     = [[Bucknell Bison men's basketball|Bucknell]]
| opprank      = 
| site_stadium = JMU Convocation Center
| site_cityst  = Harrisonburg, VA
| gamename     = CBE Classic
| tv           = 
| score        = 66–55
| overtime     =
| attend       = &nbsp;
| record       = 2–2
}}
{{CBB schedule entry
| date         =  November 24, 2010
| time         = 4:30&nbsp;pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = yes
| rank         = 
| opponent     = [[Presbyterian Blue Hose|Presbyterian]]
| opprank      = 
| site_stadium = JMU Convocation Center
| site_cityst  = Harrisonburg, VA
| gamename     = CBE Classic
| tv           = 
| score        = 67–69
| overtime     =
| attend       = &nbsp;
| record       = 2–3
}}
{{CBB schedule entry
| date         =  November 28, 2010
| time         = 2:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = 
| rank         = 
| opponent     = [[Siena Saints men's basketball|Sienna]]
| opprank      = 
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| tv           = 
| score        = 86–77
| overtime     = OT
| attend       = 1,906
| record       = 3–3
}}
{{CBB schedule entry
| date         =  November 30, 2010
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[Lafayette Leopards|Lafayette]]
| opprank      = 
| site_stadium = [[Allan P. Kirby Arena]]
| site_cityst  = [[Easton, PA]]
| gamename     = 
| tv           = LFSN
| score        = 82–64
| overtime     =
| attend       = 1,559
| record       = 4–3
}}
{{CBB schedule entry
| date         =  December 5, 2010
| time         = 5:30&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = 
| rank         = 
| opponent     = [[Saint Joseph's Hawks men's basketball|Saint Joseph's]]
| opprank      = 
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| gamename     = 
| tv           = 
| score        = 74–65 
| overtime     =
| attend       = 2,010
| record       = 5–3
}}
{{CBB schedule entry
| date         =  December 8, 2010
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      =
| rank         = 
| opponent     = [[Monmouth Hawks men's basketball|Monmouth]]
| site_stadium = [[Multipurpose Activity Center]]
| site_cityst  = [[West Long Branch, NJ]]
| tv           = 
| score        = 64–61
| overtime     = 
| attend       = 1,287
| record       = 6–3
}}
{{CBB schedule entry
| date         =  December 12, 2010
| time         = 2:00&nbsp;pm 
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[Tulsa Golden Hurricane men's basketball|Tulsa]]
| opprank      =
| site_stadium = [[Reynolds Center]]
| site_cityst  = [[Tulsa, OK]]
| tv           = 
| score        = 82–78
| overtime     = 2OT
| attend       = 4,855
| record       = 7–3
}}
{{CBB schedule entry
| date         =  December 17, 2010
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[Wagner Seahawks men's basketball|Wagner]]
| opprank      =
| site_stadium = [[Spiro Sports Center]]
| site_cityst  = [[Staten Island, NY]]
| tv           = TWCSN
| score        = 69–57
| overtime     = 
| attend       = 1,027
| record       = 8–3
}}
{{CBB schedule entry
| date         =  December 22, 2010
| time         = 4:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      = 
| rank         = 
| opponent     = [[Towson Tigers men's basketball|Towson]]
| opprank      = 
| site_stadium = [[Towson Center]]
| site_cityst  = [[Towson, MD]]
| tv           = 
| score        = 75–65
| overtime     =
| attend       = 1,891
| record       = 9–3
}}
{{CBB schedule entry
| date         =  December 29, 2010
| time         = 9:30&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         =
| neutral      = yes
| rank         = 
| opponent     = [[Northeastern Huskies men's basketball|Northeastern]]
| opprank      =
| site_stadium = [[UCF Arena]]
| site_cityst  = [[Orlando, FL]]
| gamename     = UCF Holiday Classic
| tv           = 
| score        = 65–63
| overtime     =
| attend       = 7,111
| record       = 10–3
}}
{{CBB schedule entry
| date         =  December 30, 2010
| time         = 7:30&nbsp;pm
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         = 
| opponent     = [[2010–11 UCF Knights men's basketball team|UCF]]
| opprank      = 19
| site_stadium = UCF Arena
| site_cityst  = Orlando, FL
| gamename     = UCF Holiday Classic
| tv           = 
| score        = 62–68
| overtime     =
| attend       = 5,591
| record       = 10–4
}}
{{CBB schedule entry
| date         =  January 5, 2011
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         =
| neutral      =
| rank         = 
| opponent     = [[Marist Red Foxes men's basketball|Marist]]
| opprank      =
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| tv           = 
| score        = 68–57
| overtime     = 
| attend       = 1,539
| record       = 11–4
}} 
{{CBB schedule entry
| date         =  January 23, 2011
| time         = 6:00&nbsp;pm
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         = 
| neutral      =
| rank         = 
| opponent     = [[The College of New Jersey|College of New Jersey]]
| opprank      =
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| tv           = 
| score        = 73–40
| overtime     =
| attend       = 1,837
| record       = 12–4 
}}
{{CBB schedule entry
| date         =  January 28, 2011
| time         = 7:00&nbsp;pm
| w/l          = w
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         = 
| opponent     = [[Brown Bears men's basketball|Brown]]
| opprank      =
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| tv           = [[FiOS1]]
| score        = 78–60
| overtime     =
| attend       = 2,330
| record       = 13–4 (1–0)
}}
{{CBB schedule entry
| date         =  January 29, 2011
| time         = 6:00&nbsp;pm
| w/l          = w
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         = 
| opponent     = [[Yale Bulldogs men's basketball|Yale]]
| opprank      = 
| site_stadium = Jadwin Gymnasium
| site_cityst  = Princeton, NJ
| tv           = 
| score        = 67–63
| overtime     =
| attend       = 2,658
| record       = 14–4 (2–0)
}}
{{CBB schedule entry
| date          =  February 4, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = 
| neutral       =
| rank          = 
| opponent      = [[2010-11 Harvard Crimson men's basketball team|Harvard]]
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| tv            = ESPNU
| score         = 65–61 
| overtime      =
| attend        = 4,148
| record        = 15–4 (3–0)
}}
{{CBB schedule entry
| date          =  February 5, 2011
| time          = 6:00&nbsp;pm
| w/l           = w
| nonconf       = 
| homecoming    =
| away          = 
| neutral       =
| rank          = 
| opponent      = [[Dartmouth Big Green men's basketball|Dartmouth]]
| opprank       =
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| tv            = 
| score         = 68–53
| overtime      =
| attend        = 3,346
| record        = 16–4 (4–0)
}}
{{CBB schedule entry
| date          =  February 8, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          =
| neutral       =
| rank          = 
| opponent      = [[Penn Quakers men's basketball|Penn]]
| opprank       = 
| gamename      = [[Penn–Princeton basketball rivalry|Penn–Princeton Rivalry]]
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| tv            = FiOS1/[[The Comcast Network|TCN]]
| score         = 62–59
| overtime      = OT
| attend        = 3,840
| record        = 17–4 (5–0)
}}
{{CBB schedule entry
| date          =  February 11, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = [[Columbia Lions men's basketball|Columbia]]
| opprank       = 
| site_stadium  = [[Levien Gymnasium]]
| site_cityst   = [[New York, NY]]
| tv            = 
| score         = 76–46
| overtime      =
| attend        = 1,953
| record        = 18–4 (6–0)
}}
{{CBB schedule entry
| date          =  February 12, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = [[Cornell Big Red men's basketball|Cornell]]
| opprank       = 
| site_stadium  = [[Newman Arena]]
| site_cityst   = [[Ithaca, NY]]
| tv            = 
| score         = 57–55
| overtime      =
| attend        = 4,087
| record        = 19–4 (7–0)
}}
{{CBB schedule entry
| date          =  February 18, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = Yale
| opprank       = 
| site_stadium  = [[Payne Whitney Gym]]
| site_cityst   = [[New Haven, CT]]
| tv            = [[YES Network|YES]]
| score         = 58–51
| overtime      =
| attend        = 1,935
| record        = 20–4 (8–0)
}}
{{CBB schedule entry
| date          =  February 19, 2011
| time          = 3:00&nbsp;pm
| w/l           = l
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = Brown
| opprank       = 
| site_stadium  = [[Pizzitola Sports Center]]
| site_cityst   = [[Providence, RI]]
| tv            = 
| score         = 65–75
| overtime      =
| attend        = 1,407
| record        = 20–5 (8–1)
}}
{{CBB schedule entry
| date          =  February 25, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = 
| neutral       =
| rank          = 
| opponent      = Cornell
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| tv            = FiOS1
| score         = 84–66
| overtime      =
| attend        = 3,864
| record        = 21–5 (9–1)
}}
{{CBB schedule entry
| date          =  February 26, 2011
| time          = 6:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = 
| neutral       =
| rank          = 
| opponent      = Columbia
| opprank       = 
| site_stadium  = Jadwin Gymnasium
| site_cityst   = Princeton, NJ
| tv            = 
| score         = 66–61
| overtime      =
| attend        = 4,412
| record        = 22–5 (10–1)
}}
{{CBB schedule entry
| date          =  March 4, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = Dartmouth
| opprank       = 
| site_stadium  = [[Leede Arena]]
| site_cityst   = [[Hanover, NH]]
| tv            = 
| score         = 77–55
| overtime      =
| attend        = 978
| record        = 23–5 (11–1)
}}
{{CBB schedule entry
| date          =  March 5, 2011
| time          = 7:00&nbsp;pm
| w/l           = l
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = Harvard
| opprank       = 
| site_stadium  = [[Lavietes Pavilion]]
| site_cityst   = [[Boston, MA]]
| tv            = [[ESPN3]]
| score         = 67–79
| overtime      = 
| attend        = 2,195
| record        = 23–6 (11–2)
}}
{{CBB schedule entry
| date          =  March 8, 2011
| time          = 7:00&nbsp;pm
| w/l           = w
| nonconf       =
| homecoming    =
| away          = yes
| neutral       =
| rank          = 
| opponent      = Penn
| opprank       = 
| gamename      = Penn–Princeton Rivalry
| site_stadium  = [[Palestra]]
| site_cityst   = [[Philadelphia, PA]]
| tv            = ESPN3
| score         = 70–58
| overtime      =
| attend        = 4,679
| record        = 24–6 (12–2)
}}
|-
!colspan= 9|Ivy League Playoff
{{CBB schedule entry
| date          =  March 12, 2011
| time          = 4:00&nbsp;pm 
| w/l           = w
| nonconf       = 
| homecoming    = 
| away          = 
| neutral       = yes
| rank          = 
| opponent      = Harvard
| opprank       = 
| site_stadium  = Payne Whitney Gymnasium
| site_cityst   = New Haven, CT
| tv            = ESPN3
| score         = 63–62
| overtime      = 
| attend        = &nbsp;
| record        = 25–6
}} 
|-
!colspan= 9|[[2011 NCAA Men's Division I Basketball Tournament]]
{{CBB schedule entry
| date          =  March 17, 2011
| time          = 2:45&nbsp;pm
| w/l           = l
| nonconf       = yes
| homecoming    = 
| away          = 
| neutral       = yes
| rank          = 
| opponent      = [[2010-11 Kentucky Wildcats men's basketball team|Kentucky]]
| opprank       = 11
| site_stadium  = [[St. Pete Times Forum]]
| site_cityst   = [[Tampa, FL]]
| gamename      = Second Round
| tv            = [[College Basketball on CBS|CBS]]
| score         = 57–59
| overtime      = 
| attend        = 14,835
| record        = 25–7
}} 
{{CBB schedule end
| timezone = [[Eastern Time Zone (Americas)|Eastern Time]]}}

==Honors==

===In season===
Ian Hummer was a [[National Association of Basketball Coaches]] First Team All-District selection, and Kareem Maddox was a Second Team selection.<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/weeklyreleases/2010-11_MBB_Final_Release.pdf|title=2010–11 Highlights|website=IvyLeagueSports.com|page=3}}</ref> Each week the Ivy League selects a player of the week and a rookie of the week.<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/weeklyreleases/2010-11_MBB_Release_Week_18.pdf|title=2010–11 Ivy League MEN’S BASKETBALL|accessdate=March 8, 2011|date=March 7, 2011|website=IvyLeagueSports.com}}</ref>
{| class="wikitable"
|
!colspan=4| '''Player of the Week'''
|-
| align="center" style="background:#f0f0f0;"|'''Date'''
| align="center" style="background:#f0f0f0;"|'''Name'''
| align="center" style="background:#f0f0f0;"|'''School'''
| align="center" style="background:#f0f0f0;"|'''Class'''
| align="center" style="background:#f0f0f0;"|'''Position'''
|-
| December 6, 2010||[[Ian Hummer]]||Princeton||So.||F
|-
| December 13, 2010||Kareem Maddox||Princeton||Sr.||F
|-
| December 20, 2010||Kareem Maddox||Princeton||Sr.||F
|-
| February 28, 2011||Kareem Maddox||Princeton||Sr.||F
|-
|}

===Postseason honors===
The league selected its postseason awards on March 9.<ref>{{cite web|url=http://www.ivyleaguesports.com/sports/mbkb/2010-11/releases/Mens_Basketball_All-Ivy_--_2010-11|title=Men's Basketball All-Ivy – 2010–11|accessdate=March 12, 2011|date=March 9, 2011|website=IvyLeagueSports.com}}</ref><br>
'''Defensive Player of the Year''': Kareem Maddox, Princeton (Sr., F, [[Oak Park, CA]])<br>
'''All-Ivy League''' (ALL CAPS: Unanimous)<br>
:First Team All-Ivy: KAREEM MADDOX, Princeton (Sr., F, Oak Park, CA)
:Second Team All-Ivy: [[Ian Hummer]], Princeton (So., F, [[Vienna, VA]]); Dan Mavraides, Princeton (Sr., G, [[San Mateo, CA]])

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://espn.go.com/mens-college-basketball/team/_/id/163/princeton-tigers Princeton Tigers] @ [[ESPN.com]]
*[http://espn.go.com/mens-college-basketball/team/stats/_/id/163/year/2011/princeton-tigers 2011 stats] @ [[ESPN.com]]

{{Princeton Tigers men's basketball navbox}}

{{DEFAULTSORT:2010-11 Princeton Tigers Men's Basketball Team}}
[[Category:Princeton Tigers men's basketball seasons]]
[[Category:2010–11 Ivy League men's basketball season|Princeton Tigers]]
[[Category:2011 NCAA Men's Division I Basketball Championship participants|Princeton]]