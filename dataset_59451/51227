{{Infobox journal
| title = Epigraphia Zeylanica
| cover = [[File:Epigraphia Seylanica Vol I low res cover.png]]
| editor = 
| discipline = [[Archeology]]
| former_names = 
| abbreviation = Epigr. Zeylan.
| publisher = [[Department of Archaeology (Sri Lanka)|Archaeological Department]]
| country = [[Sri Lanka]]
| frequency = Irregular
| history = 1904-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = 
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 499641639
| LCCN = 
| CODEN = 
| ISSN = 
| eISSN =
}}
'''''Epigraphia Zeylanica''''' is an irregularly published series that deals with [[epigraphy|epigraphs]] and other records from ancient [[Ceylon]].<ref>{{cite book|last=Samaraweera|first=Vijaya|title=Sri Lanka|year=1987|publisher=Clio Press|isbn=9780903450331|page=152|chapter=572: ''Epigraphica Zeylanica, being lithic and other inscriptions of Ceylon''.|quote=This periodical presents to the reader inscriptions - an invaluable source for the reconstruction of Sri Lanka's past - as they are discovered, transliterated, and translated by the staff of the Archaeological Survey. Thus far, five complete volumes have been published and the sixth volume's first number was issued in 1973.|series=World bibliographical series|volume=20}}<!--|accessdate=3 December 2012--></ref> Established in 1904, the series' contents range from individual articles and notes on inscriptions to single-author monographs. Over the last century, ''Epigraphia Zeylanica'' has functioned as a key source for the history of ancient Ceylon and its early epigraphic records.

==Publication history==
Volumes 1 and 2 were edited by [[Don Martino de Zilva Wickremasinghe]] and were extensively reviewed in ''[[The Journal of the Royal Asiatic Society of Great Britain and Ireland]]'' by [[E. Müller]].<ref>{{cite journal|last=Müller|first=E|title=Notices of Books: Archæological Survey of Ceylon: Epigraphia Zeylanica, Vol. i, Part 1 by Don Martino de Zilva Wickremasinghe|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=January 1905|pages=183–186|jstor=25208746|issn=0035-869X}}</ref><ref>{{cite journal|last=Müller|first=E.|title=Archæological Survey of Ceylon. Epigraphia Zeylanica, Vol. i, Parts 2 and 3 by Don Martino de Zilva Wickremasinghe|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=October 1907|pages=1096–1098|jstor=25210513}}</ref><ref>{{cite journal|last=Müller|first=E.|title=Epigraphia Zeylanica. Vol. I, Part IV by Martino de Zilva Wickremasinghe|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=April 1909|pages=538–541|jstor=25210777}}</ref><ref>{{cite journal|last=Müller|first=E.|title=Epigraphia Zeylanica, Being Lithic and Other Inscriptions of Ceylon. Vol. I, Part V by Don Martino de Zilva Wickremasinghe|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=April 1912|pages=514–517|jstor=25190054}}</ref><ref>{{cite journal|last=Müller|first=E.|title=Epigraphia Zeylanica, Vol. II, Pts. I, II by Don Martino de Zilva Wickremasinghe|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=January 1915|pages=167–172|jstor=25189298}}</ref>

Volume 3, covering years 1928 to 1933, was published in 1933.<ref>{{cite journal|last=Blagden|first=C. O.|title=Epigraphia Zeylanica, Being lithic and Other Inscriptions of Ceylon by S. Paranavitana|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=July 1935|issue=3|pages=550–551|jstor=25201202}}</ref> After Wickremasinghe's retirement as editor, the editing of this volume was passed to [[Senarath Paranavitana]].<ref>{{cite journal|last=Paranavitana|chapter="Preface"|title=Epigraphia Zeylanica|volume=3|year=1933|pages=iii.}}</ref>

Volume 4, covering years 1934 to 1941, was published in 1943. It was edited by Senarath Paranavitana and [[Humphry William Codrington]].

Volume 5 was published in three parts, with part 1 appearing in 1955 under the editorship of Paranavitana,<ref>{{cite journal|last=Godakumbura|first=C. E.|title=Epigraphia Zeylanica. Vol. V, Part I by S. Paranavitana|journal=The Journal of the Royal Asiatic Society of Great Britain and Ireland|date=October 1956|issue=3/4|pages=237–241|jstor=25222884}}</ref> part 2 in 1963 under the joint-editorship of Paranavitana with C. E. Godakumbura,<ref>{{cite journal|last=Reynolds|first=C. H. B.|title=Epigraphia Zeylanica, Being Lithic and Other Inscriptions of Ceylon. Vol. V, Pt. 2 by Ceylon. Archaeological Survey|journal=Bulletin of the School of Oriental and African Studies, University of London|year=1964|volume=27|issue=3|pages=635|jstor=611406|doi=10.1017/s0041977x00118531}}</ref> and part 3 in the 1965 under the same editors. An auxiliary part, consisting of a preface and index, was published in 1966, again under the same two editors.

Volume 6 was published in multiple parts under the editorship of Senarath Paranavitana, with part 1 appearing in 1973.

Volume 7 was published in 1984, edited by Saddhamangala Karunaratne.

Volume 8 was published in 2001 as the single author monograph ''The growth of Buddhist monastic institutions in Sri Lanka from Brāhmī inscriptions'', by Mālinī Ḍayas.<ref>{{cite book|title=The growth of Buddhist monastic institutions in Sri Lanka from Brāhmī inscriptions|last=Ḍayas|first=Mālinī|series=Epigraphia Zeylanica|volume=VIII|year=2001|publisher=Dept. of Archaeological Survey, Govt. of Sri Lanka|place=Colombo|url=http://openlibrary.org/works/OL3431806W/The_growth_of_Buddhist_monastic_institutions_in_Sri_Lanka_from_Bra%CC%84hmi%CC%84_inscriptions|isbn=9789559264040}}</ref>

== See also ==
* ''[[Epigraphia Carnatica]]''
* ''[[Epigraphia Indica]]''

== References ==
{{Reflist|30em}}

==External links==
* [http://hdl.handle.net/2027/mdp.39015030386539 Hathi Trust: Full text - Volume 1, 1904-1912]
* [http://hdl.handle.net/2027/mdp.39015027649477 Hathi Trust: Limited view/searchable - Volume 2, 1912-1927]
* [http://hdl.handle.net/2027/mdp.39015033271308 Hathi Trust: Limited view/searchable - Volume 6 Part 1, 1973]
* [http://hdl.handle.net/2027/mdp.39015066290134 Hathi Trust: Limited view/searchable - Volume 7, 1984]

[[Category:Sri Lanka inscriptions]]
[[Category:History of Sri Lanka]]
[[Category:Archaeology journals]]
[[Category:Publications established in 1904]]
[[Category:English-language journals]]