{{Infobox Journal
| title = Biotechnology and Applied Biochemistry
| cover = [[File:Biotech Appl Biochem Jul-Aug 2012.gif]]
| discipline = [[Biotechnology]], [[biochemistry]]
| abbreviation = Biotechnol. Appl. Biochem.
| formernames = Journal of Applied Biochemistry
| publisher = [[Wiley-Blackwell]] on behalf of the [[International Union of Biochemistry and Molecular Biology]]
| country =
| frequency = Bimonthly
| history = 1979–present
| impact = 1.429
| impact-year = 2015
| openaccess = [[Hybrid open access journals|Hybrid]]
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1470-8744
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1470-8744/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1470-8744/issues
| link2-name = Online archive
| ISSN = 0885-4513
| eISSN = 1470-8744
| ISSN2 = 0161-7354
| ISSN2label = Journal of Applied Biochemistry:
| CODEN = BABIEC
| OCLC = 12644720
| LCCN = 87640639
}}
'''''Biotechnology and Applied Biochemistry''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering [[biotechnology]] applied to [[medicine]], [[veterinary medicine]], and [[diagnostics]]. Topics covered include the expression, extraction, purification, formulation, stability and characterization of both natural and recombinant biological molecules. It is published by [[Wiley-Blackwell]] on behalf of the [[International Union of Biochemistry and Molecular Biology]]. The [[editors-in-chief]] are Gianfranco Gilardi ([[University of Torino]]) and Jian-Jiang Zhong ([[Shanghai Jiao Tong University]]).

==History==
The journal was established in 1979 under the title ''Journal of Applied Biochemistry'' by [[Academic Press]], obtaining its present title in 1986.<ref name="J Appl Biochem">{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7908148 |title=Journal of Applied Biochemistry |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-09-29}}</ref>

Former editors-in-chief include Peter Campbell ([[University College London]]; before 1996), Roger Lundblad (formerly of Baxter Biotech, [[Duarte, California]]; 1996–2002), and Parviz A. Shamlou ([[Eli Lilly and Company|Eli Lilly]]; 2003–2012).

==Abstracting and indexing==
The journal is abstracted and indexed by:
{{columns-list|colwidth=30em|
*[[Biochemistry & Biophysics Citation Index]]
*[[Biological Abstracts]]<ref name=BA>{{cite web |url=http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=BA |title=Biological Abstracts - Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-09-29}}</ref>
*[[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-09-29}}</ref>
*[[Biotechnology Citation Index]]
*[[CABI (organisation)|CABI databases]]<ref name=CABAB>{{cite web |url=http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title=Serials cited |work=[[CAB Abstracts]] |publisher=[[CABI (organisation)|CABI]] |accessdate=2016-09-29}}</ref>
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-09-29}}</ref>
*[[Compendex]]<ref name=Compendex>{{cite web |url=http://www.elsevier.com/online-tools/engineering-village/contentdatabase-overview |title=Content/Database Overview - Compendex Source List |publisher=[[Elsevier]] |work=Engineering Village |accessdate=2016-09-29}}</ref>
*[[Current Contents]]/Agriculture, Biology & Environmental Sciences<ref name=ISI/>
*Current Contents/Life Sciences<ref name=ISI/>
*[[EBSCO Information Services|EBSCO databases]]
*[[Elsevier Biobase]]
*[[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2016-09-29}}</ref>
*[[Index medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8609465 |title=Biotechnology and Applied Biochemistry |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-09-29}}</ref>
*[[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2016-09-29}}</ref>
*[[ProQuest|ProQuest databases]]
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-09-29}}</ref>}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.429.<ref name=WoS>{{cite book |year=2016 |chapter=Biotechnology and Applied Biochemistry |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist|30em}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1470-8744}}

[[Category:Publications established in 1979]]
[[Category:Biochemistry journals]]
[[Category:Biotechnology journals]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]