{{Orphan|date=March 2014}}

'''Mike Grandmaison''' (born 1954) is a [[Canadian]] nature photographer specializing in landscapes, plants and wildlife. Grandmaison is well known for his exquisite images created from his beloved country Canada. His commercial assignment photography focuses on architecture, agriculture, nature, the environment, travel and Canadian tourism while his extensive and eclectic stock photography images are licensed through his own websites as well as through stock agencies in North America. Grandmaison markets his fine art photographs principally through The Canadian Gallery.

== Early life and education ==

Mike Grandmaison was born in [[Greater Sudbury|Sudbury]], [[Ontario]] (Canada) on April 25, 1954 from Jean-Marie and Therese (born Emery) Grandmaison. He studied biology and graduated from [[Laurentian University]] in Sudbury with an Honors Bachelor of Science Degree (Hon. B.Sc.) in 1972–1976.<ref>Nault, Jennifer. 'Natural Worlds'. Laurentian, Winter 2008.</ref> During the summer periods, Grandmaison worked at various jobs including as a laborer at Falconbridge Nickel Mines and Ecole Secondaire MacDonald-Cartier, as well as a biology student for [[McMaster University]], Laurentian University and the [[Ontario Ministry of the Environment]]. He also managed a small camera store, Canadian Camera Center, in [[Sturgeon Falls]], Ontario for a brief period in 1977–1978.

== Biology career ==

Upon graduation from Laurentian University in 1976, Grandmaison worked on short-term contracts for Laurentian University as a Herbarium Assistant and for the Ontario Ministry of the Environment as a field assistant in Water Quality. In late 1978, Grandmaison moved to [[Edmonton]], [[Alberta]] in search of permanent work. He found work as a Research Technician in Air Pollution Biology for Alberta Environment in Edmonton (1979–1980) and then as a Water Quality Technician for Alberta Environment, also in Edmonton (1980). Grandmaison was then hired as an Insect & Disease Technician for the [[Canadian Forest Service]] at the Northern Forestry Centre in Edmonton in 1980–1985 after which he transferred to [[Winnipeg]], [[Manitoba]] to assume similar duties in 1985–1996. He left the Canadian Forest Service in April 1996 to pursue a career as a full-time, freelance professional photographer.

== Introduction to photography ==

Grandmaison remembers taking his first "real picture" with his mother's Kodak for a school project in grade 11 (circa 1968) when he photographed the old wooden barn on his aunt's farm in Estaire, Ontario using black & white print film. That was the extent of his photography until his graduation from university in 1976. Grandmaison's interest in photography began soon after he bought his first 35mm camera, a Nikkormat EL. Being primarily interested in botany and ecology, his objective was to make photographic records of plants and the environments they grew in. Grandmaison's interest in photography ignited when he joined the Sturgeon Falls Camera Club where he first learned the craft of black & white photography.

Shortly after moving to Edmonton, Alberta, Mike Grandmaison joined Images Alberta Camera Club (IACC) where he took on the roles of Newsletter Editor, 2nd Vice President and Programs Chairman.<ref>Smyth, Kathleen. 'Nature's Masterpiece'. Photo Imaging, Vo. 4, No. 5. September–October 2002</ref> He then joined NAPA, the National Association for Photographic Arts (now called Canadian Association for Photographic Arts (CAPA) and volunteered for the positions of district representative for Edmonton, prairie sone director, vice president and finally president. As a member of NAPA, Grandmaison met a number of excellent professional photographers like [[Freeman Patterson]], [[Ernst Haas]], [[Frans Lanting]], John Shaw, [[George Tice]], [[Paul Caponigro]], Courtney Milne, Pat O'Hara, Craig Richards and John Netherton to name a few. Seeing the works of these photographers first hand, and listening to their approaches and their stories, was invaluable. Two of those photographers, Freeman Patterson and Ernst Haas, would have a profound influence on his photography.<ref>Gosselin, Christine. 'Portrait: Grandmaison'. Liaison, No. 142, Hiver 2008–2009.</ref> Grandmaison immersed himself in anything that pertained to photography and he cites [[Brett Weston]], [[Minor White]] and [[Harry Callahan (photographer)|Harry Callahan]] as important black & white influences.

== Photography career ==

Grandmaison's photography career involved commercial photography as well as leasing his stock photographs that he made throughout Canada.<ref>Van Overbeek, Andrea. 'Taking Stock: The Photography of Mike Grandmaison'. enVision, Vol. 2, 2003.</ref> While he worked with stock agencies in North America to market his work, most of his photographs were licensed though his own marketing efforts. His stock photography has been published in a variety of markets including advertising, annual reports, books, brochures, calendars, cards, CDs, magazines, posters, trade shows and the web. His images have appeared in hundreds of calendars, often as single-photographer editions. His photographs have also graced numerous magazines including ''[[Audubon (magazine)|Audubon]]'', ''[[Canadian Gardening]]'', ''[[Canadian Geographic]]'', ''Canadian Wildlife'', ''[[Cottage Life]]'', ''Country'', ''Farm Forum'', ''[[GEO (magazine)|Geo]]'', ''[[L'Actualite]]'', ''[[Macleans]]'', ''[[National Geographic Traveler]]'', ''[[Nature's Best]]'', ''Outdoor Photography Canada'', ''Photo Life'', ''Prairies North'', ''[[Ranger Rick]]'', ''[[Sierra Club]]'' and ''[[Time Canada]]'' to name a few. In 2000, Grandmaison photographed his hometown Sudbury in Ontario for Canadian Geographic Magazine to illustrate the efforts and changes that a 'reclamation project' had on the area.<ref>Lee, David. 'Green Rebirth'. Canadian Geographic, May - June 2000</ref> Grandmaison has illustrated a regular feature called 'Discovering Canada' in each issue of ''Outdoor Photography'' since its inception in 2007.<ref>Grandmaison, Mike. 'Tall Grass Prairie'. Outdoor Photography Canada, Winter 2014.</ref>
Architecture has always attracted Grandmaison's eye and he photographed numerous architectural projects in Winnipeg for some of the largest firms including Smith Carter Architects and Engineers and LM Architectural Group. Some of the interesting buildings that he photographed include the [[Canadian Science Centre for Human and Animal Health]], the [[MTS Center]], Cancer Care Manitoba, the St. Boniface Cathedral and the [[Canadian Museum for Human Rights]].

As a result of Grandmaison's training in the biological sciences, it was natural progression for him to focus his lens on the prairie's main industry, agriculture. He has photographed agriculture for most major companies and organizations involved in Canadian agriculture: Agriculture & Agri-Food Canada, [[Agricore]], [[BASF]], BrettYoung, [[Canadian Wheat Board]], [[Cargill]], Dairy Farmers of Manitoba, [[DuPont]], [[Farm Credit Canada]], MacDon, Manitoba Agriculture, [[Monsanto Canada]], Paterson Global Foods, the [[Saskatchewan Wheat Pool]] and [[United Grain Growers]].

Grandmaison's photographs of the natural world have been published in a number of books featuring different regions of his beloved country Canada. In 2005, he released 'Canada' which quickly became a national bestseller and in which he collaborated with CBC radio broadcaster [[Shelagh Rogers]] who wrote the text.<ref>Waterhouse, Kelly. 'The Limelight Is The Sweetlight – Canada by Mike Grandmaison'. enVision, Vol. 3, 2005.</ref><ref>Walker, Morley. 'Capturing Canada'. The Winnipeg Free Press. April 21, 2005.</ref> Grandmaison's publisher at the time, [[Key Porter Books]], commissioned him to produce three other titles: 'The Canadian Rockies' (2007, Introduction by Ben Gadd), 'Georgian Bay' (2008, Introduction by Gerard Courtin) and 'Muskoka' (2010). Following the demise of Key Porter Books, Grandmaison was then invited by [[Turnstone Press]] to publish a book about his adopted home, the [[Canadian Prairies]] called 'Prairie and Beyond'(2012, Text by Jan Volney).<ref>Grandmaison, Mike. 'Prairie and Beyond'. Photo News, Vol. 21, No. 1, Spring 2012.</ref><ref>Hughes, Lionel. 'Prairie In Context'. Prairies North, Summer 2012.</ref>
Grandmaison's nature images are purchased as fine art pieces, for corporate decor and as corporate gifts. His fine art photographs may be viewed online at 'The Canadian Gallery'.<ref>Grandmaison, Mike. 'Prairie and Beyond'. Photo News, Vol. 21, No. 1, Spring 2012.</ref><ref>Grandmaison, Mike. 'The Canadian Gallery'. Friesens, 2012.</ref>

== Books ==
* ''Mike Grandmaison's Prairie and Beyond'' (2012, Turnstone Press) ISBN 978-0-88801-393-4
* ''Muskoka'' (2010, Key Porter Books). ISBN 978-1-55470-260-2
* Canada: Seasons of Wonder (2010, Key Porter Books) ISBN 978-1-55267-655-4
* ''Georgian Bay'' (2008, Key Porter Books) ISBN 978-1-55263-924-5
* ''The Canadian Rockies'' (2007, Key Porter Books) ISBN 978-1-55263-836-1
* ''Canada'' (2004, Key Porter Books) ISBN 978-1-55470-219-0
* ''Healing the Landscape'' (2001, VETAC – with Don Johnston) ISBN 978-0-9687858-1-2
* London: Claiming the Future (2000, Community Communications Inc.) ISBN 1-58192-024-5
* Hamilton: A New City for a New Millennium (2000, Community Communications Inc.) ISBN 1-58192-025-3
* Canada's Technology Triangle (1999, Community Communications Inc.) ISBN 1-885352-91-3
* Winnipeg: A Prairie Portrait (1998, Community Communications Inc.) ISBN 1-885352-88-3
* A Singular View: Fine Art Photographs by Mike Grandmaison (2012, Friesens) ISBN 978-0-9878890-1-0
* The Canadian Gallery. (2012, Friesens) ISBN 978-0-9878890-0-3

== Postage stamps ==

Grandmaison's images have appeared on eight [[Canada Post]] stamp projects including the "Canadian Museum for Human Rights" (2014), the "Red River Settlement" (2012), the "Lighthouse" series (December 2007), the "University of Manitoba" (2002), "Christmas Lights Series" (2001) and the "Pan-American Games" (1999).

== Workshops and lectures ==

Grandmaison has presented seminars and lectures to various groups and organizations on a variety of photography topics throughout Canada. He has conducted numerous photography workshops throughout Canada, including the Latow Photographers Guild in Burlington, Ontario; the Niagara School of Imaging in St. Catharines, Ontario; 'Focus on the Rockies' in Jasper National Park, Alberta; Praxis Photographic Workshops in Winnipeg and Hecla in Manitoba as well as in Sioux Narrows, Ontario; the Abbotsford Photo Arts Club in Abbotsford, British Columbia; Camera Canada College in Brandon and Winnipeg, Manitoba; Word On The Water Book Festival in Kenora, Ontario; and The Winnipeg International Writers Festival in Winnipeg, Manitoba.

== Fine art photography ==

Mike Grandmaison's fine art photographs are represented by:
* The Canadian Gallery (Winnipeg, Manitoba)
* The Birchwood Gallery (Winnipeg, Manitoba)
* Image One Design (Kenora, Ontario)

== Exhibitions ==

'''Solo exhibitions'''

* 2013 October - November. 'Natural Reflections'. Tangled Tree, Kenora, Ontario. 
* 2012–2014.	'Prairie and Beyond'. Two separate exhibitions of 12 and 26 prints are traveling to communities in Manitoba over a 2-year period. Collaboration with The Manitoba Arts Network.
* 2012 September. 'Prairie and Beyond' - 38 large-format prints. Pixels 2.1 Photography Gallery. Winnipeg, Manitoba.
* 2012 May - June. 'Prairie and Beyond' - 16 prints. Small Works Gallery. Winnipeg, Manitoba.
* 1983 'Subtle Images' - 38 prints. John Janzen Nature Centre. Edmonton, Alberta.
* 1982 'Subtle Images' - 38 prints. Edmonton Public Library Gallery. Edmonton, Alberta

'''Group exhibitions'''

* 2011	Group Exhibition at Pixels 2.1 Gallery. Winnipeg, Manitoba, Manitoba. 'Twelve Manitoba Photographers'. Displayed 6 pieces. 
* 2005	A Passion for Canada. A selection of 15 photographs. The Lab Works. 
* 1996	Group Exhibition at Camera Canada College '96. National Association for Photographic Art. Winnipeg, Manitoba  
* 1994 	Group Exhibition with Nature Photo Group. Winnipeg, Manitoba 
* 1991	Group Exhibition with Nature Photo Group. Winnipeg, Manitoba 
* 1991	Group Exhibition with Images Alberta Camera Club. Edmonton, Alberta

== Recognition ==

* Nominated for two 2013 Manitoba Book Awards for 'Prairie and Beyond' for 'Best Illustrated Book of the Year' and the 'Mary Scorer Award for Best Book by a Manitoba Publisher'.
* Received a 2012 PIA Certificate of Merit in the category 'Art Books' in the 2012 Premier Print Award Competition for 'Prairie and Beyond'
* Received an 'Honorary Membership' (Hon FCAPA) for Recognition of Distinguished Contribution to the Association (Canadian Association for Photographic Art). Aug. 31, 2012.
* Received a 'Certificate of Photographic Achievement' for Recognition of Photographic Contribution to the Association (Canadian Association for Photographic Art) for service as Past President. August 31, 2012.
* 'Honorary Patron' of the Friends of Lake Laurentian. Sudbury, Ontario. 2012.

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref>Exhibitions Without Walls. 2013. Interview with Fine Art Photographer Mike Grandmaison</ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.grandmaison.mb.ca Official website]

{{DEFAULTSORT:Grandmaison, Mike}}
[[Category:Articles created via the Article Wizard]]
[[Category:Living people]]
[[Category:1954 births]]
[[Category:Canadian photographers]]
[[Category:People from Greater Sudbury]]
[[Category:Laurentian University alumni]]
[[Category:Franco-Ontarian people]]