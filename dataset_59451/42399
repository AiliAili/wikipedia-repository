<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= EA-18G Growler
 |image= EA-18G VX-31 over Ridgecrest CA 2009.jpg
 |caption= An EA-18G Growler of VX-31 overflies [[Ridgecrest, California|Ridgecrest]], California as it returns to NAWS China Lake at the conclusion of a test mission
}}{{Infobox aircraft type
 |type= [[Electronic warfare aircraft]]
 |national origin= [[United States]]
 |manufacturer= [[Boeing]]
 |designer=
 |first flight= 15 August 2006
 |introduced= 22 September 2009<ref name=EA-18G_IOC/>
 |retired=
 |number built= <!--cite if you change this number-->100, as of May 2014<ref>{{cite web |url= http://boeing.mediaroom.com/Boeing-Delivers-100th-EA-18G-Growler-to-US-Navy |title= Boeing Delivers 100th EA-18G Growler to US Navy |publisher= Boeing |date= May 5, 2014}}</ref>
 |status= In service
 |primary user= [[United States Navy]]
 |more users= [[Royal Australian Air Force]]
 |produced= 2004–present
 |unit cost= [[United States dollar|US$]]68.2&nbsp;million ([[flyaway cost]], FY2012)<ref name=usn_fy2013_budget>[http://www.finance.hq.navy.mil/FMB/13pres/APN_BA1-4_BOOK.pdf "Fiscal Year (FY) 2013 President's Budget Submission: Navy Justification Book Volume 1 Aircraft Procurement, Navy Budget Activities 1–4."] ''U.S. Department of Defense'', February 2012. Retrieved: 24 March 2012.</ref>
 |developed from= [[Boeing F/A-18E/F Super Hornet|Boeing F/A-18F Super Hornet]]
 |variants with their own articles =
}}
|}

The '''Boeing EA-18G Growler''' is an American [[aircraft carrier|carrier-based]] [[electronic warfare aircraft]], a specialized version of the two-seat [[Boeing F/A-18E/F Super Hornet|F/A-18F Super Hornet]]. The EA-18G replaced the [[Northrop Grumman EA-6B Prowler]]s in service with the [[United States Navy]]. The Growler's [[electronic warfare]] capability is primarily provided by [[Northrop Grumman]]. The EA-18G began production in 2007 and entered operational service in late 2009.

==Development==
{{See also|Boeing F/A-18E/F Super Hornet#Development}}

===Requirement and testing===
[[File:EA-18G 060803-F-0000X-003.jpg|thumb|left|The first EA-18G at the roll-out ceremony on 3 August 2006]]

On 15 November 2001, Boeing successfully completed an initial flight demonstration of F/A-18F "F-1" fitted with the [[ALQ-99]] electronic warfare system to serve as the EA-18 Airborne Electronic Attack (AEA) concept aircraft.<ref>[http://boeing.mediaroom.com/2001-11-15-Boeing-Successfully-Completes-Initial-Flight-Demonstration-of-EA-18-Airborne-Electronic-Attack-Variant "Boeing Successfully Completes Initial Flight Demonstration of EA-18 Airborne Electronic Attack Variant."]''Boeing,'' 15 November 2001. Retrieved: 15 July 2011.</ref>  In December 2003, the US Navy awarded a development contract for the EA-18G to Boeing. As primary contractor, Boeing was to construct the forward fuselage, wings and perform the final assembly. Northrop Grumman was the principal airframe subcontractor and they would supply the center and aft fuselage as well as the principal electronic combat system.<ref name=EA-18G_IOC>{{citation|url=http://www.navair.navy.mil/index.cfm?fuseaction=home.PrintNewsStory&id=4231|title=EA-18G achieves Initial Operational Capability|author=U.S. Navy|date=2 December 2009|archiveurl=https://web.archive.org/web/20160108070729/http://www.navair.navy.mil/index.cfm?fuseaction=home.PrintNewsStory&id=4231|archivedate=8 January 2016}}</ref><ref name="9.6 Billion">[http://boeing.mediaroom.com/2003-12-29-Navy-Awards-Boeing-9.6-Billion-in-Super-Hornet-and-EA-18G-Contracts "Navy Awards Boeing $9.6&nbsp;Billion in Super Hornet and EA-18G Contracts."] ''Boeing,'' 29 December 2003. Retrieved: 15 July 2011.</ref> In 2003, the Navy expected to receive 90 EA-18Gs.<ref name="N239a.ppt">[http://www.dtic.mil/dticasd/sbir/sbir043/n239a.ppt "Next Generation Jammer."] ''DTC.mil'', 9 October 2003.</ref>

The first EA-18G test aircraft entered production on 22 October 2004.<ref>[http://boeing.mediaroom.com/2004-10-21-Boeing-Begins-Work-on-First-EA-18G-Test-Aircraft "Boeing Begins Work on First EA-18G Test Aircraft."] ''Boeing,'' 21 October 2004. Retrieved: 15 July 2011.</ref> The first test aircraft, known as EA-1, was rolled out on 3 August 2006, before making its [[maiden flight]] at St. Louis on 15 August 2006; it was later ferried to [[Naval Air Station Patuxent River]], Maryland on 22 September 2006.<ref>[http://www.boeing.com/news/frontiers/archive/2006/september/cover.pdf "Boeing rolls out first EA-18G Growler."] ''Boeing,'' 4 August 2006.</ref><ref>{{cite web |url= http://www.boeing.com/news/releases/2006/q3/060816a_nr.html |title= Boeing Flies EA-18G Growler for First Time |website= Boeing |archiveurl= https://web.archive.org/web/20061109072514/http://www.boeing.com/news/releases/2006/q3/060816a_nr.html |archivedate= 9 November 2006 |date= 16 August 2006 }}</ref> EA-1 primarily supports ground testing in the Air Combat Environment Test and Evaluation Facility (ACETEF) [[anechoic chamber]].

The second aircraft (EA-2) first flew on 10 November 2006,<ref>{{cite web |url= http://www.boeing.com/news/releases/2006/q4/061113a_nr.html |title= Second Boeing EA-18G Growler Takes to the Air |archiveurl= https://web.archive.org/web/20071023031218/http://boeing.com/news/releases/2006/q4/061113a_nr.html |archivedate= 2007 |website= Boeing |date= 13 November 2006 |accessdate= 15 July 2011 }}</ref> and was delivered to NAS Patuxent River on 29 November 2006.<ref>{{cite web |url= http://www.boeing.com/news/releases/2006/q4/061129a_nr.html |title= Boeing Delivers Second EA-18G Growler to U.S. Navy |archiveurl= https://web.archive.org/web/20071023031459/http://boeing.com/news/releases/2006/q4/061129a_nr.html |archivedate= 23 October 2007 |website= Boeing |accessdate= 15 July 2011 }}</ref>  EA-2 is an AEA flight test aircraft, initially flying on Pax River's Atlantic Test Range (ATR) for developmental test of the AEA system before transitioning to the Electronic Combat Range (ECR, or 'Echo Range') in [[Naval Air Weapons Station China Lake]] in California. Both aircraft are assigned to [[VX-23]] "Salty Dogs". EA-1 and EA-2 are F/A-18Fs F-134 and F-135, pulled from the St. Louis production line and modified by Boeing to the EA-18G configuration. However, since they were not built initially as Growlers, the Navy has designated these two test aircraft as NEA-18Gs.<ref>Parsch, Andreas. [http://www.designation-systems.net/usmilav/412015-L(addendum).html "DOD 4120.15-L – Addendum."] ''Designation-Systems.net,'' 8 July 2008. Retrieved: 15 July 2011.</ref>  There were five Growlers flying in the flight test program as of June 2008.<ref>{{ cite web |url= http://www.boeing.com/news/releases/2008/q2/080604b_nr.html |title= Boeing Delivers 1st Fleet EA-18G Growler |archiveurl= https://web.archive.org/web/20080607005138/http://www.boeing.com/news/releases/2008/q2/080604b_nr.html |archivedate= 7 June 2008 |website= Boeing |date= 4 June 2008 }}</ref>

===Procurement===
[[File:US Navy 070409-N-6247M-038 EA-18G Growler is parked on the airfield next to an EA-6B Prowler.jpg|thumb|right|An EA-18G Growler alongside an EA-6B Prowler shortly after arriving at NAS Whidbey Island, 9 April 2007.]]

In an April 2006 report, the U.S. [[Government Accountability Office]] expressed concerns. The GAO felt the electronic warfare systems on the EA-18G were not fully mature so there is a risk of "''future cost growth and schedule delays''". The report recommended that the DoD consider purchasing additional ICAP III upgrades for EA-6Bs to fill any current and near-term capability gaps and restructure the initial EA-18G production plans so that procurement takes place after the aircraft has "demonstrated full functionality".<ref name=GAO-06-446>[http://www.gao.gov/new.items/d06446.pdf "GAO-06-446, 'Option of Upgrading Additional EA-6Bs Could Reduce Risk in Development of EA-18G'."] ''Government Accountability Office'', April 2006. Retrieved: 15 July 2011.</ref>  In a 2008 GAO report, the director of the DoD's Operational Test and Evaluation department questioned the workload on the two-person Growler crew to replace the Prowler's crew of four.<ref>[http://www.gao.gov/htext/d08467sp.html GAO-08-467SP "Defense Acquisitions: Assessments of Selected Weapon Programs."] ''GAO,'' 31 March 2008.</ref>

[[File:VAQ-133 EA-18G Growler at NAS Whidbey Island.JPG|thumb|left|VAQ-133 EA-18G Growler at NAS Whidbey Island]]

The U.S Navy has ordered a total of 57 aircraft to replace its in-service [[EA-6B Prowler]]s, most of which will be based at [[Naval Air Station Whidbey Island|NAS Whidbey Island]]. The US DoD gave approval for the EA-18G program to begin low-rate initial production in 2007.<ref>Fabey, Michael. [http://www.aviationnow.com/publication/aerospacedaily/loggedin/AvnowStoryDisplay.do?fromChannel=defense&pubKey=aerospacedaily&issueDate=2007-07-19&story=xml/aerospacedaily_xml/2007/07/19/04.xml&headline=Growler+passes+Milestone+C%2c+goes+to+low%2drate+initial+production+ "Growler passes Milestone C, goes to low-rate initial production" (log-in required).] ''Aerospace Daily & Defense Report,'' 19 July 2007. {{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>  The EA-18G was scheduled to finish flight testing in 2008.<ref>{{cite web |url= http://boeing.com/news/releases/2007/q3/070925c_nr.html |title= Boeing Delivers First Production EA-18G Growler to U.S. Navy |archiveurl= https://web.archive.org/web/20071011151249/http://boeing.com/news/releases/2007/q3/070925c_nr.html |archivedate= 11 October 2007 |website= Boeing |date= 25 September 2007 }}</ref>  The Navy planned to buy approximately 85 aircraft in 2008.<ref name=Navy_Welcomes>[http://www.navy.mil/search/display.asp?story_id=37611 "Navy Welcomes New Era of Electronic Warfare."] ''US Navy'', 4 June 2008.</ref> Approval for full-rate production was expected in the third quarter of 2009,<ref name=Ready_for_Ops/> and was given on 23 November 2009. Boeing planned to ramp up production to 20 aircraft per year.<ref>[http://boeing.mediaroom.com/index.php?s=43&item=964 "Boeing EA-18G Growler to Advance to Full Rate Production."] ''Boeing,'' 30 November 2009. Retrieved: 15 July 2011.</ref> On 9 July 2009, General [[James Cartwright]] told the [[United States Senate Committee on Armed Services]] that the choice had been to continue the F/A-18 production line because the warfighting commanders needed more aerial electronic warfare capability that only the EA-18G could provide.<ref>[http://www.defensenews.com/story.php?i=4179247 "Cartwright Talks F-22, Advocates JROC Changes."] ''defensenews.com.'' Retrieved: 29 August 2012.</ref>

The Navy's submission for the 2011 defense budget put forth by the Obama Administration calls for four EA-18G Growler squadrons to be added to the fleet.<ref>[http://www.navytimes.com/news/2010/02/navy_2011_budget_020110w/ "Navy budget includes 9 ships, kills CG(X)."] ''navytimes.com,'' February 2010. Retrieved: 29 August 2012.</ref>  On 14 May 2010, Boeing and the [[United States Department of Defense|US Department of Defense]] reached an agreement for a multi-year contract for an additional 66 F/A-18E/Fs and 58 EA-18Gs over the next four years. This will raise the total to 114 EA-18Gs on order.<ref name=FG_new_purchase>Trimble, Stephen. [http://www.flightglobal.com/articles/2010/05/14/342005/us-dod-agrees-to-buy-124-fa-18efs-and-ea-18gs-over-4.html "US DoD agrees to buy 124 F/A-18E/Fs and EA-18Gs over 4 years."] ''Flightglobal'', 14 May 2010. Retrieved: 15 July 2011.</ref>

The Pentagon's Director of Operational Test and Evaluation determined that the EA-18G was "still not operationally suitable" in February 2011. Prime contractor Boeing is working to address issues with software updates.<ref>Fabey, Michael. [http://www.aviationweek.com/aw/generic/story_channel.jsp?channel=defense&id=news/asd/2011/02/03/08.xml "U.S. Navy, Pentagon Debate EA-18G Growler."]{{dead link|date=November 2013}} ''Aviation Week,'' 3 February 2011. Retrieved: 15 July 2011.</ref>  In December 2011, Operational Test and Evaluation concluded that the EA-18G software was "operationally effective and suitable".<ref name="RCS: DD-A&T(Q&A)823-378">[http://www.dod.mil/pubs/foi/logistics_material_readiness/acq_bud_fin/SARs/DEC%202011%20SAR/EA-18G%20-%20SAR%20-%2031%20DEC%202011.pdf "Selected Acquisition Report (SAR), EA-18G."]  ''DOD,'' 31 December 2011. Retrieved: 9 August 2012.</ref>

On 19 December 2014, the Navy publicly reported that it wants to modify the production contract with Boeing to slow production of the Growler from three airplanes per month to two. It will also purchase an additional 15 Growlers, funded by a spending bill that will go to President Obama for signature in late December 2014. Boeing would then be able to continue running the St. Louis production line through 2017. Boeing has said it cannot sustain the production line at fewer than two airplanes per month.<ref>{{cite web|author=Shalal, Andrea|title=Boeing, U.S. Navy in talks about stretching EA-18G jet production|date=19 December 2014|url=https://finance.yahoo.com/news/boeing-u-navy-talks-stretching-190953666.html|publisher=Yahoo News}}</ref>'



==Design==
{{See also|Boeing F/A-18E/F Super Hornet#Design}}
[[File:EA-18G Growler VX-9 from below 2008.jpg|thumb|An EA-18G Growler (BuNo 166856) of test and evaluation squadron VX-9 Vampires, carrying a payload of external fuel tanks and missiles]]

The Growler's flight performance is similar to that of the F/A-18E/F. This attribute enables the Growler to perform escort jamming as well as the traditional standoff jamming mission ([[Radar jamming and deception]]). Growlers will be able to accompany F/A-18s during all phases of an attack mission.<ref name=Boe_EA-18G>[http://boeing.com/defense-space/military/ea18g/index.html "EA-18G Airborne Electronic Attack Aircraft."] ''Boeing.'' Retrieved: 15 July 2011.</ref> In order to give the Growler more stable flight for the electronic warfare mission, Boeing changed the leading edge fairings and wing fold hinge fairings, and added wing fences and aileron "tripper strips".<ref>Croft, John. [http://www.flightglobal.com/news/articles/ea-18g-growler-new-platform-and-capabilities-set-to-un-level-the-sead-playing-225072/ "EA-18G “Growler”: New platform and capabilities set to un-level the SEAD playing field."] ''Flight International'', 8 July 2008.</ref>

The Growler has more than 90% in common with the standard Super Hornet, sharing airframe, Raytheon AN/APG-79 AESA radar and weapon systems such as the AN/AYK-22 stores management system. Most of the dedicated airborne electronic attack equipment is mounted in the space that used to house the internal 20&nbsp;mm cannon and on the wingtips. Nine weapons stations remain free to provide for additional weapons or jamming pods.<ref name=Boe_EA-18G_overv>{{cite web |url= http://www.boeing.com/defense-space/military/ea18g/docs/EA-18G_overview.pdf |title= EA-18G Growler overview |archiveurl= https://web.archive.org/web/20110728175148/http://www.boeing.com/defense-space/military/ea18g/docs/EA-18G_overview.pdf |archivedate= 28 July 2011 |website= Boeing |date= 15 July 2011 }}</ref>  The added electronics include AN/[[ALQ-218]] wideband receivers on the wingtips and [[AN/ALQ-99|ALQ-99 high and low-band tactical jamming pods]]. The ALQ-218 combined with the ALQ-99 form a full spectrum electronic warfare suite that is able to provide detection and jamming against all known surface-to-air threats.<ref name=Boe_EA-18G/> However, the current pods may be inadequate against emerging threats.<ref>[[Lexington Institute|Thompson, Loren B.]] [http://defense.aol.com/2012/07/26/navy-steps-up-new-jammer-effort-first-new-system-in-40-years/ "Navy Steps Up New Jammer Effort; First New System in 40 Years."] ''Aol Defense'', 26 July 2012.</ref>

The EA-18G can be fitted with up to five ALQ-99 jamming pods and will typically add two [[AIM-120 AMRAAM]] or [[AGM-88 HARM]] missiles.<ref name="9.6 Billion" />  The EA-18G will also use the INCANS Interference Cancellation system that will allow voice communication while jamming enemy communications, a capability not available on the EA-6B.<ref>{{ cite web |url= http://www.boeing.com/news/releases/2005/q4/nr_051108m.html |title= Boeing EA-18G Program Completes INCANS Verification Testing, Demonstration |archiveurl= https://web.archive.org/web/20051125222021/http://www.boeing.com/news/releases/2005/q4/nr_051108m.html |archivedate= 25 November 2005 |website= Boeing |date= 8 November 2005 }}</ref> In addition to the radar warning and jamming equipment, the Growler possesses a communications receiver and jamming system that will provide suppression and electronic attack against airborne communication threats.<ref name=Boe_EA-18G_overv/>

The poor reliability of the ALQ-99 jammer pod and frequent failures of the Built-In Test (BIT) have caused the crew to fly missions with undetected faults.  The ALQ-99 has also interfered with the aircraft's AESA radar and has imposed a high workload on the two-man crew, along with reducing the Growler's top speed.<ref>[http://www.gao.gov/new.items/d10388sp.pdf "Defense Acquisitions: Assessments of Selected Weapon Programs."] ''U.S. Government Accountability Office'', 30 March 2010. Retrieved: 15 July 2011.</ref>

Boeing is looking into other potential upgrades; the ALQ-99 radar jamming pod may be replaced in the future, and the company is looking into adding weapons and replacing the satellite communications receiver. The Growler is the initial platform for the [[Next Generation Jammer]] (NGJ) which uses [[Active electronically scanned array]] (AESA) technology to focus jamming power exactly where needed. The NGJ was to be implemented on the [[F-35 Lightning II|F-35]].<ref name="N239a.ppt" /> However, in May 2012, the U.S. Navy decided to focus NGJ integration on the EA-18G for an expected in-service date of 2020, and defer work for the F-35.<ref>[http://www.flightglobal.com/news/articles/in-focus-us-navy-next-generation-jammer-proceeds-but-f-35-integration-deferred-indefinitely-371742/ "US Navy Next Generation Jammer proceeds, but F-35 integration deferred indefinitely."] ''Flightglobal,'' 11 May 2012.</ref>  Boeing is also looking at exporting a Growler Lite configuration without the jamming pods for electronic awareness rather than electronic attack.<ref>Doyle, Andrew. [http://www.flightglobal.com/articles/2009/02/13/322542/aero-india-boeing-reveals-plans-for-growler-lite.html "Aero India: Boeing reveals plans for 'Growler Lite'."] ''Flight International,'' 13 February 2009. Retrieved: 15 July 2011.</ref>

Three Growlers networked together can generate targeting tracks for hostile radio-frequency sources in real time, but this is difficult to arrange with the current minimum strength US Navy squadrons.<ref>Majumdar, Dave. [http://news.usni.org/2014/03/12/navy-wants-growlers "Why the Navy wants more Growlers."] ''US Naval Institute'',  12 March 2014. Retrieved: 13 March 2014.</ref> Utilizing faster data-links, the Growler could use its EW pods to accurately locate signal sources. In a group of three planes, when one detects a signal from a source such as a cell phone, the other two can also listen for the same signal, all three measuring the amount of time taken for transmissions to travel from the source to each aircraft to triangulate the location in "a very, very small area."<ref>Weisgerber, Marcus. [http://www.defenseone.com/technology/2015/04/new-tech-will-allow-navy-target-jamming-pods/110038/?oref=d-dontmiss "New tech will allow navy to target with jamming pods."] ''Defenseone.com'', 13 April 2015.</ref> By early 2015, the Navy had demonstrated this concept using EA-18s equipped with [[Rockwell Collins]]' tactical targeting network technology (TTNT) and [[ALQ-218]] receivers to acquire emissions from a target vessel and target it from a stand-off range without using their own detectable radar emissions.<ref>Trimble, Stephen. [http://www.flightglobal.com/news/articles/us-navy-fighter-develops-passive-target-id-for-ships-411211/ "US Navy fighter develops passive target ID for ships."] ''Flightglobal.com'', 15 April 2015.</ref> Boeing announced on 1 December 2015 that they would upgrade Navy EA-18Gs with the TTNT datalink.<ref>[https://www.flightglobal.com/news/articles/usn-upgrades-ea-18g-with-long-range-targeting-system-419677/ USN upgrades EA-18G with long-range targeting system] – Flightglobal.com, 3 December 2015</ref>

Following U.S. Navy missions in [[Operation Odyssey Dawn]] during the [[2011 Libyan Revolution]], the [[Royal Australian Air Force]] decided to add the [[Raytheon]] ATFLIR ([[forward looking infrared]]) pod to their order of 12 Growler aircraft.  When Navy EA-18Gs' radar and radar detectors located possible targets, they passed the information through datalinks to strike fighters.  However, the Growlers themselves lacked the ability to visually confirm what it detected, so adding a FLIR pod gives it visual acuity to see targets and shorten the kill chain; it is not known if the U.S. Navy will also add a FLIR pod.  Australian EA-18Gs will also be equipped with the [[AIM-9|AIM-9X Sidewinder missile]].<ref>[http://news.usni.org/2015/07/30/u-s-navy-lessons-from-libya-informed-australian-ea-18g-growler-modifications U.S. Navy Lessons from Libya Informed Australian EA-18G Growler Modifications] – News.USNI.org, 31 July 2015</ref>

==Operational history==

===United States===
[[File:US Navy 090217-N-3610L-243 An EA-18G Growler assigned to the.jpg|thumb|left|An EA-18G of [[VAQ-129]] "Vikings" aligns itself for an at-sea landing aboard [[USS Ronald Reagan (CVN-76)|USS ''Ronald Reagan'']]]]

The first Growler for fleet use was officially accepted by [[VAQ-129]] "Vikings" at [[NAS Whidbey Island]], on 3 June 2008.<ref name="Navy_Welcomes" />  The Navy planned to buy approximately 85 aircraft to equip 11 squadrons as of 2008.<ref name="Navy_Welcomes" /> The EA-18G completed operational evaluation in late July 2009. The Growler was rated operationally effective and suitable for operational use.<ref name="Ready_for_Ops">Fulghum, David A. [http://www.aviationweek.com/aw/generic/story.jsp?id=news/EA18080609.xml&headline=Classified%20Tests%20Show%20Growler%20Ready%20for%20Ops&channel=defense "Classified Tests Show Growler Ready for Ops."] ''Aviation Week,'' 6 August 2009. {{dead link|date=April 2017|bot=medic}}{{cbignore|bot=medic}}</ref><ref>Fein, Geoff. [http://www.defensedaily.com/publications/dd/7646.html "EA-18G Given High Marks In OPEVAL, Boeing Says" (log in required).] ''Defense Daily,'' 4 August 2009.</ref>  On 5 August 2009, EA-18G Growlers from [[VAQ-129|Electronic Attack Squadron 129 (VAQ-129)]] and [[VAQ-132|Electronic Attack Squadron 132 (VAQ-132)]] completed their first at-sea carrier-arrested landing aboard the {{USS|Harry S. Truman|CVN-75}}.<ref>{{cite web |last1= Evans |first1= Mark L. |first2= Dale J. |last2= Gordon |url= http://www.history.navy.mil/nan/currentissue/NAN_vol94_no2_YIR2009_feature.pdf |title= Year in Review 2009 |archiveurl= https://web.archive.org/web/20101205210636/http://www.history.navy.mil/nan/currentissue/NAN_vol94_no2_YIR2009_feature.pdf |archivedate=5 December 2010 |journal= Naval Aviation News |volume= 94 |issue= 2 |ISSN= 0028-1417 |date= Summer 2010 |page= 24 |accessdate= 12 October 2010 }}</ref>

The first deployable EA-18G squadron is to be [[VAQ-132]] "Scorpions", which reached operational status in October 2009.<ref name="VAQ-132_operational">[http://www.navy.mil/Search/display.asp?story_id=48821 "VAQ-132 Becomes First Operational Growler Squadron Safe for Flight."] ''U.S. Navy'', 8 October 2009.</ref> The first Growler operational deployment was announced on 17 February 2011.<ref>[http://air-attack.com/news/article/4394/02-17-2011-Boeing-EA-18G-Growlers-Deployed-by-US-Navy.html "Boeing EA-18G Growlers Deployed by US Navy."] ''Air-attack.com,'' 17 February 2011.</ref>  In service, the EA-18's radio name during flight operations will be "Grizzly". The "Growler" nickname sounded too much like the EA-6B's "Prowler" name, so "Grizzly" will be used to avoid confusion.<ref>[http://www.navytimes.com/news/2009/06/navy_grizzly_growler_060809w/ "New ops nickname for EA-18G."] ''Navytimes.com,'' 9 June 2009.</ref>  By May 2011, 48 Growlers had been delivered to the U.S. Navy.<ref name=Boe_EA-18G_overv/>

[[File:An EA-18G Growler of VAQ-141 at Atsugi Naval Air Facility.jpg|thumb|EA-18G Growler of [[VAQ-141]] taxiing after landing at [[Naval Air Facility Atsugi]], Japan.|alt= ]]
With the termination of the [[Boeing B-52 Stratofortress|EB-52H standoff jammer]], the Growler became the sole remaining manned tactical jammer. Air Staff requirements director Maj. Gen. David Scott has indicated that the USAF will seek to provide electronic warfare officers to fly on U.S. Navy Growlers, without providing funding to purchase additional aircraft.<ref>Tirpak, John A. [http://www.airforce-magazine.com/Features/newtech/Pages/box102109growler.aspx "Seeking Growler Backseat, No BUFF SOJ."] ''Airforce-magazine.com'', 20 October 2009. Retrieved: 15 July 2011.</ref>  U.S. Air Force personnel of [[390th Electronic Combat Squadron]] stationed at [[NAS Whidbey Island]] have been supporting and flying the Growler.<ref>{{Cite web|url=http://www.cnic.navy.mil/regions/cnrnw/installations/nas_whidbey_island/about/tenant_commands/390th-electronic-combat-squadron.html |title=390th Electronic Combat Squadron |website= cnic.navy.mil|access-date=2016-06-28}}</ref>

The EA-18G was first used in combat during [[Operation Odyssey Dawn]], enforcing the UN [[no-fly zone]] over [[Libya]] in 2011.<ref>Ackerman, Spencer. [https://www.wired.com/dangerroom/2011/03/in-combat-debut-navy-jammer-targets-libyan-tanks/ "In combat debut, Navy jammer targets Libyan tanks."] ''Condé Nast'' via ''Wired'', 21 March 2011. Retrieved: 12 March 2013.</ref><ref>[http://www.seapowermagazine.org/sas/stories/20120416-sas-growler.html "Growlers in action only 48 hours after Libya notification."] ''Sea Power Magazine'' (Navy League of the United States), 16 April 2012. Retrieved: 2 March 2013.{{dead link|date=November 2013}}</ref> Five EA-18Gs were redeployed from Iraq to support operations in Libya in 2011.<ref>[http://www.defense.gov/news/newsarticle.aspx?id=63272 "Roughead: Ships Were Ready for Odyssey Dawn."] ''American Forces Press Service (US Navy)'', 23 March 2011.</ref>

===Australia===
In 2008 the [[Australian Government]] requested export approval from the US government to purchase up to six EA-18Gs,<ref>Dodd, Mark. [http://www.theaustralian.com.au/national-affairs/defence/raaf-likes-the-sound-of-growler/story-e6frg8yo-1111117199706 "RAAF likes the sound of the Growler."] ''The Australian,'' 15 August 2008. Retrieved: 15 July 2011.</ref> which would be part of the order for 24 F/A-18F Super Hornets.<ref>Kelly, Emma. [http://www.flightglobal.com/articles/2008/08/22/315036/australia-expands-p-3-upgrade-plays-down-growler-reports.html "Australia expands P-3 upgrade, plays down Growler reports."] ''Flight International'', 22 August 2008. Retrieved: 15 July 2011.</ref> On 27 February 2009, Defence Minister [[Joel Fitzgibbon]] announced that 12 of the 24 Super Hornets on order would be wired on the production line for [[For but not with|future fit-out]] as EA-18Gs. The additional wiring would cost [[Australian dollar|A$]]35&nbsp;million. The final decision on conversion to EA-18Gs, at a cost of A$300&nbsp;million, was to be announced in March 2012.<ref>[http://www.defence.gov.au/minister/70tpl.cfm?CurrentId=8817 "Super Hornets Wired For Future Upgrade."]  ''Australian Department of Defence,'' 27 February 2009.</ref><ref>{{cite news|last=McPhedran|first=Ian|author-link=Ian McPhedran|url=http://www.adelaidenow.com.au/news/national/m-refit-to-give-fighter-jets-growl/story-e6frea8c-1226277576058|title=$200m refit to give fighter jets growl|newspaper=The Advertiser|date=22 February 2012|archiveurl=https://web.archive.org/web/20120612090804/http://www.adelaidenow.com.au/news/national/m-refit-to-give-fighter-jets-growl/story-e6frea8c-1226277576058|archivedate=12 June 2012}}</ref>

On 23 August 2012, the Australian Government announced that 12 RAAF Super Hornets would be fitted with Growler capability at a cost of $1.5 billion;<ref>{{cite news|url=http://www.abc.net.au/news/2012-08-23/super-hornets-to-have-electronic-jammer/4218256|title=Super Hornets to get electronics jammer|publisher=Australian Broadcasting Corporation|website=ABC News|date=23 August 2012|archivedate=8 January 2016|archiveurl=https://web.archive.org/web/20160108070727/http://www.abc.net.au/news/2012-08-23/super-hornets-to-have-electronic-jammer/4218256}}</ref> making the Royal Australian Air Force the only military other than the U.S. to operate the Growler's electronic jamming equipment.<ref>{{citation|url=http://www.reuters.com/article/2012/08/23/us-australia-defence-idUSBRE87M0A820120823|title=Australia fighter jets first to get hi-tech U.S. jammers|publisher=Reuters|date=23 August 2012|first=James|last=Grubel|editor-first=Jeremy|editor-last=Laurence}}</ref> On 3 May 2013, the Australian Government announced that it will buy 12 new-build Growlers to supplement the existing Super Hornet fleet.<ref>Taylor, Ellis. [http://www.flightglobal.com/news/articles/canberra-commits-to-new-growlers-but-remains-coy-on-f-35s-385444/ "Canberra commits to new Growlers, but remains coy on F-35s."] ''Flight'', 3 May 2013.</ref>

Australia took delivery of the first of 12 Growlers on 29 July 2015, with the delivery of the remaining jets expected by 2017.<ref>{{Cite web|url=http://stlouis.cbslocal.com/2015/07/29/boeing-delivers-electronic-attack-aircraft-to-australian-air-force/|title=Boeing’s Growler Program  Manager Optimistic About Line’s Future|date=29 July 2015|accessdate=30 July 2015|author=Brian Kelly|publisher=CBS Local}}</ref> Uniquely, Australian Growlers will be equipped with the ASQ-228 ATFLIR targeting pod and will also have additional air-to-air weapons in the form of the [[AIM-9X]] missile.<ref>{{cite news|last=Pittaway|first=Nigel|title=Boeing Rolls Out First Growler for Australia|url=http://www.defensenews.com/story/defense/air-space/2015/07/29/boeing-rolls-out-first-growler-australia/30845221/|journal=Defense News|date=29 July 2015|accessdate=30 July 2015}}</ref> The aircraft will be operated by [[No. 6 Squadron RAAF]].<ref>{{cite journal|url=http://airforcenews.realviewdigital.com/?startpage=2&iid=84161#folio=2|last=Popp|first=Tony|title=Growler one step closer|work=Air Force|page=3|date=21 November 2013|accessdate=18 November 2013}}</ref>

==Operators==
;{{AUS}} 
* [[Royal Australian Air Force]] 
** [[No. 6 Squadron RAAF]]

;{{USA}}
* [[United States Air Force]]
**[[390th Fighter Squadron|390th Electronic Combat Squadron]] provides training for electronic warfare officers for Navy units.<ref>[http://www.af.mil/news/story.asp?id=123262586 "Air Force EWO graduates from Navy Growler training."] ''af.mil/news.'' Retrieved: 29 August 2012.</ref>
*[[United States Navy]]
**[[VAQ-129]]<ref name=Navy_Welcomes/>
**[[VAQ-130]]
**[[VAQ-131]] 
**[[VAQ-132]]<ref name=VAQ-132_operational/>
**[[VAQ-133]] 
**[[VAQ-134]]
**[[VAQ-135]]<ref>{{cite news |last= Janik |first= Cmdr. Scott |url= http://www.thenorthwestnavigator.com/photos/2011/jul/28/11613/ |title= Electronic Attack Squadron (VAQ) 135 Black Ravens’ newly painted EA-18G Growler ‘520’ flies alongside Mt. Baker |archiveurl= https://web.archive.org/web/20121107164759/http://www.thenorthwestnavigator.com/photos/2011/jul/28/11613/ |archivedate= 7 November 2012 |newspaper= Northwest Navigator |date= 28 July 2011 |accessdate= 29 August 2012 }}</ref>
**[[VAQ-136]] 
**[[VAQ-137]] 
**[[VAQ-138]]
**[[VAQ-139]] 
**[[VAQ-141]] 
**[[VAQ-142]] 
**[[VAQ-209]]
**[[VX-9]]
**[[Naval Strike and Air Warfare Center]]

==Specifications (EA-18G Growler)==
{{aircraft specifications
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]].
     Please answer the following questions. -->
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li>
-->
|ref = Boeing brochure<ref>[http://www.boeing.com/defense-space/military/ea18g/index.html "EA-18G Capabilities."] ''Boeing.'' Retrieved: 15 July 2011.</ref> and U.S. Navy F/A-18E/F fact file.<ref name=USN_fact>[http://www.navy.mil/navydata/fact_display.asp?cid=1100&tid=1200&ct=1 "F/A-18 fact file."] ''U.S. Navy,'' 26 May 2009.</ref>

|crew=2
|length main=60 ft 1.25 in
|length alt=18.31 m
|span main=44 ft 8.5 in
|span alt=13.62 m) (including wingtip-mounted pods
|height main=16 ft
|height alt=4.88 m
|area main=500 ft<sup>2</sup>
|area alt=46.5 m<sup>2</sup>
|empty weight main=33,094 lb
|empty weight alt=15,011 kg
|loaded weight main=48,000 lb
|loaded weight alt=21,772 kg
|loaded weight more=; recovery weight
|max takeoff weight main=66,000 lb
|max takeoff weight alt=29,964 kg
|max takeoff weight more=
|more general= '''Internal fuel capacity:''' 13,940 lb (6,323 kg)
*'''External fuel capacity:''' (3 x 480 gal tanks): 9,774 lb (4,420 kg)

|engine (jet)=[[General Electric F414]]-GE-400
|type of jet=[[turbofan]]s
|number of jets=2
|thrust main=14,000 lbf
|thrust alt=62.3 kN

|afterburning thrust main=22,000 lbf
|afterburning thrust alt=97.9 kN
|max speed main=Mach 1.8<ref name=USN_fact/>
|max speed alt=1,190 mph, 1,900 km/h
|max speed more=at 40,000 ft (12,190 m)
|ceiling main= >50,000 ft
|ceiling alt=15,000 m
|range main=1,275 nmi
|range alt=2,346 km
|range more=; clean plus two AIM-9s<ref name=USN_fact/>
|combat radius main=390 nmi
|combat radius alt=449 mi, 722 km
|combat radius more=; for interdiction mission<ref>[http://www.fas.org/programs/ssp/man/uswpns/air/fighter/f18.html "F/A-18 Hornet."] ''FAS.org.'' Retrieved: 15 July 2011.</ref>
|ferry range main=1,800 nmi
|ferry range alt=2,070 mi, 3,330 km
|ferry range more=; range without ordnance
|climb rate main=<!-- ft/min-->
|climb rate alt=<!-- m/s-->
|loading main=92.8 lb/ft<sup>2</sup>
|loading alt=453 kg/m<sup>2</sup>
|thrust/weight=0.93
|more performance=
|armaments=
|guns= None
|hardpoints= 9 total: 6× under-wing, and 3× under-fuselage
|hardpoint capacity= 17,750 lb (8,050 kg) external fuel and ordnance
*'''Notes:''' The two wingtips missile launcher rail for [[AIM-9 Sidewinder]], found on the E/F Super Hornet, have been replaced with [[ALQ-218|AN/ALQ-218]] detection pods, six removable under wing mounted hard points (inboard pylons will carry 480 gal fuel tanks, mid-board pylons will carry [[AN/ALQ-99]] High Band Jamming Pods, and outboard pylon reserved for [[AGM-88 HARM]] missiles), two multi-mode conformal fuselage stations ([[AIM-120 AMRAAM]] missiles), 1 centerline fuselage removable hardpoint, for AN/ALQ-99 Low Band Jamming Pod.
**Weapons employment: Currently, Phase I of the Growler will carry the AIM-120 AMRAAM missiles for self-protection at the two conformal fuselage stations and AGM-88 HARM missiles. The A/A-49A-2 gun system with the 20 mm [[M61 Vulcan|M61A2]] cannon has been removed and replaced by a pod of electronic boxes that control the AN/ALQ-218 and assist with the coordination AN/ALQ-99 jamming attacks.
**According to the possible weapon configurations which were revealed, EA-18G would also be capable of performing "time-sensitive" strike missions, carrying [[AGM-154 JSOW]] under wings, or multi-sensor reconnaissance missions with SHARP and [[AN/ASQ-228 ATFLIR]] on centerline and left conformal weapon stations, respectively.
|avionics=
*[[Raytheon]] [[AN/APG-79]] [[Active electronically scanned array]] (AESA) radar
*[[Mnemonics, Inc]] 990-1119-001 Electronic Attack Unit (EAU) AES Mission Computer
}}

==See also==
{{Portal|United States Navy|Aviation}}
{{aircontent
|see also=
|related=
*[[McDonnell Douglas F/A-18 Hornet]]
*[[Boeing F/A-18E/F Super Hornet]]
|similar aircraft=
*[[General Dynamics–Grumman EF-111A Raven]]
*[[Northrop Grumman EA-6B Prowler]]
*[[Shenyang J-16#Variants|Shenyang J-16D]]
|lists=
*[[List of active United States military aircraft]]
*[[List of fighter aircraft]]
}}

==References==

===Notes===
{{reflist|35em}}

===Bibliography===
{{Refbegin}}
*{{cite book|last=Thomason|first=Tommy H.|title=Strike from the Sea: U.S. Navy Attack Aircraft from Skyraider to Super Hornet, 1948 – present|location=North Branch, Minnesota|publisher=Specialty Press|date=2009|isbn=978-1-58007-132-1}}
{{Refend}}

==External links==
{{Commons category|Boeing EA-18G Growler}}
*{{Official website|http://www.boeing.com/defense-space/military/ea18g/index.html |Official webpage of the Boeing EA-18G Growler}}
*[http://www.globalsecurity.org/military/systems/aircraft/f-18g.htm EA-18G profile] at GlobalSecurity.org
*[http://www.defenseindustrydaily.com/ea18g-program-the-usas-electronic-growler-02427/ EA-18G Program: The USA's Electronic Growler] at DefenseIndustryDaily.com
*[http://www.flightglobal.com/articles/2008/07/08/225072/ea-18g-growler-new-platform-and-capabilities-set-to-un-level-the-sead-playing.html "EA-18G "Growler": New platform and capabilities set to un-level the SEAD playing field"]. ''[[Flight International]]'', 8 July 2008.
*[http://www.airspacemag.com/military-aviation/When-Hornets-Growl.html?c=y&page=2 "When Hornets Growl"]. ''Air & Space'', 1 March 2011.
*[http://www.vaq136.com/lacrosse4/ea18g.html VAQ-141 Shadowhawks EA-18G Growler: Photo walk around of two aircraft including the CAG bird.]

{{Boeing combat aircraft}}
{{US attack aircraft}}
{{Use dmy dates|date=January 2013}}

[[Category:Carrier-based aircraft]]
[[Category:United States attack aircraft 2000–2009]]
[[Category:2009 introductions]]