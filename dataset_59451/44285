{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox military installation
| name = RAF Woodvale<br><s>HMS Ringtail II</s>
| ensign=[[File:Ensign of the Royal Air Force.svg|90px]][[File:Naval Ensign of the United Kingdom.svg|90px]]
| native_name = 
| partof = 
| location = 
| nearest_town = [[Woodvale, Merseyside|Woodvale]], [[Merseyside]]
| country = England
| image = 
| caption = 
| image2 = RAF Woodvale crest.png
| coordinates = {{Coord|53|34|54|N|003|03|20|W|region:GB_type:airport|display=inline,title}}
| type = [[Royal Air Force station]]
| pushpin_map = Merseyside
| pushpin_map_caption = Shown within Merseyside
| pushpin_label = EGOW
| ownership = [[Ministry of Defence (United Kingdom)|Ministry of Defence]]
| operator = [[Royal Air Force]]
| controlledby = 
| code = 
| site_area = 
| built = {{Start date|1941}}
| used = 1941-Present<!--{{End date|1946}} -->
| builder = 
| materials = 
| height = 
| fate = 
| condition = 
| battles = 
| events = 
| current_commander = <!-- current commander -->
| past_commanders = <!-- past notable commander(s) -->
| garrison = 
| open_to_public = 
| occupants = 
| website = 
| IATA = 
| ICAO = EGOW
| FAA = 
| TC = 
| LID = 
| GPS = 
| elevation = {{Convert|11|m|0}}
| WMO = 
| r1-number = 03/21
| r1-length = {{Convert|1647|m|0}}
| r1-surface = [[Asphalt]]
| r2-number = 08/26
| r2-length = {{Convert|1068|m|0}}
| r2-surface = Asphalt
}}
'''Royal Air Force Woodvale''' or '''RAF Woodvale''' {{Airport codes||EGOW}} is a [[Royal Air Force]] Station located {{convert|4|mi|abbr=on}} next to the town of [[Formby]] in an area called [[Woodvale, Merseyside|Woodvale]]- just South of [[Southport]], [[Merseyside]]. Although constructed as an all-weather night fighter airfield for the defence of Liverpool, it did not open until 7 December 1941 which was just after the [[Liverpool Blitz]], which had peaked in May.<ref name="Local Newspaper">[http://www.merseyreporter.com/history/historic/woodvale/index.shtml Local Newspaper]</ref>

==The Second World War==
During the [[Second World War]] RAF squadrons were brought up from the south of England to 'rest' for short periods, during which time they defended Merseyside. [[No. 308 Polish Fighter Squadron|308 (Krakowski) Squadron]] was the first to arrive, on 12 December 1941, from [[RAF Northolt]] before leaving on 1 April 1942.<ref name="Jefford2001p85">{{Harvnb|Jefford|2001|p=85.}}</ref> Squadrons were rotated regularly. Several were Polish, including [[No. 315 Squadron RAF|315 (Dęblinski) Sqn]] and [[No. 317 Squadron RAF|317 (Wilenski) Sqn]]. [[Supermarine Spitfire|Spitfire]] IIs and Vbs were operated by these units.<ref name="Jefford2001p86">{{Harvnb|Jefford|2001|p=86.}}</ref>

Support units working with all three Services also served there, calibrating anti-aircraft guns and towing targets for the [[Royal Navy]]. In April 1945, Woodvale briefly became a Tender for the Royal Navy's [[Fleet Air Arm]] airfield at [[Burscough]], [[RNAS Burscough (HMS Ringtail)|HMS ''Ringtail'']], being given the name HMS ''Ringtail II''.<ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/woodvale |title=Woodvale |publisher=[[Airfields of Britain Conservation Trust]]|accessdate=14 June 2012}}</ref>

==Post war==
[[File:Woodvale08 019.JPG|thumb|left|[[No. 10 Air Experience Flight RAF|10 AEF]] Grob Tutor and hangar at RAF Woodvale.]]
After a period of inactivity, Woodvale reopened on 22 July 1946, when the Spitfire F14s of No. 611 (West Lancashire) Squadron, [[Royal Auxiliary Air Force]], moved here from [[Liverpool John Lennon Airport|Liverpool Airport]] at [[Speke]]. The squadron re-equipped with Spitfire F22s in February 1949. [[Gloster Meteor]] F.4 and F.8 jets were flown between May 1951 until 9 July 1951. Because of the need for better facilities, the Squadron moved to [[RAF Hooton Park]], joining No. 610 Squadron, where it remained until its disbandment on 10 March 1957.The Temperature and Humidity Flight, operating Spitfires and Mosquitos, was based there from 1953 to 1958.<ref name="Jefford2001p100">{{Harvnb|Jefford|2001|p=100.}}</ref>

1957 the British legend, the Spitfire made its last operational flight, in active British military markings, from RAF Woodvale.<ref name="Local Newspaper"/>

No. 5 Civilian Anti-Aircraft Co-Operation Unit moved to Woodvale on 1 January 1958, and operated target-towing Meteors until 30 September 1971 when the unit was disbanded.<ref name="Lake1999p48">{{Harvnb|Lake|1999|p=48.}}</ref>

==Training station==
Since 1971, RAF Woodvale has remained a training station and is currently home to:
*Liverpool [[University Air Squadron]]- LUAS moved in from RAF Hooton Park 2 July 1951.<ref name="RAF">{{cite web|url=http://www.raf.mod.uk/rafwoodvale/aboutus/whoisbasedhere.cfm |title=Who is based here |publisher=Royal Air Force|accessdate=14 June 2012}}</ref>
*Manchester and Salford University Air Squadron (then named '''Manchester University Air Squadron'''). MUAS (now MASUAS) moved in from Manchester's [[Barton Aerodrome]] in March 1953.<ref name="RAFWEB">{{cite web|url=http://www.rafweb.org/Stations/Stations-W.htm#Woodvale |title=RAF Woodvale |publisher=Air of Authority - A History of RAF Organisation|accessdate=14 June 2012}}</ref>
[[File:SAL Bulldog T.1 XX616 '3' Mcr UAS WVL 12.03.83 edited-3.jpg|thumb|Manchester University Air Squadron [[Scottish Aviation Bulldog|Bulldog T.1]] at RAF Woodvale in 1983]]
*[[10 Air Experience Flight]] - 10 AEF was formed at RAF Woodvale 25 August 1958.<ref name="RAFWEB"/>
*631 [[Volunteer Gliding Squadron]] - 631 VGS moved in from [[RAF Sealand]] in March 2006.<ref name="VGS">{{cite web|url=http://www.631vgs.com/ |title=Welcome to 631 Volunteer Gliding Squadron |publisher=631 Volunteer Gliding Squadron|accessdate=14 June 2012}}</ref>
*611 (West Lancashire)RAuxAF<ref>{{cite web|url=http://www.raf.mod.uk/rafreserves/rolesandsquadrons/611westlancashire.cfm |title=611westlancashire}}</ref> 
*Headquarters Merseyside Wing of the [[Air Training Corps]].<ref name="RAF"/>
*611 (Woodvale) Squadron ATC.<ref name="RAF"/>
*Woodvale Aircraft Owners' Group - WAOG.<ref name="VGS"/>

The current station commander is Squadron Leader Edwards.<ref>{{cite web|url=http://www.raf.mod.uk/rafwoodvale/ |title=Metropolitan Borough of Sefton mayoral engagements|publisher=Sefton Council|accessdate=8 October 2009}}</ref>

==Woodvale Rally==

In 1971, RAF Woodvale hosted the first annual Woodvale International Rally.<ref>[http://www.woodvalerally.com]</ref>  The event is a charitable event, that originally began as a model aircraft show. It has grown over the years to include [[car club]]s with both [[classic car]]s, [[vintage car]]s and other vehicle displays. It usually occupies the first weekend in August.<ref>{{cite web|url=http://www.visitsouthport.com/whats-on/woodvale-rally-new-location-and-change-of-date-p139393 |title=Whats On - Woodvale Rally|publisher=Sefton Council|accessdate=14 June 2012}}</ref> The 2012 rally had to be re-located and rescheduled<ref>Local [http://www.southportreporter.com/610/ Newspaper] report</ref>  to nearby [[Victoria Park, Southport|Victoria Park]], [[Southport]], [[Merseyside]], on safety grounds. [[Asbestos]] was discovered from old [[World War II]] structures<ref>Local [http://www.southportreporter.com/555/ Newspaper] report</ref>  that had been buried long ago.<ref>{{cite web|url=http://www.woodvalerally.com/more-info/press-release |title=Press Release |publisher=Woodvale Rally|accessdate=14 June 2012}}</ref>

==Merseyside Police Air Support Group==

Basing the Merseyside Air Support Group at RAF Woodvale made the station something of a target for criminals. Just before 2230 on Friday 9 October 2009 a window of the helicopter was smashed and petrol poured inside causing the helicopter to be grounded.<ref>{{cite web|url=http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2009/10/12/attack-on-merseyside-police-helicopter-sparks-high-speed-chase-through-liverpool-92534-24906592 |title=Liverpool Daily Post|accessdate= 1 April 2013}}</ref>

On 17 May 2010 the Merseyside Police helicopter was again attacked and grounded, after four masked intruders broke into the airbase at around 04:00 causing what was described as minor damage.<ref>{{cite web|url=http://www.liverpoolecho.co.uk/liverpool-news/local-news/2010/05/18/merseyside-police-helicopter-attack-foiled-for-second-time-100252-26465724/|title=Liverpool Echo|accessdate= 1 April 2013}}</ref>

As part of the reorganisation of Police Air Support in England and Wales and the formation of the [[National Police Air Service]], Merseyside operationally retired its dedicated Police helicopter ''G-XMII'' in July 2011.<ref name=echo>{{cite web|url=http://www.liverpoolecho.co.uk/liverpool-news/local-news/2011/07/18/merseyside-police-loses-its-own-helicopter-and-air-base-100252-29070538/|title=Liverpool Echo|accessdate= 1 April 2013}}</ref> Cover would then be provided with four aircraft from Cheshire, Lancashire, North Wales and Greater Manchester, the nearest to Merseyside being based at [[Hawarden Airport]] with ''G-XMII'' providing back up.<ref name=norway>{{cite web|title=Norwegian Police lease second EC135|url=http://helihub.com/2012/05/23/norwegian-police-buy-second-ec135/|accessdate=15 June 2013}}</ref> From 1 June 2012 Merseyside Police signed a four-year lease with [[Norwegian Police Service]] for the helicopter in response to the [[2011 Norway attacks|2011 terrorist attack]].<ref name=norway />

==See also==
*[[List of Royal Air Force stations]]

==References==

===Citations===
{{reflist|2}}

===Bibliography===
*{{wikicite|ref={{harvid|Jefford|2001}}|reference=Jefford, C.G, [[Order of the British Empire|MBE]],BA,RAF (Retd). ''RAF Squadrons, a Comprehensive Record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 2001. ISBN 1-84037-141-2.}}
*{{wikicite|ref={{harvid|Lake|1999}}|reference=Lake, A ''Flying Units of the RAF''. Shrewbury,  Airlife Publishing Ltd., 1999. ISBN 1-84037-086-6.}}

==External links==
{{Commons category|RAF Woodvale}}
*[http://www.south-lancs-aviation.bravepages.com/history_of_raf_woodvale.htm Base history]
*[http://www.masuas.co.uk/home.php MASUAS - Manchester and Salford Universities Air Squadron]
*[http://www.hms-ringtail.co.uk History of RNAS Burscough (HMS Ringtail)]
*[http://www.merseyreporter.com/history/historic/woodvale/index.shtml Mersey Reporter]
*http://derbosoft.proboards.com/thread/13753/thum-flight-1953-woodvale-speke
*{{WAD|EGOW}}
{{Royal Air Force}}

[[Category:Royal Air Force stations in Lancashire|Woodvale]]
[[Category:Royal Air Force stations in Merseyside|Woodvale]]
[[Category:Sefton]]
[[Category:Southport]]
[[Category:Airports in England|Woodvale]]