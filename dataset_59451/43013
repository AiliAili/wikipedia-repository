<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=July 2013}}
{|{{Infobox Aircraft Begin
 |name = Il-214 MTA
 |image = File:Hal MRTA.JPG
 |caption = A model of the HAL Multi-role Transport Aircraft at the [[Aero India]] exhibition in 2009.
}}
{{Infobox Aircraft Type
 |type = [[Military transport aircraft]]
 |manufacturer = [[United Aircraft Corporation]]
 |first flight = 2017 (planned)<ref name=AIN>{{cite web|url=http://www.ainonline.com/aviation-news/ain-news-live-aeroindia/2013-02-05/russian-indian-military-transport-jv-debuts-aero-india |title=Russian-Indian Military Transport JV Debuts at Aero India |last1=Karnozov |first1=Vladimir 
 |date = 5 February 2013 |accessdate=27 June 2013}}</ref>
 |introduction = 2018 (planned)<ref name=AIN/>
 |retired = 
 |status = Under development
 |primary user = [[Russian Air Force]]
 |more users = 
 |produced  = 
 |number built = 
 |program cost = [[US$]]600 million
 |unit cost = US$35–40 million
 |developed from = 
 |variants with their own articles = 
}}
|}

The '''Ilyushin Il-214 Multi-role Transport Aircraft''' ('''MTA''') is a medium-[[airlift]] [[military transport aircraft]] being developed by the [[United Aircraft Corporation]] (UAC) of [[Russia]], and [[Hindustan Aeronautics|Hindustan Aeronautics Limited (HAL)]] of [[India]].<ref>[http://www.irkut.com/en/services/research/irta/ "Tactical Transport Aircraft"]. Irkut Corporation. Retrieved 5 December 2011.</ref><ref>[http://en.rian.ru/russia/20090212/120098967.html "Russia, India may form military transport planes JV in 2–3 months"]. [[RIA Novosti]]. 12 February 2009. Retrieved 6 July 2013.</ref> The two companies began the joint venture in 2009, when it was expected that each would be investing US$300 million in the project.<ref>[http://articles.timesofindia.indiatimes.com/2009-02-12/india/28057268_1_transport-aircraft-united-aircraft-corporation-india-and-russia "India, Russia enter into $600m JV to develop new aircraft"]. ''[[Times of India]]''. 12 February 2009. Retrieved 6 July 2013.</ref>

The MTA was intended to replace the [[Indian Air Force]]'s ageing fleet of [[Antonov An-32]] transport aircraft. It is designed to perform regular transport duties and also to deploy [[paratrooper]]s. The aircraft is expected to conduct its first flight by 2017, and to enter service by 2018.<ref name=AIN/>

In January 2016, it was announced that the India's HAL would no longer be involved in the project and that Russia would proceed with the project alone.

==Design and development==
In October 2009, former [[Minister of Defence (India)|Indian Defence Minister]] [[A. K. Antony]] made an official visit to Russia, during which the two countries formally incorporated the joint venture. The governments of Russia and India agreed to produce the aircraft for their respective armed forces and for friendly third-party countries, and to develop a [[Civil aviation|civilian]] variant of the MTA in the form of a 100-seater passenger airplane, for which Hindustan Aeronautics Ltd (HAL) – owned by the Indian government – will be the lead partner and principal integrator. The Indian portion of the MTA's serial production would take place at HAL's Transport Aircraft Division in [[Kanpur]].<ref>[http://livefist.blogspot.com/2009/08/uac-hals-multirole-transport-aircraft.html "UAC-HAL's Multirole Transport Aircraft JV To Be Incorporated Next Month"]. LiveFist at Blogspot.com. 30 August 2009. Retrieved 5 December 2011.</ref>

India and Russia finalised arrangements to support the MTA's development with a contribution of US$300.35 million. Russia’s United Aircraft Corporation (UAC) and India's HAL will set up a subsidiary company to develop the aircraft. The new company, supported by US$600.7 million in funding, will begin work on developing the MTA immediately. HAL Chairman and Managing Director (CMD) Ashok Nayak confirmed that India would acquire 45 aircraft and Russia 105. There would, however, be scope for exporting the aircraft, both for civil and military use, and more MTAs could be manufactured.<ref>[http://www.indiastrategic.in/topstories716.htm "India, Russia finalize transport aircraft project"]. IndiaStrategic.in. September 2010. Retrieved 12 February 2013.</ref> In October 2012, HAL signed a preliminary design contract with UAC, stipulating that joint design work would begin in Moscow, involving 30 Indian engineers as well as UAC's design team.<ref>[http://articles.economictimes.indiatimes.com/2012-10-12/news/34412785_1_indian-aerospace-hindustan-aeronautics-russia "Aircraft: Hindustan Aeronautics Ltd signs design phase contract with Russia firm"]. ''India Times''. 12 October 2012. Retrieved 15 October 2012.</ref> In February 2015, India cancelled its existing international tender on medium-lift military transport aircraft, formalising its intent to purchase the MTA.<ref>[http://in.rbth.com/news/2015/02/06/india_chooses_multi-role_transport_aircraft_produced_jointly_with_russia_41217.html "India chooses Multi-Role Transport Aircraft produced jointly with Russia, cancels tender"]. ''[[Russia Beyond The Headlines]]''. 6 February 2015. Retrieved 29 September 2015.</ref> In March 2015, it was reported that international work sharing issues had slowed the MTA project's progress, though it remained underway.<ref>{{cite web|url=http://www.rt.com/news/245145-india-warplanes-missing-ukraine/|title=Indian warplanes go missing during upgrade in Ukraine|publisher=[[Russia Today]]|date=30 March 2015|accessdate=30 September 2015}}</ref>

The aircraft is expected to be powered by Russian-made [[Aviadvigatel PD-14|Aviadvigatel PD-14M]] turbofan engines attached to top-mounted wings,<ref>[http://www.rusbiznews.com/news/n676.html "Russian aircraft designers tested yesterday's engine"]. Rus Business News. 16 December 2009. Retrieved 6 July 2013.</ref> and will have a T-shaped tail. The cabin size would be similar to the [[Ilyushin Il-76]], but will be half the length, supporting a maximum payload of {{convert|20|t|lb}} of military or civilian cargo. The aircraft's maximum range is expected to be {{convert|2500|km|mi}}, and its top speed will be around {{convert|870|km/h|abbr=on}}.<ref name=AIN/>

On 13 January 2016, Russian state media reported that Ilyushin had "frozen" the Russian-Indian project, and that Russia would assume full responsibility for detailed design and production of the aircraft.<ref name=Frozen>{{Cite journal|last=Jennings|first=Gareth|title=Russia 'freezes' India out of MTA project, to proceed alone as Il-214|journal=Jane's Defence Weekly|volume=53|issue=10|date=13 January 2016|publisher=Jane's Information Group|location=Surrey, UK|issn=0265-3818}}</ref><ref name="Karnozov">{{cite web|url=http://www.ainonline.com/aviation-news/defense/2016-04-11/india-out-russia-continues-developing-il-214-transport|title=India Is Out, But Russia Continues Developing Il-214 Transport|last=Karnozov|first=Vladimir|date=11 April 2016|work=AINonline|accessdate=25 February 2017}}</ref>

==Specifications==
[[File:UACHAL Il-214 Multirole Transport Aircraft (MTA) model.JPG|thumb|A side cut view of the aircraft's cargo bay.]]
{{Aircraft specs
|ref=[http://www.uacrussia.ru/en/models/cargo/mts/mts_performance/ UACRussia.ru] ''and'' [http://www.ilyushin.org/en/aircrafts/projects/1182/ Ilyushin.org]
|prime units?=met
<!--
        General characteristics
-->
|crew=3
|capacity=70 to 150 passengers<br/>
*'''Payload:''' {{convert|20000|kg|lb}}
|length m=37.7
|span m=35.5
|height m=12.95
|max takeoff weight kg=68,000
|fuel capacity={{convert|25000|kg|lb}}
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Aviadvigatel PD-14|Aviadvigatel PD-14M]]
|eng1 type=turbofan engines
|eng1 lbf=34,392
<!--
        Performance
-->
|max speed kmh=870
|cruise speed kmh=810
|range km=3250
|range note=with payload of {{convert|20000|kg|lb}}
|ferry range km=7300
|ceiling m=13,100
|more performance=
*'''Takeoff run''': {{convert|1050|m|ft}}  
*'''Landing run''': {{convert|1050|m|ft}}
|avionics=
}}

==See also==
{{Aircontent
|related=
|similar aircraft=
* [[Antonov An-178]]
* [[Embraer KC-390]]
* [[Kawasaki C-2]]
}}

==References==
{{reflist}}

==External links==
{{Commons category|Ilyushin Il-214}}
* [http://www.ilyushin.org/en/aircrafts/projects/1182/ "Multi-Purpose Transport Aircraft"]. Ilyushin.org. Retrieved 4 December 2014.
* [http://www.globalsecurity.org/military/world/russia/il-214.htm "Il-214 Multi-Role Transport Aircraft (MTA)"]. GlobalSecurity.org. 2011. Retrieved 25 July 2013.

{{Ilyushin aircraft}}

{{DEFAULTSORT:UAC HAL Il-214}}
[[Category:HAL aircraft]]
[[Category:Ilyushin aircraft|Il-214]]
[[Category:International military transport aircraft]]
[[Category:International proposed aircraft]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]
[[Category:T-tail aircraft]]
[[Category:Russian military aircraft 2010–2019]]
[[Category:Indian aircraft 2010–2019]]
[[Category:India–Russia relations]]