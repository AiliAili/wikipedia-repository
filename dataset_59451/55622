{{italic title|force=yes}}
The '''''Anthropological Papers of the University of Alaska''''' is an [[academic journal]] published by the Department of Anthropology at the [[University of Alaska Fairbanks]]. It was established in December 1952 and 25 volumes appeared irregularly through 2000. A new series was begun in 2000; {{as of|2010|lc=yes}} 5 volumes have been published in it.

Several key papers in Alaskan anthropology have appeared in the journal, including [[Edward Vajda]]'s 2010 paper on the [[Dené–Yeniseian languages|Dene–Yeniseian hypothesis]].<ref>{{cite journal |doi=10.1038/476291a |title=Linguistics: Deep relationships between languages |year=2011 |last1=Diamond |first1=Jared |journal=[[Nature (journal)|Nature]] |volume=476 |issue=7360 |pages=291–292 |pmid=21850102}}</ref> {{As of|2012|04}}, the journal and back-issues are available in hard copy only; no electronic version is available.

The journal is abstracted and indexed in [[Anthropological Index Online]].<ref>{{cite web |url=http://aio.anthropology.org.uk/listjnls/getJnls.php |title=AIO - List of indexed journals |publisher=Anthropological Index Online |date= |accessdate=2013-01-11}}</ref> It should not be confused with the ''[[Alaska Journal of Anthropology]]'', published by the [[Alaska Anthropological Association]] beginning in 2001.

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.uaf.edu/anthro/apua/}}
* {{ISSN|0041-9354}}

[[Category:1952 establishments in Alaska]]
[[Category:Anthropology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1952]]
[[Category:Science and technology in Alaska]]
[[Category:University of Alaska Fairbanks]]


{{Alaska-university-stub}}
{{Anthropology-journal-stub}}