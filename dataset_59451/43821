<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=F.B.11
 | image=Vickers F.B.11.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Escort fighter
 | national origin=[[United Kingdom]]
 | manufacturer=[[Vickers Limited]]
 | designer=R.L Howard-Flanders
 | first flight=1916
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Vickers F.B.11''' was a prototype [[United Kingdom|British]] three-seat escort fighter of the [[First World War]].  A large single-engined [[biplane]], it carried one gunner in a nacelle mounted on the upper wing to give an allround field of fire.  Only a single example was completed.

==Development and design==
In early 1916, the British [[War Office]] drew up a specification for a multi-seat escort fighter to be powered by one of the new [[Rolls-Royce Eagle]] engines, intended to protect formations of bombers from German fighters such as the [[Fokker E.I]], with an additional role of destroying enemy [[airship]]s.<ref name="Bruce British p25">Bruce 1957, p.25.</ref> While the specification did not require high speed, a good field of fire for its guns was essential,<ref name="Mason fighter p67">Mason 1992, p.67.</ref> while the secondary anti-Zeppelin role demanded an  endurance of at least seven hours.<ref name="Bruce British p25"/>

Orders were placed for prototypes from [[Armstrong Whitworth Aircraft|Armstrong Whitworth]] (the [[Armstrong Whitworth F.K.6|F.K.6]]), [[Sopwith Aviation Company|Sopwith]] (the [[Sopwith L.R.T.Tr.|L.R.T.Tr.]]) and [[Vickers Limited|Vickers]]. All three designs were driven by the need to provide wide fields of fire in the absence of an effective [[Synchronization gear|synchronisation gear]] that would allow safe firing of guns through the propeller disc.<ref name="Bruce British p25"/>

The Vickers response, the F.B.11, designed by R.L. Howard-Flanders, was a large, single-bay, [[biplane]] of [[Tractor configuration|tractor layout]].  The pilot and one gunner sat in separate but closely spaced cockpits under the trailing edge of the upper wing, while a second gunner sat in a nacelle, or "fighting top", attached to, and extending forward of the upper wing.<ref name="Mason fighter p67"/><ref name="complete fighter p577">Green and Swanborough 1994, p.577.</ref> The Eagle engine was mounted in a clean cowling, with the radiator fitted behind the engine in the fuselage.<ref name="Bruce British p672">Bruce 1957, p.572.</ref>

Two prototypes were ordered, with the first flying in September–October 1916,<ref name="Mason fighter p67"/> being tested at [[RNAS Eastchurch]] in November that year.<ref name="Andrews & Morgan p69">Andrews and Morgan 1988, p.69.</ref> It proved to have poor lateral control and performance, and was destroyed in a crash.<ref name="Bruce v3 p100"/>  The second prototype was not completed,<ref name="complete fighter p578">Green and Swanborough 1994, p.578.</ref> and as effective synchronising gears were now available (including Vickers' own Vickers-Challenger gear), none of the escort fighters were developed further.<ref name="Lewis fighter p99">Lewis 1979, p.99.</ref>

==Specifications==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=War Planes of the First World War:Volume Three Fighters <ref name="Bruce v3 p100">Bruce 1969, p.100.</ref><!-- the source(s) for the information -->
|crew=three (pilot and two gunners)
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 43 ft 0 in 
|length alt=13.11 m
|span main=51 ft 0 in
|span alt=15.55 m
|height main=13 ft 8 in
|height alt=4.17 m
|area main= 845 sq ft
|area alt= 78.5 m²
|airfoil=
|empty weight main= 3,340 lb
|empty weight alt= 1,518 kg
|loaded weight main= 4,934 lb
|loaded weight alt= 2,243 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=
|engine (prop)=[[Rolls-Royce Eagle]] III
|type of prop= water-cooled [[V12 engine|V-12]]<!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 250 hp
|power alt=187 kW
|power original=
|power more=
|max speed main= 96 mph
|max speed alt=83 knots, 155 km/h
|max speed more= at 5,000 ft (1,520 m)
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 11,000 ft
|ceiling alt= 3,350 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=*'''Endurance:''' 7½ hours
*'''Climb to 5,000 ft (1,520 m):''' 16 min 30 s
*'''Climb to 10,000 ft (3,050 m)''' 55 min
|guns= 1× [[.303 British|.303 in]] (7.7 mm) [[Lewis gun]] in nacelle, 1× Lewis gun in rear gunners cockpit
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=*[[Armstrong Whitworth F.K.6]]
*[[Sopwith L.R.T.Tr.]]
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Vickers F.B.11}}
{{refbegin}}
* Andrews, C.F. and Morgan, E.B. ''Vickers Aircraft since 1908''. London:Putnam, 1988. ISBN 0-85177-815-1.
*Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957.
*Bruce, J.M. ''War Planes of the First World War: Volume Three Fighters''. London:Macdonald, 1969. ISBN 0-356-01490-8.
*Green, William and Swanborough, Gordon. ''The Complete Book of Fighters''. New York:Smithmark, 1994. ISBN 0-8317-3939-8.
*Lewis, Peter. ''The British Fighter since 1912''. London:Putnam, Fourth edition, 1979. ISBN 0-370-10049-2.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland, USA:Naval Institute Press, 1992. ISBN 1-55750-082-7

{{refend}}

<!-- ==External links== -->
{{Vickers aircraft}}
{{wwi-air}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Vickers aircraft|F.B.11]]
[[Category:Military aircraft of World War I]]