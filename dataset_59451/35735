{{Good article}}
{{Infobox military conflict
|conflict=Action of 16 October 1799
|partof=the [[French Revolutionary Wars]]
|image= [[File:Ethalion with Thetis.jpeg|300px]]
|caption= "HMS ''Ethalion'' in action with the Spanish frigate ''Thetis'' off Cape Finisterre, 16th October 1799", [[Thomas Whitcombe]], 1800
|date=16–17 October 1799
|place=Off [[Vigo]], [[Galicia (Spain)|Galicia]], [[Atlantic Ocean]]
|result=British victory
|combatant1={{flagcountry|Kingdom of Great Britain}}
|combatant2={{flagcountry|Spain|1785}}
|commander1=[[William Pierrepont (Royal Navy officer)|William Pierrepont]]
|commander2= Don Juan de Mendoza<br />Don Antonio Pillon
|strength1=[[Frigates]] [[HMS Ethalion (1797)|HMS ''Ethalion'']], [[HMS Naiad (1797)|HMS ''Naiad'']], [[HMS Alcmene (1794)|HMS ''Alcmene'']] and [[HMS Triton (1796)|HMS ''Triton'']]
|strength2=[[Frigates]] [[Spanish frigate Thetis|''Thetis'']] and [[Spanish frigate Santa Brigida|''Santa Brigida'']]
|casualties1=1 killed, 10 wounded
|casualties2=3 killed, 17 wounded<br>both frigates captured.
}}
{{Campaignbox French Revolutionary Wars: Anglo-Spanish War (1796)}}
The '''Action of 16 October 1799''' was a minor naval engagement during the [[French Revolutionary Wars]] between a squadron of British [[Royal Navy]] [[frigates]] and two [[frigates]] of the [[Spanish Navy]] close to the Spanish naval port of [[Vigo]] in [[Galicia (Spain)|Galicia]]. The Spanish ships were a treasure convoy, carrying silver [[Bullion coins|specie]] and luxury trade goods across the [[Atlantic Ocean]] from the colonies of [[New Spain]] to [[Spain]]. Sighted by British frigate [[HMS Naiad (1797)|HMS ''Naiad'']] enforcing the [[blockade]] of Vigo late on the 15 October, the Spanish ships were in the last stages of their journey. Turning to flee from ''Naiad'', the Spanish soon found themselves surrounded as more British frigates closed in.

Although they separated their ships in an effort to split their opponents, the Spanish captains were unable to escape: [[Spanish frigate Thetis|''Thetis'']] was captured after a short engagement with [[HMS Ethalion (1797)|HMS ''Ethalion'']] on the morning of 16 October, while [[Spanish frigate Santa Brigida|''Santa Brigida'']] almost reached safety, only being caught on the morning of 17 October in the approaches to the safe harbour at [[Muros, A Coruña|Muros]]. After a short engagement amid the rocks she was also captured by an overwhelming British force. Both captured ships were taken to Britain, where their combined cargoes were transported with great fanfare to the [[Bank of England]]. The eventual value of their cargo was assessed as at least £618,040, resulting in one of the largest hauls of [[prize money]] ever awarded.

==Background==
In 1796, following the secret terms of the [[Second Treaty of San Ildefonso|Treaty of San Ildefonso]], the [[Kingdom of Spain]] suddenly reversed its position in the [[French Revolutionary Wars]] turning from an enemy of the [[First French Republic|French Republic]] into an ally.<ref name="JH104">Henderson, p. 104</ref> The Spanish declaration of war on [[Kingdom of Great Britain|Great Britain]] forced the British Mediterranean Fleet to abandon the [[Mediterranean Sea]] entirely, retreating to ports at [[Gibraltar]] and [[Lisbon]].<ref name="RG120">Gardiner, p. 120</ref> This force now concentrated against the [[Spanish Navy]], most of which was stationed at the main fleet base of [[Cádiz]] in Southern Spain. A British [[blockade]] fleet won a significant victory over the Spanish at the [[Battle of Cape St Vincent (1797)|Battle of Cape St Vincent]] in February 1797, dissuading the Spanish fleet from playing a significant role in the ongoing war.<ref name="RG90">Gardiner, p. 90</ref>

Other Spanish ports were also blockaded with the intention of limiting Spanish trade and movement and intercepting treasure convoys from the colonies of [[New Spain]] and South America. Vast quantities of gold, silver and valuable trade goods crossed the Atlantic in regular armed [[frigate]] convoys.<ref name="JH103">Henderson, p. 103</ref> To intercept and seize these shipments the Royal Navy dispatched their own frigates to patrol the Spanish coast. To encourage their sailors, the Royal Navy distributed [[prize money]] to the value of the ships and material captured and the seizure of a Spanish treasure fleet could yield spectacular amounts of money: particularly large sums had been captured during previous wars in 1656, 1744 and 1762,<ref name="JH104"/> but during the first three years of conflict between Great Britain and Spain only one treasure convoy had been intercepted, near Cádiz at the [[Action of 26 April 1797]], and on that occasion the treasure was smuggled ashore before the convoy was seized.<ref name="RW99">Woodman, p. 99</ref>

==Pursuit==
On 21 August 1799, a convoy of two 34-gun frigates, [[Spanish frigate Thetis|''Thetis'']] under Captain Don Juan de Mendoza and [[Spanish frigate Santa Brigida|''Santa Brigida'']] under Captain Don Antonio Pillon, sailed from [[Veracruz, Veracruz|Vera Cruz]] in New Spain with a cargo that included [[cochineal]], [[indigo dye]], [[Cocoa bean|cocoa]] and [[sugar]] but which principally consisted of more than two million silver [[Spanish dollars]].<ref name="WJ358"/> The passage across the Atlantic was uneventful and by the afternoon of 15 October the convoy, under orders to make any Spanish port, was nearing its destination at [[Vigo]], a fortified port city in [[Galicia (Spain)|Galicia]] just south of [[Cape Finisterre]] at the most northwestern point of Spain. The ports of Northern Spain were blockaded by British frigates sailing independently, crossing the approaches in search of enemy shipping and it was one such ship, the 38-gun [[HMS Naiad (1797)|HMS ''Naiad'']] under Captain [[William Pierrepont (Royal Navy officer)|William Pierrepont]], that sighted the Spanish convoy in position {{coord|41|01|N|12|35|W|type:event}} at 20:00 on 15 October.<ref name="WJ356">James, p. 356</ref> Turning away to the southeast, the Spanish ships then made all sail northeast in search of a safe harbour, with Pierrepont in pursuit.<ref name="WLC525">Clowes, p. 525</ref>

At 03:30 on 16 October, another sail was spotted to the southwest, rapidly revealed to be a second British frigate, the 38-gun [[HMS Ethalion (1797)|HMS ''Ethalion'']] under Captain [[James Young (1762–1833)|James Young]]. ''Ethalion'' joined the chase and at dawn two more sails were sighted, the 32-gun [[HMS Alcmene (1794)|HMS ''Alcmene'']] under Captain [[Henry Digby (Royal Navy officer)|Henry Digby]] to the west and 32-gun [[HMS Triton (1796)|HMS ''Triton'']] under Captain [[John Gore (Royal Navy admiral)|John Gore]] to the north.<ref name="JH105">Henderson, p. 105</ref> With four British frigates now in full pursuit, the Spanish captains sought to split their enemy and divided, at which Pierrepont directed ''Ethalion'', the closest British ship, to pursue the faster ''Thetis''. Young complied, firing long-range shot in ''Santa Brigida''<nowiki>'</nowiki>s direction at 09:00, driving Pillon's ship further from his companion.<ref name="WJ357">James, p. 357</ref>

==Battle==
As ''Naiad'', ''Triton'' and ''Alcmene'' streamed past in pursuit of ''Santa Brigida'', Young focused his attention on ''Thetis'', coming within range at 11:30.<ref name="WLC526"/> Mendoza, seeing that battle was inevitable, bore up across ''Ethalion''<nowiki>'</nowiki>s bows in an effort to [[raking fire|rake]] Young's ship. Young turned in order to thwart the manoeuvre and fired two rapid [[broadside]]s into ''Thetis'', which responded in kind.<ref name="RW132"/> For an hour the frigates exchanged running fire until Mendoza, realising escape was impossible, surrendered. ''Thetis'' had lost one man killed and nine wounded in the exchange while ''Ethalion'' had suffered no casualties.<ref name="LG1">{{London Gazette|issue=15197|startpage=1093|endpage=1095|date=22 October 1799|accessdate=27 March 2013}}</ref>

As ''Ethalion'' subdued ''Thetis'' the remainder of the British squadron continued southwards in pursuit of ''Santa Brigida''. Pillon was an experienced officer with a good knowledge of the Northern Spanish coast and he intended to lose his pursuers in the rocky channels of Cape Finisterre.<ref name="JH105">Henderson, p. 105</ref> Early on 17 October he reached Spanish coastal waters, rounding Finisterre just beyond the Monte Lora rocks. Captain Gore on ''Triton'', which was in full flow at seven [[Knot (unit)|knots]], was unaware of the obstacle and at 05:00 crashed into them, coming to a juddering halt and inflicting severe damage to his ship's hull. Gore was able however to bring ''Triton'' off soon afterwards and continued pursuit, assisted by Digby on ''Alcmene'' who was able to block Pillon's route into Porte de Vidre.<ref name="WJ358"/> Both frigates opened fire on ''Santa Brigida'' at 07:00 as the Spanish ship sought shelter in the rocks at Commarurto close to the safe harbour at [[Muros, A Coruña|Muros]], Pillon's movement hampered by the coastal ''[[ria]]s'' that blocked the wind.<ref name="RW133">Woodman, p. 133</ref> After an hour of resistance, with ''Naiad'' belatedly approaching, Pillon was forced to surrender his ship to superior British forces. ''Santa Brigida'' had lost two men killed and eight wounded, ''Alcmene'' one killed and nine wounded and ''Triton'' a single man wounded.<ref name="LG1"/>

==Aftermath==
As the British force took control of ''Santa Brigida'', a Spanish squadron of four ships sailed from Vigo with the appearance of intending to bring Pierrepont's squadron to battle. Pierrepont immediately issued orders for his ships to meet with the Spanish who promptly turned about and returned to port without coming within range.<ref name="WJ358"/> A shore breeze enabled the British ships and their prize to extricate themselves from the Commarurto rocks without further damage.<ref name="WLC526">Clowes, p. 526</ref> They then sailed directly for the fleet base at [[Plymouth]], arriving on 22 October to find that ''Thetis'' and ''Ethalion'' had reached the port the day before. Dispatches were sent to [[Alexander Hood, 1st Viscount Bridport|Lord Bridport]], commander of the Channel Fleet, which were then forwarded to the [[Admiralty]] and revealed the scale of the prize.<ref name="LG1"/>

Aboard ''Thetis'' was found a quantity of trade cocoa and a series of boxes containing coin, including 333 boxes of 3,000 dollars each, four boxes of 2,385 dollars each, 94 boxes containing 4,000 dollars each and two golden [[doubloons]] and 90 golden half-doubloons. This totalled 1,385,292 silver dollars, with a sterling value of £311,690.<ref name="WJ357"/> On ''Santa Brigida'' were trade cocoa, sugar, indigo and cochineal worth in total about £5,000 as well as 446 boxes containing 3,000 dollars each, 59 bags and three kegs of dollars and numerous loose coins, for  a total value of at least 1,338,000 silver dollars or £301,350. Altogether the sterling value of the cargo was calculated as not less than £618,040 (the equivalent of £{{Formatnum:{{Inflation|UK|618040|1799|r=-3}}|0}} in {{CURRENTYEAR}}).{{Inflation-fn|UK}} The captured ships however were written off as worthless, although some additional money was made auctioning off their naval stores.<ref name="WJ358"/> In the aftermath, the sailors of the squadron were noted in the streets of [[Portsmouth]] wearing "bank notes stuck in their hats, buying watches for the fun of frying them, and issuing laws that any of their crew who appeared without a gold-laced hat should be [[paddle (spanking)|cobbed]], so that the unlucky man who appeared in silver could only escape by representing that the costlier articles were all bought up, but he had compelled the shopkeeper to take money for gold lace."<ref name="DH133">Hannay, pp.133–134</ref>

This vast sum of money was transported through Plymouth on 63 wagons, guarded by armed sailors and [[Royal Marines]] and accompanied by musical bands and cheering crowds to the security of the [[Royal Citadel, Plymouth|Royal Citadel]]. It remained in Plymouth until November when it was removed to London with considerable ceremony and placed at the [[Bank of England]].<ref name="RW133"/> The sums awarded as [[prize money]], distributed in equal proscribed shares among the crews of ''Ethalion'', ''Naiad'', ''Alcmene'' and ''Triton'' were among the largest ever recorded. Each captain was given £40,730<ref name=Inf2>The equivalent of £{{Formatnum:{{Inflation|UK|40730|1799|r=-1}}|0}} in {{CURRENTYEAR}}</ref> (of which a third was due to the admiral in command), each lieutenant £5,091,<ref name=Inf6>The equivalent of £{{Formatnum:{{Inflation|UK|5091|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref> each warrant officer £2,468,<ref name=Inf4>The equivalent of £{{Formatnum:{{Inflation|UK|2468|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref> each midshipman £791<ref name=Inf1>The equivalent of £{{Formatnum:{{Inflation|UK|719|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref>  and each sailor or marine £182.<ref name="WJ358">James, p. 358</ref><ref name=Inf7>The equivalent of £{{Formatnum:{{Inflation|UK|182|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref> For the regular seamen, this total was 15 times their annual pay of £12.<ref name=Inf3>The equivalent of £{{Formatnum:{{Inflation|UK|12|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref> As historian James Henderson noted "even the humblest seaman could set himself up with a cosy pub".<ref name="JH106">Henderson, p. 106.</ref> For the captains, normally paid £150 a year,<ref name=Inf8>The equivalent of £{{Formatnum:{{Inflation|UK|150|1799|r=0}}|0}} in {{CURRENTYEAR}}</ref> this was more money than they could make in 270 years.<ref name="JH106"/> On the only subsequent occasion when a Spanish treasure fleet was successfully intercepted, at the [[Battle of Cape Santa Maria]] in October 1804, an even greater haul was captured. On that occasion however the Admiralty used an obscure regulation to seize the bulk of the prize and the captains only received around £15,000 each.<ref name=Inf5>The equivalent of £{{Formatnum:{{Inflation|UK|15000|1804|r=-1}}|0}} in {{CURRENTYEAR}}</ref><ref name="JH109">Henderson, p. 109.</ref>

Historian [[Richard Woodman]] has noted that this action illustrates both the dominance of the Royal Navy and its high standards at this stage in the war, stating that "The coincidental appearance of four frigates in the vast Atlantic testifies to the enormous resources the British put into the prosecution of the war. That the four frigate captains proceeded to act in such perfect concert is further evidence, if it were needed, of the shared standards of mutual help and assistance".<ref name="RW132">Woodman, p. 132</ref>

==Notes==
{{reflist|2}}

==References==
* {{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997
 | origyear= 1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume IV
 | publisher = Chatham Publishing
 | location = London
 | isbn = 1-86176-013-2
}}
* {{cite book
 | author = Gardiner, Robert (editor)
 | year = 2001
 | origyear= 1996
 | chapter =
 | title = Fleet Battle and Blockade
 | publisher = Caxton Editions
 | isbn = 978-1-84067-363-0
}}
* {{cite book
 | last = Hannay
 | first = David
 | authorlink =
 | year = 1886
 | origyear=
 | chapter =
 | title = Admiral Blake
 | publisher = D. Appleton and Company
 | location = New York
 | isbn = 978-1-44378-410-8
}}
* {{cite book
 | last = Henderson CBE
 | first = James
 | authorlink =
 | year = 1994
 | origyear= 1970
 | chapter =
 | title = The Frigates
 | publisher = Leo Cooper
 | location =
 | isbn = 0-85052-432-6
}}
* {{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002
 | origyear= 1827
 | chapter =
 | title = The Naval History of Great Britain, Volume 2, 1797–1799
 | publisher = Conway Maritime Press
 | location = London
 | isbn = 0-85177-906-9
}}
* {{cite book
 | last = Woodman
 | first = Richard
 | authorlink = Richard Woodman
 | year = 2001
 | chapter =
 | title = The Sea Warriors
 | publisher = Constable Publishers
 | isbn = 1-84119-183-3
}}

[[Category:Conflicts in 1799]]
[[Category:Naval battles involving Great Britain]]
[[Category:Naval battles involving Spain]]
[[Category:Naval battles of the French Revolutionary Wars]]
[[Category:October 1799 events]]