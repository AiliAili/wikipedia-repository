{{cleanup|date=May 2011}}

[[File:Thermo-acoustic_cooling_machine.png|thumb|400px|A schematic representation of a thermoacoustic hot-air engine. The Heat exchanger (heat bridge not shown) has a hot side which conducts heat to or from a hot heat reservoir – and a cold side (cold bridge not shown) that conducts heat to or from a cold heat reservoir. The electro-acoustic transducer, e.g. a loudspeaker, is not shown.]]

'''Thermoacoustic engines''' (sometimes called "TA engines") are [[Thermoacoustics|thermoacoustic]] devices which use high-amplitude sound waves to [[heat pump|pump heat]] from one place to another, or conversely use a heat difference to induce high-amplitude sound waves. In general, thermoacoustic engines can be divided into [[standing wave]] and [[Wave|travelling wave]] devices. These two types of thermoacoustics devices can again be divided into two [[Thermodynamics|thermodynamic]] classes, a prime mover (or simply [[heat engine]]), and a [[heat pump]]. The prime mover creates work using heat, whereas a heat pump creates or moves heat using work.
Compared to [[Vapor-compression refrigeration|vapor refrigerators]], thermoacoustic refrigerators have no [[ozone]]-depleting or toxic coolant and few or no moving parts therefore require no dynamic sealing or lubrication.<ref>{{cite journal | author=Ceperley, P. | title=A pistonless Stirling engine – the travelling wave heat engine | journal=J. Acoust. Soc. Am. | year=1979 | volume=66 | pages=1508–1513 | doi=10.1121/1.383505|bibcode = 1979ASAJ...66.1508C }}</ref>

== Operation ==

=== Overview of device ===
A thermoacoustic device basically consists of [[heat exchanger]]s, a [[resonator]], and a stack (on standing wave devices) or [[regenerative heat exchanger|regenerator]] (on travelling wave devices). Depending on the type of engine a [[Speaker driver|driver]] or [[loudspeaker]] might be used as well to generate sound waves.

Consider a tube closed at both ends. Interference can occur between two waves traveling in opposite directions at certain frequencies. The interference causes [[resonance]] creating a standing wave. Resonance only occurs at certain frequencies called resonance frequencies, and these are mainly determined by the length of the resonator.

The stack is a part consisting of small parallel channels. When the stack is placed at a certain location in the resonator, while having a standing wave in the resonator, a temperature difference can be measured across the stack. By placing heat exchangers at each side of the stack, heat can be moved. The opposite is possible as well, by creating a temperature difference across the stack, a sound wave can be induced. The first example is a simple heat pump, while the second is a prime mover.

=== Heat pumping ===
To be able to create or move heat, work must be done, and the acoustic power provides this work. When a stack is placed inside a resonator a pressure drop occurs. Interference between the incoming and reflected wave is now imperfect since there is a difference in amplitude causing the standing wave to travel little, giving the wave acoustic power.

In the acoustic wave, parcels of gas [[Adiabatic process | adiabatically]] compress and expand. Pressure and temperature change simultaneously; when pressure reaches a maximum or minimum, so does the temperature. Heat pumping along a stack in a standing wave device can now be described using the [[Brayton cycle]].

Below is the counter-clockwise Brayton cycle consisting of four processes for a [[refrigerator]] when a parcel of gas is followed between two plates of a stack.
# ''Adiabatic compression of the gas.'' When a parcel of gas is displaced from its rightmost position to its leftmost position, the parcel is adiabatic compressed and thus the temperature increases. At the leftmost position the parcel now has a higher temperature than the warm plate.
# ''[[Isobaric process | Isobaric]] heat transfer.'' The parcel's temperature is higher than that of the plate causing it to transfer heat to the plate at constant pressure losing temperature.
# ''Adiabatic expansion of the gas.'' The gas is displaced back from the leftmost position to the rightmost position and due to adiabatic expansion the gas is cooled to a temperature lower than that of the cold plate.
# ''Isobaric heat transfer.'' The parcel's temperature is now lower than that of the plate causing heat to be transferred from the cold plate to the gas at a constant pressure, increasing the parcel's temperature back to its original value.

Travelling wave devices can be described using the [[Stirling cycle]].

=== Temperature gradient ===
An engine and heat pump both typically use a stack and heat exchangers. The boundary between a prime mover and heat pump is given by the temperature gradient operator, which is the mean temperature gradient divided by the critical temperature gradient.

:<math>\Iota = \frac{\nabla T_{m}}{\nabla T_{crit}} </math>

The mean temperature gradient is the temperature difference across the stack divided by the length of the stack.

:<math>\nabla T_{m} = \frac{\Delta T_{m}}{\Delta x_{stack}}</math>

The critical temperature gradient is a value depending on certain characteristics of the device like frequency, cross-sectional area and gas properties.

If the temperature gradient operator exceeds one, the mean temperature gradient is larger than the critical temperature gradient and the stack operates as a prime mover. If the temperature gradient operator is less than one, the mean temperature gradient is smaller than the critical gradient and the stack operates as a heat pump.

=== Theoretical efficiency ===
In thermodynamics the highest achievable efficiency is the [[Carnot's theorem (thermodynamics)|Carnot]] efficiency. The efficiency of thermoacoustic engines can be compared to Carnot efficiency using the temperature gradient operator.

The efficiency of a thermoacoustic engine is given by

:<math>\eta = \frac{\eta_{c}}{\Iota}</math>

The [[coefficient of performance]] of a thermoacoustic heat pump is given by

:<math>COP = \Iota \cdot COP_{c}</math>

== Derivations ==
Using the [[Navier–Stokes equations|Navier-Stokes equations]] for fluids, Rott was able to derive equations specific for thermoacoustics.<ref>Advances in Applied Mechanics

Volume 20, 1980, Pages 135–175</ref> Swift continued with these equations, deriving expressions for the acoustic power in thermoacoustic devices.<ref name=Swift>{{cite journal|last1=Swift|first1=Gregory W.|title=Thermoacoustic engines|journal=The Journal of the Acoustical Society of America|date=1988|volume=84|page=1145|url=http://scitation.aip.org/content/asa/journal/jasa/84/4/10.1121/1.396617|accessdate=9 October 2015|doi=10.1121/1.396617|bibcode=1988ASAJ...84.1145S}}</ref>

== Efficiency in practice ==
The ''most'' efficient thermoacoustic devices built to date have an efficiency approaching 40% of the [[Carnot heat engine#Efficiency of real heat engines|Carnot]] limit, or about 20% to 30% overall (depending on the [[heat engine]] temperatures).<ref>web archive backup: [http://web.archive.org/web/20080801212651/www.lanl.gov/mst/engine/ lanl.gov: More Efficient than Other No-Moving-Parts Heat Engines]</ref>

Higher hot-end temperatures may be possible with thermoacoustic devices because there are no [[moving parts]], thus allowing the Carnot efficiency to be higher. This may partially offset their lower efficiency, compared to conventional heat engines, as a percentage of Carnot.

The ideal Stirling cycle, approximated by traveling wave devices, is inherently more efficient than the ideal Brayton cycle, approximated by standing wave devices.  However, the narrower pores required to give good thermal contact in a travelling wave regenerator, as compared to a standing wave stack which requires deliberately imperfect thermal contact, also gives rise to greater frictional losses, reducing the efficiency of a practical engine.  The [[toroid]]al geometry often used in traveling wave devices, but not required for standing wave devices, can also give rise to losses due to Gedeon streaming around the loop.{{elucidate|date=July 2015}}

== Research in thermoacoustics ==
Modern research and development of thermoacoustic systems is largely based upon the work of Rott (1980)<ref>{{cite journal | author=Rott, N. | title=Thermoacoustics | journal=Adv. Appl. Mech. | year=1980 | volume=20 | issue=135 | doi=10.1016/S0065-2156(08)70233-3 | pages=135–175}}</ref> and later Greg Swift (1988),<ref>{{cite journal | author=Swift, G.W. | title=Thermoacoustic engines | journal=J. Acoust. Soc. Am. | year=1988 | volume=84 | pages=1145–1180 | doi=10.1121/1.396617|bibcode = 1988ASAJ...84.1145S }}</ref> 
in which linear thermoacoustic models were developed to form a basic quantitative understanding, and numeric models for computation. Commercial interest has resulted in niche applications such as small to medium scale [[cryogenic]] applications.

=== History ===
The history of thermoacoustic hot air engines started about 1887, when [[John Strutt, 3rd Baron Rayleigh|Lord Rayleigh]] discussed the possibility of pumping heat with sound. Little further research occurred until Rott's work in 1969.<ref>[http://uw.physics.wisc.edu/~timbie/P325/Fahey_thermoacoustic_oscillations.pdf Thermoacoustic Oscillations, Donald Fahey, Wave Motion & Optics, Spring 2006, Prof. Peter Timbie]</ref>

A very simple thermoacoustic hot air engine is the [[Rijke tube]] that converts heat into [[acoustic energy]].<ref>[[Pieter Rijke|P. L. Rijke]] (1859) Philosophical Magazine, '''17''', 419–422.</ref> This device however uses natural convection.

=== Current research ===
Orest Symko at [[University of Utah]] began a research project in 2005 called ''Thermal Acoustic Piezo Energy Conversion'' (TAPEC).<ref>[http://www.physorg.com/pdf100141616.pdf physorg.com: A sound way to turn heat into electricity (pdf)] Quote: "...Symko says the devices won’t create noise pollution...Symko says the ring-shaped device is twice as efficient as cylindrical devices in converting heat into sound and electricity. That is because the pressure and speed of air in the ring-shaped device are always in sync, unlike in cylinder-shaped devices..."</ref>

Score Ltd. was awarded £2M in March 2007 to research a cooking stove that will also deliver electricity and cooling using the Thermo-acoustic effect for use in developing countries.<ref>[http://arstechnica.com/news.ars/post/20070527-new-stove-generator-refrigerator-combo-aimed-at-developing-nations.html May 27, 2007, Cooking with sound: new stove/generator/refrigerator combo aimed at developing nations]</ref><ref>[http://www.score.uk.com/research/ SCORE (Stove for Cooking, Refrigeration and Electricity)], [http://www.score.uk.com/research/Scorepics/Forms/DispForm.aspx?ID=11 illustration]</ref>

A radioisotope-heated thermoacoustic system has been proposed and prototyped for deep space exploration missions by Airbus. The system has theoretical slight advantages over other generator systems like existing [[thermocouple]] based systems, or proposed [[Stirling engine]] used in [[Advanced Stirling radioisotope generator|ASRG]] prototype.<ref name=Airbus>{{cite web|title=Thermo-Acoustic Generators for space missions|url=http://pamir.sal.lv/2014/cd/container/B.7.02=DidierA_Pamir14.pdf}}</ref>

== See also ==
*[[Sound Amplification by Stimulated Emission of Radiation|SASER]], Sound Amplification by Stimulated Emission of Radiation

== References ==
{{Reflist|30em}}

== Further reading ==
{{Refbegin}}
* {{cite journal | last1=Gardner |first1=D. |last2=Swift |first2=G. | title=A cascade thermoacoustic engine | journal=J. Acoust. Soc. Am. | year=2003 | volume=114 | issue=4 | pages=1905–1919 | doi=10.1121/1.1612483 | pmid=14587591|bibcode = 2003ASAJ..114.1905G }}
* {{cite journal |last1=Garrett |first1=Steven |last2=Backaus |first2=Scott |title=The Power of Sound |work=The American Scientist |volume=88 |page=561 |date=November 2000 |doi=10.1511/2000.6.516 |url=http://www.americanscientist.org/issues/pub/the-power-of-sound/1}} Semipopular introduction to thermoacoustic effects and devices.
* Frank Wighard "Double Acting Pulse Tube Electroacoustic System" US Patent 5,813,234
{{Refend}}

== External links ==
{{Commons category|Thermoacoustic heat engines}}
{{wikibooks|1=Engineering Acoustics|2=Thermoacoustics}}
* [http://www.lanl.gov/thermoacoustics/ Los Alamos National Laboratory, New Mexico, USA]
* [http://www.acs.psu.edu/thermoacoustics/ Penn State University, USA]
* [http://www.americanscientist.org/issues/feature/the-power-of-sound/ The Power of Sound], ''American Scientist Online''
* [http://www.mecheng.adelaide.edu.au/avc/thermoacoustics/ Thermoacoustics at the University of Adelaide, Australia], web archive backup: [https://web.archive.org/web/20080723204600/www.mecheng.adelaide.edu.au/anvc/thermoacoustics/forum/ Discussion Forum]
*[http://www.mecheng.adelaide.edu.au/avc/publications/public_papers/2005/preprint_bamman_aas2005.pdf  Adelaide University]
* [https://www.wired.com/science/discoveries/news/2003/01/57063 Hear That? The Fridge Is Chilling], [[Wired Magazine]] article
* [http://www.scribd.com/doc/147785416/Experimental-Investigations-on-a-Standing-Wave-Thermoacoustic-Engine#page=52 "Experiments on a Standing-Wave Thermoacoustic Engine"]
{{Heat engines}}

{{DEFAULTSORT:Thermoacoustic Hot Air Engine}}
[[Category:Hot air engines| ]]
[[Category:Cooling technology]]
[[Category:Heat pumps]]
[[Category:Acoustics]]