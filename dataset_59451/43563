<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Rhönadler
 | image=File:Rhonadler_D-Gunther_Groenhoff_1932-34.Nachbau_(8656384467).jpg
 | caption=A Rhönadler in [[The German Gliding Museum]]
}}{{Infobox Aircraft Type
 | type=Single seat competition [[glider (sailplane)|glider]]
 | national origin=[[Germany]]
 | manufacturer=[[Alexander Schleicher GmbH & Co]]
 | designer=[[Hans Jacobs]]
 | first flight=1932
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=(Rhönadler 35) 65
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Schleicher Rhönadler''',<ref name=Hardy/> '''DFS Rhönadler'''<ref name=Zuerl/> or '''Jacobs Rhönadler'''<ref name=Ogden/> is a high performance, single seat competition [[sailplane]] built in [[Germany]] in the 1930s.<ref name=Note1 group=Notes/> More than 65 were built.

==Design and development==
About 1931 the glider manufacturer [[Alexander Schleicher]] went to [[Hans Jacobs]], then at the RRG ([[Rhön-Rossitten Gesellschaft]]) on the [[Wasserkuppe]], for glider design that, like the [[RRG Fafnir]] designed by [[Alexander Lippisch]], was capable making long cross country by travelling quickly between thermals but could be put into series production making it cheaper to build.  Jacobs responded with the Rhönadler (in English, [[Rhön]] eagle), ready for the 1932 Rhön competition.  The following year both Jacobs and Lippisch had to transfer to the state owned DFS ([[Deutsche Forschungsanstalt Für Segelflug]]) at [[Darmstadt]], where Jacobs continued to refine the Rhönbussard, hence the DFS Rhönadler name.<ref name=SimonsI/>

The Rhönadler was a wood framed aircraft with [[plywood]] and [[aircraft fabric covering|fabric]] covering. In plan its [[monoplane#Types of monoplane|high wing]] wings were straight tapered with a torsion resting D-box form by ply skin ahead of the single [[Spar (aviation)|spar]].  Aft of the spar the wings were fabric covered.  The Rhönadler [[wing root]] used a version of the thick Göttingen 652 section, modified by a reduction of its high [[camber (aerodynamics)|camber]]; further outboard this turned into the progressively lower camber Göttingen 535 and Clark Y airfoils.  [[dihedral (aircraft)|Dihedral]] was constant, to avoid the constructional complication of the Fafnir's [[gull wing]].  The [[aileron]]s were very long, occupying more than half the span. Though neither the prototype nor later production aircraft came with [[spoiler (aeronautics)|spoiler]]s or [[air brake (aircraft)|airbrakes]] such devices, opening above the wing, were often retro-fitted.<ref name=SimonsI/>

The Rhönadler's [[fuselage]] was quite slender and entirely ply covered, including the [[fin]], the balancing part of the [[rudder]] and a small tail bumper.  On the original version, even the [[cockpit]] [[canopy (aircraft)|canopy]] was a ply structure, with small, unglazed apertures  for vision.  This was progressively modified with increasing glazing into the 1935 variant's multi-framed conventional canopy.  To avoid the wing root aerodynamic interference that the Fafnir's gull wing was intended to avoid, the Rhönadler's wing was mounted just above the fuselage on a low, narrow neck or pedestal which placed the [[leading edge]] level with the top of the canopy. The high [[aspect ratio (wing)|aspect ratio]], [[stabilator|all-moving horizontal tail]] was of similar construction to the wing, with most of the taper on the [[trailing edge]] where there was a deep cut-out at the root.  The tailplane was low set on the prototype but raised just above the [[Dorsal (anatomy)|dorsal]] fuselage line on production aircraft.  The broad chord, balanced rudder was also fabric covered.  Landings were made on a sprung skid.<ref name=Hardy/><ref name=SimonsI/>

==Operational history==

At the 1932 Rhön the Rhönadler, flown by [[Peter Riedel]], did not win but impressed enough to go into series production as the Rhönadler 32, with the prototype's wing span slightly shortened, its vertical tail leading edge smoothed by a shorter rudder balance and its tailplane raised.  This sold well, though production numbers are uncertain. The 1935 version's alterations included a fully enclosed transparent canopy.  Schleicher built sixty-five of them, making it the top selling German high performance glider; several were exported.  A measure of the popularity of the Rhönadler is the number at the 1935 Rhön competition, twenty-three out of sixty contestants.<ref name=SimonsI/>

In the 1980s a new Rhönadler was built from original plans and flown. It is now in the Wasserkuppe museum.<ref name=SimonsI/>

==Variants==
;Rhönadler: Prototype.  Competed in the 1932 Rhön event. 18&nbsp;m (59&nbsp;ft&nbsp;1&nbsp;in) span. Ply fairing over cockpit with small oval openings for vision. 
;Rhönadler 32: First production version, with raised tailplane and simplified fin and rudder. Span reduced by 542&nbsp;mm (21.3&nbsp;in). Transparencies at the front of the cockpit. 
;Rhönadler 35: Second production version with full cockpit transparencies. 
;[[DFS Seeadler|Seeadler]]: Flying boat version with hull, markedly gulled wings and underwing stabilizing floats.

==Aircraft on display==
* Deutsches Segelflugmuseum mit Modellflug, [[Wasserkuppe]]:1980s built Rhönadler 35.<ref name=Ogden/><ref name=SimonsI/>

==Specifications (35)==
{{Aircraft specs
|ref=Die berümtesten Segelflugzeuge<ref name="Brütting"/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=
|length m=7.20
|length note=
|span m=17.40
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=18.0
|wing area note=
|aspect ratio=16.8
|airfoil=Root: modified Göttingen 652; mid-span: Göttingen 535; tip: Clark Y
|empty weight kg=170
|empty weight note=
|gross weight kg=250
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=130
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=50
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=20:1
|sink rate ms=0.75
|sink rate note=minimum
|lift to drag=
|wing loading kg/m2=13.6
|wing loading note=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|group=Notes|refs=
<ref name=Note1 group=Notes>The [[Schleicher K7]], a 1960s two seat glider [[training aircraft|training glider]], is also named Rhönadler: it is a completely independent design from another designer.  There was yet another Rhönadler, a record setting two seat glider dating from before the 1932 design.</ref> 
}}

==References==
{{reflist|refs=

<ref name="Brütting">{{cite book| language=de| title=Die berümtesten Segelflugzeuge| trans_title=The most famous Sailplanes| last=Brütting |first=Georg|year=1973|publisher=Motorbuch Verlag|location=Stuttgart |isbn=3-87943-171-X|pages=44–5}}</ref>

<ref name=Hardy>{{cite book |title=Gliders & Sailplanes of the World|last= Hardy |first= Michael |coauthors= |edition= |year=1982|publisher=Ian Allan Ltd|location= London|isbn=0-7110-1152-4|page=97 }}</ref>

<ref name=Ogden>{{cite book |title= Aviation Museums and Collections of North America|last=Ogden|first=Bob| year=2011|edition=2nd|publisher=Air-Britain (Historians)  |location=Tonbridge, Kent |isbn=0-85130-427-3|page=210}}</ref>

<ref name=SimonsI>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3-9806773-4-6|pages=106–110}}</ref>

<ref name=Zuerl>{{cite book |language=de |title=Segelflug |last= Zuerl |first=Hubert |coauthors= |edition= |year=1941 |publisher=E. S. Mittler & Sohn |location= Berlin |page=28 }}</ref>

}}
<!-- ==Further reading== -->

==External links==
{{commons category|Alexander Schleicher}}
*[http://www.ae.illinois.edu/m-selig/ads/afplots/goe652.gif Göttingen 652 airfoil]
*[http://www.ae.illinois.edu/m-selig/ads/afplots/goe535.gif Göttingen 535 airfoil]
*[http://www.ae.illinois.edu/m-selig/ads/afplots/clarky.gif Clarke Y airfoil]

<!-- Navboxes go here -->
{{Schleicher}}
{{Hans Jacobs aircraft}}
{{DFS aircraft}}

{{DEFAULTSORT:Schleicher Rhonadler}}
[[Category:German sailplanes 1930–1939]]
[[Category:Schleicher aircraft]]
[[Category:DFS aircraft]]