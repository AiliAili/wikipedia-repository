{{Infobox NRHP
| name = Judaculla Rock
| nrhp_type = 
| image = JudaCulla Rock.JPG
| caption = Judaculla Rock
| location= 552 Judaculla Rock Rd., [[Cullowhee, North Carolina]]
| coordinates = {{coord|35|18|02|N|83|06|34|W|display=inline,title}}
| locmapin = North Carolina#USA
| built =  
| architect = 
| added = March 27, 2013
| area = <!-- {{convert|?|acre}} -->
| governing_body = Local   
| refnum = 13000116<ref name="nps">{{cite web|url=http://www.nps.gov/nr/listings/20130412.htm|title=National Register of Historic Places Listings|date=2013-04-12|work=Weekly List of Actions Taken on Properties: 4/01/13 through 4/05/13|publisher=National Park Service}}</ref>
}}
'''Judaculla Rock''' is a curvilinear-shaped outcrop of [[soapstone]] with quarry scars and [[petroglyphs]]. It is located on a 0.85-acre rectangular-shaped property, owned by [[Jackson County, North Carolina|Jackson County]], approximately 60 meters east of Caney Fork Creek, a major branch of the northwestward-trending [[Tuckasegee River]], in the mountains of western [[North Carolina]].

The petroglyph boulder occurs within an artificially created bowl-shaped depression. Today this is covered with mowed grass (it was previously cultivated as a corn field) and bordered on the west by a thicket of river cane ([[Arundinaria gigantea]]). Slightly upslope and east of the boulder are a few smaller outcroppings of soapstone bedrock, at least two of which show definite scars left by quarrying for soapstone bowl manufacture.<ref name = nrhpinv>{{Cite web | author =unknown| title =Judaculla Rock| work = National Register of Historic Places - Nomination and Inventory | date = n.d.| url = http://www.hpo.ncdcr.gov/nr/JK0622.pdf | format = pdf | publisher = North Carolina State Historic Preservation Office | accessdate = 2015-01-01}}</ref>

==Description==
The surface of the westward-slanting main boulder with petroglyphs, which measures roughly {{convert|22|m2|sp=us}}; it includes scars left by soapstone bowl extraction. Three depressions show stem extraction and three more are hollow scallops.<ref name="JohannesLoubser">Johannes Loubser and Douglas Frink. Heritage Resource Conservation Plan for Judaculla Rock, State Archaeological Site 31JK3, North Carolina. Stratum Report submitted to Jackson County, Sylva 2008</ref> Numerous petroglyph designs have been pecked and incised into the surface. The densely packed motifs, especially those along the upper two-thirds of the boulder, often makes it difficult to distinguish between the carvings. A minimum count of the patterns revelas the following: 1,458 [[Cup and ring mark|cup marks]], 47 curvilinear units, ten bowl-shaped depressions, ten stick-like figures, nine rills, three concentric rings designs, three curvilinear motifs, three deer tracks, two claw-like imprints, one arc, one cross-in-circle, and one winged shape.

==Date range==
Petroglyphs that occur within three hollow scallops suggest that their production took place after [[soapstone]] bowl quarrying had ceased at the site, a finding supported by similar overlaps at a smaller soapstone boulder in western North Carolina (Brinkley Rock) and two more in northern Georgia (Track Rock Gap and Sprayberry Rock).<ref name="JohannesLoubser" /> In terms of stylistic cross-dating, the similarity between the concentric ring and cross-in-ring petroglyph designs on the boulder with patterns on ceramic from the region suggests that the petroglyphs over the Late Archaic soapstone extraction scars date somewhere between the [[Middle Woodland]] and [[Late Mississippian]] periods.<ref>Scott Ashcraft and David Moore. "Native American Rock Art in Western North Carolina." In ''Collected Papers on the Archaeology of Western North Carolina,'' edited by D. G. Moore and A. S. Ashcraft, pp. 59-88. Fall Meeting of the North Carolina Archaeological Society, Cherokee. 1998</ref>

Controlled archaeological excavations around the boulder revealed deposits that unfortunately have been heavily disturbed in historic times.<ref>Scott Shumate and Johannes Loubser. "Phase III Archaeological Investigations at the Judaculla Rock Site (31JK3)," Jackson County, North Carolina. Stratum Unlimited Report submitted to Jackson County, Sylva 2011</ref> However, auger sampling of soils higher on the slope suggest intact layers remain.<ref>Johannes Loubser, and Douglas Frink. "Phase I Soil Profiling and Archaeological Documentation of Conditions, Judaculla Rock, State Archaeological Site 31JK3," Jackson County, North Carolina, Sylva, 2009</ref> These layers, containing soapstone and lithic fragments left by soapstone bowl manufacture, probably date to the [[Late Archaic]] period.<ref>Dan Elliott. ''The Live Oak Soapstone Quarry, DeKalb County, Georgia.'' Garrow and Associates, Atlanta. 1986</ref><ref>Ken Sassaman. Refining Soapstone Vessel Chronology in the Southeast. Early Georgia 25(1): 1-20. 1997</ref>

==Cherokee lore==
Judaculla Rock retains a special significance to the [[Cherokee Indians]], despite their mass removal from the region in 1838.<ref>John Parris. ''The Cherokee Story.'' The Stephens Press, Asheville. 1950</ref> Cherokee accounts link Judaculla (also known as ''Tuli-cula/Juthcullah/Tsulʻkalu''), their slant-eyed Master-of-Game,<ref name="JamesMooney">James Mooney. ''Myths of the Cherokee.'' Nineteenth Annual Report of the Bureau of American Ethnology, 1897-98. Part 1. Government Printing Office, Washington, D.C. 1900</ref> with the surrounding landscape, including landforms, rivers, and Indian towns.<ref>Hiram Wilburn, "Judaculla Place-Names and the Judaculla Tales." ''Southern Indian Studies'' IV: 23-26. 1952</ref> The petroglyph boulder is on an old trail that linked the old Cherokee townhouse at Cullowhee, or "Juthcullah's Place," with Judaculla's reputed townhouse within Tannasee Bald (also known as ''Tsunegûñyĭ'').<ref>Cherokee Studies at Western Carolina University People of the Land: "An Introduction to the Cherokee Heritage Walking and Fitness Trail." Interpretive Pamphlet Published by the Western Carolina University, Cullowhee. 2007</ref> From a traditional perspective, contemporary Cherokees continue to regard the boulder as spiritually significant.

==Significance==
From a rock art perspective, the boulder is significant as it contains more petroglyphs than any other known boulder east of the Mississippi River. Judaculla Rock contains approximately 1,548 motifs, 3.7 times the total of 421 motifs at the substantial [[Track Rock|Track Rock Gap]] petroglyph boulder complex in far northern Georgia. From an archaeological perspective, the intact deposits upslope from Judaculla Rock contain physical traces of [[Late Archaic]] soapstone quarrying and bowl manufacturing activities. The location of Judaculla Rock between Cullowhee townhouse and Judaculla's townhouse in Tannasee Bald is reflected in Cherokee stories of other petroglyph boulders in the mountains and foothills of North Carolina and northern Georgia.<ref name="JamesMooney" /><ref>John Haywood. ''The Natural and Aboriginal History of Tennessee, Up to the First Settlements therein by the White People, in the Year 1768.'' George Wilson, Nashville. 1823</ref> These intermediary locations tie the summer agricultural pursuits in the floodplains to the fall and winter hunting in the uplands. It is also significant that the petroglyph boulders are probably stylized picture maps of the terrain where they are found.<ref>Johannes Loubser. "Heritage Resources Evaluation of the Allen Petroglyph Boulder, 9HM299, Habersham County, Chattooga River Ranger District, Georgia." Stratum Unlimited Report submitted to USDA Forest Service, Gainesville. 2011</ref>

Judaculla Rock appears to be physical representation of the entire landscape the Cherokees inhabited and exploited, and  it not only embodies a distinctive characteristic of petroglyph boulders in the region but also is likely to yield additional information about the history and prehistory of the area. Collaboration in [[Jackson County, North Carolina|Jackson County]] between the Parker family, the North Carolina Rock Art Project, the Eastern Band of the Cherokee Indians, the Western Carolina University, the North Carolina Department of Cultural Resources, and the Caney Fork Community Council, enabled the construction of a semi-circular elevated viewing platform at the site, complete with interpretive signs. This pro-active conservation and management of Judaculla Rock has turned it into a textbook example of how [[rock art]] sites in other parts of the country can be preserved, interpreted, and presented on a sustainable basis to the visiting public for generations to come.

In 2013, Judaculla Rock was placed on the [[National Register of Historic Places]].<ref name="nps"/>

==References==
{{reflist}}

==External links==
* [http://rec.jacksonnc.org/html/parks.html Jackson County Recreation/Parks Department]

{{National Register of Historic Places in North Carolina}}
{{Cherokee}}

[[Category:Rock formations of the United States]]
[[Category:History of the Cherokee]]
[[Category:Archaeological sites on the National Register of Historic Places in North Carolina]]
[[Category:Petroglyphs in North Carolina]]
[[Category:Buildings and structures in Jackson County, North Carolina]]
[[Category:National Register of Historic Places in Jackson County, North Carolina]]

[[it:Roccia di Judaculla]]