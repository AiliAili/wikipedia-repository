{{Other uses}}
{{distinguish|Poll evil}}
{{Taxobox
| name = Boll weevil
| image = Boll weevil.jpg
| image_width = 250px
| regnum = [[Animal]]ia
| phylum = [[Arthropod]]a
| classis = [[Insect]]a
| ordo = [[beetle|Coleoptera]]
| familia = [[Curculionidae]]
| subfamilia = [[Curculioninae]]
| tribus = [[Anthonomini]]
| genus = ''[[Anthonomus]]''
| species = '''''A. grandis'''''
| binomial = ''Anthonomus grandis''
| binomial_authority = [[Karl Henrik Boheman|Boheman]], 1843
}}
The '''boll weevil''' (''Anthonomus grandis'') is a [[beetle]] which feeds on [[cotton]] buds and flowers. Thought to be native to [[Central Mexico]],<ref>{{Cite journal|last=Jones|first=Robert|date=2000-01-21|title=Evolution of the Host Plant Associations of the Anthonomus grandis Species Group (Coleoptera: Curculionidae): Phylogenetic Tests of Various Hypotheses|url=http://www.bioone.org/doi/abs/10.1603/0013-8746%282001%29094%5B0051%3AEOTHPA%5D2.0.CO%3B2?journalCode=esaa|journal=Annals of the Entomological Society of America|volume=94|issue=1|doi=10.1603/0013-8746(2001)094[0051:EOTHPA]2.0.CO;2|pmid=|access-date=2016-08-30|via=}}</ref> it migrated into the [[United States]] from [[Mexico]] in the late 19th century and had infested all U.S. cotton-growing areas by the 1920s, devastating the industry and the people working in the American South. During the late 20th century, it became a serious pest in [[South America]] as well. Since 1978, the [[Boll Weevil Eradication Program]] in the U.S. allowed full-scale cultivation to resume in many regions.

==Lifecycle==
[[Image:Female Catolaccus grandis wasp.jpg|thumb|A female ''[[Pteromalidae|Catolaccus grandis]]'' wasp is attracted by a boll weevil larva.]]

Adult weevils overwinter in well-drained areas in or near cotton fields after [[diapause]]. They emerge and enter cotton fields from early spring through midsummer, with peak emergence in late spring, and feed on immature cotton bolls. The female lays about 200 eggs over a 10- to 12-day period. The [[oviposition]] leaves wounds on the exterior of the flower bud. The eggs hatch in 3 to 5 days within the cotton squares (larger buds before flowering), feed for 8 to 10 days, and finally [[pupate]]. The pupal stage lasts another 5 to 7 days. The lifecycle from egg to adult spans about three weeks during the summer. Under optimal conditions, 8 to 10 generations per season may occur.

Boll weevils begin to die at temperatures at or below −5&nbsp;°C (23&nbsp;°F). Research at the [[University of Missouri]] indicates they cannot survive more than an hour at −15&nbsp;°C (5&nbsp;°F). The insulation offered by leaf litter, crop residues, and snow may enable the beetle to survive when air temperatures drop to these levels. The boll weevil lays its eggs inside buds and ripening bolls (fruits) of the cotton plants. The adult insect has a long snout, is grayish color, and is usually less than 6&nbsp;mm long.

Other limitations on boll weevil populations include extreme heat and drought. Its natural predators include [[fire ant]]s, insects, spiders, birds, and a [[parasitic]] [[wasp]], ''[[Catolaccus grandis]]''. The insects sometimes emerge from diapause before cotton buds are available.

==Infestation==
The insect crossed the [[Rio Grande]] near [[Brownsville, Texas]], to enter the United States from Mexico in 1892<ref name="msstate"/> and reached southeastern [[Alabama]] in 1909. By the mid-1920s, it had entered all cotton-growing regions in the U.S., travelling 40 to 160 miles per year. It remains the most destructive cotton pest in North America. Since the boll weevil entered the United States, it has cost U.S. cotton producers about $13 billion, and in recent times about $300 million per year.<ref name="msstate">{{cite web|url=http://www.bollweevil.ext.msstate.edu/webpage_history.htm Economic impacts of the boll weevil |title=History of the Boll Weevil in the United States |author=Mississippi State University |deadurl=yes |archiveurl=https://web.archive.org/web/20080512023346/http://www.bollweevil.ext.msstate.edu/webpage_history.htm |archivedate=2008-05-12 |df= }}</ref>

[[Image:Boll weevil illustration.jpg|thumb|The cotton boll weevil: a, adult beetle; b, pupa; c, larva]]

The boll weevil contributed to the economic woes of Southern farmers during the 1920s, a situation exacerbated by the [[Great Depression]] in the 1930s.

The boll weevil appeared in [[Venezuela]] in 1949 and in [[Colombia]] in 1950.<ref name="ICAC">{{cite web | url = http://www.icac.org/Projects/CommonFund/Boll/proj_03_proposal.pdf | title = Integrated Pest Management Of The Cotton Boll Weevil In Argentina, Brazil, And Paraguay | author = ICAC}}</ref> The [[Amazon Rainforest]] was thought to present a barrier to its further spread, but it was detected in [[Brazil]] in 1983, and an estimated 90% of the cotton farms in Brazil are now infested. During the 1990s, the weevil spread to [[Paraguay]] and [[Argentina]]. The International Cotton Advisory Committee <!-- (ICAC) --> has proposed a control program similar to that used in the U.S.<ref name="ICAC"/>

==Control==
{{See also|Boll Weevil Eradication Program}}
Following World War II, the development of new pesticides such as [[DDT]] enabled U.S. farmers again to grow cotton as an economic crop. DDT was initially extremely effective, but U.S. weevil populations developed resistance by the mid-1950s.<ref name=scho>{{cite book|author=Timothy D. Schowalter|title=Insect Ecology: An Ecosystem Approach|url=https://books.google.com/books?id=2KzokTLIysQC&pg=PA482|accessdate=8 November 2011|date=31 May 2011|publisher=Academic Press|isbn=978-0-12-381351-0|page=482}}</ref> [[Methyl parathion]], [[malathion]], and [[pyrethroid]]s were subsequently used, but environmental and resistance concerns arose as they had with DDT, and control strategies changed.<ref name=scho/>

While many control methods have been investigated since the boll weevil entered the United States, insecticides have always remained the main control methods. In the 1980s, entomologists at Texas A&M University pointed to the spread of another invasive pest, the [[red imported fire ant]], as a factor in the weevils' population decline in some areas.<ref>{{cite web | url = http://www.springerlink.com/content/pl4557w722v04q54/ | title = Fire ant predation on the boll weevil |author1=D. A. Fillman  |author2=W. L. Sterling  |lastauthoramp=yes | publisher = BioControl: Volume 28, Number 4 / December, 1983}}</ref>

Other avenues of control that have been explored include weevil-resistant strains of cotton,<ref>{{cite web | url = http://cat.inist.fr/?aModele=afficheN&cpsidt=2906042 | title = Weevil-resistant strains of cotton |author1=Hedin, P. A.  |author2=McCarty, J. C.  |lastauthoramp=yes | publisher = Journal of agricultural and food chemistry:1995, vol. 43, no10, pp. 2735–2739 (19 ref.)}}</ref> the [[parasitic wasp]] ''Catolaccus grandis'',<ref>{{cite web | url = http://www.biocontrol.entomology.cornell.edu/parasitoids/catolaccus.html | author = Juan A. Morales-Ramos | publisher = Biological Control: a guide to Natural Enemies in North America | title = ''Catolaccus grandis'' (Burks) (Hymenoptera: Pteromalidae)}}</ref> the [[fungus]] ''[[Beauveria bassiana]]'',<ref>[http://www.encyclopedia.com/doc/1G1-14987730.html Biological controls of the boll weevil]</ref> and the Chilo iridescent [[virus]]. [[Genetic engineering|Genetically engineered]] [[Bacillus thuringiensis|Bt]] cotton is not protected from the boll weevil.<ref>[http://life.bio.sunysb.edu/ee/geeta/Bt-Cotton.html Bt susceptibility of insect species] {{webarchive |url=https://web.archive.org/web/20080409214952/http://life.bio.sunysb.edu/ee/geeta/Bt-Cotton.html |date=April 9, 2008 }}</ref>

Although it was possible to control the boll weevil, to do so was costly in terms of insecticide costs. The goal of many cotton entomologists was to eventually eradicate the pest from U. S. cotton. In 1978, a large-scale test was begun in eastern North Carolina and in Southampton County, Virginia, to determine the feasibility of eradication. Based on the success of this test, area-wide programs were begun in the 1980s to eradicate the insect from whole regions. These are based on cooperative effort by all growers together with the assistance of the Animal and Plant Health Inspection Service <!-- (APHIS) --> of the United States Department of Agriculture(USDA).{{citation needed|date=January 2014}}

The program has been successful in eradicating boll weevils from all cotton-growing states with the exception of Texas, and most of this state is free of boll weevils. Problems along the southern border with Mexico have delayed eradication in the extreme southern portions of this state. Follow-up programs are in place in all cotton-growing states to prevent the reintroduction of the pest. These monitoring programs rely on pheromone-baited traps for detection.{{citation needed|date=January 2014}} The boll weevil eradication program, although slow and costly, has paid off for cotton growers in reduced pesticide costs. This program and the [[Cochliomyia|screwworm program]] of the 1950s are among the biggest and most successful insect control programs in history.<ref>{{Cite web|url=http://deltafarmpress.com/most-successful-biological-pest-exclusion-program-ever|title=Delta Farm Press|last=|first=|date=|website=Delta Farm Press|publisher=|access-date=2 September 2016}}</ref>

==Impact==
The [[Library of Congress]] [[American Memory]] Project contains a number of [[oral history]] materials on the boll weevil's impact.<ref>[http://memory.loc.gov/ammem/today/dec11.html The American Memory Project – Boll weevils]</ref>

A 2009 study found "that as the weevil traversed the American South  [in the period 1892-1932], it seriously disrupted local economies, significantly reduced the value of land (at this time still the most important asset in the American South), and triggered substantial intraregional population movements."<ref>{{Cite journal|last=Lange|first=Fabian|last2=Olmstead|first2=Alan L.|last3=Rhode|first3=Paul W.|date=2009-09-01|title=The Impact of the Boll Weevil, 1892–1932|url=https://www.cambridge.org/core/journals/journal-of-economic-history/article/div-classtitlethe-impact-of-the-boll-weevil-18921932div/B726479ED1550ECE8F28A7D8115F5A52|journal=The Journal of Economic History|volume=69|issue=3|pages=685–718|doi=10.1017/S0022050709001090|issn=1471-6372}}</ref>

The boll weevil infestation has been credited with bringing about economic diversification in the Southern US, including the expansion of [[peanut]] cropping. The citizens of [[Enterprise, Alabama]], erected the [[Boll Weevil Monument]] in 1919, perceiving that their economy had been overly dependent on cotton, and that mixed farming and manufacturing were better alternatives.

The boll weevil is the mascot for the [[University of Arkansas at Monticello]] and is listed on several "silliest" or "weirdest" mascots of all time.<ref>http://www.oddee.com/item_96800.aspx</ref><ref>http://www.campusexplorer.com/Top-10-Weirdest-College-Mascots/</ref> It was also the mascot of a short-lived minor league baseball team, the [[Temple Boll Weevils]], which were alternatively called the "Cotton Bugs."

"[[Boll Weevil (song)|Boll Weevil]]" is a traditional blues song which reached #2 on the Billboard chart in 1961.

==See also==
* ''[[Lixus concavus]]'', the rhubarb curculio weevil
* [[Female sperm storage]]

==References==
{{reflist}}

==Further reading==
* Dickerson, Willard A., et al., Ed. Boll Weevil Eradication in the United States Through 1999. The Cotton Foundation, Memphis, Tn 2001. 627 pp.
* Lange, Fabian, Alan L. Olmstead, and Paul W. Rhode, "The Impact of the Boll Weevil, 1892–1932", ''Journal of Economic History'', 69 (Sept. 2009), 685–718.

===External===
* [http://www.bugwood.org/factsheets/99-001.html Boll weevil life cycle]
* [http://muextension.missouri.edu/explore/agguides/crops/g04255.htm Boll weevil biology]

==External links==
{{Commons|Anthonomus grandis}}
{{wikispecies|Anthonomus grandis|Boll weevil}}
* [http://www.txbollweevil.org/ Texas Boll Weevil Eradication Foundation]
* [https://web.archive.org/web/20090207172615/http://www.arkansasweevil.org:80/ Arkansas Boll Weevil Eradication Foundation]
* Hunter and Coad, [http://digital.library.unt.edu/permalink/meta-dc-1788:1 "The boll-weevil problem"], ''U.S. Department of Agriculture Farmers' Bulletin'', (1928). Hosted by the [[University of North Texas Libraries]] Digital Collections
* [https://web.archive.org/web/20060803182525/http://www.800alabama.com/alabama-attractions/details.cfm?id=1398 Alabama Tourism Board]
* [http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-2088 Boll Weevil in Georgia]
* [http://ceris.purdue.edu/napis/pests/bw/news/parasite.txt A 1984 paper on the effect of a parasitic wasp on the boll weevil]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}

[[Category:Agricultural pest insects]]
[[Category:Cotton diseases]]
[[Category:Beetles described in 1843]]
[[Category:Curculioninae]]
[[Category:Insects of Mexico]]