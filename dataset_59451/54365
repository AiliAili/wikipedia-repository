{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=November 2016}}
{{Userspace draft|source=ArticleWizard|date=October 2015}}

'''Maxwell Robert Arthur "Max" Lamshed''' OBE (5 April 1901 – 25 July 1971), occasionally written as "M. R. Lamshed", was a South Australian journalist, historian and Red Cross official.

==History==
Max was born in [[Rendelsham, South Australia|Rendelsham]]<ref>{{cite news |url=http://nla.gov.au/nla.news-article200114056 |title=Honor for Rendelsham Man |newspaper=[[The South Eastern Times |The South Eastern Times (Millicent, SA : 1906 – 1954)]] |location=Millicent, SA |date=5 January 1951 |accessdate=14 October 2015 |page=4 |publisher=National Library of Australia}}</ref> the only son of carpenter and builder Arthur J. Lamshed, <!-- probably not Arthur John Lamshed (1875–1954), who died at Kadina--> whose parents emigrated to South Australia in the 1850s,<ref>{{cite news |url=http://nla.gov.au/nla.news-article200111165 |title=Personalia |newspaper=[[The South Eastern Times |The South Eastern Times (Millicent, SA : 1906 – 1954)]] |location=Millicent, SA |date=6 December 1946 |accessdate=9 October 2015 |page=4 |publisher=National Library of Australia}}</ref> and was educated at Mount Gambier High School<ref>{{cite news |url=http://nla.gov.au/nla.news-article78650692 |title=New Year Honours O.B.E. To Mr. Lamshed |newspaper=[[The Border Watch|Border Watch (Mount Gambier, SA : 1861 – 1954)]] |location=Mount Gambier, SA |date=2 January 1951 |accessdate=14 October 2015 |page=12 |publisher=National Library of Australia}}</ref>

His father was at [[Robe, South Australia|Robe]] for 18 years, Rendelsham from around 1896 to 1906, where he was an active member of the local cricket team, then [[Mount Gambier, South Australia|Mount Gambier]]. Max too was a keen cricketer.

Max was employed by ''[[The Border Watch]]'' then in 1923 moved to ''[[The Advertiser (Adelaide)|The Adelaide Advertiser]]''.<ref>{{cite news |url=http://nla.gov.au/nla.news-article78588441 |title=25 Years Ago |newspaper=[[The Border Watch|Border Watch (Mount Gambier, SA : 1861 – 1954)]] |location=Mount Gambier, SA |date=15 July 1948 |accessdate=14 October 2015 |page=2 |publisher=National Library of Australia}}</ref> He became a feature writer, then News Editor, Assistant Manager and finally Promotions Manager. While working, he continued studying in his spare time, performing credibly.<ref>{{cite news |url=http://nla.gov.au/nla.news-article78016174 |title=Concerning People |newspaper=[[The Border Watch|Border Watch (Mount Gambier, SA : 1861 – 1954)]] |location=Mount Gambier, SA |date=6 December 1930 |accessdate=14 October 2015 |page=1 |publisher=National Library of Australia}}</ref>

He was seconded by ''The Advertiser'' to the 1933 [[University of Adelaide]] anthropological expedition to the western [[MacDonnell Ranges]], [[Mann Ranges]] and [[Musgrave Ranges]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article46998528 |title=Scientists' Camp in Mountain Glen |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=2 September 1933 |accessdate=14 October 2015 |page=14 |publisher=National Library of Australia}} Report No. 1</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article46998996 |title=Kindergartens in the Wilds |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=5 September 1933 |accessdate=14 October 2015 |page=8 |publisher=National Library of Australia}} Report No. 2</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article46999169 |title=Nomadic Life in No-Man's Land |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=6 September 1933 |accessdate=14 October 2015 |page=18 |publisher=National Library of Australia}}Report No. 3</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article74021224 |title=Teeth Removed as Aid to Beauty |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=7 September 1933 |accessdate=14 October 2015 |page=14 |publisher=National Library of Australia}} Report No. 4</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article74023966 |title=Busy Days at Ernabella |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=8 September 1933 |accessdate=14 October 2015 |page=28 |publisher=National Library of Australia}} Report No. 5</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article47000007 |title=Fierce Life of Far Outback |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=9 September 1933 |accessdate=14 October 2015 |page=14 |publisher=National Library of Australia}} Report No. 6</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article47000523 |title=Socialists of the Outback |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=11 September 1933 |accessdate=14 October 2015 |page=14 |publisher=National Library of Australia}} Report No. 7</ref>
<ref>{{cite news |url=http://nla.gov.au/nla.news-article74016830 |title=Revue in the Outback |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=12 September 1933 |accessdate=14 October 2015 |page=20 |publisher=National Library of Australia}} Report No. 8</ref>

In 1940 Max Lamshed wrote a piece for the ''Border Watch'' on the work of the Red Cross.<ref>{{cite news |url=http://nla.gov.au/nla.news-article78185366 |title=Birth of Red Cross. |newspaper=[[The Border Watch|Border Watch (Mount Gambier, SA : 1861 – 1954)]] |location=Mount Gambier, SA |date=22 February 1940 |accessdate=15 October 2015 |page=8 |publisher=National Library of Australia}}</ref> From 1943 to 1955 he was honorary organizer for the [[Food for Britain]] campaign in South Australia, and for this service was appointed [[Order of the British Empire|OBE]] in 1950.<ref>{{cite news |url=http://nla.gov.au/nla.news-article45696797 |title=One New Knight in S.A. Awards |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1931 – 1954)]] |location=Adelaide, SA |date=1 January 1951 |accessdate=16 October 2015 |page=3 |publisher=National Library of Australia}}</ref> From 1958 to 1963 he was chairman of the South Australian Division of the Red Cross Society.

He was appointed Press Officer to H.M the [[Queen Elizabeth The Queen Mother|Queen Mother]] for her 1964 Australian tour.<ref>{{cite web|url=http://trove.nla.gov.au/work/195164177?q&versionId=213697462|title=Robert Menzies: Media release 22 November 1963|accessdate=14 October 2015}}</ref> He retired from ''The Advertiser'' in 1964 to accept a position as administrator for the [[Adelaide Festival of Arts]], helping organise the 1966 and 1968 Festivals.

Other interests included the [[Adelaide Eisteddfod Society]], of which he was chairman from 1961 to 1966; he was a member of the [[Adelaide Hills Council|Stirling District Council]], a member of the [[National Parks Commission of SA]]; a governor of the [[Adelaide Botanic Garden]]; and a member of the Board of Governors for the [[Morialta Children's Home]]. He was a member of [[Rotary International]] and an occasional contributor to ''[[The Rotarian]]''.<ref>{{cite web|url=https://books.google.com.au/books?id=QjcEAAAAMBAJ|title=''The Rotarian''|date=August 1960|publisher=Rotary International|accessdate=15 October 2015}} This issue features an article by Lamshed on the Adelaide Festival.</ref>

==Family==
Max married Christine Joyce Davis ( – ) on 9 March 1940.<ref>{{cite news |url=http://nla.gov.au/nla.news-article78186378 |title=Wedding. |newspaper=[[The Border Watch|Border Watch (Mount Gambier, SA : 1861 – 1954)]] |location=Mount Gambier, SA |date=26 March 1940 |accessdate=14 October 2015 |page=3 |publisher=National Library of Australia}}</ref> They had two sons and two daughters, and lived at [[Crafers, South Australia|Crafers]] in the Adelaide Hills.

==Bibliography==
*''The River's Bounty – a history of Barmera and its people'' 1952 *
*''The Hardy Tradition – tracing the growth and development of a great wine-making family through its first hundred years'' 1953 *
*''Years to Remember – 1854–1954 : a record of the first hundred years of the business of D. and J. Fowler Limited'' 1954 *
*''The People's Garden: A Centenary History of the Adelaide Botanic Garden'' Government Printer, Adelaide 1955
*''The Seppelt Story 1851–1951'' 1958 *
*''The South Australian Story – a century of progress'' (illust. mostly by W. Sanders) Advertiser Newspapers Limited, Adelaide 1958 *
*''Adelaide Sketchbook'' (with Jeanette McLeod) Rigby 1967 ISBN 0727008064 
*''South East Sketchbook'' (with Ken Robins) Rigby 1970 ISBN 0851791271
*''Adelaide Hills Sketchbook'' (with Jeanette McLeod) Rigby 1971 ISBN 0851791603
*''Prospect: 1872–1972 : a portrait of a city'' Corporation of the City of Prospect, Adelaide 1972
*''Monty – the biography of C. P. Mountford'' Rigby Limited 1972 ISBN 0851794270
<nowiki>*</nowiki> Lamshed's authorship of these histories was revealed on the flyleaf of ''Monty''.

==Further reading (or listening)==
Radio interview with Max Lamshed by [[Lynne Arnold]].<ref>{{cite web|url=http://www.catalog.slsa.sa.gov.au/search/q?author=lamshed+max&title=radio+interview+with+max+lamshed&submit=Search|title=Radio interview with Max Lamshed|publisher=State Library of South Australia|accessdate=16 October 2015}} Cassette recording and 17-page transcription.</ref>

== References ==
{{Reflist}}

{{DEFAULTSORT:Lamshed, Mak}}
[[Category:Australian journalists]]
[[Category:Australian writers]]
[[Category:1901 births]]
[[Category:1971 deaths]]
[[Category:20th-century Australian historians]]