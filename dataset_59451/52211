{{Infobox journal
| title = Magnetic Resonance in Chemistry
| cover = [[File:MRC new cover.gif]]
| discipline = [[Chemistry]]
| abbreviation = Magn. Reson. Chem.
| editor = James Keeler
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Monthly
| history = 1969-present
| impact =  1.179
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-458Xa
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-458Xa/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-458Xa/issues
| link2-name = Online archive
| ISSN = 0749-1581
| eISSN = 1097-458X
| LCCN = 85647586
| CODEN = MRCHEG
| OCLC = 639062734
}}
'''''Magnetic Resonance in Chemistry''''' is a monthly [[peer review|peer-reviewed]] [[scientific journal]] publishing original research papers dealing with the application of NMR, ESR and NQR [[Spectroscopy|spectrometry]] in all branches of [[chemistry]]. The journal was established in 1969 and is published by [[John Wiley & Sons]]. The current [[editor-in-chief]] is James Keeler ([[University of Cambridge]]).

== Abstracting and indexing ==
The journal is abstracted indexed in:
* [[Chemical Abstracts Service]]
* [[Scopus]]
* [[Science Citation Index]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.179, ranking it 94th out of 157 journals in the category "Chemistry, Multidisciplinary",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry, Multidisciplinary |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 30th out of 44 journals in the category "Spectroscopy",<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Spectroscopy |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science |postscript=.}}</ref> and 1ß7th out of 157 journals in the category "Chemistry, Physical".<ref name=WoS3>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry, Physical |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science |postscript=.}}</ref>

== Highest cited papers ==
According to the [[Web of Science]], the following papers have been cited most often (> 300 times):<ref name=WoS>{{cite book |year=2012 |chapter=Magnetic Resonance in Chemistry |title=[[Science Citation Index]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>
# {{cite journal |doi=10.1002/mrc.984 |title=Modelling one- and two-dimensional solid-state NMR spectra |year=2002 |last1=Massiot |first1=Dominique |last2=Fayon |first2=Franck |last3=Capron |first3=Mickael |last4=King |first4=Ian |last5=Le Calvé |first5=Stéphanie |last6=Alonso |first6=Bruno |last7=Durand |first7=Jean-Olivier |last8=Bujoli |first8=Bruno |last9=Gan |first9=Zhehong |last10=Hoatson |first10=Gina |journal=Magnetic Resonance in Chemistry |volume=40 |pages=70}}
# {{cite journal |doi=10.1002/mrc.1260310315 |title=Gradient selection in inverse heteronuclear correlation spectroscopy |year=1993 |last1=Willker |first1=Wieland |last2=Leibfritz |first2=Dieter |last3=Kerssebaum |first3=Rainer |last4=Bermel |first4=Wolfgang |journal=Magnetic Resonance in Chemistry |volume=31 |issue=3 |pages=287}}
# {{cite journal |doi=10.1002/mrc.1260241002 |title=Conformation-dependent13C chemical shifts: A new means of conformational characterization as obtained by high-resolution solid-state13C NMR |year=1986 |last1=Saitô |first1=Hazime |journal=Magnetic Resonance in Chemistry |volume=24 |issue=10 |pages=835}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-458Xa}}

{{DEFAULTSORT:Magnetic Resonance In Chemistry}}
[[Category:Chemistry journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Publications established in 1969]]
[[Category:Monthly journals]]
[[Category:English-language journals]]