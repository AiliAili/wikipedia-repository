<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=M09
 | image=[[File:ICP M09 Aviation Engine.jpg|300px]]
 | caption=
}}{{Infobox Aircraft Engine
 |type=piston [[aircraft engine]]
 |manufacturer=[[ICP srl]]
 |designer=Franco Lambertini
 |national origin=[[Italy]]
 |first run=2014
 |major applications=
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}
The '''ICP M09''' is an [[Italy|Italian]] piston [[aircraft engine]] under development by [[ICP srl]] of [[Castelnuovo Don Bosco]],  [[Piedmont]]. The engine is intended for aircraft in the European [[Fédération Aéronautique Internationale]] [[microlight]] and the American [[light-sport aircraft]] categories.<ref name="EAA">{{cite news|url = http://experimenter.epubxp.com/i/108002/26/|title = I.C.P.’s Bolt-On 80- to 150-hp Engine - New hope for the LSA/experimental market?|accessdate = 6 March 2015|last = Boric|first = Marino|date = February 2013| work = [[Experimental Aircraft Association|EAA]] Experimenter}}</ref><ref name="Volo">{{cite news|url = http://www.volosportivo.com/2014/12/22/vola-il-motore-icp-m09/|title = Vola Il Motore ICP M09|accessdate = 6 March 2015|last = |first = |date = 15 March 2015| work = Volo Sportivo}}</ref>

==Design and development==
Design work on the M09 started in about 2008 with work proceeding secretly until it was publicly announced in mid-September 2012. The design goals were compactness and quick installation.<ref name="EAA"/>

The M09 was designed by Franco Lambertini, who previously designed [[Moto Morini]] motorcycle engines, although the engine has no commonality with other Lambertini motorcycle engines and is a clean-sheet design for aviation use.<ref name="EAA"/><ref name="Volo"/>

The engine is a [[four stroke]], 90° V twin cylinder, four valve, {{convert|1225|cc|cuin|0|abbr=on}} powerplant that delivers a maximum of {{convert|115|hp|kW|0|abbr=on}} at 7000 rpm and is capable of inverted flight as well as [[tractor configuration|tractor]] and [[pusher configuration]] installations. It is equipped with a 2.95-to-1 reduction gearbox and has a dry weight of 67&nbsp;kg (147&nbsp;lbs). It incorporates an automatic decompressor starter installed on the exhaust valve camshaft. Fuel is delivered via [[electronic fuel injection]]. [[Turbocharging]] is envisioned for some future versions.<ref name="EAA"/><ref name="Volo"/>

The family of engines is expected to produce {{convert|80|to|150|hp|kW|0|abbr=on}} when fully developed, although the initial version is expected to produce {{convert|115|hp|kW|0|abbr=on}}. On initial runs it produced {{convert|135|hp|kW|0|abbr=on}}<ref name="EAA"/>

The first flight on an [[ICP Savannah S]] test bed aircraft was on 18 December 2014.<ref name="EAA"/><ref name="Volo"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Applications== -->
<!-- Survivors -->
<!-- Engines on display== -->

==Specifications (M09) ==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref=EAA and Volo Sportivo<ref name="EAA"/><ref name="Volo"/>
|type=twin cylinder, [[Four stroke]] piston [[aircraft engine]]
|bore=
|stroke=
|displacement={{convert|1225|cc|cuin|0|abbr=on}}
|length=
|diameter=
|width=
|height=
|weight=67 kg (147 lbs) dry
|valvetrain=
|supercharger=
|turbocharger=
|fuelsystem=electronic fuel injection
|fueltype=
|oilsystem=
|coolingsystem=air
|power={{convert|115|hp|kW|0|abbr=on}}
|specpower=
|compression=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=

|designer=
|reduction_gear=2.95-to-1 reduction gearbox

|general_other=
|components_other=
|performance_other=
}}

==References==
{{Commons category}}
{{Reflist}}
<!-- ==External links== -->
{{ICP aircraft}}

[[Category:Air-cooled aircraft piston engines]]
[[Category:Aircraft piston engines 2010–2019]]