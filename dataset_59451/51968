{{Use dmy dates|date=February 2013}}
{{Infobox journal
| title = Journal of Multivariate Analysis
| cover = 
| abbreviation = J. Multivar. Anal.
| discipline = [[Multivariate statistics]]
| editor = [[Jan de Leeuw]]
| publisher = [[Elsevier]]
| country =
| history = 1971–present
| frequency = Monthly
| impact = 1.007
| impact-year = 2010
| website = http://www.journals.elsevier.com/journal-of-multivariate-analysis/ 
| link1 = http://www.sciencedirect.com/science/journal/0047259X 
| link1-name = Online access
| ISSN = 0047-259X
| eISSN =
| CODEN = JMVAAI
| LCCN = 71649078
| OCLC = 01783582
}}
The '''''Journal of Multivariate Analysis''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] that covers applications and research in the field of [[Multivariate statistics|multivariate statistical analysis]]. The journal's scope includes theoretical results as well as applications of new theoretical methods in the field. Some of the research areas covered include [[asymptotic theory]], [[Bayesian model]]s, [[cluster analysis]], [[decision theory]], [[discriminant analysis]], distributions, estimation and hypothesis testing, [[factor analysis]], measures of association, [[multidimensional scaling]], [[multivariate analysis of variance]], [[sequential analysis]], time series, and general multivariate methods.<ref>{{cite web| title=Aims and Scope|work=Journal of Multivariate Analysis |url= http://www.journals.elsevier.com/journal-of-multivariate-analysis/|accessdate=6 June 2012}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 1.007.<ref name=WoS>{{cite book |year=2012 |chapter=Journal of Multivariate Analysis |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-06-06 |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
*[[List of statistics journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/journal-of-multivariate-analysis/ }}

{{Statistics journals}}

{{DEFAULTSORT:Journal of Multivariate Analysis}}
[[Category:Statistics journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1971]]
[[Category:Elsevier academic journals]]