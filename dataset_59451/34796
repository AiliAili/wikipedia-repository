{{good article}}
{{Infobox earthquake
|title = 1872 North Cascades earthquake
|origintime = {{Tooltip|5:40 a.m December 15 UTC|9:40 p.m. local time December 14}} <ref name=Stover/>
|map2 = {{Location map+ | Washington
|places =
{{Location map~|Washington|lat=47.9|long=-120.3|label=Wenatchee|position=left|mark=Green pog.svg}}
{{Location map~|Washington|lat=48.73|long=-121.07|label=Ross Lake|position=right|mark=Green pog.svg}}
{{Location map~|Washington|lat=47.04|long=-122.89|label=Olympia|position=left|mark=Green pog.svg}}
{{Location map~|Washington|lat=49.16|long=-121.95|label=Chilliwack|position=bottom|mark=Green pog.svg}}
{{Location map~|Washington|lat=46.19|long=-123.83|label=Astoria|mark=Green pog.svg}}
{{Location map~|Washington|lat=46.45|long=-116.82|label=Fort Lapwai|mark=Green pog.svg}}
{{Location map~|Washington|lat=45.52|long=-122.68|label=Portland|position=left|mark=Green pog.svg}}
{{Location map~|Washington|lat=47.68|long=-120.21|label=Entiat|position=left|mark=Green pog.svg}}
{{Location map~|Washington|lat=47.76|long=-119.9|mark=Bullseye1.png|marksize=40|label=Proposed epicenter at Lake Chelan}}
 | relief = yes
 | label =
 | width = 250
 | float = right
 | caption = }}
|magnitude = 6.5–7.0 (est) <ref name=Bakun_p3253/>
|location = {{coord|47.9|-120.3|display=inline,title}} <ref name=Stover/>
|type = Unknown
|countries affected = [[Pacific Northwest]] 
|intensity = [[Mercalli intensity scale|VIII (''Severe'')]] <ref name=Bakun_3256>{{harvnb|Bakun|Haugerud|Hopper|Ludwin|2002|pp=3256, 3257}}</ref>
|casualties = None
}}

The '''1872 North Cascades earthquake''' occurred at {{Tooltip|9:40 p.m. local time on December 14|5:40 a.m December 15 UTC}} in northern [[Washington (state)|Washington state]]. A maximum [[Mercalli intensity scale|Mercalli intensity]] of VIII (''Severe'') was assessed for several locations, though less intense shaking was observed at many other locations in Washington, Oregon, and [[British Columbia]]. Some of these intermediate outlying areas reported V (''Moderate'') to VII (''Very strong'') shaking, but intensities as high as IV (''Light'') were reported as far distant as [[Idaho]] and [[Montana]].  Due to the remote location of the mainshock and a series of strong aftershocks, damage to man made structures was limited to a few cabins close to the areas of the highest intensity.

Because the earthquake occurred before [[seismometer]]s were operating in the region, the magnitude of the shock and its location were never precisely determined, but the intensity reports that are available for the event were studied, and various epicenters for the event were proposed based on these limited data. One study presented an estimated [[Moment magnitude scale|moment magnitude]] of 6.5–7.0, with a proposed location on the east side of the [[Cascade Range]] near [[Lake Chelan]]. The results of a separate study indicated that it may have been a larger event, placing the shock in the [[North Cascades]], just south of the [[Canada–United States border]] at [[Ross Lake (Washington)|Ross Lake]].

==Preface==
{{see also|Geology of the Pacific Northwest|Puget Sound faults}}

The [[Cascadia subduction zone]] rarely influences the western portion of Washington state, but the November 1873 [[Moment magnitude scale|M]]7.3 shock near the [[California]]–[[Oregon]] border may have been associated with it. Although activity in the Pacific Northwest (especially west of the Cascades in Washington) has occasionally been located near the [[subduction zone]], earthquakes there ([[1949 Olympia earthquake|1949 Olympia]], [[1965 Puget Sound earthquake|1965 Puget Sound]], [[2001 Nisqually earthquake|2001 Nisqually]]) have mostly been [[Slab (geology)|intraslab]] events. A large M7 earthquake on the [[Seattle Fault]] in 900 C.E. may have generated a [[tsunami]] in [[Puget Sound]]. The 1872 event east of the Cascades is not understood well due to the lack of instrumental records and reliable felt intensity reports.<ref>{{harvnb|Bakun|Haugerud|Hopper|Ludwin|2002|p=3239}}</ref><ref>{{cite journal|title=A Tsunami About 1000 Years Ago in Puget Sound, Washington|url=http://www.sciencemag.org/content/258/5088/1614.abstract?|first=B. F.|last=Atwater|authorlink=Brian Atwater|first2=A. L.|last2=Moore|year=1992|journal=[[Science (journal)|Science]]|publisher=[[American Association for the Advancement of Science]]|volume=258|number=5088|pages=1614–1617|doi=10.1126/science.258.5088.1614|pmid=17742526}}</ref>

==Earthquake==
[[File:1872 North Cascades Isoseismal Map.png|thumb|left|[[Isoseismal map]] for the event]]

As there were only six [[seismometer]]s operating in Washington state and western British Columbia even as late as 1969, there are insufficient instrumental records for older events in the region. [[Depth of focus (tectonics)|Focal depths]] are unknown for shocks that occurred before that time, but [[seismology|seismologists]] Bakun et al. concluded that the event occurred on a shallow fault on the east side of the Cascade Range. They employed a method that was developed by W. H. Bakun and C. M. Wentworth for using earthquake intensity information that could be mapped to a corresponding [[moment magnitude scale|moment magnitude]]. The intensities for twelve 20th-century Pacific Northwest earthquakes were used for calibration before analyzing the known intensities for the 1872 event in an attempt to resolve the location and magnitude. The reports were interpreted in a way that placed the epicenter near the south end of Lake Chelan, but other considerations left other plausible focal points both north and northeast of the lake. The magnitude was estimated to be 6.5–7.0 with 95% confidence.<ref name=Bakun>{{harvnb|Bakun|Haugerud|Hopper|Ludwin|2002|pp=3239, 3240, 3248, 3251}}</ref>

Using a similar strategy, S. D. Malone and S. Bor analyzed the known intensities for the 1872 shock, then compared intensity patterns for a number of instrumentally recorded earthquakes that also occurred in the Pacific Northwest. A factor that was taken into consideration was that for earthquakes that have either circular or slightly elliptical isoseismal maps, the epicenter is usually close to the center of the pattern, but that for shocks where instrumental information are also available, the epicenter is sometimes not where it would have been assumed to be, had only the intensity information (and no instrumental information) been available. An isoseismal map of the [[1949 Olympia earthquake]] was presented as an example of a distorted or convoluted [[pattern]] that was attributed to local geological conditions that either [[Attenuation|attenuated]] or amplified the [[seismic wave]]s, and it was emphasized that not taking into account these local features could lead to a misinterpretation of the felt intensities and to a misplaced epicenter.<ref>{{harvnb|Malone|Bor|1979||pp=531, 532}}</ref>

Malone and Bor ran three simulations, with a projected M7.4 event occurring at a depth of {{convert|37|mi}}, but took into consideration the differences in attenuation both east and west of the Cascades.  Three exploratory locations were investigated, including the setting at the south end of Lake Chelan that reportedly had significant ground disturbances, their preferred location near Ross Lake, and a third location north of the Canada–United States border that had been proposed much earlier by W. G. Milne. The Ross Lake site was chosen because it most closely matched their isoseismal pattern, but it was not strongly preferred over the Milne site, and the Lake Chelan location was excluded as being the epicenter, due to the regional attenuation characteristics that required a location further to the west. Several depths were investigated, but each had little impact on the isoseismal patterns below intensity VI, and since most northwest earthquakes occur between {{convert|25|-|37|mi}} deep, they believe the shock was also near that depth, but did not dismiss the possibility that it was a shallower event.<ref>{{harvnb|Malone|Bor|1979||pp=543–546}}</ref>{{clear}}

{| {| class="wikitable" style="float: right; text-align: center; font-size:90%;"
|-
| colspan="2" style="text-align: center;" | Selected Mercalli intensities
|-
! [[Mercalli intensity scale|MMI]] !! Locations
|-
| VIII (''Severe'')
| [[Entiat, Washington|Entiat, WA]], [[Wenatchee, Washington|Wenatchee, WA]]
|-
| VII (''Very strong'')
| [[Osoyoos Lake|Osoyoos Lake, B.C.]], [[Chilliwack|Chilliwack, B.C.]]
|-
| VI (''Strong'')
| [[Nicola Country|Nicola Valley, B.C.]], [[Olympia, Washington|Olympia, WA]]
|-
|  V (''Moderate'')
| [[Astoria, Oregon|Astoria, OR]], [[Portland, Oregon|Portland, OR]]
|-	
| IV (''Light'')
| [[Fort Lapwai|Fort Lapwai, ID]], [[Deer Lodge, Montana|Deer Lodge, MT]]
|-	
| colspan="3" style="text-align: center;" | <small>{{harvnb|Bakun|Haugerud|Hopper|Ludwin|2002|pp=3256, 3257}}</small>
|}

===Damage===

Though the earthquake was felt over a very wide area (from the Pacific Ocean to Montana, and British Columbia to Oregon) the area that was most affected was largely unpopulated, and very few homes existed. A log building that was built on unconsolidated river [[sediment]] close to the mouth of the [[Wenatchee River]] had dislodged roof logs, and the kitchen became detached from the rest of the structure. Another [[log cabin]] between Entiat and Winesap also had roof damage. Mercalli intensities as high as VI (''Strong'') reached the western portion of the state, near the highly populated [[Puget Sound]] region, and to the southeast beyond where the [[Hanford Site|Hanford nuclear reactor site]] later stood.<ref name=Stover/>

===Aftershocks===
{{further information|Aftershock}}

A relationship exists between the depth of the mainshock and the occurrence of aftershocks, and several Pacific Northwest earthquakes illustrate this link, like the February 1981 M5.5 Elk Lake event in southwest Washington that was followed by more than 1,000 in the first two years. The M7.3 [[1959 Hebgen Lake earthquake]] in Montana had a significant sequence of aftershocks, and the shallow M7 [[1983 Borah Peak earthquake|1983 Borah Peak event]] was followed by four aftershocks. In opposition, the intraslab events (and [[crust (geology)|crustal]] shocks above the [[subduction zone]]) on the west side of the Cascades have had insignificant aftershock sequences, usually amounting to a minimal number of small aftershocks. For example, the 2001 Nisqually shock occurred nearly {{convert|18.6|mi|sigfig=2}} deep and was followed by only four small aftershocks, and there was a similar procession for the [[1946 Vancouver Island earthquake]], a M7.6 crustal shock that also had a focal depth near {{convert|18.6|mi|sigfig=2}}.<ref name=Bakun/>

Aftershocks did follow the 1872 event, and during the initial 24 hours they were strong enough to be felt over a broad area, from Idaho and into southern British Columbia. The intensity of the shocks waned as time passed, and after a year they were still occurring, but were only being felt at Wenatchee, Lake Chelan, and Entiat. Bakun et al. listed the considerable aftershock sequence as a strong indication that the initial event was shallow.<ref name=Bakun_p3253>{{harvnb|Bakun|Haugerud|Hopper|Ludwin|2002|p=3253}}</ref>

==See also==
* [[List of earthquakes in Canada]]
* [[List of earthquakes in the United States]]
* [[List of earthquakes in Washington (state)]]
* [[List of historical earthquakes]]
* [[Omak Rock]]
* [[Ross Lake Fault]]

==References==
{{Reflist|30em|refs=
<ref name=Stover>{{citation|last1=Stover|first1=C. W.|last2=Coffman|first2=J. L.|title=Seismicity of the United States, 1568–1989 (Revised)|series=U.S. Geological Survey Professional Paper 1527|url=https://books.google.com/books?id=bY0KAQAAIAAJ&printsec=frontcover&dq=seismicity+of+the+united+states|year=1993|publisher=[[United States Government Printing Office]]|pages=380, 382–384}}</ref>
}}

'''Sources'''
{{refbegin}}
* {{citation|title=The December 1872 Washington State Earthquake|url=http://bssa.geoscienceworld.org/content/92/8/3239.abstract|first=W. H.|last=Bakun|first2=R. A.|last2=Haugerud|first3=M. G.|last3=Hopper|first4=R. S.|last4=Ludwin|year=2002|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=92|issue=8|pages=3239–3258|doi=10.1785/0120010274}}
* {{citation|title=Estimating earthquake location and magnitude from seismic intensity data|url=http://bssa.geoscienceworld.org/content/87/6/1502.short|first=W. H.|last=Bakun|first2=C. M.|last2=Wentworth|year=1997|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=87|number=6|pages=1502–1521}}
* {{citation|title=Attenuation patterns in the Pacific Northwest based on intensity data and the location of the 1872 North Cascades earthquake|url=http://bssa.geoscienceworld.org/content/69/2/531.abstract|first=S. D.|last=Malone|first2=S.|last2=Bor|year=1979|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=69|issue=2|pages=531–546}}
{{refend}}

==External links==
*[http://www.seattletimes.com/seattle-news/scientists-may-be-cracking-mystery-of-big-1872-earthquake/ Scientists may be cracking mystery of big 1872 earthquake] – ''[[The Seattle Times]]''

{{Earthquakes in Canada}}
{{Earthquakes in the United States}}
{{Earthquakes in Washington (state)}}

[[Category:Earthquakes in Washington (state)]]
[[Category:History of Washington (state)]]
[[Category:North Cascades of Washington (state)]]
[[Category:1872 earthquakes]]
[[Category:1872 in North America]]
[[Category:December 1872 events]]