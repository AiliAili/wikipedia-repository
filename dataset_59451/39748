{{Infobox military person
|name=Siegfried Barth
|birth_date={{birth date|1916|1|23|df=y}}
|death_date={{death date and age|1997|12|19|1916|1|23|df=y}}
|image=File:Siegfried Barth.jpg
|image_size=
|caption=
|nickname=Balbo
|birth_place=[[Augsburg]], [[Bavaria]]
|death_place=[[Bad Wörishofen]], Bavaria
|allegiance={{flag|Nazi Germany}} (to 1945)<br />{{flag|West Germany}}
|branch={{Luftwaffe}}<br/>{{GAF}}
|serviceyears=1936–45, 1956–73
|rank=
{{plainlist|
*[[Oberstleutnant]] (''[[Wehrmacht]]'')
*[[Oberst]] ([[Bundeswehr]])
}}
|commands=IV./[[KG 51]], KG 51, [[Jagdbombergeschwader 32|JaBoG 32]]
|unit=[[KG 51]]
|battles=[[World War II]]
*[[Battle of France]]
*[[Battle of Britain]]
*[[Eastern Front (World War II)|Eastern Front]]
**[[Operation Barbarossa]]
**[[Siege of Sevastopol (1941–42)|Siege of Sevastopol]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=
}}
'''Siegfried Barth''' (23 January 1916 – 19 December 1997) was a German bomber pilot in the [[Luftwaffe]] during [[World War II]] and commander of the fighter-bomber wing [[Jagdbombergeschwader 32]] (JaBoG 32) of the German Air Force. He was a recipient of the [[Knight's Cross of the Iron Cross]], awarded by [[Nazi Germany]] to recognise extreme battlefield bravery or successful military leadership. As a [[Bundeswehr]] officer, he served at the [[NATO]] [[Supreme Headquarters Allied Powers Europe]] (SHAPE) from 1969 to 1972.

==World War II==
Barth was born 23 January 1916 in [[Augsburg]], [[Bavaria]] and joined the military service in 1936.<ref>{{Cite web|title=Das Bundesarchiv|work=Kabinettsprotokolle Online "Barth, Siegfried" (2.26:)|url=https://www.bundesarchiv.de/cocoon/barch/1000/z/z1960a/kap1_2/para2_26.html| language=German| accessdate=11 June 2013}}</ref> He was trained as a pilot before World War II and been a member of Kampfgeschwader 255 "Edelweiß" (KG 255—255th Bomber Wing), which was renamed [[Kampfgeschwader 51]] (KG 51—51st Bomber Wing) on 1 May 1939. When II. ''[[Organization of the Luftwaffe (1933–45)#Gruppe|Gruppe]]'' (2nd group) was formed on 1 April 1940, Barth joined the 4. ''[[Organization of the Luftwaffe (1933–45)#Staffel|Staffel]]'' (4th squadron) holding the rank of ''[[Leutnant]]'' (2nd lieutenant). He flew his first combat missions in the [[Battle of France]], bombing airfields and shipping off [[Dunkirk]] for which he was awarded the [[Iron Cross]] 2nd Class on 17 July 1940.<ref name="Kaiser pp88,89">Kaiser 2010, pp. 88, 89.</ref>

KG 51 was then relocated to airfields at [[Étampes-sur-Marne|Étampes-Mondésir]] and later to [[Orly Air Base|Paris-Orly]] in France. In the [[Battle of Britain]] he bombed British ports and [[London]], [[Coventry]] and [[Portsmouth]]. Barth was promoted to ''[[Oberleutnant]]'' and following [[Operation Barbarossa]], the German invasion of the Soviet Union on 22 June 1941, flew in the southern sector of the [[Eastern Front (World War II)|Eastern Front]]. He bombed airfields, railway stations as well as tank and troop concentrations in the [[Proskurov]], [[Lvov]], [[Rostov]] and [[Taganrog]] areas. He was wounded in action on 25 June 1941 when his [[Junkers Ju 88]] A-5 was hit by [[anti-aircraft]] fire in the vicinity of Darachow.<ref name="Kaiser pp88,89"/>

Barth was appointed ''[[Gruppenkommandeur]]'' (Group Commander) on 1 February 1944 of the IV./KG 51, which was based at [[Hildesheim]] at the time. Here he was responsible for the tactical training of replacement crews. Initially they flew the [[Messerschmitt Me 410]], then the [[Focke-Wulf Fw 190]] and lastly the [[Messerschmitt Me 262]], the first operational jet fighter-bomber. He was promoted to ''[[Major (Germany)|Major]]'' (major) on 1 May 1944. The ''Gruppe'' was renamed to IV./Ergänzungskampfgeschwader 1 (EKG 1—1st Supplemantary Bomber Wing). Barth stayed in this position until 31 March 1945 before being appointed ''[[Geschwaderkommodore]]'' (Wing Commander) of KG 51 on 19 April 1945. He led the Geschwader in [[Upper Bavaria]] until it was disbanded at the end of the war.<ref name="Kaiser pp88,89"/>

==F-84 Thunderstreak incident==
{{Main article|1961 F-84 Thunderstreak incident}}

Barth, commander of Jagdbombergeschwader 32, was initially removed of his command by the [[Federal Ministry of Defence (Germany)|Minister of Defence]], [[Franz-Josef Strauß]] for the [[1961 F-84 Thunderstreak incident]]. He was later, after a number of investigations and complaints, reinstated. On 14 September 1961, two [[Republic F-84F Thunderstreak|F-84F Thunderstreak]] of 32 Fighter Bomber Wing crossed into [[East Germany|East German]] airspace due to a navigational error, eventually landing at [[Berlin Tegel Airport]], evading a large number of [[Soviet Union|Soviet]] fighter planes. The event came at a historically difficult time during the [[Cold War (1953–1962)|Cold War]], one month after the construction of the [[Berlin Wall]].<ref>{{Cite news|journal=Der Spiegel |volume= 19|year=1962 |title= Bier-Order 61 Bundeswehr |url= http://www.spiegel.de/spiegel/print/d-45139958.html| language=German|accessdate=10 June 2013}}</ref>

Barth took his case to the ''Wehrdienstsenat des Bundesdisziplinarhofs'', the highest court of German troops, to have him cleared of the charges brought against him.  The court in Munich processed Barth's complaint on 20 December. The court invited as witnesses the generals [[Josef Kammhuber]], [[Martin Harlinghausen]], [[Werner Panitzki]] and [[Werner Streib]] as well as the lieutenant colonels [[Walter Krupinski]] and [[Walter Grasemann]]. However, the Federal Minister of Defence made it known through his Secretary of State, Volkmar Hopf, before the court, that the Minister sees himself unable to give the witness the testimony rights. Nevertheless, Strauß' conduct in dismissing Barth was found to be at fault, and the latter was reinstated in his position. Strauß however ignored this decision until [[Hellmuth Heye]], [[Ombudsman]] for the Military, forced him to accept it.<ref>{{Cite news|journal=Der Spiegel |volume= 12|year=1963 |title=Der Fall Barth Die Geschichte der "Bier-Order 61" |url= http://www.spiegel.de/spiegel/print/d-45142756.html| language=German|accessdate=12 June 2013}}</ref>

==Awards==
* [[Iron Cross]] (1939) 2nd and 1st Class
* [[Ehrenpokal der Luftwaffe]] on 4 May 1942 as ''[[Oberleutnant]]'' and pilot<ref>Patzwall 2008, p. 45.</ref>
* [[German Cross]] in Gold on 27 July 1942 as ''Oberleutnant'' in the 4./KG 51<ref>Patzwall & Scherzer 2001, p. 25.</ref>
* [[Knight's Cross of the Iron Cross]] on 2 October 1942 as ''[[Hauptmann]] '' and ''[[Staffelkapitän]]'' of the 4./KG 51<ref>Fellgiebel 2000, p. 123.</ref><ref>Scherzer 2007, p. 203.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Kaiser
  |first=Jochen
  |year=2010
  |title=Die Ritterkreuzträger der Kampfflieger—Band 1
  |trans_title=The Knight's Cross Bearers of the Bomber Fliers—Volume 1
  |language=German, English
  |location=Bad Zwischenahn, Germany
  |publisher=Luftfahrtverlag-Start
  |isbn=978-3-941437-07-4
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
{{Refend}}

{{s-start}}
{{s-mil}}
{{succession box|
before=Oberstleutnant [[Rudolf Hallensleben]]|
after=disbanded|
title=Commander of [[Kampfgeschwader 51]] |
years=19 April 1945 – 28 April 1945
}}
{{succession box|
before= —|
after=Oberst Paul Schauder|
title=Commander of [[Jagdbombergeschwader 32]] |
years=22 July 1958 – 27 October 1961
}}
{{s-end}}

{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{Authority control}}

{{DEFAULTSORT:Barth, Siegfried}}
[[Category:1916 births]]
[[Category:1997 deaths]]
[[Category:People from Augsburg]]
[[Category:German Air Force pilots]]
[[Category:Luftwaffe pilots]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:German World War II pilots]]
[[Category:People from the Kingdom of Bavaria]]