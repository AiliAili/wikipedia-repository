{{about|Boise, Idaho|the airport serving Boise City, Oklahoma|Boise City Airport}}
{{Use mdy dates|date=October 2012}}
{{Infobox airport
| name         = Boise Airport
| nativename   = Boise Air Terminal
| nativename-r = Gowen Field
| image        = File:Boise Airport Logo.jpg
| image-width  = 200
| image2       = Boise Airport-ID-05 July 1998-USGS.jpg
| image2-width = 250
| caption2     = 1998 USGS Photo
| IATA         = BOI
| ICAO         = KBOI
| FAA          = BOI
| WMO          = 72681
| type         = Public
| owner-oper   = City of Boise
| city-served  = [[Boise]], [[Idaho]]
| hub          = 
| location     =
| elevation-f  = 2,871
| elevation-m  = 875
| coordinates  = {{coord|43|33|52|N|116|13|22|W|region:US-ID|display=inline,title}}
| pushpin_map            = USA Idaho#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Idaho / United States
| pushpin_label          = '''BOI'''
| pushpin_label_position = right
| r1-number    = 10L/28R
| r1-length-f  = 10,000
| r1-length-m  =
| r1-surface   = [[Asphalt]]
| r2-number    = 10R/28L
| r2-length-f  = 9,763
| r2-length-m  =
| r2-surface   = Asphalt
| stat-year    = 2015
| stat1-header = Aircraft operations 
| stat1-data   = 121,782
| stat2-header = Based aircraft 
| stat2-data   = 264
| stat3-header = Total Passengers 
| stat3-data   = 2,978,281
| website      = {{URL|www.iflyboise.com}}
| footnotes    = Sources: [[Federal Aviation Administration]]<ref name=FAA>{{FAA-airport|ID=BOI|use=PU|own=PU|site=04149.*A}}, effective 2008-04-10</ref> <br>City of Boise<ref name="iflyboise">[http://www.iflyboise.com/about-boi/statistics/]</ref>
}}
'''Boise Airport''' {{airport codes|BOI|KBOI|BOI}} ('''Boise Air Terminal''' or '''Gowen Field''')<ref name=FAA /><ref name=faq>{{cite web | year = 2005 | url = http://www.cityofboise.org/Departments/Airport/FAQ/page4226.aspx | title = FAQs | work = Boise Airport | publisher = City of Boise | accessdate = August 31, 2006}}</ref> is a joint civil-military airport three miles south of [[Boise]] in [[Ada County, Idaho|Ada County]], [[Idaho]], United States.<ref name=FAA/> The airport is operated by the city of Boise Department of Aviation and is overseen by an Airport Commission.<ref name=admin>{{cite web | year = 2005 | url = http://www.cityofboise.org/Departments/Airport/AboutBoiseAirport/index.aspx | title = Airport Administration | author= Boise Airport | publisher = City of Boise | accessdate = August 31, 2006}}</ref> It is by far the busiest airport in the state of Idaho, serving more passengers than [[List of airports in Idaho|all other Idaho airports combined]] and roughly ten times as many passengers as Idaho's second busiest airport, [[Idaho Falls Regional Airport]].

Boise is a landing rights airfield requiring international general aviation flights to receive permission from a [[U.S. Customs and Border Protection|Customs and Border Protection]] officer before landing.

In addition to being a commercial and general aviation airport, Boise also functions concurrently as a [[USAF]] military facility as used by the [[124th Fighter Wing]] (124 FW) of the [[Idaho Air National Guard]] on the [[Gowen Field Air National Guard Base]] portion of the airport.  The 124 FW operates the [[A-10 Thunderbolt II]] aircraft.

The [[National Interagency Fire Center]] (NIFC) is based in the city of Boise and the Boise Airport is used for logistical support. The [[United States Forest Service]] (USFS) also uses Boise Airport as a base for [[aerial firefighting]] air tankers during the wildfire season.<ref name=global>{{cite web | date = January 21, 2006 | url = http://www.globalsecurity.org/military/facility/gowen.htm | title =  Gowen Field Air National Guard Base | work = GlobalSecurity.org | accessdate = August 31, 2006}}</ref>

Boise Airport had 1,487,764 enplaned passengers in 2015, growing from 1,378,352 passengers in 2014, for a total of 7.94% annual growth.  BOI ranked 72nd in the nation in enplanements for 2015.<ref>http://www.faa.gov/airports/planning_capacity/passenger_allcargo_stats/passenger/media/preliminary-cy15-all-enplanements.pdf</ref>

==History==
In 1926 the first municipal airport in Boise named Booth Field was built on a gravel bed where the [[Boise State University]] campus now stands. The first commercial airmail flight in the United States passed through this airfield on April 26, 1926, carried by [[Varney Airlines]]. Varney Airlines began operating out of Boise in 1933, later merging with [[National Air Transport]] to become [[United Airlines]]. With United Airlines able to trace its roots to Varney, United is recognized as the airline that has operated the longest out of Boise, 83 years as of 2009. United Airlines has contracted out its airport operation to [[SkyWest Airlines]] and no longer has any active employees in the State of Idaho, although as of May 2016 United Airlines operates two daily mainline flights between Boise (BOI) and Denver (DEN). The airfield also played host to [[Charles Lindbergh]]'s ''[[Spirit of St. Louis]]'' on September 4, 1927.<ref name=admin/>

The current airport has its origins in 1936 when Boise began buying and leasing land for the airport. By 1938 Boise had the longest runway in the United States, {{convert|8800|ft|0}}. The steel [[hangar]] for Varney Airlines was moved to the present field in 1939. As aircraft grew the hangar was no longer big enough and was converted into a passenger terminal. It was part of the modern [[Airport terminal|terminal]] facility until the completion of a new terminal in 2004.

During World War II the [[United States Army Air Corps|Army Air Corps]], later [[Army Air Forces]], leased the field for use as a training base for [[B-17 Flying Fortress]] and [[B-24 Liberator]] bomber crews. More than 6,000 men were stationed there during the war.<ref name=admin/>

The field was named Gowen Field in 1941 on July 23, after [[First lieutenant#U.S. Army, U.S. Marine Corps and U.S. Air Force|1st Lt]] Paul R. Gowen (1909–1938).<ref name=fgrpgwn>{{cite web|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=64096160 |publisher=Find a Grave |title=Paul R. Gowen |accessdate=May 23, 2014}}</ref><ref name=imhspirprgn>{{cite web|url=http://museum.mil.idaho.gov/Newsletters/3rdQtr2003.pdf |publisher=Idaho Military Historical Society: ''Pass in Review'' |title=Lt. Paul R. Gowen |date=September 2003|pages=5, 6}}</ref> Born and raised in [[Caldwell, Idaho|Caldwell]], he attended the [[University of Idaho]] for two years, then obtained an appointment to [[United States Military Academy|West Point]] in 1929, and graduated ninth in his class in 1933.<ref name=imhspirprgn/><ref>{{cite web |url=http://issuu.com/uidahodigital/docs/gem1928/381|work=Gem of the Mountains|publisher=University of Idaho|title=Beta Theta Pi|year=1928|page=365|accessdate=August 28, 2012}}</ref> While piloting a twin-engine [[Martin B-10|B-10]] bomber in the [[United States Army Air Corps|Army Air Corps]], Gowen was killed instantly in a crash in [[Panama]] in July 1938.<ref name=imhspirprgn/> The right engine failed shortly after take-off from [[Albrook Air Force Station|Albrook Field]], near [[Panama City, Panama|Panama City]]. The other two crew members, navigator and radio operator, survived and crawled from the wreckage with burns.<ref>{{cite journal |date=July 1998|title=Whence Came the Name . . . ? |journal=Gowen Research Foundation Electronic Newsletter |volume=1 |issue=7 |url=http://bz.llano.net/gowen/electronic_newsletter/el199807.htm | accessdate = 2006-08-31}}</ref><ref name=rootobitgwn>{{cite web |url=http://www.rootsweb.ancestry.com/~idfs/PObits/Obit-PRGowen.htm |work=rootsweb.ancestry.com |title=Obituary: Lt. Paul Gowen (1909–1939) |accessdate=August 28, 2012}}</ref>

After the war the part of the field used by the Army Air Forces was returned to the city.<ref name=admin/>  The [[Idaho Air National Guard]] began leasing the airfield after the war and continues to do so at the present time.<ref name=admin/>

The jet age arrived in Boise during the mid 1960s.  In 1966, United Airlines was operating [[Boeing 727-100]] jetliners into the airport with round trip routings of Boise (BOI)-Salt Lake City (SLC)-Chicago [[O'Hare Airport]] (ORD)-Boston (BOS) and Seattle (SEA)-Portland (PDX)-Boise (BOI)-Salt Lake City (SLC)-Denver (DEN)-Chicago (ORD)-New York [[Newark Airport]] (EWR).<ref>http://www.timetableimages.com, April 24, 1966 United Airlines system timetable</ref> United was also serving the airport with [[Douglas DC-6]] and [[Douglas DC-6B]] propliners at this time.  [[West Coast Airlines]] introduced [[Douglas DC-9-10]] jet service during the late 1960s and in 1968 was operating round trip routings of Seattle [[Boeing Field]] (BFI)-Portland (PDX)-Boise (BOI)-Salt Lake City (SLC) and Portland (PDX)-Seattle [[Boeing Field]] (BFI)-Boise (BOI)-Salt Lake City (SLC) with the DC-9.<ref>http://www.timetableimages.com, April 28, 1968 West Coast Airlines system timetable</ref>  West Coast was also serving Boise with [[Fairchild F-27]] turboprops and [[Douglas DC-3]] prop aircraft in 1968.  The same year West Coast merged with [[Bonanza Air Lines]] and [[Pacific Air Lines]] to form Air West which was subsequently renamed [[Hughes Airwest]] which in turn continued to serve Boise with Douglas DC-9-10 and [[McDonnell Douglas DC-9-30]] jets. In 1972 Hughes Airwest was operating nonstop DC-9 jet service from the airport to Portland and Salt Lake City and was also flying direct, no change of plane DC-9 service to Los Angeles (LAX), Las Vegas (LAS), Phoenix (PHX), San Diego (SAN), Burbank (BUR), Santa Ana (SNA), Spokane (GEG) and other regional destinations.<ref>http://www.departedflights.com, July 1, 1972 Hughes Airwest system timetable</ref>

By 1976, Hughes Airwest and United were still the only two airlines operating jet service into Boise according the [[Official Airline Guide]] ([[OAG]]).  United had also expanded its Boise service by this time and was operating nonstop flights with [[Boeing 727-100]], [[Boeing 727-200]] and larger [[Douglas DC-8]] jetliners to Chicago [[O'Hare Airport]], Denver, Portland, Salt Lake City, San Francisco, Seattle, Reno and Spokane as well as direct, no change of plane jet service to New York [[LaGuardia Airport]], Los Angeles, Boston, Washington, D.C [[National Airport]], San Diego and Hartford according to the OAG.<ref>Feb. 1, 1976 North American Edition Official Airline Guide, Boise flight schedules</ref>  United and Hughes Airwest were operating all of their flights into Boise with jet aircraft at this time.

Following the federal [[Airline Deregulation Act]] of 1978, a number of air carriers operated jet service into the airport at different times over the years from the late 1970s through the 1990s.   The following list of airlines is taken from [[Official Airline Guide]] ([[OAG]]) editions from 1979 to 1999:<ref>http://www.departedflights.com, Official Airline Guide (OAG) editions, Nov. 15, 1979 through June 1, 1999, Boise flight schedules</ref>

* [[Alaska Airlines]] (mainline jet service)
* [[America West Airlines]]
* [[Cascade Airways]]
* [[Continental Airlines]]
* [[Frontier Airlines (1950-1986)]]
* [[Horizon Air]]
* [[Morris Air]]
* [[Northwest Airlines]]
* [[Pacific Express]]
* [[Pacific Southwest Airlines]] (PSA)
* [[Republic Airlines (1979-1986)]] (acquired and merged with Hughes Airwest in 1980)
* [[Sunworld International Airways]] (operating as Sunworld Airlines)
* [[United Express]] operated by [[Air Wisconsin]]
* [[Western Airlines]]
* [[Wien Air Alaska]]

Between 2001 and 2005 Boise Airport was remodeled. The airport now has a new terminal and an elevated roadway for departures. There were two phases in building the new terminal. Phase 1 considered amenities such as baggage claim, lobby, and food and beverage concession, which were completed in 2003. Phase 2 dealt with security checkpoints and a new concourse (Concourse C) and the remodeling of Concourse B, which were completed in 2005.<ref>{{cite web |title=History of BOI|url=http://www.iflyboise.com/airport-guide/about-the-airport/history-of-boi/|publisher=City of Boise|accessdate=May 23, 2013}}</ref>

[[File:Boise airport terminal 2009.jpg|thumb|Boise Airport's passenger terminal]]
The Boise Airport Passenger Terminal designed by [[CSHQA]] is a three-story, steel-framed {{convert|378000|sqft|m2|adj=on}} state-of-the-art aviation facility. Curvilinear, steel trusses create the undulating ceiling plane of the ticket lobby and define the signature profile of the building. The terminal has garnered national attention for the beauty of its design and is considered a prototypical post 9/11 facility.<ref>[http://www.cshqa.com/project_detail.php?cid=4&did=10 CSHQA Architecture, Engineering, Planning, Boise Idaho]. Cshqa.com.</ref>

The Boise Airport was fourth in passenger satisfaction in the [[J.D. Power and Associates]] 2004 Global Airport Satisfaction Index Study.<ref>{{cite web |date=December 6, 2004|url=http://www.jdpower.com/pdf/2004197.pdf#page=6|title=2004 Global Airport Satisfaction Index Study|format=PDF|publisher=J.D. Power and Associates|accessdate=August 31, 2006}}
</ref>

The Boise Airport was a hub for [[Horizon Air]] from the late 1980s to the early 2000s. Horizon Air was acquired by the [[Alaska Air Group]], the parent company of [[Alaska Airlines]], in 1986 and began [[code sharing]] flights for Alaska Airlines at that time.  During the summer of 1990, Horizon Air was operating up to 36 departures a day from the airport to destinations in Idaho, Oregon and Washington as well as direct one stop service to Salt Lake City.<ref>http://www.departedflights.com, July 1, 1990 Alaska Airlines/Horizon Air system timetable</ref>  By 1999, Horizon Air was operating up to 22 departures a day from Boise with [[Fokker F28 Fellowship]] jets with additional flights being operated with [[de Havilland Canada DHC-8 Dash 8]] turboprops.<ref>http://www.departedflights.com, June 1, 1999 Official Airline Guide (OAG), Boise flight schedules</ref>  The regional airline also previously operated [[Dornier 328]], [[Fairchild F-27]] and [[Swearingen Metroliner]] propjets.<ref>http://www.departedflights.com, Feb. 15, 1985 through June 1, 1999 editions, Official Airline Guide (OAG), Boise flight schedules</ref>   Boise is currently a focus city for [[Alaska Airlines]] service operated by both Horizon Air and [[code sharing]] partner [[SkyWest Airlines]].

Boise was also one of the primary destinations served by [[Cascade Airways]] which competed with Horizon Air.  In 1985, Cascade was serving the airport with [[British Aircraft Corporation]] [[BAC One-Eleven]] jets and [[Swearingen Metroliner]] propjets with regional service in Idaho, Oregon, Washington and Montana as well as nonstop jet service to Reno, Nevada and connecting flights to Calgary, Alberta in Canada.<ref>http://www.cascadeairways.com, Jan. 13, 1985 & April 4, 1985 Cascade Airways timetables</ref>

In 2013, [[Allegiant Air]] began nonstop flights once a week between Boise and Honolulu, Hawaii operated with [[Boeing 757-200]] jetliners.<ref>http://www.iflyboise.com/airport-guide/about-the-airport.news-releases/newsreleases/2012-news-releases/allegiant-air-announces-vegas-Honolulu/</ref>  The airline subsequently ceased this service to Hawaii but continues to operate nonstop flights from the airport to Las Vegas and Los Angeles at the present time.

==Facilities==
Boise Airport covers {{convert|5000|acre|ha}} at an [[elevation]] of {{convert|2871|ft}}. It has three [[runway]]s:
* 10L/28R: 10,000 x 150 feet (3,048 x 46 m) Asphalt, Weight capacity: {{convert|75000|lb}}/single wheel; [[Visual Approach Slope Indicator|VASI]] system<ref name=FAA />
* 10R/28L: 9,763 x 150 feet (2,976 x 46 m) Asphalt, Weight capacity: {{convert|75000|lb}}/single wheel; [[Visual Approach Slope Indicator|VASI]],  [[Instrument Landing System|ILS]]/[[Distance Measuring Equipment|DME]]<ref name=FAA />
* 09/27: 5,000 x 90 feet (1,524 x 27 m) Asphalt, Weight capacity: unspecified; Restrictions: Military only<ref name="airport operations">[http://www.iflyboise.com/airport-guide/airport-operations/ FAA and Airport Operations]. Iflyboise.com.</ref>

In the year ending April 30, 2007 the airport had 184,023 aircraft operations, average 504 per day: 52% [[general aviation]], 23% airline, 18% [[air taxi]], 7% military. 286 aircraft were then based at this airport: 58% single-engine, 10% multi-engine, 7% jet, 9% [[helicopter]] and 16% military.<ref name=FAA />

In 2005 over 3 million passengers passed through the airport.<ref>{{cite web | year = 2005 | url = http://www.cityofboise.org/Departments/Airport/Statistics/2005/page4229.aspx | title = Statistics 2005 | work = Boise Airport | publisher = City of Boise | accessdate = August 31, 2006}}</ref>

The airport can handle minor maintenance and repairs through [[fixed-base operator]]s Jackson Jet Center, Turbo Air and Western Aircraft.

Law enforcement is handled by the Boise Police Department.  The Airport Division has an authorized strength of 1 lieutenant, 2 sergeants, and 28 officers.  There are currently 5 TSA certified [[Detection dog|K-9 units]] trained in explosive detection.<ref>{{cite web | year = 2005 | url = http://www.cityofboise.org/Departments/Airport/AboutBoiseAirport/AirportPolice/page1949.aspx | title = Airport Police | work = Boise Airport | publisher = City of Boise | accessdate = August 31, 2006}}</ref>

===New ATC tower===
[[File:Boise new tower construction 2009.jpg|thumb|The new air traffic control tower under construction in 2009.]]
In 2008, city officials broke ground for Boise Air Terminal's latest improvement on January 4, a new airport traffic control tower. The tower's height at {{convert|295|ft|m}} made it the tallest building in the state of [[Idaho]] until it was surpassed by the Zions Bank Idaho Headquarters Building in 2013 (at 323&nbsp;ft), and the [[Pacific Northwest]]'s tallest control tower. It was relocated to the south side of the airport in order to control an existing [[Idaho Air National Guard|Guard]] assault strip, runway 09/27, south of Gowen Field. The tower was planned and constructed when it was believed that the radar functions would be moved to [[Salt Lake City Air Route Traffic Control Center|Salt Lake City]] in [[Utah]]. After it was decided to leave the radar positions in Boise, the facility at the base of the tower was redesigned and partially remodeled to house the Terminal Radar Approach Control ([[TRACON]]).

The tower and TRACON opened on September 16, 2013 with updated electronics and equipment, including the [[Standard Terminal Automation Replacement System|STARS]] radar system; improving services and safety for pilots and the flying public. With the expanded facilities and new equipment, the TRACON operates the approach control for Boise Airport, and also remotely operates the approach control for the [[Bozeman Yellowstone International Airport|Bozeman Airport]] in [[Montana]]. The TRACON was then renamed Big Sky Approach to reflect the broader geographical coverage. The consolidation of Boise and Bozeman approach control facilities into Big Sky Approach is part of the FAA's continuing plan to consolidate approach control services across the nation. Boise's TRACON was designed with the option of adding additional radar scopes, and may offer approach control services to other airports in the future.

===Gowen Field Air National Guard Base===
[[Image:C130s on gowen field in boise idaho.jpg|thumb|right|250px|C-130s previously operated by the Idaho ANG parked on the ramp at Gowen Field.]]
[[Gowen Field Air National Guard Base]] primarily refers to the military facilities on the south side of the runways, which includes [[Air National Guard]], [[Army National Guard]], and reserve units of the [[United States Army Reserve|Army]], [[United States Navy Reserve|Navy]], and [[Marine Forces Reserve|Marine Corps]]. The field is home to the [[124th Wing|124th Fighter Wing]] (124 FW), [[Idaho Air National Guard]], which consists of one flying squadron operationally-gained by the [[Air Combat Command]] (ACC) and 12 additional support units. The aircraft based at Gowen Field ANGB is the [[A-10 Thunderbolt II]] [[close air support]] attack aircraft of the [[190th Fighter Squadron]] (190 FS).

The 124 FW was previously designated as the [[124th Wing]] (124 WG), a composite [[Air Combat Command]] (ACC) and [[Air Mobility Command]] (AMC) unit that also operated [[C-130 Hercules|C-130H Hercules]] transport aircraft in the [[189th Airlift Squadron]] (189 AS), the 189 AS being operationally-gained by AMC.

[[Base Realignment and Closure|BRAC]] 2005 directed that the [[Idaho Air National Guard]] divest itself of the C-130 mission by 2009, transferring its C-130s to the [[Wyoming Air National Guard]], while retaining its A-10 fighter mission.  This action was completed in 2009 and the 124 WG was redesignated the 124 FW at that time.  The 124 FW is composed of over 1000 military personnel, consisting of just over 300 full-time [[Active Guard Reserve|Active Guard and Reserve (AGR)]] and [[Air Reserve Technician Program|Air Reserve Technician (ART)]] personnel and over 700 traditional part-time Air National Guardsmen.<ref name=global/><ref>[http://www.globalsecurity.org/military/agency/usaf/124wg.htm 124th Wing [124th WG&#93;]. Globalsecurity.org (December 31, 1952).</ref>

===First responder training area===

In February 2011, [[FedEx]] donated a surplus [[Boeing 727-200]] cargo jet (tail number N275FE) to the City of Boise for use as a training tool for emergency [[first responder]]s.  The aircraft—stripped of engines—is permanently parked near the southeastern end of Boise's third runway—a location more than a mile southeast of, and not visible from, the main [[airport terminal|passenger terminal]].  A variety of agencies use the decommissioned aircraft for training purposes.

"As we retire N275FE from our fleet, we are proud to give back to the aviation community," said David P. Sutton, Managing Director of Aircraft Acquisitions & Sales for FedEx. "The [[donation]] of this aircraft is just one example of the many ways FedEx supports training and other educational endeavors, reflecting the community spirit shared by all 1,334 FedEx team members in the State of Idaho, many of them living and working right here in the Boise community."<ref>{{cite web|title=FedEx Donates 727 for BOI Training|url=http://www.iflyboise.com/airport-guide/about-the-airport/news-releases/newsreleases/2011-news-releases/fedex-donates-727-for-boi-training/|website=www.iflyboise.com|publisher=Boise Airport|accessdate=20 September 2015|date=9 February 2011}}</ref>

==Airlines and destinations==

===Passenger===
{{Airport-dest-list
| [[Alaska Airlines]] | [[Seattle-Tacoma International Airport|Seattle/Tacoma]] (begins June 4, 2017)<ref>http://www.idahostatesman.com/news/business/article113188468.html</ref>
| [[Alaska Airlines]]<br>{{nowrap|operated by [[Horizon Air]]}} | [[Lewiston-Nez Perce County Airport|Lewiston]], [[Portland International Airport|Portland (OR)]], [[Reno-Tahoe International Airport|Reno/Tahoe]], [[Sacramento International Airport|Sacramento]], [[San Jose International Airport|San Jose (CA)]], [[Salt Lake City International Airport|Salt Lake City]], [[Seattle–Tacoma International Airport|Seattle/Tacoma]], [[Spokane International Airport|Spokane]]
| [[Alaska Airlines]]<br>{{nowrap|operated by [[SkyWest Airlines]]}} | [[Portland International Airport|Portland (OR)]], [[San Diego International Airport|San Diego]], [[Seattle–Tacoma International Airport|Seattle/Tacoma]]
| [[Allegiant Air]] | [[McCarran International Airport|Las Vegas]], [[Los Angeles International Airport|Los Angeles]]
| [[American Airlines]] | [[Dallas/Fort Worth International Airport|Dallas/Fort Worth]], [[Phoenix Sky Harbor International Airport|Phoenix–Sky Harbor]]
| [[American Eagle (airline brand)|American Eagle]] | [[O'Hare International Airport|Chicago-O'Hare]] (begins July 5, 2017)<ref>http://www.chicagotribune.com/business/ct-american-new-ohare-routes-0311-biz-20170310-story.html</ref>
| [[Delta Air Lines]] | [[Minneapolis-Saint Paul International Airport|Minneapolis/St. Paul]], [[Salt Lake City International Airport|Salt Lake City]]
| [[Delta Connection]] | [[Los Angeles International Airport|Los Angeles]], [[Minneapolis-Saint Paul International Airport|Minneapolis/St. Paul]], [[Salt Lake City International Airport|Salt Lake City]], [[Seattle–Tacoma International Airport|Seattle/Tacoma]]
| [[Gem Air]] | '''Seasonal:''' [[McCall Municipal Airport|McCall]], [[Lemhi County Airport|Salmon]], Stanley
| [[Southwest Airlines]] | [[Denver International Airport|Denver]], [[McCarran International Airport|Las Vegas]], [[Oakland International Airport|Oakland]], [[Phoenix Sky Harbor International Airport|Phoenix–Sky Harbor]], [[Sacramento International Airport|Sacramento]], [[San Diego International Airport|San Diego]] (begins June 4, 2017),<ref>http://www.routesonline.com/news/38/airlineroute/270757/southwest-airlines-adds-new-service-from-june-2017/</ref> [[Spokane International Airport|Spokane]]<br>'''Seasonal''': [[Midway International Airport|Chicago–Midway]]
| [[United Airlines]] | [[Denver International Airport|Denver]]<br>'''Seasonal:''' [[O'Hare International Airport|Chicago–O’Hare]]
| [[United Express]] | [[O'Hare International Airport|Chicago–O’Hare]], [[Denver International Airport|Denver]], [[George Bush Intercontinental Airport|Houston–Intercontinental]], [[Los Angeles International Airport|Los Angeles]], [[San Francisco International Airport|San Francisco]]
}}

In 2014, Boise was given a $700,000 grant from the U.S. Department of Transportation to help them start up nonstop service to [[Hartsfield-Jackson Atlanta International Airport|Atlanta]] flown by [[Delta Air Lines]]. According to Boise Airport Director Rebecca Hupp, the earliest that Delta would begin offering the Atlanta service was Summer 2015.<ref>{{cite news|last1=Berg|first1=Sven|title=Boise wins federal grant for Delta to launch non-stop flights to Atlanta|url=http://www.idahostatesman.com/2014/10/02/3406477_boise-wins-grant-for-atl-flight.html?rh=1|accessdate=February 9, 2015|work=Idaho Statesman|date=October 2, 2014}}</ref>  However, as of August 1, 2016 Delta was not operating nonstop service between Boise and its hub in Atlanta according to its flight schedule.<ref>http://www.delta.com, Flight Schedules</ref>

===Cargo===
{{Airport-dest-list
|[[Ameriflight]] | [[Burns Municipal Airport|Burns]], [[Portland International Airport|Portland (OR)]], [[Salt Lake City International Airport|Salt Lake City]], [[Boeing Field|Seattle–Boeing]]
|[[FedEx Express]] | [[Casper/Natrona County International Airport|Casper]], [[Memphis International Airport|Memphis]], [[Portland International Airport|Portland (OR)]], [[Salt Lake City International Airport|Salt Lake City]]
|[[UPS Airlines]] | [[Louisville International Airport|Louisville]], [[Salt Lake City International Airport|Salt Lake City]], [[Lambert-St. Louis International Airport|St. Louis]]
|Western Air Express | [[Lewiston-Nez Perce County Airport|Lewiston]], [[Portland International Airport|Portland (OR)]], [[Salt Lake City International Airport|Salt Lake City]], [[Spokane International Airport|Spokane]], [[Magic Valley Regional Airport|Twin Falls]]
}}

==Statistics==

===Top destinations===
{| class="wikitable sortable" style="font-size: 95%"
|+ '''Top Domestic Routes from BOI <br>(Dec 2015 – Nov 2016)'''<ref>[http://www.transtats.bts.gov/airports.asp?pn=1&Airport=BOI&Airport_Name=Boise,%20ID:%20Boise%20Air%20Terminal&carrier=FACTS RITA | BTS | Transtats]. Transtats.bts.gov.</ref>
|-
! Rank
! City
! Passengers
! Carriers
|-
| 1
| [[Seattle–Tacoma International Airport|Seattle/Tacoma, Washington]]
| 259,000
| Alaska, Delta
|-
| 2
| [[Denver International Airport|Denver, Colorado]]
| 214,000
| Southwest, United
|-
| 3
| [[Salt Lake City International Airport|Salt Lake City, Utah]]
| 177,000
| Alaska, Delta
|-
| 4
| [[Portland International Airport|Portland, Oregon]]
| 138,000
| Alaska
|-
| 5
| [[Phoenix Sky Harbor International Airport|Phoenix–Sky Harbor, Arizona]]
| 126,000
| American, Southwest
|-
| 6
| [[McCarran International Airport|Las Vegas, Nevada]]
| 94,000
| Allegiant, Southwest
|-
| 7
| [[Minneapolis-Saint Paul International Airport|Minneapolis/St. Paul, Minnesota]]
| 89,000
| Delta
|-
| 8
| [[Spokane International Airport|Spokane, Washington]]
| 78,000
| Alaska, Southwest
|-
| 9
| [[San Francisco International Airport|San Francisco, California]]
| 75,000
| United
|-
| 10
| [[Oakland International Airport|Oakland, California]]
| 67,000
| Southwest
|}

===Annual traffic===
{| class="wikitable sortable" style="font-size: 95%"
|+ '''Annual passenger traffic (enplaned + deplaned) at BOI Airport, 2006 through 2016<ref>{{cite web|url=http://www.iflyboise.com/airport-guide/about-the-airport/statistics/|title=Statistics|publisher=Iflyboise.com}}</ref>
! Year
! Passengers
|-
| 2016||3,230,878
|-
| 2015||2,978,281
|-
| 2014||2,753,153
|-
| 2013||2,612,457
|-
| 2012||2,609,816
|-
| 2011||2,781,708
|-
| 2010||2,805,692
|-
| 2009||2,795,297
|-
| 2008||3,185,006
|-
| 2007||3,365,303
|-
| 2006||3,289,314
|}

==Accidents and incidents==
*On December 9, 1996, [[Douglas C-47 Skytrain|Douglas C-47A]] N75142 of [[Emery Worldwide]] crashed on approach to Boise Airport killing both crew. The aircraft was on a cargo flight to [[Salt Lake City International Airport]] when the starboard engine caught fire shortly after take-off and the decision was made to return to Boise.<ref name=ASN091296>{{cite web|url=http://aviation-safety.net/database/record.php?id=19961209-0 |title=N75142 Accident description|publisher=Aviation Safety Network |accessdate=June 25, 2010}}</ref>
*On February 3, 2012, a [[Lancair IV-P]]T turboprop (N321LC) flown by [[Steve Appleton]], CEO of [[Micron Technology]], crashed shortly after take-off from runway 10R, killing the pilot.  Appleton was attempting an emergency landing, and had aborted an earlier take-off attempt for unknown reasons.<ref>[http://www.idahostatesman.com/2012/02/03/1979665/micron-ceo-steve-appleton-dies.html Steve Appleton, CEO of Micron, dies in airplane crash at Boise Airport], ''[[Idaho Statesman|IdahoStatesman.com]]'', February 3, 2012.</ref><ref>[http://www.idahostatesman.com/2012/02/04/1980571/final-flight-lasted-80-seconds.html Micron CEO Steve Appleton's final flight lasted 80 seconds], ''[[Idaho Statesman]]'', February 4, 2012.</ref>

==References==
{{reflist|2}}

==External links==
*{{official website|http://www.iflyboise.com}}
*{{FAA-diagram|00057}}
*{{FAA-procedures|BOI}}
*{{US-airport|BOI}}

[[Category:1936 establishments in Idaho]]
[[Category:Airports in Idaho]]
[[Category:Buildings and structures in Boise, Idaho]]
[[Category:Transportation in Ada County, Idaho]]
[[Category:Airports established in 1936]]