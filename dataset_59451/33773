{{Infobox film
| name           = Summer of '42
| image          = Summer of '42 POSTER.jpg
| border         = yes
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = [[Robert Mulligan]]
| producer       = Richard A. Roth
| writer         = [[Herman Raucher]]
| narrator       = Robert Mulligan
| starring       = [[Jennifer O'Neill]]<br />[[Gary Grimes]]<br />[[Jerry Houser]]<br />[[Oliver Conant]]
| music          = [[Michel Legrand]]
| cinematography = [[Robert Surtees (cinematographer)|Robert Surtees]]
| editing        = [[Folmar Blangsted]]
| distributor    = [[Warner Bros.|Warner Bros. Pictures]]
| released       = {{Film date|1971|04|18}}
| runtime        = 104 minutes<!--Theatrical runtime: 103:43--><ref>{{cite web|title=''SUMMER OF '42'' (X)|url=http://www.bbfc.co.uk/releases/summer-42|work=[[British Board of Film Classification]]|date=1971-04-15|accessdate=2013-02-06}}</ref>
| country        = United States
| language       = English
| budget         = $1 million
| gross          = $32,063,634<ref>{{cite web|url=http://www.the-numbers.com/movies/1971/0SM42.php|publisher=The Numbers|title=Summer of '42, Box Office Information|accessdate=January 12, 2012}}</ref>
}}
'''''Summer of '42''''' is a 1971 American [[coming-of-age]] [[comedy-drama]] film based on the memoirs of screenwriter [[Herman Raucher]]. It tells the story of how Raucher, in his early teens on his 1942 summer vacation on [[Nantucket, Massachusetts|Nantucket Island]] (off the coast of Cape Cod), embarks on a one-sided romance with a young woman, Dorothy, whose husband had gone off to fight in [[World War II]].

The film was directed by [[Robert Mulligan]], and starred [[Gary Grimes]] as Hermie, [[Jerry Houser]] as his best friend Oscy, [[Oliver Conant]] as their nerdy young friend Benjie, [[Jennifer O'Neill]] as Hermie's mysterious love interest, and Katherine Allentuck and [[Christopher Norris (actress)|Christopher Norris]] as a pair of girls whom Hermie and Oscy attempt to seduce. Mulligan also has an uncredited role as the voice of the adult Hermie. [[Maureen Stapleton]] (Allentuck's real-life mother) also appears in a small, uncredited voice role (calling after Hermie as he leaves the house in an early scene, and after he enters his room in a later scene).

Raucher's [[novelization]] of his screenplay [[Summer of '42 (book)|of the same name]] was released prior to the film's release and became a runaway bestseller, to the point that audiences lost sight of the fact that the book was based on the film and not vice versa. Though a [[popular culture|pop culture]] phenomenon in the first half of the 1970s, the novelization went out of print and slipped into obscurity throughout the next two decades until a [[Broadway theatre|Broadway]] adaptation in 2001 brought it back into the public light and prompted [[Barnes & Noble]] to acquire the publishing rights to the book.

==Synopsis==
The film opens with a series of still photographs appearing over melancholic music, representing the abstract memories of the unseen Herman Raucher, now a middle-aged man. We then hear Raucher recalling the summer he spent on the island in 1942. The film flashes back to a day that then 15-year-old "Hermie" and his friends – [[Jock (subculture)|jock]] Oscy and introverted [[nerd]] Benjie – spent playing on the beach. They spot a young soldier carrying his new bride into a house on the beach and are struck by her beauty, especially Hermie, who is unable to get her out of his mind.

They continue spending afternoons on the beach where, in the midst of scantily-clad teenage girls, their thoughts invariably turn to sex. All of them are virgins: Oscy is obsessed with the act of sex, while Hermie finds himself developing [[romantic interest]] in the bride, whose husband he spots leaving the island on a water taxi one morning. Later that day, Hermie finds her trying to carry bags of groceries by herself, and helps get them back to her house. They strike up a friendship and he agrees to return to help her with chores.

Meanwhile, Oscy and Hermie, thanks to a sex manual discovered by Benjie, become convinced they know everything necessary to lose their virginity. Led by Oscy, they test this by going to the cinema and picking-up a trio of high-school girls. Oscy stakes out the most attractive one, Miriam, "giving" Hermie her less attractive friend, Aggie, and leaving Benjie with Gloria, a heavyset girl with [[Dental brace|braces]]. Frightened by the immediacy of sex, Benjie runs off, and is not seen by Hermie or Oscy again that night. Hermie and Oscy spend the entirety of the evening's film attempting to "put the moves" on Miriam and Aggie. Oscy pursues Miriam, eventually making out with her during the movie, and later learns her ways are well-known on the island. Hermie finds himself succeeding with Aggie, who allows him to grope what he thinks is her breast; Oscy later points out Hermie was fondling her arm.

The next morning, Hermie helps the bride move boxes into her attic and she thanks him by giving him a kiss on the forehead. Later, in preparation for a marshmallow roast on the beach with Aggie and Miriam, Hermie goes to the local drugstore. In a painfully humorous sequence, he builds up the nerve to ask the druggist (Lou Frizzell) for condoms.

That night, Hermie roasts marshmallows with Aggie while Oscy succeeds in having sex with Miriam between the dunes. He is so successful he sneaks over to Hermie and Aggie to ask for more condoms. Confused as to what's happening, Aggie follows Oscy back, where she sees him having sex with Miriam and runs home, upset.

The next day, Hermie comes across the bride sitting outside her house, writing to her husband. Hermie offers to keep her company that night and she says she looks forward to seeing him, revealing her name is Dorothy. An elated Hermie goes home and puts on a suit, dress shirt and heads back to Dorothy's house, running into Oscy on the way; Oscy relates that Miriam's appendix burst and she's been rushed to the mainland. Hermie, convinced he is at the brink of adulthood because of his relationship with Dorothy, brushes Oscy off.

He heads to her house, which is eerily quiet. Going in, he discovers a bottle of whiskey, several cigarette butts, and a telegram from the government. Dorothy's husband is dead, his plane shot down over France. Dorothy comes out of her bedroom, crying, and Hermie tells her "I'm sorry." The sense of empathy triggers her to channel to Hermie some of her loneliness. She turns on the record player and invites Hermie to dance with her. They kiss and embrace, tears on both their faces. Without speaking, and to the sound only of the waves, they move to the bedroom, where she draws him into bed and gently makes love with him. Afterward, withdrawing again into her world of hurt, Dorothy retires to the porch, leaving Hermie alone in her bedroom. He approaches her on the porch, where she can only quietly say "Good night, Hermie." He leaves, his last image of Dorothy being of her leaning against the railing, as she smokes a cigarette and stares into the night sky.

At dawn Hermie meets Oscy and the two share a moment of reconciliation, with Oscy informing Hermie that Miriam will recover. Oscy, in an uncharacteristic act of sensitivity, lets Hermie be by himself, departing with the words, "Sometimes life is one big pain in the ass."

Trying to sort out what has happened, Hermie goes back to Dorothy's house. Dorothy has fled the island in the night and an envelope is tacked to the front door with Hermie's name on it. Inside is a note from Dorothy, saying she hopes he understands she must go back home as there is much to do. She assures Hermie she will never forget him, and he will find his way of remembering what happened that night. Her note closes with the hope that Hermie may be spared the senseless tragedies of life.

In the final scene, Hermie, suddenly approaching manhood, is seen looking at Dorothy's old house and the ocean from a distance before he turns to join his friends.  To bittersweet music, the adult Raucher sadly recounts that he has never seen Dorothy again or learned what became of her.

==Cast==
{{div col|col=2}}
* [[Jennifer O'Neill]] as Dorothy
* [[Gary Grimes]] as Hermie
* [[Jerry Houser]] as Oscy
* [[Oliver Conant]] as Benjie
* Katherine Allentuck as Aggie
* [[Christopher Norris (actress)|Christopher Norris]] as Miriam
* Lou Frizzell as Druggist
* [[Robert Mulligan]] (''uncredited voice'') as Narrator/Older Herman Raucher
* Walter Scott (''uncredited'') as Dorothy's husband
* [[Maureen Stapleton]] (''uncredited voice'') as Hermie's Mother
{{div col end}}

==Production==
[[Herman Raucher]] wrote the film script in the 1950s during his tenure as a television writer, but "couldn't give it away."<ref name=herm>{{cite news |url=http://web.tcpalm.com/specialreports/summerof42/raucher.html |title=Herman Raucher Interview (extended) |last=Park |first=Louis Hillary |newspaper=TCPalm |date=May 2002 |accessdate= July 5, 2006|dead-url=yes|archive-url=https://web.archive.org/web/20060206230410/http://web.tcpalm.com/specialreports/summerof42/raucher.html|archive-date=2006-02-06}}</ref> In the 1960s, he met [[Robert Mulligan]], best known for directing ''[[To Kill a Mockingbird (film)|To Kill a Mockingbird]]''. Raucher showed Mulligan the script, and Mulligan took it to [[Warner Bros.]], where Mulligan argued the film could be shot for the relatively low price of $1 million, and Warner approved it.<ref name=herm/> They had so little faith in the film becoming a box-office success, though, they shied from paying Raucher outright for the script, instead promising him ten percent of the gross.<ref name=herm/>

When casting for the role of Dorothy, Warner Bros. declined to audition any actresses younger than the age of 30; [[Jennifer O'Neill]]'s agent, who had developed a fondness for the script, convinced the studio to audition his client, who was only 22 at the time. O'Neill auditioned for the role, albeit hesitantly, not wanting to perform any nude scenes. O'Neill got the role and Mulligan agreed to find a way to make the film work without blatant nudity.<ref name=TCPalm>{{cite news |url=http://web.tcpalm.com/specialreports/summerof42/oneill.html |last=Park |first=Louis Hillary|title=Jennifer O’Neill Interview (extended) |newspaper=TCPalm |date=June 2002 |accessdate=February 8, 2012|dead-url=yes|archive-url=https://web.archive.org/web/20120205005107/http://web.tcpalm.com/specialreports/summerof42/oneill.html|archive-date=2012-02-05}}</ref>

Though the film took place on Nantucket, by the 1970s the island was too far modernized to be convincingly transformed to resemble a 1940s resort, so production was taken to Mendocino, California, on the West Coast of the US.<ref name=herm/> Shooting took place over eight weeks, during which O'Neill was sequestered from the three boys cast as "The Terrible Trio," in order to ensure that they did not become close and ruin the sense of awkwardness and distance that their characters felt towards Dorothy. Production ran smoothly, finishing on schedule.<ref name=herm/>

After production, Warner Bros., still wary about the film only being a minor success, asked Raucher to adapt his script into a book.<ref name=herm/> Raucher wrote it in three weeks, and Warner Bros. released it prior to the film to build interest in the story.<ref name=herm/> The book quickly became a national bestseller, so that when trailers premiered in theaters, the film was billed as being "based on the national bestseller," despite the film having been completed first.<ref name=herm/><ref name=so42/> Ultimately, the book became one of the best selling novels of the first half of the 1970s, requiring 23 reprints between 1971 and 1974 to keep up with customer demand.<ref name=so42/>

==Factual basis==
The film (and subsequent novel) were [[memoir]]s written by Herman Raucher; they detailed the events in his life over the course of the summer he spent on [[Nantucket Island]] in 1942 when he was fourteen years old.<ref name=herm/> Originally, the film was meant to be a tribute to his friend Oscar "Oscy" Seltzer, an [[combat medic|Army medic]] killed in the [[Korean War]].<ref name=herm/><ref name=osc>[http://www.koreanwar.org/html/korean_war_project_remembrance.html?KCCF1__KEY=26993 Korean War Memorial – Oscar Seltzer] ''koreanwar.org''. Retrieved July 5, 2006.</ref> Seltzer was shot dead on a battlefield in Korea whilst attending to a wounded man; this happened on Raucher's birthday, and consequently, Raucher has not celebrated a birthday since. During the course of writing the screenplay, Raucher came to the realization that despite growing up with Oscy and having bonded with him through their formative years, the two had never really had any meaningful conversations or gotten to know one another on a more personal level.<ref name=herm/>

Instead, Raucher decided to focus on the first major adult experience of his life, that of falling in love for the first time. The woman (named Dorothy, like her screen counterpart) was a fellow vacationer on the island whom the 14-year old Raucher had befriended one day when he helped her carry groceries home; he became a friend of her and her husband and helped her with chores after her husband was called to fight in [[World War II]]. On the night memorialized in the film, Raucher randomly came to visit her, unaware his arrival was just minutes after she received notification of her husband's death. She was confused and upset, had been drinking heavily, and repeatedly called Raucher by her husband's name. Although both ultimately   disrobed, contrary to popular opinion, sexual intercourse did not occur. Raucher admitted this in a 2002 interview saying it was mostly holding, but in the movie "We let you think what you want."<ref name=herm/> The next morning, Raucher discovered that she had left the island, leaving behind a note for him (which is read at the end of the film and reproduced in the book). He never saw her again; his last "encounter" with her, recounted on an episode of ''[[The Mike Douglas Show]]'', came after the film's release in 1971, when she was one of over a dozen women who wrote letters to Raucher claiming to be "his" Dorothy.<ref name=doug>The Mike Douglas Show, Interview with Herman Raucher. Original date of broadcast unknown.</ref> Raucher recognized the "real" Dorothy's handwriting, and she confirmed her identity by making references to certain events only she could have known about.<ref name=doug/> She told Raucher that she had lived for years with the guilt that she had potentially traumatized him and ruined his life. She told Raucher that she was glad he turned out all right, and that they had best not re-visit the past.<ref name=herm/><ref name=doug/>

In a 2002 Scripps Treasure Coast Publishing interview, Raucher lamented never hearing from her again and expressed his hope that she was still alive.<ref name=herm/> Raucher's novelization of the screenplay, with the dedication, "To those I love, past and present," serves more as the tribute to Seltzer that he had intended the film to be, with the focus of the book being more on the two boys' relationship than Raucher's relationship with Dorothy. Consequently, the book also mentions Seltzer's death, which is absent from the film adaptation.<ref name=so42>Raucher, Herman. Summer of '42. 23rd Edition. New York: Dell, 1974.</ref>

==Reception and awards==
The film became a blockbuster upon its release, grossing over $32 million, making it the [[1971 in film|sixth highest-grossing film of 1971]] and one of the most successful films in history, with an expense-to-profit ratio of 1:32;<ref name=lucia>[http://www.allmovie.com/work/summer-of-42-47639 Allmovie: Summer of '42] ''allmovie.com.'' Retrieved September 3, 2010.</ref> beyond that, it is estimated [[video rentals]] and purchases in the United States since the 1980s have produced an additional $20.5 million.<ref name=IMDb>[http://imdb.com/title/tt0067803/business Internet Movie Database: Summer of '42 Business Data] ''IMDb.com'' Retrieved July 3, 2006.</ref> On this point, Raucher said in May 2002 that his ten percent of the gross, in addition to royalties from book sales, "has paid bills ever since."<ref name=herm/>

As well as being a commercial success, ''Summer of '42'' also received rave critical reviews. It went on to be nominated for over a dozen awards, including [[Golden Globe Award]]s for [[Golden Globe Award for Best Motion Picture – Drama|"Best Motion Picture – Drama"]] and [[Golden Globe Award for Best Director – Motion Picture|"Best Director"]], and five [[Academy Award]] nominations for Best Original Music Score, Best Cinematography, Best Editing, Best Writing-Story and Best Screenplay.<ref>{{cite web|url=http://www.tcm.com/tcmdb/title/19262/Summer-of-42/misc-notes.html |title=Summer of '42 (1971) – Misc Notes |publisher=TCM.com |date= |accessdate=February 8, 2012}}</ref> Ultimately, the film won two awards: the [[44th Academy Awards|1972 44th Academy Awards]] Oscar for Original Dramatic Score, and the 1971 [[BAFTA Award for Best Film Music|BAFTA Anthony Asquith Award for Film Music]], both to Michel Legrand. It counted among its fans [[Stanley Kubrick]], who had the film play on a television in a scene in ''[[The Shining (film)|The Shining]]''.

The film is recognized by [[American Film Institute]] in these lists:
* 2002: [[AFI's 100 Years...100 Passions]] – Nominated<ref>{{cite web|url=http://www.afi.com/Docs/100Years/passions400.pdf |title=AFI's 100 Years...100 Passions Nominees |format=PDF |date= |accessdate=August 20, 2016}}</ref>
* 2005: [[AFI's 100 Years of Film Scores]] – Nominated<ref>{{cite web|url=http://www.afi.com/Docs/100Years/scores250.pdf |title= AFI's 100 Years of Film Scores Nominees |format=PDF |date= |accessdate=August 20, 2016}}</ref>

==Sequel==
In 1973, the film was [[Sequel|followed]] by ''[[Class of '44]]'', a [[Slice of Life Story|slice-of-life]] film made up of vignettes about Herman Raucher and Oscar Seltzer's experiences in college. ''Class of '44'' involves the boys facing army service in the last year of World War II. The only crew member from ''Summer of '42'' to return to the project was Raucher himself, who wrote the script; a new director and composer were brought in to replace Mulligan and Legrand. Of the principal four cast members of ''Summer of '42'', only Jerry Houser and Gary Grimes returned for prominent roles, with Oliver Conant making two brief appearances totaling less than two minutes of screen time. Jennifer O'Neill did not appear in the film at all, nor was the character of Dorothy mentioned.

The film is noted for featuring a young, slim [[John Candy]] briefly appearing in his first film role. The film met with poor critical reviews; the only three reviews available at [[Rotten Tomatoes]] are resoundingly negative,<ref>[http://www.rottentomatoes.com/m/class_of_44/ Rotten Tomatoes: Class of '44] ''rottentomatoes.com'' Retrieved August 11, 2006</ref><ref name=timeout>[http://www.timeout.com/film/69355.html Timeout Film Review: Class of '44] ''timeout.com'' Retrieved August 11, 2006,</ref> with Channel 4 calling it "a big disappointment,"<ref name=channel>[http://www.channel4.com/film/reviews/film.jsp?id=102128 Channel 4 Film Reviews: Class of '44] ''channel4.com'' Retrieved August 11, 2006</ref> and ''[[The New York Times]]'' stating "The only things worth attention in 'Class of '44 are the period details," and "'Class of '44' seems less like a movie than 95 minutes of animated wallpaper."<ref name=times>[http://movies2.nytimes.com/mem/movies/review.html?_r=1&title1=&title2=Class%20of%20%2744%20%28Movie%29&reviewer=VINCENT%20CANBY&v_id=9822&partner=Rotten%20Tomatoes&oref=slogin New York Times Film Review: Class of '44] ''nytimes.com'' Retrieved August 11, 2006</ref>

==Soundtrack==
{{Infobox album
| Name        = Summer of '42: Original Motion Picture Score
| Type        = Soundtrack
| Artist      = Michel Legrand
| Cover       = SO42ST.JPG
| Released    = 1971
| Label       = [[Warner Bros. Records]]
}}
The film's soundtrack consists almost entirely of compositions by [[Michel Legrand]], many of which are variants upon "The Summer Knows", the film's theme.  Lyrics are by [[Marilyn Bergman|Marilyn]] and [[Alan Bergman]].  Because the complete score runs just under 17 minutes, only the first and eighth tracks on the album are from ''Summer of '42''; the rest of the music is taken from Legrand's score for 1969's ''[[The Picasso Summer]]''.

In addition to Legrand's scoring, the film also features the song "Hold Tight" by [[The Andrews Sisters]] and the theme from ''[[Now, Voyager]]''.  On the Billboard 200, it debuted on 9/11/1971 and peaked at #52 on 11/20/1971.
{{Track listing
| collapsed       = no
| headline        = ''Summer of '42: Original Motion Picture Score''
| total_length    = 35:44
| title1          = Summer of '42 (Main Theme)
| length1         = 3:51
| title2          = Summer Song
| length2         = 4:21
| title3          = The Bacchanal
| length3         = 1:48
| title4          = Lonely Two
| length4         = 2:04
| title5          = The Danger
| length5         = 2:13
| title6          = Montage: But Not Picasso / Full Awakening
| length6         = 3:32
| title7          = High I.Q
| length7         = 2:11
| title8          = The Summer Knows
| length8         = 1:47
| title9          = The Entrance to Reality
| length9         = 3:04
| title10         = La Guerre
| length10        = 3:15
| title11         = Los Manos de Muerto
| length11        = 3:29
| title12         = Awakening Awareness
| length12        = 2:26
| title13         = And All the Time
| length13        = 1:43
}}

In 2014 [[Intrada Records]] released ''Summer of '42'' and ''The Picasso Summer'' on a limited-edition two-disc set, with the entire score for the former and the original album presentation of the latter (dubbed "The Picasso Suite") on disc 1, and the complete score for ''The Picasso Summer'' on disc 2.

Warner Bros. Publications released a sheet music folio, ''Summer of '42 & Other Hits of the Forties'', which contains the movie theme and 34 other unrelated songs.

==Cultural impact==

===Music===
Legrand's theme song for the film, "The Summer Knows", has since become a [[pop standard]], being recorded by such artists as [[Peter Nero]] (who had a charting hit with his 1971 version), [[Biddu]], [[Tony Bennett]], [[Frank Sinatra]], [[Sarah Vaughan]], [[Andy Williams]], [[Jonny Fair]], [[Scott Walker (singer)|Scott Walker]], [[Jackie Evancho]], [[Oscar Peterson]], [[Bill Evans]], [[Toots Thielemans]], and [[Barbra Streisand]].

The 1973 song "Summer (The First Time)" by [[Bobby Goldsboro]] has almost exactly the same subject and apparent setting, although there is no direct credited link. [[Bryan Adams]] has, however, credited the film as being a partial inspiration for his 1985 hit "[[Summer of '69]]".<ref name=jim>[http://www.jimvallance.com/01-music-folder/songs-folder-may-27/pg-song-adams-summer-of-69.html Summer of '69 lyrics explained by co-author] ''jimvallance.com'' Retrieved July 6, 2006.</ref>

===Film and television===
In [[Stanley Kubrick]]'s [[The Shining (film)|1980 film version]] of [[Stephen King]]'s ''[[The Shining (novel)|The Shining]]'', Wendy ([[Shelley Duvall]]) is shown watching ''Summer of '42'' on television (a brief clip of the scene featuring Hermie helping Dorothy bring her groceries in the house is playing on the television in the background during the scene).

An episode of the 1970s sitcom ''[[Happy Days]]'' was loosely based upon ''Summer of '42'', with Richie Cunningham befriending a Korean War widow.  The film's title formed the basis of a Season 7 episode of ''[[The Simpsons]]'', "[[Summer of 4 Ft. 2]]".

On the Happy Days 30th Anniversary Show it was actually a Drama called New Family In Town based on Summer Of '42 which was one of the most popular Movies at that time.

===Remakes===
In the years since the film's release, Warner Bros. has attempted to buy back Raucher's ten percent of the film as well as his rights to the story so it could be remade; Raucher has consistently declined.<ref name=herm/> The 1988 film ''[[Stealing Home]]'' has numerous similarities to both ''Summer of '42'' and ''Class of '44'', with several incidents (most notably a subplot dealing with the premature death of the protagonist's father and the protagonist's response to it) appearing to have been directly lifted from Raucher's own life; Jennifer O'Neill stated in 2002 she believes "''Home''" was an attempted remake of "''Summer''".<ref name=jenn2>[http://www.tv-now.com/intervus/oneill/index.html Jennifer O'Neill in 2002] ''tv-now.com'' Retrieved August 11, 2006 {{webarchive |url=https://web.archive.org/web/20060829114811/http://www.tv-now.com/intervus/oneill/index.html |date=August 29, 2006 }}</ref>

===Off-Broadway musical===
In 2001, Raucher consented to the film being made into an [[off Broadway]] musical play.<ref name=herm/> He was on hand opening night, giving the cast a pep-talk which he concluded, "We've now done it every possible way – except go out and piss it in the snow!"<ref name=piss>[http://www.theatermania.com/content/news.cfm/story/1848 Theatermania.com], Starry, Starry Morning: Charles Nelson's Casts and Forecasts. Retrieved September 3, 2007</ref> The play met with positive critical and fan response, and was endorsed by Raucher himself, but the play was forced to close down in the aftermath of the [[September 11, 2001 attacks|September 11 attacks]].<ref name=herm/> Nevertheless, the play was enough to spark interest in the film and book with a new generation, prompting Warner to re-issue the book (which had since gone out of print, along with all of Raucher's other works) for sale with [[Barnes & Noble]]'s online bookstore, and to restore the film and release it on DVD.<ref name=herm/> The musical has since been performed across the country, at venues such as Kalliope Stage in [[Cleveland Heights, Ohio]] in 2004 (directed by Paul Gurgol) and [[Mill Mountain Theatre]] in [[Roanoke, Virginia|Roanoke]], [[Virginia]], (directed by Jere Hodgin and choreographed by Bernard Monroe), and was subsequently recorded as a concert by the York Theatre Company in 2006.

===Alternate sequel===
In 2002, O'Neill claimed to have obtained the rights to make a sequel to ''Summer of '42'', based on a short story she wrote, which took place in an alternate reality where Herman Raucher had a son and divorced his wife, went back to Nantucket in 1962 with a still-living Oscar Seltzer, and encountered Dorothy again and married her.<ref name=TCPalm /> As of 2017, this project – which O'Neill had hoped to produce with [[Lifetime (TV network)|Lifetime]] television<ref name=usa>[http://www.usatoday.com/life/books/2002/2002-04-09-oneill.htm Summer of '42 Plus Twenty] usatoday.com. Retrieved July 6, 2006.</ref> – has not been realized, and it is unknown whether O'Neill is still attempting to get it produced, or if Raucher consented to its production.

==See also==
* [[List of American films of 1971]]

==References==
{{Reflist|30em}}

==External links==
* [http://www.hermanraucher.com www.hermanraucher.com]
* {{IMDb title|0067803|Summer of '42}}
* {{tcmdb title|19262|Summer of '42}}
* [http://www.thewordslinger.com/posts.php?id=82 ''Summer of '42''] by Andy Williamson – The Wordslinger dated April 14, 2008

{{Robert Mulligan}}
{{Featured article}}

{{DEFAULTSORT:Summer Of '42}}
[[Category:1970s comedy-drama films]]
[[Category:1970s coming-of-age films]]
[[Category:1970s teen films]]
[[Category:1971 films]]
[[Category:American biographical films]]
[[Category:American comedy-drama films]]
[[Category:American coming-of-age films]]
[[Category:American films]]
[[Category:English-language films]]
[[Category:Film scores by Michel Legrand]]
[[Category:Films about vacationing]]
[[Category:Films about virginity]]
[[Category:Films directed by Robert Mulligan]]
[[Category:Films set in 1942]]
[[Category:Films set in Massachusetts]]
[[Category:Films set on islands]]
[[Category:Films set on the home front during World War II]]
[[Category:Films that won the Best Original Score Academy Award]]
[[Category:Nantucket, Massachusetts]]
[[Category:Warner Bros. films]]