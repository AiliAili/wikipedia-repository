{{Infobox school
| name = Grey School of Wizardry (California-United States)
| seal_image = [[File:Grey_School_of_Wizardry_-_crest.jpg]]
| alt = 
| caption = 
| motto = {{Lang-la|Omnia vivunt, omnia inter se conexa}}
| motto_translation = Everything is alive; everything is interconnected
| location = [[Sonoma County, California]]
| country = USA
| coordinates = 
| opened = {{Start date|2004|08|01}}
| type = 
| founder = 
| headmaster = [[Oberon Zell-Ravenheart]]
| enrollment = 735<ref name=Myash2011/>
| enrollment_as_of = 2011
| lower_age = 11
| faculty = 16
| mascot =
| newspaper = Grey Matters
| houses = {{Bulleted list|Salamanders|Undines|Sylphs|Gnomes}}
| colors =
| website = {{Url|www.greyschool.com}}
| alumni = 
}}
The '''Grey School of Wizardry''' is a school specializing in [[occult]] [[Magic (paranormal)|magic]], operating primarily [[online]] and as a [[non-profit]] educational institution in the State of [[California]]. It was founded by present headmaster [[Oberon Zell-Ravenheart]], a founder of the [[Church of All Worlds]]. The school was reported to be "the first wizard school to be officially recognized as an academic establishment."<ref name=Myash2011>Myash, Jeff, (March 2, 2011), [http://www.dailymail.co.uk/news/article-1361852/Grey-School-Wizardry-Real-life-Dumbledore-opens-worlds-wizard-school.html "This spells trouble! Real-life Dumbledore opens world's first wizard school"], ''MailOnline'', Retrieved October 13, 2013.</ref> It is a [[secular]] institution, not a [[religion|religious]] one, and is not affiliated with any religion or religious organization. Over 450 classes are offered in 16 magical departments. Graduates are certified as “Journeyman Wizards”, using the traditional title ''[[journeyman]]'' signifying one who has completed an apprenticeship. 

== History ==
Before the school opened, the Grey Council was established in 2002 by [[Oberon Zell-Ravenheart]] as an advisory group to determine the curriculum.<ref>[http://caw.org/content/?q=oberon Oberon Zell], The Church of All Worlds. Retrieved on November 27, 2013.</ref> The Grey Council was composed of some two dozen authors, mystics, magicians and leaders of [[Modern paganism|neopagan]] communities around the world, including [[Raymond Buckland]], [[Raven Grimassi]], [[Patricia Telesco]], [[Frederic Lamond (Wiccan)|Frederic Lamond]], [[Morning Glory Zell-Ravenheart]], [[Donald Michael Kraig]], [[Katlyn Breene]], [[Robert Lee "Skip" Ellison]], [[Jesse Wolf Hardin]], [[Nicki Scully]], [[Sam Webster (writer)|Sam Webster]], [[Trina Robbins]], [[Ronald Hutton]], [[Amber K]], [[Ellen Evert Hopman]], [[Luc Sala]] and [[Jeff McBride]].<ref>Cusack, Carole M. (2010) “The Church of All Worlds and Pagan Ecotheology: Uncertain Boundaries and Unlimited Possibilities,” [http://www.basr.ac.uk/diskus/diskus11/cusack.htm ''DISKUS''], Vol 11.</ref><ref>[http://www.greyschool.com/index.php?module=Landing&func=display&page=council Grey School of Wizardry: Grey Council Membership]</ref> The Grey Council worked through 2003 to produce a textbook—a [[grimoire]]—for the school, the ''Grimoire for the Apprentice Wizard'' (2004). This was followed by the ''Companion for the Apprentice Wizard'' in 2006.<ref name="greyschool.com">[http://greyschool.com/index.php?module=Landing&func=display&page=school#about "About the School: The Grey School of Wizardry". Retrieved on November 27, 2013.]</ref>

The Grey School of Wizardry first opened on the [[pagan]] [[holiday]] of [[Lughnasadh]] on August 1, 2004. In 2011 it reported 735 students.<ref name=Myash2011/> The school's motto is: ''Omnia vivunt, omnia inter se conexa'' (“Everything is alive; everything is interconnected” — [[Cicero]]). It is a nonprofit educational institute for children 11–17 years of age, and also for adults of any age.<ref>Fleischmann, Tom (Winter, 2009), "On Alticorns," ''Indiana Review'', 31(2).</ref> The name of the school may derive from the colors associated with the [[Magician (fantasy)|wizards]] in the [[J. R. R. Tolkien]] classic ''[[Lord of the Rings]]'' and, in particular, with the appendix to the name of the protagonist [[Gandalf]] (the Grey).<ref>Cusack, Carole M. (2010) ''Invented Religions: Imagination, Fiction and Faith'' (Farnham, Surrey: Ashgate), 73-79.</ref> It was incorporated as a non-profit educational institution in California on March 14, 2004,<ref>Cusack, Carole M (2009) "Science Fiction as Scripture: Robert A. Heinlein's Stranger in a Strange Land and the Church of All Worlds," ''Literature and Aesthetics: The Journal of the Sydney Society of Literature and Aesthetics'', 19(2).</ref> and received a [[501(c)(3)]] tax exemption from the [[Internal Revenue Service]] on September 27, 2007.<ref>Knowles, George (November 29, 2009), "Oberon & Morning Glory Zell-Ravenheart," [http://www.prweb.com/releases/2007/10/prweb564025.htm "Controverscial.com"], Retrieved November 16, 2013.</ref>

== Curriculum ==
The school provides a seven-year apprenticeship curriculum in wizardry. Faculty and students represent a wide variety of faiths, including Pagan, [[Christian]], [[Jewish]], [[Buddhist]], [[Hindu]], and [[Muslim]].<ref name="greyschool.com">[http://greyschool.com/index.php?module=Landing&func=display&page=school#about "About the School: The Grey School of Wizardry", accessed February 20, 2013.]</ref>

The curriculum begins with simple lessons, and increases in complexity as students progress. More than 450 classes are offered in 16 color-coded departments. These are: Wizardry (indigo), [[Nature]] Studies (silver), Magickal Practice (gold), [[Psychic]] Arts (aqua), [[Healing]] (blue), Wortcunning/[[Herbalism]] (green), [[Divination]] (yellow), [[performance arts|Performance Magics]] (orange), [[Alchemy]] & Magickal Sciences (red), Lifeways (pink), Beast Mastery (brown), [[Cosmology]] (violet), Mathemagicks (clear), [[Ceremonial Magic]] (white), [[Folklore|Lore]] (grey), and [[Black Magic|Dark Arts]] (black). Although some classes address [[mythology]] and [[comparative religion]], the school's grimoire (textbook of magic), ''Companion for the Apprentice Wizard'', and the school's [[philosophy]] focus on magic rather than [[spirituality]].<ref name="greyschool.com"/>

The program was partially inspired by the fictitious "[[Hogwarts School of Witchcraft and Wizardry]]" from the ''[[Harry Potter]]'' novels by [[J. K. Rowling]]<ref name="Bonewits, Isaac 2005 p. 84">Bonewits, Isaac (2005) ''The Pagan Man: Priests, Warriors, Hunters, and Drummers'' (Citadel) ISBN 0-8065-2697-1, ISBN 978-0-8065-2697-3, p. 84</ref> and, like Hogwarts, the Grey School hosts four youth houses: [[Salamander (legendary creature)|Salamanders]], [[Undine (alchemy)|Undines]], [[Sylph]]s, and [[Gnome]]s, that are associated with the [[Classical element|Element]]s [[Fire (classical element)|Fire]], [[Water (classical element)|Water]], [[Air (classical element)|Air]], and [[Earth (classical element)|Earth]]. Likewise, there are four equivalent Lodges for adults: Flames, Waters, Winds, and Stones. Houses and Lodges are moderated by faculty Heads and student Prefects. Despite this resonance with a fictional school, the Grey School of Wizardry is "an entirely serious project"; it is an institution for educational enrichment with offerings for children, as well as ongoing educational opportunities for adults. The Grey School provides an interactive social environment, with clubs, merits, challenges, awards, a quarterly student-run school magazine (“Grey Matters”), several week-long summer camps (“Conclaves”) around the US and overseas, and a virtual school which offers home schooling and a G.E.D. program.<ref>Cusack, Carole M. (2010) ''Invented Religions: Imagination, Fiction and Faith'' (Farnham, Surrey: Ashgate), 75.</ref> The virtual school was founded primarily to provide an education stream for modern Pagans and their children that is Pagan-focused.<ref>Possamai, Adam (2012) ''Handbook of Hyper-Real Religions'', (Boston: Brill).</ref>

According to [[Isaac Bonewits]],<ref name=WVObit>{{cite web|url=http://www.witchvox.com/wren/wn_detail.html?id=22549|title=Isaac Bonewits (1949 - 2010) : A Tribute|accessdate=January 19, 2012|first=Peg|last=Aloi|work=Witchvox|date=August 12, 2010}}</ref> "The Grimoire collects in one book a library of wisdom about ceremonial native and Earth-centered magic, Paleo- and Neopagan religions, the obligations of the wise to protect the defenseless, great wizards and witches of the past and present, and more."<ref>Bonewits, Isaac (2005) ''The Pagan Man: Priests, Warriors, Hunters, and Drummers'' (Citadel) ISBN 0-8065-2697-1, ISBN 978-0-8065-2697-3, p. 84</ref> Bonewits also asserts that the school presents an opportunity for males who are unsatisfied by the teachings of the modern Wicca movement.<ref name="Bonewits, Isaac 2005 p. 84"/>

== References ==
{{reflist}}

== Further reading ==

===Books===
* Zell-Ravenheart, Oberon - ''Grimoire for the Apprentice Wizard''  (New Page Books, 2004)
* Bonewits, Isaac - ''The Pagan Man'' (Kensington Citadel Press, 2005) p.&nbsp;84
* Zell-Ravenheart, Oberon - ''Companion for the Apprentice Wizard'' (New Page Books, 2006)
* Adler, Margot - ''Drawing Down the Moon: Witches, Druids, Goddess-Worshippers, and Other Pagans in America Today'', (1979; revised and expanded 3rd edition Penguin Books, 2006) pp.&nbsp;330, 334
* Ash “LeopardDancer”, DeKirk - ''Dragonlore''  (New Page, 2006)
* Pesznecker, Susan “Moonwriter” - ''Gargoyles''  (New Page, 2006)
* Moonoak, Luke - ''Radiant Circles'' (The Solantis Institute, 2010)
* Cusack, Carole - ''Invented Religions'' (Ashgate, 2010)

===Magazines===
* Prosser, Lee - ''Ghostvillage.com review'' Fate Magazine (March 19, 2004)
* Seth, Shalini - ''The Grey School of Wizardry'' Emirates Today in the UAE (Nov. 18, 2005).
* Night Sky, Michael & Anne Newkirk Niven - ''Oberon Zell: A Wizard’s Vision''  (PanGaia Magazine #47) (Autumn 2007) pp.&nbsp;22–28

===Academic Papers===
* Cusack, Carole M., Associate Professor, BA (Hons), MEd, PhD, Editor, Journal of Religious History - ''The Church of All Worlds and Pagan Ecotheology: Uncertain Boundaries and Unlimited Possibilities'' Studies in Religion A20, University of Sydney NSW 2006

===Online Publications===
* Zell-Ravenheart, Oberon - ''Esoteric Education: Restoring the Wonder'' [http://www.OberonZell.com/OZEssay.html]
* Beattie, Bill - ''Interview with Oberon Zell-Ravenheart'' Witchcraft Magazine (Australia) (May 18, 2005) [http://www.witchcraftmagazine.com.au]
* O’Gaea, Ashleen - ''Interview with Oberon Zell-Ravenheart'' Tapestry (Litha and Lammas issues, 2005) [http://www.tawn.org/tapestry/tapestry.htm]
* Zell-Ravenheart, Oberon - ''The Grey School of Wizardry’s 1st Birthday'' Five Feathers Magazine (Sept. 2005) [https://groups.yahoo.com/group/5Feathers/files/Back-issues]
* Elson, Rebecca - ''Ten Questions with Oberon Zell-Ravenheart'' The Magical Buffet (issue 16) [http://www.themagicalbuffet.com/Issues/Vol03_Iss16/Article_109.html]
* Shadowlands, Harlequin - ''Interview with Oberon Zell-Ravenheart'' The Witches Codex, [http://www.witchescodex.net]
* Badb, Jillbe - ''Interview with a Living Pagan Icon'' The Druid’s Egg (Imbolg 2007) (Contributing Editor) [http://druidsegg.reformed-druids.org/newsimbolc07.htm]
* Pesznecker, Susan “Moonwriter” - ''Profile: The Grey School of Wizardry'' The Magical Buffet, March 2007. [http://themagicalbuffet.com/blog1/2007/03/31/profile-the-grey-school-of-wizardry]
* ''Online Wizardry Recognized with a 501(c)(3)'' - Alternative Approaches.com (October 29, 2007) [http://alternativeapproaches.com/pnuke1/Article3804.html]
* Wind, Mabyn ''An Interview with Oberon Zell-Ravenheart'' - Penton (Dec. 2007) [http://www.penton.co.za/interviews/oberon.htm]
* Sffarlenn, Laneth - ''Wizards of Old and New, the Grey School is Calling For You!''  The Witches’ Voice (February 11, 2008) [http://www.witchvox.com/va/dt_va.html?a=auvic&c=words&id=12285]
* Zaman, Natalie - ''We’re Off to Meet a Wizard: The Wonderful Oberon Zell!'' Broomstix (Beltane 2008) [http://www.broomstix.com]
* White, Peter M. - ''Oberon Zell-Ravenheart Interview'' The Witches’ Codex (Oct. 31, 2008) [http://www.witchescodex.net]
* Walmsley, Charlotte (at Cardiff University) - ''Welcome to the Grey School of Wizardry: Real Life Dumbledore's Harry Potter Haven'' The National Student (August 19th, 2013) [http://www.thenationalstudent.com/Features/2013-08-19/Real_life_Dumbledores_Harry_Potter_Haven.html]

==External links==
* [http://www.greyschool.com/ Grey School of Wizardry Official Website]

[[Category:Educational institutions established in 2004]]
[[Category:Online schools]]
[[Category:Magic organizations]]
[[Category:Schools in Sonoma County, California]]