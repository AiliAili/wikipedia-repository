{{advert|date=August 2013}}
{{Infobox company
| name             = Intelligent Energy
| logo             = 
File:Intelligent_Energy_Logo.jpg| caption          =
| type             = 
| traded_as        = 
| industry         = [[Fuel Cell]]s
| fate             = 
| predecessor      = Advanced Power Sources Ltd. (1995) Founders - Philip Mitchell, Paul Adcock, Jon Moore, Anthony Newbold| successor        = 
| foundation       = [[Loughborough]], United Kingdom ({{Start date|2001|}})
| founder          = Harry Bradbury
| location_city    = [[Loughborough]]
| location_country = United Kingdom
| locations        = <!-- Number of locations, stores, offices, etc. -->
| area_served      = Worldwide
| key_people       = {{unbulleted list|Martin Bloom, CEO | John Maguire, CFO }}
| products         = 
| production       = 
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|http://www.intelligent-energy.com}}
| footnotes        = 
| intl             =
| bodystyle        =
}}

'''Intelligent Energy''' is a power technology company that specialises in the development of [[Proton Exchange Membrane]] (PEM) fuel cells for application in the automotive, consumer electronics and stationary power markets. Based in the UK, the company also has offices in India, Japan, [[Singapore]], France and the United States and employs approximately 400 people.

'''Intelligent Energy Holdings plc,''' the group holding company, is listed on the London Stock Exchange (ticker symbol: IEH).

== History ==

The origins of Intelligent Energy began at [[Loughborough University]] in the UK during the late 1980s, when the University became one of Europe’s first research and development centres for proton exchange membrane (PEM) fuel cell technology. In 1995, the UK’s first kW-level PEM fuel cell stack was produced by the R&D team.  In June of that year, Advanced Power Sources (APS) Ltd was founded as a spin-out from Loughborough University by Paul Adcock, Phil Mitchell, Jon Moore and Anthony Newbold, and was the first company in the UK formed specifically to address the development and commercialisation of PEM fuel cells.

Founded by Harry Bradbury, Intelligent Energy was established in 2001, acquiring Advanced Power Sources Ltd, together with its personnel and fuel cell related intellectual property that originated from research conducted by both APS and Loughborough University into PEM fuel cell technology. This triggered investment and enabled the company to grow its business activities.<ref name="a">[http://www.cleantechinvestor.com/portal/mainmenucomp/companiesi/586-intelligent-energy-/11138-profile-intelligent-energy-published-in-cleantech-infocus-uk-innovation-in-fuel-cells-and-hydrogen.html Profile: Intelligent Energy - October 2012]. ''Cleantech Investor''. October 2012. Retrieved 18 June 2013.</ref>
In March 2005, it launched the [[ENV]], the world’s first purpose-built fuel cell motorbike which gained the company recognition as a Technology Pioneer by the [[World Economic Forum]] in 2006. The ENV incorporated the company’s air-cooled fuel cell technology hybridised with a battery pack to provide 6&nbsp;kW peak load to the motor to improve performance during spikes in power demand i.e. acceleration.<ref name="b">[http://www.greencarcongress.com/2005/03/intelligent_ene.html Intelligent Energy’s ENV Fuel Cell Motorcycle]. ''Green Car Congress''. 16 March 2005. Retrieved 18 June 2013.</ref>

In 2007, a partnership was announced with [[Suzuki Motor Corporation]] to develop hydrogen fuel cells for a range of vehicles.<ref name="d">[http://www.greencarcongress.com/2007/02/intelligent_ene.html Intelligent Energy and Suzuki to Partner on Hydrogen Fuel Cell Motorcycles]. ''Green Car Congress''. 8 February 2007. Retrieved 18 June 2013.</ref> In 2008, Intelligent Energy established the company, IE-CHP in a joint venture with [[SSE plc]], one of the UK’s largest energy businesses, to develop fuel cells and other technologies for [[Cogeneration|CHP]] (Combined Heat and Power) applications.<ref name="e">[http://www.energyefficiencynews.com/articles/i/3552/?cid=4&scid=21 Fuel cell combined heat and power venture secures £3.7 million]. Energy Efficiency News. 9 November 2010. Retrieved 18 June 2013.</ref> In the same year, Intelligent Energy also produced the power system for the first fuel cell powered manned flight in conjunction with [[Boeing]].<ref name="f">[http://www.sciencedirect.com/science/article/pii/S1464285908701469 Boeing fuel cell plane in manned aviation first]. Science Direct. April 2008. Retrieved 18 June 2013.</ref>
In 2010, its fuel-cell taxi received The Engineer Technology and Innovation Award.<ref name="ab">[http://www.theengineer.co.uk/awards/technology-and-innovation-award-winners-revealed/1006316.article Technology and Innovation award winners revealed].  The Engineer.  2010. Retrieved 19 June 2013.</ref>
In March 2011, the Suzuki Burgman fuel cell scooter, equipped with Intelligent Energy’s fuel cell system, became the first fuel cell vehicle to achieve European Whole Vehicle Type Approval.<ref name="g">[http://www.globalsuzuki.com/globalnews/2011/0309.html Suzuki’s Burgman Fuel-Cell Scooter becomes world’s first fuel cell vehicle to earn European Whole Vehicle Type Approval]. Global Suzuki. 9 March 2011. Retrieved 18 June 2013.</ref>

In 2012, SMILE FC System Corporation, a joint venture between Intelligent Energy and Suzuki Motor Corporation, was established to develop and manufacture air-cooled fuel cell systems for the automotive and a range of industry sectors. Based in Yokohama, Japan, the joint venture also included a non-exclusive license agreement giving Suzuki access to Intelligent Energy’s fuel cell technology for its next generation of environmentally friendly fuel cell vehicles.<ref name="h">[http://www.smmt.co.uk/2012/02/uk-based-company-intelligent-energy-and-partner-suzuki-unveil-joint-venture-to-develop-and-manufacture-new-fuel-cell-systems/ UK-based company Intelligent Energy and partner Suzuki unveil joint venture to develop and manufacture new fuel cell systems]. SMMT. 8 February 2012. Retrieved 18 June 2013.</ref>
During the same year, a fleet of fuel cell taxis incorporating Intelligent Energy’s technology was used during the [[2012 London Olympics]]. Part of the European Union-funded HyTEC (Hydrogen Technologies in European Cities) project launched in 2011, the taxis were used to transport VIP guests of the Mayor of London around the city.<ref name="i">[http://www.fuelcelltoday.com/news-events/news-archive/2012/october/hydrogen-fuel-cell-electric-taxis-drive-olympic-vips-2500-miles Hydrogen Fuel Cell Electric Taxis Drive Olympic VIPs 2500 Miles].  Fuel Cell Today. 1 October 2012. Retrieved 18 June 2013.</ref>
In 2013, SMILE FC Corporation announced that it had established a ready-to-scale production line for its fuel cell systems, utilising Intelligent Energy’s semi-automated production technology.<ref name="ow2">[http://electriccarsreport.com/2013/02/intelligent-energy-and-suzukiannounce-ready-to-scale-fuel-cell-production-line/ Intelligent Energy and Suzuki Announce Ready-to-Scale Fuel Cell Production Line]. Electric Cars Report. 21 February 2013. Retrieved 18 June 2013.</ref> IE-CHP also received CE certification for its first-generation product, a 10 kWe/12 kWth combined heat and power (CHP) fuel cell. The certification allows the product to be sold in the European Economic Area, confirming that the product satisfies all the EU regulatory and conformity assessment procedures covering the design, manufacture, and testing of the system.<ref name="ow">[http://www.fuelcelltoday.com/news-events/news-archive/2013/march/intelligent-energy-and-scottish-and-southern-energy-jv-ie-chp-receives-ce-certification-for-firstfuel-cell-product Intelligent Energy and Scottish and Southern Energy JV IE-CHP Receives CE Certification for First Fuel Cell Product] Fuel Cell Today. 20 March 2013. Retrieved 18 June 2013.</ref>

== Technology ==

The company has over 1,000 patents granted and over 1000 patents pending.

Its fuel-cell technology is divided into two platforms: air-cooled (AC) and evaporatively-cooled (EC). The air-cooled fuel cell systems use low-power fans to provide cooling and the oxidant supply for operation. Heat from the fuel cell stack is conducted to cooling plates and removed through airflow channels,  a simplified and cost-effective system for the power range from a few watts to several kilowatts. They  are used in a wide range of consumer electronic, stationary power and automotive applications for two-wheel and small car range extender applications.

Evaporatively-cooled (EC) fuel cell systems provide power generation from a few kilowatts up to 200&nbsp;kW. Efficient thermal management of the EC fuel cell stack reduces system complexity, mass and cost. These  systems are designed for high-volume, low-cost manufacturing, and use modular architecture that can be quickly modified to suit the application.

In March 2013, the company announced the highest power densities yet for its automotive fuel cell stacks, demonstrating continuous volumetric and gravimetric power densities of 3.7&nbsp;kW/L and 2.5&nbsp;kW/kg, respectively.<ref name="l">[http://www.businessgreen.com/bg/news/2262266/intelligent-energy-hails-fuel-cell-power-density-breakthrough Intelligent Energy hails fuel cell power density breakthrough]. ''Business Green''. 18 April 2013. Retrieved 18 June 2013.</ref>
In April 2013, Intelligent Energy and partners Dyson Technology, Ricardo and TRW Conekt announced that, as part of a project part-funded by the UK Government’s Technology Strategy Board, they had achieved an increase in system power density of more than 30% delivering an improvement in power output from 30&nbsp;kW to 40&nbsp;kW for the chosen test system without increasing system mass or size.<ref name="m">[http://www.renewableenergyfocus.com/view/31864/uk-consortium-delivers-enhanced-fuel-cell-systems-in-collaborative-project/ UK consortium delivers enhanced fuel cell systems in collaborative project] ''Renewable Energy Focus''. 18 April 2013. Retrieved 18 June 2013.</ref>

== Market sectors ==

=== Automotive ===

The firm's fuel cell stacks have been developed for  small and large cars, scooters and motorbikes. 
In 2010, the company was involved in the development of the report entitled “A portfolio of power-trains for Europe: a fact-based analysis. The role of Battery Electric Vehicles, Plug-In Hybrids and Fuel Cell Electric Vehicles”, produced by [[McKinsey & Company]] with input from car manufacturers, oil and gas suppliers, utilities and industrial gas companies, wind turbine and electrolyser companies as well as governmental and non-governmental organisations. The report concluded, amongst other findings, that fuel cell vehicles are technology ready, and cost competitive, and that decarbonisation targets for Europe are unlikely to be met without the introduction of fuel cell powertrains.<ref name="n">[http://www.fch-ju.eu/sites/default/files/documents/Power_trains_for_Europe.pdf/ A portfolio of power-trains for Europe: a fact-based analysis]. ''Fuel Cells and Hydrogen''. 9 November 2010. Retrieved 19 June 2013.</ref>

=== Consumer Electronics ===

The firm also develops power solutions for portable devices.

=== Distributed Power & Generation ===

The company’s fuel cell systems are   used to provide replacement and backup power in the telecoms sector as an alternative to diesel generators. The company is particularly active in the Indian telecommunications market requiring standby power requirements for the country’s 400,000 cell towers due to daily power outages.<ref name="o">[http://criticalpoweronline.com/news/112012-062/ Hydrogen fuel cells for telecoms: Clean power is calling]. ''Critical Power Online''. 15 November 2012. Retrieved 19 June 2013.</ref>

== Membership of industry consortia and trade associations ==

The company is a founding member of UKH<sub>2</sub> Mobility, a government and industry group aiming to accelerate the commercial roll out of hydrogen vehicles in 2014/15;<ref name="p">[http://www.lboro.ac.uk/service/publicity/news-releases/2012/08_UKH2-Mobility.html Intelligent Energy announces its support for UKH<sub>2</sub> Mobility]. ''Loughborough University''. 9 November 2010. Retrieved 19 June 2013.</ref>
It is also  an associate partner of H<sub>2</sub>Mobility, a joint initiative to  establish a nationwide network of hydrogen fuelling stations in Germany;<ref name="q">[http://www.now-gmbh.de/en/mobility/mobility-of-tomorrow/cars-buses-and-public-fuelling-stations/h2-mobility-development-of-a-network-of-hydrogen-refueling-stations-in-germany.html Industry initiative targets the development of a network of hydrogen refueling stations for fuel cell electric vehicles in Germany]. ''NOW GmbH''. Retrieved 19 June 2013.</ref> a member of New Energy World Industry Grouping (NEW-IG), a European industrial association working to accelerate the market deployment of fuel cells and hydrogen technologies;<ref name="r">[http://www.new-ig.eu/members Members List]. ''New Energy World''. Retrieved 19 June 2013.</ref>  a member of the Fuel Cell and Hydrogen Energy Association (FCHEA), the US-based trade association for the fuel cell and hydrogen energy industry, dedicated to the commercialisation of fuel cells and hydrogen energy technologies;<ref name="s">[http://www.fchea.org/index.php?id=3 Members List]. ''Fuel Cell& Hydrogen Energy Association''. Retrieved 19 June 2013.</ref> and  a member of the UK Hydrogen and Fuel Cell Association (UKHFCA).<ref name="t">[http://www.ukhfca.co.uk/members/ Members List]. ''UKHFCA''. Retrieved 19 June 2013.</ref>

== Recognition and awards ==

* 2005 – [[Tech Track 100|The Sunday Times Tech Track 100]].<ref name="u">[http://www.fasttrack.co.uk/fasttrack/leagues/dbDetails.asp?siteID=3&compID=1499&yr=2005 Tech Track Ranking]. ''Fast Track''. 2005. Retrieved 19 June 2013.</ref>
* 2005 – Popular Science: Best of What’s New 2005 Award.<ref name="v">[http://www.platinum.matthey.com/news-and-events/news-articles/2005/november/28th/top-award-for-fuel-cell-bike Top award for fuel cell bike]. ''Platinum Today''. 28 November 2005. Retrieved 19 June 2013.</ref>
* 2006 – World Economic Forum Technology Pioneer.<ref name="c">[http://www.greencarcongress.com/2005/12/world_economic.html World Economic Forum Announces 36 Technology Pioneers for 2006]. ''Green Car Congress''. 5 December 2005. Retrieved 18 June 2013.</ref>
* 2007 – Good Design Award, Transportation category.<ref name="x">[http://www.chi-athenaeum.org/gdesign/2007/transportation/theintelligententergyenvfuellcell.html Good Design Awards 2007 for Transportation]. ''Good Design Awards''. 2007. Retrieved 19 June 2013.</ref>
* 2008 – Rushlight Award, Hydrogen and Fuel Cells category.<ref name="y">[http://www.rushlightevents.com/rushlight-awards/background/roll-of-honour/ Roll of Honour]. ''Rushlight Awards''. 9 November 2010. Retrieved 19 June 2013.</ref>
* 2009 – [[European Business Awards]] “Ruban d’Honneur”.<ref name="z">[http://www.tmcnet.com/usubmit/2008/11/27/3818770.htm The European Business Awards: European Business Awards announce 2009 Ruban d'Honneur Winners]. ''Mobile Security Zone''. 27 November 2008. Retrieved 19 June 2013.</ref>
* 2010 – Rushlight Award, Powered Transport category.<ref name="aa">[http://www.cleantechinvestor.com/portal/cleantech-awards/8069-rushlight-awards-2010/ Rushlight Awards 2010]. ''Cleantech Investor''. January 2011. Retrieved 19 June 2013.</ref>
* 2010 – [[The Engineer (magazine)|The Engineer]] Technology and Innovation Energy Award.<ref name="ab">[http://www.theengineer.co.uk/awards/technology-and-innovation-award-winners-revealed/1006316.article Technology and Innovation award winners revealed].  ''The Engineer''.  2010. Retrieved 19 June 2013.</ref>
* 2010 – Tech Tour Award.<ref name="ac">[http://www.reuters.com/article/2010/04/28/idUS86945+28-Apr-2010+BW20100428 European Tech Tour Names UK and Ireland`s Most Promising High Growth Technology Companies; Selected Companies Stand to Benefit from $10 Billion in Investment Capital].  ''Reuters''. 28 April 2010. Retrieved 19 June 2013.</ref>
* 2011 – Sunday Times Tech Track 100.<ref name="ad">[http://www.fasttrack.co.uk/fasttrack/leagues/dbtechDetails.asp?siteID=3&compID=1499&yr=2011 Tech Track Ranking]. ''Fast Track''. 2011. Retrieved 19 June 2013.</ref>
* 2012 – Sunday Times Tech Track 100.<ref name="ae">[http://www.fasttrack.co.uk/fasttrack/leagues/dbtechDetails.asp?siteID=3&compID=1499&yr=2012 Tech Track Ranking]. ''Fast Track''. 2012. Retrieved 19 June 2013.</ref>
* 2013 – Leicester Mercury Business Awards winner of Company of the Year.<ref name="af">[http://www.thisisleicestershire.co.uk/Intelligent-Energy-Loughborough-named-Leicester/story-18491675-detail/story.html#axzz2RC7Wo9tm Intelligent Energy, of Loughborough, named Leicester Mercury Company of the Year]. ''Leicester Mercury''. 22 March 2013. Retrieved 19 June 2013.</ref>
* 2013 – Sunday Times Tech Track 100.<ref name="ag">[http://www.fasttrack.co.uk/fasttrack/leagues/dbtechDetails.asp?siteID=3&compID=1499&yr=2013]. ''Fast Track''. 2012. Retrieved 23 March 2016.</ref>
* 2013 - Deloitte Technology Fast500 EMEA 2013 <ref name="ah">[http://www2.deloitte.com/content/dam/Deloitte/global/Documents/Technology-Media-Telecommunications/dttl_TMT-Event-Fast-500-2013-winners-ranking.pdf]. ''Fast 500''. 2012. Retrieved 23 March 2016.</ref>
* 2014 - UKSPA Anniversary Awards <ref name="ai">[http://www.ukspa.org.uk/blog/14/07/ukspa-30-award-winners-announced]. ''UKSPA''. 2014. Retrieved 23 March 2016.</ref>
* 2015 - Edison Award, Gold <ref name="aj">[http://www.edisonawards.com/winners2015.php]. ''Edison''. 2015. Retrieved 23 March 2016.</ref>
* 2015 - BusinessGreen Technology Awards, Breakthrough of the Year and Technology of the Year <ref name="ak">[http://events.businessgreen.com/technologyawards/static/winners-2015]. ''BusinessGreen''. 2015. Retrieved 23 March 2016.</ref>

==See also==
{{Renewable energy sources}}
* [[Alternative Energy Index]]
* [[Electric vehicle]]
* [[List of renewable energy topics by country]]
* [[Renewable energy commercialization]]
{{Sustainability}}

== References ==

{{reflist}}

== External links ==

* {{official website|http://www.intelligent-energy.com}}

[[Category:Fuel cell manufacturers]]
[[Category:Power companies of England]]
[[Category:Energy]]