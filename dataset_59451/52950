{{Infobox journal
| title = Telos
| cover = [[File:TELOS153.gif]]
| abbreviation = Telos
| editor = [[Russell Berman]]
| frequency = Quarterly
| discipline = [[Politics]], [[philosophy]], [[critical theory]], [[culture]]
| publisher = Telos Press Publishing
| history = 1968-present
| impact = 0.065
| impact-year = 2013
| website = http://journal.telospress.com/
| link1 = http://journal.telospress.com/content/current
| link1-name = Online access
| link2 = http://journal.telospress.com/content/by/year
| link2-name = Online archive
| ISSN = 0090-6514
| OCLC = 1785433
| LCCN = 73641746
}}
'''''Telos''''' is a quarterly [[peer-reviewed]] [[academic journal]] established in May 1968 to provide the [[New Left]] with a coherent theoretical perspective. It sought to expand the [[Husserl]]ian diagnosis of "the crisis of European sciences" to prefigure a particular program of social reconstruction relevant for the United States. In order to avoid the high level of abstraction typical of Husserlian [[Phenomenology (philosophy)|phenomenology]], the journal began introducing the ideas of [[Western Marxism]] and of the [[critical theory]] of the [[Frankfurt School]].<ref>Gary Genosko, "The Arrival of Jean Baudrillard in English Translation: Mark Poster and Telos Press (Part I)," ''International Journal of Baudrillard Studies'', vol. 1, no. 2 (2004).</ref><ref>Timothy Luke, "The Trek with ''Telos'': A Remembrance of Paul Piccone (January 19, 1940—July 12, 2004)", ''Fast Capitalism'', vol. 1, no. 2 (2005).</ref><ref>Kenneth Anderson, [http://kennethandersonlawofwar.blogspot.com/2007/11/telos-critical-theory-journal-and-its.html "''Telos'', the critical theory journal and its blog,"] November 18, 2007.</ref>

With the disintegration of the New Left and the gradual integration of what remained of the [[American Left]] within the [[Democratic Party (United States)|Democratic Party]], ''Telos'' became increasingly critical of the Left in general. It subsequently undertook a reevaluation of 20th century [[intellectual history]], focusing primarily on forgotten and repressed authors and ideas, beginning with [[Carl Schmitt]] and American [[populism]]. Eventually the journal rejected the traditional divisions between [[left–right politics|Left and Right]] as a legitimating mechanism for [[new class]] domination and an occlusion of new, [[Post-Fordism|post-Fordist]] political conflicts. This led to a reevaluation of the primacy of culture and to efforts to understand the dynamics of cultural disintegration and reintegration as a precondition for the constitution of that [[autonomy|autonomous individuality]] critical theory had always identified as the [[telos (philosophy)|telos]] of Western civilization.<ref>[http://interactivist.autonomedia.org/node/3049 Danny Postel, "The metamorphosis of ''Telos''," ''In These Times'', April 21-30, 1991.]</ref><ref>Russell Jacoby, ''The Last Intellectuals: American Culture in the Age of Academe'' (New York: Basic Books, 1987): 151-52.</ref><ref>Jennifer M. Lehmann, ''Social Theory as Politics in Knowledge'' (New York: Emerald Group Publishing, 2005): 81-82.</ref>

The journal is published by Telos Press Publishing and the [[editor-in-chief]] is [[Russell Berman]] ([[Stanford University]]). It is affiliated with the [[Telos Institute]], which hosts annual conferences of which the proceedings are often published in ''Telos''.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]], [[Arts & Humanities Citation Index]], [[Current Contents]]/Social & Behavioral Sciences, and Current Contents/Arts & Humanities.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-03-09}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.065, ranking it 133rd out of 138 journals in the category "Sociology".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Sociology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science}}</ref>

==Telos Press Publishing==
Telos Press Publishing was founded by [[Paul Piccone]], the first editor-in-chief of ''Telos'', and is the publisher of both the journal ''Telos'' as well as a separate book line. It is based in [[Candor, New York]].

==References==
{{reflist|30em}}

==External links==
* {{Official website|http://journal.telospress.com/}}
* [http://www.telospress.com/ Telos Press Publishing]

[[Category:Philosophy journals]]
[[Category:Cultural journals]]
[[Category:Political science journals]]
[[Category:Book publishing companies of the United States]]
[[Category:Political book publishing companies]]
[[Category:Publications established in 1968]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1968]]
[[Category:1968 establishments in New York]]