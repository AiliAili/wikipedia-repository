{{Infobox pro wrestling championship
| championshipname = ACW Junior Heavyweight Championship
| image = 
| image_size = 
| caption = 
| currentholder =
| won =
| aired = 
| promotion = [[Assault Championship Wrestling]]
| brand = 
| created = February 23, 2002
| mostreigns = Jim Nastic (2 reigns)
| firstchamp = Tiger Mulligan
| longestreign = [[Eddie Edwards (wrestler)|Eddie Edwards]] (211 days)
| shortestreign = Jim Nastic (24 days)
| oldest = 
| youngest = 
| heaviest = 
| lightest = 
| pastnames = 
| titleretired = March 21, 2004
| pastlookimages = 
}}
The '''ACW Junior Heavyweight Championship''' was a secondary [[professional wrestling]] [[championship (professional wrestling)|championship title]] in the [[United States|American]] [[independent wrestling|independent]] [[professional wrestling promotion|promotion]] [[Assault Championship Wrestling]]. The first-ever champion was Tiger Mulligan who defeated Shockwav, and Frankie Starz in a [[Professional wrestling match types#Basic elimination matches|Four Corners match]] in [[Meriden, Connecticut]] on August 24, 2001. The championship was regularly defended throughout the state of [[Connecticut]], most often in [[Meriden, Connecticut]], but also in [[New Britain, Connecticut|New Britain]] and [[Waterbury, Connecticut]] until the promotion closed in early-2004.<ref name="Solie">{{cite web|url=http://www.solie.org/titlehistories/jhtacw.html|title=ACW Junior Heavyweight Title History|author=Tsakiries, Phil|year=2004|publisher=Solie's Title Histories}}</ref>

Jim Nastic holds the record for most reigns as a 2-time champion. At 211 days, [[Eddie Edwards (wrestler)|Eddie Edwards]]' reign is the longest in the title's history. Jim Nastic's first reign, which lasted only 24 days, was the shortest in the history of the title. Overall, there have been 8 reigns shared between 7 wrestlers, with one vacancy.<ref name="Solie"/>

==Title history==
{| class="wikitable"
|-
|'''#'''
|Order in reign history
|-
|'''Reign'''
|The reign number for the specific set of wrestlers listed
|-
|'''Event'''
|The event in which the title was won
|-
|&nbsp;—
|Used for vacated reigns so as not to count it as an official reign
|-
|N/A
|The information is not available or is unknown
|-
|'''+'''
|Indicates the current reign is changing daily
|}

===Reigns===
{| class="wikitable sortable" width=98% style="text-align:center;"
!style="background:#e3e3e3" width=0%| #
!style="background:#e3e3e3" width=20%|Wrestlers
!style="background:#e3e3e3" width=0%|Reign
!style="background:#e3e3e3" width=10%|Date
!style="background:#e3e3e3" width=0%|Days<br />held
!style="background:#e3e3e3" width=16%|Location
!style="background:#e3e3e3" width=18%|Event
!style="background:#e3e3e3" width=60% class="unsortable"|Notes
!style="background:#e3e3e3;" width=0% class="unsortable"|Ref.
|-
|{{sort|01|1}}
|{{sort|Mulligan|Tiger Mulligan}}
|{{sort|01|1}}
|{{dts|link=off|2002|2|23}}
|{{age in days nts|month1=2|day1=23|year1=2002|month2=3|day2=30|year2=2002}}
|{{sort|Waterbury|[[Waterbury, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Mulligan defeated Shockwave and Frankie Starz in a [[Professional wrestling match types#Basic elimination matches|four corners match]] to become the first ACW Junior Heavyweight Champion. 
|align="left"|<ref name="Solie"/>
|-
|{{sort|02|2}}
|{{sort|Charisma|[[Scotty Charisma]]}}
|{{sort|01|1}}
|{{dts|link=off|2002|3|30}}
|{{age in days nts|month1=3|day1=30|year1=2002|month2=7|day2=21|year2=2002}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|03|3}}
|{{sort|Nastic|Jim Nastic}}
|{{sort|01|1}}
|{{dts|link=off|2002|7|21}}
|{{age in days nts|month1=7|day1=21|year1=2002|month2=8|day2=14|year2=2002}}
|{{sort|Waterbury|[[Waterbury, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|04|4}}
|{{sort|Cruz|[[Chi Chi Cruz]]}}
|{{sort|01|1}}
|{{dts|link=off|2002|8|14}}
|N/A
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|4.5|—}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|—}}
|{{dts|link=off|2002|10}}
|{{sort|zz|—}}
|N/A
|N/A
|align="left"|The championship is vacated when Chi Chi Cruz is stripped of the title.
|align="left"|<ref name="Solie"/>
|-
|{{sort|05|5}}
|{{sort|Tsunami|Del Tsunami}}
|{{sort|01|1}}
|{{dts|link=off|2002|11|3}}
|{{age in days nts|month1=11|day1=3|year1=2002|month2=5|day2=11|year2=2003}}
|{{sort|New Britain|[[New Britain, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Defeated Jim Nastic, Abunai, and Scotty Charisma in a four corners match.
|align="left"|<ref name="Solie"/>
|-
|{{sort|06|6}}
|{{sort|Nastic|Jim Nastic}}
|{{sort|02|2}}
|{{dts|link=off|2003|5|11}}
|{{age in days nts|month1=5|day1=11|year1=2003|month2=7|day2=19|year2=2003}}
|{{sort|Waterbury|[[Waterbury, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/><ref>{{cite web |url=http://www.411mania.com/wrestling/news/19392 |title=411 Indies Update: BBQ, 3PW, D-Lo, Hamrick, Doink, CZW, Assault, More |author=Nason, Josh |date=May 6, 2003 |work=News |publisher=411mania.com |accessdate=26 July 2010}}</ref>
|-
|{{sort|07|7}}
|{{sort|Edwards|[[Eddie Edwards (wrestler)|Eddie Edwards]]}}
|{{sort|01|1}}
|{{dts|link=off|2003|7|19}}
|{{age in days nts|month1=7|day1=19|year1=2003|month2=2|day2=15|year2=2004}}
|{{sort|Waterbury|[[Waterbury, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|08|8}}
|{{sort|Abunai|Abunai}}
|{{sort|01|1}}
|{{dts|link=off|2004|2|15}}
|{{age in days nts|month1=2|day1=15|year1=2004|month2=3|day2=21|year2=2004}}
|{{sort|New Britain|[[New Britain, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/><ref>{{cite web |url=http://www.pwinsider.com/ViewArticle.php?id=247&p=5 |title=Indy Kingdom: Banderas, Invader, Teddy Hart, Sierra, Bigelow, Brooks, And More |author=McGrath, Jess |date=February 3, 2004 |work= |publisher=PWInsider.com |accessdate=26 July 2010}}</ref><ref>{{cite web |url=http://www.411mania.com/wrestling/news/21585 |title=411 Indies Update: Big Names Return In CA, Results |author=Levene, Lance |date=February 19, 2004 |work=News |publisher=411mania.com |accessdate=26 July 2010}}</ref>
|-
|{{sort|9.5|&mdash;}}
|{{sort|Deactivated|[[Glossary of professional wrestling terms#Vacated|Deactivated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|2004|3|21}}
|{{sort|zz|&mdash;}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|ACW holds its last show on March 21, 2004.
|align="left"|<ref name="Solie"/>
|}

==References==
<references/>

==External links==
*[http://web.archive.org/web/20031006063816/www.assaultwrestling.com/titlehistory.htm Official ACW Junior Heavyweight Championship Title History]
*[http://www.genickbruch.com/index.php?befehl=titles&kategorie=2&liga=11&titel=1839 ACW Junior Heavyweight Championship at Genickbruch.com]

[[Category:Cruiserweight wrestling championships]]