{{Infobox television episode
| title         = Black Tie
| series        = [[30 Rock]]
| image =
| caption = Liz and Jack catch sight of Jack's ex-wife at a party for Prince Gerhardt Hapsburg
| season        = 1
| episode       = 12
| airdate       = {{Start date|2007|02|01}}
| production    = 112
| writer        = [[Kay Cannon]]<br>[[Tina Fey]]
| director      = [[Don Scardino]]
| guests        =
* [[Kevin Brown (actor)|Kevin Brown]] as Walter "Dot Com" Slattery
* [[Grizz Chapman]] as Warren "Grizz" Griswold
* [[Will Forte]] as Tomas
* [[April Lee Hernández]] as Vicki
* [[Paul Reubens]] as Prince Gerhardt Hapsburg
* [[Isabella Rossellini]] as Bianca Donaghy
* [[Lonny Ross]] as Josh Girard
| episode_list  = [[30 Rock (season 1)|''30 Rock'' (season 1)]]<br>[[List of 30 Rock episodes|List of ''30 Rock'' episodes]]
| prev          = [[The Head and the Hair]]
| next          = [[Up All Night (30 Rock)|Up All Night]]
}}

"'''Black Tie'''" is the [[List of 30 Rock episodes|twelfth episode]] of the [[30 Rock (season 1)|first season]] of the American television comedy series ''[[30 Rock]]''. It was directed by [[Don Scardino]], and written by [[Kay Cannon]] and series creator [[Tina Fey]]. The episode originally aired on the [[NBC|National Broadcasting Company]] (NBC) in the United States on February 1, 2007. Guest stars in this episode include [[Kevin Brown (actor)|Kevin Brown]], [[Grizz Chapman]], [[Will Forte]], [[April Lee Hernández]], [[Paul Reubens]], and [[Isabella Rossellini]].

In the episode, [[Liz Lemon]] ([[Tina Fey]]) attends a foreign prince's (Reubens) birthday party with her boss [[Jack Donaghy]] ([[Alec Baldwin]]) and meets Jack's ex-wife (Rossellini). At the same time, [[Tracy Jordan]] ([[Tracy Morgan]]) tries to convince [[Pete Hornberger]] ([[Scott Adsit]]) to cheat on his wife at a wild party while [[NBC page]] [[Kenneth Parcell]] ([[Jack McBrayer]]) encourages him not to.

"Black Tie" received generally positive reception from television critics. According to the [[Nielsen ratings]] system, the episode was watched by 5.7&nbsp;million households during its original broadcast, and received a 2.9 rating/7 share among viewers in the 18–49 demographic.

==Plot==
[[Liz Lemon]] ([[Tina Fey]]) is invited by her boss, [[Jack Donaghy]] ([[Alec Baldwin]]), to accompany him to a birthday party for his friend, Prince Gerhardt [[House of Habsburg|Habsburg]] (Loosely based on the Habsburg monarch [[Charles II of Spain|Carlos II]]), in which she accepts. Liz fears the invitation is a date, but Jack denies this when asked. Later, Liz runs into her friend [[Jenna Maroney]] ([[Jane Krakowski]]) at the party. While the two talk, Prince Gerhardt ([[Paul Reubens]]) makes his entrance. He has a physical disability with many disfigurements and illnesses due to "centuries of [[inbreeding]]." Meanwhile, Jack is shocked to see his ex-wife Bianca ([[Isabella Rossellini]]) enter the room. When she comes over to say hello, Jack introduces Liz as his live-in girlfriend. She likes Liz and tells Jack not to let her get away.

At the same time, Prince Gerhardt spots Jenna and sends his messenger, Tomas ([[Will Forte]]), over to invite her to dine with him. Eager to become a modern-day [[Grace Kelly]], she talks herself into doing it, despite his problems. Later, Bianca tells Liz that she dislikes the idea that Liz can  make Jack truly happy. Liz tells Jack that Bianca is still not over him and to prove it, she goes to her and tells her the two are engaged. At hearing this, Bianca reacts violently and attacks Liz. Later, Prince Gerhardt decides he can now die happy, after he and Jenna discussed their relationship, and drinks some champagne, which he cannot digest. He dies because "he cannot metabolize the grapes." As a result, the party comes to a conclusion. Jack walks Liz up to her apartment. Before he leaves, he leans in to take off the necklace he lent her, which she mistakes for an attempted kiss. Jack acts semi-repulsed by the idea, and tells Liz to give it up.

Finally, at the 30 Rock studios, after overhearing [[Pete Hornberger]] ([[Scott Adsit]]) impersonating [[Elmo]] from ''[[Sesame Street]]'' while Pete was encouraging his son to do better with his potty training, [[Tracy Jordan]] ([[Tracy Morgan]]) compares Pete's relationship with his wife to that of [[Samson]] and [[Delilah]]. While working late at night in his office, Tracy enters with [[List of 30 Rock characters#Grizz Griswold|Grizz]] ([[Grizz Chapman]]) and [[List of 30 Rock characters#Dot Com Slattery|Dot Com]] ([[Kevin Brown (actor)|Kevin Brown]]) and a couple of other people, which turns into a party. Pete is still working, while Tracy's entourage enjoy themselves. Tracy encourages Pete to loosen up, and asks one of the women, Vicki ([[April Lee Hernández]]), to look after him. Pete finally loosens up and begins to give in to Vicki's temptations, but [[NBC page]] [[Kenneth Parcell]] ([[Jack McBrayer]]) sets him straight, telling him to recall the day that he got married.

==Production==
[[File:Kay Cannon.jpg|thumb|left|155px|Kay Cannon (''pictured'') wrote "Black Tie" along with co-writer Tina Fey.]]
"Black Tie" was directed by [[Don Scardino]], and written by [[Kay Cannon]] and series creator [[Tina Fey]].<ref>{{cite web|url=http://www.screenrush.co.uk/series/episodes_gen_csaison=2600&cserie=813.html|title=30 Rock: Episodes|publisher=Screenrush ([[AlloCiné]])|accessdate=2010-04-20|location=London}}</ref> This was Cannon and Fey's  first script collaboration, later co-writing the episodes "[[Somebody to Love (30 Rock)|Somebody to Love]]",<ref>{{cite web|url=http://www.screenrush.co.uk/series/episodes_gen_csaison=5594&cserie=813.html|title=30 Rock: Episodes|publisher=Screenrush ([[AlloCiné]])|accessdate=2010-04-20|location=London}}</ref> "[[Christmas Special (30 Rock)|Christmas Special]]",<ref>{{cite web|url=http://www.screenrush.co.uk/series/episodes_gen_csaison=6769&cserie=813.html|title=30 Rock: Episodes|publisher=Screenrush ([[AlloCiné]])|accessdate=2010-04-20|location=London}}</ref> and "[[Lee Marvin vs. Derek Jeter]]",<ref>{{cite web|url=https://tv.yahoo.com/30-rock/show/lee-marvin-vs-derek-jeter/episode/234634/castcrew;_ylt=AkyeTxhtPTOULbrAm3VFWNOxo9EF|title=30 Rock&nbsp;— Lee Marvin vs. Derek Jeter|accessdate=2010-04-29|publisher=[[Yahoo!]] TV}}</ref> for [[30 Rock (season 2)|season two]], [[30 Rock (season 3)|season three]], and [[30 Rock (season 4)|season four]], respectively. "Black Tie" originally aired on NBC in the United States on February 1, 2007, as the twelfth episode of the show's [[30 Rock (season 1)|first season]] and overall of the series.

This episode featured guest appearances by actors [[Paul Reubens]] and [[Isabella Rossellini]]. Fey wrote the role of Prince Gerhardt Hapsburg specifically for Reubens, and he did not have to audition.<ref>{{cite news|first=Sandy|last=Cohen|url=http://www.washingtonpost.com/wp-dyn/content/article/2007/06/19/AR2007061901103.html|title=Paul Reubens and Pee-Wee Herman Are Back|accessdate=2010-04-20|date=2007-06-19|agency=[[Associated Press]]|work=[[The Washington Post]]}}</ref> Fey, in regard to Reubens, said: "We were thrilled beyond belief to get him to do [the role]."<ref name="Commentary" /> Kevin Lasit, the show's prop master, built Gerhardt's wheelchair and puppet legs. Fey said that Lasit "really outdid himself", and noted that Reubens was kneeling in the wheelchair in the seat built over the top of him. In addition, Reubens wore prosthetic teeth and a wig to accommodate his character's look.<ref name="Commentary" /> According to Fey, in the DVD commentary for this episode, the show shot an alternate ending&mdash;at Reubens's request&mdash;in which Gerhardt does not die, "but ultimately it felt better stakes for the story if he'd die", in which [[Will Forte]]'s character says, "The Habsburg line has ended. You can pick up your gift bags at the coat check."<ref name="Commentary" /> In "Black Tie", Rosellini played Bianca, the ex-wife of Alec Baldwin's character, Jack Donaghy.<ref>{{cite news|first=Henry|last=Goldblatt|url=http://www.ew.com/ew/article/0,,20054076,00.html|title=30 Rock: Season 1 DVD Review|accessdate=2010-04-20|date=2007-08-31|work=[[Entertainment Weekly]]}}</ref> Rosellini would make a second appearance in the February 8, 2007, episode "[[Up All Night (30 Rock)|Up All Night]]".<ref>{{cite journal|first=Amy|last=Amatangelo|title=Television watch this|date=2007-02-04|work=[[Boston Herald]]}}</ref> Comedian actor Will Forte, who played Tomas, the messenger of Prince Gerhardt, has appeared in the main cast of ''[[Saturday Night Live]]'' (''SNL''),<ref name="tv guide"/> a weekly [[sketch comedy]] series which airs on NBC in the United States. Fey was the [[head writer]] on ''SNL'' from 1999 until 2006.<ref>{{cite news|first=Christopher|last=Goodwin|url=https://www.theguardian.com/lifeandstyle/2008/may/11/women.television|title=And funny with it|accessdate=2010-03-27|date=2008-05-11|work=[[The Guardian]]}}</ref>
Forte would later return to 30 Rock as Paul, a love interest of Jenna Maroney's.  

In the beginning of the episode, Liz and [[Josh Girard]] ([[Lonny Ross]]) are exchanging [[Mother insult|Yo Mamma Joke]]s. According to Fey, Liz's joke, "What's the difference between your mama and a washing machine? When I drop a load in the washing machine, it doesn't follow me around for a week", was "pretty dirty", and was surprised that the show was able to get away with the joke.<ref name="Commentary">{{cite video | people=Tina Fey|date=2007|title=30 Rock: Season 1: "Black Tie"|type = Audio commentary CD|publisher=[[Universal Studios Home Entertainment]]}}</ref> It has since been censored in syndication.
{{-}}

==Reception==
{{Quote box
 | quote  = "'Black Tie' was a great example of what makes ''30 Rock'' so darn enjoyable. It combined completely absurd, off-the-wall nonsense with real and believable human stories and had you constantly laughing as a result. Add to that two hilarious guest appearances and you're left with yet another fantastic episode".
 | source = '''Robert Canning,<br> [[IGN]]'''''<ref name="ign" />
 | align = right
| width = 195px
 }}
According to the [[Nielsen ratings]] system, "Black Tie" was watched by 5.7&nbsp;million households in its original American broadcast. It earned a 2.9 rating/7 share in the 18–49 demographic,<ref>{{cite web|url=http://www.thefutoncritic.com/news.aspx?id=20070206nbc01|title=NBC Ratings Results For The Week Of Jan. 29 - Feb.4|accessdate=2010-04-20|date=2007-02-06|publisher=The Futon Critic}}</ref> meaning that 2.9 percent of all people in that group, and 7 percent of all people from that group watching television at the time, watched the episode. This was an increase from the previous episode, "[[The Head and the Hair]]", which was watched by 5.0&nbsp;million American viewers.<ref>{{cite web|url=http://www.thefutoncritic.com/news.aspx?id=20070123nbc02|title=The Top-Rated 'Golden Globes' Telcast In Three Years Pace NBC's Week Of Jan. 15-21|accessdate=2010-04-20|date=2007-01-23|publisher=The Futon Critic}}</ref> Since airing, the episode has received generally positive reviews from television critics.

[[IGN]] contributor Robert Canning called it a "fantastic episode" and rated it 8 out of 10. He enjoyed all the subplots, and the performances by Paul Reubens and Isabella Rossellini.<ref name="ign">{{cite news|first=Robert|last=Canning|url=http://tv.ign.com/articles/760/760825p1.html|title=30 Rock: "Black Tie" Review|accessdate=2010-04-20|date=2007-02-02|publisher=[[IGN]]}}</ref> Julia Ward of [[AOL]]'s [[TV Squad]] wrote that Reubens and Rossellini were "pretty inspired choices", and said that "Tracy as the devil, Kenneth as the angel" gag&mdash;in which Tracy was encouraging Pete to cheat on his wife while Kenneth told him not to&mdash;was also "inspired".<ref>{{cite news|first=Julia|last=Ward|url=http://www.tvsquad.com/2007/02/02/30-rock-black-tie/|title=30 Rock: Black Tie|accessdate=2010-04-20|date=2007-02-02|publisher=[[TV Squad]]}}</ref> ''[[TV Guide]]''<nowiki>'</nowiki>s Matt Mitovich was favorable to the guest appearances of Reubens, Rossellini, and Will Forte, but said that Reubens's role "served up a memorable majesty on a very strong, very laugh-out-loud funny installment" of this episode. Mitovich was impressed with how "Black Tie" handled Liz's "date" with Jack, citing that lesser shows would have "gone there" in the final scene, "and really jacked up the 'tension' between its two leads, but Tina Fey and her crack writing staff didn't. They kept it 99.44 percent light, meaning we now won't&nbsp;... be wondering, 'What if...?'" in regard to the episode itself, Mitovich said it was an "awesome episode, an immeasurably enjoyable, laugh-inducing series."<ref name="tv guide">{{cite news|first=Matt|last=Mitovich|url=http://www.tvguide.com/episode-recaps/30-Rock/February-1-2007-5916.aspx|title=February 1, 2007: "It Feels Good to Laugh"|accessdate=2010-04-20|date=2007-02-01|work=[[TV Guide]]}}</ref> ''[[Seattle Post-Intelligencer]]'' contributor Melanie McFarland said that Reubens was "perfectly cast as such a freak", though in regard to Rossellini's appearance "[s]he glides in playing the class act we're used to, but when Rossellini dissolves into a shocking mess, she yields one of the series' most absurd scenes yet."<ref>{{cite journal|first=Melanie|last=McFarland|title=The Real Joke Is On Comics' Critic|date=2007-02-01|work=[[Seattle Post-Intelligencer]]|page=C1}}</ref> It was rated the 28th greatest TV episode of all time on TV Guide's list.

==References==
{{reflist|30em}}

==External links==
* {{IMDb episode|0909393}}
* {{TV.com episode|30-rock/black-tie-937428}}

{{30 Rock Season One}}
{{30 Rock}}

{{DISPLAYTITLE:Black Tie (''30 Rock'')}}
{{good article}}
[[Category:2007 television episodes]]
[[Category:30 Rock (season 1) episodes]]