{{Use dmy dates|date=September 2010}}
{{Infobox military person
|name= Sir Murray Maxwell
|birth_date=10 September 1775
|death_date= {{death date and age|df=y|1831|6|26|1775|9|10}}
|birth_place= [[Wigtownshire]], [[Scotland]]
|death_place= [[Lincoln's Inn Fields]], [[London]], [[England]]
|image=Sir Murray Maxwell.jpg
|caption=Captain Murray Maxwell, 1817
|nickname=
|allegiance= {{flagicon|United Kingdom}} United Kingdom
|serviceyears= 1790&ndash;1831
|rank= [[Captain (Royal Navy)|Captain]]
|branch= [[File:Naval Ensign of the United Kingdom.svg|22px|link=]] [[Royal Navy]]
|commands=
|unit=
|battles= [[French Revolutionary Wars]]<br/>• [[Siege of Toulon]]<br/>[[Napoleonic Wars]]<br/>[[Action of 4 April 1808]]<br/>[[Adriatic campaign of 1807–1814|Adriatic campaign]]<br/>• [[Action of 29 November 1811]]
|awards= [[Knight Bachelor]]<br/>[[Companion of the Order of the Bath]]
|laterwork=
}}

[[Captain (Royal Navy)|Captain]] '''Sir Murray Maxwell''', [[Companion of the Order of the Bath|CB]], [[Fellow of the Royal Society|FRS]] (10 September 1775 &ndash; 26 June 1831)<ref>There is some confusion over the exact date of Maxwell's death. The ''[[Oxford Dictionary of National Biography]]'' states that he died on 26 June, while ''Fraser's Magazine'' and the ''United Services Magazine'' claim that he died on 19 June. Since all sources agree on the circumstances of his death, there is no accounting for the difference in date.</ref> was a British [[Royal Navy]] officer who served with distinction in the late eighteenth and early nineteenth centuries, particularly during the [[French Revolutionary Wars|French Revolutionary]] and [[Napoleonic Wars]]. Maxwell first gained recognition as one of the British captains involved in the successful [[Adriatic campaign of 1807–1814]], during which he was responsible for the destruction of a French armaments convoy at the [[Action of 29 November 1811]]. As a result of further success in the Mediterranean, Maxwell was given increasingly important commissions and, despite the loss of his ship {{HMS|Daedalus|1811|6}} off [[Ceylon]] in 1813, was appointed to escort the British Ambassador to China in 1816.

The voyage to China subsequently became famous when Maxwell's ship {{HMS|Alceste|1806|6}} was wrecked in the [[Gaspar Strait]], and he and his crew became stranded on a nearby island.  The shipwrecked sailors ran short of food and were repeatedly attacked by [[Malays (ethnic group)|Malay]] pirates, but thanks to Maxwell's leadership no lives were lost. Eventually rescued by a British [[East India Company]] ship, the party returned to Britain as popular heroes, Maxwell being especially commended. He was [[Knight Bachelor|knighted]] for his services, and made a brief and unsuccessful foray into politics before resuming his naval career. In 1831 Maxwell was appointed [[Lieutenant-Governors of Prince Edward Island|Lieutenant Governor]] of [[Prince Edward Island]], but fell ill and died before he could take up the post.

==Early career==
Murray Maxwell was born in 1775 to James and Elizabeth Maxwell; his father was a [[British Army]] officer with the [[42nd Regiment of Foot]] (known as the "Black Watch") and the son of [[Alexander Maxwell|Sir Alexander Maxwell]], second of the [[Maxwell Baronets]] of Monreith.<ref name="ODNB">[http://www.oxforddnb.com/view/article/18408 Maxwell, Sir Murray], ''[[Oxford Dictionary of National Biography]]'', [[J. K. Laughton]], (subscription required), Retrieved 25 July 2008</ref>  The family lived in Penninghame in [[Wigtownshire]], Scotland, and Murray was intended for the armed forces from an early age: six of Murray's eight brothers would also join the Army or Navy.<ref name="ABO220">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 220</ref> In 1790, at the age of 14, he was sent to sea on board {{HMS|Juno|1780|6}}, then commanded by [[Sir Samuel Hood, 1st Baronet|Samuel Hood]]. He had been in ''Juno'' for three years when the [[French Revolutionary Wars]] broke out, and was on board when the [[frigate]] was forced to make a desperate escape from [[Toulon]] harbour under heavy fire from French Republican batteries during the [[Siege of Toulon|siege of the city]].<ref name="USM531">''United Services Magazine'', 1831 Part II, p. 531</ref> Later that year, he was engaged in the invasion of [[Corsica]] and the siege of [[Bastia]], during which he made such a favourable impression that when Hood transferred to {{HMS|Aigle|1782|6}} in 1794, he requested that Maxwell accompany him. Maxwell was transferred again during 1794, this time to the small frigate {{HMS|Nemesis|1780|6}} under the command of Hood's relative Captain [[Samuel Hood Linzee]].<ref name="USM531"/>

In December 1795 Maxwell was taken prisoner when ''Nemesis'' was captured by a superior French force in [[Smyrna]] harbour. Despite Smyrna's neutrality, the large French frigate [[French frigate Sensible (1788)|''Sensible'']] and the smaller corvette [[French corvette Sardine (1771)|''Sardine'']] entered the port, followed later by the corvette ''Rossignol'', and called on ''Nemesis'' to surrender. Linzee protested at the illegal nature of the French demands, but decided it would be futile to engage the significantly stronger force inside a neutral harbour, and complied with the French order.<ref name="WJ1:275">James, Vol. 1, p. 275</ref> Maxwell was rapidly [[prisoner exchange|exchanged]], and returned to service aboard {{HMS|Hussar|1784|6}} under Captain [[James Colnett]]. However, on 27 December 1796, ''Hussar'' was wrecked off Southern France, and Maxwell once again became a prisoner of war.<ref name="WJ2:378">James, Vol. 2, p. 378</ref> Exchanged a second time, he joined {{HMS|Blenheim|1761|6}}, and later moved to {{HMS|Princess Royal|1773|6}}, before being made lieutenant in October 1796. Following his promotion, Maxwell was not employed at sea again until 1802. In 1798 he married the daughter of an army officer, Grace Callander Waugh.<ref name="ODNB"/>

== Napoleonic Wars ==
At the conclusion of the [[Peace of Amiens]] and the start of the [[Napoleonic Wars]], Maxwell returned to sea-service in command of the [[sloop-of-war]] {{HMS|Cyane|1796|6}}.  Within days of the start of the war, ''Cyane'' captured two French transports destined for the Caribbean, and later served in the [[West Indies]], on one occasion exchanging fire with two large French frigates off [[Martinique]].<ref name="USM531"/> In 1803, Maxwell was involved in the capture of [[St Lucia]], for which he was made captain of the [[ship of the line]] {{HMS|Centaur|1797|6}}&mdash;the flagship of his former commander Sir Samuel Hood. In this ship Maxwell participated in the capture of the French and Dutch colonies of [[Tobago]], [[Demerera]] and [[Essequibo (colony)|Essequibo]] in 1803, following which his promotion to [[post captain]] was confirmed.<ref name="ODNB"/> He also blockaded Martinique, and was subsequently involved in the operation to seize [[Diamond Rock]], overseeing the construction of a gun battery on its summit.<ref name="WLC:5:333">Clowes, Vol. 5, p. 333</ref> This fortified position was able to severely restrict French shipping entering or leaving [[Fort-de-France]].<ref name="USM531"/> Present at the capture of [[Surinam]] and [[Berbice]] in 1804, Maxwell was the senior naval officer at the surrender of Surinam by its Dutch governor.<ref>{{London Gazette|issue=15712|startpage=758|endpage=759|date=19 June 1804|accessdate=2008-07-25}}</ref> His actions at Surinam: commanding the naval forces in the siege and capturing a succession of Dutch forts along the [[Suriname River]], were highly commended.<ref name="WLC:5:83">Clowes, Vol. 5, p. 83</ref> Maxwell's decisive leadership was essential in the rapid movement of troops by water to prevent the Dutch preparing fresh defensive positions; the colony surrendered after the British reached [[Paramaribo]], giving up 2,000 prisoners, several ships, large quantities of supplies and the colony itself, with its valuable plantations. British losses numbered less than 30.<ref name="ABO220-224">''Annual Biography and Obituary'', 1832 Vol. XVI, pp. 220&ndash;224</ref>

===Mediterranean service===
In 1805 Maxwell took command of the frigate {{HMS|Galatea|1794|6}} off [[Jamaica]], participating in the [[Atlantic campaign of 1806]] as part of the squadron under Rear-Admiral [[Alexander Cochrane|Sir Alexander Cochrane]] that drove off a French attack on the Jamaica convoy near [[Tortola]] on 4 July 1806.<ref name="WJ4:204">James, Vol. 4, p. 204</ref> In 1807, Maxwell was transferred to the Mediterranean in {{HMS|Alceste|1806|6}}. He was initially part of a raiding squadron that attacked coastal batteries and positions along the Spanish coast in support of the [[Peninsular War]]. In April 1808, shortly before Spain became an ally of Britain, he successfully destroyed a Spanish convoy carrying military stores off [[Rota, Spain|Rota]].<ref name="USM532">''United Services Magazine'', 1831 Part II, p. 532</ref><ref>{{London Gazette|issue=16139|startpage=570|endpage=571|date=23 April 1808|accessdate=2008-07-25}}</ref> Over the next two years Maxwell became an expert at raiding the French, Italian and Spanish coasts, destroying numerous Italian [[Martello Towers]] and small armed vessels.<ref name="WLC:5:278">Clowes, Vol. 5, p. 278</ref><ref name="ABO226">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 226</ref> In May 1810 he was commended for a raid at [[Frejus]], where he led a landing party that stormed and destroyed a coastal fort and seized a coastal convoy.<ref>{{London Gazette|issue=16392|startpage=1137|date=31 July 1810|accessdate=2008-07-25}}</ref>

===Adriatic campaign===
[[File:La Pomone contre les fregates Alceste et Active.jpg|thumb|alt=A naval painting in which a badly damaged ship in the foreground is flanked by two lightly damaged ships that are firing on the central vessel.|''La ''Pomone'' contre les frégates HMS ''Alceste'' et ''Active''<nowiki />'', by [[Pierre Julien Gilbert]]]]
Maxwell's most notable service came during the [[Adriatic campaign of 1807–1814]]. ''Alceste'' was despatched to the [[Adriatic Sea]] to support [[James Brisbane]] in the absence of [[William Hoste]], who had been wounded at the [[Battle of Lissa (1811)|Battle of Lissa]] in March 1811. On 4 May, Maxwell and Brisbane led an attack on [[Poreč|Parenza]], where a brig carrying supplies to [[Dubrovnik|Ragusa]] had taken shelter.<ref name="WLC:5:484">Clowes, Vol. 5, p. 484</ref> Seizing an island at the mouth of the harbour, the British established a [[mortar (weapon)|mortar]] position overlooking the anchorage, and sank the brig with a heavy bombardment.<ref name="WJ5:364">James, Vol. 5, p. 364</ref> In November 1811, with the temporary absence of Brisbane, Maxwell became the senior officer in the Adriatic. Seven months later, a convoy of French frigates carrying [[cannon]] from [[Corfu]] to [[Trieste]] was spotted attempting to slip past his base of operations on the island of [[Vis (island)|Lissa]]. Ashore in [[Vis (town)|Port St. George]], Maxwell was informed by [[telegraph]], and led ''Alceste'' and the rest of his squadron&mdash;{{HMS|Active|1799|6}} and {{HMS|Unite|1803|6}}&mdash;in pursuit.<ref name="WJ5:375">James, Vol. 5, p. 375</ref>

On 29 November, after a night's chase, the British caught their opponents near [[Palagruža|Pelagosa]]. The French force consisted of the large frigates [[French frigate Pauline (1807)|''Pauline'']] and [[French frigate Pomone (1805)|''Pomone'']], and the armed storeship [[French ship Persanne (1809)|''Persanne'']].<ref name="WJ5:378">James, Vol. 5, p. 378</ref> In the [[action of 29 November 1811|battle that followed]], ''Unite'' pursued and, after a lengthy chase, seized the smaller ''Persanne'', while Maxwell and [[James Alexander Gordon]] in ''Active'' engaged the frigates.<ref name="WLC:5:496">Clowes, Vol. 5, p. 496</ref> The action was bitterly contested, the British taking 61 casualties, including Gordon who lost a leg. However, ''Alceste'' and ''Active'' successfully isolated ''Pomone'', and when another British ship, {{HMS|Kingfisher|1804|6}}, appeared in the distance, ''Pauline'' fled.<ref name="RG178">Gardiner, p. 178</ref> Alone and having lost heavily, ''Pomone'' surrendered. The prizes were later sold along with their cargo of 200 cannon. Maxwell, despite attributing most of the credit for the victory to the wounded Gordon, was rewarded in 1812 with command of {{HMS|Daedalus|1811|6}}, a former [[Kingdom of Italy (Napoleonic)|Italian]] frigate captured at the Battle of Lissa.<ref name="JH152">Henderson, p. 152</ref>

===HMS ''Daedalus''===
Maxwell commanded ''Daedalus'' for less than a year. On 2 July 1813 the frigate ran aground on a shoal off [[Galle]], [[Ceylon]], causing serious damage to her keel. Although she was soon brought off, the leaks created in the grounding became so severe that Maxwell had no option but to order his crew to cease their desperate attempts to keep her afloat and abandon ship. He was the last to leave and shortly after he had been transported to a nearby [[East Indiaman]], ''Daedalus'' rolled over and sank.<ref>Grocott, p. 357</ref> Maxwell returned to Britain to face a [[court martial]] but was exonerated for the frigate's loss and reappointed to ''Alceste''. In 1815 he was made a [[Companion of the Order of the Bath]] for his naval service,<ref>{{London Gazette|issue=17061|startpage=1877|date=16 September 1815|accessdate=2008-07-25}}</ref> and although the war against France had ended, was retained for active duty at the special request of [[William Amherst, 1st Earl Amherst|Lord Amherst]].<ref name="ODNB"/><ref name="ABO229">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 229</ref>

==Voyage to China and shipwreck==
[[File:Sir Thomas Lawrence - Lord Amherst - Google Art Project.jpg|thumb|upright|[[William Amherst, 1st Earl Amherst|Lord Amherst]]|alt= An engraving of a wealthy young man wearing the robes of a member of the House of Lords]]
[[File:Alceste at Bogue.jpg|thumb|''Alceste'' firing at the Chinese forts in the [[Bocca Tigris]]]]

In 1816 Maxwell was ordered to escort Lord Amherst on a diplomatic mission to the [[Jiaqing Emperor]] of China. ''Alceste'' was accompanied by the small sloop HMS ''Lyra'', under Captain [[Basil Hall]], and the [[East Indiaman]] [[General Hewett (1811 ship)|''General Hewitt'']], which carried gifts for the Emperor. The small convoy called at [[Madeira]], [[Rio de Janeiro]], [[Cape Town]], [[Anyer|Anjere]] and [[Djakarta|Batavia]], and arrived at [[Hai River|Peiho]] after nearly six months at sea in July.<ref name="ABO229"/> Amherst went ashore with his party, instructing Maxwell to meet him at [[Guangzhou|Canton]] once his diplomatic mission was complete. The mission was expected to last several months, so Maxwell and Hall agreed to use the time to become the first British sailors to explore the [[Yellow Sea]] and beyond. Between them, ''Lyra'' and ''Alceste'' visited the [[Bohai Sea|Gulf of Pecheli]], the West coast of [[Korea]] and the [[Ryukyu Islands|"Loo-Choo" (Ryukyu) Islands]]&mdash;in some cases as the first European ships known to have sailed these waters.<ref name="ABO230">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 230</ref> During the journey, Maxwell saw the [[Great Wall of China]] and discovered serious inaccuracies in the charts of Western Korea, finding it lay 130&nbsp;miles east of its supposed position.<ref name="ABO231">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 231</ref> The expedition also made the first known British government contacts with both the Koreans and the Ryukyu Islanders, who ignored instructions from Chinese officials not to communicate with the British ships.<ref name="ABO230"/>

Maxwell arrived off the [[Pearl River (China)|Pearl River]] in November 1816 and prepared to sail to [[Huangpu District, Guangzhou|Whampoa]] for his reunion with Amherst. Amherst's mission had foundered on the British party's refusal to [[kowtow]] to the Jiaqing Emperor and offer him tribute as overlord, and Amherst and his retinue had to retire to Whampoa with their mission incomplete. At the mouth of the Pearl, ''Alceste'' was refused permission to enter the river and perfunctorily ordered to halt by a local [[mandarin (bureaucrat)|mandarin]], who threatened to sink the frigate if it tried to force passage. Responding angrily that he would pass the river with or without the mandarin's permission, Maxwell attacked the Chinese defences, breaking through a blockade of [[junk (ship)|junks]] and firing on the forts guarding the river mouth, scattering their defenders. He sailed on to Whampoa without further impediment, without casualties; Chinese losses were reportedly 47 killed and many wounded.<ref name="ABO233">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 233</ref> Maxwell himself had fired the first cannon as a statement that he took personal responsibility for the exchange of fire&mdash;reportedly, the first [[Round shot|cannonball]] was ironically marked "Tribute from the King of England to the Chinese".<ref name="USM533">''United Services Magazine'', 1831 Part II, p. 533</ref> Collecting Amherst and his party from Whampoa, Maxwell sailed back down the Pearl River and, in January 1817, began the return journey to Britain, visiting [[Macao]] and [[Manila]].<ref name="USM533"/>

On 18 February 1817 ''Alceste'' entered the [[Gaspar Strait]] between [[Bangka Island|Bangka]] and [[Liat Island|Liat]], traversing largely uncharted waters. Some hours later the frigate struck a hidden reef and grounded, sustaining severe damage to her hull. Despite Maxwell's best efforts to free his ship the carpenter reported that ''Alceste'' was taking on water and would rapidly sink if refloated.<ref name="JH154">Henderson, p. 154</ref> Ordering the ship to be abandoned, Maxwell gave the ship's barge to the ambassador and supervised the construction of a raft which, with the remaining boats, safely convoyed the crew, passengers and a quantity of supplies to a nearby island, formed largely of impenetrable [[mangrove]] swamps.<ref name="ABO235">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 235</ref> The last to leave ''Alceste'', Maxwell arrived on shore on the morning of 19 February. A council of officers subsequently decided that Amherst would take the ship's boats and 50 men and attempt to reach [[Djakarta|Batavia]], four days sail away. It was essential that Amherst reach Batavia quickly, as the supplies salvaged from the wreck, especially of drinkable water, would only last a few days shared among all 250 survivors.<ref name="ABO235"/>

===Attack by Dayaks===
To keep up morale following Amherst's departure, Maxwell began organising his remaining 200 men (and one woman) to secure their position and gather supplies. The men were divided into parties, with one ordered to dig a well while another returned to the wreck of ''Alceste'' to salvage what weapons and equipment they could.<ref name="ABO237">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 237</ref> A third party was ordered to clear a path to the island's central hill, where a cool cave could be used as a larder and trees felled to form a protective stockade. By the end of the first day the well was producing a steady supply of water.<ref name="ABO237"/>

The party aboard ''Alceste'', having determined that the ship was in no immediate danger of sinking, decided to remain aboard overnight. However, at dawn they awoke to discover the ship surrounded by [[Dayak people|Dayak]] (or [[Malays (ethnic group)|Malay]]) [[proas]] armed with [[swivel guns]]. The party escaped on the raft, only reaching the island ahead of the pursuing proas with the assistance of boats sent to meet them carrying armed [[Royal Marines]].<ref name="ABO239">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 239</ref> With the wreck vacated, the Dayaks began enthusiastically looting it and several proas approached the island, landing their crews on offshore rocks to both observe the British and store their salvage.<ref name="FM563">''Fraser's Magazine'', 1843 Vol. XXVII, p. 563</ref> Maxwell hastily organised defensive positions in case the Dayaks attacked the island, completing the stockade on the island's hill and preparing sharpened stakes and hundreds of improvised [[Cartridge (firearms)|cartridges]] for the group's 30 muskets.<ref name="JH156">Henderson, p. 156</ref> Over the next few days the proas approached the island several times, but despite attempts by the British to communicate with them, never landed. Eventually, on 22 February, Maxwell took advantage of the divided Dayak positions to drive their observers off the rocks, with the intention of recapturing the wreck.<ref name="ABO241">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 241</ref> This was initially successful, but the departing Dayaks set fire to ''Alceste'', burning her to the waterline. The destruction of the frigate's upper works exposed her hold, and the next morning the stranded sailors were able to collect some supplies that had floated out.<ref name="JH158">Henderson, p. 158</ref>

During the early morning of 26 February, British sentries spotted two proas attempting to land at the cove where the remaining British boats were anchored. Taking one of the boats to intercept the proas, Lieutenant Hay boarded a Dayak canoe and captured it, despite fire from the Dayak guns. Four Dayaks were killed, two captured, and five jumped into the sea and drowned themselves, having scuttled their proa.<ref name="ABO244">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 244</ref> Later in the day fourteen more proas appeared, led by a large vessel carrying a [[rajah]]. These approached the island and several Malays came ashore, a number of British sailors being admitted on board the rajah's canoe in return.<ref name="ABO247">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 247</ref> The inability of either side to speak the others language hindered negotiations, and the Malays retreated to their boats late in the day. The rajah subsequently directed renewed salvage operations on the wreck, seeking especially the copper nails that had held the ship's beams together.<ref name="JH158"/> By 2 March there were nearly 30 proas off the island, 20 of which were detached to open an ineffective long-range fire on the British positions ashore, accompanied by frenzied drumming and the bashing of [[gong]]s. Although further attempts were made to communicate with the proas, and messages successfully passed to them in the hope someone in authority would transmit them to nearby settlements, the British crew expected an attack at any moment.<ref name="ABO249">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 249</ref> In preparation, Maxwell gathered his men together and spoke to them:

<blockquote>My lads, you must all have observed the great increase in the enemy's force, and the threatening posture they have assumed. I have reason to believe they will attack us this night. I do not wish to conceal our real state, because I do not believe there is a man here who is afraid to face any sort of danger. We are in a position to defend ourselves against regular troops, far less a set of naked savages, with their spears and krisses. It is true they have swivels in their boats, but they cannot act here. I have not observed that they have any muskets, but if they have, so have we. When we were first thrown on shore we could only muster seventy-five musket ball cartridges &mdash; we now have sixteen hundred. They cannot send up, I believe, more than five hundred men; but with two hundred such as now stand around me, I do not fear a thousand &mdash; nor fifteen hundred of them. The pikemen standing firm, we will give them such a volley of musketry as they will be little prepared for; and when they are thrown into confusion, we will sally out, chase them into the water, and ten to one but we secure their vessels. let every man be on the alert, and should these barbarians this night attempt our hill, I trust we shall convince them they are dealing with Britons.<ref name="JH159">Henderson, p. 159</ref></blockquote>
So loud was the cheering that followed this address that the proas fell silent, the Dayaks apparently unnerved. In the morning however the 20 canoes were still offshore and, with the anticipated rescue overdue and supplies running low, a desperate plan was made to use the ship's boats to board and capture enough Dayak vessels to enable the entire crew to reach Batavia.<ref name="ABO252">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 252</ref> However, while these plans were being formed the British [[East India Company]]'s (EIC) armed [[brig]] [[Ternate (1801 EIC ship)|''Ternate'']] appeared on the southern horizon.<ref name="JH159"/>

===Napoleon===
Determined to make one last show of defiance, Maxwell ordered the marines to wade towards the proas at low tide and open fire on them. This achieved no hits, but did persuade the Dayaks to move further offshore, and they departed entirely when the ''Ternate'' was spotted.<ref name="ABO253">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 253</ref> The following day the survivors embarked on board ''Ternate'', Maxwell having lost not one man on either the shipwreck or the island. At Batavia the crew were reunited with Amherst and his party, who had sent ''Ternate'' to search for them and subsequently chartered the East Indiaman ''Caesar'' for the remainder of the journey to Britain.<ref name="JH160"/>

The voyage to Europe remained eventful. In the [[Indian Ocean]] ''Caesar'' caught fire and was almost destroyed,<ref name="FM566">''Fraser's Magazine'', 1843 Vol. XXVII, p. 566</ref> and after stopping at [[Cape Town]], the Indiaman visited [[St Helena]], where Amherst, Maxwell and the other officers were introduced to the former French emperor [[Napoleon Bonaparte]], then a prisoner on the island.<ref name="JH160">Henderson, p. 160</ref> At the meeting Bonaparte recalled Maxwell's conduct in the action of November 1811 and commended him on his victory, saying "''Vous êtes très méchant. Eh bien!'' Your government must not blame you for the loss of the Alceste, for you have taken one of my frigates".<ref name="USM533"/>

==Later service==
Returning to Britain in August 1817, where the story of his shipwreck and subsequent difficulties had become headline news, Maxwell was widely praised for his leadership. In the [[court martial]] convened to investigate the incident he was exonerated of all blame, and especially commended for his calm and authoritative control of the situation.<ref name="ABO255">''Annual Biography and Obituary'', 1832 Vol. XVI, p. 255</ref> Chief among the witnesses on his behalf was Lord Amherst himself. The court martial reported that "his coolness, self-collectedness and exertions were highly conspicuous, and everything was done by him and his officers within the power of man to execute".<ref name="USM533"/> The following year he was [[Knight Bachelor|knighted]], and in 1819 made a [[Fellow of the Royal Society]]. That same year the HEIC presented him with £1,500 as a reward for his services in China and to compensate him for his financial losses in the wreck.<ref>{{London Gazette|issue=17467|startpage=640|date=10 April 1819|accessdate=25 July 2008}}</ref> An account of the Yellow Sea voyage by Basil Hall was published in 1818 under the title "Account of a Voyage of Discovery to the West Coast of Corea and the Great Loo-Choo Islands". The book was dedicated to Sir Murray Maxwell, and proved popular.<ref name="ODNB"/>

[[File:Sir Murray Maxwell by Richard Dighton.jpg|thumb|175px|left|Etching of Maxwell by [[Richard Dighton]], c. 1818]]
Maxwell stood in the [[United Kingdom general election, 1818|1818 general election]], seeking to become [[Member of Parliament]] for [[Westminster (UK Parliament constituency)|Westminster]]. He was narrowly defeated by less than 400 votes, losing to [[Samuel Romilly|Sir Samuel Romilly]] and [[Francis Burdett|Sir Francis Burdett]]. The campaign ruined him financially, and after a "severe personal injury" in [[Covent Garden]] when he was struck in the back by a paving stone thrown from a mob opposed to his candidacy, he was left with disgust for the political process.<ref name="ODNB"/><ref name="USM533"/> Maxwell's lungs were badly damaged; he never fully recovered from the injury, and never again became involved in politics, instead returning to the Navy in 1821 as captain of {{HMS|Bulwark|1807|6}}, the flagship of Admiral [[Benjamin Hallowell Carew|Sir Benjamin Hallowell]] at [[Chatham, Kent|Chatham]]. The same year, the Arctic explorer [[Henry Parkyns Hoppner]], who had served under Maxwell aboard the ''Alceste'' on the mission to China, named [[Murray Maxwell Bay]] on [[Baffin Island]] after his former captain.<ref>{{cite book |title=The naval history of Great Britain, from the year MDCCLXXXIII. to MDCCCXXXVI |last=Brenton |first=Edward Pelham |authorlink=Edward Pelham Brenton |year=1837 |publisher=H. Colburn |location=London |isbn= |oclc=4599420 |url=https://books.google.com/?id=4uwLAAAAYAAJ&pg=PA573&lpg=PA573&dq=%22lieutenant+hoppner%22 |page=573}}</ref>

By 1823 Maxwell was in command of {{HMS|Gloucester|1812|6}} organising operations against [[smugglers]],<ref>{{London Gazette|issue=17886|startpage=47|date=11 January 1823|accessdate=2008-07-25}}</ref> and later in the year he was given a foreign posting in command of {{HMS|Briton|1812|6}} off South America. Here he observed the [[Peruvian War of Independence]] and was present at the surrender of [[Callao]], forming a friendship with the defeated [[José Ramón Rodil y Campillo|General Rodil]].<ref name="USM534">''United Services Magazine'', 1831 Part II, p. 534</ref> This posting proved a frustrating experience for Maxwell, who broke his [[kneecap]] on the outward journey and never fully recovered use of the limb. He also failed to gain any of the financial rewards that overseas postings could bring, and was unable to restore his shattered finances, returning a poorer man than when he had left.<ref name="FM567"/>

Still feeling the chest injury sustained during the 1818 election, Maxwell returned to Britain in 1826 and entered retirement; during this period he also reportedly had a bout of [[depression (mood)|depression]], especially following the sudden death of his youngest daughter in 1827.<ref name="FM567">''Fraser's Magazine'', 1843 Vol. XXVII, p. 567</ref> In 1830, he was recalled by the newly crowned [[William IV of the United Kingdom|King William IV]].  A former naval officer himself, King William selected a number of senior Navy officers to be his [[aides de camp]], including Maxwell, who was subsequently appointed to succeed [[John Ready]] as [[Lieutenant-Governors of Prince Edward Island|Lieutenant Governor]] of [[Prince Edward Island]] on 14 March 1831.<ref>{{London Gazette|issue=18784|startpage=494|date=15 March 1831|accessdate=2008-07-25}}</ref> As Maxwell sailed from his home in Scotland to London to make preparations for his departure, he was suddenly taken ill. Medical assistance was unavailable for 48 hours during the passage, and the weather too rough for him to go ashore in an open boat in his condition.<ref name="FM568">''Fraser's Magazine'', 1843 Vol. XXVII, p. 568</ref> As a result, Maxwell died shortly after arriving at Green's Hotel in [[Lincoln's Inn Fields]], London;<ref name="USM534"/> Colonel [[Aretas William Young]] took his place as governor.<ref name="PEI">[http://www.gov.pe.ca/photos/original/ele_governors.pdf Biography Sir Murray Maxwell] (PDF), ''Prince Edward Island Governors, Lieutenant Governors and Administrators'', Elections P.E.I. Office, p. 8, Retrieved 2008-07-24</ref> Maxwell was buried at [[St Marylebone Parish Church]], and was survived by his wife and their son [[John Balfour Maxwell]], who died in 1874 as an admiral of the Royal Navy.<ref name="ODNB"/>

==Notes==
{{Reflist|colwidth=30em}}

==References==

===Book sources===
*{{Cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume V
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-014-0
}}
*{{Cite book
 | last = Gardiner
 | first = Robert
 | authorlink =
 | year = 2001 |origyear=1998
 | chapter =
 | title = The Victory of Seapower: Winning the Napoleonic Wars, 1808&ndash;1814
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-359-1
}}
*{{Cite book
 | last = Grocott
 | first = Terence
 | authorlink =
 | year = 2002 |origyear=1997
 | chapter =
 | title = Shipwrecks of the Revolutionary & Napoleonic Era
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-164-5
}}
*{{Cite book
 | last = Henderson
 | first = James
 | authorlink =
 | year = 1994 |origyear=1970
 | chapter =
 | title = The Frigates, An Account of the Lighter Warships of the Napoleonic Wars 1793&ndash;1815
 | publisher = Leo Cooper
 | location =
 | isbn = 0-85052-432-6
}}
*{{Cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | chapter =
 | title = The Naval History of Great Britain, 1793&ndash;1827
 | publisher = Conway Maritime Press
 | location =
 | id =
}}

===Web sources===
* {{Cite encyclopedia
 | title = Maxwell, Sir Murray
 | encyclopedia  = [[Oxford Dictionary of National Biography]] |author= [[J. K. Laughton|Laughton, J. K.]]
 | subscription=yes |publisher =Oxford University Press |year=2004 |others=revised Andrew Lambert
 | url   = http://www.oxforddnb.com/view/article/18408}} Retrieved on 25 July 2008
* {{Cite journal
 | author1 = Carlyle, Thomas
 | title = The Life of Sir Murray Maxwell
 | journal  = Fraser's Magazine |year=1843 |volume= XXVII |pages= 557&ndash;571
 | url   = https://books.google.com/?id=FIRPrUg2dwkC&pg=PA557&dq=%22Sir+Murray+Maxwell%22#PPA570,M1}} Retrieved on 25 July 2008
* {{Cite journal
 | title = No. XV. Sir Murray Maxwell, Knight
 | journal  = The Annual Biography and Obituary |year= 1832 |volume= XVI |pages= 220&ndash;255
 | url   = https://books.google.com/?id=xtkKAAAAYAAJ&pg=PA220&dq=%22Sir+Murray+Maxwell%22#PPA220,M1}} Retrieved on 25 July 2008
* {{Cite journal
 | title = The services of the late Capt. Sir Murray Maxwell, KNT & C.B.
 | journal  = United Service Magazine|year=1831 |pages=Part II. 531&ndash;534
 | url   = https://books.google.com/?id=yQOe7brX3psC&pg=PA531&dq=%22Sir+Murray+Maxwell%22#PPA531,M1}} Retrieved on 25 July 2008
* {{Cite web
 | title = Sir Murray Maxwell
 | work  = Prince Edward Island Governors, Lieutenant Governors and Administrators |publisher=Elections P.E.I. Office |page=8
 | url   = http://www.gov.pe.ca/photos/original/ele_governors.pdf |format=pdf}} Retrieved on 25 July 2008

{{S-start}}
{{S-off}}
{{Succession box|title=[[Lieutenant Governor of Prince Edward Island]]|before=[[John Ready]]|after=[[Aretas William Young]]|years=1831}}
{{S-end}}

{{PELG}}

{{Featured article}}

{{Authority control}}

{{DEFAULTSORT:Maxwell, Murray}}
[[Category:1775 births]]
[[Category:1831 deaths]]
[[Category:Royal Navy personnel of the French Revolutionary Wars]]
[[Category:Royal Navy personnel of the Napoleonic Wars]]
[[Category:Companions of the Order of the Bath]]
[[Category:Fellows of the Royal Society]]
[[Category:Knights Bachelor]]
[[Category:People from Dumfries and Galloway]]
[[Category:Royal Navy officers]]
[[Category:Lieutenant Governors of the Colony of Prince Edward Island]]