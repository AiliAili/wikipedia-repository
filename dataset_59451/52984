{{Infobox journal
| title = The International Journal of Press/Politics
| cover = [[File:The International Journal of Press Politics Journal Front Cover.jpg]]
| editor = [[Rasmus Kleis Nielsen]]
| discipline = [[Journalism]]
| former_names =
| abbreviation = Int. J. Press/Polit.
| publisher = [[SAGE Publications]]
| country =
| frequency = Quarterly
| history = 1996–present
| openaccess =
| license =
| impact = 1.979
| impact-year = 2015
| website = http://hij.sagepub.com/
| link1 = http://hij.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hij.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1940-1612
| eISSN =
| OCLC = 637780284
| LCCN = 2007213329
}}
'''''The International Journal of Press/Politics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covering the field of [[journalism]], especially the linkages between the news media and political processes and actors. The [[editor-in-chief]] is [[Rasmus Kleis Nielsen]] ([[University of Oxford]]). It was established in 1996 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''The International Journal of Press/Politics'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.979, ranking it 18th out of 163 journals in the category "Political Science" and 10th out of 79 journals in the category "Communication".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science and Communication |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://hij.sagepub.com/}}

{{DEFAULTSORT:International Journal of Press Politics}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Media studies journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1966]]