{{Infobox journal
| title = Annales. Histoire, Sciences Sociales
| cover = 
| editor = [[Étienne Anheim]]
| discipline = [[Social history]]
| language = French
| former_names = Annales d'histoire économique et sociale (1929 to 1939), Annales d'histoire sociale (1939–1942, 1945), Mélanges d’histoire sociale (1942–1944), Annales. Economies, sociétés, civilisations (1946–1994), Annales. Histoire, Sciences Sociales (1994-present)
| abbreviation = Annales
| publisher = [[School for Advanced Studies in the Social Sciences|EHESS]]
| country = France
| frequency = three-monthly
| history = 1929-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://annales.ehess.fr/?lang=en/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 436601008
| LCCN = 49012430
| CODEN = 
| ISSN = 0395-2649
| eISSN = 
}}
'''''Annales. Histoire, Sciences Sociales''''' is a French [[academic journal]] covering [[social history]] that was established in 1929 by [[Marc Bloch]] and [[Lucien Febvre]]. The journal gave rise to an approach to history known as the [[Annales School]]. The journal began in Strasbourg as ''Annales d'histoire économique et sociale''; it moved to Paris and kept the same name from 1929 to 1939. It was successively renamed ''Annales d'histoire sociale'' (1939–1942, 1945), ''Mélanges d’histoire sociale'' (1942–1944), ''Annales. Economies, sociétés, civilisations'' (1946–1994), and, finally, ''Annales. Histoire, Sciences Sociales'' in 1994.<ref name="journal names">P. Burke, ''The French Historical Revolution. The Annales School 1929–89'', p. 116 n. 2.</ref><ref name="Hunt">Hunt, Lynn. "French History in the Last Twenty Years: the Rise and Fall of the Annales Paradigm." Journal of Contemporary History 1986 21(2): 209–224.</ref> In 2013 it began publication of an English language edition, with all the articles translated.

The scope of topics covered by the journal is wide but the emphasis is on social history and long-term trends (''[[longue durée]]''), often using quantification and paying special attention to [[geography]]<ref>See Lucien Febvre, ''La Terre et l'évolution humaine'' (1922), translated as ''A Geographical Introduction to History'' (London, 1932).</ref> and to the intellectual world view of common people, or [[History of mentalities|"mentality" (''mentalité'')]]. Less attention is paid to political, diplomatic, or military history, or to biographies of famous men. Instead the ''Annales'' focused attention on the synthesizing of historical patterns identified from social, economic, and cultural history, statistics, medical reports, family studies, and even psychoanalysis.<ref name="journal names"/><ref name="Hunt"/>

An online English language edition was planned for 2012.<ref>[http://www.editions.ehess.fr/revues/annales-histoire-sciences-sociales/ EHESS: Annales. Histoire, Sciences Sociales (Accessed Nov 2011)]</ref>

== See also ==
* [[World-systems theory]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://annales.ehess.fr/}}
* [http://www.editions.ehess.fr/revues/annales-histoire-sciences-sociales/ Éditions de l'EHESS]
* [http://www.persee.fr/web/revues/home/prescript/revue/ahess Free access to all issues of the ''Annales'' from 1929 to 2002.]
* [http://www.jstor.org/journal/annahistscisoc Jstor]

{{Annales School}}

[[Category:Bimonthly journals]]
[[Category:French-language journals]]
[[Category:History journals]]
[[Category:Sociology journals]]
[[Category:Publications established in 1929]]
[[Category:Academic journals published in France]]
[[Category:Academic journals associated with non-profit organizations]]