{{good article}}
{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
| Name          = Buddha
| Type          = Demo
| Artist        = [[Blink-182]]
| Cover         = Blink-182 - Buddha re-release cover.jpg
| Lower caption = The cover for the CD version
| Released      = January 1994<small> (original)</small><br />October 27, 1998 <small>(re-issue)</small>
| Recorded      = January 1994
| Studio = Doubletime Studios, [[Santee, California]]<ref name="linernotes"/>
| Genre         = [[Skate punk]]<ref name="am" />
| Length        = 35:49<br />31:55 <small>(re-issue)</small>
| Label         = Filter<small> (original)</small><br />[[Kung Fu Records|Kung Fu]] <small>(re-issue)</small>
| Producer      = Pat Secor
| Reviews       =
| Chronology    = [[Blink-182]]
| Last album    = ''Demo No.2''<br />(1993)
| This album    = '''''Buddha'''''<br />(1994)
| Next album    = ''[[Cheshire Cat (Blink-182 album)|Cheshire Cat]]''<br />(1995) |
 Misc =
}}
'''''Buddha''''' is the third [[demo (music)|demo]] by the American [[rock music|rock]] band [[Blink-182]]. Recorded and released in January 1994 under the name Blink, it was the band's first recording to be sold and distributed. Blink-182 was formed in [[Poway, California]], a suburb outside of [[San Diego]], in August 1992. Guitarist [[Tom DeLonge]] and [[Mark Hoppus]] were introduced to one another by Hoppus' sister. The duo recruited drummer [[Scott Raynor]] and began to practice together in his bedroom, spending hours together writing music, attending punk shows and movies and playing practical jokes. The band had recorded two previous demos in Raynor's bedroom — ''Flyswatter'' and ''Demo No.2'' — using a [[Multitrack recording|four track recorder]]. Most of the tracks from the demo were re-recorded for their debut album ''[[Cheshire Cat (Blink-182 album)|Cheshire Cat]]''.

Pat Secor, Hoppus' boss at music store [[Wherehouse Music|The Wherehouse]], was attempting to start his own record label, named Filter Records. Secor pulled money from his savings and helped finance and produce the demo recording. ''Buddha'' was recorded live at local [[Santee, California|Santee]] studio Doubletime, compiling a collection that represented nearly all of the songs the band had written up to that point. Hoppus and friends Cam Jones and Kerry Key created the cassette artwork, and the original cassette packaging was compiled by the band and Hoppus' family. Locally distributed to several [[San Diego]] record stores and available for purchase at early concerts, ''Buddha'' helped the trio cement an audience and was a deciding factor in their signing to local label [[Cargo Music|Cargo]] in 1993.

The recording became the subject of a legal dispute between the band and Secor in later years. The band accused Secor of selling the tape without paying royalties, and attempted to put a stop to his distribution with help of lawyer [[Joe Escalante]] of [[The Vandals]], who also owned independent record label [[Kung Fu Records]]. Kung Fu digitally remixed and remastered the demo and commercially re-released it in October 1998, deleting two original tracks for other recordings from the original session. Kung Fu has since reportedly sold 300,000 copies of ''Buddha''. It is currently the only commercially available demo by Blink-182.

==Background==
Blink-182 was formed in [[Poway, California]], a suburb outside of [[San Diego]], in August 1992. After Mark Hoppus graduated from high school in [[Ridgecrest, California|Ridgecrest]], he relocated to San Diego to work at a record store and attend college.<ref name=p8-9>Hoppus, 2001. pp. 8–9</ref> Tom DeLonge was kicked out of [[Poway High School|Poway High]] for attending a basketball game drunk and was forced to attend another local school for one semester. At [[Rancho Bernardo High School]], he befriended Kerry Key, also interested in punk music. Key's girlfriend, Anne Hoppus, introduced her brother Mark to DeLonge on August 2, 1992.<ref name="p8-9"/> The two clicked instantly and played for hours in DeLonge's garage, exchanging lyrics and co-writing songs—one of which became "[[Cheshire Cat (Blink-182 album)|Carousel]]".<ref name="p8-9"/> DeLonge recruited friend Scott Raynor for drums, whom he met at a Rancho Bernado Battle of the Bands competition.<ref name=p10-11>Hoppus, 2001. pp.&nbsp;10–11</ref> Raynor was by far the youngest member of the trio at 14, and his event account differs significantly: he claims he and DeLonge formed the group after meeting at the Battle of the Bands and worked through a variety of bassists before meeting Hoppus.<ref name="p10-11"/>

The trio began to practice together in Raynor's bedroom, spending hours together writing music, attending punk shows and movies, and playing practical jokes.<ref name="p10-11"/> Hoppus and DeLonge would alternate singing vocal parts. The trio first operated under a variety of names, including Duck Tape and Figure 8, until DeLonge rechristened the band "Blink".<ref name=p13shoo>Shooman, 2010. pp.&nbsp;13-14</ref> Hoppus' girlfriend was angered by her boyfriend's constant attention for the band and demanded him to make a choice between the band and her, which resulted in Hoppus leaving the band shortly after formation.<ref name=p13-15>Hoppus, 2001. pp. 13–15</ref> Shortly thereafter, DeLonge and Raynor borrowed a [[Multitrack recording|four track recorder]] from friend and collaborator Cam Jones and were preparing to record a [[Demo (music)|demo tape]], with Jones on bass.<ref name="p13shoo"/> Hoppus promptly broke up with his girlfriend and returned to the band.<ref name="p13-15"/> ''Flyswatter''—a combination of original songs and punk covers—was recorded in Raynor's bedroom in May 1993.<ref name=p16>Hoppus, 2001. p. 16</ref> [[Southern California]] had a large punk population in the early 1990s, aided by an avid surfing, skating and snowboarding scene.<ref name="p18shoo"/> In contrast to [[East Coast of the United States|East Coast]] punk music, the [[West Coast of the United States|West Coast]] wave of groups, Blink included, typically introduced [[pop punk|more melodic aspects to their music]].<ref name="p18shoo"/> "New York is gloomy, dark and cold. It makes different music. The Californian middle-class suburbs have nothing to be that bummed about," said DeLonge.<ref name=p18shoo>Shooman, 2010. pp. 18–19</ref> San Diego at this time was "hardly a hotbed of [musical] activity", but the band's popularity grew as did [[Punk rock in California|California punk rock]] concurrently in the mainstream.<ref name="p16"/>

The band's first performance was at a local high school during lunch, and soon the trio graduated to San Diego's Spirit Club and influential local shop Alley Kat Records.<ref name=p15shoo>Shooman, 2010. pp.&nbsp;15-17</ref> DeLonge called clubs constantly in San Diego asking for a spot to play, as well as calling up local high schools convincing them that Blink was a "motivational band with a strong [[Just Say No|anti-drug]] message" in hopes to play at an assembly or lunch.<ref name=p21>Hoppus, 2001. p. 21</ref> The band soon became part of a circuit that also included the likes of [[Ten Foot Pole]] and [[Unwritten Law]], and they found their way onto the bill as the opening band for local acts at [[Soma San Diego|SOMA]], a local all-ages venue which they longed to headline.<ref name="p18shoo"/> The band's equipment was piled into a blue station wagon for touring purposes and they first began to play shows outside San Diego.<ref name=p24-27>Hoppus, 2001. pp. 24–27</ref>

==Recording and production==
''Buddha'' was financed by Pat Secor, Hoppus' boss at [[Wherehouse Music]] in [[San Diego]]. Secor wanted to start his own record label and offered to help pay for costs. "He was like, hey, I'll front you the money, and we'll split the profits until you pay me back," recalled Hoppus in 2001.<ref name=p24-25>Hoppus, 2001. p. 24-25</ref> The two had met when Secor transferred from a north San Diego location. The two became friends quickly, despite Secor's seniority of post.<ref name=shooman14>Shooman, 2010. p. 14-16</ref> "At that point they'd played around enough to get their chops up so I took all the money I had in savings and we went into the studio for two days," said Secor.<ref name="shooman14" />

The recording sessions at Doubletime Studios in [[Santee, California]] took place in January 1994, and were scheduled around work and school commitments. Hoppus was sick at the time of recording.<ref name="shooman17" /> Despite this, the band carried on and the demo was complete within two days. "''Buddha'' was cut live then we added the vocals. Two days and they were done – including the mix. It's quite standard for a young punk band to do that," said engineer Jeff Forrest.<ref name=shooman17>Shooman, 2010. p. 17</ref> Despite this, the liner notes for the cassette claim it was recorded in twelve hours,<ref name="linernotes">{{cite AV media notes | title=Buddha | year=1994 | others=[[Blink-182|Blink]] | type=liner notes | publisher=Filter Records | location=[[United States|US]] | id=none}}</ref> while the later remaster of ''Buddha'' contend it was recorded "over three rainy nights."<ref name="p24-25" /><ref name="linernotesRE" /> The trio were "super stoked" about a sound effects tape they found at the studio, and took time out to add in applause and laughter tracks because they deemed it humorous.<ref name="p24-25" />

Hoppus and DeLonge took the songwriting for their first legitimate release very seriously. The two strove for perfection writing songs that they felt would be relatable.<ref name="p24-25" /> Blink also recorded joke tracks, as they felt that, in addition to the serious songs, "it was almost as important to make people laugh." DeLonge recalled that the band spent more time at the end of production on ''Buddha'' trying to perfect the joke songs rather than their serious tracks.<ref name="p24-25" /> The band's main influence on ''Buddha'', according to DeLonge, was the [[Descendents]]. "I was trying to emulate that band. Really punchy guitars, fast, simple and formulaic nursery rhyme love songs," he said in 2012.<ref name=totalguitar>{{cite journal| date =October 12, 2012| title =Tom DeLonge talks guitar tones, growing up and Blink| journal =[[Total Guitar]]| publisher =[[Future Publishing]] | location =[[Bath, Somerset|Bath]], [[United Kingdom]]| issn =1355-5049 | url =http://www.musicradar.com/news/guitars/tom-delonge-talks-guitar-tones-growing-up-and-blink-565422 | accessdate =October 13, 2012}}</ref>

==Packaging and release==
[[File:Mark Hoppus, May 1994.jpg|thumb|right|150px|Mark Hoppus in May 1994, shortly after ''Buddha'' was recorded.]]
The photographs in the original cassette release of ''Buddha'' were photographs taken by friend of the band Cam Jones.<ref name="p24-25" /> Kerry Key, drummer for the Iconoclasts and friend of the band, also is credited with artwork in the original cassette.<ref name="linernotes" /> To produce the artwork, Hoppus and Jones spent an afternoon together taking "artsy" photographs in and around Raynor's backyard.<ref name="p24-25" /> The cover art is a picture of a Buddha statue, which was a present from Hoppus' stepfather that the bassist grabbed on the way to Raynor's for the photos. After developing the photos, they took them to a copy shop to run off color copies. Afterwards, they cut, pasted and rearranged them until they found something suitable. The lyric sheets were handwritten and photocopied.<ref name="p24-25" /> Hoppus and his family in [[Ridgecrest, California|Ridgecrest]] would spend hours folding and combining pieces of artwork to compile the ''Buddha'' cassette. When this was complete, Hoppus would load the cassettes into his car and deliver them to local record stores around town.<ref name="page26" />

{{Quote|text="I totally remember driving around to all the record stores to drop off tapes to sell. I'd go to Lou's Records, and Off the Record, and Music Trader. It was so cool because the tapes were actually selling, that's why I had to keep going back every week. Music Trader would have sold one copy, Off the Record sold two, or whatever. But that meant people were actually walking into a music store and buying something we had written and recorded. It was awesome."|sign=[[Mark Hoppus]]<ref name=page26>Hoppus, 2001. p. 26</ref>}}

The demo tape, which was originally untitled, came to be known by the name ''Buddha'', and was released by Filter Records in 1994.<ref name="linernotesRE">{{cite AV media notes | title=Buddha | year=1998 | others=[[Blink-182]] | type=liner notes | publisher=[[Kung-Fu Records]] | location=[[United States|US]] | id=Kung Fu 78765-2}}</ref>  Cassette copies of ''Buddha'' were also sold at early Blink concerts, alongside homemade T-shirts.<ref name="page26" />

==Reception==
{{Album ratings
|rev1 = [[AllMusic]]
|rev1score = {{Rating|3|5}}<ref name="am" />
}}
[[Stephen Thomas Erlewine]] of [[AllMusic]] called ''Buddha'' a "promising debut," considering it "a solid skatepunk record that illustrates the group's flair for speedy, catchy hooks and irreverent humor."<ref name="am">{{cite web |author=[[Stephen Thomas Erlewine]] |url=http://www.allmusic.com/album/buddha-mw0000054080 |title=Buddha - blink-182 |publisher=[[AllMusic]] |accessdate=February 5, 2014}}</ref> ''[[Rolling Stone]]'' viewed it, alongside proper debut album ''[[Cheshire Cat (Blink-182 album)|Cheshire Cat]]'' (1995) as "slapped together lilting melodies and racing beats in an attempt to connect [[emo]] and [[skate punk]], a sort of pop hardcore."<ref name=newrs>Brackett, Nathan (ed.) (2004). ''The New Rolling Stone Album Guide''. New York: Fireside, 904 pp. First edition, 2004.</ref> "This fast and furious beauty may have been recorded in two days, but it soon had the labels knocking at DeLonge and co’s door," said ''[[Total Guitar]]'' in 2012.<ref name="totalguitar" />

==Controversy==
[[File: Blink-182 - Buddha cover.jpg|thumb|left|200px|The cover for the 1994 original cover of ''Buddha'' by Filter Records.]]
The rights to ''Buddha'' and its associated recordings were the subject of a legal dispute between the band and Secor in later years. According to Secor, he and the band had a gentleman's agreement: he would pay for the costs of recording and manufacturing the tape, and in exchange would receive half of all the profits from it. Raynor contends that the oral agreement was that Secor would invest $1,000 and when that money was recouped, the band would have complete ownership of the work product.<ref name="controversy">{{cite web|author=Katy St. Clair|url=http://www.eastbayexpress.com/eastbay/planet-clair/Content?oid=1066056|title=Planet Clair|publisher=''[[East Bay Express]]''|date=September 12, 2001|accessdate=September 14, 2013}}</ref> Secor helped the band sign to [[Cargo Music]] in 1994, as he had connections at the label; he felt that by helping the briskly growing band sign a deal he could build his own label, Filter, in the wake of Blink's success. According to Secor, he attempted to contact the band to discuss the rights to the tape, but would only receive comments such as "Oh, let me call my manager and I'll call you right back."<ref name="controversy" /> Secor asserted he should have the rights to the master tapes, as he financed the entire production. Cargo Music began calling and making threats, and Secor had no money to fight back with, as they had no written contract. In 1996, the band signed a joint-venture deal with major label [[MCA Records]], who also began making calls to Secor. "Try going up against that," Secor remarked in 2001.<ref name="controversy" />

The band began to grow suspicious that Secor was keeping the money from selling the tape, and contacted their lawyer – [[Joe Escalante]] of [[The Vandals]], who also owned independent record label [[Kung Fu Records]]. The group informed as Escalante that they believed "someone's bootlegging it," and requested his legal help to stop Secor.<ref name="controversy" /> In exchange for legal fees, Blink-182 would allow Escalante's label, Kung Fu, to re-release ''Buddha'' on [[compact disc]].<ref name="controversy" /> The band had told Secor to not sell any more copies of the tape, but they held suspicions that he had anyway. Anonymously, Escalante ordered a tape from Secor, and Secor sold it to him.<ref name="controversy" /> The band asserted that they were not receiving royalties for these sales. "I paid off all of the royalties for the remaining stash of tapes that I had of Buddha," said Secor in 2001.<ref name="controversy" /> "It was about 25. The tapes sold for five bucks, and I gave them half of what their profit would be. I wanted to have a few to give to people and to have on hand." Secor felt it was his right to sell his stock of the tape, as the band "had been paid royalties for that already."<ref name="controversy" />

Kung Fu re-released ''Buddha'' on CD and cassette in November 1998, and has since re-released the recording on vinyl and retains digital distribution. The remaster cleans and sharpens the sound of tracks, and contains a slightly different track listing. "They'd already sold 60,000 copies of ''Cheshire Cat'', and I thought, 'Man, if I can sell just 10% of that that would be great for the label,' and of course it sold a lot more because they went on to be superstars," said Escalante.<ref name=p57shoo>Shooman, 2010. pp.&nbsp;57–58</ref> In 2001, the label had reportedly sold 300,000 copies of ''Buddha''.<ref name="controversy" /> "At this point it's not even the money," Secor said at the time. "It's the fact that there is no mention of my work anywhere; no credit has been given to me."<ref name="controversy" />

==Track listing==
{{tracklist
| collapsed       =
| headline        = Original version
| all_writing     = [[Mark Hoppus]], [[Tom DeLonge]], and [[Scott Raynor]], except where noted
| total_length    = 35:49
|title1	 = Carousel
|length1 = 2:54
|title2	 = TV
|length2 = 1:41
|title3	 = Strings
|length3 = 2:25
|title4	 = Fentoozler
|length4 = 2:06
|title5	 = Time
|length5 = 2:49
|title6	 = Romeo and Rebecca
|length6 = 2:34
|title7	 = 21 Days
|length7 = 4:03
|title8	 = Sometimes
|length8 = 1:08
|title9	 = Degenerate
|length9 = 2:28
|title10  = Point of View
|length10 = 1:11
|title11  = My Pet Sally
|length11 = 1:36
|title12  = Reebok Commercial
|length12 = 2:36
|title13  = Toast and Bananas
|length13 = 2:33
|title14  = The Family Next Door
|length14 = 1:47
| note14   = Blink-182, Brian Casper<ref name="linernotes" />
|title15  = Transvestite
|length15 = 3:59
}}
{{tracklist
| collapsed       =
| headline        = Remastered version
| all_writing     =
| total_length    = 31:55
|title1  = Carousel
|length1 = 2:40
|title2  = TV
|length2 = 1:37
|title3  = Strings
|length3 = 2:28
|title4  = Fentoozler
|length4 = 2:03
|title5  = Time
|length5 = 2:46
|title6  = Romeo and Rebecca
|length6 = 2:31
|title7  = 21 Days
|length7 = 4:01
|title8  = Sometimes
|length8 = 1:04
|title9  = Point of View
|length9 = 1:11
|title10  = My Pet Sally
|length10 = 1:36
|title11  = Reebok Commercial
|length11 = 2:35
|title12  = Toast and Bananas
|length12 = 2:26
|title13  = [[Radio Blast|The Girl Next Door]]
|note13   = [[Ben Weasel]]
|length13 = 2:31
|title14  = Don't
|length14 = 2:26
}}

==Personnel==
{{col-start}}
{{col-2}}
;Blink
*[[Mark Hoppus]] – [[vocals]], [[bass guitar]], [[cover art]]
*[[Tom DeLonge]] – vocals, [[guitar]]
*[[Scott Raynor]] – [[drums]]

{{col-2}}
'''Production'''
*Pat Secor – [[Sound recording and reproduction|production]]
*Jeff Forrest – [[Audio mixing (recorded music)|mixing engineer]]

'''Design'''
*Kerry Key –  cover art
*Cam Jones –  cover art
* Grace Walker – art direction (reissue)
{{col-end}}

==Chart positions==
{{col-begin}}
{{col-2}}

=== Weekly charts ===
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (1998)
!scope="col"| Peak<br>position
|-
|Australia ([[ARIA Charts|ARIA]])<ref name=ryan>{{cite book|last=Ryan|first=Gavin|title=Australia's Music Charts 1988–2010|year=2011|publisher=Moonlight Publishing|location=Mt. Martha, VIC, Australia}}</ref>
|78
|}

==See also==
*''[[Cheshire Cat (Blink-182 album)|Cheshire Cat]]'', the band's 1995 debut album on Cargo containing re-recordings of several tracks on ''Buddha''

==References==
* {{Cite book
 | last = Hoppus
 | first = Anne
 | authorlink =
 | title = Blink-182: Tales from Beneath Your Mom
 | date = October 1, 2001
 | publisher = [[MTV|MTV Books]] / [[Pocket Books]]
 | isbn =  0-7434-2207-4
 }}
* {{Cite book
 | last = Shooman
 | first = Joe
 | authorlink =
 | title = Blink-182: The Bands, The Breakdown & The Return
 | date = June 24, 2010
 | publisher = Independent Music Press
 | isbn = 978-1-906191-10-8
 }}

==Notes==
{{reflist|30em}}

==External links==
<!-- This is a licensed stream for the album, which is allowed under Wikipedia polices -->
*[https://www.youtube.com/playlist?list=PLPywHf374Yxdrj4JvVuv-sAbOcA-EZTwF ''Buddha''] at [[YouTube]] (streamed copy where licensed)
* {{Official website|http://blink182.com/}}
* {{Discogs master|91447|Buddha|type=album}}

{{Blink-182}}

{{DEFAULTSORT:Buddha (Album)}}
[[Category:1994 albums]]
[[Category:Blink-182 albums]]
[[Category:English-language albums]]
[[Category:Kung Fu Records albums]]
[[Category:Demo albums]]