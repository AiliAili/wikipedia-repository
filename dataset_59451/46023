{{Infobox pro wrestling championship
| championshipname = ACW Heavyweight Championship
| image = 
| image_size = 
| caption = 
| currentholder =
| won =
| aired = 
| promotion = [[Assault Championship Wrestling]]
| brand = 
| created = August 24, 2001
| mostreigns = Dylan Kage (2 reigns)
| firstchamp = [[Chris Hamrick]]
| longestreign = Chris Hamrick (218 days)
| shortestreign = Jeff Rocket (4 days)
| oldest = 
| youngest = 
| heaviest = 
| lightest = 
| pastnames = 
| titleretired = March 21, 2004
| pastlookimages = 
}}
The '''ACW Heavyweight Championship''' was the top [[professional wrestling]] [[championship (professional wrestling)|championship title]] in the [[United States|American]] [[independent wrestling|independent]] [[professional wrestling promotion|promotion]] [[Assault Championship Wrestling]]. The first-ever champion was [[Chris Hamrick]] who won the title in a championship tournament held in [[Meriden, Connecticut]] on August 24, 2001. The championship was regularly defended throughout the state of [[Connecticut]], most often in [[Meriden, Connecticut]], until the promotion closed in early-2004.<ref name="Solie">{{cite web|url=http://www.solie.org/titlehistories/htacw.html|title=ACW Heavyweight Title History|author=Tsakiries, Phil|year=2004|publisher=Solie's Title Histories}}</ref>

Dylan Kage holds the record for most reigns as the only 2-time champion. At 217 days, Chris Hamrick's reign is the longest in the title's history. Jeff Rocket's reign, which lasted only 4 days before vacating due to injury, was the shortest in the history of the title. Overall, there have been 9 reigns shared between 8 wrestlers, with one vacancy.<ref name="Solie"/>

==Title history==
{| class="wikitable"
|-
|'''#'''
|Order in reign history
|-
|'''Reign'''
|The reign number for the specific set of wrestlers listed
|-
|'''Event'''
|The event in which the title was won
|-
|&nbsp;—
|Used for vacated reigns so as not to count it as an official reign
|-
|N/A
|The information is not available or is unknown
|-
|'''+'''
|Indicates the current reign is changing daily
|}

===Reigns===
{| class="wikitable sortable" width=98% style="text-align:center;"
!style="background:#e3e3e3" width=0%| #
!style="background:#e3e3e3" width=20%|Wrestlers
!style="background:#e3e3e3" width=0%|Reign
!style="background:#e3e3e3" width=10%|Date
!style="background:#e3e3e3" width=0%|Days<br />held
!style="background:#e3e3e3" width=16%|Location
!style="background:#e3e3e3" width=18%|Event
!style="background:#e3e3e3" width=60% class="unsortable"|Notes
!style="background:#e3e3e3;" width=0% class="unsortable"|Ref.
|-
|{{sort|01|1}}
|{{sort|Hamrick|[[Chris Hamrick]]}}
|{{sort|01|1}}
|{{dts|link=off|2001|8|24}}
|{{age in days nts|month1=8|day1=24|year1=2001|month2=3|day2=30|year2=2002}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Won tournament to become the first ACW Heavyweight Champion.
|align="left"|<ref name="Solie"/>
|-
|{{sort|02|2}}
|{{sort|Brown|[[Slyk Wagner Brown]]}}
|{{sort|01|1}}
|{{dts|link=off|2002|3|30}}
|{{age in days nts|month1=3|day1=30|year1=2002|month2=6|day2=9|year2=2002}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|03|3}}
|{{sort|Mahoney|[[Balls Mahoney]]}}
|{{sort|01|1}}
|{{dts|link=off|2002|6|9}}
|{{age in days nts|month1=6|day1=9|year1=2002|month2=8|day2=10|year2=2002}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|04|4}}
|{{sort|Rocket|Jeff Rocket}}
|{{sort|01|1}}
|{{dts|link=off|2002|8|10}}
|{{age in days nts|month1=8|day1=10|year1=2002|month2=8|day2=14|year2=2002}}
|{{sort|Pikesville|Pikesville, Connecticut}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|4.5|—}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|—}}
|{{dts|link=off|2002|8|14}}
|{{sort|zz|—}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|N/A
|align="left"|The championship is vacated when Jeff Rocket suffers an injury.
|align="left"|<ref name="Solie"/>
|-
|{{sort|05|5}}
|{{sort|Kage|Dylan Kage}}
|{{sort|01|1}}
|{{dts|link=off|2002|8|14}}
|{{age in days nts|month1=8|day1=14|year1=2002|month2=12|day2=1|year2=2002}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Defeated Ron Zombie in a singles match. 
|align="left"|<ref name="Solie"/>
|-
|{{sort|06|6}}
|{{sort|Normous|Paul E. Normous}}
|{{sort|01|1}}
|{{dts|link=off|2002|12|1}}
|{{age in days nts|month1=12|day1=1|year1=2002|month2=3|day2=14|year2=2003}}
|{{sort|New Britain|[[New Britain, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/><ref>{{cite web |url=http://www.onlineworldofwrestling.com/results/assault/ |title=Assault Championship Wrestling |author= |year=2003 |work=Miscellaneous Independent Results |publisher=OnlineWorldofWrestling.com |accessdate=26 July 2010}}</ref>
|-
|{{sort|07|7}}
|{{sort|Thunder|Johnny Thunder}}
|{{sort|01|1}}
|{{dts|link=off|2003|3|14}}
|{{age in days nts|month1=3|day1=14|year1=2003|month2=8|day2=10|year2=2003}}
|{{sort|East Hartford|[[East Hartford, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/><ref>{{cite web |url=http://www.metrocast.net/~jjakan/results-march03.html |title=March 14, 2003: Assault Championship Wrestling (ACW) |author= |date=March 2004 |work=Results: 2003 |publisher=The New England Independent |accessdate=26 July 2010}}</ref>
|-
|{{sort|08|8}}
|{{sort|Kage|Dylan Kage}}
|{{sort|02|2}}
|{{dts|link=off|2003|8|10}}
|{{age in days nts|month1=8|day1=10|year1=2003|month2=2|day2=15|year2=2004}}
|{{sort|Meriden|[[Meriden, Connecticut]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-
|{{sort|09|9}}
|{{sort|Zombie|Ron Zombie}}
|{{sort|01|1}}
|{{dts|link=off|2004|2|15}}
|{{age in days nts|month1=2|day1=15|year1=2004|month2=3|day2=21|year2=2004}}
|{{sort|New Britain|[[New Britain, Connecticut]]}}
|{{sort|Reload (2004)|Reload (2004)}}
|align="left"|
|align="left"|<ref name="Solie"/><ref>{{cite web |url=http://www.411mania.com/wrestling/news/21585 |title=411 Indies Update: Big Names Return In CA, Results |author=Levene, Lance |date=February 19, 2004 |work=News |publisher=411mania.com |accessdate=26 July 2010}}</ref><ref>{{cite web |url=http://www.metrocast.net/~jjakan/features-roadreport-02-15-04.html |title=Road Report: Volume 43 (Assault Championship Wrestling-2/15/04) |author= |date=February 2004 |work=Road Reports: 2004 |publisher=The New England Independent |accessdate=26 July 2010}}</ref><ref>"Assault Championship Wrestling: "Reload" 2/15/04, Club Arkadia, New Britain, CT." DeclarationofIndependents.net. Ed. Sean McCaffrey. N.p., 2004. Web. 11 Oct. 2010. <www.declarationofindependents.net/doi/pages/specialsubmits/adam/acw215.html></ref>
|-
|{{sort|9.5|&mdash;}}
|{{sort|Deactivated|[[Glossary of professional wrestling terms#Vacated|Deactivated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|2004|03|21}}
|{{sort|zz|&mdash;}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|ACW holds its last show on March 21, 2004.
|align="left"|<ref name="Solie"/>
|}

==References==
<references/>

==External links==
*[http://web.archive.org/web/20031006063816/www.assaultwrestling.com/titlehistory.htm Official ACW Heavyweight Championship Title History]
*[http://www.WrestlingData.com/index.php?befehl=titles&titel=1838 ACW Heavyweight Championship at WrestlingData.com]

[[Category:Heavyweight wrestling championships]]