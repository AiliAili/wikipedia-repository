{{Use dmy dates|date=July 2015}}
{{Infobox officeholder
|name               = Mark Hauptmann
|native_name        = 
|native_name_lang   = 
|image              = Hauptmann Mark im Deutschen Bundestag - 2013-02-15.jpg
|imagesize          = 
|smallimage         = <!--If this is specified, "image" should not be.-->
|alt                = 
|caption            = 
|order              = 
|office      = Member of the [[Bundestag]]
|term_start  = [[German federal election, 2013|2013]]
|term_end           = 
|alongside          = <!--For two or more people serving in the same position from the same district.  (e.g. United States Senators.)-->
|birth_date         = {{Birth date and age|df=yes|1984|04|29}}
|birth_place        = [[Weimar]], [[Thuringia]], [[East Germany]]<br />{{small|(now Germany)}}
|death_date         = <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|Month DD, YYYY|Month DD, YYYY}} (death date then birth date) -->
|death_place        = 
|restingplace       = 
|restingplacecoordinates = 
|birthname          = 
|citizenship        = German
|nationality        = Germany
|party              = [[Christian Democratic Union (Germany)|CDU]]
|spouse             = 
|relations          = 
|children           = 
|residence          = 
|alma_mater         = {{unbulleted list |[[University of Jena]] |[[Kansai Gaidai University]] |[[Yale University]]}}
|occupation         = Politician
|profession         = 
|cabinet            = 
|committees         = 
|portfolio          = 
|religion           = 
|signature          = 
|signature_alt      = 
|website            = 
|footnotes          = 
}}
'''Mark Lars Carsten Hauptmann''' (born April 29, 1984 in Weimar/Thuringia) is a German politician and member of the [[Christian Democratic Union of Germany ]](CDU) and since 2013 member of the 18th [[German Bundestag]].

==Early life and education==
After receiving his Senior Highschool Degree in 2003 in the city of [[Suhl]], Hauptmann studied political science, business communication, economic and social history at [[University of Jena|Friedrich Schiller University in Jena]] (FSU), [[Kansai Gaidai University]] in Osaka, Japan, and [[Yale University]] in New Haven, USA. During his studies he was granted scholarships by the [[Konrad Adenauer Foundation]] and the Japan Student Services Organization (JASSO).

Following his graduation in 2009, Hauptmann started working as head of office and parliamentary assistant to Christian Hirte, MP (CDU) at the German Bundestag in Berlin. Additionally, he became a visiting lecturer to the [[University of Erfurt]] (2010) and also to the Friedrich-Schiller University in Jena (2011), where he was teaching political science.

Hauptmann worked at the [[European Parliament]] in Brussels, Belgium, for the [[Konrad Adenauer Foundation]] in Beijing, China, as well as the Thuringia's State Ministry of Transportation, Construction and Economic Development. With ''Kairos Communication'' he also started his own business.

Hauptmann is a member of the [[Evangelical Church in Germany]].

==Political career==
In 1999 Hauptmann joined the youth organization of the Christian Democratic Union, the Junge Union Deutschland (JU). There, he chaired the JU Suhl and served as deputy chair of JU's regional group for the Rhön-Rennsteig area, among others. In 2005, he was elected a board member of Junge Union in the State of Thuringia. He was chairman of the permanent workshop on European and International Affairs. On federal level, he represented JU Thuringia until 2012. Since then, he became a member of the federal board of the "Junge Union Germany" and was appointed chairman of JU's International Commission on Foreign, European and Security Affairs until 2014.
Joining the CDU in 2003, Hauptmann soon got elected to the board of the CDU in Suhl and joined the CDU Thuringia' state committee on European Affairs.

In of the [[European Parliament election, 2009|2009 European elections]], Hauptmann ran for a seat as participant of the CDU's state list for Thuringia.

===Member of the Bundestag, 2013–present===
Hauptmann was unanimously elected CDU direct candidate for the constituency 197 (Suhl - Schmalkalden-Meiningen - Hildburghausen) for the [[German federal election, 2013|2013 German elections]]. With 42,0% of the votes, he won his constituency and became a directly elected Member of the German Bundestag.

Hauptmann is a member of the Committee on Economic Affairs and Energy as well as member of its Subcommittee on Regional Economic Policy and ERP Economic Plans. In this capacity, he serves as his parliamentary group’s rapporteur on the annual budget of the [[Federal Ministry for Economic Affairs and Energy]], the economic reconstruction of eastern Germany and social policy. He is furthermore a deputy member of the Committee on Foreign Affairs and the NATO (North Atlantic Treaty Organization) Parliamentary Assembly as well as member of the Subcommittee on Civil Crisis Prevention, Conflict Management, and Coordinated Response.

==Honors and awards==
Hauptmann is a member of the debate society Jena. In 2006, he won the German national debate tournament and became vice champion in 2007.

==References==
<ref>http://www.bundetag.de/bundestag/abgeordnete18/biografien/H/hauptmann_mark.html</ref>
<ref>http://www.cducsu.de/abgeordnete/mark-hauptmann</ref>
<ref>http://www.abgeordnetenwatch.de/mark_hauptmann-602-20958.html</ref>
{{reflist}}

==External links==

*{{official website}}

{{Authority control}}
{{DEFAULTSORT:Hauptmann, Mark}}
[[Category:1984 births]]
[[Category:Living people]]
[[Category:Christian Democratic Union of Germany politicians]]