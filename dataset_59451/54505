{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Alice in Blunderland: An Iridescent Dream
| title_orig   =
| translator   =
| image         = Alice-in-blunderland-cover-1907.png<!--prefer 1st edition - It is the first edition-->
| caption      = Front cover of the first edition
| author       = [[John Kendrick Bangs]]
| illustrator  = [[Albert Levering]]<ref name=LCC/>
| cover_artist = Levering
| country       = United States

| genre        = [[Fantasy novel]], [[parody]]
| publisher    = [[Doubleday (publisher)|Doubleday, Page & Co.]]
| pub_date     = 1907
| pages        = viii, 124<ref name=LCC/>
| oclc         = 191114417
| congress     = PS1064.B3 A7 1907<ref name=LCC>[https://lccn.loc.gov/07029167 "Alice in Blunderland, an iridescent dream"] (1st ed.). LC Online Catalog. Library of Congress. Retrieved 2016-08-29.</ref> 
}}

'''''Alice in Blunderland: An Iridescent Dream''''' is a novel by [[John Kendrick Bangs]]. It was first published in 1907 by Doubleday, Page & Co. of New York, with illustrations by Albert Levering. It is a political [[parody]] of the two Alice books by Lewis Carroll, ''[[Alice's Adventures in Wonderland]]'' and ''[[Through the Looking-Glass]]''.

It is critical of economic issues such as taxation, corporate greed, and corruption. Instead of entering [[Wonderland (fictional country)|Wonderland]], Alice finds herself in "Blunderland", which is also described as "Municipal Ownership Country".

The book features line drawings by Albert Levering 1869–1929), after the original Alice illustrations by [[John Tenniel]].

In 1902, Bangs had written a book inspired by ''Alice's Adventures in Wonderland'', called ''[[Rollo in Emblemland]]''.

Part of ''Alice in Blunderland'' was previously published as "Alice in Municipaland" in ''Concerning Municipal Ownership'', vol. 1 no. 10 (December 1906), pp. 182–87 (perhaps the first chapter of the book).<ref>[http://www.worldcat.org/oclc/225792154 "Alice in Municipaland : an iridescent dream"]. [[WorldCat]]. Retrieved 2016-08-29.</ref>

==Plot==

Alice travels to Blunderland where nothing is supposed to be, children live in the Municipal House of the Children and the Duchess and the City are their parents. We find in this novel all the familiar characters made famous by ''[[Alice in Wonderland]]'': the dormouse, [[the Mad Hatter]], the Cheshire Cat and others.<ref>{{cite web|url=http://www.barnesandnoble.com/w/alice-in-blunderland-an-iridescent-dream-john-kendrick-bangs/1100283588|title=Alice in Blunderland: An Iridescent Dream|publisher=Barnes and Noble|accessdate=February 19, 2013}}</ref>

{{clear}}
==Contents==

# Off to Blunderland 3
# The Immovable Trolley 19
# The Aromatic Gas Plant 37
# The City-owned Police 56
# The Municipaphone 73
# The Department of Public Verse 92
# Ownership of Children 108

==Bibliography== 
[[Image:Alice-in-blunderland-cover-2010.png|150px|thumb| Cover of the 2010 Evertype edition ]]

*Bangs, John Kendrick (2010) [http://www.evertype.com/books/alice-bangs.html ''Alice in Blunderland: An Iridescent Dream'']. Mhaigh Eo, Éire [County Mayo]: Evertype. ISBN 978-1-904808-56-5. {{LCCN|2014378531}}

==References==
{{Reflist|25em}}

==External links==
* {{librivox book | title=Alice in Blunderland: An Iridescent Dream | author=John Kendrick BANGS}}
* Google Books: [http://books.google.co.cr/books/about/Alice_in_Blunderland.html?id=3cc-AAAAYAAJ&redir_esc=y Alice in Blunderland: An Iridescent Dream]
* [[Dave Pitts (Figure Skater)|David Pitts and Spanky in Ice Capades' "Alice in Blunderland"]]
* {{LCAuth|n95000056|Albert Levering|37|}}

{{alice}}

[[Category:1907 novels]]
[[Category:Books based on Alice in Wonderland]]
[[Category:British fantasy novels]]
[[Category:1900s fantasy novels]]
[[Category:Doubleday, Page & Company books]]


{{1900s-fantasy-novel-stub}}