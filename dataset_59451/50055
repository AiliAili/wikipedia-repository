{{Redirect|Littlefinger|the pinky finger|Little finger}}
{{Infobox character
| series    = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| name      = Petyr Baelish
| image     = File:Aidan Gillen playing Petyr Baelish.jpg
| caption   = [[Aidan Gillen]] as Petyr Baelish
| creator   = [[George R. R. Martin]]
| first     = '''Novel''': <br>''[[A Game of Thrones]]'' (1996) <br>'''Television''': <br>"[[Lord Snow]]" (2011)
| last      =
| title     = Master of Coin<br>Lord of Harrenhal<br>Lord Protector of the Vale<br>Lord Paramount of the Trident {{small|(books only)}}
| alias     = Littlefinger
| gender    = Male
| portrayer = [[Aidan Gillen]]<br>(''[[Game of Thrones]]'')
| spouse    = [[Lysa Arryn]]
| children  = 
| lbl21     = Kingdom
| data21    = [[Riverlands (A Song of Ice and Fire)|The Riverlands]]<br>[[World of A Song of Ice and Fire#The Vale of Arryn|The Vale]]
}}

'''Petyr Baelish''', nicknamed '''Littlefinger''', is a [[fictional character]] created by American author [[George R. R. Martin]]. He is a prominent non-[[Narrative mode#Third-person view|point of view]] character in Martin's award-winning ''[[A Song of Ice and Fire]]'' series, and a main character in [[HBO]]'s adaptation of the series, ''[[Game of Thrones]]'', where he is portrayed by [[Aidan Gillen]]. In ''[[A Feast for Crows]]'' it is revealed that several major plot points have hinged on Baelish's intrigues, including the framing of [[Tyrion Lannister]] for the attempt on [[Bran Stark]]'s life, the downfall of Lord [[Ned Stark|Eddard Stark]], the deaths of Lord [[Jon Arryn]] and King [[Joffrey Baratheon]], and the [[War of the Five Kings]].

==Character==

=== Background ===
Petyr descends from a Braavosi sellsword who served House Corbray. His father befriended Lord Hoster Tully during the War of the Ninepenny Kings, and Tully took the young Petyr as a [[ward (law)|ward]]. Petyr grew up at House Tully's castle [[Riverrun (A Song of Ice and Fire)|Riverrun]] with Hoster's daughters [[Catelyn Stark|Catelyn]] and [[Lysa Arryn|Lysa]] and his son Edmure, the latter of whom gave Petyr the nickname "Littlefinger" as a reference to his short stature and his family's lands on the smallest of the Fingers. He was a sly, mischievous child with the ability to always look contrite after his mischief.<ref name="A Game of Thrones: Chapter 18">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_18 | title=A Game of Thrones: Chapter 18, Catelyn IV}}</ref> He became infatuated with Catelyn and claims to have lost his virginity to her while drunk; in reality, he lost it to Lysa, who was herself obsessed with him. When Catelyn was betrothed to Brandon Stark ([[Ned Stark|Eddard Stark]]'s elder brother), Petyr brazenly challenged him to a [[duel]] for her hand in marriage but lost easily to Brandon. His life was spared at the behest of Catelyn.<ref name="A Game of Thrones: Chapter 40">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_40 | title=A Game of Thrones: Chapter 40, Catelyn VII}}</ref> During his convalescence, he impregnated Lysa but the [[pregnancy]] ended in a forced [[abortion]] and Petyr was banished from Riverrun.

Even after her marriage to Jon Arryn, Lysa remained in love with Petyr. Her patronage saw Baelish appointed as customs officer at Gulltown, a position he excelled at. Arryn, who was Hand to King Robert Baratheon, eventually brought Baelish to King's Landing as Master of Coin. When Arryn tries to have his and Lysa's son Robert sent to Dragonstone to be fostered by Stannis Baratheon, Baelish gives Lysa poison to kill Jon and convinces her to tell Catelyn that House Lannister was responsible. This subterfuge sets in motion the main events of the series.

=== Appearance and personality ===
Petyr Baelish is a small man, shorter than Catelyn Stark.<ref name="A Game of Thrones: Chapter 18"/> He is slender and quick with sharp features and laughing gray-green eyes.<ref name="A Game of Thrones: Chapter 18"/> He has a pointed chin beard and threads of silver in his hair.<ref name="A Game of Thrones: Chapter 18"/>  He often wears a silver mockingbird to fasten his cloak.<ref name="A Game of Thrones: Chapter 18"/> He clearly enjoys the intrigues of court life at King's Landing.<ref name="A Game of Thrones: Chapter 20">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_20 | title=A Game of Thrones: Chapter 20, Eddard IV}}</ref> He is dangerously intelligent<ref name="A Clash of Kings: Chapter 15">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Clash_of_Kings-Chapter_15 | title=A Clash of Kings: Chapter 15, Tyrion III}}</ref> and cunning enough to hide his machinations.<ref name="A Game of Thrones: Chapter 32">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_32 | title=A Game of Thrones: Chapter 32, Arya III}}</ref> Since Petyr is not a POV character his motivations are somewhat of a mystery. However, his methods have been totally ruthless, including lies, treachery, and murder to achieve his goals.<ref name="A Storm of Swords: Chapter 61">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Storm_of_Swords-Chapter_61 | title=A Storm of Swords: Chapter 80, Sansa V}}</ref><ref name="A Storm of Swords: Chapter 80">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Storm_of_Swords-Chapter_80 | title=A Storm of Swords: Chapter 80, Sansa VII}}</ref>

==Storylines==

===Book series===

====A Game of Thrones====
Baelish hides Catelyn at one of his [[brothel]]s when she brings the news of the attempt on [[Bran Stark|Bran]]'s life, and tells her the [[dagger]] used was won from him by Tyrion Lannister.<ref name="A Game of Thrones: Chapter 18"/> This leads to Catelyn's capture of Tyrion; but this information is later identified as a lie.<ref name="A Game of Thrones: Chapter 31">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_31 | title=A Game of Thrones: Chapter 31, Tyrion IV}}</ref> Petyr helps Eddard expose the secret parentage of the royal children,<ref name="A Game of Thrones: Chapter 25">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_25 | title=A Game of Thrones: Chapter 25, Eddard V}}</ref><ref name="A Game of Thrones: Chapter 33">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_33 | title=A Game of Thrones: Chapter 33, Eddard VIII}}</ref><ref name="A Game of Thrones: Chapter 35">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_35 | title=A Game of Thrones: Chapter 35, Eddard IX}}</ref> but advises him to abet Joffrey's rise to power in order to consolidate their own. Ned insists that Stannis must be king and asks Baelish to secure the help of the City Watch when he moves against the Lannisters, but Petyr betrays Lord Stark<ref name="A Game of Thrones: Chapter 49">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_49 | title=A Game of Thrones: Chapter 49, Eddard XIV}}</ref> and aids his arrest.

====A Clash of Kings====
After the death of [[Renly Baratheon]], Petyr arranges an alliance between the Lannisters and the powerful [[House Tyrell]], which leads to [[Stannis Baratheon]]'s defeat. When Joffrey is convinced to marry Renly's widow, [[Margaery Tyrell|Margaery]], in ''[[A Storm of Swords]]'', Petyr is named Lord of [[Harrenhal]] and [[Lord Paramount]] of the Trident in place of [[Edmure Tully]].

====A Storm of Swords====
Baelish is given charge to marry Lysa Arryn and bring the Vale under the control of the Lannisters;<ref name="A Storm of Swords: Chapter 19">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Storm_of_Swords-Chapter_19 | title=A Storm of Swords: Chapter 19, Tyrion III}}</ref> before departing he reveals [[Olenna Tyrell]]'s plan to marry Sansa to [[Willas Tyrell]],<ref name="A Storm of Swords: Chapter 19"/> leading to Sansa's forced marriage to Tyrion. When Joffrey is poisoned at his wedding feast, Petyr has Sansa taken from King's Landing in the confusion and takes her to his holdings on The Fingers disguised as his illegitimate daughter.<ref name="A Storm of Swords: Chapter 61"/> During the voyage, Petyr reveals that he had conspired with Olenna Tyrell to poison Joffrey and blame Tyrion for the murder. Lysa arrives at Petyr's keep and the two are promptly married at Lysa's insistence, though Petyr would prefer a court wedding.<ref name="A Storm of Swords: Chapter 68">{{cite web | url=http://awoiaf.westeros.org/index.php/A_Storm_of_Swords-Chapter_68 | title=A Storm of Swords: Chapter 68, Sansa VI}}</ref> After the wedding Baelish and Sansa travel to [[the Eyrie]], where he spends much of the time asserting his new authority over the lords of the Vale. While alone with Sansa, he notes her physical similarity to Catelyn and kisses Sansa. Witnessing the encounter, Lysa tries to kill Sansa a short time later. Petyr rushes to comfort Lysa, but reveals that he had only ever loved Catelyn and pushes Lysa to her death.<ref name="A Storm of Swords: Chapter 80"/> He immediately implicates the only other witness, the [[minstrel]] Marillion.<ref name="A Storm of Swords: Chapter 80"/>

====A Feast for Crows====
Petyr names himself Lord Protector and claims Lysa's son [[Robert Arryn|Robert]] as his ward. The Lords of the Vale try to claim Robert, but Petyr bribes Lyn Corbray to help Petyr obtain leverage to keep Robert. Petyr later reveals to Sansa that he plans to either eliminate the lords or win them to his side, and to help Sansa regain [[The North (A Song of Ice and Fire)|the North]]. He also tells her that he has arranged for her to marry Robert Arryn's cousin and heir, Harrold Hardyng, and that on Robert's death she will reveal herself and use the Knights of the Vale to recover the North.

===Television series===
[[File:A Song of Ice and Fire arms of House Arryn blue scroll.png|right|thumb|150px|alt=A coat of arms showing a white falcon flying out of a white moon on a sky blue field|Coat of arms of House Arryn]]
Baelish's storyline remains much the same as in the books for the first few seasons of the television series, with only minor details changed.

====Season 3====
Having become Lord of Harrenhal, Petyr plans to sail to the Eyrie to offer Lysa Arryn a marriage proposal, taking Sansa with him. One of his prostitutes, Ros, learns of his plan and warns Varys, who arranges with Olenna Tyrell to have Sansa wed to Loras Tyrell. Baelish's spy Olyvar, who is posing as Loras' squire and lover, tips Baelish off to the plot; Baelish in turn gives this information to Tywin Lannister, who has Sansa wed to Tyrion instead. Realising that Ros has betrayed him, Baelish hands her over to Joffrey to kill for his entertainment.

====Season 4====
In the aftermath of Lysa Arryn's death, Baelish is questioned by several lords of the Vale. Baelish maintains that Lysa committed suicide, and Sansa corroborates this assertion. Baelish decides to take Robin Arryn on a tour of his domain, with Sansa accompanying them.

====Season 5====
Baelish brokers a marriage alliance between Sansa and Ramsay Bolton, the sadistic son of the new Warden of the North Roose Bolton. Although Baelish assures Roose that the marriage alliance will strengthen their respective positions, Baelish privately tells Sansa that Stannis Baratheon is marching on Winterfell and will likely defeat the Boltons in battle. However, before Sansa and Ramsay's wedding, Cersei summons Baelish to King's Landing to ascertain his loyalties. Baelish reassures Cersei of the Vale's allegiance to the Lannisters and tells her of Sansa's marriage to Ramsay, neglecting to reveal his role in arranging the marriage. Cersei is outraged, and Baelish offers to use the Vale's forces to defeat whoever is left of the Bolton and Baratheon armies following the battle, revealing that his true plan all along was to be named Warden of the North, a request Cersei grants. Before his departure Baelish meets with Olenna Tyrell, who is furious that testimony from Olyvar has led to the arrest of Margaery and Loras Tyrell. When Olenna threatens to reveal Baelish's role in Joffrey's murder, Baelish arranges for Lancel Lannister to tell religious leader the High Sparrow of Cersei's crimes, leading to her arrest.

====Season 6====
Baelish reunites with Robin Arryn at Runestone and claims that Sansa was abducted by the Boltons, before manipulating him into sending the Vale's forces to defend her. Baelish later meets Sansa in Mole's Town, insisting that he was unaware of Ramsay's cruelty. He offers the support of the knights of the Vale in retaking Winterfell and mentions that Sansa's great-uncle Brynden "Blackfish" Tully has seized Riverrun from the Freys; Sansa refuses his offer and declares that she never wants to see him again. However, after Sansa and [[Jon Snow (character)|Jon Snow]] are unable to gather enough men to match the Boltons, Sansa sends a raven to Baelish asking for his aid. Baelish leads the men on Winterfell, and they arrive in time to destroy the unsuspecting Bolton army before they can slaughter the Starks. After the battle, Baelish reveals to Sansa that his ambition is to take the Iron Thrones with her at his side, but she rebuffs his advances. Baelish is present when the Northmen and Valemen declare Jon King in the North, but instead of cheering for Jon, he glares at Sansa.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ign.com/articles/2016/06/27/game-of-thrones-the-winds-of-winter-review |archivedate=August 17, 2016 |url=http://www.ign.com/articles/2016/06/27/game-of-thrones-the-winds-of-winter-review |title=Game of Thrones: "The Winds of Winter" Review |publisher=[[IGN]] |accessdate=December 16, 2016 |date=June 27, 2016 |author=Fowler, Matt|deadurl=no}}</ref>

==Reception==
{{Undue weight section|date=October 2016}}
Writing for ''[[The Huffington Post]]'', Gil Kidron described Petyr as "gross...creepy...evil" and stated "you should root for him to win." Kidron claims "in Westeros he's a [[We are the 99%|99 percenter]]." Kidron writes Petyr is "doing it for himself", but "he's working tirelessly to destroy an old and decaying world that none of us would want to live in."<ref>{{cite web |url=http://www.huffingtonpost.com/gil-kidron/why-you-should-root-for-littlefinger-to-win_b_7191588.html |title=Why You Should Root for Littlefinger to Win  |work=[[The Huffington Post]]|first=Gil |last=Kidron |date=May 4, 2015 |accessdate=May 26, 2015}}</ref>

==References==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Baelish, Petyr}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional secret agents and spies]]
[[Category:Fictional lords and ladies]]
[[Category:Fictional murderers]]
[[Category:Fictional government officials]]
[[Category:Fictional advisors]]
[[Category:Fictional uxoricides]]