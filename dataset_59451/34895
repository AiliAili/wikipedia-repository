{{Good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1925
| Track=1925 Atlantic hurricane season summary map.png
| First storm formed=August 18, 1925
| Last storm dissipated= December 1, 1925
| Strongest storm name=One
| Strongest storm winds=70
| Strongest storm pressure=994
| Damagespre=At least
| Damages=1.6
| Inflated=1
| Fatalities=≥59 total
| five seasons=[[1923 Atlantic hurricane season|1923]], [[1924 Atlantic hurricane season|1924]], '''1925''', [[1926 Atlantic hurricane season|1926]], [[1927 Atlantic hurricane season|1927]]
}}
The '''1925 Atlantic hurricane season''' was an inactive [[Atlantic hurricane season]] during which four [[tropical cyclone]]s formed. Only one of them was a hurricane. The first storm developed on August&nbsp;18, and the last dissipated on December&nbsp;1. The season began at a late date, more than two months after the season began. The official start of the season is generally considered to be June&nbsp;1 with the end being October&nbsp;31;<ref name="FAQG1">{{cite web|author=Neal Dorst|publisher=National Oceanic and Atmospheric Administration|year=1993|accessdate=March 9, 2009|title=When is hurricane season ?|url=http://www.aoml.noaa.gov/hrd/tcfaq/G1.html| archiveurl= https://web.archive.org/web/20090308083302/http://www.aoml.noaa.gov/hrd/tcfaq/G1.html| archivedate= 8 March 2009 <!--DASHBot-->| deadurl= no}}</ref> however, the final storm of the season formed nearly a month after the official end.<ref name="MWR"/> Due to increased activity over the following decades, the official end of the hurricane season was shifted to November&nbsp;30.<ref name="FAQG1"/>

The final two storms of the season impacted several areas, with the final storm affecting areas from Cuba to Rhode Island. The third storm caused little or no damage along the Texas coastline with gale-force winds being recorded only along the coast. The last storm caused severe damage along the beaches of the [[Florida|Florida Peninsula]], with damages estimated in the millions along with four fatalities near Tampa. At least $600,000 was lost in damages dealt to the citrus industry and several maritime incidents resulted in over 55 fatalities.<ref name="MWR"/>

__TOC__
{{Clear}}

==Timeline==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/08/1925 till:01/01/1926
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/08/1925

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:18/08/1925 till:21/08/1925 color:C1 text:"One"
  from:25/08/1925 till:27/08/1925 color:TS text:"Two"
  from:06/09/1925 till:07/09/1925 color:TS text:"Three"
  from:30/09/1925 till:30/09/1925 color:TD text:"TD"
  from:27/11/1925 till:01/12/1925 color:TS text:"Four"
  
  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/08/1925 till:01/09/1925 text:August
  from:01/09/1925 till:01/10/1925 text:September
  from:01/10/1925 till:01/11/1925 text:October
  from:01/11/1925 till:01/12/1925 text:November
  from:01/12/1925 till:01/01/1926 text:December
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

==Systems==

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1925 Atlantic hurricane 1 track.png
|Formed=August 18
|Dissipated=August 21
|1-min winds=70
|Pressure=<994
}}
The first indications of a tropical cyclone developing were on August&nbsp;17. A ship in the vicinity of the developing system reported winds of 40&nbsp;mph (65&nbsp;km/h) over 78&nbsp;°F (25.5&nbsp;°C) waters.<ref name="Hur1">{{cite web|publisher=National Oceanic and Atmospheric Administration|year=2009|accessdate=March 20, 2009|title=Raw Observations for Hurricane One 1925|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/192501.xls}}</ref> Around 0000&nbsp;[[Coordinated Universal Time|UTC]] the next day, the system was classified as a modern-day tropical depression with sustained winds estimated at 30&nbsp;mph (45&nbsp;km/h). Roughly 18&nbsp;hours later, the depression strengthened into a tropical storm, the first of the season while located to the north-northeast of the [[Bahamas]]. Gradual intensification took place throughout most of the storm's life<ref name="hurdat"/> as it traveled towards the northeast<ref name="RA"/> until becoming a hurricane around 0600&nbsp;UTC on August&nbsp;20.<ref name="hurdat"/> About 30&nbsp;minutes later, a ship recorded winds of 45&nbsp;mph (75&nbsp;km/h)<ref name="Hur1"/> and a pressure of 993.6&nbsp;mbar (hPa),<ref name="MWR"/> the lowest pressure recorded in relation to the small storm.<ref name="MWR"/><ref name="Hur1"/> Shortly after, the storm reached its peak intensity with winds of 80&nbsp;mph (130&nbsp;km/h).<ref name="hurdat"/> Increasing in forward motion, the storm became [[extratropical cyclone|extratropical]] early on August&nbsp;21 after turning towards the north.<ref name="hurdat"/><ref name="RA">{{cite web|author=Chris Landsea|publisher=National Hurricane Center|date=June 28, 2005|accessdate=March 8, 2009|title=The Atlantic Hurricane Database Reanalysis for 1911 to 1930|url=http://www.aoml.noaa.gov/hrd/hurdat/presentations/irr-19111930.ppt| archiveurl= https://web.archive.org/web/20090225212436/http://www.aoml.noaa.gov/hrd/hurdat/presentations/irr-19111930.ppt| archivedate= 25 February 2009 <!--DASHBot-->| deadurl= no}}</ref>
{{Clear}}

===Tropical Storm Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1925 Atlantic tropical storm 2 track.png
|Formed=August 25
|Dissipated=August 27
|1-min winds=35
|Pressure=<1009
}}
The second storm of the season was first identified on August&nbsp;25 to the east of Florida as a tropical depression.<ref name="hurdat"/> Around this time, several ships were reporting winds up to 25&nbsp;mph (35&nbsp;km/h) in the vicinity of the system.<ref name="Hur2">{{cite web|publisher=National Oceanic and Atmospheric Administration|year=2009|accessdate=March 20, 2009|title=Raw Observations for Tropical Storm Two 1925|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/192502.xls}}</ref> Traveling towards the northeast, the storm gradually intensified, attainting tropical storm status around 0600&nbsp;[[Coordinated Universal Time|UTC]] the next day.<ref name="hurdat"/> Several hours later, a ship recorded a pressure of 1010&nbsp;mbar (hPa) while located in the vicinity of the system.<ref name="Hur2"/> The storm turned towards the west-northwest later that day and forward motion began to increase.<ref name="hurdat"/>

Early on August&nbsp;27, the storm weakened below tropical storm status and transitioned into an [[extratropical cyclone]]<ref name="hurdat"/> while moving over cooler waters.<ref name="Hur2"/> The system dissipated shortly after over open waters.<ref name="hurdat"/> Although the storm remained over water for the duration of its existence, the outer reaches of the system brought rain and light winds to [[Georgia (U.S. State)|Georgia]], [[Florida]], and the [[Carolinas]]. In [[Jacksonville, Florida]], the storm produced 0.47&nbsp;in (11.9&nbsp;mm) of rain on August&nbsp;25. Between August&nbsp;26 and 27, [[Cape Hatteras]] received 2.06&nbsp;in (52.3&nbsp;mm) of rain from the storm.<ref name="Hur2"/>
{{Clear}}

===Tropical Storm Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1925 Atlantic tropical storm 3 track.png
|Formed=September 6
|Dissipated=September 7
|1-min winds=45
|Pressure=<1002
}}
The third storm of the season was first identified as tropical depression off the northwestern coast of the [[Yucatan Peninsula]] early on September&nbsp;6. The system moved at a steady pace to the northwest towards the [[Rio Grande Valley]]. Shortly after forming, the depression strengthened into a tropical storm.<ref name="hurdat">{{cite web|title=Atlantic Best Tracks, 1851 to 2007 Reanalyzed|year=2009|publisher=[[National Hurricane Center]]|accessdate=March 20, 2009|url=http://www.aoml.noaa.gov/hrd/hurdat/tra51to07-2125rean.txt}}</ref> At this time, a ship in the vicinity of the storm recorded a pressure of 1012&nbsp;mbar (hPa).<ref name="Hur3"/> By 1800&nbsp;[[Coordinated Universal Time|UTC]] that day, the storm reached its peak intensity with winds of 50&nbsp;mph (85&nbsp;km/h).<ref name="hurdat"/> Around 9 pm CST (0300&nbsp;UTC on September&nbsp;7) that night, storm warnings were issued for areas between [[Brownsville, Texas|Brownsville]] and [[Corpus Christi, Texas]]. These warnings were later expanded to include the entire Texas coastline.<ref name="MWR">{{cite web|author=W. P. Day|title=Monthly Weather Review for 1925|year=1926|publisher=National Weather Service|accessdate=March 8, 2009|url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1925.pdf|format=[[PDF]]| archiveurl= https://web.archive.org/web/20090225212811/http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1925.pdf| archivedate= 25 February 2009 <!--DASHBot-->| deadurl= no}}</ref>

The storm made landfall in northern Mexico, just south of the Texas border,<ref name="hurdat"/> shortly after the warnings were extended.<ref name="MWR"/> Around the time the storm made landfall, a pressure of 1002&nbsp;mbar (hPa) was recorded in Brownsville, Texas.<ref name="Hur3"/> The storm dissipated later that day over southwestern Texas.<ref name="hurdat"/> No known damage was caused as a result of this storm and storm-force winds were only recorded over a small area on the Texas coastline.<ref name="MWR"/> The storm produced minor rainfall over south Texas with Brownsville recording 0.56&nbsp;in (14.2&nbsp;mm) and Corpus Christi recording 0.95&nbsp;in (24.1&nbsp;in). Winds up to 43&nbsp;mph (69&nbsp;km/h) were reported in Brownsville around 1&nbsp;am CST (0800&nbsp;UTC) on September&nbsp;7.<ref name="Hur3">{{cite web|publisher=National Oceanic and Atmospheric Administration|year=2009|accessdate=March 21, 2009|title=Raw Observations for Tropical Storm Three 1925|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/192503.xls}}</ref>
{{Clear}}

===Tropical Storm Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1925 Hurricane 4 Weather Map.jpg
|Track=1925 Atlantic hurricane 4 track.png
|Formed=November 27
|Dissipated=December 1
|1-min winds=55
|Pressure=<996}}
{{Main article|1925 Florida tropical storm}}
The final storm of the season formed as tropical depression to the southeast of the [[Yucatan Peninsula]] early on November&nbsp;27,<ref name="hurdat"/> nearly a month after the official end of the hurricane season.<ref name="FAQG1"/> The depression drifted towards the southeast, gradually strengthening into a tropical storm<ref name="hurdat"/> over 80&nbsp;°F (26.6&nbsp;°C) waters. Around 1300&nbsp;[[Coordinated Universal Time|UTC]], a ship in the vicinity of the storm recorded winds up to 40&nbsp;mph (65&nbsp;km/h).<ref name="Hur4">{{cite web|publisher=National Oceanic and Atmospheric Administration|year=2009|accessdate=March 21, 2009|title=Raw Observations for Hurricane Four 1925|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/192504.xls}}</ref>  Once in the Gulf, forward motion picked up as it intensified to winds of {{convert|65|mph|km/h|abbr=on}}.<ref name="Meta">{{Cite web|author=Hurricane Research Division|publisher=National Hurricane Center|year=2011|accessdate=August 24, 2011|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_aug11.html}}</ref> During the night of November&nbsp;30, the storm made [[Landfall (meteorology)|landfall]] just south of [[Tampa, Florida]]; a pressure of 998.8&nbsp;mbar (29.5&nbsp;inHg) was recorded along with winds up to 52&nbsp;mph (87&nbsp;km/h).<ref name="MWR"/>

Not long after making landfall, the storm weakened and transitioned into an [[extratropical cyclone]] while over center Florida. The cyclone emerged over the eastern Atlantic Ocean several hours later and regained hurricane-force winds. By 0000&nbsp;UTC, the storm reached its peak intensity as an extratropical system with winds of 90&nbsp;mph (150&nbsp;km/h) and a minimum pressure of 980&nbsp;mbar (hPa).<ref name="hurdat"/> Around this time, the U. S. S. ''Patoka'', which was in the vicinity of the storm, recorded a pressure of 978.5&nbsp;mbar (28.9&nbsp;inHg).<ref name="MWR"/> The northeastward movement slowed shortly after crossing Florida.<ref name="hurdat"/> At around 6&nbsp;p.m. EST (2300&nbsp;UTC), it made landfall between [[Wilmington, North Carolina|Wilmington]] and [[Cape Hatteras]]<ref name="MWR"/> with winds equivalent to a minimal hurricane. Shortly after landfall, maximum sustained winds in the storm dropped below 75&nbsp;mph (120&nbsp;km/h).<ref name="hurdat"/> A strong [[High pressure area|area of high pressure]] located over the [[Canadian Maritimes]] caused the system to turn towards the east-southeast.<ref name="MWR"/> On December&nbsp;5, the storm weakened further to the equivalent of a tropical depression as the system began to move towards the south.<ref name="hurdat"/> The remnants of the storm continued towards the east, passing several hundred miles north of [[Bermuda]] later that day. Several days later, the cyclone was reported near the [[Azores]]. On December&nbsp;9, it passed neat [[Horta, Azores|Horta]] where a pressure of 992.7&nbsp;mbar (29.32&nbsp;inHg) was recorded along with winds of 40&nbsp;mph (65&nbsp;km/h). After passing through the islands, it merged with another cyclone over the North Atlantic.<ref name="MWR"/>

The storm caused significant property and crop damage along the Gulf Coast of Florida. Trees, power lines, and telegraph wires were uprooted or knocked down by high winds along the [[Suwannee River]]. Structures which were previously considered to be safe from storms, being over 100&nbsp;ft (30.4&nbsp;m) inland, sustained significant damage from what was likely [[storm surge]]. The beaches along the Atlantic coast also sustained considerable damages from the storm.<ref name="MWR"/> Four people were killed near Tampa in two separate incidents . The first occurred when a house collapsed on three men, pinning them to the ground. The second incident occurred after a woman ran outside her home and was struck by a tree limb.<ref>{{cite news|author=Staff Writer|publisher=''Morning Avalanche''|date=December 2, 1925|title=Four Lives Lost in Storm Off Tampa Coast}}</ref> In North Carolina, heavy rains and strong winds were reported along the coast. Near record high water rises were recorded around Wilmington.<ref>{{cite news|author=Associated Press|publisher=''The Morning News Review''|date=December 2, 1925|title=Tropical Storm Ravages Florida Coast}}</ref> [[Cape Hatteras]] was temporarily isolated from the surrounding areas as the high winds from the storm knocked down power lines throughout the area. Several buildings along the coast and numerous boats sustained considerable damage.<ref>{{cite news|author=Staff Writer|publisher=''The Zanesville Signal''|date=December 2, 1925|title=Middle Atlantic Coast is Swept by Furious Gale}}</ref>

Throughout Florida, there were no reports of fatalities but property loses were estimated in the millions, with $1&nbsp;million in [[Jacksonville, Florida|Jacksonville]] alone. Damages to the citrus industry  were also significant, with total losses exceeding $600,000. In addition to the severe impacts on land, numerous shipping incidents resulted in several deaths. A schooner carrying seven people sunk with no survivors. A tug boat sank off the coast of [[Mobile, Alabama]] while towing a lumber barge, the fate of the crew is unknown. A ship named the American S. S. ''Catopazi'' sank between [[Charleston, South Carolina]] and the northern coast of [[Cuba]]; all 30 crew members drowned. A ship carrying about 2,000 cases of liquor with a crew of six sank near [[Daytona Beach]]. The last incident, involving a yacht, occurred off the coast of Georgia. The ship sank near [[Savannah, Georgia]] with the 12 crew members drowning. The total loss of life at sea was at least 55.<ref name="MWR"/>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]
*[[List of Florida hurricanes]]

==References==
{{Reflist|2}}

==External links==
{{Commons category|1925 Atlantic hurricane season}}
*[http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1925.pdf Monthly Weather Review]

{{TC Decades|Year=1920|basin=Atlantic|type=hurricane}}
{{1925 Atlantic hurricane season buttons}}

{{DEFAULTSORT:1925 Atlantic Hurricane Season}}
[[Category:1920–1929 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]