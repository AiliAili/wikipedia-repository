{{advert|date=March 2017}}
{{Infobox Organization
|name =                 Catholic Medical Mission Board
|image =                Logo CMMB.jpg
|abbreviation =         CMMB
|headquarters=          100 Wall Street, 9th Floor <br />
                        [[New York City|New York]], NY 10005
|founder =              Dr. Paluel Flagg
|formation =            1912-1928
|type =                 [[501(c)(3)]]
|focus =                Women’s and children’s health
|region_served =          Worldwide
|key_people =           Bruce Wilkinson ([[CEO]])<br /> John Celentano ([[Chairman|Board Chair]])
|slogan =               Healthier Lives Worldwide
|num_employees =        318 (2015)
|num_volunteers =       629 (2015)
|status =               Active
|website =              {{URL|www.cmmb.org}}
}}

The '''Catholic Medical Mission Board''' (CMMB) is an international, faith-based [[Non-governmental organization|NGO]], providing long-term, co-operative medical and development aid to communities affected by poverty and healthcare issues.<ref>m|Oppenheim TV, [http://www.moppenheim.tv/insight-bruce-wilkinson-catholic-medical-mission-board/ "INSIGHT: Bruce Wilkinson – Catholic Medical Mission Board"], Interview. September 2, 2015</ref>  It was established in 1912 and officially registered in 1928. CMMB is headquartered in [[New York City]], USA and currently has country offices in [[Haiti]], [[Kenya]], [[Peru]], [[South Sudan]] and [[Zambia]].<ref name="Flagg">Tom Gallagher, [http://ncronline.org/news/global/catholic-organization-exports-us-health-care-poor-around-world "Catholic organization exports US health care to the poor around the world"], ''National Catholic Reporter''. January 31, 2013</ref>

CMMB’s health programs include [http://www.cmmb.org/our-approach/champs/ Children and Mothers Partnerships] (CHAMPS), shipping of medical supplies, placement of international medical and non-medical volunteers,<ref name="WHO">WHO, Members, [http://www.who.int/pmnch/about/members/database/cmmb/en/ CMMB]. Retrieved on 03/03/2016</ref> [[HIV/AIDS]],<ref name="Give">Give.org, [http://www.give.org/charity-reviews/national/health/catholic-medical-mission-board-in-new-york-ny-2232 Catholic Medical Mission Board]. Retrieved on 06/22/2016</ref><ref>amFAR, [http://copsdata.amfar.org/s/Catholic%20Medical%20Mission%20Board Welcome]. Retrieved on 07/05/2016</ref> prevention and treatment of [[neglected tropical diseases]],<ref name="Berkley">Georgetown University, Berkley Center, [http://berkleycenter.georgetown.edu/organizations/catholic-medical-mission-board Catholic Medical Mission Board]. Retrieved on 03/03/2016</ref> and disaster relief to areas that experienced natural or political catastrophes.<ref name="Charity Navigator">Charity Navigator, [http://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=7635#.Vqu3SZorJdh Catholic Medical Mission Board]. Retrieved on 03/15/2016</ref>
In 2015, CMMB provided health services to 680,000 people, including 200,000 pregnant women and children under age of five, trained 3,843 health workers and placed 1,200 volunteers.<ref>Valentina Ieri, [http://www.ipsnews.net/2016/03/ngo-pledges-500-million-towards-sustainable-development-goals/ "NGO Pledges $500 Million Towards Sustainable Development Goals"]. Retrieved on 06/22/2016</ref>
CMMB actively works with the U.S. government on programming, such as the [[President's Emergency Plan for AIDS Relief]] (PEPFAR),<ref name="PEPFAR">Relief Web, [http://reliefweb.int/report/world/cmmb-celebrates-saving-more-700000-lives-globally-through-aidsrelief-program "CMMB Celebrates Saving More than 700,000 Lives Globally through the AIDSRelief Program"], published 08/12/2013.</ref><ref>Institute of Human Virology, [http://www.ihv.org/Global-Programs/AIDSRelief-Consortium.html AIDSRELIEF CONSORTIUM]. Retrieved on 07/06/2016</ref> [[United States Agency for International Development|USAID]],<ref name="USAID"/> [[Centers for Disease Control and Prevention|CDC]]<ref>CDC, [https://npin.cdc.gov/organization/catholic-medical-mission-board Caotholic Medical Mission Board Health Programs]. Retrieved on 07/06/2016</ref> multilateral donors including [[UNICEF]],<ref>UNICEF, [http://www.unicef.org/aids/3823_31411.html "UNICEF and the Catholic Medical Mission Board join forces to fight AIDS"], updated 03/02/2006.</ref>  [[United Nations High Commissioner for Refugees|UNHCR]] <ref>ReliefWeb, [http://reliefweb.int/report/south-sudan/south-sudan-unhcr-operational-update-no-262015-2-7-july-2015 South Sudan UNHCR Operational Update no. 26/2015, 2-7 July 2015]. Published 07/07/2015</ref> and [[Pan American Health Organization|PAHO]],<ref name="Give"/> and [[public-private partnerships]], including the Survive and Thrive Global Development Alliance.<ref>The Survive and Thrive Global Development Alliance, [https://sustainabledevelopment.un.org/partnership/?p=11932 Progress Reports]. Retrieved on 07/06/2016</ref>

==Programs==

===CHAMPS===
[http://www.cmmb.org/our-approach/champs/ Children and Mothers Partnerships] (CHAMPS) is CMMB's long-term initiative to address the leading causes of maternal and child death, disability and illness, including [[diarrhea]], [[pneumonia]], [[malaria]] and HIV. The program works both at community and clinical levels throughout Africa, [[Latin America]] and the [[Caribbean]].<ref>Every Woman Every Child, [http://www.everywomaneverychild.org/commitments/all-commitments/CMMB%20 Catholic Medical Mission Board (CMMB)]. Retrieved on 06/22/2016</ref> It addresses common causes of poor health, including access to clean water, healthcare facilities and medicine supply, [[sanitation]], agriculture/nutrition, social justice and economic development.<ref>Valentina Ieri, [http://www.globalissues.org/news/2016/03/25/21954 "NGO Pledges $500 Million Towards Sustainable Development Goals"], United Nations. Published on 03/25/2016</ref>

===Healing Help and Volunteer Programs===
Delivering volunteers and medical supplies to resource-poor areas are two legacy programs that have existed throughout the history of CMMB.<ref name="CVN">Catholic Volunteer Network, [https://catholicvolunteernetwork.org/program/medical-volunteer-program-catholic-medical-mission-board CMMB Volunteer Program]. Retrieved on 06/22/2016</ref> The Healing Help<ref>GuideStar, [http://www.guidestar.org/profile/13-5602319 Catholic Medical Mission Board]. Retrieved on 03/08/2016</ref> pharmaceutical donation program delivers and distributes donated medicines and medical supplies free of charge and without [[discrimination]]. Over the last eight years, CMMB has provided over US$2 billion worth of donated medicines to its trusted local healthcare partners in 120 countries.<ref>Philanthropy News Digest, [http://philanthropynewsdigest.org/news/cmmb-pledges-500-million-for-un-s-sustainable-development-goals "CMMB Pledges $500 Million for UN’s Sustainable Development Goals"]. Published on 03/22/2016.</ref> CMMB’s volunteer program provides medical and business expertise for community development at faith-based facilities in Haiti, Kenya, Peru, South Sudan, Zambia and other locations.<ref name="WHO"/><ref name="CVN"/>

===HIV/AIDS===
CMMB provides [[HIV/AIDS]] care, treatment and support across all age groups and genders. Since 2003, CMMB has been a part of the U.S. government-funded AIDS Relief care and treatment program (PEPFAR).<ref>PEPFAR, [http://www.pepfar.gov/press/77502.htm New Partner Organizations: First Round] (Updated January 2009). Retrieved on 03/08/2016</ref> The program has helped more than 700,000 people living with HIV worldwide as of 2013.<ref name="PEPFAR"/> 
CMMB partnered with the [[Southern African Catholic Bishops Conference]] (SACBC) on the HIV/AIDS relief program, ''Choose To Care'', supported by funding from the Bristol-Myers Squibb Foundation and other sources, which assisted more than 140 community-based initiatives in South Africa, Namibia, Swaziland, Lesotho and Botswana.<ref name="UNAIDS">UNAIDS Best Practices. [http://data.unaids.org/pub/Report/2007/jc1281_choose_to_care_en.pdf A Faith-Based Response to HIV in Southern Africa: the Choose to Care Initiative]. December, 2006.</ref>

==History==

The history of CMMB (known as a committee of the [[Catholic Health Association of the United States|Catholic Hospital Association]] until 1927) dates back to 1912, when a personal tragedy inspired CMMB’s founder, Dr.  Paluel J. Flagg to commit to medical [[missionary]] work. On his first international volunteer trip, Dr. Flagg ministered to [[leprosy]] patients in Haiti.<ref name="Flagg"/>

For many decades, CMMB prioritized the shipping of medical supplies and equipment to missionaries and health partners around the world.<ref>Edward F. Garesche, [http://epublications.marquette.edu/cgi/viewcontent.cgi?article=1052&context=lnq The Catholic Medical Mission Board and its Work], ''The Linacre Quarterly'', Volume 2, Number 4. September 1934</ref> In 1949, [[Fulton J. Sheen|Bishop Fulton J. Sheen]], a prominent preacher, television personality and national director of the [[Society of the Propagation of the Faith]] – joined CMMB’s board and served for 26 years.<ref name="Flagg"/>

Dr. [[Tom Catena]], American surgeon and Christian missionary,<ref>Alex Perry, [http://world.time.com/2012/04/25/alone-and-forgotten-one-american-doctor-saves-lives-in-sudans-nuba-mountains/ Alone and Forgotten, One American Doctor Saves Lives in Sudan’s Nuba Mountains], ''Time''. April 25, 2012</ref> has volunteered with CMMB since 1999, working on multiple projects in [[Guyana]], Honduras, Kenya, Nairobi, and southern Sudan.<ref>Gordon Morton, [http://www.brownalumnimagazine.com/content/view/3380/40/ On A Mission], ''Brown Alumni Magazine''. 2013, March/April Issue</ref>

In 2002, CMMB introduced ''Born To Live'',<ref name="Born To live">Georgetown University, Berkley Center, [http://berkleycenter.georgetown.edu/publications/first-nationwide-faith-based-initiative-to-fight-mother-to-child-transmission-of-hiv-launched-in-kenya First Nationwide Faith-based Initiative to Fight Mother-to-Child Transmission of HIV Launched in Kenya]. October 14, 2002</ref> a program designed for the prevention of  [[Unite for Children, Unite Against AIDS#Prevention of mother-to-child transmission (PMTCT)|mother-to-child (PMTCT) transmission of HIV]], affecting nearly 60,000 women in Haiti, Kenya and South Sudan.
 
In 2003, CMMB launched ''Action for Family Health'' to help reduce the mortality and morbidity rates of children in five Latin American and Caribbean countries, through a partnership with the [[Pan American Health Organization]] (PAHO), Catholic healthcare networks and the respective Ministries of Health in each country.<ref name="Berkley" /> CMMB provided [[deworming]] medicines 
<ref>PAHO, [http://www.paho.org/hq/index.php?option=com_docman&task=doc_view&gid=13723&Itemid=4031 "A Call to Action: Addressing Soil-transmitted Helminths in Latin America & the Caribbean"]. Retrieved on 06/22/2016</ref> to children in Haiti, [[Nicaragua]], Honduras, [[El Salvador]] and the [[Dominican Republic]].

In 2004, CMMB supported international and local partners in response to the [[2004 Indian Ocean earthquake and tsunami|tsunami disaster]], committing US$3.1 million in health aid to survivors in India, [[Indonesia]] and [[Sri Lanka]].<ref>Claudia McDonnel, [http://cny.org/stories/Tsunami-Aid,2702?content_source=&category_id=&search_filter=&event_mode=&event_ts_from=&list_type=&order_by=&order_sort=&content_class=&sub_type=stories&town_id "Tsunami Aid"]. Published on 02/03/2005</ref>

CMMB became a member of the AIDS Relief consortium in response to the [[President's Emergency Plan for AIDS Relief]] ([[George W. Bush|President Bush]]).<ref name="PEPFAR"/>

In 2010, CMMB provided over US$49 million and over 500 tons of pharmaceutical and medical supplies worth US$46.7 million to Haiti for [[Humanitarian response by non-governmental organizations to the 2010 Haiti earthquake|earthquake relief]]. CMMB distributed over 20,000 hygiene kits and 500,000 meals to survivors through its local partners.<ref name="FADICA">FADICA, [http://www.fadica.org/main/CatholicCompass/tabid/123/ctl/ArticleView/mid/454/articleId/39/Y-and-H-Soda-Foundation-and-Catholic-Medical-Mission-Board-Welcomed-to-FADICA-Membership.aspx Y and H Soda Foundation and Catholic Medical Mission Board Welcomed to FADICA Membership]. Retrieved on 06/22/2016</ref> CMMB also co-founded the Haiti Amputee Coalition, created after the earthquake to provide amputees with urgent medical care, high-quality [[prostheses]], food, shelter, physical and psycho-social therapy.<ref>Scott Alessi, [https://www.osv.com/OSVNewsweekly/Story/TabId/2672/ArtMID/13567/ArticleID/13805/Hope-for-Haiti.aspx "Hope for Haiti"], OSV Newsweekly. Published on 12/30/2013</ref>

In 2013, CMMB delivered medicine and medical supplies valued at more than US$10 million to survivors of [[Humanitarian response to Typhoon Haiyan|typhoon Haiyan]] (Yolanda) in the Philippines.

==Country Offices==

===Haiti===

CMMB’s work in Haiti goes back to 1912.<ref>Colum Wood, [http://www.autoguide.com/auto-news/2010/03/ferrari-458-italia-fetches-530000-at-auction-for-haiti-relief.html Ferrari 458 Italia Fetches $530,000 at Auction for Haiti Relief]. March 19, 2010</ref> CMMB focuses on providing treatment and medicine for people living with HIV/AIDS.<ref name="FADICA"/>

CMMB is building a primary healthcare facility, the Bishop Joseph M. Sullivan Center for Health, in [[Côtes-de-Fer]]. The new hospital will offer basic and emergency care for approximately 55,000 Haitians. The project was named after [[Joseph Michael Sullivan|Joseph M. Sullivan]], the Bishop of the [[Roman Catholic Diocese of Brooklyn|Diocese of Brooklyn]], New York, and a member of the CMMB's Board of Directors, who died in 2013.<ref>Catholic Health Association of the United States, [https://www.chausa.org/publications/catholic-health-world/article/august-1-2013/cmmb-to-use-challenge-grant-to-build-health-center-in-haiti "CMMB to use challenge grant to build health center in Haiti"]. Published on 08/01/2013</ref>

===Kenya===

Since 2003, CMMB has delivered comprehensive HIV care and treatment programs. In 2010, CMMB provided life-saving antiretroviral treatment to nearly 47,000 Kenyans. CMMB’s mentors helped more than 24,000 people living with HIV/AIDS<ref name="FADICA"/> adhere to their medical treatment and combat [[social stigma]] within their families and communities in Kenya, South Sudan and Haiti.

CMMB’s ''Women Fighting AIDS Kenya'' (WOFAK) is a community-based organization, providing prevention education, support groups, clinical and nutritional care to 15,000 women and 5,000 children each year.<ref>IAAT, [http://www.emtct-iatt.org/wp-content/uploads/2012/11/IATT_Community-Action-Best-Practices.pdf Communities taking action for women, mothers and children]. Retrieved on 03/03/2016</ref> CMMB provides voluntary [[Circumcision and HIV|medical male circumcision]] services<ref>CDC, [http://www.cdc.gov/mmwr/preview/mmwrhtml/mm6147a2.htm Progress in Voluntary Medical Male Circumcision Service Provision — Kenya, 2008–2011]. November 30, 2012</ref> as part of a comprehensive HIV-prevention package<ref>[https://www.malecircumcision.org/sites/default/files/document_library/Nyanza_Newsletter_052010.pdf Male Circumcision in a Comprehensive HIV Prevention Package], ''Nuanza Update'', p.3. May 2010</ref>  in [[Nairobi]] and [[Kisumu County]].

===Peru===

CMMB focuses on improving the health and nutrition of children under the age of five and pregnant women in under-served areas of Peru. CMMB trains health professionals and community healthcare workers, advises parents on child and family health and nutrition, provides [[Dietary supplement|nutritional supplementation]] to  malnourished children, institutes [[Public health surveillance|community health-surveillance]] and addresses issues of economic self-sufficiency through [[agriculture|agricultural]] programs.<ref name="hso.bonsecours.com">Bon Secours Health System, Press Release, [http://hso.bonsecours.com/news-and-events-press-releases.html?newsID=3B40D08F-BD36-45D0-B9EC-A095A4BE8151 CMMB Partners with Leading Catholic Health Care Networks to Launch Major Child Survival Program in Peru]. Retrieved on 03/03/2016</ref>

In 2010-2013, CMMB, Bon Secours Health System, [[CHRISTUS Health]], and [http://www.caritas.org.pe/ Caritas del Peru] partnered to implement the ''Unidos Contra la Mortalidad Infantil'' (United Against Infant Mortality) program.<ref name="hso.bonsecours.com"/> The program was designed to decrease morbidity and mortality in children under five years of age.

CMMB provides physical, social and psychological care for [[Developmental disability|mentally disabled]] children, which helps form a more supportive environment for these children, whom are often not accepted at their local schools.

===South Sudan===

CMMB’s work in South Sudan includes HIV/AIDS prevention, care and treatment, [[refugee]] health services, gender-based violence prevention, child protection and primary healthcare.

In 2009, CMMB established ''ANISA'', healthcare initiatives to support local HIV/AIDS program. ''ANISA'', meaning “together” in the [[Zande language]], aims to reduce the incidences of new HIV infections through HIV testing and counseling, and prevent PMTCT and sexual transmission of the disease.<ref>amfAR, [http://copsdata.amfar.org/mechanism/South%20Sudan/2013/12473 Detailed Mechanism Funding and Narrative]. Retrieved on 06/22/2016</ref> CMMB’s partner organization, World Vision, provides community outreach in HIV prevention for target audience of over 120,000 people annually.<ref>PEPFAR, [http://www.pepfar.gov/documents/organization/199709.pdf South Sudan. Operational Plan Report. FY 2011]. Retrieved on 06/22/2016</ref>

Since 2012, CMMB trains and provides support to birth attendants and nurses in safe delivery and infant health, as well as provided surgical and ambulance support services.<ref>Theresa Consoli, [http://www.academia.edu/13444550/Maternal_Mortality_in_South_Sudan_The_Safe_Motherhood_Project Maternal Mortality in South Sudan:The Safe Motherhood Project]. Retrieved on 06/22/2016</ref>

===Zambia===

CMMB focuses on improving access to services for maternal and child health, including a four-year program for prevention of mother-to-child transmission of HIV (PMTCT),<ref>KHN, BD, [http://khn.org/morning-breakout/dr00030049/ Catholic Medical Mission Board Launch HIV/AIDS Project in Zambia]. May 13, 2005</ref> increasing uptake of voluntary medical male circumcision (VMMC) and HIV counseling and testing (HCT),<ref name="USAID">USAID, [http://www.npi-connect.net/documents/592341/749044/NPI+SA+EOP+Session+1+Track+C+-+CMMB+Presentation Community and Government Networks’ Partnerships Improves the Referral Systems and Coordination of PMTCT and HCT Services in Zambia]. Retrieved on 06/22/2016</ref> implementing community-based HIV prevention programs, and preventing and treating malaria, TB and leprosy.

CMMB promotes male involvement<ref>NPI-Connect, [http://www.npi-connect.net/cmmbsuccess Transforming Men Into Leaders In The Fight Against HIV/AIDS]. Retrieved on 03/03/2016</ref> in antenatal clinics and responsibility for the health of the family under the ''Men Taking Action'' (MTA™) model,<ref>American Public Health Association, 141st APHA Annual Meeting Recordings, [https://apha.confex.com/apha/141am/webprogram/Paper284761.html Men as partners: Preventing HIV/AIDS and promoting gender equity with innovative male involvement in PMTCT and VCT services]. November 4, 2013</ref> including PMTCT, HCT services and [[Management of HIV/AIDS|antiretroviral]] (ARV) treatment at 31 participating Church Health Institutions.<ref>XIX International AIDS Conference, Washington DC, USA. [http://pag.aids2012.org/abstracts.aspx?aid=3646 Involvement of traditional community leaders improves uptake and community ownership of PMTCT and HCT services in rural areas of Zambia]. Retrieved on 03/08/2016</ref>

Between 2009-2012, CMMB managed the USAID-funded ''Malaria Communities Program'' under the [[President's Malaria Initiative]] (PMI) in the high transmission [[Luapula Province]].<ref>President’s Malaria Initiative, [http://www.pmi.gov/docs/default-source/default-document-library/malaria-operational-plans/fy12/zambia_mop_fy12.pdf?sfvrsn=8 Malaria Operational Plan (MOP)], p.19. September 19, 2011.</ref>

==Recognition==

*Ranked #3 on [[CNBC]]'s ''Top 10 Charities Changing the World in 2015''.<ref>CNBC, [http://www.cnbc.com/2015/12/01/the-top-10-charities-changing-the-world-in-2015.html?slide=9 The Top 10 Charities Changing The World]. Retrieved on 03/08/2016</ref>
*Ranked #33 on [[Forbes]] ''The 50 Largest U.S. Charities'' in 2015.<ref>''Forbes'', [http://www.forbes.com/companies/catholic-medical-mission-board/ Catholic Medical Mission Board On Forbes List]. Retrieved on 03/08/2016</ref>
*Ranked #62 on ''The Philanthropy 400'' in 2015.<ref>[https://philanthropy.com/items/biz/pdf/UpDown.pdf The Philanthropy 400], Info graphics. Retrieved on 03/08/2016</ref>
*Received Charity Navigator’s "4 Star Charity" ranking in 2015, for a fifth consecutive year.<ref name="Navigator">Charity Navigator, [http://www.charitynavigator.org/index.cfm?bay=search.history&orgid=7635 Catholic Medical Mission Board. Historical Ratings]. Retrieved on 07/07/2016</ref>
*Ranked #5 on Charity Navigator’s “10 of the Best Charities Everyone’s Heard Of”.<ref name="Navigator"/> 
*''[[BBB Wise Giving Alliance|“BBB Accredited Charity”]]'' in 2014.<ref>BBB Wise Giving Alliance, [http://www.give.org/charity-reviews/national/health/catholic-medical-mission-board-in-new-york-ny-2232 Catholic Medical Mission Board]. Retrieved on 03/08/2016</ref>
*Ranked #48 on ''The [[The NonProfit Times|NonProfit Times]] Top 100'' in 2012.<ref>The NonProfitTimes, [https://www.thenonprofittimes.com/wp-content/uploads/print/1351778098_2012_Top100.pdf The NPT 2012 Top 100]. November 1, 2012</ref>
*In 2006 UNAIDS recognized CMMB’s HIV/AIDS programs as a “best practice” in global health.<ref name="UNAIDS"/>  
*In 1989 won the [[Damien-Dutton Award]] that honored its work in the conquest of leprosy.<ref>[http://dev.worldpossible.org:81/wikipedia_de_all_2016-04/A/Damien-Dutton_Award.html Damien-Dutton Award]. Retrieved on 06/22/2016</ref>

==References==
{{Reflist}}

==External links==
* [http://www.cmmb.org/ Official Website]

{{Subject bar |portal1= Catholicism |portal2= Medicine |portal3= Health}}
{{DEFAULTSORT:CMMB}}
[[Category:Global health]]
[[Category:International medical and health organizations]]
[[Category:Religious organizations established in 1912]]
[[Category:Catholic charities]]
[[Category:Child-related organizations]]
[[Category:Children's charities]]
[[Category:Non-profit organizations]]
[[Category:Charities based in New York]]
[[Category:501(c)(3) nonprofit organizations]]
[[Category:International charities]]
[[Category:Christian organizations established in the 20th century]]
[[Category:Health charities in the United States]]
[[Category:Organizations based in New York City]]
[[Category:Non-profit organizations based in New York City]]
[[Category:Health-related fundraisers]]
[[Category:Children's charities based in the United States]]