{{Infobox journal
| title = Atlantic Geology
| abbreviation = Atl. Geol.
| formernames = Maritime Sediments, Maritime Sediments and Atlantic Geology
| editor = Sandra Barr, Rob Fensome, Simon Haslett, David West
| cover = [[File:Atlantic Geology journal cover.jpg]]
| discipline = [[Geology]]
| publisher = [[Atlantic Geoscience Society]]
| country = Canada
| history = 1965-present
| frequency =
| impact = 0.856
| impact-year = 2012
| website = http://journals.hil.unb.ca/index.php/ag
| link1 = http://journals.hil.unb.ca/index.php/ag/issue/current
| link1-name = Online access
| link2 = http://journals.hil.unb.ca/index.php/ag/issue/archive
| link2-name = Online archive
| ISSN = 0843-5561
| eISSN = 1718-7885
| OCLC = 612156124
| LCCN = 90648412
| CODEN = ATGEEB
}}
'''''Atlantic Geology''''' is a [[peer-reviewed]] [[scientific journal]] covering the [[geology]] of [[Atlantic Canada]] and related areas. It is the only regional geology journal in Canada<ref>{{cite web |url=http://ags.earthsciences.dal.ca/History.php |title=History of the Atlantic Geoscience Society |publisher=[[Atlantic Geoscience Society]] |accessdate=14 Feb 2014}}</ref> and publishes papers, notes and discussions on original research, and [[review paper]]s. It was established in 1965 and since 1986 has been published by the [[Atlantic Geoscience Society]] with digital publishing assistance from the [[University of New Brunswick]].<ref>{{cite web |url=http://journals.hil.unb.ca/index.php/ag/about |title=About the journal |publisher=[[University of New Brunswick]] |accessdate=14 February 2014}}</ref> The journal was one of the first all-digital publications in Canada.<ref name=barr>Barr, S (2014). The history of the Atlantic Geology journal: the first 50 years. Keynote talk at the [[Atlantic Geoscience Society]] annual colloquium, Wolfville, Nova Scotia, February 2014.</ref>

==History==
The journal was established in 1965 as ''Maritime Sediments'' and later renamed ''Maritime Sediments and Atlantic Geology'' before obtaining its current name. The founding [[editor-in-chief]] was Daniel Stanley ([[Dalhousie University]]).<ref name=barr />

Subsequent editors have included Deryck Laming, Bernard Pelletier, George Pajari, Ron Pickerill, G Williams, and the current editors, Sandra Barr ([[Acadia University]]), Rob Fensome ([[Geological Survey of Canada]]), Simon Haslett ([[Cardiff University]]), and David West ([[Middlebury College]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[GeoRef]],<ref>{{cite web |url=http://www.agiweb.org/georef/about/serials.html |title=GeoRef serials |publisher=[[American Geosciences Institute]] |accessdate=14 Feb 2014}}</ref> [[Scopus]],<ref name=Scopus>{{cite web|url=http://cdn.elsevier.com/assets/excel_doc/0003/148548/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work= |accessdate=2014-02-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20131202041814/http://cdn.elsevier.com:80/assets/excel_doc/0003/148548/title_list.xlsx |archivedate=2013-12-02 |df= }}</ref> [[Science Citation Index Expanded]], [[Current Contents]]/Physical, Chemical & Earth Sciences, and [[The Zoological Record]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-02-14}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.856.<ref name=WoS>{{cite book |year=2013 |chapter=Atlantic Geology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist|30em}}

==External links==
* {{Official website|http://journals.hil.unb.ca/index.php/ag}}

[[Category:English-language journals]]
[[Category:Publications established in 1965]]
[[Category:Geology journals]]
[[Category:Academic journals published by learned and professional societies]]