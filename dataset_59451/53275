{{Infobox college sports rivalry
| name = Boston College–Syracuse football rivalry
| team1_image = Boston College Eagles wordmark.png
| team1_image_size =
| team2_image =  SyracuseOrange.svg
| team2_image_size =
| team1 = [[Boston College Eagles football|Boston College Eagles]]
| team2 = [[Syracuse Orange football|Syracuse Orange]]
| total_meetings = 50
| series_record = Syracuse leads, 31–19
| first_meeting_date = October 18, 1924
| first_meeting_result = Syracuse 10, Boston College 0
| last_meeting_date = October 22, 2016
| last_meeting_result  = Syracuse 28, Boston College 20
| next_meeting_date =
| largest_win_margin = Syracuse, 43–10 (2004) 
| longest_win_streak = Syracuse, 6 (1987–92)
| current_win_streak = Syracuse, 2 (2015–present) 
}}

The '''Boston College–Syracuse football rivalry''' is an American [[college football]] [[college rivalry|rivalry]] between the Boston College Eagles and Syracuse Orange.

The two schools first met on October 18, 1924. The Eagles and Orange played annually from 1971 to 2004. To date, Boston College and Syracuse have played each other 50 times. Aside from [[Holy Cross Crusaders football|Holy Cross]], no team has played Boston College more than Syracuse. Syracuse leads the series 31–19.

Boston College and Syracuse were founding members of the [[Big East Conference (1979–2013)|Big East Conference]], first as a basketball conference in 1979, then a football conference in 1991.

To start the 2005 season, Boston College left the Big East to become the 12th member of the [[Atlantic Coast Conference|ACC]]. The future of the rivalry was in doubt. The Eagles and Orange signed a deal to play a non-conference game through 2021. The Eagles won the 2010 meeting 16–7. In September 2011, the ACC announced that they had accepted bids from Syracuse and [[Pittsburgh Panthers football|Pitt]] to become the 13th and 14th members of the ACC.<ref>{{Cite news |url=http://espn.go.com/college-sports/story/_/id/6988468/acc-accepts-pittsburgh-panthers-syracuse-orange-14-team-league |title=ACC Adding Big East's Syracuse, Pitt |date=September 19, 2011 |first=Heather |last=Dinich |publisher=ESPN.com}}</ref> It was later determined that Syracuse and Pitt would join the ACC in July 2013.

==Game results==
{| class="wikitable" style="font-size: 90%;"
!style="background: #e3e3e3;"|Date
!style="background: #e3e3e3;"|Winner
!style="background: #e3e3e3;"|Score
!style="background: #e3e3e3;"|Location
!style="background: #e3e3e3;"|Series
|-align=center style="background: #D44500; color: white"
| October 18, 1924 || '''Syracuse''' || 10–0 || Syracuse, NY || Syracuse 1–0
|-align=center style="background: #910039; color: white" 
| October 28, 1944 || '''Boston College''' || 19–12 || Boston, MA || Tied 1–1
|-align=center style="background: #D44500; color: white"
| September 27, 1958 || '''Syracuse''' || 24–14 || Syracuse, NY || Syracuse 2–1
|-align=center style="background: #D44500; color: white"
| November 25, 1961 || '''Syracuse''' || 28–13 || Chestnut Hill, MA || Syracuse 3–1
|-align=center style="background: #D44500; color: white"
| October 13, 1962 || '''Syracuse''' || 12–0 || Syracuse, NY || Syracuse 4–1
|-align=center style="background: #D44500; color: white"
| September 21, 1963 || '''Syracuse''' || 32–21 || Syracuse, NY || Syracuse 5–1
|-align=center style="background: #910039; color: white" 
| September 19, 1964 || '''Boston College''' || 21–14 || Chestnut Hill, MA || Syracuse 5–2
|-align=center style="background: #D44500; color: white"
| November 20, 1965 || '''Syracuse''' || 21–13 || Syracuse, NY || Syracuse 6–2
|-align=center style="background: #D44500; color: white"
| October 15, 1966 || '''Syracuse''' || 30–0 || Chestnut Hill, MA || Syracuse 7–2
|-align=center style="background: #D44500; color: white"
| November 18, 1967 || '''Syracuse''' || 32–20 || Chestnut Hill, MA || Syracuse 8–2
|-align=center style="background: #910039; color: white" 
| November 29, 1969 || '''Boston College''' || 35–10 || Syracuse, NY || Syracuse 8–3
|-align=center style="background: #910039; color: white" 
| November 6, 1971 || '''Boston College''' || 10–3 || Syracuse, NY || Syracuse 8–4
|-align=center style="background: #910039; color: white" 
| November 4, 1972 || '''Boston College''' || 37–0 || Chestnut Hill, MA || Syracuse 8–5
|-align=center style="background: #D44500; color: white"
| November 17, 1973 || '''Syracuse''' || 24–13 || Syracuse, NY || Syracuse 9–5
|-align=center style="background: #910039; color: white" 
| November 16, 1974 || '''Boston College''' || 45–0 || Chestnut, Hill || Syracuse 9–6
|-align=center style="background: #D44500; color: white"
| October 25, 1975 || '''Syracuse''' || 22–14 || Syracuse, NY || Syracuse 10–6
|-align=center style="background: #910039; color: white" 
| November 13, 1976 || '''Boston College''' || 28–14 || Chestnut Hill, MA || Syracuse 10–7
|-align=center style="background: #D44500; color: white"
| November 12, 1977 || '''Syracuse''' || 20–3 || Syracuse, NY || Syracuse 11–7
|-align=center style="background: #D44500; color: white"
| November 18, 1978 || '''Syracuse''' || 37–23 || Chesnut Hill, MA || Syracuse 12–7
|-align=center style="background: #910039; color: white" 
| November 17, 1979 || '''Boston College''' || 27–10 || Ithaca, NY || Syracuse 12–8
|-align=center style="background: #910039; color: white" 
| November 15, 1980 || '''Boston College''' || 27–16 || Chestnut Hill, MA || Syracuse 12–9
|-align=center style="background: #D44500; color: white"
| November 14, 1981 || '''Syracuse''' || 27–17 || Syracuse, NY || Syracuse 13–9
|-align=center style="background: #910039; color: white" 
| November 13, 1982 || '''Boston College''' || 20–13 || Chestnut Hill, MA || Syracuse 13–10
|-align=center style="background: #D44500; color: white"
| November 12, 1983 || '''Syracuse''' || 21–10 || Syracuse, NY || Syracuse 14–10
|-align=center style="background: #910039; color: white" 
| November 17, 1984 || '''Boston College''' || 24–16 || Foxboro, MA || Syracuse 14–11
|-align=center style="background: #D44500; color: white"
| November 16, 1985 || '''Syracuse''' || 41–21 || Syracuse, NY || Syracuse 15–11
|-align=center style="background: #910039; color: white" 
| November 15, 1986 || '''Boston College''' || 27–9 || Chestnut Hill, MA || Syracuse 15–12
|-align=center style="background: #D44500; color: white"
| November 14, 1987 || '''Syracuse''' || 45–17 || Syracuse, NY || Syracuse 16–12
|-align=center style="background: #D44500; color: white"
| November 12, 1988 || '''Syracuse''' || 45–20 || Chestnut Hill, MA || Syracuse 17–12
|-align=center style="background: #D44500; color: white"
| November 4, 1989 || '''Syracuse''' || 23–11 || Syracuse, NY || Syracuse 18–12
|-align=center style="background: #D44500; color: white"
| November 3, 1990 || '''Syracuse''' || 35–6 || Chestnut Hill, MA || Syracuse 19–12
|-align=center style="background: #D44500; color: white"
| November 16, 1991 || '''Syracuse''' || 38–16 || Syracuse, NY || Syracuse 20–12
|-align=center style="background: #D44500; color: white"
| November 14, 1992 || '''Syracuse''' || 27–10 || Chestnut Hill, MA || Syracuse 21–12
|-align=center style="background: #910039; color: white" 
| October 2, 1993 || '''Boston College''' || 33–29 || Syracuse, NY || Syracuse 21–13
|-align=center style="background: #910039; color: white" 
| November 12, 1994 || '''Boston College''' || 31–0 || Chestnut Hill, MA || Syracuse 21–14
|-align=center style="background: #D44500; color: white"
| November 18, 1995 || '''Syracuse''' || 58–29 || Syracuse, NY || Syracuse 22–14
|-align=center style="background: #D44500; color: white"
| October 26, 1996 || '''Syracuse''' || 45–17 || Chestnut Hill, MA || Syracuse 23–14
|-align=center style="background: #D44500; color: white"
| November 8, 1997 || '''Syracuse''' || 20–13 || Syracuse, NY || Syracuse 24–14
|-align=center style="background: #D44500; color: white"
| October 17, 1998 || '''Syracuse''' || 42–25 || Chestnut Hill, MA || Syracuse 25–14
|-align=center style="background: #910039; color: white" 
| October 30, 1999 || '''Boston College''' || 24–23 || Syracuse, NY || Syracuse 25–15
|-align=center style="background: #910039; color: white" 
| October 14, 2000 || '''Boston College''' || 20–13 || Chestnut Hill, MA || Syracuse 25–16
|-align=center style="background: #D44500; color: white"
| November 24, 2001 || '''Syracuse''' || 39–28 || Syracuse, NY || Syracuse 26–16
|-align=center style="background: #910039; color: white" 
| November 16, 2002 || '''Boston College''' || 41–20 || Chestnut Hill, MA || Syracuse 26–17
|-align=center style="background: #D44500; color: white"
| October 18, 2003 || '''Syracuse''' || 39–14 || Syracuse, NY || Syracuse 27–17
|-align=center style="background: #D44500; color: white"
| November 27, 2004 || '''Syracuse''' || 43–17 || Chestnut Hill, MA || Syracuse 28–17
|-align=center style="background: #910039; color: white" 
| November 27, 2010 || '''Boston College''' || 16–7 || Syracuse, NY || Syracuse 28–18
|-align=center style="background: #D44500; color: white"
| November 30, 2013 || '''Syracuse''' || 34–31 || Syracuse, NY || Syracuse 29–18
|-align=center style="background: #910039; color: white" 
| November 29, 2014 || '''Boston College''' || 28–7 || Chestnut Hill, MA || Syracuse 29–19
|-align=center style="background: #D44500; color: white"
| November 28, 2015 || '''Syracuse''' || 20–17 || Syracuse, NY || Syracuse 30–19
|-align=center style="background: #D44500; color: white"
| October 22, 2016|| '''Syracuse''' || 28–20 || Chestnut Hill, MA || Syracuse 31–19
|}

==Memorable games==
In 2004, Boston College was in first place in the Big East and needed a win against then 5–6 Syracuse in the regular season finale to clinch their first [[Bowl Championship Series|BCS]] bowl berth.

With Syracuse's two RB's out with injuries, DB [[Diamond Ferri]] filled in as RB. Ferri rushed for 141 yards and 2 TDs. Ferri also had a 44-yard interception return for a TD. Syracuse went on to pull off the shocking blowout upset, winning 43–17.<ref>{{Cite web |url=http://scores.espn.go.com/ncf/recap?gameId=243320103 |title=BC Deprived of Farewell Outright Big East Title}}</ref> This game was also the first career start by Boston College QB [[Matt Ryan (American football)|Matt Ryan]]; he was filling in for the injured [[Paul Peterson (American football)|Paul Peterson]]. This was BC's final game as a member of the Big East; they began playing in the ACC in 2005.

In 2013, Syracuse was playing in their first year as a member of the [[Atlantic Coast Conference|ACC]]. Syracuse was 5–6 heading into the final game of the season against Boston College, needing to win to become [[Bowl eligibility|bowl-eligible]]. Boston College was up 31–27 with 2:08 left when Syracuse drove down the field and scored the game-winning touchdown with 6 seconds left.<ref>{{Cite web |url=http://scores.espn.go.com/ncf/recap?gameId=333340183 |title=Andre Williams hurt as Syracuse stuns BC late|date=November 30, 2013|last=Associated Press|publisher=ESPN.com}}</ref>

==References==
{{Reflist}}

{{Boston College Eagles football navbox}}
{{Syracuse Orange football navbox}}
{{Atlantic Coast Conference rivalry navbox}}

{{DEFAULTSORT:Boston College-Syracuse football rivalry}}
[[Category:College football rivalries in the United States]]
[[Category:Boston College Eagles football]]
[[Category:Syracuse Orange football]]