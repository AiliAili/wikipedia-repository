{{Infobox journal
| title        = Oxford University Commonwealth Law Journal
| cover        = [[File:Oxford University Commonwealth Law Journal - lo-res cover.jpg]]
| editor       = Tobias Lutzi
| discipline   = [[International law]]
| abbreviation = OUCLJ
| publisher    = [[Taylor and Francis]]
| country      = [[United Kingdom]]
| frequency    = Biannually
| history      = 2001-present
| website      = https://www.law.ox.ac.uk/ouclj
| link1        = http://www.tandfonline.com/toc/rouc20/current
| link1-name   = Online access
| ISSN         = 1472-9342
}}
The '''''Oxford University Commonwealth Law Journal''''' ('''''OUCLJ''''') is a [[postgraduate education|postgraduate]]-edited [[international law|international]] and [[comparative law|comparative]] [[law review|law journal]] from the [[University of Oxford]]'s [[Faculty of Law, University of Oxford|Faculty of Law]], covering the study of legal trends and developments within and between [[Commonwealth of Nations|Commonwealth]] [[jurisdiction]]s.

==Content==
The journal includes articles, case notes and book reviews. Case notes critically analyse and evaluate rulings from the [[Judicial functions of the House of Lords|House of Lords]], the [[Judicial Committee of the Privy Council|Privy Council]], and the national courts of the Commonwealth States.

===Topics of recent articles===

*''Comparative'': good faith; agency; consideration; legitimate expectation; equity and unregistered land rights; recovery of economy loss in negligence; the Westminster system; breach of trust; comparative models of constitutionalism; unjust enrichment;<ref>Tang Hang Wu, ''Natural Obligations and the Common Law of Unjust Enrichment'' (2006) 6 OUCLJ 133 [http://www.hartjournals.co.uk/ouclj/sample.html (free download)]</ref> rights of survivorship; violence in professional sport; entrapment; Internet libel.
*''Africa'': state liability in [[Botswana]]; economic development in [[Ghana]]; policing in [[Kenya]]; management of land in [[Nigeria]]; extrajudicial services of judges in [[South Africa]]; corporate insolvency in [[Zambia]].
*''Asia'': human rights litigation in [[Hong Kong]]; confidentiality regarding HIV/AIDS in [[India]]; case-management in [[Singapore]].
*''The Americas'': capital punishment in [[the Caribbean]]; freedom of religious expression in Canada.
*''Oceania'': native title in [[Australia]]; the rule of law in [[Fiji]]; jurisprudential theory in [[New Zealand]]; divorce in the [[Solomon Islands]].
*''Compulsory voting
*''Statutory interpretation

==Organisation==
The ''OUCLJ'' was for its first decade published by [[Hart Publishing]] but moved to [[Taylor & Francis]] in 2015.<ref>{{Cite journal|last = Krishnaprasad|first = K V|date = 2015|title = General Editor's Foreword|url = http://www.tandfonline.com/doi/pdf/10.1080/14729342.2016.1140364|journal = Oxford University Commonwealth Law Journal|volume = 15|doi =|pmid =|access-date =}}</ref> It is the flagship journal of the [[University of Oxford]]'s postgraduate law community, designed for contributions from academics, professionals and policy-makers, wherever situated, on matters of current interest to Commonwealth legal systems. Created in 2001, the journal provides a forum for international debate on both private and public law topics. Some pieces are explicitly comparative in orientation, while others concern a single jurisdiction only. All pieces published in the ''OUCLJ'' are selected on the basis that they are likely to be of interest to a larger Commonwealth audience. Submissions for publication are [[Double-blind peer review|double-blind peer reviewed]].<ref>http://www.tandfonline.com/action/authorSubmission?journalCode=rouc20&page=instructions</ref> The journal is financially assisted by the [[Rhodes House|Rhodes Trust]].

==Patrons==

The Journal is supported by a board of patrons consisting of current or former judges from Commonwealth jurisdictions,<ref>{{cite web |url=http://users.ox.ac.uk/~ouclj/patrons.html  |title=Oxford University Commonwealth Law Journal - Board of Patrons |accessdate=2009-03-19}}</ref> namely:
* [[Edwin Cameron]] ([[South Africa]])
* [[James Edelman]] ([[Australia]])
* [[Robert Goff, Baron Goff of Chieveley|Lord Goff of Chieveley]] ([[England and Wales]])
* [[Kenneth Hayne]] (Australia)
* [[Kenneth Keith|Sir Kenneth Keith]] ([[New Zealand]])
* [[Sujata Manohar]] ([[India]])
* [[Robert Ribeiro]] ([[Hong Kong]])
* [[Robert Sharpe]] ([[Canada]])
* [[Anthony Smellie]] ([[Cayman Islands]])

==External links==
*{{Official website|http://www.tandfonline.com/toc/rouc20/current}}
*[https://www.law.ox.ac.uk/ouclj Oxford Law Faculty page]

==References==
{{reflist}}

[[Category:British law journals]]