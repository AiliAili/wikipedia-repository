{{Use dmy dates|date=July 2011}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Carbon Dragon
 | image=Carbon Dragon Logo.png
 | caption=Carbon Dragon logo from the 1988 sales brochure
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=United States
 | manufacturer=
 | designer=[[Jim Maupin]]
 | first flight=1988
 | introduced=
 | retired=
 | status=Plans no longer available
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least four
 | program cost= <!--Total program cost-->
 | unit cost= US$2000 (estimate 1988)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Maupin Carbon Dragon''' is an American, [[high-wing]], single-seat, [[Glider (sailplane)|glider]] that was designed by [[Jim Maupin]] and made available as plans for [[Homebuilt aircraft|amateur construction]]. Plans are no longer available.<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=54|title = Carbon Dragon Maupin |accessdate =7 July 2011|last = Activate Media|year = 2006}}</ref><ref name="Maupin">Maupin, Jim: ''Carbon Dragon'', sales brochure, October 1988</ref><ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 57. Pagefast Ltd, Lancaster OK, 2003. ISSN 1368-485X</ref>

==Design and development==
The Carbon Dragon was intended to take advantage of the US [[FAR 103 Ultralight Vehicles]] regulations that classify unpowered aircraft with empty weights of {{convert|155|lb|kg|0|abbr=on}} or less as [[hang glider]]s and thus allow them to be flown without a pilot license, [[Federal Aviation Administration]] [[aircraft registration]] or a [[Certificate of Airworthiness]]. The Carbon Dragon's standard empty weight is {{convert|145|lb|kg|0|abbr=on}} and the aircraft has a gross weight of {{convert|335|lb|kg|0|abbr=on}}, giving a payload of {{convert|190|lb|kg|0|abbr=on}}. The designer said "The philosophy behind its development was to try to bring foot launch soaring performance up into the lower performance range of sailplanes." The Carbon Dragon was intended to be similar in concept to the [[Hall Vector 1]].<ref name="SD" /><ref name="Maupin" />

The original design was intended to be a much more complex aircraft, as Maupin explained:

{{quotation|As originally envisioned, it would have 40 ft span, and a sailcloth flap that would roll up on a roller inside the wing, changing the area from 100 to 140 square feet and back again. As calculations and drawings progressed, it got more and more complicated: chain drive to turn the roller; 1000 lb. pull to get the flap out; required five pullies: and double cables behind the wing and in the slip stream, etc., etc., not to mention the aileron problem. One evening over coffee in our motel room in Hemet, my friend, mentor, advisor, and consultant, Irv Culver, said, "Jim, you're going to build that, and then spend the springtime of your youth getting it all to work. And, it'll get heavier and more complex and ultimately the drag and weight of all that stuff will defeat the whole purpose. Why don't you throw all that out and put everything you save in weight and complexity into added span? If you like, I'll run the numbers for drag on all that hardware." When Irv offers to "run the numbers" to prove his point, it's time to get out gracefully.<ref name="Maupin" /> }}

As a result the aircraft was redesigned to its final configuration, a simpler and lighter aircraft with a {{convert|44|ft|m|1|abbr=on}} span wing.<ref name="Maupin" />

The Carbon Dragon is predominantly a conventional wood and doped [[aircraft fabric]] glider, making judicious use of [[carbon fiber]] in the wing spar caps, control rods, [[flaperon]]s and the elliptical tail boom to save weight. The control tubes are constructed by laying up the carbon fiber on aluminium tubing and then, when the carbon has cured, dissolving the aluminium with swimming pool acid. The cockpit is totally enclosed and the original design called for a cockpit width of {{convert|17|in|cm|0|abbr=on}} at the hips and {{convert|25|in|cm|0|abbr=on}} at the shoulder, although some have been modified to accommodate pilots of larger dimensions. The main aircraft structure consists of dual triangular torque boxes on each side of the [[fuselage]]. The wing employs a Culver SD [[airfoil]] that was designed by [[Irv Culver]] for the project and full-span flaperons of 30% chord. The flaperons can deploy from −5° to +15° as [[Flap (aircraft)|flaps]] and −4° to +16° as [[aileron]]s, with a 4:1 [[Aileron#Differential ailerons|differential]]. The flaperons are driven by two, vertically mounted pushrods enclosed within the fuselage and connected to the [[side stick]]. The wing ribs forward of the [[spar (aviation)|spar]] are fabricated from {{convert|1/4|in|mm|0|abbr=on}} 5-ply [[mahogany]] and aft of the spar from {{convert|5/16|in|mm|0|abbr=on}} square spruce. The landing gear is a fixed monowheel, mounted on the hinged cockpit bottom door, that opens to allow the pilot to lift the aircraft for foot-launching. The aircraft was designed so that if the pilot falls while foot-launching his body will be in the rear fuselage cavity and not pinned under the aircraft. The ultimate structural load limit is +/-7.5 [[G-force|g]], with a +/-5.0 g operational load limit.<ref name="SD" /><ref name="Maupin" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate =7 July 2011|last = Lednicer |first = David |year = 2010}}</ref>

The Carbon Dragon was designed to be launched by foot-launch, [[aero-tow]], [[winch-launch]], [[auto-tow]] or [[bungee launch]].<ref name="SD" /><ref name="Maupin" />

In October 1988 Maupin reported that the prototype had been flown by ten different pilots ranging in weight from {{convert|120|to|210|lb|kg|0|abbr=on}}, had achieved a 100 fpm (0.51&nbsp;m/s) sink rate and had been launched by auto-tow, aero-tow and bungee, but had not been foot-launched. In October 1988 Maupin estimated that building a Carbon Dragon would cost US$2000 and take 1000–1500 hours of construction time.<ref name="Maupin" />

When they were available the plans consisted of 23 sheets of 2' X 4' (61 X 122&nbsp;cm) blueprints and sold for US$150.<ref name="Maupin" />

At least one Carbon Dragon was modified to include a cockpit roof-mounted pentagonal [[Spoiler (aeronautics)|spoiler]], similar to that used on the [[Maupin Windrose]].<ref name="SD" />

==Operational history==
Many of the early test flights were done near [[Tehachapi, California]] by auto-tow using a {{convert|2000|ft|m|0|abbr=on}} rope and these included several 45 minute soaring flights in evening convergent lift. The designer conducted many of the prototype flights himself and said of flying the aircraft, "It's great fun to fly, everything happens so slowly".<ref name="Maupin" />

In October 1988 Maupin stated that 70 sets of plans had been sold.<ref name="Maupin" />

In the 1994 Kansas Kowbell Klassic, a scheduled, non-handicapped cross-country distance contest, Gary Osoba flew a Carbon Dragon to win with a distance of {{convert|180|mi|km|0|abbr=on}}.<ref name=VariometerJuly1994>{{cite journal|last=McNay|first=Curt|title=Sunflower Seeds|journal=Variometer|date=July 1994|pages=4|accessdate=6 May 2012}}</ref>  In July 1995 Gary Osoba flew a Carbon Dragon to a US National and World Record in the Ultralight Category for Distance up to Three Turnpoints for a flight of {{convert|237.440|mi|km|0|abbr=on}}. In September 1995, Osoba set US National and World Records in the Ultralight Category for {{convert|100|km|mi|0|abbr=on}} Triangle Speed, Triangle Distance, and Out & Return Distance of {{convert|24.48|mph|km/h|0|abbr=on}}, {{convert|133.02|mi|km|0|abbr=on}}, and {{convert|115.52|mi|km|0|abbr=on}} respectively.<ref name="Soaring October 1998">{{cite journal|last=Ruprecht|first=Judy|title=Badges & Records|journal=Soaring|date=October 1998|volume=62|issue=10|pages=41|accessdate=8 July 2011}}</ref>

Qualifying as a FAR Part 103 hang glider, the Carbon Dragon does not require FAA registration and thus an accurate number of the total completed is not available, but the ''Soaring Directory'' reports four have been flown.<ref name="SD" /><ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=CARBON+DRAGON&PageNo=1|title = Make / Model Inquiry Results|accessdate =7 July 2011|last = [[Federal Aviation Administration]]|date=July 2011}}</ref>

==Variants==
[[File:Steve arndt cd.jpg||250px|thumb|right|The Arndt Magic Dragon under construction]]
;Carbon Dragon
:Initial version<ref name="SD" />
;Magic Dragon
:Improved version developed by Steve Arndt

==Specifications (Carbon Dragon) ==
{{Aircraft specs
|ref=Sailplane Directory and Jim Maupin<ref name="SD" /><ref name="Maupin" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=One
|capacity=
|length m=6.1
|length ft=
|length in=
|length note=
|span m=
|span ft=44
|span in=0
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=153.34
|wing area note=
|aspect ratio=12.62:1
|airfoil=Culver SD
|empty weight kg=
|empty weight lb=145
|empty weight note=
|gross weight kg=
|gross weight lb=335
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=19
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=+/-7.5 g ultimate, +/-5.0 g load limit
|roll rate=
|glide ratio=25:1 at {{convert|35|mph|km/h|0|abbr=on}}
|sink rate ms=
|sink rate ftmin=100
|sink rate note= 
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=2.18
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
*[[Microlift glider]]
|related=<!-- related developments -->
|similar aircraft=
*[[Advanced Aeromarine Sierra]]
*[[Hall Vector 1]]
*[[Rensselaer RP-1]]
*[[Ruppert Archaeopteryx]]
|lists=
* [[List of gliders]]
}}

==References==
{{reflist}}

==External links==
*[http://www.carbondragon.us/ Carbon Dragon Technical Website]
*[http://www.ihpa.ie/carbon-dragon/ Carbon Dragon Archive (2012)]
{{Jim Maupin aircraft}}

[[Category:United States sailplanes 1980–1989]]
[[Category:Sailplanes designed for foot-launching]]
[[Category:Homebuilt aircraft]]