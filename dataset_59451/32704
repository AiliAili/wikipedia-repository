{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Norse-American medal{{sfn|Swiatek|pp=168–169}}
| Diameter            = {{plainlist|
*Gold: 24 mm
*Silver: 29 mm
}}
Bronze: 69.5| Thickness          = 
{{plainlist |
*Gold: 2 mm
*Thin silver: 1.6 mm
*Thick silver: 2 mm}}
Bronze: 4.5
| Edge                = plain
| Composition =
  {{plainlist |
* Gold: 90.0% gold, 10.0% copper
* Silver: 90.0% silver, 10.0% copper
* Bronze: 90.0% copper, 10.0% zinc, with silver plating
 }}
| Years of Minting    = 1925
| Mintage = {{plainlist|
*Gold: 100 (less 47 melted)
*Thin silver: 6,000
*Thick silver: 33,750
*Bronze: est. 60–75
}}
| Mint marks            = None, all pieces struck at Philadelphia Mint without mint mark.
| Obverse             = {{Css Image Crop|Image =1925 Medal Norse Gold commemorative.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center|alt=An octagonal gold medal depicting a Viking chieftain; full details in "Design" section}}
| Obverse Design      = [[Viking raid warfare and tactics|Viking warrior]]
| Obverse Designer    = [[James Earle Fraser (sculptor)|James Earle Fraser]]
| Obverse Design Date   = 1925
| Reverse               = {{Css Image Crop|Image = 1925 Medal Norse Gold commemorative.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|alt=Octagonal gold medal showing a Viking ship, as per "Design" section}}
| Reverse Design        = [[Viking longship]]
| Reverse Designer      =[[James Earle Fraser (sculptor)|James Earle Fraser]]
| Reverse Design Date   = 1925
}}
The '''Norse-American medal''' was struck at the [[Philadelphia Mint]] in 1925, pursuant to an act of the [[United States Congress]]. It was issued for [[Norse-American Centennial|the 100th anniversary of the voyage]] of the ship ''[[Restauration (ship)|Restauration]]'', bringing early Norwegian immigrants to the United States.

Minnesota Congressman [[Ole Juulson Kvale]], a [[Norwegian-American|Norse-American]], wanted a commemorative for the centennial celebrations of the ''Restauration'' journey. Rebuffed by the [[United States Department of the Treasury|Treasury Department]] when he sought the issuance of a special coin, he instead settled for a medal. Sculpted by [[Buffalo nickel]] designer [[James Earle Fraser (sculptor)|James Earle Fraser]], the medals recognize those immigrants' [[Vikings|Viking]] heritage, depicting a warrior of that culture on the [[obverse and reverse|obverse]] and his vessel on the reverse. The medals also recall the early Viking [[Norse colonization of the Americas|explorations of North America]].

Once authorized by Congress, they were produced in various metals and sizes, for the most part prior to the celebrations near [[Minneapolis]] in June 1925. Only 53 were issued in gold, and they are rare and valuable today; those struck in silver or bronze  have appreciated much less in value. They are sometimes collected as part of [[Early United States commemorative coins|the U.S. commemorative coin series]].

==Background and inception==
On July 4 or 5, 1825, the vessel ''[[Restauration (ship)|Restauration]]'' sailed from [[Stavanger]], Norway, for the United States, with 45 emigrants aboard.{{sfn|Blegen|pp=599–601}} According to what ''[[The New York Times]]'' deemed "[[bacchanalia]]n" legends of its passage, the expedition anchored off an English seacoast village and traded ashore some of its rum, only to depart in haste when local officials took an interest.<ref name = "nyt2">{{cite news|title=First Norse Settlers Arrived 100 Years Ago|date=April 12, 1925|newspaper=[[The New York Times]]| url=http://search.proquest.com/docview/103458202}} {{subscription}}</ref> Off [[Madeira]], expedition leader Lars Larsen is said to have fished a cask from the sea, which proved to be filled with rare wine that was thoroughly enjoyed by those aboard.<ref name = "nyt2" /> After they arrived in New York on October 9, the ship was seized pursuant to a court order, as the passengers exceeded the permitted number for a ship of its size by 21, counting a baby girl born to the Larsens en route. In addition, a fine was to be imposed, but because the immigrants spoke no English and had no knowledge of American laws, President [[John Quincy Adams]] issued a pardon, releasing the ship and remitting the fine.{{sfn|Blegen|pp=601, 613–618}} Initially settling on land they purchased near the shore of [[Lake Ontario]], about {{convert|35|mi}} from [[Rochester, New York]], the passengers were the first of many organized groups of [[Norwegian-American|Norse-Americans]] who [[Transatlantic crossing|crossed the Atlantic]], especially to the northern and western United States.<ref name = "nyt2" />{{sfn|Swiatek|p=168}}

[[Ole Juulson Kvale]] was a [[Minnesota]] congressman of the [[Farmer-Labor Party]],<ref name = "profile" /> and a proud Norse-American. Kvale was a member of the Norse-American Centennial Commission, which was to organize a 100th anniversary celebration of the ''Restauration''{{'s}} voyage.{{sfn|Swiatek|p=165}} This celebration was important to a Norse-American community that had been perceived as [[Opposition to World War I#In the United States|antiwar]] during [[World War I]], and was attempting to display both ethnic pride and assimilation.{{sfn|Shultz|pp=1265, 1267}} Kvale, a [[The Norwegian Lutheran Church in the United States|Lutheran]] minister, was also a member of the [[House Committee on Coinage, Weights, and Measures]],<ref>{{cite web|url=http://bioguide.congress.gov/scripts/biodisplay.pl?index=K000349|title = Kvale, Ole Juulson (1869–1929)|publisher=United States Congress|accessdate=September 5, 2015}}</ref> and in January 1925 approached the [[United States Department of the Treasury|Treasury Department]], seeking its support for a commemorative coin in honor of the anniversary; he was told that the Treasury would oppose it.{{sfn|House hearings|pp=3–4}} Commemorative coins for ethnic heritage groups were unlikely to pass Congress at that time due to the controversy caused by the 1924 [[Huguenot-Walloon Tercentenary half dollar]], seen by some as Protestant propaganda.{{sfn|Shultz|p=1287}}

On January 30, 1925, Kvale attended a meeting of the Coinage Committee, at which the proposal that would become the [[Vermont Sesquicentennial half dollar]] was considered.  Treasury officials were present in opposition, suggesting a medal be issued instead, and Kvale asked several questions about the Mint's issuance of medals.{{sfn|Vermont House hearings|pp=7–8}} On February 3, Kvale and [[Paul Kvale|his son Paul]] met with Treasury officials, bringing a draft bill authorizing the [[United States Mint|Bureau of the Mint]] to strike commemorative medals for the ''Restauration'' anniversary. Acting Mint Director [[Mary M. O'Reilly]] and Treasury Undersecretary Garrard Winston were dubious about the idea of striking silver medals that would be between the [[Standing Liberty quarter|quarter]] and [[Walking Liberty half dollar|half dollar]] in size. To offset this concern, Paul Kvale suggested making the medal octagonal or hexagonal. O'Reilly and Winston favored the idea, and after Congressman Kvale met with legal counsel to the Treasury and other officials, he was assured of the department's full support.{{sfn|Swiatek|p=165}} Kvale also successfully lobbied the [[United States Post Office Department|Post Office Department]] for the issuance of [[commemorative stamp]]s; he told Third Assistant [[United States Postmaster General|Postmaster General]] Warren I. Glover that, in a broader sense, the medal recognized the North American explorations of the [[Vikings]] around the year 1000.{{sfn|Swiatek|p=173}} Kvale declared that in seeking the souvenir medal and stamps, he was contributing to the "growth of the Norwegian heritage by having it 'preserved in metal' as well as 'paper [[time capsule]]s'."{{sfn|Shultz|p=1287}}

==Legislation==
Kvale introduced a bill for a Norse-American medal in the [[United States House of Representatives|House of Representatives]] on February 4, 1925.<ref name = "profile" /> It was referred to the Committee on Coinage, Weights, and Measures.<ref>{{USCongRec|1925|3030|date=February 4, 1925}} {{subscription}}</ref> On behalf of that committee, Kvale reported it favorably to the full House on February 10. In the report, Kvale stated that the 40,000 medals would be struck without expense to the government, and that Treasury officials supported the bill. "In view of the importance of this celebration to the many descendants of the Norse immigrants into this country, and through these to the State of Minnesota, which is officially sponsoring the event, and to the great Northwest, which they have been such a large factor in developing, the committee believes that such a medal is fitting and proper and that this bill should be enacted into law."<ref name = "house report">{{cite web|author=((House Committee on Coinage, Weights, and Measures))|url=http://congressional.proquest.com/congressional/docview/t47.d48.8391_h.rp.1437|title=Coinage of a Medal with Appropriate Emblems Commemorative of the Norse-American Centennial|date=February 10, 1925}} {{subscription}}</ref>

South Dakota Senator [[Peter Norbeck]] also introduced legislation for a Norse-American medal on February 5, 1925. It was referred to the [[United States Congress Joint Committee on the Library|Committee on the Library]]. On the 6th, that committee was discharged of responsibility for the bill and it was referred instead to the [[United States Senate Committee on Banking, Housing, and Urban Affairs|Committee on Banking and Currency]]. Norbeck, on behalf of the Banking Committee, reported the bill favorably and without amendment to the Senate on February 13.<ref name = "senate bill">{{cite web|title=S. 4230, Calendar No. 1176|publisher=[[United States Government Printing Office]]|date=February 13, 1925|url=http://congressional.proquest.com/congressional/docview/t01.d02.68_s_4230_rs_19250203}} {{subscription}}</ref> It was passed by the Senate without objection or amendment on the 18th.<ref>{{USCongRec|1925|4061|date=February 18, 1925}} {{subscription}}</ref>

The Senate-passed bill then was transmitted to the House of Representatives, and was referred to the Coinage Committee on February 20.<ref>{{USCongRec|1925|4277|date=February 20, 1925}} {{subscription}}</ref> It was brought forward on February 27, 1925. When the [[Speaker of the United States House of Representatives|Speaker]], [[Frederick H. Gillett]], asked if there was objection to the consideration of the bill, Ohio's [[James T. Begg]] wanted to know if there was anyone present who could give him information about it, and if there was not, he would object. Kvale stated that he could, and when Begg inquired if [[United States Secretary of the Treasury|Treasury Secretary]] [[Andrew W. Mellon]] favored the bill, he assured the Ohioan that this was so. Kvale had the Senate-passed bill substituted for the one he had introduced, and it received the House's endorsement without objection or amendment.<ref>{{USCongRec|1925|4873–4874|date=February 27, 1925}} {{subscription}}</ref> It was passed into law with the signature of President [[Calvin Coolidge]] on March 2, 1925.<ref name = "profile">{{cite web|url=http://congressional.proquest.com/congressional/result/congressional/pqpdocumentview?accountid=14541&groupid=96011&pgId=c8a527df-ec7a-4643-b568-bb789d141cf7&rsId=14EF88C3DE0#RelatedPublications|title=68 Bill Profile H.R. 12160 (1923–1925)|publisher=ProQuest Congressional|accessdate=September 3, 2015}} {{subscription}}</ref>

The act provided for a maximum of 40,000 medals, to be struck at the [[Philadelphia Mint]], from design models prepared by the Norse-American Centennial Commission. Medals would be turned over to a designated agent of the commission on payment of the cost of producing them. They were to be made subject to the provisions of section 52 of the [[Coinage Act of 1873]].<ref>{{USPL|68|524}}</ref> That section permitted medals of a national character to be struck at the Philadelphia Mint, but forbade Mint personnel from making [[Glossary of numismatics#D|dies]] for private medals,<ref>{{USPL|42|131}}(third session)</ref> and was enacted after Philadelphia Mint Chief Coiner [[Franklin Peale]] had for some years run a private medals business on Mint premises, prior to his firing in 1854.{{sfn|Taxay|pp=188–190}}

==Preparation==
Kvale hoped that his friend and fellow Minnesotan, Senator [[Henrik Shipstead]], could persuade sculptor [[Gutzon Borglum]] to design the medal for no fee or a nominal one. Borglum, who was busy with construction at [[Stone Mountain]] in Georgia (he would later sculpt [[Mount Rushmore]]), had designed the [[Stone Mountain Memorial half dollar]]; he had no time to accept the work. [[Buffalo nickel]] designer [[James Earle Fraser (sculptor)|James Earle Fraser]], a member of the [[Commission of Fine Arts]], was engaged for a fee of $1,500, about the usual for a commemorative coin.{{sfn|Swiatek|p=166}}

Fraser prepared designs and submitted them to the Mint; O'Reilly sent them to the Commission of Fine Arts on April 14, 1925. The commission approved them; its only suggestion was that the first "the" be removed from the inscription on the reverse, "<small>AUTHORIZED BY THE CONGRESS OF THE UNITED STATES OF AMERICA</small>", and this was done.{{sfn|Swiatek|p=167}} Sketches had been printed in the ''[[Minneapolis Journal]]'' on March 29, provoking some reaction from those who felt that the design implied that Norwegians still dressed like Vikings in 1825, and that the date, 1000, should be moved from reverse to obverse to eliminate the confusion. The public objections had no effect on events.{{sfn|Swiatek|p=166}}

==Design==
{{multiple image|caption_align=center|header_align=center
 | align = left
 | direction = vertical
 | width = 200
 | header =Norse-American postal issue, 1925
 | alt1 = A bicolor two cent postage stamp, showing  a 19th century sailing ship
 | alt2 =  Bicolor five cent postage stamp showing a Viking ship, pointed right (east)
 | image1 = Norse American Centennial Sloop 1925 Issue-2c.jpg
 | image2 = Norse American Centennial Viking 1925 Issue-5c.jpg
 | footer = Issued for the anniversary, the two-cent depicts the ''Restauration'' and the five-cent a Viking vessel.
}}
The obverse of the medal shows a Norwegian Viking chieftain who has just come ashore from his ship (seen behind him) and is armed for war, with [[horned helmet]], shield, sword, and ''svard'' (dagger). He is intended to be landing at [[Vinland]], the lands in the Americas explored and to some extent settled by the Vikings about the year 1000. The helmet is most likely an anachronism, as they are not believed to have been used for two millennia prior to the Vinland landing, and were probably ceremonial, rather than intended for battle. The centennial and the years are recognized on the obverse. The reverse shows a Viking ship, along with the authorization by Congress and the approximate year in which Vinland was settled. <small>"OPUS FRASER"</small> (Fraser's work), the artist's signature, is to the left of the ship.{{sfn|Swiatek|p=168}}

Numismatist Anthony Swiatek, in his volume on commemoratives, wonders if [[Leif Erikson]], the famed explorer of that period, would not have been a superior choice. He concludes that Kvale would not have supported such a depiction, because "he was interested in pure romanticization. He saw a Viking ship and his chieftain in full regalia".{{sfn|Swiatek|p=168}}

Julie Shultz, in her journal article on the 1925 celebration, finds it significant that the medal has nothing to do with the arrival of the ''Restauration'' in an already-formed United States, but symbolizes ethnic pride in the early explorers. Noting that one of the stamps depicts a Viking ship and the other the ''Restauration'', she concludes of these three government issues for the celebration: "Though outwardly, these souvenirs were to symbolize the Norwegian immigrant heritage that began in 1825, they actually invert the dominant narrative by using an American form to proclaim that Norwegians were the first Europeans to land on American soil."{{sfn|Shultz|p=1288}}

==Production, distribution, and collecting==
Six thousand silver medals on a thin (1.6&nbsp;mm) [[planchet]] were struck between May 21 and 23, 1925, at the Philadelphia Mint. They were handled like ordinary coins: They were counted, bagged, and transported to the Fourth Street National Bank of Philadelphia for the centennial commission's use. Between May 29 and June 13, a total of 33,750 pieces were struck on a thick (2&nbsp;mm) silver planchet. The reason for the two varieties is uncertain; Swiatek theorizes that the Norse-American Centennial Commission might not have liked how the thin ones looked, or might have wanted collectors to buy two medals. One hundred were struck in gold, on June 3 and 4—Kvale received the second one struck. The medals cost the commission 30 cents each for the thin ones, 45 cents for the thick, and $10.14 for the gold. The retail price of the thin ones is uncertain (Swiatek estimates $1.75), the thick ones are known to have cost $1.25, and the gold pieces about $20. They were sold by mail order; none were sold at the celebrations or in person. There was a sales limit of one per person, but purchasers could buy on behalf of as many family members as they wanted. The thin pieces were not offered for sale until November or December 1925, and sold mostly to numismatists—the commission's secretary, J.A. Horvik, was frustrated that more "Norsemen" were not buying the medal.{{sfn|Swiatek|pp=166–171}} After the celebrations, Kvale took 5,000 medals to New York, hoping to sell them, but was not successful.<ref>
{{cite journal
 | last=Schmidt
 | first = David
 | title = Norwegian heritage: Pieces commemorated immigrant ship's arrival in the United States
 | url = http://www.exacteditions.com/browse/21053/21950/42880/3/25/0/norse-american%20medal
 | subscription = yes
 | journal = [[The Numismatist]]
 | publisher = [[American Numismatic Association]]
 | date = December 2012
 | pages = 23
}}</ref> Of the 100 gold pieces struck, 47 were eventually returned to the Treasury when they could not be sold, and some of the silver pieces (most likely thick ones) were as well.{{sfn|Swiatek|pp=166–171}}

The [[Norse-American Centennial|Norse-American Immigration Centennial Celebration]] was held at the [[Minnesota State Fair]]grounds near Minneapolis from June 6–9, 1925. Automobile caravans were organized to bring attendees from the [[Red River Valley]], bearing the slogan, "The Norsemen are Coming!"<ref name = "nyt2" /> President Coolidge was present; he called the Viking explorers "these [[Norse mythology|sons of Thor and Odin]]", and told attendees, "the pledge of the Norwegian people has never yet gone unredeemed. I have every faith that through the vigorous performance of your duties you will add new lustre to your glories in the days to come."<ref name = "nyt" /> ''[[The New York Times]]'' noted that commemorative stamps and a congressionally authorized medal had been issued for the celebration. "Seldom before has the celebration of a similar event been so honored by the Government, as has this centennial."<ref name ="nyt" >{{cite news|title=City to Celebrate Norse Centennial|date=September 13, 1925|newspaper=[[The New York Times]]|url=http://search.proquest.com/docview/103531028}} {{subscription}}</ref>

The ''Times'' had inaccurately described the medal as "the first commemorative medal to be issued in the history of the mint".<ref name = "nyt2" />{{sfn|Swiatek|pp=169–170}}  The publicity people for the celebration had billed it as the first medal to be issued pursuant to an act of Congress, but in October 1925, Mint Director [[Robert J. Grant]] learned that a medal had been authorized by Congress for the centennial of American independence in 1876, and it had been issued in different sizes. He informed Kvale, who was intrigued by the fact that the independence medal had been issued in a 3-inch (76&nbsp;mm) size. The congressman felt that the larger size would allow the detail of his medal to be better shown, which would help when one was exhibited in a museum display case. Not all members of his board were enthusiastic about the idea, but between 60 and 75 of these larger medals were struck, likely in December at the Philadelphia Mint, with Kvale undertaking to purchase any that were not sold. They were plated in silver by a private firm in Washington, D.C., and about 30 were presented or mailed to dignitaries, including one to President Coolidge.{{sfn|Swiatek|pp=169–170}}

The Norse-American medal is not a coin, and is not [[legal tender]]. Due to its similarity to a coin, and the fact that it was authorized by Congress, it is sometimes collected as part of the U.S. commemorative coin series. Though the silver ones can be purchased for less than $100 up to $500, and the silver-plated one for between $500 and $3,500, the gold specimen has sold for as high as $40,000. Some medals were used as pocket pieces or worn in mountings to the fair, and display damage or wear.{{sfn|Swiatek|p=171}}

==References==
{{reflist|30em}}

==Sources==
*{{cite book
|last=Blegen
|first=Theodore C.
|title=Norwegian Migration to America: The American Transition
|publisher=Haskell House Publishers Ltd.
|year=1940
|oclc=423581
|url=https://books.google.com/books?id=vmjz0Iq9x5EC&lpg=PA3&dq=helland%20pardon%20john%20quincy%20adams&pg=PR4#v=onepage&q&f=false
| ref = {{sfnRef|Blegen}}
  }}
*{{cite journal
|last=Shultz
|first=April
|title='The Pride of the Race Had Been Touched': The 1925 Norse-American Immigration Centennial and Ethnic Identity
|journal=Journal of American History
|date=March 1991
|volume=77
|issue=4
|pages=1265–1295
|url=http://web.b.ebscohost.com/ehost/pdfviewer/pdfviewer?sid=394ff980-69f4-458d-bf1f-c108a51869b1%40sessionmgr111&vid=15&hid=124
|issn=0021-8723
|jstor=2078262
| ref = {{sfnRef|Shultz}}
  }} {{subscription}}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York
  | year = 1983
  | edition = reprint of 1966
  | isbn = 978-0-915262-68-7
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
|author=((United States House of Representatives Committee on Coinage, Weights, and Measures))
|title=Coinage of 50-Cent Pieces in Commemoration of Sesquicentennial of Discovery of Hawaii
|date=January 23, 1928
|url=http://congressional.proquest.com/congressional/docview/t29.d30.hrg-1928-cwe-0002
|publisher=[[United States Government Printing Office]]
|ref={{sfnRef|House hearings}}
}} {{subscription}}
* {{cite book
|author=United States House of Representatives Committee on Coinage, Weights and Measures
|title=Coinage of 50 Cent Pieces in Commemoration of the One Hundred and Fiftieth Anniversary of the Battle of Bennington and the Independence of Vermont
|date=1925
|url=http://congressional.proquest.com.mutex.gmu.edu/congressional/docview/t29.d30.hrg-1925-cwe-0001?accountid=14541
|publisher=[[United States Government Printing Office]]
|ref={{sfnRef|Vermont House hearings}}
|subscription=yes
}}
{{Coinage (United States)}}
{{Portal bar|Arts|Minnesota|Norway|Numismatics|United States|Visual arts}}

[[Category:1925 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Norwegian-American culture]]
[[Category:Norwegian-American history]]
[[Category:Norwegian migration to North America]]