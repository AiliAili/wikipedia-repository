<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Platz glider
 | image=Platz glider flying.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=foldaway [[glider (aircraft)|glider]]
 | national origin=[[Germany]]
 | manufacturer=
 | designer=[[Reinhold Platz]]
 | first flight=February 1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Platz glider''' was a very simple, though unusual, collapsible [[canard (aeronautics)|canard]] [[glider (aircraft)|glider]] designed and tested in [[Germany]] in the early 1920s. The Platz glider predated the well known [[Rogallo]] designs by over two decades. But in the same decade of the 1920s was a device that had also a high second deck: the Argabrite man-carrying device that featured a triangle undercarriage with wheels on the basebar.<ref>https://scontent-sjc2-1.xx.fbcdn.net/hphotos-xaf1/t31.0-8/10459010_742294779145610_690244495220901145_o.jpg</ref><ref>Aviation Week, December 27, 1927, p. 1087.</ref>

The Platz glider was intended to provide a cheap, easily transported, and simple to fly introduction to the increasingly popular sport.
[[File:Platz glider folded.jpg|thumb|right|Platz glider being transported]]

==Design and development==

In [[Germany]], just after the end of [[World War I]], the 1919 [[Versailles treaty]] imposed a ban on powered flight.  As a result there was a rapid increase of interest in [[gliding]].<ref name=SimonsI/> Rheinhart Platz, the chief designer for [[Fokker]]'s after June 1916,<ref name=Postma1/> perceived a role for a glider that was cheap to buy, costing less than "one good pedal cycle", and cheap to maintain, while being robust and capable of being transported, by train or otherwise, and rapidly erected by one man.

Platz recalled sailing a [[sloop]] rigged boat, which had been very stable upwind and capable of maintaining its course without [[rudder]] input. He reasoned that the same stability he saw in that boat might be achieved by a similarly rigged glider with a small forewing and a larger rear plane.  Just as the sloop could be controlled by adjusting its [[jib]], the glider could be controlled by foreplane trimming.  After some preliminary experiments with simple paper models, Platz designed the one-man [[canard (aeronautics)|canard]] glider which was then named after him.<ref name=S&G/>

The Platz glider was built around a central, two part boom. A curved, circular cross-section steel tube reached from the nose at least as far aft as the welded sockets which received the ends of the main wing [[spar (aviation)|spar]]s. A solid, circular section wood beam was inserted into this steel tube, extending it rearwards.<ref name=S&G/> The wing spars were also circular, solid and wooden, set with strong [[dihedral (aircraft)|dihedral]] which took their tips to the height of the extreme nose so that the foreplanes, [[elevator (aircraft)|elevators]] or jibs could be attached between these three points.  Their inner [[trailing edge]]s were directly controlled by the pilot, who sat over the central beam-wing spar joint. They were initially hinged together at their [[leading edge]]s, but later the hinge point was moved rearwards towards the [[aerodynamic centre]] to reduce pilot load and separated only behind the hinge.  Since there were no ribs, the [[airfoil]] was determined by the airflow and the pilot, as for the sloop's jib.  The main wing, a  single surface stretched between the spars and the extreme tail, also had its [[camber (aerodynamics)|camber]] determined by the airflow, like the mainsail of the sloop.<ref name=S&G/><ref name=Flight/>  Both wing sheets were produced by sewing together narrow strips of material; the longitudinal joints between them are prominent in some back lit, better quality images.<ref name=S&G/><ref name=ARW/>

The Platz could be disassembled into a {{convert|3300|mm|in|abbr=on|0}} × {{convert|350|mm|in|abbr=on|0}} × {{convert|250|mm|in|abbr=on|0}} pack, weighing {{convert|40|kg|lb|abbr=on|0}} in fifteen minutes and reassembled in ten.<ref name=S&G/>  Transport by bicycle, with care, was possible.<ref name=Flight/>

Free flight trials began without pilots and with increasing loads (up to {{convert|75|kg|lb|abbr=on|0}}) into strengthening wind and eventually over sandhills as high as {{convert|25|m|ft|abbr=on|0}}. With a pilot in place, the glider was then flown tethered like a kite. Several people, with weights up to {{convert|100|kg|lb|abbr=on|0}} flew it this way, all reporting that forewing control loads were low.  In February 1923 it was free flown in a moderate wind over {{convert|10|m|ft|abbr=on|0}} dunes.  Platz decided that the dunes did not provide usable soaring, their next goal, after which the experiments would end.  He noted that, whilst his design could not compete with the best conventional gliders, it had met the initial targets outlined above and thought it or something similar would be of great value, seemingly content to leave others to judge his design.<ref name=S&G/>

==Specifications==
[[File:Platz glider schematic.jpg|thumb|right|General layout of the Platz glider; not a scaled or detailed drawing]]
{{Aircraft specs
|ref=Flight, 6 March 1924, p.130<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=
|length ft=
|length in=
|length note=
|span m=6.60
|span note=<ref name=Postma2/>
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=172
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=40
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=SimonsI>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9806773 4 6|page=7}}</ref>

<ref name=ARW>{{cite book |title=Fokker: the creative years |last=Weyl|first=Alfred R.|year=1965|publisher=Putnam|location=London|isbn=}}</ref>

<ref name=Postma1>{{cite book |title=Fokker - Aircraft builders to the World |last=Postma|first=Thijs|year=1980|publisher=Jane's Publishing Company, Ltd,  |location=London|isbn=0 7106 0059 3|page=33}}</ref>

<ref name=Postma2>{{cite book |title=Fokker - Aircraft builders to the World |page=57}}</ref>

<ref name=Flight>{{cite magazine|date=6 March 1924|title= A new idea in gliders|magazine= [[Flight International|Flight]]|volume=XVI|issue=10 |pages=129–30|url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200129.html}}</ref>

<ref name=S&G>{{cite journal|date=6 January 1924|last=Platz|first=Rheinhold|title= A novel sailplane|journal= Sailplane & Glider|volume=21|issue=4 |pages=17–18|url= http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Glider%201930%20-%201955/Volume%2021%20No.%204%20Apr%201953.pdf}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->

[[Category:Canard aircraft]]
[[Category:German sailplanes 1920–1929]]