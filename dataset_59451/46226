{{Geobox|River
<!-- *** Image *** -->
| image                       = Apple Creek sign from Highway 61.jpg
| image_size                  = 250
| image_caption               = Apple Creek (stream) sign from Highway 61

<!-- *** Name section *** -->
|name = Apple Creek 
|native_name =
|other_name =
|other_name1 =
|category = Stream
|category_hide = 
|etymology= [[French language|French]]: ''Rivière à la Pomme'' meaning "Apple River"
|country = [[United States]]
|state = [[Missouri]]
|city = 
<!-- *** Mouth *** -->
|mouth_name = [[Mississippi River]]
|mouth_location = 
|mouth_district =
|mouth_region = 
|mouth_state = 
|mouth_country =
|mouth_lat_d = 37
|mouth_lat_m = 33
|mouth_lat_s = 56
|mouth_lat_NS = N
|mouth_long_d = 89
|mouth_long_m = 31
|mouth_long_s = 37
|mouth_long_EW = W
|mouth_elevation_imperial = 322 
|mouth_elevation_metric = 98 
<!-- Area/codes & others -->
|feature ID = 713356 <ref>Geographic Names Information System (GNIS) http://missouri.hometownlocator.com/maps/feature-map,ftc,1,fid,713356,n,apple%20creek.cfm
</ref>
|water body ID = 
}}

'''Apple Creek''' is a stream that rises in western [[Perry County, Missouri]] and empties into the [[Mississippi River]], forming the boundary between Perry and [[Cape Girardeau County, Missouri|Cape Girardeau]] counties.

==Name==
The name of Apple Creek derives from the French name ''Rivière à la Pomme''.  Since the [[Shawnee Indians]] cultivated farms and had a number of villages along this creek, it is probable that the early French travelers and hunters gave the name "Rivière à la Pomme" (later Americanized to ''Apple River'' or ''Apple Creek''), from the [[apple trees]] which grew there.<ref>{{cite web|url=https://www.sites.google.com/site/burckbuchler2/Lieux/apple-creek--missouri--usa/1-apple-creek-le-nom |title=1. Apple Creek: the Name - BÜRCKBÜCHLER Genealogy |publisher=Sites.google.com |date= |accessdate=2013-11-27}}</ref>

==History==
Although it is not known who the original native inhabitants were of the Apple Creek area, the area eventually become home to the Shawnee and [[Lenape|Delaware]] Indians.  Having originated in present-day [[Delaware]] and [[Pennsylvania]], the Shawnee and Delaware Indians had been pushed off their lands by white settlement.<ref name="nps.gov">{{cite web|url=http://www.nps.gov/history/history/online_books/ozar/hrs2.htm |title=Ozark NSR: A Homeland and Hinterland (Chapter 2) |publisher=Nps.gov |date=2005-03-02 |accessdate=2013-11-27}}</ref>
The Spanish encouraged Shawnee and Delaware immigration, and granted them two large tracts of land in the Apple creek watershed, with the intention of the Shawnee and Delaware acting as a buffer against the [[Osage Indians]], who were not on friendly terms with the Spanish authorities.<ref name="nps.gov"/>  Some 1,200 Shawnee and 600 Delaware were convinced to relocate to the Apple Creek watershed, and in 1784 the group of Shawnee and Delaware migrated and settled down in the area of Old Appleton.  The "[[Le Grand Village Sauvage, Missouri|Grand Village Sauvage]]" was to the west of present-day Old Appleton and the "Petit Village Sauvage" was to the east of Old Appleton.<ref>{{cite journal | url = https://books.google.com/books?id=bGcOAAAAIAAJ&pg=PA213&lpg=PA213&dq=shawnee+villages+apple+creek+history+missouri#v=onepage&q=shawnee%20villages%20apple%20creek%20history%20missouri&f=false | title = A History of Missouri: From the Earliest Explorations and Settlements Until the Admission of the State Into the Union | author1 = Louis Houck | year = 1908}}</ref><ref>{{cite web|url=http://www.nps.gov/jeff/historyculture/shawnee-and-delaware.htm |title=Shawnee and Delaware - Jefferson National Expansion Memorial (U.S. National Park Service) |publisher=Nps.gov |date=2013-11-14 |accessdate=2013-11-27}}</ref>
In the early 1800s, American settlers began encroaching on the Shawnee and Delaware lands around Apple Creek. The Shawnee also requested the Spanish authorities to grant them protection from the Osage in 1807.

By 1815 the situation with white settlers had worsened so much that [[Louisiana Purchase|territorial governor]] [[William Clark (explorer)|William Clark]] and [[United States President]] [[James Madison]] ordered all white intruders removed from Shawnee and Delaware lands.  However, this order was largely ignored by the authorities. By 1816, the encroachment by white settlers left the [[US Federal Government]] no choice but to relocate the Shawnee and Delaware to lands further west.<ref>{{cite journal | url = https://books.google.com/books?id=7j3NNrHFVR4C&pg=PA375&lpg=PA375&dq=shawnee+villages+apple+creek+history+missouri#v=onepage&q=shawnee%20villages%20apple%20creek%20history%20missouri&f=false | title = Opening the Ozarks: A Historical Geography of Missouri's Ste. Genevieve District, 1760-1830 | isbn = 9780826263063 | author1 = Walter A. Schroeder | year = 2002}}</ref><ref>{{cite journal | url = https://books.google.com/books?id=iHr4Z4a6VMEC&pg=PA35&lpg=PA35&dq=shawnee+villages+apple+creek+history+missouri#v=onepage&q=shawnee%20villages%20apple%20creek%20history%20missouri&f=false | title = Missouri: A Guide to the Show Me State | isbn = 9781623760243 | author1 = Federal Writers' Project | year = 1941}}</ref>

==Physical geography==
Apple Creek has a number of [[tributaries]]:<ref>{{cite journal|url=https://books.google.com/books?id=S-1LAAAAMAAJ&pg=PA283&lpg=PA283&dq=apple+creek+indian+creek+perry+county+missouri&source=bl&ots=-08Mh_cbzK&sig=WpodxTZodbn_NhQUI9vNA470E8c&hl=en&sa=X&ei=MeJsUtnhLcWqigKMlYDYAg&ved=0CD0Q6AEwAzgU#v=onepage&q=apple%20creek%20indian%20creek%20perry%20county%20missouri&f=false|title = Reports on the Geological Survey of the State of Missouri: 1855-1871 |author1 =Garland Carr Broadhead|author2=Fielding Bradford Meek|author3=Benjamin Franklin Shumard| year = 1873}}</ref>

{|
|- valign="top"
|
*[[Allie Creek]]
*[[Blue Shawnee Creek]]
*[[Buckeye Creek (Apple Creek)|Buckeye Creek]]
*[[Flatrock Creek (Apple Creek)|Flatrock Creek]]
*Froggy Branch
|
*Gerier Lake
*[[Hughes Creek (Apple Creek)|Hughes Creek]]
*[[Indian Creek (Mississippi River)|Indian Creek]]
*Little Apple Creek
*[[Poor Creek]]
|
*Rocky Branch
*[[Shawnee Creek (Apple Creek)|Shawnee Creek]]
*South Fork Apple Creek
|
|}

==Cultural geography==
Apple Creek rises in the southern part of Perry County and flows east to form the boundary line between Perry and Cape Girardeau counties. A number of bridges cross Apple Creek, including Apple Creek CR 630 bridge the Apple Creek Railroad bridge, the wrought-iron Old Appleton Bridge, and the pony truss Old Appleton US 61 Bridge. A [[Apple Creek, Missouri|town by the same name]] lies north of Apple Creek in Perry County, Missouri. The town of Old Appleton, Missouri, was also earlier known as Apple Creek, but changed its name to Old Appleton in the years leading up to the establishment of its post office in 1876.<ref>{{cite web|url=https://www.sites.google.com/site/burckbuchler2/Lieux/apple-creek--missouri--usa/1-apple-creek-le-nom |title=1. Apple Creek: the Name - BÜRCKBÜCHLER Genealogy |publisher=Sites.google.com |date= |accessdate=2013-11-27}}</ref><ref>{{cite journal | url = https://books.google.com/books?id=G-AlAAAAMAAJ&pg=PA490&lpg=PA490&dq=apple+creek+missouri+history&source=bl&ots=uYyeBs9mfY&sig=zxxi4GZdjqc7SDxqodcsFMvYI7o&hl=en&sa=X&ei=M9jYUeGrBuWgiALX_oCgCQ&ved=0CFwQ6AEwBw#v=onepage&q=appleton&f=false | title = History of Southeast Missouri: A Narrative Account of Its ..., Volume 1 | author1 = Robert Sidney Douglass | year = 1912}}</ref>

==Communities==
*[[Old Appleton, Missouri|Old Appleton]]

==Photo gallery==
<gallery>
File:Apple Creek 1.jpg|Highway 61 bridge over Apple Creek
File:Apple Creek foot bridge.jpg|Apple Creek foot bridge
File:Apple Creek bridge 2.jpg|Highway 61 over Apple Creek
File:Apple Creek foot bridge 2.jpg|Apple Creek foot bridge
</gallery>

==References==
{{Reflist}}

{{Geographical features of Perry County, Missouri}}
{{Perry County, Missouri}}
{{Geographical features of Cape Girardeau County, Missouri}}
{{Cape Girardeau County, Missouri}}
{{Missouri}}

[[Category:Rivers of Missouri]]
[[Category:Landforms of Perry County, Missouri]]
[[Category:Landforms of Cape Girardeau County, Missouri]]