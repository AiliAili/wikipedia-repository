<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, guidelines. -->
{|{{Infobox aircraft begin
 |name= KC-10 Extender
 |image= File:KC-10_Extender (2151957820).jpg
 |caption= A [[United States Air Force]] KC-10 Extender refueling an [[F-16 Fighting Falcon]]|alt=Large three-engined aircraft refueling a jet fighter while two more of the latter fly in the distance.
}}{{Infobox aircraft type
 |type= [[Aerial refueling]] tanker, multi-role aircraft
 |manufacturer= [[McDonnell Douglas]]
 |national origin= United States
 |designer=
 |first flight= 12&nbsp;July&nbsp;1980
 |introduced= March&nbsp;1981
 |retired=
 |status= In service
 |primary user= [[United States Air Force]]
 |more users= [[Royal Netherlands Air Force]]
 |produced= KC-10: 1979–1987
 |number built= KC-10: 60;  KDC-10: 2
 |unit cost= KC-10: {{US$|88.4|link=yes}}&nbsp;million (FY1998)<ref name=USAF_factht/>
 |developed from= [[McDonnell Douglas DC-10]]
 |variants with their own articles =
}}
|}

The '''McDonnell Douglas KC-10 Extender''' is an aerial refueling tanker aircraft – the military version of the three-engined [[McDonnell Douglas DC-10|DC-10]] airliner - operated by the [[United States Air Force]] (USAF).{{#tag:ref|The [[Handley Page Jetstream]] had been initially ordered as the C-10A, but was soon cancelled.  This allowed the tanker version of the DC-10 to be designated KC-10A.|group="N"}} The KC-10 was developed from the '''Advanced Tanker Cargo Aircraft Program'''.  It incorporates military-specific equipment for its primary roles of transport and [[aerial refueling]]. It was developed to supplement the [[KC-135 Stratotanker]] following experiences in [[Southeast Asia]] and the [[Middle East]]. The KC-10 was the second McDonnell Douglas transport aircraft to be selected by the Air Force following the [[McDonnell Douglas C-9|C-9]]. A total of 60 KC-10s were produced for the USAF.  The [[Royal Netherlands Air Force]] operates two similar tankers designated ''KDC-10'' that were converted from DC-10s.

The KC-10 plays a key role in the mobilization of US military assets, taking part in overseas operations far from home. These aircraft performed airlift and aerial refueling during the [[Bombing of Libya (1986)|1986 bombing of Libya]] (Operation Eldorado Canyon), the 1990–91 [[Gulf War]] with Iraq (Operations Desert Shield and Desert Storm), the [[NATO bombing of Yugoslavia]] (Operation Allied Force), [[War in Afghanistan (2001–present)|War in Afghanistan]] ([[Operation Enduring Freedom|Operations Enduring Freedom]]), and [[Iraq War]] (Operations Iraqi Freedom and New Dawn). The KC-10 is expected to serve until 2043.

==Design and development==

===Advanced Tanker Cargo Aircraft Program===
During the [[Vietnam War]], doubts began to be raised regarding the [[Boeing KC-135 Stratotanker]] fleet's ability to meet the needs of the United States' global commitments. The [[aerial refueling]] fleet was deployed to Southeast Asia to support tactical aircraft and strategic bombers, while maintaining the U.S.-based support of the nuclear-bomber fleet. Consequently, the Air Force sought an aerial tanker with greater capabilities than the KC-135. In 1972, two [[McDonnell Douglas DC-10|DC-10]]s were flown in trials at [[Edwards Air Force Base]], simulating air refuelings to check for possible wake issues. Boeing performed similar tests with a 747.<ref name=Waddington>Waddington 2000, pp. 116–120.</ref>

During the 1973 [[Yom Kippur War]], the Air Force commenced [[Operation Nickel Grass]] to supply Israel with weapons and supplies. The operation demonstrated the necessity for adequate air-refueling capabilities; denied landing rights in Europe, [[Lockheed C-5 Galaxy|C-5 Galaxy]] transports were forced to carry a fraction of their maximum payload on direct flights from the continental United States to [[Israel]].<ref>Endres 1998, p.&nbsp;65.</ref><ref name=Steffan_p103>Steffan 1998, p.&nbsp;103.</ref> To address this shortfall in mobility, in 1975, under the ''Advanced Tanker Cargo Aircraft Program'', four aircraft were evaluated&nbsp;––&nbsp;the Lockheed C-5, the [[Boeing 747]], the McDonnell Douglas DC-10, and the [[Lockheed L-1011]].<ref name=Frawley_mil>Frawley 2002, p.&nbsp;119.</ref> The only serious contenders were Boeing and McDonnell Douglas. On 19 December 1977, McDonnell Douglas's DC-10 was chosen. The primary reason of this choice was the KC-10's ability to operate from shorter runways.<ref name=Steffan_p103/>  Initially, a batch of 12 aircraft was ordered, but this was later increased to 60.<ref name=Steffan_p103/>

===KC-10 Extender===
The KC-10 Extender first flew on 12 July 1980, but it was not until October the same year that the first aerial refuel sortie was performed.<ref name=Steffan_p104>Steffan 1998, p.&nbsp;104.</ref><ref name=GS_KC10_bg>{{cite web|title=KC-10A Extender: Background|url=http://www.globalsecurity.org/military/systems/aircraft/kc-10-background.htm |work=Globalsecurity.org|accessdate=27 August 2011}}</ref> The design for the KC-10 involved modifications from the DC-10-30CF design. Unnecessary airline features were replaced by an improved cargo-handling system and military [[avionics]].<ref>Eden 2004, p&nbsp;286.</ref> Meanwhile, the KC-10 retains 88% [[commonality]] with its commercial counterparts, giving it greater access to the worldwide commercial support system.<ref name=Steffan_p103/> Other changes from the DC-10-30CF include the removal of most windows and lower cargo doors.<ref name=GS_KC10_char>{{cite web|url=http://www.globalsecurity.org/military/systems/aircraft/kc-10-characteristics.htm |work=Globalsecurity.org|title=KC-10A Extender: Characteristics|accessdate=27 August 2011}}</ref> Early aircraft featured a distinctive light gray, white and blue paint scheme, but a gray-green camouflage scheme was used on later tankers. The paint scheme was switched to a medium gray color by the late 1990s.<ref name=steffen_p103-7>Steffen 1998, pp.&nbsp;103–107.</ref>

[[File:US Navy 021107-N-2794R-002 KC-10 'Extender' refuels a F-A-18C 'Hornet'.jpg|thumb|The KC-10's mixed refueling system of hose-and-drogue and flying-boom allow it to refuel the aircraft of the U.S. Air Force, U.S. Navy, U.S. Marine Corps and allied forces.<ref name=Steffan_p104/>|alt=A jet aircraft refuels from a gray three-engine tanker via a long boom located under the tanker's aft fuselage.]]

The most notable changes were the addition of the McDonnell Douglas Advanced Aerial Refueling Boom (AARB) and additional fuel tanks located in the baggage compartments below the main deck. The extra tanks increase the KC-10's fuel capacity to 356,000&nbsp;lb (161,478&nbsp;kg), nearly doubling the KC-135's capacity.<ref name=Steffan_p104/> The KC-10 has both a centerline [[Flying boom|refueling boom]] — unique in that it sports a control surface system at its aft end that differs from the [[V-tail]] design used on previous tankers — and a [[Aerial refueling#Probe-and-drogue|drogue-and-hose]] system on the starboard side of the rear fuselage. The KC-10 boom operator is located in the rear of the aircraft with a wide window for monitoring refueling. The operator controls refueling operations through a digital [[Aircraft flight control system#Fly-by-wire control systems|fly-by wire system]].<ref name=USAF_factht>{{cite web |url=http://www.af.mil/AboutUs/FactSheets/Display/tabid/224/Article/104520/kc-10-extender.aspx |title=Factsheets: KC-10 Extender |work=US Air Force |accessdate=30 November 2013}}</ref><ref name=steffen_p103-7/><ref>Eden 2004, p&nbsp;287.</ref> Unlike the KC-135, the KC-10's hose-and-drogue system allows refueling of [[United States Navy|Navy]], [[United States Marine Corps|Marine Corps]], and most allied aircraft, all in one mission.<ref name=Steffan_p104/> The final twenty KC-10s produced included wing-mounted pods for added refueling locations.<ref name="Waddington"/> In addition to its tanking role, the KC-10 can carry a complement 75 personnel with 146,000&nbsp;lb (66,225&nbsp;kg) of cargo, or 170,000&nbsp;lb (77,110&nbsp;kg) in an all-cargo configuration.<ref name=Steffan_p104/>  The KC-10 has a side cargo door for loading and unloading cargo.  Handling equipment is required to raise and lower loads to the cargo opening.

===Further developments===
A need for new transport aircraft for the [[Royal Netherlands Air Force]] (''Koninklijke Luchtmacht'') was first identified in 1984.<ref name="dutch-kdc10-eval"/> The 1991 [[Gulf War]] highlighted the deficiencies in mobility of European forces.<ref>Steffen 1997, p.&nbsp;107.</ref>  In 1991 four categories of transport requirements were established. Category A required a large cargo aircraft with a range of at least 4,500&nbsp;km and the capability to refuel F-16s. In 1992, two DC-10-30CFs were acquired from [[Martinair]] in a buy/[[leaseback]] contract. When one of the two aircraft was lost in the [[Martinair Flight 495]] crash, a third aircraft was bought from Martinair.<ref name="dutch-kdc10-eval"/>

The conversion was handled via the United States [[foreign military sales]] program, which in turn contracted [[McDonnell Douglas]]. Costs for the conversion were initially estimated at $89.5&nbsp;million (FY 1994). The aircraft was to be equipped with both a boom and a probe and drogue system. However, because McDonnell Douglas did not have any experience with the requested Remote Aerial Refueling Operator (RARO) system, and because the third aircraft differed from the original two, the program could not be completed at budget. By omitting the probe and drogue system and a fixed partition wall between the cargo and passenger, the cost could be limited at $96&nbsp;million. To make up for the cost increase McDonnell Douglas hired Dutch companies to do part of the work. The actual converting of the aircraft was done by [[KLM]]. Conversion of the aircraft was done from October 1994 to September 1995 for the first aircraft and from February to December 1995 for the second. This was much longer than planned, mostly because McDonnell Douglas delivered the parts late. This would have again increased the cost, but in the contract for the [[AH-64 Apache]]s which the Royal Netherlands Air Force also bought from McDonnell Douglas, the price was agreed to be kept at $96&nbsp;million.<ref name="dutch-kdc10-eval">{{cite web |url=https://zoek.officielebekendmakingen.nl/kst-26665-1.html |title=Evaluatie project KDC-10 |language=Dutch |work=Ministerie van Defensie |date=1 July 1999 |accessdate=5 January 2014}}</ref>

To modernize the KC-10, the USAF has awarded a contract to Boeing in 2010 to upgrade the fleet of 59 aircraft with new communication, navigation, surveillance and air traffic management (CNS/ATM) system. This was to allow the aircraft to fly in civil airspace as new [[International Civil Aviation Organization|ICAO]] and [[Federal Aviation Administration|FAA]] standards took effect in 2015.<ref name="fg_kc10"/>  Rockwell Collins was also awarded a contract in 2011 for avionics and systems integration for the cockpit modernization program.<ref>{{cite web|url=http://www.defenseindustrydaily.com/Boeing-Wins-CNS-ATM-Upgrade-Contract-for-USAFs-KC-10-Tankers-06448/ |title=Boeing Wins CNS-ATM Upgrade Contract for USAF’s KC-10 Tankers |website=Defenseindustrydaily.com |date=2014-08-21 |accessdate=2017-03-16}}</ref><ref>[https://web.archive.org/web/20110909051542/http://www.rockwellcollins.com:80/sitecore/content/Data/News/2011_Cal_Yr/GS/FY11GSNR27-KC10_cockpit_upgrade.aspx] </ref>

==Operational history==
===United States===
The first KC-10s was delivered to the Air Force's [[Strategic Air Command]] (SAC) in March&nbsp;1981 at [[Barksdale AFB]]; the 60th and final KC-10 arrived on 29&nbsp;November&nbsp;1988.<ref>Steffen 1998, pp.&nbsp;104, 107.</ref>  The KC-10s served with SAC until 1992, when they were reassigned to the newly established [[Air Mobility Command]]. In the aerial refueling role, the KC-10s have been operated largely in the strategic refueling of large number of tactical aircraft on ferry flights and the refueling of other strategic transport aircraft. Conversely, the [[Boeing KC-135 Stratotanker|KC-135]] fleet has operated largely in the in-theater tactical role. There are 59 KC-10 Extenders in service with the USAF as of 2010.<ref name=USAF_factht/><ref name="fg_kc10">{{cite web |last=Trimble |first=Stephen |url=http://www.flightglobal.com/articles/2010/06/24/343673/boeing-outlines-c-130h-and-kc-10-cockpit-upgrades.html |title=Boeing outlines C-130H and KC-10 cockpit upgrades |work=[[Flightglobal.com]] |date= 24 June 2010 |accessdate=19 September 2010}}</ref> The USAF's KC-10s are stationed primarily at [[Travis Air Force Base|Travis AFB]], California, and Joint Base McGuire-Dix-Lakehurst formerly known as [[McGuire Air Force Base|McGuire AFB]], New Jersey.

[[File:USAF F-14D and F-18C's prepare to refuel.jpg|thumb|left|A USN [[F-14D]] and two [[F/A-18C]]s prepare to refuel from a KC-10 in 2005 over the [[Persian Gulf]].]]

When faced with refusals of basing and overflight rights from continental European countries during [[Operation El Dorado Canyon]], the U.S. was forced to use the UK-based [[General Dynamics F-111 Aardvark|F-111s]] in the 1986 air-strikes against [[Libya]]. The KC-10s and KC-135s allowed 29 F-111s, along with other Air Force and Navy aircraft, to reach their targets.<ref>{{cite web |title=Operation El Dorado Canyon |url=http://www.globalsecurity.org/military/ops/el_dorado_canyon.htm |work=Globalsecurity.org |accessdate=27 August 2011}}</ref> The KC-10 again played a key role during [[Operation Desert Shield|Operations Desert Shield]] and [[Operation Desert Storm|Desert Storm]] in 1991; KC-10s facilitated the deployment of tactical, strategic, and transport aircraft to [[Saudi Arabia]]. In the early stages of Operation Desert Shield, aerial refueling was key to the rapid airlift of materiel and forces. In addition to refueling airlift aircraft, the KC-10, along with the smaller KC-135, moved thousands of tons of cargo and thousands of troops in support of the massive buildup. The KC-10 and the KC-135 conducted about 51,700 separate refueling operations and delivered 125 million gallons (475 million liters) of fuel without missing a single scheduled rendezvous.<ref name=USAF_factht/>

Since then, the KC-10 had participated in other smaller conflicts. In March&nbsp;1999, NATO launched [[Operation Allied Force]] against the government of [[Yugoslavia]]. The mobility portion of the operation began in February and was heavily dependent on tankers. By early May&nbsp;1999, some 150 KC-10s and KC-135s deployed to Europe where they refueled bombers, fighters and support aircraft engaged in the conflict. The KC-10 flew 409 missions throughout the entire Allied Force campaign and continued support operations in [[Kosovo]].<ref name=USAF_factht/> Since 11&nbsp;September&nbsp;2001, KC-10s had also flown more than 350 missions guarding U.S. skies as a part of [[Operation Noble Eagle]]. During [[Operation Enduring Freedom|Operations Enduring Freedom]] and [[Operation Iraqi Freedom|Iraqi Freedom]], KC-10s have flown more than 1,390 missions delivering critical air refueling support to numerous joint and Coalition receiver aircraft.<ref name=USAF_factht/> KC-10s are expected to serve until 2043.<ref>Veronico and Dunn 2004, p.&nbsp;58.</ref>

The Air Force considered retiring its fleet of KC-10 tankers in response to sequestration budget cuts as part of the service's FY 2015 budget.  A "vertical chop" to divest all KC-10s was suggested because there are fewer KC-10s than KC-135s, having three different tanker models in service (after the introduction of the [[KC-46]]) would be costly, and a "horizontal cut" across the refueling fleets would achieve small efficiencies.<ref>[http://www.dodbuzz.com/2013/09/17/air-force-may-scrap-kc-10-tanker-fleet-general/ Air Force May Scrap KC-10 Tanker Fleet] – DoDBuzz.com, 17 September 2013</ref> Some believed retiring the KC-10 would not benefit the Air Force, given that it is equipped with both boom and hose-and-drogue refueling systems and the fleet's relatively young age.<ref>{{cite web |url=http://www.defensenews.com/article/20130915/DEFREG02/309150004/ |title=USAF Weighs Scrapping KC-10, A-10 Fleets |last1=WEISGERBER |first1=MARCUS |last2=MEHTA |first2=AARON |date=15 September 2013 |website=www.defensenews.com |publisher=Gannett Government Media Corporation  |accessdate=30 October 2013}}</ref> At first, officials claimed that the initial focus on retiring the KC-10 in September 2013 was a "trial balloon" to call attention to Air Force operating cost issues; as of early 2013, the KC-10 had a per hour flying cost of $21,170 and a mission capable rate of 87 percent.<ref>[http://www.defensemedianetwork.com/stories/air-force-may-dump-all-a-10s/ Air Force May Scrap Entire A-10 Fleet] – Defensemedianetwork.com, 17 October 2013</ref> A FY 2015 budget plan did not include cuts to the KC-10.<ref>[http://www.flightglobal.com/news/articles/how-hagel-spending-plan-will-transform-us-military-396320/ How Hagel spending plan will transform US military] – Flightglobal.com, 26 February 2014</ref>

The KC-10 uniquely has the ability to act as a cargo aircraft and a tanker on overseas deployments of fighters by carrying the fighter support personnel and equipment while simultaneously refueling the fighters.<ref name=USAF_factht/>

===Netherlands===
[[File:McDonnell Douglas KDC-10-30CF der RNLAF.jpg|thumb|The second [[Royal Netherlands Air Force]] KDC-10 with landing gear down]]

The two Dutch KDC-10s are used for both refueling and transport.<ref>Steffen 1997, pp.&nbsp;106–107.</ref> They are stationed on [[Eindhoven Airport]] as part of the 334th Transport Squadron. Of the 5,500 hours flown in the first 3 years of use, the aircraft were used in their tanker role for 50% of the time. Besides being used by the air force and NATO allies, the KDC-10s are also used to support [[peacekeeping]] and [[humanitarian aid]] operations. Of the first three years, 32% of the flight hours were used for peacekeeping and humanitarian aid.<ref name="dutch-kdc10-eval"/>

In this function, the aircraft have been deployed to Kosovo to evacuate refugees, to the [[Caribbean]] and [[Central America]] to provide humanitarian aid after the hurricanes [[Hurricane Luis|Luis]], [[Hurricane Georges|Georges]] and [[Hurricane Mitch|Mitch]] and to various countries in Africa and Asia to provide [[development aid]]. In 1998, the aircraft were also used to evacuate Dutch citizens from Indonesia during the [[Fall of Suharto]]. Dutch KDC-10s have been operating out of [[Transit Center at Manas|Manas AFB]] in support of allied forces during Operation Enduring Freedom.

===Civilian operators===
[[File:Omega Tanker McDonnell Douglas DC-10-40 Avalon Vabre.jpg|thumb|Omega's KDC-10 tanker in March 2009]]

Commercial refueling companies [[Omega Aerial Refueling Services]] and [[Global Air Tanker Service]] operate two KDC-10 tankers (N974VV and N852V, respectively) for lease.<ref>{{cite web|url=http://www.omegaairrefueling.com/FAQs.htm#Question3 |title=Frequently Asked Questions |work=Omega Air Refueling |accessdate=27 August 2011}}</ref><ref>[http://www.globalairtankerservice.com/kdc10.html "KDC-10 Air Refueling Tanker Aircraft."] ''Global Airtanker Service.'' Retrieved: 19 September 2010.</ref>  They were converted from DC-10-40s and provide probe and drogue refueling capabilities from wing pods similar to the KC-10.<ref>[http://www.globalairtankerservice.com/kdc10_po.html "KDC-10: Primary Options."]  ''Global Air Tanker Services''. Retrieved: 25 February 2008.</ref>  In June and July 2011, Omega Air's KDC-10 supported 3 of [[Royal Australian Air Force]]'s [[McDonnell Douglas F/A-18 Hornet|F/A-18 Hornet]]s, en route to [[Red Flag – Alaska]].<ref name="AFM282">"RAAF Hornets Participate in Red Flag Alaska" ''[[Air Forces Monthly]]'' ([[Key Publishing]]), Issue 282, September 2011, pp. 37. {{ISSN|0955-7091}}. Retrieved: 30 September 2011.</ref>

==Operators==
[[File:KC-10 Extender and C-17 Globemaster.jpg|thumb|A KC-10 (right foreground) and [[Boeing C-17 Globemaster III|C-17]] (left background) at [[Avalon Airport]], Australia, for the 2005 [[Australian International Airshow]]|alt=Two large gray jet aircraft on roomy ramp surrounded by grass, both angled away from the runway. The one closer to camera is three-engined, while the one further in the background is fourengined.]]
[[File:USAF KC-10 Extender takeoff at RAF Mildenhall.jpg|thumb|A KC-10 from Travis AFB taking off from [[RAF Mildenhall]]|alt=Large gray jet aircraft with three engines (two under the wings and one under the vertical stabilizer). The aircraft had just lifted off from runway, with landing gear fully extended.]]

;{{USA}}
[[United States Air Force]] – 59 KC-10 aircraft in use as of September 2012.<ref>[https://web.archive.org/web/20131102111137/http://www.airforcemag.com/MagazineArchive/Magazine/2013/0513fullissue.pdf] </ref>
:[[Strategic Air Command]]
*[[2d Bomb Wing]] – [[Barksdale Air Force Base]], Louisiana 1981–92
::2d Air Refueling Squadron 1989–92
::32d Air Refueling Squadron 1981–92
*4th Wing – Seymour-Johnson Air Force Base, North Carolina 1991–92
::344th Air Refueling Squadron
::911th Air Refueling Squadron
*22d Air Refueling Wing – [[March Air Reserve Base|March Air Force Base]], California 1982–92
::6th Air Refueling Squadron 1989–92
::9th Air Refueling Squadron 1982–92
*68th Air Refueling Group/Air Refueling Wing – [[Seymour Johnson Air Force Base]], North Carolina 1982–91
::344th Air Refueling Squadron 1986–91
::911th Air Refueling Squadron 1982–91
*802d Air Refueling Wing, Provisional – [[Lajes Air Base]], [[Portugal]] 1990–91
::802d Air Refueling Squadron
*1701st Air Refueling Wing, Provisional – [[King Abdul Aziz Air Base]], [[Saudi Arabia]] 1990–91
::1710th Air Refueling Squadron
:[[Air Mobility Command]]
*[[22d Air Refueling Wing]] – March Air Force Base, California 1992–94
::6th Air Refueling Squadron
::9th Air Refueling Squadron
:4th Operations Group – Seymour-Johnson Air Force Base, North Carolina 1992–94
::[[344th Air Refueling Squadron]]
::[[911th Air Refueling Squadron]]
:458th Operations Group – Barksdale Air Force Base, Louisiana  1992–94
::2d Air Refueling Squadron
::32d Air Refueling Squadron
*[[60th Air Mobility Wing]] – [[Travis Air Force Base|Travis AFB]], California 1994–
::[[6th Air Refueling Squadron]] 1995–
::[[9th Air Refueling Squadron]] 1994–
*[[305th Air Mobility Wing]] – [[Joint Base McGuire-Dix-Lakehurst]] formerly known as [[McGuire Air Force Base|McGuire AFB]], New Jersey 1994–
::[[2d Air Refueling Squadron]] 
::[[32d Air Refueling Squadron]]
*[[380th Air Expeditionary Wing]] – [[Al Dhafra Air Base]], [[United Arab Emirates]] 1993–
::908th Expeditionary Air Refueling Squadron
*722d Air Refueling Wing – March Air Force Base, California 1994–95
::706th Air Refueling Squadron 1994–95
::709th Air Refueling Squadron 1994-95
'''[[Air Force Reserve Command]]'''
*98th Air Refueling Group (Associate) – Barksdale Air Force Base, Louisiana 1987–94
::78th Air Refueling Squadron
*[[349th Air Mobility Wing]] (Associate) – [[Travis Air Force Base|Travis AFB]], California 1994–
::[[70th Air Refueling Squadron]] 1994–
::[[79th Air Refueling Squadron]] 1995–
*[[413th Flight Test Group]] – [[Robins AFB]], [[Georgia (U.S. state)|Georgia]] 2003–
::[[370th Flight Test Squadron]] ([[Edwards AFB]], [[California]])
*452d Air Refueling Wing/Air Mobility Wing (Associate) – March Air Force Base, California 1981–95
::78th Air Refueling Squadron 1981–87
::79th Air Refueling Squadron 1982–95
*[[514th Air Mobility Wing]] (Associate) – [[McGuire Air Force Base|McGuire AFB]], New Jersey 1994–
::[[76th Air Refueling Squadron]]
::[[78th Air Refueling Squadron]]
*916th Air Refueling Group (Associate) – Seymour-Johnson Air Force Base, North Carolina 1985–94
::[[77th Air Refueling Squadron]]

;{{NLD}}
*[[Royal Netherlands Air Force]] operates 2 KDC-10s
**[[334th Squadron]] – [[Eindhoven Airport]]

==Incidents==
On 17 September 1987, KC-10A serial number ''82-0190'' was undergoing maintenance on the ground at [[Barksdale Air Force Base|Barksdale AFB]], [[Louisiana]] and suffered an explosion and subsequent fire.  The KC-10 was significantly damaged and written-off.  One member of the ground crew, Sgt. Joseph M. Burgio died in the fire.<ref>[http://aviation-safety.net/database/record.php?id=19870917-1 "McDonnell Douglas KC-10A 82-0190."] ''[[Aviation Safety Network]] Database.'' Retrieved: 19 September 2010.</ref>

==Specifications (KC-10A)==
{{externalimage |topic=McDonnell Douglas KC-10A Cutaway |width= |align=right |image1=[http://www.flightglobal.com/airspace/media/militaryaviation1946-2006cutaways/images/14160/mcdonnell-douglas-kc-10a-cutaway.jpg McDonnell Douglas KC-10A Cutaway from] [[Flightglobal.com]]}}

{{aircraft specifications
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]]
     Please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Airtemp]].
-->
|ref=USAF Fact sheet,<ref name=USAF_factht/> Steffen<ref name=steffen_p107>Steffen 1998, p. 107.</ref>
<!--
      General characteristics -->
|crew=4 (Aircraft Commander, copilot, flight engineer, boom operator); additional crewmembers such as an aeromedical evacuation team and cargo handlers may be added
|length main=181 ft 7 in
|length alt=55.35 m
|span main=165 ft 4.5 in
|span alt=50.41 m
|height main=58 ft 1 in
|height alt=17.70 m
|area main=3,958 ft²
|area alt=367.71 m²
|empty weight main=241,027 lb
|empty weight alt=109,328 kg
|loaded weight main=593,000 lb
|loaded weight alt=268,980 kg
|max takeoff weight main=590,000 lb
|max takeoff weight alt=267,620 kg
|more general= '''Maximum fuel capacity:''' 356,000 lb (161,480 kg)
<!--
     Powerplant -->
|engine (jet)=[[General Electric CF6|F103/General Electric CF6-50C2]]
|type of jet=[[turbofan]]s
|number of jets=3
|thrust main=52,500 [[pound-force|lb<sub>f</sub>]]
|thrust alt=236 kN
<!--
     Performance -->
|max speed main=538 knots/0.89 mach
|max speed alt=619 mph, 996 km/h
|range main=4,400 mi
|range alt=7,080 km
|range more=
|ferry range main=11,500 mi
|ferry range alt=18,507 km
|ferry range more=
|ceiling main=42,000 ft
|ceiling alt=12,800 m
|climb rate main=6,870 ft/min
|climb rate alt=2094 m/min., 34.9 m/s
|loading main=
|loading alt=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{Portal|United States Air Force|Aviation}}
{{aircontent
|related=
*[[McDonnell Douglas DC-10]]
|similar aircraft=
*[[Lockheed TriStar (RAF)]]
*[[Ilyushin Il-78]]
|lists=
*[[List of active United States military aircraft]]
* [[List of United States military aerial refueling aircraft]]
|see also=
}}

==References==
;Notes
{{Reflist|group=N}}

;Citations
{{Reflist|30em}}

;Bibliography
{{Refbegin}}
* {{Cite book|editor-last=Eden|editor-first=Paul|title=The Encyclopedia of Modern Military Aircraft|location=London|publisher=Amber Books|year=2004|isbn=1-904687-84-9|ref={{harvid|Eden|2004}}}}
* {{Cite book|last=Frawley|first=Gerard|title=The International Directory of Military Aircraft, 2002–2003|location=Fyshwick, Australia|publisher=Aerospace Publications|year=2002|isbn=1-875671-55-2}}
* {{Cite book|last=Steffen|first=Arthur A. C.|title=McDonnell Douglas DC-10 and KC-10 Extender|location=Hinckley, Leicester, UK|publisher=Midland Publishing|year=1998|isbn=1-85780-051-6}}
* {{Cite book|last1=Veronico|first1=Nick |last2=Dunn|first2=Jim|url=https://books.google.com/books?id=3zz3KLbOvYUC&pg=PA83&dq=E-3+Sentry&hl=en&ei=MvZOTtq0Eo3ymAW3w_nwBg&sa=X&oi=book_result&ct=book-preview-link&resnum=10&ved=0CF4QuwUwCQ#v=onepage&q&f=false|title=21st Century U.S. Air Power|location= Grand Rapids, Michigan|publisher=Zenith Imprint|year=2004|isbn=978-0-7603-2014-3}}
* {{Cite book|last=Waddington|first=Terry|title=McDonnell Douglas DC-10|location=Miami, Florida|publisher=World Transport Press|year=2000|isbn=1-892437-04-X}}
* {{Cite book|last=Endres|first=Gunter|title=McDonnell Douglas DC-10, Volume 10|location=Grand Rapids, Michigan|publisher=Zenith Imprint|year=1998|isbn=978-0-7603-0617-8}}
{{Refend}}

==External links==
{{Commons category|McDonnell Douglas KC-10 Extender}}
*[http://www.boeing.com/defense-space/military/kc10/index.html KC-10 page on Boeing.com]
*[http://www.af.mil/information/factsheets/factsheet.asp?id=109 USAF KC-10 fact sheet]
*[http://www.kc-10.net/ KC-10 web site]
*[http://www.theaviationzone.com/factsheets/kc10.asp KC-10 fact sheet on TheAviationZone.com]
*[http://www.worldwide-military.com/Military%20Aircraft/Tankers/KDC-10_Algemene_info_english.htm KDC-10 in the Dutch Air Force]
*[http://www.ts.northropgrumman.com/kc-10/index.html KC-10 CLS Competition]
*{{Youtube|XgjZul4P7X8|McDonnell Douglas promotional video of the KC-10 Extender}}

{{McDD aircraft}}
{{US transport aircraft}}

{{Use dmy dates|date=September 2010}}

{{DEFAULTSORT:Kc-10 Extender}}
[[Category:United States military tanker aircraft 1980–1989]]
[[Category:Air refueling]]
[[Category:1981 introductions]]
[[Category:Trijets]]