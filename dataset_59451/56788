{{Orphan|date=July 2015}}

{{Infobox journal
| title = Epigenetics & Chromatin
| cover = 
| editor = [[Frank Grosveld]], Steven Henikoff
| discipline = [[Epigenetics]]
| abbreviation = Epigenetics Chromatin
| publisher = [[BioMed Central]]
| country =
| frequency =
| history = 2008-present
| openaccess = Yes
| license = [[Creative Commons Attribution License 4.0]]
| impact = 5.333
| impact-year =
| website = http://www.epigeneticsandchromatin.com
| link1 =
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 263688252
| LCCN = 
| CODEN = ECPHAK
| ISSN = 
| eISSN = 1756-8935
}}
'''''Epigenetics & Chromatin''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] that covers the biology of [[epigenetics]] and [[chromatin]]. It was established in 2008 and is published by [[BioMed Central]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[BIOSIS Previews]],<ref name=ISI/> [[Chemical Abstracts Service]],<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-07-03 }}</ref> [[Embase]],<ref name=Embase>{{cite web |url=http://www.elsevier.com/online-tools/embase/about |title=Journal titles covered in Embase |publisher=[[Elsevier]] |work=Embase |accessdate=2015-07-03}}</ref> [[Science Citation Index Expanded]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-03}}</ref> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-03}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.333.<ref name=WoS>{{cite book |year=2015 |chapter=JOURNALNAME |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== Conference ==
BioMed Central hosted a conference titled "Epigenetics & Chromatin: Interactions and processes" at [[Harvard Medical School]] on March 11–13, 2013. Abstracts from the conference were published in ''Epigenetics & Chromatin''.<ref>{{cite web |url=http://www.epigeneticsandchromatin.com/conference |title=Epigenetics & Chromatin:Interactions and Processes |accessdate=2015-06-30}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.epigeneticsandchromatin.com}}

{{DEFAULTSORT:Epigenetics and Chromatin}}
[[Category:BioMed Central academic journals]]
[[Category:Online-only journals]]
[[Category:Genetics journals]]
[[Category:Publications established in 2008]]
[[Category:English-language journals]]
[[Category:Creative Commons Attribution-licensed journals]]


{{genetics-journal-stub}}