[[File:Joseph Sadi-Lecointe.jpg|thumbnail|Sadi-Lecointe in 1919]]
'''Joseph Sadi-Lecointe''' (1891 &ndash; 1944) was a [[France|French]] aviator, best known for breaking a number of speed and altitude records in the 1920s.

==Biography==
Sadi-Lecointe was born on 11 July 1891 at [[Saint-Germain-sur-Bresle]].  He learned to fly at the Zenith school at [[Issy-les-Moulineaux]] in 1910<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200532.html The Zenith School at Issy][[Flight International|''Flight'']], 9 July 1910</ref> and was awarded French Aero Club license No. 431 on 3 May 1911 <ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200439.html More French Pilots][[Flight International|''Flight'']], 20 May 1911.</ref>  Before formally qualifying, he had taken [[Georges Clemenceau]] for a short flight<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200182.html M. Clemenceau in the Air] [[Flight International|''Flight'']], 4 March 1911</ref>

During the First World War he saw active service with Escadrille BL.10 and later, flying Nieuport scouts, with Escadrille MS.48. He became a flying instructor in 1916 and in September 1917 he became a test pilot with Bleriot-SPAD, working on the development of the [[SPAD XIII]].<ref name=ACSL>{{cite web|publisher=Aéro-Club Sadi-Lecointe|title=Sadi-Lecointe, un Grand Figure de l'aviation|language=french|accessdate= 8 January 2013}}</ref>

After the war he became a test pilot for Nieuport-Delage, flying their aircraft in a number of races and also using them to set seven speed and three altitude records. He was the winner of the 1920 [[Gordon Bennett Trophy (aeroplanes)|Gordon Bennett]] race, securing permanent possession of the trophy for the ''Aero Club de France'' and was to fly the French entry for the 1921 [[Schneider Trophy]] at Venice, but had to withdraw after an accident during practice.<ref>[http://www.flightglobal.com/pdfarchive/view/1929/1929-1%20-%200968.html Venice 1921][[Flight International|''Flight'']] 6 September 1929</ref>

Between 1925 and 1927 he returned to military service as a volunteer, taking part in the [[Rif War]] in [[Morocco]], afterwards returning to his job as chief test pilot with Nieuport Delage.

In 1936 he was appointed Inspector General of Aviation by the French Air Ministry. Mobilised at the outbreak of the Second World War, he became the Inspector of Flying Schools. His political sympathies did not allow him to serve under the [[Vichy government]] after the fall of France in 1940, and instead he was active in the [[French Resistance]]. On 21 March 1944 he was arrested by the [[Gestapo]] and held in [[Fresnes prison]]. Released after two months, he died on 15 July 1944 as a result of being tortured while in prison.<ref>Villard 1987, p.346.</ref>

==Records==
'''Speed'''
*7 February 1920 &mdash; Speed over 1&nbsp;km &mdash; {{convert|275.86|kph|mph|abbr=on}} &mdash; [[Nieuport-Delage NiD 29| Nieuport-Delage NiD 29V]]<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15467|publisher=FIA|title=FIA Record ID#15467 |accessdate= 8 January 2013}}</ref>
*25 September 1920 &mdash; Speed over 100&nbsp;km &mdash; {{convert|279.50|kph|mph|abbr=on}} &mdash; Nieuport-Delage NiD 29<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15489|publisher=FIA|title=FIA Record ID#15489   |accessdate= 8 January 2013}}</ref>
*28 September 1920 &mdash; Speed over 200&nbsp;km &mdash; {{convert|274.60|kph|mph|abbr=on}} &mdash; Nieuport-Delage NiD 29<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15494|publisher=FIA|title=FIA Record ID#15494   |accessdate= 8 January 2013}}</ref>
*10 October 1920 &mdash; Speed over 1&nbsp;km &mdash; {{convert|296.69|kph|mph|abbr=on}}{{convert|275.86|kph|mph|abbr=on}} &mdash; Nieuport-Delage NiD 29V Bis <ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15498|publisher=FIA|title=FIA Record ID#15498 |accessdate= 8 January 2013}}</ref>   
*20 October 1920 &mdash; Speed over 1&nbsp;km &mdash; {{convert|302.53|kph|mph|abbr=on}}{{convert|275.86|kph|mph|abbr=on}} &mdash; Nieuport-Delage NiD 29V Bis  <ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15499|publisher=FIA|title=FIA Record ID#15499 |accessdate=8 January 2013}}</ref>
*15 February 1923 &mdash; Speed over 1&nbsp;km &mdash; {{convert|375|kph|mph|abbr=on}} &mdash; [[Nieuport-Delage NiD 42|Nieuport-Delage NiD 42S]]  <ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=15279|publisher=FIA|title=FIA Record ID#15279 |accessdate= 8 January 2013}}</ref> 
*23 June 1924 &mdash; Speed over a distance of {{convert|500|km|mi|abbr=on}} &mdash; {{convert|306.70|kph|mph|abbr=on}} &mdash; [[Nieuport-Delage NiD 42]]<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=14618|publisher=FIA|title=FIA Record ID#14618   |accessdate= 8 January 2013}}</ref>
 
'''Altitude'''
*5 September 1923 &mdash; {{convert|10741 |m|ft|abbr=on}} &mdash; [[Nieuport-Delage NiD 29#Variants|Nieuport-Delage NiD 40R]]  <ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=8246|publisher=FIA|title=FIA Record ID#8246   |accessdate= 8 January 2013}}</ref> 
*30 October 1923 &mdash;  {{convert|11145 |m|ft|abbr=on}} &mdash; Nieuport-Delage NiD 40R <ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=8223|publisher=FIA|title=FIA Record ID#8223   |accessdate= 8 January 2013}}</ref>    
*11 March  1924 &mdash; {{convert|8980 |m|ft|abbr=on}}   Nieuport-Delage NiD 40RH (altitude record for floatplanes)<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=11750|publisher=FIA|title=FIA Record ID#11750   |accessdate= 8 January 2013}}</ref>

==Honours==
*[[Legion of Honour|Commandeur de la Légion d’Honneur]]<ref>Villard 1887, p.146</ref>
*[[Croix de Guerre 1914-1918]]
*[[Croix de guerre des théâtres d'opérations extérieures]]
*[[Croix de Guerre 1939-1945]]
*[[Médaille de la Résistance]]
*Grande Médaille d’Or de l’Aéro-Club de France
 
There is a street in the XIXieme arrondissement of Paris named in his honour.<ref>Villard 1987, p.247.</ref>

==Notes==
{{reflist}}

==References==
*[[Henry Serrano Villard|Villard, Henry S]] ''The Blue Riband of the Air''. Washington D.C.:Smithsonian Institution Press, 1987. ISBN 087474942 5

{{Authority control}}

{{DEFAULTSORT:Sadi-Lecointe, Joseph}}
[[Category:1891 births]]
[[Category:1944 deaths]]
[[Category:French aviators]]
[[Category:Commandeurs of the Légion d'honneur]]
[[Category:Flight altitude record holders]]