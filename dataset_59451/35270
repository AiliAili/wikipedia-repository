{{good article}}
{{Infobox individual snooker tournament
|tournament_name=[[Malta Cup|European Open]]
|dates=1–6 March 2004
|venue=[[Hilton Conference Centre]]
|location=[[Portomaso]]
|country=[[Malta]]
|organisation=[[World Professional Billiards and Snooker Association|WPBSA]]
|format=[[Snooker world rankings|Ranking event]]
|Total prize fund=[[Pound sterling|£]]400,800
|winners_share=£48,000
|highest_break=
|winner={{flagicon|SCO}} [[Stephen Maguire]]
|runner_up={{flagicon|ENG}} [[Jimmy White]]
|score=9–3
|previous=[[2003 European Open (snooker)|2003]]
|next=[[2005 Malta Cup|2005]]}}

The '''2004 European Open''' was the 2004 edition of the [[Malta Cup|European Open]] [[snooker]] [[List of snooker tournaments|tournament]], held from 1 to 6 March 2004, at the [[Hilton Conference Centre]], [[Portomaso]], [[Malta]]. It was the final year the event was known as European Open, as the event was renamed to Malta Cup in next year. [[Stephen Maguire]] defeated [[Jimmy White]] by nine {{Cuegloss|frame|frames}} to three (9–3) in the final to claim his first [[List of snooker players by number of ranking titles|ranking-event title]], transforming him from "talented underachiever into a world-ranking event winner", according to ''[[The Times]]''. In the semi-finals Maguire defeated [[Stephen Lee (snooker player)|Stephen Lee]] and White beat [[Tony Drago]]. The tournament was the fifth of eight [[World Professional Billiards and Snooker Association|WPBSA]] ranking events in the [[2003/2004 snooker season|2003/2004 season]], following the [[2004 Welsh Open (snooker)|Welsh Open]] and preceding the [[2004 Irish Masters|Irish Masters]].

== Tournament summary ==
Prior to the 1988/1989 season no [[Snooker world rankings|ranking tournament]] had been continuously staged outside of the United Kingdom (although the [[World Snooker Championship|World Championship]] had been held twice in Australia). The [[snooker]] governing body, the [[World Professional Billiards and Snooker Association]] (WPBSA), decided to include overseas events and the first two locations chosen were Canada and Europe. The European Open was first held in [[1989 European Open (snooker)|1989]] in Deauville, France, and was suspended for 1997/1998 and 2000/2001. It moved to the [[Hilton Conference Centre]], [[Portomaso]], [[Malta]] for the first time in 2004 and was renamed the [[Malta Cup]] the following season.<ref>[http://www.cajt.pwp.blueyonder.co.uk/Euro.html European Open. German Open.  German Masters. Irish Open]. cajt.pwp.blueyonder.co.uk (Chris Turner's Snooker Archive). Retrieved 12 September 2010. [http://web.archive.org/web/20120216155305/http://www.cajt.pwp.blueyonder.co.uk/Euro.html Archived] 16 February 2012</ref>

The 2004 tournament was the fifth of eight WPBSA ranking events in the [[2003/2004 snooker season|2003/2004 season]], following the [[2004 Welsh Open (snooker)|Welsh Open]] and preceding the [[2004 Irish Masters|Irish Masters]].<ref>[http://www.snooker.org/trn/0304/eo2004_res.shtml "European Open 2004"]. Snooker.org. Retrieved 12 September 2010.</ref> Held in January, the Welsh Open was won by [[Ronnie O'Sullivan]], who defeated [[Steve Davis]] by nine {{Cuegloss|frame|frames}} to eight (9–8) in the final.<ref>[http://www.snooker.org/trn/0304/wo2004_res.shtml "Welsh Open 2004"]. Snooker.org. Retrieved 12 September 2010.</ref> The defending European Open champion was also O'Sullivan, who defeated [[Stephen Hendry]] 9–6 in [[2003 European Open|last year's]] final.<ref>[http://www.snooker.org/trn/0203/eo2003_res.shtml "European Open 2003"]. Snooker.org. Retrieved 12 September 2010.</ref> [[Paul Hunter (snooker player)|Paul Hunter]], who had defeated O'Sullivan in the final of the non-ranking [[2004 Masters (snooker)|Masters]] in February, entered the tournament "playing the best snooker of his career", according to Phil Yates of ''[[The Times]]''.<ref>Yates, Phil. [http://www.timesonline.co.uk/tol/sport/article1033533.ece "Hunter hoping to sustain peak form"]. ''[[The Times]]''. 1 March 2004. Retrieved 2 October 2010.</ref>

=== Qualifying ===
The qualifying stage was played between players [[Snooker world rankings 2003/2004|ranked]] 17 and those ranked lower for one of 16 places in the final stage. The matches were best-of-9 frames until the semi-finals. In March 2004 Maltese player [[Tony Drago]] defeated [[Adrian Gunnell]] 5–2 in a match held over from the qualifying stage in November.<ref>Proby, Johnny. [http://www.rte.ie/sport/2004/0302/snooker.html "Irish interest ends in Malta"]. [[RTÉ Sport]]. 2 March 2004. Retrieved 12 September 2010.</ref>

=== Round 1 ===
The qualifiers went through to face members of the top 16. In this round, Davis came from 2–4 down to beat [[Joe Swail]] 5–4, in a match where both players missed chances. In the deciding frame, Davis won on the [[Billiard ball#Snooker|pink ball]] after Swail had missed the brown. After the match, Davis said it was a historic day as he had never won a match in the country.<ref name=BBC1Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3523069.stm "Davis squeezes past Swail"]. [[BBC Sport]]. 1 March 2004. Retrieved 12 September 2010.</ref> [[Stephen Lee (snooker player)|Stephen Lee]] received a walkover to the next round after his opponent [[Robin Hull]] withdrew due to medical reasons.<ref name=BBC1Mar04 /><ref name=Yates2Mar04>Yates, Phil. [http://www.timesonline.co.uk/tol/sport/article1034911.ece "Small overcomes pain and Stevens"]. ''[[The Times]]''. 2 March 2004. Retrieved 12 September 2010.</ref> World number 41 [[Stephen Maguire]] made a {{Cuegloss|break|break}} of 89 in the final frame of his match against [[Peter Ebdon]] to win 5–4, and world number three O'Sullivan opened his match against [[Marco Fu]] with a [[century break]] of 110 and went on to win 5–1.<ref name=BBC1Mar04 /> [[Chris Small]], who suffers from the spinal condition [[ankylosing spondylitis]], whitewashed [[UK Championship (snooker)|UK champion]] [[Matthew Stevens]] 5–0, in a match lasting almost three hours and which saw Stevens lose two frames on the black ball.<ref name=Yates2Mar04 /><ref>Dee, John. [http://www.telegraph.co.uk/sport/othersports/snooker/2374097/Snooker-OSullivan-ahead-in-century-race.html "Snooker: O'Sullivan ahead in century race"]. ''[[The Daily Telegraph]]''. 2 March 2004. Retrieved 12 September 2010.</ref> [[Neil Robertson (snooker player)|Neil Robertson]] defeated [[Ken Doherty]] 5–3, and [[Joe Perry (snooker player)|Joe Perry]] beat [[David Roe]] by the same scoreline. In the last match of the day, [[David Gray (snooker player)|David Gray]] beat [[Fergal O'Brien]] 5–3.<ref name=BBC1Mar04 />

[[List of world number one snooker players|World number one]] [[Mark Williams (snooker player)|Mark Williams]] was defeated 1–5 by [[Anthony Hamilton (snooker player)|Anthony Hamilton]], who made a break of 133.<ref name=Dee3Mar04>Dee, John. [http://www.telegraph.co.uk/sport/othersports/snooker/2374161/Snooker-Williams-shocked-by-Hamilton.html "Snooker: Williams shocked by Hamilton"]. ''[[The Daily Telegraph]]''. 3 March 2004. Retrieved 2 October 2010.</ref> Williams refused to answer questions at the post-match press conference, explaining: "I'm not saying anything because if I do I could be in trouble so I'm keeping my mouth shut."<ref name=BBC2Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3526645.stm "Williams falls at the first"]. [[BBC Sport]]. 2 March 2004. Retrieved 12 September 2010.</ref> Hunter and Hendry made high breaks of 49 and 55 in defeating [[Brian Morgan (snooker player)|Brian Morgan]] and [[Jimmy Michie]] 5–1, respectively.<ref name=Dee3Mar04 /> After the match Hendry—the world number two and a seven-time [[World Snooker Championship|world champion]] said "I feel like going to apologise to each fan one by one because it was such a bad match".<ref name=BBC2Mar04 /> [[John Higgins (snooker player)|John Higgins]] whitewashed [[Barry Pinches]] 5–0, compiling a 132 break in the last frame in a match where Pinches made a high break of 33.<ref>Proby, Johnny. [http://www.rte.ie/sport/2004/0302/higginsj.html "Higgins easily through in Malta"]. [[RTÉ Sport]]. 2 March 2004. Retrieved 2 October 2010.</ref> [[Jimmy White]] overcame [[James Wattana]] 5–4 having trailed 2–3. [[Graeme Dott]] defeated [[Drew Henry]] 5-3 and [[Quinten Hann]] beat [[Simon Bedford]] 5–0. In the last game of the day, Drago beat [[Alan McManus]] 5–4.<ref name=BBC2Mar04 />

=== Round 2 ===
In round two O'Sullivan defeated Small 5–1, coming from behind to win in each of the first three frames with breaks of 58, 81, and 46. A break of 112 completed the victory, after which O'Sullivan said his opponent had made him work.<ref>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3529815.stm "O'Sullivan eases past Small"]. [[BBC Sport]]. 3 March 2004. Retrieved 12 September 2010.</ref> In the fifth frame O'Sullivan continued playing despite needing snookers, later explaining, "I wanted to keep playing because I was enjoying it so much".<ref>[http://www.rte.ie/sport/2004/0303/snooker.html "O'Sullivan into quarters, Davis falls to Lee"]. [[RTÉ Sport]]. 3 March 2004. Retrieved 12 September 2010.</ref> White made breaks of 72, 52, 69, 51, and 65 in defeating Hendry 5–3, after which Hendry said his performance was "horrendous", and White said his refusal to go out the night before contributed to his performance.<ref name=BBC3Mar2004b>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3530879.stm "White too strong for Hendry"]. [[BBC Sport]]. 3 March 2004. Retrieved 2 October 2010.</ref> In the sixth frame White led by 41 points before missing an easy red, allowing Hendry to win on the black with a 55 break to level at 3–3. White dominated the next two frames for the victory.<ref name=Yates4Mar04>Yates, Phil. [http://www.timesonline.co.uk/tol/sport/article1036567.ece "Drago earns place in last eight with help from the locals"]. ''[[The Times]]''. 4 March 2004. Retrieved 12 September 2010.</ref> Higgins, without a tournament victory for over two years, whitewashed Dott 5–0 with breaks of 82, 81, 57, and 52, and said it was the "best [he had] felt for ages".<ref>Dee, John. [http://www.telegraph.co.uk/sport/othersports/snooker/2374204/Snooker-White-hot-effort-bad-for-Hendry.html "Snooker: White hot effort bad for Hendry"]. ''[[The Daily Telegraph]]''. 4 March 2004. Retrieved 2 October 2010.</ref> Drago defeated Hunter 5–2 to reach the quarter-finals of a ranking events for the first time since 1998, in a low-quality match where Drago made one break over 50. Lee defeated Davis 5–3 in a four-hour match,<ref name=Yates4Mar04 /> and Higgins completed a second whitewash when he beat Dott 5–0, bringing his career record against Dott to 9–1.<ref>Proby, Johnny. [http://www.rte.ie/sport/2004/0303/snooker1.html "White and Higgins safely through"]. [[RTÉ Sport]]. 3 March 2004. Retrieved 2 October 2010.</ref> Maguire defeated Perry 5–4 to reach his first ranking quarter-final,<ref>Dee, John. [http://www.telegraph.co.uk/sport/othersports/snooker/2374336/Snooker-Maguire-reaches-first-final.html "Snooker: Maguire reaches first final"]. ''[[The Daily Telegraph]]''. 6 March 2004. Retrieved 2 October 2010.</ref> and Hann beat Hamilton 5–1 to claim the final place in the next round.<ref name=BBC3Mar2004b />

=== Quarter-finals ===
In the quarter-finals Lee defeated O'Sullivan 5–4 in a match that lasted 3 hours and 32 minutes. O'Sullivan came from 2–3 down to lead 4–3 before Lee leveled the match. In the final frame O'Sullivan led 36–0, before Lee made a 46 break and fluked a snooker that enabled him to claim victory. O'Sullivan said his performance was very poor, while Lee said he was quietly confident.<ref name=BBC4Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3533613.stm "Lee ends O'Sullivan hopes"]. [[BBC Sport]]. 4 March 2004. Retrieved 2 October 2010.</ref><ref>Dee, John. [http://www.telegraph.co.uk/sport/othersports/snooker/2374282/Snooker-OSullivan-off-colour.html "Snooker: O'Sullivan off colour"]. ''[[The Daily Telegraph]]''. 5 March 2004. Retrieved 2 October 2010.</ref> White defeated Robertson 5–3 to reach his third semi-final of the season. White led 4–1 and was 56 points ahead in the sixth but missed a straightforward green, allowing Robertson to win on the black with a 67 break. Robertson took the next frame before a risky long pot in the eighth allowed White to win the match. White—who last won a ranking title [[1992 European Open (snooker)|12 years ago prior at this event]]—said, "Everyone knows I've been in front so many times in the past and tossed it away so I was having nightmares out there".<ref>Yates, Phil. [http://www.timesonline.co.uk/tol/sport/article1037699.ece "White wobbles on way to semi-finals"]. ''[[The Times]]''. 5 March 2004. Retrieved 2 October 2010.</ref> Drago quickly won the first four frames of his match against Hann, conceding the fifth, before completing a 5–1 victory, after which he said the crowd support helped him. Maguire caused an upset when he beat Higgins 5–3, a match that saw Maguire lose the first two frames before winning the next four. After the match Maguire he said he had "starting to think about winning it now".<ref name=BBC4Mar04 />

=== Semi-finals ===
The semi-finals were best-of-11 frames. White reached his first final in four years when he defeated Drago 6–4. Leading 4–1 White made a break of 104 to win the sixth frame, before missing a straightforward red to allow his opponent to win the seventh with an 84 break. Drago won the next two in 15 minutes with breaks of 44 and 109—completing the latter in four minutes<ref>Boylan, James. [http://www.rte.ie/sport/2004/0306/snooker.html "White seals final place"]. [[RTÉ Sport]]. 6 March 2004. Retrieved 2 October 2010.</ref>—before an 86 break gave White the victory, after which White said, "Playing Tony here, I got a taste of what players have against me at the Masters when the crowd are all on my side but they were fair and I enjoyed every minute of it."<ref name=Yates6Mar04>Yates, Phil. [http://www.timesonline.co.uk/tol/sport/article1038946.ece "White on verge of glory after composed display"]. ''[[The Times]]''. 6 March 2004. Retrieved 12 September 2010.</ref><ref name=BBC5Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3537865.stm "White through to final"]. [[BBC Sport]]. 5 March 2004. Retrieved 12 September 2010.</ref>

In the other semi-final Maguire beat Lee 6–4 in an error-strewn match which lasted four hours.<ref name=Yates6Mar04 /> After winning the first two frames Maguire lost the next three, but "kept his cool" to seal the victory.<ref name=BBC5Mar04 /> Maguire said the match was a "battle" and that he was surprised at how badly his opponents had played in the tournament, while Lee said he "just blew up" and that, "When you’re as poor as that you get into such a state of mind that you can’t think straight".<ref name=Yates6Mar04 /><ref name=BBC5Mar04 />

=== Final ===
The match was White's 23rd appearance in a final and his first since the [[2000 British Open (snooker)|2000 British Open]]. In the best-of-17 final Maguire defeated White 9–3 to win his first ranking title at the age of 22, earning £48,000 in prize money.<ref name=Yates6Mar04 /><ref name=BBC6Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3539613.stm "Maguire overwhelms White"]. [[BBC Sport]]. 6 March 2004. Retrieved 12 September 2010.</ref> The victory, according to ''[[The Times]]'', transformed Maguire from "talented underachiever into a world-ranking event winner";
<ref>[http://www.timesonline.co.uk/tol/sport/article1040250.ece "Sport in Brief"]. ''[[The Times]]''. 8 March 2004. Retrieved 12 September 2010.</ref> according to [[BBC Sport]] his victory was a surprise.<ref name=BBC10Mar04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3496826.stm "Maguire ready for big time"]. [[BBC Sport]]. 10 March 2004. Retrieved 12 September 2010.</ref>

In the afternoon session Maguire made two sizeable breaks and one of 137 to lead 3–0. He won the next frame and compiled a century in the fifth. The sixth frame was awarded to Maguire, when White violated the [[Rules of snooker#The end of a frame|three-miss rule]]. {{cuegloss|Snooker|Snookered}} in the jaws of a corner pocket, White twice attempted to hit the pack of five reds off a side cushion and missed. On his third attempt he adopted a slow roll to the pack and again missed. In the evening session, trailing 0–6, White won his first frame before the next four were shared, the last of which included a break of 125 by White. At 8–2 a break of 57 gave Maguire the victory.<ref name=BBC6Mar04 />

After his victory Maguire acknowledged the influence of [[Terry Griffiths]] who had been working with him on the mental side of the game: "He's been on the phone just telling me to keep calm and that I can do it if I believe in myself".<ref name=BBC7Nov04>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3539913.stm "Maguire hails Griffiths"]. [[BBC Sport]]. 7 March 2004. Retrieved 12 September 2010.</ref> Maguire said he always knew he was good enough to win a tournament and that he would aim for a top-16 finish for the season.<ref name=BBC10Mar04 /><ref name=BBC7Nov04 /> White said, "Stephen outplayed me in safety, potting and position so he deserved to win" and, "He gave me a good bashing. I'm pleased for him because he's a nice lad but I'm disappointed because I didn't compete".<ref>[http://news.bbc.co.uk/sport1/hi/other_sports/snooker/3539947.stm "White laments poor display"]. [[BBC Sport]]. 22 March 2004. Retrieved 3 October 2010.</ref>

== Main draw ==
{{32TeamBracket
| RD1=Last 32<br /> Best of 9 frames
| RD2=Last 16<br />Best of 9 frames
| RD3=Quarter-finals<br />Best of 9 frames
| RD4=Semi-finals<br />Best of 11 frames
| RD5=Final<br />Best of 17 frames
| RD1-seed01=1
| RD1-team01={{nowrap|{{flagicon|ENG}} '''[[Ronnie O'Sullivan]]'''}}
| RD1-score01='''5'''
| RD1-seed02=19
| RD1-team02={{flagicon|HKG}} [[Marco Fu]]
| RD1-score02=1
| RD1-seed03=9
| RD1-team03={{flagicon|WAL}} [[Matthew Stevens]]
| RD1-score03=0
| RD1-seed04=18
| RD1-team04={{flagicon|SCO}} '''[[Chris Small]]'''
| RD1-score04='''5'''
| RD1-seed05=11
| RD1-team05={{flagicon|ENG}} '''[[Steve Davis]]'''
| RD1-score05='''5'''
| RD1-seed06=27
| RD1-team06={{flagicon|NIR}} [[Joe Swail]]
| RD1-score06=4
| RD1-seed07=5
| RD1-team07={{flagicon|ENG}} '''[[Stephen Lee (snooker player)|Stephen Lee]]'''
| RD1-score07='''w/o'''
| RD1-seed08=32
| RD1-team08={{flagicon|FIN}} [[Robin Hull]]
| RD1-score08=w/d
| RD1-seed09=7
| RD1-team09={{flagicon|ENG}} [[Peter Ebdon]]
| RD1-score09=4
| RD1-seed10=41
| RD1-team10={{flagicon|SCO}} '''[[Stephen Maguire]]'''
| RD1-score10='''5'''
| RD1-seed11=16
| RD1-team11={{flagicon|ENG}} '''[[Joe Perry (snooker player)|Joe Perry]]
| RD1-score11='''5'''
| RD1-seed12=57
| RD1-team12={{flagicon|ENG}} [[David Roe]]
| RD1-score12=3
| RD1-seed13=13
| RD1-team13={{flagicon|SCO}} '''[[Graeme Dott]]'''
| RD1-score13='''5'''
| RD1-seed14=25
| RD1-team14={{flagicon|SCO}} [[Drew Henry]]
| RD1-score14=3
| RD1-seed15=4
| RD1-team15={{flagicon|SCO}} '''[[John Higgins (snooker player)|John Higgins]]'''
| RD1-score15='''5'''
| RD1-seed16=36
| RD1-team16={{flagicon|ENG}} [[Barry Pinches]]
| RD1-score16=0
| RD1-seed17=3
| RD1-team17={{flagicon|SCO}} '''[[Stephen Hendry]]'''
| RD1-score17='''5'''
| RD1-seed18=60
| RD1-team18={{flagicon|ENG}} [[Jimmy Michie]]
| RD1-score18=1
| RD1-seed19=15
| RD1-team19={{flagicon|ENG}} '''[[Jimmy White]]'''
| RD1-score19='''5'''
| RD1-seed20=34
| RD1-team20={{flagicon|THA}} [[James Wattana]]
| RD1-score20=4
| RD1-seed21=12
| RD1-team21={{flagicon|ENG}} '''[[David Gray (snooker player)|David Gray]]
| RD1-score21='''5'''
| RD1-seed22=33
| RD1-team22={{flagicon|IRE}} [[Fergal O'Brien]]
| RD1-score22=3
| RD1-seed23=6
| RD1-team23={{flagicon|IRE}} [[Ken Doherty]]
| RD1-score23=3
| RD1-seed24=
| RD1-team24={{flagicon|AUS}} '''[[Neil Robertson (snooker player)|Neil Robertson]]'''
| RD1-score24='''5'''
| RD1-seed25=8
| RD1-team25={{flagicon|ENG}} '''[[Paul Hunter (snooker player)|Paul Hunter]]'''
| RD1-score25='''5'''
| RD1-seed26=45
| RD1-team26={{flagicon|ENG}} [[Brian Morgan (snooker player)|Brian Morgan]]
| RD1-score26=1
| RD1-seed27=10
| RD1-team27={{flagicon|SCO}} [[Alan McManus]]
| RD1-score27=4
| RD1-seed28=24
| RD1-team28={{flagicon|MLT}} '''[[Tony Drago]]'''
| RD1-score28='''5'''
| RD1-seed29=14
| RD1-team29={{flagicon|AUS}} '''[[Quinten Hann]]'''
| RD1-score29='''5'''
| RD1-seed30=
| RD1-team30={{flagicon|ENG}} [[Simon Bedford]]
| RD1-score30=0
| RD1-seed31=2
| RD1-team31={{flagicon|WAL}} [[Mark Williams (snooker player)|Mark Williams]]
| RD1-score31=1
| RD1-seed32=20
| RD1-team32={{nowrap|{{flagicon|ENG}} '''[[Anthony Hamilton (snooker player)|Anthony Hamilton]]'''}}
| RD1-score32='''5'''
| RD2-seed01=1
| RD2-team01={{nowrap|{{flagicon|ENG}} '''[[Ronnie O'Sullivan]]'''}}
| RD2-score01='''5'''
| RD2-seed02=18
| RD2-team02={{flagicon|SCO}} [[Chris Small]]
| RD2-score02=1
| RD2-seed03=11
| RD2-team03={{flagicon|ENG}} [[Steve Davis]]
| RD2-score03=3
| RD2-seed04=5
| RD2-team04={{flagicon|ENG}} '''[[Stephen Lee (snooker player)|Stephen Lee]]'''
| RD2-score04='''5'''
| RD2-seed05=41
| RD2-team05={{flagicon|SCO}} '''[[Stephen Maguire]]'''
| RD2-score05='''5'''
| RD2-seed06=16
| RD2-team06={{flagicon|ENG}} [[Joe Perry (snooker player)|Joe Perry]]
| RD2-score06=4
| RD2-seed07=13
| RD2-team07={{flagicon|SCO}} [[Graeme Dott]]
| RD2-score07=0
| RD2-seed08=4
| RD2-team08={{flagicon|SCO}} '''[[John Higgins (snooker player)|John Higgins]]'''
| RD2-score08='''5'''
| RD2-seed09=3
| RD2-team09={{flagicon|SCO}} [[Stephen Hendry]]
| RD2-score09=3
| RD2-seed10=15
| RD2-team10={{flagicon|ENG}} '''[[Jimmy White]]'''
| RD2-score10='''5'''
| RD2-seed11=12
| RD2-team11={{flagicon|ENG}} [[David Gray (snooker player)|David Gray]]
| RD2-score11=2
| RD2-seed12=
| RD2-team12={{flagicon|AUS}} '''[[Neil Robertson (snooker player)|Neil Robertson]]'''
| RD2-score12='''5'''
| RD2-seed13=8
| RD2-team13={{flagicon|ENG}} [[Paul Hunter (snooker player)|Paul Hunter]]
| RD2-score13=2
| RD2-seed14=24
| RD2-team14={{flagicon|MLT}} '''[[Tony Drago]]'''
| RD2-score14='''5'''
| RD2-seed15=14
| RD2-team15={{flagicon|AUS}} '''[[Quinten Hann]]'''
| RD2-score15='''5'''
| RD2-seed16=20
| RD2-team16={{flagicon|ENG}} [[Anthony Hamilton (snooker player)|Anthony Hamilton]]
| RD2-score16=1
| RD3-seed01=1
| RD3-team01={{nowrap|{{flagicon|ENG}} [[Ronnie O'Sullivan]]}}
| RD3-score01=4
| RD3-seed02=5
| RD3-team02={{flagicon|ENG}} '''[[Stephen Lee (snooker player)|Stephen Lee]]'''
| RD3-score02='''5'''
| RD3-seed03=41
| RD3-team03={{nowrap|{{flagicon|SCO}} '''[[Stephen Maguire]]'''}}
| RD3-score03='''5'''
| RD3-seed04=4
| RD3-team04={{flagicon|SCO}} [[John Higgins (snooker player)|John Higgins]]
| RD3-score04=3
| RD3-seed05=15
| RD3-team05={{flagicon|ENG}} '''[[Jimmy White]]'''
| RD3-score05='''5'''
| RD3-seed06=
| RD3-team06={{flagicon|AUS}} [[Neil Robertson (snooker player)|Neil Robertson]]
| RD3-score06=3
| RD3-seed07=24
| RD3-team07={{flagicon|MLT}} '''[[Tony Drago]]'''
| RD3-score07='''5'''
| RD3-seed08=14
| RD3-team08={{flagicon|AUS}} [[Quinten Hann]]
| RD3-score08=1
| RD4-seed01=5
| RD4-team01={{flagicon|ENG}} [[Stephen Lee (snooker player)|Stephen Lee]]
| RD4-score01=4
| RD4-seed02=41
| RD4-team02={{nowrap|{{flagicon|SCO}} '''[[Stephen Maguire]]'''}}
| RD4-score02='''6'''
| RD4-seed03=15
| RD4-team03={{flagicon|ENG}} '''[[Jimmy White]]'''
| RD4-score03='''6'''
| RD4-seed04=24
| RD4-team04={{flagicon|MLT}} [[Tony Drago]]
| RD4-score04=4
| RD5-seed01=41
| RD5-team01={{nowrap|{{flagicon|SCO}} '''[[Stephen Maguire]]'''}}
| RD5-score01='''9'''
| RD5-seed02=15
| RD5-team02={{flagicon|ENG}} [[Jimmy White]]
| RD5-score02=3
}}

;Key

w/o = walkover<br/>
w/d = withdrew

== Final ==
{| class="wikitable" style="font-size: 95%; margin: 1em auto 1em auto;"
|-
| colspan="3" align="center" bgcolor="#ffd700" | '''Final:''' Best of 17 frames. Referee: {{flagicon|NLD}} [[Jan Verhaas]].<br/>[[Hilton Conference Centre]], [[Portomaso]], [[Malta]], 6 March 2004.
|-
| width="200" align="right" | '''[[Stephen Maguire]]''' (41)<br/>{{SCO}}
| width="100" align="center" | '''9'''–3
| width="200" | [[Jimmy White]] (15)<br/>{{ENG}}
|-
| colspan="3" align="center" style="font-size: 100%" |''Afternoon'': '''121'''–2 ('''96'''), '''79'''–44 ('''72'''), '''137'''–0 ('''137'''), '''81'''–23, '''113'''–8 ('''103'''), '''58'''–6{{#tag:ref|Maguire was awarded the frame when White violated the [[Rules of snooker#The end of a frame|three-miss rule]].|group=i}}<br/>''Evening'': 63–'''67''', '''62'''–60 ('''62'''), 18–'''91''' ('''78'''), '''77'''–1, 0–'''125''' ('''125'''), '''86'''–6 ('''57''')
|-
| align="right" | '''137'''
| align="center" | Highest break
| align="left" | 125
|-
| align="right" | '''2'''
| align="center" | Century breaks
| align="left" | 1
|-
| align="right" | '''6'''
| align="center" | 50+ breaks
| align="left" | 2
|}

== Notes ==
{{reflist|group=i}}

==Century breaks==

== References ==
{{Reflist|2}}

== Sources ==
* [http://www.snooker.org/trn/0304/eo2004_res.shtml European Open 2004]. snooker.org.
* [http://www.snooker.org/rnk/0304/ranking.asp Embassy World Rankings 2003/2004]. snooker.org.

{{Malta Cup}}
{{Snooker season 2003/2004}}

{{DEFAULTSORT:2004 European Open (snooker)}}
[[Category:Malta Cup|2004]]
[[Category:2004 in snooker|European Open]]
[[Category:2004 in Maltese sport|European Open (snooker)]]