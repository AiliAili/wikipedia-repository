{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| image         = [[File:Baltimore Saturday Visiter, October 19, 1833, Prize tale.jpg|200px]]
| image_caption = ''Baltimore Saturday Visiter'', October 19, 1833
| name          = MS. Found in a Bottle
| title_orig    = 
| translator    = 
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      = English
| series        = 
| genre         = [[Adventure]]<br>[[Short story]]
| published_in   = ''[[Baltimore Saturday Visiter]]''
| publisher     = 
| media_type    = Print ([[Periodical]])
| pub_date  = October 19, 1833
| english_pub_date = 
| preceded_by   = 
| followed_by   = 
}}
"'''MS. Found in a Bottle'''" is an 1833 [[short story]] by American writer [[Edgar Allan Poe]]. The plot follows an unnamed narrator at sea who finds himself in a series of harrowing circumstances. As he nears his own disastrous death while his ship drives ever southward, he writes an "MS.", or manuscript, telling of his adventures which he casts into the sea. Some critics believe the story was meant as a [[satire]] of typical sea tales.

Poe submitted "MS. Found in a Bottle" as one of many entries to a writing contest offered by the weekly ''[[Baltimore Saturday Visiter]]''<!-- yes that's the way it's spelled -->. Each of the stories was well liked by the judges but they unanimously chose "MS. Found in a Bottle" as the contest's winner, earning Poe a $50 prize. The story was then published in the October 19, 1833, issue of the ''Visiter''.

==Plot summary==
[[File:MS Found in a Bottle Wogel.JPG|thumb|Illustration by "Wogel" for an early edition]]
An unnamed narrator, estranged from his family and country, sets sail as a passenger aboard a cargo ship from [[Jakarta|Batavia]] (now known as Jakarta, [[Indonesia]]). Some days into the voyage, the ship is first becalmed then hit by a [[simoom]] (a combination of a sand storm and hurricane) that capsizes the ship and sends everyone except the narrator and an old [[Swedish people|Swede]] overboard. Driven southward by the magical simoom towards the South Pole, the narrator's ship eventually collides with a gigantic black [[galleon]], and only the narrator manages to scramble aboard. Once the new ship arrives, the narrator finds outdated maps and useless navigational tools throughout the ship.  Also, he finds it to be manned by elderly crewmen who are unable to see him; he steals writing materials from the captain's cabin to keep a journal (the "[[wikt:MS#Abbreviation|manuscript]]" of the title) which he resolves to cast into the sea. This ship too continues to be driven southward, and he notices the crew appears to show signs of hope at the prospect of their destruction as it reaches [[Antarctica]]. The ship enters a clearing in the ice where it is caught in a vast [[whirlpool]] and begins to sink into the sea.

==Analysis==
"MS. Found in a Bottle" is one of Poe's sea tales (others are "[[A Descent into the Maelström]]" and "[[The Oblong Box (short story)|The Oblong Box]]"). The story's horror comes from its scientific imaginings and its description of a physical world beyond the limits of human exploration.

Biographer [[Kenneth Silverman]] wrote that the story is "a sustained crescendo of ever building dread in the face of ever stranger and ever more imminent catastrophe".<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York City: Harper Perennial, 1991: 91. ISBN 0-06-092331-8</ref> This prospect of unknown catastrophe both horrifies and stimulates the narrator.<ref>Stashower, Daniel. ''The Beautiful Cigar Girl: Mary Rogers, Edgar Allan Poe, and the Invention of Murder''. New York: Dutton, 2006: 65. ISBN 0-525-94981-X</ref> Like Poe's narrator in another early work, "[[Berenice (short story)|Berenice]]", the narrator in "MS. Found in a Bottle" lives predominantly through his books, or more accurately his manuscripts.<ref>Peeples, Scott. ''Edgar Allan Poe Revisited''. New York: Twayne Publishers, 1998: 50. ISBN 0-8057-4572-6</ref>

The otherworldly ship on which the narrator finds himself may evoke the legendary [[ghost ship]], the ''[[Flying Dutchman]]''.<ref name=Carlson119>Carlson, Eric W. ''A Companion to Poe Studies''. Westport, CT: Greenwood, 1996: 119. ISBN 0-313-26506-2</ref> A number of critics have argued that the story's ending references the [[Hollow Earth]] theories propounded by [[John Cleves Symmes, Jr.]] and [[Jeremiah N. Reynolds]]. Symmes and Reynold proposed that the planet's interior was hollow and habitable, and was accessible via openings at the two poles.<ref name=Carlson119/><ref>Bittner, William. ''Poe: A Biography''. Boston: Little, Brown & Company, 1962: 132.</ref> The idea was considered scientifically plausible during the early 19th century.<ref>Meyers, Jeffrey. Edgar Allan Poe: His Life and Legacy. New York: Cooper Square Press, 1991: 100. ISBN 0-8154-1038-7</ref> Poe also incorporated Symmes' theories into his later work ''[[The Narrative of Arthur Gordon Pym of Nantucket]]'' (1838), his only novel. ''Pym'' bears a number of similarities to "MS. Found in a Bottle", including an abrupt ending set in the Antarctic.<ref>Carlson, Eric W. ''A Companion to Poe Studies''. Westport, CT: Greenwood, 1996: 212–213. ISBN 0-313-26506-2</ref><ref>Whalen, Terrance. ''Edgar Allan Poe and the Masses: The Political Economy of Literature in Antebellum America''. Princeton University Press, 1999: 160–161. ISBN 0-691-00199-5.</ref>

However, Poe's story may have been intended to poke fun at the more outlandish claims in Symmes' theory.<ref name=Carlson119/> Indeed, some scholars suggest that "MS. Found in a Bottle" was meant to be a [[parody]] or [[satire]] of [[nautical fiction|sea stories]] in general, especially in light of the absurdity of the plot and the fact that the narrator unrealistically keeps a diary through it all.<ref name=Bittner90>Bittner, William. ''Poe: A Biography''. Boston: Little, Brown & Company, 1962: 90.</ref> William Bittner, for example, wrote that it was poking fun specifically at [[Jane Porter]]'s novel ''Sir Edward Seaward's Narrative'' (1831) or [[Symzonia: A Voyage of Discovery|''Symzonia'' (1820)]] by the pseudonymous "Captain Adam Seaborn", who was possibly John Cleves Symmes.<ref name=Bittner90/> It may be significant that the other tales that Poe wrote during this period, including "[[Bon-Bon (short story)|Bon-Bon]]", were meant to be humorous or, as Poe wrote, "[[Burlesque (literature)|burlesques]] upon criticism generally".<ref>Peeples, Scott. ''Edgar Allan Poe Revisited''. New York: Twayne Publishers, 1998: 32. ISBN 0-8057-4572-6</ref>

==Critical reception==
The editors who first published "MS. Found in a Bottle" called it "eminently distinguished by a wild, vigorous and poetical imagination, a rich style, a fertile invention, and varied and curious learning."<ref name=Sova162>Sova, Dawn B. Edgar Allan Poe: A to Z. New York City: Checkmark Books, 2001: 162. ISBN 0-8160-4161-X</ref> Writer [[Joseph Conrad]] considered the story "about as fine as anything of that kind can be—so authentic in detail that it might have been told by a sailor of sombre and poetical genius in the invention of the fantastic".<ref name=Sova162/> Poe scholar Scott Peeples summarizes the importance of "MS. Found in a Bottle" as "the story that launched Poe's career".<ref>Peeples, Scott. ''Edgar Allan Poe Revisited''. New York: Twayne Publishers, 1998: 46. ISBN 0-8057-4572-6</ref>

The story was likely an influence on [[Herman Melville]] and bears a similarity to his novel ''[[Moby-Dick]]''. As scholar Jack Scherting noted:
{{quote|Two well-known works of American fiction fit the following description. Composed in the 19th century each is an account of an observant, first-person narrator who, prompted by a nervous restlessness, went to sea only to find himself aboard an ill-fated ship. The ship, manned by a strange crew and under the command of a strange, awesome captain, is destroyed in an improbable catastrophe; and were it not for the fortuitous recovery of a floating vessel and its freight, the narrative of the disastrous voyage would never have reached the public. The two works are, of course, Melville's ''Moby-Dick'' (1851) and Poe's "MS. Found in a Bottle" (1833), and the correspondences are in some respects so close as to suggest a causal rather than a coincidental relationship between the two tales.<ref>Scherting, Jack. "The Bottle and the Coffin: Further Speculation on Poe and ''Moby-Dick''", ''Poe Newsletter'', vol. I, no. 2, October 1968: 22.</ref>}}

==Publication history==
{|style="float: right;"
| [[File:The gift 1836 poe MS..jpg|thumb|''The Gift'', Carey and Hart, Philadelphia, 1836]]
|-
| [[File:Southern lit mess poe 1835.jpg|thumb|December 1835 issue of ''Southern Literary Messenger'', featuring "MS. Found in a Bottle" (p. 33) and "[[Politian (play)|Politian]]" (p. 13) by Edgar Allan Poe]]
|}
{{wikisource|Baltimore Saturday Visiter/June 15, 1833 - Premiums|Baltimore Saturday Visiter<br>June 15, 1833}}
{{wikisource|Baltimore Saturday Visiter/October 12, 1833 - Premiums|Baltimore Saturday Visiter<br>October 12, 1833}}

In the June 15, 1833, issue of the ''[[Baltimore Saturday Visiter]]'', its publishers Charles F. Cloud and William L. Pouder announced prizes of "50 dollars for the best Tale and 25 dollars for the best poem, not exceeding one hundred lines", submitted by October 1, 1833. Poe submitted "MS. Found in a Bottle" along with five others. The judges—[[John P. Kennedy|John Pendleton Kennedy]], Dr. James Henry Miller and John H. B. Latrobe—met at the house of Latrobe on October 7<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 130. ISBN 0-8161-8734-7</ref> and unanimously selected Poe's tale for the prize. The award was announced in the October 12 issue, and the tale was printed in the following issue on October 19, with the remark: "The following is the Tale to which the Premium of Fifty Dollars has been awarded by the Committee. It will be found highly graphic in its style of Composition."<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 133. ISBN 0-8161-8734-7</ref> Poe's poetry submission, "[[Poems by Edgar Allan Poe#The Coliseum (1833)|The Coliseum]]", was published a few days later, but did not win the prize.<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. Checkmark Books, 2001.</ref> The poetry winner turned out to be the editor of the ''Visiter'', John H. Hewitt, using the pseudonym "Henry Wilton". Poe was outraged and suggested the contest was rigged. Hewitt claimed, decades later in 1885, that Poe and Hewitt brawled in the streets because of the contest, though the fight is not verified.<ref>Poe, Harry Lee. ''Edgar Allan Poe: An Illustrated Companion to His Tell-Tale Stories''. New York: Metro Books, 2008: 55. ISBN 978-1-4351-0469-3</ref> Poe believed his own poem was the actual winner, a fact which Latrobe later substantiated.<ref>Meyers, Jeffrey. ''Edgar Allan Poe: His Life and Legacy''. New York: Cooper Square Press, 1992: 65. ISBN 0-8154-1038-7</ref>

Kennedy was particularly supportive of Poe's fledgling career and gave him work for the ''Visiter'' after the contest.<ref name=PoeLog135>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 135. ISBN 0-8161-8734-7</ref> He assisted in getting "MS. Found in a Bottle" reprinted in an annual [[gift book]] called the ''The Gift: A Christmas and New Year's Present'' in its 1836 issue.<ref>Benton, Richard P. "The Tales: 1831–1835", ''A Companion to Poe Studies'', Eric W. Carlson, ed. Westport, CT: Greenwood Press, 1996: 111. ISBN 0-313-26506-2</ref> Kennedy also urged Poe to collect the stories he submitted to the contest, including "MS. Found in a Bottle", into one edition and contacted publisher Carey & Lea on his behalf.<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York City: Harper Perennial, 1991: 93. ISBN 0-06-092331-8</ref> A plan was made to publish the stories as a volume called ''Tales of the Folio Club,'' and the ''Saturday Visiter'' promoted it by issuing a call for subscribers to purchase the book in October 1833 for $1 apiece.<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 134. ISBN 0-8161-8734-7</ref> The "Folio Club" was intended to be a fictitious literary society the author called a group of "dunderheads" out to "abolish literature".<ref>Sova, Dawn B. Edgar Allan Poe: A to Z. New York City: Checkmark Books, 2001: 88. ISBN 0-8160-4161-X</ref> The idea was similar in some respects to ''[[The Canterbury Tales]]'' by [[Geoffrey Chaucer]]. At each  monthly meeting, a member would present a story. A week after the ''Visiter'' issued its advertisement, however, the newspaper announced that the author had withdrawn the pieces with the expectation they would be printed in [[Philadelphia, Pennsylvania]].<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York City: Harper Perennial, 1991: 92–93. ISBN 0-06-092331-8</ref> Publishers Harper and Brothers were offered the collection but rejected it, saying that readers wanted long narratives and novels, inspiring Poe to write ''[[The Narrative of Arthur Gordon Pym of Nantucket]]'', another sea tale.<ref>Peeples, Scott. ''Edgar Allan Poe Revisited''. New York: Twayne Publishers, 1998: 56. ISBN 0-8057-4572-6</ref>

After its first publication, "MS. Found in a Bottle" was almost immediately pirated by the ''[[People's Advocate]]'' of [[Newburyport, Massachusetts]], which published it without permission on October 26, 1833.<ref name=PoeLog135/>

In August 1835, Poe took a job as a staff writer and critic for the ''[[Southern Literary Messenger]]'' in Richmond, Virginia. That magazine's December 1835 issue carries a copy of "MS. Found in a Bottle" (see picture to the right).

==References==
{{reflist|2}}

==External links==
*{{wikisource-inline|single=true}}
*{{Commonscat-inline}}
*[http://www.eapoe.org/WORKS/info/pt009.htm Publication history of "MS. Found in a Bottle"] at the [http://www.eapoe.org Edgar Allan Poe Society]
* {{librivox book | title=Ms. Found in a Bottle | author=Edgar Allan Poe}}

{{Flying Dutchman}}
{{Edgar Allan Poe}}

[[Category:1833 short stories]]
[[Category:Short stories by Edgar Allan Poe]]
[[Category:Antarctica in fiction]]
[[Category:Works originally published in American magazines]]
[[Category:Works originally published in literary magazines]]