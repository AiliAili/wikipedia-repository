{{Primary sources|date=July 2009}}
{{Accounting}}
An '''accounting period''' is a period with reference to which [[United Kingdom corporation tax]] is charged.<ref>Section 12 of the Income and Corporation Taxes Act 1988</ref> It helps dictate when tax is paid on income and gains. An accounting period begins whenever a company comes within the corporation tax charge, and whenever an accounting period ends without the company ceasing to be within the charge.
There are a number of rules about when an accounting period ends, and we look at each of these below. 

Often an accounting period coincides with a company's ''period of account''. This is the period for which it draws up [[financial statements|accounts]],<ref>Section 832(1) of the Income and Corporation Taxes Act 1988</ref> except for a life assurance company, where it is the period for which it draws up its periodical return.<ref>Section 431 of the Income and Corporation Taxes Act 1988</ref> However, periods of account and accounting periods do not necessarily coincide.

==Basic rules==

An accounting period begins when:<ref>Section 12(2) of the Income and Corporation Taxes Act 1988</ref>

*the company comes within the charge to corporation tax. A company usually first comes within the corporation tax charge when it first acquires a source of income.<ref>Section 832(1) of the Income and Corporation Taxes Act 1988 (see the closing words) and HM Revenue and Customs' ''Company Taxation Manual'' at [http://www.hmrc.gov.uk/manuals/ctmanual/CTM01410.htm CTM01410]</ref> However, it will also come within the charge if it commences business but is not already within the charge.<ref name="ReferenceA">Section 12(4) of the Income and Corporation Taxes Act 1988</ref> Overseas companies usually come within the charge if they become UK resident or start trading in the UK through a UK [[permanent establishment]];<ref>Section 11 of the Income and Corporation Taxes Act 1988</ref><ref>It was also held in ''Walker v Centaur Clothes Group Ltd'' 72 TC 379 that a source is not always necessary. In that case, it was held that the company becoming liable to pay advance corporation tax was enough to bring it within the charge to tax.</ref>
*an accounting period of the company ends, and the company is still within the charge to corporation tax; or
*the company does not currently have an accounting period and has a chargeable gain or allowable loss.<ref>Section 12(6) of the Income and Corporation Taxes Act 1988</ref> Chargeable gains and allowable losses are taxable gains and tax relievable losses that arise on the disposal of certain capital assets, for example on the disposal of the company's head office.<ref>The full definition of what constitutes a chargeable gain or allowable loss is in the ''Taxation of Chargeable Gains Act 1992''</ref>

An accounting period ends on the earliest of the following:<ref>Section 12(3) of the Income and Corporation Taxes Act 1988</ref>

*the expiration of 12 months from the beginning of the accounting period;
*an accounting date of the company or, if there is a period for which the company does not draw up accounts, the end of that period;
*the company beginning or ceasing to trade or to be, in respect of the trade or (if more than one) of all trades carried on by it, within the charge to corporation tax;
*the company beginning or ceasing to be UK resident;
*the company ceasing to be within the charge to corporation tax.

There are further rules to deal with windings up, life assurance companies and lessor companies.

A UK-resident company is treated as coming within the charge to corporation tax (if it has not already come within the charge to corporation tax) when it commences to carry on business.<ref name="ReferenceA"/>

==Example==

Suppose ABC Ltd, a UK resident company is incorporated on 1 August 20X1. It acquires a source of income (an interest-bearing bank account) on 1 September 20X1 and commences trading on 1 October 20X1 and continues trading throughout all other periods under review. It draws up its accounts for the following periods:

:1 August 20X1 to 31 December 20X1
:1 January 20X2 to 31 December 20X2
:1 January 20X3 to 30 June 20X3
:1 July 20X3 to 31 December 20X4

Then ABC Ltd would have the following accounting periods:

:1 September 20X1 to 30 September 20X1 (from when it came into charge to corporation tax to the commencement of trade)
:1 October 20X1 to 31 December 20X1 (to the end of a period of account)
:1 January 20X2 to 31 December 20X2 (to the end of a period of account)
:1 January 20X3 to 30 June 20X3 (to the end of a period of account)
:1 July 20X3 to 30 June 20X4 (to the expiry of 12 months)
:1 July 20X4 to 31 December 20X4 (to the end of a period of account)

==Company with more than one trade==

If a company carrying on more than one trade draws up accounts of any of them to different dates, and does not make up general accounts for the whole of the company's activities, the company may determine that any one of those dates shall be taken into consideration for the purposes of determining whether an accounting period has ended under the rule in the second bullet point above.<ref>Section 12(5) of the Income and Corporation Taxes Act 1988</ref>

If, however, the Board of [[HMRC]] is of the opinion, on reasonable grounds, that the date so chosen by the company is inappropriate, they may, if it is reasonable to do so, give notice that the accounting date of another of the company's trades should be used instead.<ref>Section 12(5A) of the Income and Corporation Taxes Act 1988</ref>

==Mean accounting date==

Some companies draw up financial statements to the same day of the week, a Sunday, say, each year. They will therefore have 364-day (52-week) or 371-day (53-week) periods of account. As long as the company draws its accounts up to within four days of a mean accounting date each year, it may, for the purposes of determining its accounting periods, deem its accounts to be drawn up for a twelve-month period. For example, when there is a 371-day period of account, instead of having one accounting period of 365 days (in a non-leap year) and another of 6 days, it will prepare its return on the assumption that the period of account was 365 days long.<ref>See the ''[[Institute of Chartered Accountants in England and Wales]]'s'' Technical Release 500 and [[HM Revenue and Customs]]' ''Company Taxation Manual'' at [http://www.hmrc.gov.uk/manuals/ctmanual/CTM01560.htm CTM01560].</ref>

This method is related to the [[4-4-5 Calendar]] of accounts, where a quarter is deemed to comprise two months of 4 weeks each and one month of 5 weeks, a total of 13 weeks or 91 days.  In the [[United States]] the method is referred to as the 52-53 Week Fiscal Year and is approved for use under US Generally Accepted Accounting Principles (GAAP) and the [[Internal Revenue Code]].  See article under [[4-4-5 Calendar]] for discussion of the US method application.

==Inspector determining the accounting period==

Where it appears to the tax inspector that the beginning or end of any accounting period is uncertain, he may make an assessment on the company for such period, not exceeding 12 months, as appears to him appropriate. In that event, that period is treated for all purposes as an accounting period unless the inspector sees fit to revise it as a result of receiving further facts, or on an appeal against the assessment in respect of some other matter the company shows the true accounting periods. If on an appeal against an assessment made under this rule the company shows the true accounting periods, the assessment appealed against has effect as an assessment or assessments for the true accounting periods, and assessments may be made for any such periods as might have been made at the time when the assessment appealed against was made.<ref>Section 12(8) of the Income and Corporation Taxes Act 1988</ref>

==Administration and winding up==

===Administration===

An accounting period ends immediately before the day a company enters into [[administration (insolvency)|administration]]. For this purpose a company enters administration when it enters administration under Schedule B1 to the [[Insolvency Act 1986]] or is subject to any corresponding procedure otherwise than under that Act (for example, in an overseas jurisdiction).<ref name="ReferenceB">Section 12(7ZA) of the Income and Corporation Taxes Act 1988</ref> 

An accounting period ends when a company ceases to be in administration.<ref>Section 12(3)(da) of the Income and Corporation Taxes Act 1988</ref> For these purposes a company ceases to be in administration when it ceases to be in administration under Schedule B1 to the Insolvency Act 1986 or any corresponding event occurs otherwise than under that Act<ref>Section 12(5B) of the Income and Corporation Taxes Act 1988</ref> (for example, in an overseas jurisdiction).

===Winding up===

An accounting period ends and a new one begins with the commencement of a [[winding up]]. For this purpose a winding up is taken to commence on the passing of a resolution for the winding up of the company, or on the presentation of a winding up petition if no such resolution has previously been passed and a winding up order is made on the petition, or on the doing of any other act for a like purpose in the case of a winding up otherwise than under the Insolvency Act 1986.<ref name="ReferenceC">Section 12(7) of the Income and Corporation Taxes Act 1988</ref>

After this, an accounting period does not end other than by the expiration of 12 months from its beginning or by the completion of the winding up.<ref name="ReferenceC"/> However, if the company later enters administration, this rule is disapplied from that point (and with an accounting period ending because the company has entered into administration).<ref name="ReferenceB"/>

During the course of a winding up, a company may make an estimate of when it will be wound up so that it can prepare tax returns for its final accounting period before that period ends. If the date assumed for the winding up turns out to be before the actual winding up, then an accounting period ends on the date assumed for the winding up, a new accounting period starts, and the immediately preceding paragraph applies as if the winding up had started with that accounting period.<ref>Section 342(6) of the Income and Corporation Taxes Act 1988</ref>

==Cooperatives preparing quarterly and half-yearly accounts==

HMRC will, under certain conditions, allow [[cooperative]]s that prepare quarterly and half-yearly accounts to merge a number of periods of account for tax purposes so that they have only accounting date each year.<ref>Inland Revenue ''Extra-statutory concession C12'' See page 67 of [http://www.hmrc.gov.uk/specialist/esc.pdf this pdf] from HM Revenue and Customs (772k)</ref>

==Life assurance companies==

Additionally, there are special rules that apply to [[life assurance]] companies from which life assurance business is transferred under an insurance business transfer scheme. Different rules apply depending on whether section 444AA of the Income and Corporation Taxes Act 1988 applies. For transfers before 1 January 2007, s444AA applies where the whole of the transferor's long-term insurance business is being transferred and the company. It is proposed that the Finance Bill 2007 will amend this for transfers on or after 1 January, so that section 444AA applies where the whole or substantially the whole of the transferor's long-term insurance business is transferred, in which case the transfer time is taken to be the time some business is first transferred/<ref>[http://www.hmrc.gov.uk/pbr2006/index.htm See draft legislation released by HMRC on 6 December 2006] {{webarchive |url=https://web.archive.org/web/20061230091149/http://www.hmrc.gov.uk/pbr2006/index.htm |date=December 30, 2006 }}</ref>

If section 444AA does not apply, an accounting period of the transferor company ends with the day of the transfer.<ref>Section 12(7A) of the Income and Corporation Taxes Act 1988</ref>

If section 444AA applies, an accounting period of the transferor company ends immediately before the transfer, and the transferor also has an accounting period covering the instant of the transfer (except for the purposes of calculating the equalisation provision for tax purposes).<ref>Section 12(7C) of the Income and Corporation Taxes Act 1988</ref>

===Example where s444AA applies===

Suppose a transferor company, XYZ Life Ltd, has in the past prepared periodical returns for a period encompassing the calendar year. It transfers the whole of its business at 11.59pm on 31 December 20X1, after which it retains some cash on deposit. It agrees with the [[Financial Services Authority]] that it does not need to prepare a periodical return for 20X1.

XYZ Life Ltd will have the following accounting periods in 20X1:

:1 January 20X1 to immediately before the transfer of business
:An accounting period covering the instant of the transfer (11.59pm on 31 December 20X1)
:An accounting period covering the period beginning immediately after the transfer to midnight on 31 December 20X1

==Lessor companies==

If a company carries on a business of [[leasing]] plant or machinery, is within the charge to corporation tax in respect of the business and there is a qualifying change of ownership in relation to the company, then an accounting period of the company ends on the day of that qualifying change in ownership, and a new accounting period begins on the following day. There are exceptions to this in Paragraph 40.<ref>Paragraphs 3, 33, 40 Schedule 10 to the [[Finance Act]] 2006</ref>

==Partial deeming provisions==

Other provisions in the [[Taxes Acts]] sometimes deem there to be accounting periods for specific purposes only. This is typically on the introduction of new legislation. For example, when section 75 of the Income and Corporation Taxes Act 1988 was replaced as a result of the Finance Act 2004, companies, if necessary, deemed themselves to have an accounting period ending on 31 March 2004 for the purposes of calculating management expenses that they were entitled to under the rules applying pre-1 April 2004 and post-31 March 2004.

==Notes==
{{reflist|2}}

==References==
*For a copy of the ''[[Income and Corporation Taxes Act 1988]]'' as it stands after the enactment of the Finance Act 2006, see ''The Red Book 2006-07'' (1A); Consultant editors: Ian Barlow MA FCA, David Milne, QC, MA, FCA; Publisher: CCH; ISBN 1-84140-767-4.
*For a copy of the ''[[Finance Act 2006]]'' at its enactment see ''The Red Book 2006-07'' (1B); Consultant editors: Ian Barlow MA FCA, David Milne, QC, MA, FCA; Publisher: CCH; ISBN 1-84140-768-2. Alternatively, see [https://web.archive.org/web/20070218083645/http://www.opsi.gov.uk:80/acts/acts2006/20060025.htm opsi.gov.uk].
*[[HM Revenue and Customs]]' ''Company Taxation Manual'' at [http://www.hmrc.gov.uk/manuals/ctmanual/CTM01400.htm CTM01400-CTM1560]

{{DEFAULTSORT:Accounting Period (Uk Taxation)}}
[[Category:Corporate taxation in the United Kingdom]]

[[cs:Účetní období]]
[[de:Rechnungsperiode]]