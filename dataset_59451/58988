{{Infobox journal
 | title        = Monatshefte für Mathematik
 | cover        = 
 | abbreviation = Monatsh. Math.
 | discipline   = [[Mathematics]]
 | editor       = Adrian Constantin
 | publisher    = [[Springer Science+Business Media|Springer]]
 | frequency    = Monthly
 | history      = 1890–present
 | impact       = 0.764
 | impact-year  = 2009
 | url          = http://www.springer.com/mathematics/journal/605
 | ISSN         = 0026-9255
 | eISSN        = 1436-5081
 | CODEN        = MNMTA2
 | LCCN         = 51026443
 | OCLC         = 01589453
 | formernames = Monatshefte für Mathematik und Physik
 | link1        = http://www.springerlink.com/content/1436-5081/ 
 | link1-name   = Online access
}}

''''' Monatshefte für Mathematik''''' is a [[peer review|peer-reviewed]] [[mathematics journal]] established in 1890.  Among its well-known papers is "[[Über formal unentscheidbare Sätze der Principia Mathematica und verwandter Systeme I]]" by [[Kurt Gödel]], published in 1931.

The journal was founded by [[Gustav von Escherich]] and [[Emil Weyr]] in 1890 as ''Monatshefte für Mathematik und Physik'' and published until 1941. In 1947 it was reestablished by [[Johann Radon]] under its current title. It is currently published by [[Springer Science+Business Media|Springer]] in cooperation with the [[Austrian Mathematical Society]]. The journal is indexed by ''[[Mathematical Reviews]]'' and [[Zentralblatt MATH]].
Its 2009 [[Mathematical Citation Quotient|MCQ]] was 0.58, and its 2009 [[impact factor]] was 0.764.

==External links==
*{{Official website|1=http://www.springer.com/mathematics/journal/605}}

{{DEFAULTSORT:Monatshefte fur Mathematik}}
[[Category:Mathematics journals]]
[[Category:Publications established in 1890]]
[[Category:English-language journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]


{{math-journal-stub}}