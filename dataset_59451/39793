{{Infobox engineer
|image                = 1985 Roy retired.JPG
|image_size           = <!-- Default is frameless -->
|alt                  = 
|caption              = Roy Chaplin (1899-1988) around 1985
|honorific_prefix     = 
|name                 = Roy Chaplin
|honorific_suffix     = [[OBE]], [[FRAeS]]
|native_name          =
|native_name_lang     =
|nationality          = British
|citizenship          = 
|birth_name           = Roland Henry Chaplin
|birth_date           = {{Birth date|1899|5|16|df=y}}
|birth_place          = [[Kingston upon Thames]], England
|death_date           = {{Death date and age|1988|12|13|1899|5|16|df=y}}
|death_place          = [[Charlton, Brinkworth|Charlton]], England
|resting_place        = 
|resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
|education            = [[Tiffin School]]
|spouse               = Madge Macey
|parents              = 
|children             = 2 sons
|discipline           = Aeronautics
|institutions         = [[Royal Aeronautical Society|FRAeS]]
|practice_name        = 
|employer             = [[Hawker Aircraft]]
|significant_projects = 
|significant_design   = [[Hawker Hurricane]]
|significant_advance  = 
|significant_awards   = [[Royal Aeronautical Society|RAeS]] Silver Medal (1960)
|signature            = 
}}
'''Roland Henry Chaplin''', [[Order of the British Empire|OBE]], [[Fellow of the Royal Aeronautical Society|FRAeS]], (16 May 1899 – 13 December 1988), known as "Roy", was an aeronautical engineer who worked with [[Sydney Camm]] at [[Hawker Aircraft Limited]] from 1927 to 1962. He helped design the [[Hawker Fury]] biplane, the [[Hawker Hurricane|Hurricane]] monoplane, and the [[Harrier Jump Jet|Harrier]] jump-jet. He graduated with a degree in engineering from [[London University]] and retired in the 1960s.

== Early life ==
Chaplin was educated at [[Tiffin School|Tiffin Boys School]], Kingston upon Thames. He studied engineering at [[London University]]. The [[World War I|First World War]] interrupted his studies, when he was commissioned into the [[Royal Engineers]], serving in France and Belgium until 1919. Chaplin was then obliged to repeat his second year at London University, graduating with an honours degree in engineering ([[Bachelor of Science|BSc]]) in 1921.<ref name="Times1988">{{cite news |title=Roy Chaplin; Obituary |first= |last= |url=http://go.galegroup.com/ps/i.do?id=GALE%7CA117348047&v=2.1&u=cam_earl&it=r&p=SPJ.SP01&sw=w |newspaper=Times|location=London, England |date=20 December 1988|accessdate=31 January 2013}} {{subscription required}}</ref> In 1922 Chaplin started his first job, at pump manufacturer [[Gwynnes Limited|Gwynnes of Hammersmith]].<ref name="Guardian1988">{{cite news|last=Barfield|first=Norman|title=Obituary of Roy Chaplin: Shaper of our planes|accessdate=2 February 2013|newspaper=The Guardian|date=21 December 1988}} {{subscription required}}</ref>

== Hawker ==
[[File:HawkerFury43sqn.jpg|thumb|left|Chaplin worked on the [[Hawker Fury]]. This [[No. 43 Squadron RAF]] example ''c''.&nbsp;1931]]
In 1926, Chaplin joined [[Hawker Aircraft]] at [[Kingston upon Thames (parish)|Kingston]],<ref name="Times1988" /> when it had fewer than forty design staff. Chaplin worked on the [[Hawker Fury]] biplane design.<ref>{{cite book|last=Fozard|first=John|title=Sydney Camm and the Hurricane|year=1991|publisher=Airlife|location=Shrewsbury|url=https://books.google.com/books?id=pJTfAAAAMAAJ|pages=44–57|isbn=9781560980346}}</ref> He recalled that, in 1933, many Air Ministry diehards believed that the monoplane structure was unsuitable and dangerous for use as a military aircraft.<ref name="Hurricane">{{Citation | title = Proceedings of the Hurricane 50th Anniversary Symposium| editor-last = Fozard | editor-first = John W | date = 6 November 1985 | place = Brooklands | publisher = [[Royal Aeronautical Society]]. Weybridge Branch | url =https://books.google.com/books/about/Proceedings_of_the_Hurricane_50th_Annive.html?id=42jzGwAACAAJ|pages=7, 13, 7&ndash;17}}</ref>
Hawker's developed the idea of a monoplane version of the Fury biplane fighter with other refinements, such as an enclosed cockpit. Chaplin played a role in the 1934 development of the 'Hawker Fury Monoplane', which became the [[Hawker Hurricane|‘Hurricane’]]. The Hurricane prototype first flew from [[Brooklands]] on 6&nbsp;November 1935.<ref name="Flight1940">{{cite journal | date = 31 October 1940| title = The Hawker Hurricane| journal = [[Flight (magazine)|Flight]] | page = h| url =http://www.flightglobal.com/pdfarchive/view/1940/1940%20-%203082.html| accessdate = 31 January 2013 | quote=[I]t would be impossible to mention all. However, we feel that at the very least the names of Messrs. R. H. Chaplin, S. D. Davies, R. Mclntyre, and R. Lickley should be brought into this account of the birth and growth of the Hurricane.}}</ref>
 
[[File:Hawker Hurricane before maiden flight 1935.jpg|thumb|right|Chaplin's contributions to the design of the [[Hawker Hurricane|Hurricane]] were rewarded in 1946 with an&nbsp;[[OBE]]. Pre-production version at [[Brooklands]], 6&nbsp;Nov 1935]]By March 1936, test flying of the prototype was completed. The Hawker Board issued instructions planning, jigging, and tooling for the production of one thousand aircraft. The official contract followed three months later.<ref name="Hurricane" /> According to Chaplin, "The Hurricane brought down more enemy aircraft in the Battle of Britain than all the Spitfires, Blenheims, Defiants, and ground defences put together."<ref name="Times1988" />

In 1939, Chaplin was promoted to Assistant Chief Designer. [[Ralph Hooper]] recalled during an interview with Thomas Lean in 2010 that "Sydney Camm used to reduce his secretaries to tears from time to time and Roy Chaplin who was his number two used to come in and cheer them up again".<ref name="Hooper2010">{{cite interview |last=Hooper |first=Ralph|authorlink=Ralph Hooper |interviewer=Thomas Lean|title='Made in Britain' strand of An Oral History of British Science|publisher =[[British Library]] |location=London |date=4&nbsp;August 2010 &ndash; 23&nbsp;November 2010|work=Sound and moving image catalogue|at=Tracks 3, 4, 5 and 10|url=http://sounds.bl.uk/related-content/TRANSCRIPTS/021T-C1379X0027XX-0000A1.pdf|accessdate=6 February 2013|quote=On the other side he [Sydney Camm] used to reduce his secretaries to tears from time to time and Roy Chaplin who was his number two used to come in and cheer them up again. }}</ref> In 1940, Chaplin and most of the Hawker Design Office moved down the Portsmouth Road from Kingston to [[Claremont (country house)|Claremont]], an 18th-century mansion at [[Esher]]. From 1937 until after the Second World War, he was involved with the design of the [[Hawker Typhoon|Typhoon]], [[Hawker Tempest|Tempest]] and [[Hawker Sea Fury|Sea Fury]] aircraft. In 1946 Chaplin’s role in the design and development of the Hurricane was acknowledged when he was appointed an [[Order of the British Empire|Officer of the Order of the British Empire]].<ref>{{London Gazette |issue=37412 |date= 9&nbsp;January 1946|startpage=277|supp=yes }}</ref><ref name="RAeS awards">{{cite book|last=Royal Aeronautical Society|title=Wilbur and Orville Wright Medals and Awards Brochure|year=1960|location=London}}</ref>

== Jets ==
[[File:Hawker P. 1127 - NASA.jpg|thumb|left|Undated image from NASA. Chaplin was proud of his work on the [[Harrier Jump Jet|Harrier jump-jet]] (P.1127).]]Less than twenty years after Chaplin joined Hawker's, the company moved into the jet era when work was started on the [[Hawker Sea Hawk|P.1040]] in 1944.<ref name="Mason1991">{{cite book|last=Mason|first=Francis K|title=Hawker Aircraft since 1920|year=1991|publisher=Putnam|url=https://books.google.com/books?id=ooJH-hcMJA0C|isbn=9781557503510}}</ref>

The [[1957 Defence White Paper]] contained the pronouncement that the defence of the country would in the future be achieved with guided weapons, rather than piloted fighters. Hawker's were then proceeding with a private venture design of a [[Mach number|Mach 2+]] fighter designated [[Hawker P.1121|P.1121]]. The cancellation of the P.1121 led to increased emphasis on another project which soon became the [[Hawker P.1127|P.1127]].<ref name="Guardian1988" />

In 1957, Chaplin became Chief Designer<ref name="Times1988" /> and in 1959 an Executive Director on the Hawker Board.<ref name="RAeS awards" /><ref name="Flight1959">{{cite journal | date = 24 April 1959| title = Hawker Board Strengthened| journal = [[Flight (magazine)|Flight]] | page = 562| url = http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%201167.html| accessdate = 31 January 2013}}</ref> That year an order for two prototypes of the P.1127 was placed, with first flights of this revolutionary machine in 1960. It was eventually developed into the operational [[Harrier Jump Jet|Harrier jump-jet]] which was notable for its role in the [[Falklands War|Falklands conflict]].<ref name="RAeS awards" /> In 1960, the [[Royal Aeronautical Society]] awarded Chaplin its Silver Medal "for his achievements in the design and development of military aircraft".<ref name="Times1960">{{cite newspaper The Times | title = Aircraft Designers Honoured | url =http://infotrac.galegroup.com/itw/infomark/434/259/206326802w16/purl=rc1_TTDA_0_CS118316722&dyn=17!xrn_33_0_CS118316722&hst_1?sw_aep=cam_earl | location=London | date = 18 May 1960 | page = 7| issue = 54774 |column = C |accessdate = 1 February 2013 |subscription=yes}}</ref><ref name="Flight1960">{{cite journal |date=20 May 1960 |title=RAeS Honours|journal=[[Flight (magazine)|Flight]] |volume= |issue=|page=675|publisher=|doi= |url=http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200675.html|accessdate=31 January 2013}}</ref> In mid-1961 Chaplin suffered a heart attack; he retired in 1962.<ref name="Flight1962">{{cite journal |date=10 May 1962 | title = Hawker Aircraft Change | journal = [[Flight (magazine)|Flight]] | page=721 | url =http://www.flightglobal.com/pdfarchive/view/1962/1962%20-%200723.html|accessdate=31 January 2013}}</ref>

{{Clear left}}<!--Places following text ''after'' image-->

== Retirement ==
Hawker's had full UK government support for the supersonic vertical and/or short take-off and landing ([[V/STOL]]) [[Hawker P.1154|P.1154]] while its competitor, the [[British Aircraft Corporation]], was building the [[BAC TSR2|TSR2]]. In 1965, [[Harold Wilson]]’s government realized that the country could afford neither of these aircraft, and abruptly cancelled them.

The 1982 [[Falklands War|Falklands conflict]] might not have been won by Britain but for the [[BAE Sea Harrier|Harrier]], in the opinion of [[Admiral of the Fleet (Royal Navy)|Admiral of the fleet]] [[Terence Lewin, Baron Lewin]].<ref name="Godden1983">{{cite book|last=Godden|first=John|title=Harrier - Ski Jump to Victory|year=1983|publisher=Brassey|location=Oxford|pages=v|url=https://books.google.com/books?id=_2nENAAACAAJ}}</ref> Libby Fawbert interviewed Chaplin in his quiet Wiltshire home for PM on [[BBC Radio 4]].<ref name="BBC1982">{{cite interview |last=Chaplin |first=R H |authorlink=Roy Chaplin |interviewer=Libby Fawbert |title=The Falklands War: Roy Chaplin on the Harrier |publisher =[[BBC]]|location=London |date=late-May/early-June 1982 |work=PM on [[BBC Radio 4|Radio 4]] at 17:00|accessdate=7 February 2013}}</ref>
 
1985 was the fiftieth anniversary of the first flight of the Hurricane. The Royal Aeronautical Society organized a Hurricane Golden Jubilee symposium. At eighty-seven years old, Chaplin was the oldest of four speakers who had worked on the design of the aircraft.<ref name="Hurricane" />

Chaplin died on 13&nbsp;December 1988 at age 89. A Harrier jump-jet flew over the village church in tribute, at the end of his memorial service.<ref name="Times1988" />

== See also ==
* [[Aerospace industry in the United Kingdom]]
* Roy Chaplin interviewed by Geoff Meade in 1982 for [[ITV Central|Central TV (now ITV Central)]]
[[File:Interview of Roy Chaplin 1982 by Geoff Meade for Central TV.webm|thumb|left|Roy Chaplin interviewed by Geoff Meade in 1982 for [[ITV Central|Central TV]]]]

{{clear}}

== References ==
{{reflist|2}}

== Further reading ==
* {{cite journal |last=Hislop|first=GS|year=1998 |title=Sir Robert Lickley|journal=[[Royal Society of Edinburgh]] |volume= |issue=|page=|format=PDF|publisher= |doi= |url=http://www.royalsoced.org.uk/cms/files/fellows/obits_alpha/Lickley_r.pdf|accessdate=31 January 2013}}
* {{cite journal | date=6 April 1939| title=Ancillary Power Services| journal=[[Flight (magazine)|Flight]] | pages=357&ndash;358| url=http://www.flightglobal.com/pdfarchive/view/1939/1939%20-%201030.html|accessdate=31 January 2013}}
* {{cite journal |date=30 November 1951 | title = Hawker Executives | journal = [[Flight (magazine)|Flight]] | page=696| url = http://www.flightglobal.com/pdfarchive/view/1951/1951%20-%202403.html|accessdate=31 January 2013 | quote = [Photos] ... Below, left to right: R. H. Chaplin, O.B.E., B.Sc, F.R.Ae.S. (asst. chief designer); ...}}
* {{cite journal | date = 5 October 1953| title = Hawker P.1067| journal = [[Flight (magazine)|Flight]] | page = 454|url =http://www.flightglobal.com/pdfarchive/view/1951/1951%20-%202017.html| accessdate = 31 January 2013 | quote=In presenting this feature on Hawker's new fighter we had hoped to include, as well as the machine, the men behind it; but the retiring nature so characteristic of the scientist and technician has asserted itself to such an extent that the team responsible (led, of course, by Mr. Sydney Camm, C.B.E., F.R.Ae.S.) prefer to remain pictorially anonymous, ... We can, however, name those chiefly concerned. They are: R. H. Chaplin, O.B.E., B.Sc, F.R.Ae.S.; ...}}
* {{cite journal | date = 24 June 1948| title = A Model Day| journal = [[Flight (magazine)|Flight]] | page = 684| url =http://www.flightglobal.com/pdfarchive/view/1948/1948%20-%200944.html| accessdate = 31 January 2013 | quote=N.H.M.F.C. Distinguished visitors who had the honour of being presented to Her Majesty were ... Mr. and Mrs. R. H. Chaplin, ...}}
* {{cite journal | date = 14 October 1960| title = Top Designers at Avro| journal = [[Flight (magazine)|Flight]] | page = 621|url =http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%202327.html| accessdate = 31 January 2013 | quote=Members of the group's Design Council, from left to right in the top picture, are: R. W. Walker, director and chief designer, Gloster Aircraft; C. E. Fielding, works director, A. V. Roe & Co; R. H. Chaplin, executive director and chief designer, Hawker Aircraft; ...}}<!--Image of Chaplin but we can't use it due copyright-->
* {{cite journal | date=15 June 1961| title= Hawker and Folland Changes| journal = [[Flight (magazine)|Flight]] | page=818| url = http://www.flightglobal.com/pdfarchive/view/1961/1961%20-%200808.html|accessdate=31 January 2013}}
*{{cite newspaper The Times|title=Stuart Davies Obituary|accessdate=2 February 2013|date=11 February 1995}} (mentions working with Roy Chaplin and Sydney Camm on the Fury Monoplane in 1933.) 
*{{cite news|last=Smith|first=Godfrey|title=Why the left has no time for Tiffin|accessdate=2 February 2013|newspaper=The Sunday Times|date=22 November 1981}}(brief discussion of a dinner held at Tiffin's at Kingston)

{{DEFAULTSORT:Chaplin, Roy}}
[[Category:1899 births]]
[[Category:1988 deaths]]
[[Category:Aircraft designers]]
[[Category:People from Kingston upon Thames]]
[[Category:Battle of Britain]]
[[Category:English aerospace engineers]]
[[Category:Fellows of the Royal Aeronautical Society]]
[[Category:Hawker aircraft]]
[[Category:Officers of the Order of the British Empire]]
[[Category:People educated at Tiffin School]]
[[Category:Royal Engineers officers]]
[[Category:British military personnel of World War I]]
[[Category:Articles containing video clips]]