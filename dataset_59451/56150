{{Infobox journal
| cover = [[File:CoverIssueCatalysisST.jpg]]
| discipline = [[Chemistry]], [[catalysis]]
| abbreviation = Catal. Sci. Tech.
| impact = 5.426 (2014)
| openaccess = [[Hybrid open access journal|Hybrid]]
| editor = Noritaka Mizuno, Piet van Leeuwen
| website = http://www.rsc.org/catalysis
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| history = 2011-present
| frequency = Monthly
| ISSN = 2044-4753
| eISSN = 2044-4761
| CODEN = CSTAGD
| OCLC = 703389810
}}
{{Portal|Chemistry}}
'''''Catalysis Science & Technology''''' is a [[peer-reviewed]] [[scientific journal]] that is published monthly by the [[Royal Society of Chemistry]].<ref name="about CST" /><ref name="Queen's Uni" /> The [[Editor-in-chief|editors-in-chief]] are Noritaka Mizuno ([[University of Tokyo]], Japan) and Piet van Leeuwen ([[Rovira i Virgili University #University institutes|Institute of Chemical Research of Catalonia, ICIQ]], Tarragona, [[Catalonia]], Spain).<ref name="CST Board" />

The first online articles were published in January 2011, and the first issue of ''Catalysis Science & Technology'' appeared in March 2011. All articles published up to the end of 2012 are available free online.<ref name="RSC blog" /> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.426.<ref name="WoS" />

==Scope==
''Catalysis Science & Technology'' covers both the science of [[catalysis]] and catalysis technology, including applications addressing global issues. The journal publishes research in the [[applied science|applied]], [[Fundamental science|fundamental]], [[Experimental science|experimental]] and [[Computational science|computational]] areas of catalysis. Contributions are made by the [[homogeneous catalysis|homogeneous]], [[heterogeneous catalysis|heterogeneous]] and [[biocatalysis]] communities.<ref name="CS&T Journal Policy" />

==Article types==
''Catalysis Science & Technology'' publishes the following types of articles:<ref name="CS&T Journal Policy" />
* Full Papers (original scientific work)
* Communications (preliminary accounts that merit urgent publication)
* Mini-reviews (short accounts of the published articles on the topic of catalysis)
* Perspectives (personal accounts or critical analyses of specialist areas)

== Abstracting and indexing ==
''Catalysis Science & Technology'' is abstracted and indexed in the [[Science Citation Index]]<ref name="masterList" /> and [[Scopus]].

==References==
{{reflist |refs=

<ref name="about CST">{{cite web |url=http://www.rsc.org/publishing/journals/CY/about.asp |title=About Catalysis Science & Technology |publisher=Royal Society of Chemistry |accessdate=2 February 2015}}</ref>

<ref name="Queen's Uni">{{cite web |url=http://library.queensu.ca/library/news/story/11/02/08/new-journal-catalysis-science-technology |title=New Journal: Catalysis Science & Technology |publisher=Queen's University Library |accessdate=2 February 2015}}</ref>

<ref name="CST Board">{{cite web |url=http://www.rsc.org/publishing/journals/cy/staff.asp |title=Catalysis Science & Technology Staff, Editorial Board and Contact Details |year=2015 |publisher=Royal Society of Chemistry |accessdate=2 February 2015}}</ref>

<ref name="RSC blog">{{cite web |url=http://blogs.rsc.org/cy/2011/01/31/first-catalysis-science-technology-articles-published-online/ |title=First Catalysis Science & Technology articles published online |publisher=Royal Society of Chemistry |accessdate=2 February 2015}}</ref>

<ref name="WoS">{{cite book |year=2014 |chapter=Catalysis Science and Technology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

<ref name="CS&T Journal Policy">{{cite web |title=Catalysis Science and Technology Journal Policy |url=http://www.rsc.org/Publishing/Journals/guidelines/AuthorGuidelines/JournalPolicy/Journals/CY.asp |accessdate=2 February 2015}}</ref>

<ref name="masterList">{{cite web |title=Catalysis Science & Technology |work=Master Journal List |publisher=[[Thomson Reuters]] |url=http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=2044-4753 |accessdate=2 February 2015}}</ref>

}}

== External links ==
* {{Official website|http://www.rsc.org/catalysis}}

{{Royal Society of Chemistry |state=collapsed}}

{{DEFAULTSORT:Catalysis Science and Technology}}
[[Category:Chemistry journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Hybrid open access journals]]
[[Category:Publications established in 2011]]