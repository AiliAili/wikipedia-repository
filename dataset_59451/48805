{{Underlinked|date=June 2013}}

The ISTAF World Cup is a competition organised by the [[International Sepaktakraw Federation|International Sepaktakraw Federation (ISTAF)]] to modernise the traditional sport of [[Sepaktakraw]]. Alongside the ISTAF SuperSeries, the ISTAF World Cup is a platform to showcase the best of the sport and seeks to broaden the appeal of the sport to the international community.<ref>{{cite web|title=Intentions to expand the ISTAF SuperSeries in Season Two|url=http://sportsbusinessinsider.com.au/international-news/intentions-to-expand-the-istaf-super-series-in-season-two/|publisher=Sports Business Insider|accessdate=11 June 2013}}</ref>

The first ever ISTAF World Cup debuted in Titiwangsa Stadium, Kuala Lumpur, Malaysia during July 2011 and saw the participation of 36 international teams and a total of 180 athletes.<ref>{{cite web|title=Malaysia Hosts First ISTAF Sepaktakraw World Cup|url=http://sportsnewsmalaysia.blogspot.sg/2011/02/malaysia-hosts-first-istaf-sepaktakraw.html|publisher=Malaysian Sports|accessdate=11 June 2013}}</ref>

The winner for the 1st ISTAF World Cup (Men) was Thailand and the runner-up was Malaysia. The winner for the 1st ISTAF World Cup (Women) was Thailand and the runner-up was Vietnam.<ref>{{cite web|title=Vietnam takes silver in World Sepak Takraw Cup|url=http://vietnambreakingnews.com/2011/07/vietnam-takes-silver-in-world-sepak-takraw-cup/#.UbfT8vYY3MQ|publisher=Vietnam Breaking News}}</ref><ref>{{cite web|title=Thailand wins first Sepaktakraw WC|url=http://mmtimes.com/index.php/sports/2313-thailand-wins-first-sepaktakraw-wc.html|publisher=Myanmar Times|accessdate=12 June 2013}}</ref>

== Men's draw ==

The first round, or group stage, saw 23 men's teams divided into 8 groups of 3 (one group had only 2 teams). Each group featured a round robin of 3 games, with each team playing against every other team in their group once.

Based on points accumulated, the top 2 teams from each group advanced to the second round, or playoff stage. The host nation also advanced automatically to the playoff stage.

== Women's draw ==

The first round, or group stage, saw 11 women's teams divided into 4 groups of 3 (one group had only 2 teams). Each group featured a round robin of 3 games, with each team playing against every other team in their group once.

Based on points accumulated, the top 2 teams from each group advanced to the quarter-finals, or playoff stage. The host nation also advanced automatically to the playoff stage.

== Participating countries ==

{| class="wikitable" style= "text-align: left; Width:70%"
!colspan= 4| Participating countries (men)
|- 
!colspan=4 style= "text-align: left"| Host 
|-
| colspan=4| {{MAS}}
|-
!width=10%|Group A
!width=10%|Group B
!width=10%|Group C
!width=10%|Group D
|-
| {{THA}} (Seed) || {{VIE}} (Seed) || {{BRU}} (Seed) || {{SIN}} (Seed)
|-
| {{PHI}} || {{CHN}} || {{AUS}} || {{GER}}
|-
| {{BAN}} || {{IRN}} || {{JAP}} || {{LKA}}
|-
!width=10%|Group E
!width=10%|Group F
!width=10%|Group G
!width=10%|Group H
|-
| {{MMR}} (Seed) || {{INA}} (Seed) || {{KOR}} (Seed) || {{LAO}} (Seed)
|-
| {{TPE}} || {{USA}} || {{IND}} || {{PAK}}
|-
| {{FRA}} || {{SWI}} || {{CAM}} || - 
|}

{| class="wikitable" style= "text-align: left; Width:70%"
!colspan= 4| Participating countries (women)
|- 
!colspan=4 style= "text-align: left"| Host 
|-
| colspan=4| {{MAS}}
|-
!width=10%|Group A
!width=10%|Group B
!width=10%|Group C
!width=10%|Group D
|-
| {{CHN}} (Seed) || {{MMR}} (Seed) || {{VIE}} (Seed) || {{THA}} (Seed)
|-
| {{INA}} || {{KOR}} || {{PHI}} || {{CAM}}
|-
| - || {{JAP}} || {{IND}} || {{PAK}} 
|}

== Men's results ==

=== Group A (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group A
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{THA}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{PHI}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{BAN}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{THA}} || align=center| 3-0 || {{PHI}}
|- style=font-size:90%
|align=right| {{PHI}} || align=center| 3-0 || {{BAN}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{BAN}} || align=center| 0-3 || {{THA}}
|}

=== Group B (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group B
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{VIE}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{IRN}} || 2 || 1 || 1 || 4
|-
| 3 ||style= "text-align: left"| {{CHN}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{VIE}} || align=center| 3-1 || {{IRN}}
|- style=font-size:90%
|align=right| {{IRN}} || align=center| 3-0 || {{CHN}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{CHN}} || align=center| 0-3 || {{VIE}}
|}

=== Group C (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group C
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{BRU}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{JAP}} || 2 || 1 || 1 || 4
|-
| 3 ||style= "text-align: left"| {{AUS}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{AUS}} || align=center| 0-3 || {{BRU}}
|- style=font-size:90%
|align=right| {{BRU}} || align=center| 3-1 || {{JAP}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{JAP}} || align=center| 3-0 || {{AUS}}
|}

=== Group D (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group D
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{SIN}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{GER}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{LKA}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{SIN}} || align=center| 3-0 || {{GER}}
|- style=font-size:90%
|align=right| {{GER}} || align=center| 3-0 || {{LKA}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{LKA}} || align=center| 0-3 || {{SIN}}
|}

=== Group E (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group E
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{MMR}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{TPE}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{FRA}} || 2 || 0 || 2 || 2
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{MMR}} || align=center| 3-0 || {{TPE}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{FRA}} || align=center| 0-3 || {{MMR}}
|- style=font-size:90%
|align=right| {{TPE}} || align=center| 3-2 || {{FRA}}
|}

=== Group F (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group F
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{INA}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{USA}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{SWI}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{SWI}} || align=center| 0-3 || {{USA}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{USA}} || align=center| 0-3 || {{INA}}
|- style=font-size:90%
|align=right| {{INA}} || align=center| 3-0 || {{SWI}}
|}

=== Group G (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group G
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{KOR}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{IND}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{CAM}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{CAM}} || align=center| 0-3 || {{KOR}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{IND}} || align=center| 3-0 || {{CAM}}
|- style=font-size:90%
|align=right| {{KOR}} || align=center| 3-0 || {{IND}}
|}

=== Group H (men) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group H
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{LAO}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{PAK}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{LAO}} || align=center| 3-0 || {{PAK}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{PAK}} || align=center| 0-3 || {{LAO}}
|}

=== Knock-out rounds (men) ===

{{Round16
<!-- Date|Team 1|Score 1|Team 2|Score 2 -->
<!-- round of 16 -->
| 23 July  | {{TPE}} | 0 | {{MAS}} | '''3'''
| 23 July  | {{PHI}} | '''3''' | {{BRU}} | 2
| 23 July  | {{SIN}} | '''3''' | {{USA}} | 2
| 23 July  | {{VIE}} | 1 | {{IND}} | '''3'''
| 23 July  | {{THA}} | '''3''' | {{IRN}} | 0
| 23 July  | {{INA}} | '''3''' | {{LAO}} | 2
| 23 July  | {{JAP}} | 0 | {{KOR}} | '''3'''
| 23 July  | {{GER}} | 0 | {{MMR}} | '''3'''
<!-- quarter-finals -->
| 23 July  | {{MAS}} | '''3''' | {{PHI}} | 0
| 23 July  | {{SIN}} | '''3''' | {{IND}} | 2
| 23 July  | {{THA}} | '''3''' | {{INA}} | 0
| 23 July  | {{KOR}} | 0 | {{MMR}} | '''3'''
<!-- semi-finals -->
| 24 July  | {{MAS}} | '''3''' | {{SIN}} | 0
| 24 July  | {{THA}} | '''3''' | {{MMR}} | 0
<!-- Final -->
| 24 July  | {{MAS}} | 0 | {{THA}} | '''3'''
<!-- Third-place playoff -->
|  | - |  | - | 
|widescore=yes}}

== Women's results ==

=== Group A (women) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group A
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{CHN}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{INA}} || 2 || 0 || 2 || 1
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{INA}} || align=center| 1-3 || {{CHN}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{CHN}} || align=center| 3-0 || {{INA}}
|}

=== Group B (women) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group B
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{MMR}} || 2 || 1 || 1 || 4
|-
| 2 ||style= "text-align: left"| {{KOR}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{JAP}} || 2 || 1 || 1 || 3
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{JAP}} || align=center| 0-3 || {{MMR}}
|- style=font-size:90%
|align=right| {{KOR}} || align=center| 0-3 || {{JAP}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{MMR}} || align=center| 1-3 || {{KOR}}
|}

=== Group C (women) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group C
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{IND}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{VIE}} || 2 || 1 || 1 || 5
|-
| 3 ||style= "text-align: left"| {{PHI}} || 2 || 0 || 2 || 1
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{VIE}} || align=center| 3-0 || {{PHI}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{PHI}} || align=center| 1-3 || {{IND}}
|-
|- style=font-size:90%
|align=right| {{IND}} || align=center| 3-2 || {{VIE}}
|}

=== Group D (women) ===

{| class="wikitable" style= "text-align: center; Width:50%"
!colspan= 6| Group D
|-
!width=2%|Rank
!width=10%|Country
!width=5%|Games <br> played
!width=5%|Win
!width=5%|Lost
!width=5%|Pts
|-
| 1 ||style= "text-align: left"| {{THA}} || 2 || 2 || 0 || 6
|-
| 2 ||style= "text-align: left"| {{CAM}} || 2 || 1 || 1 || 3
|-
| 3 ||style= "text-align: left"| {{PAK}} || 2 || 0 || 2 || 0
|}

{| style="width:50%;" cellspacing="1"
|-
!width=15%|
!width=10%|
!width=15%|
|-
|21 July 2011
|- style=font-size:90%
|align=right| {{CAM}} || align=center| 3-0 || {{PAK}}
|- style=font-size:90%
|align=right| {{PAK}} || align=center| 0-3 || {{THA}}
|-
|22 July 2011
|- style=font-size:90%
|align=right| {{THA}} || align=center| 3-0 || {{CAM}}
|}

=== Knock-out rounds (women) ===

{{Round8
<!--Date-Place|Team 1|Score 1|Team 2|Score 2 -->
<!--quarter finals  -->
|23 July | {{MAS}} | '''3''' | {{CAM}} | 0
|23 July | {{MMR}} | 0 | {{VIE}} | '''3'''
|23 July | {{THA}} | '''3''' | {{KOR}} | 0
|23 July | {{IND}} | 0 | {{CHN}} | '''3'''
<!--semi finals  -->
|24 July | {{MAS}} | 1 | {{VIE}} | '''3'''
|24 July | {{THA}} | '''3''' | {{CHN}} | 0
<!--final  -->
|24 July | {{VIE}} | 0 | {{THA}} | '''3'''
<!--third place  -->
| | - | | - |
|widescore=yes}}

== Broadcast and media ==

Internationally, the ISTAF World Cup 2011 was broadcast throughout 68 countries across the seven continents:<ref>{{cite web|title=UFA secures another sepaktrakaw partnership|url=http://www.sportspromedia.com/news/ufa_secures_another_sepaktrakaw_parntership/|publisher=SportsPro|accessdate=12 June 2013}}</ref>

{| class="wikitable" style= "text-align: left; Width:100%"
!width=13%|Region
!width=10%|Country
!width=13%|Region
!width=10%|Country
!width=13%|Region
!width=10%|Country
|-
| rowspan="9" | South-East Asia || Brunei || rowspan="14" | Western & Northern Europe || Austria || rowspan="24" | Central & Eastern Europe || Albania
|-
| Cambodia || Belgium || Armenia
|- 
| Indonesia || Denmark || Azerbaijan
|-
| Malaysia || Finland || Belarus
|-
| Myanmar || France || Bosnia-Herzegovina
|-
| Philippines || Germany || Bulgaria
|-
| Singapore || Ireland || Croatia
|-
| Thailand || Iceland || Czech Republic
|- 
| Vietnam || Luxemburg || Estonia
|-
| rowspan="6" | East & South Asia || China || Netherlands || Georgia
|- 
| Hong Kong || Norway || Hungary
|-
| Taiwan || Sweden || Kazakhstan
|-
| Mongolia || Switzerland || Latvia
|- 
| Korea || United Kingdom || Lithuania
|-
| Sri Lanka || rowspan="9" | Southern Europe || Cyprus || Macedonia
|-
| rowspan="5" | Oceania || Australia || Greece || Maldova
|- 
| New Zealand || Italy || Montenegro
|-
| Fiji || Lebanon || Poland
|-
| Papua New Guinea || Malta || Romania
|-
| Tonga || Portugal || Russia
|-
| North America || USA || Spain || Serbia
|-
| South America || Brazil || Turkey || Slovak Republic
|-
| - || - || Israel || Slovenia
|-
| - || - || - || - || Ukraine
|}

== References ==

{{Reflist}}

== External links ==

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box.  Just press "Save page". -->

[[Category:Articles created via the Article Wizard]]
[[Category:Sepak takraw]]