'''Derek Bouchard-Hall''' (born July 16, 1970) is a former US professional cyclist, whose career highlights include winning the US Professional Criterium Championship, winning the gold medal in the team pursuit at the Pan American Games, and competing on the 2000 US Olympic team in Sydney, Australia and for Mercury at Paris–Roubaix in 2001. After retiring from cycling in 2002, Bouchard-Hall pursued a career in business, including positions as a management consultant at McKinsey and as a Director at international cycling retailer, Wiggle.com. From June 2015, he will be the CEO and President of USA Cycling.<ref>https://www.usacycling.org/bouchard-hall-named-usa-cycling-chief-executive-officer.htm/ref></ref>

== Biography ==

'''Cycling and Education'''

Derek Albert Bouchard-Hall was born  at the naval base in Port Hueneme, CA, USA, where his naval officer father was based. He spent most of his childhood in Norton, MA, where he graduated from Norton High School in 1988.  Though a successful runner in high school, a foot injury led Bouchard-Hall to pursue cycling at [[Princeton University]].   Bouchard-Hall graduated from Princeton in 1992, with a degree in Architectural Engineering, then moved to the West Coast to attend [[Stanford University]].

Bouchard-Hall graduated from Stanford in 1994 with an MS in Structural Engineering and began his professional cycling career, signing with Team Shaklee. Shaklee teammates introduced Bouchard-Hall to [[track cycling]], and he soon began to train with the US track cycling team.  From 1996–2000, Bouchard-Hall divided his cycling calendar into training and competing for both his professional road cycling team and for the US National track cycling team.

In 1998, Bouchard-Hall moved from Team Shaklee to the newly formed Mercury Cycling Team, where he competed both domestically and internationally. It was as a member of the Mercury team where Bouchard-Hall achieved his greatest road successes, winning the [[United States National Criterium Championships]] in 2000 and competing in [[Paris–Roubaix]], [[Gent–Wevelgem]], and [[Critérium International]].

In 1999, Bouchard-Hall was a member of the gold medal winning team pursuit squad at the [[Pan American Games]] in Winnipeg, Canada.<ref>{{cite web|url=http://sportsillustrated.cnn.com/olympics/news/1999/07/30/panam_results_ap/ |title=CNN/SI – Olympics – Pan Am Games Friday Event Capsules – Saturday July 31, 1999 02:48 AM |publisher=Sportsillustrated.cnn.com |date=1999-07-31 |accessdate=2014-05-19}}</ref>
In 2000, Bouchard-Hall achieved another career highlight when he was selected as a member of the US Olympic team, to compete in the track cycling team pursuit event, after winning the US National Championship in the team pursuit with Erin Hartwell, Mariano Friedick, and Tommy Mulkey.  At the [[2000 Sydney Olympics]], Bouchard-Hall and the US team pursuit team finished in 10th place.<ref>{{cite web|url=http://www.sports-reference.com/olympics/athletes/bo/derek-bouchard-hall-1.html |title=Derek Bouchard-Hall Bio, Stats, and Results &#124; Olympics at |publisher=Sports-reference.com |date=1970-07-16 |accessdate=2014-05-19}}</ref><ref>{{cite web|url=http://www.paloaltoonline.com/weekly/morgue/sports/2000_Sep_22.CYCLING.html |title=Truing Olympic cycling dreams |publisher=Paloaltoonline.com |date=2000-09-22 |accessdate=2014-05-19}}</ref>
Bouchard-Hall continued to cycle for Mercury until the 2002 cycling season, when he announced his retirement at the age of 32 during the US Road Championships in Philadelphia, PA., in June.  At the time, Mercury was ranked #1 in the NRC team standings.<ref>{{cite web|url=http://autobus.cyclingnews.com/news/?id=2002/jun02/jun12news |title=www.cyclingnews.com – the world centre of cycling |publisher=Autobus.cyclingnews.com |date=2002-06-12 |accessdate=2014-05-19}}</ref><ref>{{cite web|url=http://www.dailypeloton.com/displayarticle.asp?pk=926 |title=Pro Cycling News |publisher=Daily Peloton |date=2002-06-13 |accessdate=2014-05-19}}</ref>

'''Exterior and Common Iliac Endofibrosis'''

Toward the end of his cycling career, Bouchard-Hall began experiencing pain and loss of strength in his left leg when he was training.  After months of various medical and physio examinations, and through his own extensive research, he sought the advice of a vascular surgeon at the [[Mayo Clinic]] in Rochester, MN, USA.<ref>{{cite web|url=http://www.apnewsarchive.com/2000/Cyclist-Bouchard-Hall-Perseveres/id-9ab0560e55b12249e0f77d33d564ca7d |title=Cyclist Bouchard-Hall Perseveres |publisher=Apnewsarchive.com |date=2000-08-25 |accessdate=2014-05-19}}</ref>  Bouchard-Hall underwent surgery at [[Stanford University Medical Center]] to correct the problem in the winter of 2000, which ultimately led to his most successful year as a professional cyclist.  In 2002, however, the pain and numbness in his leg returned, and, rather than risking a second highly invasive surgery, he retired.<ref>{{cite web|url=http://velonews.competitor.com/2002/06/news/mercurys-bouchard-hall-retires_2361 |title=Mercury’s Bouchard-Hall retires – VeloNews.com |publisher=Velonews.competitor.com |date=2002-06-11 |accessdate=2014-05-19}}</ref>

'''Post-Cycling Career'''

From 2002–2004, Bouchard-Hall attended [[Harvard Business School]], graduating with an MBA in 2004, and began working in management consulting, first in Boston and then, in 2006, for [[McKinsey and Company]] in London. In 2009–2010, he served as interim director of the US Small Business Administration's [[HUBZone]] program, in Washington, DC, before returning to London in the summer of 2010.  Since October 2011, Bouchard-Hall has been an executive with [[Wiggle Ltd]],<ref>{{cite web|last=Harker |first=Jonathon |url=http://www.bikebiz.com/news/read/us-olympic-cyclist-joins-wiggle/012079 |title=US Olympic cyclist joins Wiggle &#124; Bicycle Business |publisher=BikeBiz |date=2011-10-21 |accessdate=2014-05-19}}</ref> one of the world's largest internet retailers of cycling, running, and triathlon equipment. Bouchard-Hall succeeded Steve Johnson as President and CEO of [[USA Cycling]] in June 2015.<ref>{{cite web|url=http://velonews.competitor.com/2015/04/news/wiggle-exec-and-former-pro-derek-bouchard-hall-appointed-ceo-of-usa-cycling_368479 |title=Wiggle exec and former pro Derek Bouchard-Hall appointed CEO of USA Cycling |publisher=Velonews.com |date=2015-04-28 |accessdate=2015-05-12}}</ref>

'''Personal'''

Bouchard-Hall is married with two children and resides in the UK.

•••••••••••••

'''Professional Cycling Resume'''<ref>{{cite web|url=http://www.cyclingarchives.com/coureurfiche.php?coureurid=2516 |title=Derek Bouchard-Hall |publisher=Cyclingarchives.com |date=1970-07-16 |accessdate=2014-05-19}}</ref>

1994
* 1st in Prologue Tour de Toona, United States of America

1995
* 3rd in National Championship, Track, Points race, Elite, United States of America, USA
* 2nd in National Championship, Track, Elimination, Elite, United States of America, USA
* 2nd in National Championship, Track, Scratch, Elite, United States of America, USA

1996
* 1st in Prologue Norman, United States of America
* 1st in Stage 3 Wichita Falls Race, United States of America
* 5th in National Championship, Road, Criterium, Elite, United States of America, Downers Grove (Illinois), USA

1997
* 1st in Prologue Stage Race Norman, USA
* 1st in Stage 2 Merced, USA
* 3rd in Stage 1 Redlands Bicycle Classic, Highland (California), USA

1998
* 1st in Los Gatos, USA
* 2nd in Prologue Redlands Bicycle Classic, Highland (California), USA
* 3rd in Stage 1 Redlands Bicycle Classic, Highland (California), USA

1999
* 3rd in Downers Grove, USA
* 1st in Stage 4 Fitchburg Longsjo Classic, USA
* 1st in Merced, USA
* 1st in Prologue Tour of Willamette, USA
* 2nd in Stage 2 Merced, USA
* 1st in Pescadero, USA
* 2nd in National Championship, Road, Criterium, Elite, United States of America, Downers Grove (Illinois), USA
* 3rd in Stage 6 Trans Canada, Canada

2000
* 1st in Downers Grove, USA
* 2nd in Natchez, USA
* 1st in National Championship, Track, Team Pursuit, Elite, United States of America, USA (w/Erin Hartwell,Mariano Friedick, Tommy Mulkey)
* 1st in Santa Rosa, Criterium, USA
* 3rd in Milton, USA
* 1st in Pescadero, United States of America
* 2nd in Stage 4 Cascade Classic, Bend (Oregon), USA
* 3rd in Stage 6 Cascade Classic, Bend (Oregon), USA
* 2nd in Stage 1 Tour de Toona, USA
* 3rd in Stage 3 Tour de Toona, Hollidaysburg (Pennsylvania), USA
* 1st in National Championship, Road, Criterium, Elite, United States of America, Downers Grove (Illinois), USA

2001
* 1st in Extran Challenge, USA
* 1st in Stage 1 Valley of the Sun Stage Race, USA
* 2nd in Snelling, USA
* 1st in Tour of the Hilltowns, USA
* 1st in Attleboro, USA
* 1st in Stage 5 Wendy's International Cycling Classic, USA
* 2nd in General Classification Wendy's International Cycling Classic, USA
* 1st in Concord, USA
* 1st in Bow, USA
* 2nd in National Championship, Road, Criterium, Elite, United States of America, USA

2002
* 1st in Cantua Creek, USA
* 3rd in Dinuba, Criterium, USA

== References ==
<!--- See [[Wikipedia:Footnotes]] on how to create references using tags which will then appear here automatically -->
{{Reflist}}

{{Use mdy dates|date=December 2015}}

{{DEFAULTSORT:Bouchard-Hall, Derek}}
[[Category:Articles created via the Article Wizard]]
[[Category:1970 births]]
[[Category:Living people]]
[[Category:Cyclists at the 2000 Summer Olympics]]
[[Category:Olympic cyclists of the United States]]
[[Category:American male cyclists]]
[[Category:Harvard Business School alumni]]
[[Category:Pan American Games medalists in cycling]]
[[Category:Pan American Games gold medalists for the United States]]