{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Pilgrim Tercentenary half dollar
| Value               = 50 cents (0.50
| Unit                = [[United States dollar|US dollars]])
| Mass                = 12.5
| Diameter            = 30.61
| Diameter_inh       = 1.20
| Thickness           = 2.15
| Thickness_inch      = 0.08
| Edge                = [[Reeding|Reeded]]
| Silver_troy_oz      = 0.36169
| Composition =
  {{plainlist |
* 90.0% silver
* 10.0% copper
 }}
| Years of Minting    = 1920–21
| Mintage = {{plainlist|1920: 200,112 including 112 pieces for the [[United States Assay Commission|Assay Commission]] (48,000 melted)<br>
1921: 100,053 including 53 assay pieces (80,000 melted)}}
| Mint marks            = None, all pieces struck at the [[Philadelphia Mint]] without mint mark
| Obverse             = Pilgrim tercentenary half dollar commemorative obverse.jpg
| Obverse Design      = Governor [[William Bradford (Plymouth Colony governor)|William Bradford]] (coins struck in 1920 do not display a date on this side)
| Obverse Designer    = [[Cyrus E. Dallin]]
| Obverse Design Date   = 1920
| Reverse               = Pilgrim tercentenary half dollar commemorative reverse.jpg
| Reverse Design        = The ''[[Mayflower]]''
| Reverse Designer      =[[Cyrus E. Dallin]]
| Reverse Design Date   = 1920
}}

The '''Pilgrim Tercentenary half dollar''' or '''Pilgrim half dollar''' was a [[Early United States commemorative coins|commemorative]] fifty-cent coin struck by the [[United States Mint|United States Bureau of the Mint]] in 1920 and 1921 to mark the 300th anniversary ([[tercentenary]]) of the arrival of the [[Pilgrim Fathers|Pilgrims]] in North America. It was designed by [[Cyrus E. Dallin]].

Massachusetts Congressman [[Joseph Walsh (Massachusetts)|Joseph Walsh]] was involved in joint federal and state efforts to mark the anniversary.  He saw a reference to a proposed [[Maine Centennial half dollar]] and realized that a coin could be issued for the Pilgrim anniversary in support of the observances at [[Plymouth, Massachusetts]]. The bill moved quickly through the legislative process and became the Act of May 12, 1920 with the signature of President [[Woodrow Wilson]].

Sculptor [[James Earle Fraser (sculptor)|James Earle Fraser]] criticized some aspects of the design,  but the Treasury approved it anyway. After a promising start, sales tailed off, and tens of thousands of coins from each year were returned to the [[Philadelphia Mint]] for melting. [[Numismatics|Numismatist]] [[Q. David Bowers]] has cited the fact that the coins were struck in a second year as the start of a trend to force collectors to buy more than one piece in order to have a complete set.

== Background ==
{{main|Pilgrim Fathers}}
The [[Pilgrim Fathers|Pilgrims]] were [[Brownist]] [[English Dissenters]]; they sought a version of the Christian religion without things they deemed nonessential, such as bishops or Christmas. They differed from the [[Puritans]]; the Pilgrims were stricter, and instead of seeking to reform the [[Church of England]] from within, sought to [[Definitions of Puritanism#Separatist groups|separate themselves from it]].{{sfn|Slabaugh|p=44}} They had left England for the Netherlands because in 1608, King [[James I of England|James I]] began to persecute Separatists. Among those who fled then was [[William Bradford (Plymouth Colony governor)|William Bradford]].{{sfn|Flynn|p=151}}

Things became more difficult for the Separatists in the Netherlands in the late 1610s as the Dutch government moved towards alliance with England.{{sfn|Flynn|p=151}} They had few opportunities in the Netherlands as they were limited to manual labor by the [[guild]]s' refusal to accept them, and they feared that their children were straying from their language and religion. Investors led by [[Thomas Weston (merchant adventurer)|Thomas Weston]] agreed to finance an expedition to North America, and the ship [[Speedwell (1577 ship)|''Speedwell'']] was sent to fetch Separatists from the Netherlands, then join the larger ''[[Mayflower]]'' to form a two-ship expedition.  After transporting the Separatists, the ''Speedwell'' proved unseaworthy for the ocean voyage.  The ''Mayflower'''s passenger list was formed from some Separatists who had gone to the Netherlands and some who had stayed in England, as well as a scattering of others.  Some would-be pioneers had to be left behind because of the problems with the ''Speedwell''. The ''Mayflower'' sailed from [[Plymouth]], in [[South West England]], on September 6, 1620, with 102 passengers and a crew of 47.{{sfn|Slabaugh|p=44}}

{{quote box | align = right | width = 24em | salign = right
| quote =  So they left that goodly and plesante citie, which had been their resting place near 12 years; but they knew they were pilgrimes and looked not much on those things, but lift up their eyes to the heavens, their dearest cuntrie, and quieted their spirits.
| source = [[William Bradford (Plymouth Colony governor)|William Bradford]] on the Pilgrims' departure from [[Leiden]]{{sfn|Slabaugh|p=44}}}}

The expedition sighted [[Cape Cod]] on November 9, 1620, and landed at what is now [[Provincetown, Massachusetts]]. Two days later, the men signed the [[Mayflower Compact|''Mayflower'' Compact]], wherein all agreed to submit themselves to the will of the majority—one of the foundation documents of American democracy. They established a settlement at [[Plymouth Colony]] (today [[Plymouth, Massachusetts]]), but they had expected to land further south and were ill-equipped for a Massachusetts winter. Half died before spring came. There had been few Native Americans in the area, but in 1621, the settlers were approached by a group, including two, [[Samoset]] and [[Squanto]], who spoke some English. Squanto taught the Pilgrims indigenous methods for cultivating [[corn]] (maize), a plant native to the New World with which the emigrants were unfamiliar. This knowledge helped the Pilgrims gradually become established. The Pilgrims grew in population relatively slowly over the first generations in America, and became a minority among settlers in the area.  In 1691, [[Plymouth Colony]] became part of the [[Province of Massachusetts Bay]]. Bradford's wife had died while the ship was in [[Provincetown Harbor]]; after Governor [[John Carver (Plymouth Colony governor)|John Carver]] perished during the first winter, Bradford was elected in his place, and served fifteen two-year terms. He guided the colony from the communal economy necessary at first, to privatization, greatly increasing the harvest in the process. His diaries were published as ''[[Of Plymouth Plantation]]'' and constitute the major source of information concerning the Pilgrims' daily lives.{{sfn|Slabaugh|pp=44–45}}{{sfn|Flynn|pp=151–52}}

In 1920, the government did not sell commemorative coins.  Congress, during the [[Early United States commemorative coins|early years of commemorative coinage]], usually designated a specific organization allowed to buy them at face value and to vend them to the public at a premium.{{sfn|Taxay|pp=v–vii}}{{sfn|Slabaugh|pp=3–5}} In the case of the Pilgrim Tercentenary half dollar, the enabling legislation did not name an organization,<ref>{{USPL|66|182}}</ref> but it was the Pilgrim Tercentenary Commission; profits from the coin were to go towards financing the observances in honor of the 300th anniversary of the Pilgrims' arrival.{{sfn|Flynn|p=152}}

== Legislation ==
Legislation for a Pilgrim Tercentenary half dollar was introduced in the House of Representatives by Massachusetts' [[Joseph Walsh (Massachusetts)|Joseph Walsh]] on March 23, 1920, with the bill designated as H.R. 13227.<ref name ="profile">{{cite web|url=http://li.proquest.com/legislativeinsight/LegHistMain.jsp?searchtype=DOCPAGE&parentAccNo=PL66-203&docAccNo=PL66-203&docType=LEG_HIST&resultsClick=true&id=1463843788412|title=Fifty-Cent Piece Commemoration Act|publisher=ProQuest Congressional|accessdate=May 21, 2016|subscription=yes}}</ref> It was referred to the Committee on Coinage, Weights and Measures, of which Indiana Congressman [[Albert Vestal]] was the chairman. That committee held hearings on the bill on March 26, 1920, as well as on the coinage proposal that would become the [[Alabama Centennial half dollar]], which was the first order of business. Once the committee voted to favorably recommend the Alabama bill, which provided for 100,000 half dollars, it proceeded to consider the Pilgrim proposal. Vestal had two days previously written to [[United States Secretary of the Treasury|Treasury Secretary]] [[David F. Houston]], and Houston responded that while his department had not opposed the Maine Centennial (previously approved by the committee) or Alabama coinage bills, the Treasury had concerns that issuing large numbers of different designs would aid fraudsters.{{sfn|House hearings|pp=3–5}}

[[File:Landing of the Pilgrims 1920 U.S. stamp.1.jpg|thumb|left|Two-cent stamp for the tercentenary depicting the landing of the Pilgrims]]
Walsh appeared and explained to the committee that Congress had previously authorized a commission to work with state and local authorities in planning for observances which were to be held in December 1920 on the 300th anniversary of the ''Mayflower'' landing, and were also to occur the following summer. Although events were to be held elsewhere in Massachusetts, and even outside the state, the focus would be on Plymouth, with the beautification of the area around [[Plymouth Rock]] a major goal. The commission had recommended the issuance of commemorative stamps, and also a federal appropriation, but it was not until Walsh saw a committee report for the Maine Centennial piece listed on a House document that he came up with the idea of a commemorative coin for the anniversary.  This had been done in the past for the [[World Columbian Exposition]] of 1893, Walsh recalled, and Ohio's [[William A. Ashbrook]] chimed in (incorrectly) that a coin had been issued for the [[Jamestown Exposition]] of 1907.  Walsh stated that more were being asked for (500,000) than for the Alabama coin because of the great interest in the celebration; he suggested that to increase regional interest, the coins be struck at all the mints ([[Philadelphia Mint|Philadelphia]], [[Denver Mint|Denver]], and [[San Francisco Mint|San Francisco]]).  Ashbrook felt the number to be issued excessive, he suggested 300,000 as more appropriate, and Walsh consented. Missouri's [[William L. Nelson]] moved that the committee approve the amended bill, and this carried.{{sfn|House hearings|pp=5–9}} Vestal issued a report on behalf of his committee on March 26, 1920, indicating his committee's support for the bill once amended.<ref>{{cite web|author=House Committee on Coinage, Weights and Measures|url=http://congressional.proquest.com/congressional/result/pqpresultpage.gispdfhitspanel.pdflink/$2fapp-bin$2fgis-serialset$2f7$2f4$2fe$2fc$2f7653_hrp773_from_1_to_1.pdf+/pilgrim+tercentenary+AND+coin$40$2fapp-gis$2fserialset$2f7653_h.rp.773$40Serial+Set+1$40House+and+Senate+Reports$3b+Reports+on+Public+Bill$40March+26,+1920$40null?pgId=0e7555da-6562-45e2-aea6-f3be9f668b30&rsId=15439529AB7|subscription=yes|title=Coinage of a 50-Cent Piece in Commemoration of the Three Hundredth Anniversary of the Landing of the Pilgrims|date=March 26, 1920}}</ref>

[[File:Mayflower 1920 Issue-1c.jpg|thumb|thumb|right|One-cent U.S. stamp for the tercentenary depicting the ''[[Mayflower]]'']]
The three coinage bills—Maine Centennial, Alabama Centennial, and Pilgrim Tercentenary—were considered in that order by the House of Representatives on April 21, 1920. As the Maine and Alabama pieces were considered, Ohio's [[Warren Gard]] asked a number of questions about various matters, including regarding the wisdom of having so many half dollar designs issued, though he did not object to the passage of either bill.<ref name = "cr1">{{USCongRec|1920|5947–5950|date=April 21, 1920}}</ref> After the Pilgrim bill came to the floor, Gard asked Walsh why the bill provided for 500,000 half dollars, so much more than for the state coins; Walsh responded that it was because the coin was for a 300th anniversary rather than just a 100th.  Gard followed up this point, asking if it had been a 400th anniversary, whether even more coins would be justified, and Walsh agreed. Both Walsh and Vestal were asked by Gard how many half dollars were in circulation, but neither legislator knew. Washington Representative [[John Franklin Miller (representative)|John Franklin Miller]] asked whether the coin was to commemorate the original landing of the Pilgrims at Provincetown or their subsequent landing at Plymouth, but Walsh did not answer the question definitively. Congressman [[James W. Dunbar]] of Indiana noted that the price of silver had risen so high that the Mint might be out of pocket in striking the coins, and asked if the federal government would cover any losses; Vestal responded that Massachusetts would. Vestal then moved the committee amendment to reduce the authorized mintage from 500,000 to 300,000, attracting the attention of Gard, who asked the reason for the change and was told the original bill had a typographical error.<ref name = "cr1" /> Walsh had told the committee that he had selected the figure of 500,000 since the Pilgrim anniversary was more important than the Maine state centennial.{{sfn|House hearings|pp=6–7}} Gard enquired if the amendment had been approved by the committee, and when he was told it had, asked if the Pilgrim half dollars would buy more than [[Walking Liberty half dollar|ordinary half dollars]] to which Walsh replied, "why, I think very likely, especially on Cape Cod."<ref name = "cr2">{{USCongRec|1920|5950|date=April 21, 1920}}</ref> Gard indicated his hope that he could spend a few days there that summer, and Vestal stated that Gard would be welcome to obtain and spend half dollars in any of the three states celebrating anniversaries. The bill passed the House without recorded opposition.<ref name = "cr2" />

The following day, April 22, 1920, the House reported its passage of the bill to the Senate.<ref>{{USCongRec|1920|5966|date=April 22, 1920}}</ref> The bill was referred to the [[Senate Committee on Banking and Currency]]; on April 28, Connecticut's [[George P. McLean]] reported it back with a recommendation it pass.<ref>{{USCongRec|1920|6202|date=April 28, 1920}}</ref> On May 3, McLean asked that the three coin bills (Maine, Alabama and Pilgrim) be considered by the Senate immediately, rather than awaiting their turns, but Utah Senator [[Reed Smoot]] objected: Smoot's attempt to bring up an anti-[[Dumping (pricing policy)|dumping]] trade bill had just been objected to by [[Charles S. Thomas]] of Colorado. Smoot, however, stated if the bills had not been considered by about 2:00 pm, there would probably not be any objection.<ref>{{USCongRec|1920|6443|date=May 3, 1920}}</ref> When McLean tried again to advance the coin bills, Kansas' [[Charles Curtis]] asked if there was any urgency. McLean replied that as the three coin bills were to mark ongoing anniversaries, there was a need to have them authorized and get the production process started. All three bills passed the Senate without opposition<ref>{{USCongRec|1920|6454|date=May 3, 1920}}</ref> and the Pilgrim bill was enacted with the signature of President [[Woodrow Wilson]] on May 12, 1920.<ref name = "profile" />

== Preparation ==
[[File:WilliamBradfordStatue.jpg|thumb|175px|upright|left|''Governor William Bradford'' (1920) by [[Cyrus E. Dallin]], who also designed the coin]]
The Pilgrim Tercentenary Commission made sketches for a design, which were converted to three-dimensional plaster models by [[Cyrus E. Dallin]], a Boston sculptor known for his portrayals of Native Americans{{sfn|Bowers|p=141}} who had also created works related to the Pilgrims.{{sfn|Slabaugh|p=43}} As the legislation was not approved until May 12, 1920, and the commission hoped to have the coins available for sale as early as possible, Dallin was urged to hurry with his work.{{sfn|Bowers|p=142}} The selection of Dallin apparently delighted [[Commission of Fine Arts]] (CFA) chairman [[Charles Moore (Commission of Fine Arts)|Charles Moore]], who wrote to the sculptor in convivial terms.{{sfn|Taxay|pp=48–51}}

Dallin finished his models in August 1920 and the commission referred the designs to sculptor member [[James Earle Fraser (sculptor)|James Earle Fraser]]. On examining Dallin's work, Fraser deemed the lettering crude, but in an undated letter to Moore (probably late August) regretted that due to the tight timeline for production, there was no opportunity to make changes. He suggested that the Mint be urged to allow three months in future for CFA consideration. After the commission met on September 3, a letter to that effect was sent to the [[Director of the United States Mint]], [[Raymond T. Baker]].{{sfn|Taxay|pp=48–51}} The letter was ignored, but the Treasury approved the designs.{{sfn|Flynn|p=152}}

== Design ==

[[File:Springfield Puritan detail.jpg|thumb|Part of [[The Puritan (Springfield, Massachusetts)|''The Puritan'']] by [[Augustus Saint-Gaudens]] ]]
The [[obverse and reverse|obverse]] of the coin features William Bradford. He wears a hat and carries a Bible under his arm.{{sfn|Slabaugh|p=43}} Bradford, noted for piety, is intended to be seen in a moment of meditation.{{sfn|Vermeule|p=161}} Dallin's plaster models had the words "<small>HOLY BIBLE</small>" on the volume; these, together with Dallin's initials "<small>CED</small>", were removed.{{sfn|Swiatek|p=115}} Instead, the initial D was placed under Bradford's elbow, likely impressed upon the [[glossary of numismatics#H|hub]] as an afterthought by a punch normally used to create the [[mint mark]] D for the Denver Mint.{{sfn|Bowers|p=142}} Numismatists Anthony Swiatek and [[Walter Breen]] deemed Bradford's broad collar near enough to Puritan wear of the day to pass, though they questioned the authenticity of the ruffled [[cravat]].{{sfn|Swiatek & Breen|pp=209, 211}} Bradford's portrait is in any case an invention; no genuine likeness of him is known.{{sfn|Bowers|p=144}} The crudeness of the lettering complained of by Fraser is not apparent due to the relatively small size of the coins.{{sfn|Taxay|p=51}}

The reverse depicts the ''Mayflower''{{sfn|Bowers|p=144}} under full sail. Numismatic writers have focused much attention on the fact that the ship bears a  triangular [[flying jib]], a type of sail not used at the time of the ''Mayflower'' voyage.{{sfn|Swiatek|p=115}}{{sfn|Swiatek & Breen|p=211}}{{sfn|Taxay|p=49}} This error was avoided by [[Clair Aubrey Huston]] in his design for the one-cent stamp issued on December 21, 1920, for the anniversary. The inscriptions and dates that encircle the coin are self-explanatory.{{sfn|Swiatek & Breen|pp=211, 213}}

Art historian [[Cornelius Vermeule]], in his volume on U. S. coins and medals, deemed the Pilgrim half dollar "a masterpiece in the conservative tradition".{{sfn|Vermeule|p=160}} He suggested that Dallin's portrait of Bradford was influenced by [[Augustus Saint-Gaudens]] and his sculpture, [[The Puritan (Springfield, Massachusetts)|''The Puritan'']]. Vermeule deemed the ship on the reverse a great advance on [[George T. Morgan]]'s 1892 depiction of the [[Santa María (ship)|''Santa María'']] on the [[Columbian half dollar]], and felt that Dallin's vessel presaged the ships (at least five) on commemorative coins of the 1930s. "Seen from the stern on the waves, the Pilgrims' ship is impressive."{{sfn|Vermeule|pp=161–62}}

== Production, distribution, and collecting ==
[[File:Mayflower compact 1920 U.S. stamp.1.jpg|thumb|Five-cent stamp for the tercentenary depicting the signing of the [[Mayflower Compact]]]]
The [[Philadelphia Mint]] coined 200,112 half dollars in October 1920, with the excess above the round number reserved for inspection and testing at the 1921 meeting of the annual [[Assay Commission]]. They were shipped to the [[FleetBoston Financial|National Shawmut Bank]] of Boston, which sold the coins for $1 each to the public, with the profits to go to the tercentenary commission.{{sfn|Swiatek & Breen|p=211}} The coins could be ordered through any bank in Boston or Plymouth. Swiatek believed the sale of 1920-dated coins to have been very successful, and there was no thought at that time of returning coins to the Mint for redemption and melting. As the first order had not exhausted the authorized mintage, when sales slowed, the tercentenary commission ordered 100,000 more during the spring of 1921. These (together with 53 pieces held for the 1922 Assay Commission) were coined in July, and have the year of issue added, on the obverse to the left of Bradford.{{sfn|Slabaugh|p=43}}{{sfn|Swiatek|p=115}} This was done to comply with the [[Coinage Act of 1873]], which required the year of striking to appear on coins.{{sfn|Swiatek & Breen|p=211}} The [[recession of 1921]] began soon after; sales dropped, and tens of thousands of both dates remained unsold. The tercentenary commission returned 48,000 of the 1920 issue and 80,000 of the 1921 to the Mint.{{sfn|Swiatek|p=115}}

Both dates of the Pilgrim half dollar have appreciated in price over the years, with the rarer 1921, of which only 20,000 are extant, leading the way. At the peak of the first commemorative coin boom in 1936, the 1920 sold for $1.75 and the 1921 for $8; at the peak of the second boom in 1980 the 1920 sold for $275 and the 1921 for $800.{{sfn|Bowers|pp=144–45}} The deluxe edition of [[R. S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'', published in 2015, lists the 1920 at between $85 and $650 and the 1921 at between $170 and $850, each depending on condition. An exceptional specimen of the 1920 sold at auction in 2014 for $7,344.{{sfn|Yeoman|pp=1125–26}}

Coin dealer and numismatic author [[Q. David Bowers]] deemed the Pilgrim coin the first time that a commemorative half dollar had been struck over multiple years for the purpose of making numismatists buy multiple coins to keep their collections complete:

{{quote|Up until this time relatively little attention had been paid to promoting commemorative coins to collectors. The numismatic fraternity accounted for only a small percentage of the total sales&nbsp;... The 1921-dated Pilgrim halves were created to "get" the collector, as [coin dealer] B. Max Mehl put it, to pander to the desire of numismatists to achieve complete sets. The handwriting was on the wall, and in the same year it soon became every collector for himself against the greed of the issuers of the 1921 Alabama and [[Missouri Centennial half dollar|Missouri]] halves. The age of innocence had ended.{{sfn|Bowers|p=146}} 
}}

== References ==
{{Reflist|20em}}

== Sources ==
* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 1992
  | title = Commemorative Coins of the United States: A Complete Encyclopedia
  | publisher = Bowers and Merena Galleries, Inc.
  | location = Wolfeboro, NH
  | isbn = 978-0-943161-35-8
  | ref = {{sfnRef|Bowers}}
  }}
* {{cite book
  | last = Flynn
  | first = Kevin
  | year = 2008
  | title = The Authoritative Reference on Commemorative Coins 1892–1954
  | publisher = Kyle Vick
  | location = Roswell, GA
  | oclc = 711779330
  | ref = {{sfnRef|Flynn}}
  }}
* {{cite book
  | last = Slabaugh
  | first = Arlie R.
  | edition = second
  | year = 1975
  | title = United States Commemorative Coinage
  | publisher = Whitman Publishing
  | location = Racine, WI
  | isbn = 978-0-307-09377-6
  | ref = {{sfnRef|Slabaugh}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-01536-3
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
|author=United States House of Representatives Committee on Coinage, Weights and Measures
|title=Authorizing Coinage of Memorial 50-Cent Piece for the State of Alabama
|date=March 26, 1920
|url=http://congressional.proquest.com/congressional/result/pqpresultpage.gispdfhitspanel.pdflink/$2fapp-bin$2fgis-hearing$2ff$2f3$2fb$2f2$2fhrg-1920-cwe-0003_from_1_to_10.pdf+/13227,+66,+H.R.$40$2fapp-gis$2fhearing$2fhrg-1920-cwe-0003$40Hearing$40Hearings+Published$40March+26,+1920$40?pgId=d3a77df2-fdfe-4a18-bd1f-d33d359f7041&rsId=154395B5B2D
|publisher=[[United States Government Printing Office]]
|ref={{sfnRef|House hearings}}
|subscription=yes
}}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2015
  | title = [[A Guide Book of United States Coins]]
  | edition = 1st Deluxe
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4307-6
  | ref = {{sfnRef|Yeoman}}
  }}

==External links==
* {{Commons category-inline|Pilgrim Tercentenary half dollar}}

{{Coinage (United States)}}
{{Portal bar|Arts|Business and economics|Massachusetts|Numismatics|United States|Visual arts}}

[[Category:1920 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Fifty-cent coins]]
[[Category:United States silver coins]]