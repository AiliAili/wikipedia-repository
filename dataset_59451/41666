{{EngvarB|date=May 2013}}
{{Use dmy dates|date=May 2013}}
{{Infobox law enforcement agency
| agencyname      = 
| nativename      = 
| nativenamea     = 
| nativenamer     = 
| commonname      = 
| abbreviation    = GSU
| fictional       = 
| patch           = 
| patchcaption    = 
| logo            = 
| logocaption     = 
| badge           = 
| badgecaption    = 
| flag            = 
| flagcaption     = 
| imagesize       = 
| motto           = 
| mottotranslated = 
| formedmonthday  = September
| formedyear      = 1953
| preceding1      = Emergency Company or Regular Police Reserve 
<!-- (...up to 6...) -->
| dissolved       = 
| superseding     = 
| employees       = ≈5000 (2007)<ref name="Police"/>
| volunteers      = 
| budget          = 
| legalpersonality = 
| country         = 
| countryabbr     = 
| national        = 
| federal         = 
| international   = <!--NNN or {{collapsible list |title=NNN |[[country1]] . . |[[countryNNN]]}}-->
| divtype         = 
| divname         = 
| divdab          = 
| subdivtype      = 
| subdivname      = 
| subdivdab       = 
| map             = 
| mapcaption      = 
| dmap            = 
| sizearea        = 
| sizepopulation  = 
| legaljuris      = 
| governingbody   = 
| governingbodyscnd = 
| constitution1   = 
<!-- (...up to 6...) -->
| police          = 
| local           = 
| military        = 
| paramilitary    = 
| provost         = 
| gendarmerie     = 
| religious       = 
| speciality1     = 
<!-- (...up to 6...) -->
| secret          = 
| overviewtype    = 
| overviewbody    = 
| headquarters    = [[Nairobi]], [[Kenya]]
| hqlocmap        = 
| hqlocmapborder  = 
| hqlocmapwidth   = 
| hqlocmapheight  = 
| hqlocleft       = 
| hqloctop        = 
| hqlocmappoptitle = 
| sworntype       = 
| sworn           = 
| unsworntype     = 
| unsworn         = 
| multinational   = <!--NNN or {{collapsible list |title=NNN |[[country1]] . . |[[countryNNN]]}}-->
| electeetype     = 
| minister1name   = 
| minister1pfo    = 
<!-- (...up to 6...) -->
| chief1name      = Stephen Toroitich Chelimo
| chief1position  = Commandant 
<!-- (...up to 6...) -->
| parentagency    = [[Kenya Police]]
| child1agency    = 
<!-- (...up to 6...) -->
| unittype        = 
| unitname        = <!--NNN or {{collapsible list |title=NNN |[[Unit1]] . . |[[UnitNNN]]}}-->
| officetype      = 
| officename      = <!--NNN or {{collapsible list |title=NNN |[[Office1]] . . |[[OfficeNNN]]}}-->
| provideragency  = 
| uniformedas     = 
| stationtype     = 
| stations        = <!--NNN or {{collapsible list |title=NNN |[[Station1]] . . |[[StationNNN]]}}-->
| airbases        = <!--NNN or {{collapsible list |title=NNN |[[Airbase1]] . . |[[AirbaseNNN]]}}-->
| lockuptype      = 
| lockups         = <!--NNN or {{collapsible list |title=NNN |[[Lockup1]] . . |[[LockupNNN]]}}-->
| vehicle1type    = 
| vehicles1       = <!--NNN or {{collapsible list |title=NNN |[[Vehicle1]] . . |[[VehicleNNN]]}}-->
<!-- (...up to 3...) -->
| boat1type       = 
| boats1          = <!--NNN or {{collapsible list |title=NNN |[[Boat1]] . . |[[BoatNNN]]}}-->
<!-- (...up to 3...) -->
| aircraft1type   = 
| aircraft1       = <!--NNN or {{collapsible list |title=NNN |[[Aircraft1]] . . |[[AircraftNNN]]}}-->
<!-- (...up to 3...) -->
| animal1type     = 
| animals1        = <!--NNN or {{collapsible list |title=NNN |[[Animal1]] . . |[[AnimalNNN]]}}-->
<!-- (...up to 3...) -->
| person1name     = 
| person1reason   = 
| person1type     = 
<!-- (...up to 6...) -->
| programme1      = 
<!-- (...up to 6...) -->
| activity1name   = 
<!-- (...up to 6...) -->
| activitytype    = 
| anniversary1    = 
<!-- (...up to 6...) -->
| award1          = 
<!-- (...up to 6...) -->
| website         = 
| footnotes       = 
| reference       = 
}}
<!-- Image with inadequate rationale removed: [[File:LogoKenpol.png|280px|thumb|right|The Kenyan Police Emblem]] -->
The '''General Service Unit''' ('''GSU''') is a [[paramilitary]] wing in the [[National Police Service of Kenya]], consisting of highly trained police officers, transported by seven dedicated [[Cessna]]s and three [[Bell Helicopter Textron|Bell helicopters]]<ref>Kenya Security [http://www.iss.co.za/Af/profiles/Kenya/SecInfo.html Information] –  [http://www.iss.co.za/ Institute for Security Studies] retrieved on 28 May 2007</ref> Having been in existence since the late 1940s, the GSU has fought in a number of conflicts in and around Kenya, including the 1963 – 1969 [[Shifta War]] and the [[1982 Kenyan coup]].<ref>Sabar p. 181</ref> The Kenyan police outlines the objectives of the GSU as follows: to deal with situations affecting internal security throughout the Republic, to be an operational force that is not intended for use on duties of a permanent static nature, and primarily, to be a reserve force to deal with special operations and civil disorders.<ref name="Police"/>

==History==

Initially created as the Emergency Company or Regular Police Reserve in 1948, the GSU began as a unit of 50 men armed with Bren guns carriers and armoured cars<ref name="Police">Kenyan Police [http://www.kenyapolice.go.ke/general%20service%20unit.asp page] retrieved on 24 May 2007</ref> and was involved in a number of uprisings including the [[Mau Mau Uprising]] before being renamed the General Service Unit in September 1953.<ref name="Police"/> The newly designated GSU consisted of 47 European officers and 1058 Africans divided into 5 regional companies each consisting of a number of 39-man platoons.<ref name="Police"/> In 1957 further reorganisations took place and the GSU was brought under one commander, a Mr. S. G. Thomson. In 1961 the unit left Kenya for the first time to deal with civil unrest in [[Zanzibar]], and then from 1963 until 1969 the GSU fought the secessionists during the [[Shifta War]].

During the 1990s the GSU worked in central Kenya to quell socialist political unrest and demonstrations against the Kenyan government, such as the [[Saba Saba Day]] (7 July) celebrations of 1990, where 30 people were killed as the police and General Service Units took action.<ref>Barkan p. 59</ref> More recently, in July 2005, troops of the GSU were sent to northern Kenya to seek out those responsible for the deaths of 76 people, 22 of them children, at a school in the area.<ref name="news1">BBC News article [http://news.bbc.co.uk/2/hi/africa/4685461.stm ''Crack troops seek Kenya killers'']</ref> The GSU helped prevent further friction between feuding [[Gabra people|Gabra]] and [[Borana Oromo|Borana]] communities when they were transported to the region by two police and two military helicopters, as well as two ministers from the Kenyan government.<ref name="news1"/> Currently, the GSU has around 5,000 paramilitary troops, of which 2000 are the [[Israel]]i trained and battle hardened 'Recce group'. It is regulated under chapter nine of the Kenya Police Force Standing Orders.<ref name="Police"/>

==Personnel==

Originally, GSU members were drawn from the existing ranks of the Kenyan Police force, were from a number of ethnic backgrounds, and were trained in the Kenya Police College and placed on a two-year tour of duty.<ref name="Police"/> However, with the increase of Africanisation in 1963, the majority of members were by 1967 from native Kenyan tribes such as the [[Luo (family of ethnic groups)|Luo]] or the [[Kikuyu people|Kikuyu]]<ref name="Provizer p. 343">Provizer p. 343</ref> who total 5.9 million, equal to about 13% and 7.4 million, equal to about 22% of [[Demographics of Kenya|Kenya's total population]], respectively<ref>CIA World Factbook [https://www.cia.gov/library/publications/the-world-factbook/geos/ke.html#People figures] retrieved on 28 May 2007</ref> Most recently, all GSU members have been trained at the GSU-specific Training School in [[Embakasi]] and its Field Training Camp in [[Magadi]], on 10-month-long courses, with further 5-month long courses required for promotion.<ref name="Police"/> As with various branches of the Kenyan armed forces,<ref name="Provizer p. 343"/> the GSU also sends its officers to Great Britain to be trained in such facilities as the [[Britannia Royal Naval College]] and [[Royal Military Academy Sandhurst|Sandhurst]].<ref>Provizer p. 344</ref>

==Organisational Structure==

The GSU is organised in companies each under a company commander. Four of these companies have commanding officers and they are regarded as the 'big four'
These are 
* The GSU Training School at Embakasi
* The Recce Company based at Ruiru
* The Headquarters Company based at Ruaraka Nairobi, and
* The (Presidential) Guard Company 'G' coy whose officers are solely charged with the duties of protecting the various state houses and lodges.

The other companies are listed in alphabetical order starting from 'A' company to 'Q' company. There is no 'o' company.
Each company consists of three platoons and the company headquarters personnel. Originally a platoon consisted of 30 personnel but nowadays a platoon can have as many as 60 members.

=== Commanders. ===

* Mr. S. G. Thompson	– 19 May 1961 – 17 July 1961
* Mr. S. G. Smith	– 27 July 1961 – 20 March 1963
* Mr. Mackenzie	– 21 March 1963 – 26 February 1964
* Mr. R. J. Angel	– 27 February 1964 – 17 April 1967
* Mr. B. M. Gethi	– 18 April 1967 – 1 September 1978
* Mr. P. Mbuthia	– 2 September 1978 – 24 August 1982
* Mr. E. K. Mbijiwe – 25 August 1982 – 8 April 1987
* Mr. J. K. A. Kosgei – 8 April 1987 – 9 March 1993
* Mr. C. C. Kimurgor – 10 March 1993 – 29 June 1999
* Mr. S. K. Cheramboss – 29 June 1999 – 30 August 2002
* Mr. Lawrence Mwadime – 16 January 2003 – 2 June 2005
* Mr mathew iteere – 2 June 2005 – 8 September 2009
* Mr. William Saiya Aswenje – from 23 September 2009 - 2014
* Mr joel kitili mboya 2014-2015
*

==Notes==

{{reflist}}

==References==

'''Printed Sources:'''

* Barkan, Joel D. ''Beyond Capitalism Vs. Socialism in Kenya and Tanzania'', 1994 ISBN 1-55587-530-0
* Provizer, Norman W. ''Analyzing the Third World: Essays from "Comparative Politics"'', 1978 ISBN 0-87073-943-3
* [[Galia Sabar|Sabar, Galia]] ''Church, State and Society in Kenya: from mediation to opposition, 1963–1993'', 2002 ISBN 0-7146-5077-3

'''Websites:'''

* Kenya Security [http://www.iss.co.za/Af/profiles/Kenya/SecInfo.html Information] –  [http://www.iss.co.za/ Institute for Security Studies] retrieved on 28 May 2007
* BBC News article [http://news.bbc.co.uk/2/hi/africa/4685461.stm ''Crack troops seek Kenya killers'']
* Kenyan Police [http://www.kenyapolice.go.ke/general%20service%20unit.asp page] retrieved on 24 May 2007

{{Military of Kenya}}

[[Category:Military of Kenya]]
[[Category:Law enforcement in Kenya]]
[[Category:Government agencies of Kenya]]