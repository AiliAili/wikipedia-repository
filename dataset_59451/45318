{{Use British English|date=June 2015}}
{{Use dmy dates|date=April 2012}}
{{Infobox person
| name           = Reginald Denny
| image          = Reginald Denny in Stars of the Photoplay, 1924.jpg
| caption        = Reginald Denny in ''Stars of the Photoplay,'' 1924
| birth_date     = {{Birth date|df=yes|1891|11|20|}}
| birth_place    = [[Richmond, London|Richmond]], [[London]], [[Surrey]], [[England]], United Kingdom
| death_date     = {{Death date and age|df=yes|1967|06|16|1891|11|20|}}
| death_place    = Richmond, London, Surrey, England, United Kingdom
| occupation     = Actor, aviator
| yearsactive    = 1915&ndash;1966
| spouse         = Betsy Lee (1928–1967); 3 children<br>Irene Haisman (1913–1928; divorced); 1 child}} 

'''Reginald Leigh Denny''' (20 November 1891 – 16 June 1967) was an [[England|English]] [[stage (theatre)|stage]], [[film]] and [[television]] [[actor]] as well as an aviator and [[History of unmanned aerial vehicles#World War II|UAV pioneer]]. He was once an amateur [[boxing]] champion of [[UK|Great Britain]].

==Acting career==
Born in [[Richmond, London]], [[Surrey]], England, Denny (sources differ on his birth name giving variously '''Reginald Daymore''',<ref>{{Cite book|page=128|title=The Illustrated Directory of Film Stars|author=David Quinlan|publisher=Hippocrene Books|date=1981|isbn=978-0-882-54654-4}}</ref> '''Reginald Leigh Daymore'''<ref>"Halliwell's Filmgoer's companion, 1980 cited by {{Cite web|title=Library of Congress|url=http://id.loc.gov/authorities/names/n85151526.html}}</ref> and '''Reginald Leigh Dugmore Denny'''<ref>{{Cite book|page=448|title=The Parade's Gone by ...|author=Kevin Brownlow|publisher=University of California Press|date=1968|isbn=978-0-52-003068-8}}</ref>), but listed in birth records as Richard Leigh Dugmore, began his stage career at age seven in ''The Royal Family'' and in ''[[The Merry Widow]]'' at age 16, the year he left St. Francis Xavier College, [[Mayfield, East Sussex|Mayfield]], [[Sussex]].  Years later he joined an opera company as a baritone, and toured India. After continuing his stage career in America, his film career started in 1915 with the old [[World Film Company]] and he made films both in the United States and Britain until the 1960s.  He came from a theatrical family which went to the United States in 1912 to appear in the stage production ''Quaker Girl''.  His father was the actor and singer [[W.H. Denny]]. Reginald appeared in [[John Barrymore]]'s 1920 Broadway production of [[Richard III (play)|Richard III]]; the two actors became friends. {{Citation needed|date=August 2013}}
[[File:Reginald Denny & Daughter - Dec 1922 UW.jpg|thumb|right|Denny and his daughter in 1922]]
Denny was a well-known actor in silent films and with the advent of [[talkies]], he became a character actor. He played the lead role in a number of his earlier films, generally as a comedic Englishman in such works as ''[[Private Lives (film)|Private Lives]]'' and later had reasonably steady work as a supporting actor in dozens of films, including ''[[The Little Minister (1934 film)|The Little Minister]]'' (1934) with [[Katharine Hepburn]], ''[[Anna Karenina]]'' (1935) with [[Greta Garbo]], [[Alfred Hitchcock]]'s ''[[Rebecca (1940 film)|Rebecca]]'' (1940) and the [[Frank Sinatra]] crime caper film ''[[Assault on a Queen]]'' (1966). His last role was in ''[[Batman (1966 film)|Batman]]'' (1966) as Commodore Schmidlapp. He made frequent appearances in television during the 1950s and 1960s.

==Aviation career==
[[File:Cropped RDenny.jpg|thumb|right|Reginald Denny 1917]]
He served as an observer/gunner in [[World War I]] in the [[Royal Flying Corps]],<ref>[http://www.ctie.monash.edu/hargrave/denny_black_cat.html Black Cats]</ref> and in the 1920s he performed as a stunt pilot. In the early 1930s, Denny became interested in [[radio]] controlled [[Scale model|model]] [[airplane|aeroplanes]]. He and his business partners formed [http://www.vectorsite.net/twuav_01.html#m1 Reginald Denny Industries] and opened a model plane shop in 1934 known as Reginald Denny Hobby Shops.

He bought a plane design from Walter Righter in 1938 and began marketing it as the "Dennyplane", and a model engine called the "Dennymite".<ref>[http://www.ctie.monash.edu.au/hargrave/dennyplane.html Denny plane]</ref> In 1940, Denny and his partners won a [[United States Army Air Corps|US Army]] contract for their radio-controlled target drone, the [[OQ-2 Radioplane]]. They manufactured nearly fifteen thousand drones for the [[US Army]] during the [[Second World War]]. The company was purchased by [[Northrop Corporation|Northrop]] in 1952.<ref>[http://www.modelaircraft.org/museum/bio/Denny.pdf#search='reginald%20denny' Reginald Denny profile at modelaircraft.org (PDF)] {{webarchive |url=https://web.archive.org/web/20051106114026/http://www.modelaircraft.org/museum/bio/Denny.pdf#search='reginald%20denny' |date=6 November 2005 }}</ref><ref>Parker, Dana T. ''Building Victory: Aircraft Manufacturing in the Los Angeles Area in World War II,'' pp. 129-30, Cypress, California, 2013.</ref>

[[Marilyn Monroe]] was discovered working as an assembler at Radioplane. A photographer assigned by Denny's friend, Army publicist (and future [[US President]]) Captain [[Ronald Reagan]], took several shots and persuaded her to work as a model, which was the beginning of her career.<ref>''Smart Weapons:Top Secret History of Remote-controlled Airborne Weapons'', by Hugh McDaid and David Oliver, 1997, Barnes & Noble Books<!-- isbn needed --></ref><ref>Parker, Dana T. ''Building Victory: Aircraft Manufacturing in the Los Angeles Area in World War II,'' p. 130, Cypress, CA, 2013.</ref>

==Death==
Reginald Denny died on 16 June 1967 in his 75th year after suffering a stroke whilst in England. His body was buried at [[Forest Lawn Memorial Park (Hollywood Hills)|Forest Lawn-Hollywood Hills Cemetery]] in [[Los Angeles]], [[California]].<ref>[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=5153&pt= Reginald Denny profile at Find a grave]</ref> He was survived by his widow, Betsy who died in 1996 aged 89 and their three children. Denny was preceded in death by his daughter (from his first marriage to Irene Haisman [m. 1913–1928], which ended in divorce), Barbara Denny-Simmons who died in 1948, at age 32.

==Partial filmography==
{{div col|cols=2}}
'''Silent'''
* ''Niobe'' (1915) as Cornelius Griffin
* ''[[The Melting Pot (1915 film)|The Melting Pot]]'' (1915) as Undetermined Role (uncredited)
* ''[[The Red Lantern]]'' (1919)
* ''Bringing Up Betty'' (1919) as Tom Waring
* ''[[The Oakdale Affair]]'' (1919) as Arthur Stockbridge
* ''[[A Dark Lantern]]'' (1920) as Prince Anton
* ''[[39 East]]'' (1920) as Napolean Gibbs Jr.
* ''[[Paying the Piper (film)|Paying the Piper]]'' (1921) as Keith Larne
* ''[[The Price of Possession]]'' (1921) as Robert Dawnay
* ''[[Experience (1921 film)|Experience]]'' (1921)
* ''[[Disraeli (1921 film)|Disraeli]]'' (1921) as Charles, Viscount Deeford
* ''[[Footlights (1921 film)|Footlights]]'' (1921) as Brett Page
* ''The Beggar Maid'' (1921) (* short) as The Earl of Winston / King Cophetua
* ''Tropical Love'' (1921) as The Drifter
* ''[[The Iron Trail]]'' (1921) as Dan Appleton
* ''Let's Go'' (1922) (* short) as Kane Halliday / 'Kid' Roberts
* ''Round Two'' (1922) (* short) as Kane 'Kid Roberts' Halliday
* ''[[Sherlock Holmes (1922 film)|Sherlock Holmes]]'' (1922) as Prince Alexis
* ''Payment Through the Nose'' (1922) (* short) as Kane Halliday / Kid Roberts
* ''[[The Leather Pushers]]'' (1922) as Kane Halliday / Kid Roberts
* ''A Fool and His Money'' (1922) (* short) as Kane Halliday / Kid Roberts
* ''The Taming of the Shrewd'' (1922) (* short) as Kane Halliday / Kid Roberts
* ''Whipsawed'' (1922) (* short) as Kane Halliday / Kid Roberts
* ''Never Let Go'' (1922) (* short) as Campbell - the Mountie
* ''The Jaws of Steel'' (1922) (* short) as Cpl. Haldene, N.W.M.P.
* ''Plain Grit'' (1922) (* short) 
* ''[[The Kentucky Derby (1922 film)|The Kentucky Derby]]'' (1922) as Donald Gordon
* ''Young King Cole'' (1922) (* short) as Kane Halliday / Kid Roberts
* ''He Raised Kane'' (1922) as Kane Halliday / Kid Roberts
* ''The Chickasha Bone Crusher'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''When Kane Met Abel'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Strike Father, Strike Son'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Joan of Newark'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Abysmal Brute'' (1923) as Pat Glendon, Jr
* ''The Wandering Two'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''The Widower's Mite'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Don Coyote'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Something for Nothing'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Columbia, the Gem, and the Ocean'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''Barnaby's Grudge'' (1923) (* short) as Kane Halliday / Kid Roberts
* ''The Thrill Chaser'' (1924) as Cameo appearance
* ''[[Sporting Youth]]'' (1924) as Jimmy Wood
* ''[[The Reckless Age]]'' (1924) as Dick Minot
* ''The Fast Worker'' (1924) as Terry Brock
* ''[[Oh Doctor! (1925 film)|Oh Doctor!]]'' (1925) as Rufus Billings Jr.
* ''I'll Show You the Town'' (1925) as Alec Dupree
* ''[[Where Was I? (film)|Where Was I?]]'' (1925) as Thomas S. Berford
* ''[[California Straight Ahead (1925 film)|California Straight Ahead]]'' (1925) as Tom Hayden
* ''[[What Happened to Jones]]'' (1926) as Tom Jones
* ''[[Skinner's Dress Suit]]'' (1926) as Skinner
* ''[[Rolling Home (1926 film)|Rolling Home]]'' (1926) as Nat Alden
* ''[[Take It from Me (1926 film)|Take It from Me]]'' (1926) as Tom Eggett
* ''[[The Cheerful Fraud]]'' (1926) as Sir Michael Fairlie
* ''[[Fast and Furious (1927 film)|Fast and Furious]]'' (1927) as Tom Brown
* ''[[Out All Night (1927 film)|Out All Night]]'' (1927) as John Graham
* ''[[On Your Toes (1927 film)|On Your Toes]]'' (1927) as Elliott Beresford
* ''[[That's My Daddy]]'' (1927) as James 'Jimmy' Norton
* ''[[Good Morning, Judge]]'' (1928) as Freddie Grey
* ''[[The Night Bird]]'' (1928) as Kid Davis (his last silent film)

'''Sound'''
* ''Red Hot Speed'' (1928) as Darrow
* ''Clear the Decks'' (1929) as Jack Armitage
* ''His Lucky Day'' (1929) as Charles Blaydon
* ''[[One Hysterical Night]]'' (1929) as William 'Napoleon' Judd
* ''[[Embarrassing Moments]]'' (1930) as Thaddeus Cruikshank
* ''[[What a Man! (1930 film)|What a Man!]]'' (1930) as Wade Rawlins
* ''[[Madam Satan]]'' (1930) as Bob Brooks
* ''[[Those Three French Girls]]'' (1930) as Larry
* ''[[A Lady's Morals]]'' (1930) as Paul Brandt
* ''[[Oh, for a Man!]]'' (1930) as Barney McGann
* ''[[Parlor, Bedroom and Bath]]'' (1931) as Jeffrey Haywood
* ''[[Kiki (1931 film)|Kiki]]'' (1931) as Victor Randall
* ''[[Stepping Out (1931 film)|Stepping Out]]'' (1931) as Tom Martin
* ''[[Private Lives (film)|Private Lives]]'' (1931) as Victor
* ''[[Strange Justice (1932 film)|Strange Justice]]'' (1932) as Judson
* ''The Iron Master'' (1933) as Steve Mason
* ''[[The Barbarian (1933 film)|The Barbarian]]'' (1933) as Gerald Hume - Diana's Fiancee
* ''[[The Big Bluff (1933 film)|The Big Bluff]]'' (1933)
* ''[[Only Yesterday (1933 film)|Only Yesterday]]'' (1933) as Bob
* ''[[Fog (1933 film)|Fog]]'' (1933) as Dr. Winstay
* ''[[The Lost Patrol (1934 film)|The Lost Patrol]]'' (1934) as Brown
* ''Dancing Man'' (1934) as Paul Drexel
* ''[[The World Moves On]]'' (1934) as Griffiths
* ''[[Of Human Bondage (1934 film)|Of Human Bondage]]'' (1934) as Griffiths
* ''[[We're Rich Again]]'' (1934) as Bookington 'Bookie' Wells
* ''[[One More River]]'' (1934) as David Dornford
* ''[[The Richest Girl in the World (1934 film)|The Richest Girl in the World]]'' (1934) as Phillip Lockwood
* ''[[The Little Minister (1934 film)|The Little Minister]]'' (1934) as Captain Halliwell
* ''[[Lottery Lover]]'' (1935) as Capt. Payne
* ''Without Children'' (1935) as Phil Graham
* ''[[Vagabond Lady]]'' (1935) as John 'Johnny' Spear
* ''[[No More Ladies]]'' (1935) as Oliver
* ''Here's to Romance'' (1935) as Emery Gerard
* ''[[Anna Karenina (1935 film)|Anna Karenina]]'' (1935) as Yashvin
* ''[[The Lady in Scarlet (1935 film)|The Lady in Scarlet]]'' (1935) as Oliver Keith
* ''[[Remember Last Night?]]'' (1935) as Jake Whitridge
* ''[[Midnight Phantom]]'' (1935) as Prof. David Graham
* ''[[The Preview Murder Mystery]]'' (1936) as Johnny Morgan
* ''[[It Couldn't Have Happened - But It Did]]'' (1936) as Greg Stone
* ''[[Romeo and Juliet (1936 film)|Romeo and Juliet]]'' (1936) as Benvolio - Nephew to Montgue and Friend to Romeo
* ''[[Two in a Crowd]]'' (1936) as James Stewart Anthony
* ''[[More Than a Secretary]]'' (1936) as Bill Houston
* ''[[We're in the Legion Now!]]'' (1936) as Dan Linton
* ''[[Bulldog Drummond Escapes]]'' (1937) as Algy Langworth
* ''[[Join the Marines]]'' (1937) as Steve Lodge
* ''[[Women of Glamour]]'' (1937) as Fritz 'Frederick' Eagan
* ''[[Let's Get Married (1937 film)|Let's Get Married]]'' (1937) as George Willoughby
* ''[[The Great Gambini]]'' (1937) as William Randall
* ''[[Jungle Menace]]'' (1937 serial) as Ralph Marshall [Chs.1-3]
* ''[[Bulldog Drummond Comes Back]]'' (1937) as Algy Longworth
* ''Beg, Borrow or Steal'' (1937) as Clifton Summitt
* ''[[Bulldog Drummond's Revenge]]'' (1937) as Algy Longworth
* ''[[Bulldog Drummond's Peril]]'' (1938) as Algy Longworth
* ''[[Four Men and a Prayer]]'' (1938) as Capt. Douglas Loveland
* ''[[Blockade (1938 film)|Blockade]]'' (1938) as Edward Grant
* ''[[Bulldog Drummond in Africa]]'' (1938) as Algy Longworth
* ''[[Arrest Bulldog Drummond]]'' (1938) as Algy Longworth
* ''[[Bulldog Drummond's Secret Police]]'' (1939) as Algy Longworth
* ''[[Everybody's Baby]]'' (1939) as Dr. Pilcoff
* ''[[Bulldog Drummond's Bride]]'' (1939) as Algy Longworth
* ''[[Rebecca (1940 film)|Rebecca]]'' (1940) as Frank Crawley
* ''[[Spring Parade]]'' (1940) as The Major
* ''[[Seven Sinners (1940 film)|Seven Sinners]]'' (1940) as Captain Church
* ''[[One Night in Lisbon]]'' (1941) as Erich Strasser
* ''[[International Squadron (film)|International Squadron]]'' (1941) as Wing Commander Severn
* ''[[Appointment for Love]]'' (1941) as Michael Dailey
* ''[[Captains of the Clouds]]'' (1942) as Commanding Officer
* ''[[Sherlock Holmes and the Voice of Terror]]'' (1942) as Sir Evan Barham
* ''[[Eyes in the Night]]'' (1942) as Stephen Lawry
* ''[[Thunder Birds (1942 film)|Thunder Birds]]'' (1942) as Barrett
* ''[[Over My Dead Body (1942 film)|Over My Dead Body]]'' (1942) as Richard 'Dick' Brenner
* ''[[The Crime Doctor's Strangest Case]]'' (1943) as Paul Ashley
* ''[[Song of the Open Road]]'' (1944) as Director Curtis
* ''[[Love Letters (1945 film)|Love Letters]]'' (1945) as Defense Counsel Phillips
* ''[[Tangier (1946 film)|Tangier]]'' (1946) as Fernandez
* ''[[The Locket]]'' (1946) as Mr. Wendell
* ''[[My Favorite Brunette]]'' (1947) as James Collins
* ''[[The Macomber Affair]]'' (1947) as Police Inspector
* ''[[The Secret Life of Walter Mitty (1947 film)|The Secret Life of Walter Mitty]]'' (1947) as Colonel
* ''[[Christmas Eve (1947 film)|Christmas Eve]]'' (1947) as Phillip Hastings
* ''[[Escape Me Never (1947 film)|Escape Me Never]]'' (1947) as Mr. MacLean
* ''[[Mr. Blandings Builds His Dream House]]'' (1948) as Simms
* ''[[The Iroquois Trail]]'' (1950) as Capt. Edward Brownell
* ''[[Fort Vengeance]]'' (1953) as Inspector Trevett
* ''[[Abbott and Costello Meet Dr. Jekyll and Mr. Hyde]]'' (1953) as Inspector
* ''[[World for Ransom]]'' (1954) as Maj. Ian Bone
* ''[[Sabaka]]'' (1954) as Sir Cedric
* ''[[Escape to Burma]]'' (1955) as Commissioner
* ''[[The Donald O'Connor Show]]'' (NBC) (1955) as Himself 
* ''[[Around the World in 80 Days (1956 film)|Around the World in 80 Days]]'' (1956) as Bombay Police Inspector
* ''[[Cat Ballou]]'' (1965) as Sir Harry Percival
* ''[[Batman]]'' Series TV (1966, episodes 11 and 12) as King Boris
* ''[[Assault on a Queen]]'' (1966) as Master-at-Arms
* ''[[Batman (1966 film)|Batman]]'' (1966) as Commodore Schmidlapp (final film role)
{{div col end}}

==References==
{{reflist}}

==External links==
{{Portal|Biography}}
{{Commons category|Reginald Denny (actor)}}
*{{IMDb name|0219666|Reginald Denny}}
*{{IBDB name}}
*[http://film.virtual-history.com/person.php?personid=938 Photographs and literature]
*{{Find a Grave|5153|Reginald Denny}}
*[https://web.archive.org/web/20130114021405/http://www.vectorsite.net/twuav_01.html Archive.org cache of UAV history site showing the Radioplane]
{{Authority control}}

{{DEFAULTSORT:Denny, Reginald}}
[[Category:1891 births]]
[[Category:1967 deaths]]
[[Category:Male actors from London]]
[[Category:Burials at Forest Lawn Memorial Park (Hollywood Hills)]]
[[Category:British Army personnel of World War I]]
[[Category:English aviators]]
[[Category:English male boxers]]
[[Category:English businesspeople]]
[[Category:English male film actors]]
[[Category:English male silent film actors]]
[[Category:English male stage actors]]
[[Category:English male television actors]]
[[Category:Disease-related deaths in England]]
[[Category:Male actors from Surrey]]
[[Category:Royal Air Force officers]]
[[Category:20th-century English male actors]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]