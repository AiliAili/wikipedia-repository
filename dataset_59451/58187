{{Infobox journal
| title = Journal of Hymenoptera Research
| formernames = 
| cover = 
| editor = Stefan Schmidt
| discipline = [[Systematics]]
| abbreviation = J. Hymenopt. Res.
| publisher = [[Pensoft Publishers]] on behalf of the [[International Society of Hymenopterists]]
| country =
| frequency = Upon acceptance
| history = 1992-present
| openaccess = Yes
| license = [[Creative Commons by Attribution 4.0]]
| impact = 0.966
| impact-year = 2013
| website = http://www.pensoft.net/journals/jhr/
| link1 = http://www.pensoft.net/journals/jhr/issue/current/
| link1-name = Online access
| link2 = http://www.pensoft.net/journals/jhr/archive
| link2-name = Online archive
| JSTOR =
| OCLC = 28308627
| LCCN = 98660130
| CODEN = JHYREJ
| ISSN = 1070-9428
| eISSN = 1314-2607
}}
The '''''Journal of Hymenoptera Research''''' is a [[peer-reviewed]] [[scientific journal]] covering systematics, taxonomy, and ecology of [[Hymenoptera]]. It was established in 1992, and transferred to publishing with [[Pensoft Publishers]] in 2011, under an [[open access]] system.<ref>{{Cite journal |doi=10.3897/jhr.30.4733 |title=The move to open access and growth: Experience from Journal of Hymenoptera Research |journal=Journal of Hymenoptera Research |volume=30 |pages=1-6 |year=2013 |last1=Schmidt |first1=Stefan |last2=Broad |first2=Gavin |last3=Stoev |first3=Pavel |last4=Mietchen |first4=Daniel |last5=Penev |first5=Lyubomir}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.966.<ref name=WoS>{{cite book |year=2014 |chapter=Journal of Hymenoptera Research |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
{{Commonscat|Media from Journal of Hymenoptera Research}}
* {{Official website|http://www.pensoft.net/journals/jhr}}
* [http://www.hymenopterists.org/journal.php Journal page] at society website
* [http://www.biodiversitylibrary.org/bibliography/2680 ''Journal of Hymenoptera Research''] at the [[Biodiversity Heritage Library]]

[[Category:Entomology journals and magazines]]
[[Category:Publications established in 1992]]
[[Category:English-language journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Pensoft Publishers academic journals]]
[[Category:Academic journals associated with learned and professional societies]]