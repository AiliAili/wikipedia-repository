{{Good article}}
{{Infobox Mayor
| image =Joseph Allen Portrait.png
| honorific-prefix    =
| name                =Joseph H. Allen
| honorific-suffix    =
| imagesize           =
| small| caption      =
| order               =
| office              =21st [[Town supervisor|Supervisor]] of [[Brunswick, New York|Brunswick]], [[New York (state)|New York]]
| term_start          =1856
| term_end            =1857
| predecessor         =William Lape
| successor           =Alanson Cook
| constituency        =
| majority            =
| birth_date          ={{Birth date|1821|09|05}}
| birth_place         =[[Alburg, Vermont|Alburg]], [[Vermont]]
| death_date          ={{Death date and age|1884|04|24|1821|09|05}}
| death_place         =[[Eagle Mills, New York|Eagle Mills]], [[New York (state)|New York]]
| nationality         =[[United States|American]]
| spouse              =Sarah H. Payne
| party               =[[Whig Party (United States)|Whig]], [[Democratic Party (United States)|Democrat]]
| relations           =
| children            =Sarah and Cornelia May
| residence           =
| alma_mater          =
| occupation          =Infantryman, businessman, industrialist, politician
| profession          =
| religion            =
| signature           =Joseph Allen Signature.svg
| allegiance = [[United States]]
| branch = [[United States Army]]<br/>[[Union Army]]
| rank = [[File:Union Army LTC rank insignia.png|35px]] [[Lieutenant colonel (United States)|Lieutenant colonel]]
| serviceyears = 1862–1865
| commands = [[169th New York Volunteer Infantry]]
| battles =[[American Civil War|Civil War]]
*[[Battle of Cold Harbor]]
*[[First Battle of Fort Fisher]]
| footnotes           =
}}
'''Joseph H. Allen''' (September 5, 1821 – April 24, 1884) was an industrial businessman, an officer in the [[American Civil War|Civil War]], and a [[town supervisor]]{{Ref_label|a|a|none}} of [[Brunswick, New York|Brunswick]], [[New York (state)|New York]]. Allen was born in [[Alburg, Vermont|Alburg]], [[Vermont]], to parents of [[British people|British descent]] and left home at an early age. After several business ventures, he became successful in the [[auger (drill)|auger]] and [[garden hoe|hoe]] business, selling mainly to the [[American South]]. He successfully ran for supervisor of the Town of Brunswick in 1856 and [[justice of the peace]] in 1861. At the beginning of the Civil War, his sales plummeted so he closed his business and enlisted in the [[Union Army]]. Allen was wounded multiple times during his service and ended his career at the rank of [[Lieutenant colonel (United States)|lieutenant colonel]]. He returned home to reopen his factory, which was instrumental to the [[Industrial Revolution|industrial success]] of the [[Hamlet (New York)|hamlet]] of [[Eagle Mills, New York|Eagle Mills]] in the mid to late 19th century. He died in 1884 at the age of 62.

==Early life==
Allen was the son of John and Sarah Allen. His father was a native of [[Connecticut]] and of [[English people|English]] origin, and his mother was originally from [[Vermont]] and of [[Scottish people|Scottish]] descent. He left home when he was eight years old "to carve out a fortune for himself", as Sylvester put it in his ''History of Rensselaer County, New York''. He began work in an [[auger (drill)|auger]] factory in [[Hamden, Connecticut|Hamden]], Connecticut, at an early age and continued until he was seventeen years old. At that time, he purchased a clock store, which he traded for horses in Vermont and [[Canada]]. He shipped his horses to the [[West Indies]], but the ship was wrecked and his horses were lost, thus placing him badly in debt. However, he soon obtained credit, and again shipped a number of horses to the West Indies, this time successfully, enabling him to pay all his debts and even earning him a profit.<ref name=sylvester544>Sylvester (1880), p. 544</ref>

==Before the war==
Allen soon resumed his place in the auger factory in Connecticut, where he remained until September 1, 1843, when he moved to [[Troy, New York|Troy]], New York. There he entered into partnership with O. W. Edson in manufacturing augers. The partnership continued until the following January when Allen purchased his partner’s share and continued in this business until the factory burned down in 1850.<ref name=sylvester544/>
[[File:Millville 1876.png|thumb|left|350px|Allen's factory can be seen west of the [[Poesten Kill]] and north of the bridge in this 1876 map. His residence is also in the northwest corner.]]
In January 1851, Allen purchased the Eagle flour mills, an historically important industrial building in the area, which eventually gave the hamlet of [[Eagle Mills, New York|Eagle Mills]]{{Ref_label|b|b|none}} its name.<ref name=sylvester544/> The factory had a long history of failed business attempts. It was built in 1821 by Daniel Sheldon to mill flour using wheat supplied from Troy. After brief success, the business closed and the building sat idle. It was sold to James Bumstead who reopened the building as a [[Cattle feeding|feed]] mill. He too had troubles and sold the building to James McChesney, who kept the building idle for some time before transferring the property to Catlin and Saxton, who began the manufacture of augers and [[Tool bit|bits]]. Also unsuccessful, Catlin and Saxton abandoned the business and James McChesney (whose name was still on the deed) sold the property to Groome and Shattuck, who began manufacturing [[monkey wrench]]es. This too failed and the property was sold to Paul Smith, a miller from nearby [[Cropseyville, New York|Cropseyville]], who sold the property to Allen in 1851 because Smith later decided that he preferred to keep his mills consolidated in Cropseyville.<ref name=anderson524525>[https://archive.org/stream/landmarksofrenss00ande#page/524/mode/2up Anderson] (1896), pp. 524–525</ref>

The factory was located on the rocky banks of the [[Poesten Kill]], a creek that was commonly used for water power in the area. Allen had two new buildings erected and funneled creek water to them through tunnels in the bedrock underneath the buildings. He named his enterprise the Millville Manufacturing Company and converted the mill into an auger and bit factory. At one point, he experimented with producing cable chains and files, as well. But the company soon ran into legal difficulties and was dissolved.<ref name=sylvester544/><ref name=sylvester542543>Sylvester (1880), pp. 542–543</ref>

Allen reopened the company in 1859, having added machinery for the manufacture of [[garden hoe|hoes]], and opened a retail store on site. He found a successful market in the [[American South|South]], where demand was high. His company, newly named the Planters' Hoe Company, was a considerable success. However, as the nation came closer to civil war, demand dropped and sales in the South became impossible. The factory was closed in 1861, not only due to poor sales, but because Allen enlisted in the [[Union Army]].<ref name=sylvester544/><ref name="sylvester542543"/>

==Public and armed service==
[[File:Allen House (544) Restored (Downsampled).png|thumb|left|Allen's home in [[Eagle Mills, New York|Eagle Mills]], which still stands today]]
Allen's presence was noticeable within Brunswick. In March 1853, he presided over a meeting to incorporate the "Church of the Disciples of Christ at the village of Millville", now known simply as Church of the Disciples of Christ. That summer, a church building was erected at a cost of about $1,200.<ref name=weise105>[https://archive.org/stream/cu31924064123015#page/n109/mode/2up Weise] (1880), p. 105</ref> In 1856 Allen became the [[Town supervisor|supervisor]] of Brunswick, a position he was reelected to once and held until 1857; he ran as a [[Whig Party (United States)|Whig]].<ref name=paine74>[https://archive.org/stream/painefamilyrecor02pain#page/145/mode/2up/ Paine] (1883), p. 74</ref><ref name=anderson529>[https://archive.org/stream/landmarksofrenss00ande#page/528/mode/2up/search/brunswick Anderson] (1897), p. 529</ref> He was also elected [[justice of the peace]] in 1861,<ref name=anderson530>[https://archive.org/stream/landmarksofrenss00ande#page/530/mode/2up Anderson] (1897), p. 530</ref> but he did not finish his term because he decided to serve in the Civil War; this time he ran as a [[Democratic Party (United States)|Democrat]], and remained one for the rest of his life.<ref name=sylvester544/>

In early 1862, the 125th Volunteer Infantry Regiment had been put together in Brunswick and a call by [[Abraham Lincoln|President Lincoln]] for more troops was answered by Allen that September. He enlisted in Company C of the [[169th New York Volunteer Infantry]] and was chosen [[Captain (United States O-3)|captain]], having organized the company within six days.<ref name=sylvester544/> Most of the infantrymen came from Brunswick, though some came from surrounding towns. Residents of the town raised the money necessary to cover various "bounties and expenses" incurred by Allen's soldiers while en route to war.<ref name=anderson524>[https://archive.org/stream/landmarksofrenss00ande#page/524/mode/2up Anderson] (1897), p. 524</ref>

Allen participated in all the battles in which his regiment took part except a few minor ones, due to wounds he received in battle. During the [[Battle of Cold Harbor]], the bones in his wrist were shattered, and he later incurred a flesh wound in the same wrist at the [[First Battle of Fort Fisher]]. He was also wounded in the leg near his ankle at Fort Fisher; the [[Musket#Ammunition|musket ball]] remained there until his death. While recovering from injuries, he was reassigned to New York to recruit new soldiers. He was promoted to the rank of [[Major (United States)|major]] in June 1864, and, for "meritorious conduct" at Fort Fisher, he was recommended for promotion by President Lincoln and [[brevet (military)|brevetted]] [[Lieutenant colonel (United States)|lieutenant colonel]] on March 13, 1865.<ref name=sylvester544/>

==Post war and personal life==
Allen returned to Brunswick after the war and reopened the Planters' Hoe Company in association with George T. Lane. He ended the production of augers, which still made up a small percentage of production even after closing Millville Manufacturing. Allen put himself in charge of manufacturing and sales<ref name=sylvester544/><ref name=anderson525>[https://archive.org/stream/landmarksofrenss00ande#page/n691/mode/2up Anderson] (1897), p. 525</ref> and he again secured a solid market share in the South. A prosperous trade resumed and was still strong at least until the 1880s.<ref name=sylvester544/> The company eventually expanded and began manufacturing lighter common hoes for the general public, adding to its heavy southern-style line of hoes.<ref name=sylvester542543/> The Planters' Hoe Company was still a success as late as the 1890s,<ref name=sylvester544/> but it was extinct by the 1920s.<ref name=hayner104>Hayner (1925), p. 104</ref>

In addition to holding the offices of justice of the peace and town supervisor, it is also said that Allen was one of the foremost supporters of education in the town. On July 6, 1847, he married Sarah H. Payne, daughter of David H. and Catharine C. Payne, of Hamden, Connecticut. The couple had two children: Sarah and Cornelia May.<ref name=sylvester544/>

Allen died on April 24, 1884, and is buried in Eagle Mills Cemetery on [[New York State Route 2|Brunswick Road]] in Eagle Mills; his wife died in 1907 and is buried next to him.<ref name=grave>[[:File:Joseph H Allen Grave 3.jpg|Allen Family Gravestone]]. "Col. Joseph H. Allen; 169th Regiment NY Vols.; Died April 24, 1884; Aged 62 Years; His Wife; Sarah H. Allen; 1826–1907". Eagle Mills Cemetery in Eagle Mills New York.</ref> Based on insignia carved on his gravestone (specifically the [[Square and Compasses]]), Allen was a [[Freemasonry|Mason]].<ref name=grave/>

==Notes==
{{Refbegin}}
*'''a.''' {{Note_label|a|a|none}} In New York, a [[town supervisor]] is viewed by many to be equivalent to a mayor, but a supervisor is actually the presiding officer of the town council and is therefore part of the legislative branch; town governments in New York do not have an executive branch.<ref>{{cite web |url=http://www.dos.state.ny.us/lgss/pdfs/Handbook.pdf#page=63 |title=Local Government Handbook |format=PDF |page=63 |publisher=New York State Department of State |year=2009|edition=6th |accessdate=2010-03-27}}</ref>
*'''b.''' {{Note_label|b|b|none}} [[Eagle Mills, New York|Eagle Mills]] is a [[Hamlet (New York)|hamlet]] (a small, [[Municipal corporation|unincorporated settlement]]) in the [[Town (New York)|Town]] of Brunswick. It was previously known as Milltown and later Millville; both of these names are now defunct, though "Milltown" was hardly used to begin with.<ref name=weise105/>
{{Refend}}

==References==
{{Portal|Capital District}}
{{Reflist}}

==Bibliography==
{{Commons category}}
*{{Cite book |last=Anderson |first=George Baker |title=Landmarks of Rensselaer County New York |publisher=D. Mason and Company |location=Syracuse, New York |year=1897 |url=https://archive.org/stream/landmarksofrenss00ande#page/n5/mode/2up |oclc= 1728151}}{{via|IA}}
*{{Cite book |last=Hayner |first=Rutherford |title=Troy and Rensselaer County New York: A History |location=New York |publisher=Lewis Historical Publishing Company, Inc |year=1925 |oclc=22524006}}
*{{Cite book |last=Paine |first=Henry Delaven |title=Paine Family Records: A Journal of Genealogical and Biographical Information Respecting the American Families of Payne, Paine, Payn, &c. (Volume II) |publisher=J. Munsell |year=1883 |location=[[Albany, New York]] |url=https://archive.org/stream/painefamilyrecor02pain#page/n7/mode/2up |oclc= 247490136}}{{via|IA}}
*{{Cite book |last=Sylvester |first=Nathaniel Bartlett |title=History of Rensselaer Co., New York with Illustrations and Biographical Sketches of its Prominent Men and Pioneers |publisher=Everts & Peck |location=Philadelphia |year=1880 |oclc= 3496287}}
*{{Cite book |last=Weise |first=Arthur James |title=History of the Seventeen Towns of Rensselaer County from the Colonization of the Manor of Rensselaerwyck to the Present Time |year=1880 |publisher=J. M. Francis & Tucker |location=Troy, New York |url=https://archive.org/stream/cu31924064123015#page/n5/mode/2up |oclc= 6637788}}{{via|IA}}

{{s-start}}
{{s-bef|before=William Lape}}
{{s-ttl|title=[[Town Supervisor|Supervisor]] of the [[Brunswick, New York|Town of Brunswick]]|years=1856–1857}}
{{s-aft|after=Alanson Cook}}
{{end}}

{{DEFAULTSORT:Allen, Joseph H.}}
[[Category:1821 births]]
[[Category:People from Grand Isle County, Vermont]]
[[Category:People from Brunswick, New York]]
[[Category:New York Whigs]]
[[Category:19th-century American politicians]]
[[Category:New York Democrats]]
[[Category:Union Army officers]]
[[Category:1884 deaths]]