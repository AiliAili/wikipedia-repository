{{Orphan|date=December 2014}}

An aluminum fence is a fence constructed primarily out of the element [[aluminium]]. Due to the metal’s low density and ability to resist corrosion, it has become a popular choice as lightweight, durable fence and railing structure.

Aluminum fencing is used for both commercial and residential use. Aluminum fence is generally available preformed, in a wide variety of styles. It tends to be easy to clean, resists weathering and has low maintenance requirements. It can also be more cost-efficient than comparable materials, such as [[wrought iron]] and [[steel]].

Many aluminum fences have rackable fence sections, which are designed to rack 25 inches over 6’ with no special rails. The fence can then adjust to the slope of your land, meaning there will be no gaps at the bottom of the fence on uneven or sloping ground. This will accommodate all but the steepest grades.

Aluminum fences are made in a wide range of sizes and shapes. They come in many different colors, which are applied using a powder coating. The coating is applied through an electrostatic process and cured under heat to form a strong smooth surface. The powder coating adheres to aluminum better than paint or other coatings. It has the ability to withstand harsh conditions including extreme heat and cold.<ref>{{cite web|title=Powder Coating: The Solution to Rust|url=http://ornacofence.com/why-choose-powder-covered-aluminum-fence/|website=ornacofence.com|publisher=ORNACO Fence|accessdate=2014-11-19}}</ref> Its weather resistant quality leads many manufacturers to offer very long [[Warranty|warranties]], from decades up to "lifetime" for some brands.

Aluminum fence materials are often in the form of rigid rails or posts, but flexible forms are also used. Aluminum fences are made in multiple grades ranging from light weight residential to heavy weight industrial.

Aluminum fencing didn’t become popular in the residential world until 10–15 years ago as it was more expensive and the benefits were lesser known. Over the past 5 years, many residential neighborhood Home Owners Associations began requiring aluminum fencing due to its uniform look from home to home. As it requires little to no maintenance and does not rust, the fences remain “new” in appearance for many years.<ref>{{cite web|title=Ornamental Fencing|url=http://americanfenceassociation.com/project/ornamental/|website=American Fence Association|accessdate=2014-11-19}}</ref>

Aluminiyum fences can be used for  Privacy fencing, to provide privacy and security; [[Pool fence|Pool fencing]]; Decorative or ornamental fencing, to enhance the appearance of a property; Boundary fencing, to demarcate a piece of real property (link), or 	[[Perimeter fencing]], to prevent children and/or pets from wandering away

==References==
{{reflist}}

[[Category:Fences]]