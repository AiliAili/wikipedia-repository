{{Use dmy dates|date=November 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Blériot XI
  |image = Bleriot XI Thulin A 1910 a.jpg
  |caption = Thulin A (licence-built Blériot XI)
}}{{Infobox Aircraft Type
  |type = Civil tourer/trainer/military
  |manufacturer = [[Blériot Aéronautique|Louis Blériot]]
  |designer = Louis Blériot and Raymond Saulnier
  |first flight =  23 January 1909
  |introduction = 
  |retired =
  |status = 
  |primary user = 
  |more users = 
  |produced = 
  |number built = 
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}
The '''Blériot XI''' is a French aircraft. The first example was used by [[Louis Blériot]]  to make the first flight across the [[English Channel]] in a heavier-than-air aircraft on 25 July 1909.  This achievement is one of the most famous accomplishments of the [[Aviation in the pioneer era|pioneer era]] of aviation, and not only won Blériot a lasting place in history but also assured the future of his aircraft manufacturing business. The event caused a major reappraisal of the importance of aviation; the English newspaper ''[[The Daily Express]]'' led its story of the flight with the headline "Britain is no longer an Island".<ref>[http://www.dailymail.co.uk/news/article-1202165/THE-WIDER-VIEW-100-years-Bleriot-flew-Channel-identical-plane-repeats-feat.html "The Wider View: 100 years after Blériot first flew across the Channel, an identical plane repeats the feat (but not before the French had blocked the first attempt)."] ''The Daily Express,'' 26 July 2009. Retrieved: 8 January 2012.</ref>

It was produced in both single- and two-seat versions, powered by a number of different engines and  was widely used for competition and training purposes. Military versions were bought by many countries, continuing in service until after the outbreak of  [[World War I]] in 1914.  Two restored examples — one in the [[United Kingdom]] and one in the [[United States]] — of original Blériot XI aircraft are thought to be the two oldest flyable aircraft in the world.
{{TOC limit|limit=2}}

==Design==
[[File:CNAM-IMG 0625.jpg|thumb|left|200px|Closeup of the original Bleriot XI's main landing gear]]
[[File:Bleriot.jpg|thumb|right|Blériot XI as first built: note small "teardrop" profile fin on cabane]]
The '''Blériot XI''', largely designed  by Raymond Saulnier,<ref>Elliott 2000, p. 142.</ref> was a development of the [[Blériot VIII]], which Blériot had flown successfully in 1908. Like its predecessor, it was a [[tractor configuration|tractor-configuration]] [[monoplane]] with a partially covered box-girder [[fuselage]] built from [[Fraxinus excelsior#Uses|ash]] with wire cross bracing.  The principal difference was the use of [[wing warping]] for lateral control.  The tail surfaces consisted of a small [[balanced rudder|balanced]] "all-moving" [[rudder]] mounted on the rearmost vertical member of the fuselage and a horizontal [[tailplane]] mounted under the lower [[longeron]]s. This had [[elevator (aircraft)|elevator]] surfaces making up the outermost part of the fixed horizontal surface; these "tip elevators" were linked by a torque tube running through the inner section.  The bracing and warping wires  were attached to a dorsal [[cabane strut|cabane]] consisting of a pair of inverted V struts with their apexes connected by a longitudinal tube and an inverted four-sided pyramidal ventral cabane, also of steel tubing, below.  When first built it had a [[wingspan]] of {{convert|7|m|ft|abbr=on}} and a small teardrop-shaped fin mounted on the cabane,<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200018.html "Blériot No.9"][[Flight International|'Flight'']] 9 January 1909</ref> which was later removed.

Like its predecessor, it had the engine mounted directly in front of the [[leading edge]] of the wing and the main undercarriage was also like that of the Type VIII, with the wheels  mounted in castering trailing arms which could slide up and down steel tubes, the movement being sprung by [[bungee cord]]s. This simple and ingenious design allowed [[crosswind landing]]s with less risk of damage. A sprung tailwheel was fitted to the rear fuselage in front of the tailplane, with a similar castering arrangement.

When shown at the Paris Aero Salon in December 1908, the aircraft was powered by a {{convert|35|hp|kW|abbr=on|order=flip}} 7-cylinder [[R.E.P.]] engine driving a four-bladed paddle-type propeller. The aircraft was first flown at [[Issy-les-Moulineaux]] on 23 January 1909,<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200062.html "Bleriot Flies His Short-Span Machine."] [[Flight International|''Flight'']], 30 February 1909.</ref> but although the aircraft handled well, the engine proved extremely unreliable and, at the suggestion of his mechanic Ferdinand Collin, Blériot made contact with [[Alessandro Anzani]], a famous motorcycle racer whose successes were due to the engines that he made, and who had recently entered the field of aero-engine manufacture.  On 27 May 1909, a {{convert|25|hp|kW|abbr=on|order=flip}} [[Anzani 3-cylinder]] fan-configuration (semi-radial) engine was fitted.<ref>Elliott 2000, p. 73.</ref> The propeller was also replaced with a Chauvière ''Intégrale'' two-bladed propeller made from laminated [[Juglans#Wood|walnut wood]]. This propeller design was a major advance in French aircraft technology and was the first European propeller to rival the efficiency of the propellers used by the [[Wright Brothers]].<ref>Gibbs-Smith, C.H., ''Aviation''. London: NMSO, 2003, p. 150.</ref>

During early July, Blériot was occupied with flight trials of a new aircraft, the two-seater [[Blériot XII|Type XII]], but resumed flying the Type XI on 18 July. By then, the small cabane fin had been removed and the wingspan increased by {{convert|79|cm|abbr=on}}. On 26 June, he managed a flight lasting 36 minutes 55 seconds, and on 13 July, Blériot won the Aero Club de France's first ''Prix du Voyage'' with a {{convert|42|km|mi|abbr=on}} flight between [[Etampes]] and [[Orléans]].<ref>Eliott 2000, p. 96.</ref>

==The Channel crossing==
{{See also|Louis Blériot#1909 Channel crossing}}
[[File:Bleriot mid-channel.jpg|thumb|Blériot over the English Channel, 25 July 1909]]
The Blériot XI gained lasting fame on 25 July 1909, when Blériot crossed the [[English Channel]] from [[Calais]] to [[Dover]], winning a £1,000 prize awarded by the ''[[Daily Mail aviation prizes|Daily Mail]]''. For several days, high winds had grounded Blériot and his rivals: [[Hubert Latham]], who flew an [[Antoinette (manufacturer)|Antoinette]] monoplane, and [[Charles de Lambert (aviator)|Count de Lambert]], who brought two [[Wright Model A|Wright biplanes]].  On 25 July, when the wind had dropped in the morning and the skies had cleared, Blériot took off at sunrise.  Flying without the aid of a compass, he deviated to the east of his intended course, but, nonetheless, spotted the English coast to his left. Battling turbulent wind conditions, Blériot made a heavy "pancake" landing, nearly collapsing the undercarriage and shattering one blade of the propeller, but he was unhurt. The flight had taken 36.5 minutes and had made Blériot a celebrity, instantly resulting in many orders for copies of his aircraft.

The aircraft, which never flew again, was hurriedly repaired and put on display at [[Selfridges]] department store in London. It was later displayed outside the offices of the French newspaper ''[[Le Matin (France)|Le Matin]]'' and eventually bought by the ''[[Musee des Arts et Metiers]]'' in Paris.

==Subsequent history==
After the successful crossing of the English Channel, there was a great demand for Blériot XIs.  By the end of September 1909, orders had been received for 103 aircraft.<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200593.html "M. Bleriot's Plans."] [[Flight International|''Flight']], 25 September 1909.</ref> After an accident at an aviation meeting in [[Istanbul]] in December 1909, Blériot gave up competition flying, and the company's entries for competitions were flown by other pilots, including [[Alfred Leblanc]], who had managed the logistics of the cross-channel flight, and subsequently bought the first production Type XI, going on to become one of the chief instructors at the flying schools established by Blériot.

In February 1912 the future of the Type XI was threatened by the French army placing a ban on the use of all monoplanes. This was the result of a series of accidents in which Blériot aircraft had suffered wing failure in flight. The first of these incidents had occurred on 4 January 1910, killing [[Léon Delagrange]], and was generally attributed to the fact that Delagrange had fitted an over-powerful engine, so overstressing the airframe. A similar accident had killed Peruvian pilot [[Jorge Chavez]] at the end of 1910 at the end of the first flight over the Alps, and in response to this the wing spars of the Blériot had been strengthened.  A subsequent accident led to a further strengthening of the spars.<ref name= "Bleriot report">[http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200284.html Monoplane Failures][[Flight International|''Flight'']] 30 March 1912</ref>  Blériot, understandably, took this matter very seriously, and produced a report for the French government which came to the conclusion that the problem was not the strength of the wing spars but a failure to take into account the amount of downward force to which aircraft wings could be subjected, and that the problem could be solved by increasing the strength of the upper bracing wires.  This analysis was accepted, and Blériot's prompt and thorough response to the problem enhanced rather than damaged his reputation.<ref name = "Bleriot report"/>

===Further development===
The Type XI remained in production until the outbreak of the First World War, and a number of variations were produced. Various types of engine were fitted, including the 120° Y-configuration, "full radial" three-cylinder Anzani (like the restored example at [[Old Rhinebeck Aerodrome]] still flies with) and the {{convert|50|hp|kW|abbr=on|order=flip}} and {{convert|70|hp|kW|abbr=on|order=flip}}, seven cylinder [[Gnome et Rhône|Gnome]] [[rotary engine]]s.  Both single and two-seat versions were built, and there were variations in wingspan and fuselage length. In later aircraft the tip elevators were replaced by a more conventional trailing edge elevator, the tailwheel was replaced by a skid, and the former "house-roof" five-member dorsal cabane being replaced by a simpler, four-sided pyramidally framed unit similar to the ventral arrangement for the later rotary-powered versions. Blériot marketed the aircraft in four categories: trainers, sport or touring models, military aircraft, and racing or exhibition aircraft.

===Civil use===
The Type XI took part in many competitions and races.  In August 1910 Leblanc won the {{convert|805|km|mi|abbr=on}} ''Circuit de l'Est'' race, and another Blériot flown by [[Emile Aubrun]] was the only other aircraft to finish the course.<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200695.html "The Circuit de l'Est."] [[Flight International|''Flight'']], 27 August 1910.</ref> In October 1910, [[Claude Grahame-White]] won the second competition for the [[Gordon Bennett Trophy (aeroplanes)|Gordon Bennett Trophy]] flying a Type XI fitted with a {{convert|100|hp|kW|abbr=on|order=flip}} Gnome,  beating a similar aircraft flown by Leblanc, which force-landed on the last lap. During the race Leblanc had established a new world speed record.<ref>Villard 1987, p. 86</ref> In 1911, [[Andre Beaumont]] won the Circuit of Europe in a Type XI and another, flown by [[Roland Garros (aviator)|Roland Garros]], came second.    
[[File:Blériot XI engine.jpg|thumb|right|Anzani engined Blériot XI similar to the aircraft used for the Channel flight]]
[[File:Bleriot XI Wing.JPG|thumb|right|Detail of replica Blériot XI wing, Hamburg Airport Days, 2007]]

Louis Blériot established his first flying school at [[Etampes]] near [[Rouen]] in 1909.  Another was started at [[Pau, Pyrénées-Atlantiques|Pau]], where the climate made year-round flying more practical, in early 1910 and in September 1910 a third was established at [[Hendon Aerodrome]] near [[London]].  A considerable number of pilots were trained: by 1914 nearly 1,000 pilots had gained their ''Aero Club de France'' license at the Blériot schools, around half the total number of licences issued.<ref>Elliott 2000, p. 173.</ref>  Flight training was offered free to those who had bought a Blériot aircraft: for others it initially cost 2,000 francs, this being reduced to 800 francs in 1912.  A gifted pupil favoured by good weather could gain his license in as little as eight days, although for some it took as long as six weeks.  There were no dual control aircraft in these early days, training simply consisting of basic instruction on the use of the controls followed by solo taxying exercises, progressing to short straight-line flights and then to circuits.  To gain a license a pilot had to make three circular flights of more than 5&nbsp;km (3&nbsp;mi), landing within  {{convert|150|m|ft|abbr=on}} of a designated point.<ref>Elliott 2000, p. 171.</ref>

=== Military use ===
The first Blériot XIs entered military service in Italy and France in 1910, and a year later, some of those were used in action by Italy in North Africa (the first use of aircraft in a war) and in Mexico.<ref>[http://www.sedena.gob.mx/index.php?id_art=343 "Bleriot XI."] ''Secretaría de la Defensa Nacional''. Retrieved: 17 July 2010.</ref>  The British [[Royal Flying Corps]] received its first Blériots in 1912.  During the early stages of [[World War I]], eight French, six British and six Italian squadrons operated various military versions of the aircraft, mainly in [[Reconnaisance|observation]] duties but also as trainers, and in the case of single-seaters, as light bombers with a bomb load of up to 25&nbsp;kg.

==Famous Blériot Monoplane pilots==<!-- Please keep this list in alphabetical order by LAST NAME -->
[[File:Oskar Bider 1913 Bern.jpg|thumb|[[Oskar Bider]] starting from [[Bern]] to his flight over the Alps, showing the pyramidal dorsal cabane of later Bleriot XI examples]]
*[[Oskar Bider]] – Swiss aviator who flew over the Pyrenees and the Alps in 1913.<ref>[http://www.azimut270.ch/en/pilots/emile-taddeoli.html "Otto Britschgi."]  ''AeroRevue'' via ''azimut270.ch,'' October 2007. Retrieved: 14 January 2012.</ref>
* Baron [[Carl Calle Cederström]], who made the first flight of a heavier-than-air craft in Norway on 14 October 1910. He made a flight of 23 minutes and reached a height of 300 metres (983.9&nbsp;feet).<ref>Mulder, Rob. [http://www.europeanairlines.no/wp-content/uploads/2011/01/Timeline-06-01-2011.pdf "Timeline of Civil Aviation in Norway."] ''europeanairlines.no,'' 6 January 2011. Retrieved: 14 January 2012.</ref>
*[[Jean Louis Conneau|Jean Conneau]] (André Beaumont) In 1911 won the Paris-Rome race, the ''Circuit d'Europe'' (Tour of Europe) on 7 July  and the [[Daily Mail Circuit of Britain Air Race|Daily Mail Circuit of Britain Race]] on 26 July 1911.
*[[Jorge Chavez]] – French-Peruvian aviator who crossed the Alps in 1910, but crashed on arrival and was killed.<ref>Warth, John. [http://www.smithsonianeducation.org/scitech/impacto/graphic/aviation/jorge.html "Adventurers of the Air"] Smithsonian Institution. Retrieved: 16 January 2012.</ref>
*[[Denys Corbett Wilson|Denys Corbett-Wilson]] – Anglo-Irish aviator who made the first successful flight from Britain to Ireland in April 1912.<ref>[http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200379.html "Flying the Irish Channel"] [[Flight International|''Flight]] Volume IV, Issue 17, p. 379. Retrieved: 16 January 2012.</ref>
*[[Leon Delagrange]] – One of the first people to fly an aircraft in France, killed on 4 January 1910 flying a Blériot XI when a wing failed.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200086.html "Aero Club of France: Leon Delagrange."] ''[[Flight magazine|''Flight'']],'' 4 February 1911, p. 88. Retrieved: 16 January 2012.</ref>
*Carlo Piazza -  On October 22/23 1911, Captain Piazza of the [[Italian Royal Army]] Air Services conducted the first aerial reconnaissance flight between Tripoli & Aziza during the [[Italo-Turkish War]].<ref>http://www.airspacemag.com/daily-planet/the-worlds-first-warplane-115175678/?no-ist=</ref>
*[[John Domenjoz]] (1886–1952) – Performed aerobatics in South, Central and North America in 1914–1918.  His Gnome rotary-powered Blériot-XI is displayed at the National Air & Space Museum, Washington.<ref>[http://www.pionnair-ge.com/spip1/spip.php?article186 "John Domenjoz, 1886–1952: le roi de la voltige aérienne entre 1913 et 1920 vidéo" (in French).] ''Pionnair-ge.com''. Retrieved: 17 July 2010.</ref><ref>Cooper, Ralph. [http://www.earlyaviators.com/edomenjo.htm "John Domenjoz."] ''earlyaviators.com,'' 2010. Retrieved: 29 October 2010.</ref>
*[[Roland Garros (aviator)|Roland Garros]] - Won second place in the 1911 Circuit of Europe race, and set two world altitude records in 1912 in an adapted Type XI, flying to {{convert|5000|m|ft|abbr=on}} on 6 September 1912<ref>[http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200841.html "Garros Regains the Height Record."] [[Flight International|''Flight'']], 4 September 1912. Retrieved: 26 April 2012.</ref>
*[[Claude Grahame-White]] Won the 1910 [[Gordon Bennett Trophy (aeroplanes)|Gordon Bennett Trophy]] race held in New York flying a Blériot<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200919.html "The American International Meeting"][[Flight International]] 5 November 1910</ref>
*[[Eugène Gilbert]] – Went to the Blériot school in 1910 after having built his own small unsuccessful aircraft in 1909. During a flight across the [[Pyrenees Mountains]] in the [[1911 Paris to Madrid air race]] he and his Blériot XI were attacked by a large eagle, which Gilbert drove off by firing a pistol.<ref>Cooper, Ralph. [http://earlyaviators.com/egilbert.htm "Eugene Gilbert."] ''EarlyAviators.com.'' Retrieved: 8 January 2012.</ref>
*[[Tryggve Gran]] – Norwegian aviator, first to cross the North Sea from Scotland to Norway in 1914.<ref>[http://www.snl.no/Tryggve_Herman_Gran "Tryggve Herman Gran" (in Norwegian).] ''Store norske leksikon.'' Retrieved: 8 January 2012.</ref>*
*[[Maurice Guillaux]] – French aviator, visited Australia April–October 1914. Flew Australia's first air mail and air freight from Melbourne to Sydney, 16–18 July 1914.<ref>Parnell, Neville and Boughton, Trevor, ''Flypast'' Australian Government Publishing Service, Canberra, 1988, page 21 ISBN 0 644 07918 5</ref>
*[[Gustav Hamel]] – Flew the world's first regular airmail service between Hendon and Windsor in September 1911.<ref>[http://www.thamesweb.co.uk/aviation/aerialpost.html "The First Aerial Post: Hendon to Windsor & Windsor to Hendon."] ''Thamesweb.'' Retrieved: 8 January 2012.</ref>
*[[Vasily Kamensky]] – a famous [[Russian Futurist]] poet, one of the pioneering aviators of Russia.<ref>[http://russia-ic.com/people/general/k/277 "Vasily Kamensky."] ''russia-ic.com.'' Retrieved: 8 January 2012.</ref>
*[[Jan Kašpar]] – Czech aviator, first person to fly in [[Czech lands]] on 16 April 1910.<ref>Horáková, Pavla. [http://www.radio.cz/en/article/24573 "First Czech aviator Jan Kaspar died 75 years ago."] ''[[Czech Radio]],'' 1 February 2002. Retrieved: 8 January 2012.</ref>
*[[Hubert Le Blon]] - A former racing car driver who took up aviation and designed his own monoplane. On 2 April 1910, flying a Bleriot XI, he became the second (after Delagrange) fatality in the type after crashing in San Sebastian, Spain.
*[[Alfred Leblanc]] – Broke the [[flight airspeed record]] on 29 October 1910 while flying a Blériot XI. His speed was calculated at 68.20&nbsp;mph (109.76&nbsp;km/h): on 11 April 1911 he raised the record to 111.8 kph<ref>[http://www.flightglobal.com/pdfarchive/view/1951/1951%20-%200988.html "The World Speed Record"][[Flight International|''Flight'']] 25 May 1951</ref>
*[[Jan Olieslagers]] (1883–1942) – Lieutenant in the Belgian Army during the First World War.<ref>[http://www.theaerodrome.com/aces/belgium/olieslagers.php "Jan Olieslagers."] ''The Aerodrome,'' 2011.  Retrieved: 8 January 2012.</ref>
*[[Earle Ovington]] – First airmail pilot in the United States, used a Blériot XI to carry a sack of mail from [[Garden City, New York]] to [[Mineo]], [[Long Island]]<ref>[https://select.nytimes.com/gst/abstract.html?res=F7081EFF355B1B7B93C1AB178CD85F428385F9 ] ''[[The New York Times]],'' 23 July 1936. Retrieved: 8 January 2012.</ref>
*[[Adolphe Pégoud]] – First man to demonstrate the full aerobatic potential of the Blériot XI, flying a loop with it in 1913.  Together with John Domenjoz and Edmond Perreyon, he successfully created what is considered the first [[Airshow|air show]].<ref>[http://www.theaerodrome.com/aces/france/pegoud.php "Adolphe Pégoud."] ''The Aerodrome,'' 2011.  Retrieved: 8 January 2012.</ref>
*[[Harriet Quimby]] – First licensed female pilot in the United States; first female to fly the English Channel solo.<ref>Koontz, Giacinta Bradley. [http://www.harrietquimby.org/ "Harriet Quimby."] ''harrietquimby.org'', 2010. Retrieved: 17 July 2010.</ref> Died on 1 July 1912 when she and her passenger were ejected from her new Blériot XI-2.
*[[Rene Simon (aviator)|Rene Simon]] (1885-192?) – In February 1911, the Mexican government engaged Rene Simon, a member of an aerial circus touring the southwestern United States, to reconnoiter rebel positions near the border city of Juarez.<ref>Villard 2002, p. 116.</ref>
*[[Emile Taddéoli]] – Swiss aviator who first flew on 22 March 1910, in his newly bought Blériot XI, and flew about {{convert|150000|km|mi}} during the next five years, using various aircraft, among them the Blériot XI, [[Morane-Borel monoplane]], [[Dufaux 4]], [[Dufaux 5]] and [[SIAI S.13]] seaplane.<ref>[http://www.azimut270.ch/en/pilots/emile-taddeoli.html "Emile Taddéoli."]  ''AeroRevue'' via ''azimut270.ch,'' October 2007. Retrieved: 14 January 2012.</ref>

==Variants==
;Blériot XI (REP)
:1908, the first Type XI, powered by a {{convert|30|hp|kW|abbr=on|order=flip}} REP engine, displayed at the 1908 Paris Salon Exposition, first flown at Issy on 18 January 1909.<ref name=opdycke>{{cite book |last=Opdycke |first=Leonard E. |title=French Aeroplanes before the Great War |year=1999 |publisher=SchifferPublishing Limited |location=Atglen |isbn=0 7643 0752 5}}</ref>

;Blériot XI (Anzani)
:1909, the first aircraft re-engined with a {{convert|25|hp|kW|abbr=on|order=flip}} Anzani engine and with wings enlarged from {{convert|12|to|14|m2|sqft|abbr=on}}. Fitted with a flotation bag for Blériot's cross channel flight.<ref name=opdycke/>

;Blériot XI ''Militaire''
:Military single-seater, powered by a {{convert|50|hp|kW|abbr=on|order=flip}} Gnome engine.<ref name=opdycke/>

;Blériot XI ''Artillerie''
:Very similar to the ''Militaire'' version, but with a fuselage divided into two sections so that it could be folded for transport.<ref name=opdycke/>

;Blériot XI E1
:Single-seat training version.

;Blériot XI Type Ecole
: A trainer with considerable wing dihedral looped cane tailskid, tip elevators and other modifications.<ref name=opdycke/>

;Blériot XI R1 ''Pinguin''
:''Rouleur'' or ground training aircraft, fitted with clipped wings and a wide-track undercarriage with a pair of forward-projecting skids to prevent nose-overs. Some examples were fitted with a {{convert|35|hp|kW|abbr=on|order=flip}} Anzani engine and others with old {{convert|50|hp|kW|abbr=on|order=flip}} Gnome engines that were no longer producing their full power output.<ref name=opdycke/>

;Blériot XI (1912)
:From March 1912 with two-piece elevators and high fuselage skid.<ref name=opdycke/>

;Blériot XI Parasol:
:aka '''Blériot-gourin''', modified by Lieutenant Gouin and Henri Chazal with a parasol wing and split airbrake/rudder.<ref name=opdycke/>

;Blériot XIbis
:In January 1910 the bis introduced more conventional tail feathers and elliptical elevators with a half-cowled Gnome engine.<ref name=opdycke/>

;Blériot XI-2 Tandem
:Standard tandem 2-seat touring, reconnaissance, training model, powered by a {{convert|70|hp|kW|abbr=on|order=flip}} Gnome 7B rotary piston engine.<ref name=opdycke/>

;Blériot XI-2 bis''' "''côte-à-côte''"[[File:Bleriot.png|thumb|Blériot XI-2 bis]]
:February 1910 2-seat model, with side-by-side seating and a non-lifting triangular tailplane with semi-elliptical trailing-edge elevators, with several variations such as floats extended nose, modified tail-skid and other changes.<ref name=opdycke/>  (Length {{convert|8.32|m|ft|abbr=on}},  Wingspan {{convert|10.97|m|ft|abbr=on}}<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%201070.html "The Bleriot 2-Seater Monoplane, Type XI 2 bis."] [[Flight International|''Flight'']], 31 December 1911.</ref>

;Blériot XI-2 ''Hydroaeroplane''
: Two-seater [[floatplane]] with  wingspan of {{convert|11|m|ft|abbr=on}} powered by a  {{convert|80|hp|kW|abbr=on|order=flip}} Rhône engine.<ref>[http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201281.html "The New Blériot Hydro-Aeroplane."] [[Flight International|''Flight'']], 28 November 1913. Retrieved: 26 April 2012.</ref>  First flown with an extended rudder with a float on the bottom: this was later replaced by a standard rudder and a float fitted under the rear fuselage.<ref name=opdycke/>
;Blériot XI-2 ''Artillerie''
:Military 2-seat model, powered by a {{convert|70|hp|kW|abbr=on|order=flip}} Gnome rotary piston engine. Two aircraft or versions of the same aircraft with differing elevators.

;Blériot XI-2 ''Génie''
:Military version, designed for easy transport, it could be broken down/reassembled in 25 minutes.<ref name=opdycke/>

;Blériot XI-2 ''Vision totale''
:XI-2 modified with a parasol wing in July 1914.<ref name=opdycke/>

;Blériot XI-2 ''Hauteur''
:Powered by an {{convert|80|hp|kW|abbr=on|order=flip}} Gnome rotary piston engine and used by [[Roland Garros (aviator)|roland Garros]] in altitude record flights in August 1912 and March 1913.<ref name=opdycke/>

;Blériot XI-2 BG
:Two-seat high-wing parasol model.

;Blériot XI-3 Concours Militaire
:Tandem 3-seat model, powered by a twin-row 14-cylinder, {{convert|140|hp|kW|abbr=on|order=flip}} Gnome Double Lambda rotary engine. Span 11.35&nbsp;m (37&nbsp;ft&nbsp;3&nbsp;in), length {{convert|8.5|m|ft|abbr=on}}<ref name=opdycke/><ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200896.html "Nassau Boulevard Meeting."] [[Flight International|''Flight'']], 24 October 1911.</ref>

;Thulin A
:Licence-built in [[Sweden]]

==Military operators==
[[File:Blériot XI.svg|thumb|300px|Blériot XI]]
;{{ARG}}
*[[Argentine Air Force]]
;{{AUS}}
*[[Australian Flying Corps]]
**[[Central Flying School RAAF|Central Flying School AFC]] at [[Point Cook, Victoria]]
;{{BEL}}
*[[Belgian Air Force]]
;{{flag|Bolivia}}
*[[Bolivian Air Force]]
;{{flag|Brazil}}
*[[Brazilian Air Force]]
;{{flag|Bulgaria}}
*[[Bulgarian Air Force]]
;{{CHL}}
*[[Chilean Air Force]]
;{{DNK}}
*[[Royal Danish Air Force]]
;{{FRA}}
*[[French Navy]]
;{{flag|Greece|old}}
*[[Hellenic Air Force]]
;{{GTM}}
*[[Guatemalan Air Force]]
;{{flag|Kingdom of Italy}}
*[[Corpo Aeronautico Militare]]
;{{JPN}}
*[[Imperial Japanese Army Air Service]]
;{{flag|Mexico}}
*[[Mexican Air Force]]
;{{NOR}}: [[Norwegian Army Air Service]]. One only: [[Tryggve Gran]]'s
;{{NZL}}: [[Royal New Zealand Air Force]]. One aircraft named "Brittania"; it was in service from 1913 to 1914.
;{{flag|Romania}}
*[[Romanian Air Force]]
;{{RUS}}
*[[Imperial Russian Air Service]]
[[File:FirstSerbianArmedPlane1915.jpg|thumb|Blériot XI used by Serbia, 1915]]
;{{flagicon|Kingdom of Serbia}} [[Kingdom of Serbia|Serbia]]
*[[Serbian Air Force and Air Defense]]
;{{SWE}}
*[[Swedish Air Force]]
*[[Swedish Navy]]
;{{CHE}}
*[[Swiss Air Force]]
;{{flag|Ottoman Empire}}
*[[Aviation Squadrons (Ottoman Empire)|Ottoman Aviation squadrons]]
[[File:RFC Bleriot.jpg|thumb|Blériot XI with RFC markings during WW1]]
;{{UK}}
*[[Royal Flying Corps]]
**[[No. 2 Squadron RAF|No. 2 Squadron RFC]]
**[[No. 3 Squadron RAF|No. 3 Squadron RFC]]
**[[No. 4 Squadron RAF|No. 4 Squadron RFC]]
**[[No. 5 Squadron RAF|No. 5 Squadron RFC]]
**[[No. 6 Squadron RAF|No. 6 Squadron RFC]]
**[[No. 9 Squadron RAF|No. 9 Squadron RFC]]
**[[No. 10 Squadron RAF|No. 10 Squadron RFC]]
**[[No. 16 Squadron RAF|No. 16 Squadron RFC]]
**[[No. 23 Squadron RAF|No. 23 Squadron RFC]]
**[[No. 24 Squadron RAF|No. 24 Squadron RFC]]
;{{URU}}
*[[Uruguayan Air Force]]

==Survivors==
[[File:Avion Bleriot XI 1909.jpg|thumb|The original Blériot XI on which [[Louis Blériot]] crossed the Channel in 1909 in the Musée des Arts et Métiers, Paris.]]
In addition to the aircraft used by Louis Blériot to make his cross-channel flight in 1909, on display in the [[Musée des Arts et Métiers]] in Paris, a number of examples have been preserved. Both the British and American restored-to-airworthiness examples, each now over a century old and believed to be the two oldest flyable aircraft anywhere on Earth, are usually only "hopped" for short distances due to their uniqueness.

===Airworthy aircraft===

*A 1909-built Blériot XI, with British civil registration ''G-AANG'', is on display at the [[Shuttleworth Collection]], Old Warden, England. It is the world's oldest airworthy aircraft, powered by a three-cylinder "W form" Anzani engine, identical to Blériot's original cross-Channel aircraft engine.
*A restored and flyable Blériot XI, powered by a 120°-angle regular "radial" Anzani three-cylinder engine and identified by Blériot factory serial number 56 and bearing U.S. civil registration ''N60094'', is at the [[Old Rhinebeck Aerodrome]] (ORA). It is believed to have been built only three weeks after the Shuttleworth example.
[[File:Bleriot XI Thulin A Gardet.JPG|thumb|Maiden public flight by a Blériot XI, manufactured 1918 under license by [[AB Thulinverken]] (AETA) in Landskrona, Sweden as type '''Thulin A'''. Photo: Bengt Oberger.]]  
*A Blériot XI, the oldest airworthy museum aircraft in Sweden, manufactured in 1918 under licence by AETA, Enoch Thulins Aeroplane Works, in [[Landskrona]], Sweden, as type '''Thulin A''', has been owned by The Museum of Science and Technology in Stockholm, Sweden since 1928. Following a two-year restoration by Mikael Carlson, the Blériot XI made what was probably its maiden flight to celebrate the Centenary of Flight in Sweden, at the Stockholm Festival of Flight on 20–22 August 2010. Registered with the Swedish Civil Air Traffic Authority in 2010 as SE-AEC, the Blériot uses its original rotary engine, a [[AB Thulinverken|Thulin]]-built copy of the [[Gnome Omega]]. At the Stockholm Festival of Flight, the Blériot took off and landed no less than six times from a grass strip at The Royal Park, and was finally rolled 200 meters back to the Museum Exhibition Hall.
*Although not a survivor, a reproduction of the Blériot XI "Le Scarabée", flown over Montreal by Count Jacques de Lesseps in 1910, was built by volunteers at the Canadian Aviation Heritage Centre (CAHC/CCPA) in [[Sainte-Anne-de-Bellevue, Quebec|Sainte-Anne-de-Bellevue]] (a neighborhood in [[Montreal, QC]]).  They spent nearly 15 years building this exacting reproduction from original blueprints; its first flight took place in September 2014. It is currently on display at the Canadian Aviation Heritage Centre.

===Display aircraft===
* [[Musée des Arts et Métiers]], Paris, France. The original aircraft.
* [[Musée de l'Air et de l'Espace]], Le Bourget, France
* [[Museo Nacional de Aeronáutica]] in [[Morón, Buenos Aires|Morón]], [[Buenos Aires Province]], Argentina. The aircraft has replica wings and is powered by a "W" three-cylinder Anzani 25&nbsp;hp engine.<ref>[http://museonacionaldeaeronauticamoron.blogspot.com/2011/02/el-bleriot-y-el-castaibert-nuevamente.html "Museo Nacional de Aeronáutica (in Spanish)."] ''museonacionaldeaeronauticamoron,'' 16 February 2011. Retrieved: 15 January 2012.</ref>
* [[Smithsonian Institution|Smithsonian]] [[National Air and Space Museum]], [[Washington, D.C.]], United States of America: Swiss-born pilot John Domenjoz's Blériot XI was manufactured in 1914 and powered by a {{convert|50|hp|kW|abbr=on|order=flip}} Gnôme.<ref>[http://www.nasm.si.edu/collections/artifact.cfm?id=A19500095000 "Blériot XI."] {{webarchive |url=https://web.archive.org/web/20120105050920/http://www.nasm.si.edu/collections/artifact.cfm?id=A19500095000 |date=5 January 2012 }} ''National Air and Space Museum, Smithsonian.'' Retrieved: 15 January 2012.</ref>
* [[RAF Museum]], Hendon, England: Factory Serial Number: 164 and powered by a six-cylinder Anzani.<ref>{{cite web|url=http://www.rafmuseum.org.uk/research/collections/bl-riot-xi/|title=Blériot XI|publisher=Royal Air Force Museum  |accessdate =20 October 2013}}</ref>
* [[Army Aviation Museum]], [[Fort Rucker]], [[Alabama]], United States of America.<ref>Holcomb, Kevin. [http://www.airminded.net/bleriot/bleriot2.html "Surviving Bleriot XI's."] ''Holcomb's Aerodrome.'' Retrieved: 15 January 2012.</ref>
*[[Canada Aviation and Space Museum]], [[Ottawa|Ottawa, Ontario]], Canada: License built by the  California Aeroplane Manufacturing and Supply Company, United States in 1911 and powered by an Elbridge Aero Special 60&nbsp;hp, 4-cylinder, water-cooled engine.<ref>[http://www.aviation.technomuses.ca/collections/artifacts/aircraft/BleriotXI/ "Blériot XI."] ''Canada Aviation and Space Museum.'' Retrieved: 15 January 2012.</ref>
*[[Cradle of Aviation Museum]], New York: Originally purchased by [[Rodman Wanamaker]], the first aircraft to be imported into America,<ref>Stoff, Joshua. [http://www.cradleofaviation.org/exhibits/hp/bleriot/index.html "The Blériot #153 Comes to The Cradle of Aviation Museum."] ''The Cradle of Aviation Museum.'' Retrieved: 15 January 2012.</ref> and formerly in the Old Rhinebeck collection of [[Cole Palen]].
* Old Rhinebeck Aerodrome — A second Bleriot XI in the collection now on static display, was formerly flown under Gnome Omega rotary engine power at the Aerodrome before heading off for display for a period of time at the USS Intrepid Sea-Air-Space Museum in New York City. It was built in the USA in 1911 by the American Aeroplane & Supply House of [[Hempstead, New York]] as a "cross-country" longer-ranged version with a copper ventral fuel tank, and had been in storage at least since November 1915 before its discovery in 1963, and restored in 1975-76 by Cole Palen.<ref>[http://oldrhinebeck.org/ORA/bleriot-xi-cross-country/ Bleriot XI "Cross Country"] ''Old Rhinebeck Aerodrome'' Retrieved: February 28, 2014.</ref>
*[[Powerhouse Museum]], Sydney, Australia: The aircraft flown by Maurice Guillaux with first Australian airmail from Melbourne to Sydney in 1914.<ref>Thompson, Stephen. [http://www.migrationheritage.nsw.gov.au/exhibition/objectsthroughtime/bleriot-monoplane/ "1914 Bleriot XI Monoplane."] ''Migration Heritage Centre,'' 2011.  Retrieved: 15 January 2012.</ref>
* [[New England Air Museum]], Windsor Locks, CT:  a restored Bleriot XI with a Detroit Aero engine built in 1911 by Ernest Hall.
*[[Museo del Aire (Madrid)|Museo del Aire]], Madrid: A Spanish-built Blériot XI, the Vilanova Acedo.<ref>{{cite web|url=http://www.ejercitodelaire.mde.es/ea/pag?idDoc=00C3BA0E9F6D393AC125746C002A3D33|title=Hangar 1 del Museo de Aeronáutica y Astronáutica|publisher=Museo del Ejército del Aire  |language=Spanish|accessdate=20 October 2013}}</ref>
* [[National Technical Museum (Prague)]] displays the original Bleriot of Jan Kašpar.

==Specifications (Blériot XI)==
{{Aircraft specs
|ref=<ref name="Ang">Angelucci 1983, p. 20.</ref>
|prime units?=met
<!--
        General characteristics
-->
|crew=1
|length m=7.62
|span m=7.79
|height m=2.69
|wing area sqm=14
|empty weight kg=230
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Anzani 3-cylinder fan engines|Anzani 3-cyl. fan]]
|eng1 type=3-cyl. air-cooled fan-style radial piston engine
|eng1 hp=25<!-- prop engines -->
|prop blade number=2<!-- propeller aircraft -->
|prop name=[[Chauvière Intégrale]]
|prop dia m=2.08<!-- propeller aircraft -->
<!--
        Performance
-->
|max speed kmh=75.6
|ceiling m=1,000
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Angelucci, Enzo. ''The Rand McNally Encyclopedia of Military Aircraft, 1914–1980''. San Diego, California: The Military Press, 1983. ISBN 0-517-41021-4.
* Charlson, Carl and [[:fr:Christian Cascio]], directors. ''A Daring Flight ''(DVD). Boston: WGBH Boston Video, 2005.
* Crouch, Tom D. ''Blériot XI: The Story of a Classic Aircraft.'' Washington, D.C.: Smithsonian Institution Press, 1982. ISBN 978-0-87474-345-6.
* Elliott, Bryan A. ''Blériot: Herald Of An Age.'' Stroud, Gloucestershire: Tempus, 2000. ISBN 0-7524-1739-8.
* Munson, Kenneth. ''Bombers, Patrol and Reconnaissance Aircraft 1914–1919'' (Blandford Colour Series). London: Associate R.Ae.S., 1977. {{Listed Invalid ISBN|0-7137-0632-8}}
*[[Henry Villard|Villard, Henry Serrano]]. ''Blue Ribbon of the Air''. Washington: Smithsonian Press, 1987. ISBN 0-87474-942-5.
* [[Henry Villard|Villard, Henry Serrano]]. ''Contact! The Story of the Early Aviators.'' Boston: Dover Publications, 2002. ISBN 978-0-486-42327-2.
* Vivien, F. Louis. [http://issuu.com/chestnuts/docs/avia_n__1_1911_bleriot11 "Description détaillée du monoplan Blériot" (in French).] Paris: librairie des Sciences aéronautiques, 1905. (Original ''1911 AVIA book'' French book with Blériot XI characteristics and specifications).
{{Refend}}

== External links ==
{{Commons category|Blériot XI}}
*[http://oldrhinebeck.org/ORA/bleriot-xi/ Old Rhinebeck Aerodrome's 1909-10 Blériot XI page]
*[https://www.youtube.com/watch?v=tJErvYHZ0uk YouTube video of Old Rhinebeck's N60094 Blériot XI making a short flight]
*[https://www.youtube.com/watch?v=Xe0KZD3kPBA The Shuttleworth Collection's oldest-of-all Blériot XI making a flight]
*[https://web.archive.org/web/20050823033829/http://www.centennialofflight.gov:80/ Louis Blériot – Developer of Commercial and Military Aircraft ''US Centennial of Flight Commission''.]
*[http://musee-dufresne.com/ A Blériot XI at Maurice Dufresne Museum, France]
*[http://www.maket.ch/walkaround/category/27-bleriot-type-xi/ Blériot XI at Musée des transports de Lucerne, Switzerland]
*[http://www.mapica.org/index.php?lang=english Bleriot Type XI N° 225 at MAPICA La Baule, France]
*[http://www.pionnair-ge.com/spip1/spip.php?article186 John Domenjoz, barnstormer & aerobatics]
*[http://pagesperso-orange.fr/bleriot11/ Blériot XI for flight simulator ( FS9 or FSX )]
*[https://web.archive.org/web/20090401203021/http://campus.3ds.com:80/students/project-showroom/go/project/bleriot-xi Virtual Blériot XI redesigned in 3D by engineering students in India, Brazil and France]
*[http://www.tekniskamuseet.se/1/1552.html/ Link to website for Museum of Science and Technology, Stockholm. Documentation of Blériot XI restoration with Swedish text, videos of public display flight, test flight, motor test and wing assembling]

{{Louis Blériot aircraft}}

{{DEFAULTSORT:Bleriot 11}}
[[Category:French experimental aircraft 1900–1909]]
[[Category:Blériot aircraft|11]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Military aircraft of World War I]]
[[Category:Racing aircraft]]
[[Category:French military trainer aircraft 1910–1919]]