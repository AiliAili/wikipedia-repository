{{For|the journal published under this title from 1909 till 1933|Clinical Science (journal)}}
{{Infobox journal
| title = Heart
| cover = [[File:New Heart cover.tif|200px]]
| editor = Catherine Otto
| discipline = [[Cardiology]]
| formernames = British Heart Journal
| abbreviation = Heart
| publisher = [[BMJ Group]]
| country =
| frequency = Biweekly
| history = 1939-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license = 
| impact = 5.595
| impact-year = 2014
| website = http://heart.bmj.com
| link1 = http://heart.bmj.com/content/current
| link1-name = Online access
| link2 = http://heart.bmj.com/content/
| link2-name = Online archive
| JSTOR = 
| OCLC = 34055332
| LCCN =
| CODEN = HEARFR
| ISSN = 1355-6037
| eISSN = 1468-201X
}}
'''''Heart''''' is a biweekly [[peer-reviewed]] [[medical journal]] covering all areas of [[cardiology|cardiovascular medicine and surgery]]. It is the official journal of the [[British Cardiovascular Society]]. It was established in 1939 as the ''British Heart Journal'' and is published by the [[BMJ Group]].<ref name="Heart">[http://heart.bmj.com ''Heart'' Homepage]</ref> The name was changed from ''British Heart Journal'' to ''Heart'' in 1996 with the start of volume 75.

Topics covered include [[coronary disease]], [[electrophysiology]], [[valve disease]], [[imaging techniques]], [[congenital heart defect|congenital heart disease]] (fetal, paediatric, and adult), [[heart failure]], [[surgery]], and [[fundamental science|basic science]]. Each issue also contains an extensive [[continuing professional development|continuing professional education]] section ("Education in Heart").

The journal is available online by subscription, with archives from before 2006 accessible free of charge. The [[editor-in-chief]] is [[Catherine Otto]] ([[University of Washington]]).<ref name="Heart"/>

==Abstracting and indexing==
The journal is abstracted and indexed by [[Index Medicus]], [[Science Citation Index]], and [[Excerpta Medica]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 5.595, ranking it 15th out of 123 journals in the category "Cardiac and Cardiovascular Systems".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Cardiac and Cardiovascular Systems |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Editors-in-chief ==
The following persons have been editor-in-chief of the journal:
* D Even Bedford, J M Campbell 1939-?
* K. Shirley Smith ?-1972
* [[Walter Somerville]] 1973-?
* [[Catherine Otto]] ?-present

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://heart.bmj.com}}

{{DEFAULTSORT:Heart (Journal)}}
[[Category:Biweekly journals]]
[[Category:Publications established in 1939]]
[[Category:English-language journals]]
[[Category:Cardiology journals]]
[[Category:BMJ Group academic journals]]
[[Category:Academic journals associated with learned and professional societies]]