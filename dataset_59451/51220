{{primary sources|date=August 2013}}
{{Infobox journal
| title = Environmental Health Perspectives
| editor = Sally Perreault Darney, PhD
| cover = EHP May 2015 Cover.jpg
| caption=EHP May 2016 Cover
| discipline = [[Environmental health]]
| abbreviation =
| publisher = [[National Institute of Environmental Health Sciences]]
| country = United States
| frequency = Monthly
| history = 1972-present
| openaccess = Yes
| license = [[Public domain]]
| impact = 8.44 (2015)
| website = http://ehp03.niehs.nih.gov/home.action
| link1 = http://ehp.niehs.nih.gov/journal-archive/
| link1-name = Online archive
| link2 = http://www.ncbi.nlm.nih.gov/pmc/journals/253/
| link2-name = PubMed Central
| link3 = http://www.jstor.org/journal/envihealpers
| link3-name = JSTOR
| link4 = http://www.editorialmanager.com/ehp/default.aspx
| link4-name = Manuscript submission
| OCLC = 01727134
| LCCN = 76642723
| CODEN = EVHPAZ
| ISSN = 0091-6765
| eISSN = 1552-9924
}}

'''''Environmental Health Perspectives''''' ('''''EHP''''') is a [[peer-reviewed]] journal published monthly with support from the U.S. [[National Institute of Environmental Health Sciences]] (NIEHS). The primary purposes of ''EHP'' are to communicate recent scientific findings and trends in the environmental health sciences; to improve the environmental health knowledge base among researchers, administrators, and policy makers; and to inform the public about important topics in environmental health. For 2015 ''EHP'' has an impact factor of 8.44.<ref name="nih.gov">[http://ehp.niehs.nih.gov/journal-information/ Journal Information, ''EHP'' website. Retrieved 2014-11-10]</ref>

''EHP'' publishes original research, reviews, commentaries, editorials, and news from a wide range of scientific disciplines encompassing basic research, human studies, and ''[[in vitro]]'' and ''[[in vivo]]'' research with a clear relationship to human health effects. The journal also publishes a quarterly Chinese-language edition.<ref name="nih.gov"/>

Research areas that are highly represented in ''EHP'' include [[epidemiology]], [[toxicology]], [[exposure science]], [[risk assessment]], [[public health]], and [[climate change]]. In addition, each issue includes a section devoted to children's health research, and each year children's health is the focus of the October issue. ''EHP'' also addresses ethical, legal, social, and policy issues, as well as studies dealing with ecological issues or effects on wildlife when the relevance of their findings to human health is evident.<ref name="nih.gov"/>

''EHP'' is a fully [[open-access journal]]; its content is available free of charge online. ''EHP'' supports global environmental health research through its policy of open access and commitment to dissemination of research and information to the developing world.<ref name="nih.gov"/>

As a publication of the [[Federal Government of the United States]], ''EHP''<nowiki>'</nowiki>s content is considered [[public domain]], except for images that have been licensed for illustrative purposes.<ref name="nih.gov"/>

''EHP'' charges no submission or publication fees. The journal provides Advance Publication versions of papers upon acceptance and deposits their content into [[PubMed]].<ref name="nih.gov1">[http://ehp.niehs.nih.gov/instructions-to-authors/ Instructions to Authors, ''EHP'' website. Retrieved 2014-11-10]</ref>

Through its presence on [[Twitter]] and [[Facebook]], ''EHP'' publicizes new content, appearances at meetings, and news items of interest to the environmental health community. It also uses these platforms to engage with the public and increase awareness of environmental health issues.

==History==

''EHP'' was founded in 1972 as a vehicle for publishing conference proceedings in the nascent environmental health sciences. Its publisher, the NIEHS, had been established in 1969, and the inception of [[Earth Day]] in 1970 has been called the birth of the modern environmental movement. By the end of 1970, President [[Richard Nixon]] had signed the [[Clean Air Act Extension]], which created regulatory programs governing [[National Ambient Air Quality Standards]] (NAAQS), [[State Implementation Plan]]s (SIPs), [[New Source Performance Standard]]s (NSPS), and [[National Emissions Standards for Hazardous Air Pollutants]] (NESHAPs). In 1972 the Federal Water Pollution Control Amendments, or [[Clean Water Act]], became law. With growing awareness of environmental health issues, the journal was conceptualized by then-NIEHS director [[David Rall]] as a means for rapidly disseminating information on the emerging field.

Over the next 22&nbsp;years the journal published 100 [[monographs]], most of which arose from symposium or conference proceedings. ''EHP'' adopted its current monthly news and research format in 1993. The goal of the new format was to not only continue publishing the best environmental health research but also foster discussion among researchers and to educate the public about environmental health issues.

"Traditionally, laboratory researchers have tended to communicate primarily with each other, and the dissemination of information to the public has been slow and haphazard," original ''EHP'' editors-in-chief Gary E.R. Hook and George Lucier wrote in an editorial introducing the inaugural issue of the reformatted ''EHP''. "It is clear that enhanced communications could contribute to the avoidance of environmental crises through both increased understanding of the underlying science and the identification of potential problems before they become overwhelming, expensive, and perhaps irreversible."<ref>{{cite journal|title=Environmental Health Perspectives: a new beginning|first1=Gary E. R.|last1=Hook|first2=George W.|last2=Lucier|date=1 April 1993|journal=Environmental Health Perspectives|volume=100|pages=317–319|via=PubMed Central|pmc=1519584}}</ref>

The addition of editorials, commentaries, correspondence, and news sections provided the desired forum for discussion of environmental health information. The restructured journal also introduced several new formats for the publication of scientific manuscripts: research articles, brief reports, and research advances.

With the reformatted ''EHP'', symposium and conference proceedings were published separately as ''EHP'' Supplements. This daughter series was discontinued in 2008. Today ''EHP'' publishes occasional special reports, collections, and proceedings.

== Types of articles ==
Currently ''EHP'' publishes peer-reviewed articles in the following formats:<ref name="nih.gov1"/>

*''Commentaries'' (≤ 5,000 words) present information and personal insight on a particular topic.
*''Research articles'' (≤ 7,000 words) report original scientific research and discovery.
*''Substantive reviews'' (≤ 10,000 words) provide an overview, integration of information, and critical analysis of a particular field of research or theme related to environmental health sciences.
*''Quantitative reviews and meta-analyses'' (≤ 10,000 words) present, contrast, and (when appropriate) combine data across studies to address a specific study question related to environmental health.
*''Reviews based on meetings or conferences'' (≤ 10,000 words) should review the state of the science for a particular area, identify research gaps and needs, and explain how the outcome of the meeting or conference addresses those gaps and needs.

==International outreach==

The journal's International Program publishes a bimonthly Chinese-language edition consisting primarily of translated ''EHP'' news, along with original or reprinted editorials, research abstracts from past issues of ''EHP'', and occasional commentaries and reviews.<ref>{{cite web|url=http://ehp.niehs.nih.gov/international/|title=Environmental Health Perspectives   –  International Program|publisher=}}</ref> The Chinese edition premiered in 2001 as a quarterly publication. In 2004 ''EHP'' teamed up with the Shanghai Municipal Center for Disease Control and Prevention to distribute print issues of ''EHP'' Chinese Edition to 30,000 subscribers in China, Taiwan, Hong Kong, and Singapore.

''EHP'' also collaborates with numerous foreign-language journals seeking to increase their publishing capacity, some of which also publish translated ''EHP'' content:

*'''''Annales Africaines de Médicine''''' is a quarterly medical journal published in French by the Université de Kinshasa Faculté de Médecine. The partnership between Annales Africaines de Médecine and ''EHP'' is a result of the African Journal Partnership Project,<ref name="nih.gov2">{{cite web|url=http://ehp.niehs.nih.gov/international/#ajpp|title=Environmental Health Perspectives   –  International Program|publisher=}}</ref> a multinational initiative that focuses on capacity building in Africa.
*'''''Ciência & Saúde Coletiva''''' is a quarterly public health journal published in Brazil by the Associação Brasileira de Pós-Graduação em Saúde Coletiva (Brazilian Association of Public Health).
*'''''Ciencia y Trabajo''''' is a regional Latin American occupational and environmental health journal published by the Fundación Científica y Tecnológica, [[Asociación Chilena de Seguridad]] (Chilean Safety Association; ACHS). Each quarterly edition of Ciencia y Trabajo includes Spanish translations of selected ''EHP'' news articles. ''EHP'' and Ciencia y Trabajo provide reciprocal links on their websites.
*'''''Environment, Risques et Santé''''' is a cross-disciplinary journal published in French by John Libbey Eurotext. The journal publishes articles on health issues relevant to climatology, toxicology, epidemiology, biophysics, earth and water sciences, and radiation, from identifying health risks to examining legislation and standards.
*'''''Mali Médical''''' is a quarterly medical journal published in Mali by the Société de Médecine du Mali. The partnership between Mali Médical and ''EHP'' is a result of the African Journal Partnership Project,<ref name="nih.gov2"/> a multinational initiative that focuses on journal capacity building in Africa.
*'''''Salud Pública de México''''' is a peer-reviewed public health journal published bimonthly by the National Institute of Public Health, Mexico.

==References==
{{Reflist|2}}

[[Category:Environmental social science journals]]
[[Category:Publications established in 1972]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Open access journals]]
[[Category:Environmental health journals]]
[[Category:Academic journals published by the United States government]]