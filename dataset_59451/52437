{{one source|date=December 2013}}
{{Infobox publisher
| image        = [[File:NUSPress logo.gif|120px|NUS Press]]
| parent       = [[National University of Singapore]]
| status       = 
| founded      = 1971
| founder      = 
| successor    = 
| country      = [[Singapore]]
| headquarters = [[Singapore]]
| distribution = Worldwide
| keypeople    = [[Peter Schoppert]]
| publications = 
| topics       = 
| genre        = 
| imprints     = 
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = [http://nuspress.nus.edu.sg nuspress.nus.edu.sg]
}}

'''NUS Press''' is the academic press of the [[National University of Singapore]]. It is organized as a private limited company that is 100% owned by the University. 

==History==
Prior to the independence of Singapore in 1965, the [[University of Malaya]] in Singapore (founded 1949) published books under the imprint of the [[University of Malaya Press]]. When the two divisions of the University of Malaya separated to become two universities, steps were taken to establish separate university presses. Thus the [[Singapore University Press]] was formally established in 1971.  

In 2002, then NUS President Professor [[Shih Choon Fong]] approved a new investment plan for the university press, a plan which would allow for a modest expansion of the Singapore University Press' output. This would require that the Press focus its efforts on Asia-related social science and humanities, with concentrations on [[Singapore]], [[Southeast Asia]] and [[East Asia]]. Subsequently, Singapore University Press was succeeded by a new NUS Press in 2006, reflecting the name of its parent institution and to align the Press closer to the university's overall branding.

==Mission==
The mission of the NUS Press is to enable the dissemination and creation of knowledge through the publishing of scholarly and academic books; and to empower learning, innovation and enterprise for the Singapore- and Asia-focused global community, as a publisher of authoritative works for the trade and professional markets.<ref>{{cite web|url=http://www.nus.edu.sg/nuspress/aboutus.html |title=National University of Singapore |publisher=Nus.edu.sg |date=2008-12-18 |accessdate=2012-08-16}}</ref>

==Main Subject Areas==
* Anthropology
* Archaeology
* Architecture and Building
* Business
* Economics
* Geography
* History
* Language Learning
* Literature & Linguistics
* Medicine & Life Sciences
* Memoirs
* Politics and International Relations
* Religion
* Sociology
* Visual Arts & Visual Culture

==Book Series==
* Asian Studies Association of Australia (ASAA) - Southeast Asian Publications Series
* Kyoto CSEAS Series on Asian Studies
* Challenges of Agrarian Transition in Southeast Asia
* History of Medicine in Southeast Asia
* IRASEC Studies of Contemporary Southeast Asia
* Studies in Asian Security

==Journals==
*''Asian Bioethics Review''
*''[[China: An International Journal]]'' 
*''The Journal of Burma Studies''
*''The Heritage Journal''

==References==
{{Reflist}}

==External links==
* [http://nuspress.nus.edu.sg NUS Press]
{{National University of Singapore}}
[[Category:National University of Singapore]]
[[Category:University presses of Singapore]]
[[Category:Book publishing companies of Singapore]]
[[Category:Publishing companies established in 1971]]