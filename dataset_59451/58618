{{Infobox journal
| title = Journal of Theoretical Politics
| cover = [[File:Journal of Theoretical Politics front cover image.jpg]]
| editor = Torun Dewan, John W. Patty
| discipline = [[Political Science]]
| former_names = 
| abbreviation = J. Theor. Polit.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 1989-present
| openaccess = 
| license = 
| impact = 0.837
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal200984/title
| link1 = http://jtp.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jtp.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 38526092
| LCCN = 91642994 
| CODEN = JTPOEF
| ISSN = 0951-6298 
| eISSN = 1460-3667
}}
'''''Journal of Theoretical Politics''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[political science]]. It is published by [[Sage Publications]]. It was established in 1989 and the [[editors-in-chief]] are Torun Dewan ([[London School of Economics]]) and John W. Patty ([[University of Chicago]]).

== Abstracting and Indexing ==
The journal is abstracted and indexed in [[Academic Search Premier]], [[International Political Science Abstracts]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.837, ranking it 70th out of 161 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal200984/title}}

[[Category:SAGE Publications academic journals]]
[[Category:Quarterly journals]]
[[Category:Political science journals]]
[[Category:Publications established in 1989]]
[[Category:English-language journals]]