{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{{Infobox Locomotive
|name = Brush Type 4<br />British Rail Class 47
|powertype = Diesel-electric
|image = 47424 47607 Bangor1987.jpg
|caption = Two Class 47s, Nos. 47424 and 47607, at [[Bangor (Gwynedd) railway station|Bangor station]] with a passenger train in 1987
|fleetnumbers = D1500–D1999, D1100–D1111<br />later 47&nbsp;001–47&nbsp;981
|totalproduction = 512
|builder = [[Brush Traction]]<br />[[British Rail]] [[Crewe Works]]
|builddate = 1962–1968
|gauge = {{RailGauge|ussg|allk=on}}
|primemover = [[Sulzer (manufacturer)|Sulzer]] 12LDA28-C
|generator = 
|tractionmotors = 
|whytetype = Co-Co
|aarwheels = C-C<!-- For our North American friends -->
|uicclass = [[Co-Co locomotives|Co'Co']]
|wheeldiameter = {{convert|3|ft|9|in|m|3|abbr=on}}
|minimumcurve = 
|trainbrakes  = [[Vacuum brake|Vacuum]], [[Railway airbrake|Air]], or [[Vacuum brake#Dual brakes|Dual]]
|locobrakeforce = {{convert|61|LTf|kN|sigfig=3|abbr=on|lk=in}}
|wheelbase = {{convert|51|ft|6|in|m|2|abbr=on}}
|length    = {{convert|63|ft|7|in|m|2|abbr=on}}
|width     = {{convert|8|ft|10|in|m|2|abbr=on}}
|height    = {{convert|12|ft|9+1/2|in|m|2|abbr=on}}
|locoweight = {{convert|112|LT|t ST|sigfig=3|lk=on}} to {{convert|125|LT|t ST|sigfig=3}}
|maxspeed = {{convert|75|mph|abbr=on}} or {{convert|95|mph|abbr=on}}
|poweroutput = ''Engine:''  originally {{convert|2750|bhp|abbr=on}}, later derated to {{convert|2580|bhp|abbr=on}}
|tractiveeffort = ''Maximum:'' {{convert|55000|lbf|kN|0|abbr=on}} to {{convert|60000|lbf|kN|0|abbr=on}}
|fuelcap = {{convert|850|impgal|abbr=on}}
|trainheating = ''47/0:'' [[Steam generator (railroad)|Steam generator]]<br />''47/3:'' None<br />''47/4:'' [[Head end power|Electric Train Heat]]
|multipleworking = Not fitted when built. Some now retrofitted with <span style="color:green;">&#x25cf;</span> Green Circle
|axleloadclass = [[Route availability]] 6 or 7
|operator = [[British Rail]]<br />[[DB Cargo UK|English Welsh & Scottish]]<br />[[Colas Rail]]<br />[[Direct Rail Services]]<br />[[Freightliner Group|Freightliner]]<br />[[Harry Needle Railroad Company]]<br />[[Riviera Trains]]<br />[[West Coast Railway Company]]
}}

The '''British Rail Class 47''' is a class of British railway [[diesel-electric locomotive]] that was developed in the 1960s by [[Brush Traction]]. A total of 512 Class 47s were built at [[Crewe Works]] and Brush's Falcon Works, [[Loughborough]] between 1962 and 1968, which made them the most numerous class of British mainline diesel locomotive.

They were fitted with the [[Sulzer (manufacturer)|Sulzer]] 12LDA28C  [[U engine|twin-bank]] twelve-cylinder unit producing {{convert|2750|bhp|abbr=on|lk=in}} - though this was later derated to {{convert|2580|bhp|abbr=on}} to improve reliability - and have been used on both passenger and freight trains on Britain's railways for over 50 years. Despite the introduction of more modern types of traction, a significant number are still in use, both on the mainline and on [[heritage railway]]s.  As of October 2016, 81 locomotives still exist as Class 47s, with further examples having been converted to other classes; 30 retain "operational status" on the mainline.

==Origins==
The Class 47 history begins in the early 1960s with the stated aim of the [[British Transport Commission]] (BTC) to completely remove [[steam locomotive]]s from [[British Rail]] by a target date of 1968.<ref>The railways archive - Retrieved on 2007-06-15{{full citation needed|date=January 2013}}</ref> They therefore required a large build of lightweight [[List of British Rail power classifications|Type 4]] locomotives to achieve this aim.  This required locomotives producing at least {{convert|2500|bhp|abbr=on}} but with an [[axle load]] of no more than {{convert|19|LT|t}}.  However, the BTC were not convinced that the future of diesel traction lay down the [[Diesel-hydraulic#Diesel-hydraulic|hydraulic transmission]] path of the [[Western Region of British Railways|Western Region]], and began looking at various [[Diesel-electric train|diesel-electric]] designs.

Despite the construction of two demonstration locomotives ([[British Rail D0260|''D0260 LION'']], produced by [[British Thomson-Houston|AEI]] and [[Birmingham Railway Carriage and Wagon Company|BRC&W]] using a [[Sulzer (manufacturer)|Sulzer]] engine,<ref>[http://www.class47.co.uk/c47_feature_425.php Feature on D0260 LION] Class47.com - Retrieved on 2007-05-31</ref> and [[British Rail Class 53|''D0280 FALCON'']], built by [[Brush Traction]] using [[Maybach]] engines),<ref>[http://www.class47.co.uk/c47_feature_424.php Feature on D0280 FALCON] Class47.com - Retrieved on 2007-05-31</ref> the need for a large number of locomotives quickly was deemed paramount, and the pilot build of what would become Class 47 began before the prototypes could be comprehensively assessed.<ref>[http://www.class47.co.uk/c47_features.php Introduction to Prototypes] Class47.com Retrieved on 2007-05-31</ref>  This initial build of 20 locomotives (Nos. D1500 to D1519) were mechanically different from the remainder of the type,<ref>[http://glostransporthistory.visit-gloucestershire.co.uk/RODClass47.htm Class 47 history] Gloucester Transport History - Retrieved on 2007-06-04</ref> and would be withdrawn earlier.  However, based on these and the success of ''LION'', an order for 270 locomotives was made, which was later revised upwards a number of times to reach the final total of 512.  Five locomotives, Nos. D1702 to D1706, were fitted with a Sulzer [[V12 engine|V12]] 12LVA24 power unit and classified as [[British Rail Class 48|Class 48s]]; the experiment was not deemed a success, and they were later converted to standard 47s.

==In service==
Eventually, 310 locomotives were constructed by Brush in Loughborough, and the remaining 202 at BR's Crewe Works.<ref>[http://www.semgonline.com/diesel/class47_2.html Class 47 History] SEMG - Retrieved on 2007-06-01</ref>  The first 500 locomotives were numbered sequentially from D1500 to D1999, with the remaining twelve being numbered from D1100 to D1111.  The locomotives went to work on passenger and freight duties on all regions of British Rail.  Large numbers went to replace steam locomotives, especially on express passenger duties.<ref>[http://www.gwsr.com/html/47376.html Class 47 History] {{webarchive |url=https://web.archive.org/web/20070614195024/http://www.gwsr.com/html/47376.html |date=14 June 2007 }} GSWR - Retrieved on 2007-06-04</ref>

The locomotives, bar a batch of 81 built for freight duties, were all fitted with [[Steam generator (railroad)|steam heating boilers]] for train heat duties.  The initial batch of twenty, plus D1960 and D1961, were also fitted with [[Electric train supply|electric train heating (ETH)]].<ref>[http://www.semgonline.com/diesel/class47_3.html Early diesel locomotives] SEMG - Retrieved on 2007-06-01</ref>  With this type of heating becoming standard, a further large number of locomotives were later fitted with this equipment.

In the mid 1960s, it was decided to [[Derating|de-rate]] the engine output of the fleet from {{convert|2750|bhp|abbr=on}} to {{convert|2580|bhp|abbr=on}}.<ref>[http://rail.felgall.com/c47.htm Class 47 history] Felgall.com - Retrieved on 2007-06-01</ref>  This significantly improved reliability by reducing stresses on the power plant, whilst not causing a noticeable reduction in performance.<ref name="mod">[http://www.therailwaycentre.com/Pages%20Loco/Recognition%20loco/Illus_47A.html Locomotive modifications] The Railway Centre - Retrieved on 2007-06-04</ref>

==Sub-Classes==
In the early 1970s, the fleet was renumbered into the 47xxx series to conform with the computerised [[TOPS]] systems.  This enabled a number of easily recognisable sub-classes to be created, depending on the differing equipment fitted.  The original series were based on train heating capability and were as follows;<ref>[http://www.therailwaycentre.com/New%20Loco%20Tech%20Data/Class47.html Class 47 numbering] The Railway Centre - Retrieved on 2007-05-31</ref>

* Class 47/0: Locomotives with steam heating equipment.<ref name="Toms66-67">{{harvnb|Toms|1978|pages=66–67}}</ref>
* Class 47/3: Locomotives with no train heating.<ref name="Toms66-67" />
* Class 47/4: Locomotives with dual or electric train heating.<ref name="Toms66-67" />

However, this numbering system was later disrupted as locomotives were fitted with extra equipment and were renumbered into other sub-classes.<ref>[http://www.thejunction.org.uk/cl47.html Class 47 sub-classes] The Junction - Retrieved on 2007-06-04</ref><ref name="numbers">[http://www.class47.co.uk/c47_numbers.php Class47.com numbering] Retrieved on 2007-06-14</ref>  For an overview of  the renumbering see the [[Class 47 renumbering]] page.  This section summarises the main sub-classes that were created.
{{Clear}}
[[File:47293 York 1987.jpg|thumb|right|Class 47/0 No. 47&nbsp;293 with a relief passenger train at [[York railway station|York station]] in 1987]]

===Class 47/0===
Originally numbered from 47&nbsp;001 to 47&nbsp;298, these locomotives were the "basic" Class 47 with steam heating equipment fitted.<ref>[http://www.class47.co.uk/c47_numbers.php?index=0 Class 47/0] Class47.com -Retrieved on 2007-06-15</ref>  In the 1970s and 1980s, with steam heating of trains gradually being phased out, all locomotives fitted with the equipment gradually had their steam heating boilers removed.  Some were fitted with ETH and became 47/4s, whilst the others remained with no train heating capability and were therefore used mainly on freight work.   In the 1990s, the class designation 47/2 was applied to some class 47/0s and class 47/3s after they were fitted with [[multiple working]] equipment.<ref name="mod" />  The locomotives involved also had their [[Vacuum brake|vacuum braking]] systems removed or isolated, leaving them [[Air brake (rail)|air braked]] only. This was mainly a paper exercise, however, and the locomotives were not renumbered; in this article they are included in Class 47/0.
{{Clear}}
[[File:47376 at Toddington.JPG|thumb|left|Class 47/3 No. 47&nbsp;376 in [[Freightliner Group|Freightliner]] livery, at [[Toddington railway station|Toddington station]]]]

===Class 47/3===
Originally numbered from 47&nbsp;301 to 47&nbsp;381, this sub-class was originally built with no train heating equipment and therefore remained as freight locomotives almost exclusively for their working lives.<ref>[http://www.class47.co.uk/c47_numbers.php?index=1 Class 47/3] Class47.com -Retrieved on 2007-06-15</ref>  They were all fitted with [[Slow speed control#Locomotive control|slow speed control]] for working [[Merry-go-round train|MGR coal trains]] (as were a number of Class 47/0s).<ref>[http://www.c58lg.co.uk/html/mgr_haa_history.html Slow speed control] {{webarchive |url=https://web.archive.org/web/20080724000000/http://www.c58lg.co.uk/html/mgr_haa_history.html |date=24 July 2008 }} Class 58 Loco Group - Retrieved on 2007-06-15</ref>  However, during the summer months when train heat was not required, 47/3s could regularly be found hauling the extra trains that the holiday season brought.<ref>{{cite book | last = Lund | first = E | title = To the last drop | publisher = Longden technical Publications | year = 1980 | location = Chesterfield | isbn = 0-9507063-0-2}}</ref> The sub-type remained stable until withdrawals started, although an "extra" 47/3, No.47&nbsp;300, was created in 1992 when No.47&nbsp;468 had its train heating equipment removed and was renumbered.<ref>[http://www.brushtype4.co.uk/bt4_numbers.php?index=1&jndex=0&kndex=0&s_loco=47300 No.47300] {{webarchive |url=https://web.archive.org/web/20090108204356/http://www.brushtype4.co.uk/bt4_numbers.php?index=1&jndex=0&kndex=0&s_loco=47300 |date=8 January 2009 }} Class47.com -Retrieved on 2007-06-15</ref> This was a direct replacement for collision damaged 47&nbsp;343. Also, No.47&nbsp;364 was renumbered to 47&nbsp;981 in 1993 for use on RTC test trains.<ref>[http://www.brushtype4.co.uk/bt4_numbers.php?index=1&jndex=0&kndex=0&s_loco=47981 No.47981] {{webarchive |url=https://web.archive.org/web/20090108152126/http://www.brushtype4.co.uk/bt4_numbers.php?index=1&jndex=0&kndex=0&s_loco=47981 |date=8 January 2009 }} Class47.com -Retrieved on 2007-06-15</ref>
{{Clear}}
[[File:47523 BNS 1988.jpg|thumb|right|Class 47/4 No. 47&nbsp;523 in standard [[Rail Blue|BR Blue]], at [[Birmingham New Street station]] in 1988]]

===Class 47/4===
The designation for standard locomotives fitted with ETH and therefore used for passenger, [[Travelling Post Office|mail]] and parcels use. 133 locomotives had been fitted by the time renumbering occurred, and shortly afterwards the sub-class had settled down to 154 locomotives, numbered 47&nbsp;401-47&nbsp;547 and 47&nbsp;549-47&nbsp;555. Later, further class 47/0s were converted to class 47/4s and renumbered into the series from 47&nbsp;556 onwards, which eventually reached 47&nbsp;665.<ref>[http://www.class47.co.uk/c47_numbers.php?index=2 Class 47/4] Class47.com - Retrieved on 2007-06-15</ref>
{{Clear}}
[[File:47901 Westbury 1987.jpg|thumb|left|Class 47/9 No. 47901 on a [[railtour]] at [[Westbury railway station|Westbury station]] in 1987]]

===Class 47/6 and Class 47/9===
After being severely damaged in a derailment near Peterborough in 1974, locomotive 47&nbsp;046 was selected to be a testbed for the projected [[British Rail Class 56|Class 56]], and was fitted with a 16-cylinder [[Ruston (engine builder)|Ruston]] 16RK3CT engine rated at {{convert|3250|bhp|abbr=on}} for assessment purposes.<ref>{{cite book | last = Williams | first = Alan |author2=Percival, David | title = British Railways Locomotives and Multiple Units including Preserved Locomotives 1977 Combined Volume| publisher = Ian Allan Ltd | year = 1977 | location = Shepperton | isbn = 0-7110-0751-9}}</ref>  To identify it as unique, it was renumbered 47&nbsp;601 (at the time the number range for Class 47s only extended as far as 47&nbsp;555). Later, in 1979, it was used again for the [[British Rail Class 58|Class 58]] project, fitted with a 12-cylinder Ruston engine (this time of {{convert|3300|bhp|abbr=on}}), and renumbered 47&nbsp;901. It continued with this non-standard engine fitted until its withdrawal in 1990.<ref>[http://www.class47.co.uk/c47_numbers.php?index=3 Class 47/6] Class47.com - Retrieved on 2007-06-15</ref>
{{Clear}}
[[File:05.06.82 Haymarket 47707 (6016240566).jpg|thumb|right|British Rail Class 47 47&nbsp;707 with a Glasgow–Edinburgh shuttle at Haymarket in 1982.]]

===Class 47/7===
In the late 1970s, BR authorities identified a need to replace the ageing trains operating the [[Glasgow Queen Street railway station|Glasgow]] to [[Edinburgh Waverley railway station|Edinburgh]] shuttle services, in order to increase speed and reliability. The trains were operated by pairs of [[British Rail Class 27|Class 27]]s, one at each end of this train. It was decided to convert twelve 47/4s to operate the service in [[Push-pull train|push-pull]] mode. The locomotives would be known as Class 47/7 and would be fitted with [[Time-division multiplexing|TDM]] push-pull equipment<ref>{{cite web|title=TDM/RCH specification|url=http://www.rssb.co.uk/SiteCollectionDocuments/rv_coupling_system_data/SD001-%20E024.pdf|work=Mechanical And Electrical Coupling Index|publisher=RSSB}}</ref>  and long-range fuel tanks, and be maintained to operate at {{convert|100|mph|abbr=on}}. The conversions began in 1979 and the service was operated completely by them from 1980. In 1985, the push-pull service spread to Glasgow-[[Aberdeen]] services, and a further four locomotives were converted. The sub-class therefore comprised Nos. 47&nbsp;701 to 47&nbsp;716, though a further locomotive, 47&nbsp;717, was converted in 1988 after the fire-damaged 47&nbsp;713 was withdrawn.<ref>[http://rail.felgall.com/c47.htm Class 47/7] Felgall Rail - Retrieved on 2007-06-15</ref>
[[File:47787 at Rugby.jpg|thumb|left|Class 47/7b No. 47&nbsp;787 parked in a bay platform at [[Rugby railway station|Rugby station]]]]

===Class 47/7b and 47/7c===
In the 1990s, further 47/4s were converted with long-range fuel tanks and equipment to allow them to work with a type of [[rolling stock]] known as [[Propelling Control Vehicle|propelling control vehicles-PCV]], which utilised RCH (Railway Clearing House) cables to allow the PCV driver to signal to the driver on the locomotive to apply power and operate the brakes - neither these locomotives or the PCVs were equipped with  [[Time-division multiplexing|TDM]] push-pull equipment. They were also numbered into the 47/7 series, from 47&nbsp;721 onwards. With dwindling passenger work for them, a number of 47/8s, already fitted with the extra fuel tanks, were also renumbered into this series.<ref>[http://www.class47.co.uk/c47_numbers.php?index=4  Class 47/7] Class47.com - Retrieved on 2007-06-15</ref>

Two locomotives, 47&nbsp;798 ''Prince William'' and 47&nbsp;799 ''Prince Henry'', were dedicated for use on the [[British Royal Train|Royal Train]], and were designated as Class 47/7c.<ref>[http://www.class47.co.uk/c47_numbers.php?index=2 Locomotive pools] Class47.com Retrieved on 2007-07-09</ref>
{{Clear}}
[[File:47839 Canterbury West.jpg|thumb|right|Class 47/4 No. 47&nbsp;839 ''Pegasus'' at [[Canterbury West railway station|Canterbury West station]] in 2007]]

===Class 47/4 (47 8xx series)===
The last of the original 47/4 conversions, from 47&nbsp;650 to 47&nbsp;665, were fitted with extra fuel tanks, giving them an extended range. Four earlier Class 47/4s were also converted. In 1989 it was decided to give these locomotives easy recognisability, and so these locomotives were renumbered into their own series from 47&nbsp;801 to 47&nbsp;820. At the same time, further locomotives were fitted with extra fuel tanks and renumbered; the series eventually reached 47&nbsp;854.<ref name="withdrawal" /> After the [[privatisation of British Rail]], the locomotives in the 47/8 number range were mainly used by [[Virgin Trains]] on [[Cross Country services|cross-country]] work until the introduction of their [[British Rail Class 220|Voyager]] trains. These duties have kept them maintained in serviceable condition, allowing them to remain operational longer than the majority of their classmates. As a consequence most of them  received relatively recent overhauls. The locomotives in this number range are officially Class 47/4s under the TOPS system.{{citation needed|date=June 2013}}
{{Clear}}

==Decline==
By 1986, only five of the original 512 locomotives had been withdrawn from service, all because of serious accident damage.<ref name="withdrawal">[http://www.class47.co.uk/c47_data_437.php?G=gone Class 47 withdrawal data]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Class47.com - Retrieved on 2007-06-04</ref>  However, with work for the class declining due to the introduction of new rolling stock, and spare parts becoming difficult to source, some inroads started being made.

The first locomotives to be targeted were the non-standard pilot batch of 20, now numbered 47&nbsp;401-47&nbsp;420.  Three locomotives were withdrawn as life-expired in February 1986, and the remainder of the batch that had not recently been overhauled followed in the next two years. All 20 were withdrawn by 1992.<ref>[http://www.47401project.co.uk/ 47401 History] The 47401 Project - Retrieved on 2007-06-10</ref>

Meanwhile, BR drew up a 'hit-list' of locomotives for early withdrawal, mainly including those with non-standard electrical equipment, known as ''series parallel'' locomotives.<ref>[http://myweb.tiscali.co.uk/roysrailpage/frame/Details/47.html Class 47 Technical Details] Railfan page - Retrieved on 2007-06-04</ref>  In the outset, withdrawals were slow, mainly due to the surplus of spare parts and new flows of freight traffic which required extra locomotives; only 61 locomotives had been withdrawn by the end of 1992.  However, with the introduction of new locomotives, the rate of withdrawal quickly rose, with 86 more 47s reaching the end of their lives in the next three years.<ref name="withdrawal" />  With most of the non-standard locomotives withdrawn, the reduction of the fleet again proceeded more slowly. The privatisation of British Rail also produced new independent rail companies needing available traction until they could order new locomotives. From 1996 to 2006, an average of around fifteen locomotives per year were taken out of service.<ref name="withdrawal" />

During the decline in passenger work a number of locomotives were painted in "celebrity" colours, depicting various liveries that the type had carried during its history. This continued a tradition of painting 47s in unusual liveries, which dates back to 1977, when [[Stratford TMD|Stratford depot]] in East London painted two locomotives with huge [[Union Flag]]s to celebrate the [[Silver Jubilee of Elizabeth II|Silver Jubilee of Queen Elizabeth II]].<ref>[http://www.brushtype4.co.uk/bt4_liveries.php?s_liv=99 Silver Jubilee livery] {{webarchive |url=https://web.archive.org/web/20090108185026/http://www.brushtype4.co.uk/bt4_liveries.php?s_liv=99 |date=8 January 2009 }} Brushtype4.co.uk, retrieved on 2008-04-30</ref>

==Current mainline operation==
In 2017, after over 55 years of front line passenger and freight operations, 30 locomotives retain operational status on the National network.<ref>{{cite web|title=Fleet status, October 2016|url=http://www.class47.co.uk/c47_data_431.php|accessdate=30 October 2016}}</ref>  The following is a list of companies currently operating Class 47s.
*[[West Coast Railway Company]] is primarily a charter train operator, and expanded its fleet by overhauling withdrawn locomotives. Its ten locomotives currently operational are 47&nbsp;237, 47&nbsp;245, 47&nbsp;746, 47&nbsp;760, 47&nbsp;786, 47&nbsp;802, 47&nbsp;804, 47&nbsp;826, 47&nbsp;851 and 47&nbsp;854.
*[[Rail Operations Group]] is a new rolling stock provider; locomotives currently operational are 47&nbsp;812, 47&nbsp;815, 47&nbsp;843, 47&nbsp;847 and 47&nbsp;848.<ref>[http://www.railmagazine.com/news/network/2016/06/21/rail-operations-group-buys-five-class-47s Rail Operations Group buys five Class 47s] - Rail Magazine. Retrieved on 2017-04-06.</ref>
*[[Colas Rail]] owns a small fleet of locomotives for duties hauling its track maintenance trains and occasional steel traffic. Locomotives currently operational are 47&nbsp;727, 47&nbsp;739 and 47&nbsp;749.
*[[Freightliner Group]], a freight company, operate 47&nbsp;830 (D1645), which was named ''Beeching's Legacy'' on 12 November 2015 to mark 50 years since the first container train ran under British Rail. The naming ceremony was held at the [[National Railway Museum]] at York, with the [[Secretary of State for Transport]], [[Patrick McLoughlin]] in attendance.<ref name="BeechingLegacy">{{cite magazine |title=Freightliner marks 50 years with a naming |magazine=The Railway Magazine |date=2 December 2015 |volume=161 |issue=1,377 |page=7 |publisher=[[Mortons Media Group]] |location=Horncastle, Lincs |issn=0033-8923}}</ref>
* A number of other locomotives are stored in a serviceable condition by these and other operators (currently, 47&nbsp;501 and 47&nbsp;701 fall into this category). Both these and some preserved locomotives that are maintained to mainline standards (such as "Royal Train" locomotive 47&nbsp;798, along with 47&nbsp;270, 47&nbsp;580, 47&nbsp;715 and 47&nbsp;773) may appear on the network at any time. 
* 19 further locomotives, owned by various companies, remain in non-operational "stored" condition. As of October 2016 these are 47&nbsp;194, 47&nbsp;236, 47&nbsp;355, 47&nbsp;368, 47&nbsp;488, 47&nbsp;492, 47&nbsp;500, 47&nbsp;526, 47&nbsp;703, 47&nbsp;714, 47&nbsp;744, 47&nbsp;761, 47&nbsp;768, 47&nbsp;769, 47&nbsp;772, 47&nbsp;776, 47&nbsp;787, 47&nbsp;811 and 47&nbsp;816. Some are used for spares, some are awaiting scrapping and the rest are pending repairs to enable them to return to the main line.

==Other working locomotives==
[[File:01I09I20013 WSR Late Summer Weekend E4.jpg|thumb|left|Preserved D1661 'North Star' at Washford on the West Somerset Railway]]
Class 47s have proved very popular with preservationists and private railways, and 32 are currently in preservation, with the majority in working order.<ref>[http://www.preserved-diesels.co.uk/engines/index_47.htm Class 47 Page] Preserved Diesels - Retrieved on 2007-04-30</ref>  A full list can be found at [[list of preserved British Rail Class 47 locomotives]].

Thirty-three locomotives were rebuilt with [[Electro-Motive Diesel|EMD]] engines and re-classified as [[British Rail Class 57|Class 57s]].<ref>[http://www.thejunction.org.uk/cl57.html Class 57 page] The Junction - Retrieved on 2007-04-30</ref><ref>[http://www.therailwaycentre.com/Resource_data/Class57.pdf Class 57 conversion] The Railway Centre - Retrieved on 2007-04-30</ref> [[Freightliner Group|Freightliner]] took 12, [[Virgin Trains]] 16 and [[Great Western Railway (train operating company)|First Great Western]] five. Today<!--August 2014--> these are owned by [[Direct Rail Services]] (22), [[Great Western Railway (train operating company)|Great Western Railway]] (four) and [[West Coast Railway Company]] (eight, including the prototype passenger class 57, 57 601).
{{clear}}

==Accidents and incidents==
* 11 January 1965: D1734 was severely damaged after [[Coton Hill rail crash|the freight train it was hauling ran out of control]] near [[Shrewsbury railway station|Shrewsbury]], eventually demolishing a [[signal box]]. It was withdrawn two months later, becoming the first Class 47 withdrawn after a working life of only eight months.<ref>[http://www.brushtype4.co.uk/bt4_numbers.php?s_loco=D1734 locomotive D1734] Class47.com - Retrieved on 2007-07-08</ref>
* 17 December 1965: D1671 ''THOR'' was derailed near [[Bridgend railway station|Bridgend]] whilst hauling a train of empty coaches.<ref name=Toms69>{{harvnb|Toms|1978|page=69}}</ref> Shortly afterwards, a freight train collided heavily with the wreckage, killing the drivers of both locomotives.  D1671 was withdrawn some four months later. Its nameplates were salvaged, and transferred to No. D1677.<ref>[http://www.class47.co.uk/c47_numbers.php?index=7&jndex=2&kndex=71&s_loco=1671 locomotive D1671] Class47.com - Retrieved on 2007-07-10</ref>
* 8 April 1969: D1908 was badly damaged when, while hauling a freight train at [[Monmore Green]], it was struck head-on by a passenger train that had passed a signal at danger. D1908 caught fire after the accident and became the third Class 47 withdrawn.<ref>[http://www.class47.co.uk/c47_numbers.php?index=7&jndex=5&kndex=8&s_loco=1908 locomotive D1908] Class47.com - Retrieved on 2007-07-10</ref>
* 13 March 1971: D1562 was wrecked after its power unit, which had been experimentally uprated, exploded at [[Haughley Green|Haughley Junction]] while the locomotive was hauling a [[Liverpool Street station|Liverpool Street]] to [[Norwich railway station|Norwich]] express.<ref>[http://www.class47.co.uk/c47_numbers.php?index=6&jndex=1&kndex=62&s_loco=1562 - Retrieved on 2014-30-11]</ref>
* 11 June 1972: D1630 was involved in the [[Eltham Well Hall rail crash]] in which six people were killed.  The locomotive was repaired, but much later in its life when numbered 47&nbsp;849, it was withdrawn from the Class 57 rebuilding programme after damage was discovered which was thought to have dated back to the accident.<ref>[http://www.class47.co.uk/c47_numbers.php?index=2&jndex=4&kndex=48&s_loco=47849 locomotive D1630] Class47.com - Retrieved on 2007-05-14</ref>
* 25 August 1974: 47&nbsp;236 was hauling a passenger train that passed a signal at danger and was derailed at {{rws|Dorchester West}}. Eighteen people were injured.<ref name=DW>{{cite web |url=http://www.railwaysarchive.co.uk/documents/DoE_Dorchester1974.pdf |title= Report on the Derailment that occurred on 25 August 1974 at Dorchester West |publisher= Her Majesty's Stationery Office |agency= Railway Inspectorate, Department of the Environment |date= 20 November 1975 |accessdate= 21 March 2017}}</ref>
* 16 March 1976: 47&nbsp;274 collided with a lorry that had fallen from a bridge onto the line near [[Eastriggs]]. The drivers of both the train and the lorry were killed.<ref>[http://www.railwaysarchive.co.uk/eventsummary.php?eventID=6422] Railwaysarchive.co.uk - Retrieved on 2015-11-05</ref>
* 5 September 1977: 47&nbsp;402 was hauling a mail train when it was in a head-on collision with a [[diesel multiple unit]] at Farnley Junction, [[Leeds]], [[West Yorkshire]] due to a signalling fault. Two people were killed and fifteen were injured.<ref name=Trevena2>{{cite book |last=Trevena |first=Arthur |title=Trains in Trouble: Vol. 2. |year=1981 |publisher=Atlantic Books |location=Redruth |isbn=0-906899-03-6 |page=47 }}</ref>
* 22 October 1979: 47&nbsp;208 became the fifth Class 47 to be withdrawn after suffering severe damage in a fatal [[Invergowrie rail crash|accident at Invergowrie in Scotland]]. 47&nbsp;208 was hauling a [[Glasgow Queen Street railway station|Glasgow]] to [[Aberdeen railway station|Aberdeen]] service which collided with a local train which had stopped in front.<ref>[http://www.railwaysarchive.co.uk/documents/DoT_Invergowrie1979.pdf Invergowrie accident report] Railwaysarchive.co.uk - Retrieved on 2007-04-30</ref>
* 9 December 1983: 47&nbsp;299 (formerly 47&nbsp;216) was involved in a [[Wrawby Junction rail crash|serious accident at Wrawby Junction]] in [[Lincolnshire]], when whilst hauling an oil train, the locomotive collided with a local train resulting in the death of a passenger. It later emerged that the locomotive's renumbering was allegedly due to a warning given to BR by a clairvoyant who claimed to have foreseen a serious accident involving a locomotive numbered "47216".<ref>[http://www.angelfire.com/mn2/Oubliette/47299.html Jinxed locomotive 47299] Railfan article - Retrieved on 2007-04-30</ref>
* 30 July 1984: 47&nbsp;707 ''Holyrood'' was propelling the 17:30 express from [[Edinburgh Waverley railway station|Edinburgh]] to [[Glasgow Queen Street railway station|Glasgow]] from the rear, when [[Polmont rail crash|the train collided with a cow near Polmont]] and was derailed, resulting in 13 deaths. The accident raised serious concerns about the safety of push-pull operation where the locomotive was at the rear of the train.<ref>[http://danger-ahead.railfan.net/reports/rep2001/selby20010228_rs.html The Polmont accident in the light of 2001 Selby Crash] Danger Ahead - Retrieved on 2007-04-30</ref>
*20 December 1984: [[Summit Tunnel fire]]: Locomotive 47&nbsp;125 was hauling a freight train of petrol tankers which derailed and caught fire in [[Summit Tunnel]], on the [[Lancashire]]/[[West Yorkshire]] border.
*18 January 1986: Locomotive No. 47&nbsp;111 was run into by a [[British Rail Class 104|Class 104]] [[diesel multiple unit]] which had a brake failure and had passed three signals at danger at {{rws|Preston}}. Forty-four people were injured.<ref name=DoT290587>{{cite web |url=http://www.railwaysarchive.co.uk/documents/DoT_Preston1986.pdf |title=Report on the Collision that occurred on 18 January 1986 at Preston |author=Department of Transport |publisher=Her Majesty's Stationary Office |date=29 May 1987 |accessdate=2 April 2017}}</ref>
*9 March 1986: Locomotive No. 47&nbsp;334 was one of two light engines that were hit head-on by a passenger train at [[Chinley]], [[Derbyshire]] due to a signalman's error. One person was killed. Lack of training and a power cut were contributory factors.<ref>{{cite book |last=Vaughan |first=Adrian |title=Obstruction Danger |year=1989 |publisher=Patrick Stephens Limited |location=Wellingborough |isbn=1-85260-055-1 |pages=240–48 }}</ref>
* 20 February 1987: 47&nbsp;089 ''Amazon'' was hauling a freight train that ran away and was derailed by trap points at North Junction, [[Chinley]], [[Derbyshire]]. Another train ran into the wreckage and was derailed.<ref name=Earnshaw7>{{cite book |last=Earnshaw |first=Alan |title=Trains in Trouble: Vol. 7 |year=1991 |publisher=Atlantic Books |location=Penryn |isbn=0-906899-50-8 |page=45 }}</ref>
* 24 March 1987: 47&nbsp;202 was hauling a freight train that overran signals and was in a head-on collision with a passenger train (hauled by [[British Rail Class 33|33 032]]) at Frome North Junction, [[Somerset]]. Fifteen people were injured, some seriously.<ref>{{cite book |title=Tracks to Disaster |first=Adrian |last=Vaughan |publisher=Ian Allan |location=Hersham |year=2003 |origyear=2000 |isbn=0 7110 2985 7 |pages=10–11 }}</ref><ref name=Frome>{{cite web |url=http://www.railwaysarchive.co.uk/documents/DoT_Frome1987.pdf |title=Report on the Collision that occurred on 24th March 1987 at Frome |publisher=Her Majesty's Stationery Office |author=Department of  Transport |date=6 May 1988 |accessdate=21 March 2017}}</ref>

==Gallery of liveries==
<gallery>
File:47851 Traction Magazine.JPG|Class 47/4 No.47&nbsp;851 in a "heritage" livery of the original BR two-tone green with full yellow cabs
Image:47316 'Cam Peak' at Doncaster Works.JPG|Class 47/3 No. 47&nbsp;316 'Cam Peak', carrying the livery of [[Cotswold Rail]]
Image:Anglia_47_Norwich.JPG|Class 47/7a No.47&nbsp;714 in [[Anglia Railways|Anglia]] livery
Image:47100 Blue SF roof.jpg|Class 47/0 No.47&nbsp;100 in the modified [[Rail Blue]] livery with a silver roof, used by [[Stratford TMD]]
Image:47702 Scotrail livery.jpg|Class 47/7a No.47&nbsp;702 in [[ScotRail (British Rail)|ScotRail]] livery
Image:47588 2toneRF livery.jpg|Class 47/4 No.47&nbsp;588 in "triple-grey" [[Railfreight Distribution]] livery
Image:47487 Original IC.jpg|Class 47/4 No.47&nbsp;487 in original [[InterCity (British Rail)|InterCity]] livery
Image:47818 at Cambridge.JPG|Class 47/4 No. 47&nbsp;818, in [['one']] livery at [[Cambridge railway station|Cambridge station]] in 2004
File:47237 & 57005, Derby.JPG|Class 47/0 No. 47&nbsp;237, in [[Advenza Freight]] livery and [[British Rail Class 57|Class 57]] No. 57&nbsp;005 in [[Freightliner Group|Freightliner]] livery at [[Derby railway station]] in 2008
File:47145 Barrow Hill.JPG|Class 47/0 No. 47&nbsp;145 in [[Railfreight Distribution]] non-standard blue livery operated by [[Fragonset Railways]] at this time. This loco has now been scrapped.
File:47375 Barrow Hill.JPG|Class 47/3 No. 47&nbsp;375 in [[British Rail|BR]] [[Rail Blue]] livery at [[Barrow Hill Engine Shed]] in June 2010
File:47292 Ruddington.JPG|Class 47/0 No. 47&nbsp;292 in a non-standard variation of [[British Railways|BR]] [[Rail Blue]] at the [[Great Central Railway (preserved)]] in July 2010
File:WCRC 47.JPG|A [[West Coast Railway Company]] Class 47 on the [[East Coast Main Line]] south of {{Stnlnk|Retford}}
File:47715 Poseidon British Rail Class 47.7a locomotive - National Railway Museum - York - 2005-10-15.jpg|Preserved 47&nbsp;715 ''Poseidon'' at the [[National Railway Museum]] in 2005
</gallery>

==Cuba==
Between 1963 and 1966 ten locomotives similar to the British Rail Class 47 were supplied to [[Ferrocarriles de Cuba]] ([[Cuba]]n National Railways).<ref>{{cite web |url=http://www.class47.co.uk/c47_feature_423.php |title=Cuba |publisher=Class47.co.uk |date=1965-07-30 |accessdate=2012-08-15}}</ref>

==See also==
{{Portal|UK Railways}}
*[[List of British Rail modern traction locomotive classes]]

==References and sources==

===References===
{{Reflist|2}}

===Sources===
*{{cite book | last = Stevens-Stratten | first = S.W. |author2=Carter, R.S. | title = British Rail Main-Line Diesels | publisher = Ian Allan Ltd | year = 1978 | location = Shepperton | isbn = 0-7110-0617-2}}
*{{cite book|title=Brush Diesel Locomotives, 1940-78|first=George|last=Toms|publisher=Turntable Publications|location=Sheffield|year=1978|isbn=0902844482|oclc=11213057|ref=harv}}

==Further reading==
*{{cite book|title=Looking back at Class 47 Locomotives|first=Kevin|last=Derrick|publisher=Strathwood|year=2013|isbn=9781905276233}}
*{{cite book|title=Class 47: 50 Years of Locomotive History|first=Simon|last=Lilley|publisher=OPC|year=2012|isbn=9780860936480|oclc=794815314}}
*{{cite book|title=Class 47 Photo File|first=Martin|last=Loader|publisher=Vanguard Publications|year=1998|isbn=9781900872041|oclc=650096345}}
*{{cite book |last=McManus |first=Michael |title=Ultimate Allocations, British Railways Locomotives 1948 - 1968 |publisher=Wirral. Michael McManus }}
*{{cite book|title=Class 47/8s|first=Nick|last=Meskell|publisher=Train Crazy Publishing|year=2006|isbn=9780954803551|oclc=190776405}}
*{{cite book|title=Profile of the 47s|first=Brian|last=Morrison|publisher=OPC|year=1987|isbn=9780860932406|oclc=16924495}}
*{{cite book|title=BR Brush Class 47: 48 Years of Different Livieries|first=Gavin|last=Morrison|publisher=Book Law Publications|year=2011|isbn=9781907094668|oclc=751709977}}
*{{cite book|title=Class 47s|first=Gavin|last=Morrison|publisher=Ian Allan|year=1999|isbn=9780711026773|oclc=41503765}}
*{{cite book|title=Brush-Sulzer Class 47 Diesel-Electrics|first=Brian|last=Ringer|publisher=Bradford Barton|year=1979|isbn=9780851533261|oclc=16431685}}
*{{cite book|title=Class 47 Diesels|first1=A.T.H.|last1=Tayler|first2=W.G.F.|last2=Thorley|first3=T.J.|last3=Hill|publisher=Ian Allan|year=1979|isbn=9780711009158|oclc=6425727}}
*{{cite book|title=Class 47 and 57 Locomotives|first=Ross|last=Taylor|publisher=Amberley Publishing|year=2016|isbn=9781445658636|oclc= 	934603465}}
*{{cite book|title=Diesel Retrospective: Class 47|first=John|last=Vaughan|publisher=Ian Allan|year=2007|isbn=9780711032019|oclc=163322802}}
*{{cite book|title=British Rail Class 47s|publisher=Peter Watts|year=1980|isbn=9780906025130|oclc=655703332}}
*{{cite magazine|title=Big Brushes that swept clean!|first=Fred|last=Kerr|pages=34-39|date=August 1983||magazine=[[RAIL (magazine)|Rail Enthusiast]]|publisher=EMAP National Publications|issn=0262-561X|oclc=49957965}}
*{{cite magazine|title=The big Brushes - part two|first=Fred|last=Kerr|pages=10-14|date=October 1983||magazine=[[RAIL (magazine)|Rail Enthusiast]]|publisher=EMAP National Publications|issn=0262-561X|oclc=49957965}}

==External links==
{{commons category|British Rail Class 47}}
*[http://www.class47preservationproject.co.uk/ Class 47 Preservation Project]
*[http://www.stratford47group.co.uk/ Stratford 47 Group]

{{Brush Traction}}
{{British Rail Locomotives}}
{{good article}}

[[Category:British Rail diesel locomotives|47]]
[[Category:Brush Traction locomotives]]
[[Category:Co-Co locomotives]]
[[Category:Railway locomotives introduced in 1962]]
[[Category:Standard gauge locomotives of Great Britain]]