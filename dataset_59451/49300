{{Multiple issues|
{{cleanup|reason=Promtional material needs to be removed and all her books need to be removed from refs.|date=January 2016}}
{{BLP primary sources|date=March 2012}}
{{Autobiography|date=October 2016}}
}}

{{Infobox officeholder
|honorific-prefix =
| name = Lillian Glass
| image =
| imagesize =
| caption =
| birth_place = [[Miami]], [[Florida]]
| occupation = interpersonal communication and body language expert, media commentator, litigation consultant
| website = http://www.drlillianglass.com/}}

'''Lillian Glass''' is an [[United States|American]] interpersonal communication and body language expert, media commentator, litigation consultant, and author.  Her books include ''Toxic People'', ''Talk to Win'', ''Say It Right'', ''He Says She Says'', and several books on body language.

==Education==
Glass received a bachelor of science degree from [[Bradley University]]<ref>[http://www.bradley.edu/about/recognition/centurion/  Bradley University Recognition]</ref> in Speech and Hearing Sciences, where she was named one of ''[[Glamour Magazine]]'''s Top 10 College Women.<ref>{{cite web|url=http://www.thecrimson.com/article/1999/9/22/hochman-named-to-glamour-list-while/ |title=The Harvard Crimson: Hochman Named to Glamour List |publisher=Thecrimson.com |date=1999-09-22 |accessdate=2011-01-13}}</ref> She earned her master of science degree from the [[University of Michigan]], where she became interested in gender differences in communication and the study of cranial and dental- facial abnormalities.<ref>{{cite journal | last1 = Glass | first1 = Lillian | last2 = Knapp | first2 = John | last3 = Bloomer | first3 = H. Harlan | year = 1977 | title = Lingual behavior Before and After Manibular Osteotomy | journal = Journal of Oral Surgery | volume = 35 | issue = 2| pages = 104–109 | pmid = 264502 }}</ref>

At age 24, she received a doctorate degree from the [[University of Minnesota]] and was awarded a [[Bush Foundation]] fellowship.{{citation needed|date=October 2016}} She majored in communication disorders, with an emphasis on speech and hearing sciences, and minored in clinical genetics.  Because of her interest in psychology, her doctoral dissertation focused on Psychosocial Perceptions of Speech and Cosmetic Appearance of Patients with Craniofacial Abnormalities.<ref>{{cite journal | last1 = Glass | first1 = Lillian | last2 = Starr | first2 = Clark D. | year = 1979 | title = A Study of Relationships Between Judgments of Speech and Appearance of Patients With Orofacial Clefts | journal = The Cleft Palate Journal | volume = 16 | issue = 4| pages = 436–440 | pmid = 290434 }}</ref>

She received a post doctorate in Medical Genetics at [[UCLA]] School of Medicine where she received a National Institutes of Health (NIH) Grant and did research on speech and hearing problems of patients with a variety of genetic diseases, including those with neurological and skeletal problems. Glass published her research in various professional journals, including the ''[[New England Journal of Medicine]]'' where she described distinct voice patterns in patients with neurofibromatosis.<ref>{{cite journal | doi = 10.1056/NEJM198112313052704 | last1 = Glass | first1 = Lillian | last2 = Riccardi | first2 = Vincent M. | year = 1981 | title = Speech and Von Recklinghausen Neurofibromatosis | journal = New England Journal of Medicine | volume = 305 | issue = 27| pages = 1617–1626 }}</ref> She also discovered a genetic syndrome involving deafness and a dental abnormality, described as the Glass-Gorlin Syndrome (oligondtia and sensorineural deafness).<ref>Glass. Lillian and Gorlin, Robert J. Congenital Profound Sensorineural Deafness and Oligodontia--A New Syndrome ""The Glass Gorlin Syndrome"" ''Archives in Otolaryngology'', 1980</ref><ref>{{cite web|url=http://www.wrongdiagnosis.com/medical/glass_gorlin_syndrome.htm |title=Glass-Gorlin syndrome |publisher=WrongDiagnosis.com |date=2010-11-18 |accessdate=2011-01-13}}</ref>

In 2013, Glass attended [[Pepperdine University]]'s School of Law Straus Institute for Dispute Resolution, where she studied mediation.<ref name="smashwords.com">https://www.smashwords.com/profile/view/drlillianglass</ref>

==Career==

===Academics===
Upon completing her post doctoral training at [[UCLA School of Medicine]], Glass became an Associate Professor at the [[University of Southern California]] (USC). She held joint appointments with the School of Medicine,<ref name="news.google.com">"Quiet Please", Family Weekly, 1981, 4.26.  https://news.google.com/newspapers?nid=1291&dat=19810426&id=mQJVAAAAIBAJ&sjid=HJQDAAAAIBAJ&pg=5676,5346876</ref>   Department of Medical Genetics, School of Dentistry, and Department of Speech Communication and was a researcher at the [[University of Southern California]] Speech Research Lab.<ref>150.	Peacock, Jill. Dr.Lillian Glass Speech Pathologist Works With all Types of Problems. Associated Press. (Herald- Journal). 1981, 12.13.</ref>

===Private practice===
Glass began her private practice in [[Beverly Hills, California]], treating patients with speech and hearing difficulties. Her practice progressed to training actors for movie roles.  One of her first clients was [[Dustin Hoffman]], whom she helped train to talk like a woman for his role in the film ''[[Tootsie]]''.<ref>https://www.nytimes.com/1984/03/19/style/relationships-the-sexes-differences-in-speech.html</ref> Subsequently, she helped other actors including [[Sean Connery]].<ref>https://www.newspapers.com/newspage/113554074/</ref> She also coached deaf actress [[Marlee Matlin]], in speaking publicly at the Academy Award Ceremonies and in film roles.,<ref>{{cite web|url=http://webcache.googleusercontent.com/search?q=cache:ZXXuUoHhm-UJ:articles.chicagotribune.com/1989-04-09/entertainment/8904020394_1_jack-jason-deaf-actress-sarah-norman/2+marlee+matlin+lIllian+Glass&cd=9&hl=en&ct=clnk&gl=us&client=firefox-a |title=Silence Presents No Barriers For Marlee Matlin - Page 2 - Chicago Tribune |publisher=Webcache.googleusercontent.com |date=1989-04-09 |accessdate=2011-01-13}}</ref> and [[Rob Pilatus]] and [[Fabrice Morvan]] - known as [[Milli Vanilli]] - during their  press conference when they gave back their Grammy for lip synching.<ref>http://articles.orlandosentinel.com/1991-01-26/lifestyle/9101250473_1_milli-glass-dustin-hoffman</ref>  Transgender [[Caitlyn Jenner]] also worked with Glass to learn how to feminize her voice and body language as she transitioned from being [[Bruce Jenner]].<ref>http://radaronline.com/exclusives/2015/08/caitlyn-jenner-surgery-vocal-cord-operation/</ref>

Glass has lectured on the topic of communication and body language.<ref>[http://www.aegis.com/successful-body-language-training-human-deception-detection-course/ "A Very Successful Body Language Training & Human Deception Detection Course"], Zisner, Jeff, ''Aegis News''. November 23, 2013</ref>

===Media===
Glass began her television career as a co-host on ''Alive and Well'' on [[USA Network]]{{citation needed|date=October 2016}} and as a psychology reporter for the ABC affiliate in [[Los Angeles]], [[KABC-TV|KABC]] Television. She has since appeared on a variety news and talk shows, including [[Good Morning America]] and the [[Today (U.S. TV program)|Today Show]].<ref>{{cite web|url=http://www.tvguide.com/celebrities/lillian-glass/credits/252800 |title=Lillian Glass Credits |work=TV Guide |accessdate=2011-01-13}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=dcRWsVAjWqM |title=Dr. Lillian Glass on the Today Show |publisher=YouTube |accessdate=2011-01-13}}</ref>  
She has appeared as a body language expert on [[HLN (TV channel)|HLN]]'s ''Nancy Grace Show'',<ref>{{cite web|url=https://www.youtube.com/watch?v=Ph85Udl8vqQ |title=Dr. Lillian Glass Nancy Grace re Kyron Horman's stepmother's affair.mp4 |publisher=YouTube |date=2010-06-04 |accessdate=2011-01-13}}</ref>  [[MSNBC]],<ref>{{cite web|url=https://www.youtube.com/watch?v=o5yym2dY0t8 |title=Dr. Lillian Glass on msnbc discussing why so many politicians cheat on their wives |publisher=YouTube |accessdate=2011-01-13}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=ZVL7ZRr2LMA |title=Dr. Lillian Glass on msnbc re White House Party Crashers.wmv |publisher=YouTube |date=2009-12-02 |accessdate=2011-01-13}}</ref>  ''[[Entertainment Tonight]]'',<ref>{{cite web|url=https://www.youtube.com/watch?v=5Y-dgTnl0rg |title=Entertainment Tonight Dr. Lillian Glass Body Language of Jon and Kate |publisher=YouTube |date=2009-10-28 |accessdate=2011-01-13}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=FVjs0OGJqaU |title=Dr. Lillian Glass on ET Bachelor Jake and Vienna Reunion Show .mp4 |publisher=YouTube |accessdate=2011-01-13}}</ref> and others.<ref>{{cite web|url=https://www.youtube.com/watch?v=kPL6lKzVR1o |title=Dr.Lillian Glass The Insider Brad and Angelina on Red Carpet.mp4 |publisher=YouTube |accessdate=2011-01-13}}</ref> and on [[20/20]] <ref>http://abcnews.go.com/2020/video/pants-fire-truth-lying-18320587</ref> Finally, she has appeared on numerous reality shows,<ref>{{cite web|url=http://health.discovery.com/videos/how-to-body-language.html |title=How to...: Body Language : Video : Discovery Health |publisher=Health.discovery.com |date=2006-05-16 |accessdate=2011-01-13}}</ref><ref>[https://www.youtube.com/watch?v=cuHGNAPicIw YouTube: ''Busted and Disgusted'']</ref> and was the body language expert on Season 8 of ''[[Dancing With the Stars]]'',<ref>[https://www.youtube.com/watch?v=PwYsh6QnQCk YouTube: Dancing with the Stars]</ref> [[Bravo (US TV channel)|BRAVO]]'s ''Millionaire Matchmaker'',<ref>{{cite web|url=http://www.bravotv.com/the-millionaire-matchmaker/season-4/opposites-dont-attract |title=The Millionaire Matchmaker Season 4 - Episode 7 - Opposites Don't Attract - Bravo TV Official Site |publisher=Bravotv.com |accessdate=2011-01-13}}</ref> and ''Swift Justice''.<ref>[https://www.youtube.com/watch?v=9lvNq0I2QO4 "YouTube: Swift Justice"]</ref>

Glass has written a monthly body language column for [[Cosmopolitan (magazine)|''Cosmopolitan'' Magazine]].<ref>{{cite web|url=http://www.instantpulp.com/detail/how-to-tell-if-a-guy-is-into-you-lillian-glass-body-language-book-cosmopolitan |title=Struts Problem Report |publisher=Instantpulp.com |accessdate=2011-01-13}}</ref>

Along with her "Dr. Lillian Glass Body Language Blog'", Glass also writes about celebrities and newsmakers in her blog, ''Reading People'', for ''[[Psychology Today]]''.<ref>{{cite web|last=Glass |first=Lillian |url=http://www.psychologytoday.com/blog/reading-people |title=Reading People |publisher=Psychology Today |accessdate=2011-01-13}}</ref> She has also been a contributor to ''[[Women in Crime Ink]]''.

===Legal consultant===
Glass has provided expert testimony regarding body language, speech and vocal forensics and behavioral analysis. She has been retained by attorneys as a jury/litigation consultant, and has done jury selection and witness preparation.  Glass is also a member of the American Society of Trial Consultants and a member of the Pro Bono Committee of the ASTC which provides free jury and trial consulting for pro bono groups.

==Other==

===Honors and awards===
She was selected as "Outstanding Young Graduate" by [[Bradley University]] and inducted into membership into their highest alumni honor society, "The Centurion Society", for alumni who have achieved national and international fame.<ref>http://www.bradley.edu/about/recognition/centurion/</ref>
 
Glass was also honored by ''[[Glamour Magazine]]'' as one of 10 "Outstanding Young Working Women" and  also selected as one of the nation’s nine "Successful Business Women."

===Noise pollution awareness advocacy===
As a USC professor, Glass helped create public awareness via a media campaign, to warn the public against the dangers of  noise pollution.<ref name="news.google.com"/>  She, along with a group of celebrities also advocated the building of acoustical or noise barriers to help mitigate noise pollution in Los Angeles. She was also on the Board of Directors of a consumer advocate group established by the American Speech Language and Hearing Association to help create awareness with regard to noise pollution.

==Controversy==

===Copyright infringement of Glass's materials===
In 2010, Glass won a jury verdict for copyright infringement against Marsha Petrie Sue for having published in 2007 a  a book entitled ''Toxic People''.  Glass had written a book by that same title in 1995, and the 2007 book contained word-for-word copying of Glass's work.<ref>{{cite web|url=http://content.usatoday.com/topics/article/Culture/Television/Programming/Dancing+with+the+Stars/02hV4OJeZDa5g/1 |title=Dancing with the Stars Story Page - ''USA Today''. |work=USA Today |date=2010-12-14 |accessdate=2011-01-13}}</ref>

==Published works==

===Books===
* ''How to Deprogram Your Valley Girl'' 1982 (NY: Workman Publishing). ISBN 0-89480-239-9.
* ''Talk to Win- Six Steps to a Successful Vocal Image'' 1987 (NY: Putman Publishing). ISBN 0399513868.
* ''Confident Conversation'', 1991 (London: Piatkus Publishing). ISBN 0-7499-1081-X (hard cover) . ISBN 0-7499-1085-2 (soft cover).
* ''Say It Right-How to Talk In Any Social or Business Situation'' 1991, 1992 (NY:Perigee Books). ISBN 0399516999.
* ''He Says She Says: Closing the Communication Gap Between the Sexes'' 1992 (NY: GP. Putnam and Sons). ISBN 0-399-51737-5.
* ''Toxic People - 10 Ways of Dealing with People Who Make Your Life Miserable'' 1995 (NY: Simon and Schuster ). ISBN 0-684-80315-1 (hard cover). (NY: St. Martin's Press). ISBN 0-312-15232-9 (soft cover).
* ''Attracting Terrific People- How to Find and Keep the People Who Bring Your Life Joy" New 1997 (NY:St. Martin's Press). ISBN 0-312-15058-X (hard cover). ISBN 0-312-18045-4 (soft cover).
* ''I Know What You're Thinking- Using the Four Codes of Reading People to Improve Your Life'', 2002  (NY: Wiley). ISBN 0-471-38140-3 (hard cover). ISBN 0-471-43029-3 (soft cover).
* ''The Complete Idiot's Guide to Verbal Self Defense'' 1999 (NY: Alpha Books, Macmillan Publishing). ISBN 0-02-862741-5.
* ''The Complete Idiot's Guide to Understanding Men and Women'', 2000 (NY : Alpha Books, Macmillan Publishing). ISBN 0-02-862414-9.
* ''Toxic Men - 10 ways of Identifying, Dealing With and Healing From Men Who Make Your Life Miserable'', 2010 (hard cover) (Massachusetts:Adams Media). ISBN 1-4405-0007-X
*  ''Guide to Identifying Terrorists Through Body Language'',  Lillian Glass PhD,  Vincent Sullivan JD, 2012, (electronic book) (Massachusetts: Adams Media), ASIN B007BQFMSG.
* ''The Body Language Advantage: Maximize Your Personal and Professional  Relationships with this Ultimate Photo Guide to Deciphering What Others Are Secretly Saying,in Any Situation'', 2012 (Massachusetts: Fair Winds Press). ISBN 978-1592335152.	
* ''The Body Language of Liars: From Little White Lies to Pathological Deception - How to See through the Fibs, Frauds, and Falsehoods People Tell You Every Day'', 2013, (Massachusetts: Career Press). ISBN 1601632800.
* ''50 Ways My Dog Made Me Into A Better Person'', 2015, (California:Your Total Image Publishing). ISBN 978-1929873524.
* ''Body Language of Terrorists: Protect Yourself and Those Around You By Spotting Signals That  Identify Potential Threats'', 2015 (California: Your Total Image Publishing). ISBN 978-1929873487.
* ''Bikram Vocal Yoga:Voice Communication and Body Language Skills to Increase Confidence and Enrich Your Life'', 2015 (California: Your Total Image Publishing). ISBN 978-1929873357.

==References==
{{Reflist|30em}}

==External links==
*[http://drlillianglassbodylanguageblog.wordpress.com/ Dr. Lillian Glass body language blog]

{{Authority control}}

{{DEFAULTSORT:Glass, Lillian}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American bloggers]]
[[Category:American columnists]]
[[Category:American television personalities]]
[[Category:American women writers]]
[[Category:Writers from Miami]]
[[Category:University of Minnesota alumni]]
[[Category:University of Michigan alumni]]
[[Category:Women columnists]]
[[Category:Women bloggers]]
[[Category:David Geffen School of Medicine at UCLA alumni]]