{{Use mdy dates|date=May 2016}}
{{Infobox musical artist
| name = Lunice
| image =Lunice 2014.jpg
| caption = Lunice in 2014
| background = non_vocal_instrumentalist
| birth_name = Lunice Fermin Pierre II
| alias = Lunice, TNGHT
| birth_date = {{birth date and age|1988|05|15}}
| birth_place = [[Montréal]], [[Québec]], Canada
| genre = [[Instrumental hip hop]], [[trap music|trap]], [[Bassline (dance music)|bassline]], [[Electronic music|electronic]]
| occupation = Musician, record producer
| associated_acts = [[Azealia Banks]], [[Hudson Mohawke]], [[TNGHT]]
| years_active = 2007–present
| label =LuckyMe Records<br> Warp Records
| website ={{URL|lunice.com/}}
}}
[[File:TNGHT Mezzanine San Francisco 4-20-2013.jpg|thumb|right|240px|TNGHT performing in 2013]]
'''Lunice Fermin Pierre II''' (born 1988), better known by his stage name '''Lunice''', is a producer and DJ from [[Montréal]], Canada.<ref>[http://www.factmag.com/2010/10/01/fact-mix-189-lunice/2/ Fact Mix] ''Fact'' Magazine, October 1, 2010</ref> Lunice also formed the production duo [[TNGHT]] with [[Hudson Mohawke]].

==Biography==

Lunice is a child of [[Filipino people|Filipino]] and [[Haitian people|Haitian]] immigrants.<ref>[http://www.mtviggy.com/interviews/qa-with-canadian-producer-lunice-on-the-philippines-winter-and-his-new-track-bus-stop-jazz/ Interview with Lunice] MTV, December 2010</ref>

Lunice started as a [[B-boying|b-boy]], dancing competitively for the 701 Squad.<ref name="dazeddigital">Teh, Terrence.  [http://www.dazeddigital.com/music/article/3477/1/lunice-exclusive-mix Lunice Exclusive Mix] Dazed Digital, April 2010</ref><ref>Fly, Monk & Powell, Anna.  [http://pulseradio.net/articles/2011/10/lunice  ‘Future Beats B-Boy On Blast] Pulse, October 19, 2011</ref> In the early 2000s, Lunice was inspired to make his own music after hearing how producer [[9th Wonder]] constructed beats on the computer program [[Fruity Loops]].<ref>Drake, David. [http://www.thefader.com/2011/12/21/gen-f-lunice/#ixzz23dqysqWw Gen F] The Fader, December 2011</ref>

In 2007, Lunice played his first gig at Hovatron's monthly Bass Culture in Montréal, where he met [[Sixtoo]], and was invited to perform at the Megasoid parties.  He has been a part of the Turbo Crunk crew ever since.<ref name="dazeddigital" />

In 2008, Lunice released a video for Lazersword's "Gucci Sweatshirt".<ref>[http://www.nme.com/nme-video/youtube/id/or3zUKiJb50 Lunice x LAZERSWORD] ''NME'', retrieved September 12, 2012</ref> In 2010, he participated in the London installment of the [[Red Bull Music Academy]].<ref>[http://www.redbullmusicacademy.com/academy/london2010 Red Bull Music Academy, 2010] Red Bull, 2010</ref> Also in the same year, Lunice performed at the Sonar Music Festival.<ref>[http://www.electronicbeats.net/2011/10/14/interview-lunice-sonar-was-really-the-epic-moment/ Lunice Interview] Electronic Beats, October 14, 2011</ref>

Lunice signed to [[LuckyMe (record label)|LuckyMe]] for the release of two EPs, ''Stacker Upper'' (2010) and ''One Hunned'' (2011), and soon connected with producer [[Diplo (DJ)|Diplo]] and his [[Mad Decent]] label for a remix of [[Deerhunter]]'s "Helicopter",<ref>Breihan, Tom.  [http://pitchfork.com/forkcast/14902-helicopter-diplo-lunice-remix/ “Helicoptor”] Pitchfork, September 29, 2010</ref> which led to further collaborations on remixes, mixes for the likes of [[BBC Radio 1]],<ref>[http://www.bbc.co.uk/programmes/b01g8j8f Radio 1 Mix] BBC Radio 1</ref> and performances with the Mad Decent Block Party.<ref>[http://www.rollingstone.com/music/pictures/mad-decent-block-party-takes-over-brooklyn-20120807/lunice-0143046 Mad Decent Block Party Takes Over Brooklyn] ''Rolling Stone'', August 2012</ref>
In 2011, Lunice collaborated with [[Azealia Banks]] and was featured in her video for the song "212".<ref>Lyons, Patrick. [http://www.thefader.com/2011/10/13/video-azealia-banks-212/ 212] The Fader, October 2011</ref>

Since then, he has done work with such labels as [[XL Recordings]], [[Warner Bros. Records]], Palms Out Sounds, Young Turks, [[Big Dada]], and Top Billin'. Throughout 2010 and 2011, Lunice toured internationally.<ref>[http://lookoutpresents.com/blog/interview-lunice-at-lookout-x-pop-montreal/ Interview: Lunice] Lookout Presents, November 29, 2012</ref>

Lunice and [[Hudson Mohawke]] formed the group TNGHT,<ref>[http://www.scionav.com/artist/#!artist/1515 Artists] Scion AV</ref> for the 2012 release of an EP on [[Warp Records]]/[[LuckyMe (record label)|LuckyMe]],<ref>Caramanica, Jon.  [https://www.nytimes.com/2012/07/29/arts/music/new-releases-from-friends-wade-bowen-and-joey-badass.html New Releases] ''The New York Times'', July 29, 2012</ref> following a successful headlining Warp's SXSW showcase earlier in the year.<ref>[http://www.synthglasgow.com/blog/412/56/Review-TNGHT-Hudson-Mohawke-x-Lunice.html TNGHT] Synth Glasgow, July 2012</ref> The duo met in 2008, when Lunice booked Hudson Mohawke to perform at one of his Turbo Crunk parties.<ref>Dombal, Ryan.  [http://pitchfork.com/features/rising/8868-tnght/ Rising: TNGHT] Pitchfork, June 15, 2012</ref>

Lunice served as a judge for The 13th and 14th Annual [[Independent Music Awards]] in 2014 and 2015, and his contributions helped assist the careers of upcoming independent artists.

==Discography==

===EPs===
*2011: ''Stacker Upper'', LuckyMe
*2011: ''One Hunned'', LuckyMe

===Singles===
;Charting singles
{| class="wikitable"
!align="center" rowspan="2" width="10"|Year
!align="center" rowspan="2" width="170"|Single
!align="center" colspan="1" width="20"|Peak positions
!align="center" rowspan="2" width="70"|Album
|-
! scope="col" style="width:3em;font-size:90%;" | [[Ultratop|BEL <br>(Vl)]]<br><ref name="vl">{{cite web | url=http://www.ultratop.be/nl/showinterpret.asp?interpret=Lunice | title=Discografie Lunice | publisher=Hung Medien | work=''ultratop.be'' |accessdate=}}</ref>
|-
| style="text-align:center;"|2012
| "Higher Ground"<br /><small>(with [[Hudson Mohawke]] as [[TNGHT]])</small>
| style="text-align:center;"|33*<br><small>(Ultratip)</small>
| style="text-align:center;"|
|-
|}
<small>*Did not appear in the official Belgian Ultratop 50 charts, but rather in the bubbling under Ultratip charts.</small>

===Other releases===
*2011: "Get Her High", feat. 2lettaz & DaVinci [Southern Hospitality]
*2011: "Bus Stop Jazz", with The Jealous Guys [Southern Hospitality]
*2012: "Runnin'", Azealia Banks feat. Lunice 
*2014: "Can't Wait To" [LuckyMe]

===Remixes===
*2009: Matt B – "Cars Go Boom" (Lunice Remix) / Made in Glitch
*2009: Xrabit & DMG$ – "Damaged Goods" (Lunice Remix) / [[Big Dada]]
*2009: [[Thunderheist]] – "L.B.G." (Lunice Remix) / [[Big Dada]]
*2009: [[The XX]] – "Basic Space" (Lunice Remix) / Young Turks / [[XL Recordings]]
*2009: Hovatron – "Gold Star Radiation" (Lunice Remix) / Lo-Fi-Fnk
*2009: Diamond K – "Handz in the Air" (Lunice Remix) / Top Billin
*2010: Mexicans with Guns – "Sell Your Soul" (Lunice Remix) / FoF Music
*2010: [[Ryan Leslie]] – "Addiction" (Lunice Remix) / Local Action Records
*2010: [[Deerhunter]] – "Helicopter" (Diplo & Lunice Remix) / [[Mad Decent]]
*2010: Invent & OSTR feat. KRS ONE – "Hey You" (Lunice Remix)
*2010: Radio Radio – "EJ Savais Pas Mieux" (Lunice Remix) / [[Bonsound]]
*2010: Elephant Man – "Shake It" (Lunice Remix) / Truckback Records
*2011: The Touch – "All I Find" (Lunice Remix) / Palms Out Sounds
*2011: [[XV (rapper)|XV]] – "Swervin'" (Lunice Remix) / [[Warner Bros. Records]]
*2011: [[Theophilus London]] feat. Sara Quinn – "W.E.T." (Lunice Remix) / [[Warner Bros. Records]]
*2011: [[Foster the People]] – "Pumped Up Kicks" (Lunice Remix) / [[Mad Decent]]
*2012: [[Flosstradamus]] feat. [[Danny Brown (rapper)|Danny Brown]] – "From the Back" (Lunice Remix) / [[Fool's Gold Records]]
*2013: [[Rockie Fresh]] feat. ''Lunice'' – "[[Superman OG]]" / [[Maybach Music Group]]
*2013: [[Rockie Fresh]] feat. ''Rick Ross & Lunice'' – "Panera Bread" / [[Maybach Music Group]]

===Mixtapes===
*2006: ''Kibbles N Beats''
*2007: ''Bossa – Rendez Vous'' 
*2008: ''LAZERemix Volumeone'' 
*2008: ''Out of Touch''
*2009: ''LAZERemix Volumetwo'' 
*2009: ''DAZED Digital Lunice Exclusive Mix'' 
*2010: ''FACT Mix 189'' – Lunice
*2010: ''BIGUP Magazine Mix''
*2010: ''Loukout Mixtape #5: Lunice "Pretty Boy" Mixxx'' 
*2011: ''SAFEWALLS Mixtape''
*2011: ''Boiler Room Lunice's 60min Mix''

==External links==
* [http://www.lunice.com Lunice.com]
* [https://www.twitter.com/Lunice Lunice on Twitter]
* [http://thisisluckyme.com/music/lunice/ Lunice at LuckyMe Records]
* [http://warp.net/records/tnght/ TNGHT at Warp Records]

==References==
{{reflist}}

{{Authority control}}

[[Category:Canadian electronic musicians]]
[[Category:Canadian hip hop record producers]]
[[Category:Musicians from Montreal]]
[[Category:Black Canadian musicians]]
[[Category:1988 births]]
[[Category:Living people]]
[[Category:Date of birth missing (living people)]]