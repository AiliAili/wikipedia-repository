{{Refimprove|date=June 2013}}
[[File:Marina Mall Abu Dhabi.jpg|thumb|Marina Mall Abu Dhabi, Abu Dhabi's premier shopping destination]]
[[File:Marina Mall Main Entrance.jpg|thumb|Main entrance lit up at night]]
[[File:Marina Mall Fountain.jpg|thumb|Internal steam fountain in main atrium]]
[[File:The Corniche as seen from the Marina Mall, Abu Dhabi, UAE.JPG|thumb|The Corniche as seen from the Marina Mall complex]]

'''Marina Mall''' ([[Arabic]]: '''مركز المارينا''') is an [[Abu Dhabi]] [[shopping mall]] and entertainment venue which opened in March 2001. One of the biggest in Abu Dhabi, the Mall is located along the breakwater on Corniche road, close to the [[Emirates Palace]] hotel. In January 2017, Forbes recognized Marina Mall as one of the top shopping malls in Abu Dhabi.<ref>{{cite web|url=http://www.forbes.com/sites/bishopjordan/2017/01/03/best-shopping-malls-abu-dhabi/ |title=The Five Best Shopping Malls In Abu Dhabi |accessdate=2017-01-09}}</ref>

==Details==
Geographically located in one of the city’s most prominent districts, it comprises 122,000&nbsp;m<sup>2</sup> of retail space featuring a unique selection of fashion, leisure and entertainment brands arranged over four levels, in addition to a 100 metre observatory, a bowling alley, a multiplex movie complex, musical fountains and a Carrefour departmental store.<ref>{{cite web |title=Marina Mall |url=http://visitabudhabi.ae/en/what.to.do/shopping/shopping.malls/marina.mall.aspx |work=Visit Abu Dhabi |publisher=Abu Dhabi Tourism and Culture Authority |accessdate=13 May 2013}}</ref>

==Extensions==
The mall was extended in 2005 and the masterplan included the observatory and other facilities. Another extension is also being considered <ref>http://www.nicuae.ae/en/projects/marina-mall-extension-phase-ll/</ref>

== References ==
{{reflist}}

== External links ==
*{{Official website|http://www.marinamall.ae/}}

{{coord|24.475964|54.321315|display=t|type:landmark}}

{{Shopping malls in the United Arab Emirates}}
[[Category:Shopping malls in Abu Dhabi]]
[[Category:Tourist attractions in Abu Dhabi]]


{{UnitedArabEmirates-stub}}