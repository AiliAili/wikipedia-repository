{{Infobox journal
| title = Multiple Sclerosis Journal
| cover = [[File:Multiple Sclerosis Journal.jpg]]
| editor = Alan J Thompson 
| discipline = [[Multiple sclerosis]]
| former_names = Multiple Sclerosis
| abbreviation = Mult. Scler. J.
| publisher = [[Sage Publications]]
| country = 
| frequency = Monthly
| history = 1995-present
| openaccess = 
| license = 
| impact = 4.822
| impact-year = 2014
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201820
| link1 = http://msj.sagepub.com/content/current
| link1-name = Online access
| link2 = http://msj.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 39932110
| LCCN = 96039038
| CODEN = MUSCFZ
| ISSN = 1352-4585
| eISSN = 1477-0970
}}
The '''''Multiple Sclerosis Journal''''' (formerly '''''Multiple Sclerosis''''') is a monthly [[peer-reviewed]] [[medical journal]] covering the [[clinical neurology]] of [[multiple sclerosis]]. The [[editor-in-chief]] is Alan J. Thompson ([[University College London]]). It was established in 1995 and is published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Biological Abstracts]]
* [[BIOSIS]]
* [[Current Contents]]
* [[EMBASE]]
* [[Excerpta Medica]]
* [[Elsevier BIOBASE]]
* [[Current Awareness in Biological Sciences]]
* [[EMBASE]]
* [[Excerpta Medica]]
* [[Index Medicus]]/[[MEDLINE]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 4.822, ranking it 22 out of 192 journals in the category "Clinical Neurology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Clinical Neurology|title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science}}</ref>

== History ==
{{unsourced|section|date=August 2015}}
The journal was first established in 1995 with Ingrid Allen ([[Queen's University Belfast]]) as founding editor. Three issues appeared in 1995, five in 1996, and from 1997 six issues appeared each year until 2006. In 2007 there were nine issues and in 2009, the current monthly publication was established. Donald Silverberg took over the editorship of the journal. He retired in 2006 and was succeeded by Alan Thompson.

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201820}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Neurology journals]]
[[Category:Publications established in 1995]]
[[Category:Monthly journals]]