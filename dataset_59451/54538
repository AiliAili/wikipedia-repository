{{Infobox book
| name          = ''Clara Vaughan''
| image         = ClaraVaughan cover.jpg
| caption = Cover of 2009 Edition of Clara Vaughan 
| author        = [[R. D. Blackmore]]
| country       = England
| language      = English
| genre         = Sensation novel
| publisher     = [[Macmillan & Co.]]
| pub_date      = [[1864 in literature|1864]] 
| media_type    = Print 
| pages         = 
}}

'''''Clara Vaughan''''' is a sensation novel by [[R. D. Blackmore]], who was later to achieve lasting fame for another Romantic novel, ''[[Lorna Doone]]''. It was written in 1853 and published anonymously in 1864. It was Blackmore's first novel.<ref>"Blackmore" entry in ''Merriam-Webster's Encyclopedia Of Literature'' (1995), Merriam-Webster.</ref> The novel was generally well received by the public, though some reviewers at the time believed it to have been written by [[Mary Elizabeth Braddon]] and criticised its author for not knowing about the law.

==Plot introduction==
''Clara Vaughan'', which takes place in the mid-19th century, is the story of the eponymous heroine, an only child whose father is mysteriously murdered when she is a young girl. As a young woman, she sets out to uncover the identity of her father's killer, and for this reason the novel is often classed among the first detective novels in English. In addition to this overarching theme, there are several subplots involving family secrets, romances, and questions of familial inheritance. 
<!--This should give some idea of the type of novel this article 
is about, the setting, the period and its place in literature. 
Consider not giving information that would be thought of as ''spoilers'' unless that is necessary per [[WP:NPOV]] and [[WP:LEAD]]. -->
<!--==Plot summary==
~Plot outline description~

==Characters in "Clara Vaughan"==
~Describe and possibly link to characters of novel.~

==Major themes==
~thematic description, using the work of literary critics (i.e. scholars)~

==Literary significance and reception==
~description of the work's initial reception and legacy based on the work of literary critics and commentators over 
the years, give citations~
-->

==Publication history==
The novel was first published in 1864 and was still in print in various editions into the early 20th century.

===1872 revision===
Blackmore chose to fully revise the novel for a new edition in 1872. Blackmore states in the preface that he removed "many things offensive to maturer taste and judgement".<ref>[https://archive.org/details/06680807.1797.emory.edu Clara Vaughan] (1889 edition) at the Internet Archive</ref>

==References==
{{reflist}}

==External links==
{{Gutenberg|no=41020|name=Clara Vaughan, Volume I|author=R. D. Blackmore|year=1864}} 
{{Gutenberg|no=41021|name=Clara Vaughan, Volume II|author=R. D. Blackmore|year=1864}} 
{{Gutenberg|no=41022|name=Clara Vaughan, Volume III|author=R. D. Blackmore|year=1864}}
*{{Librivox book|Clara Vaughan |author=R. D. Blackmore}}

{{R. D. Blackmore}}

[[Category:1864 novels]]
[[Category:English novels]]
[[Category:Victorian novels]]
[[Category:Works published anonymously]]
[[Category:Novels about orphans]]
[[Category:Novels by Richard Doddridge Blackmore]]
[[Category:Fictional orphans|Vaughan, Clara]]
[[Category:Fictional characters introduced in 1864|Vaughan, Clara]]


{{1860s-novel-stub}}