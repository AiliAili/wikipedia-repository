{{Infobox journal
| title = Applied Spectroscopy
| cover = 
| editor = [[Michael W. Blades]]
| discipline = [[Spectroscopy]]
| peer-reviewed = 
| former_names = Bulletin of the Society for Applied Spectroscopy
| abbreviation = Appl. Spectrosc.
| publisher = [[Society for Applied Spectroscopy]]
| country = 
| frequency = Monthly
| history = 1946–present
| openaccess = 
| license = 
| impact =2.014 
| impact-year =2015 
| website = http://www.s-a-s.org/4DCGI/cms/review.html?Action=CMS_Document&DocID=28&MenuKey=journal
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 1577663
| LCCN = 56056515
| CODEN = APSPA4
| ISSN = 0003-7028
| eISSN = 1943-3530
}}
'''''Applied Spectroscopy''''' is a [[peer review|peer-reviewed]] [[scientific journal]] published monthly by the [[Society for Applied Spectroscopy]], and it is also the official journal for this society. The [[editor-in-chief]] is [[Michael W. Blades]] ([[The University of British Columbia]]). The journal covers applications of spectroscopy in [[analytical chemistry]], materials science, [[biotechnology]], and chemical characterization.<ref name=masthead>
{{Cite web
 | title = Applied Spectroscopy: About the Journal
 | url = http://www.opticsinfobase.org/as/home.cfm
 | work = OpticsInfobase
 | publisher = [[Optical Society of America]]
 | accessdate = 2011-07-23
}}</ref>

The journal is a continuation of ''Bulletin of the Society for Applied Spectroscopy'' ({{ISSN|0096-8706}}), which was first published in February 1946. This title continued until July 1951. The frequency of this publication varied between 1951 and 1991. Then in 1992 it became a monthly journal.<ref>''Applied Spectroscopy'' @ [[WorldCat]], {{OCLC|1577663}}</ref><ref>''Applied Spectroscopy'' @ [[Library of Congress]], {{LCCN|56056515}}</ref><ref name=loc>''Bulletin of the Society for Applied Spectroscopy'' @ [[Library of Congress]], {{LCCN|sn82001893}}</ref>

== Aims and Scope ==
The journal seeks to be comprehensive in scope, with its primary aim the publication of papers on both the fundamentals and applications of photon-based spectroscopy. These include, but are not limited to, [[Ultraviolet–visible spectroscopy|ultraviolet-visible absorption]], [[Fluorescence spectroscopy|fluorescence]] and phosphorescence, [[Infrared spectroscopy|mid-infrared]], [[Raman spectroscopy|Raman]], [[near-infrared]], [[Terahertz spectroscopy|terahertz]], and [[Microwave spectroscopy|microwave]], and [[Atomic absorption spectroscopy|atomic absorption]], [[Atomic emission spectroscopy|atomic emission]], and [[Laser-induced breakdown spectroscopy|laser-induced breakdown]] spectroscopies (and ICP-MS), as well as cutting-edge hyphenated and interdisciplinary techniques.

Fundamental topics include, but are not restricted to, the theory of optical spectra and their interpretation, instrumentation design, and operational principles. Reports of spectral processing methodologies such as 2D correlation spectroscopy (2D-COS), baseline correction, and chemometric methods applied to spectra are also strongly encouraged.

Application papers are intended to feature novel, innovative applications of spectroscopic methods and techniques. Papers from all fields of scientific endeavor in which applied spectroscopy can be utilized will be considered for publication. Representative fields include chemistry, physics, biological and health sciences, environmental science, materials science, archeology and art conservation, and forensic science.

In addition to full papers, the journal publishes Rapid Communications, Spectroscopic Techniques, Notes, and Correspondence related to previously published papers. A regular feature of the journal, ‘‘[http://s-a-s.org/journal/viewer/focal-point/ Focal Point Reviews],’’ provides definitive, comprehensive reviews of spectroscopic techniques and applications and is available as open access.

==Abstracting and indexing==
This journal is abstracted and indexed in:<ref>[http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0003-7028 Journal Master List: "Applied Spectroscopy"]. [[Thomson Reuters]]. 2011</ref><ref>
{{cite web
 |url=http://www.ebscohost.com/titleLists/a2h-journals.htm
 |title=Magazines and Journals
 |work=Academic Search Alumni Edition
 |publisher=[[EBSCO]]
 |accessdate=2011-07-23
| archiveurl= https://web.archive.org/web/20110623120238/http://ebscohost.com/titleLists/a2h-journals.htm| archivedate= 23 June 2011 <!--DASHBot-->| deadurl= no}}</ref>
*[[Academic Search]]
*[[BIOSIS Previews]]
*[[Chemical Abstracts Service|Chemical Abstracts Service/CASSI]]
*[[Current Contents]]/Physical, Chemical & Earth Sciences
*[[Science Citation Index]]
*[[MEDLINE]]

==References==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.opticsinfobase.org/as/home.cfm}}

[[Category:Spectroscopy]]
[[Category:Chemistry journals]]
[[Category:Materials science journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1946]]