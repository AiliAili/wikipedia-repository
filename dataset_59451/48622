'''Hummers' method''' is a chemical process that can be used to generate [[graphite oxide]] through the addition of [[potassium permanganate]] to a solution of [[graphite]], [[sodium nitrate]], and [[sulfuric acid]]. It is commonly used by engineering and lab technicians as a reliable method of producing quantities of graphite oxide. It is also able to be revised in the creation of a one-molecule-thick version of the substance known as [[graphene oxide]].

== Graphite oxide ==
[[File:Graphite oxide.svg|thumbnail|Molecular model of graphite oxide]]
{{main article|Graphite oxide}}
Graphite oxide is a compound of [[carbon]], [[oxygen]], and [[hydrogen]] where there is a ratio between 2.1 and 2.9 of carbon to oxygen. Graphite oxide is typically a yellowish solid. It is also known as graphene oxide when used to form unimolecular sheets.
{{clear}}

== Method ==
Hummers' method<ref name="Hummers Method">{{cite journal|last1=Hummers|first1=William S.|last2=Offeman|first2=Richard E.|title=Preparation of Graphitic Oxide|journal=Journal of the American Chemical Society|date=March 20, 1958|volume=80|issue=6|page=1339|doi=10.1021/ja01539a017}}<!--|access-date=30 October 2014--></ref> was developed in 1958 as a safer, faster and more efficient method of producing graphite oxide. Before the method was developed, the production of graphite oxide was slow and hazardous to make because of the use of concentrated sulfuric and nitric acid. The [[Staudenmeier–Hoffman–Hamdi method]]<ref>{{cite journal|last1=Ojha|first1=Kasinath|last2=Anjaneyulu|first2=Oruganti|last3=Ganguli|first3=Ashok|title=Graphene-based hybrid materials: synthetic  approaches and properties|journal=Current Science|date=10 August 2014|volume=107|issue=3|pages=397–418|url=http://www.currentscience.ac.in/Volumes/107/03/0397.pdf|access-date=7 November 2014}}</ref> introduced the addition of potassium chlorate. However, this method had more hazards and produced one gram of graphite oxide to ten grams of potassium chlorate.<ref>{{cite web|last1=Murray-Smith|first1=Robert|title=How to make graphene oxide|url=https://www.youtube.com/watch?v=lRDPZ8yLh04|website=YouTube|access-date=16 November 2014}}</ref>

[[William S. Hummers]] and [[Richard E. Offeman]] created their method as an alternative to the above methods after noting the hazards they posed to workers at the [[National Lead Company]]. Their approach was similar in that it involved adding graphite to a solution of concentrated acid. However, they simplified it to just graphite, concentrated sulfuric acid, sodium nitrate, and potassium permanganate. They also did not have to use temperatures above 98&nbsp;°C and avoided most of the explosive risk of the Staudenmeier–Hoffman–Hamdi method.

The procedure starts with 100&nbsp;g graphite and 50&nbsp;g of sodium nitrate in sulfuric acid at 66&nbsp;°C which is then cooled to 0&nbsp;°C. 300&nbsp;g of potassium permanganate is then added to the solution and stirred. Water is then added in increments until the solution is approximately 32 liters.

The final solution contains about 0.5% of solids to then be cleaned of impurities and dehydrated with [[phosphorus pentoxide]].
<!-- Until this section text gets longer, please leave this space for the right-justified image–-->

== Chemical equations and efficiency ==
The basic chemical reaction involved in the Hummers' method is the [[Redox|oxidation]] of graphite, introducing molecules of oxygen to the pure carbon graphene. The reaction occurs between the graphene and the concentrated sulfuric acid with the potassium permanganate and sodium nitrate acting as catalysts. The process is capable of yielding approximately 188&nbsp;g of graphite oxide to 100&nbsp;g of graphite used. The ratio of carbon to oxygen produced is within the range of 1 to 2.1–2.9 that is characteristic of graphite oxide. The contaminants are determined to be mostly ash and water. Toxic gases such as [[dinitrogen tetraoxide]] and [[nitrogen dioxide]] are evolved in the process. The final product is typically 47.06% oxygen, 27.97% carbon, 22.99% water, and 1.98% ash with a carbon-to-oxygen ratio of 2.25. All of these results are comparable to the methods that preceded them.

{| class="wikitable"
|+ A comparison of Hummers method to the Staudenmeier method<ref name="Hummers Method"/>
|-
! Method !! % Oxygen !! % Carbon !! % Water !! % Ash !! Carbon-to-oxygen atomic ratio
|-
| Hummers || 47.06 || 27.97 || 22.99 || 1.98 || 2.25
|-
| Staudenmeier || 52.112 || 23.99 || 22.2 || 1.90 || 2.89

|}
<!-- Until this section text gets longer, please leave this space for the right-justified image -->

== Significance ==
The method has been taken up by many researchers and chemists who are interested in the use of graphite oxide for other purposes, because it is the fastest<ref>{{cite journal|last1=Ciszewski|first1=Mateusz|last2=Mianowski|first2=Andrzej|title=Survey of graphite oxidation methods using oxidizing mixtures in inorganic acids|journal=CHEMIK|date=2013|volume=67|issue=4|pages=267–274|url=http://www.chemikinternational.com/year-2013/year-2013-issue-4/survey-of-graphite-oxidation-methods-using-oxidizing-mixtures-in-inorganic-acids/|accessdate=15 November 2014}}</ref> conventional method of producing graphite oxide while maintaining a relatively high C/O ratio. When researchers and chemists are introducing a large quantity of graphite oxide within time limitations, Hummers' method is usually referenced in some form.

== Modern variations ==
Graphite oxide captured the attention of the scientific community after the discovery of graphene in 2004. Many teams are looking into ways of using graphite oxide as a shortcut to mass production of graphene. So far, the materials produced by these methods have shown to have more defects than those produced directly from graphite. Hummers' method remains a key point of interest because it is an easy method of producing large quantities of graphite oxide.

Other groups have been focused on making improvements to the Hummers' method to make it more efficient and environmentally friendly. One such process is eliminating the use of NaNO<sub>3</sub> from the process.<ref>{{cite journal|last1=Kovtyukhova|first1=N.I.|last2=Ollivier|first2=P.J.|last3=Martin|first3=B.J.|last4=Mallouk|first4=T.E.|last5=Chizhik|first5=S.A.|last6=Buzaneva|first6=E.V.|last7=Gorchinskiy|first7=A.D.|title= Layer-by-Layer Assembly of Ultrathin Composite Films from Micron-Size Graphite Oxide Sheets and Polycations |journal=Chemistry of Materials|date=January 1999|volume=11|pages=771–778|doi= 10.1021/cm981085u}}</ref><ref>{{cite journal|last1=Chen|first1=Ji|last2=Yao|first2=Bowen|last3=Li|first3=Chun|last4=Shi|first4=Gaoquan|title=An improved Hummers method for eco-friendly synthesis of graphene oxide|journal=Carbon|date=November 2013|volume=64|pages=225–229|doi=10.1016/j.carbon.2013.07.055}}</ref> The addition of persufate (S<sub>2</sub>O<sub>8</sub><sup>2−</sup>) ensures the complete oxidation and exfoliation of graphite to yield suspensions of individual graphite oxide sheets.  The elimination of nitrate is also advantageous as it stops the production of gases such as nitrogen dioxide and dinitrogen tetraoxide.

The method has also been modified in some cases to produce graphene oxide through the use of exfoliation and ultrasonic waves.<ref>{{cite journal|last1=Shahriary|first1=Leila|last2=Athawale|first2=Anjali|title=Graphene Oxide Synthesized by using Modified Hummers Approach|journal=Renewable Energy and Environmental Engineering|date=January 2014|volume=2|issue=1|url=http://isindexing.com/isi/papers/1390996530.pdf}}</ref>
[[File:Hoffman structural model of the GO sheet.jpg|thumbnail|Structural Model of a Molecule of Graphene Oxide]]

== Future uses ==
Besides graphene, Hummer's method has become a point of interest in [[photocatalysts]].<ref>{{cite journal|last1=Tu|first1=Wenguang|last2=Zhou|first2=Yong|last3=Zou|first3=Zhigang|title=Versatile Graphene-Promoting Photocatalytic Performance of Semiconductors: Basic Principles, Synthesis, Solar Energy Conversion, and Environmental Applications|journal=Advanced Functional Materials|date=October 2013|volume=23|issue=40|pages=4996–5008|doi=10.1002/adfm.201203547}}</ref> After discovering that graphite oxide is reactive to many of the wavelengths of light found within sunlight, teams have been looking into methods of using it to bolster the speed of reaction in decomposition of water and organic matter. The most common method for producing the graphite oxide in these experiments has been Hummers' method.

== See also ==
* [[Graphite Oxide]]

== References ==
{{Reflist}}

[[Category:Graphite]]
[[Category:Name reactions]]