__NOTOC__
{{Infobox TV channel
|name           = Abu Dhabi Sports
|launch         = 1969
|twitter        = @ADSportsTV
|owner          = [[Abu Dhabi Media]]
|picture format = 16:9, eight [[1080i]] [[High-definition television|HD]] channels
|web            = http://adsports.ae
|country        = [[United Arab Emirates]]
|broadcast area = '''[[Middle East]]'''<br>'''[[North Africa]]'''<br>'''[[Europe]]'''<br>'''[[North America]]'''<br>'''[[Australia]]'''<br>'''[[Russia]]'''
| sister names = [[Drama TV]]
|network_type   = [[Cable television|Cable]] and [[Satellite television|Satellite]] [[television network]]|
| sat serv 1    = [[Dish Network]] ([[United States]])
| sat chan 1    = Channel 767, 768
| sat serv 2    = 
| sat chan 2    = 
| sat serv 3    = [[Nilesat]]<br>[[Arab Satellite Communications Organization#Arabsat-4|BADR-6]]
| sat chan 3    = [[Free-to-air|FTA]] channels 1 - 2 (HD & SD) , subscription required for the other channels
| sat serv 4    = [[Hot Bird]]
| sat chan 4    = ([[Free-to-air|FTA]] channel 1 only)
| cable serv 1  = 
| cable chan 1  = 
| adsl serv 1   = [[Telus|TELUS TV]]
| adsl chan 1   = Channel 530
| online serv 1 = [[YouTube]]
| online chan 1 = [https://www.youtube.com/adsportschannels Watch online] {{small|(SD)}}
}}

'''Abu Dhabi Al Riyadiya''' ({{lang-ar|{{Arabi|أبوظبي الرياضية}}}}) is an [[Arabic language|Arabic]] [[television station]]. It broadcasts from [[Abu Dhabi]], capital of the [[United Arab Emirates]] and is owned by [[Abu Dhabi Media]] Abu Dhabi Sports Channel is an UAE Arab satellite channel broadcast from Abu Dhabi headed by Yaqoob Al Saadi.

==Channels== 
* Abu Dhabi Sports 1 HD ([[Free-to-air|FTA]])
* Abu Dhabi Sports 2 HD ([[Free-to-air|FTA]])
* Abu Dhabi Sports 3 HD 
* Abu Dhabi Sports 4 HD
* Abu Dhabi Sports 5 HD
* Abu Dhabi Sports 6 HD
* Edge Sport HD
* YAS Sports HD ([[Free-to-air|FTA]])

==Football==

=== League's ===

*[[UAE Arabian Gulf League]]
*[[MLS]]
*[[Russian Premier League]]
*[[Indian Super League]]
*[[Egyptian Premier League]]
*[[Bahraini Premier League]]
*[[Kuwait VIVA Premier League]]

=== Cup's ===
*[[Coppa Italia]]
*[[Copa del Rey]] (Final's)
*[[Supercopa de España]]
*[[Copa Do Brasil]]
*[[Copa Sudamericana]]
*[[Copa Libertadores de América]]
*[[CONCACAF Gold Cup]]
*[[CONCACAF Champions League]]
*[[UAE League Cup]]
*[[Bahraini King's Cup]]
*[[MLS Cup]]

=== Qualifying & Qualification's ===
*[[2018 FIFA World Cup qualification (UEFA)]]
*[[UEFA Euro 2016 qualifying]]
*[[2018 FIFA World Cup qualification (CONCACAF)]]

==American Football ==
*[[NCAA]] Men's College Football 2013-2014

== Basketball ==
*[[United Arab Emirates national basketball team]]
*[[NCAA]] Men's College Basketball 2013-2014

== Tennis ==
*[[Mubadala World Tennis Championship]] Exhibition tournament
*[[Moselle Open]]
*[[Luxembourg Open]]

== Handball ==
*[[United Arab Emirates Men's Handball League]]

== Motorsport ==
*[[24 Hours of Le Mans]]
*[[24 Hours of Nürburgring]]
*[[Macau GP]]
*[[Superleague Formula]]
*[[NASCAR]]
*[[World Rally Championship|WRC]]

== Wrestling ==
*[[UFC]]
*[[jiu jitsu]] (UAE)
*[[WWE]]

== Club's TV Channel's ==
*[[Manchester City TV]] (exclusive)
*Roma TV (exclusive)
*[[Juventus TV]] (exclusive)
*Lazio TV (exclusive)
*[[Benfica TV]] (exclusive)
*[[Bayern TV]] (exclusive)

==Famous guests==
The channel hosted a number of premier league celebrities like :
[[Andrew Cole]], [[Peter Reid]], [[Lee Sharpe]], [[Ron Atkinson]], [[Hatem Trabelsi]], [[Danny Mills]], [[Dietmar Hamann]], [[Steve Bruce]], [[John Barnes (footballer)|John Barnes]]...

==Frequencies==
Abu Dhabi Sports has many frequencies on different satellites.  Its pay channels are available in [[MENA]] on Nilesat (7.0W). One of its [[Free-to-air]] channels is available on Hotbird (13.0E):
* '''Nilesat (7.0W):'''
** Channels: Abu Dhabi Sports 1 ([[Free-to-air|FTA]], all HD)
** Frequency: 12226.
** Polarization: Horizontal.
** Symbol Rate: 27500.
** FEC: 3/4
* '''Nilesat (7.0W):'''
** Channels: Abu Dhabi Sports 2 ([[Free-to-air|FTA]])a
** Frequency: 12226.
** Polarization: Horizontal.
** Symbol Rate: 27500.
** FEC: 3/4
* '''Eutelsat 7 West A (7.3W):'''
** Channels: Abu Dhabi Sports 1 HD, 2 HD, Extra HD ([[Free-to-air|FTA]], all HD)
** Frequency: 12467.
** Polarization: Horizontal.
** Symbol Rate: 27500.
** FEC: 5/6
* '''Nilesat (7.0W)'''
** Channels: Abu Dhabi Sports 3, 4, 5, 6, 7 (all HD)
** Frequency: 12092.
** Polarization: Vertical.
** Symbol Rate: 27500.
** FEC: 5/6
* '''BADR 6 (26.0E)'''
** Channels: Abu Dhabi Sports 1 & 2, Abu Dhabi Sports Extra ([[Free-to-air|FTA]])
** Frequency: 11804.
** Polarization: Horizontal.
** Symbol Rate: 27500.
** FEC: 3/4
* '''Hotbird (13.0E)'''
** Channels: Abu Dhabi Sports 1 ([[Free-to-air|FTA]], all HD)
** Frequency: 11747.
** Polarization: Vertical.
** Symbol Rate: 27500.
** FEC: 3/4
* '''Yahsat 1A (52.5E)'''
** Channels: Abu Dhabi Sports 1 HD, 2 HD ([[Free-to-air|FTA]])
** Frequency: 11861.
** Polarization: Horizontal.
** Symbol Rate: 27500.
** FEC: 8/9

==Revamp==
In October 2008, Abu Dhabi Al Riyadiya went online with a new identity, logo, and programs.

==External links==
*adsports.ae
*http://www.adtvnetwork.com
*[https://web.archive.org/web/20111025133941/http://www.lyngsat.com:80/packages/admcnile.html ADMC Sports Channel] at [[LyngSat]] Packages page for Nilesat 7.0W
*[https://web.archive.org/web/20111014234837/http://www.lyngsat.com:80/packages/admcbadr.html ADMC Sports Channel] at [[LyngSat]] Packages page for BADR6 26.0E
*[http://www.lyngsat.com/hotbird.html ADMC Sports Channel] at [[LyngSat]] Satellite page for Hotbird 13.0E

{{CATV Africa, Asia, and Oceania}}

{{DEFAULTSORT:Abu Dhabi}}
[[Category:Sports television networks]]
[[Category:Television stations in the United Arab Emirates]]
[[Category:Television channels and stations established in 1969]]
[[Category:Direct broadcast satellite services]]
[[Category:Media in Abu Dhabi]]
[[Category:Sports television in the United Arab Emirates]]