{{Orphan|date=May 2015}}

Niban-like [[protein]] 2.<ref name="genenames">{{cite web|url=http://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=HGNC:24130|title=FAM129C Symbol Report - HUGO Gene Nomenclature Committee|publisher=}}</ref>(NLP2) is a [[protein]] that in [[human]]s is encoded by the FAM129C<ref name="genenames" /> [[gene]]. [[Homology (biology)#Paralogy|Paralogs]] of this gene include [[FAM129A]], and FAM129B.<ref name=":2">http://www.genecards.org/cgi-bin/carddisp.pl?gene=FAM129C&search=61d05826c90d036d78dc844f00fa067c</ref> Its aliases include [[B cell|B-Cell]] Novel Protein 1 (BCNP1), and Family with Sequence Similarity 129 Member C (FAM129C).<ref>{{Cite web|title = NCBI - WWW Error Blocked Diagnostic|url = http://www.ncbi.nlm.nih.gov/gene/199786|website = www.ncbi.nlm.nih.gov|accessdate = 2015-05-08}}</ref><ref>{{Cite web|title = FAM129C - Niban-like protein 2 - Homo sapiens (Human)|url = http://www.uniprot.org/uniprot/Q86XR2|website = www.uniprot.org|accessdate = 2015-05-08}}</ref>

==Gene==
[[File:FAM129C gene location 19p13.11, National Center for Biotechnology Information,.png|thumb|348x348px|FAM129C gene location on chromosome 19, genomic context]]
The FAM129C gene is 30,538 [[base pair]]s long and is mapped to 19p.13.112 on [[Chromosome 19 (human)|chromosome 19]] (NC_000019.10) from 17523301 to 17553839 in humans. Chromosome 19 has highest [[gene density]] of all human chromosomes<ref name=":0">{{Cite journal|title = The DNA sequence and biology of human chromosome 19|journal = Nature|date = Apr 1, 2004|issn = 1476-4687|pmid = 15057824|pages = 529–535|volume = 428|issue = 6982|doi = 10.1038/nature02399|first = Jane|last = Grimwood|first2 = Laurie A.|last2 = Gordon|first3 = Anne|last3 = Olsen|first4 = Astrid|last4 = Terry|first5 = Jeremy|last5 = Schmutz|first6 = Jane|last6 = Lamerdin|first7 = Uffe|last7 = Hellsten|first8 = David|last8 = Goodstein|first9 = Olivier|last9 = Couronne}}</ref> and large clustered [[Gene family|gene families]] corresponding to high [[Glycine|G]] + [[Cytosine|C]] content, [[CpG site|CpG]] islands, and high-density repetitive DNA suggest [[evolution]]ary significance for genes located here.<ref name=":0" /> Based on location and expression of FAM129C gene, this would suggest it has a role in [[immune system]] function.

==Gene conservation==
True [[Homology (biology)|orthologs]] of FAM129C seem to be highly conserved in [[mammal]]s, [[reptile]]s, [[marsupial]]s, [[Osteichthyes|bony]] and [[Chondrichthyes|cartilaginous fish]]. The most distant ortholog of FAM129C were found to be in a cellular slime mould, [[Polysphondylium pallidum|''Polysphondyllum'' ''pallidum'']], and even a species of barley, [[Barley|''Hordeum'' ''vulgare'']].
{| class="wikitable"
!Species
!Common Name
!NCBI Accession #
!Sequence Length
!E value
|-
|''[[Homo sapiens]]''
|Human
|[http://www.ncbi.nlm.nih.gov/protein/189442855?report=genbank&log$=prottop&blast_rank=1&RID=F3WWWKBG015 AAI67806]
|697
|–––––––
|-
|[[Polysphondylium pallidum|''Polysphondyllum pallidum'']]
|Cellular slime mould
|[http://www.ncbi.nlm.nih.gov/protein/281211133?report=genbank&log$=prottop&blast_rank=3&RID=EYD3US79015 ADBJ01000008.1]
|532
|1.00E-05
|-
|[[Barley|''Hordeum vulgare'']]
|Barley
|[http://www.ncbi.nlm.nih.gov/protein/326531476?report=genbank&log$=prottop&blast_rank=4&RID=EYD3US79015 AK366539.1]
|553
|2.00E-04
|}

==Gene Expression==
[[File:Fam129c Dilated cardiomyopathy.png|thumb|FAM129C expression in dilated cardiomyopathy tissue]]
[[File:Fam129c Rubinstein-Taybi.png|thumb|p300 genetic reduction model of Rubinstein-Taybi syndrome: hippocampus]]
In normal tissues, the highest expression was in [[lymph]], [[bone marrow]], and [[spleen]] tissue, with low expression in other parts of the human body.<ref>{{Cite web|title=OMIM Entry               - * 609967 - B-CELL NOVEL PROTEIN 1 |url=http://omim.org/entry/609967 |website=omim.org |accessdate=2015-05-08 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{Cite web|title = NCBI - WWW Error Blocked Diagnostic|url = http://www.ncbi.nlm.nih.gov/IEB/Research/Acembly/av.cgi?db=human&term=fam129c&submit=Go|website = www.ncbi.nlm.nih.gov|accessdate = 2015-05-09}}</ref> FAM129C contains [[pleckstrin homology domain]] that may cause the protein to associate with the [[plasma membrane]].<ref name=":1">{{Cite web|title = Leicester Research Archive: Preliminary Characterisation of FAM129C, a Novel Protein Identified from Proteomic Screening of CLL Samples|url = https://lra.le.ac.uk/handle/2381/9292|website = lra.le.ac.uk|accessdate = 2015-05-08}}</ref> It is expressed in early stages of [[B cell|B-cell]] [[Cellular differentiation|differentiation]], and in high levels in chronic [[Lymphoid leukemia|lymphocytic leukemia]], and in the activated subtype of diffuse large B-cell lymphoma.<ref>{{Cite journal|title = Proteomic analysis of the cell-surface membrane in chronic lymphocytic leukemia: identification of two novel proteins, BCNP1 and MIG2B|url = http://www.nature.com/leu/journal/v17/n8/full/2402993a.html|journal = Leukemia|date = 2003|access-date = 2015-05-09|issn = 0887-6924|pages = 1605–1612|volume = 17|issue = 8|doi = 10.1038/sj.leu.2402993|first = R. S.|last = Boyd|first2 = P. J.|last2 = Adam|first3 = S.|last3 = Patel|first4 = J. A.|last4 = Loader|first5 = J.|last5 = Berry|first6 = N. T.|last6 = Redpath|first7 = H. R.|last7 = Poyser|first8 = G. C.|last8 = Fletcher|first9 = N. A.|last9 = Burgess|pmid=12886250}}</ref> FAM129C is mainly expressed in the cytoplasm.<ref name=":2" /> The pattern of expression is similar to that of CXCR4, so may be involved in B cell development and B cell maturation during [[Germinal center|germinal center reaction]].<ref name=":1" />

In the human GEO profile, FAM129C appears to be expressed at lower levels in tissues with dilated [[cardiomyopathy]] by almost 50% when compared to non-failing septum [[Tissue (biology)|tissue]].<ref>{{Cite web|title = NCBI - WWW Error Blocked Diagnostic|url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS2206:IMAGp998J015620|website = www.ncbi.nlm.nih.gov|accessdate = 2015-05-09}}</ref> This may mean that FAM129C plays a role in non-failing heart tissue. Another condition in which FAM129C is significantly down-regulated is with the wild-type [[genotype]] [[hippocampus|hippocampal]] tissue of [[Rubinstein–Taybi syndrome|Rubinstein-Taybi]] compared with the p300 +/- genotype.<ref>{{Cite web|title = NCBI - WWW Error Blocked Diagnostic|url = http://www.ncbi.nlm.nih.gov/geo/tools/profileGraph.cgi?ID=GDS3598:1457728_at|website = www.ncbi.nlm.nih.gov|accessdate = 2015-05-09}}</ref> People with this condition have an increased risk of developing noncancerous and cancerous tumors such as [[leukemia]] and [[lymphoma]]

==Protein==
The [[isoelectric point]] of NLP2 is 8.576000.<ref name=":3">{{Cite web|title = SDSC Biology Workbench|url = http://seqtool.sdsc.edu/|website = seqtool.sdsc.edu|accessdate = 2015-05-09}}</ref> The [[molecular weight]] is 77.4 [[Kilodalton|kdal]].<ref name=":3" /> The  [[amino acid sequence]] is 697aa long<ref name=":2" />

===  Structure ===
The predicted tertiary structure for NLP2 shows the FAM129C PH domain. There are seven predicted β sheets at the N terminus.<ref name=":1" /><ref>{{Cite web|title = PHYRE2 Protein Fold Recognition Server|url = http://www.sbg.bio.ic.ac.uk/phyre2/html/page.cgi?id=index|website = www.sbg.bio.ic.ac.uk|accessdate = 2015-05-09|first = Lawrence|last = Kelley}}</ref> This will form the tertiary structure of the pleckstrin homology domain.<ref name=":1" />

==Protein Post-Modification==
Transmembrane domains, peptide cleavage sites, or strong glycosylation sites were not predicted for NLP2.<ref>{{Cite web|title = TMHMM Server, v. 2.0|url = http://www.cbs.dtu.dk/services/TMHMM/|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref><ref>{{Cite web|title = SignalP 4.1 Server|url = http://www.cbs.dtu.dk/services/SignalP/|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref><ref>{{Cite web|title = TargetP 1.1 Server|url = http://www.cbs.dtu.dk/services/TargetP/|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref><ref>{{Cite web|title = ProP 1.0 Server|url = http://www.cbs.dtu.dk/services/ProP/|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref><ref>{{Cite web|title = NetNGlyc 1.0 Server|url = http://www.cbs.dtu.dk/services/NetNGlyc/|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref><ref>{{Cite web|title = DISULFIND - Cysteines Disulfide Bonding State and Connectivity Predictor|url = http://disulfind.dsi.unifi.it/|website = disulfind.dsi.unifi.it|accessdate = 2015-05-09}}</ref> A total of 32 likely phosphorylation sites were predicted on [[Serine]] (25,) [[Threonine]] (5), and [[Tyrosine]] (2).<ref>{{Cite web|title = NetPhos 2.0 Server - prediction results|url = http://www.cbs.dtu.dk/cgi-bin/webface2.fcgi?jobid=554D6BF300004259E20ADEE0&wait=20|website = www.cbs.dtu.dk|accessdate = 2015-05-09}}</ref>

==References==
{{reflist}}

*

[[Category:Human proteins]]