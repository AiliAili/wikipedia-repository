{{Use dmy dates|date=February 2015}}
{{Infobox hospital
| name        = Abu Dhabi Falcon Hospital (ADFH) <br/> <big>'''مستشفى أبو ظبي للصقور'''</big>
| org/group   = 
| image       =
| image alt   = 
| image_size  = 
| caption     = 
| map_type    =
| map_caption = 
| logo        = 
| logo size   = 
| location    = 
| region      = 
| state       = [[Abu Dhabi]]
| country     = [[UAE]]  
| coordinates = {{Coord|24|24|27|N|54|41|55|E|display=title,inline}}
| address     = P.O.Box: 45553
| healthcare  = [[Veterinary]]
| funding     =
| type        = 
| speciality  = 
| standards   = 
| emergency   = 
| helipad     = 
| affiliation = 
| patron      = 
| network     = 
| beds        = 
| founded     = 1999
| closed      = <!-- Use if defunct, please also add to Category:Defunct hospitals -->
| website     = {{URL|http://www.falconhospital.com}}
| Wiki-Links  = 
}}

The '''Abu Dhabi Falcon Hospital''' is the world's foremost [[falcon]] welfare institution,<ref>{{Cite news|url= http://www.mydestination.com/abudhabi/things-to-do/167300/abu-dhabi-falcon-hospital
|title= The Abu Dhabi Falcon Hospital is the premier institution world-wide for falcon medicine and research in falcon’s diseases|accessdate=2012-05-08}}</ref><ref>{{Cite news|url= http://www.silverkris.com/destinations/5-must-dos/5-must-dos-abu-dhabi
|title= the Falcon Hospital has won six international awards for its education initiatives.|accessdate=2012-05-08|location=Abu Dhabi|work= Silverkris |first=MIKE |last=MACEACHERAN}}</ref> and the first of its kind.<ref>{{Cite news|url= http://www.telegraph.co.uk/expat/expatnews/7187478/UAE-hospital-offers-medical-treatment-thats-for-the-birds.html |title= "This hospital is the largest falcon hospital in the whole world," says its director, German veterinarian Margit Muller. "It was the first public falcon hospital," she adds. |accessdate=2012-05-08| location=Abu Dhabi |work= www.telegraph.co.uk |first=W.G. |last=Dunlop |date=2010-02-08}}</ref>  It opened on 3 October 1999, as an affiliate of the Abu Dhabi Environment Agency, since when it has been directed by German [[veterinary surgery|veterinary surgeon]] Dr Margit Gabriele Müller and has become one of the largest avian hospitals.<ref>{{Cite news|url= http://www.nzherald.co.nz/international-travel/news/image.cfm?c_id=1501830&gal_objectid=10799062&gallery_id=125130#8880720 |title= This unusual avian hospital is the biggest of its kinds in the world |accessdate=2012-05-08|location=Auckland|work= [[The New Zealand Herald]]}}</ref>

The hospital operates under the [[patronage]] of His Highness [[Sheikh]] [[Mohammed bin Zayed Al Nahyan|Mohamed bin Zayed]], [[Crown Prince]] of Abu Dhabi.<ref name="usembassy"/>  It has proven itself one of  [[Abu Dhabi]]'s major [[tourist attraction]]s.<ref>{{Cite news|url= http://www.arabiantravelmarket.com/__novadocuments/8851 |title= The Abu Dhabi Falcon Hospital has become one of the main tourism attractions in Abu Dhabi |accessdate=2012-05-08| location=Abu Dhabi}}</ref><ref>{{Cite news|url= http://www.departures.com/articles/guide-to-the-persian-gulf-abu-dhabi
|title= Anyone interested in the culture of the Emirates should tour the Abu Dhabi Falcon Hospital, a veterinary facility that explains the region’s noble culture of falconry to visitors|accessdate=2012-05-08|first=Amin|last=Jaffer}}</ref> In 2010 it was visited by [[United States Ambassador to the United Arab Emirates|U.S. Ambassador]] [[Richard Olson]].<ref name="usembassy">{{Cite news|url= http://abudhabi.usembassy.gov/pe_07182010.html |title= U.S. Ambassador Richard Olson visited the Abu Dhabi Falcon Hospital |accessdate=2012-05-08 |location=Abu Dhabi|work= The Embassy of the United States in Abu Dhabi Homepage}}</ref>

==Description==
The hospital has individual air-conditioned rooms for over 200 birds. Each year about 6,000 falcons enter its care.<ref>{{Cite news|url= http://www.abudhabiweek.ae/component/content/article/159-this-is-abu-dhabi/6011-winged-wonders?directory=234&Itemid=234 |title= the hospital can accommodate more than 200 falcons at the same time, housed in individual air-conditioned rooms, and treats about 6,000 patients a year.|accessdate=2012-05-08 | location=Abu Dhabi |work= Abu Dhabi Week |first=Jon |last=Muller}}</ref> It offers annual check-ups because it is often difficult to recognise whether a falcon has a health issue.<ref>{{Cite news|url= http://www.dailymail.co.uk/travel/article-1391524/Abu-Dhabi-Things-emirate-bling.html |title= Run by the charismatic German, Dr Margit Muller, the hospital has encouraged Emiratis to bring in their birds for annual checkups.|accessdate=2012-05-08| location=Abu Dhabi|work= Mail Online|first=Jody |last=Thompson|date=2011-05-31}}</ref> The staff is experienced in treating everything they might diagnose. Even the birds' feathers are carefully scrutinised. As visitors learn during the popular tours a falcon's flight gets significantly unbalanced when they lose even a single [[flight feather]]. Consequently it is standard treatment to repair or substitute lost feathers by those kept in stock.<ref>{{Cite news|url= http://www.northernstar.com.au/story/2012/04/20/friends-high-places/|title= "If they lose a feather," our guide explained, "their flight is unbalanced. We keep a supply of spare feathers here in case the missing one cannot be recovered."|accessdate=2012-05-08|location=Lismore, New South Wales|work= [[The Northern Star]]|first=Jim |last=Eagles}}</ref>

==Passports==
Since the [[United Arab Emirates]] introduced falcon [[passport]]s it is the ADFH's prerogative to issue them.<ref>{{Cite news|url= http://www.falcons.co.uk/default.asp?id=23&menu=3 |title= The falcon registration forms are available at the Abu Dhabi Falcon Hospital, which is the only hospital in the UAE entirely dedicated to treating falcons that have been injured or are in poor health. It is the only hospital authorised to issue falcon passports.|accessdate=2012-05-08}}</ref>

==Animal Shelter==
The Abu Dhabi Falcon Hospital also manages an [[animal shelter]]. This is based on the approval of the Abu Dhabi Executive Council of the Abu Dhabi government.<ref>{{Cite news|url= http://www.abudhabianimalshelter.com/frmlistlosfound.aspx |title= The Abu Dhabi Animal Shelter for stray cats and dogs for Abu Dhabi Emirate has been established following the decision of the Abu Dhabi Executive Council of Abu Dhabi government  on 15th April 2010.|accessdate=2012-05-08| location=Abu Dhabi|work= Abu Dhabi Animal Shelter Homepage}}</ref>  The facility can accommodate hundreds of stray dogs and cats.<ref>{{Cite news|url= http://www.timeoutabudhabi.com/community/features/24656-abu-dhabi-falcon-hospital|title= The ADFH makes room for hundreds of homeless animals |accessdate=2012-05-08|location=Abu Dhabi |work= Time Out Abu Dhabi|first=Karl|last=Baz}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.falconhospital.com/web/default.aspx Official Homepage]
*{{YouTube|id=m_wYPBhXyNY&feature=player_embedded  |title=The Abu Dhabi Falcon Hospital}}
*[http://www.arabiantravelmarket.com/__novadocuments/8851  Falcons - Heritage of Abu Dhabi]

{{DEFAULTSORT:Abu Dhabi Falcon Hospital}}
[[Category:Raptor organisations]]
[[Category:Veterinary hospitals]]
[[Category:1999 establishments in the United Arab Emirates]]
[[Category:Animal welfare organisations in Abu Dhabi]]
[[Category:Hospital buildings completed in 1999]]
[[Category:Hospitals in the United Arab Emirates]]
[[Category:Hospitals established in 1999]]