{{Orphan|date=February 2013}}

{{Infobox_gene}}
'''Protein FAM83A''' (family member with sequence similarity 83) also known as '''tumor antigen BJ-TSA-9''' is a [[protein]] that in humans is encoded by the FAM83A [[gene]].<ref name="Entrez Gene">{{cite web|url=http://www.ncbi.nlm.nih.gov/nuccore/46255015 |title=Entrez Gene, FAM83A |publisher=NCBI |accessdate=2012-04-22}}</ref>

This protein is predicted to contain one domain of unknown function 1669 (DUF1669), which places this protein into the PLDc_SuperFamily.<ref name="Entrez Gene" /><ref name="PLDc_SF">{{cite web|url=http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=197200|title=PLDc_SF|publisher=NCBI}}</ref><ref name="PLDc">{{cite web|url=http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=197278|title=FAM83A, PLDc_SF categorization |publisher=NCBI}}</ref> It has been linked to be a potential [[biomarker]] in [[lung]], [[prostate]], and [[bladder]] [[cancers]].<ref name="Liu_2008">{{cite journal |vauthors=Liu L, Liao GQ, He P, Zhu H, Liu PH, Qu YM, Song XM, Xu QW, Gao Q, Zhang Y, Chen WF, Yin YH | title = Detection of circulating cancer cells in lung cancer patients with a panel of marker genes | journal = Biochem. Biophys. Res. Commun. | volume = 372 | issue = 4 | pages = 756–60 |date=August 2008 | pmid = 18514066 | doi = 10.1016/j.bbrc.2008.05.101 | url = }}</ref><ref name="Li_2005">{{cite journal |vauthors=Li Y, Dong X, Yin Y, Su Y, Xu Q, Zhang Y, Pang X, Zhang Y, Chen W | title = BJ-TSA-9, a novel human tumor-specific gene, has potential as a biomarker of lung cancer | journal = Neoplasia | volume = 7 | issue = 12 | pages = 1073–80 |date=December 2005 | pmid = 16354590 | pmc = 1501171 | doi = 10.1593/neo.05406| url = }}</ref><ref name="Jensen_2009">{{cite journal |vauthors=Jensen TJ, Wozniak RJ, Eblin KE, Wnek SM, Gandolfi AJ, Futscher BW | title = Epigenetic mediated transcriptional activation of WNT5A participates in arsenical-associated malignant transformation | journal = Toxicol. Appl. Pharmacol. | volume = 235 | issue = 1 | pages = 39–46 |date=February 2009 | pmid = 19061910 | doi = 10.1016/j.taap.2008.10.013 | url = }}</ref>

== Gene ==

FAM83A is located on [[chromosome 8]], locus q24.13,<ref name="Entrez Gene"/> and spans 27,566 [[base pairs]].<ref name="Entrez Gene"/> There is a [[Promoter (genetics)|promoter]] approximately 4,000 base pairs upstream as predicted by the tool ElDorado by Genomatix.<ref name="Genomatix">{{cite web|url=http://www.genomatix.de/ |title=Genomatix, ElDorado |publisher=Genomatix |accessdate=2102-04-26}}</ref> Deletions in this part of the chromosome, including the FAM83A gene, often result in [[Langer-Giedion syndrome]].<ref name="Pereza_2012">{{cite journal |vauthors=Pereza N, Severinski S, Ostojić S, Volk M, Maver A, Dekanić KB, Kapović M, Peterlin B | title = Third case of 8q23.3-q24.13 deletion in a patient with Langer-Giedion syndrome phenotype without TRPS1 gene deletion | journal = Am. J. Med. Genet. A | volume = 158A | issue = 3 | pages = 659–63 |date=March 2012 | pmid = 22315192 | doi = 10.1002/ajmg.a.35201 }}</ref>

== mRNA ==

The FAM83A [[mRNA]] has 10 different splice forms,<ref name="Ensemble splice">{{cite web |url=http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=ENSG00000147689;gene=ENSG00000147689;r=8:124191200-124222314 |title=Ensembl, FAM83A |accessdate=2012-04-26}}</ref> with transcript variant 1 being the subject of this article. This mRNA consists of approximately 2,900 base pairs. It contains a domain of unknown function 1669 (DUF1669), and is a member of the PLDc_SF superfamily.<ref name="Entrez PLDc_SF">{{cite web|url=http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=197200|title=Entrez, PLDc_SuperFamily|publisher=NCBI|accessdate=2012-04-22}}</ref> It has been placed into this superfamily based on highly similar sequences between FAM83A and other N-terminus [[phospholipase]] D-like domains; however, it's missing the functional histidine and therefore only predicted to have a similar structure.<ref name="PLDc"/> Four [[exons]] comprise this mRNA.<ref name="Entrez Gene" /><ref name="Kent_2002"/> Predictions show several possible RNA [[stem loop]] formations in the untranslated regions of the mRNA.<ref name="Biology Workbench">{{cite web|url=http://workbench.sdsc.edu/ |title=Biology Workbench |publisher=San Diego Supercomputer}}</ref>

[[File:FAM83A geneticfeaturesmap.tif.png|thumb|right|Schematic of the FAM83A protein product showing its major hypothesized and predicted landmarks.]]

== Protein ==

The FAM83A gene encodes for the FAM83A protein isoform A.<ref name="Entrez Gene"/> This protein is 434 [[amino acids]] in length, and weighs approximately 45 kiloDaltons.<ref name="Entrez Protein">{{cite web|url=http://www.ncbi.nlm.nih.gov/protein/NP_116288.2 |title=GenPept, FAM83A |publisher=NCBI |accessdate=2012-04-26}}</ref> Although the structure is not confirmed, it is predicted using GOR4 to have four [[alpha helices]] and five [[beta sheets]],<ref name="Sen_2005">{{cite journal |vauthors=Sen TZ, Jernigan RL, Garnier J, Kloczkowski A | title = GOR V server for protein secondary structure prediction | journal = Bioinformatics | volume = 21 | issue = 11 | pages = 2787–8 |date=June 2005 | pmid = 15797907 | pmc = 2553678 | doi = 10.1093/bioinformatics/bti408 | url = }}</ref> with an [[isoelectric point]] of 8.964<ref name="Biology Workbench" /> There appears to be no [[signal peptide]]<ref name="Petersen_2011">{{cite journal |vauthors=Petersen TN, Brunak S, von Heijne G, Nielsen H | title = SignalP 4.0: discriminating signal peptides from transmembrane regions | journal = Nat. Methods | volume = 8 | issue = 10 | pages = 785–6 | year = 2011 | pmid = 21959131 | doi = 10.1038/nmeth.1701 }}</ref> and is not a [[transmembrane protein]].<ref name="Krogh_2001">{{cite journal |vauthors=Krogh A, Larsson B, von Heijne G, Sonnhammer EL | title = Predicting transmembrane protein topology with a hidden Markov model: application to complete genomes | journal = J. Mol. Biol. | volume = 305 | issue = 3 | pages = 567–80 |date=January 2001 | pmid = 11152613 | doi = 10.1006/jmbi.2000.4315 }}</ref> Using CBLAST, it was observed to share a common sequence with the protein [[3H0G]],<ref name="CBLAST">{{cite web |url=http://www.ncbi.nlm.nih.gov/Structure/cblast/cblast.cgi |title=CBLAST |publisher=NCBI |accessdate=2012-04-26}}</ref> however, not in the predicted functional region of FAM83A. This may still provide valuable information, though, on a possible partial structure for this protein.

== Expression ==

FAM83A is expressed in many different areas of the human body, and at very different levels.<ref name="EST">{{cite web|url=http://www.ncbi.nlm.nih.gov/UniGene/ESTProfileViewer.cgi?uglist=Hs.379821|title=EST Profiles, FAM83A|publisher=NCBI}}</ref> [[expressed sequence tag|EST]] data suggests that this gene is expressed primarily in adults. It shows significant levels in the [[mouth]] and [[larynx]], with other raised expression in the prostate, lung, and [[esophagus]].<ref name="EST"/> These normal levels are even more elevated in head and neck [[tumors]], lung tumors, prostate cancers, and bladder [[carcinomas]].<ref name="EST"/> [[Microarray]] expression data also show different environmental conditions, especially irritants. Such irritants can be cigarette smoke, where FAM83A has been shown to increase expression after 24 hours exposure,<ref name="GDS1348">{{cite web|url=http://www.ncbi.nlm.nih.gov/geo/gds/profileGraph.cgi?&dataset=A5KYKBJEB4MVOzwuoi&dataset=awhtm-la8vptmyyyxx$&gmin=0.511550&gmax=8.553317&absc=&uid=12856830&gds=1348&idref=002003020005&annot=FAM83A|title=GDS1348, FAM83A expression to cigarette smoke |publisher=NCBI}}</ref> and house dust mite extract on bronchial [[epithelial]] cells, where FAM83A expression also increases after exposure.<ref name="Vroling_2007">{{cite journal |vauthors=Vroling AB, Duinsbergen D, Fokkens WJ, van Drunen CM | title = Allergen induced gene expression of airway epithelial cells shows a possible role for TNF-alpha | journal = Allergy | volume = 62 | issue = 11 | pages = 1310–9 |date=November 2007 | pmid = 17919147 | doi = 10.1111/j.1398-9995.2007.01495.x | url = }}</ref> FAM83A shows decreased expression when [[activating transcription factor 2]] (ATF2) is knocked out.<ref name="Bhoumik_2008">{{cite journal |vauthors=Bhoumik A, Fichtman B, Derossi C, Breitwieser W, Kluger HM, Davis S, Subtil A, Meltzer P, Krajewski S, Jones N, Ronai Z | title = Suppressor role of activating transcription factor 2 (ATF2) in skin cancer | journal = Proc. Natl. Acad. Sci. U.S.A. | volume = 105 | issue = 5 | pages = 1674–9 |date=February 2008 | pmid = 18227516 | pmc = 2234203 | doi = 10.1073/pnas.0706057105 | url = }}</ref> Since ATF2 was not predicted to bind in the promoter region, it suggests that there is an indirect relationship between FAM83A and ATF2. With increased expression to irritants and allergens along with an indirect relationship with ATF2, it suggests FAM83A may be in a signaling pathway.

{{multiple image

| align = center
| image1    = GDS1348_FAM83A.tif.png
| width1    = 300
| caption1  = Microarray expression data for FAM83A when exposed to cigarette smoke. As seen, after 24 hours, expression levels increase compared to the control.<ref name="GDS1348"/>

| image2    = GDS3003_FAM83A.tif.png
| width2    = 180
| caption2  = Microarray expression data for FAM83A when exposed to house dust mite extract. FAM83A expression increases when exposed to this irritant compared to the control.<ref name="Vroling_2007"/>

| image3 = GDS3334_FAM83A.tif.png
| width3 = 205
| caption3 = Microarray expression data for FAM83A when ATF2 is knocked out. As seen, expression levels decrease along with decreased ATF2 levels. Since there is not a hypothesized direct interaction, an indirect interaction is suggested.<ref name="Bhoumik_2008"/>
}}
{{Clear}}

== Cancer ==

There are multiple studies that link FAM83A overexpression to lung, prostate, and bladder cancers. Researchers believe that this gene might make a good candidate for early detection of these cancers, especially lung cancer.<ref name="Liu_2008"/><ref name="Li_2005"/> It is unknown why or how FAM83A is upregulated. Studies have shown that arsenic can acetylate the promoter causing upregulation, suggesting this may be a similar mechanism to how this gene becomes unregulated in cancer.<ref name="Jensen_2009"/>

== Protein interaction ==

FAM83A has been shown to interact with palate, lung, and nasal epithelium carcinoma associated protein ([[Plunc|PLUNC]]) through the STRING tool.<ref name="STRING">{{cite journal |vauthors=Szklarczyk D, Franceschini A, Kuhn M, Simonovic M, Roth A, Minguez P, Doerks T, Stark M, Muller J, Bork P, Jensen LJ, von Mering C | title = The STRING database in 2011: functional interaction networks of proteins, globally integrated and scored | journal = Nucleic Acids Res. | volume = 39 | issue = Database issue | pages = D561–8 |date=January 2011 | pmid = 21045058 | pmc = 3013807 | doi = 10.1093/nar/gkq973 }}</ref> This information gathered came from textmining information dealing with cancer.<ref name="Li_2005"/>

== Homology ==

FAM83A appears to be a relatively new gene, with [[BLAST]] and [[BLAT (bioinformatics)|BLAT]] only showing [[Homology (biology)#Orthology|orthology]] back through bony fishes.<ref name="Kent_2002">{{cite journal | author = Kent WJ | title = BLAT--the BLAST-like alignment tool | journal = Genome Res. | volume = 12 | issue = 4 | pages = 656–64 |date=April 2002 | pmid = 11932250 | pmc = 187518 | doi = 10.1101/gr.229202 | url = }}</ref><ref name="BLAST">{{cite web|url=http://blast.ncbi.nlm.nih.gov/Blast.cgi |title=BLAST |publisher=NCBI |accessdate=2012-04-22}}</ref> It is highly conserved through its relatives<ref name="BLAST"/> The DUF1669 domain is the most highly conserved region of the protein, with areas outside of it being more variable. Below is a table of orthologs, noting that this is not a comprehensive list:

{| class="wikitable sortable"
|-
! [[Genus]] [[Species]] !! Organism Common Name !! Divergence from Humans (MYA) <ref name="Time Tree">{{cite web | title = Time Tree| url =http://www.timetree.org/}}</ref> !! NCBI Protein Accession Number !! Sequence Identity !! Protein Length
|-
| [[Homo sapiens]]|| Humans || -- || NM_032899.4<ref name="Entrez Protein"/>|| 100% || 434
|-
| [[Pan troglodytes]]|| Chimpanzee || 6.2 || XP_001147312.2<ref name="Nuc: Ptr">{{cite web | title = NCBI Nucleotide: XP_001147312.2| url =http://www.ncbi.nlm.nih.gov/protein/XP_001147312.2}}</ref>|| 99% || 434
|-
| [[Bos taurus]]|| Cattle || 97.4 || XP_599662.2<ref name="Nuc: Bta">{{cite web| title=NCBI Nucleotide: XP_599662.2| url=http://www.ncbi.nlm.nih.gov/protein/XP_599662.2}}</ref>|| 89% || 435
|-
| [[Canis lupus]]|| Dog || 97.4 || XP_851601.2<ref name="Nuc: Clu">{{cite web|title=NCBI Nucleotide: XP_851601.2|url=http://www.ncbi.nlm.nih.gov/protein/XP_851601.2}}</ref>|| 89% || 435
|-
| [[Mus musculus]]|| Mouse || 91 || NP_776287.2<ref name="Nuc: Mmu">{{cite web|title=NCBI Nucleotide: NP_776287.2|url=http://www.ncbi.nlm.nih.gov/protein/NP_776287.2}}</ref>|| 87% || 436
|-
| [[Anolis carolinensis]]|| Lizard || 324.5 || XP_003228186.1<ref name="Nuc: Aca">{{cite web|title=NCBI Nucleotide: XP_003228186.1|url=http://www.ncbi.nlm.nih.gov/protein/XP_003228186.1}}</ref>|| 70% || 438
|-
| [[Gallus gallus]]|| Chicken || 324.5 || XP_001233861.2<ref name="Nuc: Gga">{{cite web|title=NCBI Nucleotide: XP_001233861.2|url=http://www.ncbi.nlm.nih.gov/protein/XP_001233861.2}}</ref>|| 69% || 439
|-
| [[Xenopus laevis]]|| Frog || 361.2 || NP_001079963.1<ref name="Nuc: Xla">{{cite web|title=NCBI Nucleotide: NP_001079963.1|url=http://www.ncbi.nlm.nih.gov/protein/NP_001079963.1}}</ref>|| 64% || 443
|-
| [[Danio rerio]]|| Zebrafish || 454.6 || XP_001340276.3<ref name="Nuc: Dre">{{cite web|title=NCBI Nucleotide: XP_001340276.3|url=http://www.ncbi.nlm.nih.gov/protein/XP_001340276.3}}</ref>|| 59% || 426
|-
|}

== References ==
{{Reflist|2}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]