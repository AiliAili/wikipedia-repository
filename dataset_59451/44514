{{other uses}}
{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name = Ligeia
| title_orig = 
| translator = 
| image = Ligeia-Clarke.jpg
| caption = Illustration of "Ligeia" by [[Harry Clarke]], 1919.
| author = [[Edgar Allan Poe]]
| country = United States
| language = English
| series = 
| genre = [[Gothic Fiction|Gothic]] [[Romance novel|Romance]]<br />[[Short story]]
| publisher = ''The American Museum''
| media_type = Print ([[Literary magazine|Journal]])
| pub_date = September 1838
| english_pub_date = 
| preceded_by = 
| followed_by = 
}}
"'''Ligeia'''" (usually pronounced {{IPA-en|laɪˈʤi:ə|}}) is an early [[short story]] by American writer [[Edgar Allan Poe]], first published in 1838. The story follows an unnamed narrator and his wife Ligeia, a beautiful and intelligent raven-haired woman. She falls ill, composes "[[The Conqueror Worm]]", and quotes lines attributed to [[Joseph Glanvill]] (which suggest that life is sustainable only through willpower) shortly before dying. After her death, the narrator marries the Lady Rowena. Rowena becomes ill and she dies as well. The distraught narrator stays with her body overnight and watches as Rowena slowly comes back from the dead – though she has transformed into Ligeia. The story may be the narrator's [[opium]]-induced hallucination and there is debate whether the story was a [[satire]]. After the story's first publication in ''[[Nathan C. Brooks#The American Museum|The American Museum]]'', it was heavily revised and reprinted throughout Poe's life.

==Plot summary==
The story is told by an unnamed [[narrator]] who describes the qualities of Ligeia: a beautiful, passionate and intellectual woman, raven-haired and dark-eyed. He thinks he remembers meeting her "in some large, old decaying city near the [[Rhine]]". He is unable to recall anything about the history of Ligeia, including her family's name, but remembers her beautiful appearance. Her beauty, however, is not conventional. He describes her as emaciated, with some "strangeness". He describes her face in detail, from her "faultless" forehead to the "divine orbs" of her eyes. They marry, and Ligeia impresses her husband with her immense knowledge of physical and mathematical science, and her proficiency in classical languages. She begins to show her husband her knowledge of [[metaphysics|metaphysical]] and "forbidden" wisdom.

After an unspecified length of time Ligeia becomes ill, struggles internally with human mortality, and ultimately dies. The narrator, grief-stricken, buys and refurbishes an [[abbey]] in [[England]]. He soon enters into a loveless marriage with "the fair-haired and blue-eyed Lady Rowena Trevanion, of Tremaine".

In the second month of the marriage, Rowena begins to suffer from worsening [[anxiety]] and [[fever]]. One night, when she is about to faint, the narrator pours her a goblet of wine. Drugged with [[opium]], he sees (or thinks he sees) drops of "a brilliant and ruby colored fluid" fall into the goblet. Her condition rapidly worsens, and a few days later she dies and her body is wrapped for burial.

As the narrator keeps vigil overnight, he notices a brief return of color to Rowena's cheeks. She repeatedly shows signs of reviving, before relapsing into apparent death. As he attempts resuscitation, the revivals become progressively stronger, but the relapses more final. As dawn breaks, and the narrator is sitting emotionally exhausted from the night's struggle, the shrouded body revives once more, stands and walks into the middle of the room. When he touches the figure, its head bandages fall away to reveal masses of raven hair and dark eyes: Rowena has transformed into Ligeia.

==Analysis==
{{synthesis|section|date=October 2015}}
[[File:Poe ligeia byam shaw.JPG|thumb|Illustration by [[Byam Shaw]], circa 1909]]
The narrator relies on Ligeia as if he were a child, looking on her with "child-like confidence". On her death, he is "a child groping benighted" with "childlike perversity". Poe biographer [[Kenneth Silverman]] notes that, despite this dependency on her, the narrator has a simultaneous desire to forget her, perhaps causing him to be unable to love Rowena. This desire to forget is exemplified in his inability to recall Ligeia's last name.<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York: Harper Perennial, 1991: 139–140. ISBN 0-06-092331-8</ref> The story tells us however that the narrator never knew her last name at all.

Ligeia, the narrator tells us, is extremely intelligent, "such as I have never known in a woman". Most importantly, she served as the narrator's teacher in "[[Metaphysics|metaphysical]] investigation", passing on "wisdom too divinely precious not to be forbidden!" So, her knowledge in mysticism, combined with an intense desire for life may have led to her revival. The opening [[Epigraph (literature)|epigraph]], which is repeated in the body of the story, is attributed to [[Joseph Glanvill]], though this quote has not been found in Glanvill's extant work. Poe may have fabricated the quote and attached Glanvill's name in order to associate with Glanvill's belief in [[witchcraft]].<ref>Hoffman, Daniel. ''Poe Poe Poe Poe Poe Poe Poe''. Baton Rouge: Louisiana State University Press, 1972: 248. ISBN 0-8071-2321-8</ref>

Ligeia and Rowena serve as aesthetic opposites:<ref>Kennedy, J. Gerald. ''Poe, Death, and the Life of Writing''. New Haven, CT: Yale University Press, 1987: 83. ISBN 0-300-03773-2</ref> Ligeia is raven-haired from a city by the [[Rhine]] while Rowena (believed to be named after the character in ''[[Ivanhoe]]'') is a blonde [[Anglo-Saxons|Anglo-Saxon]]. This symbolic opposition implies the contrast between German and English [[romanticism]].<ref>Kennedy, J. Gerald. "Poe, 'Ligeia,' and the Problem of Dying Women" collected in ''New Essays on Poe's Major Tales'', edited by Kenneth Silverman. Cambridge University Press, 1993: 119–120. ISBN 0-521-42243-4</ref>

Exactly what Poe was trying to depict in the metamorphosis scene has been debated, fueled in part by one of Poe's personal letters in which he denies that Ligeia was reborn in Rowena's body<ref>Kennedy, J. Gerald. "Poe, 'Ligeia,' and the Problem of Dying Women" collected in ''New Essays on Poe's Major Tales'', edited by Kenneth Silverman. Cambridge University Press, 1993: 119. ISBN 0-521-42243-4</ref> (a statement he later retracts). If Rowena had actually transformed into the dead Ligeia, it is only evidenced in the words of the narrator, leaving room to question its validity. The narrator has already been established as an opium addict, making him an [[unreliable narrator]]. The narrator early in the story describes Ligeia's beauty as "the radiance of an opium-dream". He also tells us that "in the excitement of my opium dreams, I would call aloud upon her name, during the silence of the night... as if... I could restore her to the pathway she had abandoned... upon the earth". This may be interpreted as evidence that Ligeia's return was nothing more than a drug-induced [[hallucination]].

If Ligeia's return from death is literal, however, it seems to stem from her assertion that a person dies only by a weak will. This implies, then, that a strong will can keep someone alive. It is unclear, however, if it is Ligeia's will or her husband's will that brings Ligeia back from the dead.<ref>Hoffman, Daniel. ''Poe Poe Poe Poe Poe Poe Poe''. Baton Rouge: Louisiana State University Press, 1972: 249. ISBN 0-8071-2321-8</ref> Her illness may have been [[Tuberculosis|consumption]].<ref>{{cite journal|last1=Pérez Arranz|first1=Cristina|title=Edgar Allan Poe, MD: Medical Fiction and the Birth of Modern Medicine|journal=Trespassing journal|issn=2147-2734|date=2014|volume=Fall 2014|issue=4|pages=63-78|url=http://trespassingjournal.com/?page_id=854}}</ref>

The poem within the story, "[[The Conqueror Worm]]", also leads to some questioning of Ligeia's alleged resurrection. The poem essentially shows an admission of her own inevitable [[death|mortality]]. The inclusion of the bitter poem may have been meant to be ironic or a [[parody]] of the convention at the time, both in literature and in life. In the mid-19th century it was common to emphasize the sacredness of death and the beauty of dying (consider [[Charles Dickens]]'s Little Johnny character in ''[[Our Mutual Friend]]'' or the death of Helen Burns in [[Charlotte Brontë]]'s ''[[Jane Eyre]]''). Instead, Ligeia speaks of fear personified in the "blood-red thing".<ref>Kennedy, J. Gerald. ''Poe, Death, and the Life of Writing.'' New Haven, CT: Yale University Press, 1987: 1–2. ISBN 0-300-03773-2</ref>  Other interpretations have been suggested however.<ref>Mabbott, T. O. ''Edgar Allan Poe: Complete Poems''. University of Illinois Press, 2000. ISBN 0-252-06921-8</ref>

Poe's friend and fellow Southern writer [[Philip Pendleton Cooke]] suggested the story would have been more artistic if Rowena's possession by Ligeia was more gradual; Poe later agreed, though he had already used a slower possession in "[[Morella (short story)|Morella]]".<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Baltimore: The Johns Hopkins University Press, 1998: 270–271. ISBN 0-8018-5730-9</ref> Poe also wrote that he should have had the Ligeia-possessed Rowena relapse back to her true self so that she could be entombed as Rowena, "the bodily alterations having gradually faded away".<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Baltimore: The Johns Hopkins University Press, 1998: 271. ISBN 0-8018-5730-9</ref> However, in a subsequent letter he retracted this statement.

===As satire===
There has been some debate that Poe may have intended "Ligeia" to be a [[satire]] of [[Gothic fiction]]. The year that "Ligeia" was published, Poe published only two other prose pieces: "Siope—A Fable" and "[[A Predicament (short story)|The Psyche Zenobia]]", both Gothic-styled satires.<ref>Griffith, Clark. "Poe's 'Ligeia' and the English Romantics" in ''Twentieth Century Interpretations of Poe's Tales''. Englewood Cliffs, NJ: Prentice-Hall, 1971: 64.</ref> Supporting evidence for this theory includes the implication that Ligeia is from [[Germany]], a main source of Gothic fiction in the 19th century, and that the description of her hints at much but says nothing, especially in the description of her eyes. The narrator describes their "expression", which he admits is a "word of no meaning". The story also suggests Ligeia is a [[transcendentalism|transcendentalist]], a group of people Poe often criticized.<ref>Griffith, Clark. "Poe's 'Ligeia' and the English Romantics" in ''Twentieth Century Interpretations of Poe's Tales''. Englewood Cliffs, NJ: Prentice-Hall, 1971: 66.</ref>

===Major themes===
*Death of a beautiful woman (see also: "[[Berenice (short story)|Berenice]]", "[[The Fall of the House of Usher]]", "[[Morella (short story)|Morella]]")
*Resurrection (see also: "[[The Fall of the House of Usher]]", "[[Morella (short story)|Morella]]", "[[Metzengerstein]]")
*Substance abuse (see also: "[[The Black Cat (short story)|The Black Cat]]", "[[Hop-Frog]]")

==Publication history==
"Ligeia" was first published in the September 18, 1838, edition of the ''[[Nathan C. Brooks#The American Museum|American Museum]]'', a magazine edited by two of Poe's friends, Dr. [[Nathan C. Brooks]] and Dr. Joseph E. Snodgrass. The magazine paid Poe $10 for "Ligeia".<ref>Ostram, John Ward. "Poe's Literary Labors and Rewards" in ''Myths and Reality: The Mysterious Mr. Poe''. Baltimore: The Edgar Allan Poe Society, 1987: 38.</ref>

The story was extensively revised throughout its publication history. It was reprinted in ''[[Tales of the Grotesque and Arabesque]]'' (1840), the one volume of ''Phantasy Pieces'' (1842), and ''Tales by Edgar Allan Poe'' (1845), the ''New York World'' (February 15, 1845), and the ''[[Broadway Journal]]'' (September 27, 1845). The poem "[[The Conqueror Worm]]" was first incorporated into the text (as a poem composed by Ligeia) in the ''New York World''.<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001: 134. ISBN 0-8160-4161-X</ref>

==Critical reception==
Charles Eames of ''The New World'' commented: "The force and boldness of the conception and the high artistic skill, with which the writer's purpose is wrought out, are equally admirable".<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 502. ISBN 0-8161-8734-7</ref> [[Thomas Dunn English]], writing in the October 1845 ''Aristidean'', said that "Ligeia" was "the most extraordinary, of its kind, of his productions".<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 586–587. ISBN 0-7838-1401-1</ref>

Irish critic and playwright [[George Bernard Shaw]] said, "The story of the Lady Ligeia is not merely one of the wonders of literature: it is unparalleled and unapproached".

==Film, TV or theatrical adaptations==
[[Roger Corman]] adapted the story into ''[[The Tomb of Ligeia]]'' in 1964. It would be the last of Corman's eight film adaptations of works by [[Edgar Allan Poe]].

Ligeia's theme of the death and resurrection of a beloved woman was subsequently developed by Alfred Hitchcock in ''[[Vertigo (film)|Vertigo]]''.

The story has also recently been adapted into the [[2008 in film|2008]] independent feature originally known by the title ''Edgar Allan Poe's Ligeia'' but later renamed to ''The Tomb'', by writer John Shirley and produced by Jeff Most, [[Donald P. Borchers]]. The film stars [[Wes Bentley]], [[Michael Madsen]], and [[Eric Roberts]].

==References==
{{reflist|2}}

==External links==
{{wikisource}}
{{Commons category|Ligeia (Poe)}}
*[http://www.cummingsstudyguides.net/Guides2/Ligeia.html Ligeia: A Study Guide]
* {{librivox book | title=Ligeia | author=Edgar Allan Poe}}

{{Edgar Allan Poe}}

[[Category:Short stories by Edgar Allan Poe]]
[[Category:1838 short stories]]
[[Category:Short stories adapted into films]]