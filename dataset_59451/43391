<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
<!-- text begins below this table - scroll down to edit-->
{|{{Infobox Aircraft Begin
 |name = Pfalz D.XII
 |image =PfalzD12.jpg|caption =
}}{{Infobox Aircraft Type
 |type = Fighter
 |manufacturer = [[Pfalz Flugzeugwerke]] GmbH
 |designer =Rudolph Gehringer
 |first flight = March [[1918 in aviation|1918]]
 |introduced = 
 |retired =
 |status =
 |primary user = ''[[Luftstreitkräfte]]''
 |more users =
 |produced = 
 |number built = approximately 800
 |unit cost =
 |variants with their own articles =
}}
|}
The '''Pfalz D.XII''' was a [[Germany|German]] [[fighter aircraft]] built by [[Pfalz Flugzeugwerke]]. Designed by Rudolph Gehringer as a successor to the [[Pfalz D.III]], the D.XII entered service in significant numbers near the end of the [[World War I|First World War]]. It was the last Pfalz aircraft to see widespread service. Though the D.XII was an effective fighter aircraft, it was overshadowed by the highly successful [[Fokker D.VII]].

==Design and development==
[[File:Pf12.jpg|thumb|right|Test pilot Otto August in an early '''Pfalz D.XII''']]
In early 1918, the ''[[Idflieg]]'' (''Inspektion der Fliegertruppen'') distributed to German aircraft manufacturers a detailed engineering report on the [[SPAD S.VII]], whose wing structure ''Idflieg'' considered to be well-designed.<ref name="Herris p87">Herris 2001, p. 87.</ref> Pfalz accordingly produced several [[Pfalz D.III]]-derived prototypes with SPAD-type wings.<ref name="Herris p87"/>  These developed into the Pfalz D.XII. The new aircraft was powered by the 180&nbsp;hp [[Mercedes D.III]]aü engine and continued the use of [[Luft-Fahrzeug-Gesellschaft|LFG-Roland]]'s patented ''Wickelrumpf'' plywood-skinned monocoque fuselage construction.<ref group=Note>In ''Wickelrumpf'' construction, two layers of thin plywood strips were placed over a mold to form one half of a fuselage shell. The fuselage halves were then glued together, covered with a layer of fabric, and doped. [[Luft-Fahrzeug-Gesellschaft|LFG-Roland]] introduced the construction method in the [[Roland C.II]], and licensed it for use in the Pfalz D.III.</ref> Unlike the earlier aircraft, the D.XII used a two-bay wing cellule. Furthermore, the flush wing radiator was replaced with a car-type radiator mounted in front of the engine.<ref name="Gray p192">Gray and Thetford 1962, p. 192.</ref>

The prototype D.XII first flew in March 1918. Subsequently, ''Idflieg'' issued a production order for 50 aircraft. Pfalz entered several D.XII prototypes in the second fighter competition at [[Adlershof]] in May/June 1918. Only [[Ernst Udet]] and Hans Weiss favored the D.XII over the Fokker D.VII,<ref name="Gray p191">Gray and Thetford 1962, p. 191.</ref> but Udet's opinion carried such weight that Pfalz received substantial production orders for the D.XII.<ref name="Weyl p322">Weyl 1965, p. 322.</ref> The aircraft passed its ''Typenprüfung'' (official type test) on 19 June 1918.

Difficulties with the radiator, which used vertical tubes rather than the more common honeycomb structure, delayed initial deliveries of the D.XII until June.<ref name="Herris p87"/> The first 200 production examples could be distinguished by their rectangular fin and rudder. Subsequent aircraft featured a larger, rounded rudder profile.

==Operational use==
[[File:Pfd12.jpg|thumb|right|Captured '''Pfalz D.XII''' (serial 1970/18) in Canada after the war]]
[[File:Pd12.jpg|thumb|right|'''Pfalz D.XII''' (serial 1443/18)]]
The D.XII began reaching the ''Jagdstaffeln'', primarily Bavarian units, in July 1918. Most units operated the D.XII in conjunction with other fighter types, but units in quieter sectors of the front were completely equipped with the D.XII.

While the D.XII was a marked improvement over the obsolescent [[Albatros D.V]]a and Pfalz D.IIIa, it nevertheless found little favor with German pilots, who strongly preferred the Fokker D.VII.<ref name="VanWyngarden p88">VanWyngarden 2006, p. 88.</ref> ''Leutnant'' Rudolf Stark, commander of ''Jasta'' 35, wrote:

{{cquote|No one wanted to fly those Pfalzs except under compulsion, and those who had to made as much fuss as they could about practicing on them.

Later their pilots got on very well with them. They flew quite decently and could always keep pace with the Fokkers; in fact they dived even faster. But they were heavy for turns and fighting purposes, in which respect they were not to be compared with the Fokkers. The Fokker was a bloodstock animal that answered to the slightest movement of the hand and could almost guess the rider's will in advance. The Pfalz was a clumsy cart-horse that went heavy in the reins and obeyed nothing but the most brutal force.

Those who flew the Pfalzs did so because there were no other machines for them. But they always gazed enviously at the Fokkers and prayed for the quick chance of an exchange.<ref name="VanWyngarden p86">VanWyngarden 2006, p. 86.</ref>}} 
Thanks to its sturdy wing and thin airfoil section, the D.XII maintained the excellent high-speed dive characteristics of the earlier Pfalz D.III. Like most contemporary fighters, however, the D.XII had an abrupt stall and a pronounced tendency to spin.<ref name="Weyl p322">Weyl 1965, p. 322.</ref> Furthermore, pilots consistently criticized the D.XII for its long takeoff run, heavy controls, and "clumsy" handling qualities in the air.<ref name="Herris p101">Herris 2001, p. 101.</ref><ref name="VanWyngarden p85">VanWyngarden 2006, p. 85.</ref> Rate of roll, in particular, appears to have been deficient.<ref name="Herris p101"/>  Landings were difficult because the D.XII tended to float above the ground and the landing gear was weak.<ref name="Herris p101"/> Ground crews disliked the extensive wire bracing of the two-bay wings, which required more maintenance than the Fokker D.VII's semi-cantilever wings.<ref name="Gray p191"/> Evaluations of captured aircraft by Allied pilots were similarly unfavorable.<ref name="Herris p101-102">Herris 2001, pp. 101–102.</ref>

Between 750 and 800 D.XII scouts were completed by the Armistice. A substantial number, perhaps as many as 175, were surrendered to the Allies. Of these, a few were shipped to the United States and Canada for evaluation.

==Variants==

=== Pfalz experimental D types ===
During the development of the D.XII, Pfalz produced several [[Pfalz D.III]]-derived prototypes with SPAD-type wings and Windhoff "ear" radiators.<ref name="Herris p87"/><ref name="Gray, Peter and Owen Thetford p504">Gray and Thetford 1962, p. 504.</ref>

===Pfalz D.XIIf===
The overcompressed [[BMW IIIa]] engine would have provided improved performance in the D.XIIf variant. Records show that Pfalz received 84 such engines between July and October 1918, but there is no photographic evidence of any production D.XII equipped with the BMW IIIa.<ref name="Herris p87"/> In his autobiography, [[Anthony Fokker]] claimed that pilots deliberately wrecked D.XIIf aircraft so the engines could be salvaged and installed on Fokker D.VIIs.<ref name="Weyl p322"/>

===Pfalz D.XIV===
[[File:DXIV.jpg|thumb|right|'''Pfalz D.XIV''' (serial 2800/18)]]
The Pfalz D.XIV was a derivative of the D.XII, utilizing the same fuselage and basic wing structure. The D.XIV differed primarily by replacing the 180&nbsp;hp Mercedes D.IIIaü with the 200&nbsp;hp [[Benz Bz.IV]]ü, a substantially heavier engine.<ref name="Herris p104">Herris 2001, p. 104.</ref> To cope with the increased power and weight, the D.XIV featured longer span wings and an enlarged vertical stabilizer.<ref name="Herris p104"/> Enlarged ailerons were used to maintain rate of roll. A few prototypes were tested at the second Adlershof competition and a small production order ensued.<ref name="Herris p104"/> Production was quickly terminated, however, and the D.XIV did not see active service. The D.XIV did not offer an appreciable increase in performance over the D.XII, and the Benz Bz.IVü engine was needed for reconnaissance aircraft.<ref name="Herris p104"/>

==Survivors==
[[File:pfdd12.jpg|thumb|'''Pfalz D.XII''' at the [[National Air and Space Museum]]. The aircraft wears spurious markings from the movie ''[[The Dawn Patrol (1930 film)|The Dawn Patrol]]'']]
[[File:Pfalz D.XII Serial 2600-18.jpg|thumb|'''D.XII''' at the [[Australian War Memorial]]'s ANZAC Hall.]]
* In the 1920s, two D.XIIs were sold as war surplus to the Crawford Aeroplane & Supply Co. of Venice, California. Though badly deteriorated, the aircraft briefly appeared as props in the 1930 movie ''[[The Dawn Patrol (1930 film)|The Dawn Patrol]]''.<ref name="Wynne p97">Wynne 1987, p. 97.</ref> Both D.XIIs were eventually sold to private collectors. Today, one of these aircraft is now displayed at the [[Seattle Museum of Flight]], after it was acquired from the defunct [[Champlin Fighter Museum]], in Mesa, Arizona.  The second is exhibited at the [[National Air and Space Museum]], in Washington D.C.
* A preserved D.XII aircraft is also displayed at the [[Musée de l'Air et de l'Espace]] in Paris.
* Serial 2600/18 was one of several Pfalz D.XIIs awarded to Australia in 1919 under the terms of the Armistice. Its service history is unknown. In late 1919, the aircraft was shipped from 2nd Aircraft Salvage Depot in France to England, and subsequently to Australia. It was temporarily exhibited in Melbourne and Adelaide in 1920. In 1924, the aircraft went on display in Sydney.

:Serial 2600/18 was removed to storage in 2001. After an extensive restoration at the Treloar Technology Centre in Canberra, the aircraft went on display at the AWM's ANZAC Hall in 2008.<ref>[http://cas.awm.gov.au/item/RELAWM04805 "RELAWM04805 - Pfalz D.XII Scout Aircraft."] ''Australian War Memorial''. Retrieved: 28 May 2012.</ref><ref>Goddard, Chris. [http://www.southsearepublic.org/2004_2002/aircraft/museum/awm_pfalz.html "Pfalz DXII, Australian War Memorial, Canberra, Australia."] ''Museum Aircraft of the Australian Flying Corps''. Retrieved: 28 May 2012.</ref>

==Operators==

===Military operators===
;{{flag|German Empire}}
* ''[[Luftstreitkräfte]]''
;{{POL}}
* [[Polish Air Force]] (2 aircraft postwar)

===Civil operators===
;{{flag|United States|1912}}
* [[Paramount Pictures]] property manager Louis Kinnell took one airframe to the shops of Chaffee Junior College and restored it to flying condition. This aircraft was kept at Dycer Field ([[Los Angeles]], California) and was flown without registration for a short time in 1939.

==Specifications (D.XII)==
[[File:Pfalz D.XII LeB 05.07R.jpg|thumb|right|'''Pfalz D.XII''' (serial 2690/18) displayed at the [[Musée de l'Air et de l'Espace]]]]
{{Aircraft specs
|ref=German Aircraft of the First World War<ref name="Gray p193-194">Gray and Thetford 1962, pp. 193–194.</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|crew=1
|length m=6.35
|span m=9
|height m=2.7
|height ft=
|height in=
|height note=
|wing area sqm=21.7
|empty weight kg=716
|gross weight kg=897
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Mercedes D.IIIa]]
|eng1 type=6-cyl water-cooled in-line piston engine
|eng1 hp=160<!-- prop engines -->
<!--
        Performance
-->
|max speed kmh=170
|endurance=<!-- if range unknown -->2½ hours
|ceiling ft=18,500
|climb rate ms=4.09
|time to altitude=<br/>
**{{convert|1,000|m|ft|abbr=on|0}} in 3 minutes 24 seconds
**{{convert|5,000|m|ft|abbr=on|0}} in 29 minutes 54 seconds
;
<!--
        Armament
-->
|guns= 2 ×  {{convert|7.92|mm|in|abbr=on|3}} [[LMG 08/15]] [[machine gun]]s
}}

==See also==
{{aircontent
|related=
|similar aircraft=
|sequence=
|lists=
* [[List of military aircraft of Germany]]
* [[List of fighter aircraft]]
|see also=
}}

==References==
;Footnotes
{{Reflist|group=Note}}
;Citations
{{reflist|2}}
;Bibliography
{{Refbegin}}
* Gray, Peter and Owen Thetford. ''German Aircraft of the First World War''. London: Putnam, 1962. ISBN 0-933852-71-1.
* Herris, Jack. ''Pfalz Aircraft of World War I (Great War Aircraft in Profile, Volume 4)''. Boulder, Colorado: Flying Machine Press, 2001. ISBN 1-891268-15-5.
* VanWyngarden, Greg. ''Pfalz Scout Aces of World War I (Aircraft of the Aces No. 71)''. Oxford, UK: Osprey Publishing, 2006. ISBN 1-84176-998-3.
* Weyl, Alfred Richard. ''Fokker: The Creative Years''. London: Putnam, 1965. ISBN 0-85177-817-8.
* Wynne, H. Hugh. ''The Motion Picture Stunt Pilots and Hollywood's Classic Aviation Movies.'' Missoula, Montana: Pictorial Histories Publishing Company, 1987. ISBN 0-933126-85-9.
;Further reading
* {{cite journal |date=24 April 1919 |title=The Pfalz (D XII) Single-Seater Fighter |format=PDF |journal=[[Flight (magazine)|Flight]] |volume=XI |issue=17 |id=No. 539 |pages=528–533 |url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200528.html |accessdate=12 January 2011}} 
{{Refend}}

==External links==
{{commons category|Pfalz D.XII}}
* [http://collections.nasm.si.edu/code/emuseum.asp?profile=objects&newstyle=single&quicksearch=A19540015000 Smithsonian Collection Pfalz D.XII]

{{Pfalz aircraft}}
{{Idflieg fighter designations}}
{{World War I Aircraft of the Central Powers}}
{{wwi-air}}

[[Category:German fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Pfalz aircraft|D.XII]]