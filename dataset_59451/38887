{{good article}}
{{infobox country at games
| NOC = BRU
| NOCname = [[Brunei Darussalam National Olympic Council]]
| games = Summer Olympics
| year = 2004
| flagcaption = 
| oldcode = 
| website = {{url|www.bruneiolympic.org }}
| location = [[Athens]]
| competitors = '''1'''
| sports = 1
| flagbearer = [[Jimmy Anak Ahar]]
| rank = 
| gold = 0
| silver = 0
| bronze = 0
| officials = 
| appearances = auto
| app_begin_year = 
| app_end_year = 
| summerappearances = 
| winterappearances = 
| seealso = 
}}

[[Brunei]], as ''Brunei Darussalam'', competed at the [[2004 Summer Olympics]] in [[Athens]], [[Greece]], which took place between 13 and 29 August 2004. The country's participation in Athens marked its fourth appearance at the Summer Olympics since its debut in the [[1988 Summer Olympics]]. 

The Brunei delegation included only one track and field athlete, meaning Brunei, along with [[British Virgin Islands]] and [[Liechtenstein]], sent the lowest number of athletes to the 2004 Summer Games at one. The athlete selected was [[Jimmy Anak Ahar]], who was a middle-distance runner that was selected via a wildcard as the nation had no athletes that met the "A" or "B" standards in any event. Ahar was also selected as [[List of flag bearers for Brunei at the Olympics|flag bearer]] for the [[2004 Summer Olympics opening ceremony|opening ceremony]]. Ultimately, Ahar did not progress beyond the heats, meaning Brunei won no medals at this Summer Olympics.

==Background==
Although Brunei first participated in the Summer Olympic in the [[1988 Summer Olympics]] in [[ Seoul]], [[South Korea]], it was only represented by one official.<ref>{{cite book | url=http://library.la84.org/6oic/OfficialReports/1988/1988v2.pdf | title=Official Report, Games of the XXIVth Olympiad Seoul 1988, Volume 2: Competition Summary and Results | publisher=Seoul Olympic Organizing Committee | year=1989 | pages=153}}</ref> It would not be until the [[1996 Summer Olympics]] in [[Atlanta]], United States that the country would send athletes to the Games. Since then, it has participated in four Summer Olympics between its debut and the 2004 Summer Olympics.<ref name="Sports Reference - Countries - Brunei">{{cite web | url=http://www.sports-reference.com/olympics/countries/BRU/ | title=Brunei | publisher=[[Sports Reference]] | accessdate=2 July 2016}}</ref> No Brunei athlete had ever won a medal at the Summer Olympics before the 2004 Athens Games.<ref name="Sports Reference - Countries - Brunei" /> 

The Brunei [[National Olympic Committee]] (NOC) selected one athlete via a wildcard. Usually, a NOC would be able to enter up to three qualified athletes in each individual event as long as each athlete met the "A" standard, or one athlete per event if they met the "B" standard.<ref name="Entry Standards">{{cite web | url=http://www.iaaf.org/news/news/olympic-games-athens | title=IAAF Games of the XXX Olympiad – Athens 2004 Entry Standards | publisher=[[IAAF]] | accessdate=2 July 2016}}</ref> However, since Brunei had no athletes that met either standard, they were allowed to select an athlete as a wildcard. The one athlete that was selected to compete in the Athens Games was Jimmy Anak Ahar in the Men's [[Athletics at the 2004 Summer Olympics – Men's 1500 metres|1500 meters]].<ref name="Sports Reference - Countries - Brunei - 2004">{{cite web | url=http://www.sports-reference.com/olympics/countries/BRU/summer/2004/ | title=Brunei at the 2004 Athina Summer Games | publisher=Sports Reference | accessdate=2 July 2016}}</ref> Sending only one athlete to the Athens Games meant that the country, along with British Virgin Islands and Liechtenstein, was notable for sending the lowest number of athletes to the 2004 Summer Games.<ref name="Naplesnews">{{cite news | access-date=2 July 2016 | archive-date=5 August 2011 | archive-url=https://web.archive.org/web/20110805223908/http://www.naplesnews.com/news/2004/aug/26/ndn_olympics__one_is_the_lonliest_number_for_this_/ | url=http://www.naplesnews.com/news/2004/aug/26/ndn_olympics__one_is_the_lonliest_number_for_this_/ | title=Olympics: One is the lonliest number for this trio | publisher=[[Naples Daily News]] | date=26 August 2004 | last=Tomasson | first=Chris}}</ref> Ahar was flag bearer for the opening ceremony.<ref name="Flag Bearers">{{cite web | access-date=22 July 2016 | archive-date=3 July 2015 | archive-url=https://web.archive.org/web/20150703042916/http://www.olympic.org/content/news/media-resources/manual-news/1999-2009/2004/08/13/flag-bearers-for-the-opening-ceremony/ | url=http://www.olympic.org/content/news/media-resources/manual-news/1999-2009/2004/08/13/flag-bearers-for-the-opening-ceremony/ | title=2004 Athens: Flag Bearers for the opening ceremony | publisher=[[Olympics]] | date=13 August 2004}}</ref> Among officials, sports official Sofian Ibrahim represented the country.<ref name="Naplesnews" />  

==Athletics ==
{{main|Athletics at the 2004 Summer Olympics}}
[[File:Olympic stadium,Athens 19.JPG|thumb|250px|right|The [[Olympic Stadium (Athens)|Athens Olympic Stadium]], where Ahar competed in the Men's 1500 meters]]
Making his Summer Olympic debut, Jimmy Anak Ahar was notable for becoming the youngest ever competitor to represent Brunei at the Olympics aged 22. The age record stood until the [[2012 Summer Olympics]], when [[Anderson Chee Wei Lim]] surpassed it.<ref>{{cite web | url=http://www.sports-reference.com/olympics/athletes/an/jimmy-anak-ahar-1.html | title=Jimmy Anak Ahar | publisher=Sports Reference | accessdate=2 July 2016}}</ref><ref name="Flag Bearers" /><ref name="Sports Reference - Countries - Brunei" /> Ahar qualified for the 2004 Athens Games as a wildcard, as his best time, three minutes, 59.81 seconds at the [[2003 Southeast Asian Games]] Men's 1500 meters, was 21.81 seconds slower than the "B" qualifying standard required.<ref name="IAAF - Athletes - Brunei - Jimmy Anak Ahar - Progression">{{cite web | url=http://www.iaaf.org/athletes/brunei/jimmy-anak-ahar-177680 | title=IAAF – Athletes – Brunei – Jimmy Anak Ahar – Progression | publisher=[[IAAF]] | accessdate=2 July 2016}}</ref><ref name="Entry Standards" /> He competed on 20 August in the Men's 1500 meters against thirteen other athletes in the third heat. He ran a time of 4 minutes, 14.11 seconds, finishing 13th.<ref name="IAAF – Results – Olympic Games – 2004 – Men – 1500 meters – Heats – Results">{{cite web | url=http://www.iaaf.org/results/olympic-games/2004/28th-olympic-games-3201/men/1500-metres/heats/result | title=IAAF – Results – Olympic Games – 2004 – Men – 1500 meters – Heats – Results | publisher=IAAF | accessdate=2 July 2016}}</ref> He ranked ahead of [[Tanzania]]'s [[Samwel Mwera]] (who did not start), and behind [[Guam]]'s [[Neil Weare]] (4&nbsp;minutes, 5.86&nbsp;seconds), in a heat led by [[Great Britain]]'s [[Michael East (athlete)|Michael East]] (3&nbsp;minutes, 37.37&nbsp;seconds). Overall, Ahar placed 39th out of the 41 athletes that competed{{efn|Two athletes, [[Peter Roko Ashak]] and Samwel Mwera, did not start.<ref name="IAAF – Results – Olympic Games – 2004 – Men – 1500 meters – Heats – Results" />|name=DQ/DNS}}, and was 32.97 seconds behind the slowest athlete that progressed to the next round. Therefore, that was the end of his competition.<ref name="IAAF – Results – Olympic Games – 2004 – Men – 1500 meters – Heats – Summary">{{cite web | url=http://www.iaaf.org/results/olympic-games/2004/28th-olympic-games-3201/men/1500-metres/heats/summary | title=IAAF – Results – Olympic Games – 2004 – Men – 1500 meters – Heats – Summary | publisher=IAAF | accessdate=2 July 2016}}</ref>

<small>
;Key
*'''Note'''&ndash;Ranks given for track events are within the athlete's heat only
*'''Q''' = Qualified for the next round
*'''q''' = Qualified for the next round as a fastest loser ''or'', in field events, by position without achieving the qualifying target
*'''NR''' = National record
*N/A = Round not applicable for the event
*Bye = Athlete not required to compete in round
</small>

;Men
{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Semifinal
!colspan="2"|Final
|- style="font-size:95%"
!Result
!Rank
!Result
!Rank
!Result
!Rank
|-align=center
|align=left|[[Jimmy Anak Ahar]]
|align=left|[[Athletics at the 2004 Summer Olympics – Men's 1500 metres|1500 m]]
|4:14.11
|13
|colspan=4|Did not advance
|}

==Notes==
{{notelist}}

==References==
{{reflist}}

{{Nations at the 2004 Summer Olympics}}

{{DEFAULTSORT:Brunei At The 2004 Summer Olympics}}
[[Category:Nations at the 2004 Summer Olympics]]
[[Category:Brunei at the Summer Olympics by year|2004]]
[[Category:2004 in Brunei|Summer Olympics]]