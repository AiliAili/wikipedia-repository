{{Use dmy dates|date=September 2015}}
[[File:The South Front of West Wycombe Park.jpg|thumb|300x300px|The double, superimposed [[colonnade]] on the south front of West Wycombe. It has a Tuscan order ground floor and Corinthian upper floor with a central projecting pediment, very unusual in English architecture. (''Marked '''M''' on plan below'')]]
[[File:Sirfrancisdashwood.jpg|thumb|right|200px|[[Francis Dashwood, 11th Baron le Despencer|Sir Francis Dashwood, 2nd Baronet]] (1708–1781), notorious "bon vivant" and builder of West Wycombe, dressed in his "Ottoman" dining club garb.]]

'''West Wycombe Park''' is a [[English country house|country house]] built between 1740 and 1800 near the village of [[West Wycombe]] in Buckinghamshire, England. It was conceived as a pleasure palace for the 18th-century [[libertine]] and [[wikt:dilettante|dilettante]] [[Francis Dashwood, 11th Baron le Despencer|Sir Francis Dashwood, 2nd Baronet]]. The house is a long rectangle with four façades that are [[column]]ed and [[pediment]]ed, three theatrically so.  The house encapsulates the entire progression of British 18th-century architecture from early idiosyncratic [[Palladian]] to the [[Neoclassical architecture|Neoclassical]], although anomalies in its design  make it architecturally unique. The mansion is set within an 18th-century landscaped park containing many small temples and [[folly|follies]], which act as satellites to the greater temple, the house.

The house, which is a Grade I [[listed building]],<ref>{{IoE|46157}}</ref> was given to the [[National Trust for Places of Historic Interest or Natural Beauty|National Trust]] in 1943 by [[Dashwood baronets|Sir John Dashwood, 10th Baronet]] (1896–1966), an action strongly resented by his heir.<ref>Knox p 62.</ref> Dashwood retained ownership of the surrounding estate and the contents of the house, most of which he sold; after his death, the house was restored at the expense of his son, the 11th Baronet. Today, while the structure is owned by the National Trust, the house is still the home of the Dashwood family. The house is open to the public during the summer months and is a venue for civil weddings and corporate entertainment, which help to fund its maintenance and upkeep.

==Architecture==

===Ethos===
[[Image:Palazzo Chiericati.jpg|thumb|150px|left|[[Palazzo Chiericati]] by [[Palladio]] (c.1550) has a superposed colonnade similar to that at West Wycombe, but the inspiration for the south front may have been Palladio's reconstruction of [[Vitruvius]]'s Roman villa illustrated in his ''[[Quattro Libri]]''.]]

[[Image:Villa La Rotonda.JPG|thumb|150px|right|Palladio's [[Villa Capra "La Rotonda"|Villa Capra detta La Rotonda]] was the inspiration for [[Mereworth Castle]], the home of Dashwood's uncle, [[John Fane, 7th Earl of Westmorland|Lord Westmorland]]. West Wycombe's east front was in turn built as a homage to Westmorland's artistic taste at Mereworth.<ref>Knox p 9.</ref>]]

West Wycombe Park, architecturally inspired by the villas of the [[Veneto]] constructed during the late-[[renaissance]] period, is not one of the largest, grandest or best-known of England's many country houses.  Compared to its [[Palladian]] contemporaries, such as [[Holkham Hall]], [[Woburn Abbey]] and [[Ragley Hall]], it is quite small, yet it is architecturally important as it encapsulates a period of 18th-century English social history, when young men, known as [[dilettanti]], returning from the nearly obligatory [[Grand Tour]] with newly purchased acquisitions of art, often built a country house to accommodate their collections and display in stone the learning and culture they had acquired on their travels.<ref>Girouard p 177.</ref>

The West Wycombe estate was acquired by [[Sir Francis Dashwood, 1st Baronet]], and his brother [[Samuel Dashwood|Samuel]] in 1698. Dashwood demolished the existing [[manor house]] and built a modern mansion on higher ground nearby. This mansion forms the core of the present house. Images of the house on early estate plans show a red-brick house with stone dressings and a hipped roof in the contemporary [[Queen Anne style architecture|Queen Anne style]].<ref>National Trust p. 7.</ref> In 1724, Dashwood bequeathed this square conventional house to his 16-year-old son, the 2nd Baronet, also [[Francis Dashwood, 11th Baron le Despencer|Francis]], who later inherited the title [[Baron le Despencer]] through his mother and is perhaps best known for establishing the [[Hellfire Club]] close to the mansion, in the [[West Wycombe Caves]]. Between 1726 and 1741, Dashwood embarked on a series of Grand Tours: the ideas and manners he learned during this period influenced him throughout his life and were pivotal in the rebuilding of his father's simple house, transforming it into the classical edifice that exists today.<ref name="NGA">{{citation |last=Knox |first=Tim |year=2008 |title=Sir Francis Dashwood of West Wycombe Park, Buckinghamshire, as a Collector of Ancient and Modern Sculpture |journal=Studies in the History of Art |volume=70 |publisher=National Gallery of Art |pp=396–419 |jstor=42622688}}</ref>
[[Image:Farnesina frescoes.jpg|thumb|150px|left|[[Fresco]]es in the [[Villa Farnesina]] (c.1510) inspired the decoration of West Wycombe's interior.]]

West Wycombe has been described as "one of the most theatrical and Italianate mid-18th century buildings in England".<ref>All about Britain.com</ref> Of all the 18th-century country houses, its façades replicate in undiluted form not only the [[Classical architecture|classical]] [[villa]]s of Italy on which [[Palladianism]] was founded, but also the temples of [[Classical antiquity|antiquity]] on which [[Neoclassicism]] was based.  The [[Doric order|Greek Doric]] of the house's west portico is the earliest example of the [[Greek revival]] in Britain.<ref name=Knox7/>

The late 18th century was a period of change in the interior design of English country houses. The [[Baroque]] concept of the principal floor, or ''[[piano nobile]]'', with a large bedroom suite known as the [[state apartments]], was gradually abandoned in favour of smaller, more private bedrooms on the upper floors.<ref>Girouard p 230.</ref> The principal floor became a series of reception rooms, each with a designated purpose, creating separate [[withdrawing room|withdrawing]], dining, music, and ballrooms. In the late-18th century, it became common to arrange reception and public rooms on a lower floor, with bedrooms and more private rooms above.<ref>Girouard pp 220, 230.</ref> West Wycombe perfectly reflects this arrangement.

===Exterior===
[[Image:North Front of West Wycombe.jpg|thumb|right|300px|The north front of West Wycombe (''marked '''O''' on plan below'')]]

The builder of West Wycombe, Sir Francis Dashwood, 2nd Baronet, employed three architects and two [[landscape architect]]s in the design of the house and its grounds.  He had a huge input himself: having made the [[Grand Tour]] and seen the villas of the [[Italian renaissance]] first hand, he wished to emulate them.<ref>Dashwood pp 18, 192, 194.</ref>

Work began in about 1735 and continued until Dashwood's death in 1781, when the older house had been transformed inside and out.<ref>Greeves p 334; National Trust p 4.</ref> The long building time partly explains the flaws and variations in design: when building commenced, Palladianism was the height of fashion, but by the time of its completion, Palladianism had been succeeded by Neoclassicism; thus, the house is a marriage of both styles. While the marriage is not completely unhappy, the Palladian features are marred by the lack of [[Palladio]]'s proportions: the east portico is asymmetrical with the axis of the house, and trees were planted either side to draw the eye away from the flaw.<ref>National Trust pp 12–13.</ref>

[[Image:East Portico, West Wycombe.png|thumb|left|250px|1781 view of the south façade, showing the trees planted to hide the asymmetrical east [[portico]]. The building to the left is the "Temple of [[Apollo]]".]]

The finest architects of the day submitted plans to transform the older family house into a modern architectural extravaganza.  Among them was [[Robert Adam]], who submitted a plan for the west portico, but his idea was dropped.<ref>National Trust p 10.</ref><ref>Pevsner p 286 attributes the adjoining, but now semi-demolished, service block and stables to [[Robert Adam]]. This attribution is not repeated in other reference books.</ref> The architect [[Nicholas Revett]] was consulted and created the west portico.<ref name="d194">Dashwood p 194.</ref> Today, the first sight of the house as approached from the drive is this west end of the house, which appears as a Grecian temple. The eight-columned portico, inspired by the [[Temple of Bacchus]] in [[Teos]] was completed by 1770, and is considered to be the earliest example of [[Greek revival architecture]] in Britain.<ref name=Knox7>Knox p 7.</ref> The opposite (east) end of the house, designed by [[John Donowell]] and completed c. 1755,<ref name="d194" /> appears equally temple-like, but this time the muse was the [[Villa Rotunda]] in [[Vicenza]]. Thus the two opposing porticos, east and west, illustrate two architectural styles of the late-18th century: the earlier Roman inspired Palladian architecture and the more Greek inspired Neoclassicism.<ref>National Trust pp 10, 12.</ref>

[[Image:West Wycombe Park 1 (Giano).png|thumb|right|250px|West Wycombe's east portico, with planting attempting to hide its asymmetrical alignment with the house (''marked '''N''' on plan below'')]]

The principal façade is the great south front, a two-storey [[colonnade]] of [[Corinthian order|Corinthian]] columns superimposed on [[Tuscan order|Tuscan]], the whole surmounted by a central pediment. The [[column]]s are not stone, but wood coated in [[stucco]]. This is particularly interesting, as cost was no object in the house's construction. The architect for this elevation was John Donowell, who executed the work between 1761 and 1763 (although he had to wait until 1775 for payment<ref>Wallace p 12.</ref>). The façade, which has similarities to the main façade of Palladio's [[Palazzo Chiericati]] of 1550, was originally the entrance front. The front door is still in the centre of the ground floor leading into the main entry hall.<ref>National Trust p 13.</ref> This is a substantial deviation from the classical form of English Palladianism in which the main entrance and principal rooms would be on the first floor reached by an outer staircase, giving the main reception rooms elevated views, with the ground floor given over to service rooms.<ref>Girouard p 160.</ref>

The more severe north front is of eleven [[Bay (architecture)|bays]], with the end bays given significance by [[rustication (architecture)|rustication]] at ground floor level. The centre of the façade has [[Ionic order|Ionic]] columns supporting a pediment and originally had the Dashwood [[coat of arms]]. This façade is thought to date from around 1750–51, although its segmented windows suggest it was one of the first of the 2nd Baronet's improvements to the original house to be completed, as the curved or segmented window heads are symbolic of the earlier part of the 18th century.<ref>Pevsner p 283.</ref>
[[St Lawrence's Church, West Wycombe]], referred to by tourists as "The Golden Ball" is located adjacent to the Dashwood Mausoleum, atop West Wycombe Hill. It is visible from West Wycombe Park and was intended to be part of the landscaping. The church was rebuilt in the 18th century by Sir Edward Dashwood and his architect, it was completed in 1763 by either or both of the following architects, Nicholas Revett and John Donowell. It shows a grand interior in the classical style, which adjoins an extended medieval tower topped by a golden ball.

===Interior===
[[Image:West Wycombe Plan.png|thumb|right|400px|Room plan of the ground floor. Key: '''A''' Hall; '''B''' Saloon; '''C''' Red Drawing room; '''D''' Study; '''E''' Music room; '''F''' Blue Drawing Room; '''G''' Staircase; '''H''' Dining Room; '''J''' Tapestry Room; '''K''' King's Room (former principal bedroom); '''L''' West Portico; '''M''' South Front and colonnade; '''N''' East Portico; '''O''' North Front; '''P''' service wing.]]

The principal reception rooms are on the ground floor with large [[sash window]]s opening immediately into the porticos and the colonnades, and therefore onto the gardens, a situation unheard of in the grand villas and palaces of [[Renaissance]] Italy. The mansion contains a series of 18th century salons decorated and furnished in the style of that period, with [[polychrome]] marble floors, and painted ceilings depicting classical scenes of [[Greek mythology|Greek]] and [[Roman mythology]]. Of particular note is the entrance hall, which resembles a Roman [[Atrium (architecture)|atrium]] with marbled columns and a painted ceiling copied from Robert Wood's ''Ruins of Palmyra''.<ref>Dashwood p 196.</ref>

Many of the reception rooms have painted ceilings copied from Italian [[palazzo|palazzi]], most notably from the [[Palazzo Farnese]] in Rome. The largest room in the house is the Music Room, which opens onto the east portico. The ceiling fresco in this room depicts the "Banquet of the Gods" and was copied from the [[Villa Farnesina]].<ref>Dashwood p 216.</ref> The Saloon, which occupies the centre of the north front, contains many marbles, including statuettes of the four seasons. The ceiling depicting "The Council of the Gods and the Admission of Psyche" is also a copy from Villa Farnesina.<ref>Dashwood pp 207–208.</ref>

[[Image:West Wycombe Park - tecto.png|thumb|left|150px|[[Giuseppe Mattia Borgnis]]'s 1752 ceiling in West Wycombe's Blue Drawing Room is a direct copy of [[Annibale Carracci]]'s original work at the [[Palazzo Farnese]].]]

The Dining Room walls are painted faux [[jasper]] and hold paintings of the house's patron—[[Francis Dashwood, 11th Baron le Despencer|Sir Francis Dashwood]]—and his fellow members of the [[Divan Club]] (a society for those who had visited the [[Ottoman Empire]]). The room also has a painted ceiling from Wood's ''Palmyra''.<ref>Dashwood pp 203–204.</ref>

The Blue Drawing Room is dominated by the elaborate painted ceiling depicting "The Triumph of Bacchus and Ariadne" (''illustrated left''). This room houses a plaster statuette of the [[Venus de' Medici]] and marks the 2nd Baronet's risqué devotion to that goddess of love. The room has walls of blue [[Flocking (texture)|flock]], applied in the 1850s and later renewed, bearing paintings from various Italian schools of the 17th century.<ref>Dashwood p 213.</ref> The Red Drawing Room is lined in crimson silk and is furnished with [[marquetry]] [[commode]]s.<ref>National Trust p 20.</ref>

The relatively small study contains plans for the house and potential impressions for various elevations. One is reputed to have been drawn by Sir Francis Dashwood himself,<ref>Dashwood p 212; National Trust p 21.</ref> while the Tapestry Room, once ante-room to the adjoining former principal bedroom, is hung with [[Brussels tapestry|Brussels tapestries]] depicting peasant scenes by [[Teniers]]. Dashwood inherited them in 1763 from his uncle [[John Fane, 7th Earl of Westmorland|Lord Westmorland]], who is said to have been given them by [[John Churchill, 1st Duke of Marlborough|the 1st Duke of Marlborough]] to celebrate their victories in the [[Low Countries]].<ref>National Trust p 17.</ref>

==Gardens and park==
{{See also|List of garden structures at West Wycombe Park}}
[[Image:Temple of Music in West Wycombe Park.jpg|thumb|right|The "Temple of Music" situated on one of the islands of the swan-shaped lake. In the background, on the hill, are the Dashwood [[mausoleum]] and church.]]
The gardens at West Wycombe Park are among the finest and most idiosyncratic 18th century gardens surviving in England.<ref>Knox, p 30.</ref> The park is unique in its consistent use of Classical architecture from both Greece and Italy. The two principal architects of the gardens were John Donowell and [[Nicholas Revett]], who designed all of the ornamental [[List of garden structures at West Wycombe Park|buildings in the park]]. The landscape architect [[Thomas Cook (landscape architect)|Thomas Cook]] began to execute the plans for the park, with a nine-acre man-made lake created from the nearby [[River Wye, Buckinghamshire|River Wye]] in the form of a swan. The lake originally had a [[snow (ship)|snow]] (a sailing vessel) for the amusement of Dashwood's guests, complete with a resident captain on board.<ref>Knox p 4.</ref><ref>Dashwood p 226.</ref> Water leaves the lake down a [[waterfall|cascade]] and into a canal pond.<ref>Dashwood p 224.</ref>

[[Georgian period in British history|Georgian]] [[English landscape garden]]s, such as West Wycombe and [[Stowe Landscape Gardens|Stowe]], are arranged as a walk or series of walks that take the visitor through a range of locations, each with its own specific character and separate from the last. Planting and the shape of the landscape is used, alongside [[folly|follies]] and man-made water features, to create pleasant vistas and set pieces centred on a building, straight avenue, serpentine walk, or viewpoint.<ref>National Trust p 25.</ref> In the later years of the 18th century, the 5,000&nbsp;acres (20&nbsp;km²) of grounds were extended to the east, towards the nearby town of [[High Wycombe]], and [[Humphrey Repton]] completed the creation of the gardens, until they appeared much as they do today.<ref>National Trust p 26.</ref>

[[Image:West wycome Temple.JPG|thumb|left|The "Temple of Apollo" was originally a gateway and later used for [[cock fighting]]; it also screened the view of the domestic [[service wing]] from the main house. (''Marked '''P''' on plan above'')]]

The park still contains many follies and temples.  The "Temple of Music" is on an island in the lake, inspired by the [[Temple of Vesta]] in Rome.  It was designed for Dashwood's [[fête champêtre|fêtes champêtres]],<ref>Jackson-Stops, p 192.</ref> with the temple used as a theatre; the remains of the stage survive.<ref>Knox, p 37.</ref> Opposite the temple is the garden's main cascade which has statues of two [[Nymph|water nymphs]]. The present cascade has been remade, as the original was demolished in the 1830s. An [[octagon]]al tower known as the "Temple of the Winds" is based in design on the [[Tower of the Winds]] in Athens.<ref>Knox p 36.</ref>

Classical architecture continues along the path around the lake, with the "Temple of [[Flora (mythology)|Flora]]", a hidden [[summer house|summerhouse]], and the "Temple of [[Daphne (mythology)|Daphne]]", both reminiscent of a small temple on the [[Acropolis]]. Another hidden temple, the "Round Temple", has a curved [[loggia]]. Nearer the house, screening the service wing from view, is a Roman [[triumphal arch]], the "Temple of [[Apollo]]", also known (because of its former use a venue for [[cock fighting]]) as "Cockpit Arch", which holds a copy of the famed [[Apollo Belvedere]]. Close by is the "Temple of [[Diana (mythology)|Diana]]", with a small niche containing a statue of the goddess. Another goddess is celebrated in the "Temple of [[Venus (mythology)|Venus]]". Below this is an [[Exedra]], a [[grotto]] (known as Venus's Parlour) and a statue of [[Mercury (mythology)|Mercury]]. This once held a copy of the [[Venus de' Medici]]; it was demolished in the 1820s but was reconstructed in the 1980s and now holds a replica of the [[Venus de Milo]].<ref>Dashwood p 225.</ref>

Later structures that break the classical theme include the [[Gothic Revival architecture|Gothic]] style [[boathouse]], a Gothic Alcove – now a romantic ruin hidden amongst undergrowth – and a Gothic Chapel, once home of the village [[shoemaking|cobbler]] (and facetiously named St Crispin's)<ref>Dashwood pp 229–230.</ref> but later used as the estate [[kennel]]s.<ref>Dashwood p 81.</ref> A monument dedicated to [[Elizabeth II|Queen Elizabeth II]] was erected on her 60th birthday in 1986.<ref>Dashwood p 224; National Trust p 28.</ref>

The gardens are listed Grade I on the [[Register of Historic Parks and Gardens of special historic interest in England|Register of Historic Parks and Gardens]].<ref name=NHLE>{{NHLE|num=1000447|desc=West Wycombe Park|access-date=9 February 2016|mode=cs2}}</ref>

==Dashwoods of West Wycombe==
{{Further|Dashwood baronets}}
[[Image:Dashwoods of West Wycombe.png|thumb|300px|right|Painting titled "Sir Francis and Lady Dashwood at West Wycombe Park", painted in 1776, with the newly completed house behind the couple.<ref>In the background on the hill is the newly completed church, yet curiously the mausoleum constructed c.1764 (by a member of the [[Bastard brothers|Bastard family]]) is absent, suggesting the attribution of date to the painting is wrong. If the date 1776 was correct then the lady portrayed is likely to be not Lady Dashwood, who died in 1769, but Frances Barry, Dashwood's lover and mother of his two children, with whom he lived after the death of his wife.</ref>]]

Sir Francis Dashwood built West Wycombe to entertain, and there has been much speculation on the kind of entertainment he provided for his guests. Judged against the sexual morals of the late 18th century, Dashwood and his clique were regarded as promiscuous; while it is likely that the contemporary reports of the [[bacchanalia]]n [[orgy|orgies]] over which Dashwood presided in the [[Hellfire Club|Hellfire]] caves above West Wycombe were exaggerated, [[free love]] and heavy drinking did take place there.<ref name="Knox">Knox p 50.</ref> Dashwood often had himself depicted in [[portrait]]s in fancy dress (in one, dressed as the pope toasting a female [[Herme]]<ref name="Knox"/>), and it is his love of fancy dress which seems to have pervaded through to his parties at West Wycombe Park. Following the dedication of the West portico as a bacchanalian temple in 1771, Dashwood and his friends dressed in skins adorned with vine leaves and went to party by the lake for "[[Paean]]s and [[libation]]s".<ref>Jackson-Stops p 148.</ref> On another occasion, during a [[naumachia|mock sea battle]] on the canal, the captain of the snow, "attacking" a [[Artillery battery|battery]] constructed on the bank, was struck by the [[wadding]] of a gun and suffered an internal injury.<ref>Dashwood, p 227.</ref> Dashwood seems to have mellowed in his later years and devoted his life to charitable works. He died in 1781, bequeathing West Wycombe to his half-brother [[Sir John Dashwood-King, 3rd Baronet]].<ref name="ODNB">{{cite web |last=Woodland |first=Patrick |title=Dashwood, Francis, eleventh Baron Le Despencer (1708–1781) |work=Oxford Dictionary of National Biography |edition=online |year=2004 |url=http://www.oxforddnb.com/view/article/7179 |publisher=Oxford University Press |accessdate=6 February 2016 |mode=cs2}} {{ODNBsub}}</ref>

Dashwood-King spent little time at West Wycombe.  On his death in 1793, the estate was inherited by his son [[Sir John Dashwood-King, 4th Baronet|Sir John Dashwood, 4th Baronet]], Member of Parliament for [[Wycombe (UK Parliament constituency)|Wycombe]] and a friend of the [[George IV of the United Kingdom|Prince of Wales]], although their friendship was tested when Sir John accused his wife of an affair with the prince.<ref>Knox p 57.</ref> Like his father, Sir John cared little for West Wycombe and held a five-day sale of West Wycombe's furniture in 1800. In 1806, he was prevented from selling West Wycombe by the [[trustee]]s of his son, to whom the estate was [[entailed]].<ref>Dashwood, p 80.</ref> He became religious in the last years of his life,<ref>Dashwood, p 84.</ref> holding ostentatiously [[teetotal]] parties in West Wycombe's gardens in aid of the "Friends of Order and Sobriety" – these would have been vastly different from the bacchanalian fêtes given by his uncle in the grounds. In 1847, Sir John was bankrupt and [[bailiff]]s possessed the furniture from his home at [[Halton House|Halton]]. He died estranged from his wife and surviving son in 1849.<ref>Dashwood, pp 85–86.</ref>

[[Image:Church and mausoleum in West Wycombe Park.jpg|thumb|right|300px|Overlooking the gardens and park are the tower of West Wycombe Church (left), and the [[mausoleum]] (right). In the hexagonal structure, more a walled enclosure than mausoleum, were entombed the hearts of associates of the [[Hellfire Club]]. The family are buried in a [[Burial vault (tomb)|vault]] beneath the church.<ref>Knowles</ref>]]

Sir John was succeeded by his estranged son [[Sir George Dashwood, 5th Baronet]]. For the first time since the death of the 2nd Baronet in 1781, West Wycombe became again a favoured residence.<ref>Dashwood, p 86.</ref> However, the estate was heavily in debt and Sir George was forced to sell the unentailed estates, including Halton, which was sold in 1851 to [[Lionel de Rothschild]] for the then huge sum of £54,000 (£{{formatprice|{{inflation|UK|54000|1851}}}} in {{CURRENTYEAR}}). The change in the Dashwoods' fortunes allowed for the refurbishment and restoration of West Wycombe.<ref>Dashwood, p 90.</ref> Sir George died childless in 1862, and left his wife, Elizabeth, a [[life tenancy]] of the house<ref>Dashwood, p 91.</ref> while the title and ownership passed briefly to his brother and then a nephew. Lady Dashwood's continuing occupation of the house prevented the nephew, [[Sir Edwin Dashwood, 7th Baronet|Sir Edwin Hare Dashwood, 7th Baronet]], an alcoholic sheep farmer in the [[South Island]] of New Zealand, from living in the mansion until she died in 1889, leaving a neglected and crumbling estate.<ref>Dashwood, pp 100, 102.</ref>

The 7th Baronet's son, Sir Edwin Dashwood, 8th Baronet, arrived from New Zealand to claim the house, only to find Lady Dashwood's heirs claiming the house's contents and family jewellery, which they subsequently sold. As a consequence, Sir Edwin was forced to mortgage the house and estate in 1892. He died suddenly the following year, and the heavily indebted estate passed to his brother, Sir Robert Dashwood, 9th Baronet. Sir Robert embarked on a costly legal case against the executors of Lady Dashwood, which he lost, and raised money by denuding the estate's woodlands and leasing the family town house in London for 99 years.<ref>Dashwood, p 103.</ref> On his death in 1908, the house passed to his 13-year-old son Sir John Dashwood, 10th Baronet, who in his adulthood sold much of the remaining original furnishings (including the state bed, for £58<ref>Dashwood, p 199.</ref> – this important item of the house's history complete with its gilded pineapples is now lost). In 1922, he attempted to sell the house itself. He received only one offer, of £10,000 (£{{formatnum:{{inflation|UK|10000|1922|r=-3}}}} in {{CURRENTYEAR}}), so the house was withdrawn from sale.<ref>Dashwood, p 106.</ref> Forced to live in a house he disliked,<ref name="Knox-61">Knox p 61.</ref> the village of West Wycombe was sold in its entirety to pay for renovations. Not all these renovations were beneficial: painted 18th century ceilings were overpainted white, and the dining room was divided into service rooms, allowing the large service wing to be abandoned to rot.<ref>Dashwood p 199.</ref>

A form of salvation for West Wycombe was Sir John's wife: Lady Dashwood, the former Helen Eaton, a Canadian and sister of American novelist [[Evelyn Eaton]], was a socialite who loved entertaining, and did so in some style at West Wycombe throughout the 1930s.<ref>Dashwood pp 110–111.</ref> Living a semi-estranged life from her husband, occupying opposite ends of the mansion, she frequently gave "large and stylish" house parties.<ref name="Knox-61"/>

During [[World War II]], the house saw service as a depository for the evacuated [[Wallace Collection]] and a convalescent home. A troop of gunners occupied the decaying service wing, and the park was used for the inflation of [[barrage balloon]]s. During this turmoil, the Dashwoods retreated to the upper floor and took in lodgers to pay the bills, albeit very superior lodgers, who included [[Nancy Mitford]] and [[James Lees-Milne]],<ref>Dashwood p 114.</ref> who was secretary of the Country House Committee of the National Trust and instrumental in the Trust's acquisition of many such houses.<ref>{{cite news|last=Fergusson|first=James|date=29 December 1997|title=Obituary: James Lees-Milne|url=http://www.independent.co.uk/news/obituaries/obituary-james-leesmilne-1291030.html|work=The Independent|accessdate=11 March 2016|mode=cs2}}</ref> In 1943, Sir John gave the house and grounds to the National Trust, on condition that he and his descendants could continue living in the house.<ref name=baronets>{{cite web|url=https://www.nationaltrust.org.uk/features/introducing-the-dashwood-baronets|title=Introducing the Dashwood baronets|publisher=National Trust|accessdate=11 March 2016|mode=cs2}}</ref>

==West Wycombe after 1943==
{{See also|List of films shot at West Wycombe Park}}
In the latter half of the 20th century, Sir Francis Dashwood, 11th Baronet, embarked on a program of restoration and improvement.<ref name=baronets/> His efforts included the installation of a huge [[equestrian sculpture]] as the focal point of a long tree lined [[Vista (landscaping)|vista]] from the house. On close inspection, it proves to be a [[fibre glass]] [[Theatrical property|prop]] found at [[Pinewood Studios]] by the 11th Baronet who paid for it with 12 bottles of champagne.<ref>Knox p 35.</ref> The local [[Local government in the United Kingdom|planning authority]] was furious but lost their lawsuit to have it removed. Today, from a distance, it has been "known to fool experts".<ref>Knox p 35. Knox does not name these experts</ref>

The present head of the Dashwood family is Sir Edward Dashwood (born 1964), who is married and has three children.<ref name=baronets/> The contents of the house are owned by the family, who also own and run the estate.<ref>National Trust p 5.</ref> The house can be hired as a filming location, and, in addition to agricultural and equestrian enterprises, there is a large [[Pheasant|pheasant shoot]] with paying guns. The park, a natural [[amphitheatre]],<ref>Knox p 5.</ref> is often the setting for large public concerts and firework displays, and the mansion is available for weddings and corporate entertainment.<ref>{{cite web|url=http://www.westwycombeestate.co.uk/visit-west-wycombe-park/|title=West Wycombe Estate|accessdate=11 March 2016|mode=cs2}}</ref>

While the estate remains in private hands, the National Trust owns the house and gardens, the park, the village of [[West Wycombe]], and the hill on which the Dashwood mausoleum sits. The hill was the first part of the property given to the Trust by Sir John Dashwood in 1925. The village was bought by the [[Royal Society of Arts]] from Sir John in 1929 and given to the Trust five years later.<ref name=baronets/> The grounds are open to the public in the afternoon only from April to August annually, and the house is open from June to August.<ref>{{cite web|url=http://www.nationaltrust.org.uk/west-wycombe-park-village-and-hill|title=West Wycombe Park Village and Hill|publisher=National Trust|accessdate=11 March 2016|mode=cs2}}</ref>

==Notes==
{{Reflist|33em}}

==References==
*{{cite book
 | last=Dashwood
 | first=Sir Francis
 | authorlink=Sir Francis Dashwood, 11th Baronet
 | year=1987
 | title=The Dashwoods of West Wycombe
 | publisher=Aurum Press Ltd.
 | location=London
 | isbn=0-948149-77-9
}}
*{{cite book
 | last = Girouard
 | first = Mark
 | authorlink = Mark Girouard
 | year = 1978
 | title = Life in the English country house
 | publisher = Yale University
 | location = Yale
}}
*{{cite book
 | last = Greeves
 | first = Lydia
 | year = 2008
 | title = Houses of the National Trust
 | publisher = National Trust
 | location = London
 | isbn = 978-1-905400-669
}}
*{{cite book
 | last = Jackson-Stops
 | first = Gervase
 | year = 1988
 | title = The Country House Garden (A grand tour)
 | publisher = Pavilion Book Ltd
 | location = London
 | isbn = 1-85145-123-4
}}
*{{cite book
 | last = Knox
 | first = Tim
 | year = 2001
 | title = West Wycombe Park
 | publisher = National Trust
 | location = Bromley, Kent
}}
*{{cite book
 | last = National Trust
 | year = 1989
 | origyear = 1978
 | title = West Wycombe Park
 | publisher = National Trust
}}
*{{cite book
 | last = Pevsner
 | first = Nikolaus
 | year = 1973
 | title = Buckinghamshire
 | publisher = Penguin Books Ltd
 | location = England
 | isbn = 0-14-071019-1
}}
*{{cite book
 | last = Wallace
 | first = Carew
 | year = 1967
 | title = West Wycombe Park
 | publisher = National Trust
 | location = England
}}
*[http://www.aboutbritain.com/WestWycombePark.htm All about Britain.com] Retrieved 18 August 2006
*[http://www.dicamillocompanion.com/houses_detail.asp?ID=2093 The di Camillo Companion, database of houses.] Retrieved 18 August 2006
*[http://www.controverscial.com/Sir%20Francis%20Dashwood.htm Knowles George. ''Sir Francis Dashwood'' Controverscial.Com] Retrieved 20 August 2006

==External links==
{{oscoor gbx|SU 8293 9430}}
*[https://www.nationaltrust.org.uk/west-wycombe-park-village-and-hill West Wycombe Park information from the National Trust]
*{{National Heritage List for England|num=1000447}}

{{featured article}}
{{coord|51.6414|-0.8029|type:landmark_region:GB|display=title}}

[[Category:Houses completed in 1800]]
[[Category:Country houses in Buckinghamshire]]
[[Category:Gardens in Buckinghamshire]]
[[Category:Historic house museums in Buckinghamshire]]
[[Category:Grade I listed buildings in Buckinghamshire]]
[[Category:Grade II* listed buildings in Buckinghamshire]]
[[Category:Grade I listed houses]]
[[Category:Grade I listed parks and gardens in Buckinghamshire]]
[[Category:National Trust properties in Buckinghamshire]]
[[Category:Neoclassical architecture in England]]