{{Use mdy dates|date=April 2016}}
{{Infobox military person
| honorific_prefix          = General
| name                      = Thomas Adams Smith
| image                     = General_Thomas_Adams_Smith.jpg
| image_size                = 300px
| caption                   = Gen. Thomas A. Smith
| birth_date                = {{birth date|1781|08|12}}
| death_date                = {{Death-date and age| June 25, 1844 | August 12, 1781 }}
| birth_place               = Piscataway, [[Essex County, Virginia]]
| death_place               = Experiment Farm, [[Saline County, Missouri]]
| placeofburial             = Memorial Presbyterian Church Cemetery, Napton, Saline County, Missouri
| placeofburial_label       = Resting Place
| placeofburial_coordinates = {{Coord|39.04747|-93.10258|display=inline,title}}
|allegiance                 = {{USA}}
|branch                     = {{army|United States}}
| serviceyears              = 1803 — 1818
| rank                      = [[Brigadier general (United States)|Brigadier General]]
| unit                      = 
| commands                  = {{plainlist|
* [[Regiment of Riflemen (United States)|Regiment of Riflemen]]
* [[Military District#United States historical military districts|Ninth Military District]]
}}
| spouse                    = Cynthia Berry White Smith
| relations                 = {{plainlist|
* Dr. Crawford E. Smith, son
* Cynthia White Smith Berkeley, daughter
* Six other children
}}
}}
'''Thomas Adams Smith''' was an [[United States|American]] military officer and, later, a government official, in the first half of the 19th century. He commanded troops in the "Patriot War" in Spanish [[East Florida]]. He commanded the [[Regiment of Riflemen (United States)|Regiment of Riflemen]] and then the Ninth Military Department. He was a slave owner. The city of [[Fort Smith, Arkansas]], is named for Smith, although he never went to its location. 

==Early life==
'''Thomas Adams Smith''' was born on August 12, 1781 in Piscataway, [[Essex County, Virginia]]. He was the fifth of seven children of Francis and Lucy Wilkinson Smith.<ref name=FG>{{Find a Grave|grid=30779207|name=Thomas Adams Smith|date=October 22, 2008|accessdate=September 13, 2014|author=Parker, Richard}}</ref> At some point prior to entering the [[U.S. Army]], Smith moved to [[Georgia (U.S. state)|Georgia]].<ref name=Heitman>{{cite book|last1=Heitman|first1=Francis B.|title=Historical register and dictionary of the United States Army : from its organization, September 29, 1789, to March 2, 1903|date=1903|page=903|edition=1|volume= 1|url=https://archive.org/stream/historicalregist01heitrich#page/902/mode/2up|accessdate=September 12, 2014|quote=This is the unofficial work of a private compiler, purchased and published by direction of Congress}}</ref>

==Military career==
Smith was commissioned as a second lieutenant of artillery on December 15, 1803, and promoted to first lieutenant on December 31, 1805.<ref name=Heitman/> In October 1806, General [[James Wilkinson]] used Smith, then serving as Wilkinson's aide, as a courier to transport letters relating to the [[Burr conspiracy]] to President [[Thomas Jefferson]].<ref name=Napton>{{cite book|last1=Napton|first1=William Barclay|title=Past and Present of Saline County Missouri|date=1910|publisher=B. F. Bowen & Company|location=Chicago, Illinois|url=https://archive.org/stream/pastpresentofsal00napt#page/n7/mode/2up|accessdate=September 13, 2014}}</ref>{{rp|319}}

Smith enjoyed the support of Senator [[William H. Crawford]] (whose present-day namesake [[Crawford County, Arkansas]], lies across the [[Arkansas River]] from its neighbor Fort Smith) and Congressman [[George Troup|George M. Troup]], both of [[Georgia (U.S. state)|Georgia]]. It is unclear whether patronage was involved but Smith, now an experienced officer, was promoted to captain in the [[Regiment of Riflemen (United States)|Regiment of Riflemen]] on May 3, 1808. When Lieutenant Colonel [[William J. Duane|William Duane]] proved unequal to the task of being second in command of the Regiment of Riflemen, Smith was promoted to lieutenant colonel on July 31, 1810, and replaced Duane; he was promoted over John Fuller, the major in the regiment, who left the Army.<ref name=Fredriksen>{{cite book|last1=Fredriksen|first1=John C.|title=Green Coats and Glory: The United States Regiment of Riflemen, 1808-1821|date=November 2000|publisher=Old Fort Niagara Association, Inc.|location=Youngstown, New York|edition=1st|accessdate=September 5, 2014}}</ref>{{rp|13–23}}

A group of [[Georgia (U.S. state)|Georgia]]ns, calling themselves "Patriots," crossed into Spanish [[East Florida]] and, on March 17, 1812, captured [[Amelia Island]] from the Spanish garrison. The Patriots then "ceded" Amelia Island and the surrounding area to the United States. On April 12, 1812, Smith led two companies of riflemen who occupied [[Fort Mose|Fort Mose, Spanish East Florida]] as part of the [[Seminole Wars#Patriot War|Patriot War of East Florida]]. The riflemen received little support from the US Government or the Patriots. Smith attempted a siege of [[St. Augustine, Florida]], but his supply lines were not secure and the Spanish garrison of [[Castillo de San Marcos]] threatened his command. The Spanish counterattacked Fort Mose and Smith retreated to an encampment further from St. Augustine, Florida. On May 16, 1812, the Spanish set fire to Fort Mose to prevent its reoccupation. All US troops were withdrawn from East Florida by May 1813.<ref name=Brenner>{{cite web|last1=Brenner|first1=James T.|title=The Green Against the British Red: U.S. Rifle Regiments in the Northwestern Army|url=http://warof1812.ohio.gov/_assets/docs/Rifles.pdf|accessdate=September 14, 2014}}</ref>{{rp|2}}<ref name=Davis>{{cite web|last1=Davis|first1=T. Frederick|title=United States Troops in Spanish East Florida, 1812-1813|url=http://fortmose.com/index.html|accessdate=August 30, 2014}}</ref> Troops retreated to [[St. Marys, Georgia|Point Petre, Georgia]], under the leadership of Captain Abraham A. Massias.<ref name=Fredriksen/>{{rp|27–28}}

On July 6, 1812, Colonel [[Alexander Smyth]] left the regiment to become Inspector General of the Army and Smith was promoted to colonel and assumed command of the regiment.<ref name=Fredriksen/>{{rp|13–23}}<ref name=Davis/>

During the [[War of 1812]], elements of the Regiment of Riflemen were allocated to different commands and rarely fought together. On January 24, 1814, Smith was promoted to brigadier general. He relinquished command to the riflemen to George W. Sevier and assumed command of a light infantry brigade near Plattsburg, New York. In September 1814, Smith's brigade, including elements of the Regiment of Riflemen, proceeded to join forces operating near Niagara, New York. They failed to arrive before the campaign season ended in December. Smith was allowed to take leave in Knoxville, Tennessee. While he was on leave, the war ended.<ref name=Napton/>{{rp|320}}<ref name=Fredriksen/>{{rp|47}}

Following the end of the War of 1812, the Army was reduced in size. Smith was retained but reverted, on May 17, 1815, to his earlier rank of colonel; however, he was concurrently brevetted as a brigadier general postdated to January 24, 1814, the date of his wartime promotion.<ref name=Heitman/>

In July 1815, Smith was ordered to report to St. Louis, Missouri, and arrived on September 1, 1815. He resumed command of the Regiment of Riflemen.<ref name=Napton/>{{rp|320}}<ref name=Fredriksen/>{{rp|69}}

During Smith's tenure, the Regiment of Riflemen founded [[Fort Armstrong, Illinois|Fort Armstrong]], Rock Island, Illinois; [[Fort Crawford]], Prairie du Chien, Wisconsin; [[Fort Howard, Wisconsin|Fort Howard]], Green Bay, Wisconsin; and Fort Smith, Arkansas. The last was named after Thomas Smith and a community grew up around the fort. After a few years, the Army abandoned the installation, but the city of [[Fort Smith, Arkansas|Fort Smith]] remained, and continued to grow. Throughout the twentieth century, it was the second-largest city in Arkansas.<ref name=Fredriksen/>{{rp|69}}

Smith resigned from the Army on November 10, 1818.<ref name=Heitman/>

==Later life==
During the time Smith was assigned to [[Fort Bellefontaine|Fort Bellefontaine, Missouri]], legislation opened up new areas of Missouri for settlement and for the opening of a land officer in [[Franklin, Missouri]]. Smith wanted to be appointed as the receiver of the office because he wanted to establish a permanent residence for his wife and children as well as their inherited slaves. [[John O'Fallon]], formerly a captain in the Regiment of Riflemen and now a prominent businessman in St. Louis successfully pleaded his case and Smith was appointed to the position. Later, he was able to acquire six or seven thousand acres of land, on which he established a farm he named "Experiment." In 1829, Smith resigned his position as receiver and moved with his family to the house he had built on the farm, becoming a full-time farmer. He never sought another public office.<ref name=Napton/>{{rp|321–324}}

Smith died on June 25, 1844 at Experiment Farm.<ref name=FG/><ref name=Napton/>{{rp|324}} (Heitman shows his date of death as December 1818.<ref name=Heitman/>)

==References==
{{reflist}}

{{DEFAULTSORT:Smith Thomas Adams}}
[[Category:1781 births]]
[[Category:1844 deaths]]
[[Category:American people of the War of 1812]]
[[Category:United States Army generals]]