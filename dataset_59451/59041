{{About|the scientific journal entitled "Natural Computing"|the concept from which the journal draws its name|Natural computing}}
{{Italic title}}
'''''Natural Computing''''' is a [[scientific journal]] covering [[natural computing]] research. It has been published quarterly by [[Springer Science+Business Media|Springer Verlag]] (Springer Netherlands) in print ({{ISSN|1567-7818}}) and online ({{ISSN|1572-9796}}) since 2002.<ref name=NC>{{cite journal | url=http://link.springer.com/journal/11047 | journal= 'Natural Computing', ''SpringerLink'' | date=2002 | title=An International Journal }}</ref>

"Natural Computing refers to computational processes observed in nature, and human-designed computing inspired by nature ... molecular computing and quantum computing ... use of algorithms to consider evolution as a computational process, and neural networks in light of computational trends in brain research."<ref name=NC/> 

It includes 19 open access articles as of 19-Jun-2016<ref>{{cite journal | url=http://link.springer.com/search?query=&search-within=Journal&facet-journal-id=11047&package=openaccessarticles | journal= 'Natural Computing', ''SpringerLink'' | date=2016 | title=Open Access at Natural Computing }}</ref> and has an [[Impact Factor]] of 
1.310.<ref name=NC/> 

==References==
<references/>

[[Category:Computer science journals]]
[[Category:English-language journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2002]]


{{compu-journal-stub}}