{{Infobox aviator
|name        = Phoebe Omlie
|image       = Phoebe Omlie.jpg
|image_size  = 
|caption     = 
|full_name   = Phoebe Jane Fairgrave Omlie 
|birth_date  = {{birth date|1902|11|21}}
|birth_place = [[Des Moines, Iowa]]
|death_date  = {{death date and age|1975|7|17|1902|11|21}}
|death_place = [[Indianapolis, Indiana]]
|death_cause = [[lung cancer]]
|nationality = [[United States|American]]
|spouse      = Vernon Omlie
|relatives   = Andrew Fairgrave (father)<br/>Madge Traister (mother)
|known_for   = Numerous firsts as a female pilot
}}

'''Phoebe Jane Fairgrave Omlie''' (November 21, 1902 &ndash; July 17, 1975) was an American aviation pioneer, particularly noted for her accomplishments as an early female aviator.<ref name="Soared">{{cite web |url=http://www.desmoinesregister.com/article/99999999/FAMOUSIOWANS/807270357/-1/famousiowans |title=Aviator Omlie soared to success |accessdate=2009-02-14 |date=2009-02-07 |last=Longden |first=Tom |publisher=Des Moines Register |archive-url=https://www.newspapers.com/clip/4960483/aviator_omlie_soared_to_success_phoebe/ |archive-date=2016-04-16}}</ref> Omlie was the first woman to receive an [[Aircraft Maintenance Technician|airplane mechanic's license]], the first [[pilot certification in the United States|licensed]] female [[Airline Transport Pilot License|transport pilot]], and the first woman to be appointed to a federal position in the aviation field.<ref name="NanceLove">{{cite book |last=Rickman |first=Sarah Byrn |title=Nancy Love and the WASP Ferry Pilots of World War II |date=March 2008 |publisher=University of North Texas Press |location=Denton, Texas |isbn=1-57441-241-8 |oclc=173502734 |pages=34–36 |chapter=Stretching Her Wings}}</ref>

During the late 1920s and early 1930s, Omlie set several world records in aviation, including the highest altitude parachute jump by a woman. She was also the first woman to cross the [[Rocky Mountains]] in a [[light aircraft]],<ref name="Soared"/> and was considered by [[First Lady of the United States|First Lady]] [[Eleanor Roosevelt]] to be one of "eleven women whose achievements make it safe to say the world is progressing".<ref name="PuffDerby">{{cite book |last=Jessen
|first=Gene Nora |title=The Powder Puff Derby of 1929: The First All Women's Transcontinental Air Race |date=2002-03-01 |publisher=Sourcebooks, Inc |location=Naperville, Illinois |isbn=1-57071-769-9 |oclc=46918327 |pages=238–239 |chapter=Epilogue}}</ref>

==Early life==
Phoebe Jane Fairgrave was born in [[Des Moines, Iowa]] on November 21, 1902,<ref name="WomanPilot">{{cite web |url=http://womanpilot.com/?p=13 |title=Aviation pioneer Phoebe Fairgrave Omlie |accessdate=2009-02-14 |date=2008-03-29 |last=Sherman |first=Janann |publisher=Woman Pilot Magazine}}</ref> and was the only daughter of parents Harry J. Park and Madge Traister Park. After divorcing Harry Park, Madge married Andrew Fairgrave, who adopted her two children, Phoebe and Paul.<ref name="Soared"/> Phoebe and her brother, Paul, attended Oak Park School in Des Moines until she was 12, when she and her family moved to [[St. Paul, Minnesota]].<ref name="Soared"/> There, Fairgrave attended Madison School and Mechanic Arts High School and graduated in 1920. Fairgrave's interest in aviation was sparked the day before she graduated, when President [[Woodrow Wilson]] visited Minneapolis.<ref name="HistoryNet">{{cite web |url=http://www.historynet.com/phoebe-and-vernon-omlie-from-barnstormers-to-aviation-innovators.htm |title=Phoebe and Vernon Omlie: From Barnstormers to Aviation Innovators |accessdate=2009-02-14 |last=Wilson |first=George T |date=June 2002 |publisher=Aviation History Magazine}}</ref> President Wilson's visit was commemorated by a [[Flypast|flyover]] and was the first [[airshow]] of any kind that Fairgrave had witnessed.<ref name="WomanPilot"/>

==Aviation career==
Shortly after graduating high school, Fairgrave spent a few months at the [[Guy Durrell Dramatic School]] and worked briefly as a secretary.<ref name="Soared"/> Bored with the prospects, she began hanging around airfields near her home and attempted to convince the airport manager to allow one of his flight instructors to take her flying.<ref name="WomanPilot"/> The manager finally agreed, thinking that he could scare Fairgrave's interest in aviation out of her by performing various [[aerobatic maneuver]]s in an attempt to make her sick.<ref name="HistoryNet"/> Instead, Fairgrave demanded more flight time and used some of her inheritance to purchase a [[Curtiss JN-4]] [[biplane]] after her fourth flight.<ref name="HistoryNet"/>

Still in her teens, Fairgrave started performing stunts on the wing of her aircraft as another pilot remained at the controls.<ref name="WomanPilot"/> Fairgrave began [[wing walking]], learned to hang below the plane by her teeth, [[parachute]], and "dance the [[Charleston (dance)|Charleston]] on the top wing".<ref name="WomanPilot"/> Using the stunts she had learned, Fairgrave claimed the record for the highest parachute jump for a woman by jumping from her plane at {{convert|15,200|ft|abbr=on}} ([[Sea level|MSL]]) and earned a movie deal, flying aerobatic stunts for the [[film serial]] ''[[The Perils of Pauline (1914 serial)|The Perils of Pauline]]''.<ref name="Soared"/> This was her first flight with Vernon C. Omlie, who would become her husband.<ref name="WomanPilot"/> Following the record setting jump, Fairgrave and Omlie flew around the country on a [[barnstorming]] tour and eventually married in 1922.<ref name="HistoryNet"/>

In 1925, the Omlies moved to [[Memphis, Tennessee]] and began offering flying lessons and mechanical services to local residents.<ref name="HistoryNet"/> A year later, in 1927, Phoebe became the first woman to receive an [[Aircraft Maintenance Technician|airplane mechanic's license]], as well as the first [[pilot certification in the United States|licensed]] female [[Airline Transport Pilot License|transport pilot]].<ref name="Soared"/> While Vernon continued operating the business and working as a flight instructor, Phoebe began working for the [[Mono Aircraft Company]]. Flying the company's [[Monocoupe 90]] light aircraft out of [[Quad City International Airport]] in 1928, Omlie set a world altitude record for women when she reached {{convert|25,400|ft|abbr=on}} (MSL).<ref name="Soared"/><ref name="WomanPilot"/><ref name="HistoryNet"/> That same year, Omlie competed in the [[Ford National Reliability Air Tour|Edsel Ford Air Tour]] and became the first woman to cross the Rocky Mountains in a light aircraft.<ref name="WomanPilot"/> Omlie later joined the [[Ninety-Nines]] as a charter member after competing in a race with [[Amelia Earhart]].<ref name="WomanPilot"/><ref name="Engines">{{cite episode |title=Phoebe Omlie and Her Monocoupe |url=http://www.uh.edu/engines/epi2438.htm |series=The Engines of Our Ingenuity |credits=John H. Lienhard |station=KUHF-FM |location=Houston}}</ref>

Omlie's success as a pilot was recognized by the [[Democratic National Committee]], and she was enlisted to fly a female speaker around the country for [[Governor of New York|Governor]] [[Franklin D. Roosevelt]]'s [[United States presidential election, 1932|1932 presidential campaign]].<ref name="WomanPilot"/> After the successful campaign, Omlie was appointed by President Roosevelt as the "Special Adviser for Air Intelligence to the [[National Advisory Committee for Aeronautics]]".<ref name="WomanPilot"/> This made her the first woman to be appointed to a [[Federal government of the United States|federal]] aviation position.<ref name="NanceLove"/> In this role, Omlie acted as a "liaison between the National Advisory Committee of Aeronautics and the Bureau of Air Commerce" alongside Amelia Earhart to create what would become the [[National Airspace System]].<ref name="HistoryNet"/>

On August 5, 1936, Vernon Omlie and seven passengers were killed when a commercial flight they were aboard crashed in [[St. Louis, Missouri]] while attempting to land in foggy conditions.<ref name="WomanPilot"/>  Phoebe Omlie immediately resigned her position in [[Washington, D.C.]] and returned to Memphis.<ref name="WomanPilot"/> Following her husband's death, Omlie did not return to Washington, D.C. until 1941, when she accepted a job as "Senior Private Flying Specialist of the [[Civil Aeronautics Authority]]".<ref name="WomanPilot"/> In this position, and to meet the severe need for pilots for service in WWII, Omlie established 66 [[flight school]]s in 46 states, including a school in [[Tuskegee, Alabama]] that would later train the infamous [[Tuskegee Airmen]].<ref name="WomanPilot"/> With the Tennessee Bureau of Aeronautics, she established an "experimental" program to train women as instructors. The first class, ten women from various states, trained between September and February 1943, and was meant to establish her strong and, to some, controversial belief that " . . . if women can teach men to walk, they can teach them to fly." These women went on to instruct both men and women pilots both in military and civilian flight training programs, including the Navy V-5 and the USAAF [[Women Airforce Service Pilots]].<ref name="Biography of Dorothy Swain Lewis">{{cite book|last=Cooper|first=Ann L|title=How High She Flies | year=1999|isbn=1-928760-00-7}}</ref>  

Unhappy about the increasing regulation of the aviation industry by the United States Federal Government under President [[Harry S. Truman]], Omlie resigned in 1952 and left aviation.<ref name="HistoryNet"/>

==Later life==
After resigning from the Civil Aeronautics Authority, Omlie returned to Memphis and purchased a cattle farm in [[Como, Mississippi]].<ref name="WomanPilot"/> Omlie's inexperience with operating a cattle farm posed a problem in running the business, and she traded the farm a few years later for a small cafe and hotel in [[Lambert, Mississippi]].<ref name="HistoryNet"/> The hotel business proved to be just as unsuccessful for Omlie, who returned to Memphis in 1961.<ref name="HistoryNet"/> 

Leading up to the final years before her death, Omlie made little money as a public speaker.<ref name="HistoryNet"/> The last few years of Omlie's life were spent in seclusion, living in a [[flophouse]] in [[Indianapolis, Indiana]], fighting lung cancer and alcoholism.<ref name="Soared"/><ref name="HistoryNet"/> Omlie died on July 17, 1975, and was buried next to her husband in Forest Hill Cemetery.<ref name="Soared"/>

In June 1982, a new [[air traffic control tower]] was dedicated and named in honor of Phoebe and Vernon Omlie at the [[Memphis International Airport]].<ref name="AOPA tower">{{cite web |url=http://blog.aopa.org/blog/?p=2569 |title=A legacy is secured and it only took 30 years |last=Tallman |first=Jill |date=2011-10-27 |website=AOPA Reporting Points |publisher=AOPA |access-date=2016-11-03}}</ref><ref name="HR3072">{{cite web |url=https://www.govtrack.us/congress/bills/97/hr3072 |title=H.R. 3072 (97th) |author=97th Congress (1981) |date=1981-04-07 |access-date=2016-11-03 |work=Legislation |publisher=GovTrack.us |quote=A bill to designate the control tower at Memphis International Airport the Omlie Tower}}</ref>

==References==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Omlie, Phoebe}}
[[Category:1902 births]]
[[Category:1975 deaths]]
[[Category:American aviators]]
[[Category:Aviation pioneers]]
[[Category:Aviators from Iowa]]
[[Category:Deaths from cancer in Indiana]]
[[Category:Female aviators]]
[[Category:People from Des Moines, Iowa]]
[[Category:American female aviators]]