{{Infobox journal
| title = Journal of Fluid Mechanics
| cover =
| editor = [[Grae Worster]]
| discipline = [[Fluid mechanics]]
| abbreviation = J. Fluid Mech.
| publisher = [[Cambridge University Press]]
| country =
| frequency = Biweekly
| history = 1956–present
| openaccess =
| license =
| impact = 2.51
| impact-year = 2015
| website = http://journals.cambridge.org/action/displayJournal?jid=FLM
| link1 = http://journals.cambridge.org/action/displayIssue?jid=FLM&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=FLM
| link2-name = Online archive
| JSTOR =
| OCLC = 01782778
| LCCN = 57003918
| CODEN = JFLSA7
| ISSN = 0022-1120
| eISSN =
}}
The '''''Journal of Fluid Mechanics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of [[fluid mechanics]]. It publishes original work on theoretical, computational, and experimental aspects of the subject.

The journal is published by [[Cambridge University Press]] and retains a strong association with the [[University of Cambridge]], in particular the [[Department of Applied Mathematics and Theoretical Physics]] (DAMTP). Volumes are published twice a month in a single-column [[ISO 216|B5]] format.

The journal was established in 1956 by [[George Batchelor]], who remained the [[editor-in-chief]] for some forty years. He started out as the sole editor, but later a team of associate editors provided assistance in arranging the review of articles. As of January 2012, there are 2 deputy editors and 20 associate editors.

== Editors ==
The following people have been editor (later, editor in chief) of the ''Journal of Fluid Mechanics'':
* 1956&ndash;1996: [[George Batchelor]] (DAMTP)
* 1966&ndash;1983: [[Keith Moffatt]] (DAMTP)
* 1996&ndash;2000: [[David Crighton]] (DAMTP)
* 2000&ndash;2006: [[Tim Pedley]] (DAMTP)
* 2000&ndash;2010: [[Stephen H. Davis]] ([[Northwestern University]])
* 2007&ndash;present: [[Grae Worster]] (DAMTP)

== Further reading ==
* {{cite journal |doi=10.1017/S0022112081001493 |title=Preoccupations of a journal editor |year=2006 |last1=Batchelor |first1=G. K. |journal=Journal of Fluid Mechanics |volume=106 |pages=1|bibcode = 1981JFM...106....1B }}
* {{cite journal |doi=10.1017/S0022112000009514 |title=Editorial |year=2000 |last1=Davis |first1=Steve |last2=Pedley |first2=Tim |journal=Journal of Fluid Mechanics |volume=415 |pages=0}}
* {{cite journal|doi=10.1017/S0022112006009566|title=Editorial: JFM at 50|year=2006|last1=Davis|first1=S. H.|last2=Pedley|first2=T. J.|journal=Journal of Fluid Mechanics|volume=554|pages=1|bibcode = 2006JFM...554....1D }}
* Huppert, H. E. (2006). [http://www.itg.cam.ac.uk/people/heh/jfm50.pdf 50 Years of Impact of JFM].

== External links ==
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=FLM}}

[[Category:Fluid dynamics journals]]
[[Category:Publications established in 1956]]
[[Category:Cambridge University Press academic journals]]
[[Category:Biweekly journals]]
[[Category:English-language journals]]
[[Category:Cambridge University academic journals]]