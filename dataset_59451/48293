{{Infobox German location
|name = Großweier
|type = Borough
|Town = Achern
|image_coa = Wappen Grossweier.png
|image_photo = Pfarrkirche St Martin Grossweier.jpg
|image_caption = Parish Church of St. Martin
|image_plan =
|state = [[Baden-Württemberg]]
|regbzk = [[Freiburg (region)|Freiburg]]
|district = [[Ortenaukreis]]
|population = 1,512<ref>Andreas Cibis. Baden Online. [http://www.bo.de/lokales/achern-oberkirch/zu-wenig-kernstaedter-im-rat "Insufficient Core City Dwellers in the Council"], February 18, 2014. Retrieved on 29 October 2015.</ref>
|population_as_of = {{date|2013-12-31}}
|population_ref =
|Gemeindeschlüssel =
|pop_dens =
|area =
|elevation = 150
|coordinates = {{coord|48|39|20|N|8|2|50|E|format=dms|display=inline,title}}
|postal_code = 77855
|area_code = 07841
|licence =
|mayor = Helmut Huber
|website = [http://www.grossweier.de/willkommen Grossweier Municipality]
}}
'''Großweier''' is an urban subdivision ([[Stadtteil]]) of the major county town (Große Kreisstadt) of [[Achern]] in the [[Germany|German]] state of [[Baden-Württemberg]]. The population of Großweier is approximately 1,500 inhabitants.  Großweier was an autonomous rural municipality since the [[High Middle Ages]].  In 1973, Großweier was incorporated as an urban subdivision of Achern in the [[Ortenaukreis]] district.

== History ==

===Origins===
Großweier was settled during the [[Middle Ages]].  The century of its original settlement is unknown.  However, Großweier was dense, uninhabited forests at least until the 8th century CE.<ref name="First Beginnings">District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/Erste-Anf%C3%A4nge-der-Urbarmachung-und-Besiedelung "First beginnings of Cultivation and Colonization"]. Retrieved on 1 November 2015.</ref>  The surrounding [[Ortenau]] region, which Großweier belonged, was controlled by the [[Celts]].  In 12 BCE, during the reign of [[Emperor Augustus]], the region was conquered by the [[Roman Empire]] as part of [[Germania Superior]].  The closest known Roman settlements to Großweier were Aquae ([[Baden-Baden]]) and [[Argentorate]] ([[Strasbourg]]).<ref name="First Beginnings" />  The region was subsequently conquered by the [[Alamannia]] after the rupture of the ''[[Limes Germanicus]]'' in 260 CE.<ref name="First Beginnings" />  The [[Franks]] expanded into Ortenau towards the end of the 7th century with the arrival of the first Christian monks.<ref name="First Beginnings" />  By 720, five Frankish monasteries are recorded in the Ortenau, including: Schwarzach, Schuttern, Honau and Gengenbach.<ref name="First Beginnings" />  Deforestation projects began in the early 8th century, as laymen and farmers arrived at the monasteries.<ref name="First Beginnings" /> Both spiritual and secular rulers took possession of newly deforested land and thus increased their spheres of influence.<ref name="First Beginnings" />  Gradually, farms and hamlets were settled and the population increased.  The settlement of Großweier was a result of this era of expanding colonization into the [[Black Forest]].

Großweier is first referenced in the undated [[Benedictine]] monastery records of Reichenbach, sometime between 1091 and 1115.<ref>District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/850-Jahre-Gro%C3%9Fweier "850 years Großweier"]. Retrieved on 29 October 2015.</ref>  The region that is referenced is referred to as United Weierer.  Großweier was first mentioned in the records as Crosvilare.<ref name="The First Recorded Origins">District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/Die-ersten-%C3%BCberlieferten-Anf%C3%A4nge "The First Recorded Origins"] Retrieved on 29 October 2015.</ref>  It was later referred to as Croschweier.  In the early  12th century, Crosvilare was the capital of eight surrounding small settlements, identified in the Reichenbach monastery records as farms or hamlets, including: Oberweier, Hesselbach, Rodt, Fronrode, High Hurst and Egdessenloch.<ref name="The First Recorded Origins" />  In 1263, records mention a [[moated castle]] as having been previously built in Großweier. However, the year of this construction is unknown.<ref name="A Random Designation">District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/Eine-Zufallsnennung "A Random Designation"]. Retrieved on 29 October 2015.</ref> The Großweier castle is likely to have been the center of local government to the surrounding farms and hamlets.<ref name="The First Recorded Origins" />

Großweier and the surrounding lands were ruled by the Knights of United Weier.  In the 13th century, the knights provided the city of Strasbourg with military aid against Walther von Geroldseck.<ref name= "The United Weierer knights and their Wasserburg">District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/-Das-Gro%C3%9Fweierer-Rittergeschlecht-und-ihre-Wasserburg "The United Weierer Knights and Their Wasserburg"]. Retrieved on 29 October 2015.</ref> In recognition of this service, the Count of Eberstein granted landed gentry to Junker Johann in 1263, during a hearing in Strasbourg.<ref name="The United Weierer knights and their Wasserburg" />  The knight Junker Johann took the surname of von Crosvilare, after the name of the town.  Eventually, the von Crosvilares were without heir and became extinct in 1484.  Kraft von Crosvilare died heirless and was survived by his wife Christine von Seldeneck. Consequently, the fiefdom became the property of the Lords of Seldeneck, a wealthy merchant family.  By May 12, 1484, Philipp von Seldeneck became owner and Lord of Großweier for the princely sum of 2,500 guilders.

===16th to 19th centuries===
In 1583, Großweier was sold to the Margrave of Baden.  The Margrave of Baden used the moated castle in Großweier as the seat of the local bailiff, thus operating as a district court.  The castle was destroyed in 1689 by French troops of the "Sun King" [[Louis XIV]], during the early stages of the [[Nine Years' War]] (War of the League of Augsburg).  The bailiff seat was subsequently moved to [[Bühl (Baden)|Bühl]].

Like many of the surrounding towns, Großweier was depopulated and devastated during the [[Thirty Years' War]].  Between 1575 and 1630, eight witch trials were conducted in Großweier.<ref name="Burkart">Martin Burkart (2009) ''Witches and Witch Trials in Baden'', Durmersheim, pp 308-389.</ref>  Consequently, two men were found guilty and executed during this period.<ref name="Burkart" />  In 1797, French troops occupied Großweier for approximately one year, until the end of the [[War of the First Coalition]].  Großweier was economically hit by the Napoleonic Wars and struggled to recover after they ended.  The issue was further exacerbated by three sequential crop failures in Großweier from 1845 through 1847.<ref name="Crop Failure">District of Großweier. [http://www.grossweier.de/de/Gro%C3%9Fweier/Geschichte/Missernten-und-nachfolgende-Notzeiten "Crop Failures and Subsequent Times of Need."] Retrieved on 1 November 2015.</ref>  Many of the poor faced starvation.  A special meeting was called and it was agreed to donate 800 gilders to feeding the poor.  The population subsequently began to deplete.  In particular, the period from 1843 to 1852 saw many inhabitants migrating to the [[United States]].  The majority of those immigrating to the US were done so forcibly.  In 1852, Großweier voted to eliminate the poor and banish them, raising the minimum financing necessary to ship them from [[Le Havre]] to [[New Orleans]].  A number of these immigrants died on the voyage or shortly after arrival to the United States, from [[yellow fever]] or other afflictions.  After eliminating the poor, Großweier lost 25 percent of its population within a ten-year period, from 800 to 600 residents.

===20th and 21st centuries===
In 1902, the parish church of St. Martin was completed. During [[World War I]], Großweier mourned the loss of 29 killed and missing.  In [[World War II]], 83 people were killed and missing.  In 1958, highway construction reached Großweier, connecting the town to the modern transportation system.  In 1961, Großweier underwent land consolidation of agricultural and forestry land.  On January 1, 1973, Großweier was incorporated as an urban subdivision of Achern. In 2005, Großweier celebrated its 850th anniversary.

== Geography ==
Großweier is located in the northern Black Forest. Großweier is located approximately 30 kilometers south of the Rhine valley of [[Baden-Baden]] and about 30 kilometers north of [[Offenburg]]. The western [[Strasbourg]] in [[Alsace]] on the French side of the [[Rhine river|Rhine]] is also approximately 30 kilometers away.

==Demographics==
As of the 2013 census, there were 1,512 residents in Großweier.  There were 687 residents in 1933 and 692 residents in 1939.<ref>Deutsche Geshichte.  [http://www.verwaltungsgeschichte.de/buehl.html Landkreis Buhl]. Retrieved on 2 November 2015.</ref>

==Culture==

===Architecture===
[[File:St. Martin Grossweier Schwarz Orgel.jpg|thumbnail|right|Interior of the Grossweier Parish Church of St. Martin]]
The Church of St. Martin is the parish church of Großweier.  The church was completed in 1904 and is one of the central pieces of architecture in Großweier.

==Economy==
Großweier has a [[service economy]] that also includes [[heavy industry]] and [[Transportation industry|commercial transportation]].  Großweier is home to the international headquarters of Hodapp GmbH & Co. KG, a [[manufacturer]] specializing in the production of [[Door|steel doors]] and gates.<ref name="Hodapp">Hodapp. [http://www.hodapp.de/ "Portrait: Three Generations on the Road to Success"].  Retrieved on 2 November 2015.</ref>  The company was founded in 1948 by Eugen Hodapp.<ref name="Hodapp History">Hodapp. [http://www.hodapp.de/ "Company History."]  Retrieved on 2 November 2015.</ref>  Presently, the company has branches in [[Spain]], [[France]], and [[Austria]].<ref name="Hodapp History" />  Hodapp designs doors with a variety of safety features, including: [[fire protection]], [[burglary]], and [[radiation]].<ref name="Hodapp" />  The corporate headquarters in Großweier is a 20,000 meter facility that employs 180 people.<ref name="Hodapp" />  

Großweier is also the headquarters of the international commercial transportation company Firma Deichelbohrer.  The company has a fleet of [[tractor-trailers]] that provide transportation services across [[Europe]], as well as parts of the [[Middle East]].<ref>Firma Deichelbohrer. [http://www.deichelbohrer.de/projekt01/index.php?lang=1&idcatside=2&view=preview "History"]. Retrieved on 2 November 2015.</ref>

== See also ==
{{Portal|Germany}}
* [[Achern]]

== References ==
{{Reflist}}

==Bibliography==
*Bönsch, Karl. ''Grossweier Through The Ages''. Ortsverwaltung Großweier, 1985.
*Federle, Rolf.  ''Ortsfamilienbuch Großweier''. 2nd revised ed., Dinglingen, 2005.
*Federle, Rolf.  ''Großweier Book of Field Names''. Ortsverwaltung Großweier: Ewald Hall, 2014.

{{DEFAULTSORT:Grossweier}}
{{Achern}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Ortenaukreis]]