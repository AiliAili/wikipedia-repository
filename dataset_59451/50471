The '''American Musicological Society''' is a membership-based [[musicology|musicological]] organization founded in 1934 to advance scholarly research in the various fields of music as a branch of learning and scholarship; it grew out of a small contingent of the [[Music Teachers National Association]] and, more directly, the New York Musicological Society (1930–1934). Its founders were George S. Dickinson, [[Carl Engel]], [[Gustave Reese]], [[Helen Heffron Roberts]], [[Joseph Schillinger]], [[Charles Seeger]], Harold Spivacke, [[Oliver Strunk]], and [[Joseph Yasser]]; its first president was [[Otto Kinkeldey]], the first American to receive an appointment as professor of musicology ([[Cornell University]], 1930).

{{Infobox Organization
|name         = American Musicological Society
|image        = American Musicological Society - IMG 9857.JPG
|image_border = 
|size         = 
|caption      = American Musicological Society, Bowdoin College Chapel, [[Bowdoin College]],  Brunswich, Maine, USA
|motto        = 
|formation    = 1942<ref name= AMS990>{{cite web |title= 2013 Form 990 |work= American Musicological Society |publisher= [[Guidestar]] |date= June 30, 2014 |url= http://www.guidestar.org/FinDocuments/2014/231/577/2014-231577392-0b2d2680-9.pdf }}</ref>
|type         = [[501(c)_organization#501(c)(3)|501(c)(3)]] [[nonprofit organization]]
|status       = 
|purpose      = Advancement of research in the various fields of music as a learning branch
|headquarters = [[Brunswick, Maine]]
|region_served= United States
|leader_title = Director
|leader_name  = Robert Judd<ref name= AMS990/>
|main_organ   = 
|revenue      = $861,587<ref name= AMS990/>
|revenue_year = 2014
|expenses     = $691,796<ref name= AMS990/>

|expenses_year= 2014
|staff        = 1<ref name= AMS990/>
|staff_year   = 2014
|volunteers   = 300<ref name= AMS990/>

|volunteers_year=2014

|tax_id       = 23-1577392<ref name= AMS990/>
|website      = {{url|http://www.ams-net.org/}}
}}

== Overview ==
The society consists of individual members divided among fifteen regional chapters across the United States, Canada, and elsewhere, as well as subscribing institutions. It was admitted to the [[American Council of Learned Societies]] in 1951, and participates in the [[Répertoire International des Sources Musicales]] and the [[Répertoire International de Littérature Musicale]].

The society's annual meetings consist of presentations, [[symposium|symposia]], and concerts, as well as more-or-less informal meetings of numerous related musical societies. Many of the society's awards, prizes and fellowships are announced at these meetings.

== Publications ==
Most of the society's resources are dedicated to musicological publications: the triannual ''[[Journal of the American Musicological Society]]'' (1948-present) published by the [[University of California Press]]. The journal was preceded by the annual ''Bulletin'' (1936–1947) and the annual ''Papers'' (1936–1941). Online versions of these publications are available at [[JSTOR]] and the University of California Press.

Other studies and documents published by the society include the complete works of [[William Billings]], edited by [[Karl Kroeger]] et al. (4 vols, 1977–1990), the series ''Music of the United States of America'' (1993–present),<ref>http://www.umich.edu/%7Emusausa/</ref> [[Johannes Ockeghem]]'s collected works edited by [[Dragan Plamenac]] and Richard Wexler (3 vols., 1966, 1992), [[John Dunstaple]]'s complete works edited by [[Manfred Bukofzer]], published jointly with [[Musica Britannica]] (1970), [[Joseph Kerman]]'s ''The Elizabethan Madrigal'' (1962),<ref>http://www.ams-net.org/pubs/Kerman.php</ref> E. R. Reilly's ''Quantz and his Versuch'' (1971), E. H. Sparks's ''The Music of Noel Bauldeweyn'' (1972), ''Essays in Musicology: a Tribute to [[Alvin H. Johnson]]'' edited by [[Lewis Lockwood]] and Edward Roesner (1990),<ref>{{cite web |url=http://www.ams-net.org/pubs/Johnson.php |title=Essays in Musicology: a Tribute to Alvin H. Johnson |website=American Musicological Society |accessdate=October 12, 2016}}</ref> and, in conjunction with the [[International Musicological Society]], ''Doctoral Dissertations in Musicology'' edited by C. D. Adkins and A. Dickinson in succession to [[Helen Hewitt]] (1952, 1957, 1961, 1965, 1971, 1977, 1984 [first cumulative edition], 1990, 1996 [second series, second cumulative edition]).<ref>http://www.ams-net.org/ddm/</ref>(Brunswick, 2011)

== See also ==
* [[Music history of the United States]]

== References ==
{{Reflist}}

== Further reading ==
* Oliver Strunk: ''State and Resources of Musicology in the United States'', ''ACLS Bulletin'' 19 (1932)
* [[Arthur Mendel]], [[Curt Sachs]], and [[Carroll C. Pratt]]: ''Some Aspects of Musicology'' (New York, 1957)
* B. S. Brook, ed.: ''American Musicological Society, Greater New York Chapter: a Programmatic History 1935-1965'' (New York, c1965)
* W. J. Mitchell: "A Hitherto Unknown--or a Recently Discovered...," ''Musicology and the Computer,'' ed. B. S. Brook (New York, 1970), 1-8
* [[Richard Crawford (music historian)|Richard Crawford]]: [http://www.ams-net.org/resources/Anniversary_Essay.pdf ''The American Musicological Society 1934-1984. An Anniversary Essay''] (Philadelphia, 1984)
* [http://www.ams-net.org/resources/Celebrating-the-AMS-at-Seventy-five.pdf ''Celebrating the American Musicological Society at Seventy-Five'']

== External links ==
{{Commons category|American Musicological Society}}
* {{Official website|http://www.ams-net.org/}}
* [http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_MsColl221 American Musicological records, Kislak Center for Special Collections, Rare Books and Manuscripts, University of Pennsylvania]
* [http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_PUSpMsColl962 American Musicological Society oral history collection, 1996-present, Kislak Center for Special Collections, Rare Books and Manuscripts, University of Pennsylvania]
* [http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_MsColl645 American Musicological Society supplementary records, 1950-2003 (bulk 1980-2003), Ms. Coll. 645, Kislak Center for Special Collections, Rare Books and Manuscripts, University of Pennsylvania]
* [http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_MsColl222 Journal of the American Musicological Society records, Kislak Center for Special Collections, Rare Books and Manuscripts, University of Pennsylvania]

[[Category:Academic organizations based in the United States]]
[[Category:Musicology]]
[[Category:Member organizations of the American Council of Learned Societies]]
[[Category:Music organizations based in the United States]]
[[Category:1934 establishments in the United States]]
[[Category:Organizations established in 1934]]
[[Category:Music-related professional associations]]