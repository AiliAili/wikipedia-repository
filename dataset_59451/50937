{{Infobox journal
| title         = Clinical Microbiology Reviews
| cover         = [[File:Clin_Microbiol_Rev_cover.gif|200px]]
| caption       = 
| abbreviation  = Clin. Microbiol. Rev.
| discipline    = [[Microbiology]], [[immunology]]
| language      = English
| editor        = Jo-Anne H. Young
| publisher     = [[American Society for Microbiology]]
| country       = United States
| history       = 1988-present
| frequency     = Continuous online publication, quarterly print
| openaccess    = [[Delayed open access journal|Delayed]], after 12 months
| license       = 
| impact        = 17.406
| impact-year   = 2014
| ISSNlabel     =
| ISSN          = 0893-8512
| eISSN         = 1098-6618
| CODEN         = CMIREX
| JSTOR         = 
| LCCN          = 88647279
| OCLC          = 38839512
| website       = http://cmr.asm.org/
| link3         = http://www.ncbi.nlm.nih.gov/pmc/journals/85/
| link3-name    = PubMed Central archive
| link1         = http://cmr.asm.org/content/current
| link1-name    = Online access
| link2         = http://cmr.asm.org/content/by/year
| link2-name    = Online archive
| boxwidth      = 
}}

'''''Clinical Microbiology Reviews''''' (CMR) is a [[peer reviewed|peer-reviewed]] [[academic journal]] that publishes scholarly works of interest in the areas of clinical [[microbiology]], [[immunology]], medical microbiology, infectious diseases, veterinary microbiology, and microbial pathogenesis. It is a [[delayed open access journal]], full content is accessible via [[PubMed Central]] and the journal's website after a 12-month [[Embargo (academic publishing)|embargo]]. In April 2015, the journal transitioned to a continuous online publication model for CMR (whereby articles are published as they become ready, before the issue in which they will appear has been finalized). There is still a quarterly print issue. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 17.406, ranking it 2nd out of 119 journals in the category "Microbiology".<ref name="WoS">{{cite book |year=2014 |chapter=Journals Ranked by Impact: Microbiology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> CMR was established in January 1988.<ref name="Morello1999"/> The founding editor was [[Josephine A. Morello]] ([[University of Chicago Medical Center]]).<ref name="Morello1999">{{cite journal |last= Morello |first= JA |date= April 1999 |title= ''Clinical Microbiology Reviews'': Genesis of a journal |journal= Clinical Microbiology Reviews |volume= 12 |issue= 2 |pages= 183–6 |pmid= 10194455 |pmc= 88913 |url= http://cmr.asm.org/content/12/2/183.long}}</ref> Editorial board structure changed in 1992 and Morello became [[editor-in-chief]].<ref name="Morello1999"/> [[Betty Ann Forbes]] ([[State University of New York]]) was appointed editor-in-chief in 1997.<ref name="Morello1999"/> [[Irving Nachamkin (University of Pennsylvania)]] was appointed editor-in-chief in 2002 until 2012. [[Jo-Anne H. Young]]'s ([[University of Minnesota]]) term as editor-in-chief ends in 2017. It is the ninth journal established and published by the [[American Society for Microbiology]].<ref name="Morello1999"/>

==Abstracting and indexing==

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[AGRICOLA]]
*[[BIOSIS Previews]]<ref name= ISI/>
*[[CAB Abstracts]]<ref name= CABAB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title= Serials cited |work= [[CAB Abstracts]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-13}}</ref>
*[[Cambridge Scientific Abstracts]]
*[[Chemical Abstracts]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-13 }}</ref>
*[[Current Contents]]- Life Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-13}}</ref>
*[[Elsevier BIOBASE]]<ref name=UWEB/>
*[[Embase]]<ref name=UWEB/>
*[[Food Science and Technology Abstracts]]
*[[Global Health]]<ref name= CABGH>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/global-health/ |title= Serials cited |work= [[Global Health]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
*Illustrata
*[[Index Medicus]]/[[PubMed]]/[[MEDLINE]]/[[PubMed Central]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8807282 |title=''Clinical Microbiology Reviews'' |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-13}}</ref>
*[[International Bibliography of Periodical Literature]]<ref name=UWEB>{{cite web |title= ''Clinical Microbiology Reviews'' |url= http://www.ulrichsweb.serialssolutions.com/title/1418911900433/168868 |work= [[Ulrichsweb]] |publisher= [[ProQuest]] |accessdate=2014-12-18 |subscription= yes}}</ref>
*[[Science Citation Index Expanded]]<ref name= ISI/>
*[[Scopus]]<ref name=UWEB/>
*[[Tropical Diseases Bulletin]]<ref name= CABTDB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/tropical-diseases-bulletin/ |title= Serials cited |work= [[Tropical Diseases Bulletin]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 17.406, ranking it 2nd out of 119 journals in the category "Microbiology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Microbiology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
*[[Clinical research]]

==References==
{{reflist|30em}}

== External links ==
*{{Official website|http://cmr.asm.org}}
*[http://www.asm.org/ American Society for Microbiology]

[[Category:Microbiology journals]]
[[Category:Delayed open access journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Review journals]]
[[Category:Publications established in 1988]]