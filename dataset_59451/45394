{{refimprove|date=July 2016}}
{{Infobox military person
|honorific_prefix  =
|name              =Hans-Joachim Herrmann
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         = Bundesarchiv Bild 146-1985-015-20, Hajo Herrmann.jpg
|image_size    =
|alt           =
|caption       =Hajo Herrmann in January 1944
|birth_date    ={{Birth date|1913|08|01|df=yes}}
|death_date    ={{Death date and age|2010|11|05|1913|08|01|df=yes}}
|birth_place   =[[Kiel]], [[Kingdom of Prussia]]
|death_place   =
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =Hajo
|birth_name    =
|allegiance    ={{flag|Nazi Germany|23px}}
|branch        ={{Luftwaffe}}
|serviceyears  =1935–45
|rank          =[[Oberst]]
|servicenumber =
|unit          =[[KG 4]]<br/>[[KG 30]]<br/>[[JG 300]]
|commands      =
|battles       =[[Spanish Civil War]]
----
[[World War II]]
*[[Battle of Britain]]
*[[Balkans Campaign (World War II)|Balkans Campaign]]
|battles_label =
|awards        =[[Knight's Cross of the Iron Cross with Oak Leaves and Swords]]
|spouse        =[[Ingeborg Reichelt]]
|laterwork     =[[Lawyer]]
|signature     =
|website       = <!-- {{URL|example.com}} -->
}}

'''Hans-Joachim "Hajo" Herrmann''' (1 August 1913 – 5 November 2010)<ref>http://www.pro-sarrazin.net/archives/2549</ref><ref>http://npd-thueringen.de/?p=430</ref> was a [[Luftwaffe]] (Nazi Germany air force) bomber pilot.  In [[World War II]], he was a high-ranking and influential member of the Luftwaffe, and a recipient of the [[Knight's Cross of the Iron Cross with Oak Leaves and Swords]]. The Knight's Cross of the Iron Cross and its higher grade Oak Leaves and Swords was awarded to recognise extreme battlefield bravery or successful military leadership.

After World War II Hermann became a lawyer whose high-profile cases included the defense of [[neo-Nazi]]s and [[Holocaust denial|Holocaust deniers]].

== Military career ==
Beginning his military career as an infantry officer, Herrmann was commissioned in the newly formed Luftwaffe in 1935. From 1936 until 1937, he was a bomber pilot in the [[Condor Legion]]. During the [[Spanish Civil War]], Herrmann joined [[Kampfgeschwader 4|KG-4]]. When World War II began, he flew bombing missions into [[Poland]] and [[Norway]]. By 1940, he was Commander of the 7th Staffel of KG-4, and took part in the [[Battle of Britain]]. In February 1941, his group went to [[Sicily]], from where it attacked [[Malta]] and Greece. In one such attack, Herrmann dropped a single bomb on an ammunition ship, the [[SS Clan Fraser (1938)|SS Clan Fraser]];{{sfn|Smith|Kay|1972|p=405}} the resulting explosion sank 11 ships and made the Greek port of [[Piraeus]] unusable for many months. In early 1942, he was commander of [[Kampfgeschwader 30|III./KG 30]], attacking Arctic convoys from Norway, including the attacks on [[Convoy PQ 17]]. In July 1942 he was assigned to the general staff in Germany, where he became a close confidant of [[Hermann Göring]], the head of the Luftwaffe. During his career as a bomber pilot, Herrmann flew 320 missions and sank 12 ships totalling 70,000 tons.

In 1942, Herrmann was appointed to the Luftwaffe Operational Staff. Gaining a reputation as a tactical and operational innovators, he was the creator of the Luftwaffe [[night fighter]] wing designated [[Jagdgeschwader 300]], which was a response to the [[Royal Air Force]] Bomber Command's night raids on the Germany in mid-1943. RAF had achieved ascendancy over the Luftwaffe's ''Nachtjäger'' radar-guided night fighter forces through the use of the [[radar countermeasure]] [[chaff (radar countermeasure)|chaff]], and Herrmann's theory was for experienced night flying pilots and ex-instructors to be equipped with [[Fw 190]] day fighters and visually 'free-hunt' the bombers by the light of the fires below and with the aid of special 'flare-carrier' [[Junkers Ju 88]]s following the bomber streams. He also called for the use of the [[Naxos radar detector]] units on some of these single engined fighters to locate the bombers when they were aiming using radar. Herrmann himself flew more than 50 night fighter missions and claimed nine RAF bombers destroyed. Although JG 300 and subsequent units had promising initial success, the wastage of both pilots and aircraft due to high accident rates curtailed extensive use of 'Wilde Sau' beyond the start of 1944.

In December 1943, Herrmann was appointed Luftwaffe Inspector of Aerial Defence.  By 1944, he was Inspector General of night fighters and received the Knight's Cross with Oak Leaves and Swords. At the end of 1944, he led the [[9th Air Division (Germany)|9. ''Flieger-division (J)'']]. At this time he was a leading exponent of the tactical deployment of the so-called ''Rammjäger'' ''[[Sonderkommando Elbe]]'' (German: ram fighters, task force [[Elbe]]), sent into action in April 1945. Pilot volunteers, often aged 18 to 20, were to be trained to be simply competent enough to control specially lightened and unarmoured [[Bf 109]] fighters and charged with downing Allied bombers by deliberately ramming the tail or control surfaces with the propellers of their aircraft, and thereafter bailing out, if possible. Herrmann's intention was to gather a large number of these fighters for a one-off attack on the [[USAAF]] bomber streams, hopefully causing enough losses to curtail the bombing offensive for a few months. Fuel shortages prevented employment of the large numbers necessary, although from one mission of this type, on 7 April 1945, of the 120 planes thus committed only 15 came back.{{sfn|Smith|Kay|1972|p=492}}

== Post war activities ==
Herrmann was captured by the Soviets after the war and was held prisoner for 10 years before returning to Germany in 1955. Once back, he studied law and settled in [[Düsseldorf]]. Among others, he defended [[Otto Ernst Remer]], the head of the [[neo-Nazi]] [[Socialist Reich Party]]; and the [[Holocaust denial|Holocaust deniers]] [[David Irving]], and [[Fred A. Leuchter]].<ref>{{cite web|title=Documents on the Legal Actions against David Irving|url=http://www.fpp.co.uk/Irving/photos/1990s/Munich_lawyers_130193.html|publisher=|accessdate=2 August 2013}}</ref>  In 1959 Herrmann married the German soprano [[Ingeborg Reichelt]], and the couple had two children.<ref>{{cite web|title=Obituaries|url=http://www.telegraph.co.uk/news/obituaries/military-obituaries/air-force-obituaries/8158054/Hans-Joachim-Herrmann.html|publisher=Telegraph|accessdate=2 August 2013}}</ref>

== Awards ==
* [[Spanish Cross]] in Bronze with Swords{{sfn|Berger|1999|p=125}}
* [[Iron Cross]] (1939) 2nd Class (October 1939) & 1st Class (27 May 1940){{sfn|Thomas|1997|p=275}}
* [[Ehrenpokal der Luftwaffe]] (28 September 1940){{sfn|Obermaier|1989|p=34}}
<!---* [[Front Flying Clasp of the Luftwaffe]] in Gold with Pennant "300"{{sfn|Berger|1999|p=125}}
* [[Pilot/Observer Badge]] in Gold with Diamonds--->
* [[German Cross]] in Gold on 5 June 1942 as ''[[Hauptmann]]'' in the III./KG 30{{sfn|Patzwall|Scherzer|2001|p=181}}
* [[Knight's Cross of the Iron Cross with Oak Leaves and Swords]]
** Knight's Cross on 13 October 1940 as ''[[Oberleutnant]]'' and ''[[Staffelkapitän]]'' of the 7./KG 4 "General Wever"{{sfn|Scherzer|2007|p=385}}
** 269th Oak Leaves on 2 August 1943 as ''[[Major (Germany)|Major]]'' and ''[[Geschwaderkommodore]]'' of JG 300{{sfn|Scherzer|2007|p=385}}
** 43rd Swords on 23 January 1944 as ''[[Oberst]]'' and ''Inspekteur der Nachtjagd'' in the ''[[Reichsluftfahrtministerium]]'' and commander of the 30. Jagd-Division{{sfn|Scherzer|2007|p=385}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Berger
  |first=Florian
  |year=1999
  |title=Mit Eichenlaub und Schwertern. Die höchstdekorierten Soldaten des Zweiten Weltkrieges
  |trans_title=With Oak Leaves and Swords. The Highest Decorated Soldiers of the Second World War
  |language=German
  |location=Vienna, Austria
  |publisher=Selbstverlag Florian Berger
  |isbn=978-3-9501307-0-6
  |ref=harv
}}
* {{Cite book
  |last=Kaiser
  |first=Jochen
  |year=2010
  |title=Die Ritterkreuzträger der Kampfflieger—Band 1
  |trans_title=The Knight's Cross Bearers of the Bomber Fliers—Volume 1
  |language=German, English
  |location=Bad Zwischenahn, Germany
  |publisher=Luftfahrtverlag-Start
  |isbn=978-3-941437-07-4
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last1=Smith
  |first1=J.R.
  |last2=Kay
  |first2=A.
  |year=1972
  |title=German Aircraft of the Second World War
  |location=London
  |publisher=Putnam
  |isbn=978-0-370-00024-4
  |ref=harv
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
  |ref=harv
}}
{{Refend}}

== External links ==
* {{DNB portal|118703854|TYP=}}
* [http://www.telegraph.co.uk/news/obituaries/military-obituaries/air-force-obituaries/8158054/Hans-Joachim-Herrmann.html Obituary of Hans-Joachim Herrmann, The Daily Telegraph, 24 November, 2010]

{{S-start}}
{{S-mil}}
{{Succession box
| before = none
| after  = Oberstleutnant [[Kurd Kettner]]
| title  = Commander of [[Jagdgeschwader 300]]
| years  = June 1943 – 26 September 1943
}}
{{Succession box
| before = none
| after  = disbanded
| title  = Commander of ''[[30th Fighter Division (Germany)|30. Jagd-Division]]''
| years  = September 1943 – 16 March 1944
}}
{{Succession box
| before = Oberst [[Günther Lützow]]
| after  = Generalleutnant [[Kurt Kleinrath]]
| title  = Commander of ''[[1st Fighter Division (Germany)|1. Jagd-Division]]''
| years  = 23 March 1944 – 1 September 1944
}}
{{Succession box
| before = none
| after  = none
| title  = Commander of ''[[9th Air Division (Germany)|9. Flieger-Division (J)]]''
| years  = 26 January 1945 – 8 May 1945
}}
{{S-end}}

{{KCwithOLandSW}}
{{Knight's Cross recipients of KG 4}}
{{Knight's Cross recipients of JG 30X}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
| commons=y
}}

{{DEFAULTSORT:Herrmann, Hajo}}
[[Category:1913 births]]
[[Category:2010 deaths]]
[[Category:German World War II flying aces]]
[[Category:Luftwaffe World War II generals]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves and Swords]]
[[Category:Recipients of the Spanish Cross]]
[[Category:People from Kiel]]
[[Category:People from the Province of Schleswig-Holstein]]
[[Category:Condor Legion personnel]]
[[Category:Burials at sea]]