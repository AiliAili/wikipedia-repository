{{good article}}
{{Infobox settlement
| name                 = Banton
| official_name        = ''Bayan ng Banton''
| native_name          = 
| other_name           = 
| settlement_type      = [[Municipalities of the Philippines|Municipality]]
| image_skyline        = {{Photomontage
| photo1a = Banton Island Horizon.JPG
| photo2a = Fuerza San Jose.JPG
| photo2b = Banton Church 20.JPG
| photo3a = Macat-ang Beach Banton.JPG
| photo3b = Banton wooden coffin.JPG
| photo4a = Banton town hall.JPG
| photo4b = Banton Poblacion.JPG
| size = 250
| position = center
| spacing = 5
| color = transparent
| border = 0
}}
| image_caption        = (From top, left to right) Banton Island, Fort San Jose, the 16th century St. Nicholas de Tolentino Parish Church,  Macat-ang Beach in Brgy. Mainit, a pre-colonial artifacts found in Banton's Guyangan Cave System, Banton Civic Center, and Banton's ''poblacion'' viewed from Manamyaw Cliff.
| image_seal           = Banton Romblon.png
| seal_size            = 100x80px
| image_map            = {{PH wikidata|image_map}}
| map_caption          = Map of Romblon with Banton highlighted
| pushpin_map          = Philippines
| pushpin_map_caption  = Location within the Philippines
| coordinates_wikidata = yes
| coordinates_display  = inline,title
| coordinates_region   = PH
| subdivision_type     = Country
| subdivision_name     = [[Philippines]]
| subdivision_type1    = [[Regions of the Philippines|Region]]
| subdivision_name1    = [[MIMAROPA]] (Region IV-B)
| subdivision_type2    = [[Provinces of the Philippines|Province]]
| subdivision_name2    = [[Romblon]]
| subdivision_type3    = [[Legislative districts of the Philippines|District]]
| subdivision_name3    = [[Legislative district of Romblon|Lone district]]
| established_title    = Founded
| established_date     = 1622<ref name="Banton">{{cite web |url=http://www.yagting.com/bantonvol01/briefhistory.asp |title=Banton: Brief History |publisher=Banton Official Website |accessdate=14 April 2012}}</ref>
| parts_type           = [[Barangay]]s
| parts                = 17 (see § [[#Barangays|Barangays]])
| government_type      = [[Mayor-Council]]
| government_footnotes = <ref name=Election2016>{{cite web |url=http://www.romblonnews.net/index.php/election-2016/782-list-of-winners-municipality-of-banton |title=List of Winners: Municipality of Banton |publisher=''Romblon News'' |location=Odiongan, Romblon |date=12 May 2016 |accessdate=28 July 2016}}</ref>
| leader_title         = Mayor
| leader_name          = Joseph Fadri ([[Liberal Party (Philippines)|LP]])
| leader_title1        = Vice Mayor
| leader_name1         = Loijorge Fegalan ([[Nacionalista Party|NP]])
| leader_title2        = Town Council
| leader_name2         = {{PH Town Council
 | Bert Fadrilan
 | RG Festin
 | Tess Fajut
 | Allan Fabula
 | Jeff Famadico
 | Pat Flores
 | Imelda Fietas
 | Carmie Faderanga
}}
| area_footnotes         = <ref name=nscb>{{cite web|title=Province: Romblon |url=http://www.nscb.gov.ph/activestats/psgc/province.asp?regName=REGION+IV-B+%28MIMAROPA%29&regCode=17&provCode=175900000&provName=ROMBLON |work=PSGC Interactive |publisher=National Statistical Coordination Board |accessdate=31 January 2013 |location=Makati City, Philippines |deadurl=yes |archiveurl=https://web.archive.org/web/20121114165642/http://www.nscb.gov.ph/activestats/psgc/province.asp?regName=REGION+IV%2DB+%28MIMAROPA%29&regCode=17&provCode=175900000&provName=ROMBLON |archivedate=14 November 2012 |df= }}</ref>
| area_total_km2         = {{PH wikidata|area}}
| elevation_max_m        = 596
| elevation_min_m        = 
| population_footnotes   = {{PH census|current}}
| population_total       = {{PH wikidata|population_total}}
| population_as_of       = {{PH wikidata|population_as_of}}
| population_density_km2 = auto
| population_demonym     = Bantoanon<ref name="Profile">{{cite web |url=http://tourism-philippines.com/banton-island/ |title=Banton Island |first=Epi |last=Fabonan III |date=29 May 2009 |publisher=Tourism Philippines |accessdate=14 April 2012}}</ref>
| timezone               = [[Time in the Philippines|PST]]
| utc_offset             = +8
| postal_code_type       = [[List of ZIP codes in the Philippines|Zip Code]]
| postal_code            = {{PH wikidata|postal_code}}
| area_code_type         = {{areacodestyle}}
| area_code              = {{PH wikidata|area_code}}
| blank1_name            = [[Languages]]
| blank1_info            = [[Asi language|Asi]], [[English language|English]], [[Filipino language|Filipino]]
| blank2_name            = [[Municipalities of the Philippines#Income classification|Income class]]
| blank2_info            = 5th class
| blank_name_sec1        = [[Patron saint]]
| blank_info_sec1        = [[St. Nicholas of Tolentino|San Nicolas de Tolentino]]
}}
'''Banton''' (formerly known as '''Jones''') is a [[Philippine municipality|fifth-class municipality]] in the [[Philippine province|province]] of [[Romblon]], Philippines. Its territory encompasses the entire island of [[Banton Island|Banton]] located on the northern portion of the province and lies on the northern portion of the [[Sibuyan Sea]] near the southern tip of [[Marinduque]]. It is a town of about 5,000 people majority of which speak the [[Asi language|Bantoanon language]], one of the five primary branches of the [[Visayan languages]].

Banton is thought to be already inhabited by [[Filipinos]] since the pre-colonial period, based on analysis of discovered human remains, coffins, an ancient burial cloth and other archaeological finds by the [[National Museum of the Philippines|National Museum]] in the 1930s. The present settlement was founded in 1622 by the Spanish and is the oldest settlement in the province. During the American colonial period, the municipality changed its name to Jones in honor of American [[congressman]] [[William Atkinson Jones|William Jones]], who authored the [[Jones Law (Philippines)|Philippine Autonomy Act of 1916]]. Today, Banton is one of Romblon's thriving municipalities, with an economy dependent on [[copra]] farming, fishing, [[raffia palm|raffia palm weaving]], and tourism.

==Etymology==
The name "Banton" was derived from the [[Asi language|Asi]] word ''batoon'', meaning "rocky", referring to the mountainous and rocky topography of the island due to its volcanic origin.<ref name="Profile"/> Another possible origin is the word ''bantoy'', which is the Asi word for the venomous [[Synanceia|stonefish]].<ref name="Banton"/>

==History==
[[File:Ipot Cave.JPG|thumb|left|Ipot Cave, where the earliest known warp ''[[ikat]]'' textile in Southeast Asia was found in 1936]]
[[File:Fort San Jose, Banton, Romblon.JPG|thumb|left|Fuerza de San Jose, Banton's Spanish colonial era fort]]

===Early history===
Banton was already inhabited during pre-colonial times as proven by ancient artefacts such as wooden coffins and skeletal remains found in the island's caves in 1936 by a team of researchers from the [[National Museum of the Philippines]]. Among the artefacts was the Banton Cloth, a piece of a traditional burial cloth found in one of the wooden coffins. It is estimated to be 400 years old, making it the earliest known warp [[ikat]] (tie-resist dyeing) textile in the Philippines and Southeast Asia.<ref  name=NM>{{cite web|url=http://www.nationalmuseum.gov.ph/nationalmuseumbeta/Collections/Archaeo/Banton.html|title=Banton Cloth|last=|first=|date=10 February 2014|website=|publisher=National Museum of the Philippines|access-date=3 March 2016}}</ref><ref name="Ikat">{{cite news |url=http://opinion.inquirer.net/15599/history-and-design-in-death-blankets |title=Looking Back: History and design in Death Blankets |first=Ambeth |last=Ocampo |date=19 October 2011 |work=[[Philippine Daily Inquirer]] |deadurl=no |accessdate=24 April 2014}}</ref><ref>{{cite web |url=http://www.filipiknow.net/archaeological-discoveries-in-the-philippines/ |title=15 Most Intense Archaeological Discoveries in Philippine History |first=Luisito |last=Batongbakal, Jr. |work=Filipiknow.net |deadurl=no |accessdate=14 August 2015}}</ref> These artifacts are now preserved at the [[National Museum of Anthropology (Manila)|National Museum of Anthropology]] in [[Manila]].<ref  name=NM/>

The [[municipality]] of Banton was established by Spanish colonial authorities in 1622, the first town established in [[Romblon]], which was then part of [[Capiz]] province. It was initially founded in a site in Bacoco Hill (now part of Barangay Hambian), south-west of its present site. The administration of the other islands of Romblon were put under the jurisdiction of Banton until 1631, when [[Romblon, Romblon|Pueblo de Romblon]] was founded.<ref name="Banton"/> In 1640, due to frequent raids by [[Moro people|Moros]], who looted and pillaged the settlement, the limestone fort called ''Fuerza de San Jose'' and the [[Banton Church|San Nicolas de Tolentino Parish Church]] was constructed under the leadership of Father Agustin de San Pedro, also known as ''El Padre Capitan'', who was the parish priest of Banton at that time. The construction was completed in 1644, and in 1648, [[Nicholas of Tolentino|San Nicolas de Tolentino]] was installed as the town's patron saint. The fort effectively protected the town against further Moro raids. Banton ceased being part of Capiz when the Spanish colonial government created the politico-military province of Romblon on 11 January 1868.<ref name="Banton"/> 

===Modern history===
When [[civilian government]] was introduced in Romblon by the [[United States|American]]s on 16 March 1901, Banton was one of the 11 new municipalities reinstated or created. In 1918, the municipality was renamed Jones in honor of American [[congressman]] [[William Atkinson Jones|William Jones]], who authored the [[Jones Law (Philippines)|Philippine Autonomy Act of 1916]] that provided for greater autonomy for the Philippines under American colonial rule.<ref name="Jones">Buencamino, Felipe & Villamor, Ignacio. (1920)</ref> On 8 June 1940, with the passage of Commonwealth Act No. 581, all of Romblon's municipalities were dissolved and Jones, together with Corcuera and Concepcion, were consolidated into a special municipality called Maghali, one of four special municipalities that the law created (the rest being Romblon, Tablas and Sibuyan).<ref name="Esquejo">{{cite book |title=The Making of a Philippine Province: Romblon During the American Colonial Period |author=Esquejo, Kristoffer |year=2014 |work=Asian Studies Journal |location=Manila: University of the Philippines-Diliman}}</ref> The reorganization proved to be difficult for the province's local leaders, and after [[World War II]], Republic Act No. 38 repealed Commonwealth Act No. 581, restoring Jones to its pre-war status.<ref name="Esquejo"/> On 24 April 1959, Republic Act No. 2158 restored the island to its former name, Banton.<ref>{{cite web |url=http://philippinelaw.info/statutes/ra2158.html |title=R. A. No. 2158 |work=An Act Changing the Name of the Municipality of Jones, province of Romblon, to municipality of Banton |publisher=PhilippineLaw.info |accessdate=14 April 2012}}</ref>

In 2013, Banton was one of the sites of a detailed resource assessment by the [[Department of Energy (Philippines)|Department of Energy (DOE)]], along with [[Maricaban Island]] in [[Batangas]] and [[Balut Island]] in [[Saranggani]]. The study aimed to determine whether the island can be a site for low [[enthalpy]] [[geothermal electricity|geothermal power generation]]. However, no  exploitable geothermal  resource  has  been delineated on the island.<ref name="Volcano">Halcon, Rainier, Fronda, Ariel et al. (2015)</ref><ref>{{cite web|url=http://www.mb.com.ph/doe-to-bid-out-3-new-geothermal-sites/ |title=DOE to bid out 3 new geothermal sites |first=James |last=Loyola |work=Manila Bulletin |date=21 June 2014 |accessdate=14 August 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20151209080818/http://www.mb.com.ph/doe-to-bid-out-3-new-geothermal-sites/ |archivedate=9 December 2015 |df= }}</ref><ref>{{cite web |url=http://www.bworldonline.com/content.php?section=Economy&title=Geothermal-area-up-for-auction-in-Q3&id=89513 |title=Geothermal area up for auction in Q3 |first=Claire-Ann |last=Feliciano |date=19 June 2014 |publisher=''BusinessWorld'' |accessdate=14 August 2015}}</ref> On 19 March 2013, the National Museum declared as Important Cultural Property the island's Guyangan Cave System, where precolonial wooden coffins, remains, and the Banton Cloth were found.<ref name=Guyangan>{{cite web |url=http://www.nationalmuseum.gov.ph/nationalmuseumbeta/Museums%20and%20Branches/PressRelease2.html |title=2013 Heritage Pride: NM declarations of National Cultural Treasures (NCTs) and Important Cultural Properties (ICPs) |author=Cultural Properties Division |publisher=National Museum of the Philippines |accessdate=14 August 2015}}</ref><ref>{{cite web |url=http://lifestyle.inquirer.net/105261/romblon-cathedral-ancient-hispanic-forts-declared-national-cultural-treasures/ |title=Romblon cathedral, ancient Hispanic forts declared National Cultural Treasures |first=Levine Andro |last=Lao |work=Inquirer.net |date=26 May 2013 |accessdate=14 August 2015}}</ref><ref>{{cite web |url=http://www.ivanhenares.com/2014/01/philippine-registry-of-cultural.html |title=Updated Philippine Registry of Cultural Property (PRECUP) |first=Ivan |last=Henares |work=Ivan About Town|date=26 January 2014 |accessdate=14 August 2015}}</ref>

On 15 December 2015, [[Typhoon Melor (2015)|Typhoon Melor]] made its fourth landfall over the island as it crossed central Philippines, causing severe devastation.<ref>{{cite web |url=http://www.philstar.com/headlines/2015/12/15/1532884/nona-makes-fifth-landfall-oriental-mindoro |title='Nona' makes fifth landfall in Oriental Mindoro |date=15 December 2015 |work=[[The Philippine STAR]] |accessdate=16 December 2015}}</ref>

==Geography==
[[File:Banton Island Horizon.JPG|thumb|right|Banton Island]]
[[File:Bantoncillo.JPG|thumb|right|Bantoncillo Island]]
[[File:Dos Hermanas, Banton, Romblon.JPG|thumb|right|The Dos Hermanas Islands, composed of Carlota and Isabel Islands]]
Banton lies on the northern portion of the [[Sibuyan Sea]], and is equidistant between [[Marinduque Island]] to the north and [[Tablas Island]] to the south. It is composed of the main island of [[Banton Island|Banton]] and the uninhabited islands of [[Bantoncillo Island|Bantoncillo]], [[Carlota Island|Carlota]] and [[Isabela Island (Philippines)|Isabel]], the last two of which are collectively known as the Dos Hermanas Islands. There is also an islet near Tabonan Beach on the north-west of the island.<ref name="Banton"/>
 
Banton has a total land area of {{convert|3,248|ha|km2}}.<ref name="Stats">{{cite web |title=Municipality: Banton |url=http://www.nscb.gov.ph/activestats/psgc/municipality.asp?muncode=175902000&regcode=17&provcode=59 |work=PSGC Interactive |publisher=National Statistical Coordination Board |accessdate=31 January 2013 |location=Makati City, Philippines}}</ref> Based on rock [[petrology]], the island is a [[volcano|dormant volcano]] which lies at the southernmost portion of the [[Pleistocene]]-[[Quaternary]] West Luzon [[volcanic arc]] and may have been active during the [[Pliocene|Pliocene period]].<ref name="Volcano"/> Because of its volcanic origin, the island has a mountainous, rocky topography, with very few patches of flat land suitable for farming. The island's highest elevation, Mount Ampongo, rises at {{convert|596|m|ft}}.<ref name="Profile"/>

===Barangays===
Banton is politically subdivided into 17 [[barangay]]s.<ref name="Stats"/> In 1954, the [[sitio]]s of Mahaba, Angomon, Solocan, Kapanranan, and Yabawon were consolidated into the barangay known as Yabawon.<ref name="Barrios">{{cite web|url=http://lawph.com/statutes/ra1014.html |archive-url=https://archive.is/20120712145110/http://lawph.com/statutes/ra1014.html |dead-url=yes |archive-date=12 July 2012 |title=R.A. No. 1014 |work=An Act Creating the Barrio of Yabawon in the Municipality of Jones, Province of Romblon |publisher=PhilippineLaw.info |accessdate=24 April 2014 }}</ref>

{{columns-list|3|
* Balogo
* Banice
* Hambi-an
* Lagang
* Libtong
* Mainit
* Nabalay
* Nasunogan
* Poblacion
* Sibay
* Tan-Ag
* Toctoc
* Togbongan
* Togong
* Tungonan
* Tumalum
* Yabawon
}}

===Climate===
As part of Romblon, Banton is classified under Type III of the Corona climatic classification system. This type of climate is described as having no prominent wet or dry seasons. The wet season, which usually occurs from June to November can extend up to December during the onset of the southwest monsoon. The dry season from January to May may sometimes have periods of rainfall or even inclement weather.<ref name="RomblonProv">{{cite web |url=http://www.romblonprov.gov.ph/pages.php?m=romblon_province |title=History |work=Romblon Province |publisher=Romblon Provincial Government |accessdate=8 September 2015}}</ref><ref name="PSA">{{cite web |url=https://psa4bromblon.files.wordpress.com/2013/08/romblon-profile.pdf |title=History |work=Profile of Romblon Province |location=Romblon: Philippine Statistical Authority |year=2013 |accessdate=8 September 2015}}</ref>
{{Weather box|width=auto
| location     = Banton, Romblon
| metric first = Yes
| single line  = Yes
| Jan high C = 28
| Feb high C = 29
| Mar high C = 29
| Apr high C = 27
| May high C = 32
| Jun high C = 31
| Jul high C = 29
| Aug high C = 30
| Sep high C = 31
| Oct high C = 30
| Nov high C = 29
| Dec high C = 28
| year high C = 29
| Jan low C = 23
| Feb low C = 24
| Mar low C = 24
| Apr low C = 23
| May low C = 25
| Jun low C = 25
| Jul low C = 24
| Aug low C = 25
| Sep low C = 25
| Oct low C = 25
| Nov low C = 24
| Dec low C = 24
| year low C = 25
| precipitation colour = green
| Jan precipitation mm = 102
| Feb precipitation mm = 27
| Mar precipitation mm = 30
| Apr precipitation mm = 129
| May precipitation mm = 120
| Jun precipitation mm = 237
| Jul precipitation mm = 189
| Aug precipitation mm = 186
| Sep precipitation mm = 126
| Oct precipitation mm = 231
| Nov precipitation mm = 162
| Dec precipitation mm = 90
| year precipitation mm = 
| Jan rain days = 14
| Feb rain days = 12
| Mar rain days = 9
| Apr rain days = 11
| May rain days = 20
| Jun rain days = 20
| Jul rain days = 21
| Aug rain days = 22
| Sep rain days = 19
| Oct rain days = 21
| Nov rain days = 17
| Dec rain days = 17
| source 1 = World Weather Online<ref name="met_norms">
{{cite web
| url = http://www.worldweatheronline.com/Banton-weather-averages/Romblon/PH.aspx
| title = Banton, Romblon: Average Temperatures and Rainfall
| publisher = World Weather Online
| accessdate = 11 August 2014}}</ref>
| date     = August 2014}}

==Demographics==
{{Philippine Census| title = Population census of Banton
| 1980 = 7362
| 1990 = 7077
| 1995 = 6069
| 2000 = 6769
| 2007 = 6799
| 2010 = 5963
| 2015 = 5536
| footnote = Source: National Statistics Office{{PH census|2015}}<ref name=NSO10>{{cite web|url=http://www.census.gov.ph/sites/default/files/attachments/hsd/pressrelease/MIMAROPA.pdf |title=Total Population by Province, City, Municipality and Barangay: as of May 1, 2010 |work=2010 Census of Population and Housing |publisher=National Statistics Office |accessdate=31 January 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20121115103316/http://www.census.gov.ph/sites/default/files/attachments/hsd/pressrelease/MIMAROPA.pdf |archivedate=15 November 2012 |df= }}</ref><ref>{{cite web |title=Province of Romblon |url=http://122.54.214.222/population/MunPop.asp?prov=ROM&province=Romblon |work=Municipality Population Data |publisher=LWUA Research Division |accessdate=20 August 2014}}</ref>
|align=right
}}
[[File:Banton Church 2.JPG|right|thumb|Bird's eye view of Barangay Poblacion]]
[[File:Banton Church Retablo Mayor.JPG|right|thumb|The ''retablo'' or altarpiece of Banton Church showing different Catholic saints.]]
According to the {{PH wikidata|population_as_of}}, Banton has a population of {{PH wikidata|population_total}} people.{{PH census|current}} The island municipality is sparsely populated with a population density of {{Pop density|5536|32.48|km2|mi2|prec=0}}. According to the [[Department of Interior and Local Government (Philippines)|Department of Interior and Local Government (DILG)]] and the [[Commission on Elections (Philippines)|Commission on Elections (COMELEC)]], in 2013, there were 3,694 registered voters in Banton, spread over 31 electoral precincts. Of this figure, 1,794 are male registered voters, while 1,900 are female.<ref>{{cite web |url=http://lgu201.dilg.gov.ph/view.php?r=17&p=59&m=02 |title=Political Information: Banton, Romblon |publisher=Department of Interior and Local Government |year=2013 |accessdate=17 August 2015}}</ref>

===Language===
{{main|Asi language}}
The island's inhabitants speak the Asi language|Bantoanon language, one of three major languages spoken in Romblon and one of five primary branches of the [[Visayan languages|Visayan language family]]. The island's inhabitants were the first speakers of the language throughout the province, having spoken it since precolonial times. From Banton, the language spread to other island like [[Concepcion, Romblon|Maestro de Campo]], [[Corcuera, Romblon|Simara]], and in the towns of [[Calatrava, Romblon|Calatrava]], and [[Odiongan, Romblon|Odiongan]] in Tablas Island.<ref name="Banton"/>

David Paul Zorc, a [[linguist]] from the [[Australian National University]] whose expertise is on [[Philippine languages]], notes that Asi speakers may have been the first Visayan speakers in the [[Romblon]] region. He also suggests that Asi may have a [[Cebuano language|Cebuano]] [[substratum]] and that many of its words may have been influenced by the later influx of other languages such as [[Romblomanon language|Romblomanon]].<ref name="Zorc">Zorc, R. David Paul. (1977)</ref>

==Economy==
[[File:Copra farming in Romblon.JPG|right|thumb|[[Copra]] farming in Banton]]
Banton has a primarily agricultural economy, with [[copra]] farming and fishing as the main sources of livelihood. There is also an indigenous [[raffia palm]] handicraft industry. Other crops grown in the island are root crops (such as [[cassava]], [[sweet potatoes]]), fruits and vegetables. The locals also engage in livestock raising for local consumption, and small-scale shipbuilding of wooden boats and launches.<ref name="Profile"/>

Due to the island's rocky topography and lack of a stable supply of freshwater, rice production is difficult in the island. Rice from [[Mindoro]], [[Marinduque]] or [[Quezon Province|Quezon]] is supplied to the island by local traders. In recent years, the island has also become a small tourist hub for Asi expatriates and foreign tourists from the United States and other countries.<ref name="Profile"/>

==Tourism==
[[File:Tambak Beach.JPG|thumb|right|Tambak Beach]]
[[File:Tabunan Beach.JPG|thumb|right|Tabonan Beach]]
Banton is an [[eco-tourism]] and heritage destination due to its beaches, diving sites, caves, churches and Spanish-era fortifications.<ref name="Profile"/>

===Heritage sites===
Being the oldest settlement in Romblon, Banton has several Spanish-era fortifications and churches, as well as American-era houses. These include Fuerza de San Jose, Banton Church, the old campanile made of limestone at Everlast in Barangay Poblacion, and a limestone watchtower at Onte in Barangay Toctoc. There is an American-era house at Pinagkaisahan in Barangay Poblacion which used to be the Ugat Faigao Museum but now serves as a [[sari-sari store]]. The Asi Studies Center for Culture and the Arts (also in Everlast) serves as an information center for the Asi language and Banton history, as well as depository of Banton's archaeological and cultural artifacts. The Church of San Nicolas de Tolentino also has a small museum of pre-colonial and Spanish-era artifacts.<ref name="Banton"/><ref name="Profile"/>

===Natural formations===
Caves are Banton's well-known natural formations. The Guyangan Cave System, situated at the boundary of Barangay Toctoc and Togbongan, has seven caves, some of which were inhabited during pre-colonial times, and is now an Important Cultural Treasure as declared by the National Museum.<ref name=Guyangan/> Guyangan Hill, where the caves are situated, also has a natural view deck called Manamyaw overlooking Barangay Poblacion and the Sibuyan Sea. On a clear day, the islands of [[Sibuyan]], [[Romblon Island|Romblon]], and Tablas, as well as [[Burias Island]], can be easily seen from Manamyaw. The island has several rock formations as well. Punta Matagar in Barangay Poblacion is a pointed [[rock formation]] in the shape of a spear or arrow head. In Barangay Banice, on the southern portion of the island, lies a [[rock arch]] said to be the anchorage of "Lolo Amang", a mythological figure in Romblon's [[Sailors' superstitions|nautical folklore]] similar to the [[Flying Dutchman]].<ref name="Banton"/><ref name="Profile"/>

===Beaches===
Several beaches dot Banton's coast including Macat-ang, Tabonan, Mahaba, Recodo, Togbongan, Mainit, and Tambak beaches. Some like Macat-ang, Tabonan, and Tambak are white sand beaches, while others, like Togbongan, are pebbled and rocky.<ref name="Banton"/><ref name="Profile"/> The island's waters are also well-known dive sites, with [[coral]]s that serve as breeding ground for [[groupers]], [[Northern red snapper|snappers]], [[sharks]], and [[stingrays]].<ref>David Espinosa, Fiona Nichols, et al. (1997)</ref>

===Festivals===
Banton has annual religious and cultural festivals. The ''Sanrokan'' festival showcases the local tradition of sharing food, especially [[viand]], among neighbors and starts from [[Holy Saturday]] up to [[Easter Sunday]]. The festival has two phases: the ''Sanrokan sa Barangay'' (sharing of food in the villages) and the ''Sanrokan sa Poblacion'' (sharing of food at the town proper). [[Parlor games]] such as chasing the pig and ''[[palosebo]]'' (climbing a greased bamboo pole to claim a prize) are held during the celebration. This is followed by the ''Hanrumanan'' (meaning "souvenir/legacy") [[street dancing]] and parade.<ref name="Profile"/> Meanwhile, every year, on 10 September, the entire island pays tribute and homage to the town's patron saint, [[St. Nicholas of Tolentino|San Nicolas de Tolentino]] through the ''Biniray'' festival. [[Mass (liturgy)|Holy mass]] is held during feast day, followed by the parading of the saint's image around town. This leads to a fluvial parade around the island, with each village giving homage to the saint. Bantoanons also hold an annual [[Stations of the Cross|Via Crucis]] during the [[Holy Week]] and [[Flores de Mayo]] in May.<ref name="Banton"/>

==Local Government==
[[File:Banton town hall.JPG|right|thumb|Banton Civic Center in Barangay Poblacion is the seat of the municipal government of Banton, Romblon]]
{{main|Philippine municipality}}
Pursuant to the Local Government Code of 1991,<ref>{{cite web |url=http://www.lawphil.net/statutes/repacts/ra1991/ra_7160_1991.html |title=An Act Providing for a Local Government Code of 1991 |publisher=8th Congress of the Republic of the Philippines |accessdate=April 21, 2014}}</ref> the Banton municipal government is composed of a [[mayor]] (''alkalde''), a [[vice mayor]] (''bise alkalde'') and eight [[councilors|members]] (''kagawad'') of the ''[[Sangguniang Bayan]]'' or town council, alongside a secretary to the said council, all of which are elected to a three-year term and are eligible to run for three consecutive terms. Banton's incumbent mayor is Joseph Fadri of the [[Liberal Party (Philippines)|Liberal Party]] while the incumbent Vice Mayor is Loijorge Fegalan of the [[Nacionalista Party]].<ref name=Election2016/>

The barangays or villages, meanwhile, are headed by elected officials, the topmost being the ''[[Punong Barangay]]'' or the Barangay Chairperson (addressed as ''Kapitan''; also known as the Barangay Captain). The ''Kapitan'' is aided by the ''[[Sangguniang Barangay]]'' (Barangay Council) whose members, called ''[[Barangay Kagawad]]'' (Councilors), are also elected.<ref>{{cite web |url=http://www.chanrobles.com/localgov3.htm |title=The Barangay |work=Local Government Code of the Philippines|publisher=Chan Robles Law Library}}</ref>

In 2011 and 2013, Banton was a recipient of the Seal of Good Housekeeping from the Department of Interior and Local Government (DILG). As recipient of the award, the local government was rewarded with one million pesos from the Performance Challenge Fund of the DILG for use in local projects.<ref>{{cite web |url=http://pcf.dilg.gov.ph/beneficiaries.php?year=2011&region=17&province=59&citymun=1619 |title=2011 PCF Beneficiaries Per Seal of Good Housekeeping |publisher=Department of Interior and Local Government |year=2011 |accessdate=17 August 2015}}</ref><ref>{{cite web |url=http://pcf.dilg.gov.ph/beneficiaries.php?year=2013&region=17&province=59&citymun=1619 |title=2013 PCF Beneficiaries Per Seal of Good Housekeeping |publisher=Department of Interior and Local Government |year=2013 |accessdate=17 August 2015}}</ref> The Seal of Good Housekeeping is a mechanism which tracks the performance of local government units, "specifically in the areas of local legislation, development planning, resource generation, and resource allocation".<ref>{{cite web |url=http://www.rappler.com/move-ph/issues/budget-watch/48073-dilg-seal-good-local-governance |title=DILG introduces 'seal of good local governance' |first=Raisa |last=Serafica |work=Rappler |date=16 January 2014 |accessdate=17 August 2015}}</ref>

==Infrastructure==
[[File:Banton motorcycle.JPG|thumb|[[Motorcycle taxi|Passenger motorcycles]] are the main mode of transportation in Banton.]]

===Utilities===
Electricity in the island is supplied by a 0.326 MW [[Diesel engine|diesel]] power plant of the Romblon Electric Cooperative (ROMELCO). However, electricity service is only available in early morning, from 4:00 to 6:00&nbsp;a.m. and at night, from 5:00 to 11:00&nbsp;p.m., due to limited fuel supplies.<ref>{{cite web |url=http://www.spug.ph/MEP%202012-2021/MEP%202012-2021%20LUZON%20AREA/MEP%202012-2021%20ROMBLON.pdf |title=Missionary Electrification Plan (2012–2021) |publisher=Small Power Utilities Group, National Power Corporation |year=2011 |accessdate=17 August 2015}}</ref> As for water supply, potable water for drinking and washing comes from water pumps, artesian wells, springs, and rainwater collection tanks in individual homes. The island has access to cellular phone and Internet service through [[Smart Communications|Smart]] and [[Globe Telecom|Globe]]. Terrestrial and cable television service are also available.<ref name="Profile"/>

===Transportation===
As seas surrounding Banton can be rough during the wet season, the best time to visit the island is from March to May during the dry (summer) season. This is also the typical time for [[Asi language|Asi]] families living in [[Metro Manila]] or abroad to visit the island since it coincides with the [[Lent]]en season and barangay fiestas.<ref name="Profile"/> Within the island, the main forms of transportation are passenger motorcycles (known elsewhere as ''habal-habal'') and motorized boats. A circumferential road connects the 17 barangays of Banton to each other.<ref name="Profile"/>

'''By sea''': Banton is accessible via wooden launches and motorized boats that regularly travel from [[Lucena, Philippines|Lucena City]], [[Quezon]]. Tourists and visitors can also take [[RORO]] vessels that ply the [[Manila]]-[[Odiongan, Romblon|Odiongan]], [[Batangas City]]-[[Odiongan, Romblon|Odiongan]], or the [[Roxas, Oriental Mindoro|Roxas]]-[[Odiongan, Romblon|Odiongan]] route. From Odiongan, Banton can be reached by jeepney and motorized boat via [[Calatrava, Romblon]].<ref name="Profile"/><ref name="Travel">{{cite web|url=http://www.romblonlifestyles.com/general-info/how-to-get-to-romblon-province-plane-boat-boracay |title=How to get to Romblon |publisher=Romblon Lifestyles |accessdate=11 August 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20150228003714/http://www.romblonlifestyles.com/general-info/how-to-get-to-romblon-province-plane-boat-boracay |archivedate=28 February 2015 |df= }}</ref> Another RORO route is from Lucena City to [[Boac, Marinduque|Boac]] or [[Mogpog, Marinduque|Mogpog]] in Marinduque. From these towns, travelers can take jeepneys to [[Buenavista, Marinduque|Buenavista]], which is only three hours away from Banton. Another alternative route is through [[Pinamalayan, Oriental Mindoro]] by motorized boats.<ref name="Profile"/><ref name="Travel"/>

'''By air''': The closest airport with active airline service is [[Tugdan Airport]] in [[Alcantara, Romblon]]. [[Philippine Airlines]] operates three weekly flights to Romblon from Manila.<ref>{{cite web|url=http://www.philippineairlines.com/news-and-events/pal-advises-passengers-come-airport-early-27/ |title=PAL opens Manila-Tablas service |publisher=Philippine Airlines |date=20 March 2015 |accessdate=17 August 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150828054029/http://www.philippineairlines.com/news-and-events/pal-advises-passengers-come-airport-early-27/ |archivedate=28 August 2015 |df= }}</ref><ref>{{cite web |url=http://www.manilatimes.net/pal-bares-new-domestic-routes/167442/ |title=PAL bares new domestic routes |first=Benjie |last=Vergara |work=The Manila Times |date=5 March 2015 |accessdate=17 August 2015}}</ref> From Alcantara, Banton can be reached in five to six hours by [[jeepney]] and motorized outrigger boats from Calatrava.<ref name="Profile"/>

==Education==
Banton has a high literacy rate owing to the establishment of several public elementary and secondary schools. All schools in the island are administered by the [[Department of Education (Philippines)|Department of Education (DepEd)]]. The main public elementary school, Banton Central School, and the main secondary school, Banton National High School, are both located in the main village of Poblacion. There are public elementary schools as well in the villages of Balogo, Banice, Libtong, Nasunogan, Tan-ag, Tungonan, and Tumalum (shared with the village of Lagang). Another secondary school, Tungonan National High School, is located in Tungonan.<ref>{{cite web |url=http://www.deped.gov.ph/sites/default/files/datasets/2013/Masterlist%20of%20Elementary%20Schools.xlsx |title=Master List of Schools for SY 2013–2014 |place=Manila, Philippines |publisher=Bureau of Public Schools, Department of Education |year=2013 |accessdate=17 August 2015}}</ref>

==Gallery==
<center>
<gallery mode=packed>
File:Banton Island.JPG|Banton Island in the [[Sibuyan Sea]]
File:Punta Matagar.JPG|Punta Matagar rock formation
File:Banton Church 16.JPG|The 15th-century limestone walls of Fuerza de San Jose
File:Banton Island, Romblon 10.JPG|Heritage houses along Banton's seawall
File:Banton_Church_25.JPG|The interior of the [[Banton Church|San Nicolas de Tolentino Parish Church]]
File:Banton Church 3.JPG|A Spanish era cannon in front of the Banton Civic Center
File:Onti Watchtower.JPG|Ruins of an old Spanish era watchtower in barrio Onti
</gallery>
</center>

==Notes==
{{reflist|30em}}

==References==
* Buencamino, Felipe & Villamor, Ignacio (1920). ''Census of the Philippine Islands Taken Under the Direction of the Philippine Legislature in the Year 1918, Volume 1.'' Manila, [[Philippines]]: Bureau of Printing.
* {{cite web|url=https://pangea.stanford.edu/ERE/db/WGC/papers/WGC/2015/16016.pdf |title=Detailed Resource Assessment of Selected Low-Enthalpy Geothermal Areas in the Philippines |author=Halcon, Rainier |author2=Fronda, Ariel |publisher=Geothermal Energy Management Division, Renewable Energy Management Bureau, Department of Energy |display-authors=etal}}
* Zorc, R. David Paul (1977). ''The Visayan Dialects of the Philippines: Subgrouping and Reconstruction.'' Canberra, Australia: Department of Linguistics, Research School of Pacific Studies, Australian National University.
* David Espinosa, Fiona Nichols, et al. (1997) ''Diving Southeast Asia.''  University of California: Periplus Action Guides. ISBN 978-962-593-141-8

==External links==
{{Commons category}}
* [http://www.yagting.com/bantonvol01/banton_vol1.asp Yagting Web Service Provider: Banton, Romblon]
* {{Facebook|BantonPH}}

{{Geographic location
| Centre = Banton
| North  = [[Buenavista, Marinduque]]<br>''[[Sibuyan Sea]]''
| East   = ''[[Sibuyan Sea]]''
| South  = ''[[Sibuyan Sea]]''<br>[[Corcuera, Romblon|Corcuera]]
| West   = [[Concepcion, Romblon|Concepcion]] / ''[[Sibuyan Sea]]''
}}
{{Romblon}}
{{MIMAROPA}}

[[Category:Municipalities of Romblon]]
[[Category:Island municipalities in the Philippines]]