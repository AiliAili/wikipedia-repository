{|{{Infobox Aircraft Begin
 | name=Synergy
 | image=Synergy Artist's Concept.jpg
 | caption=Artist's concept
}}{{Infobox Aircraft Type
 | type=[[Kit aircraft]]
 | national origin=United States
 | manufacturer=Synergy Aircraft
 | designer=John McGinnis
 | first flight=
 | introduced=
 | retired=
 | status=Under development
 | primary user=
 | number built=None
 | developed from=
 | variants with their own articles=
}}
|}
The '''Synergy Aircraft Synergy''' is a proposed five-seat, single-engine, [[Homebuilt aircraft|kit aircraft]], designed by John McGinnis of [[Kalispell, Montana]] and intended for production by his company, [[Synergy Aircraft]].<ref>{{cite web|url = http://missoulian.com/business/local/article_50d1dd28-83f4-11e0-ae9b-001cc4c002e0.html |title = Synergy Aircraft Hopes to be the Future of Flight |work = Albertson, Kristi, Daily Inter Lake |date = 2011-05-21 |accessdate = 2011-05-09}}</ref><ref name="EAA_ARTICLE">{{cite web|url = http://eaa.org/news/2011/2011-04-29_synergy.asp |title = 'Synergy' Project Revealed |work = [[Experimental Aircraft Association|EAA]] |date = 2011-04-29  |accessdate = 2011-04-14}}</ref><ref name="homepage">{{cite web|url=http://www.synergyaircraft.com/ |title=Synergy Aircraft |publisher=Synergy Aircraft |date=2012-04-27 |accessdate=2012-08-07}}</ref>

The aircraft's [[closed wing]] design, termed a "double box tail", is intended to lower [[induced drag]] and be [[Stall (flight)|stall]] resistant, along with boundary layer control methods.<ref name="EAA_ARTICLE" /> Many of the details are disclosed in {{US patent reference
 | number = 8657226
}}.
==Design and development==
Development was started in 2010 to develop the Synergy as a future kit airplane. The Synergy is the first aircraft that was designed to use the {{convert|200|hp|kW|0|abbr=on}} [[DeltaHawk Engines, Inc.|DeltaHawk]] V-4 engine. An electric-powered 1/4 scale version of the aircraft has been built and flown via radio control.<ref name="EAA_ARTICLE" />

The Synergy design was unveiled at the 2011 [[CAFE Foundation]] electric aircraft symposium.<ref>{{cite web|url=http://eaa.org/news/2011/2011-04-29_synergy.asp |title=EAA News - 'Synergy’ Project Revealed |publisher=Eaa.org |date=2011-04-29 |accessdate=2012-05-29}}</ref> The aircraft was intended to compete in the 2011 [[Green Flight Challenge#Green flight challenge|NASA/CAFE Green Flight Challenge]],<ref name="WIRED_ARTICLE">{{cite web|url = https://www.wired.com/autopia/2011/05/diesel-airplane-design-aims-for-maximum-efficiency/ |title = Odd Diesel Airplane Aims For Maximum Efficiency |work = Paur, Jason, [[Wired (magazine)]] |date = 2011-05-03  |accessdate = 2011-04-14}}</ref> but its funding and engine were delayed, forcing the team to withdraw from the competition.<ref name="NASA - After the Challenge">{{cite web|url=http://www.nasa.gov/offices/oct/stp/centennial_challenges/after_challenge/synergy.html |title=NASA - After the Challenge: Synergy Aircraft |publisher=Nasa.gov |date=2012-11-23 |accessdate=2013-04-10}}</ref>

After receiving the DeltaHawk engine in December 2011 work resumed and a funding drive was launched to complete the prototype. Intended as a [[Kickstarter]] [[crowdfunding]] project, the initial project application and appeal were rejected on the basis of not fitting in with Kickstarter's creative arts focus.<ref>{{cite web|url=http://www.avweb.com/avwebflash/news/NoKickstartForSynergy_206585-1.html |title=No Kickstart For Synergy |publisher=Avweb.com |date=2012-04-24 |accessdate=2012-05-29}}</ref> On 13 May 2012, however, Kickstarter informed McGinnis that they had reconsidered and that the project was approved.<ref name="Grady16May12">{{cite news|url = http://www.avweb.com/avwebflash/news/KickstarterRelentsOkaysSynergyProject_206698-1.html|title = Kickstarter Relents, OK's Synergy Project|accessdate = 5 June 2012|last = Grady|first = Mary|date = 16 May 2012| work = AVweb}}</ref> The project raised [[US$]]95,627 gross funds.<ref>{{cite web|url=http://www.kickstarter.com/projects/launchsynergy/synergy-aircraft-project |title=Synergy Aircraft Project on Kickstarter |publisher=kickstarter.com}}</ref>

By mid-December 2012 McGinnis indicated that the Kickstarter campaign had raised US$80,000 and that he was intending to have a flying proof-of-concept aircraft at [[AirVenture]] 2013. He also stated that if the aircraft is not complete then he will not have a display there. The Kickstarter campaign also attracted a lot of interest, but answering email and phones calls has slowed work on the prototype down.<ref name="Pew15Dec12">{{cite news|url = http://www.avweb.com/avwebflash/news/synergy_efficient_aircraft_oshkosh_airventure_OSH_207850-1.html|title = Synergy Efficient Aircraft Ready For OSH?|accessdate = 17 December 2012|last = Pew|first = Glenn|date = 15 December 2012| work = AVweb}}</ref>

==Specifications==
{{Aircraft specs
|ref=[[Experimental Aircraft Association]] and Synergy<ref name="EAA_ARTICLE" /><ref name="homepage" />
|prime units?=<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->imp
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity= 4 passengers
|length m=
|length ft=21
|length in=
|length note=
|span m=
|span ft=32
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=144.6
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=1650
|empty weight note=
|gross weight kg=
|gross weight lb=3100
|gross weight note=
|more general=
|fuel gal=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[DeltaHawk DH200]]
|eng1 type=liquid-cooled V-4 two-stroke diesel engine
|eng1 kw=
|eng1 hp=200

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note= min level flight speed
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=1500
|range note= plus reserve
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note= pressure limit
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=23.2
|wing loading note=
|power/mass met=
|power/mass imp=
|power/mass note=
|thrust/weight=
|thrust/weight note=
|more performance=
|avionics=
}}

==References==
{{reflist|30em}}

==External links==
*{{official website|http://www.synergyaircraft.com/}}
*[http://www.whaleofanidea.net/show-episodes/airplane.html Interview with John McGinnis]

[[Category:Single-engine aircraft]]
[[Category:Proposed aircraft of the United States]]
[[Category:Kickstarter projects]]