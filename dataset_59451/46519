{{Infobox person
| name          = Patrick Bet-David 
| image         = Patrick bet david CU 2014.jpg
| alt           = 
| caption       = Author/Entrepreneur Patrick Bet-David
| birth_name    = 
| birth_date    =  <!-- {{birth date and age|YYYY|MM|DD}} for living people. For people who have died, use {{Birth date|YYYY|MM|DD}}. -->
| birth_place   = 
| death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place   = 
| nationality   = American
| other_names   = 
| occupation    = Entrepreneur and financial advisor
| years_active  = 
| known_for     = 
| notable_works = The 25 Laws for Doing the Impossible, The Next Perfect Storm
}}

{{Orphan|date=July 2013}}
'''Patrick Bet-David''' is an American entrepreneur and [[financial advisor]]. 

In the late 1980s he fled Iran with his father during the [[Iran–Iraq War]]; they lived for two years in a refugee camp in Germany before immigrating to the US in 1990, when Bet-David was 12 years old.<ref name=OL2015>Joe Schoffstall for Opportunity Lives.  July 31, 2015 [https://web.archive.org/web/20150803031452/http://opportunitylives.com/his-family-was-forced-to-flee-iran-now-hes-educating-americans-on-the-importance-of-free-enterprise/ His Family was Forced to Flee Iran – Now He’s Educating Americans on the Importance of Free Enterprise]</ref><ref name=DenverPost/>  After high school he enlisted in the US army and served with the [[101st Airborne Division]].<ref name=OL2015/>

After leaving the Army he got a job at [[Morgan Stanley]].<ref name=OL2015/> After a dinner event with [[George Will]] in 2009, he launched a website called Saving America and later a radio show broadcast in Los Angeles, to encourage people to be entrepreneurial and to educate people about fiscal responsibility.<ref name=OL2015/> Later that year he opened a financial services agency offering term and permanent insurance, debt settlements, and  401(k) rollovers into indexed annuities called PHP Agency, which stood for "People Helping People".<ref name=OL2015/><ref name=DenverPost/> 

Before starting People Helping People, he was part of another financial marketing organization called World Financial Group from 2002-2009. He went on to become one of the Chairmans of the company before 2009 where he began PHP Agency.{{cn|date=March 2017}}

In 2013 he gave interviews advising people to avoid debt, and based on the housing market at that time, not to buy houses but instead rent, and to use any wealth they had acquired to start a business.<ref name=DenverPost>Marni Jameson for the Denver Post. October 18, 2013  [http://www.denverpost.com/homegarden/ci_24341379/home-buying-go-cold-clear-logic-and-rent?source=rss Home buying: Go with cold, clear logic (and rent)]</ref><ref name=Fox>Donna Fuscaldo for Fox News Business. May 15, 2013 [http://fxn.ws/10P7AA8 Should Home Ownership still be Part of the American Dream?]</ref>  In 2015 he gave an interview advising people graduating from high school to avoid the debt of going to college unless they had a clear professional goal of being a doctor or lawyer or want to play professional sports, and advised them instead to travel or intern for a year.<ref>Natalie Walters for Business Insider. Dec. 29, 2015 [http://www.businessinsider.com/unconventional-advice-for-young-people-2015-11 Here's the unconventional piece of career advice one entrepreneur is dishing out]</ref>

Bet-David has published four books, ''The 25 Laws for Doing the Impossible'' (2011), ''The Next Perfect Storm'' (2012), "The Life of an Entrepreneur in 90 Pages" (2016) and his most controversial publication, "Drop Out and Get Schooled" (2017). Doing the Impossible is a self-help book about realizing your goals, The Perfect Storm tries to teach people how to profit from bubbles like the real estate market in the 2000s and the stock market of the late 1990s, Entrepreneur in 90 pages outlines a basis for starting a business and Drop out addresses the problems with modern day higher education.<ref name=OL2015/>

== References ==
{{reflist}}

== External links ==
* [http://www.Patrickbetdavid.com www.patrickbetdavid.com/]

{{DEFAULTSORT:Bet-David, Patrick}}
[[Category:Living people]]
[[Category:American people of Iranian descent]]
[[Category:Financial advisors]]