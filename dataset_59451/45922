{{Use dmy dates|date=February 2015}}
{{Use British English|date=February 2015}}
{{Infobox Football club season
|club= [[Chelsea F.C.|Chelsea]]
|season=1985-86
|manager=[[John Hollins]]
|chairman=[[Ken Bates]]
|stadium=[[Stamford Bridge (stadium)|Stamford Bridge]]
|league=[[1985-86 Football League|First Division]]
|league result=6th
|cup2=[[1985-86 FA Cup|FA Cup]]
|cup2 result=[[1985-86 FA Cup#Fourth Round Proper|Fourth round proper]]
|cup3=[[1985-86 Football League Cup|Football League Cup]]
|cup3 result=[[1985-86 Football League Cup#Replays 3|Fifth round replay]]
|league topscorer=[[Kerry Dixon|Dixon]]/[[David Speedie|Speedie]] (14)
|season topscorer=
|highest attendance=
|lowest attendance=
|average attendance=
| pattern_b1  = _chelsea1985h
| pattern_la1 = _chelsea1985h
| pattern_ra1 = _chelsea1985h
| pattern_sh1 = 
| leftarm1    = 0000FF
| body1       = 0000FF
| rightarm1   = 0000FF
| shorts1     = 0000FF
| socks1      = 0000FF
| pattern_b2  = _chelsea1985a
| pattern_ra2 = _chelsea1985a
| pattern_la2 = _chelsea1985a
| pattern_so2 = 
| leftarm2    = FF0000
| body2       = FF0000
| rightarm2   = FF0000
| shorts2     = FF0000
| socks2      = FF0000
| pattern_b3  =  _chelsea1984a
| pattern_ra3  = _chelsea1984a
| pattern_la3  = _chelsea1984a
| pattern_so3  = 
| leftarm3     = FFFFFF
| body3        = FFFFFF
| rightarm3    = FFFFFF
| shorts3      = FFFFFF
| socks3       = FFFFFF
|prevseason=[[1984-85 Chelsea F.C. season|1984–85]]
|nextseason=[[1986-87 Chelsea F.C. season|1986–87]]
}}
In the 1985-86 season [[Chelsea F.C.|Chelsea]] played in the First Division for the second successive season.

It was the first season under the management of [[John Hollins]], who had previously been a member of Chelsea's victories in the [[1970 FA Cup Final]] and [[1971 European Cup Winners' Cup Final]]. On 1 January 1986 Chelsea were second, two points behind leaders [[Manchester United]] and in March the club were still in second, four points behind [[Everton F.C.|Everton]] with two games in hand. The two games in hand were lost 4–0 at home to [[West Ham United]] and 6–0 away at [[Queens Park Rangers]]. After gaining just 9 points from a possible 33 in the last 11 games, Chelsea finished 6th for the second season in succession.<ref name="guardian1">[https://www.theguardian.com/sport/blog/2008/nov/27/scott-murray-85-86-football-season "The forgotten story of: the 1985–86 First Division season"]. ''[[The Guardian]]''. 27 November 2008. Retrieved 5 June 2013.</ref>

On 23 March 1986 Chelsea won the [[1986 Full Members Cup Final]] 5–4 at [[Wembley Stadium]] against [[Manchester City]] with [[David Speedie]] (the club's joint top scorer in the league along with Kerry Dixon on 14 goals) scoring Chelsea's only Wembley hat-trick. [[Colin Lee]] scored Chelsea's other two goals. Due to the widespread negaivity over English football after the 1985 [[Heysel disaster]], victorious manager Hollins was quoted as saying "If football is dying, I hope it's dying like that".<ref name="guardian1"/>

After an away win at [[Shrewsbury Town]] in the 3rd round of the FA Cup, Chelsea lost 2–1 at home to eventual Champions Liverpool in the fourth round.<ref>[http://thechels.info/wiki/1985-86_FA_Cup "1985–86 FA Cup"]. The Chels. Retrieved 5 June 2013.</ref>

In the [[1985-86 Football League Cup]] Chelsea beat [[Mansfield Town]] in a two-legged second round but required replays to advance past Fulham and Everton in the subsequent rounds. After drawing 1–1 away to Queens Park Rangers Chelsea lost the fifth round replay 2–1 at Stamford Bridge.<ref>[http://thechels.info/wiki/1985-86_League_Cup "1985–86 League Cup"]. The Chels. Retrieved 5 June 2013.</ref>

==League Scorers==
*{{flagicon|ENG}} [[Kerry Dixon]] 14
*{{flagicon|SCO}} [[David Speedie]] 14
*{{flagicon|SCO}} [[Pat Nevin]] 7
*{{flagicon|ENG}} [[Nigel Spackman]] 7
*{{flagicon|ENG}} [[Jerry Murphy]] 3
*{{flagicon|ENG}} [[Keith Jones (English footballer)|Keith Jones]] 2
*{{flagicon|SCO}} [[Doug Rougvie]] 2
*{{flagicon|ENG}} [[John Bumstead]] 1
*{{flagicon|ENG}} [[Paul Canoville]] 1
*{{flagicon|ENG}} [[Micky Hazard]] 1
*{{flagicon|SCO}} [[Joe McLaughlin (footballer)|Joe McLaughlin]] 1
*{{flagicon|ENG}} [[Colin Pates]] 1
*{{flagicon|SCO}} [[Duncan Shearer]] 1
*[[Own goal]] 2<ref>[http://free-elements.com/England/Goals/CheL/CheL1985.html "Chelsea 1985–1986"]. Free-elements.com. Retrieved 5 June 2013.</ref>

===Other players===
*{{flagicon|ENG}} [[Steve Francis (footballer)|Steve Francis]]
*{{flagicon|ENG}} [[Darren Wood]]
*{{flagicon|ENG}} [[Colin Lee]]
*{{flagicon|ENG}} [[Keith Dublin]]

==Kit==
Chelsea's kit was produced by [[Le Coq Sportif]] for the final of five seasons (starting 1981–82). The home kit was only worn this season, and re-introduced blue socks in favour of white for the first time since 1967–68. The shirt had shadow-stripes, a popular 1980s style, and the only white was around the v-neck and sleeves. The kit was unsponsored, as it had been since the end of Chelsea's first-ever sponsorship, with [[Gulf Air]] in the 1983–84 season.<ref>[http://historicalkits.co.uk/Chelsea/Chelsea.htm "Chelsea"]. Historical Football Kits. Retrieved 5 June 2013.</ref>

The away kit closely followed the design of the home, but in red with white trim. While the home kit had plain white socks, the red socks of the away kit featured four hoops: a white and blue on both the turnover and the leg. For a third kit, the white away kit from the 1984–85 season was retained. As well as a V-neck, it included red and blue horizontal pinstripes on the shirt and hoops of blue and red on the socks.<ref>[http://historicalkits.co.uk/Chelsea/Chelsea-change-kits.html "Chelsea Change Kits"]. Historical Football Kits. Retrieved 5 June 2013.</ref>

==References==
{{Reflist|colwidth=40em}}

{{Chelsea F.C. seasons}}
{{1985–86 in English football}}

{{DEFAULTSORT:1985-86 Chelsea F.C. season}}
[[Category:Articles created via the Article Wizard]]
[[Category:Chelsea F.C. seasons]]
[[Category:English football clubs 1985–86 season|Chelsea]]