{{italic title}}
{{ infobox bibliographic database
| image       = 
| caption     = 
| producer    =[[DECHEMA]] 
| country     = Germany
| history     =  
| languages   = German, English
| providers   = Dialog database
| title       = Chemical Engineering and Biotechnology Abstracts 
| disciplines = Chemical property data, Chemical processing, Laboratory experiments, Engineering theory, Computer applications, Environmental protection, Reaction engineering, Plant and personnel safety, Measurement and process control, and Physical property data
| depth       = Index & abstract
| formats     = Meetings, Conferences, Symposia,  Reports, Books, Monographs, Journal Articles, and Press Releases 
| temporal    = 1963 to the present 
| geospatial  = Global with no restrictions
| number      = 685,000 +
| updates     = Monthly
| p_title     = Theoretical Chemical Engineering, Environmental Protection and Process Safety,  
Process and Chemical Engineering, Biotechnology: Apparatus, Plant, and Equipment, and Current Biotechnology
| p_dates     = 
| ISSN        = 
| web         = 
| titles      =  
}}

'''Chemical Engineering and Biotechnology Abstracts''' ('''CEABA-VTB''') is an abstracting and indexing service that is published by DECHEMA, [[BASF]], and Bayer Technology Services, all based in [[Germany]]. This is a bibliographic database that covers multiple disciplines.

==Subject coverage==
Subject coverage includes [[engineering]], [[management]], [[manufacturing plant]]s, equipment, production, and processing pertaining to various disciplines. The fields of interest are bio-process engineering, [[chemical engineering]], [[process engineering]], [[environmental protection]] (including safety), [[Industrial fermentation|fermentation]], enzymology, bio-transformation, information technology, technology and testing of materials (including [[corrosion]]), mathematical methods (including modeling), measurement (including control of processes), utilities (including services). Also covered are production processes and process development. [[CAS registry numbers]] are also part of this database.<ref name=blue>
{{cite web
  | title =Chemical Engineering And Biotechnology Abstracts 
  | work = 
  | publisher =Dialog Bluesheets
  | date =December 7, 2007
  | url =http://library.dialog.com/bluesheets/html/bl0315.html
  | format =Online web page 
  | doi = 
  | accessdate =2011-10-12}}</ref><ref name=info>
{{cite web
  | title =Chemical Engineering And Biotechnology Abstracts 
  | work =STN database 
  | publisher =American Chemical Society and FIZ Karlsruhe
  | date =March 2011 
  | url =http://www.cas.org/ASSETS/02004C24863242C684295F8159564907/ceabavtb.pdf 
  | format =Free PDF download 
  | doi = 
  | accessdate =2011-10-12}}</ref><ref name=summary>
{{cite web
  | title =STN Database Summary Sheets
  | work = 
  | publisher =American Chemical Society 
  | date =March 2011 
  | url =http://www.cas.org/support/stngen/dbss/index.html
  | format =Online web page 
  | doi = 
  | accessdate =2011-10-12}}</ref>

==References==
{{reflist}}

[[Category:Bibliographic databases in engineering]]
[[Category:Bibliographic databases in computer science]]