{{good article}}

{{Infobox building
| name                = Borley Rectory
| former_names        = 
| alternate_names     = 
| status              = 
| image               = BorleyRectory1892.jpg
| image_alt           = 
| image_size          =
| caption             = The east face of the rectory in 1892
| building_type       = [[Rectory]]
| architectural_style = [[Gothic Revival architecture|Gothic Revival]]
| structural_system   = 
| cost                = 
| ren_cost            = 
| client              = 
| owner               = 
| address             = [[Borley]], [[Essex]], England
| coordinates         = {{coord|52.0546|0.6942|type:landmark_region:GB-ESS}}
| groundbreaking_date = 
| completion_date     = {{Start date|1862}}
| demolition_date     = {{End date|1944}}
| destruction_date    = 
| height              = 
| floor_count         = 
| floor_area          = 
| grounds_area        = 
| architect           = 
| main_contractor     = 
| awards              = 
| designations        = 
| rooms               = 
| url                 = 
}}

'''Borley Rectory''' was a Victorian house that gained fame as "the most haunted house in England" after being described as such by [[Harry Price]].{{sfnp|Floyd|2002|p=36}} Built in 1862 to house the [[rector (ecclesiastical)#Anglican churches|rector]] of the parish of [[Borley]] and his family, it was badly damaged by fire in 1939 and demolished in 1944.

The large [[Gothic Revival architecture|Gothic]]-style rectory in the village of Borley had been alleged to be haunted ever since it was built. These reports multiplied suddenly in 1929, after the ''[[Daily Mirror]]'' published an account of a visit by paranormal researcher Harry Price, who wrote two books supporting claims of paranormal activity.

The uncritical acceptance of Price's reports prompted a formal study by the [[Society for Psychical Research]] (SPR), which rejected most of the sightings as either imagined or fabricated and cast doubt on Price's credibility. His claims are now generally discredited by ghost historians. Neither the SPR's report nor the more recent biography of Price has quelled public interest in the stories, and new books and television documentaries continue to satisfy public fascination with the rectory.

A short programme commissioned by the [[BBC]] about the alleged manifestations, scheduled to be broadcast in September 1956, was cancelled owing to concerns about a possible legal action by Marianne Foyster, widow of the last rector to live in the house.{{r|HauntedRectory}}
{{TOC limit|2}}
==History==
Borley Rectory was constructed on Hall Road near [[Borley Church]] by the Reverend Henry Dawson Ellis Bull in 1862;<ref>''Bury and Norwich Post'', August 1862</ref> he moved in a year after being named [[Rector (ecclesiastical)|rector]] of the parish.<ref>''Suffolk Free Press'', February 20, 1862</ref> The house replaced an earlier rectory on the site that had been destroyed by fire in 1841.{{sfnp|Downes|2012|loc=Background to Borley Rectory}} It was eventually enlarged by the addition of a wing to house Bull's family of fourteen children.{{r|Glanville}}

The nearby church, the nave of which may date from the 12th century,{{sfnp|Pevsner|1973|p=95|ps=}} serves a scattered rural community of three hamlets that make up the parish. There are several substantial farmhouses and the fragmentary remains of Borley Hall, once the seat of the [[Waldegrave family]]. Ghost hunters quote the legend of a [[Benedictine]] [[monastery]] supposedly built in this area in about 1362, according to which a monk from the monastery conducted a relationship with a nun from a nearby convent. After their affair was discovered, the monk was executed and the nun bricked up alive in the convent walls. It was confirmed in 1938 that this legend had no historical basis and seemed to have been fabricated by the rector's children to romanticise their Gothic-style red-brick rectory. The story of the walling-up of the nun may have come from [[Rider Haggard]]'s novel ''[[Montezuma's Daughter]]'' (1893) or [[Walter Scott]]'s epic poem "[[Marmion (poem)|Marmion]]" (1808).{{r|Bullsheet}}

==Hauntings==
[[File:Borley3.jpg|thumb|right|250px|Alleged sighting in the grounds of the rectory]]
The first paranormal events reportedly occurred in about 1863, since a few locals later remembered having heard unexplained footsteps within the house at about that time. On 28 July 1900, four daughters of the rector, Henry Dawson Ellis Bull, saw what they thought was the ghost of a nun at twilight, about {{convert|40|yard}} from the house; they tried to talk to it, but it disappeared as they got closer.{{sfnp|Price|2006|pp=28–30}} The local organist, Ernest Ambrose later said that the family at the rectory were "very convinced that they had seen an apparition on several occasions".<ref>{{cite news |first=Ernest |last=Ambrose |title=Organs and Organists |work=Melford Memories |year=1972}}</ref> Various people claimed to have witnessed a variety of puzzling incidents, such as a phantom coach driven by two [[Headless Horseman|headless horsemen]], during the next four decades. Bull died in 1892 and his son, the Reverend Henry ("Harry") Foyster Bull, took over the living.{{sfnp|Price|2006|p=16}}

On 9 June 1928, Harry Bull died and the rectory again became vacant.{{sfnp|Price|2006|pp=16–17}} In the following year, on 2 October,{{sfnp|Price|2006|p=17}} the Reverend Guy Eric Smith and his wife moved into the house. Soon after moving in, Smith's wife, while cleaning out a cupboard, came across a brown paper package containing the skull of a young woman.{{sfnp|Price|2006|p=20}} Shortly after, the family reported a variety of incidents including the sounds of servant bells ringing despite their being disconnected, lights appearing in windows and unexplained footsteps. In addition, Smith's wife believed she saw a [[horse-drawn carriage]] at night. The Smiths contacted the ''[[Daily Mirror]]'' asking to be put in touch with the [[Society for Psychical Research]] (SPR). On 10 June 1929 the newspaper sent a reporter, who promptly wrote the first in a series of articles detailing the mysteries of Borley. The paper also arranged for [[Harry Price]], a paranormal researcher, to make his first visit to the house. He arrived on 12 June{{sfnp|Price|2006|p=19}} and immediately phenomena of a new kind appeared, such as the throwing of stones, a vase and other objects. "Spirit messages" were tapped out from the frame of a mirror. As soon as Price left, these ceased. Smith's wife later maintained that she already suspected Price, an expert conjurer, of falsifying the phenomena.{{sfnp|Dingwall|Goldney|Hall|1956|p=44}}

The Smiths left Borley on 14 July 1929 and the parish had some difficulty in finding a replacement. The following year the Reverend Lionel Algernon Foyster (1878–1945), a first cousin of the Bulls, and his wife Marianne (née Marianne Emily Rebecca Shaw) (1899–1992) moved into the rectory{{sfnp|Price|2006|p=17}} with their adopted daughter Adelaide, on 16 October 1930.{{sfnp|Dingwall|Goldney|Hall|1956|p=75}} Lionel Foyster wrote an account of various strange incidents that occurred between the time the Foysters moved in and October 1935, which was sent to Harry Price. These included bell-ringing, windows shattering, throwing of stones and bottles, wall-writing and the locking of their daughter in a room with no key. Marianne Foyster reported to her husband a whole range of [[poltergeist]] phenomena that included her being thrown from her bed.{{sfnp|Price|2006|p=36}} On one occasion, Adelaide was attacked by "something horrible".{{sfnp|O'Neal|1994|p=80}} Foyster tried twice to conduct an [[exorcism]], but his efforts were fruitless; in the middle of the first exorcism, he was struck in the shoulder by a fist-size stone. Because of the publicity in the ''Daily Mirror'', these incidents attracted the attention of several [[psychic researcher]]s, who after investigation were unanimous in suspecting that they were caused, consciously or unconsciously, by Marianne Foyster. She later said that she felt that some of the incidents were caused by her husband in concert with one of the psychic researchers, but other events appeared to her to be genuine paranormal phenomena. She later admitted that she was having a [[sexual relationship]] with the lodger, Frank Pearless,{{r|Lawless}}{{efn|Pearless styled himself François D'Arles, and in his diaries Lionel Foyster refers to him as "Frank Lawless".{{r|Lawless}}}} and that she used paranormal explanations to cover up her liaisons.{{sfnp|Wood|1992|p=}} The Foysters left Borley in October 1935 as a result of Lionel Foyster's ill health.{{sfnp|Dingwall|Goldney|Hall|1956|p=75}}

==Price investigation==
Borley remained vacant for some time after the Foysters' departure. In May 1937, Price took out a year-long rental agreement with [[Queen Anne's Bounty]], the owners of the property.{{sfnp|Price|2006|p=}}<ref name=nicholas1>{{cite book |last=Nicholas |first=Margaret |year=1986 |title=World's Greatest Psychics & Mystics |publisher=Octopus |isbn=978-0-600-58612-8}}</ref>

Through an advertisement in ''[[The Times]]'' on 25 May 1937{{sfnp|Price|2006|p=38}} and subsequent personal interviews, Price recruited a corps of 48 "official observers", mostly students, who spent periods, mainly during weekends, at the rectory with instructions to report any phenomena that occurred. In March 1938 Helen Glanville (the daughter of S. J. Glanville, one of Price's helpers) conducted a [[planchette]] [[séance]] in Streatham in south London.{{sfnp|Price|2006|pp=276–80}} Price reported that she made contact with two spirits, the first of which was that of a young nun who identified herself as Marie Lairre.{{sfnp|Price|2006|pp=276–80}} According to the planchette story Marie was a French nun who left her religious order and travelled to England to marry a member of the Waldegrave family, the owners of Borley's 17th-century manor house, Borley Hall. She was said to have been murdered in an earlier building on the site of the rectory, and her body either buried in the cellar or thrown into a disused well.{{sfnp|Fanthorpe|Fanthorpe|1997|p=52}} The wall writings were alleged to be her pleas for help; one read "Marianne, please help me get out".{{sfnp|Floyd|2002|p=37}}

The second spirit to be contacted identified himself as Sunex Amures,{{sfnp|Price|2006|pp=279–80}} and claimed that he would set fire to the rectory at nine o'clock that night, 27 March 1938.{{sfnp|Karl|2007|p=33}} He also said that, at that time, the bones of a murdered person would be revealed.{{sfnp|Wood|1992|p=50}}

===Fire===
[[File:Borley Rectoy2.jpg|thumb|right|Rectory after the fire]]
On 27 February 1939 the new owner of the rectory, Captain W. H. Gregson, was unpacking boxes and accidentally knocked over an [[oil lamp]] in the hallway.{{sfnp|Price|2006|p=13}}{{efn|The house was never connected to a gas or electricity supply, and water was obtained from a well in the courtyard.{{r|Glanville}}}} The fire quickly spread and the house was severely damaged. After investigating the cause of the blaze the insurance company concluded that the fire had been started deliberately.{{sfnp|Wood|1992|pp=3–4}}

A Miss Williams from nearby Borley Lodge said she saw the figure of the ghostly nun in the upstairs window and, according to Harry Price, demanded a fee of one guinea for her story.{{sfnp|Dingwall|Goldney|Hall|1956|p=147}} In August 1943, Price conducted a brief dig in the cellars of the ruined house and discovered two bones thought to be of a young woman.{{sfnp|Dingwall|Goldney|Hall|1956|p=154}} The bones were given a Christian burial in Liston churchyard, after the parish of Borley refused to allow the ceremony to take place on account of the local opinion that the bones found were those of a pig.{{sfnp|Fielding|O'Keeffe|2011|loc=chapter 4}}

==Society for Psychical Research investigation==
After Price's death in 1948, [[Eric J. Dingwall|Eric Dingwall]], [[K. M. Goldney]] and [[Trevor H. Hall]], three members of the [[Society for Psychical Research]] (SPR), two of whom had been Price's most loyal associates, investigated his claims about Borley. Their findings were published in a 1956 book, ''The Haunting of Borley Rectory'', which concluded that Price had fraudulently produced some of the phenomena.<ref name="Dingwall1956">Dingwall, E. J.; Goldney, K. M.; Hall, T. H. (1956). [http://www.harrypricewebsite.co.uk/Borley/PriceatBorley/HBR/hbr-contents.htm ''The Haunting of Borley Rectory'']. Duckworth.</ref>

The "Borley Report", as the SPR study has become known, stated that many of the phenomena were either faked or due to natural causes such as rats and the strange acoustics attributed to the odd shape of the house. In their conclusion, Dingwall, Goldney, and Hall wrote "when analysed, the evidence for haunting and poltergeist activity for each and every period appears to diminish in force and finally to vanish away."<ref name="Dingwall1956"/> [[Terence Hines]] wrote that "Mrs. Marianne Foyster, wife of the Rev. Lionel Foyster who lived at the rectory from 1930 to 1935, was actively engaged in fraudulently creating [haunted] phenomena. Price himself 'salted the mine' and faked several phenomena while he was at the rectory."<ref>[[Terence Hines|Hines, Terence]]. (2003). ''Pseudoscience and the Paranormal''. Prometheus Books. pp. 94–95. ISBN 978-1573929790</ref>

Marianne Foyster, later in her life, admitted she had seen no apparitions and that the alleged [[ghost]]ly noises were caused by the wind, friends she invited to the house and in other cases by herself playing practical jokes on her husband.<ref name="Hoggart1995">Hoggart, Simon., Hutchinson, Mike. (1995). ''Bizarre Beliefs''. Richard Cohen Books. p. 186. ISBN 978-1573921565</ref> Many of the legends about the rectory had been invented. The children of the Rev. Harry Bull who lived in the house before Lionel Foyster claimed to have seen nothing and were surprised they had been living in what was described as England's most haunted house.<ref name="Hoggart1995"/>

Robert Hastings was one of the few SPR researchers to defend Price.<ref>Hastings, Robert. (1969).    [http://www.harrypricewebsite.co.uk/Borley/PriceatBorley/Hastings/hastings-contents.htm ''An Examination of the Borley Report'']. Journal of the Society for Psychical Research 55: 66–175.</ref> Price's literary executor [[Paul Tabori]] and [[Peter Underwood (parapsychologist)|Peter Underwood]] have also defended Price against accusations of fraud. A similar approach was made by Ivan Banks in 1996.<ref>Tabori, Paul., [[Peter Underwood (parapsychologist)|Underwood, Peter]]. (1973). ''Ghosts of Borley: Annals of the Haunted Rectory''. David & Charles. ISBN 978-0715361184</ref><ref>Banks, Ivan. (1996). ''The Enigma of Borley Rectory''. Foulsham & Co Ltd. ISBN 978-0572021627</ref> Michael Coleman in an SPR report in 1997 wrote Price's defenders are unable to rebut the criticisms convincingly.<ref>Coleman, Michael. (1997). ''The Flying Bricks of Borley''. Journal of the Society for Psychical Research. Volume. 61, No. 847.</ref>

==See also==
*[[List of ghosts]]

==References==
===Notes===
{{notelist|notes=}}

===Citations===
{{reflist|30em|refs=

<ref name="Bullsheet">
{{cite web |last=Clarke |first=Andrew |title=Bullsheet. The Bulls at Borley Rectory |work=The Bones of Borley |year=2000 |url=http://www.foxearth.org.uk/BorleyRectory/BullSheet.htm |publisher=Foxearth and District Local History Society |accessdate=16 August 2013}}
</ref>

<ref name="Glanville">
{{cite journal |last=Glanville |first=Sidney H. |title=The Strange Happenings at Borley Rectory&nbsp;– Full Account of England's Most Famous Modern Ghost |date=October 1951 |magazine=Fate |volume=4 |issue=7 |pages=89–107}}
</ref>

<ref name="HauntedRectory">
{{cite web |title=The Haunted Rectory ... |url=http://www.foxearth.org.uk/TheHauntedRectory.html |work=The Bones of Borley |publisher=Foxearth and District Local History Society |accessdate=16 August 2013}}
</ref>

<ref name="Lawless">
{{cite web |last=Clarke |first=Andrew |title=Lawless, the Lodger |url=http://www.foxearth.org.uk/BorleyRectory/LawlessTheLodger.html |work=The Bones of Borley |year=2003 |publisher=Foxearth and District Local History Society |accessdate=16 August 2013}}</ref>

}}

===Bibliography===
{{refbegin}}
*{{cite book |last1=Dingwall |first1=E. J. |last2=Goldney |first2=K. M. |last3=Hall |first3=T. H. |title=The Haunting of Borley Rectory |year=1956 |publisher=Duckworth |ref=harv}}
*{{cite book |last=Downes |first=Wesley |contribution=Background to Borley Rectory |title=The Ghosts of Borley |year=2012 |publisher=David & Charles |isbn=978-1-4463-5788-0 |ref={{harvid|Downes|2012}}}}
*{{cite book |last1=Fanthorpe |first1=Lionel |last2=Fanthorpe |first2=Patricia |title=World's Greatest Unsolved Mysteries |year=1997 |publisher=Dundum |isbn=978-0-88882-194-2 |ref=harv}}
*{{cite book |last1=Fielding |first1=Yvette |last2=O'Keeffe |first2=Ciaran |title=Ghost Hunters: A Guide to Investigating the Paranormal |year=2011 |publisher=Hachette UK |isbn=978-1-4447-4029-5 |ref={{harvid|Fielding|O'Keeffe|2011}}}}
*{{cite book |last=Floyd |first=E. Randall |title=In the Realm of Ghosts and Hauntings |year=2002 |publisher=Harbor House |isbn=978-1-891799-06-8 |ref=harv}}
*{{cite book |last=Karl |first=Jason |title=Illustrated History of the Haunted World |year=2007 |publisher=New Holland Publishers |isbn=978-1-845376-87-1 |ref=harv}}
*{{cite book |last=O'Neal |first=Michael |title=Haunted Houses: Opposing Viewpoints |year=1994 |publisher=Greenhaven Press |isbn=978-1-565100-95-4 |ref=harv}}
*{{cite book |last=Pevsner |first=Nikolaus |authorlink=Nikolaus Pevsner |title=London |year=1973 |publisher=Penguin |isbn=978-0-1407-1011-3 |ref=harv}}
*{{cite book |last=Price |first=Harry |title=The End of Borley Rectory |publisher=Hesperides Press [orig. G. G. Harrap & Co.] |year=2006 |isbn=978-1-4067-2212-3 |origyear=1946 |ref=harv}}
*{{cite book |last=Wood |first=Robert |title=The Widow of Borley |year=1992 |publisher=Duckworth |isbn=978-0-7156-2419-7 |ref=harv}}
{{refend}}

==Further reading==
*{{citation |last=Bardens |first=Dennis| authorlink=Dennis Bardens |title=Ghosts and Hauntings|year=1997 |publisher=Senate Books |ref=none}}
*{{citation |last=Booth |first=John| authorlink=John Booth (magician) |title=Psychic Paradoxes|year=1986 |publisher=Prometheus Books |ref=none}}
*{{citation |last=Cohen |first=Daniel| authorlink=Daniel Cohen (children's writer) |title=The Encyclopedia of Ghosts|year=1991 |publisher=HarperCollins Publishers |ref=none}}
*{{citation |last=Hall |first=Trevor| authorlink=Trevor H. Hall |title=New Light on Old Ghosts|year=1965 |publisher=Duckworth |ref=none}}
*{{citation |last=Paul |first=Philip| title=Some Unseen Power: Diary of a Ghost-hunter|year=1985 |publisher=Robert Hale |ref=none}}
*{{citation |last=Turner |first=James |title=My Life with Borley Rectory|year=1950 |publisher=Bodley Head |ref=none}}
*{{citation |last=Bloom |first=Clive |title=Harry Price and the Haunted Rectory |work=Creepers: British Horror and Fantasy in the Twentieth Century |pages=75–85 |year=1993 |editor-last=Bloom |editor-first=Clive |ref=none}}

==External links==
{{Commons category|Borley Rectory}}
*[http://harrypricewebsite.co.uk Harry Price Website – Contains a comprehensive section on Borley Rectory]
*[http://www.harrypricewebsite.co.uk/BooksabtHP/psychic-detective-review-mail.htm Ghostbuster of Fraud by Simon Edge, reprinted in HarryPrice website]
*[http://www.foxearth.org.uk/BorleyRectory/index.html Local History site for Borley] – Has a comprehensive historical analysis of the Borley Rectory affair
*[http://www.foxearth.org.uk/BorleyRectoryPictures/Page.html Large collection of photographs of Borley Rectory and the various participants of the affair ]
*[http://archives.ulrls.lon.ac.uk/dispatcher.aspx?action=search&database=ChoiceArchive&search=priref=110007211   Harry Price papers including archives on Borley Rectory]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* [https://3dwarehouse.sketchup.com/model.html?id=6e06d36775ed574b8a9e853793ff1633 3D Interactive model built in Google Sketchup from the original Glanville plans]

{{Ghosts}}

[[Category:Clergy houses in England]]
[[Category:Houses in Essex]]
[[Category:Essex folklore]]
[[Category:Reportedly haunted locations in the East of England]]